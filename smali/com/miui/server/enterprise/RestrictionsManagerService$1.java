class com.miui.server.enterprise.RestrictionsManagerService$1 extends android.database.ContentObserver {
	 /* .source "RestrictionsManagerService.java" */
	 /* # annotations */
	 /* .annotation system Ldalvik/annotation/EnclosingMethod; */
	 /* value = Lcom/miui/server/enterprise/RestrictionsManagerService;->startWatchLocationRestriction()V */
} // .end annotation
/* .annotation system Ldalvik/annotation/InnerClass; */
/* accessFlags = 0x0 */
/* name = null */
} // .end annotation
/* # instance fields */
final com.miui.server.enterprise.RestrictionsManagerService this$0; //synthetic
/* # direct methods */
 com.miui.server.enterprise.RestrictionsManagerService$1 ( ) {
/* .locals 0 */
/* .param p1, "this$0" # Lcom/miui/server/enterprise/RestrictionsManagerService; */
/* .param p2, "handler" # Landroid/os/Handler; */
/* .line 102 */
this.this$0 = p1;
/* invoke-direct {p0, p2}, Landroid/database/ContentObserver;-><init>(Landroid/os/Handler;)V */
return;
} // .end method
/* # virtual methods */
public void onChange ( Boolean p0 ) {
/* .locals 5 */
/* .param p1, "selfChange" # Z */
/* .line 105 */
/* invoke-super {p0, p1}, Landroid/database/ContentObserver;->onChange(Z)V */
/* .line 106 */
v0 = miui.securityspace.CrossUserUtils .getCurrentUserId ( );
/* .line 107 */
/* .local v0, "currentUserId":I */
v1 = this.this$0;
com.miui.server.enterprise.RestrictionsManagerService .-$$Nest$fgetmContext ( v1 );
final String v2 = "gps_state"; // const-string v2, "gps_state"
int v3 = 1; // const/4 v3, 0x1
v1 = com.miui.enterprise.settings.EnterpriseSettings .getInt ( v1,v2,v3,v0 );
/* .line 109 */
/* .local v1, "mode":I */
int v2 = 4; // const/4 v2, 0x4
final String v3 = "location_mode"; // const-string v3, "location_mode"
final String v4 = "Enterprise-restric"; // const-string v4, "Enterprise-restric"
/* if-ne v1, v2, :cond_0 */
/* .line 110 */
final String v2 = "FORCE_OPEN GPS"; // const-string v2, "FORCE_OPEN GPS"
android.util.Slog .d ( v4,v2 );
/* .line 111 */
v2 = this.this$0;
com.miui.server.enterprise.RestrictionsManagerService .-$$Nest$fgetmContext ( v2 );
(( android.content.Context ) v2 ).getContentResolver ( ); // invoke-virtual {v2}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;
int v4 = 3; // const/4 v4, 0x3
android.provider.Settings$Secure .putIntForUser ( v2,v3,v4,v0 );
/* .line 113 */
} // :cond_0
/* if-nez v1, :cond_1 */
/* .line 114 */
final String v2 = "Close GPS"; // const-string v2, "Close GPS"
android.util.Slog .d ( v4,v2 );
/* .line 115 */
v2 = this.this$0;
com.miui.server.enterprise.RestrictionsManagerService .-$$Nest$fgetmContext ( v2 );
(( android.content.Context ) v2 ).getContentResolver ( ); // invoke-virtual {v2}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;
int v4 = 0; // const/4 v4, 0x0
android.provider.Settings$Secure .putIntForUser ( v2,v3,v4,v0 );
/* .line 118 */
} // :cond_1
} // :goto_0
return;
} // .end method
