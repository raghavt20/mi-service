.class public Lcom/miui/server/enterprise/ApplicationManagerService;
.super Lcom/miui/enterprise/IApplicationManager$Stub;
.source "ApplicationManagerService.java"


# static fields
.field private static final ACTION_APP_RUNNING_BLOCK:Ljava/lang/String; = "com.miui.securitycore.APP_RUNNING_BLOCK"

.field private static final PACKAGE_SECURITY_CORE:Ljava/lang/String; = "com.miui.securitycore"

.field private static final TAG:Ljava/lang/String; = "Enterprise-App"


# instance fields
.field private mAMS:Lcom/android/server/am/ActivityManagerService;

.field private mAppOpsManager:Landroid/app/AppOpsManager;

.field private mContext:Landroid/content/Context;

.field private mDevicePolicyManager:Landroid/app/admin/IDevicePolicyManager;

.field private mDisAllowRunningHandleIntent:Landroid/content/Intent;

.field private mPMS:Lcom/android/server/pm/PackageManagerService$IPackageManagerImpl;

.field private mPMSI:Lcom/miui/server/process/ProcessManagerInternal;


# direct methods
.method static bridge synthetic -$$Nest$fgetmPMS(Lcom/miui/server/enterprise/ApplicationManagerService;)Lcom/android/server/pm/PackageManagerService$IPackageManagerImpl;
    .locals 0

    iget-object p0, p0, Lcom/miui/server/enterprise/ApplicationManagerService;->mPMS:Lcom/android/server/pm/PackageManagerService$IPackageManagerImpl;

    return-object p0
.end method

.method constructor <init>(Landroid/content/Context;)V
    .locals 2
    .param p1, "context"    # Landroid/content/Context;

    .line 59
    invoke-direct {p0}, Lcom/miui/enterprise/IApplicationManager$Stub;-><init>()V

    .line 60
    iput-object p1, p0, Lcom/miui/server/enterprise/ApplicationManagerService;->mContext:Landroid/content/Context;

    .line 61
    const-string v0, "package"

    invoke-static {v0}, Landroid/os/ServiceManager;->getService(Ljava/lang/String;)Landroid/os/IBinder;

    move-result-object v0

    check-cast v0, Lcom/android/server/pm/PackageManagerService$IPackageManagerImpl;

    iput-object v0, p0, Lcom/miui/server/enterprise/ApplicationManagerService;->mPMS:Lcom/android/server/pm/PackageManagerService$IPackageManagerImpl;

    .line 62
    const-string v0, "activity"

    invoke-static {v0}, Landroid/os/ServiceManager;->getService(Ljava/lang/String;)Landroid/os/IBinder;

    move-result-object v0

    check-cast v0, Lcom/android/server/am/ActivityManagerService;

    iput-object v0, p0, Lcom/miui/server/enterprise/ApplicationManagerService;->mAMS:Lcom/android/server/am/ActivityManagerService;

    .line 63
    const-class v0, Lcom/miui/server/process/ProcessManagerInternal;

    invoke-static {v0}, Lcom/android/server/LocalServices;->getService(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/miui/server/process/ProcessManagerInternal;

    iput-object v0, p0, Lcom/miui/server/enterprise/ApplicationManagerService;->mPMSI:Lcom/miui/server/process/ProcessManagerInternal;

    .line 64
    nop

    .line 65
    const-string v0, "device_policy"

    invoke-static {v0}, Landroid/os/ServiceManager;->getService(Ljava/lang/String;)Landroid/os/IBinder;

    move-result-object v0

    .line 64
    invoke-static {v0}, Landroid/app/admin/IDevicePolicyManager$Stub;->asInterface(Landroid/os/IBinder;)Landroid/app/admin/IDevicePolicyManager;

    move-result-object v0

    iput-object v0, p0, Lcom/miui/server/enterprise/ApplicationManagerService;->mDevicePolicyManager:Landroid/app/admin/IDevicePolicyManager;

    .line 66
    iget-object v0, p0, Lcom/miui/server/enterprise/ApplicationManagerService;->mContext:Landroid/content/Context;

    const-string v1, "appops"

    invoke-virtual {v0, v1}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/app/AppOpsManager;

    iput-object v0, p0, Lcom/miui/server/enterprise/ApplicationManagerService;->mAppOpsManager:Landroid/app/AppOpsManager;

    .line 67
    new-instance v0, Landroid/content/Intent;

    const-string v1, "com.miui.securitycore.APP_RUNNING_BLOCK"

    invoke-direct {v0, v1}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    iput-object v0, p0, Lcom/miui/server/enterprise/ApplicationManagerService;->mDisAllowRunningHandleIntent:Landroid/content/Intent;

    .line 68
    const-string v1, "com.miui.securitycore"

    invoke-virtual {v0, v1}, Landroid/content/Intent;->setPackage(Ljava/lang/String;)Landroid/content/Intent;

    .line 69
    iget-object v0, p0, Lcom/miui/server/enterprise/ApplicationManagerService;->mDisAllowRunningHandleIntent:Landroid/content/Intent;

    const/high16 v1, 0x10800000

    invoke-virtual {v0, v1}, Landroid/content/Intent;->setFlags(I)Landroid/content/Intent;

    .line 70
    return-void
.end method

.method private forceCloseTask(Ljava/util/List;I)V
    .locals 9
    .param p2, "userId"    # I
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List<",
            "Ljava/lang/String;",
            ">;I)V"
        }
    .end annotation

    .line 322
    .local p1, "packages":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;"
    iget-object v0, p0, Lcom/miui/server/enterprise/ApplicationManagerService;->mContext:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v0

    .line 323
    .local v0, "pm":Landroid/content/pm/PackageManager;
    iget-object v1, p0, Lcom/miui/server/enterprise/ApplicationManagerService;->mAMS:Lcom/android/server/am/ActivityManagerService;

    const/16 v2, 0x3e9

    const/4 v3, 0x0

    invoke-virtual {v1, v2, v3, p2}, Lcom/android/server/am/ActivityManagerService;->getRecentTasks(III)Landroid/content/pm/ParceledListSlice;

    move-result-object v1

    .line 324
    .local v1, "slice":Landroid/content/pm/ParceledListSlice;, "Landroid/content/pm/ParceledListSlice<Landroid/app/ActivityManager$RecentTaskInfo;>;"
    const/4 v2, 0x0

    .line 325
    .local v2, "tasks":Ljava/util/List;, "Ljava/util/List<Landroid/app/ActivityManager$RecentTaskInfo;>;"
    if-eqz v1, :cond_0

    .line 326
    invoke-virtual {v1}, Landroid/content/pm/ParceledListSlice;->getList()Ljava/util/List;

    move-result-object v2

    .line 328
    :cond_0
    if-eqz v2, :cond_4

    invoke-interface {v2}, Ljava/util/List;->isEmpty()Z

    move-result v3

    if-eqz v3, :cond_1

    goto :goto_1

    .line 331
    :cond_1
    invoke-interface {v2}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :goto_0
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v4

    if-eqz v4, :cond_3

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Landroid/app/ActivityManager$RecentTaskInfo;

    .line 332
    .local v4, "info":Landroid/app/ActivityManager$RecentTaskInfo;
    invoke-direct {p0, v0, v4}, Lcom/miui/server/enterprise/ApplicationManagerService;->getResolveInfoFromTask(Landroid/content/pm/PackageManager;Landroid/app/ActivityManager$RecentTaskInfo;)Landroid/content/pm/ResolveInfo;

    move-result-object v5

    .line 333
    .local v5, "ri":Landroid/content/pm/ResolveInfo;
    if-eqz v5, :cond_2

    iget-object v6, v5, Landroid/content/pm/ResolveInfo;->activityInfo:Landroid/content/pm/ActivityInfo;

    iget-object v6, v6, Landroid/content/pm/ActivityInfo;->packageName:Ljava/lang/String;

    if-eqz v6, :cond_2

    .line 334
    iget-object v6, v5, Landroid/content/pm/ResolveInfo;->activityInfo:Landroid/content/pm/ActivityInfo;

    iget-object v6, v6, Landroid/content/pm/ActivityInfo;->packageName:Ljava/lang/String;

    .line 335
    .local v6, "packageName":Ljava/lang/String;
    invoke-interface {p1, v6}, Ljava/util/List;->contains(Ljava/lang/Object;)Z

    move-result v7

    if-eqz v7, :cond_2

    .line 336
    iget-object v7, p0, Lcom/miui/server/enterprise/ApplicationManagerService;->mAMS:Lcom/android/server/am/ActivityManagerService;

    iget v8, v4, Landroid/app/ActivityManager$RecentTaskInfo;->persistentId:I

    invoke-virtual {v7, v8}, Lcom/android/server/am/ActivityManagerService;->removeTask(I)Z

    .line 339
    .end local v4    # "info":Landroid/app/ActivityManager$RecentTaskInfo;
    .end local v5    # "ri":Landroid/content/pm/ResolveInfo;
    .end local v6    # "packageName":Ljava/lang/String;
    :cond_2
    goto :goto_0

    .line 340
    :cond_3
    return-void

    .line 329
    :cond_4
    :goto_1
    return-void
.end method

.method private getAccessibilityServiceFromPackage(Ljava/lang/String;)Ljava/util/Set;
    .locals 8
    .param p1, "pkgName"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            ")",
            "Ljava/util/Set<",
            "Landroid/content/ComponentName;",
            ">;"
        }
    .end annotation

    .line 389
    new-instance v0, Ljava/util/HashSet;

    invoke-direct {v0}, Ljava/util/HashSet;-><init>()V

    .line 390
    .local v0, "services":Ljava/util/Set;, "Ljava/util/Set<Landroid/content/ComponentName;>;"
    new-instance v1, Landroid/content/Intent;

    const-string v2, "android.accessibilityservice.AccessibilityService"

    invoke-direct {v1, v2}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 391
    .local v1, "accessibilityIntent":Landroid/content/Intent;
    invoke-virtual {v1, p1}, Landroid/content/Intent;->setPackage(Ljava/lang/String;)Landroid/content/Intent;

    .line 392
    iget-object v2, p0, Lcom/miui/server/enterprise/ApplicationManagerService;->mContext:Landroid/content/Context;

    invoke-virtual {v2}, Landroid/content/Context;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v2

    const/4 v3, 0x4

    invoke-virtual {v2, v1, v3}, Landroid/content/pm/PackageManager;->queryIntentServices(Landroid/content/Intent;I)Ljava/util/List;

    move-result-object v2

    .line 393
    .local v2, "resolveInfos":Ljava/util/List;, "Ljava/util/List<Landroid/content/pm/ResolveInfo;>;"
    invoke-interface {v2}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :goto_0
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v4

    if-eqz v4, :cond_1

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Landroid/content/pm/ResolveInfo;

    .line 394
    .local v4, "resolveInfo":Landroid/content/pm/ResolveInfo;
    iget-object v5, v4, Landroid/content/pm/ResolveInfo;->serviceInfo:Landroid/content/pm/ServiceInfo;

    if-eqz v5, :cond_0

    .line 395
    new-instance v5, Landroid/content/ComponentName;

    iget-object v6, v4, Landroid/content/pm/ResolveInfo;->serviceInfo:Landroid/content/pm/ServiceInfo;

    iget-object v6, v6, Landroid/content/pm/ServiceInfo;->packageName:Ljava/lang/String;

    iget-object v7, v4, Landroid/content/pm/ResolveInfo;->serviceInfo:Landroid/content/pm/ServiceInfo;

    iget-object v7, v7, Landroid/content/pm/ServiceInfo;->name:Ljava/lang/String;

    invoke-direct {v5, v6, v7}, Landroid/content/ComponentName;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-interface {v0, v5}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    .line 397
    .end local v4    # "resolveInfo":Landroid/content/pm/ResolveInfo;
    :cond_0
    goto :goto_0

    .line 398
    :cond_1
    return-object v0
.end method

.method private getResolveInfoFromTask(Landroid/content/pm/PackageManager;Landroid/app/ActivityManager$RecentTaskInfo;)Landroid/content/pm/ResolveInfo;
    .locals 3
    .param p1, "packageManager"    # Landroid/content/pm/PackageManager;
    .param p2, "recentInfo"    # Landroid/app/ActivityManager$RecentTaskInfo;

    .line 343
    new-instance v0, Landroid/content/Intent;

    iget-object v1, p2, Landroid/app/ActivityManager$RecentTaskInfo;->baseIntent:Landroid/content/Intent;

    invoke-direct {v0, v1}, Landroid/content/Intent;-><init>(Landroid/content/Intent;)V

    .line 344
    .local v0, "intent":Landroid/content/Intent;
    iget-object v1, p2, Landroid/app/ActivityManager$RecentTaskInfo;->origActivity:Landroid/content/ComponentName;

    if-eqz v1, :cond_0

    .line 345
    iget-object v1, p2, Landroid/app/ActivityManager$RecentTaskInfo;->origActivity:Landroid/content/ComponentName;

    invoke-virtual {v0, v1}, Landroid/content/Intent;->setComponent(Landroid/content/ComponentName;)Landroid/content/Intent;

    .line 347
    :cond_0
    invoke-virtual {v0}, Landroid/content/Intent;->getFlags()I

    move-result v1

    const v2, -0x200001

    and-int/2addr v1, v2

    const/high16 v2, 0x10000000

    or-int/2addr v1, v2

    invoke-virtual {v0, v1}, Landroid/content/Intent;->setFlags(I)Landroid/content/Intent;

    .line 349
    const/4 v1, 0x0

    invoke-virtual {p1, v0, v1}, Landroid/content/pm/PackageManager;->resolveActivity(Landroid/content/Intent;I)Landroid/content/pm/ResolveInfo;

    move-result-object v1

    return-object v1
.end method

.method private readEnabeledAccessibilityService()Ljava/util/Set;
    .locals 5
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/Set<",
            "Landroid/content/ComponentName;",
            ">;"
        }
    .end annotation

    .line 402
    iget-object v0, p0, Lcom/miui/server/enterprise/ApplicationManagerService;->mContext:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    const-string v1, "enabled_accessibility_services"

    invoke-static {v0, v1}, Landroid/provider/Settings$Secure;->getString(Landroid/content/ContentResolver;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 404
    .local v0, "enabledServicesSetting":Ljava/lang/String;
    if-nez v0, :cond_0

    .line 405
    const/4 v1, 0x0

    return-object v1

    .line 407
    :cond_0
    new-instance v1, Ljava/util/HashSet;

    invoke-direct {v1}, Ljava/util/HashSet;-><init>()V

    .line 408
    .local v1, "enabledServices":Ljava/util/Set;, "Ljava/util/Set<Landroid/content/ComponentName;>;"
    new-instance v2, Landroid/text/TextUtils$SimpleStringSplitter;

    const/16 v3, 0x3a

    invoke-direct {v2, v3}, Landroid/text/TextUtils$SimpleStringSplitter;-><init>(C)V

    .line 409
    .local v2, "stringSplitter":Landroid/text/TextUtils$SimpleStringSplitter;
    invoke-virtual {v2, v0}, Landroid/text/TextUtils$SimpleStringSplitter;->setString(Ljava/lang/String;)V

    .line 411
    :goto_0
    invoke-virtual {v2}, Landroid/text/TextUtils$SimpleStringSplitter;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_2

    .line 412
    invoke-virtual {v2}, Landroid/text/TextUtils$SimpleStringSplitter;->next()Ljava/lang/String;

    move-result-object v3

    .line 413
    .local v3, "enabledServiceName":Ljava/lang/String;
    invoke-static {v3}, Landroid/content/ComponentName;->unflattenFromString(Ljava/lang/String;)Landroid/content/ComponentName;

    move-result-object v4

    .line 414
    .local v4, "enabledComponent":Landroid/content/ComponentName;
    if-eqz v4, :cond_1

    .line 415
    invoke-interface {v1, v4}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    .line 417
    .end local v3    # "enabledServiceName":Ljava/lang/String;
    .end local v4    # "enabledComponent":Landroid/content/ComponentName;
    :cond_1
    goto :goto_0

    .line 418
    :cond_2
    return-object v1
.end method

.method private restoreAppRunningControl(I)V
    .locals 3
    .param p1, "userId"    # I

    .line 78
    invoke-virtual {p0, p1}, Lcom/miui/server/enterprise/ApplicationManagerService;->getDisallowedRunningAppList(I)Ljava/util/List;

    move-result-object v0

    .line 79
    .local v0, "blackList":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;"
    if-eqz v0, :cond_1

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v1

    if-nez v1, :cond_0

    goto :goto_0

    .line 83
    :cond_0
    invoke-static {}, Lmiui/security/AppRunningControlManager;->getInstance()Lmiui/security/AppRunningControlManager;

    move-result-object v1

    const/4 v2, 0x1

    invoke-virtual {v1, v2}, Lmiui/security/AppRunningControlManager;->setBlackListEnable(Z)V

    .line 84
    invoke-static {}, Lmiui/security/AppRunningControlManager;->getInstance()Lmiui/security/AppRunningControlManager;

    move-result-object v1

    iget-object v2, p0, Lcom/miui/server/enterprise/ApplicationManagerService;->mDisAllowRunningHandleIntent:Landroid/content/Intent;

    invoke-virtual {v1, v0, v2}, Lmiui/security/AppRunningControlManager;->setDisallowRunningList(Ljava/util/List;Landroid/content/Intent;)V

    .line 85
    return-void

    .line 80
    :cond_1
    :goto_0
    invoke-static {}, Lmiui/security/AppRunningControlManager;->getInstance()Lmiui/security/AppRunningControlManager;

    move-result-object v1

    const/4 v2, 0x0

    invoke-virtual {v1, v2}, Lmiui/security/AppRunningControlManager;->setBlackListEnable(Z)V

    .line 81
    return-void
.end method


# virtual methods
.method public addTrustedAppStore(Ljava/util/List;I)V
    .locals 3
    .param p2, "userId"    # I
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List<",
            "Ljava/lang/String;",
            ">;I)V"
        }
    .end annotation

    .line 435
    .local p1, "packages":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;"
    iget-object v0, p0, Lcom/miui/server/enterprise/ApplicationManagerService;->mContext:Landroid/content/Context;

    invoke-static {v0}, Lcom/miui/server/enterprise/ServiceUtils;->checkPermission(Landroid/content/Context;)V

    .line 436
    iget-object v0, p0, Lcom/miui/server/enterprise/ApplicationManagerService;->mContext:Landroid/content/Context;

    .line 437
    invoke-static {p1}, Lcom/miui/enterprise/settings/EnterpriseSettings;->generateListSettings(Ljava/util/List;)Ljava/lang/String;

    move-result-object v1

    .line 436
    const-string v2, "ep_trusted_app_stores"

    invoke-static {v0, v2, v1, p2}, Lcom/miui/enterprise/settings/EnterpriseSettings;->putString(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;I)V

    .line 438
    return-void
.end method

.method public bootComplete()V
    .locals 2

    .line 73
    const-string v0, "Enterprise-App"

    const-string v1, "ApplicationManagerService init"

    invoke-static {v0, v1}, Landroid/util/Slog;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 74
    const/4 v0, 0x0

    invoke-direct {p0, v0}, Lcom/miui/server/enterprise/ApplicationManagerService;->restoreAppRunningControl(I)V

    .line 75
    return-void
.end method

.method public clearApplicationCache(Ljava/lang/String;I)V
    .locals 2
    .param p1, "packageName"    # Ljava/lang/String;
    .param p2, "userId"    # I

    .line 429
    iget-object v0, p0, Lcom/miui/server/enterprise/ApplicationManagerService;->mContext:Landroid/content/Context;

    invoke-static {v0}, Lcom/miui/server/enterprise/ServiceUtils;->checkPermission(Landroid/content/Context;)V

    .line 430
    iget-object v0, p0, Lcom/miui/server/enterprise/ApplicationManagerService;->mPMS:Lcom/android/server/pm/PackageManagerService$IPackageManagerImpl;

    const/4 v1, 0x0

    invoke-virtual {v0, p1, p2, v1}, Lcom/android/server/pm/PackageManagerService$IPackageManagerImpl;->deleteApplicationCacheFilesAsUser(Ljava/lang/String;ILandroid/content/pm/IPackageDataObserver;)V

    .line 431
    return-void
.end method

.method public clearApplicationUserData(Ljava/lang/String;I)V
    .locals 2
    .param p1, "packageName"    # Ljava/lang/String;
    .param p2, "userId"    # I

    .line 423
    iget-object v0, p0, Lcom/miui/server/enterprise/ApplicationManagerService;->mContext:Landroid/content/Context;

    invoke-static {v0}, Lcom/miui/server/enterprise/ServiceUtils;->checkPermission(Landroid/content/Context;)V

    .line 424
    iget-object v0, p0, Lcom/miui/server/enterprise/ApplicationManagerService;->mPMS:Lcom/android/server/pm/PackageManagerService$IPackageManagerImpl;

    const/4 v1, 0x0

    invoke-virtual {v0, p1, v1, p2}, Lcom/android/server/pm/PackageManagerService$IPackageManagerImpl;->clearApplicationUserData(Ljava/lang/String;Landroid/content/pm/IPackageDataObserver;I)V

    .line 425
    return-void
.end method

.method public deletePackage(Ljava/lang/String;ILcom/miui/enterprise/sdk/IEpDeletePackageObserver;I)V
    .locals 2
    .param p1, "packageName"    # Ljava/lang/String;
    .param p2, "flags"    # I
    .param p3, "observer"    # Lcom/miui/enterprise/sdk/IEpDeletePackageObserver;
    .param p4, "userId"    # I

    .line 95
    iget-object v0, p0, Lcom/miui/server/enterprise/ApplicationManagerService;->mContext:Landroid/content/Context;

    invoke-static {v0}, Lcom/miui/server/enterprise/ServiceUtils;->checkPermission(Landroid/content/Context;)V

    .line 96
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "delete package "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    const-string v1, "Enterprise-App"

    invoke-static {v1, v0}, Landroid/util/Slog;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 97
    new-instance v0, Lcom/miui/server/enterprise/ApplicationManagerService$1;

    invoke-direct {v0, p0, p3}, Lcom/miui/server/enterprise/ApplicationManagerService$1;-><init>(Lcom/miui/server/enterprise/ApplicationManagerService;Lcom/miui/enterprise/sdk/IEpDeletePackageObserver;)V

    .line 103
    .local v0, "deleteObserver":Landroid/content/pm/IPackageDeleteObserver;
    iget-object v1, p0, Lcom/miui/server/enterprise/ApplicationManagerService;->mContext:Landroid/content/Context;

    invoke-virtual {v1}, Landroid/content/Context;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v1

    invoke-virtual {v1, p1, v0, p2, p4}, Landroid/content/pm/PackageManager;->deletePackageAsUser(Ljava/lang/String;Landroid/content/pm/IPackageDeleteObserver;II)V

    .line 104
    return-void
.end method

.method public enableAccessibilityService(Landroid/content/ComponentName;Z)V
    .locals 7
    .param p1, "componentName"    # Landroid/content/ComponentName;
    .param p2, "enabled"    # Z

    .line 360
    iget-object v0, p0, Lcom/miui/server/enterprise/ApplicationManagerService;->mContext:Landroid/content/Context;

    invoke-static {v0}, Lcom/miui/server/enterprise/ServiceUtils;->checkPermission(Landroid/content/Context;)V

    .line 361
    invoke-virtual {p1}, Landroid/content/ComponentName;->getPackageName()Ljava/lang/String;

    move-result-object v0

    invoke-direct {p0, v0}, Lcom/miui/server/enterprise/ApplicationManagerService;->getAccessibilityServiceFromPackage(Ljava/lang/String;)Ljava/util/Set;

    move-result-object v0

    .line 362
    .local v0, "services":Ljava/util/Set;, "Ljava/util/Set<Landroid/content/ComponentName;>;"
    invoke-direct {p0}, Lcom/miui/server/enterprise/ApplicationManagerService;->readEnabeledAccessibilityService()Ljava/util/Set;

    move-result-object v1

    .line 363
    .local v1, "enabledServices":Ljava/util/Set;, "Ljava/util/Set<Landroid/content/ComponentName;>;"
    if-nez v1, :cond_0

    .line 364
    new-instance v2, Ljava/util/HashSet;

    invoke-direct {v2}, Ljava/util/HashSet;-><init>()V

    move-object v1, v2

    .line 366
    :cond_0
    if-eqz p2, :cond_1

    .line 367
    invoke-interface {v1, v0}, Ljava/util/Set;->addAll(Ljava/util/Collection;)Z

    goto :goto_0

    .line 369
    :cond_1
    invoke-interface {v1, v0}, Ljava/util/Set;->removeAll(Ljava/util/Collection;)Z

    .line 372
    :goto_0
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    .line 373
    .local v2, "enabledServicesBuilder":Ljava/lang/StringBuilder;
    invoke-interface {v1}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :goto_1
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v4

    if-eqz v4, :cond_2

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Landroid/content/ComponentName;

    .line 374
    .local v4, "enabledService":Landroid/content/ComponentName;
    invoke-virtual {v4}, Landroid/content/ComponentName;->flattenToString()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v2, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 375
    const/16 v5, 0x3a

    invoke-virtual {v2, v5}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    .line 376
    .end local v4    # "enabledService":Landroid/content/ComponentName;
    goto :goto_1

    .line 377
    :cond_2
    invoke-virtual {v2}, Ljava/lang/StringBuilder;->length()I

    move-result v3

    .line 378
    .local v3, "enabledServicesBuilderLength":I
    if-lez v3, :cond_3

    .line 379
    add-int/lit8 v4, v3, -0x1

    invoke-virtual {v2, v4}, Ljava/lang/StringBuilder;->deleteCharAt(I)Ljava/lang/StringBuilder;

    .line 381
    :cond_3
    iget-object v4, p0, Lcom/miui/server/enterprise/ApplicationManagerService;->mContext:Landroid/content/Context;

    invoke-virtual {v4}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v4

    .line 383
    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    .line 381
    const-string v6, "enabled_accessibility_services"

    invoke-static {v4, v6, v5}, Landroid/provider/Settings$Secure;->putString(Landroid/content/ContentResolver;Ljava/lang/String;Ljava/lang/String;)Z

    .line 384
    iget-object v4, p0, Lcom/miui/server/enterprise/ApplicationManagerService;->mContext:Landroid/content/Context;

    invoke-virtual {v4}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v4

    .line 385
    invoke-interface {v1}, Ljava/util/Set;->isEmpty()Z

    move-result v5

    .line 384
    xor-int/lit8 v5, v5, 0x1

    const-string v6, "accessibility_enabled"

    invoke-static {v4, v6, v5}, Landroid/provider/Settings$Secure;->putInt(Landroid/content/ContentResolver;Ljava/lang/String;I)Z

    .line 386
    return-void
.end method

.method public enableNotifications(Ljava/lang/String;Z)V
    .locals 1
    .param p1, "pkg"    # Ljava/lang/String;
    .param p2, "enabled"    # Z

    .line 471
    iget-object v0, p0, Lcom/miui/server/enterprise/ApplicationManagerService;->mContext:Landroid/content/Context;

    invoke-static {v0}, Lcom/miui/server/enterprise/ServiceUtils;->checkPermission(Landroid/content/Context;)V

    .line 472
    iget-object v0, p0, Lcom/miui/server/enterprise/ApplicationManagerService;->mContext:Landroid/content/Context;

    invoke-static {v0, p1, p2}, Lmiui/util/NotificationFilterHelper;->enableNotifications(Landroid/content/Context;Ljava/lang/String;Z)V

    .line 473
    return-void
.end method

.method public enableTrustedAppStore(ZI)V
    .locals 2
    .param p1, "enabled"    # Z
    .param p2, "userId"    # I

    .line 449
    iget-object v0, p0, Lcom/miui/server/enterprise/ApplicationManagerService;->mContext:Landroid/content/Context;

    invoke-static {v0}, Lcom/miui/server/enterprise/ServiceUtils;->checkPermission(Landroid/content/Context;)V

    .line 450
    iget-object v0, p0, Lcom/miui/server/enterprise/ApplicationManagerService;->mContext:Landroid/content/Context;

    .line 451
    nop

    .line 450
    const-string v1, "ep_trusted_app_store_enabled"

    invoke-static {v0, v1, p1, p2}, Lcom/miui/enterprise/settings/EnterpriseSettings;->putInt(Landroid/content/Context;Ljava/lang/String;II)V

    .line 452
    return-void
.end method

.method public getApplicationBlackList(I)Ljava/util/List;
    .locals 2
    .param p1, "userId"    # I
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(I)",
            "Ljava/util/List<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .line 256
    iget-object v0, p0, Lcom/miui/server/enterprise/ApplicationManagerService;->mContext:Landroid/content/Context;

    invoke-static {v0}, Lcom/miui/server/enterprise/ServiceUtils;->checkPermission(Landroid/content/Context;)V

    .line 257
    iget-object v0, p0, Lcom/miui/server/enterprise/ApplicationManagerService;->mContext:Landroid/content/Context;

    const-string v1, "ep_app_black_list"

    invoke-static {v0, v1, p1}, Lcom/miui/enterprise/settings/EnterpriseSettings;->getString(Landroid/content/Context;Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v0

    .line 259
    .local v0, "savedStr":Ljava/lang/String;
    invoke-static {v0}, Lcom/miui/enterprise/settings/EnterpriseSettings;->parseListSettings(Ljava/lang/String;)Ljava/util/List;

    move-result-object v1

    return-object v1
.end method

.method public getApplicationRestriction(I)I
    .locals 3
    .param p1, "userId"    # I

    .line 286
    iget-object v0, p0, Lcom/miui/server/enterprise/ApplicationManagerService;->mContext:Landroid/content/Context;

    invoke-static {v0}, Lcom/miui/server/enterprise/ServiceUtils;->checkPermission(Landroid/content/Context;)V

    .line 287
    iget-object v0, p0, Lcom/miui/server/enterprise/ApplicationManagerService;->mContext:Landroid/content/Context;

    const-string v1, "ep_app_restriction_mode"

    const/4 v2, 0x0

    invoke-static {v0, v1, v2, p1}, Lcom/miui/enterprise/settings/EnterpriseSettings;->getInt(Landroid/content/Context;Ljava/lang/String;II)I

    move-result v0

    return v0
.end method

.method public getApplicationSettings(Ljava/lang/String;I)I
    .locals 3
    .param p1, "packageName"    # Ljava/lang/String;
    .param p2, "userId"    # I

    .line 215
    iget-object v0, p0, Lcom/miui/server/enterprise/ApplicationManagerService;->mContext:Landroid/content/Context;

    invoke-static {v0}, Lcom/miui/server/enterprise/ServiceUtils;->checkPermission(Landroid/content/Context;)V

    .line 216
    iget-object v0, p0, Lcom/miui/server/enterprise/ApplicationManagerService;->mContext:Landroid/content/Context;

    invoke-static {p1}, Lcom/miui/enterprise/ApplicationHelper;->buildPackageSettingKey(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    const/4 v2, 0x0

    invoke-static {v0, v1, v2, p2}, Lcom/miui/enterprise/settings/EnterpriseSettings;->getInt(Landroid/content/Context;Ljava/lang/String;II)I

    move-result v0

    return v0
.end method

.method public getApplicationWhiteList(I)Ljava/util/List;
    .locals 2
    .param p1, "userId"    # I
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(I)",
            "Ljava/util/List<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .line 272
    iget-object v0, p0, Lcom/miui/server/enterprise/ApplicationManagerService;->mContext:Landroid/content/Context;

    invoke-static {v0}, Lcom/miui/server/enterprise/ServiceUtils;->checkPermission(Landroid/content/Context;)V

    .line 273
    iget-object v0, p0, Lcom/miui/server/enterprise/ApplicationManagerService;->mContext:Landroid/content/Context;

    const-string v1, "ep_app_white_list"

    invoke-static {v0, v1, p1}, Lcom/miui/enterprise/settings/EnterpriseSettings;->getString(Landroid/content/Context;Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v0

    .line 275
    .local v0, "savedStr":Ljava/lang/String;
    invoke-static {v0}, Lcom/miui/enterprise/settings/EnterpriseSettings;->parseListSettings(Ljava/lang/String;)Ljava/util/List;

    move-result-object v1

    return-object v1
.end method

.method public getDisallowedRunningAppList(I)Ljava/util/List;
    .locals 2
    .param p1, "userId"    # I
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(I)",
            "Ljava/util/List<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .line 316
    iget-object v0, p0, Lcom/miui/server/enterprise/ApplicationManagerService;->mContext:Landroid/content/Context;

    invoke-static {v0}, Lcom/miui/server/enterprise/ServiceUtils;->checkPermission(Landroid/content/Context;)V

    .line 317
    iget-object v0, p0, Lcom/miui/server/enterprise/ApplicationManagerService;->mContext:Landroid/content/Context;

    const-string v1, "ep_app_disallow_running_list"

    invoke-static {v0, v1, p1}, Lcom/miui/enterprise/settings/EnterpriseSettings;->getString(Landroid/content/Context;Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/miui/enterprise/settings/EnterpriseSettings;->parseListSettings(Ljava/lang/String;)Ljava/util/List;

    move-result-object v0

    return-object v0
.end method

.method public getTrustedAppStore(I)Ljava/util/List;
    .locals 2
    .param p1, "userId"    # I
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(I)",
            "Ljava/util/List<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .line 442
    iget-object v0, p0, Lcom/miui/server/enterprise/ApplicationManagerService;->mContext:Landroid/content/Context;

    invoke-static {v0}, Lcom/miui/server/enterprise/ServiceUtils;->checkPermission(Landroid/content/Context;)V

    .line 443
    iget-object v0, p0, Lcom/miui/server/enterprise/ApplicationManagerService;->mContext:Landroid/content/Context;

    .line 444
    const-string v1, "ep_trusted_app_stores"

    invoke-static {v0, v1, p1}, Lcom/miui/enterprise/settings/EnterpriseSettings;->getString(Landroid/content/Context;Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v0

    .line 443
    invoke-static {v0}, Lcom/miui/enterprise/settings/EnterpriseSettings;->parseListSettings(Ljava/lang/String;)Ljava/util/List;

    move-result-object v0

    return-object v0
.end method

.method public getXSpaceBlack()Ljava/util/List;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .line 504
    iget-object v0, p0, Lcom/miui/server/enterprise/ApplicationManagerService;->mContext:Landroid/content/Context;

    invoke-static {v0}, Lcom/miui/server/enterprise/ServiceUtils;->checkPermission(Landroid/content/Context;)V

    .line 505
    iget-object v0, p0, Lcom/miui/server/enterprise/ApplicationManagerService;->mContext:Landroid/content/Context;

    const-string v1, "ep_app_black_xsapce"

    invoke-static {v0, v1}, Lcom/miui/enterprise/settings/EnterpriseSettings;->getString(Landroid/content/Context;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 507
    .local v0, "savedStr":Ljava/lang/String;
    invoke-static {v0}, Lcom/miui/enterprise/settings/EnterpriseSettings;->parseListSettings(Ljava/lang/String;)Ljava/util/List;

    move-result-object v1

    return-object v1
.end method

.method public grantRuntimePermission(Ljava/lang/String;Ljava/lang/String;I)V
    .locals 4
    .param p1, "packageName"    # Ljava/lang/String;
    .param p2, "permission"    # Ljava/lang/String;
    .param p3, "userId"    # I

    .line 512
    iget-object v0, p0, Lcom/miui/server/enterprise/ApplicationManagerService;->mContext:Landroid/content/Context;

    invoke-static {v0}, Lcom/miui/server/enterprise/ServiceUtils;->checkPermission(Landroid/content/Context;)V

    .line 513
    iget-object v0, p0, Lcom/miui/server/enterprise/ApplicationManagerService;->mContext:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v0

    .line 514
    .local v0, "pm":Landroid/content/pm/PackageManager;
    const/4 v1, 0x1

    invoke-virtual {v0, p1, p2, v1}, Landroid/content/pm/PackageManager;->addWhitelistedRestrictedPermission(Ljava/lang/String;Ljava/lang/String;I)Z

    move-result v1

    .line 516
    .local v1, "grantPermission":Z
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "add restricted permission white list:"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, ",success?="

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    const-string v3, "Enterprise-App"

    invoke-static {v3, v2}, Landroid/util/Slog;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 517
    iget-object v2, p0, Lcom/miui/server/enterprise/ApplicationManagerService;->mContext:Landroid/content/Context;

    invoke-virtual {v2}, Landroid/content/Context;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v2

    sget-object v3, Landroid/os/UserHandle;->OWNER:Landroid/os/UserHandle;

    invoke-virtual {v2, p1, p2, v3}, Landroid/content/pm/PackageManager;->grantRuntimePermission(Ljava/lang/String;Ljava/lang/String;Landroid/os/UserHandle;)V

    .line 518
    return-void
.end method

.method public installPackage(Ljava/lang/String;ILcom/miui/enterprise/sdk/IEpInstallPackageObserver;I)V
    .locals 9
    .param p1, "path"    # Ljava/lang/String;
    .param p2, "flags"    # I
    .param p3, "observer"    # Lcom/miui/enterprise/sdk/IEpInstallPackageObserver;
    .param p4, "userId"    # I

    .line 89
    iget-object v0, p0, Lcom/miui/server/enterprise/ApplicationManagerService;->mContext:Landroid/content/Context;

    invoke-static {v0}, Lcom/miui/server/enterprise/ServiceUtils;->checkPermission(Landroid/content/Context;)V

    .line 90
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "install package "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    const-string v1, "Enterprise-App"

    invoke-static {v1, v0}, Landroid/util/Slog;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 91
    iget-object v2, p0, Lcom/miui/server/enterprise/ApplicationManagerService;->mContext:Landroid/content/Context;

    iget-object v3, p0, Lcom/miui/server/enterprise/ApplicationManagerService;->mPMS:Lcom/android/server/pm/PackageManagerService$IPackageManagerImpl;

    const-string v7, "Enterprise"

    move-object v4, p1

    move-object v5, p3

    move v6, p2

    move v8, p4

    invoke-static/range {v2 .. v8}, Lcom/miui/server/enterprise/ApplicationManagerServiceProxy;->installPackageAsUser(Landroid/content/Context;Lcom/android/server/pm/PackageManagerService$IPackageManagerImpl;Ljava/lang/String;Lcom/miui/enterprise/sdk/IEpInstallPackageObserver;ILjava/lang/String;I)V

    .line 92
    return-void
.end method

.method public installPackageWithPendingIntent(Ljava/lang/String;Landroid/app/PendingIntent;I)V
    .locals 8
    .param p1, "path"    # Ljava/lang/String;
    .param p2, "pendingIntent"    # Landroid/app/PendingIntent;
    .param p3, "userId"    # I

    .line 108
    iget-object v0, p0, Lcom/miui/server/enterprise/ApplicationManagerService;->mContext:Landroid/content/Context;

    invoke-static {v0}, Lcom/miui/server/enterprise/ServiceUtils;->checkPermission(Landroid/content/Context;)V

    .line 109
    iget-object v1, p0, Lcom/miui/server/enterprise/ApplicationManagerService;->mContext:Landroid/content/Context;

    iget-object v2, p0, Lcom/miui/server/enterprise/ApplicationManagerService;->mPMS:Lcom/android/server/pm/PackageManagerService$IPackageManagerImpl;

    new-instance v4, Lcom/miui/server/enterprise/ApplicationManagerService$2;

    invoke-direct {v4, p0, p3, p2}, Lcom/miui/server/enterprise/ApplicationManagerService$2;-><init>(Lcom/miui/server/enterprise/ApplicationManagerService;ILandroid/app/PendingIntent;)V

    const/4 v5, 0x2

    const-string v6, "Enterprise"

    move-object v3, p1

    move v7, p3

    invoke-static/range {v1 .. v7}, Lcom/miui/server/enterprise/ApplicationManagerServiceProxy;->installPackageAsUser(Landroid/content/Context;Lcom/android/server/pm/PackageManagerService$IPackageManagerImpl;Ljava/lang/String;Lcom/miui/enterprise/sdk/IEpInstallPackageObserver;ILjava/lang/String;I)V

    .line 149
    return-void
.end method

.method public isTrustedAppStoreEnabled(I)Z
    .locals 3
    .param p1, "userId"    # I

    .line 456
    iget-object v0, p0, Lcom/miui/server/enterprise/ApplicationManagerService;->mContext:Landroid/content/Context;

    invoke-static {v0}, Lcom/miui/server/enterprise/ServiceUtils;->checkPermission(Landroid/content/Context;)V

    .line 457
    iget-object v0, p0, Lcom/miui/server/enterprise/ApplicationManagerService;->mContext:Landroid/content/Context;

    const-string v1, "ep_trusted_app_store_enabled"

    const/4 v2, 0x0

    invoke-static {v0, v1, v2, p1}, Lcom/miui/enterprise/settings/EnterpriseSettings;->getInt(Landroid/content/Context;Ljava/lang/String;II)I

    move-result v0

    const/4 v1, 0x1

    if-ne v0, v1, :cond_0

    move v2, v1

    :cond_0
    return v2
.end method

.method public killProcess(Ljava/lang/String;I)V
    .locals 2
    .param p1, "packageName"    # Ljava/lang/String;
    .param p2, "userId"    # I

    .line 354
    iget-object v0, p0, Lcom/miui/server/enterprise/ApplicationManagerService;->mContext:Landroid/content/Context;

    invoke-static {v0}, Lcom/miui/server/enterprise/ServiceUtils;->checkPermission(Landroid/content/Context;)V

    .line 355
    iget-object v0, p0, Lcom/miui/server/enterprise/ApplicationManagerService;->mPMSI:Lcom/miui/server/process/ProcessManagerInternal;

    const-string v1, "enterprise"

    invoke-virtual {v0, p1, p2, v1}, Lcom/miui/server/process/ProcessManagerInternal;->forceStopPackage(Ljava/lang/String;ILjava/lang/String;)V

    .line 356
    return-void
.end method

.method public removeDeviceAdmin(Landroid/content/ComponentName;I)Z
    .locals 4
    .param p1, "component"    # Landroid/content/ComponentName;
    .param p2, "userId"    # I

    .line 235
    const-string v0, "Remove device admin["

    const-string v1, "Enterprise-App"

    iget-object v2, p0, Lcom/miui/server/enterprise/ApplicationManagerService;->mContext:Landroid/content/Context;

    invoke-static {v2}, Lcom/miui/server/enterprise/ServiceUtils;->checkPermission(Landroid/content/Context;)V

    .line 237
    :try_start_0
    iget-object v2, p0, Lcom/miui/server/enterprise/ApplicationManagerService;->mDevicePolicyManager:Landroid/app/admin/IDevicePolicyManager;

    invoke-interface {v2, p1, p2}, Landroid/app/admin/IDevicePolicyManager;->removeActiveAdmin(Landroid/content/ComponentName;I)V

    .line 238
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, "]"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Slog;->d(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 239
    const/4 v0, 0x1

    return v0

    .line 240
    :catch_0
    move-exception v2

    .line 241
    .local v2, "e":Ljava/lang/Exception;
    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v3, "] failed"

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v1, v0, v2}, Landroid/util/Slog;->d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    .line 242
    const/4 v0, 0x0

    return v0
.end method

.method public setApplicationBlackList(Ljava/util/List;I)V
    .locals 3
    .param p2, "userId"    # I
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List<",
            "Ljava/lang/String;",
            ">;I)V"
        }
    .end annotation

    .line 248
    .local p1, "packages":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;"
    iget-object v0, p0, Lcom/miui/server/enterprise/ApplicationManagerService;->mContext:Landroid/content/Context;

    invoke-static {v0}, Lcom/miui/server/enterprise/ServiceUtils;->checkPermission(Landroid/content/Context;)V

    .line 249
    iget-object v0, p0, Lcom/miui/server/enterprise/ApplicationManagerService;->mContext:Landroid/content/Context;

    .line 251
    invoke-static {p1}, Lcom/miui/enterprise/settings/EnterpriseSettings;->generateListSettings(Ljava/util/List;)Ljava/lang/String;

    move-result-object v1

    .line 249
    const-string v2, "ep_app_black_list"

    invoke-static {v0, v2, v1, p2}, Lcom/miui/enterprise/settings/EnterpriseSettings;->putString(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;I)V

    .line 252
    return-void
.end method

.method public setApplicationEnabled(Ljava/lang/String;ZI)V
    .locals 7
    .param p1, "packageName"    # Ljava/lang/String;
    .param p2, "enable"    # Z
    .param p3, "userId"    # I

    .line 463
    iget-object v0, p0, Lcom/miui/server/enterprise/ApplicationManagerService;->mContext:Landroid/content/Context;

    invoke-static {v0}, Lcom/miui/server/enterprise/ServiceUtils;->checkPermission(Landroid/content/Context;)V

    .line 464
    iget-object v1, p0, Lcom/miui/server/enterprise/ApplicationManagerService;->mPMS:Lcom/android/server/pm/PackageManagerService$IPackageManagerImpl;

    .line 465
    if-eqz p2, :cond_0

    const/4 v0, 0x0

    move v3, v0

    goto :goto_0

    .line 466
    :cond_0
    const/4 v0, 0x2

    move v3, v0

    :goto_0
    const/4 v4, 0x0

    const-string v6, "Enterprise"

    .line 464
    move-object v2, p1

    move v5, p3

    invoke-virtual/range {v1 .. v6}, Lcom/android/server/pm/PackageManagerService$IPackageManagerImpl;->setApplicationEnabledSetting(Ljava/lang/String;IIILjava/lang/String;)V

    .line 467
    return-void
.end method

.method public setApplicationRestriction(II)V
    .locals 2
    .param p1, "mode"    # I
    .param p2, "userId"    # I

    .line 280
    iget-object v0, p0, Lcom/miui/server/enterprise/ApplicationManagerService;->mContext:Landroid/content/Context;

    invoke-static {v0}, Lcom/miui/server/enterprise/ServiceUtils;->checkPermission(Landroid/content/Context;)V

    .line 281
    iget-object v0, p0, Lcom/miui/server/enterprise/ApplicationManagerService;->mContext:Landroid/content/Context;

    const-string v1, "ep_app_restriction_mode"

    invoke-static {v0, v1, p1, p2}, Lcom/miui/enterprise/settings/EnterpriseSettings;->putInt(Landroid/content/Context;Ljava/lang/String;II)V

    .line 282
    return-void
.end method

.method public setApplicationSettings(Ljava/lang/String;II)V
    .locals 16
    .param p1, "packageName"    # Ljava/lang/String;
    .param p2, "flags"    # I
    .param p3, "userId"    # I

    .line 153
    move-object/from16 v1, p0

    move-object/from16 v2, p1

    move/from16 v3, p2

    move/from16 v4, p3

    iget-object v0, v1, Lcom/miui/server/enterprise/ApplicationManagerService;->mContext:Landroid/content/Context;

    invoke-static {v0}, Lcom/miui/server/enterprise/ServiceUtils;->checkPermission(Landroid/content/Context;)V

    .line 154
    invoke-static/range {p1 .. p1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    const-string v5, "Enterprise-App"

    if-nez v0, :cond_7

    if-gez v3, :cond_0

    goto/16 :goto_5

    .line 158
    :cond_0
    iget-object v0, v1, Lcom/miui/server/enterprise/ApplicationManagerService;->mContext:Landroid/content/Context;

    invoke-static/range {p1 .. p1}, Lcom/miui/enterprise/ApplicationHelper;->buildPackageSettingKey(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v6

    invoke-static {v0, v6, v3, v4}, Lcom/miui/enterprise/settings/EnterpriseSettings;->putInt(Landroid/content/Context;Ljava/lang/String;II)V

    .line 160
    and-int/lit8 v0, v3, 0x8

    const/4 v6, 0x3

    const-string v7, "6"

    const-string v8, "content://com.lbe.security.miui.permmgr"

    const-string v9, "extra_package"

    const-string v10, "extra_action"

    const-wide/16 v11, 0x4000

    const-string v13, "extra_permission"

    const/4 v14, 0x0

    if-eqz v0, :cond_1

    .line 161
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v15, "allowed "

    invoke-virtual {v0, v15}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v15, " auto start"

    invoke-virtual {v0, v15}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v5, v0}, Landroid/util/Slog;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 162
    new-instance v0, Landroid/os/Bundle;

    invoke-direct {v0}, Landroid/os/Bundle;-><init>()V

    .line 163
    .local v0, "extras":Landroid/os/Bundle;
    invoke-virtual {v0, v13, v11, v12}, Landroid/os/Bundle;->putLong(Ljava/lang/String;J)V

    .line 164
    invoke-virtual {v0, v10, v6}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    .line 165
    filled-new-array/range {p1 .. p1}, [Ljava/lang/String;

    move-result-object v10

    invoke-virtual {v0, v9, v10}, Landroid/os/Bundle;->putStringArray(Ljava/lang/String;[Ljava/lang/String;)V

    .line 166
    iget-object v9, v1, Lcom/miui/server/enterprise/ApplicationManagerService;->mContext:Landroid/content/Context;

    invoke-virtual {v9}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v9

    invoke-static {v8}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v8

    invoke-virtual {v9, v8, v7, v14, v0}, Landroid/content/ContentResolver;->call(Landroid/net/Uri;Ljava/lang/String;Ljava/lang/String;Landroid/os/Bundle;)Landroid/os/Bundle;

    .line 168
    .end local v0    # "extras":Landroid/os/Bundle;
    const/4 v11, 0x0

    goto :goto_0

    .line 169
    :cond_1
    new-instance v0, Landroid/os/Bundle;

    invoke-direct {v0}, Landroid/os/Bundle;-><init>()V

    .line 170
    .restart local v0    # "extras":Landroid/os/Bundle;
    invoke-virtual {v0, v13, v11, v12}, Landroid/os/Bundle;->putLong(Ljava/lang/String;J)V

    .line 171
    const/4 v11, 0x0

    invoke-virtual {v0, v10, v11}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    .line 172
    filled-new-array/range {p1 .. p1}, [Ljava/lang/String;

    move-result-object v10

    invoke-virtual {v0, v9, v10}, Landroid/os/Bundle;->putStringArray(Ljava/lang/String;[Ljava/lang/String;)V

    .line 173
    iget-object v9, v1, Lcom/miui/server/enterprise/ApplicationManagerService;->mContext:Landroid/content/Context;

    invoke-virtual {v9}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v9

    invoke-static {v8}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v8

    invoke-virtual {v9, v8, v7, v14, v0}, Landroid/content/ContentResolver;->call(Landroid/net/Uri;Ljava/lang/String;Ljava/lang/String;Landroid/os/Bundle;)Landroid/os/Bundle;

    .line 177
    .end local v0    # "extras":Landroid/os/Bundle;
    :goto_0
    new-instance v0, Landroid/content/Intent;

    .line 178
    const-string v7, "package"

    invoke-static {v7, v2, v14}, Landroid/net/Uri;->fromParts(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v7

    const-string v8, "android.intent.action.PACKAGE_ADDED"

    invoke-direct {v0, v8, v7}, Landroid/content/Intent;-><init>(Ljava/lang/String;Landroid/net/Uri;)V

    move-object v7, v0

    .line 179
    .local v7, "intent":Landroid/content/Intent;
    const-string v0, "android.intent.extra.user_handle"

    invoke-virtual {v7, v0, v4}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    .line 180
    const-string v0, "com.lbe.security.miui"

    invoke-virtual {v7, v0}, Landroid/content/Intent;->setPackage(Ljava/lang/String;)Landroid/content/Intent;

    .line 181
    const/high16 v0, 0x10000000

    invoke-virtual {v7, v0}, Landroid/content/Intent;->addFlags(I)Landroid/content/Intent;

    .line 182
    iget-object v0, v1, Lcom/miui/server/enterprise/ApplicationManagerService;->mContext:Landroid/content/Context;

    new-instance v8, Landroid/os/UserHandle;

    invoke-direct {v8, v4}, Landroid/os/UserHandle;-><init>(I)V

    invoke-virtual {v0, v7, v8}, Landroid/content/Context;->sendBroadcastAsUser(Landroid/content/Intent;Landroid/os/UserHandle;)V

    .line 183
    iget-object v0, v1, Lcom/miui/server/enterprise/ApplicationManagerService;->mPMS:Lcom/android/server/pm/PackageManagerService$IPackageManagerImpl;

    const-wide/16 v8, 0x0

    invoke-virtual {v0, v2, v8, v9, v4}, Lcom/android/server/pm/PackageManagerService$IPackageManagerImpl;->getApplicationInfo(Ljava/lang/String;JI)Landroid/content/pm/ApplicationInfo;

    move-result-object v8

    .line 184
    .local v8, "info":Landroid/content/pm/ApplicationInfo;
    and-int/lit8 v0, v3, 0x10

    const/4 v9, 0x1

    if-eqz v0, :cond_2

    move v0, v9

    goto :goto_1

    :cond_2
    move v0, v11

    :goto_1
    move v10, v0

    .line 186
    .local v10, "shouldGrantPermission":Z
    if-eqz v8, :cond_4

    .line 187
    if-eqz v10, :cond_3

    move v6, v11

    :cond_3
    move v0, v6

    .line 188
    .local v0, "opsMode":I
    iget-object v6, v1, Lcom/miui/server/enterprise/ApplicationManagerService;->mAppOpsManager:Landroid/app/AppOpsManager;

    const/16 v12, 0x2b

    iget v13, v8, Landroid/content/pm/ApplicationInfo;->uid:I

    invoke-virtual {v6, v12, v13, v2, v0}, Landroid/app/AppOpsManager;->setMode(IILjava/lang/String;I)V

    .line 190
    iget-object v6, v1, Lcom/miui/server/enterprise/ApplicationManagerService;->mAppOpsManager:Landroid/app/AppOpsManager;

    const/16 v12, 0x2726

    iget v13, v8, Landroid/content/pm/ApplicationInfo;->uid:I

    invoke-virtual {v6, v12, v13, v2, v0}, Landroid/app/AppOpsManager;->setMode(IILjava/lang/String;I)V

    .line 193
    .end local v0    # "opsMode":I
    :cond_4
    and-int/lit8 v0, v3, 0x1

    if-eqz v0, :cond_5

    move v15, v9

    goto :goto_2

    :cond_5
    move v15, v11

    :goto_2
    move v6, v15

    .line 195
    .local v6, "isKeepAlive":Z
    new-instance v0, Landroid/os/Bundle;

    invoke-direct {v0}, Landroid/os/Bundle;-><init>()V

    move-object v9, v0

    .line 196
    .local v9, "bundle":Landroid/os/Bundle;
    const-string/jumbo v0, "userId"

    invoke-virtual {v9, v0, v4}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    .line 197
    const-string v0, "pkgName"

    invoke-virtual {v9, v0, v2}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 198
    if-eqz v6, :cond_6

    const-string v0, "noRestrict"

    goto :goto_3

    :cond_6
    const-string v0, "miuiAuto"

    :goto_3
    const-string v11, "bgControl"

    invoke-virtual {v9, v11, v0}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 200
    :try_start_0
    iget-object v0, v1, Lcom/miui/server/enterprise/ApplicationManagerService;->mContext:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    const-string v11, "content://com.miui.powerkeeper.configure"

    .line 201
    invoke-static {v11}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v11

    const-string/jumbo v12, "userTable"

    .line 200
    invoke-static {v11, v12}, Landroid/net/Uri;->withAppendedPath(Landroid/net/Uri;Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v11

    const-string/jumbo v12, "userTableupdate"

    invoke-virtual {v0, v11, v12, v14, v9}, Landroid/content/ContentResolver;->call(Landroid/net/Uri;Ljava/lang/String;Ljava/lang/String;Landroid/os/Bundle;)Landroid/os/Bundle;
    :try_end_0
    .catch Ljava/lang/IllegalArgumentException; {:try_start_0 .. :try_end_0} :catch_0

    .line 206
    goto :goto_4

    .line 204
    :catch_0
    move-exception v0

    .line 205
    .local v0, "e":Ljava/lang/IllegalArgumentException;
    new-instance v11, Ljava/lang/StringBuilder;

    invoke-direct {v11}, Ljava/lang/StringBuilder;-><init>()V

    const-string v12, "Failed to process powerkeeper config for pkg "

    invoke-virtual {v11, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v11

    invoke-virtual {v11, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v11

    invoke-virtual {v11}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v11

    invoke-static {v5, v11, v0}, Landroid/util/Slog;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    .line 207
    .end local v0    # "e":Ljava/lang/IllegalArgumentException;
    :goto_4
    invoke-static {v2, v4, v6}, Lmiui/process/ProcessManager;->updateApplicationLockedState(Ljava/lang/String;IZ)V

    .line 209
    iget-object v0, v1, Lcom/miui/server/enterprise/ApplicationManagerService;->mPMSI:Lcom/miui/server/process/ProcessManagerInternal;

    invoke-virtual {v0, v2, v6}, Lcom/miui/server/process/ProcessManagerInternal;->updateEnterpriseWhiteList(Ljava/lang/String;Z)V

    .line 211
    return-void

    .line 155
    .end local v6    # "isKeepAlive":Z
    .end local v7    # "intent":Landroid/content/Intent;
    .end local v8    # "info":Landroid/content/pm/ApplicationInfo;
    .end local v9    # "bundle":Landroid/os/Bundle;
    .end local v10    # "shouldGrantPermission":Z
    :cond_7
    :goto_5
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "Invalidate param packageName:"

    invoke-virtual {v0, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v6, ", flags:"

    invoke-virtual {v0, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v5, v0}, Landroid/util/Slog;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 156
    return-void
.end method

.method public setApplicationWhiteList(Ljava/util/List;I)V
    .locals 3
    .param p2, "userId"    # I
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List<",
            "Ljava/lang/String;",
            ">;I)V"
        }
    .end annotation

    .line 264
    .local p1, "packages":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;"
    iget-object v0, p0, Lcom/miui/server/enterprise/ApplicationManagerService;->mContext:Landroid/content/Context;

    invoke-static {v0}, Lcom/miui/server/enterprise/ServiceUtils;->checkPermission(Landroid/content/Context;)V

    .line 265
    iget-object v0, p0, Lcom/miui/server/enterprise/ApplicationManagerService;->mContext:Landroid/content/Context;

    .line 267
    invoke-static {p1}, Lcom/miui/enterprise/settings/EnterpriseSettings;->generateListSettings(Ljava/util/List;)Ljava/lang/String;

    move-result-object v1

    .line 265
    const-string v2, "ep_app_white_list"

    invoke-static {v0, v2, v1, p2}, Lcom/miui/enterprise/settings/EnterpriseSettings;->putString(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;I)V

    .line 268
    return-void
.end method

.method public setDeviceAdmin(Landroid/content/ComponentName;I)Z
    .locals 5
    .param p1, "component"    # Landroid/content/ComponentName;
    .param p2, "userId"    # I

    .line 222
    const-string v0, "Add device admin["

    const-string v1, "Enterprise-App"

    iget-object v2, p0, Lcom/miui/server/enterprise/ApplicationManagerService;->mContext:Landroid/content/Context;

    invoke-static {v2}, Lcom/miui/server/enterprise/ServiceUtils;->checkPermission(Landroid/content/Context;)V

    .line 224
    :try_start_0
    iget-object v2, p0, Lcom/miui/server/enterprise/ApplicationManagerService;->mDevicePolicyManager:Landroid/app/admin/IDevicePolicyManager;

    const/4 v3, 0x1

    invoke-interface {v2, p1, v3, p2}, Landroid/app/admin/IDevicePolicyManager;->setActiveAdmin(Landroid/content/ComponentName;ZI)V

    .line 225
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v4, "]"

    invoke-virtual {v2, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Slog;->d(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    .line 226
    return v3

    .line 227
    :catch_0
    move-exception v2

    .line 228
    .local v2, "e":Landroid/os/RemoteException;
    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v3, "] failed"

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v1, v0, v2}, Landroid/util/Slog;->d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    .line 229
    const/4 v0, 0x0

    return v0
.end method

.method public setDisallowedRunningAppList(Ljava/util/List;I)V
    .locals 5
    .param p2, "userId"    # I
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List<",
            "Ljava/lang/String;",
            ">;I)V"
        }
    .end annotation

    .line 293
    .local p1, "packages":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;"
    iget-object v0, p0, Lcom/miui/server/enterprise/ApplicationManagerService;->mContext:Landroid/content/Context;

    invoke-static {v0}, Lcom/miui/server/enterprise/ServiceUtils;->checkPermission(Landroid/content/Context;)V

    .line 294
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    .line 295
    .local v0, "sb":Ljava/lang/StringBuilder;
    if-nez p1, :cond_0

    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    move-object p1, v1

    .line 296
    :cond_0
    invoke-direct {p0, p1, p2}, Lcom/miui/server/enterprise/ApplicationManagerService;->forceCloseTask(Ljava/util/List;I)V

    .line 297
    invoke-interface {p1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_1

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/lang/String;

    .line 298
    .local v2, "pkg":Ljava/lang/String;
    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, ";"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 299
    iget-object v3, p0, Lcom/miui/server/enterprise/ApplicationManagerService;->mPMSI:Lcom/miui/server/process/ProcessManagerInternal;

    const-string v4, "enterprise disallow running"

    invoke-virtual {v3, v2, p2, v4}, Lcom/miui/server/process/ProcessManagerInternal;->forceStopPackage(Ljava/lang/String;ILjava/lang/String;)V

    .line 300
    .end local v2    # "pkg":Ljava/lang/String;
    goto :goto_0

    .line 301
    :cond_1
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->length()I

    move-result v1

    const/4 v2, 0x1

    if-lez v1, :cond_2

    .line 302
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->length()I

    move-result v1

    sub-int/2addr v1, v2

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->deleteCharAt(I)Ljava/lang/StringBuilder;

    .line 304
    :cond_2
    iget-object v1, p0, Lcom/miui/server/enterprise/ApplicationManagerService;->mContext:Landroid/content/Context;

    .line 305
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    .line 304
    const-string v4, "ep_app_disallow_running_list"

    invoke-static {v1, v4, v3, p2}, Lcom/miui/enterprise/settings/EnterpriseSettings;->putString(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;I)V

    .line 306
    invoke-interface {p1}, Ljava/util/List;->isEmpty()Z

    move-result v1

    if-eqz v1, :cond_3

    .line 307
    invoke-static {}, Lmiui/security/AppRunningControlManager;->getInstance()Lmiui/security/AppRunningControlManager;

    move-result-object v1

    const/4 v2, 0x0

    invoke-virtual {v1, v2}, Lmiui/security/AppRunningControlManager;->setBlackListEnable(Z)V

    goto :goto_1

    .line 309
    :cond_3
    invoke-static {}, Lmiui/security/AppRunningControlManager;->getInstance()Lmiui/security/AppRunningControlManager;

    move-result-object v1

    iget-object v3, p0, Lcom/miui/server/enterprise/ApplicationManagerService;->mDisAllowRunningHandleIntent:Landroid/content/Intent;

    invoke-virtual {v1, p1, v3}, Lmiui/security/AppRunningControlManager;->setDisallowRunningList(Ljava/util/List;Landroid/content/Intent;)V

    .line 310
    invoke-static {}, Lmiui/security/AppRunningControlManager;->getInstance()Lmiui/security/AppRunningControlManager;

    move-result-object v1

    invoke-virtual {v1, v2}, Lmiui/security/AppRunningControlManager;->setBlackListEnable(Z)V

    .line 312
    :goto_1
    return-void
.end method

.method public setNotificaitonFilter(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Z)V
    .locals 2
    .param p1, "pkg"    # Ljava/lang/String;
    .param p2, "channelId"    # Ljava/lang/String;
    .param p3, "type"    # Ljava/lang/String;
    .param p4, "allow"    # Z

    .line 477
    iget-object v0, p0, Lcom/miui/server/enterprise/ApplicationManagerService;->mContext:Landroid/content/Context;

    invoke-static {v0}, Lcom/miui/server/enterprise/ServiceUtils;->checkPermission(Landroid/content/Context;)V

    .line 478
    invoke-static {p2}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    const-string v1, "float"

    if-eqz v0, :cond_1

    .line 479
    invoke-virtual {v1, p3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 480
    iget-object v0, p0, Lcom/miui/server/enterprise/ApplicationManagerService;->mContext:Landroid/content/Context;

    invoke-static {v0, p1, p4}, Lmiui/util/NotificationFilterHelper;->enableStatusIcon(Landroid/content/Context;Ljava/lang/String;Z)V

    goto :goto_0

    .line 482
    :cond_0
    iget-object v0, p0, Lcom/miui/server/enterprise/ApplicationManagerService;->mContext:Landroid/content/Context;

    invoke-static {v0, p1, p3, p4}, Lmiui/util/NotificationFilterHelper;->setAllow(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;Z)V

    goto :goto_0

    .line 485
    :cond_1
    invoke-virtual {v1, p3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 486
    iget-object v0, p0, Lcom/miui/server/enterprise/ApplicationManagerService;->mContext:Landroid/content/Context;

    invoke-static {v0, p1, p2, p4}, Lmiui/util/NotificationFilterHelper;->enableStatusIcon(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;Z)V

    goto :goto_0

    .line 488
    :cond_2
    iget-object v0, p0, Lcom/miui/server/enterprise/ApplicationManagerService;->mContext:Landroid/content/Context;

    invoke-static {v0, p1, p2, p3, p4}, Lmiui/util/NotificationFilterHelper;->setAllow(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Z)V

    .line 491
    :goto_0
    return-void
.end method

.method public setXSpaceBlack(Ljava/util/List;)V
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List<",
            "Ljava/lang/String;",
            ">;)V"
        }
    .end annotation

    .line 495
    .local p1, "packages":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;"
    iget-object v0, p0, Lcom/miui/server/enterprise/ApplicationManagerService;->mContext:Landroid/content/Context;

    invoke-static {v0}, Lcom/miui/server/enterprise/ServiceUtils;->checkPermission(Landroid/content/Context;)V

    .line 496
    iget-object v0, p0, Lcom/miui/server/enterprise/ApplicationManagerService;->mContext:Landroid/content/Context;

    .line 498
    invoke-static {p1}, Lcom/miui/enterprise/settings/EnterpriseSettings;->generateListSettings(Ljava/util/List;)Ljava/lang/String;

    move-result-object v1

    .line 496
    const-string v2, "ep_app_black_xsapce"

    invoke-static {v0, v2, v1}, Lcom/miui/enterprise/settings/EnterpriseSettings;->putString(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;)V

    .line 500
    return-void
.end method
