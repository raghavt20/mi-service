.class public Lcom/miui/server/enterprise/PhoneManagerService;
.super Lcom/miui/enterprise/IPhoneManager$Stub;
.source "PhoneManagerService.java"


# static fields
.field private static final TAG:Ljava/lang/String; = "Enterprise-phone"


# instance fields
.field private mContext:Landroid/content/Context;


# direct methods
.method constructor <init>(Landroid/content/Context;)V
    .locals 0
    .param p1, "context"    # Landroid/content/Context;

    .line 20
    invoke-direct {p0}, Lcom/miui/enterprise/IPhoneManager$Stub;-><init>()V

    .line 21
    iput-object p1, p0, Lcom/miui/server/enterprise/PhoneManagerService;->mContext:Landroid/content/Context;

    .line 22
    return-void
.end method

.method private shouldClose(I)Z
    .locals 1
    .param p1, "state"    # I

    .line 53
    if-eqz p1, :cond_1

    const/4 v0, 0x3

    if-ne p1, v0, :cond_0

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    goto :goto_1

    :cond_1
    :goto_0
    const/4 v0, 0x1

    :goto_1
    return v0
.end method

.method private shouldOpen(I)Z
    .locals 1
    .param p1, "state"    # I

    .line 49
    const/4 v0, 0x2

    if-eq p1, v0, :cond_1

    const/4 v0, 0x4

    if-ne p1, v0, :cond_0

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    goto :goto_1

    :cond_1
    :goto_0
    const/4 v0, 0x1

    :goto_1
    return v0
.end method


# virtual methods
.method public controlCellular(II)V
    .locals 2
    .param p1, "flag"    # I
    .param p2, "userId"    # I

    .line 38
    iget-object v0, p0, Lcom/miui/server/enterprise/PhoneManagerService;->mContext:Landroid/content/Context;

    invoke-static {v0}, Lcom/miui/server/enterprise/ServiceUtils;->checkPermission(Landroid/content/Context;)V

    .line 39
    iget-object v0, p0, Lcom/miui/server/enterprise/PhoneManagerService;->mContext:Landroid/content/Context;

    const-string v1, "ep_cellular_status"

    invoke-static {v0, v1, p1, p2}, Lcom/miui/enterprise/settings/EnterpriseSettings;->putInt(Landroid/content/Context;Ljava/lang/String;II)V

    .line 40
    invoke-direct {p0, p1}, Lcom/miui/server/enterprise/PhoneManagerService;->shouldOpen(I)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 41
    iget-object v0, p0, Lcom/miui/server/enterprise/PhoneManagerService;->mContext:Landroid/content/Context;

    invoke-static {v0}, Landroid/telephony/TelephonyManager;->from(Landroid/content/Context;)Landroid/telephony/TelephonyManager;

    move-result-object v0

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Landroid/telephony/TelephonyManager;->setDataEnabled(Z)V

    .line 43
    :cond_0
    invoke-direct {p0, p1}, Lcom/miui/server/enterprise/PhoneManagerService;->shouldClose(I)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 44
    iget-object v0, p0, Lcom/miui/server/enterprise/PhoneManagerService;->mContext:Landroid/content/Context;

    invoke-static {v0}, Landroid/telephony/TelephonyManager;->from(Landroid/content/Context;)Landroid/telephony/TelephonyManager;

    move-result-object v0

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/telephony/TelephonyManager;->setDataEnabled(Z)V

    .line 46
    :cond_1
    return-void
.end method

.method public controlPhoneCall(II)V
    .locals 2
    .param p1, "flags"    # I
    .param p2, "userId"    # I

    .line 32
    iget-object v0, p0, Lcom/miui/server/enterprise/PhoneManagerService;->mContext:Landroid/content/Context;

    invoke-static {v0}, Lcom/miui/server/enterprise/ServiceUtils;->checkPermission(Landroid/content/Context;)V

    .line 33
    iget-object v0, p0, Lcom/miui/server/enterprise/PhoneManagerService;->mContext:Landroid/content/Context;

    const-string v1, "ep_phone_call_status"

    invoke-static {v0, v1, p1, p2}, Lcom/miui/enterprise/settings/EnterpriseSettings;->putInt(Landroid/content/Context;Ljava/lang/String;II)V

    .line 34
    return-void
.end method

.method public controlSMS(II)V
    .locals 2
    .param p1, "flags"    # I
    .param p2, "userId"    # I

    .line 26
    iget-object v0, p0, Lcom/miui/server/enterprise/PhoneManagerService;->mContext:Landroid/content/Context;

    invoke-static {v0}, Lcom/miui/server/enterprise/ServiceUtils;->checkPermission(Landroid/content/Context;)V

    .line 27
    iget-object v0, p0, Lcom/miui/server/enterprise/PhoneManagerService;->mContext:Landroid/content/Context;

    const-string v1, "ep_sms_status"

    invoke-static {v0, v1, p1, p2}, Lcom/miui/enterprise/settings/EnterpriseSettings;->putInt(Landroid/content/Context;Ljava/lang/String;II)V

    .line 28
    return-void
.end method

.method public disableCallForward(Z)V
    .locals 2
    .param p1, "disabled"    # Z

    .line 193
    iget-object v0, p0, Lcom/miui/server/enterprise/PhoneManagerService;->mContext:Landroid/content/Context;

    invoke-static {v0}, Lcom/miui/server/enterprise/ServiceUtils;->checkPermission(Landroid/content/Context;)V

    .line 194
    iget-object v0, p0, Lcom/miui/server/enterprise/PhoneManagerService;->mContext:Landroid/content/Context;

    const-string v1, "ep_disable_call_forward"

    invoke-static {v0, v1, p1}, Lcom/miui/enterprise/settings/EnterpriseSettings;->putInt(Landroid/content/Context;Ljava/lang/String;I)V

    .line 195
    return-void
.end method

.method public disableCallLog(Z)V
    .locals 2
    .param p1, "disable"    # Z

    .line 199
    iget-object v0, p0, Lcom/miui/server/enterprise/PhoneManagerService;->mContext:Landroid/content/Context;

    invoke-static {v0}, Lcom/miui/server/enterprise/ServiceUtils;->checkPermission(Landroid/content/Context;)V

    .line 200
    iget-object v0, p0, Lcom/miui/server/enterprise/PhoneManagerService;->mContext:Landroid/content/Context;

    const-string v1, "ep_disable_call_log"

    invoke-static {v0, v1, p1}, Lcom/miui/enterprise/settings/EnterpriseSettings;->putInt(Landroid/content/Context;Ljava/lang/String;I)V

    .line 201
    return-void
.end method

.method public endCall()V
    .locals 2

    .line 185
    iget-object v0, p0, Lcom/miui/server/enterprise/PhoneManagerService;->mContext:Landroid/content/Context;

    invoke-static {v0}, Lcom/miui/server/enterprise/ServiceUtils;->checkPermission(Landroid/content/Context;)V

    .line 186
    const-string v0, "Enterprise-phone"

    const-string v1, "End current call"

    invoke-static {v0, v1}, Landroid/util/Slog;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 187
    invoke-static {}, Lmiui/telephony/TelephonyManagerEx;->getDefault()Lmiui/telephony/TelephonyManagerEx;

    move-result-object v0

    invoke-virtual {v0}, Lmiui/telephony/TelephonyManagerEx;->endCall()Z

    .line 188
    new-instance v0, Ljava/lang/RuntimeException;

    const-string v1, "Not implemented"

    invoke-direct {v0, v1}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    throw v0
.end method

.method public getAreaCode(Ljava/lang/String;)Ljava/lang/String;
    .locals 2
    .param p1, "phoneNumber"    # Ljava/lang/String;

    .line 205
    iget-object v0, p0, Lcom/miui/server/enterprise/PhoneManagerService;->mContext:Landroid/content/Context;

    invoke-static {v0}, Lcom/miui/server/enterprise/ServiceUtils;->checkPermission(Landroid/content/Context;)V

    .line 206
    invoke-static {p1}, Lmiui/telephony/PhoneNumberUtils$PhoneNumber;->parse(Ljava/lang/CharSequence;)Lmiui/telephony/PhoneNumberUtils$PhoneNumber;

    move-result-object v0

    iget-object v1, p0, Lcom/miui/server/enterprise/PhoneManagerService;->mContext:Landroid/content/Context;

    invoke-virtual {v0, v1}, Lmiui/telephony/PhoneNumberUtils$PhoneNumber;->getLocationAreaCode(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public getCallBlackList(I)Ljava/util/List;
    .locals 2
    .param p1, "userId"    # I
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(I)",
            "Ljava/util/List<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .line 159
    iget-object v0, p0, Lcom/miui/server/enterprise/PhoneManagerService;->mContext:Landroid/content/Context;

    invoke-static {v0}, Lcom/miui/server/enterprise/ServiceUtils;->checkPermission(Landroid/content/Context;)V

    .line 160
    iget-object v0, p0, Lcom/miui/server/enterprise/PhoneManagerService;->mContext:Landroid/content/Context;

    .line 161
    const-string v1, "ep_call_black_list"

    invoke-static {v0, v1, p1}, Lcom/miui/enterprise/settings/EnterpriseSettings;->getString(Landroid/content/Context;Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v0

    .line 160
    invoke-static {v0}, Lcom/miui/enterprise/settings/EnterpriseSettings;->parseListSettings(Ljava/lang/String;)Ljava/util/List;

    move-result-object v0

    return-object v0
.end method

.method public getCallContactRestriction(I)I
    .locals 3
    .param p1, "userId"    # I

    .line 179
    iget-object v0, p0, Lcom/miui/server/enterprise/PhoneManagerService;->mContext:Landroid/content/Context;

    invoke-static {v0}, Lcom/miui/server/enterprise/ServiceUtils;->checkPermission(Landroid/content/Context;)V

    .line 180
    iget-object v0, p0, Lcom/miui/server/enterprise/PhoneManagerService;->mContext:Landroid/content/Context;

    const-string v1, "ep_call_restriction_mode"

    const/4 v2, 0x0

    invoke-static {v0, v1, v2, p1}, Lcom/miui/enterprise/settings/EnterpriseSettings;->getInt(Landroid/content/Context;Ljava/lang/String;II)I

    move-result v0

    return v0
.end method

.method public getCallWhiteList(I)Ljava/util/List;
    .locals 2
    .param p1, "userId"    # I
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(I)",
            "Ljava/util/List<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .line 166
    iget-object v0, p0, Lcom/miui/server/enterprise/PhoneManagerService;->mContext:Landroid/content/Context;

    invoke-static {v0}, Lcom/miui/server/enterprise/ServiceUtils;->checkPermission(Landroid/content/Context;)V

    .line 167
    iget-object v0, p0, Lcom/miui/server/enterprise/PhoneManagerService;->mContext:Landroid/content/Context;

    .line 168
    const-string v1, "ep_call_white_list"

    invoke-static {v0, v1, p1}, Lcom/miui/enterprise/settings/EnterpriseSettings;->getString(Landroid/content/Context;Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v0

    .line 167
    invoke-static {v0}, Lcom/miui/enterprise/settings/EnterpriseSettings;->parseListSettings(Ljava/lang/String;)Ljava/util/List;

    move-result-object v0

    return-object v0
.end method

.method public getCellularStatus(I)I
    .locals 3
    .param p1, "userId"    # I

    .line 72
    iget-object v0, p0, Lcom/miui/server/enterprise/PhoneManagerService;->mContext:Landroid/content/Context;

    invoke-static {v0}, Lcom/miui/server/enterprise/ServiceUtils;->checkPermission(Landroid/content/Context;)V

    .line 73
    iget-object v0, p0, Lcom/miui/server/enterprise/PhoneManagerService;->mContext:Landroid/content/Context;

    const-string v1, "ep_cellular_status"

    const/4 v2, 0x0

    invoke-static {v0, v1, v2, p1}, Lcom/miui/enterprise/settings/EnterpriseSettings;->getInt(Landroid/content/Context;Ljava/lang/String;II)I

    move-result v0

    return v0
.end method

.method public getIMEI(I)Ljava/lang/String;
    .locals 1
    .param p1, "slotId"    # I

    .line 79
    iget-object v0, p0, Lcom/miui/server/enterprise/PhoneManagerService;->mContext:Landroid/content/Context;

    invoke-static {v0}, Lcom/miui/server/enterprise/ServiceUtils;->checkPermission(Landroid/content/Context;)V

    .line 80
    invoke-static {}, Lmiui/telephony/TelephonyManager;->getDefault()Lmiui/telephony/TelephonyManager;

    move-result-object v0

    invoke-virtual {v0, p1}, Lmiui/telephony/TelephonyManager;->getImeiForSlot(I)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public getMeid(I)Ljava/lang/String;
    .locals 1
    .param p1, "slotId"    # I

    .line 211
    iget-object v0, p0, Lcom/miui/server/enterprise/PhoneManagerService;->mContext:Landroid/content/Context;

    invoke-static {v0}, Lcom/miui/server/enterprise/ServiceUtils;->checkPermission(Landroid/content/Context;)V

    .line 212
    invoke-static {}, Lmiui/telephony/TelephonyManager;->getDefault()Lmiui/telephony/TelephonyManager;

    move-result-object v0

    invoke-virtual {v0, p1}, Lmiui/telephony/TelephonyManager;->getMeidForSlot(I)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public getPhoneCallStatus(I)I
    .locals 3
    .param p1, "userId"    # I

    .line 65
    iget-object v0, p0, Lcom/miui/server/enterprise/PhoneManagerService;->mContext:Landroid/content/Context;

    invoke-static {v0}, Lcom/miui/server/enterprise/ServiceUtils;->checkPermission(Landroid/content/Context;)V

    .line 66
    iget-object v0, p0, Lcom/miui/server/enterprise/PhoneManagerService;->mContext:Landroid/content/Context;

    const-string v1, "ep_phone_call_status"

    const/4 v2, 0x0

    invoke-static {v0, v1, v2, p1}, Lcom/miui/enterprise/settings/EnterpriseSettings;->getInt(Landroid/content/Context;Ljava/lang/String;II)I

    move-result v0

    return v0
.end method

.method public getSMSBlackList(I)Ljava/util/List;
    .locals 2
    .param p1, "userId"    # I
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(I)",
            "Ljava/util/List<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .line 119
    iget-object v0, p0, Lcom/miui/server/enterprise/PhoneManagerService;->mContext:Landroid/content/Context;

    invoke-static {v0}, Lcom/miui/server/enterprise/ServiceUtils;->checkPermission(Landroid/content/Context;)V

    .line 120
    iget-object v0, p0, Lcom/miui/server/enterprise/PhoneManagerService;->mContext:Landroid/content/Context;

    .line 121
    const-string v1, "ep_sms_black_list"

    invoke-static {v0, v1, p1}, Lcom/miui/enterprise/settings/EnterpriseSettings;->getString(Landroid/content/Context;Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v0

    .line 120
    invoke-static {v0}, Lcom/miui/enterprise/settings/EnterpriseSettings;->parseListSettings(Ljava/lang/String;)Ljava/util/List;

    move-result-object v0

    return-object v0
.end method

.method public getSMSContactRestriction(I)I
    .locals 3
    .param p1, "userId"    # I

    .line 139
    iget-object v0, p0, Lcom/miui/server/enterprise/PhoneManagerService;->mContext:Landroid/content/Context;

    invoke-static {v0}, Lcom/miui/server/enterprise/ServiceUtils;->checkPermission(Landroid/content/Context;)V

    .line 140
    iget-object v0, p0, Lcom/miui/server/enterprise/PhoneManagerService;->mContext:Landroid/content/Context;

    const-string v1, "ep_sms_restriction_mode"

    const/4 v2, 0x0

    invoke-static {v0, v1, v2, p1}, Lcom/miui/enterprise/settings/EnterpriseSettings;->getInt(Landroid/content/Context;Ljava/lang/String;II)I

    move-result v0

    return v0
.end method

.method public getSMSStatus(I)I
    .locals 3
    .param p1, "userId"    # I

    .line 58
    iget-object v0, p0, Lcom/miui/server/enterprise/PhoneManagerService;->mContext:Landroid/content/Context;

    invoke-static {v0}, Lcom/miui/server/enterprise/ServiceUtils;->checkPermission(Landroid/content/Context;)V

    .line 59
    iget-object v0, p0, Lcom/miui/server/enterprise/PhoneManagerService;->mContext:Landroid/content/Context;

    const-string v1, "ep_sms_status"

    const/4 v2, 0x0

    invoke-static {v0, v1, v2, p1}, Lcom/miui/enterprise/settings/EnterpriseSettings;->getInt(Landroid/content/Context;Ljava/lang/String;II)I

    move-result v0

    return v0
.end method

.method public getSMSWhiteList(I)Ljava/util/List;
    .locals 2
    .param p1, "userId"    # I
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(I)",
            "Ljava/util/List<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .line 126
    iget-object v0, p0, Lcom/miui/server/enterprise/PhoneManagerService;->mContext:Landroid/content/Context;

    invoke-static {v0}, Lcom/miui/server/enterprise/ServiceUtils;->checkPermission(Landroid/content/Context;)V

    .line 127
    iget-object v0, p0, Lcom/miui/server/enterprise/PhoneManagerService;->mContext:Landroid/content/Context;

    .line 128
    const-string v1, "ep_sms_white_list"

    invoke-static {v0, v1, p1}, Lcom/miui/enterprise/settings/EnterpriseSettings;->getString(Landroid/content/Context;Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v0

    .line 127
    invoke-static {v0}, Lcom/miui/enterprise/settings/EnterpriseSettings;->parseListSettings(Ljava/lang/String;)Ljava/util/List;

    move-result-object v0

    return-object v0
.end method

.method public isAutoRecordPhoneCall(I)Z
    .locals 3
    .param p1, "userId"    # I

    .line 99
    iget-object v0, p0, Lcom/miui/server/enterprise/PhoneManagerService;->mContext:Landroid/content/Context;

    invoke-static {v0}, Lcom/miui/server/enterprise/ServiceUtils;->checkPermission(Landroid/content/Context;)V

    .line 100
    iget-object v0, p0, Lcom/miui/server/enterprise/PhoneManagerService;->mContext:Landroid/content/Context;

    const-string v1, "ep_force_auto_call_record"

    const/4 v2, 0x0

    invoke-static {v0, v1, v2, p1}, Lcom/miui/enterprise/settings/EnterpriseSettings;->getInt(Landroid/content/Context;Ljava/lang/String;II)I

    move-result v0

    const/4 v1, 0x1

    if-ne v0, v1, :cond_0

    move v2, v1

    :cond_0
    return v2
.end method

.method public setCallBlackList(Ljava/util/List;I)V
    .locals 3
    .param p2, "userId"    # I
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List<",
            "Ljava/lang/String;",
            ">;I)V"
        }
    .end annotation

    .line 145
    .local p1, "list":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;"
    iget-object v0, p0, Lcom/miui/server/enterprise/PhoneManagerService;->mContext:Landroid/content/Context;

    invoke-static {v0}, Lcom/miui/server/enterprise/ServiceUtils;->checkPermission(Landroid/content/Context;)V

    .line 146
    iget-object v0, p0, Lcom/miui/server/enterprise/PhoneManagerService;->mContext:Landroid/content/Context;

    .line 147
    invoke-static {p1}, Lcom/miui/enterprise/settings/EnterpriseSettings;->generateListSettings(Ljava/util/List;)Ljava/lang/String;

    move-result-object v1

    .line 146
    const-string v2, "ep_call_black_list"

    invoke-static {v0, v2, v1, p2}, Lcom/miui/enterprise/settings/EnterpriseSettings;->putString(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;I)V

    .line 148
    return-void
.end method

.method public setCallContactRestriction(II)V
    .locals 2
    .param p1, "mode"    # I
    .param p2, "userId"    # I

    .line 173
    iget-object v0, p0, Lcom/miui/server/enterprise/PhoneManagerService;->mContext:Landroid/content/Context;

    invoke-static {v0}, Lcom/miui/server/enterprise/ServiceUtils;->checkPermission(Landroid/content/Context;)V

    .line 174
    iget-object v0, p0, Lcom/miui/server/enterprise/PhoneManagerService;->mContext:Landroid/content/Context;

    const-string v1, "ep_call_restriction_mode"

    invoke-static {v0, v1, p1, p2}, Lcom/miui/enterprise/settings/EnterpriseSettings;->putInt(Landroid/content/Context;Ljava/lang/String;II)V

    .line 175
    return-void
.end method

.method public setCallWhiteList(Ljava/util/List;I)V
    .locals 3
    .param p2, "userId"    # I
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List<",
            "Ljava/lang/String;",
            ">;I)V"
        }
    .end annotation

    .line 152
    .local p1, "list":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;"
    iget-object v0, p0, Lcom/miui/server/enterprise/PhoneManagerService;->mContext:Landroid/content/Context;

    invoke-static {v0}, Lcom/miui/server/enterprise/ServiceUtils;->checkPermission(Landroid/content/Context;)V

    .line 153
    iget-object v0, p0, Lcom/miui/server/enterprise/PhoneManagerService;->mContext:Landroid/content/Context;

    .line 154
    invoke-static {p1}, Lcom/miui/enterprise/settings/EnterpriseSettings;->generateListSettings(Ljava/util/List;)Ljava/lang/String;

    move-result-object v1

    .line 153
    const-string v2, "ep_call_white_list"

    invoke-static {v0, v2, v1, p2}, Lcom/miui/enterprise/settings/EnterpriseSettings;->putString(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;I)V

    .line 155
    return-void
.end method

.method public setIccCardActivate(IZ)V
    .locals 4
    .param p1, "slotId"    # I
    .param p2, "isActive"    # Z

    .line 217
    iget-object v0, p0, Lcom/miui/server/enterprise/PhoneManagerService;->mContext:Landroid/content/Context;

    invoke-static {v0}, Lcom/miui/server/enterprise/ServiceUtils;->checkPermission(Landroid/content/Context;)V

    .line 218
    const/4 v0, 0x1

    if-eqz p1, :cond_1

    if-ne p1, v0, :cond_0

    goto :goto_0

    .line 219
    :cond_0
    new-instance v0, Ljava/lang/IllegalArgumentException;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Invalid slotId value "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ". Must be either PhoneManagerMode.SLOT_ID_1 or PhoneManagerMode.SLOT_ID_2."

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 222
    :cond_1
    :goto_0
    invoke-static {}, Lmiui/telephony/TelephonyManager;->getDefault()Lmiui/telephony/TelephonyManager;

    move-result-object v1

    .line 223
    .local v1, "telephonyManager":Lmiui/telephony/TelephonyManager;
    invoke-virtual {v1, p1}, Lmiui/telephony/TelephonyManager;->getSimStateForSlot(I)I

    move-result v2

    .line 224
    .local v2, "simState":I
    if-ne v2, v0, :cond_2

    .line 225
    return-void

    .line 227
    :cond_2
    if-nez p1, :cond_3

    .line 228
    xor-int/lit8 v0, p2, 0x1

    const-string v3, "ep_icc_card_1_disable"

    invoke-static {v3, v0}, Lcom/miui/enterprise/settings/EnterpriseSettings;->putInt(Ljava/lang/String;I)V

    goto :goto_1

    .line 230
    :cond_3
    xor-int/lit8 v0, p2, 0x1

    const-string v3, "ep_icc_card_2_disable"

    invoke-static {v3, v0}, Lcom/miui/enterprise/settings/EnterpriseSettings;->putInt(Ljava/lang/String;I)V

    .line 232
    :goto_1
    invoke-virtual {v1, p1, p2}, Lmiui/telephony/TelephonyManager;->setIccCardActivate(IZ)V

    .line 233
    return-void
.end method

.method public setPhoneCallAutoRecord(ZI)V
    .locals 2
    .param p1, "isAutoRecord"    # Z
    .param p2, "userId"    # I

    .line 85
    iget-object v0, p0, Lcom/miui/server/enterprise/PhoneManagerService;->mContext:Landroid/content/Context;

    invoke-static {v0}, Lcom/miui/server/enterprise/ServiceUtils;->checkPermission(Landroid/content/Context;)V

    .line 86
    iget-object v0, p0, Lcom/miui/server/enterprise/PhoneManagerService;->mContext:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    const-string v1, "button_auto_record_call"

    invoke-static {v0, v1, p1, p2}, Landroid/provider/MiuiSettings$System;->putBooleanForUser(Landroid/content/ContentResolver;Ljava/lang/String;ZI)Z

    .line 88
    iget-object v0, p0, Lcom/miui/server/enterprise/PhoneManagerService;->mContext:Landroid/content/Context;

    const-string v1, "ep_force_auto_call_record"

    invoke-static {v0, v1, p1, p2}, Lcom/miui/enterprise/settings/EnterpriseSettings;->putInt(Landroid/content/Context;Ljava/lang/String;II)V

    .line 89
    return-void
.end method

.method public setPhoneCallAutoRecordDir(Ljava/lang/String;)V
    .locals 2
    .param p1, "dir"    # Ljava/lang/String;

    .line 93
    iget-object v0, p0, Lcom/miui/server/enterprise/PhoneManagerService;->mContext:Landroid/content/Context;

    invoke-static {v0}, Lcom/miui/server/enterprise/ServiceUtils;->checkPermission(Landroid/content/Context;)V

    .line 94
    iget-object v0, p0, Lcom/miui/server/enterprise/PhoneManagerService;->mContext:Landroid/content/Context;

    const-string v1, "ep_force_auto_call_record_dir"

    invoke-static {v0, v1, p1}, Lcom/miui/enterprise/settings/EnterpriseSettings;->putString(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;)V

    .line 95
    return-void
.end method

.method public setSMSBlackList(Ljava/util/List;I)V
    .locals 3
    .param p2, "userId"    # I
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List<",
            "Ljava/lang/String;",
            ">;I)V"
        }
    .end annotation

    .line 105
    .local p1, "list":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;"
    iget-object v0, p0, Lcom/miui/server/enterprise/PhoneManagerService;->mContext:Landroid/content/Context;

    invoke-static {v0}, Lcom/miui/server/enterprise/ServiceUtils;->checkPermission(Landroid/content/Context;)V

    .line 106
    iget-object v0, p0, Lcom/miui/server/enterprise/PhoneManagerService;->mContext:Landroid/content/Context;

    .line 107
    invoke-static {p1}, Lcom/miui/enterprise/settings/EnterpriseSettings;->generateListSettings(Ljava/util/List;)Ljava/lang/String;

    move-result-object v1

    .line 106
    const-string v2, "ep_sms_black_list"

    invoke-static {v0, v2, v1, p2}, Lcom/miui/enterprise/settings/EnterpriseSettings;->putString(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;I)V

    .line 108
    return-void
.end method

.method public setSMSContactRestriction(II)V
    .locals 2
    .param p1, "mode"    # I
    .param p2, "userId"    # I

    .line 133
    iget-object v0, p0, Lcom/miui/server/enterprise/PhoneManagerService;->mContext:Landroid/content/Context;

    invoke-static {v0}, Lcom/miui/server/enterprise/ServiceUtils;->checkPermission(Landroid/content/Context;)V

    .line 134
    iget-object v0, p0, Lcom/miui/server/enterprise/PhoneManagerService;->mContext:Landroid/content/Context;

    const-string v1, "ep_sms_restriction_mode"

    invoke-static {v0, v1, p1, p2}, Lcom/miui/enterprise/settings/EnterpriseSettings;->putInt(Landroid/content/Context;Ljava/lang/String;II)V

    .line 135
    return-void
.end method

.method public setSMSWhiteList(Ljava/util/List;I)V
    .locals 3
    .param p2, "userId"    # I
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List<",
            "Ljava/lang/String;",
            ">;I)V"
        }
    .end annotation

    .line 112
    .local p1, "list":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;"
    iget-object v0, p0, Lcom/miui/server/enterprise/PhoneManagerService;->mContext:Landroid/content/Context;

    invoke-static {v0}, Lcom/miui/server/enterprise/ServiceUtils;->checkPermission(Landroid/content/Context;)V

    .line 113
    iget-object v0, p0, Lcom/miui/server/enterprise/PhoneManagerService;->mContext:Landroid/content/Context;

    .line 114
    invoke-static {p1}, Lcom/miui/enterprise/settings/EnterpriseSettings;->generateListSettings(Ljava/util/List;)Ljava/lang/String;

    move-result-object v1

    .line 113
    const-string v2, "ep_sms_white_list"

    invoke-static {v0, v2, v1, p2}, Lcom/miui/enterprise/settings/EnterpriseSettings;->putString(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;I)V

    .line 115
    return-void
.end method
