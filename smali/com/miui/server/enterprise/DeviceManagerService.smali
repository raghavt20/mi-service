.class public Lcom/miui/server/enterprise/DeviceManagerService;
.super Lcom/miui/enterprise/IDeviceManager$Stub;
.source "DeviceManagerService.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/miui/server/enterprise/DeviceManagerService$ServiceHandler;
    }
.end annotation


# static fields
.field private static final PERSIST_ANIMATION_PATH:Ljava/lang/String; = "/data/system/theme_magic/enterprise/"

.field private static final TAG:Ljava/lang/String; = "Enterprise-device"


# instance fields
.field private mContext:Landroid/content/Context;

.field private mDisplayManager:Landroid/hardware/display/DisplayManager;

.field private mServiceHandler:Lcom/miui/server/enterprise/DeviceManagerService$ServiceHandler;

.field private mWifiManager:Landroid/net/wifi/WifiManager;


# direct methods
.method static bridge synthetic -$$Nest$fgetmWifiManager(Lcom/miui/server/enterprise/DeviceManagerService;)Landroid/net/wifi/WifiManager;
    .locals 0

    iget-object p0, p0, Lcom/miui/server/enterprise/DeviceManagerService;->mWifiManager:Landroid/net/wifi/WifiManager;

    return-object p0
.end method

.method constructor <init>(Landroid/content/Context;)V
    .locals 3
    .param p1, "context"    # Landroid/content/Context;

    .line 69
    invoke-direct {p0}, Lcom/miui/enterprise/IDeviceManager$Stub;-><init>()V

    .line 70
    iput-object p1, p0, Lcom/miui/server/enterprise/DeviceManagerService;->mContext:Landroid/content/Context;

    .line 71
    const-string/jumbo v0, "wifi"

    invoke-virtual {p1, v0}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/net/wifi/WifiManager;

    iput-object v0, p0, Lcom/miui/server/enterprise/DeviceManagerService;->mWifiManager:Landroid/net/wifi/WifiManager;

    .line 72
    iget-object v0, p0, Lcom/miui/server/enterprise/DeviceManagerService;->mContext:Landroid/content/Context;

    .line 73
    const-string v1, "display"

    invoke-virtual {v0, v1}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/hardware/display/DisplayManager;

    iput-object v0, p0, Lcom/miui/server/enterprise/DeviceManagerService;->mDisplayManager:Landroid/hardware/display/DisplayManager;

    .line 74
    new-instance v0, Landroid/os/HandlerThread;

    const-string v1, "DeviceManagerService"

    invoke-direct {v0, v1}, Landroid/os/HandlerThread;-><init>(Ljava/lang/String;)V

    .line 75
    .local v0, "thread":Landroid/os/HandlerThread;
    invoke-virtual {v0}, Landroid/os/HandlerThread;->start()V

    .line 76
    new-instance v1, Lcom/miui/server/enterprise/DeviceManagerService$ServiceHandler;

    invoke-virtual {v0}, Landroid/os/HandlerThread;->getLooper()Landroid/os/Looper;

    move-result-object v2

    invoke-direct {v1, p0, v2}, Lcom/miui/server/enterprise/DeviceManagerService$ServiceHandler;-><init>(Lcom/miui/server/enterprise/DeviceManagerService;Landroid/os/Looper;)V

    iput-object v1, p0, Lcom/miui/server/enterprise/DeviceManagerService;->mServiceHandler:Lcom/miui/server/enterprise/DeviceManagerService$ServiceHandler;

    .line 77
    return-void
.end method

.method private createEntDir()V
    .locals 3
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/system/ErrnoException;
        }
    .end annotation

    .line 386
    new-instance v0, Ljava/io/File;

    const-string v1, "/data/system/theme_magic/enterprise/"

    invoke-direct {v0, v1}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 387
    .local v0, "dir":Ljava/io/File;
    invoke-virtual {v0}, Ljava/io/File;->exists()Z

    move-result v2

    if-nez v2, :cond_1

    .line 388
    invoke-virtual {v0}, Ljava/io/File;->mkdirs()Z

    move-result v2

    if-eqz v2, :cond_0

    .line 389
    const/16 v2, 0x1fd

    invoke-static {v1, v2}, Landroid/system/Os;->chmod(Ljava/lang/String;I)V

    goto :goto_0

    .line 391
    :cond_0
    const-string v1, "Enterprise-device"

    const-string v2, "createEntDir failed"

    invoke-static {v1, v2}, Landroid/util/Slog;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 394
    :cond_1
    :goto_0
    return-void
.end method

.method private getFullScreenshot(Landroid/content/Context;)Landroid/graphics/Bitmap;
    .locals 8
    .param p1, "context"    # Landroid/content/Context;

    .line 281
    const/4 v0, 0x2

    new-array v0, v0, [I

    .line 282
    .local v0, "pixelsWH":[I
    const-string/jumbo v1, "window"

    invoke-virtual {p1, v1}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/view/WindowManager;

    .line 283
    .local v1, "wm":Landroid/view/WindowManager;
    invoke-interface {v1}, Landroid/view/WindowManager;->getCurrentWindowMetrics()Landroid/view/WindowMetrics;

    move-result-object v2

    .line 284
    .local v2, "windowMetrics":Landroid/view/WindowMetrics;
    invoke-virtual {v2}, Landroid/view/WindowMetrics;->getBounds()Landroid/graphics/Rect;

    move-result-object v3

    invoke-virtual {v3}, Landroid/graphics/Rect;->width()I

    move-result v3

    const/4 v4, 0x0

    aput v3, v0, v4

    .line 285
    invoke-virtual {v2}, Landroid/view/WindowMetrics;->getBounds()Landroid/graphics/Rect;

    move-result-object v3

    invoke-virtual {v3}, Landroid/graphics/Rect;->height()I

    move-result v3

    const/4 v5, 0x1

    aput v3, v0, v5

    .line 286
    iget-object v3, p0, Lcom/miui/server/enterprise/DeviceManagerService;->mDisplayManager:Landroid/hardware/display/DisplayManager;

    invoke-virtual {v3, v4}, Landroid/hardware/display/DisplayManager;->getDisplay(I)Landroid/view/Display;

    move-result-object v3

    .line 287
    .local v3, "defaultDisplay":Landroid/view/Display;
    new-instance v6, Landroid/graphics/Rect;

    aget v7, v0, v4

    aget v5, v0, v5

    invoke-direct {v6, v4, v4, v7, v5}, Landroid/graphics/Rect;-><init>(IIII)V

    invoke-direct {p0, v6, v3}, Lcom/miui/server/enterprise/DeviceManagerService;->getScreenshot(Landroid/graphics/Rect;Landroid/view/Display;)Landroid/graphics/Bitmap;

    move-result-object v4

    return-object v4
.end method

.method private getScreenshot(Landroid/graphics/Rect;Landroid/view/Display;)Landroid/graphics/Bitmap;
    .locals 9
    .param p1, "crop"    # Landroid/graphics/Rect;
    .param p2, "display"    # Landroid/view/Display;

    .line 292
    const/4 v0, 0x0

    .line 293
    .local v0, "screenshot":Landroid/graphics/Bitmap;
    invoke-virtual {p1}, Landroid/graphics/Rect;->width()I

    move-result v1

    .line 294
    .local v1, "width":I
    invoke-virtual {p1}, Landroid/graphics/Rect;->height()I

    move-result v2

    .line 295
    .local v2, "height":I
    invoke-virtual {p2}, Landroid/view/Display;->getAddress()Landroid/view/DisplayAddress;

    move-result-object v3

    .line 296
    .local v3, "address":Landroid/view/DisplayAddress;
    instance-of v4, v3, Landroid/view/DisplayAddress$Physical;

    if-nez v4, :cond_0

    .line 297
    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "Skipping Screenshot - Default display does not have a physical address: "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    const-string v5, "Enterprise-device"

    invoke-static {v5, v4}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_1

    .line 301
    :cond_0
    const/4 v4, 0x0

    .line 302
    .local v4, "screenshotBuffer":Landroid/window/ScreenCapture$ScreenshotHardwareBuffer;
    move-object v5, v3

    check-cast v5, Landroid/view/DisplayAddress$Physical;

    .line 305
    .local v5, "physicalAddress":Landroid/view/DisplayAddress$Physical;
    invoke-virtual {v5}, Landroid/view/DisplayAddress$Physical;->getPhysicalDisplayId()J

    move-result-wide v6

    invoke-static {v6, v7}, Lcom/android/server/display/DisplayControl;->getPhysicalDisplayToken(J)Landroid/os/IBinder;

    move-result-object v6

    .line 307
    .local v6, "displayToken":Landroid/os/IBinder;
    new-instance v7, Landroid/window/ScreenCapture$DisplayCaptureArgs$Builder;

    invoke-direct {v7, v6}, Landroid/window/ScreenCapture$DisplayCaptureArgs$Builder;-><init>(Landroid/os/IBinder;)V

    .line 309
    invoke-virtual {v7, p1}, Landroid/window/ScreenCapture$DisplayCaptureArgs$Builder;->setSourceCrop(Landroid/graphics/Rect;)Landroid/window/ScreenCapture$CaptureArgs$Builder;

    move-result-object v7

    check-cast v7, Landroid/window/ScreenCapture$DisplayCaptureArgs$Builder;

    .line 310
    invoke-virtual {v7, v1, v2}, Landroid/window/ScreenCapture$DisplayCaptureArgs$Builder;->setSize(II)Landroid/window/ScreenCapture$DisplayCaptureArgs$Builder;

    move-result-object v7

    .line 311
    invoke-virtual {v7}, Landroid/window/ScreenCapture$DisplayCaptureArgs$Builder;->build()Landroid/window/ScreenCapture$DisplayCaptureArgs;

    move-result-object v7

    .line 312
    .local v7, "captureArgs":Landroid/window/ScreenCapture$DisplayCaptureArgs;
    nop

    .line 313
    invoke-static {v7}, Landroid/window/ScreenCapture;->captureDisplay(Landroid/window/ScreenCapture$DisplayCaptureArgs;)Landroid/window/ScreenCapture$ScreenshotHardwareBuffer;

    move-result-object v4

    .line 315
    if-nez v4, :cond_1

    const/4 v8, 0x0

    goto :goto_0

    :cond_1
    invoke-virtual {v4}, Landroid/window/ScreenCapture$ScreenshotHardwareBuffer;->asBitmap()Landroid/graphics/Bitmap;

    move-result-object v8

    :goto_0
    move-object v0, v8

    .line 318
    .end local v4    # "screenshotBuffer":Landroid/window/ScreenCapture$ScreenshotHardwareBuffer;
    .end local v5    # "physicalAddress":Landroid/view/DisplayAddress$Physical;
    .end local v6    # "displayToken":Landroid/os/IBinder;
    .end local v7    # "captureArgs":Landroid/window/ScreenCapture$DisplayCaptureArgs;
    :goto_1
    return-object v0
.end method

.method private rebootWifi()V
    .locals 4

    .line 397
    iget-object v0, p0, Lcom/miui/server/enterprise/DeviceManagerService;->mWifiManager:Landroid/net/wifi/WifiManager;

    invoke-virtual {v0}, Landroid/net/wifi/WifiManager;->isWifiEnabled()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 398
    iget-object v0, p0, Lcom/miui/server/enterprise/DeviceManagerService;->mWifiManager:Landroid/net/wifi/WifiManager;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/net/wifi/WifiManager;->setWifiEnabled(Z)Z

    .line 399
    iget-object v0, p0, Lcom/miui/server/enterprise/DeviceManagerService;->mServiceHandler:Lcom/miui/server/enterprise/DeviceManagerService$ServiceHandler;

    new-instance v1, Lcom/miui/server/enterprise/DeviceManagerService$2;

    invoke-direct {v1, p0}, Lcom/miui/server/enterprise/DeviceManagerService$2;-><init>(Lcom/miui/server/enterprise/DeviceManagerService;)V

    const-wide/16 v2, 0x3e8

    invoke-virtual {v0, v1, v2, v3}, Lcom/miui/server/enterprise/DeviceManagerService$ServiceHandler;->postDelayed(Ljava/lang/Runnable;J)Z

    .line 406
    :cond_0
    return-void
.end method


# virtual methods
.method public captureScreen()Landroid/graphics/Bitmap;
    .locals 4

    .line 265
    iget-object v0, p0, Lcom/miui/server/enterprise/DeviceManagerService;->mContext:Landroid/content/Context;

    invoke-static {v0}, Lcom/miui/server/enterprise/ServiceUtils;->checkPermission(Landroid/content/Context;)V

    .line 266
    iget-object v0, p0, Lcom/miui/server/enterprise/DeviceManagerService;->mContext:Landroid/content/Context;

    invoke-direct {p0, v0}, Lcom/miui/server/enterprise/DeviceManagerService;->getFullScreenshot(Landroid/content/Context;)Landroid/graphics/Bitmap;

    move-result-object v0

    .line 267
    .local v0, "bitmap":Landroid/graphics/Bitmap;
    if-nez v0, :cond_0

    .line 268
    return-object v0

    .line 271
    :cond_0
    move-object v1, v0

    .line 272
    .local v1, "original":Landroid/graphics/Bitmap;
    sget-object v2, Landroid/graphics/Bitmap$Config;->ARGB_8888:Landroid/graphics/Bitmap$Config;

    const/4 v3, 0x0

    invoke-virtual {v1, v2, v3}, Landroid/graphics/Bitmap;->copy(Landroid/graphics/Bitmap$Config;Z)Landroid/graphics/Bitmap;

    move-result-object v0

    .line 273
    invoke-virtual {v1}, Landroid/graphics/Bitmap;->recycle()V

    .line 275
    invoke-virtual {v0, v3}, Landroid/graphics/Bitmap;->setHasAlpha(Z)V

    .line 276
    invoke-virtual {v0}, Landroid/graphics/Bitmap;->prepareToDraw()V

    .line 277
    return-object v0
.end method

.method public deviceReboot()V
    .locals 4

    .line 100
    iget-object v0, p0, Lcom/miui/server/enterprise/DeviceManagerService;->mContext:Landroid/content/Context;

    invoke-static {v0}, Lcom/miui/server/enterprise/ServiceUtils;->checkPermission(Landroid/content/Context;)V

    .line 101
    nop

    .line 102
    const-string v0, "power"

    invoke-static {v0}, Landroid/os/ServiceManager;->getService(Ljava/lang/String;)Landroid/os/IBinder;

    move-result-object v0

    .line 101
    invoke-static {v0}, Landroid/os/IPowerManager$Stub;->asInterface(Landroid/os/IBinder;)Landroid/os/IPowerManager;

    move-result-object v0

    .line 105
    .local v0, "pm":Landroid/os/IPowerManager;
    const/4 v1, 0x0

    const/4 v2, 0x1

    const/4 v3, 0x0

    :try_start_0
    invoke-interface {v0, v3, v1, v2}, Landroid/os/IPowerManager;->reboot(ZLjava/lang/String;Z)V
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    .line 108
    goto :goto_0

    .line 106
    :catch_0
    move-exception v1

    .line 109
    :goto_0
    return-void
.end method

.method public deviceShutDown()V
    .locals 3

    .line 87
    iget-object v0, p0, Lcom/miui/server/enterprise/DeviceManagerService;->mContext:Landroid/content/Context;

    invoke-static {v0}, Lcom/miui/server/enterprise/ServiceUtils;->checkPermission(Landroid/content/Context;)V

    .line 88
    nop

    .line 89
    const-string v0, "power"

    invoke-static {v0}, Landroid/os/ServiceManager;->getService(Ljava/lang/String;)Landroid/os/IBinder;

    move-result-object v0

    .line 88
    invoke-static {v0}, Landroid/os/IPowerManager$Stub;->asInterface(Landroid/os/IBinder;)Landroid/os/IPowerManager;

    move-result-object v0

    .line 92
    .local v0, "pm":Landroid/os/IPowerManager;
    const/4 v1, 0x0

    const/4 v2, 0x0

    :try_start_0
    invoke-interface {v0, v2, v1, v2}, Landroid/os/IPowerManager;->shutdown(ZLjava/lang/String;Z)V
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    .line 95
    goto :goto_0

    .line 93
    :catch_0
    move-exception v1

    .line 96
    :goto_0
    return-void
.end method

.method public enableUsbDebug(Z)V
    .locals 2
    .param p1, "enable"    # Z

    .line 357
    iget-object v0, p0, Lcom/miui/server/enterprise/DeviceManagerService;->mContext:Landroid/content/Context;

    invoke-static {v0}, Lcom/miui/server/enterprise/ServiceUtils;->checkPermission(Landroid/content/Context;)V

    .line 358
    iget-object v0, p0, Lcom/miui/server/enterprise/DeviceManagerService;->mContext:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    const-string v1, "adb_enabled"

    invoke-static {v0, v1, p1}, Landroid/provider/Settings$Global;->putInt(Landroid/content/ContentResolver;Ljava/lang/String;I)Z

    .line 359
    return-void
.end method

.method public formatSdCard()V
    .locals 6

    .line 113
    iget-object v0, p0, Lcom/miui/server/enterprise/DeviceManagerService;->mContext:Landroid/content/Context;

    invoke-static {v0}, Lcom/miui/server/enterprise/ServiceUtils;->checkPermission(Landroid/content/Context;)V

    .line 114
    iget-object v0, p0, Lcom/miui/server/enterprise/DeviceManagerService;->mContext:Landroid/content/Context;

    const-class v1, Landroid/os/storage/StorageManager;

    invoke-virtual {v0, v1}, Landroid/content/Context;->getSystemService(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/os/storage/StorageManager;

    .line 115
    .local v0, "storageManager":Landroid/os/storage/StorageManager;
    const/4 v1, 0x0

    .line 116
    .local v1, "usbVol":Landroid/os/storage/VolumeInfo;
    invoke-virtual {v0}, Landroid/os/storage/StorageManager;->getVolumes()Ljava/util/List;

    move-result-object v2

    invoke-interface {v2}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_1

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Landroid/os/storage/VolumeInfo;

    .line 117
    .local v3, "vol":Landroid/os/storage/VolumeInfo;
    invoke-virtual {v3}, Landroid/os/storage/VolumeInfo;->getType()I

    move-result v4

    if-nez v4, :cond_0

    .line 118
    invoke-virtual {v3}, Landroid/os/storage/VolumeInfo;->getDisk()Landroid/os/storage/DiskInfo;

    move-result-object v4

    iget v4, v4, Landroid/os/storage/DiskInfo;->flags:I

    const/4 v5, 0x4

    and-int/2addr v4, v5

    if-ne v4, v5, :cond_0

    .line 119
    move-object v1, v3

    .line 120
    goto :goto_1

    .line 122
    .end local v3    # "vol":Landroid/os/storage/VolumeInfo;
    :cond_0
    goto :goto_0

    .line 123
    :cond_1
    :goto_1
    if-eqz v1, :cond_2

    .line 124
    invoke-virtual {v1}, Landroid/os/storage/VolumeInfo;->getState()I

    move-result v2

    const/4 v3, 0x2

    if-ne v2, v3, :cond_2

    .line 125
    invoke-virtual {v1}, Landroid/os/storage/VolumeInfo;->getId()Ljava/lang/String;

    move-result-object v2

    .line 126
    .local v2, "volId":Ljava/lang/String;
    new-instance v3, Ljava/lang/Thread;

    new-instance v4, Lcom/miui/server/enterprise/DeviceManagerService$1;

    invoke-direct {v4, p0, v0, v2}, Lcom/miui/server/enterprise/DeviceManagerService$1;-><init>(Lcom/miui/server/enterprise/DeviceManagerService;Landroid/os/storage/StorageManager;Ljava/lang/String;)V

    invoke-direct {v3, v4}, Ljava/lang/Thread;-><init>(Ljava/lang/Runnable;)V

    .line 131
    invoke-virtual {v3}, Ljava/lang/Thread;->start()V

    .line 134
    .end local v2    # "volId":Ljava/lang/String;
    :cond_2
    return-void
.end method

.method public getDefaultHome()Ljava/lang/String;
    .locals 2

    .line 450
    iget-object v0, p0, Lcom/miui/server/enterprise/DeviceManagerService;->mContext:Landroid/content/Context;

    const-string v1, "ep_default_home"

    invoke-static {v0, v1}, Lcom/miui/enterprise/settings/EnterpriseSettings;->getString(Landroid/content/Context;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public getIpBlackList(I)Ljava/util/List;
    .locals 2
    .param p1, "userId"    # I
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(I)",
            "Ljava/util/List<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .line 350
    iget-object v0, p0, Lcom/miui/server/enterprise/DeviceManagerService;->mContext:Landroid/content/Context;

    invoke-static {v0}, Lcom/miui/server/enterprise/ServiceUtils;->checkPermission(Landroid/content/Context;)V

    .line 351
    iget-object v0, p0, Lcom/miui/server/enterprise/DeviceManagerService;->mContext:Landroid/content/Context;

    const-string v1, "ep_ip_black_list"

    invoke-static {v0, v1, p1}, Lcom/miui/enterprise/settings/EnterpriseSettings;->getString(Landroid/content/Context;Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/miui/enterprise/settings/EnterpriseSettings;->parseListSettings(Ljava/lang/String;)Ljava/util/List;

    move-result-object v0

    return-object v0
.end method

.method public getIpWhiteList(I)Ljava/util/List;
    .locals 2
    .param p1, "userId"    # I
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(I)",
            "Ljava/util/List<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .line 343
    iget-object v0, p0, Lcom/miui/server/enterprise/DeviceManagerService;->mContext:Landroid/content/Context;

    invoke-static {v0}, Lcom/miui/server/enterprise/ServiceUtils;->checkPermission(Landroid/content/Context;)V

    .line 344
    iget-object v0, p0, Lcom/miui/server/enterprise/DeviceManagerService;->mContext:Landroid/content/Context;

    const-string v1, "ep_ip_white_list"

    invoke-static {v0, v1, p1}, Lcom/miui/enterprise/settings/EnterpriseSettings;->getString(Landroid/content/Context;Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/miui/enterprise/settings/EnterpriseSettings;->parseListSettings(Ljava/lang/String;)Ljava/util/List;

    move-result-object v0

    return-object v0
.end method

.method public getUrlBlackList(I)Ljava/util/List;
    .locals 2
    .param p1, "userId"    # I
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(I)",
            "Ljava/util/List<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .line 159
    iget-object v0, p0, Lcom/miui/server/enterprise/DeviceManagerService;->mContext:Landroid/content/Context;

    invoke-static {v0}, Lcom/miui/server/enterprise/ServiceUtils;->checkPermission(Landroid/content/Context;)V

    .line 160
    iget-object v0, p0, Lcom/miui/server/enterprise/DeviceManagerService;->mContext:Landroid/content/Context;

    const-string v1, "ep_url_black_list"

    invoke-static {v0, v1, p1}, Lcom/miui/enterprise/settings/EnterpriseSettings;->getString(Landroid/content/Context;Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/miui/enterprise/settings/EnterpriseSettings;->parseListSettings(Ljava/lang/String;)Ljava/util/List;

    move-result-object v0

    return-object v0
.end method

.method public getUrlWhiteList(I)Ljava/util/List;
    .locals 2
    .param p1, "userId"    # I
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(I)",
            "Ljava/util/List<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .line 152
    iget-object v0, p0, Lcom/miui/server/enterprise/DeviceManagerService;->mContext:Landroid/content/Context;

    invoke-static {v0}, Lcom/miui/server/enterprise/ServiceUtils;->checkPermission(Landroid/content/Context;)V

    .line 153
    iget-object v0, p0, Lcom/miui/server/enterprise/DeviceManagerService;->mContext:Landroid/content/Context;

    const-string v1, "ep_url_white_list"

    invoke-static {v0, v1, p1}, Lcom/miui/enterprise/settings/EnterpriseSettings;->getString(Landroid/content/Context;Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/miui/enterprise/settings/EnterpriseSettings;->parseListSettings(Ljava/lang/String;)Ljava/util/List;

    move-result-object v0

    return-object v0
.end method

.method public getWifiApBssidBlackList(I)Ljava/util/List;
    .locals 2
    .param p1, "userId"    # I
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(I)",
            "Ljava/util/List<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .line 246
    iget-object v0, p0, Lcom/miui/server/enterprise/DeviceManagerService;->mContext:Landroid/content/Context;

    invoke-static {v0}, Lcom/miui/server/enterprise/ServiceUtils;->checkPermission(Landroid/content/Context;)V

    .line 247
    iget-object v0, p0, Lcom/miui/server/enterprise/DeviceManagerService;->mContext:Landroid/content/Context;

    const-string v1, "ep_wifi_ap_bssid_black_list"

    invoke-static {v0, v1, p1}, Lcom/miui/enterprise/settings/EnterpriseSettings;->getString(Landroid/content/Context;Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/miui/enterprise/settings/EnterpriseSettings;->parseListSettings(Ljava/lang/String;)Ljava/util/List;

    move-result-object v0

    return-object v0
.end method

.method public getWifiApBssidWhiteList(I)Ljava/util/List;
    .locals 2
    .param p1, "userId"    # I
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(I)",
            "Ljava/util/List<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .line 232
    iget-object v0, p0, Lcom/miui/server/enterprise/DeviceManagerService;->mContext:Landroid/content/Context;

    invoke-static {v0}, Lcom/miui/server/enterprise/ServiceUtils;->checkPermission(Landroid/content/Context;)V

    .line 233
    iget-object v0, p0, Lcom/miui/server/enterprise/DeviceManagerService;->mContext:Landroid/content/Context;

    const-string v1, "ep_wifi_ap_bssid_white_list"

    invoke-static {v0, v1, p1}, Lcom/miui/enterprise/settings/EnterpriseSettings;->getString(Landroid/content/Context;Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/miui/enterprise/settings/EnterpriseSettings;->parseListSettings(Ljava/lang/String;)Ljava/util/List;

    move-result-object v0

    return-object v0
.end method

.method public getWifiApSsidBlackList(I)Ljava/util/List;
    .locals 2
    .param p1, "userId"    # I
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(I)",
            "Ljava/util/List<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .line 239
    iget-object v0, p0, Lcom/miui/server/enterprise/DeviceManagerService;->mContext:Landroid/content/Context;

    invoke-static {v0}, Lcom/miui/server/enterprise/ServiceUtils;->checkPermission(Landroid/content/Context;)V

    .line 240
    iget-object v0, p0, Lcom/miui/server/enterprise/DeviceManagerService;->mContext:Landroid/content/Context;

    const-string v1, "ep_wifi_ap_ssid_black_list"

    invoke-static {v0, v1, p1}, Lcom/miui/enterprise/settings/EnterpriseSettings;->getString(Landroid/content/Context;Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/miui/enterprise/settings/EnterpriseSettings;->parseListSettings(Ljava/lang/String;)Ljava/util/List;

    move-result-object v0

    return-object v0
.end method

.method public getWifiApSsidWhiteList(I)Ljava/util/List;
    .locals 2
    .param p1, "userId"    # I
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(I)",
            "Ljava/util/List<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .line 225
    iget-object v0, p0, Lcom/miui/server/enterprise/DeviceManagerService;->mContext:Landroid/content/Context;

    invoke-static {v0}, Lcom/miui/server/enterprise/ServiceUtils;->checkPermission(Landroid/content/Context;)V

    .line 226
    iget-object v0, p0, Lcom/miui/server/enterprise/DeviceManagerService;->mContext:Landroid/content/Context;

    const-string v1, "ep_wifi_ap_ssid_white_list"

    invoke-static {v0, v1, p1}, Lcom/miui/enterprise/settings/EnterpriseSettings;->getString(Landroid/content/Context;Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/miui/enterprise/settings/EnterpriseSettings;->parseListSettings(Ljava/lang/String;)Ljava/util/List;

    move-result-object v0

    return-object v0
.end method

.method public getWifiConnRestriction(I)I
    .locals 3
    .param p1, "userId"    # I

    .line 186
    iget-object v0, p0, Lcom/miui/server/enterprise/DeviceManagerService;->mContext:Landroid/content/Context;

    invoke-static {v0}, Lcom/miui/server/enterprise/ServiceUtils;->checkPermission(Landroid/content/Context;)V

    .line 187
    iget-object v0, p0, Lcom/miui/server/enterprise/DeviceManagerService;->mContext:Landroid/content/Context;

    const-string v1, "ep_wifi_conn_restriction_mode"

    const/4 v2, 0x0

    invoke-static {v0, v1, v2}, Lcom/miui/enterprise/settings/EnterpriseSettings;->getInt(Landroid/content/Context;Ljava/lang/String;I)I

    move-result v0

    return v0
.end method

.method public isDeviceRoot()Z
    .locals 2

    .line 81
    iget-object v0, p0, Lcom/miui/server/enterprise/DeviceManagerService;->mContext:Landroid/content/Context;

    invoke-static {v0}, Lcom/miui/server/enterprise/ServiceUtils;->checkPermission(Landroid/content/Context;)V

    .line 82
    new-instance v0, Ljava/io/File;

    const-string v1, "/system/xbin/su"

    invoke-direct {v0, v1}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0}, Ljava/io/File;->exists()Z

    move-result v0

    if-nez v0, :cond_1

    new-instance v0, Ljava/io/File;

    const-string v1, "/system/bin/su"

    invoke-direct {v0, v1}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0}, Ljava/io/File;->exists()Z

    move-result v0

    if-eqz v0, :cond_0

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    goto :goto_1

    :cond_1
    :goto_0
    const/4 v0, 0x1

    :goto_1
    return v0
.end method

.method public recoveryFactory(Z)V
    .locals 2
    .param p1, "formatSdcard"    # Z

    .line 166
    iget-object v0, p0, Lcom/miui/server/enterprise/DeviceManagerService;->mContext:Landroid/content/Context;

    invoke-static {v0}, Lcom/miui/server/enterprise/ServiceUtils;->checkPermission(Landroid/content/Context;)V

    .line 167
    new-instance v0, Landroid/content/Intent;

    const-string v1, "android.intent.action.MASTER_CLEAR"

    invoke-direct {v0, v1}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 168
    .local v0, "factoryResetIntent":Landroid/content/Intent;
    const-string v1, "format_sdcard"

    invoke-virtual {v0, v1, p1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    .line 169
    nop

    .line 170
    const-string v1, "android.intent.action.FACTORY_RESET"

    invoke-virtual {v0, v1}, Landroid/content/Intent;->setAction(Ljava/lang/String;)Landroid/content/Intent;

    .line 171
    const-string v1, "android"

    invoke-virtual {v0, v1}, Landroid/content/Intent;->setPackage(Ljava/lang/String;)Landroid/content/Intent;

    .line 173
    const/high16 v1, 0x10000000

    invoke-virtual {v0, v1}, Landroid/content/Intent;->addFlags(I)Landroid/content/Intent;

    .line 174
    iget-object v1, p0, Lcom/miui/server/enterprise/DeviceManagerService;->mContext:Landroid/content/Context;

    invoke-virtual {v1, v0}, Landroid/content/Context;->sendBroadcast(Landroid/content/Intent;)V

    .line 175
    return-void
.end method

.method public setBootAnimation(Ljava/lang/String;)Z
    .locals 5
    .param p1, "path"    # Ljava/lang/String;

    .line 364
    const-string v0, "Enterprise-device"

    :try_start_0
    iget-object v1, p0, Lcom/miui/server/enterprise/DeviceManagerService;->mContext:Landroid/content/Context;

    invoke-static {v1}, Lcom/miui/server/enterprise/ServiceUtils;->checkPermission(Landroid/content/Context;)V
    :try_end_0
    .catch Ljava/lang/SecurityException; {:try_start_0 .. :try_end_0} :catch_1

    .line 368
    nop

    .line 370
    :try_start_1
    invoke-direct {p0}, Lcom/miui/server/enterprise/DeviceManagerService;->createEntDir()V

    .line 371
    new-instance v1, Landroid/content/Intent;

    invoke-direct {v1}, Landroid/content/Intent;-><init>()V

    .line 372
    .local v1, "intent":Landroid/content/Intent;
    new-instance v2, Landroid/content/ComponentName;

    const-string v3, "com.miui.securitycore"

    const-string v4, "com.miui.enterprise.service.EntInstallService"

    invoke-direct {v2, v3, v4}, Landroid/content/ComponentName;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {v1, v2}, Landroid/content/Intent;->setComponent(Landroid/content/ComponentName;)Landroid/content/Intent;

    .line 374
    new-instance v2, Landroid/os/Bundle;

    invoke-direct {v2}, Landroid/os/Bundle;-><init>()V

    .line 375
    .local v2, "bundle":Landroid/os/Bundle;
    const-string v3, "apkPath"

    invoke-virtual {v2, v3, p1}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 376
    const-string/jumbo v3, "type"

    const-string v4, "BootAnimation"

    invoke-virtual {v2, v3, v4}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 377
    invoke-virtual {v1, v2}, Landroid/content/Intent;->putExtras(Landroid/os/Bundle;)Landroid/content/Intent;

    .line 378
    iget-object v3, p0, Lcom/miui/server/enterprise/DeviceManagerService;->mContext:Landroid/content/Context;

    invoke-virtual {v3, v1}, Landroid/content/Context;->startService(Landroid/content/Intent;)Landroid/content/ComponentName;
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_0

    .line 381
    nop

    .end local v1    # "intent":Landroid/content/Intent;
    .end local v2    # "bundle":Landroid/os/Bundle;
    goto :goto_0

    .line 379
    :catch_0
    move-exception v1

    .line 380
    .local v1, "e":Ljava/lang/Exception;
    const-string/jumbo v2, "setBootAnimation"

    invoke-static {v0, v2, v1}, Landroid/util/Slog;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    .line 382
    .end local v1    # "e":Ljava/lang/Exception;
    :goto_0
    const/4 v0, 0x1

    return v0

    .line 365
    :catch_1
    move-exception v1

    .line 366
    .local v1, "e":Ljava/lang/SecurityException;
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "Uid "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-static {}, Landroid/os/Binder;->getCallingUid()I

    move-result v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, " has no permission to access this API"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v0, v2, v1}, Landroid/util/Slog;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    .line 367
    const/4 v0, 0x0

    return v0
.end method

.method public setBrowserRestriction(II)V
    .locals 2
    .param p1, "mode"    # I
    .param p2, "userId"    # I

    .line 259
    iget-object v0, p0, Lcom/miui/server/enterprise/DeviceManagerService;->mContext:Landroid/content/Context;

    invoke-static {v0}, Lcom/miui/server/enterprise/ServiceUtils;->checkPermission(Landroid/content/Context;)V

    .line 260
    iget-object v0, p0, Lcom/miui/server/enterprise/DeviceManagerService;->mContext:Landroid/content/Context;

    const-string v1, "ep_host_restriction_mode"

    invoke-static {v0, v1, p1, p2}, Lcom/miui/enterprise/settings/EnterpriseSettings;->putInt(Landroid/content/Context;Ljava/lang/String;II)V

    .line 261
    return-void
.end method

.method public setDefaultHome(Ljava/lang/String;)V
    .locals 14
    .param p1, "pkgName"    # Ljava/lang/String;

    .line 410
    iget-object v0, p0, Lcom/miui/server/enterprise/DeviceManagerService;->mContext:Landroid/content/Context;

    invoke-static {v0}, Lcom/miui/server/enterprise/ServiceUtils;->checkPermission(Landroid/content/Context;)V

    .line 411
    const/4 v0, 0x0

    .line 412
    .local v0, "isPkgInstalled":Z
    const/4 v1, 0x0

    .line 413
    .local v1, "packageInfo":Landroid/content/pm/ResolveInfo;
    iget-object v2, p0, Lcom/miui/server/enterprise/DeviceManagerService;->mContext:Landroid/content/Context;

    invoke-virtual {v2}, Landroid/content/Context;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v2

    .line 415
    .local v2, "pm":Landroid/content/pm/PackageManager;
    new-instance v3, Landroid/content/Intent;

    const-string v4, "android.intent.action.MAIN"

    invoke-direct {v3, v4}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 416
    .local v3, "homeIntent":Landroid/content/Intent;
    const-string v5, "android.intent.category.HOME"

    invoke-virtual {v3, v5}, Landroid/content/Intent;->addCategory(Ljava/lang/String;)Landroid/content/Intent;

    .line 417
    const/high16 v6, 0x20000

    invoke-virtual {v2, v3, v6}, Landroid/content/pm/PackageManager;->queryIntentActivities(Landroid/content/Intent;I)Ljava/util/List;

    move-result-object v6

    .line 418
    .local v6, "lists":Ljava/util/List;, "Ljava/util/List<Landroid/content/pm/ResolveInfo;>;"
    invoke-interface {v6}, Ljava/util/List;->size()I

    move-result v7

    new-array v7, v7, [Landroid/content/ComponentName;

    .line 420
    .local v7, "set":[Landroid/content/ComponentName;
    const/4 v8, 0x0

    .line 421
    .local v8, "bestMatch":I
    const/4 v9, 0x0

    .local v9, "i":I
    :goto_0
    invoke-interface {v6}, Ljava/util/List;->size()I

    move-result v10

    if-ge v9, v10, :cond_2

    .line 422
    invoke-interface {v6, v9}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v10

    check-cast v10, Landroid/content/pm/ResolveInfo;

    .line 423
    .local v10, "item":Landroid/content/pm/ResolveInfo;
    iget-object v11, v10, Landroid/content/pm/ResolveInfo;->activityInfo:Landroid/content/pm/ActivityInfo;

    iget-object v11, v11, Landroid/content/pm/ActivityInfo;->packageName:Ljava/lang/String;

    invoke-virtual {p1, v11}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v11

    if-eqz v11, :cond_0

    .line 424
    const/4 v0, 0x1

    .line 425
    move-object v1, v10

    .line 427
    :cond_0
    new-instance v11, Landroid/content/ComponentName;

    iget-object v12, v10, Landroid/content/pm/ResolveInfo;->activityInfo:Landroid/content/pm/ActivityInfo;

    iget-object v12, v12, Landroid/content/pm/ActivityInfo;->packageName:Ljava/lang/String;

    iget-object v13, v10, Landroid/content/pm/ResolveInfo;->activityInfo:Landroid/content/pm/ActivityInfo;

    iget-object v13, v13, Landroid/content/pm/ActivityInfo;->name:Ljava/lang/String;

    invoke-direct {v11, v12, v13}, Landroid/content/ComponentName;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    aput-object v11, v7, v9

    .line 429
    iget v11, v10, Landroid/content/pm/ResolveInfo;->match:I

    if-le v11, v8, :cond_1

    iget v8, v10, Landroid/content/pm/ResolveInfo;->match:I

    .line 421
    .end local v10    # "item":Landroid/content/pm/ResolveInfo;
    :cond_1
    add-int/lit8 v9, v9, 0x1

    goto :goto_0

    .line 432
    .end local v9    # "i":I
    :cond_2
    const/4 v9, 0x0

    invoke-virtual {v2, v3, v9}, Landroid/content/pm/PackageManager;->resolveActivity(Landroid/content/Intent;I)Landroid/content/pm/ResolveInfo;

    move-result-object v9

    .line 433
    .local v9, "resolveInfo":Landroid/content/pm/ResolveInfo;
    iget-object v10, v9, Landroid/content/pm/ResolveInfo;->activityInfo:Landroid/content/pm/ActivityInfo;

    iget-object v10, v10, Landroid/content/pm/ActivityInfo;->packageName:Ljava/lang/String;

    invoke-virtual {v2, v10}, Landroid/content/pm/PackageManager;->clearPackagePreferredActivities(Ljava/lang/String;)V

    .line 434
    if-eqz v0, :cond_3

    if-eqz v1, :cond_3

    .line 435
    new-instance v10, Landroid/content/IntentFilter;

    invoke-direct {v10}, Landroid/content/IntentFilter;-><init>()V

    .line 436
    .local v10, "intentFilter":Landroid/content/IntentFilter;
    invoke-virtual {v10, v4}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    .line 437
    invoke-virtual {v10, v5}, Landroid/content/IntentFilter;->addCategory(Ljava/lang/String;)V

    .line 438
    const-string v4, "android.intent.category.DEFAULT"

    invoke-virtual {v10, v4}, Landroid/content/IntentFilter;->addCategory(Ljava/lang/String;)V

    .line 439
    const-string v4, "android.intent.category.BROWSABLE"

    invoke-virtual {v10, v4}, Landroid/content/IntentFilter;->addCategory(Ljava/lang/String;)V

    .line 440
    new-instance v4, Landroid/content/ComponentName;

    iget-object v5, v1, Landroid/content/pm/ResolveInfo;->activityInfo:Landroid/content/pm/ActivityInfo;

    iget-object v5, v5, Landroid/content/pm/ActivityInfo;->packageName:Ljava/lang/String;

    iget-object v11, v1, Landroid/content/pm/ResolveInfo;->activityInfo:Landroid/content/pm/ActivityInfo;

    iget-object v11, v11, Landroid/content/pm/ActivityInfo;->name:Ljava/lang/String;

    invoke-direct {v4, v5, v11}, Landroid/content/ComponentName;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {v2, v10, v8, v7, v4}, Landroid/content/pm/PackageManager;->addPreferredActivity(Landroid/content/IntentFilter;I[Landroid/content/ComponentName;Landroid/content/ComponentName;)V

    .line 442
    new-instance v4, Landroid/content/ComponentName;

    iget-object v5, v1, Landroid/content/pm/ResolveInfo;->activityInfo:Landroid/content/pm/ActivityInfo;

    iget-object v5, v5, Landroid/content/pm/ActivityInfo;->packageName:Ljava/lang/String;

    iget-object v11, v1, Landroid/content/pm/ResolveInfo;->activityInfo:Landroid/content/pm/ActivityInfo;

    iget-object v11, v11, Landroid/content/pm/ActivityInfo;->name:Ljava/lang/String;

    invoke-direct {v4, v5, v11}, Landroid/content/ComponentName;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {v2, v10, v8, v7, v4}, Landroid/content/pm/PackageManager;->replacePreferredActivity(Landroid/content/IntentFilter;I[Landroid/content/ComponentName;Landroid/content/ComponentName;)V

    .line 444
    iget-object v4, p0, Lcom/miui/server/enterprise/DeviceManagerService;->mContext:Landroid/content/Context;

    const-string v5, "ep_default_home"

    invoke-static {v4, v5, p1}, Lcom/miui/enterprise/settings/EnterpriseSettings;->putString(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;)V

    .line 446
    .end local v10    # "intentFilter":Landroid/content/IntentFilter;
    :cond_3
    return-void
.end method

.method public setIpBlackList(Ljava/util/List;I)V
    .locals 3
    .param p2, "userId"    # I
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List<",
            "Ljava/lang/String;",
            ">;I)V"
        }
    .end annotation

    .line 336
    .local p1, "list":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;"
    iget-object v0, p0, Lcom/miui/server/enterprise/DeviceManagerService;->mContext:Landroid/content/Context;

    invoke-static {v0}, Lcom/miui/server/enterprise/ServiceUtils;->checkPermission(Landroid/content/Context;)V

    .line 337
    iget-object v0, p0, Lcom/miui/server/enterprise/DeviceManagerService;->mContext:Landroid/content/Context;

    .line 338
    invoke-static {p1}, Lcom/miui/enterprise/settings/EnterpriseSettings;->generateListSettings(Ljava/util/List;)Ljava/lang/String;

    move-result-object v1

    .line 337
    const-string v2, "ep_ip_black_list"

    invoke-static {v0, v2, v1, p2}, Lcom/miui/enterprise/settings/EnterpriseSettings;->putString(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;I)V

    .line 339
    return-void
.end method

.method public setIpRestriction(II)V
    .locals 2
    .param p1, "mode"    # I
    .param p2, "userId"    # I

    .line 323
    iget-object v0, p0, Lcom/miui/server/enterprise/DeviceManagerService;->mContext:Landroid/content/Context;

    invoke-static {v0}, Lcom/miui/server/enterprise/ServiceUtils;->checkPermission(Landroid/content/Context;)V

    .line 324
    iget-object v0, p0, Lcom/miui/server/enterprise/DeviceManagerService;->mContext:Landroid/content/Context;

    const-string v1, "ep_ip_restriction_mode"

    invoke-static {v0, v1, p1, p2}, Lcom/miui/enterprise/settings/EnterpriseSettings;->putInt(Landroid/content/Context;Ljava/lang/String;II)V

    .line 325
    return-void
.end method

.method public setIpWhiteList(Ljava/util/List;I)V
    .locals 3
    .param p2, "userId"    # I
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List<",
            "Ljava/lang/String;",
            ">;I)V"
        }
    .end annotation

    .line 329
    .local p1, "list":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;"
    iget-object v0, p0, Lcom/miui/server/enterprise/DeviceManagerService;->mContext:Landroid/content/Context;

    invoke-static {v0}, Lcom/miui/server/enterprise/ServiceUtils;->checkPermission(Landroid/content/Context;)V

    .line 330
    iget-object v0, p0, Lcom/miui/server/enterprise/DeviceManagerService;->mContext:Landroid/content/Context;

    .line 331
    invoke-static {p1}, Lcom/miui/enterprise/settings/EnterpriseSettings;->generateListSettings(Ljava/util/List;)Ljava/lang/String;

    move-result-object v1

    .line 330
    const-string v2, "ep_ip_white_list"

    invoke-static {v0, v2, v1, p2}, Lcom/miui/enterprise/settings/EnterpriseSettings;->putString(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;I)V

    .line 332
    return-void
.end method

.method public setLockWallPaper(Ljava/lang/String;)V
    .locals 4
    .param p1, "path"    # Ljava/lang/String;

    .line 473
    iget-object v0, p0, Lcom/miui/server/enterprise/DeviceManagerService;->mContext:Landroid/content/Context;

    invoke-static {v0}, Lcom/miui/server/enterprise/ServiceUtils;->checkPermission(Landroid/content/Context;)V

    .line 475
    :try_start_0
    new-instance v0, Landroid/content/Intent;

    invoke-direct {v0}, Landroid/content/Intent;-><init>()V

    .line 476
    .local v0, "intent":Landroid/content/Intent;
    new-instance v1, Landroid/content/ComponentName;

    const-string v2, "com.miui.securitycore"

    const-string v3, "com.miui.enterprise.service.EntInstallService"

    invoke-direct {v1, v2, v3}, Landroid/content/ComponentName;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {v0, v1}, Landroid/content/Intent;->setComponent(Landroid/content/ComponentName;)Landroid/content/Intent;

    .line 478
    new-instance v1, Landroid/os/Bundle;

    invoke-direct {v1}, Landroid/os/Bundle;-><init>()V

    .line 479
    .local v1, "bundle":Landroid/os/Bundle;
    const-string v2, "apkPath"

    invoke-virtual {v1, v2, p1}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 480
    const-string/jumbo v2, "type"

    const-string v3, "LockWallPaper"

    invoke-virtual {v1, v2, v3}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 481
    invoke-virtual {v0, v1}, Landroid/content/Intent;->putExtras(Landroid/os/Bundle;)Landroid/content/Intent;

    .line 482
    iget-object v2, p0, Lcom/miui/server/enterprise/DeviceManagerService;->mContext:Landroid/content/Context;

    invoke-virtual {v2, v0}, Landroid/content/Context;->startService(Landroid/content/Intent;)Landroid/content/ComponentName;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 485
    nop

    .end local v0    # "intent":Landroid/content/Intent;
    .end local v1    # "bundle":Landroid/os/Bundle;
    goto :goto_0

    .line 483
    :catch_0
    move-exception v0

    .line 484
    .local v0, "e":Ljava/lang/Exception;
    const-string v1, "Enterprise-device"

    const-string/jumbo v2, "setWallPaper"

    invoke-static {v1, v2, v0}, Landroid/util/Slog;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    .line 486
    .end local v0    # "e":Ljava/lang/Exception;
    :goto_0
    return-void
.end method

.method public setRingerMode(I)V
    .locals 2
    .param p1, "ringerMode"    # I

    .line 253
    iget-object v0, p0, Lcom/miui/server/enterprise/DeviceManagerService;->mContext:Landroid/content/Context;

    invoke-static {v0}, Lcom/miui/server/enterprise/ServiceUtils;->checkPermission(Landroid/content/Context;)V

    .line 254
    iget-object v0, p0, Lcom/miui/server/enterprise/DeviceManagerService;->mContext:Landroid/content/Context;

    const-string v1, "audio"

    invoke-virtual {v0, v1}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/media/AudioManager;

    invoke-virtual {v0, p1}, Landroid/media/AudioManager;->setRingerMode(I)V

    .line 255
    return-void
.end method

.method public setUrlBlackList(Ljava/util/List;I)V
    .locals 3
    .param p2, "userId"    # I
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List<",
            "Ljava/lang/String;",
            ">;I)V"
        }
    .end annotation

    .line 145
    .local p1, "urls":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;"
    iget-object v0, p0, Lcom/miui/server/enterprise/DeviceManagerService;->mContext:Landroid/content/Context;

    invoke-static {v0}, Lcom/miui/server/enterprise/ServiceUtils;->checkPermission(Landroid/content/Context;)V

    .line 146
    iget-object v0, p0, Lcom/miui/server/enterprise/DeviceManagerService;->mContext:Landroid/content/Context;

    .line 147
    invoke-static {p1}, Lcom/miui/enterprise/settings/EnterpriseSettings;->generateListSettings(Ljava/util/List;)Ljava/lang/String;

    move-result-object v1

    .line 146
    const-string v2, "ep_url_white_list"

    invoke-static {v0, v2, v1, p2}, Lcom/miui/enterprise/settings/EnterpriseSettings;->putString(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;I)V

    .line 148
    return-void
.end method

.method public setUrlWhiteList(Ljava/util/List;I)V
    .locals 3
    .param p2, "userId"    # I
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List<",
            "Ljava/lang/String;",
            ">;I)V"
        }
    .end annotation

    .line 138
    .local p1, "urls":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;"
    iget-object v0, p0, Lcom/miui/server/enterprise/DeviceManagerService;->mContext:Landroid/content/Context;

    invoke-static {v0}, Lcom/miui/server/enterprise/ServiceUtils;->checkPermission(Landroid/content/Context;)V

    .line 139
    iget-object v0, p0, Lcom/miui/server/enterprise/DeviceManagerService;->mContext:Landroid/content/Context;

    .line 140
    invoke-static {p1}, Lcom/miui/enterprise/settings/EnterpriseSettings;->generateListSettings(Ljava/util/List;)Ljava/lang/String;

    move-result-object v1

    .line 139
    const-string v2, "ep_url_white_list"

    invoke-static {v0, v2, v1, p2}, Lcom/miui/enterprise/settings/EnterpriseSettings;->putString(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;I)V

    .line 141
    return-void
.end method

.method public setWallPaper(Ljava/lang/String;)V
    .locals 4
    .param p1, "path"    # Ljava/lang/String;

    .line 455
    iget-object v0, p0, Lcom/miui/server/enterprise/DeviceManagerService;->mContext:Landroid/content/Context;

    invoke-static {v0}, Lcom/miui/server/enterprise/ServiceUtils;->checkPermission(Landroid/content/Context;)V

    .line 457
    :try_start_0
    new-instance v0, Landroid/content/Intent;

    invoke-direct {v0}, Landroid/content/Intent;-><init>()V

    .line 458
    .local v0, "intent":Landroid/content/Intent;
    new-instance v1, Landroid/content/ComponentName;

    const-string v2, "com.miui.securitycore"

    const-string v3, "com.miui.enterprise.service.EntInstallService"

    invoke-direct {v1, v2, v3}, Landroid/content/ComponentName;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {v0, v1}, Landroid/content/Intent;->setComponent(Landroid/content/ComponentName;)Landroid/content/Intent;

    .line 460
    new-instance v1, Landroid/os/Bundle;

    invoke-direct {v1}, Landroid/os/Bundle;-><init>()V

    .line 461
    .local v1, "bundle":Landroid/os/Bundle;
    const-string v2, "apkPath"

    invoke-virtual {v1, v2, p1}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 462
    const-string/jumbo v2, "type"

    const-string v3, "WallPaper"

    invoke-virtual {v1, v2, v3}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 463
    invoke-virtual {v0, v1}, Landroid/content/Intent;->putExtras(Landroid/os/Bundle;)Landroid/content/Intent;

    .line 464
    iget-object v2, p0, Lcom/miui/server/enterprise/DeviceManagerService;->mContext:Landroid/content/Context;

    invoke-virtual {v2, v0}, Landroid/content/Context;->startService(Landroid/content/Intent;)Landroid/content/ComponentName;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 467
    nop

    .end local v0    # "intent":Landroid/content/Intent;
    .end local v1    # "bundle":Landroid/os/Bundle;
    goto :goto_0

    .line 465
    :catch_0
    move-exception v0

    .line 466
    .local v0, "e":Ljava/lang/Exception;
    const-string v1, "Enterprise-device"

    const-string/jumbo v2, "setWallPaper"

    invoke-static {v1, v2, v0}, Landroid/util/Slog;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    .line 469
    .end local v0    # "e":Ljava/lang/Exception;
    :goto_0
    return-void
.end method

.method public setWifiApBssidBlackList(Ljava/util/List;I)V
    .locals 3
    .param p2, "userId"    # I
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List<",
            "Ljava/lang/String;",
            ">;I)V"
        }
    .end annotation

    .line 217
    .local p1, "bssid":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;"
    iget-object v0, p0, Lcom/miui/server/enterprise/DeviceManagerService;->mContext:Landroid/content/Context;

    invoke-static {v0}, Lcom/miui/server/enterprise/ServiceUtils;->checkPermission(Landroid/content/Context;)V

    .line 218
    iget-object v0, p0, Lcom/miui/server/enterprise/DeviceManagerService;->mContext:Landroid/content/Context;

    .line 219
    invoke-static {p1}, Lcom/miui/enterprise/settings/EnterpriseSettings;->generateListSettings(Ljava/util/List;)Ljava/lang/String;

    move-result-object v1

    .line 218
    const-string v2, "ep_wifi_ap_bssid_black_list"

    invoke-static {v0, v2, v1, p2}, Lcom/miui/enterprise/settings/EnterpriseSettings;->putString(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;I)V

    .line 220
    invoke-direct {p0}, Lcom/miui/server/enterprise/DeviceManagerService;->rebootWifi()V

    .line 221
    return-void
.end method

.method public setWifiApBssidWhiteList(Ljava/util/List;I)V
    .locals 3
    .param p2, "userId"    # I
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List<",
            "Ljava/lang/String;",
            ">;I)V"
        }
    .end annotation

    .line 201
    .local p1, "bssid":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;"
    iget-object v0, p0, Lcom/miui/server/enterprise/DeviceManagerService;->mContext:Landroid/content/Context;

    invoke-static {v0}, Lcom/miui/server/enterprise/ServiceUtils;->checkPermission(Landroid/content/Context;)V

    .line 202
    iget-object v0, p0, Lcom/miui/server/enterprise/DeviceManagerService;->mContext:Landroid/content/Context;

    .line 203
    invoke-static {p1}, Lcom/miui/enterprise/settings/EnterpriseSettings;->generateListSettings(Ljava/util/List;)Ljava/lang/String;

    move-result-object v1

    .line 202
    const-string v2, "ep_wifi_ap_bssid_white_list"

    invoke-static {v0, v2, v1, p2}, Lcom/miui/enterprise/settings/EnterpriseSettings;->putString(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;I)V

    .line 204
    invoke-direct {p0}, Lcom/miui/server/enterprise/DeviceManagerService;->rebootWifi()V

    .line 205
    return-void
.end method

.method public setWifiApSsidBlackList(Ljava/util/List;I)V
    .locals 3
    .param p2, "userId"    # I
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List<",
            "Ljava/lang/String;",
            ">;I)V"
        }
    .end annotation

    .line 209
    .local p1, "ssids":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;"
    iget-object v0, p0, Lcom/miui/server/enterprise/DeviceManagerService;->mContext:Landroid/content/Context;

    invoke-static {v0}, Lcom/miui/server/enterprise/ServiceUtils;->checkPermission(Landroid/content/Context;)V

    .line 210
    iget-object v0, p0, Lcom/miui/server/enterprise/DeviceManagerService;->mContext:Landroid/content/Context;

    .line 211
    invoke-static {p1}, Lcom/miui/enterprise/settings/EnterpriseSettings;->generateListSettings(Ljava/util/List;)Ljava/lang/String;

    move-result-object v1

    .line 210
    const-string v2, "ep_wifi_ap_ssid_black_list"

    invoke-static {v0, v2, v1, p2}, Lcom/miui/enterprise/settings/EnterpriseSettings;->putString(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;I)V

    .line 212
    invoke-direct {p0}, Lcom/miui/server/enterprise/DeviceManagerService;->rebootWifi()V

    .line 213
    return-void
.end method

.method public setWifiApSsidWhiteList(Ljava/util/List;I)V
    .locals 3
    .param p2, "userId"    # I
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List<",
            "Ljava/lang/String;",
            ">;I)V"
        }
    .end annotation

    .line 193
    .local p1, "ssids":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;"
    iget-object v0, p0, Lcom/miui/server/enterprise/DeviceManagerService;->mContext:Landroid/content/Context;

    invoke-static {v0}, Lcom/miui/server/enterprise/ServiceUtils;->checkPermission(Landroid/content/Context;)V

    .line 194
    iget-object v0, p0, Lcom/miui/server/enterprise/DeviceManagerService;->mContext:Landroid/content/Context;

    .line 195
    invoke-static {p1}, Lcom/miui/enterprise/settings/EnterpriseSettings;->generateListSettings(Ljava/util/List;)Ljava/lang/String;

    move-result-object v1

    .line 194
    const-string v2, "ep_wifi_ap_ssid_white_list"

    invoke-static {v0, v2, v1, p2}, Lcom/miui/enterprise/settings/EnterpriseSettings;->putString(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;I)V

    .line 196
    invoke-direct {p0}, Lcom/miui/server/enterprise/DeviceManagerService;->rebootWifi()V

    .line 197
    return-void
.end method

.method public setWifiConnRestriction(II)V
    .locals 2
    .param p1, "mode"    # I
    .param p2, "userId"    # I

    .line 179
    iget-object v0, p0, Lcom/miui/server/enterprise/DeviceManagerService;->mContext:Landroid/content/Context;

    invoke-static {v0}, Lcom/miui/server/enterprise/ServiceUtils;->checkPermission(Landroid/content/Context;)V

    .line 180
    iget-object v0, p0, Lcom/miui/server/enterprise/DeviceManagerService;->mContext:Landroid/content/Context;

    const-string v1, "ep_wifi_conn_restriction_mode"

    invoke-static {v0, v1, p1, p2}, Lcom/miui/enterprise/settings/EnterpriseSettings;->putInt(Landroid/content/Context;Ljava/lang/String;II)V

    .line 181
    invoke-direct {p0}, Lcom/miui/server/enterprise/DeviceManagerService;->rebootWifi()V

    .line 182
    return-void
.end method
