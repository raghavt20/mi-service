public class com.miui.server.enterprise.ApplicationManagerService extends com.miui.enterprise.IApplicationManager$Stub {
	 /* .source "ApplicationManagerService.java" */
	 /* # static fields */
	 private static final java.lang.String ACTION_APP_RUNNING_BLOCK;
	 private static final java.lang.String PACKAGE_SECURITY_CORE;
	 private static final java.lang.String TAG;
	 /* # instance fields */
	 private com.android.server.am.ActivityManagerService mAMS;
	 private android.app.AppOpsManager mAppOpsManager;
	 private android.content.Context mContext;
	 private android.app.admin.IDevicePolicyManager mDevicePolicyManager;
	 private android.content.Intent mDisAllowRunningHandleIntent;
	 private com.android.server.pm.PackageManagerService$IPackageManagerImpl mPMS;
	 private com.miui.server.process.ProcessManagerInternal mPMSI;
	 /* # direct methods */
	 static com.android.server.pm.PackageManagerService$IPackageManagerImpl -$$Nest$fgetmPMS ( com.miui.server.enterprise.ApplicationManagerService p0 ) { //bridge//synthethic
		 /* .locals 0 */
		 p0 = this.mPMS;
	 } // .end method
	 com.miui.server.enterprise.ApplicationManagerService ( ) {
		 /* .locals 2 */
		 /* .param p1, "context" # Landroid/content/Context; */
		 /* .line 59 */
		 /* invoke-direct {p0}, Lcom/miui/enterprise/IApplicationManager$Stub;-><init>()V */
		 /* .line 60 */
		 this.mContext = p1;
		 /* .line 61 */
		 final String v0 = "package"; // const-string v0, "package"
		 android.os.ServiceManager .getService ( v0 );
		 /* check-cast v0, Lcom/android/server/pm/PackageManagerService$IPackageManagerImpl; */
		 this.mPMS = v0;
		 /* .line 62 */
		 final String v0 = "activity"; // const-string v0, "activity"
		 android.os.ServiceManager .getService ( v0 );
		 /* check-cast v0, Lcom/android/server/am/ActivityManagerService; */
		 this.mAMS = v0;
		 /* .line 63 */
		 /* const-class v0, Lcom/miui/server/process/ProcessManagerInternal; */
		 com.android.server.LocalServices .getService ( v0 );
		 /* check-cast v0, Lcom/miui/server/process/ProcessManagerInternal; */
		 this.mPMSI = v0;
		 /* .line 64 */
		 /* nop */
		 /* .line 65 */
		 final String v0 = "device_policy"; // const-string v0, "device_policy"
		 android.os.ServiceManager .getService ( v0 );
		 /* .line 64 */
		 android.app.admin.IDevicePolicyManager$Stub .asInterface ( v0 );
		 this.mDevicePolicyManager = v0;
		 /* .line 66 */
		 v0 = this.mContext;
		 final String v1 = "appops"; // const-string v1, "appops"
		 (( android.content.Context ) v0 ).getSystemService ( v1 ); // invoke-virtual {v0, v1}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;
		 /* check-cast v0, Landroid/app/AppOpsManager; */
		 this.mAppOpsManager = v0;
		 /* .line 67 */
		 /* new-instance v0, Landroid/content/Intent; */
		 final String v1 = "com.miui.securitycore.APP_RUNNING_BLOCK"; // const-string v1, "com.miui.securitycore.APP_RUNNING_BLOCK"
		 /* invoke-direct {v0, v1}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V */
		 this.mDisAllowRunningHandleIntent = v0;
		 /* .line 68 */
		 final String v1 = "com.miui.securitycore"; // const-string v1, "com.miui.securitycore"
		 (( android.content.Intent ) v0 ).setPackage ( v1 ); // invoke-virtual {v0, v1}, Landroid/content/Intent;->setPackage(Ljava/lang/String;)Landroid/content/Intent;
		 /* .line 69 */
		 v0 = this.mDisAllowRunningHandleIntent;
		 /* const/high16 v1, 0x10800000 */
		 (( android.content.Intent ) v0 ).setFlags ( v1 ); // invoke-virtual {v0, v1}, Landroid/content/Intent;->setFlags(I)Landroid/content/Intent;
		 /* .line 70 */
		 return;
	 } // .end method
	 private void forceCloseTask ( java.util.List p0, Integer p1 ) {
		 /* .locals 9 */
		 /* .param p2, "userId" # I */
		 /* .annotation system Ldalvik/annotation/Signature; */
		 /* value = { */
		 /* "(", */
		 /* "Ljava/util/List<", */
		 /* "Ljava/lang/String;", */
		 /* ">;I)V" */
		 /* } */
	 } // .end annotation
	 /* .line 322 */
	 /* .local p1, "packages":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;" */
	 v0 = this.mContext;
	 (( android.content.Context ) v0 ).getPackageManager ( ); // invoke-virtual {v0}, Landroid/content/Context;->getPackageManager()Landroid/content/pm/PackageManager;
	 /* .line 323 */
	 /* .local v0, "pm":Landroid/content/pm/PackageManager; */
	 v1 = this.mAMS;
	 /* const/16 v2, 0x3e9 */
	 int v3 = 0; // const/4 v3, 0x0
	 (( com.android.server.am.ActivityManagerService ) v1 ).getRecentTasks ( v2, v3, p2 ); // invoke-virtual {v1, v2, v3, p2}, Lcom/android/server/am/ActivityManagerService;->getRecentTasks(III)Landroid/content/pm/ParceledListSlice;
	 /* .line 324 */
	 /* .local v1, "slice":Landroid/content/pm/ParceledListSlice;, "Landroid/content/pm/ParceledListSlice<Landroid/app/ActivityManager$RecentTaskInfo;>;" */
	 int v2 = 0; // const/4 v2, 0x0
	 /* .line 325 */
	 /* .local v2, "tasks":Ljava/util/List;, "Ljava/util/List<Landroid/app/ActivityManager$RecentTaskInfo;>;" */
	 if ( v1 != null) { // if-eqz v1, :cond_0
		 /* .line 326 */
		 (( android.content.pm.ParceledListSlice ) v1 ).getList ( ); // invoke-virtual {v1}, Landroid/content/pm/ParceledListSlice;->getList()Ljava/util/List;
		 /* .line 328 */
	 } // :cond_0
		 v3 = 	 if ( v2 != null) { // if-eqz v2, :cond_4
		 if ( v3 != null) { // if-eqz v3, :cond_1
			 /* .line 331 */
		 } // :cond_1
	 v4 = 	 } // :goto_0
	 if ( v4 != null) { // if-eqz v4, :cond_3
		 /* check-cast v4, Landroid/app/ActivityManager$RecentTaskInfo; */
		 /* .line 332 */
		 /* .local v4, "info":Landroid/app/ActivityManager$RecentTaskInfo; */
		 /* invoke-direct {p0, v0, v4}, Lcom/miui/server/enterprise/ApplicationManagerService;->getResolveInfoFromTask(Landroid/content/pm/PackageManager;Landroid/app/ActivityManager$RecentTaskInfo;)Landroid/content/pm/ResolveInfo; */
		 /* .line 333 */
		 /* .local v5, "ri":Landroid/content/pm/ResolveInfo; */
		 if ( v5 != null) { // if-eqz v5, :cond_2
			 v6 = this.activityInfo;
			 v6 = this.packageName;
			 if ( v6 != null) { // if-eqz v6, :cond_2
				 /* .line 334 */
				 v6 = this.activityInfo;
				 v6 = this.packageName;
				 /* .line 335 */
				 v7 = 				 /* .local v6, "packageName":Ljava/lang/String; */
				 if ( v7 != null) { // if-eqz v7, :cond_2
					 /* .line 336 */
					 v7 = this.mAMS;
					 /* iget v8, v4, Landroid/app/ActivityManager$RecentTaskInfo;->persistentId:I */
					 (( com.android.server.am.ActivityManagerService ) v7 ).removeTask ( v8 ); // invoke-virtual {v7, v8}, Lcom/android/server/am/ActivityManagerService;->removeTask(I)Z
					 /* .line 339 */
				 } // .end local v4 # "info":Landroid/app/ActivityManager$RecentTaskInfo;
			 } // .end local v5 # "ri":Landroid/content/pm/ResolveInfo;
		 } // .end local v6 # "packageName":Ljava/lang/String;
	 } // :cond_2
	 /* .line 340 */
} // :cond_3
return;
/* .line 329 */
} // :cond_4
} // :goto_1
return;
} // .end method
private java.util.Set getAccessibilityServiceFromPackage ( java.lang.String p0 ) {
/* .locals 8 */
/* .param p1, "pkgName" # Ljava/lang/String; */
/* .annotation system Ldalvik/annotation/Signature; */
/* value = { */
/* "(", */
/* "Ljava/lang/String;", */
/* ")", */
/* "Ljava/util/Set<", */
/* "Landroid/content/ComponentName;", */
/* ">;" */
/* } */
} // .end annotation
/* .line 389 */
/* new-instance v0, Ljava/util/HashSet; */
/* invoke-direct {v0}, Ljava/util/HashSet;-><init>()V */
/* .line 390 */
/* .local v0, "services":Ljava/util/Set;, "Ljava/util/Set<Landroid/content/ComponentName;>;" */
/* new-instance v1, Landroid/content/Intent; */
final String v2 = "android.accessibilityservice.AccessibilityService"; // const-string v2, "android.accessibilityservice.AccessibilityService"
/* invoke-direct {v1, v2}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V */
/* .line 391 */
/* .local v1, "accessibilityIntent":Landroid/content/Intent; */
(( android.content.Intent ) v1 ).setPackage ( p1 ); // invoke-virtual {v1, p1}, Landroid/content/Intent;->setPackage(Ljava/lang/String;)Landroid/content/Intent;
/* .line 392 */
v2 = this.mContext;
(( android.content.Context ) v2 ).getPackageManager ( ); // invoke-virtual {v2}, Landroid/content/Context;->getPackageManager()Landroid/content/pm/PackageManager;
int v3 = 4; // const/4 v3, 0x4
(( android.content.pm.PackageManager ) v2 ).queryIntentServices ( v1, v3 ); // invoke-virtual {v2, v1, v3}, Landroid/content/pm/PackageManager;->queryIntentServices(Landroid/content/Intent;I)Ljava/util/List;
/* .line 393 */
/* .local v2, "resolveInfos":Ljava/util/List;, "Ljava/util/List<Landroid/content/pm/ResolveInfo;>;" */
v4 = } // :goto_0
if ( v4 != null) { // if-eqz v4, :cond_1
/* check-cast v4, Landroid/content/pm/ResolveInfo; */
/* .line 394 */
/* .local v4, "resolveInfo":Landroid/content/pm/ResolveInfo; */
v5 = this.serviceInfo;
if ( v5 != null) { // if-eqz v5, :cond_0
/* .line 395 */
/* new-instance v5, Landroid/content/ComponentName; */
v6 = this.serviceInfo;
v6 = this.packageName;
v7 = this.serviceInfo;
v7 = this.name;
/* invoke-direct {v5, v6, v7}, Landroid/content/ComponentName;-><init>(Ljava/lang/String;Ljava/lang/String;)V */
/* .line 397 */
} // .end local v4 # "resolveInfo":Landroid/content/pm/ResolveInfo;
} // :cond_0
/* .line 398 */
} // :cond_1
} // .end method
private android.content.pm.ResolveInfo getResolveInfoFromTask ( android.content.pm.PackageManager p0, android.app.ActivityManager$RecentTaskInfo p1 ) {
/* .locals 3 */
/* .param p1, "packageManager" # Landroid/content/pm/PackageManager; */
/* .param p2, "recentInfo" # Landroid/app/ActivityManager$RecentTaskInfo; */
/* .line 343 */
/* new-instance v0, Landroid/content/Intent; */
v1 = this.baseIntent;
/* invoke-direct {v0, v1}, Landroid/content/Intent;-><init>(Landroid/content/Intent;)V */
/* .line 344 */
/* .local v0, "intent":Landroid/content/Intent; */
v1 = this.origActivity;
if ( v1 != null) { // if-eqz v1, :cond_0
/* .line 345 */
v1 = this.origActivity;
(( android.content.Intent ) v0 ).setComponent ( v1 ); // invoke-virtual {v0, v1}, Landroid/content/Intent;->setComponent(Landroid/content/ComponentName;)Landroid/content/Intent;
/* .line 347 */
} // :cond_0
v1 = (( android.content.Intent ) v0 ).getFlags ( ); // invoke-virtual {v0}, Landroid/content/Intent;->getFlags()I
/* const v2, -0x200001 */
/* and-int/2addr v1, v2 */
/* const/high16 v2, 0x10000000 */
/* or-int/2addr v1, v2 */
(( android.content.Intent ) v0 ).setFlags ( v1 ); // invoke-virtual {v0, v1}, Landroid/content/Intent;->setFlags(I)Landroid/content/Intent;
/* .line 349 */
int v1 = 0; // const/4 v1, 0x0
(( android.content.pm.PackageManager ) p1 ).resolveActivity ( v0, v1 ); // invoke-virtual {p1, v0, v1}, Landroid/content/pm/PackageManager;->resolveActivity(Landroid/content/Intent;I)Landroid/content/pm/ResolveInfo;
} // .end method
private java.util.Set readEnabeledAccessibilityService ( ) {
/* .locals 5 */
/* .annotation system Ldalvik/annotation/Signature; */
/* value = { */
/* "()", */
/* "Ljava/util/Set<", */
/* "Landroid/content/ComponentName;", */
/* ">;" */
/* } */
} // .end annotation
/* .line 402 */
v0 = this.mContext;
(( android.content.Context ) v0 ).getContentResolver ( ); // invoke-virtual {v0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;
final String v1 = "enabled_accessibility_services"; // const-string v1, "enabled_accessibility_services"
android.provider.Settings$Secure .getString ( v0,v1 );
/* .line 404 */
/* .local v0, "enabledServicesSetting":Ljava/lang/String; */
/* if-nez v0, :cond_0 */
/* .line 405 */
int v1 = 0; // const/4 v1, 0x0
/* .line 407 */
} // :cond_0
/* new-instance v1, Ljava/util/HashSet; */
/* invoke-direct {v1}, Ljava/util/HashSet;-><init>()V */
/* .line 408 */
/* .local v1, "enabledServices":Ljava/util/Set;, "Ljava/util/Set<Landroid/content/ComponentName;>;" */
/* new-instance v2, Landroid/text/TextUtils$SimpleStringSplitter; */
/* const/16 v3, 0x3a */
/* invoke-direct {v2, v3}, Landroid/text/TextUtils$SimpleStringSplitter;-><init>(C)V */
/* .line 409 */
/* .local v2, "stringSplitter":Landroid/text/TextUtils$SimpleStringSplitter; */
(( android.text.TextUtils$SimpleStringSplitter ) v2 ).setString ( v0 ); // invoke-virtual {v2, v0}, Landroid/text/TextUtils$SimpleStringSplitter;->setString(Ljava/lang/String;)V
/* .line 411 */
} // :goto_0
v3 = (( android.text.TextUtils$SimpleStringSplitter ) v2 ).hasNext ( ); // invoke-virtual {v2}, Landroid/text/TextUtils$SimpleStringSplitter;->hasNext()Z
if ( v3 != null) { // if-eqz v3, :cond_2
/* .line 412 */
(( android.text.TextUtils$SimpleStringSplitter ) v2 ).next ( ); // invoke-virtual {v2}, Landroid/text/TextUtils$SimpleStringSplitter;->next()Ljava/lang/String;
/* .line 413 */
/* .local v3, "enabledServiceName":Ljava/lang/String; */
android.content.ComponentName .unflattenFromString ( v3 );
/* .line 414 */
/* .local v4, "enabledComponent":Landroid/content/ComponentName; */
if ( v4 != null) { // if-eqz v4, :cond_1
/* .line 415 */
/* .line 417 */
} // .end local v3 # "enabledServiceName":Ljava/lang/String;
} // .end local v4 # "enabledComponent":Landroid/content/ComponentName;
} // :cond_1
/* .line 418 */
} // :cond_2
} // .end method
private void restoreAppRunningControl ( Integer p0 ) {
/* .locals 3 */
/* .param p1, "userId" # I */
/* .line 78 */
(( com.miui.server.enterprise.ApplicationManagerService ) p0 ).getDisallowedRunningAppList ( p1 ); // invoke-virtual {p0, p1}, Lcom/miui/server/enterprise/ApplicationManagerService;->getDisallowedRunningAppList(I)Ljava/util/List;
/* .line 79 */
/* .local v0, "blackList":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;" */
v1 = if ( v0 != null) { // if-eqz v0, :cond_1
/* if-nez v1, :cond_0 */
/* .line 83 */
} // :cond_0
miui.security.AppRunningControlManager .getInstance ( );
int v2 = 1; // const/4 v2, 0x1
(( miui.security.AppRunningControlManager ) v1 ).setBlackListEnable ( v2 ); // invoke-virtual {v1, v2}, Lmiui/security/AppRunningControlManager;->setBlackListEnable(Z)V
/* .line 84 */
miui.security.AppRunningControlManager .getInstance ( );
v2 = this.mDisAllowRunningHandleIntent;
(( miui.security.AppRunningControlManager ) v1 ).setDisallowRunningList ( v0, v2 ); // invoke-virtual {v1, v0, v2}, Lmiui/security/AppRunningControlManager;->setDisallowRunningList(Ljava/util/List;Landroid/content/Intent;)V
/* .line 85 */
return;
/* .line 80 */
} // :cond_1
} // :goto_0
miui.security.AppRunningControlManager .getInstance ( );
int v2 = 0; // const/4 v2, 0x0
(( miui.security.AppRunningControlManager ) v1 ).setBlackListEnable ( v2 ); // invoke-virtual {v1, v2}, Lmiui/security/AppRunningControlManager;->setBlackListEnable(Z)V
/* .line 81 */
return;
} // .end method
/* # virtual methods */
public void addTrustedAppStore ( java.util.List p0, Integer p1 ) {
/* .locals 3 */
/* .param p2, "userId" # I */
/* .annotation system Ldalvik/annotation/Signature; */
/* value = { */
/* "(", */
/* "Ljava/util/List<", */
/* "Ljava/lang/String;", */
/* ">;I)V" */
/* } */
} // .end annotation
/* .line 435 */
/* .local p1, "packages":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;" */
v0 = this.mContext;
com.miui.server.enterprise.ServiceUtils .checkPermission ( v0 );
/* .line 436 */
v0 = this.mContext;
/* .line 437 */
com.miui.enterprise.settings.EnterpriseSettings .generateListSettings ( p1 );
/* .line 436 */
final String v2 = "ep_trusted_app_stores"; // const-string v2, "ep_trusted_app_stores"
com.miui.enterprise.settings.EnterpriseSettings .putString ( v0,v2,v1,p2 );
/* .line 438 */
return;
} // .end method
public void bootComplete ( ) {
/* .locals 2 */
/* .line 73 */
final String v0 = "Enterprise-App"; // const-string v0, "Enterprise-App"
final String v1 = "ApplicationManagerService init"; // const-string v1, "ApplicationManagerService init"
android.util.Slog .d ( v0,v1 );
/* .line 74 */
int v0 = 0; // const/4 v0, 0x0
/* invoke-direct {p0, v0}, Lcom/miui/server/enterprise/ApplicationManagerService;->restoreAppRunningControl(I)V */
/* .line 75 */
return;
} // .end method
public void clearApplicationCache ( java.lang.String p0, Integer p1 ) {
/* .locals 2 */
/* .param p1, "packageName" # Ljava/lang/String; */
/* .param p2, "userId" # I */
/* .line 429 */
v0 = this.mContext;
com.miui.server.enterprise.ServiceUtils .checkPermission ( v0 );
/* .line 430 */
v0 = this.mPMS;
int v1 = 0; // const/4 v1, 0x0
(( com.android.server.pm.PackageManagerService$IPackageManagerImpl ) v0 ).deleteApplicationCacheFilesAsUser ( p1, p2, v1 ); // invoke-virtual {v0, p1, p2, v1}, Lcom/android/server/pm/PackageManagerService$IPackageManagerImpl;->deleteApplicationCacheFilesAsUser(Ljava/lang/String;ILandroid/content/pm/IPackageDataObserver;)V
/* .line 431 */
return;
} // .end method
public void clearApplicationUserData ( java.lang.String p0, Integer p1 ) {
/* .locals 2 */
/* .param p1, "packageName" # Ljava/lang/String; */
/* .param p2, "userId" # I */
/* .line 423 */
v0 = this.mContext;
com.miui.server.enterprise.ServiceUtils .checkPermission ( v0 );
/* .line 424 */
v0 = this.mPMS;
int v1 = 0; // const/4 v1, 0x0
(( com.android.server.pm.PackageManagerService$IPackageManagerImpl ) v0 ).clearApplicationUserData ( p1, v1, p2 ); // invoke-virtual {v0, p1, v1, p2}, Lcom/android/server/pm/PackageManagerService$IPackageManagerImpl;->clearApplicationUserData(Ljava/lang/String;Landroid/content/pm/IPackageDataObserver;I)V
/* .line 425 */
return;
} // .end method
public void deletePackage ( java.lang.String p0, Integer p1, com.miui.enterprise.sdk.IEpDeletePackageObserver p2, Integer p3 ) {
/* .locals 2 */
/* .param p1, "packageName" # Ljava/lang/String; */
/* .param p2, "flags" # I */
/* .param p3, "observer" # Lcom/miui/enterprise/sdk/IEpDeletePackageObserver; */
/* .param p4, "userId" # I */
/* .line 95 */
v0 = this.mContext;
com.miui.server.enterprise.ServiceUtils .checkPermission ( v0 );
/* .line 96 */
/* new-instance v0, Ljava/lang/StringBuilder; */
/* invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V */
final String v1 = "delete package "; // const-string v1, "delete package "
(( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v0 ).append ( p1 ); // invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v0 ).toString ( ); // invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
final String v1 = "Enterprise-App"; // const-string v1, "Enterprise-App"
android.util.Slog .d ( v1,v0 );
/* .line 97 */
/* new-instance v0, Lcom/miui/server/enterprise/ApplicationManagerService$1; */
/* invoke-direct {v0, p0, p3}, Lcom/miui/server/enterprise/ApplicationManagerService$1;-><init>(Lcom/miui/server/enterprise/ApplicationManagerService;Lcom/miui/enterprise/sdk/IEpDeletePackageObserver;)V */
/* .line 103 */
/* .local v0, "deleteObserver":Landroid/content/pm/IPackageDeleteObserver; */
v1 = this.mContext;
(( android.content.Context ) v1 ).getPackageManager ( ); // invoke-virtual {v1}, Landroid/content/Context;->getPackageManager()Landroid/content/pm/PackageManager;
(( android.content.pm.PackageManager ) v1 ).deletePackageAsUser ( p1, v0, p2, p4 ); // invoke-virtual {v1, p1, v0, p2, p4}, Landroid/content/pm/PackageManager;->deletePackageAsUser(Ljava/lang/String;Landroid/content/pm/IPackageDeleteObserver;II)V
/* .line 104 */
return;
} // .end method
public void enableAccessibilityService ( android.content.ComponentName p0, Boolean p1 ) {
/* .locals 7 */
/* .param p1, "componentName" # Landroid/content/ComponentName; */
/* .param p2, "enabled" # Z */
/* .line 360 */
v0 = this.mContext;
com.miui.server.enterprise.ServiceUtils .checkPermission ( v0 );
/* .line 361 */
(( android.content.ComponentName ) p1 ).getPackageName ( ); // invoke-virtual {p1}, Landroid/content/ComponentName;->getPackageName()Ljava/lang/String;
/* invoke-direct {p0, v0}, Lcom/miui/server/enterprise/ApplicationManagerService;->getAccessibilityServiceFromPackage(Ljava/lang/String;)Ljava/util/Set; */
/* .line 362 */
/* .local v0, "services":Ljava/util/Set;, "Ljava/util/Set<Landroid/content/ComponentName;>;" */
/* invoke-direct {p0}, Lcom/miui/server/enterprise/ApplicationManagerService;->readEnabeledAccessibilityService()Ljava/util/Set; */
/* .line 363 */
/* .local v1, "enabledServices":Ljava/util/Set;, "Ljava/util/Set<Landroid/content/ComponentName;>;" */
/* if-nez v1, :cond_0 */
/* .line 364 */
/* new-instance v2, Ljava/util/HashSet; */
/* invoke-direct {v2}, Ljava/util/HashSet;-><init>()V */
/* move-object v1, v2 */
/* .line 366 */
} // :cond_0
if ( p2 != null) { // if-eqz p2, :cond_1
/* .line 367 */
/* .line 369 */
} // :cond_1
/* .line 372 */
} // :goto_0
/* new-instance v2, Ljava/lang/StringBuilder; */
/* invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V */
/* .line 373 */
/* .local v2, "enabledServicesBuilder":Ljava/lang/StringBuilder; */
v4 = } // :goto_1
if ( v4 != null) { // if-eqz v4, :cond_2
/* check-cast v4, Landroid/content/ComponentName; */
/* .line 374 */
/* .local v4, "enabledService":Landroid/content/ComponentName; */
(( android.content.ComponentName ) v4 ).flattenToString ( ); // invoke-virtual {v4}, Landroid/content/ComponentName;->flattenToString()Ljava/lang/String;
(( java.lang.StringBuilder ) v2 ).append ( v5 ); // invoke-virtual {v2, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
/* .line 375 */
/* const/16 v5, 0x3a */
(( java.lang.StringBuilder ) v2 ).append ( v5 ); // invoke-virtual {v2, v5}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;
/* .line 376 */
} // .end local v4 # "enabledService":Landroid/content/ComponentName;
/* .line 377 */
} // :cond_2
v3 = (( java.lang.StringBuilder ) v2 ).length ( ); // invoke-virtual {v2}, Ljava/lang/StringBuilder;->length()I
/* .line 378 */
/* .local v3, "enabledServicesBuilderLength":I */
/* if-lez v3, :cond_3 */
/* .line 379 */
/* add-int/lit8 v4, v3, -0x1 */
(( java.lang.StringBuilder ) v2 ).deleteCharAt ( v4 ); // invoke-virtual {v2, v4}, Ljava/lang/StringBuilder;->deleteCharAt(I)Ljava/lang/StringBuilder;
/* .line 381 */
} // :cond_3
v4 = this.mContext;
(( android.content.Context ) v4 ).getContentResolver ( ); // invoke-virtual {v4}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;
/* .line 383 */
(( java.lang.StringBuilder ) v2 ).toString ( ); // invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
/* .line 381 */
final String v6 = "enabled_accessibility_services"; // const-string v6, "enabled_accessibility_services"
android.provider.Settings$Secure .putString ( v4,v6,v5 );
/* .line 384 */
v4 = this.mContext;
(( android.content.Context ) v4 ).getContentResolver ( ); // invoke-virtual {v4}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;
v5 = /* .line 385 */
/* .line 384 */
/* xor-int/lit8 v5, v5, 0x1 */
final String v6 = "accessibility_enabled"; // const-string v6, "accessibility_enabled"
android.provider.Settings$Secure .putInt ( v4,v6,v5 );
/* .line 386 */
return;
} // .end method
public void enableNotifications ( java.lang.String p0, Boolean p1 ) {
/* .locals 1 */
/* .param p1, "pkg" # Ljava/lang/String; */
/* .param p2, "enabled" # Z */
/* .line 471 */
v0 = this.mContext;
com.miui.server.enterprise.ServiceUtils .checkPermission ( v0 );
/* .line 472 */
v0 = this.mContext;
miui.util.NotificationFilterHelper .enableNotifications ( v0,p1,p2 );
/* .line 473 */
return;
} // .end method
public void enableTrustedAppStore ( Boolean p0, Integer p1 ) {
/* .locals 2 */
/* .param p1, "enabled" # Z */
/* .param p2, "userId" # I */
/* .line 449 */
v0 = this.mContext;
com.miui.server.enterprise.ServiceUtils .checkPermission ( v0 );
/* .line 450 */
v0 = this.mContext;
/* .line 451 */
/* nop */
/* .line 450 */
final String v1 = "ep_trusted_app_store_enabled"; // const-string v1, "ep_trusted_app_store_enabled"
com.miui.enterprise.settings.EnterpriseSettings .putInt ( v0,v1,p1,p2 );
/* .line 452 */
return;
} // .end method
public java.util.List getApplicationBlackList ( Integer p0 ) {
/* .locals 2 */
/* .param p1, "userId" # I */
/* .annotation system Ldalvik/annotation/Signature; */
/* value = { */
/* "(I)", */
/* "Ljava/util/List<", */
/* "Ljava/lang/String;", */
/* ">;" */
/* } */
} // .end annotation
/* .line 256 */
v0 = this.mContext;
com.miui.server.enterprise.ServiceUtils .checkPermission ( v0 );
/* .line 257 */
v0 = this.mContext;
final String v1 = "ep_app_black_list"; // const-string v1, "ep_app_black_list"
com.miui.enterprise.settings.EnterpriseSettings .getString ( v0,v1,p1 );
/* .line 259 */
/* .local v0, "savedStr":Ljava/lang/String; */
com.miui.enterprise.settings.EnterpriseSettings .parseListSettings ( v0 );
} // .end method
public Integer getApplicationRestriction ( Integer p0 ) {
/* .locals 3 */
/* .param p1, "userId" # I */
/* .line 286 */
v0 = this.mContext;
com.miui.server.enterprise.ServiceUtils .checkPermission ( v0 );
/* .line 287 */
v0 = this.mContext;
final String v1 = "ep_app_restriction_mode"; // const-string v1, "ep_app_restriction_mode"
int v2 = 0; // const/4 v2, 0x0
v0 = com.miui.enterprise.settings.EnterpriseSettings .getInt ( v0,v1,v2,p1 );
} // .end method
public Integer getApplicationSettings ( java.lang.String p0, Integer p1 ) {
/* .locals 3 */
/* .param p1, "packageName" # Ljava/lang/String; */
/* .param p2, "userId" # I */
/* .line 215 */
v0 = this.mContext;
com.miui.server.enterprise.ServiceUtils .checkPermission ( v0 );
/* .line 216 */
v0 = this.mContext;
com.miui.enterprise.ApplicationHelper .buildPackageSettingKey ( p1 );
int v2 = 0; // const/4 v2, 0x0
v0 = com.miui.enterprise.settings.EnterpriseSettings .getInt ( v0,v1,v2,p2 );
} // .end method
public java.util.List getApplicationWhiteList ( Integer p0 ) {
/* .locals 2 */
/* .param p1, "userId" # I */
/* .annotation system Ldalvik/annotation/Signature; */
/* value = { */
/* "(I)", */
/* "Ljava/util/List<", */
/* "Ljava/lang/String;", */
/* ">;" */
/* } */
} // .end annotation
/* .line 272 */
v0 = this.mContext;
com.miui.server.enterprise.ServiceUtils .checkPermission ( v0 );
/* .line 273 */
v0 = this.mContext;
final String v1 = "ep_app_white_list"; // const-string v1, "ep_app_white_list"
com.miui.enterprise.settings.EnterpriseSettings .getString ( v0,v1,p1 );
/* .line 275 */
/* .local v0, "savedStr":Ljava/lang/String; */
com.miui.enterprise.settings.EnterpriseSettings .parseListSettings ( v0 );
} // .end method
public java.util.List getDisallowedRunningAppList ( Integer p0 ) {
/* .locals 2 */
/* .param p1, "userId" # I */
/* .annotation system Ldalvik/annotation/Signature; */
/* value = { */
/* "(I)", */
/* "Ljava/util/List<", */
/* "Ljava/lang/String;", */
/* ">;" */
/* } */
} // .end annotation
/* .line 316 */
v0 = this.mContext;
com.miui.server.enterprise.ServiceUtils .checkPermission ( v0 );
/* .line 317 */
v0 = this.mContext;
final String v1 = "ep_app_disallow_running_list"; // const-string v1, "ep_app_disallow_running_list"
com.miui.enterprise.settings.EnterpriseSettings .getString ( v0,v1,p1 );
com.miui.enterprise.settings.EnterpriseSettings .parseListSettings ( v0 );
} // .end method
public java.util.List getTrustedAppStore ( Integer p0 ) {
/* .locals 2 */
/* .param p1, "userId" # I */
/* .annotation system Ldalvik/annotation/Signature; */
/* value = { */
/* "(I)", */
/* "Ljava/util/List<", */
/* "Ljava/lang/String;", */
/* ">;" */
/* } */
} // .end annotation
/* .line 442 */
v0 = this.mContext;
com.miui.server.enterprise.ServiceUtils .checkPermission ( v0 );
/* .line 443 */
v0 = this.mContext;
/* .line 444 */
final String v1 = "ep_trusted_app_stores"; // const-string v1, "ep_trusted_app_stores"
com.miui.enterprise.settings.EnterpriseSettings .getString ( v0,v1,p1 );
/* .line 443 */
com.miui.enterprise.settings.EnterpriseSettings .parseListSettings ( v0 );
} // .end method
public java.util.List getXSpaceBlack ( ) {
/* .locals 2 */
/* .annotation system Ldalvik/annotation/Signature; */
/* value = { */
/* "()", */
/* "Ljava/util/List<", */
/* "Ljava/lang/String;", */
/* ">;" */
/* } */
} // .end annotation
/* .line 504 */
v0 = this.mContext;
com.miui.server.enterprise.ServiceUtils .checkPermission ( v0 );
/* .line 505 */
v0 = this.mContext;
final String v1 = "ep_app_black_xsapce"; // const-string v1, "ep_app_black_xsapce"
com.miui.enterprise.settings.EnterpriseSettings .getString ( v0,v1 );
/* .line 507 */
/* .local v0, "savedStr":Ljava/lang/String; */
com.miui.enterprise.settings.EnterpriseSettings .parseListSettings ( v0 );
} // .end method
public void grantRuntimePermission ( java.lang.String p0, java.lang.String p1, Integer p2 ) {
/* .locals 4 */
/* .param p1, "packageName" # Ljava/lang/String; */
/* .param p2, "permission" # Ljava/lang/String; */
/* .param p3, "userId" # I */
/* .line 512 */
v0 = this.mContext;
com.miui.server.enterprise.ServiceUtils .checkPermission ( v0 );
/* .line 513 */
v0 = this.mContext;
(( android.content.Context ) v0 ).getPackageManager ( ); // invoke-virtual {v0}, Landroid/content/Context;->getPackageManager()Landroid/content/pm/PackageManager;
/* .line 514 */
/* .local v0, "pm":Landroid/content/pm/PackageManager; */
int v1 = 1; // const/4 v1, 0x1
v1 = (( android.content.pm.PackageManager ) v0 ).addWhitelistedRestrictedPermission ( p1, p2, v1 ); // invoke-virtual {v0, p1, p2, v1}, Landroid/content/pm/PackageManager;->addWhitelistedRestrictedPermission(Ljava/lang/String;Ljava/lang/String;I)Z
/* .line 516 */
/* .local v1, "grantPermission":Z */
/* new-instance v2, Ljava/lang/StringBuilder; */
/* invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V */
final String v3 = "add restricted permission white list:"; // const-string v3, "add restricted permission white list:"
(( java.lang.StringBuilder ) v2 ).append ( v3 ); // invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v2 ).append ( p2 ); // invoke-virtual {v2, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
final String v3 = ",success?="; // const-string v3, ",success?="
(( java.lang.StringBuilder ) v2 ).append ( v3 ); // invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v2 ).append ( v1 ); // invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v2 ).toString ( ); // invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
final String v3 = "Enterprise-App"; // const-string v3, "Enterprise-App"
android.util.Slog .d ( v3,v2 );
/* .line 517 */
v2 = this.mContext;
(( android.content.Context ) v2 ).getPackageManager ( ); // invoke-virtual {v2}, Landroid/content/Context;->getPackageManager()Landroid/content/pm/PackageManager;
v3 = android.os.UserHandle.OWNER;
(( android.content.pm.PackageManager ) v2 ).grantRuntimePermission ( p1, p2, v3 ); // invoke-virtual {v2, p1, p2, v3}, Landroid/content/pm/PackageManager;->grantRuntimePermission(Ljava/lang/String;Ljava/lang/String;Landroid/os/UserHandle;)V
/* .line 518 */
return;
} // .end method
public void installPackage ( java.lang.String p0, Integer p1, com.miui.enterprise.sdk.IEpInstallPackageObserver p2, Integer p3 ) {
/* .locals 9 */
/* .param p1, "path" # Ljava/lang/String; */
/* .param p2, "flags" # I */
/* .param p3, "observer" # Lcom/miui/enterprise/sdk/IEpInstallPackageObserver; */
/* .param p4, "userId" # I */
/* .line 89 */
v0 = this.mContext;
com.miui.server.enterprise.ServiceUtils .checkPermission ( v0 );
/* .line 90 */
/* new-instance v0, Ljava/lang/StringBuilder; */
/* invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V */
final String v1 = "install package "; // const-string v1, "install package "
(( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v0 ).append ( p1 ); // invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v0 ).toString ( ); // invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
final String v1 = "Enterprise-App"; // const-string v1, "Enterprise-App"
android.util.Slog .d ( v1,v0 );
/* .line 91 */
v2 = this.mContext;
v3 = this.mPMS;
final String v7 = "Enterprise"; // const-string v7, "Enterprise"
/* move-object v4, p1 */
/* move-object v5, p3 */
/* move v6, p2 */
/* move v8, p4 */
/* invoke-static/range {v2 ..v8}, Lcom/miui/server/enterprise/ApplicationManagerServiceProxy;->installPackageAsUser(Landroid/content/Context;Lcom/android/server/pm/PackageManagerService$IPackageManagerImpl;Ljava/lang/String;Lcom/miui/enterprise/sdk/IEpInstallPackageObserver;ILjava/lang/String;I)V */
/* .line 92 */
return;
} // .end method
public void installPackageWithPendingIntent ( java.lang.String p0, android.app.PendingIntent p1, Integer p2 ) {
/* .locals 8 */
/* .param p1, "path" # Ljava/lang/String; */
/* .param p2, "pendingIntent" # Landroid/app/PendingIntent; */
/* .param p3, "userId" # I */
/* .line 108 */
v0 = this.mContext;
com.miui.server.enterprise.ServiceUtils .checkPermission ( v0 );
/* .line 109 */
v1 = this.mContext;
v2 = this.mPMS;
/* new-instance v4, Lcom/miui/server/enterprise/ApplicationManagerService$2; */
/* invoke-direct {v4, p0, p3, p2}, Lcom/miui/server/enterprise/ApplicationManagerService$2;-><init>(Lcom/miui/server/enterprise/ApplicationManagerService;ILandroid/app/PendingIntent;)V */
int v5 = 2; // const/4 v5, 0x2
final String v6 = "Enterprise"; // const-string v6, "Enterprise"
/* move-object v3, p1 */
/* move v7, p3 */
/* invoke-static/range {v1 ..v7}, Lcom/miui/server/enterprise/ApplicationManagerServiceProxy;->installPackageAsUser(Landroid/content/Context;Lcom/android/server/pm/PackageManagerService$IPackageManagerImpl;Ljava/lang/String;Lcom/miui/enterprise/sdk/IEpInstallPackageObserver;ILjava/lang/String;I)V */
/* .line 149 */
return;
} // .end method
public Boolean isTrustedAppStoreEnabled ( Integer p0 ) {
/* .locals 3 */
/* .param p1, "userId" # I */
/* .line 456 */
v0 = this.mContext;
com.miui.server.enterprise.ServiceUtils .checkPermission ( v0 );
/* .line 457 */
v0 = this.mContext;
final String v1 = "ep_trusted_app_store_enabled"; // const-string v1, "ep_trusted_app_store_enabled"
int v2 = 0; // const/4 v2, 0x0
v0 = com.miui.enterprise.settings.EnterpriseSettings .getInt ( v0,v1,v2,p1 );
int v1 = 1; // const/4 v1, 0x1
/* if-ne v0, v1, :cond_0 */
/* move v2, v1 */
} // :cond_0
} // .end method
public void killProcess ( java.lang.String p0, Integer p1 ) {
/* .locals 2 */
/* .param p1, "packageName" # Ljava/lang/String; */
/* .param p2, "userId" # I */
/* .line 354 */
v0 = this.mContext;
com.miui.server.enterprise.ServiceUtils .checkPermission ( v0 );
/* .line 355 */
v0 = this.mPMSI;
final String v1 = "enterprise"; // const-string v1, "enterprise"
(( com.miui.server.process.ProcessManagerInternal ) v0 ).forceStopPackage ( p1, p2, v1 ); // invoke-virtual {v0, p1, p2, v1}, Lcom/miui/server/process/ProcessManagerInternal;->forceStopPackage(Ljava/lang/String;ILjava/lang/String;)V
/* .line 356 */
return;
} // .end method
public Boolean removeDeviceAdmin ( android.content.ComponentName p0, Integer p1 ) {
/* .locals 4 */
/* .param p1, "component" # Landroid/content/ComponentName; */
/* .param p2, "userId" # I */
/* .line 235 */
final String v0 = "Remove device admin["; // const-string v0, "Remove device admin["
final String v1 = "Enterprise-App"; // const-string v1, "Enterprise-App"
v2 = this.mContext;
com.miui.server.enterprise.ServiceUtils .checkPermission ( v2 );
/* .line 237 */
try { // :try_start_0
v2 = this.mDevicePolicyManager;
/* .line 238 */
/* new-instance v2, Ljava/lang/StringBuilder; */
/* invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V */
(( java.lang.StringBuilder ) v2 ).append ( v0 ); // invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v2 ).append ( p1 ); // invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;
final String v3 = "]"; // const-string v3, "]"
(( java.lang.StringBuilder ) v2 ).append ( v3 ); // invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v2 ).toString ( ); // invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
android.util.Slog .d ( v1,v2 );
/* :try_end_0 */
/* .catch Ljava/lang/Exception; {:try_start_0 ..:try_end_0} :catch_0 */
/* .line 239 */
int v0 = 1; // const/4 v0, 0x1
/* .line 240 */
/* :catch_0 */
/* move-exception v2 */
/* .line 241 */
/* .local v2, "e":Ljava/lang/Exception; */
/* new-instance v3, Ljava/lang/StringBuilder; */
/* invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V */
(( java.lang.StringBuilder ) v3 ).append ( v0 ); // invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v0 ).append ( p1 ); // invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;
final String v3 = "] failed"; // const-string v3, "] failed"
(( java.lang.StringBuilder ) v0 ).append ( v3 ); // invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v0 ).toString ( ); // invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
android.util.Slog .d ( v1,v0,v2 );
/* .line 242 */
int v0 = 0; // const/4 v0, 0x0
} // .end method
public void setApplicationBlackList ( java.util.List p0, Integer p1 ) {
/* .locals 3 */
/* .param p2, "userId" # I */
/* .annotation system Ldalvik/annotation/Signature; */
/* value = { */
/* "(", */
/* "Ljava/util/List<", */
/* "Ljava/lang/String;", */
/* ">;I)V" */
/* } */
} // .end annotation
/* .line 248 */
/* .local p1, "packages":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;" */
v0 = this.mContext;
com.miui.server.enterprise.ServiceUtils .checkPermission ( v0 );
/* .line 249 */
v0 = this.mContext;
/* .line 251 */
com.miui.enterprise.settings.EnterpriseSettings .generateListSettings ( p1 );
/* .line 249 */
final String v2 = "ep_app_black_list"; // const-string v2, "ep_app_black_list"
com.miui.enterprise.settings.EnterpriseSettings .putString ( v0,v2,v1,p2 );
/* .line 252 */
return;
} // .end method
public void setApplicationEnabled ( java.lang.String p0, Boolean p1, Integer p2 ) {
/* .locals 7 */
/* .param p1, "packageName" # Ljava/lang/String; */
/* .param p2, "enable" # Z */
/* .param p3, "userId" # I */
/* .line 463 */
v0 = this.mContext;
com.miui.server.enterprise.ServiceUtils .checkPermission ( v0 );
/* .line 464 */
v1 = this.mPMS;
/* .line 465 */
if ( p2 != null) { // if-eqz p2, :cond_0
int v0 = 0; // const/4 v0, 0x0
/* move v3, v0 */
/* .line 466 */
} // :cond_0
int v0 = 2; // const/4 v0, 0x2
/* move v3, v0 */
} // :goto_0
int v4 = 0; // const/4 v4, 0x0
final String v6 = "Enterprise"; // const-string v6, "Enterprise"
/* .line 464 */
/* move-object v2, p1 */
/* move v5, p3 */
/* invoke-virtual/range {v1 ..v6}, Lcom/android/server/pm/PackageManagerService$IPackageManagerImpl;->setApplicationEnabledSetting(Ljava/lang/String;IIILjava/lang/String;)V */
/* .line 467 */
return;
} // .end method
public void setApplicationRestriction ( Integer p0, Integer p1 ) {
/* .locals 2 */
/* .param p1, "mode" # I */
/* .param p2, "userId" # I */
/* .line 280 */
v0 = this.mContext;
com.miui.server.enterprise.ServiceUtils .checkPermission ( v0 );
/* .line 281 */
v0 = this.mContext;
final String v1 = "ep_app_restriction_mode"; // const-string v1, "ep_app_restriction_mode"
com.miui.enterprise.settings.EnterpriseSettings .putInt ( v0,v1,p1,p2 );
/* .line 282 */
return;
} // .end method
public void setApplicationSettings ( java.lang.String p0, Integer p1, Integer p2 ) {
/* .locals 16 */
/* .param p1, "packageName" # Ljava/lang/String; */
/* .param p2, "flags" # I */
/* .param p3, "userId" # I */
/* .line 153 */
/* move-object/from16 v1, p0 */
/* move-object/from16 v2, p1 */
/* move/from16 v3, p2 */
/* move/from16 v4, p3 */
v0 = this.mContext;
com.miui.server.enterprise.ServiceUtils .checkPermission ( v0 );
/* .line 154 */
v0 = /* invoke-static/range {p1 ..p1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z */
final String v5 = "Enterprise-App"; // const-string v5, "Enterprise-App"
/* if-nez v0, :cond_7 */
/* if-gez v3, :cond_0 */
/* goto/16 :goto_5 */
/* .line 158 */
} // :cond_0
v0 = this.mContext;
/* invoke-static/range {p1 ..p1}, Lcom/miui/enterprise/ApplicationHelper;->buildPackageSettingKey(Ljava/lang/String;)Ljava/lang/String; */
com.miui.enterprise.settings.EnterpriseSettings .putInt ( v0,v6,v3,v4 );
/* .line 160 */
/* and-int/lit8 v0, v3, 0x8 */
int v6 = 3; // const/4 v6, 0x3
final String v7 = "6"; // const-string v7, "6"
final String v8 = "content://com.lbe.security.miui.permmgr"; // const-string v8, "content://com.lbe.security.miui.permmgr"
final String v9 = "extra_package"; // const-string v9, "extra_package"
final String v10 = "extra_action"; // const-string v10, "extra_action"
/* const-wide/16 v11, 0x4000 */
final String v13 = "extra_permission"; // const-string v13, "extra_permission"
int v14 = 0; // const/4 v14, 0x0
if ( v0 != null) { // if-eqz v0, :cond_1
/* .line 161 */
/* new-instance v0, Ljava/lang/StringBuilder; */
/* invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V */
final String v15 = "allowed "; // const-string v15, "allowed "
(( java.lang.StringBuilder ) v0 ).append ( v15 ); // invoke-virtual {v0, v15}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v0 ).append ( v2 ); // invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
final String v15 = " auto start"; // const-string v15, " auto start"
(( java.lang.StringBuilder ) v0 ).append ( v15 ); // invoke-virtual {v0, v15}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v0 ).toString ( ); // invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
android.util.Slog .d ( v5,v0 );
/* .line 162 */
/* new-instance v0, Landroid/os/Bundle; */
/* invoke-direct {v0}, Landroid/os/Bundle;-><init>()V */
/* .line 163 */
/* .local v0, "extras":Landroid/os/Bundle; */
(( android.os.Bundle ) v0 ).putLong ( v13, v11, v12 ); // invoke-virtual {v0, v13, v11, v12}, Landroid/os/Bundle;->putLong(Ljava/lang/String;J)V
/* .line 164 */
(( android.os.Bundle ) v0 ).putInt ( v10, v6 ); // invoke-virtual {v0, v10, v6}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V
/* .line 165 */
/* filled-new-array/range {p1 ..p1}, [Ljava/lang/String; */
(( android.os.Bundle ) v0 ).putStringArray ( v9, v10 ); // invoke-virtual {v0, v9, v10}, Landroid/os/Bundle;->putStringArray(Ljava/lang/String;[Ljava/lang/String;)V
/* .line 166 */
v9 = this.mContext;
(( android.content.Context ) v9 ).getContentResolver ( ); // invoke-virtual {v9}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;
android.net.Uri .parse ( v8 );
(( android.content.ContentResolver ) v9 ).call ( v8, v7, v14, v0 ); // invoke-virtual {v9, v8, v7, v14, v0}, Landroid/content/ContentResolver;->call(Landroid/net/Uri;Ljava/lang/String;Ljava/lang/String;Landroid/os/Bundle;)Landroid/os/Bundle;
/* .line 168 */
} // .end local v0 # "extras":Landroid/os/Bundle;
int v11 = 0; // const/4 v11, 0x0
/* .line 169 */
} // :cond_1
/* new-instance v0, Landroid/os/Bundle; */
/* invoke-direct {v0}, Landroid/os/Bundle;-><init>()V */
/* .line 170 */
/* .restart local v0 # "extras":Landroid/os/Bundle; */
(( android.os.Bundle ) v0 ).putLong ( v13, v11, v12 ); // invoke-virtual {v0, v13, v11, v12}, Landroid/os/Bundle;->putLong(Ljava/lang/String;J)V
/* .line 171 */
int v11 = 0; // const/4 v11, 0x0
(( android.os.Bundle ) v0 ).putInt ( v10, v11 ); // invoke-virtual {v0, v10, v11}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V
/* .line 172 */
/* filled-new-array/range {p1 ..p1}, [Ljava/lang/String; */
(( android.os.Bundle ) v0 ).putStringArray ( v9, v10 ); // invoke-virtual {v0, v9, v10}, Landroid/os/Bundle;->putStringArray(Ljava/lang/String;[Ljava/lang/String;)V
/* .line 173 */
v9 = this.mContext;
(( android.content.Context ) v9 ).getContentResolver ( ); // invoke-virtual {v9}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;
android.net.Uri .parse ( v8 );
(( android.content.ContentResolver ) v9 ).call ( v8, v7, v14, v0 ); // invoke-virtual {v9, v8, v7, v14, v0}, Landroid/content/ContentResolver;->call(Landroid/net/Uri;Ljava/lang/String;Ljava/lang/String;Landroid/os/Bundle;)Landroid/os/Bundle;
/* .line 177 */
} // .end local v0 # "extras":Landroid/os/Bundle;
} // :goto_0
/* new-instance v0, Landroid/content/Intent; */
/* .line 178 */
final String v7 = "package"; // const-string v7, "package"
android.net.Uri .fromParts ( v7,v2,v14 );
final String v8 = "android.intent.action.PACKAGE_ADDED"; // const-string v8, "android.intent.action.PACKAGE_ADDED"
/* invoke-direct {v0, v8, v7}, Landroid/content/Intent;-><init>(Ljava/lang/String;Landroid/net/Uri;)V */
/* move-object v7, v0 */
/* .line 179 */
/* .local v7, "intent":Landroid/content/Intent; */
final String v0 = "android.intent.extra.user_handle"; // const-string v0, "android.intent.extra.user_handle"
(( android.content.Intent ) v7 ).putExtra ( v0, v4 ); // invoke-virtual {v7, v0, v4}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;
/* .line 180 */
final String v0 = "com.lbe.security.miui"; // const-string v0, "com.lbe.security.miui"
(( android.content.Intent ) v7 ).setPackage ( v0 ); // invoke-virtual {v7, v0}, Landroid/content/Intent;->setPackage(Ljava/lang/String;)Landroid/content/Intent;
/* .line 181 */
/* const/high16 v0, 0x10000000 */
(( android.content.Intent ) v7 ).addFlags ( v0 ); // invoke-virtual {v7, v0}, Landroid/content/Intent;->addFlags(I)Landroid/content/Intent;
/* .line 182 */
v0 = this.mContext;
/* new-instance v8, Landroid/os/UserHandle; */
/* invoke-direct {v8, v4}, Landroid/os/UserHandle;-><init>(I)V */
(( android.content.Context ) v0 ).sendBroadcastAsUser ( v7, v8 ); // invoke-virtual {v0, v7, v8}, Landroid/content/Context;->sendBroadcastAsUser(Landroid/content/Intent;Landroid/os/UserHandle;)V
/* .line 183 */
v0 = this.mPMS;
/* const-wide/16 v8, 0x0 */
(( com.android.server.pm.PackageManagerService$IPackageManagerImpl ) v0 ).getApplicationInfo ( v2, v8, v9, v4 ); // invoke-virtual {v0, v2, v8, v9, v4}, Lcom/android/server/pm/PackageManagerService$IPackageManagerImpl;->getApplicationInfo(Ljava/lang/String;JI)Landroid/content/pm/ApplicationInfo;
/* .line 184 */
/* .local v8, "info":Landroid/content/pm/ApplicationInfo; */
/* and-int/lit8 v0, v3, 0x10 */
int v9 = 1; // const/4 v9, 0x1
if ( v0 != null) { // if-eqz v0, :cond_2
/* move v0, v9 */
} // :cond_2
/* move v0, v11 */
} // :goto_1
/* move v10, v0 */
/* .line 186 */
/* .local v10, "shouldGrantPermission":Z */
if ( v8 != null) { // if-eqz v8, :cond_4
/* .line 187 */
if ( v10 != null) { // if-eqz v10, :cond_3
/* move v6, v11 */
} // :cond_3
/* move v0, v6 */
/* .line 188 */
/* .local v0, "opsMode":I */
v6 = this.mAppOpsManager;
/* const/16 v12, 0x2b */
/* iget v13, v8, Landroid/content/pm/ApplicationInfo;->uid:I */
(( android.app.AppOpsManager ) v6 ).setMode ( v12, v13, v2, v0 ); // invoke-virtual {v6, v12, v13, v2, v0}, Landroid/app/AppOpsManager;->setMode(IILjava/lang/String;I)V
/* .line 190 */
v6 = this.mAppOpsManager;
/* const/16 v12, 0x2726 */
/* iget v13, v8, Landroid/content/pm/ApplicationInfo;->uid:I */
(( android.app.AppOpsManager ) v6 ).setMode ( v12, v13, v2, v0 ); // invoke-virtual {v6, v12, v13, v2, v0}, Landroid/app/AppOpsManager;->setMode(IILjava/lang/String;I)V
/* .line 193 */
} // .end local v0 # "opsMode":I
} // :cond_4
/* and-int/lit8 v0, v3, 0x1 */
if ( v0 != null) { // if-eqz v0, :cond_5
/* move v15, v9 */
} // :cond_5
/* move v15, v11 */
} // :goto_2
/* move v6, v15 */
/* .line 195 */
/* .local v6, "isKeepAlive":Z */
/* new-instance v0, Landroid/os/Bundle; */
/* invoke-direct {v0}, Landroid/os/Bundle;-><init>()V */
/* move-object v9, v0 */
/* .line 196 */
/* .local v9, "bundle":Landroid/os/Bundle; */
/* const-string/jumbo v0, "userId" */
(( android.os.Bundle ) v9 ).putInt ( v0, v4 ); // invoke-virtual {v9, v0, v4}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V
/* .line 197 */
final String v0 = "pkgName"; // const-string v0, "pkgName"
(( android.os.Bundle ) v9 ).putString ( v0, v2 ); // invoke-virtual {v9, v0, v2}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V
/* .line 198 */
if ( v6 != null) { // if-eqz v6, :cond_6
final String v0 = "noRestrict"; // const-string v0, "noRestrict"
} // :cond_6
final String v0 = "miuiAuto"; // const-string v0, "miuiAuto"
} // :goto_3
final String v11 = "bgControl"; // const-string v11, "bgControl"
(( android.os.Bundle ) v9 ).putString ( v11, v0 ); // invoke-virtual {v9, v11, v0}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V
/* .line 200 */
try { // :try_start_0
v0 = this.mContext;
(( android.content.Context ) v0 ).getContentResolver ( ); // invoke-virtual {v0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;
final String v11 = "content://com.miui.powerkeeper.configure"; // const-string v11, "content://com.miui.powerkeeper.configure"
/* .line 201 */
android.net.Uri .parse ( v11 );
/* const-string/jumbo v12, "userTable" */
/* .line 200 */
android.net.Uri .withAppendedPath ( v11,v12 );
/* const-string/jumbo v12, "userTableupdate" */
(( android.content.ContentResolver ) v0 ).call ( v11, v12, v14, v9 ); // invoke-virtual {v0, v11, v12, v14, v9}, Landroid/content/ContentResolver;->call(Landroid/net/Uri;Ljava/lang/String;Ljava/lang/String;Landroid/os/Bundle;)Landroid/os/Bundle;
/* :try_end_0 */
/* .catch Ljava/lang/IllegalArgumentException; {:try_start_0 ..:try_end_0} :catch_0 */
/* .line 206 */
/* .line 204 */
/* :catch_0 */
/* move-exception v0 */
/* .line 205 */
/* .local v0, "e":Ljava/lang/IllegalArgumentException; */
/* new-instance v11, Ljava/lang/StringBuilder; */
/* invoke-direct {v11}, Ljava/lang/StringBuilder;-><init>()V */
final String v12 = "Failed to process powerkeeper config for pkg "; // const-string v12, "Failed to process powerkeeper config for pkg "
(( java.lang.StringBuilder ) v11 ).append ( v12 ); // invoke-virtual {v11, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v11 ).append ( v2 ); // invoke-virtual {v11, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v11 ).toString ( ); // invoke-virtual {v11}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
android.util.Slog .e ( v5,v11,v0 );
/* .line 207 */
} // .end local v0 # "e":Ljava/lang/IllegalArgumentException;
} // :goto_4
miui.process.ProcessManager .updateApplicationLockedState ( v2,v4,v6 );
/* .line 209 */
v0 = this.mPMSI;
(( com.miui.server.process.ProcessManagerInternal ) v0 ).updateEnterpriseWhiteList ( v2, v6 ); // invoke-virtual {v0, v2, v6}, Lcom/miui/server/process/ProcessManagerInternal;->updateEnterpriseWhiteList(Ljava/lang/String;Z)V
/* .line 211 */
return;
/* .line 155 */
} // .end local v6 # "isKeepAlive":Z
} // .end local v7 # "intent":Landroid/content/Intent;
} // .end local v8 # "info":Landroid/content/pm/ApplicationInfo;
} // .end local v9 # "bundle":Landroid/os/Bundle;
} // .end local v10 # "shouldGrantPermission":Z
} // :cond_7
} // :goto_5
/* new-instance v0, Ljava/lang/StringBuilder; */
/* invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V */
final String v6 = "Invalidate param packageName:"; // const-string v6, "Invalidate param packageName:"
(( java.lang.StringBuilder ) v0 ).append ( v6 ); // invoke-virtual {v0, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v0 ).append ( v2 ); // invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
final String v6 = ", flags:"; // const-string v6, ", flags:"
(( java.lang.StringBuilder ) v0 ).append ( v6 ); // invoke-virtual {v0, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v0 ).append ( v3 ); // invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v0 ).toString ( ); // invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
android.util.Slog .e ( v5,v0 );
/* .line 156 */
return;
} // .end method
public void setApplicationWhiteList ( java.util.List p0, Integer p1 ) {
/* .locals 3 */
/* .param p2, "userId" # I */
/* .annotation system Ldalvik/annotation/Signature; */
/* value = { */
/* "(", */
/* "Ljava/util/List<", */
/* "Ljava/lang/String;", */
/* ">;I)V" */
/* } */
} // .end annotation
/* .line 264 */
/* .local p1, "packages":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;" */
v0 = this.mContext;
com.miui.server.enterprise.ServiceUtils .checkPermission ( v0 );
/* .line 265 */
v0 = this.mContext;
/* .line 267 */
com.miui.enterprise.settings.EnterpriseSettings .generateListSettings ( p1 );
/* .line 265 */
final String v2 = "ep_app_white_list"; // const-string v2, "ep_app_white_list"
com.miui.enterprise.settings.EnterpriseSettings .putString ( v0,v2,v1,p2 );
/* .line 268 */
return;
} // .end method
public Boolean setDeviceAdmin ( android.content.ComponentName p0, Integer p1 ) {
/* .locals 5 */
/* .param p1, "component" # Landroid/content/ComponentName; */
/* .param p2, "userId" # I */
/* .line 222 */
final String v0 = "Add device admin["; // const-string v0, "Add device admin["
final String v1 = "Enterprise-App"; // const-string v1, "Enterprise-App"
v2 = this.mContext;
com.miui.server.enterprise.ServiceUtils .checkPermission ( v2 );
/* .line 224 */
try { // :try_start_0
v2 = this.mDevicePolicyManager;
int v3 = 1; // const/4 v3, 0x1
/* .line 225 */
/* new-instance v2, Ljava/lang/StringBuilder; */
/* invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V */
(( java.lang.StringBuilder ) v2 ).append ( v0 ); // invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v2 ).append ( p1 ); // invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;
final String v4 = "]"; // const-string v4, "]"
(( java.lang.StringBuilder ) v2 ).append ( v4 ); // invoke-virtual {v2, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v2 ).toString ( ); // invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
android.util.Slog .d ( v1,v2 );
/* :try_end_0 */
/* .catch Landroid/os/RemoteException; {:try_start_0 ..:try_end_0} :catch_0 */
/* .line 226 */
/* .line 227 */
/* :catch_0 */
/* move-exception v2 */
/* .line 228 */
/* .local v2, "e":Landroid/os/RemoteException; */
/* new-instance v3, Ljava/lang/StringBuilder; */
/* invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V */
(( java.lang.StringBuilder ) v3 ).append ( v0 ); // invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v0 ).append ( p1 ); // invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;
final String v3 = "] failed"; // const-string v3, "] failed"
(( java.lang.StringBuilder ) v0 ).append ( v3 ); // invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v0 ).toString ( ); // invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
android.util.Slog .d ( v1,v0,v2 );
/* .line 229 */
int v0 = 0; // const/4 v0, 0x0
} // .end method
public void setDisallowedRunningAppList ( java.util.List p0, Integer p1 ) {
/* .locals 5 */
/* .param p2, "userId" # I */
/* .annotation system Ldalvik/annotation/Signature; */
/* value = { */
/* "(", */
/* "Ljava/util/List<", */
/* "Ljava/lang/String;", */
/* ">;I)V" */
/* } */
} // .end annotation
/* .line 293 */
/* .local p1, "packages":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;" */
v0 = this.mContext;
com.miui.server.enterprise.ServiceUtils .checkPermission ( v0 );
/* .line 294 */
/* new-instance v0, Ljava/lang/StringBuilder; */
/* invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V */
/* .line 295 */
/* .local v0, "sb":Ljava/lang/StringBuilder; */
/* if-nez p1, :cond_0 */
/* new-instance v1, Ljava/util/ArrayList; */
/* invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V */
/* move-object p1, v1 */
/* .line 296 */
} // :cond_0
/* invoke-direct {p0, p1, p2}, Lcom/miui/server/enterprise/ApplicationManagerService;->forceCloseTask(Ljava/util/List;I)V */
/* .line 297 */
v2 = } // :goto_0
if ( v2 != null) { // if-eqz v2, :cond_1
/* check-cast v2, Ljava/lang/String; */
/* .line 298 */
/* .local v2, "pkg":Ljava/lang/String; */
(( java.lang.StringBuilder ) v0 ).append ( v2 ); // invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
final String v4 = ";"; // const-string v4, ";"
(( java.lang.StringBuilder ) v3 ).append ( v4 ); // invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
/* .line 299 */
v3 = this.mPMSI;
final String v4 = "enterprise disallow running"; // const-string v4, "enterprise disallow running"
(( com.miui.server.process.ProcessManagerInternal ) v3 ).forceStopPackage ( v2, p2, v4 ); // invoke-virtual {v3, v2, p2, v4}, Lcom/miui/server/process/ProcessManagerInternal;->forceStopPackage(Ljava/lang/String;ILjava/lang/String;)V
/* .line 300 */
} // .end local v2 # "pkg":Ljava/lang/String;
/* .line 301 */
} // :cond_1
v1 = (( java.lang.StringBuilder ) v0 ).length ( ); // invoke-virtual {v0}, Ljava/lang/StringBuilder;->length()I
int v2 = 1; // const/4 v2, 0x1
/* if-lez v1, :cond_2 */
/* .line 302 */
v1 = (( java.lang.StringBuilder ) v0 ).length ( ); // invoke-virtual {v0}, Ljava/lang/StringBuilder;->length()I
/* sub-int/2addr v1, v2 */
(( java.lang.StringBuilder ) v0 ).deleteCharAt ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->deleteCharAt(I)Ljava/lang/StringBuilder;
/* .line 304 */
} // :cond_2
v1 = this.mContext;
/* .line 305 */
(( java.lang.StringBuilder ) v0 ).toString ( ); // invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
/* .line 304 */
final String v4 = "ep_app_disallow_running_list"; // const-string v4, "ep_app_disallow_running_list"
com.miui.enterprise.settings.EnterpriseSettings .putString ( v1,v4,v3,p2 );
v1 = /* .line 306 */
if ( v1 != null) { // if-eqz v1, :cond_3
/* .line 307 */
miui.security.AppRunningControlManager .getInstance ( );
int v2 = 0; // const/4 v2, 0x0
(( miui.security.AppRunningControlManager ) v1 ).setBlackListEnable ( v2 ); // invoke-virtual {v1, v2}, Lmiui/security/AppRunningControlManager;->setBlackListEnable(Z)V
/* .line 309 */
} // :cond_3
miui.security.AppRunningControlManager .getInstance ( );
v3 = this.mDisAllowRunningHandleIntent;
(( miui.security.AppRunningControlManager ) v1 ).setDisallowRunningList ( p1, v3 ); // invoke-virtual {v1, p1, v3}, Lmiui/security/AppRunningControlManager;->setDisallowRunningList(Ljava/util/List;Landroid/content/Intent;)V
/* .line 310 */
miui.security.AppRunningControlManager .getInstance ( );
(( miui.security.AppRunningControlManager ) v1 ).setBlackListEnable ( v2 ); // invoke-virtual {v1, v2}, Lmiui/security/AppRunningControlManager;->setBlackListEnable(Z)V
/* .line 312 */
} // :goto_1
return;
} // .end method
public void setNotificaitonFilter ( java.lang.String p0, java.lang.String p1, java.lang.String p2, Boolean p3 ) {
/* .locals 2 */
/* .param p1, "pkg" # Ljava/lang/String; */
/* .param p2, "channelId" # Ljava/lang/String; */
/* .param p3, "type" # Ljava/lang/String; */
/* .param p4, "allow" # Z */
/* .line 477 */
v0 = this.mContext;
com.miui.server.enterprise.ServiceUtils .checkPermission ( v0 );
/* .line 478 */
v0 = android.text.TextUtils .isEmpty ( p2 );
final String v1 = "float"; // const-string v1, "float"
if ( v0 != null) { // if-eqz v0, :cond_1
/* .line 479 */
v0 = (( java.lang.String ) v1 ).equals ( p3 ); // invoke-virtual {v1, p3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z
if ( v0 != null) { // if-eqz v0, :cond_0
/* .line 480 */
v0 = this.mContext;
miui.util.NotificationFilterHelper .enableStatusIcon ( v0,p1,p4 );
/* .line 482 */
} // :cond_0
v0 = this.mContext;
miui.util.NotificationFilterHelper .setAllow ( v0,p1,p3,p4 );
/* .line 485 */
} // :cond_1
v0 = (( java.lang.String ) v1 ).equals ( p3 ); // invoke-virtual {v1, p3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z
if ( v0 != null) { // if-eqz v0, :cond_2
/* .line 486 */
v0 = this.mContext;
miui.util.NotificationFilterHelper .enableStatusIcon ( v0,p1,p2,p4 );
/* .line 488 */
} // :cond_2
v0 = this.mContext;
miui.util.NotificationFilterHelper .setAllow ( v0,p1,p2,p3,p4 );
/* .line 491 */
} // :goto_0
return;
} // .end method
public void setXSpaceBlack ( java.util.List p0 ) {
/* .locals 3 */
/* .annotation system Ldalvik/annotation/Signature; */
/* value = { */
/* "(", */
/* "Ljava/util/List<", */
/* "Ljava/lang/String;", */
/* ">;)V" */
/* } */
} // .end annotation
/* .line 495 */
/* .local p1, "packages":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;" */
v0 = this.mContext;
com.miui.server.enterprise.ServiceUtils .checkPermission ( v0 );
/* .line 496 */
v0 = this.mContext;
/* .line 498 */
com.miui.enterprise.settings.EnterpriseSettings .generateListSettings ( p1 );
/* .line 496 */
final String v2 = "ep_app_black_xsapce"; // const-string v2, "ep_app_black_xsapce"
com.miui.enterprise.settings.EnterpriseSettings .putString ( v0,v2,v1 );
/* .line 500 */
return;
} // .end method
