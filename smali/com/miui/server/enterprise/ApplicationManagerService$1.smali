.class Lcom/miui/server/enterprise/ApplicationManagerService$1;
.super Landroid/content/pm/IPackageDeleteObserver$Stub;
.source "ApplicationManagerService.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/miui/server/enterprise/ApplicationManagerService;->deletePackage(Ljava/lang/String;ILcom/miui/enterprise/sdk/IEpDeletePackageObserver;I)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/miui/server/enterprise/ApplicationManagerService;

.field final synthetic val$observer:Lcom/miui/enterprise/sdk/IEpDeletePackageObserver;


# direct methods
.method constructor <init>(Lcom/miui/server/enterprise/ApplicationManagerService;Lcom/miui/enterprise/sdk/IEpDeletePackageObserver;)V
    .locals 0
    .param p1, "this$0"    # Lcom/miui/server/enterprise/ApplicationManagerService;

    .line 97
    iput-object p1, p0, Lcom/miui/server/enterprise/ApplicationManagerService$1;->this$0:Lcom/miui/server/enterprise/ApplicationManagerService;

    iput-object p2, p0, Lcom/miui/server/enterprise/ApplicationManagerService$1;->val$observer:Lcom/miui/enterprise/sdk/IEpDeletePackageObserver;

    invoke-direct {p0}, Landroid/content/pm/IPackageDeleteObserver$Stub;-><init>()V

    return-void
.end method


# virtual methods
.method public packageDeleted(Ljava/lang/String;I)V
    .locals 1
    .param p1, "packageName"    # Ljava/lang/String;
    .param p2, "returnCode"    # I
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    .line 100
    iget-object v0, p0, Lcom/miui/server/enterprise/ApplicationManagerService$1;->val$observer:Lcom/miui/enterprise/sdk/IEpDeletePackageObserver;

    invoke-interface {v0, p1, p2}, Lcom/miui/enterprise/sdk/IEpDeletePackageObserver;->onPackageDeleted(Ljava/lang/String;I)V

    .line 101
    return-void
.end method
