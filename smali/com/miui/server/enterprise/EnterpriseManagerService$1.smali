.class Lcom/miui/server/enterprise/EnterpriseManagerService$1;
.super Landroid/content/BroadcastReceiver;
.source "EnterpriseManagerService.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/miui/server/enterprise/EnterpriseManagerService;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/miui/server/enterprise/EnterpriseManagerService;


# direct methods
.method constructor <init>(Lcom/miui/server/enterprise/EnterpriseManagerService;)V
    .locals 0
    .param p1, "this$0"    # Lcom/miui/server/enterprise/EnterpriseManagerService;

    .line 55
    iput-object p1, p0, Lcom/miui/server/enterprise/EnterpriseManagerService$1;->this$0:Lcom/miui/server/enterprise/EnterpriseManagerService;

    invoke-direct {p0}, Landroid/content/BroadcastReceiver;-><init>()V

    return-void
.end method


# virtual methods
.method public onReceive(Landroid/content/Context;Landroid/content/Intent;)V
    .locals 3
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "intent"    # Landroid/content/Intent;

    .line 58
    invoke-virtual {p2}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v0

    if-nez v0, :cond_0

    .line 59
    return-void

    .line 61
    :cond_0
    invoke-virtual {p2}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/String;->hashCode()I

    move-result v1

    const/4 v2, 0x0

    sparse-switch v1, :sswitch_data_0

    :cond_1
    goto :goto_0

    :sswitch_0
    const-string v1, "android.intent.action.USER_SWITCHED"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    const/4 v0, 0x2

    goto :goto_1

    :sswitch_1
    const-string v1, "android.intent.action.BOOT_COMPLETED"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    move v0, v2

    goto :goto_1

    :sswitch_2
    const-string v1, "com.miui.enterprise.ACTION_CERT_UPDATE"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    const/4 v0, 0x4

    goto :goto_1

    :sswitch_3
    const-string v1, "android.intent.action.USER_STARTED"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    const/4 v0, 0x1

    goto :goto_1

    :sswitch_4
    const-string v1, "android.intent.action.USER_REMOVED"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    const/4 v0, 0x3

    goto :goto_1

    :goto_0
    const/4 v0, -0x1

    :goto_1
    const-string v1, "android.intent.extra.user_handle"

    packed-switch v0, :pswitch_data_0

    goto :goto_2

    .line 78
    :pswitch_0
    iget-object v0, p0, Lcom/miui/server/enterprise/EnterpriseManagerService$1;->this$0:Lcom/miui/server/enterprise/EnterpriseManagerService;

    const-string v1, "extra_ent_cert"

    invoke-virtual {p2, v1}, Landroid/content/Intent;->getParcelableExtra(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v1

    check-cast v1, Lcom/miui/enterprise/signature/EnterpriseCer;

    invoke-static {v0, v1}, Lcom/miui/server/enterprise/EnterpriseManagerService;->-$$Nest$fputmCert(Lcom/miui/server/enterprise/EnterpriseManagerService;Lcom/miui/enterprise/signature/EnterpriseCer;)V

    .line 79
    goto :goto_2

    .line 74
    :pswitch_1
    invoke-virtual {p2, v1, v2}, Landroid/content/Intent;->getIntExtra(Ljava/lang/String;I)I

    move-result v0

    .line 75
    .local v0, "userId":I
    iget-object v1, p0, Lcom/miui/server/enterprise/EnterpriseManagerService$1;->this$0:Lcom/miui/server/enterprise/EnterpriseManagerService;

    invoke-static {v1, v0}, Lcom/miui/server/enterprise/EnterpriseManagerService;->-$$Nest$monUserRemoved(Lcom/miui/server/enterprise/EnterpriseManagerService;I)V

    .line 76
    goto :goto_2

    .line 70
    .end local v0    # "userId":I
    :pswitch_2
    invoke-virtual {p2, v1, v2}, Landroid/content/Intent;->getIntExtra(Ljava/lang/String;I)I

    move-result v0

    .line 71
    .restart local v0    # "userId":I
    iget-object v1, p0, Lcom/miui/server/enterprise/EnterpriseManagerService$1;->this$0:Lcom/miui/server/enterprise/EnterpriseManagerService;

    invoke-static {v1, v0}, Lcom/miui/server/enterprise/EnterpriseManagerService;->-$$Nest$monUserSwitched(Lcom/miui/server/enterprise/EnterpriseManagerService;I)V

    .line 72
    goto :goto_2

    .line 66
    .end local v0    # "userId":I
    :pswitch_3
    invoke-virtual {p2, v1, v2}, Landroid/content/Intent;->getIntExtra(Ljava/lang/String;I)I

    move-result v0

    .line 67
    .restart local v0    # "userId":I
    iget-object v1, p0, Lcom/miui/server/enterprise/EnterpriseManagerService$1;->this$0:Lcom/miui/server/enterprise/EnterpriseManagerService;

    invoke-static {v1, v0}, Lcom/miui/server/enterprise/EnterpriseManagerService;->-$$Nest$monUserStarted(Lcom/miui/server/enterprise/EnterpriseManagerService;I)V

    .line 68
    goto :goto_2

    .line 63
    .end local v0    # "userId":I
    :pswitch_4
    iget-object v0, p0, Lcom/miui/server/enterprise/EnterpriseManagerService$1;->this$0:Lcom/miui/server/enterprise/EnterpriseManagerService;

    invoke-static {v0}, Lcom/miui/server/enterprise/EnterpriseManagerService;->-$$Nest$mbootComplete(Lcom/miui/server/enterprise/EnterpriseManagerService;)V

    .line 64
    nop

    .line 82
    :goto_2
    return-void

    :sswitch_data_0
    .sparse-switch
        -0x7ad942ef -> :sswitch_4
        -0x2d021ace -> :sswitch_3
        0x3f221b7 -> :sswitch_2
        0x2f94f923 -> :sswitch_1
        0x392cb822 -> :sswitch_0
    .end sparse-switch

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_4
        :pswitch_3
        :pswitch_2
        :pswitch_1
        :pswitch_0
    .end packed-switch
.end method
