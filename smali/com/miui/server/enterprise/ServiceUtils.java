public class com.miui.server.enterprise.ServiceUtils {
	 /* .source "ServiceUtils.java" */
	 /* # static fields */
	 private static volatile com.miui.server.enterprise.EnterpriseManagerService sEntService;
	 /* # direct methods */
	 public com.miui.server.enterprise.ServiceUtils ( ) {
		 /* .locals 0 */
		 /* .line 15 */
		 /* invoke-direct {p0}, Ljava/lang/Object;-><init>()V */
		 return;
	 } // .end method
	 public static void checkPermission ( android.content.Context p0 ) {
		 /* .locals 10 */
		 /* .param p0, "context" # Landroid/content/Context; */
		 /* .line 20 */
		 v0 = 		 android.os.Binder .getCallingUid ( );
		 v0 = 		 android.os.UserHandle .getAppId ( v0 );
		 /* const/16 v1, 0x3e8 */
		 /* if-ne v0, v1, :cond_0 */
		 /* .line 21 */
		 return;
		 /* .line 23 */
	 } // :cond_0
	 com.miui.server.enterprise.ServiceUtils .getEntService ( );
	 v0 = 	 (( com.miui.server.enterprise.EnterpriseManagerService ) v0 ).isSignatureVerified ( ); // invoke-virtual {v0}, Lcom/miui/server/enterprise/EnterpriseManagerService;->isSignatureVerified()Z
	 if ( v0 != null) { // if-eqz v0, :cond_6
		 /* .line 26 */
		 com.miui.server.enterprise.ServiceUtils .getEntService ( );
		 (( com.miui.server.enterprise.EnterpriseManagerService ) v0 ).getEnterpriseCert ( ); // invoke-virtual {v0}, Lcom/miui/server/enterprise/EnterpriseManagerService;->getEnterpriseCert()Lcom/miui/enterprise/signature/EnterpriseCer;
		 /* .line 27 */
		 /* .local v0, "cert":Lcom/miui/enterprise/signature/EnterpriseCer; */
		 java.util.Calendar .getInstance ( );
		 (( java.util.Calendar ) v1 ).getTime ( ); // invoke-virtual {v1}, Ljava/util/Calendar;->getTime()Ljava/util/Date;
		 /* .line 28 */
		 /* .local v1, "date":Ljava/util/Date; */
		 (( com.miui.enterprise.signature.EnterpriseCer ) v0 ).getValidFrom ( ); // invoke-virtual {v0}, Lcom/miui/enterprise/signature/EnterpriseCer;->getValidFrom()Ljava/util/Date;
		 v2 = 		 (( java.util.Date ) v1 ).before ( v2 ); // invoke-virtual {v1, v2}, Ljava/util/Date;->before(Ljava/util/Date;)Z
		 /* if-nez v2, :cond_5 */
		 (( com.miui.enterprise.signature.EnterpriseCer ) v0 ).getValidTo ( ); // invoke-virtual {v0}, Lcom/miui/enterprise/signature/EnterpriseCer;->getValidTo()Ljava/util/Date;
		 v2 = 		 (( java.util.Date ) v1 ).after ( v2 ); // invoke-virtual {v1, v2}, Ljava/util/Date;->after(Ljava/util/Date;)Z
		 /* if-nez v2, :cond_5 */
		 /* .line 31 */
		 v2 = this.permissions;
		 if ( v2 != null) { // if-eqz v2, :cond_4
			 v2 = this.permissions;
			 /* array-length v2, v2 */
			 if ( v2 != null) { // if-eqz v2, :cond_4
				 /* .line 32 */
				 java.lang.Thread .currentThread ( );
				 (( java.lang.Thread ) v2 ).getStackTrace ( ); // invoke-virtual {v2}, Ljava/lang/Thread;->getStackTrace()[Ljava/lang/StackTraceElement;
				 /* .line 33 */
				 /* .local v2, "elements":[Ljava/lang/StackTraceElement; */
				 int v3 = 3; // const/4 v3, 0x3
				 /* aget-object v3, v2, v3 */
				 (( java.lang.StackTraceElement ) v3 ).getMethodName ( ); // invoke-virtual {v3}, Ljava/lang/StackTraceElement;->getMethodName()Ljava/lang/String;
				 /* .line 34 */
				 /* .local v3, "currentMethod":Ljava/lang/String; */
				 int v4 = 0; // const/4 v4, 0x0
				 /* .line 35 */
				 /* .local v4, "match":Z */
				 v5 = this.permissions;
				 /* array-length v6, v5 */
				 int v7 = 0; // const/4 v7, 0x0
			 } // :goto_0
			 /* if-ge v7, v6, :cond_2 */
			 /* aget-object v8, v5, v7 */
			 /* .line 36 */
			 /* .local v8, "permission":Ljava/lang/String; */
			 v9 = 			 android.text.TextUtils .equals ( v3,v8 );
			 if ( v9 != null) { // if-eqz v9, :cond_1
				 /* .line 37 */
				 int v4 = 1; // const/4 v4, 0x1
				 /* .line 38 */
				 /* .line 35 */
			 } // .end local v8 # "permission":Ljava/lang/String;
		 } // :cond_1
		 /* add-int/lit8 v7, v7, 0x1 */
		 /* .line 41 */
	 } // :cond_2
} // :goto_1
if ( v4 != null) { // if-eqz v4, :cond_3
	 /* .line 42 */
} // :cond_3
/* new-instance v5, Ljava/lang/SecurityException; */
/* new-instance v6, Ljava/lang/StringBuilder; */
/* invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V */
final String v7 = "Permission denied for "; // const-string v7, "Permission denied for "
(( java.lang.StringBuilder ) v6 ).append ( v7 ); // invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v6 ).append ( v3 ); // invoke-virtual {v6, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v6 ).toString ( ); // invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
/* invoke-direct {v5, v6}, Ljava/lang/SecurityException;-><init>(Ljava/lang/String;)V */
/* throw v5 */
/* .line 45 */
} // .end local v2 # "elements":[Ljava/lang/StackTraceElement;
} // .end local v3 # "currentMethod":Ljava/lang/String;
} // .end local v4 # "match":Z
} // :cond_4
} // :goto_2
android.os.Binder .clearCallingIdentity ( );
/* .line 46 */
return;
/* .line 29 */
} // :cond_5
/* new-instance v2, Ljava/lang/SecurityException; */
final String v3 = "Enterprise cert out of date"; // const-string v3, "Enterprise cert out of date"
/* invoke-direct {v2, v3}, Ljava/lang/SecurityException;-><init>(Ljava/lang/String;)V */
/* throw v2 */
/* .line 24 */
} // .end local v0 # "cert":Lcom/miui/enterprise/signature/EnterpriseCer;
} // .end local v1 # "date":Ljava/util/Date;
} // :cond_6
/* new-instance v0, Ljava/lang/SecurityException; */
final String v1 = "No enterprise cert"; // const-string v1, "No enterprise cert"
/* invoke-direct {v0, v1}, Ljava/lang/SecurityException;-><init>(Ljava/lang/String;)V */
/* throw v0 */
} // .end method
private static synchronized com.miui.server.enterprise.EnterpriseManagerService getEntService ( ) {
/* .locals 2 */
/* const-class v0, Lcom/miui/server/enterprise/ServiceUtils; */
/* monitor-enter v0 */
/* .line 49 */
try { // :try_start_0
v1 = com.miui.server.enterprise.ServiceUtils.sEntService;
/* if-nez v1, :cond_0 */
/* .line 50 */
final String v1 = "EnterpriseManager"; // const-string v1, "EnterpriseManager"
android.os.ServiceManager .getService ( v1 );
/* check-cast v1, Lcom/miui/server/enterprise/EnterpriseManagerService; */
/* .line 52 */
} // :cond_0
v1 = com.miui.server.enterprise.ServiceUtils.sEntService;
/* :try_end_0 */
/* .catchall {:try_start_0 ..:try_end_0} :catchall_0 */
/* monitor-exit v0 */
/* .line 48 */
/* :catchall_0 */
/* move-exception v1 */
/* monitor-exit v0 */
/* throw v1 */
} // .end method
