public class com.miui.server.enterprise.APNManagerService extends com.miui.enterprise.IAPNManager$Stub {
	 /* .source "APNManagerService.java" */
	 /* # static fields */
	 private static final android.net.Uri DEFAULTAPN_URI;
	 private static final android.net.Uri PREFERAPN_URI;
	 private static final java.lang.String TAG;
	 /* # instance fields */
	 private android.content.Context mContext;
	 private android.telephony.TelephonyManager mTelephonyManager;
	 /* # direct methods */
	 static com.miui.server.enterprise.APNManagerService ( ) {
		 /* .locals 1 */
		 /* .line 28 */
		 final String v0 = "content://telephony/carriers/preferapn"; // const-string v0, "content://telephony/carriers/preferapn"
		 android.net.Uri .parse ( v0 );
		 /* .line 29 */
		 final String v0 = "content://telephony/carriers/restore"; // const-string v0, "content://telephony/carriers/restore"
		 android.net.Uri .parse ( v0 );
		 return;
	 } // .end method
	 com.miui.server.enterprise.APNManagerService ( ) {
		 /* .locals 1 */
		 /* .param p1, "context" # Landroid/content/Context; */
		 /* .line 32 */
		 /* invoke-direct {p0}, Lcom/miui/enterprise/IAPNManager$Stub;-><init>()V */
		 /* .line 33 */
		 this.mContext = p1;
		 /* .line 34 */
		 final String v0 = "phone"; // const-string v0, "phone"
		 (( android.content.Context ) p1 ).getSystemService ( v0 ); // invoke-virtual {p1, v0}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;
		 /* check-cast v0, Landroid/telephony/TelephonyManager; */
		 this.mTelephonyManager = v0;
		 /* .line 35 */
		 return;
	 } // .end method
	 private java.lang.String buildNameSelection ( java.lang.String p0 ) {
		 /* .locals 2 */
		 /* .param p1, "name" # Ljava/lang/String; */
		 /* .line 50 */
		 /* new-instance v0, Ljava/lang/StringBuilder; */
		 /* invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V */
		 final String v1 = "name=\""; // const-string v1, "name=\""
		 (( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
		 (( java.lang.StringBuilder ) v0 ).append ( p1 ); // invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
		 final String v1 = "\""; // const-string v1, "\""
		 (( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
		 (( java.lang.StringBuilder ) v0 ).toString ( ); // invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
	 } // .end method
	 private java.lang.String buildNumericAndNameSelection ( java.lang.String p0, java.lang.String p1 ) {
		 /* .locals 2 */
		 /* .param p1, "numeric" # Ljava/lang/String; */
		 /* .param p2, "name" # Ljava/lang/String; */
		 /* .line 42 */
		 /* new-instance v0, Ljava/lang/StringBuilder; */
		 /* invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V */
		 /* invoke-direct {p0, p1}, Lcom/miui/server/enterprise/APNManagerService;->buildNumericSelection(Ljava/lang/String;)Ljava/lang/String; */
		 (( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
		 final String v1 = " and "; // const-string v1, " and "
		 (( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
		 /* invoke-direct {p0, p2}, Lcom/miui/server/enterprise/APNManagerService;->buildNameSelection(Ljava/lang/String;)Ljava/lang/String; */
		 (( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
		 (( java.lang.StringBuilder ) v0 ).toString ( ); // invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
	 } // .end method
	 private java.lang.String buildNumericSelection ( java.lang.String p0 ) {
		 /* .locals 2 */
		 /* .param p1, "numeric" # Ljava/lang/String; */
		 /* .line 46 */
		 /* new-instance v0, Ljava/lang/StringBuilder; */
		 /* invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V */
		 final String v1 = "numeric=\""; // const-string v1, "numeric=\""
		 (( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
		 (( java.lang.StringBuilder ) v0 ).append ( p1 ); // invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
		 final String v1 = "\""; // const-string v1, "\""
		 (( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
		 (( java.lang.StringBuilder ) v0 ).toString ( ); // invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
	 } // .end method
	 private void closeCursor ( android.database.Cursor p0 ) {
		 /* .locals 1 */
		 /* .param p1, "cursor" # Landroid/database/Cursor; */
		 /* .line 64 */
			 v0 = 		 if ( p1 != null) { // if-eqz p1, :cond_0
			 /* if-nez v0, :cond_0 */
			 /* .line 65 */
			 /* .line 67 */
		 } // :cond_0
		 return;
	 } // .end method
	 private android.content.ContentResolver getContentResolver ( ) {
		 /* .locals 3 */
		 /* .line 38 */
		 v0 = this.mContext;
		 /* new-instance v1, Landroid/os/UserHandle; */
		 int v2 = 0; // const/4 v2, 0x0
		 /* invoke-direct {v1, v2}, Landroid/os/UserHandle;-><init>(I)V */
		 (( android.content.Context ) v0 ).getContentResolverForUser ( v1 ); // invoke-virtual {v0, v1}, Landroid/content/Context;->getContentResolverForUser(Landroid/os/UserHandle;)Landroid/content/ContentResolver;
	 } // .end method
	 private java.lang.String getNumeric ( ) {
		 /* .locals 5 */
		 /* .line 54 */
		 miui.telephony.SubscriptionManager .getDefault ( );
		 v0 = 		 (( miui.telephony.SubscriptionManager ) v0 ).getDefaultDataSubscriptionId ( ); // invoke-virtual {v0}, Lmiui/telephony/SubscriptionManager;->getDefaultDataSubscriptionId()I
		 /* .line 55 */
		 /* .local v0, "subId":I */
		 v1 = this.mContext;
		 android.telephony.SubscriptionManager .from ( v1 );
		 (( android.telephony.SubscriptionManager ) v1 ).getActiveSubscriptionInfo ( v0 ); // invoke-virtual {v1, v0}, Landroid/telephony/SubscriptionManager;->getActiveSubscriptionInfo(I)Landroid/telephony/SubscriptionInfo;
		 /* .line 56 */
		 /* .local v1, "subscriptionInfo":Landroid/telephony/SubscriptionInfo; */
		 /* if-nez v1, :cond_0 */
		 final String v2 = ""; // const-string v2, ""
		 /* .line 57 */
	 } // :cond_0
	 v2 = this.mTelephonyManager;
	 v3 = 	 (( android.telephony.SubscriptionInfo ) v1 ).getSubscriptionId ( ); // invoke-virtual {v1}, Landroid/telephony/SubscriptionInfo;->getSubscriptionId()I
	 (( android.telephony.TelephonyManager ) v2 ).getSimOperator ( v3 ); // invoke-virtual {v2, v3}, Landroid/telephony/TelephonyManager;->getSimOperator(I)Ljava/lang/String;
} // :goto_0
/* nop */
/* .line 58 */
/* .local v2, "mccmnc":Ljava/lang/String; */
miui.telephony.SubscriptionManager .getDefault ( );
v3 = (( miui.telephony.SubscriptionManager ) v3 ).getDefaultDataSlotId ( ); // invoke-virtual {v3}, Lmiui/telephony/SubscriptionManager;->getDefaultDataSlotId()I
v4 = miui.telephony.TelephonyConstants.PROPERTY_APN_SIM_OPERATOR_NUMERIC;
android.telephony.TelephonyManager .getTelephonyProperty ( v3,v4,v2 );
/* .line 60 */
} // .end method
private android.net.Uri getUriForCurrSubId ( android.net.Uri p0 ) {
/* .locals 5 */
/* .param p1, "uri" # Landroid/net/Uri; */
/* .line 518 */
miui.telephony.SubscriptionManager .getDefault ( );
v0 = (( miui.telephony.SubscriptionManager ) v0 ).getDefaultDataSubscriptionId ( ); // invoke-virtual {v0}, Lmiui/telephony/SubscriptionManager;->getDefaultDataSubscriptionId()I
/* .line 519 */
/* .local v0, "subId":I */
v1 = this.mContext;
android.telephony.SubscriptionManager .from ( v1 );
(( android.telephony.SubscriptionManager ) v1 ).getActiveSubscriptionInfo ( v0 ); // invoke-virtual {v1, v0}, Landroid/telephony/SubscriptionManager;->getActiveSubscriptionInfo(I)Landroid/telephony/SubscriptionInfo;
/* .line 520 */
/* .local v1, "subscriptionInfo":Landroid/telephony/SubscriptionInfo; */
if ( v1 != null) { // if-eqz v1, :cond_0
	 v2 = 	 (( android.telephony.SubscriptionInfo ) v1 ).getSubscriptionId ( ); // invoke-virtual {v1}, Landroid/telephony/SubscriptionInfo;->getSubscriptionId()I
	 /* .line 521 */
} // :cond_0
int v2 = -1; // const/4 v2, -0x1
} // :goto_0
/* nop */
/* .line 522 */
/* .local v2, "subId0":I */
v3 = android.telephony.SubscriptionManager .isValidSubscriptionId ( v2 );
if ( v3 != null) { // if-eqz v3, :cond_1
/* .line 523 */
/* new-instance v3, Ljava/lang/StringBuilder; */
/* invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V */
/* const-string/jumbo v4, "subId/" */
(( java.lang.StringBuilder ) v3 ).append ( v4 ); // invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
java.lang.String .valueOf ( v2 );
(( java.lang.StringBuilder ) v3 ).append ( v4 ); // invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v3 ).toString ( ); // invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
android.net.Uri .withAppendedPath ( p1,v3 );
/* .line 525 */
} // :cond_1
} // .end method
/* # virtual methods */
public Boolean activeAPN ( java.lang.String p0 ) {
/* .locals 10 */
/* .param p1, "name" # Ljava/lang/String; */
/* .line 453 */
v0 = this.mContext;
com.miui.server.enterprise.ServiceUtils .checkPermission ( v0 );
/* .line 454 */
/* invoke-direct {p0}, Lcom/miui/server/enterprise/APNManagerService;->getNumeric()Ljava/lang/String; */
/* .line 455 */
/* .local v0, "numeric":Ljava/lang/String; */
v1 = android.text.TextUtils .isEmpty ( p1 );
final String v2 = "APNManagerMode"; // const-string v2, "APNManagerMode"
int v3 = 0; // const/4 v3, 0x0
/* if-nez v1, :cond_3 */
v1 = android.text.TextUtils .isEmpty ( v0 );
if ( v1 != null) { // if-eqz v1, :cond_0
/* goto/16 :goto_1 */
/* .line 459 */
} // :cond_0
/* invoke-direct {p0}, Lcom/miui/server/enterprise/APNManagerService;->getContentResolver()Landroid/content/ContentResolver; */
v5 = android.provider.Telephony$Carriers.CONTENT_URI;
final String v1 = "_id"; // const-string v1, "_id"
/* filled-new-array {v1}, [Ljava/lang/String; */
/* .line 460 */
/* invoke-direct {p0, v0, p1}, Lcom/miui/server/enterprise/APNManagerService;->buildNumericAndNameSelection(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String; */
int v8 = 0; // const/4 v8, 0x0
final String v9 = "name ASC"; // const-string v9, "name ASC"
/* .line 459 */
/* invoke-virtual/range {v4 ..v9}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor; */
/* .line 463 */
/* .local v1, "cursor":Landroid/database/Cursor; */
if ( v1 != null) { // if-eqz v1, :cond_2
	 v4 = try { // :try_start_0
	 /* if-lez v4, :cond_2 */
	 /* .line 464 */
	 v4 = 	 /* .line 465 */
	 /* .line 466 */
	 /* .local v4, "apnId":I */
	 /* new-instance v5, Landroid/content/ContentValues; */
	 /* invoke-direct {v5}, Landroid/content/ContentValues;-><init>()V */
	 /* .line 467 */
	 /* .local v5, "values":Landroid/content/ContentValues; */
	 final String v6 = "apn_id"; // const-string v6, "apn_id"
	 java.lang.Integer .valueOf ( v4 );
	 (( android.content.ContentValues ) v5 ).put ( v6, v7 ); // invoke-virtual {v5, v6, v7}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V
	 /* .line 468 */
	 /* invoke-direct {p0}, Lcom/miui/server/enterprise/APNManagerService;->getContentResolver()Landroid/content/ContentResolver; */
	 v7 = com.miui.server.enterprise.APNManagerService.PREFERAPN_URI;
	 int v8 = 0; // const/4 v8, 0x0
	 v6 = 	 (( android.content.ContentResolver ) v6 ).update ( v7, v5, v8, v8 ); // invoke-virtual {v6, v7, v5, v8, v8}, Landroid/content/ContentResolver;->update(Landroid/net/Uri;Landroid/content/ContentValues;Ljava/lang/String;[Ljava/lang/String;)I
	 /* .line 470 */
	 /* .local v6, "result":I */
	 /* new-instance v7, Ljava/lang/StringBuilder; */
	 /* invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V */
	 final String v8 = "active apn("; // const-string v8, "active apn("
	 (( java.lang.StringBuilder ) v7 ).append ( v8 ); // invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
	 (( java.lang.StringBuilder ) v7 ).append ( v4 ); // invoke-virtual {v7, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;
	 final String v8 = "), result: "; // const-string v8, "), result: "
	 (( java.lang.StringBuilder ) v7 ).append ( v8 ); // invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
	 (( java.lang.StringBuilder ) v7 ).append ( v6 ); // invoke-virtual {v7, v6}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;
	 (( java.lang.StringBuilder ) v7 ).toString ( ); // invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
	 android.util.Slog .d ( v2,v7 );
	 /* :try_end_0 */
	 /* .catchall {:try_start_0 ..:try_end_0} :catchall_0 */
	 /* .line 471 */
	 /* if-lez v6, :cond_1 */
	 int v3 = 1; // const/4 v3, 0x1
	 /* .line 477 */
} // :cond_1
/* invoke-direct {p0, v1}, Lcom/miui/server/enterprise/APNManagerService;->closeCursor(Landroid/database/Cursor;)V */
/* .line 471 */
/* .line 477 */
} // .end local v4 # "apnId":I
} // .end local v5 # "values":Landroid/content/ContentValues;
} // .end local v6 # "result":I
/* :catchall_0 */
/* move-exception v2 */
/* .line 473 */
} // :cond_2
try { // :try_start_1
/* new-instance v4, Ljava/lang/StringBuilder; */
/* invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V */
final String v5 = "No such config: "; // const-string v5, "No such config: "
(( java.lang.StringBuilder ) v4 ).append ( v5 ); // invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v4 ).append ( p1 ); // invoke-virtual {v4, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v4 ).toString ( ); // invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
android.util.Slog .d ( v2,v4 );
/* :try_end_1 */
/* .catchall {:try_start_1 ..:try_end_1} :catchall_0 */
/* .line 474 */
/* nop */
/* .line 477 */
/* invoke-direct {p0, v1}, Lcom/miui/server/enterprise/APNManagerService;->closeCursor(Landroid/database/Cursor;)V */
/* .line 474 */
/* .line 477 */
} // :goto_0
/* invoke-direct {p0, v1}, Lcom/miui/server/enterprise/APNManagerService;->closeCursor(Landroid/database/Cursor;)V */
/* .line 478 */
/* throw v2 */
/* .line 456 */
} // .end local v1 # "cursor":Landroid/database/Cursor;
} // :cond_3
} // :goto_1
final String v1 = "neither name or numeric can\'t be null"; // const-string v1, "neither name or numeric can\'t be null"
android.util.Slog .e ( v2,v1 );
/* .line 457 */
} // .end method
public void activeAPNForNumeric ( java.lang.String p0, java.lang.String p1 ) {
/* .locals 8 */
/* .param p1, "numeric" # Ljava/lang/String; */
/* .param p2, "name" # Ljava/lang/String; */
/* .line 429 */
v0 = this.mContext;
com.miui.server.enterprise.ServiceUtils .checkPermission ( v0 );
/* .line 430 */
v0 = android.text.TextUtils .isEmpty ( p2 );
final String v1 = "APNManagerMode"; // const-string v1, "APNManagerMode"
/* if-nez v0, :cond_2 */
v0 = android.text.TextUtils .isEmpty ( p1 );
if ( v0 != null) { // if-eqz v0, :cond_0
/* goto/16 :goto_1 */
/* .line 434 */
} // :cond_0
/* invoke-direct {p0}, Lcom/miui/server/enterprise/APNManagerService;->getContentResolver()Landroid/content/ContentResolver; */
v3 = android.provider.Telephony$Carriers.CONTENT_URI;
final String v0 = "_id"; // const-string v0, "_id"
/* filled-new-array {v0}, [Ljava/lang/String; */
/* .line 435 */
/* invoke-direct {p0, p1, p2}, Lcom/miui/server/enterprise/APNManagerService;->buildNumericAndNameSelection(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String; */
int v6 = 0; // const/4 v6, 0x0
final String v7 = "name ASC"; // const-string v7, "name ASC"
/* .line 434 */
/* invoke-virtual/range {v2 ..v7}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor; */
/* .line 437 */
/* .local v0, "cursor":Landroid/database/Cursor; */
v2 = if ( v0 != null) { // if-eqz v0, :cond_1
/* if-lez v2, :cond_1 */
/* .line 438 */
/* .line 439 */
v2 = int v2 = 0; // const/4 v2, 0x0
/* .line 440 */
/* .local v2, "apnId":I */
/* new-instance v3, Landroid/content/ContentValues; */
/* invoke-direct {v3}, Landroid/content/ContentValues;-><init>()V */
/* .line 441 */
/* .local v3, "values":Landroid/content/ContentValues; */
final String v4 = "apn_id"; // const-string v4, "apn_id"
java.lang.Integer .valueOf ( v2 );
(( android.content.ContentValues ) v3 ).put ( v4, v5 ); // invoke-virtual {v3, v4, v5}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V
/* .line 442 */
/* invoke-direct {p0}, Lcom/miui/server/enterprise/APNManagerService;->getContentResolver()Landroid/content/ContentResolver; */
v5 = com.miui.server.enterprise.APNManagerService.PREFERAPN_URI;
int v6 = 0; // const/4 v6, 0x0
v4 = (( android.content.ContentResolver ) v4 ).update ( v5, v3, v6, v6 ); // invoke-virtual {v4, v5, v3, v6, v6}, Landroid/content/ContentResolver;->update(Landroid/net/Uri;Landroid/content/ContentValues;Ljava/lang/String;[Ljava/lang/String;)I
/* .line 444 */
/* .local v4, "result":I */
/* new-instance v5, Ljava/lang/StringBuilder; */
/* invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V */
final String v6 = "active apn("; // const-string v6, "active apn("
(( java.lang.StringBuilder ) v5 ).append ( v6 ); // invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v5 ).append ( v2 ); // invoke-virtual {v5, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;
final String v6 = "), result: "; // const-string v6, "), result: "
(( java.lang.StringBuilder ) v5 ).append ( v6 ); // invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v5 ).append ( v4 ); // invoke-virtual {v5, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v5 ).toString ( ); // invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
android.util.Slog .d ( v1,v5 );
/* .line 445 */
/* .line 446 */
} // .end local v2 # "apnId":I
} // .end local v3 # "values":Landroid/content/ContentValues;
} // .end local v4 # "result":I
/* .line 447 */
} // :cond_1
/* new-instance v2, Ljava/lang/StringBuilder; */
/* invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V */
final String v3 = "No such config: "; // const-string v3, "No such config: "
(( java.lang.StringBuilder ) v2 ).append ( v3 ); // invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v2 ).append ( p2 ); // invoke-virtual {v2, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v2 ).toString ( ); // invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
android.util.Slog .d ( v1,v2 );
/* .line 449 */
} // :goto_0
return;
/* .line 431 */
} // .end local v0 # "cursor":Landroid/database/Cursor;
} // :cond_2
} // :goto_1
final String v0 = "neither name or numeric can\'t be null"; // const-string v0, "neither name or numeric can\'t be null"
android.util.Slog .e ( v1,v0 );
/* .line 432 */
return;
} // .end method
public Boolean addAPN ( com.miui.enterprise.sdk.APNConfig p0 ) {
/* .locals 7 */
/* .param p1, "config" # Lcom/miui/enterprise/sdk/APNConfig; */
/* .line 250 */
v0 = this.mContext;
com.miui.server.enterprise.ServiceUtils .checkPermission ( v0 );
/* .line 252 */
v0 = this.mNumeric;
v0 = android.text.TextUtils .isEmpty ( v0 );
if ( v0 != null) { // if-eqz v0, :cond_0
/* .line 253 */
/* invoke-direct {p0}, Lcom/miui/server/enterprise/APNManagerService;->getNumeric()Ljava/lang/String; */
/* .local v0, "numeric":Ljava/lang/String; */
/* .line 255 */
} // .end local v0 # "numeric":Ljava/lang/String;
} // :cond_0
v0 = this.mNumeric;
/* .line 257 */
/* .restart local v0 # "numeric":Ljava/lang/String; */
} // :goto_0
v1 = android.text.TextUtils .isEmpty ( v0 );
final String v2 = "APNManagerMode"; // const-string v2, "APNManagerMode"
int v3 = 0; // const/4 v3, 0x0
if ( v1 != null) { // if-eqz v1, :cond_1
/* .line 258 */
final String v1 = "addAPN:: Invalidate numeric"; // const-string v1, "addAPN:: Invalidate numeric"
android.util.Slog .e ( v2,v1 );
/* .line 259 */
/* .line 261 */
} // :cond_1
/* new-instance v1, Landroid/content/ContentValues; */
/* invoke-direct {v1}, Landroid/content/ContentValues;-><init>()V */
/* .line 262 */
/* .local v1, "values":Landroid/content/ContentValues; */
final String v4 = "name"; // const-string v4, "name"
v5 = this.mName;
(( android.content.ContentValues ) v1 ).put ( v4, v5 ); // invoke-virtual {v1, v4, v5}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V
/* .line 263 */
final String v4 = "apn"; // const-string v4, "apn"
v5 = this.mApn;
(( android.content.ContentValues ) v1 ).put ( v4, v5 ); // invoke-virtual {v1, v4, v5}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V
/* .line 264 */
/* const-string/jumbo v4, "user" */
v5 = this.mUser;
(( android.content.ContentValues ) v1 ).put ( v4, v5 ); // invoke-virtual {v1, v4, v5}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V
/* .line 265 */
final String v4 = "password"; // const-string v4, "password"
v5 = this.mPassword;
(( android.content.ContentValues ) v1 ).put ( v4, v5 ); // invoke-virtual {v1, v4, v5}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V
/* .line 266 */
/* iget v4, p1, Lcom/miui/enterprise/sdk/APNConfig;->mAuthType:I */
java.lang.Integer .valueOf ( v4 );
final String v5 = "authtype"; // const-string v5, "authtype"
(( android.content.ContentValues ) v1 ).put ( v5, v4 ); // invoke-virtual {v1, v5, v4}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V
/* .line 267 */
/* iget v4, p1, Lcom/miui/enterprise/sdk/APNConfig;->mBearer:I */
java.lang.Integer .valueOf ( v4 );
final String v5 = "bearer"; // const-string v5, "bearer"
(( android.content.ContentValues ) v1 ).put ( v5, v4 ); // invoke-virtual {v1, v5, v4}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V
/* .line 268 */
v4 = this.mMcc;
v4 = android.text.TextUtils .isEmpty ( v4 );
int v5 = 3; // const/4 v5, 0x3
if ( v4 != null) { // if-eqz v4, :cond_2
(( java.lang.String ) v0 ).substring ( v3, v5 ); // invoke-virtual {v0, v3, v5}, Ljava/lang/String;->substring(II)Ljava/lang/String;
} // :cond_2
v4 = this.mMcc;
} // :goto_1
final String v6 = "mcc"; // const-string v6, "mcc"
(( android.content.ContentValues ) v1 ).put ( v6, v4 ); // invoke-virtual {v1, v6, v4}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V
/* .line 269 */
v4 = this.mMnc;
v4 = android.text.TextUtils .isEmpty ( v4 );
if ( v4 != null) { // if-eqz v4, :cond_3
int v4 = 5; // const/4 v4, 0x5
(( java.lang.String ) v0 ).substring ( v5, v4 ); // invoke-virtual {v0, v5, v4}, Ljava/lang/String;->substring(II)Ljava/lang/String;
} // :cond_3
v4 = this.mMnc;
} // :goto_2
final String v5 = "mnc"; // const-string v5, "mnc"
(( android.content.ContentValues ) v1 ).put ( v5, v4 ); // invoke-virtual {v1, v5, v4}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V
/* .line 270 */
final String v4 = "numeric"; // const-string v4, "numeric"
(( android.content.ContentValues ) v1 ).put ( v4, v0 ); // invoke-virtual {v1, v4, v0}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V
/* .line 274 */
/* iget v4, p1, Lcom/miui/enterprise/sdk/APNConfig;->mCarrier_enabled:I */
int v5 = -1; // const/4 v5, -0x1
/* if-eq v4, v5, :cond_4 */
/* .line 275 */
/* iget v4, p1, Lcom/miui/enterprise/sdk/APNConfig;->mCarrier_enabled:I */
java.lang.Integer .valueOf ( v4 );
final String v6 = "carrier_enabled"; // const-string v6, "carrier_enabled"
(( android.content.ContentValues ) v1 ).put ( v6, v4 ); // invoke-virtual {v1, v6, v4}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V
/* .line 277 */
} // :cond_4
/* iget v4, p1, Lcom/miui/enterprise/sdk/APNConfig;->mCurrent:I */
/* if-eq v4, v5, :cond_5 */
/* .line 278 */
/* iget v4, p1, Lcom/miui/enterprise/sdk/APNConfig;->mCarrier_enabled:I */
java.lang.Integer .valueOf ( v4 );
final String v5 = "current"; // const-string v5, "current"
(( android.content.ContentValues ) v1 ).put ( v5, v4 ); // invoke-virtual {v1, v5, v4}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V
/* .line 281 */
} // :cond_5
v4 = this.mMmsc;
if ( v4 != null) { // if-eqz v4, :cond_6
/* .line 282 */
final String v4 = "mmsc"; // const-string v4, "mmsc"
v5 = this.mMmsc;
(( android.content.ContentValues ) v1 ).put ( v4, v5 ); // invoke-virtual {v1, v4, v5}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V
/* .line 285 */
} // :cond_6
v4 = this.mMmsport;
if ( v4 != null) { // if-eqz v4, :cond_7
/* .line 286 */
final String v4 = "mmsport"; // const-string v4, "mmsport"
v5 = this.mMmsport;
(( android.content.ContentValues ) v1 ).put ( v4, v5 ); // invoke-virtual {v1, v4, v5}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V
/* .line 288 */
} // :cond_7
v4 = this.mMmsproxy;
if ( v4 != null) { // if-eqz v4, :cond_8
/* .line 289 */
final String v4 = "mmsproxy"; // const-string v4, "mmsproxy"
v5 = this.mMmsproxy;
(( android.content.ContentValues ) v1 ).put ( v4, v5 ); // invoke-virtual {v1, v4, v5}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V
/* .line 290 */
} // :cond_8
v4 = this.mMvno_match_data;
if ( v4 != null) { // if-eqz v4, :cond_9
/* .line 291 */
final String v4 = "mvno_match_data"; // const-string v4, "mvno_match_data"
v5 = this.mMvno_match_data;
(( android.content.ContentValues ) v1 ).put ( v4, v5 ); // invoke-virtual {v1, v4, v5}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V
/* .line 292 */
} // :cond_9
v4 = this.mMvno_type;
if ( v4 != null) { // if-eqz v4, :cond_a
/* .line 293 */
final String v4 = "mvno_type"; // const-string v4, "mvno_type"
v5 = this.mMvno_type;
(( android.content.ContentValues ) v1 ).put ( v4, v5 ); // invoke-virtual {v1, v4, v5}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V
/* .line 294 */
} // :cond_a
v4 = this.mPort;
if ( v4 != null) { // if-eqz v4, :cond_b
/* .line 295 */
final String v4 = "port"; // const-string v4, "port"
v5 = this.mPort;
(( android.content.ContentValues ) v1 ).put ( v4, v5 ); // invoke-virtual {v1, v4, v5}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V
/* .line 296 */
} // :cond_b
v4 = this.mProtocol;
if ( v4 != null) { // if-eqz v4, :cond_c
/* .line 297 */
final String v4 = "protocol"; // const-string v4, "protocol"
v5 = this.mProtocol;
(( android.content.ContentValues ) v1 ).put ( v4, v5 ); // invoke-virtual {v1, v4, v5}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V
/* .line 298 */
} // :cond_c
v4 = this.mProxy;
if ( v4 != null) { // if-eqz v4, :cond_d
/* .line 299 */
final String v4 = "proxy"; // const-string v4, "proxy"
v5 = this.mProxy;
(( android.content.ContentValues ) v1 ).put ( v4, v5 ); // invoke-virtual {v1, v4, v5}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V
/* .line 300 */
} // :cond_d
v4 = this.mRoaming_protocol;
if ( v4 != null) { // if-eqz v4, :cond_e
/* .line 301 */
final String v4 = "roaming_protocol"; // const-string v4, "roaming_protocol"
v5 = this.mRoaming_protocol;
(( android.content.ContentValues ) v1 ).put ( v4, v5 ); // invoke-virtual {v1, v4, v5}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V
/* .line 302 */
} // :cond_e
v4 = this.mServer;
if ( v4 != null) { // if-eqz v4, :cond_f
/* .line 303 */
/* const-string/jumbo v4, "server" */
v5 = this.mServer;
(( android.content.ContentValues ) v1 ).put ( v4, v5 ); // invoke-virtual {v1, v4, v5}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V
/* .line 304 */
} // :cond_f
v4 = this.mSub_id;
if ( v4 != null) { // if-eqz v4, :cond_10
/* .line 305 */
/* const-string/jumbo v4, "sub_id" */
v5 = this.mSub_id;
(( android.content.ContentValues ) v1 ).put ( v4, v5 ); // invoke-virtual {v1, v4, v5}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V
/* .line 306 */
} // :cond_10
v4 = this.mType;
if ( v4 != null) { // if-eqz v4, :cond_11
/* .line 307 */
/* const-string/jumbo v4, "type" */
v5 = this.mType;
(( android.content.ContentValues ) v1 ).put ( v4, v5 ); // invoke-virtual {v1, v4, v5}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V
/* .line 310 */
} // :cond_11
/* invoke-direct {p0}, Lcom/miui/server/enterprise/APNManagerService;->getContentResolver()Landroid/content/ContentResolver; */
v5 = android.provider.Telephony$Carriers.CONTENT_URI;
(( android.content.ContentResolver ) v4 ).insert ( v5, v1 ); // invoke-virtual {v4, v5, v1}, Landroid/content/ContentResolver;->insert(Landroid/net/Uri;Landroid/content/ContentValues;)Landroid/net/Uri;
/* .line 311 */
/* .local v4, "uri":Landroid/net/Uri; */
/* new-instance v5, Ljava/lang/StringBuilder; */
/* invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V */
final String v6 = "addAPN:: New apn config: "; // const-string v6, "addAPN:: New apn config: "
(( java.lang.StringBuilder ) v5 ).append ( v6 ); // invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v5 ).append ( v4 ); // invoke-virtual {v5, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v5 ).toString ( ); // invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
android.util.Slog .d ( v2,v5 );
/* .line 312 */
if ( v4 != null) { // if-eqz v4, :cond_12
int v3 = 1; // const/4 v3, 0x1
} // :cond_12
} // .end method
public void addAPNForNumeric ( java.lang.String p0, com.miui.enterprise.sdk.APNConfig p1 ) {
/* .locals 5 */
/* .param p1, "numeric" # Ljava/lang/String; */
/* .param p2, "config" # Lcom/miui/enterprise/sdk/APNConfig; */
/* .line 227 */
v0 = this.mContext;
com.miui.server.enterprise.ServiceUtils .checkPermission ( v0 );
/* .line 228 */
v0 = android.text.TextUtils .isEmpty ( p1 );
final String v1 = "APNManagerMode"; // const-string v1, "APNManagerMode"
if ( v0 != null) { // if-eqz v0, :cond_0
/* .line 229 */
final String v0 = "addAPNForNumeric:: Invalidate numeric"; // const-string v0, "addAPNForNumeric:: Invalidate numeric"
android.util.Slog .e ( v1,v0 );
/* .line 230 */
return;
/* .line 232 */
} // :cond_0
/* new-instance v0, Landroid/content/ContentValues; */
/* invoke-direct {v0}, Landroid/content/ContentValues;-><init>()V */
/* .line 233 */
/* .local v0, "values":Landroid/content/ContentValues; */
final String v2 = "name"; // const-string v2, "name"
v3 = this.mName;
(( android.content.ContentValues ) v0 ).put ( v2, v3 ); // invoke-virtual {v0, v2, v3}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V
/* .line 234 */
final String v2 = "apn"; // const-string v2, "apn"
v3 = this.mApn;
(( android.content.ContentValues ) v0 ).put ( v2, v3 ); // invoke-virtual {v0, v2, v3}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V
/* .line 235 */
/* const-string/jumbo v2, "user" */
v3 = this.mUser;
(( android.content.ContentValues ) v0 ).put ( v2, v3 ); // invoke-virtual {v0, v2, v3}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V
/* .line 236 */
final String v2 = "password"; // const-string v2, "password"
v3 = this.mPassword;
(( android.content.ContentValues ) v0 ).put ( v2, v3 ); // invoke-virtual {v0, v2, v3}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V
/* .line 237 */
/* iget v2, p2, Lcom/miui/enterprise/sdk/APNConfig;->mAuthType:I */
java.lang.Integer .valueOf ( v2 );
final String v3 = "authtype"; // const-string v3, "authtype"
(( android.content.ContentValues ) v0 ).put ( v3, v2 ); // invoke-virtual {v0, v3, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V
/* .line 238 */
/* iget v2, p2, Lcom/miui/enterprise/sdk/APNConfig;->mBearer:I */
java.lang.Integer .valueOf ( v2 );
final String v3 = "bearer"; // const-string v3, "bearer"
(( android.content.ContentValues ) v0 ).put ( v3, v2 ); // invoke-virtual {v0, v3, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V
/* .line 239 */
int v2 = 0; // const/4 v2, 0x0
int v3 = 3; // const/4 v3, 0x3
(( java.lang.String ) p1 ).substring ( v2, v3 ); // invoke-virtual {p1, v2, v3}, Ljava/lang/String;->substring(II)Ljava/lang/String;
final String v4 = "mcc"; // const-string v4, "mcc"
(( android.content.ContentValues ) v0 ).put ( v4, v2 ); // invoke-virtual {v0, v4, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V
/* .line 240 */
int v2 = 5; // const/4 v2, 0x5
(( java.lang.String ) p1 ).substring ( v3, v2 ); // invoke-virtual {p1, v3, v2}, Ljava/lang/String;->substring(II)Ljava/lang/String;
final String v3 = "mnc"; // const-string v3, "mnc"
(( android.content.ContentValues ) v0 ).put ( v3, v2 ); // invoke-virtual {v0, v3, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V
/* .line 241 */
final String v2 = "numeric"; // const-string v2, "numeric"
(( android.content.ContentValues ) v0 ).put ( v2, p1 ); // invoke-virtual {v0, v2, p1}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V
/* .line 244 */
/* invoke-direct {p0}, Lcom/miui/server/enterprise/APNManagerService;->getContentResolver()Landroid/content/ContentResolver; */
v3 = android.provider.Telephony$Carriers.CONTENT_URI;
(( android.content.ContentResolver ) v2 ).insert ( v3, v0 ); // invoke-virtual {v2, v3, v0}, Landroid/content/ContentResolver;->insert(Landroid/net/Uri;Landroid/content/ContentValues;)Landroid/net/Uri;
/* .line 245 */
/* .local v2, "uri":Landroid/net/Uri; */
/* new-instance v3, Ljava/lang/StringBuilder; */
/* invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V */
final String v4 = "addAPNForNumeric:: New apn config: "; // const-string v4, "addAPNForNumeric:: New apn config: "
(( java.lang.StringBuilder ) v3 ).append ( v4 ); // invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v3 ).append ( v2 ); // invoke-virtual {v3, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v3 ).toString ( ); // invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
android.util.Slog .d ( v1,v3 );
/* .line 246 */
return;
} // .end method
public Boolean deleteAPN ( java.lang.String p0 ) {
/* .locals 6 */
/* .param p1, "name" # Ljava/lang/String; */
/* .line 329 */
v0 = this.mContext;
com.miui.server.enterprise.ServiceUtils .checkPermission ( v0 );
/* .line 331 */
v0 = android.text.TextUtils .isEmpty ( p1 );
int v1 = 0; // const/4 v1, 0x0
final String v2 = "APNManagerMode"; // const-string v2, "APNManagerMode"
if ( v0 != null) { // if-eqz v0, :cond_0
/* .line 332 */
final String v0 = "neither name can\'t be null"; // const-string v0, "neither name can\'t be null"
android.util.Slog .e ( v2,v0 );
/* .line 333 */
/* .line 335 */
} // :cond_0
/* invoke-direct {p0}, Lcom/miui/server/enterprise/APNManagerService;->getContentResolver()Landroid/content/ContentResolver; */
v3 = android.provider.Telephony$Carriers.CONTENT_URI;
/* .line 336 */
/* invoke-direct {p0, p1}, Lcom/miui/server/enterprise/APNManagerService;->buildNameSelection(Ljava/lang/String;)Ljava/lang/String; */
/* .line 335 */
int v5 = 0; // const/4 v5, 0x0
v0 = (( android.content.ContentResolver ) v0 ).delete ( v3, v4, v5 ); // invoke-virtual {v0, v3, v4, v5}, Landroid/content/ContentResolver;->delete(Landroid/net/Uri;Ljava/lang/String;[Ljava/lang/String;)I
/* .line 337 */
/* .local v0, "count":I */
/* new-instance v3, Ljava/lang/StringBuilder; */
/* invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V */
final String v4 = "Delete apn "; // const-string v4, "Delete apn "
(( java.lang.StringBuilder ) v3 ).append ( v4 ); // invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v3 ).append ( v0 ); // invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;
final String v4 = "rows"; // const-string v4, "rows"
(( java.lang.StringBuilder ) v3 ).append ( v4 ); // invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v3 ).toString ( ); // invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
android.util.Slog .d ( v2,v3 );
/* .line 338 */
/* if-lez v0, :cond_1 */
int v1 = 1; // const/4 v1, 0x1
} // :cond_1
} // .end method
public void deleteAPNForNumeric ( java.lang.String p0, java.lang.String p1 ) {
/* .locals 5 */
/* .param p1, "numeric" # Ljava/lang/String; */
/* .param p2, "name" # Ljava/lang/String; */
/* .line 317 */
v0 = this.mContext;
com.miui.server.enterprise.ServiceUtils .checkPermission ( v0 );
/* .line 318 */
v0 = android.text.TextUtils .isEmpty ( p2 );
final String v1 = "APNManagerMode"; // const-string v1, "APNManagerMode"
/* if-nez v0, :cond_1 */
v0 = android.text.TextUtils .isEmpty ( p1 );
if ( v0 != null) { // if-eqz v0, :cond_0
/* .line 322 */
} // :cond_0
/* invoke-direct {p0}, Lcom/miui/server/enterprise/APNManagerService;->getContentResolver()Landroid/content/ContentResolver; */
v2 = android.provider.Telephony$Carriers.CONTENT_URI;
/* .line 323 */
/* invoke-direct {p0, p1, p2}, Lcom/miui/server/enterprise/APNManagerService;->buildNumericAndNameSelection(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String; */
/* .line 322 */
int v4 = 0; // const/4 v4, 0x0
v0 = (( android.content.ContentResolver ) v0 ).delete ( v2, v3, v4 ); // invoke-virtual {v0, v2, v3, v4}, Landroid/content/ContentResolver;->delete(Landroid/net/Uri;Ljava/lang/String;[Ljava/lang/String;)I
/* .line 324 */
/* .local v0, "count":I */
/* new-instance v2, Ljava/lang/StringBuilder; */
/* invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V */
final String v3 = "Delete apn "; // const-string v3, "Delete apn "
(( java.lang.StringBuilder ) v2 ).append ( v3 ); // invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v2 ).append ( v0 ); // invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;
final String v3 = "rows"; // const-string v3, "rows"
(( java.lang.StringBuilder ) v2 ).append ( v3 ); // invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v2 ).toString ( ); // invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
android.util.Slog .d ( v1,v2 );
/* .line 325 */
return;
/* .line 319 */
} // .end local v0 # "count":I
} // :cond_1
} // :goto_0
final String v0 = "neither name or numeric can\'t be null"; // const-string v0, "neither name or numeric can\'t be null"
android.util.Slog .e ( v1,v0 );
/* .line 320 */
return;
} // .end method
public Boolean editAPN ( java.lang.String p0, com.miui.enterprise.sdk.APNConfig p1 ) {
/* .locals 7 */
/* .param p1, "name" # Ljava/lang/String; */
/* .param p2, "config" # Lcom/miui/enterprise/sdk/APNConfig; */
/* .line 365 */
v0 = this.mContext;
com.miui.server.enterprise.ServiceUtils .checkPermission ( v0 );
/* .line 367 */
v0 = this.mNumeric;
v0 = android.text.TextUtils .isEmpty ( v0 );
if ( v0 != null) { // if-eqz v0, :cond_0
/* .line 368 */
/* invoke-direct {p0}, Lcom/miui/server/enterprise/APNManagerService;->getNumeric()Ljava/lang/String; */
/* .local v0, "numeric":Ljava/lang/String; */
/* .line 370 */
} // .end local v0 # "numeric":Ljava/lang/String;
} // :cond_0
v0 = this.mNumeric;
/* .line 372 */
/* .restart local v0 # "numeric":Ljava/lang/String; */
} // :goto_0
v1 = android.text.TextUtils .isEmpty ( p1 );
int v2 = 0; // const/4 v2, 0x0
/* if-nez v1, :cond_13 */
v1 = android.text.TextUtils .isEmpty ( v0 );
if ( v1 != null) { // if-eqz v1, :cond_1
/* goto/16 :goto_3 */
/* .line 376 */
} // :cond_1
/* new-instance v1, Landroid/content/ContentValues; */
/* invoke-direct {v1}, Landroid/content/ContentValues;-><init>()V */
/* .line 377 */
/* .local v1, "values":Landroid/content/ContentValues; */
final String v3 = "name"; // const-string v3, "name"
v4 = this.mName;
(( android.content.ContentValues ) v1 ).put ( v3, v4 ); // invoke-virtual {v1, v3, v4}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V
/* .line 378 */
final String v3 = "apn"; // const-string v3, "apn"
v4 = this.mApn;
(( android.content.ContentValues ) v1 ).put ( v3, v4 ); // invoke-virtual {v1, v3, v4}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V
/* .line 379 */
/* const-string/jumbo v3, "user" */
v4 = this.mUser;
(( android.content.ContentValues ) v1 ).put ( v3, v4 ); // invoke-virtual {v1, v3, v4}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V
/* .line 380 */
final String v3 = "password"; // const-string v3, "password"
v4 = this.mPassword;
(( android.content.ContentValues ) v1 ).put ( v3, v4 ); // invoke-virtual {v1, v3, v4}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V
/* .line 381 */
/* iget v3, p2, Lcom/miui/enterprise/sdk/APNConfig;->mAuthType:I */
java.lang.Integer .valueOf ( v3 );
final String v4 = "authtype"; // const-string v4, "authtype"
(( android.content.ContentValues ) v1 ).put ( v4, v3 ); // invoke-virtual {v1, v4, v3}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V
/* .line 382 */
/* iget v3, p2, Lcom/miui/enterprise/sdk/APNConfig;->mBearer:I */
java.lang.Integer .valueOf ( v3 );
final String v4 = "bearer"; // const-string v4, "bearer"
(( android.content.ContentValues ) v1 ).put ( v4, v3 ); // invoke-virtual {v1, v4, v3}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V
/* .line 383 */
v3 = this.mMcc;
v3 = android.text.TextUtils .isEmpty ( v3 );
int v4 = 3; // const/4 v4, 0x3
if ( v3 != null) { // if-eqz v3, :cond_2
(( java.lang.String ) v0 ).substring ( v2, v4 ); // invoke-virtual {v0, v2, v4}, Ljava/lang/String;->substring(II)Ljava/lang/String;
} // :cond_2
v3 = this.mMcc;
} // :goto_1
final String v5 = "mcc"; // const-string v5, "mcc"
(( android.content.ContentValues ) v1 ).put ( v5, v3 ); // invoke-virtual {v1, v5, v3}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V
/* .line 384 */
v3 = this.mMnc;
v3 = android.text.TextUtils .isEmpty ( v3 );
if ( v3 != null) { // if-eqz v3, :cond_3
int v3 = 5; // const/4 v3, 0x5
(( java.lang.String ) v0 ).substring ( v4, v3 ); // invoke-virtual {v0, v4, v3}, Ljava/lang/String;->substring(II)Ljava/lang/String;
} // :cond_3
v3 = this.mMnc;
} // :goto_2
final String v4 = "mnc"; // const-string v4, "mnc"
(( android.content.ContentValues ) v1 ).put ( v4, v3 ); // invoke-virtual {v1, v4, v3}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V
/* .line 385 */
final String v3 = "numeric"; // const-string v3, "numeric"
(( android.content.ContentValues ) v1 ).put ( v3, v0 ); // invoke-virtual {v1, v3, v0}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V
/* .line 387 */
/* iget v3, p2, Lcom/miui/enterprise/sdk/APNConfig;->mCarrier_enabled:I */
int v4 = -1; // const/4 v4, -0x1
/* if-eq v3, v4, :cond_4 */
/* .line 388 */
/* iget v3, p2, Lcom/miui/enterprise/sdk/APNConfig;->mCarrier_enabled:I */
java.lang.Integer .valueOf ( v3 );
final String v5 = "carrier_enabled"; // const-string v5, "carrier_enabled"
(( android.content.ContentValues ) v1 ).put ( v5, v3 ); // invoke-virtual {v1, v5, v3}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V
/* .line 390 */
} // :cond_4
/* iget v3, p2, Lcom/miui/enterprise/sdk/APNConfig;->mCurrent:I */
/* if-eq v3, v4, :cond_5 */
/* .line 391 */
/* iget v3, p2, Lcom/miui/enterprise/sdk/APNConfig;->mCarrier_enabled:I */
java.lang.Integer .valueOf ( v3 );
final String v4 = "current"; // const-string v4, "current"
(( android.content.ContentValues ) v1 ).put ( v4, v3 ); // invoke-virtual {v1, v4, v3}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V
/* .line 394 */
} // :cond_5
v3 = this.mMmsc;
if ( v3 != null) { // if-eqz v3, :cond_6
/* .line 395 */
final String v3 = "mmsc"; // const-string v3, "mmsc"
v4 = this.mMmsc;
(( android.content.ContentValues ) v1 ).put ( v3, v4 ); // invoke-virtual {v1, v3, v4}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V
/* .line 398 */
} // :cond_6
v3 = this.mMmsport;
if ( v3 != null) { // if-eqz v3, :cond_7
/* .line 399 */
final String v3 = "mmsport"; // const-string v3, "mmsport"
v4 = this.mMmsport;
(( android.content.ContentValues ) v1 ).put ( v3, v4 ); // invoke-virtual {v1, v3, v4}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V
/* .line 401 */
} // :cond_7
v3 = this.mMmsproxy;
if ( v3 != null) { // if-eqz v3, :cond_8
/* .line 402 */
final String v3 = "mmsproxy"; // const-string v3, "mmsproxy"
v4 = this.mMmsproxy;
(( android.content.ContentValues ) v1 ).put ( v3, v4 ); // invoke-virtual {v1, v3, v4}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V
/* .line 403 */
} // :cond_8
v3 = this.mMvno_match_data;
if ( v3 != null) { // if-eqz v3, :cond_9
/* .line 404 */
final String v3 = "mvno_match_data"; // const-string v3, "mvno_match_data"
v4 = this.mMvno_match_data;
(( android.content.ContentValues ) v1 ).put ( v3, v4 ); // invoke-virtual {v1, v3, v4}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V
/* .line 405 */
} // :cond_9
v3 = this.mMvno_type;
if ( v3 != null) { // if-eqz v3, :cond_a
/* .line 406 */
final String v3 = "mvno_type"; // const-string v3, "mvno_type"
v4 = this.mMvno_type;
(( android.content.ContentValues ) v1 ).put ( v3, v4 ); // invoke-virtual {v1, v3, v4}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V
/* .line 407 */
} // :cond_a
v3 = this.mPort;
if ( v3 != null) { // if-eqz v3, :cond_b
/* .line 408 */
final String v3 = "port"; // const-string v3, "port"
v4 = this.mPort;
(( android.content.ContentValues ) v1 ).put ( v3, v4 ); // invoke-virtual {v1, v3, v4}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V
/* .line 409 */
} // :cond_b
v3 = this.mProtocol;
if ( v3 != null) { // if-eqz v3, :cond_c
/* .line 410 */
final String v3 = "protocol"; // const-string v3, "protocol"
v4 = this.mProtocol;
(( android.content.ContentValues ) v1 ).put ( v3, v4 ); // invoke-virtual {v1, v3, v4}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V
/* .line 411 */
} // :cond_c
v3 = this.mProxy;
if ( v3 != null) { // if-eqz v3, :cond_d
/* .line 412 */
final String v3 = "proxy"; // const-string v3, "proxy"
v4 = this.mProxy;
(( android.content.ContentValues ) v1 ).put ( v3, v4 ); // invoke-virtual {v1, v3, v4}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V
/* .line 413 */
} // :cond_d
v3 = this.mRoaming_protocol;
if ( v3 != null) { // if-eqz v3, :cond_e
/* .line 414 */
final String v3 = "roaming_protocol"; // const-string v3, "roaming_protocol"
v4 = this.mRoaming_protocol;
(( android.content.ContentValues ) v1 ).put ( v3, v4 ); // invoke-virtual {v1, v3, v4}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V
/* .line 415 */
} // :cond_e
v3 = this.mServer;
if ( v3 != null) { // if-eqz v3, :cond_f
/* .line 416 */
/* const-string/jumbo v3, "server" */
v4 = this.mServer;
(( android.content.ContentValues ) v1 ).put ( v3, v4 ); // invoke-virtual {v1, v3, v4}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V
/* .line 417 */
} // :cond_f
v3 = this.mSub_id;
if ( v3 != null) { // if-eqz v3, :cond_10
/* .line 418 */
/* const-string/jumbo v3, "sub_id" */
v4 = this.mSub_id;
(( android.content.ContentValues ) v1 ).put ( v3, v4 ); // invoke-virtual {v1, v3, v4}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V
/* .line 419 */
} // :cond_10
v3 = this.mType;
if ( v3 != null) { // if-eqz v3, :cond_11
/* .line 420 */
/* const-string/jumbo v3, "type" */
v4 = this.mType;
(( android.content.ContentValues ) v1 ).put ( v3, v4 ); // invoke-virtual {v1, v3, v4}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V
/* .line 422 */
} // :cond_11
/* invoke-direct {p0}, Lcom/miui/server/enterprise/APNManagerService;->getContentResolver()Landroid/content/ContentResolver; */
v4 = android.provider.Telephony$Carriers.CONTENT_URI;
/* .line 423 */
/* invoke-direct {p0, v0, p1}, Lcom/miui/server/enterprise/APNManagerService;->buildNumericAndNameSelection(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String; */
/* .line 422 */
int v6 = 0; // const/4 v6, 0x0
v3 = (( android.content.ContentResolver ) v3 ).update ( v4, v1, v5, v6 ); // invoke-virtual {v3, v4, v1, v5, v6}, Landroid/content/ContentResolver;->update(Landroid/net/Uri;Landroid/content/ContentValues;Ljava/lang/String;[Ljava/lang/String;)I
/* .line 424 */
/* .local v3, "count":I */
/* if-lez v3, :cond_12 */
int v2 = 1; // const/4 v2, 0x1
} // :cond_12
/* .line 373 */
} // .end local v1 # "values":Landroid/content/ContentValues;
} // .end local v3 # "count":I
} // :cond_13
} // :goto_3
final String v1 = "APNManagerMode"; // const-string v1, "APNManagerMode"
final String v3 = "neither name or numeric can\'t be null"; // const-string v3, "neither name or numeric can\'t be null"
android.util.Slog .e ( v1,v3 );
/* .line 374 */
} // .end method
public void editAPNForNumeric ( java.lang.String p0, java.lang.String p1, com.miui.enterprise.sdk.APNConfig p2 ) {
/* .locals 6 */
/* .param p1, "numeric" # Ljava/lang/String; */
/* .param p2, "name" # Ljava/lang/String; */
/* .param p3, "config" # Lcom/miui/enterprise/sdk/APNConfig; */
/* .line 343 */
v0 = this.mContext;
com.miui.server.enterprise.ServiceUtils .checkPermission ( v0 );
/* .line 344 */
v0 = android.text.TextUtils .isEmpty ( p2 );
final String v1 = "APNManagerMode"; // const-string v1, "APNManagerMode"
/* if-nez v0, :cond_1 */
v0 = android.text.TextUtils .isEmpty ( p1 );
if ( v0 != null) { // if-eqz v0, :cond_0
/* goto/16 :goto_0 */
/* .line 348 */
} // :cond_0
/* new-instance v0, Landroid/content/ContentValues; */
/* invoke-direct {v0}, Landroid/content/ContentValues;-><init>()V */
/* .line 349 */
/* .local v0, "values":Landroid/content/ContentValues; */
final String v2 = "name"; // const-string v2, "name"
v3 = this.mName;
(( android.content.ContentValues ) v0 ).put ( v2, v3 ); // invoke-virtual {v0, v2, v3}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V
/* .line 350 */
final String v2 = "apn"; // const-string v2, "apn"
v3 = this.mApn;
(( android.content.ContentValues ) v0 ).put ( v2, v3 ); // invoke-virtual {v0, v2, v3}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V
/* .line 351 */
/* const-string/jumbo v2, "user" */
v3 = this.mUser;
(( android.content.ContentValues ) v0 ).put ( v2, v3 ); // invoke-virtual {v0, v2, v3}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V
/* .line 352 */
final String v2 = "password"; // const-string v2, "password"
v3 = this.mPassword;
(( android.content.ContentValues ) v0 ).put ( v2, v3 ); // invoke-virtual {v0, v2, v3}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V
/* .line 353 */
/* iget v2, p3, Lcom/miui/enterprise/sdk/APNConfig;->mAuthType:I */
java.lang.Integer .valueOf ( v2 );
final String v3 = "authtype"; // const-string v3, "authtype"
(( android.content.ContentValues ) v0 ).put ( v3, v2 ); // invoke-virtual {v0, v3, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V
/* .line 354 */
/* iget v2, p3, Lcom/miui/enterprise/sdk/APNConfig;->mBearer:I */
java.lang.Integer .valueOf ( v2 );
final String v3 = "bearer"; // const-string v3, "bearer"
(( android.content.ContentValues ) v0 ).put ( v3, v2 ); // invoke-virtual {v0, v3, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V
/* .line 355 */
int v2 = 0; // const/4 v2, 0x0
int v3 = 3; // const/4 v3, 0x3
(( java.lang.String ) p1 ).substring ( v2, v3 ); // invoke-virtual {p1, v2, v3}, Ljava/lang/String;->substring(II)Ljava/lang/String;
final String v4 = "mcc"; // const-string v4, "mcc"
(( android.content.ContentValues ) v0 ).put ( v4, v2 ); // invoke-virtual {v0, v4, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V
/* .line 356 */
int v2 = 5; // const/4 v2, 0x5
(( java.lang.String ) p1 ).substring ( v3, v2 ); // invoke-virtual {p1, v3, v2}, Ljava/lang/String;->substring(II)Ljava/lang/String;
final String v3 = "mnc"; // const-string v3, "mnc"
(( android.content.ContentValues ) v0 ).put ( v3, v2 ); // invoke-virtual {v0, v3, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V
/* .line 357 */
final String v2 = "numeric"; // const-string v2, "numeric"
(( android.content.ContentValues ) v0 ).put ( v2, p1 ); // invoke-virtual {v0, v2, p1}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V
/* .line 358 */
/* invoke-direct {p0}, Lcom/miui/server/enterprise/APNManagerService;->getContentResolver()Landroid/content/ContentResolver; */
v3 = android.provider.Telephony$Carriers.CONTENT_URI;
/* .line 359 */
/* invoke-direct {p0, p1, p2}, Lcom/miui/server/enterprise/APNManagerService;->buildNumericAndNameSelection(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String; */
/* .line 358 */
int v5 = 0; // const/4 v5, 0x0
v2 = (( android.content.ContentResolver ) v2 ).update ( v3, v0, v4, v5 ); // invoke-virtual {v2, v3, v0, v4, v5}, Landroid/content/ContentResolver;->update(Landroid/net/Uri;Landroid/content/ContentValues;Ljava/lang/String;[Ljava/lang/String;)I
/* .line 360 */
/* .local v2, "count":I */
/* new-instance v3, Ljava/lang/StringBuilder; */
/* invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V */
final String v4 = "Update apn "; // const-string v4, "Update apn "
(( java.lang.StringBuilder ) v3 ).append ( v4 ); // invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v3 ).append ( v2 ); // invoke-virtual {v3, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;
final String v4 = "rows"; // const-string v4, "rows"
(( java.lang.StringBuilder ) v3 ).append ( v4 ); // invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v3 ).toString ( ); // invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
android.util.Slog .d ( v1,v3 );
/* .line 361 */
return;
/* .line 345 */
} // .end local v0 # "values":Landroid/content/ContentValues;
} // .end local v2 # "count":I
} // :cond_1
} // :goto_0
final String v0 = "neither name or numeric can\'t be null"; // const-string v0, "neither name or numeric can\'t be null"
android.util.Slog .e ( v1,v0 );
/* .line 346 */
return;
} // .end method
public com.miui.enterprise.sdk.APNConfig getAPN ( java.lang.String p0 ) {
/* .locals 27 */
/* .param p1, "name" # Ljava/lang/String; */
/* .line 178 */
/* move-object/from16 v1, p0 */
v0 = this.mContext;
com.miui.server.enterprise.ServiceUtils .checkPermission ( v0 );
/* .line 180 */
/* invoke-direct/range {p0 ..p0}, Lcom/miui/server/enterprise/APNManagerService;->getContentResolver()Landroid/content/ContentResolver; */
v3 = android.provider.Telephony$Carriers.CONTENT_URI;
final String v4 = "name"; // const-string v4, "name"
final String v5 = "apn"; // const-string v5, "apn"
/* const-string/jumbo v6, "user" */
final String v7 = "password"; // const-string v7, "password"
final String v8 = "authtype"; // const-string v8, "authtype"
final String v9 = "bearer"; // const-string v9, "bearer"
final String v10 = "mcc"; // const-string v10, "mcc"
final String v11 = "mnc"; // const-string v11, "mnc"
final String v12 = "numeric"; // const-string v12, "numeric"
final String v13 = "carrier_enabled"; // const-string v13, "carrier_enabled"
final String v14 = "current"; // const-string v14, "current"
final String v15 = "mmsc"; // const-string v15, "mmsc"
final String v16 = "mmsport"; // const-string v16, "mmsport"
final String v17 = "mmsproxy"; // const-string v17, "mmsproxy"
final String v18 = "mvno_match_data"; // const-string v18, "mvno_match_data"
final String v19 = "mvno_type"; // const-string v19, "mvno_type"
final String v20 = "port"; // const-string v20, "port"
final String v21 = "protocol"; // const-string v21, "protocol"
final String v22 = "proxy"; // const-string v22, "proxy"
final String v23 = "roaming_protocol"; // const-string v23, "roaming_protocol"
/* const-string/jumbo v24, "server" */
/* const-string/jumbo v25, "sub_id" */
/* const-string/jumbo v26, "type" */
/* filled-new-array/range {v4 ..v26}, [Ljava/lang/String; */
/* .line 187 */
/* invoke-direct/range {p0 ..p1}, Lcom/miui/server/enterprise/APNManagerService;->buildNameSelection(Ljava/lang/String;)Ljava/lang/String; */
int v6 = 0; // const/4 v6, 0x0
final String v7 = "name ASC"; // const-string v7, "name ASC"
/* .line 180 */
/* invoke-virtual/range {v2 ..v7}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor; */
/* .line 190 */
/* .local v2, "cursor":Landroid/database/Cursor; */
if ( v2 != null) { // if-eqz v2, :cond_0
v0 = try { // :try_start_0
/* if-lez v0, :cond_0 */
/* .line 191 */
/* .line 192 */
/* new-instance v0, Lcom/miui/enterprise/sdk/APNConfig; */
/* invoke-direct {v0}, Lcom/miui/enterprise/sdk/APNConfig;-><init>()V */
/* .line 193 */
/* .local v0, "configItem":Lcom/miui/enterprise/sdk/APNConfig; */
int v3 = 0; // const/4 v3, 0x0
this.mName = v3;
/* .line 194 */
int v3 = 1; // const/4 v3, 0x1
this.mApn = v3;
/* .line 195 */
int v3 = 2; // const/4 v3, 0x2
this.mUser = v3;
/* .line 196 */
int v3 = 3; // const/4 v3, 0x3
this.mPassword = v3;
/* .line 197 */
v3 = int v3 = 4; // const/4 v3, 0x4
/* iput v3, v0, Lcom/miui/enterprise/sdk/APNConfig;->mAuthType:I */
/* .line 198 */
v3 = int v3 = 5; // const/4 v3, 0x5
/* iput v3, v0, Lcom/miui/enterprise/sdk/APNConfig;->mBearer:I */
/* .line 199 */
int v3 = 6; // const/4 v3, 0x6
this.mMcc = v3;
/* .line 200 */
int v3 = 7; // const/4 v3, 0x7
this.mMnc = v3;
/* .line 201 */
/* const/16 v3, 0x8 */
this.mNumeric = v3;
/* .line 202 */
v3 = /* const/16 v3, 0x9 */
/* iput v3, v0, Lcom/miui/enterprise/sdk/APNConfig;->mCarrier_enabled:I */
/* .line 203 */
v3 = /* const/16 v3, 0xa */
/* iput v3, v0, Lcom/miui/enterprise/sdk/APNConfig;->mCurrent:I */
/* .line 204 */
/* const/16 v3, 0xb */
this.mMmsc = v3;
/* .line 205 */
/* const/16 v3, 0xc */
this.mMmsport = v3;
/* .line 206 */
/* const/16 v3, 0xd */
this.mMmsproxy = v3;
/* .line 207 */
/* const/16 v3, 0xe */
this.mMvno_match_data = v3;
/* .line 208 */
/* const/16 v3, 0xf */
this.mMvno_type = v3;
/* .line 209 */
/* const/16 v3, 0x10 */
this.mPort = v3;
/* .line 210 */
/* const/16 v3, 0x11 */
this.mProtocol = v3;
/* .line 211 */
/* const/16 v3, 0x12 */
this.mProxy = v3;
/* .line 212 */
/* const/16 v3, 0x13 */
this.mRoaming_protocol = v3;
/* .line 213 */
/* const/16 v3, 0x14 */
this.mServer = v3;
/* .line 214 */
/* const/16 v3, 0x15 */
this.mSub_id = v3;
/* .line 215 */
/* const/16 v3, 0x16 */
this.mType = v3;
/* :try_end_0 */
/* .catchall {:try_start_0 ..:try_end_0} :catchall_0 */
/* .line 216 */
/* nop */
/* .line 221 */
/* invoke-direct {v1, v2}, Lcom/miui/server/enterprise/APNManagerService;->closeCursor(Landroid/database/Cursor;)V */
/* .line 216 */
/* .line 221 */
} // .end local v0 # "configItem":Lcom/miui/enterprise/sdk/APNConfig;
/* :catchall_0 */
/* move-exception v0 */
/* invoke-direct {v1, v2}, Lcom/miui/server/enterprise/APNManagerService;->closeCursor(Landroid/database/Cursor;)V */
/* .line 222 */
/* throw v0 */
/* .line 218 */
} // :cond_0
/* nop */
/* .line 221 */
/* invoke-direct {v1, v2}, Lcom/miui/server/enterprise/APNManagerService;->closeCursor(Landroid/database/Cursor;)V */
/* .line 218 */
int v0 = 0; // const/4 v0, 0x0
} // .end method
public Integer getAPNActiveMode ( ) {
/* .locals 3 */
/* .line 497 */
v0 = this.mContext;
com.miui.server.enterprise.ServiceUtils .checkPermission ( v0 );
/* .line 498 */
v0 = this.mContext;
final String v1 = "ep_apn_switch_mode"; // const-string v1, "ep_apn_switch_mode"
int v2 = 0; // const/4 v2, 0x0
v0 = com.miui.enterprise.settings.EnterpriseSettings .getInt ( v0,v1,v2,v2 );
} // .end method
public com.miui.enterprise.sdk.APNConfig getAPNForNumeric ( java.lang.String p0, java.lang.String p1 ) {
/* .locals 9 */
/* .param p1, "numeric" # Ljava/lang/String; */
/* .param p2, "name" # Ljava/lang/String; */
/* .line 152 */
v0 = this.mContext;
com.miui.server.enterprise.ServiceUtils .checkPermission ( v0 );
/* .line 153 */
/* invoke-direct {p0}, Lcom/miui/server/enterprise/APNManagerService;->getContentResolver()Landroid/content/ContentResolver; */
v2 = android.provider.Telephony$Carriers.CONTENT_URI;
final String v3 = "name"; // const-string v3, "name"
final String v4 = "apn"; // const-string v4, "apn"
/* const-string/jumbo v5, "user" */
final String v6 = "password"; // const-string v6, "password"
final String v7 = "authtype"; // const-string v7, "authtype"
final String v8 = "bearer"; // const-string v8, "bearer"
/* filled-new-array/range {v3 ..v8}, [Ljava/lang/String; */
/* .line 155 */
/* invoke-direct {p0, p1, p2}, Lcom/miui/server/enterprise/APNManagerService;->buildNumericAndNameSelection(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String; */
int v5 = 0; // const/4 v5, 0x0
final String v6 = "name ASC"; // const-string v6, "name ASC"
/* .line 153 */
/* invoke-virtual/range {v1 ..v6}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor; */
/* .line 158 */
/* .local v0, "cursor":Landroid/database/Cursor; */
if ( v0 != null) { // if-eqz v0, :cond_0
v1 = try { // :try_start_0
/* if-lez v1, :cond_0 */
/* .line 159 */
/* .line 160 */
/* new-instance v1, Lcom/miui/enterprise/sdk/APNConfig; */
/* invoke-direct {v1}, Lcom/miui/enterprise/sdk/APNConfig;-><init>()V */
/* .line 161 */
/* .local v1, "configItem":Lcom/miui/enterprise/sdk/APNConfig; */
int v2 = 0; // const/4 v2, 0x0
this.mName = v2;
/* .line 162 */
int v2 = 1; // const/4 v2, 0x1
this.mApn = v2;
/* .line 163 */
int v2 = 2; // const/4 v2, 0x2
this.mUser = v2;
/* .line 164 */
int v2 = 3; // const/4 v2, 0x3
this.mPassword = v2;
/* .line 165 */
v2 = int v2 = 4; // const/4 v2, 0x4
/* iput v2, v1, Lcom/miui/enterprise/sdk/APNConfig;->mAuthType:I */
/* .line 166 */
v2 = int v2 = 5; // const/4 v2, 0x5
/* iput v2, v1, Lcom/miui/enterprise/sdk/APNConfig;->mBearer:I */
/* :try_end_0 */
/* .catchall {:try_start_0 ..:try_end_0} :catchall_0 */
/* .line 167 */
/* nop */
/* .line 172 */
/* invoke-direct {p0, v0}, Lcom/miui/server/enterprise/APNManagerService;->closeCursor(Landroid/database/Cursor;)V */
/* .line 167 */
/* .line 172 */
} // .end local v1 # "configItem":Lcom/miui/enterprise/sdk/APNConfig;
/* :catchall_0 */
/* move-exception v1 */
/* invoke-direct {p0, v0}, Lcom/miui/server/enterprise/APNManagerService;->closeCursor(Landroid/database/Cursor;)V */
/* .line 173 */
/* throw v1 */
/* .line 169 */
} // :cond_0
/* nop */
/* .line 172 */
/* invoke-direct {p0, v0}, Lcom/miui/server/enterprise/APNManagerService;->closeCursor(Landroid/database/Cursor;)V */
/* .line 169 */
int v1 = 0; // const/4 v1, 0x0
} // .end method
public java.util.List getAPNList ( ) {
/* .locals 28 */
/* .annotation system Ldalvik/annotation/Signature; */
/* value = { */
/* "()", */
/* "Ljava/util/List<", */
/* "Lcom/miui/enterprise/sdk/APNConfig;", */
/* ">;" */
/* } */
} // .end annotation
/* .line 100 */
/* move-object/from16 v1, p0 */
v0 = this.mContext;
com.miui.server.enterprise.ServiceUtils .checkPermission ( v0 );
/* .line 101 */
/* invoke-direct/range {p0 ..p0}, Lcom/miui/server/enterprise/APNManagerService;->getNumeric()Ljava/lang/String; */
/* .line 102 */
/* .local v2, "numeric":Ljava/lang/String; */
/* invoke-direct/range {p0 ..p0}, Lcom/miui/server/enterprise/APNManagerService;->getContentResolver()Landroid/content/ContentResolver; */
v4 = android.provider.Telephony$Carriers.CONTENT_URI;
final String v5 = "name"; // const-string v5, "name"
final String v6 = "apn"; // const-string v6, "apn"
/* const-string/jumbo v7, "user" */
final String v8 = "password"; // const-string v8, "password"
final String v9 = "authtype"; // const-string v9, "authtype"
final String v10 = "bearer"; // const-string v10, "bearer"
final String v11 = "mcc"; // const-string v11, "mcc"
final String v12 = "mnc"; // const-string v12, "mnc"
final String v13 = "numeric"; // const-string v13, "numeric"
final String v14 = "carrier_enabled"; // const-string v14, "carrier_enabled"
final String v15 = "current"; // const-string v15, "current"
final String v16 = "mmsc"; // const-string v16, "mmsc"
final String v17 = "mmsport"; // const-string v17, "mmsport"
final String v18 = "mmsproxy"; // const-string v18, "mmsproxy"
final String v19 = "mvno_match_data"; // const-string v19, "mvno_match_data"
final String v20 = "mvno_type"; // const-string v20, "mvno_type"
final String v21 = "port"; // const-string v21, "port"
final String v22 = "protocol"; // const-string v22, "protocol"
final String v23 = "proxy"; // const-string v23, "proxy"
final String v24 = "roaming_protocol"; // const-string v24, "roaming_protocol"
/* const-string/jumbo v25, "server" */
/* const-string/jumbo v26, "sub_id" */
/* const-string/jumbo v27, "type" */
/* filled-new-array/range {v5 ..v27}, [Ljava/lang/String; */
/* .line 109 */
/* invoke-direct {v1, v2}, Lcom/miui/server/enterprise/APNManagerService;->buildNumericSelection(Ljava/lang/String;)Ljava/lang/String; */
int v7 = 0; // const/4 v7, 0x0
final String v8 = "name ASC"; // const-string v8, "name ASC"
/* .line 102 */
/* invoke-virtual/range {v3 ..v8}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor; */
/* .line 110 */
/* .local v3, "cursor":Landroid/database/Cursor; */
/* new-instance v0, Ljava/util/ArrayList; */
/* invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V */
/* move-object v4, v0 */
/* .line 112 */
/* .local v4, "configs":Ljava/util/List;, "Ljava/util/List<Lcom/miui/enterprise/sdk/APNConfig;>;" */
if ( v3 != null) { // if-eqz v3, :cond_0
/* .line 113 */
try { // :try_start_0
final String v0 = "APNManagerMode"; // const-string v0, "APNManagerMode"
/* new-instance v5, Ljava/lang/StringBuilder; */
/* invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V */
final String v6 = "Query result size: "; // const-string v6, "Query result size: "
v6 = (( java.lang.StringBuilder ) v5 ).append ( v6 ); // invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v5 ).append ( v6 ); // invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v5 ).toString ( ); // invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
android.util.Slog .d ( v0,v5 );
/* .line 114 */
/* .line 115 */
v0 = } // :goto_0
/* if-nez v0, :cond_0 */
/* .line 116 */
/* new-instance v0, Lcom/miui/enterprise/sdk/APNConfig; */
/* invoke-direct {v0}, Lcom/miui/enterprise/sdk/APNConfig;-><init>()V */
/* .line 117 */
/* .local v0, "configItem":Lcom/miui/enterprise/sdk/APNConfig; */
int v5 = 0; // const/4 v5, 0x0
this.mName = v5;
/* .line 118 */
int v5 = 1; // const/4 v5, 0x1
this.mApn = v5;
/* .line 119 */
int v5 = 2; // const/4 v5, 0x2
this.mUser = v5;
/* .line 120 */
int v5 = 3; // const/4 v5, 0x3
this.mPassword = v5;
/* .line 121 */
v5 = int v5 = 4; // const/4 v5, 0x4
/* iput v5, v0, Lcom/miui/enterprise/sdk/APNConfig;->mAuthType:I */
/* .line 122 */
v5 = int v5 = 5; // const/4 v5, 0x5
/* iput v5, v0, Lcom/miui/enterprise/sdk/APNConfig;->mBearer:I */
/* .line 123 */
int v5 = 6; // const/4 v5, 0x6
this.mMcc = v5;
/* .line 124 */
int v5 = 7; // const/4 v5, 0x7
this.mMnc = v5;
/* .line 125 */
/* const/16 v5, 0x8 */
this.mNumeric = v5;
/* .line 126 */
v5 = /* const/16 v5, 0x9 */
/* iput v5, v0, Lcom/miui/enterprise/sdk/APNConfig;->mCarrier_enabled:I */
/* .line 127 */
v5 = /* const/16 v5, 0xa */
/* iput v5, v0, Lcom/miui/enterprise/sdk/APNConfig;->mCurrent:I */
/* .line 128 */
/* const/16 v5, 0xb */
this.mMmsc = v5;
/* .line 129 */
/* const/16 v5, 0xc */
this.mMmsport = v5;
/* .line 130 */
/* const/16 v5, 0xd */
this.mMmsproxy = v5;
/* .line 131 */
/* const/16 v5, 0xe */
this.mMvno_match_data = v5;
/* .line 132 */
/* const/16 v5, 0xf */
this.mMvno_type = v5;
/* .line 133 */
/* const/16 v5, 0x10 */
this.mPort = v5;
/* .line 134 */
/* const/16 v5, 0x11 */
this.mProtocol = v5;
/* .line 135 */
/* const/16 v5, 0x12 */
this.mProxy = v5;
/* .line 136 */
/* const/16 v5, 0x13 */
this.mRoaming_protocol = v5;
/* .line 137 */
/* const/16 v5, 0x14 */
this.mServer = v5;
/* .line 138 */
/* const/16 v5, 0x15 */
this.mSub_id = v5;
/* .line 139 */
/* const/16 v5, 0x16 */
this.mType = v5;
/* .line 140 */
/* .line 141 */
/* :try_end_0 */
/* .catchall {:try_start_0 ..:try_end_0} :catchall_0 */
/* .line 142 */
/* nop */
} // .end local v0 # "configItem":Lcom/miui/enterprise/sdk/APNConfig;
/* goto/16 :goto_0 */
/* .line 145 */
/* :catchall_0 */
/* move-exception v0 */
/* invoke-direct {v1, v3}, Lcom/miui/server/enterprise/APNManagerService;->closeCursor(Landroid/database/Cursor;)V */
/* .line 146 */
/* throw v0 */
/* .line 145 */
} // :cond_0
/* invoke-direct {v1, v3}, Lcom/miui/server/enterprise/APNManagerService;->closeCursor(Landroid/database/Cursor;)V */
/* .line 146 */
/* nop */
/* .line 147 */
} // .end method
public java.util.List getAPNListForNumeric ( java.lang.String p0 ) {
/* .locals 9 */
/* .param p1, "numeric" # Ljava/lang/String; */
/* .annotation system Ldalvik/annotation/Signature; */
/* value = { */
/* "(", */
/* "Ljava/lang/String;", */
/* ")", */
/* "Ljava/util/List<", */
/* "Lcom/miui/enterprise/sdk/APNConfig;", */
/* ">;" */
/* } */
} // .end annotation
/* .line 71 */
v0 = this.mContext;
com.miui.server.enterprise.ServiceUtils .checkPermission ( v0 );
/* .line 72 */
/* invoke-direct {p0}, Lcom/miui/server/enterprise/APNManagerService;->getContentResolver()Landroid/content/ContentResolver; */
v2 = android.provider.Telephony$Carriers.CONTENT_URI;
final String v3 = "name"; // const-string v3, "name"
final String v4 = "apn"; // const-string v4, "apn"
/* const-string/jumbo v5, "user" */
final String v6 = "password"; // const-string v6, "password"
final String v7 = "authtype"; // const-string v7, "authtype"
final String v8 = "bearer"; // const-string v8, "bearer"
/* filled-new-array/range {v3 ..v8}, [Ljava/lang/String; */
/* .line 74 */
/* invoke-direct {p0, p1}, Lcom/miui/server/enterprise/APNManagerService;->buildNumericSelection(Ljava/lang/String;)Ljava/lang/String; */
int v5 = 0; // const/4 v5, 0x0
final String v6 = "name ASC"; // const-string v6, "name ASC"
/* .line 72 */
/* invoke-virtual/range {v1 ..v6}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor; */
/* .line 75 */
/* .local v0, "cursor":Landroid/database/Cursor; */
/* new-instance v1, Ljava/util/ArrayList; */
/* invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V */
/* .line 77 */
/* .local v1, "configs":Ljava/util/List;, "Ljava/util/List<Lcom/miui/enterprise/sdk/APNConfig;>;" */
if ( v0 != null) { // if-eqz v0, :cond_0
/* .line 78 */
try { // :try_start_0
final String v2 = "APNManagerMode"; // const-string v2, "APNManagerMode"
/* new-instance v3, Ljava/lang/StringBuilder; */
/* invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V */
final String v4 = "Query result size: "; // const-string v4, "Query result size: "
v4 = (( java.lang.StringBuilder ) v3 ).append ( v4 ); // invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v3 ).append ( v4 ); // invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v3 ).toString ( ); // invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
android.util.Slog .d ( v2,v3 );
/* .line 79 */
/* .line 80 */
v2 = } // :goto_0
/* if-nez v2, :cond_0 */
/* .line 81 */
/* new-instance v2, Lcom/miui/enterprise/sdk/APNConfig; */
/* invoke-direct {v2}, Lcom/miui/enterprise/sdk/APNConfig;-><init>()V */
/* .line 82 */
/* .local v2, "configItem":Lcom/miui/enterprise/sdk/APNConfig; */
int v3 = 0; // const/4 v3, 0x0
this.mName = v3;
/* .line 83 */
int v3 = 1; // const/4 v3, 0x1
this.mApn = v3;
/* .line 84 */
int v3 = 2; // const/4 v3, 0x2
this.mUser = v3;
/* .line 85 */
int v3 = 3; // const/4 v3, 0x3
this.mPassword = v3;
/* .line 86 */
v3 = int v3 = 4; // const/4 v3, 0x4
/* iput v3, v2, Lcom/miui/enterprise/sdk/APNConfig;->mAuthType:I */
/* .line 87 */
v3 = int v3 = 5; // const/4 v3, 0x5
/* iput v3, v2, Lcom/miui/enterprise/sdk/APNConfig;->mBearer:I */
/* .line 88 */
/* .line 89 */
/* :try_end_0 */
/* .catchall {:try_start_0 ..:try_end_0} :catchall_0 */
/* .line 90 */
/* nop */
} // .end local v2 # "configItem":Lcom/miui/enterprise/sdk/APNConfig;
/* .line 93 */
/* :catchall_0 */
/* move-exception v2 */
/* invoke-direct {p0, v0}, Lcom/miui/server/enterprise/APNManagerService;->closeCursor(Landroid/database/Cursor;)V */
/* .line 94 */
/* throw v2 */
/* .line 93 */
} // :cond_0
/* invoke-direct {p0, v0}, Lcom/miui/server/enterprise/APNManagerService;->closeCursor(Landroid/database/Cursor;)V */
/* .line 94 */
/* nop */
/* .line 95 */
} // .end method
public java.util.List queryApn ( java.lang.String p0 ) {
/* .locals 6 */
/* .param p1, "selections" # Ljava/lang/String; */
/* .annotation system Ldalvik/annotation/Signature; */
/* value = { */
/* "(", */
/* "Ljava/lang/String;", */
/* ")", */
/* "Ljava/util/List<", */
/* "Ljava/lang/String;", */
/* ">;" */
/* } */
} // .end annotation
/* .line 503 */
/* invoke-direct {p0}, Lcom/miui/server/enterprise/APNManagerService;->getContentResolver()Landroid/content/ContentResolver; */
v1 = android.provider.Telephony$Carriers.CONTENT_URI;
final String v2 = "name"; // const-string v2, "name"
/* filled-new-array {v2}, [Ljava/lang/String; */
int v4 = 0; // const/4 v4, 0x0
final String v5 = "name ASC"; // const-string v5, "name ASC"
/* move-object v3, p1 */
/* invoke-virtual/range {v0 ..v5}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor; */
/* .line 505 */
/* .local v0, "cursor":Landroid/database/Cursor; */
/* new-instance v1, Ljava/util/ArrayList; */
/* invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V */
/* .line 507 */
/* .local v1, "result":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;" */
} // :goto_0
if ( v0 != null) { // if-eqz v0, :cond_0
v2 = try { // :try_start_0
if ( v2 != null) { // if-eqz v2, :cond_0
/* .line 508 */
int v2 = 0; // const/4 v2, 0x0
/* :try_end_0 */
/* .catchall {:try_start_0 ..:try_end_0} :catchall_0 */
/* .line 511 */
/* :catchall_0 */
/* move-exception v2 */
/* invoke-direct {p0, v0}, Lcom/miui/server/enterprise/APNManagerService;->closeCursor(Landroid/database/Cursor;)V */
/* .line 512 */
/* throw v2 */
/* .line 511 */
} // :cond_0
/* invoke-direct {p0, v0}, Lcom/miui/server/enterprise/APNManagerService;->closeCursor(Landroid/database/Cursor;)V */
/* .line 512 */
/* nop */
/* .line 513 */
} // .end method
public Boolean resetAPN ( ) {
/* .locals 3 */
/* .line 483 */
v0 = this.mContext;
com.miui.server.enterprise.ServiceUtils .checkPermission ( v0 );
/* .line 484 */
/* invoke-direct {p0}, Lcom/miui/server/enterprise/APNManagerService;->getContentResolver()Landroid/content/ContentResolver; */
/* .line 485 */
/* .local v0, "resolver":Landroid/content/ContentResolver; */
v1 = com.miui.server.enterprise.APNManagerService.DEFAULTAPN_URI;
/* invoke-direct {p0, v1}, Lcom/miui/server/enterprise/APNManagerService;->getUriForCurrSubId(Landroid/net/Uri;)Landroid/net/Uri; */
int v2 = 0; // const/4 v2, 0x0
v1 = (( android.content.ContentResolver ) v0 ).delete ( v1, v2, v2 ); // invoke-virtual {v0, v1, v2, v2}, Landroid/content/ContentResolver;->delete(Landroid/net/Uri;Ljava/lang/String;[Ljava/lang/String;)I
/* .line 486 */
/* .local v1, "result":I */
if ( v1 != null) { // if-eqz v1, :cond_0
int v2 = 1; // const/4 v2, 0x1
} // :cond_0
int v2 = 0; // const/4 v2, 0x0
} // :goto_0
} // .end method
public void setAPNActiveMode ( Integer p0 ) {
/* .locals 3 */
/* .param p1, "mode" # I */
/* .line 491 */
v0 = this.mContext;
com.miui.server.enterprise.ServiceUtils .checkPermission ( v0 );
/* .line 492 */
v0 = this.mContext;
final String v1 = "ep_apn_switch_mode"; // const-string v1, "ep_apn_switch_mode"
int v2 = 0; // const/4 v2, 0x0
com.miui.enterprise.settings.EnterpriseSettings .putInt ( v0,v1,p1,v2 );
/* .line 493 */
return;
} // .end method
