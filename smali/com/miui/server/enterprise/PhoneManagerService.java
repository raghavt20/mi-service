public class com.miui.server.enterprise.PhoneManagerService extends com.miui.enterprise.IPhoneManager$Stub {
	 /* .source "PhoneManagerService.java" */
	 /* # static fields */
	 private static final java.lang.String TAG;
	 /* # instance fields */
	 private android.content.Context mContext;
	 /* # direct methods */
	 com.miui.server.enterprise.PhoneManagerService ( ) {
		 /* .locals 0 */
		 /* .param p1, "context" # Landroid/content/Context; */
		 /* .line 20 */
		 /* invoke-direct {p0}, Lcom/miui/enterprise/IPhoneManager$Stub;-><init>()V */
		 /* .line 21 */
		 this.mContext = p1;
		 /* .line 22 */
		 return;
	 } // .end method
	 private Boolean shouldClose ( Integer p0 ) {
		 /* .locals 1 */
		 /* .param p1, "state" # I */
		 /* .line 53 */
		 if ( p1 != null) { // if-eqz p1, :cond_1
			 int v0 = 3; // const/4 v0, 0x3
			 /* if-ne p1, v0, :cond_0 */
		 } // :cond_0
		 int v0 = 0; // const/4 v0, 0x0
	 } // :cond_1
} // :goto_0
int v0 = 1; // const/4 v0, 0x1
} // :goto_1
} // .end method
private Boolean shouldOpen ( Integer p0 ) {
/* .locals 1 */
/* .param p1, "state" # I */
/* .line 49 */
int v0 = 2; // const/4 v0, 0x2
/* if-eq p1, v0, :cond_1 */
int v0 = 4; // const/4 v0, 0x4
/* if-ne p1, v0, :cond_0 */
} // :cond_0
int v0 = 0; // const/4 v0, 0x0
} // :cond_1
} // :goto_0
int v0 = 1; // const/4 v0, 0x1
} // :goto_1
} // .end method
/* # virtual methods */
public void controlCellular ( Integer p0, Integer p1 ) {
/* .locals 2 */
/* .param p1, "flag" # I */
/* .param p2, "userId" # I */
/* .line 38 */
v0 = this.mContext;
com.miui.server.enterprise.ServiceUtils .checkPermission ( v0 );
/* .line 39 */
v0 = this.mContext;
final String v1 = "ep_cellular_status"; // const-string v1, "ep_cellular_status"
com.miui.enterprise.settings.EnterpriseSettings .putInt ( v0,v1,p1,p2 );
/* .line 40 */
v0 = /* invoke-direct {p0, p1}, Lcom/miui/server/enterprise/PhoneManagerService;->shouldOpen(I)Z */
if ( v0 != null) { // if-eqz v0, :cond_0
/* .line 41 */
v0 = this.mContext;
android.telephony.TelephonyManager .from ( v0 );
int v1 = 1; // const/4 v1, 0x1
(( android.telephony.TelephonyManager ) v0 ).setDataEnabled ( v1 ); // invoke-virtual {v0, v1}, Landroid/telephony/TelephonyManager;->setDataEnabled(Z)V
/* .line 43 */
} // :cond_0
v0 = /* invoke-direct {p0, p1}, Lcom/miui/server/enterprise/PhoneManagerService;->shouldClose(I)Z */
if ( v0 != null) { // if-eqz v0, :cond_1
/* .line 44 */
v0 = this.mContext;
android.telephony.TelephonyManager .from ( v0 );
int v1 = 0; // const/4 v1, 0x0
(( android.telephony.TelephonyManager ) v0 ).setDataEnabled ( v1 ); // invoke-virtual {v0, v1}, Landroid/telephony/TelephonyManager;->setDataEnabled(Z)V
/* .line 46 */
} // :cond_1
return;
} // .end method
public void controlPhoneCall ( Integer p0, Integer p1 ) {
/* .locals 2 */
/* .param p1, "flags" # I */
/* .param p2, "userId" # I */
/* .line 32 */
v0 = this.mContext;
com.miui.server.enterprise.ServiceUtils .checkPermission ( v0 );
/* .line 33 */
v0 = this.mContext;
final String v1 = "ep_phone_call_status"; // const-string v1, "ep_phone_call_status"
com.miui.enterprise.settings.EnterpriseSettings .putInt ( v0,v1,p1,p2 );
/* .line 34 */
return;
} // .end method
public void controlSMS ( Integer p0, Integer p1 ) {
/* .locals 2 */
/* .param p1, "flags" # I */
/* .param p2, "userId" # I */
/* .line 26 */
v0 = this.mContext;
com.miui.server.enterprise.ServiceUtils .checkPermission ( v0 );
/* .line 27 */
v0 = this.mContext;
final String v1 = "ep_sms_status"; // const-string v1, "ep_sms_status"
com.miui.enterprise.settings.EnterpriseSettings .putInt ( v0,v1,p1,p2 );
/* .line 28 */
return;
} // .end method
public void disableCallForward ( Boolean p0 ) {
/* .locals 2 */
/* .param p1, "disabled" # Z */
/* .line 193 */
v0 = this.mContext;
com.miui.server.enterprise.ServiceUtils .checkPermission ( v0 );
/* .line 194 */
v0 = this.mContext;
final String v1 = "ep_disable_call_forward"; // const-string v1, "ep_disable_call_forward"
com.miui.enterprise.settings.EnterpriseSettings .putInt ( v0,v1,p1 );
/* .line 195 */
return;
} // .end method
public void disableCallLog ( Boolean p0 ) {
/* .locals 2 */
/* .param p1, "disable" # Z */
/* .line 199 */
v0 = this.mContext;
com.miui.server.enterprise.ServiceUtils .checkPermission ( v0 );
/* .line 200 */
v0 = this.mContext;
final String v1 = "ep_disable_call_log"; // const-string v1, "ep_disable_call_log"
com.miui.enterprise.settings.EnterpriseSettings .putInt ( v0,v1,p1 );
/* .line 201 */
return;
} // .end method
public void endCall ( ) {
/* .locals 2 */
/* .line 185 */
v0 = this.mContext;
com.miui.server.enterprise.ServiceUtils .checkPermission ( v0 );
/* .line 186 */
final String v0 = "Enterprise-phone"; // const-string v0, "Enterprise-phone"
final String v1 = "End current call"; // const-string v1, "End current call"
android.util.Slog .d ( v0,v1 );
/* .line 187 */
miui.telephony.TelephonyManagerEx .getDefault ( );
(( miui.telephony.TelephonyManagerEx ) v0 ).endCall ( ); // invoke-virtual {v0}, Lmiui/telephony/TelephonyManagerEx;->endCall()Z
/* .line 188 */
/* new-instance v0, Ljava/lang/RuntimeException; */
final String v1 = "Not implemented"; // const-string v1, "Not implemented"
/* invoke-direct {v0, v1}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V */
/* throw v0 */
} // .end method
public java.lang.String getAreaCode ( java.lang.String p0 ) {
/* .locals 2 */
/* .param p1, "phoneNumber" # Ljava/lang/String; */
/* .line 205 */
v0 = this.mContext;
com.miui.server.enterprise.ServiceUtils .checkPermission ( v0 );
/* .line 206 */
miui.telephony.PhoneNumberUtils$PhoneNumber .parse ( p1 );
v1 = this.mContext;
(( miui.telephony.PhoneNumberUtils$PhoneNumber ) v0 ).getLocationAreaCode ( v1 ); // invoke-virtual {v0, v1}, Lmiui/telephony/PhoneNumberUtils$PhoneNumber;->getLocationAreaCode(Landroid/content/Context;)Ljava/lang/String;
} // .end method
public java.util.List getCallBlackList ( Integer p0 ) {
/* .locals 2 */
/* .param p1, "userId" # I */
/* .annotation system Ldalvik/annotation/Signature; */
/* value = { */
/* "(I)", */
/* "Ljava/util/List<", */
/* "Ljava/lang/String;", */
/* ">;" */
/* } */
} // .end annotation
/* .line 159 */
v0 = this.mContext;
com.miui.server.enterprise.ServiceUtils .checkPermission ( v0 );
/* .line 160 */
v0 = this.mContext;
/* .line 161 */
final String v1 = "ep_call_black_list"; // const-string v1, "ep_call_black_list"
com.miui.enterprise.settings.EnterpriseSettings .getString ( v0,v1,p1 );
/* .line 160 */
com.miui.enterprise.settings.EnterpriseSettings .parseListSettings ( v0 );
} // .end method
public Integer getCallContactRestriction ( Integer p0 ) {
/* .locals 3 */
/* .param p1, "userId" # I */
/* .line 179 */
v0 = this.mContext;
com.miui.server.enterprise.ServiceUtils .checkPermission ( v0 );
/* .line 180 */
v0 = this.mContext;
final String v1 = "ep_call_restriction_mode"; // const-string v1, "ep_call_restriction_mode"
int v2 = 0; // const/4 v2, 0x0
v0 = com.miui.enterprise.settings.EnterpriseSettings .getInt ( v0,v1,v2,p1 );
} // .end method
public java.util.List getCallWhiteList ( Integer p0 ) {
/* .locals 2 */
/* .param p1, "userId" # I */
/* .annotation system Ldalvik/annotation/Signature; */
/* value = { */
/* "(I)", */
/* "Ljava/util/List<", */
/* "Ljava/lang/String;", */
/* ">;" */
/* } */
} // .end annotation
/* .line 166 */
v0 = this.mContext;
com.miui.server.enterprise.ServiceUtils .checkPermission ( v0 );
/* .line 167 */
v0 = this.mContext;
/* .line 168 */
final String v1 = "ep_call_white_list"; // const-string v1, "ep_call_white_list"
com.miui.enterprise.settings.EnterpriseSettings .getString ( v0,v1,p1 );
/* .line 167 */
com.miui.enterprise.settings.EnterpriseSettings .parseListSettings ( v0 );
} // .end method
public Integer getCellularStatus ( Integer p0 ) {
/* .locals 3 */
/* .param p1, "userId" # I */
/* .line 72 */
v0 = this.mContext;
com.miui.server.enterprise.ServiceUtils .checkPermission ( v0 );
/* .line 73 */
v0 = this.mContext;
final String v1 = "ep_cellular_status"; // const-string v1, "ep_cellular_status"
int v2 = 0; // const/4 v2, 0x0
v0 = com.miui.enterprise.settings.EnterpriseSettings .getInt ( v0,v1,v2,p1 );
} // .end method
public java.lang.String getIMEI ( Integer p0 ) {
/* .locals 1 */
/* .param p1, "slotId" # I */
/* .line 79 */
v0 = this.mContext;
com.miui.server.enterprise.ServiceUtils .checkPermission ( v0 );
/* .line 80 */
miui.telephony.TelephonyManager .getDefault ( );
(( miui.telephony.TelephonyManager ) v0 ).getImeiForSlot ( p1 ); // invoke-virtual {v0, p1}, Lmiui/telephony/TelephonyManager;->getImeiForSlot(I)Ljava/lang/String;
} // .end method
public java.lang.String getMeid ( Integer p0 ) {
/* .locals 1 */
/* .param p1, "slotId" # I */
/* .line 211 */
v0 = this.mContext;
com.miui.server.enterprise.ServiceUtils .checkPermission ( v0 );
/* .line 212 */
miui.telephony.TelephonyManager .getDefault ( );
(( miui.telephony.TelephonyManager ) v0 ).getMeidForSlot ( p1 ); // invoke-virtual {v0, p1}, Lmiui/telephony/TelephonyManager;->getMeidForSlot(I)Ljava/lang/String;
} // .end method
public Integer getPhoneCallStatus ( Integer p0 ) {
/* .locals 3 */
/* .param p1, "userId" # I */
/* .line 65 */
v0 = this.mContext;
com.miui.server.enterprise.ServiceUtils .checkPermission ( v0 );
/* .line 66 */
v0 = this.mContext;
final String v1 = "ep_phone_call_status"; // const-string v1, "ep_phone_call_status"
int v2 = 0; // const/4 v2, 0x0
v0 = com.miui.enterprise.settings.EnterpriseSettings .getInt ( v0,v1,v2,p1 );
} // .end method
public java.util.List getSMSBlackList ( Integer p0 ) {
/* .locals 2 */
/* .param p1, "userId" # I */
/* .annotation system Ldalvik/annotation/Signature; */
/* value = { */
/* "(I)", */
/* "Ljava/util/List<", */
/* "Ljava/lang/String;", */
/* ">;" */
/* } */
} // .end annotation
/* .line 119 */
v0 = this.mContext;
com.miui.server.enterprise.ServiceUtils .checkPermission ( v0 );
/* .line 120 */
v0 = this.mContext;
/* .line 121 */
final String v1 = "ep_sms_black_list"; // const-string v1, "ep_sms_black_list"
com.miui.enterprise.settings.EnterpriseSettings .getString ( v0,v1,p1 );
/* .line 120 */
com.miui.enterprise.settings.EnterpriseSettings .parseListSettings ( v0 );
} // .end method
public Integer getSMSContactRestriction ( Integer p0 ) {
/* .locals 3 */
/* .param p1, "userId" # I */
/* .line 139 */
v0 = this.mContext;
com.miui.server.enterprise.ServiceUtils .checkPermission ( v0 );
/* .line 140 */
v0 = this.mContext;
final String v1 = "ep_sms_restriction_mode"; // const-string v1, "ep_sms_restriction_mode"
int v2 = 0; // const/4 v2, 0x0
v0 = com.miui.enterprise.settings.EnterpriseSettings .getInt ( v0,v1,v2,p1 );
} // .end method
public Integer getSMSStatus ( Integer p0 ) {
/* .locals 3 */
/* .param p1, "userId" # I */
/* .line 58 */
v0 = this.mContext;
com.miui.server.enterprise.ServiceUtils .checkPermission ( v0 );
/* .line 59 */
v0 = this.mContext;
final String v1 = "ep_sms_status"; // const-string v1, "ep_sms_status"
int v2 = 0; // const/4 v2, 0x0
v0 = com.miui.enterprise.settings.EnterpriseSettings .getInt ( v0,v1,v2,p1 );
} // .end method
public java.util.List getSMSWhiteList ( Integer p0 ) {
/* .locals 2 */
/* .param p1, "userId" # I */
/* .annotation system Ldalvik/annotation/Signature; */
/* value = { */
/* "(I)", */
/* "Ljava/util/List<", */
/* "Ljava/lang/String;", */
/* ">;" */
/* } */
} // .end annotation
/* .line 126 */
v0 = this.mContext;
com.miui.server.enterprise.ServiceUtils .checkPermission ( v0 );
/* .line 127 */
v0 = this.mContext;
/* .line 128 */
final String v1 = "ep_sms_white_list"; // const-string v1, "ep_sms_white_list"
com.miui.enterprise.settings.EnterpriseSettings .getString ( v0,v1,p1 );
/* .line 127 */
com.miui.enterprise.settings.EnterpriseSettings .parseListSettings ( v0 );
} // .end method
public Boolean isAutoRecordPhoneCall ( Integer p0 ) {
/* .locals 3 */
/* .param p1, "userId" # I */
/* .line 99 */
v0 = this.mContext;
com.miui.server.enterprise.ServiceUtils .checkPermission ( v0 );
/* .line 100 */
v0 = this.mContext;
final String v1 = "ep_force_auto_call_record"; // const-string v1, "ep_force_auto_call_record"
int v2 = 0; // const/4 v2, 0x0
v0 = com.miui.enterprise.settings.EnterpriseSettings .getInt ( v0,v1,v2,p1 );
int v1 = 1; // const/4 v1, 0x1
/* if-ne v0, v1, :cond_0 */
/* move v2, v1 */
} // :cond_0
} // .end method
public void setCallBlackList ( java.util.List p0, Integer p1 ) {
/* .locals 3 */
/* .param p2, "userId" # I */
/* .annotation system Ldalvik/annotation/Signature; */
/* value = { */
/* "(", */
/* "Ljava/util/List<", */
/* "Ljava/lang/String;", */
/* ">;I)V" */
/* } */
} // .end annotation
/* .line 145 */
/* .local p1, "list":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;" */
v0 = this.mContext;
com.miui.server.enterprise.ServiceUtils .checkPermission ( v0 );
/* .line 146 */
v0 = this.mContext;
/* .line 147 */
com.miui.enterprise.settings.EnterpriseSettings .generateListSettings ( p1 );
/* .line 146 */
final String v2 = "ep_call_black_list"; // const-string v2, "ep_call_black_list"
com.miui.enterprise.settings.EnterpriseSettings .putString ( v0,v2,v1,p2 );
/* .line 148 */
return;
} // .end method
public void setCallContactRestriction ( Integer p0, Integer p1 ) {
/* .locals 2 */
/* .param p1, "mode" # I */
/* .param p2, "userId" # I */
/* .line 173 */
v0 = this.mContext;
com.miui.server.enterprise.ServiceUtils .checkPermission ( v0 );
/* .line 174 */
v0 = this.mContext;
final String v1 = "ep_call_restriction_mode"; // const-string v1, "ep_call_restriction_mode"
com.miui.enterprise.settings.EnterpriseSettings .putInt ( v0,v1,p1,p2 );
/* .line 175 */
return;
} // .end method
public void setCallWhiteList ( java.util.List p0, Integer p1 ) {
/* .locals 3 */
/* .param p2, "userId" # I */
/* .annotation system Ldalvik/annotation/Signature; */
/* value = { */
/* "(", */
/* "Ljava/util/List<", */
/* "Ljava/lang/String;", */
/* ">;I)V" */
/* } */
} // .end annotation
/* .line 152 */
/* .local p1, "list":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;" */
v0 = this.mContext;
com.miui.server.enterprise.ServiceUtils .checkPermission ( v0 );
/* .line 153 */
v0 = this.mContext;
/* .line 154 */
com.miui.enterprise.settings.EnterpriseSettings .generateListSettings ( p1 );
/* .line 153 */
final String v2 = "ep_call_white_list"; // const-string v2, "ep_call_white_list"
com.miui.enterprise.settings.EnterpriseSettings .putString ( v0,v2,v1,p2 );
/* .line 155 */
return;
} // .end method
public void setIccCardActivate ( Integer p0, Boolean p1 ) {
/* .locals 4 */
/* .param p1, "slotId" # I */
/* .param p2, "isActive" # Z */
/* .line 217 */
v0 = this.mContext;
com.miui.server.enterprise.ServiceUtils .checkPermission ( v0 );
/* .line 218 */
int v0 = 1; // const/4 v0, 0x1
if ( p1 != null) { // if-eqz p1, :cond_1
/* if-ne p1, v0, :cond_0 */
/* .line 219 */
} // :cond_0
/* new-instance v0, Ljava/lang/IllegalArgumentException; */
/* new-instance v1, Ljava/lang/StringBuilder; */
/* invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V */
final String v2 = "Invalid slotId value "; // const-string v2, "Invalid slotId value "
(( java.lang.StringBuilder ) v1 ).append ( v2 ); // invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v1 ).append ( p1 ); // invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;
final String v2 = ".Must be either PhoneManagerMode.SLOT_ID_1 or PhoneManagerMode.SLOT_ID_2."; // const-string v2, ".Must be either PhoneManagerMode.SLOT_ID_1 or PhoneManagerMode.SLOT_ID_2."
(( java.lang.StringBuilder ) v1 ).append ( v2 ); // invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v1 ).toString ( ); // invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
/* invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V */
/* throw v0 */
/* .line 222 */
} // :cond_1
} // :goto_0
miui.telephony.TelephonyManager .getDefault ( );
/* .line 223 */
/* .local v1, "telephonyManager":Lmiui/telephony/TelephonyManager; */
v2 = (( miui.telephony.TelephonyManager ) v1 ).getSimStateForSlot ( p1 ); // invoke-virtual {v1, p1}, Lmiui/telephony/TelephonyManager;->getSimStateForSlot(I)I
/* .line 224 */
/* .local v2, "simState":I */
/* if-ne v2, v0, :cond_2 */
/* .line 225 */
return;
/* .line 227 */
} // :cond_2
/* if-nez p1, :cond_3 */
/* .line 228 */
/* xor-int/lit8 v0, p2, 0x1 */
final String v3 = "ep_icc_card_1_disable"; // const-string v3, "ep_icc_card_1_disable"
com.miui.enterprise.settings.EnterpriseSettings .putInt ( v3,v0 );
/* .line 230 */
} // :cond_3
/* xor-int/lit8 v0, p2, 0x1 */
final String v3 = "ep_icc_card_2_disable"; // const-string v3, "ep_icc_card_2_disable"
com.miui.enterprise.settings.EnterpriseSettings .putInt ( v3,v0 );
/* .line 232 */
} // :goto_1
(( miui.telephony.TelephonyManager ) v1 ).setIccCardActivate ( p1, p2 ); // invoke-virtual {v1, p1, p2}, Lmiui/telephony/TelephonyManager;->setIccCardActivate(IZ)V
/* .line 233 */
return;
} // .end method
public void setPhoneCallAutoRecord ( Boolean p0, Integer p1 ) {
/* .locals 2 */
/* .param p1, "isAutoRecord" # Z */
/* .param p2, "userId" # I */
/* .line 85 */
v0 = this.mContext;
com.miui.server.enterprise.ServiceUtils .checkPermission ( v0 );
/* .line 86 */
v0 = this.mContext;
(( android.content.Context ) v0 ).getContentResolver ( ); // invoke-virtual {v0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;
final String v1 = "button_auto_record_call"; // const-string v1, "button_auto_record_call"
android.provider.MiuiSettings$System .putBooleanForUser ( v0,v1,p1,p2 );
/* .line 88 */
v0 = this.mContext;
final String v1 = "ep_force_auto_call_record"; // const-string v1, "ep_force_auto_call_record"
com.miui.enterprise.settings.EnterpriseSettings .putInt ( v0,v1,p1,p2 );
/* .line 89 */
return;
} // .end method
public void setPhoneCallAutoRecordDir ( java.lang.String p0 ) {
/* .locals 2 */
/* .param p1, "dir" # Ljava/lang/String; */
/* .line 93 */
v0 = this.mContext;
com.miui.server.enterprise.ServiceUtils .checkPermission ( v0 );
/* .line 94 */
v0 = this.mContext;
final String v1 = "ep_force_auto_call_record_dir"; // const-string v1, "ep_force_auto_call_record_dir"
com.miui.enterprise.settings.EnterpriseSettings .putString ( v0,v1,p1 );
/* .line 95 */
return;
} // .end method
public void setSMSBlackList ( java.util.List p0, Integer p1 ) {
/* .locals 3 */
/* .param p2, "userId" # I */
/* .annotation system Ldalvik/annotation/Signature; */
/* value = { */
/* "(", */
/* "Ljava/util/List<", */
/* "Ljava/lang/String;", */
/* ">;I)V" */
/* } */
} // .end annotation
/* .line 105 */
/* .local p1, "list":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;" */
v0 = this.mContext;
com.miui.server.enterprise.ServiceUtils .checkPermission ( v0 );
/* .line 106 */
v0 = this.mContext;
/* .line 107 */
com.miui.enterprise.settings.EnterpriseSettings .generateListSettings ( p1 );
/* .line 106 */
final String v2 = "ep_sms_black_list"; // const-string v2, "ep_sms_black_list"
com.miui.enterprise.settings.EnterpriseSettings .putString ( v0,v2,v1,p2 );
/* .line 108 */
return;
} // .end method
public void setSMSContactRestriction ( Integer p0, Integer p1 ) {
/* .locals 2 */
/* .param p1, "mode" # I */
/* .param p2, "userId" # I */
/* .line 133 */
v0 = this.mContext;
com.miui.server.enterprise.ServiceUtils .checkPermission ( v0 );
/* .line 134 */
v0 = this.mContext;
final String v1 = "ep_sms_restriction_mode"; // const-string v1, "ep_sms_restriction_mode"
com.miui.enterprise.settings.EnterpriseSettings .putInt ( v0,v1,p1,p2 );
/* .line 135 */
return;
} // .end method
public void setSMSWhiteList ( java.util.List p0, Integer p1 ) {
/* .locals 3 */
/* .param p2, "userId" # I */
/* .annotation system Ldalvik/annotation/Signature; */
/* value = { */
/* "(", */
/* "Ljava/util/List<", */
/* "Ljava/lang/String;", */
/* ">;I)V" */
/* } */
} // .end annotation
/* .line 112 */
/* .local p1, "list":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;" */
v0 = this.mContext;
com.miui.server.enterprise.ServiceUtils .checkPermission ( v0 );
/* .line 113 */
v0 = this.mContext;
/* .line 114 */
com.miui.enterprise.settings.EnterpriseSettings .generateListSettings ( p1 );
/* .line 113 */
final String v2 = "ep_sms_white_list"; // const-string v2, "ep_sms_white_list"
com.miui.enterprise.settings.EnterpriseSettings .putString ( v0,v2,v1,p2 );
/* .line 115 */
return;
} // .end method
