.class public Lcom/miui/server/enterprise/RestrictionsManagerService;
.super Lcom/miui/enterprise/IRestrictionsManager$Stub;
.source "RestrictionsManagerService.java"


# static fields
.field private static final TAG:Ljava/lang/String; = "Enterprise-restric"


# instance fields
.field private mAppOpsManager:Landroid/app/AppOpsManager;

.field private mContext:Landroid/content/Context;

.field private mDeviceOwner:Landroid/content/ComponentName;

.field private mDevicePolicyManager:Landroid/app/admin/IDevicePolicyManager;

.field private mHandler:Landroid/os/Handler;

.field private mPMS:Lcom/android/server/pm/PackageManagerService$IPackageManagerImpl;

.field private mUserManager:Lcom/android/server/pm/UserManagerService;

.field private mWindowManagerService:Lcom/android/server/wm/WindowManagerService;


# direct methods
.method static bridge synthetic -$$Nest$fgetmContext(Lcom/miui/server/enterprise/RestrictionsManagerService;)Landroid/content/Context;
    .locals 0

    iget-object p0, p0, Lcom/miui/server/enterprise/RestrictionsManagerService;->mContext:Landroid/content/Context;

    return-object p0
.end method

.method constructor <init>(Landroid/content/Context;)V
    .locals 2
    .param p1, "context"    # Landroid/content/Context;

    .line 84
    invoke-direct {p0}, Lcom/miui/enterprise/IRestrictionsManager$Stub;-><init>()V

    .line 85
    iput-object p1, p0, Lcom/miui/server/enterprise/RestrictionsManagerService;->mContext:Landroid/content/Context;

    .line 86
    const-string/jumbo v0, "window"

    invoke-static {v0}, Landroid/os/ServiceManager;->getService(Ljava/lang/String;)Landroid/os/IBinder;

    move-result-object v0

    check-cast v0, Lcom/android/server/wm/WindowManagerService;

    iput-object v0, p0, Lcom/miui/server/enterprise/RestrictionsManagerService;->mWindowManagerService:Lcom/android/server/wm/WindowManagerService;

    .line 87
    const-string v0, "package"

    invoke-static {v0}, Landroid/os/ServiceManager;->getService(Ljava/lang/String;)Landroid/os/IBinder;

    move-result-object v0

    check-cast v0, Lcom/android/server/pm/PackageManagerService$IPackageManagerImpl;

    iput-object v0, p0, Lcom/miui/server/enterprise/RestrictionsManagerService;->mPMS:Lcom/android/server/pm/PackageManagerService$IPackageManagerImpl;

    .line 88
    const-string/jumbo v0, "user"

    invoke-static {v0}, Landroid/os/ServiceManager;->getService(Ljava/lang/String;)Landroid/os/IBinder;

    move-result-object v0

    check-cast v0, Lcom/android/server/pm/UserManagerService;

    iput-object v0, p0, Lcom/miui/server/enterprise/RestrictionsManagerService;->mUserManager:Lcom/android/server/pm/UserManagerService;

    .line 89
    const-string v0, "device_policy"

    invoke-static {v0}, Landroid/os/ServiceManager;->getService(Ljava/lang/String;)Landroid/os/IBinder;

    move-result-object v0

    invoke-static {v0}, Landroid/app/admin/IDevicePolicyManager$Stub;->asInterface(Landroid/os/IBinder;)Landroid/app/admin/IDevicePolicyManager;

    move-result-object v0

    iput-object v0, p0, Lcom/miui/server/enterprise/RestrictionsManagerService;->mDevicePolicyManager:Landroid/app/admin/IDevicePolicyManager;

    .line 91
    const/4 v1, 0x1

    :try_start_0
    invoke-interface {v0, v1}, Landroid/app/admin/IDevicePolicyManager;->getDeviceOwnerComponent(Z)Landroid/content/ComponentName;

    move-result-object v0

    iput-object v0, p0, Lcom/miui/server/enterprise/RestrictionsManagerService;->mDeviceOwner:Landroid/content/ComponentName;
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    .line 94
    goto :goto_0

    .line 92
    :catch_0
    move-exception v0

    .line 95
    :goto_0
    iget-object v0, p0, Lcom/miui/server/enterprise/RestrictionsManagerService;->mContext:Landroid/content/Context;

    const-string v1, "appops"

    invoke-virtual {v0, v1}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/app/AppOpsManager;

    iput-object v0, p0, Lcom/miui/server/enterprise/RestrictionsManagerService;->mAppOpsManager:Landroid/app/AppOpsManager;

    .line 96
    new-instance v0, Landroid/os/Handler;

    invoke-direct {v0}, Landroid/os/Handler;-><init>()V

    iput-object v0, p0, Lcom/miui/server/enterprise/RestrictionsManagerService;->mHandler:Landroid/os/Handler;

    .line 97
    return-void
.end method

.method private closeCloudBackup(I)V
    .locals 9
    .param p1, "userId"    # I

    .line 369
    const/4 v0, 0x0

    .line 370
    .local v0, "account":Landroid/accounts/Account;
    iget-object v1, p0, Lcom/miui/server/enterprise/RestrictionsManagerService;->mContext:Landroid/content/Context;

    invoke-static {v1}, Landroid/accounts/AccountManager;->get(Landroid/content/Context;)Landroid/accounts/AccountManager;

    move-result-object v1

    .line 371
    .local v1, "am":Landroid/accounts/AccountManager;
    new-instance v2, Landroid/os/UserHandle;

    invoke-direct {v2, p1}, Landroid/os/UserHandle;-><init>(I)V

    const-string v3, "com.xiaomi"

    invoke-virtual {v1, v3, v2}, Landroid/accounts/AccountManager;->getAccountsByTypeAsUser(Ljava/lang/String;Landroid/os/UserHandle;)[Landroid/accounts/Account;

    move-result-object v2

    .line 372
    .local v2, "accounts":[Landroid/accounts/Account;
    array-length v3, v2

    const/4 v4, 0x0

    if-lez v3, :cond_0

    .line 373
    aget-object v0, v2, v4

    .line 375
    :cond_0
    if-eqz v0, :cond_1

    .line 376
    new-instance v3, Landroid/content/ContentValues;

    invoke-direct {v3}, Landroid/content/ContentValues;-><init>()V

    .line 377
    .local v3, "values":Landroid/content/ContentValues;
    const-string v5, "account_name"

    iget-object v6, v0, Landroid/accounts/Account;->name:Ljava/lang/String;

    invoke-virtual {v3, v5, v6}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 378
    const-string v5, "is_open"

    invoke-static {v4}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v4

    invoke-virtual {v3, v5, v4}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Boolean;)V

    .line 379
    const-string v4, "content://com.miui.micloud"

    invoke-static {v4}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v4

    .line 380
    invoke-virtual {v4}, Landroid/net/Uri;->buildUpon()Landroid/net/Uri$Builder;

    move-result-object v4

    const-string v5, "cloud_backup_info"

    invoke-virtual {v4, v5}, Landroid/net/Uri$Builder;->appendPath(Ljava/lang/String;)Landroid/net/Uri$Builder;

    move-result-object v4

    invoke-virtual {v4}, Landroid/net/Uri$Builder;->build()Landroid/net/Uri;

    move-result-object v4

    .line 381
    .local v4, "cloudBackupInfoUri":Landroid/net/Uri;
    invoke-static {v4, p1}, Landroid/content/ContentProvider;->maybeAddUserId(Landroid/net/Uri;I)Landroid/net/Uri;

    move-result-object v4

    .line 382
    iget-object v5, p0, Lcom/miui/server/enterprise/RestrictionsManagerService;->mContext:Landroid/content/Context;

    invoke-virtual {v5}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v5

    const/4 v6, 0x0

    invoke-virtual {v5, v4, v3, v6, v6}, Landroid/content/ContentResolver;->update(Landroid/net/Uri;Landroid/content/ContentValues;Ljava/lang/String;[Ljava/lang/String;)I

    .line 383
    new-instance v5, Landroid/content/Intent;

    invoke-direct {v5}, Landroid/content/Intent;-><init>()V

    .line 384
    .local v5, "intent":Landroid/content/Intent;
    new-instance v6, Landroid/content/ComponentName;

    const-string v7, "com.miui.cloudbackup"

    const-string v8, "com.miui.cloudbackup.service.CloudBackupService"

    invoke-direct {v6, v7, v8}, Landroid/content/ComponentName;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {v5, v6}, Landroid/content/Intent;->setComponent(Landroid/content/ComponentName;)Landroid/content/Intent;

    .line 386
    const-string v6, "close_cloud_back_up"

    invoke-virtual {v5, v6}, Landroid/content/Intent;->setAction(Ljava/lang/String;)Landroid/content/Intent;

    .line 387
    iget-object v6, p0, Lcom/miui/server/enterprise/RestrictionsManagerService;->mContext:Landroid/content/Context;

    new-instance v7, Landroid/os/UserHandle;

    invoke-direct {v7, p1}, Landroid/os/UserHandle;-><init>(I)V

    invoke-virtual {v6, v5, v7}, Landroid/content/Context;->startServiceAsUser(Landroid/content/Intent;Landroid/os/UserHandle;)Landroid/content/ComponentName;

    .line 389
    .end local v3    # "values":Landroid/content/ContentValues;
    .end local v4    # "cloudBackupInfoUri":Landroid/net/Uri;
    .end local v5    # "intent":Landroid/content/Intent;
    :cond_1
    return-void
.end method

.method private setUsbFunction(Landroid/hardware/usb/UsbManager;Ljava/lang/String;)V
    .locals 8
    .param p1, "usbManager"    # Landroid/hardware/usb/UsbManager;
    .param p2, "function"    # Ljava/lang/String;

    .line 393
    const-string/jumbo v0, "setCurrentFunction"

    const/4 v1, 0x0

    const/4 v2, 0x1

    :try_start_0
    const-class v3, Landroid/hardware/usb/UsbManager;

    new-array v4, v2, [Ljava/lang/Class;

    const-class v5, Ljava/lang/String;

    aput-object v5, v4, v1

    invoke-virtual {v3, v0, v4}, Ljava/lang/Class;->getDeclaredMethod(Ljava/lang/String;[Ljava/lang/Class;)Ljava/lang/reflect/Method;

    move-result-object v3

    .line 394
    .local v3, "method":Ljava/lang/reflect/Method;
    invoke-virtual {v3, v2}, Ljava/lang/reflect/Method;->setAccessible(Z)V

    .line 395
    filled-new-array {p2}, [Ljava/lang/Object;

    move-result-object v4

    invoke-virtual {v3, p1, v4}, Ljava/lang/reflect/Method;->invoke(Ljava/lang/Object;[Ljava/lang/Object;)Ljava/lang/Object;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 404
    nop

    .end local v3    # "method":Ljava/lang/reflect/Method;
    goto :goto_0

    .line 396
    :catch_0
    move-exception v3

    .line 398
    .local v3, "e":Ljava/lang/Exception;
    :try_start_1
    const-class v4, Landroid/hardware/usb/UsbManager;

    const/4 v5, 0x2

    new-array v6, v5, [Ljava/lang/Class;

    const-class v7, Ljava/lang/String;

    aput-object v7, v6, v1

    sget-object v7, Ljava/lang/Boolean;->TYPE:Ljava/lang/Class;

    aput-object v7, v6, v2

    invoke-virtual {v4, v0, v6}, Ljava/lang/Class;->getDeclaredMethod(Ljava/lang/String;[Ljava/lang/Class;)Ljava/lang/reflect/Method;

    move-result-object v0

    .line 399
    .local v0, "method":Ljava/lang/reflect/Method;
    invoke-virtual {v0, v2}, Ljava/lang/reflect/Method;->setAccessible(Z)V

    .line 400
    new-array v4, v5, [Ljava/lang/Object;

    aput-object p2, v4, v1

    invoke-static {v1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v1

    aput-object v1, v4, v2

    invoke-virtual {v0, p1, v4}, Ljava/lang/reflect/Method;->invoke(Ljava/lang/Object;[Ljava/lang/Object;)Ljava/lang/Object;
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_1

    .line 403
    nop

    .end local v0    # "method":Ljava/lang/reflect/Method;
    goto :goto_0

    .line 401
    :catch_1
    move-exception v0

    .line 402
    .local v0, "e1":Ljava/lang/Exception;
    const-string v1, "Enterprise-restric"

    const-string v2, "Failed to set usb function"

    invoke-static {v1, v2, v0}, Landroid/util/Slog;->d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    .line 405
    .end local v0    # "e1":Ljava/lang/Exception;
    .end local v3    # "e":Ljava/lang/Exception;
    :goto_0
    return-void
.end method

.method private shouldClose(I)Z
    .locals 1
    .param p1, "state"    # I

    .line 197
    if-eqz p1, :cond_1

    const/4 v0, 0x3

    if-ne p1, v0, :cond_0

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    goto :goto_1

    :cond_1
    :goto_0
    const/4 v0, 0x1

    :goto_1
    return v0
.end method

.method private shouldOpen(I)Z
    .locals 1
    .param p1, "state"    # I

    .line 193
    const/4 v0, 0x2

    if-eq p1, v0, :cond_1

    const/4 v0, 0x4

    if-ne p1, v0, :cond_0

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    goto :goto_1

    :cond_1
    :goto_0
    const/4 v0, 0x1

    :goto_1
    return v0
.end method

.method private startWatchLocationRestriction()V
    .locals 5

    .line 100
    const-string v0, "location_providers_allowed"

    invoke-static {v0}, Landroid/provider/Settings$Secure;->getUriFor(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v0

    .line 101
    .local v0, "uri":Landroid/net/Uri;
    iget-object v1, p0, Lcom/miui/server/enterprise/RestrictionsManagerService;->mContext:Landroid/content/Context;

    invoke-virtual {v1}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v1

    new-instance v2, Lcom/miui/server/enterprise/RestrictionsManagerService$1;

    iget-object v3, p0, Lcom/miui/server/enterprise/RestrictionsManagerService;->mHandler:Landroid/os/Handler;

    invoke-direct {v2, p0, v3}, Lcom/miui/server/enterprise/RestrictionsManagerService$1;-><init>(Lcom/miui/server/enterprise/RestrictionsManagerService;Landroid/os/Handler;)V

    const/4 v3, -0x1

    const/4 v4, 0x1

    invoke-virtual {v1, v0, v4, v2, v3}, Landroid/content/ContentResolver;->registerContentObserver(Landroid/net/Uri;ZLandroid/database/ContentObserver;I)V

    .line 120
    return-void
.end method

.method private unmountPublicVolume(I)V
    .locals 5
    .param p1, "volFlag"    # I

    .line 346
    iget-object v0, p0, Lcom/miui/server/enterprise/RestrictionsManagerService;->mContext:Landroid/content/Context;

    const-class v1, Landroid/os/storage/StorageManager;

    invoke-virtual {v0, v1}, Landroid/content/Context;->getSystemService(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/os/storage/StorageManager;

    .line 347
    .local v0, "storageManager":Landroid/os/storage/StorageManager;
    const/4 v1, 0x0

    .line 348
    .local v1, "usbVol":Landroid/os/storage/VolumeInfo;
    invoke-virtual {v0}, Landroid/os/storage/StorageManager;->getVolumes()Ljava/util/List;

    move-result-object v2

    invoke-interface {v2}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_1

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Landroid/os/storage/VolumeInfo;

    .line 349
    .local v3, "vol":Landroid/os/storage/VolumeInfo;
    invoke-virtual {v3}, Landroid/os/storage/VolumeInfo;->getType()I

    move-result v4

    if-nez v4, :cond_0

    .line 350
    invoke-virtual {v3}, Landroid/os/storage/VolumeInfo;->getDisk()Landroid/os/storage/DiskInfo;

    move-result-object v4

    iget v4, v4, Landroid/os/storage/DiskInfo;->flags:I

    and-int/2addr v4, p1

    if-ne v4, p1, :cond_0

    .line 351
    move-object v1, v3

    .line 352
    goto :goto_1

    .line 354
    .end local v3    # "vol":Landroid/os/storage/VolumeInfo;
    :cond_0
    goto :goto_0

    .line 355
    :cond_1
    :goto_1
    if-eqz v1, :cond_2

    .line 356
    invoke-virtual {v1}, Landroid/os/storage/VolumeInfo;->getState()I

    move-result v2

    const/4 v3, 0x2

    if-ne v2, v3, :cond_2

    .line 357
    invoke-virtual {v1}, Landroid/os/storage/VolumeInfo;->getId()Ljava/lang/String;

    move-result-object v2

    .line 358
    .local v2, "volId":Ljava/lang/String;
    new-instance v3, Ljava/lang/Thread;

    new-instance v4, Lcom/miui/server/enterprise/RestrictionsManagerService$2;

    invoke-direct {v4, p0, v0, v2}, Lcom/miui/server/enterprise/RestrictionsManagerService$2;-><init>(Lcom/miui/server/enterprise/RestrictionsManagerService;Landroid/os/storage/StorageManager;Ljava/lang/String;)V

    invoke-direct {v3, v4}, Ljava/lang/Thread;-><init>(Ljava/lang/Runnable;)V

    .line 363
    invoke-virtual {v3}, Ljava/lang/Thread;->start()V

    .line 366
    .end local v2    # "volId":Ljava/lang/String;
    :cond_2
    return-void
.end method


# virtual methods
.method bootComplete()V
    .locals 11

    .line 64
    const-string v0, "Enterprise-restric"

    const-string v1, "Restriction init"

    invoke-static {v0, v1}, Landroid/util/Slog;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 65
    iget-object v0, p0, Lcom/miui/server/enterprise/RestrictionsManagerService;->mContext:Landroid/content/Context;

    const-string/jumbo v1, "user"

    invoke-virtual {v0, v1}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/os/UserManager;

    .line 66
    .local v0, "userManager":Landroid/os/UserManager;
    invoke-virtual {v0}, Landroid/os/UserManager;->getUsers()Ljava/util/List;

    move-result-object v1

    .line 67
    .local v1, "users":Ljava/util/List;, "Ljava/util/List<Landroid/content/pm/UserInfo;>;"
    invoke-interface {v1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_4

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Landroid/content/pm/UserInfo;

    .line 68
    .local v3, "user":Landroid/content/pm/UserInfo;
    iget-object v4, p0, Lcom/miui/server/enterprise/RestrictionsManagerService;->mContext:Landroid/content/Context;

    const-string v5, "disallow_screencapture"

    iget v6, v3, Landroid/content/pm/UserInfo;->id:I

    invoke-static {v4, v5, v6}, Lcom/miui/enterprise/RestrictionsHelper;->hasRestriction(Landroid/content/Context;Ljava/lang/String;I)Z

    move-result v4

    if-eqz v4, :cond_0

    .line 69
    iget-object v4, p0, Lcom/miui/server/enterprise/RestrictionsManagerService;->mWindowManagerService:Lcom/android/server/wm/WindowManagerService;

    iget-object v5, p0, Lcom/miui/server/enterprise/RestrictionsManagerService;->mContext:Landroid/content/Context;

    iget v6, v3, Landroid/content/pm/UserInfo;->id:I

    const/4 v7, 0x1

    invoke-static {v4, v5, v6, v7}, Lcom/miui/server/enterprise/RestrictionManagerServiceProxy;->setScreenCaptureDisabled(Lcom/android/server/wm/WindowManagerService;Landroid/content/Context;IZ)V

    .line 71
    :cond_0
    iget-object v4, p0, Lcom/miui/server/enterprise/RestrictionsManagerService;->mContext:Landroid/content/Context;

    const-string v5, "disallow_vpn"

    iget v6, v3, Landroid/content/pm/UserInfo;->id:I

    invoke-static {v4, v5, v6}, Lcom/miui/enterprise/RestrictionsHelper;->hasRestriction(Landroid/content/Context;Ljava/lang/String;I)Z

    move-result v4

    if-eqz v4, :cond_1

    .line 72
    iget-object v5, p0, Lcom/miui/server/enterprise/RestrictionsManagerService;->mAppOpsManager:Landroid/app/AppOpsManager;

    const/16 v6, 0x2f

    const/4 v7, 0x1

    const/4 v9, 0x0

    iget v10, v3, Landroid/content/pm/UserInfo;->id:I

    move-object v8, p0

    invoke-virtual/range {v5 .. v10}, Landroid/app/AppOpsManager;->setUserRestrictionForUser(IZLandroid/os/IBinder;Landroid/os/PackageTagsList;I)V

    .line 74
    :cond_1
    iget-object v4, p0, Lcom/miui/server/enterprise/RestrictionsManagerService;->mContext:Landroid/content/Context;

    const-string v5, "disallow_fingerprint"

    iget v6, v3, Landroid/content/pm/UserInfo;->id:I

    invoke-static {v4, v5, v6}, Lcom/miui/enterprise/RestrictionsHelper;->hasRestriction(Landroid/content/Context;Ljava/lang/String;I)Z

    move-result v4

    if-eqz v4, :cond_2

    .line 75
    iget-object v5, p0, Lcom/miui/server/enterprise/RestrictionsManagerService;->mAppOpsManager:Landroid/app/AppOpsManager;

    const/16 v6, 0x37

    const/4 v7, 0x1

    const/4 v9, 0x0

    iget v10, v3, Landroid/content/pm/UserInfo;->id:I

    move-object v8, p0

    invoke-virtual/range {v5 .. v10}, Landroid/app/AppOpsManager;->setUserRestrictionForUser(IZLandroid/os/IBinder;Landroid/os/PackageTagsList;I)V

    .line 77
    :cond_2
    iget-object v4, p0, Lcom/miui/server/enterprise/RestrictionsManagerService;->mContext:Landroid/content/Context;

    const-string v5, "disallow_imeiread"

    iget v6, v3, Landroid/content/pm/UserInfo;->id:I

    invoke-static {v4, v5, v6}, Lcom/miui/enterprise/RestrictionsHelper;->hasRestriction(Landroid/content/Context;Ljava/lang/String;I)Z

    move-result v4

    if-eqz v4, :cond_3

    .line 78
    iget-object v5, p0, Lcom/miui/server/enterprise/RestrictionsManagerService;->mAppOpsManager:Landroid/app/AppOpsManager;

    const/16 v6, 0x33

    const/4 v7, 0x1

    const/4 v9, 0x0

    iget v10, v3, Landroid/content/pm/UserInfo;->id:I

    move-object v8, p0

    invoke-virtual/range {v5 .. v10}, Landroid/app/AppOpsManager;->setUserRestrictionForUser(IZLandroid/os/IBinder;Landroid/os/PackageTagsList;I)V

    .line 80
    .end local v3    # "user":Landroid/content/pm/UserInfo;
    :cond_3
    goto :goto_0

    .line 81
    :cond_4
    invoke-direct {p0}, Lcom/miui/server/enterprise/RestrictionsManagerService;->startWatchLocationRestriction()V

    .line 82
    return-void
.end method

.method public getControlStatus(Ljava/lang/String;I)I
    .locals 2
    .param p1, "key"    # Ljava/lang/String;
    .param p2, "userId"    # I

    .line 335
    iget-object v0, p0, Lcom/miui/server/enterprise/RestrictionsManagerService;->mContext:Landroid/content/Context;

    invoke-static {v0}, Lcom/miui/server/enterprise/ServiceUtils;->checkPermission(Landroid/content/Context;)V

    .line 336
    iget-object v0, p0, Lcom/miui/server/enterprise/RestrictionsManagerService;->mContext:Landroid/content/Context;

    const/4 v1, 0x1

    invoke-static {v0, p1, v1, p2}, Lcom/miui/enterprise/settings/EnterpriseSettings;->getInt(Landroid/content/Context;Ljava/lang/String;II)I

    move-result v0

    return v0
.end method

.method public hasRestriction(Ljava/lang/String;I)Z
    .locals 3
    .param p1, "key"    # Ljava/lang/String;
    .param p2, "userId"    # I

    .line 341
    iget-object v0, p0, Lcom/miui/server/enterprise/RestrictionsManagerService;->mContext:Landroid/content/Context;

    invoke-static {v0}, Lcom/miui/server/enterprise/ServiceUtils;->checkPermission(Landroid/content/Context;)V

    .line 342
    iget-object v0, p0, Lcom/miui/server/enterprise/RestrictionsManagerService;->mContext:Landroid/content/Context;

    const/4 v1, 0x0

    invoke-static {v0, p1, v1, p2}, Lcom/miui/enterprise/settings/EnterpriseSettings;->getInt(Landroid/content/Context;Ljava/lang/String;II)I

    move-result v0

    const/4 v2, 0x1

    if-ne v0, v2, :cond_0

    move v1, v2

    :cond_0
    return v1
.end method

.method public setControlStatus(Ljava/lang/String;II)V
    .locals 7
    .param p1, "key"    # Ljava/lang/String;
    .param p2, "value"    # I
    .param p3, "userId"    # I

    .line 124
    iget-object v0, p0, Lcom/miui/server/enterprise/RestrictionsManagerService;->mContext:Landroid/content/Context;

    invoke-static {v0}, Lcom/miui/server/enterprise/ServiceUtils;->checkPermission(Landroid/content/Context;)V

    .line 125
    invoke-static {}, Lmiui/securityspace/CrossUserUtils;->getCurrentUserId()I

    move-result v0

    if-eq v0, p3, :cond_0

    .line 126
    return-void

    .line 128
    :cond_0
    iget-object v0, p0, Lcom/miui/server/enterprise/RestrictionsManagerService;->mContext:Landroid/content/Context;

    const/4 v1, 0x1

    invoke-static {v0, p1, v1, p3}, Lcom/miui/enterprise/settings/EnterpriseSettings;->getInt(Landroid/content/Context;Ljava/lang/String;II)I

    move-result v0

    .line 129
    .local v0, "originState":I
    const/4 v2, 0x4

    const/4 v3, 0x2

    const/4 v4, 0x3

    if-eqz v0, :cond_1

    if-ne v0, v2, :cond_2

    :cond_1
    if-eq p2, v3, :cond_a

    if-ne p2, v4, :cond_2

    goto/16 :goto_3

    .line 134
    :cond_2
    iget-object v5, p0, Lcom/miui/server/enterprise/RestrictionsManagerService;->mContext:Landroid/content/Context;

    invoke-static {v5, p1, v1, p3}, Lcom/miui/enterprise/settings/EnterpriseSettings;->putInt(Landroid/content/Context;Ljava/lang/String;II)V

    .line 135
    invoke-virtual {p1}, Ljava/lang/String;->hashCode()I

    move-result v5

    const/4 v6, 0x0

    sparse-switch v5, :sswitch_data_0

    :cond_3
    goto :goto_0

    :sswitch_0
    const-string v3, "airplane_state"

    invoke-virtual {p1, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_3

    goto :goto_1

    :sswitch_1
    const-string v2, "nfc_state"

    invoke-virtual {p1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_3

    move v2, v4

    goto :goto_1

    :sswitch_2
    const-string/jumbo v2, "wifi_state"

    invoke-virtual {p1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_3

    move v2, v6

    goto :goto_1

    :sswitch_3
    const-string v2, "bluetooth_state"

    invoke-virtual {p1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_3

    move v2, v1

    goto :goto_1

    :sswitch_4
    const-string v2, "gps_state"

    invoke-virtual {p1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_3

    move v2, v3

    goto :goto_1

    :goto_0
    const/4 v2, -0x1

    :goto_1
    packed-switch v2, :pswitch_data_0

    .line 186
    new-instance v1, Ljava/lang/IllegalArgumentException;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "Unknown restriction item: "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-direct {v1, v2}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v1

    .line 177
    :pswitch_0
    iget-object v2, p0, Lcom/miui/server/enterprise/RestrictionsManagerService;->mContext:Landroid/content/Context;

    const-string v3, "connectivity"

    invoke-virtual {v2, v3}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Landroid/net/ConnectivityManager;

    .line 178
    .local v2, "connectivityManager":Landroid/net/ConnectivityManager;
    invoke-direct {p0, p2}, Lcom/miui/server/enterprise/RestrictionsManagerService;->shouldClose(I)Z

    move-result v3

    if-eqz v3, :cond_4

    .line 179
    invoke-virtual {v2, v6}, Landroid/net/ConnectivityManager;->setAirplaneMode(Z)V

    .line 181
    :cond_4
    invoke-direct {p0, p2}, Lcom/miui/server/enterprise/RestrictionsManagerService;->shouldOpen(I)Z

    move-result v3

    if-eqz v3, :cond_9

    .line 182
    invoke-virtual {v2, v1}, Landroid/net/ConnectivityManager;->setAirplaneMode(Z)V

    goto/16 :goto_2

    .line 166
    .end local v2    # "connectivityManager":Landroid/net/ConnectivityManager;
    :pswitch_1
    iget-object v1, p0, Lcom/miui/server/enterprise/RestrictionsManagerService;->mContext:Landroid/content/Context;

    invoke-static {v1}, Landroid/nfc/NfcAdapter;->getDefaultAdapter(Landroid/content/Context;)Landroid/nfc/NfcAdapter;

    move-result-object v1

    .line 167
    .local v1, "adapter":Landroid/nfc/NfcAdapter;
    if-eqz v1, :cond_9

    .line 168
    invoke-direct {p0, p2}, Lcom/miui/server/enterprise/RestrictionsManagerService;->shouldClose(I)Z

    move-result v2

    if-eqz v2, :cond_5

    .line 169
    invoke-virtual {v1}, Landroid/nfc/NfcAdapter;->disable()Z

    .line 171
    :cond_5
    invoke-direct {p0, p2}, Lcom/miui/server/enterprise/RestrictionsManagerService;->shouldOpen(I)Z

    move-result v2

    if-eqz v2, :cond_9

    .line 172
    invoke-virtual {v1}, Landroid/nfc/NfcAdapter;->enable()Z

    goto :goto_2

    .line 156
    .end local v1    # "adapter":Landroid/nfc/NfcAdapter;
    :pswitch_2
    invoke-direct {p0, p2}, Lcom/miui/server/enterprise/RestrictionsManagerService;->shouldClose(I)Z

    move-result v1

    const-string v2, "location_mode"

    if-eqz v1, :cond_6

    .line 157
    iget-object v1, p0, Lcom/miui/server/enterprise/RestrictionsManagerService;->mContext:Landroid/content/Context;

    invoke-virtual {v1}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v1

    invoke-static {v1, v2, v6, p3}, Landroid/provider/Settings$Secure;->putIntForUser(Landroid/content/ContentResolver;Ljava/lang/String;II)Z

    .line 160
    :cond_6
    invoke-direct {p0, p2}, Lcom/miui/server/enterprise/RestrictionsManagerService;->shouldOpen(I)Z

    move-result v1

    if-eqz v1, :cond_9

    .line 161
    iget-object v1, p0, Lcom/miui/server/enterprise/RestrictionsManagerService;->mContext:Landroid/content/Context;

    invoke-virtual {v1}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v1

    invoke-static {v1, v2, v4, p3}, Landroid/provider/Settings$Secure;->putIntForUser(Landroid/content/ContentResolver;Ljava/lang/String;II)Z

    goto :goto_2

    .line 147
    :pswitch_3
    invoke-static {}, Landroid/bluetooth/BluetoothAdapter;->getDefaultAdapter()Landroid/bluetooth/BluetoothAdapter;

    move-result-object v1

    .line 148
    .local v1, "btAdapter":Landroid/bluetooth/BluetoothAdapter;
    invoke-direct {p0, p2}, Lcom/miui/server/enterprise/RestrictionsManagerService;->shouldClose(I)Z

    move-result v2

    if-eqz v2, :cond_7

    .line 149
    invoke-virtual {v1}, Landroid/bluetooth/BluetoothAdapter;->disable()Z

    .line 151
    :cond_7
    invoke-direct {p0, p2}, Lcom/miui/server/enterprise/RestrictionsManagerService;->shouldOpen(I)Z

    move-result v2

    if-eqz v2, :cond_9

    .line 152
    invoke-virtual {v1}, Landroid/bluetooth/BluetoothAdapter;->enable()Z

    goto :goto_2

    .line 138
    .end local v1    # "btAdapter":Landroid/bluetooth/BluetoothAdapter;
    :pswitch_4
    iget-object v2, p0, Lcom/miui/server/enterprise/RestrictionsManagerService;->mContext:Landroid/content/Context;

    const-string/jumbo v3, "wifi"

    invoke-virtual {v2, v3}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Landroid/net/wifi/WifiManager;

    .line 139
    .local v2, "manager":Landroid/net/wifi/WifiManager;
    invoke-direct {p0, p2}, Lcom/miui/server/enterprise/RestrictionsManagerService;->shouldClose(I)Z

    move-result v3

    if-eqz v3, :cond_8

    .line 140
    invoke-virtual {v2, v6}, Landroid/net/wifi/WifiManager;->setWifiEnabled(Z)Z

    .line 142
    :cond_8
    invoke-direct {p0, p2}, Lcom/miui/server/enterprise/RestrictionsManagerService;->shouldOpen(I)Z

    move-result v3

    if-eqz v3, :cond_9

    .line 143
    invoke-virtual {v2, v1}, Landroid/net/wifi/WifiManager;->setWifiEnabled(Z)Z

    .line 189
    .end local v2    # "manager":Landroid/net/wifi/WifiManager;
    :cond_9
    :goto_2
    iget-object v1, p0, Lcom/miui/server/enterprise/RestrictionsManagerService;->mContext:Landroid/content/Context;

    invoke-static {v1, p1, p2, p3}, Lcom/miui/enterprise/settings/EnterpriseSettings;->putInt(Landroid/content/Context;Ljava/lang/String;II)V

    .line 190
    return-void

    .line 131
    :cond_a
    :goto_3
    return-void

    :sswitch_data_0
    .sparse-switch
        -0x79dfef44 -> :sswitch_4
        -0x601b5040 -> :sswitch_3
        0x1d0272e7 -> :sswitch_2
        0x36cfa4fd -> :sswitch_1
        0x61188164 -> :sswitch_0
    .end sparse-switch

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_4
        :pswitch_3
        :pswitch_2
        :pswitch_1
        :pswitch_0
    .end packed-switch
.end method

.method public setRestriction(Ljava/lang/String;ZI)V
    .locals 8
    .param p1, "key"    # Ljava/lang/String;
    .param p2, "value"    # Z
    .param p3, "userId"    # I

    .line 202
    iget-object v0, p0, Lcom/miui/server/enterprise/RestrictionsManagerService;->mContext:Landroid/content/Context;

    invoke-static {v0}, Lcom/miui/server/enterprise/ServiceUtils;->checkPermission(Landroid/content/Context;)V

    .line 203
    iget-object v0, p0, Lcom/miui/server/enterprise/RestrictionsManagerService;->mContext:Landroid/content/Context;

    invoke-static {v0, p1, p2, p3}, Lcom/miui/enterprise/settings/EnterpriseSettings;->putInt(Landroid/content/Context;Ljava/lang/String;II)V

    .line 204
    invoke-virtual {p1}, Ljava/lang/String;->hashCode()I

    move-result v0

    const/4 v1, 0x4

    const/16 v2, 0x8

    const/4 v3, 0x0

    sparse-switch v0, :sswitch_data_0

    :cond_0
    goto/16 :goto_0

    :sswitch_0
    const-string v0, "disallow_microphone"

    invoke-virtual {p1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x3

    goto/16 :goto_1

    :sswitch_1
    const-string v0, "disable_accelerometer"

    invoke-virtual {p1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    const/16 v0, 0xe

    goto/16 :goto_1

    :sswitch_2
    const-string v0, "disallow_mi_account"

    invoke-virtual {p1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    const/16 v0, 0x15

    goto/16 :goto_1

    :sswitch_3
    const-string v0, "disallow_status_bar"

    invoke-virtual {p1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    const/16 v0, 0x14

    goto/16 :goto_1

    :sswitch_4
    const-string v0, "disallow_fingerprint"

    invoke-virtual {p1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    const/16 v0, 0xa

    goto/16 :goto_1

    :sswitch_5
    const-string v0, "disallow_change_language"

    invoke-virtual {p1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    const/16 v0, 0x12

    goto/16 :goto_1

    :sswitch_6
    const-string v0, "disallow_screencapture"

    invoke-virtual {p1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    move v0, v1

    goto/16 :goto_1

    :sswitch_7
    const-string v0, "disallow_key_menu"

    invoke-virtual {p1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    const/16 v0, 0x18

    goto/16 :goto_1

    :sswitch_8
    const-string v0, "disallow_key_home"

    invoke-virtual {p1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    const/16 v0, 0x17

    goto/16 :goto_1

    :sswitch_9
    const-string v0, "disallow_key_back"

    invoke-virtual {p1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    const/16 v0, 0x16

    goto/16 :goto_1

    :sswitch_a
    const-string v0, "disallow_safe_mode"

    invoke-virtual {p1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    const/16 v0, 0x11

    goto/16 :goto_1

    :sswitch_b
    const-string v0, "disallow_factoryreset"

    invoke-virtual {p1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    const/16 v0, 0xb

    goto/16 :goto_1

    :sswitch_c
    const-string v0, "disable_usb_device"

    invoke-virtual {p1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    move v0, v2

    goto/16 :goto_1

    :sswitch_d
    const-string v0, "disallow_timeset"

    invoke-virtual {p1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    const/16 v0, 0xc

    goto/16 :goto_1

    :sswitch_e
    const-string v0, "disallow_vpn"

    invoke-virtual {p1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    move v0, v3

    goto/16 :goto_1

    :sswitch_f
    const-string v0, "disallow_otg"

    invoke-virtual {p1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x7

    goto/16 :goto_1

    :sswitch_10
    const-string v0, "disallow_mtp"

    invoke-virtual {p1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x6

    goto :goto_1

    :sswitch_11
    const-string v0, "disallow_usbdebug"

    invoke-virtual {p1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    const/16 v0, 0x9

    goto :goto_1

    :sswitch_12
    const-string v0, "disallow_imeiread"

    invoke-virtual {p1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    const/16 v0, 0xd

    goto :goto_1

    :sswitch_13
    const-string v0, "disallow_auto_sync"

    invoke-virtual {p1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    const/16 v0, 0x10

    goto :goto_1

    :sswitch_14
    const-string v0, "disallow_tether"

    invoke-virtual {p1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    goto :goto_1

    :sswitch_15
    const-string v0, "disallow_sdcard"

    invoke-virtual {p1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x5

    goto :goto_1

    :sswitch_16
    const-string v0, "disallow_landscape_statusbar"

    invoke-virtual {p1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    const/16 v0, 0x19

    goto :goto_1

    :sswitch_17
    const-string v0, "disallow_system_update"

    invoke-virtual {p1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    const/16 v0, 0x13

    goto :goto_1

    :sswitch_18
    const-string v0, "disallow_camera"

    invoke-virtual {p1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x2

    goto :goto_1

    :sswitch_19
    const-string v0, "disallow_backup"

    invoke-virtual {p1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    const/16 v0, 0xf

    goto :goto_1

    :goto_0
    const/4 v0, -0x1

    :goto_1
    const-string v4, "Enterprise-restric"

    packed-switch v0, :pswitch_data_0

    .line 329
    new-instance v0, Ljava/lang/IllegalArgumentException;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Unknown restriction item: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 327
    :pswitch_0
    goto/16 :goto_3

    .line 325
    :pswitch_1
    goto/16 :goto_3

    .line 323
    :pswitch_2
    goto/16 :goto_3

    .line 321
    :pswitch_3
    goto/16 :goto_3

    .line 319
    :pswitch_4
    goto/16 :goto_3

    .line 311
    :pswitch_5
    iget-object v0, p0, Lcom/miui/server/enterprise/RestrictionsManagerService;->mContext:Landroid/content/Context;

    const-string/jumbo v1, "statusbar"

    invoke-virtual {v0, v1}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/app/StatusBarManager;

    .line 312
    .local v0, "statusBarManager":Landroid/app/StatusBarManager;
    if-nez v0, :cond_1

    .line 313
    const-string/jumbo v1, "statusBarManager is null!"

    invoke-static {v4, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 314
    goto/16 :goto_3

    .line 316
    :cond_1
    if-eqz p2, :cond_2

    const/high16 v3, 0x10000

    :cond_2
    invoke-virtual {v0, v3}, Landroid/app/StatusBarManager;->disable(I)V

    .line 317
    goto/16 :goto_3

    .line 309
    .end local v0    # "statusBarManager":Landroid/app/StatusBarManager;
    :pswitch_6
    goto/16 :goto_3

    .line 306
    :pswitch_7
    sget-object v0, Ljava/util/Locale;->CHINA:Ljava/util/Locale;

    invoke-static {v0}, Lcom/android/internal/app/LocalePicker;->updateLocale(Ljava/util/Locale;)V

    .line 307
    goto/16 :goto_3

    .line 303
    :pswitch_8
    iget-object v0, p0, Lcom/miui/server/enterprise/RestrictionsManagerService;->mUserManager:Lcom/android/server/pm/UserManagerService;

    const-string v1, "no_safe_boot"

    invoke-virtual {v0, v1, p2, p3}, Lcom/android/server/pm/UserManagerService;->setUserRestriction(Ljava/lang/String;ZI)V

    .line 304
    goto/16 :goto_3

    .line 297
    :pswitch_9
    if-eqz p2, :cond_a

    .line 298
    invoke-static {v3, p3}, Landroid/content/ContentResolver;->setMasterSyncAutomaticallyAsUser(ZI)V

    .line 299
    invoke-direct {p0, p3}, Lcom/miui/server/enterprise/RestrictionsManagerService;->closeCloudBackup(I)V

    goto/16 :goto_3

    .line 286
    :pswitch_a
    if-eqz p2, :cond_3

    .line 287
    iget-object v1, p0, Lcom/miui/server/enterprise/RestrictionsManagerService;->mPMS:Lcom/android/server/pm/PackageManagerService$IPackageManagerImpl;

    const-string v2, "com.miui.backup"

    const/4 v3, 0x2

    const/4 v4, 0x0

    iget-object v0, p0, Lcom/miui/server/enterprise/RestrictionsManagerService;->mContext:Landroid/content/Context;

    .line 289
    invoke-virtual {v0}, Landroid/content/Context;->getPackageName()Ljava/lang/String;

    move-result-object v6

    .line 287
    move v5, p3

    invoke-virtual/range {v1 .. v6}, Lcom/android/server/pm/PackageManagerService$IPackageManagerImpl;->setApplicationEnabledSetting(Ljava/lang/String;IIILjava/lang/String;)V

    goto/16 :goto_3

    .line 291
    :cond_3
    iget-object v1, p0, Lcom/miui/server/enterprise/RestrictionsManagerService;->mPMS:Lcom/android/server/pm/PackageManagerService$IPackageManagerImpl;

    const-string v2, "com.miui.backup"

    const/4 v3, 0x1

    const/4 v4, 0x0

    iget-object v0, p0, Lcom/miui/server/enterprise/RestrictionsManagerService;->mContext:Landroid/content/Context;

    .line 293
    invoke-virtual {v0}, Landroid/content/Context;->getPackageName()Ljava/lang/String;

    move-result-object v6

    .line 291
    move v5, p3

    invoke-virtual/range {v1 .. v6}, Lcom/android/server/pm/PackageManagerService$IPackageManagerImpl;->setApplicationEnabledSetting(Ljava/lang/String;IIILjava/lang/String;)V

    .line 295
    goto/16 :goto_3

    .line 284
    :pswitch_b
    goto/16 :goto_3

    .line 280
    :pswitch_c
    iget-object v1, p0, Lcom/miui/server/enterprise/RestrictionsManagerService;->mAppOpsManager:Landroid/app/AppOpsManager;

    const/16 v2, 0x33

    const/4 v5, 0x0

    move v3, p2

    move-object v4, p0

    move v6, p3

    invoke-virtual/range {v1 .. v6}, Landroid/app/AppOpsManager;->setUserRestrictionForUser(IZLandroid/os/IBinder;Landroid/os/PackageTagsList;I)V

    .line 281
    goto/16 :goto_3

    .line 273
    :pswitch_d
    invoke-static {}, Lmiui/securityspace/CrossUserUtils;->getCurrentUserId()I

    move-result v0

    if-ne v0, p3, :cond_4

    .line 274
    iget-object v0, p0, Lcom/miui/server/enterprise/RestrictionsManagerService;->mContext:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    const-string v1, "auto_time"

    invoke-static {v0, v1, p2}, Landroid/provider/Settings$Global;->putInt(Landroid/content/ContentResolver;Ljava/lang/String;I)Z

    .line 276
    :cond_4
    iget-object v0, p0, Lcom/miui/server/enterprise/RestrictionsManagerService;->mContext:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    .line 277
    nop

    .line 276
    const-string/jumbo v1, "time_change_disallow"

    invoke-static {v0, v1, p2, p3}, Landroid/provider/Settings$Secure;->putIntForUser(Landroid/content/ContentResolver;Ljava/lang/String;II)Z

    .line 278
    goto/16 :goto_3

    .line 270
    :pswitch_e
    iget-object v0, p0, Lcom/miui/server/enterprise/RestrictionsManagerService;->mUserManager:Lcom/android/server/pm/UserManagerService;

    const-string v1, "no_factory_reset"

    invoke-virtual {v0, v1, p2, p3}, Lcom/android/server/pm/UserManagerService;->setUserRestriction(Ljava/lang/String;ZI)V

    .line 271
    goto/16 :goto_3

    .line 267
    :pswitch_f
    iget-object v1, p0, Lcom/miui/server/enterprise/RestrictionsManagerService;->mAppOpsManager:Landroid/app/AppOpsManager;

    const/16 v2, 0x37

    const/4 v5, 0x0

    move v3, p2

    move-object v4, p0

    move v6, p3

    invoke-virtual/range {v1 .. v6}, Landroid/app/AppOpsManager;->setUserRestrictionForUser(IZLandroid/os/IBinder;Landroid/os/PackageTagsList;I)V

    .line 268
    goto/16 :goto_3

    .line 261
    :pswitch_10
    invoke-static {}, Lmiui/securityspace/CrossUserUtils;->getCurrentUserId()I

    move-result v0

    if-ne v0, p3, :cond_5

    .line 262
    iget-object v0, p0, Lcom/miui/server/enterprise/RestrictionsManagerService;->mContext:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    const-string v1, "adb_enabled"

    invoke-static {v0, v1, v3}, Landroid/provider/Settings$Global;->putInt(Landroid/content/ContentResolver;Ljava/lang/String;I)Z

    .line 264
    :cond_5
    iget-object v0, p0, Lcom/miui/server/enterprise/RestrictionsManagerService;->mUserManager:Lcom/android/server/pm/UserManagerService;

    const-string v1, "no_debugging_features"

    invoke-virtual {v0, v1, p2, p3}, Lcom/android/server/pm/UserManagerService;->setUserRestriction(Ljava/lang/String;ZI)V

    .line 265
    goto/16 :goto_3

    .line 256
    :pswitch_11
    if-eqz p2, :cond_a

    invoke-static {}, Lmiui/securityspace/CrossUserUtils;->getCurrentUserId()I

    move-result v0

    if-ne v0, p3, :cond_a

    .line 257
    invoke-direct {p0, v2}, Lcom/miui/server/enterprise/RestrictionsManagerService;->unmountPublicVolume(I)V

    goto/16 :goto_3

    .line 254
    :pswitch_12
    goto/16 :goto_3

    .line 249
    :pswitch_13
    iget-object v0, p0, Lcom/miui/server/enterprise/RestrictionsManagerService;->mContext:Landroid/content/Context;

    const-string/jumbo v1, "usb"

    invoke-virtual {v0, v1}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/hardware/usb/UsbManager;

    .line 250
    .local v0, "usbManager":Landroid/hardware/usb/UsbManager;
    iget-object v1, p0, Lcom/miui/server/enterprise/RestrictionsManagerService;->mUserManager:Lcom/android/server/pm/UserManagerService;

    const-string v2, "no_usb_file_transfer"

    invoke-virtual {v1, v2, p2, p3}, Lcom/android/server/pm/UserManagerService;->setUserRestriction(Ljava/lang/String;ZI)V

    .line 251
    const-string v1, "none"

    invoke-direct {p0, v0, v1}, Lcom/miui/server/enterprise/RestrictionsManagerService;->setUsbFunction(Landroid/hardware/usb/UsbManager;Ljava/lang/String;)V

    .line 252
    goto/16 :goto_3

    .line 244
    .end local v0    # "usbManager":Landroid/hardware/usb/UsbManager;
    :pswitch_14
    if-eqz p2, :cond_a

    invoke-static {}, Lmiui/securityspace/CrossUserUtils;->getCurrentUserId()I

    move-result v0

    if-ne v0, p3, :cond_a

    .line 245
    invoke-direct {p0, v1}, Lcom/miui/server/enterprise/RestrictionsManagerService;->unmountPublicVolume(I)V

    goto/16 :goto_3

    .line 241
    :pswitch_15
    iget-object v0, p0, Lcom/miui/server/enterprise/RestrictionsManagerService;->mWindowManagerService:Lcom/android/server/wm/WindowManagerService;

    iget-object v1, p0, Lcom/miui/server/enterprise/RestrictionsManagerService;->mContext:Landroid/content/Context;

    invoke-static {v0, v1, p3, p2}, Lcom/miui/server/enterprise/RestrictionManagerServiceProxy;->setScreenCaptureDisabled(Lcom/android/server/wm/WindowManagerService;Landroid/content/Context;IZ)V

    .line 242
    goto/16 :goto_3

    .line 238
    :pswitch_16
    iget-object v0, p0, Lcom/miui/server/enterprise/RestrictionsManagerService;->mUserManager:Lcom/android/server/pm/UserManagerService;

    const-string v1, "no_record_audio"

    invoke-virtual {v0, v1, p2, p3}, Lcom/android/server/pm/UserManagerService;->setUserRestriction(Ljava/lang/String;ZI)V

    .line 239
    goto/16 :goto_3

    .line 234
    :pswitch_17
    iget-object v0, p0, Lcom/miui/server/enterprise/RestrictionsManagerService;->mUserManager:Lcom/android/server/pm/UserManagerService;

    const-string v1, "no_camera"

    invoke-virtual {v0, v1, p2, p3}, Lcom/android/server/pm/UserManagerService;->setUserRestriction(Ljava/lang/String;ZI)V

    .line 236
    goto/16 :goto_3

    .line 227
    :pswitch_18
    iget-object v0, p0, Lcom/miui/server/enterprise/RestrictionsManagerService;->mUserManager:Lcom/android/server/pm/UserManagerService;

    const-string v1, "no_config_tethering"

    invoke-virtual {v0, v1, p3}, Lcom/android/server/pm/UserManagerService;->hasUserRestriction(Ljava/lang/String;I)Z

    move-result v0

    .line 228
    .local v0, "hasUserRestriction":Z
    if-eqz p2, :cond_6

    invoke-static {}, Lmiui/securityspace/CrossUserUtils;->getCurrentUserId()I

    move-result v2

    if-ne p3, v2, :cond_6

    if-nez v0, :cond_6

    .line 229
    iget-object v2, p0, Lcom/miui/server/enterprise/RestrictionsManagerService;->mContext:Landroid/content/Context;

    invoke-static {v2, v3}, Lcom/miui/server/enterprise/RestrictionManagerServiceProxy;->setWifiApEnabled(Landroid/content/Context;Z)V

    .line 231
    :cond_6
    iget-object v2, p0, Lcom/miui/server/enterprise/RestrictionsManagerService;->mUserManager:Lcom/android/server/pm/UserManagerService;

    invoke-virtual {v2, v1, p2, p3}, Lcom/android/server/pm/UserManagerService;->setUserRestriction(Ljava/lang/String;ZI)V

    .line 232
    goto :goto_3

    .line 206
    .end local v0    # "hasUserRestriction":Z
    :pswitch_19
    iget-object v0, p0, Lcom/miui/server/enterprise/RestrictionsManagerService;->mUserManager:Lcom/android/server/pm/UserManagerService;

    const-string v1, "no_config_vpn"

    invoke-virtual {v0, v1, p3}, Lcom/android/server/pm/UserManagerService;->hasUserRestriction(Ljava/lang/String;I)Z

    move-result v7

    .line 207
    .local v7, "hasUserRestriction":Z
    if-eqz p2, :cond_9

    invoke-static {}, Lmiui/securityspace/CrossUserUtils;->getCurrentUserId()I

    move-result v0

    if-ne v0, p3, :cond_9

    if-nez v7, :cond_9

    .line 209
    :try_start_0
    const-string/jumbo v0, "vpn_management"

    .line 210
    invoke-static {v0}, Landroid/os/ServiceManager;->getService(Ljava/lang/String;)Landroid/os/IBinder;

    move-result-object v0

    .line 209
    invoke-static {v0}, Landroid/net/IVpnManager$Stub;->asInterface(Landroid/os/IBinder;)Landroid/net/IVpnManager;

    move-result-object v0

    .line 211
    .local v0, "vpnmanager":Landroid/net/IVpnManager;
    invoke-static {}, Landroid/os/UserHandle;->myUserId()I

    move-result v2

    invoke-interface {v0, v2}, Landroid/net/IVpnManager;->getVpnConfig(I)Lcom/android/internal/net/VpnConfig;

    move-result-object v2

    .line 212
    .local v2, "mConfig":Lcom/android/internal/net/VpnConfig;
    if-eqz v2, :cond_7

    .line 213
    iget-object v3, v2, Lcom/android/internal/net/VpnConfig;->configureIntent:Landroid/app/PendingIntent;

    invoke-virtual {v3}, Landroid/app/PendingIntent;->send()V

    .line 215
    :cond_7
    invoke-static {}, Landroid/os/UserHandle;->myUserId()I

    move-result v3

    invoke-interface {v0, v3}, Landroid/net/IVpnManager;->getLegacyVpnInfo(I)Lcom/android/internal/net/LegacyVpnInfo;

    move-result-object v3

    .line 216
    .local v3, "info":Lcom/android/internal/net/LegacyVpnInfo;
    if-eqz v3, :cond_8

    .line 217
    iget-object v5, v3, Lcom/android/internal/net/LegacyVpnInfo;->intent:Landroid/app/PendingIntent;

    invoke-virtual {v5}, Landroid/app/PendingIntent;->send()V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 221
    .end local v0    # "vpnmanager":Landroid/net/IVpnManager;
    .end local v2    # "mConfig":Lcom/android/internal/net/VpnConfig;
    .end local v3    # "info":Lcom/android/internal/net/LegacyVpnInfo;
    :cond_8
    goto :goto_2

    .line 219
    :catch_0
    move-exception v0

    .line 220
    .local v0, "e":Ljava/lang/Exception;
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "Something wrong while close vpn: "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v4, v2}, Landroid/util/Slog;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 223
    .end local v0    # "e":Ljava/lang/Exception;
    :cond_9
    :goto_2
    iget-object v0, p0, Lcom/miui/server/enterprise/RestrictionsManagerService;->mUserManager:Lcom/android/server/pm/UserManagerService;

    invoke-virtual {v0, v1, p2, p3}, Lcom/android/server/pm/UserManagerService;->setUserRestriction(Ljava/lang/String;ZI)V

    .line 224
    iget-object v1, p0, Lcom/miui/server/enterprise/RestrictionsManagerService;->mAppOpsManager:Landroid/app/AppOpsManager;

    const/16 v2, 0x2f

    const/4 v5, 0x0

    move v3, p2

    move-object v4, p0

    move v6, p3

    invoke-virtual/range {v1 .. v6}, Landroid/app/AppOpsManager;->setUserRestrictionForUser(IZLandroid/os/IBinder;Landroid/os/PackageTagsList;I)V

    .line 225
    nop

    .line 331
    .end local v7    # "hasUserRestriction":Z
    :cond_a
    :goto_3
    return-void

    :sswitch_data_0
    .sparse-switch
        -0x7227a4fa -> :sswitch_19
        -0x706e5797 -> :sswitch_18
        -0x6edcd8eb -> :sswitch_17
        -0x5c87e867 -> :sswitch_16
        -0x54fb21db -> :sswitch_15
        -0x53305eaa -> :sswitch_14
        -0x4139f099 -> :sswitch_13
        -0x3193455e -> :sswitch_12
        -0x1b0ab86d -> :sswitch_11
        -0x199e971b -> :sswitch_10
        -0x199e8fa2 -> :sswitch_f
        -0x199e75d0 -> :sswitch_e
        -0xc6be24f -> :sswitch_d
        -0xa2e54d8 -> :sswitch_c
        0x4f1e4c9 -> :sswitch_b
        0x1853c751 -> :sswitch_a
        0x1889e70b -> :sswitch_9
        0x188cd703 -> :sswitch_8
        0x188ef783 -> :sswitch_7
        0x1c0d5b96 -> :sswitch_6
        0x1c9f9e43 -> :sswitch_5
        0x1d15ae20 -> :sswitch_4
        0x2b28d58a -> :sswitch_3
        0x361585ce -> :sswitch_2
        0x44f96f38 -> :sswitch_1
        0x6e123c6e -> :sswitch_0
    .end sparse-switch

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_19
        :pswitch_18
        :pswitch_17
        :pswitch_16
        :pswitch_15
        :pswitch_14
        :pswitch_13
        :pswitch_12
        :pswitch_11
        :pswitch_10
        :pswitch_f
        :pswitch_e
        :pswitch_d
        :pswitch_c
        :pswitch_b
        :pswitch_a
        :pswitch_9
        :pswitch_8
        :pswitch_7
        :pswitch_6
        :pswitch_5
        :pswitch_4
        :pswitch_3
        :pswitch_2
        :pswitch_1
        :pswitch_0
    .end packed-switch
.end method
