public class com.miui.server.enterprise.DeviceManagerService extends com.miui.enterprise.IDeviceManager$Stub {
	 /* .source "DeviceManagerService.java" */
	 /* # annotations */
	 /* .annotation system Ldalvik/annotation/MemberClasses; */
	 /* value = { */
	 /* Lcom/miui/server/enterprise/DeviceManagerService$ServiceHandler; */
	 /* } */
} // .end annotation
/* # static fields */
private static final java.lang.String PERSIST_ANIMATION_PATH;
private static final java.lang.String TAG;
/* # instance fields */
private android.content.Context mContext;
private android.hardware.display.DisplayManager mDisplayManager;
private com.miui.server.enterprise.DeviceManagerService$ServiceHandler mServiceHandler;
private android.net.wifi.WifiManager mWifiManager;
/* # direct methods */
static android.net.wifi.WifiManager -$$Nest$fgetmWifiManager ( com.miui.server.enterprise.DeviceManagerService p0 ) { //bridge//synthethic
	 /* .locals 0 */
	 p0 = this.mWifiManager;
} // .end method
 com.miui.server.enterprise.DeviceManagerService ( ) {
	 /* .locals 3 */
	 /* .param p1, "context" # Landroid/content/Context; */
	 /* .line 69 */
	 /* invoke-direct {p0}, Lcom/miui/enterprise/IDeviceManager$Stub;-><init>()V */
	 /* .line 70 */
	 this.mContext = p1;
	 /* .line 71 */
	 /* const-string/jumbo v0, "wifi" */
	 (( android.content.Context ) p1 ).getSystemService ( v0 ); // invoke-virtual {p1, v0}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;
	 /* check-cast v0, Landroid/net/wifi/WifiManager; */
	 this.mWifiManager = v0;
	 /* .line 72 */
	 v0 = this.mContext;
	 /* .line 73 */
	 final String v1 = "display"; // const-string v1, "display"
	 (( android.content.Context ) v0 ).getSystemService ( v1 ); // invoke-virtual {v0, v1}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;
	 /* check-cast v0, Landroid/hardware/display/DisplayManager; */
	 this.mDisplayManager = v0;
	 /* .line 74 */
	 /* new-instance v0, Landroid/os/HandlerThread; */
	 final String v1 = "DeviceManagerService"; // const-string v1, "DeviceManagerService"
	 /* invoke-direct {v0, v1}, Landroid/os/HandlerThread;-><init>(Ljava/lang/String;)V */
	 /* .line 75 */
	 /* .local v0, "thread":Landroid/os/HandlerThread; */
	 (( android.os.HandlerThread ) v0 ).start ( ); // invoke-virtual {v0}, Landroid/os/HandlerThread;->start()V
	 /* .line 76 */
	 /* new-instance v1, Lcom/miui/server/enterprise/DeviceManagerService$ServiceHandler; */
	 (( android.os.HandlerThread ) v0 ).getLooper ( ); // invoke-virtual {v0}, Landroid/os/HandlerThread;->getLooper()Landroid/os/Looper;
	 /* invoke-direct {v1, p0, v2}, Lcom/miui/server/enterprise/DeviceManagerService$ServiceHandler;-><init>(Lcom/miui/server/enterprise/DeviceManagerService;Landroid/os/Looper;)V */
	 this.mServiceHandler = v1;
	 /* .line 77 */
	 return;
} // .end method
private void createEntDir ( ) {
	 /* .locals 3 */
	 /* .annotation system Ldalvik/annotation/Throws; */
	 /* value = { */
	 /* Landroid/system/ErrnoException; */
	 /* } */
} // .end annotation
/* .line 386 */
/* new-instance v0, Ljava/io/File; */
final String v1 = "/data/system/theme_magic/enterprise/"; // const-string v1, "/data/system/theme_magic/enterprise/"
/* invoke-direct {v0, v1}, Ljava/io/File;-><init>(Ljava/lang/String;)V */
/* .line 387 */
/* .local v0, "dir":Ljava/io/File; */
v2 = (( java.io.File ) v0 ).exists ( ); // invoke-virtual {v0}, Ljava/io/File;->exists()Z
/* if-nez v2, :cond_1 */
/* .line 388 */
v2 = (( java.io.File ) v0 ).mkdirs ( ); // invoke-virtual {v0}, Ljava/io/File;->mkdirs()Z
if ( v2 != null) { // if-eqz v2, :cond_0
	 /* .line 389 */
	 /* const/16 v2, 0x1fd */
	 android.system.Os .chmod ( v1,v2 );
	 /* .line 391 */
} // :cond_0
final String v1 = "Enterprise-device"; // const-string v1, "Enterprise-device"
final String v2 = "createEntDir failed"; // const-string v2, "createEntDir failed"
android.util.Slog .e ( v1,v2 );
/* .line 394 */
} // :cond_1
} // :goto_0
return;
} // .end method
private android.graphics.Bitmap getFullScreenshot ( android.content.Context p0 ) {
/* .locals 8 */
/* .param p1, "context" # Landroid/content/Context; */
/* .line 281 */
int v0 = 2; // const/4 v0, 0x2
/* new-array v0, v0, [I */
/* .line 282 */
/* .local v0, "pixelsWH":[I */
/* const-string/jumbo v1, "window" */
(( android.content.Context ) p1 ).getSystemService ( v1 ); // invoke-virtual {p1, v1}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;
/* check-cast v1, Landroid/view/WindowManager; */
/* .line 283 */
/* .local v1, "wm":Landroid/view/WindowManager; */
/* .line 284 */
/* .local v2, "windowMetrics":Landroid/view/WindowMetrics; */
(( android.view.WindowMetrics ) v2 ).getBounds ( ); // invoke-virtual {v2}, Landroid/view/WindowMetrics;->getBounds()Landroid/graphics/Rect;
v3 = (( android.graphics.Rect ) v3 ).width ( ); // invoke-virtual {v3}, Landroid/graphics/Rect;->width()I
int v4 = 0; // const/4 v4, 0x0
/* aput v3, v0, v4 */
/* .line 285 */
(( android.view.WindowMetrics ) v2 ).getBounds ( ); // invoke-virtual {v2}, Landroid/view/WindowMetrics;->getBounds()Landroid/graphics/Rect;
v3 = (( android.graphics.Rect ) v3 ).height ( ); // invoke-virtual {v3}, Landroid/graphics/Rect;->height()I
int v5 = 1; // const/4 v5, 0x1
/* aput v3, v0, v5 */
/* .line 286 */
v3 = this.mDisplayManager;
(( android.hardware.display.DisplayManager ) v3 ).getDisplay ( v4 ); // invoke-virtual {v3, v4}, Landroid/hardware/display/DisplayManager;->getDisplay(I)Landroid/view/Display;
/* .line 287 */
/* .local v3, "defaultDisplay":Landroid/view/Display; */
/* new-instance v6, Landroid/graphics/Rect; */
/* aget v7, v0, v4 */
/* aget v5, v0, v5 */
/* invoke-direct {v6, v4, v4, v7, v5}, Landroid/graphics/Rect;-><init>(IIII)V */
/* invoke-direct {p0, v6, v3}, Lcom/miui/server/enterprise/DeviceManagerService;->getScreenshot(Landroid/graphics/Rect;Landroid/view/Display;)Landroid/graphics/Bitmap; */
} // .end method
private android.graphics.Bitmap getScreenshot ( android.graphics.Rect p0, android.view.Display p1 ) {
/* .locals 9 */
/* .param p1, "crop" # Landroid/graphics/Rect; */
/* .param p2, "display" # Landroid/view/Display; */
/* .line 292 */
int v0 = 0; // const/4 v0, 0x0
/* .line 293 */
/* .local v0, "screenshot":Landroid/graphics/Bitmap; */
v1 = (( android.graphics.Rect ) p1 ).width ( ); // invoke-virtual {p1}, Landroid/graphics/Rect;->width()I
/* .line 294 */
/* .local v1, "width":I */
v2 = (( android.graphics.Rect ) p1 ).height ( ); // invoke-virtual {p1}, Landroid/graphics/Rect;->height()I
/* .line 295 */
/* .local v2, "height":I */
(( android.view.Display ) p2 ).getAddress ( ); // invoke-virtual {p2}, Landroid/view/Display;->getAddress()Landroid/view/DisplayAddress;
/* .line 296 */
/* .local v3, "address":Landroid/view/DisplayAddress; */
/* instance-of v4, v3, Landroid/view/DisplayAddress$Physical; */
/* if-nez v4, :cond_0 */
/* .line 297 */
/* new-instance v4, Ljava/lang/StringBuilder; */
/* invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V */
final String v5 = "Skipping Screenshot - Default display does not have a physical address: "; // const-string v5, "Skipping Screenshot - Default display does not have a physical address: "
(( java.lang.StringBuilder ) v4 ).append ( v5 ); // invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v4 ).append ( p2 ); // invoke-virtual {v4, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v4 ).toString ( ); // invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
final String v5 = "Enterprise-device"; // const-string v5, "Enterprise-device"
android.util.Log .e ( v5,v4 );
/* .line 301 */
} // :cond_0
int v4 = 0; // const/4 v4, 0x0
/* .line 302 */
/* .local v4, "screenshotBuffer":Landroid/window/ScreenCapture$ScreenshotHardwareBuffer; */
/* move-object v5, v3 */
/* check-cast v5, Landroid/view/DisplayAddress$Physical; */
/* .line 305 */
/* .local v5, "physicalAddress":Landroid/view/DisplayAddress$Physical; */
(( android.view.DisplayAddress$Physical ) v5 ).getPhysicalDisplayId ( ); // invoke-virtual {v5}, Landroid/view/DisplayAddress$Physical;->getPhysicalDisplayId()J
/* move-result-wide v6 */
com.android.server.display.DisplayControl .getPhysicalDisplayToken ( v6,v7 );
/* .line 307 */
/* .local v6, "displayToken":Landroid/os/IBinder; */
/* new-instance v7, Landroid/window/ScreenCapture$DisplayCaptureArgs$Builder; */
/* invoke-direct {v7, v6}, Landroid/window/ScreenCapture$DisplayCaptureArgs$Builder;-><init>(Landroid/os/IBinder;)V */
/* .line 309 */
(( android.window.ScreenCapture$DisplayCaptureArgs$Builder ) v7 ).setSourceCrop ( p1 ); // invoke-virtual {v7, p1}, Landroid/window/ScreenCapture$DisplayCaptureArgs$Builder;->setSourceCrop(Landroid/graphics/Rect;)Landroid/window/ScreenCapture$CaptureArgs$Builder;
/* check-cast v7, Landroid/window/ScreenCapture$DisplayCaptureArgs$Builder; */
/* .line 310 */
(( android.window.ScreenCapture$DisplayCaptureArgs$Builder ) v7 ).setSize ( v1, v2 ); // invoke-virtual {v7, v1, v2}, Landroid/window/ScreenCapture$DisplayCaptureArgs$Builder;->setSize(II)Landroid/window/ScreenCapture$DisplayCaptureArgs$Builder;
/* .line 311 */
(( android.window.ScreenCapture$DisplayCaptureArgs$Builder ) v7 ).build ( ); // invoke-virtual {v7}, Landroid/window/ScreenCapture$DisplayCaptureArgs$Builder;->build()Landroid/window/ScreenCapture$DisplayCaptureArgs;
/* .line 312 */
/* .local v7, "captureArgs":Landroid/window/ScreenCapture$DisplayCaptureArgs; */
/* nop */
/* .line 313 */
android.window.ScreenCapture .captureDisplay ( v7 );
/* .line 315 */
/* if-nez v4, :cond_1 */
int v8 = 0; // const/4 v8, 0x0
} // :cond_1
(( android.window.ScreenCapture$ScreenshotHardwareBuffer ) v4 ).asBitmap ( ); // invoke-virtual {v4}, Landroid/window/ScreenCapture$ScreenshotHardwareBuffer;->asBitmap()Landroid/graphics/Bitmap;
} // :goto_0
/* move-object v0, v8 */
/* .line 318 */
} // .end local v4 # "screenshotBuffer":Landroid/window/ScreenCapture$ScreenshotHardwareBuffer;
} // .end local v5 # "physicalAddress":Landroid/view/DisplayAddress$Physical;
} // .end local v6 # "displayToken":Landroid/os/IBinder;
} // .end local v7 # "captureArgs":Landroid/window/ScreenCapture$DisplayCaptureArgs;
} // :goto_1
} // .end method
private void rebootWifi ( ) {
/* .locals 4 */
/* .line 397 */
v0 = this.mWifiManager;
v0 = (( android.net.wifi.WifiManager ) v0 ).isWifiEnabled ( ); // invoke-virtual {v0}, Landroid/net/wifi/WifiManager;->isWifiEnabled()Z
if ( v0 != null) { // if-eqz v0, :cond_0
/* .line 398 */
v0 = this.mWifiManager;
int v1 = 0; // const/4 v1, 0x0
(( android.net.wifi.WifiManager ) v0 ).setWifiEnabled ( v1 ); // invoke-virtual {v0, v1}, Landroid/net/wifi/WifiManager;->setWifiEnabled(Z)Z
/* .line 399 */
v0 = this.mServiceHandler;
/* new-instance v1, Lcom/miui/server/enterprise/DeviceManagerService$2; */
/* invoke-direct {v1, p0}, Lcom/miui/server/enterprise/DeviceManagerService$2;-><init>(Lcom/miui/server/enterprise/DeviceManagerService;)V */
/* const-wide/16 v2, 0x3e8 */
(( com.miui.server.enterprise.DeviceManagerService$ServiceHandler ) v0 ).postDelayed ( v1, v2, v3 ); // invoke-virtual {v0, v1, v2, v3}, Lcom/miui/server/enterprise/DeviceManagerService$ServiceHandler;->postDelayed(Ljava/lang/Runnable;J)Z
/* .line 406 */
} // :cond_0
return;
} // .end method
/* # virtual methods */
public android.graphics.Bitmap captureScreen ( ) {
/* .locals 4 */
/* .line 265 */
v0 = this.mContext;
com.miui.server.enterprise.ServiceUtils .checkPermission ( v0 );
/* .line 266 */
v0 = this.mContext;
/* invoke-direct {p0, v0}, Lcom/miui/server/enterprise/DeviceManagerService;->getFullScreenshot(Landroid/content/Context;)Landroid/graphics/Bitmap; */
/* .line 267 */
/* .local v0, "bitmap":Landroid/graphics/Bitmap; */
/* if-nez v0, :cond_0 */
/* .line 268 */
/* .line 271 */
} // :cond_0
/* move-object v1, v0 */
/* .line 272 */
/* .local v1, "original":Landroid/graphics/Bitmap; */
v2 = android.graphics.Bitmap$Config.ARGB_8888;
int v3 = 0; // const/4 v3, 0x0
(( android.graphics.Bitmap ) v1 ).copy ( v2, v3 ); // invoke-virtual {v1, v2, v3}, Landroid/graphics/Bitmap;->copy(Landroid/graphics/Bitmap$Config;Z)Landroid/graphics/Bitmap;
/* .line 273 */
(( android.graphics.Bitmap ) v1 ).recycle ( ); // invoke-virtual {v1}, Landroid/graphics/Bitmap;->recycle()V
/* .line 275 */
(( android.graphics.Bitmap ) v0 ).setHasAlpha ( v3 ); // invoke-virtual {v0, v3}, Landroid/graphics/Bitmap;->setHasAlpha(Z)V
/* .line 276 */
(( android.graphics.Bitmap ) v0 ).prepareToDraw ( ); // invoke-virtual {v0}, Landroid/graphics/Bitmap;->prepareToDraw()V
/* .line 277 */
} // .end method
public void deviceReboot ( ) {
/* .locals 4 */
/* .line 100 */
v0 = this.mContext;
com.miui.server.enterprise.ServiceUtils .checkPermission ( v0 );
/* .line 101 */
/* nop */
/* .line 102 */
final String v0 = "power"; // const-string v0, "power"
android.os.ServiceManager .getService ( v0 );
/* .line 101 */
android.os.IPowerManager$Stub .asInterface ( v0 );
/* .line 105 */
/* .local v0, "pm":Landroid/os/IPowerManager; */
int v1 = 0; // const/4 v1, 0x0
int v2 = 1; // const/4 v2, 0x1
int v3 = 0; // const/4 v3, 0x0
try { // :try_start_0
/* :try_end_0 */
/* .catch Landroid/os/RemoteException; {:try_start_0 ..:try_end_0} :catch_0 */
/* .line 108 */
/* .line 106 */
/* :catch_0 */
/* move-exception v1 */
/* .line 109 */
} // :goto_0
return;
} // .end method
public void deviceShutDown ( ) {
/* .locals 3 */
/* .line 87 */
v0 = this.mContext;
com.miui.server.enterprise.ServiceUtils .checkPermission ( v0 );
/* .line 88 */
/* nop */
/* .line 89 */
final String v0 = "power"; // const-string v0, "power"
android.os.ServiceManager .getService ( v0 );
/* .line 88 */
android.os.IPowerManager$Stub .asInterface ( v0 );
/* .line 92 */
/* .local v0, "pm":Landroid/os/IPowerManager; */
int v1 = 0; // const/4 v1, 0x0
int v2 = 0; // const/4 v2, 0x0
try { // :try_start_0
/* :try_end_0 */
/* .catch Landroid/os/RemoteException; {:try_start_0 ..:try_end_0} :catch_0 */
/* .line 95 */
/* .line 93 */
/* :catch_0 */
/* move-exception v1 */
/* .line 96 */
} // :goto_0
return;
} // .end method
public void enableUsbDebug ( Boolean p0 ) {
/* .locals 2 */
/* .param p1, "enable" # Z */
/* .line 357 */
v0 = this.mContext;
com.miui.server.enterprise.ServiceUtils .checkPermission ( v0 );
/* .line 358 */
v0 = this.mContext;
(( android.content.Context ) v0 ).getContentResolver ( ); // invoke-virtual {v0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;
final String v1 = "adb_enabled"; // const-string v1, "adb_enabled"
android.provider.Settings$Global .putInt ( v0,v1,p1 );
/* .line 359 */
return;
} // .end method
public void formatSdCard ( ) {
/* .locals 6 */
/* .line 113 */
v0 = this.mContext;
com.miui.server.enterprise.ServiceUtils .checkPermission ( v0 );
/* .line 114 */
v0 = this.mContext;
/* const-class v1, Landroid/os/storage/StorageManager; */
(( android.content.Context ) v0 ).getSystemService ( v1 ); // invoke-virtual {v0, v1}, Landroid/content/Context;->getSystemService(Ljava/lang/Class;)Ljava/lang/Object;
/* check-cast v0, Landroid/os/storage/StorageManager; */
/* .line 115 */
/* .local v0, "storageManager":Landroid/os/storage/StorageManager; */
int v1 = 0; // const/4 v1, 0x0
/* .line 116 */
/* .local v1, "usbVol":Landroid/os/storage/VolumeInfo; */
(( android.os.storage.StorageManager ) v0 ).getVolumes ( ); // invoke-virtual {v0}, Landroid/os/storage/StorageManager;->getVolumes()Ljava/util/List;
v3 = } // :goto_0
if ( v3 != null) { // if-eqz v3, :cond_1
/* check-cast v3, Landroid/os/storage/VolumeInfo; */
/* .line 117 */
/* .local v3, "vol":Landroid/os/storage/VolumeInfo; */
v4 = (( android.os.storage.VolumeInfo ) v3 ).getType ( ); // invoke-virtual {v3}, Landroid/os/storage/VolumeInfo;->getType()I
/* if-nez v4, :cond_0 */
/* .line 118 */
(( android.os.storage.VolumeInfo ) v3 ).getDisk ( ); // invoke-virtual {v3}, Landroid/os/storage/VolumeInfo;->getDisk()Landroid/os/storage/DiskInfo;
/* iget v4, v4, Landroid/os/storage/DiskInfo;->flags:I */
int v5 = 4; // const/4 v5, 0x4
/* and-int/2addr v4, v5 */
/* if-ne v4, v5, :cond_0 */
/* .line 119 */
/* move-object v1, v3 */
/* .line 120 */
/* .line 122 */
} // .end local v3 # "vol":Landroid/os/storage/VolumeInfo;
} // :cond_0
/* .line 123 */
} // :cond_1
} // :goto_1
if ( v1 != null) { // if-eqz v1, :cond_2
/* .line 124 */
v2 = (( android.os.storage.VolumeInfo ) v1 ).getState ( ); // invoke-virtual {v1}, Landroid/os/storage/VolumeInfo;->getState()I
int v3 = 2; // const/4 v3, 0x2
/* if-ne v2, v3, :cond_2 */
/* .line 125 */
(( android.os.storage.VolumeInfo ) v1 ).getId ( ); // invoke-virtual {v1}, Landroid/os/storage/VolumeInfo;->getId()Ljava/lang/String;
/* .line 126 */
/* .local v2, "volId":Ljava/lang/String; */
/* new-instance v3, Ljava/lang/Thread; */
/* new-instance v4, Lcom/miui/server/enterprise/DeviceManagerService$1; */
/* invoke-direct {v4, p0, v0, v2}, Lcom/miui/server/enterprise/DeviceManagerService$1;-><init>(Lcom/miui/server/enterprise/DeviceManagerService;Landroid/os/storage/StorageManager;Ljava/lang/String;)V */
/* invoke-direct {v3, v4}, Ljava/lang/Thread;-><init>(Ljava/lang/Runnable;)V */
/* .line 131 */
(( java.lang.Thread ) v3 ).start ( ); // invoke-virtual {v3}, Ljava/lang/Thread;->start()V
/* .line 134 */
} // .end local v2 # "volId":Ljava/lang/String;
} // :cond_2
return;
} // .end method
public java.lang.String getDefaultHome ( ) {
/* .locals 2 */
/* .line 450 */
v0 = this.mContext;
final String v1 = "ep_default_home"; // const-string v1, "ep_default_home"
com.miui.enterprise.settings.EnterpriseSettings .getString ( v0,v1 );
} // .end method
public java.util.List getIpBlackList ( Integer p0 ) {
/* .locals 2 */
/* .param p1, "userId" # I */
/* .annotation system Ldalvik/annotation/Signature; */
/* value = { */
/* "(I)", */
/* "Ljava/util/List<", */
/* "Ljava/lang/String;", */
/* ">;" */
/* } */
} // .end annotation
/* .line 350 */
v0 = this.mContext;
com.miui.server.enterprise.ServiceUtils .checkPermission ( v0 );
/* .line 351 */
v0 = this.mContext;
final String v1 = "ep_ip_black_list"; // const-string v1, "ep_ip_black_list"
com.miui.enterprise.settings.EnterpriseSettings .getString ( v0,v1,p1 );
com.miui.enterprise.settings.EnterpriseSettings .parseListSettings ( v0 );
} // .end method
public java.util.List getIpWhiteList ( Integer p0 ) {
/* .locals 2 */
/* .param p1, "userId" # I */
/* .annotation system Ldalvik/annotation/Signature; */
/* value = { */
/* "(I)", */
/* "Ljava/util/List<", */
/* "Ljava/lang/String;", */
/* ">;" */
/* } */
} // .end annotation
/* .line 343 */
v0 = this.mContext;
com.miui.server.enterprise.ServiceUtils .checkPermission ( v0 );
/* .line 344 */
v0 = this.mContext;
final String v1 = "ep_ip_white_list"; // const-string v1, "ep_ip_white_list"
com.miui.enterprise.settings.EnterpriseSettings .getString ( v0,v1,p1 );
com.miui.enterprise.settings.EnterpriseSettings .parseListSettings ( v0 );
} // .end method
public java.util.List getUrlBlackList ( Integer p0 ) {
/* .locals 2 */
/* .param p1, "userId" # I */
/* .annotation system Ldalvik/annotation/Signature; */
/* value = { */
/* "(I)", */
/* "Ljava/util/List<", */
/* "Ljava/lang/String;", */
/* ">;" */
/* } */
} // .end annotation
/* .line 159 */
v0 = this.mContext;
com.miui.server.enterprise.ServiceUtils .checkPermission ( v0 );
/* .line 160 */
v0 = this.mContext;
final String v1 = "ep_url_black_list"; // const-string v1, "ep_url_black_list"
com.miui.enterprise.settings.EnterpriseSettings .getString ( v0,v1,p1 );
com.miui.enterprise.settings.EnterpriseSettings .parseListSettings ( v0 );
} // .end method
public java.util.List getUrlWhiteList ( Integer p0 ) {
/* .locals 2 */
/* .param p1, "userId" # I */
/* .annotation system Ldalvik/annotation/Signature; */
/* value = { */
/* "(I)", */
/* "Ljava/util/List<", */
/* "Ljava/lang/String;", */
/* ">;" */
/* } */
} // .end annotation
/* .line 152 */
v0 = this.mContext;
com.miui.server.enterprise.ServiceUtils .checkPermission ( v0 );
/* .line 153 */
v0 = this.mContext;
final String v1 = "ep_url_white_list"; // const-string v1, "ep_url_white_list"
com.miui.enterprise.settings.EnterpriseSettings .getString ( v0,v1,p1 );
com.miui.enterprise.settings.EnterpriseSettings .parseListSettings ( v0 );
} // .end method
public java.util.List getWifiApBssidBlackList ( Integer p0 ) {
/* .locals 2 */
/* .param p1, "userId" # I */
/* .annotation system Ldalvik/annotation/Signature; */
/* value = { */
/* "(I)", */
/* "Ljava/util/List<", */
/* "Ljava/lang/String;", */
/* ">;" */
/* } */
} // .end annotation
/* .line 246 */
v0 = this.mContext;
com.miui.server.enterprise.ServiceUtils .checkPermission ( v0 );
/* .line 247 */
v0 = this.mContext;
final String v1 = "ep_wifi_ap_bssid_black_list"; // const-string v1, "ep_wifi_ap_bssid_black_list"
com.miui.enterprise.settings.EnterpriseSettings .getString ( v0,v1,p1 );
com.miui.enterprise.settings.EnterpriseSettings .parseListSettings ( v0 );
} // .end method
public java.util.List getWifiApBssidWhiteList ( Integer p0 ) {
/* .locals 2 */
/* .param p1, "userId" # I */
/* .annotation system Ldalvik/annotation/Signature; */
/* value = { */
/* "(I)", */
/* "Ljava/util/List<", */
/* "Ljava/lang/String;", */
/* ">;" */
/* } */
} // .end annotation
/* .line 232 */
v0 = this.mContext;
com.miui.server.enterprise.ServiceUtils .checkPermission ( v0 );
/* .line 233 */
v0 = this.mContext;
final String v1 = "ep_wifi_ap_bssid_white_list"; // const-string v1, "ep_wifi_ap_bssid_white_list"
com.miui.enterprise.settings.EnterpriseSettings .getString ( v0,v1,p1 );
com.miui.enterprise.settings.EnterpriseSettings .parseListSettings ( v0 );
} // .end method
public java.util.List getWifiApSsidBlackList ( Integer p0 ) {
/* .locals 2 */
/* .param p1, "userId" # I */
/* .annotation system Ldalvik/annotation/Signature; */
/* value = { */
/* "(I)", */
/* "Ljava/util/List<", */
/* "Ljava/lang/String;", */
/* ">;" */
/* } */
} // .end annotation
/* .line 239 */
v0 = this.mContext;
com.miui.server.enterprise.ServiceUtils .checkPermission ( v0 );
/* .line 240 */
v0 = this.mContext;
final String v1 = "ep_wifi_ap_ssid_black_list"; // const-string v1, "ep_wifi_ap_ssid_black_list"
com.miui.enterprise.settings.EnterpriseSettings .getString ( v0,v1,p1 );
com.miui.enterprise.settings.EnterpriseSettings .parseListSettings ( v0 );
} // .end method
public java.util.List getWifiApSsidWhiteList ( Integer p0 ) {
/* .locals 2 */
/* .param p1, "userId" # I */
/* .annotation system Ldalvik/annotation/Signature; */
/* value = { */
/* "(I)", */
/* "Ljava/util/List<", */
/* "Ljava/lang/String;", */
/* ">;" */
/* } */
} // .end annotation
/* .line 225 */
v0 = this.mContext;
com.miui.server.enterprise.ServiceUtils .checkPermission ( v0 );
/* .line 226 */
v0 = this.mContext;
final String v1 = "ep_wifi_ap_ssid_white_list"; // const-string v1, "ep_wifi_ap_ssid_white_list"
com.miui.enterprise.settings.EnterpriseSettings .getString ( v0,v1,p1 );
com.miui.enterprise.settings.EnterpriseSettings .parseListSettings ( v0 );
} // .end method
public Integer getWifiConnRestriction ( Integer p0 ) {
/* .locals 3 */
/* .param p1, "userId" # I */
/* .line 186 */
v0 = this.mContext;
com.miui.server.enterprise.ServiceUtils .checkPermission ( v0 );
/* .line 187 */
v0 = this.mContext;
final String v1 = "ep_wifi_conn_restriction_mode"; // const-string v1, "ep_wifi_conn_restriction_mode"
int v2 = 0; // const/4 v2, 0x0
v0 = com.miui.enterprise.settings.EnterpriseSettings .getInt ( v0,v1,v2 );
} // .end method
public Boolean isDeviceRoot ( ) {
/* .locals 2 */
/* .line 81 */
v0 = this.mContext;
com.miui.server.enterprise.ServiceUtils .checkPermission ( v0 );
/* .line 82 */
/* new-instance v0, Ljava/io/File; */
final String v1 = "/system/xbin/su"; // const-string v1, "/system/xbin/su"
/* invoke-direct {v0, v1}, Ljava/io/File;-><init>(Ljava/lang/String;)V */
v0 = (( java.io.File ) v0 ).exists ( ); // invoke-virtual {v0}, Ljava/io/File;->exists()Z
/* if-nez v0, :cond_1 */
/* new-instance v0, Ljava/io/File; */
final String v1 = "/system/bin/su"; // const-string v1, "/system/bin/su"
/* invoke-direct {v0, v1}, Ljava/io/File;-><init>(Ljava/lang/String;)V */
v0 = (( java.io.File ) v0 ).exists ( ); // invoke-virtual {v0}, Ljava/io/File;->exists()Z
if ( v0 != null) { // if-eqz v0, :cond_0
} // :cond_0
int v0 = 0; // const/4 v0, 0x0
} // :cond_1
} // :goto_0
int v0 = 1; // const/4 v0, 0x1
} // :goto_1
} // .end method
public void recoveryFactory ( Boolean p0 ) {
/* .locals 2 */
/* .param p1, "formatSdcard" # Z */
/* .line 166 */
v0 = this.mContext;
com.miui.server.enterprise.ServiceUtils .checkPermission ( v0 );
/* .line 167 */
/* new-instance v0, Landroid/content/Intent; */
final String v1 = "android.intent.action.MASTER_CLEAR"; // const-string v1, "android.intent.action.MASTER_CLEAR"
/* invoke-direct {v0, v1}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V */
/* .line 168 */
/* .local v0, "factoryResetIntent":Landroid/content/Intent; */
final String v1 = "format_sdcard"; // const-string v1, "format_sdcard"
(( android.content.Intent ) v0 ).putExtra ( v1, p1 ); // invoke-virtual {v0, v1, p1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;
/* .line 169 */
/* nop */
/* .line 170 */
final String v1 = "android.intent.action.FACTORY_RESET"; // const-string v1, "android.intent.action.FACTORY_RESET"
(( android.content.Intent ) v0 ).setAction ( v1 ); // invoke-virtual {v0, v1}, Landroid/content/Intent;->setAction(Ljava/lang/String;)Landroid/content/Intent;
/* .line 171 */
final String v1 = "android"; // const-string v1, "android"
(( android.content.Intent ) v0 ).setPackage ( v1 ); // invoke-virtual {v0, v1}, Landroid/content/Intent;->setPackage(Ljava/lang/String;)Landroid/content/Intent;
/* .line 173 */
/* const/high16 v1, 0x10000000 */
(( android.content.Intent ) v0 ).addFlags ( v1 ); // invoke-virtual {v0, v1}, Landroid/content/Intent;->addFlags(I)Landroid/content/Intent;
/* .line 174 */
v1 = this.mContext;
(( android.content.Context ) v1 ).sendBroadcast ( v0 ); // invoke-virtual {v1, v0}, Landroid/content/Context;->sendBroadcast(Landroid/content/Intent;)V
/* .line 175 */
return;
} // .end method
public Boolean setBootAnimation ( java.lang.String p0 ) {
/* .locals 5 */
/* .param p1, "path" # Ljava/lang/String; */
/* .line 364 */
final String v0 = "Enterprise-device"; // const-string v0, "Enterprise-device"
try { // :try_start_0
v1 = this.mContext;
com.miui.server.enterprise.ServiceUtils .checkPermission ( v1 );
/* :try_end_0 */
/* .catch Ljava/lang/SecurityException; {:try_start_0 ..:try_end_0} :catch_1 */
/* .line 368 */
/* nop */
/* .line 370 */
try { // :try_start_1
/* invoke-direct {p0}, Lcom/miui/server/enterprise/DeviceManagerService;->createEntDir()V */
/* .line 371 */
/* new-instance v1, Landroid/content/Intent; */
/* invoke-direct {v1}, Landroid/content/Intent;-><init>()V */
/* .line 372 */
/* .local v1, "intent":Landroid/content/Intent; */
/* new-instance v2, Landroid/content/ComponentName; */
final String v3 = "com.miui.securitycore"; // const-string v3, "com.miui.securitycore"
final String v4 = "com.miui.enterprise.service.EntInstallService"; // const-string v4, "com.miui.enterprise.service.EntInstallService"
/* invoke-direct {v2, v3, v4}, Landroid/content/ComponentName;-><init>(Ljava/lang/String;Ljava/lang/String;)V */
(( android.content.Intent ) v1 ).setComponent ( v2 ); // invoke-virtual {v1, v2}, Landroid/content/Intent;->setComponent(Landroid/content/ComponentName;)Landroid/content/Intent;
/* .line 374 */
/* new-instance v2, Landroid/os/Bundle; */
/* invoke-direct {v2}, Landroid/os/Bundle;-><init>()V */
/* .line 375 */
/* .local v2, "bundle":Landroid/os/Bundle; */
final String v3 = "apkPath"; // const-string v3, "apkPath"
(( android.os.Bundle ) v2 ).putString ( v3, p1 ); // invoke-virtual {v2, v3, p1}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V
/* .line 376 */
/* const-string/jumbo v3, "type" */
final String v4 = "BootAnimation"; // const-string v4, "BootAnimation"
(( android.os.Bundle ) v2 ).putString ( v3, v4 ); // invoke-virtual {v2, v3, v4}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V
/* .line 377 */
(( android.content.Intent ) v1 ).putExtras ( v2 ); // invoke-virtual {v1, v2}, Landroid/content/Intent;->putExtras(Landroid/os/Bundle;)Landroid/content/Intent;
/* .line 378 */
v3 = this.mContext;
(( android.content.Context ) v3 ).startService ( v1 ); // invoke-virtual {v3, v1}, Landroid/content/Context;->startService(Landroid/content/Intent;)Landroid/content/ComponentName;
/* :try_end_1 */
/* .catch Ljava/lang/Exception; {:try_start_1 ..:try_end_1} :catch_0 */
/* .line 381 */
/* nop */
} // .end local v1 # "intent":Landroid/content/Intent;
} // .end local v2 # "bundle":Landroid/os/Bundle;
/* .line 379 */
/* :catch_0 */
/* move-exception v1 */
/* .line 380 */
/* .local v1, "e":Ljava/lang/Exception; */
/* const-string/jumbo v2, "setBootAnimation" */
android.util.Slog .e ( v0,v2,v1 );
/* .line 382 */
} // .end local v1 # "e":Ljava/lang/Exception;
} // :goto_0
int v0 = 1; // const/4 v0, 0x1
/* .line 365 */
/* :catch_1 */
/* move-exception v1 */
/* .line 366 */
/* .local v1, "e":Ljava/lang/SecurityException; */
/* new-instance v2, Ljava/lang/StringBuilder; */
/* invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V */
final String v3 = "Uid "; // const-string v3, "Uid "
(( java.lang.StringBuilder ) v2 ).append ( v3 ); // invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
v3 = android.os.Binder .getCallingUid ( );
(( java.lang.StringBuilder ) v2 ).append ( v3 ); // invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;
final String v3 = " has no permission to access this API"; // const-string v3, " has no permission to access this API"
(( java.lang.StringBuilder ) v2 ).append ( v3 ); // invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v2 ).toString ( ); // invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
android.util.Slog .e ( v0,v2,v1 );
/* .line 367 */
int v0 = 0; // const/4 v0, 0x0
} // .end method
public void setBrowserRestriction ( Integer p0, Integer p1 ) {
/* .locals 2 */
/* .param p1, "mode" # I */
/* .param p2, "userId" # I */
/* .line 259 */
v0 = this.mContext;
com.miui.server.enterprise.ServiceUtils .checkPermission ( v0 );
/* .line 260 */
v0 = this.mContext;
final String v1 = "ep_host_restriction_mode"; // const-string v1, "ep_host_restriction_mode"
com.miui.enterprise.settings.EnterpriseSettings .putInt ( v0,v1,p1,p2 );
/* .line 261 */
return;
} // .end method
public void setDefaultHome ( java.lang.String p0 ) {
/* .locals 14 */
/* .param p1, "pkgName" # Ljava/lang/String; */
/* .line 410 */
v0 = this.mContext;
com.miui.server.enterprise.ServiceUtils .checkPermission ( v0 );
/* .line 411 */
int v0 = 0; // const/4 v0, 0x0
/* .line 412 */
/* .local v0, "isPkgInstalled":Z */
int v1 = 0; // const/4 v1, 0x0
/* .line 413 */
/* .local v1, "packageInfo":Landroid/content/pm/ResolveInfo; */
v2 = this.mContext;
(( android.content.Context ) v2 ).getPackageManager ( ); // invoke-virtual {v2}, Landroid/content/Context;->getPackageManager()Landroid/content/pm/PackageManager;
/* .line 415 */
/* .local v2, "pm":Landroid/content/pm/PackageManager; */
/* new-instance v3, Landroid/content/Intent; */
final String v4 = "android.intent.action.MAIN"; // const-string v4, "android.intent.action.MAIN"
/* invoke-direct {v3, v4}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V */
/* .line 416 */
/* .local v3, "homeIntent":Landroid/content/Intent; */
final String v5 = "android.intent.category.HOME"; // const-string v5, "android.intent.category.HOME"
(( android.content.Intent ) v3 ).addCategory ( v5 ); // invoke-virtual {v3, v5}, Landroid/content/Intent;->addCategory(Ljava/lang/String;)Landroid/content/Intent;
/* .line 417 */
/* const/high16 v6, 0x20000 */
(( android.content.pm.PackageManager ) v2 ).queryIntentActivities ( v3, v6 ); // invoke-virtual {v2, v3, v6}, Landroid/content/pm/PackageManager;->queryIntentActivities(Landroid/content/Intent;I)Ljava/util/List;
/* .line 418 */
v7 = /* .local v6, "lists":Ljava/util/List;, "Ljava/util/List<Landroid/content/pm/ResolveInfo;>;" */
/* new-array v7, v7, [Landroid/content/ComponentName; */
/* .line 420 */
/* .local v7, "set":[Landroid/content/ComponentName; */
int v8 = 0; // const/4 v8, 0x0
/* .line 421 */
/* .local v8, "bestMatch":I */
int v9 = 0; // const/4 v9, 0x0
/* .local v9, "i":I */
v10 = } // :goto_0
/* if-ge v9, v10, :cond_2 */
/* .line 422 */
/* check-cast v10, Landroid/content/pm/ResolveInfo; */
/* .line 423 */
/* .local v10, "item":Landroid/content/pm/ResolveInfo; */
v11 = this.activityInfo;
v11 = this.packageName;
v11 = (( java.lang.String ) p1 ).equals ( v11 ); // invoke-virtual {p1, v11}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z
if ( v11 != null) { // if-eqz v11, :cond_0
/* .line 424 */
int v0 = 1; // const/4 v0, 0x1
/* .line 425 */
/* move-object v1, v10 */
/* .line 427 */
} // :cond_0
/* new-instance v11, Landroid/content/ComponentName; */
v12 = this.activityInfo;
v12 = this.packageName;
v13 = this.activityInfo;
v13 = this.name;
/* invoke-direct {v11, v12, v13}, Landroid/content/ComponentName;-><init>(Ljava/lang/String;Ljava/lang/String;)V */
/* aput-object v11, v7, v9 */
/* .line 429 */
/* iget v11, v10, Landroid/content/pm/ResolveInfo;->match:I */
/* if-le v11, v8, :cond_1 */
/* iget v8, v10, Landroid/content/pm/ResolveInfo;->match:I */
/* .line 421 */
} // .end local v10 # "item":Landroid/content/pm/ResolveInfo;
} // :cond_1
/* add-int/lit8 v9, v9, 0x1 */
/* .line 432 */
} // .end local v9 # "i":I
} // :cond_2
int v9 = 0; // const/4 v9, 0x0
(( android.content.pm.PackageManager ) v2 ).resolveActivity ( v3, v9 ); // invoke-virtual {v2, v3, v9}, Landroid/content/pm/PackageManager;->resolveActivity(Landroid/content/Intent;I)Landroid/content/pm/ResolveInfo;
/* .line 433 */
/* .local v9, "resolveInfo":Landroid/content/pm/ResolveInfo; */
v10 = this.activityInfo;
v10 = this.packageName;
(( android.content.pm.PackageManager ) v2 ).clearPackagePreferredActivities ( v10 ); // invoke-virtual {v2, v10}, Landroid/content/pm/PackageManager;->clearPackagePreferredActivities(Ljava/lang/String;)V
/* .line 434 */
if ( v0 != null) { // if-eqz v0, :cond_3
if ( v1 != null) { // if-eqz v1, :cond_3
/* .line 435 */
/* new-instance v10, Landroid/content/IntentFilter; */
/* invoke-direct {v10}, Landroid/content/IntentFilter;-><init>()V */
/* .line 436 */
/* .local v10, "intentFilter":Landroid/content/IntentFilter; */
(( android.content.IntentFilter ) v10 ).addAction ( v4 ); // invoke-virtual {v10, v4}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V
/* .line 437 */
(( android.content.IntentFilter ) v10 ).addCategory ( v5 ); // invoke-virtual {v10, v5}, Landroid/content/IntentFilter;->addCategory(Ljava/lang/String;)V
/* .line 438 */
final String v4 = "android.intent.category.DEFAULT"; // const-string v4, "android.intent.category.DEFAULT"
(( android.content.IntentFilter ) v10 ).addCategory ( v4 ); // invoke-virtual {v10, v4}, Landroid/content/IntentFilter;->addCategory(Ljava/lang/String;)V
/* .line 439 */
final String v4 = "android.intent.category.BROWSABLE"; // const-string v4, "android.intent.category.BROWSABLE"
(( android.content.IntentFilter ) v10 ).addCategory ( v4 ); // invoke-virtual {v10, v4}, Landroid/content/IntentFilter;->addCategory(Ljava/lang/String;)V
/* .line 440 */
/* new-instance v4, Landroid/content/ComponentName; */
v5 = this.activityInfo;
v5 = this.packageName;
v11 = this.activityInfo;
v11 = this.name;
/* invoke-direct {v4, v5, v11}, Landroid/content/ComponentName;-><init>(Ljava/lang/String;Ljava/lang/String;)V */
(( android.content.pm.PackageManager ) v2 ).addPreferredActivity ( v10, v8, v7, v4 ); // invoke-virtual {v2, v10, v8, v7, v4}, Landroid/content/pm/PackageManager;->addPreferredActivity(Landroid/content/IntentFilter;I[Landroid/content/ComponentName;Landroid/content/ComponentName;)V
/* .line 442 */
/* new-instance v4, Landroid/content/ComponentName; */
v5 = this.activityInfo;
v5 = this.packageName;
v11 = this.activityInfo;
v11 = this.name;
/* invoke-direct {v4, v5, v11}, Landroid/content/ComponentName;-><init>(Ljava/lang/String;Ljava/lang/String;)V */
(( android.content.pm.PackageManager ) v2 ).replacePreferredActivity ( v10, v8, v7, v4 ); // invoke-virtual {v2, v10, v8, v7, v4}, Landroid/content/pm/PackageManager;->replacePreferredActivity(Landroid/content/IntentFilter;I[Landroid/content/ComponentName;Landroid/content/ComponentName;)V
/* .line 444 */
v4 = this.mContext;
final String v5 = "ep_default_home"; // const-string v5, "ep_default_home"
com.miui.enterprise.settings.EnterpriseSettings .putString ( v4,v5,p1 );
/* .line 446 */
} // .end local v10 # "intentFilter":Landroid/content/IntentFilter;
} // :cond_3
return;
} // .end method
public void setIpBlackList ( java.util.List p0, Integer p1 ) {
/* .locals 3 */
/* .param p2, "userId" # I */
/* .annotation system Ldalvik/annotation/Signature; */
/* value = { */
/* "(", */
/* "Ljava/util/List<", */
/* "Ljava/lang/String;", */
/* ">;I)V" */
/* } */
} // .end annotation
/* .line 336 */
/* .local p1, "list":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;" */
v0 = this.mContext;
com.miui.server.enterprise.ServiceUtils .checkPermission ( v0 );
/* .line 337 */
v0 = this.mContext;
/* .line 338 */
com.miui.enterprise.settings.EnterpriseSettings .generateListSettings ( p1 );
/* .line 337 */
final String v2 = "ep_ip_black_list"; // const-string v2, "ep_ip_black_list"
com.miui.enterprise.settings.EnterpriseSettings .putString ( v0,v2,v1,p2 );
/* .line 339 */
return;
} // .end method
public void setIpRestriction ( Integer p0, Integer p1 ) {
/* .locals 2 */
/* .param p1, "mode" # I */
/* .param p2, "userId" # I */
/* .line 323 */
v0 = this.mContext;
com.miui.server.enterprise.ServiceUtils .checkPermission ( v0 );
/* .line 324 */
v0 = this.mContext;
final String v1 = "ep_ip_restriction_mode"; // const-string v1, "ep_ip_restriction_mode"
com.miui.enterprise.settings.EnterpriseSettings .putInt ( v0,v1,p1,p2 );
/* .line 325 */
return;
} // .end method
public void setIpWhiteList ( java.util.List p0, Integer p1 ) {
/* .locals 3 */
/* .param p2, "userId" # I */
/* .annotation system Ldalvik/annotation/Signature; */
/* value = { */
/* "(", */
/* "Ljava/util/List<", */
/* "Ljava/lang/String;", */
/* ">;I)V" */
/* } */
} // .end annotation
/* .line 329 */
/* .local p1, "list":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;" */
v0 = this.mContext;
com.miui.server.enterprise.ServiceUtils .checkPermission ( v0 );
/* .line 330 */
v0 = this.mContext;
/* .line 331 */
com.miui.enterprise.settings.EnterpriseSettings .generateListSettings ( p1 );
/* .line 330 */
final String v2 = "ep_ip_white_list"; // const-string v2, "ep_ip_white_list"
com.miui.enterprise.settings.EnterpriseSettings .putString ( v0,v2,v1,p2 );
/* .line 332 */
return;
} // .end method
public void setLockWallPaper ( java.lang.String p0 ) {
/* .locals 4 */
/* .param p1, "path" # Ljava/lang/String; */
/* .line 473 */
v0 = this.mContext;
com.miui.server.enterprise.ServiceUtils .checkPermission ( v0 );
/* .line 475 */
try { // :try_start_0
/* new-instance v0, Landroid/content/Intent; */
/* invoke-direct {v0}, Landroid/content/Intent;-><init>()V */
/* .line 476 */
/* .local v0, "intent":Landroid/content/Intent; */
/* new-instance v1, Landroid/content/ComponentName; */
final String v2 = "com.miui.securitycore"; // const-string v2, "com.miui.securitycore"
final String v3 = "com.miui.enterprise.service.EntInstallService"; // const-string v3, "com.miui.enterprise.service.EntInstallService"
/* invoke-direct {v1, v2, v3}, Landroid/content/ComponentName;-><init>(Ljava/lang/String;Ljava/lang/String;)V */
(( android.content.Intent ) v0 ).setComponent ( v1 ); // invoke-virtual {v0, v1}, Landroid/content/Intent;->setComponent(Landroid/content/ComponentName;)Landroid/content/Intent;
/* .line 478 */
/* new-instance v1, Landroid/os/Bundle; */
/* invoke-direct {v1}, Landroid/os/Bundle;-><init>()V */
/* .line 479 */
/* .local v1, "bundle":Landroid/os/Bundle; */
final String v2 = "apkPath"; // const-string v2, "apkPath"
(( android.os.Bundle ) v1 ).putString ( v2, p1 ); // invoke-virtual {v1, v2, p1}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V
/* .line 480 */
/* const-string/jumbo v2, "type" */
final String v3 = "LockWallPaper"; // const-string v3, "LockWallPaper"
(( android.os.Bundle ) v1 ).putString ( v2, v3 ); // invoke-virtual {v1, v2, v3}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V
/* .line 481 */
(( android.content.Intent ) v0 ).putExtras ( v1 ); // invoke-virtual {v0, v1}, Landroid/content/Intent;->putExtras(Landroid/os/Bundle;)Landroid/content/Intent;
/* .line 482 */
v2 = this.mContext;
(( android.content.Context ) v2 ).startService ( v0 ); // invoke-virtual {v2, v0}, Landroid/content/Context;->startService(Landroid/content/Intent;)Landroid/content/ComponentName;
/* :try_end_0 */
/* .catch Ljava/lang/Exception; {:try_start_0 ..:try_end_0} :catch_0 */
/* .line 485 */
/* nop */
} // .end local v0 # "intent":Landroid/content/Intent;
} // .end local v1 # "bundle":Landroid/os/Bundle;
/* .line 483 */
/* :catch_0 */
/* move-exception v0 */
/* .line 484 */
/* .local v0, "e":Ljava/lang/Exception; */
final String v1 = "Enterprise-device"; // const-string v1, "Enterprise-device"
/* const-string/jumbo v2, "setWallPaper" */
android.util.Slog .e ( v1,v2,v0 );
/* .line 486 */
} // .end local v0 # "e":Ljava/lang/Exception;
} // :goto_0
return;
} // .end method
public void setRingerMode ( Integer p0 ) {
/* .locals 2 */
/* .param p1, "ringerMode" # I */
/* .line 253 */
v0 = this.mContext;
com.miui.server.enterprise.ServiceUtils .checkPermission ( v0 );
/* .line 254 */
v0 = this.mContext;
final String v1 = "audio"; // const-string v1, "audio"
(( android.content.Context ) v0 ).getSystemService ( v1 ); // invoke-virtual {v0, v1}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;
/* check-cast v0, Landroid/media/AudioManager; */
(( android.media.AudioManager ) v0 ).setRingerMode ( p1 ); // invoke-virtual {v0, p1}, Landroid/media/AudioManager;->setRingerMode(I)V
/* .line 255 */
return;
} // .end method
public void setUrlBlackList ( java.util.List p0, Integer p1 ) {
/* .locals 3 */
/* .param p2, "userId" # I */
/* .annotation system Ldalvik/annotation/Signature; */
/* value = { */
/* "(", */
/* "Ljava/util/List<", */
/* "Ljava/lang/String;", */
/* ">;I)V" */
/* } */
} // .end annotation
/* .line 145 */
/* .local p1, "urls":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;" */
v0 = this.mContext;
com.miui.server.enterprise.ServiceUtils .checkPermission ( v0 );
/* .line 146 */
v0 = this.mContext;
/* .line 147 */
com.miui.enterprise.settings.EnterpriseSettings .generateListSettings ( p1 );
/* .line 146 */
final String v2 = "ep_url_white_list"; // const-string v2, "ep_url_white_list"
com.miui.enterprise.settings.EnterpriseSettings .putString ( v0,v2,v1,p2 );
/* .line 148 */
return;
} // .end method
public void setUrlWhiteList ( java.util.List p0, Integer p1 ) {
/* .locals 3 */
/* .param p2, "userId" # I */
/* .annotation system Ldalvik/annotation/Signature; */
/* value = { */
/* "(", */
/* "Ljava/util/List<", */
/* "Ljava/lang/String;", */
/* ">;I)V" */
/* } */
} // .end annotation
/* .line 138 */
/* .local p1, "urls":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;" */
v0 = this.mContext;
com.miui.server.enterprise.ServiceUtils .checkPermission ( v0 );
/* .line 139 */
v0 = this.mContext;
/* .line 140 */
com.miui.enterprise.settings.EnterpriseSettings .generateListSettings ( p1 );
/* .line 139 */
final String v2 = "ep_url_white_list"; // const-string v2, "ep_url_white_list"
com.miui.enterprise.settings.EnterpriseSettings .putString ( v0,v2,v1,p2 );
/* .line 141 */
return;
} // .end method
public void setWallPaper ( java.lang.String p0 ) {
/* .locals 4 */
/* .param p1, "path" # Ljava/lang/String; */
/* .line 455 */
v0 = this.mContext;
com.miui.server.enterprise.ServiceUtils .checkPermission ( v0 );
/* .line 457 */
try { // :try_start_0
/* new-instance v0, Landroid/content/Intent; */
/* invoke-direct {v0}, Landroid/content/Intent;-><init>()V */
/* .line 458 */
/* .local v0, "intent":Landroid/content/Intent; */
/* new-instance v1, Landroid/content/ComponentName; */
final String v2 = "com.miui.securitycore"; // const-string v2, "com.miui.securitycore"
final String v3 = "com.miui.enterprise.service.EntInstallService"; // const-string v3, "com.miui.enterprise.service.EntInstallService"
/* invoke-direct {v1, v2, v3}, Landroid/content/ComponentName;-><init>(Ljava/lang/String;Ljava/lang/String;)V */
(( android.content.Intent ) v0 ).setComponent ( v1 ); // invoke-virtual {v0, v1}, Landroid/content/Intent;->setComponent(Landroid/content/ComponentName;)Landroid/content/Intent;
/* .line 460 */
/* new-instance v1, Landroid/os/Bundle; */
/* invoke-direct {v1}, Landroid/os/Bundle;-><init>()V */
/* .line 461 */
/* .local v1, "bundle":Landroid/os/Bundle; */
final String v2 = "apkPath"; // const-string v2, "apkPath"
(( android.os.Bundle ) v1 ).putString ( v2, p1 ); // invoke-virtual {v1, v2, p1}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V
/* .line 462 */
/* const-string/jumbo v2, "type" */
final String v3 = "WallPaper"; // const-string v3, "WallPaper"
(( android.os.Bundle ) v1 ).putString ( v2, v3 ); // invoke-virtual {v1, v2, v3}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V
/* .line 463 */
(( android.content.Intent ) v0 ).putExtras ( v1 ); // invoke-virtual {v0, v1}, Landroid/content/Intent;->putExtras(Landroid/os/Bundle;)Landroid/content/Intent;
/* .line 464 */
v2 = this.mContext;
(( android.content.Context ) v2 ).startService ( v0 ); // invoke-virtual {v2, v0}, Landroid/content/Context;->startService(Landroid/content/Intent;)Landroid/content/ComponentName;
/* :try_end_0 */
/* .catch Ljava/lang/Exception; {:try_start_0 ..:try_end_0} :catch_0 */
/* .line 467 */
/* nop */
} // .end local v0 # "intent":Landroid/content/Intent;
} // .end local v1 # "bundle":Landroid/os/Bundle;
/* .line 465 */
/* :catch_0 */
/* move-exception v0 */
/* .line 466 */
/* .local v0, "e":Ljava/lang/Exception; */
final String v1 = "Enterprise-device"; // const-string v1, "Enterprise-device"
/* const-string/jumbo v2, "setWallPaper" */
android.util.Slog .e ( v1,v2,v0 );
/* .line 469 */
} // .end local v0 # "e":Ljava/lang/Exception;
} // :goto_0
return;
} // .end method
public void setWifiApBssidBlackList ( java.util.List p0, Integer p1 ) {
/* .locals 3 */
/* .param p2, "userId" # I */
/* .annotation system Ldalvik/annotation/Signature; */
/* value = { */
/* "(", */
/* "Ljava/util/List<", */
/* "Ljava/lang/String;", */
/* ">;I)V" */
/* } */
} // .end annotation
/* .line 217 */
/* .local p1, "bssid":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;" */
v0 = this.mContext;
com.miui.server.enterprise.ServiceUtils .checkPermission ( v0 );
/* .line 218 */
v0 = this.mContext;
/* .line 219 */
com.miui.enterprise.settings.EnterpriseSettings .generateListSettings ( p1 );
/* .line 218 */
final String v2 = "ep_wifi_ap_bssid_black_list"; // const-string v2, "ep_wifi_ap_bssid_black_list"
com.miui.enterprise.settings.EnterpriseSettings .putString ( v0,v2,v1,p2 );
/* .line 220 */
/* invoke-direct {p0}, Lcom/miui/server/enterprise/DeviceManagerService;->rebootWifi()V */
/* .line 221 */
return;
} // .end method
public void setWifiApBssidWhiteList ( java.util.List p0, Integer p1 ) {
/* .locals 3 */
/* .param p2, "userId" # I */
/* .annotation system Ldalvik/annotation/Signature; */
/* value = { */
/* "(", */
/* "Ljava/util/List<", */
/* "Ljava/lang/String;", */
/* ">;I)V" */
/* } */
} // .end annotation
/* .line 201 */
/* .local p1, "bssid":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;" */
v0 = this.mContext;
com.miui.server.enterprise.ServiceUtils .checkPermission ( v0 );
/* .line 202 */
v0 = this.mContext;
/* .line 203 */
com.miui.enterprise.settings.EnterpriseSettings .generateListSettings ( p1 );
/* .line 202 */
final String v2 = "ep_wifi_ap_bssid_white_list"; // const-string v2, "ep_wifi_ap_bssid_white_list"
com.miui.enterprise.settings.EnterpriseSettings .putString ( v0,v2,v1,p2 );
/* .line 204 */
/* invoke-direct {p0}, Lcom/miui/server/enterprise/DeviceManagerService;->rebootWifi()V */
/* .line 205 */
return;
} // .end method
public void setWifiApSsidBlackList ( java.util.List p0, Integer p1 ) {
/* .locals 3 */
/* .param p2, "userId" # I */
/* .annotation system Ldalvik/annotation/Signature; */
/* value = { */
/* "(", */
/* "Ljava/util/List<", */
/* "Ljava/lang/String;", */
/* ">;I)V" */
/* } */
} // .end annotation
/* .line 209 */
/* .local p1, "ssids":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;" */
v0 = this.mContext;
com.miui.server.enterprise.ServiceUtils .checkPermission ( v0 );
/* .line 210 */
v0 = this.mContext;
/* .line 211 */
com.miui.enterprise.settings.EnterpriseSettings .generateListSettings ( p1 );
/* .line 210 */
final String v2 = "ep_wifi_ap_ssid_black_list"; // const-string v2, "ep_wifi_ap_ssid_black_list"
com.miui.enterprise.settings.EnterpriseSettings .putString ( v0,v2,v1,p2 );
/* .line 212 */
/* invoke-direct {p0}, Lcom/miui/server/enterprise/DeviceManagerService;->rebootWifi()V */
/* .line 213 */
return;
} // .end method
public void setWifiApSsidWhiteList ( java.util.List p0, Integer p1 ) {
/* .locals 3 */
/* .param p2, "userId" # I */
/* .annotation system Ldalvik/annotation/Signature; */
/* value = { */
/* "(", */
/* "Ljava/util/List<", */
/* "Ljava/lang/String;", */
/* ">;I)V" */
/* } */
} // .end annotation
/* .line 193 */
/* .local p1, "ssids":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;" */
v0 = this.mContext;
com.miui.server.enterprise.ServiceUtils .checkPermission ( v0 );
/* .line 194 */
v0 = this.mContext;
/* .line 195 */
com.miui.enterprise.settings.EnterpriseSettings .generateListSettings ( p1 );
/* .line 194 */
final String v2 = "ep_wifi_ap_ssid_white_list"; // const-string v2, "ep_wifi_ap_ssid_white_list"
com.miui.enterprise.settings.EnterpriseSettings .putString ( v0,v2,v1,p2 );
/* .line 196 */
/* invoke-direct {p0}, Lcom/miui/server/enterprise/DeviceManagerService;->rebootWifi()V */
/* .line 197 */
return;
} // .end method
public void setWifiConnRestriction ( Integer p0, Integer p1 ) {
/* .locals 2 */
/* .param p1, "mode" # I */
/* .param p2, "userId" # I */
/* .line 179 */
v0 = this.mContext;
com.miui.server.enterprise.ServiceUtils .checkPermission ( v0 );
/* .line 180 */
v0 = this.mContext;
final String v1 = "ep_wifi_conn_restriction_mode"; // const-string v1, "ep_wifi_conn_restriction_mode"
com.miui.enterprise.settings.EnterpriseSettings .putInt ( v0,v1,p1,p2 );
/* .line 181 */
/* invoke-direct {p0}, Lcom/miui/server/enterprise/DeviceManagerService;->rebootWifi()V */
/* .line 182 */
return;
} // .end method
