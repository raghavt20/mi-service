.class public final Lcom/miui/server/enterprise/EnterpriseManagerService$Lifecycle;
.super Lcom/android/server/SystemService;
.source "EnterpriseManagerService.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/miui/server/enterprise/EnterpriseManagerService;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "Lifecycle"
.end annotation


# instance fields
.field private final mService:Lcom/miui/server/enterprise/EnterpriseManagerService;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 2
    .param p1, "context"    # Landroid/content/Context;

    .line 38
    invoke-direct {p0, p1}, Lcom/android/server/SystemService;-><init>(Landroid/content/Context;)V

    .line 39
    new-instance v0, Lcom/miui/server/enterprise/EnterpriseManagerService;

    const/4 v1, 0x0

    invoke-direct {v0, p1, v1}, Lcom/miui/server/enterprise/EnterpriseManagerService;-><init>(Landroid/content/Context;Lcom/miui/server/enterprise/EnterpriseManagerService-IA;)V

    iput-object v0, p0, Lcom/miui/server/enterprise/EnterpriseManagerService$Lifecycle;->mService:Lcom/miui/server/enterprise/EnterpriseManagerService;

    .line 40
    return-void
.end method


# virtual methods
.method public onStart()V
    .locals 2

    .line 44
    const-string v0, "EnterpriseManager"

    iget-object v1, p0, Lcom/miui/server/enterprise/EnterpriseManagerService$Lifecycle;->mService:Lcom/miui/server/enterprise/EnterpriseManagerService;

    invoke-virtual {p0, v0, v1}, Lcom/miui/server/enterprise/EnterpriseManagerService$Lifecycle;->publishBinderService(Ljava/lang/String;Landroid/os/IBinder;)V

    .line 45
    return-void
.end method
