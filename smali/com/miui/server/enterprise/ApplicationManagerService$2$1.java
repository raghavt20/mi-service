class com.miui.server.enterprise.ApplicationManagerService$2$1 implements java.lang.Runnable {
	 /* .source "ApplicationManagerService.java" */
	 /* # interfaces */
	 /* # annotations */
	 /* .annotation system Ldalvik/annotation/EnclosingMethod; */
	 /* value = Lcom/miui/server/enterprise/ApplicationManagerService$2;->onPackageInstalled(Ljava/lang/String;ILjava/lang/String;Landroid/os/Bundle;)V */
} // .end annotation
/* .annotation system Ldalvik/annotation/InnerClass; */
/* accessFlags = 0x0 */
/* name = null */
} // .end annotation
/* # instance fields */
final com.miui.server.enterprise.ApplicationManagerService$2 this$1; //synthetic
final java.lang.String val$basePackageName; //synthetic
/* # direct methods */
 com.miui.server.enterprise.ApplicationManagerService$2$1 ( ) {
/* .locals 0 */
/* .param p1, "this$1" # Lcom/miui/server/enterprise/ApplicationManagerService$2; */
/* .annotation system Ldalvik/annotation/Signature; */
/* value = { */
/* "()V" */
/* } */
} // .end annotation
/* .line 119 */
this.this$1 = p1;
this.val$basePackageName = p2;
/* invoke-direct {p0}, Ljava/lang/Object;-><init>()V */
return;
} // .end method
/* # virtual methods */
public void run ( ) {
/* .locals 5 */
/* .line 122 */
int v0 = 0; // const/4 v0, 0x0
/* .line 123 */
/* .local v0, "i":I */
} // :goto_0
int v1 = 5; // const/4 v1, 0x5
final String v2 = "Enterprise-App"; // const-string v2, "Enterprise-App"
/* if-ge v0, v1, :cond_0 */
/* .line 126 */
try { // :try_start_0
v1 = this.this$1;
v1 = this.this$0;
com.miui.server.enterprise.ApplicationManagerService .-$$Nest$fgetmPMS ( v1 );
v3 = this.val$basePackageName;
v4 = this.this$1;
/* iget v4, v4, Lcom/miui/server/enterprise/ApplicationManagerService$2;->val$userId:I */
(( com.android.server.pm.PackageManagerService$IPackageManagerImpl ) v1 ).checkPackageStartable ( v3, v4 ); // invoke-virtual {v1, v3, v4}, Lcom/android/server/pm/PackageManagerService$IPackageManagerImpl;->checkPackageStartable(Ljava/lang/String;I)V
/* :try_end_0 */
/* .catch Ljava/lang/SecurityException; {:try_start_0 ..:try_end_0} :catch_0 */
/* .line 127 */
/* .line 128 */
/* :catch_0 */
/* move-exception v1 */
/* .line 130 */
/* .local v1, "e":Ljava/lang/SecurityException; */
/* new-instance v3, Ljava/lang/StringBuilder; */
/* invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V */
final String v4 = "Package "; // const-string v4, "Package "
(( java.lang.StringBuilder ) v3 ).append ( v4 ); // invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
v4 = this.val$basePackageName;
(( java.lang.StringBuilder ) v3 ).append ( v4 ); // invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
final String v4 = " is still frozen"; // const-string v4, " is still frozen"
(( java.lang.StringBuilder ) v3 ).append ( v4 ); // invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v3 ).toString ( ); // invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
android.util.Slog .d ( v2,v3 );
/* .line 132 */
/* const-wide/16 v2, 0x3e8 */
try { // :try_start_1
java.lang.Thread .sleep ( v2,v3 );
/* :try_end_1 */
/* .catch Ljava/lang/InterruptedException; {:try_start_1 ..:try_end_1} :catch_1 */
/* .line 135 */
/* .line 133 */
/* :catch_1 */
/* move-exception v2 */
/* .line 136 */
} // :goto_1
/* nop */
} // .end local v1 # "e":Ljava/lang/SecurityException;
/* add-int/lit8 v0, v0, 0x1 */
/* .line 137 */
/* .line 140 */
} // :cond_0
} // :goto_2
try { // :try_start_2
v1 = this.this$1;
v1 = this.val$pendingIntent;
(( android.app.PendingIntent ) v1 ).send ( ); // invoke-virtual {v1}, Landroid/app/PendingIntent;->send()V
/* .line 141 */
/* new-instance v1, Ljava/lang/StringBuilder; */
/* invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V */
final String v3 = "Send pending intent: "; // const-string v3, "Send pending intent: "
(( java.lang.StringBuilder ) v1 ).append ( v3 ); // invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
v3 = this.this$1;
v3 = this.val$pendingIntent;
(( java.lang.StringBuilder ) v1 ).append ( v3 ); // invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v1 ).toString ( ); // invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
android.util.Slog .d ( v2,v1 );
/* :try_end_2 */
/* .catch Landroid/app/PendingIntent$CanceledException; {:try_start_2 ..:try_end_2} :catch_2 */
/* .line 144 */
/* .line 142 */
/* :catch_2 */
/* move-exception v1 */
/* .line 143 */
/* .local v1, "e":Landroid/app/PendingIntent$CanceledException; */
final String v3 = "Failed to send pending intent"; // const-string v3, "Failed to send pending intent"
android.util.Slog .e ( v2,v3,v1 );
/* .line 145 */
} // .end local v1 # "e":Landroid/app/PendingIntent$CanceledException;
} // :goto_3
return;
} // .end method
