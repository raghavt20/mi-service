class com.miui.server.enterprise.ApplicationManagerService$2 extends com.miui.enterprise.sdk.IEpInstallPackageObserver$Stub {
	 /* .source "ApplicationManagerService.java" */
	 /* # annotations */
	 /* .annotation system Ldalvik/annotation/EnclosingMethod; */
	 /* value = Lcom/miui/server/enterprise/ApplicationManagerService;->installPackageWithPendingIntent(Ljava/lang/String;Landroid/app/PendingIntent;I)V */
} // .end annotation
/* .annotation system Ldalvik/annotation/InnerClass; */
/* accessFlags = 0x0 */
/* name = null */
} // .end annotation
/* # instance fields */
final com.miui.server.enterprise.ApplicationManagerService this$0; //synthetic
final android.app.PendingIntent val$pendingIntent; //synthetic
final Integer val$userId; //synthetic
/* # direct methods */
 com.miui.server.enterprise.ApplicationManagerService$2 ( ) {
/* .locals 0 */
/* .param p1, "this$0" # Lcom/miui/server/enterprise/ApplicationManagerService; */
/* .line 110 */
this.this$0 = p1;
/* iput p2, p0, Lcom/miui/server/enterprise/ApplicationManagerService$2;->val$userId:I */
this.val$pendingIntent = p3;
/* invoke-direct {p0}, Lcom/miui/enterprise/sdk/IEpInstallPackageObserver$Stub;-><init>()V */
return;
} // .end method
/* # virtual methods */
public void onPackageInstalled ( java.lang.String p0, Integer p1, java.lang.String p2, android.os.Bundle p3 ) {
/* .locals 2 */
/* .param p1, "basePackageName" # Ljava/lang/String; */
/* .param p2, "returnCode" # I */
/* .param p3, "msg" # Ljava/lang/String; */
/* .param p4, "extras" # Landroid/os/Bundle; */
/* .line 114 */
int v0 = 1; // const/4 v0, 0x1
/* if-eq p2, v0, :cond_0 */
/* .line 115 */
/* new-instance v0, Ljava/lang/StringBuilder; */
/* invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V */
final String v1 = "Failed to install package: "; // const-string v1, "Failed to install package: "
(( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v0 ).append ( p1 ); // invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
final String v1 = ", returnCode: "; // const-string v1, ", returnCode: "
(( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v0 ).append ( p2 ); // invoke-virtual {v0, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;
final String v1 = ", msg: "; // const-string v1, ", msg: "
(( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v0 ).append ( p3 ); // invoke-virtual {v0, p3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v0 ).toString ( ); // invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
final String v1 = "Enterprise-App"; // const-string v1, "Enterprise-App"
android.util.Slog .e ( v1,v0 );
/* .line 117 */
return;
/* .line 119 */
} // :cond_0
/* new-instance v0, Ljava/lang/Thread; */
/* new-instance v1, Lcom/miui/server/enterprise/ApplicationManagerService$2$1; */
/* invoke-direct {v1, p0, p1}, Lcom/miui/server/enterprise/ApplicationManagerService$2$1;-><init>(Lcom/miui/server/enterprise/ApplicationManagerService$2;Ljava/lang/String;)V */
/* invoke-direct {v0, v1}, Ljava/lang/Thread;-><init>(Ljava/lang/Runnable;)V */
/* .line 146 */
(( java.lang.Thread ) v0 ).start ( ); // invoke-virtual {v0}, Ljava/lang/Thread;->start()V
/* .line 147 */
return;
} // .end method
