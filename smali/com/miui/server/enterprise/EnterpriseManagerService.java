public class com.miui.server.enterprise.EnterpriseManagerService extends com.miui.enterprise.IEnterpriseManager$Stub {
	 /* .source "EnterpriseManagerService.java" */
	 /* # annotations */
	 /* .annotation system Ldalvik/annotation/MemberClasses; */
	 /* value = { */
	 /* Lcom/miui/server/enterprise/EnterpriseManagerService$Lifecycle; */
	 /* } */
} // .end annotation
/* # static fields */
private static final java.lang.String TAG;
/* # instance fields */
private com.miui.server.enterprise.APNManagerService mAPNManagerService;
private com.miui.server.enterprise.ApplicationManagerService mApplicationManagerService;
private com.miui.enterprise.signature.EnterpriseCer mCert;
private android.content.Context mContext;
private Integer mCurrentUserId;
private com.miui.server.enterprise.DeviceManagerService mDeviceManagerService;
private android.content.BroadcastReceiver mLifecycleReceiver;
private com.miui.server.enterprise.PhoneManagerService mPhoneManagerService;
private com.miui.server.enterprise.RestrictionsManagerService mRestrictionsManagerService;
private java.util.Map mServices;
/* .annotation system Ldalvik/annotation/Signature; */
/* value = { */
/* "Ljava/util/Map<", */
/* "Ljava/lang/String;", */
/* "Landroid/os/IBinder;", */
/* ">;" */
/* } */
} // .end annotation
} // .end field
/* # direct methods */
static void -$$Nest$fputmCert ( com.miui.server.enterprise.EnterpriseManagerService p0, com.miui.enterprise.signature.EnterpriseCer p1 ) { //bridge//synthethic
/* .locals 0 */
this.mCert = p1;
return;
} // .end method
static void -$$Nest$mbootComplete ( com.miui.server.enterprise.EnterpriseManagerService p0 ) { //bridge//synthethic
/* .locals 0 */
/* invoke-direct {p0}, Lcom/miui/server/enterprise/EnterpriseManagerService;->bootComplete()V */
return;
} // .end method
static void -$$Nest$monUserRemoved ( com.miui.server.enterprise.EnterpriseManagerService p0, Integer p1 ) { //bridge//synthethic
/* .locals 0 */
/* invoke-direct {p0, p1}, Lcom/miui/server/enterprise/EnterpriseManagerService;->onUserRemoved(I)V */
return;
} // .end method
static void -$$Nest$monUserStarted ( com.miui.server.enterprise.EnterpriseManagerService p0, Integer p1 ) { //bridge//synthethic
/* .locals 0 */
/* invoke-direct {p0, p1}, Lcom/miui/server/enterprise/EnterpriseManagerService;->onUserStarted(I)V */
return;
} // .end method
static void -$$Nest$monUserSwitched ( com.miui.server.enterprise.EnterpriseManagerService p0, Integer p1 ) { //bridge//synthethic
/* .locals 0 */
/* invoke-direct {p0, p1}, Lcom/miui/server/enterprise/EnterpriseManagerService;->onUserSwitched(I)V */
return;
} // .end method
private com.miui.server.enterprise.EnterpriseManagerService ( ) {
/* .locals 3 */
/* .param p1, "context" # Landroid/content/Context; */
/* .line 85 */
/* invoke-direct {p0}, Lcom/miui/enterprise/IEnterpriseManager$Stub;-><init>()V */
/* .line 28 */
/* new-instance v0, Ljava/util/HashMap; */
/* invoke-direct {v0}, Ljava/util/HashMap;-><init>()V */
this.mServices = v0;
/* .line 48 */
int v0 = 0; // const/4 v0, 0x0
/* iput v0, p0, Lcom/miui/server/enterprise/EnterpriseManagerService;->mCurrentUserId:I */
/* .line 55 */
/* new-instance v0, Lcom/miui/server/enterprise/EnterpriseManagerService$1; */
/* invoke-direct {v0, p0}, Lcom/miui/server/enterprise/EnterpriseManagerService$1;-><init>(Lcom/miui/server/enterprise/EnterpriseManagerService;)V */
this.mLifecycleReceiver = v0;
/* .line 87 */
this.mContext = p1;
/* .line 88 */
/* new-instance v0, Lcom/miui/server/enterprise/APNManagerService; */
/* invoke-direct {v0, p1}, Lcom/miui/server/enterprise/APNManagerService;-><init>(Landroid/content/Context;)V */
this.mAPNManagerService = v0;
/* .line 89 */
/* new-instance v0, Lcom/miui/server/enterprise/ApplicationManagerService; */
/* invoke-direct {v0, p1}, Lcom/miui/server/enterprise/ApplicationManagerService;-><init>(Landroid/content/Context;)V */
this.mApplicationManagerService = v0;
/* .line 90 */
/* new-instance v0, Lcom/miui/server/enterprise/DeviceManagerService; */
/* invoke-direct {v0, p1}, Lcom/miui/server/enterprise/DeviceManagerService;-><init>(Landroid/content/Context;)V */
this.mDeviceManagerService = v0;
/* .line 91 */
/* new-instance v0, Lcom/miui/server/enterprise/PhoneManagerService; */
/* invoke-direct {v0, p1}, Lcom/miui/server/enterprise/PhoneManagerService;-><init>(Landroid/content/Context;)V */
this.mPhoneManagerService = v0;
/* .line 92 */
/* new-instance v0, Lcom/miui/server/enterprise/RestrictionsManagerService; */
/* invoke-direct {v0, p1}, Lcom/miui/server/enterprise/RestrictionsManagerService;-><init>(Landroid/content/Context;)V */
this.mRestrictionsManagerService = v0;
/* .line 93 */
v0 = this.mServices;
final String v1 = "apn_manager"; // const-string v1, "apn_manager"
v2 = this.mAPNManagerService;
/* .line 94 */
v0 = this.mServices;
final String v1 = "application_manager"; // const-string v1, "application_manager"
v2 = this.mApplicationManagerService;
/* .line 95 */
v0 = this.mServices;
final String v1 = "device_manager"; // const-string v1, "device_manager"
v2 = this.mDeviceManagerService;
/* .line 96 */
v0 = this.mServices;
final String v1 = "phone_manager"; // const-string v1, "phone_manager"
v2 = this.mPhoneManagerService;
/* .line 97 */
v0 = this.mServices;
final String v1 = "restrictions_manager"; // const-string v1, "restrictions_manager"
v2 = this.mRestrictionsManagerService;
/* .line 98 */
/* new-instance v0, Landroid/content/IntentFilter; */
/* invoke-direct {v0}, Landroid/content/IntentFilter;-><init>()V */
/* .line 99 */
/* .local v0, "intentFilter":Landroid/content/IntentFilter; */
final String v1 = "android.intent.action.BOOT_COMPLETED"; // const-string v1, "android.intent.action.BOOT_COMPLETED"
(( android.content.IntentFilter ) v0 ).addAction ( v1 ); // invoke-virtual {v0, v1}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V
/* .line 100 */
final String v1 = "android.intent.action.USER_STARTED"; // const-string v1, "android.intent.action.USER_STARTED"
(( android.content.IntentFilter ) v0 ).addAction ( v1 ); // invoke-virtual {v0, v1}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V
/* .line 101 */
final String v1 = "android.intent.action.USER_SWITCHED"; // const-string v1, "android.intent.action.USER_SWITCHED"
(( android.content.IntentFilter ) v0 ).addAction ( v1 ); // invoke-virtual {v0, v1}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V
/* .line 102 */
final String v1 = "android.intent.action.USER_REMOVED"; // const-string v1, "android.intent.action.USER_REMOVED"
(( android.content.IntentFilter ) v0 ).addAction ( v1 ); // invoke-virtual {v0, v1}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V
/* .line 103 */
final String v1 = "com.miui.enterprise.ACTION_CERT_UPDATE"; // const-string v1, "com.miui.enterprise.ACTION_CERT_UPDATE"
(( android.content.IntentFilter ) v0 ).addAction ( v1 ); // invoke-virtual {v0, v1}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V
/* .line 104 */
v1 = this.mLifecycleReceiver;
(( android.content.Context ) p1 ).registerReceiver ( v1, v0 ); // invoke-virtual {p1, v1, v0}, Landroid/content/Context;->registerReceiver(Landroid/content/BroadcastReceiver;Landroid/content/IntentFilter;)Landroid/content/Intent;
/* .line 105 */
/* invoke-direct {p0}, Lcom/miui/server/enterprise/EnterpriseManagerService;->loadEnterpriseCert()V */
/* .line 106 */
return;
} // .end method
 com.miui.server.enterprise.EnterpriseManagerService ( ) { //synthethic
/* .locals 0 */
/* invoke-direct {p0, p1}, Lcom/miui/server/enterprise/EnterpriseManagerService;-><init>(Landroid/content/Context;)V */
return;
} // .end method
private void bootComplete ( ) {
/* .locals 1 */
/* .line 109 */
v0 = this.mApplicationManagerService;
(( com.miui.server.enterprise.ApplicationManagerService ) v0 ).bootComplete ( ); // invoke-virtual {v0}, Lcom/miui/server/enterprise/ApplicationManagerService;->bootComplete()V
/* .line 110 */
v0 = this.mRestrictionsManagerService;
(( com.miui.server.enterprise.RestrictionsManagerService ) v0 ).bootComplete ( ); // invoke-virtual {v0}, Lcom/miui/server/enterprise/RestrictionsManagerService;->bootComplete()V
/* .line 111 */
return;
} // .end method
private void checkEnterprisePermission ( ) {
/* .locals 0 */
/* .line 154 */
return;
} // .end method
private void loadEnterpriseCert ( ) {
/* .locals 4 */
/* .line 126 */
/* new-instance v0, Ljava/io/File; */
final String v1 = "/data/system/entcert"; // const-string v1, "/data/system/entcert"
/* invoke-direct {v0, v1}, Ljava/io/File;-><init>(Ljava/lang/String;)V */
/* .line 127 */
/* .local v0, "certFile":Ljava/io/File; */
v1 = (( java.io.File ) v0 ).exists ( ); // invoke-virtual {v0}, Ljava/io/File;->exists()Z
/* if-nez v1, :cond_0 */
/* .line 128 */
return;
/* .line 130 */
} // :cond_0
try { // :try_start_0
/* new-instance v1, Ljava/io/FileInputStream; */
/* invoke-direct {v1, v0}, Ljava/io/FileInputStream;-><init>(Ljava/io/File;)V */
/* :try_end_0 */
/* .catch Ljava/io/IOException; {:try_start_0 ..:try_end_0} :catch_0 */
/* .line 131 */
/* .local v1, "inputStream":Ljava/io/InputStream; */
try { // :try_start_1
/* new-instance v2, Lcom/miui/enterprise/signature/EnterpriseCer; */
/* invoke-direct {v2, v1}, Lcom/miui/enterprise/signature/EnterpriseCer;-><init>(Ljava/io/InputStream;)V */
this.mCert = v2;
/* :try_end_1 */
/* .catchall {:try_start_1 ..:try_end_1} :catchall_0 */
/* .line 132 */
try { // :try_start_2
	 (( java.io.InputStream ) v1 ).close ( ); // invoke-virtual {v1}, Ljava/io/InputStream;->close()V
	 /* :try_end_2 */
	 /* .catch Ljava/io/IOException; {:try_start_2 ..:try_end_2} :catch_0 */
	 /* .line 134 */
} // .end local v1 # "inputStream":Ljava/io/InputStream;
/* .line 130 */
/* .restart local v1 # "inputStream":Ljava/io/InputStream; */
/* :catchall_0 */
/* move-exception v2 */
try { // :try_start_3
	 (( java.io.InputStream ) v1 ).close ( ); // invoke-virtual {v1}, Ljava/io/InputStream;->close()V
	 /* :try_end_3 */
	 /* .catchall {:try_start_3 ..:try_end_3} :catchall_1 */
	 /* :catchall_1 */
	 /* move-exception v3 */
	 try { // :try_start_4
		 (( java.lang.Throwable ) v2 ).addSuppressed ( v3 ); // invoke-virtual {v2, v3}, Ljava/lang/Throwable;->addSuppressed(Ljava/lang/Throwable;)V
	 } // .end local v0 # "certFile":Ljava/io/File;
} // .end local p0 # "this":Lcom/miui/server/enterprise/EnterpriseManagerService;
} // :goto_0
/* throw v2 */
/* :try_end_4 */
/* .catch Ljava/io/IOException; {:try_start_4 ..:try_end_4} :catch_0 */
/* .line 132 */
} // .end local v1 # "inputStream":Ljava/io/InputStream;
/* .restart local v0 # "certFile":Ljava/io/File; */
/* .restart local p0 # "this":Lcom/miui/server/enterprise/EnterpriseManagerService; */
/* :catch_0 */
/* move-exception v1 */
/* .line 133 */
/* .local v1, "e":Ljava/io/IOException; */
final String v2 = "Enterprise"; // const-string v2, "Enterprise"
final String v3 = "Something wrong"; // const-string v3, "Something wrong"
android.util.Slog .e ( v2,v3,v1 );
/* .line 135 */
} // .end local v1 # "e":Ljava/io/IOException;
} // :goto_1
return;
} // .end method
private void onUserRemoved ( Integer p0 ) {
/* .locals 0 */
/* .param p1, "userId" # I */
/* .line 123 */
return;
} // .end method
private void onUserStarted ( Integer p0 ) {
/* .locals 0 */
/* .param p1, "userId" # I */
/* .line 115 */
return;
} // .end method
private void onUserSwitched ( Integer p0 ) {
/* .locals 0 */
/* .param p1, "userId" # I */
/* .line 118 */
/* iput p1, p0, Lcom/miui/server/enterprise/EnterpriseManagerService;->mCurrentUserId:I */
/* .line 119 */
return;
} // .end method
/* # virtual methods */
public com.miui.enterprise.signature.EnterpriseCer getEnterpriseCert ( ) {
/* .locals 1 */
/* .line 142 */
v0 = this.mCert;
} // .end method
public android.os.IBinder getService ( java.lang.String p0 ) {
/* .locals 1 */
/* .param p1, "serviceName" # Ljava/lang/String; */
/* .line 147 */
/* invoke-direct {p0}, Lcom/miui/server/enterprise/EnterpriseManagerService;->checkEnterprisePermission()V */
/* .line 148 */
v0 = this.mServices;
/* check-cast v0, Landroid/os/IBinder; */
} // .end method
public Boolean isSignatureVerified ( ) {
/* .locals 1 */
/* .line 138 */
v0 = this.mCert;
if ( v0 != null) { // if-eqz v0, :cond_0
int v0 = 1; // const/4 v0, 0x1
} // :cond_0
int v0 = 0; // const/4 v0, 0x0
} // :goto_0
} // .end method
