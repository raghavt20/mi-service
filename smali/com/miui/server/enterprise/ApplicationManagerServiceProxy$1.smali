.class Lcom/miui/server/enterprise/ApplicationManagerServiceProxy$1;
.super Landroid/os/IMessenger$Stub;
.source "ApplicationManagerServiceProxy.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/miui/server/enterprise/ApplicationManagerServiceProxy;->installPackageAsUser(Landroid/content/Context;Lcom/android/server/pm/PackageManagerService$IPackageManagerImpl;Ljava/lang/String;Lcom/miui/enterprise/sdk/IEpInstallPackageObserver;ILjava/lang/String;I)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic val$observer:Lcom/miui/enterprise/sdk/IEpInstallPackageObserver;


# direct methods
.method constructor <init>(Lcom/miui/enterprise/sdk/IEpInstallPackageObserver;)V
    .locals 0

    .line 26
    iput-object p1, p0, Lcom/miui/server/enterprise/ApplicationManagerServiceProxy$1;->val$observer:Lcom/miui/enterprise/sdk/IEpInstallPackageObserver;

    invoke-direct {p0}, Landroid/os/IMessenger$Stub;-><init>()V

    return-void
.end method


# virtual methods
.method public send(Landroid/os/Message;)V
    .locals 6
    .param p1, "msg"    # Landroid/os/Message;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    .line 30
    :try_start_0
    invoke-virtual {p1}, Landroid/os/Message;->getData()Landroid/os/Bundle;

    move-result-object v0

    .line 31
    .local v0, "retData":Landroid/os/Bundle;
    if-eqz v0, :cond_0

    .line 32
    const-string v1, "pkg"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 33
    .local v1, "packageName":Ljava/lang/String;
    const-string v2, "retCode"

    invoke-virtual {v0, v2}, Landroid/os/Bundle;->getInt(Ljava/lang/String;)I

    move-result v2

    .line 34
    .local v2, "retCode":I
    const-string v3, "AMSProxy"

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "onPackageInstalled = "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, " ,pkg= "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 35
    iget-object v3, p0, Lcom/miui/server/enterprise/ApplicationManagerServiceProxy$1;->val$observer:Lcom/miui/enterprise/sdk/IEpInstallPackageObserver;

    const/4 v4, 0x0

    invoke-interface {v3, v1, v2, v4, v4}, Lcom/miui/enterprise/sdk/IEpInstallPackageObserver;->onPackageInstalled(Ljava/lang/String;ILjava/lang/String;Landroid/os/Bundle;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 39
    .end local v0    # "retData":Landroid/os/Bundle;
    .end local v1    # "packageName":Ljava/lang/String;
    .end local v2    # "retCode":I
    :cond_0
    goto :goto_0

    .line 37
    :catch_0
    move-exception v0

    .line 38
    .local v0, "e":Ljava/lang/Exception;
    invoke-virtual {v0}, Ljava/lang/Exception;->printStackTrace()V

    .line 40
    .end local v0    # "e":Ljava/lang/Exception;
    :goto_0
    return-void
.end method
