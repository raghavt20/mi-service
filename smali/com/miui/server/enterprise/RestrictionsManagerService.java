public class com.miui.server.enterprise.RestrictionsManagerService extends com.miui.enterprise.IRestrictionsManager$Stub {
	 /* .source "RestrictionsManagerService.java" */
	 /* # static fields */
	 private static final java.lang.String TAG;
	 /* # instance fields */
	 private android.app.AppOpsManager mAppOpsManager;
	 private android.content.Context mContext;
	 private android.content.ComponentName mDeviceOwner;
	 private android.app.admin.IDevicePolicyManager mDevicePolicyManager;
	 private android.os.Handler mHandler;
	 private com.android.server.pm.PackageManagerService$IPackageManagerImpl mPMS;
	 private com.android.server.pm.UserManagerService mUserManager;
	 private com.android.server.wm.WindowManagerService mWindowManagerService;
	 /* # direct methods */
	 static android.content.Context -$$Nest$fgetmContext ( com.miui.server.enterprise.RestrictionsManagerService p0 ) { //bridge//synthethic
		 /* .locals 0 */
		 p0 = this.mContext;
	 } // .end method
	 com.miui.server.enterprise.RestrictionsManagerService ( ) {
		 /* .locals 2 */
		 /* .param p1, "context" # Landroid/content/Context; */
		 /* .line 84 */
		 /* invoke-direct {p0}, Lcom/miui/enterprise/IRestrictionsManager$Stub;-><init>()V */
		 /* .line 85 */
		 this.mContext = p1;
		 /* .line 86 */
		 /* const-string/jumbo v0, "window" */
		 android.os.ServiceManager .getService ( v0 );
		 /* check-cast v0, Lcom/android/server/wm/WindowManagerService; */
		 this.mWindowManagerService = v0;
		 /* .line 87 */
		 final String v0 = "package"; // const-string v0, "package"
		 android.os.ServiceManager .getService ( v0 );
		 /* check-cast v0, Lcom/android/server/pm/PackageManagerService$IPackageManagerImpl; */
		 this.mPMS = v0;
		 /* .line 88 */
		 /* const-string/jumbo v0, "user" */
		 android.os.ServiceManager .getService ( v0 );
		 /* check-cast v0, Lcom/android/server/pm/UserManagerService; */
		 this.mUserManager = v0;
		 /* .line 89 */
		 final String v0 = "device_policy"; // const-string v0, "device_policy"
		 android.os.ServiceManager .getService ( v0 );
		 android.app.admin.IDevicePolicyManager$Stub .asInterface ( v0 );
		 this.mDevicePolicyManager = v0;
		 /* .line 91 */
		 int v1 = 1; // const/4 v1, 0x1
		 try { // :try_start_0
			 this.mDeviceOwner = v0;
			 /* :try_end_0 */
			 /* .catch Landroid/os/RemoteException; {:try_start_0 ..:try_end_0} :catch_0 */
			 /* .line 94 */
			 /* .line 92 */
			 /* :catch_0 */
			 /* move-exception v0 */
			 /* .line 95 */
		 } // :goto_0
		 v0 = this.mContext;
		 final String v1 = "appops"; // const-string v1, "appops"
		 (( android.content.Context ) v0 ).getSystemService ( v1 ); // invoke-virtual {v0, v1}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;
		 /* check-cast v0, Landroid/app/AppOpsManager; */
		 this.mAppOpsManager = v0;
		 /* .line 96 */
		 /* new-instance v0, Landroid/os/Handler; */
		 /* invoke-direct {v0}, Landroid/os/Handler;-><init>()V */
		 this.mHandler = v0;
		 /* .line 97 */
		 return;
	 } // .end method
	 private void closeCloudBackup ( Integer p0 ) {
		 /* .locals 9 */
		 /* .param p1, "userId" # I */
		 /* .line 369 */
		 int v0 = 0; // const/4 v0, 0x0
		 /* .line 370 */
		 /* .local v0, "account":Landroid/accounts/Account; */
		 v1 = this.mContext;
		 android.accounts.AccountManager .get ( v1 );
		 /* .line 371 */
		 /* .local v1, "am":Landroid/accounts/AccountManager; */
		 /* new-instance v2, Landroid/os/UserHandle; */
		 /* invoke-direct {v2, p1}, Landroid/os/UserHandle;-><init>(I)V */
		 final String v3 = "com.xiaomi"; // const-string v3, "com.xiaomi"
		 (( android.accounts.AccountManager ) v1 ).getAccountsByTypeAsUser ( v3, v2 ); // invoke-virtual {v1, v3, v2}, Landroid/accounts/AccountManager;->getAccountsByTypeAsUser(Ljava/lang/String;Landroid/os/UserHandle;)[Landroid/accounts/Account;
		 /* .line 372 */
		 /* .local v2, "accounts":[Landroid/accounts/Account; */
		 /* array-length v3, v2 */
		 int v4 = 0; // const/4 v4, 0x0
		 /* if-lez v3, :cond_0 */
		 /* .line 373 */
		 /* aget-object v0, v2, v4 */
		 /* .line 375 */
	 } // :cond_0
	 if ( v0 != null) { // if-eqz v0, :cond_1
		 /* .line 376 */
		 /* new-instance v3, Landroid/content/ContentValues; */
		 /* invoke-direct {v3}, Landroid/content/ContentValues;-><init>()V */
		 /* .line 377 */
		 /* .local v3, "values":Landroid/content/ContentValues; */
		 final String v5 = "account_name"; // const-string v5, "account_name"
		 v6 = this.name;
		 (( android.content.ContentValues ) v3 ).put ( v5, v6 ); // invoke-virtual {v3, v5, v6}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V
		 /* .line 378 */
		 final String v5 = "is_open"; // const-string v5, "is_open"
		 java.lang.Boolean .valueOf ( v4 );
		 (( android.content.ContentValues ) v3 ).put ( v5, v4 ); // invoke-virtual {v3, v5, v4}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Boolean;)V
		 /* .line 379 */
		 final String v4 = "content://com.miui.micloud"; // const-string v4, "content://com.miui.micloud"
		 android.net.Uri .parse ( v4 );
		 /* .line 380 */
		 (( android.net.Uri ) v4 ).buildUpon ( ); // invoke-virtual {v4}, Landroid/net/Uri;->buildUpon()Landroid/net/Uri$Builder;
		 final String v5 = "cloud_backup_info"; // const-string v5, "cloud_backup_info"
		 (( android.net.Uri$Builder ) v4 ).appendPath ( v5 ); // invoke-virtual {v4, v5}, Landroid/net/Uri$Builder;->appendPath(Ljava/lang/String;)Landroid/net/Uri$Builder;
		 (( android.net.Uri$Builder ) v4 ).build ( ); // invoke-virtual {v4}, Landroid/net/Uri$Builder;->build()Landroid/net/Uri;
		 /* .line 381 */
		 /* .local v4, "cloudBackupInfoUri":Landroid/net/Uri; */
		 android.content.ContentProvider .maybeAddUserId ( v4,p1 );
		 /* .line 382 */
		 v5 = this.mContext;
		 (( android.content.Context ) v5 ).getContentResolver ( ); // invoke-virtual {v5}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;
		 int v6 = 0; // const/4 v6, 0x0
		 (( android.content.ContentResolver ) v5 ).update ( v4, v3, v6, v6 ); // invoke-virtual {v5, v4, v3, v6, v6}, Landroid/content/ContentResolver;->update(Landroid/net/Uri;Landroid/content/ContentValues;Ljava/lang/String;[Ljava/lang/String;)I
		 /* .line 383 */
		 /* new-instance v5, Landroid/content/Intent; */
		 /* invoke-direct {v5}, Landroid/content/Intent;-><init>()V */
		 /* .line 384 */
		 /* .local v5, "intent":Landroid/content/Intent; */
		 /* new-instance v6, Landroid/content/ComponentName; */
		 final String v7 = "com.miui.cloudbackup"; // const-string v7, "com.miui.cloudbackup"
		 final String v8 = "com.miui.cloudbackup.service.CloudBackupService"; // const-string v8, "com.miui.cloudbackup.service.CloudBackupService"
		 /* invoke-direct {v6, v7, v8}, Landroid/content/ComponentName;-><init>(Ljava/lang/String;Ljava/lang/String;)V */
		 (( android.content.Intent ) v5 ).setComponent ( v6 ); // invoke-virtual {v5, v6}, Landroid/content/Intent;->setComponent(Landroid/content/ComponentName;)Landroid/content/Intent;
		 /* .line 386 */
		 final String v6 = "close_cloud_back_up"; // const-string v6, "close_cloud_back_up"
		 (( android.content.Intent ) v5 ).setAction ( v6 ); // invoke-virtual {v5, v6}, Landroid/content/Intent;->setAction(Ljava/lang/String;)Landroid/content/Intent;
		 /* .line 387 */
		 v6 = this.mContext;
		 /* new-instance v7, Landroid/os/UserHandle; */
		 /* invoke-direct {v7, p1}, Landroid/os/UserHandle;-><init>(I)V */
		 (( android.content.Context ) v6 ).startServiceAsUser ( v5, v7 ); // invoke-virtual {v6, v5, v7}, Landroid/content/Context;->startServiceAsUser(Landroid/content/Intent;Landroid/os/UserHandle;)Landroid/content/ComponentName;
		 /* .line 389 */
	 } // .end local v3 # "values":Landroid/content/ContentValues;
} // .end local v4 # "cloudBackupInfoUri":Landroid/net/Uri;
} // .end local v5 # "intent":Landroid/content/Intent;
} // :cond_1
return;
} // .end method
private void setUsbFunction ( android.hardware.usb.UsbManager p0, java.lang.String p1 ) {
/* .locals 8 */
/* .param p1, "usbManager" # Landroid/hardware/usb/UsbManager; */
/* .param p2, "function" # Ljava/lang/String; */
/* .line 393 */
/* const-string/jumbo v0, "setCurrentFunction" */
int v1 = 0; // const/4 v1, 0x0
int v2 = 1; // const/4 v2, 0x1
try { // :try_start_0
/* const-class v3, Landroid/hardware/usb/UsbManager; */
/* new-array v4, v2, [Ljava/lang/Class; */
/* const-class v5, Ljava/lang/String; */
/* aput-object v5, v4, v1 */
(( java.lang.Class ) v3 ).getDeclaredMethod ( v0, v4 ); // invoke-virtual {v3, v0, v4}, Ljava/lang/Class;->getDeclaredMethod(Ljava/lang/String;[Ljava/lang/Class;)Ljava/lang/reflect/Method;
/* .line 394 */
/* .local v3, "method":Ljava/lang/reflect/Method; */
(( java.lang.reflect.Method ) v3 ).setAccessible ( v2 ); // invoke-virtual {v3, v2}, Ljava/lang/reflect/Method;->setAccessible(Z)V
/* .line 395 */
/* filled-new-array {p2}, [Ljava/lang/Object; */
(( java.lang.reflect.Method ) v3 ).invoke ( p1, v4 ); // invoke-virtual {v3, p1, v4}, Ljava/lang/reflect/Method;->invoke(Ljava/lang/Object;[Ljava/lang/Object;)Ljava/lang/Object;
/* :try_end_0 */
/* .catch Ljava/lang/Exception; {:try_start_0 ..:try_end_0} :catch_0 */
/* .line 404 */
/* nop */
} // .end local v3 # "method":Ljava/lang/reflect/Method;
/* .line 396 */
/* :catch_0 */
/* move-exception v3 */
/* .line 398 */
/* .local v3, "e":Ljava/lang/Exception; */
try { // :try_start_1
/* const-class v4, Landroid/hardware/usb/UsbManager; */
int v5 = 2; // const/4 v5, 0x2
/* new-array v6, v5, [Ljava/lang/Class; */
/* const-class v7, Ljava/lang/String; */
/* aput-object v7, v6, v1 */
v7 = java.lang.Boolean.TYPE;
/* aput-object v7, v6, v2 */
(( java.lang.Class ) v4 ).getDeclaredMethod ( v0, v6 ); // invoke-virtual {v4, v0, v6}, Ljava/lang/Class;->getDeclaredMethod(Ljava/lang/String;[Ljava/lang/Class;)Ljava/lang/reflect/Method;
/* .line 399 */
/* .local v0, "method":Ljava/lang/reflect/Method; */
(( java.lang.reflect.Method ) v0 ).setAccessible ( v2 ); // invoke-virtual {v0, v2}, Ljava/lang/reflect/Method;->setAccessible(Z)V
/* .line 400 */
/* new-array v4, v5, [Ljava/lang/Object; */
/* aput-object p2, v4, v1 */
java.lang.Boolean .valueOf ( v1 );
/* aput-object v1, v4, v2 */
(( java.lang.reflect.Method ) v0 ).invoke ( p1, v4 ); // invoke-virtual {v0, p1, v4}, Ljava/lang/reflect/Method;->invoke(Ljava/lang/Object;[Ljava/lang/Object;)Ljava/lang/Object;
/* :try_end_1 */
/* .catch Ljava/lang/Exception; {:try_start_1 ..:try_end_1} :catch_1 */
/* .line 403 */
/* nop */
} // .end local v0 # "method":Ljava/lang/reflect/Method;
/* .line 401 */
/* :catch_1 */
/* move-exception v0 */
/* .line 402 */
/* .local v0, "e1":Ljava/lang/Exception; */
final String v1 = "Enterprise-restric"; // const-string v1, "Enterprise-restric"
final String v2 = "Failed to set usb function"; // const-string v2, "Failed to set usb function"
android.util.Slog .d ( v1,v2,v0 );
/* .line 405 */
} // .end local v0 # "e1":Ljava/lang/Exception;
} // .end local v3 # "e":Ljava/lang/Exception;
} // :goto_0
return;
} // .end method
private Boolean shouldClose ( Integer p0 ) {
/* .locals 1 */
/* .param p1, "state" # I */
/* .line 197 */
if ( p1 != null) { // if-eqz p1, :cond_1
int v0 = 3; // const/4 v0, 0x3
/* if-ne p1, v0, :cond_0 */
} // :cond_0
int v0 = 0; // const/4 v0, 0x0
} // :cond_1
} // :goto_0
int v0 = 1; // const/4 v0, 0x1
} // :goto_1
} // .end method
private Boolean shouldOpen ( Integer p0 ) {
/* .locals 1 */
/* .param p1, "state" # I */
/* .line 193 */
int v0 = 2; // const/4 v0, 0x2
/* if-eq p1, v0, :cond_1 */
int v0 = 4; // const/4 v0, 0x4
/* if-ne p1, v0, :cond_0 */
} // :cond_0
int v0 = 0; // const/4 v0, 0x0
} // :cond_1
} // :goto_0
int v0 = 1; // const/4 v0, 0x1
} // :goto_1
} // .end method
private void startWatchLocationRestriction ( ) {
/* .locals 5 */
/* .line 100 */
final String v0 = "location_providers_allowed"; // const-string v0, "location_providers_allowed"
android.provider.Settings$Secure .getUriFor ( v0 );
/* .line 101 */
/* .local v0, "uri":Landroid/net/Uri; */
v1 = this.mContext;
(( android.content.Context ) v1 ).getContentResolver ( ); // invoke-virtual {v1}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;
/* new-instance v2, Lcom/miui/server/enterprise/RestrictionsManagerService$1; */
v3 = this.mHandler;
/* invoke-direct {v2, p0, v3}, Lcom/miui/server/enterprise/RestrictionsManagerService$1;-><init>(Lcom/miui/server/enterprise/RestrictionsManagerService;Landroid/os/Handler;)V */
int v3 = -1; // const/4 v3, -0x1
int v4 = 1; // const/4 v4, 0x1
(( android.content.ContentResolver ) v1 ).registerContentObserver ( v0, v4, v2, v3 ); // invoke-virtual {v1, v0, v4, v2, v3}, Landroid/content/ContentResolver;->registerContentObserver(Landroid/net/Uri;ZLandroid/database/ContentObserver;I)V
/* .line 120 */
return;
} // .end method
private void unmountPublicVolume ( Integer p0 ) {
/* .locals 5 */
/* .param p1, "volFlag" # I */
/* .line 346 */
v0 = this.mContext;
/* const-class v1, Landroid/os/storage/StorageManager; */
(( android.content.Context ) v0 ).getSystemService ( v1 ); // invoke-virtual {v0, v1}, Landroid/content/Context;->getSystemService(Ljava/lang/Class;)Ljava/lang/Object;
/* check-cast v0, Landroid/os/storage/StorageManager; */
/* .line 347 */
/* .local v0, "storageManager":Landroid/os/storage/StorageManager; */
int v1 = 0; // const/4 v1, 0x0
/* .line 348 */
/* .local v1, "usbVol":Landroid/os/storage/VolumeInfo; */
(( android.os.storage.StorageManager ) v0 ).getVolumes ( ); // invoke-virtual {v0}, Landroid/os/storage/StorageManager;->getVolumes()Ljava/util/List;
v3 = } // :goto_0
if ( v3 != null) { // if-eqz v3, :cond_1
/* check-cast v3, Landroid/os/storage/VolumeInfo; */
/* .line 349 */
/* .local v3, "vol":Landroid/os/storage/VolumeInfo; */
v4 = (( android.os.storage.VolumeInfo ) v3 ).getType ( ); // invoke-virtual {v3}, Landroid/os/storage/VolumeInfo;->getType()I
/* if-nez v4, :cond_0 */
/* .line 350 */
(( android.os.storage.VolumeInfo ) v3 ).getDisk ( ); // invoke-virtual {v3}, Landroid/os/storage/VolumeInfo;->getDisk()Landroid/os/storage/DiskInfo;
/* iget v4, v4, Landroid/os/storage/DiskInfo;->flags:I */
/* and-int/2addr v4, p1 */
/* if-ne v4, p1, :cond_0 */
/* .line 351 */
/* move-object v1, v3 */
/* .line 352 */
/* .line 354 */
} // .end local v3 # "vol":Landroid/os/storage/VolumeInfo;
} // :cond_0
/* .line 355 */
} // :cond_1
} // :goto_1
if ( v1 != null) { // if-eqz v1, :cond_2
/* .line 356 */
v2 = (( android.os.storage.VolumeInfo ) v1 ).getState ( ); // invoke-virtual {v1}, Landroid/os/storage/VolumeInfo;->getState()I
int v3 = 2; // const/4 v3, 0x2
/* if-ne v2, v3, :cond_2 */
/* .line 357 */
(( android.os.storage.VolumeInfo ) v1 ).getId ( ); // invoke-virtual {v1}, Landroid/os/storage/VolumeInfo;->getId()Ljava/lang/String;
/* .line 358 */
/* .local v2, "volId":Ljava/lang/String; */
/* new-instance v3, Ljava/lang/Thread; */
/* new-instance v4, Lcom/miui/server/enterprise/RestrictionsManagerService$2; */
/* invoke-direct {v4, p0, v0, v2}, Lcom/miui/server/enterprise/RestrictionsManagerService$2;-><init>(Lcom/miui/server/enterprise/RestrictionsManagerService;Landroid/os/storage/StorageManager;Ljava/lang/String;)V */
/* invoke-direct {v3, v4}, Ljava/lang/Thread;-><init>(Ljava/lang/Runnable;)V */
/* .line 363 */
(( java.lang.Thread ) v3 ).start ( ); // invoke-virtual {v3}, Ljava/lang/Thread;->start()V
/* .line 366 */
} // .end local v2 # "volId":Ljava/lang/String;
} // :cond_2
return;
} // .end method
/* # virtual methods */
void bootComplete ( ) {
/* .locals 11 */
/* .line 64 */
final String v0 = "Enterprise-restric"; // const-string v0, "Enterprise-restric"
final String v1 = "Restriction init"; // const-string v1, "Restriction init"
android.util.Slog .d ( v0,v1 );
/* .line 65 */
v0 = this.mContext;
/* const-string/jumbo v1, "user" */
(( android.content.Context ) v0 ).getSystemService ( v1 ); // invoke-virtual {v0, v1}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;
/* check-cast v0, Landroid/os/UserManager; */
/* .line 66 */
/* .local v0, "userManager":Landroid/os/UserManager; */
(( android.os.UserManager ) v0 ).getUsers ( ); // invoke-virtual {v0}, Landroid/os/UserManager;->getUsers()Ljava/util/List;
/* .line 67 */
/* .local v1, "users":Ljava/util/List;, "Ljava/util/List<Landroid/content/pm/UserInfo;>;" */
v3 = } // :goto_0
if ( v3 != null) { // if-eqz v3, :cond_4
/* check-cast v3, Landroid/content/pm/UserInfo; */
/* .line 68 */
/* .local v3, "user":Landroid/content/pm/UserInfo; */
v4 = this.mContext;
final String v5 = "disallow_screencapture"; // const-string v5, "disallow_screencapture"
/* iget v6, v3, Landroid/content/pm/UserInfo;->id:I */
v4 = com.miui.enterprise.RestrictionsHelper .hasRestriction ( v4,v5,v6 );
if ( v4 != null) { // if-eqz v4, :cond_0
/* .line 69 */
v4 = this.mWindowManagerService;
v5 = this.mContext;
/* iget v6, v3, Landroid/content/pm/UserInfo;->id:I */
int v7 = 1; // const/4 v7, 0x1
com.miui.server.enterprise.RestrictionManagerServiceProxy .setScreenCaptureDisabled ( v4,v5,v6,v7 );
/* .line 71 */
} // :cond_0
v4 = this.mContext;
final String v5 = "disallow_vpn"; // const-string v5, "disallow_vpn"
/* iget v6, v3, Landroid/content/pm/UserInfo;->id:I */
v4 = com.miui.enterprise.RestrictionsHelper .hasRestriction ( v4,v5,v6 );
if ( v4 != null) { // if-eqz v4, :cond_1
/* .line 72 */
v5 = this.mAppOpsManager;
/* const/16 v6, 0x2f */
int v7 = 1; // const/4 v7, 0x1
int v9 = 0; // const/4 v9, 0x0
/* iget v10, v3, Landroid/content/pm/UserInfo;->id:I */
/* move-object v8, p0 */
/* invoke-virtual/range {v5 ..v10}, Landroid/app/AppOpsManager;->setUserRestrictionForUser(IZLandroid/os/IBinder;Landroid/os/PackageTagsList;I)V */
/* .line 74 */
} // :cond_1
v4 = this.mContext;
final String v5 = "disallow_fingerprint"; // const-string v5, "disallow_fingerprint"
/* iget v6, v3, Landroid/content/pm/UserInfo;->id:I */
v4 = com.miui.enterprise.RestrictionsHelper .hasRestriction ( v4,v5,v6 );
if ( v4 != null) { // if-eqz v4, :cond_2
/* .line 75 */
v5 = this.mAppOpsManager;
/* const/16 v6, 0x37 */
int v7 = 1; // const/4 v7, 0x1
int v9 = 0; // const/4 v9, 0x0
/* iget v10, v3, Landroid/content/pm/UserInfo;->id:I */
/* move-object v8, p0 */
/* invoke-virtual/range {v5 ..v10}, Landroid/app/AppOpsManager;->setUserRestrictionForUser(IZLandroid/os/IBinder;Landroid/os/PackageTagsList;I)V */
/* .line 77 */
} // :cond_2
v4 = this.mContext;
final String v5 = "disallow_imeiread"; // const-string v5, "disallow_imeiread"
/* iget v6, v3, Landroid/content/pm/UserInfo;->id:I */
v4 = com.miui.enterprise.RestrictionsHelper .hasRestriction ( v4,v5,v6 );
if ( v4 != null) { // if-eqz v4, :cond_3
/* .line 78 */
v5 = this.mAppOpsManager;
/* const/16 v6, 0x33 */
int v7 = 1; // const/4 v7, 0x1
int v9 = 0; // const/4 v9, 0x0
/* iget v10, v3, Landroid/content/pm/UserInfo;->id:I */
/* move-object v8, p0 */
/* invoke-virtual/range {v5 ..v10}, Landroid/app/AppOpsManager;->setUserRestrictionForUser(IZLandroid/os/IBinder;Landroid/os/PackageTagsList;I)V */
/* .line 80 */
} // .end local v3 # "user":Landroid/content/pm/UserInfo;
} // :cond_3
/* .line 81 */
} // :cond_4
/* invoke-direct {p0}, Lcom/miui/server/enterprise/RestrictionsManagerService;->startWatchLocationRestriction()V */
/* .line 82 */
return;
} // .end method
public Integer getControlStatus ( java.lang.String p0, Integer p1 ) {
/* .locals 2 */
/* .param p1, "key" # Ljava/lang/String; */
/* .param p2, "userId" # I */
/* .line 335 */
v0 = this.mContext;
com.miui.server.enterprise.ServiceUtils .checkPermission ( v0 );
/* .line 336 */
v0 = this.mContext;
int v1 = 1; // const/4 v1, 0x1
v0 = com.miui.enterprise.settings.EnterpriseSettings .getInt ( v0,p1,v1,p2 );
} // .end method
public Boolean hasRestriction ( java.lang.String p0, Integer p1 ) {
/* .locals 3 */
/* .param p1, "key" # Ljava/lang/String; */
/* .param p2, "userId" # I */
/* .line 341 */
v0 = this.mContext;
com.miui.server.enterprise.ServiceUtils .checkPermission ( v0 );
/* .line 342 */
v0 = this.mContext;
int v1 = 0; // const/4 v1, 0x0
v0 = com.miui.enterprise.settings.EnterpriseSettings .getInt ( v0,p1,v1,p2 );
int v2 = 1; // const/4 v2, 0x1
/* if-ne v0, v2, :cond_0 */
/* move v1, v2 */
} // :cond_0
} // .end method
public void setControlStatus ( java.lang.String p0, Integer p1, Integer p2 ) {
/* .locals 7 */
/* .param p1, "key" # Ljava/lang/String; */
/* .param p2, "value" # I */
/* .param p3, "userId" # I */
/* .line 124 */
v0 = this.mContext;
com.miui.server.enterprise.ServiceUtils .checkPermission ( v0 );
/* .line 125 */
v0 = miui.securityspace.CrossUserUtils .getCurrentUserId ( );
/* if-eq v0, p3, :cond_0 */
/* .line 126 */
return;
/* .line 128 */
} // :cond_0
v0 = this.mContext;
int v1 = 1; // const/4 v1, 0x1
v0 = com.miui.enterprise.settings.EnterpriseSettings .getInt ( v0,p1,v1,p3 );
/* .line 129 */
/* .local v0, "originState":I */
int v2 = 4; // const/4 v2, 0x4
int v3 = 2; // const/4 v3, 0x2
int v4 = 3; // const/4 v4, 0x3
if ( v0 != null) { // if-eqz v0, :cond_1
/* if-ne v0, v2, :cond_2 */
} // :cond_1
/* if-eq p2, v3, :cond_a */
/* if-ne p2, v4, :cond_2 */
/* goto/16 :goto_3 */
/* .line 134 */
} // :cond_2
v5 = this.mContext;
com.miui.enterprise.settings.EnterpriseSettings .putInt ( v5,p1,v1,p3 );
/* .line 135 */
v5 = (( java.lang.String ) p1 ).hashCode ( ); // invoke-virtual {p1}, Ljava/lang/String;->hashCode()I
int v6 = 0; // const/4 v6, 0x0
/* sparse-switch v5, :sswitch_data_0 */
} // :cond_3
/* :sswitch_0 */
final String v3 = "airplane_state"; // const-string v3, "airplane_state"
v3 = (( java.lang.String ) p1 ).equals ( v3 ); // invoke-virtual {p1, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z
if ( v3 != null) { // if-eqz v3, :cond_3
/* :sswitch_1 */
final String v2 = "nfc_state"; // const-string v2, "nfc_state"
v2 = (( java.lang.String ) p1 ).equals ( v2 ); // invoke-virtual {p1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z
if ( v2 != null) { // if-eqz v2, :cond_3
/* move v2, v4 */
/* :sswitch_2 */
/* const-string/jumbo v2, "wifi_state" */
v2 = (( java.lang.String ) p1 ).equals ( v2 ); // invoke-virtual {p1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z
if ( v2 != null) { // if-eqz v2, :cond_3
/* move v2, v6 */
/* :sswitch_3 */
final String v2 = "bluetooth_state"; // const-string v2, "bluetooth_state"
v2 = (( java.lang.String ) p1 ).equals ( v2 ); // invoke-virtual {p1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z
if ( v2 != null) { // if-eqz v2, :cond_3
/* move v2, v1 */
/* :sswitch_4 */
final String v2 = "gps_state"; // const-string v2, "gps_state"
v2 = (( java.lang.String ) p1 ).equals ( v2 ); // invoke-virtual {p1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z
if ( v2 != null) { // if-eqz v2, :cond_3
/* move v2, v3 */
} // :goto_0
int v2 = -1; // const/4 v2, -0x1
} // :goto_1
/* packed-switch v2, :pswitch_data_0 */
/* .line 186 */
/* new-instance v1, Ljava/lang/IllegalArgumentException; */
/* new-instance v2, Ljava/lang/StringBuilder; */
/* invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V */
final String v3 = "Unknown restriction item: "; // const-string v3, "Unknown restriction item: "
(( java.lang.StringBuilder ) v2 ).append ( v3 ); // invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v2 ).append ( p1 ); // invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v2 ).toString ( ); // invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
/* invoke-direct {v1, v2}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V */
/* throw v1 */
/* .line 177 */
/* :pswitch_0 */
v2 = this.mContext;
final String v3 = "connectivity"; // const-string v3, "connectivity"
(( android.content.Context ) v2 ).getSystemService ( v3 ); // invoke-virtual {v2, v3}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;
/* check-cast v2, Landroid/net/ConnectivityManager; */
/* .line 178 */
/* .local v2, "connectivityManager":Landroid/net/ConnectivityManager; */
v3 = /* invoke-direct {p0, p2}, Lcom/miui/server/enterprise/RestrictionsManagerService;->shouldClose(I)Z */
if ( v3 != null) { // if-eqz v3, :cond_4
/* .line 179 */
(( android.net.ConnectivityManager ) v2 ).setAirplaneMode ( v6 ); // invoke-virtual {v2, v6}, Landroid/net/ConnectivityManager;->setAirplaneMode(Z)V
/* .line 181 */
} // :cond_4
v3 = /* invoke-direct {p0, p2}, Lcom/miui/server/enterprise/RestrictionsManagerService;->shouldOpen(I)Z */
if ( v3 != null) { // if-eqz v3, :cond_9
/* .line 182 */
(( android.net.ConnectivityManager ) v2 ).setAirplaneMode ( v1 ); // invoke-virtual {v2, v1}, Landroid/net/ConnectivityManager;->setAirplaneMode(Z)V
/* goto/16 :goto_2 */
/* .line 166 */
} // .end local v2 # "connectivityManager":Landroid/net/ConnectivityManager;
/* :pswitch_1 */
v1 = this.mContext;
android.nfc.NfcAdapter .getDefaultAdapter ( v1 );
/* .line 167 */
/* .local v1, "adapter":Landroid/nfc/NfcAdapter; */
if ( v1 != null) { // if-eqz v1, :cond_9
/* .line 168 */
v2 = /* invoke-direct {p0, p2}, Lcom/miui/server/enterprise/RestrictionsManagerService;->shouldClose(I)Z */
if ( v2 != null) { // if-eqz v2, :cond_5
/* .line 169 */
(( android.nfc.NfcAdapter ) v1 ).disable ( ); // invoke-virtual {v1}, Landroid/nfc/NfcAdapter;->disable()Z
/* .line 171 */
} // :cond_5
v2 = /* invoke-direct {p0, p2}, Lcom/miui/server/enterprise/RestrictionsManagerService;->shouldOpen(I)Z */
if ( v2 != null) { // if-eqz v2, :cond_9
/* .line 172 */
(( android.nfc.NfcAdapter ) v1 ).enable ( ); // invoke-virtual {v1}, Landroid/nfc/NfcAdapter;->enable()Z
/* .line 156 */
} // .end local v1 # "adapter":Landroid/nfc/NfcAdapter;
/* :pswitch_2 */
v1 = /* invoke-direct {p0, p2}, Lcom/miui/server/enterprise/RestrictionsManagerService;->shouldClose(I)Z */
final String v2 = "location_mode"; // const-string v2, "location_mode"
if ( v1 != null) { // if-eqz v1, :cond_6
/* .line 157 */
v1 = this.mContext;
(( android.content.Context ) v1 ).getContentResolver ( ); // invoke-virtual {v1}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;
android.provider.Settings$Secure .putIntForUser ( v1,v2,v6,p3 );
/* .line 160 */
} // :cond_6
v1 = /* invoke-direct {p0, p2}, Lcom/miui/server/enterprise/RestrictionsManagerService;->shouldOpen(I)Z */
if ( v1 != null) { // if-eqz v1, :cond_9
/* .line 161 */
v1 = this.mContext;
(( android.content.Context ) v1 ).getContentResolver ( ); // invoke-virtual {v1}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;
android.provider.Settings$Secure .putIntForUser ( v1,v2,v4,p3 );
/* .line 147 */
/* :pswitch_3 */
android.bluetooth.BluetoothAdapter .getDefaultAdapter ( );
/* .line 148 */
/* .local v1, "btAdapter":Landroid/bluetooth/BluetoothAdapter; */
v2 = /* invoke-direct {p0, p2}, Lcom/miui/server/enterprise/RestrictionsManagerService;->shouldClose(I)Z */
if ( v2 != null) { // if-eqz v2, :cond_7
/* .line 149 */
(( android.bluetooth.BluetoothAdapter ) v1 ).disable ( ); // invoke-virtual {v1}, Landroid/bluetooth/BluetoothAdapter;->disable()Z
/* .line 151 */
} // :cond_7
v2 = /* invoke-direct {p0, p2}, Lcom/miui/server/enterprise/RestrictionsManagerService;->shouldOpen(I)Z */
if ( v2 != null) { // if-eqz v2, :cond_9
/* .line 152 */
(( android.bluetooth.BluetoothAdapter ) v1 ).enable ( ); // invoke-virtual {v1}, Landroid/bluetooth/BluetoothAdapter;->enable()Z
/* .line 138 */
} // .end local v1 # "btAdapter":Landroid/bluetooth/BluetoothAdapter;
/* :pswitch_4 */
v2 = this.mContext;
/* const-string/jumbo v3, "wifi" */
(( android.content.Context ) v2 ).getSystemService ( v3 ); // invoke-virtual {v2, v3}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;
/* check-cast v2, Landroid/net/wifi/WifiManager; */
/* .line 139 */
/* .local v2, "manager":Landroid/net/wifi/WifiManager; */
v3 = /* invoke-direct {p0, p2}, Lcom/miui/server/enterprise/RestrictionsManagerService;->shouldClose(I)Z */
if ( v3 != null) { // if-eqz v3, :cond_8
/* .line 140 */
(( android.net.wifi.WifiManager ) v2 ).setWifiEnabled ( v6 ); // invoke-virtual {v2, v6}, Landroid/net/wifi/WifiManager;->setWifiEnabled(Z)Z
/* .line 142 */
} // :cond_8
v3 = /* invoke-direct {p0, p2}, Lcom/miui/server/enterprise/RestrictionsManagerService;->shouldOpen(I)Z */
if ( v3 != null) { // if-eqz v3, :cond_9
/* .line 143 */
(( android.net.wifi.WifiManager ) v2 ).setWifiEnabled ( v1 ); // invoke-virtual {v2, v1}, Landroid/net/wifi/WifiManager;->setWifiEnabled(Z)Z
/* .line 189 */
} // .end local v2 # "manager":Landroid/net/wifi/WifiManager;
} // :cond_9
} // :goto_2
v1 = this.mContext;
com.miui.enterprise.settings.EnterpriseSettings .putInt ( v1,p1,p2,p3 );
/* .line 190 */
return;
/* .line 131 */
} // :cond_a
} // :goto_3
return;
/* :sswitch_data_0 */
/* .sparse-switch */
/* -0x79dfef44 -> :sswitch_4 */
/* -0x601b5040 -> :sswitch_3 */
/* 0x1d0272e7 -> :sswitch_2 */
/* 0x36cfa4fd -> :sswitch_1 */
/* 0x61188164 -> :sswitch_0 */
} // .end sparse-switch
/* :pswitch_data_0 */
/* .packed-switch 0x0 */
/* :pswitch_4 */
/* :pswitch_3 */
/* :pswitch_2 */
/* :pswitch_1 */
/* :pswitch_0 */
} // .end packed-switch
} // .end method
public void setRestriction ( java.lang.String p0, Boolean p1, Integer p2 ) {
/* .locals 8 */
/* .param p1, "key" # Ljava/lang/String; */
/* .param p2, "value" # Z */
/* .param p3, "userId" # I */
/* .line 202 */
v0 = this.mContext;
com.miui.server.enterprise.ServiceUtils .checkPermission ( v0 );
/* .line 203 */
v0 = this.mContext;
com.miui.enterprise.settings.EnterpriseSettings .putInt ( v0,p1,p2,p3 );
/* .line 204 */
v0 = (( java.lang.String ) p1 ).hashCode ( ); // invoke-virtual {p1}, Ljava/lang/String;->hashCode()I
int v1 = 4; // const/4 v1, 0x4
/* const/16 v2, 0x8 */
int v3 = 0; // const/4 v3, 0x0
/* sparse-switch v0, :sswitch_data_0 */
} // :cond_0
/* goto/16 :goto_0 */
/* :sswitch_0 */
final String v0 = "disallow_microphone"; // const-string v0, "disallow_microphone"
v0 = (( java.lang.String ) p1 ).equals ( v0 ); // invoke-virtual {p1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z
if ( v0 != null) { // if-eqz v0, :cond_0
int v0 = 3; // const/4 v0, 0x3
/* goto/16 :goto_1 */
/* :sswitch_1 */
final String v0 = "disable_accelerometer"; // const-string v0, "disable_accelerometer"
v0 = (( java.lang.String ) p1 ).equals ( v0 ); // invoke-virtual {p1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z
if ( v0 != null) { // if-eqz v0, :cond_0
/* const/16 v0, 0xe */
/* goto/16 :goto_1 */
/* :sswitch_2 */
final String v0 = "disallow_mi_account"; // const-string v0, "disallow_mi_account"
v0 = (( java.lang.String ) p1 ).equals ( v0 ); // invoke-virtual {p1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z
if ( v0 != null) { // if-eqz v0, :cond_0
/* const/16 v0, 0x15 */
/* goto/16 :goto_1 */
/* :sswitch_3 */
final String v0 = "disallow_status_bar"; // const-string v0, "disallow_status_bar"
v0 = (( java.lang.String ) p1 ).equals ( v0 ); // invoke-virtual {p1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z
if ( v0 != null) { // if-eqz v0, :cond_0
/* const/16 v0, 0x14 */
/* goto/16 :goto_1 */
/* :sswitch_4 */
final String v0 = "disallow_fingerprint"; // const-string v0, "disallow_fingerprint"
v0 = (( java.lang.String ) p1 ).equals ( v0 ); // invoke-virtual {p1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z
if ( v0 != null) { // if-eqz v0, :cond_0
/* const/16 v0, 0xa */
/* goto/16 :goto_1 */
/* :sswitch_5 */
final String v0 = "disallow_change_language"; // const-string v0, "disallow_change_language"
v0 = (( java.lang.String ) p1 ).equals ( v0 ); // invoke-virtual {p1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z
if ( v0 != null) { // if-eqz v0, :cond_0
/* const/16 v0, 0x12 */
/* goto/16 :goto_1 */
/* :sswitch_6 */
final String v0 = "disallow_screencapture"; // const-string v0, "disallow_screencapture"
v0 = (( java.lang.String ) p1 ).equals ( v0 ); // invoke-virtual {p1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z
if ( v0 != null) { // if-eqz v0, :cond_0
/* move v0, v1 */
/* goto/16 :goto_1 */
/* :sswitch_7 */
final String v0 = "disallow_key_menu"; // const-string v0, "disallow_key_menu"
v0 = (( java.lang.String ) p1 ).equals ( v0 ); // invoke-virtual {p1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z
if ( v0 != null) { // if-eqz v0, :cond_0
/* const/16 v0, 0x18 */
/* goto/16 :goto_1 */
/* :sswitch_8 */
final String v0 = "disallow_key_home"; // const-string v0, "disallow_key_home"
v0 = (( java.lang.String ) p1 ).equals ( v0 ); // invoke-virtual {p1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z
if ( v0 != null) { // if-eqz v0, :cond_0
/* const/16 v0, 0x17 */
/* goto/16 :goto_1 */
/* :sswitch_9 */
final String v0 = "disallow_key_back"; // const-string v0, "disallow_key_back"
v0 = (( java.lang.String ) p1 ).equals ( v0 ); // invoke-virtual {p1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z
if ( v0 != null) { // if-eqz v0, :cond_0
/* const/16 v0, 0x16 */
/* goto/16 :goto_1 */
/* :sswitch_a */
final String v0 = "disallow_safe_mode"; // const-string v0, "disallow_safe_mode"
v0 = (( java.lang.String ) p1 ).equals ( v0 ); // invoke-virtual {p1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z
if ( v0 != null) { // if-eqz v0, :cond_0
/* const/16 v0, 0x11 */
/* goto/16 :goto_1 */
/* :sswitch_b */
final String v0 = "disallow_factoryreset"; // const-string v0, "disallow_factoryreset"
v0 = (( java.lang.String ) p1 ).equals ( v0 ); // invoke-virtual {p1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z
if ( v0 != null) { // if-eqz v0, :cond_0
/* const/16 v0, 0xb */
/* goto/16 :goto_1 */
/* :sswitch_c */
final String v0 = "disable_usb_device"; // const-string v0, "disable_usb_device"
v0 = (( java.lang.String ) p1 ).equals ( v0 ); // invoke-virtual {p1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z
if ( v0 != null) { // if-eqz v0, :cond_0
/* move v0, v2 */
/* goto/16 :goto_1 */
/* :sswitch_d */
final String v0 = "disallow_timeset"; // const-string v0, "disallow_timeset"
v0 = (( java.lang.String ) p1 ).equals ( v0 ); // invoke-virtual {p1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z
if ( v0 != null) { // if-eqz v0, :cond_0
/* const/16 v0, 0xc */
/* goto/16 :goto_1 */
/* :sswitch_e */
final String v0 = "disallow_vpn"; // const-string v0, "disallow_vpn"
v0 = (( java.lang.String ) p1 ).equals ( v0 ); // invoke-virtual {p1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z
if ( v0 != null) { // if-eqz v0, :cond_0
/* move v0, v3 */
/* goto/16 :goto_1 */
/* :sswitch_f */
final String v0 = "disallow_otg"; // const-string v0, "disallow_otg"
v0 = (( java.lang.String ) p1 ).equals ( v0 ); // invoke-virtual {p1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z
if ( v0 != null) { // if-eqz v0, :cond_0
int v0 = 7; // const/4 v0, 0x7
/* goto/16 :goto_1 */
/* :sswitch_10 */
final String v0 = "disallow_mtp"; // const-string v0, "disallow_mtp"
v0 = (( java.lang.String ) p1 ).equals ( v0 ); // invoke-virtual {p1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z
if ( v0 != null) { // if-eqz v0, :cond_0
int v0 = 6; // const/4 v0, 0x6
/* :sswitch_11 */
final String v0 = "disallow_usbdebug"; // const-string v0, "disallow_usbdebug"
v0 = (( java.lang.String ) p1 ).equals ( v0 ); // invoke-virtual {p1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z
if ( v0 != null) { // if-eqz v0, :cond_0
/* const/16 v0, 0x9 */
/* :sswitch_12 */
final String v0 = "disallow_imeiread"; // const-string v0, "disallow_imeiread"
v0 = (( java.lang.String ) p1 ).equals ( v0 ); // invoke-virtual {p1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z
if ( v0 != null) { // if-eqz v0, :cond_0
/* const/16 v0, 0xd */
/* :sswitch_13 */
final String v0 = "disallow_auto_sync"; // const-string v0, "disallow_auto_sync"
v0 = (( java.lang.String ) p1 ).equals ( v0 ); // invoke-virtual {p1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z
if ( v0 != null) { // if-eqz v0, :cond_0
/* const/16 v0, 0x10 */
/* :sswitch_14 */
final String v0 = "disallow_tether"; // const-string v0, "disallow_tether"
v0 = (( java.lang.String ) p1 ).equals ( v0 ); // invoke-virtual {p1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z
if ( v0 != null) { // if-eqz v0, :cond_0
int v0 = 1; // const/4 v0, 0x1
/* :sswitch_15 */
final String v0 = "disallow_sdcard"; // const-string v0, "disallow_sdcard"
v0 = (( java.lang.String ) p1 ).equals ( v0 ); // invoke-virtual {p1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z
if ( v0 != null) { // if-eqz v0, :cond_0
int v0 = 5; // const/4 v0, 0x5
/* :sswitch_16 */
final String v0 = "disallow_landscape_statusbar"; // const-string v0, "disallow_landscape_statusbar"
v0 = (( java.lang.String ) p1 ).equals ( v0 ); // invoke-virtual {p1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z
if ( v0 != null) { // if-eqz v0, :cond_0
/* const/16 v0, 0x19 */
/* :sswitch_17 */
final String v0 = "disallow_system_update"; // const-string v0, "disallow_system_update"
v0 = (( java.lang.String ) p1 ).equals ( v0 ); // invoke-virtual {p1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z
if ( v0 != null) { // if-eqz v0, :cond_0
/* const/16 v0, 0x13 */
/* :sswitch_18 */
final String v0 = "disallow_camera"; // const-string v0, "disallow_camera"
v0 = (( java.lang.String ) p1 ).equals ( v0 ); // invoke-virtual {p1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z
if ( v0 != null) { // if-eqz v0, :cond_0
int v0 = 2; // const/4 v0, 0x2
/* :sswitch_19 */
final String v0 = "disallow_backup"; // const-string v0, "disallow_backup"
v0 = (( java.lang.String ) p1 ).equals ( v0 ); // invoke-virtual {p1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z
if ( v0 != null) { // if-eqz v0, :cond_0
	 /* const/16 v0, 0xf */
} // :goto_0
int v0 = -1; // const/4 v0, -0x1
} // :goto_1
final String v4 = "Enterprise-restric"; // const-string v4, "Enterprise-restric"
/* packed-switch v0, :pswitch_data_0 */
/* .line 329 */
/* new-instance v0, Ljava/lang/IllegalArgumentException; */
/* new-instance v1, Ljava/lang/StringBuilder; */
/* invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V */
final String v2 = "Unknown restriction item: "; // const-string v2, "Unknown restriction item: "
(( java.lang.StringBuilder ) v1 ).append ( v2 ); // invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v1 ).append ( p1 ); // invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v1 ).toString ( ); // invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
/* invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V */
/* throw v0 */
/* .line 327 */
/* :pswitch_0 */
/* goto/16 :goto_3 */
/* .line 325 */
/* :pswitch_1 */
/* goto/16 :goto_3 */
/* .line 323 */
/* :pswitch_2 */
/* goto/16 :goto_3 */
/* .line 321 */
/* :pswitch_3 */
/* goto/16 :goto_3 */
/* .line 319 */
/* :pswitch_4 */
/* goto/16 :goto_3 */
/* .line 311 */
/* :pswitch_5 */
v0 = this.mContext;
/* const-string/jumbo v1, "statusbar" */
(( android.content.Context ) v0 ).getSystemService ( v1 ); // invoke-virtual {v0, v1}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;
/* check-cast v0, Landroid/app/StatusBarManager; */
/* .line 312 */
/* .local v0, "statusBarManager":Landroid/app/StatusBarManager; */
/* if-nez v0, :cond_1 */
/* .line 313 */
/* const-string/jumbo v1, "statusBarManager is null!" */
android.util.Log .e ( v4,v1 );
/* .line 314 */
/* goto/16 :goto_3 */
/* .line 316 */
} // :cond_1
if ( p2 != null) { // if-eqz p2, :cond_2
/* const/high16 v3, 0x10000 */
} // :cond_2
(( android.app.StatusBarManager ) v0 ).disable ( v3 ); // invoke-virtual {v0, v3}, Landroid/app/StatusBarManager;->disable(I)V
/* .line 317 */
/* goto/16 :goto_3 */
/* .line 309 */
} // .end local v0 # "statusBarManager":Landroid/app/StatusBarManager;
/* :pswitch_6 */
/* goto/16 :goto_3 */
/* .line 306 */
/* :pswitch_7 */
v0 = java.util.Locale.CHINA;
com.android.internal.app.LocalePicker .updateLocale ( v0 );
/* .line 307 */
/* goto/16 :goto_3 */
/* .line 303 */
/* :pswitch_8 */
v0 = this.mUserManager;
final String v1 = "no_safe_boot"; // const-string v1, "no_safe_boot"
(( com.android.server.pm.UserManagerService ) v0 ).setUserRestriction ( v1, p2, p3 ); // invoke-virtual {v0, v1, p2, p3}, Lcom/android/server/pm/UserManagerService;->setUserRestriction(Ljava/lang/String;ZI)V
/* .line 304 */
/* goto/16 :goto_3 */
/* .line 297 */
/* :pswitch_9 */
if ( p2 != null) { // if-eqz p2, :cond_a
/* .line 298 */
android.content.ContentResolver .setMasterSyncAutomaticallyAsUser ( v3,p3 );
/* .line 299 */
/* invoke-direct {p0, p3}, Lcom/miui/server/enterprise/RestrictionsManagerService;->closeCloudBackup(I)V */
/* goto/16 :goto_3 */
/* .line 286 */
/* :pswitch_a */
if ( p2 != null) { // if-eqz p2, :cond_3
/* .line 287 */
v1 = this.mPMS;
final String v2 = "com.miui.backup"; // const-string v2, "com.miui.backup"
int v3 = 2; // const/4 v3, 0x2
int v4 = 0; // const/4 v4, 0x0
v0 = this.mContext;
/* .line 289 */
(( android.content.Context ) v0 ).getPackageName ( ); // invoke-virtual {v0}, Landroid/content/Context;->getPackageName()Ljava/lang/String;
/* .line 287 */
/* move v5, p3 */
/* invoke-virtual/range {v1 ..v6}, Lcom/android/server/pm/PackageManagerService$IPackageManagerImpl;->setApplicationEnabledSetting(Ljava/lang/String;IIILjava/lang/String;)V */
/* goto/16 :goto_3 */
/* .line 291 */
} // :cond_3
v1 = this.mPMS;
final String v2 = "com.miui.backup"; // const-string v2, "com.miui.backup"
int v3 = 1; // const/4 v3, 0x1
int v4 = 0; // const/4 v4, 0x0
v0 = this.mContext;
/* .line 293 */
(( android.content.Context ) v0 ).getPackageName ( ); // invoke-virtual {v0}, Landroid/content/Context;->getPackageName()Ljava/lang/String;
/* .line 291 */
/* move v5, p3 */
/* invoke-virtual/range {v1 ..v6}, Lcom/android/server/pm/PackageManagerService$IPackageManagerImpl;->setApplicationEnabledSetting(Ljava/lang/String;IIILjava/lang/String;)V */
/* .line 295 */
/* goto/16 :goto_3 */
/* .line 284 */
/* :pswitch_b */
/* goto/16 :goto_3 */
/* .line 280 */
/* :pswitch_c */
v1 = this.mAppOpsManager;
/* const/16 v2, 0x33 */
int v5 = 0; // const/4 v5, 0x0
/* move v3, p2 */
/* move-object v4, p0 */
/* move v6, p3 */
/* invoke-virtual/range {v1 ..v6}, Landroid/app/AppOpsManager;->setUserRestrictionForUser(IZLandroid/os/IBinder;Landroid/os/PackageTagsList;I)V */
/* .line 281 */
/* goto/16 :goto_3 */
/* .line 273 */
/* :pswitch_d */
v0 = miui.securityspace.CrossUserUtils .getCurrentUserId ( );
/* if-ne v0, p3, :cond_4 */
/* .line 274 */
v0 = this.mContext;
(( android.content.Context ) v0 ).getContentResolver ( ); // invoke-virtual {v0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;
final String v1 = "auto_time"; // const-string v1, "auto_time"
android.provider.Settings$Global .putInt ( v0,v1,p2 );
/* .line 276 */
} // :cond_4
v0 = this.mContext;
(( android.content.Context ) v0 ).getContentResolver ( ); // invoke-virtual {v0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;
/* .line 277 */
/* nop */
/* .line 276 */
/* const-string/jumbo v1, "time_change_disallow" */
android.provider.Settings$Secure .putIntForUser ( v0,v1,p2,p3 );
/* .line 278 */
/* goto/16 :goto_3 */
/* .line 270 */
/* :pswitch_e */
v0 = this.mUserManager;
final String v1 = "no_factory_reset"; // const-string v1, "no_factory_reset"
(( com.android.server.pm.UserManagerService ) v0 ).setUserRestriction ( v1, p2, p3 ); // invoke-virtual {v0, v1, p2, p3}, Lcom/android/server/pm/UserManagerService;->setUserRestriction(Ljava/lang/String;ZI)V
/* .line 271 */
/* goto/16 :goto_3 */
/* .line 267 */
/* :pswitch_f */
v1 = this.mAppOpsManager;
/* const/16 v2, 0x37 */
int v5 = 0; // const/4 v5, 0x0
/* move v3, p2 */
/* move-object v4, p0 */
/* move v6, p3 */
/* invoke-virtual/range {v1 ..v6}, Landroid/app/AppOpsManager;->setUserRestrictionForUser(IZLandroid/os/IBinder;Landroid/os/PackageTagsList;I)V */
/* .line 268 */
/* goto/16 :goto_3 */
/* .line 261 */
/* :pswitch_10 */
v0 = miui.securityspace.CrossUserUtils .getCurrentUserId ( );
/* if-ne v0, p3, :cond_5 */
/* .line 262 */
v0 = this.mContext;
(( android.content.Context ) v0 ).getContentResolver ( ); // invoke-virtual {v0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;
final String v1 = "adb_enabled"; // const-string v1, "adb_enabled"
android.provider.Settings$Global .putInt ( v0,v1,v3 );
/* .line 264 */
} // :cond_5
v0 = this.mUserManager;
final String v1 = "no_debugging_features"; // const-string v1, "no_debugging_features"
(( com.android.server.pm.UserManagerService ) v0 ).setUserRestriction ( v1, p2, p3 ); // invoke-virtual {v0, v1, p2, p3}, Lcom/android/server/pm/UserManagerService;->setUserRestriction(Ljava/lang/String;ZI)V
/* .line 265 */
/* goto/16 :goto_3 */
/* .line 256 */
/* :pswitch_11 */
if ( p2 != null) { // if-eqz p2, :cond_a
v0 = miui.securityspace.CrossUserUtils .getCurrentUserId ( );
/* if-ne v0, p3, :cond_a */
/* .line 257 */
/* invoke-direct {p0, v2}, Lcom/miui/server/enterprise/RestrictionsManagerService;->unmountPublicVolume(I)V */
/* goto/16 :goto_3 */
/* .line 254 */
/* :pswitch_12 */
/* goto/16 :goto_3 */
/* .line 249 */
/* :pswitch_13 */
v0 = this.mContext;
/* const-string/jumbo v1, "usb" */
(( android.content.Context ) v0 ).getSystemService ( v1 ); // invoke-virtual {v0, v1}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;
/* check-cast v0, Landroid/hardware/usb/UsbManager; */
/* .line 250 */
/* .local v0, "usbManager":Landroid/hardware/usb/UsbManager; */
v1 = this.mUserManager;
final String v2 = "no_usb_file_transfer"; // const-string v2, "no_usb_file_transfer"
(( com.android.server.pm.UserManagerService ) v1 ).setUserRestriction ( v2, p2, p3 ); // invoke-virtual {v1, v2, p2, p3}, Lcom/android/server/pm/UserManagerService;->setUserRestriction(Ljava/lang/String;ZI)V
/* .line 251 */
final String v1 = "none"; // const-string v1, "none"
/* invoke-direct {p0, v0, v1}, Lcom/miui/server/enterprise/RestrictionsManagerService;->setUsbFunction(Landroid/hardware/usb/UsbManager;Ljava/lang/String;)V */
/* .line 252 */
/* goto/16 :goto_3 */
/* .line 244 */
} // .end local v0 # "usbManager":Landroid/hardware/usb/UsbManager;
/* :pswitch_14 */
if ( p2 != null) { // if-eqz p2, :cond_a
v0 = miui.securityspace.CrossUserUtils .getCurrentUserId ( );
/* if-ne v0, p3, :cond_a */
/* .line 245 */
/* invoke-direct {p0, v1}, Lcom/miui/server/enterprise/RestrictionsManagerService;->unmountPublicVolume(I)V */
/* goto/16 :goto_3 */
/* .line 241 */
/* :pswitch_15 */
v0 = this.mWindowManagerService;
v1 = this.mContext;
com.miui.server.enterprise.RestrictionManagerServiceProxy .setScreenCaptureDisabled ( v0,v1,p3,p2 );
/* .line 242 */
/* goto/16 :goto_3 */
/* .line 238 */
/* :pswitch_16 */
v0 = this.mUserManager;
final String v1 = "no_record_audio"; // const-string v1, "no_record_audio"
(( com.android.server.pm.UserManagerService ) v0 ).setUserRestriction ( v1, p2, p3 ); // invoke-virtual {v0, v1, p2, p3}, Lcom/android/server/pm/UserManagerService;->setUserRestriction(Ljava/lang/String;ZI)V
/* .line 239 */
/* goto/16 :goto_3 */
/* .line 234 */
/* :pswitch_17 */
v0 = this.mUserManager;
final String v1 = "no_camera"; // const-string v1, "no_camera"
(( com.android.server.pm.UserManagerService ) v0 ).setUserRestriction ( v1, p2, p3 ); // invoke-virtual {v0, v1, p2, p3}, Lcom/android/server/pm/UserManagerService;->setUserRestriction(Ljava/lang/String;ZI)V
/* .line 236 */
/* goto/16 :goto_3 */
/* .line 227 */
/* :pswitch_18 */
v0 = this.mUserManager;
final String v1 = "no_config_tethering"; // const-string v1, "no_config_tethering"
v0 = (( com.android.server.pm.UserManagerService ) v0 ).hasUserRestriction ( v1, p3 ); // invoke-virtual {v0, v1, p3}, Lcom/android/server/pm/UserManagerService;->hasUserRestriction(Ljava/lang/String;I)Z
/* .line 228 */
/* .local v0, "hasUserRestriction":Z */
if ( p2 != null) { // if-eqz p2, :cond_6
v2 = miui.securityspace.CrossUserUtils .getCurrentUserId ( );
/* if-ne p3, v2, :cond_6 */
/* if-nez v0, :cond_6 */
/* .line 229 */
v2 = this.mContext;
com.miui.server.enterprise.RestrictionManagerServiceProxy .setWifiApEnabled ( v2,v3 );
/* .line 231 */
} // :cond_6
v2 = this.mUserManager;
(( com.android.server.pm.UserManagerService ) v2 ).setUserRestriction ( v1, p2, p3 ); // invoke-virtual {v2, v1, p2, p3}, Lcom/android/server/pm/UserManagerService;->setUserRestriction(Ljava/lang/String;ZI)V
/* .line 232 */
/* .line 206 */
} // .end local v0 # "hasUserRestriction":Z
/* :pswitch_19 */
v0 = this.mUserManager;
final String v1 = "no_config_vpn"; // const-string v1, "no_config_vpn"
v7 = (( com.android.server.pm.UserManagerService ) v0 ).hasUserRestriction ( v1, p3 ); // invoke-virtual {v0, v1, p3}, Lcom/android/server/pm/UserManagerService;->hasUserRestriction(Ljava/lang/String;I)Z
/* .line 207 */
/* .local v7, "hasUserRestriction":Z */
if ( p2 != null) { // if-eqz p2, :cond_9
v0 = miui.securityspace.CrossUserUtils .getCurrentUserId ( );
/* if-ne v0, p3, :cond_9 */
/* if-nez v7, :cond_9 */
/* .line 209 */
try { // :try_start_0
/* const-string/jumbo v0, "vpn_management" */
/* .line 210 */
android.os.ServiceManager .getService ( v0 );
/* .line 209 */
android.net.IVpnManager$Stub .asInterface ( v0 );
/* .line 211 */
/* .local v0, "vpnmanager":Landroid/net/IVpnManager; */
v2 = android.os.UserHandle .myUserId ( );
/* .line 212 */
/* .local v2, "mConfig":Lcom/android/internal/net/VpnConfig; */
if ( v2 != null) { // if-eqz v2, :cond_7
/* .line 213 */
v3 = this.configureIntent;
(( android.app.PendingIntent ) v3 ).send ( ); // invoke-virtual {v3}, Landroid/app/PendingIntent;->send()V
/* .line 215 */
} // :cond_7
v3 = android.os.UserHandle .myUserId ( );
/* .line 216 */
/* .local v3, "info":Lcom/android/internal/net/LegacyVpnInfo; */
if ( v3 != null) { // if-eqz v3, :cond_8
/* .line 217 */
v5 = this.intent;
(( android.app.PendingIntent ) v5 ).send ( ); // invoke-virtual {v5}, Landroid/app/PendingIntent;->send()V
/* :try_end_0 */
/* .catch Ljava/lang/Exception; {:try_start_0 ..:try_end_0} :catch_0 */
/* .line 221 */
} // .end local v0 # "vpnmanager":Landroid/net/IVpnManager;
} // .end local v2 # "mConfig":Lcom/android/internal/net/VpnConfig;
} // .end local v3 # "info":Lcom/android/internal/net/LegacyVpnInfo;
} // :cond_8
/* .line 219 */
/* :catch_0 */
/* move-exception v0 */
/* .line 220 */
/* .local v0, "e":Ljava/lang/Exception; */
/* new-instance v2, Ljava/lang/StringBuilder; */
/* invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V */
final String v3 = "Something wrong while close vpn: "; // const-string v3, "Something wrong while close vpn: "
(( java.lang.StringBuilder ) v2 ).append ( v3 ); // invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v2 ).append ( v0 ); // invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v2 ).toString ( ); // invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
android.util.Slog .d ( v4,v2 );
/* .line 223 */
} // .end local v0 # "e":Ljava/lang/Exception;
} // :cond_9
} // :goto_2
v0 = this.mUserManager;
(( com.android.server.pm.UserManagerService ) v0 ).setUserRestriction ( v1, p2, p3 ); // invoke-virtual {v0, v1, p2, p3}, Lcom/android/server/pm/UserManagerService;->setUserRestriction(Ljava/lang/String;ZI)V
/* .line 224 */
v1 = this.mAppOpsManager;
/* const/16 v2, 0x2f */
int v5 = 0; // const/4 v5, 0x0
/* move v3, p2 */
/* move-object v4, p0 */
/* move v6, p3 */
/* invoke-virtual/range {v1 ..v6}, Landroid/app/AppOpsManager;->setUserRestrictionForUser(IZLandroid/os/IBinder;Landroid/os/PackageTagsList;I)V */
/* .line 225 */
/* nop */
/* .line 331 */
} // .end local v7 # "hasUserRestriction":Z
} // :cond_a
} // :goto_3
return;
/* :sswitch_data_0 */
/* .sparse-switch */
/* -0x7227a4fa -> :sswitch_19 */
/* -0x706e5797 -> :sswitch_18 */
/* -0x6edcd8eb -> :sswitch_17 */
/* -0x5c87e867 -> :sswitch_16 */
/* -0x54fb21db -> :sswitch_15 */
/* -0x53305eaa -> :sswitch_14 */
/* -0x4139f099 -> :sswitch_13 */
/* -0x3193455e -> :sswitch_12 */
/* -0x1b0ab86d -> :sswitch_11 */
/* -0x199e971b -> :sswitch_10 */
/* -0x199e8fa2 -> :sswitch_f */
/* -0x199e75d0 -> :sswitch_e */
/* -0xc6be24f -> :sswitch_d */
/* -0xa2e54d8 -> :sswitch_c */
/* 0x4f1e4c9 -> :sswitch_b */
/* 0x1853c751 -> :sswitch_a */
/* 0x1889e70b -> :sswitch_9 */
/* 0x188cd703 -> :sswitch_8 */
/* 0x188ef783 -> :sswitch_7 */
/* 0x1c0d5b96 -> :sswitch_6 */
/* 0x1c9f9e43 -> :sswitch_5 */
/* 0x1d15ae20 -> :sswitch_4 */
/* 0x2b28d58a -> :sswitch_3 */
/* 0x361585ce -> :sswitch_2 */
/* 0x44f96f38 -> :sswitch_1 */
/* 0x6e123c6e -> :sswitch_0 */
} // .end sparse-switch
/* :pswitch_data_0 */
/* .packed-switch 0x0 */
/* :pswitch_19 */
/* :pswitch_18 */
/* :pswitch_17 */
/* :pswitch_16 */
/* :pswitch_15 */
/* :pswitch_14 */
/* :pswitch_13 */
/* :pswitch_12 */
/* :pswitch_11 */
/* :pswitch_10 */
/* :pswitch_f */
/* :pswitch_e */
/* :pswitch_d */
/* :pswitch_c */
/* :pswitch_b */
/* :pswitch_a */
/* :pswitch_9 */
/* :pswitch_8 */
/* :pswitch_7 */
/* :pswitch_6 */
/* :pswitch_5 */
/* :pswitch_4 */
/* :pswitch_3 */
/* :pswitch_2 */
/* :pswitch_1 */
/* :pswitch_0 */
} // .end packed-switch
} // .end method
