public class com.miui.server.enterprise.RestrictionManagerServiceProxy {
	 /* .source "RestrictionManagerServiceProxy.java" */
	 /* # direct methods */
	 public com.miui.server.enterprise.RestrictionManagerServiceProxy ( ) {
		 /* .locals 0 */
		 /* .line 8 */
		 /* invoke-direct {p0}, Ljava/lang/Object;-><init>()V */
		 return;
	 } // .end method
	 static void setScreenCaptureDisabled ( com.android.server.wm.WindowManagerService p0, android.content.Context p1, Integer p2, Boolean p3 ) {
		 /* .locals 0 */
		 /* .param p0, "service" # Lcom/android/server/wm/WindowManagerService; */
		 /* .param p1, "context" # Landroid/content/Context; */
		 /* .param p2, "userId" # I */
		 /* .param p3, "enabled" # Z */
		 /* .line 10 */
		 (( com.android.server.wm.WindowManagerService ) p0 ).refreshScreenCaptureDisabled ( ); // invoke-virtual {p0}, Lcom/android/server/wm/WindowManagerService;->refreshScreenCaptureDisabled()V
		 /* .line 11 */
		 return;
	 } // .end method
	 static void setWifiApEnabled ( android.content.Context p0, Boolean p1 ) {
		 /* .locals 2 */
		 /* .param p0, "context" # Landroid/content/Context; */
		 /* .param p1, "enabled" # Z */
		 /* .line 14 */
		 final String v0 = "connectivity"; // const-string v0, "connectivity"
		 (( android.content.Context ) p0 ).getSystemService ( v0 ); // invoke-virtual {p0, v0}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;
		 /* check-cast v0, Landroid/net/ConnectivityManager; */
		 /* .line 15 */
		 /* .local v0, "connectivityManager":Landroid/net/ConnectivityManager; */
		 int v1 = 0; // const/4 v1, 0x0
		 (( android.net.ConnectivityManager ) v0 ).stopTethering ( v1 ); // invoke-virtual {v0, v1}, Landroid/net/ConnectivityManager;->stopTethering(I)V
		 /* .line 16 */
		 int v1 = 1; // const/4 v1, 0x1
		 (( android.net.ConnectivityManager ) v0 ).stopTethering ( v1 ); // invoke-virtual {v0, v1}, Landroid/net/ConnectivityManager;->stopTethering(I)V
		 /* .line 17 */
		 int v1 = 2; // const/4 v1, 0x2
		 (( android.net.ConnectivityManager ) v0 ).stopTethering ( v1 ); // invoke-virtual {v0, v1}, Landroid/net/ConnectivityManager;->stopTethering(I)V
		 /* .line 18 */
		 return;
	 } // .end method
