class com.miui.server.enterprise.ApplicationManagerServiceProxy$1 extends android.os.IMessenger$Stub {
	 /* .source "ApplicationManagerServiceProxy.java" */
	 /* # annotations */
	 /* .annotation system Ldalvik/annotation/EnclosingMethod; */
	 /* value = Lcom/miui/server/enterprise/ApplicationManagerServiceProxy;->installPackageAsUser(Landroid/content/Context;Lcom/android/server/pm/PackageManagerService$IPackageManagerImpl;Ljava/lang/String;Lcom/miui/enterprise/sdk/IEpInstallPackageObserver;ILjava/lang/String;I)V */
} // .end annotation
/* .annotation system Ldalvik/annotation/InnerClass; */
/* accessFlags = 0x0 */
/* name = null */
} // .end annotation
/* # instance fields */
final com.miui.enterprise.sdk.IEpInstallPackageObserver val$observer; //synthetic
/* # direct methods */
 com.miui.server.enterprise.ApplicationManagerServiceProxy$1 ( ) {
/* .locals 0 */
/* .line 26 */
this.val$observer = p1;
/* invoke-direct {p0}, Landroid/os/IMessenger$Stub;-><init>()V */
return;
} // .end method
/* # virtual methods */
public void send ( android.os.Message p0 ) {
/* .locals 6 */
/* .param p1, "msg" # Landroid/os/Message; */
/* .annotation system Ldalvik/annotation/Throws; */
/* value = { */
/* Landroid/os/RemoteException; */
/* } */
} // .end annotation
/* .line 30 */
try { // :try_start_0
(( android.os.Message ) p1 ).getData ( ); // invoke-virtual {p1}, Landroid/os/Message;->getData()Landroid/os/Bundle;
/* .line 31 */
/* .local v0, "retData":Landroid/os/Bundle; */
if ( v0 != null) { // if-eqz v0, :cond_0
	 /* .line 32 */
	 final String v1 = "pkg"; // const-string v1, "pkg"
	 (( android.os.Bundle ) v0 ).getString ( v1 ); // invoke-virtual {v0, v1}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;
	 /* .line 33 */
	 /* .local v1, "packageName":Ljava/lang/String; */
	 final String v2 = "retCode"; // const-string v2, "retCode"
	 v2 = 	 (( android.os.Bundle ) v0 ).getInt ( v2 ); // invoke-virtual {v0, v2}, Landroid/os/Bundle;->getInt(Ljava/lang/String;)I
	 /* .line 34 */
	 /* .local v2, "retCode":I */
	 final String v3 = "AMSProxy"; // const-string v3, "AMSProxy"
	 /* new-instance v4, Ljava/lang/StringBuilder; */
	 /* invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V */
	 final String v5 = "onPackageInstalled = "; // const-string v5, "onPackageInstalled = "
	 (( java.lang.StringBuilder ) v4 ).append ( v5 ); // invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
	 (( java.lang.StringBuilder ) v4 ).append ( v2 ); // invoke-virtual {v4, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;
	 final String v5 = " ,pkg= "; // const-string v5, " ,pkg= "
	 (( java.lang.StringBuilder ) v4 ).append ( v5 ); // invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
	 (( java.lang.StringBuilder ) v4 ).append ( v1 ); // invoke-virtual {v4, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
	 (( java.lang.StringBuilder ) v4 ).toString ( ); // invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
	 android.util.Log .d ( v3,v4 );
	 /* .line 35 */
	 v3 = this.val$observer;
	 int v4 = 0; // const/4 v4, 0x0
	 /* :try_end_0 */
	 /* .catch Ljava/lang/Exception; {:try_start_0 ..:try_end_0} :catch_0 */
	 /* .line 39 */
} // .end local v0 # "retData":Landroid/os/Bundle;
} // .end local v1 # "packageName":Ljava/lang/String;
} // .end local v2 # "retCode":I
} // :cond_0
/* .line 37 */
/* :catch_0 */
/* move-exception v0 */
/* .line 38 */
/* .local v0, "e":Ljava/lang/Exception; */
(( java.lang.Exception ) v0 ).printStackTrace ( ); // invoke-virtual {v0}, Ljava/lang/Exception;->printStackTrace()V
/* .line 40 */
} // .end local v0 # "e":Ljava/lang/Exception;
} // :goto_0
return;
} // .end method
