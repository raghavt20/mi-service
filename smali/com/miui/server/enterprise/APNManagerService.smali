.class public Lcom/miui/server/enterprise/APNManagerService;
.super Lcom/miui/enterprise/IAPNManager$Stub;
.source "APNManagerService.java"


# static fields
.field private static final DEFAULTAPN_URI:Landroid/net/Uri;

.field private static final PREFERAPN_URI:Landroid/net/Uri;

.field private static final TAG:Ljava/lang/String; = "APNManagerMode"


# instance fields
.field private mContext:Landroid/content/Context;

.field private mTelephonyManager:Landroid/telephony/TelephonyManager;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .line 28
    const-string v0, "content://telephony/carriers/preferapn"

    invoke-static {v0}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v0

    sput-object v0, Lcom/miui/server/enterprise/APNManagerService;->PREFERAPN_URI:Landroid/net/Uri;

    .line 29
    const-string v0, "content://telephony/carriers/restore"

    invoke-static {v0}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v0

    sput-object v0, Lcom/miui/server/enterprise/APNManagerService;->DEFAULTAPN_URI:Landroid/net/Uri;

    return-void
.end method

.method constructor <init>(Landroid/content/Context;)V
    .locals 1
    .param p1, "context"    # Landroid/content/Context;

    .line 32
    invoke-direct {p0}, Lcom/miui/enterprise/IAPNManager$Stub;-><init>()V

    .line 33
    iput-object p1, p0, Lcom/miui/server/enterprise/APNManagerService;->mContext:Landroid/content/Context;

    .line 34
    const-string v0, "phone"

    invoke-virtual {p1, v0}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/telephony/TelephonyManager;

    iput-object v0, p0, Lcom/miui/server/enterprise/APNManagerService;->mTelephonyManager:Landroid/telephony/TelephonyManager;

    .line 35
    return-void
.end method

.method private buildNameSelection(Ljava/lang/String;)Ljava/lang/String;
    .locals 2
    .param p1, "name"    # Ljava/lang/String;

    .line 50
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "name=\""

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "\""

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method private buildNumericAndNameSelection(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;
    .locals 2
    .param p1, "numeric"    # Ljava/lang/String;
    .param p2, "name"    # Ljava/lang/String;

    .line 42
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    invoke-direct {p0, p1}, Lcom/miui/server/enterprise/APNManagerService;->buildNumericSelection(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, " and "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-direct {p0, p2}, Lcom/miui/server/enterprise/APNManagerService;->buildNameSelection(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method private buildNumericSelection(Ljava/lang/String;)Ljava/lang/String;
    .locals 2
    .param p1, "numeric"    # Ljava/lang/String;

    .line 46
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "numeric=\""

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "\""

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method private closeCursor(Landroid/database/Cursor;)V
    .locals 1
    .param p1, "cursor"    # Landroid/database/Cursor;

    .line 64
    if-eqz p1, :cond_0

    invoke-interface {p1}, Landroid/database/Cursor;->isClosed()Z

    move-result v0

    if-nez v0, :cond_0

    .line 65
    invoke-interface {p1}, Landroid/database/Cursor;->close()V

    .line 67
    :cond_0
    return-void
.end method

.method private getContentResolver()Landroid/content/ContentResolver;
    .locals 3

    .line 38
    iget-object v0, p0, Lcom/miui/server/enterprise/APNManagerService;->mContext:Landroid/content/Context;

    new-instance v1, Landroid/os/UserHandle;

    const/4 v2, 0x0

    invoke-direct {v1, v2}, Landroid/os/UserHandle;-><init>(I)V

    invoke-virtual {v0, v1}, Landroid/content/Context;->getContentResolverForUser(Landroid/os/UserHandle;)Landroid/content/ContentResolver;

    move-result-object v0

    return-object v0
.end method

.method private getNumeric()Ljava/lang/String;
    .locals 5

    .line 54
    invoke-static {}, Lmiui/telephony/SubscriptionManager;->getDefault()Lmiui/telephony/SubscriptionManager;

    move-result-object v0

    invoke-virtual {v0}, Lmiui/telephony/SubscriptionManager;->getDefaultDataSubscriptionId()I

    move-result v0

    .line 55
    .local v0, "subId":I
    iget-object v1, p0, Lcom/miui/server/enterprise/APNManagerService;->mContext:Landroid/content/Context;

    invoke-static {v1}, Landroid/telephony/SubscriptionManager;->from(Landroid/content/Context;)Landroid/telephony/SubscriptionManager;

    move-result-object v1

    invoke-virtual {v1, v0}, Landroid/telephony/SubscriptionManager;->getActiveSubscriptionInfo(I)Landroid/telephony/SubscriptionInfo;

    move-result-object v1

    .line 56
    .local v1, "subscriptionInfo":Landroid/telephony/SubscriptionInfo;
    if-nez v1, :cond_0

    const-string v2, ""

    goto :goto_0

    .line 57
    :cond_0
    iget-object v2, p0, Lcom/miui/server/enterprise/APNManagerService;->mTelephonyManager:Landroid/telephony/TelephonyManager;

    invoke-virtual {v1}, Landroid/telephony/SubscriptionInfo;->getSubscriptionId()I

    move-result v3

    invoke-virtual {v2, v3}, Landroid/telephony/TelephonyManager;->getSimOperator(I)Ljava/lang/String;

    move-result-object v2

    :goto_0
    nop

    .line 58
    .local v2, "mccmnc":Ljava/lang/String;
    invoke-static {}, Lmiui/telephony/SubscriptionManager;->getDefault()Lmiui/telephony/SubscriptionManager;

    move-result-object v3

    invoke-virtual {v3}, Lmiui/telephony/SubscriptionManager;->getDefaultDataSlotId()I

    move-result v3

    sget-object v4, Lmiui/telephony/TelephonyConstants;->PROPERTY_APN_SIM_OPERATOR_NUMERIC:Ljava/lang/String;

    invoke-static {v3, v4, v2}, Landroid/telephony/TelephonyManager;->getTelephonyProperty(ILjava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    .line 60
    return-object v2
.end method

.method private getUriForCurrSubId(Landroid/net/Uri;)Landroid/net/Uri;
    .locals 5
    .param p1, "uri"    # Landroid/net/Uri;

    .line 518
    invoke-static {}, Lmiui/telephony/SubscriptionManager;->getDefault()Lmiui/telephony/SubscriptionManager;

    move-result-object v0

    invoke-virtual {v0}, Lmiui/telephony/SubscriptionManager;->getDefaultDataSubscriptionId()I

    move-result v0

    .line 519
    .local v0, "subId":I
    iget-object v1, p0, Lcom/miui/server/enterprise/APNManagerService;->mContext:Landroid/content/Context;

    invoke-static {v1}, Landroid/telephony/SubscriptionManager;->from(Landroid/content/Context;)Landroid/telephony/SubscriptionManager;

    move-result-object v1

    invoke-virtual {v1, v0}, Landroid/telephony/SubscriptionManager;->getActiveSubscriptionInfo(I)Landroid/telephony/SubscriptionInfo;

    move-result-object v1

    .line 520
    .local v1, "subscriptionInfo":Landroid/telephony/SubscriptionInfo;
    if-eqz v1, :cond_0

    invoke-virtual {v1}, Landroid/telephony/SubscriptionInfo;->getSubscriptionId()I

    move-result v2

    goto :goto_0

    .line 521
    :cond_0
    const/4 v2, -0x1

    :goto_0
    nop

    .line 522
    .local v2, "subId0":I
    invoke-static {v2}, Landroid/telephony/SubscriptionManager;->isValidSubscriptionId(I)Z

    move-result v3

    if-eqz v3, :cond_1

    .line 523
    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v4, "subId/"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-static {v2}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {p1, v3}, Landroid/net/Uri;->withAppendedPath(Landroid/net/Uri;Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v3

    return-object v3

    .line 525
    :cond_1
    return-object p1
.end method


# virtual methods
.method public activeAPN(Ljava/lang/String;)Z
    .locals 10
    .param p1, "name"    # Ljava/lang/String;

    .line 453
    iget-object v0, p0, Lcom/miui/server/enterprise/APNManagerService;->mContext:Landroid/content/Context;

    invoke-static {v0}, Lcom/miui/server/enterprise/ServiceUtils;->checkPermission(Landroid/content/Context;)V

    .line 454
    invoke-direct {p0}, Lcom/miui/server/enterprise/APNManagerService;->getNumeric()Ljava/lang/String;

    move-result-object v0

    .line 455
    .local v0, "numeric":Ljava/lang/String;
    invoke-static {p1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    const-string v2, "APNManagerMode"

    const/4 v3, 0x0

    if-nez v1, :cond_3

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-eqz v1, :cond_0

    goto/16 :goto_1

    .line 459
    :cond_0
    invoke-direct {p0}, Lcom/miui/server/enterprise/APNManagerService;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v4

    sget-object v5, Landroid/provider/Telephony$Carriers;->CONTENT_URI:Landroid/net/Uri;

    const-string v1, "_id"

    filled-new-array {v1}, [Ljava/lang/String;

    move-result-object v6

    .line 460
    invoke-direct {p0, v0, p1}, Lcom/miui/server/enterprise/APNManagerService;->buildNumericAndNameSelection(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v7

    const/4 v8, 0x0

    const-string v9, "name ASC"

    .line 459
    invoke-virtual/range {v4 .. v9}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v1

    .line 463
    .local v1, "cursor":Landroid/database/Cursor;
    if-eqz v1, :cond_2

    :try_start_0
    invoke-interface {v1}, Landroid/database/Cursor;->getCount()I

    move-result v4

    if-lez v4, :cond_2

    .line 464
    invoke-interface {v1}, Landroid/database/Cursor;->moveToFirst()Z

    .line 465
    invoke-interface {v1, v3}, Landroid/database/Cursor;->getInt(I)I

    move-result v4

    .line 466
    .local v4, "apnId":I
    new-instance v5, Landroid/content/ContentValues;

    invoke-direct {v5}, Landroid/content/ContentValues;-><init>()V

    .line 467
    .local v5, "values":Landroid/content/ContentValues;
    const-string v6, "apn_id"

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v7

    invoke-virtual {v5, v6, v7}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 468
    invoke-direct {p0}, Lcom/miui/server/enterprise/APNManagerService;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v6

    sget-object v7, Lcom/miui/server/enterprise/APNManagerService;->PREFERAPN_URI:Landroid/net/Uri;

    const/4 v8, 0x0

    invoke-virtual {v6, v7, v5, v8, v8}, Landroid/content/ContentResolver;->update(Landroid/net/Uri;Landroid/content/ContentValues;Ljava/lang/String;[Ljava/lang/String;)I

    move-result v6

    .line 470
    .local v6, "result":I
    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    const-string v8, "active apn("

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v7

    const-string v8, "), result: "

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7, v6}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-static {v2, v7}, Landroid/util/Slog;->d(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 471
    if-lez v6, :cond_1

    const/4 v3, 0x1

    .line 477
    :cond_1
    invoke-direct {p0, v1}, Lcom/miui/server/enterprise/APNManagerService;->closeCursor(Landroid/database/Cursor;)V

    .line 471
    return v3

    .line 477
    .end local v4    # "apnId":I
    .end local v5    # "values":Landroid/content/ContentValues;
    .end local v6    # "result":I
    :catchall_0
    move-exception v2

    goto :goto_0

    .line 473
    :cond_2
    :try_start_1
    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "No such config: "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v2, v4}, Landroid/util/Slog;->d(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 474
    nop

    .line 477
    invoke-direct {p0, v1}, Lcom/miui/server/enterprise/APNManagerService;->closeCursor(Landroid/database/Cursor;)V

    .line 474
    return v3

    .line 477
    :goto_0
    invoke-direct {p0, v1}, Lcom/miui/server/enterprise/APNManagerService;->closeCursor(Landroid/database/Cursor;)V

    .line 478
    throw v2

    .line 456
    .end local v1    # "cursor":Landroid/database/Cursor;
    :cond_3
    :goto_1
    const-string v1, "neither name or numeric can\'t be null"

    invoke-static {v2, v1}, Landroid/util/Slog;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 457
    return v3
.end method

.method public activeAPNForNumeric(Ljava/lang/String;Ljava/lang/String;)V
    .locals 8
    .param p1, "numeric"    # Ljava/lang/String;
    .param p2, "name"    # Ljava/lang/String;

    .line 429
    iget-object v0, p0, Lcom/miui/server/enterprise/APNManagerService;->mContext:Landroid/content/Context;

    invoke-static {v0}, Lcom/miui/server/enterprise/ServiceUtils;->checkPermission(Landroid/content/Context;)V

    .line 430
    invoke-static {p2}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    const-string v1, "APNManagerMode"

    if-nez v0, :cond_2

    invoke-static {p1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_0

    goto/16 :goto_1

    .line 434
    :cond_0
    invoke-direct {p0}, Lcom/miui/server/enterprise/APNManagerService;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v2

    sget-object v3, Landroid/provider/Telephony$Carriers;->CONTENT_URI:Landroid/net/Uri;

    const-string v0, "_id"

    filled-new-array {v0}, [Ljava/lang/String;

    move-result-object v4

    .line 435
    invoke-direct {p0, p1, p2}, Lcom/miui/server/enterprise/APNManagerService;->buildNumericAndNameSelection(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v5

    const/4 v6, 0x0

    const-string v7, "name ASC"

    .line 434
    invoke-virtual/range {v2 .. v7}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v0

    .line 437
    .local v0, "cursor":Landroid/database/Cursor;
    if-eqz v0, :cond_1

    invoke-interface {v0}, Landroid/database/Cursor;->getCount()I

    move-result v2

    if-lez v2, :cond_1

    .line 438
    invoke-interface {v0}, Landroid/database/Cursor;->moveToFirst()Z

    .line 439
    const/4 v2, 0x0

    invoke-interface {v0, v2}, Landroid/database/Cursor;->getInt(I)I

    move-result v2

    .line 440
    .local v2, "apnId":I
    new-instance v3, Landroid/content/ContentValues;

    invoke-direct {v3}, Landroid/content/ContentValues;-><init>()V

    .line 441
    .local v3, "values":Landroid/content/ContentValues;
    const-string v4, "apn_id"

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    invoke-virtual {v3, v4, v5}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 442
    invoke-direct {p0}, Lcom/miui/server/enterprise/APNManagerService;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v4

    sget-object v5, Lcom/miui/server/enterprise/APNManagerService;->PREFERAPN_URI:Landroid/net/Uri;

    const/4 v6, 0x0

    invoke-virtual {v4, v5, v3, v6, v6}, Landroid/content/ContentResolver;->update(Landroid/net/Uri;Landroid/content/ContentValues;Ljava/lang/String;[Ljava/lang/String;)I

    move-result v4

    .line 444
    .local v4, "result":I
    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "active apn("

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, "), result: "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v1, v5}, Landroid/util/Slog;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 445
    invoke-interface {v0}, Landroid/database/Cursor;->close()V

    .line 446
    .end local v2    # "apnId":I
    .end local v3    # "values":Landroid/content/ContentValues;
    .end local v4    # "result":I
    goto :goto_0

    .line 447
    :cond_1
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "No such config: "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Slog;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 449
    :goto_0
    return-void

    .line 431
    .end local v0    # "cursor":Landroid/database/Cursor;
    :cond_2
    :goto_1
    const-string v0, "neither name or numeric can\'t be null"

    invoke-static {v1, v0}, Landroid/util/Slog;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 432
    return-void
.end method

.method public addAPN(Lcom/miui/enterprise/sdk/APNConfig;)Z
    .locals 7
    .param p1, "config"    # Lcom/miui/enterprise/sdk/APNConfig;

    .line 250
    iget-object v0, p0, Lcom/miui/server/enterprise/APNManagerService;->mContext:Landroid/content/Context;

    invoke-static {v0}, Lcom/miui/server/enterprise/ServiceUtils;->checkPermission(Landroid/content/Context;)V

    .line 252
    iget-object v0, p1, Lcom/miui/enterprise/sdk/APNConfig;->mNumeric:Ljava/lang/String;

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 253
    invoke-direct {p0}, Lcom/miui/server/enterprise/APNManagerService;->getNumeric()Ljava/lang/String;

    move-result-object v0

    .local v0, "numeric":Ljava/lang/String;
    goto :goto_0

    .line 255
    .end local v0    # "numeric":Ljava/lang/String;
    :cond_0
    iget-object v0, p1, Lcom/miui/enterprise/sdk/APNConfig;->mNumeric:Ljava/lang/String;

    .line 257
    .restart local v0    # "numeric":Ljava/lang/String;
    :goto_0
    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    const-string v2, "APNManagerMode"

    const/4 v3, 0x0

    if-eqz v1, :cond_1

    .line 258
    const-string v1, "addAPN:: Invalidate numeric"

    invoke-static {v2, v1}, Landroid/util/Slog;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 259
    return v3

    .line 261
    :cond_1
    new-instance v1, Landroid/content/ContentValues;

    invoke-direct {v1}, Landroid/content/ContentValues;-><init>()V

    .line 262
    .local v1, "values":Landroid/content/ContentValues;
    const-string v4, "name"

    iget-object v5, p1, Lcom/miui/enterprise/sdk/APNConfig;->mName:Ljava/lang/String;

    invoke-virtual {v1, v4, v5}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 263
    const-string v4, "apn"

    iget-object v5, p1, Lcom/miui/enterprise/sdk/APNConfig;->mApn:Ljava/lang/String;

    invoke-virtual {v1, v4, v5}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 264
    const-string/jumbo v4, "user"

    iget-object v5, p1, Lcom/miui/enterprise/sdk/APNConfig;->mUser:Ljava/lang/String;

    invoke-virtual {v1, v4, v5}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 265
    const-string v4, "password"

    iget-object v5, p1, Lcom/miui/enterprise/sdk/APNConfig;->mPassword:Ljava/lang/String;

    invoke-virtual {v1, v4, v5}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 266
    iget v4, p1, Lcom/miui/enterprise/sdk/APNConfig;->mAuthType:I

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    const-string v5, "authtype"

    invoke-virtual {v1, v5, v4}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 267
    iget v4, p1, Lcom/miui/enterprise/sdk/APNConfig;->mBearer:I

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    const-string v5, "bearer"

    invoke-virtual {v1, v5, v4}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 268
    iget-object v4, p1, Lcom/miui/enterprise/sdk/APNConfig;->mMcc:Ljava/lang/String;

    invoke-static {v4}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v4

    const/4 v5, 0x3

    if-eqz v4, :cond_2

    invoke-virtual {v0, v3, v5}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v4

    goto :goto_1

    :cond_2
    iget-object v4, p1, Lcom/miui/enterprise/sdk/APNConfig;->mMcc:Ljava/lang/String;

    :goto_1
    const-string v6, "mcc"

    invoke-virtual {v1, v6, v4}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 269
    iget-object v4, p1, Lcom/miui/enterprise/sdk/APNConfig;->mMnc:Ljava/lang/String;

    invoke-static {v4}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v4

    if-eqz v4, :cond_3

    const/4 v4, 0x5

    invoke-virtual {v0, v5, v4}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v4

    goto :goto_2

    :cond_3
    iget-object v4, p1, Lcom/miui/enterprise/sdk/APNConfig;->mMnc:Ljava/lang/String;

    :goto_2
    const-string v5, "mnc"

    invoke-virtual {v1, v5, v4}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 270
    const-string v4, "numeric"

    invoke-virtual {v1, v4, v0}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 274
    iget v4, p1, Lcom/miui/enterprise/sdk/APNConfig;->mCarrier_enabled:I

    const/4 v5, -0x1

    if-eq v4, v5, :cond_4

    .line 275
    iget v4, p1, Lcom/miui/enterprise/sdk/APNConfig;->mCarrier_enabled:I

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    const-string v6, "carrier_enabled"

    invoke-virtual {v1, v6, v4}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 277
    :cond_4
    iget v4, p1, Lcom/miui/enterprise/sdk/APNConfig;->mCurrent:I

    if-eq v4, v5, :cond_5

    .line 278
    iget v4, p1, Lcom/miui/enterprise/sdk/APNConfig;->mCarrier_enabled:I

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    const-string v5, "current"

    invoke-virtual {v1, v5, v4}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 281
    :cond_5
    iget-object v4, p1, Lcom/miui/enterprise/sdk/APNConfig;->mMmsc:Ljava/lang/String;

    if-eqz v4, :cond_6

    .line 282
    const-string v4, "mmsc"

    iget-object v5, p1, Lcom/miui/enterprise/sdk/APNConfig;->mMmsc:Ljava/lang/String;

    invoke-virtual {v1, v4, v5}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 285
    :cond_6
    iget-object v4, p1, Lcom/miui/enterprise/sdk/APNConfig;->mMmsport:Ljava/lang/String;

    if-eqz v4, :cond_7

    .line 286
    const-string v4, "mmsport"

    iget-object v5, p1, Lcom/miui/enterprise/sdk/APNConfig;->mMmsport:Ljava/lang/String;

    invoke-virtual {v1, v4, v5}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 288
    :cond_7
    iget-object v4, p1, Lcom/miui/enterprise/sdk/APNConfig;->mMmsproxy:Ljava/lang/String;

    if-eqz v4, :cond_8

    .line 289
    const-string v4, "mmsproxy"

    iget-object v5, p1, Lcom/miui/enterprise/sdk/APNConfig;->mMmsproxy:Ljava/lang/String;

    invoke-virtual {v1, v4, v5}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 290
    :cond_8
    iget-object v4, p1, Lcom/miui/enterprise/sdk/APNConfig;->mMvno_match_data:Ljava/lang/String;

    if-eqz v4, :cond_9

    .line 291
    const-string v4, "mvno_match_data"

    iget-object v5, p1, Lcom/miui/enterprise/sdk/APNConfig;->mMvno_match_data:Ljava/lang/String;

    invoke-virtual {v1, v4, v5}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 292
    :cond_9
    iget-object v4, p1, Lcom/miui/enterprise/sdk/APNConfig;->mMvno_type:Ljava/lang/String;

    if-eqz v4, :cond_a

    .line 293
    const-string v4, "mvno_type"

    iget-object v5, p1, Lcom/miui/enterprise/sdk/APNConfig;->mMvno_type:Ljava/lang/String;

    invoke-virtual {v1, v4, v5}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 294
    :cond_a
    iget-object v4, p1, Lcom/miui/enterprise/sdk/APNConfig;->mPort:Ljava/lang/String;

    if-eqz v4, :cond_b

    .line 295
    const-string v4, "port"

    iget-object v5, p1, Lcom/miui/enterprise/sdk/APNConfig;->mPort:Ljava/lang/String;

    invoke-virtual {v1, v4, v5}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 296
    :cond_b
    iget-object v4, p1, Lcom/miui/enterprise/sdk/APNConfig;->mProtocol:Ljava/lang/String;

    if-eqz v4, :cond_c

    .line 297
    const-string v4, "protocol"

    iget-object v5, p1, Lcom/miui/enterprise/sdk/APNConfig;->mProtocol:Ljava/lang/String;

    invoke-virtual {v1, v4, v5}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 298
    :cond_c
    iget-object v4, p1, Lcom/miui/enterprise/sdk/APNConfig;->mProxy:Ljava/lang/String;

    if-eqz v4, :cond_d

    .line 299
    const-string v4, "proxy"

    iget-object v5, p1, Lcom/miui/enterprise/sdk/APNConfig;->mProxy:Ljava/lang/String;

    invoke-virtual {v1, v4, v5}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 300
    :cond_d
    iget-object v4, p1, Lcom/miui/enterprise/sdk/APNConfig;->mRoaming_protocol:Ljava/lang/String;

    if-eqz v4, :cond_e

    .line 301
    const-string v4, "roaming_protocol"

    iget-object v5, p1, Lcom/miui/enterprise/sdk/APNConfig;->mRoaming_protocol:Ljava/lang/String;

    invoke-virtual {v1, v4, v5}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 302
    :cond_e
    iget-object v4, p1, Lcom/miui/enterprise/sdk/APNConfig;->mServer:Ljava/lang/String;

    if-eqz v4, :cond_f

    .line 303
    const-string/jumbo v4, "server"

    iget-object v5, p1, Lcom/miui/enterprise/sdk/APNConfig;->mServer:Ljava/lang/String;

    invoke-virtual {v1, v4, v5}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 304
    :cond_f
    iget-object v4, p1, Lcom/miui/enterprise/sdk/APNConfig;->mSub_id:Ljava/lang/String;

    if-eqz v4, :cond_10

    .line 305
    const-string/jumbo v4, "sub_id"

    iget-object v5, p1, Lcom/miui/enterprise/sdk/APNConfig;->mSub_id:Ljava/lang/String;

    invoke-virtual {v1, v4, v5}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 306
    :cond_10
    iget-object v4, p1, Lcom/miui/enterprise/sdk/APNConfig;->mType:Ljava/lang/String;

    if-eqz v4, :cond_11

    .line 307
    const-string/jumbo v4, "type"

    iget-object v5, p1, Lcom/miui/enterprise/sdk/APNConfig;->mType:Ljava/lang/String;

    invoke-virtual {v1, v4, v5}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 310
    :cond_11
    invoke-direct {p0}, Lcom/miui/server/enterprise/APNManagerService;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v4

    sget-object v5, Landroid/provider/Telephony$Carriers;->CONTENT_URI:Landroid/net/Uri;

    invoke-virtual {v4, v5, v1}, Landroid/content/ContentResolver;->insert(Landroid/net/Uri;Landroid/content/ContentValues;)Landroid/net/Uri;

    move-result-object v4

    .line 311
    .local v4, "uri":Landroid/net/Uri;
    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "addAPN:: New apn config: "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v2, v5}, Landroid/util/Slog;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 312
    if-eqz v4, :cond_12

    const/4 v3, 0x1

    :cond_12
    return v3
.end method

.method public addAPNForNumeric(Ljava/lang/String;Lcom/miui/enterprise/sdk/APNConfig;)V
    .locals 5
    .param p1, "numeric"    # Ljava/lang/String;
    .param p2, "config"    # Lcom/miui/enterprise/sdk/APNConfig;

    .line 227
    iget-object v0, p0, Lcom/miui/server/enterprise/APNManagerService;->mContext:Landroid/content/Context;

    invoke-static {v0}, Lcom/miui/server/enterprise/ServiceUtils;->checkPermission(Landroid/content/Context;)V

    .line 228
    invoke-static {p1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    const-string v1, "APNManagerMode"

    if-eqz v0, :cond_0

    .line 229
    const-string v0, "addAPNForNumeric:: Invalidate numeric"

    invoke-static {v1, v0}, Landroid/util/Slog;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 230
    return-void

    .line 232
    :cond_0
    new-instance v0, Landroid/content/ContentValues;

    invoke-direct {v0}, Landroid/content/ContentValues;-><init>()V

    .line 233
    .local v0, "values":Landroid/content/ContentValues;
    const-string v2, "name"

    iget-object v3, p2, Lcom/miui/enterprise/sdk/APNConfig;->mName:Ljava/lang/String;

    invoke-virtual {v0, v2, v3}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 234
    const-string v2, "apn"

    iget-object v3, p2, Lcom/miui/enterprise/sdk/APNConfig;->mApn:Ljava/lang/String;

    invoke-virtual {v0, v2, v3}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 235
    const-string/jumbo v2, "user"

    iget-object v3, p2, Lcom/miui/enterprise/sdk/APNConfig;->mUser:Ljava/lang/String;

    invoke-virtual {v0, v2, v3}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 236
    const-string v2, "password"

    iget-object v3, p2, Lcom/miui/enterprise/sdk/APNConfig;->mPassword:Ljava/lang/String;

    invoke-virtual {v0, v2, v3}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 237
    iget v2, p2, Lcom/miui/enterprise/sdk/APNConfig;->mAuthType:I

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    const-string v3, "authtype"

    invoke-virtual {v0, v3, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 238
    iget v2, p2, Lcom/miui/enterprise/sdk/APNConfig;->mBearer:I

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    const-string v3, "bearer"

    invoke-virtual {v0, v3, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 239
    const/4 v2, 0x0

    const/4 v3, 0x3

    invoke-virtual {p1, v2, v3}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v2

    const-string v4, "mcc"

    invoke-virtual {v0, v4, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 240
    const/4 v2, 0x5

    invoke-virtual {p1, v3, v2}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v2

    const-string v3, "mnc"

    invoke-virtual {v0, v3, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 241
    const-string v2, "numeric"

    invoke-virtual {v0, v2, p1}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 244
    invoke-direct {p0}, Lcom/miui/server/enterprise/APNManagerService;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v2

    sget-object v3, Landroid/provider/Telephony$Carriers;->CONTENT_URI:Landroid/net/Uri;

    invoke-virtual {v2, v3, v0}, Landroid/content/ContentResolver;->insert(Landroid/net/Uri;Landroid/content/ContentValues;)Landroid/net/Uri;

    move-result-object v2

    .line 245
    .local v2, "uri":Landroid/net/Uri;
    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "addAPNForNumeric:: New apn config: "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v1, v3}, Landroid/util/Slog;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 246
    return-void
.end method

.method public deleteAPN(Ljava/lang/String;)Z
    .locals 6
    .param p1, "name"    # Ljava/lang/String;

    .line 329
    iget-object v0, p0, Lcom/miui/server/enterprise/APNManagerService;->mContext:Landroid/content/Context;

    invoke-static {v0}, Lcom/miui/server/enterprise/ServiceUtils;->checkPermission(Landroid/content/Context;)V

    .line 331
    invoke-static {p1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    const/4 v1, 0x0

    const-string v2, "APNManagerMode"

    if-eqz v0, :cond_0

    .line 332
    const-string v0, "neither name can\'t be null"

    invoke-static {v2, v0}, Landroid/util/Slog;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 333
    return v1

    .line 335
    :cond_0
    invoke-direct {p0}, Lcom/miui/server/enterprise/APNManagerService;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    sget-object v3, Landroid/provider/Telephony$Carriers;->CONTENT_URI:Landroid/net/Uri;

    .line 336
    invoke-direct {p0, p1}, Lcom/miui/server/enterprise/APNManagerService;->buildNameSelection(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    .line 335
    const/4 v5, 0x0

    invoke-virtual {v0, v3, v4, v5}, Landroid/content/ContentResolver;->delete(Landroid/net/Uri;Ljava/lang/String;[Ljava/lang/String;)I

    move-result v0

    .line 337
    .local v0, "count":I
    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "Delete apn "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, "rows"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Landroid/util/Slog;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 338
    if-lez v0, :cond_1

    const/4 v1, 0x1

    :cond_1
    return v1
.end method

.method public deleteAPNForNumeric(Ljava/lang/String;Ljava/lang/String;)V
    .locals 5
    .param p1, "numeric"    # Ljava/lang/String;
    .param p2, "name"    # Ljava/lang/String;

    .line 317
    iget-object v0, p0, Lcom/miui/server/enterprise/APNManagerService;->mContext:Landroid/content/Context;

    invoke-static {v0}, Lcom/miui/server/enterprise/ServiceUtils;->checkPermission(Landroid/content/Context;)V

    .line 318
    invoke-static {p2}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    const-string v1, "APNManagerMode"

    if-nez v0, :cond_1

    invoke-static {p1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_0

    goto :goto_0

    .line 322
    :cond_0
    invoke-direct {p0}, Lcom/miui/server/enterprise/APNManagerService;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    sget-object v2, Landroid/provider/Telephony$Carriers;->CONTENT_URI:Landroid/net/Uri;

    .line 323
    invoke-direct {p0, p1, p2}, Lcom/miui/server/enterprise/APNManagerService;->buildNumericAndNameSelection(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    .line 322
    const/4 v4, 0x0

    invoke-virtual {v0, v2, v3, v4}, Landroid/content/ContentResolver;->delete(Landroid/net/Uri;Ljava/lang/String;[Ljava/lang/String;)I

    move-result v0

    .line 324
    .local v0, "count":I
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "Delete apn "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, "rows"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Slog;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 325
    return-void

    .line 319
    .end local v0    # "count":I
    :cond_1
    :goto_0
    const-string v0, "neither name or numeric can\'t be null"

    invoke-static {v1, v0}, Landroid/util/Slog;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 320
    return-void
.end method

.method public editAPN(Ljava/lang/String;Lcom/miui/enterprise/sdk/APNConfig;)Z
    .locals 7
    .param p1, "name"    # Ljava/lang/String;
    .param p2, "config"    # Lcom/miui/enterprise/sdk/APNConfig;

    .line 365
    iget-object v0, p0, Lcom/miui/server/enterprise/APNManagerService;->mContext:Landroid/content/Context;

    invoke-static {v0}, Lcom/miui/server/enterprise/ServiceUtils;->checkPermission(Landroid/content/Context;)V

    .line 367
    iget-object v0, p2, Lcom/miui/enterprise/sdk/APNConfig;->mNumeric:Ljava/lang/String;

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 368
    invoke-direct {p0}, Lcom/miui/server/enterprise/APNManagerService;->getNumeric()Ljava/lang/String;

    move-result-object v0

    .local v0, "numeric":Ljava/lang/String;
    goto :goto_0

    .line 370
    .end local v0    # "numeric":Ljava/lang/String;
    :cond_0
    iget-object v0, p2, Lcom/miui/enterprise/sdk/APNConfig;->mNumeric:Ljava/lang/String;

    .line 372
    .restart local v0    # "numeric":Ljava/lang/String;
    :goto_0
    invoke-static {p1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    const/4 v2, 0x0

    if-nez v1, :cond_13

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-eqz v1, :cond_1

    goto/16 :goto_3

    .line 376
    :cond_1
    new-instance v1, Landroid/content/ContentValues;

    invoke-direct {v1}, Landroid/content/ContentValues;-><init>()V

    .line 377
    .local v1, "values":Landroid/content/ContentValues;
    const-string v3, "name"

    iget-object v4, p2, Lcom/miui/enterprise/sdk/APNConfig;->mName:Ljava/lang/String;

    invoke-virtual {v1, v3, v4}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 378
    const-string v3, "apn"

    iget-object v4, p2, Lcom/miui/enterprise/sdk/APNConfig;->mApn:Ljava/lang/String;

    invoke-virtual {v1, v3, v4}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 379
    const-string/jumbo v3, "user"

    iget-object v4, p2, Lcom/miui/enterprise/sdk/APNConfig;->mUser:Ljava/lang/String;

    invoke-virtual {v1, v3, v4}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 380
    const-string v3, "password"

    iget-object v4, p2, Lcom/miui/enterprise/sdk/APNConfig;->mPassword:Ljava/lang/String;

    invoke-virtual {v1, v3, v4}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 381
    iget v3, p2, Lcom/miui/enterprise/sdk/APNConfig;->mAuthType:I

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    const-string v4, "authtype"

    invoke-virtual {v1, v4, v3}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 382
    iget v3, p2, Lcom/miui/enterprise/sdk/APNConfig;->mBearer:I

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    const-string v4, "bearer"

    invoke-virtual {v1, v4, v3}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 383
    iget-object v3, p2, Lcom/miui/enterprise/sdk/APNConfig;->mMcc:Ljava/lang/String;

    invoke-static {v3}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v3

    const/4 v4, 0x3

    if-eqz v3, :cond_2

    invoke-virtual {v0, v2, v4}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v3

    goto :goto_1

    :cond_2
    iget-object v3, p2, Lcom/miui/enterprise/sdk/APNConfig;->mMcc:Ljava/lang/String;

    :goto_1
    const-string v5, "mcc"

    invoke-virtual {v1, v5, v3}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 384
    iget-object v3, p2, Lcom/miui/enterprise/sdk/APNConfig;->mMnc:Ljava/lang/String;

    invoke-static {v3}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v3

    if-eqz v3, :cond_3

    const/4 v3, 0x5

    invoke-virtual {v0, v4, v3}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v3

    goto :goto_2

    :cond_3
    iget-object v3, p2, Lcom/miui/enterprise/sdk/APNConfig;->mMnc:Ljava/lang/String;

    :goto_2
    const-string v4, "mnc"

    invoke-virtual {v1, v4, v3}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 385
    const-string v3, "numeric"

    invoke-virtual {v1, v3, v0}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 387
    iget v3, p2, Lcom/miui/enterprise/sdk/APNConfig;->mCarrier_enabled:I

    const/4 v4, -0x1

    if-eq v3, v4, :cond_4

    .line 388
    iget v3, p2, Lcom/miui/enterprise/sdk/APNConfig;->mCarrier_enabled:I

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    const-string v5, "carrier_enabled"

    invoke-virtual {v1, v5, v3}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 390
    :cond_4
    iget v3, p2, Lcom/miui/enterprise/sdk/APNConfig;->mCurrent:I

    if-eq v3, v4, :cond_5

    .line 391
    iget v3, p2, Lcom/miui/enterprise/sdk/APNConfig;->mCarrier_enabled:I

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    const-string v4, "current"

    invoke-virtual {v1, v4, v3}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 394
    :cond_5
    iget-object v3, p2, Lcom/miui/enterprise/sdk/APNConfig;->mMmsc:Ljava/lang/String;

    if-eqz v3, :cond_6

    .line 395
    const-string v3, "mmsc"

    iget-object v4, p2, Lcom/miui/enterprise/sdk/APNConfig;->mMmsc:Ljava/lang/String;

    invoke-virtual {v1, v3, v4}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 398
    :cond_6
    iget-object v3, p2, Lcom/miui/enterprise/sdk/APNConfig;->mMmsport:Ljava/lang/String;

    if-eqz v3, :cond_7

    .line 399
    const-string v3, "mmsport"

    iget-object v4, p2, Lcom/miui/enterprise/sdk/APNConfig;->mMmsport:Ljava/lang/String;

    invoke-virtual {v1, v3, v4}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 401
    :cond_7
    iget-object v3, p2, Lcom/miui/enterprise/sdk/APNConfig;->mMmsproxy:Ljava/lang/String;

    if-eqz v3, :cond_8

    .line 402
    const-string v3, "mmsproxy"

    iget-object v4, p2, Lcom/miui/enterprise/sdk/APNConfig;->mMmsproxy:Ljava/lang/String;

    invoke-virtual {v1, v3, v4}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 403
    :cond_8
    iget-object v3, p2, Lcom/miui/enterprise/sdk/APNConfig;->mMvno_match_data:Ljava/lang/String;

    if-eqz v3, :cond_9

    .line 404
    const-string v3, "mvno_match_data"

    iget-object v4, p2, Lcom/miui/enterprise/sdk/APNConfig;->mMvno_match_data:Ljava/lang/String;

    invoke-virtual {v1, v3, v4}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 405
    :cond_9
    iget-object v3, p2, Lcom/miui/enterprise/sdk/APNConfig;->mMvno_type:Ljava/lang/String;

    if-eqz v3, :cond_a

    .line 406
    const-string v3, "mvno_type"

    iget-object v4, p2, Lcom/miui/enterprise/sdk/APNConfig;->mMvno_type:Ljava/lang/String;

    invoke-virtual {v1, v3, v4}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 407
    :cond_a
    iget-object v3, p2, Lcom/miui/enterprise/sdk/APNConfig;->mPort:Ljava/lang/String;

    if-eqz v3, :cond_b

    .line 408
    const-string v3, "port"

    iget-object v4, p2, Lcom/miui/enterprise/sdk/APNConfig;->mPort:Ljava/lang/String;

    invoke-virtual {v1, v3, v4}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 409
    :cond_b
    iget-object v3, p2, Lcom/miui/enterprise/sdk/APNConfig;->mProtocol:Ljava/lang/String;

    if-eqz v3, :cond_c

    .line 410
    const-string v3, "protocol"

    iget-object v4, p2, Lcom/miui/enterprise/sdk/APNConfig;->mProtocol:Ljava/lang/String;

    invoke-virtual {v1, v3, v4}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 411
    :cond_c
    iget-object v3, p2, Lcom/miui/enterprise/sdk/APNConfig;->mProxy:Ljava/lang/String;

    if-eqz v3, :cond_d

    .line 412
    const-string v3, "proxy"

    iget-object v4, p2, Lcom/miui/enterprise/sdk/APNConfig;->mProxy:Ljava/lang/String;

    invoke-virtual {v1, v3, v4}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 413
    :cond_d
    iget-object v3, p2, Lcom/miui/enterprise/sdk/APNConfig;->mRoaming_protocol:Ljava/lang/String;

    if-eqz v3, :cond_e

    .line 414
    const-string v3, "roaming_protocol"

    iget-object v4, p2, Lcom/miui/enterprise/sdk/APNConfig;->mRoaming_protocol:Ljava/lang/String;

    invoke-virtual {v1, v3, v4}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 415
    :cond_e
    iget-object v3, p2, Lcom/miui/enterprise/sdk/APNConfig;->mServer:Ljava/lang/String;

    if-eqz v3, :cond_f

    .line 416
    const-string/jumbo v3, "server"

    iget-object v4, p2, Lcom/miui/enterprise/sdk/APNConfig;->mServer:Ljava/lang/String;

    invoke-virtual {v1, v3, v4}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 417
    :cond_f
    iget-object v3, p2, Lcom/miui/enterprise/sdk/APNConfig;->mSub_id:Ljava/lang/String;

    if-eqz v3, :cond_10

    .line 418
    const-string/jumbo v3, "sub_id"

    iget-object v4, p2, Lcom/miui/enterprise/sdk/APNConfig;->mSub_id:Ljava/lang/String;

    invoke-virtual {v1, v3, v4}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 419
    :cond_10
    iget-object v3, p2, Lcom/miui/enterprise/sdk/APNConfig;->mType:Ljava/lang/String;

    if-eqz v3, :cond_11

    .line 420
    const-string/jumbo v3, "type"

    iget-object v4, p2, Lcom/miui/enterprise/sdk/APNConfig;->mType:Ljava/lang/String;

    invoke-virtual {v1, v3, v4}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 422
    :cond_11
    invoke-direct {p0}, Lcom/miui/server/enterprise/APNManagerService;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v3

    sget-object v4, Landroid/provider/Telephony$Carriers;->CONTENT_URI:Landroid/net/Uri;

    .line 423
    invoke-direct {p0, v0, p1}, Lcom/miui/server/enterprise/APNManagerService;->buildNumericAndNameSelection(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v5

    .line 422
    const/4 v6, 0x0

    invoke-virtual {v3, v4, v1, v5, v6}, Landroid/content/ContentResolver;->update(Landroid/net/Uri;Landroid/content/ContentValues;Ljava/lang/String;[Ljava/lang/String;)I

    move-result v3

    .line 424
    .local v3, "count":I
    if-lez v3, :cond_12

    const/4 v2, 0x1

    :cond_12
    return v2

    .line 373
    .end local v1    # "values":Landroid/content/ContentValues;
    .end local v3    # "count":I
    :cond_13
    :goto_3
    const-string v1, "APNManagerMode"

    const-string v3, "neither name or numeric can\'t be null"

    invoke-static {v1, v3}, Landroid/util/Slog;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 374
    return v2
.end method

.method public editAPNForNumeric(Ljava/lang/String;Ljava/lang/String;Lcom/miui/enterprise/sdk/APNConfig;)V
    .locals 6
    .param p1, "numeric"    # Ljava/lang/String;
    .param p2, "name"    # Ljava/lang/String;
    .param p3, "config"    # Lcom/miui/enterprise/sdk/APNConfig;

    .line 343
    iget-object v0, p0, Lcom/miui/server/enterprise/APNManagerService;->mContext:Landroid/content/Context;

    invoke-static {v0}, Lcom/miui/server/enterprise/ServiceUtils;->checkPermission(Landroid/content/Context;)V

    .line 344
    invoke-static {p2}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    const-string v1, "APNManagerMode"

    if-nez v0, :cond_1

    invoke-static {p1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_0

    goto/16 :goto_0

    .line 348
    :cond_0
    new-instance v0, Landroid/content/ContentValues;

    invoke-direct {v0}, Landroid/content/ContentValues;-><init>()V

    .line 349
    .local v0, "values":Landroid/content/ContentValues;
    const-string v2, "name"

    iget-object v3, p3, Lcom/miui/enterprise/sdk/APNConfig;->mName:Ljava/lang/String;

    invoke-virtual {v0, v2, v3}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 350
    const-string v2, "apn"

    iget-object v3, p3, Lcom/miui/enterprise/sdk/APNConfig;->mApn:Ljava/lang/String;

    invoke-virtual {v0, v2, v3}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 351
    const-string/jumbo v2, "user"

    iget-object v3, p3, Lcom/miui/enterprise/sdk/APNConfig;->mUser:Ljava/lang/String;

    invoke-virtual {v0, v2, v3}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 352
    const-string v2, "password"

    iget-object v3, p3, Lcom/miui/enterprise/sdk/APNConfig;->mPassword:Ljava/lang/String;

    invoke-virtual {v0, v2, v3}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 353
    iget v2, p3, Lcom/miui/enterprise/sdk/APNConfig;->mAuthType:I

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    const-string v3, "authtype"

    invoke-virtual {v0, v3, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 354
    iget v2, p3, Lcom/miui/enterprise/sdk/APNConfig;->mBearer:I

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    const-string v3, "bearer"

    invoke-virtual {v0, v3, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 355
    const/4 v2, 0x0

    const/4 v3, 0x3

    invoke-virtual {p1, v2, v3}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v2

    const-string v4, "mcc"

    invoke-virtual {v0, v4, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 356
    const/4 v2, 0x5

    invoke-virtual {p1, v3, v2}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v2

    const-string v3, "mnc"

    invoke-virtual {v0, v3, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 357
    const-string v2, "numeric"

    invoke-virtual {v0, v2, p1}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 358
    invoke-direct {p0}, Lcom/miui/server/enterprise/APNManagerService;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v2

    sget-object v3, Landroid/provider/Telephony$Carriers;->CONTENT_URI:Landroid/net/Uri;

    .line 359
    invoke-direct {p0, p1, p2}, Lcom/miui/server/enterprise/APNManagerService;->buildNumericAndNameSelection(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    .line 358
    const/4 v5, 0x0

    invoke-virtual {v2, v3, v0, v4, v5}, Landroid/content/ContentResolver;->update(Landroid/net/Uri;Landroid/content/ContentValues;Ljava/lang/String;[Ljava/lang/String;)I

    move-result v2

    .line 360
    .local v2, "count":I
    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "Update apn "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, "rows"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v1, v3}, Landroid/util/Slog;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 361
    return-void

    .line 345
    .end local v0    # "values":Landroid/content/ContentValues;
    .end local v2    # "count":I
    :cond_1
    :goto_0
    const-string v0, "neither name or numeric can\'t be null"

    invoke-static {v1, v0}, Landroid/util/Slog;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 346
    return-void
.end method

.method public getAPN(Ljava/lang/String;)Lcom/miui/enterprise/sdk/APNConfig;
    .locals 27
    .param p1, "name"    # Ljava/lang/String;

    .line 178
    move-object/from16 v1, p0

    iget-object v0, v1, Lcom/miui/server/enterprise/APNManagerService;->mContext:Landroid/content/Context;

    invoke-static {v0}, Lcom/miui/server/enterprise/ServiceUtils;->checkPermission(Landroid/content/Context;)V

    .line 180
    invoke-direct/range {p0 .. p0}, Lcom/miui/server/enterprise/APNManagerService;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v2

    sget-object v3, Landroid/provider/Telephony$Carriers;->CONTENT_URI:Landroid/net/Uri;

    const-string v4, "name"

    const-string v5, "apn"

    const-string/jumbo v6, "user"

    const-string v7, "password"

    const-string v8, "authtype"

    const-string v9, "bearer"

    const-string v10, "mcc"

    const-string v11, "mnc"

    const-string v12, "numeric"

    const-string v13, "carrier_enabled"

    const-string v14, "current"

    const-string v15, "mmsc"

    const-string v16, "mmsport"

    const-string v17, "mmsproxy"

    const-string v18, "mvno_match_data"

    const-string v19, "mvno_type"

    const-string v20, "port"

    const-string v21, "protocol"

    const-string v22, "proxy"

    const-string v23, "roaming_protocol"

    const-string/jumbo v24, "server"

    const-string/jumbo v25, "sub_id"

    const-string/jumbo v26, "type"

    filled-new-array/range {v4 .. v26}, [Ljava/lang/String;

    move-result-object v4

    .line 187
    invoke-direct/range {p0 .. p1}, Lcom/miui/server/enterprise/APNManagerService;->buildNameSelection(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v5

    const/4 v6, 0x0

    const-string v7, "name ASC"

    .line 180
    invoke-virtual/range {v2 .. v7}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v2

    .line 190
    .local v2, "cursor":Landroid/database/Cursor;
    if-eqz v2, :cond_0

    :try_start_0
    invoke-interface {v2}, Landroid/database/Cursor;->getCount()I

    move-result v0

    if-lez v0, :cond_0

    .line 191
    invoke-interface {v2}, Landroid/database/Cursor;->moveToFirst()Z

    .line 192
    new-instance v0, Lcom/miui/enterprise/sdk/APNConfig;

    invoke-direct {v0}, Lcom/miui/enterprise/sdk/APNConfig;-><init>()V

    .line 193
    .local v0, "configItem":Lcom/miui/enterprise/sdk/APNConfig;
    const/4 v3, 0x0

    invoke-interface {v2, v3}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v3

    iput-object v3, v0, Lcom/miui/enterprise/sdk/APNConfig;->mName:Ljava/lang/String;

    .line 194
    const/4 v3, 0x1

    invoke-interface {v2, v3}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v3

    iput-object v3, v0, Lcom/miui/enterprise/sdk/APNConfig;->mApn:Ljava/lang/String;

    .line 195
    const/4 v3, 0x2

    invoke-interface {v2, v3}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v3

    iput-object v3, v0, Lcom/miui/enterprise/sdk/APNConfig;->mUser:Ljava/lang/String;

    .line 196
    const/4 v3, 0x3

    invoke-interface {v2, v3}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v3

    iput-object v3, v0, Lcom/miui/enterprise/sdk/APNConfig;->mPassword:Ljava/lang/String;

    .line 197
    const/4 v3, 0x4

    invoke-interface {v2, v3}, Landroid/database/Cursor;->getInt(I)I

    move-result v3

    iput v3, v0, Lcom/miui/enterprise/sdk/APNConfig;->mAuthType:I

    .line 198
    const/4 v3, 0x5

    invoke-interface {v2, v3}, Landroid/database/Cursor;->getInt(I)I

    move-result v3

    iput v3, v0, Lcom/miui/enterprise/sdk/APNConfig;->mBearer:I

    .line 199
    const/4 v3, 0x6

    invoke-interface {v2, v3}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v3

    iput-object v3, v0, Lcom/miui/enterprise/sdk/APNConfig;->mMcc:Ljava/lang/String;

    .line 200
    const/4 v3, 0x7

    invoke-interface {v2, v3}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v3

    iput-object v3, v0, Lcom/miui/enterprise/sdk/APNConfig;->mMnc:Ljava/lang/String;

    .line 201
    const/16 v3, 0x8

    invoke-interface {v2, v3}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v3

    iput-object v3, v0, Lcom/miui/enterprise/sdk/APNConfig;->mNumeric:Ljava/lang/String;

    .line 202
    const/16 v3, 0x9

    invoke-interface {v2, v3}, Landroid/database/Cursor;->getInt(I)I

    move-result v3

    iput v3, v0, Lcom/miui/enterprise/sdk/APNConfig;->mCarrier_enabled:I

    .line 203
    const/16 v3, 0xa

    invoke-interface {v2, v3}, Landroid/database/Cursor;->getInt(I)I

    move-result v3

    iput v3, v0, Lcom/miui/enterprise/sdk/APNConfig;->mCurrent:I

    .line 204
    const/16 v3, 0xb

    invoke-interface {v2, v3}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v3

    iput-object v3, v0, Lcom/miui/enterprise/sdk/APNConfig;->mMmsc:Ljava/lang/String;

    .line 205
    const/16 v3, 0xc

    invoke-interface {v2, v3}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v3

    iput-object v3, v0, Lcom/miui/enterprise/sdk/APNConfig;->mMmsport:Ljava/lang/String;

    .line 206
    const/16 v3, 0xd

    invoke-interface {v2, v3}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v3

    iput-object v3, v0, Lcom/miui/enterprise/sdk/APNConfig;->mMmsproxy:Ljava/lang/String;

    .line 207
    const/16 v3, 0xe

    invoke-interface {v2, v3}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v3

    iput-object v3, v0, Lcom/miui/enterprise/sdk/APNConfig;->mMvno_match_data:Ljava/lang/String;

    .line 208
    const/16 v3, 0xf

    invoke-interface {v2, v3}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v3

    iput-object v3, v0, Lcom/miui/enterprise/sdk/APNConfig;->mMvno_type:Ljava/lang/String;

    .line 209
    const/16 v3, 0x10

    invoke-interface {v2, v3}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v3

    iput-object v3, v0, Lcom/miui/enterprise/sdk/APNConfig;->mPort:Ljava/lang/String;

    .line 210
    const/16 v3, 0x11

    invoke-interface {v2, v3}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v3

    iput-object v3, v0, Lcom/miui/enterprise/sdk/APNConfig;->mProtocol:Ljava/lang/String;

    .line 211
    const/16 v3, 0x12

    invoke-interface {v2, v3}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v3

    iput-object v3, v0, Lcom/miui/enterprise/sdk/APNConfig;->mProxy:Ljava/lang/String;

    .line 212
    const/16 v3, 0x13

    invoke-interface {v2, v3}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v3

    iput-object v3, v0, Lcom/miui/enterprise/sdk/APNConfig;->mRoaming_protocol:Ljava/lang/String;

    .line 213
    const/16 v3, 0x14

    invoke-interface {v2, v3}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v3

    iput-object v3, v0, Lcom/miui/enterprise/sdk/APNConfig;->mServer:Ljava/lang/String;

    .line 214
    const/16 v3, 0x15

    invoke-interface {v2, v3}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v3

    iput-object v3, v0, Lcom/miui/enterprise/sdk/APNConfig;->mSub_id:Ljava/lang/String;

    .line 215
    const/16 v3, 0x16

    invoke-interface {v2, v3}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v3

    iput-object v3, v0, Lcom/miui/enterprise/sdk/APNConfig;->mType:Ljava/lang/String;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 216
    nop

    .line 221
    invoke-direct {v1, v2}, Lcom/miui/server/enterprise/APNManagerService;->closeCursor(Landroid/database/Cursor;)V

    .line 216
    return-object v0

    .line 221
    .end local v0    # "configItem":Lcom/miui/enterprise/sdk/APNConfig;
    :catchall_0
    move-exception v0

    invoke-direct {v1, v2}, Lcom/miui/server/enterprise/APNManagerService;->closeCursor(Landroid/database/Cursor;)V

    .line 222
    throw v0

    .line 218
    :cond_0
    nop

    .line 221
    invoke-direct {v1, v2}, Lcom/miui/server/enterprise/APNManagerService;->closeCursor(Landroid/database/Cursor;)V

    .line 218
    const/4 v0, 0x0

    return-object v0
.end method

.method public getAPNActiveMode()I
    .locals 3

    .line 497
    iget-object v0, p0, Lcom/miui/server/enterprise/APNManagerService;->mContext:Landroid/content/Context;

    invoke-static {v0}, Lcom/miui/server/enterprise/ServiceUtils;->checkPermission(Landroid/content/Context;)V

    .line 498
    iget-object v0, p0, Lcom/miui/server/enterprise/APNManagerService;->mContext:Landroid/content/Context;

    const-string v1, "ep_apn_switch_mode"

    const/4 v2, 0x0

    invoke-static {v0, v1, v2, v2}, Lcom/miui/enterprise/settings/EnterpriseSettings;->getInt(Landroid/content/Context;Ljava/lang/String;II)I

    move-result v0

    return v0
.end method

.method public getAPNForNumeric(Ljava/lang/String;Ljava/lang/String;)Lcom/miui/enterprise/sdk/APNConfig;
    .locals 9
    .param p1, "numeric"    # Ljava/lang/String;
    .param p2, "name"    # Ljava/lang/String;

    .line 152
    iget-object v0, p0, Lcom/miui/server/enterprise/APNManagerService;->mContext:Landroid/content/Context;

    invoke-static {v0}, Lcom/miui/server/enterprise/ServiceUtils;->checkPermission(Landroid/content/Context;)V

    .line 153
    invoke-direct {p0}, Lcom/miui/server/enterprise/APNManagerService;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v1

    sget-object v2, Landroid/provider/Telephony$Carriers;->CONTENT_URI:Landroid/net/Uri;

    const-string v3, "name"

    const-string v4, "apn"

    const-string/jumbo v5, "user"

    const-string v6, "password"

    const-string v7, "authtype"

    const-string v8, "bearer"

    filled-new-array/range {v3 .. v8}, [Ljava/lang/String;

    move-result-object v3

    .line 155
    invoke-direct {p0, p1, p2}, Lcom/miui/server/enterprise/APNManagerService;->buildNumericAndNameSelection(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    const/4 v5, 0x0

    const-string v6, "name ASC"

    .line 153
    invoke-virtual/range {v1 .. v6}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v0

    .line 158
    .local v0, "cursor":Landroid/database/Cursor;
    if-eqz v0, :cond_0

    :try_start_0
    invoke-interface {v0}, Landroid/database/Cursor;->getCount()I

    move-result v1

    if-lez v1, :cond_0

    .line 159
    invoke-interface {v0}, Landroid/database/Cursor;->moveToFirst()Z

    .line 160
    new-instance v1, Lcom/miui/enterprise/sdk/APNConfig;

    invoke-direct {v1}, Lcom/miui/enterprise/sdk/APNConfig;-><init>()V

    .line 161
    .local v1, "configItem":Lcom/miui/enterprise/sdk/APNConfig;
    const/4 v2, 0x0

    invoke-interface {v0, v2}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v2

    iput-object v2, v1, Lcom/miui/enterprise/sdk/APNConfig;->mName:Ljava/lang/String;

    .line 162
    const/4 v2, 0x1

    invoke-interface {v0, v2}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v2

    iput-object v2, v1, Lcom/miui/enterprise/sdk/APNConfig;->mApn:Ljava/lang/String;

    .line 163
    const/4 v2, 0x2

    invoke-interface {v0, v2}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v2

    iput-object v2, v1, Lcom/miui/enterprise/sdk/APNConfig;->mUser:Ljava/lang/String;

    .line 164
    const/4 v2, 0x3

    invoke-interface {v0, v2}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v2

    iput-object v2, v1, Lcom/miui/enterprise/sdk/APNConfig;->mPassword:Ljava/lang/String;

    .line 165
    const/4 v2, 0x4

    invoke-interface {v0, v2}, Landroid/database/Cursor;->getInt(I)I

    move-result v2

    iput v2, v1, Lcom/miui/enterprise/sdk/APNConfig;->mAuthType:I

    .line 166
    const/4 v2, 0x5

    invoke-interface {v0, v2}, Landroid/database/Cursor;->getInt(I)I

    move-result v2

    iput v2, v1, Lcom/miui/enterprise/sdk/APNConfig;->mBearer:I
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 167
    nop

    .line 172
    invoke-direct {p0, v0}, Lcom/miui/server/enterprise/APNManagerService;->closeCursor(Landroid/database/Cursor;)V

    .line 167
    return-object v1

    .line 172
    .end local v1    # "configItem":Lcom/miui/enterprise/sdk/APNConfig;
    :catchall_0
    move-exception v1

    invoke-direct {p0, v0}, Lcom/miui/server/enterprise/APNManagerService;->closeCursor(Landroid/database/Cursor;)V

    .line 173
    throw v1

    .line 169
    :cond_0
    nop

    .line 172
    invoke-direct {p0, v0}, Lcom/miui/server/enterprise/APNManagerService;->closeCursor(Landroid/database/Cursor;)V

    .line 169
    const/4 v1, 0x0

    return-object v1
.end method

.method public getAPNList()Ljava/util/List;
    .locals 28
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List<",
            "Lcom/miui/enterprise/sdk/APNConfig;",
            ">;"
        }
    .end annotation

    .line 100
    move-object/from16 v1, p0

    iget-object v0, v1, Lcom/miui/server/enterprise/APNManagerService;->mContext:Landroid/content/Context;

    invoke-static {v0}, Lcom/miui/server/enterprise/ServiceUtils;->checkPermission(Landroid/content/Context;)V

    .line 101
    invoke-direct/range {p0 .. p0}, Lcom/miui/server/enterprise/APNManagerService;->getNumeric()Ljava/lang/String;

    move-result-object v2

    .line 102
    .local v2, "numeric":Ljava/lang/String;
    invoke-direct/range {p0 .. p0}, Lcom/miui/server/enterprise/APNManagerService;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v3

    sget-object v4, Landroid/provider/Telephony$Carriers;->CONTENT_URI:Landroid/net/Uri;

    const-string v5, "name"

    const-string v6, "apn"

    const-string/jumbo v7, "user"

    const-string v8, "password"

    const-string v9, "authtype"

    const-string v10, "bearer"

    const-string v11, "mcc"

    const-string v12, "mnc"

    const-string v13, "numeric"

    const-string v14, "carrier_enabled"

    const-string v15, "current"

    const-string v16, "mmsc"

    const-string v17, "mmsport"

    const-string v18, "mmsproxy"

    const-string v19, "mvno_match_data"

    const-string v20, "mvno_type"

    const-string v21, "port"

    const-string v22, "protocol"

    const-string v23, "proxy"

    const-string v24, "roaming_protocol"

    const-string/jumbo v25, "server"

    const-string/jumbo v26, "sub_id"

    const-string/jumbo v27, "type"

    filled-new-array/range {v5 .. v27}, [Ljava/lang/String;

    move-result-object v5

    .line 109
    invoke-direct {v1, v2}, Lcom/miui/server/enterprise/APNManagerService;->buildNumericSelection(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v6

    const/4 v7, 0x0

    const-string v8, "name ASC"

    .line 102
    invoke-virtual/range {v3 .. v8}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v3

    .line 110
    .local v3, "cursor":Landroid/database/Cursor;
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    move-object v4, v0

    .line 112
    .local v4, "configs":Ljava/util/List;, "Ljava/util/List<Lcom/miui/enterprise/sdk/APNConfig;>;"
    if-eqz v3, :cond_0

    .line 113
    :try_start_0
    const-string v0, "APNManagerMode"

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "Query result size: "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-interface {v3}, Landroid/database/Cursor;->getCount()I

    move-result v6

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v0, v5}, Landroid/util/Slog;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 114
    invoke-interface {v3}, Landroid/database/Cursor;->moveToFirst()Z

    .line 115
    :goto_0
    invoke-interface {v3}, Landroid/database/Cursor;->isAfterLast()Z

    move-result v0

    if-nez v0, :cond_0

    .line 116
    new-instance v0, Lcom/miui/enterprise/sdk/APNConfig;

    invoke-direct {v0}, Lcom/miui/enterprise/sdk/APNConfig;-><init>()V

    .line 117
    .local v0, "configItem":Lcom/miui/enterprise/sdk/APNConfig;
    const/4 v5, 0x0

    invoke-interface {v3, v5}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v5

    iput-object v5, v0, Lcom/miui/enterprise/sdk/APNConfig;->mName:Ljava/lang/String;

    .line 118
    const/4 v5, 0x1

    invoke-interface {v3, v5}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v5

    iput-object v5, v0, Lcom/miui/enterprise/sdk/APNConfig;->mApn:Ljava/lang/String;

    .line 119
    const/4 v5, 0x2

    invoke-interface {v3, v5}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v5

    iput-object v5, v0, Lcom/miui/enterprise/sdk/APNConfig;->mUser:Ljava/lang/String;

    .line 120
    const/4 v5, 0x3

    invoke-interface {v3, v5}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v5

    iput-object v5, v0, Lcom/miui/enterprise/sdk/APNConfig;->mPassword:Ljava/lang/String;

    .line 121
    const/4 v5, 0x4

    invoke-interface {v3, v5}, Landroid/database/Cursor;->getInt(I)I

    move-result v5

    iput v5, v0, Lcom/miui/enterprise/sdk/APNConfig;->mAuthType:I

    .line 122
    const/4 v5, 0x5

    invoke-interface {v3, v5}, Landroid/database/Cursor;->getInt(I)I

    move-result v5

    iput v5, v0, Lcom/miui/enterprise/sdk/APNConfig;->mBearer:I

    .line 123
    const/4 v5, 0x6

    invoke-interface {v3, v5}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v5

    iput-object v5, v0, Lcom/miui/enterprise/sdk/APNConfig;->mMcc:Ljava/lang/String;

    .line 124
    const/4 v5, 0x7

    invoke-interface {v3, v5}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v5

    iput-object v5, v0, Lcom/miui/enterprise/sdk/APNConfig;->mMnc:Ljava/lang/String;

    .line 125
    const/16 v5, 0x8

    invoke-interface {v3, v5}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v5

    iput-object v5, v0, Lcom/miui/enterprise/sdk/APNConfig;->mNumeric:Ljava/lang/String;

    .line 126
    const/16 v5, 0x9

    invoke-interface {v3, v5}, Landroid/database/Cursor;->getInt(I)I

    move-result v5

    iput v5, v0, Lcom/miui/enterprise/sdk/APNConfig;->mCarrier_enabled:I

    .line 127
    const/16 v5, 0xa

    invoke-interface {v3, v5}, Landroid/database/Cursor;->getInt(I)I

    move-result v5

    iput v5, v0, Lcom/miui/enterprise/sdk/APNConfig;->mCurrent:I

    .line 128
    const/16 v5, 0xb

    invoke-interface {v3, v5}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v5

    iput-object v5, v0, Lcom/miui/enterprise/sdk/APNConfig;->mMmsc:Ljava/lang/String;

    .line 129
    const/16 v5, 0xc

    invoke-interface {v3, v5}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v5

    iput-object v5, v0, Lcom/miui/enterprise/sdk/APNConfig;->mMmsport:Ljava/lang/String;

    .line 130
    const/16 v5, 0xd

    invoke-interface {v3, v5}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v5

    iput-object v5, v0, Lcom/miui/enterprise/sdk/APNConfig;->mMmsproxy:Ljava/lang/String;

    .line 131
    const/16 v5, 0xe

    invoke-interface {v3, v5}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v5

    iput-object v5, v0, Lcom/miui/enterprise/sdk/APNConfig;->mMvno_match_data:Ljava/lang/String;

    .line 132
    const/16 v5, 0xf

    invoke-interface {v3, v5}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v5

    iput-object v5, v0, Lcom/miui/enterprise/sdk/APNConfig;->mMvno_type:Ljava/lang/String;

    .line 133
    const/16 v5, 0x10

    invoke-interface {v3, v5}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v5

    iput-object v5, v0, Lcom/miui/enterprise/sdk/APNConfig;->mPort:Ljava/lang/String;

    .line 134
    const/16 v5, 0x11

    invoke-interface {v3, v5}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v5

    iput-object v5, v0, Lcom/miui/enterprise/sdk/APNConfig;->mProtocol:Ljava/lang/String;

    .line 135
    const/16 v5, 0x12

    invoke-interface {v3, v5}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v5

    iput-object v5, v0, Lcom/miui/enterprise/sdk/APNConfig;->mProxy:Ljava/lang/String;

    .line 136
    const/16 v5, 0x13

    invoke-interface {v3, v5}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v5

    iput-object v5, v0, Lcom/miui/enterprise/sdk/APNConfig;->mRoaming_protocol:Ljava/lang/String;

    .line 137
    const/16 v5, 0x14

    invoke-interface {v3, v5}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v5

    iput-object v5, v0, Lcom/miui/enterprise/sdk/APNConfig;->mServer:Ljava/lang/String;

    .line 138
    const/16 v5, 0x15

    invoke-interface {v3, v5}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v5

    iput-object v5, v0, Lcom/miui/enterprise/sdk/APNConfig;->mSub_id:Ljava/lang/String;

    .line 139
    const/16 v5, 0x16

    invoke-interface {v3, v5}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v5

    iput-object v5, v0, Lcom/miui/enterprise/sdk/APNConfig;->mType:Ljava/lang/String;

    .line 140
    invoke-interface {v4, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 141
    invoke-interface {v3}, Landroid/database/Cursor;->moveToNext()Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 142
    nop

    .end local v0    # "configItem":Lcom/miui/enterprise/sdk/APNConfig;
    goto/16 :goto_0

    .line 145
    :catchall_0
    move-exception v0

    invoke-direct {v1, v3}, Lcom/miui/server/enterprise/APNManagerService;->closeCursor(Landroid/database/Cursor;)V

    .line 146
    throw v0

    .line 145
    :cond_0
    invoke-direct {v1, v3}, Lcom/miui/server/enterprise/APNManagerService;->closeCursor(Landroid/database/Cursor;)V

    .line 146
    nop

    .line 147
    return-object v4
.end method

.method public getAPNListForNumeric(Ljava/lang/String;)Ljava/util/List;
    .locals 9
    .param p1, "numeric"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            ")",
            "Ljava/util/List<",
            "Lcom/miui/enterprise/sdk/APNConfig;",
            ">;"
        }
    .end annotation

    .line 71
    iget-object v0, p0, Lcom/miui/server/enterprise/APNManagerService;->mContext:Landroid/content/Context;

    invoke-static {v0}, Lcom/miui/server/enterprise/ServiceUtils;->checkPermission(Landroid/content/Context;)V

    .line 72
    invoke-direct {p0}, Lcom/miui/server/enterprise/APNManagerService;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v1

    sget-object v2, Landroid/provider/Telephony$Carriers;->CONTENT_URI:Landroid/net/Uri;

    const-string v3, "name"

    const-string v4, "apn"

    const-string/jumbo v5, "user"

    const-string v6, "password"

    const-string v7, "authtype"

    const-string v8, "bearer"

    filled-new-array/range {v3 .. v8}, [Ljava/lang/String;

    move-result-object v3

    .line 74
    invoke-direct {p0, p1}, Lcom/miui/server/enterprise/APNManagerService;->buildNumericSelection(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    const/4 v5, 0x0

    const-string v6, "name ASC"

    .line 72
    invoke-virtual/range {v1 .. v6}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v0

    .line 75
    .local v0, "cursor":Landroid/database/Cursor;
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    .line 77
    .local v1, "configs":Ljava/util/List;, "Ljava/util/List<Lcom/miui/enterprise/sdk/APNConfig;>;"
    if-eqz v0, :cond_0

    .line 78
    :try_start_0
    const-string v2, "APNManagerMode"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "Query result size: "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-interface {v0}, Landroid/database/Cursor;->getCount()I

    move-result v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Landroid/util/Slog;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 79
    invoke-interface {v0}, Landroid/database/Cursor;->moveToFirst()Z

    .line 80
    :goto_0
    invoke-interface {v0}, Landroid/database/Cursor;->isAfterLast()Z

    move-result v2

    if-nez v2, :cond_0

    .line 81
    new-instance v2, Lcom/miui/enterprise/sdk/APNConfig;

    invoke-direct {v2}, Lcom/miui/enterprise/sdk/APNConfig;-><init>()V

    .line 82
    .local v2, "configItem":Lcom/miui/enterprise/sdk/APNConfig;
    const/4 v3, 0x0

    invoke-interface {v0, v3}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v3

    iput-object v3, v2, Lcom/miui/enterprise/sdk/APNConfig;->mName:Ljava/lang/String;

    .line 83
    const/4 v3, 0x1

    invoke-interface {v0, v3}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v3

    iput-object v3, v2, Lcom/miui/enterprise/sdk/APNConfig;->mApn:Ljava/lang/String;

    .line 84
    const/4 v3, 0x2

    invoke-interface {v0, v3}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v3

    iput-object v3, v2, Lcom/miui/enterprise/sdk/APNConfig;->mUser:Ljava/lang/String;

    .line 85
    const/4 v3, 0x3

    invoke-interface {v0, v3}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v3

    iput-object v3, v2, Lcom/miui/enterprise/sdk/APNConfig;->mPassword:Ljava/lang/String;

    .line 86
    const/4 v3, 0x4

    invoke-interface {v0, v3}, Landroid/database/Cursor;->getInt(I)I

    move-result v3

    iput v3, v2, Lcom/miui/enterprise/sdk/APNConfig;->mAuthType:I

    .line 87
    const/4 v3, 0x5

    invoke-interface {v0, v3}, Landroid/database/Cursor;->getInt(I)I

    move-result v3

    iput v3, v2, Lcom/miui/enterprise/sdk/APNConfig;->mBearer:I

    .line 88
    invoke-interface {v1, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 89
    invoke-interface {v0}, Landroid/database/Cursor;->moveToNext()Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 90
    nop

    .end local v2    # "configItem":Lcom/miui/enterprise/sdk/APNConfig;
    goto :goto_0

    .line 93
    :catchall_0
    move-exception v2

    invoke-direct {p0, v0}, Lcom/miui/server/enterprise/APNManagerService;->closeCursor(Landroid/database/Cursor;)V

    .line 94
    throw v2

    .line 93
    :cond_0
    invoke-direct {p0, v0}, Lcom/miui/server/enterprise/APNManagerService;->closeCursor(Landroid/database/Cursor;)V

    .line 94
    nop

    .line 95
    return-object v1
.end method

.method public queryApn(Ljava/lang/String;)Ljava/util/List;
    .locals 6
    .param p1, "selections"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            ")",
            "Ljava/util/List<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .line 503
    invoke-direct {p0}, Lcom/miui/server/enterprise/APNManagerService;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    sget-object v1, Landroid/provider/Telephony$Carriers;->CONTENT_URI:Landroid/net/Uri;

    const-string v2, "name"

    filled-new-array {v2}, [Ljava/lang/String;

    move-result-object v2

    const/4 v4, 0x0

    const-string v5, "name ASC"

    move-object v3, p1

    invoke-virtual/range {v0 .. v5}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v0

    .line 505
    .local v0, "cursor":Landroid/database/Cursor;
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    .line 507
    .local v1, "result":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;"
    :goto_0
    if-eqz v0, :cond_0

    :try_start_0
    invoke-interface {v0}, Landroid/database/Cursor;->moveToNext()Z

    move-result v2

    if-eqz v2, :cond_0

    .line 508
    const/4 v2, 0x0

    invoke-interface {v0, v2}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-interface {v1, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto :goto_0

    .line 511
    :catchall_0
    move-exception v2

    invoke-direct {p0, v0}, Lcom/miui/server/enterprise/APNManagerService;->closeCursor(Landroid/database/Cursor;)V

    .line 512
    throw v2

    .line 511
    :cond_0
    invoke-direct {p0, v0}, Lcom/miui/server/enterprise/APNManagerService;->closeCursor(Landroid/database/Cursor;)V

    .line 512
    nop

    .line 513
    return-object v1
.end method

.method public resetAPN()Z
    .locals 3

    .line 483
    iget-object v0, p0, Lcom/miui/server/enterprise/APNManagerService;->mContext:Landroid/content/Context;

    invoke-static {v0}, Lcom/miui/server/enterprise/ServiceUtils;->checkPermission(Landroid/content/Context;)V

    .line 484
    invoke-direct {p0}, Lcom/miui/server/enterprise/APNManagerService;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    .line 485
    .local v0, "resolver":Landroid/content/ContentResolver;
    sget-object v1, Lcom/miui/server/enterprise/APNManagerService;->DEFAULTAPN_URI:Landroid/net/Uri;

    invoke-direct {p0, v1}, Lcom/miui/server/enterprise/APNManagerService;->getUriForCurrSubId(Landroid/net/Uri;)Landroid/net/Uri;

    move-result-object v1

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2, v2}, Landroid/content/ContentResolver;->delete(Landroid/net/Uri;Ljava/lang/String;[Ljava/lang/String;)I

    move-result v1

    .line 486
    .local v1, "result":I
    if-eqz v1, :cond_0

    const/4 v2, 0x1

    goto :goto_0

    :cond_0
    const/4 v2, 0x0

    :goto_0
    return v2
.end method

.method public setAPNActiveMode(I)V
    .locals 3
    .param p1, "mode"    # I

    .line 491
    iget-object v0, p0, Lcom/miui/server/enterprise/APNManagerService;->mContext:Landroid/content/Context;

    invoke-static {v0}, Lcom/miui/server/enterprise/ServiceUtils;->checkPermission(Landroid/content/Context;)V

    .line 492
    iget-object v0, p0, Lcom/miui/server/enterprise/APNManagerService;->mContext:Landroid/content/Context;

    const-string v1, "ep_apn_switch_mode"

    const/4 v2, 0x0

    invoke-static {v0, v1, p1, v2}, Lcom/miui/enterprise/settings/EnterpriseSettings;->putInt(Landroid/content/Context;Ljava/lang/String;II)V

    .line 493
    return-void
.end method
