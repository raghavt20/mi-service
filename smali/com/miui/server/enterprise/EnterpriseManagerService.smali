.class public Lcom/miui/server/enterprise/EnterpriseManagerService;
.super Lcom/miui/enterprise/IEnterpriseManager$Stub;
.source "EnterpriseManagerService.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/miui/server/enterprise/EnterpriseManagerService$Lifecycle;
    }
.end annotation


# static fields
.field private static final TAG:Ljava/lang/String; = "Enterprise"


# instance fields
.field private mAPNManagerService:Lcom/miui/server/enterprise/APNManagerService;

.field private mApplicationManagerService:Lcom/miui/server/enterprise/ApplicationManagerService;

.field private mCert:Lcom/miui/enterprise/signature/EnterpriseCer;

.field private mContext:Landroid/content/Context;

.field private mCurrentUserId:I

.field private mDeviceManagerService:Lcom/miui/server/enterprise/DeviceManagerService;

.field private mLifecycleReceiver:Landroid/content/BroadcastReceiver;

.field private mPhoneManagerService:Lcom/miui/server/enterprise/PhoneManagerService;

.field private mRestrictionsManagerService:Lcom/miui/server/enterprise/RestrictionsManagerService;

.field private mServices:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map<",
            "Ljava/lang/String;",
            "Landroid/os/IBinder;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method static bridge synthetic -$$Nest$fputmCert(Lcom/miui/server/enterprise/EnterpriseManagerService;Lcom/miui/enterprise/signature/EnterpriseCer;)V
    .locals 0

    iput-object p1, p0, Lcom/miui/server/enterprise/EnterpriseManagerService;->mCert:Lcom/miui/enterprise/signature/EnterpriseCer;

    return-void
.end method

.method static bridge synthetic -$$Nest$mbootComplete(Lcom/miui/server/enterprise/EnterpriseManagerService;)V
    .locals 0

    invoke-direct {p0}, Lcom/miui/server/enterprise/EnterpriseManagerService;->bootComplete()V

    return-void
.end method

.method static bridge synthetic -$$Nest$monUserRemoved(Lcom/miui/server/enterprise/EnterpriseManagerService;I)V
    .locals 0

    invoke-direct {p0, p1}, Lcom/miui/server/enterprise/EnterpriseManagerService;->onUserRemoved(I)V

    return-void
.end method

.method static bridge synthetic -$$Nest$monUserStarted(Lcom/miui/server/enterprise/EnterpriseManagerService;I)V
    .locals 0

    invoke-direct {p0, p1}, Lcom/miui/server/enterprise/EnterpriseManagerService;->onUserStarted(I)V

    return-void
.end method

.method static bridge synthetic -$$Nest$monUserSwitched(Lcom/miui/server/enterprise/EnterpriseManagerService;I)V
    .locals 0

    invoke-direct {p0, p1}, Lcom/miui/server/enterprise/EnterpriseManagerService;->onUserSwitched(I)V

    return-void
.end method

.method private constructor <init>(Landroid/content/Context;)V
    .locals 3
    .param p1, "context"    # Landroid/content/Context;

    .line 85
    invoke-direct {p0}, Lcom/miui/enterprise/IEnterpriseManager$Stub;-><init>()V

    .line 28
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    iput-object v0, p0, Lcom/miui/server/enterprise/EnterpriseManagerService;->mServices:Ljava/util/Map;

    .line 48
    const/4 v0, 0x0

    iput v0, p0, Lcom/miui/server/enterprise/EnterpriseManagerService;->mCurrentUserId:I

    .line 55
    new-instance v0, Lcom/miui/server/enterprise/EnterpriseManagerService$1;

    invoke-direct {v0, p0}, Lcom/miui/server/enterprise/EnterpriseManagerService$1;-><init>(Lcom/miui/server/enterprise/EnterpriseManagerService;)V

    iput-object v0, p0, Lcom/miui/server/enterprise/EnterpriseManagerService;->mLifecycleReceiver:Landroid/content/BroadcastReceiver;

    .line 87
    iput-object p1, p0, Lcom/miui/server/enterprise/EnterpriseManagerService;->mContext:Landroid/content/Context;

    .line 88
    new-instance v0, Lcom/miui/server/enterprise/APNManagerService;

    invoke-direct {v0, p1}, Lcom/miui/server/enterprise/APNManagerService;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Lcom/miui/server/enterprise/EnterpriseManagerService;->mAPNManagerService:Lcom/miui/server/enterprise/APNManagerService;

    .line 89
    new-instance v0, Lcom/miui/server/enterprise/ApplicationManagerService;

    invoke-direct {v0, p1}, Lcom/miui/server/enterprise/ApplicationManagerService;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Lcom/miui/server/enterprise/EnterpriseManagerService;->mApplicationManagerService:Lcom/miui/server/enterprise/ApplicationManagerService;

    .line 90
    new-instance v0, Lcom/miui/server/enterprise/DeviceManagerService;

    invoke-direct {v0, p1}, Lcom/miui/server/enterprise/DeviceManagerService;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Lcom/miui/server/enterprise/EnterpriseManagerService;->mDeviceManagerService:Lcom/miui/server/enterprise/DeviceManagerService;

    .line 91
    new-instance v0, Lcom/miui/server/enterprise/PhoneManagerService;

    invoke-direct {v0, p1}, Lcom/miui/server/enterprise/PhoneManagerService;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Lcom/miui/server/enterprise/EnterpriseManagerService;->mPhoneManagerService:Lcom/miui/server/enterprise/PhoneManagerService;

    .line 92
    new-instance v0, Lcom/miui/server/enterprise/RestrictionsManagerService;

    invoke-direct {v0, p1}, Lcom/miui/server/enterprise/RestrictionsManagerService;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Lcom/miui/server/enterprise/EnterpriseManagerService;->mRestrictionsManagerService:Lcom/miui/server/enterprise/RestrictionsManagerService;

    .line 93
    iget-object v0, p0, Lcom/miui/server/enterprise/EnterpriseManagerService;->mServices:Ljava/util/Map;

    const-string v1, "apn_manager"

    iget-object v2, p0, Lcom/miui/server/enterprise/EnterpriseManagerService;->mAPNManagerService:Lcom/miui/server/enterprise/APNManagerService;

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 94
    iget-object v0, p0, Lcom/miui/server/enterprise/EnterpriseManagerService;->mServices:Ljava/util/Map;

    const-string v1, "application_manager"

    iget-object v2, p0, Lcom/miui/server/enterprise/EnterpriseManagerService;->mApplicationManagerService:Lcom/miui/server/enterprise/ApplicationManagerService;

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 95
    iget-object v0, p0, Lcom/miui/server/enterprise/EnterpriseManagerService;->mServices:Ljava/util/Map;

    const-string v1, "device_manager"

    iget-object v2, p0, Lcom/miui/server/enterprise/EnterpriseManagerService;->mDeviceManagerService:Lcom/miui/server/enterprise/DeviceManagerService;

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 96
    iget-object v0, p0, Lcom/miui/server/enterprise/EnterpriseManagerService;->mServices:Ljava/util/Map;

    const-string v1, "phone_manager"

    iget-object v2, p0, Lcom/miui/server/enterprise/EnterpriseManagerService;->mPhoneManagerService:Lcom/miui/server/enterprise/PhoneManagerService;

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 97
    iget-object v0, p0, Lcom/miui/server/enterprise/EnterpriseManagerService;->mServices:Ljava/util/Map;

    const-string v1, "restrictions_manager"

    iget-object v2, p0, Lcom/miui/server/enterprise/EnterpriseManagerService;->mRestrictionsManagerService:Lcom/miui/server/enterprise/RestrictionsManagerService;

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 98
    new-instance v0, Landroid/content/IntentFilter;

    invoke-direct {v0}, Landroid/content/IntentFilter;-><init>()V

    .line 99
    .local v0, "intentFilter":Landroid/content/IntentFilter;
    const-string v1, "android.intent.action.BOOT_COMPLETED"

    invoke-virtual {v0, v1}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    .line 100
    const-string v1, "android.intent.action.USER_STARTED"

    invoke-virtual {v0, v1}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    .line 101
    const-string v1, "android.intent.action.USER_SWITCHED"

    invoke-virtual {v0, v1}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    .line 102
    const-string v1, "android.intent.action.USER_REMOVED"

    invoke-virtual {v0, v1}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    .line 103
    const-string v1, "com.miui.enterprise.ACTION_CERT_UPDATE"

    invoke-virtual {v0, v1}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    .line 104
    iget-object v1, p0, Lcom/miui/server/enterprise/EnterpriseManagerService;->mLifecycleReceiver:Landroid/content/BroadcastReceiver;

    invoke-virtual {p1, v1, v0}, Landroid/content/Context;->registerReceiver(Landroid/content/BroadcastReceiver;Landroid/content/IntentFilter;)Landroid/content/Intent;

    .line 105
    invoke-direct {p0}, Lcom/miui/server/enterprise/EnterpriseManagerService;->loadEnterpriseCert()V

    .line 106
    return-void
.end method

.method synthetic constructor <init>(Landroid/content/Context;Lcom/miui/server/enterprise/EnterpriseManagerService-IA;)V
    .locals 0

    invoke-direct {p0, p1}, Lcom/miui/server/enterprise/EnterpriseManagerService;-><init>(Landroid/content/Context;)V

    return-void
.end method

.method private bootComplete()V
    .locals 1

    .line 109
    iget-object v0, p0, Lcom/miui/server/enterprise/EnterpriseManagerService;->mApplicationManagerService:Lcom/miui/server/enterprise/ApplicationManagerService;

    invoke-virtual {v0}, Lcom/miui/server/enterprise/ApplicationManagerService;->bootComplete()V

    .line 110
    iget-object v0, p0, Lcom/miui/server/enterprise/EnterpriseManagerService;->mRestrictionsManagerService:Lcom/miui/server/enterprise/RestrictionsManagerService;

    invoke-virtual {v0}, Lcom/miui/server/enterprise/RestrictionsManagerService;->bootComplete()V

    .line 111
    return-void
.end method

.method private checkEnterprisePermission()V
    .locals 0

    .line 154
    return-void
.end method

.method private loadEnterpriseCert()V
    .locals 4

    .line 126
    new-instance v0, Ljava/io/File;

    const-string v1, "/data/system/entcert"

    invoke-direct {v0, v1}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 127
    .local v0, "certFile":Ljava/io/File;
    invoke-virtual {v0}, Ljava/io/File;->exists()Z

    move-result v1

    if-nez v1, :cond_0

    .line 128
    return-void

    .line 130
    :cond_0
    :try_start_0
    new-instance v1, Ljava/io/FileInputStream;

    invoke-direct {v1, v0}, Ljava/io/FileInputStream;-><init>(Ljava/io/File;)V
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    .line 131
    .local v1, "inputStream":Ljava/io/InputStream;
    :try_start_1
    new-instance v2, Lcom/miui/enterprise/signature/EnterpriseCer;

    invoke-direct {v2, v1}, Lcom/miui/enterprise/signature/EnterpriseCer;-><init>(Ljava/io/InputStream;)V

    iput-object v2, p0, Lcom/miui/server/enterprise/EnterpriseManagerService;->mCert:Lcom/miui/enterprise/signature/EnterpriseCer;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 132
    :try_start_2
    invoke-virtual {v1}, Ljava/io/InputStream;->close()V
    :try_end_2
    .catch Ljava/io/IOException; {:try_start_2 .. :try_end_2} :catch_0

    .line 134
    .end local v1    # "inputStream":Ljava/io/InputStream;
    goto :goto_1

    .line 130
    .restart local v1    # "inputStream":Ljava/io/InputStream;
    :catchall_0
    move-exception v2

    :try_start_3
    invoke-virtual {v1}, Ljava/io/InputStream;->close()V
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    goto :goto_0

    :catchall_1
    move-exception v3

    :try_start_4
    invoke-virtual {v2, v3}, Ljava/lang/Throwable;->addSuppressed(Ljava/lang/Throwable;)V

    .end local v0    # "certFile":Ljava/io/File;
    .end local p0    # "this":Lcom/miui/server/enterprise/EnterpriseManagerService;
    :goto_0
    throw v2
    :try_end_4
    .catch Ljava/io/IOException; {:try_start_4 .. :try_end_4} :catch_0

    .line 132
    .end local v1    # "inputStream":Ljava/io/InputStream;
    .restart local v0    # "certFile":Ljava/io/File;
    .restart local p0    # "this":Lcom/miui/server/enterprise/EnterpriseManagerService;
    :catch_0
    move-exception v1

    .line 133
    .local v1, "e":Ljava/io/IOException;
    const-string v2, "Enterprise"

    const-string v3, "Something wrong"

    invoke-static {v2, v3, v1}, Landroid/util/Slog;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    .line 135
    .end local v1    # "e":Ljava/io/IOException;
    :goto_1
    return-void
.end method

.method private onUserRemoved(I)V
    .locals 0
    .param p1, "userId"    # I

    .line 123
    return-void
.end method

.method private onUserStarted(I)V
    .locals 0
    .param p1, "userId"    # I

    .line 115
    return-void
.end method

.method private onUserSwitched(I)V
    .locals 0
    .param p1, "userId"    # I

    .line 118
    iput p1, p0, Lcom/miui/server/enterprise/EnterpriseManagerService;->mCurrentUserId:I

    .line 119
    return-void
.end method


# virtual methods
.method public getEnterpriseCert()Lcom/miui/enterprise/signature/EnterpriseCer;
    .locals 1

    .line 142
    iget-object v0, p0, Lcom/miui/server/enterprise/EnterpriseManagerService;->mCert:Lcom/miui/enterprise/signature/EnterpriseCer;

    return-object v0
.end method

.method public getService(Ljava/lang/String;)Landroid/os/IBinder;
    .locals 1
    .param p1, "serviceName"    # Ljava/lang/String;

    .line 147
    invoke-direct {p0}, Lcom/miui/server/enterprise/EnterpriseManagerService;->checkEnterprisePermission()V

    .line 148
    iget-object v0, p0, Lcom/miui/server/enterprise/EnterpriseManagerService;->mServices:Ljava/util/Map;

    invoke-interface {v0, p1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/os/IBinder;

    return-object v0
.end method

.method public isSignatureVerified()Z
    .locals 1

    .line 138
    iget-object v0, p0, Lcom/miui/server/enterprise/EnterpriseManagerService;->mCert:Lcom/miui/enterprise/signature/EnterpriseCer;

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    :goto_0
    return v0
.end method
