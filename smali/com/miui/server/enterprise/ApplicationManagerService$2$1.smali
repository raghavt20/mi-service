.class Lcom/miui/server/enterprise/ApplicationManagerService$2$1;
.super Ljava/lang/Object;
.source "ApplicationManagerService.java"

# interfaces
.implements Ljava/lang/Runnable;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/miui/server/enterprise/ApplicationManagerService$2;->onPackageInstalled(Ljava/lang/String;ILjava/lang/String;Landroid/os/Bundle;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$1:Lcom/miui/server/enterprise/ApplicationManagerService$2;

.field final synthetic val$basePackageName:Ljava/lang/String;


# direct methods
.method constructor <init>(Lcom/miui/server/enterprise/ApplicationManagerService$2;Ljava/lang/String;)V
    .locals 0
    .param p1, "this$1"    # Lcom/miui/server/enterprise/ApplicationManagerService$2;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .line 119
    iput-object p1, p0, Lcom/miui/server/enterprise/ApplicationManagerService$2$1;->this$1:Lcom/miui/server/enterprise/ApplicationManagerService$2;

    iput-object p2, p0, Lcom/miui/server/enterprise/ApplicationManagerService$2$1;->val$basePackageName:Ljava/lang/String;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public run()V
    .locals 5

    .line 122
    const/4 v0, 0x0

    .line 123
    .local v0, "i":I
    :goto_0
    const/4 v1, 0x5

    const-string v2, "Enterprise-App"

    if-ge v0, v1, :cond_0

    .line 126
    :try_start_0
    iget-object v1, p0, Lcom/miui/server/enterprise/ApplicationManagerService$2$1;->this$1:Lcom/miui/server/enterprise/ApplicationManagerService$2;

    iget-object v1, v1, Lcom/miui/server/enterprise/ApplicationManagerService$2;->this$0:Lcom/miui/server/enterprise/ApplicationManagerService;

    invoke-static {v1}, Lcom/miui/server/enterprise/ApplicationManagerService;->-$$Nest$fgetmPMS(Lcom/miui/server/enterprise/ApplicationManagerService;)Lcom/android/server/pm/PackageManagerService$IPackageManagerImpl;

    move-result-object v1

    iget-object v3, p0, Lcom/miui/server/enterprise/ApplicationManagerService$2$1;->val$basePackageName:Ljava/lang/String;

    iget-object v4, p0, Lcom/miui/server/enterprise/ApplicationManagerService$2$1;->this$1:Lcom/miui/server/enterprise/ApplicationManagerService$2;

    iget v4, v4, Lcom/miui/server/enterprise/ApplicationManagerService$2;->val$userId:I

    invoke-virtual {v1, v3, v4}, Lcom/android/server/pm/PackageManagerService$IPackageManagerImpl;->checkPackageStartable(Ljava/lang/String;I)V
    :try_end_0
    .catch Ljava/lang/SecurityException; {:try_start_0 .. :try_end_0} :catch_0

    .line 127
    goto :goto_2

    .line 128
    :catch_0
    move-exception v1

    .line 130
    .local v1, "e":Ljava/lang/SecurityException;
    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "Package "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    iget-object v4, p0, Lcom/miui/server/enterprise/ApplicationManagerService$2$1;->val$basePackageName:Ljava/lang/String;

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, " is still frozen"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Landroid/util/Slog;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 132
    const-wide/16 v2, 0x3e8

    :try_start_1
    invoke-static {v2, v3}, Ljava/lang/Thread;->sleep(J)V
    :try_end_1
    .catch Ljava/lang/InterruptedException; {:try_start_1 .. :try_end_1} :catch_1

    .line 135
    goto :goto_1

    .line 133
    :catch_1
    move-exception v2

    .line 136
    :goto_1
    nop

    .end local v1    # "e":Ljava/lang/SecurityException;
    add-int/lit8 v0, v0, 0x1

    .line 137
    goto :goto_0

    .line 140
    :cond_0
    :goto_2
    :try_start_2
    iget-object v1, p0, Lcom/miui/server/enterprise/ApplicationManagerService$2$1;->this$1:Lcom/miui/server/enterprise/ApplicationManagerService$2;

    iget-object v1, v1, Lcom/miui/server/enterprise/ApplicationManagerService$2;->val$pendingIntent:Landroid/app/PendingIntent;

    invoke-virtual {v1}, Landroid/app/PendingIntent;->send()V

    .line 141
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "Send pending intent: "

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v3, p0, Lcom/miui/server/enterprise/ApplicationManagerService$2$1;->this$1:Lcom/miui/server/enterprise/ApplicationManagerService$2;

    iget-object v3, v3, Lcom/miui/server/enterprise/ApplicationManagerService$2;->val$pendingIntent:Landroid/app/PendingIntent;

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v2, v1}, Landroid/util/Slog;->d(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_2
    .catch Landroid/app/PendingIntent$CanceledException; {:try_start_2 .. :try_end_2} :catch_2

    .line 144
    goto :goto_3

    .line 142
    :catch_2
    move-exception v1

    .line 143
    .local v1, "e":Landroid/app/PendingIntent$CanceledException;
    const-string v3, "Failed to send pending intent"

    invoke-static {v2, v3, v1}, Landroid/util/Slog;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    .line 145
    .end local v1    # "e":Landroid/app/PendingIntent$CanceledException;
    :goto_3
    return-void
.end method
