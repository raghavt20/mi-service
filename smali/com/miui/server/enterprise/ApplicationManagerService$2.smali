.class Lcom/miui/server/enterprise/ApplicationManagerService$2;
.super Lcom/miui/enterprise/sdk/IEpInstallPackageObserver$Stub;
.source "ApplicationManagerService.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/miui/server/enterprise/ApplicationManagerService;->installPackageWithPendingIntent(Ljava/lang/String;Landroid/app/PendingIntent;I)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/miui/server/enterprise/ApplicationManagerService;

.field final synthetic val$pendingIntent:Landroid/app/PendingIntent;

.field final synthetic val$userId:I


# direct methods
.method constructor <init>(Lcom/miui/server/enterprise/ApplicationManagerService;ILandroid/app/PendingIntent;)V
    .locals 0
    .param p1, "this$0"    # Lcom/miui/server/enterprise/ApplicationManagerService;

    .line 110
    iput-object p1, p0, Lcom/miui/server/enterprise/ApplicationManagerService$2;->this$0:Lcom/miui/server/enterprise/ApplicationManagerService;

    iput p2, p0, Lcom/miui/server/enterprise/ApplicationManagerService$2;->val$userId:I

    iput-object p3, p0, Lcom/miui/server/enterprise/ApplicationManagerService$2;->val$pendingIntent:Landroid/app/PendingIntent;

    invoke-direct {p0}, Lcom/miui/enterprise/sdk/IEpInstallPackageObserver$Stub;-><init>()V

    return-void
.end method


# virtual methods
.method public onPackageInstalled(Ljava/lang/String;ILjava/lang/String;Landroid/os/Bundle;)V
    .locals 2
    .param p1, "basePackageName"    # Ljava/lang/String;
    .param p2, "returnCode"    # I
    .param p3, "msg"    # Ljava/lang/String;
    .param p4, "extras"    # Landroid/os/Bundle;

    .line 114
    const/4 v0, 0x1

    if-eq p2, v0, :cond_0

    .line 115
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "Failed to install package: "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", returnCode: "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", msg: "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    const-string v1, "Enterprise-App"

    invoke-static {v1, v0}, Landroid/util/Slog;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 117
    return-void

    .line 119
    :cond_0
    new-instance v0, Ljava/lang/Thread;

    new-instance v1, Lcom/miui/server/enterprise/ApplicationManagerService$2$1;

    invoke-direct {v1, p0, p1}, Lcom/miui/server/enterprise/ApplicationManagerService$2$1;-><init>(Lcom/miui/server/enterprise/ApplicationManagerService$2;Ljava/lang/String;)V

    invoke-direct {v0, v1}, Ljava/lang/Thread;-><init>(Ljava/lang/Runnable;)V

    .line 146
    invoke-virtual {v0}, Ljava/lang/Thread;->start()V

    .line 147
    return-void
.end method
