.class Lcom/miui/server/multisence/MultiSenceDynamicController$2;
.super Landroid/database/ContentObserver;
.source "MultiSenceDynamicController.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/miui/server/multisence/MultiSenceDynamicController;->registerVRSCloudObserver()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/miui/server/multisence/MultiSenceDynamicController;


# direct methods
.method constructor <init>(Lcom/miui/server/multisence/MultiSenceDynamicController;Landroid/os/Handler;)V
    .locals 0
    .param p1, "this$0"    # Lcom/miui/server/multisence/MultiSenceDynamicController;
    .param p2, "handler"    # Landroid/os/Handler;

    .line 129
    iput-object p1, p0, Lcom/miui/server/multisence/MultiSenceDynamicController$2;->this$0:Lcom/miui/server/multisence/MultiSenceDynamicController;

    invoke-direct {p0, p2}, Landroid/database/ContentObserver;-><init>(Landroid/os/Handler;)V

    return-void
.end method


# virtual methods
.method public onChange(ZLandroid/net/Uri;)V
    .locals 4
    .param p1, "selfChange"    # Z
    .param p2, "uri"    # Landroid/net/Uri;

    .line 132
    if-eqz p2, :cond_0

    const-string/jumbo v0, "support_common_vrs_app"

    invoke-static {v0}, Landroid/provider/Settings$System;->getUriFor(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v1

    invoke-virtual {p2, v1}, Landroid/net/Uri;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 133
    iget-object v1, p0, Lcom/miui/server/multisence/MultiSenceDynamicController$2;->this$0:Lcom/miui/server/multisence/MultiSenceDynamicController;

    invoke-static {v1}, Lcom/miui/server/multisence/MultiSenceDynamicController;->-$$Nest$fgetmContext(Lcom/miui/server/multisence/MultiSenceDynamicController;)Landroid/content/Context;

    move-result-object v1

    invoke-virtual {v1}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v1

    const/4 v2, -0x2

    invoke-static {v1, v0, v2}, Landroid/provider/Settings$System;->getStringForUser(Landroid/content/ContentResolver;Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v0

    .line 135
    .local v0, "cloudInfo":Ljava/lang/String;
    sget-boolean v1, Lcom/miui/server/multisence/MultiSenceConfig;->DEBUG_VRS:Z

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v3, "vrs-whitelist from cloud: "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/miui/server/multisence/MultiSenceServiceUtils;->msLogD(ZLjava/lang/String;)V

    .line 136
    invoke-static {}, Lcom/miui/server/multisence/MultiSenceConfig;->getInstance()Lcom/miui/server/multisence/MultiSenceConfig;

    move-result-object v1

    invoke-virtual {v1, v0}, Lcom/miui/server/multisence/MultiSenceConfig;->updateVRSWhiteListAll(Ljava/lang/String;)V

    .line 138
    .end local v0    # "cloudInfo":Ljava/lang/String;
    :cond_0
    return-void
.end method
