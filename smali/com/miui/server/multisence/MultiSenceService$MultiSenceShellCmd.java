class com.miui.server.multisence.MultiSenceService$MultiSenceShellCmd extends android.os.ShellCommand {
	 /* .source "MultiSenceService.java" */
	 /* # annotations */
	 /* .annotation system Ldalvik/annotation/EnclosingClass; */
	 /* value = Lcom/miui/server/multisence/MultiSenceService; */
} // .end annotation
/* .annotation system Ldalvik/annotation/InnerClass; */
/* accessFlags = 0x2 */
/* name = "MultiSenceShellCmd" */
} // .end annotation
/* # instance fields */
com.miui.server.multisence.MultiSenceService mService;
final com.miui.server.multisence.MultiSenceService this$0; //synthetic
/* # direct methods */
public com.miui.server.multisence.MultiSenceService$MultiSenceShellCmd ( ) {
/* .locals 0 */
/* .param p2, "service" # Lcom/miui/server/multisence/MultiSenceService; */
/* .line 468 */
this.this$0 = p1;
/* invoke-direct {p0}, Landroid/os/ShellCommand;-><init>()V */
/* .line 469 */
this.mService = p2;
/* .line 470 */
return;
} // .end method
/* # virtual methods */
public Integer onCommand ( java.lang.String p0 ) {
/* .locals 5 */
/* .param p1, "cmd" # Ljava/lang/String; */
/* .line 474 */
/* if-nez p1, :cond_0 */
/* .line 475 */
v0 = (( com.miui.server.multisence.MultiSenceService$MultiSenceShellCmd ) p0 ).handleDefaultCommands ( p1 ); // invoke-virtual {p0, p1}, Lcom/miui/server/multisence/MultiSenceService$MultiSenceShellCmd;->handleDefaultCommands(Ljava/lang/String;)I
/* .line 477 */
} // :cond_0
(( com.miui.server.multisence.MultiSenceService$MultiSenceShellCmd ) p0 ).getOutPrintWriter ( ); // invoke-virtual {p0}, Lcom/miui/server/multisence/MultiSenceService$MultiSenceShellCmd;->getOutPrintWriter()Ljava/io/PrintWriter;
/* .line 479 */
/* .local v0, "pw":Ljava/io/PrintWriter; */
int v1 = -1; // const/4 v1, -0x1
try { // :try_start_0
v2 = (( java.lang.String ) p1 ).hashCode ( ); // invoke-virtual {p1}, Ljava/lang/String;->hashCode()I
int v3 = 0; // const/4 v3, 0x0
/* sparse-switch v2, :sswitch_data_0 */
} // :cond_1
/* :sswitch_0 */
final String v2 = "help"; // const-string v2, "help"
v2 = (( java.lang.String ) p1 ).equals ( v2 ); // invoke-virtual {p1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z
if ( v2 != null) { // if-eqz v2, :cond_1
/* move v2, v3 */
/* :sswitch_1 */
/* const-string/jumbo v2, "update-status-sub-function" */
v2 = (( java.lang.String ) p1 ).equals ( v2 ); // invoke-virtual {p1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z
if ( v2 != null) { // if-eqz v2, :cond_1
	 int v2 = 1; // const/4 v2, 0x1
	 /* :sswitch_2 */
	 /* const-string/jumbo v2, "update-debug" */
	 v2 = 	 (( java.lang.String ) p1 ).equals ( v2 ); // invoke-virtual {p1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z
	 if ( v2 != null) { // if-eqz v2, :cond_1
		 int v2 = 2; // const/4 v2, 0x2
	 } // :goto_0
	 /* move v2, v1 */
} // :goto_1
/* packed-switch v2, :pswitch_data_0 */
/* .line 490 */
v1 = (( com.miui.server.multisence.MultiSenceService$MultiSenceShellCmd ) p0 ).handleDefaultCommands ( p1 ); // invoke-virtual {p0, p1}, Lcom/miui/server/multisence/MultiSenceService$MultiSenceShellCmd;->handleDefaultCommands(Ljava/lang/String;)I
/* .line 487 */
/* :pswitch_0 */
com.miui.server.multisence.MultiSenceConfig .getInstance ( );
(( com.miui.server.multisence.MultiSenceConfig ) v2 ).updateDebugInfo ( ); // invoke-virtual {v2}, Lcom/miui/server/multisence/MultiSenceConfig;->updateDebugInfo()V
/* .line 488 */
/* .line 484 */
/* :pswitch_1 */
com.miui.server.multisence.MultiSenceConfig .getInstance ( );
(( com.miui.server.multisence.MultiSenceConfig ) v2 ).updateSubFuncStatus ( ); // invoke-virtual {v2}, Lcom/miui/server/multisence/MultiSenceConfig;->updateSubFuncStatus()V
/* .line 485 */
/* .line 481 */
/* :pswitch_2 */
(( com.miui.server.multisence.MultiSenceService$MultiSenceShellCmd ) p0 ).onHelp ( ); // invoke-virtual {p0}, Lcom/miui/server/multisence/MultiSenceService$MultiSenceShellCmd;->onHelp()V
/* :try_end_0 */
/* .catch Ljava/lang/Exception; {:try_start_0 ..:try_end_0} :catch_0 */
/* .line 482 */
/* nop */
/* .line 496 */
} // :goto_2
/* nop */
/* .line 497 */
/* .line 490 */
} // :goto_3
/* .line 492 */
/* :catch_0 */
/* move-exception v2 */
/* .line 493 */
/* .local v2, "e":Ljava/lang/Exception; */
/* new-instance v3, Ljava/lang/StringBuilder; */
/* invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V */
final String v4 = "Error occurred.Check logcat for details."; // const-string v4, "Error occurred.Check logcat for details."
(( java.lang.StringBuilder ) v3 ).append ( v4 ); // invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.Exception ) v2 ).getMessage ( ); // invoke-virtual {v2}, Ljava/lang/Exception;->getMessage()Ljava/lang/String;
(( java.lang.StringBuilder ) v3 ).append ( v4 ); // invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v3 ).toString ( ); // invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
(( java.io.PrintWriter ) v0 ).println ( v3 ); // invoke-virtual {v0, v3}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V
/* .line 494 */
final String v3 = "ShellCommand"; // const-string v3, "ShellCommand"
final String v4 = "Error running shell command"; // const-string v4, "Error running shell command"
android.util.Slog .e ( v3,v4,v2 );
/* .line 495 */
/* :sswitch_data_0 */
/* .sparse-switch */
/* -0x79ca97b1 -> :sswitch_2 */
/* -0x7edc844 -> :sswitch_1 */
/* 0x30cf41 -> :sswitch_0 */
} // .end sparse-switch
/* :pswitch_data_0 */
/* .packed-switch 0x0 */
/* :pswitch_2 */
/* :pswitch_1 */
/* :pswitch_0 */
} // .end packed-switch
} // .end method
public void onHelp ( ) {
/* .locals 2 */
/* .line 502 */
(( com.miui.server.multisence.MultiSenceService$MultiSenceShellCmd ) p0 ).getOutPrintWriter ( ); // invoke-virtual {p0}, Lcom/miui/server/multisence/MultiSenceService$MultiSenceShellCmd;->getOutPrintWriter()Ljava/io/PrintWriter;
/* .line 503 */
/* .local v0, "pw":Ljava/io/PrintWriter; */
final String v1 = "Multi-Sence Service Commands:"; // const-string v1, "Multi-Sence Service Commands:"
(( java.io.PrintWriter ) v0 ).println ( v1 ); // invoke-virtual {v0, v1}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V
/* .line 504 */
final String v1 = " help"; // const-string v1, " help"
(( java.io.PrintWriter ) v0 ).println ( v1 ); // invoke-virtual {v0, v1}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V
/* .line 505 */
return;
} // .end method
