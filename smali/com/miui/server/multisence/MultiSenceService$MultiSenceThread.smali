.class Lcom/miui/server/multisence/MultiSenceService$MultiSenceThread;
.super Lcom/android/server/ServiceThread;
.source "MultiSenceService.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/miui/server/multisence/MultiSenceService;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x8
    name = "MultiSenceThread"
.end annotation


# static fields
.field private static sInstance:Lcom/miui/server/multisence/MultiSenceService$MultiSenceThread;


# direct methods
.method private constructor <init>()V
    .locals 3

    .line 203
    const/4 v0, -0x2

    const/4 v1, 0x1

    const-string v2, "MultiSence"

    invoke-direct {p0, v2, v0, v1}, Lcom/android/server/ServiceThread;-><init>(Ljava/lang/String;IZ)V

    .line 204
    return-void
.end method

.method private static ensureThreadLocked()V
    .locals 1

    .line 207
    sget-object v0, Lcom/miui/server/multisence/MultiSenceService$MultiSenceThread;->sInstance:Lcom/miui/server/multisence/MultiSenceService$MultiSenceThread;

    if-nez v0, :cond_0

    .line 208
    new-instance v0, Lcom/miui/server/multisence/MultiSenceService$MultiSenceThread;

    invoke-direct {v0}, Lcom/miui/server/multisence/MultiSenceService$MultiSenceThread;-><init>()V

    sput-object v0, Lcom/miui/server/multisence/MultiSenceService$MultiSenceThread;->sInstance:Lcom/miui/server/multisence/MultiSenceService$MultiSenceThread;

    .line 209
    invoke-virtual {v0}, Lcom/miui/server/multisence/MultiSenceService$MultiSenceThread;->start()V

    .line 211
    :cond_0
    return-void
.end method

.method public static getInstance()Lcom/miui/server/multisence/MultiSenceService$MultiSenceThread;
    .locals 2

    .line 214
    const-class v0, Lcom/miui/server/multisence/MultiSenceService$MultiSenceThread;

    monitor-enter v0

    .line 215
    :try_start_0
    invoke-static {}, Lcom/miui/server/multisence/MultiSenceService$MultiSenceThread;->ensureThreadLocked()V

    .line 216
    sget-object v1, Lcom/miui/server/multisence/MultiSenceService$MultiSenceThread;->sInstance:Lcom/miui/server/multisence/MultiSenceService$MultiSenceThread;

    monitor-exit v0

    return-object v1

    .line 217
    :catchall_0
    move-exception v1

    monitor-exit v0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v1
.end method
