.class public final enum Lcom/miui/server/multisence/SingleWindowInfo$WindowChangeState;
.super Ljava/lang/Enum;
.source "SingleWindowInfo.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/miui/server/multisence/SingleWindowInfo;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x4019
    name = "WindowChangeState"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum<",
        "Lcom/miui/server/multisence/SingleWindowInfo$WindowChangeState;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[Lcom/miui/server/multisence/SingleWindowInfo$WindowChangeState;

.field public static final enum CHANGED:Lcom/miui/server/multisence/SingleWindowInfo$WindowChangeState;

.field public static final enum NOT_CHANGED:Lcom/miui/server/multisence/SingleWindowInfo$WindowChangeState;

.field public static final enum RESET:Lcom/miui/server/multisence/SingleWindowInfo$WindowChangeState;

.field public static final enum UNKNOWN:Lcom/miui/server/multisence/SingleWindowInfo$WindowChangeState;


# instance fields
.field final id:I


# direct methods
.method private static synthetic $values()[Lcom/miui/server/multisence/SingleWindowInfo$WindowChangeState;
    .locals 4

    .line 215
    sget-object v0, Lcom/miui/server/multisence/SingleWindowInfo$WindowChangeState;->UNKNOWN:Lcom/miui/server/multisence/SingleWindowInfo$WindowChangeState;

    sget-object v1, Lcom/miui/server/multisence/SingleWindowInfo$WindowChangeState;->NOT_CHANGED:Lcom/miui/server/multisence/SingleWindowInfo$WindowChangeState;

    sget-object v2, Lcom/miui/server/multisence/SingleWindowInfo$WindowChangeState;->RESET:Lcom/miui/server/multisence/SingleWindowInfo$WindowChangeState;

    sget-object v3, Lcom/miui/server/multisence/SingleWindowInfo$WindowChangeState;->CHANGED:Lcom/miui/server/multisence/SingleWindowInfo$WindowChangeState;

    filled-new-array {v0, v1, v2, v3}, [Lcom/miui/server/multisence/SingleWindowInfo$WindowChangeState;

    move-result-object v0

    return-object v0
.end method

.method static constructor <clinit>()V
    .locals 4

    .line 216
    new-instance v0, Lcom/miui/server/multisence/SingleWindowInfo$WindowChangeState;

    const/4 v1, -0x1

    const-string v2, "UNKNOWN"

    const/4 v3, 0x0

    invoke-direct {v0, v2, v3, v1}, Lcom/miui/server/multisence/SingleWindowInfo$WindowChangeState;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/miui/server/multisence/SingleWindowInfo$WindowChangeState;->UNKNOWN:Lcom/miui/server/multisence/SingleWindowInfo$WindowChangeState;

    .line 217
    new-instance v0, Lcom/miui/server/multisence/SingleWindowInfo$WindowChangeState;

    const-string v1, "NOT_CHANGED"

    const/4 v2, 0x1

    invoke-direct {v0, v1, v2, v3}, Lcom/miui/server/multisence/SingleWindowInfo$WindowChangeState;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/miui/server/multisence/SingleWindowInfo$WindowChangeState;->NOT_CHANGED:Lcom/miui/server/multisence/SingleWindowInfo$WindowChangeState;

    .line 218
    new-instance v0, Lcom/miui/server/multisence/SingleWindowInfo$WindowChangeState;

    const-string v1, "RESET"

    const/4 v3, 0x2

    invoke-direct {v0, v1, v3, v2}, Lcom/miui/server/multisence/SingleWindowInfo$WindowChangeState;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/miui/server/multisence/SingleWindowInfo$WindowChangeState;->RESET:Lcom/miui/server/multisence/SingleWindowInfo$WindowChangeState;

    .line 219
    new-instance v0, Lcom/miui/server/multisence/SingleWindowInfo$WindowChangeState;

    const-string v1, "CHANGED"

    const/4 v2, 0x3

    invoke-direct {v0, v1, v2, v3}, Lcom/miui/server/multisence/SingleWindowInfo$WindowChangeState;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/miui/server/multisence/SingleWindowInfo$WindowChangeState;->CHANGED:Lcom/miui/server/multisence/SingleWindowInfo$WindowChangeState;

    .line 215
    invoke-static {}, Lcom/miui/server/multisence/SingleWindowInfo$WindowChangeState;->$values()[Lcom/miui/server/multisence/SingleWindowInfo$WindowChangeState;

    move-result-object v0

    sput-object v0, Lcom/miui/server/multisence/SingleWindowInfo$WindowChangeState;->$VALUES:[Lcom/miui/server/multisence/SingleWindowInfo$WindowChangeState;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;II)V
    .locals 0
    .param p3, "id"    # I
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(I)V"
        }
    .end annotation

    .line 221
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    .line 222
    iput p3, p0, Lcom/miui/server/multisence/SingleWindowInfo$WindowChangeState;->id:I

    .line 223
    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/miui/server/multisence/SingleWindowInfo$WindowChangeState;
    .locals 1
    .param p0, "name"    # Ljava/lang/String;

    .line 215
    const-class v0, Lcom/miui/server/multisence/SingleWindowInfo$WindowChangeState;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/miui/server/multisence/SingleWindowInfo$WindowChangeState;

    return-object v0
.end method

.method public static values()[Lcom/miui/server/multisence/SingleWindowInfo$WindowChangeState;
    .locals 1

    .line 215
    sget-object v0, Lcom/miui/server/multisence/SingleWindowInfo$WindowChangeState;->$VALUES:[Lcom/miui/server/multisence/SingleWindowInfo$WindowChangeState;

    invoke-virtual {v0}, [Lcom/miui/server/multisence/SingleWindowInfo$WindowChangeState;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/miui/server/multisence/SingleWindowInfo$WindowChangeState;

    return-object v0
.end method
