.class Lcom/miui/server/multisence/MultiSenceDynamicController$1;
.super Landroid/database/ContentObserver;
.source "MultiSenceDynamicController.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/miui/server/multisence/MultiSenceDynamicController;->registerCloudObserver()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/miui/server/multisence/MultiSenceDynamicController;


# direct methods
.method constructor <init>(Lcom/miui/server/multisence/MultiSenceDynamicController;Landroid/os/Handler;)V
    .locals 0
    .param p1, "this$0"    # Lcom/miui/server/multisence/MultiSenceDynamicController;
    .param p2, "handler"    # Landroid/os/Handler;

    .line 59
    iput-object p1, p0, Lcom/miui/server/multisence/MultiSenceDynamicController$1;->this$0:Lcom/miui/server/multisence/MultiSenceDynamicController;

    invoke-direct {p0, p2}, Landroid/database/ContentObserver;-><init>(Landroid/os/Handler;)V

    return-void
.end method


# virtual methods
.method public onChange(ZLandroid/net/Uri;)V
    .locals 8
    .param p1, "selfChange"    # Z
    .param p2, "uri"    # Landroid/net/Uri;

    .line 62
    const-string v0, "persist.multisence.nonsupport"

    sget-boolean v1, Lcom/miui/server/multisence/MultiSenceConfig;->DEBUG_DYNAMIC_CONTROLLER:Z

    const-string v2, "onChange"

    invoke-static {v1, v2}, Lcom/miui/server/multisence/MultiSenceServiceUtils;->msLogD(ZLjava/lang/String;)V

    .line 63
    const/4 v1, -0x2

    if-eqz p2, :cond_1

    const-string v2, "cloud_multisence_enable"

    invoke-static {v2}, Landroid/provider/Settings$System;->getUriFor(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v3

    invoke-virtual {p2, v3}, Landroid/net/Uri;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_1

    .line 64
    iget-object v3, p0, Lcom/miui/server/multisence/MultiSenceDynamicController$1;->this$0:Lcom/miui/server/multisence/MultiSenceDynamicController;

    invoke-static {v3}, Lcom/miui/server/multisence/MultiSenceDynamicController;->-$$Nest$fgetmContext(Lcom/miui/server/multisence/MultiSenceDynamicController;)Landroid/content/Context;

    move-result-object v3

    invoke-virtual {v3}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v3

    invoke-static {v3, v2, v1}, Landroid/provider/Settings$System;->getStringForUser(Landroid/content/ContentResolver;Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Ljava/lang/Boolean;->parseBoolean(Ljava/lang/String;)Z

    move-result v2

    sput-boolean v2, Lcom/miui/server/multisence/MultiSenceDynamicController;->IS_CLOUD_ENABLED:Z

    .line 66
    sget-boolean v2, Lcom/miui/server/multisence/MultiSenceDynamicController;->IS_CLOUD_ENABLED:Z

    invoke-static {v2}, Ljava/lang/String;->valueOf(Z)Ljava/lang/String;

    move-result-object v2

    const-string v3, "persist.multisence.cloud.enable"

    invoke-static {v3, v2}, Landroid/os/SystemProperties;->set(Ljava/lang/String;Ljava/lang/String;)V

    .line 67
    iget-object v2, p0, Lcom/miui/server/multisence/MultiSenceDynamicController$1;->this$0:Lcom/miui/server/multisence/MultiSenceDynamicController;

    invoke-static {v2}, Lcom/miui/server/multisence/MultiSenceDynamicController;->-$$Nest$fgetTAG(Lcom/miui/server/multisence/MultiSenceDynamicController;)Ljava/lang/String;

    move-result-object v2

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "cloud control set received: "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    sget-boolean v4, Lcom/miui/server/multisence/MultiSenceDynamicController;->IS_CLOUD_ENABLED:Z

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Landroid/util/Slog;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 69
    sget-boolean v2, Lcom/miui/server/multisence/MultiSenceDynamicController;->IS_CLOUD_ENABLED:Z

    if-nez v2, :cond_0

    .line 70
    iget-object v2, p0, Lcom/miui/server/multisence/MultiSenceDynamicController$1;->this$0:Lcom/miui/server/multisence/MultiSenceDynamicController;

    invoke-static {v2}, Lcom/miui/server/multisence/MultiSenceDynamicController;->-$$Nest$fgetTAG(Lcom/miui/server/multisence/MultiSenceDynamicController;)Ljava/lang/String;

    move-result-object v2

    const-string/jumbo v3, "unregister listner due to cloud controller"

    invoke-static {v2, v3}, Landroid/util/Slog;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 71
    iget-object v2, p0, Lcom/miui/server/multisence/MultiSenceDynamicController$1;->this$0:Lcom/miui/server/multisence/MultiSenceDynamicController;

    invoke-static {v2}, Lcom/miui/server/multisence/MultiSenceDynamicController;->-$$Nest$fgetmService(Lcom/miui/server/multisence/MultiSenceDynamicController;)Lcom/miui/server/multisence/MultiSenceService;

    move-result-object v2

    invoke-virtual {v2}, Lcom/miui/server/multisence/MultiSenceService;->unregisterMultiTaskActionListener()V

    .line 72
    iget-object v2, p0, Lcom/miui/server/multisence/MultiSenceDynamicController$1;->this$0:Lcom/miui/server/multisence/MultiSenceDynamicController;

    invoke-static {v2}, Lcom/miui/server/multisence/MultiSenceDynamicController;->-$$Nest$fgetmService(Lcom/miui/server/multisence/MultiSenceDynamicController;)Lcom/miui/server/multisence/MultiSenceService;

    move-result-object v2

    invoke-virtual {v2}, Lcom/miui/server/multisence/MultiSenceService;->clearServiceStatus()V

    goto :goto_0

    .line 74
    :cond_0
    iget-object v2, p0, Lcom/miui/server/multisence/MultiSenceDynamicController$1;->this$0:Lcom/miui/server/multisence/MultiSenceDynamicController;

    invoke-static {v2}, Lcom/miui/server/multisence/MultiSenceDynamicController;->-$$Nest$fgetTAG(Lcom/miui/server/multisence/MultiSenceDynamicController;)Ljava/lang/String;

    move-result-object v2

    const-string v3, "register listner due to cloud controller"

    invoke-static {v2, v3}, Landroid/util/Slog;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 75
    iget-object v2, p0, Lcom/miui/server/multisence/MultiSenceDynamicController$1;->this$0:Lcom/miui/server/multisence/MultiSenceDynamicController;

    invoke-static {v2}, Lcom/miui/server/multisence/MultiSenceDynamicController;->-$$Nest$fgetmService(Lcom/miui/server/multisence/MultiSenceDynamicController;)Lcom/miui/server/multisence/MultiSenceService;

    move-result-object v2

    invoke-virtual {v2}, Lcom/miui/server/multisence/MultiSenceService;->registerMultiTaskActionListener()V

    .line 77
    sget-boolean v2, Lcom/miui/server/multisence/MultiSenceDynamicController;->IS_CLOUD_ENABLED:Z

    invoke-static {v2}, Ljava/lang/String;->valueOf(Z)Ljava/lang/String;

    move-result-object v2

    const-string v3, "persist.multisence.enable"

    invoke-static {v3, v2}, Landroid/os/SystemProperties;->set(Ljava/lang/String;Ljava/lang/String;)V

    .line 78
    iget-object v2, p0, Lcom/miui/server/multisence/MultiSenceDynamicController$1;->this$0:Lcom/miui/server/multisence/MultiSenceDynamicController;

    invoke-static {v2}, Lcom/miui/server/multisence/MultiSenceDynamicController;->-$$Nest$fgetmService(Lcom/miui/server/multisence/MultiSenceDynamicController;)Lcom/miui/server/multisence/MultiSenceService;

    move-result-object v2

    sget-boolean v3, Lcom/miui/server/multisence/MultiSenceDynamicController;->IS_CLOUD_ENABLED:Z

    invoke-virtual {v2, v3}, Lcom/miui/server/multisence/MultiSenceService;->notifyMultiSenceEnable(Z)V

    .line 80
    :goto_0
    invoke-static {}, Lcom/miui/server/multisence/MultiSenceConfig;->getInstance()Lcom/miui/server/multisence/MultiSenceConfig;

    move-result-object v2

    invoke-virtual {v2}, Lcom/miui/server/multisence/MultiSenceConfig;->updateSubFuncStatus()V

    .line 83
    :cond_1
    const/4 v2, 0x0

    if-eqz p2, :cond_6

    const-string v3, "POWER_PERFORMANCE_MODE_OPEN"

    invoke-static {v3}, Landroid/provider/Settings$System;->getUriFor(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v4

    invoke-virtual {p2, v4}, Landroid/net/Uri;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_6

    .line 85
    :try_start_0
    iget-object v4, p0, Lcom/miui/server/multisence/MultiSenceDynamicController$1;->this$0:Lcom/miui/server/multisence/MultiSenceDynamicController;

    invoke-static {v4}, Lcom/miui/server/multisence/MultiSenceDynamicController;->-$$Nest$fgetmContext(Lcom/miui/server/multisence/MultiSenceDynamicController;)Landroid/content/Context;

    move-result-object v4

    .line 86
    invoke-virtual {v4}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v4

    .line 85
    invoke-static {v4, v3, v2, v1}, Landroid/provider/Settings$System;->getIntForUser(Landroid/content/ContentResolver;Ljava/lang/String;II)I

    move-result v3

    const/4 v4, 0x1

    if-eqz v3, :cond_2

    move v3, v4

    goto :goto_1

    :cond_2
    move v3, v2

    .line 87
    .local v3, "isPowerMode":Z
    :goto_1
    iget-object v5, p0, Lcom/miui/server/multisence/MultiSenceDynamicController$1;->this$0:Lcom/miui/server/multisence/MultiSenceDynamicController;

    invoke-static {v5}, Lcom/miui/server/multisence/MultiSenceDynamicController;->-$$Nest$fgetmService(Lcom/miui/server/multisence/MultiSenceDynamicController;)Lcom/miui/server/multisence/MultiSenceService;

    move-result-object v5

    invoke-virtual {v5}, Lcom/miui/server/multisence/MultiSenceService;->getPolicy()Lcom/miui/server/multisence/MultiSenceServicePolicy;

    move-result-object v5

    if-eqz v5, :cond_4

    .line 88
    iget-object v5, p0, Lcom/miui/server/multisence/MultiSenceDynamicController$1;->this$0:Lcom/miui/server/multisence/MultiSenceDynamicController;

    invoke-static {v5}, Lcom/miui/server/multisence/MultiSenceDynamicController;->-$$Nest$fgetmService(Lcom/miui/server/multisence/MultiSenceDynamicController;)Lcom/miui/server/multisence/MultiSenceService;

    move-result-object v5

    invoke-virtual {v5}, Lcom/miui/server/multisence/MultiSenceService;->getPolicy()Lcom/miui/server/multisence/MultiSenceServicePolicy;

    move-result-object v5

    if-nez v3, :cond_3

    move v6, v4

    goto :goto_2

    :cond_3
    move v6, v2

    :goto_2
    invoke-virtual {v5, v6}, Lcom/miui/server/multisence/MultiSenceServicePolicy;->updatePowerMode(Z)V

    .line 90
    :cond_4
    sget-boolean v5, Lcom/miui/server/multisence/MultiSenceConfig;->DEBUG_DYNAMIC_CONTROLLER:Z

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v7, "updatePowerMode isPowerMode: "

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    if-nez v3, :cond_5

    goto :goto_3

    :cond_5
    move v4, v2

    :goto_3
    invoke-virtual {v6, v4}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v5, v4}, Lcom/miui/server/multisence/MultiSenceServiceUtils;->msLogD(ZLjava/lang/String;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 93
    .end local v3    # "isPowerMode":Z
    goto :goto_4

    .line 91
    :catch_0
    move-exception v3

    .line 92
    .local v3, "e":Ljava/lang/Exception;
    iget-object v4, p0, Lcom/miui/server/multisence/MultiSenceDynamicController$1;->this$0:Lcom/miui/server/multisence/MultiSenceDynamicController;

    invoke-static {v4}, Lcom/miui/server/multisence/MultiSenceDynamicController;->-$$Nest$fgetTAG(Lcom/miui/server/multisence/MultiSenceDynamicController;)Ljava/lang/String;

    move-result-object v4

    const-string/jumbo v5, "updatePowerModeParas fail"

    invoke-static {v4, v5}, Landroid/util/Slog;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 96
    .end local v3    # "e":Ljava/lang/Exception;
    :cond_6
    :goto_4
    if-eqz p2, :cond_8

    const-string v3, "cloud_multisence_subfunc_nonsupport"

    invoke-static {v3}, Landroid/provider/Settings$System;->getUriFor(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v4

    invoke-virtual {p2, v4}, Landroid/net/Uri;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_8

    .line 98
    :try_start_1
    iget-object v4, p0, Lcom/miui/server/multisence/MultiSenceDynamicController$1;->this$0:Lcom/miui/server/multisence/MultiSenceDynamicController;

    invoke-static {v4}, Lcom/miui/server/multisence/MultiSenceDynamicController;->-$$Nest$fgetmContext(Lcom/miui/server/multisence/MultiSenceDynamicController;)Landroid/content/Context;

    move-result-object v4

    invoke-virtual {v4}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v4

    invoke-static {v4, v3, v1}, Landroid/provider/Settings$System;->getStringForUser(Landroid/content/ContentResolver;Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v1

    .line 100
    .local v1, "nonSupportCode":I
    invoke-static {v0, v2}, Landroid/os/SystemProperties;->getInt(Ljava/lang/String;I)I

    move-result v2

    .line 101
    .local v2, "oldNonSupportCode":I
    if-eq v1, v2, :cond_7

    .line 102
    invoke-static {v1}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v3

    invoke-static {v0, v3}, Landroid/os/SystemProperties;->set(Ljava/lang/String;Ljava/lang/String;)V

    .line 103
    invoke-static {}, Lcom/miui/server/multisence/MultiSenceConfig;->getInstance()Lcom/miui/server/multisence/MultiSenceConfig;

    move-result-object v0

    invoke-virtual {v0}, Lcom/miui/server/multisence/MultiSenceConfig;->updateSubFuncStatus()V

    goto :goto_5

    .line 105
    :cond_7
    sget-boolean v0, Lcom/miui/server/multisence/MultiSenceConfig;->DEBUG_DYNAMIC_CONTROLLER:Z

    const-string/jumbo v3, "sub-function is not changed."

    invoke-static {v0, v3}, Lcom/miui/server/multisence/MultiSenceServiceUtils;->msLogD(ZLjava/lang/String;)V
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_1

    .line 109
    .end local v1    # "nonSupportCode":I
    .end local v2    # "oldNonSupportCode":I
    :goto_5
    goto :goto_6

    .line 107
    :catch_1
    move-exception v0

    .line 108
    .local v0, "e":Ljava/lang/Exception;
    iget-object v1, p0, Lcom/miui/server/multisence/MultiSenceDynamicController$1;->this$0:Lcom/miui/server/multisence/MultiSenceDynamicController;

    invoke-static {v1}, Lcom/miui/server/multisence/MultiSenceDynamicController;->-$$Nest$fgetTAG(Lcom/miui/server/multisence/MultiSenceDynamicController;)Ljava/lang/String;

    move-result-object v1

    const-string/jumbo v2, "update sub-function nonSupport parameter fail."

    invoke-static {v1, v2}, Landroid/util/Slog;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 112
    .end local v0    # "e":Ljava/lang/Exception;
    :cond_8
    :goto_6
    return-void
.end method
