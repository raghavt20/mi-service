public class com.miui.server.multisence.SingleWindowInfo$FreeFormInfo {
	 /* .source "SingleWindowInfo.java" */
	 /* # annotations */
	 /* .annotation system Ldalvik/annotation/EnclosingClass; */
	 /* value = Lcom/miui/server/multisence/SingleWindowInfo; */
} // .end annotation
/* .annotation system Ldalvik/annotation/InnerClass; */
/* accessFlags = 0x9 */
/* name = "FreeFormInfo" */
} // .end annotation
/* # instance fields */
public Integer mPid;
public android.graphics.Rect mRect;
public com.miui.server.multisence.SingleWindowInfo$WindowForm mWindowForm;
/* # direct methods */
public com.miui.server.multisence.SingleWindowInfo$FreeFormInfo ( ) {
/* .locals 0 */
/* .param p1, "windowForm" # Lcom/miui/server/multisence/SingleWindowInfo$WindowForm; */
/* .param p2, "rect" # Landroid/graphics/Rect; */
/* .param p3, "pid" # I */
/* .line 308 */
/* invoke-direct {p0}, Ljava/lang/Object;-><init>()V */
/* .line 309 */
this.mWindowForm = p1;
/* .line 310 */
this.mRect = p2;
/* .line 311 */
/* iput p3, p0, Lcom/miui/server/multisence/SingleWindowInfo$FreeFormInfo;->mPid:I */
/* .line 312 */
return;
} // .end method
