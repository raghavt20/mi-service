class com.miui.server.multisence.MultiSenceService$MultiSenceThread extends com.android.server.ServiceThread {
	 /* .source "MultiSenceService.java" */
	 /* # annotations */
	 /* .annotation system Ldalvik/annotation/EnclosingClass; */
	 /* value = Lcom/miui/server/multisence/MultiSenceService; */
} // .end annotation
/* .annotation system Ldalvik/annotation/InnerClass; */
/* accessFlags = 0x8 */
/* name = "MultiSenceThread" */
} // .end annotation
/* # static fields */
private static com.miui.server.multisence.MultiSenceService$MultiSenceThread sInstance;
/* # direct methods */
private com.miui.server.multisence.MultiSenceService$MultiSenceThread ( ) {
/* .locals 3 */
/* .line 203 */
int v0 = -2; // const/4 v0, -0x2
int v1 = 1; // const/4 v1, 0x1
final String v2 = "MultiSence"; // const-string v2, "MultiSence"
/* invoke-direct {p0, v2, v0, v1}, Lcom/android/server/ServiceThread;-><init>(Ljava/lang/String;IZ)V */
/* .line 204 */
return;
} // .end method
private static void ensureThreadLocked ( ) {
/* .locals 1 */
/* .line 207 */
v0 = com.miui.server.multisence.MultiSenceService$MultiSenceThread.sInstance;
/* if-nez v0, :cond_0 */
/* .line 208 */
/* new-instance v0, Lcom/miui/server/multisence/MultiSenceService$MultiSenceThread; */
/* invoke-direct {v0}, Lcom/miui/server/multisence/MultiSenceService$MultiSenceThread;-><init>()V */
/* .line 209 */
(( com.miui.server.multisence.MultiSenceService$MultiSenceThread ) v0 ).start ( ); // invoke-virtual {v0}, Lcom/miui/server/multisence/MultiSenceService$MultiSenceThread;->start()V
/* .line 211 */
} // :cond_0
return;
} // .end method
public static com.miui.server.multisence.MultiSenceService$MultiSenceThread getInstance ( ) {
/* .locals 2 */
/* .line 214 */
/* const-class v0, Lcom/miui/server/multisence/MultiSenceService$MultiSenceThread; */
/* monitor-enter v0 */
/* .line 215 */
try { // :try_start_0
com.miui.server.multisence.MultiSenceService$MultiSenceThread .ensureThreadLocked ( );
/* .line 216 */
v1 = com.miui.server.multisence.MultiSenceService$MultiSenceThread.sInstance;
/* monitor-exit v0 */
/* .line 217 */
/* :catchall_0 */
/* move-exception v1 */
/* monitor-exit v0 */
/* :try_end_0 */
/* .catchall {:try_start_0 ..:try_end_0} :catchall_0 */
/* throw v1 */
} // .end method
