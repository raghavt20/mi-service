public class com.miui.server.multisence.MultiSenceConfig {
	 /* .source "MultiSenceConfig.java" */
	 /* # annotations */
	 /* .annotation system Ldalvik/annotation/MemberClasses; */
	 /* value = { */
	 /* Lcom/miui/server/multisence/MultiSenceConfig$MultiSenceConfigHolder; */
	 /* } */
} // .end annotation
/* # static fields */
public static Boolean DEBUG;
private static final Integer DEBUG_BIT_CONFIG;
private static final Integer DEBUG_BIT_DYNAMIC_CONTROLLER;
private static final Integer DEBUG_BIT_DYN_FPS;
private static final Integer DEBUG_BIT_FW;
private static final Integer DEBUG_BIT_POLICY_BASE;
private static final Integer DEBUG_BIT_TRACE;
private static final Integer DEBUG_BIT_VRS;
public static Boolean DEBUG_CONFIG;
public static Boolean DEBUG_DYNAMIC_CONTROLLER;
public static Boolean DEBUG_DYN_FPS;
public static Boolean DEBUG_FW;
public static Boolean DEBUG_POLICY_BASE;
public static Boolean DEBUG_TRACE;
public static Boolean DEBUG_VRS;
public static Boolean DYN_FPS_ENABLED;
public static Boolean FREMAPREDICT_ENABLE;
public static Boolean FW_ENABLE;
public static Boolean MULTITASK_ANIM_SCHED_ENABLED;
private static final Integer NONSUPPORT_DYNAMIC_FPS;
private static final Integer NONSUPPORT_FRAMEPREDICT;
private static final Integer NONSUPPORT_FW;
private static final Integer NONSUPPORT_MULTITASK_ANIM_SCHED;
private static final Integer NONSUPPORT_POLICY_WINDOWSIZE;
private static final Integer NONSUPPORT_VRS;
public static Boolean POLICY_WINDOWSIZE_ENABLE;
public static final java.lang.String PROPERTY_PREFIX;
public static final java.lang.String PROP_MULTISENCE_ENABLE;
public static final java.lang.String PROP_SUB_FUNC_NONSUPPORT;
public static final Boolean SERVICE_ENABLED;
public static final java.lang.String SERVICE_JAVA_NAME;
private static Integer SUB_FUNC_NONSUPPORT;
private static final java.lang.String TAG;
public static Boolean VRS_ENABLE;
/* # instance fields */
public java.util.Set floatingWindowWhiteList;
/* .annotation system Ldalvik/annotation/Signature; */
/* value = { */
/* "Ljava/util/Set<", */
/* "Ljava/lang/String;", */
/* ">;" */
/* } */
} // .end annotation
} // .end field
private android.content.Context mContext;
private Boolean mReady;
com.miui.server.multisence.MultiSenceService mService;
private java.util.Set mWhiteListVRS;
/* .annotation system Ldalvik/annotation/Signature; */
/* value = { */
/* "Ljava/util/Set<", */
/* "Ljava/lang/String;", */
/* ">;" */
/* } */
} // .end annotation
} // .end field
/* # direct methods */
static com.miui.server.multisence.MultiSenceConfig ( ) {
/* .locals 3 */
/* .line 31 */
final String v0 = "persist.multisence.nonsupport"; // const-string v0, "persist.multisence.nonsupport"
int v1 = 0; // const/4 v1, 0x0
v0 = android.os.SystemProperties .getInt ( v0,v1 );
/* .line 46 */
final String v0 = "persist.multisence.enable"; // const-string v0, "persist.multisence.enable"
int v2 = 1; // const/4 v2, 0x1
v0 = android.os.SystemProperties .getBoolean ( v0,v2 );
com.miui.server.multisence.MultiSenceConfig.SERVICE_ENABLED = (v0!= 0);
/* .line 48 */
com.miui.server.multisence.MultiSenceConfig.DYN_FPS_ENABLED = (v1!= 0);
/* .line 50 */
com.miui.server.multisence.MultiSenceConfig.MULTITASK_ANIM_SCHED_ENABLED = (v1!= 0);
/* .line 52 */
com.miui.server.multisence.MultiSenceConfig.FREMAPREDICT_ENABLE = (v1!= 0);
/* .line 54 */
com.miui.server.multisence.MultiSenceConfig.VRS_ENABLE = (v1!= 0);
/* .line 56 */
com.miui.server.multisence.MultiSenceConfig.POLICY_WINDOWSIZE_ENABLE = (v1!= 0);
/* .line 58 */
com.miui.server.multisence.MultiSenceConfig.FW_ENABLE = (v1!= 0);
/* .line 60 */
final String v0 = "persist.multisence.debug.on"; // const-string v0, "persist.multisence.debug.on"
v0 = android.os.SystemProperties .getBoolean ( v0,v1 );
com.miui.server.multisence.MultiSenceConfig.DEBUG = (v0!= 0);
/* .line 77 */
com.miui.server.multisence.MultiSenceConfig.DEBUG_POLICY_BASE = (v1!= 0);
/* .line 79 */
com.miui.server.multisence.MultiSenceConfig.DEBUG_DYN_FPS = (v1!= 0);
/* .line 81 */
com.miui.server.multisence.MultiSenceConfig.DEBUG_VRS = (v1!= 0);
/* .line 83 */
com.miui.server.multisence.MultiSenceConfig.DEBUG_CONFIG = (v1!= 0);
/* .line 85 */
com.miui.server.multisence.MultiSenceConfig.DEBUG_DYNAMIC_CONTROLLER = (v1!= 0);
/* .line 87 */
com.miui.server.multisence.MultiSenceConfig.DEBUG_TRACE = (v1!= 0);
/* .line 89 */
com.miui.server.multisence.MultiSenceConfig.DEBUG_FW = (v1!= 0);
return;
} // .end method
private com.miui.server.multisence.MultiSenceConfig ( ) {
/* .locals 1 */
/* .line 99 */
/* invoke-direct {p0}, Ljava/lang/Object;-><init>()V */
/* .line 28 */
int v0 = 0; // const/4 v0, 0x0
/* iput-boolean v0, p0, Lcom/miui/server/multisence/MultiSenceConfig;->mReady:Z */
/* .line 94 */
/* new-instance v0, Ljava/util/HashSet; */
/* invoke-direct {v0}, Ljava/util/HashSet;-><init>()V */
this.mWhiteListVRS = v0;
/* .line 96 */
/* new-instance v0, Ljava/util/HashSet; */
/* invoke-direct {v0}, Ljava/util/HashSet;-><init>()V */
this.floatingWindowWhiteList = v0;
/* .line 99 */
return;
} // .end method
 com.miui.server.multisence.MultiSenceConfig ( ) { //synthethic
/* .locals 0 */
/* invoke-direct {p0}, Lcom/miui/server/multisence/MultiSenceConfig;-><init>()V */
return;
} // .end method
public static com.miui.server.multisence.MultiSenceConfig getInstance ( ) {
/* .locals 1 */
/* .line 106 */
com.miui.server.multisence.MultiSenceConfig$MultiSenceConfigHolder .-$$Nest$sfgetINSTANCE ( );
} // .end method
/* # virtual methods */
public Boolean checkVrsSupport ( java.lang.String p0 ) {
/* .locals 2 */
/* .param p1, "name" # Ljava/lang/String; */
/* .line 211 */
int v0 = 0; // const/4 v0, 0x0
/* .line 212 */
/* .local v0, "ret":Z */
v1 = v1 = this.mWhiteListVRS;
if ( v1 != null) { // if-eqz v1, :cond_0
/* .line 213 */
int v0 = 1; // const/4 v0, 0x1
/* .line 215 */
} // :cond_0
} // .end method
public void dumpConfig ( java.io.PrintWriter p0 ) {
/* .locals 2 */
/* .param p1, "pw" # Ljava/io/PrintWriter; */
/* .line 259 */
/* new-instance v0, Ljava/lang/StringBuilder; */
final String v1 = "Config:\n"; // const-string v1, "Config:\n"
/* invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V */
/* .line 260 */
/* .local v0, "details":Ljava/lang/StringBuilder; */
(( com.miui.server.multisence.MultiSenceConfig ) p0 ).showSubFunctionStatus ( ); // invoke-virtual {p0}, Lcom/miui/server/multisence/MultiSenceConfig;->showSubFunctionStatus()Ljava/lang/String;
(( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
/* .line 261 */
(( java.io.PrintWriter ) p1 ).print ( v0 ); // invoke-virtual {p1, v0}, Ljava/io/PrintWriter;->print(Ljava/lang/Object;)V
/* .line 262 */
return;
} // .end method
public void init ( com.miui.server.multisence.MultiSenceService p0, android.content.Context p1 ) {
/* .locals 1 */
/* .param p1, "service" # Lcom/miui/server/multisence/MultiSenceService; */
/* .param p2, "context" # Landroid/content/Context; */
/* .line 110 */
this.mService = p1;
/* .line 111 */
this.mContext = p2;
/* .line 112 */
(( com.miui.server.multisence.MultiSenceConfig ) p0 ).updateDebugInfo ( ); // invoke-virtual {p0}, Lcom/miui/server/multisence/MultiSenceConfig;->updateDebugInfo()V
/* .line 113 */
v0 = this.mService;
if ( v0 != null) { // if-eqz v0, :cond_0
/* .line 114 */
int v0 = 1; // const/4 v0, 0x1
/* iput-boolean v0, p0, Lcom/miui/server/multisence/MultiSenceConfig;->mReady:Z */
/* .line 116 */
} // :cond_0
return;
} // .end method
public void initFWWhiteList ( ) {
/* .locals 5 */
/* .line 219 */
v0 = this.mContext;
/* if-nez v0, :cond_0 */
return;
/* .line 221 */
} // :cond_0
(( android.content.Context ) v0 ).getContentResolver ( ); // invoke-virtual {v0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;
/* const-string/jumbo v1, "support_fw_app" */
int v2 = -2; // const/4 v2, -0x2
android.provider.Settings$System .getStringForUser ( v0,v1,v2 );
/* .line 223 */
/* .local v0, "cloudInfo":Ljava/lang/String; */
if ( v0 != null) { // if-eqz v0, :cond_2
v1 = (( java.lang.String ) v0 ).length ( ); // invoke-virtual {v0}, Ljava/lang/String;->length()I
/* if-nez v1, :cond_1 */
/* .line 228 */
} // :cond_1
(( com.miui.server.multisence.MultiSenceConfig ) p0 ).updateFWWhiteListAll ( v0 ); // invoke-virtual {p0, v0}, Lcom/miui/server/multisence/MultiSenceConfig;->updateFWWhiteListAll(Ljava/lang/String;)V
/* .line 224 */
} // :cond_2
} // :goto_0
v1 = this.mContext;
(( android.content.Context ) v1 ).getResources ( ); // invoke-virtual {v1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;
/* const v2, 0x11030071 */
(( android.content.res.Resources ) v1 ).getStringArray ( v2 ); // invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getStringArray(I)[Ljava/lang/String;
/* .line 225 */
/* .local v1, "fwPackageNames":[Ljava/lang/String; */
v2 = this.floatingWindowWhiteList;
java.util.Arrays .asList ( v1 );
/* .line 226 */
/* sget-boolean v2, Lcom/miui/server/multisence/MultiSenceConfig;->DEBUG_FW:Z */
/* new-instance v3, Ljava/lang/StringBuilder; */
/* invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V */
final String v4 = "local floating window whitelist: "; // const-string v4, "local floating window whitelist: "
(( java.lang.StringBuilder ) v3 ).append ( v4 ); // invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
v4 = this.floatingWindowWhiteList;
(( java.lang.StringBuilder ) v3 ).append ( v4 ); // invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v3 ).toString ( ); // invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
com.miui.server.multisence.MultiSenceServiceUtils .msLogD ( v2,v3 );
/* .line 227 */
} // .end local v1 # "fwPackageNames":[Ljava/lang/String;
/* nop */
/* .line 230 */
} // :goto_1
return;
} // .end method
public void initVRSWhiteList ( ) {
/* .locals 3 */
/* .line 161 */
v0 = this.mContext;
/* if-nez v0, :cond_0 */
return;
/* .line 163 */
} // :cond_0
(( android.content.Context ) v0 ).getContentResolver ( ); // invoke-virtual {v0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;
/* const-string/jumbo v1, "support_common_vrs_app" */
int v2 = -2; // const/4 v2, -0x2
android.provider.Settings$System .getStringForUser ( v0,v1,v2 );
/* .line 165 */
/* .local v0, "cloudInfo":Ljava/lang/String; */
(( com.miui.server.multisence.MultiSenceConfig ) p0 ).updateVRSWhiteListAll ( v0 ); // invoke-virtual {p0, v0}, Lcom/miui/server/multisence/MultiSenceConfig;->updateVRSWhiteListAll(Ljava/lang/String;)V
/* .line 166 */
return;
} // .end method
public java.lang.String showSubFunctionStatus ( ) {
/* .locals 4 */
/* .line 265 */
/* new-instance v0, Ljava/lang/StringBuilder; */
final String v1 = " Subfunction support: {\n"; // const-string v1, " Subfunction support: {\n"
/* invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V */
/* .line 266 */
/* .local v0, "subFunctionStatus":Ljava/lang/StringBuilder; */
/* new-instance v1, Ljava/lang/StringBuilder; */
/* invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V */
final String v2 = " dyn-fps: "; // const-string v2, " dyn-fps: "
(( java.lang.StringBuilder ) v1 ).append ( v2 ); // invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
/* sget-boolean v2, Lcom/miui/server/multisence/MultiSenceConfig;->DYN_FPS_ENABLED:Z */
(( java.lang.StringBuilder ) v1 ).append ( v2 ); // invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;
final String v2 = ",\n"; // const-string v2, ",\n"
(( java.lang.StringBuilder ) v1 ).append ( v2 ); // invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v1 ).toString ( ); // invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
(( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
/* .line 267 */
/* new-instance v1, Ljava/lang/StringBuilder; */
/* invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V */
final String v3 = " multi-task-animation: "; // const-string v3, " multi-task-animation: "
(( java.lang.StringBuilder ) v1 ).append ( v3 ); // invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
/* sget-boolean v3, Lcom/miui/server/multisence/MultiSenceConfig;->MULTITASK_ANIM_SCHED_ENABLED:Z */
(( java.lang.StringBuilder ) v1 ).append ( v3 ); // invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v1 ).append ( v2 ); // invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v1 ).toString ( ); // invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
(( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
/* .line 268 */
/* new-instance v1, Ljava/lang/StringBuilder; */
/* invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V */
final String v3 = " framepredict: "; // const-string v3, " framepredict: "
(( java.lang.StringBuilder ) v1 ).append ( v3 ); // invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
/* sget-boolean v3, Lcom/miui/server/multisence/MultiSenceConfig;->FREMAPREDICT_ENABLE:Z */
(( java.lang.StringBuilder ) v1 ).append ( v3 ); // invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v1 ).append ( v2 ); // invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v1 ).toString ( ); // invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
(( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
/* .line 269 */
/* new-instance v1, Ljava/lang/StringBuilder; */
/* invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V */
final String v3 = " VRS: "; // const-string v3, " VRS: "
(( java.lang.StringBuilder ) v1 ).append ( v3 ); // invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
/* sget-boolean v3, Lcom/miui/server/multisence/MultiSenceConfig;->VRS_ENABLE:Z */
(( java.lang.StringBuilder ) v1 ).append ( v3 ); // invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v1 ).append ( v2 ); // invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v1 ).toString ( ); // invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
(( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
/* .line 270 */
/* new-instance v1, Ljava/lang/StringBuilder; */
/* invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V */
final String v3 = " policy[window-size]: "; // const-string v3, " policy[window-size]: "
(( java.lang.StringBuilder ) v1 ).append ( v3 ); // invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
/* sget-boolean v3, Lcom/miui/server/multisence/MultiSenceConfig;->POLICY_WINDOWSIZE_ENABLE:Z */
(( java.lang.StringBuilder ) v1 ).append ( v3 ); // invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v1 ).append ( v2 ); // invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v1 ).toString ( ); // invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
(( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
/* .line 271 */
/* new-instance v1, Ljava/lang/StringBuilder; */
/* invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V */
final String v3 = " floatingwindow: "; // const-string v3, " floatingwindow: "
(( java.lang.StringBuilder ) v1 ).append ( v3 ); // invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
/* sget-boolean v3, Lcom/miui/server/multisence/MultiSenceConfig;->FW_ENABLE:Z */
(( java.lang.StringBuilder ) v1 ).append ( v3 ); // invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v1 ).append ( v2 ); // invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v1 ).toString ( ); // invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
(( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
/* .line 272 */
final String v1 = " }\n"; // const-string v1, " }\n"
(( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
/* .line 273 */
(( java.lang.StringBuilder ) v0 ).toString ( ); // invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
} // .end method
public void updateDebugInfo ( ) {
/* .locals 5 */
/* .line 119 */
final String v0 = "persist.multisence.debug.code"; // const-string v0, "persist.multisence.debug.code"
int v1 = 0; // const/4 v1, 0x0
v0 = android.os.SystemProperties .getInt ( v0,v1 );
/* .line 120 */
/* .local v0, "debugCode":I */
final String v2 = "persist.multisence.debug.on"; // const-string v2, "persist.multisence.debug.on"
v2 = android.os.SystemProperties .getBoolean ( v2,v1 );
com.miui.server.multisence.MultiSenceConfig.DEBUG = (v2!= 0);
/* .line 121 */
int v3 = 1; // const/4 v3, 0x1
if ( v2 != null) { // if-eqz v2, :cond_1
/* and-int/lit8 v4, v0, 0x1 */
/* if-nez v4, :cond_0 */
} // :cond_0
/* move v4, v3 */
} // :cond_1
} // :goto_0
/* move v4, v1 */
} // :goto_1
com.miui.server.multisence.MultiSenceConfig.DEBUG_POLICY_BASE = (v4!= 0);
/* .line 122 */
if ( v2 != null) { // if-eqz v2, :cond_3
/* and-int/lit8 v4, v0, 0x2 */
/* if-nez v4, :cond_2 */
} // :cond_2
/* move v4, v3 */
} // :cond_3
} // :goto_2
/* move v4, v1 */
} // :goto_3
com.miui.server.multisence.MultiSenceConfig.DEBUG_DYN_FPS = (v4!= 0);
/* .line 123 */
if ( v2 != null) { // if-eqz v2, :cond_5
/* and-int/lit8 v4, v0, 0x4 */
/* if-nez v4, :cond_4 */
} // :cond_4
/* move v4, v3 */
} // :cond_5
} // :goto_4
/* move v4, v1 */
} // :goto_5
com.miui.server.multisence.MultiSenceConfig.DEBUG_VRS = (v4!= 0);
/* .line 124 */
if ( v2 != null) { // if-eqz v2, :cond_7
/* and-int/lit8 v4, v0, 0x8 */
/* if-nez v4, :cond_6 */
} // :cond_6
/* move v4, v3 */
} // :cond_7
} // :goto_6
/* move v4, v1 */
} // :goto_7
com.miui.server.multisence.MultiSenceConfig.DEBUG_CONFIG = (v4!= 0);
/* .line 125 */
if ( v2 != null) { // if-eqz v2, :cond_9
/* and-int/lit8 v4, v0, 0x10 */
/* if-nez v4, :cond_8 */
} // :cond_8
/* move v4, v3 */
} // :cond_9
} // :goto_8
/* move v4, v1 */
} // :goto_9
com.miui.server.multisence.MultiSenceConfig.DEBUG_DYNAMIC_CONTROLLER = (v4!= 0);
/* .line 126 */
if ( v2 != null) { // if-eqz v2, :cond_b
/* and-int/lit8 v4, v0, 0x20 */
/* if-nez v4, :cond_a */
} // :cond_a
/* move v4, v3 */
} // :cond_b
} // :goto_a
/* move v4, v1 */
} // :goto_b
com.miui.server.multisence.MultiSenceConfig.DEBUG_TRACE = (v4!= 0);
/* .line 127 */
if ( v2 != null) { // if-eqz v2, :cond_d
/* and-int/lit8 v2, v0, 0x40 */
/* if-nez v2, :cond_c */
} // :cond_c
/* move v1, v3 */
} // :cond_d
} // :goto_c
com.miui.server.multisence.MultiSenceConfig.DEBUG_FW = (v1!= 0);
/* .line 128 */
return;
} // .end method
public void updateFWWhiteListAll ( java.lang.String p0 ) {
/* .locals 6 */
/* .param p1, "whitelistinfo" # Ljava/lang/String; */
/* .line 233 */
/* sget-boolean v0, Lcom/miui/server/multisence/MultiSenceConfig;->DEBUG_FW:Z */
/* new-instance v1, Ljava/lang/StringBuilder; */
/* invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V */
final String v2 = "floating window whitelist: "; // const-string v2, "floating window whitelist: "
(( java.lang.StringBuilder ) v1 ).append ( v2 ); // invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v1 ).append ( p1 ); // invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v1 ).toString ( ); // invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
com.miui.server.multisence.MultiSenceServiceUtils .msLogD ( v0,v1 );
/* .line 241 */
/* new-instance v0, Ljava/lang/StringBuilder; */
/* invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V */
/* .line 242 */
/* .local v0, "whitelistinfoModify":Ljava/lang/StringBuilder; */
(( java.lang.String ) p1 ).toCharArray ( ); // invoke-virtual {p1}, Ljava/lang/String;->toCharArray()[C
/* array-length v2, v1 */
int v3 = 0; // const/4 v3, 0x0
} // :goto_0
/* if-ge v3, v2, :cond_1 */
/* aget-char v4, v1, v3 */
/* .line 243 */
/* .local v4, "c":C */
/* const/16 v5, 0x5b */
/* if-eq v4, v5, :cond_0 */
/* const/16 v5, 0x5d */
/* if-eq v4, v5, :cond_0 */
/* const/16 v5, 0x22 */
/* if-eq v4, v5, :cond_0 */
/* .line 244 */
(( java.lang.StringBuilder ) v0 ).append ( v4 ); // invoke-virtual {v0, v4}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;
/* .line 242 */
} // .end local v4 # "c":C
} // :cond_0
/* add-int/lit8 v3, v3, 0x1 */
/* .line 247 */
} // :cond_1
/* sget-boolean v1, Lcom/miui/server/multisence/MultiSenceConfig;->DEBUG_FW:Z */
/* new-instance v2, Ljava/lang/StringBuilder; */
/* invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V */
final String v3 = "modified: "; // const-string v3, "modified: "
(( java.lang.StringBuilder ) v2 ).append ( v3 ); // invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v0 ).toString ( ); // invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
(( java.lang.StringBuilder ) v2 ).append ( v3 ); // invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v2 ).toString ( ); // invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
com.miui.server.multisence.MultiSenceServiceUtils .msLogD ( v1,v2 );
/* .line 251 */
v1 = this.floatingWindowWhiteList;
/* monitor-enter v1 */
/* .line 252 */
try { // :try_start_0
v2 = this.floatingWindowWhiteList;
/* .line 253 */
v2 = this.floatingWindowWhiteList;
(( java.lang.StringBuilder ) v0 ).toString ( ); // invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
final String v4 = ","; // const-string v4, ","
(( java.lang.String ) v3 ).split ( v4 ); // invoke-virtual {v3, v4}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;
java.util.Arrays .asList ( v3 );
/* .line 254 */
/* sget-boolean v2, Lcom/miui/server/multisence/MultiSenceConfig;->DEBUG_FW:Z */
/* new-instance v3, Ljava/lang/StringBuilder; */
/* invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V */
final String v4 = "cloud floating window whitelist: "; // const-string v4, "cloud floating window whitelist: "
(( java.lang.StringBuilder ) v3 ).append ( v4 ); // invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
v4 = this.floatingWindowWhiteList;
(( java.lang.StringBuilder ) v3 ).append ( v4 ); // invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v3 ).toString ( ); // invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
com.miui.server.multisence.MultiSenceServiceUtils .msLogD ( v2,v3 );
/* .line 255 */
/* monitor-exit v1 */
/* .line 256 */
return;
/* .line 255 */
/* :catchall_0 */
/* move-exception v2 */
/* monitor-exit v1 */
/* :try_end_0 */
/* .catchall {:try_start_0 ..:try_end_0} :catchall_0 */
/* throw v2 */
} // .end method
public void updateSubFuncStatus ( ) {
/* .locals 4 */
/* .line 131 */
final String v0 = "persist.multisence.nonsupport"; // const-string v0, "persist.multisence.nonsupport"
int v1 = 0; // const/4 v1, 0x0
v0 = android.os.SystemProperties .getInt ( v0,v1 );
/* .line 133 */
/* sget-boolean v0, Lcom/miui/server/multisence/MultiSenceConfig;->SERVICE_ENABLED:Z */
int v2 = 1; // const/4 v2, 0x1
if ( v0 != null) { // if-eqz v0, :cond_0
/* sget-boolean v3, Lcom/miui/server/multisence/MultiSenceDynamicController;->IS_CLOUD_ENABLED:Z */
if ( v3 != null) { // if-eqz v3, :cond_0
/* and-int/2addr v3, v2 */
/* if-nez v3, :cond_0 */
/* move v3, v2 */
} // :cond_0
/* move v3, v1 */
} // :goto_0
com.miui.server.multisence.MultiSenceConfig.DYN_FPS_ENABLED = (v3!= 0);
/* .line 136 */
if ( v0 != null) { // if-eqz v0, :cond_1
/* sget-boolean v3, Lcom/miui/server/multisence/MultiSenceDynamicController;->IS_CLOUD_ENABLED:Z */
if ( v3 != null) { // if-eqz v3, :cond_1
/* and-int/lit8 v3, v3, 0x2 */
/* if-nez v3, :cond_1 */
/* move v3, v2 */
} // :cond_1
/* move v3, v1 */
} // :goto_1
com.miui.server.multisence.MultiSenceConfig.MULTITASK_ANIM_SCHED_ENABLED = (v3!= 0);
/* .line 139 */
if ( v0 != null) { // if-eqz v0, :cond_2
/* sget-boolean v3, Lcom/miui/server/multisence/MultiSenceDynamicController;->IS_CLOUD_ENABLED:Z */
if ( v3 != null) { // if-eqz v3, :cond_2
/* and-int/lit8 v3, v3, 0x4 */
/* if-nez v3, :cond_2 */
/* move v3, v2 */
} // :cond_2
/* move v3, v1 */
} // :goto_2
com.miui.server.multisence.MultiSenceConfig.FREMAPREDICT_ENABLE = (v3!= 0);
/* .line 142 */
if ( v0 != null) { // if-eqz v0, :cond_3
/* sget-boolean v3, Lcom/miui/server/multisence/MultiSenceDynamicController;->IS_CLOUD_ENABLED:Z */
if ( v3 != null) { // if-eqz v3, :cond_3
/* and-int/lit8 v3, v3, 0x8 */
/* if-nez v3, :cond_3 */
/* move v3, v2 */
} // :cond_3
/* move v3, v1 */
} // :goto_3
com.miui.server.multisence.MultiSenceConfig.VRS_ENABLE = (v3!= 0);
/* .line 145 */
if ( v0 != null) { // if-eqz v0, :cond_4
/* sget-boolean v3, Lcom/miui/server/multisence/MultiSenceDynamicController;->IS_CLOUD_ENABLED:Z */
if ( v3 != null) { // if-eqz v3, :cond_4
/* and-int/lit8 v3, v3, 0x10 */
/* if-nez v3, :cond_4 */
/* move v3, v2 */
} // :cond_4
/* move v3, v1 */
} // :goto_4
com.miui.server.multisence.MultiSenceConfig.POLICY_WINDOWSIZE_ENABLE = (v3!= 0);
/* .line 148 */
if ( v0 != null) { // if-eqz v0, :cond_5
/* sget-boolean v0, Lcom/miui/server/multisence/MultiSenceDynamicController;->IS_CLOUD_ENABLED:Z */
if ( v0 != null) { // if-eqz v0, :cond_5
/* and-int/lit8 v0, v0, 0x20 */
/* if-nez v0, :cond_5 */
/* move v1, v2 */
} // :cond_5
com.miui.server.multisence.MultiSenceConfig.FW_ENABLE = (v1!= 0);
/* .line 151 */
/* iget-boolean v0, p0, Lcom/miui/server/multisence/MultiSenceConfig;->mReady:Z */
if ( v0 != null) { // if-eqz v0, :cond_6
/* sget-boolean v0, Lcom/miui/server/multisence/MultiSenceConfig;->DYN_FPS_ENABLED:Z */
/* if-nez v0, :cond_6 */
v0 = this.mService;
(( com.miui.server.multisence.MultiSenceService ) v0 ).getPolicy ( ); // invoke-virtual {v0}, Lcom/miui/server/multisence/MultiSenceService;->getPolicy()Lcom/miui/server/multisence/MultiSenceServicePolicy;
if ( v0 != null) { // if-eqz v0, :cond_6
/* .line 152 */
v0 = this.mService;
(( com.miui.server.multisence.MultiSenceService ) v0 ).getPolicy ( ); // invoke-virtual {v0}, Lcom/miui/server/multisence/MultiSenceService;->getPolicy()Lcom/miui/server/multisence/MultiSenceServicePolicy;
(( com.miui.server.multisence.MultiSenceServicePolicy ) v0 ).resetDynFps ( ); // invoke-virtual {v0}, Lcom/miui/server/multisence/MultiSenceServicePolicy;->resetDynFps()V
/* .line 154 */
} // :cond_6
/* iget-boolean v0, p0, Lcom/miui/server/multisence/MultiSenceConfig;->mReady:Z */
if ( v0 != null) { // if-eqz v0, :cond_7
/* sget-boolean v0, Lcom/miui/server/multisence/MultiSenceConfig;->VRS_ENABLE:Z */
/* if-nez v0, :cond_7 */
v0 = this.mService;
(( com.miui.server.multisence.MultiSenceService ) v0 ).getPolicy ( ); // invoke-virtual {v0}, Lcom/miui/server/multisence/MultiSenceService;->getPolicy()Lcom/miui/server/multisence/MultiSenceServicePolicy;
if ( v0 != null) { // if-eqz v0, :cond_7
/* .line 155 */
v0 = this.mService;
(( com.miui.server.multisence.MultiSenceService ) v0 ).getPolicy ( ); // invoke-virtual {v0}, Lcom/miui/server/multisence/MultiSenceService;->getPolicy()Lcom/miui/server/multisence/MultiSenceServicePolicy;
(( com.miui.server.multisence.MultiSenceServicePolicy ) v0 ).resetVrs ( ); // invoke-virtual {v0}, Lcom/miui/server/multisence/MultiSenceServicePolicy;->resetVrs()V
/* .line 157 */
} // :cond_7
/* sget-boolean v0, Lcom/miui/server/multisence/MultiSenceConfig;->DEBUG_CONFIG:Z */
(( com.miui.server.multisence.MultiSenceConfig ) p0 ).showSubFunctionStatus ( ); // invoke-virtual {p0}, Lcom/miui/server/multisence/MultiSenceConfig;->showSubFunctionStatus()Ljava/lang/String;
com.miui.server.multisence.MultiSenceServiceUtils .msLogD ( v0,v1 );
/* .line 158 */
return;
} // .end method
public void updateVRSWhiteListAll ( java.lang.String p0 ) {
/* .locals 9 */
/* .param p1, "whitelistinfo" # Ljava/lang/String; */
/* .line 168 */
if ( p1 != null) { // if-eqz p1, :cond_4
v0 = (( java.lang.String ) p1 ).length ( ); // invoke-virtual {p1}, Ljava/lang/String;->length()I
/* if-nez v0, :cond_0 */
/* goto/16 :goto_2 */
/* .line 171 */
} // :cond_0
/* sget-boolean v0, Lcom/miui/server/multisence/MultiSenceConfig;->DEBUG_VRS:Z */
/* new-instance v1, Ljava/lang/StringBuilder; */
/* invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V */
/* const-string/jumbo v2, "vrs whitelist: " */
(( java.lang.StringBuilder ) v1 ).append ( v2 ); // invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v1 ).append ( p1 ); // invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v1 ).toString ( ); // invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
com.miui.server.multisence.MultiSenceServiceUtils .msLogD ( v0,v1 );
/* .line 178 */
/* new-instance v0, Ljava/lang/StringBuilder; */
/* invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V */
/* .line 179 */
/* .local v0, "whitelistinfoModify":Ljava/lang/StringBuilder; */
(( java.lang.String ) p1 ).toCharArray ( ); // invoke-virtual {p1}, Ljava/lang/String;->toCharArray()[C
/* array-length v2, v1 */
int v3 = 0; // const/4 v3, 0x0
/* move v4, v3 */
} // :goto_0
/* if-ge v4, v2, :cond_2 */
/* aget-char v5, v1, v4 */
/* .line 180 */
/* .local v5, "c":C */
/* const/16 v6, 0x5b */
/* if-eq v5, v6, :cond_1 */
/* const/16 v6, 0x5d */
/* if-eq v5, v6, :cond_1 */
/* const/16 v6, 0x22 */
/* if-eq v5, v6, :cond_1 */
/* .line 181 */
(( java.lang.StringBuilder ) v0 ).append ( v5 ); // invoke-virtual {v0, v5}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;
/* .line 179 */
} // .end local v5 # "c":C
} // :cond_1
/* add-int/lit8 v4, v4, 0x1 */
/* .line 184 */
} // :cond_2
/* sget-boolean v1, Lcom/miui/server/multisence/MultiSenceConfig;->DEBUG_VRS:Z */
/* new-instance v2, Ljava/lang/StringBuilder; */
/* invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V */
final String v4 = "mofidied: "; // const-string v4, "mofidied: "
(( java.lang.StringBuilder ) v2 ).append ( v4 ); // invoke-virtual {v2, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v0 ).toString ( ); // invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
(( java.lang.StringBuilder ) v2 ).append ( v4 ); // invoke-virtual {v2, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v2 ).toString ( ); // invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
com.miui.server.multisence.MultiSenceServiceUtils .msLogD ( v1,v2 );
/* .line 188 */
(( java.lang.StringBuilder ) v0 ).toString ( ); // invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
final String v2 = ","; // const-string v2, ","
(( java.lang.String ) v1 ).split ( v2 ); // invoke-virtual {v1, v2}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;
/* .line 189 */
/* .local v1, "nameWhiteList":[Ljava/lang/String; */
int v2 = 0; // const/4 v2, 0x0
/* .line 190 */
/* .local v2, "count":I */
/* array-length v4, v1 */
} // :goto_1
/* if-ge v3, v4, :cond_3 */
/* aget-object v5, v1, v3 */
/* .line 191 */
/* .local v5, "name":Ljava/lang/String; */
/* sget-boolean v6, Lcom/miui/server/multisence/MultiSenceConfig;->DEBUG_VRS:Z */
/* new-instance v7, Ljava/lang/StringBuilder; */
/* invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V */
final String v8 = "["; // const-string v8, "["
(( java.lang.StringBuilder ) v7 ).append ( v8 ); // invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v7 ).append ( v2 ); // invoke-virtual {v7, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;
final String v8 = "]"; // const-string v8, "]"
(( java.lang.StringBuilder ) v7 ).append ( v8 ); // invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v7 ).append ( v5 ); // invoke-virtual {v7, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v7 ).toString ( ); // invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
com.miui.server.multisence.MultiSenceServiceUtils .msLogD ( v6,v7 );
/* .line 192 */
int v6 = 1; // const/4 v6, 0x1
(( com.miui.server.multisence.MultiSenceConfig ) p0 ).updateVRSWhiteListByApp ( v5, v6 ); // invoke-virtual {p0, v5, v6}, Lcom/miui/server/multisence/MultiSenceConfig;->updateVRSWhiteListByApp(Ljava/lang/String;Z)V
/* .line 193 */
/* nop */
} // .end local v5 # "name":Ljava/lang/String;
/* add-int/lit8 v2, v2, 0x1 */
/* .line 190 */
/* add-int/lit8 v3, v3, 0x1 */
/* .line 195 */
} // :cond_3
return;
/* .line 169 */
} // .end local v0 # "whitelistinfoModify":Ljava/lang/StringBuilder;
} // .end local v1 # "nameWhiteList":[Ljava/lang/String;
} // .end local v2 # "count":I
} // :cond_4
} // :goto_2
return;
} // .end method
public void updateVRSWhiteListByApp ( java.lang.String p0, Boolean p1 ) {
/* .locals 2 */
/* .param p1, "name" # Ljava/lang/String; */
/* .param p2, "isAdd" # Z */
/* .line 198 */
/* if-nez p1, :cond_0 */
/* .line 199 */
final String v0 = "MultiSenceConfig"; // const-string v0, "MultiSenceConfig"
final String v1 = "invalid operation when add app to support VRS"; // const-string v1, "invalid operation when add app to support VRS"
android.util.Slog .w ( v0,v1 );
/* .line 200 */
return;
/* .line 202 */
} // :cond_0
v0 = v0 = this.mWhiteListVRS;
/* .line 203 */
/* .local v0, "isContained":Z */
if ( p2 != null) { // if-eqz p2, :cond_1
/* if-nez v0, :cond_1 */
/* .line 204 */
v1 = this.mWhiteListVRS;
/* .line 205 */
} // :cond_1
/* if-nez p2, :cond_2 */
if ( v0 != null) { // if-eqz v0, :cond_2
/* .line 206 */
v1 = this.mWhiteListVRS;
/* .line 208 */
} // :cond_2
} // :goto_0
return;
} // .end method
