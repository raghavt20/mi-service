.class public Lcom/miui/server/multisence/MultiSenceConfig;
.super Ljava/lang/Object;
.source "MultiSenceConfig.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/miui/server/multisence/MultiSenceConfig$MultiSenceConfigHolder;
    }
.end annotation


# static fields
.field public static DEBUG:Z = false

.field private static final DEBUG_BIT_CONFIG:I = 0x8

.field private static final DEBUG_BIT_DYNAMIC_CONTROLLER:I = 0x10

.field private static final DEBUG_BIT_DYN_FPS:I = 0x2

.field private static final DEBUG_BIT_FW:I = 0x40

.field private static final DEBUG_BIT_POLICY_BASE:I = 0x1

.field private static final DEBUG_BIT_TRACE:I = 0x20

.field private static final DEBUG_BIT_VRS:I = 0x4

.field public static DEBUG_CONFIG:Z = false

.field public static DEBUG_DYNAMIC_CONTROLLER:Z = false

.field public static DEBUG_DYN_FPS:Z = false

.field public static DEBUG_FW:Z = false

.field public static DEBUG_POLICY_BASE:Z = false

.field public static DEBUG_TRACE:Z = false

.field public static DEBUG_VRS:Z = false

.field public static DYN_FPS_ENABLED:Z = false

.field public static FREMAPREDICT_ENABLE:Z = false

.field public static FW_ENABLE:Z = false

.field public static MULTITASK_ANIM_SCHED_ENABLED:Z = false

.field private static final NONSUPPORT_DYNAMIC_FPS:I = 0x1

.field private static final NONSUPPORT_FRAMEPREDICT:I = 0x4

.field private static final NONSUPPORT_FW:I = 0x20

.field private static final NONSUPPORT_MULTITASK_ANIM_SCHED:I = 0x2

.field private static final NONSUPPORT_POLICY_WINDOWSIZE:I = 0x10

.field private static final NONSUPPORT_VRS:I = 0x8

.field public static POLICY_WINDOWSIZE_ENABLE:Z = false

.field public static final PROPERTY_PREFIX:Ljava/lang/String; = "persist.multisence."

.field public static final PROP_MULTISENCE_ENABLE:Ljava/lang/String; = "persist.multisence.enable"

.field public static final PROP_SUB_FUNC_NONSUPPORT:Ljava/lang/String; = "persist.multisence.nonsupport"

.field public static final SERVICE_ENABLED:Z

.field public static final SERVICE_JAVA_NAME:Ljava/lang/String; = "MultiSence"

.field private static SUB_FUNC_NONSUPPORT:I = 0x0

.field private static final TAG:Ljava/lang/String; = "MultiSenceConfig"

.field public static VRS_ENABLE:Z


# instance fields
.field public floatingWindowWhiteList:Ljava/util/Set;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Set<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private mContext:Landroid/content/Context;

.field private mReady:Z

.field mService:Lcom/miui/server/multisence/MultiSenceService;

.field private mWhiteListVRS:Ljava/util/Set;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Set<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .locals 3

    .line 31
    const-string v0, "persist.multisence.nonsupport"

    const/4 v1, 0x0

    invoke-static {v0, v1}, Landroid/os/SystemProperties;->getInt(Ljava/lang/String;I)I

    move-result v0

    sput v0, Lcom/miui/server/multisence/MultiSenceConfig;->SUB_FUNC_NONSUPPORT:I

    .line 46
    const-string v0, "persist.multisence.enable"

    const/4 v2, 0x1

    invoke-static {v0, v2}, Landroid/os/SystemProperties;->getBoolean(Ljava/lang/String;Z)Z

    move-result v0

    sput-boolean v0, Lcom/miui/server/multisence/MultiSenceConfig;->SERVICE_ENABLED:Z

    .line 48
    sput-boolean v1, Lcom/miui/server/multisence/MultiSenceConfig;->DYN_FPS_ENABLED:Z

    .line 50
    sput-boolean v1, Lcom/miui/server/multisence/MultiSenceConfig;->MULTITASK_ANIM_SCHED_ENABLED:Z

    .line 52
    sput-boolean v1, Lcom/miui/server/multisence/MultiSenceConfig;->FREMAPREDICT_ENABLE:Z

    .line 54
    sput-boolean v1, Lcom/miui/server/multisence/MultiSenceConfig;->VRS_ENABLE:Z

    .line 56
    sput-boolean v1, Lcom/miui/server/multisence/MultiSenceConfig;->POLICY_WINDOWSIZE_ENABLE:Z

    .line 58
    sput-boolean v1, Lcom/miui/server/multisence/MultiSenceConfig;->FW_ENABLE:Z

    .line 60
    const-string v0, "persist.multisence.debug.on"

    invoke-static {v0, v1}, Landroid/os/SystemProperties;->getBoolean(Ljava/lang/String;Z)Z

    move-result v0

    sput-boolean v0, Lcom/miui/server/multisence/MultiSenceConfig;->DEBUG:Z

    .line 77
    sput-boolean v1, Lcom/miui/server/multisence/MultiSenceConfig;->DEBUG_POLICY_BASE:Z

    .line 79
    sput-boolean v1, Lcom/miui/server/multisence/MultiSenceConfig;->DEBUG_DYN_FPS:Z

    .line 81
    sput-boolean v1, Lcom/miui/server/multisence/MultiSenceConfig;->DEBUG_VRS:Z

    .line 83
    sput-boolean v1, Lcom/miui/server/multisence/MultiSenceConfig;->DEBUG_CONFIG:Z

    .line 85
    sput-boolean v1, Lcom/miui/server/multisence/MultiSenceConfig;->DEBUG_DYNAMIC_CONTROLLER:Z

    .line 87
    sput-boolean v1, Lcom/miui/server/multisence/MultiSenceConfig;->DEBUG_TRACE:Z

    .line 89
    sput-boolean v1, Lcom/miui/server/multisence/MultiSenceConfig;->DEBUG_FW:Z

    return-void
.end method

.method private constructor <init>()V
    .locals 1

    .line 99
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 28
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/miui/server/multisence/MultiSenceConfig;->mReady:Z

    .line 94
    new-instance v0, Ljava/util/HashSet;

    invoke-direct {v0}, Ljava/util/HashSet;-><init>()V

    iput-object v0, p0, Lcom/miui/server/multisence/MultiSenceConfig;->mWhiteListVRS:Ljava/util/Set;

    .line 96
    new-instance v0, Ljava/util/HashSet;

    invoke-direct {v0}, Ljava/util/HashSet;-><init>()V

    iput-object v0, p0, Lcom/miui/server/multisence/MultiSenceConfig;->floatingWindowWhiteList:Ljava/util/Set;

    .line 99
    return-void
.end method

.method synthetic constructor <init>(Lcom/miui/server/multisence/MultiSenceConfig-IA;)V
    .locals 0

    invoke-direct {p0}, Lcom/miui/server/multisence/MultiSenceConfig;-><init>()V

    return-void
.end method

.method public static getInstance()Lcom/miui/server/multisence/MultiSenceConfig;
    .locals 1

    .line 106
    invoke-static {}, Lcom/miui/server/multisence/MultiSenceConfig$MultiSenceConfigHolder;->-$$Nest$sfgetINSTANCE()Lcom/miui/server/multisence/MultiSenceConfig;

    move-result-object v0

    return-object v0
.end method


# virtual methods
.method public checkVrsSupport(Ljava/lang/String;)Z
    .locals 2
    .param p1, "name"    # Ljava/lang/String;

    .line 211
    const/4 v0, 0x0

    .line 212
    .local v0, "ret":Z
    iget-object v1, p0, Lcom/miui/server/multisence/MultiSenceConfig;->mWhiteListVRS:Ljava/util/Set;

    invoke-interface {v1, p1}, Ljava/util/Set;->contains(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 213
    const/4 v0, 0x1

    .line 215
    :cond_0
    return v0
.end method

.method public dumpConfig(Ljava/io/PrintWriter;)V
    .locals 2
    .param p1, "pw"    # Ljava/io/PrintWriter;

    .line 259
    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "Config:\n"

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    .line 260
    .local v0, "details":Ljava/lang/StringBuilder;
    invoke-virtual {p0}, Lcom/miui/server/multisence/MultiSenceConfig;->showSubFunctionStatus()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 261
    invoke-virtual {p1, v0}, Ljava/io/PrintWriter;->print(Ljava/lang/Object;)V

    .line 262
    return-void
.end method

.method public init(Lcom/miui/server/multisence/MultiSenceService;Landroid/content/Context;)V
    .locals 1
    .param p1, "service"    # Lcom/miui/server/multisence/MultiSenceService;
    .param p2, "context"    # Landroid/content/Context;

    .line 110
    iput-object p1, p0, Lcom/miui/server/multisence/MultiSenceConfig;->mService:Lcom/miui/server/multisence/MultiSenceService;

    .line 111
    iput-object p2, p0, Lcom/miui/server/multisence/MultiSenceConfig;->mContext:Landroid/content/Context;

    .line 112
    invoke-virtual {p0}, Lcom/miui/server/multisence/MultiSenceConfig;->updateDebugInfo()V

    .line 113
    iget-object v0, p0, Lcom/miui/server/multisence/MultiSenceConfig;->mService:Lcom/miui/server/multisence/MultiSenceService;

    if-eqz v0, :cond_0

    .line 114
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/miui/server/multisence/MultiSenceConfig;->mReady:Z

    .line 116
    :cond_0
    return-void
.end method

.method public initFWWhiteList()V
    .locals 5

    .line 219
    iget-object v0, p0, Lcom/miui/server/multisence/MultiSenceConfig;->mContext:Landroid/content/Context;

    if-nez v0, :cond_0

    return-void

    .line 221
    :cond_0
    invoke-virtual {v0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    const-string/jumbo v1, "support_fw_app"

    const/4 v2, -0x2

    invoke-static {v0, v1, v2}, Landroid/provider/Settings$System;->getStringForUser(Landroid/content/ContentResolver;Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v0

    .line 223
    .local v0, "cloudInfo":Ljava/lang/String;
    if-eqz v0, :cond_2

    invoke-virtual {v0}, Ljava/lang/String;->length()I

    move-result v1

    if-nez v1, :cond_1

    goto :goto_0

    .line 228
    :cond_1
    invoke-virtual {p0, v0}, Lcom/miui/server/multisence/MultiSenceConfig;->updateFWWhiteListAll(Ljava/lang/String;)V

    goto :goto_1

    .line 224
    :cond_2
    :goto_0
    iget-object v1, p0, Lcom/miui/server/multisence/MultiSenceConfig;->mContext:Landroid/content/Context;

    invoke-virtual {v1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x11030071

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getStringArray(I)[Ljava/lang/String;

    move-result-object v1

    .line 225
    .local v1, "fwPackageNames":[Ljava/lang/String;
    iget-object v2, p0, Lcom/miui/server/multisence/MultiSenceConfig;->floatingWindowWhiteList:Ljava/util/Set;

    invoke-static {v1}, Ljava/util/Arrays;->asList([Ljava/lang/Object;)Ljava/util/List;

    move-result-object v3

    invoke-interface {v2, v3}, Ljava/util/Set;->addAll(Ljava/util/Collection;)Z

    .line 226
    sget-boolean v2, Lcom/miui/server/multisence/MultiSenceConfig;->DEBUG_FW:Z

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "local floating window whitelist: "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    iget-object v4, p0, Lcom/miui/server/multisence/MultiSenceConfig;->floatingWindowWhiteList:Ljava/util/Set;

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Lcom/miui/server/multisence/MultiSenceServiceUtils;->msLogD(ZLjava/lang/String;)V

    .line 227
    .end local v1    # "fwPackageNames":[Ljava/lang/String;
    nop

    .line 230
    :goto_1
    return-void
.end method

.method public initVRSWhiteList()V
    .locals 3

    .line 161
    iget-object v0, p0, Lcom/miui/server/multisence/MultiSenceConfig;->mContext:Landroid/content/Context;

    if-nez v0, :cond_0

    return-void

    .line 163
    :cond_0
    invoke-virtual {v0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    const-string/jumbo v1, "support_common_vrs_app"

    const/4 v2, -0x2

    invoke-static {v0, v1, v2}, Landroid/provider/Settings$System;->getStringForUser(Landroid/content/ContentResolver;Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v0

    .line 165
    .local v0, "cloudInfo":Ljava/lang/String;
    invoke-virtual {p0, v0}, Lcom/miui/server/multisence/MultiSenceConfig;->updateVRSWhiteListAll(Ljava/lang/String;)V

    .line 166
    return-void
.end method

.method public showSubFunctionStatus()Ljava/lang/String;
    .locals 4

    .line 265
    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "  Subfunction support: {\n"

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    .line 266
    .local v0, "subFunctionStatus":Ljava/lang/StringBuilder;
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "    dyn-fps: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    sget-boolean v2, Lcom/miui/server/multisence/MultiSenceConfig;->DYN_FPS_ENABLED:Z

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ",\n"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 267
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "    multi-task-animation: "

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    sget-boolean v3, Lcom/miui/server/multisence/MultiSenceConfig;->MULTITASK_ANIM_SCHED_ENABLED:Z

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 268
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "    framepredict: "

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    sget-boolean v3, Lcom/miui/server/multisence/MultiSenceConfig;->FREMAPREDICT_ENABLE:Z

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 269
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "    VRS: "

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    sget-boolean v3, Lcom/miui/server/multisence/MultiSenceConfig;->VRS_ENABLE:Z

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 270
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "    policy[window-size]: "

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    sget-boolean v3, Lcom/miui/server/multisence/MultiSenceConfig;->POLICY_WINDOWSIZE_ENABLE:Z

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 271
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "    floatingwindow: "

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    sget-boolean v3, Lcom/miui/server/multisence/MultiSenceConfig;->FW_ENABLE:Z

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 272
    const-string v1, "  }\n"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 273
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    return-object v1
.end method

.method public updateDebugInfo()V
    .locals 5

    .line 119
    const-string v0, "persist.multisence.debug.code"

    const/4 v1, 0x0

    invoke-static {v0, v1}, Landroid/os/SystemProperties;->getInt(Ljava/lang/String;I)I

    move-result v0

    .line 120
    .local v0, "debugCode":I
    const-string v2, "persist.multisence.debug.on"

    invoke-static {v2, v1}, Landroid/os/SystemProperties;->getBoolean(Ljava/lang/String;Z)Z

    move-result v2

    sput-boolean v2, Lcom/miui/server/multisence/MultiSenceConfig;->DEBUG:Z

    .line 121
    const/4 v3, 0x1

    if-eqz v2, :cond_1

    and-int/lit8 v4, v0, 0x1

    if-nez v4, :cond_0

    goto :goto_0

    :cond_0
    move v4, v3

    goto :goto_1

    :cond_1
    :goto_0
    move v4, v1

    :goto_1
    sput-boolean v4, Lcom/miui/server/multisence/MultiSenceConfig;->DEBUG_POLICY_BASE:Z

    .line 122
    if-eqz v2, :cond_3

    and-int/lit8 v4, v0, 0x2

    if-nez v4, :cond_2

    goto :goto_2

    :cond_2
    move v4, v3

    goto :goto_3

    :cond_3
    :goto_2
    move v4, v1

    :goto_3
    sput-boolean v4, Lcom/miui/server/multisence/MultiSenceConfig;->DEBUG_DYN_FPS:Z

    .line 123
    if-eqz v2, :cond_5

    and-int/lit8 v4, v0, 0x4

    if-nez v4, :cond_4

    goto :goto_4

    :cond_4
    move v4, v3

    goto :goto_5

    :cond_5
    :goto_4
    move v4, v1

    :goto_5
    sput-boolean v4, Lcom/miui/server/multisence/MultiSenceConfig;->DEBUG_VRS:Z

    .line 124
    if-eqz v2, :cond_7

    and-int/lit8 v4, v0, 0x8

    if-nez v4, :cond_6

    goto :goto_6

    :cond_6
    move v4, v3

    goto :goto_7

    :cond_7
    :goto_6
    move v4, v1

    :goto_7
    sput-boolean v4, Lcom/miui/server/multisence/MultiSenceConfig;->DEBUG_CONFIG:Z

    .line 125
    if-eqz v2, :cond_9

    and-int/lit8 v4, v0, 0x10

    if-nez v4, :cond_8

    goto :goto_8

    :cond_8
    move v4, v3

    goto :goto_9

    :cond_9
    :goto_8
    move v4, v1

    :goto_9
    sput-boolean v4, Lcom/miui/server/multisence/MultiSenceConfig;->DEBUG_DYNAMIC_CONTROLLER:Z

    .line 126
    if-eqz v2, :cond_b

    and-int/lit8 v4, v0, 0x20

    if-nez v4, :cond_a

    goto :goto_a

    :cond_a
    move v4, v3

    goto :goto_b

    :cond_b
    :goto_a
    move v4, v1

    :goto_b
    sput-boolean v4, Lcom/miui/server/multisence/MultiSenceConfig;->DEBUG_TRACE:Z

    .line 127
    if-eqz v2, :cond_d

    and-int/lit8 v2, v0, 0x40

    if-nez v2, :cond_c

    goto :goto_c

    :cond_c
    move v1, v3

    :cond_d
    :goto_c
    sput-boolean v1, Lcom/miui/server/multisence/MultiSenceConfig;->DEBUG_FW:Z

    .line 128
    return-void
.end method

.method public updateFWWhiteListAll(Ljava/lang/String;)V
    .locals 6
    .param p1, "whitelistinfo"    # Ljava/lang/String;

    .line 233
    sget-boolean v0, Lcom/miui/server/multisence/MultiSenceConfig;->DEBUG_FW:Z

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "floating window whitelist: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/miui/server/multisence/MultiSenceServiceUtils;->msLogD(ZLjava/lang/String;)V

    .line 241
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    .line 242
    .local v0, "whitelistinfoModify":Ljava/lang/StringBuilder;
    invoke-virtual {p1}, Ljava/lang/String;->toCharArray()[C

    move-result-object v1

    array-length v2, v1

    const/4 v3, 0x0

    :goto_0
    if-ge v3, v2, :cond_1

    aget-char v4, v1, v3

    .line 243
    .local v4, "c":C
    const/16 v5, 0x5b

    if-eq v4, v5, :cond_0

    const/16 v5, 0x5d

    if-eq v4, v5, :cond_0

    const/16 v5, 0x22

    if-eq v4, v5, :cond_0

    .line 244
    invoke-virtual {v0, v4}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    .line 242
    .end local v4    # "c":C
    :cond_0
    add-int/lit8 v3, v3, 0x1

    goto :goto_0

    .line 247
    :cond_1
    sget-boolean v1, Lcom/miui/server/multisence/MultiSenceConfig;->DEBUG_FW:Z

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "modified: "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/miui/server/multisence/MultiSenceServiceUtils;->msLogD(ZLjava/lang/String;)V

    .line 251
    iget-object v1, p0, Lcom/miui/server/multisence/MultiSenceConfig;->floatingWindowWhiteList:Ljava/util/Set;

    monitor-enter v1

    .line 252
    :try_start_0
    iget-object v2, p0, Lcom/miui/server/multisence/MultiSenceConfig;->floatingWindowWhiteList:Ljava/util/Set;

    invoke-interface {v2}, Ljava/util/Set;->clear()V

    .line 253
    iget-object v2, p0, Lcom/miui/server/multisence/MultiSenceConfig;->floatingWindowWhiteList:Ljava/util/Set;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    const-string v4, ","

    invoke-virtual {v3, v4}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v3

    invoke-static {v3}, Ljava/util/Arrays;->asList([Ljava/lang/Object;)Ljava/util/List;

    move-result-object v3

    invoke-interface {v2, v3}, Ljava/util/Set;->addAll(Ljava/util/Collection;)Z

    .line 254
    sget-boolean v2, Lcom/miui/server/multisence/MultiSenceConfig;->DEBUG_FW:Z

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "cloud floating window whitelist: "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    iget-object v4, p0, Lcom/miui/server/multisence/MultiSenceConfig;->floatingWindowWhiteList:Ljava/util/Set;

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Lcom/miui/server/multisence/MultiSenceServiceUtils;->msLogD(ZLjava/lang/String;)V

    .line 255
    monitor-exit v1

    .line 256
    return-void

    .line 255
    :catchall_0
    move-exception v2

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v2
.end method

.method public updateSubFuncStatus()V
    .locals 4

    .line 131
    const-string v0, "persist.multisence.nonsupport"

    const/4 v1, 0x0

    invoke-static {v0, v1}, Landroid/os/SystemProperties;->getInt(Ljava/lang/String;I)I

    move-result v0

    sput v0, Lcom/miui/server/multisence/MultiSenceConfig;->SUB_FUNC_NONSUPPORT:I

    .line 133
    sget-boolean v0, Lcom/miui/server/multisence/MultiSenceConfig;->SERVICE_ENABLED:Z

    const/4 v2, 0x1

    if-eqz v0, :cond_0

    sget-boolean v3, Lcom/miui/server/multisence/MultiSenceDynamicController;->IS_CLOUD_ENABLED:Z

    if-eqz v3, :cond_0

    sget v3, Lcom/miui/server/multisence/MultiSenceConfig;->SUB_FUNC_NONSUPPORT:I

    and-int/2addr v3, v2

    if-nez v3, :cond_0

    move v3, v2

    goto :goto_0

    :cond_0
    move v3, v1

    :goto_0
    sput-boolean v3, Lcom/miui/server/multisence/MultiSenceConfig;->DYN_FPS_ENABLED:Z

    .line 136
    if-eqz v0, :cond_1

    sget-boolean v3, Lcom/miui/server/multisence/MultiSenceDynamicController;->IS_CLOUD_ENABLED:Z

    if-eqz v3, :cond_1

    sget v3, Lcom/miui/server/multisence/MultiSenceConfig;->SUB_FUNC_NONSUPPORT:I

    and-int/lit8 v3, v3, 0x2

    if-nez v3, :cond_1

    move v3, v2

    goto :goto_1

    :cond_1
    move v3, v1

    :goto_1
    sput-boolean v3, Lcom/miui/server/multisence/MultiSenceConfig;->MULTITASK_ANIM_SCHED_ENABLED:Z

    .line 139
    if-eqz v0, :cond_2

    sget-boolean v3, Lcom/miui/server/multisence/MultiSenceDynamicController;->IS_CLOUD_ENABLED:Z

    if-eqz v3, :cond_2

    sget v3, Lcom/miui/server/multisence/MultiSenceConfig;->SUB_FUNC_NONSUPPORT:I

    and-int/lit8 v3, v3, 0x4

    if-nez v3, :cond_2

    move v3, v2

    goto :goto_2

    :cond_2
    move v3, v1

    :goto_2
    sput-boolean v3, Lcom/miui/server/multisence/MultiSenceConfig;->FREMAPREDICT_ENABLE:Z

    .line 142
    if-eqz v0, :cond_3

    sget-boolean v3, Lcom/miui/server/multisence/MultiSenceDynamicController;->IS_CLOUD_ENABLED:Z

    if-eqz v3, :cond_3

    sget v3, Lcom/miui/server/multisence/MultiSenceConfig;->SUB_FUNC_NONSUPPORT:I

    and-int/lit8 v3, v3, 0x8

    if-nez v3, :cond_3

    move v3, v2

    goto :goto_3

    :cond_3
    move v3, v1

    :goto_3
    sput-boolean v3, Lcom/miui/server/multisence/MultiSenceConfig;->VRS_ENABLE:Z

    .line 145
    if-eqz v0, :cond_4

    sget-boolean v3, Lcom/miui/server/multisence/MultiSenceDynamicController;->IS_CLOUD_ENABLED:Z

    if-eqz v3, :cond_4

    sget v3, Lcom/miui/server/multisence/MultiSenceConfig;->SUB_FUNC_NONSUPPORT:I

    and-int/lit8 v3, v3, 0x10

    if-nez v3, :cond_4

    move v3, v2

    goto :goto_4

    :cond_4
    move v3, v1

    :goto_4
    sput-boolean v3, Lcom/miui/server/multisence/MultiSenceConfig;->POLICY_WINDOWSIZE_ENABLE:Z

    .line 148
    if-eqz v0, :cond_5

    sget-boolean v0, Lcom/miui/server/multisence/MultiSenceDynamicController;->IS_CLOUD_ENABLED:Z

    if-eqz v0, :cond_5

    sget v0, Lcom/miui/server/multisence/MultiSenceConfig;->SUB_FUNC_NONSUPPORT:I

    and-int/lit8 v0, v0, 0x20

    if-nez v0, :cond_5

    move v1, v2

    :cond_5
    sput-boolean v1, Lcom/miui/server/multisence/MultiSenceConfig;->FW_ENABLE:Z

    .line 151
    iget-boolean v0, p0, Lcom/miui/server/multisence/MultiSenceConfig;->mReady:Z

    if-eqz v0, :cond_6

    sget-boolean v0, Lcom/miui/server/multisence/MultiSenceConfig;->DYN_FPS_ENABLED:Z

    if-nez v0, :cond_6

    iget-object v0, p0, Lcom/miui/server/multisence/MultiSenceConfig;->mService:Lcom/miui/server/multisence/MultiSenceService;

    invoke-virtual {v0}, Lcom/miui/server/multisence/MultiSenceService;->getPolicy()Lcom/miui/server/multisence/MultiSenceServicePolicy;

    move-result-object v0

    if-eqz v0, :cond_6

    .line 152
    iget-object v0, p0, Lcom/miui/server/multisence/MultiSenceConfig;->mService:Lcom/miui/server/multisence/MultiSenceService;

    invoke-virtual {v0}, Lcom/miui/server/multisence/MultiSenceService;->getPolicy()Lcom/miui/server/multisence/MultiSenceServicePolicy;

    move-result-object v0

    invoke-virtual {v0}, Lcom/miui/server/multisence/MultiSenceServicePolicy;->resetDynFps()V

    .line 154
    :cond_6
    iget-boolean v0, p0, Lcom/miui/server/multisence/MultiSenceConfig;->mReady:Z

    if-eqz v0, :cond_7

    sget-boolean v0, Lcom/miui/server/multisence/MultiSenceConfig;->VRS_ENABLE:Z

    if-nez v0, :cond_7

    iget-object v0, p0, Lcom/miui/server/multisence/MultiSenceConfig;->mService:Lcom/miui/server/multisence/MultiSenceService;

    invoke-virtual {v0}, Lcom/miui/server/multisence/MultiSenceService;->getPolicy()Lcom/miui/server/multisence/MultiSenceServicePolicy;

    move-result-object v0

    if-eqz v0, :cond_7

    .line 155
    iget-object v0, p0, Lcom/miui/server/multisence/MultiSenceConfig;->mService:Lcom/miui/server/multisence/MultiSenceService;

    invoke-virtual {v0}, Lcom/miui/server/multisence/MultiSenceService;->getPolicy()Lcom/miui/server/multisence/MultiSenceServicePolicy;

    move-result-object v0

    invoke-virtual {v0}, Lcom/miui/server/multisence/MultiSenceServicePolicy;->resetVrs()V

    .line 157
    :cond_7
    sget-boolean v0, Lcom/miui/server/multisence/MultiSenceConfig;->DEBUG_CONFIG:Z

    invoke-virtual {p0}, Lcom/miui/server/multisence/MultiSenceConfig;->showSubFunctionStatus()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/miui/server/multisence/MultiSenceServiceUtils;->msLogD(ZLjava/lang/String;)V

    .line 158
    return-void
.end method

.method public updateVRSWhiteListAll(Ljava/lang/String;)V
    .locals 9
    .param p1, "whitelistinfo"    # Ljava/lang/String;

    .line 168
    if-eqz p1, :cond_4

    invoke-virtual {p1}, Ljava/lang/String;->length()I

    move-result v0

    if-nez v0, :cond_0

    goto/16 :goto_2

    .line 171
    :cond_0
    sget-boolean v0, Lcom/miui/server/multisence/MultiSenceConfig;->DEBUG_VRS:Z

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v2, "vrs whitelist: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/miui/server/multisence/MultiSenceServiceUtils;->msLogD(ZLjava/lang/String;)V

    .line 178
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    .line 179
    .local v0, "whitelistinfoModify":Ljava/lang/StringBuilder;
    invoke-virtual {p1}, Ljava/lang/String;->toCharArray()[C

    move-result-object v1

    array-length v2, v1

    const/4 v3, 0x0

    move v4, v3

    :goto_0
    if-ge v4, v2, :cond_2

    aget-char v5, v1, v4

    .line 180
    .local v5, "c":C
    const/16 v6, 0x5b

    if-eq v5, v6, :cond_1

    const/16 v6, 0x5d

    if-eq v5, v6, :cond_1

    const/16 v6, 0x22

    if-eq v5, v6, :cond_1

    .line 181
    invoke-virtual {v0, v5}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    .line 179
    .end local v5    # "c":C
    :cond_1
    add-int/lit8 v4, v4, 0x1

    goto :goto_0

    .line 184
    :cond_2
    sget-boolean v1, Lcom/miui/server/multisence/MultiSenceConfig;->DEBUG_VRS:Z

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "mofidied: "

    invoke-virtual {v2, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v2, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/miui/server/multisence/MultiSenceServiceUtils;->msLogD(ZLjava/lang/String;)V

    .line 188
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    const-string v2, ","

    invoke-virtual {v1, v2}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v1

    .line 189
    .local v1, "nameWhiteList":[Ljava/lang/String;
    const/4 v2, 0x0

    .line 190
    .local v2, "count":I
    array-length v4, v1

    :goto_1
    if-ge v3, v4, :cond_3

    aget-object v5, v1, v3

    .line 191
    .local v5, "name":Ljava/lang/String;
    sget-boolean v6, Lcom/miui/server/multisence/MultiSenceConfig;->DEBUG_VRS:Z

    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    const-string v8, "["

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v7

    const-string v8, "]"

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-static {v6, v7}, Lcom/miui/server/multisence/MultiSenceServiceUtils;->msLogD(ZLjava/lang/String;)V

    .line 192
    const/4 v6, 0x1

    invoke-virtual {p0, v5, v6}, Lcom/miui/server/multisence/MultiSenceConfig;->updateVRSWhiteListByApp(Ljava/lang/String;Z)V

    .line 193
    nop

    .end local v5    # "name":Ljava/lang/String;
    add-int/lit8 v2, v2, 0x1

    .line 190
    add-int/lit8 v3, v3, 0x1

    goto :goto_1

    .line 195
    :cond_3
    return-void

    .line 169
    .end local v0    # "whitelistinfoModify":Ljava/lang/StringBuilder;
    .end local v1    # "nameWhiteList":[Ljava/lang/String;
    .end local v2    # "count":I
    :cond_4
    :goto_2
    return-void
.end method

.method public updateVRSWhiteListByApp(Ljava/lang/String;Z)V
    .locals 2
    .param p1, "name"    # Ljava/lang/String;
    .param p2, "isAdd"    # Z

    .line 198
    if-nez p1, :cond_0

    .line 199
    const-string v0, "MultiSenceConfig"

    const-string v1, "invalid operation when add app to support VRS"

    invoke-static {v0, v1}, Landroid/util/Slog;->w(Ljava/lang/String;Ljava/lang/String;)I

    .line 200
    return-void

    .line 202
    :cond_0
    iget-object v0, p0, Lcom/miui/server/multisence/MultiSenceConfig;->mWhiteListVRS:Ljava/util/Set;

    invoke-interface {v0, p1}, Ljava/util/Set;->contains(Ljava/lang/Object;)Z

    move-result v0

    .line 203
    .local v0, "isContained":Z
    if-eqz p2, :cond_1

    if-nez v0, :cond_1

    .line 204
    iget-object v1, p0, Lcom/miui/server/multisence/MultiSenceConfig;->mWhiteListVRS:Ljava/util/Set;

    invoke-interface {v1, p1}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 205
    :cond_1
    if-nez p2, :cond_2

    if-eqz v0, :cond_2

    .line 206
    iget-object v1, p0, Lcom/miui/server/multisence/MultiSenceConfig;->mWhiteListVRS:Ljava/util/Set;

    invoke-interface {v1, p1}, Ljava/util/Set;->remove(Ljava/lang/Object;)Z

    .line 208
    :cond_2
    :goto_0
    return-void
.end method
