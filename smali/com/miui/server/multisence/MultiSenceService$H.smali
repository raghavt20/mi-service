.class Lcom/miui/server/multisence/MultiSenceService$H;
.super Landroid/os/Handler;
.source "MultiSenceService.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/miui/server/multisence/MultiSenceService;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "H"
.end annotation


# instance fields
.field final synthetic this$0:Lcom/miui/server/multisence/MultiSenceService;


# direct methods
.method constructor <init>(Lcom/miui/server/multisence/MultiSenceService;Landroid/os/Looper;)V
    .locals 0
    .param p2, "looper"    # Landroid/os/Looper;

    .line 222
    iput-object p1, p0, Lcom/miui/server/multisence/MultiSenceService$H;->this$0:Lcom/miui/server/multisence/MultiSenceService;

    .line 223
    invoke-direct {p0, p2}, Landroid/os/Handler;-><init>(Landroid/os/Looper;)V

    .line 224
    return-void
.end method


# virtual methods
.method public handleMessage(Landroid/os/Message;)V
    .locals 6
    .param p1, "msg"    # Landroid/os/Message;

    .line 228
    iget v0, p1, Landroid/os/Message;->arg1:I

    .line 229
    .local v0, "uid":I
    iget v1, p1, Landroid/os/Message;->arg2:I

    if-nez v1, :cond_0

    const/4 v1, 0x0

    goto :goto_0

    :cond_0
    const/4 v1, 0x1

    .line 230
    .local v1, "isStarted":Z
    :goto_0
    iget-object v2, p1, Landroid/os/Message;->obj:Ljava/lang/Object;

    check-cast v2, [I

    .line 231
    .local v2, "tids":[I
    iget v3, p1, Landroid/os/Message;->what:I

    sparse-switch v3, :sswitch_data_0

    .line 256
    iget-object v3, p0, Lcom/miui/server/multisence/MultiSenceService$H;->this$0:Lcom/miui/server/multisence/MultiSenceService;

    invoke-static {v3}, Lcom/miui/server/multisence/MultiSenceService;->-$$Nest$fgetmPolicy(Lcom/miui/server/multisence/MultiSenceService;)Lcom/miui/server/multisence/MultiSenceServicePolicy;

    move-result-object v3

    const/4 v4, 0x0

    invoke-virtual {v3, v0, v2, v1, v4}, Lcom/miui/server/multisence/MultiSenceServicePolicy;->dynamicSenceSchedDefault(I[IZLjava/lang/String;)V

    goto/16 :goto_1

    .line 253
    :sswitch_0
    iget-object v3, p0, Lcom/miui/server/multisence/MultiSenceService$H;->this$0:Lcom/miui/server/multisence/MultiSenceService;

    invoke-static {v3}, Lcom/miui/server/multisence/MultiSenceService;->-$$Nest$fgetmPolicy(Lcom/miui/server/multisence/MultiSenceService;)Lcom/miui/server/multisence/MultiSenceServicePolicy;

    move-result-object v3

    invoke-virtual {v3, v0, v2, v1}, Lcom/miui/server/multisence/MultiSenceServicePolicy;->dynamicSenceSchedFloatingWindowMove(I[IZ)V

    .line 254
    goto/16 :goto_1

    .line 248
    :sswitch_1
    iget-object v3, p0, Lcom/miui/server/multisence/MultiSenceService$H;->this$0:Lcom/miui/server/multisence/MultiSenceService;

    invoke-static {v3}, Lcom/miui/server/multisence/MultiSenceService;->-$$Nest$fgetmPolicy(Lcom/miui/server/multisence/MultiSenceService;)Lcom/miui/server/multisence/MultiSenceServicePolicy;

    move-result-object v3

    iget-object v4, p0, Lcom/miui/server/multisence/MultiSenceService$H;->this$0:Lcom/miui/server/multisence/MultiSenceService;

    invoke-static {v4}, Lcom/miui/server/multisence/MultiSenceService;->-$$Nest$fgetmultiSencePackages(Lcom/miui/server/multisence/MultiSenceService;)Ljava/util/Map;

    move-result-object v4

    const/16 v5, 0x4001

    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    invoke-interface {v4, v5}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Ljava/lang/String;

    invoke-virtual {v3, v0, v2, v1, v4}, Lcom/miui/server/multisence/MultiSenceServicePolicy;->dynamicSenceSchedMWSActionMove(I[IZLjava/lang/String;)V

    .line 249
    goto/16 :goto_1

    .line 251
    :sswitch_2
    goto/16 :goto_1

    .line 245
    :sswitch_3
    iget-object v3, p0, Lcom/miui/server/multisence/MultiSenceService$H;->this$0:Lcom/miui/server/multisence/MultiSenceService;

    invoke-static {v3}, Lcom/miui/server/multisence/MultiSenceService;->-$$Nest$fgetmPolicy(Lcom/miui/server/multisence/MultiSenceService;)Lcom/miui/server/multisence/MultiSenceServicePolicy;

    move-result-object v3

    iget-object v4, p0, Lcom/miui/server/multisence/MultiSenceService$H;->this$0:Lcom/miui/server/multisence/MultiSenceService;

    invoke-static {v4}, Lcom/miui/server/multisence/MultiSenceService;->-$$Nest$fgetmultiSencePackages(Lcom/miui/server/multisence/MultiSenceService;)Ljava/util/Map;

    move-result-object v4

    const/16 v5, 0x2001

    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    invoke-interface {v4, v5}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Ljava/lang/String;

    invoke-virtual {v3, v0, v2, v1, v4}, Lcom/miui/server/multisence/MultiSenceServicePolicy;->dynamicSenceSchedSplitscreenctionDivider(I[IZLjava/lang/String;)V

    .line 246
    goto :goto_1

    .line 242
    :sswitch_4
    iget-object v3, p0, Lcom/miui/server/multisence/MultiSenceService$H;->this$0:Lcom/miui/server/multisence/MultiSenceService;

    invoke-static {v3}, Lcom/miui/server/multisence/MultiSenceService;->-$$Nest$fgetmPolicy(Lcom/miui/server/multisence/MultiSenceService;)Lcom/miui/server/multisence/MultiSenceServicePolicy;

    move-result-object v3

    iget-object v4, p0, Lcom/miui/server/multisence/MultiSenceService$H;->this$0:Lcom/miui/server/multisence/MultiSenceService;

    invoke-static {v4}, Lcom/miui/server/multisence/MultiSenceService;->-$$Nest$fgetmultiSencePackages(Lcom/miui/server/multisence/MultiSenceService;)Ljava/util/Map;

    move-result-object v4

    const/16 v5, 0x1006

    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    invoke-interface {v4, v5}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Ljava/lang/String;

    invoke-virtual {v3, v0, v2, v1, v4}, Lcom/miui/server/multisence/MultiSenceServicePolicy;->dynamicSenceSchedFreeformActionElude(I[IZLjava/lang/String;)V

    .line 243
    goto :goto_1

    .line 239
    :sswitch_5
    iget-object v3, p0, Lcom/miui/server/multisence/MultiSenceService$H;->this$0:Lcom/miui/server/multisence/MultiSenceService;

    invoke-static {v3}, Lcom/miui/server/multisence/MultiSenceService;->-$$Nest$fgetmPolicy(Lcom/miui/server/multisence/MultiSenceService;)Lcom/miui/server/multisence/MultiSenceServicePolicy;

    move-result-object v3

    iget-object v4, p0, Lcom/miui/server/multisence/MultiSenceService$H;->this$0:Lcom/miui/server/multisence/MultiSenceService;

    invoke-static {v4}, Lcom/miui/server/multisence/MultiSenceService;->-$$Nest$fgetmultiSencePackages(Lcom/miui/server/multisence/MultiSenceService;)Ljava/util/Map;

    move-result-object v4

    const/16 v5, 0x1005

    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    invoke-interface {v4, v5}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Ljava/lang/String;

    invoke-virtual {v3, v0, v2, v1, v4}, Lcom/miui/server/multisence/MultiSenceServicePolicy;->dynamicSenceSchedFreeformActionEnterFullScreen(I[IZLjava/lang/String;)V

    .line 240
    goto :goto_1

    .line 236
    :sswitch_6
    iget-object v3, p0, Lcom/miui/server/multisence/MultiSenceService$H;->this$0:Lcom/miui/server/multisence/MultiSenceService;

    invoke-static {v3}, Lcom/miui/server/multisence/MultiSenceService;->-$$Nest$fgetmPolicy(Lcom/miui/server/multisence/MultiSenceService;)Lcom/miui/server/multisence/MultiSenceServicePolicy;

    move-result-object v3

    iget-object v4, p0, Lcom/miui/server/multisence/MultiSenceService$H;->this$0:Lcom/miui/server/multisence/MultiSenceService;

    invoke-static {v4}, Lcom/miui/server/multisence/MultiSenceService;->-$$Nest$fgetmultiSencePackages(Lcom/miui/server/multisence/MultiSenceService;)Ljava/util/Map;

    move-result-object v4

    const/16 v5, 0x1004

    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    invoke-interface {v4, v5}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Ljava/lang/String;

    invoke-virtual {v3, v0, v2, v1, v4}, Lcom/miui/server/multisence/MultiSenceServicePolicy;->dynamicSenceSchedFreeformActionResize(I[IZLjava/lang/String;)V

    .line 237
    goto :goto_1

    .line 233
    :sswitch_7
    iget-object v3, p0, Lcom/miui/server/multisence/MultiSenceService$H;->this$0:Lcom/miui/server/multisence/MultiSenceService;

    invoke-static {v3}, Lcom/miui/server/multisence/MultiSenceService;->-$$Nest$fgetmPolicy(Lcom/miui/server/multisence/MultiSenceService;)Lcom/miui/server/multisence/MultiSenceServicePolicy;

    move-result-object v3

    iget-object v4, p0, Lcom/miui/server/multisence/MultiSenceService$H;->this$0:Lcom/miui/server/multisence/MultiSenceService;

    invoke-static {v4}, Lcom/miui/server/multisence/MultiSenceService;->-$$Nest$fgetmultiSencePackages(Lcom/miui/server/multisence/MultiSenceService;)Ljava/util/Map;

    move-result-object v4

    const/16 v5, 0x1001

    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    invoke-interface {v4, v5}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Ljava/lang/String;

    invoke-virtual {v3, v0, v2, v1, v4}, Lcom/miui/server/multisence/MultiSenceServicePolicy;->dynamicSenceSchedFreeformActionMove(I[IZLjava/lang/String;)V

    .line 234
    nop

    .line 259
    :goto_1
    return-void

    nop

    :sswitch_data_0
    .sparse-switch
        0x1001 -> :sswitch_7
        0x1004 -> :sswitch_6
        0x1005 -> :sswitch_5
        0x1006 -> :sswitch_4
        0x2001 -> :sswitch_3
        0x4000 -> :sswitch_2
        0x4001 -> :sswitch_1
        0x5001 -> :sswitch_0
    .end sparse-switch
.end method
