class com.miui.server.multisence.MultiSenceDynamicController$1 extends android.database.ContentObserver {
	 /* .source "MultiSenceDynamicController.java" */
	 /* # annotations */
	 /* .annotation system Ldalvik/annotation/EnclosingMethod; */
	 /* value = Lcom/miui/server/multisence/MultiSenceDynamicController;->registerCloudObserver()V */
} // .end annotation
/* .annotation system Ldalvik/annotation/InnerClass; */
/* accessFlags = 0x0 */
/* name = null */
} // .end annotation
/* # instance fields */
final com.miui.server.multisence.MultiSenceDynamicController this$0; //synthetic
/* # direct methods */
 com.miui.server.multisence.MultiSenceDynamicController$1 ( ) {
/* .locals 0 */
/* .param p1, "this$0" # Lcom/miui/server/multisence/MultiSenceDynamicController; */
/* .param p2, "handler" # Landroid/os/Handler; */
/* .line 59 */
this.this$0 = p1;
/* invoke-direct {p0, p2}, Landroid/database/ContentObserver;-><init>(Landroid/os/Handler;)V */
return;
} // .end method
/* # virtual methods */
public void onChange ( Boolean p0, android.net.Uri p1 ) {
/* .locals 8 */
/* .param p1, "selfChange" # Z */
/* .param p2, "uri" # Landroid/net/Uri; */
/* .line 62 */
final String v0 = "persist.multisence.nonsupport"; // const-string v0, "persist.multisence.nonsupport"
/* sget-boolean v1, Lcom/miui/server/multisence/MultiSenceConfig;->DEBUG_DYNAMIC_CONTROLLER:Z */
final String v2 = "onChange"; // const-string v2, "onChange"
com.miui.server.multisence.MultiSenceServiceUtils .msLogD ( v1,v2 );
/* .line 63 */
int v1 = -2; // const/4 v1, -0x2
if ( p2 != null) { // if-eqz p2, :cond_1
	 final String v2 = "cloud_multisence_enable"; // const-string v2, "cloud_multisence_enable"
	 android.provider.Settings$System .getUriFor ( v2 );
	 v3 = 	 (( android.net.Uri ) p2 ).equals ( v3 ); // invoke-virtual {p2, v3}, Landroid/net/Uri;->equals(Ljava/lang/Object;)Z
	 if ( v3 != null) { // if-eqz v3, :cond_1
		 /* .line 64 */
		 v3 = this.this$0;
		 com.miui.server.multisence.MultiSenceDynamicController .-$$Nest$fgetmContext ( v3 );
		 (( android.content.Context ) v3 ).getContentResolver ( ); // invoke-virtual {v3}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;
		 android.provider.Settings$System .getStringForUser ( v3,v2,v1 );
		 v2 = 		 java.lang.Boolean .parseBoolean ( v2 );
		 com.miui.server.multisence.MultiSenceDynamicController.IS_CLOUD_ENABLED = (v2!= 0);
		 /* .line 66 */
		 /* sget-boolean v2, Lcom/miui/server/multisence/MultiSenceDynamicController;->IS_CLOUD_ENABLED:Z */
		 java.lang.String .valueOf ( v2 );
		 final String v3 = "persist.multisence.cloud.enable"; // const-string v3, "persist.multisence.cloud.enable"
		 android.os.SystemProperties .set ( v3,v2 );
		 /* .line 67 */
		 v2 = this.this$0;
		 com.miui.server.multisence.MultiSenceDynamicController .-$$Nest$fgetTAG ( v2 );
		 /* new-instance v3, Ljava/lang/StringBuilder; */
		 /* invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V */
		 final String v4 = "cloud control set received: "; // const-string v4, "cloud control set received: "
		 (( java.lang.StringBuilder ) v3 ).append ( v4 ); // invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
		 /* sget-boolean v4, Lcom/miui/server/multisence/MultiSenceDynamicController;->IS_CLOUD_ENABLED:Z */
		 (( java.lang.StringBuilder ) v3 ).append ( v4 ); // invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;
		 (( java.lang.StringBuilder ) v3 ).toString ( ); // invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
		 android.util.Slog .i ( v2,v3 );
		 /* .line 69 */
		 /* sget-boolean v2, Lcom/miui/server/multisence/MultiSenceDynamicController;->IS_CLOUD_ENABLED:Z */
		 /* if-nez v2, :cond_0 */
		 /* .line 70 */
		 v2 = this.this$0;
		 com.miui.server.multisence.MultiSenceDynamicController .-$$Nest$fgetTAG ( v2 );
		 /* const-string/jumbo v3, "unregister listner due to cloud controller" */
		 android.util.Slog .i ( v2,v3 );
		 /* .line 71 */
		 v2 = this.this$0;
		 com.miui.server.multisence.MultiSenceDynamicController .-$$Nest$fgetmService ( v2 );
		 (( com.miui.server.multisence.MultiSenceService ) v2 ).unregisterMultiTaskActionListener ( ); // invoke-virtual {v2}, Lcom/miui/server/multisence/MultiSenceService;->unregisterMultiTaskActionListener()V
		 /* .line 72 */
		 v2 = this.this$0;
		 com.miui.server.multisence.MultiSenceDynamicController .-$$Nest$fgetmService ( v2 );
		 (( com.miui.server.multisence.MultiSenceService ) v2 ).clearServiceStatus ( ); // invoke-virtual {v2}, Lcom/miui/server/multisence/MultiSenceService;->clearServiceStatus()V
		 /* .line 74 */
	 } // :cond_0
	 v2 = this.this$0;
	 com.miui.server.multisence.MultiSenceDynamicController .-$$Nest$fgetTAG ( v2 );
	 final String v3 = "register listner due to cloud controller"; // const-string v3, "register listner due to cloud controller"
	 android.util.Slog .i ( v2,v3 );
	 /* .line 75 */
	 v2 = this.this$0;
	 com.miui.server.multisence.MultiSenceDynamicController .-$$Nest$fgetmService ( v2 );
	 (( com.miui.server.multisence.MultiSenceService ) v2 ).registerMultiTaskActionListener ( ); // invoke-virtual {v2}, Lcom/miui/server/multisence/MultiSenceService;->registerMultiTaskActionListener()V
	 /* .line 77 */
	 /* sget-boolean v2, Lcom/miui/server/multisence/MultiSenceDynamicController;->IS_CLOUD_ENABLED:Z */
	 java.lang.String .valueOf ( v2 );
	 final String v3 = "persist.multisence.enable"; // const-string v3, "persist.multisence.enable"
	 android.os.SystemProperties .set ( v3,v2 );
	 /* .line 78 */
	 v2 = this.this$0;
	 com.miui.server.multisence.MultiSenceDynamicController .-$$Nest$fgetmService ( v2 );
	 /* sget-boolean v3, Lcom/miui/server/multisence/MultiSenceDynamicController;->IS_CLOUD_ENABLED:Z */
	 (( com.miui.server.multisence.MultiSenceService ) v2 ).notifyMultiSenceEnable ( v3 ); // invoke-virtual {v2, v3}, Lcom/miui/server/multisence/MultiSenceService;->notifyMultiSenceEnable(Z)V
	 /* .line 80 */
} // :goto_0
com.miui.server.multisence.MultiSenceConfig .getInstance ( );
(( com.miui.server.multisence.MultiSenceConfig ) v2 ).updateSubFuncStatus ( ); // invoke-virtual {v2}, Lcom/miui/server/multisence/MultiSenceConfig;->updateSubFuncStatus()V
/* .line 83 */
} // :cond_1
int v2 = 0; // const/4 v2, 0x0
if ( p2 != null) { // if-eqz p2, :cond_6
final String v3 = "POWER_PERFORMANCE_MODE_OPEN"; // const-string v3, "POWER_PERFORMANCE_MODE_OPEN"
android.provider.Settings$System .getUriFor ( v3 );
v4 = (( android.net.Uri ) p2 ).equals ( v4 ); // invoke-virtual {p2, v4}, Landroid/net/Uri;->equals(Ljava/lang/Object;)Z
if ( v4 != null) { // if-eqz v4, :cond_6
	 /* .line 85 */
	 try { // :try_start_0
		 v4 = this.this$0;
		 com.miui.server.multisence.MultiSenceDynamicController .-$$Nest$fgetmContext ( v4 );
		 /* .line 86 */
		 (( android.content.Context ) v4 ).getContentResolver ( ); // invoke-virtual {v4}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;
		 /* .line 85 */
		 v3 = 		 android.provider.Settings$System .getIntForUser ( v4,v3,v2,v1 );
		 int v4 = 1; // const/4 v4, 0x1
		 if ( v3 != null) { // if-eqz v3, :cond_2
			 /* move v3, v4 */
		 } // :cond_2
		 /* move v3, v2 */
		 /* .line 87 */
		 /* .local v3, "isPowerMode":Z */
	 } // :goto_1
	 v5 = this.this$0;
	 com.miui.server.multisence.MultiSenceDynamicController .-$$Nest$fgetmService ( v5 );
	 (( com.miui.server.multisence.MultiSenceService ) v5 ).getPolicy ( ); // invoke-virtual {v5}, Lcom/miui/server/multisence/MultiSenceService;->getPolicy()Lcom/miui/server/multisence/MultiSenceServicePolicy;
	 if ( v5 != null) { // if-eqz v5, :cond_4
		 /* .line 88 */
		 v5 = this.this$0;
		 com.miui.server.multisence.MultiSenceDynamicController .-$$Nest$fgetmService ( v5 );
		 (( com.miui.server.multisence.MultiSenceService ) v5 ).getPolicy ( ); // invoke-virtual {v5}, Lcom/miui/server/multisence/MultiSenceService;->getPolicy()Lcom/miui/server/multisence/MultiSenceServicePolicy;
		 /* if-nez v3, :cond_3 */
		 /* move v6, v4 */
	 } // :cond_3
	 /* move v6, v2 */
} // :goto_2
(( com.miui.server.multisence.MultiSenceServicePolicy ) v5 ).updatePowerMode ( v6 ); // invoke-virtual {v5, v6}, Lcom/miui/server/multisence/MultiSenceServicePolicy;->updatePowerMode(Z)V
/* .line 90 */
} // :cond_4
/* sget-boolean v5, Lcom/miui/server/multisence/MultiSenceConfig;->DEBUG_DYNAMIC_CONTROLLER:Z */
/* new-instance v6, Ljava/lang/StringBuilder; */
/* invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V */
/* const-string/jumbo v7, "updatePowerMode isPowerMode: " */
(( java.lang.StringBuilder ) v6 ).append ( v7 ); // invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
/* if-nez v3, :cond_5 */
} // :cond_5
/* move v4, v2 */
} // :goto_3
(( java.lang.StringBuilder ) v6 ).append ( v4 ); // invoke-virtual {v6, v4}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v4 ).toString ( ); // invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
com.miui.server.multisence.MultiSenceServiceUtils .msLogD ( v5,v4 );
/* :try_end_0 */
/* .catch Ljava/lang/Exception; {:try_start_0 ..:try_end_0} :catch_0 */
/* .line 93 */
} // .end local v3 # "isPowerMode":Z
/* .line 91 */
/* :catch_0 */
/* move-exception v3 */
/* .line 92 */
/* .local v3, "e":Ljava/lang/Exception; */
v4 = this.this$0;
com.miui.server.multisence.MultiSenceDynamicController .-$$Nest$fgetTAG ( v4 );
/* const-string/jumbo v5, "updatePowerModeParas fail" */
android.util.Slog .e ( v4,v5 );
/* .line 96 */
} // .end local v3 # "e":Ljava/lang/Exception;
} // :cond_6
} // :goto_4
if ( p2 != null) { // if-eqz p2, :cond_8
final String v3 = "cloud_multisence_subfunc_nonsupport"; // const-string v3, "cloud_multisence_subfunc_nonsupport"
android.provider.Settings$System .getUriFor ( v3 );
v4 = (( android.net.Uri ) p2 ).equals ( v4 ); // invoke-virtual {p2, v4}, Landroid/net/Uri;->equals(Ljava/lang/Object;)Z
if ( v4 != null) { // if-eqz v4, :cond_8
/* .line 98 */
try { // :try_start_1
v4 = this.this$0;
com.miui.server.multisence.MultiSenceDynamicController .-$$Nest$fgetmContext ( v4 );
(( android.content.Context ) v4 ).getContentResolver ( ); // invoke-virtual {v4}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;
android.provider.Settings$System .getStringForUser ( v4,v3,v1 );
v1 = java.lang.Integer .parseInt ( v1 );
/* .line 100 */
/* .local v1, "nonSupportCode":I */
v2 = android.os.SystemProperties .getInt ( v0,v2 );
/* .line 101 */
/* .local v2, "oldNonSupportCode":I */
/* if-eq v1, v2, :cond_7 */
/* .line 102 */
java.lang.String .valueOf ( v1 );
android.os.SystemProperties .set ( v0,v3 );
/* .line 103 */
com.miui.server.multisence.MultiSenceConfig .getInstance ( );
(( com.miui.server.multisence.MultiSenceConfig ) v0 ).updateSubFuncStatus ( ); // invoke-virtual {v0}, Lcom/miui/server/multisence/MultiSenceConfig;->updateSubFuncStatus()V
/* .line 105 */
} // :cond_7
/* sget-boolean v0, Lcom/miui/server/multisence/MultiSenceConfig;->DEBUG_DYNAMIC_CONTROLLER:Z */
/* const-string/jumbo v3, "sub-function is not changed." */
com.miui.server.multisence.MultiSenceServiceUtils .msLogD ( v0,v3 );
/* :try_end_1 */
/* .catch Ljava/lang/Exception; {:try_start_1 ..:try_end_1} :catch_1 */
/* .line 109 */
} // .end local v1 # "nonSupportCode":I
} // .end local v2 # "oldNonSupportCode":I
} // :goto_5
/* .line 107 */
/* :catch_1 */
/* move-exception v0 */
/* .line 108 */
/* .local v0, "e":Ljava/lang/Exception; */
v1 = this.this$0;
com.miui.server.multisence.MultiSenceDynamicController .-$$Nest$fgetTAG ( v1 );
/* const-string/jumbo v2, "update sub-function nonSupport parameter fail." */
android.util.Slog .e ( v1,v2 );
/* .line 112 */
} // .end local v0 # "e":Ljava/lang/Exception;
} // :cond_8
} // :goto_6
return;
} // .end method
