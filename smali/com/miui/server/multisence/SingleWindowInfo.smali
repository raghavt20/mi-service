.class public Lcom/miui/server/multisence/SingleWindowInfo;
.super Ljava/lang/Object;
.source "SingleWindowInfo.java"

# interfaces
.implements Ljava/lang/Comparable;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/miui/server/multisence/SingleWindowInfo$WindowForm;,
        Lcom/miui/server/multisence/SingleWindowInfo$AppType;,
        Lcom/miui/server/multisence/SingleWindowInfo$WindowChangeState;,
        Lcom/miui/server/multisence/SingleWindowInfo$ScheduleStatus;,
        Lcom/miui/server/multisence/SingleWindowInfo$FreeFormInfo;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Ljava/lang/Comparable<",
        "Lcom/miui/server/multisence/SingleWindowInfo;",
        ">;"
    }
.end annotation


# static fields
.field private static final TAG:Ljava/lang/String; = "SingleWindowInfo"


# instance fields
.field private FPS_LIMIT_DEFAULT:I

.field private FPS_LIMIT_IN_FREEFORM_MINI:I

.field private FPS_LIMIT_IN_FULLSCREEN_COMMON:I

.field private isCovered:Z

.field private isFPSLimited:Z

.field private isHighPriority:Z

.field private isInputFocused:Z

.field private isVisible:Z

.field private isVrsCovered:Z

.field private layerOrder:I

.field private mChangeStatus:Lcom/miui/server/multisence/SingleWindowInfo$WindowChangeState;

.field private mForm:Lcom/miui/server/multisence/SingleWindowInfo$WindowForm;

.field private mPackageName:Ljava/lang/String;

.field private mRect:Landroid/graphics/Rect;

.field private mScheduleStatus:Ljava/util/Set;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Set<",
            "Lcom/miui/server/multisence/SingleWindowInfo$ScheduleStatus;",
            ">;"
        }
    .end annotation
.end field

.field private mType:Lcom/miui/server/multisence/SingleWindowInfo$AppType;

.field private mWindowMode:Ljava/lang/String;

.field private myPid:I

.field private myRenderThreadTid:I

.field private myUid:I

.field private windowCount:I


# direct methods
.method public constructor <init>()V
    .locals 3

    .line 82
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 25
    sget-object v0, Lcom/miui/server/multisence/SingleWindowInfo$WindowForm;->UNKNOWN:Lcom/miui/server/multisence/SingleWindowInfo$WindowForm;

    iput-object v0, p0, Lcom/miui/server/multisence/SingleWindowInfo;->mForm:Lcom/miui/server/multisence/SingleWindowInfo$WindowForm;

    .line 26
    sget-object v0, Lcom/miui/server/multisence/SingleWindowInfo$AppType;->UNKNOWN:Lcom/miui/server/multisence/SingleWindowInfo$AppType;

    iput-object v0, p0, Lcom/miui/server/multisence/SingleWindowInfo;->mType:Lcom/miui/server/multisence/SingleWindowInfo$AppType;

    .line 27
    const-string/jumbo v0, "unknown"

    iput-object v0, p0, Lcom/miui/server/multisence/SingleWindowInfo;->mWindowMode:Ljava/lang/String;

    .line 28
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/miui/server/multisence/SingleWindowInfo;->isInputFocused:Z

    .line 29
    iput-boolean v0, p0, Lcom/miui/server/multisence/SingleWindowInfo;->isHighPriority:Z

    .line 30
    iput-boolean v0, p0, Lcom/miui/server/multisence/SingleWindowInfo;->isVisible:Z

    .line 31
    iput-boolean v0, p0, Lcom/miui/server/multisence/SingleWindowInfo;->isCovered:Z

    .line 32
    iput-boolean v0, p0, Lcom/miui/server/multisence/SingleWindowInfo;->isVrsCovered:Z

    .line 33
    sget-object v1, Lcom/miui/server/multisence/SingleWindowInfo$WindowChangeState;->UNKNOWN:Lcom/miui/server/multisence/SingleWindowInfo$WindowChangeState;

    iput-object v1, p0, Lcom/miui/server/multisence/SingleWindowInfo;->mChangeStatus:Lcom/miui/server/multisence/SingleWindowInfo$WindowChangeState;

    .line 34
    new-instance v1, Ljava/util/HashSet;

    invoke-direct {v1}, Ljava/util/HashSet;-><init>()V

    iput-object v1, p0, Lcom/miui/server/multisence/SingleWindowInfo;->mScheduleStatus:Ljava/util/Set;

    .line 36
    const/16 v1, 0x3c

    iput v1, p0, Lcom/miui/server/multisence/SingleWindowInfo;->FPS_LIMIT_DEFAULT:I

    .line 37
    const/16 v2, 0x1e

    iput v2, p0, Lcom/miui/server/multisence/SingleWindowInfo;->FPS_LIMIT_IN_FREEFORM_MINI:I

    .line 38
    iput v1, p0, Lcom/miui/server/multisence/SingleWindowInfo;->FPS_LIMIT_IN_FULLSCREEN_COMMON:I

    .line 39
    iput-boolean v0, p0, Lcom/miui/server/multisence/SingleWindowInfo;->isFPSLimited:Z

    .line 44
    const/4 v0, -0x1

    iput v0, p0, Lcom/miui/server/multisence/SingleWindowInfo;->layerOrder:I

    .line 45
    const/4 v0, 0x1

    iput v0, p0, Lcom/miui/server/multisence/SingleWindowInfo;->windowCount:I

    .line 83
    sget-boolean v0, Lcom/miui/server/multisence/MultiSenceConfig;->DEBUG:Z

    const-string v1, "new a SingleWindowInfo object."

    invoke-static {v0, v1}, Lcom/miui/server/multisence/MultiSenceServiceUtils;->msLogD(ZLjava/lang/String;)V

    .line 84
    return-void
.end method

.method public constructor <init>(Ljava/lang/String;Lcom/miui/server/multisence/SingleWindowInfo$AppType;)V
    .locals 3
    .param p1, "name"    # Ljava/lang/String;
    .param p2, "type"    # Lcom/miui/server/multisence/SingleWindowInfo$AppType;

    .line 86
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 25
    sget-object v0, Lcom/miui/server/multisence/SingleWindowInfo$WindowForm;->UNKNOWN:Lcom/miui/server/multisence/SingleWindowInfo$WindowForm;

    iput-object v0, p0, Lcom/miui/server/multisence/SingleWindowInfo;->mForm:Lcom/miui/server/multisence/SingleWindowInfo$WindowForm;

    .line 26
    sget-object v0, Lcom/miui/server/multisence/SingleWindowInfo$AppType;->UNKNOWN:Lcom/miui/server/multisence/SingleWindowInfo$AppType;

    iput-object v0, p0, Lcom/miui/server/multisence/SingleWindowInfo;->mType:Lcom/miui/server/multisence/SingleWindowInfo$AppType;

    .line 27
    const-string/jumbo v0, "unknown"

    iput-object v0, p0, Lcom/miui/server/multisence/SingleWindowInfo;->mWindowMode:Ljava/lang/String;

    .line 28
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/miui/server/multisence/SingleWindowInfo;->isInputFocused:Z

    .line 29
    iput-boolean v0, p0, Lcom/miui/server/multisence/SingleWindowInfo;->isHighPriority:Z

    .line 30
    iput-boolean v0, p0, Lcom/miui/server/multisence/SingleWindowInfo;->isVisible:Z

    .line 31
    iput-boolean v0, p0, Lcom/miui/server/multisence/SingleWindowInfo;->isCovered:Z

    .line 32
    iput-boolean v0, p0, Lcom/miui/server/multisence/SingleWindowInfo;->isVrsCovered:Z

    .line 33
    sget-object v1, Lcom/miui/server/multisence/SingleWindowInfo$WindowChangeState;->UNKNOWN:Lcom/miui/server/multisence/SingleWindowInfo$WindowChangeState;

    iput-object v1, p0, Lcom/miui/server/multisence/SingleWindowInfo;->mChangeStatus:Lcom/miui/server/multisence/SingleWindowInfo$WindowChangeState;

    .line 34
    new-instance v1, Ljava/util/HashSet;

    invoke-direct {v1}, Ljava/util/HashSet;-><init>()V

    iput-object v1, p0, Lcom/miui/server/multisence/SingleWindowInfo;->mScheduleStatus:Ljava/util/Set;

    .line 36
    const/16 v1, 0x3c

    iput v1, p0, Lcom/miui/server/multisence/SingleWindowInfo;->FPS_LIMIT_DEFAULT:I

    .line 37
    const/16 v2, 0x1e

    iput v2, p0, Lcom/miui/server/multisence/SingleWindowInfo;->FPS_LIMIT_IN_FREEFORM_MINI:I

    .line 38
    iput v1, p0, Lcom/miui/server/multisence/SingleWindowInfo;->FPS_LIMIT_IN_FULLSCREEN_COMMON:I

    .line 39
    iput-boolean v0, p0, Lcom/miui/server/multisence/SingleWindowInfo;->isFPSLimited:Z

    .line 44
    const/4 v0, -0x1

    iput v0, p0, Lcom/miui/server/multisence/SingleWindowInfo;->layerOrder:I

    .line 45
    const/4 v0, 0x1

    iput v0, p0, Lcom/miui/server/multisence/SingleWindowInfo;->windowCount:I

    .line 87
    iput-object p1, p0, Lcom/miui/server/multisence/SingleWindowInfo;->mPackageName:Ljava/lang/String;

    .line 88
    const-string v0, "com.miui.home"

    invoke-virtual {p1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 89
    sget-object v0, Lcom/miui/server/multisence/SingleWindowInfo$AppType;->HOME:Lcom/miui/server/multisence/SingleWindowInfo$AppType;

    iput-object v0, p0, Lcom/miui/server/multisence/SingleWindowInfo;->mType:Lcom/miui/server/multisence/SingleWindowInfo$AppType;

    goto :goto_0

    .line 91
    :cond_0
    iput-object p2, p0, Lcom/miui/server/multisence/SingleWindowInfo;->mType:Lcom/miui/server/multisence/SingleWindowInfo$AppType;

    .line 93
    :goto_0
    return-void
.end method


# virtual methods
.method public checkSchedStatus(Lcom/miui/server/multisence/SingleWindowInfo$ScheduleStatus;)Z
    .locals 1
    .param p1, "status"    # Lcom/miui/server/multisence/SingleWindowInfo$ScheduleStatus;

    .line 250
    iget-object v0, p0, Lcom/miui/server/multisence/SingleWindowInfo;->mScheduleStatus:Ljava/util/Set;

    invoke-interface {v0, p1}, Ljava/util/Set;->contains(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 251
    const/4 v0, 0x1

    return v0

    .line 253
    :cond_0
    const/4 v0, 0x0

    return v0
.end method

.method public compareTo(Lcom/miui/server/multisence/SingleWindowInfo;)I
    .locals 2
    .param p1, "window"    # Lcom/miui/server/multisence/SingleWindowInfo;

    .line 181
    invoke-virtual {p1}, Lcom/miui/server/multisence/SingleWindowInfo;->getLayerOrder()I

    move-result v0

    invoke-virtual {p0}, Lcom/miui/server/multisence/SingleWindowInfo;->getLayerOrder()I

    move-result v1

    sub-int/2addr v0, v1

    return v0
.end method

.method public bridge synthetic compareTo(Ljava/lang/Object;)I
    .locals 0

    .line 21
    check-cast p1, Lcom/miui/server/multisence/SingleWindowInfo;

    invoke-virtual {p0, p1}, Lcom/miui/server/multisence/SingleWindowInfo;->compareTo(Lcom/miui/server/multisence/SingleWindowInfo;)I

    move-result p1

    return p1
.end method

.method public doSched()Lcom/miui/server/multisence/SingleWindowInfo;
    .locals 7

    .line 257
    sget-boolean v0, Lcom/miui/server/multisence/MultiSenceConfig;->DEBUG:Z

    invoke-virtual {p0}, Lcom/miui/server/multisence/SingleWindowInfo;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/miui/server/multisence/MultiSenceServiceUtils;->msLogD(ZLjava/lang/String;)V

    .line 258
    iget-boolean v0, p0, Lcom/miui/server/multisence/SingleWindowInfo;->isHighPriority:Z

    if-eqz v0, :cond_1

    .line 259
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "doSched app: "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lcom/miui/server/multisence/SingleWindowInfo;->mPackageName:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, " pid: "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget v1, p0, Lcom/miui/server/multisence/SingleWindowInfo;->myPid:I

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, " high priority: "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-boolean v1, p0, Lcom/miui/server/multisence/SingleWindowInfo;->isHighPriority:Z

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    const-wide/16 v1, 0x20

    invoke-static {v1, v2, v0}, Landroid/os/Trace;->traceBegin(JLjava/lang/String;)V

    .line 260
    invoke-static {}, Landroid/os/Process;->myUid()I

    move-result v0

    .line 261
    .local v0, "callingUid":I
    iget v3, p0, Lcom/miui/server/multisence/SingleWindowInfo;->myPid:I

    if-gtz v3, :cond_0

    .line 262
    sget-boolean v1, Lcom/miui/server/multisence/MultiSenceConfig;->DEBUG:Z

    const-string v2, "Pid recorded error"

    invoke-static {v1, v2}, Lcom/miui/server/multisence/MultiSenceServiceUtils;->msLogD(ZLjava/lang/String;)V

    .line 263
    return-object p0

    .line 265
    :cond_0
    invoke-static {}, Lcom/android/server/am/MiuiBoosterUtilsStub;->getInstance()Lcom/android/server/am/MiuiBoosterUtilsStub;

    move-result-object v3

    iget v4, p0, Lcom/miui/server/multisence/SingleWindowInfo;->myPid:I

    filled-new-array {v4}, [I

    move-result-object v4

    const/4 v5, 0x3

    const/16 v6, 0x7d0

    invoke-interface {v3, v0, v4, v5, v6}, Lcom/android/server/am/MiuiBoosterUtilsStub;->requestBindCore(I[III)I

    .line 267
    invoke-static {v1, v2}, Landroid/os/Trace;->traceEnd(J)V

    .line 269
    .end local v0    # "callingUid":I
    :cond_1
    return-object p0
.end method

.method public getAppType()Lcom/miui/server/multisence/SingleWindowInfo$AppType;
    .locals 1

    .line 114
    iget-object v0, p0, Lcom/miui/server/multisence/SingleWindowInfo;->mType:Lcom/miui/server/multisence/SingleWindowInfo$AppType;

    return-object v0
.end method

.method public getChangeStatus()Lcom/miui/server/multisence/SingleWindowInfo$WindowChangeState;
    .locals 1

    .line 278
    iget-object v0, p0, Lcom/miui/server/multisence/SingleWindowInfo;->mChangeStatus:Lcom/miui/server/multisence/SingleWindowInfo$WindowChangeState;

    return-object v0
.end method

.method public getLayerOrder()I
    .locals 1

    .line 158
    iget v0, p0, Lcom/miui/server/multisence/SingleWindowInfo;->layerOrder:I

    return v0
.end method

.method public getLimitFPS()I
    .locals 1

    .line 149
    iget v0, p0, Lcom/miui/server/multisence/SingleWindowInfo;->FPS_LIMIT_IN_FREEFORM_MINI:I

    return v0
.end method

.method public getPackageName()Ljava/lang/String;
    .locals 1

    .line 190
    iget-object v0, p0, Lcom/miui/server/multisence/SingleWindowInfo;->mPackageName:Ljava/lang/String;

    return-object v0
.end method

.method public getRectValue()Landroid/graphics/Rect;
    .locals 1

    .line 171
    iget-object v0, p0, Lcom/miui/server/multisence/SingleWindowInfo;->mRect:Landroid/graphics/Rect;

    return-object v0
.end method

.method public getWindowCount()I
    .locals 1

    .line 167
    iget v0, p0, Lcom/miui/server/multisence/SingleWindowInfo;->windowCount:I

    return v0
.end method

.method public getWindowForm()Lcom/miui/server/multisence/SingleWindowInfo$WindowForm;
    .locals 1

    .line 118
    iget-object v0, p0, Lcom/miui/server/multisence/SingleWindowInfo;->mForm:Lcom/miui/server/multisence/SingleWindowInfo$WindowForm;

    return-object v0
.end method

.method public getWindowingModeString()Ljava/lang/String;
    .locals 1

    .line 141
    iget-object v0, p0, Lcom/miui/server/multisence/SingleWindowInfo;->mWindowMode:Ljava/lang/String;

    return-object v0
.end method

.method public isCovered()Z
    .locals 1

    .line 106
    iget-boolean v0, p0, Lcom/miui/server/multisence/SingleWindowInfo;->isCovered:Z

    return v0
.end method

.method public isFPSLimited()Z
    .locals 1

    .line 137
    iget-boolean v0, p0, Lcom/miui/server/multisence/SingleWindowInfo;->isFPSLimited:Z

    return v0
.end method

.method public isHighPriority()Z
    .locals 1

    .line 203
    iget-boolean v0, p0, Lcom/miui/server/multisence/SingleWindowInfo;->isHighPriority:Z

    return v0
.end method

.method public isInFreeform()Z
    .locals 2

    .line 145
    iget-object v0, p0, Lcom/miui/server/multisence/SingleWindowInfo;->mWindowMode:Ljava/lang/String;

    const-string v1, "freeform"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    return v0
.end method

.method public isInputFocused()Z
    .locals 1

    .line 199
    iget-boolean v0, p0, Lcom/miui/server/multisence/SingleWindowInfo;->isInputFocused:Z

    return v0
.end method

.method public isVisible()Z
    .locals 1

    .line 212
    iget-boolean v0, p0, Lcom/miui/server/multisence/SingleWindowInfo;->isVisible:Z

    return v0
.end method

.method public isVrsCovered()Z
    .locals 1

    .line 110
    iget-boolean v0, p0, Lcom/miui/server/multisence/SingleWindowInfo;->isVrsCovered:Z

    return v0
.end method

.method public setChangeStatus(Lcom/miui/server/multisence/SingleWindowInfo$WindowChangeState;)Lcom/miui/server/multisence/SingleWindowInfo;
    .locals 0
    .param p1, "status"    # Lcom/miui/server/multisence/SingleWindowInfo$WindowChangeState;

    .line 273
    iput-object p1, p0, Lcom/miui/server/multisence/SingleWindowInfo;->mChangeStatus:Lcom/miui/server/multisence/SingleWindowInfo$WindowChangeState;

    .line 274
    return-object p0
.end method

.method public setCovered(Z)Lcom/miui/server/multisence/SingleWindowInfo;
    .locals 0
    .param p1, "covered"    # Z

    .line 96
    iput-boolean p1, p0, Lcom/miui/server/multisence/SingleWindowInfo;->isCovered:Z

    .line 97
    return-object p0
.end method

.method public setFPSLimited(Z)Lcom/miui/server/multisence/SingleWindowInfo;
    .locals 0
    .param p1, "isLimited"    # Z

    .line 127
    iput-boolean p1, p0, Lcom/miui/server/multisence/SingleWindowInfo;->isFPSLimited:Z

    .line 128
    return-object p0
.end method

.method public setFocused(Z)Lcom/miui/server/multisence/SingleWindowInfo;
    .locals 0
    .param p1, "focused"    # Z

    .line 194
    iput-boolean p1, p0, Lcom/miui/server/multisence/SingleWindowInfo;->isInputFocused:Z

    .line 195
    return-object p0
.end method

.method public setLayerOrder(I)Lcom/miui/server/multisence/SingleWindowInfo;
    .locals 0
    .param p1, "order"    # I

    .line 153
    iput p1, p0, Lcom/miui/server/multisence/SingleWindowInfo;->layerOrder:I

    .line 154
    return-object p0
.end method

.method public setPid(I)Lcom/miui/server/multisence/SingleWindowInfo;
    .locals 0
    .param p1, "pid"    # I

    .line 49
    iput p1, p0, Lcom/miui/server/multisence/SingleWindowInfo;->myPid:I

    .line 50
    return-object p0
.end method

.method public setRectValue(Landroid/graphics/Rect;)Lcom/miui/server/multisence/SingleWindowInfo;
    .locals 0
    .param p1, "rect"    # Landroid/graphics/Rect;

    .line 175
    iput-object p1, p0, Lcom/miui/server/multisence/SingleWindowInfo;->mRect:Landroid/graphics/Rect;

    .line 176
    return-object p0
.end method

.method public setRenderThreadTid(I)Lcom/miui/server/multisence/SingleWindowInfo;
    .locals 0
    .param p1, "tid"    # I

    .line 59
    iput p1, p0, Lcom/miui/server/multisence/SingleWindowInfo;->myRenderThreadTid:I

    .line 60
    return-object p0
.end method

.method public setSchedPriority(Z)Lcom/miui/server/multisence/SingleWindowInfo;
    .locals 0
    .param p1, "priority"    # Z

    .line 207
    iput-boolean p1, p0, Lcom/miui/server/multisence/SingleWindowInfo;->isHighPriority:Z

    .line 208
    return-object p0
.end method

.method public setSchedStatus(Lcom/miui/server/multisence/SingleWindowInfo$ScheduleStatus;)Lcom/miui/server/multisence/SingleWindowInfo;
    .locals 1
    .param p1, "status"    # Lcom/miui/server/multisence/SingleWindowInfo$ScheduleStatus;

    .line 242
    iget-object v0, p0, Lcom/miui/server/multisence/SingleWindowInfo;->mScheduleStatus:Ljava/util/Set;

    invoke-interface {v0, p1}, Ljava/util/Set;->contains(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 243
    return-object p0

    .line 245
    :cond_0
    iget-object v0, p0, Lcom/miui/server/multisence/SingleWindowInfo;->mScheduleStatus:Ljava/util/Set;

    invoke-interface {v0, p1}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    .line 246
    return-object p0
.end method

.method public setUid(I)Lcom/miui/server/multisence/SingleWindowInfo;
    .locals 0
    .param p1, "uid"    # I

    .line 54
    iput p1, p0, Lcom/miui/server/multisence/SingleWindowInfo;->myUid:I

    .line 55
    return-object p0
.end method

.method public setVisiable(Z)Lcom/miui/server/multisence/SingleWindowInfo;
    .locals 0
    .param p1, "visible"    # Z

    .line 132
    iput-boolean p1, p0, Lcom/miui/server/multisence/SingleWindowInfo;->isVisible:Z

    .line 133
    return-object p0
.end method

.method public setVrsCovered(Z)Lcom/miui/server/multisence/SingleWindowInfo;
    .locals 0
    .param p1, "covered"    # Z

    .line 101
    iput-boolean p1, p0, Lcom/miui/server/multisence/SingleWindowInfo;->isVrsCovered:Z

    .line 102
    return-object p0
.end method

.method public setWindowCount(I)Lcom/miui/server/multisence/SingleWindowInfo;
    .locals 0
    .param p1, "count"    # I

    .line 162
    iput p1, p0, Lcom/miui/server/multisence/SingleWindowInfo;->windowCount:I

    .line 163
    return-object p0
.end method

.method public setWindowingModeString(Ljava/lang/String;)Lcom/miui/server/multisence/SingleWindowInfo;
    .locals 0
    .param p1, "mode"    # Ljava/lang/String;

    .line 185
    iput-object p1, p0, Lcom/miui/server/multisence/SingleWindowInfo;->mWindowMode:Ljava/lang/String;

    .line 186
    return-object p0
.end method

.method public setgetWindowForm(Lcom/miui/server/multisence/SingleWindowInfo$WindowForm;)Lcom/miui/server/multisence/SingleWindowInfo;
    .locals 0
    .param p1, "form"    # Lcom/miui/server/multisence/SingleWindowInfo$WindowForm;

    .line 122
    iput-object p1, p0, Lcom/miui/server/multisence/SingleWindowInfo;->mForm:Lcom/miui/server/multisence/SingleWindowInfo$WindowForm;

    .line 123
    return-object p0
.end method

.method public toString()Ljava/lang/String;
    .locals 5

    .line 282
    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "AppInfo["

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    .line 283
    .local v0, "stringBuilder":Ljava/lang/StringBuilder;
    const-string v1, "name: "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lcom/miui/server/multisence/SingleWindowInfo;->mPackageName:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 284
    const-string v1, ", "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 285
    const-string/jumbo v2, "uid: "

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget v3, p0, Lcom/miui/server/multisence/SingleWindowInfo;->myUid:I

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    .line 286
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 287
    const-string v2, "pid: "

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget v3, p0, Lcom/miui/server/multisence/SingleWindowInfo;->myPid:I

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    .line 288
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 289
    const-string v2, "isHighPriority: "

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget-boolean v3, p0, Lcom/miui/server/multisence/SingleWindowInfo;->isHighPriority:Z

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    .line 290
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 291
    const-string/jumbo v2, "window state: "

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget-object v3, p0, Lcom/miui/server/multisence/SingleWindowInfo;->mForm:Lcom/miui/server/multisence/SingleWindowInfo$WindowForm;

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 292
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 293
    const-string v2, "covered: "

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget-boolean v3, p0, Lcom/miui/server/multisence/SingleWindowInfo;->isCovered:Z

    if-eqz v3, :cond_0

    const-string/jumbo v3, "y"

    goto :goto_0

    :cond_0
    const-string v3, "n"

    :goto_0
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 294
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 295
    const-string v1, "sched: "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 296
    iget-object v1, p0, Lcom/miui/server/multisence/SingleWindowInfo;->mScheduleStatus:Ljava/util/Set;

    invoke-interface {v1}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_1
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_1

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/miui/server/multisence/SingleWindowInfo$ScheduleStatus;

    .line 297
    .local v2, "sSched":Lcom/miui/server/multisence/SingleWindowInfo$ScheduleStatus;
    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    iget v4, v2, Lcom/miui/server/multisence/SingleWindowInfo$ScheduleStatus;->id:I

    invoke-static {v4}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, " "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 298
    .end local v2    # "sSched":Lcom/miui/server/multisence/SingleWindowInfo$ScheduleStatus;
    goto :goto_1

    .line 300
    :cond_1
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    return-object v1
.end method
