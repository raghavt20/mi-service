.class public final enum Lcom/miui/server/multisence/SingleWindowInfo$WindowForm;
.super Ljava/lang/Enum;
.source "SingleWindowInfo.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/miui/server/multisence/SingleWindowInfo;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x4019
    name = "WindowForm"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum<",
        "Lcom/miui/server/multisence/SingleWindowInfo$WindowForm;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[Lcom/miui/server/multisence/SingleWindowInfo$WindowForm;

.field public static final enum MUTIL_FREEDOM:Lcom/miui/server/multisence/SingleWindowInfo$WindowForm;

.field public static final enum MUTIL_FREEDOM_MINI:Lcom/miui/server/multisence/SingleWindowInfo$WindowForm;

.field public static final enum MUTIL_FREEDOM_PIN:Lcom/miui/server/multisence/SingleWindowInfo$WindowForm;

.field public static final enum MUTIL_FULLSCREEN_PARALLEL:Lcom/miui/server/multisence/SingleWindowInfo$WindowForm;

.field public static final enum MUTIL_FULLSCREEN_SINGLE:Lcom/miui/server/multisence/SingleWindowInfo$WindowForm;

.field public static final enum MUTIL_FULLSCREEN_SPLIT:Lcom/miui/server/multisence/SingleWindowInfo$WindowForm;

.field public static final enum MUTIL_VIDEO:Lcom/miui/server/multisence/SingleWindowInfo$WindowForm;

.field public static final enum UNKNOWN:Lcom/miui/server/multisence/SingleWindowInfo$WindowForm;


# direct methods
.method private static synthetic $values()[Lcom/miui/server/multisence/SingleWindowInfo$WindowForm;
    .locals 8

    .line 63
    sget-object v0, Lcom/miui/server/multisence/SingleWindowInfo$WindowForm;->UNKNOWN:Lcom/miui/server/multisence/SingleWindowInfo$WindowForm;

    sget-object v1, Lcom/miui/server/multisence/SingleWindowInfo$WindowForm;->MUTIL_FREEDOM:Lcom/miui/server/multisence/SingleWindowInfo$WindowForm;

    sget-object v2, Lcom/miui/server/multisence/SingleWindowInfo$WindowForm;->MUTIL_FREEDOM_MINI:Lcom/miui/server/multisence/SingleWindowInfo$WindowForm;

    sget-object v3, Lcom/miui/server/multisence/SingleWindowInfo$WindowForm;->MUTIL_FREEDOM_PIN:Lcom/miui/server/multisence/SingleWindowInfo$WindowForm;

    sget-object v4, Lcom/miui/server/multisence/SingleWindowInfo$WindowForm;->MUTIL_FULLSCREEN_SINGLE:Lcom/miui/server/multisence/SingleWindowInfo$WindowForm;

    sget-object v5, Lcom/miui/server/multisence/SingleWindowInfo$WindowForm;->MUTIL_FULLSCREEN_PARALLEL:Lcom/miui/server/multisence/SingleWindowInfo$WindowForm;

    sget-object v6, Lcom/miui/server/multisence/SingleWindowInfo$WindowForm;->MUTIL_FULLSCREEN_SPLIT:Lcom/miui/server/multisence/SingleWindowInfo$WindowForm;

    sget-object v7, Lcom/miui/server/multisence/SingleWindowInfo$WindowForm;->MUTIL_VIDEO:Lcom/miui/server/multisence/SingleWindowInfo$WindowForm;

    filled-new-array/range {v0 .. v7}, [Lcom/miui/server/multisence/SingleWindowInfo$WindowForm;

    move-result-object v0

    return-object v0
.end method

.method static constructor <clinit>()V
    .locals 3

    .line 64
    new-instance v0, Lcom/miui/server/multisence/SingleWindowInfo$WindowForm;

    const-string v1, "UNKNOWN"

    const/4 v2, 0x0

    invoke-direct {v0, v1, v2}, Lcom/miui/server/multisence/SingleWindowInfo$WindowForm;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/miui/server/multisence/SingleWindowInfo$WindowForm;->UNKNOWN:Lcom/miui/server/multisence/SingleWindowInfo$WindowForm;

    .line 65
    new-instance v0, Lcom/miui/server/multisence/SingleWindowInfo$WindowForm;

    const-string v1, "MUTIL_FREEDOM"

    const/4 v2, 0x1

    invoke-direct {v0, v1, v2}, Lcom/miui/server/multisence/SingleWindowInfo$WindowForm;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/miui/server/multisence/SingleWindowInfo$WindowForm;->MUTIL_FREEDOM:Lcom/miui/server/multisence/SingleWindowInfo$WindowForm;

    .line 66
    new-instance v0, Lcom/miui/server/multisence/SingleWindowInfo$WindowForm;

    const-string v1, "MUTIL_FREEDOM_MINI"

    const/4 v2, 0x2

    invoke-direct {v0, v1, v2}, Lcom/miui/server/multisence/SingleWindowInfo$WindowForm;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/miui/server/multisence/SingleWindowInfo$WindowForm;->MUTIL_FREEDOM_MINI:Lcom/miui/server/multisence/SingleWindowInfo$WindowForm;

    .line 67
    new-instance v0, Lcom/miui/server/multisence/SingleWindowInfo$WindowForm;

    const-string v1, "MUTIL_FREEDOM_PIN"

    const/4 v2, 0x3

    invoke-direct {v0, v1, v2}, Lcom/miui/server/multisence/SingleWindowInfo$WindowForm;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/miui/server/multisence/SingleWindowInfo$WindowForm;->MUTIL_FREEDOM_PIN:Lcom/miui/server/multisence/SingleWindowInfo$WindowForm;

    .line 68
    new-instance v0, Lcom/miui/server/multisence/SingleWindowInfo$WindowForm;

    const-string v1, "MUTIL_FULLSCREEN_SINGLE"

    const/4 v2, 0x4

    invoke-direct {v0, v1, v2}, Lcom/miui/server/multisence/SingleWindowInfo$WindowForm;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/miui/server/multisence/SingleWindowInfo$WindowForm;->MUTIL_FULLSCREEN_SINGLE:Lcom/miui/server/multisence/SingleWindowInfo$WindowForm;

    .line 69
    new-instance v0, Lcom/miui/server/multisence/SingleWindowInfo$WindowForm;

    const-string v1, "MUTIL_FULLSCREEN_PARALLEL"

    const/4 v2, 0x5

    invoke-direct {v0, v1, v2}, Lcom/miui/server/multisence/SingleWindowInfo$WindowForm;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/miui/server/multisence/SingleWindowInfo$WindowForm;->MUTIL_FULLSCREEN_PARALLEL:Lcom/miui/server/multisence/SingleWindowInfo$WindowForm;

    .line 70
    new-instance v0, Lcom/miui/server/multisence/SingleWindowInfo$WindowForm;

    const-string v1, "MUTIL_FULLSCREEN_SPLIT"

    const/4 v2, 0x6

    invoke-direct {v0, v1, v2}, Lcom/miui/server/multisence/SingleWindowInfo$WindowForm;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/miui/server/multisence/SingleWindowInfo$WindowForm;->MUTIL_FULLSCREEN_SPLIT:Lcom/miui/server/multisence/SingleWindowInfo$WindowForm;

    .line 71
    new-instance v0, Lcom/miui/server/multisence/SingleWindowInfo$WindowForm;

    const-string v1, "MUTIL_VIDEO"

    const/4 v2, 0x7

    invoke-direct {v0, v1, v2}, Lcom/miui/server/multisence/SingleWindowInfo$WindowForm;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/miui/server/multisence/SingleWindowInfo$WindowForm;->MUTIL_VIDEO:Lcom/miui/server/multisence/SingleWindowInfo$WindowForm;

    .line 63
    invoke-static {}, Lcom/miui/server/multisence/SingleWindowInfo$WindowForm;->$values()[Lcom/miui/server/multisence/SingleWindowInfo$WindowForm;

    move-result-object v0

    sput-object v0, Lcom/miui/server/multisence/SingleWindowInfo$WindowForm;->$VALUES:[Lcom/miui/server/multisence/SingleWindowInfo$WindowForm;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .line 63
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/miui/server/multisence/SingleWindowInfo$WindowForm;
    .locals 1
    .param p0, "name"    # Ljava/lang/String;

    .line 63
    const-class v0, Lcom/miui/server/multisence/SingleWindowInfo$WindowForm;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/miui/server/multisence/SingleWindowInfo$WindowForm;

    return-object v0
.end method

.method public static values()[Lcom/miui/server/multisence/SingleWindowInfo$WindowForm;
    .locals 1

    .line 63
    sget-object v0, Lcom/miui/server/multisence/SingleWindowInfo$WindowForm;->$VALUES:[Lcom/miui/server/multisence/SingleWindowInfo$WindowForm;

    invoke-virtual {v0}, [Lcom/miui/server/multisence/SingleWindowInfo$WindowForm;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/miui/server/multisence/SingleWindowInfo$WindowForm;

    return-object v0
.end method
