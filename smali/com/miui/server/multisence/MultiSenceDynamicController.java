public class com.miui.server.multisence.MultiSenceDynamicController {
	 /* .source "MultiSenceDynamicController.java" */
	 /* # static fields */
	 private static final java.lang.String CLOUD_MULTISENCE_ENABLE;
	 private static final java.lang.String CLOUD_MULTISENCE_SUBFUNC_NONSUPPORT;
	 public static final java.lang.String FW_WHITELIST;
	 public static Boolean IS_CLOUD_ENABLED;
	 private static final java.lang.String KEY_POWER_PERFORMANCE_MODE_OPEN;
	 private static final java.lang.String PROPERTY_CLOUD_MULTISENCE_ENABLE;
	 public static final java.lang.String VRS_WHITELIST;
	 /* # instance fields */
	 private java.lang.String TAG;
	 private android.content.Context mContext;
	 private android.os.Handler mHandler;
	 private com.miui.server.multisence.MultiSenceService mService;
	 /* # direct methods */
	 static java.lang.String -$$Nest$fgetTAG ( com.miui.server.multisence.MultiSenceDynamicController p0 ) { //bridge//synthethic
		 /* .locals 0 */
		 p0 = this.TAG;
	 } // .end method
	 static android.content.Context -$$Nest$fgetmContext ( com.miui.server.multisence.MultiSenceDynamicController p0 ) { //bridge//synthethic
		 /* .locals 0 */
		 p0 = this.mContext;
	 } // .end method
	 static com.miui.server.multisence.MultiSenceService -$$Nest$fgetmService ( com.miui.server.multisence.MultiSenceDynamicController p0 ) { //bridge//synthethic
		 /* .locals 0 */
		 p0 = this.mService;
	 } // .end method
	 static com.miui.server.multisence.MultiSenceDynamicController ( ) {
		 /* .locals 2 */
		 /* .line 34 */
		 final String v0 = "persist.multisence.cloud.enable"; // const-string v0, "persist.multisence.cloud.enable"
		 int v1 = 0; // const/4 v1, 0x0
		 v0 = 		 android.os.SystemProperties .getBoolean ( v0,v1 );
		 com.miui.server.multisence.MultiSenceDynamicController.IS_CLOUD_ENABLED = (v0!= 0);
		 return;
	 } // .end method
	 public com.miui.server.multisence.MultiSenceDynamicController ( ) {
		 /* .locals 1 */
		 /* .param p1, "service" # Lcom/miui/server/multisence/MultiSenceService; */
		 /* .param p2, "handler" # Landroid/os/Handler; */
		 /* .param p3, "context" # Landroid/content/Context; */
		 /* .line 52 */
		 /* invoke-direct {p0}, Ljava/lang/Object;-><init>()V */
		 /* .line 29 */
		 final String v0 = "MultiSenceDynamicController"; // const-string v0, "MultiSenceDynamicController"
		 this.TAG = v0;
		 /* .line 53 */
		 this.mService = p1;
		 /* .line 54 */
		 this.mHandler = p2;
		 /* .line 55 */
		 this.mContext = p3;
		 /* .line 56 */
		 return;
	 } // .end method
	 /* # virtual methods */
	 public void registerCloudObserver ( ) {
		 /* .locals 5 */
		 /* .line 59 */
		 /* new-instance v0, Lcom/miui/server/multisence/MultiSenceDynamicController$1; */
		 v1 = this.mHandler;
		 /* invoke-direct {v0, p0, v1}, Lcom/miui/server/multisence/MultiSenceDynamicController$1;-><init>(Lcom/miui/server/multisence/MultiSenceDynamicController;Landroid/os/Handler;)V */
		 /* .line 115 */
		 /* .local v0, "observer":Landroid/database/ContentObserver; */
		 v1 = this.mContext;
		 (( android.content.Context ) v1 ).getContentResolver ( ); // invoke-virtual {v1}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;
		 /* .line 116 */
		 final String v2 = "cloud_multisence_enable"; // const-string v2, "cloud_multisence_enable"
		 android.provider.Settings$System .getUriFor ( v2 );
		 /* .line 115 */
		 int v3 = 0; // const/4 v3, 0x0
		 int v4 = -2; // const/4 v4, -0x2
		 (( android.content.ContentResolver ) v1 ).registerContentObserver ( v2, v3, v0, v4 ); // invoke-virtual {v1, v2, v3, v0, v4}, Landroid/content/ContentResolver;->registerContentObserver(Landroid/net/Uri;ZLandroid/database/ContentObserver;I)V
		 /* .line 118 */
		 v1 = this.mContext;
		 (( android.content.Context ) v1 ).getContentResolver ( ); // invoke-virtual {v1}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;
		 /* .line 119 */
		 final String v2 = "POWER_PERFORMANCE_MODE_OPEN"; // const-string v2, "POWER_PERFORMANCE_MODE_OPEN"
		 android.provider.Settings$System .getUriFor ( v2 );
		 /* .line 118 */
		 (( android.content.ContentResolver ) v1 ).registerContentObserver ( v2, v3, v0, v4 ); // invoke-virtual {v1, v2, v3, v0, v4}, Landroid/content/ContentResolver;->registerContentObserver(Landroid/net/Uri;ZLandroid/database/ContentObserver;I)V
		 /* .line 121 */
		 v1 = this.mContext;
		 (( android.content.Context ) v1 ).getContentResolver ( ); // invoke-virtual {v1}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;
		 /* .line 122 */
		 final String v2 = "cloud_multisence_subfunc_nonsupport"; // const-string v2, "cloud_multisence_subfunc_nonsupport"
		 android.provider.Settings$System .getUriFor ( v2 );
		 /* .line 121 */
		 (( android.content.ContentResolver ) v1 ).registerContentObserver ( v2, v3, v0, v4 ); // invoke-virtual {v1, v2, v3, v0, v4}, Landroid/content/ContentResolver;->registerContentObserver(Landroid/net/Uri;ZLandroid/database/ContentObserver;I)V
		 /* .line 125 */
		 /* sget-boolean v1, Lcom/miui/server/multisence/MultiSenceConfig;->DEBUG_DYNAMIC_CONTROLLER:Z */
		 /* new-instance v2, Ljava/lang/StringBuilder; */
		 /* invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V */
		 final String v3 = "isCloudEnable: "; // const-string v3, "isCloudEnable: "
		 (( java.lang.StringBuilder ) v2 ).append ( v3 ); // invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
		 /* sget-boolean v3, Lcom/miui/server/multisence/MultiSenceDynamicController;->IS_CLOUD_ENABLED:Z */
		 (( java.lang.StringBuilder ) v2 ).append ( v3 ); // invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;
		 (( java.lang.StringBuilder ) v2 ).toString ( ); // invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
		 com.miui.server.multisence.MultiSenceServiceUtils .msLogD ( v1,v2 );
		 /* .line 126 */
		 return;
	 } // .end method
	 public void registerFWCloudObserver ( ) {
		 /* .locals 5 */
		 /* .line 147 */
		 /* new-instance v0, Lcom/miui/server/multisence/MultiSenceDynamicController$3; */
		 v1 = this.mHandler;
		 /* invoke-direct {v0, p0, v1}, Lcom/miui/server/multisence/MultiSenceDynamicController$3;-><init>(Lcom/miui/server/multisence/MultiSenceDynamicController;Landroid/os/Handler;)V */
		 /* .line 156 */
		 /* .local v0, "observer":Landroid/database/ContentObserver; */
		 v1 = this.mContext;
		 (( android.content.Context ) v1 ).getContentResolver ( ); // invoke-virtual {v1}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;
		 /* .line 157 */
		 /* const-string/jumbo v2, "support_fw_app" */
		 android.provider.Settings$System .getUriFor ( v2 );
		 /* .line 156 */
		 int v3 = 0; // const/4 v3, 0x0
		 int v4 = -2; // const/4 v4, -0x2
		 (( android.content.ContentResolver ) v1 ).registerContentObserver ( v2, v3, v0, v4 ); // invoke-virtual {v1, v2, v3, v0, v4}, Landroid/content/ContentResolver;->registerContentObserver(Landroid/net/Uri;ZLandroid/database/ContentObserver;I)V
		 /* .line 159 */
		 return;
	 } // .end method
	 public void registerVRSCloudObserver ( ) {
		 /* .locals 5 */
		 /* .line 129 */
		 /* new-instance v0, Lcom/miui/server/multisence/MultiSenceDynamicController$2; */
		 v1 = this.mHandler;
		 /* invoke-direct {v0, p0, v1}, Lcom/miui/server/multisence/MultiSenceDynamicController$2;-><init>(Lcom/miui/server/multisence/MultiSenceDynamicController;Landroid/os/Handler;)V */
		 /* .line 141 */
		 /* .local v0, "observer":Landroid/database/ContentObserver; */
		 v1 = this.mContext;
		 (( android.content.Context ) v1 ).getContentResolver ( ); // invoke-virtual {v1}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;
		 /* .line 142 */
		 /* const-string/jumbo v2, "support_common_vrs_app" */
		 android.provider.Settings$System .getUriFor ( v2 );
		 /* .line 141 */
		 int v3 = 0; // const/4 v3, 0x0
		 int v4 = -2; // const/4 v4, -0x2
		 (( android.content.ContentResolver ) v1 ).registerContentObserver ( v2, v3, v0, v4 ); // invoke-virtual {v1, v2, v3, v0, v4}, Landroid/content/ContentResolver;->registerContentObserver(Landroid/net/Uri;ZLandroid/database/ContentObserver;I)V
		 /* .line 144 */
		 return;
	 } // .end method
