.class public interface abstract Lcom/miui/server/multisence/MultiSenceManagerInternal;
.super Ljava/lang/Object;
.source "MultiSenceManagerInternal.java"


# virtual methods
.method public abstract setUpdateReason(Ljava/lang/String;)V
.end method

.method public abstract showAllWindowInfo(Ljava/lang/String;)V
.end method

.method public abstract updateDynamicWindowsInfo(II[IZ)V
.end method

.method public abstract updateStaticWindowsInfo(Ljava/util/Map;)V
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/Map<",
            "Ljava/lang/String;",
            "Lcom/miui/server/multisence/SingleWindowInfo;",
            ">;)V"
        }
    .end annotation
.end method
