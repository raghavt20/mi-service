class com.miui.server.multisence.MultiSenceService$H extends android.os.Handler {
	 /* .source "MultiSenceService.java" */
	 /* # annotations */
	 /* .annotation system Ldalvik/annotation/EnclosingClass; */
	 /* value = Lcom/miui/server/multisence/MultiSenceService; */
} // .end annotation
/* .annotation system Ldalvik/annotation/InnerClass; */
/* accessFlags = 0x2 */
/* name = "H" */
} // .end annotation
/* # instance fields */
final com.miui.server.multisence.MultiSenceService this$0; //synthetic
/* # direct methods */
 com.miui.server.multisence.MultiSenceService$H ( ) {
/* .locals 0 */
/* .param p2, "looper" # Landroid/os/Looper; */
/* .line 222 */
this.this$0 = p1;
/* .line 223 */
/* invoke-direct {p0, p2}, Landroid/os/Handler;-><init>(Landroid/os/Looper;)V */
/* .line 224 */
return;
} // .end method
/* # virtual methods */
public void handleMessage ( android.os.Message p0 ) {
/* .locals 6 */
/* .param p1, "msg" # Landroid/os/Message; */
/* .line 228 */
/* iget v0, p1, Landroid/os/Message;->arg1:I */
/* .line 229 */
/* .local v0, "uid":I */
/* iget v1, p1, Landroid/os/Message;->arg2:I */
/* if-nez v1, :cond_0 */
int v1 = 0; // const/4 v1, 0x0
} // :cond_0
int v1 = 1; // const/4 v1, 0x1
/* .line 230 */
/* .local v1, "isStarted":Z */
} // :goto_0
v2 = this.obj;
/* check-cast v2, [I */
/* .line 231 */
/* .local v2, "tids":[I */
/* iget v3, p1, Landroid/os/Message;->what:I */
/* sparse-switch v3, :sswitch_data_0 */
/* .line 256 */
v3 = this.this$0;
com.miui.server.multisence.MultiSenceService .-$$Nest$fgetmPolicy ( v3 );
int v4 = 0; // const/4 v4, 0x0
(( com.miui.server.multisence.MultiSenceServicePolicy ) v3 ).dynamicSenceSchedDefault ( v0, v2, v1, v4 ); // invoke-virtual {v3, v0, v2, v1, v4}, Lcom/miui/server/multisence/MultiSenceServicePolicy;->dynamicSenceSchedDefault(I[IZLjava/lang/String;)V
/* goto/16 :goto_1 */
/* .line 253 */
/* :sswitch_0 */
v3 = this.this$0;
com.miui.server.multisence.MultiSenceService .-$$Nest$fgetmPolicy ( v3 );
(( com.miui.server.multisence.MultiSenceServicePolicy ) v3 ).dynamicSenceSchedFloatingWindowMove ( v0, v2, v1 ); // invoke-virtual {v3, v0, v2, v1}, Lcom/miui/server/multisence/MultiSenceServicePolicy;->dynamicSenceSchedFloatingWindowMove(I[IZ)V
/* .line 254 */
/* goto/16 :goto_1 */
/* .line 248 */
/* :sswitch_1 */
v3 = this.this$0;
com.miui.server.multisence.MultiSenceService .-$$Nest$fgetmPolicy ( v3 );
v4 = this.this$0;
com.miui.server.multisence.MultiSenceService .-$$Nest$fgetmultiSencePackages ( v4 );
/* const/16 v5, 0x4001 */
java.lang.Integer .valueOf ( v5 );
/* check-cast v4, Ljava/lang/String; */
(( com.miui.server.multisence.MultiSenceServicePolicy ) v3 ).dynamicSenceSchedMWSActionMove ( v0, v2, v1, v4 ); // invoke-virtual {v3, v0, v2, v1, v4}, Lcom/miui/server/multisence/MultiSenceServicePolicy;->dynamicSenceSchedMWSActionMove(I[IZLjava/lang/String;)V
/* .line 249 */
/* goto/16 :goto_1 */
/* .line 251 */
/* :sswitch_2 */
/* goto/16 :goto_1 */
/* .line 245 */
/* :sswitch_3 */
v3 = this.this$0;
com.miui.server.multisence.MultiSenceService .-$$Nest$fgetmPolicy ( v3 );
v4 = this.this$0;
com.miui.server.multisence.MultiSenceService .-$$Nest$fgetmultiSencePackages ( v4 );
/* const/16 v5, 0x2001 */
java.lang.Integer .valueOf ( v5 );
/* check-cast v4, Ljava/lang/String; */
(( com.miui.server.multisence.MultiSenceServicePolicy ) v3 ).dynamicSenceSchedSplitscreenctionDivider ( v0, v2, v1, v4 ); // invoke-virtual {v3, v0, v2, v1, v4}, Lcom/miui/server/multisence/MultiSenceServicePolicy;->dynamicSenceSchedSplitscreenctionDivider(I[IZLjava/lang/String;)V
/* .line 246 */
/* .line 242 */
/* :sswitch_4 */
v3 = this.this$0;
com.miui.server.multisence.MultiSenceService .-$$Nest$fgetmPolicy ( v3 );
v4 = this.this$0;
com.miui.server.multisence.MultiSenceService .-$$Nest$fgetmultiSencePackages ( v4 );
/* const/16 v5, 0x1006 */
java.lang.Integer .valueOf ( v5 );
/* check-cast v4, Ljava/lang/String; */
(( com.miui.server.multisence.MultiSenceServicePolicy ) v3 ).dynamicSenceSchedFreeformActionElude ( v0, v2, v1, v4 ); // invoke-virtual {v3, v0, v2, v1, v4}, Lcom/miui/server/multisence/MultiSenceServicePolicy;->dynamicSenceSchedFreeformActionElude(I[IZLjava/lang/String;)V
/* .line 243 */
/* .line 239 */
/* :sswitch_5 */
v3 = this.this$0;
com.miui.server.multisence.MultiSenceService .-$$Nest$fgetmPolicy ( v3 );
v4 = this.this$0;
com.miui.server.multisence.MultiSenceService .-$$Nest$fgetmultiSencePackages ( v4 );
/* const/16 v5, 0x1005 */
java.lang.Integer .valueOf ( v5 );
/* check-cast v4, Ljava/lang/String; */
(( com.miui.server.multisence.MultiSenceServicePolicy ) v3 ).dynamicSenceSchedFreeformActionEnterFullScreen ( v0, v2, v1, v4 ); // invoke-virtual {v3, v0, v2, v1, v4}, Lcom/miui/server/multisence/MultiSenceServicePolicy;->dynamicSenceSchedFreeformActionEnterFullScreen(I[IZLjava/lang/String;)V
/* .line 240 */
/* .line 236 */
/* :sswitch_6 */
v3 = this.this$0;
com.miui.server.multisence.MultiSenceService .-$$Nest$fgetmPolicy ( v3 );
v4 = this.this$0;
com.miui.server.multisence.MultiSenceService .-$$Nest$fgetmultiSencePackages ( v4 );
/* const/16 v5, 0x1004 */
java.lang.Integer .valueOf ( v5 );
/* check-cast v4, Ljava/lang/String; */
(( com.miui.server.multisence.MultiSenceServicePolicy ) v3 ).dynamicSenceSchedFreeformActionResize ( v0, v2, v1, v4 ); // invoke-virtual {v3, v0, v2, v1, v4}, Lcom/miui/server/multisence/MultiSenceServicePolicy;->dynamicSenceSchedFreeformActionResize(I[IZLjava/lang/String;)V
/* .line 237 */
/* .line 233 */
/* :sswitch_7 */
v3 = this.this$0;
com.miui.server.multisence.MultiSenceService .-$$Nest$fgetmPolicy ( v3 );
v4 = this.this$0;
com.miui.server.multisence.MultiSenceService .-$$Nest$fgetmultiSencePackages ( v4 );
/* const/16 v5, 0x1001 */
java.lang.Integer .valueOf ( v5 );
/* check-cast v4, Ljava/lang/String; */
(( com.miui.server.multisence.MultiSenceServicePolicy ) v3 ).dynamicSenceSchedFreeformActionMove ( v0, v2, v1, v4 ); // invoke-virtual {v3, v0, v2, v1, v4}, Lcom/miui/server/multisence/MultiSenceServicePolicy;->dynamicSenceSchedFreeformActionMove(I[IZLjava/lang/String;)V
/* .line 234 */
/* nop */
/* .line 259 */
} // :goto_1
return;
/* nop */
/* :sswitch_data_0 */
/* .sparse-switch */
/* 0x1001 -> :sswitch_7 */
/* 0x1004 -> :sswitch_6 */
/* 0x1005 -> :sswitch_5 */
/* 0x1006 -> :sswitch_4 */
/* 0x2001 -> :sswitch_3 */
/* 0x4000 -> :sswitch_2 */
/* 0x4001 -> :sswitch_1 */
/* 0x5001 -> :sswitch_0 */
} // .end sparse-switch
} // .end method
