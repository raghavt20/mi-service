.class public final enum Lcom/miui/server/multisence/SingleWindowInfo$ScheduleStatus;
.super Ljava/lang/Enum;
.source "SingleWindowInfo.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/miui/server/multisence/SingleWindowInfo;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x4019
    name = "ScheduleStatus"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum<",
        "Lcom/miui/server/multisence/SingleWindowInfo$ScheduleStatus;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[Lcom/miui/server/multisence/SingleWindowInfo$ScheduleStatus;

.field public static final enum DYN_FPS_SET:Lcom/miui/server/multisence/SingleWindowInfo$ScheduleStatus;

.field public static final enum UNKNOWN:Lcom/miui/server/multisence/SingleWindowInfo$ScheduleStatus;

.field public static final enum VRS_RESET:Lcom/miui/server/multisence/SingleWindowInfo$ScheduleStatus;

.field public static final enum VRS_SET:Lcom/miui/server/multisence/SingleWindowInfo$ScheduleStatus;


# instance fields
.field final id:I


# direct methods
.method private static synthetic $values()[Lcom/miui/server/multisence/SingleWindowInfo$ScheduleStatus;
    .locals 4

    .line 228
    sget-object v0, Lcom/miui/server/multisence/SingleWindowInfo$ScheduleStatus;->UNKNOWN:Lcom/miui/server/multisence/SingleWindowInfo$ScheduleStatus;

    sget-object v1, Lcom/miui/server/multisence/SingleWindowInfo$ScheduleStatus;->DYN_FPS_SET:Lcom/miui/server/multisence/SingleWindowInfo$ScheduleStatus;

    sget-object v2, Lcom/miui/server/multisence/SingleWindowInfo$ScheduleStatus;->VRS_RESET:Lcom/miui/server/multisence/SingleWindowInfo$ScheduleStatus;

    sget-object v3, Lcom/miui/server/multisence/SingleWindowInfo$ScheduleStatus;->VRS_SET:Lcom/miui/server/multisence/SingleWindowInfo$ScheduleStatus;

    filled-new-array {v0, v1, v2, v3}, [Lcom/miui/server/multisence/SingleWindowInfo$ScheduleStatus;

    move-result-object v0

    return-object v0
.end method

.method static constructor <clinit>()V
    .locals 3

    .line 229
    new-instance v0, Lcom/miui/server/multisence/SingleWindowInfo$ScheduleStatus;

    const-string v1, "UNKNOWN"

    const/4 v2, 0x0

    invoke-direct {v0, v1, v2, v2}, Lcom/miui/server/multisence/SingleWindowInfo$ScheduleStatus;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/miui/server/multisence/SingleWindowInfo$ScheduleStatus;->UNKNOWN:Lcom/miui/server/multisence/SingleWindowInfo$ScheduleStatus;

    .line 230
    new-instance v0, Lcom/miui/server/multisence/SingleWindowInfo$ScheduleStatus;

    const-string v1, "DYN_FPS_SET"

    const/4 v2, 0x1

    invoke-direct {v0, v1, v2, v2}, Lcom/miui/server/multisence/SingleWindowInfo$ScheduleStatus;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/miui/server/multisence/SingleWindowInfo$ScheduleStatus;->DYN_FPS_SET:Lcom/miui/server/multisence/SingleWindowInfo$ScheduleStatus;

    .line 231
    new-instance v0, Lcom/miui/server/multisence/SingleWindowInfo$ScheduleStatus;

    const-string v1, "VRS_RESET"

    const/4 v2, 0x2

    invoke-direct {v0, v1, v2, v2}, Lcom/miui/server/multisence/SingleWindowInfo$ScheduleStatus;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/miui/server/multisence/SingleWindowInfo$ScheduleStatus;->VRS_RESET:Lcom/miui/server/multisence/SingleWindowInfo$ScheduleStatus;

    .line 232
    new-instance v0, Lcom/miui/server/multisence/SingleWindowInfo$ScheduleStatus;

    const-string v1, "VRS_SET"

    const/4 v2, 0x3

    invoke-direct {v0, v1, v2, v2}, Lcom/miui/server/multisence/SingleWindowInfo$ScheduleStatus;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/miui/server/multisence/SingleWindowInfo$ScheduleStatus;->VRS_SET:Lcom/miui/server/multisence/SingleWindowInfo$ScheduleStatus;

    .line 228
    invoke-static {}, Lcom/miui/server/multisence/SingleWindowInfo$ScheduleStatus;->$values()[Lcom/miui/server/multisence/SingleWindowInfo$ScheduleStatus;

    move-result-object v0

    sput-object v0, Lcom/miui/server/multisence/SingleWindowInfo$ScheduleStatus;->$VALUES:[Lcom/miui/server/multisence/SingleWindowInfo$ScheduleStatus;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;II)V
    .locals 0
    .param p3, "id"    # I
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(I)V"
        }
    .end annotation

    .line 234
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    .line 235
    iput p3, p0, Lcom/miui/server/multisence/SingleWindowInfo$ScheduleStatus;->id:I

    .line 236
    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/miui/server/multisence/SingleWindowInfo$ScheduleStatus;
    .locals 1
    .param p0, "name"    # Ljava/lang/String;

    .line 228
    const-class v0, Lcom/miui/server/multisence/SingleWindowInfo$ScheduleStatus;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/miui/server/multisence/SingleWindowInfo$ScheduleStatus;

    return-object v0
.end method

.method public static values()[Lcom/miui/server/multisence/SingleWindowInfo$ScheduleStatus;
    .locals 1

    .line 228
    sget-object v0, Lcom/miui/server/multisence/SingleWindowInfo$ScheduleStatus;->$VALUES:[Lcom/miui/server/multisence/SingleWindowInfo$ScheduleStatus;

    invoke-virtual {v0}, [Lcom/miui/server/multisence/SingleWindowInfo$ScheduleStatus;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/miui/server/multisence/SingleWindowInfo$ScheduleStatus;

    return-object v0
.end method
