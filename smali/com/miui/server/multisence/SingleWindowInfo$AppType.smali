.class public final enum Lcom/miui/server/multisence/SingleWindowInfo$AppType;
.super Ljava/lang/Enum;
.source "SingleWindowInfo.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/miui/server/multisence/SingleWindowInfo;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x4019
    name = "AppType"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum<",
        "Lcom/miui/server/multisence/SingleWindowInfo$AppType;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[Lcom/miui/server/multisence/SingleWindowInfo$AppType;

.field public static final enum COMMON:Lcom/miui/server/multisence/SingleWindowInfo$AppType;

.field public static final enum GAME:Lcom/miui/server/multisence/SingleWindowInfo$AppType;

.field public static final enum HOME:Lcom/miui/server/multisence/SingleWindowInfo$AppType;

.field public static final enum UNKNOWN:Lcom/miui/server/multisence/SingleWindowInfo$AppType;

.field public static final enum VIDEO:Lcom/miui/server/multisence/SingleWindowInfo$AppType;


# direct methods
.method private static synthetic $values()[Lcom/miui/server/multisence/SingleWindowInfo$AppType;
    .locals 5

    .line 74
    sget-object v0, Lcom/miui/server/multisence/SingleWindowInfo$AppType;->UNKNOWN:Lcom/miui/server/multisence/SingleWindowInfo$AppType;

    sget-object v1, Lcom/miui/server/multisence/SingleWindowInfo$AppType;->COMMON:Lcom/miui/server/multisence/SingleWindowInfo$AppType;

    sget-object v2, Lcom/miui/server/multisence/SingleWindowInfo$AppType;->GAME:Lcom/miui/server/multisence/SingleWindowInfo$AppType;

    sget-object v3, Lcom/miui/server/multisence/SingleWindowInfo$AppType;->VIDEO:Lcom/miui/server/multisence/SingleWindowInfo$AppType;

    sget-object v4, Lcom/miui/server/multisence/SingleWindowInfo$AppType;->HOME:Lcom/miui/server/multisence/SingleWindowInfo$AppType;

    filled-new-array {v0, v1, v2, v3, v4}, [Lcom/miui/server/multisence/SingleWindowInfo$AppType;

    move-result-object v0

    return-object v0
.end method

.method static constructor <clinit>()V
    .locals 3

    .line 75
    new-instance v0, Lcom/miui/server/multisence/SingleWindowInfo$AppType;

    const-string v1, "UNKNOWN"

    const/4 v2, 0x0

    invoke-direct {v0, v1, v2}, Lcom/miui/server/multisence/SingleWindowInfo$AppType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/miui/server/multisence/SingleWindowInfo$AppType;->UNKNOWN:Lcom/miui/server/multisence/SingleWindowInfo$AppType;

    .line 76
    new-instance v0, Lcom/miui/server/multisence/SingleWindowInfo$AppType;

    const-string v1, "COMMON"

    const/4 v2, 0x1

    invoke-direct {v0, v1, v2}, Lcom/miui/server/multisence/SingleWindowInfo$AppType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/miui/server/multisence/SingleWindowInfo$AppType;->COMMON:Lcom/miui/server/multisence/SingleWindowInfo$AppType;

    .line 77
    new-instance v0, Lcom/miui/server/multisence/SingleWindowInfo$AppType;

    const-string v1, "GAME"

    const/4 v2, 0x2

    invoke-direct {v0, v1, v2}, Lcom/miui/server/multisence/SingleWindowInfo$AppType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/miui/server/multisence/SingleWindowInfo$AppType;->GAME:Lcom/miui/server/multisence/SingleWindowInfo$AppType;

    .line 78
    new-instance v0, Lcom/miui/server/multisence/SingleWindowInfo$AppType;

    const-string v1, "VIDEO"

    const/4 v2, 0x3

    invoke-direct {v0, v1, v2}, Lcom/miui/server/multisence/SingleWindowInfo$AppType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/miui/server/multisence/SingleWindowInfo$AppType;->VIDEO:Lcom/miui/server/multisence/SingleWindowInfo$AppType;

    .line 79
    new-instance v0, Lcom/miui/server/multisence/SingleWindowInfo$AppType;

    const-string v1, "HOME"

    const/4 v2, 0x4

    invoke-direct {v0, v1, v2}, Lcom/miui/server/multisence/SingleWindowInfo$AppType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/miui/server/multisence/SingleWindowInfo$AppType;->HOME:Lcom/miui/server/multisence/SingleWindowInfo$AppType;

    .line 74
    invoke-static {}, Lcom/miui/server/multisence/SingleWindowInfo$AppType;->$values()[Lcom/miui/server/multisence/SingleWindowInfo$AppType;

    move-result-object v0

    sput-object v0, Lcom/miui/server/multisence/SingleWindowInfo$AppType;->$VALUES:[Lcom/miui/server/multisence/SingleWindowInfo$AppType;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .line 74
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/miui/server/multisence/SingleWindowInfo$AppType;
    .locals 1
    .param p0, "name"    # Ljava/lang/String;

    .line 74
    const-class v0, Lcom/miui/server/multisence/SingleWindowInfo$AppType;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/miui/server/multisence/SingleWindowInfo$AppType;

    return-object v0
.end method

.method public static values()[Lcom/miui/server/multisence/SingleWindowInfo$AppType;
    .locals 1

    .line 74
    sget-object v0, Lcom/miui/server/multisence/SingleWindowInfo$AppType;->$VALUES:[Lcom/miui/server/multisence/SingleWindowInfo$AppType;

    invoke-virtual {v0}, [Lcom/miui/server/multisence/SingleWindowInfo$AppType;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/miui/server/multisence/SingleWindowInfo$AppType;

    return-object v0
.end method
