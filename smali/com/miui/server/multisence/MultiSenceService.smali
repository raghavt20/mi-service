.class public Lcom/miui/server/multisence/MultiSenceService;
.super Lcom/android/server/SystemService;
.source "MultiSenceService.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/miui/server/multisence/MultiSenceService$BinderService;,
        Lcom/miui/server/multisence/MultiSenceService$MultiSenceThread;,
        Lcom/miui/server/multisence/MultiSenceService$H;,
        Lcom/miui/server/multisence/MultiSenceService$MultiSenceShellCmd;
    }
.end annotation


# static fields
.field private static final DEBUG:Z

.field private static final DEFAULT_MULTISENCE_FREEFORM_PACKAGE:Ljava/lang/String; = "com.android.systemui"

.field private static final DELAY_MS:I = 0x32

.field public static IS_SERVICE_ENABLED:Z = false

.field private static final MCD_DF_PATH:Ljava/lang/String; = "/data/system/mcd/mwdf"

.field public static final SERVICE_NAME:Ljava/lang/String; = "miui_multi_sence"

.field private static final TAG:Ljava/lang/String; = "MultiSenceService"

.field private static mMultiTaskActionListenerRegistered:Z


# instance fields
.field private mBinderService:Lcom/miui/server/multisence/MultiSenceService$BinderService;

.field private mContext:Landroid/content/Context;

.field private mController:Lcom/miui/server/multisence/MultiSenceDynamicController;

.field public mHandler:Landroid/os/Handler;

.field mMultiSenceListener:Lcom/android/server/wm/MultiSenceListener;

.field private mNewWindows:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map<",
            "Ljava/lang/String;",
            "Lcom/miui/server/multisence/SingleWindowInfo;",
            ">;"
        }
    .end annotation
.end field

.field private mPolicy:Lcom/miui/server/multisence/MultiSenceServicePolicy;

.field private mReason:Ljava/lang/String;

.field private final mSmartPowerService:Lcom/miui/app/smartpower/SmartPowerServiceInternal;

.field private mThread:Lcom/android/server/ServiceThread;

.field private final mUpdateStaticWindows:Ljava/lang/Runnable;

.field private mWindows:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map<",
            "Ljava/lang/String;",
            "Lcom/miui/server/multisence/SingleWindowInfo;",
            ">;"
        }
    .end annotation
.end field

.field private multiSencePackages:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map<",
            "Ljava/lang/Integer;",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private multiTaskActionManager:Lmiui/smartpower/MultiTaskActionManager;


# direct methods
.method static bridge synthetic -$$Nest$fgetmContext(Lcom/miui/server/multisence/MultiSenceService;)Landroid/content/Context;
    .locals 0

    iget-object p0, p0, Lcom/miui/server/multisence/MultiSenceService;->mContext:Landroid/content/Context;

    return-object p0
.end method

.method static bridge synthetic -$$Nest$fgetmPolicy(Lcom/miui/server/multisence/MultiSenceService;)Lcom/miui/server/multisence/MultiSenceServicePolicy;
    .locals 0

    iget-object p0, p0, Lcom/miui/server/multisence/MultiSenceService;->mPolicy:Lcom/miui/server/multisence/MultiSenceServicePolicy;

    return-object p0
.end method

.method static bridge synthetic -$$Nest$fgetmultiSencePackages(Lcom/miui/server/multisence/MultiSenceService;)Ljava/util/Map;
    .locals 0

    iget-object p0, p0, Lcom/miui/server/multisence/MultiSenceService;->multiSencePackages:Ljava/util/Map;

    return-object p0
.end method

.method static constructor <clinit>()V
    .locals 2

    .line 62
    const-string v0, "persist.multisence.enable"

    const/4 v1, 0x1

    invoke-static {v0, v1}, Landroid/os/SystemProperties;->getBoolean(Ljava/lang/String;Z)Z

    move-result v0

    sput-boolean v0, Lcom/miui/server/multisence/MultiSenceService;->IS_SERVICE_ENABLED:Z

    .line 63
    const-string v0, "persist.multisence.debug.on"

    const/4 v1, 0x0

    invoke-static {v0, v1}, Landroid/os/SystemProperties;->getBoolean(Ljava/lang/String;Z)Z

    move-result v0

    sput-boolean v0, Lcom/miui/server/multisence/MultiSenceService;->DEBUG:Z

    .line 85
    sput-boolean v1, Lcom/miui/server/multisence/MultiSenceService;->mMultiTaskActionListenerRegistered:Z

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;)V
    .locals 3
    .param p1, "context"    # Landroid/content/Context;

    .line 90
    invoke-direct {p0, p1}, Lcom/android/server/SystemService;-><init>(Landroid/content/Context;)V

    .line 66
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    iput-object v0, p0, Lcom/miui/server/multisence/MultiSenceService;->mWindows:Ljava/util/Map;

    .line 67
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/miui/server/multisence/MultiSenceService;->mNewWindows:Ljava/util/Map;

    .line 69
    new-instance v1, Ljava/util/HashMap;

    invoke-direct {v1}, Ljava/util/HashMap;-><init>()V

    iput-object v1, p0, Lcom/miui/server/multisence/MultiSenceService;->multiSencePackages:Ljava/util/Map;

    .line 71
    iput-object v0, p0, Lcom/miui/server/multisence/MultiSenceService;->mPolicy:Lcom/miui/server/multisence/MultiSenceServicePolicy;

    .line 72
    iput-object v0, p0, Lcom/miui/server/multisence/MultiSenceService;->mBinderService:Lcom/miui/server/multisence/MultiSenceService$BinderService;

    .line 73
    const-class v0, Lcom/miui/app/smartpower/SmartPowerServiceInternal;

    invoke-static {v0}, Lcom/android/server/LocalServices;->getService(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/miui/app/smartpower/SmartPowerServiceInternal;

    iput-object v0, p0, Lcom/miui/server/multisence/MultiSenceService;->mSmartPowerService:Lcom/miui/app/smartpower/SmartPowerServiceInternal;

    .line 74
    new-instance v0, Lmiui/smartpower/MultiTaskActionManager;

    invoke-direct {v0}, Lmiui/smartpower/MultiTaskActionManager;-><init>()V

    iput-object v0, p0, Lcom/miui/server/multisence/MultiSenceService;->multiTaskActionManager:Lmiui/smartpower/MultiTaskActionManager;

    .line 81
    const-string/jumbo v0, "unknown"

    iput-object v0, p0, Lcom/miui/server/multisence/MultiSenceService;->mReason:Ljava/lang/String;

    .line 359
    new-instance v0, Lcom/miui/server/multisence/MultiSenceService$1;

    invoke-direct {v0, p0}, Lcom/miui/server/multisence/MultiSenceService$1;-><init>(Lcom/miui/server/multisence/MultiSenceService;)V

    iput-object v0, p0, Lcom/miui/server/multisence/MultiSenceService;->mUpdateStaticWindows:Ljava/lang/Runnable;

    .line 91
    iput-object p1, p0, Lcom/miui/server/multisence/MultiSenceService;->mContext:Landroid/content/Context;

    .line 92
    new-instance v0, Lcom/miui/server/multisence/MultiSenceServicePolicy;

    invoke-direct {v0}, Lcom/miui/server/multisence/MultiSenceServicePolicy;-><init>()V

    iput-object v0, p0, Lcom/miui/server/multisence/MultiSenceService;->mPolicy:Lcom/miui/server/multisence/MultiSenceServicePolicy;

    .line 93
    invoke-static {}, Lcom/miui/server/multisence/MultiSenceService$MultiSenceThread;->getInstance()Lcom/miui/server/multisence/MultiSenceService$MultiSenceThread;

    move-result-object v0

    iput-object v0, p0, Lcom/miui/server/multisence/MultiSenceService;->mThread:Lcom/android/server/ServiceThread;

    .line 94
    new-instance v0, Lcom/miui/server/multisence/MultiSenceService$H;

    iget-object v1, p0, Lcom/miui/server/multisence/MultiSenceService;->mThread:Lcom/android/server/ServiceThread;

    invoke-virtual {v1}, Lcom/android/server/ServiceThread;->getLooper()Landroid/os/Looper;

    move-result-object v1

    invoke-direct {v0, p0, v1}, Lcom/miui/server/multisence/MultiSenceService$H;-><init>(Lcom/miui/server/multisence/MultiSenceService;Landroid/os/Looper;)V

    iput-object v0, p0, Lcom/miui/server/multisence/MultiSenceService;->mHandler:Landroid/os/Handler;

    .line 95
    new-instance v0, Lcom/android/server/wm/MultiSenceListener;

    iget-object v1, p0, Lcom/miui/server/multisence/MultiSenceService;->mThread:Lcom/android/server/ServiceThread;

    invoke-virtual {v1}, Lcom/android/server/ServiceThread;->getLooper()Landroid/os/Looper;

    move-result-object v1

    invoke-direct {v0, v1}, Lcom/android/server/wm/MultiSenceListener;-><init>(Landroid/os/Looper;)V

    iput-object v0, p0, Lcom/miui/server/multisence/MultiSenceService;->mMultiSenceListener:Lcom/android/server/wm/MultiSenceListener;

    .line 97
    invoke-static {}, Lcom/miui/server/multisence/MultiSenceConfig;->getInstance()Lcom/miui/server/multisence/MultiSenceConfig;

    move-result-object v0

    iget-object v1, p0, Lcom/miui/server/multisence/MultiSenceService;->mContext:Landroid/content/Context;

    invoke-virtual {v0, p0, v1}, Lcom/miui/server/multisence/MultiSenceConfig;->init(Lcom/miui/server/multisence/MultiSenceService;Landroid/content/Context;)V

    .line 100
    const-string v0, "/data/system/mcd/mwdf"

    invoke-direct {p0, v0}, Lcom/miui/server/multisence/MultiSenceService;->createFileDevice(Ljava/lang/String;)V

    .line 102
    invoke-virtual {p0}, Lcom/miui/server/multisence/MultiSenceService;->registerMultiTaskActionListener()V

    .line 105
    new-instance v0, Lcom/miui/server/multisence/MultiSenceDynamicController;

    iget-object v1, p0, Lcom/miui/server/multisence/MultiSenceService;->mHandler:Landroid/os/Handler;

    iget-object v2, p0, Lcom/miui/server/multisence/MultiSenceService;->mContext:Landroid/content/Context;

    invoke-direct {v0, p0, v1, v2}, Lcom/miui/server/multisence/MultiSenceDynamicController;-><init>(Lcom/miui/server/multisence/MultiSenceService;Landroid/os/Handler;Landroid/content/Context;)V

    iput-object v0, p0, Lcom/miui/server/multisence/MultiSenceService;->mController:Lcom/miui/server/multisence/MultiSenceDynamicController;

    .line 106
    invoke-virtual {v0}, Lcom/miui/server/multisence/MultiSenceDynamicController;->registerCloudObserver()V

    .line 107
    iget-object v0, p0, Lcom/miui/server/multisence/MultiSenceService;->mController:Lcom/miui/server/multisence/MultiSenceDynamicController;

    invoke-virtual {v0}, Lcom/miui/server/multisence/MultiSenceDynamicController;->registerVRSCloudObserver()V

    .line 108
    iget-object v0, p0, Lcom/miui/server/multisence/MultiSenceService;->mController:Lcom/miui/server/multisence/MultiSenceDynamicController;

    invoke-virtual {v0}, Lcom/miui/server/multisence/MultiSenceDynamicController;->registerFWCloudObserver()V

    .line 110
    iget-object v0, p0, Lcom/miui/server/multisence/MultiSenceService;->multiSencePackages:Ljava/util/Map;

    const/16 v1, 0x1001

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    const-string v2, "com.android.systemui"

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 111
    iget-object v0, p0, Lcom/miui/server/multisence/MultiSenceService;->multiSencePackages:Ljava/util/Map;

    const/16 v1, 0x1005

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 112
    iget-object v0, p0, Lcom/miui/server/multisence/MultiSenceService;->multiSencePackages:Ljava/util/Map;

    const/16 v1, 0x1006

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 113
    iget-object v0, p0, Lcom/miui/server/multisence/MultiSenceService;->multiSencePackages:Ljava/util/Map;

    const/16 v1, 0x1004

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 114
    iget-object v0, p0, Lcom/miui/server/multisence/MultiSenceService;->multiSencePackages:Ljava/util/Map;

    const/16 v1, 0x2001

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 115
    iget-object v0, p0, Lcom/miui/server/multisence/MultiSenceService;->multiSencePackages:Ljava/util/Map;

    const/16 v1, 0x4001

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 116
    return-void
.end method

.method private LOG_IF_DEBUG(Ljava/lang/String;)V
    .locals 1
    .param p1, "log"    # Ljava/lang/String;

    .line 509
    sget-boolean v0, Lcom/miui/server/multisence/MultiSenceService;->DEBUG:Z

    if-eqz v0, :cond_0

    .line 510
    const-string v0, "MultiSenceService"

    invoke-static {v0, p1}, Landroid/util/Slog;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 512
    :cond_0
    return-void
.end method

.method private createFileDevice(Ljava/lang/String;)V
    .locals 5
    .param p1, "name"    # Ljava/lang/String;

    .line 140
    const-string v0, "Create ending: "

    const-string v1, "MultiSenceService"

    :try_start_0
    new-instance v2, Ljava/io/File;

    invoke-direct {v2, p1}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 141
    .local v2, "mwdf_file":Ljava/io/File;
    invoke-virtual {v2}, Ljava/io/File;->exists()Z

    move-result v3

    if-nez v3, :cond_0

    .line 142
    invoke-virtual {v2}, Ljava/io/File;->createNewFile()Z

    .line 144
    :cond_0
    const/16 v3, 0x1b6

    invoke-static {p1, v3}, Landroid/system/Os;->chmod(Ljava/lang/String;I)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 148
    .end local v2    # "mwdf_file":Ljava/io/File;
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    :goto_0
    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v1, v0}, Landroid/util/Slog;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 149
    goto :goto_1

    .line 148
    :catchall_0
    move-exception v2

    goto :goto_2

    .line 145
    :catch_0
    move-exception v2

    .line 146
    .local v2, "e":Ljava/lang/Exception;
    :try_start_1
    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "Failed to create: "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, ", error: "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v2}, Ljava/lang/Exception;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v1, v3}, Landroid/util/Slog;->e(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 148
    nop

    .end local v2    # "e":Ljava/lang/Exception;
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    goto :goto_0

    .line 150
    :goto_1
    return-void

    .line 148
    :goto_2
    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v1, v0}, Landroid/util/Slog;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 149
    throw v2
.end method

.method private isScreenStateChanged(Ljava/util/Map;)Z
    .locals 7
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/Map<",
            "Ljava/lang/String;",
            "Lcom/miui/server/multisence/SingleWindowInfo;",
            ">;)Z"
        }
    .end annotation

    .line 410
    .local p1, "inWindows":Ljava/util/Map;, "Ljava/util/Map<Ljava/lang/String;Lcom/miui/server/multisence/SingleWindowInfo;>;"
    invoke-interface {p1}, Ljava/util/Map;->size()I

    move-result v0

    iget-object v1, p0, Lcom/miui/server/multisence/MultiSenceService;->mWindows:Ljava/util/Map;

    invoke-interface {v1}, Ljava/util/Map;->size()I

    move-result v1

    const/4 v2, 0x1

    if-eq v0, v1, :cond_0

    .line 411
    return v2

    .line 413
    :cond_0
    invoke-interface {p1}, Ljava/util/Map;->keySet()Ljava/util/Set;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v0

    :goto_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_6

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/String;

    .line 414
    .local v1, "name_in":Ljava/lang/String;
    iget-object v3, p0, Lcom/miui/server/multisence/MultiSenceService;->mWindows:Ljava/util/Map;

    invoke-interface {v3, v1}, Ljava/util/Map;->containsKey(Ljava/lang/Object;)Z

    move-result v3

    if-nez v3, :cond_1

    .line 415
    return v2

    .line 417
    :cond_1
    invoke-interface {p1, v1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/miui/server/multisence/SingleWindowInfo;

    .line 418
    .local v3, "inWindow":Lcom/miui/server/multisence/SingleWindowInfo;
    iget-object v4, p0, Lcom/miui/server/multisence/MultiSenceService;->mWindows:Ljava/util/Map;

    invoke-interface {v4, v1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lcom/miui/server/multisence/SingleWindowInfo;

    .line 419
    .local v4, "window":Lcom/miui/server/multisence/SingleWindowInfo;
    invoke-virtual {v3}, Lcom/miui/server/multisence/SingleWindowInfo;->isInputFocused()Z

    move-result v5

    invoke-virtual {v4}, Lcom/miui/server/multisence/SingleWindowInfo;->isInputFocused()Z

    move-result v6

    if-ne v5, v6, :cond_5

    .line 420
    invoke-virtual {v3}, Lcom/miui/server/multisence/SingleWindowInfo;->getWindowingModeString()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4}, Lcom/miui/server/multisence/SingleWindowInfo;->getWindowingModeString()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-nez v5, :cond_2

    goto :goto_1

    .line 423
    :cond_2
    invoke-virtual {v4}, Lcom/miui/server/multisence/SingleWindowInfo;->isInFreeform()Z

    move-result v5

    if-eqz v5, :cond_3

    invoke-virtual {v3}, Lcom/miui/server/multisence/SingleWindowInfo;->isInFreeform()Z

    move-result v5

    if-eqz v5, :cond_3

    .line 424
    invoke-virtual {v4}, Lcom/miui/server/multisence/SingleWindowInfo;->getWindowForm()Lcom/miui/server/multisence/SingleWindowInfo$WindowForm;

    move-result-object v5

    invoke-virtual {v3}, Lcom/miui/server/multisence/SingleWindowInfo;->getWindowForm()Lcom/miui/server/multisence/SingleWindowInfo$WindowForm;

    move-result-object v6

    if-eq v5, v6, :cond_3

    .line 425
    return v2

    .line 428
    :cond_3
    invoke-virtual {v4}, Lcom/miui/server/multisence/SingleWindowInfo;->getWindowForm()Lcom/miui/server/multisence/SingleWindowInfo$WindowForm;

    move-result-object v5

    sget-object v6, Lcom/miui/server/multisence/SingleWindowInfo$WindowForm;->MUTIL_FREEDOM:Lcom/miui/server/multisence/SingleWindowInfo$WindowForm;

    if-ne v5, v6, :cond_4

    .line 429
    invoke-virtual {v3}, Lcom/miui/server/multisence/SingleWindowInfo;->getWindowForm()Lcom/miui/server/multisence/SingleWindowInfo$WindowForm;

    move-result-object v5

    sget-object v6, Lcom/miui/server/multisence/SingleWindowInfo$WindowForm;->MUTIL_FREEDOM:Lcom/miui/server/multisence/SingleWindowInfo$WindowForm;

    if-ne v5, v6, :cond_4

    .line 430
    invoke-virtual {v4}, Lcom/miui/server/multisence/SingleWindowInfo;->getRectValue()Landroid/graphics/Rect;

    move-result-object v5

    invoke-virtual {v3}, Lcom/miui/server/multisence/SingleWindowInfo;->getRectValue()Landroid/graphics/Rect;

    move-result-object v6

    invoke-virtual {v5, v6}, Landroid/graphics/Rect;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-nez v5, :cond_4

    .line 431
    return v2

    .line 433
    .end local v1    # "name_in":Ljava/lang/String;
    .end local v3    # "inWindow":Lcom/miui/server/multisence/SingleWindowInfo;
    .end local v4    # "window":Lcom/miui/server/multisence/SingleWindowInfo;
    :cond_4
    goto :goto_0

    .line 421
    .restart local v1    # "name_in":Ljava/lang/String;
    .restart local v3    # "inWindow":Lcom/miui/server/multisence/SingleWindowInfo;
    .restart local v4    # "window":Lcom/miui/server/multisence/SingleWindowInfo;
    :cond_5
    :goto_1
    return v2

    .line 435
    .end local v1    # "name_in":Ljava/lang/String;
    .end local v3    # "inWindow":Lcom/miui/server/multisence/SingleWindowInfo;
    .end local v4    # "window":Lcom/miui/server/multisence/SingleWindowInfo;
    :cond_6
    const/4 v0, 0x0

    return v0
.end method


# virtual methods
.method protected clearServiceStatus()V
    .locals 1

    .line 193
    monitor-enter p0

    .line 194
    :try_start_0
    iget-object v0, p0, Lcom/miui/server/multisence/MultiSenceService;->mPolicy:Lcom/miui/server/multisence/MultiSenceServicePolicy;

    invoke-virtual {v0}, Lcom/miui/server/multisence/MultiSenceServicePolicy;->reset()V

    .line 195
    iget-object v0, p0, Lcom/miui/server/multisence/MultiSenceService;->mWindows:Ljava/util/Map;

    invoke-interface {v0}, Ljava/util/Map;->clear()V

    .line 196
    monitor-exit p0

    .line 197
    return-void

    .line 196
    :catchall_0
    move-exception v0

    monitor-exit p0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method

.method public floatingWindowClose([ILjava/lang/String;)V
    .locals 2
    .param p1, "req_tids"    # [I
    .param p2, "packageName"    # Ljava/lang/String;

    .line 335
    const/4 v0, 0x0

    const/16 v1, 0x5003

    invoke-virtual {p0, p1, p2, v0, v1}, Lcom/miui/server/multisence/MultiSenceService;->notifyMultiTaskActionEnd([ILjava/lang/String;Landroid/graphics/Rect;I)V

    .line 337
    return-void
.end method

.method public floatingWindowMoveEnd([ILjava/lang/String;Landroid/graphics/Rect;)V
    .locals 1
    .param p1, "req_tids"    # [I
    .param p2, "packageName"    # Ljava/lang/String;
    .param p3, "bounds"    # Landroid/graphics/Rect;

    .line 347
    const/16 v0, 0x5001

    invoke-virtual {p0, p1, p2, p3, v0}, Lcom/miui/server/multisence/MultiSenceService;->notifyMultiTaskActionEnd([ILjava/lang/String;Landroid/graphics/Rect;I)V

    .line 349
    return-void
.end method

.method public floatingWindowMoveStart([ILjava/lang/String;)V
    .locals 6
    .param p1, "req_tids"    # [I
    .param p2, "packageName"    # Ljava/lang/String;

    .line 340
    invoke-static {}, Landroid/os/Binder;->clearCallingIdentity()J

    move-result-wide v0

    .line 341
    .local v0, "origId":J
    iget-object v2, p0, Lcom/miui/server/multisence/MultiSenceService;->multiTaskActionManager:Lmiui/smartpower/MultiTaskActionManager;

    new-instance v3, Lmiui/smartpower/MultiTaskActionManager$ActionInfo;

    .line 342
    invoke-static {}, Landroid/os/Process;->myUid()I

    move-result v4

    const/16 v5, 0x5001

    invoke-direct {v3, v5, v4, p1}, Lmiui/smartpower/MultiTaskActionManager$ActionInfo;-><init>(II[I)V

    .line 341
    invoke-virtual {v2, v3}, Lmiui/smartpower/MultiTaskActionManager;->notifyMultiTaskActionStart(Lmiui/smartpower/MultiTaskActionManager$ActionInfo;)V

    .line 343
    invoke-static {v0, v1}, Landroid/os/Binder;->restoreCallingIdentity(J)V

    .line 344
    return-void
.end method

.method public floatingWindowShow([ILjava/lang/String;Landroid/graphics/Rect;)V
    .locals 1
    .param p1, "req_tids"    # [I
    .param p2, "packageName"    # Ljava/lang/String;
    .param p3, "bounds"    # Landroid/graphics/Rect;

    .line 330
    const/16 v0, 0x5002

    invoke-virtual {p0, p1, p2, p3, v0}, Lcom/miui/server/multisence/MultiSenceService;->notifyMultiTaskActionEnd([ILjava/lang/String;Landroid/graphics/Rect;I)V

    .line 332
    return-void
.end method

.method public getPolicy()Lcom/miui/server/multisence/MultiSenceServicePolicy;
    .locals 1

    .line 439
    iget-object v0, p0, Lcom/miui/server/multisence/MultiSenceService;->mPolicy:Lcom/miui/server/multisence/MultiSenceServicePolicy;

    return-object v0
.end method

.method protected notifyMultiSenceEnable(Z)V
    .locals 1
    .param p1, "isEnable"    # Z

    .line 443
    iget-object v0, p0, Lcom/miui/server/multisence/MultiSenceService;->mSmartPowerService:Lcom/miui/app/smartpower/SmartPowerServiceInternal;

    invoke-interface {v0, p1}, Lcom/miui/app/smartpower/SmartPowerServiceInternal;->notifyMultiSenceEnable(Z)V

    .line 444
    return-void
.end method

.method public notifyMultiTaskActionEnd([ILjava/lang/String;Landroid/graphics/Rect;I)V
    .locals 4
    .param p1, "req_tids"    # [I
    .param p2, "packageName"    # Ljava/lang/String;
    .param p3, "bounds"    # Landroid/graphics/Rect;
    .param p4, "senceId"    # I

    .line 353
    invoke-static {}, Landroid/os/Binder;->clearCallingIdentity()J

    move-result-wide v0

    .line 354
    .local v0, "origId":J
    new-instance v2, Lmiui/smartpower/MultiTaskActionManager$ActionInfo;

    invoke-static {}, Landroid/os/Process;->myUid()I

    move-result v3

    invoke-direct {v2, p4, v3, p1}, Lmiui/smartpower/MultiTaskActionManager$ActionInfo;-><init>(II[I)V

    .line 355
    .local v2, "floatingWindowInfo":Lmiui/smartpower/MultiTaskActionManager$ActionInfo;
    iget-object v3, p0, Lcom/miui/server/multisence/MultiSenceService;->multiTaskActionManager:Lmiui/smartpower/MultiTaskActionManager;

    invoke-virtual {v3, v2}, Lmiui/smartpower/MultiTaskActionManager;->notifyMultiTaskActionEnd(Lmiui/smartpower/MultiTaskActionManager$ActionInfo;)V

    .line 356
    invoke-static {v0, v1}, Landroid/os/Binder;->restoreCallingIdentity(J)V

    .line 357
    return-void
.end method

.method public onBootPhase(I)V
    .locals 3
    .param p1, "phase"    # I

    .line 125
    const/16 v0, 0x1f4

    if-ne p1, v0, :cond_0

    .line 126
    const-string v0, "System ready: 500"

    invoke-direct {p0, v0}, Lcom/miui/server/multisence/MultiSenceService;->LOG_IF_DEBUG(Ljava/lang/String;)V

    .line 127
    invoke-static {}, Lcom/miui/server/multisence/MultiSenceConfig;->getInstance()Lcom/miui/server/multisence/MultiSenceConfig;

    move-result-object v0

    invoke-virtual {v0}, Lcom/miui/server/multisence/MultiSenceConfig;->updateSubFuncStatus()V

    .line 128
    iget-object v0, p0, Lcom/miui/server/multisence/MultiSenceService;->mMultiSenceListener:Lcom/android/server/wm/MultiSenceListener;

    invoke-virtual {v0}, Lcom/android/server/wm/MultiSenceListener;->systemReady()Z

    .line 129
    invoke-static {}, Lcom/android/server/wm/MultiSenceManagerInternalStub;->getInstance()Lcom/android/server/wm/MultiSenceManagerInternalStub;

    move-result-object v0

    invoke-interface {v0}, Lcom/android/server/wm/MultiSenceManagerInternalStub;->systemReady()Z

    .line 130
    invoke-static {}, Lcom/android/server/wm/MultiSenceManagerInternalStub;->getInstance()Lcom/android/server/wm/MultiSenceManagerInternalStub;

    move-result-object v0

    iget-object v1, p0, Lcom/miui/server/multisence/MultiSenceService;->mContext:Landroid/content/Context;

    iget-object v2, p0, Lcom/miui/server/multisence/MultiSenceService;->mHandler:Landroid/os/Handler;

    invoke-interface {v0, v1, v2}, Lcom/android/server/wm/MultiSenceManagerInternalStub;->init(Landroid/content/Context;Landroid/os/Handler;)V

    goto :goto_0

    .line 131
    :cond_0
    const/16 v0, 0x3e8

    if-ne p1, v0, :cond_1

    .line 132
    invoke-static {}, Lcom/android/server/wm/MultiSenceManagerInternalStub;->getInstance()Lcom/android/server/wm/MultiSenceManagerInternalStub;

    move-result-object v0

    invoke-interface {v0}, Lcom/android/server/wm/MultiSenceManagerInternalStub;->bootComplete()V

    .line 133
    invoke-static {}, Lcom/miui/server/multisence/MultiSenceConfig;->getInstance()Lcom/miui/server/multisence/MultiSenceConfig;

    move-result-object v0

    invoke-virtual {v0}, Lcom/miui/server/multisence/MultiSenceConfig;->initVRSWhiteList()V

    .line 134
    invoke-static {}, Lcom/miui/server/multisence/MultiSenceConfig;->getInstance()Lcom/miui/server/multisence/MultiSenceConfig;

    move-result-object v0

    invoke-virtual {v0}, Lcom/miui/server/multisence/MultiSenceConfig;->initFWWhiteList()V

    .line 136
    :cond_1
    :goto_0
    return-void
.end method

.method public onStart()V
    .locals 2

    .line 120
    new-instance v0, Lcom/miui/server/multisence/MultiSenceService$BinderService;

    const/4 v1, 0x0

    invoke-direct {v0, p0, v1}, Lcom/miui/server/multisence/MultiSenceService$BinderService;-><init>(Lcom/miui/server/multisence/MultiSenceService;Lcom/miui/server/multisence/MultiSenceService$BinderService-IA;)V

    iput-object v0, p0, Lcom/miui/server/multisence/MultiSenceService;->mBinderService:Lcom/miui/server/multisence/MultiSenceService$BinderService;

    .line 121
    const-string v1, "miui_multi_sence"

    invoke-virtual {p0, v1, v0}, Lcom/miui/server/multisence/MultiSenceService;->publishBinderService(Ljava/lang/String;Landroid/os/IBinder;)V

    .line 122
    return-void
.end method

.method protected registerMultiTaskActionListener()V
    .locals 4

    .line 153
    sget-boolean v0, Lcom/miui/server/multisence/MultiSenceDynamicController;->IS_CLOUD_ENABLED:Z

    const-string v1, "MultiSenceService"

    if-nez v0, :cond_0

    .line 154
    const-string v0, "need not to register multisence listener due to cloud controller."

    invoke-static {v1, v0}, Landroid/util/Slog;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 155
    return-void

    .line 158
    :cond_0
    sget-boolean v0, Lcom/miui/server/multisence/MultiSenceService;->mMultiTaskActionListenerRegistered:Z

    if-eqz v0, :cond_1

    .line 159
    const-string v0, "Listener has been registered. Do not register again."

    invoke-static {v1, v0}, Landroid/util/Slog;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 160
    return-void

    .line 163
    :cond_1
    iget-object v0, p0, Lcom/miui/server/multisence/MultiSenceService;->mSmartPowerService:Lcom/miui/app/smartpower/SmartPowerServiceInternal;

    iget-object v1, p0, Lcom/miui/server/multisence/MultiSenceService;->mHandler:Landroid/os/Handler;

    iget-object v2, p0, Lcom/miui/server/multisence/MultiSenceService;->mMultiSenceListener:Lcom/android/server/wm/MultiSenceListener;

    const/4 v3, 0x2

    invoke-interface {v0, v3, v1, v2}, Lcom/miui/app/smartpower/SmartPowerServiceInternal;->registerMultiTaskActionListener(ILandroid/os/Handler;Lcom/miui/app/smartpower/SmartPowerServiceInternal$MultiTaskActionListener;)Z

    move-result v0

    .line 168
    .local v0, "isRegisterSuccess":Z
    if-eqz v0, :cond_2

    .line 169
    const/4 v1, 0x1

    sput-boolean v1, Lcom/miui/server/multisence/MultiSenceService;->mMultiTaskActionListenerRegistered:Z

    .line 171
    :cond_2
    return-void
.end method

.method public setUpdateReason(Ljava/lang/String;)V
    .locals 0
    .param p1, "reason"    # Ljava/lang/String;

    .line 326
    iput-object p1, p0, Lcom/miui/server/multisence/MultiSenceService;->mReason:Ljava/lang/String;

    .line 327
    return-void
.end method

.method public showAllWindowInfo(Ljava/lang/String;)V
    .locals 6
    .param p1, "reason"    # Ljava/lang/String;

    .line 447
    sget-boolean v0, Lcom/miui/server/multisence/MultiSenceService;->DEBUG:Z

    if-nez v0, :cond_0

    .line 448
    return-void

    .line 450
    :cond_0
    const/4 v0, 0x0

    .line 452
    .local v0, "count":I
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "reason: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {p0, v1}, Lcom/miui/server/multisence/MultiSenceService;->LOG_IF_DEBUG(Ljava/lang/String;)V

    .line 454
    iget-object v1, p0, Lcom/miui/server/multisence/MultiSenceService;->mWindows:Ljava/util/Map;

    invoke-interface {v1}, Ljava/util/Map;->keySet()Ljava/util/Set;

    move-result-object v1

    invoke-interface {v1}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_1

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/lang/String;

    .line 455
    .local v2, "key":Ljava/lang/String;
    add-int/lit8 v0, v0, 0x1

    .line 456
    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "["

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, "]: "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, "-"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    iget-object v5, p0, Lcom/miui/server/multisence/MultiSenceService;->mWindows:Ljava/util/Map;

    .line 457
    invoke-interface {v5, v2}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Lcom/miui/server/multisence/SingleWindowInfo;

    invoke-virtual {v5}, Lcom/miui/server/multisence/SingleWindowInfo;->getWindowingModeString()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v3, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    iget-object v5, p0, Lcom/miui/server/multisence/MultiSenceService;->mWindows:Ljava/util/Map;

    .line 458
    invoke-interface {v5, v2}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Lcom/miui/server/multisence/SingleWindowInfo;

    invoke-virtual {v5}, Lcom/miui/server/multisence/SingleWindowInfo;->isInputFocused()Z

    move-result v5

    invoke-virtual {v3, v5}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    iget-object v4, p0, Lcom/miui/server/multisence/MultiSenceService;->mWindows:Ljava/util/Map;

    .line 459
    invoke-interface {v4, v2}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lcom/miui/server/multisence/SingleWindowInfo;

    invoke-virtual {v4}, Lcom/miui/server/multisence/SingleWindowInfo;->isVisible()Z

    move-result v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    .line 456
    invoke-direct {p0, v3}, Lcom/miui/server/multisence/MultiSenceService;->LOG_IF_DEBUG(Ljava/lang/String;)V

    .line 460
    .end local v2    # "key":Ljava/lang/String;
    goto :goto_0

    .line 462
    :cond_1
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "There are ["

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "] windows in Screen"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {p0, v1}, Lcom/miui/server/multisence/MultiSenceService;->LOG_IF_DEBUG(Ljava/lang/String;)V

    .line 463
    return-void
.end method

.method protected unregisterMultiTaskActionListener()V
    .locals 3

    .line 174
    sget-boolean v0, Lcom/miui/server/multisence/MultiSenceService;->mMultiTaskActionListenerRegistered:Z

    const-string v1, "MultiSenceService"

    if-nez v0, :cond_0

    .line 175
    const-string v0, "Listener has not been registered. Do need to unregister."

    invoke-static {v1, v0}, Landroid/util/Slog;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 176
    return-void

    .line 178
    :cond_0
    sget-boolean v0, Lcom/miui/server/multisence/MultiSenceDynamicController;->IS_CLOUD_ENABLED:Z

    if-eqz v0, :cond_1

    .line 179
    const-string v0, "need not to unregister multisence listener due to cloud controller."

    invoke-static {v1, v0}, Landroid/util/Slog;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 180
    return-void

    .line 183
    :cond_1
    iget-object v0, p0, Lcom/miui/server/multisence/MultiSenceService;->mSmartPowerService:Lcom/miui/app/smartpower/SmartPowerServiceInternal;

    const/4 v1, 0x2

    iget-object v2, p0, Lcom/miui/server/multisence/MultiSenceService;->mMultiSenceListener:Lcom/android/server/wm/MultiSenceListener;

    invoke-interface {v0, v1, v2}, Lcom/miui/app/smartpower/SmartPowerServiceInternal;->unregisterMultiTaskActionListener(ILcom/miui/app/smartpower/SmartPowerServiceInternal$MultiTaskActionListener;)Z

    move-result v0

    .line 187
    .local v0, "isUnRegisterSuccess":Z
    if-eqz v0, :cond_2

    .line 188
    const/4 v1, 0x0

    sput-boolean v1, Lcom/miui/server/multisence/MultiSenceService;->mMultiTaskActionListenerRegistered:Z

    .line 190
    :cond_2
    return-void
.end method

.method public updateStaticWindowsInfo()V
    .locals 5

    .line 372
    iget-object v0, p0, Lcom/miui/server/multisence/MultiSenceService;->mNewWindows:Ljava/util/Map;

    invoke-interface {v0}, Ljava/util/Map;->keySet()Ljava/util/Set;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v0

    :goto_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_2

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/String;

    .line 373
    .local v1, "key":Ljava/lang/String;
    iget-object v2, p0, Lcom/miui/server/multisence/MultiSenceService;->mNewWindows:Ljava/util/Map;

    invoke-interface {v2, v1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/miui/server/multisence/SingleWindowInfo;

    .line 374
    .local v2, "window":Lcom/miui/server/multisence/SingleWindowInfo;
    if-nez v2, :cond_0

    .line 375
    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v4, "window {"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string/jumbo v4, "} in null"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    const-string v4, "MultiSenceService"

    invoke-static {v4, v3}, Landroid/util/Slog;->w(Ljava/lang/String;Ljava/lang/String;)I

    .line 376
    goto :goto_0

    .line 378
    :cond_0
    invoke-virtual {v2}, Lcom/miui/server/multisence/SingleWindowInfo;->isInputFocused()Z

    move-result v3

    if-eqz v3, :cond_1

    .line 379
    invoke-virtual {v2}, Lcom/miui/server/multisence/SingleWindowInfo;->getPackageName()Ljava/lang/String;

    move-result-object v0

    .line 380
    .local v0, "newInputFocus":Ljava/lang/String;
    iget-object v3, p0, Lcom/miui/server/multisence/MultiSenceService;->mPolicy:Lcom/miui/server/multisence/MultiSenceServicePolicy;

    invoke-virtual {v3, v0}, Lcom/miui/server/multisence/MultiSenceServicePolicy;->updateInputFocus(Ljava/lang/String;)V

    .line 381
    goto :goto_1

    .line 383
    .end local v0    # "newInputFocus":Ljava/lang/String;
    .end local v1    # "key":Ljava/lang/String;
    .end local v2    # "window":Lcom/miui/server/multisence/SingleWindowInfo;
    :cond_1
    goto :goto_0

    .line 385
    :cond_2
    :goto_1
    sget-boolean v0, Lcom/miui/server/multisence/MultiSenceDynamicController;->IS_CLOUD_ENABLED:Z

    if-nez v0, :cond_3

    .line 386
    const-string v0, "Cloud Controller refuses."

    invoke-direct {p0, v0}, Lcom/miui/server/multisence/MultiSenceService;->LOG_IF_DEBUG(Ljava/lang/String;)V

    .line 387
    return-void

    .line 390
    :cond_3
    iget-object v0, p0, Lcom/miui/server/multisence/MultiSenceService;->mNewWindows:Ljava/util/Map;

    invoke-direct {p0, v0}, Lcom/miui/server/multisence/MultiSenceService;->isScreenStateChanged(Ljava/util/Map;)Z

    move-result v0

    if-nez v0, :cond_4

    .line 391
    const-string v0, "not changed"

    invoke-virtual {p0, v0}, Lcom/miui/server/multisence/MultiSenceService;->showAllWindowInfo(Ljava/lang/String;)V

    .line 392
    return-void

    .line 396
    :cond_4
    iget-object v0, p0, Lcom/miui/server/multisence/MultiSenceService;->mPolicy:Lcom/miui/server/multisence/MultiSenceServicePolicy;

    iget-object v1, p0, Lcom/miui/server/multisence/MultiSenceService;->mNewWindows:Ljava/util/Map;

    invoke-virtual {v0, v1}, Lcom/miui/server/multisence/MultiSenceServicePolicy;->windowInfoPerPorcessing(Ljava/util/Map;)V

    .line 399
    iget-object v0, p0, Lcom/miui/server/multisence/MultiSenceService;->mPolicy:Lcom/miui/server/multisence/MultiSenceServicePolicy;

    iget-object v1, p0, Lcom/miui/server/multisence/MultiSenceService;->mWindows:Ljava/util/Map;

    iget-object v2, p0, Lcom/miui/server/multisence/MultiSenceService;->mNewWindows:Ljava/util/Map;

    invoke-virtual {v0, v1, v2}, Lcom/miui/server/multisence/MultiSenceServicePolicy;->updateSchedPolicy(Ljava/util/Map;Ljava/util/Map;)V

    .line 402
    iget-object v0, p0, Lcom/miui/server/multisence/MultiSenceService;->mPolicy:Lcom/miui/server/multisence/MultiSenceServicePolicy;

    invoke-virtual {v0}, Lcom/miui/server/multisence/MultiSenceServicePolicy;->doSched()V

    .line 405
    iget-object v0, p0, Lcom/miui/server/multisence/MultiSenceService;->mNewWindows:Ljava/util/Map;

    iput-object v0, p0, Lcom/miui/server/multisence/MultiSenceService;->mWindows:Ljava/util/Map;

    .line 406
    iget-object v0, p0, Lcom/miui/server/multisence/MultiSenceService;->mReason:Ljava/lang/String;

    invoke-virtual {p0, v0}, Lcom/miui/server/multisence/MultiSenceService;->showAllWindowInfo(Ljava/lang/String;)V

    .line 407
    return-void
.end method

.method public updateStaticWindowsInfo(Ljava/util/Map;)V
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/Map<",
            "Ljava/lang/String;",
            "Lcom/miui/server/multisence/SingleWindowInfo;",
            ">;)V"
        }
    .end annotation

    .line 366
    .local p1, "inWindows":Ljava/util/Map;, "Ljava/util/Map<Ljava/lang/String;Lcom/miui/server/multisence/SingleWindowInfo;>;"
    iget-object v0, p0, Lcom/miui/server/multisence/MultiSenceService;->mHandler:Landroid/os/Handler;

    iget-object v1, p0, Lcom/miui/server/multisence/MultiSenceService;->mUpdateStaticWindows:Ljava/lang/Runnable;

    invoke-virtual {v0, v1}, Landroid/os/Handler;->removeCallbacks(Ljava/lang/Runnable;)V

    .line 367
    iput-object p1, p0, Lcom/miui/server/multisence/MultiSenceService;->mNewWindows:Ljava/util/Map;

    .line 368
    iget-object v0, p0, Lcom/miui/server/multisence/MultiSenceService;->mHandler:Landroid/os/Handler;

    iget-object v1, p0, Lcom/miui/server/multisence/MultiSenceService;->mUpdateStaticWindows:Ljava/lang/Runnable;

    const-wide/16 v2, 0x32

    invoke-virtual {v0, v1, v2, v3}, Landroid/os/Handler;->postDelayed(Ljava/lang/Runnable;J)Z

    .line 369
    return-void
.end method
