public class com.miui.server.multisence.MultiSenceService extends com.android.server.SystemService {
	 /* .source "MultiSenceService.java" */
	 /* # annotations */
	 /* .annotation system Ldalvik/annotation/MemberClasses; */
	 /* value = { */
	 /* Lcom/miui/server/multisence/MultiSenceService$BinderService;, */
	 /* Lcom/miui/server/multisence/MultiSenceService$MultiSenceThread;, */
	 /* Lcom/miui/server/multisence/MultiSenceService$H;, */
	 /* Lcom/miui/server/multisence/MultiSenceService$MultiSenceShellCmd; */
	 /* } */
} // .end annotation
/* # static fields */
private static final Boolean DEBUG;
private static final java.lang.String DEFAULT_MULTISENCE_FREEFORM_PACKAGE;
private static final Integer DELAY_MS;
public static Boolean IS_SERVICE_ENABLED;
private static final java.lang.String MCD_DF_PATH;
public static final java.lang.String SERVICE_NAME;
private static final java.lang.String TAG;
private static Boolean mMultiTaskActionListenerRegistered;
/* # instance fields */
private com.miui.server.multisence.MultiSenceService$BinderService mBinderService;
private android.content.Context mContext;
private com.miui.server.multisence.MultiSenceDynamicController mController;
public android.os.Handler mHandler;
com.android.server.wm.MultiSenceListener mMultiSenceListener;
private java.util.Map mNewWindows;
/* .annotation system Ldalvik/annotation/Signature; */
/* value = { */
/* "Ljava/util/Map<", */
/* "Ljava/lang/String;", */
/* "Lcom/miui/server/multisence/SingleWindowInfo;", */
/* ">;" */
/* } */
} // .end annotation
} // .end field
private com.miui.server.multisence.MultiSenceServicePolicy mPolicy;
private java.lang.String mReason;
private final com.miui.app.smartpower.SmartPowerServiceInternal mSmartPowerService;
private com.android.server.ServiceThread mThread;
private final java.lang.Runnable mUpdateStaticWindows;
private java.util.Map mWindows;
/* .annotation system Ldalvik/annotation/Signature; */
/* value = { */
/* "Ljava/util/Map<", */
/* "Ljava/lang/String;", */
/* "Lcom/miui/server/multisence/SingleWindowInfo;", */
/* ">;" */
/* } */
} // .end annotation
} // .end field
private java.util.Map multiSencePackages;
/* .annotation system Ldalvik/annotation/Signature; */
/* value = { */
/* "Ljava/util/Map<", */
/* "Ljava/lang/Integer;", */
/* "Ljava/lang/String;", */
/* ">;" */
/* } */
} // .end annotation
} // .end field
private miui.smartpower.MultiTaskActionManager multiTaskActionManager;
/* # direct methods */
static android.content.Context -$$Nest$fgetmContext ( com.miui.server.multisence.MultiSenceService p0 ) { //bridge//synthethic
/* .locals 0 */
p0 = this.mContext;
} // .end method
static com.miui.server.multisence.MultiSenceServicePolicy -$$Nest$fgetmPolicy ( com.miui.server.multisence.MultiSenceService p0 ) { //bridge//synthethic
/* .locals 0 */
p0 = this.mPolicy;
} // .end method
static java.util.Map -$$Nest$fgetmultiSencePackages ( com.miui.server.multisence.MultiSenceService p0 ) { //bridge//synthethic
/* .locals 0 */
p0 = this.multiSencePackages;
} // .end method
static com.miui.server.multisence.MultiSenceService ( ) {
/* .locals 2 */
/* .line 62 */
final String v0 = "persist.multisence.enable"; // const-string v0, "persist.multisence.enable"
int v1 = 1; // const/4 v1, 0x1
v0 = android.os.SystemProperties .getBoolean ( v0,v1 );
com.miui.server.multisence.MultiSenceService.IS_SERVICE_ENABLED = (v0!= 0);
/* .line 63 */
final String v0 = "persist.multisence.debug.on"; // const-string v0, "persist.multisence.debug.on"
int v1 = 0; // const/4 v1, 0x0
v0 = android.os.SystemProperties .getBoolean ( v0,v1 );
com.miui.server.multisence.MultiSenceService.DEBUG = (v0!= 0);
/* .line 85 */
com.miui.server.multisence.MultiSenceService.mMultiTaskActionListenerRegistered = (v1!= 0);
return;
} // .end method
public com.miui.server.multisence.MultiSenceService ( ) {
/* .locals 3 */
/* .param p1, "context" # Landroid/content/Context; */
/* .line 90 */
/* invoke-direct {p0, p1}, Lcom/android/server/SystemService;-><init>(Landroid/content/Context;)V */
/* .line 66 */
/* new-instance v0, Ljava/util/HashMap; */
/* invoke-direct {v0}, Ljava/util/HashMap;-><init>()V */
this.mWindows = v0;
/* .line 67 */
int v0 = 0; // const/4 v0, 0x0
this.mNewWindows = v0;
/* .line 69 */
/* new-instance v1, Ljava/util/HashMap; */
/* invoke-direct {v1}, Ljava/util/HashMap;-><init>()V */
this.multiSencePackages = v1;
/* .line 71 */
this.mPolicy = v0;
/* .line 72 */
this.mBinderService = v0;
/* .line 73 */
/* const-class v0, Lcom/miui/app/smartpower/SmartPowerServiceInternal; */
com.android.server.LocalServices .getService ( v0 );
/* check-cast v0, Lcom/miui/app/smartpower/SmartPowerServiceInternal; */
this.mSmartPowerService = v0;
/* .line 74 */
/* new-instance v0, Lmiui/smartpower/MultiTaskActionManager; */
/* invoke-direct {v0}, Lmiui/smartpower/MultiTaskActionManager;-><init>()V */
this.multiTaskActionManager = v0;
/* .line 81 */
/* const-string/jumbo v0, "unknown" */
this.mReason = v0;
/* .line 359 */
/* new-instance v0, Lcom/miui/server/multisence/MultiSenceService$1; */
/* invoke-direct {v0, p0}, Lcom/miui/server/multisence/MultiSenceService$1;-><init>(Lcom/miui/server/multisence/MultiSenceService;)V */
this.mUpdateStaticWindows = v0;
/* .line 91 */
this.mContext = p1;
/* .line 92 */
/* new-instance v0, Lcom/miui/server/multisence/MultiSenceServicePolicy; */
/* invoke-direct {v0}, Lcom/miui/server/multisence/MultiSenceServicePolicy;-><init>()V */
this.mPolicy = v0;
/* .line 93 */
com.miui.server.multisence.MultiSenceService$MultiSenceThread .getInstance ( );
this.mThread = v0;
/* .line 94 */
/* new-instance v0, Lcom/miui/server/multisence/MultiSenceService$H; */
v1 = this.mThread;
(( com.android.server.ServiceThread ) v1 ).getLooper ( ); // invoke-virtual {v1}, Lcom/android/server/ServiceThread;->getLooper()Landroid/os/Looper;
/* invoke-direct {v0, p0, v1}, Lcom/miui/server/multisence/MultiSenceService$H;-><init>(Lcom/miui/server/multisence/MultiSenceService;Landroid/os/Looper;)V */
this.mHandler = v0;
/* .line 95 */
/* new-instance v0, Lcom/android/server/wm/MultiSenceListener; */
v1 = this.mThread;
(( com.android.server.ServiceThread ) v1 ).getLooper ( ); // invoke-virtual {v1}, Lcom/android/server/ServiceThread;->getLooper()Landroid/os/Looper;
/* invoke-direct {v0, v1}, Lcom/android/server/wm/MultiSenceListener;-><init>(Landroid/os/Looper;)V */
this.mMultiSenceListener = v0;
/* .line 97 */
com.miui.server.multisence.MultiSenceConfig .getInstance ( );
v1 = this.mContext;
(( com.miui.server.multisence.MultiSenceConfig ) v0 ).init ( p0, v1 ); // invoke-virtual {v0, p0, v1}, Lcom/miui/server/multisence/MultiSenceConfig;->init(Lcom/miui/server/multisence/MultiSenceService;Landroid/content/Context;)V
/* .line 100 */
final String v0 = "/data/system/mcd/mwdf"; // const-string v0, "/data/system/mcd/mwdf"
/* invoke-direct {p0, v0}, Lcom/miui/server/multisence/MultiSenceService;->createFileDevice(Ljava/lang/String;)V */
/* .line 102 */
(( com.miui.server.multisence.MultiSenceService ) p0 ).registerMultiTaskActionListener ( ); // invoke-virtual {p0}, Lcom/miui/server/multisence/MultiSenceService;->registerMultiTaskActionListener()V
/* .line 105 */
/* new-instance v0, Lcom/miui/server/multisence/MultiSenceDynamicController; */
v1 = this.mHandler;
v2 = this.mContext;
/* invoke-direct {v0, p0, v1, v2}, Lcom/miui/server/multisence/MultiSenceDynamicController;-><init>(Lcom/miui/server/multisence/MultiSenceService;Landroid/os/Handler;Landroid/content/Context;)V */
this.mController = v0;
/* .line 106 */
(( com.miui.server.multisence.MultiSenceDynamicController ) v0 ).registerCloudObserver ( ); // invoke-virtual {v0}, Lcom/miui/server/multisence/MultiSenceDynamicController;->registerCloudObserver()V
/* .line 107 */
v0 = this.mController;
(( com.miui.server.multisence.MultiSenceDynamicController ) v0 ).registerVRSCloudObserver ( ); // invoke-virtual {v0}, Lcom/miui/server/multisence/MultiSenceDynamicController;->registerVRSCloudObserver()V
/* .line 108 */
v0 = this.mController;
(( com.miui.server.multisence.MultiSenceDynamicController ) v0 ).registerFWCloudObserver ( ); // invoke-virtual {v0}, Lcom/miui/server/multisence/MultiSenceDynamicController;->registerFWCloudObserver()V
/* .line 110 */
v0 = this.multiSencePackages;
/* const/16 v1, 0x1001 */
java.lang.Integer .valueOf ( v1 );
final String v2 = "com.android.systemui"; // const-string v2, "com.android.systemui"
/* .line 111 */
v0 = this.multiSencePackages;
/* const/16 v1, 0x1005 */
java.lang.Integer .valueOf ( v1 );
/* .line 112 */
v0 = this.multiSencePackages;
/* const/16 v1, 0x1006 */
java.lang.Integer .valueOf ( v1 );
/* .line 113 */
v0 = this.multiSencePackages;
/* const/16 v1, 0x1004 */
java.lang.Integer .valueOf ( v1 );
/* .line 114 */
v0 = this.multiSencePackages;
/* const/16 v1, 0x2001 */
java.lang.Integer .valueOf ( v1 );
/* .line 115 */
v0 = this.multiSencePackages;
/* const/16 v1, 0x4001 */
java.lang.Integer .valueOf ( v1 );
/* .line 116 */
return;
} // .end method
private void LOG_IF_DEBUG ( java.lang.String p0 ) {
/* .locals 1 */
/* .param p1, "log" # Ljava/lang/String; */
/* .line 509 */
/* sget-boolean v0, Lcom/miui/server/multisence/MultiSenceService;->DEBUG:Z */
if ( v0 != null) { // if-eqz v0, :cond_0
/* .line 510 */
final String v0 = "MultiSenceService"; // const-string v0, "MultiSenceService"
android.util.Slog .d ( v0,p1 );
/* .line 512 */
} // :cond_0
return;
} // .end method
private void createFileDevice ( java.lang.String p0 ) {
/* .locals 5 */
/* .param p1, "name" # Ljava/lang/String; */
/* .line 140 */
final String v0 = "Create ending: "; // const-string v0, "Create ending: "
final String v1 = "MultiSenceService"; // const-string v1, "MultiSenceService"
try { // :try_start_0
/* new-instance v2, Ljava/io/File; */
/* invoke-direct {v2, p1}, Ljava/io/File;-><init>(Ljava/lang/String;)V */
/* .line 141 */
/* .local v2, "mwdf_file":Ljava/io/File; */
v3 = (( java.io.File ) v2 ).exists ( ); // invoke-virtual {v2}, Ljava/io/File;->exists()Z
/* if-nez v3, :cond_0 */
/* .line 142 */
(( java.io.File ) v2 ).createNewFile ( ); // invoke-virtual {v2}, Ljava/io/File;->createNewFile()Z
/* .line 144 */
} // :cond_0
/* const/16 v3, 0x1b6 */
android.system.Os .chmod ( p1,v3 );
/* :try_end_0 */
/* .catch Ljava/lang/Exception; {:try_start_0 ..:try_end_0} :catch_0 */
/* .catchall {:try_start_0 ..:try_end_0} :catchall_0 */
/* .line 148 */
} // .end local v2 # "mwdf_file":Ljava/io/File;
/* new-instance v2, Ljava/lang/StringBuilder; */
/* invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V */
} // :goto_0
(( java.lang.StringBuilder ) v2 ).append ( v0 ); // invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v0 ).append ( p1 ); // invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v0 ).toString ( ); // invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
android.util.Slog .e ( v1,v0 );
/* .line 149 */
/* .line 148 */
/* :catchall_0 */
/* move-exception v2 */
/* .line 145 */
/* :catch_0 */
/* move-exception v2 */
/* .line 146 */
/* .local v2, "e":Ljava/lang/Exception; */
try { // :try_start_1
/* new-instance v3, Ljava/lang/StringBuilder; */
/* invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V */
final String v4 = "Failed to create: "; // const-string v4, "Failed to create: "
(( java.lang.StringBuilder ) v3 ).append ( v4 ); // invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v3 ).append ( p1 ); // invoke-virtual {v3, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
final String v4 = ", error: "; // const-string v4, ", error: "
(( java.lang.StringBuilder ) v3 ).append ( v4 ); // invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.Exception ) v2 ).toString ( ); // invoke-virtual {v2}, Ljava/lang/Exception;->toString()Ljava/lang/String;
(( java.lang.StringBuilder ) v3 ).append ( v4 ); // invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v3 ).toString ( ); // invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
android.util.Slog .e ( v1,v3 );
/* :try_end_1 */
/* .catchall {:try_start_1 ..:try_end_1} :catchall_0 */
/* .line 148 */
/* nop */
} // .end local v2 # "e":Ljava/lang/Exception;
/* new-instance v2, Ljava/lang/StringBuilder; */
/* invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V */
/* .line 150 */
} // :goto_1
return;
/* .line 148 */
} // :goto_2
/* new-instance v3, Ljava/lang/StringBuilder; */
/* invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V */
(( java.lang.StringBuilder ) v3 ).append ( v0 ); // invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v0 ).append ( p1 ); // invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v0 ).toString ( ); // invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
android.util.Slog .e ( v1,v0 );
/* .line 149 */
/* throw v2 */
} // .end method
private Boolean isScreenStateChanged ( java.util.Map p0 ) {
/* .locals 7 */
/* .annotation system Ldalvik/annotation/Signature; */
/* value = { */
/* "(", */
/* "Ljava/util/Map<", */
/* "Ljava/lang/String;", */
/* "Lcom/miui/server/multisence/SingleWindowInfo;", */
/* ">;)Z" */
/* } */
} // .end annotation
/* .line 410 */
v0 = /* .local p1, "inWindows":Ljava/util/Map;, "Ljava/util/Map<Ljava/lang/String;Lcom/miui/server/multisence/SingleWindowInfo;>;" */
v1 = v1 = this.mWindows;
int v2 = 1; // const/4 v2, 0x1
/* if-eq v0, v1, :cond_0 */
/* .line 411 */
/* .line 413 */
} // :cond_0
v1 = } // :goto_0
if ( v1 != null) { // if-eqz v1, :cond_6
/* check-cast v1, Ljava/lang/String; */
/* .line 414 */
/* .local v1, "name_in":Ljava/lang/String; */
v3 = v3 = this.mWindows;
/* if-nez v3, :cond_1 */
/* .line 415 */
/* .line 417 */
} // :cond_1
/* check-cast v3, Lcom/miui/server/multisence/SingleWindowInfo; */
/* .line 418 */
/* .local v3, "inWindow":Lcom/miui/server/multisence/SingleWindowInfo; */
v4 = this.mWindows;
/* check-cast v4, Lcom/miui/server/multisence/SingleWindowInfo; */
/* .line 419 */
/* .local v4, "window":Lcom/miui/server/multisence/SingleWindowInfo; */
v5 = (( com.miui.server.multisence.SingleWindowInfo ) v3 ).isInputFocused ( ); // invoke-virtual {v3}, Lcom/miui/server/multisence/SingleWindowInfo;->isInputFocused()Z
v6 = (( com.miui.server.multisence.SingleWindowInfo ) v4 ).isInputFocused ( ); // invoke-virtual {v4}, Lcom/miui/server/multisence/SingleWindowInfo;->isInputFocused()Z
/* if-ne v5, v6, :cond_5 */
/* .line 420 */
(( com.miui.server.multisence.SingleWindowInfo ) v3 ).getWindowingModeString ( ); // invoke-virtual {v3}, Lcom/miui/server/multisence/SingleWindowInfo;->getWindowingModeString()Ljava/lang/String;
(( com.miui.server.multisence.SingleWindowInfo ) v4 ).getWindowingModeString ( ); // invoke-virtual {v4}, Lcom/miui/server/multisence/SingleWindowInfo;->getWindowingModeString()Ljava/lang/String;
v5 = (( java.lang.String ) v5 ).equals ( v6 ); // invoke-virtual {v5, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z
/* if-nez v5, :cond_2 */
/* .line 423 */
} // :cond_2
v5 = (( com.miui.server.multisence.SingleWindowInfo ) v4 ).isInFreeform ( ); // invoke-virtual {v4}, Lcom/miui/server/multisence/SingleWindowInfo;->isInFreeform()Z
if ( v5 != null) { // if-eqz v5, :cond_3
v5 = (( com.miui.server.multisence.SingleWindowInfo ) v3 ).isInFreeform ( ); // invoke-virtual {v3}, Lcom/miui/server/multisence/SingleWindowInfo;->isInFreeform()Z
if ( v5 != null) { // if-eqz v5, :cond_3
/* .line 424 */
(( com.miui.server.multisence.SingleWindowInfo ) v4 ).getWindowForm ( ); // invoke-virtual {v4}, Lcom/miui/server/multisence/SingleWindowInfo;->getWindowForm()Lcom/miui/server/multisence/SingleWindowInfo$WindowForm;
(( com.miui.server.multisence.SingleWindowInfo ) v3 ).getWindowForm ( ); // invoke-virtual {v3}, Lcom/miui/server/multisence/SingleWindowInfo;->getWindowForm()Lcom/miui/server/multisence/SingleWindowInfo$WindowForm;
/* if-eq v5, v6, :cond_3 */
/* .line 425 */
/* .line 428 */
} // :cond_3
(( com.miui.server.multisence.SingleWindowInfo ) v4 ).getWindowForm ( ); // invoke-virtual {v4}, Lcom/miui/server/multisence/SingleWindowInfo;->getWindowForm()Lcom/miui/server/multisence/SingleWindowInfo$WindowForm;
v6 = com.miui.server.multisence.SingleWindowInfo$WindowForm.MUTIL_FREEDOM;
/* if-ne v5, v6, :cond_4 */
/* .line 429 */
(( com.miui.server.multisence.SingleWindowInfo ) v3 ).getWindowForm ( ); // invoke-virtual {v3}, Lcom/miui/server/multisence/SingleWindowInfo;->getWindowForm()Lcom/miui/server/multisence/SingleWindowInfo$WindowForm;
v6 = com.miui.server.multisence.SingleWindowInfo$WindowForm.MUTIL_FREEDOM;
/* if-ne v5, v6, :cond_4 */
/* .line 430 */
(( com.miui.server.multisence.SingleWindowInfo ) v4 ).getRectValue ( ); // invoke-virtual {v4}, Lcom/miui/server/multisence/SingleWindowInfo;->getRectValue()Landroid/graphics/Rect;
(( com.miui.server.multisence.SingleWindowInfo ) v3 ).getRectValue ( ); // invoke-virtual {v3}, Lcom/miui/server/multisence/SingleWindowInfo;->getRectValue()Landroid/graphics/Rect;
v5 = (( android.graphics.Rect ) v5 ).equals ( v6 ); // invoke-virtual {v5, v6}, Landroid/graphics/Rect;->equals(Ljava/lang/Object;)Z
/* if-nez v5, :cond_4 */
/* .line 431 */
/* .line 433 */
} // .end local v1 # "name_in":Ljava/lang/String;
} // .end local v3 # "inWindow":Lcom/miui/server/multisence/SingleWindowInfo;
} // .end local v4 # "window":Lcom/miui/server/multisence/SingleWindowInfo;
} // :cond_4
/* .line 421 */
/* .restart local v1 # "name_in":Ljava/lang/String; */
/* .restart local v3 # "inWindow":Lcom/miui/server/multisence/SingleWindowInfo; */
/* .restart local v4 # "window":Lcom/miui/server/multisence/SingleWindowInfo; */
} // :cond_5
} // :goto_1
/* .line 435 */
} // .end local v1 # "name_in":Ljava/lang/String;
} // .end local v3 # "inWindow":Lcom/miui/server/multisence/SingleWindowInfo;
} // .end local v4 # "window":Lcom/miui/server/multisence/SingleWindowInfo;
} // :cond_6
int v0 = 0; // const/4 v0, 0x0
} // .end method
/* # virtual methods */
protected void clearServiceStatus ( ) {
/* .locals 1 */
/* .line 193 */
/* monitor-enter p0 */
/* .line 194 */
try { // :try_start_0
v0 = this.mPolicy;
(( com.miui.server.multisence.MultiSenceServicePolicy ) v0 ).reset ( ); // invoke-virtual {v0}, Lcom/miui/server/multisence/MultiSenceServicePolicy;->reset()V
/* .line 195 */
v0 = this.mWindows;
/* .line 196 */
/* monitor-exit p0 */
/* .line 197 */
return;
/* .line 196 */
/* :catchall_0 */
/* move-exception v0 */
/* monitor-exit p0 */
/* :try_end_0 */
/* .catchall {:try_start_0 ..:try_end_0} :catchall_0 */
/* throw v0 */
} // .end method
public void floatingWindowClose ( Integer[] p0, java.lang.String p1 ) {
/* .locals 2 */
/* .param p1, "req_tids" # [I */
/* .param p2, "packageName" # Ljava/lang/String; */
/* .line 335 */
int v0 = 0; // const/4 v0, 0x0
/* const/16 v1, 0x5003 */
(( com.miui.server.multisence.MultiSenceService ) p0 ).notifyMultiTaskActionEnd ( p1, p2, v0, v1 ); // invoke-virtual {p0, p1, p2, v0, v1}, Lcom/miui/server/multisence/MultiSenceService;->notifyMultiTaskActionEnd([ILjava/lang/String;Landroid/graphics/Rect;I)V
/* .line 337 */
return;
} // .end method
public void floatingWindowMoveEnd ( Integer[] p0, java.lang.String p1, android.graphics.Rect p2 ) {
/* .locals 1 */
/* .param p1, "req_tids" # [I */
/* .param p2, "packageName" # Ljava/lang/String; */
/* .param p3, "bounds" # Landroid/graphics/Rect; */
/* .line 347 */
/* const/16 v0, 0x5001 */
(( com.miui.server.multisence.MultiSenceService ) p0 ).notifyMultiTaskActionEnd ( p1, p2, p3, v0 ); // invoke-virtual {p0, p1, p2, p3, v0}, Lcom/miui/server/multisence/MultiSenceService;->notifyMultiTaskActionEnd([ILjava/lang/String;Landroid/graphics/Rect;I)V
/* .line 349 */
return;
} // .end method
public void floatingWindowMoveStart ( Integer[] p0, java.lang.String p1 ) {
/* .locals 6 */
/* .param p1, "req_tids" # [I */
/* .param p2, "packageName" # Ljava/lang/String; */
/* .line 340 */
android.os.Binder .clearCallingIdentity ( );
/* move-result-wide v0 */
/* .line 341 */
/* .local v0, "origId":J */
v2 = this.multiTaskActionManager;
/* new-instance v3, Lmiui/smartpower/MultiTaskActionManager$ActionInfo; */
/* .line 342 */
v4 = android.os.Process .myUid ( );
/* const/16 v5, 0x5001 */
/* invoke-direct {v3, v5, v4, p1}, Lmiui/smartpower/MultiTaskActionManager$ActionInfo;-><init>(II[I)V */
/* .line 341 */
(( miui.smartpower.MultiTaskActionManager ) v2 ).notifyMultiTaskActionStart ( v3 ); // invoke-virtual {v2, v3}, Lmiui/smartpower/MultiTaskActionManager;->notifyMultiTaskActionStart(Lmiui/smartpower/MultiTaskActionManager$ActionInfo;)V
/* .line 343 */
android.os.Binder .restoreCallingIdentity ( v0,v1 );
/* .line 344 */
return;
} // .end method
public void floatingWindowShow ( Integer[] p0, java.lang.String p1, android.graphics.Rect p2 ) {
/* .locals 1 */
/* .param p1, "req_tids" # [I */
/* .param p2, "packageName" # Ljava/lang/String; */
/* .param p3, "bounds" # Landroid/graphics/Rect; */
/* .line 330 */
/* const/16 v0, 0x5002 */
(( com.miui.server.multisence.MultiSenceService ) p0 ).notifyMultiTaskActionEnd ( p1, p2, p3, v0 ); // invoke-virtual {p0, p1, p2, p3, v0}, Lcom/miui/server/multisence/MultiSenceService;->notifyMultiTaskActionEnd([ILjava/lang/String;Landroid/graphics/Rect;I)V
/* .line 332 */
return;
} // .end method
public com.miui.server.multisence.MultiSenceServicePolicy getPolicy ( ) {
/* .locals 1 */
/* .line 439 */
v0 = this.mPolicy;
} // .end method
protected void notifyMultiSenceEnable ( Boolean p0 ) {
/* .locals 1 */
/* .param p1, "isEnable" # Z */
/* .line 443 */
v0 = this.mSmartPowerService;
/* .line 444 */
return;
} // .end method
public void notifyMultiTaskActionEnd ( Integer[] p0, java.lang.String p1, android.graphics.Rect p2, Integer p3 ) {
/* .locals 4 */
/* .param p1, "req_tids" # [I */
/* .param p2, "packageName" # Ljava/lang/String; */
/* .param p3, "bounds" # Landroid/graphics/Rect; */
/* .param p4, "senceId" # I */
/* .line 353 */
android.os.Binder .clearCallingIdentity ( );
/* move-result-wide v0 */
/* .line 354 */
/* .local v0, "origId":J */
/* new-instance v2, Lmiui/smartpower/MultiTaskActionManager$ActionInfo; */
v3 = android.os.Process .myUid ( );
/* invoke-direct {v2, p4, v3, p1}, Lmiui/smartpower/MultiTaskActionManager$ActionInfo;-><init>(II[I)V */
/* .line 355 */
/* .local v2, "floatingWindowInfo":Lmiui/smartpower/MultiTaskActionManager$ActionInfo; */
v3 = this.multiTaskActionManager;
(( miui.smartpower.MultiTaskActionManager ) v3 ).notifyMultiTaskActionEnd ( v2 ); // invoke-virtual {v3, v2}, Lmiui/smartpower/MultiTaskActionManager;->notifyMultiTaskActionEnd(Lmiui/smartpower/MultiTaskActionManager$ActionInfo;)V
/* .line 356 */
android.os.Binder .restoreCallingIdentity ( v0,v1 );
/* .line 357 */
return;
} // .end method
public void onBootPhase ( Integer p0 ) {
/* .locals 3 */
/* .param p1, "phase" # I */
/* .line 125 */
/* const/16 v0, 0x1f4 */
/* if-ne p1, v0, :cond_0 */
/* .line 126 */
final String v0 = "System ready: 500"; // const-string v0, "System ready: 500"
/* invoke-direct {p0, v0}, Lcom/miui/server/multisence/MultiSenceService;->LOG_IF_DEBUG(Ljava/lang/String;)V */
/* .line 127 */
com.miui.server.multisence.MultiSenceConfig .getInstance ( );
(( com.miui.server.multisence.MultiSenceConfig ) v0 ).updateSubFuncStatus ( ); // invoke-virtual {v0}, Lcom/miui/server/multisence/MultiSenceConfig;->updateSubFuncStatus()V
/* .line 128 */
v0 = this.mMultiSenceListener;
(( com.android.server.wm.MultiSenceListener ) v0 ).systemReady ( ); // invoke-virtual {v0}, Lcom/android/server/wm/MultiSenceListener;->systemReady()Z
/* .line 129 */
com.android.server.wm.MultiSenceManagerInternalStub .getInstance ( );
/* .line 130 */
com.android.server.wm.MultiSenceManagerInternalStub .getInstance ( );
v1 = this.mContext;
v2 = this.mHandler;
/* .line 131 */
} // :cond_0
/* const/16 v0, 0x3e8 */
/* if-ne p1, v0, :cond_1 */
/* .line 132 */
com.android.server.wm.MultiSenceManagerInternalStub .getInstance ( );
/* .line 133 */
com.miui.server.multisence.MultiSenceConfig .getInstance ( );
(( com.miui.server.multisence.MultiSenceConfig ) v0 ).initVRSWhiteList ( ); // invoke-virtual {v0}, Lcom/miui/server/multisence/MultiSenceConfig;->initVRSWhiteList()V
/* .line 134 */
com.miui.server.multisence.MultiSenceConfig .getInstance ( );
(( com.miui.server.multisence.MultiSenceConfig ) v0 ).initFWWhiteList ( ); // invoke-virtual {v0}, Lcom/miui/server/multisence/MultiSenceConfig;->initFWWhiteList()V
/* .line 136 */
} // :cond_1
} // :goto_0
return;
} // .end method
public void onStart ( ) {
/* .locals 2 */
/* .line 120 */
/* new-instance v0, Lcom/miui/server/multisence/MultiSenceService$BinderService; */
int v1 = 0; // const/4 v1, 0x0
/* invoke-direct {v0, p0, v1}, Lcom/miui/server/multisence/MultiSenceService$BinderService;-><init>(Lcom/miui/server/multisence/MultiSenceService;Lcom/miui/server/multisence/MultiSenceService$BinderService-IA;)V */
this.mBinderService = v0;
/* .line 121 */
final String v1 = "miui_multi_sence"; // const-string v1, "miui_multi_sence"
(( com.miui.server.multisence.MultiSenceService ) p0 ).publishBinderService ( v1, v0 ); // invoke-virtual {p0, v1, v0}, Lcom/miui/server/multisence/MultiSenceService;->publishBinderService(Ljava/lang/String;Landroid/os/IBinder;)V
/* .line 122 */
return;
} // .end method
protected void registerMultiTaskActionListener ( ) {
/* .locals 4 */
/* .line 153 */
/* sget-boolean v0, Lcom/miui/server/multisence/MultiSenceDynamicController;->IS_CLOUD_ENABLED:Z */
final String v1 = "MultiSenceService"; // const-string v1, "MultiSenceService"
/* if-nez v0, :cond_0 */
/* .line 154 */
final String v0 = "need not to register multisence listener due to cloud controller."; // const-string v0, "need not to register multisence listener due to cloud controller."
android.util.Slog .i ( v1,v0 );
/* .line 155 */
return;
/* .line 158 */
} // :cond_0
/* sget-boolean v0, Lcom/miui/server/multisence/MultiSenceService;->mMultiTaskActionListenerRegistered:Z */
if ( v0 != null) { // if-eqz v0, :cond_1
/* .line 159 */
final String v0 = "Listener has been registered.Do not register again."; // const-string v0, "Listener has been registered.Do not register again."
android.util.Slog .i ( v1,v0 );
/* .line 160 */
return;
/* .line 163 */
} // :cond_1
v0 = this.mSmartPowerService;
v1 = this.mHandler;
v2 = this.mMultiSenceListener;
v0 = int v3 = 2; // const/4 v3, 0x2
/* .line 168 */
/* .local v0, "isRegisterSuccess":Z */
if ( v0 != null) { // if-eqz v0, :cond_2
/* .line 169 */
int v1 = 1; // const/4 v1, 0x1
com.miui.server.multisence.MultiSenceService.mMultiTaskActionListenerRegistered = (v1!= 0);
/* .line 171 */
} // :cond_2
return;
} // .end method
public void setUpdateReason ( java.lang.String p0 ) {
/* .locals 0 */
/* .param p1, "reason" # Ljava/lang/String; */
/* .line 326 */
this.mReason = p1;
/* .line 327 */
return;
} // .end method
public void showAllWindowInfo ( java.lang.String p0 ) {
/* .locals 6 */
/* .param p1, "reason" # Ljava/lang/String; */
/* .line 447 */
/* sget-boolean v0, Lcom/miui/server/multisence/MultiSenceService;->DEBUG:Z */
/* if-nez v0, :cond_0 */
/* .line 448 */
return;
/* .line 450 */
} // :cond_0
int v0 = 0; // const/4 v0, 0x0
/* .line 452 */
/* .local v0, "count":I */
/* new-instance v1, Ljava/lang/StringBuilder; */
/* invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V */
final String v2 = "reason: "; // const-string v2, "reason: "
(( java.lang.StringBuilder ) v1 ).append ( v2 ); // invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v1 ).append ( p1 ); // invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v1 ).toString ( ); // invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
/* invoke-direct {p0, v1}, Lcom/miui/server/multisence/MultiSenceService;->LOG_IF_DEBUG(Ljava/lang/String;)V */
/* .line 454 */
v1 = this.mWindows;
v2 = } // :goto_0
if ( v2 != null) { // if-eqz v2, :cond_1
/* check-cast v2, Ljava/lang/String; */
/* .line 455 */
/* .local v2, "key":Ljava/lang/String; */
/* add-int/lit8 v0, v0, 0x1 */
/* .line 456 */
/* new-instance v3, Ljava/lang/StringBuilder; */
/* invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V */
final String v4 = "["; // const-string v4, "["
(( java.lang.StringBuilder ) v3 ).append ( v4 ); // invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v3 ).append ( v0 ); // invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;
final String v4 = "]: "; // const-string v4, "]: "
(( java.lang.StringBuilder ) v3 ).append ( v4 ); // invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v3 ).append ( v2 ); // invoke-virtual {v3, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
final String v4 = "-"; // const-string v4, "-"
(( java.lang.StringBuilder ) v3 ).append ( v4 ); // invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
v5 = this.mWindows;
/* .line 457 */
/* check-cast v5, Lcom/miui/server/multisence/SingleWindowInfo; */
(( com.miui.server.multisence.SingleWindowInfo ) v5 ).getWindowingModeString ( ); // invoke-virtual {v5}, Lcom/miui/server/multisence/SingleWindowInfo;->getWindowingModeString()Ljava/lang/String;
(( java.lang.StringBuilder ) v3 ).append ( v5 ); // invoke-virtual {v3, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v3 ).append ( v4 ); // invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
v5 = this.mWindows;
/* .line 458 */
/* check-cast v5, Lcom/miui/server/multisence/SingleWindowInfo; */
v5 = (( com.miui.server.multisence.SingleWindowInfo ) v5 ).isInputFocused ( ); // invoke-virtual {v5}, Lcom/miui/server/multisence/SingleWindowInfo;->isInputFocused()Z
(( java.lang.StringBuilder ) v3 ).append ( v5 ); // invoke-virtual {v3, v5}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v3 ).append ( v4 ); // invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
v4 = this.mWindows;
/* .line 459 */
/* check-cast v4, Lcom/miui/server/multisence/SingleWindowInfo; */
v4 = (( com.miui.server.multisence.SingleWindowInfo ) v4 ).isVisible ( ); // invoke-virtual {v4}, Lcom/miui/server/multisence/SingleWindowInfo;->isVisible()Z
(( java.lang.StringBuilder ) v3 ).append ( v4 ); // invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v3 ).toString ( ); // invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
/* .line 456 */
/* invoke-direct {p0, v3}, Lcom/miui/server/multisence/MultiSenceService;->LOG_IF_DEBUG(Ljava/lang/String;)V */
/* .line 460 */
} // .end local v2 # "key":Ljava/lang/String;
/* .line 462 */
} // :cond_1
/* new-instance v1, Ljava/lang/StringBuilder; */
/* invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V */
final String v2 = "There are ["; // const-string v2, "There are ["
(( java.lang.StringBuilder ) v1 ).append ( v2 ); // invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v1 ).append ( v0 ); // invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;
final String v2 = "] windows in Screen"; // const-string v2, "] windows in Screen"
(( java.lang.StringBuilder ) v1 ).append ( v2 ); // invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v1 ).toString ( ); // invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
/* invoke-direct {p0, v1}, Lcom/miui/server/multisence/MultiSenceService;->LOG_IF_DEBUG(Ljava/lang/String;)V */
/* .line 463 */
return;
} // .end method
protected void unregisterMultiTaskActionListener ( ) {
/* .locals 3 */
/* .line 174 */
/* sget-boolean v0, Lcom/miui/server/multisence/MultiSenceService;->mMultiTaskActionListenerRegistered:Z */
final String v1 = "MultiSenceService"; // const-string v1, "MultiSenceService"
/* if-nez v0, :cond_0 */
/* .line 175 */
final String v0 = "Listener has not been registered.Do need to unregister."; // const-string v0, "Listener has not been registered.Do need to unregister."
android.util.Slog .i ( v1,v0 );
/* .line 176 */
return;
/* .line 178 */
} // :cond_0
/* sget-boolean v0, Lcom/miui/server/multisence/MultiSenceDynamicController;->IS_CLOUD_ENABLED:Z */
if ( v0 != null) { // if-eqz v0, :cond_1
/* .line 179 */
final String v0 = "need not to unregister multisence listener due to cloud controller."; // const-string v0, "need not to unregister multisence listener due to cloud controller."
android.util.Slog .i ( v1,v0 );
/* .line 180 */
return;
/* .line 183 */
} // :cond_1
v0 = this.mSmartPowerService;
int v1 = 2; // const/4 v1, 0x2
v0 = v2 = this.mMultiSenceListener;
/* .line 187 */
/* .local v0, "isUnRegisterSuccess":Z */
if ( v0 != null) { // if-eqz v0, :cond_2
/* .line 188 */
int v1 = 0; // const/4 v1, 0x0
com.miui.server.multisence.MultiSenceService.mMultiTaskActionListenerRegistered = (v1!= 0);
/* .line 190 */
} // :cond_2
return;
} // .end method
public void updateStaticWindowsInfo ( ) {
/* .locals 5 */
/* .line 372 */
v0 = this.mNewWindows;
v1 = } // :goto_0
if ( v1 != null) { // if-eqz v1, :cond_2
/* check-cast v1, Ljava/lang/String; */
/* .line 373 */
/* .local v1, "key":Ljava/lang/String; */
v2 = this.mNewWindows;
/* check-cast v2, Lcom/miui/server/multisence/SingleWindowInfo; */
/* .line 374 */
/* .local v2, "window":Lcom/miui/server/multisence/SingleWindowInfo; */
/* if-nez v2, :cond_0 */
/* .line 375 */
/* new-instance v3, Ljava/lang/StringBuilder; */
/* invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V */
/* const-string/jumbo v4, "window {" */
(( java.lang.StringBuilder ) v3 ).append ( v4 ); // invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v3 ).append ( v1 ); // invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
/* const-string/jumbo v4, "} in null" */
(( java.lang.StringBuilder ) v3 ).append ( v4 ); // invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v3 ).toString ( ); // invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
final String v4 = "MultiSenceService"; // const-string v4, "MultiSenceService"
android.util.Slog .w ( v4,v3 );
/* .line 376 */
/* .line 378 */
} // :cond_0
v3 = (( com.miui.server.multisence.SingleWindowInfo ) v2 ).isInputFocused ( ); // invoke-virtual {v2}, Lcom/miui/server/multisence/SingleWindowInfo;->isInputFocused()Z
if ( v3 != null) { // if-eqz v3, :cond_1
/* .line 379 */
(( com.miui.server.multisence.SingleWindowInfo ) v2 ).getPackageName ( ); // invoke-virtual {v2}, Lcom/miui/server/multisence/SingleWindowInfo;->getPackageName()Ljava/lang/String;
/* .line 380 */
/* .local v0, "newInputFocus":Ljava/lang/String; */
v3 = this.mPolicy;
(( com.miui.server.multisence.MultiSenceServicePolicy ) v3 ).updateInputFocus ( v0 ); // invoke-virtual {v3, v0}, Lcom/miui/server/multisence/MultiSenceServicePolicy;->updateInputFocus(Ljava/lang/String;)V
/* .line 381 */
/* .line 383 */
} // .end local v0 # "newInputFocus":Ljava/lang/String;
} // .end local v1 # "key":Ljava/lang/String;
} // .end local v2 # "window":Lcom/miui/server/multisence/SingleWindowInfo;
} // :cond_1
/* .line 385 */
} // :cond_2
} // :goto_1
/* sget-boolean v0, Lcom/miui/server/multisence/MultiSenceDynamicController;->IS_CLOUD_ENABLED:Z */
/* if-nez v0, :cond_3 */
/* .line 386 */
final String v0 = "Cloud Controller refuses."; // const-string v0, "Cloud Controller refuses."
/* invoke-direct {p0, v0}, Lcom/miui/server/multisence/MultiSenceService;->LOG_IF_DEBUG(Ljava/lang/String;)V */
/* .line 387 */
return;
/* .line 390 */
} // :cond_3
v0 = this.mNewWindows;
v0 = /* invoke-direct {p0, v0}, Lcom/miui/server/multisence/MultiSenceService;->isScreenStateChanged(Ljava/util/Map;)Z */
/* if-nez v0, :cond_4 */
/* .line 391 */
final String v0 = "not changed"; // const-string v0, "not changed"
(( com.miui.server.multisence.MultiSenceService ) p0 ).showAllWindowInfo ( v0 ); // invoke-virtual {p0, v0}, Lcom/miui/server/multisence/MultiSenceService;->showAllWindowInfo(Ljava/lang/String;)V
/* .line 392 */
return;
/* .line 396 */
} // :cond_4
v0 = this.mPolicy;
v1 = this.mNewWindows;
(( com.miui.server.multisence.MultiSenceServicePolicy ) v0 ).windowInfoPerPorcessing ( v1 ); // invoke-virtual {v0, v1}, Lcom/miui/server/multisence/MultiSenceServicePolicy;->windowInfoPerPorcessing(Ljava/util/Map;)V
/* .line 399 */
v0 = this.mPolicy;
v1 = this.mWindows;
v2 = this.mNewWindows;
(( com.miui.server.multisence.MultiSenceServicePolicy ) v0 ).updateSchedPolicy ( v1, v2 ); // invoke-virtual {v0, v1, v2}, Lcom/miui/server/multisence/MultiSenceServicePolicy;->updateSchedPolicy(Ljava/util/Map;Ljava/util/Map;)V
/* .line 402 */
v0 = this.mPolicy;
(( com.miui.server.multisence.MultiSenceServicePolicy ) v0 ).doSched ( ); // invoke-virtual {v0}, Lcom/miui/server/multisence/MultiSenceServicePolicy;->doSched()V
/* .line 405 */
v0 = this.mNewWindows;
this.mWindows = v0;
/* .line 406 */
v0 = this.mReason;
(( com.miui.server.multisence.MultiSenceService ) p0 ).showAllWindowInfo ( v0 ); // invoke-virtual {p0, v0}, Lcom/miui/server/multisence/MultiSenceService;->showAllWindowInfo(Ljava/lang/String;)V
/* .line 407 */
return;
} // .end method
public void updateStaticWindowsInfo ( java.util.Map p0 ) {
/* .locals 4 */
/* .annotation system Ldalvik/annotation/Signature; */
/* value = { */
/* "(", */
/* "Ljava/util/Map<", */
/* "Ljava/lang/String;", */
/* "Lcom/miui/server/multisence/SingleWindowInfo;", */
/* ">;)V" */
/* } */
} // .end annotation
/* .line 366 */
/* .local p1, "inWindows":Ljava/util/Map;, "Ljava/util/Map<Ljava/lang/String;Lcom/miui/server/multisence/SingleWindowInfo;>;" */
v0 = this.mHandler;
v1 = this.mUpdateStaticWindows;
(( android.os.Handler ) v0 ).removeCallbacks ( v1 ); // invoke-virtual {v0, v1}, Landroid/os/Handler;->removeCallbacks(Ljava/lang/Runnable;)V
/* .line 367 */
this.mNewWindows = p1;
/* .line 368 */
v0 = this.mHandler;
v1 = this.mUpdateStaticWindows;
/* const-wide/16 v2, 0x32 */
(( android.os.Handler ) v0 ).postDelayed ( v1, v2, v3 ); // invoke-virtual {v0, v1, v2, v3}, Landroid/os/Handler;->postDelayed(Ljava/lang/Runnable;J)Z
/* .line 369 */
return;
} // .end method
