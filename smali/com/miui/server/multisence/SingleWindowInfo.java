public class com.miui.server.multisence.SingleWindowInfo implements java.lang.Comparable {
	 /* .source "SingleWindowInfo.java" */
	 /* # interfaces */
	 /* # annotations */
	 /* .annotation system Ldalvik/annotation/MemberClasses; */
	 /* value = { */
	 /* Lcom/miui/server/multisence/SingleWindowInfo$WindowForm;, */
	 /* Lcom/miui/server/multisence/SingleWindowInfo$AppType;, */
	 /* Lcom/miui/server/multisence/SingleWindowInfo$WindowChangeState;, */
	 /* Lcom/miui/server/multisence/SingleWindowInfo$ScheduleStatus;, */
	 /* Lcom/miui/server/multisence/SingleWindowInfo$FreeFormInfo; */
	 /* } */
} // .end annotation
/* .annotation system Ldalvik/annotation/Signature; */
/* value = { */
/* "Ljava/lang/Object;", */
/* "Ljava/lang/Comparable<", */
/* "Lcom/miui/server/multisence/SingleWindowInfo;", */
/* ">;" */
/* } */
} // .end annotation
/* # static fields */
private static final java.lang.String TAG;
/* # instance fields */
private Integer FPS_LIMIT_DEFAULT;
private Integer FPS_LIMIT_IN_FREEFORM_MINI;
private Integer FPS_LIMIT_IN_FULLSCREEN_COMMON;
private Boolean isCovered;
private Boolean isFPSLimited;
private Boolean isHighPriority;
private Boolean isInputFocused;
private Boolean isVisible;
private Boolean isVrsCovered;
private Integer layerOrder;
private com.miui.server.multisence.SingleWindowInfo$WindowChangeState mChangeStatus;
private com.miui.server.multisence.SingleWindowInfo$WindowForm mForm;
private java.lang.String mPackageName;
private android.graphics.Rect mRect;
private java.util.Set mScheduleStatus;
/* .annotation system Ldalvik/annotation/Signature; */
/* value = { */
/* "Ljava/util/Set<", */
/* "Lcom/miui/server/multisence/SingleWindowInfo$ScheduleStatus;", */
/* ">;" */
/* } */
} // .end annotation
} // .end field
private com.miui.server.multisence.SingleWindowInfo$AppType mType;
private java.lang.String mWindowMode;
private Integer myPid;
private Integer myRenderThreadTid;
private Integer myUid;
private Integer windowCount;
/* # direct methods */
public com.miui.server.multisence.SingleWindowInfo ( ) {
/* .locals 3 */
/* .line 82 */
/* invoke-direct {p0}, Ljava/lang/Object;-><init>()V */
/* .line 25 */
v0 = com.miui.server.multisence.SingleWindowInfo$WindowForm.UNKNOWN;
this.mForm = v0;
/* .line 26 */
v0 = com.miui.server.multisence.SingleWindowInfo$AppType.UNKNOWN;
this.mType = v0;
/* .line 27 */
/* const-string/jumbo v0, "unknown" */
this.mWindowMode = v0;
/* .line 28 */
int v0 = 0; // const/4 v0, 0x0
/* iput-boolean v0, p0, Lcom/miui/server/multisence/SingleWindowInfo;->isInputFocused:Z */
/* .line 29 */
/* iput-boolean v0, p0, Lcom/miui/server/multisence/SingleWindowInfo;->isHighPriority:Z */
/* .line 30 */
/* iput-boolean v0, p0, Lcom/miui/server/multisence/SingleWindowInfo;->isVisible:Z */
/* .line 31 */
/* iput-boolean v0, p0, Lcom/miui/server/multisence/SingleWindowInfo;->isCovered:Z */
/* .line 32 */
/* iput-boolean v0, p0, Lcom/miui/server/multisence/SingleWindowInfo;->isVrsCovered:Z */
/* .line 33 */
v1 = com.miui.server.multisence.SingleWindowInfo$WindowChangeState.UNKNOWN;
this.mChangeStatus = v1;
/* .line 34 */
/* new-instance v1, Ljava/util/HashSet; */
/* invoke-direct {v1}, Ljava/util/HashSet;-><init>()V */
this.mScheduleStatus = v1;
/* .line 36 */
/* const/16 v1, 0x3c */
/* iput v1, p0, Lcom/miui/server/multisence/SingleWindowInfo;->FPS_LIMIT_DEFAULT:I */
/* .line 37 */
/* const/16 v2, 0x1e */
/* iput v2, p0, Lcom/miui/server/multisence/SingleWindowInfo;->FPS_LIMIT_IN_FREEFORM_MINI:I */
/* .line 38 */
/* iput v1, p0, Lcom/miui/server/multisence/SingleWindowInfo;->FPS_LIMIT_IN_FULLSCREEN_COMMON:I */
/* .line 39 */
/* iput-boolean v0, p0, Lcom/miui/server/multisence/SingleWindowInfo;->isFPSLimited:Z */
/* .line 44 */
int v0 = -1; // const/4 v0, -0x1
/* iput v0, p0, Lcom/miui/server/multisence/SingleWindowInfo;->layerOrder:I */
/* .line 45 */
int v0 = 1; // const/4 v0, 0x1
/* iput v0, p0, Lcom/miui/server/multisence/SingleWindowInfo;->windowCount:I */
/* .line 83 */
/* sget-boolean v0, Lcom/miui/server/multisence/MultiSenceConfig;->DEBUG:Z */
final String v1 = "new a SingleWindowInfo object."; // const-string v1, "new a SingleWindowInfo object."
com.miui.server.multisence.MultiSenceServiceUtils .msLogD ( v0,v1 );
/* .line 84 */
return;
} // .end method
public com.miui.server.multisence.SingleWindowInfo ( ) {
/* .locals 3 */
/* .param p1, "name" # Ljava/lang/String; */
/* .param p2, "type" # Lcom/miui/server/multisence/SingleWindowInfo$AppType; */
/* .line 86 */
/* invoke-direct {p0}, Ljava/lang/Object;-><init>()V */
/* .line 25 */
v0 = com.miui.server.multisence.SingleWindowInfo$WindowForm.UNKNOWN;
this.mForm = v0;
/* .line 26 */
v0 = com.miui.server.multisence.SingleWindowInfo$AppType.UNKNOWN;
this.mType = v0;
/* .line 27 */
/* const-string/jumbo v0, "unknown" */
this.mWindowMode = v0;
/* .line 28 */
int v0 = 0; // const/4 v0, 0x0
/* iput-boolean v0, p0, Lcom/miui/server/multisence/SingleWindowInfo;->isInputFocused:Z */
/* .line 29 */
/* iput-boolean v0, p0, Lcom/miui/server/multisence/SingleWindowInfo;->isHighPriority:Z */
/* .line 30 */
/* iput-boolean v0, p0, Lcom/miui/server/multisence/SingleWindowInfo;->isVisible:Z */
/* .line 31 */
/* iput-boolean v0, p0, Lcom/miui/server/multisence/SingleWindowInfo;->isCovered:Z */
/* .line 32 */
/* iput-boolean v0, p0, Lcom/miui/server/multisence/SingleWindowInfo;->isVrsCovered:Z */
/* .line 33 */
v1 = com.miui.server.multisence.SingleWindowInfo$WindowChangeState.UNKNOWN;
this.mChangeStatus = v1;
/* .line 34 */
/* new-instance v1, Ljava/util/HashSet; */
/* invoke-direct {v1}, Ljava/util/HashSet;-><init>()V */
this.mScheduleStatus = v1;
/* .line 36 */
/* const/16 v1, 0x3c */
/* iput v1, p0, Lcom/miui/server/multisence/SingleWindowInfo;->FPS_LIMIT_DEFAULT:I */
/* .line 37 */
/* const/16 v2, 0x1e */
/* iput v2, p0, Lcom/miui/server/multisence/SingleWindowInfo;->FPS_LIMIT_IN_FREEFORM_MINI:I */
/* .line 38 */
/* iput v1, p0, Lcom/miui/server/multisence/SingleWindowInfo;->FPS_LIMIT_IN_FULLSCREEN_COMMON:I */
/* .line 39 */
/* iput-boolean v0, p0, Lcom/miui/server/multisence/SingleWindowInfo;->isFPSLimited:Z */
/* .line 44 */
int v0 = -1; // const/4 v0, -0x1
/* iput v0, p0, Lcom/miui/server/multisence/SingleWindowInfo;->layerOrder:I */
/* .line 45 */
int v0 = 1; // const/4 v0, 0x1
/* iput v0, p0, Lcom/miui/server/multisence/SingleWindowInfo;->windowCount:I */
/* .line 87 */
this.mPackageName = p1;
/* .line 88 */
final String v0 = "com.miui.home"; // const-string v0, "com.miui.home"
v0 = (( java.lang.String ) p1 ).equals ( v0 ); // invoke-virtual {p1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z
if ( v0 != null) { // if-eqz v0, :cond_0
/* .line 89 */
v0 = com.miui.server.multisence.SingleWindowInfo$AppType.HOME;
this.mType = v0;
/* .line 91 */
} // :cond_0
this.mType = p2;
/* .line 93 */
} // :goto_0
return;
} // .end method
/* # virtual methods */
public Boolean checkSchedStatus ( com.miui.server.multisence.SingleWindowInfo$ScheduleStatus p0 ) {
/* .locals 1 */
/* .param p1, "status" # Lcom/miui/server/multisence/SingleWindowInfo$ScheduleStatus; */
/* .line 250 */
v0 = v0 = this.mScheduleStatus;
if ( v0 != null) { // if-eqz v0, :cond_0
/* .line 251 */
int v0 = 1; // const/4 v0, 0x1
/* .line 253 */
} // :cond_0
int v0 = 0; // const/4 v0, 0x0
} // .end method
public Integer compareTo ( com.miui.server.multisence.SingleWindowInfo p0 ) {
/* .locals 2 */
/* .param p1, "window" # Lcom/miui/server/multisence/SingleWindowInfo; */
/* .line 181 */
v0 = (( com.miui.server.multisence.SingleWindowInfo ) p1 ).getLayerOrder ( ); // invoke-virtual {p1}, Lcom/miui/server/multisence/SingleWindowInfo;->getLayerOrder()I
v1 = (( com.miui.server.multisence.SingleWindowInfo ) p0 ).getLayerOrder ( ); // invoke-virtual {p0}, Lcom/miui/server/multisence/SingleWindowInfo;->getLayerOrder()I
/* sub-int/2addr v0, v1 */
} // .end method
public Integer compareTo ( java.lang.Object p0 ) { //bridge//synthethic
/* .locals 0 */
/* .line 21 */
/* check-cast p1, Lcom/miui/server/multisence/SingleWindowInfo; */
p1 = (( com.miui.server.multisence.SingleWindowInfo ) p0 ).compareTo ( p1 ); // invoke-virtual {p0, p1}, Lcom/miui/server/multisence/SingleWindowInfo;->compareTo(Lcom/miui/server/multisence/SingleWindowInfo;)I
} // .end method
public com.miui.server.multisence.SingleWindowInfo doSched ( ) {
/* .locals 7 */
/* .line 257 */
/* sget-boolean v0, Lcom/miui/server/multisence/MultiSenceConfig;->DEBUG:Z */
(( com.miui.server.multisence.SingleWindowInfo ) p0 ).toString ( ); // invoke-virtual {p0}, Lcom/miui/server/multisence/SingleWindowInfo;->toString()Ljava/lang/String;
com.miui.server.multisence.MultiSenceServiceUtils .msLogD ( v0,v1 );
/* .line 258 */
/* iget-boolean v0, p0, Lcom/miui/server/multisence/SingleWindowInfo;->isHighPriority:Z */
if ( v0 != null) { // if-eqz v0, :cond_1
/* .line 259 */
/* new-instance v0, Ljava/lang/StringBuilder; */
/* invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V */
final String v1 = "doSched app: "; // const-string v1, "doSched app: "
(( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
v1 = this.mPackageName;
(( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
final String v1 = " pid: "; // const-string v1, " pid: "
(( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
/* iget v1, p0, Lcom/miui/server/multisence/SingleWindowInfo;->myPid:I */
(( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;
final String v1 = " high priority: "; // const-string v1, " high priority: "
(( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
/* iget-boolean v1, p0, Lcom/miui/server/multisence/SingleWindowInfo;->isHighPriority:Z */
(( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v0 ).toString ( ); // invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
/* const-wide/16 v1, 0x20 */
android.os.Trace .traceBegin ( v1,v2,v0 );
/* .line 260 */
v0 = android.os.Process .myUid ( );
/* .line 261 */
/* .local v0, "callingUid":I */
/* iget v3, p0, Lcom/miui/server/multisence/SingleWindowInfo;->myPid:I */
/* if-gtz v3, :cond_0 */
/* .line 262 */
/* sget-boolean v1, Lcom/miui/server/multisence/MultiSenceConfig;->DEBUG:Z */
final String v2 = "Pid recorded error"; // const-string v2, "Pid recorded error"
com.miui.server.multisence.MultiSenceServiceUtils .msLogD ( v1,v2 );
/* .line 263 */
/* .line 265 */
} // :cond_0
com.android.server.am.MiuiBoosterUtilsStub .getInstance ( );
/* iget v4, p0, Lcom/miui/server/multisence/SingleWindowInfo;->myPid:I */
/* filled-new-array {v4}, [I */
int v5 = 3; // const/4 v5, 0x3
/* const/16 v6, 0x7d0 */
/* .line 267 */
android.os.Trace .traceEnd ( v1,v2 );
/* .line 269 */
} // .end local v0 # "callingUid":I
} // :cond_1
} // .end method
public com.miui.server.multisence.SingleWindowInfo$AppType getAppType ( ) {
/* .locals 1 */
/* .line 114 */
v0 = this.mType;
} // .end method
public com.miui.server.multisence.SingleWindowInfo$WindowChangeState getChangeStatus ( ) {
/* .locals 1 */
/* .line 278 */
v0 = this.mChangeStatus;
} // .end method
public Integer getLayerOrder ( ) {
/* .locals 1 */
/* .line 158 */
/* iget v0, p0, Lcom/miui/server/multisence/SingleWindowInfo;->layerOrder:I */
} // .end method
public Integer getLimitFPS ( ) {
/* .locals 1 */
/* .line 149 */
/* iget v0, p0, Lcom/miui/server/multisence/SingleWindowInfo;->FPS_LIMIT_IN_FREEFORM_MINI:I */
} // .end method
public java.lang.String getPackageName ( ) {
/* .locals 1 */
/* .line 190 */
v0 = this.mPackageName;
} // .end method
public android.graphics.Rect getRectValue ( ) {
/* .locals 1 */
/* .line 171 */
v0 = this.mRect;
} // .end method
public Integer getWindowCount ( ) {
/* .locals 1 */
/* .line 167 */
/* iget v0, p0, Lcom/miui/server/multisence/SingleWindowInfo;->windowCount:I */
} // .end method
public com.miui.server.multisence.SingleWindowInfo$WindowForm getWindowForm ( ) {
/* .locals 1 */
/* .line 118 */
v0 = this.mForm;
} // .end method
public java.lang.String getWindowingModeString ( ) {
/* .locals 1 */
/* .line 141 */
v0 = this.mWindowMode;
} // .end method
public Boolean isCovered ( ) {
/* .locals 1 */
/* .line 106 */
/* iget-boolean v0, p0, Lcom/miui/server/multisence/SingleWindowInfo;->isCovered:Z */
} // .end method
public Boolean isFPSLimited ( ) {
/* .locals 1 */
/* .line 137 */
/* iget-boolean v0, p0, Lcom/miui/server/multisence/SingleWindowInfo;->isFPSLimited:Z */
} // .end method
public Boolean isHighPriority ( ) {
/* .locals 1 */
/* .line 203 */
/* iget-boolean v0, p0, Lcom/miui/server/multisence/SingleWindowInfo;->isHighPriority:Z */
} // .end method
public Boolean isInFreeform ( ) {
/* .locals 2 */
/* .line 145 */
v0 = this.mWindowMode;
final String v1 = "freeform"; // const-string v1, "freeform"
v0 = (( java.lang.String ) v0 ).equals ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z
} // .end method
public Boolean isInputFocused ( ) {
/* .locals 1 */
/* .line 199 */
/* iget-boolean v0, p0, Lcom/miui/server/multisence/SingleWindowInfo;->isInputFocused:Z */
} // .end method
public Boolean isVisible ( ) {
/* .locals 1 */
/* .line 212 */
/* iget-boolean v0, p0, Lcom/miui/server/multisence/SingleWindowInfo;->isVisible:Z */
} // .end method
public Boolean isVrsCovered ( ) {
/* .locals 1 */
/* .line 110 */
/* iget-boolean v0, p0, Lcom/miui/server/multisence/SingleWindowInfo;->isVrsCovered:Z */
} // .end method
public com.miui.server.multisence.SingleWindowInfo setChangeStatus ( com.miui.server.multisence.SingleWindowInfo$WindowChangeState p0 ) {
/* .locals 0 */
/* .param p1, "status" # Lcom/miui/server/multisence/SingleWindowInfo$WindowChangeState; */
/* .line 273 */
this.mChangeStatus = p1;
/* .line 274 */
} // .end method
public com.miui.server.multisence.SingleWindowInfo setCovered ( Boolean p0 ) {
/* .locals 0 */
/* .param p1, "covered" # Z */
/* .line 96 */
/* iput-boolean p1, p0, Lcom/miui/server/multisence/SingleWindowInfo;->isCovered:Z */
/* .line 97 */
} // .end method
public com.miui.server.multisence.SingleWindowInfo setFPSLimited ( Boolean p0 ) {
/* .locals 0 */
/* .param p1, "isLimited" # Z */
/* .line 127 */
/* iput-boolean p1, p0, Lcom/miui/server/multisence/SingleWindowInfo;->isFPSLimited:Z */
/* .line 128 */
} // .end method
public com.miui.server.multisence.SingleWindowInfo setFocused ( Boolean p0 ) {
/* .locals 0 */
/* .param p1, "focused" # Z */
/* .line 194 */
/* iput-boolean p1, p0, Lcom/miui/server/multisence/SingleWindowInfo;->isInputFocused:Z */
/* .line 195 */
} // .end method
public com.miui.server.multisence.SingleWindowInfo setLayerOrder ( Integer p0 ) {
/* .locals 0 */
/* .param p1, "order" # I */
/* .line 153 */
/* iput p1, p0, Lcom/miui/server/multisence/SingleWindowInfo;->layerOrder:I */
/* .line 154 */
} // .end method
public com.miui.server.multisence.SingleWindowInfo setPid ( Integer p0 ) {
/* .locals 0 */
/* .param p1, "pid" # I */
/* .line 49 */
/* iput p1, p0, Lcom/miui/server/multisence/SingleWindowInfo;->myPid:I */
/* .line 50 */
} // .end method
public com.miui.server.multisence.SingleWindowInfo setRectValue ( android.graphics.Rect p0 ) {
/* .locals 0 */
/* .param p1, "rect" # Landroid/graphics/Rect; */
/* .line 175 */
this.mRect = p1;
/* .line 176 */
} // .end method
public com.miui.server.multisence.SingleWindowInfo setRenderThreadTid ( Integer p0 ) {
/* .locals 0 */
/* .param p1, "tid" # I */
/* .line 59 */
/* iput p1, p0, Lcom/miui/server/multisence/SingleWindowInfo;->myRenderThreadTid:I */
/* .line 60 */
} // .end method
public com.miui.server.multisence.SingleWindowInfo setSchedPriority ( Boolean p0 ) {
/* .locals 0 */
/* .param p1, "priority" # Z */
/* .line 207 */
/* iput-boolean p1, p0, Lcom/miui/server/multisence/SingleWindowInfo;->isHighPriority:Z */
/* .line 208 */
} // .end method
public com.miui.server.multisence.SingleWindowInfo setSchedStatus ( com.miui.server.multisence.SingleWindowInfo$ScheduleStatus p0 ) {
/* .locals 1 */
/* .param p1, "status" # Lcom/miui/server/multisence/SingleWindowInfo$ScheduleStatus; */
/* .line 242 */
v0 = v0 = this.mScheduleStatus;
if ( v0 != null) { // if-eqz v0, :cond_0
/* .line 243 */
/* .line 245 */
} // :cond_0
v0 = this.mScheduleStatus;
/* .line 246 */
} // .end method
public com.miui.server.multisence.SingleWindowInfo setUid ( Integer p0 ) {
/* .locals 0 */
/* .param p1, "uid" # I */
/* .line 54 */
/* iput p1, p0, Lcom/miui/server/multisence/SingleWindowInfo;->myUid:I */
/* .line 55 */
} // .end method
public com.miui.server.multisence.SingleWindowInfo setVisiable ( Boolean p0 ) {
/* .locals 0 */
/* .param p1, "visible" # Z */
/* .line 132 */
/* iput-boolean p1, p0, Lcom/miui/server/multisence/SingleWindowInfo;->isVisible:Z */
/* .line 133 */
} // .end method
public com.miui.server.multisence.SingleWindowInfo setVrsCovered ( Boolean p0 ) {
/* .locals 0 */
/* .param p1, "covered" # Z */
/* .line 101 */
/* iput-boolean p1, p0, Lcom/miui/server/multisence/SingleWindowInfo;->isVrsCovered:Z */
/* .line 102 */
} // .end method
public com.miui.server.multisence.SingleWindowInfo setWindowCount ( Integer p0 ) {
/* .locals 0 */
/* .param p1, "count" # I */
/* .line 162 */
/* iput p1, p0, Lcom/miui/server/multisence/SingleWindowInfo;->windowCount:I */
/* .line 163 */
} // .end method
public com.miui.server.multisence.SingleWindowInfo setWindowingModeString ( java.lang.String p0 ) {
/* .locals 0 */
/* .param p1, "mode" # Ljava/lang/String; */
/* .line 185 */
this.mWindowMode = p1;
/* .line 186 */
} // .end method
public com.miui.server.multisence.SingleWindowInfo setgetWindowForm ( com.miui.server.multisence.SingleWindowInfo$WindowForm p0 ) {
/* .locals 0 */
/* .param p1, "form" # Lcom/miui/server/multisence/SingleWindowInfo$WindowForm; */
/* .line 122 */
this.mForm = p1;
/* .line 123 */
} // .end method
public java.lang.String toString ( ) {
/* .locals 5 */
/* .line 282 */
/* new-instance v0, Ljava/lang/StringBuilder; */
final String v1 = "AppInfo["; // const-string v1, "AppInfo["
/* invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V */
/* .line 283 */
/* .local v0, "stringBuilder":Ljava/lang/StringBuilder; */
final String v1 = "name: "; // const-string v1, "name: "
(( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
v2 = this.mPackageName;
(( java.lang.StringBuilder ) v1 ).append ( v2 ); // invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
/* .line 284 */
final String v1 = ", "; // const-string v1, ", "
(( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
/* .line 285 */
/* const-string/jumbo v2, "uid: " */
(( java.lang.StringBuilder ) v0 ).append ( v2 ); // invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
/* iget v3, p0, Lcom/miui/server/multisence/SingleWindowInfo;->myUid:I */
(( java.lang.StringBuilder ) v2 ).append ( v3 ); // invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;
/* .line 286 */
(( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
/* .line 287 */
final String v2 = "pid: "; // const-string v2, "pid: "
(( java.lang.StringBuilder ) v0 ).append ( v2 ); // invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
/* iget v3, p0, Lcom/miui/server/multisence/SingleWindowInfo;->myPid:I */
(( java.lang.StringBuilder ) v2 ).append ( v3 ); // invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;
/* .line 288 */
(( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
/* .line 289 */
final String v2 = "isHighPriority: "; // const-string v2, "isHighPriority: "
(( java.lang.StringBuilder ) v0 ).append ( v2 ); // invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
/* iget-boolean v3, p0, Lcom/miui/server/multisence/SingleWindowInfo;->isHighPriority:Z */
(( java.lang.StringBuilder ) v2 ).append ( v3 ); // invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;
/* .line 290 */
(( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
/* .line 291 */
/* const-string/jumbo v2, "window state: " */
(( java.lang.StringBuilder ) v0 ).append ( v2 ); // invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
v3 = this.mForm;
(( java.lang.StringBuilder ) v2 ).append ( v3 ); // invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;
/* .line 292 */
(( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
/* .line 293 */
final String v2 = "covered: "; // const-string v2, "covered: "
(( java.lang.StringBuilder ) v0 ).append ( v2 ); // invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
/* iget-boolean v3, p0, Lcom/miui/server/multisence/SingleWindowInfo;->isCovered:Z */
if ( v3 != null) { // if-eqz v3, :cond_0
/* const-string/jumbo v3, "y" */
} // :cond_0
final String v3 = "n"; // const-string v3, "n"
} // :goto_0
(( java.lang.StringBuilder ) v2 ).append ( v3 ); // invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
/* .line 294 */
(( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
/* .line 295 */
final String v1 = "sched: "; // const-string v1, "sched: "
(( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
/* .line 296 */
v1 = this.mScheduleStatus;
v2 = } // :goto_1
if ( v2 != null) { // if-eqz v2, :cond_1
/* check-cast v2, Lcom/miui/server/multisence/SingleWindowInfo$ScheduleStatus; */
/* .line 297 */
/* .local v2, "sSched":Lcom/miui/server/multisence/SingleWindowInfo$ScheduleStatus; */
/* new-instance v3, Ljava/lang/StringBuilder; */
/* invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V */
/* iget v4, v2, Lcom/miui/server/multisence/SingleWindowInfo$ScheduleStatus;->id:I */
java.lang.Integer .toString ( v4 );
(( java.lang.StringBuilder ) v3 ).append ( v4 ); // invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
final String v4 = " "; // const-string v4, " "
(( java.lang.StringBuilder ) v3 ).append ( v4 ); // invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v3 ).toString ( ); // invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
(( java.lang.StringBuilder ) v0 ).append ( v3 ); // invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
/* .line 298 */
} // .end local v2 # "sSched":Lcom/miui/server/multisence/SingleWindowInfo$ScheduleStatus;
/* .line 300 */
} // :cond_1
(( java.lang.StringBuilder ) v0 ).toString ( ); // invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
} // .end method
