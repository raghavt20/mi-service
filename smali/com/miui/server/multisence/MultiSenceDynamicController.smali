.class public Lcom/miui/server/multisence/MultiSenceDynamicController;
.super Ljava/lang/Object;
.source "MultiSenceDynamicController.java"


# static fields
.field private static final CLOUD_MULTISENCE_ENABLE:Ljava/lang/String; = "cloud_multisence_enable"

.field private static final CLOUD_MULTISENCE_SUBFUNC_NONSUPPORT:Ljava/lang/String; = "cloud_multisence_subfunc_nonsupport"

.field public static final FW_WHITELIST:Ljava/lang/String; = "support_fw_app"

.field public static IS_CLOUD_ENABLED:Z = false

.field private static final KEY_POWER_PERFORMANCE_MODE_OPEN:Ljava/lang/String; = "POWER_PERFORMANCE_MODE_OPEN"

.field private static final PROPERTY_CLOUD_MULTISENCE_ENABLE:Ljava/lang/String; = "persist.multisence.cloud.enable"

.field public static final VRS_WHITELIST:Ljava/lang/String; = "support_common_vrs_app"


# instance fields
.field private TAG:Ljava/lang/String;

.field private mContext:Landroid/content/Context;

.field private mHandler:Landroid/os/Handler;

.field private mService:Lcom/miui/server/multisence/MultiSenceService;


# direct methods
.method static bridge synthetic -$$Nest$fgetTAG(Lcom/miui/server/multisence/MultiSenceDynamicController;)Ljava/lang/String;
    .locals 0

    iget-object p0, p0, Lcom/miui/server/multisence/MultiSenceDynamicController;->TAG:Ljava/lang/String;

    return-object p0
.end method

.method static bridge synthetic -$$Nest$fgetmContext(Lcom/miui/server/multisence/MultiSenceDynamicController;)Landroid/content/Context;
    .locals 0

    iget-object p0, p0, Lcom/miui/server/multisence/MultiSenceDynamicController;->mContext:Landroid/content/Context;

    return-object p0
.end method

.method static bridge synthetic -$$Nest$fgetmService(Lcom/miui/server/multisence/MultiSenceDynamicController;)Lcom/miui/server/multisence/MultiSenceService;
    .locals 0

    iget-object p0, p0, Lcom/miui/server/multisence/MultiSenceDynamicController;->mService:Lcom/miui/server/multisence/MultiSenceService;

    return-object p0
.end method

.method static constructor <clinit>()V
    .locals 2

    .line 34
    const-string v0, "persist.multisence.cloud.enable"

    const/4 v1, 0x0

    invoke-static {v0, v1}, Landroid/os/SystemProperties;->getBoolean(Ljava/lang/String;Z)Z

    move-result v0

    sput-boolean v0, Lcom/miui/server/multisence/MultiSenceDynamicController;->IS_CLOUD_ENABLED:Z

    return-void
.end method

.method public constructor <init>(Lcom/miui/server/multisence/MultiSenceService;Landroid/os/Handler;Landroid/content/Context;)V
    .locals 1
    .param p1, "service"    # Lcom/miui/server/multisence/MultiSenceService;
    .param p2, "handler"    # Landroid/os/Handler;
    .param p3, "context"    # Landroid/content/Context;

    .line 52
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 29
    const-string v0, "MultiSenceDynamicController"

    iput-object v0, p0, Lcom/miui/server/multisence/MultiSenceDynamicController;->TAG:Ljava/lang/String;

    .line 53
    iput-object p1, p0, Lcom/miui/server/multisence/MultiSenceDynamicController;->mService:Lcom/miui/server/multisence/MultiSenceService;

    .line 54
    iput-object p2, p0, Lcom/miui/server/multisence/MultiSenceDynamicController;->mHandler:Landroid/os/Handler;

    .line 55
    iput-object p3, p0, Lcom/miui/server/multisence/MultiSenceDynamicController;->mContext:Landroid/content/Context;

    .line 56
    return-void
.end method


# virtual methods
.method public registerCloudObserver()V
    .locals 5

    .line 59
    new-instance v0, Lcom/miui/server/multisence/MultiSenceDynamicController$1;

    iget-object v1, p0, Lcom/miui/server/multisence/MultiSenceDynamicController;->mHandler:Landroid/os/Handler;

    invoke-direct {v0, p0, v1}, Lcom/miui/server/multisence/MultiSenceDynamicController$1;-><init>(Lcom/miui/server/multisence/MultiSenceDynamicController;Landroid/os/Handler;)V

    .line 115
    .local v0, "observer":Landroid/database/ContentObserver;
    iget-object v1, p0, Lcom/miui/server/multisence/MultiSenceDynamicController;->mContext:Landroid/content/Context;

    invoke-virtual {v1}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v1

    .line 116
    const-string v2, "cloud_multisence_enable"

    invoke-static {v2}, Landroid/provider/Settings$System;->getUriFor(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v2

    .line 115
    const/4 v3, 0x0

    const/4 v4, -0x2

    invoke-virtual {v1, v2, v3, v0, v4}, Landroid/content/ContentResolver;->registerContentObserver(Landroid/net/Uri;ZLandroid/database/ContentObserver;I)V

    .line 118
    iget-object v1, p0, Lcom/miui/server/multisence/MultiSenceDynamicController;->mContext:Landroid/content/Context;

    invoke-virtual {v1}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v1

    .line 119
    const-string v2, "POWER_PERFORMANCE_MODE_OPEN"

    invoke-static {v2}, Landroid/provider/Settings$System;->getUriFor(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v2

    .line 118
    invoke-virtual {v1, v2, v3, v0, v4}, Landroid/content/ContentResolver;->registerContentObserver(Landroid/net/Uri;ZLandroid/database/ContentObserver;I)V

    .line 121
    iget-object v1, p0, Lcom/miui/server/multisence/MultiSenceDynamicController;->mContext:Landroid/content/Context;

    invoke-virtual {v1}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v1

    .line 122
    const-string v2, "cloud_multisence_subfunc_nonsupport"

    invoke-static {v2}, Landroid/provider/Settings$System;->getUriFor(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v2

    .line 121
    invoke-virtual {v1, v2, v3, v0, v4}, Landroid/content/ContentResolver;->registerContentObserver(Landroid/net/Uri;ZLandroid/database/ContentObserver;I)V

    .line 125
    sget-boolean v1, Lcom/miui/server/multisence/MultiSenceConfig;->DEBUG_DYNAMIC_CONTROLLER:Z

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "isCloudEnable: "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    sget-boolean v3, Lcom/miui/server/multisence/MultiSenceDynamicController;->IS_CLOUD_ENABLED:Z

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/miui/server/multisence/MultiSenceServiceUtils;->msLogD(ZLjava/lang/String;)V

    .line 126
    return-void
.end method

.method public registerFWCloudObserver()V
    .locals 5

    .line 147
    new-instance v0, Lcom/miui/server/multisence/MultiSenceDynamicController$3;

    iget-object v1, p0, Lcom/miui/server/multisence/MultiSenceDynamicController;->mHandler:Landroid/os/Handler;

    invoke-direct {v0, p0, v1}, Lcom/miui/server/multisence/MultiSenceDynamicController$3;-><init>(Lcom/miui/server/multisence/MultiSenceDynamicController;Landroid/os/Handler;)V

    .line 156
    .local v0, "observer":Landroid/database/ContentObserver;
    iget-object v1, p0, Lcom/miui/server/multisence/MultiSenceDynamicController;->mContext:Landroid/content/Context;

    invoke-virtual {v1}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v1

    .line 157
    const-string/jumbo v2, "support_fw_app"

    invoke-static {v2}, Landroid/provider/Settings$System;->getUriFor(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v2

    .line 156
    const/4 v3, 0x0

    const/4 v4, -0x2

    invoke-virtual {v1, v2, v3, v0, v4}, Landroid/content/ContentResolver;->registerContentObserver(Landroid/net/Uri;ZLandroid/database/ContentObserver;I)V

    .line 159
    return-void
.end method

.method public registerVRSCloudObserver()V
    .locals 5

    .line 129
    new-instance v0, Lcom/miui/server/multisence/MultiSenceDynamicController$2;

    iget-object v1, p0, Lcom/miui/server/multisence/MultiSenceDynamicController;->mHandler:Landroid/os/Handler;

    invoke-direct {v0, p0, v1}, Lcom/miui/server/multisence/MultiSenceDynamicController$2;-><init>(Lcom/miui/server/multisence/MultiSenceDynamicController;Landroid/os/Handler;)V

    .line 141
    .local v0, "observer":Landroid/database/ContentObserver;
    iget-object v1, p0, Lcom/miui/server/multisence/MultiSenceDynamicController;->mContext:Landroid/content/Context;

    invoke-virtual {v1}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v1

    .line 142
    const-string/jumbo v2, "support_common_vrs_app"

    invoke-static {v2}, Landroid/provider/Settings$System;->getUriFor(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v2

    .line 141
    const/4 v3, 0x0

    const/4 v4, -0x2

    invoke-virtual {v1, v2, v3, v0, v4}, Landroid/content/ContentResolver;->registerContentObserver(Landroid/net/Uri;ZLandroid/database/ContentObserver;I)V

    .line 144
    return-void
.end method
