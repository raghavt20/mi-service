.class public Lcom/miui/server/multisence/MultiSenceServicePolicy;
.super Ljava/lang/Object;
.source "MultiSenceServicePolicy.java"


# static fields
.field private static final DEBUG:Z

.field private static final DEFAULT_PRIORITY_LEVEL:I

.field private static final DEFAULT_PRIORITY_TIMEOUT:I

.field private static final FREEFORM_ELUDE_PRIORITY_LEVEL:I

.field private static final FREEFORM_ELUDE_TIMOUT:I

.field private static final FREEFORM_ENTERFULLSCREEN_PRIORITY_LEVEL:I

.field private static final FREEFORM_ENTERFULLSCREEN_PRIORITY_TIMEOUT:I

.field private static final FREEFORM_MOVE_PRIORITY_LEVEL:I

.field private static final FREEFORM_MOVE_PRIORITY_TIMEOUT:I

.field private static final FREEFORM_RESIZE_PRIORITY_LEVEL:I

.field private static final FREEFORM_RESIZE_PRIORITY_TIMEOUT:I

.field private static final FW_PRIORITY_LEVEL:I

.field private static final FW_PRIORITY_TIMEOUT:I

.field private static final MCD_DF_PATH:Ljava/lang/String; = "/data/system/mcd/mwdf"

.field private static final MWS_PRIORITY_LEVEL:I

.field private static final MWS_PRIORITY_TIMEOUT:I

.field private static final SPLITSCREEN_PRIORITY_LEVEL:I

.field private static final SPLITSCREEN_PRIORITY_TIMEOUT:I

.field private static final TAG:Ljava/lang/String; = "MultiSenceServicePolicy"

.field private static final VRA_CMD_APP_CONTROL:I = 0x2

.field private static final VRA_CMD_SYNC_WHITELIST:I = 0x1

.field private static mJoyoseService:Landroid/os/IBinder;


# instance fields
.field currentInputFocus:Ljava/lang/String;

.field dynamicSencePackage:Ljava/lang/String;

.field focusPackage:Ljava/lang/String;

.field isDynamicSenceStarting:Z

.field private mDefaultRequestIds:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList<",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation
.end field

.field private mFWBindCoreRequestId:I

.field private mFWRequestId:I

.field private mFreeformEludeBindCoreRequestId:I

.field private mFreeformEludeRequestId:I

.field private mFreeformEnterFullScreenBindCoreRequestId:I

.field private mFreeformEnterFullScreenRequestId:I

.field private mFreeformMoveBindCoreRequestId:I

.field private mFreeformMoveRequestId:I

.field private mFreeformResizeBindCoreRequestId:I

.field private mFreeformResizeRequestId:I

.field private mMWSBindCoreRequestId:I

.field private mMWSRequestId:I

.field private mSplitscreenBindCoreRequestId:I

.field private mSplitscreenRequestId:I

.field private mVrsWorkList:Ljava/util/Set;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Set<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field schedWindows:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map<",
            "Ljava/lang/String;",
            "Lcom/miui/server/multisence/SingleWindowInfo;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .locals 4

    .line 47
    const-string v0, "persist.multisence.debug.on"

    const/4 v1, 0x0

    invoke-static {v0, v1}, Landroid/os/SystemProperties;->getBoolean(Ljava/lang/String;Z)Z

    move-result v0

    sput-boolean v0, Lcom/miui/server/multisence/MultiSenceServicePolicy;->DEBUG:Z

    .line 51
    const/4 v0, 0x0

    sput-object v0, Lcom/miui/server/multisence/MultiSenceServicePolicy;->mJoyoseService:Landroid/os/IBinder;

    .line 73
    const-string v0, "persist.multisence.pri_time.freeform_move"

    const/16 v1, 0x1f40

    invoke-static {v0, v1}, Landroid/os/SystemProperties;->getInt(Ljava/lang/String;I)I

    move-result v0

    sput v0, Lcom/miui/server/multisence/MultiSenceServicePolicy;->FREEFORM_MOVE_PRIORITY_TIMEOUT:I

    .line 74
    const-string v0, "persist.multisence.pri_level.freeform_move"

    const/4 v2, 0x1

    invoke-static {v0, v2}, Landroid/os/SystemProperties;->getInt(Ljava/lang/String;I)I

    move-result v0

    sput v0, Lcom/miui/server/multisence/MultiSenceServicePolicy;->FREEFORM_MOVE_PRIORITY_LEVEL:I

    .line 75
    const-string v0, "persist.multisence.pri_time.freeform_resize"

    invoke-static {v0, v1}, Landroid/os/SystemProperties;->getInt(Ljava/lang/String;I)I

    move-result v0

    sput v0, Lcom/miui/server/multisence/MultiSenceServicePolicy;->FREEFORM_RESIZE_PRIORITY_TIMEOUT:I

    .line 76
    const-string v0, "persist.multisence.pri_level.freeform_resize"

    invoke-static {v0, v2}, Landroid/os/SystemProperties;->getInt(Ljava/lang/String;I)I

    move-result v0

    sput v0, Lcom/miui/server/multisence/MultiSenceServicePolicy;->FREEFORM_RESIZE_PRIORITY_LEVEL:I

    .line 77
    const-string v0, "persist.multisence.pri_time.freeform_enterfullscreen"

    invoke-static {v0, v1}, Landroid/os/SystemProperties;->getInt(Ljava/lang/String;I)I

    move-result v0

    sput v0, Lcom/miui/server/multisence/MultiSenceServicePolicy;->FREEFORM_ENTERFULLSCREEN_PRIORITY_TIMEOUT:I

    .line 78
    const-string v0, "persist.multisence.pri_level.freeform_enterfullscreen"

    invoke-static {v0, v2}, Landroid/os/SystemProperties;->getInt(Ljava/lang/String;I)I

    move-result v0

    sput v0, Lcom/miui/server/multisence/MultiSenceServicePolicy;->FREEFORM_ENTERFULLSCREEN_PRIORITY_LEVEL:I

    .line 79
    const-string v0, "persist.multisence.pri_time.freeform_elude"

    const/16 v1, 0x3e8

    invoke-static {v0, v1}, Landroid/os/SystemProperties;->getInt(Ljava/lang/String;I)I

    move-result v0

    sput v0, Lcom/miui/server/multisence/MultiSenceServicePolicy;->FREEFORM_ELUDE_TIMOUT:I

    .line 80
    const-string v0, "persist.multisence.pri_level.freeform_elude"

    invoke-static {v0, v2}, Landroid/os/SystemProperties;->getInt(Ljava/lang/String;I)I

    move-result v0

    sput v0, Lcom/miui/server/multisence/MultiSenceServicePolicy;->FREEFORM_ELUDE_PRIORITY_LEVEL:I

    .line 81
    const-string v0, "persist.multisence.pri_time.splitscreen"

    const/16 v3, 0x7d0

    invoke-static {v0, v3}, Landroid/os/SystemProperties;->getInt(Ljava/lang/String;I)I

    move-result v0

    sput v0, Lcom/miui/server/multisence/MultiSenceServicePolicy;->SPLITSCREEN_PRIORITY_TIMEOUT:I

    .line 82
    const-string v0, "persist.multisence.pri_level.splitscreen"

    invoke-static {v0, v2}, Landroid/os/SystemProperties;->getInt(Ljava/lang/String;I)I

    move-result v0

    sput v0, Lcom/miui/server/multisence/MultiSenceServicePolicy;->SPLITSCREEN_PRIORITY_LEVEL:I

    .line 83
    const-string v0, "persist.multisence.pri_time.mws"

    const/16 v3, 0x1770

    invoke-static {v0, v3}, Landroid/os/SystemProperties;->getInt(Ljava/lang/String;I)I

    move-result v0

    sput v0, Lcom/miui/server/multisence/MultiSenceServicePolicy;->MWS_PRIORITY_TIMEOUT:I

    .line 84
    const-string v0, "persist.multisence.pri_level.mws"

    invoke-static {v0, v2}, Landroid/os/SystemProperties;->getInt(Ljava/lang/String;I)I

    move-result v0

    sput v0, Lcom/miui/server/multisence/MultiSenceServicePolicy;->MWS_PRIORITY_LEVEL:I

    .line 85
    const-string v0, "persist.multisence.pri_time.default"

    invoke-static {v0, v1}, Landroid/os/SystemProperties;->getInt(Ljava/lang/String;I)I

    move-result v0

    sput v0, Lcom/miui/server/multisence/MultiSenceServicePolicy;->DEFAULT_PRIORITY_TIMEOUT:I

    .line 86
    const-string v0, "persist.multisence.pri_level.default"

    const/4 v1, 0x2

    invoke-static {v0, v1}, Landroid/os/SystemProperties;->getInt(Ljava/lang/String;I)I

    move-result v0

    sput v0, Lcom/miui/server/multisence/MultiSenceServicePolicy;->DEFAULT_PRIORITY_LEVEL:I

    .line 87
    nop

    .line 88
    const-string v0, "persist.multisence.pri_time.floatingwindow_move"

    const/16 v1, 0xfa0

    invoke-static {v0, v1}, Landroid/os/SystemProperties;->getInt(Ljava/lang/String;I)I

    move-result v0

    sput v0, Lcom/miui/server/multisence/MultiSenceServicePolicy;->FW_PRIORITY_TIMEOUT:I

    .line 89
    nop

    .line 90
    const-string v0, "persist.multisence.pri_level.floatingwindow_move"

    invoke-static {v0, v2}, Landroid/os/SystemProperties;->getInt(Ljava/lang/String;I)I

    move-result v0

    sput v0, Lcom/miui/server/multisence/MultiSenceServicePolicy;->FW_PRIORITY_LEVEL:I

    .line 89
    return-void
.end method

.method public constructor <init>()V
    .locals 2

    .line 98
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 55
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    iput-object v0, p0, Lcom/miui/server/multisence/MultiSenceServicePolicy;->schedWindows:Ljava/util/Map;

    .line 57
    const/4 v0, -0x1

    iput v0, p0, Lcom/miui/server/multisence/MultiSenceServicePolicy;->mFreeformMoveRequestId:I

    .line 58
    iput v0, p0, Lcom/miui/server/multisence/MultiSenceServicePolicy;->mFreeformMoveBindCoreRequestId:I

    .line 59
    iput v0, p0, Lcom/miui/server/multisence/MultiSenceServicePolicy;->mFreeformResizeRequestId:I

    .line 60
    iput v0, p0, Lcom/miui/server/multisence/MultiSenceServicePolicy;->mFreeformResizeBindCoreRequestId:I

    .line 61
    iput v0, p0, Lcom/miui/server/multisence/MultiSenceServicePolicy;->mFreeformEnterFullScreenRequestId:I

    .line 62
    iput v0, p0, Lcom/miui/server/multisence/MultiSenceServicePolicy;->mFreeformEnterFullScreenBindCoreRequestId:I

    .line 63
    iput v0, p0, Lcom/miui/server/multisence/MultiSenceServicePolicy;->mFreeformEludeRequestId:I

    .line 64
    iput v0, p0, Lcom/miui/server/multisence/MultiSenceServicePolicy;->mFreeformEludeBindCoreRequestId:I

    .line 65
    iput v0, p0, Lcom/miui/server/multisence/MultiSenceServicePolicy;->mSplitscreenRequestId:I

    .line 66
    iput v0, p0, Lcom/miui/server/multisence/MultiSenceServicePolicy;->mSplitscreenBindCoreRequestId:I

    .line 67
    iput v0, p0, Lcom/miui/server/multisence/MultiSenceServicePolicy;->mMWSRequestId:I

    .line 68
    iput v0, p0, Lcom/miui/server/multisence/MultiSenceServicePolicy;->mMWSBindCoreRequestId:I

    .line 69
    iput v0, p0, Lcom/miui/server/multisence/MultiSenceServicePolicy;->mFWRequestId:I

    .line 70
    iput v0, p0, Lcom/miui/server/multisence/MultiSenceServicePolicy;->mFWBindCoreRequestId:I

    .line 71
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/miui/server/multisence/MultiSenceServicePolicy;->mDefaultRequestIds:Ljava/util/ArrayList;

    .line 96
    new-instance v0, Ljava/util/HashSet;

    invoke-direct {v0}, Ljava/util/HashSet;-><init>()V

    iput-object v0, p0, Lcom/miui/server/multisence/MultiSenceServicePolicy;->mVrsWorkList:Ljava/util/Set;

    .line 99
    sget-boolean v0, Lcom/miui/server/multisence/MultiSenceConfig;->DEBUG_POLICY_BASE:Z

    const-string v1, "new MultiSenceServicePolicy"

    invoke-static {v0, v1}, Lcom/miui/server/multisence/MultiSenceServiceUtils;->msLogD(ZLjava/lang/String;)V

    .line 100
    return-void
.end method

.method private LOG_IF_DEBUG(Ljava/lang/String;)V
    .locals 1
    .param p1, "log"    # Ljava/lang/String;

    .line 787
    sget-boolean v0, Lcom/miui/server/multisence/MultiSenceServicePolicy;->DEBUG:Z

    if-eqz v0, :cond_0

    .line 788
    const-string v0, "MultiSenceServicePolicy"

    invoke-static {v0, p1}, Landroid/util/Slog;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 790
    :cond_0
    return-void
.end method

.method private calculateFocusAndPolicy()V
    .locals 3

    .line 497
    sget-boolean v0, Lcom/miui/server/multisence/MultiSenceConfig;->FREMAPREDICT_ENABLE:Z

    if-nez v0, :cond_0

    .line 498
    const-string v0, "framepredict is not support by multisence"

    invoke-direct {p0, v0}, Lcom/miui/server/multisence/MultiSenceServicePolicy;->LOG_IF_DEBUG(Ljava/lang/String;)V

    .line 499
    return-void

    .line 501
    :cond_0
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "calculateFocusAndPolicy: currentInputFocus="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lcom/miui/server/multisence/MultiSenceServicePolicy;->currentInputFocus:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, " isDynamicSenceStarting="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-boolean v1, p0, Lcom/miui/server/multisence/MultiSenceServicePolicy;->isDynamicSenceStarting:Z

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, " dynamicSencePackage"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lcom/miui/server/multisence/MultiSenceServicePolicy;->dynamicSencePackage:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-direct {p0, v0}, Lcom/miui/server/multisence/MultiSenceServicePolicy;->LOG_IF_DEBUG(Ljava/lang/String;)V

    .line 503
    iget-boolean v0, p0, Lcom/miui/server/multisence/MultiSenceServicePolicy;->isDynamicSenceStarting:Z

    if-nez v0, :cond_1

    .line 504
    iget-object v0, p0, Lcom/miui/server/multisence/MultiSenceServicePolicy;->currentInputFocus:Ljava/lang/String;

    .local v0, "newFocus":Ljava/lang/String;
    goto :goto_0

    .line 506
    .end local v0    # "newFocus":Ljava/lang/String;
    :cond_1
    iget-object v0, p0, Lcom/miui/server/multisence/MultiSenceServicePolicy;->dynamicSencePackage:Ljava/lang/String;

    .line 508
    .restart local v0    # "newFocus":Ljava/lang/String;
    :goto_0
    if-eqz v0, :cond_4

    const-string v1, ""

    invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    goto :goto_1

    .line 513
    :cond_2
    iget-object v1, p0, Lcom/miui/server/multisence/MultiSenceServicePolicy;->focusPackage:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_3

    .line 514
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "final focus changed: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " old focus="

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lcom/miui/server/multisence/MultiSenceServicePolicy;->focusPackage:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {p0, v1}, Lcom/miui/server/multisence/MultiSenceServicePolicy;->LOG_IF_DEBUG(Ljava/lang/String;)V

    .line 515
    iput-object v0, p0, Lcom/miui/server/multisence/MultiSenceServicePolicy;->focusPackage:Ljava/lang/String;

    .line 516
    invoke-static {}, Lcom/android/server/am/MiuiBoosterUtilsStub;->getInstance()Lcom/android/server/am/MiuiBoosterUtilsStub;

    move-result-object v1

    iget-object v2, p0, Lcom/miui/server/multisence/MultiSenceServicePolicy;->focusPackage:Ljava/lang/String;

    invoke-interface {v1, v2}, Lcom/android/server/am/MiuiBoosterUtilsStub;->notifyForegroundAppChanged(Ljava/lang/String;)V

    .line 518
    :cond_3
    return-void

    .line 509
    :cond_4
    :goto_1
    const-string v1, "calculateFocusAndPolicy: fail, newFocus is null"

    invoke-direct {p0, v1}, Lcom/miui/server/multisence/MultiSenceServicePolicy;->LOG_IF_DEBUG(Ljava/lang/String;)V

    .line 510
    return-void
.end method

.method private frameDynamicFpsSched()V
    .locals 7

    .line 287
    sget-boolean v0, Lcom/miui/server/multisence/MultiSenceConfig;->DYN_FPS_ENABLED:Z

    if-nez v0, :cond_2

    .line 288
    sget-boolean v0, Lcom/miui/server/multisence/MultiSenceConfig;->DEBUG_DYN_FPS:Z

    if-nez v0, :cond_1

    sget-boolean v0, Lcom/miui/server/multisence/MultiSenceConfig;->DEBUG_CONFIG:Z

    if-eqz v0, :cond_0

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    goto :goto_1

    :cond_1
    :goto_0
    const/4 v0, 0x1

    :goto_1
    const-string v1, "dyn fps is not supported by multisence config"

    invoke-static {v0, v1}, Lcom/miui/server/multisence/MultiSenceServiceUtils;->msLogD(ZLjava/lang/String;)V

    .line 289
    return-void

    .line 291
    :cond_2
    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, ""

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    .line 292
    .local v0, "dynFpsCmd":Ljava/lang/StringBuilder;
    iget-object v1, p0, Lcom/miui/server/multisence/MultiSenceServicePolicy;->schedWindows:Ljava/util/Map;

    invoke-interface {v1}, Ljava/util/Map;->keySet()Ljava/util/Set;

    move-result-object v1

    invoke-interface {v1}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_2
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    const-string v3, "\n"

    if-eqz v2, :cond_7

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/lang/String;

    .line 293
    .local v2, "appName":Ljava/lang/String;
    if-nez v2, :cond_3

    goto :goto_2

    .line 294
    :cond_3
    iget-object v4, p0, Lcom/miui/server/multisence/MultiSenceServicePolicy;->schedWindows:Ljava/util/Map;

    invoke-interface {v4, v2}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lcom/miui/server/multisence/SingleWindowInfo;

    .line 295
    .local v4, "app":Lcom/miui/server/multisence/SingleWindowInfo;
    if-nez v4, :cond_4

    goto :goto_2

    .line 297
    :cond_4
    invoke-virtual {v4}, Lcom/miui/server/multisence/SingleWindowInfo;->getChangeStatus()Lcom/miui/server/multisence/SingleWindowInfo$WindowChangeState;

    move-result-object v5

    sget-object v6, Lcom/miui/server/multisence/SingleWindowInfo$WindowChangeState;->RESET:Lcom/miui/server/multisence/SingleWindowInfo$WindowChangeState;

    if-ne v5, v6, :cond_5

    .line 298
    goto :goto_2

    .line 301
    :cond_5
    sget-object v5, Lcom/miui/server/multisence/SingleWindowInfo$ScheduleStatus;->DYN_FPS_SET:Lcom/miui/server/multisence/SingleWindowInfo$ScheduleStatus;

    invoke-virtual {v4, v5}, Lcom/miui/server/multisence/SingleWindowInfo;->checkSchedStatus(Lcom/miui/server/multisence/SingleWindowInfo$ScheduleStatus;)Z

    move-result v5

    if-eqz v5, :cond_6

    .line 302
    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v5, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, " "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v4}, Lcom/miui/server/multisence/SingleWindowInfo;->getLimitFPS()I

    move-result v6

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 304
    .end local v2    # "appName":Ljava/lang/String;
    .end local v4    # "app":Lcom/miui/server/multisence/SingleWindowInfo;
    :cond_6
    goto :goto_2

    .line 305
    :cond_7
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->length()I

    move-result v1

    if-nez v1, :cond_8

    .line 306
    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 308
    :cond_8
    sget-boolean v1, Lcom/miui/server/multisence/MultiSenceConfig;->DEBUG_DYN_FPS:Z

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "App fps cmd: "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/miui/server/multisence/MultiSenceServiceUtils;->msLogD(ZLjava/lang/String;)V

    .line 309
    const-string v1, "/data/system/mcd/mwdf"

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/miui/server/multisence/MultiSenceServiceUtils;->writeToFile(Ljava/lang/String;Ljava/lang/String;)Z

    .line 310
    return-void
.end method

.method private frameVrsSched()V
    .locals 8

    .line 313
    sget-boolean v0, Lcom/miui/server/multisence/MultiSenceConfig;->VRS_ENABLE:Z

    if-nez v0, :cond_2

    .line 314
    sget-boolean v0, Lcom/miui/server/multisence/MultiSenceConfig;->DEBUG_VRS:Z

    if-nez v0, :cond_1

    sget-boolean v0, Lcom/miui/server/multisence/MultiSenceConfig;->DEBUG_CONFIG:Z

    if-eqz v0, :cond_0

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    goto :goto_1

    :cond_1
    :goto_0
    const/4 v0, 0x1

    :goto_1
    const-string/jumbo v1, "vrs is not supported by multisence config"

    invoke-static {v0, v1}, Lcom/miui/server/multisence/MultiSenceServiceUtils;->msLogD(ZLjava/lang/String;)V

    .line 315
    return-void

    .line 317
    :cond_2
    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, ""

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    .line 318
    .local v0, "vrsCmd":Ljava/lang/StringBuilder;
    const/4 v1, 0x0

    .line 319
    .local v1, "count":I
    iget-object v2, p0, Lcom/miui/server/multisence/MultiSenceServicePolicy;->schedWindows:Ljava/util/Map;

    invoke-interface {v2}, Ljava/util/Map;->keySet()Ljava/util/Set;

    move-result-object v2

    invoke-interface {v2}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_2
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_a

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/lang/String;

    .line 320
    .local v3, "appName":Ljava/lang/String;
    if-nez v3, :cond_3

    goto :goto_2

    .line 321
    :cond_3
    iget-object v4, p0, Lcom/miui/server/multisence/MultiSenceServicePolicy;->schedWindows:Ljava/util/Map;

    invoke-interface {v4, v3}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lcom/miui/server/multisence/SingleWindowInfo;

    .line 322
    .local v4, "app":Lcom/miui/server/multisence/SingleWindowInfo;
    if-nez v4, :cond_4

    goto :goto_2

    .line 323
    :cond_4
    invoke-static {}, Lcom/miui/server/multisence/MultiSenceConfig;->getInstance()Lcom/miui/server/multisence/MultiSenceConfig;

    move-result-object v5

    invoke-virtual {v5, v3}, Lcom/miui/server/multisence/MultiSenceConfig;->checkVrsSupport(Ljava/lang/String;)Z

    move-result v5

    if-nez v5, :cond_5

    .line 324
    sget-boolean v5, Lcom/miui/server/multisence/MultiSenceConfig;->DEBUG_VRS:Z

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v6, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    const-string v7, " is not it vrs whitelist."

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-static {v5, v6}, Lcom/miui/server/multisence/MultiSenceServiceUtils;->msLogD(ZLjava/lang/String;)V

    .line 325
    goto :goto_2

    .line 328
    :cond_5
    invoke-virtual {v4}, Lcom/miui/server/multisence/SingleWindowInfo;->getChangeStatus()Lcom/miui/server/multisence/SingleWindowInfo$WindowChangeState;

    move-result-object v5

    sget-object v6, Lcom/miui/server/multisence/SingleWindowInfo$WindowChangeState;->RESET:Lcom/miui/server/multisence/SingleWindowInfo$WindowChangeState;

    if-ne v5, v6, :cond_7

    .line 329
    invoke-direct {p0, v0, v3}, Lcom/miui/server/multisence/MultiSenceServicePolicy;->setVrsOff(Ljava/lang/StringBuilder;Ljava/lang/String;)Z

    move-result v5

    if-eqz v5, :cond_6

    add-int/lit8 v1, v1, 0x1

    .line 330
    :cond_6
    goto :goto_2

    .line 332
    :cond_7
    sget-object v5, Lcom/miui/server/multisence/SingleWindowInfo$ScheduleStatus;->VRS_RESET:Lcom/miui/server/multisence/SingleWindowInfo$ScheduleStatus;

    invoke-virtual {v4, v5}, Lcom/miui/server/multisence/SingleWindowInfo;->checkSchedStatus(Lcom/miui/server/multisence/SingleWindowInfo$ScheduleStatus;)Z

    move-result v5

    if-eqz v5, :cond_8

    .line 333
    invoke-direct {p0, v0, v3}, Lcom/miui/server/multisence/MultiSenceServicePolicy;->setVrsOff(Ljava/lang/StringBuilder;Ljava/lang/String;)Z

    move-result v5

    if-eqz v5, :cond_9

    add-int/lit8 v1, v1, 0x1

    goto :goto_3

    .line 334
    :cond_8
    sget-object v5, Lcom/miui/server/multisence/SingleWindowInfo$ScheduleStatus;->VRS_SET:Lcom/miui/server/multisence/SingleWindowInfo$ScheduleStatus;

    invoke-virtual {v4, v5}, Lcom/miui/server/multisence/SingleWindowInfo;->checkSchedStatus(Lcom/miui/server/multisence/SingleWindowInfo$ScheduleStatus;)Z

    move-result v5

    if-eqz v5, :cond_9

    .line 335
    invoke-direct {p0, v0, v3}, Lcom/miui/server/multisence/MultiSenceServicePolicy;->setVrsOn(Ljava/lang/StringBuilder;Ljava/lang/String;)Z

    move-result v5

    if-eqz v5, :cond_9

    add-int/lit8 v1, v1, 0x1

    .line 337
    .end local v3    # "appName":Ljava/lang/String;
    .end local v4    # "app":Lcom/miui/server/multisence/SingleWindowInfo;
    :cond_9
    :goto_3
    goto :goto_2

    .line 338
    :cond_a
    const-string v2, "\n"

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 339
    invoke-direct {p0}, Lcom/miui/server/multisence/MultiSenceServicePolicy;->showVrsWorkApp()V

    .line 340
    sget-boolean v2, Lcom/miui/server/multisence/MultiSenceConfig;->DEBUG_VRS:Z

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    invoke-static {v1}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, " comonds for vrs-sched: "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Lcom/miui/server/multisence/MultiSenceServiceUtils;->msLogD(ZLjava/lang/String;)V

    .line 341
    if-gtz v1, :cond_b

    .line 342
    return-void

    .line 344
    :cond_b
    const/4 v2, 0x2

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {p0, v2, v3}, Lcom/miui/server/multisence/MultiSenceServicePolicy;->setCommonVrsParams(ILjava/lang/String;)V

    .line 345
    return-void
.end method

.method static synthetic lambda$rectangleArea$0([I[I)I
    .locals 3
    .param p0, "l1"    # [I
    .param p1, "l2"    # [I

    .line 437
    const/4 v0, 0x0

    aget v1, p0, v0

    aget v2, p1, v0

    if-eq v1, v2, :cond_0

    aget v1, p0, v0

    aget v0, p1, v0

    goto :goto_0

    :cond_0
    const/4 v0, 0x1

    aget v1, p0, v0

    aget v0, p1, v0

    :goto_0
    sub-int/2addr v1, v0

    return v1
.end method

.method private perfSenceSched()V
    .locals 0

    .line 260
    invoke-direct {p0}, Lcom/miui/server/multisence/MultiSenceServicePolicy;->frameDynamicFpsSched()V

    .line 261
    invoke-direct {p0}, Lcom/miui/server/multisence/MultiSenceServicePolicy;->frameVrsSched()V

    .line 262
    return-void
.end method

.method private requestBindCore(I[III)I
    .locals 1
    .param p1, "uid"    # I
    .param p2, "tids"    # [I
    .param p3, "timeout"    # I
    .param p4, "level"    # I

    .line 735
    invoke-static {}, Lcom/android/server/am/MiuiBoosterUtilsStub;->getInstance()Lcom/android/server/am/MiuiBoosterUtilsStub;

    move-result-object v0

    invoke-interface {v0, p1, p2, p4, p3}, Lcom/android/server/am/MiuiBoosterUtilsStub;->requestBindCore(I[III)I

    move-result v0

    return v0
.end method

.method private requestThreadPriority(I[III)I
    .locals 1
    .param p1, "uid"    # I
    .param p2, "tids"    # [I
    .param p3, "timeout"    # I
    .param p4, "level"    # I

    .line 731
    invoke-static {}, Lcom/android/server/am/MiuiBoosterUtilsStub;->getInstance()Lcom/android/server/am/MiuiBoosterUtilsStub;

    move-result-object v0

    invoke-interface {v0, p1, p2, p3, p4}, Lcom/android/server/am/MiuiBoosterUtilsStub;->requestThreadPriority(I[III)I

    move-result v0

    return v0
.end method

.method private setVrsOff(Ljava/lang/StringBuilder;Ljava/lang/String;)Z
    .locals 2
    .param p1, "cmdList"    # Ljava/lang/StringBuilder;
    .param p2, "name"    # Ljava/lang/String;

    .line 276
    if-nez p1, :cond_0

    .line 277
    const/4 v0, 0x0

    return v0

    .line 279
    :cond_0
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v0, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "#0\n"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 280
    iget-object v0, p0, Lcom/miui/server/multisence/MultiSenceServicePolicy;->mVrsWorkList:Ljava/util/Set;

    invoke-interface {v0, p2}, Ljava/util/Set;->contains(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 281
    iget-object v0, p0, Lcom/miui/server/multisence/MultiSenceServicePolicy;->mVrsWorkList:Ljava/util/Set;

    invoke-interface {v0, p2}, Ljava/util/Set;->remove(Ljava/lang/Object;)Z

    .line 283
    :cond_1
    const/4 v0, 0x1

    return v0
.end method

.method private setVrsOn(Ljava/lang/StringBuilder;Ljava/lang/String;)Z
    .locals 2
    .param p1, "cmdList"    # Ljava/lang/StringBuilder;
    .param p2, "name"    # Ljava/lang/String;

    .line 265
    if-nez p1, :cond_0

    .line 266
    const/4 v0, 0x0

    return v0

    .line 268
    :cond_0
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v0, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "#1\n"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 269
    iget-object v0, p0, Lcom/miui/server/multisence/MultiSenceServicePolicy;->mVrsWorkList:Ljava/util/Set;

    invoke-interface {v0, p2}, Ljava/util/Set;->contains(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_1

    .line 270
    iget-object v0, p0, Lcom/miui/server/multisence/MultiSenceServicePolicy;->mVrsWorkList:Ljava/util/Set;

    invoke-interface {v0, p2}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    .line 272
    :cond_1
    const/4 v0, 0x1

    return v0
.end method

.method private showSchedWindowInfo()V
    .locals 5

    .line 769
    iget-object v0, p0, Lcom/miui/server/multisence/MultiSenceServicePolicy;->schedWindows:Ljava/util/Map;

    invoke-interface {v0}, Ljava/util/Map;->keySet()Ljava/util/Set;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v0

    :goto_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_0

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/String;

    .line 770
    .local v1, "name":Ljava/lang/String;
    iget-object v2, p0, Lcom/miui/server/multisence/MultiSenceServicePolicy;->schedWindows:Ljava/util/Map;

    invoke-interface {v2, v1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/miui/server/multisence/SingleWindowInfo;

    .line 771
    .local v2, "app":Lcom/miui/server/multisence/SingleWindowInfo;
    sget-boolean v3, Lcom/miui/server/multisence/MultiSenceConfig;->DEBUG_POLICY_BASE:Z

    invoke-virtual {v2}, Lcom/miui/server/multisence/SingleWindowInfo;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Lcom/miui/server/multisence/MultiSenceServiceUtils;->msLogD(ZLjava/lang/String;)V

    .line 772
    .end local v1    # "name":Ljava/lang/String;
    .end local v2    # "app":Lcom/miui/server/multisence/SingleWindowInfo;
    goto :goto_0

    .line 773
    :cond_0
    return-void
.end method

.method private showVrsWorkApp()V
    .locals 5

    .line 776
    sget-boolean v0, Lcom/miui/server/multisence/MultiSenceConfig;->DEBUG_VRS:Z

    if-nez v0, :cond_0

    .line 777
    return-void

    .line 779
    :cond_0
    new-instance v0, Ljava/lang/StringBuilder;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "VRS works in "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lcom/miui/server/multisence/MultiSenceServicePolicy;->mVrsWorkList:Ljava/util/Set;

    invoke-interface {v2}, Ljava/util/Set;->size()I

    move-result v2

    invoke-static {v2}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " apps:"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    .line 780
    .local v0, "str":Ljava/lang/StringBuilder;
    iget-object v1, p0, Lcom/miui/server/multisence/MultiSenceServicePolicy;->mVrsWorkList:Ljava/util/Set;

    invoke-interface {v1}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_1

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/lang/String;

    .line 781
    .local v2, "name":Ljava/lang/String;
    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, " "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 782
    .end local v2    # "name":Ljava/lang/String;
    goto :goto_0

    .line 783
    :cond_1
    sget-boolean v1, Lcom/miui/server/multisence/MultiSenceConfig;->DEBUG_VRS:Z

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/miui/server/multisence/MultiSenceServiceUtils;->msLogD(ZLjava/lang/String;)V

    .line 784
    return-void
.end method

.method private singleAppSched()V
    .locals 4

    .line 471
    iget-object v0, p0, Lcom/miui/server/multisence/MultiSenceServicePolicy;->schedWindows:Ljava/util/Map;

    invoke-interface {v0}, Ljava/util/Map;->keySet()Ljava/util/Set;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v0

    :goto_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_0

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/String;

    .line 474
    .local v1, "key":Ljava/lang/String;
    iget-object v2, p0, Lcom/miui/server/multisence/MultiSenceServicePolicy;->schedWindows:Ljava/util/Map;

    invoke-interface {v2, v1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/miui/server/multisence/SingleWindowInfo;

    sget-object v3, Lcom/miui/server/multisence/SingleWindowInfo$WindowChangeState;->NOT_CHANGED:Lcom/miui/server/multisence/SingleWindowInfo$WindowChangeState;

    invoke-virtual {v2, v3}, Lcom/miui/server/multisence/SingleWindowInfo;->setChangeStatus(Lcom/miui/server/multisence/SingleWindowInfo$WindowChangeState;)Lcom/miui/server/multisence/SingleWindowInfo;

    .line 475
    .end local v1    # "key":Ljava/lang/String;
    goto :goto_0

    .line 476
    :cond_0
    return-void
.end method

.method private updateExistedWindowPolicy(Ljava/util/Map;Lcom/miui/server/multisence/SingleWindowInfo;Lcom/miui/server/multisence/SingleWindowInfo;)V
    .locals 7
    .param p2, "newWindow"    # Lcom/miui/server/multisence/SingleWindowInfo;
    .param p3, "oldWindow"    # Lcom/miui/server/multisence/SingleWindowInfo;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/Map<",
            "Ljava/lang/String;",
            "Lcom/miui/server/multisence/SingleWindowInfo;",
            ">;",
            "Lcom/miui/server/multisence/SingleWindowInfo;",
            "Lcom/miui/server/multisence/SingleWindowInfo;",
            ")V"
        }
    .end annotation

    .line 167
    .local p1, "schedList":Ljava/util/Map;, "Ljava/util/Map<Ljava/lang/String;Lcom/miui/server/multisence/SingleWindowInfo;>;"
    invoke-virtual {p2}, Lcom/miui/server/multisence/SingleWindowInfo;->getPackageName()Ljava/lang/String;

    move-result-object v0

    .line 168
    .local v0, "name":Ljava/lang/String;
    invoke-virtual {p2}, Lcom/miui/server/multisence/SingleWindowInfo;->getWindowCount()I

    move-result v1

    .line 169
    .local v1, "count":I
    invoke-virtual {p3}, Lcom/miui/server/multisence/SingleWindowInfo;->getPackageName()Ljava/lang/String;

    move-result-object v2

    .line 170
    .local v2, "oldName":Ljava/lang/String;
    if-eqz v0, :cond_0

    invoke-virtual {v0, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-nez v3, :cond_0

    .line 171
    return-void

    .line 173
    :cond_0
    const/4 v3, 0x0

    .line 174
    .local v3, "isChanged":Z
    invoke-virtual {p2}, Lcom/miui/server/multisence/SingleWindowInfo;->isInputFocused()Z

    move-result v4

    invoke-virtual {p3}, Lcom/miui/server/multisence/SingleWindowInfo;->isInputFocused()Z

    move-result v5

    if-eq v4, v5, :cond_1

    .line 175
    const/4 v3, 0x1

    .line 178
    :cond_1
    invoke-virtual {p2}, Lcom/miui/server/multisence/SingleWindowInfo;->getWindowingModeString()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {p3}, Lcom/miui/server/multisence/SingleWindowInfo;->getWindowingModeString()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-nez v4, :cond_2

    .line 179
    const/4 v3, 0x1

    .line 189
    :cond_2
    invoke-virtual {p2}, Lcom/miui/server/multisence/SingleWindowInfo;->isVrsCovered()Z

    move-result v4

    if-nez v4, :cond_3

    invoke-virtual {p3}, Lcom/miui/server/multisence/SingleWindowInfo;->isVrsCovered()Z

    move-result v4

    if-eqz v4, :cond_3

    .line 190
    sget-object v4, Lcom/miui/server/multisence/SingleWindowInfo$ScheduleStatus;->VRS_RESET:Lcom/miui/server/multisence/SingleWindowInfo$ScheduleStatus;

    invoke-virtual {p2, v4}, Lcom/miui/server/multisence/SingleWindowInfo;->setSchedStatus(Lcom/miui/server/multisence/SingleWindowInfo$ScheduleStatus;)Lcom/miui/server/multisence/SingleWindowInfo;

    .line 191
    const/4 v3, 0x1

    .line 193
    :cond_3
    invoke-virtual {p2}, Lcom/miui/server/multisence/SingleWindowInfo;->isCovered()Z

    move-result v4

    const/4 v5, 0x1

    if-eqz v4, :cond_6

    .line 194
    if-ne v1, v5, :cond_4

    .line 195
    sget-object v4, Lcom/miui/server/multisence/SingleWindowInfo$ScheduleStatus;->DYN_FPS_SET:Lcom/miui/server/multisence/SingleWindowInfo$ScheduleStatus;

    invoke-virtual {p2, v4}, Lcom/miui/server/multisence/SingleWindowInfo;->setSchedStatus(Lcom/miui/server/multisence/SingleWindowInfo$ScheduleStatus;)Lcom/miui/server/multisence/SingleWindowInfo;

    .line 197
    :cond_4
    invoke-virtual {p2}, Lcom/miui/server/multisence/SingleWindowInfo;->isVrsCovered()Z

    move-result v4

    if-eqz v4, :cond_5

    invoke-virtual {p3}, Lcom/miui/server/multisence/SingleWindowInfo;->isVrsCovered()Z

    move-result v4

    if-nez v4, :cond_5

    .line 198
    sget-object v4, Lcom/miui/server/multisence/SingleWindowInfo$ScheduleStatus;->VRS_SET:Lcom/miui/server/multisence/SingleWindowInfo$ScheduleStatus;

    invoke-virtual {p2, v4}, Lcom/miui/server/multisence/SingleWindowInfo;->setSchedStatus(Lcom/miui/server/multisence/SingleWindowInfo$ScheduleStatus;)Lcom/miui/server/multisence/SingleWindowInfo;

    .line 200
    :cond_5
    const/4 v3, 0x1

    .line 210
    :cond_6
    invoke-virtual {p2}, Lcom/miui/server/multisence/SingleWindowInfo;->getWindowForm()Lcom/miui/server/multisence/SingleWindowInfo$WindowForm;

    move-result-object v4

    sget-object v6, Lcom/miui/server/multisence/SingleWindowInfo$WindowForm;->MUTIL_FREEDOM_MINI:Lcom/miui/server/multisence/SingleWindowInfo$WindowForm;

    if-ne v4, v6, :cond_9

    .line 211
    if-ne v1, v5, :cond_7

    .line 212
    sget-object v4, Lcom/miui/server/multisence/SingleWindowInfo$ScheduleStatus;->DYN_FPS_SET:Lcom/miui/server/multisence/SingleWindowInfo$ScheduleStatus;

    invoke-virtual {p2, v4}, Lcom/miui/server/multisence/SingleWindowInfo;->setSchedStatus(Lcom/miui/server/multisence/SingleWindowInfo$ScheduleStatus;)Lcom/miui/server/multisence/SingleWindowInfo;

    .line 214
    :cond_7
    invoke-virtual {p3}, Lcom/miui/server/multisence/SingleWindowInfo;->getWindowForm()Lcom/miui/server/multisence/SingleWindowInfo$WindowForm;

    move-result-object v4

    sget-object v5, Lcom/miui/server/multisence/SingleWindowInfo$WindowForm;->MUTIL_FREEDOM_MINI:Lcom/miui/server/multisence/SingleWindowInfo$WindowForm;

    if-eq v4, v5, :cond_8

    .line 215
    sget-object v4, Lcom/miui/server/multisence/SingleWindowInfo$ScheduleStatus;->VRS_SET:Lcom/miui/server/multisence/SingleWindowInfo$ScheduleStatus;

    invoke-virtual {p2, v4}, Lcom/miui/server/multisence/SingleWindowInfo;->setSchedStatus(Lcom/miui/server/multisence/SingleWindowInfo$ScheduleStatus;)Lcom/miui/server/multisence/SingleWindowInfo;

    .line 217
    :cond_8
    const/4 v3, 0x1

    goto :goto_0

    .line 219
    :cond_9
    invoke-virtual {p3}, Lcom/miui/server/multisence/SingleWindowInfo;->getWindowForm()Lcom/miui/server/multisence/SingleWindowInfo$WindowForm;

    move-result-object v4

    sget-object v5, Lcom/miui/server/multisence/SingleWindowInfo$WindowForm;->MUTIL_FREEDOM_MINI:Lcom/miui/server/multisence/SingleWindowInfo$WindowForm;

    if-ne v4, v5, :cond_a

    .line 220
    sget-object v4, Lcom/miui/server/multisence/SingleWindowInfo$ScheduleStatus;->VRS_RESET:Lcom/miui/server/multisence/SingleWindowInfo$ScheduleStatus;

    invoke-virtual {p2, v4}, Lcom/miui/server/multisence/SingleWindowInfo;->setSchedStatus(Lcom/miui/server/multisence/SingleWindowInfo$ScheduleStatus;)Lcom/miui/server/multisence/SingleWindowInfo;

    .line 221
    const/4 v3, 0x1

    .line 225
    :cond_a
    :goto_0
    if-eqz v3, :cond_b

    sget-object v4, Lcom/miui/server/multisence/SingleWindowInfo$WindowChangeState;->CHANGED:Lcom/miui/server/multisence/SingleWindowInfo$WindowChangeState;

    goto :goto_1

    :cond_b
    sget-object v4, Lcom/miui/server/multisence/SingleWindowInfo$WindowChangeState;->NOT_CHANGED:Lcom/miui/server/multisence/SingleWindowInfo$WindowChangeState;

    :goto_1
    invoke-virtual {p2, v4}, Lcom/miui/server/multisence/SingleWindowInfo;->setChangeStatus(Lcom/miui/server/multisence/SingleWindowInfo$WindowChangeState;)Lcom/miui/server/multisence/SingleWindowInfo;

    .line 226
    iget-object v4, p0, Lcom/miui/server/multisence/MultiSenceServicePolicy;->schedWindows:Ljava/util/Map;

    invoke-interface {v4, v0, p2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 227
    return-void
.end method

.method private updateLeftWindowPolicy(Ljava/util/Map;Lcom/miui/server/multisence/SingleWindowInfo;)V
    .locals 2
    .param p2, "leftWindow"    # Lcom/miui/server/multisence/SingleWindowInfo;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/Map<",
            "Ljava/lang/String;",
            "Lcom/miui/server/multisence/SingleWindowInfo;",
            ">;",
            "Lcom/miui/server/multisence/SingleWindowInfo;",
            ")V"
        }
    .end annotation

    .line 133
    .local p1, "schedList":Ljava/util/Map;, "Ljava/util/Map<Ljava/lang/String;Lcom/miui/server/multisence/SingleWindowInfo;>;"
    invoke-virtual {p2}, Lcom/miui/server/multisence/SingleWindowInfo;->getPackageName()Ljava/lang/String;

    move-result-object v0

    .line 134
    .local v0, "name":Ljava/lang/String;
    if-nez v0, :cond_0

    .line 135
    return-void

    .line 137
    :cond_0
    sget-object v1, Lcom/miui/server/multisence/SingleWindowInfo$WindowChangeState;->RESET:Lcom/miui/server/multisence/SingleWindowInfo$WindowChangeState;

    invoke-virtual {p2, v1}, Lcom/miui/server/multisence/SingleWindowInfo;->setChangeStatus(Lcom/miui/server/multisence/SingleWindowInfo$WindowChangeState;)Lcom/miui/server/multisence/SingleWindowInfo;

    .line 138
    iget-object v1, p0, Lcom/miui/server/multisence/MultiSenceServicePolicy;->schedWindows:Ljava/util/Map;

    invoke-interface {v1, v0, p2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 139
    return-void
.end method

.method private updateNewWindowPolicy(Ljava/util/Map;Lcom/miui/server/multisence/SingleWindowInfo;)V
    .locals 5
    .param p2, "newWindow"    # Lcom/miui/server/multisence/SingleWindowInfo;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/Map<",
            "Ljava/lang/String;",
            "Lcom/miui/server/multisence/SingleWindowInfo;",
            ">;",
            "Lcom/miui/server/multisence/SingleWindowInfo;",
            ")V"
        }
    .end annotation

    .line 142
    .local p1, "schedList":Ljava/util/Map;, "Ljava/util/Map<Ljava/lang/String;Lcom/miui/server/multisence/SingleWindowInfo;>;"
    invoke-virtual {p2}, Lcom/miui/server/multisence/SingleWindowInfo;->getPackageName()Ljava/lang/String;

    move-result-object v0

    .line 143
    .local v0, "name":Ljava/lang/String;
    invoke-virtual {p2}, Lcom/miui/server/multisence/SingleWindowInfo;->getWindowCount()I

    move-result v1

    .line 144
    .local v1, "count":I
    if-nez v0, :cond_0

    .line 145
    return-void

    .line 148
    :cond_0
    invoke-virtual {p2}, Lcom/miui/server/multisence/SingleWindowInfo;->getWindowForm()Lcom/miui/server/multisence/SingleWindowInfo$WindowForm;

    move-result-object v2

    sget-object v3, Lcom/miui/server/multisence/SingleWindowInfo$WindowForm;->MUTIL_FREEDOM_MINI:Lcom/miui/server/multisence/SingleWindowInfo$WindowForm;

    const/4 v4, 0x1

    if-ne v2, v3, :cond_1

    .line 149
    sget-object v2, Lcom/miui/server/multisence/SingleWindowInfo$ScheduleStatus;->VRS_SET:Lcom/miui/server/multisence/SingleWindowInfo$ScheduleStatus;

    invoke-virtual {p2, v2}, Lcom/miui/server/multisence/SingleWindowInfo;->setSchedStatus(Lcom/miui/server/multisence/SingleWindowInfo$ScheduleStatus;)Lcom/miui/server/multisence/SingleWindowInfo;

    .line 150
    if-ne v1, v4, :cond_1

    .line 151
    sget-object v2, Lcom/miui/server/multisence/SingleWindowInfo$ScheduleStatus;->DYN_FPS_SET:Lcom/miui/server/multisence/SingleWindowInfo$ScheduleStatus;

    invoke-virtual {p2, v2}, Lcom/miui/server/multisence/SingleWindowInfo;->setSchedStatus(Lcom/miui/server/multisence/SingleWindowInfo$ScheduleStatus;)Lcom/miui/server/multisence/SingleWindowInfo;

    .line 154
    :cond_1
    invoke-virtual {p2}, Lcom/miui/server/multisence/SingleWindowInfo;->isCovered()Z

    move-result v2

    if-eqz v2, :cond_3

    .line 155
    if-ne v1, v4, :cond_2

    .line 156
    sget-object v2, Lcom/miui/server/multisence/SingleWindowInfo$ScheduleStatus;->DYN_FPS_SET:Lcom/miui/server/multisence/SingleWindowInfo$ScheduleStatus;

    invoke-virtual {p2, v2}, Lcom/miui/server/multisence/SingleWindowInfo;->setSchedStatus(Lcom/miui/server/multisence/SingleWindowInfo$ScheduleStatus;)Lcom/miui/server/multisence/SingleWindowInfo;

    .line 158
    :cond_2
    invoke-virtual {p2}, Lcom/miui/server/multisence/SingleWindowInfo;->isVrsCovered()Z

    move-result v2

    if-eqz v2, :cond_3

    .line 159
    sget-object v2, Lcom/miui/server/multisence/SingleWindowInfo$ScheduleStatus;->VRS_SET:Lcom/miui/server/multisence/SingleWindowInfo$ScheduleStatus;

    invoke-virtual {p2, v2}, Lcom/miui/server/multisence/SingleWindowInfo;->setSchedStatus(Lcom/miui/server/multisence/SingleWindowInfo$ScheduleStatus;)Lcom/miui/server/multisence/SingleWindowInfo;

    .line 162
    :cond_3
    sget-object v2, Lcom/miui/server/multisence/SingleWindowInfo$WindowChangeState;->CHANGED:Lcom/miui/server/multisence/SingleWindowInfo$WindowChangeState;

    invoke-virtual {p2, v2}, Lcom/miui/server/multisence/SingleWindowInfo;->setChangeStatus(Lcom/miui/server/multisence/SingleWindowInfo$WindowChangeState;)Lcom/miui/server/multisence/SingleWindowInfo;

    .line 163
    iget-object v2, p0, Lcom/miui/server/multisence/MultiSenceServicePolicy;->schedWindows:Ljava/util/Map;

    invoke-interface {v2, v0, p2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 164
    return-void
.end method


# virtual methods
.method public convertToArray(Ljava/util/List;)[[I
    .locals 6
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List<",
            "Landroid/graphics/Rect;",
            ">;)[[I"
        }
    .end annotation

    .line 406
    .local p1, "overLayList":Ljava/util/List;, "Ljava/util/List<Landroid/graphics/Rect;>;"
    invoke-interface {p1}, Ljava/util/List;->size()I

    move-result v0

    .line 407
    .local v0, "N":I
    const/4 v1, 0x4

    filled-new-array {v0, v1}, [I

    move-result-object v1

    sget-object v2, Ljava/lang/Integer;->TYPE:Ljava/lang/Class;

    invoke-static {v2, v1}, Ljava/lang/reflect/Array;->newInstance(Ljava/lang/Class;[I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, [[I

    .line 408
    .local v1, "arr":[[I
    const/4 v2, 0x0

    .local v2, "i":I
    :goto_0
    if-ge v2, v0, :cond_0

    .line 409
    aget-object v3, v1, v2

    invoke-interface {p1, v2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Landroid/graphics/Rect;

    iget v4, v4, Landroid/graphics/Rect;->left:I

    const/4 v5, 0x0

    aput v4, v3, v5

    .line 410
    aget-object v3, v1, v2

    invoke-interface {p1, v2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Landroid/graphics/Rect;

    iget v4, v4, Landroid/graphics/Rect;->top:I

    const/4 v5, 0x1

    aput v4, v3, v5

    .line 411
    aget-object v3, v1, v2

    invoke-interface {p1, v2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Landroid/graphics/Rect;

    iget v4, v4, Landroid/graphics/Rect;->right:I

    const/4 v5, 0x2

    aput v4, v3, v5

    .line 412
    aget-object v3, v1, v2

    invoke-interface {p1, v2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Landroid/graphics/Rect;

    iget v4, v4, Landroid/graphics/Rect;->bottom:I

    const/4 v5, 0x3

    aput v4, v3, v5

    .line 408
    add-int/lit8 v2, v2, 0x1

    goto :goto_0

    .line 414
    .end local v2    # "i":I
    :cond_0
    return-object v1
.end method

.method public doSched()V
    .locals 0

    .line 253
    invoke-direct {p0}, Lcom/miui/server/multisence/MultiSenceServicePolicy;->perfSenceSched()V

    .line 254
    invoke-direct {p0}, Lcom/miui/server/multisence/MultiSenceServicePolicy;->showSchedWindowInfo()V

    .line 256
    invoke-direct {p0}, Lcom/miui/server/multisence/MultiSenceServicePolicy;->singleAppSched()V

    .line 257
    return-void
.end method

.method public dynamicSenceSchedDefault(I[IZLjava/lang/String;)V
    .locals 4
    .param p1, "uid"    # I
    .param p2, "tids"    # [I
    .param p3, "isStarted"    # Z
    .param p4, "packageName"    # Ljava/lang/String;

    .line 709
    iput-boolean p3, p0, Lcom/miui/server/multisence/MultiSenceServicePolicy;->isDynamicSenceStarting:Z

    .line 710
    iput-object p4, p0, Lcom/miui/server/multisence/MultiSenceServicePolicy;->dynamicSencePackage:Ljava/lang/String;

    .line 711
    invoke-direct {p0}, Lcom/miui/server/multisence/MultiSenceServicePolicy;->calculateFocusAndPolicy()V

    .line 712
    if-eqz p3, :cond_1

    .line 713
    invoke-static {}, Lcom/android/server/am/MiuiBoosterUtilsStub;->getInstance()Lcom/android/server/am/MiuiBoosterUtilsStub;

    move-result-object v0

    sget v1, Lcom/miui/server/multisence/MultiSenceServicePolicy;->DEFAULT_PRIORITY_TIMEOUT:I

    sget v2, Lcom/miui/server/multisence/MultiSenceServicePolicy;->DEFAULT_PRIORITY_LEVEL:I

    invoke-interface {v0, p1, p2, v1, v2}, Lcom/android/server/am/MiuiBoosterUtilsStub;->requestThreadPriority(I[III)I

    move-result v0

    .line 715
    .local v0, "requsetId":I
    iget-object v1, p0, Lcom/miui/server/multisence/MultiSenceServicePolicy;->mDefaultRequestIds:Ljava/util/ArrayList;

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/util/ArrayList;->contains(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_0

    .line 716
    iget-object v1, p0, Lcom/miui/server/multisence/MultiSenceServicePolicy;->mDefaultRequestIds:Ljava/util/ArrayList;

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 717
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Default requestId gains: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {p0, v1}, Lcom/miui/server/multisence/MultiSenceServicePolicy;->LOG_IF_DEBUG(Ljava/lang/String;)V

    .line 719
    .end local v0    # "requsetId":I
    :cond_0
    goto :goto_1

    .line 720
    :cond_1
    iget-object v0, p0, Lcom/miui/server/multisence/MultiSenceServicePolicy;->mDefaultRequestIds:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_3

    .line 721
    iget-object v0, p0, Lcom/miui/server/multisence/MultiSenceServicePolicy;->mDefaultRequestIds:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v0

    :goto_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_2

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/Integer;

    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v1

    .line 722
    .local v1, "defaultRequestId":I
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "defaultRequestId cancel: "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-direct {p0, v2}, Lcom/miui/server/multisence/MultiSenceServicePolicy;->LOG_IF_DEBUG(Ljava/lang/String;)V

    .line 723
    invoke-static {}, Lcom/android/server/am/MiuiBoosterUtilsStub;->getInstance()Lcom/android/server/am/MiuiBoosterUtilsStub;

    move-result-object v2

    invoke-interface {v2, p1, v1}, Lcom/android/server/am/MiuiBoosterUtilsStub;->cancelThreadPriority(II)V

    .line 724
    .end local v1    # "defaultRequestId":I
    goto :goto_0

    .line 725
    :cond_2
    iget-object v0, p0, Lcom/miui/server/multisence/MultiSenceServicePolicy;->mDefaultRequestIds:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->clear()V

    .line 728
    :cond_3
    :goto_1
    return-void
.end method

.method public dynamicSenceSchedFloatingWindowMove(I[IZ)V
    .locals 3
    .param p1, "uid"    # I
    .param p2, "tids"    # [I
    .param p3, "isStarted"    # Z

    .line 683
    invoke-direct {p0}, Lcom/miui/server/multisence/MultiSenceServicePolicy;->calculateFocusAndPolicy()V

    .line 684
    const/4 v0, -0x1

    if-eqz p3, :cond_1

    .line 685
    iget v1, p0, Lcom/miui/server/multisence/MultiSenceServicePolicy;->mFWRequestId:I

    if-ne v1, v0, :cond_0

    .line 686
    sget v1, Lcom/miui/server/multisence/MultiSenceServicePolicy;->FW_PRIORITY_TIMEOUT:I

    sget v2, Lcom/miui/server/multisence/MultiSenceServicePolicy;->FW_PRIORITY_LEVEL:I

    invoke-direct {p0, p1, p2, v1, v2}, Lcom/miui/server/multisence/MultiSenceServicePolicy;->requestThreadPriority(I[III)I

    move-result v1

    iput v1, p0, Lcom/miui/server/multisence/MultiSenceServicePolicy;->mFWRequestId:I

    .line 688
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "FWMoveRequestId gains: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget v2, p0, Lcom/miui/server/multisence/MultiSenceServicePolicy;->mFWRequestId:I

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {p0, v1}, Lcom/miui/server/multisence/MultiSenceServicePolicy;->LOG_IF_DEBUG(Ljava/lang/String;)V

    .line 691
    :cond_0
    iget v1, p0, Lcom/miui/server/multisence/MultiSenceServicePolicy;->mFWBindCoreRequestId:I

    if-ne v1, v0, :cond_3

    .line 692
    sget v0, Lcom/miui/server/multisence/MultiSenceServicePolicy;->FW_PRIORITY_TIMEOUT:I

    const/4 v1, 0x4

    invoke-direct {p0, p1, p2, v0, v1}, Lcom/miui/server/multisence/MultiSenceServicePolicy;->requestBindCore(I[III)I

    move-result v0

    iput v0, p0, Lcom/miui/server/multisence/MultiSenceServicePolicy;->mFWBindCoreRequestId:I

    .line 693
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "FWBindCoreRequestId gains: "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget v1, p0, Lcom/miui/server/multisence/MultiSenceServicePolicy;->mFWBindCoreRequestId:I

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-direct {p0, v0}, Lcom/miui/server/multisence/MultiSenceServicePolicy;->LOG_IF_DEBUG(Ljava/lang/String;)V

    goto :goto_0

    .line 696
    :cond_1
    iget v1, p0, Lcom/miui/server/multisence/MultiSenceServicePolicy;->mFWRequestId:I

    if-eq v1, v0, :cond_2

    .line 697
    invoke-static {}, Lcom/android/server/am/MiuiBoosterUtilsStub;->getInstance()Lcom/android/server/am/MiuiBoosterUtilsStub;

    move-result-object v1

    iget v2, p0, Lcom/miui/server/multisence/MultiSenceServicePolicy;->mFWRequestId:I

    invoke-interface {v1, p1, v2}, Lcom/android/server/am/MiuiBoosterUtilsStub;->cancelThreadPriority(II)V

    .line 698
    iput v0, p0, Lcom/miui/server/multisence/MultiSenceServicePolicy;->mFWRequestId:I

    .line 701
    :cond_2
    iget v1, p0, Lcom/miui/server/multisence/MultiSenceServicePolicy;->mFWBindCoreRequestId:I

    if-eq v1, v0, :cond_3

    .line 702
    invoke-static {}, Lcom/android/server/am/MiuiBoosterUtilsStub;->getInstance()Lcom/android/server/am/MiuiBoosterUtilsStub;

    move-result-object v1

    iget v2, p0, Lcom/miui/server/multisence/MultiSenceServicePolicy;->mFWBindCoreRequestId:I

    invoke-interface {v1, p1, v2}, Lcom/android/server/am/MiuiBoosterUtilsStub;->cancelBindCore(II)V

    .line 703
    iput v0, p0, Lcom/miui/server/multisence/MultiSenceServicePolicy;->mFWBindCoreRequestId:I

    .line 706
    :cond_3
    :goto_0
    return-void
.end method

.method public dynamicSenceSchedFreeformActionElude(I[IZLjava/lang/String;)V
    .locals 3
    .param p1, "uid"    # I
    .param p2, "tids"    # [I
    .param p3, "isStarted"    # Z
    .param p4, "packageName"    # Ljava/lang/String;

    .line 602
    iput-boolean p3, p0, Lcom/miui/server/multisence/MultiSenceServicePolicy;->isDynamicSenceStarting:Z

    .line 603
    iput-object p4, p0, Lcom/miui/server/multisence/MultiSenceServicePolicy;->dynamicSencePackage:Ljava/lang/String;

    .line 604
    invoke-direct {p0}, Lcom/miui/server/multisence/MultiSenceServicePolicy;->calculateFocusAndPolicy()V

    .line 605
    const/4 v0, -0x1

    if-eqz p3, :cond_1

    .line 606
    iget v1, p0, Lcom/miui/server/multisence/MultiSenceServicePolicy;->mFreeformEludeRequestId:I

    if-ne v1, v0, :cond_0

    .line 607
    sget v1, Lcom/miui/server/multisence/MultiSenceServicePolicy;->FREEFORM_ELUDE_TIMOUT:I

    sget v2, Lcom/miui/server/multisence/MultiSenceServicePolicy;->FREEFORM_ELUDE_PRIORITY_LEVEL:I

    invoke-direct {p0, p1, p2, v1, v2}, Lcom/miui/server/multisence/MultiSenceServicePolicy;->requestThreadPriority(I[III)I

    move-result v1

    iput v1, p0, Lcom/miui/server/multisence/MultiSenceServicePolicy;->mFreeformEludeRequestId:I

    .line 608
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "freeform-elude proi requset id gains: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget v2, p0, Lcom/miui/server/multisence/MultiSenceServicePolicy;->mFreeformEludeRequestId:I

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {p0, v1}, Lcom/miui/server/multisence/MultiSenceServicePolicy;->LOG_IF_DEBUG(Ljava/lang/String;)V

    .line 611
    :cond_0
    iget v1, p0, Lcom/miui/server/multisence/MultiSenceServicePolicy;->mFreeformEludeBindCoreRequestId:I

    if-ne v1, v0, :cond_3

    .line 612
    sget v0, Lcom/miui/server/multisence/MultiSenceServicePolicy;->FREEFORM_ELUDE_TIMOUT:I

    const/4 v1, 0x4

    invoke-direct {p0, p1, p2, v0, v1}, Lcom/miui/server/multisence/MultiSenceServicePolicy;->requestBindCore(I[III)I

    move-result v0

    iput v0, p0, Lcom/miui/server/multisence/MultiSenceServicePolicy;->mFreeformEludeBindCoreRequestId:I

    .line 613
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "freeform-elude bindcore requset id gains: "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget v1, p0, Lcom/miui/server/multisence/MultiSenceServicePolicy;->mFreeformEludeBindCoreRequestId:I

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-direct {p0, v0}, Lcom/miui/server/multisence/MultiSenceServicePolicy;->LOG_IF_DEBUG(Ljava/lang/String;)V

    goto :goto_0

    .line 616
    :cond_1
    iget v1, p0, Lcom/miui/server/multisence/MultiSenceServicePolicy;->mFreeformEludeRequestId:I

    if-eq v1, v0, :cond_2

    .line 617
    invoke-static {}, Lcom/android/server/am/MiuiBoosterUtilsStub;->getInstance()Lcom/android/server/am/MiuiBoosterUtilsStub;

    move-result-object v1

    iget v2, p0, Lcom/miui/server/multisence/MultiSenceServicePolicy;->mFreeformEludeRequestId:I

    invoke-interface {v1, p1, v2}, Lcom/android/server/am/MiuiBoosterUtilsStub;->cancelThreadPriority(II)V

    .line 618
    iput v0, p0, Lcom/miui/server/multisence/MultiSenceServicePolicy;->mFreeformEludeRequestId:I

    .line 621
    :cond_2
    iget v1, p0, Lcom/miui/server/multisence/MultiSenceServicePolicy;->mFreeformEludeBindCoreRequestId:I

    if-eq v1, v0, :cond_3

    .line 622
    invoke-static {}, Lcom/android/server/am/MiuiBoosterUtilsStub;->getInstance()Lcom/android/server/am/MiuiBoosterUtilsStub;

    move-result-object v1

    iget v2, p0, Lcom/miui/server/multisence/MultiSenceServicePolicy;->mFreeformEludeBindCoreRequestId:I

    invoke-interface {v1, p1, v2}, Lcom/android/server/am/MiuiBoosterUtilsStub;->cancelBindCore(II)V

    .line 623
    iput v0, p0, Lcom/miui/server/multisence/MultiSenceServicePolicy;->mFreeformEludeBindCoreRequestId:I

    .line 626
    :cond_3
    :goto_0
    return-void
.end method

.method public dynamicSenceSchedFreeformActionEnterFullScreen(I[IZLjava/lang/String;)V
    .locals 3
    .param p1, "uid"    # I
    .param p2, "tids"    # [I
    .param p3, "isStarted"    # Z
    .param p4, "packageName"    # Ljava/lang/String;

    .line 575
    iput-boolean p3, p0, Lcom/miui/server/multisence/MultiSenceServicePolicy;->isDynamicSenceStarting:Z

    .line 576
    iput-object p4, p0, Lcom/miui/server/multisence/MultiSenceServicePolicy;->dynamicSencePackage:Ljava/lang/String;

    .line 577
    invoke-direct {p0}, Lcom/miui/server/multisence/MultiSenceServicePolicy;->calculateFocusAndPolicy()V

    .line 578
    const/4 v0, -0x1

    if-eqz p3, :cond_1

    .line 579
    iget v1, p0, Lcom/miui/server/multisence/MultiSenceServicePolicy;->mFreeformEnterFullScreenRequestId:I

    if-ne v1, v0, :cond_0

    .line 580
    sget v1, Lcom/miui/server/multisence/MultiSenceServicePolicy;->FREEFORM_ENTERFULLSCREEN_PRIORITY_TIMEOUT:I

    sget v2, Lcom/miui/server/multisence/MultiSenceServicePolicy;->FREEFORM_ENTERFULLSCREEN_PRIORITY_LEVEL:I

    invoke-direct {p0, p1, p2, v1, v2}, Lcom/miui/server/multisence/MultiSenceServicePolicy;->requestThreadPriority(I[III)I

    move-result v1

    iput v1, p0, Lcom/miui/server/multisence/MultiSenceServicePolicy;->mFreeformEnterFullScreenRequestId:I

    .line 581
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "FreeformEnterFullScreenRequestId gains: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget v2, p0, Lcom/miui/server/multisence/MultiSenceServicePolicy;->mFreeformEnterFullScreenRequestId:I

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {p0, v1}, Lcom/miui/server/multisence/MultiSenceServicePolicy;->LOG_IF_DEBUG(Ljava/lang/String;)V

    .line 584
    :cond_0
    iget v1, p0, Lcom/miui/server/multisence/MultiSenceServicePolicy;->mFreeformEnterFullScreenBindCoreRequestId:I

    if-ne v1, v0, :cond_3

    .line 585
    sget v0, Lcom/miui/server/multisence/MultiSenceServicePolicy;->FREEFORM_ENTERFULLSCREEN_PRIORITY_TIMEOUT:I

    const/4 v1, 0x4

    invoke-direct {p0, p1, p2, v0, v1}, Lcom/miui/server/multisence/MultiSenceServicePolicy;->requestBindCore(I[III)I

    move-result v0

    iput v0, p0, Lcom/miui/server/multisence/MultiSenceServicePolicy;->mFreeformEnterFullScreenBindCoreRequestId:I

    .line 586
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "FreeformEnterFullScreenBindCoreRequestId gains: "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget v1, p0, Lcom/miui/server/multisence/MultiSenceServicePolicy;->mFreeformEnterFullScreenBindCoreRequestId:I

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-direct {p0, v0}, Lcom/miui/server/multisence/MultiSenceServicePolicy;->LOG_IF_DEBUG(Ljava/lang/String;)V

    goto :goto_0

    .line 589
    :cond_1
    iget v1, p0, Lcom/miui/server/multisence/MultiSenceServicePolicy;->mFreeformEnterFullScreenRequestId:I

    if-eq v1, v0, :cond_2

    .line 590
    invoke-static {}, Lcom/android/server/am/MiuiBoosterUtilsStub;->getInstance()Lcom/android/server/am/MiuiBoosterUtilsStub;

    move-result-object v1

    iget v2, p0, Lcom/miui/server/multisence/MultiSenceServicePolicy;->mFreeformEnterFullScreenRequestId:I

    invoke-interface {v1, p1, v2}, Lcom/android/server/am/MiuiBoosterUtilsStub;->cancelThreadPriority(II)V

    .line 591
    iput v0, p0, Lcom/miui/server/multisence/MultiSenceServicePolicy;->mFreeformEnterFullScreenRequestId:I

    .line 594
    :cond_2
    iget v1, p0, Lcom/miui/server/multisence/MultiSenceServicePolicy;->mFreeformEnterFullScreenBindCoreRequestId:I

    if-eq v1, v0, :cond_3

    .line 595
    invoke-static {}, Lcom/android/server/am/MiuiBoosterUtilsStub;->getInstance()Lcom/android/server/am/MiuiBoosterUtilsStub;

    move-result-object v1

    iget v2, p0, Lcom/miui/server/multisence/MultiSenceServicePolicy;->mFreeformEnterFullScreenBindCoreRequestId:I

    invoke-interface {v1, p1, v2}, Lcom/android/server/am/MiuiBoosterUtilsStub;->cancelBindCore(II)V

    .line 596
    iput v0, p0, Lcom/miui/server/multisence/MultiSenceServicePolicy;->mFreeformEnterFullScreenBindCoreRequestId:I

    .line 599
    :cond_3
    :goto_0
    return-void
.end method

.method public dynamicSenceSchedFreeformActionMove(I[IZLjava/lang/String;)V
    .locals 3
    .param p1, "uid"    # I
    .param p2, "tids"    # [I
    .param p3, "isStarted"    # Z
    .param p4, "packageName"    # Ljava/lang/String;

    .line 521
    iput-boolean p3, p0, Lcom/miui/server/multisence/MultiSenceServicePolicy;->isDynamicSenceStarting:Z

    .line 522
    iput-object p4, p0, Lcom/miui/server/multisence/MultiSenceServicePolicy;->dynamicSencePackage:Ljava/lang/String;

    .line 523
    invoke-direct {p0}, Lcom/miui/server/multisence/MultiSenceServicePolicy;->calculateFocusAndPolicy()V

    .line 524
    const/4 v0, -0x1

    if-eqz p3, :cond_1

    .line 525
    iget v1, p0, Lcom/miui/server/multisence/MultiSenceServicePolicy;->mFreeformMoveRequestId:I

    if-ne v1, v0, :cond_0

    .line 526
    sget v1, Lcom/miui/server/multisence/MultiSenceServicePolicy;->FREEFORM_MOVE_PRIORITY_TIMEOUT:I

    sget v2, Lcom/miui/server/multisence/MultiSenceServicePolicy;->FREEFORM_MOVE_PRIORITY_LEVEL:I

    invoke-direct {p0, p1, p2, v1, v2}, Lcom/miui/server/multisence/MultiSenceServicePolicy;->requestThreadPriority(I[III)I

    move-result v1

    iput v1, p0, Lcom/miui/server/multisence/MultiSenceServicePolicy;->mFreeformMoveRequestId:I

    .line 527
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "FreeformMoveRequestId gains: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget v2, p0, Lcom/miui/server/multisence/MultiSenceServicePolicy;->mFreeformMoveRequestId:I

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {p0, v1}, Lcom/miui/server/multisence/MultiSenceServicePolicy;->LOG_IF_DEBUG(Ljava/lang/String;)V

    .line 530
    :cond_0
    iget v1, p0, Lcom/miui/server/multisence/MultiSenceServicePolicy;->mFreeformMoveBindCoreRequestId:I

    if-ne v1, v0, :cond_3

    .line 531
    sget v0, Lcom/miui/server/multisence/MultiSenceServicePolicy;->FREEFORM_MOVE_PRIORITY_TIMEOUT:I

    const/4 v1, 0x4

    invoke-direct {p0, p1, p2, v0, v1}, Lcom/miui/server/multisence/MultiSenceServicePolicy;->requestBindCore(I[III)I

    move-result v0

    iput v0, p0, Lcom/miui/server/multisence/MultiSenceServicePolicy;->mFreeformMoveBindCoreRequestId:I

    .line 532
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "FreeformMoveBindCoreRequestId gains: "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget v1, p0, Lcom/miui/server/multisence/MultiSenceServicePolicy;->mFreeformMoveBindCoreRequestId:I

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-direct {p0, v0}, Lcom/miui/server/multisence/MultiSenceServicePolicy;->LOG_IF_DEBUG(Ljava/lang/String;)V

    goto :goto_0

    .line 535
    :cond_1
    iget v1, p0, Lcom/miui/server/multisence/MultiSenceServicePolicy;->mFreeformMoveRequestId:I

    if-eq v1, v0, :cond_2

    .line 536
    invoke-static {}, Lcom/android/server/am/MiuiBoosterUtilsStub;->getInstance()Lcom/android/server/am/MiuiBoosterUtilsStub;

    move-result-object v1

    iget v2, p0, Lcom/miui/server/multisence/MultiSenceServicePolicy;->mFreeformMoveRequestId:I

    invoke-interface {v1, p1, v2}, Lcom/android/server/am/MiuiBoosterUtilsStub;->cancelThreadPriority(II)V

    .line 537
    iput v0, p0, Lcom/miui/server/multisence/MultiSenceServicePolicy;->mFreeformMoveRequestId:I

    .line 540
    :cond_2
    iget v1, p0, Lcom/miui/server/multisence/MultiSenceServicePolicy;->mFreeformMoveBindCoreRequestId:I

    if-eq v1, v0, :cond_3

    .line 541
    invoke-static {}, Lcom/android/server/am/MiuiBoosterUtilsStub;->getInstance()Lcom/android/server/am/MiuiBoosterUtilsStub;

    move-result-object v1

    iget v2, p0, Lcom/miui/server/multisence/MultiSenceServicePolicy;->mFreeformMoveBindCoreRequestId:I

    invoke-interface {v1, p1, v2}, Lcom/android/server/am/MiuiBoosterUtilsStub;->cancelBindCore(II)V

    .line 542
    iput v0, p0, Lcom/miui/server/multisence/MultiSenceServicePolicy;->mFreeformMoveBindCoreRequestId:I

    .line 545
    :cond_3
    :goto_0
    return-void
.end method

.method public dynamicSenceSchedFreeformActionResize(I[IZLjava/lang/String;)V
    .locals 3
    .param p1, "uid"    # I
    .param p2, "tids"    # [I
    .param p3, "isStarted"    # Z
    .param p4, "packageName"    # Ljava/lang/String;

    .line 548
    iput-boolean p3, p0, Lcom/miui/server/multisence/MultiSenceServicePolicy;->isDynamicSenceStarting:Z

    .line 549
    iput-object p4, p0, Lcom/miui/server/multisence/MultiSenceServicePolicy;->dynamicSencePackage:Ljava/lang/String;

    .line 550
    invoke-direct {p0}, Lcom/miui/server/multisence/MultiSenceServicePolicy;->calculateFocusAndPolicy()V

    .line 551
    const/4 v0, -0x1

    if-eqz p3, :cond_1

    .line 552
    iget v1, p0, Lcom/miui/server/multisence/MultiSenceServicePolicy;->mFreeformResizeRequestId:I

    if-ne v1, v0, :cond_0

    .line 553
    sget v1, Lcom/miui/server/multisence/MultiSenceServicePolicy;->FREEFORM_RESIZE_PRIORITY_TIMEOUT:I

    sget v2, Lcom/miui/server/multisence/MultiSenceServicePolicy;->FREEFORM_RESIZE_PRIORITY_LEVEL:I

    invoke-direct {p0, p1, p2, v1, v2}, Lcom/miui/server/multisence/MultiSenceServicePolicy;->requestThreadPriority(I[III)I

    move-result v1

    iput v1, p0, Lcom/miui/server/multisence/MultiSenceServicePolicy;->mFreeformResizeRequestId:I

    .line 554
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "FreeformResizeRequestId gains: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget v2, p0, Lcom/miui/server/multisence/MultiSenceServicePolicy;->mFreeformResizeRequestId:I

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {p0, v1}, Lcom/miui/server/multisence/MultiSenceServicePolicy;->LOG_IF_DEBUG(Ljava/lang/String;)V

    .line 557
    :cond_0
    iget v1, p0, Lcom/miui/server/multisence/MultiSenceServicePolicy;->mFreeformResizeBindCoreRequestId:I

    if-ne v1, v0, :cond_3

    .line 558
    sget v0, Lcom/miui/server/multisence/MultiSenceServicePolicy;->FREEFORM_RESIZE_PRIORITY_TIMEOUT:I

    const/4 v1, 0x4

    invoke-direct {p0, p1, p2, v0, v1}, Lcom/miui/server/multisence/MultiSenceServicePolicy;->requestBindCore(I[III)I

    move-result v0

    iput v0, p0, Lcom/miui/server/multisence/MultiSenceServicePolicy;->mFreeformResizeBindCoreRequestId:I

    .line 559
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "FreeformResizeBindCoreRequestId gains: "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget v1, p0, Lcom/miui/server/multisence/MultiSenceServicePolicy;->mFreeformResizeBindCoreRequestId:I

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-direct {p0, v0}, Lcom/miui/server/multisence/MultiSenceServicePolicy;->LOG_IF_DEBUG(Ljava/lang/String;)V

    goto :goto_0

    .line 562
    :cond_1
    iget v1, p0, Lcom/miui/server/multisence/MultiSenceServicePolicy;->mFreeformResizeRequestId:I

    if-eq v1, v0, :cond_2

    .line 563
    invoke-static {}, Lcom/android/server/am/MiuiBoosterUtilsStub;->getInstance()Lcom/android/server/am/MiuiBoosterUtilsStub;

    move-result-object v1

    iget v2, p0, Lcom/miui/server/multisence/MultiSenceServicePolicy;->mFreeformResizeRequestId:I

    invoke-interface {v1, p1, v2}, Lcom/android/server/am/MiuiBoosterUtilsStub;->cancelThreadPriority(II)V

    .line 564
    iput v0, p0, Lcom/miui/server/multisence/MultiSenceServicePolicy;->mFreeformResizeRequestId:I

    .line 567
    :cond_2
    iget v1, p0, Lcom/miui/server/multisence/MultiSenceServicePolicy;->mFreeformResizeBindCoreRequestId:I

    if-eq v1, v0, :cond_3

    .line 568
    invoke-static {}, Lcom/android/server/am/MiuiBoosterUtilsStub;->getInstance()Lcom/android/server/am/MiuiBoosterUtilsStub;

    move-result-object v1

    iget v2, p0, Lcom/miui/server/multisence/MultiSenceServicePolicy;->mFreeformResizeBindCoreRequestId:I

    invoke-interface {v1, p1, v2}, Lcom/android/server/am/MiuiBoosterUtilsStub;->cancelBindCore(II)V

    .line 569
    iput v0, p0, Lcom/miui/server/multisence/MultiSenceServicePolicy;->mFreeformResizeBindCoreRequestId:I

    .line 572
    :cond_3
    :goto_0
    return-void
.end method

.method public dynamicSenceSchedMWSActionMove(I[IZLjava/lang/String;)V
    .locals 3
    .param p1, "uid"    # I
    .param p2, "tids"    # [I
    .param p3, "isStarted"    # Z
    .param p4, "packageName"    # Ljava/lang/String;

    .line 656
    iput-boolean p3, p0, Lcom/miui/server/multisence/MultiSenceServicePolicy;->isDynamicSenceStarting:Z

    .line 657
    iput-object p4, p0, Lcom/miui/server/multisence/MultiSenceServicePolicy;->dynamicSencePackage:Ljava/lang/String;

    .line 658
    invoke-direct {p0}, Lcom/miui/server/multisence/MultiSenceServicePolicy;->calculateFocusAndPolicy()V

    .line 659
    const/4 v0, -0x1

    if-eqz p3, :cond_1

    .line 660
    iget v1, p0, Lcom/miui/server/multisence/MultiSenceServicePolicy;->mMWSRequestId:I

    if-ne v1, v0, :cond_0

    .line 661
    sget v1, Lcom/miui/server/multisence/MultiSenceServicePolicy;->MWS_PRIORITY_TIMEOUT:I

    sget v2, Lcom/miui/server/multisence/MultiSenceServicePolicy;->MWS_PRIORITY_LEVEL:I

    invoke-direct {p0, p1, p2, v1, v2}, Lcom/miui/server/multisence/MultiSenceServicePolicy;->requestThreadPriority(I[III)I

    move-result v1

    iput v1, p0, Lcom/miui/server/multisence/MultiSenceServicePolicy;->mMWSRequestId:I

    .line 662
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "MWSRequestId gains: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget v2, p0, Lcom/miui/server/multisence/MultiSenceServicePolicy;->mMWSRequestId:I

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {p0, v1}, Lcom/miui/server/multisence/MultiSenceServicePolicy;->LOG_IF_DEBUG(Ljava/lang/String;)V

    .line 665
    :cond_0
    iget v1, p0, Lcom/miui/server/multisence/MultiSenceServicePolicy;->mMWSBindCoreRequestId:I

    if-ne v1, v0, :cond_3

    .line 666
    sget v0, Lcom/miui/server/multisence/MultiSenceServicePolicy;->MWS_PRIORITY_TIMEOUT:I

    const/4 v1, 0x4

    invoke-direct {p0, p1, p2, v0, v1}, Lcom/miui/server/multisence/MultiSenceServicePolicy;->requestBindCore(I[III)I

    move-result v0

    iput v0, p0, Lcom/miui/server/multisence/MultiSenceServicePolicy;->mMWSBindCoreRequestId:I

    .line 667
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "MWSBindCoreRequestId gains: "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget v1, p0, Lcom/miui/server/multisence/MultiSenceServicePolicy;->mMWSBindCoreRequestId:I

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-direct {p0, v0}, Lcom/miui/server/multisence/MultiSenceServicePolicy;->LOG_IF_DEBUG(Ljava/lang/String;)V

    goto :goto_0

    .line 670
    :cond_1
    iget v1, p0, Lcom/miui/server/multisence/MultiSenceServicePolicy;->mMWSRequestId:I

    if-eq v1, v0, :cond_2

    .line 671
    invoke-static {}, Lcom/android/server/am/MiuiBoosterUtilsStub;->getInstance()Lcom/android/server/am/MiuiBoosterUtilsStub;

    move-result-object v1

    iget v2, p0, Lcom/miui/server/multisence/MultiSenceServicePolicy;->mMWSRequestId:I

    invoke-interface {v1, p1, v2}, Lcom/android/server/am/MiuiBoosterUtilsStub;->cancelThreadPriority(II)V

    .line 672
    iput v0, p0, Lcom/miui/server/multisence/MultiSenceServicePolicy;->mMWSRequestId:I

    .line 675
    :cond_2
    iget v1, p0, Lcom/miui/server/multisence/MultiSenceServicePolicy;->mMWSBindCoreRequestId:I

    if-eq v1, v0, :cond_3

    .line 676
    invoke-static {}, Lcom/android/server/am/MiuiBoosterUtilsStub;->getInstance()Lcom/android/server/am/MiuiBoosterUtilsStub;

    move-result-object v1

    iget v2, p0, Lcom/miui/server/multisence/MultiSenceServicePolicy;->mMWSBindCoreRequestId:I

    invoke-interface {v1, p1, v2}, Lcom/android/server/am/MiuiBoosterUtilsStub;->cancelBindCore(II)V

    .line 677
    iput v0, p0, Lcom/miui/server/multisence/MultiSenceServicePolicy;->mMWSBindCoreRequestId:I

    .line 680
    :cond_3
    :goto_0
    return-void
.end method

.method public dynamicSenceSchedSplitscreenctionDivider(I[IZLjava/lang/String;)V
    .locals 3
    .param p1, "uid"    # I
    .param p2, "tids"    # [I
    .param p3, "isStarted"    # Z
    .param p4, "packageName"    # Ljava/lang/String;

    .line 629
    iput-boolean p3, p0, Lcom/miui/server/multisence/MultiSenceServicePolicy;->isDynamicSenceStarting:Z

    .line 630
    iput-object p4, p0, Lcom/miui/server/multisence/MultiSenceServicePolicy;->dynamicSencePackage:Ljava/lang/String;

    .line 631
    invoke-direct {p0}, Lcom/miui/server/multisence/MultiSenceServicePolicy;->calculateFocusAndPolicy()V

    .line 632
    const/4 v0, -0x1

    if-eqz p3, :cond_1

    .line 633
    iget v1, p0, Lcom/miui/server/multisence/MultiSenceServicePolicy;->mSplitscreenRequestId:I

    if-ne v1, v0, :cond_0

    .line 634
    sget v1, Lcom/miui/server/multisence/MultiSenceServicePolicy;->SPLITSCREEN_PRIORITY_TIMEOUT:I

    sget v2, Lcom/miui/server/multisence/MultiSenceServicePolicy;->SPLITSCREEN_PRIORITY_LEVEL:I

    invoke-direct {p0, p1, p2, v1, v2}, Lcom/miui/server/multisence/MultiSenceServicePolicy;->requestThreadPriority(I[III)I

    move-result v1

    iput v1, p0, Lcom/miui/server/multisence/MultiSenceServicePolicy;->mSplitscreenRequestId:I

    .line 635
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "SplitscreenRequestId gains: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget v2, p0, Lcom/miui/server/multisence/MultiSenceServicePolicy;->mSplitscreenRequestId:I

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {p0, v1}, Lcom/miui/server/multisence/MultiSenceServicePolicy;->LOG_IF_DEBUG(Ljava/lang/String;)V

    .line 638
    :cond_0
    iget v1, p0, Lcom/miui/server/multisence/MultiSenceServicePolicy;->mSplitscreenBindCoreRequestId:I

    if-ne v1, v0, :cond_3

    .line 639
    sget v0, Lcom/miui/server/multisence/MultiSenceServicePolicy;->SPLITSCREEN_PRIORITY_TIMEOUT:I

    const/4 v1, 0x4

    invoke-direct {p0, p1, p2, v0, v1}, Lcom/miui/server/multisence/MultiSenceServicePolicy;->requestBindCore(I[III)I

    move-result v0

    iput v0, p0, Lcom/miui/server/multisence/MultiSenceServicePolicy;->mSplitscreenBindCoreRequestId:I

    .line 640
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "SplitscreenBindCoreRequestId gains: "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget v1, p0, Lcom/miui/server/multisence/MultiSenceServicePolicy;->mSplitscreenBindCoreRequestId:I

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-direct {p0, v0}, Lcom/miui/server/multisence/MultiSenceServicePolicy;->LOG_IF_DEBUG(Ljava/lang/String;)V

    goto :goto_0

    .line 643
    :cond_1
    iget v1, p0, Lcom/miui/server/multisence/MultiSenceServicePolicy;->mSplitscreenRequestId:I

    if-eq v1, v0, :cond_2

    .line 644
    invoke-static {}, Lcom/android/server/am/MiuiBoosterUtilsStub;->getInstance()Lcom/android/server/am/MiuiBoosterUtilsStub;

    move-result-object v1

    iget v2, p0, Lcom/miui/server/multisence/MultiSenceServicePolicy;->mSplitscreenRequestId:I

    invoke-interface {v1, p1, v2}, Lcom/android/server/am/MiuiBoosterUtilsStub;->cancelThreadPriority(II)V

    .line 645
    iput v0, p0, Lcom/miui/server/multisence/MultiSenceServicePolicy;->mSplitscreenRequestId:I

    .line 648
    :cond_2
    iget v1, p0, Lcom/miui/server/multisence/MultiSenceServicePolicy;->mSplitscreenBindCoreRequestId:I

    if-eq v1, v0, :cond_3

    .line 649
    invoke-static {}, Lcom/android/server/am/MiuiBoosterUtilsStub;->getInstance()Lcom/android/server/am/MiuiBoosterUtilsStub;

    move-result-object v1

    iget v2, p0, Lcom/miui/server/multisence/MultiSenceServicePolicy;->mSplitscreenBindCoreRequestId:I

    invoke-interface {v1, p1, v2}, Lcom/android/server/am/MiuiBoosterUtilsStub;->cancelBindCore(II)V

    .line 650
    iput v0, p0, Lcom/miui/server/multisence/MultiSenceServicePolicy;->mSplitscreenBindCoreRequestId:I

    .line 653
    :cond_3
    :goto_0
    return-void
.end method

.method public findOverlay(Landroid/graphics/Rect;Landroid/graphics/Rect;)Landroid/graphics/Rect;
    .locals 5
    .param p1, "rect1"    # Landroid/graphics/Rect;
    .param p2, "rect2"    # Landroid/graphics/Rect;

    .line 455
    iget v0, p1, Landroid/graphics/Rect;->left:I

    iget v1, p2, Landroid/graphics/Rect;->left:I

    invoke-static {v0, v1}, Ljava/lang/Math;->max(II)I

    move-result v0

    .line 456
    .local v0, "x1":I
    iget v1, p1, Landroid/graphics/Rect;->top:I

    iget v2, p2, Landroid/graphics/Rect;->top:I

    invoke-static {v1, v2}, Ljava/lang/Math;->max(II)I

    move-result v1

    .line 457
    .local v1, "y1":I
    iget v2, p1, Landroid/graphics/Rect;->right:I

    iget v3, p2, Landroid/graphics/Rect;->right:I

    invoke-static {v2, v3}, Ljava/lang/Math;->min(II)I

    move-result v2

    .line 458
    .local v2, "x2":I
    iget v3, p1, Landroid/graphics/Rect;->bottom:I

    iget v4, p2, Landroid/graphics/Rect;->bottom:I

    invoke-static {v3, v4}, Ljava/lang/Math;->min(II)I

    move-result v3

    .line 460
    .local v3, "y2":I
    if-ge v0, v2, :cond_0

    if-ge v1, v3, :cond_0

    .line 461
    new-instance v4, Landroid/graphics/Rect;

    invoke-direct {v4, v0, v1, v2, v3}, Landroid/graphics/Rect;-><init>(IIII)V

    return-object v4

    .line 463
    :cond_0
    const/4 v4, 0x0

    return-object v4
.end method

.method public getOverLayScale(ILjava/util/List;)F
    .locals 5
    .param p1, "curArea"    # I
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(I",
            "Ljava/util/List<",
            "Landroid/graphics/Rect;",
            ">;)F"
        }
    .end annotation

    .line 390
    .local p2, "overLayList":Ljava/util/List;, "Ljava/util/List<Landroid/graphics/Rect;>;"
    invoke-interface {p2}, Ljava/util/List;->size()I

    move-result v0

    .line 393
    .local v0, "N":I
    if-nez v0, :cond_0

    .line 394
    const/4 v1, 0x0

    .local v1, "overArea":I
    goto :goto_0

    .line 395
    .end local v1    # "overArea":I
    :cond_0
    const/4 v1, 0x1

    if-ne v0, v1, :cond_1

    .line 396
    const/4 v1, 0x0

    invoke-interface {p2, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Landroid/graphics/Rect;

    invoke-virtual {v2}, Landroid/graphics/Rect;->width()I

    move-result v2

    invoke-interface {p2, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/graphics/Rect;

    invoke-virtual {v1}, Landroid/graphics/Rect;->height()I

    move-result v1

    mul-int/2addr v1, v2

    .restart local v1    # "overArea":I
    goto :goto_0

    .line 398
    .end local v1    # "overArea":I
    :cond_1
    invoke-virtual {p0, p2}, Lcom/miui/server/multisence/MultiSenceServicePolicy;->convertToArray(Ljava/util/List;)[[I

    move-result-object v1

    .line 399
    .local v1, "overLayArray":[[I
    invoke-virtual {p0, v1}, Lcom/miui/server/multisence/MultiSenceServicePolicy;->rectangleArea([[I)I

    move-result v2

    move v1, v2

    .line 401
    .local v1, "overArea":I
    :goto_0
    sget-boolean v2, Lcom/miui/server/multisence/MultiSenceConfig;->DEBUG_POLICY_BASE:Z

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "curArea: "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, ", overArea: "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Lcom/miui/server/multisence/MultiSenceServiceUtils;->msLogD(ZLjava/lang/String;)V

    .line 402
    int-to-float v2, v1

    int-to-float v3, p1

    div-float/2addr v2, v3

    return v2
.end method

.method public limitFreeFormRes(Ljava/util/List;)V
    .locals 12
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List<",
            "Lcom/miui/server/multisence/SingleWindowInfo;",
            ">;)V"
        }
    .end annotation

    .line 349
    .local p1, "schedFreeForm":Ljava/util/List;, "Ljava/util/List<Lcom/miui/server/multisence/SingleWindowInfo;>;"
    sget-boolean v0, Lcom/miui/server/multisence/MultiSenceConfig;->POLICY_WINDOWSIZE_ENABLE:Z

    const/4 v1, 0x1

    if-nez v0, :cond_2

    .line 350
    sget-boolean v0, Lcom/miui/server/multisence/MultiSenceConfig;->DEBUG_POLICY_BASE:Z

    if-nez v0, :cond_1

    sget-boolean v0, Lcom/miui/server/multisence/MultiSenceConfig;->DEBUG_CONFIG:Z

    if-eqz v0, :cond_0

    goto :goto_0

    :cond_0
    const/4 v1, 0x0

    :cond_1
    :goto_0
    const-string/jumbo v0, "windowsize policy is not supported by multisence config"

    invoke-static {v1, v0}, Lcom/miui/server/multisence/MultiSenceServiceUtils;->msLogD(ZLjava/lang/String;)V

    .line 351
    return-void

    .line 354
    :cond_2
    invoke-static {p1}, Ljava/util/Collections;->sort(Ljava/util/List;)V

    .line 355
    invoke-interface {p1}, Ljava/util/List;->size()I

    move-result v0

    .line 356
    .local v0, "N":I
    const/4 v2, 0x0

    .local v2, "i":I
    :goto_1
    if-ge v2, v0, :cond_3

    .line 357
    sget-boolean v3, Lcom/miui/server/multisence/MultiSenceConfig;->DEBUG_POLICY_BASE:Z

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "schedFreeForm "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-interface {p1, v2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Lcom/miui/server/multisence/SingleWindowInfo;

    invoke-virtual {v5}, Lcom/miui/server/multisence/SingleWindowInfo;->getPackageName()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, " : "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-interface {p1, v2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v6

    check-cast v6, Lcom/miui/server/multisence/SingleWindowInfo;

    invoke-virtual {v6}, Lcom/miui/server/multisence/SingleWindowInfo;->getLayerOrder()I

    move-result v6

    invoke-virtual {v4, v6}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    .line 358
    invoke-interface {p1, v2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Lcom/miui/server/multisence/SingleWindowInfo;

    invoke-virtual {v5}, Lcom/miui/server/multisence/SingleWindowInfo;->getRectValue()Landroid/graphics/Rect;

    move-result-object v5

    invoke-virtual {v5}, Landroid/graphics/Rect;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    .line 357
    invoke-static {v3, v4}, Lcom/miui/server/multisence/MultiSenceServiceUtils;->msLogD(ZLjava/lang/String;)V

    .line 356
    add-int/lit8 v2, v2, 0x1

    goto :goto_1

    .line 360
    .end local v2    # "i":I
    :cond_3
    if-gt v0, v1, :cond_4

    return-void

    .line 361
    :cond_4
    const/4 v2, 0x0

    .restart local v2    # "i":I
    :goto_2
    add-int/lit8 v3, v0, -0x1

    if-ge v2, v3, :cond_9

    .line 362
    invoke-interface {p1, v2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/miui/server/multisence/SingleWindowInfo;

    .line 363
    .local v3, "app":Lcom/miui/server/multisence/SingleWindowInfo;
    invoke-virtual {v3}, Lcom/miui/server/multisence/SingleWindowInfo;->isHighPriority()Z

    move-result v4

    if-eqz v4, :cond_5

    goto/16 :goto_4

    .line 364
    :cond_5
    new-instance v4, Ljava/util/ArrayList;

    invoke-direct {v4}, Ljava/util/ArrayList;-><init>()V

    .line 365
    .local v4, "overLayList":Ljava/util/List;, "Ljava/util/List<Landroid/graphics/Rect;>;"
    invoke-virtual {v3}, Lcom/miui/server/multisence/SingleWindowInfo;->getRectValue()Landroid/graphics/Rect;

    move-result-object v5

    invoke-virtual {v5}, Landroid/graphics/Rect;->width()I

    move-result v5

    invoke-virtual {v3}, Lcom/miui/server/multisence/SingleWindowInfo;->getRectValue()Landroid/graphics/Rect;

    move-result-object v6

    invoke-virtual {v6}, Landroid/graphics/Rect;->height()I

    move-result v6

    mul-int/2addr v5, v6

    .line 367
    .local v5, "curArea":I
    add-int/lit8 v6, v2, 0x1

    .local v6, "j":I
    :goto_3
    if-ge v6, v0, :cond_7

    .line 368
    invoke-interface {p1, v6}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v7

    check-cast v7, Lcom/miui/server/multisence/SingleWindowInfo;

    .line 369
    .local v7, "app_tmp":Lcom/miui/server/multisence/SingleWindowInfo;
    invoke-virtual {v3}, Lcom/miui/server/multisence/SingleWindowInfo;->getRectValue()Landroid/graphics/Rect;

    move-result-object v8

    invoke-virtual {v7}, Lcom/miui/server/multisence/SingleWindowInfo;->getRectValue()Landroid/graphics/Rect;

    move-result-object v9

    invoke-virtual {p0, v8, v9}, Lcom/miui/server/multisence/MultiSenceServicePolicy;->findOverlay(Landroid/graphics/Rect;Landroid/graphics/Rect;)Landroid/graphics/Rect;

    move-result-object v8

    .line 370
    .local v8, "result":Landroid/graphics/Rect;
    if-eqz v8, :cond_6

    .line 371
    invoke-interface {v4, v8}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 372
    sget-boolean v9, Lcom/miui/server/multisence/MultiSenceConfig;->DEBUG_POLICY_BASE:Z

    new-instance v10, Ljava/lang/StringBuilder;

    invoke-direct {v10}, Ljava/lang/StringBuilder;-><init>()V

    const-string v11, "above layer is "

    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-virtual {v7}, Lcom/miui/server/multisence/SingleWindowInfo;->getPackageName()Ljava/lang/String;

    move-result-object v11

    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    const-string v11, ", overlay info "

    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-virtual {v8}, Landroid/graphics/Rect;->toString()Ljava/lang/String;

    move-result-object v11

    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-virtual {v10}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v10

    invoke-static {v9, v10}, Lcom/miui/server/multisence/MultiSenceServiceUtils;->msLogD(ZLjava/lang/String;)V

    .line 367
    .end local v7    # "app_tmp":Lcom/miui/server/multisence/SingleWindowInfo;
    .end local v8    # "result":Landroid/graphics/Rect;
    :cond_6
    add-int/lit8 v6, v6, 0x1

    goto :goto_3

    .line 376
    .end local v6    # "j":I
    :cond_7
    invoke-virtual {p0, v5, v4}, Lcom/miui/server/multisence/MultiSenceServicePolicy;->getOverLayScale(ILjava/util/List;)F

    move-result v6

    .line 377
    .local v6, "scale":F
    sget-boolean v7, Lcom/miui/server/multisence/MultiSenceConfig;->DEBUG_POLICY_BASE:Z

    new-instance v8, Ljava/lang/StringBuilder;

    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v3}, Lcom/miui/server/multisence/SingleWindowInfo;->getPackageName()Ljava/lang/String;

    move-result-object v9

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    const-string v9, " is covered : "

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    const/high16 v9, 0x42c80000    # 100.0f

    mul-float/2addr v9, v6

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;

    move-result-object v8

    const-string v9, "%"

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    invoke-static {v7, v8}, Lcom/miui/server/multisence/MultiSenceServiceUtils;->msLogD(ZLjava/lang/String;)V

    .line 379
    float-to-double v7, v6

    const-wide v9, 0x3feccccccccccccdL    # 0.9

    cmpl-double v7, v7, v9

    if-ltz v7, :cond_8

    const/high16 v7, 0x3f800000    # 1.0f

    cmpg-float v8, v6, v7

    if-gtz v8, :cond_8

    .line 380
    invoke-virtual {v3}, Lcom/miui/server/multisence/SingleWindowInfo;->getPackageName()Ljava/lang/String;

    move-result-object v8

    .line 381
    .local v8, "name":Ljava/lang/String;
    invoke-virtual {v3, v1}, Lcom/miui/server/multisence/SingleWindowInfo;->setCovered(Z)Lcom/miui/server/multisence/SingleWindowInfo;

    .line 382
    int-to-float v9, v5

    sub-float/2addr v7, v6

    mul-float/2addr v9, v7

    .line 383
    .local v9, "unCoveredArea":F
    sget-boolean v7, Lcom/miui/server/multisence/MultiSenceConfig;->DEBUG_POLICY_BASE:Z

    new-instance v10, Ljava/lang/StringBuilder;

    invoke-direct {v10}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v3}, Lcom/miui/server/multisence/SingleWindowInfo;->getPackageName()Ljava/lang/String;

    move-result-object v11

    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    const-string v11, " is uncovered : "

    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-virtual {v10, v9}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-virtual {v10}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v10

    invoke-static {v7, v10}, Lcom/miui/server/multisence/MultiSenceServiceUtils;->msLogD(ZLjava/lang/String;)V

    .line 384
    const v7, 0x47435000    # 50000.0f

    cmpg-float v7, v9, v7

    if-gez v7, :cond_8

    invoke-virtual {v3, v1}, Lcom/miui/server/multisence/SingleWindowInfo;->setVrsCovered(Z)Lcom/miui/server/multisence/SingleWindowInfo;

    .line 361
    .end local v3    # "app":Lcom/miui/server/multisence/SingleWindowInfo;
    .end local v4    # "overLayList":Ljava/util/List;, "Ljava/util/List<Landroid/graphics/Rect;>;"
    .end local v5    # "curArea":I
    .end local v6    # "scale":F
    .end local v8    # "name":Ljava/lang/String;
    .end local v9    # "unCoveredArea":F
    :cond_8
    add-int/lit8 v2, v2, 0x1

    goto/16 :goto_2

    .line 387
    .end local v2    # "i":I
    :cond_9
    :goto_4
    return-void
.end method

.method public rectangleArea([[I)I
    .locals 21
    .param p1, "rs"    # [[I

    .line 423
    move-object/from16 v0, p1

    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    .line 424
    .local v1, "list":Ljava/util/List;, "Ljava/util/List<Ljava/lang/Integer;>;"
    array-length v2, v0

    const/4 v3, 0x0

    move v4, v3

    :goto_0
    const/4 v5, 0x2

    if-ge v4, v2, :cond_0

    aget-object v6, v0, v4

    .line 425
    .local v6, "info":[I
    aget v7, v6, v3

    invoke-static {v7}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v7

    invoke-interface {v1, v7}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    aget v5, v6, v5

    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    invoke-interface {v1, v5}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 424
    .end local v6    # "info":[I
    add-int/lit8 v4, v4, 0x1

    goto :goto_0

    .line 427
    :cond_0
    invoke-static {v1}, Ljava/util/Collections;->sort(Ljava/util/List;)V

    .line 428
    const-wide/16 v6, 0x0

    .line 429
    .local v6, "ans":J
    const/4 v2, 0x1

    .local v2, "i":I
    :goto_1
    invoke-interface {v1}, Ljava/util/List;->size()I

    move-result v4

    if-ge v2, v4, :cond_7

    .line 430
    add-int/lit8 v4, v2, -0x1

    invoke-interface {v1, v4}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Ljava/lang/Integer;

    invoke-virtual {v4}, Ljava/lang/Integer;->intValue()I

    move-result v4

    .local v4, "a":I
    invoke-interface {v1, v2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v8

    check-cast v8, Ljava/lang/Integer;

    invoke-virtual {v8}, Ljava/lang/Integer;->intValue()I

    move-result v8

    .local v8, "b":I
    sub-int v9, v8, v4

    .line 431
    .local v9, "len":I
    if-nez v9, :cond_1

    goto/16 :goto_5

    .line 432
    :cond_1
    new-instance v10, Ljava/util/ArrayList;

    invoke-direct {v10}, Ljava/util/ArrayList;-><init>()V

    .line 433
    .local v10, "lines":Ljava/util/List;, "Ljava/util/List<[I>;"
    array-length v11, v0

    move v12, v3

    :goto_2
    const/4 v13, 0x1

    if-ge v12, v11, :cond_3

    aget-object v14, v0, v12

    .line 434
    .local v14, "info":[I
    aget v15, v14, v3

    if-gt v15, v4, :cond_2

    aget v15, v14, v5

    if-gt v8, v15, :cond_2

    aget v13, v14, v13

    const/4 v15, 0x3

    aget v15, v14, v15

    filled-new-array {v13, v15}, [I

    move-result-object v13

    invoke-interface {v10, v13}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 433
    .end local v14    # "info":[I
    :cond_2
    add-int/lit8 v12, v12, 0x1

    goto :goto_2

    .line 436
    :cond_3
    new-instance v11, Lcom/miui/server/multisence/MultiSenceServicePolicy$$ExternalSyntheticLambda0;

    invoke-direct {v11}, Lcom/miui/server/multisence/MultiSenceServicePolicy$$ExternalSyntheticLambda0;-><init>()V

    invoke-static {v10, v11}, Ljava/util/Collections;->sort(Ljava/util/List;Ljava/util/Comparator;)V

    .line 439
    const-wide/16 v11, 0x0

    .local v11, "tot":J
    const-wide/16 v14, -0x1

    .local v14, "l":J
    const-wide/16 v16, -0x1

    .line 440
    .local v16, "r":J
    invoke-interface {v10}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v18

    :goto_3
    invoke-interface/range {v18 .. v18}, Ljava/util/Iterator;->hasNext()Z

    move-result v19

    if-eqz v19, :cond_6

    invoke-interface/range {v18 .. v18}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v19

    check-cast v19, [I

    .line 441
    .local v19, "cur":[I
    aget v5, v19, v3

    move/from16 v20, v4

    .end local v4    # "a":I
    .local v20, "a":I
    int-to-long v3, v5

    cmp-long v3, v3, v16

    if-lez v3, :cond_4

    .line 442
    sub-long v3, v16, v14

    add-long/2addr v11, v3

    .line 443
    const/4 v3, 0x0

    aget v4, v19, v3

    int-to-long v4, v4

    .end local v14    # "l":J
    .local v4, "l":J
    aget v14, v19, v13

    int-to-long v14, v14

    move-wide/from16 v16, v14

    move-wide v14, v4

    .end local v16    # "r":J
    .local v14, "r":J
    goto :goto_4

    .line 444
    .end local v4    # "l":J
    .local v14, "l":J
    .restart local v16    # "r":J
    :cond_4
    const/4 v3, 0x0

    aget v4, v19, v13

    int-to-long v4, v4

    cmp-long v4, v4, v16

    if-lez v4, :cond_5

    .line 445
    aget v4, v19, v13

    int-to-long v4, v4

    move-wide/from16 v16, v4

    .line 447
    .end local v19    # "cur":[I
    :cond_5
    :goto_4
    move/from16 v4, v20

    const/4 v5, 0x2

    goto :goto_3

    .line 448
    .end local v20    # "a":I
    .local v4, "a":I
    :cond_6
    move/from16 v20, v4

    .end local v4    # "a":I
    .restart local v20    # "a":I
    sub-long v4, v16, v14

    add-long/2addr v11, v4

    .line 449
    int-to-long v4, v9

    mul-long/2addr v4, v11

    add-long/2addr v6, v4

    .line 429
    .end local v8    # "b":I
    .end local v9    # "len":I
    .end local v10    # "lines":Ljava/util/List;, "Ljava/util/List<[I>;"
    .end local v11    # "tot":J
    .end local v14    # "l":J
    .end local v16    # "r":J
    .end local v20    # "a":I
    :goto_5
    add-int/lit8 v2, v2, 0x1

    const/4 v5, 0x2

    goto/16 :goto_1

    .line 451
    .end local v2    # "i":I
    :cond_7
    long-to-int v2, v6

    return v2
.end method

.method public reset()V
    .locals 0

    .line 230
    invoke-virtual {p0}, Lcom/miui/server/multisence/MultiSenceServicePolicy;->resetDynFps()V

    .line 231
    invoke-virtual {p0}, Lcom/miui/server/multisence/MultiSenceServicePolicy;->resetVrs()V

    .line 232
    return-void
.end method

.method public resetDynFps()V
    .locals 2

    .line 235
    const-string v0, "/data/system/mcd/mwdf"

    const-string v1, "\n"

    invoke-static {v0, v1}, Lcom/miui/server/multisence/MultiSenceServiceUtils;->writeToFile(Ljava/lang/String;Ljava/lang/String;)Z

    .line 236
    return-void
.end method

.method public resetVrs()V
    .locals 5

    .line 238
    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, ""

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    .line 239
    .local v0, "vrsResetCmd":Ljava/lang/StringBuilder;
    const/4 v1, 0x0

    .line 240
    .local v1, "count":I
    iget-object v2, p0, Lcom/miui/server/multisence/MultiSenceServicePolicy;->mVrsWorkList:Ljava/util/Set;

    invoke-interface {v2}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_1

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/lang/String;

    .line 241
    .local v3, "appName":Ljava/lang/String;
    invoke-direct {p0, v0, v3}, Lcom/miui/server/multisence/MultiSenceServicePolicy;->setVrsOff(Ljava/lang/StringBuilder;Ljava/lang/String;)Z

    move-result v4

    if-eqz v4, :cond_0

    add-int/lit8 v1, v1, 0x1

    .line 242
    .end local v3    # "appName":Ljava/lang/String;
    :cond_0
    goto :goto_0

    .line 243
    :cond_1
    const-string v2, "\n"

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 244
    sget-boolean v2, Lcom/miui/server/multisence/MultiSenceConfig;->DEBUG_VRS:Z

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Lcom/miui/server/multisence/MultiSenceServiceUtils;->msLogD(ZLjava/lang/String;)V

    .line 245
    if-gtz v1, :cond_2

    .line 246
    return-void

    .line 248
    :cond_2
    const/4 v2, 0x2

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {p0, v2, v3}, Lcom/miui/server/multisence/MultiSenceServicePolicy;->setCommonVrsParams(ILjava/lang/String;)V

    .line 249
    iget-object v2, p0, Lcom/miui/server/multisence/MultiSenceServicePolicy;->mVrsWorkList:Ljava/util/Set;

    invoke-interface {v2}, Ljava/util/Set;->clear()V

    .line 250
    return-void
.end method

.method public setCommonVrsParams(ILjava/lang/String;)V
    .locals 5
    .param p1, "cmd"    # I
    .param p2, "params"    # Ljava/lang/String;

    .line 739
    sget-boolean v0, Lcom/miui/server/multisence/MultiSenceConfig;->DEBUG_VRS:Z

    const-string/jumbo v1, "start send VRS cmd to joyose."

    invoke-static {v0, v1}, Lcom/miui/server/multisence/MultiSenceServiceUtils;->msLogD(ZLjava/lang/String;)V

    .line 740
    invoke-static {}, Landroid/os/Parcel;->obtain()Landroid/os/Parcel;

    move-result-object v0

    .line 741
    .local v0, "data":Landroid/os/Parcel;
    invoke-static {}, Landroid/os/Parcel;->obtain()Landroid/os/Parcel;

    move-result-object v1

    .line 743
    .local v1, "reply":Landroid/os/Parcel;
    :try_start_0
    sget-object v2, Lcom/miui/server/multisence/MultiSenceServicePolicy;->mJoyoseService:Landroid/os/IBinder;

    if-nez v2, :cond_2

    .line 744
    const-string/jumbo v2, "xiaomi.joyose"

    invoke-static {v2}, Landroid/os/ServiceManager;->getService(Ljava/lang/String;)Landroid/os/IBinder;

    move-result-object v2

    sput-object v2, Lcom/miui/server/multisence/MultiSenceServicePolicy;->mJoyoseService:Landroid/os/IBinder;

    .line 745
    if-nez v2, :cond_2

    .line 746
    sget-boolean v2, Lcom/miui/server/multisence/MultiSenceConfig;->DEBUG_VRS:Z

    const-string v3, "not find joyose service"

    invoke-static {v2, v3}, Lcom/miui/server/multisence/MultiSenceServiceUtils;->msLogW(ZLjava/lang/String;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 759
    if-eqz v0, :cond_0

    .line 760
    invoke-virtual {v0}, Landroid/os/Parcel;->recycle()V

    .line 762
    :cond_0
    if-eqz v1, :cond_1

    .line 763
    invoke-virtual {v1}, Landroid/os/Parcel;->recycle()V

    .line 747
    :cond_1
    return-void

    .line 751
    :cond_2
    :try_start_1
    const-string v2, "com.xiaomi.joyose.IJoyoseInterface"

    invoke-virtual {v0, v2}, Landroid/os/Parcel;->writeInterfaceToken(Ljava/lang/String;)V

    .line 752
    invoke-virtual {v0, p1}, Landroid/os/Parcel;->writeInt(I)V

    .line 753
    invoke-virtual {v0, p2}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 754
    sget-object v2, Lcom/miui/server/multisence/MultiSenceServicePolicy;->mJoyoseService:Landroid/os/IBinder;

    const/16 v3, 0x18

    const/4 v4, 0x0

    invoke-interface {v2, v3, v0, v1, v4}, Landroid/os/IBinder;->transact(ILandroid/os/Parcel;Landroid/os/Parcel;I)Z
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_0
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 759
    if-eqz v0, :cond_3

    .line 760
    invoke-virtual {v0}, Landroid/os/Parcel;->recycle()V

    .line 762
    :cond_3
    if-eqz v1, :cond_5

    .line 763
    goto :goto_0

    .line 759
    :catchall_0
    move-exception v2

    goto :goto_1

    .line 755
    :catch_0
    move-exception v2

    .line 756
    .local v2, "e":Ljava/lang/Exception;
    const/4 v3, 0x0

    :try_start_2
    sput-object v3, Lcom/miui/server/multisence/MultiSenceServicePolicy;->mJoyoseService:Landroid/os/IBinder;

    .line 757
    invoke-virtual {v2}, Ljava/lang/Exception;->printStackTrace()V
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    .line 759
    .end local v2    # "e":Ljava/lang/Exception;
    if-eqz v0, :cond_4

    .line 760
    invoke-virtual {v0}, Landroid/os/Parcel;->recycle()V

    .line 762
    :cond_4
    if-eqz v1, :cond_5

    .line 763
    :goto_0
    invoke-virtual {v1}, Landroid/os/Parcel;->recycle()V

    .line 766
    :cond_5
    return-void

    .line 759
    :goto_1
    if-eqz v0, :cond_6

    .line 760
    invoke-virtual {v0}, Landroid/os/Parcel;->recycle()V

    .line 762
    :cond_6
    if-eqz v1, :cond_7

    .line 763
    invoke-virtual {v1}, Landroid/os/Parcel;->recycle()V

    .line 765
    :cond_7
    throw v2
.end method

.method public updateInputFocus(Ljava/lang/String;)V
    .locals 2
    .param p1, "newInputFocus"    # Ljava/lang/String;

    .line 479
    if-eqz p1, :cond_2

    const-string v0, ""

    invoke-virtual {v0, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    goto :goto_0

    .line 484
    :cond_0
    iget-object v0, p0, Lcom/miui/server/multisence/MultiSenceServicePolicy;->currentInputFocus:Ljava/lang/String;

    invoke-virtual {p1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_1

    .line 485
    iput-object p1, p0, Lcom/miui/server/multisence/MultiSenceServicePolicy;->currentInputFocus:Ljava/lang/String;

    .line 486
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "input focus changed: "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-direct {p0, v0}, Lcom/miui/server/multisence/MultiSenceServicePolicy;->LOG_IF_DEBUG(Ljava/lang/String;)V

    .line 487
    invoke-direct {p0}, Lcom/miui/server/multisence/MultiSenceServicePolicy;->calculateFocusAndPolicy()V

    .line 489
    :cond_1
    return-void

    .line 480
    :cond_2
    :goto_0
    const-string/jumbo v0, "updateInputFocus: fail, null"

    invoke-direct {p0, v0}, Lcom/miui/server/multisence/MultiSenceServicePolicy;->LOG_IF_DEBUG(Ljava/lang/String;)V

    .line 481
    return-void
.end method

.method public updatePowerMode(Z)V
    .locals 2
    .param p1, "isPowerMode"    # Z

    .line 492
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v1, "updatePowerMode: isPowerMode= "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-direct {p0, v0}, Lcom/miui/server/multisence/MultiSenceServicePolicy;->LOG_IF_DEBUG(Ljava/lang/String;)V

    .line 493
    invoke-static {}, Lcom/android/server/am/MiuiBoosterUtilsStub;->getInstance()Lcom/android/server/am/MiuiBoosterUtilsStub;

    move-result-object v0

    invoke-interface {v0, p1}, Lcom/android/server/am/MiuiBoosterUtilsStub;->notifyPowerModeChanged(Z)V

    .line 494
    return-void
.end method

.method public updateSchedPolicy(Ljava/util/Map;Ljava/util/Map;)V
    .locals 5
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/Map<",
            "Ljava/lang/String;",
            "Lcom/miui/server/multisence/SingleWindowInfo;",
            ">;",
            "Ljava/util/Map<",
            "Ljava/lang/String;",
            "Lcom/miui/server/multisence/SingleWindowInfo;",
            ">;)V"
        }
    .end annotation

    .line 114
    .local p1, "oldWindows":Ljava/util/Map;, "Ljava/util/Map<Ljava/lang/String;Lcom/miui/server/multisence/SingleWindowInfo;>;"
    .local p2, "inWindows":Ljava/util/Map;, "Ljava/util/Map<Ljava/lang/String;Lcom/miui/server/multisence/SingleWindowInfo;>;"
    iget-object v0, p0, Lcom/miui/server/multisence/MultiSenceServicePolicy;->schedWindows:Ljava/util/Map;

    invoke-interface {v0}, Ljava/util/Map;->clear()V

    .line 115
    invoke-interface {p2}, Ljava/util/Map;->keySet()Ljava/util/Set;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v0

    :goto_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_1

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/String;

    .line 116
    .local v1, "name_in":Ljava/lang/String;
    invoke-interface {p2, v1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/miui/server/multisence/SingleWindowInfo;

    .line 117
    .local v2, "inWindow":Lcom/miui/server/multisence/SingleWindowInfo;
    invoke-interface {p1, v1}, Ljava/util/Map;->containsKey(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_0

    .line 118
    invoke-interface {p1, v1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/miui/server/multisence/SingleWindowInfo;

    .line 119
    .local v3, "oldWindow":Lcom/miui/server/multisence/SingleWindowInfo;
    iget-object v4, p0, Lcom/miui/server/multisence/MultiSenceServicePolicy;->schedWindows:Ljava/util/Map;

    invoke-direct {p0, v4, v2, v3}, Lcom/miui/server/multisence/MultiSenceServicePolicy;->updateExistedWindowPolicy(Ljava/util/Map;Lcom/miui/server/multisence/SingleWindowInfo;Lcom/miui/server/multisence/SingleWindowInfo;)V

    .line 120
    invoke-interface {p1, v1}, Ljava/util/Map;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    .line 121
    .end local v3    # "oldWindow":Lcom/miui/server/multisence/SingleWindowInfo;
    goto :goto_1

    .line 122
    :cond_0
    iget-object v3, p0, Lcom/miui/server/multisence/MultiSenceServicePolicy;->schedWindows:Ljava/util/Map;

    invoke-direct {p0, v3, v2}, Lcom/miui/server/multisence/MultiSenceServicePolicy;->updateNewWindowPolicy(Ljava/util/Map;Lcom/miui/server/multisence/SingleWindowInfo;)V

    .line 124
    .end local v1    # "name_in":Ljava/lang/String;
    .end local v2    # "inWindow":Lcom/miui/server/multisence/SingleWindowInfo;
    :goto_1
    goto :goto_0

    .line 126
    :cond_1
    invoke-interface {p1}, Ljava/util/Map;->keySet()Ljava/util/Set;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v0

    :goto_2
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_2

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/String;

    .line 127
    .local v1, "name_left":Ljava/lang/String;
    invoke-interface {p1, v1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/miui/server/multisence/SingleWindowInfo;

    .line 128
    .local v2, "leftWindow":Lcom/miui/server/multisence/SingleWindowInfo;
    iget-object v3, p0, Lcom/miui/server/multisence/MultiSenceServicePolicy;->schedWindows:Ljava/util/Map;

    invoke-direct {p0, v3, v2}, Lcom/miui/server/multisence/MultiSenceServicePolicy;->updateLeftWindowPolicy(Ljava/util/Map;Lcom/miui/server/multisence/SingleWindowInfo;)V

    .line 129
    .end local v1    # "name_left":Ljava/lang/String;
    .end local v2    # "leftWindow":Lcom/miui/server/multisence/SingleWindowInfo;
    goto :goto_2

    .line 130
    :cond_2
    return-void
.end method

.method public windowInfoPerPorcessing(Ljava/util/Map;)V
    .locals 6
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/Map<",
            "Ljava/lang/String;",
            "Lcom/miui/server/multisence/SingleWindowInfo;",
            ">;)V"
        }
    .end annotation

    .line 103
    .local p1, "inWindows":Ljava/util/Map;, "Ljava/util/Map<Ljava/lang/String;Lcom/miui/server/multisence/SingleWindowInfo;>;"
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    .line 104
    .local v0, "freeformWindows":Ljava/util/List;, "Ljava/util/List<Lcom/miui/server/multisence/SingleWindowInfo;>;"
    invoke-interface {p1}, Ljava/util/Map;->keySet()Ljava/util/Set;

    move-result-object v1

    invoke-interface {v1}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_1

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/lang/String;

    .line 105
    .local v2, "appName":Ljava/lang/String;
    invoke-interface {p1, v2}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/miui/server/multisence/SingleWindowInfo;

    .line 106
    .local v3, "app":Lcom/miui/server/multisence/SingleWindowInfo;
    invoke-virtual {v3}, Lcom/miui/server/multisence/SingleWindowInfo;->getWindowForm()Lcom/miui/server/multisence/SingleWindowInfo$WindowForm;

    move-result-object v4

    sget-object v5, Lcom/miui/server/multisence/SingleWindowInfo$WindowForm;->MUTIL_FREEDOM:Lcom/miui/server/multisence/SingleWindowInfo$WindowForm;

    if-ne v4, v5, :cond_0

    .line 107
    invoke-interface {v0, v3}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 109
    .end local v2    # "appName":Ljava/lang/String;
    .end local v3    # "app":Lcom/miui/server/multisence/SingleWindowInfo;
    :cond_0
    goto :goto_0

    .line 110
    :cond_1
    invoke-virtual {p0, v0}, Lcom/miui/server/multisence/MultiSenceServicePolicy;->limitFreeFormRes(Ljava/util/List;)V

    .line 111
    return-void
.end method
