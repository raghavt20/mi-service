.class final Lcom/miui/server/multisence/MultiSenceService$BinderService;
.super Landroid/view/IFloatingWindow$Stub;
.source "MultiSenceService.java"

# interfaces
.implements Lcom/miui/server/multisence/MultiSenceManagerInternal;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/miui/server/multisence/MultiSenceService;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x12
    name = "BinderService"
.end annotation


# instance fields
.field final synthetic this$0:Lcom/miui/server/multisence/MultiSenceService;


# direct methods
.method private constructor <init>(Lcom/miui/server/multisence/MultiSenceService;)V
    .locals 0

    .line 262
    iput-object p1, p0, Lcom/miui/server/multisence/MultiSenceService$BinderService;->this$0:Lcom/miui/server/multisence/MultiSenceService;

    invoke-direct {p0}, Landroid/view/IFloatingWindow$Stub;-><init>()V

    return-void
.end method

.method synthetic constructor <init>(Lcom/miui/server/multisence/MultiSenceService;Lcom/miui/server/multisence/MultiSenceService$BinderService-IA;)V
    .locals 0

    invoke-direct {p0, p1}, Lcom/miui/server/multisence/MultiSenceService$BinderService;-><init>(Lcom/miui/server/multisence/MultiSenceService;)V

    return-void
.end method


# virtual methods
.method protected dump(Ljava/io/FileDescriptor;Ljava/io/PrintWriter;[Ljava/lang/String;)V
    .locals 2
    .param p1, "fd"    # Ljava/io/FileDescriptor;
    .param p2, "pw"    # Ljava/io/PrintWriter;
    .param p3, "args"    # [Ljava/lang/String;

    .line 264
    iget-object v0, p0, Lcom/miui/server/multisence/MultiSenceService$BinderService;->this$0:Lcom/miui/server/multisence/MultiSenceService;

    invoke-static {v0}, Lcom/miui/server/multisence/MultiSenceService;->-$$Nest$fgetmContext(Lcom/miui/server/multisence/MultiSenceService;)Landroid/content/Context;

    move-result-object v0

    const-string v1, "MultiSenceService"

    invoke-static {v0, v1, p2}, Lcom/android/internal/util/DumpUtils;->checkDumpPermission(Landroid/content/Context;Ljava/lang/String;Ljava/io/PrintWriter;)Z

    move-result v0

    if-nez v0, :cond_0

    return-void

    .line 265
    :cond_0
    const-string v0, "multisence (miui_multi_sence):"

    invoke-virtual {p2, v0}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V

    .line 267
    :try_start_0
    array-length v0, p3

    if-gtz v0, :cond_1

    .line 268
    invoke-static {}, Lcom/miui/server/multisence/MultiSenceConfig;->getInstance()Lcom/miui/server/multisence/MultiSenceConfig;

    move-result-object v0

    invoke-virtual {v0, p2}, Lcom/miui/server/multisence/MultiSenceConfig;->dumpConfig(Ljava/io/PrintWriter;)V

    .line 270
    :cond_1
    const-string v0, "config"

    const/4 v1, 0x0

    aget-object v1, p3, v1

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 271
    invoke-static {}, Lcom/miui/server/multisence/MultiSenceConfig;->getInstance()Lcom/miui/server/multisence/MultiSenceConfig;

    move-result-object v0

    invoke-virtual {v0, p2}, Lcom/miui/server/multisence/MultiSenceConfig;->dumpConfig(Ljava/io/PrintWriter;)V

    .line 273
    :cond_2
    const-string v0, "multisence dump end"

    invoke-virtual {p2, v0}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 276
    goto :goto_0

    .line 274
    :catch_0
    move-exception v0

    .line 275
    .local v0, "e":Ljava/lang/Exception;
    invoke-virtual {v0}, Ljava/lang/Exception;->printStackTrace()V

    .line 277
    .end local v0    # "e":Ljava/lang/Exception;
    :goto_0
    return-void
.end method

.method public floatingWindowClose([ILjava/lang/String;)V
    .locals 1
    .param p1, "req_tids"    # [I
    .param p2, "packageName"    # Ljava/lang/String;

    .line 312
    iget-object v0, p0, Lcom/miui/server/multisence/MultiSenceService$BinderService;->this$0:Lcom/miui/server/multisence/MultiSenceService;

    invoke-virtual {v0, p1, p2}, Lcom/miui/server/multisence/MultiSenceService;->floatingWindowClose([ILjava/lang/String;)V

    .line 313
    return-void
.end method

.method public floatingWindowMoveEnd([ILjava/lang/String;Landroid/graphics/Rect;)V
    .locals 1
    .param p1, "req_tids"    # [I
    .param p2, "packageName"    # Ljava/lang/String;
    .param p3, "bounds"    # Landroid/graphics/Rect;

    .line 320
    iget-object v0, p0, Lcom/miui/server/multisence/MultiSenceService$BinderService;->this$0:Lcom/miui/server/multisence/MultiSenceService;

    invoke-virtual {v0, p1, p2, p3}, Lcom/miui/server/multisence/MultiSenceService;->floatingWindowMoveEnd([ILjava/lang/String;Landroid/graphics/Rect;)V

    .line 321
    return-void
.end method

.method public floatingWindowMoveStart([ILjava/lang/String;)V
    .locals 1
    .param p1, "req_tids"    # [I
    .param p2, "packageName"    # Ljava/lang/String;

    .line 316
    iget-object v0, p0, Lcom/miui/server/multisence/MultiSenceService$BinderService;->this$0:Lcom/miui/server/multisence/MultiSenceService;

    invoke-virtual {v0, p1, p2}, Lcom/miui/server/multisence/MultiSenceService;->floatingWindowMoveStart([ILjava/lang/String;)V

    .line 317
    return-void
.end method

.method public floatingWindowShow([ILjava/lang/String;Landroid/graphics/Rect;)V
    .locals 1
    .param p1, "req_tids"    # [I
    .param p2, "packageName"    # Ljava/lang/String;
    .param p3, "bounds"    # Landroid/graphics/Rect;

    .line 308
    iget-object v0, p0, Lcom/miui/server/multisence/MultiSenceService$BinderService;->this$0:Lcom/miui/server/multisence/MultiSenceService;

    invoke-virtual {v0, p1, p2, p3}, Lcom/miui/server/multisence/MultiSenceService;->floatingWindowShow([ILjava/lang/String;Landroid/graphics/Rect;)V

    .line 309
    return-void
.end method

.method public onShellCommand(Ljava/io/FileDescriptor;Ljava/io/FileDescriptor;Ljava/io/FileDescriptor;[Ljava/lang/String;Landroid/os/ShellCallback;Landroid/os/ResultReceiver;)V
    .locals 8
    .param p1, "in"    # Ljava/io/FileDescriptor;
    .param p2, "out"    # Ljava/io/FileDescriptor;
    .param p3, "err"    # Ljava/io/FileDescriptor;
    .param p4, "args"    # [Ljava/lang/String;
    .param p5, "callback"    # Landroid/os/ShellCallback;
    .param p6, "resultReceiver"    # Landroid/os/ResultReceiver;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    .line 283
    new-instance v0, Lcom/miui/server/multisence/MultiSenceService$MultiSenceShellCmd;

    iget-object v1, p0, Lcom/miui/server/multisence/MultiSenceService$BinderService;->this$0:Lcom/miui/server/multisence/MultiSenceService;

    invoke-direct {v0, v1, v1}, Lcom/miui/server/multisence/MultiSenceService$MultiSenceShellCmd;-><init>(Lcom/miui/server/multisence/MultiSenceService;Lcom/miui/server/multisence/MultiSenceService;)V

    .line 284
    move-object v1, p0

    move-object v2, p1

    move-object v3, p2

    move-object v4, p3

    move-object v5, p4

    move-object v6, p5

    move-object v7, p6

    invoke-virtual/range {v0 .. v7}, Lcom/miui/server/multisence/MultiSenceService$MultiSenceShellCmd;->exec(Landroid/os/Binder;Ljava/io/FileDescriptor;Ljava/io/FileDescriptor;Ljava/io/FileDescriptor;[Ljava/lang/String;Landroid/os/ShellCallback;Landroid/os/ResultReceiver;)I

    .line 285
    return-void
.end method

.method public setUpdateReason(Ljava/lang/String;)V
    .locals 1
    .param p1, "reason"    # Ljava/lang/String;

    .line 304
    iget-object v0, p0, Lcom/miui/server/multisence/MultiSenceService$BinderService;->this$0:Lcom/miui/server/multisence/MultiSenceService;

    invoke-virtual {v0, p1}, Lcom/miui/server/multisence/MultiSenceService;->setUpdateReason(Ljava/lang/String;)V

    .line 305
    return-void
.end method

.method public showAllWindowInfo(Ljava/lang/String;)V
    .locals 1
    .param p1, "reason"    # Ljava/lang/String;

    .line 300
    iget-object v0, p0, Lcom/miui/server/multisence/MultiSenceService$BinderService;->this$0:Lcom/miui/server/multisence/MultiSenceService;

    invoke-virtual {v0, p1}, Lcom/miui/server/multisence/MultiSenceService;->showAllWindowInfo(Ljava/lang/String;)V

    .line 301
    return-void
.end method

.method public updateDynamicWindowsInfo(II[IZ)V
    .locals 2
    .param p1, "senceId"    # I
    .param p2, "uid"    # I
    .param p3, "tids"    # [I
    .param p4, "isStarted"    # Z

    .line 292
    iget-object v0, p0, Lcom/miui/server/multisence/MultiSenceService$BinderService;->this$0:Lcom/miui/server/multisence/MultiSenceService;

    iget-object v0, v0, Lcom/miui/server/multisence/MultiSenceService;->mHandler:Landroid/os/Handler;

    invoke-virtual {v0, p1}, Landroid/os/Handler;->obtainMessage(I)Landroid/os/Message;

    move-result-object v0

    .line 293
    .local v0, "message":Landroid/os/Message;
    iput p2, v0, Landroid/os/Message;->arg1:I

    .line 294
    iput p4, v0, Landroid/os/Message;->arg2:I

    .line 295
    iput-object p3, v0, Landroid/os/Message;->obj:Ljava/lang/Object;

    .line 296
    iget-object v1, p0, Lcom/miui/server/multisence/MultiSenceService$BinderService;->this$0:Lcom/miui/server/multisence/MultiSenceService;

    iget-object v1, v1, Lcom/miui/server/multisence/MultiSenceService;->mHandler:Landroid/os/Handler;

    invoke-virtual {v1, v0}, Landroid/os/Handler;->sendMessage(Landroid/os/Message;)Z

    .line 297
    return-void
.end method

.method public updateStaticWindowsInfo(Ljava/util/Map;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/Map<",
            "Ljava/lang/String;",
            "Lcom/miui/server/multisence/SingleWindowInfo;",
            ">;)V"
        }
    .end annotation

    .line 288
    .local p1, "sWindows":Ljava/util/Map;, "Ljava/util/Map<Ljava/lang/String;Lcom/miui/server/multisence/SingleWindowInfo;>;"
    iget-object v0, p0, Lcom/miui/server/multisence/MultiSenceService$BinderService;->this$0:Lcom/miui/server/multisence/MultiSenceService;

    invoke-virtual {v0, p1}, Lcom/miui/server/multisence/MultiSenceService;->updateStaticWindowsInfo(Ljava/util/Map;)V

    .line 289
    return-void
.end method
