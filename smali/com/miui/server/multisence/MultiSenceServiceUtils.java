public class com.miui.server.multisence.MultiSenceServiceUtils {
	 /* .source "MultiSenceServiceUtils.java" */
	 /* # static fields */
	 private static final java.lang.String TAG;
	 /* # direct methods */
	 public com.miui.server.multisence.MultiSenceServiceUtils ( ) {
		 /* .locals 0 */
		 /* .line 16 */
		 /* invoke-direct {p0}, Ljava/lang/Object;-><init>()V */
		 return;
	 } // .end method
	 public static void msLogD ( Boolean p0, java.lang.String p1 ) {
		 /* .locals 1 */
		 /* .param p0, "isDebug" # Z */
		 /* .param p1, "log" # Ljava/lang/String; */
		 /* .line 20 */
		 /* if-nez p0, :cond_0 */
		 /* .line 21 */
		 return;
		 /* .line 23 */
	 } // :cond_0
	 final String v0 = "MultiSenceService"; // const-string v0, "MultiSenceService"
	 android.util.Slog .d ( v0,p1 );
	 /* .line 24 */
	 return;
} // .end method
static void msLogW ( Boolean p0, java.lang.String p1 ) {
	 /* .locals 1 */
	 /* .param p0, "isDebug" # Z */
	 /* .param p1, "log" # Ljava/lang/String; */
	 /* .line 27 */
	 /* if-nez p0, :cond_0 */
	 /* .line 28 */
	 return;
	 /* .line 30 */
} // :cond_0
final String v0 = "MultiSenceService"; // const-string v0, "MultiSenceService"
android.util.Slog .w ( v0,p1 );
/* .line 31 */
return;
} // .end method
static void msTraceBegin ( java.lang.String p0 ) {
/* .locals 2 */
/* .param p0, "tips" # Ljava/lang/String; */
/* .line 34 */
/* sget-boolean v0, Lcom/miui/server/multisence/MultiSenceConfig;->DEBUG_TRACE:Z */
/* if-nez v0, :cond_0 */
/* .line 35 */
return;
/* .line 37 */
} // :cond_0
/* const-wide/16 v0, 0x20 */
android.os.Trace .traceBegin ( v0,v1,p0 );
/* .line 38 */
return;
} // .end method
static void msTraceEnd ( ) {
/* .locals 2 */
/* .line 41 */
/* sget-boolean v0, Lcom/miui/server/multisence/MultiSenceConfig;->DEBUG_TRACE:Z */
/* if-nez v0, :cond_0 */
/* .line 42 */
return;
/* .line 44 */
} // :cond_0
/* const-wide/16 v0, 0x20 */
android.os.Trace .traceEnd ( v0,v1 );
/* .line 45 */
return;
} // .end method
static java.lang.String readFromFile ( java.lang.String p0 ) {
/* .locals 8 */
/* .param p0, "filePath" # Ljava/lang/String; */
/* .line 70 */
final String v0 = "optSched IOexception : "; // const-string v0, "optSched IOexception : "
final String v1 = "MultiSenceService"; // const-string v1, "MultiSenceService"
int v2 = 0; // const/4 v2, 0x0
/* .line 71 */
/* .local v2, "fis":Ljava/io/FileInputStream; */
/* new-instance v3, Ljava/lang/StringBuilder; */
/* invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V */
/* .line 74 */
/* .local v3, "ret":Ljava/lang/StringBuilder; */
try { // :try_start_0
/* new-instance v4, Ljava/io/File; */
/* invoke-direct {v4, p0}, Ljava/io/File;-><init>(Ljava/lang/String;)V */
/* .line 75 */
/* .local v4, "file":Ljava/io/File; */
/* new-instance v5, Ljava/io/FileInputStream; */
/* invoke-direct {v5, v4}, Ljava/io/FileInputStream;-><init>(Ljava/io/File;)V */
/* move-object v2, v5 */
/* .line 76 */
int v5 = 0; // const/4 v5, 0x0
/* .line 77 */
/* .local v5, "readData":I */
} // :goto_0
v6 = (( java.io.FileInputStream ) v2 ).read ( ); // invoke-virtual {v2}, Ljava/io/FileInputStream;->read()I
/* move v5, v6 */
int v7 = -1; // const/4 v7, -0x1
/* if-eq v6, v7, :cond_0 */
/* .line 78 */
/* int-to-char v6, v5 */
(( java.lang.StringBuilder ) v3 ).append ( v6 ); // invoke-virtual {v3, v6}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;
/* :try_end_0 */
/* .catch Ljava/lang/Exception; {:try_start_0 ..:try_end_0} :catch_1 */
/* .catchall {:try_start_0 ..:try_end_0} :catchall_0 */
/* .line 83 */
} // .end local v4 # "file":Ljava/io/File;
} // .end local v5 # "readData":I
} // :cond_0
/* nop */
/* .line 85 */
try { // :try_start_1
(( java.io.FileInputStream ) v2 ).close ( ); // invoke-virtual {v2}, Ljava/io/FileInputStream;->close()V
/* :try_end_1 */
/* .catch Ljava/io/IOException; {:try_start_1 ..:try_end_1} :catch_0 */
/* .line 88 */
} // :goto_1
/* .line 86 */
/* :catch_0 */
/* move-exception v4 */
/* .line 87 */
/* .local v4, "e":Ljava/io/IOException; */
/* new-instance v5, Ljava/lang/StringBuilder; */
/* invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V */
} // :goto_2
(( java.lang.StringBuilder ) v5 ).append ( v0 ); // invoke-virtual {v5, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.io.IOException ) v4 ).toString ( ); // invoke-virtual {v4}, Ljava/io/IOException;->toString()Ljava/lang/String;
(( java.lang.StringBuilder ) v0 ).append ( v5 ); // invoke-virtual {v0, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v0 ).toString ( ); // invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
android.util.Slog .e ( v1,v0 );
} // .end local v4 # "e":Ljava/io/IOException;
/* .line 83 */
/* :catchall_0 */
/* move-exception v4 */
/* .line 80 */
/* :catch_1 */
/* move-exception v4 */
/* .line 81 */
/* .local v4, "e":Ljava/lang/Exception; */
try { // :try_start_2
(( java.lang.Exception ) v4 ).toString ( ); // invoke-virtual {v4}, Ljava/lang/Exception;->toString()Ljava/lang/String;
android.util.Slog .e ( v1,v5 );
/* :try_end_2 */
/* .catchall {:try_start_2 ..:try_end_2} :catchall_0 */
/* .line 83 */
/* nop */
} // .end local v4 # "e":Ljava/lang/Exception;
if ( v2 != null) { // if-eqz v2, :cond_1
/* .line 85 */
try { // :try_start_3
(( java.io.FileInputStream ) v2 ).close ( ); // invoke-virtual {v2}, Ljava/io/FileInputStream;->close()V
/* :try_end_3 */
/* .catch Ljava/io/IOException; {:try_start_3 ..:try_end_3} :catch_2 */
/* .line 86 */
/* :catch_2 */
/* move-exception v4 */
/* .line 87 */
/* .local v4, "e":Ljava/io/IOException; */
/* new-instance v5, Ljava/lang/StringBuilder; */
/* invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V */
/* .line 91 */
} // .end local v4 # "e":Ljava/io/IOException;
} // :cond_1
} // :goto_3
(( java.lang.StringBuilder ) v3 ).toString ( ); // invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
/* .line 83 */
} // :goto_4
if ( v2 != null) { // if-eqz v2, :cond_2
/* .line 85 */
try { // :try_start_4
(( java.io.FileInputStream ) v2 ).close ( ); // invoke-virtual {v2}, Ljava/io/FileInputStream;->close()V
/* :try_end_4 */
/* .catch Ljava/io/IOException; {:try_start_4 ..:try_end_4} :catch_3 */
/* .line 88 */
/* .line 86 */
/* :catch_3 */
/* move-exception v5 */
/* .line 87 */
/* .local v5, "e":Ljava/io/IOException; */
/* new-instance v6, Ljava/lang/StringBuilder; */
/* invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V */
(( java.lang.StringBuilder ) v6 ).append ( v0 ); // invoke-virtual {v6, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.io.IOException ) v5 ).toString ( ); // invoke-virtual {v5}, Ljava/io/IOException;->toString()Ljava/lang/String;
(( java.lang.StringBuilder ) v0 ).append ( v6 ); // invoke-virtual {v0, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v0 ).toString ( ); // invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
android.util.Slog .e ( v1,v0 );
/* .line 90 */
} // .end local v5 # "e":Ljava/io/IOException;
} // :cond_2
} // :goto_5
/* throw v4 */
} // .end method
static Boolean writeToFile ( java.lang.String p0, java.lang.String p1 ) {
/* .locals 7 */
/* .param p0, "filePath" # Ljava/lang/String; */
/* .param p1, "value" # Ljava/lang/String; */
/* .line 48 */
final String v0 = "optSched IOexception : "; // const-string v0, "optSched IOexception : "
final String v1 = "MultiSenceService"; // const-string v1, "MultiSenceService"
int v2 = 0; // const/4 v2, 0x0
/* .line 49 */
/* .local v2, "fos":Ljava/io/FileOutputStream; */
int v3 = 1; // const/4 v3, 0x1
/* .line 51 */
/* .local v3, "success":Z */
try { // :try_start_0
/* new-instance v4, Ljava/io/File; */
/* invoke-direct {v4, p0}, Ljava/io/File;-><init>(Ljava/lang/String;)V */
/* .line 52 */
/* .local v4, "file":Ljava/io/File; */
/* new-instance v5, Ljava/io/FileOutputStream; */
/* invoke-direct {v5, v4}, Ljava/io/FileOutputStream;-><init>(Ljava/io/File;)V */
/* move-object v2, v5 */
/* .line 53 */
(( java.lang.String ) p1 ).getBytes ( ); // invoke-virtual {p1}, Ljava/lang/String;->getBytes()[B
(( java.io.FileOutputStream ) v2 ).write ( v5 ); // invoke-virtual {v2, v5}, Ljava/io/FileOutputStream;->write([B)V
/* :try_end_0 */
/* .catch Ljava/lang/Exception; {:try_start_0 ..:try_end_0} :catch_1 */
/* .catchall {:try_start_0 ..:try_end_0} :catchall_0 */
/* .line 58 */
} // .end local v4 # "file":Ljava/io/File;
/* nop */
/* .line 60 */
try { // :try_start_1
(( java.io.FileOutputStream ) v2 ).close ( ); // invoke-virtual {v2}, Ljava/io/FileOutputStream;->close()V
/* :try_end_1 */
/* .catch Ljava/io/IOException; {:try_start_1 ..:try_end_1} :catch_0 */
/* .line 63 */
} // :goto_0
/* .line 61 */
/* :catch_0 */
/* move-exception v4 */
/* .line 62 */
/* .local v4, "e":Ljava/io/IOException; */
/* new-instance v5, Ljava/lang/StringBuilder; */
/* invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V */
} // :goto_1
(( java.lang.StringBuilder ) v5 ).append ( v0 ); // invoke-virtual {v5, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.io.IOException ) v4 ).toString ( ); // invoke-virtual {v4}, Ljava/io/IOException;->toString()Ljava/lang/String;
(( java.lang.StringBuilder ) v0 ).append ( v5 ); // invoke-virtual {v0, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v0 ).toString ( ); // invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
android.util.Slog .e ( v1,v0 );
} // .end local v4 # "e":Ljava/io/IOException;
/* .line 58 */
/* :catchall_0 */
/* move-exception v4 */
/* .line 54 */
/* :catch_1 */
/* move-exception v4 */
/* .line 55 */
/* .local v4, "e":Ljava/lang/Exception; */
int v3 = 0; // const/4 v3, 0x0
/* .line 56 */
try { // :try_start_2
(( java.lang.Exception ) v4 ).toString ( ); // invoke-virtual {v4}, Ljava/lang/Exception;->toString()Ljava/lang/String;
android.util.Slog .e ( v1,v5 );
/* :try_end_2 */
/* .catchall {:try_start_2 ..:try_end_2} :catchall_0 */
/* .line 58 */
/* nop */
} // .end local v4 # "e":Ljava/lang/Exception;
if ( v2 != null) { // if-eqz v2, :cond_0
/* .line 60 */
try { // :try_start_3
(( java.io.FileOutputStream ) v2 ).close ( ); // invoke-virtual {v2}, Ljava/io/FileOutputStream;->close()V
/* :try_end_3 */
/* .catch Ljava/io/IOException; {:try_start_3 ..:try_end_3} :catch_2 */
/* .line 61 */
/* :catch_2 */
/* move-exception v4 */
/* .line 62 */
/* .local v4, "e":Ljava/io/IOException; */
/* new-instance v5, Ljava/lang/StringBuilder; */
/* invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V */
/* .line 66 */
} // .end local v4 # "e":Ljava/io/IOException;
} // :cond_0
} // :goto_2
/* .line 58 */
} // :goto_3
if ( v2 != null) { // if-eqz v2, :cond_1
/* .line 60 */
try { // :try_start_4
(( java.io.FileOutputStream ) v2 ).close ( ); // invoke-virtual {v2}, Ljava/io/FileOutputStream;->close()V
/* :try_end_4 */
/* .catch Ljava/io/IOException; {:try_start_4 ..:try_end_4} :catch_3 */
/* .line 63 */
/* .line 61 */
/* :catch_3 */
/* move-exception v5 */
/* .line 62 */
/* .local v5, "e":Ljava/io/IOException; */
/* new-instance v6, Ljava/lang/StringBuilder; */
/* invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V */
(( java.lang.StringBuilder ) v6 ).append ( v0 ); // invoke-virtual {v6, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.io.IOException ) v5 ).toString ( ); // invoke-virtual {v5}, Ljava/io/IOException;->toString()Ljava/lang/String;
(( java.lang.StringBuilder ) v0 ).append ( v6 ); // invoke-virtual {v0, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v0 ).toString ( ); // invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
android.util.Slog .e ( v1,v0 );
/* .line 65 */
} // .end local v5 # "e":Ljava/io/IOException;
} // :cond_1
} // :goto_4
/* throw v4 */
} // .end method
