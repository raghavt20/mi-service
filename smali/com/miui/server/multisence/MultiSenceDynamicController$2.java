class com.miui.server.multisence.MultiSenceDynamicController$2 extends android.database.ContentObserver {
	 /* .source "MultiSenceDynamicController.java" */
	 /* # annotations */
	 /* .annotation system Ldalvik/annotation/EnclosingMethod; */
	 /* value = Lcom/miui/server/multisence/MultiSenceDynamicController;->registerVRSCloudObserver()V */
} // .end annotation
/* .annotation system Ldalvik/annotation/InnerClass; */
/* accessFlags = 0x0 */
/* name = null */
} // .end annotation
/* # instance fields */
final com.miui.server.multisence.MultiSenceDynamicController this$0; //synthetic
/* # direct methods */
 com.miui.server.multisence.MultiSenceDynamicController$2 ( ) {
/* .locals 0 */
/* .param p1, "this$0" # Lcom/miui/server/multisence/MultiSenceDynamicController; */
/* .param p2, "handler" # Landroid/os/Handler; */
/* .line 129 */
this.this$0 = p1;
/* invoke-direct {p0, p2}, Landroid/database/ContentObserver;-><init>(Landroid/os/Handler;)V */
return;
} // .end method
/* # virtual methods */
public void onChange ( Boolean p0, android.net.Uri p1 ) {
/* .locals 4 */
/* .param p1, "selfChange" # Z */
/* .param p2, "uri" # Landroid/net/Uri; */
/* .line 132 */
if ( p2 != null) { // if-eqz p2, :cond_0
	 /* const-string/jumbo v0, "support_common_vrs_app" */
	 android.provider.Settings$System .getUriFor ( v0 );
	 v1 = 	 (( android.net.Uri ) p2 ).equals ( v1 ); // invoke-virtual {p2, v1}, Landroid/net/Uri;->equals(Ljava/lang/Object;)Z
	 if ( v1 != null) { // if-eqz v1, :cond_0
		 /* .line 133 */
		 v1 = this.this$0;
		 com.miui.server.multisence.MultiSenceDynamicController .-$$Nest$fgetmContext ( v1 );
		 (( android.content.Context ) v1 ).getContentResolver ( ); // invoke-virtual {v1}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;
		 int v2 = -2; // const/4 v2, -0x2
		 android.provider.Settings$System .getStringForUser ( v1,v0,v2 );
		 /* .line 135 */
		 /* .local v0, "cloudInfo":Ljava/lang/String; */
		 /* sget-boolean v1, Lcom/miui/server/multisence/MultiSenceConfig;->DEBUG_VRS:Z */
		 /* new-instance v2, Ljava/lang/StringBuilder; */
		 /* invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V */
		 /* const-string/jumbo v3, "vrs-whitelist from cloud: " */
		 (( java.lang.StringBuilder ) v2 ).append ( v3 ); // invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
		 (( java.lang.StringBuilder ) v2 ).append ( v0 ); // invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
		 (( java.lang.StringBuilder ) v2 ).toString ( ); // invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
		 com.miui.server.multisence.MultiSenceServiceUtils .msLogD ( v1,v2 );
		 /* .line 136 */
		 com.miui.server.multisence.MultiSenceConfig .getInstance ( );
		 (( com.miui.server.multisence.MultiSenceConfig ) v1 ).updateVRSWhiteListAll ( v0 ); // invoke-virtual {v1, v0}, Lcom/miui/server/multisence/MultiSenceConfig;->updateVRSWhiteListAll(Ljava/lang/String;)V
		 /* .line 138 */
	 } // .end local v0 # "cloudInfo":Ljava/lang/String;
} // :cond_0
return;
} // .end method
