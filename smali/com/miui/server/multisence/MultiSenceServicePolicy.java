public class com.miui.server.multisence.MultiSenceServicePolicy {
	 /* .source "MultiSenceServicePolicy.java" */
	 /* # static fields */
	 private static final Boolean DEBUG;
	 private static final Integer DEFAULT_PRIORITY_LEVEL;
	 private static final Integer DEFAULT_PRIORITY_TIMEOUT;
	 private static final Integer FREEFORM_ELUDE_PRIORITY_LEVEL;
	 private static final Integer FREEFORM_ELUDE_TIMOUT;
	 private static final Integer FREEFORM_ENTERFULLSCREEN_PRIORITY_LEVEL;
	 private static final Integer FREEFORM_ENTERFULLSCREEN_PRIORITY_TIMEOUT;
	 private static final Integer FREEFORM_MOVE_PRIORITY_LEVEL;
	 private static final Integer FREEFORM_MOVE_PRIORITY_TIMEOUT;
	 private static final Integer FREEFORM_RESIZE_PRIORITY_LEVEL;
	 private static final Integer FREEFORM_RESIZE_PRIORITY_TIMEOUT;
	 private static final Integer FW_PRIORITY_LEVEL;
	 private static final Integer FW_PRIORITY_TIMEOUT;
	 private static final java.lang.String MCD_DF_PATH;
	 private static final Integer MWS_PRIORITY_LEVEL;
	 private static final Integer MWS_PRIORITY_TIMEOUT;
	 private static final Integer SPLITSCREEN_PRIORITY_LEVEL;
	 private static final Integer SPLITSCREEN_PRIORITY_TIMEOUT;
	 private static final java.lang.String TAG;
	 private static final Integer VRA_CMD_APP_CONTROL;
	 private static final Integer VRA_CMD_SYNC_WHITELIST;
	 private static android.os.IBinder mJoyoseService;
	 /* # instance fields */
	 java.lang.String currentInputFocus;
	 java.lang.String dynamicSencePackage;
	 java.lang.String focusPackage;
	 Boolean isDynamicSenceStarting;
	 private java.util.ArrayList mDefaultRequestIds;
	 /* .annotation system Ldalvik/annotation/Signature; */
	 /* value = { */
	 /* "Ljava/util/ArrayList<", */
	 /* "Ljava/lang/Integer;", */
	 /* ">;" */
	 /* } */
} // .end annotation
} // .end field
private Integer mFWBindCoreRequestId;
private Integer mFWRequestId;
private Integer mFreeformEludeBindCoreRequestId;
private Integer mFreeformEludeRequestId;
private Integer mFreeformEnterFullScreenBindCoreRequestId;
private Integer mFreeformEnterFullScreenRequestId;
private Integer mFreeformMoveBindCoreRequestId;
private Integer mFreeformMoveRequestId;
private Integer mFreeformResizeBindCoreRequestId;
private Integer mFreeformResizeRequestId;
private Integer mMWSBindCoreRequestId;
private Integer mMWSRequestId;
private Integer mSplitscreenBindCoreRequestId;
private Integer mSplitscreenRequestId;
private java.util.Set mVrsWorkList;
/* .annotation system Ldalvik/annotation/Signature; */
/* value = { */
/* "Ljava/util/Set<", */
/* "Ljava/lang/String;", */
/* ">;" */
/* } */
} // .end annotation
} // .end field
java.util.Map schedWindows;
/* .annotation system Ldalvik/annotation/Signature; */
/* value = { */
/* "Ljava/util/Map<", */
/* "Ljava/lang/String;", */
/* "Lcom/miui/server/multisence/SingleWindowInfo;", */
/* ">;" */
/* } */
} // .end annotation
} // .end field
/* # direct methods */
static com.miui.server.multisence.MultiSenceServicePolicy ( ) {
/* .locals 4 */
/* .line 47 */
final String v0 = "persist.multisence.debug.on"; // const-string v0, "persist.multisence.debug.on"
int v1 = 0; // const/4 v1, 0x0
v0 = android.os.SystemProperties .getBoolean ( v0,v1 );
com.miui.server.multisence.MultiSenceServicePolicy.DEBUG = (v0!= 0);
/* .line 51 */
int v0 = 0; // const/4 v0, 0x0
/* .line 73 */
final String v0 = "persist.multisence.pri_time.freeform_move"; // const-string v0, "persist.multisence.pri_time.freeform_move"
/* const/16 v1, 0x1f40 */
v0 = android.os.SystemProperties .getInt ( v0,v1 );
/* .line 74 */
final String v0 = "persist.multisence.pri_level.freeform_move"; // const-string v0, "persist.multisence.pri_level.freeform_move"
int v2 = 1; // const/4 v2, 0x1
v0 = android.os.SystemProperties .getInt ( v0,v2 );
/* .line 75 */
final String v0 = "persist.multisence.pri_time.freeform_resize"; // const-string v0, "persist.multisence.pri_time.freeform_resize"
v0 = android.os.SystemProperties .getInt ( v0,v1 );
/* .line 76 */
final String v0 = "persist.multisence.pri_level.freeform_resize"; // const-string v0, "persist.multisence.pri_level.freeform_resize"
v0 = android.os.SystemProperties .getInt ( v0,v2 );
/* .line 77 */
final String v0 = "persist.multisence.pri_time.freeform_enterfullscreen"; // const-string v0, "persist.multisence.pri_time.freeform_enterfullscreen"
v0 = android.os.SystemProperties .getInt ( v0,v1 );
/* .line 78 */
final String v0 = "persist.multisence.pri_level.freeform_enterfullscreen"; // const-string v0, "persist.multisence.pri_level.freeform_enterfullscreen"
v0 = android.os.SystemProperties .getInt ( v0,v2 );
/* .line 79 */
final String v0 = "persist.multisence.pri_time.freeform_elude"; // const-string v0, "persist.multisence.pri_time.freeform_elude"
/* const/16 v1, 0x3e8 */
v0 = android.os.SystemProperties .getInt ( v0,v1 );
/* .line 80 */
final String v0 = "persist.multisence.pri_level.freeform_elude"; // const-string v0, "persist.multisence.pri_level.freeform_elude"
v0 = android.os.SystemProperties .getInt ( v0,v2 );
/* .line 81 */
final String v0 = "persist.multisence.pri_time.splitscreen"; // const-string v0, "persist.multisence.pri_time.splitscreen"
/* const/16 v3, 0x7d0 */
v0 = android.os.SystemProperties .getInt ( v0,v3 );
/* .line 82 */
final String v0 = "persist.multisence.pri_level.splitscreen"; // const-string v0, "persist.multisence.pri_level.splitscreen"
v0 = android.os.SystemProperties .getInt ( v0,v2 );
/* .line 83 */
final String v0 = "persist.multisence.pri_time.mws"; // const-string v0, "persist.multisence.pri_time.mws"
/* const/16 v3, 0x1770 */
v0 = android.os.SystemProperties .getInt ( v0,v3 );
/* .line 84 */
final String v0 = "persist.multisence.pri_level.mws"; // const-string v0, "persist.multisence.pri_level.mws"
v0 = android.os.SystemProperties .getInt ( v0,v2 );
/* .line 85 */
final String v0 = "persist.multisence.pri_time.default"; // const-string v0, "persist.multisence.pri_time.default"
v0 = android.os.SystemProperties .getInt ( v0,v1 );
/* .line 86 */
final String v0 = "persist.multisence.pri_level.default"; // const-string v0, "persist.multisence.pri_level.default"
int v1 = 2; // const/4 v1, 0x2
v0 = android.os.SystemProperties .getInt ( v0,v1 );
/* .line 87 */
/* nop */
/* .line 88 */
final String v0 = "persist.multisence.pri_time.floatingwindow_move"; // const-string v0, "persist.multisence.pri_time.floatingwindow_move"
/* const/16 v1, 0xfa0 */
v0 = android.os.SystemProperties .getInt ( v0,v1 );
/* .line 89 */
/* nop */
/* .line 90 */
final String v0 = "persist.multisence.pri_level.floatingwindow_move"; // const-string v0, "persist.multisence.pri_level.floatingwindow_move"
v0 = android.os.SystemProperties .getInt ( v0,v2 );
/* .line 89 */
return;
} // .end method
public com.miui.server.multisence.MultiSenceServicePolicy ( ) {
/* .locals 2 */
/* .line 98 */
/* invoke-direct {p0}, Ljava/lang/Object;-><init>()V */
/* .line 55 */
/* new-instance v0, Ljava/util/HashMap; */
/* invoke-direct {v0}, Ljava/util/HashMap;-><init>()V */
this.schedWindows = v0;
/* .line 57 */
int v0 = -1; // const/4 v0, -0x1
/* iput v0, p0, Lcom/miui/server/multisence/MultiSenceServicePolicy;->mFreeformMoveRequestId:I */
/* .line 58 */
/* iput v0, p0, Lcom/miui/server/multisence/MultiSenceServicePolicy;->mFreeformMoveBindCoreRequestId:I */
/* .line 59 */
/* iput v0, p0, Lcom/miui/server/multisence/MultiSenceServicePolicy;->mFreeformResizeRequestId:I */
/* .line 60 */
/* iput v0, p0, Lcom/miui/server/multisence/MultiSenceServicePolicy;->mFreeformResizeBindCoreRequestId:I */
/* .line 61 */
/* iput v0, p0, Lcom/miui/server/multisence/MultiSenceServicePolicy;->mFreeformEnterFullScreenRequestId:I */
/* .line 62 */
/* iput v0, p0, Lcom/miui/server/multisence/MultiSenceServicePolicy;->mFreeformEnterFullScreenBindCoreRequestId:I */
/* .line 63 */
/* iput v0, p0, Lcom/miui/server/multisence/MultiSenceServicePolicy;->mFreeformEludeRequestId:I */
/* .line 64 */
/* iput v0, p0, Lcom/miui/server/multisence/MultiSenceServicePolicy;->mFreeformEludeBindCoreRequestId:I */
/* .line 65 */
/* iput v0, p0, Lcom/miui/server/multisence/MultiSenceServicePolicy;->mSplitscreenRequestId:I */
/* .line 66 */
/* iput v0, p0, Lcom/miui/server/multisence/MultiSenceServicePolicy;->mSplitscreenBindCoreRequestId:I */
/* .line 67 */
/* iput v0, p0, Lcom/miui/server/multisence/MultiSenceServicePolicy;->mMWSRequestId:I */
/* .line 68 */
/* iput v0, p0, Lcom/miui/server/multisence/MultiSenceServicePolicy;->mMWSBindCoreRequestId:I */
/* .line 69 */
/* iput v0, p0, Lcom/miui/server/multisence/MultiSenceServicePolicy;->mFWRequestId:I */
/* .line 70 */
/* iput v0, p0, Lcom/miui/server/multisence/MultiSenceServicePolicy;->mFWBindCoreRequestId:I */
/* .line 71 */
/* new-instance v0, Ljava/util/ArrayList; */
/* invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V */
this.mDefaultRequestIds = v0;
/* .line 96 */
/* new-instance v0, Ljava/util/HashSet; */
/* invoke-direct {v0}, Ljava/util/HashSet;-><init>()V */
this.mVrsWorkList = v0;
/* .line 99 */
/* sget-boolean v0, Lcom/miui/server/multisence/MultiSenceConfig;->DEBUG_POLICY_BASE:Z */
final String v1 = "new MultiSenceServicePolicy"; // const-string v1, "new MultiSenceServicePolicy"
com.miui.server.multisence.MultiSenceServiceUtils .msLogD ( v0,v1 );
/* .line 100 */
return;
} // .end method
private void LOG_IF_DEBUG ( java.lang.String p0 ) {
/* .locals 1 */
/* .param p1, "log" # Ljava/lang/String; */
/* .line 787 */
/* sget-boolean v0, Lcom/miui/server/multisence/MultiSenceServicePolicy;->DEBUG:Z */
if ( v0 != null) { // if-eqz v0, :cond_0
/* .line 788 */
final String v0 = "MultiSenceServicePolicy"; // const-string v0, "MultiSenceServicePolicy"
android.util.Slog .d ( v0,p1 );
/* .line 790 */
} // :cond_0
return;
} // .end method
private void calculateFocusAndPolicy ( ) {
/* .locals 3 */
/* .line 497 */
/* sget-boolean v0, Lcom/miui/server/multisence/MultiSenceConfig;->FREMAPREDICT_ENABLE:Z */
/* if-nez v0, :cond_0 */
/* .line 498 */
final String v0 = "framepredict is not support by multisence"; // const-string v0, "framepredict is not support by multisence"
/* invoke-direct {p0, v0}, Lcom/miui/server/multisence/MultiSenceServicePolicy;->LOG_IF_DEBUG(Ljava/lang/String;)V */
/* .line 499 */
return;
/* .line 501 */
} // :cond_0
/* new-instance v0, Ljava/lang/StringBuilder; */
/* invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V */
final String v1 = "calculateFocusAndPolicy: currentInputFocus="; // const-string v1, "calculateFocusAndPolicy: currentInputFocus="
(( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
v1 = this.currentInputFocus;
(( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
final String v1 = " isDynamicSenceStarting="; // const-string v1, " isDynamicSenceStarting="
(( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
/* iget-boolean v1, p0, Lcom/miui/server/multisence/MultiSenceServicePolicy;->isDynamicSenceStarting:Z */
(( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;
final String v1 = " dynamicSencePackage"; // const-string v1, " dynamicSencePackage"
(( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
v1 = this.dynamicSencePackage;
(( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v0 ).toString ( ); // invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
/* invoke-direct {p0, v0}, Lcom/miui/server/multisence/MultiSenceServicePolicy;->LOG_IF_DEBUG(Ljava/lang/String;)V */
/* .line 503 */
/* iget-boolean v0, p0, Lcom/miui/server/multisence/MultiSenceServicePolicy;->isDynamicSenceStarting:Z */
/* if-nez v0, :cond_1 */
/* .line 504 */
v0 = this.currentInputFocus;
/* .local v0, "newFocus":Ljava/lang/String; */
/* .line 506 */
} // .end local v0 # "newFocus":Ljava/lang/String;
} // :cond_1
v0 = this.dynamicSencePackage;
/* .line 508 */
/* .restart local v0 # "newFocus":Ljava/lang/String; */
} // :goto_0
if ( v0 != null) { // if-eqz v0, :cond_4
final String v1 = ""; // const-string v1, ""
v1 = (( java.lang.String ) v1 ).equals ( v0 ); // invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z
if ( v1 != null) { // if-eqz v1, :cond_2
/* .line 513 */
} // :cond_2
v1 = this.focusPackage;
v1 = (( java.lang.String ) v0 ).equals ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z
/* if-nez v1, :cond_3 */
/* .line 514 */
/* new-instance v1, Ljava/lang/StringBuilder; */
/* invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V */
final String v2 = "final focus changed: "; // const-string v2, "final focus changed: "
(( java.lang.StringBuilder ) v1 ).append ( v2 ); // invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v1 ).append ( v0 ); // invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
final String v2 = " old focus="; // const-string v2, " old focus="
(( java.lang.StringBuilder ) v1 ).append ( v2 ); // invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
v2 = this.focusPackage;
(( java.lang.StringBuilder ) v1 ).append ( v2 ); // invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v1 ).toString ( ); // invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
/* invoke-direct {p0, v1}, Lcom/miui/server/multisence/MultiSenceServicePolicy;->LOG_IF_DEBUG(Ljava/lang/String;)V */
/* .line 515 */
this.focusPackage = v0;
/* .line 516 */
com.android.server.am.MiuiBoosterUtilsStub .getInstance ( );
v2 = this.focusPackage;
/* .line 518 */
} // :cond_3
return;
/* .line 509 */
} // :cond_4
} // :goto_1
final String v1 = "calculateFocusAndPolicy: fail, newFocus is null"; // const-string v1, "calculateFocusAndPolicy: fail, newFocus is null"
/* invoke-direct {p0, v1}, Lcom/miui/server/multisence/MultiSenceServicePolicy;->LOG_IF_DEBUG(Ljava/lang/String;)V */
/* .line 510 */
return;
} // .end method
private void frameDynamicFpsSched ( ) {
/* .locals 7 */
/* .line 287 */
/* sget-boolean v0, Lcom/miui/server/multisence/MultiSenceConfig;->DYN_FPS_ENABLED:Z */
/* if-nez v0, :cond_2 */
/* .line 288 */
/* sget-boolean v0, Lcom/miui/server/multisence/MultiSenceConfig;->DEBUG_DYN_FPS:Z */
/* if-nez v0, :cond_1 */
/* sget-boolean v0, Lcom/miui/server/multisence/MultiSenceConfig;->DEBUG_CONFIG:Z */
if ( v0 != null) { // if-eqz v0, :cond_0
} // :cond_0
int v0 = 0; // const/4 v0, 0x0
} // :cond_1
} // :goto_0
int v0 = 1; // const/4 v0, 0x1
} // :goto_1
final String v1 = "dyn fps is not supported by multisence config"; // const-string v1, "dyn fps is not supported by multisence config"
com.miui.server.multisence.MultiSenceServiceUtils .msLogD ( v0,v1 );
/* .line 289 */
return;
/* .line 291 */
} // :cond_2
/* new-instance v0, Ljava/lang/StringBuilder; */
final String v1 = ""; // const-string v1, ""
/* invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V */
/* .line 292 */
/* .local v0, "dynFpsCmd":Ljava/lang/StringBuilder; */
v1 = this.schedWindows;
v2 = } // :goto_2
final String v3 = "\n"; // const-string v3, "\n"
if ( v2 != null) { // if-eqz v2, :cond_7
/* check-cast v2, Ljava/lang/String; */
/* .line 293 */
/* .local v2, "appName":Ljava/lang/String; */
/* if-nez v2, :cond_3 */
/* .line 294 */
} // :cond_3
v4 = this.schedWindows;
/* check-cast v4, Lcom/miui/server/multisence/SingleWindowInfo; */
/* .line 295 */
/* .local v4, "app":Lcom/miui/server/multisence/SingleWindowInfo; */
/* if-nez v4, :cond_4 */
/* .line 297 */
} // :cond_4
(( com.miui.server.multisence.SingleWindowInfo ) v4 ).getChangeStatus ( ); // invoke-virtual {v4}, Lcom/miui/server/multisence/SingleWindowInfo;->getChangeStatus()Lcom/miui/server/multisence/SingleWindowInfo$WindowChangeState;
v6 = com.miui.server.multisence.SingleWindowInfo$WindowChangeState.RESET;
/* if-ne v5, v6, :cond_5 */
/* .line 298 */
/* .line 301 */
} // :cond_5
v5 = com.miui.server.multisence.SingleWindowInfo$ScheduleStatus.DYN_FPS_SET;
v5 = (( com.miui.server.multisence.SingleWindowInfo ) v4 ).checkSchedStatus ( v5 ); // invoke-virtual {v4, v5}, Lcom/miui/server/multisence/SingleWindowInfo;->checkSchedStatus(Lcom/miui/server/multisence/SingleWindowInfo$ScheduleStatus;)Z
if ( v5 != null) { // if-eqz v5, :cond_6
/* .line 302 */
/* new-instance v5, Ljava/lang/StringBuilder; */
/* invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V */
(( java.lang.StringBuilder ) v5 ).append ( v2 ); // invoke-virtual {v5, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
final String v6 = " "; // const-string v6, " "
(( java.lang.StringBuilder ) v5 ).append ( v6 ); // invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
v6 = (( com.miui.server.multisence.SingleWindowInfo ) v4 ).getLimitFPS ( ); // invoke-virtual {v4}, Lcom/miui/server/multisence/SingleWindowInfo;->getLimitFPS()I
(( java.lang.StringBuilder ) v5 ).append ( v6 ); // invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v5 ).append ( v3 ); // invoke-virtual {v5, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v3 ).toString ( ); // invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
(( java.lang.StringBuilder ) v0 ).append ( v3 ); // invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
/* .line 304 */
} // .end local v2 # "appName":Ljava/lang/String;
} // .end local v4 # "app":Lcom/miui/server/multisence/SingleWindowInfo;
} // :cond_6
/* .line 305 */
} // :cond_7
v1 = (( java.lang.StringBuilder ) v0 ).length ( ); // invoke-virtual {v0}, Ljava/lang/StringBuilder;->length()I
/* if-nez v1, :cond_8 */
/* .line 306 */
(( java.lang.StringBuilder ) v0 ).append ( v3 ); // invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
/* .line 308 */
} // :cond_8
/* sget-boolean v1, Lcom/miui/server/multisence/MultiSenceConfig;->DEBUG_DYN_FPS:Z */
/* new-instance v2, Ljava/lang/StringBuilder; */
/* invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V */
final String v3 = "App fps cmd: "; // const-string v3, "App fps cmd: "
(( java.lang.StringBuilder ) v2 ).append ( v3 ); // invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v0 ).toString ( ); // invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
(( java.lang.StringBuilder ) v2 ).append ( v3 ); // invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v2 ).toString ( ); // invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
com.miui.server.multisence.MultiSenceServiceUtils .msLogD ( v1,v2 );
/* .line 309 */
final String v1 = "/data/system/mcd/mwdf"; // const-string v1, "/data/system/mcd/mwdf"
(( java.lang.StringBuilder ) v0 ).toString ( ); // invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
com.miui.server.multisence.MultiSenceServiceUtils .writeToFile ( v1,v2 );
/* .line 310 */
return;
} // .end method
private void frameVrsSched ( ) {
/* .locals 8 */
/* .line 313 */
/* sget-boolean v0, Lcom/miui/server/multisence/MultiSenceConfig;->VRS_ENABLE:Z */
/* if-nez v0, :cond_2 */
/* .line 314 */
/* sget-boolean v0, Lcom/miui/server/multisence/MultiSenceConfig;->DEBUG_VRS:Z */
/* if-nez v0, :cond_1 */
/* sget-boolean v0, Lcom/miui/server/multisence/MultiSenceConfig;->DEBUG_CONFIG:Z */
if ( v0 != null) { // if-eqz v0, :cond_0
} // :cond_0
int v0 = 0; // const/4 v0, 0x0
} // :cond_1
} // :goto_0
int v0 = 1; // const/4 v0, 0x1
} // :goto_1
/* const-string/jumbo v1, "vrs is not supported by multisence config" */
com.miui.server.multisence.MultiSenceServiceUtils .msLogD ( v0,v1 );
/* .line 315 */
return;
/* .line 317 */
} // :cond_2
/* new-instance v0, Ljava/lang/StringBuilder; */
final String v1 = ""; // const-string v1, ""
/* invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V */
/* .line 318 */
/* .local v0, "vrsCmd":Ljava/lang/StringBuilder; */
int v1 = 0; // const/4 v1, 0x0
/* .line 319 */
/* .local v1, "count":I */
v2 = this.schedWindows;
v3 = } // :goto_2
if ( v3 != null) { // if-eqz v3, :cond_a
/* check-cast v3, Ljava/lang/String; */
/* .line 320 */
/* .local v3, "appName":Ljava/lang/String; */
/* if-nez v3, :cond_3 */
/* .line 321 */
} // :cond_3
v4 = this.schedWindows;
/* check-cast v4, Lcom/miui/server/multisence/SingleWindowInfo; */
/* .line 322 */
/* .local v4, "app":Lcom/miui/server/multisence/SingleWindowInfo; */
/* if-nez v4, :cond_4 */
/* .line 323 */
} // :cond_4
com.miui.server.multisence.MultiSenceConfig .getInstance ( );
v5 = (( com.miui.server.multisence.MultiSenceConfig ) v5 ).checkVrsSupport ( v3 ); // invoke-virtual {v5, v3}, Lcom/miui/server/multisence/MultiSenceConfig;->checkVrsSupport(Ljava/lang/String;)Z
/* if-nez v5, :cond_5 */
/* .line 324 */
/* sget-boolean v5, Lcom/miui/server/multisence/MultiSenceConfig;->DEBUG_VRS:Z */
/* new-instance v6, Ljava/lang/StringBuilder; */
/* invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V */
(( java.lang.StringBuilder ) v6 ).append ( v3 ); // invoke-virtual {v6, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
final String v7 = " is not it vrs whitelist."; // const-string v7, " is not it vrs whitelist."
(( java.lang.StringBuilder ) v6 ).append ( v7 ); // invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v6 ).toString ( ); // invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
com.miui.server.multisence.MultiSenceServiceUtils .msLogD ( v5,v6 );
/* .line 325 */
/* .line 328 */
} // :cond_5
(( com.miui.server.multisence.SingleWindowInfo ) v4 ).getChangeStatus ( ); // invoke-virtual {v4}, Lcom/miui/server/multisence/SingleWindowInfo;->getChangeStatus()Lcom/miui/server/multisence/SingleWindowInfo$WindowChangeState;
v6 = com.miui.server.multisence.SingleWindowInfo$WindowChangeState.RESET;
/* if-ne v5, v6, :cond_7 */
/* .line 329 */
v5 = /* invoke-direct {p0, v0, v3}, Lcom/miui/server/multisence/MultiSenceServicePolicy;->setVrsOff(Ljava/lang/StringBuilder;Ljava/lang/String;)Z */
if ( v5 != null) { // if-eqz v5, :cond_6
/* add-int/lit8 v1, v1, 0x1 */
/* .line 330 */
} // :cond_6
/* .line 332 */
} // :cond_7
v5 = com.miui.server.multisence.SingleWindowInfo$ScheduleStatus.VRS_RESET;
v5 = (( com.miui.server.multisence.SingleWindowInfo ) v4 ).checkSchedStatus ( v5 ); // invoke-virtual {v4, v5}, Lcom/miui/server/multisence/SingleWindowInfo;->checkSchedStatus(Lcom/miui/server/multisence/SingleWindowInfo$ScheduleStatus;)Z
if ( v5 != null) { // if-eqz v5, :cond_8
/* .line 333 */
v5 = /* invoke-direct {p0, v0, v3}, Lcom/miui/server/multisence/MultiSenceServicePolicy;->setVrsOff(Ljava/lang/StringBuilder;Ljava/lang/String;)Z */
if ( v5 != null) { // if-eqz v5, :cond_9
/* add-int/lit8 v1, v1, 0x1 */
/* .line 334 */
} // :cond_8
v5 = com.miui.server.multisence.SingleWindowInfo$ScheduleStatus.VRS_SET;
v5 = (( com.miui.server.multisence.SingleWindowInfo ) v4 ).checkSchedStatus ( v5 ); // invoke-virtual {v4, v5}, Lcom/miui/server/multisence/SingleWindowInfo;->checkSchedStatus(Lcom/miui/server/multisence/SingleWindowInfo$ScheduleStatus;)Z
if ( v5 != null) { // if-eqz v5, :cond_9
/* .line 335 */
v5 = /* invoke-direct {p0, v0, v3}, Lcom/miui/server/multisence/MultiSenceServicePolicy;->setVrsOn(Ljava/lang/StringBuilder;Ljava/lang/String;)Z */
if ( v5 != null) { // if-eqz v5, :cond_9
/* add-int/lit8 v1, v1, 0x1 */
/* .line 337 */
} // .end local v3 # "appName":Ljava/lang/String;
} // .end local v4 # "app":Lcom/miui/server/multisence/SingleWindowInfo;
} // :cond_9
} // :goto_3
/* .line 338 */
} // :cond_a
final String v2 = "\n"; // const-string v2, "\n"
(( java.lang.StringBuilder ) v0 ).append ( v2 ); // invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
/* .line 339 */
/* invoke-direct {p0}, Lcom/miui/server/multisence/MultiSenceServicePolicy;->showVrsWorkApp()V */
/* .line 340 */
/* sget-boolean v2, Lcom/miui/server/multisence/MultiSenceConfig;->DEBUG_VRS:Z */
/* new-instance v3, Ljava/lang/StringBuilder; */
/* invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V */
java.lang.Integer .toString ( v1 );
(( java.lang.StringBuilder ) v3 ).append ( v4 ); // invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
final String v4 = " comonds for vrs-sched: "; // const-string v4, " comonds for vrs-sched: "
(( java.lang.StringBuilder ) v3 ).append ( v4 ); // invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v0 ).toString ( ); // invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
(( java.lang.StringBuilder ) v3 ).append ( v4 ); // invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v3 ).toString ( ); // invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
com.miui.server.multisence.MultiSenceServiceUtils .msLogD ( v2,v3 );
/* .line 341 */
/* if-gtz v1, :cond_b */
/* .line 342 */
return;
/* .line 344 */
} // :cond_b
int v2 = 2; // const/4 v2, 0x2
(( java.lang.StringBuilder ) v0 ).toString ( ); // invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
(( com.miui.server.multisence.MultiSenceServicePolicy ) p0 ).setCommonVrsParams ( v2, v3 ); // invoke-virtual {p0, v2, v3}, Lcom/miui/server/multisence/MultiSenceServicePolicy;->setCommonVrsParams(ILjava/lang/String;)V
/* .line 345 */
return;
} // .end method
static Integer lambda$rectangleArea$0 ( Integer[] p0, Integer[] p1 ) { //synthethic
/* .locals 3 */
/* .param p0, "l1" # [I */
/* .param p1, "l2" # [I */
/* .line 437 */
int v0 = 0; // const/4 v0, 0x0
/* aget v1, p0, v0 */
/* aget v2, p1, v0 */
/* if-eq v1, v2, :cond_0 */
/* aget v1, p0, v0 */
/* aget v0, p1, v0 */
} // :cond_0
int v0 = 1; // const/4 v0, 0x1
/* aget v1, p0, v0 */
/* aget v0, p1, v0 */
} // :goto_0
/* sub-int/2addr v1, v0 */
} // .end method
private void perfSenceSched ( ) {
/* .locals 0 */
/* .line 260 */
/* invoke-direct {p0}, Lcom/miui/server/multisence/MultiSenceServicePolicy;->frameDynamicFpsSched()V */
/* .line 261 */
/* invoke-direct {p0}, Lcom/miui/server/multisence/MultiSenceServicePolicy;->frameVrsSched()V */
/* .line 262 */
return;
} // .end method
private Integer requestBindCore ( Integer p0, Integer[] p1, Integer p2, Integer p3 ) {
/* .locals 1 */
/* .param p1, "uid" # I */
/* .param p2, "tids" # [I */
/* .param p3, "timeout" # I */
/* .param p4, "level" # I */
/* .line 735 */
v0 = com.android.server.am.MiuiBoosterUtilsStub .getInstance ( );
} // .end method
private Integer requestThreadPriority ( Integer p0, Integer[] p1, Integer p2, Integer p3 ) {
/* .locals 1 */
/* .param p1, "uid" # I */
/* .param p2, "tids" # [I */
/* .param p3, "timeout" # I */
/* .param p4, "level" # I */
/* .line 731 */
v0 = com.android.server.am.MiuiBoosterUtilsStub .getInstance ( );
} // .end method
private Boolean setVrsOff ( java.lang.StringBuilder p0, java.lang.String p1 ) {
/* .locals 2 */
/* .param p1, "cmdList" # Ljava/lang/StringBuilder; */
/* .param p2, "name" # Ljava/lang/String; */
/* .line 276 */
/* if-nez p1, :cond_0 */
/* .line 277 */
int v0 = 0; // const/4 v0, 0x0
/* .line 279 */
} // :cond_0
/* new-instance v0, Ljava/lang/StringBuilder; */
/* invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V */
(( java.lang.StringBuilder ) v0 ).append ( p2 ); // invoke-virtual {v0, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
final String v1 = "#0\n"; // const-string v1, "#0\n"
(( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v0 ).toString ( ); // invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
(( java.lang.StringBuilder ) p1 ).append ( v0 ); // invoke-virtual {p1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
/* .line 280 */
v0 = v0 = this.mVrsWorkList;
if ( v0 != null) { // if-eqz v0, :cond_1
/* .line 281 */
v0 = this.mVrsWorkList;
/* .line 283 */
} // :cond_1
int v0 = 1; // const/4 v0, 0x1
} // .end method
private Boolean setVrsOn ( java.lang.StringBuilder p0, java.lang.String p1 ) {
/* .locals 2 */
/* .param p1, "cmdList" # Ljava/lang/StringBuilder; */
/* .param p2, "name" # Ljava/lang/String; */
/* .line 265 */
/* if-nez p1, :cond_0 */
/* .line 266 */
int v0 = 0; // const/4 v0, 0x0
/* .line 268 */
} // :cond_0
/* new-instance v0, Ljava/lang/StringBuilder; */
/* invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V */
(( java.lang.StringBuilder ) v0 ).append ( p2 ); // invoke-virtual {v0, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
final String v1 = "#1\n"; // const-string v1, "#1\n"
(( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v0 ).toString ( ); // invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
(( java.lang.StringBuilder ) p1 ).append ( v0 ); // invoke-virtual {p1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
/* .line 269 */
v0 = v0 = this.mVrsWorkList;
/* if-nez v0, :cond_1 */
/* .line 270 */
v0 = this.mVrsWorkList;
/* .line 272 */
} // :cond_1
int v0 = 1; // const/4 v0, 0x1
} // .end method
private void showSchedWindowInfo ( ) {
/* .locals 5 */
/* .line 769 */
v0 = this.schedWindows;
v1 = } // :goto_0
if ( v1 != null) { // if-eqz v1, :cond_0
/* check-cast v1, Ljava/lang/String; */
/* .line 770 */
/* .local v1, "name":Ljava/lang/String; */
v2 = this.schedWindows;
/* check-cast v2, Lcom/miui/server/multisence/SingleWindowInfo; */
/* .line 771 */
/* .local v2, "app":Lcom/miui/server/multisence/SingleWindowInfo; */
/* sget-boolean v3, Lcom/miui/server/multisence/MultiSenceConfig;->DEBUG_POLICY_BASE:Z */
(( com.miui.server.multisence.SingleWindowInfo ) v2 ).toString ( ); // invoke-virtual {v2}, Lcom/miui/server/multisence/SingleWindowInfo;->toString()Ljava/lang/String;
com.miui.server.multisence.MultiSenceServiceUtils .msLogD ( v3,v4 );
/* .line 772 */
} // .end local v1 # "name":Ljava/lang/String;
} // .end local v2 # "app":Lcom/miui/server/multisence/SingleWindowInfo;
/* .line 773 */
} // :cond_0
return;
} // .end method
private void showVrsWorkApp ( ) {
/* .locals 5 */
/* .line 776 */
/* sget-boolean v0, Lcom/miui/server/multisence/MultiSenceConfig;->DEBUG_VRS:Z */
/* if-nez v0, :cond_0 */
/* .line 777 */
return;
/* .line 779 */
} // :cond_0
/* new-instance v0, Ljava/lang/StringBuilder; */
/* new-instance v1, Ljava/lang/StringBuilder; */
/* invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V */
final String v2 = "VRS works in "; // const-string v2, "VRS works in "
(( java.lang.StringBuilder ) v1 ).append ( v2 ); // invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
v2 = v2 = this.mVrsWorkList;
java.lang.Integer .toString ( v2 );
(( java.lang.StringBuilder ) v1 ).append ( v2 ); // invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
final String v2 = " apps:"; // const-string v2, " apps:"
(( java.lang.StringBuilder ) v1 ).append ( v2 ); // invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v1 ).toString ( ); // invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
/* invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V */
/* .line 780 */
/* .local v0, "str":Ljava/lang/StringBuilder; */
v1 = this.mVrsWorkList;
v2 = } // :goto_0
if ( v2 != null) { // if-eqz v2, :cond_1
/* check-cast v2, Ljava/lang/String; */
/* .line 781 */
/* .local v2, "name":Ljava/lang/String; */
/* new-instance v3, Ljava/lang/StringBuilder; */
/* invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V */
final String v4 = " "; // const-string v4, " "
(( java.lang.StringBuilder ) v3 ).append ( v4 ); // invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v3 ).append ( v2 ); // invoke-virtual {v3, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v3 ).toString ( ); // invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
(( java.lang.StringBuilder ) v0 ).append ( v3 ); // invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
/* .line 782 */
} // .end local v2 # "name":Ljava/lang/String;
/* .line 783 */
} // :cond_1
/* sget-boolean v1, Lcom/miui/server/multisence/MultiSenceConfig;->DEBUG_VRS:Z */
(( java.lang.StringBuilder ) v0 ).toString ( ); // invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
com.miui.server.multisence.MultiSenceServiceUtils .msLogD ( v1,v2 );
/* .line 784 */
return;
} // .end method
private void singleAppSched ( ) {
/* .locals 4 */
/* .line 471 */
v0 = this.schedWindows;
v1 = } // :goto_0
if ( v1 != null) { // if-eqz v1, :cond_0
/* check-cast v1, Ljava/lang/String; */
/* .line 474 */
/* .local v1, "key":Ljava/lang/String; */
v2 = this.schedWindows;
/* check-cast v2, Lcom/miui/server/multisence/SingleWindowInfo; */
v3 = com.miui.server.multisence.SingleWindowInfo$WindowChangeState.NOT_CHANGED;
(( com.miui.server.multisence.SingleWindowInfo ) v2 ).setChangeStatus ( v3 ); // invoke-virtual {v2, v3}, Lcom/miui/server/multisence/SingleWindowInfo;->setChangeStatus(Lcom/miui/server/multisence/SingleWindowInfo$WindowChangeState;)Lcom/miui/server/multisence/SingleWindowInfo;
/* .line 475 */
} // .end local v1 # "key":Ljava/lang/String;
/* .line 476 */
} // :cond_0
return;
} // .end method
private void updateExistedWindowPolicy ( java.util.Map p0, com.miui.server.multisence.SingleWindowInfo p1, com.miui.server.multisence.SingleWindowInfo p2 ) {
/* .locals 7 */
/* .param p2, "newWindow" # Lcom/miui/server/multisence/SingleWindowInfo; */
/* .param p3, "oldWindow" # Lcom/miui/server/multisence/SingleWindowInfo; */
/* .annotation system Ldalvik/annotation/Signature; */
/* value = { */
/* "(", */
/* "Ljava/util/Map<", */
/* "Ljava/lang/String;", */
/* "Lcom/miui/server/multisence/SingleWindowInfo;", */
/* ">;", */
/* "Lcom/miui/server/multisence/SingleWindowInfo;", */
/* "Lcom/miui/server/multisence/SingleWindowInfo;", */
/* ")V" */
/* } */
} // .end annotation
/* .line 167 */
/* .local p1, "schedList":Ljava/util/Map;, "Ljava/util/Map<Ljava/lang/String;Lcom/miui/server/multisence/SingleWindowInfo;>;" */
(( com.miui.server.multisence.SingleWindowInfo ) p2 ).getPackageName ( ); // invoke-virtual {p2}, Lcom/miui/server/multisence/SingleWindowInfo;->getPackageName()Ljava/lang/String;
/* .line 168 */
/* .local v0, "name":Ljava/lang/String; */
v1 = (( com.miui.server.multisence.SingleWindowInfo ) p2 ).getWindowCount ( ); // invoke-virtual {p2}, Lcom/miui/server/multisence/SingleWindowInfo;->getWindowCount()I
/* .line 169 */
/* .local v1, "count":I */
(( com.miui.server.multisence.SingleWindowInfo ) p3 ).getPackageName ( ); // invoke-virtual {p3}, Lcom/miui/server/multisence/SingleWindowInfo;->getPackageName()Ljava/lang/String;
/* .line 170 */
/* .local v2, "oldName":Ljava/lang/String; */
if ( v0 != null) { // if-eqz v0, :cond_0
v3 = (( java.lang.String ) v0 ).equals ( v2 ); // invoke-virtual {v0, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z
/* if-nez v3, :cond_0 */
/* .line 171 */
return;
/* .line 173 */
} // :cond_0
int v3 = 0; // const/4 v3, 0x0
/* .line 174 */
/* .local v3, "isChanged":Z */
v4 = (( com.miui.server.multisence.SingleWindowInfo ) p2 ).isInputFocused ( ); // invoke-virtual {p2}, Lcom/miui/server/multisence/SingleWindowInfo;->isInputFocused()Z
v5 = (( com.miui.server.multisence.SingleWindowInfo ) p3 ).isInputFocused ( ); // invoke-virtual {p3}, Lcom/miui/server/multisence/SingleWindowInfo;->isInputFocused()Z
/* if-eq v4, v5, :cond_1 */
/* .line 175 */
int v3 = 1; // const/4 v3, 0x1
/* .line 178 */
} // :cond_1
(( com.miui.server.multisence.SingleWindowInfo ) p2 ).getWindowingModeString ( ); // invoke-virtual {p2}, Lcom/miui/server/multisence/SingleWindowInfo;->getWindowingModeString()Ljava/lang/String;
(( com.miui.server.multisence.SingleWindowInfo ) p3 ).getWindowingModeString ( ); // invoke-virtual {p3}, Lcom/miui/server/multisence/SingleWindowInfo;->getWindowingModeString()Ljava/lang/String;
v4 = (( java.lang.String ) v4 ).equals ( v5 ); // invoke-virtual {v4, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z
/* if-nez v4, :cond_2 */
/* .line 179 */
int v3 = 1; // const/4 v3, 0x1
/* .line 189 */
} // :cond_2
v4 = (( com.miui.server.multisence.SingleWindowInfo ) p2 ).isVrsCovered ( ); // invoke-virtual {p2}, Lcom/miui/server/multisence/SingleWindowInfo;->isVrsCovered()Z
/* if-nez v4, :cond_3 */
v4 = (( com.miui.server.multisence.SingleWindowInfo ) p3 ).isVrsCovered ( ); // invoke-virtual {p3}, Lcom/miui/server/multisence/SingleWindowInfo;->isVrsCovered()Z
if ( v4 != null) { // if-eqz v4, :cond_3
/* .line 190 */
v4 = com.miui.server.multisence.SingleWindowInfo$ScheduleStatus.VRS_RESET;
(( com.miui.server.multisence.SingleWindowInfo ) p2 ).setSchedStatus ( v4 ); // invoke-virtual {p2, v4}, Lcom/miui/server/multisence/SingleWindowInfo;->setSchedStatus(Lcom/miui/server/multisence/SingleWindowInfo$ScheduleStatus;)Lcom/miui/server/multisence/SingleWindowInfo;
/* .line 191 */
int v3 = 1; // const/4 v3, 0x1
/* .line 193 */
} // :cond_3
v4 = (( com.miui.server.multisence.SingleWindowInfo ) p2 ).isCovered ( ); // invoke-virtual {p2}, Lcom/miui/server/multisence/SingleWindowInfo;->isCovered()Z
int v5 = 1; // const/4 v5, 0x1
if ( v4 != null) { // if-eqz v4, :cond_6
/* .line 194 */
/* if-ne v1, v5, :cond_4 */
/* .line 195 */
v4 = com.miui.server.multisence.SingleWindowInfo$ScheduleStatus.DYN_FPS_SET;
(( com.miui.server.multisence.SingleWindowInfo ) p2 ).setSchedStatus ( v4 ); // invoke-virtual {p2, v4}, Lcom/miui/server/multisence/SingleWindowInfo;->setSchedStatus(Lcom/miui/server/multisence/SingleWindowInfo$ScheduleStatus;)Lcom/miui/server/multisence/SingleWindowInfo;
/* .line 197 */
} // :cond_4
v4 = (( com.miui.server.multisence.SingleWindowInfo ) p2 ).isVrsCovered ( ); // invoke-virtual {p2}, Lcom/miui/server/multisence/SingleWindowInfo;->isVrsCovered()Z
if ( v4 != null) { // if-eqz v4, :cond_5
v4 = (( com.miui.server.multisence.SingleWindowInfo ) p3 ).isVrsCovered ( ); // invoke-virtual {p3}, Lcom/miui/server/multisence/SingleWindowInfo;->isVrsCovered()Z
/* if-nez v4, :cond_5 */
/* .line 198 */
v4 = com.miui.server.multisence.SingleWindowInfo$ScheduleStatus.VRS_SET;
(( com.miui.server.multisence.SingleWindowInfo ) p2 ).setSchedStatus ( v4 ); // invoke-virtual {p2, v4}, Lcom/miui/server/multisence/SingleWindowInfo;->setSchedStatus(Lcom/miui/server/multisence/SingleWindowInfo$ScheduleStatus;)Lcom/miui/server/multisence/SingleWindowInfo;
/* .line 200 */
} // :cond_5
int v3 = 1; // const/4 v3, 0x1
/* .line 210 */
} // :cond_6
(( com.miui.server.multisence.SingleWindowInfo ) p2 ).getWindowForm ( ); // invoke-virtual {p2}, Lcom/miui/server/multisence/SingleWindowInfo;->getWindowForm()Lcom/miui/server/multisence/SingleWindowInfo$WindowForm;
v6 = com.miui.server.multisence.SingleWindowInfo$WindowForm.MUTIL_FREEDOM_MINI;
/* if-ne v4, v6, :cond_9 */
/* .line 211 */
/* if-ne v1, v5, :cond_7 */
/* .line 212 */
v4 = com.miui.server.multisence.SingleWindowInfo$ScheduleStatus.DYN_FPS_SET;
(( com.miui.server.multisence.SingleWindowInfo ) p2 ).setSchedStatus ( v4 ); // invoke-virtual {p2, v4}, Lcom/miui/server/multisence/SingleWindowInfo;->setSchedStatus(Lcom/miui/server/multisence/SingleWindowInfo$ScheduleStatus;)Lcom/miui/server/multisence/SingleWindowInfo;
/* .line 214 */
} // :cond_7
(( com.miui.server.multisence.SingleWindowInfo ) p3 ).getWindowForm ( ); // invoke-virtual {p3}, Lcom/miui/server/multisence/SingleWindowInfo;->getWindowForm()Lcom/miui/server/multisence/SingleWindowInfo$WindowForm;
v5 = com.miui.server.multisence.SingleWindowInfo$WindowForm.MUTIL_FREEDOM_MINI;
/* if-eq v4, v5, :cond_8 */
/* .line 215 */
v4 = com.miui.server.multisence.SingleWindowInfo$ScheduleStatus.VRS_SET;
(( com.miui.server.multisence.SingleWindowInfo ) p2 ).setSchedStatus ( v4 ); // invoke-virtual {p2, v4}, Lcom/miui/server/multisence/SingleWindowInfo;->setSchedStatus(Lcom/miui/server/multisence/SingleWindowInfo$ScheduleStatus;)Lcom/miui/server/multisence/SingleWindowInfo;
/* .line 217 */
} // :cond_8
int v3 = 1; // const/4 v3, 0x1
/* .line 219 */
} // :cond_9
(( com.miui.server.multisence.SingleWindowInfo ) p3 ).getWindowForm ( ); // invoke-virtual {p3}, Lcom/miui/server/multisence/SingleWindowInfo;->getWindowForm()Lcom/miui/server/multisence/SingleWindowInfo$WindowForm;
v5 = com.miui.server.multisence.SingleWindowInfo$WindowForm.MUTIL_FREEDOM_MINI;
/* if-ne v4, v5, :cond_a */
/* .line 220 */
v4 = com.miui.server.multisence.SingleWindowInfo$ScheduleStatus.VRS_RESET;
(( com.miui.server.multisence.SingleWindowInfo ) p2 ).setSchedStatus ( v4 ); // invoke-virtual {p2, v4}, Lcom/miui/server/multisence/SingleWindowInfo;->setSchedStatus(Lcom/miui/server/multisence/SingleWindowInfo$ScheduleStatus;)Lcom/miui/server/multisence/SingleWindowInfo;
/* .line 221 */
int v3 = 1; // const/4 v3, 0x1
/* .line 225 */
} // :cond_a
} // :goto_0
if ( v3 != null) { // if-eqz v3, :cond_b
v4 = com.miui.server.multisence.SingleWindowInfo$WindowChangeState.CHANGED;
} // :cond_b
v4 = com.miui.server.multisence.SingleWindowInfo$WindowChangeState.NOT_CHANGED;
} // :goto_1
(( com.miui.server.multisence.SingleWindowInfo ) p2 ).setChangeStatus ( v4 ); // invoke-virtual {p2, v4}, Lcom/miui/server/multisence/SingleWindowInfo;->setChangeStatus(Lcom/miui/server/multisence/SingleWindowInfo$WindowChangeState;)Lcom/miui/server/multisence/SingleWindowInfo;
/* .line 226 */
v4 = this.schedWindows;
/* .line 227 */
return;
} // .end method
private void updateLeftWindowPolicy ( java.util.Map p0, com.miui.server.multisence.SingleWindowInfo p1 ) {
/* .locals 2 */
/* .param p2, "leftWindow" # Lcom/miui/server/multisence/SingleWindowInfo; */
/* .annotation system Ldalvik/annotation/Signature; */
/* value = { */
/* "(", */
/* "Ljava/util/Map<", */
/* "Ljava/lang/String;", */
/* "Lcom/miui/server/multisence/SingleWindowInfo;", */
/* ">;", */
/* "Lcom/miui/server/multisence/SingleWindowInfo;", */
/* ")V" */
/* } */
} // .end annotation
/* .line 133 */
/* .local p1, "schedList":Ljava/util/Map;, "Ljava/util/Map<Ljava/lang/String;Lcom/miui/server/multisence/SingleWindowInfo;>;" */
(( com.miui.server.multisence.SingleWindowInfo ) p2 ).getPackageName ( ); // invoke-virtual {p2}, Lcom/miui/server/multisence/SingleWindowInfo;->getPackageName()Ljava/lang/String;
/* .line 134 */
/* .local v0, "name":Ljava/lang/String; */
/* if-nez v0, :cond_0 */
/* .line 135 */
return;
/* .line 137 */
} // :cond_0
v1 = com.miui.server.multisence.SingleWindowInfo$WindowChangeState.RESET;
(( com.miui.server.multisence.SingleWindowInfo ) p2 ).setChangeStatus ( v1 ); // invoke-virtual {p2, v1}, Lcom/miui/server/multisence/SingleWindowInfo;->setChangeStatus(Lcom/miui/server/multisence/SingleWindowInfo$WindowChangeState;)Lcom/miui/server/multisence/SingleWindowInfo;
/* .line 138 */
v1 = this.schedWindows;
/* .line 139 */
return;
} // .end method
private void updateNewWindowPolicy ( java.util.Map p0, com.miui.server.multisence.SingleWindowInfo p1 ) {
/* .locals 5 */
/* .param p2, "newWindow" # Lcom/miui/server/multisence/SingleWindowInfo; */
/* .annotation system Ldalvik/annotation/Signature; */
/* value = { */
/* "(", */
/* "Ljava/util/Map<", */
/* "Ljava/lang/String;", */
/* "Lcom/miui/server/multisence/SingleWindowInfo;", */
/* ">;", */
/* "Lcom/miui/server/multisence/SingleWindowInfo;", */
/* ")V" */
/* } */
} // .end annotation
/* .line 142 */
/* .local p1, "schedList":Ljava/util/Map;, "Ljava/util/Map<Ljava/lang/String;Lcom/miui/server/multisence/SingleWindowInfo;>;" */
(( com.miui.server.multisence.SingleWindowInfo ) p2 ).getPackageName ( ); // invoke-virtual {p2}, Lcom/miui/server/multisence/SingleWindowInfo;->getPackageName()Ljava/lang/String;
/* .line 143 */
/* .local v0, "name":Ljava/lang/String; */
v1 = (( com.miui.server.multisence.SingleWindowInfo ) p2 ).getWindowCount ( ); // invoke-virtual {p2}, Lcom/miui/server/multisence/SingleWindowInfo;->getWindowCount()I
/* .line 144 */
/* .local v1, "count":I */
/* if-nez v0, :cond_0 */
/* .line 145 */
return;
/* .line 148 */
} // :cond_0
(( com.miui.server.multisence.SingleWindowInfo ) p2 ).getWindowForm ( ); // invoke-virtual {p2}, Lcom/miui/server/multisence/SingleWindowInfo;->getWindowForm()Lcom/miui/server/multisence/SingleWindowInfo$WindowForm;
v3 = com.miui.server.multisence.SingleWindowInfo$WindowForm.MUTIL_FREEDOM_MINI;
int v4 = 1; // const/4 v4, 0x1
/* if-ne v2, v3, :cond_1 */
/* .line 149 */
v2 = com.miui.server.multisence.SingleWindowInfo$ScheduleStatus.VRS_SET;
(( com.miui.server.multisence.SingleWindowInfo ) p2 ).setSchedStatus ( v2 ); // invoke-virtual {p2, v2}, Lcom/miui/server/multisence/SingleWindowInfo;->setSchedStatus(Lcom/miui/server/multisence/SingleWindowInfo$ScheduleStatus;)Lcom/miui/server/multisence/SingleWindowInfo;
/* .line 150 */
/* if-ne v1, v4, :cond_1 */
/* .line 151 */
v2 = com.miui.server.multisence.SingleWindowInfo$ScheduleStatus.DYN_FPS_SET;
(( com.miui.server.multisence.SingleWindowInfo ) p2 ).setSchedStatus ( v2 ); // invoke-virtual {p2, v2}, Lcom/miui/server/multisence/SingleWindowInfo;->setSchedStatus(Lcom/miui/server/multisence/SingleWindowInfo$ScheduleStatus;)Lcom/miui/server/multisence/SingleWindowInfo;
/* .line 154 */
} // :cond_1
v2 = (( com.miui.server.multisence.SingleWindowInfo ) p2 ).isCovered ( ); // invoke-virtual {p2}, Lcom/miui/server/multisence/SingleWindowInfo;->isCovered()Z
if ( v2 != null) { // if-eqz v2, :cond_3
/* .line 155 */
/* if-ne v1, v4, :cond_2 */
/* .line 156 */
v2 = com.miui.server.multisence.SingleWindowInfo$ScheduleStatus.DYN_FPS_SET;
(( com.miui.server.multisence.SingleWindowInfo ) p2 ).setSchedStatus ( v2 ); // invoke-virtual {p2, v2}, Lcom/miui/server/multisence/SingleWindowInfo;->setSchedStatus(Lcom/miui/server/multisence/SingleWindowInfo$ScheduleStatus;)Lcom/miui/server/multisence/SingleWindowInfo;
/* .line 158 */
} // :cond_2
v2 = (( com.miui.server.multisence.SingleWindowInfo ) p2 ).isVrsCovered ( ); // invoke-virtual {p2}, Lcom/miui/server/multisence/SingleWindowInfo;->isVrsCovered()Z
if ( v2 != null) { // if-eqz v2, :cond_3
/* .line 159 */
v2 = com.miui.server.multisence.SingleWindowInfo$ScheduleStatus.VRS_SET;
(( com.miui.server.multisence.SingleWindowInfo ) p2 ).setSchedStatus ( v2 ); // invoke-virtual {p2, v2}, Lcom/miui/server/multisence/SingleWindowInfo;->setSchedStatus(Lcom/miui/server/multisence/SingleWindowInfo$ScheduleStatus;)Lcom/miui/server/multisence/SingleWindowInfo;
/* .line 162 */
} // :cond_3
v2 = com.miui.server.multisence.SingleWindowInfo$WindowChangeState.CHANGED;
(( com.miui.server.multisence.SingleWindowInfo ) p2 ).setChangeStatus ( v2 ); // invoke-virtual {p2, v2}, Lcom/miui/server/multisence/SingleWindowInfo;->setChangeStatus(Lcom/miui/server/multisence/SingleWindowInfo$WindowChangeState;)Lcom/miui/server/multisence/SingleWindowInfo;
/* .line 163 */
v2 = this.schedWindows;
/* .line 164 */
return;
} // .end method
/* # virtual methods */
public I convertToArray ( java.util.List p0 ) {
/* .locals 6 */
/* .annotation system Ldalvik/annotation/Signature; */
/* value = { */
/* "(", */
/* "Ljava/util/List<", */
/* "Landroid/graphics/Rect;", */
/* ">;)[[I" */
/* } */
} // .end annotation
/* .line 406 */
v0 = /* .local p1, "overLayList":Ljava/util/List;, "Ljava/util/List<Landroid/graphics/Rect;>;" */
/* .line 407 */
/* .local v0, "N":I */
int v1 = 4; // const/4 v1, 0x4
/* filled-new-array {v0, v1}, [I */
v2 = java.lang.Integer.TYPE;
java.lang.reflect.Array .newInstance ( v2,v1 );
/* check-cast v1, [[I */
/* .line 408 */
/* .local v1, "arr":[[I */
int v2 = 0; // const/4 v2, 0x0
/* .local v2, "i":I */
} // :goto_0
/* if-ge v2, v0, :cond_0 */
/* .line 409 */
/* aget-object v3, v1, v2 */
/* check-cast v4, Landroid/graphics/Rect; */
/* iget v4, v4, Landroid/graphics/Rect;->left:I */
int v5 = 0; // const/4 v5, 0x0
/* aput v4, v3, v5 */
/* .line 410 */
/* aget-object v3, v1, v2 */
/* check-cast v4, Landroid/graphics/Rect; */
/* iget v4, v4, Landroid/graphics/Rect;->top:I */
int v5 = 1; // const/4 v5, 0x1
/* aput v4, v3, v5 */
/* .line 411 */
/* aget-object v3, v1, v2 */
/* check-cast v4, Landroid/graphics/Rect; */
/* iget v4, v4, Landroid/graphics/Rect;->right:I */
int v5 = 2; // const/4 v5, 0x2
/* aput v4, v3, v5 */
/* .line 412 */
/* aget-object v3, v1, v2 */
/* check-cast v4, Landroid/graphics/Rect; */
/* iget v4, v4, Landroid/graphics/Rect;->bottom:I */
int v5 = 3; // const/4 v5, 0x3
/* aput v4, v3, v5 */
/* .line 408 */
/* add-int/lit8 v2, v2, 0x1 */
/* .line 414 */
} // .end local v2 # "i":I
} // :cond_0
} // .end method
public void doSched ( ) {
/* .locals 0 */
/* .line 253 */
/* invoke-direct {p0}, Lcom/miui/server/multisence/MultiSenceServicePolicy;->perfSenceSched()V */
/* .line 254 */
/* invoke-direct {p0}, Lcom/miui/server/multisence/MultiSenceServicePolicy;->showSchedWindowInfo()V */
/* .line 256 */
/* invoke-direct {p0}, Lcom/miui/server/multisence/MultiSenceServicePolicy;->singleAppSched()V */
/* .line 257 */
return;
} // .end method
public void dynamicSenceSchedDefault ( Integer p0, Integer[] p1, Boolean p2, java.lang.String p3 ) {
/* .locals 4 */
/* .param p1, "uid" # I */
/* .param p2, "tids" # [I */
/* .param p3, "isStarted" # Z */
/* .param p4, "packageName" # Ljava/lang/String; */
/* .line 709 */
/* iput-boolean p3, p0, Lcom/miui/server/multisence/MultiSenceServicePolicy;->isDynamicSenceStarting:Z */
/* .line 710 */
this.dynamicSencePackage = p4;
/* .line 711 */
/* invoke-direct {p0}, Lcom/miui/server/multisence/MultiSenceServicePolicy;->calculateFocusAndPolicy()V */
/* .line 712 */
if ( p3 != null) { // if-eqz p3, :cond_1
/* .line 713 */
v0 = com.android.server.am.MiuiBoosterUtilsStub .getInstance ( );
/* .line 715 */
/* .local v0, "requsetId":I */
v1 = this.mDefaultRequestIds;
java.lang.Integer .valueOf ( v0 );
v1 = (( java.util.ArrayList ) v1 ).contains ( v2 ); // invoke-virtual {v1, v2}, Ljava/util/ArrayList;->contains(Ljava/lang/Object;)Z
/* if-nez v1, :cond_0 */
/* .line 716 */
v1 = this.mDefaultRequestIds;
java.lang.Integer .valueOf ( v0 );
(( java.util.ArrayList ) v1 ).add ( v2 ); // invoke-virtual {v1, v2}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z
/* .line 717 */
/* new-instance v1, Ljava/lang/StringBuilder; */
/* invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V */
final String v2 = "Default requestId gains: "; // const-string v2, "Default requestId gains: "
(( java.lang.StringBuilder ) v1 ).append ( v2 ); // invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v1 ).append ( v0 ); // invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v1 ).toString ( ); // invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
/* invoke-direct {p0, v1}, Lcom/miui/server/multisence/MultiSenceServicePolicy;->LOG_IF_DEBUG(Ljava/lang/String;)V */
/* .line 719 */
} // .end local v0 # "requsetId":I
} // :cond_0
/* .line 720 */
} // :cond_1
v0 = this.mDefaultRequestIds;
v0 = (( java.util.ArrayList ) v0 ).isEmpty ( ); // invoke-virtual {v0}, Ljava/util/ArrayList;->isEmpty()Z
/* if-nez v0, :cond_3 */
/* .line 721 */
v0 = this.mDefaultRequestIds;
(( java.util.ArrayList ) v0 ).iterator ( ); // invoke-virtual {v0}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;
v1 = } // :goto_0
if ( v1 != null) { // if-eqz v1, :cond_2
/* check-cast v1, Ljava/lang/Integer; */
v1 = (( java.lang.Integer ) v1 ).intValue ( ); // invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I
/* .line 722 */
/* .local v1, "defaultRequestId":I */
/* new-instance v2, Ljava/lang/StringBuilder; */
/* invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V */
final String v3 = "defaultRequestId cancel: "; // const-string v3, "defaultRequestId cancel: "
(( java.lang.StringBuilder ) v2 ).append ( v3 ); // invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v2 ).append ( v1 ); // invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v2 ).toString ( ); // invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
/* invoke-direct {p0, v2}, Lcom/miui/server/multisence/MultiSenceServicePolicy;->LOG_IF_DEBUG(Ljava/lang/String;)V */
/* .line 723 */
com.android.server.am.MiuiBoosterUtilsStub .getInstance ( );
/* .line 724 */
} // .end local v1 # "defaultRequestId":I
/* .line 725 */
} // :cond_2
v0 = this.mDefaultRequestIds;
(( java.util.ArrayList ) v0 ).clear ( ); // invoke-virtual {v0}, Ljava/util/ArrayList;->clear()V
/* .line 728 */
} // :cond_3
} // :goto_1
return;
} // .end method
public void dynamicSenceSchedFloatingWindowMove ( Integer p0, Integer[] p1, Boolean p2 ) {
/* .locals 3 */
/* .param p1, "uid" # I */
/* .param p2, "tids" # [I */
/* .param p3, "isStarted" # Z */
/* .line 683 */
/* invoke-direct {p0}, Lcom/miui/server/multisence/MultiSenceServicePolicy;->calculateFocusAndPolicy()V */
/* .line 684 */
int v0 = -1; // const/4 v0, -0x1
if ( p3 != null) { // if-eqz p3, :cond_1
/* .line 685 */
/* iget v1, p0, Lcom/miui/server/multisence/MultiSenceServicePolicy;->mFWRequestId:I */
/* if-ne v1, v0, :cond_0 */
/* .line 686 */
v1 = /* invoke-direct {p0, p1, p2, v1, v2}, Lcom/miui/server/multisence/MultiSenceServicePolicy;->requestThreadPriority(I[III)I */
/* iput v1, p0, Lcom/miui/server/multisence/MultiSenceServicePolicy;->mFWRequestId:I */
/* .line 688 */
/* new-instance v1, Ljava/lang/StringBuilder; */
/* invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V */
final String v2 = "FWMoveRequestId gains: "; // const-string v2, "FWMoveRequestId gains: "
(( java.lang.StringBuilder ) v1 ).append ( v2 ); // invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
/* iget v2, p0, Lcom/miui/server/multisence/MultiSenceServicePolicy;->mFWRequestId:I */
(( java.lang.StringBuilder ) v1 ).append ( v2 ); // invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v1 ).toString ( ); // invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
/* invoke-direct {p0, v1}, Lcom/miui/server/multisence/MultiSenceServicePolicy;->LOG_IF_DEBUG(Ljava/lang/String;)V */
/* .line 691 */
} // :cond_0
/* iget v1, p0, Lcom/miui/server/multisence/MultiSenceServicePolicy;->mFWBindCoreRequestId:I */
/* if-ne v1, v0, :cond_3 */
/* .line 692 */
int v1 = 4; // const/4 v1, 0x4
v0 = /* invoke-direct {p0, p1, p2, v0, v1}, Lcom/miui/server/multisence/MultiSenceServicePolicy;->requestBindCore(I[III)I */
/* iput v0, p0, Lcom/miui/server/multisence/MultiSenceServicePolicy;->mFWBindCoreRequestId:I */
/* .line 693 */
/* new-instance v0, Ljava/lang/StringBuilder; */
/* invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V */
final String v1 = "FWBindCoreRequestId gains: "; // const-string v1, "FWBindCoreRequestId gains: "
(( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
/* iget v1, p0, Lcom/miui/server/multisence/MultiSenceServicePolicy;->mFWBindCoreRequestId:I */
(( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v0 ).toString ( ); // invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
/* invoke-direct {p0, v0}, Lcom/miui/server/multisence/MultiSenceServicePolicy;->LOG_IF_DEBUG(Ljava/lang/String;)V */
/* .line 696 */
} // :cond_1
/* iget v1, p0, Lcom/miui/server/multisence/MultiSenceServicePolicy;->mFWRequestId:I */
/* if-eq v1, v0, :cond_2 */
/* .line 697 */
com.android.server.am.MiuiBoosterUtilsStub .getInstance ( );
/* iget v2, p0, Lcom/miui/server/multisence/MultiSenceServicePolicy;->mFWRequestId:I */
/* .line 698 */
/* iput v0, p0, Lcom/miui/server/multisence/MultiSenceServicePolicy;->mFWRequestId:I */
/* .line 701 */
} // :cond_2
/* iget v1, p0, Lcom/miui/server/multisence/MultiSenceServicePolicy;->mFWBindCoreRequestId:I */
/* if-eq v1, v0, :cond_3 */
/* .line 702 */
com.android.server.am.MiuiBoosterUtilsStub .getInstance ( );
/* iget v2, p0, Lcom/miui/server/multisence/MultiSenceServicePolicy;->mFWBindCoreRequestId:I */
/* .line 703 */
/* iput v0, p0, Lcom/miui/server/multisence/MultiSenceServicePolicy;->mFWBindCoreRequestId:I */
/* .line 706 */
} // :cond_3
} // :goto_0
return;
} // .end method
public void dynamicSenceSchedFreeformActionElude ( Integer p0, Integer[] p1, Boolean p2, java.lang.String p3 ) {
/* .locals 3 */
/* .param p1, "uid" # I */
/* .param p2, "tids" # [I */
/* .param p3, "isStarted" # Z */
/* .param p4, "packageName" # Ljava/lang/String; */
/* .line 602 */
/* iput-boolean p3, p0, Lcom/miui/server/multisence/MultiSenceServicePolicy;->isDynamicSenceStarting:Z */
/* .line 603 */
this.dynamicSencePackage = p4;
/* .line 604 */
/* invoke-direct {p0}, Lcom/miui/server/multisence/MultiSenceServicePolicy;->calculateFocusAndPolicy()V */
/* .line 605 */
int v0 = -1; // const/4 v0, -0x1
if ( p3 != null) { // if-eqz p3, :cond_1
/* .line 606 */
/* iget v1, p0, Lcom/miui/server/multisence/MultiSenceServicePolicy;->mFreeformEludeRequestId:I */
/* if-ne v1, v0, :cond_0 */
/* .line 607 */
v1 = /* invoke-direct {p0, p1, p2, v1, v2}, Lcom/miui/server/multisence/MultiSenceServicePolicy;->requestThreadPriority(I[III)I */
/* iput v1, p0, Lcom/miui/server/multisence/MultiSenceServicePolicy;->mFreeformEludeRequestId:I */
/* .line 608 */
/* new-instance v1, Ljava/lang/StringBuilder; */
/* invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V */
final String v2 = "freeform-elude proi requset id gains: "; // const-string v2, "freeform-elude proi requset id gains: "
(( java.lang.StringBuilder ) v1 ).append ( v2 ); // invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
/* iget v2, p0, Lcom/miui/server/multisence/MultiSenceServicePolicy;->mFreeformEludeRequestId:I */
(( java.lang.StringBuilder ) v1 ).append ( v2 ); // invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v1 ).toString ( ); // invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
/* invoke-direct {p0, v1}, Lcom/miui/server/multisence/MultiSenceServicePolicy;->LOG_IF_DEBUG(Ljava/lang/String;)V */
/* .line 611 */
} // :cond_0
/* iget v1, p0, Lcom/miui/server/multisence/MultiSenceServicePolicy;->mFreeformEludeBindCoreRequestId:I */
/* if-ne v1, v0, :cond_3 */
/* .line 612 */
int v1 = 4; // const/4 v1, 0x4
v0 = /* invoke-direct {p0, p1, p2, v0, v1}, Lcom/miui/server/multisence/MultiSenceServicePolicy;->requestBindCore(I[III)I */
/* iput v0, p0, Lcom/miui/server/multisence/MultiSenceServicePolicy;->mFreeformEludeBindCoreRequestId:I */
/* .line 613 */
/* new-instance v0, Ljava/lang/StringBuilder; */
/* invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V */
final String v1 = "freeform-elude bindcore requset id gains: "; // const-string v1, "freeform-elude bindcore requset id gains: "
(( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
/* iget v1, p0, Lcom/miui/server/multisence/MultiSenceServicePolicy;->mFreeformEludeBindCoreRequestId:I */
(( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v0 ).toString ( ); // invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
/* invoke-direct {p0, v0}, Lcom/miui/server/multisence/MultiSenceServicePolicy;->LOG_IF_DEBUG(Ljava/lang/String;)V */
/* .line 616 */
} // :cond_1
/* iget v1, p0, Lcom/miui/server/multisence/MultiSenceServicePolicy;->mFreeformEludeRequestId:I */
/* if-eq v1, v0, :cond_2 */
/* .line 617 */
com.android.server.am.MiuiBoosterUtilsStub .getInstance ( );
/* iget v2, p0, Lcom/miui/server/multisence/MultiSenceServicePolicy;->mFreeformEludeRequestId:I */
/* .line 618 */
/* iput v0, p0, Lcom/miui/server/multisence/MultiSenceServicePolicy;->mFreeformEludeRequestId:I */
/* .line 621 */
} // :cond_2
/* iget v1, p0, Lcom/miui/server/multisence/MultiSenceServicePolicy;->mFreeformEludeBindCoreRequestId:I */
/* if-eq v1, v0, :cond_3 */
/* .line 622 */
com.android.server.am.MiuiBoosterUtilsStub .getInstance ( );
/* iget v2, p0, Lcom/miui/server/multisence/MultiSenceServicePolicy;->mFreeformEludeBindCoreRequestId:I */
/* .line 623 */
/* iput v0, p0, Lcom/miui/server/multisence/MultiSenceServicePolicy;->mFreeformEludeBindCoreRequestId:I */
/* .line 626 */
} // :cond_3
} // :goto_0
return;
} // .end method
public void dynamicSenceSchedFreeformActionEnterFullScreen ( Integer p0, Integer[] p1, Boolean p2, java.lang.String p3 ) {
/* .locals 3 */
/* .param p1, "uid" # I */
/* .param p2, "tids" # [I */
/* .param p3, "isStarted" # Z */
/* .param p4, "packageName" # Ljava/lang/String; */
/* .line 575 */
/* iput-boolean p3, p0, Lcom/miui/server/multisence/MultiSenceServicePolicy;->isDynamicSenceStarting:Z */
/* .line 576 */
this.dynamicSencePackage = p4;
/* .line 577 */
/* invoke-direct {p0}, Lcom/miui/server/multisence/MultiSenceServicePolicy;->calculateFocusAndPolicy()V */
/* .line 578 */
int v0 = -1; // const/4 v0, -0x1
if ( p3 != null) { // if-eqz p3, :cond_1
/* .line 579 */
/* iget v1, p0, Lcom/miui/server/multisence/MultiSenceServicePolicy;->mFreeformEnterFullScreenRequestId:I */
/* if-ne v1, v0, :cond_0 */
/* .line 580 */
v1 = /* invoke-direct {p0, p1, p2, v1, v2}, Lcom/miui/server/multisence/MultiSenceServicePolicy;->requestThreadPriority(I[III)I */
/* iput v1, p0, Lcom/miui/server/multisence/MultiSenceServicePolicy;->mFreeformEnterFullScreenRequestId:I */
/* .line 581 */
/* new-instance v1, Ljava/lang/StringBuilder; */
/* invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V */
final String v2 = "FreeformEnterFullScreenRequestId gains: "; // const-string v2, "FreeformEnterFullScreenRequestId gains: "
(( java.lang.StringBuilder ) v1 ).append ( v2 ); // invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
/* iget v2, p0, Lcom/miui/server/multisence/MultiSenceServicePolicy;->mFreeformEnterFullScreenRequestId:I */
(( java.lang.StringBuilder ) v1 ).append ( v2 ); // invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v1 ).toString ( ); // invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
/* invoke-direct {p0, v1}, Lcom/miui/server/multisence/MultiSenceServicePolicy;->LOG_IF_DEBUG(Ljava/lang/String;)V */
/* .line 584 */
} // :cond_0
/* iget v1, p0, Lcom/miui/server/multisence/MultiSenceServicePolicy;->mFreeformEnterFullScreenBindCoreRequestId:I */
/* if-ne v1, v0, :cond_3 */
/* .line 585 */
int v1 = 4; // const/4 v1, 0x4
v0 = /* invoke-direct {p0, p1, p2, v0, v1}, Lcom/miui/server/multisence/MultiSenceServicePolicy;->requestBindCore(I[III)I */
/* iput v0, p0, Lcom/miui/server/multisence/MultiSenceServicePolicy;->mFreeformEnterFullScreenBindCoreRequestId:I */
/* .line 586 */
/* new-instance v0, Ljava/lang/StringBuilder; */
/* invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V */
final String v1 = "FreeformEnterFullScreenBindCoreRequestId gains: "; // const-string v1, "FreeformEnterFullScreenBindCoreRequestId gains: "
(( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
/* iget v1, p0, Lcom/miui/server/multisence/MultiSenceServicePolicy;->mFreeformEnterFullScreenBindCoreRequestId:I */
(( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v0 ).toString ( ); // invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
/* invoke-direct {p0, v0}, Lcom/miui/server/multisence/MultiSenceServicePolicy;->LOG_IF_DEBUG(Ljava/lang/String;)V */
/* .line 589 */
} // :cond_1
/* iget v1, p0, Lcom/miui/server/multisence/MultiSenceServicePolicy;->mFreeformEnterFullScreenRequestId:I */
/* if-eq v1, v0, :cond_2 */
/* .line 590 */
com.android.server.am.MiuiBoosterUtilsStub .getInstance ( );
/* iget v2, p0, Lcom/miui/server/multisence/MultiSenceServicePolicy;->mFreeformEnterFullScreenRequestId:I */
/* .line 591 */
/* iput v0, p0, Lcom/miui/server/multisence/MultiSenceServicePolicy;->mFreeformEnterFullScreenRequestId:I */
/* .line 594 */
} // :cond_2
/* iget v1, p0, Lcom/miui/server/multisence/MultiSenceServicePolicy;->mFreeformEnterFullScreenBindCoreRequestId:I */
/* if-eq v1, v0, :cond_3 */
/* .line 595 */
com.android.server.am.MiuiBoosterUtilsStub .getInstance ( );
/* iget v2, p0, Lcom/miui/server/multisence/MultiSenceServicePolicy;->mFreeformEnterFullScreenBindCoreRequestId:I */
/* .line 596 */
/* iput v0, p0, Lcom/miui/server/multisence/MultiSenceServicePolicy;->mFreeformEnterFullScreenBindCoreRequestId:I */
/* .line 599 */
} // :cond_3
} // :goto_0
return;
} // .end method
public void dynamicSenceSchedFreeformActionMove ( Integer p0, Integer[] p1, Boolean p2, java.lang.String p3 ) {
/* .locals 3 */
/* .param p1, "uid" # I */
/* .param p2, "tids" # [I */
/* .param p3, "isStarted" # Z */
/* .param p4, "packageName" # Ljava/lang/String; */
/* .line 521 */
/* iput-boolean p3, p0, Lcom/miui/server/multisence/MultiSenceServicePolicy;->isDynamicSenceStarting:Z */
/* .line 522 */
this.dynamicSencePackage = p4;
/* .line 523 */
/* invoke-direct {p0}, Lcom/miui/server/multisence/MultiSenceServicePolicy;->calculateFocusAndPolicy()V */
/* .line 524 */
int v0 = -1; // const/4 v0, -0x1
if ( p3 != null) { // if-eqz p3, :cond_1
/* .line 525 */
/* iget v1, p0, Lcom/miui/server/multisence/MultiSenceServicePolicy;->mFreeformMoveRequestId:I */
/* if-ne v1, v0, :cond_0 */
/* .line 526 */
v1 = /* invoke-direct {p0, p1, p2, v1, v2}, Lcom/miui/server/multisence/MultiSenceServicePolicy;->requestThreadPriority(I[III)I */
/* iput v1, p0, Lcom/miui/server/multisence/MultiSenceServicePolicy;->mFreeformMoveRequestId:I */
/* .line 527 */
/* new-instance v1, Ljava/lang/StringBuilder; */
/* invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V */
final String v2 = "FreeformMoveRequestId gains: "; // const-string v2, "FreeformMoveRequestId gains: "
(( java.lang.StringBuilder ) v1 ).append ( v2 ); // invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
/* iget v2, p0, Lcom/miui/server/multisence/MultiSenceServicePolicy;->mFreeformMoveRequestId:I */
(( java.lang.StringBuilder ) v1 ).append ( v2 ); // invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v1 ).toString ( ); // invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
/* invoke-direct {p0, v1}, Lcom/miui/server/multisence/MultiSenceServicePolicy;->LOG_IF_DEBUG(Ljava/lang/String;)V */
/* .line 530 */
} // :cond_0
/* iget v1, p0, Lcom/miui/server/multisence/MultiSenceServicePolicy;->mFreeformMoveBindCoreRequestId:I */
/* if-ne v1, v0, :cond_3 */
/* .line 531 */
int v1 = 4; // const/4 v1, 0x4
v0 = /* invoke-direct {p0, p1, p2, v0, v1}, Lcom/miui/server/multisence/MultiSenceServicePolicy;->requestBindCore(I[III)I */
/* iput v0, p0, Lcom/miui/server/multisence/MultiSenceServicePolicy;->mFreeformMoveBindCoreRequestId:I */
/* .line 532 */
/* new-instance v0, Ljava/lang/StringBuilder; */
/* invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V */
final String v1 = "FreeformMoveBindCoreRequestId gains: "; // const-string v1, "FreeformMoveBindCoreRequestId gains: "
(( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
/* iget v1, p0, Lcom/miui/server/multisence/MultiSenceServicePolicy;->mFreeformMoveBindCoreRequestId:I */
(( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v0 ).toString ( ); // invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
/* invoke-direct {p0, v0}, Lcom/miui/server/multisence/MultiSenceServicePolicy;->LOG_IF_DEBUG(Ljava/lang/String;)V */
/* .line 535 */
} // :cond_1
/* iget v1, p0, Lcom/miui/server/multisence/MultiSenceServicePolicy;->mFreeformMoveRequestId:I */
/* if-eq v1, v0, :cond_2 */
/* .line 536 */
com.android.server.am.MiuiBoosterUtilsStub .getInstance ( );
/* iget v2, p0, Lcom/miui/server/multisence/MultiSenceServicePolicy;->mFreeformMoveRequestId:I */
/* .line 537 */
/* iput v0, p0, Lcom/miui/server/multisence/MultiSenceServicePolicy;->mFreeformMoveRequestId:I */
/* .line 540 */
} // :cond_2
/* iget v1, p0, Lcom/miui/server/multisence/MultiSenceServicePolicy;->mFreeformMoveBindCoreRequestId:I */
/* if-eq v1, v0, :cond_3 */
/* .line 541 */
com.android.server.am.MiuiBoosterUtilsStub .getInstance ( );
/* iget v2, p0, Lcom/miui/server/multisence/MultiSenceServicePolicy;->mFreeformMoveBindCoreRequestId:I */
/* .line 542 */
/* iput v0, p0, Lcom/miui/server/multisence/MultiSenceServicePolicy;->mFreeformMoveBindCoreRequestId:I */
/* .line 545 */
} // :cond_3
} // :goto_0
return;
} // .end method
public void dynamicSenceSchedFreeformActionResize ( Integer p0, Integer[] p1, Boolean p2, java.lang.String p3 ) {
/* .locals 3 */
/* .param p1, "uid" # I */
/* .param p2, "tids" # [I */
/* .param p3, "isStarted" # Z */
/* .param p4, "packageName" # Ljava/lang/String; */
/* .line 548 */
/* iput-boolean p3, p0, Lcom/miui/server/multisence/MultiSenceServicePolicy;->isDynamicSenceStarting:Z */
/* .line 549 */
this.dynamicSencePackage = p4;
/* .line 550 */
/* invoke-direct {p0}, Lcom/miui/server/multisence/MultiSenceServicePolicy;->calculateFocusAndPolicy()V */
/* .line 551 */
int v0 = -1; // const/4 v0, -0x1
if ( p3 != null) { // if-eqz p3, :cond_1
/* .line 552 */
/* iget v1, p0, Lcom/miui/server/multisence/MultiSenceServicePolicy;->mFreeformResizeRequestId:I */
/* if-ne v1, v0, :cond_0 */
/* .line 553 */
v1 = /* invoke-direct {p0, p1, p2, v1, v2}, Lcom/miui/server/multisence/MultiSenceServicePolicy;->requestThreadPriority(I[III)I */
/* iput v1, p0, Lcom/miui/server/multisence/MultiSenceServicePolicy;->mFreeformResizeRequestId:I */
/* .line 554 */
/* new-instance v1, Ljava/lang/StringBuilder; */
/* invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V */
final String v2 = "FreeformResizeRequestId gains: "; // const-string v2, "FreeformResizeRequestId gains: "
(( java.lang.StringBuilder ) v1 ).append ( v2 ); // invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
/* iget v2, p0, Lcom/miui/server/multisence/MultiSenceServicePolicy;->mFreeformResizeRequestId:I */
(( java.lang.StringBuilder ) v1 ).append ( v2 ); // invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v1 ).toString ( ); // invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
/* invoke-direct {p0, v1}, Lcom/miui/server/multisence/MultiSenceServicePolicy;->LOG_IF_DEBUG(Ljava/lang/String;)V */
/* .line 557 */
} // :cond_0
/* iget v1, p0, Lcom/miui/server/multisence/MultiSenceServicePolicy;->mFreeformResizeBindCoreRequestId:I */
/* if-ne v1, v0, :cond_3 */
/* .line 558 */
int v1 = 4; // const/4 v1, 0x4
v0 = /* invoke-direct {p0, p1, p2, v0, v1}, Lcom/miui/server/multisence/MultiSenceServicePolicy;->requestBindCore(I[III)I */
/* iput v0, p0, Lcom/miui/server/multisence/MultiSenceServicePolicy;->mFreeformResizeBindCoreRequestId:I */
/* .line 559 */
/* new-instance v0, Ljava/lang/StringBuilder; */
/* invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V */
final String v1 = "FreeformResizeBindCoreRequestId gains: "; // const-string v1, "FreeformResizeBindCoreRequestId gains: "
(( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
/* iget v1, p0, Lcom/miui/server/multisence/MultiSenceServicePolicy;->mFreeformResizeBindCoreRequestId:I */
(( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v0 ).toString ( ); // invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
/* invoke-direct {p0, v0}, Lcom/miui/server/multisence/MultiSenceServicePolicy;->LOG_IF_DEBUG(Ljava/lang/String;)V */
/* .line 562 */
} // :cond_1
/* iget v1, p0, Lcom/miui/server/multisence/MultiSenceServicePolicy;->mFreeformResizeRequestId:I */
/* if-eq v1, v0, :cond_2 */
/* .line 563 */
com.android.server.am.MiuiBoosterUtilsStub .getInstance ( );
/* iget v2, p0, Lcom/miui/server/multisence/MultiSenceServicePolicy;->mFreeformResizeRequestId:I */
/* .line 564 */
/* iput v0, p0, Lcom/miui/server/multisence/MultiSenceServicePolicy;->mFreeformResizeRequestId:I */
/* .line 567 */
} // :cond_2
/* iget v1, p0, Lcom/miui/server/multisence/MultiSenceServicePolicy;->mFreeformResizeBindCoreRequestId:I */
/* if-eq v1, v0, :cond_3 */
/* .line 568 */
com.android.server.am.MiuiBoosterUtilsStub .getInstance ( );
/* iget v2, p0, Lcom/miui/server/multisence/MultiSenceServicePolicy;->mFreeformResizeBindCoreRequestId:I */
/* .line 569 */
/* iput v0, p0, Lcom/miui/server/multisence/MultiSenceServicePolicy;->mFreeformResizeBindCoreRequestId:I */
/* .line 572 */
} // :cond_3
} // :goto_0
return;
} // .end method
public void dynamicSenceSchedMWSActionMove ( Integer p0, Integer[] p1, Boolean p2, java.lang.String p3 ) {
/* .locals 3 */
/* .param p1, "uid" # I */
/* .param p2, "tids" # [I */
/* .param p3, "isStarted" # Z */
/* .param p4, "packageName" # Ljava/lang/String; */
/* .line 656 */
/* iput-boolean p3, p0, Lcom/miui/server/multisence/MultiSenceServicePolicy;->isDynamicSenceStarting:Z */
/* .line 657 */
this.dynamicSencePackage = p4;
/* .line 658 */
/* invoke-direct {p0}, Lcom/miui/server/multisence/MultiSenceServicePolicy;->calculateFocusAndPolicy()V */
/* .line 659 */
int v0 = -1; // const/4 v0, -0x1
if ( p3 != null) { // if-eqz p3, :cond_1
/* .line 660 */
/* iget v1, p0, Lcom/miui/server/multisence/MultiSenceServicePolicy;->mMWSRequestId:I */
/* if-ne v1, v0, :cond_0 */
/* .line 661 */
v1 = /* invoke-direct {p0, p1, p2, v1, v2}, Lcom/miui/server/multisence/MultiSenceServicePolicy;->requestThreadPriority(I[III)I */
/* iput v1, p0, Lcom/miui/server/multisence/MultiSenceServicePolicy;->mMWSRequestId:I */
/* .line 662 */
/* new-instance v1, Ljava/lang/StringBuilder; */
/* invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V */
final String v2 = "MWSRequestId gains: "; // const-string v2, "MWSRequestId gains: "
(( java.lang.StringBuilder ) v1 ).append ( v2 ); // invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
/* iget v2, p0, Lcom/miui/server/multisence/MultiSenceServicePolicy;->mMWSRequestId:I */
(( java.lang.StringBuilder ) v1 ).append ( v2 ); // invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v1 ).toString ( ); // invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
/* invoke-direct {p0, v1}, Lcom/miui/server/multisence/MultiSenceServicePolicy;->LOG_IF_DEBUG(Ljava/lang/String;)V */
/* .line 665 */
} // :cond_0
/* iget v1, p0, Lcom/miui/server/multisence/MultiSenceServicePolicy;->mMWSBindCoreRequestId:I */
/* if-ne v1, v0, :cond_3 */
/* .line 666 */
int v1 = 4; // const/4 v1, 0x4
v0 = /* invoke-direct {p0, p1, p2, v0, v1}, Lcom/miui/server/multisence/MultiSenceServicePolicy;->requestBindCore(I[III)I */
/* iput v0, p0, Lcom/miui/server/multisence/MultiSenceServicePolicy;->mMWSBindCoreRequestId:I */
/* .line 667 */
/* new-instance v0, Ljava/lang/StringBuilder; */
/* invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V */
final String v1 = "MWSBindCoreRequestId gains: "; // const-string v1, "MWSBindCoreRequestId gains: "
(( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
/* iget v1, p0, Lcom/miui/server/multisence/MultiSenceServicePolicy;->mMWSBindCoreRequestId:I */
(( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v0 ).toString ( ); // invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
/* invoke-direct {p0, v0}, Lcom/miui/server/multisence/MultiSenceServicePolicy;->LOG_IF_DEBUG(Ljava/lang/String;)V */
/* .line 670 */
} // :cond_1
/* iget v1, p0, Lcom/miui/server/multisence/MultiSenceServicePolicy;->mMWSRequestId:I */
/* if-eq v1, v0, :cond_2 */
/* .line 671 */
com.android.server.am.MiuiBoosterUtilsStub .getInstance ( );
/* iget v2, p0, Lcom/miui/server/multisence/MultiSenceServicePolicy;->mMWSRequestId:I */
/* .line 672 */
/* iput v0, p0, Lcom/miui/server/multisence/MultiSenceServicePolicy;->mMWSRequestId:I */
/* .line 675 */
} // :cond_2
/* iget v1, p0, Lcom/miui/server/multisence/MultiSenceServicePolicy;->mMWSBindCoreRequestId:I */
/* if-eq v1, v0, :cond_3 */
/* .line 676 */
com.android.server.am.MiuiBoosterUtilsStub .getInstance ( );
/* iget v2, p0, Lcom/miui/server/multisence/MultiSenceServicePolicy;->mMWSBindCoreRequestId:I */
/* .line 677 */
/* iput v0, p0, Lcom/miui/server/multisence/MultiSenceServicePolicy;->mMWSBindCoreRequestId:I */
/* .line 680 */
} // :cond_3
} // :goto_0
return;
} // .end method
public void dynamicSenceSchedSplitscreenctionDivider ( Integer p0, Integer[] p1, Boolean p2, java.lang.String p3 ) {
/* .locals 3 */
/* .param p1, "uid" # I */
/* .param p2, "tids" # [I */
/* .param p3, "isStarted" # Z */
/* .param p4, "packageName" # Ljava/lang/String; */
/* .line 629 */
/* iput-boolean p3, p0, Lcom/miui/server/multisence/MultiSenceServicePolicy;->isDynamicSenceStarting:Z */
/* .line 630 */
this.dynamicSencePackage = p4;
/* .line 631 */
/* invoke-direct {p0}, Lcom/miui/server/multisence/MultiSenceServicePolicy;->calculateFocusAndPolicy()V */
/* .line 632 */
int v0 = -1; // const/4 v0, -0x1
if ( p3 != null) { // if-eqz p3, :cond_1
/* .line 633 */
/* iget v1, p0, Lcom/miui/server/multisence/MultiSenceServicePolicy;->mSplitscreenRequestId:I */
/* if-ne v1, v0, :cond_0 */
/* .line 634 */
v1 = /* invoke-direct {p0, p1, p2, v1, v2}, Lcom/miui/server/multisence/MultiSenceServicePolicy;->requestThreadPriority(I[III)I */
/* iput v1, p0, Lcom/miui/server/multisence/MultiSenceServicePolicy;->mSplitscreenRequestId:I */
/* .line 635 */
/* new-instance v1, Ljava/lang/StringBuilder; */
/* invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V */
final String v2 = "SplitscreenRequestId gains: "; // const-string v2, "SplitscreenRequestId gains: "
(( java.lang.StringBuilder ) v1 ).append ( v2 ); // invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
/* iget v2, p0, Lcom/miui/server/multisence/MultiSenceServicePolicy;->mSplitscreenRequestId:I */
(( java.lang.StringBuilder ) v1 ).append ( v2 ); // invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v1 ).toString ( ); // invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
/* invoke-direct {p0, v1}, Lcom/miui/server/multisence/MultiSenceServicePolicy;->LOG_IF_DEBUG(Ljava/lang/String;)V */
/* .line 638 */
} // :cond_0
/* iget v1, p0, Lcom/miui/server/multisence/MultiSenceServicePolicy;->mSplitscreenBindCoreRequestId:I */
/* if-ne v1, v0, :cond_3 */
/* .line 639 */
int v1 = 4; // const/4 v1, 0x4
v0 = /* invoke-direct {p0, p1, p2, v0, v1}, Lcom/miui/server/multisence/MultiSenceServicePolicy;->requestBindCore(I[III)I */
/* iput v0, p0, Lcom/miui/server/multisence/MultiSenceServicePolicy;->mSplitscreenBindCoreRequestId:I */
/* .line 640 */
/* new-instance v0, Ljava/lang/StringBuilder; */
/* invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V */
final String v1 = "SplitscreenBindCoreRequestId gains: "; // const-string v1, "SplitscreenBindCoreRequestId gains: "
(( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
/* iget v1, p0, Lcom/miui/server/multisence/MultiSenceServicePolicy;->mSplitscreenBindCoreRequestId:I */
(( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v0 ).toString ( ); // invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
/* invoke-direct {p0, v0}, Lcom/miui/server/multisence/MultiSenceServicePolicy;->LOG_IF_DEBUG(Ljava/lang/String;)V */
/* .line 643 */
} // :cond_1
/* iget v1, p0, Lcom/miui/server/multisence/MultiSenceServicePolicy;->mSplitscreenRequestId:I */
/* if-eq v1, v0, :cond_2 */
/* .line 644 */
com.android.server.am.MiuiBoosterUtilsStub .getInstance ( );
/* iget v2, p0, Lcom/miui/server/multisence/MultiSenceServicePolicy;->mSplitscreenRequestId:I */
/* .line 645 */
/* iput v0, p0, Lcom/miui/server/multisence/MultiSenceServicePolicy;->mSplitscreenRequestId:I */
/* .line 648 */
} // :cond_2
/* iget v1, p0, Lcom/miui/server/multisence/MultiSenceServicePolicy;->mSplitscreenBindCoreRequestId:I */
/* if-eq v1, v0, :cond_3 */
/* .line 649 */
com.android.server.am.MiuiBoosterUtilsStub .getInstance ( );
/* iget v2, p0, Lcom/miui/server/multisence/MultiSenceServicePolicy;->mSplitscreenBindCoreRequestId:I */
/* .line 650 */
/* iput v0, p0, Lcom/miui/server/multisence/MultiSenceServicePolicy;->mSplitscreenBindCoreRequestId:I */
/* .line 653 */
} // :cond_3
} // :goto_0
return;
} // .end method
public android.graphics.Rect findOverlay ( android.graphics.Rect p0, android.graphics.Rect p1 ) {
/* .locals 5 */
/* .param p1, "rect1" # Landroid/graphics/Rect; */
/* .param p2, "rect2" # Landroid/graphics/Rect; */
/* .line 455 */
/* iget v0, p1, Landroid/graphics/Rect;->left:I */
/* iget v1, p2, Landroid/graphics/Rect;->left:I */
v0 = java.lang.Math .max ( v0,v1 );
/* .line 456 */
/* .local v0, "x1":I */
/* iget v1, p1, Landroid/graphics/Rect;->top:I */
/* iget v2, p2, Landroid/graphics/Rect;->top:I */
v1 = java.lang.Math .max ( v1,v2 );
/* .line 457 */
/* .local v1, "y1":I */
/* iget v2, p1, Landroid/graphics/Rect;->right:I */
/* iget v3, p2, Landroid/graphics/Rect;->right:I */
v2 = java.lang.Math .min ( v2,v3 );
/* .line 458 */
/* .local v2, "x2":I */
/* iget v3, p1, Landroid/graphics/Rect;->bottom:I */
/* iget v4, p2, Landroid/graphics/Rect;->bottom:I */
v3 = java.lang.Math .min ( v3,v4 );
/* .line 460 */
/* .local v3, "y2":I */
/* if-ge v0, v2, :cond_0 */
/* if-ge v1, v3, :cond_0 */
/* .line 461 */
/* new-instance v4, Landroid/graphics/Rect; */
/* invoke-direct {v4, v0, v1, v2, v3}, Landroid/graphics/Rect;-><init>(IIII)V */
/* .line 463 */
} // :cond_0
int v4 = 0; // const/4 v4, 0x0
} // .end method
public Float getOverLayScale ( Integer p0, java.util.List p1 ) {
/* .locals 5 */
/* .param p1, "curArea" # I */
/* .annotation system Ldalvik/annotation/Signature; */
/* value = { */
/* "(I", */
/* "Ljava/util/List<", */
/* "Landroid/graphics/Rect;", */
/* ">;)F" */
/* } */
} // .end annotation
/* .line 390 */
v0 = /* .local p2, "overLayList":Ljava/util/List;, "Ljava/util/List<Landroid/graphics/Rect;>;" */
/* .line 393 */
/* .local v0, "N":I */
/* if-nez v0, :cond_0 */
/* .line 394 */
int v1 = 0; // const/4 v1, 0x0
/* .local v1, "overArea":I */
/* .line 395 */
} // .end local v1 # "overArea":I
} // :cond_0
int v1 = 1; // const/4 v1, 0x1
/* if-ne v0, v1, :cond_1 */
/* .line 396 */
int v1 = 0; // const/4 v1, 0x0
/* check-cast v2, Landroid/graphics/Rect; */
v2 = (( android.graphics.Rect ) v2 ).width ( ); // invoke-virtual {v2}, Landroid/graphics/Rect;->width()I
/* check-cast v1, Landroid/graphics/Rect; */
v1 = (( android.graphics.Rect ) v1 ).height ( ); // invoke-virtual {v1}, Landroid/graphics/Rect;->height()I
/* mul-int/2addr v1, v2 */
/* .restart local v1 # "overArea":I */
/* .line 398 */
} // .end local v1 # "overArea":I
} // :cond_1
(( com.miui.server.multisence.MultiSenceServicePolicy ) p0 ).convertToArray ( p2 ); // invoke-virtual {p0, p2}, Lcom/miui/server/multisence/MultiSenceServicePolicy;->convertToArray(Ljava/util/List;)[[I
/* .line 399 */
/* .local v1, "overLayArray":[[I */
v2 = (( com.miui.server.multisence.MultiSenceServicePolicy ) p0 ).rectangleArea ( v1 ); // invoke-virtual {p0, v1}, Lcom/miui/server/multisence/MultiSenceServicePolicy;->rectangleArea([[I)I
/* move v1, v2 */
/* .line 401 */
/* .local v1, "overArea":I */
} // :goto_0
/* sget-boolean v2, Lcom/miui/server/multisence/MultiSenceConfig;->DEBUG_POLICY_BASE:Z */
/* new-instance v3, Ljava/lang/StringBuilder; */
/* invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V */
final String v4 = "curArea: "; // const-string v4, "curArea: "
(( java.lang.StringBuilder ) v3 ).append ( v4 ); // invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v3 ).append ( p1 ); // invoke-virtual {v3, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;
final String v4 = ", overArea: "; // const-string v4, ", overArea: "
(( java.lang.StringBuilder ) v3 ).append ( v4 ); // invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v3 ).append ( v1 ); // invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v3 ).toString ( ); // invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
com.miui.server.multisence.MultiSenceServiceUtils .msLogD ( v2,v3 );
/* .line 402 */
/* int-to-float v2, v1 */
/* int-to-float v3, p1 */
/* div-float/2addr v2, v3 */
} // .end method
public void limitFreeFormRes ( java.util.List p0 ) {
/* .locals 12 */
/* .annotation system Ldalvik/annotation/Signature; */
/* value = { */
/* "(", */
/* "Ljava/util/List<", */
/* "Lcom/miui/server/multisence/SingleWindowInfo;", */
/* ">;)V" */
/* } */
} // .end annotation
/* .line 349 */
/* .local p1, "schedFreeForm":Ljava/util/List;, "Ljava/util/List<Lcom/miui/server/multisence/SingleWindowInfo;>;" */
/* sget-boolean v0, Lcom/miui/server/multisence/MultiSenceConfig;->POLICY_WINDOWSIZE_ENABLE:Z */
int v1 = 1; // const/4 v1, 0x1
/* if-nez v0, :cond_2 */
/* .line 350 */
/* sget-boolean v0, Lcom/miui/server/multisence/MultiSenceConfig;->DEBUG_POLICY_BASE:Z */
/* if-nez v0, :cond_1 */
/* sget-boolean v0, Lcom/miui/server/multisence/MultiSenceConfig;->DEBUG_CONFIG:Z */
if ( v0 != null) { // if-eqz v0, :cond_0
} // :cond_0
int v1 = 0; // const/4 v1, 0x0
} // :cond_1
} // :goto_0
/* const-string/jumbo v0, "windowsize policy is not supported by multisence config" */
com.miui.server.multisence.MultiSenceServiceUtils .msLogD ( v1,v0 );
/* .line 351 */
return;
/* .line 354 */
} // :cond_2
java.util.Collections .sort ( p1 );
v0 = /* .line 355 */
/* .line 356 */
/* .local v0, "N":I */
int v2 = 0; // const/4 v2, 0x0
/* .local v2, "i":I */
} // :goto_1
/* if-ge v2, v0, :cond_3 */
/* .line 357 */
/* sget-boolean v3, Lcom/miui/server/multisence/MultiSenceConfig;->DEBUG_POLICY_BASE:Z */
/* new-instance v4, Ljava/lang/StringBuilder; */
/* invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V */
final String v5 = "schedFreeForm "; // const-string v5, "schedFreeForm "
(( java.lang.StringBuilder ) v4 ).append ( v5 ); // invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
/* check-cast v5, Lcom/miui/server/multisence/SingleWindowInfo; */
(( com.miui.server.multisence.SingleWindowInfo ) v5 ).getPackageName ( ); // invoke-virtual {v5}, Lcom/miui/server/multisence/SingleWindowInfo;->getPackageName()Ljava/lang/String;
(( java.lang.StringBuilder ) v4 ).append ( v5 ); // invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
final String v5 = " : "; // const-string v5, " : "
(( java.lang.StringBuilder ) v4 ).append ( v5 ); // invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
/* check-cast v6, Lcom/miui/server/multisence/SingleWindowInfo; */
v6 = (( com.miui.server.multisence.SingleWindowInfo ) v6 ).getLayerOrder ( ); // invoke-virtual {v6}, Lcom/miui/server/multisence/SingleWindowInfo;->getLayerOrder()I
(( java.lang.StringBuilder ) v4 ).append ( v6 ); // invoke-virtual {v4, v6}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v4 ).append ( v5 ); // invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
/* .line 358 */
/* check-cast v5, Lcom/miui/server/multisence/SingleWindowInfo; */
(( com.miui.server.multisence.SingleWindowInfo ) v5 ).getRectValue ( ); // invoke-virtual {v5}, Lcom/miui/server/multisence/SingleWindowInfo;->getRectValue()Landroid/graphics/Rect;
(( android.graphics.Rect ) v5 ).toString ( ); // invoke-virtual {v5}, Landroid/graphics/Rect;->toString()Ljava/lang/String;
(( java.lang.StringBuilder ) v4 ).append ( v5 ); // invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v4 ).toString ( ); // invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
/* .line 357 */
com.miui.server.multisence.MultiSenceServiceUtils .msLogD ( v3,v4 );
/* .line 356 */
/* add-int/lit8 v2, v2, 0x1 */
/* .line 360 */
} // .end local v2 # "i":I
} // :cond_3
/* if-gt v0, v1, :cond_4 */
return;
/* .line 361 */
} // :cond_4
int v2 = 0; // const/4 v2, 0x0
/* .restart local v2 # "i":I */
} // :goto_2
/* add-int/lit8 v3, v0, -0x1 */
/* if-ge v2, v3, :cond_9 */
/* .line 362 */
/* check-cast v3, Lcom/miui/server/multisence/SingleWindowInfo; */
/* .line 363 */
/* .local v3, "app":Lcom/miui/server/multisence/SingleWindowInfo; */
v4 = (( com.miui.server.multisence.SingleWindowInfo ) v3 ).isHighPriority ( ); // invoke-virtual {v3}, Lcom/miui/server/multisence/SingleWindowInfo;->isHighPriority()Z
if ( v4 != null) { // if-eqz v4, :cond_5
/* goto/16 :goto_4 */
/* .line 364 */
} // :cond_5
/* new-instance v4, Ljava/util/ArrayList; */
/* invoke-direct {v4}, Ljava/util/ArrayList;-><init>()V */
/* .line 365 */
/* .local v4, "overLayList":Ljava/util/List;, "Ljava/util/List<Landroid/graphics/Rect;>;" */
(( com.miui.server.multisence.SingleWindowInfo ) v3 ).getRectValue ( ); // invoke-virtual {v3}, Lcom/miui/server/multisence/SingleWindowInfo;->getRectValue()Landroid/graphics/Rect;
v5 = (( android.graphics.Rect ) v5 ).width ( ); // invoke-virtual {v5}, Landroid/graphics/Rect;->width()I
(( com.miui.server.multisence.SingleWindowInfo ) v3 ).getRectValue ( ); // invoke-virtual {v3}, Lcom/miui/server/multisence/SingleWindowInfo;->getRectValue()Landroid/graphics/Rect;
v6 = (( android.graphics.Rect ) v6 ).height ( ); // invoke-virtual {v6}, Landroid/graphics/Rect;->height()I
/* mul-int/2addr v5, v6 */
/* .line 367 */
/* .local v5, "curArea":I */
/* add-int/lit8 v6, v2, 0x1 */
/* .local v6, "j":I */
} // :goto_3
/* if-ge v6, v0, :cond_7 */
/* .line 368 */
/* check-cast v7, Lcom/miui/server/multisence/SingleWindowInfo; */
/* .line 369 */
/* .local v7, "app_tmp":Lcom/miui/server/multisence/SingleWindowInfo; */
(( com.miui.server.multisence.SingleWindowInfo ) v3 ).getRectValue ( ); // invoke-virtual {v3}, Lcom/miui/server/multisence/SingleWindowInfo;->getRectValue()Landroid/graphics/Rect;
(( com.miui.server.multisence.SingleWindowInfo ) v7 ).getRectValue ( ); // invoke-virtual {v7}, Lcom/miui/server/multisence/SingleWindowInfo;->getRectValue()Landroid/graphics/Rect;
(( com.miui.server.multisence.MultiSenceServicePolicy ) p0 ).findOverlay ( v8, v9 ); // invoke-virtual {p0, v8, v9}, Lcom/miui/server/multisence/MultiSenceServicePolicy;->findOverlay(Landroid/graphics/Rect;Landroid/graphics/Rect;)Landroid/graphics/Rect;
/* .line 370 */
/* .local v8, "result":Landroid/graphics/Rect; */
if ( v8 != null) { // if-eqz v8, :cond_6
/* .line 371 */
/* .line 372 */
/* sget-boolean v9, Lcom/miui/server/multisence/MultiSenceConfig;->DEBUG_POLICY_BASE:Z */
/* new-instance v10, Ljava/lang/StringBuilder; */
/* invoke-direct {v10}, Ljava/lang/StringBuilder;-><init>()V */
final String v11 = "above layer is "; // const-string v11, "above layer is "
(( java.lang.StringBuilder ) v10 ).append ( v11 ); // invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( com.miui.server.multisence.SingleWindowInfo ) v7 ).getPackageName ( ); // invoke-virtual {v7}, Lcom/miui/server/multisence/SingleWindowInfo;->getPackageName()Ljava/lang/String;
(( java.lang.StringBuilder ) v10 ).append ( v11 ); // invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
final String v11 = ", overlay info "; // const-string v11, ", overlay info "
(( java.lang.StringBuilder ) v10 ).append ( v11 ); // invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( android.graphics.Rect ) v8 ).toString ( ); // invoke-virtual {v8}, Landroid/graphics/Rect;->toString()Ljava/lang/String;
(( java.lang.StringBuilder ) v10 ).append ( v11 ); // invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v10 ).toString ( ); // invoke-virtual {v10}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
com.miui.server.multisence.MultiSenceServiceUtils .msLogD ( v9,v10 );
/* .line 367 */
} // .end local v7 # "app_tmp":Lcom/miui/server/multisence/SingleWindowInfo;
} // .end local v8 # "result":Landroid/graphics/Rect;
} // :cond_6
/* add-int/lit8 v6, v6, 0x1 */
/* .line 376 */
} // .end local v6 # "j":I
} // :cond_7
v6 = (( com.miui.server.multisence.MultiSenceServicePolicy ) p0 ).getOverLayScale ( v5, v4 ); // invoke-virtual {p0, v5, v4}, Lcom/miui/server/multisence/MultiSenceServicePolicy;->getOverLayScale(ILjava/util/List;)F
/* .line 377 */
/* .local v6, "scale":F */
/* sget-boolean v7, Lcom/miui/server/multisence/MultiSenceConfig;->DEBUG_POLICY_BASE:Z */
/* new-instance v8, Ljava/lang/StringBuilder; */
/* invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V */
(( com.miui.server.multisence.SingleWindowInfo ) v3 ).getPackageName ( ); // invoke-virtual {v3}, Lcom/miui/server/multisence/SingleWindowInfo;->getPackageName()Ljava/lang/String;
(( java.lang.StringBuilder ) v8 ).append ( v9 ); // invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
final String v9 = " is covered : "; // const-string v9, " is covered : "
(( java.lang.StringBuilder ) v8 ).append ( v9 ); // invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
/* const/high16 v9, 0x42c80000 # 100.0f */
/* mul-float/2addr v9, v6 */
(( java.lang.StringBuilder ) v8 ).append ( v9 ); // invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;
final String v9 = "%"; // const-string v9, "%"
(( java.lang.StringBuilder ) v8 ).append ( v9 ); // invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v8 ).toString ( ); // invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
com.miui.server.multisence.MultiSenceServiceUtils .msLogD ( v7,v8 );
/* .line 379 */
/* float-to-double v7, v6 */
/* const-wide v9, 0x3feccccccccccccdL # 0.9 */
/* cmpl-double v7, v7, v9 */
/* if-ltz v7, :cond_8 */
/* const/high16 v7, 0x3f800000 # 1.0f */
/* cmpg-float v8, v6, v7 */
/* if-gtz v8, :cond_8 */
/* .line 380 */
(( com.miui.server.multisence.SingleWindowInfo ) v3 ).getPackageName ( ); // invoke-virtual {v3}, Lcom/miui/server/multisence/SingleWindowInfo;->getPackageName()Ljava/lang/String;
/* .line 381 */
/* .local v8, "name":Ljava/lang/String; */
(( com.miui.server.multisence.SingleWindowInfo ) v3 ).setCovered ( v1 ); // invoke-virtual {v3, v1}, Lcom/miui/server/multisence/SingleWindowInfo;->setCovered(Z)Lcom/miui/server/multisence/SingleWindowInfo;
/* .line 382 */
/* int-to-float v9, v5 */
/* sub-float/2addr v7, v6 */
/* mul-float/2addr v9, v7 */
/* .line 383 */
/* .local v9, "unCoveredArea":F */
/* sget-boolean v7, Lcom/miui/server/multisence/MultiSenceConfig;->DEBUG_POLICY_BASE:Z */
/* new-instance v10, Ljava/lang/StringBuilder; */
/* invoke-direct {v10}, Ljava/lang/StringBuilder;-><init>()V */
(( com.miui.server.multisence.SingleWindowInfo ) v3 ).getPackageName ( ); // invoke-virtual {v3}, Lcom/miui/server/multisence/SingleWindowInfo;->getPackageName()Ljava/lang/String;
(( java.lang.StringBuilder ) v10 ).append ( v11 ); // invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
final String v11 = " is uncovered : "; // const-string v11, " is uncovered : "
(( java.lang.StringBuilder ) v10 ).append ( v11 ); // invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v10 ).append ( v9 ); // invoke-virtual {v10, v9}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v10 ).toString ( ); // invoke-virtual {v10}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
com.miui.server.multisence.MultiSenceServiceUtils .msLogD ( v7,v10 );
/* .line 384 */
/* const v7, 0x47435000 # 50000.0f */
/* cmpg-float v7, v9, v7 */
/* if-gez v7, :cond_8 */
(( com.miui.server.multisence.SingleWindowInfo ) v3 ).setVrsCovered ( v1 ); // invoke-virtual {v3, v1}, Lcom/miui/server/multisence/SingleWindowInfo;->setVrsCovered(Z)Lcom/miui/server/multisence/SingleWindowInfo;
/* .line 361 */
} // .end local v3 # "app":Lcom/miui/server/multisence/SingleWindowInfo;
} // .end local v4 # "overLayList":Ljava/util/List;, "Ljava/util/List<Landroid/graphics/Rect;>;"
} // .end local v5 # "curArea":I
} // .end local v6 # "scale":F
} // .end local v8 # "name":Ljava/lang/String;
} // .end local v9 # "unCoveredArea":F
} // :cond_8
/* add-int/lit8 v2, v2, 0x1 */
/* goto/16 :goto_2 */
/* .line 387 */
} // .end local v2 # "i":I
} // :cond_9
} // :goto_4
return;
} // .end method
public Integer rectangleArea ( Integer[] p0 ) {
/* .locals 21 */
/* .param p1, "rs" # [[I */
/* .line 423 */
/* move-object/from16 v0, p1 */
/* new-instance v1, Ljava/util/ArrayList; */
/* invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V */
/* .line 424 */
/* .local v1, "list":Ljava/util/List;, "Ljava/util/List<Ljava/lang/Integer;>;" */
/* array-length v2, v0 */
int v3 = 0; // const/4 v3, 0x0
/* move v4, v3 */
} // :goto_0
int v5 = 2; // const/4 v5, 0x2
/* if-ge v4, v2, :cond_0 */
/* aget-object v6, v0, v4 */
/* .line 425 */
/* .local v6, "info":[I */
/* aget v7, v6, v3 */
java.lang.Integer .valueOf ( v7 );
/* aget v5, v6, v5 */
java.lang.Integer .valueOf ( v5 );
/* .line 424 */
} // .end local v6 # "info":[I
/* add-int/lit8 v4, v4, 0x1 */
/* .line 427 */
} // :cond_0
java.util.Collections .sort ( v1 );
/* .line 428 */
/* const-wide/16 v6, 0x0 */
/* .line 429 */
/* .local v6, "ans":J */
int v2 = 1; // const/4 v2, 0x1
/* .local v2, "i":I */
v4 = } // :goto_1
/* if-ge v2, v4, :cond_7 */
/* .line 430 */
/* add-int/lit8 v4, v2, -0x1 */
/* check-cast v4, Ljava/lang/Integer; */
v4 = (( java.lang.Integer ) v4 ).intValue ( ); // invoke-virtual {v4}, Ljava/lang/Integer;->intValue()I
/* .local v4, "a":I */
/* check-cast v8, Ljava/lang/Integer; */
v8 = (( java.lang.Integer ) v8 ).intValue ( ); // invoke-virtual {v8}, Ljava/lang/Integer;->intValue()I
/* .local v8, "b":I */
/* sub-int v9, v8, v4 */
/* .line 431 */
/* .local v9, "len":I */
/* if-nez v9, :cond_1 */
/* goto/16 :goto_5 */
/* .line 432 */
} // :cond_1
/* new-instance v10, Ljava/util/ArrayList; */
/* invoke-direct {v10}, Ljava/util/ArrayList;-><init>()V */
/* .line 433 */
/* .local v10, "lines":Ljava/util/List;, "Ljava/util/List<[I>;" */
/* array-length v11, v0 */
/* move v12, v3 */
} // :goto_2
int v13 = 1; // const/4 v13, 0x1
/* if-ge v12, v11, :cond_3 */
/* aget-object v14, v0, v12 */
/* .line 434 */
/* .local v14, "info":[I */
/* aget v15, v14, v3 */
/* if-gt v15, v4, :cond_2 */
/* aget v15, v14, v5 */
/* if-gt v8, v15, :cond_2 */
/* aget v13, v14, v13 */
int v15 = 3; // const/4 v15, 0x3
/* aget v15, v14, v15 */
/* filled-new-array {v13, v15}, [I */
/* .line 433 */
} // .end local v14 # "info":[I
} // :cond_2
/* add-int/lit8 v12, v12, 0x1 */
/* .line 436 */
} // :cond_3
/* new-instance v11, Lcom/miui/server/multisence/MultiSenceServicePolicy$$ExternalSyntheticLambda0; */
/* invoke-direct {v11}, Lcom/miui/server/multisence/MultiSenceServicePolicy$$ExternalSyntheticLambda0;-><init>()V */
java.util.Collections .sort ( v10,v11 );
/* .line 439 */
/* const-wide/16 v11, 0x0 */
/* .local v11, "tot":J */
/* const-wide/16 v14, -0x1 */
/* .local v14, "l":J */
/* const-wide/16 v16, -0x1 */
/* .line 440 */
/* .local v16, "r":J */
} // :goto_3
v19 = /* invoke-interface/range {v18 ..v18}, Ljava/util/Iterator;->hasNext()Z */
if ( v19 != null) { // if-eqz v19, :cond_6
/* invoke-interface/range {v18 ..v18}, Ljava/util/Iterator;->next()Ljava/lang/Object; */
/* check-cast v19, [I */
/* .line 441 */
/* .local v19, "cur":[I */
/* aget v5, v19, v3 */
/* move/from16 v20, v4 */
} // .end local v4 # "a":I
/* .local v20, "a":I */
/* int-to-long v3, v5 */
/* cmp-long v3, v3, v16 */
/* if-lez v3, :cond_4 */
/* .line 442 */
/* sub-long v3, v16, v14 */
/* add-long/2addr v11, v3 */
/* .line 443 */
int v3 = 0; // const/4 v3, 0x0
/* aget v4, v19, v3 */
/* int-to-long v4, v4 */
} // .end local v14 # "l":J
/* .local v4, "l":J */
/* aget v14, v19, v13 */
/* int-to-long v14, v14 */
/* move-wide/from16 v16, v14 */
/* move-wide v14, v4 */
} // .end local v16 # "r":J
/* .local v14, "r":J */
/* .line 444 */
} // .end local v4 # "l":J
/* .local v14, "l":J */
/* .restart local v16 # "r":J */
} // :cond_4
int v3 = 0; // const/4 v3, 0x0
/* aget v4, v19, v13 */
/* int-to-long v4, v4 */
/* cmp-long v4, v4, v16 */
/* if-lez v4, :cond_5 */
/* .line 445 */
/* aget v4, v19, v13 */
/* int-to-long v4, v4 */
/* move-wide/from16 v16, v4 */
/* .line 447 */
} // .end local v19 # "cur":[I
} // :cond_5
} // :goto_4
/* move/from16 v4, v20 */
int v5 = 2; // const/4 v5, 0x2
/* .line 448 */
} // .end local v20 # "a":I
/* .local v4, "a":I */
} // :cond_6
/* move/from16 v20, v4 */
} // .end local v4 # "a":I
/* .restart local v20 # "a":I */
/* sub-long v4, v16, v14 */
/* add-long/2addr v11, v4 */
/* .line 449 */
/* int-to-long v4, v9 */
/* mul-long/2addr v4, v11 */
/* add-long/2addr v6, v4 */
/* .line 429 */
} // .end local v8 # "b":I
} // .end local v9 # "len":I
} // .end local v10 # "lines":Ljava/util/List;, "Ljava/util/List<[I>;"
} // .end local v11 # "tot":J
} // .end local v14 # "l":J
} // .end local v16 # "r":J
} // .end local v20 # "a":I
} // :goto_5
/* add-int/lit8 v2, v2, 0x1 */
int v5 = 2; // const/4 v5, 0x2
/* goto/16 :goto_1 */
/* .line 451 */
} // .end local v2 # "i":I
} // :cond_7
/* long-to-int v2, v6 */
} // .end method
public void reset ( ) {
/* .locals 0 */
/* .line 230 */
(( com.miui.server.multisence.MultiSenceServicePolicy ) p0 ).resetDynFps ( ); // invoke-virtual {p0}, Lcom/miui/server/multisence/MultiSenceServicePolicy;->resetDynFps()V
/* .line 231 */
(( com.miui.server.multisence.MultiSenceServicePolicy ) p0 ).resetVrs ( ); // invoke-virtual {p0}, Lcom/miui/server/multisence/MultiSenceServicePolicy;->resetVrs()V
/* .line 232 */
return;
} // .end method
public void resetDynFps ( ) {
/* .locals 2 */
/* .line 235 */
final String v0 = "/data/system/mcd/mwdf"; // const-string v0, "/data/system/mcd/mwdf"
final String v1 = "\n"; // const-string v1, "\n"
com.miui.server.multisence.MultiSenceServiceUtils .writeToFile ( v0,v1 );
/* .line 236 */
return;
} // .end method
public void resetVrs ( ) {
/* .locals 5 */
/* .line 238 */
/* new-instance v0, Ljava/lang/StringBuilder; */
final String v1 = ""; // const-string v1, ""
/* invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V */
/* .line 239 */
/* .local v0, "vrsResetCmd":Ljava/lang/StringBuilder; */
int v1 = 0; // const/4 v1, 0x0
/* .line 240 */
/* .local v1, "count":I */
v2 = this.mVrsWorkList;
v3 = } // :goto_0
if ( v3 != null) { // if-eqz v3, :cond_1
/* check-cast v3, Ljava/lang/String; */
/* .line 241 */
/* .local v3, "appName":Ljava/lang/String; */
v4 = /* invoke-direct {p0, v0, v3}, Lcom/miui/server/multisence/MultiSenceServicePolicy;->setVrsOff(Ljava/lang/StringBuilder;Ljava/lang/String;)Z */
if ( v4 != null) { // if-eqz v4, :cond_0
/* add-int/lit8 v1, v1, 0x1 */
/* .line 242 */
} // .end local v3 # "appName":Ljava/lang/String;
} // :cond_0
/* .line 243 */
} // :cond_1
final String v2 = "\n"; // const-string v2, "\n"
(( java.lang.StringBuilder ) v0 ).append ( v2 ); // invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
/* .line 244 */
/* sget-boolean v2, Lcom/miui/server/multisence/MultiSenceConfig;->DEBUG_VRS:Z */
(( java.lang.StringBuilder ) v0 ).toString ( ); // invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
com.miui.server.multisence.MultiSenceServiceUtils .msLogD ( v2,v3 );
/* .line 245 */
/* if-gtz v1, :cond_2 */
/* .line 246 */
return;
/* .line 248 */
} // :cond_2
int v2 = 2; // const/4 v2, 0x2
(( java.lang.StringBuilder ) v0 ).toString ( ); // invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
(( com.miui.server.multisence.MultiSenceServicePolicy ) p0 ).setCommonVrsParams ( v2, v3 ); // invoke-virtual {p0, v2, v3}, Lcom/miui/server/multisence/MultiSenceServicePolicy;->setCommonVrsParams(ILjava/lang/String;)V
/* .line 249 */
v2 = this.mVrsWorkList;
/* .line 250 */
return;
} // .end method
public void setCommonVrsParams ( Integer p0, java.lang.String p1 ) {
/* .locals 5 */
/* .param p1, "cmd" # I */
/* .param p2, "params" # Ljava/lang/String; */
/* .line 739 */
/* sget-boolean v0, Lcom/miui/server/multisence/MultiSenceConfig;->DEBUG_VRS:Z */
/* const-string/jumbo v1, "start send VRS cmd to joyose." */
com.miui.server.multisence.MultiSenceServiceUtils .msLogD ( v0,v1 );
/* .line 740 */
android.os.Parcel .obtain ( );
/* .line 741 */
/* .local v0, "data":Landroid/os/Parcel; */
android.os.Parcel .obtain ( );
/* .line 743 */
/* .local v1, "reply":Landroid/os/Parcel; */
try { // :try_start_0
v2 = com.miui.server.multisence.MultiSenceServicePolicy.mJoyoseService;
/* if-nez v2, :cond_2 */
/* .line 744 */
/* const-string/jumbo v2, "xiaomi.joyose" */
android.os.ServiceManager .getService ( v2 );
/* .line 745 */
/* if-nez v2, :cond_2 */
/* .line 746 */
/* sget-boolean v2, Lcom/miui/server/multisence/MultiSenceConfig;->DEBUG_VRS:Z */
final String v3 = "not find joyose service"; // const-string v3, "not find joyose service"
com.miui.server.multisence.MultiSenceServiceUtils .msLogW ( v2,v3 );
/* :try_end_0 */
/* .catch Ljava/lang/Exception; {:try_start_0 ..:try_end_0} :catch_0 */
/* .catchall {:try_start_0 ..:try_end_0} :catchall_0 */
/* .line 759 */
if ( v0 != null) { // if-eqz v0, :cond_0
/* .line 760 */
(( android.os.Parcel ) v0 ).recycle ( ); // invoke-virtual {v0}, Landroid/os/Parcel;->recycle()V
/* .line 762 */
} // :cond_0
if ( v1 != null) { // if-eqz v1, :cond_1
/* .line 763 */
(( android.os.Parcel ) v1 ).recycle ( ); // invoke-virtual {v1}, Landroid/os/Parcel;->recycle()V
/* .line 747 */
} // :cond_1
return;
/* .line 751 */
} // :cond_2
try { // :try_start_1
final String v2 = "com.xiaomi.joyose.IJoyoseInterface"; // const-string v2, "com.xiaomi.joyose.IJoyoseInterface"
(( android.os.Parcel ) v0 ).writeInterfaceToken ( v2 ); // invoke-virtual {v0, v2}, Landroid/os/Parcel;->writeInterfaceToken(Ljava/lang/String;)V
/* .line 752 */
(( android.os.Parcel ) v0 ).writeInt ( p1 ); // invoke-virtual {v0, p1}, Landroid/os/Parcel;->writeInt(I)V
/* .line 753 */
(( android.os.Parcel ) v0 ).writeString ( p2 ); // invoke-virtual {v0, p2}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V
/* .line 754 */
v2 = com.miui.server.multisence.MultiSenceServicePolicy.mJoyoseService;
/* const/16 v3, 0x18 */
int v4 = 0; // const/4 v4, 0x0
/* :try_end_1 */
/* .catch Ljava/lang/Exception; {:try_start_1 ..:try_end_1} :catch_0 */
/* .catchall {:try_start_1 ..:try_end_1} :catchall_0 */
/* .line 759 */
if ( v0 != null) { // if-eqz v0, :cond_3
/* .line 760 */
(( android.os.Parcel ) v0 ).recycle ( ); // invoke-virtual {v0}, Landroid/os/Parcel;->recycle()V
/* .line 762 */
} // :cond_3
if ( v1 != null) { // if-eqz v1, :cond_5
/* .line 763 */
/* .line 759 */
/* :catchall_0 */
/* move-exception v2 */
/* .line 755 */
/* :catch_0 */
/* move-exception v2 */
/* .line 756 */
/* .local v2, "e":Ljava/lang/Exception; */
int v3 = 0; // const/4 v3, 0x0
try { // :try_start_2
/* .line 757 */
(( java.lang.Exception ) v2 ).printStackTrace ( ); // invoke-virtual {v2}, Ljava/lang/Exception;->printStackTrace()V
/* :try_end_2 */
/* .catchall {:try_start_2 ..:try_end_2} :catchall_0 */
/* .line 759 */
} // .end local v2 # "e":Ljava/lang/Exception;
if ( v0 != null) { // if-eqz v0, :cond_4
/* .line 760 */
(( android.os.Parcel ) v0 ).recycle ( ); // invoke-virtual {v0}, Landroid/os/Parcel;->recycle()V
/* .line 762 */
} // :cond_4
if ( v1 != null) { // if-eqz v1, :cond_5
/* .line 763 */
} // :goto_0
(( android.os.Parcel ) v1 ).recycle ( ); // invoke-virtual {v1}, Landroid/os/Parcel;->recycle()V
/* .line 766 */
} // :cond_5
return;
/* .line 759 */
} // :goto_1
if ( v0 != null) { // if-eqz v0, :cond_6
/* .line 760 */
(( android.os.Parcel ) v0 ).recycle ( ); // invoke-virtual {v0}, Landroid/os/Parcel;->recycle()V
/* .line 762 */
} // :cond_6
if ( v1 != null) { // if-eqz v1, :cond_7
/* .line 763 */
(( android.os.Parcel ) v1 ).recycle ( ); // invoke-virtual {v1}, Landroid/os/Parcel;->recycle()V
/* .line 765 */
} // :cond_7
/* throw v2 */
} // .end method
public void updateInputFocus ( java.lang.String p0 ) {
/* .locals 2 */
/* .param p1, "newInputFocus" # Ljava/lang/String; */
/* .line 479 */
if ( p1 != null) { // if-eqz p1, :cond_2
final String v0 = ""; // const-string v0, ""
v0 = (( java.lang.String ) v0 ).equals ( p1 ); // invoke-virtual {v0, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z
if ( v0 != null) { // if-eqz v0, :cond_0
/* .line 484 */
} // :cond_0
v0 = this.currentInputFocus;
v0 = (( java.lang.String ) p1 ).equals ( v0 ); // invoke-virtual {p1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z
/* if-nez v0, :cond_1 */
/* .line 485 */
this.currentInputFocus = p1;
/* .line 486 */
/* new-instance v0, Ljava/lang/StringBuilder; */
/* invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V */
final String v1 = "input focus changed: "; // const-string v1, "input focus changed: "
(( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v0 ).append ( p1 ); // invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v0 ).toString ( ); // invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
/* invoke-direct {p0, v0}, Lcom/miui/server/multisence/MultiSenceServicePolicy;->LOG_IF_DEBUG(Ljava/lang/String;)V */
/* .line 487 */
/* invoke-direct {p0}, Lcom/miui/server/multisence/MultiSenceServicePolicy;->calculateFocusAndPolicy()V */
/* .line 489 */
} // :cond_1
return;
/* .line 480 */
} // :cond_2
} // :goto_0
/* const-string/jumbo v0, "updateInputFocus: fail, null" */
/* invoke-direct {p0, v0}, Lcom/miui/server/multisence/MultiSenceServicePolicy;->LOG_IF_DEBUG(Ljava/lang/String;)V */
/* .line 481 */
return;
} // .end method
public void updatePowerMode ( Boolean p0 ) {
/* .locals 2 */
/* .param p1, "isPowerMode" # Z */
/* .line 492 */
/* new-instance v0, Ljava/lang/StringBuilder; */
/* invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V */
/* const-string/jumbo v1, "updatePowerMode: isPowerMode= " */
(( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v0 ).append ( p1 ); // invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v0 ).toString ( ); // invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
/* invoke-direct {p0, v0}, Lcom/miui/server/multisence/MultiSenceServicePolicy;->LOG_IF_DEBUG(Ljava/lang/String;)V */
/* .line 493 */
com.android.server.am.MiuiBoosterUtilsStub .getInstance ( );
/* .line 494 */
return;
} // .end method
public void updateSchedPolicy ( java.util.Map p0, java.util.Map p1 ) {
/* .locals 5 */
/* .annotation system Ldalvik/annotation/Signature; */
/* value = { */
/* "(", */
/* "Ljava/util/Map<", */
/* "Ljava/lang/String;", */
/* "Lcom/miui/server/multisence/SingleWindowInfo;", */
/* ">;", */
/* "Ljava/util/Map<", */
/* "Ljava/lang/String;", */
/* "Lcom/miui/server/multisence/SingleWindowInfo;", */
/* ">;)V" */
/* } */
} // .end annotation
/* .line 114 */
/* .local p1, "oldWindows":Ljava/util/Map;, "Ljava/util/Map<Ljava/lang/String;Lcom/miui/server/multisence/SingleWindowInfo;>;" */
/* .local p2, "inWindows":Ljava/util/Map;, "Ljava/util/Map<Ljava/lang/String;Lcom/miui/server/multisence/SingleWindowInfo;>;" */
v0 = this.schedWindows;
/* .line 115 */
v1 = } // :goto_0
if ( v1 != null) { // if-eqz v1, :cond_1
/* check-cast v1, Ljava/lang/String; */
/* .line 116 */
/* .local v1, "name_in":Ljava/lang/String; */
/* check-cast v2, Lcom/miui/server/multisence/SingleWindowInfo; */
/* .line 117 */
v3 = /* .local v2, "inWindow":Lcom/miui/server/multisence/SingleWindowInfo; */
if ( v3 != null) { // if-eqz v3, :cond_0
/* .line 118 */
/* check-cast v3, Lcom/miui/server/multisence/SingleWindowInfo; */
/* .line 119 */
/* .local v3, "oldWindow":Lcom/miui/server/multisence/SingleWindowInfo; */
v4 = this.schedWindows;
/* invoke-direct {p0, v4, v2, v3}, Lcom/miui/server/multisence/MultiSenceServicePolicy;->updateExistedWindowPolicy(Ljava/util/Map;Lcom/miui/server/multisence/SingleWindowInfo;Lcom/miui/server/multisence/SingleWindowInfo;)V */
/* .line 120 */
/* .line 121 */
} // .end local v3 # "oldWindow":Lcom/miui/server/multisence/SingleWindowInfo;
/* .line 122 */
} // :cond_0
v3 = this.schedWindows;
/* invoke-direct {p0, v3, v2}, Lcom/miui/server/multisence/MultiSenceServicePolicy;->updateNewWindowPolicy(Ljava/util/Map;Lcom/miui/server/multisence/SingleWindowInfo;)V */
/* .line 124 */
} // .end local v1 # "name_in":Ljava/lang/String;
} // .end local v2 # "inWindow":Lcom/miui/server/multisence/SingleWindowInfo;
} // :goto_1
/* .line 126 */
} // :cond_1
v1 = } // :goto_2
if ( v1 != null) { // if-eqz v1, :cond_2
/* check-cast v1, Ljava/lang/String; */
/* .line 127 */
/* .local v1, "name_left":Ljava/lang/String; */
/* check-cast v2, Lcom/miui/server/multisence/SingleWindowInfo; */
/* .line 128 */
/* .local v2, "leftWindow":Lcom/miui/server/multisence/SingleWindowInfo; */
v3 = this.schedWindows;
/* invoke-direct {p0, v3, v2}, Lcom/miui/server/multisence/MultiSenceServicePolicy;->updateLeftWindowPolicy(Ljava/util/Map;Lcom/miui/server/multisence/SingleWindowInfo;)V */
/* .line 129 */
} // .end local v1 # "name_left":Ljava/lang/String;
} // .end local v2 # "leftWindow":Lcom/miui/server/multisence/SingleWindowInfo;
/* .line 130 */
} // :cond_2
return;
} // .end method
public void windowInfoPerPorcessing ( java.util.Map p0 ) {
/* .locals 6 */
/* .annotation system Ldalvik/annotation/Signature; */
/* value = { */
/* "(", */
/* "Ljava/util/Map<", */
/* "Ljava/lang/String;", */
/* "Lcom/miui/server/multisence/SingleWindowInfo;", */
/* ">;)V" */
/* } */
} // .end annotation
/* .line 103 */
/* .local p1, "inWindows":Ljava/util/Map;, "Ljava/util/Map<Ljava/lang/String;Lcom/miui/server/multisence/SingleWindowInfo;>;" */
/* new-instance v0, Ljava/util/ArrayList; */
/* invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V */
/* .line 104 */
/* .local v0, "freeformWindows":Ljava/util/List;, "Ljava/util/List<Lcom/miui/server/multisence/SingleWindowInfo;>;" */
v2 = } // :goto_0
if ( v2 != null) { // if-eqz v2, :cond_1
/* check-cast v2, Ljava/lang/String; */
/* .line 105 */
/* .local v2, "appName":Ljava/lang/String; */
/* check-cast v3, Lcom/miui/server/multisence/SingleWindowInfo; */
/* .line 106 */
/* .local v3, "app":Lcom/miui/server/multisence/SingleWindowInfo; */
(( com.miui.server.multisence.SingleWindowInfo ) v3 ).getWindowForm ( ); // invoke-virtual {v3}, Lcom/miui/server/multisence/SingleWindowInfo;->getWindowForm()Lcom/miui/server/multisence/SingleWindowInfo$WindowForm;
v5 = com.miui.server.multisence.SingleWindowInfo$WindowForm.MUTIL_FREEDOM;
/* if-ne v4, v5, :cond_0 */
/* .line 107 */
/* .line 109 */
} // .end local v2 # "appName":Ljava/lang/String;
} // .end local v3 # "app":Lcom/miui/server/multisence/SingleWindowInfo;
} // :cond_0
/* .line 110 */
} // :cond_1
(( com.miui.server.multisence.MultiSenceServicePolicy ) p0 ).limitFreeFormRes ( v0 ); // invoke-virtual {p0, v0}, Lcom/miui/server/multisence/MultiSenceServicePolicy;->limitFreeFormRes(Ljava/util/List;)V
/* .line 111 */
return;
} // .end method
