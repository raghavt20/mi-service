public abstract class com.miui.server.multisence.MultiSenceManagerInternal {
	 /* .source "MultiSenceManagerInternal.java" */
	 /* # virtual methods */
	 public abstract void setUpdateReason ( java.lang.String p0 ) {
	 } // .end method
	 public abstract void showAllWindowInfo ( java.lang.String p0 ) {
	 } // .end method
	 public abstract void updateDynamicWindowsInfo ( Integer p0, Integer p1, Integer[] p2, Boolean p3 ) {
	 } // .end method
	 public abstract void updateStaticWindowsInfo ( java.util.Map p0 ) {
		 /* .annotation system Ldalvik/annotation/Signature; */
		 /* value = { */
		 /* "(", */
		 /* "Ljava/util/Map<", */
		 /* "Ljava/lang/String;", */
		 /* "Lcom/miui/server/multisence/SingleWindowInfo;", */
		 /* ">;)V" */
		 /* } */
	 } // .end annotation
} // .end method
