.class Lcom/miui/server/multisence/MultiSenceDynamicController$3;
.super Landroid/database/ContentObserver;
.source "MultiSenceDynamicController.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/miui/server/multisence/MultiSenceDynamicController;->registerFWCloudObserver()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/miui/server/multisence/MultiSenceDynamicController;


# direct methods
.method constructor <init>(Lcom/miui/server/multisence/MultiSenceDynamicController;Landroid/os/Handler;)V
    .locals 0
    .param p1, "this$0"    # Lcom/miui/server/multisence/MultiSenceDynamicController;
    .param p2, "handler"    # Landroid/os/Handler;

    .line 147
    iput-object p1, p0, Lcom/miui/server/multisence/MultiSenceDynamicController$3;->this$0:Lcom/miui/server/multisence/MultiSenceDynamicController;

    invoke-direct {p0, p2}, Landroid/database/ContentObserver;-><init>(Landroid/os/Handler;)V

    return-void
.end method


# virtual methods
.method public onChange(ZLandroid/net/Uri;)V
    .locals 1
    .param p1, "selfChange"    # Z
    .param p2, "uri"    # Landroid/net/Uri;

    .line 150
    if-eqz p2, :cond_0

    const-string/jumbo v0, "support_fw_app"

    invoke-static {v0}, Landroid/provider/Settings$System;->getUriFor(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v0

    invoke-virtual {p2, v0}, Landroid/net/Uri;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 151
    invoke-static {}, Lcom/miui/server/multisence/MultiSenceConfig;->getInstance()Lcom/miui/server/multisence/MultiSenceConfig;

    move-result-object v0

    invoke-virtual {v0}, Lcom/miui/server/multisence/MultiSenceConfig;->initFWWhiteList()V

    .line 153
    :cond_0
    return-void
.end method
