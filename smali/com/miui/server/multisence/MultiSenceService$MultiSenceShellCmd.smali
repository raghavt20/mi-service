.class Lcom/miui/server/multisence/MultiSenceService$MultiSenceShellCmd;
.super Landroid/os/ShellCommand;
.source "MultiSenceService.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/miui/server/multisence/MultiSenceService;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "MultiSenceShellCmd"
.end annotation


# instance fields
.field mService:Lcom/miui/server/multisence/MultiSenceService;

.field final synthetic this$0:Lcom/miui/server/multisence/MultiSenceService;


# direct methods
.method public constructor <init>(Lcom/miui/server/multisence/MultiSenceService;Lcom/miui/server/multisence/MultiSenceService;)V
    .locals 0
    .param p2, "service"    # Lcom/miui/server/multisence/MultiSenceService;

    .line 468
    iput-object p1, p0, Lcom/miui/server/multisence/MultiSenceService$MultiSenceShellCmd;->this$0:Lcom/miui/server/multisence/MultiSenceService;

    invoke-direct {p0}, Landroid/os/ShellCommand;-><init>()V

    .line 469
    iput-object p2, p0, Lcom/miui/server/multisence/MultiSenceService$MultiSenceShellCmd;->mService:Lcom/miui/server/multisence/MultiSenceService;

    .line 470
    return-void
.end method


# virtual methods
.method public onCommand(Ljava/lang/String;)I
    .locals 5
    .param p1, "cmd"    # Ljava/lang/String;

    .line 474
    if-nez p1, :cond_0

    .line 475
    invoke-virtual {p0, p1}, Lcom/miui/server/multisence/MultiSenceService$MultiSenceShellCmd;->handleDefaultCommands(Ljava/lang/String;)I

    move-result v0

    return v0

    .line 477
    :cond_0
    invoke-virtual {p0}, Lcom/miui/server/multisence/MultiSenceService$MultiSenceShellCmd;->getOutPrintWriter()Ljava/io/PrintWriter;

    move-result-object v0

    .line 479
    .local v0, "pw":Ljava/io/PrintWriter;
    const/4 v1, -0x1

    :try_start_0
    invoke-virtual {p1}, Ljava/lang/String;->hashCode()I

    move-result v2

    const/4 v3, 0x0

    sparse-switch v2, :sswitch_data_0

    :cond_1
    goto :goto_0

    :sswitch_0
    const-string v2, "help"

    invoke-virtual {p1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_1

    move v2, v3

    goto :goto_1

    :sswitch_1
    const-string/jumbo v2, "update-status-sub-function"

    invoke-virtual {p1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_1

    const/4 v2, 0x1

    goto :goto_1

    :sswitch_2
    const-string/jumbo v2, "update-debug"

    invoke-virtual {p1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_1

    const/4 v2, 0x2

    goto :goto_1

    :goto_0
    move v2, v1

    :goto_1
    packed-switch v2, :pswitch_data_0

    .line 490
    invoke-virtual {p0, p1}, Lcom/miui/server/multisence/MultiSenceService$MultiSenceShellCmd;->handleDefaultCommands(Ljava/lang/String;)I

    move-result v1

    goto :goto_3

    .line 487
    :pswitch_0
    invoke-static {}, Lcom/miui/server/multisence/MultiSenceConfig;->getInstance()Lcom/miui/server/multisence/MultiSenceConfig;

    move-result-object v2

    invoke-virtual {v2}, Lcom/miui/server/multisence/MultiSenceConfig;->updateDebugInfo()V

    .line 488
    goto :goto_2

    .line 484
    :pswitch_1
    invoke-static {}, Lcom/miui/server/multisence/MultiSenceConfig;->getInstance()Lcom/miui/server/multisence/MultiSenceConfig;

    move-result-object v2

    invoke-virtual {v2}, Lcom/miui/server/multisence/MultiSenceConfig;->updateSubFuncStatus()V

    .line 485
    goto :goto_2

    .line 481
    :pswitch_2
    invoke-virtual {p0}, Lcom/miui/server/multisence/MultiSenceService$MultiSenceShellCmd;->onHelp()V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 482
    nop

    .line 496
    :goto_2
    nop

    .line 497
    return v3

    .line 490
    :goto_3
    return v1

    .line 492
    :catch_0
    move-exception v2

    .line 493
    .local v2, "e":Ljava/lang/Exception;
    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "Error occurred. Check logcat for details. "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v2}, Ljava/lang/Exception;->getMessage()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v0, v3}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V

    .line 494
    const-string v3, "ShellCommand"

    const-string v4, "Error running shell command"

    invoke-static {v3, v4, v2}, Landroid/util/Slog;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    .line 495
    return v1

    :sswitch_data_0
    .sparse-switch
        -0x79ca97b1 -> :sswitch_2
        -0x7edc844 -> :sswitch_1
        0x30cf41 -> :sswitch_0
    .end sparse-switch

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_2
        :pswitch_1
        :pswitch_0
    .end packed-switch
.end method

.method public onHelp()V
    .locals 2

    .line 502
    invoke-virtual {p0}, Lcom/miui/server/multisence/MultiSenceService$MultiSenceShellCmd;->getOutPrintWriter()Ljava/io/PrintWriter;

    move-result-object v0

    .line 503
    .local v0, "pw":Ljava/io/PrintWriter;
    const-string v1, "Multi-Sence Service Commands:"

    invoke-virtual {v0, v1}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V

    .line 504
    const-string v1, "  help"

    invoke-virtual {v0, v1}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V

    .line 505
    return-void
.end method
