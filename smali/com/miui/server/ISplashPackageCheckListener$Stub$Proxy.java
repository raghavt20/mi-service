class com.miui.server.ISplashPackageCheckListener$Stub$Proxy implements com.miui.server.ISplashPackageCheckListener {
	 /* .source "ISplashPackageCheckListener.java" */
	 /* # interfaces */
	 /* # annotations */
	 /* .annotation system Ldalvik/annotation/EnclosingClass; */
	 /* value = Lcom/miui/server/ISplashPackageCheckListener$Stub; */
} // .end annotation
/* .annotation system Ldalvik/annotation/InnerClass; */
/* accessFlags = 0xa */
/* name = "Proxy" */
} // .end annotation
/* # instance fields */
private android.os.IBinder mRemote;
/* # direct methods */
 com.miui.server.ISplashPackageCheckListener$Stub$Proxy ( ) {
/* .locals 0 */
/* .param p1, "remote" # Landroid/os/IBinder; */
/* .line 73 */
/* invoke-direct {p0}, Ljava/lang/Object;-><init>()V */
/* .line 74 */
this.mRemote = p1;
/* .line 75 */
return;
} // .end method
/* # virtual methods */
public android.os.IBinder asBinder ( ) {
/* .locals 1 */
/* .line 78 */
v0 = this.mRemote;
} // .end method
public java.lang.String getInterfaceDescriptor ( ) {
/* .locals 1 */
/* .line 82 */
final String v0 = "com.miui.server.ISplashPackageCheckListener"; // const-string v0, "com.miui.server.ISplashPackageCheckListener"
} // .end method
public void updateSplashPackageCheckInfo ( com.miui.server.SplashPackageCheckInfo p0 ) {
/* .locals 5 */
/* .param p1, "splashPackageCheckInfo" # Lcom/miui/server/SplashPackageCheckInfo; */
/* .annotation system Ldalvik/annotation/Throws; */
/* value = { */
/* Landroid/os/RemoteException; */
/* } */
} // .end annotation
/* .line 105 */
android.os.Parcel .obtain ( );
/* .line 107 */
/* .local v0, "_data":Landroid/os/Parcel; */
try { // :try_start_0
final String v1 = "com.miui.server.ISplashPackageCheckListener"; // const-string v1, "com.miui.server.ISplashPackageCheckListener"
(( android.os.Parcel ) v0 ).writeInterfaceToken ( v1 ); // invoke-virtual {v0, v1}, Landroid/os/Parcel;->writeInterfaceToken(Ljava/lang/String;)V
/* .line 108 */
int v1 = 1; // const/4 v1, 0x1
int v2 = 0; // const/4 v2, 0x0
if ( p1 != null) { // if-eqz p1, :cond_0
	 /* .line 109 */
	 (( android.os.Parcel ) v0 ).writeInt ( v1 ); // invoke-virtual {v0, v1}, Landroid/os/Parcel;->writeInt(I)V
	 /* .line 110 */
	 (( com.miui.server.SplashPackageCheckInfo ) p1 ).writeToParcel ( v0, v2 ); // invoke-virtual {p1, v0, v2}, Lcom/miui/server/SplashPackageCheckInfo;->writeToParcel(Landroid/os/Parcel;I)V
	 /* .line 113 */
} // :cond_0
(( android.os.Parcel ) v0 ).writeInt ( v2 ); // invoke-virtual {v0, v2}, Landroid/os/Parcel;->writeInt(I)V
/* .line 115 */
} // :goto_0
v2 = this.mRemote;
int v3 = 2; // const/4 v3, 0x2
int v4 = 0; // const/4 v4, 0x0
/* :try_end_0 */
/* .catchall {:try_start_0 ..:try_end_0} :catchall_0 */
/* .line 118 */
(( android.os.Parcel ) v0 ).recycle ( ); // invoke-virtual {v0}, Landroid/os/Parcel;->recycle()V
/* .line 119 */
/* nop */
/* .line 120 */
return;
/* .line 118 */
/* :catchall_0 */
/* move-exception v1 */
(( android.os.Parcel ) v0 ).recycle ( ); // invoke-virtual {v0}, Landroid/os/Parcel;->recycle()V
/* .line 119 */
/* throw v1 */
} // .end method
public void updateSplashPackageCheckInfoList ( java.util.List p0 ) {
/* .locals 4 */
/* .annotation system Ldalvik/annotation/Signature; */
/* value = { */
/* "(", */
/* "Ljava/util/List<", */
/* "Lcom/miui/server/SplashPackageCheckInfo;", */
/* ">;)V" */
/* } */
} // .end annotation
/* .annotation system Ldalvik/annotation/Throws; */
/* value = { */
/* Landroid/os/RemoteException; */
/* } */
} // .end annotation
/* .line 93 */
/* .local p1, "splashPackageCheckInfos":Ljava/util/List;, "Ljava/util/List<Lcom/miui/server/SplashPackageCheckInfo;>;" */
android.os.Parcel .obtain ( );
/* .line 95 */
/* .local v0, "_data":Landroid/os/Parcel; */
try { // :try_start_0
final String v1 = "com.miui.server.ISplashPackageCheckListener"; // const-string v1, "com.miui.server.ISplashPackageCheckListener"
(( android.os.Parcel ) v0 ).writeInterfaceToken ( v1 ); // invoke-virtual {v0, v1}, Landroid/os/Parcel;->writeInterfaceToken(Ljava/lang/String;)V
/* .line 96 */
(( android.os.Parcel ) v0 ).writeTypedList ( p1 ); // invoke-virtual {v0, p1}, Landroid/os/Parcel;->writeTypedList(Ljava/util/List;)V
/* .line 97 */
v1 = this.mRemote;
int v2 = 0; // const/4 v2, 0x0
int v3 = 1; // const/4 v3, 0x1
/* :try_end_0 */
/* .catchall {:try_start_0 ..:try_end_0} :catchall_0 */
/* .line 100 */
(( android.os.Parcel ) v0 ).recycle ( ); // invoke-virtual {v0}, Landroid/os/Parcel;->recycle()V
/* .line 101 */
/* nop */
/* .line 102 */
return;
/* .line 100 */
/* :catchall_0 */
/* move-exception v1 */
(( android.os.Parcel ) v0 ).recycle ( ); // invoke-virtual {v0}, Landroid/os/Parcel;->recycle()V
/* .line 101 */
/* throw v1 */
} // .end method
