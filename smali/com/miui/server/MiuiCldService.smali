.class public Lcom/miui/server/MiuiCldService;
.super Lmiui/hardware/ICldManager$Stub;
.source "MiuiCldService.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/miui/server/MiuiCldService$HALCallback;,
        Lcom/miui/server/MiuiCldService$Lifecycle;
    }
.end annotation


# static fields
.field private static final CLD_OPER_ABORTED:I = 0x3

.field private static final CLD_OPER_DONE:I = 0x2

.field private static final CLD_OPER_FATAL_ERR:I = 0x4

.field private static final CLD_OPER_NOT_SUPPORTED:I = 0x0

.field private static final CLD_OPER_PROCESSING:I = 0x1

.field private static final FRAG_ANALYSIS:I = 0x0

.field private static final FRAG_LEVEL_LOW:I = 0x1

.field private static final FRAG_LEVEL_MEDIUM:I = 0x2

.field private static final FRAG_LEVEL_SERVERE:I = 0x3

.field private static final FRAG_LEVEL_UNKNOWN:I = 0x4

.field private static final GET_CLD_OPERATION_STATUS:I = 0x4

.field private static final GET_FRAGMENT_LEVEL:I = 0x2

.field private static final HALF_A_DAY_MS:J = 0x2932e00L

.field private static final HAL_DEFAULT:Ljava/lang/String; = "default"

.field private static final HAL_INTERFACE_DESCRIPTOR:Ljava/lang/String; = "vendor.xiaomi.hardware.cld@1.0::ICld"

.field private static final HAL_SERVICE_NAME:Ljava/lang/String; = "vendor.xiaomi.hardware.cld@1.0::ICld"

.field private static final IS_CLD_SUPPORTED:I = 0x1

.field private static final MIUI_CLD_PROCESSED_DONE:Ljava/lang/String; = "miui.intent.action.CLD_PROCESSED_DONE"

.field public static final SERVICE_NAME:Ljava/lang/String; = "miui.cld.service"

.field private static final SET_CALLBACK:I = 0x5

.field private static final TAG:Ljava/lang/String;

.field private static final TRIGGER_CLD:I = 0x3


# instance fields
.field private final mContext:Landroid/content/Context;

.field private mHALCallback:Lcom/miui/server/MiuiCldService$HALCallback;

.field private sLastCld:Ljava/util/Date;


# direct methods
.method static bridge synthetic -$$Nest$mreportCldProcessedBroadcast(Lcom/miui/server/MiuiCldService;I)V
    .locals 0

    invoke-direct {p0, p1}, Lcom/miui/server/MiuiCldService;->reportCldProcessedBroadcast(I)V

    return-void
.end method

.method static constructor <clinit>()V
    .locals 1

    .line 29
    const-class v0, Lcom/miui/server/MiuiCldService;

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/miui/server/MiuiCldService;->TAG:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;)V
    .locals 3
    .param p1, "context"    # Landroid/content/Context;

    .line 60
    invoke-direct {p0}, Lmiui/hardware/ICldManager$Stub;-><init>()V

    .line 61
    iput-object p1, p0, Lcom/miui/server/MiuiCldService;->mContext:Landroid/content/Context;

    .line 62
    new-instance v0, Ljava/util/Date;

    const-wide/16 v1, 0x0

    invoke-direct {v0, v1, v2}, Ljava/util/Date;-><init>(J)V

    iput-object v0, p0, Lcom/miui/server/MiuiCldService;->sLastCld:Ljava/util/Date;

    .line 63
    invoke-direct {p0}, Lcom/miui/server/MiuiCldService;->initCallback()V

    .line 64
    return-void
.end method

.method private initCallback()V
    .locals 5

    .line 74
    const-string/jumbo v0, "vendor.xiaomi.hardware.cld@1.0::ICld"

    new-instance v1, Lcom/miui/server/MiuiCldService$HALCallback;

    invoke-direct {v1, p0}, Lcom/miui/server/MiuiCldService$HALCallback;-><init>(Lcom/miui/server/MiuiCldService;)V

    iput-object v1, p0, Lcom/miui/server/MiuiCldService;->mHALCallback:Lcom/miui/server/MiuiCldService$HALCallback;

    .line 75
    new-instance v1, Landroid/os/HwParcel;

    invoke-direct {v1}, Landroid/os/HwParcel;-><init>()V

    .line 77
    .local v1, "hidl_reply":Landroid/os/HwParcel;
    :try_start_0
    const-string v2, "default"

    invoke-static {v0, v2}, Landroid/os/HwBinder;->getService(Ljava/lang/String;Ljava/lang/String;)Landroid/os/IHwBinder;

    move-result-object v2

    .line 78
    .local v2, "hwService":Landroid/os/IHwBinder;
    if-eqz v2, :cond_0

    .line 79
    new-instance v3, Landroid/os/HwParcel;

    invoke-direct {v3}, Landroid/os/HwParcel;-><init>()V

    .line 80
    .local v3, "hidl_request":Landroid/os/HwParcel;
    invoke-virtual {v3, v0}, Landroid/os/HwParcel;->writeInterfaceToken(Ljava/lang/String;)V

    .line 81
    iget-object v0, p0, Lcom/miui/server/MiuiCldService;->mHALCallback:Lcom/miui/server/MiuiCldService$HALCallback;

    invoke-virtual {v0}, Lcom/miui/server/MiuiCldService$HALCallback;->asBinder()Landroid/os/IHwBinder;

    move-result-object v0

    invoke-virtual {v3, v0}, Landroid/os/HwParcel;->writeStrongBinder(Landroid/os/IHwBinder;)V

    .line 82
    const/4 v0, 0x5

    const/4 v4, 0x0

    invoke-interface {v2, v0, v3, v1, v4}, Landroid/os/IHwBinder;->transact(ILandroid/os/HwParcel;Landroid/os/HwParcel;I)V

    .line 83
    invoke-virtual {v1}, Landroid/os/HwParcel;->verifySuccess()V

    .line 84
    invoke-virtual {v3}, Landroid/os/HwParcel;->releaseTemporaryStorage()V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 90
    invoke-virtual {v1}, Landroid/os/HwParcel;->release()V

    .line 85
    return-void

    .line 90
    .end local v2    # "hwService":Landroid/os/IHwBinder;
    .end local v3    # "hidl_request":Landroid/os/HwParcel;
    :cond_0
    nop

    :goto_0
    invoke-virtual {v1}, Landroid/os/HwParcel;->release()V

    .line 91
    goto :goto_1

    .line 90
    :catchall_0
    move-exception v0

    goto :goto_2

    .line 87
    :catch_0
    move-exception v0

    .line 88
    .local v0, "e":Ljava/lang/Exception;
    :try_start_1
    sget-object v2, Lcom/miui/server/MiuiCldService;->TAG:Ljava/lang/String;

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "Transaction failed: "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Landroid/util/Slog;->e(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 90
    nop

    .end local v0    # "e":Ljava/lang/Exception;
    goto :goto_0

    .line 93
    :goto_1
    sget-object v0, Lcom/miui/server/MiuiCldService;->TAG:Ljava/lang/String;

    const-string v2, "initCallback failed."

    invoke-static {v0, v2}, Landroid/util/Slog;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 94
    return-void

    .line 90
    :goto_2
    invoke-virtual {v1}, Landroid/os/HwParcel;->release()V

    .line 91
    throw v0
.end method

.method private reportCldProcessedBroadcast(I)V
    .locals 4
    .param p1, "level"    # I

    .line 67
    new-instance v0, Landroid/content/Intent;

    const-string v1, "miui.intent.action.CLD_PROCESSED_DONE"

    invoke-direct {v0, v1}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 68
    .local v0, "intent":Landroid/content/Intent;
    const-string/jumbo v1, "status"

    invoke-static {p1}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 69
    sget-object v1, Lcom/miui/server/MiuiCldService;->TAG:Ljava/lang/String;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "Send CLD broadcast, status = "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Slog;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 70
    iget-object v1, p0, Lcom/miui/server/MiuiCldService;->mContext:Landroid/content/Context;

    invoke-virtual {v1, v0}, Landroid/content/Context;->sendBroadcast(Landroid/content/Intent;)V

    .line 71
    return-void
.end method


# virtual methods
.method public getCldOperationStatus()I
    .locals 8

    .line 187
    const-string/jumbo v0, "vendor.xiaomi.hardware.cld@1.0::ICld"

    new-instance v1, Landroid/os/HwParcel;

    invoke-direct {v1}, Landroid/os/HwParcel;-><init>()V

    .line 189
    .local v1, "hidl_reply":Landroid/os/HwParcel;
    const/4 v2, 0x0

    :try_start_0
    const-string v3, "default"

    invoke-static {v0, v3}, Landroid/os/HwBinder;->getService(Ljava/lang/String;Ljava/lang/String;)Landroid/os/IHwBinder;

    move-result-object v3

    .line 190
    .local v3, "hwService":Landroid/os/IHwBinder;
    if-eqz v3, :cond_2

    .line 191
    new-instance v4, Landroid/os/HwParcel;

    invoke-direct {v4}, Landroid/os/HwParcel;-><init>()V

    .line 192
    .local v4, "hidl_request":Landroid/os/HwParcel;
    invoke-virtual {v4, v0}, Landroid/os/HwParcel;->writeInterfaceToken(Ljava/lang/String;)V

    .line 193
    const/4 v0, 0x4

    invoke-interface {v3, v0, v4, v1, v2}, Landroid/os/IHwBinder;->transact(ILandroid/os/HwParcel;Landroid/os/HwParcel;I)V

    .line 194
    invoke-virtual {v1}, Landroid/os/HwParcel;->verifySuccess()V

    .line 195
    invoke-virtual {v4}, Landroid/os/HwParcel;->releaseTemporaryStorage()V

    .line 196
    invoke-virtual {v1}, Landroid/os/HwParcel;->readInt32()I

    move-result v5
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 197
    .local v5, "val":I
    if-ltz v5, :cond_1

    if-le v5, v0, :cond_0

    goto :goto_0

    .line 202
    :cond_0
    nop

    .line 207
    invoke-virtual {v1}, Landroid/os/HwParcel;->release()V

    .line 202
    return v5

    .line 198
    :cond_1
    :goto_0
    :try_start_1
    sget-object v0, Lcom/miui/server/MiuiCldService;->TAG:Ljava/lang/String;

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string v7, "Got invalid operation status: "

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6, v5}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-static {v0, v6}, Landroid/util/Slog;->e(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_0
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 199
    nop

    .line 207
    invoke-virtual {v1}, Landroid/os/HwParcel;->release()V

    .line 199
    return v2

    .line 207
    .end local v3    # "hwService":Landroid/os/IHwBinder;
    .end local v4    # "hidl_request":Landroid/os/HwParcel;
    .end local v5    # "val":I
    :cond_2
    nop

    :goto_1
    invoke-virtual {v1}, Landroid/os/HwParcel;->release()V

    .line 208
    goto :goto_2

    .line 207
    :catchall_0
    move-exception v0

    goto :goto_3

    .line 204
    :catch_0
    move-exception v0

    .line 205
    .local v0, "e":Ljava/lang/Exception;
    :try_start_2
    sget-object v3, Lcom/miui/server/MiuiCldService;->TAG:Ljava/lang/String;

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "Transaction failed: "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Landroid/util/Slog;->e(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    .line 207
    nop

    .end local v0    # "e":Ljava/lang/Exception;
    goto :goto_1

    .line 210
    :goto_2
    sget-object v0, Lcom/miui/server/MiuiCldService;->TAG:Ljava/lang/String;

    const-string v3, "Failed calling getCldOperationStatus."

    invoke-static {v0, v3}, Landroid/util/Slog;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 211
    return v2

    .line 207
    :goto_3
    invoke-virtual {v1}, Landroid/os/HwParcel;->release()V

    .line 208
    throw v0
.end method

.method public getFragmentLevel()I
    .locals 8

    .line 128
    const-string/jumbo v0, "vendor.xiaomi.hardware.cld@1.0::ICld"

    new-instance v1, Landroid/os/HwParcel;

    invoke-direct {v1}, Landroid/os/HwParcel;-><init>()V

    .line 130
    .local v1, "hidl_reply":Landroid/os/HwParcel;
    const/4 v2, 0x4

    :try_start_0
    const-string v3, "default"

    invoke-static {v0, v3}, Landroid/os/HwBinder;->getService(Ljava/lang/String;Ljava/lang/String;)Landroid/os/IHwBinder;

    move-result-object v3

    .line 131
    .local v3, "hwService":Landroid/os/IHwBinder;
    if-eqz v3, :cond_2

    .line 132
    new-instance v4, Landroid/os/HwParcel;

    invoke-direct {v4}, Landroid/os/HwParcel;-><init>()V

    .line 133
    .local v4, "hidl_request":Landroid/os/HwParcel;
    invoke-virtual {v4, v0}, Landroid/os/HwParcel;->writeInterfaceToken(Ljava/lang/String;)V

    .line 134
    const/4 v0, 0x2

    const/4 v5, 0x0

    invoke-interface {v3, v0, v4, v1, v5}, Landroid/os/IHwBinder;->transact(ILandroid/os/HwParcel;Landroid/os/HwParcel;I)V

    .line 135
    invoke-virtual {v1}, Landroid/os/HwParcel;->verifySuccess()V

    .line 136
    invoke-virtual {v4}, Landroid/os/HwParcel;->releaseTemporaryStorage()V

    .line 137
    invoke-virtual {v1}, Landroid/os/HwParcel;->readInt32()I

    move-result v0
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 138
    .local v0, "val":I
    if-ltz v0, :cond_1

    if-le v0, v2, :cond_0

    goto :goto_0

    .line 143
    :cond_0
    nop

    .line 148
    invoke-virtual {v1}, Landroid/os/HwParcel;->release()V

    .line 143
    return v0

    .line 139
    :cond_1
    :goto_0
    :try_start_1
    sget-object v5, Lcom/miui/server/MiuiCldService;->TAG:Ljava/lang/String;

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string v7, "Got invalid fragment level: "

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-static {v5, v6}, Landroid/util/Slog;->e(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_0
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 140
    nop

    .line 148
    invoke-virtual {v1}, Landroid/os/HwParcel;->release()V

    .line 140
    return v2

    .line 148
    .end local v0    # "val":I
    .end local v3    # "hwService":Landroid/os/IHwBinder;
    .end local v4    # "hidl_request":Landroid/os/HwParcel;
    :cond_2
    nop

    :goto_1
    invoke-virtual {v1}, Landroid/os/HwParcel;->release()V

    .line 149
    goto :goto_2

    .line 148
    :catchall_0
    move-exception v0

    goto :goto_3

    .line 145
    :catch_0
    move-exception v0

    .line 146
    .local v0, "e":Ljava/lang/Exception;
    :try_start_2
    sget-object v3, Lcom/miui/server/MiuiCldService;->TAG:Ljava/lang/String;

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "Transaction failed: "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Landroid/util/Slog;->e(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    .line 148
    nop

    .end local v0    # "e":Ljava/lang/Exception;
    goto :goto_1

    .line 151
    :goto_2
    sget-object v0, Lcom/miui/server/MiuiCldService;->TAG:Ljava/lang/String;

    const-string v3, "Failed calling getFragmentLevel."

    invoke-static {v0, v3}, Landroid/util/Slog;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 152
    return v2

    .line 148
    :goto_3
    invoke-virtual {v1}, Landroid/os/HwParcel;->release()V

    .line 149
    throw v0
.end method

.method public isCldSupported()Z
    .locals 8

    .line 98
    const-string/jumbo v0, "vendor.xiaomi.hardware.cld@1.0::ICld"

    new-instance v1, Landroid/os/HwParcel;

    invoke-direct {v1}, Landroid/os/HwParcel;-><init>()V

    .line 100
    .local v1, "hidl_reply":Landroid/os/HwParcel;
    const/4 v2, 0x0

    :try_start_0
    const-string v3, "default"

    invoke-static {v0, v3}, Landroid/os/HwBinder;->getService(Ljava/lang/String;Ljava/lang/String;)Landroid/os/IHwBinder;

    move-result-object v3

    .line 101
    .local v3, "hwService":Landroid/os/IHwBinder;
    if-eqz v3, :cond_1

    .line 102
    new-instance v4, Landroid/os/HwParcel;

    invoke-direct {v4}, Landroid/os/HwParcel;-><init>()V

    .line 103
    .local v4, "hidl_request":Landroid/os/HwParcel;
    invoke-virtual {v4, v0}, Landroid/os/HwParcel;->writeInterfaceToken(Ljava/lang/String;)V

    .line 104
    const/4 v0, 0x1

    invoke-interface {v3, v0, v4, v1, v2}, Landroid/os/IHwBinder;->transact(ILandroid/os/HwParcel;Landroid/os/HwParcel;I)V

    .line 105
    invoke-virtual {v1}, Landroid/os/HwParcel;->verifySuccess()V

    .line 106
    invoke-virtual {v4}, Landroid/os/HwParcel;->releaseTemporaryStorage()V

    .line 107
    invoke-virtual {v1}, Landroid/os/HwParcel;->readInt32()I

    move-result v5

    .line 108
    .local v5, "val":I
    if-nez v5, :cond_0

    .line 109
    sget-object v0, Lcom/miui/server/MiuiCldService;->TAG:Ljava/lang/String;

    const-string v6, "CLD not supported on current device!"

    invoke-static {v0, v6}, Landroid/util/Slog;->e(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 110
    nop

    .line 119
    invoke-virtual {v1}, Landroid/os/HwParcel;->release()V

    .line 110
    return v2

    .line 112
    :cond_0
    :try_start_1
    sget-object v6, Lcom/miui/server/MiuiCldService;->TAG:Ljava/lang/String;

    const-string v7, "CLD supported."

    invoke-static {v6, v7}, Landroid/util/Slog;->i(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_0
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 113
    nop

    .line 119
    invoke-virtual {v1}, Landroid/os/HwParcel;->release()V

    .line 113
    return v0

    .line 119
    .end local v3    # "hwService":Landroid/os/IHwBinder;
    .end local v4    # "hidl_request":Landroid/os/HwParcel;
    .end local v5    # "val":I
    :cond_1
    nop

    :goto_0
    invoke-virtual {v1}, Landroid/os/HwParcel;->release()V

    .line 120
    goto :goto_1

    .line 119
    :catchall_0
    move-exception v0

    goto :goto_2

    .line 116
    :catch_0
    move-exception v0

    .line 117
    .local v0, "e":Ljava/lang/Exception;
    :try_start_2
    sget-object v3, Lcom/miui/server/MiuiCldService;->TAG:Ljava/lang/String;

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "Transaction failed: "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Landroid/util/Slog;->e(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    .line 119
    nop

    .end local v0    # "e":Ljava/lang/Exception;
    goto :goto_0

    .line 122
    :goto_1
    sget-object v0, Lcom/miui/server/MiuiCldService;->TAG:Ljava/lang/String;

    const-string v3, "Failed calling isCldSupported."

    invoke-static {v0, v3}, Landroid/util/Slog;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 123
    return v2

    .line 119
    :goto_2
    invoke-virtual {v1}, Landroid/os/HwParcel;->release()V

    .line 120
    throw v0
.end method

.method public triggerCld(I)V
    .locals 6
    .param p1, "val"    # I

    .line 158
    const-string/jumbo v0, "vendor.xiaomi.hardware.cld@1.0::ICld"

    new-instance v1, Ljava/util/Date;

    invoke-direct {v1}, Ljava/util/Date;-><init>()V

    .line 159
    .local v1, "now":Ljava/util/Date;
    invoke-virtual {v1}, Ljava/util/Date;->getTime()J

    move-result-wide v2

    iget-object v4, p0, Lcom/miui/server/MiuiCldService;->sLastCld:Ljava/util/Date;

    invoke-virtual {v4}, Ljava/util/Date;->getTime()J

    move-result-wide v4

    sub-long/2addr v2, v4

    const-wide/32 v4, 0x2932e00

    cmp-long v2, v2, v4

    if-gez v2, :cond_0

    .line 160
    sget-object v2, Lcom/miui/server/MiuiCldService;->TAG:Ljava/lang/String;

    const-string v3, "Less than half a day before last defragmentation!"

    invoke-static {v2, v3}, Landroid/util/Slog;->w(Ljava/lang/String;Ljava/lang/String;)I

    .line 162
    :cond_0
    iput-object v1, p0, Lcom/miui/server/MiuiCldService;->sLastCld:Ljava/util/Date;

    .line 164
    new-instance v2, Landroid/os/HwParcel;

    invoke-direct {v2}, Landroid/os/HwParcel;-><init>()V

    .line 166
    .local v2, "hidl_reply":Landroid/os/HwParcel;
    :try_start_0
    const-string v3, "default"

    invoke-static {v0, v3}, Landroid/os/HwBinder;->getService(Ljava/lang/String;Ljava/lang/String;)Landroid/os/IHwBinder;

    move-result-object v3

    .line 167
    .local v3, "hwService":Landroid/os/IHwBinder;
    if-eqz v3, :cond_1

    .line 168
    new-instance v4, Landroid/os/HwParcel;

    invoke-direct {v4}, Landroid/os/HwParcel;-><init>()V

    .line 169
    .local v4, "hidl_request":Landroid/os/HwParcel;
    invoke-virtual {v4, v0}, Landroid/os/HwParcel;->writeInterfaceToken(Ljava/lang/String;)V

    .line 170
    invoke-virtual {v4, p1}, Landroid/os/HwParcel;->writeInt32(I)V

    .line 171
    const/4 v0, 0x3

    const/4 v5, 0x0

    invoke-interface {v3, v0, v4, v2, v5}, Landroid/os/IHwBinder;->transact(ILandroid/os/HwParcel;Landroid/os/HwParcel;I)V

    .line 172
    invoke-virtual {v2}, Landroid/os/HwParcel;->verifySuccess()V

    .line 173
    invoke-virtual {v4}, Landroid/os/HwParcel;->releaseTemporaryStorage()V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 179
    invoke-virtual {v2}, Landroid/os/HwParcel;->release()V

    .line 174
    return-void

    .line 179
    .end local v3    # "hwService":Landroid/os/IHwBinder;
    .end local v4    # "hidl_request":Landroid/os/HwParcel;
    :cond_1
    nop

    :goto_0
    invoke-virtual {v2}, Landroid/os/HwParcel;->release()V

    .line 180
    goto :goto_1

    .line 179
    :catchall_0
    move-exception v0

    goto :goto_2

    .line 176
    :catch_0
    move-exception v0

    .line 177
    .local v0, "e":Ljava/lang/Exception;
    :try_start_1
    sget-object v3, Lcom/miui/server/MiuiCldService;->TAG:Ljava/lang/String;

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "Transaction failed: "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Landroid/util/Slog;->e(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 179
    nop

    .end local v0    # "e":Ljava/lang/Exception;
    goto :goto_0

    .line 182
    :goto_1
    sget-object v0, Lcom/miui/server/MiuiCldService;->TAG:Ljava/lang/String;

    const-string v3, "Failed calling triggerCld."

    invoke-static {v0, v3}, Landroid/util/Slog;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 183
    return-void

    .line 179
    :goto_2
    invoke-virtual {v2}, Landroid/os/HwParcel;->release()V

    .line 180
    throw v0
.end method
