.class Lcom/miui/server/InputMethodHelper$InputMethodTbCmdRuleObserver;
.super Landroid/database/ContentObserver;
.source "InputMethodHelper.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/miui/server/InputMethodHelper;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0xa
    name = "InputMethodTbCmdRuleObserver"
.end annotation


# instance fields
.field private mContext:Landroid/content/Context;


# direct methods
.method public constructor <init>(Landroid/os/Handler;Landroid/content/Context;)V
    .locals 0
    .param p1, "handler"    # Landroid/os/Handler;
    .param p2, "context"    # Landroid/content/Context;

    .line 120
    invoke-direct {p0, p1}, Landroid/database/ContentObserver;-><init>(Landroid/os/Handler;)V

    .line 121
    iput-object p2, p0, Lcom/miui/server/InputMethodHelper$InputMethodTbCmdRuleObserver;->mContext:Landroid/content/Context;

    .line 122
    return-void
.end method


# virtual methods
.method public onChange(Z)V
    .locals 1
    .param p1, "selfChange"    # Z

    .line 126
    invoke-super {p0, p1}, Landroid/database/ContentObserver;->onChange(Z)V

    .line 127
    iget-object v0, p0, Lcom/miui/server/InputMethodHelper$InputMethodTbCmdRuleObserver;->mContext:Landroid/content/Context;

    invoke-static {v0}, Lcom/miui/server/InputMethodHelper;->-$$Nest$sminitInputMethodTbCmdRule(Landroid/content/Context;)V

    .line 128
    return-void
.end method
