.class Lcom/miui/server/GreenGuardManagerService$GreenGuardServiceConn;
.super Ljava/lang/Object;
.source "GreenGuardManagerService.java"

# interfaces
.implements Landroid/content/ServiceConnection;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/miui/server/GreenGuardManagerService;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x8
    name = "GreenGuardServiceConn"
.end annotation


# instance fields
.field private final mContext:Landroid/content/Context;


# direct methods
.method public static synthetic $r8$lambda$d7pOZ-1HlkJK0vHz3FOHR4cpU1s(Lcom/miui/server/GreenGuardManagerService$GreenGuardServiceConn;)V
    .locals 0

    invoke-direct {p0}, Lcom/miui/server/GreenGuardManagerService$GreenGuardServiceConn;->lambda$onServiceDisconnected$0()V

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;)V
    .locals 0
    .param p1, "context"    # Landroid/content/Context;

    .line 80
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 81
    iput-object p1, p0, Lcom/miui/server/GreenGuardManagerService$GreenGuardServiceConn;->mContext:Landroid/content/Context;

    .line 82
    return-void
.end method

.method private synthetic lambda$onServiceDisconnected$0()V
    .locals 1

    .line 93
    iget-object v0, p0, Lcom/miui/server/GreenGuardManagerService$GreenGuardServiceConn;->mContext:Landroid/content/Context;

    invoke-static {v0}, Lcom/miui/server/GreenGuardManagerService;->-$$Nest$smstartWatchGreenGuardProcessInner(Landroid/content/Context;)V

    return-void
.end method


# virtual methods
.method public onServiceConnected(Landroid/content/ComponentName;Landroid/os/IBinder;)V
    .locals 2
    .param p1, "name"    # Landroid/content/ComponentName;
    .param p2, "service"    # Landroid/os/IBinder;

    .line 86
    const-string v0, "GreenKidManagerService"

    const-string v1, "On GreenKidService Connected"

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 87
    return-void
.end method

.method public onServiceDisconnected(Landroid/content/ComponentName;)V
    .locals 4
    .param p1, "name"    # Landroid/content/ComponentName;

    .line 91
    const-string v0, "GreenKidManagerService"

    const-string v1, "On GreenKidService Disconnected , schedule restart it in 10s."

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 92
    invoke-static {}, Lcom/android/internal/os/BackgroundThread;->getHandler()Landroid/os/Handler;

    move-result-object v0

    new-instance v1, Lcom/miui/server/GreenGuardManagerService$GreenGuardServiceConn$$ExternalSyntheticLambda0;

    invoke-direct {v1, p0}, Lcom/miui/server/GreenGuardManagerService$GreenGuardServiceConn$$ExternalSyntheticLambda0;-><init>(Lcom/miui/server/GreenGuardManagerService$GreenGuardServiceConn;)V

    const-wide/16 v2, 0x2710

    invoke-virtual {v0, v1, v2, v3}, Landroid/os/Handler;->postDelayed(Ljava/lang/Runnable;J)Z

    .line 94
    return-void
.end method
