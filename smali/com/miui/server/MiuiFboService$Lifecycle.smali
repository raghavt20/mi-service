.class public final Lcom/miui/server/MiuiFboService$Lifecycle;
.super Lcom/android/server/SystemService;
.source "MiuiFboService.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/miui/server/MiuiFboService;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "Lifecycle"
.end annotation


# instance fields
.field private final mService:Lcom/miui/server/MiuiFboService;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 1
    .param p1, "context"    # Landroid/content/Context;

    .line 368
    invoke-direct {p0, p1}, Lcom/android/server/SystemService;-><init>(Landroid/content/Context;)V

    .line 369
    invoke-static {}, Lcom/miui/server/MiuiFboService;->getInstance()Lcom/miui/server/MiuiFboService;

    move-result-object v0

    invoke-static {v0, p1}, Lcom/miui/server/MiuiFboService;->-$$Nest$mforSystemServerInitialization(Lcom/miui/server/MiuiFboService;Landroid/content/Context;)Lcom/miui/server/MiuiFboService;

    move-result-object v0

    iput-object v0, p0, Lcom/miui/server/MiuiFboService$Lifecycle;->mService:Lcom/miui/server/MiuiFboService;

    .line 370
    return-void
.end method


# virtual methods
.method public onStart()V
    .locals 2

    .line 374
    const-string v0, "miui.fbo.service"

    iget-object v1, p0, Lcom/miui/server/MiuiFboService$Lifecycle;->mService:Lcom/miui/server/MiuiFboService;

    invoke-virtual {p0, v0, v1}, Lcom/miui/server/MiuiFboService$Lifecycle;->publishBinderService(Ljava/lang/String;Landroid/os/IBinder;)V

    .line 375
    return-void
.end method
