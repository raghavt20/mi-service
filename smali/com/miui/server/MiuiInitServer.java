public class com.miui.server.MiuiInitServer extends miui.os.IMiuiInit$Stub {
	 /* .source "MiuiInitServer.java" */
	 /* # annotations */
	 /* .annotation system Ldalvik/annotation/MemberClasses; */
	 /* value = { */
	 /* Lcom/miui/server/MiuiInitServer$InitCustEnvironmentTask;, */
	 /* Lcom/miui/server/MiuiInitServer$Lifecycle; */
	 /* } */
} // .end annotation
/* # static fields */
private static final java.lang.String CARRIER_REGION_PROP_FILE_NAME;
private static final java.lang.String CUST_PROPERTIES_FILE_NAME;
private static final java.lang.String PREINSTALL_APP_HISTORY_FILE;
private static final java.lang.String PREINSTALL_PACKAGE_LIST;
private static final java.lang.String TAG;
/* # instance fields */
com.miui.server.MiuiCompatModePackages mCompatModePackages;
private final android.content.Context mContext;
private Boolean mDoing;
private com.android.server.pm.MiuiPreinstallHelper mMiuiPreinstallHelper;
Boolean mNeedAspectSettings;
private java.util.HashMap mPreinstallHistoryMap;
/* .annotation system Ldalvik/annotation/Signature; */
/* value = { */
/* "Ljava/util/HashMap<", */
/* "Ljava/lang/String;", */
/* "Ljava/lang/String;", */
/* ">;" */
/* } */
} // .end annotation
} // .end field
private java.util.ArrayList mPreinstalledChannels;
/* .annotation system Ldalvik/annotation/Signature; */
/* value = { */
/* "Ljava/util/ArrayList<", */
/* "Ljava/lang/String;", */
/* ">;" */
/* } */
} // .end annotation
} // .end field
/* # direct methods */
static android.content.Context -$$Nest$fgetmContext ( com.miui.server.MiuiInitServer p0 ) { //bridge//synthethic
/* .locals 0 */
p0 = this.mContext;
} // .end method
static com.android.server.pm.MiuiPreinstallHelper -$$Nest$fgetmMiuiPreinstallHelper ( com.miui.server.MiuiInitServer p0 ) { //bridge//synthethic
/* .locals 0 */
p0 = this.mMiuiPreinstallHelper;
} // .end method
static void -$$Nest$fputmDoing ( com.miui.server.MiuiInitServer p0, Boolean p1 ) { //bridge//synthethic
/* .locals 0 */
/* iput-boolean p1, p0, Lcom/miui/server/MiuiInitServer;->mDoing:Z */
return;
} // .end method
static void -$$Nest$mdeletePackagesByRegion ( com.miui.server.MiuiInitServer p0 ) { //bridge//synthethic
/* .locals 0 */
/* invoke-direct {p0}, Lcom/miui/server/MiuiInitServer;->deletePackagesByRegion()V */
return;
} // .end method
public com.miui.server.MiuiInitServer ( ) {
/* .locals 3 */
/* .param p1, "context" # Landroid/content/Context; */
/* .line 82 */
/* invoke-direct {p0}, Lmiui/os/IMiuiInit$Stub;-><init>()V */
/* .line 83 */
this.mContext = p1;
/* .line 84 */
com.miui.server.EnableStateManager .updateApplicationEnableState ( p1 );
/* .line 85 */
/* invoke-direct {p0}, Lcom/miui/server/MiuiInitServer;->deletePackagesByRegion()V */
/* .line 86 */
com.android.server.pm.MiuiPreinstallHelper .getInstance ( );
this.mMiuiPreinstallHelper = v0;
/* .line 89 */
miui.util.MiuiFeatureUtils .setMiuisdkProperties ( );
/* .line 90 */
/* const-string/jumbo v0, "window" */
android.os.ServiceManager .getService ( v0 );
/* check-cast v0, Lcom/android/server/wm/WindowManagerService; */
/* .line 91 */
/* .local v0, "windowManager":Lcom/android/server/wm/WindowManagerService; */
int v1 = 0; // const/4 v1, 0x0
v2 = (( com.android.server.wm.WindowManagerService ) v0 ).hasNavigationBar ( v1 ); // invoke-virtual {v0, v1}, Lcom/android/server/wm/WindowManagerService;->hasNavigationBar(I)Z
/* iput-boolean v2, p0, Lcom/miui/server/MiuiInitServer;->mNeedAspectSettings:Z */
/* .line 92 */
if ( v2 != null) { // if-eqz v2, :cond_0
/* sget-boolean v2, Lmiui/os/Build;->IS_TABLET:Z */
/* if-nez v2, :cond_0 */
int v1 = 1; // const/4 v1, 0x1
} // :cond_0
/* iput-boolean v1, p0, Lcom/miui/server/MiuiInitServer;->mNeedAspectSettings:Z */
/* .line 93 */
/* if-nez v1, :cond_1 */
/* sget-boolean v1, Lmiui/util/CustomizeUtil;->HAS_NOTCH:Z */
if ( v1 != null) { // if-eqz v1, :cond_2
/* .line 94 */
} // :cond_1
/* new-instance v1, Lcom/miui/server/MiuiCompatModePackages; */
/* invoke-direct {v1, p1}, Lcom/miui/server/MiuiCompatModePackages;-><init>(Landroid/content/Context;)V */
this.mCompatModePackages = v1;
/* .line 96 */
} // :cond_2
return;
} // .end method
private void checkPermission ( java.lang.String p0 ) {
/* .locals 4 */
/* .param p1, "reason" # Ljava/lang/String; */
/* .line 581 */
v0 = this.mContext;
final String v1 = "android.permission.SET_SCREEN_COMPATIBILITY"; // const-string v1, "android.permission.SET_SCREEN_COMPATIBILITY"
v0 = (( android.content.Context ) v0 ).checkCallingPermission ( v1 ); // invoke-virtual {v0, v1}, Landroid/content/Context;->checkCallingPermission(Ljava/lang/String;)I
/* .line 583 */
/* .local v0, "permission":I */
/* if-nez v0, :cond_0 */
/* .line 584 */
return;
/* .line 585 */
} // :cond_0
/* new-instance v1, Ljava/lang/SecurityException; */
/* new-instance v2, Ljava/lang/StringBuilder; */
/* invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V */
final String v3 = "Permission Denial: "; // const-string v3, "Permission Denial: "
(( java.lang.StringBuilder ) v2 ).append ( v3 ); // invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v2 ).append ( p1 ); // invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
final String v3 = " pid="; // const-string v3, " pid="
(( java.lang.StringBuilder ) v2 ).append ( v3 ); // invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
/* .line 586 */
v3 = android.os.Binder .getCallingPid ( );
(( java.lang.StringBuilder ) v2 ).append ( v3 ); // invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;
final String v3 = ", uid="; // const-string v3, ", uid="
(( java.lang.StringBuilder ) v2 ).append ( v3 ); // invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
v3 = android.os.Binder .getCallingUid ( );
(( java.lang.StringBuilder ) v2 ).append ( v3 ); // invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v2 ).toString ( ); // invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
/* invoke-direct {v1, v2}, Ljava/lang/SecurityException;-><init>(Ljava/lang/String;)V */
/* throw v1 */
} // .end method
private void deletePackagesByRegion ( ) {
/* .locals 4 */
/* .line 591 */
final String v0 = "ro.miui.region"; // const-string v0, "ro.miui.region"
final String v1 = ""; // const-string v1, ""
android.os.SystemProperties .get ( v0,v1 );
final String v2 = "IT"; // const-string v2, "IT"
v0 = (( java.lang.String ) v2 ).equals ( v0 ); // invoke-virtual {v2, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z
if ( v0 != null) { // if-eqz v0, :cond_0
final String v0 = "ro.miui.build.region"; // const-string v0, "ro.miui.build.region"
android.os.SystemProperties .get ( v0,v1 );
final String v1 = "eea"; // const-string v1, "eea"
v0 = (( java.lang.String ) v1 ).equals ( v0 ); // invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z
if ( v0 != null) { // if-eqz v0, :cond_0
/* .line 593 */
try { // :try_start_0
v0 = this.mContext;
(( android.content.Context ) v0 ).getPackageManager ( ); // invoke-virtual {v0}, Landroid/content/Context;->getPackageManager()Landroid/content/pm/PackageManager;
/* .line 594 */
/* .local v0, "pm":Landroid/content/pm/PackageManager; */
final String v1 = "com.miui.fm"; // const-string v1, "com.miui.fm"
int v2 = 0; // const/4 v2, 0x0
int v3 = 0; // const/4 v3, 0x0
(( android.content.pm.PackageManager ) v0 ).deletePackage ( v1, v3, v2 ); // invoke-virtual {v0, v1, v3, v2}, Landroid/content/pm/PackageManager;->deletePackage(Ljava/lang/String;Landroid/content/pm/IPackageDeleteObserver;I)V
/* .line 595 */
final String v1 = "com.miui.fmservice"; // const-string v1, "com.miui.fmservice"
(( android.content.pm.PackageManager ) v0 ).deletePackage ( v1, v3, v2 ); // invoke-virtual {v0, v1, v3, v2}, Landroid/content/pm/PackageManager;->deletePackage(Ljava/lang/String;Landroid/content/pm/IPackageDeleteObserver;I)V
/* :try_end_0 */
/* .catch Ljava/lang/Exception; {:try_start_0 ..:try_end_0} :catch_0 */
/* .line 597 */
} // .end local v0 # "pm":Landroid/content/pm/PackageManager;
/* .line 596 */
/* :catch_0 */
/* move-exception v0 */
/* .line 599 */
} // :cond_0
} // :goto_0
return;
} // .end method
/* # virtual methods */
public void copyPreinstallPAITrackingFile ( java.lang.String p0, java.lang.String p1, java.lang.String p2 ) {
/* .locals 3 */
/* .param p1, "type" # Ljava/lang/String; */
/* .param p2, "fileName" # Ljava/lang/String; */
/* .param p3, "content" # Ljava/lang/String; */
/* .annotation system Ldalvik/annotation/Throws; */
/* value = { */
/* Landroid/os/RemoteException; */
/* } */
} // .end annotation
/* .line 463 */
v0 = this.mContext;
final String v1 = "miui.os.permisson.INIT_MIUI_ENVIRONMENT"; // const-string v1, "miui.os.permisson.INIT_MIUI_ENVIRONMENT"
int v2 = 0; // const/4 v2, 0x0
(( android.content.Context ) v0 ).enforceCallingOrSelfPermission ( v1, v2 ); // invoke-virtual {v0, v1, v2}, Landroid/content/Context;->enforceCallingOrSelfPermission(Ljava/lang/String;Ljava/lang/String;)V
/* .line 466 */
v0 = android.text.TextUtils .isEmpty ( p1 );
/* if-nez v0, :cond_2 */
v0 = android.text.TextUtils .isEmpty ( p3 );
if ( v0 != null) { // if-eqz v0, :cond_0
/* .line 470 */
} // :cond_0
v0 = this.mMiuiPreinstallHelper;
v0 = (( com.android.server.pm.MiuiPreinstallHelper ) v0 ).isSupportNewFrame ( ); // invoke-virtual {v0}, Lcom/android/server/pm/MiuiPreinstallHelper;->isSupportNewFrame()Z
if ( v0 != null) { // if-eqz v0, :cond_1
/* .line 471 */
com.android.server.pm.MiuiPAIPreinstallConfig .copyPreinstallPAITrackingFile ( p1,p2,p3 );
/* .line 473 */
} // :cond_1
com.android.server.pm.PreinstallApp .copyPreinstallPAITrackingFile ( p1,p2,p3 );
/* .line 475 */
} // :goto_0
return;
/* .line 467 */
} // :cond_2
} // :goto_1
return;
} // .end method
public Boolean deleteAllEventRecords ( Integer p0 ) {
/* .locals 1 */
/* .param p1, "eventType" # I */
/* .line 612 */
/* const-class v0, Lcom/android/server/pm/PackageEventRecorderInternal; */
com.android.server.LocalServices .getService ( v0 );
/* check-cast v0, Lcom/android/server/pm/PackageEventRecorderInternal; */
v0 = /* .line 613 */
/* .line 612 */
} // .end method
public void deleteEventRecords ( Integer p0, java.util.List p1 ) {
/* .locals 1 */
/* .param p1, "eventType" # I */
/* .annotation system Ldalvik/annotation/Signature; */
/* value = { */
/* "(I", */
/* "Ljava/util/List<", */
/* "Ljava/lang/String;", */
/* ">;)V" */
/* } */
} // .end annotation
/* .line 607 */
/* .local p2, "eventIds":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;" */
/* const-class v0, Lcom/android/server/pm/PackageEventRecorderInternal; */
com.android.server.LocalServices .getService ( v0 );
/* check-cast v0, Lcom/android/server/pm/PackageEventRecorderInternal; */
/* .line 608 */
/* .line 609 */
return;
} // .end method
public void doFactoryReset ( Boolean p0 ) {
/* .locals 2 */
/* .param p1, "keepUserApps" # Z */
/* .annotation system Ldalvik/annotation/Throws; */
/* value = { */
/* Landroid/os/RemoteException; */
/* } */
} // .end annotation
/* .line 351 */
/* sget-boolean v0, Lmiui/os/Build;->IS_GLOBAL_BUILD:Z */
if ( v0 != null) { // if-eqz v0, :cond_0
/* .line 352 */
final String v0 = ""; // const-string v0, ""
miui.util.CustomizeUtil .setMiuiCustVariatDir ( v0 );
/* .line 353 */
miui.util.CustomizeUtil .getMiuiCustVariantFile ( );
/* .line 354 */
/* .local v0, "file":Ljava/io/File; */
v1 = (( java.io.File ) v0 ).exists ( ); // invoke-virtual {v0}, Ljava/io/File;->exists()Z
if ( v1 != null) { // if-eqz v1, :cond_0
/* .line 355 */
(( java.io.File ) v0 ).delete ( ); // invoke-virtual {v0}, Ljava/io/File;->delete()Z
/* .line 358 */
} // .end local v0 # "file":Ljava/io/File;
} // :cond_0
/* if-nez p1, :cond_1 */
/* .line 359 */
/* new-instance v0, Ljava/io/File; */
final String v1 = "/data/app/preinstall_history"; // const-string v1, "/data/app/preinstall_history"
/* invoke-direct {v0, v1}, Ljava/io/File;-><init>(Ljava/lang/String;)V */
/* .line 360 */
/* .restart local v0 # "file":Ljava/io/File; */
v1 = (( java.io.File ) v0 ).exists ( ); // invoke-virtual {v0}, Ljava/io/File;->exists()Z
if ( v1 != null) { // if-eqz v1, :cond_1
/* .line 361 */
(( java.io.File ) v0 ).delete ( ); // invoke-virtual {v0}, Ljava/io/File;->delete()Z
/* .line 364 */
} // .end local v0 # "file":Ljava/io/File;
} // :cond_1
return;
} // .end method
public Float getAspectRatio ( java.lang.String p0 ) {
/* .locals 1 */
/* .param p1, "pkg" # Ljava/lang/String; */
/* .line 527 */
/* iget-boolean v0, p0, Lcom/miui/server/MiuiInitServer;->mNeedAspectSettings:Z */
/* if-nez v0, :cond_0 */
/* .line 528 */
/* const/high16 v0, 0x40400000 # 3.0f */
/* .line 530 */
} // :cond_0
v0 = this.mCompatModePackages;
v0 = (( com.miui.server.MiuiCompatModePackages ) v0 ).getAspectRatio ( p1 ); // invoke-virtual {v0, p1}, Lcom/miui/server/MiuiCompatModePackages;->getAspectRatio(Ljava/lang/String;)F
} // .end method
public java.lang.String getCustVariants ( ) {
/* .locals 13 */
/* .annotation system Ldalvik/annotation/Throws; */
/* value = { */
/* Landroid/os/RemoteException; */
/* } */
} // .end annotation
/* .line 327 */
/* new-instance v0, Ljava/util/ArrayList; */
/* invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V */
/* .line 328 */
/* .local v0, "regionList":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/String;>;" */
miui.util.CustomizeUtil .getMiuiCustPropDir ( );
/* .line 329 */
/* .local v1, "cust":Ljava/io/File; */
java.util.Locale .getISOCountries ( );
/* .line 330 */
/* .local v2, "cs":[Ljava/lang/String; */
(( java.io.File ) v1 ).listFiles ( ); // invoke-virtual {v1}, Ljava/io/File;->listFiles()[Ljava/io/File;
/* .line 331 */
/* .local v3, "resgions":[Ljava/io/File; */
int v4 = 0; // const/4 v4, 0x0
if ( v3 != null) { // if-eqz v3, :cond_3
/* .line 332 */
/* array-length v5, v3 */
/* move v6, v4 */
} // :goto_0
/* if-ge v6, v5, :cond_3 */
/* aget-object v7, v3, v6 */
/* .line 333 */
/* .local v7, "region":Ljava/io/File; */
v8 = (( java.io.File ) v7 ).isDirectory ( ); // invoke-virtual {v7}, Ljava/io/File;->isDirectory()Z
/* if-nez v8, :cond_0 */
/* .line 334 */
/* .line 337 */
} // :cond_0
(( java.io.File ) v7 ).getName ( ); // invoke-virtual {v7}, Ljava/io/File;->getName()Ljava/lang/String;
/* .line 338 */
/* .local v8, "r":Ljava/lang/String; */
/* array-length v9, v2 */
/* move v10, v4 */
} // :goto_1
/* if-ge v10, v9, :cond_2 */
/* aget-object v11, v2, v10 */
/* .line 339 */
/* .local v11, "c":Ljava/lang/String; */
v12 = (( java.lang.String ) v11 ).equalsIgnoreCase ( v8 ); // invoke-virtual {v11, v8}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z
if ( v12 != null) { // if-eqz v12, :cond_1
/* .line 340 */
(( java.util.ArrayList ) v0 ).add ( v8 ); // invoke-virtual {v0, v8}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z
/* .line 341 */
/* nop */
/* .line 338 */
} // .end local v11 # "c":Ljava/lang/String;
} // :cond_1
/* add-int/lit8 v10, v10, 0x1 */
/* .line 332 */
} // .end local v7 # "region":Ljava/io/File;
} // .end local v8 # "r":Ljava/lang/String;
} // :cond_2
} // :goto_2
/* add-int/lit8 v6, v6, 0x1 */
/* .line 346 */
} // :cond_3
/* new-array v4, v4, [Ljava/lang/String; */
(( java.util.ArrayList ) v0 ).toArray ( v4 ); // invoke-virtual {v0, v4}, Ljava/util/ArrayList;->toArray([Ljava/lang/Object;)[Ljava/lang/Object;
/* check-cast v4, [Ljava/lang/String; */
} // .end method
public Integer getCutoutMode ( java.lang.String p0 ) {
/* .locals 3 */
/* .param p1, "pkg" # Ljava/lang/String; */
/* .line 569 */
/* sget-boolean v0, Lmiui/util/CustomizeUtil;->HAS_NOTCH:Z */
/* if-nez v0, :cond_0 */
/* .line 570 */
int v0 = 0; // const/4 v0, 0x0
/* .line 572 */
} // :cond_0
android.os.Binder .clearCallingIdentity ( );
/* move-result-wide v0 */
/* .line 574 */
/* .local v0, "identity":J */
try { // :try_start_0
v2 = this.mCompatModePackages;
v2 = (( com.miui.server.MiuiCompatModePackages ) v2 ).getCutoutMode ( p1 ); // invoke-virtual {v2, p1}, Lcom/miui/server/MiuiCompatModePackages;->getCutoutMode(Ljava/lang/String;)I
/* :try_end_0 */
/* .catchall {:try_start_0 ..:try_end_0} :catchall_0 */
/* .line 576 */
android.os.Binder .restoreCallingIdentity ( v0,v1 );
/* .line 574 */
/* .line 576 */
/* :catchall_0 */
/* move-exception v2 */
android.os.Binder .restoreCallingIdentity ( v0,v1 );
/* .line 577 */
/* throw v2 */
} // .end method
public Integer getDefaultAspectType ( java.lang.String p0 ) {
/* .locals 1 */
/* .param p1, "pkg" # Ljava/lang/String; */
/* .line 534 */
/* iget-boolean v0, p0, Lcom/miui/server/MiuiInitServer;->mNeedAspectSettings:Z */
/* if-nez v0, :cond_0 */
/* .line 535 */
int v0 = 0; // const/4 v0, 0x0
/* .line 537 */
} // :cond_0
v0 = this.mCompatModePackages;
v0 = (( com.miui.server.MiuiCompatModePackages ) v0 ).getDefaultAspectType ( p1 ); // invoke-virtual {v0, p1}, Lcom/miui/server/MiuiCompatModePackages;->getDefaultAspectType(Ljava/lang/String;)I
} // .end method
public java.lang.String getMiuiChannelPath ( java.lang.String p0 ) {
/* .locals 4 */
/* .param p1, "pkg" # Ljava/lang/String; */
/* .line 391 */
v0 = android.text.TextUtils .isEmpty ( p1 );
final String v1 = ""; // const-string v1, ""
if ( v0 != null) { // if-eqz v0, :cond_0
/* .line 392 */
/* .line 394 */
} // :cond_0
v0 = (( com.miui.server.MiuiInitServer ) p0 ).isPreinstalledPackage ( p1 ); // invoke-virtual {p0, p1}, Lcom/miui/server/MiuiInitServer;->isPreinstalledPackage(Ljava/lang/String;)Z
/* if-nez v0, :cond_1 */
/* .line 395 */
/* .line 397 */
} // :cond_1
v0 = this.mPreinstalledChannels;
/* if-nez v0, :cond_3 */
/* .line 398 */
v0 = this.mMiuiPreinstallHelper;
v0 = (( com.android.server.pm.MiuiPreinstallHelper ) v0 ).isSupportNewFrame ( ); // invoke-virtual {v0}, Lcom/android/server/pm/MiuiPreinstallHelper;->isSupportNewFrame()Z
if ( v0 != null) { // if-eqz v0, :cond_2
/* .line 399 */
v0 = this.mMiuiPreinstallHelper;
/* .line 400 */
(( com.android.server.pm.MiuiPreinstallHelper ) v0 ).getBusinessPreinstallConfig ( ); // invoke-virtual {v0}, Lcom/android/server/pm/MiuiPreinstallHelper;->getBusinessPreinstallConfig()Lcom/android/server/pm/MiuiBusinessPreinstallConfig;
(( com.android.server.pm.MiuiBusinessPreinstallConfig ) v0 ).getPeinstalledChannelList ( ); // invoke-virtual {v0}, Lcom/android/server/pm/MiuiBusinessPreinstallConfig;->getPeinstalledChannelList()Ljava/util/ArrayList;
this.mPreinstalledChannels = v0;
/* .line 402 */
} // :cond_2
com.android.server.pm.PreinstallApp .getPeinstalledChannelList ( );
this.mPreinstalledChannels = v0;
/* .line 405 */
} // :cond_3
} // :goto_0
v0 = this.mPreinstalledChannels;
(( java.util.ArrayList ) v0 ).iterator ( ); // invoke-virtual {v0}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;
v2 = } // :goto_1
if ( v2 != null) { // if-eqz v2, :cond_5
/* check-cast v2, Ljava/lang/String; */
/* .line 406 */
/* .local v2, "channel":Ljava/lang/String; */
v3 = (( java.lang.String ) v2 ).contains ( p1 ); // invoke-virtual {v2, p1}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z
if ( v3 != null) { // if-eqz v3, :cond_4
/* new-instance v3, Ljava/io/File; */
/* invoke-direct {v3, v2}, Ljava/io/File;-><init>(Ljava/lang/String;)V */
v3 = (( java.io.File ) v3 ).exists ( ); // invoke-virtual {v3}, Ljava/io/File;->exists()Z
if ( v3 != null) { // if-eqz v3, :cond_4
/* .line 407 */
/* .line 409 */
} // .end local v2 # "channel":Ljava/lang/String;
} // :cond_4
/* .line 410 */
} // :cond_5
} // .end method
public java.lang.String getMiuiPreinstallAppPath ( java.lang.String p0 ) {
/* .locals 7 */
/* .param p1, "pkg" # Ljava/lang/String; */
/* .line 491 */
v0 = android.text.TextUtils .isEmpty ( p1 );
final String v1 = ""; // const-string v1, ""
if ( v0 != null) { // if-eqz v0, :cond_0
/* .line 492 */
/* .line 494 */
} // :cond_0
v0 = this.mPreinstallHistoryMap;
/* if-nez v0, :cond_2 */
/* .line 495 */
/* new-instance v0, Ljava/util/HashMap; */
/* invoke-direct {v0}, Ljava/util/HashMap;-><init>()V */
this.mPreinstallHistoryMap = v0;
/* .line 497 */
try { // :try_start_0
final String v0 = "removable_apk_list"; // const-string v0, "removable_apk_list"
miui.util.FeatureParser .getStringArray ( v0 );
/* .line 498 */
/* .local v0, "pkgNameList":[Ljava/lang/String; */
final String v2 = "removable_apk_path_list"; // const-string v2, "removable_apk_path_list"
miui.util.FeatureParser .getStringArray ( v2 );
/* .line 499 */
/* .local v2, "appPathList":[Ljava/lang/String; */
if ( v0 != null) { // if-eqz v0, :cond_1
if ( v2 != null) { // if-eqz v2, :cond_1
/* array-length v3, v0 */
/* array-length v4, v2 */
/* if-ne v3, v4, :cond_1 */
/* .line 500 */
int v3 = 0; // const/4 v3, 0x0
/* .local v3, "i":I */
} // :goto_0
/* array-length v4, v0 */
/* if-ge v3, v4, :cond_1 */
/* .line 501 */
v4 = this.mPreinstallHistoryMap;
/* aget-object v5, v0, v3 */
/* aget-object v6, v2, v3 */
(( java.util.HashMap ) v4 ).put ( v5, v6 ); // invoke-virtual {v4, v5, v6}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;
/* :try_end_0 */
/* .catch Ljava/lang/Exception; {:try_start_0 ..:try_end_0} :catch_0 */
/* .line 500 */
/* add-int/lit8 v3, v3, 0x1 */
/* .line 506 */
} // .end local v0 # "pkgNameList":[Ljava/lang/String;
} // .end local v2 # "appPathList":[Ljava/lang/String;
} // .end local v3 # "i":I
} // :cond_1
/* .line 504 */
/* :catch_0 */
/* move-exception v0 */
/* .line 505 */
/* .local v0, "e":Ljava/lang/Exception; */
/* new-instance v2, Ljava/lang/StringBuilder; */
/* invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V */
final String v3 = "Error occurs while get miui preinstall app path + "; // const-string v3, "Error occurs while get miui preinstall app path + "
(( java.lang.StringBuilder ) v2 ).append ( v3 ); // invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v2 ).append ( v0 ); // invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v2 ).toString ( ); // invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
final String v3 = "MiuiInitServer"; // const-string v3, "MiuiInitServer"
android.util.Log .i ( v3,v2 );
/* .line 508 */
} // .end local v0 # "e":Ljava/lang/Exception;
} // :cond_2
} // :goto_1
v0 = this.mPreinstallHistoryMap;
(( java.util.HashMap ) v0 ).get ( p1 ); // invoke-virtual {v0, p1}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;
/* if-nez v0, :cond_3 */
} // :cond_3
v0 = this.mPreinstallHistoryMap;
(( java.util.HashMap ) v0 ).get ( p1 ); // invoke-virtual {v0, p1}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;
/* move-object v1, v0 */
/* check-cast v1, Ljava/lang/String; */
} // :goto_2
} // .end method
public Integer getNotchConfig ( java.lang.String p0 ) {
/* .locals 3 */
/* .param p1, "pkg" # Ljava/lang/String; */
/* .line 541 */
/* sget-boolean v0, Lmiui/util/CustomizeUtil;->HAS_NOTCH:Z */
/* if-nez v0, :cond_0 */
/* .line 542 */
int v0 = 0; // const/4 v0, 0x0
/* .line 544 */
} // :cond_0
android.os.Binder .clearCallingIdentity ( );
/* move-result-wide v0 */
/* .line 546 */
/* .local v0, "identity":J */
try { // :try_start_0
v2 = this.mCompatModePackages;
v2 = (( com.miui.server.MiuiCompatModePackages ) v2 ).getNotchConfig ( p1 ); // invoke-virtual {v2, p1}, Lcom/miui/server/MiuiCompatModePackages;->getNotchConfig(Ljava/lang/String;)I
/* :try_end_0 */
/* .catchall {:try_start_0 ..:try_end_0} :catchall_0 */
/* .line 548 */
android.os.Binder .restoreCallingIdentity ( v0,v1 );
/* .line 546 */
/* .line 548 */
/* :catchall_0 */
/* move-exception v2 */
android.os.Binder .restoreCallingIdentity ( v0,v1 );
/* .line 549 */
/* throw v2 */
} // .end method
public android.os.Bundle getPackageEventRecords ( Integer p0 ) {
/* .locals 1 */
/* .param p1, "eventType" # I */
/* .line 602 */
/* const-class v0, Lcom/android/server/pm/PackageEventRecorderInternal; */
com.android.server.LocalServices .getService ( v0 );
/* check-cast v0, Lcom/android/server/pm/PackageEventRecorderInternal; */
/* .line 603 */
/* .line 602 */
} // .end method
public Integer getPreinstalledAppVersion ( java.lang.String p0 ) {
/* .locals 1 */
/* .param p1, "pkg" # Ljava/lang/String; */
/* .line 480 */
v0 = android.text.TextUtils .isEmpty ( p1 );
if ( v0 != null) { // if-eqz v0, :cond_0
/* .line 481 */
int v0 = -1; // const/4 v0, -0x1
/* .line 483 */
} // :cond_0
v0 = this.mMiuiPreinstallHelper;
v0 = (( com.android.server.pm.MiuiPreinstallHelper ) v0 ).isSupportNewFrame ( ); // invoke-virtual {v0}, Lcom/android/server/pm/MiuiPreinstallHelper;->isSupportNewFrame()Z
if ( v0 != null) { // if-eqz v0, :cond_1
/* .line 484 */
v0 = this.mMiuiPreinstallHelper;
v0 = (( com.android.server.pm.MiuiPreinstallHelper ) v0 ).getPreinstallAppVersion ( p1 ); // invoke-virtual {v0, p1}, Lcom/android/server/pm/MiuiPreinstallHelper;->getPreinstallAppVersion(Ljava/lang/String;)I
/* .line 486 */
} // :cond_1
v0 = com.android.server.pm.PreinstallApp .getPreinstalledAppVersion ( p1 );
} // .end method
public Boolean initCustEnvironment ( java.lang.String p0, miui.os.IMiuiInitObserver p1 ) {
/* .locals 4 */
/* .param p1, "custVariant" # Ljava/lang/String; */
/* .param p2, "obs" # Lmiui/os/IMiuiInitObserver; */
/* .annotation system Ldalvik/annotation/Throws; */
/* value = { */
/* Landroid/os/RemoteException; */
/* } */
} // .end annotation
/* .line 294 */
v0 = this.mContext;
final String v1 = "miui.os.permisson.INIT_MIUI_ENVIRONMENT"; // const-string v1, "miui.os.permisson.INIT_MIUI_ENVIRONMENT"
int v2 = 0; // const/4 v2, 0x0
(( android.content.Context ) v0 ).enforceCallingOrSelfPermission ( v1, v2 ); // invoke-virtual {v0, v1, v2}, Landroid/content/Context;->enforceCallingOrSelfPermission(Ljava/lang/String;Ljava/lang/String;)V
/* .line 296 */
final String v0 = "MiuiInitServer"; // const-string v0, "MiuiInitServer"
/* new-instance v1, Ljava/lang/StringBuilder; */
/* invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V */
final String v2 = "check status, cust variant["; // const-string v2, "check status, cust variant["
(( java.lang.StringBuilder ) v1 ).append ( v2 ); // invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v1 ).append ( p1 ); // invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
final String v2 = "]"; // const-string v2, "]"
(( java.lang.StringBuilder ) v1 ).append ( v2 ); // invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v1 ).toString ( ); // invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
android.util.Slog .i ( v0,v1 );
/* .line 297 */
/* monitor-enter p0 */
/* .line 298 */
try { // :try_start_0
/* iget-boolean v0, p0, Lcom/miui/server/MiuiInitServer;->mDoing:Z */
int v1 = 0; // const/4 v1, 0x0
if ( v0 != null) { // if-eqz v0, :cond_0
/* .line 299 */
final String v0 = "MiuiInitServer"; // const-string v0, "MiuiInitServer"
/* const-string/jumbo v2, "skip, initializing cust environment" */
android.util.Slog .w ( v0,v2 );
/* .line 300 */
/* monitor-exit p0 */
/* .line 302 */
} // :cond_0
v0 = android.text.TextUtils .isEmpty ( p1 );
if ( v0 != null) { // if-eqz v0, :cond_1
/* .line 303 */
final String v0 = "MiuiInitServer"; // const-string v0, "MiuiInitServer"
/* new-instance v2, Ljava/lang/StringBuilder; */
/* invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V */
/* const-string/jumbo v3, "skip, cust variant[" */
(( java.lang.StringBuilder ) v2 ).append ( v3 ); // invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v2 ).append ( p1 ); // invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
final String v3 = "] is empty"; // const-string v3, "] is empty"
(( java.lang.StringBuilder ) v2 ).append ( v3 ); // invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v2 ).toString ( ); // invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
android.util.Slog .w ( v0,v2 );
/* .line 304 */
/* monitor-exit p0 */
/* .line 306 */
} // :cond_1
int v0 = 1; // const/4 v0, 0x1
/* iput-boolean v0, p0, Lcom/miui/server/MiuiInitServer;->mDoing:Z */
/* .line 307 */
final String v1 = "MiuiInitServer"; // const-string v1, "MiuiInitServer"
final String v2 = "initializing cust environment"; // const-string v2, "initializing cust environment"
android.util.Slog .i ( v1,v2 );
/* .line 308 */
/* new-instance v1, Lcom/miui/server/MiuiInitServer$InitCustEnvironmentTask; */
/* invoke-direct {v1, p0, p1, p2}, Lcom/miui/server/MiuiInitServer$InitCustEnvironmentTask;-><init>(Lcom/miui/server/MiuiInitServer;Ljava/lang/String;Lmiui/os/IMiuiInitObserver;)V */
(( com.miui.server.MiuiInitServer$InitCustEnvironmentTask ) v1 ).start ( ); // invoke-virtual {v1}, Lcom/miui/server/MiuiInitServer$InitCustEnvironmentTask;->start()V
/* .line 309 */
/* monitor-exit p0 */
/* .line 310 */
/* :catchall_0 */
/* move-exception v0 */
/* monitor-exit p0 */
/* :try_end_0 */
/* .catchall {:try_start_0 ..:try_end_0} :catchall_0 */
/* throw v0 */
} // .end method
public void installPreinstallApp ( ) {
/* .locals 3 */
/* .line 316 */
v0 = this.mContext;
final String v1 = "miui.os.permisson.INIT_MIUI_ENVIRONMENT"; // const-string v1, "miui.os.permisson.INIT_MIUI_ENVIRONMENT"
int v2 = 0; // const/4 v2, 0x0
(( android.content.Context ) v0 ).enforceCallingOrSelfPermission ( v1, v2 ); // invoke-virtual {v0, v1, v2}, Landroid/content/Context;->enforceCallingOrSelfPermission(Ljava/lang/String;Ljava/lang/String;)V
/* .line 318 */
v0 = this.mMiuiPreinstallHelper;
v0 = (( com.android.server.pm.MiuiPreinstallHelper ) v0 ).isSupportNewFrame ( ); // invoke-virtual {v0}, Lcom/android/server/pm/MiuiPreinstallHelper;->isSupportNewFrame()Z
if ( v0 != null) { // if-eqz v0, :cond_0
/* .line 319 */
v0 = this.mMiuiPreinstallHelper;
(( com.android.server.pm.MiuiPreinstallHelper ) v0 ).installCustApps ( ); // invoke-virtual {v0}, Lcom/android/server/pm/MiuiPreinstallHelper;->installCustApps()V
/* .line 321 */
} // :cond_0
v0 = this.mContext;
com.android.server.pm.PreinstallApp .installCustApps ( v0 );
/* .line 323 */
} // :goto_0
return;
} // .end method
public Boolean isPreinstalledPAIPackage ( java.lang.String p0 ) {
/* .locals 1 */
/* .param p1, "pkg" # Ljava/lang/String; */
/* .line 380 */
v0 = android.text.TextUtils .isEmpty ( p1 );
if ( v0 != null) { // if-eqz v0, :cond_0
/* .line 381 */
int v0 = 0; // const/4 v0, 0x0
/* .line 383 */
} // :cond_0
v0 = this.mMiuiPreinstallHelper;
v0 = (( com.android.server.pm.MiuiPreinstallHelper ) v0 ).isSupportNewFrame ( ); // invoke-virtual {v0}, Lcom/android/server/pm/MiuiPreinstallHelper;->isSupportNewFrame()Z
if ( v0 != null) { // if-eqz v0, :cond_1
/* .line 384 */
v0 = com.android.server.pm.MiuiPAIPreinstallConfig .isPreinstalledPAIPackage ( p1 );
/* .line 386 */
} // :cond_1
v0 = com.android.server.pm.PreinstallApp .isPreinstalledPAIPackage ( p1 );
} // .end method
public Boolean isPreinstalledPackage ( java.lang.String p0 ) {
/* .locals 1 */
/* .param p1, "pkg" # Ljava/lang/String; */
/* .line 368 */
v0 = android.text.TextUtils .isEmpty ( p1 );
if ( v0 != null) { // if-eqz v0, :cond_0
/* .line 369 */
int v0 = 0; // const/4 v0, 0x0
/* .line 371 */
} // :cond_0
v0 = this.mMiuiPreinstallHelper;
v0 = (( com.android.server.pm.MiuiPreinstallHelper ) v0 ).isSupportNewFrame ( ); // invoke-virtual {v0}, Lcom/android/server/pm/MiuiPreinstallHelper;->isSupportNewFrame()Z
if ( v0 != null) { // if-eqz v0, :cond_1
/* .line 372 */
v0 = this.mMiuiPreinstallHelper;
v0 = (( com.android.server.pm.MiuiPreinstallHelper ) v0 ).isPreinstalledPackage ( p1 ); // invoke-virtual {v0, p1}, Lcom/android/server/pm/MiuiPreinstallHelper;->isPreinstalledPackage(Ljava/lang/String;)Z
/* .line 374 */
} // :cond_1
v0 = com.android.server.pm.PreinstallApp .isPreinstalledPackage ( p1 );
} // .end method
public Boolean isRestrictAspect ( java.lang.String p0 ) {
/* .locals 1 */
/* .param p1, "packageName" # Ljava/lang/String; */
/* .line 520 */
/* iget-boolean v0, p0, Lcom/miui/server/MiuiInitServer;->mNeedAspectSettings:Z */
/* if-nez v0, :cond_0 */
/* .line 521 */
int v0 = 1; // const/4 v0, 0x1
/* .line 523 */
} // :cond_0
v0 = this.mCompatModePackages;
v0 = (( com.miui.server.MiuiCompatModePackages ) v0 ).isRestrictAspect ( p1 ); // invoke-virtual {v0, p1}, Lcom/miui/server/MiuiCompatModePackages;->isRestrictAspect(Ljava/lang/String;)Z
} // .end method
public void removeFromPreinstallList ( java.lang.String p0 ) {
/* .locals 3 */
/* .param p1, "pkg" # Ljava/lang/String; */
/* .line 416 */
v0 = this.mContext;
final String v1 = "miui.os.permisson.INIT_MIUI_ENVIRONMENT"; // const-string v1, "miui.os.permisson.INIT_MIUI_ENVIRONMENT"
int v2 = 0; // const/4 v2, 0x0
(( android.content.Context ) v0 ).enforceCallingOrSelfPermission ( v1, v2 ); // invoke-virtual {v0, v1, v2}, Landroid/content/Context;->enforceCallingOrSelfPermission(Ljava/lang/String;Ljava/lang/String;)V
/* .line 419 */
v0 = android.text.TextUtils .isEmpty ( p1 );
if ( v0 != null) { // if-eqz v0, :cond_0
/* .line 420 */
return;
/* .line 423 */
} // :cond_0
com.android.server.pm.PreinstallApp .removeFromPreinstallList ( p1 );
/* .line 424 */
return;
} // .end method
public void removeFromPreinstallPAIList ( java.lang.String p0 ) {
/* .locals 3 */
/* .param p1, "pkg" # Ljava/lang/String; */
/* .line 429 */
v0 = this.mContext;
final String v1 = "miui.os.permisson.INIT_MIUI_ENVIRONMENT"; // const-string v1, "miui.os.permisson.INIT_MIUI_ENVIRONMENT"
int v2 = 0; // const/4 v2, 0x0
(( android.content.Context ) v0 ).enforceCallingOrSelfPermission ( v1, v2 ); // invoke-virtual {v0, v1, v2}, Landroid/content/Context;->enforceCallingOrSelfPermission(Ljava/lang/String;Ljava/lang/String;)V
/* .line 432 */
v0 = android.text.TextUtils .isEmpty ( p1 );
if ( v0 != null) { // if-eqz v0, :cond_0
/* .line 433 */
return;
/* .line 436 */
} // :cond_0
v0 = this.mMiuiPreinstallHelper;
v0 = (( com.android.server.pm.MiuiPreinstallHelper ) v0 ).isSupportNewFrame ( ); // invoke-virtual {v0}, Lcom/android/server/pm/MiuiPreinstallHelper;->isSupportNewFrame()Z
if ( v0 != null) { // if-eqz v0, :cond_1
/* .line 437 */
com.android.server.pm.MiuiPAIPreinstallConfig .removeFromPreinstallPAIList ( p1 );
/* .line 439 */
} // :cond_1
com.android.server.pm.PreinstallApp .removeFromPreinstallPAIList ( p1 );
/* .line 441 */
} // :goto_0
return;
} // .end method
public void setCutoutMode ( java.lang.String p0, Integer p1 ) {
/* .locals 1 */
/* .param p1, "pkg" # Ljava/lang/String; */
/* .param p2, "mode" # I */
/* .line 561 */
/* sget-boolean v0, Lmiui/util/CustomizeUtil;->HAS_NOTCH:Z */
/* if-nez v0, :cond_0 */
/* .line 562 */
return;
/* .line 564 */
} // :cond_0
/* const-string/jumbo v0, "setCutoutMode" */
/* invoke-direct {p0, v0}, Lcom/miui/server/MiuiInitServer;->checkPermission(Ljava/lang/String;)V */
/* .line 565 */
v0 = this.mCompatModePackages;
(( com.miui.server.MiuiCompatModePackages ) v0 ).setCutoutMode ( p1, p2 ); // invoke-virtual {v0, p1, p2}, Lcom/miui/server/MiuiCompatModePackages;->setCutoutMode(Ljava/lang/String;I)V
/* .line 566 */
return;
} // .end method
public void setNotchSpecialMode ( java.lang.String p0, Boolean p1 ) {
/* .locals 1 */
/* .param p1, "pkg" # Ljava/lang/String; */
/* .param p2, "special" # Z */
/* .line 553 */
/* sget-boolean v0, Lmiui/util/CustomizeUtil;->HAS_NOTCH:Z */
/* if-nez v0, :cond_0 */
/* .line 554 */
return;
/* .line 556 */
} // :cond_0
/* const-string/jumbo v0, "setNotchSpecialMode" */
/* invoke-direct {p0, v0}, Lcom/miui/server/MiuiInitServer;->checkPermission(Ljava/lang/String;)V */
/* .line 557 */
v0 = this.mCompatModePackages;
(( com.miui.server.MiuiCompatModePackages ) v0 ).setNotchSpecialMode ( p1, p2 ); // invoke-virtual {v0, p1, p2}, Lcom/miui/server/MiuiCompatModePackages;->setNotchSpecialMode(Ljava/lang/String;Z)V
/* .line 558 */
return;
} // .end method
public void setRestrictAspect ( java.lang.String p0, Boolean p1 ) {
/* .locals 1 */
/* .param p1, "pkg" # Ljava/lang/String; */
/* .param p2, "restrict" # Z */
/* .line 512 */
/* iget-boolean v0, p0, Lcom/miui/server/MiuiInitServer;->mNeedAspectSettings:Z */
/* if-nez v0, :cond_0 */
/* .line 513 */
return;
/* .line 515 */
} // :cond_0
/* const-string/jumbo v0, "setRestrictAspect" */
/* invoke-direct {p0, v0}, Lcom/miui/server/MiuiInitServer;->checkPermission(Ljava/lang/String;)V */
/* .line 516 */
v0 = this.mCompatModePackages;
(( com.miui.server.MiuiCompatModePackages ) v0 ).setRestrictAspect ( p1, p2 ); // invoke-virtual {v0, p1, p2}, Lcom/miui/server/MiuiCompatModePackages;->setRestrictAspect(Ljava/lang/String;Z)V
/* .line 517 */
return;
} // .end method
public void writePreinstallPAIPackage ( java.lang.String p0 ) {
/* .locals 3 */
/* .param p1, "pkg" # Ljava/lang/String; */
/* .annotation system Ldalvik/annotation/Throws; */
/* value = { */
/* Landroid/os/RemoteException; */
/* } */
} // .end annotation
/* .line 446 */
v0 = this.mContext;
final String v1 = "miui.os.permisson.INIT_MIUI_ENVIRONMENT"; // const-string v1, "miui.os.permisson.INIT_MIUI_ENVIRONMENT"
int v2 = 0; // const/4 v2, 0x0
(( android.content.Context ) v0 ).enforceCallingOrSelfPermission ( v1, v2 ); // invoke-virtual {v0, v1, v2}, Landroid/content/Context;->enforceCallingOrSelfPermission(Ljava/lang/String;Ljava/lang/String;)V
/* .line 449 */
v0 = android.text.TextUtils .isEmpty ( p1 );
if ( v0 != null) { // if-eqz v0, :cond_0
/* .line 450 */
return;
/* .line 453 */
} // :cond_0
v0 = this.mMiuiPreinstallHelper;
v0 = (( com.android.server.pm.MiuiPreinstallHelper ) v0 ).isSupportNewFrame ( ); // invoke-virtual {v0}, Lcom/android/server/pm/MiuiPreinstallHelper;->isSupportNewFrame()Z
if ( v0 != null) { // if-eqz v0, :cond_1
/* .line 454 */
com.android.server.pm.MiuiPAIPreinstallConfig .writePreinstallPAIPackage ( p1 );
/* .line 456 */
} // :cond_1
com.android.server.pm.PreinstallApp .writePreinstallPAIPackage ( p1 );
/* .line 458 */
} // :goto_0
return;
} // .end method
