public class com.miui.server.MiuiFboService extends miui.fbo.IFboManager$Stub implements com.miui.app.MiuiFboServiceInternal {
	 /* .source "MiuiFboService.java" */
	 /* # interfaces */
	 /* # annotations */
	 /* .annotation system Ldalvik/annotation/MemberClasses; */
	 /* value = { */
	 /* Lcom/miui/server/MiuiFboService$AlarmReceiver;, */
	 /* Lcom/miui/server/MiuiFboService$FboServiceHandler;, */
	 /* Lcom/miui/server/MiuiFboService$Lifecycle; */
	 /* } */
} // .end annotation
/* # static fields */
private static final Integer APP_GC_AND_DISCARD;
private static final java.lang.String APP_ID;
public static final Integer CHANGE_USB_STATUS;
private static final java.lang.String CONNECT_NATIVESERVICE_NAME;
public static final java.lang.String CONTINUE;
private static final java.lang.String ENABLED_FBO;
private static final Integer EUA_PECYCLE_FREEBLOCK;
private static final Integer EUA_UNMAP;
private static final java.lang.String FALSE;
private static final java.lang.String FBO_EVENT_NAME;
private static final java.lang.String FBO_START_COUNT;
private static final Integer FBO_STATECTL;
private static final Integer FLAG_NOT_LIMITED_BY_USER_EXPERIENCE_PLAN;
private static final java.lang.String HAL_DEFAULT;
private static final java.lang.String HAL_INTERFACE_DESCRIPTOR;
private static final java.lang.String HAL_SERVICE_NAME;
private static final java.lang.String HANDLER_NAME;
private static final Integer IS_FBO_SUPPORTED;
private static final java.lang.String MIUI_FBO_PROCESSED_DONE;
public static final java.lang.String MIUI_FBO_RECEIVER_START;
public static final java.lang.String MIUI_FBO_RECEIVER_STARTAGAIN;
public static final java.lang.String MIUI_FBO_RECEIVER_STOP;
public static final java.lang.String MIUI_FBO_RECEIVER_TRANSFERFBOTRIGGER;
private static final java.lang.String NATIVE_SERVICE_KEY;
private static final java.lang.String NATIVE_SOCKET_NAME;
private static final java.lang.String ONETRACK_PACKAGE_NAME;
private static final java.lang.String ONE_TRACK_ACTION;
private static final Integer OVERLOAD_FBO_SUPPORTED;
private static final java.lang.String PACKAGE;
public static final java.lang.String SERVICE_NAME;
public static final Integer START_FBO;
public static final Integer START_FBO_AGAIN;
public static final java.lang.String STOP;
public static final java.lang.String STOPDUETOBATTERYTEMPERATURE;
public static final java.lang.String STOPDUETOSCREEN;
public static final Integer STOP_DUETO_BATTERYTEMPERATURE;
public static final Integer STOP_DUETO_SCREEN;
public static final Integer STOP_FBO;
private static final java.lang.String TAG;
private static final Integer TRIGGER_CLD;
private static final java.lang.String TRUE;
private static miui.hardware.CldManager cldManager;
private static java.io.InputStream inputStream;
private static android.net.LocalSocket interactClientSocket;
private static Integer listSize;
private static android.app.AlarmManager mAlarmManager;
private static java.lang.String mCurrentPackageName;
private static Boolean mFinishedApp;
private static java.lang.Long mFragmentCount;
private static Boolean mKeepRunning;
private static final java.lang.Object mLock;
private static android.app.PendingIntent mPendingIntent;
private static android.net.LocalServerSocket mServerSocket;
private static java.util.ArrayList mStopReason;
/* .annotation system Ldalvik/annotation/Signature; */
/* value = { */
/* "Ljava/util/ArrayList<", */
/* "Ljava/lang/String;", */
/* ">;" */
/* } */
} // .end annotation
} // .end field
private static java.io.OutputStream outputStream;
private static java.util.ArrayList packageName;
/* .annotation system Ldalvik/annotation/Signature; */
/* value = { */
/* "Ljava/util/ArrayList<", */
/* "Ljava/lang/String;", */
/* ">;" */
/* } */
} // .end annotation
} // .end field
private static java.util.ArrayList packageNameList;
/* .annotation system Ldalvik/annotation/Signature; */
/* value = { */
/* "Ljava/util/ArrayList<", */
/* "Ljava/lang/String;", */
/* ">;" */
/* } */
} // .end annotation
} // .end field
private static java.util.ArrayList pendingIntentList;
/* .annotation system Ldalvik/annotation/Signature; */
/* value = { */
/* "Ljava/util/ArrayList<", */
/* "Landroid/app/PendingIntent;", */
/* ">;" */
/* } */
} // .end annotation
} // .end field
private static volatile com.miui.server.MiuiFboService sInstance;
/* # instance fields */
private Integer batteryLevel;
private Integer batteryStatus;
private Integer batteryStatusMark;
private Integer batteryTemperature;
private volatile Boolean cldStrategyStatus;
private Boolean dueToScreenWait;
private Boolean enableNightJudgment;
private Boolean globalSwitch;
private android.content.Context mContext;
private Integer mCurrentBatteryLevel;
private Boolean mDriverSupport;
private volatile miui.fbo.IFbo mFboNativeService;
private volatile com.miui.server.MiuiFboService$FboServiceHandler mFboServiceHandler;
private android.os.HandlerThread mFboServiceThread;
private java.lang.String mVdexPath;
private Boolean messageHasbeenSent;
private Boolean nativeIsRunning;
private Boolean screenOn;
private Integer screenOnTimes;
private java.lang.String stagingData;
private Boolean usbState;
/* # direct methods */
static Integer -$$Nest$fgetbatteryLevel ( com.miui.server.MiuiFboService p0 ) { //bridge//synthethic
/* .locals 0 */
/* iget p0, p0, Lcom/miui/server/MiuiFboService;->batteryLevel:I */
} // .end method
static Integer -$$Nest$fgetbatteryStatus ( com.miui.server.MiuiFboService p0 ) { //bridge//synthethic
/* .locals 0 */
/* iget p0, p0, Lcom/miui/server/MiuiFboService;->batteryStatus:I */
} // .end method
static Integer -$$Nest$fgetbatteryTemperature ( com.miui.server.MiuiFboService p0 ) { //bridge//synthethic
/* .locals 0 */
/* iget p0, p0, Lcom/miui/server/MiuiFboService;->batteryTemperature:I */
} // .end method
static android.content.Context -$$Nest$fgetmContext ( com.miui.server.MiuiFboService p0 ) { //bridge//synthethic
/* .locals 0 */
p0 = this.mContext;
} // .end method
static Boolean -$$Nest$fgetmDriverSupport ( com.miui.server.MiuiFboService p0 ) { //bridge//synthethic
/* .locals 0 */
/* iget-boolean p0, p0, Lcom/miui/server/MiuiFboService;->mDriverSupport:Z */
} // .end method
static miui.fbo.IFbo -$$Nest$fgetmFboNativeService ( com.miui.server.MiuiFboService p0 ) { //bridge//synthethic
/* .locals 0 */
p0 = this.mFboNativeService;
} // .end method
static com.miui.server.MiuiFboService$FboServiceHandler -$$Nest$fgetmFboServiceHandler ( com.miui.server.MiuiFboService p0 ) { //bridge//synthethic
/* .locals 0 */
p0 = this.mFboServiceHandler;
} // .end method
static java.lang.String -$$Nest$fgetmVdexPath ( com.miui.server.MiuiFboService p0 ) { //bridge//synthethic
/* .locals 0 */
p0 = this.mVdexPath;
} // .end method
static Boolean -$$Nest$fgetscreenOn ( com.miui.server.MiuiFboService p0 ) { //bridge//synthethic
/* .locals 0 */
/* iget-boolean p0, p0, Lcom/miui/server/MiuiFboService;->screenOn:Z */
} // .end method
static Integer -$$Nest$fgetscreenOnTimes ( com.miui.server.MiuiFboService p0 ) { //bridge//synthethic
/* .locals 0 */
/* iget p0, p0, Lcom/miui/server/MiuiFboService;->screenOnTimes:I */
} // .end method
static java.lang.String -$$Nest$fgetstagingData ( com.miui.server.MiuiFboService p0 ) { //bridge//synthethic
/* .locals 0 */
p0 = this.stagingData;
} // .end method
static void -$$Nest$fputcldStrategyStatus ( com.miui.server.MiuiFboService p0, Boolean p1 ) { //bridge//synthethic
/* .locals 0 */
/* iput-boolean p1, p0, Lcom/miui/server/MiuiFboService;->cldStrategyStatus:Z */
return;
} // .end method
static void -$$Nest$fputmCurrentBatteryLevel ( com.miui.server.MiuiFboService p0, Integer p1 ) { //bridge//synthethic
/* .locals 0 */
/* iput p1, p0, Lcom/miui/server/MiuiFboService;->mCurrentBatteryLevel:I */
return;
} // .end method
static void -$$Nest$fputmFboNativeService ( com.miui.server.MiuiFboService p0, miui.fbo.IFbo p1 ) { //bridge//synthethic
/* .locals 0 */
this.mFboNativeService = p1;
return;
} // .end method
static void -$$Nest$fputmVdexPath ( com.miui.server.MiuiFboService p0, java.lang.String p1 ) { //bridge//synthethic
/* .locals 0 */
this.mVdexPath = p1;
return;
} // .end method
static void -$$Nest$fputmessageHasbeenSent ( com.miui.server.MiuiFboService p0, Boolean p1 ) { //bridge//synthethic
/* .locals 0 */
/* iput-boolean p1, p0, Lcom/miui/server/MiuiFboService;->messageHasbeenSent:Z */
return;
} // .end method
static void -$$Nest$fputscreenOnTimes ( com.miui.server.MiuiFboService p0, Integer p1 ) { //bridge//synthethic
/* .locals 0 */
/* iput p1, p0, Lcom/miui/server/MiuiFboService;->screenOnTimes:I */
return;
} // .end method
static void -$$Nest$fputstagingData ( com.miui.server.MiuiFboService p0, java.lang.String p1 ) { //bridge//synthethic
/* .locals 0 */
this.stagingData = p1;
return;
} // .end method
static com.miui.server.MiuiFboService -$$Nest$mforSystemServerInitialization ( com.miui.server.MiuiFboService p0, android.content.Context p1 ) { //bridge//synthethic
/* .locals 0 */
/* invoke-direct {p0, p1}, Lcom/miui/server/MiuiFboService;->forSystemServerInitialization(Landroid/content/Context;)Lcom/miui/server/MiuiFboService; */
} // .end method
static Boolean -$$Nest$misWithinTheTimeInterval ( com.miui.server.MiuiFboService p0 ) { //bridge//synthethic
/* .locals 0 */
p0 = /* invoke-direct {p0}, Lcom/miui/server/MiuiFboService;->isWithinTheTimeInterval()Z */
} // .end method
static void -$$Nest$mreportFboEvent ( com.miui.server.MiuiFboService p0 ) { //bridge//synthethic
/* .locals 0 */
/* invoke-direct {p0}, Lcom/miui/server/MiuiFboService;->reportFboEvent()V */
return;
} // .end method
static void -$$Nest$msendStopOrContinueToHal ( com.miui.server.MiuiFboService p0, java.lang.String p1 ) { //bridge//synthethic
/* .locals 0 */
/* invoke-direct {p0, p1}, Lcom/miui/server/MiuiFboService;->sendStopOrContinueToHal(Ljava/lang/String;)V */
return;
} // .end method
static void -$$Nest$muserAreaExtend ( com.miui.server.MiuiFboService p0 ) { //bridge//synthethic
/* .locals 0 */
/* invoke-direct {p0}, Lcom/miui/server/MiuiFboService;->userAreaExtend()V */
return;
} // .end method
static java.lang.String -$$Nest$sfgetTAG ( ) { //bridge//synthethic
/* .locals 1 */
v0 = com.miui.server.MiuiFboService.TAG;
} // .end method
static java.io.InputStream -$$Nest$sfgetinputStream ( ) { //bridge//synthethic
/* .locals 1 */
v0 = com.miui.server.MiuiFboService.inputStream;
} // .end method
static android.net.LocalSocket -$$Nest$sfgetinteractClientSocket ( ) { //bridge//synthethic
/* .locals 1 */
v0 = com.miui.server.MiuiFboService.interactClientSocket;
} // .end method
static Integer -$$Nest$sfgetlistSize ( ) { //bridge//synthethic
/* .locals 1 */
} // .end method
static java.lang.String -$$Nest$sfgetmCurrentPackageName ( ) { //bridge//synthethic
/* .locals 1 */
v0 = com.miui.server.MiuiFboService.mCurrentPackageName;
} // .end method
static Boolean -$$Nest$sfgetmFinishedApp ( ) { //bridge//synthethic
/* .locals 1 */
/* sget-boolean v0, Lcom/miui/server/MiuiFboService;->mFinishedApp:Z */
} // .end method
static java.lang.Long -$$Nest$sfgetmFragmentCount ( ) { //bridge//synthethic
/* .locals 1 */
v0 = com.miui.server.MiuiFboService.mFragmentCount;
} // .end method
static Boolean -$$Nest$sfgetmKeepRunning ( ) { //bridge//synthethic
/* .locals 1 */
/* sget-boolean v0, Lcom/miui/server/MiuiFboService;->mKeepRunning:Z */
} // .end method
static android.net.LocalServerSocket -$$Nest$sfgetmServerSocket ( ) { //bridge//synthethic
/* .locals 1 */
v0 = com.miui.server.MiuiFboService.mServerSocket;
} // .end method
static java.util.ArrayList -$$Nest$sfgetmStopReason ( ) { //bridge//synthethic
/* .locals 1 */
v0 = com.miui.server.MiuiFboService.mStopReason;
} // .end method
static java.io.OutputStream -$$Nest$sfgetoutputStream ( ) { //bridge//synthethic
/* .locals 1 */
v0 = com.miui.server.MiuiFboService.outputStream;
} // .end method
static java.util.ArrayList -$$Nest$sfgetpackageNameList ( ) { //bridge//synthethic
/* .locals 1 */
v0 = com.miui.server.MiuiFboService.packageNameList;
} // .end method
static com.miui.server.MiuiFboService -$$Nest$sfgetsInstance ( ) { //bridge//synthethic
/* .locals 1 */
v0 = com.miui.server.MiuiFboService.sInstance;
} // .end method
static void -$$Nest$sfputinputStream ( java.io.InputStream p0 ) { //bridge//synthethic
/* .locals 0 */
return;
} // .end method
static void -$$Nest$sfputinteractClientSocket ( android.net.LocalSocket p0 ) { //bridge//synthethic
/* .locals 0 */
return;
} // .end method
static void -$$Nest$sfputlistSize ( Integer p0 ) { //bridge//synthethic
/* .locals 0 */
return;
} // .end method
static void -$$Nest$sfputmCurrentPackageName ( java.lang.String p0 ) { //bridge//synthethic
/* .locals 0 */
return;
} // .end method
static void -$$Nest$sfputmFinishedApp ( Boolean p0 ) { //bridge//synthethic
/* .locals 0 */
com.miui.server.MiuiFboService.mFinishedApp = (p0!= 0);
return;
} // .end method
static void -$$Nest$sfputmFragmentCount ( java.lang.Long p0 ) { //bridge//synthethic
/* .locals 0 */
return;
} // .end method
static void -$$Nest$sfputmKeepRunning ( Boolean p0 ) { //bridge//synthethic
/* .locals 0 */
com.miui.server.MiuiFboService.mKeepRunning = (p0!= 0);
return;
} // .end method
static void -$$Nest$sfputmServerSocket ( android.net.LocalServerSocket p0 ) { //bridge//synthethic
/* .locals 0 */
return;
} // .end method
static void -$$Nest$sfputoutputStream ( java.io.OutputStream p0 ) { //bridge//synthethic
/* .locals 0 */
return;
} // .end method
static void -$$Nest$smaggregateBroadcastData ( java.lang.String p0 ) { //bridge//synthethic
/* .locals 0 */
com.miui.server.MiuiFboService .aggregateBroadcastData ( p0 );
return;
} // .end method
static java.lang.Object -$$Nest$smcallHalFunction ( java.lang.String p0, Integer p1 ) { //bridge//synthethic
/* .locals 0 */
com.miui.server.MiuiFboService .callHalFunction ( p0,p1 );
} // .end method
static void -$$Nest$smclearBroadcastData ( ) { //bridge//synthethic
/* .locals 0 */
com.miui.server.MiuiFboService .clearBroadcastData ( );
return;
} // .end method
static void -$$Nest$smsetAlarm ( java.lang.String p0, java.lang.String p1, Long p2, Boolean p3 ) { //bridge//synthethic
/* .locals 0 */
com.miui.server.MiuiFboService .setAlarm ( p0,p1,p2,p3,p4 );
return;
} // .end method
static void -$$Nest$smuseCldStrategy ( Integer p0 ) { //bridge//synthethic
/* .locals 0 */
com.miui.server.MiuiFboService .useCldStrategy ( p0 );
return;
} // .end method
static void -$$Nest$smwriteFailToHal ( ) { //bridge//synthethic
/* .locals 0 */
com.miui.server.MiuiFboService .writeFailToHal ( );
return;
} // .end method
static com.miui.server.MiuiFboService ( ) {
/* .locals 2 */
/* .line 56 */
/* const-class v0, Lcom/miui/server/MiuiFboService; */
(( java.lang.Class ) v0 ).getSimpleName ( ); // invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;
/* .line 58 */
int v0 = 0; // const/4 v0, 0x0
/* .line 77 */
int v1 = 1; // const/4 v1, 0x1
com.miui.server.MiuiFboService.mKeepRunning = (v1!= 0);
/* .line 78 */
int v1 = 0; // const/4 v1, 0x0
com.miui.server.MiuiFboService.mFinishedApp = (v1!= 0);
/* .line 81 */
/* .line 82 */
/* .line 83 */
/* new-instance v0, Ljava/util/ArrayList; */
/* invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V */
/* .line 85 */
/* .line 88 */
/* new-instance v0, Ljava/util/ArrayList; */
/* invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V */
/* .line 89 */
/* new-instance v0, Ljava/lang/Object; */
/* invoke-direct {v0}, Ljava/lang/Object;-><init>()V */
/* .line 137 */
/* const-wide/16 v0, 0x0 */
java.lang.Long .valueOf ( v0,v1 );
/* .line 138 */
/* new-instance v0, Ljava/util/ArrayList; */
/* invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V */
/* .line 139 */
/* new-instance v0, Ljava/util/ArrayList; */
/* invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V */
return;
} // .end method
private com.miui.server.MiuiFboService ( ) {
/* .locals 2 */
/* .line 378 */
/* invoke-direct {p0}, Lmiui/fbo/IFboManager$Stub;-><init>()V */
/* .line 63 */
int v0 = 0; // const/4 v0, 0x0
/* iput v0, p0, Lcom/miui/server/MiuiFboService;->batteryStatusMark:I */
/* .line 65 */
/* iput-boolean v0, p0, Lcom/miui/server/MiuiFboService;->messageHasbeenSent:Z */
/* .line 66 */
/* iput-boolean v0, p0, Lcom/miui/server/MiuiFboService;->nativeIsRunning:Z */
/* .line 67 */
/* iput-boolean v0, p0, Lcom/miui/server/MiuiFboService;->globalSwitch:Z */
/* .line 68 */
/* iput-boolean v0, p0, Lcom/miui/server/MiuiFboService;->dueToScreenWait:Z */
/* .line 69 */
/* iput v0, p0, Lcom/miui/server/MiuiFboService;->screenOnTimes:I */
/* .line 71 */
int v1 = 1; // const/4 v1, 0x1
/* iput-boolean v1, p0, Lcom/miui/server/MiuiFboService;->enableNightJudgment:Z */
/* .line 72 */
/* iput-boolean v1, p0, Lcom/miui/server/MiuiFboService;->usbState:Z */
/* .line 73 */
/* iput-boolean v0, p0, Lcom/miui/server/MiuiFboService;->mDriverSupport:Z */
/* .line 74 */
/* iput v0, p0, Lcom/miui/server/MiuiFboService;->mCurrentBatteryLevel:I */
/* .line 379 */
com.miui.server.MiuiFboService .initFboSocket ( );
/* .line 380 */
/* new-instance v0, Landroid/os/HandlerThread; */
final String v1 = "fboServiceWork"; // const-string v1, "fboServiceWork"
/* invoke-direct {v0, v1}, Landroid/os/HandlerThread;-><init>(Ljava/lang/String;)V */
this.mFboServiceThread = v0;
/* .line 381 */
(( android.os.HandlerThread ) v0 ).start ( ); // invoke-virtual {v0}, Landroid/os/HandlerThread;->start()V
/* .line 382 */
/* new-instance v0, Lcom/miui/server/MiuiFboService$FboServiceHandler; */
v1 = this.mFboServiceThread;
(( android.os.HandlerThread ) v1 ).getLooper ( ); // invoke-virtual {v1}, Landroid/os/HandlerThread;->getLooper()Landroid/os/Looper;
/* invoke-direct {v0, p0, v1}, Lcom/miui/server/MiuiFboService$FboServiceHandler;-><init>(Lcom/miui/server/MiuiFboService;Landroid/os/Looper;)V */
this.mFboServiceHandler = v0;
/* .line 383 */
/* const-class v0, Lcom/miui/app/MiuiFboServiceInternal; */
com.android.server.LocalServices .addService ( v0,p0 );
/* .line 384 */
return;
} // .end method
private static void aggregateBroadcastData ( java.lang.String p0 ) {
/* .locals 10 */
/* .param p0, "dataReceived" # Ljava/lang/String; */
/* .line 448 */
try { // :try_start_0
v0 = com.miui.server.MiuiFboService.mLock;
/* monitor-enter v0 */
/* :try_end_0 */
/* .catch Ljava/lang/Exception; {:try_start_0 ..:try_end_0} :catch_0 */
/* .line 449 */
try { // :try_start_1
final String v1 = ":"; // const-string v1, ":"
(( java.lang.String ) p0 ).split ( v1 ); // invoke-virtual {p0, v1}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;
/* .line 450 */
/* .local v1, "splitDataReceived":[Ljava/lang/String; */
v2 = com.miui.server.MiuiFboService.packageName;
int v3 = 1; // const/4 v3, 0x1
/* aget-object v3, v1, v3 */
(( java.util.ArrayList ) v2 ).add ( v3 ); // invoke-virtual {v2, v3}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z
/* .line 451 */
int v2 = 3; // const/4 v2, 0x3
/* aget-object v2, v1, v2 */
java.lang.Long .parseLong ( v2 );
/* move-result-wide v2 */
java.lang.Long .valueOf ( v2,v3 );
/* .line 452 */
/* .local v2, "beforeCleaning":Ljava/lang/Long; */
int v3 = 4; // const/4 v3, 0x4
/* aget-object v3, v1, v3 */
java.lang.Long .parseLong ( v3 );
/* move-result-wide v3 */
java.lang.Long .valueOf ( v3,v4 );
/* .line 453 */
/* .local v3, "afterCleaning":Ljava/lang/Long; */
v4 = com.miui.server.MiuiFboService.mFragmentCount;
(( java.lang.Long ) v4 ).longValue ( ); // invoke-virtual {v4}, Ljava/lang/Long;->longValue()J
/* move-result-wide v4 */
(( java.lang.Long ) v2 ).longValue ( ); // invoke-virtual {v2}, Ljava/lang/Long;->longValue()J
/* move-result-wide v6 */
(( java.lang.Long ) v3 ).longValue ( ); // invoke-virtual {v3}, Ljava/lang/Long;->longValue()J
/* move-result-wide v8 */
/* sub-long/2addr v6, v8 */
/* add-long/2addr v4, v6 */
java.lang.Long .valueOf ( v4,v5 );
/* .line 454 */
v4 = com.miui.server.MiuiFboService.sInstance;
/* invoke-direct {v4}, Lcom/miui/server/MiuiFboService;->reportFboProcessedBroadcast()V */
/* .line 455 */
v4 = com.miui.server.MiuiFboService.outputStream;
/* const-string/jumbo v5, "{broadcast send success}" */
(( java.lang.String ) v5 ).getBytes ( ); // invoke-virtual {v5}, Ljava/lang/String;->getBytes()[B
(( java.io.OutputStream ) v4 ).write ( v5 ); // invoke-virtual {v4, v5}, Ljava/io/OutputStream;->write([B)V
/* .line 456 */
} // .end local v1 # "splitDataReceived":[Ljava/lang/String;
} // .end local v2 # "beforeCleaning":Ljava/lang/Long;
} // .end local v3 # "afterCleaning":Ljava/lang/Long;
/* monitor-exit v0 */
/* .line 460 */
/* .line 456 */
/* :catchall_0 */
/* move-exception v1 */
/* monitor-exit v0 */
/* :try_end_1 */
/* .catchall {:try_start_1 ..:try_end_1} :catchall_0 */
} // .end local p0 # "dataReceived":Ljava/lang/String;
try { // :try_start_2
/* throw v1 */
/* :try_end_2 */
/* .catch Ljava/lang/Exception; {:try_start_2 ..:try_end_2} :catch_0 */
/* .line 457 */
/* .restart local p0 # "dataReceived":Ljava/lang/String; */
/* :catch_0 */
/* move-exception v0 */
/* .line 458 */
/* .local v0, "e":Ljava/lang/Exception; */
(( java.lang.Exception ) v0 ).printStackTrace ( ); // invoke-virtual {v0}, Ljava/lang/Exception;->printStackTrace()V
/* .line 459 */
com.miui.server.MiuiFboService .writeFailToNative ( );
/* .line 461 */
} // .end local v0 # "e":Ljava/lang/Exception;
} // :goto_0
return;
} // .end method
private static java.lang.Object callHalFunction ( java.lang.String p0, Integer p1 ) {
/* .locals 6 */
/* .param p0, "writeData" # Ljava/lang/String; */
/* .param p1, "halFunctionName" # I */
/* .line 656 */
/* const-string/jumbo v0, "vendor.xiaomi.hardware.fbo@1.0::IFbo" */
/* new-instance v1, Landroid/os/HwParcel; */
/* invoke-direct {v1}, Landroid/os/HwParcel;-><init>()V */
/* .line 658 */
/* .local v1, "hidl_reply":Landroid/os/HwParcel; */
int v2 = 0; // const/4 v2, 0x0
try { // :try_start_0
final String v3 = "default"; // const-string v3, "default"
android.os.HwBinder .getService ( v0,v3 );
/* .line 659 */
/* .local v3, "hwService":Landroid/os/IHwBinder; */
if ( v3 != null) { // if-eqz v3, :cond_1
/* .line 660 */
/* new-instance v4, Landroid/os/HwParcel; */
/* invoke-direct {v4}, Landroid/os/HwParcel;-><init>()V */
/* .line 661 */
/* .local v4, "hidl_request":Landroid/os/HwParcel; */
(( android.os.HwParcel ) v4 ).writeInterfaceToken ( v0 ); // invoke-virtual {v4, v0}, Landroid/os/HwParcel;->writeInterfaceToken(Ljava/lang/String;)V
/* .line 662 */
if ( p0 != null) { // if-eqz p0, :cond_0
/* .line 663 */
(( android.os.HwParcel ) v4 ).writeString ( p0 ); // invoke-virtual {v4, p0}, Landroid/os/HwParcel;->writeString(Ljava/lang/String;)V
/* .line 665 */
} // :cond_0
int v0 = 0; // const/4 v0, 0x0
/* .line 666 */
(( android.os.HwParcel ) v1 ).verifySuccess ( ); // invoke-virtual {v1}, Landroid/os/HwParcel;->verifySuccess()V
/* .line 667 */
(( android.os.HwParcel ) v4 ).releaseTemporaryStorage ( ); // invoke-virtual {v4}, Landroid/os/HwParcel;->releaseTemporaryStorage()V
/* .line 668 */
/* packed-switch p1, :pswitch_data_0 */
/* :pswitch_0 */
/* .line 676 */
/* :pswitch_1 */
(( android.os.HwParcel ) v1 ).readString ( ); // invoke-virtual {v1}, Landroid/os/HwParcel;->readString()Ljava/lang/String;
/* :try_end_0 */
/* .catch Ljava/lang/Exception; {:try_start_0 ..:try_end_0} :catch_0 */
/* .catchall {:try_start_0 ..:try_end_0} :catchall_0 */
/* .line 684 */
(( android.os.HwParcel ) v1 ).release ( ); // invoke-virtual {v1}, Landroid/os/HwParcel;->release()V
/* .line 676 */
/* .line 672 */
/* :pswitch_2 */
try { // :try_start_1
v0 = (( android.os.HwParcel ) v1 ).readBool ( ); // invoke-virtual {v1}, Landroid/os/HwParcel;->readBool()Z
java.lang.Boolean .valueOf ( v0 );
/* :try_end_1 */
/* .catch Ljava/lang/Exception; {:try_start_1 ..:try_end_1} :catch_0 */
/* .catchall {:try_start_1 ..:try_end_1} :catchall_0 */
/* .line 684 */
(( android.os.HwParcel ) v1 ).release ( ); // invoke-virtual {v1}, Landroid/os/HwParcel;->release()V
/* .line 672 */
/* .line 679 */
} // .end local v4 # "hidl_request":Landroid/os/HwParcel;
} // :cond_1
} // :goto_0
/* nop */
/* .line 684 */
(( android.os.HwParcel ) v1 ).release ( ); // invoke-virtual {v1}, Landroid/os/HwParcel;->release()V
/* .line 679 */
/* .line 684 */
} // .end local v3 # "hwService":Landroid/os/IHwBinder;
/* :catchall_0 */
/* move-exception v0 */
/* .line 680 */
/* :catch_0 */
/* move-exception v0 */
/* .line 681 */
/* .local v0, "e":Ljava/lang/Exception; */
try { // :try_start_2
v3 = com.miui.server.MiuiFboService.TAG;
/* new-instance v4, Ljava/lang/StringBuilder; */
/* invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V */
final String v5 = "crash in the callHalFunction:"; // const-string v5, "crash in the callHalFunction:"
(( java.lang.StringBuilder ) v4 ).append ( v5 ); // invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v4 ).append ( v0 ); // invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v4 ).toString ( ); // invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
android.util.Slog .e ( v3,v4 );
/* :try_end_2 */
/* .catchall {:try_start_2 ..:try_end_2} :catchall_0 */
/* .line 682 */
/* nop */
/* .line 684 */
(( android.os.HwParcel ) v1 ).release ( ); // invoke-virtual {v1}, Landroid/os/HwParcel;->release()V
/* .line 682 */
/* .line 684 */
} // .end local v0 # "e":Ljava/lang/Exception;
} // :goto_1
(( android.os.HwParcel ) v1 ).release ( ); // invoke-virtual {v1}, Landroid/os/HwParcel;->release()V
/* .line 685 */
/* throw v0 */
/* nop */
/* :pswitch_data_0 */
/* .packed-switch 0x1 */
/* :pswitch_2 */
/* :pswitch_1 */
/* :pswitch_0 */
/* :pswitch_0 */
/* :pswitch_2 */
/* :pswitch_1 */
} // .end packed-switch
} // .end method
private static void clearBroadcastData ( ) {
/* .locals 2 */
/* .line 783 */
/* const-wide/16 v0, 0x0 */
java.lang.Long .valueOf ( v0,v1 );
/* .line 784 */
v0 = com.miui.server.MiuiFboService.packageName;
(( java.util.ArrayList ) v0 ).clear ( ); // invoke-virtual {v0}, Ljava/util/ArrayList;->clear()V
/* .line 785 */
return;
} // .end method
private com.miui.server.MiuiFboService forSystemServerInitialization ( android.content.Context p0 ) {
/* .locals 4 */
/* .param p1, "context" # Landroid/content/Context; */
/* .line 338 */
final String v0 = "persist.sys.fboservice.ctrl"; // const-string v0, "persist.sys.fboservice.ctrl"
final String v1 = "false"; // const-string v1, "false"
android.os.SystemProperties .set ( v0,v1 );
/* .line 339 */
this.mContext = p1;
/* .line 340 */
miui.hardware.CldManager .getInstance ( p1 );
/* .line 341 */
v0 = this.mContext;
final String v1 = "alarm"; // const-string v1, "alarm"
(( android.content.Context ) v0 ).getSystemService ( v1 ); // invoke-virtual {v0, v1}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;
/* check-cast v0, Landroid/app/AlarmManager; */
/* .line 342 */
/* new-instance v0, Landroid/content/IntentFilter; */
/* invoke-direct {v0}, Landroid/content/IntentFilter;-><init>()V */
/* .line 343 */
/* .local v0, "intentFilter":Landroid/content/IntentFilter; */
/* new-instance v1, Lcom/miui/server/MiuiFboService$AlarmReceiver; */
/* invoke-direct {v1}, Lcom/miui/server/MiuiFboService$AlarmReceiver;-><init>()V */
/* .line 344 */
/* .local v1, "alarmReceiver":Lcom/miui/server/MiuiFboService$AlarmReceiver; */
final String v2 = "miui.intent.action.start"; // const-string v2, "miui.intent.action.start"
(( android.content.IntentFilter ) v0 ).addAction ( v2 ); // invoke-virtual {v0, v2}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V
/* .line 345 */
final String v2 = "miui.intent.action.startAgain"; // const-string v2, "miui.intent.action.startAgain"
(( android.content.IntentFilter ) v0 ).addAction ( v2 ); // invoke-virtual {v0, v2}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V
/* .line 346 */
final String v2 = "miui.intent.action.transferFboTrigger"; // const-string v2, "miui.intent.action.transferFboTrigger"
(( android.content.IntentFilter ) v0 ).addAction ( v2 ); // invoke-virtual {v0, v2}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V
/* .line 347 */
final String v2 = "miui.intent.action.stop"; // const-string v2, "miui.intent.action.stop"
(( android.content.IntentFilter ) v0 ).addAction ( v2 ); // invoke-virtual {v0, v2}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V
/* .line 348 */
v2 = this.mContext;
(( android.content.Context ) v2 ).registerReceiver ( v1, v0 ); // invoke-virtual {v2, v1, v0}, Landroid/content/Context;->registerReceiver(Landroid/content/BroadcastReceiver;Landroid/content/IntentFilter;)Landroid/content/Intent;
/* .line 349 */
final String v2 = "persist.sys.stability.miui_fbo_start_count"; // const-string v2, "persist.sys.stability.miui_fbo_start_count"
final String v3 = "0"; // const-string v3, "0"
android.os.SystemProperties .set ( v2,v3 );
/* .line 350 */
com.miui.server.MiuiFboService .getInstance ( );
} // .end method
private static void formatAppList ( java.lang.String p0 ) {
/* .locals 5 */
/* .param p0, "appList" # Ljava/lang/String; */
/* .line 605 */
v0 = com.miui.server.MiuiFboService.packageNameList;
(( java.util.ArrayList ) v0 ).clear ( ); // invoke-virtual {v0}, Ljava/util/ArrayList;->clear()V
/* .line 606 */
final String v0 = ","; // const-string v0, ","
(( java.lang.String ) p0 ).split ( v0 ); // invoke-virtual {p0, v0}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;
/* .line 607 */
/* .local v0, "split":[Ljava/lang/String; */
int v1 = 0; // const/4 v1, 0x0
/* .local v1, "i":I */
} // :goto_0
/* array-length v2, v0 */
/* if-ge v1, v2, :cond_0 */
/* .line 608 */
/* aget-object v2, v0, v1 */
final String v3 = "\""; // const-string v3, "\""
(( java.lang.String ) v2 ).split ( v3 ); // invoke-virtual {v2, v3}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;
/* .line 609 */
/* .local v2, "splitFinal":[Ljava/lang/String; */
v3 = com.miui.server.MiuiFboService.packageNameList;
int v4 = 1; // const/4 v4, 0x1
/* aget-object v4, v2, v4 */
(( java.util.ArrayList ) v3 ).add ( v4 ); // invoke-virtual {v3, v4}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z
/* .line 607 */
} // .end local v2 # "splitFinal":[Ljava/lang/String;
/* add-int/lit8 v1, v1, 0x1 */
/* .line 611 */
} // .end local v1 # "i":I
} // :cond_0
return;
} // .end method
public static com.miui.server.MiuiFboService getInstance ( ) {
/* .locals 2 */
/* .line 354 */
v0 = com.miui.server.MiuiFboService.sInstance;
/* if-nez v0, :cond_1 */
/* .line 355 */
/* const-class v0, Lcom/miui/server/MiuiFboService; */
/* monitor-enter v0 */
/* .line 356 */
try { // :try_start_0
v1 = com.miui.server.MiuiFboService.sInstance;
/* if-nez v1, :cond_0 */
/* .line 357 */
/* new-instance v1, Lcom/miui/server/MiuiFboService; */
/* invoke-direct {v1}, Lcom/miui/server/MiuiFboService;-><init>()V */
/* .line 359 */
} // :cond_0
/* monitor-exit v0 */
/* :catchall_0 */
/* move-exception v1 */
/* monitor-exit v0 */
/* :try_end_0 */
/* .catchall {:try_start_0 ..:try_end_0} :catchall_0 */
/* throw v1 */
/* .line 361 */
} // :cond_1
} // :goto_0
v0 = com.miui.server.MiuiFboService.sInstance;
} // .end method
private miui.fbo.IFbo getNativeService ( ) {
/* .locals 4 */
/* .line 627 */
/* const-string/jumbo v0, "true" */
final String v1 = "persist.sys.fboservice.ctrl"; // const-string v1, "persist.sys.fboservice.ctrl"
android.os.SystemProperties .set ( v1,v0 );
/* .line 628 */
int v0 = 0; // const/4 v0, 0x0
/* .line 630 */
/* .local v0, "binder":Landroid/os/IBinder; */
/* const-wide/16 v2, 0x3e8 */
try { // :try_start_0
java.lang.Thread .sleep ( v2,v3 );
/* .line 631 */
final String v2 = "FboNativeService"; // const-string v2, "FboNativeService"
android.os.ServiceManager .getService ( v2 );
/* move-object v0, v2 */
/* .line 632 */
if ( v0 != null) { // if-eqz v0, :cond_0
/* .line 633 */
/* new-instance v2, Lcom/miui/server/MiuiFboService$2; */
/* invoke-direct {v2, p0}, Lcom/miui/server/MiuiFboService$2;-><init>(Lcom/miui/server/MiuiFboService;)V */
int v3 = 0; // const/4 v3, 0x0
/* :try_end_0 */
/* .catch Ljava/lang/Exception; {:try_start_0 ..:try_end_0} :catch_0 */
/* .line 644 */
} // :cond_0
/* .line 641 */
/* :catch_0 */
/* move-exception v2 */
/* .line 642 */
/* .local v2, "e":Ljava/lang/Exception; */
int v0 = 0; // const/4 v0, 0x0
/* .line 643 */
final String v3 = "false"; // const-string v3, "false"
android.os.SystemProperties .set ( v1,v3 );
/* .line 645 */
} // .end local v2 # "e":Ljava/lang/Exception;
} // :goto_0
if ( v0 != null) { // if-eqz v0, :cond_1
/* .line 646 */
miui.fbo.IFbo$Stub .asInterface ( v0 );
this.mFboNativeService = v1;
/* .line 647 */
v1 = this.mFboNativeService;
/* .line 649 */
} // :cond_1
int v1 = 0; // const/4 v1, 0x0
this.mFboNativeService = v1;
/* .line 650 */
v2 = com.miui.server.MiuiFboService.TAG;
final String v3 = "IFbo not found; trying again"; // const-string v3, "IFbo not found; trying again"
android.util.Slog .w ( v2,v3 );
/* .line 652 */
} // .end method
private static void initFboSocket ( ) {
/* .locals 3 */
/* .line 387 */
/* new-instance v0, Ljava/lang/Thread; */
/* new-instance v1, Lcom/miui/server/MiuiFboService$1; */
/* invoke-direct {v1}, Lcom/miui/server/MiuiFboService$1;-><init>()V */
final String v2 = "fboSocketThread"; // const-string v2, "fboSocketThread"
/* invoke-direct {v0, v1, v2}, Ljava/lang/Thread;-><init>(Ljava/lang/Runnable;Ljava/lang/String;)V */
/* .line 443 */
/* .local v0, "thread":Ljava/lang/Thread; */
(( java.lang.Thread ) v0 ).start ( ); // invoke-virtual {v0}, Ljava/lang/Thread;->start()V
/* .line 444 */
return;
} // .end method
private Boolean isWithinTheTimeInterval ( ) {
/* .locals 10 */
/* .line 731 */
v0 = (( com.miui.server.MiuiFboService ) p0 ).getEnableNightJudgment ( ); // invoke-virtual {p0}, Lcom/miui/server/MiuiFboService;->getEnableNightJudgment()Z
int v1 = 1; // const/4 v1, 0x1
/* if-nez v0, :cond_0 */
/* .line 732 */
/* .line 735 */
} // :cond_0
java.util.Calendar .getInstance ( );
/* .line 736 */
/* .local v0, "calendar":Ljava/util/Calendar; */
/* const/16 v2, 0xb */
v2 = (( java.util.Calendar ) v0 ).get ( v2 ); // invoke-virtual {v0, v2}, Ljava/util/Calendar;->get(I)I
/* .line 737 */
/* .local v2, "hour":I */
/* const/16 v3, 0xc */
v3 = (( java.util.Calendar ) v0 ).get ( v3 ); // invoke-virtual {v0, v3}, Ljava/util/Calendar;->get(I)I
/* .line 738 */
/* .local v3, "minute":I */
/* mul-int/lit8 v4, v2, 0x3c */
/* add-int/2addr v4, v3 */
/* .line 740 */
/* .local v4, "minuteOfDay":I */
int v5 = 0; // const/4 v5, 0x0
/* .line 741 */
/* .local v5, "start":I */
/* const/16 v6, 0x12c */
/* .line 743 */
/* .local v6, "end":I */
/* if-lt v4, v5, :cond_1 */
/* if-gt v4, v6, :cond_1 */
/* .line 744 */
v7 = com.miui.server.MiuiFboService.TAG;
/* new-instance v8, Ljava/lang/StringBuilder; */
/* invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V */
/* const-string/jumbo v9, "until five in the morning :" */
(( java.lang.StringBuilder ) v8 ).append ( v9 ); // invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
/* sub-int v9, v6, v4 */
(( java.lang.StringBuilder ) v8 ).append ( v9 ); // invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v8 ).toString ( ); // invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
android.util.Slog .d ( v7,v8 );
/* .line 745 */
/* .line 747 */
} // :cond_1
int v1 = 0; // const/4 v1, 0x0
} // .end method
private static void removePendingIntentList ( ) {
/* .locals 3 */
/* .line 721 */
v0 = com.miui.server.MiuiFboService.pendingIntentList;
v0 = (( java.util.ArrayList ) v0 ).size ( ); // invoke-virtual {v0}, Ljava/util/ArrayList;->size()I
/* if-lez v0, :cond_1 */
/* .line 722 */
v0 = com.miui.server.MiuiFboService.pendingIntentList;
v0 = (( java.util.ArrayList ) v0 ).size ( ); // invoke-virtual {v0}, Ljava/util/ArrayList;->size()I
/* add-int/lit8 v0, v0, -0x1 */
/* .local v0, "i":I */
} // :goto_0
/* if-ltz v0, :cond_0 */
/* .line 723 */
v1 = com.miui.server.MiuiFboService.pendingIntentList;
(( java.util.ArrayList ) v1 ).remove ( v0 ); // invoke-virtual {v1, v0}, Ljava/util/ArrayList;->remove(I)Ljava/lang/Object;
/* check-cast v1, Landroid/app/PendingIntent; */
/* .line 724 */
/* .local v1, "PendingIntent":Landroid/app/PendingIntent; */
v2 = com.miui.server.MiuiFboService.mAlarmManager;
(( android.app.AlarmManager ) v2 ).cancel ( v1 ); // invoke-virtual {v2, v1}, Landroid/app/AlarmManager;->cancel(Landroid/app/PendingIntent;)V
/* .line 722 */
} // .end local v1 # "PendingIntent":Landroid/app/PendingIntent;
/* add-int/lit8 v0, v0, -0x1 */
/* .line 726 */
} // .end local v0 # "i":I
} // :cond_0
v0 = com.miui.server.MiuiFboService.TAG;
final String v1 = "removePendingIntentList"; // const-string v1, "removePendingIntentList"
android.util.Slog .d ( v0,v1 );
/* .line 728 */
} // :cond_1
return;
} // .end method
private void reportFboEvent ( ) {
/* .locals 6 */
/* .line 485 */
final String v0 = "persist.sys.stability.miui_fbo_start_count"; // const-string v0, "persist.sys.stability.miui_fbo_start_count"
try { // :try_start_0
/* new-instance v1, Landroid/content/Intent; */
final String v2 = "onetrack.action.TRACK_EVENT"; // const-string v2, "onetrack.action.TRACK_EVENT"
/* invoke-direct {v1, v2}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V */
/* .line 486 */
/* .local v1, "intent":Landroid/content/Intent; */
final String v2 = "com.miui.analytics"; // const-string v2, "com.miui.analytics"
(( android.content.Intent ) v1 ).setPackage ( v2 ); // invoke-virtual {v1, v2}, Landroid/content/Intent;->setPackage(Ljava/lang/String;)Landroid/content/Intent;
/* .line 487 */
final String v2 = "APP_ID"; // const-string v2, "APP_ID"
final String v3 = "31000000454"; // const-string v3, "31000000454"
(( android.content.Intent ) v1 ).putExtra ( v2, v3 ); // invoke-virtual {v1, v2, v3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;
/* .line 488 */
final String v2 = "PACKAGE"; // const-string v2, "PACKAGE"
final String v3 = "android"; // const-string v3, "android"
(( android.content.Intent ) v1 ).putExtra ( v2, v3 ); // invoke-virtual {v1, v2, v3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;
/* .line 489 */
final String v2 = "EVENT_NAME"; // const-string v2, "EVENT_NAME"
final String v3 = "fbo_event"; // const-string v3, "fbo_event"
(( android.content.Intent ) v1 ).putExtra ( v2, v3 ); // invoke-virtual {v1, v2, v3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;
/* .line 490 */
int v2 = 0; // const/4 v2, 0x0
v2 = android.os.SystemProperties .getInt ( v0,v2 );
int v3 = 1; // const/4 v3, 0x1
/* add-int/2addr v2, v3 */
/* .line 491 */
/* .local v2, "startCount":I */
/* const-string/jumbo v4, "start_count" */
java.lang.String .valueOf ( v2 );
(( android.content.Intent ) v1 ).putExtra ( v4, v5 ); // invoke-virtual {v1, v4, v5}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;
/* .line 492 */
java.lang.String .valueOf ( v2 );
android.os.SystemProperties .set ( v0,v4 );
/* .line 493 */
/* sget-boolean v0, Lcom/miui/server/MiuiFboService;->mFinishedApp:Z */
/* :try_end_0 */
/* .catch Ljava/lang/Exception; {:try_start_0 ..:try_end_0} :catch_0 */
/* .catchall {:try_start_0 ..:try_end_0} :catchall_0 */
final String v4 = "fragment_count"; // const-string v4, "fragment_count"
/* if-nez v0, :cond_0 */
/* .line 494 */
try { // :try_start_1
final String v0 = "Defragmentation of any app has not been completed"; // const-string v0, "Defragmentation of any app has not been completed"
(( android.content.Intent ) v1 ).putExtra ( v4, v0 ); // invoke-virtual {v1, v4, v0}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;
/* .line 496 */
} // :cond_0
v0 = com.miui.server.MiuiFboService.mFragmentCount;
java.lang.String .valueOf ( v0 );
(( android.content.Intent ) v1 ).putExtra ( v4, v0 ); // invoke-virtual {v1, v4, v0}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;
/* .line 498 */
} // :goto_0
/* const-string/jumbo v0, "stop_reason" */
v4 = com.miui.server.MiuiFboService.mStopReason;
java.lang.String .valueOf ( v4 );
(( android.content.Intent ) v1 ).putExtra ( v0, v4 ); // invoke-virtual {v1, v0, v4}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;
/* .line 499 */
(( android.content.Intent ) v1 ).setFlags ( v3 ); // invoke-virtual {v1, v3}, Landroid/content/Intent;->setFlags(I)Landroid/content/Intent;
/* .line 500 */
v0 = this.mContext;
v3 = android.os.UserHandle.CURRENT;
(( android.content.Context ) v0 ).startServiceAsUser ( v1, v3 ); // invoke-virtual {v0, v1, v3}, Landroid/content/Context;->startServiceAsUser(Landroid/content/Intent;Landroid/os/UserHandle;)Landroid/content/ComponentName;
/* .line 501 */
v0 = com.miui.server.MiuiFboService.TAG;
/* new-instance v3, Ljava/lang/StringBuilder; */
/* invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V */
final String v4 = "fbo event report data startCount:"; // const-string v4, "fbo event report data startCount:"
(( java.lang.StringBuilder ) v3 ).append ( v4 ); // invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v3 ).append ( v2 ); // invoke-virtual {v3, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;
final String v4 = ",fragment_count:"; // const-string v4, ",fragment_count:"
(( java.lang.StringBuilder ) v3 ).append ( v4 ); // invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
v4 = com.miui.server.MiuiFboService.mFragmentCount;
(( java.lang.StringBuilder ) v3 ).append ( v4 ); // invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;
final String v4 = ",stopReason:"; // const-string v4, ",stopReason:"
(( java.lang.StringBuilder ) v3 ).append ( v4 ); // invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
v4 = com.miui.server.MiuiFboService.mStopReason;
(( java.lang.StringBuilder ) v3 ).append ( v4 ); // invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v3 ).toString ( ); // invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
android.util.Slog .d ( v0,v3 );
/* :try_end_1 */
/* .catch Ljava/lang/Exception; {:try_start_1 ..:try_end_1} :catch_0 */
/* .catchall {:try_start_1 ..:try_end_1} :catchall_0 */
/* .line 506 */
/* nop */
} // .end local v1 # "intent":Landroid/content/Intent;
} // .end local v2 # "startCount":I
/* :catchall_0 */
/* move-exception v0 */
/* .line 503 */
/* :catch_0 */
/* move-exception v0 */
/* .line 504 */
/* .local v0, "e":Ljava/lang/Exception; */
try { // :try_start_2
v1 = com.miui.server.MiuiFboService.TAG;
final String v2 = "Upload onetrack exception!"; // const-string v2, "Upload onetrack exception!"
android.util.Slog .e ( v1,v2,v0 );
/* :try_end_2 */
/* .catchall {:try_start_2 ..:try_end_2} :catchall_0 */
/* .line 506 */
/* nop */
} // .end local v0 # "e":Ljava/lang/Exception;
} // :goto_1
v0 = com.miui.server.MiuiFboService.mStopReason;
(( java.util.ArrayList ) v0 ).clear ( ); // invoke-virtual {v0}, Ljava/util/ArrayList;->clear()V
/* .line 507 */
/* nop */
/* .line 508 */
return;
/* .line 506 */
} // :goto_2
v1 = com.miui.server.MiuiFboService.mStopReason;
(( java.util.ArrayList ) v1 ).clear ( ); // invoke-virtual {v1}, Ljava/util/ArrayList;->clear()V
/* .line 507 */
/* throw v0 */
} // .end method
private void reportFboProcessedBroadcast ( ) {
/* .locals 5 */
/* .line 511 */
/* new-instance v0, Landroid/content/Intent; */
final String v1 = "miui.intent.action.FBO_PROCESSED_DONE"; // const-string v1, "miui.intent.action.FBO_PROCESSED_DONE"
/* invoke-direct {v0, v1}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V */
/* .line 512 */
/* .local v0, "intent":Landroid/content/Intent; */
final String v1 = "resultNumber"; // const-string v1, "resultNumber"
v2 = com.miui.server.MiuiFboService.mFragmentCount;
(( android.content.Intent ) v0 ).putExtra ( v1, v2 ); // invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/io/Serializable;)Landroid/content/Intent;
/* .line 513 */
v1 = com.miui.server.MiuiFboService.packageName;
final String v2 = "resultPkg"; // const-string v2, "resultPkg"
(( android.content.Intent ) v0 ).putStringArrayListExtra ( v2, v1 ); // invoke-virtual {v0, v2, v1}, Landroid/content/Intent;->putStringArrayListExtra(Ljava/lang/String;Ljava/util/ArrayList;)Landroid/content/Intent;
/* .line 514 */
v1 = this.mContext;
(( android.content.Context ) v1 ).sendBroadcast ( v0 ); // invoke-virtual {v1, v0}, Landroid/content/Context;->sendBroadcast(Landroid/content/Intent;)V
/* .line 515 */
v1 = com.miui.server.MiuiFboService.TAG;
/* new-instance v3, Ljava/lang/StringBuilder; */
/* invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V */
/* const-string/jumbo v4, "sendBroadcast and resultNumber:" */
(( java.lang.StringBuilder ) v3 ).append ( v4 ); // invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
v4 = com.miui.server.MiuiFboService.mFragmentCount;
(( java.lang.StringBuilder ) v3 ).append ( v4 ); // invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v3 ).append ( v2 ); // invoke-virtual {v3, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
v3 = com.miui.server.MiuiFboService.packageName;
(( java.lang.StringBuilder ) v2 ).append ( v3 ); // invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v2 ).toString ( ); // invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
android.util.Slog .d ( v1,v2 );
/* .line 517 */
return;
} // .end method
private void sendStopMessage ( ) {
/* .locals 9 */
/* .line 751 */
java.util.Calendar .getInstance ( );
/* .line 752 */
/* .local v0, "calendar":Ljava/util/Calendar; */
/* const/16 v1, 0xb */
v1 = (( java.util.Calendar ) v0 ).get ( v1 ); // invoke-virtual {v0, v1}, Ljava/util/Calendar;->get(I)I
/* .line 753 */
/* .local v1, "hour":I */
/* const/16 v2, 0xc */
v2 = (( java.util.Calendar ) v0 ).get ( v2 ); // invoke-virtual {v0, v2}, Ljava/util/Calendar;->get(I)I
/* .line 755 */
/* .local v2, "minute":I */
/* mul-int/lit8 v3, v1, 0x3c */
/* add-int/2addr v3, v2 */
/* rsub-int v3, v3, 0x12c */
/* .line 756 */
/* .local v3, "stopTime":I */
/* mul-int/lit8 v4, v3, 0x3c */
/* int-to-long v4, v4 */
/* const-wide/16 v6, 0x3e8 */
/* mul-long/2addr v4, v6 */
int v6 = 0; // const/4 v6, 0x0
final String v7 = "miui.intent.action.stop"; // const-string v7, "miui.intent.action.stop"
int v8 = 0; // const/4 v8, 0x0
com.miui.server.MiuiFboService .setAlarm ( v7,v8,v4,v5,v6 );
/* .line 757 */
return;
} // .end method
private void sendStopOrContinueToHal ( java.lang.String p0 ) {
/* .locals 5 */
/* .param p1, "cmd" # Ljava/lang/String; */
/* .line 760 */
/* iget-boolean v0, p0, Lcom/miui/server/MiuiFboService;->mDriverSupport:Z */
/* if-nez v0, :cond_0 */
/* .line 761 */
return;
/* .line 763 */
} // :cond_0
/* new-instance v0, Landroid/os/HwParcel; */
/* invoke-direct {v0}, Landroid/os/HwParcel;-><init>()V */
/* .line 765 */
/* .local v0, "hidl_reply":Landroid/os/HwParcel; */
int v1 = 3; // const/4 v1, 0x3
try { // :try_start_0
com.miui.server.MiuiFboService .callHalFunction ( p1,v1 );
/* .line 766 */
v1 = com.miui.server.MiuiFboService.TAG;
/* new-instance v2, Ljava/lang/StringBuilder; */
/* invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V */
/* const-string/jumbo v3, "sendStopOrContinueToHal:" */
(( java.lang.StringBuilder ) v2 ).append ( v3 ); // invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v2 ).append ( p1 ); // invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v2 ).toString ( ); // invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
android.util.Slog .d ( v1,v2 );
/* :try_end_0 */
/* .catch Ljava/lang/Exception; {:try_start_0 ..:try_end_0} :catch_0 */
/* .line 769 */
/* .line 767 */
/* :catch_0 */
/* move-exception v1 */
/* .line 768 */
/* .local v1, "e":Ljava/lang/Exception; */
v2 = com.miui.server.MiuiFboService.TAG;
/* new-instance v3, Ljava/lang/StringBuilder; */
/* invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V */
final String v4 = "crash in the sendStopOrContinueToHal:"; // const-string v4, "crash in the sendStopOrContinueToHal:"
(( java.lang.StringBuilder ) v3 ).append ( v4 ); // invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v3 ).append ( v1 ); // invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v3 ).toString ( ); // invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
android.util.Slog .e ( v2,v3 );
/* .line 770 */
} // .end local v1 # "e":Ljava/lang/Exception;
} // :goto_0
return;
} // .end method
private static void setAlarm ( java.lang.String p0, java.lang.String p1, Long p2, Boolean p3 ) {
/* .locals 6 */
/* .param p0, "action" # Ljava/lang/String; */
/* .param p1, "appList" # Ljava/lang/String; */
/* .param p2, "delayTime" # J */
/* .param p4, "record" # Z */
/* .line 614 */
/* new-instance v0, Landroid/content/Intent; */
/* invoke-direct {v0}, Landroid/content/Intent;-><init>()V */
/* .line 615 */
/* .local v0, "intent":Landroid/content/Intent; */
(( android.content.Intent ) v0 ).setAction ( p0 ); // invoke-virtual {v0, p0}, Landroid/content/Intent;->setAction(Ljava/lang/String;)Landroid/content/Intent;
/* .line 616 */
if ( p1 != null) { // if-eqz p1, :cond_0
/* .line 617 */
final String v1 = "appList"; // const-string v1, "appList"
(( android.content.Intent ) v0 ).putExtra ( v1, p1 ); // invoke-virtual {v0, v1, p1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;
/* .line 619 */
} // :cond_0
v1 = com.miui.server.MiuiFboService.sInstance;
v1 = this.mContext;
int v2 = 0; // const/4 v2, 0x0
/* const/high16 v3, 0x4000000 */
android.app.PendingIntent .getBroadcast ( v1,v2,v0,v3 );
/* .line 620 */
if ( p4 != null) { // if-eqz p4, :cond_1
/* .line 621 */
v2 = com.miui.server.MiuiFboService.pendingIntentList;
(( java.util.ArrayList ) v2 ).add ( v1 ); // invoke-virtual {v2, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z
/* .line 623 */
} // :cond_1
v1 = com.miui.server.MiuiFboService.mAlarmManager;
android.os.SystemClock .elapsedRealtime ( );
/* move-result-wide v2 */
/* add-long/2addr v2, p2 */
v4 = com.miui.server.MiuiFboService.mPendingIntent;
int v5 = 2; // const/4 v5, 0x2
(( android.app.AlarmManager ) v1 ).setExactAndAllowWhileIdle ( v5, v2, v3, v4 ); // invoke-virtual {v1, v5, v2, v3, v4}, Landroid/app/AlarmManager;->setExactAndAllowWhileIdle(IJLandroid/app/PendingIntent;)V
/* .line 624 */
return;
} // .end method
private static void suspendTransferFboTrigger ( java.lang.String p0, Boolean p1, Boolean p2 ) {
/* .locals 9 */
/* .param p0, "appList" # Ljava/lang/String; */
/* .param p1, "cancel" # Z */
/* .param p2, "record" # Z */
/* .line 707 */
java.util.Calendar .getInstance ( );
/* .line 708 */
/* .local v0, "calendar":Ljava/util/Calendar; */
/* const/16 v1, 0xb */
v1 = (( java.util.Calendar ) v0 ).get ( v1 ); // invoke-virtual {v0, v1}, Ljava/util/Calendar;->get(I)I
/* .line 709 */
/* .local v1, "hour":I */
/* const/16 v2, 0xc */
v2 = (( java.util.Calendar ) v0 ).get ( v2 ); // invoke-virtual {v0, v2}, Ljava/util/Calendar;->get(I)I
/* .line 710 */
/* .local v2, "minute":I */
/* mul-int/lit8 v3, v1, 0x3c */
/* add-int/2addr v3, v2 */
/* .line 711 */
/* .local v3, "minuteOfDay":I */
/* rsub-int v4, v3, 0x5a0 */
/* .line 712 */
/* .local v4, "startTime":I */
if ( p1 != null) { // if-eqz p1, :cond_0
v5 = com.miui.server.MiuiFboService.mPendingIntent;
if ( v5 != null) { // if-eqz v5, :cond_0
/* .line 713 */
v6 = com.miui.server.MiuiFboService.mAlarmManager;
(( android.app.AlarmManager ) v6 ).cancel ( v5 ); // invoke-virtual {v6, v5}, Landroid/app/AlarmManager;->cancel(Landroid/app/PendingIntent;)V
/* .line 715 */
} // :cond_0
com.miui.server.MiuiFboService .removePendingIntentList ( );
/* .line 716 */
/* mul-int/lit8 v5, v4, 0x3c */
/* int-to-long v5, v5 */
/* const-wide/16 v7, 0x3e8 */
/* mul-long/2addr v5, v7 */
final String v7 = "miui.intent.action.transferFboTrigger"; // const-string v7, "miui.intent.action.transferFboTrigger"
com.miui.server.MiuiFboService .setAlarm ( v7,p0,v5,v6,p2 );
/* .line 717 */
v5 = com.miui.server.MiuiFboService.TAG;
/* new-instance v6, Ljava/lang/StringBuilder; */
/* invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V */
/* const-string/jumbo v7, "suspendTransferFboTrigger and suspendTime:" */
(( java.lang.StringBuilder ) v6 ).append ( v7 ); // invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v6 ).append ( v4 ); // invoke-virtual {v6, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v6 ).toString ( ); // invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
android.util.Slog .d ( v5,v6 );
/* .line 718 */
return;
} // .end method
private static void useCldStrategy ( Integer p0 ) {
/* .locals 4 */
/* .param p0, "val" # I */
/* .line 774 */
try { // :try_start_0
v0 = com.miui.server.MiuiFboService.cldManager;
v0 = (( miui.hardware.CldManager ) v0 ).isCldSupported ( ); // invoke-virtual {v0}, Lmiui/hardware/CldManager;->isCldSupported()Z
if ( v0 != null) { // if-eqz v0, :cond_0
v0 = com.miui.server.MiuiFboService.sInstance;
/* iget-boolean v0, v0, Lcom/miui/server/MiuiFboService;->cldStrategyStatus:Z */
if ( v0 != null) { // if-eqz v0, :cond_0
/* .line 775 */
v0 = com.miui.server.MiuiFboService.cldManager;
(( miui.hardware.CldManager ) v0 ).triggerCld ( p0 ); // invoke-virtual {v0, p0}, Lmiui/hardware/CldManager;->triggerCld(I)V
/* :try_end_0 */
/* .catch Ljava/lang/Exception; {:try_start_0 ..:try_end_0} :catch_0 */
/* .line 779 */
} // :cond_0
/* .line 777 */
/* :catch_0 */
/* move-exception v0 */
/* .line 778 */
/* .local v0, "e":Ljava/lang/Exception; */
v1 = com.miui.server.MiuiFboService.TAG;
/* new-instance v2, Ljava/lang/StringBuilder; */
/* invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V */
final String v3 = "crash in the useCldStrategy:"; // const-string v3, "crash in the useCldStrategy:"
(( java.lang.StringBuilder ) v2 ).append ( v3 ); // invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v2 ).append ( v0 ); // invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v2 ).toString ( ); // invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
android.util.Slog .e ( v1,v2 );
/* .line 780 */
} // .end local v0 # "e":Ljava/lang/Exception;
} // :goto_0
return;
} // .end method
private void userAreaExtend ( ) {
/* .locals 7 */
/* .line 690 */
int v0 = 0; // const/4 v0, 0x0
int v1 = 6; // const/4 v1, 0x6
try { // :try_start_0
com.miui.server.MiuiFboService .callHalFunction ( v0,v1 );
/* check-cast v0, Ljava/lang/String; */
/* .line 691 */
/* .local v0, "euaInfo":Ljava/lang/String; */
if ( v0 != null) { // if-eqz v0, :cond_1
/* .line 692 */
final String v1 = ":"; // const-string v1, ":"
(( java.lang.String ) v0 ).split ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;
/* .line 693 */
/* .local v1, "strArray":[Ljava/lang/String; */
int v2 = 0; // const/4 v2, 0x0
/* aget-object v2, v1, v2 */
v2 = java.lang.Integer .parseInt ( v2 );
/* .line 694 */
/* .local v2, "ufsPecycle":I */
int v3 = 1; // const/4 v3, 0x1
/* aget-object v4, v1, v3 */
v4 = java.lang.Integer .parseInt ( v4 );
/* .line 695 */
/* .local v4, "supportEua":I */
v5 = this.mFboNativeService;
/* .line 696 */
/* if-ne v4, v3, :cond_0 */
/* .line 697 */
v3 = this.mFboNativeService;
/* .line 699 */
} // :cond_0
v3 = com.miui.server.MiuiFboService.TAG;
/* new-instance v5, Ljava/lang/StringBuilder; */
/* invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V */
/* const-string/jumbo v6, "ufsPecycle:" */
(( java.lang.StringBuilder ) v5 ).append ( v6 ); // invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v5 ).append ( v2 ); // invoke-virtual {v5, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;
final String v6 = ",supportEua:"; // const-string v6, ",supportEua:"
(( java.lang.StringBuilder ) v5 ).append ( v6 ); // invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v5 ).append ( v4 ); // invoke-virtual {v5, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v5 ).toString ( ); // invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
android.util.Slog .d ( v3,v5 );
/* :try_end_0 */
/* .catch Ljava/lang/Exception; {:try_start_0 ..:try_end_0} :catch_0 */
/* .line 703 */
} // .end local v0 # "euaInfo":Ljava/lang/String;
} // .end local v1 # "strArray":[Ljava/lang/String;
} // .end local v2 # "ufsPecycle":I
} // .end local v4 # "supportEua":I
} // :cond_1
/* .line 701 */
/* :catch_0 */
/* move-exception v0 */
/* .line 702 */
/* .local v0, "e":Ljava/lang/Exception; */
v1 = com.miui.server.MiuiFboService.TAG;
/* new-instance v2, Ljava/lang/StringBuilder; */
/* invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V */
final String v3 = "crash in the userAreaExtend:"; // const-string v3, "crash in the userAreaExtend:"
(( java.lang.StringBuilder ) v2 ).append ( v3 ); // invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v2 ).append ( v0 ); // invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v2 ).toString ( ); // invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
android.util.Slog .e ( v1,v2 );
/* .line 704 */
} // .end local v0 # "e":Ljava/lang/Exception;
} // :goto_0
return;
} // .end method
private static void writeFailToHal ( ) {
/* .locals 2 */
/* .line 465 */
try { // :try_start_0
v0 = com.miui.server.MiuiFboService.outputStream;
if ( v0 != null) { // if-eqz v0, :cond_0
/* .line 466 */
/* const-string/jumbo v1, "{fail}" */
(( java.lang.String ) v1 ).getBytes ( ); // invoke-virtual {v1}, Ljava/lang/String;->getBytes()[B
(( java.io.OutputStream ) v0 ).write ( v1 ); // invoke-virtual {v0, v1}, Ljava/io/OutputStream;->write([B)V
/* :try_end_0 */
/* .catch Ljava/lang/Exception; {:try_start_0 ..:try_end_0} :catch_0 */
/* .line 470 */
} // :cond_0
/* .line 468 */
/* :catch_0 */
/* move-exception v0 */
/* .line 469 */
/* .local v0, "e":Ljava/lang/Exception; */
(( java.lang.Exception ) v0 ).printStackTrace ( ); // invoke-virtual {v0}, Ljava/lang/Exception;->printStackTrace()V
/* .line 471 */
} // .end local v0 # "e":Ljava/lang/Exception;
} // :goto_0
return;
} // .end method
private static void writeFailToNative ( ) {
/* .locals 2 */
/* .line 475 */
try { // :try_start_0
v0 = com.miui.server.MiuiFboService.outputStream;
if ( v0 != null) { // if-eqz v0, :cond_0
/* .line 476 */
/* const-string/jumbo v1, "{broadcast send fail}" */
(( java.lang.String ) v1 ).getBytes ( ); // invoke-virtual {v1}, Ljava/lang/String;->getBytes()[B
(( java.io.OutputStream ) v0 ).write ( v1 ); // invoke-virtual {v0, v1}, Ljava/io/OutputStream;->write([B)V
/* :try_end_0 */
/* .catch Ljava/lang/Exception; {:try_start_0 ..:try_end_0} :catch_0 */
/* .line 480 */
} // :cond_0
/* .line 478 */
/* :catch_0 */
/* move-exception v0 */
/* .line 479 */
/* .local v0, "e":Ljava/lang/Exception; */
(( java.lang.Exception ) v0 ).printStackTrace ( ); // invoke-virtual {v0}, Ljava/lang/Exception;->printStackTrace()V
/* .line 481 */
} // .end local v0 # "e":Ljava/lang/Exception;
} // :goto_0
return;
} // .end method
/* # virtual methods */
public Boolean FBO_isSupport ( ) {
/* .locals 5 */
/* .line 522 */
int v0 = 0; // const/4 v0, 0x0
int v1 = 1; // const/4 v1, 0x1
try { // :try_start_0
com.miui.server.MiuiFboService .callHalFunction ( v0,v1 );
/* check-cast v0, Ljava/lang/Boolean; */
v0 = (( java.lang.Boolean ) v0 ).booleanValue ( ); // invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z
/* iput-boolean v0, p0, Lcom/miui/server/MiuiFboService;->mDriverSupport:Z */
/* .line 523 */
v0 = com.miui.server.MiuiFboService.TAG;
/* new-instance v2, Ljava/lang/StringBuilder; */
/* invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V */
final String v3 = "mDriverSupport = "; // const-string v3, "mDriverSupport = "
(( java.lang.StringBuilder ) v2 ).append ( v3 ); // invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
/* iget-boolean v3, p0, Lcom/miui/server/MiuiFboService;->mDriverSupport:Z */
(( java.lang.StringBuilder ) v2 ).append ( v3 ); // invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v2 ).toString ( ); // invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
android.util.Slog .d ( v0,v2 );
/* :try_end_0 */
/* .catch Ljava/lang/Exception; {:try_start_0 ..:try_end_0} :catch_0 */
/* .line 526 */
/* .line 524 */
/* :catch_0 */
/* move-exception v0 */
/* .line 525 */
/* .local v0, "e":Ljava/lang/Exception; */
v2 = com.miui.server.MiuiFboService.TAG;
/* new-instance v3, Ljava/lang/StringBuilder; */
/* invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V */
final String v4 = "crash in the isSupportFbo"; // const-string v4, "crash in the isSupportFbo"
(( java.lang.StringBuilder ) v3 ).append ( v4 ); // invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v3 ).append ( v0 ); // invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v3 ).toString ( ); // invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
android.util.Slog .e ( v2,v3 );
/* .line 527 */
} // .end local v0 # "e":Ljava/lang/Exception;
} // :goto_0
} // .end method
public Boolean FBO_new_isSupport ( java.lang.String p0 ) {
/* .locals 4 */
/* .param p1, "val" # Ljava/lang/String; */
/* .line 533 */
int v0 = 5; // const/4 v0, 0x5
try { // :try_start_0
com.miui.server.MiuiFboService .callHalFunction ( p1,v0 );
/* check-cast v0, Ljava/lang/Boolean; */
v0 = (( java.lang.Boolean ) v0 ).booleanValue ( ); // invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z
/* iput-boolean v0, p0, Lcom/miui/server/MiuiFboService;->mDriverSupport:Z */
/* .line 534 */
v0 = com.miui.server.MiuiFboService.TAG;
/* new-instance v1, Ljava/lang/StringBuilder; */
/* invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V */
final String v2 = "mDriverSupport = "; // const-string v2, "mDriverSupport = "
(( java.lang.StringBuilder ) v1 ).append ( v2 ); // invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
/* iget-boolean v2, p0, Lcom/miui/server/MiuiFboService;->mDriverSupport:Z */
(( java.lang.StringBuilder ) v1 ).append ( v2 ); // invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v1 ).toString ( ); // invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
android.util.Slog .d ( v0,v1 );
/* :try_end_0 */
/* .catch Ljava/lang/Exception; {:try_start_0 ..:try_end_0} :catch_0 */
/* .line 537 */
/* .line 535 */
/* :catch_0 */
/* move-exception v0 */
/* .line 536 */
/* .local v0, "e":Ljava/lang/Exception; */
v1 = com.miui.server.MiuiFboService.TAG;
/* new-instance v2, Ljava/lang/StringBuilder; */
/* invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V */
final String v3 = "crash in the isSupportFbo"; // const-string v3, "crash in the isSupportFbo"
(( java.lang.StringBuilder ) v2 ).append ( v3 ); // invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v2 ).append ( v0 ); // invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v2 ).toString ( ); // invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
android.util.Slog .e ( v1,v2 );
/* .line 538 */
} // .end local v0 # "e":Ljava/lang/Exception;
} // :goto_0
int v0 = 1; // const/4 v0, 0x1
} // .end method
public void FBO_new_trigger ( java.lang.String p0, Boolean p1 ) {
/* .locals 1 */
/* .param p1, "appList" # Ljava/lang/String; */
/* .param p2, "flag" # Z */
/* .line 543 */
if ( p2 != null) { // if-eqz p2, :cond_0
/* .line 544 */
int v0 = 0; // const/4 v0, 0x0
(( com.miui.server.MiuiFboService ) p0 ).setEnableNightJudgment ( v0 ); // invoke-virtual {p0, v0}, Lcom/miui/server/MiuiFboService;->setEnableNightJudgment(Z)V
/* .line 546 */
} // :cond_0
int v0 = 1; // const/4 v0, 0x1
(( com.miui.server.MiuiFboService ) p0 ).setEnableNightJudgment ( v0 ); // invoke-virtual {p0, v0}, Lcom/miui/server/MiuiFboService;->setEnableNightJudgment(Z)V
/* .line 549 */
} // :goto_0
v0 = com.miui.server.MiuiFboService.sInstance;
(( com.miui.server.MiuiFboService ) v0 ).FBO_trigger ( p1 ); // invoke-virtual {v0, p1}, Lcom/miui/server/MiuiFboService;->FBO_trigger(Ljava/lang/String;)V
/* .line 550 */
return;
} // .end method
public void FBO_notifyFragStatus ( ) {
/* .locals 4 */
/* .line 598 */
try { // :try_start_0
/* invoke-direct {p0}, Lcom/miui/server/MiuiFboService;->reportFboProcessedBroadcast()V */
/* :try_end_0 */
/* .catch Ljava/lang/Exception; {:try_start_0 ..:try_end_0} :catch_0 */
/* .line 601 */
/* .line 599 */
/* :catch_0 */
/* move-exception v0 */
/* .line 600 */
/* .local v0, "e":Ljava/lang/Exception; */
v1 = com.miui.server.MiuiFboService.TAG;
/* new-instance v2, Ljava/lang/StringBuilder; */
/* invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V */
final String v3 = "crash in the notifyFragStatus:"; // const-string v3, "crash in the notifyFragStatus:"
(( java.lang.StringBuilder ) v2 ).append ( v3 ); // invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v2 ).append ( v0 ); // invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v2 ).toString ( ); // invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
android.util.Slog .e ( v1,v2 );
/* .line 602 */
} // .end local v0 # "e":Ljava/lang/Exception;
} // :goto_0
return;
} // .end method
public void FBO_trigger ( java.lang.String p0 ) {
/* .locals 6 */
/* .param p1, "appList" # Ljava/lang/String; */
/* .line 554 */
v0 = (( com.miui.server.MiuiFboService ) p0 ).checkPolicy ( ); // invoke-virtual {p0}, Lcom/miui/server/MiuiFboService;->checkPolicy()Z
/* if-nez v0, :cond_0 */
/* .line 555 */
v0 = com.miui.server.MiuiFboService.TAG;
final String v1 = "Current policy disables fbo functionality"; // const-string v1, "Current policy disables fbo functionality"
android.util.Slog .d ( v0,v1 );
/* .line 556 */
return;
/* .line 559 */
} // :cond_0
v0 = (( com.miui.server.MiuiFboService ) p0 ).getGlobalSwitch ( ); // invoke-virtual {p0}, Lcom/miui/server/MiuiFboService;->getGlobalSwitch()Z
int v1 = 1; // const/4 v1, 0x1
int v2 = 0; // const/4 v2, 0x0
/* if-nez v0, :cond_5 */
/* iget-boolean v0, p0, Lcom/miui/server/MiuiFboService;->messageHasbeenSent:Z */
if ( v0 != null) { // if-eqz v0, :cond_1
/* goto/16 :goto_1 */
/* .line 565 */
} // :cond_1
v0 = /* invoke-direct {p0}, Lcom/miui/server/MiuiFboService;->isWithinTheTimeInterval()Z */
/* if-nez v0, :cond_2 */
v0 = (( com.miui.server.MiuiFboService ) p0 ).getGlobalSwitch ( ); // invoke-virtual {p0}, Lcom/miui/server/MiuiFboService;->getGlobalSwitch()Z
/* if-nez v0, :cond_2 */
/* .line 566 */
v0 = com.miui.server.MiuiFboService.TAG;
final String v3 = "execute suspendTransferFboTrigger()"; // const-string v3, "execute suspendTransferFboTrigger()"
android.util.Slog .d ( v0,v3 );
/* .line 567 */
com.miui.server.MiuiFboService .suspendTransferFboTrigger ( p1,v1,v2 );
/* .line 568 */
return;
/* .line 571 */
} // :cond_2
v0 = (( com.miui.server.MiuiFboService ) p0 ).getUsbState ( ); // invoke-virtual {p0}, Lcom/miui/server/MiuiFboService;->getUsbState()Z
if ( v0 != null) { // if-eqz v0, :cond_3
/* .line 572 */
v0 = com.miui.server.MiuiFboService.TAG;
/* const-string/jumbo v1, "will not execute fbo service because usbState" */
android.util.Slog .d ( v0,v1 );
/* .line 573 */
return;
/* .line 576 */
} // :cond_3
/* invoke-direct {p0}, Lcom/miui/server/MiuiFboService;->getNativeService()Lmiui/fbo/IFbo; */
this.mFboNativeService = v0;
/* .line 578 */
try { // :try_start_0
v0 = this.mFboNativeService;
if ( v0 != null) { // if-eqz v0, :cond_4
/* .line 579 */
v0 = /* invoke-direct {p0}, Lcom/miui/server/MiuiFboService;->isWithinTheTimeInterval()Z */
if ( v0 != null) { // if-eqz v0, :cond_4
v0 = (( com.miui.server.MiuiFboService ) p0 ).getGlobalSwitch ( ); // invoke-virtual {p0}, Lcom/miui/server/MiuiFboService;->getGlobalSwitch()Z
/* if-nez v0, :cond_4 */
/* .line 580 */
com.miui.server.MiuiFboService .removePendingIntentList ( );
/* .line 581 */
com.miui.server.MiuiFboService .formatAppList ( p1 );
/* .line 582 */
v0 = com.miui.server.MiuiFboService.packageNameList;
v0 = (( java.util.ArrayList ) v0 ).size ( ); // invoke-virtual {v0}, Ljava/util/ArrayList;->size()I
/* sub-int/2addr v0, v1 */
/* .line 583 */
v0 = com.miui.server.MiuiFboService.TAG;
/* new-instance v3, Ljava/lang/StringBuilder; */
/* invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V */
final String v4 = "appList is:"; // const-string v4, "appList is:"
(( java.lang.StringBuilder ) v3 ).append ( v4 ); // invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
v4 = com.miui.server.MiuiFboService.packageNameList;
(( java.lang.StringBuilder ) v3 ).append ( v4 ); // invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v3 ).toString ( ); // invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
android.util.Slog .i ( v0,v3 );
/* .line 584 */
final String v0 = "miui.intent.action.start"; // const-string v0, "miui.intent.action.start"
int v3 = 0; // const/4 v3, 0x0
/* const-wide/32 v4, 0x1b7740 */
com.miui.server.MiuiFboService .setAlarm ( v0,v3,v4,v5,v2 );
/* .line 585 */
/* invoke-direct {p0}, Lcom/miui/server/MiuiFboService;->sendStopMessage()V */
/* .line 586 */
/* iput-boolean v1, p0, Lcom/miui/server/MiuiFboService;->messageHasbeenSent:Z */
/* .line 587 */
/* iput v2, p0, Lcom/miui/server/MiuiFboService;->screenOnTimes:I */
/* :try_end_0 */
/* .catch Ljava/lang/Exception; {:try_start_0 ..:try_end_0} :catch_0 */
/* .line 592 */
} // :cond_4
/* .line 590 */
/* :catch_0 */
/* move-exception v0 */
/* .line 591 */
/* .local v0, "e":Ljava/lang/Exception; */
v1 = com.miui.server.MiuiFboService.TAG;
/* new-instance v2, Ljava/lang/StringBuilder; */
/* invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V */
final String v3 = "crash in the FBO_trigger:"; // const-string v3, "crash in the FBO_trigger:"
(( java.lang.StringBuilder ) v2 ).append ( v3 ); // invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v2 ).append ( v0 ); // invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v2 ).toString ( ); // invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
android.util.Slog .e ( v1,v2 );
/* .line 593 */
} // .end local v0 # "e":Ljava/lang/Exception;
} // :goto_0
return;
/* .line 560 */
} // :cond_5
} // :goto_1
v0 = com.miui.server.MiuiFboService.TAG;
final String v3 = "Currently executing fbo or waiting for execution message, so store application list"; // const-string v3, "Currently executing fbo or waiting for execution message, so store application list"
android.util.Slog .d ( v0,v3 );
/* .line 561 */
com.miui.server.MiuiFboService .suspendTransferFboTrigger ( p1,v2,v1 );
/* .line 562 */
return;
} // .end method
public Boolean checkPolicy ( ) {
/* .locals 2 */
/* .line 873 */
final String v0 = "persist.sys.stability.miui_fbo_enable"; // const-string v0, "persist.sys.stability.miui_fbo_enable"
int v1 = 0; // const/4 v1, 0x0
v0 = android.os.SystemProperties .getBoolean ( v0,v1 );
} // .end method
public void deliverMessage ( java.lang.String p0, Integer p1, Long p2 ) {
/* .locals 5 */
/* .param p1, "data" # Ljava/lang/String; */
/* .param p2, "number" # I */
/* .param p3, "sleepTime" # J */
/* .line 789 */
v0 = com.miui.server.MiuiFboService.sInstance;
v0 = this.mFboServiceHandler;
(( com.miui.server.MiuiFboService$FboServiceHandler ) v0 ).obtainMessage ( ); // invoke-virtual {v0}, Lcom/miui/server/MiuiFboService$FboServiceHandler;->obtainMessage()Landroid/os/Message;
/* .line 790 */
/* .local v0, "message":Landroid/os/Message; */
/* iput p2, v0, Landroid/os/Message;->what:I */
/* .line 791 */
this.obj = p1;
/* .line 792 */
v1 = com.miui.server.MiuiFboService.sInstance;
v1 = this.mFboServiceHandler;
(( com.miui.server.MiuiFboService$FboServiceHandler ) v1 ).sendMessageDelayed ( v0, p3, p4 ); // invoke-virtual {v1, v0, p3, p4}, Lcom/miui/server/MiuiFboService$FboServiceHandler;->sendMessageDelayed(Landroid/os/Message;J)Z
/* .line 793 */
v1 = com.miui.server.MiuiFboService.TAG;
/* new-instance v2, Ljava/lang/StringBuilder; */
/* invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V */
final String v3 = "msg.what = "; // const-string v3, "msg.what = "
(( java.lang.StringBuilder ) v2 ).append ( v3 ); // invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
/* iget v3, v0, Landroid/os/Message;->what:I */
(( java.lang.StringBuilder ) v2 ).append ( v3 ); // invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;
/* const-string/jumbo v3, "send message time = " */
(( java.lang.StringBuilder ) v2 ).append ( v3 ); // invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
java.lang.System .currentTimeMillis ( );
/* move-result-wide v3 */
(( java.lang.StringBuilder ) v2 ).append ( v3, v4 ); // invoke-virtual {v2, v3, v4}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v2 ).toString ( ); // invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
android.util.Slog .d ( v1,v2 );
/* .line 794 */
return;
} // .end method
public Boolean getDueToScreenWait ( ) {
/* .locals 1 */
/* .line 853 */
/* iget-boolean v0, p0, Lcom/miui/server/MiuiFboService;->dueToScreenWait:Z */
} // .end method
public Boolean getEnableNightJudgment ( ) {
/* .locals 1 */
/* .line 861 */
/* iget-boolean v0, p0, Lcom/miui/server/MiuiFboService;->enableNightJudgment:Z */
} // .end method
public Boolean getGlobalSwitch ( ) {
/* .locals 1 */
/* .line 844 */
/* iget-boolean v0, p0, Lcom/miui/server/MiuiFboService;->globalSwitch:Z */
} // .end method
public Boolean getNativeIsRunning ( ) {
/* .locals 1 */
/* .line 835 */
/* iget-boolean v0, p0, Lcom/miui/server/MiuiFboService;->nativeIsRunning:Z */
} // .end method
public Boolean getUsbState ( ) {
/* .locals 1 */
/* .line 869 */
/* iget-boolean v0, p0, Lcom/miui/server/MiuiFboService;->usbState:Z */
} // .end method
public void setBatteryInfos ( Integer p0, Integer p1, Integer p2 ) {
/* .locals 5 */
/* .param p1, "batteryStatus" # I */
/* .param p2, "batteryLevel" # I */
/* .param p3, "batteryTemperature" # I */
/* .line 798 */
/* iget v0, p0, Lcom/miui/server/MiuiFboService;->batteryStatusMark:I */
/* if-eq p1, v0, :cond_0 */
/* .line 799 */
/* iput p1, p0, Lcom/miui/server/MiuiFboService;->batteryStatusMark:I */
/* .line 800 */
v0 = com.miui.server.MiuiFboService.sInstance;
int v1 = 6; // const/4 v1, 0x6
/* const-wide/16 v2, 0x2710 */
final String v4 = ""; // const-string v4, ""
(( com.miui.server.MiuiFboService ) v0 ).deliverMessage ( v4, v1, v2, v3 ); // invoke-virtual {v0, v4, v1, v2, v3}, Lcom/miui/server/MiuiFboService;->deliverMessage(Ljava/lang/String;IJ)V
/* .line 802 */
} // :cond_0
/* iget v0, p0, Lcom/miui/server/MiuiFboService;->mCurrentBatteryLevel:I */
/* sub-int/2addr v0, p2 */
int v1 = 5; // const/4 v1, 0x5
/* if-le v0, v1, :cond_1 */
/* if-gtz p1, :cond_1 */
/* .line 803 */
v0 = (( com.miui.server.MiuiFboService ) p0 ).getGlobalSwitch ( ); // invoke-virtual {p0}, Lcom/miui/server/MiuiFboService;->getGlobalSwitch()Z
if ( v0 != null) { // if-eqz v0, :cond_1
/* .line 804 */
int v0 = 3; // const/4 v0, 0x3
/* const-wide/16 v1, 0x0 */
/* const-string/jumbo v3, "stop" */
(( com.miui.server.MiuiFboService ) p0 ).deliverMessage ( v3, v0, v1, v2 ); // invoke-virtual {p0, v3, v0, v1, v2}, Lcom/miui/server/MiuiFboService;->deliverMessage(Ljava/lang/String;IJ)V
/* .line 806 */
} // :cond_1
(( com.miui.server.MiuiFboService ) p0 ).setBatteryStatus ( p1 ); // invoke-virtual {p0, p1}, Lcom/miui/server/MiuiFboService;->setBatteryStatus(I)V
/* .line 807 */
(( com.miui.server.MiuiFboService ) p0 ).setBatteryLevel ( p2 ); // invoke-virtual {p0, p2}, Lcom/miui/server/MiuiFboService;->setBatteryLevel(I)V
/* .line 808 */
(( com.miui.server.MiuiFboService ) p0 ).setBatteryTemperature ( p3 ); // invoke-virtual {p0, p3}, Lcom/miui/server/MiuiFboService;->setBatteryTemperature(I)V
/* .line 809 */
return;
} // .end method
public void setBatteryLevel ( Integer p0 ) {
/* .locals 0 */
/* .param p1, "batteryLevel" # I */
/* .line 822 */
/* iput p1, p0, Lcom/miui/server/MiuiFboService;->batteryLevel:I */
/* .line 823 */
return;
} // .end method
public void setBatteryStatus ( Integer p0 ) {
/* .locals 0 */
/* .param p1, "batteryStatus" # I */
/* .line 818 */
/* iput p1, p0, Lcom/miui/server/MiuiFboService;->batteryStatus:I */
/* .line 819 */
return;
} // .end method
public void setBatteryTemperature ( Integer p0 ) {
/* .locals 0 */
/* .param p1, "batteryTemperature" # I */
/* .line 826 */
/* iput p1, p0, Lcom/miui/server/MiuiFboService;->batteryTemperature:I */
/* .line 827 */
return;
} // .end method
public void setDueToScreenWait ( Boolean p0 ) {
/* .locals 0 */
/* .param p1, "dueToScreenWait" # Z */
/* .line 848 */
/* iput-boolean p1, p0, Lcom/miui/server/MiuiFboService;->dueToScreenWait:Z */
/* .line 849 */
return;
} // .end method
public void setEnableNightJudgment ( Boolean p0 ) {
/* .locals 0 */
/* .param p1, "enableNightJudgment" # Z */
/* .line 857 */
/* iput-boolean p1, p0, Lcom/miui/server/MiuiFboService;->enableNightJudgment:Z */
/* .line 858 */
return;
} // .end method
public void setGlobalSwitch ( Boolean p0 ) {
/* .locals 0 */
/* .param p1, "globalSwitch" # Z */
/* .line 839 */
/* iput-boolean p1, p0, Lcom/miui/server/MiuiFboService;->globalSwitch:Z */
/* .line 840 */
return;
} // .end method
public void setNativeIsRunning ( Boolean p0 ) {
/* .locals 0 */
/* .param p1, "nativeIsRunning" # Z */
/* .line 830 */
/* iput-boolean p1, p0, Lcom/miui/server/MiuiFboService;->nativeIsRunning:Z */
/* .line 831 */
return;
} // .end method
public void setScreenStatus ( Boolean p0 ) {
/* .locals 1 */
/* .param p1, "screenOn" # Z */
/* .line 813 */
/* iget v0, p0, Lcom/miui/server/MiuiFboService;->screenOnTimes:I */
/* add-int/lit8 v0, v0, 0x1 */
/* iput v0, p0, Lcom/miui/server/MiuiFboService;->screenOnTimes:I */
/* .line 814 */
/* iput-boolean p1, p0, Lcom/miui/server/MiuiFboService;->screenOn:Z */
/* .line 815 */
return;
} // .end method
public void setUsbState ( Boolean p0 ) {
/* .locals 0 */
/* .param p1, "usbState" # Z */
/* .line 865 */
/* iput-boolean p1, p0, Lcom/miui/server/MiuiFboService;->usbState:Z */
/* .line 866 */
return;
} // .end method
