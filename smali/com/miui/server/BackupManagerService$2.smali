.class Lcom/miui/server/BackupManagerService$2;
.super Ljava/lang/Object;
.source "BackupManagerService.java"

# interfaces
.implements Ljava/lang/Runnable;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/miui/server/BackupManagerService;->startConfirmationUi(ILjava/lang/String;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/miui/server/BackupManagerService;

.field final synthetic val$token:I


# direct methods
.method constructor <init>(Lcom/miui/server/BackupManagerService;I)V
    .locals 0
    .param p1, "this$0"    # Lcom/miui/server/BackupManagerService;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .line 290
    iput-object p1, p0, Lcom/miui/server/BackupManagerService$2;->this$0:Lcom/miui/server/BackupManagerService;

    iput p2, p0, Lcom/miui/server/BackupManagerService$2;->val$token:I

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public run()V
    .locals 8

    .line 293
    nop

    .line 294
    const-string v0, "backup"

    invoke-static {v0}, Landroid/os/ServiceManager;->getService(Ljava/lang/String;)Landroid/os/IBinder;

    move-result-object v0

    check-cast v0, Landroid/app/backup/IBackupManager;

    .line 296
    .local v0, "bm":Landroid/app/backup/IBackupManager;
    :try_start_0
    iget v2, p0, Lcom/miui/server/BackupManagerService$2;->val$token:I

    const/4 v3, 0x1

    const-string v4, ""

    iget-object v1, p0, Lcom/miui/server/BackupManagerService$2;->this$0:Lcom/miui/server/BackupManagerService;

    invoke-static {v1}, Lcom/miui/server/BackupManagerService;->-$$Nest$fgetmPwd(Lcom/miui/server/BackupManagerService;)Ljava/lang/String;

    move-result-object v5

    new-instance v6, Lcom/miui/server/BackupManagerService$FullBackupRestoreObserver;

    iget-object v1, p0, Lcom/miui/server/BackupManagerService$2;->this$0:Lcom/miui/server/BackupManagerService;

    const/4 v7, 0x0

    invoke-direct {v6, v1, v7}, Lcom/miui/server/BackupManagerService$FullBackupRestoreObserver;-><init>(Lcom/miui/server/BackupManagerService;Lcom/miui/server/BackupManagerService$FullBackupRestoreObserver-IA;)V

    move-object v1, v0

    invoke-interface/range {v1 .. v6}, Landroid/app/backup/IBackupManager;->acknowledgeFullBackupOrRestore(IZLjava/lang/String;Ljava/lang/String;Landroid/app/backup/IFullBackupRestoreObserver;)V
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    .line 299
    goto :goto_0

    .line 297
    :catch_0
    move-exception v1

    .line 298
    .local v1, "e":Landroid/os/RemoteException;
    const-string v2, "Backup:BackupManagerService"

    const-string v3, "acknowledgeFullBackupOrRestore failed"

    invoke-static {v2, v3, v1}, Landroid/util/Slog;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    .line 300
    .end local v1    # "e":Landroid/os/RemoteException;
    :goto_0
    return-void
.end method
