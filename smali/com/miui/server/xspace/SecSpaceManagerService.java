public class com.miui.server.xspace.SecSpaceManagerService {
	 /* .source "SecSpaceManagerService.java" */
	 /* # static fields */
	 public static Integer KID_SPACE_ID;
	 public static Integer SECOND_USER_ID;
	 public static final java.lang.String TAG;
	 private static java.util.List sDataTransferPackageNames;
	 /* .annotation system Ldalvik/annotation/Signature; */
	 /* value = { */
	 /* "Ljava/util/List<", */
	 /* "Ljava/lang/String;", */
	 /* ">;" */
	 /* } */
} // .end annotation
} // .end field
/* # direct methods */
static Integer -$$Nest$smgetSecondSpaceId ( android.content.Context p0 ) { //bridge//synthethic
/* .locals 0 */
p0 = com.miui.server.xspace.SecSpaceManagerService .getSecondSpaceId ( p0 );
} // .end method
static com.miui.server.xspace.SecSpaceManagerService ( ) {
/* .locals 2 */
/* .line 22 */
/* const/16 v0, -0x2710 */
/* .line 23 */
/* .line 24 */
/* new-instance v0, Ljava/util/ArrayList; */
/* invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V */
/* .line 27 */
final String v1 = "com.android.fileexplorer"; // const-string v1, "com.android.fileexplorer"
/* .line 28 */
v0 = com.miui.server.xspace.SecSpaceManagerService.sDataTransferPackageNames;
final String v1 = "com.mi.android.globalFileexplorer"; // const-string v1, "com.mi.android.globalFileexplorer"
/* .line 29 */
v0 = com.miui.server.xspace.SecSpaceManagerService.sDataTransferPackageNames;
final String v1 = "com.miui.securitycore"; // const-string v1, "com.miui.securitycore"
/* .line 30 */
v0 = com.miui.server.xspace.SecSpaceManagerService.sDataTransferPackageNames;
final String v1 = "com.miui.gallery"; // const-string v1, "com.miui.gallery"
/* .line 31 */
v0 = com.miui.server.xspace.SecSpaceManagerService.sDataTransferPackageNames;
final String v1 = "com.android.providers.media.module"; // const-string v1, "com.android.providers.media.module"
/* .line 32 */
v0 = com.miui.server.xspace.SecSpaceManagerService.sDataTransferPackageNames;
final String v1 = "com.google.android.providers.media.module"; // const-string v1, "com.google.android.providers.media.module"
/* .line 33 */
return;
} // .end method
public com.miui.server.xspace.SecSpaceManagerService ( ) {
/* .locals 0 */
/* .line 20 */
/* invoke-direct {p0}, Ljava/lang/Object;-><init>()V */
return;
} // .end method
private static Integer getKidSpaceId ( android.content.Context p0 ) {
/* .locals 4 */
/* .param p0, "context" # Landroid/content/Context; */
/* .line 88 */
(( android.content.Context ) p0 ).getContentResolver ( ); // invoke-virtual {p0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;
/* const/16 v1, -0x2710 */
int v2 = 0; // const/4 v2, 0x0
final String v3 = "kid_user_id"; // const-string v3, "kid_user_id"
v0 = android.provider.Settings$Secure .getIntForUser ( v0,v3,v1,v2 );
} // .end method
private static Integer getSecondSpaceId ( android.content.Context p0 ) {
/* .locals 3 */
/* .param p0, "context" # Landroid/content/Context; */
/* .line 83 */
(( android.content.Context ) p0 ).getContentResolver ( ); // invoke-virtual {p0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;
/* const-string/jumbo v1, "second_user_id" */
/* const/16 v2, -0x2710 */
v0 = android.provider.Settings$Secure .getInt ( v0,v1,v2 );
} // .end method
public static void init ( android.content.Context p0 ) {
/* .locals 3 */
/* .param p0, "context" # Landroid/content/Context; */
/* .line 36 */
final String v0 = "init SecSpaceManagerService"; // const-string v0, "init SecSpaceManagerService"
final String v1 = "SecSpaceManagerService"; // const-string v1, "SecSpaceManagerService"
android.util.Slog .d ( v1,v0 );
/* .line 37 */
/* sget-boolean v0, Lmiui/util/OldmanUtil;->IS_ELDER_MODE:Z */
if ( v0 != null) { // if-eqz v0, :cond_0
	 /* .line 38 */
	 return;
	 /* .line 40 */
} // :cond_0
v0 = com.miui.server.xspace.SecSpaceManagerService .getSecondSpaceId ( p0 );
/* .line 41 */
/* const/16 v2, -0x2710 */
/* if-eq v0, v2, :cond_1 */
/* .line 42 */
com.miui.server.xspace.SecSpaceManagerService .startSecSpace ( p0 );
/* .line 44 */
} // :cond_1
v0 = com.miui.server.xspace.SecSpaceManagerService .getKidSpaceId ( p0 );
/* .line 45 */
/* if-eq v0, v2, :cond_2 */
/* .line 46 */
/* const-string/jumbo v0, "start KidModeSpaceService" */
android.util.Slog .d ( v1,v0 );
/* .line 47 */
com.miui.server.xspace.SecSpaceManagerService .startKidSpace ( p0 );
/* .line 49 */
} // :cond_2
com.miui.server.xspace.SecSpaceManagerService .registerContentObserver ( p0 );
/* .line 50 */
return;
} // .end method
public static Boolean isDataTransferProcess ( Integer p0, java.lang.String p1 ) {
/* .locals 1 */
/* .param p0, "userId" # I */
/* .param p1, "packageName" # Ljava/lang/String; */
/* .line 94 */
/* if-ne p0, v0, :cond_0 */
v0 = v0 = com.miui.server.xspace.SecSpaceManagerService.sDataTransferPackageNames;
if ( v0 != null) { // if-eqz v0, :cond_0
int v0 = 1; // const/4 v0, 0x1
} // :cond_0
int v0 = 0; // const/4 v0, 0x0
} // :goto_0
} // .end method
private static void registerContentObserver ( android.content.Context p0 ) {
/* .locals 5 */
/* .param p0, "context" # Landroid/content/Context; */
/* .line 72 */
/* new-instance v0, Lcom/miui/server/xspace/SecSpaceManagerService$1; */
int v1 = 0; // const/4 v1, 0x0
/* invoke-direct {v0, v1, p0}, Lcom/miui/server/xspace/SecSpaceManagerService$1;-><init>(Landroid/os/Handler;Landroid/content/Context;)V */
/* .line 78 */
/* .local v0, "contentObserver":Landroid/database/ContentObserver; */
(( android.content.Context ) p0 ).getContentResolver ( ); // invoke-virtual {p0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;
/* const-string/jumbo v2, "second_user_id" */
android.provider.Settings$Secure .getUriFor ( v2 );
int v3 = 1; // const/4 v3, 0x1
int v4 = 0; // const/4 v4, 0x0
(( android.content.ContentResolver ) v1 ).registerContentObserver ( v2, v3, v0, v4 ); // invoke-virtual {v1, v2, v3, v0, v4}, Landroid/content/ContentResolver;->registerContentObserver(Landroid/net/Uri;ZLandroid/database/ContentObserver;I)V
/* .line 80 */
return;
} // .end method
private static void startKidSpace ( android.content.Context p0 ) {
/* .locals 4 */
/* .param p0, "context" # Landroid/content/Context; */
/* .line 62 */
try { // :try_start_0
/* new-instance v0, Landroid/content/Intent; */
/* invoke-direct {v0}, Landroid/content/Intent;-><init>()V */
/* .line 63 */
/* .local v0, "intent":Landroid/content/Intent; */
/* new-instance v1, Landroid/content/ComponentName; */
final String v2 = "com.miui.securitycore"; // const-string v2, "com.miui.securitycore"
final String v3 = "com.miui.securityspace.service.KidModeSpaceService"; // const-string v3, "com.miui.securityspace.service.KidModeSpaceService"
/* invoke-direct {v1, v2, v3}, Landroid/content/ComponentName;-><init>(Ljava/lang/String;Ljava/lang/String;)V */
(( android.content.Intent ) v0 ).setComponent ( v1 ); // invoke-virtual {v0, v1}, Landroid/content/Intent;->setComponent(Landroid/content/ComponentName;)Landroid/content/Intent;
/* .line 65 */
(( android.content.Context ) p0 ).startService ( v0 ); // invoke-virtual {p0, v0}, Landroid/content/Context;->startService(Landroid/content/Intent;)Landroid/content/ComponentName;
/* :try_end_0 */
/* .catch Ljava/lang/Exception; {:try_start_0 ..:try_end_0} :catch_0 */
/* .line 68 */
/* nop */
} // .end local v0 # "intent":Landroid/content/Intent;
/* .line 66 */
/* :catch_0 */
/* move-exception v0 */
/* .line 67 */
/* .local v0, "e":Ljava/lang/Exception; */
final String v1 = "SecSpaceManagerService"; // const-string v1, "SecSpaceManagerService"
/* const-string/jumbo v2, "start KidModeSpaceService" */
android.util.Slog .e ( v1,v2,v0 );
/* .line 69 */
} // .end local v0 # "e":Ljava/lang/Exception;
} // :goto_0
return;
} // .end method
private static void startSecSpace ( android.content.Context p0 ) {
/* .locals 4 */
/* .param p0, "context" # Landroid/content/Context; */
/* .line 54 */
/* new-instance v0, Landroid/content/Intent; */
/* invoke-direct {v0}, Landroid/content/Intent;-><init>()V */
/* .line 55 */
/* .local v0, "intent":Landroid/content/Intent; */
/* new-instance v1, Landroid/content/ComponentName; */
final String v2 = "com.miui.securitycore"; // const-string v2, "com.miui.securitycore"
final String v3 = "com.miui.securityspace.service.SecondSpaceService"; // const-string v3, "com.miui.securityspace.service.SecondSpaceService"
/* invoke-direct {v1, v2, v3}, Landroid/content/ComponentName;-><init>(Ljava/lang/String;Ljava/lang/String;)V */
(( android.content.Intent ) v0 ).setComponent ( v1 ); // invoke-virtual {v0, v1}, Landroid/content/Intent;->setComponent(Landroid/content/ComponentName;)Landroid/content/Intent;
/* .line 57 */
(( android.content.Context ) p0 ).startService ( v0 ); // invoke-virtual {p0, v0}, Landroid/content/Context;->startService(Landroid/content/Intent;)Landroid/content/ComponentName;
/* .line 58 */
return;
} // .end method
