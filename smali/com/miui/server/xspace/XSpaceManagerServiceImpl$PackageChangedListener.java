class com.miui.server.xspace.XSpaceManagerServiceImpl$PackageChangedListener extends android.content.BroadcastReceiver {
	 /* .source "XSpaceManagerServiceImpl.java" */
	 /* # annotations */
	 /* .annotation system Ldalvik/annotation/EnclosingClass; */
	 /* value = Lcom/miui/server/xspace/XSpaceManagerServiceImpl; */
} // .end annotation
/* .annotation system Ldalvik/annotation/InnerClass; */
/* accessFlags = 0x2 */
/* name = "PackageChangedListener" */
} // .end annotation
/* # instance fields */
final com.miui.server.xspace.XSpaceManagerServiceImpl this$0; //synthetic
/* # direct methods */
private com.miui.server.xspace.XSpaceManagerServiceImpl$PackageChangedListener ( ) {
/* .locals 0 */
/* .line 501 */
this.this$0 = p1;
/* invoke-direct {p0}, Landroid/content/BroadcastReceiver;-><init>()V */
return;
} // .end method
 com.miui.server.xspace.XSpaceManagerServiceImpl$PackageChangedListener ( ) { //synthethic
/* .locals 0 */
/* invoke-direct {p0, p1}, Lcom/miui/server/xspace/XSpaceManagerServiceImpl$PackageChangedListener;-><init>(Lcom/miui/server/xspace/XSpaceManagerServiceImpl;)V */
return;
} // .end method
/* # virtual methods */
public void onReceive ( android.content.Context p0, android.content.Intent p1 ) {
/* .locals 6 */
/* .param p1, "context" # Landroid/content/Context; */
/* .param p2, "intent" # Landroid/content/Intent; */
/* .line 505 */
final String v0 = "android.intent.extra.user_handle"; // const-string v0, "android.intent.extra.user_handle"
/* const/16 v1, -0x2710 */
v0 = (( android.content.Intent ) p2 ).getIntExtra ( v0, v1 ); // invoke-virtual {p2, v0, v1}, Landroid/content/Intent;->getIntExtra(Ljava/lang/String;I)I
/* .line 507 */
/* .local v0, "userId":I */
/* if-ne v0, v1, :cond_0 */
/* .line 508 */
/* new-instance v1, Ljava/lang/StringBuilder; */
/* invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V */
final String v2 = "Intent broadcast does not contain user handle: "; // const-string v2, "Intent broadcast does not contain user handle: "
(( java.lang.StringBuilder ) v1 ).append ( v2 ); // invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v1 ).append ( p2 ); // invoke-virtual {v1, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v1 ).toString ( ); // invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
final String v2 = "XSpaceManagerServiceImpl"; // const-string v2, "XSpaceManagerServiceImpl"
android.util.Slog .w ( v2,v1 );
/* .line 509 */
return;
/* .line 511 */
} // :cond_0
(( android.content.Intent ) p2 ).getAction ( ); // invoke-virtual {p2}, Landroid/content/Intent;->getAction()Ljava/lang/String;
/* .line 513 */
/* .local v1, "action":Ljava/lang/String; */
/* new-instance v2, Landroid/os/UserHandle; */
/* invoke-direct {v2, v0}, Landroid/os/UserHandle;-><init>(I)V */
/* .line 514 */
/* .local v2, "user":Landroid/os/UserHandle; */
v3 = this.this$0;
com.miui.server.xspace.XSpaceManagerServiceImpl .-$$Nest$mgetPackageName ( v3,p2 );
/* .line 515 */
/* .local v3, "packageName":Ljava/lang/String; */
v4 = (( java.lang.String ) v1 ).hashCode ( ); // invoke-virtual {v1}, Ljava/lang/String;->hashCode()I
final String v5 = "android.intent.action.PACKAGE_ADDED"; // const-string v5, "android.intent.action.PACKAGE_ADDED"
/* sparse-switch v4, :sswitch_data_0 */
} // :cond_1
/* :sswitch_0 */
v4 = (( java.lang.String ) v1 ).equals ( v5 ); // invoke-virtual {v1, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z
if ( v4 != null) { // if-eqz v4, :cond_1
int v4 = 0; // const/4 v4, 0x0
/* :sswitch_1 */
final String v4 = "android.intent.action.PACKAGE_REMOVED_INTERNAL"; // const-string v4, "android.intent.action.PACKAGE_REMOVED_INTERNAL"
v4 = (( java.lang.String ) v1 ).equals ( v4 ); // invoke-virtual {v1, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z
if ( v4 != null) { // if-eqz v4, :cond_1
int v4 = 1; // const/4 v4, 0x1
} // :goto_0
int v4 = -1; // const/4 v4, -0x1
} // :goto_1
/* packed-switch v4, :pswitch_data_0 */
/* .line 520 */
/* :pswitch_0 */
v4 = this.this$0;
final String v5 = "android.intent.action.PACKAGE_REMOVED"; // const-string v5, "android.intent.action.PACKAGE_REMOVED"
com.miui.server.xspace.XSpaceManagerServiceImpl .-$$Nest$monPackageCallback ( v4,v3,v2,v5 );
/* .line 521 */
/* .line 517 */
/* :pswitch_1 */
v4 = this.this$0;
com.miui.server.xspace.XSpaceManagerServiceImpl .-$$Nest$monPackageCallback ( v4,v3,v2,v5 );
/* .line 518 */
/* nop */
/* .line 525 */
} // :goto_2
return;
/* :sswitch_data_0 */
/* .sparse-switch */
/* -0x5cddf766 -> :sswitch_1 */
/* 0x5c1076e2 -> :sswitch_0 */
} // .end sparse-switch
/* :pswitch_data_0 */
/* .packed-switch 0x0 */
/* :pswitch_1 */
/* :pswitch_0 */
} // .end packed-switch
} // .end method
