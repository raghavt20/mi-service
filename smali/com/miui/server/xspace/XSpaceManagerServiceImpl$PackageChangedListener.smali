.class Lcom/miui/server/xspace/XSpaceManagerServiceImpl$PackageChangedListener;
.super Landroid/content/BroadcastReceiver;
.source "XSpaceManagerServiceImpl.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/miui/server/xspace/XSpaceManagerServiceImpl;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "PackageChangedListener"
.end annotation


# instance fields
.field final synthetic this$0:Lcom/miui/server/xspace/XSpaceManagerServiceImpl;


# direct methods
.method private constructor <init>(Lcom/miui/server/xspace/XSpaceManagerServiceImpl;)V
    .locals 0

    .line 501
    iput-object p1, p0, Lcom/miui/server/xspace/XSpaceManagerServiceImpl$PackageChangedListener;->this$0:Lcom/miui/server/xspace/XSpaceManagerServiceImpl;

    invoke-direct {p0}, Landroid/content/BroadcastReceiver;-><init>()V

    return-void
.end method

.method synthetic constructor <init>(Lcom/miui/server/xspace/XSpaceManagerServiceImpl;Lcom/miui/server/xspace/XSpaceManagerServiceImpl$PackageChangedListener-IA;)V
    .locals 0

    invoke-direct {p0, p1}, Lcom/miui/server/xspace/XSpaceManagerServiceImpl$PackageChangedListener;-><init>(Lcom/miui/server/xspace/XSpaceManagerServiceImpl;)V

    return-void
.end method


# virtual methods
.method public onReceive(Landroid/content/Context;Landroid/content/Intent;)V
    .locals 6
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "intent"    # Landroid/content/Intent;

    .line 505
    const-string v0, "android.intent.extra.user_handle"

    const/16 v1, -0x2710

    invoke-virtual {p2, v0, v1}, Landroid/content/Intent;->getIntExtra(Ljava/lang/String;I)I

    move-result v0

    .line 507
    .local v0, "userId":I
    if-ne v0, v1, :cond_0

    .line 508
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Intent broadcast does not contain user handle: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    const-string v2, "XSpaceManagerServiceImpl"

    invoke-static {v2, v1}, Landroid/util/Slog;->w(Ljava/lang/String;Ljava/lang/String;)I

    .line 509
    return-void

    .line 511
    :cond_0
    invoke-virtual {p2}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v1

    .line 513
    .local v1, "action":Ljava/lang/String;
    new-instance v2, Landroid/os/UserHandle;

    invoke-direct {v2, v0}, Landroid/os/UserHandle;-><init>(I)V

    .line 514
    .local v2, "user":Landroid/os/UserHandle;
    iget-object v3, p0, Lcom/miui/server/xspace/XSpaceManagerServiceImpl$PackageChangedListener;->this$0:Lcom/miui/server/xspace/XSpaceManagerServiceImpl;

    invoke-static {v3, p2}, Lcom/miui/server/xspace/XSpaceManagerServiceImpl;->-$$Nest$mgetPackageName(Lcom/miui/server/xspace/XSpaceManagerServiceImpl;Landroid/content/Intent;)Ljava/lang/String;

    move-result-object v3

    .line 515
    .local v3, "packageName":Ljava/lang/String;
    invoke-virtual {v1}, Ljava/lang/String;->hashCode()I

    move-result v4

    const-string v5, "android.intent.action.PACKAGE_ADDED"

    sparse-switch v4, :sswitch_data_0

    :cond_1
    goto :goto_0

    :sswitch_0
    invoke-virtual {v1, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_1

    const/4 v4, 0x0

    goto :goto_1

    :sswitch_1
    const-string v4, "android.intent.action.PACKAGE_REMOVED_INTERNAL"

    invoke-virtual {v1, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_1

    const/4 v4, 0x1

    goto :goto_1

    :goto_0
    const/4 v4, -0x1

    :goto_1
    packed-switch v4, :pswitch_data_0

    goto :goto_2

    .line 520
    :pswitch_0
    iget-object v4, p0, Lcom/miui/server/xspace/XSpaceManagerServiceImpl$PackageChangedListener;->this$0:Lcom/miui/server/xspace/XSpaceManagerServiceImpl;

    const-string v5, "android.intent.action.PACKAGE_REMOVED"

    invoke-static {v4, v3, v2, v5}, Lcom/miui/server/xspace/XSpaceManagerServiceImpl;->-$$Nest$monPackageCallback(Lcom/miui/server/xspace/XSpaceManagerServiceImpl;Ljava/lang/String;Landroid/os/UserHandle;Ljava/lang/String;)V

    .line 521
    goto :goto_2

    .line 517
    :pswitch_1
    iget-object v4, p0, Lcom/miui/server/xspace/XSpaceManagerServiceImpl$PackageChangedListener;->this$0:Lcom/miui/server/xspace/XSpaceManagerServiceImpl;

    invoke-static {v4, v3, v2, v5}, Lcom/miui/server/xspace/XSpaceManagerServiceImpl;->-$$Nest$monPackageCallback(Lcom/miui/server/xspace/XSpaceManagerServiceImpl;Ljava/lang/String;Landroid/os/UserHandle;Ljava/lang/String;)V

    .line 518
    nop

    .line 525
    :goto_2
    return-void

    :sswitch_data_0
    .sparse-switch
        -0x5cddf766 -> :sswitch_1
        0x5c1076e2 -> :sswitch_0
    .end sparse-switch

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_1
        :pswitch_0
    .end packed-switch
.end method
