class com.miui.server.xspace.XSpaceManagerServiceImpl$2 implements java.lang.Runnable {
	 /* .source "XSpaceManagerServiceImpl.java" */
	 /* # interfaces */
	 /* # annotations */
	 /* .annotation system Ldalvik/annotation/EnclosingMethod; */
	 /* value = Lcom/miui/server/xspace/XSpaceManagerServiceImpl;->showSwitchingDialog(Lcom/android/server/am/ActivityManagerService;Landroid/content/Context;IILandroid/os/Handler;I)Z */
} // .end annotation
/* .annotation system Ldalvik/annotation/InnerClass; */
/* accessFlags = 0x0 */
/* name = null */
} // .end annotation
/* # instance fields */
final com.miui.server.xspace.XSpaceManagerServiceImpl this$0; //synthetic
final com.android.server.am.ActivityManagerService val$ams; //synthetic
final android.content.Context val$context; //synthetic
final Integer val$currentUserId; //synthetic
final android.os.Handler val$handler; //synthetic
final Integer val$targetUserId; //synthetic
final Integer val$userSwitchUiMessage; //synthetic
/* # direct methods */
 com.miui.server.xspace.XSpaceManagerServiceImpl$2 ( ) {
/* .locals 0 */
/* .param p1, "this$0" # Lcom/miui/server/xspace/XSpaceManagerServiceImpl; */
/* .annotation system Ldalvik/annotation/Signature; */
/* value = { */
/* "()V" */
/* } */
} // .end annotation
/* .line 798 */
this.this$0 = p1;
this.val$context = p2;
/* iput p3, p0, Lcom/miui/server/xspace/XSpaceManagerServiceImpl$2;->val$targetUserId:I */
/* iput p4, p0, Lcom/miui/server/xspace/XSpaceManagerServiceImpl$2;->val$currentUserId:I */
this.val$ams = p5;
this.val$handler = p6;
/* iput p7, p0, Lcom/miui/server/xspace/XSpaceManagerServiceImpl$2;->val$userSwitchUiMessage:I */
/* invoke-direct {p0}, Ljava/lang/Object;-><init>()V */
return;
} // .end method
/* # virtual methods */
public void run ( ) {
/* .locals 7 */
/* .line 800 */
v0 = this.val$context;
(( android.content.Context ) v0 ).getContentResolver ( ); // invoke-virtual {v0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;
/* const-string/jumbo v1, "second_user_id" */
/* const/16 v2, -0x2710 */
int v3 = 0; // const/4 v3, 0x0
v0 = android.provider.Settings$Secure .getIntForUser ( v0,v1,v2,v3 );
/* .line 802 */
/* .local v0, "secondSpaceId":I */
v1 = this.val$context;
(( android.content.Context ) v1 ).getContentResolver ( ); // invoke-virtual {v1}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;
final String v4 = "kid_user_id"; // const-string v4, "kid_user_id"
v1 = android.provider.Settings$Secure .getIntForUser ( v1,v4,v2,v3 );
/* .line 804 */
/* .local v1, "kidSpaceUserId":I */
/* iget v2, p0, Lcom/miui/server/xspace/XSpaceManagerServiceImpl$2;->val$targetUserId:I */
/* if-ne v2, v0, :cond_0 */
/* iget v3, p0, Lcom/miui/server/xspace/XSpaceManagerServiceImpl$2;->val$currentUserId:I */
if ( v3 != null) { // if-eqz v3, :cond_1
} // :cond_0
/* iget v3, p0, Lcom/miui/server/xspace/XSpaceManagerServiceImpl$2;->val$currentUserId:I */
/* if-ne v3, v0, :cond_2 */
/* if-nez v2, :cond_2 */
/* .line 806 */
} // :cond_1
/* new-instance v2, Lcom/miui/server/xspace/SecondSpaceSwitchingDialog; */
v3 = this.val$ams;
v4 = this.val$context;
/* iget v5, p0, Lcom/miui/server/xspace/XSpaceManagerServiceImpl$2;->val$targetUserId:I */
/* invoke-direct {v2, v3, v4, v5}, Lcom/miui/server/xspace/SecondSpaceSwitchingDialog;-><init>(Lcom/android/server/am/ActivityManagerService;Landroid/content/Context;I)V */
com.miui.server.xspace.XSpaceManagerServiceImpl .-$$Nest$sfputmDialog ( v2 );
/* .line 807 */
com.miui.server.xspace.XSpaceManagerServiceImpl .-$$Nest$sfgetmDialog ( );
(( com.miui.server.xspace.BaseUserSwitchingDialog ) v2 ).show ( ); // invoke-virtual {v2}, Lcom/miui/server/xspace/BaseUserSwitchingDialog;->show()V
/* .line 808 */
} // :cond_2
/* if-ne v2, v1, :cond_3 */
if ( v3 != null) { // if-eqz v3, :cond_4
} // :cond_3
/* if-ne v3, v1, :cond_5 */
/* if-nez v2, :cond_5 */
/* .line 810 */
} // :cond_4
/* new-instance v2, Lcom/miui/server/xspace/KidSpaceSwitchingDialog; */
v3 = this.val$ams;
v4 = this.val$context;
/* iget v5, p0, Lcom/miui/server/xspace/XSpaceManagerServiceImpl$2;->val$targetUserId:I */
/* invoke-direct {v2, v3, v4, v5}, Lcom/miui/server/xspace/KidSpaceSwitchingDialog;-><init>(Lcom/android/server/am/ActivityManagerService;Landroid/content/Context;I)V */
com.miui.server.xspace.XSpaceManagerServiceImpl .-$$Nest$sfputmDialog ( v2 );
/* .line 811 */
com.miui.server.xspace.XSpaceManagerServiceImpl .-$$Nest$sfgetmDialog ( );
(( com.miui.server.xspace.BaseUserSwitchingDialog ) v2 ).show ( ); // invoke-virtual {v2}, Lcom/miui/server/xspace/BaseUserSwitchingDialog;->show()V
/* .line 813 */
} // :cond_5
com.miui.server.xspace.XSpaceManagerServiceImpl .-$$Nest$smgetUserManager ( );
/* iget v3, p0, Lcom/miui/server/xspace/XSpaceManagerServiceImpl$2;->val$currentUserId:I */
(( com.android.server.pm.UserManagerService ) v2 ).getUserInfo ( v3 ); // invoke-virtual {v2, v3}, Lcom/android/server/pm/UserManagerService;->getUserInfo(I)Landroid/content/pm/UserInfo;
/* .line 814 */
/* .local v2, "currentUserInfo":Landroid/content/pm/UserInfo; */
com.miui.server.xspace.XSpaceManagerServiceImpl .-$$Nest$smgetUserManager ( );
/* iget v4, p0, Lcom/miui/server/xspace/XSpaceManagerServiceImpl$2;->val$targetUserId:I */
(( com.android.server.pm.UserManagerService ) v3 ).getUserInfo ( v4 ); // invoke-virtual {v3, v4}, Lcom/android/server/pm/UserManagerService;->getUserInfo(I)Landroid/content/pm/UserInfo;
/* .line 815 */
/* .local v3, "targetUserInfo":Landroid/content/pm/UserInfo; */
/* new-instance v4, Landroid/util/Pair; */
/* invoke-direct {v4, v2, v3}, Landroid/util/Pair;-><init>(Ljava/lang/Object;Ljava/lang/Object;)V */
/* .line 816 */
/* .local v4, "userNames":Landroid/util/Pair;, "Landroid/util/Pair<Landroid/content/pm/UserInfo;Landroid/content/pm/UserInfo;>;" */
v5 = this.val$handler;
/* iget v6, p0, Lcom/miui/server/xspace/XSpaceManagerServiceImpl$2;->val$userSwitchUiMessage:I */
(( android.os.Handler ) v5 ).obtainMessage ( v6, v4 ); // invoke-virtual {v5, v6, v4}, Landroid/os/Handler;->obtainMessage(ILjava/lang/Object;)Landroid/os/Message;
(( android.os.Handler ) v5 ).sendMessage ( v6 ); // invoke-virtual {v5, v6}, Landroid/os/Handler;->sendMessage(Landroid/os/Message;)Z
/* .line 819 */
} // .end local v2 # "currentUserInfo":Landroid/content/pm/UserInfo;
} // .end local v3 # "targetUserInfo":Landroid/content/pm/UserInfo;
} // .end local v4 # "userNames":Landroid/util/Pair;, "Landroid/util/Pair<Landroid/content/pm/UserInfo;Landroid/content/pm/UserInfo;>;"
} // :goto_0
return;
} // .end method
