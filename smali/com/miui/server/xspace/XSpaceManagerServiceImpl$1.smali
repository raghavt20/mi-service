.class Lcom/miui/server/xspace/XSpaceManagerServiceImpl$1;
.super Landroid/database/ContentObserver;
.source "XSpaceManagerServiceImpl.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/miui/server/xspace/XSpaceManagerServiceImpl;->init(Landroid/content/Context;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/miui/server/xspace/XSpaceManagerServiceImpl;


# direct methods
.method constructor <init>(Lcom/miui/server/xspace/XSpaceManagerServiceImpl;Landroid/os/Handler;)V
    .locals 0
    .param p1, "this$0"    # Lcom/miui/server/xspace/XSpaceManagerServiceImpl;
    .param p2, "handler"    # Landroid/os/Handler;

    .line 445
    iput-object p1, p0, Lcom/miui/server/xspace/XSpaceManagerServiceImpl$1;->this$0:Lcom/miui/server/xspace/XSpaceManagerServiceImpl;

    invoke-direct {p0, p2}, Landroid/database/ContentObserver;-><init>(Landroid/os/Handler;)V

    return-void
.end method


# virtual methods
.method public onChange(Z)V
    .locals 4
    .param p1, "selfChange"    # Z

    .line 448
    invoke-super {p0, p1}, Landroid/database/ContentObserver;->onChange(Z)V

    .line 449
    iget-object v0, p0, Lcom/miui/server/xspace/XSpaceManagerServiceImpl$1;->this$0:Lcom/miui/server/xspace/XSpaceManagerServiceImpl;

    invoke-static {v0}, Lcom/miui/server/xspace/XSpaceManagerServiceImpl;->-$$Nest$fgetmResolver(Lcom/miui/server/xspace/XSpaceManagerServiceImpl;)Landroid/content/ContentResolver;

    move-result-object v1

    const-string/jumbo v2, "xspace_enabled"

    const/4 v3, 0x0

    invoke-static {v1, v2, v3}, Landroid/provider/MiuiSettings$Secure;->getBoolean(Landroid/content/ContentResolver;Ljava/lang/String;Z)Z

    move-result v1

    invoke-static {v0, v1}, Lcom/miui/server/xspace/XSpaceManagerServiceImpl;->-$$Nest$fputmIsXSpaceActived(Lcom/miui/server/xspace/XSpaceManagerServiceImpl;Z)V

    .line 451
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v1, "update XSpace status, active:"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lcom/miui/server/xspace/XSpaceManagerServiceImpl$1;->this$0:Lcom/miui/server/xspace/XSpaceManagerServiceImpl;

    invoke-static {v1}, Lcom/miui/server/xspace/XSpaceManagerServiceImpl;->-$$Nest$fgetmIsXSpaceActived(Lcom/miui/server/xspace/XSpaceManagerServiceImpl;)Z

    move-result v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    const-string v1, "XSpaceManagerServiceImpl"

    invoke-static {v1, v0}, Landroid/util/Slog;->w(Ljava/lang/String;Ljava/lang/String;)I

    .line 452
    return-void
.end method
