.class public Lcom/miui/server/xspace/SecSpaceManagerService;
.super Ljava/lang/Object;
.source "SecSpaceManagerService.java"


# static fields
.field public static KID_SPACE_ID:I = 0x0

.field public static SECOND_USER_ID:I = 0x0

.field public static final TAG:Ljava/lang/String; = "SecSpaceManagerService"

.field private static sDataTransferPackageNames:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method static bridge synthetic -$$Nest$smgetSecondSpaceId(Landroid/content/Context;)I
    .locals 0

    invoke-static {p0}, Lcom/miui/server/xspace/SecSpaceManagerService;->getSecondSpaceId(Landroid/content/Context;)I

    move-result p0

    return p0
.end method

.method static constructor <clinit>()V
    .locals 2

    .line 22
    const/16 v0, -0x2710

    sput v0, Lcom/miui/server/xspace/SecSpaceManagerService;->SECOND_USER_ID:I

    .line 23
    sput v0, Lcom/miui/server/xspace/SecSpaceManagerService;->KID_SPACE_ID:I

    .line 24
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    sput-object v0, Lcom/miui/server/xspace/SecSpaceManagerService;->sDataTransferPackageNames:Ljava/util/List;

    .line 27
    const-string v1, "com.android.fileexplorer"

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 28
    sget-object v0, Lcom/miui/server/xspace/SecSpaceManagerService;->sDataTransferPackageNames:Ljava/util/List;

    const-string v1, "com.mi.android.globalFileexplorer"

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 29
    sget-object v0, Lcom/miui/server/xspace/SecSpaceManagerService;->sDataTransferPackageNames:Ljava/util/List;

    const-string v1, "com.miui.securitycore"

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 30
    sget-object v0, Lcom/miui/server/xspace/SecSpaceManagerService;->sDataTransferPackageNames:Ljava/util/List;

    const-string v1, "com.miui.gallery"

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 31
    sget-object v0, Lcom/miui/server/xspace/SecSpaceManagerService;->sDataTransferPackageNames:Ljava/util/List;

    const-string v1, "com.android.providers.media.module"

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 32
    sget-object v0, Lcom/miui/server/xspace/SecSpaceManagerService;->sDataTransferPackageNames:Ljava/util/List;

    const-string v1, "com.google.android.providers.media.module"

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 33
    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .line 20
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method private static getKidSpaceId(Landroid/content/Context;)I
    .locals 4
    .param p0, "context"    # Landroid/content/Context;

    .line 88
    invoke-virtual {p0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    const/16 v1, -0x2710

    const/4 v2, 0x0

    const-string v3, "kid_user_id"

    invoke-static {v0, v3, v1, v2}, Landroid/provider/Settings$Secure;->getIntForUser(Landroid/content/ContentResolver;Ljava/lang/String;II)I

    move-result v0

    return v0
.end method

.method private static getSecondSpaceId(Landroid/content/Context;)I
    .locals 3
    .param p0, "context"    # Landroid/content/Context;

    .line 83
    invoke-virtual {p0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    const-string/jumbo v1, "second_user_id"

    const/16 v2, -0x2710

    invoke-static {v0, v1, v2}, Landroid/provider/Settings$Secure;->getInt(Landroid/content/ContentResolver;Ljava/lang/String;I)I

    move-result v0

    return v0
.end method

.method public static init(Landroid/content/Context;)V
    .locals 3
    .param p0, "context"    # Landroid/content/Context;

    .line 36
    const-string v0, "init SecSpaceManagerService"

    const-string v1, "SecSpaceManagerService"

    invoke-static {v1, v0}, Landroid/util/Slog;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 37
    sget-boolean v0, Lmiui/util/OldmanUtil;->IS_ELDER_MODE:Z

    if-eqz v0, :cond_0

    .line 38
    return-void

    .line 40
    :cond_0
    invoke-static {p0}, Lcom/miui/server/xspace/SecSpaceManagerService;->getSecondSpaceId(Landroid/content/Context;)I

    move-result v0

    sput v0, Lcom/miui/server/xspace/SecSpaceManagerService;->SECOND_USER_ID:I

    .line 41
    const/16 v2, -0x2710

    if-eq v0, v2, :cond_1

    .line 42
    invoke-static {p0}, Lcom/miui/server/xspace/SecSpaceManagerService;->startSecSpace(Landroid/content/Context;)V

    .line 44
    :cond_1
    invoke-static {p0}, Lcom/miui/server/xspace/SecSpaceManagerService;->getKidSpaceId(Landroid/content/Context;)I

    move-result v0

    sput v0, Lcom/miui/server/xspace/SecSpaceManagerService;->KID_SPACE_ID:I

    .line 45
    if-eq v0, v2, :cond_2

    .line 46
    const-string/jumbo v0, "start KidModeSpaceService"

    invoke-static {v1, v0}, Landroid/util/Slog;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 47
    invoke-static {p0}, Lcom/miui/server/xspace/SecSpaceManagerService;->startKidSpace(Landroid/content/Context;)V

    .line 49
    :cond_2
    invoke-static {p0}, Lcom/miui/server/xspace/SecSpaceManagerService;->registerContentObserver(Landroid/content/Context;)V

    .line 50
    return-void
.end method

.method public static isDataTransferProcess(ILjava/lang/String;)Z
    .locals 1
    .param p0, "userId"    # I
    .param p1, "packageName"    # Ljava/lang/String;

    .line 94
    sget v0, Lcom/miui/server/xspace/SecSpaceManagerService;->SECOND_USER_ID:I

    if-ne p0, v0, :cond_0

    sget-object v0, Lcom/miui/server/xspace/SecSpaceManagerService;->sDataTransferPackageNames:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->contains(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    :goto_0
    return v0
.end method

.method private static registerContentObserver(Landroid/content/Context;)V
    .locals 5
    .param p0, "context"    # Landroid/content/Context;

    .line 72
    new-instance v0, Lcom/miui/server/xspace/SecSpaceManagerService$1;

    const/4 v1, 0x0

    invoke-direct {v0, v1, p0}, Lcom/miui/server/xspace/SecSpaceManagerService$1;-><init>(Landroid/os/Handler;Landroid/content/Context;)V

    .line 78
    .local v0, "contentObserver":Landroid/database/ContentObserver;
    invoke-virtual {p0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v1

    const-string/jumbo v2, "second_user_id"

    invoke-static {v2}, Landroid/provider/Settings$Secure;->getUriFor(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v2

    const/4 v3, 0x1

    const/4 v4, 0x0

    invoke-virtual {v1, v2, v3, v0, v4}, Landroid/content/ContentResolver;->registerContentObserver(Landroid/net/Uri;ZLandroid/database/ContentObserver;I)V

    .line 80
    return-void
.end method

.method private static startKidSpace(Landroid/content/Context;)V
    .locals 4
    .param p0, "context"    # Landroid/content/Context;

    .line 62
    :try_start_0
    new-instance v0, Landroid/content/Intent;

    invoke-direct {v0}, Landroid/content/Intent;-><init>()V

    .line 63
    .local v0, "intent":Landroid/content/Intent;
    new-instance v1, Landroid/content/ComponentName;

    const-string v2, "com.miui.securitycore"

    const-string v3, "com.miui.securityspace.service.KidModeSpaceService"

    invoke-direct {v1, v2, v3}, Landroid/content/ComponentName;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {v0, v1}, Landroid/content/Intent;->setComponent(Landroid/content/ComponentName;)Landroid/content/Intent;

    .line 65
    invoke-virtual {p0, v0}, Landroid/content/Context;->startService(Landroid/content/Intent;)Landroid/content/ComponentName;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 68
    nop

    .end local v0    # "intent":Landroid/content/Intent;
    goto :goto_0

    .line 66
    :catch_0
    move-exception v0

    .line 67
    .local v0, "e":Ljava/lang/Exception;
    const-string v1, "SecSpaceManagerService"

    const-string/jumbo v2, "start KidModeSpaceService"

    invoke-static {v1, v2, v0}, Landroid/util/Slog;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    .line 69
    .end local v0    # "e":Ljava/lang/Exception;
    :goto_0
    return-void
.end method

.method private static startSecSpace(Landroid/content/Context;)V
    .locals 4
    .param p0, "context"    # Landroid/content/Context;

    .line 54
    new-instance v0, Landroid/content/Intent;

    invoke-direct {v0}, Landroid/content/Intent;-><init>()V

    .line 55
    .local v0, "intent":Landroid/content/Intent;
    new-instance v1, Landroid/content/ComponentName;

    const-string v2, "com.miui.securitycore"

    const-string v3, "com.miui.securityspace.service.SecondSpaceService"

    invoke-direct {v1, v2, v3}, Landroid/content/ComponentName;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {v0, v1}, Landroid/content/Intent;->setComponent(Landroid/content/ComponentName;)Landroid/content/Intent;

    .line 57
    invoke-virtual {p0, v0}, Landroid/content/Context;->startService(Landroid/content/Intent;)Landroid/content/ComponentName;

    .line 58
    return-void
.end method
