.class Lcom/miui/server/xspace/BaseUserSwitchingDialog;
.super Landroid/app/AlertDialog;
.source "BaseUserSwitchingDialog.java"


# static fields
.field static final MSG_START_USER:I = 0x1

.field private static final TAG:Ljava/lang/String; = "BaseUserSwitchingDialog"

.field private static final WINDOW_SHOWN_TIMEOUT_MS:I = 0xbb8


# instance fields
.field private final mHandler:Landroid/os/Handler;

.field mOnWindowShownListener:Landroid/view/ViewTreeObserver$OnWindowShownListener;

.field private final mService:Lcom/android/server/am/ActivityManagerService;

.field private mStartedUser:Z

.field protected final mUserId:I


# direct methods
.method public constructor <init>(Lcom/android/server/am/ActivityManagerService;Landroid/content/Context;II)V
    .locals 1
    .param p1, "service"    # Lcom/android/server/am/ActivityManagerService;
    .param p2, "context"    # Landroid/content/Context;
    .param p3, "styleId"    # I
    .param p4, "userId"    # I

    .line 35
    invoke-direct {p0, p2, p3}, Landroid/app/AlertDialog;-><init>(Landroid/content/Context;I)V

    .line 92
    new-instance v0, Lcom/miui/server/xspace/BaseUserSwitchingDialog$2;

    invoke-direct {v0, p0}, Lcom/miui/server/xspace/BaseUserSwitchingDialog$2;-><init>(Lcom/miui/server/xspace/BaseUserSwitchingDialog;)V

    iput-object v0, p0, Lcom/miui/server/xspace/BaseUserSwitchingDialog;->mHandler:Landroid/os/Handler;

    .line 36
    iput-object p1, p0, Lcom/miui/server/xspace/BaseUserSwitchingDialog;->mService:Lcom/android/server/am/ActivityManagerService;

    .line 37
    iput p4, p0, Lcom/miui/server/xspace/BaseUserSwitchingDialog;->mUserId:I

    .line 39
    new-instance v0, Lcom/miui/server/xspace/BaseUserSwitchingDialog$1;

    invoke-direct {v0, p0}, Lcom/miui/server/xspace/BaseUserSwitchingDialog$1;-><init>(Lcom/miui/server/xspace/BaseUserSwitchingDialog;)V

    iput-object v0, p0, Lcom/miui/server/xspace/BaseUserSwitchingDialog;->mOnWindowShownListener:Landroid/view/ViewTreeObserver$OnWindowShownListener;

    .line 45
    return-void
.end method


# virtual methods
.method protected onCreate(Landroid/os/Bundle;)V
    .locals 2
    .param p1, "savedInstanceState"    # Landroid/os/Bundle;

    .line 49
    invoke-super {p0, p1}, Landroid/app/AlertDialog;->onCreate(Landroid/os/Bundle;)V

    .line 50
    const/4 v0, 0x0

    invoke-virtual {p0, v0}, Lcom/miui/server/xspace/BaseUserSwitchingDialog;->setCancelable(Z)V

    .line 51
    invoke-virtual {p0}, Lcom/miui/server/xspace/BaseUserSwitchingDialog;->getWindow()Landroid/view/Window;

    move-result-object v0

    const/16 v1, 0x7da

    invoke-virtual {v0, v1}, Landroid/view/Window;->setType(I)V

    .line 52
    invoke-virtual {p0}, Lcom/miui/server/xspace/BaseUserSwitchingDialog;->getWindow()Landroid/view/Window;

    move-result-object v0

    invoke-virtual {v0}, Landroid/view/Window;->getAttributes()Landroid/view/WindowManager$LayoutParams;

    move-result-object v0

    .line 53
    .local v0, "attrs":Landroid/view/WindowManager$LayoutParams;
    const/16 v1, 0x110

    iput v1, v0, Landroid/view/WindowManager$LayoutParams;->privateFlags:I

    .line 55
    invoke-virtual {p0}, Lcom/miui/server/xspace/BaseUserSwitchingDialog;->getWindow()Landroid/view/Window;

    move-result-object v1

    invoke-virtual {v1, v0}, Landroid/view/Window;->setAttributes(Landroid/view/WindowManager$LayoutParams;)V

    .line 56
    return-void
.end method

.method public show()V
    .locals 5

    .line 60
    invoke-super {p0}, Landroid/app/AlertDialog;->show()V

    .line 61
    invoke-virtual {p0}, Lcom/miui/server/xspace/BaseUserSwitchingDialog;->getWindow()Landroid/view/Window;

    move-result-object v0

    invoke-virtual {v0}, Landroid/view/Window;->getDecorView()Landroid/view/View;

    move-result-object v0

    .line 62
    .local v0, "view":Landroid/view/View;
    if-eqz v0, :cond_0

    .line 63
    invoke-virtual {v0}, Landroid/view/View;->getViewTreeObserver()Landroid/view/ViewTreeObserver;

    move-result-object v1

    iget-object v2, p0, Lcom/miui/server/xspace/BaseUserSwitchingDialog;->mOnWindowShownListener:Landroid/view/ViewTreeObserver$OnWindowShownListener;

    invoke-virtual {v1, v2}, Landroid/view/ViewTreeObserver;->addOnWindowShownListener(Landroid/view/ViewTreeObserver$OnWindowShownListener;)V

    .line 68
    :cond_0
    iget-object v1, p0, Lcom/miui/server/xspace/BaseUserSwitchingDialog;->mHandler:Landroid/os/Handler;

    const/4 v2, 0x1

    invoke-virtual {v1, v2}, Landroid/os/Handler;->obtainMessage(I)Landroid/os/Message;

    move-result-object v2

    const-wide/16 v3, 0xbb8

    invoke-virtual {v1, v2, v3, v4}, Landroid/os/Handler;->sendMessageDelayed(Landroid/os/Message;J)Z

    .line 70
    return-void
.end method

.method startUser()V
    .locals 8

    .line 73
    monitor-enter p0

    .line 74
    :try_start_0
    iget-boolean v0, p0, Lcom/miui/server/xspace/BaseUserSwitchingDialog;->mStartedUser:Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    if-nez v0, :cond_1

    .line 76
    :try_start_1
    iget-object v0, p0, Lcom/miui/server/xspace/BaseUserSwitchingDialog;->mService:Lcom/android/server/am/ActivityManagerService;

    const-string v1, "mUserController"

    invoke-static {v0, v1}, Lcom/miui/server/xspace/ReflectUtil;->getObjectField(Ljava/lang/Object;Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    sget-object v1, Ljava/lang/Void;->TYPE:Ljava/lang/Class;

    const-string/jumbo v2, "startUserInForeground"

    const/4 v3, 0x1

    new-array v4, v3, [Ljava/lang/Class;

    sget-object v5, Ljava/lang/Integer;->TYPE:Ljava/lang/Class;

    const/4 v6, 0x0

    aput-object v5, v4, v6

    new-array v5, v3, [Ljava/lang/Object;

    iget v7, p0, Lcom/miui/server/xspace/BaseUserSwitchingDialog;->mUserId:I

    .line 77
    invoke-static {v7}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v7

    aput-object v7, v5, v6

    .line 76
    invoke-static {v0, v1, v2, v4, v5}, Lcom/miui/server/xspace/ReflectUtil;->callObjectMethod(Ljava/lang/Object;Ljava/lang/Class;Ljava/lang/String;[Ljava/lang/Class;[Ljava/lang/Object;)Ljava/lang/Object;

    .line 78
    iput-boolean v3, p0, Lcom/miui/server/xspace/BaseUserSwitchingDialog;->mStartedUser:Z

    .line 79
    invoke-virtual {p0}, Lcom/miui/server/xspace/BaseUserSwitchingDialog;->getWindow()Landroid/view/Window;

    move-result-object v0

    invoke-virtual {v0}, Landroid/view/Window;->getDecorView()Landroid/view/View;

    move-result-object v0

    .line 80
    .local v0, "view":Landroid/view/View;
    if-eqz v0, :cond_0

    .line 81
    invoke-virtual {v0}, Landroid/view/View;->getViewTreeObserver()Landroid/view/ViewTreeObserver;

    move-result-object v1

    iget-object v2, p0, Lcom/miui/server/xspace/BaseUserSwitchingDialog;->mOnWindowShownListener:Landroid/view/ViewTreeObserver$OnWindowShownListener;

    invoke-virtual {v1, v2}, Landroid/view/ViewTreeObserver;->removeOnWindowShownListener(Landroid/view/ViewTreeObserver$OnWindowShownListener;)V

    .line 84
    :cond_0
    iget-object v1, p0, Lcom/miui/server/xspace/BaseUserSwitchingDialog;->mHandler:Landroid/os/Handler;

    invoke-virtual {v1, v3}, Landroid/os/Handler;->removeMessages(I)V
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_0
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 87
    .end local v0    # "view":Landroid/view/View;
    goto :goto_0

    .line 85
    :catch_0
    move-exception v0

    .line 86
    .local v0, "exception":Ljava/lang/Exception;
    :try_start_2
    const-string v1, "BaseUserSwitchingDialog"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "Call startUserInForeground fail."

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Slog;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 89
    .end local v0    # "exception":Ljava/lang/Exception;
    :cond_1
    :goto_0
    monitor-exit p0

    .line 90
    return-void

    .line 89
    :catchall_0
    move-exception v0

    monitor-exit p0
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    throw v0
.end method
