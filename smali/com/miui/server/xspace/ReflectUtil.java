public class com.miui.server.xspace.ReflectUtil {
	 /* .source "ReflectUtil.java" */
	 /* # direct methods */
	 public com.miui.server.xspace.ReflectUtil ( ) {
		 /* .locals 0 */
		 /* .line 12 */
		 /* invoke-direct {p0}, Ljava/lang/Object;-><init>()V */
		 return;
	 } // .end method
	 public static java.lang.Object callAnyObjectMethod ( java.lang.Class p0, java.lang.Object p1, java.lang.String p2, java.lang.Class[] p3, java.lang.Object...p4 ) {
		 /* .locals 2 */
		 /* .param p1, "target" # Ljava/lang/Object; */
		 /* .param p2, "method" # Ljava/lang/String; */
		 /* .param p4, "values" # [Ljava/lang/Object; */
		 /* .annotation system Ldalvik/annotation/Signature; */
		 /* value = { */
		 /* "(", */
		 /* "Ljava/lang/Class<", */
		 /* "+", */
		 /* "Ljava/lang/Object;", */
		 /* ">;", */
		 /* "Ljava/lang/Object;", */
		 /* "Ljava/lang/String;", */
		 /* "[", */
		 /* "Ljava/lang/Class<", */
		 /* "*>;[", */
		 /* "Ljava/lang/Object;", */
		 /* ")", */
		 /* "Ljava/lang/Object;" */
		 /* } */
	 } // .end annotation
	 /* .annotation system Ldalvik/annotation/Throws; */
	 /* value = { */
	 /* Ljava/lang/NoSuchMethodException;, */
	 /* Ljava/lang/SecurityException;, */
	 /* Ljava/lang/IllegalAccessException;, */
	 /* Ljava/lang/IllegalArgumentException;, */
	 /* Ljava/lang/reflect/InvocationTargetException; */
	 /* } */
} // .end annotation
/* .line 226 */
/* .local p0, "clazz":Ljava/lang/Class;, "Ljava/lang/Class<+Ljava/lang/Object;>;" */
/* .local p3, "parameterTypes":[Ljava/lang/Class;, "[Ljava/lang/Class<*>;" */
(( java.lang.Class ) p0 ).getDeclaredMethod ( p2, p3 ); // invoke-virtual {p0, p2, p3}, Ljava/lang/Class;->getDeclaredMethod(Ljava/lang/String;[Ljava/lang/Class;)Ljava/lang/reflect/Method;
/* .line 227 */
/* .local v0, "declaredMethod":Ljava/lang/reflect/Method; */
int v1 = 1; // const/4 v1, 0x1
(( java.lang.reflect.Method ) v0 ).setAccessible ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/reflect/Method;->setAccessible(Z)V
/* .line 228 */
(( java.lang.reflect.Method ) v0 ).invoke ( p1, p4 ); // invoke-virtual {v0, p1, p4}, Ljava/lang/reflect/Method;->invoke(Ljava/lang/Object;[Ljava/lang/Object;)Ljava/lang/Object;
} // .end method
public static java.lang.Object callObjectMethod ( java.lang.Object p0, java.lang.Class p1, java.lang.String p2, java.lang.Class[] p3, java.lang.Object...p4 ) {
/* .locals 3 */
/* .param p0, "target" # Ljava/lang/Object; */
/* .param p2, "method" # Ljava/lang/String; */
/* .param p4, "values" # [Ljava/lang/Object; */
/* .annotation system Ldalvik/annotation/Signature; */
/* value = { */
/* "<T:", */
/* "Ljava/lang/Object;", */
/* ">(", */
/* "Ljava/lang/Object;", */
/* "Ljava/lang/Class<", */
/* "TT;>;", */
/* "Ljava/lang/String;", */
/* "[", */
/* "Ljava/lang/Class<", */
/* "*>;[", */
/* "Ljava/lang/Object;", */
/* ")TT;" */
/* } */
} // .end annotation
/* .annotation system Ldalvik/annotation/Throws; */
/* value = { */
/* Ljava/lang/NoSuchMethodException;, */
/* Ljava/lang/SecurityException;, */
/* Ljava/lang/IllegalAccessException;, */
/* Ljava/lang/IllegalArgumentException;, */
/* Ljava/lang/reflect/InvocationTargetException; */
/* } */
} // .end annotation
/* .line 30 */
/* .local p1, "returnType":Ljava/lang/Class;, "Ljava/lang/Class<TT;>;" */
/* .local p3, "parameterTypes":[Ljava/lang/Class;, "[Ljava/lang/Class<*>;" */
(( java.lang.Object ) p0 ).getClass ( ); // invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;
/* .line 31 */
/* .local v0, "clazz":Ljava/lang/Class;, "Ljava/lang/Class<+Ljava/lang/Object;>;" */
(( java.lang.Class ) v0 ).getDeclaredMethod ( p2, p3 ); // invoke-virtual {v0, p2, p3}, Ljava/lang/Class;->getDeclaredMethod(Ljava/lang/String;[Ljava/lang/Class;)Ljava/lang/reflect/Method;
/* .line 32 */
/* .local v1, "declaredMethod":Ljava/lang/reflect/Method; */
int v2 = 1; // const/4 v2, 0x1
(( java.lang.reflect.Method ) v1 ).setAccessible ( v2 ); // invoke-virtual {v1, v2}, Ljava/lang/reflect/Method;->setAccessible(Z)V
/* .line 33 */
(( java.lang.reflect.Method ) v1 ).invoke ( p0, p4 ); // invoke-virtual {v1, p0, p4}, Ljava/lang/reflect/Method;->invoke(Ljava/lang/Object;[Ljava/lang/Object;)Ljava/lang/Object;
} // .end method
public static java.lang.Object callObjectMethod ( java.lang.Object p0, java.lang.String p1, java.lang.Class p2, java.lang.Class[] p3, java.lang.Object...p4 ) {
/* .locals 2 */
/* .param p0, "target" # Ljava/lang/Object; */
/* .param p1, "method" # Ljava/lang/String; */
/* .param p4, "values" # [Ljava/lang/Object; */
/* .annotation system Ldalvik/annotation/Signature; */
/* value = { */
/* "(", */
/* "Ljava/lang/Object;", */
/* "Ljava/lang/String;", */
/* "Ljava/lang/Class<", */
/* "*>;[", */
/* "Ljava/lang/Class<", */
/* "*>;[", */
/* "Ljava/lang/Object;", */
/* ")", */
/* "Ljava/lang/Object;" */
/* } */
} // .end annotation
/* .annotation system Ldalvik/annotation/Throws; */
/* value = { */
/* Ljava/lang/NoSuchMethodException;, */
/* Ljava/lang/SecurityException;, */
/* Ljava/lang/IllegalAccessException;, */
/* Ljava/lang/IllegalArgumentException;, */
/* Ljava/lang/reflect/InvocationTargetException; */
/* } */
} // .end annotation
/* .line 61 */
/* .local p2, "clazz":Ljava/lang/Class;, "Ljava/lang/Class<*>;" */
/* .local p3, "parameterTypes":[Ljava/lang/Class;, "[Ljava/lang/Class<*>;" */
(( java.lang.Class ) p2 ).getDeclaredMethod ( p1, p3 ); // invoke-virtual {p2, p1, p3}, Ljava/lang/Class;->getDeclaredMethod(Ljava/lang/String;[Ljava/lang/Class;)Ljava/lang/reflect/Method;
/* .line 62 */
/* .local v0, "declaredMethod":Ljava/lang/reflect/Method; */
int v1 = 1; // const/4 v1, 0x1
(( java.lang.reflect.Method ) v0 ).setAccessible ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/reflect/Method;->setAccessible(Z)V
/* .line 63 */
(( java.lang.reflect.Method ) v0 ).invoke ( p0, p4 ); // invoke-virtual {v0, p0, p4}, Ljava/lang/reflect/Method;->invoke(Ljava/lang/Object;[Ljava/lang/Object;)Ljava/lang/Object;
} // .end method
public static java.lang.Object callObjectMethod ( java.lang.Object p0, java.lang.String p1, java.lang.Class[] p2, java.lang.Object...p3 ) {
/* .locals 3 */
/* .param p0, "target" # Ljava/lang/Object; */
/* .param p1, "method" # Ljava/lang/String; */
/* .param p3, "values" # [Ljava/lang/Object; */
/* .annotation system Ldalvik/annotation/Signature; */
/* value = { */
/* "(", */
/* "Ljava/lang/Object;", */
/* "Ljava/lang/String;", */
/* "[", */
/* "Ljava/lang/Class<", */
/* "*>;[", */
/* "Ljava/lang/Object;", */
/* ")", */
/* "Ljava/lang/Object;" */
/* } */
} // .end annotation
/* .annotation system Ldalvik/annotation/Throws; */
/* value = { */
/* Ljava/lang/NoSuchMethodException;, */
/* Ljava/lang/SecurityException;, */
/* Ljava/lang/IllegalAccessException;, */
/* Ljava/lang/IllegalArgumentException;, */
/* Ljava/lang/reflect/InvocationTargetException; */
/* } */
} // .end annotation
/* .line 44 */
/* .local p2, "parameterTypes":[Ljava/lang/Class;, "[Ljava/lang/Class<*>;" */
(( java.lang.Object ) p0 ).getClass ( ); // invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;
/* .line 45 */
/* .local v0, "clazz":Ljava/lang/Class;, "Ljava/lang/Class<+Ljava/lang/Object;>;" */
(( java.lang.Class ) v0 ).getDeclaredMethod ( p1, p2 ); // invoke-virtual {v0, p1, p2}, Ljava/lang/Class;->getDeclaredMethod(Ljava/lang/String;[Ljava/lang/Class;)Ljava/lang/reflect/Method;
/* .line 46 */
/* .local v1, "declaredMethod":Ljava/lang/reflect/Method; */
(( java.lang.reflect.Method ) v1 ).invoke ( p0, p3 ); // invoke-virtual {v1, p0, p3}, Ljava/lang/reflect/Method;->invoke(Ljava/lang/Object;[Ljava/lang/Object;)Ljava/lang/Object;
} // .end method
public static java.lang.Object callObjectMethod2 ( java.lang.Object p0, java.lang.Class p1, java.lang.String p2, java.lang.Class[] p3, java.lang.Object...p4 ) {
/* .locals 3 */
/* .param p0, "target" # Ljava/lang/Object; */
/* .param p2, "method" # Ljava/lang/String; */
/* .param p4, "values" # [Ljava/lang/Object; */
/* .annotation system Ldalvik/annotation/Signature; */
/* value = { */
/* "<T:", */
/* "Ljava/lang/Object;", */
/* ">(", */
/* "Ljava/lang/Object;", */
/* "Ljava/lang/Class<", */
/* "TT;>;", */
/* "Ljava/lang/String;", */
/* "[", */
/* "Ljava/lang/Class<", */
/* "*>;[", */
/* "Ljava/lang/Object;", */
/* ")TT;" */
/* } */
} // .end annotation
/* .annotation system Ldalvik/annotation/Throws; */
/* value = { */
/* Ljava/lang/NoSuchMethodException;, */
/* Ljava/lang/SecurityException;, */
/* Ljava/lang/IllegalAccessException;, */
/* Ljava/lang/IllegalArgumentException;, */
/* Ljava/lang/reflect/InvocationTargetException; */
/* } */
} // .end annotation
/* .line 37 */
/* .local p1, "returnType":Ljava/lang/Class;, "Ljava/lang/Class<TT;>;" */
/* .local p3, "parameterTypes":[Ljava/lang/Class;, "[Ljava/lang/Class<*>;" */
(( java.lang.Object ) p0 ).getClass ( ); // invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;
/* .line 38 */
/* .local v0, "clazz":Ljava/lang/Class;, "Ljava/lang/Class<+Ljava/lang/Object;>;" */
(( java.lang.Class ) v0 ).getMethod ( p2, p3 ); // invoke-virtual {v0, p2, p3}, Ljava/lang/Class;->getMethod(Ljava/lang/String;[Ljava/lang/Class;)Ljava/lang/reflect/Method;
/* .line 39 */
/* .local v1, "declaredMethod":Ljava/lang/reflect/Method; */
int v2 = 1; // const/4 v2, 0x1
(( java.lang.reflect.Method ) v1 ).setAccessible ( v2 ); // invoke-virtual {v1, v2}, Ljava/lang/reflect/Method;->setAccessible(Z)V
/* .line 40 */
(( java.lang.reflect.Method ) v1 ).invoke ( p0, p4 ); // invoke-virtual {v1, p0, p4}, Ljava/lang/reflect/Method;->invoke(Ljava/lang/Object;[Ljava/lang/Object;)Ljava/lang/Object;
} // .end method
public static java.lang.Object callObjectMethod2 ( java.lang.Object p0, java.lang.String p1, java.lang.Class[] p2, java.lang.Object...p3 ) {
/* .locals 3 */
/* .param p0, "target" # Ljava/lang/Object; */
/* .param p1, "method" # Ljava/lang/String; */
/* .param p3, "values" # [Ljava/lang/Object; */
/* .annotation system Ldalvik/annotation/Signature; */
/* value = { */
/* "(", */
/* "Ljava/lang/Object;", */
/* "Ljava/lang/String;", */
/* "[", */
/* "Ljava/lang/Class<", */
/* "*>;[", */
/* "Ljava/lang/Object;", */
/* ")", */
/* "Ljava/lang/Object;" */
/* } */
} // .end annotation
/* .annotation system Ldalvik/annotation/Throws; */
/* value = { */
/* Ljava/lang/NoSuchMethodException;, */
/* Ljava/lang/SecurityException;, */
/* Ljava/lang/IllegalAccessException;, */
/* Ljava/lang/IllegalArgumentException;, */
/* Ljava/lang/reflect/InvocationTargetException; */
/* } */
} // .end annotation
/* .line 55 */
/* .local p2, "parameterTypes":[Ljava/lang/Class;, "[Ljava/lang/Class<*>;" */
(( java.lang.Object ) p0 ).getClass ( ); // invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;
/* .line 56 */
/* .local v0, "clazz":Ljava/lang/Class;, "Ljava/lang/Class<+Ljava/lang/Object;>;" */
(( java.lang.Class ) v0 ).getMethod ( p1, p2 ); // invoke-virtual {v0, p1, p2}, Ljava/lang/Class;->getMethod(Ljava/lang/String;[Ljava/lang/Class;)Ljava/lang/reflect/Method;
/* .line 57 */
/* .local v1, "getMethod":Ljava/lang/reflect/Method; */
(( java.lang.reflect.Method ) v1 ).invoke ( p0, p3 ); // invoke-virtual {v1, p0, p3}, Ljava/lang/reflect/Method;->invoke(Ljava/lang/Object;[Ljava/lang/Object;)Ljava/lang/Object;
} // .end method
public static java.lang.Object callStaticObjectMethod ( java.lang.Class p0, java.lang.Class p1, java.lang.String p2, java.lang.Class[] p3, java.lang.Object...p4 ) {
/* .locals 2 */
/* .param p2, "method" # Ljava/lang/String; */
/* .param p4, "values" # [Ljava/lang/Object; */
/* .annotation system Ldalvik/annotation/Signature; */
/* value = { */
/* "<T:", */
/* "Ljava/lang/Object;", */
/* ">(", */
/* "Ljava/lang/Class<", */
/* "*>;", */
/* "Ljava/lang/Class<", */
/* "TT;>;", */
/* "Ljava/lang/String;", */
/* "[", */
/* "Ljava/lang/Class<", */
/* "*>;[", */
/* "Ljava/lang/Object;", */
/* ")TT;" */
/* } */
} // .end annotation
/* .annotation system Ldalvik/annotation/Throws; */
/* value = { */
/* Ljava/lang/NoSuchMethodException;, */
/* Ljava/lang/SecurityException;, */
/* Ljava/lang/IllegalAccessException;, */
/* Ljava/lang/IllegalArgumentException;, */
/* Ljava/lang/reflect/InvocationTargetException; */
/* } */
} // .end annotation
/* .line 83 */
/* .local p0, "clazz":Ljava/lang/Class;, "Ljava/lang/Class<*>;" */
/* .local p1, "returnType":Ljava/lang/Class;, "Ljava/lang/Class<TT;>;" */
/* .local p3, "parameterTypes":[Ljava/lang/Class;, "[Ljava/lang/Class<*>;" */
(( java.lang.Class ) p0 ).getDeclaredMethod ( p2, p3 ); // invoke-virtual {p0, p2, p3}, Ljava/lang/Class;->getDeclaredMethod(Ljava/lang/String;[Ljava/lang/Class;)Ljava/lang/reflect/Method;
/* .line 84 */
/* .local v0, "declaredMethod":Ljava/lang/reflect/Method; */
int v1 = 1; // const/4 v1, 0x1
(( java.lang.reflect.Method ) v0 ).setAccessible ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/reflect/Method;->setAccessible(Z)V
/* .line 85 */
int v1 = 0; // const/4 v1, 0x0
(( java.lang.reflect.Method ) v0 ).invoke ( v1, p4 ); // invoke-virtual {v0, v1, p4}, Ljava/lang/reflect/Method;->invoke(Ljava/lang/Object;[Ljava/lang/Object;)Ljava/lang/Object;
} // .end method
public static java.lang.Object callStaticObjectMethod ( java.lang.Class p0, java.lang.String p1, java.lang.Class[] p2, java.lang.Object...p3 ) {
/* .locals 2 */
/* .param p1, "method" # Ljava/lang/String; */
/* .param p3, "values" # [Ljava/lang/Object; */
/* .annotation system Ldalvik/annotation/Signature; */
/* value = { */
/* "(", */
/* "Ljava/lang/Class<", */
/* "*>;", */
/* "Ljava/lang/String;", */
/* "[", */
/* "Ljava/lang/Class<", */
/* "*>;[", */
/* "Ljava/lang/Object;", */
/* ")", */
/* "Ljava/lang/Object;" */
/* } */
} // .end annotation
/* .annotation system Ldalvik/annotation/Throws; */
/* value = { */
/* Ljava/lang/NoSuchMethodException;, */
/* Ljava/lang/SecurityException;, */
/* Ljava/lang/IllegalAccessException;, */
/* Ljava/lang/IllegalArgumentException;, */
/* Ljava/lang/reflect/InvocationTargetException; */
/* } */
} // .end annotation
/* .line 89 */
/* .local p0, "clazz":Ljava/lang/Class;, "Ljava/lang/Class<*>;" */
/* .local p2, "parameterTypes":[Ljava/lang/Class;, "[Ljava/lang/Class<*>;" */
(( java.lang.Class ) p0 ).getDeclaredMethod ( p1, p2 ); // invoke-virtual {p0, p1, p2}, Ljava/lang/Class;->getDeclaredMethod(Ljava/lang/String;[Ljava/lang/Class;)Ljava/lang/reflect/Method;
/* .line 90 */
/* .local v0, "declaredMethod":Ljava/lang/reflect/Method; */
int v1 = 1; // const/4 v1, 0x1
(( java.lang.reflect.Method ) v0 ).setAccessible ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/reflect/Method;->setAccessible(Z)V
/* .line 91 */
int v1 = 0; // const/4 v1, 0x0
(( java.lang.reflect.Method ) v0 ).invoke ( v1, p3 ); // invoke-virtual {v0, v1, p3}, Ljava/lang/reflect/Method;->invoke(Ljava/lang/Object;[Ljava/lang/Object;)Ljava/lang/Object;
} // .end method
public static java.lang.Object getObjectField ( java.lang.Object p0, java.lang.Class p1, java.lang.String p2 ) {
/* .locals 2 */
/* .param p0, "target" # Ljava/lang/Object; */
/* .param p2, "field" # Ljava/lang/String; */
/* .annotation system Ldalvik/annotation/Signature; */
/* value = { */
/* "(", */
/* "Ljava/lang/Object;", */
/* "Ljava/lang/Class<", */
/* "*>;", */
/* "Ljava/lang/String;", */
/* ")", */
/* "Ljava/lang/Object;" */
/* } */
} // .end annotation
/* .annotation system Ldalvik/annotation/Throws; */
/* value = { */
/* Ljava/lang/NoSuchFieldException;, */
/* Ljava/lang/SecurityException;, */
/* Ljava/lang/IllegalArgumentException;, */
/* Ljava/lang/IllegalAccessException; */
/* } */
} // .end annotation
/* .line 163 */
/* .local p1, "clazz":Ljava/lang/Class;, "Ljava/lang/Class<*>;" */
(( java.lang.Class ) p1 ).getDeclaredField ( p2 ); // invoke-virtual {p1, p2}, Ljava/lang/Class;->getDeclaredField(Ljava/lang/String;)Ljava/lang/reflect/Field;
/* .line 164 */
/* .local v0, "declaredField":Ljava/lang/reflect/Field; */
int v1 = 1; // const/4 v1, 0x1
(( java.lang.reflect.Field ) v0 ).setAccessible ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/reflect/Field;->setAccessible(Z)V
/* .line 165 */
(( java.lang.reflect.Field ) v0 ).get ( p0 ); // invoke-virtual {v0, p0}, Ljava/lang/reflect/Field;->get(Ljava/lang/Object;)Ljava/lang/Object;
} // .end method
public static java.lang.Object getObjectField ( java.lang.Object p0, java.lang.String p1 ) {
/* .locals 3 */
/* .param p0, "target" # Ljava/lang/Object; */
/* .param p1, "field" # Ljava/lang/String; */
/* .annotation system Ldalvik/annotation/Throws; */
/* value = { */
/* Ljava/lang/NoSuchFieldException;, */
/* Ljava/lang/SecurityException;, */
/* Ljava/lang/IllegalArgumentException;, */
/* Ljava/lang/IllegalAccessException; */
/* } */
} // .end annotation
/* .line 132 */
(( java.lang.Object ) p0 ).getClass ( ); // invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;
/* .line 133 */
/* .local v0, "clazz":Ljava/lang/Class;, "Ljava/lang/Class<+Ljava/lang/Object;>;" */
(( java.lang.Class ) v0 ).getDeclaredField ( p1 ); // invoke-virtual {v0, p1}, Ljava/lang/Class;->getDeclaredField(Ljava/lang/String;)Ljava/lang/reflect/Field;
/* .line 134 */
/* .local v1, "declaredField":Ljava/lang/reflect/Field; */
int v2 = 1; // const/4 v2, 0x1
(( java.lang.reflect.Field ) v1 ).setAccessible ( v2 ); // invoke-virtual {v1, v2}, Ljava/lang/reflect/Field;->setAccessible(Z)V
/* .line 135 */
(( java.lang.reflect.Field ) v1 ).get ( p0 ); // invoke-virtual {v1, p0}, Ljava/lang/reflect/Field;->get(Ljava/lang/Object;)Ljava/lang/Object;
} // .end method
public static java.lang.Object getObjectField ( java.lang.Object p0, java.lang.String p1, java.lang.Class p2 ) {
/* .locals 3 */
/* .param p0, "target" # Ljava/lang/Object; */
/* .param p1, "field" # Ljava/lang/String; */
/* .annotation system Ldalvik/annotation/Signature; */
/* value = { */
/* "<T:", */
/* "Ljava/lang/Object;", */
/* ">(", */
/* "Ljava/lang/Object;", */
/* "Ljava/lang/String;", */
/* "Ljava/lang/Class<", */
/* "TT;>;)TT;" */
/* } */
} // .end annotation
/* .annotation system Ldalvik/annotation/Throws; */
/* value = { */
/* Ljava/lang/NoSuchFieldException;, */
/* Ljava/lang/SecurityException;, */
/* Ljava/lang/IllegalArgumentException;, */
/* Ljava/lang/IllegalAccessException; */
/* } */
} // .end annotation
/* .line 156 */
/* .local p2, "returnType":Ljava/lang/Class;, "Ljava/lang/Class<TT;>;" */
(( java.lang.Object ) p0 ).getClass ( ); // invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;
/* .line 157 */
/* .local v0, "clazz":Ljava/lang/Class;, "Ljava/lang/Class<+Ljava/lang/Object;>;" */
(( java.lang.Class ) v0 ).getDeclaredField ( p1 ); // invoke-virtual {v0, p1}, Ljava/lang/Class;->getDeclaredField(Ljava/lang/String;)Ljava/lang/reflect/Field;
/* .line 158 */
/* .local v1, "declaredField":Ljava/lang/reflect/Field; */
int v2 = 1; // const/4 v2, 0x1
(( java.lang.reflect.Field ) v1 ).setAccessible ( v2 ); // invoke-virtual {v1, v2}, Ljava/lang/reflect/Field;->setAccessible(Z)V
/* .line 159 */
(( java.lang.reflect.Field ) v1 ).get ( p0 ); // invoke-virtual {v1, p0}, Ljava/lang/reflect/Field;->get(Ljava/lang/Object;)Ljava/lang/Object;
} // .end method
public static java.lang.Object getObjectField2 ( java.lang.Object p0, java.lang.String p1 ) {
/* .locals 3 */
/* .param p0, "target" # Ljava/lang/Object; */
/* .param p1, "field" # Ljava/lang/String; */
/* .annotation system Ldalvik/annotation/Throws; */
/* value = { */
/* Ljava/lang/NoSuchFieldException;, */
/* Ljava/lang/SecurityException;, */
/* Ljava/lang/IllegalArgumentException;, */
/* Ljava/lang/IllegalAccessException; */
/* } */
} // .end annotation
/* .line 149 */
(( java.lang.Object ) p0 ).getClass ( ); // invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;
/* .line 150 */
/* .local v0, "clazz":Ljava/lang/Class;, "Ljava/lang/Class<+Ljava/lang/Object;>;" */
(( java.lang.Class ) v0 ).getField ( p1 ); // invoke-virtual {v0, p1}, Ljava/lang/Class;->getField(Ljava/lang/String;)Ljava/lang/reflect/Field;
/* .line 151 */
/* .local v1, "declaredField":Ljava/lang/reflect/Field; */
int v2 = 1; // const/4 v2, 0x1
(( java.lang.reflect.Field ) v1 ).setAccessible ( v2 ); // invoke-virtual {v1, v2}, Ljava/lang/reflect/Field;->setAccessible(Z)V
/* .line 152 */
(( java.lang.reflect.Field ) v1 ).get ( p0 ); // invoke-virtual {v1, p0}, Ljava/lang/reflect/Field;->get(Ljava/lang/Object;)Ljava/lang/Object;
} // .end method
public static java.lang.Object getStaticObjectField ( java.lang.Class p0, java.lang.String p1 ) {
/* .locals 2 */
/* .param p1, "field" # Ljava/lang/String; */
/* .annotation system Ldalvik/annotation/Signature; */
/* value = { */
/* "(", */
/* "Ljava/lang/Class<", */
/* "*>;", */
/* "Ljava/lang/String;", */
/* ")", */
/* "Ljava/lang/Object;" */
/* } */
} // .end annotation
/* .annotation system Ldalvik/annotation/Throws; */
/* value = { */
/* Ljava/lang/NoSuchFieldException;, */
/* Ljava/lang/SecurityException;, */
/* Ljava/lang/IllegalArgumentException;, */
/* Ljava/lang/IllegalAccessException; */
/* } */
} // .end annotation
/* .line 197 */
/* .local p0, "clazz":Ljava/lang/Class;, "Ljava/lang/Class<*>;" */
(( java.lang.Class ) p0 ).getDeclaredField ( p1 ); // invoke-virtual {p0, p1}, Ljava/lang/Class;->getDeclaredField(Ljava/lang/String;)Ljava/lang/reflect/Field;
/* .line 198 */
/* .local v0, "declaredField":Ljava/lang/reflect/Field; */
int v1 = 1; // const/4 v1, 0x1
(( java.lang.reflect.Field ) v0 ).setAccessible ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/reflect/Field;->setAccessible(Z)V
/* .line 199 */
int v1 = 0; // const/4 v1, 0x0
(( java.lang.reflect.Field ) v0 ).get ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/reflect/Field;->get(Ljava/lang/Object;)Ljava/lang/Object;
} // .end method
public static java.lang.Object getStaticObjectField ( java.lang.Class p0, java.lang.String p1, java.lang.Class p2 ) {
/* .locals 2 */
/* .param p1, "field" # Ljava/lang/String; */
/* .annotation system Ldalvik/annotation/Signature; */
/* value = { */
/* "<T:", */
/* "Ljava/lang/Object;", */
/* ">(", */
/* "Ljava/lang/Class<", */
/* "*>;", */
/* "Ljava/lang/String;", */
/* "Ljava/lang/Class<", */
/* "TT;>;)TT;" */
/* } */
} // .end annotation
/* .annotation system Ldalvik/annotation/Throws; */
/* value = { */
/* Ljava/lang/NoSuchFieldException;, */
/* Ljava/lang/SecurityException;, */
/* Ljava/lang/IllegalArgumentException;, */
/* Ljava/lang/IllegalAccessException; */
/* } */
} // .end annotation
/* .line 204 */
/* .local p0, "clazz":Ljava/lang/Class;, "Ljava/lang/Class<*>;" */
/* .local p2, "returnType":Ljava/lang/Class;, "Ljava/lang/Class<TT;>;" */
(( java.lang.Class ) p0 ).getDeclaredField ( p1 ); // invoke-virtual {p0, p1}, Ljava/lang/Class;->getDeclaredField(Ljava/lang/String;)Ljava/lang/reflect/Field;
/* .line 205 */
/* .local v0, "declaredField":Ljava/lang/reflect/Field; */
int v1 = 1; // const/4 v1, 0x1
(( java.lang.reflect.Field ) v0 ).setAccessible ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/reflect/Field;->setAccessible(Z)V
/* .line 206 */
int v1 = 0; // const/4 v1, 0x0
(( java.lang.reflect.Field ) v0 ).get ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/reflect/Field;->get(Ljava/lang/Object;)Ljava/lang/Object;
} // .end method
public static void setObjectField ( java.lang.Object p0, java.lang.Class p1, java.lang.String p2, java.lang.Object p3 ) {
/* .locals 2 */
/* .param p0, "target" # Ljava/lang/Object; */
/* .param p2, "field" # Ljava/lang/String; */
/* .param p3, "value" # Ljava/lang/Object; */
/* .annotation system Ldalvik/annotation/Signature; */
/* value = { */
/* "(", */
/* "Ljava/lang/Object;", */
/* "Ljava/lang/Class<", */
/* "*>;", */
/* "Ljava/lang/String;", */
/* "Ljava/lang/Object;", */
/* ")V" */
/* } */
} // .end annotation
/* .annotation system Ldalvik/annotation/Throws; */
/* value = { */
/* Ljava/lang/NoSuchFieldException;, */
/* Ljava/lang/SecurityException;, */
/* Ljava/lang/IllegalArgumentException;, */
/* Ljava/lang/IllegalAccessException; */
/* } */
} // .end annotation
/* .line 114 */
/* .local p1, "clazz":Ljava/lang/Class;, "Ljava/lang/Class<*>;" */
(( java.lang.Class ) p1 ).getDeclaredField ( p2 ); // invoke-virtual {p1, p2}, Ljava/lang/Class;->getDeclaredField(Ljava/lang/String;)Ljava/lang/reflect/Field;
/* .line 115 */
/* .local v0, "declaredField":Ljava/lang/reflect/Field; */
int v1 = 1; // const/4 v1, 0x1
(( java.lang.reflect.Field ) v0 ).setAccessible ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/reflect/Field;->setAccessible(Z)V
/* .line 116 */
(( java.lang.reflect.Field ) v0 ).set ( p0, p3 ); // invoke-virtual {v0, p0, p3}, Ljava/lang/reflect/Field;->set(Ljava/lang/Object;Ljava/lang/Object;)V
/* .line 117 */
return;
} // .end method
public static void setObjectField ( java.lang.Object p0, java.lang.String p1, java.lang.Object p2 ) {
/* .locals 3 */
/* .param p0, "target" # Ljava/lang/Object; */
/* .param p1, "field" # Ljava/lang/String; */
/* .param p2, "value" # Ljava/lang/Object; */
/* .annotation system Ldalvik/annotation/Throws; */
/* value = { */
/* Ljava/lang/NoSuchFieldException;, */
/* Ljava/lang/SecurityException;, */
/* Ljava/lang/IllegalArgumentException;, */
/* Ljava/lang/IllegalAccessException; */
/* } */
} // .end annotation
/* .line 106 */
(( java.lang.Object ) p0 ).getClass ( ); // invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;
/* .line 107 */
/* .local v0, "clazz":Ljava/lang/Class;, "Ljava/lang/Class<+Ljava/lang/Object;>;" */
(( java.lang.Class ) v0 ).getDeclaredField ( p1 ); // invoke-virtual {v0, p1}, Ljava/lang/Class;->getDeclaredField(Ljava/lang/String;)Ljava/lang/reflect/Field;
/* .line 108 */
/* .local v1, "declaredField":Ljava/lang/reflect/Field; */
int v2 = 1; // const/4 v2, 0x1
(( java.lang.reflect.Field ) v1 ).setAccessible ( v2 ); // invoke-virtual {v1, v2}, Ljava/lang/reflect/Field;->setAccessible(Z)V
/* .line 109 */
(( java.lang.reflect.Field ) v1 ).set ( p0, p2 ); // invoke-virtual {v1, p0, p2}, Ljava/lang/reflect/Field;->set(Ljava/lang/Object;Ljava/lang/Object;)V
/* .line 110 */
return;
} // .end method
public static void setStaticObjectField ( java.lang.Class p0, java.lang.String p1, java.lang.Object p2 ) {
/* .locals 2 */
/* .param p1, "field" # Ljava/lang/String; */
/* .param p2, "value" # Ljava/lang/Object; */
/* .annotation system Ldalvik/annotation/Signature; */
/* value = { */
/* "(", */
/* "Ljava/lang/Class<", */
/* "*>;", */
/* "Ljava/lang/String;", */
/* "Ljava/lang/Object;", */
/* ")V" */
/* } */
} // .end annotation
/* .annotation system Ldalvik/annotation/Throws; */
/* value = { */
/* Ljava/lang/NoSuchFieldException;, */
/* Ljava/lang/SecurityException;, */
/* Ljava/lang/IllegalArgumentException;, */
/* Ljava/lang/IllegalAccessException; */
/* } */
} // .end annotation
/* .line 180 */
/* .local p0, "clazz":Ljava/lang/Class;, "Ljava/lang/Class<*>;" */
(( java.lang.Class ) p0 ).getDeclaredField ( p1 ); // invoke-virtual {p0, p1}, Ljava/lang/Class;->getDeclaredField(Ljava/lang/String;)Ljava/lang/reflect/Field;
/* .line 181 */
/* .local v0, "declaredField":Ljava/lang/reflect/Field; */
int v1 = 1; // const/4 v1, 0x1
(( java.lang.reflect.Field ) v0 ).setAccessible ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/reflect/Field;->setAccessible(Z)V
/* .line 182 */
int v1 = 0; // const/4 v1, 0x0
(( java.lang.reflect.Field ) v0 ).set ( v1, p2 ); // invoke-virtual {v0, v1, p2}, Ljava/lang/reflect/Field;->set(Ljava/lang/Object;Ljava/lang/Object;)V
/* .line 183 */
return;
} // .end method
