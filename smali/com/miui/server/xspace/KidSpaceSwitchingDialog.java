public class com.miui.server.xspace.KidSpaceSwitchingDialog extends com.miui.server.xspace.BaseUserSwitchingDialog {
	 /* .source "KidSpaceSwitchingDialog.java" */
	 /* # direct methods */
	 public com.miui.server.xspace.KidSpaceSwitchingDialog ( ) {
		 /* .locals 1 */
		 /* .param p1, "service" # Lcom/android/server/am/ActivityManagerService; */
		 /* .param p2, "context" # Landroid/content/Context; */
		 /* .param p3, "userId" # I */
		 /* .line 30 */
		 /* const v0, 0x11100009 */
		 /* invoke-direct {p0, p1, p2, v0, p3}, Lcom/miui/server/xspace/BaseUserSwitchingDialog;-><init>(Lcom/android/server/am/ActivityManagerService;Landroid/content/Context;II)V */
		 /* .line 31 */
		 return;
	 } // .end method
	 /* # virtual methods */
	 protected void onCreate ( android.os.Bundle p0 ) {
		 /* .locals 6 */
		 /* .param p1, "savedInstanceState" # Landroid/os/Bundle; */
		 /* .line 35 */
		 /* invoke-super {p0, p1}, Lcom/miui/server/xspace/BaseUserSwitchingDialog;->onCreate(Landroid/os/Bundle;)V */
		 /* .line 36 */
		 (( com.miui.server.xspace.KidSpaceSwitchingDialog ) p0 ).getWindow ( ); // invoke-virtual {p0}, Lcom/miui/server/xspace/KidSpaceSwitchingDialog;->getWindow()Landroid/view/Window;
		 /* .line 37 */
		 /* .local v0, "win":Landroid/view/Window; */
		 (( android.view.Window ) v0 ).getDecorView ( ); // invoke-virtual {v0}, Landroid/view/Window;->getDecorView()Landroid/view/View;
		 int v2 = 0; // const/4 v2, 0x0
		 (( android.view.View ) v1 ).setPadding ( v2, v2, v2, v2 ); // invoke-virtual {v1, v2, v2, v2, v2}, Landroid/view/View;->setPadding(IIII)V
		 /* .line 38 */
		 (( android.view.Window ) v0 ).getAttributes ( ); // invoke-virtual {v0}, Landroid/view/Window;->getAttributes()Landroid/view/WindowManager$LayoutParams;
		 /* .line 39 */
		 /* .local v1, "lp":Landroid/view/WindowManager$LayoutParams; */
		 int v3 = -1; // const/4 v3, -0x1
		 /* iput v3, v1, Landroid/view/WindowManager$LayoutParams;->width:I */
		 /* .line 40 */
		 /* iput v3, v1, Landroid/view/WindowManager$LayoutParams;->height:I */
		 /* .line 41 */
		 int v3 = 1; // const/4 v3, 0x1
		 /* iput v3, v1, Landroid/view/WindowManager$LayoutParams;->layoutInDisplayCutoutMode:I */
		 /* .line 43 */
		 /* iget v4, p0, Lcom/miui/server/xspace/KidSpaceSwitchingDialog;->mUserId:I */
		 if ( v4 != null) { // if-eqz v4, :cond_0
			 /* move v4, v3 */
		 } // :cond_0
		 /* move v4, v2 */
		 /* .line 45 */
		 /* .local v4, "isEnterKidMode":Z */
	 } // :goto_0
	 if ( v4 != null) { // if-eqz v4, :cond_1
		 /* .line 46 */
		 /* .line 47 */
	 } // :cond_1
	 /* move v2, v3 */
} // :goto_1
/* iput v2, v1, Landroid/view/WindowManager$LayoutParams;->screenOrientation:I */
/* .line 49 */
(( android.view.Window ) v0 ).setAttributes ( v1 ); // invoke-virtual {v0, v1}, Landroid/view/Window;->setAttributes(Landroid/view/WindowManager$LayoutParams;)V
/* .line 51 */
(( android.view.Window ) v0 ).getDecorView ( ); // invoke-virtual {v0}, Landroid/view/Window;->getDecorView()Landroid/view/View;
/* const/16 v3, 0xf06 */
(( android.view.View ) v2 ).setSystemUiVisibility ( v3 ); // invoke-virtual {v2, v3}, Landroid/view/View;->setSystemUiVisibility(I)V
/* .line 57 */
(( com.miui.server.xspace.KidSpaceSwitchingDialog ) p0 ).getContext ( ); // invoke-virtual {p0}, Lcom/miui/server/xspace/KidSpaceSwitchingDialog;->getContext()Landroid/content/Context;
android.view.LayoutInflater .from ( v2 );
/* const v3, 0x110c001d */
int v5 = 0; // const/4 v5, 0x0
(( android.view.LayoutInflater ) v2 ).inflate ( v3, v5 ); // invoke-virtual {v2, v3, v5}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;
/* .line 58 */
/* .local v2, "view":Landroid/view/View; */
/* const v3, 0x110a0079 */
(( android.view.View ) v2 ).findViewById ( v3 ); // invoke-virtual {v2, v3}, Landroid/view/View;->findViewById(I)Landroid/view/View;
/* check-cast v3, Landroid/widget/ImageView; */
/* .line 60 */
/* .local v3, "imageView":Landroid/widget/ImageView; */
if ( v4 != null) { // if-eqz v4, :cond_2
	 /* .line 61 */
	 /* const v5, 0x11080193 */
	 /* .line 62 */
} // :cond_2
/* const v5, 0x11080194 */
/* .line 60 */
} // :goto_2
(( android.widget.ImageView ) v3 ).setImageResource ( v5 ); // invoke-virtual {v3, v5}, Landroid/widget/ImageView;->setImageResource(I)V
/* .line 64 */
(( com.miui.server.xspace.KidSpaceSwitchingDialog ) p0 ).setContentView ( v2 ); // invoke-virtual {p0, v2}, Lcom/miui/server/xspace/KidSpaceSwitchingDialog;->setContentView(Landroid/view/View;)V
/* .line 65 */
return;
} // .end method
public void show ( ) { //bridge//synthethic
/* .locals 0 */
/* .line 27 */
/* invoke-super {p0}, Lcom/miui/server/xspace/BaseUserSwitchingDialog;->show()V */
return;
} // .end method
