.class public final Lcom/miui/server/xspace/XSpaceManagerServiceImpl$Provider;
.super Ljava/lang/Object;
.source "XSpaceManagerServiceImpl$Provider.java"

# interfaces
.implements Lcom/miui/base/MiuiStubRegistry$ImplProvider;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/miui/server/xspace/XSpaceManagerServiceImpl$Provider$SINGLETON;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Lcom/miui/base/MiuiStubRegistry$ImplProvider<",
        "Lcom/miui/server/xspace/XSpaceManagerServiceImpl;",
        ">;"
    }
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 0

    .line 11
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public provideNewInstance()Lcom/miui/server/xspace/XSpaceManagerServiceImpl;
    .locals 1

    .line 17
    new-instance v0, Lcom/miui/server/xspace/XSpaceManagerServiceImpl;

    invoke-direct {v0}, Lcom/miui/server/xspace/XSpaceManagerServiceImpl;-><init>()V

    return-object v0
.end method

.method public bridge synthetic provideNewInstance()Ljava/lang/Object;
    .locals 1

    .line 10
    invoke-virtual {p0}, Lcom/miui/server/xspace/XSpaceManagerServiceImpl$Provider;->provideNewInstance()Lcom/miui/server/xspace/XSpaceManagerServiceImpl;

    move-result-object v0

    return-object v0
.end method

.method public provideSingleton()Lcom/miui/server/xspace/XSpaceManagerServiceImpl;
    .locals 1

    .line 13
    sget-object v0, Lcom/miui/server/xspace/XSpaceManagerServiceImpl$Provider$SINGLETON;->INSTANCE:Lcom/miui/server/xspace/XSpaceManagerServiceImpl;

    return-object v0
.end method

.method public bridge synthetic provideSingleton()Ljava/lang/Object;
    .locals 1

    .line 10
    invoke-virtual {p0}, Lcom/miui/server/xspace/XSpaceManagerServiceImpl$Provider;->provideSingleton()Lcom/miui/server/xspace/XSpaceManagerServiceImpl;

    move-result-object v0

    return-object v0
.end method
