class com.miui.server.xspace.SecondSpaceSwitchingDialog extends com.miui.server.xspace.BaseUserSwitchingDialog {
	 /* .source "SecondSpaceSwitchingDialog.java" */
	 /* # direct methods */
	 public com.miui.server.xspace.SecondSpaceSwitchingDialog ( ) {
		 /* .locals 1 */
		 /* .param p1, "service" # Lcom/android/server/am/ActivityManagerService; */
		 /* .param p2, "context" # Landroid/content/Context; */
		 /* .param p3, "userId" # I */
		 /* .line 24 */
		 /* const v0, 0x1110001b */
		 /* invoke-direct {p0, p1, p2, v0, p3}, Lcom/miui/server/xspace/BaseUserSwitchingDialog;-><init>(Lcom/android/server/am/ActivityManagerService;Landroid/content/Context;II)V */
		 /* .line 25 */
		 return;
	 } // .end method
	 /* # virtual methods */
	 protected void onCreate ( android.os.Bundle p0 ) {
		 /* .locals 5 */
		 /* .param p1, "savedInstanceState" # Landroid/os/Bundle; */
		 /* .line 28 */
		 /* invoke-super {p0, p1}, Lcom/miui/server/xspace/BaseUserSwitchingDialog;->onCreate(Landroid/os/Bundle;)V */
		 /* .line 29 */
		 (( com.miui.server.xspace.SecondSpaceSwitchingDialog ) p0 ).getWindow ( ); // invoke-virtual {p0}, Lcom/miui/server/xspace/SecondSpaceSwitchingDialog;->getWindow()Landroid/view/Window;
		 /* .line 30 */
		 /* .local v0, "win":Landroid/view/Window; */
		 (( android.view.Window ) v0 ).getDecorView ( ); // invoke-virtual {v0}, Landroid/view/Window;->getDecorView()Landroid/view/View;
		 int v2 = 0; // const/4 v2, 0x0
		 (( android.view.View ) v1 ).setPadding ( v2, v2, v2, v2 ); // invoke-virtual {v1, v2, v2, v2, v2}, Landroid/view/View;->setPadding(IIII)V
		 /* .line 31 */
		 (( android.view.Window ) v0 ).getAttributes ( ); // invoke-virtual {v0}, Landroid/view/Window;->getAttributes()Landroid/view/WindowManager$LayoutParams;
		 /* .line 32 */
		 /* .local v1, "lp":Landroid/view/WindowManager$LayoutParams; */
		 int v2 = -1; // const/4 v2, -0x1
		 /* iput v2, v1, Landroid/view/WindowManager$LayoutParams;->width:I */
		 /* .line 33 */
		 /* iput v2, v1, Landroid/view/WindowManager$LayoutParams;->height:I */
		 /* .line 34 */
		 int v2 = 1; // const/4 v2, 0x1
		 /* iput v2, v1, Landroid/view/WindowManager$LayoutParams;->layoutInDisplayCutoutMode:I */
		 /* .line 35 */
		 (( android.view.Window ) v0 ).setAttributes ( v1 ); // invoke-virtual {v0, v1}, Landroid/view/Window;->setAttributes(Landroid/view/WindowManager$LayoutParams;)V
		 /* .line 37 */
		 (( android.view.Window ) v0 ).getDecorView ( ); // invoke-virtual {v0}, Landroid/view/Window;->getDecorView()Landroid/view/View;
		 /* const/16 v3, 0xf06 */
		 (( android.view.View ) v2 ).setSystemUiVisibility ( v3 ); // invoke-virtual {v2, v3}, Landroid/view/View;->setSystemUiVisibility(I)V
		 /* .line 43 */
		 (( com.miui.server.xspace.SecondSpaceSwitchingDialog ) p0 ).getContext ( ); // invoke-virtual {p0}, Lcom/miui/server/xspace/SecondSpaceSwitchingDialog;->getContext()Landroid/content/Context;
		 android.view.LayoutInflater .from ( v2 );
		 /* const v3, 0x110c0044 */
		 int v4 = 0; // const/4 v4, 0x0
		 (( android.view.LayoutInflater ) v2 ).inflate ( v3, v4 ); // invoke-virtual {v2, v3, v4}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;
		 /* .line 44 */
		 /* .local v2, "view":Landroid/view/View; */
		 (( com.miui.server.xspace.SecondSpaceSwitchingDialog ) p0 ).setContentView ( v2 ); // invoke-virtual {p0, v2}, Lcom/miui/server/xspace/SecondSpaceSwitchingDialog;->setContentView(Landroid/view/View;)V
		 /* .line 45 */
		 return;
	 } // .end method
