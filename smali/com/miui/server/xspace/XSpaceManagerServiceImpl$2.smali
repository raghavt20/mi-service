.class Lcom/miui/server/xspace/XSpaceManagerServiceImpl$2;
.super Ljava/lang/Object;
.source "XSpaceManagerServiceImpl.java"

# interfaces
.implements Ljava/lang/Runnable;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/miui/server/xspace/XSpaceManagerServiceImpl;->showSwitchingDialog(Lcom/android/server/am/ActivityManagerService;Landroid/content/Context;IILandroid/os/Handler;I)Z
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/miui/server/xspace/XSpaceManagerServiceImpl;

.field final synthetic val$ams:Lcom/android/server/am/ActivityManagerService;

.field final synthetic val$context:Landroid/content/Context;

.field final synthetic val$currentUserId:I

.field final synthetic val$handler:Landroid/os/Handler;

.field final synthetic val$targetUserId:I

.field final synthetic val$userSwitchUiMessage:I


# direct methods
.method constructor <init>(Lcom/miui/server/xspace/XSpaceManagerServiceImpl;Landroid/content/Context;IILcom/android/server/am/ActivityManagerService;Landroid/os/Handler;I)V
    .locals 0
    .param p1, "this$0"    # Lcom/miui/server/xspace/XSpaceManagerServiceImpl;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .line 798
    iput-object p1, p0, Lcom/miui/server/xspace/XSpaceManagerServiceImpl$2;->this$0:Lcom/miui/server/xspace/XSpaceManagerServiceImpl;

    iput-object p2, p0, Lcom/miui/server/xspace/XSpaceManagerServiceImpl$2;->val$context:Landroid/content/Context;

    iput p3, p0, Lcom/miui/server/xspace/XSpaceManagerServiceImpl$2;->val$targetUserId:I

    iput p4, p0, Lcom/miui/server/xspace/XSpaceManagerServiceImpl$2;->val$currentUserId:I

    iput-object p5, p0, Lcom/miui/server/xspace/XSpaceManagerServiceImpl$2;->val$ams:Lcom/android/server/am/ActivityManagerService;

    iput-object p6, p0, Lcom/miui/server/xspace/XSpaceManagerServiceImpl$2;->val$handler:Landroid/os/Handler;

    iput p7, p0, Lcom/miui/server/xspace/XSpaceManagerServiceImpl$2;->val$userSwitchUiMessage:I

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public run()V
    .locals 7

    .line 800
    iget-object v0, p0, Lcom/miui/server/xspace/XSpaceManagerServiceImpl$2;->val$context:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    const-string/jumbo v1, "second_user_id"

    const/16 v2, -0x2710

    const/4 v3, 0x0

    invoke-static {v0, v1, v2, v3}, Landroid/provider/Settings$Secure;->getIntForUser(Landroid/content/ContentResolver;Ljava/lang/String;II)I

    move-result v0

    .line 802
    .local v0, "secondSpaceId":I
    iget-object v1, p0, Lcom/miui/server/xspace/XSpaceManagerServiceImpl$2;->val$context:Landroid/content/Context;

    invoke-virtual {v1}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v1

    const-string v4, "kid_user_id"

    invoke-static {v1, v4, v2, v3}, Landroid/provider/Settings$Secure;->getIntForUser(Landroid/content/ContentResolver;Ljava/lang/String;II)I

    move-result v1

    .line 804
    .local v1, "kidSpaceUserId":I
    iget v2, p0, Lcom/miui/server/xspace/XSpaceManagerServiceImpl$2;->val$targetUserId:I

    if-ne v2, v0, :cond_0

    iget v3, p0, Lcom/miui/server/xspace/XSpaceManagerServiceImpl$2;->val$currentUserId:I

    if-eqz v3, :cond_1

    :cond_0
    iget v3, p0, Lcom/miui/server/xspace/XSpaceManagerServiceImpl$2;->val$currentUserId:I

    if-ne v3, v0, :cond_2

    if-nez v2, :cond_2

    .line 806
    :cond_1
    new-instance v2, Lcom/miui/server/xspace/SecondSpaceSwitchingDialog;

    iget-object v3, p0, Lcom/miui/server/xspace/XSpaceManagerServiceImpl$2;->val$ams:Lcom/android/server/am/ActivityManagerService;

    iget-object v4, p0, Lcom/miui/server/xspace/XSpaceManagerServiceImpl$2;->val$context:Landroid/content/Context;

    iget v5, p0, Lcom/miui/server/xspace/XSpaceManagerServiceImpl$2;->val$targetUserId:I

    invoke-direct {v2, v3, v4, v5}, Lcom/miui/server/xspace/SecondSpaceSwitchingDialog;-><init>(Lcom/android/server/am/ActivityManagerService;Landroid/content/Context;I)V

    invoke-static {v2}, Lcom/miui/server/xspace/XSpaceManagerServiceImpl;->-$$Nest$sfputmDialog(Lcom/miui/server/xspace/BaseUserSwitchingDialog;)V

    .line 807
    invoke-static {}, Lcom/miui/server/xspace/XSpaceManagerServiceImpl;->-$$Nest$sfgetmDialog()Lcom/miui/server/xspace/BaseUserSwitchingDialog;

    move-result-object v2

    invoke-virtual {v2}, Lcom/miui/server/xspace/BaseUserSwitchingDialog;->show()V

    goto :goto_0

    .line 808
    :cond_2
    if-ne v2, v1, :cond_3

    if-eqz v3, :cond_4

    :cond_3
    if-ne v3, v1, :cond_5

    if-nez v2, :cond_5

    .line 810
    :cond_4
    new-instance v2, Lcom/miui/server/xspace/KidSpaceSwitchingDialog;

    iget-object v3, p0, Lcom/miui/server/xspace/XSpaceManagerServiceImpl$2;->val$ams:Lcom/android/server/am/ActivityManagerService;

    iget-object v4, p0, Lcom/miui/server/xspace/XSpaceManagerServiceImpl$2;->val$context:Landroid/content/Context;

    iget v5, p0, Lcom/miui/server/xspace/XSpaceManagerServiceImpl$2;->val$targetUserId:I

    invoke-direct {v2, v3, v4, v5}, Lcom/miui/server/xspace/KidSpaceSwitchingDialog;-><init>(Lcom/android/server/am/ActivityManagerService;Landroid/content/Context;I)V

    invoke-static {v2}, Lcom/miui/server/xspace/XSpaceManagerServiceImpl;->-$$Nest$sfputmDialog(Lcom/miui/server/xspace/BaseUserSwitchingDialog;)V

    .line 811
    invoke-static {}, Lcom/miui/server/xspace/XSpaceManagerServiceImpl;->-$$Nest$sfgetmDialog()Lcom/miui/server/xspace/BaseUserSwitchingDialog;

    move-result-object v2

    invoke-virtual {v2}, Lcom/miui/server/xspace/BaseUserSwitchingDialog;->show()V

    goto :goto_0

    .line 813
    :cond_5
    invoke-static {}, Lcom/miui/server/xspace/XSpaceManagerServiceImpl;->-$$Nest$smgetUserManager()Lcom/android/server/pm/UserManagerService;

    move-result-object v2

    iget v3, p0, Lcom/miui/server/xspace/XSpaceManagerServiceImpl$2;->val$currentUserId:I

    invoke-virtual {v2, v3}, Lcom/android/server/pm/UserManagerService;->getUserInfo(I)Landroid/content/pm/UserInfo;

    move-result-object v2

    .line 814
    .local v2, "currentUserInfo":Landroid/content/pm/UserInfo;
    invoke-static {}, Lcom/miui/server/xspace/XSpaceManagerServiceImpl;->-$$Nest$smgetUserManager()Lcom/android/server/pm/UserManagerService;

    move-result-object v3

    iget v4, p0, Lcom/miui/server/xspace/XSpaceManagerServiceImpl$2;->val$targetUserId:I

    invoke-virtual {v3, v4}, Lcom/android/server/pm/UserManagerService;->getUserInfo(I)Landroid/content/pm/UserInfo;

    move-result-object v3

    .line 815
    .local v3, "targetUserInfo":Landroid/content/pm/UserInfo;
    new-instance v4, Landroid/util/Pair;

    invoke-direct {v4, v2, v3}, Landroid/util/Pair;-><init>(Ljava/lang/Object;Ljava/lang/Object;)V

    .line 816
    .local v4, "userNames":Landroid/util/Pair;, "Landroid/util/Pair<Landroid/content/pm/UserInfo;Landroid/content/pm/UserInfo;>;"
    iget-object v5, p0, Lcom/miui/server/xspace/XSpaceManagerServiceImpl$2;->val$handler:Landroid/os/Handler;

    iget v6, p0, Lcom/miui/server/xspace/XSpaceManagerServiceImpl$2;->val$userSwitchUiMessage:I

    invoke-virtual {v5, v6, v4}, Landroid/os/Handler;->obtainMessage(ILjava/lang/Object;)Landroid/os/Message;

    move-result-object v6

    invoke-virtual {v5, v6}, Landroid/os/Handler;->sendMessage(Landroid/os/Message;)Z

    .line 819
    .end local v2    # "currentUserInfo":Landroid/content/pm/UserInfo;
    .end local v3    # "targetUserInfo":Landroid/content/pm/UserInfo;
    .end local v4    # "userNames":Landroid/util/Pair;, "Landroid/util/Pair<Landroid/content/pm/UserInfo;Landroid/content/pm/UserInfo;>;"
    :goto_0
    return-void
.end method
