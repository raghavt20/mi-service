class com.miui.server.xspace.BaseUserSwitchingDialog extends android.app.AlertDialog {
	 /* .source "BaseUserSwitchingDialog.java" */
	 /* # static fields */
	 static final Integer MSG_START_USER;
	 private static final java.lang.String TAG;
	 private static final Integer WINDOW_SHOWN_TIMEOUT_MS;
	 /* # instance fields */
	 private final android.os.Handler mHandler;
	 android.view.ViewTreeObserver$OnWindowShownListener mOnWindowShownListener;
	 private final com.android.server.am.ActivityManagerService mService;
	 private Boolean mStartedUser;
	 protected final Integer mUserId;
	 /* # direct methods */
	 public com.miui.server.xspace.BaseUserSwitchingDialog ( ) {
		 /* .locals 1 */
		 /* .param p1, "service" # Lcom/android/server/am/ActivityManagerService; */
		 /* .param p2, "context" # Landroid/content/Context; */
		 /* .param p3, "styleId" # I */
		 /* .param p4, "userId" # I */
		 /* .line 35 */
		 /* invoke-direct {p0, p2, p3}, Landroid/app/AlertDialog;-><init>(Landroid/content/Context;I)V */
		 /* .line 92 */
		 /* new-instance v0, Lcom/miui/server/xspace/BaseUserSwitchingDialog$2; */
		 /* invoke-direct {v0, p0}, Lcom/miui/server/xspace/BaseUserSwitchingDialog$2;-><init>(Lcom/miui/server/xspace/BaseUserSwitchingDialog;)V */
		 this.mHandler = v0;
		 /* .line 36 */
		 this.mService = p1;
		 /* .line 37 */
		 /* iput p4, p0, Lcom/miui/server/xspace/BaseUserSwitchingDialog;->mUserId:I */
		 /* .line 39 */
		 /* new-instance v0, Lcom/miui/server/xspace/BaseUserSwitchingDialog$1; */
		 /* invoke-direct {v0, p0}, Lcom/miui/server/xspace/BaseUserSwitchingDialog$1;-><init>(Lcom/miui/server/xspace/BaseUserSwitchingDialog;)V */
		 this.mOnWindowShownListener = v0;
		 /* .line 45 */
		 return;
	 } // .end method
	 /* # virtual methods */
	 protected void onCreate ( android.os.Bundle p0 ) {
		 /* .locals 2 */
		 /* .param p1, "savedInstanceState" # Landroid/os/Bundle; */
		 /* .line 49 */
		 /* invoke-super {p0, p1}, Landroid/app/AlertDialog;->onCreate(Landroid/os/Bundle;)V */
		 /* .line 50 */
		 int v0 = 0; // const/4 v0, 0x0
		 (( com.miui.server.xspace.BaseUserSwitchingDialog ) p0 ).setCancelable ( v0 ); // invoke-virtual {p0, v0}, Lcom/miui/server/xspace/BaseUserSwitchingDialog;->setCancelable(Z)V
		 /* .line 51 */
		 (( com.miui.server.xspace.BaseUserSwitchingDialog ) p0 ).getWindow ( ); // invoke-virtual {p0}, Lcom/miui/server/xspace/BaseUserSwitchingDialog;->getWindow()Landroid/view/Window;
		 /* const/16 v1, 0x7da */
		 (( android.view.Window ) v0 ).setType ( v1 ); // invoke-virtual {v0, v1}, Landroid/view/Window;->setType(I)V
		 /* .line 52 */
		 (( com.miui.server.xspace.BaseUserSwitchingDialog ) p0 ).getWindow ( ); // invoke-virtual {p0}, Lcom/miui/server/xspace/BaseUserSwitchingDialog;->getWindow()Landroid/view/Window;
		 (( android.view.Window ) v0 ).getAttributes ( ); // invoke-virtual {v0}, Landroid/view/Window;->getAttributes()Landroid/view/WindowManager$LayoutParams;
		 /* .line 53 */
		 /* .local v0, "attrs":Landroid/view/WindowManager$LayoutParams; */
		 /* const/16 v1, 0x110 */
		 /* iput v1, v0, Landroid/view/WindowManager$LayoutParams;->privateFlags:I */
		 /* .line 55 */
		 (( com.miui.server.xspace.BaseUserSwitchingDialog ) p0 ).getWindow ( ); // invoke-virtual {p0}, Lcom/miui/server/xspace/BaseUserSwitchingDialog;->getWindow()Landroid/view/Window;
		 (( android.view.Window ) v1 ).setAttributes ( v0 ); // invoke-virtual {v1, v0}, Landroid/view/Window;->setAttributes(Landroid/view/WindowManager$LayoutParams;)V
		 /* .line 56 */
		 return;
	 } // .end method
	 public void show ( ) {
		 /* .locals 5 */
		 /* .line 60 */
		 /* invoke-super {p0}, Landroid/app/AlertDialog;->show()V */
		 /* .line 61 */
		 (( com.miui.server.xspace.BaseUserSwitchingDialog ) p0 ).getWindow ( ); // invoke-virtual {p0}, Lcom/miui/server/xspace/BaseUserSwitchingDialog;->getWindow()Landroid/view/Window;
		 (( android.view.Window ) v0 ).getDecorView ( ); // invoke-virtual {v0}, Landroid/view/Window;->getDecorView()Landroid/view/View;
		 /* .line 62 */
		 /* .local v0, "view":Landroid/view/View; */
		 if ( v0 != null) { // if-eqz v0, :cond_0
			 /* .line 63 */
			 (( android.view.View ) v0 ).getViewTreeObserver ( ); // invoke-virtual {v0}, Landroid/view/View;->getViewTreeObserver()Landroid/view/ViewTreeObserver;
			 v2 = this.mOnWindowShownListener;
			 (( android.view.ViewTreeObserver ) v1 ).addOnWindowShownListener ( v2 ); // invoke-virtual {v1, v2}, Landroid/view/ViewTreeObserver;->addOnWindowShownListener(Landroid/view/ViewTreeObserver$OnWindowShownListener;)V
			 /* .line 68 */
		 } // :cond_0
		 v1 = this.mHandler;
		 int v2 = 1; // const/4 v2, 0x1
		 (( android.os.Handler ) v1 ).obtainMessage ( v2 ); // invoke-virtual {v1, v2}, Landroid/os/Handler;->obtainMessage(I)Landroid/os/Message;
		 /* const-wide/16 v3, 0xbb8 */
		 (( android.os.Handler ) v1 ).sendMessageDelayed ( v2, v3, v4 ); // invoke-virtual {v1, v2, v3, v4}, Landroid/os/Handler;->sendMessageDelayed(Landroid/os/Message;J)Z
		 /* .line 70 */
		 return;
	 } // .end method
	 void startUser ( ) {
		 /* .locals 8 */
		 /* .line 73 */
		 /* monitor-enter p0 */
		 /* .line 74 */
		 try { // :try_start_0
			 /* iget-boolean v0, p0, Lcom/miui/server/xspace/BaseUserSwitchingDialog;->mStartedUser:Z */
			 /* :try_end_0 */
			 /* .catchall {:try_start_0 ..:try_end_0} :catchall_0 */
			 /* if-nez v0, :cond_1 */
			 /* .line 76 */
			 try { // :try_start_1
				 v0 = this.mService;
				 final String v1 = "mUserController"; // const-string v1, "mUserController"
				 com.miui.server.xspace.ReflectUtil .getObjectField ( v0,v1 );
				 v1 = java.lang.Void.TYPE;
				 /* const-string/jumbo v2, "startUserInForeground" */
				 int v3 = 1; // const/4 v3, 0x1
				 /* new-array v4, v3, [Ljava/lang/Class; */
				 v5 = java.lang.Integer.TYPE;
				 int v6 = 0; // const/4 v6, 0x0
				 /* aput-object v5, v4, v6 */
				 /* new-array v5, v3, [Ljava/lang/Object; */
				 /* iget v7, p0, Lcom/miui/server/xspace/BaseUserSwitchingDialog;->mUserId:I */
				 /* .line 77 */
				 java.lang.Integer .valueOf ( v7 );
				 /* aput-object v7, v5, v6 */
				 /* .line 76 */
				 com.miui.server.xspace.ReflectUtil .callObjectMethod ( v0,v1,v2,v4,v5 );
				 /* .line 78 */
				 /* iput-boolean v3, p0, Lcom/miui/server/xspace/BaseUserSwitchingDialog;->mStartedUser:Z */
				 /* .line 79 */
				 (( com.miui.server.xspace.BaseUserSwitchingDialog ) p0 ).getWindow ( ); // invoke-virtual {p0}, Lcom/miui/server/xspace/BaseUserSwitchingDialog;->getWindow()Landroid/view/Window;
				 (( android.view.Window ) v0 ).getDecorView ( ); // invoke-virtual {v0}, Landroid/view/Window;->getDecorView()Landroid/view/View;
				 /* .line 80 */
				 /* .local v0, "view":Landroid/view/View; */
				 if ( v0 != null) { // if-eqz v0, :cond_0
					 /* .line 81 */
					 (( android.view.View ) v0 ).getViewTreeObserver ( ); // invoke-virtual {v0}, Landroid/view/View;->getViewTreeObserver()Landroid/view/ViewTreeObserver;
					 v2 = this.mOnWindowShownListener;
					 (( android.view.ViewTreeObserver ) v1 ).removeOnWindowShownListener ( v2 ); // invoke-virtual {v1, v2}, Landroid/view/ViewTreeObserver;->removeOnWindowShownListener(Landroid/view/ViewTreeObserver$OnWindowShownListener;)V
					 /* .line 84 */
				 } // :cond_0
				 v1 = this.mHandler;
				 (( android.os.Handler ) v1 ).removeMessages ( v3 ); // invoke-virtual {v1, v3}, Landroid/os/Handler;->removeMessages(I)V
				 /* :try_end_1 */
				 /* .catch Ljava/lang/Exception; {:try_start_1 ..:try_end_1} :catch_0 */
				 /* .catchall {:try_start_1 ..:try_end_1} :catchall_0 */
				 /* .line 87 */
			 } // .end local v0 # "view":Landroid/view/View;
			 /* .line 85 */
			 /* :catch_0 */
			 /* move-exception v0 */
			 /* .line 86 */
			 /* .local v0, "exception":Ljava/lang/Exception; */
			 try { // :try_start_2
				 final String v1 = "BaseUserSwitchingDialog"; // const-string v1, "BaseUserSwitchingDialog"
				 /* new-instance v2, Ljava/lang/StringBuilder; */
				 /* invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V */
				 final String v3 = "Call startUserInForeground fail."; // const-string v3, "Call startUserInForeground fail."
				 (( java.lang.StringBuilder ) v2 ).append ( v3 ); // invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
				 (( java.lang.StringBuilder ) v2 ).append ( v0 ); // invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;
				 (( java.lang.StringBuilder ) v2 ).toString ( ); // invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
				 android.util.Slog .e ( v1,v2 );
				 /* .line 89 */
			 } // .end local v0 # "exception":Ljava/lang/Exception;
		 } // :cond_1
	 } // :goto_0
	 /* monitor-exit p0 */
	 /* .line 90 */
	 return;
	 /* .line 89 */
	 /* :catchall_0 */
	 /* move-exception v0 */
	 /* monitor-exit p0 */
	 /* :try_end_2 */
	 /* .catchall {:try_start_2 ..:try_end_2} :catchall_0 */
	 /* throw v0 */
} // .end method
