class com.miui.server.xspace.XSpaceManagerServiceImpl$1 extends android.database.ContentObserver {
	 /* .source "XSpaceManagerServiceImpl.java" */
	 /* # annotations */
	 /* .annotation system Ldalvik/annotation/EnclosingMethod; */
	 /* value = Lcom/miui/server/xspace/XSpaceManagerServiceImpl;->init(Landroid/content/Context;)V */
} // .end annotation
/* .annotation system Ldalvik/annotation/InnerClass; */
/* accessFlags = 0x0 */
/* name = null */
} // .end annotation
/* # instance fields */
final com.miui.server.xspace.XSpaceManagerServiceImpl this$0; //synthetic
/* # direct methods */
 com.miui.server.xspace.XSpaceManagerServiceImpl$1 ( ) {
/* .locals 0 */
/* .param p1, "this$0" # Lcom/miui/server/xspace/XSpaceManagerServiceImpl; */
/* .param p2, "handler" # Landroid/os/Handler; */
/* .line 445 */
this.this$0 = p1;
/* invoke-direct {p0, p2}, Landroid/database/ContentObserver;-><init>(Landroid/os/Handler;)V */
return;
} // .end method
/* # virtual methods */
public void onChange ( Boolean p0 ) {
/* .locals 4 */
/* .param p1, "selfChange" # Z */
/* .line 448 */
/* invoke-super {p0, p1}, Landroid/database/ContentObserver;->onChange(Z)V */
/* .line 449 */
v0 = this.this$0;
com.miui.server.xspace.XSpaceManagerServiceImpl .-$$Nest$fgetmResolver ( v0 );
/* const-string/jumbo v2, "xspace_enabled" */
int v3 = 0; // const/4 v3, 0x0
v1 = android.provider.MiuiSettings$Secure .getBoolean ( v1,v2,v3 );
com.miui.server.xspace.XSpaceManagerServiceImpl .-$$Nest$fputmIsXSpaceActived ( v0,v1 );
/* .line 451 */
/* new-instance v0, Ljava/lang/StringBuilder; */
/* invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V */
/* const-string/jumbo v1, "update XSpace status, active:" */
(( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
v1 = this.this$0;
v1 = com.miui.server.xspace.XSpaceManagerServiceImpl .-$$Nest$fgetmIsXSpaceActived ( v1 );
(( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v0 ).toString ( ); // invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
final String v1 = "XSpaceManagerServiceImpl"; // const-string v1, "XSpaceManagerServiceImpl"
android.util.Slog .w ( v1,v0 );
/* .line 452 */
return;
} // .end method
