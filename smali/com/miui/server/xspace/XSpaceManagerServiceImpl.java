public class com.miui.server.xspace.XSpaceManagerServiceImpl extends com.miui.server.xspace.XSpaceManagerServiceStub {
	 /* .source "XSpaceManagerServiceImpl.java" */
	 /* # annotations */
	 /* .annotation runtime Lcom/miui/base/annotations/MiuiStubHead; */
	 /* manifestName = "com.miui.server.xspace.XSpaceManagerServiceStub$$" */
} // .end annotation
/* .annotation system Ldalvik/annotation/MemberClasses; */
/* value = { */
/* Lcom/miui/server/xspace/XSpaceManagerServiceImpl$PackageChangedListener; */
/* } */
} // .end annotation
/* # static fields */
private static final java.lang.String ACTION_START_DUAL_ANIMATION;
private static final java.lang.String ACTION_WEIBO_SDK_REQ;
public static final java.lang.String ACTION_XSPACE_RESOLVER_ACTIVITY_FROM_CORE;
private static final java.lang.String EXTRA_XSPACE_CACHED_USERID;
private static final java.lang.String EXTRA_XSPACE_RESOLVE_INTENT_AGAIN;
private static final java.lang.String EXTRA_XSPACE_USERID_SELECTED;
private static final Integer MAX_COMPETE_XSPACE_NOTIFICATION_TIMES;
private static final java.lang.String ORG_IFAA_AIDL_MANAGER_PKGNAME;
private static final java.lang.String PACKAGE_ALIPAY;
private static final java.lang.String PACKAGE_ANDROID_INSTALLER;
private static final java.lang.String PACKAGE_GOOGLE_INSTALLER;
private static final java.lang.String PACKAGE_LINKER;
private static final java.lang.String PACKAGE_MIUI_INSTALLER;
private static final java.lang.String PACKAGE_SECURITYADD;
private static final java.lang.String PACKAGE_SETTING;
private static final java.lang.String PACKAGE_XMSF;
private static final java.lang.String SYSTEM_PROP_XSPACE_CREATED;
private static final java.lang.String TAG;
private static final java.lang.String WEIXIN_SDK_REQ_CLASSNAME;
private static final java.lang.String XIAOMI_GAMECENTER_SDK_PKGNAME;
private static final java.lang.String XSPACE_ANIMATION_STATUS;
private static final Integer XSPACE_APP_LIST_INIT_NUMBER;
private static final java.lang.String XSPACE_CLOUD_CONTROL_STATUS;
private static final java.lang.String XSPACE_SERVICE_COMPONENT;
private static com.miui.server.xspace.BaseUserSwitchingDialog mDialog;
private static com.android.server.pm.UserManagerService mUserManager;
private static android.app.UserSwitchObserver mUserSwitchObserver;
private static final java.util.List mediaAccessRequired;
/* .annotation system Ldalvik/annotation/Signature; */
/* value = { */
/* "Ljava/util/List<", */
/* "Ljava/lang/String;", */
/* ">;" */
/* } */
} // .end annotation
} // .end field
private static final java.util.ArrayList sAddUserPackagesBlackList;
/* .annotation system Ldalvik/annotation/Signature; */
/* value = { */
/* "Ljava/util/ArrayList<", */
/* "Ljava/lang/String;", */
/* ">;" */
/* } */
} // .end annotation
} // .end field
private static final java.util.HashMap sCachedCallingRelationSelfLocked;
/* .annotation system Ldalvik/annotation/Signature; */
/* value = { */
/* "Ljava/util/HashMap<", */
/* "Ljava/lang/String;", */
/* "Ljava/lang/Integer;", */
/* ">;" */
/* } */
} // .end annotation
} // .end field
private static final java.util.ArrayList sCrossUserAimPackagesWhiteList;
/* .annotation system Ldalvik/annotation/Signature; */
/* value = { */
/* "Ljava/util/ArrayList<", */
/* "Ljava/lang/String;", */
/* ">;" */
/* } */
} // .end annotation
} // .end field
private static final java.util.ArrayList sCrossUserCallingPackagesWhiteList;
/* .annotation system Ldalvik/annotation/Signature; */
/* value = { */
/* "Ljava/util/ArrayList<", */
/* "Ljava/lang/String;", */
/* ">;" */
/* } */
} // .end annotation
} // .end field
private static final java.util.ArrayList sCrossUserDisableComponentActionWhiteList;
/* .annotation system Ldalvik/annotation/Signature; */
/* value = { */
/* "Ljava/util/ArrayList<", */
/* "Ljava/lang/String;", */
/* ">;" */
/* } */
} // .end annotation
} // .end field
private static final java.util.ArrayList sPublicActionList;
/* .annotation system Ldalvik/annotation/Signature; */
/* value = { */
/* "Ljava/util/ArrayList<", */
/* "Ljava/lang/String;", */
/* ">;" */
/* } */
} // .end annotation
} // .end field
private static Integer sSwitchUserCallingUid;
private static final java.util.ArrayList sXSpaceApplicationList;
/* .annotation system Ldalvik/annotation/Signature; */
/* value = { */
/* "Ljava/util/ArrayList<", */
/* "Ljava/lang/String;", */
/* ">;" */
/* } */
} // .end annotation
} // .end field
private static final java.util.ArrayList sXSpaceInstalledPackagesSelfLocked;
/* .annotation system Ldalvik/annotation/Signature; */
/* value = { */
/* "Ljava/util/ArrayList<", */
/* "Ljava/lang/String;", */
/* ">;" */
/* } */
} // .end annotation
} // .end field
private static final java.util.ArrayList sXspaceAnimWhiteList;
/* .annotation system Ldalvik/annotation/Signature; */
/* value = { */
/* "Ljava/util/ArrayList<", */
/* "Ljava/lang/String;", */
/* ">;" */
/* } */
} // .end annotation
} // .end field
/* # instance fields */
private Boolean mAnimStatus;
private android.content.Context mContext;
private Boolean mIsXSpaceActived;
private Boolean mIsXSpaceCreated;
private Long mLastTime;
private final com.miui.server.xspace.XSpaceManagerServiceImpl$PackageChangedListener mPackageChangedListener;
private android.content.ContentResolver mResolver;
/* # direct methods */
static Boolean -$$Nest$fgetmIsXSpaceActived ( com.miui.server.xspace.XSpaceManagerServiceImpl p0 ) { //bridge//synthethic
/* .locals 0 */
/* iget-boolean p0, p0, Lcom/miui/server/xspace/XSpaceManagerServiceImpl;->mIsXSpaceActived:Z */
} // .end method
static android.content.ContentResolver -$$Nest$fgetmResolver ( com.miui.server.xspace.XSpaceManagerServiceImpl p0 ) { //bridge//synthethic
/* .locals 0 */
p0 = this.mResolver;
} // .end method
static void -$$Nest$fputmIsXSpaceActived ( com.miui.server.xspace.XSpaceManagerServiceImpl p0, Boolean p1 ) { //bridge//synthethic
/* .locals 0 */
/* iput-boolean p1, p0, Lcom/miui/server/xspace/XSpaceManagerServiceImpl;->mIsXSpaceActived:Z */
return;
} // .end method
static java.lang.String -$$Nest$mgetPackageName ( com.miui.server.xspace.XSpaceManagerServiceImpl p0, android.content.Intent p1 ) { //bridge//synthethic
/* .locals 0 */
/* invoke-direct {p0, p1}, Lcom/miui/server/xspace/XSpaceManagerServiceImpl;->getPackageName(Landroid/content/Intent;)Ljava/lang/String; */
} // .end method
static void -$$Nest$monPackageCallback ( com.miui.server.xspace.XSpaceManagerServiceImpl p0, java.lang.String p1, android.os.UserHandle p2, java.lang.String p3 ) { //bridge//synthethic
/* .locals 0 */
/* invoke-direct {p0, p1, p2, p3}, Lcom/miui/server/xspace/XSpaceManagerServiceImpl;->onPackageCallback(Ljava/lang/String;Landroid/os/UserHandle;Ljava/lang/String;)V */
return;
} // .end method
static com.miui.server.xspace.BaseUserSwitchingDialog -$$Nest$sfgetmDialog ( ) { //bridge//synthethic
/* .locals 1 */
v0 = com.miui.server.xspace.XSpaceManagerServiceImpl.mDialog;
} // .end method
static void -$$Nest$sfputmDialog ( com.miui.server.xspace.BaseUserSwitchingDialog p0 ) { //bridge//synthethic
/* .locals 0 */
return;
} // .end method
static com.android.server.pm.UserManagerService -$$Nest$smgetUserManager ( ) { //bridge//synthethic
/* .locals 1 */
com.miui.server.xspace.XSpaceManagerServiceImpl .getUserManager ( );
} // .end method
static com.miui.server.xspace.XSpaceManagerServiceImpl ( ) {
/* .locals 8 */
/* .line 78 */
/* new-instance v0, Ljava/util/ArrayList; */
/* invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V */
/* .line 80 */
/* new-instance v1, Ljava/util/ArrayList; */
/* invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V */
/* .line 81 */
/* new-instance v2, Ljava/util/ArrayList; */
/* invoke-direct {v2}, Ljava/util/ArrayList;-><init>()V */
/* .line 110 */
/* new-instance v3, Ljava/lang/StringBuilder; */
/* invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V */
/* .line 111 */
/* .local v3, "stringBuilder":Ljava/lang/StringBuilder; */
final String v4 = "com.eg.android."; // const-string v4, "com.eg.android."
(( java.lang.StringBuilder ) v3 ).append ( v4 ); // invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
/* .line 112 */
final String v4 = "AlipayGphone"; // const-string v4, "AlipayGphone"
(( java.lang.StringBuilder ) v3 ).append ( v4 ); // invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
/* .line 113 */
(( java.lang.StringBuilder ) v3 ).toString ( ); // invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
/* .line 115 */
final String v4 = "android"; // const-string v4, "android"
(( java.util.ArrayList ) v0 ).add ( v4 ); // invoke-virtual {v0, v4}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z
/* .line 116 */
final String v4 = "com.android.settings"; // const-string v4, "com.android.settings"
(( java.util.ArrayList ) v0 ).add ( v4 ); // invoke-virtual {v0, v4}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z
/* .line 117 */
final String v4 = "com.android.systemui"; // const-string v4, "com.android.systemui"
(( java.util.ArrayList ) v0 ).add ( v4 ); // invoke-virtual {v0, v4}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z
/* .line 118 */
final String v4 = "com.miui.securitycenter"; // const-string v4, "com.miui.securitycenter"
(( java.util.ArrayList ) v0 ).add ( v4 ); // invoke-virtual {v0, v4}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z
/* .line 119 */
final String v4 = "com.miui.home"; // const-string v4, "com.miui.home"
(( java.util.ArrayList ) v0 ).add ( v4 ); // invoke-virtual {v0, v4}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z
/* .line 120 */
final String v5 = "com.miui.fliphome"; // const-string v5, "com.miui.fliphome"
(( java.util.ArrayList ) v0 ).add ( v5 ); // invoke-virtual {v0, v5}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z
/* .line 121 */
final String v6 = "com.android.shell"; // const-string v6, "com.android.shell"
(( java.util.ArrayList ) v0 ).add ( v6 ); // invoke-virtual {v0, v6}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z
/* .line 123 */
final String v6 = "com.mi.android.globallauncher"; // const-string v6, "com.mi.android.globallauncher"
(( java.util.ArrayList ) v0 ).add ( v6 ); // invoke-virtual {v0, v6}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z
/* .line 124 */
final String v7 = "com.lbe.security.miui"; // const-string v7, "com.lbe.security.miui"
(( java.util.ArrayList ) v0 ).add ( v7 ); // invoke-virtual {v0, v7}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z
/* .line 125 */
final String v0 = "android.nfc.action.TECH_DISCOVERED"; // const-string v0, "android.nfc.action.TECH_DISCOVERED"
(( java.util.ArrayList ) v1 ).add ( v0 ); // invoke-virtual {v1, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z
/* .line 126 */
final String v0 = "com.android.fileexplorer"; // const-string v0, "com.android.fileexplorer"
/* .line 127 */
final String v0 = "com.miui.huanji"; // const-string v0, "com.miui.huanji"
/* .line 129 */
} // .end local v3 # "stringBuilder":Ljava/lang/StringBuilder;
/* new-instance v0, Ljava/util/ArrayList; */
/* invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V */
/* .line 131 */
final String v1 = "android.intent.action.SEND"; // const-string v1, "android.intent.action.SEND"
(( java.util.ArrayList ) v0 ).add ( v1 ); // invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z
/* .line 132 */
final String v1 = "android.intent.action.SEND_MULTIPLE"; // const-string v1, "android.intent.action.SEND_MULTIPLE"
(( java.util.ArrayList ) v0 ).add ( v1 ); // invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z
/* .line 133 */
final String v1 = "android.intent.action.SENDTO"; // const-string v1, "android.intent.action.SENDTO"
(( java.util.ArrayList ) v0 ).add ( v1 ); // invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z
/* .line 134 */
final String v1 = "android.content.pm.action.REQUEST_PERMISSIONS_FOR_OTHER"; // const-string v1, "android.content.pm.action.REQUEST_PERMISSIONS_FOR_OTHER"
(( java.util.ArrayList ) v0 ).add ( v1 ); // invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z
/* .line 136 */
/* new-instance v0, Ljava/util/ArrayList; */
/* invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V */
/* .line 138 */
final String v1 = "com.xiaomi.xmsf"; // const-string v1, "com.xiaomi.xmsf"
(( java.util.ArrayList ) v0 ).add ( v1 ); // invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z
/* .line 139 */
v1 = miui.securityspace.XSpaceConstant.REQUIRED_APPS;
(( java.util.ArrayList ) v0 ).addAll ( v1 ); // invoke-virtual {v0, v1}, Ljava/util/ArrayList;->addAll(Ljava/util/Collection;)Z
/* .line 140 */
final String v1 = "com.xiaomi.gamecenter.sdk.service"; // const-string v1, "com.xiaomi.gamecenter.sdk.service"
(( java.util.ArrayList ) v0 ).remove ( v1 ); // invoke-virtual {v0, v1}, Ljava/util/ArrayList;->remove(Ljava/lang/Object;)Z
/* .line 141 */
final String v2 = "com.google.android.gsf"; // const-string v2, "com.google.android.gsf"
(( java.util.ArrayList ) v0 ).add ( v2 ); // invoke-virtual {v0, v2}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z
/* .line 142 */
final String v2 = "com.google.android.gsf.login"; // const-string v2, "com.google.android.gsf.login"
(( java.util.ArrayList ) v0 ).add ( v2 ); // invoke-virtual {v0, v2}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z
/* .line 143 */
final String v2 = "com.google.android.gms"; // const-string v2, "com.google.android.gms"
(( java.util.ArrayList ) v0 ).add ( v2 ); // invoke-virtual {v0, v2}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z
/* .line 144 */
final String v2 = "com.google.android.play.games"; // const-string v2, "com.google.android.play.games"
(( java.util.ArrayList ) v0 ).add ( v2 ); // invoke-virtual {v0, v2}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z
/* .line 146 */
/* new-instance v0, Ljava/util/ArrayList; */
/* invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V */
/* .line 148 */
(( java.util.ArrayList ) v0 ).add ( v4 ); // invoke-virtual {v0, v4}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z
/* .line 149 */
(( java.util.ArrayList ) v0 ).add ( v5 ); // invoke-virtual {v0, v5}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z
/* .line 150 */
(( java.util.ArrayList ) v0 ).add ( v6 ); // invoke-virtual {v0, v6}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z
/* .line 153 */
/* new-instance v0, Ljava/util/ArrayList; */
/* invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V */
/* .line 155 */
final String v2 = "com.android.contacts"; // const-string v2, "com.android.contacts"
(( java.util.ArrayList ) v0 ).add ( v2 ); // invoke-virtual {v0, v2}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z
/* .line 157 */
/* new-instance v0, Ljava/util/ArrayList; */
/* invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V */
/* .line 159 */
(( java.util.ArrayList ) v0 ).add ( v1 ); // invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z
/* .line 160 */
final String v1 = "org.ifaa.aidl.manager"; // const-string v1, "org.ifaa.aidl.manager"
(( java.util.ArrayList ) v0 ).add ( v1 ); // invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z
/* .line 161 */
v0 = (( java.util.ArrayList ) v0 ).size ( ); // invoke-virtual {v0}, Ljava/util/ArrayList;->size()I
/* .line 163 */
/* new-instance v0, Ljava/util/ArrayList; */
/* invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V */
/* .line 165 */
final String v1 = "com.android.vending"; // const-string v1, "com.android.vending"
(( java.util.ArrayList ) v0 ).add ( v1 ); // invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z
/* .line 167 */
/* new-instance v0, Ljava/util/HashMap; */
/* invoke-direct {v0}, Ljava/util/HashMap;-><init>()V */
/* .line 826 */
/* new-instance v0, Lcom/miui/server/xspace/XSpaceManagerServiceImpl$3; */
/* invoke-direct {v0}, Lcom/miui/server/xspace/XSpaceManagerServiceImpl$3;-><init>()V */
return;
} // .end method
public com.miui.server.xspace.XSpaceManagerServiceImpl ( ) {
/* .locals 2 */
/* .line 71 */
/* invoke-direct {p0}, Lcom/miui/server/xspace/XSpaceManagerServiceStub;-><init>()V */
/* .line 103 */
int v0 = 0; // const/4 v0, 0x0
/* iput-boolean v0, p0, Lcom/miui/server/xspace/XSpaceManagerServiceImpl;->mIsXSpaceCreated:Z */
/* .line 104 */
/* iput-boolean v0, p0, Lcom/miui/server/xspace/XSpaceManagerServiceImpl;->mIsXSpaceActived:Z */
/* .line 108 */
int v0 = 1; // const/4 v0, 0x1
/* iput-boolean v0, p0, Lcom/miui/server/xspace/XSpaceManagerServiceImpl;->mAnimStatus:Z */
/* .line 168 */
/* new-instance v0, Lcom/miui/server/xspace/XSpaceManagerServiceImpl$PackageChangedListener; */
int v1 = 0; // const/4 v1, 0x0
/* invoke-direct {v0, p0, v1}, Lcom/miui/server/xspace/XSpaceManagerServiceImpl$PackageChangedListener;-><init>(Lcom/miui/server/xspace/XSpaceManagerServiceImpl;Lcom/miui/server/xspace/XSpaceManagerServiceImpl$PackageChangedListener-IA;)V */
this.mPackageChangedListener = v0;
return;
} // .end method
private Boolean checkCallXSpacePermission ( java.lang.String p0 ) {
/* .locals 5 */
/* .param p1, "callingPkg" # Ljava/lang/String; */
/* .line 428 */
int v0 = 0; // const/4 v0, 0x0
try { // :try_start_0
android.app.AppGlobals .getPackageManager ( );
/* .line 429 */
/* const-wide/16 v2, 0x0 */
/* .line 430 */
/* .local v1, "appInfo":Landroid/content/pm/ApplicationInfo; */
/* iget v2, v1, Landroid/content/pm/ApplicationInfo;->flags:I */
int v3 = 1; // const/4 v3, 0x1
/* and-int/2addr v2, v3 */
/* if-gtz v2, :cond_1 */
/* iget v2, v1, Landroid/content/pm/ApplicationInfo;->uid:I */
/* :try_end_0 */
/* .catch Landroid/os/RemoteException; {:try_start_0 ..:try_end_0} :catch_0 */
/* const/16 v4, 0x3e8 */
/* if-gt v2, v4, :cond_0 */
/* .line 435 */
} // .end local v1 # "appInfo":Landroid/content/pm/ApplicationInfo;
} // :cond_0
/* .line 431 */
/* .restart local v1 # "appInfo":Landroid/content/pm/ApplicationInfo; */
} // :cond_1
} // :goto_0
/* .line 433 */
} // .end local v1 # "appInfo":Landroid/content/pm/ApplicationInfo;
/* :catch_0 */
/* move-exception v1 */
/* .line 434 */
/* .local v1, "e":Landroid/os/RemoteException; */
/* new-instance v2, Ljava/lang/StringBuilder; */
/* invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V */
final String v3 = "Failed to read package info of: "; // const-string v3, "Failed to read package info of: "
(( java.lang.StringBuilder ) v2 ).append ( v3 ); // invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v2 ).append ( p1 ); // invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v2 ).toString ( ); // invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
final String v3 = "XSpaceManagerServiceImpl"; // const-string v3, "XSpaceManagerServiceImpl"
android.util.Slog .e ( v3,v2,v1 );
/* .line 436 */
} // .end local v1 # "e":Landroid/os/RemoteException;
} // :goto_1
} // .end method
private android.content.Intent creatDualAnimIntent ( android.content.Intent p0 ) {
/* .locals 3 */
/* .param p1, "intent" # Landroid/content/Intent; */
/* .line 303 */
/* new-instance v0, Landroid/content/Intent; */
final String v1 = "miui.intent.action.START_DUAL_ANIMATION"; // const-string v1, "miui.intent.action.START_DUAL_ANIMATION"
/* invoke-direct {v0, v1}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V */
/* .line 304 */
/* .local v0, "result":Landroid/content/Intent; */
final String v1 = "com.miui.securityadd"; // const-string v1, "com.miui.securityadd"
(( android.content.Intent ) v0 ).setPackage ( v1 ); // invoke-virtual {v0, v1}, Landroid/content/Intent;->setPackage(Ljava/lang/String;)Landroid/content/Intent;
/* .line 305 */
/* const/high16 v1, 0x800000 */
(( android.content.Intent ) v0 ).addFlags ( v1 ); // invoke-virtual {v0, v1}, Landroid/content/Intent;->addFlags(I)Landroid/content/Intent;
/* .line 307 */
final String v1 = "android.intent.extra.xspace_cached_uid"; // const-string v1, "android.intent.extra.xspace_cached_uid"
int v2 = 0; // const/4 v2, 0x0
(( android.content.Intent ) v0 ).putExtra ( v1, v2 ); // invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;
/* .line 309 */
final String v1 = "android.intent.extra.auth_to_call_xspace"; // const-string v1, "android.intent.extra.auth_to_call_xspace"
int v2 = 1; // const/4 v2, 0x1
(( android.content.Intent ) p1 ).putExtra ( v1, v2 ); // invoke-virtual {p1, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;
/* .line 311 */
final String v1 = "android.intent.extra.INTENT"; // const-string v1, "android.intent.extra.INTENT"
(( android.content.Intent ) v0 ).putExtra ( v1, p1 ); // invoke-virtual {v0, v1, p1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Landroid/os/Parcelable;)Landroid/content/Intent;
/* .line 312 */
} // .end method
private void disableGMSApps ( ) {
/* .locals 10 */
/* .line 600 */
final String v0 = "XSpaceManagerServiceImpl"; // const-string v0, "XSpaceManagerServiceImpl"
android.app.AppGlobals .getPackageManager ( );
/* .line 601 */
/* .local v7, "iPackageManager":Landroid/content/pm/IPackageManager; */
v1 = miui.securityspace.XSpaceConstant.GMS_RELATED_APPS;
(( java.util.ArrayList ) v1 ).iterator ( ); // invoke-virtual {v1}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;
v1 = } // :goto_0
if ( v1 != null) { // if-eqz v1, :cond_1
/* move-object v9, v1 */
/* check-cast v9, Ljava/lang/String; */
/* .line 602 */
/* .local v9, "pkgName":Ljava/lang/String; */
v1 = this.mContext;
v1 = miui.securityspace.XSpaceUserHandle .isAppInXSpace ( v1,v9 );
if ( v1 != null) { // if-eqz v1, :cond_0
/* .line 604 */
int v3 = -1; // const/4 v3, -0x1
int v4 = 0; // const/4 v4, 0x0
/* const/16 v5, 0x3e7 */
int v6 = 4; // const/4 v6, 0x4
/* move-object v1, v7 */
/* move-object v2, v9 */
try { // :try_start_0
/* invoke-interface/range {v1 ..v6}, Landroid/content/pm/IPackageManager;->deletePackageAsUser(Ljava/lang/String;ILandroid/content/pm/IPackageDeleteObserver;II)V */
/* :try_end_0 */
/* .catch Landroid/os/RemoteException; {:try_start_0 ..:try_end_0} :catch_1 */
/* .catch Ljava/lang/IllegalArgumentException; {:try_start_0 ..:try_end_0} :catch_0 */
/* .line 609 */
} // :goto_1
/* .line 607 */
/* :catch_0 */
/* move-exception v1 */
/* .line 608 */
/* .local v1, "e":Ljava/lang/IllegalArgumentException; */
/* new-instance v2, Ljava/lang/StringBuilder; */
/* invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V */
final String v3 = "Unknown package: "; // const-string v3, "Unknown package: "
(( java.lang.StringBuilder ) v2 ).append ( v3 ); // invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v2 ).append ( v9 ); // invoke-virtual {v2, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v2 ).toString ( ); // invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
android.util.Slog .e ( v0,v2,v1 );
/* .line 605 */
} // .end local v1 # "e":Ljava/lang/IllegalArgumentException;
/* :catch_1 */
/* move-exception v1 */
/* .line 606 */
/* .local v1, "e":Landroid/os/RemoteException; */
/* new-instance v2, Ljava/lang/StringBuilder; */
/* invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V */
final String v3 = "Unable to delete package: "; // const-string v3, "Unable to delete package: "
(( java.lang.StringBuilder ) v2 ).append ( v3 ); // invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v2 ).append ( v9 ); // invoke-virtual {v2, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v2 ).toString ( ); // invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
android.util.Slog .e ( v0,v2,v1 );
} // .end local v1 # "e":Landroid/os/RemoteException;
/* .line 611 */
} // .end local v9 # "pkgName":Ljava/lang/String;
} // :cond_0
} // :goto_2
/* .line 612 */
} // :cond_1
return;
} // .end method
private java.lang.String getAimPkg ( android.content.Intent p0 ) {
/* .locals 2 */
/* .param p1, "intent" # Landroid/content/Intent; */
/* .line 398 */
(( android.content.Intent ) p1 ).getPackage ( ); // invoke-virtual {p1}, Landroid/content/Intent;->getPackage()Ljava/lang/String;
/* .line 399 */
/* .local v0, "aimPkg":Ljava/lang/String; */
/* if-nez v0, :cond_0 */
/* .line 400 */
(( android.content.Intent ) p1 ).getComponent ( ); // invoke-virtual {p1}, Landroid/content/Intent;->getComponent()Landroid/content/ComponentName;
/* .line 401 */
/* .local v1, "componentName":Landroid/content/ComponentName; */
if ( v1 != null) { // if-eqz v1, :cond_0
/* .line 402 */
(( android.content.ComponentName ) v1 ).getPackageName ( ); // invoke-virtual {v1}, Landroid/content/ComponentName;->getPackageName()Ljava/lang/String;
/* .line 405 */
} // .end local v1 # "componentName":Landroid/content/ComponentName;
} // :cond_0
} // .end method
private Integer getCachedUserId ( android.content.Intent p0, java.lang.String p1 ) {
/* .locals 4 */
/* .param p1, "intent" # Landroid/content/Intent; */
/* .param p2, "callingPackage" # Ljava/lang/String; */
/* .line 333 */
/* const/16 v0, -0x2710 */
/* .line 334 */
/* .local v0, "cachedUserId":I */
/* invoke-direct {p0, p1}, Lcom/miui/server/xspace/XSpaceManagerServiceImpl;->getAimPkg(Landroid/content/Intent;)Ljava/lang/String; */
/* .line 335 */
/* .local v1, "aimPkg":Ljava/lang/String; */
v2 = android.text.TextUtils .equals ( v1,p2 );
/* if-nez v2, :cond_0 */
/* .line 336 */
final String v2 = "android.intent.extra.xspace_cached_uid"; // const-string v2, "android.intent.extra.xspace_cached_uid"
v3 = (( android.content.Intent ) p1 ).hasExtra ( v2 ); // invoke-virtual {p1, v2}, Landroid/content/Intent;->hasExtra(Ljava/lang/String;)Z
if ( v3 != null) { // if-eqz v3, :cond_0
/* .line 337 */
/* const/16 v3, -0x2710 */
v0 = (( android.content.Intent ) p1 ).getIntExtra ( v2, v3 ); // invoke-virtual {p1, v2, v3}, Landroid/content/Intent;->getIntExtra(Ljava/lang/String;I)I
/* .line 338 */
(( android.content.Intent ) p1 ).removeExtra ( v2 ); // invoke-virtual {p1, v2}, Landroid/content/Intent;->removeExtra(Ljava/lang/String;)V
/* .line 340 */
} // :cond_0
} // .end method
private java.lang.String getPackageName ( android.content.Intent p0 ) {
/* .locals 2 */
/* .param p1, "intent" # Landroid/content/Intent; */
/* .line 529 */
(( android.content.Intent ) p1 ).getData ( ); // invoke-virtual {p1}, Landroid/content/Intent;->getData()Landroid/net/Uri;
/* .line 530 */
/* .local v0, "uri":Landroid/net/Uri; */
if ( v0 != null) { // if-eqz v0, :cond_0
(( android.net.Uri ) v0 ).getSchemeSpecificPart ( ); // invoke-virtual {v0}, Landroid/net/Uri;->getSchemeSpecificPart()Ljava/lang/String;
} // :cond_0
int v1 = 0; // const/4 v1, 0x0
/* .line 531 */
/* .local v1, "pkg":Ljava/lang/String; */
} // :goto_0
} // .end method
private android.content.Intent getResolverActivity ( android.content.Intent p0, java.lang.String p1, Integer p2, java.lang.String p3 ) {
/* .locals 4 */
/* .param p1, "intent" # Landroid/content/Intent; */
/* .param p2, "aimPkg" # Ljava/lang/String; */
/* .param p3, "requestCode" # I */
/* .param p4, "callingPackage" # Ljava/lang/String; */
/* .line 410 */
/* new-instance v0, Landroid/content/Intent; */
final String v1 = "miui.intent.action.ACTION_XSPACE_RESOLVER_ACTIVITY_FROM_CORE"; // const-string v1, "miui.intent.action.ACTION_XSPACE_RESOLVER_ACTIVITY_FROM_CORE"
/* invoke-direct {v0, v1}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V */
/* .line 411 */
/* .local v0, "resolverActivityIntent":Landroid/content/Intent; */
/* const/high16 v1, 0x2000000 */
/* if-ltz p3, :cond_0 */
/* .line 412 */
(( android.content.Intent ) p1 ).addFlags ( v1 ); // invoke-virtual {p1, v1}, Landroid/content/Intent;->addFlags(I)Landroid/content/Intent;
/* .line 414 */
} // :cond_0
v2 = (( android.content.Intent ) p1 ).getFlags ( ); // invoke-virtual {p1}, Landroid/content/Intent;->getFlags()I
/* and-int/2addr v2, v1 */
if ( v2 != null) { // if-eqz v2, :cond_1
/* .line 415 */
(( android.content.Intent ) v0 ).addFlags ( v1 ); // invoke-virtual {v0, v1}, Landroid/content/Intent;->addFlags(I)Landroid/content/Intent;
/* .line 417 */
} // :cond_1
} // :goto_0
final String v1 = "miui.intent.extra.xspace_resolver_activity_calling_package"; // const-string v1, "miui.intent.extra.xspace_resolver_activity_calling_package"
(( android.content.Intent ) p1 ).putExtra ( v1, p4 ); // invoke-virtual {p1, v1, p4}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;
/* .line 418 */
final String v1 = "android.intent.extra.xspace_userid_selected"; // const-string v1, "android.intent.extra.xspace_userid_selected"
int v2 = 1; // const/4 v2, 0x1
(( android.content.Intent ) p1 ).putExtra ( v1, v2 ); // invoke-virtual {p1, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;
/* .line 419 */
final String v1 = "android.intent.extra.xspace_resolver_activity_original_intent"; // const-string v1, "android.intent.extra.xspace_resolver_activity_original_intent"
(( android.content.Intent ) v0 ).putExtra ( v1, p1 ); // invoke-virtual {v0, v1, p1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Landroid/os/Parcelable;)Landroid/content/Intent;
/* .line 420 */
final String v1 = "android.intent.extra.xspace_resolver_activity_aim_package"; // const-string v1, "android.intent.extra.xspace_resolver_activity_aim_package"
(( android.content.Intent ) v0 ).putExtra ( v1, p2 ); // invoke-virtual {v0, v1, p2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;
/* .line 421 */
final String v1 = "com.miui.securitycore"; // const-string v1, "com.miui.securitycore"
final String v3 = "com.miui.xspace.ui.activity.XSpaceResolveActivity"; // const-string v3, "com.miui.xspace.ui.activity.XSpaceResolveActivity"
(( android.content.Intent ) v0 ).setClassName ( v1, v3 ); // invoke-virtual {v0, v1, v3}, Landroid/content/Intent;->setClassName(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;
/* .line 422 */
final String v1 = "android.intent.extra.xspace_resolve_intent_again"; // const-string v1, "android.intent.extra.xspace_resolve_intent_again"
(( android.content.Intent ) v0 ).putExtra ( v1, v2 ); // invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;
/* .line 423 */
} // .end method
private Integer getToUserIdFromCachedCallingRelation ( java.lang.String p0, java.lang.String p1 ) {
/* .locals 7 */
/* .param p1, "aimPkg" # Ljava/lang/String; */
/* .param p2, "callingPackage" # Ljava/lang/String; */
/* .line 383 */
/* const/16 v0, -0x2710 */
/* .line 384 */
/* .local v0, "cachedToUserId":I */
v1 = com.miui.server.xspace.XSpaceManagerServiceImpl.sCachedCallingRelationSelfLocked;
/* monitor-enter v1 */
/* .line 385 */
try { // :try_start_0
/* new-instance v2, Ljava/lang/StringBuilder; */
/* invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V */
(( java.lang.StringBuilder ) v2 ).append ( p2 ); // invoke-virtual {v2, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
final String v3 = "@"; // const-string v3, "@"
(( java.lang.StringBuilder ) v2 ).append ( v3 ); // invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v2 ).append ( p1 ); // invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v2 ).toString ( ); // invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
/* .line 386 */
/* .local v2, "callingRelationKey":Ljava/lang/String; */
(( java.util.HashMap ) v1 ).get ( v2 ); // invoke-virtual {v1, v2}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;
/* check-cast v3, Ljava/lang/Integer; */
/* .line 387 */
/* .local v3, "toUserIdObj":Ljava/lang/Integer; */
if ( v3 != null) { // if-eqz v3, :cond_0
/* .line 389 */
v4 = (( java.lang.Integer ) v3 ).intValue ( ); // invoke-virtual {v3}, Ljava/lang/Integer;->intValue()I
/* move v0, v4 */
/* .line 390 */
(( java.util.HashMap ) v1 ).remove ( v2 ); // invoke-virtual {v1, v2}, Ljava/util/HashMap;->remove(Ljava/lang/Object;)Ljava/lang/Object;
/* .line 391 */
final String v4 = "XSpaceManagerServiceImpl"; // const-string v4, "XSpaceManagerServiceImpl"
/* new-instance v5, Ljava/lang/StringBuilder; */
/* invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V */
final String v6 = "got callingRelationKey :"; // const-string v6, "got callingRelationKey :"
(( java.lang.StringBuilder ) v5 ).append ( v6 ); // invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v5 ).append ( v2 ); // invoke-virtual {v5, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
final String v6 = ", cachedToUserId:"; // const-string v6, ", cachedToUserId:"
(( java.lang.StringBuilder ) v5 ).append ( v6 ); // invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v5 ).append ( v0 ); // invoke-virtual {v5, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v5 ).toString ( ); // invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
android.util.Slog .w ( v4,v5 );
/* .line 393 */
} // .end local v2 # "callingRelationKey":Ljava/lang/String;
} // .end local v3 # "toUserIdObj":Ljava/lang/Integer;
} // :cond_0
/* monitor-exit v1 */
/* .line 394 */
/* .line 393 */
/* :catchall_0 */
/* move-exception v2 */
/* monitor-exit v1 */
/* :try_end_0 */
/* .catchall {:try_start_0 ..:try_end_0} :catchall_0 */
/* throw v2 */
} // .end method
private static com.android.server.pm.UserManagerService getUserManager ( ) {
/* .locals 2 */
/* .line 840 */
v0 = com.miui.server.xspace.XSpaceManagerServiceImpl.mUserManager;
/* if-nez v0, :cond_0 */
/* .line 841 */
/* const-string/jumbo v0, "user" */
android.os.ServiceManager .getService ( v0 );
/* .line 842 */
/* .local v0, "b":Landroid/os/IBinder; */
android.os.IUserManager$Stub .asInterface ( v0 );
/* check-cast v1, Lcom/android/server/pm/UserManagerService; */
/* .line 844 */
} // .end local v0 # "b":Landroid/os/IBinder;
} // :cond_0
v0 = com.miui.server.xspace.XSpaceManagerServiceImpl.mUserManager;
} // .end method
private void initXSpaceAppList ( ) {
/* .locals 8 */
/* .line 471 */
int v0 = 0; // const/4 v0, 0x0
/* .line 473 */
/* .local v0, "slice":Landroid/content/pm/ParceledListSlice;, "Landroid/content/pm/ParceledListSlice<Landroid/content/pm/PackageInfo;>;" */
try { // :try_start_0
android.app.AppGlobals .getPackageManager ( );
/* const-wide/16 v2, 0x0 */
/* const/16 v4, 0x3e7 */
/* :try_end_0 */
/* .catch Landroid/os/RemoteException; {:try_start_0 ..:try_end_0} :catch_0 */
/* move-object v0, v1 */
/* .line 476 */
/* .line 474 */
/* :catch_0 */
/* move-exception v1 */
/* .line 475 */
/* .local v1, "e":Landroid/os/RemoteException; */
(( android.os.RemoteException ) v1 ).printStackTrace ( ); // invoke-virtual {v1}, Landroid/os/RemoteException;->printStackTrace()V
/* .line 477 */
} // .end local v1 # "e":Landroid/os/RemoteException;
} // :goto_0
if ( v0 != null) { // if-eqz v0, :cond_2
/* .line 478 */
(( android.content.pm.ParceledListSlice ) v0 ).getList ( ); // invoke-virtual {v0}, Landroid/content/pm/ParceledListSlice;->getList()Ljava/util/List;
/* .line 479 */
/* .local v1, "appList":Ljava/util/List;, "Ljava/util/List<Landroid/content/pm/PackageInfo;>;" */
v2 = com.miui.server.xspace.XSpaceManagerServiceImpl.sXSpaceInstalledPackagesSelfLocked;
/* monitor-enter v2 */
/* .line 480 */
try { // :try_start_1
v4 = } // :goto_1
if ( v4 != null) { // if-eqz v4, :cond_1
/* check-cast v4, Landroid/content/pm/PackageInfo; */
/* .line 481 */
/* .local v4, "pkgInfo":Landroid/content/pm/PackageInfo; */
v5 = this.applicationInfo;
/* .line 482 */
/* .local v5, "appInfo":Landroid/content/pm/ApplicationInfo; */
v6 = /* invoke-direct {p0, v5}, Lcom/miui/server/xspace/XSpaceManagerServiceImpl;->isSystemApp(Landroid/content/pm/ApplicationInfo;)Z */
/* if-nez v6, :cond_0 */
v6 = miui.securityspace.XSpaceConstant.GMS_RELATED_APPS;
v7 = this.packageName;
v6 = (( java.util.ArrayList ) v6 ).contains ( v7 ); // invoke-virtual {v6, v7}, Ljava/util/ArrayList;->contains(Ljava/lang/Object;)Z
/* if-nez v6, :cond_0 */
/* .line 483 */
v6 = com.miui.server.xspace.XSpaceManagerServiceImpl.sXSpaceInstalledPackagesSelfLocked;
v7 = this.packageName;
(( java.util.ArrayList ) v6 ).add ( v7 ); // invoke-virtual {v6, v7}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z
/* .line 485 */
} // .end local v4 # "pkgInfo":Landroid/content/pm/PackageInfo;
} // .end local v5 # "appInfo":Landroid/content/pm/ApplicationInfo;
} // :cond_0
/* .line 486 */
} // :cond_1
/* monitor-exit v2 */
/* :catchall_0 */
/* move-exception v3 */
/* monitor-exit v2 */
/* :try_end_1 */
/* .catchall {:try_start_1 ..:try_end_1} :catchall_0 */
/* throw v3 */
/* .line 488 */
} // .end local v1 # "appList":Ljava/util/List;, "Ljava/util/List<Landroid/content/pm/PackageInfo;>;"
} // :cond_2
} // :goto_2
v1 = com.miui.server.xspace.XSpaceManagerServiceImpl.sXSpaceInstalledPackagesSelfLocked;
v2 = (( java.util.ArrayList ) v1 ).size ( ); // invoke-virtual {v1}, Ljava/util/ArrayList;->size()I
/* if-le v2, v3, :cond_3 */
/* .line 489 */
int v2 = 1; // const/4 v2, 0x1
/* iput-boolean v2, p0, Lcom/miui/server/xspace/XSpaceManagerServiceImpl;->mIsXSpaceActived:Z */
/* .line 491 */
} // :cond_3
v2 = this.mResolver;
/* const-string/jumbo v4, "xspace_enabled" */
/* iget-boolean v5, p0, Lcom/miui/server/xspace/XSpaceManagerServiceImpl;->mIsXSpaceActived:Z */
android.provider.MiuiSettings$Secure .putBoolean ( v2,v4,v5 );
/* .line 492 */
final String v2 = "XSpaceManagerServiceImpl"; // const-string v2, "XSpaceManagerServiceImpl"
/* new-instance v4, Ljava/lang/StringBuilder; */
/* invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V */
final String v5 = "initXSpaceAppList sXSpaceInstalledPackagesSelfLocked ="; // const-string v5, "initXSpaceAppList sXSpaceInstalledPackagesSelfLocked ="
(( java.lang.StringBuilder ) v4 ).append ( v5 ); // invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.util.ArrayList ) v1 ).toString ( ); // invoke-virtual {v1}, Ljava/util/ArrayList;->toString()Ljava/lang/String;
(( java.lang.StringBuilder ) v4 ).append ( v1 ); // invoke-virtual {v4, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
final String v4 = " XSPACE_APP_LIST_INIT_NUMBER ="; // const-string v4, " XSPACE_APP_LIST_INIT_NUMBER ="
(( java.lang.StringBuilder ) v1 ).append ( v4 ); // invoke-virtual {v1, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v1 ).append ( v3 ); // invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v1 ).toString ( ); // invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
android.util.Slog .d ( v2,v1 );
/* .line 494 */
final String v1 = "XSpaceManagerServiceImpl"; // const-string v1, "XSpaceManagerServiceImpl"
/* new-instance v2, Ljava/lang/StringBuilder; */
/* invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V */
final String v3 = "Reset XSpace enable, active:"; // const-string v3, "Reset XSpace enable, active:"
(( java.lang.StringBuilder ) v2 ).append ( v3 ); // invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
/* iget-boolean v3, p0, Lcom/miui/server/xspace/XSpaceManagerServiceImpl;->mIsXSpaceActived:Z */
(( java.lang.StringBuilder ) v2 ).append ( v3 ); // invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v2 ).toString ( ); // invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
android.util.Slog .w ( v1,v2 );
/* .line 495 */
return;
} // .end method
private Boolean isComponentEnabled ( android.content.pm.ActivityInfo p0, Integer p1 ) {
/* .locals 5 */
/* .param p1, "info" # Landroid/content/pm/ActivityInfo; */
/* .param p2, "userId" # I */
/* .line 662 */
/* if-nez p1, :cond_0 */
/* .line 663 */
int v0 = 1; // const/4 v0, 0x1
/* .line 665 */
} // :cond_0
int v0 = 0; // const/4 v0, 0x0
/* .line 666 */
/* .local v0, "enabled":Z */
/* new-instance v1, Landroid/content/ComponentName; */
v2 = this.packageName;
v3 = this.name;
/* invoke-direct {v1, v2, v3}, Landroid/content/ComponentName;-><init>(Ljava/lang/String;Ljava/lang/String;)V */
/* .line 672 */
/* .local v1, "compname":Landroid/content/ComponentName; */
try { // :try_start_0
android.app.AppGlobals .getPackageManager ( );
/* const-wide/16 v3, 0x0 */
/* :try_end_0 */
/* .catch Landroid/os/RemoteException; {:try_start_0 ..:try_end_0} :catch_0 */
if ( v2 != null) { // if-eqz v2, :cond_1
/* .line 673 */
int v0 = 1; // const/4 v0, 0x1
/* .line 677 */
} // :cond_1
/* .line 675 */
/* :catch_0 */
/* move-exception v2 */
/* .line 676 */
/* .local v2, "e":Landroid/os/RemoteException; */
int v0 = 0; // const/4 v0, 0x0
/* .line 678 */
} // .end local v2 # "e":Landroid/os/RemoteException;
} // :goto_0
/* if-nez v0, :cond_2 */
/* .line 679 */
/* new-instance v2, Ljava/lang/StringBuilder; */
/* invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V */
final String v3 = "Component not enabled: "; // const-string v3, "Component not enabled: "
(( java.lang.StringBuilder ) v2 ).append ( v3 ); // invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v2 ).append ( v1 ); // invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;
final String v3 = " in user "; // const-string v3, " in user "
(( java.lang.StringBuilder ) v2 ).append ( v3 ); // invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v2 ).append ( p2 ); // invoke-virtual {v2, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v2 ).toString ( ); // invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
final String v3 = "XSpaceManagerServiceImpl"; // const-string v3, "XSpaceManagerServiceImpl"
android.util.Slog .d ( v3,v2 );
/* .line 681 */
} // :cond_2
} // .end method
private Boolean isPublicIntent ( android.content.Intent p0, java.lang.String p1, android.content.pm.ActivityInfo p2 ) {
/* .locals 7 */
/* .param p1, "intent" # Landroid/content/Intent; */
/* .param p2, "callingPackage" # Ljava/lang/String; */
/* .param p3, "aInfo" # Landroid/content/pm/ActivityInfo; */
/* .line 347 */
v0 = android.text.TextUtils .isEmpty ( p2 );
int v1 = 0; // const/4 v1, 0x0
if ( v0 != null) { // if-eqz v0, :cond_0
/* .line 349 */
/* .line 352 */
} // :cond_0
if ( p3 != null) { // if-eqz p3, :cond_1
/* .line 353 */
v0 = this.packageName;
/* .local v0, "aimPkg":Ljava/lang/String; */
/* .line 355 */
} // .end local v0 # "aimPkg":Ljava/lang/String;
} // :cond_1
/* invoke-direct {p0, p1}, Lcom/miui/server/xspace/XSpaceManagerServiceImpl;->getAimPkg(Landroid/content/Intent;)Ljava/lang/String; */
/* .line 357 */
/* .restart local v0 # "aimPkg":Ljava/lang/String; */
} // :goto_0
v2 = android.text.TextUtils .equals ( v0,p2 );
if ( v2 != null) { // if-eqz v2, :cond_2
/* .line 358 */
/* .line 360 */
} // :cond_2
/* new-instance v2, Landroid/content/Intent; */
/* invoke-direct {v2, p1}, Landroid/content/Intent;-><init>(Landroid/content/Intent;)V */
/* .line 362 */
/* .local v2, "newIntent":Landroid/content/Intent; */
int v3 = 1; // const/4 v3, 0x1
try { // :try_start_0
android.os.BaseBundle .setShouldDefuse ( v1 );
/* .line 363 */
final String v4 = ""; // const-string v4, ""
(( android.content.Intent ) v2 ).hasExtra ( v4 ); // invoke-virtual {v2, v4}, Landroid/content/Intent;->hasExtra(Ljava/lang/String;)Z
/* :try_end_0 */
/* .catchall {:try_start_0 ..:try_end_0} :catchall_0 */
/* .line 369 */
android.os.BaseBundle .setShouldDefuse ( v3 );
/* .line 370 */
/* nop */
/* .line 371 */
/* .line 364 */
/* :catchall_0 */
/* move-exception v4 */
/* .line 366 */
/* .local v4, "e":Ljava/lang/Throwable; */
try { // :try_start_1
final String v5 = "XSpaceManagerServiceImpl"; // const-string v5, "XSpaceManagerServiceImpl"
final String v6 = "Private intent"; // const-string v6, "Private intent"
android.util.Slog .w ( v5,v6,v4 );
/* :try_end_1 */
/* .catchall {:try_start_1 ..:try_end_1} :catchall_1 */
/* .line 367 */
/* nop */
/* .line 369 */
android.os.BaseBundle .setShouldDefuse ( v3 );
/* .line 367 */
/* .line 369 */
} // .end local v4 # "e":Ljava/lang/Throwable;
/* :catchall_1 */
/* move-exception v1 */
android.os.BaseBundle .setShouldDefuse ( v3 );
/* .line 370 */
/* throw v1 */
} // .end method
private Boolean isSystemApp ( android.content.pm.ApplicationInfo p0 ) {
/* .locals 3 */
/* .param p1, "appInfo" # Landroid/content/pm/ApplicationInfo; */
/* .line 498 */
/* iget v0, p1, Landroid/content/pm/ApplicationInfo;->flags:I */
int v1 = 1; // const/4 v1, 0x1
/* and-int/2addr v0, v1 */
/* if-gtz v0, :cond_1 */
/* iget v0, p1, Landroid/content/pm/ApplicationInfo;->uid:I */
/* const/16 v2, 0x3e8 */
/* if-gt v0, v2, :cond_0 */
} // :cond_0
int v1 = 0; // const/4 v1, 0x0
} // :cond_1
} // :goto_0
} // .end method
private Boolean needSetBootXSpaceGuideTaskForCompete ( android.content.Context p0 ) {
/* .locals 2 */
/* .param p1, "context" # Landroid/content/Context; */
/* .line 651 */
final String v0 = "key_xspace_boot_guide_times"; // const-string v0, "key_xspace_boot_guide_times"
v0 = android.provider.MiuiSettings$XSpace .getGuideNotificationTimes ( p1,v0 );
int v1 = 1; // const/4 v1, 0x1
/* if-ge v0, v1, :cond_0 */
} // :cond_0
int v1 = 0; // const/4 v1, 0x0
} // :goto_0
} // .end method
private Boolean needSetInstallXSpaceGuideTaskForCompete ( android.content.Context p0 ) {
/* .locals 2 */
/* .param p1, "context" # Landroid/content/Context; */
/* .line 657 */
final String v0 = "key_xspace_compete_guide_times"; // const-string v0, "key_xspace_compete_guide_times"
v0 = android.provider.MiuiSettings$XSpace .getGuideNotificationTimes ( p1,v0 );
int v1 = 1; // const/4 v1, 0x1
/* if-ge v0, v1, :cond_0 */
} // :cond_0
int v1 = 0; // const/4 v1, 0x0
} // :goto_0
} // .end method
private void onPackageCallback ( java.lang.String p0, android.os.UserHandle p1, java.lang.String p2 ) {
/* .locals 6 */
/* .param p1, "packageName" # Ljava/lang/String; */
/* .param p2, "user" # Landroid/os/UserHandle; */
/* .param p3, "action" # Ljava/lang/String; */
/* .line 535 */
final String v0 = "XSpaceManagerServiceImpl"; // const-string v0, "XSpaceManagerServiceImpl"
/* new-instance v1, Ljava/lang/StringBuilder; */
/* invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V */
/* const-string/jumbo v2, "update XSpace App: packageName:" */
(( java.lang.StringBuilder ) v1 ).append ( v2 ); // invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v1 ).append ( p1 ); // invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
final String v2 = ", user:"; // const-string v2, ", user:"
(( java.lang.StringBuilder ) v1 ).append ( v2 ); // invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v1 ).append ( p2 ); // invoke-virtual {v1, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;
final String v2 = ", action:"; // const-string v2, ", action:"
(( java.lang.StringBuilder ) v1 ).append ( v2 ); // invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v1 ).append ( p3 ); // invoke-virtual {v1, p3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v1 ).toString ( ); // invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
android.util.Slog .w ( v0,v1 );
/* .line 536 */
v0 = miui.securityspace.XSpaceUserHandle .isXSpaceUser ( p2 );
if ( v0 != null) { // if-eqz v0, :cond_c
final String v0 = "android.intent.action.PACKAGE_ADDED"; // const-string v0, "android.intent.action.PACKAGE_ADDED"
/* .line 537 */
v0 = (( java.lang.String ) v0 ).equals ( p3 ); // invoke-virtual {v0, p3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z
/* if-nez v0, :cond_0 */
final String v0 = "android.intent.action.PACKAGE_REMOVED"; // const-string v0, "android.intent.action.PACKAGE_REMOVED"
v0 = (( java.lang.String ) v0 ).equals ( p3 ); // invoke-virtual {v0, p3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z
if ( v0 != null) { // if-eqz v0, :cond_c
/* .line 538 */
} // :cond_0
int v0 = 1; // const/4 v0, 0x1
/* iput-boolean v0, p0, Lcom/miui/server/xspace/XSpaceManagerServiceImpl;->mIsXSpaceCreated:Z */
/* .line 539 */
final String v1 = "persist.sys.xspace_created"; // const-string v1, "persist.sys.xspace_created"
final String v2 = "1"; // const-string v2, "1"
android.os.SystemProperties .set ( v1,v2 );
/* .line 540 */
final String v1 = "android.intent.action.PACKAGE_ADDED"; // const-string v1, "android.intent.action.PACKAGE_ADDED"
v1 = (( java.lang.String ) v1 ).equals ( p3 ); // invoke-virtual {v1, p3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z
/* const/16 v2, 0x3e7 */
if ( v1 != null) { // if-eqz v1, :cond_6
/* .line 541 */
/* invoke-direct {p0}, Lcom/miui/server/xspace/XSpaceManagerServiceImpl;->setXSpaceApplicationHidden()V */
/* .line 542 */
final String v1 = "com.xiaomi.gamecenter.sdk.service"; // const-string v1, "com.xiaomi.gamecenter.sdk.service"
v1 = android.text.TextUtils .equals ( v1,p1 );
/* if-nez v1, :cond_1 */
final String v1 = "org.ifaa.aidl.manager"; // const-string v1, "org.ifaa.aidl.manager"
v1 = android.text.TextUtils .equals ( v1,p1 );
if ( v1 != null) { // if-eqz v1, :cond_2
/* .line 543 */
} // :cond_1
v1 = com.miui.server.xspace.XSpaceManagerServiceImpl.sXSpaceInstalledPackagesSelfLocked;
/* monitor-enter v1 */
/* .line 544 */
try { // :try_start_0
/* invoke-direct {p0, v0}, Lcom/miui/server/xspace/XSpaceManagerServiceImpl;->updateXSpaceStatusLocked(Z)V */
/* .line 545 */
(( java.util.ArrayList ) v1 ).add ( p1 ); // invoke-virtual {v1, p1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z
/* .line 546 */
/* monitor-exit v1 */
/* :try_end_0 */
/* .catchall {:try_start_0 ..:try_end_0} :catchall_1 */
/* .line 548 */
} // :cond_2
int v1 = 0; // const/4 v1, 0x0
/* .line 550 */
/* .local v1, "isSystemApp":Z */
try { // :try_start_1
android.app.AppGlobals .getPackageManager ( );
/* .line 551 */
/* const-wide/16 v4, 0x0 */
/* .line 552 */
/* .local v2, "appInfo":Landroid/content/pm/ApplicationInfo; */
/* if-nez v2, :cond_3 */
/* .line 553 */
return;
/* .line 555 */
} // :cond_3
v3 = /* invoke-direct {p0, v2}, Lcom/miui/server/xspace/XSpaceManagerServiceImpl;->isSystemApp(Landroid/content/pm/ApplicationInfo;)Z */
/* :try_end_1 */
/* .catch Landroid/os/RemoteException; {:try_start_1 ..:try_end_1} :catch_0 */
/* move v1, v3 */
/* .line 558 */
} // .end local v2 # "appInfo":Landroid/content/pm/ApplicationInfo;
/* .line 556 */
/* :catch_0 */
/* move-exception v2 */
/* .line 557 */
/* .local v2, "e":Landroid/os/RemoteException; */
final String v3 = "XSpaceManagerServiceImpl"; // const-string v3, "XSpaceManagerServiceImpl"
final String v4 = "PMS died"; // const-string v4, "PMS died"
android.util.Slog .d ( v3,v4,v2 );
/* .line 559 */
} // .end local v2 # "e":Landroid/os/RemoteException;
} // :goto_0
/* if-nez v1, :cond_5 */
v2 = miui.securityspace.XSpaceConstant.GMS_RELATED_APPS;
v2 = (( java.util.ArrayList ) v2 ).contains ( p1 ); // invoke-virtual {v2, p1}, Ljava/util/ArrayList;->contains(Ljava/lang/Object;)Z
if ( v2 != null) { // if-eqz v2, :cond_4
/* .line 564 */
} // :cond_4
v2 = com.miui.server.xspace.XSpaceManagerServiceImpl.sXSpaceInstalledPackagesSelfLocked;
/* monitor-enter v2 */
/* .line 565 */
try { // :try_start_2
/* invoke-direct {p0, v0}, Lcom/miui/server/xspace/XSpaceManagerServiceImpl;->updateXSpaceStatusLocked(Z)V */
/* .line 566 */
(( java.util.ArrayList ) v2 ).add ( p1 ); // invoke-virtual {v2, p1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z
/* .line 567 */
/* monitor-exit v2 */
/* .line 568 */
} // .end local v1 # "isSystemApp":Z
/* goto/16 :goto_5 */
/* .line 567 */
/* .restart local v1 # "isSystemApp":Z */
/* :catchall_0 */
/* move-exception v0 */
/* monitor-exit v2 */
/* :try_end_2 */
/* .catchall {:try_start_2 ..:try_end_2} :catchall_0 */
/* throw v0 */
/* .line 561 */
} // :cond_5
} // :goto_1
final String v0 = "XSpaceManagerServiceImpl"; // const-string v0, "XSpaceManagerServiceImpl"
/* new-instance v2, Ljava/lang/StringBuilder; */
/* invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V */
final String v3 = "XSpace ignore system or GMS package: "; // const-string v3, "XSpace ignore system or GMS package: "
(( java.lang.StringBuilder ) v2 ).append ( v3 ); // invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v2 ).append ( p1 ); // invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v2 ).toString ( ); // invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
android.util.Slog .d ( v0,v2 );
/* .line 562 */
return;
/* .line 546 */
} // .end local v1 # "isSystemApp":Z
/* :catchall_1 */
/* move-exception v0 */
try { // :try_start_3
/* monitor-exit v1 */
/* :try_end_3 */
/* .catchall {:try_start_3 ..:try_end_3} :catchall_1 */
/* throw v0 */
/* .line 569 */
} // :cond_6
v0 = this.mContext;
android.provider.MiuiSettings$XSpace .resetDefaultSetting ( v0,p1 );
/* .line 570 */
v0 = com.miui.server.xspace.XSpaceManagerServiceImpl.sXSpaceInstalledPackagesSelfLocked;
/* monitor-enter v0 */
/* .line 571 */
try { // :try_start_4
(( java.util.ArrayList ) v0 ).remove ( p1 ); // invoke-virtual {v0, p1}, Ljava/util/ArrayList;->remove(Ljava/lang/Object;)Z
/* .line 572 */
int v1 = 0; // const/4 v1, 0x0
/* invoke-direct {p0, v1}, Lcom/miui/server/xspace/XSpaceManagerServiceImpl;->updateXSpaceStatusLocked(Z)V */
/* .line 573 */
/* monitor-exit v0 */
/* :try_end_4 */
/* .catchall {:try_start_4 ..:try_end_4} :catchall_2 */
/* .line 574 */
v0 = this.mContext;
(( android.content.Context ) v0 ).getPackageManager ( ); // invoke-virtual {v0}, Landroid/content/Context;->getPackageManager()Landroid/content/pm/PackageManager;
/* .line 575 */
/* const/16 v1, 0x40 */
(( android.content.pm.PackageManager ) v0 ).getInstalledPackagesAsUser ( v1, v2 ); // invoke-virtual {v0, v1, v2}, Landroid/content/pm/PackageManager;->getInstalledPackagesAsUser(II)Ljava/util/List;
/* .line 576 */
/* .local v0, "appList":Ljava/util/List;, "Ljava/util/List<Landroid/content/pm/PackageInfo;>;" */
v1 = if ( v0 != null) { // if-eqz v0, :cond_b
if ( v1 != null) { // if-eqz v1, :cond_7
/* .line 579 */
} // :cond_7
int v1 = 1; // const/4 v1, 0x1
/* .line 580 */
/* .local v1, "needRemoveGMS":Z */
v3 = } // :goto_2
if ( v3 != null) { // if-eqz v3, :cond_9
/* check-cast v3, Landroid/content/pm/PackageInfo; */
/* .line 581 */
/* .local v3, "packageInfo":Landroid/content/pm/PackageInfo; */
v4 = this.mContext;
v5 = this.packageName;
v4 = miui.securityspace.XSpaceUtils .isGMSRequired ( v4,v5 );
if ( v4 != null) { // if-eqz v4, :cond_8
/* .line 582 */
int v1 = 0; // const/4 v1, 0x0
/* .line 583 */
/* .line 585 */
} // .end local v3 # "packageInfo":Landroid/content/pm/PackageInfo;
} // :cond_8
/* .line 586 */
} // :cond_9
} // :goto_3
if ( v1 != null) { // if-eqz v1, :cond_a
/* .line 587 */
/* invoke-direct {p0}, Lcom/miui/server/xspace/XSpaceManagerServiceImpl;->disableGMSApps()V */
/* .line 589 */
} // .end local v0 # "appList":Ljava/util/List;, "Ljava/util/List<Landroid/content/pm/PackageInfo;>;"
} // .end local v1 # "needRemoveGMS":Z
} // :cond_a
/* .line 577 */
/* .restart local v0 # "appList":Ljava/util/List;, "Ljava/util/List<Landroid/content/pm/PackageInfo;>;" */
} // :cond_b
} // :goto_4
return;
/* .line 573 */
} // .end local v0 # "appList":Ljava/util/List;, "Ljava/util/List<Landroid/content/pm/PackageInfo;>;"
/* :catchall_2 */
/* move-exception v1 */
try { // :try_start_5
/* monitor-exit v0 */
/* :try_end_5 */
/* .catchall {:try_start_5 ..:try_end_5} :catchall_2 */
/* throw v1 */
/* .line 590 */
} // :cond_c
v0 = (( android.os.UserHandle ) p2 ).getIdentifier ( ); // invoke-virtual {p2}, Landroid/os/UserHandle;->getIdentifier()I
/* if-nez v0, :cond_d */
final String v0 = "android.intent.action.PACKAGE_ADDED"; // const-string v0, "android.intent.action.PACKAGE_ADDED"
/* .line 591 */
v0 = (( java.lang.String ) v0 ).equals ( p3 ); // invoke-virtual {v0, p3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z
if ( v0 != null) { // if-eqz v0, :cond_d
v0 = android.provider.MiuiSettings$XSpace.sCompeteXSpaceApps;
/* .line 592 */
v0 = (( java.util.ArrayList ) v0 ).contains ( p1 ); // invoke-virtual {v0, p1}, Ljava/util/ArrayList;->contains(Ljava/lang/Object;)Z
if ( v0 != null) { // if-eqz v0, :cond_d
v0 = this.mContext;
/* .line 593 */
v0 = /* invoke-direct {p0, v0}, Lcom/miui/server/xspace/XSpaceManagerServiceImpl;->needSetInstallXSpaceGuideTaskForCompete(Landroid/content/Context;)Z */
if ( v0 != null) { // if-eqz v0, :cond_d
v0 = this.mContext;
/* .line 594 */
v0 = miui.securityspace.CrossUserUtils .hasXSpaceUser ( v0 );
/* if-nez v0, :cond_d */
/* .line 595 */
v0 = this.mContext;
final String v1 = "param_intent_value_compete_install_xspace_guide"; // const-string v1, "param_intent_value_compete_install_xspace_guide"
/* invoke-direct {p0, v0, v1}, Lcom/miui/server/xspace/XSpaceManagerServiceImpl;->startXSpaceService(Landroid/content/Context;Ljava/lang/String;)V */
/* .line 597 */
} // :cond_d
} // :goto_5
return;
} // .end method
private void putCachedCallingRelation ( java.lang.String p0, java.lang.String p1, Integer p2 ) {
/* .locals 5 */
/* .param p1, "aimPkg" # Ljava/lang/String; */
/* .param p2, "callingPackage" # Ljava/lang/String; */
/* .param p3, "cachedUserId" # I */
/* .line 375 */
v0 = com.miui.server.xspace.XSpaceManagerServiceImpl.sCachedCallingRelationSelfLocked;
/* monitor-enter v0 */
/* .line 376 */
try { // :try_start_0
/* new-instance v1, Ljava/lang/StringBuilder; */
/* invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V */
(( java.lang.StringBuilder ) v1 ).append ( p1 ); // invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
final String v2 = "@"; // const-string v2, "@"
(( java.lang.StringBuilder ) v1 ).append ( v2 ); // invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v1 ).append ( p2 ); // invoke-virtual {v1, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v1 ).toString ( ); // invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
/* .line 377 */
/* .local v1, "callingRelationKey":Ljava/lang/String; */
java.lang.Integer .valueOf ( p3 );
(( java.util.HashMap ) v0 ).put ( v1, v2 ); // invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;
/* .line 378 */
final String v2 = "XSpaceManagerServiceImpl"; // const-string v2, "XSpaceManagerServiceImpl"
/* new-instance v3, Ljava/lang/StringBuilder; */
/* invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V */
final String v4 = "putCachedCallingRelationm, callingRelationKey:"; // const-string v4, "putCachedCallingRelationm, callingRelationKey:"
(( java.lang.StringBuilder ) v3 ).append ( v4 ); // invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v3 ).append ( v1 ); // invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
final String v4 = ", cachedUserId:"; // const-string v4, ", cachedUserId:"
(( java.lang.StringBuilder ) v3 ).append ( v4 ); // invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v3 ).append ( p3 ); // invoke-virtual {v3, p3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v3 ).toString ( ); // invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
android.util.Slog .w ( v2,v3 );
/* .line 379 */
/* nop */
} // .end local v1 # "callingRelationKey":Ljava/lang/String;
/* monitor-exit v0 */
/* .line 380 */
return;
/* .line 379 */
/* :catchall_0 */
/* move-exception v1 */
/* monitor-exit v0 */
/* :try_end_0 */
/* .catchall {:try_start_0 ..:try_end_0} :catchall_0 */
/* throw v1 */
} // .end method
private android.content.pm.ActivityInfo resolveActivity ( android.content.Intent p0, com.android.server.wm.ActivityTaskSupervisor p1, java.lang.String p2, Integer p3, android.app.ProfilerInfo p4, Integer p5, Integer p6, Integer p7 ) {
/* .locals 13 */
/* .param p1, "intent" # Landroid/content/Intent; */
/* .param p2, "stack" # Lcom/android/server/wm/ActivityTaskSupervisor; */
/* .param p3, "resolvedType" # Ljava/lang/String; */
/* .param p4, "startFlags" # I */
/* .param p5, "profilerInfo" # Landroid/app/ProfilerInfo; */
/* .param p6, "userId" # I */
/* .param p7, "callingUid" # I */
/* .param p8, "callingPid" # I */
/* .line 889 */
int v1 = 0; // const/4 v1, 0x0
/* .line 891 */
/* .local v1, "activityInfo":Landroid/content/pm/ActivityInfo; */
try { // :try_start_0
/* const-class v0, Landroid/content/pm/ActivityInfo; */
final String v2 = "resolveActivity"; // const-string v2, "resolveActivity"
int v3 = 7; // const/4 v3, 0x7
/* new-array v4, v3, [Ljava/lang/Class; */
/* const-class v5, Landroid/content/Intent; */
int v6 = 0; // const/4 v6, 0x0
/* aput-object v5, v4, v6 */
/* const-class v5, Ljava/lang/String; */
int v7 = 1; // const/4 v7, 0x1
/* aput-object v5, v4, v7 */
v5 = java.lang.Integer.TYPE;
int v8 = 2; // const/4 v8, 0x2
/* aput-object v5, v4, v8 */
/* const-class v5, Landroid/app/ProfilerInfo; */
int v9 = 3; // const/4 v9, 0x3
/* aput-object v5, v4, v9 */
v5 = java.lang.Integer.TYPE;
int v10 = 4; // const/4 v10, 0x4
/* aput-object v5, v4, v10 */
v5 = java.lang.Integer.TYPE;
int v11 = 5; // const/4 v11, 0x5
/* aput-object v5, v4, v11 */
v5 = java.lang.Integer.TYPE;
int v12 = 6; // const/4 v12, 0x6
/* aput-object v5, v4, v12 */
/* new-array v3, v3, [Ljava/lang/Object; */
/* aput-object p1, v3, v6 */
/* aput-object p3, v3, v7 */
/* .line 893 */
/* invoke-static/range {p4 ..p4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer; */
/* aput-object v5, v3, v8 */
/* aput-object p5, v3, v9 */
/* invoke-static/range {p6 ..p6}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer; */
/* aput-object v5, v3, v10 */
/* invoke-static/range {p7 ..p7}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer; */
/* aput-object v5, v3, v11 */
/* invoke-static/range {p8 ..p8}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer; */
/* aput-object v5, v3, v12 */
/* :try_end_0 */
/* .catch Ljava/lang/Exception; {:try_start_0 ..:try_end_0} :catch_1 */
/* .line 891 */
/* move-object v5, p2 */
try { // :try_start_1
com.miui.server.xspace.ReflectUtil .callObjectMethod ( p2,v0,v2,v4,v3 );
/* check-cast v0, Landroid/content/pm/ActivityInfo; */
/* :try_end_1 */
/* .catch Ljava/lang/Exception; {:try_start_1 ..:try_end_1} :catch_0 */
/* move-object v1, v0 */
/* .line 896 */
/* .line 894 */
/* :catch_0 */
/* move-exception v0 */
/* :catch_1 */
/* move-exception v0 */
/* move-object v5, p2 */
/* .line 895 */
/* .local v0, "exception":Ljava/lang/Exception; */
} // :goto_0
final String v2 = "XSpaceManagerServiceImpl"; // const-string v2, "XSpaceManagerServiceImpl"
final String v3 = "Call resolveIntent fail."; // const-string v3, "Call resolveIntent fail."
android.util.Slog .e ( v2,v3,v0 );
/* .line 898 */
} // .end local v0 # "exception":Ljava/lang/Exception;
} // :goto_1
} // .end method
private void setSwitchUserCallingUid ( Integer p0 ) {
/* .locals 0 */
/* .param p1, "uid" # I */
/* .line 836 */
/* .line 837 */
return;
} // .end method
private void setXSpaceApplicationHidden ( ) {
/* .locals 5 */
/* .line 615 */
/* sget-boolean v0, Lmiui/os/Build;->IS_INTERNATIONAL_BUILD:Z */
/* if-nez v0, :cond_0 */
/* .line 616 */
return;
/* .line 619 */
} // :cond_0
try { // :try_start_0
v0 = com.miui.server.xspace.XSpaceManagerServiceImpl.sXSpaceApplicationList;
(( java.util.ArrayList ) v0 ).iterator ( ); // invoke-virtual {v0}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;
v1 = } // :goto_0
if ( v1 != null) { // if-eqz v1, :cond_2
/* check-cast v1, Ljava/lang/String; */
/* .line 620 */
/* .local v1, "pkgName":Ljava/lang/String; */
android.app.AppGlobals .getPackageManager ( );
v2 = /* const/16 v3, 0x3e7 */
/* if-nez v2, :cond_1 */
/* .line 621 */
android.app.AppGlobals .getPackageManager ( );
int v4 = 1; // const/4 v4, 0x1
/* :try_end_0 */
/* .catch Landroid/os/RemoteException; {:try_start_0 ..:try_end_0} :catch_0 */
/* .line 623 */
} // .end local v1 # "pkgName":Ljava/lang/String;
} // :cond_1
/* .line 626 */
} // :cond_2
/* .line 624 */
/* :catch_0 */
/* move-exception v0 */
/* .line 625 */
/* .local v0, "e":Landroid/os/RemoteException; */
final String v1 = "XSpaceManagerServiceImpl"; // const-string v1, "XSpaceManagerServiceImpl"
final String v2 = "hidden xspace error"; // const-string v2, "hidden xspace error"
android.util.Slog .d ( v1,v2,v0 );
/* .line 627 */
} // .end local v0 # "e":Landroid/os/RemoteException;
} // :goto_1
return;
} // .end method
private Boolean shouldResolveAgain ( android.content.Intent p0, java.lang.String p1 ) {
/* .locals 6 */
/* .param p1, "intent" # Landroid/content/Intent; */
/* .param p2, "callingPackage" # Ljava/lang/String; */
/* .line 316 */
final String v0 = "android.intent.extra.xspace_resolve_intent_again"; // const-string v0, "android.intent.extra.xspace_resolve_intent_again"
/* invoke-direct {p0, p1}, Lcom/miui/server/xspace/XSpaceManagerServiceImpl;->getAimPkg(Landroid/content/Intent;)Ljava/lang/String; */
/* .line 317 */
/* .local v1, "aimPkg":Ljava/lang/String; */
/* new-instance v2, Landroid/content/Intent; */
/* invoke-direct {v2, p1}, Landroid/content/Intent;-><init>(Landroid/content/Intent;)V */
/* .line 319 */
/* .local v2, "newIntent":Landroid/content/Intent; */
int v3 = 0; // const/4 v3, 0x0
try { // :try_start_0
v4 = android.text.TextUtils .equals ( v1,p2 );
/* if-nez v4, :cond_0 */
/* .line 320 */
v4 = (( android.content.Intent ) v2 ).hasExtra ( v0 ); // invoke-virtual {v2, v0}, Landroid/content/Intent;->hasExtra(Ljava/lang/String;)Z
if ( v4 != null) { // if-eqz v4, :cond_0
/* .line 321 */
(( android.content.Intent ) p1 ).removeExtra ( v0 ); // invoke-virtual {p1, v0}, Landroid/content/Intent;->removeExtra(Ljava/lang/String;)V
/* :try_end_0 */
/* .catch Ljava/lang/Exception; {:try_start_0 ..:try_end_0} :catch_0 */
/* .line 322 */
int v0 = 1; // const/4 v0, 0x1
/* .line 328 */
} // :cond_0
/* nop */
/* .line 329 */
/* .line 324 */
/* :catch_0 */
/* move-exception v0 */
/* .line 326 */
/* .local v0, "e":Ljava/lang/Exception; */
final String v4 = "XSpaceManagerServiceImpl"; // const-string v4, "XSpaceManagerServiceImpl"
final String v5 = "Private intent: "; // const-string v5, "Private intent: "
android.util.Slog .w ( v4,v5,v0 );
/* .line 327 */
} // .end method
private void startXSpaceService ( android.content.Context p0, java.lang.String p1 ) {
/* .locals 2 */
/* .param p1, "context" # Landroid/content/Context; */
/* .param p2, "extra" # Ljava/lang/String; */
/* .line 640 */
/* new-instance v0, Landroid/content/Intent; */
/* invoke-direct {v0}, Landroid/content/Intent;-><init>()V */
/* .line 641 */
/* .local v0, "intent":Landroid/content/Intent; */
final String v1 = "com.miui.securitycore/com.miui.xspace.service.XSpaceService"; // const-string v1, "com.miui.securitycore/com.miui.xspace.service.XSpaceService"
android.content.ComponentName .unflattenFromString ( v1 );
(( android.content.Intent ) v0 ).setComponent ( v1 ); // invoke-virtual {v0, v1}, Landroid/content/Intent;->setComponent(Landroid/content/ComponentName;)Landroid/content/Intent;
/* .line 643 */
if ( p2 != null) { // if-eqz p2, :cond_0
/* .line 644 */
final String v1 = "param_intent_key_has_extra"; // const-string v1, "param_intent_key_has_extra"
(( android.content.Intent ) v0 ).putExtra ( v1, p2 ); // invoke-virtual {v0, v1, p2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;
/* .line 646 */
} // :cond_0
(( android.content.Context ) p1 ).startService ( v0 ); // invoke-virtual {p1, v0}, Landroid/content/Context;->startService(Landroid/content/Intent;)Landroid/content/ComponentName;
/* .line 647 */
return;
} // .end method
private void updateXSpaceStatusLocked ( Boolean p0 ) {
/* .locals 4 */
/* .param p1, "isXSpaceActive" # Z */
/* .line 631 */
v0 = com.miui.server.xspace.XSpaceManagerServiceImpl.sXSpaceInstalledPackagesSelfLocked;
v1 = (( java.util.ArrayList ) v0 ).size ( ); // invoke-virtual {v0}, Ljava/util/ArrayList;->size()I
/* if-ne v1, v2, :cond_0 */
/* .line 632 */
/* new-instance v1, Ljava/lang/StringBuilder; */
/* invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V */
/* const-string/jumbo v3, "updateXSpaceStatusLocked sXSpaceInstalledPackagesSelfLocked =" */
(( java.lang.StringBuilder ) v1 ).append ( v3 ); // invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.util.ArrayList ) v0 ).toString ( ); // invoke-virtual {v0}, Ljava/util/ArrayList;->toString()Ljava/lang/String;
(( java.lang.StringBuilder ) v1 ).append ( v0 ); // invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
final String v1 = " XSPACE_APP_LIST_INIT_NUMBER ="; // const-string v1, " XSPACE_APP_LIST_INIT_NUMBER ="
(( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v0 ).append ( v2 ); // invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v0 ).toString ( ); // invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
final String v1 = "XSpaceManagerServiceImpl"; // const-string v1, "XSpaceManagerServiceImpl"
android.util.Slog .d ( v1,v0 );
/* .line 634 */
/* new-instance v0, Ljava/lang/StringBuilder; */
/* invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V */
/* const-string/jumbo v2, "update XSpace Enable = " */
(( java.lang.StringBuilder ) v0 ).append ( v2 ); // invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v0 ).append ( p1 ); // invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v0 ).toString ( ); // invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
android.util.Slog .w ( v1,v0 );
/* .line 635 */
v0 = this.mResolver;
/* const-string/jumbo v1, "xspace_enabled" */
android.provider.MiuiSettings$Secure .putBoolean ( v0,v1,p1 );
/* .line 637 */
} // :cond_0
return;
} // .end method
/* # virtual methods */
public Integer checkAndGetXSpaceUserId ( Integer p0, Integer p1 ) {
/* .locals 1 */
/* .param p1, "flags" # I */
/* .param p2, "defUserId" # I */
/* .line 767 */
v0 = miui.securityspace.XSpaceUserHandle .checkAndGetXSpaceUserId ( p1,p2 );
} // .end method
public Boolean checkExternalStorageForXSpace ( android.content.Context p0, Integer p1, java.lang.String p2 ) {
/* .locals 4 */
/* .param p1, "context" # Landroid/content/Context; */
/* .param p2, "uid" # I */
/* .param p3, "pkgName" # Ljava/lang/String; */
/* .line 736 */
v0 = miui.securityspace.XSpaceUserHandle .isUidBelongtoXSpace ( p2 );
int v1 = 0; // const/4 v1, 0x0
/* if-nez v0, :cond_0 */
/* .line 737 */
/* .line 741 */
} // :cond_0
try { // :try_start_0
(( android.content.Context ) p1 ).getPackageManager ( ); // invoke-virtual {p1}, Landroid/content/Context;->getPackageManager()Landroid/content/pm/PackageManager;
(( android.content.pm.PackageManager ) v0 ).getApplicationInfo ( p3, v1 ); // invoke-virtual {v0, p3, v1}, Landroid/content/pm/PackageManager;->getApplicationInfo(Ljava/lang/String;I)Landroid/content/pm/ApplicationInfo;
/* :try_end_0 */
/* .catch Landroid/content/pm/PackageManager$NameNotFoundException; {:try_start_0 ..:try_end_0} :catch_0 */
/* .line 745 */
/* .local v0, "appInfo":Landroid/content/pm/ApplicationInfo; */
/* nop */
/* .line 746 */
/* iget v2, v0, Landroid/content/pm/ApplicationInfo;->flags:I */
int v3 = 1; // const/4 v3, 0x1
/* and-int/2addr v2, v3 */
/* if-lez v2, :cond_1 */
/* move v1, v3 */
} // :cond_1
/* .line 742 */
} // .end local v0 # "appInfo":Landroid/content/pm/ApplicationInfo;
/* :catch_0 */
/* move-exception v0 */
/* .line 743 */
/* .local v0, "e":Landroid/content/pm/PackageManager$NameNotFoundException; */
/* new-instance v2, Ljava/lang/StringBuilder; */
/* invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V */
final String v3 = "Failed to get package info for "; // const-string v3, "Failed to get package info for "
(( java.lang.StringBuilder ) v2 ).append ( v3 ); // invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v2 ).append ( p3 ); // invoke-virtual {v2, p3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v2 ).toString ( ); // invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
final String v3 = "XSpaceManagerServiceImpl"; // const-string v3, "XSpaceManagerServiceImpl"
android.util.Slog .e ( v3,v2,v0 );
/* .line 744 */
} // .end method
public android.content.Intent checkXSpaceControl ( android.content.Context p0, android.content.pm.ActivityInfo p1, android.content.Intent p2, Boolean p3, Integer p4, Integer p5, java.lang.String p6, Integer p7, com.android.server.wm.SafeActivityOptions p8 ) {
/* .locals 14 */
/* .param p1, "context" # Landroid/content/Context; */
/* .param p2, "aInfo" # Landroid/content/pm/ActivityInfo; */
/* .param p3, "intent" # Landroid/content/Intent; */
/* .param p4, "fromActivity" # Z */
/* .param p5, "requestCode" # I */
/* .param p6, "userId" # I */
/* .param p7, "callingPackage" # Ljava/lang/String; */
/* .param p8, "callingUserId" # I */
/* .param p9, "safeActivityOptions" # Lcom/android/server/wm/SafeActivityOptions; */
/* .line 173 */
/* move-object v1, p0 */
/* move-object/from16 v2, p2 */
/* move-object/from16 v3, p3 */
/* move/from16 v4, p6 */
/* move-object/from16 v5, p7 */
/* move/from16 v6, p8 */
v0 = /* invoke-direct {p0, v3, v5, v2}, Lcom/miui/server/xspace/XSpaceManagerServiceImpl;->isPublicIntent(Landroid/content/Intent;Ljava/lang/String;Landroid/content/pm/ActivityInfo;)Z */
/* if-nez v0, :cond_0 */
/* .line 174 */
/* .line 178 */
} // :cond_0
/* invoke-virtual/range {p3 ..p3}, Landroid/content/Intent;->getAction()Ljava/lang/String; */
/* .line 179 */
/* .local v7, "action":Ljava/lang/String; */
if ( v2 != null) { // if-eqz v2, :cond_1
/* .line 180 */
v0 = this.packageName;
/* move-object v8, v0 */
/* .local v0, "aimPkg":Ljava/lang/String; */
/* .line 182 */
} // .end local v0 # "aimPkg":Ljava/lang/String;
} // :cond_1
/* invoke-direct {p0, v3}, Lcom/miui/server/xspace/XSpaceManagerServiceImpl;->getAimPkg(Landroid/content/Intent;)Ljava/lang/String; */
/* move-object v8, v0 */
/* .line 184 */
/* .local v8, "aimPkg":Ljava/lang/String; */
} // :goto_0
final String v0 = "XSpaceManagerServiceImpl"; // const-string v0, "XSpaceManagerServiceImpl"
/* new-instance v9, Ljava/lang/StringBuilder; */
/* invoke-direct {v9}, Ljava/lang/StringBuilder;-><init>()V */
final String v10 = "checkXSpaceControl, from:"; // const-string v10, "checkXSpaceControl, from:"
(( java.lang.StringBuilder ) v9 ).append ( v10 ); // invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v9 ).append ( v5 ); // invoke-virtual {v9, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
final String v10 = ", to:"; // const-string v10, ", to:"
(( java.lang.StringBuilder ) v9 ).append ( v10 ); // invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v9 ).append ( v8 ); // invoke-virtual {v9, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
final String v10 = ", with act:"; // const-string v10, ", with act:"
(( java.lang.StringBuilder ) v9 ).append ( v10 ); // invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v9 ).append ( v7 ); // invoke-virtual {v9, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
final String v10 = ", callingUserId:"; // const-string v10, ", callingUserId:"
(( java.lang.StringBuilder ) v9 ).append ( v10 ); // invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v9 ).append ( v6 ); // invoke-virtual {v9, v6}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;
final String v10 = ", toUserId:"; // const-string v10, ", toUserId:"
(( java.lang.StringBuilder ) v9 ).append ( v10 ); // invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v9 ).append ( v4 ); // invoke-virtual {v9, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v9 ).toString ( ); // invoke-virtual {v9}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
android.util.Slog .w ( v0,v9 );
/* .line 187 */
/* const/16 v0, 0x3e7 */
if ( v6 != null) { // if-eqz v6, :cond_2
/* if-eq v6, v0, :cond_2 */
/* .line 189 */
/* .line 191 */
} // :cond_2
/* if-nez v6, :cond_3 */
/* if-nez v4, :cond_3 */
v9 = com.miui.server.xspace.XSpaceManagerServiceImpl.sXSpaceInstalledPackagesSelfLocked;
/* .line 192 */
v10 = (( java.util.ArrayList ) v9 ).contains ( v8 ); // invoke-virtual {v9, v8}, Ljava/util/ArrayList;->contains(Ljava/lang/Object;)Z
/* if-nez v10, :cond_3 */
/* .line 193 */
v9 = (( java.util.ArrayList ) v9 ).contains ( v5 ); // invoke-virtual {v9, v5}, Ljava/util/ArrayList;->contains(Ljava/lang/Object;)Z
/* if-nez v9, :cond_3 */
/* .line 195 */
/* .line 197 */
} // :cond_3
v9 = /* invoke-virtual/range {p3 ..p3}, Landroid/content/Intent;->getFlags()I */
/* const/high16 v10, 0x100000 */
/* and-int/2addr v9, v10 */
if ( v9 != null) { // if-eqz v9, :cond_4
/* .line 199 */
/* .line 201 */
} // :cond_4
/* invoke-virtual/range {p3 ..p3}, Landroid/content/Intent;->getComponent()Landroid/content/ComponentName; */
if ( v9 != null) { // if-eqz v9, :cond_5
/* .line 202 */
/* const-class v9, Landroid/content/pm/PackageManagerInternal; */
com.android.server.LocalServices .getService ( v9 );
/* check-cast v9, Landroid/content/pm/PackageManagerInternal; */
/* .line 203 */
/* .local v9, "packageManagerInternal":Landroid/content/pm/PackageManagerInternal; */
(( android.content.pm.PackageManagerInternal ) v9 ).getDisabledComponents ( v8, v0 ); // invoke-virtual {v9, v8, v0}, Landroid/content/pm/PackageManagerInternal;->getDisabledComponents(Ljava/lang/String;I)Landroid/util/ArraySet;
/* .line 204 */
/* .local v10, "disabledComponents":Landroid/util/ArraySet;, "Landroid/util/ArraySet<Ljava/lang/String;>;" */
if ( v10 != null) { // if-eqz v10, :cond_5
/* invoke-virtual/range {p3 ..p3}, Landroid/content/Intent;->getComponent()Landroid/content/ComponentName; */
(( android.content.ComponentName ) v11 ).getClassName ( ); // invoke-virtual {v11}, Landroid/content/ComponentName;->getClassName()Ljava/lang/String;
v11 = (( android.util.ArraySet ) v10 ).contains ( v11 ); // invoke-virtual {v10, v11}, Landroid/util/ArraySet;->contains(Ljava/lang/Object;)Z
if ( v11 != null) { // if-eqz v11, :cond_5
/* .line 205 */
/* .line 208 */
} // .end local v9 # "packageManagerInternal":Landroid/content/pm/PackageManagerInternal;
} // .end local v10 # "disabledComponents":Landroid/util/ArraySet;, "Landroid/util/ArraySet<Ljava/lang/String;>;"
} // :cond_5
v9 = com.miui.server.xspace.XSpaceManagerServiceImpl.sCrossUserCallingPackagesWhiteList;
v10 = (( java.util.ArrayList ) v9 ).contains ( v5 ); // invoke-virtual {v9, v5}, Ljava/util/ArrayList;->contains(Ljava/lang/Object;)Z
if ( v10 != null) { // if-eqz v10, :cond_6
v10 = com.miui.server.xspace.XSpaceManagerServiceImpl.sPublicActionList;
v10 = (( java.util.ArrayList ) v10 ).contains ( v7 ); // invoke-virtual {v10, v7}, Ljava/util/ArrayList;->contains(Ljava/lang/Object;)Z
if ( v10 != null) { // if-eqz v10, :cond_7
} // :cond_6
v10 = com.miui.server.xspace.XSpaceManagerServiceImpl.sCrossUserAimPackagesWhiteList;
/* .line 209 */
v10 = (( java.util.ArrayList ) v10 ).contains ( v8 ); // invoke-virtual {v10, v8}, Ljava/util/ArrayList;->contains(Ljava/lang/Object;)Z
if ( v10 != null) { // if-eqz v10, :cond_8
/* .line 211 */
} // :cond_7
/* .line 214 */
} // :cond_8
v10 = com.miui.server.xspace.XSpaceManagerServiceImpl.sCrossUserDisableComponentActionWhiteList;
v10 = (( java.util.ArrayList ) v10 ).contains ( v7 ); // invoke-virtual {v10, v7}, Ljava/util/ArrayList;->contains(Ljava/lang/Object;)Z
if ( v10 != null) { // if-eqz v10, :cond_9
/* .line 215 */
v10 = /* invoke-direct {p0, v2, v0}, Lcom/miui/server/xspace/XSpaceManagerServiceImpl;->isComponentEnabled(Landroid/content/pm/ActivityInfo;I)Z */
/* if-nez v10, :cond_9 */
/* .line 216 */
/* .line 219 */
} // :cond_9
final String v10 = "android.intent.extra.xspace_userid_selected"; // const-string v10, "android.intent.extra.xspace_userid_selected"
v10 = (( android.content.Intent ) v3 ).hasExtra ( v10 ); // invoke-virtual {v3, v10}, Landroid/content/Intent;->hasExtra(Ljava/lang/String;)Z
if ( v10 != null) { // if-eqz v10, :cond_a
/* .line 221 */
final String v0 = "XSpaceManagerServiceImpl"; // const-string v0, "XSpaceManagerServiceImpl"
final String v9 = "from XSpace ResolverActivity"; // const-string v9, "from XSpace ResolverActivity"
android.util.Slog .w ( v0,v9 );
/* .line 222 */
final String v0 = "android.intent.extra.xspace_resolve_intent_again"; // const-string v0, "android.intent.extra.xspace_resolve_intent_again"
(( android.content.Intent ) v3 ).removeExtra ( v0 ); // invoke-virtual {v3, v0}, Landroid/content/Intent;->removeExtra(Ljava/lang/String;)V
/* .line 223 */
miui.securityspace.XSpaceIntentCompat .prepareToLeaveUser ( v3,v6 );
/* .line 224 */
/* invoke-direct {p0, v8, v5, v6}, Lcom/miui/server/xspace/XSpaceManagerServiceImpl;->putCachedCallingRelation(Ljava/lang/String;Ljava/lang/String;I)V */
/* .line 227 */
} // :cond_a
v10 = /* invoke-direct {p0, v8, v5}, Lcom/miui/server/xspace/XSpaceManagerServiceImpl;->getToUserIdFromCachedCallingRelation(Ljava/lang/String;Ljava/lang/String;)I */
/* .line 228 */
/* .local v10, "cachedToUserId":I */
/* const/16 v11, -0x2710 */
/* if-eq v10, v11, :cond_b */
/* .line 230 */
final String v0 = "XSpaceManagerServiceImpl"; // const-string v0, "XSpaceManagerServiceImpl"
/* const-string/jumbo v9, "using cached calling relation" */
android.util.Slog .w ( v0,v9 );
/* .line 231 */
final String v0 = "android.intent.extra.xspace_cached_uid"; // const-string v0, "android.intent.extra.xspace_cached_uid"
(( android.content.Intent ) v3 ).putExtra ( v0, v10 ); // invoke-virtual {v3, v0, v10}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;
/* .line 267 */
} // .end local v10 # "cachedToUserId":I
} // :goto_1
/* move/from16 v12, p5 */
/* goto/16 :goto_6 */
/* .line 234 */
/* .restart local v10 # "cachedToUserId":I */
} // :cond_b
v11 = com.miui.server.xspace.XSpaceManagerServiceImpl.sXSpaceInstalledPackagesSelfLocked;
/* monitor-enter v11 */
/* .line 235 */
try { // :try_start_0
v12 = (( java.util.ArrayList ) v11 ).contains ( v8 ); // invoke-virtual {v11, v8}, Ljava/util/ArrayList;->contains(Ljava/lang/Object;)Z
if ( v12 != null) { // if-eqz v12, :cond_14
/* .line 236 */
final String v0 = "android.intent.extra.auth_to_call_xspace"; // const-string v0, "android.intent.extra.auth_to_call_xspace"
v0 = (( android.content.Intent ) v3 ).hasExtra ( v0 ); // invoke-virtual {v3, v0}, Landroid/content/Intent;->hasExtra(Ljava/lang/String;)Z
if ( v0 != null) { // if-eqz v0, :cond_d
/* .line 237 */
v0 = /* invoke-direct {p0, v5}, Lcom/miui/server/xspace/XSpaceManagerServiceImpl;->checkCallXSpacePermission(Ljava/lang/String;)Z */
/* if-nez v0, :cond_c */
} // :cond_c
/* move/from16 v12, p5 */
/* goto/16 :goto_4 */
} // :cond_d
} // :goto_2
v0 = com.miui.server.xspace.XSpaceManagerServiceImpl.PACKAGE_ALIPAY;
/* .line 238 */
v0 = (( java.lang.String ) v0 ).equals ( v8 ); // invoke-virtual {v0, v8}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z
/* if-nez v0, :cond_13 */
final String v0 = "com.xiaomi.gamecenter.sdk.service"; // const-string v0, "com.xiaomi.gamecenter.sdk.service"
/* .line 239 */
v0 = (( java.lang.String ) v0 ).equals ( v8 ); // invoke-virtual {v0, v8}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z
/* if-nez v0, :cond_12 */
final String v0 = "com.xiaomi.xmsf"; // const-string v0, "com.xiaomi.xmsf"
/* .line 240 */
v0 = android.text.TextUtils .equals ( v0,v5 );
/* if-nez v0, :cond_11 */
final String v0 = "com.sina.weibo.sdk.action.ACTION_SDK_REQ_ACTIVITY"; // const-string v0, "com.sina.weibo.sdk.action.ACTION_SDK_REQ_ACTIVITY"
/* .line 241 */
v0 = android.text.TextUtils .equals ( v0,v7 );
/* if-nez v0, :cond_10 */
/* new-instance v0, Ljava/lang/StringBuilder; */
/* invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V */
(( java.lang.StringBuilder ) v0 ).append ( v8 ); // invoke-virtual {v0, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
final String v9 = ".wxapi.WXEntryActivity"; // const-string v9, ".wxapi.WXEntryActivity"
(( java.lang.StringBuilder ) v0 ).append ( v9 ); // invoke-virtual {v0, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v0 ).toString ( ); // invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
/* .line 242 */
/* invoke-virtual/range {p3 ..p3}, Landroid/content/Intent;->getComponent()Landroid/content/ComponentName; */
/* if-nez v9, :cond_e */
final String v9 = ""; // const-string v9, ""
} // :cond_e
/* invoke-virtual/range {p3 ..p3}, Landroid/content/Intent;->getComponent()Landroid/content/ComponentName; */
(( android.content.ComponentName ) v9 ).getClassName ( ); // invoke-virtual {v9}, Landroid/content/ComponentName;->getClassName()Ljava/lang/String;
} // :goto_3
v0 = android.text.TextUtils .equals ( v0,v9 );
if ( v0 != null) { // if-eqz v0, :cond_f
/* move/from16 v12, p5 */
/* .line 247 */
} // :cond_f
final String v0 = "XSpaceManagerServiceImpl"; // const-string v0, "XSpaceManagerServiceImpl"
final String v9 = "pop up ResolverActivity"; // const-string v9, "pop up ResolverActivity"
android.util.Slog .w ( v0,v9 );
/* :try_end_0 */
/* .catchall {:try_start_0 ..:try_end_0} :catchall_1 */
/* .line 248 */
/* move/from16 v12, p5 */
try { // :try_start_1
/* invoke-direct {p0, v3, v8, v12, v5}, Lcom/miui/server/xspace/XSpaceManagerServiceImpl;->getResolverActivity(Landroid/content/Intent;Ljava/lang/String;ILjava/lang/String;)Landroid/content/Intent; */
/* move-object v3, v0 */
} // .end local p3 # "intent":Landroid/content/Intent;
/* .local v0, "intent":Landroid/content/Intent; */
/* .line 241 */
} // .end local v0 # "intent":Landroid/content/Intent;
/* .restart local p3 # "intent":Landroid/content/Intent; */
} // :cond_10
/* move/from16 v12, p5 */
/* .line 240 */
} // :cond_11
/* move/from16 v12, p5 */
/* .line 239 */
} // :cond_12
/* move/from16 v12, p5 */
/* .line 238 */
} // :cond_13
/* move/from16 v12, p5 */
/* .line 243 */
} // :goto_4
final String v0 = "XSpaceManagerServiceImpl"; // const-string v0, "XSpaceManagerServiceImpl"
final String v9 = "call XSpace directly"; // const-string v9, "call XSpace directly"
android.util.Slog .i ( v0,v9 );
/* .line 244 */
/* monitor-exit v11 */
/* .line 249 */
} // :cond_14
/* move/from16 v12, p5 */
v13 = (( java.util.ArrayList ) v11 ).contains ( v5 ); // invoke-virtual {v11, v5}, Ljava/util/ArrayList;->contains(Ljava/lang/Object;)Z
/* if-nez v13, :cond_15 */
/* .line 250 */
v9 = (( java.util.ArrayList ) v9 ).contains ( v8 ); // invoke-virtual {v9, v8}, Ljava/util/ArrayList;->contains(Ljava/lang/Object;)Z
if ( v9 != null) { // if-eqz v9, :cond_18
/* if-ne v4, v0, :cond_18 */
/* .line 251 */
} // :cond_15
final String v0 = "XSpaceManagerServiceImpl"; // const-string v0, "XSpaceManagerServiceImpl"
final String v9 = "XSpace installed App to normal App"; // const-string v9, "XSpace installed App to normal App"
android.util.Slog .w ( v0,v9 );
/* .line 252 */
v0 = com.miui.server.xspace.XSpaceManagerServiceImpl.sAddUserPackagesBlackList;
v0 = (( java.util.ArrayList ) v0 ).contains ( v8 ); // invoke-virtual {v0, v8}, Ljava/util/ArrayList;->contains(Ljava/lang/Object;)Z
/* if-nez v0, :cond_16 */
/* .line 253 */
miui.securityspace.XSpaceIntentCompat .prepareToLeaveUser ( v3,v6 );
/* .line 255 */
} // :cond_16
final String v0 = "android.settings.APPLICATION_DETAILS_SETTINGS"; // const-string v0, "android.settings.APPLICATION_DETAILS_SETTINGS"
v0 = android.text.TextUtils .equals ( v7,v0 );
if ( v0 != null) { // if-eqz v0, :cond_17
/* .line 256 */
final String v0 = "miui.intent.extra.USER_ID"; // const-string v0, "miui.intent.extra.USER_ID"
(( android.content.Intent ) v3 ).putExtra ( v0, v6 ); // invoke-virtual {v3, v0, v6}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;
/* .line 258 */
} // :cond_17
final String v0 = "android.intent.extra.xspace_cached_uid"; // const-string v0, "android.intent.extra.xspace_cached_uid"
int v9 = 0; // const/4 v9, 0x0
(( android.content.Intent ) v3 ).putExtra ( v0, v9 ); // invoke-virtual {v3, v0, v9}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;
/* .line 260 */
/* const-string/jumbo v0, "userId" */
(( android.content.Intent ) v3 ).putExtra ( v0, v4 ); // invoke-virtual {v3, v0, v4}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;
/* .line 262 */
final String v0 = "calling_relation"; // const-string v0, "calling_relation"
int v9 = 1; // const/4 v9, 0x1
(( android.content.Intent ) v3 ).putExtra ( v0, v9 ); // invoke-virtual {v3, v0, v9}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;
/* :try_end_1 */
/* .catchall {:try_start_1 ..:try_end_1} :catchall_0 */
/* .line 264 */
} // .end local p3 # "intent":Landroid/content/Intent;
/* .local v3, "intent":Landroid/content/Intent; */
} // :cond_18
} // :goto_5
try { // :try_start_2
/* monitor-exit v11 */
/* .line 267 */
} // .end local v10 # "cachedToUserId":I
} // :goto_6
/* .line 264 */
} // .end local v3 # "intent":Landroid/content/Intent;
/* .restart local v10 # "cachedToUserId":I */
/* .restart local p3 # "intent":Landroid/content/Intent; */
/* :catchall_0 */
/* move-exception v0 */
/* :catchall_1 */
/* move-exception v0 */
/* move/from16 v12, p5 */
} // .end local p3 # "intent":Landroid/content/Intent;
/* .restart local v3 # "intent":Landroid/content/Intent; */
} // :goto_7
/* monitor-exit v11 */
/* :try_end_2 */
/* .catchall {:try_start_2 ..:try_end_2} :catchall_2 */
/* throw v0 */
/* :catchall_2 */
/* move-exception v0 */
} // .end method
public computeGids ( Integer p0, Integer[] p1, java.lang.String p2 ) {
/* .locals 4 */
/* .param p1, "userId" # I */
/* .param p2, "gids" # [I */
/* .param p3, "packageName" # Ljava/lang/String; */
/* .line 695 */
v0 = v0 = com.miui.server.xspace.XSpaceManagerServiceImpl.mediaAccessRequired;
if ( v0 != null) { // if-eqz v0, :cond_0
if ( p2 != null) { // if-eqz p2, :cond_0
/* .line 696 */
/* const/16 v0, 0x3ff */
com.android.internal.util.ArrayUtils .appendInt ( p2,v0 );
/* .line 698 */
} // :cond_0
int v0 = 0; // const/4 v0, 0x0
if ( p1 != null) { // if-eqz p1, :cond_2
v1 = com.miui.server.xspace.SecSpaceManagerService .isDataTransferProcess ( p1,p3 );
if ( v1 != null) { // if-eqz v1, :cond_2
/* .line 699 */
v0 = android.os.UserHandle .getUserGid ( v0 );
com.android.internal.util.ArrayUtils .appendInt ( p2,v0 );
/* .line 700 */
/* iget-boolean v0, p0, Lcom/miui/server/xspace/XSpaceManagerServiceImpl;->mIsXSpaceCreated:Z */
if ( v0 != null) { // if-eqz v0, :cond_1
/* .line 701 */
com.android.internal.util.ArrayUtils .appendInt ( p2,v0 );
/* .line 703 */
} // :cond_1
/* .line 706 */
} // :cond_2
v1 = miui.securityspace.XSpaceConstant.XSPACE_PACKAGES_SHARED_GID;
v1 = (( java.util.ArrayList ) v1 ).contains ( p3 ); // invoke-virtual {v1, p3}, Ljava/util/ArrayList;->contains(Ljava/lang/Object;)Z
if ( v1 != null) { // if-eqz v1, :cond_3
/* .line 707 */
com.android.internal.util.ArrayUtils .appendInt ( p2,v0 );
/* .line 708 */
/* .line 711 */
} // :cond_3
/* iget-boolean v1, p0, Lcom/miui/server/xspace/XSpaceManagerServiceImpl;->mIsXSpaceCreated:Z */
if ( v1 != null) { // if-eqz v1, :cond_a
/* if-nez p2, :cond_4 */
/* .line 714 */
} // :cond_4
v1 = miui.securityspace.XSpaceUserHandle .isXSpaceUserId ( p1 );
if ( v1 != null) { // if-eqz v1, :cond_7
/* .line 715 */
/* array-length v1, p2 */
} // :goto_0
/* if-ge v0, v1, :cond_6 */
/* aget v2, p2, v0 */
/* .line 716 */
/* .local v2, "gid":I */
/* if-ne v2, v3, :cond_5 */
/* .line 717 */
com.android.internal.util.ArrayUtils .appendInt ( p2,v0 );
/* .line 718 */
/* .line 715 */
} // .end local v2 # "gid":I
} // :cond_5
/* add-int/lit8 v0, v0, 0x1 */
} // :cond_6
} // :goto_1
/* .line 721 */
} // :cond_7
/* if-nez p1, :cond_9 */
/* .line 722 */
/* array-length v1, p2 */
} // :goto_2
/* if-ge v0, v1, :cond_9 */
/* aget v2, p2, v0 */
/* .line 723 */
/* .restart local v2 # "gid":I */
/* if-ne v2, v3, :cond_8 */
/* .line 724 */
com.android.internal.util.ArrayUtils .appendInt ( p2,v0 );
/* .line 725 */
/* .line 722 */
} // .end local v2 # "gid":I
} // :cond_8
/* add-int/lit8 v0, v0, 0x1 */
/* .line 729 */
} // :cond_9
} // :goto_3
/* .line 712 */
} // :cond_a
} // :goto_4
} // .end method
public Integer getDefaultUserId ( ) {
/* .locals 4 */
/* .line 853 */
try { // :try_start_0
/* const-string/jumbo v0, "user" */
android.os.ServiceManager .getService ( v0 );
android.os.IUserManager$Stub .asInterface ( v0 );
/* .line 854 */
/* .local v0, "um":Landroid/os/IUserManager; */
int v1 = 0; // const/4 v1, 0x0
v2 = } // :goto_0
if ( v2 != null) { // if-eqz v2, :cond_1
/* check-cast v2, Landroid/content/pm/UserInfo; */
/* .line 855 */
/* .local v2, "userInfo":Landroid/content/pm/UserInfo; */
v3 = miui.securityspace.XSpaceUserHandle .isXSpaceUser ( v2 );
if ( v3 != null) { // if-eqz v3, :cond_0
/* .line 856 */
v1 = android.app.ActivityManager .getCurrentUser ( );
/* :try_end_0 */
/* .catch Landroid/os/RemoteException; {:try_start_0 ..:try_end_0} :catch_0 */
/* .line 858 */
} // .end local v2 # "userInfo":Landroid/content/pm/UserInfo;
} // :cond_0
/* .line 861 */
} // .end local v0 # "um":Landroid/os/IUserManager;
} // :cond_1
/* .line 859 */
/* :catch_0 */
/* move-exception v0 */
/* .line 862 */
} // :goto_1
int v0 = -1; // const/4 v0, -0x1
} // .end method
public Integer getUserTypeCount ( Integer[] p0 ) {
/* .locals 4 */
/* .param p1, "userIds" # [I */
/* .line 757 */
/* array-length v0, p1 */
/* .line 758 */
/* .local v0, "result":I */
int v1 = 0; // const/4 v1, 0x0
/* .local v1, "i":I */
} // :goto_0
/* array-length v2, p1 */
/* if-ge v1, v2, :cond_1 */
/* .line 759 */
/* aget v2, p1, v1 */
/* const/16 v3, 0x3e7 */
/* if-ne v2, v3, :cond_0 */
/* .line 760 */
/* add-int/lit8 v0, v0, -0x1 */
/* .line 758 */
} // :cond_0
/* add-int/lit8 v1, v1, 0x1 */
/* .line 763 */
} // .end local v1 # "i":I
} // :cond_1
} // .end method
public Integer getXSpaceUserId ( ) {
/* .locals 1 */
/* .line 866 */
/* const/16 v0, 0x3e7 */
} // .end method
public void handleWindowManagerAndUserLru ( android.content.Context p0, Integer p1, Integer p2, Integer p3, com.android.server.wm.WindowManagerService p4, Integer[] p5 ) {
/* .locals 5 */
/* .param p1, "context" # Landroid/content/Context; */
/* .param p2, "userId" # I */
/* .param p3, "userIdOrig" # I */
/* .param p4, "oldUserId" # I */
/* .param p5, "mWindowManager" # Lcom/android/server/wm/WindowManagerService; */
/* .param p6, "mCurrentProfileIds" # [I */
/* .line 772 */
(( com.android.server.wm.WindowManagerService ) p5 ).setCurrentUser ( p3 ); // invoke-virtual {p5, p3}, Lcom/android/server/wm/WindowManagerService;->setCurrentUser(I)V
/* .line 773 */
(( android.content.Context ) p1 ).getContentResolver ( ); // invoke-virtual {p1}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;
/* const-string/jumbo v1, "second_user_id" */
/* const/16 v2, -0x2710 */
v0 = android.provider.Settings$Secure .getInt ( v0,v1,v2 );
/* .line 775 */
/* .local v0, "privacyUserId":I */
(( android.content.Context ) p1 ).getContentResolver ( ); // invoke-virtual {p1}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;
final String v3 = "kid_user_id"; // const-string v3, "kid_user_id"
v1 = android.provider.Settings$Secure .getInt ( v1,v3,v2 );
/* .line 777 */
/* .local v1, "kidSpaceUserId":I */
/* new-instance v2, Ljava/lang/StringBuilder; */
/* invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V */
final String v3 = "privacyUserId :"; // const-string v3, "privacyUserId :"
(( java.lang.StringBuilder ) v2 ).append ( v3 ); // invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v2 ).append ( v0 ); // invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;
final String v3 = " userId:"; // const-string v3, " userId:"
(( java.lang.StringBuilder ) v2 ).append ( v3 ); // invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v2 ).append ( p2 ); // invoke-virtual {v2, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;
final String v3 = " userIdOrig:"; // const-string v3, " userIdOrig:"
(( java.lang.StringBuilder ) v2 ).append ( v3 ); // invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v2 ).append ( p3 ); // invoke-virtual {v2, p3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;
final String v3 = " oldUserId:"; // const-string v3, " oldUserId:"
(( java.lang.StringBuilder ) v2 ).append ( v3 ); // invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v2 ).append ( p4 ); // invoke-virtual {v2, p4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;
final String v3 = " kidSpaceUserId:"; // const-string v3, " kidSpaceUserId:"
(( java.lang.StringBuilder ) v2 ).append ( v3 ); // invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v2 ).append ( v1 ); // invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v2 ).toString ( ); // invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
final String v3 = "XSpaceManagerServiceImpl"; // const-string v3, "XSpaceManagerServiceImpl"
android.util.Slog .d ( v3,v2 );
/* .line 779 */
v2 = android.os.UserHandle .getAppId ( v2 );
/* const/16 v4, 0x3e8 */
/* if-ne v2, v4, :cond_4 */
/* if-ne v0, p4, :cond_0 */
if ( p2 != null) { // if-eqz p2, :cond_3
} // :cond_0
/* if-nez p4, :cond_1 */
/* if-eq p2, v0, :cond_3 */
} // :cond_1
/* if-ne v1, p4, :cond_2 */
if ( p2 != null) { // if-eqz p2, :cond_3
} // :cond_2
/* if-nez p4, :cond_4 */
/* if-ne p2, v1, :cond_4 */
/* .line 784 */
} // :cond_3
/* const-string/jumbo v2, "switch without lock" */
android.util.Slog .d ( v3,v2 );
/* .line 788 */
} // :cond_4
int v2 = 0; // const/4 v2, 0x0
(( com.android.server.wm.WindowManagerService ) p5 ).lockNow ( v2 ); // invoke-virtual {p5, v2}, Lcom/android/server/wm/WindowManagerService;->lockNow(Landroid/os/Bundle;)V
/* .line 790 */
} // :goto_0
return;
} // .end method
public void init ( android.content.Context p0 ) {
/* .locals 7 */
/* .param p1, "context" # Landroid/content/Context; */
/* .line 440 */
this.mContext = p1;
/* .line 441 */
(( android.content.Context ) p1 ).getContentResolver ( ); // invoke-virtual {p1}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;
this.mResolver = v0;
/* .line 442 */
/* invoke-direct {p0}, Lcom/miui/server/xspace/XSpaceManagerServiceImpl;->initXSpaceAppList()V */
/* .line 443 */
v0 = this.mResolver;
/* .line 444 */
/* const-string/jumbo v1, "xspace_enabled" */
android.provider.Settings$Secure .getUriFor ( v1 );
/* new-instance v2, Lcom/miui/server/xspace/XSpaceManagerServiceImpl$1; */
/* new-instance v3, Landroid/os/Handler; */
/* invoke-direct {v3}, Landroid/os/Handler;-><init>()V */
/* invoke-direct {v2, p0, v3}, Lcom/miui/server/xspace/XSpaceManagerServiceImpl$1;-><init>(Lcom/miui/server/xspace/XSpaceManagerServiceImpl;Landroid/os/Handler;)V */
/* .line 443 */
int v3 = 0; // const/4 v3, 0x0
(( android.content.ContentResolver ) v0 ).registerContentObserver ( v1, v3, v2 ); // invoke-virtual {v0, v1, v3, v2}, Landroid/content/ContentResolver;->registerContentObserver(Landroid/net/Uri;ZLandroid/database/ContentObserver;)V
/* .line 455 */
/* new-instance v0, Landroid/content/IntentFilter; */
/* invoke-direct {v0}, Landroid/content/IntentFilter;-><init>()V */
/* .line 456 */
/* .local v0, "filter":Landroid/content/IntentFilter; */
final String v1 = "android.intent.action.PACKAGE_ADDED"; // const-string v1, "android.intent.action.PACKAGE_ADDED"
(( android.content.IntentFilter ) v0 ).addAction ( v1 ); // invoke-virtual {v0, v1}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V
/* .line 457 */
final String v1 = "android.intent.action.PACKAGE_REMOVED_INTERNAL"; // const-string v1, "android.intent.action.PACKAGE_REMOVED_INTERNAL"
(( android.content.IntentFilter ) v0 ).addAction ( v1 ); // invoke-virtual {v0, v1}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V
/* .line 458 */
final String v1 = "package"; // const-string v1, "package"
(( android.content.IntentFilter ) v0 ).addDataScheme ( v1 ); // invoke-virtual {v0, v1}, Landroid/content/IntentFilter;->addDataScheme(Ljava/lang/String;)V
/* .line 459 */
v1 = this.mContext;
v2 = this.mPackageChangedListener;
/* const/16 v3, 0x3e7 */
android.os.UserHandle .of ( v3 );
int v5 = 0; // const/4 v5, 0x0
int v6 = 0; // const/4 v6, 0x0
/* move-object v4, v0 */
/* invoke-virtual/range {v1 ..v6}, Landroid/content/Context;->registerReceiverAsUser(Landroid/content/BroadcastReceiver;Landroid/os/UserHandle;Landroid/content/IntentFilter;Ljava/lang/String;Landroid/os/Handler;)Landroid/content/Intent; */
/* .line 461 */
/* new-instance v1, Ljava/lang/StringBuilder; */
/* invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V */
final String v2 = "XSpace init, active:"; // const-string v2, "XSpace init, active:"
(( java.lang.StringBuilder ) v1 ).append ( v2 ); // invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
/* iget-boolean v2, p0, Lcom/miui/server/xspace/XSpaceManagerServiceImpl;->mIsXSpaceActived:Z */
(( java.lang.StringBuilder ) v1 ).append ( v2 ); // invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v1 ).toString ( ); // invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
final String v2 = "XSpaceManagerServiceImpl"; // const-string v2, "XSpaceManagerServiceImpl"
android.util.Slog .w ( v2,v1 );
/* .line 462 */
v1 = miui.securityspace.CrossUserUtils .hasXSpaceUser ( p1 );
/* iput-boolean v1, p0, Lcom/miui/server/xspace/XSpaceManagerServiceImpl;->mIsXSpaceCreated:Z */
/* .line 463 */
if ( v1 != null) { // if-eqz v1, :cond_0
/* .line 464 */
final String v1 = "persist.sys.xspace_created"; // const-string v1, "persist.sys.xspace_created"
final String v2 = "1"; // const-string v2, "1"
android.os.SystemProperties .set ( v1,v2 );
/* .line 465 */
int v1 = 0; // const/4 v1, 0x0
/* invoke-direct {p0, p1, v1}, Lcom/miui/server/xspace/XSpaceManagerServiceImpl;->startXSpaceService(Landroid/content/Context;Ljava/lang/String;)V */
/* .line 467 */
} // :cond_0
com.miui.server.xspace.SecSpaceManagerService .init ( p1 );
/* .line 468 */
return;
} // .end method
public Boolean isXSpaceActive ( ) {
/* .locals 1 */
/* .line 691 */
/* iget-boolean v0, p0, Lcom/miui/server/xspace/XSpaceManagerServiceImpl;->mIsXSpaceActived:Z */
} // .end method
public Boolean needCheckUser ( android.content.pm.ProviderInfo p0, java.lang.String p1, Integer p2, Boolean p3 ) {
/* .locals 1 */
/* .param p1, "cpi" # Landroid/content/pm/ProviderInfo; */
/* .param p2, "processName" # Ljava/lang/String; */
/* .param p3, "userId" # I */
/* .param p4, "checkUser" # Z */
/* .line 848 */
v0 = miui.securityspace.CrossUserUtils .needCheckUser ( p1,p2,p3,p4 );
} // .end method
public android.content.pm.ActivityInfo resolveXSpaceIntent ( android.content.pm.ActivityInfo p0, android.content.Intent p1, com.android.server.wm.ActivityTaskSupervisor p2, android.app.ProfilerInfo p3, java.lang.String p4, Integer p5, Integer p6, java.lang.String p7, Integer p8, Integer p9 ) {
/* .locals 18 */
/* .param p1, "aInfo" # Landroid/content/pm/ActivityInfo; */
/* .param p2, "intent" # Landroid/content/Intent; */
/* .param p3, "stack" # Lcom/android/server/wm/ActivityTaskSupervisor; */
/* .param p4, "profilerInfo" # Landroid/app/ProfilerInfo; */
/* .param p5, "resolvedType" # Ljava/lang/String; */
/* .param p6, "startFlags" # I */
/* .param p7, "userId" # I */
/* .param p8, "callingPackage" # Ljava/lang/String; */
/* .param p9, "callingUserId" # I */
/* .param p10, "callingPid" # I */
/* .line 273 */
/* move-object/from16 v10, p0 */
/* move-object/from16 v1, p1 */
/* move-object/from16 v11, p2 */
/* move/from16 v12, p7 */
/* move-object/from16 v13, p8 */
if ( v12 != null) { // if-eqz v12, :cond_0
/* const/16 v0, 0x3e7 */
/* if-ne v12, v0, :cond_1 */
/* .line 274 */
} // :cond_0
v0 = /* invoke-direct {v10, v11, v13, v1}, Lcom/miui/server/xspace/XSpaceManagerServiceImpl;->isPublicIntent(Landroid/content/Intent;Ljava/lang/String;Landroid/content/pm/ActivityInfo;)Z */
/* if-nez v0, :cond_2 */
/* .line 275 */
} // :cond_1
/* .line 278 */
} // :cond_2
v0 = /* invoke-direct {v10, v11, v13}, Lcom/miui/server/xspace/XSpaceManagerServiceImpl;->shouldResolveAgain(Landroid/content/Intent;Ljava/lang/String;)Z */
if ( v0 != null) { // if-eqz v0, :cond_3
/* .line 280 */
try { // :try_start_0
android.app.AppGlobals .getPackageManager ( );
int v4 = 0; // const/4 v4, 0x0
/* const-wide/16 v5, -0x1 */
/* move-object/from16 v3, p2 */
/* move/from16 v7, p7 */
/* invoke-interface/range {v2 ..v7}, Landroid/content/pm/IPackageManager;->resolveIntent(Landroid/content/Intent;Ljava/lang/String;JI)Landroid/content/pm/ResolveInfo; */
v0 = this.activityInfo;
/* :try_end_0 */
/* .catch Landroid/os/RemoteException; {:try_start_0 ..:try_end_0} :catch_0 */
/* .line 284 */
} // .end local p1 # "aInfo":Landroid/content/pm/ActivityInfo;
/* .local v0, "aInfo":Landroid/content/pm/ActivityInfo; */
/* .line 282 */
} // .end local v0 # "aInfo":Landroid/content/pm/ActivityInfo;
/* .restart local p1 # "aInfo":Landroid/content/pm/ActivityInfo; */
/* :catch_0 */
/* move-exception v0 */
/* .line 283 */
/* .local v0, "e":Landroid/os/RemoteException; */
/* new-instance v2, Ljava/lang/StringBuilder; */
/* invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V */
final String v3 = "Can not get aInfo, intent = "; // const-string v3, "Can not get aInfo, intent = "
(( java.lang.StringBuilder ) v2 ).append ( v3 ); // invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v2 ).append ( v11 ); // invoke-virtual {v2, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v2 ).toString ( ); // invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
final String v3 = "XSpaceManagerServiceImpl"; // const-string v3, "XSpaceManagerServiceImpl"
android.util.Slog .e ( v3,v2,v0 );
/* .line 287 */
} // .end local v0 # "e":Landroid/os/RemoteException;
} // :cond_3
/* move-object v0, v1 */
} // .end local p1 # "aInfo":Landroid/content/pm/ActivityInfo;
/* .local v0, "aInfo":Landroid/content/pm/ActivityInfo; */
} // :goto_0
v14 = /* invoke-direct {v10, v11, v13}, Lcom/miui/server/xspace/XSpaceManagerServiceImpl;->getCachedUserId(Landroid/content/Intent;Ljava/lang/String;)I */
/* .line 288 */
/* .local v14, "cachedUserId":I */
/* const/16 v1, -0x2710 */
/* if-eq v14, v1, :cond_5 */
/* .line 290 */
v15 = android.os.Binder .getCallingUid ( );
/* .line 291 */
/* .local v15, "callingUid":I */
android.os.Binder .clearCallingIdentity ( );
/* move-result-wide v16 */
/* .line 292 */
/* .local v16, "token":J */
/* move-object/from16 v1, p0 */
/* move-object/from16 v2, p2 */
/* move-object/from16 v3, p3 */
/* move-object/from16 v4, p5 */
/* move/from16 v5, p6 */
/* move-object/from16 v6, p4 */
/* move v7, v14 */
/* move v8, v15 */
/* move/from16 v9, p10 */
/* invoke-direct/range {v1 ..v9}, Lcom/miui/server/xspace/XSpaceManagerServiceImpl;->resolveActivity(Landroid/content/Intent;Lcom/android/server/wm/ActivityTaskSupervisor;Ljava/lang/String;ILandroid/app/ProfilerInfo;III)Landroid/content/pm/ActivityInfo; */
/* .line 293 */
/* invoke-static/range {v16 ..v17}, Landroid/os/Binder;->restoreCallingIdentity(J)V */
/* .line 294 */
final String v1 = "calling_relation"; // const-string v1, "calling_relation"
int v2 = 0; // const/4 v2, 0x0
v1 = (( android.content.Intent ) v11 ).getBooleanExtra ( v1, v2 ); // invoke-virtual {v11, v1, v2}, Landroid/content/Intent;->getBooleanExtra(Ljava/lang/String;Z)Z
if ( v1 != null) { // if-eqz v1, :cond_4
if ( v0 != null) { // if-eqz v0, :cond_4
/* .line 296 */
v1 = this.packageName;
/* move/from16 v2, p9 */
/* invoke-direct {v10, v1, v13, v2}, Lcom/miui/server/xspace/XSpaceManagerServiceImpl;->putCachedCallingRelation(Ljava/lang/String;Ljava/lang/String;I)V */
/* .line 294 */
} // :cond_4
/* move/from16 v2, p9 */
/* .line 288 */
} // .end local v15 # "callingUid":I
} // .end local v16 # "token":J
} // :cond_5
/* move/from16 v2, p9 */
/* .line 299 */
} // :goto_1
} // .end method
public void setXSpaceCreated ( Integer p0, Boolean p1 ) {
/* .locals 1 */
/* .param p1, "userId" # I */
/* .param p2, "isXSpaceCreated" # Z */
/* .line 685 */
/* const/16 v0, 0x3e7 */
/* if-ne p1, v0, :cond_0 */
/* .line 686 */
/* iput-boolean p2, p0, Lcom/miui/server/xspace/XSpaceManagerServiceImpl;->mIsXSpaceCreated:Z */
/* .line 688 */
} // :cond_0
return;
} // .end method
public Boolean shouldCrossXSpace ( java.lang.String p0, Integer p1 ) {
/* .locals 6 */
/* .param p1, "packageName" # Ljava/lang/String; */
/* .param p2, "userId" # I */
/* .line 870 */
/* const/16 v0, 0x3e7 */
int v1 = 0; // const/4 v1, 0x0
/* if-ne p2, v0, :cond_2 */
/* .line 871 */
android.os.Binder .clearCallingIdentity ( );
/* move-result-wide v2 */
/* .line 873 */
/* .local v2, "origId":J */
try { // :try_start_0
android.app.AppGlobals .getPackageManager ( );
/* const-wide/16 v4, 0x0 */
/* if-nez v0, :cond_1 */
/* .line 874 */
android.app.AppGlobals .getPackageManager ( );
/* .line 875 */
/* .local v0, "packageInfo":Landroid/content/pm/PackageInfo; */
if ( v0 != null) { // if-eqz v0, :cond_0
v4 = this.applicationInfo;
if ( v4 != null) { // if-eqz v4, :cond_0
v4 = this.applicationInfo;
/* iget-boolean v4, v4, Landroid/content/pm/ApplicationInfo;->enabled:Z */
/* :try_end_0 */
/* .catch Landroid/os/RemoteException; {:try_start_0 ..:try_end_0} :catch_0 */
/* .catchall {:try_start_0 ..:try_end_0} :catchall_0 */
if ( v4 != null) { // if-eqz v4, :cond_0
int v1 = 1; // const/4 v1, 0x1
/* .line 880 */
} // :cond_0
android.os.Binder .restoreCallingIdentity ( v2,v3 );
/* .line 875 */
/* .line 880 */
} // .end local v0 # "packageInfo":Landroid/content/pm/PackageInfo;
/* :catchall_0 */
/* move-exception v0 */
android.os.Binder .restoreCallingIdentity ( v2,v3 );
/* .line 881 */
/* throw v0 */
/* .line 877 */
/* :catch_0 */
/* move-exception v0 */
/* .line 880 */
} // :cond_1
android.os.Binder .restoreCallingIdentity ( v2,v3 );
/* .line 881 */
/* nop */
/* .line 883 */
} // .end local v2 # "origId":J
} // :cond_2
} // .end method
public Boolean shouldInstallInXSpace ( android.os.UserHandle p0, Integer p1 ) {
/* .locals 1 */
/* .param p1, "installUser" # Landroid/os/UserHandle; */
/* .param p2, "userId" # I */
/* .line 750 */
/* if-nez p1, :cond_0 */
v0 = miui.securityspace.XSpaceUserHandle .isXSpaceUserId ( p2 );
if ( v0 != null) { // if-eqz v0, :cond_0
/* .line 751 */
int v0 = 0; // const/4 v0, 0x0
/* .line 753 */
} // :cond_0
int v0 = 1; // const/4 v0, 0x1
} // .end method
public Boolean showSwitchingDialog ( com.android.server.am.ActivityManagerService p0, android.content.Context p1, Integer p2, Integer p3, android.os.Handler p4, Integer p5 ) {
/* .locals 11 */
/* .param p1, "ams" # Lcom/android/server/am/ActivityManagerService; */
/* .param p2, "context" # Landroid/content/Context; */
/* .param p3, "currentUserId" # I */
/* .param p4, "targetUserId" # I */
/* .param p5, "handler" # Landroid/os/Handler; */
/* .param p6, "userSwitchUiMessage" # I */
/* .line 793 */
v0 = android.os.Binder .getCallingUid ( );
/* move-object v9, p0 */
/* invoke-direct {p0, v0}, Lcom/miui/server/xspace/XSpaceManagerServiceImpl;->setSwitchUserCallingUid(I)V */
/* .line 794 */
v0 = android.os.Binder .getCallingPid ( );
com.android.server.am.ProcessUtils .getPackageNameByPid ( v0 );
/* .line 795 */
/* .local v0, "pkgName":Ljava/lang/String; */
if ( v0 != null) { // if-eqz v0, :cond_1
final String v1 = "com.android.systemui"; // const-string v1, "com.android.systemui"
v1 = (( java.lang.String ) v0 ).equals ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z
/* if-nez v1, :cond_0 */
final String v1 = "com.android.keyguard"; // const-string v1, "com.android.keyguard"
v1 = (( java.lang.String ) v0 ).equals ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z
if ( v1 != null) { // if-eqz v1, :cond_1
/* .line 796 */
} // :cond_0
int v1 = 0; // const/4 v1, 0x0
/* .line 798 */
} // :cond_1
/* new-instance v10, Lcom/miui/server/xspace/XSpaceManagerServiceImpl$2; */
/* move-object v1, v10 */
/* move-object v2, p0 */
/* move-object v3, p2 */
/* move v4, p4 */
/* move v5, p3 */
/* move-object v6, p1 */
/* move-object/from16 v7, p5 */
/* move/from16 v8, p6 */
/* invoke-direct/range {v1 ..v8}, Lcom/miui/server/xspace/XSpaceManagerServiceImpl$2;-><init>(Lcom/miui/server/xspace/XSpaceManagerServiceImpl;Landroid/content/Context;IILcom/android/server/am/ActivityManagerService;Landroid/os/Handler;I)V */
/* move-object/from16 v1, p5 */
(( android.os.Handler ) v1 ).post ( v10 ); // invoke-virtual {v1, v10}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z
/* .line 821 */
v2 = com.miui.server.xspace.XSpaceManagerServiceImpl.mUserSwitchObserver;
final String v3 = "XSpaceManagerServiceImpl"; // const-string v3, "XSpaceManagerServiceImpl"
/* move-object v4, p1 */
(( com.android.server.am.ActivityManagerService ) p1 ).registerUserSwitchObserver ( v2, v3 ); // invoke-virtual {p1, v2, v3}, Lcom/android/server/am/ActivityManagerService;->registerUserSwitchObserver(Landroid/app/IUserSwitchObserver;Ljava/lang/String;)V
/* .line 822 */
int v2 = 1; // const/4 v2, 0x1
} // .end method
