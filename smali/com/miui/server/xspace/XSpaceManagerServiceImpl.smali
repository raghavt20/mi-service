.class public Lcom/miui/server/xspace/XSpaceManagerServiceImpl;
.super Lcom/miui/server/xspace/XSpaceManagerServiceStub;
.source "XSpaceManagerServiceImpl.java"


# annotations
.annotation runtime Lcom/miui/base/annotations/MiuiStubHead;
    manifestName = "com.miui.server.xspace.XSpaceManagerServiceStub$$"
.end annotation

.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/miui/server/xspace/XSpaceManagerServiceImpl$PackageChangedListener;
    }
.end annotation


# static fields
.field private static final ACTION_START_DUAL_ANIMATION:Ljava/lang/String; = "miui.intent.action.START_DUAL_ANIMATION"

.field private static final ACTION_WEIBO_SDK_REQ:Ljava/lang/String; = "com.sina.weibo.sdk.action.ACTION_SDK_REQ_ACTIVITY"

.field public static final ACTION_XSPACE_RESOLVER_ACTIVITY_FROM_CORE:Ljava/lang/String; = "miui.intent.action.ACTION_XSPACE_RESOLVER_ACTIVITY_FROM_CORE"

.field private static final EXTRA_XSPACE_CACHED_USERID:Ljava/lang/String; = "android.intent.extra.xspace_cached_uid"

.field private static final EXTRA_XSPACE_RESOLVE_INTENT_AGAIN:Ljava/lang/String; = "android.intent.extra.xspace_resolve_intent_again"

.field private static final EXTRA_XSPACE_USERID_SELECTED:Ljava/lang/String; = "android.intent.extra.xspace_userid_selected"

.field private static final MAX_COMPETE_XSPACE_NOTIFICATION_TIMES:I = 0x1

.field private static final ORG_IFAA_AIDL_MANAGER_PKGNAME:Ljava/lang/String; = "org.ifaa.aidl.manager"

.field private static final PACKAGE_ALIPAY:Ljava/lang/String;

.field private static final PACKAGE_ANDROID_INSTALLER:Ljava/lang/String; = "com.android.packageinstaller"

.field private static final PACKAGE_GOOGLE_INSTALLER:Ljava/lang/String; = "com.google.android.packageinstaller"

.field private static final PACKAGE_LINKER:Ljava/lang/String; = "@"

.field private static final PACKAGE_MIUI_INSTALLER:Ljava/lang/String; = "com.miui.packageinstaller"

.field private static final PACKAGE_SECURITYADD:Ljava/lang/String; = "com.miui.securityadd"

.field private static final PACKAGE_SETTING:Ljava/lang/String; = "com.android.settings"

.field private static final PACKAGE_XMSF:Ljava/lang/String; = "com.xiaomi.xmsf"

.field private static final SYSTEM_PROP_XSPACE_CREATED:Ljava/lang/String; = "persist.sys.xspace_created"

.field private static final TAG:Ljava/lang/String; = "XSpaceManagerServiceImpl"

.field private static final WEIXIN_SDK_REQ_CLASSNAME:Ljava/lang/String; = ".wxapi.WXEntryActivity"

.field private static final XIAOMI_GAMECENTER_SDK_PKGNAME:Ljava/lang/String; = "com.xiaomi.gamecenter.sdk.service"

.field private static final XSPACE_ANIMATION_STATUS:Ljava/lang/String; = "xspace_animation_status"

.field private static final XSPACE_APP_LIST_INIT_NUMBER:I

.field private static final XSPACE_CLOUD_CONTROL_STATUS:Ljava/lang/String; = "dual_animation_switch"

.field private static final XSPACE_SERVICE_COMPONENT:Ljava/lang/String; = "com.miui.securitycore/com.miui.xspace.service.XSpaceService"

.field private static mDialog:Lcom/miui/server/xspace/BaseUserSwitchingDialog;

.field private static mUserManager:Lcom/android/server/pm/UserManagerService;

.field private static mUserSwitchObserver:Landroid/app/UserSwitchObserver;

.field private static final mediaAccessRequired:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private static final sAddUserPackagesBlackList:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private static final sCachedCallingRelationSelfLocked:Ljava/util/HashMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/HashMap<",
            "Ljava/lang/String;",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation
.end field

.field private static final sCrossUserAimPackagesWhiteList:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private static final sCrossUserCallingPackagesWhiteList:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private static final sCrossUserDisableComponentActionWhiteList:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private static final sPublicActionList:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private static sSwitchUserCallingUid:I

.field private static final sXSpaceApplicationList:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private static final sXSpaceInstalledPackagesSelfLocked:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private static final sXspaceAnimWhiteList:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field private mAnimStatus:Z

.field private mContext:Landroid/content/Context;

.field private mIsXSpaceActived:Z

.field private mIsXSpaceCreated:Z

.field private mLastTime:J

.field private final mPackageChangedListener:Lcom/miui/server/xspace/XSpaceManagerServiceImpl$PackageChangedListener;

.field private mResolver:Landroid/content/ContentResolver;


# direct methods
.method static bridge synthetic -$$Nest$fgetmIsXSpaceActived(Lcom/miui/server/xspace/XSpaceManagerServiceImpl;)Z
    .locals 0

    iget-boolean p0, p0, Lcom/miui/server/xspace/XSpaceManagerServiceImpl;->mIsXSpaceActived:Z

    return p0
.end method

.method static bridge synthetic -$$Nest$fgetmResolver(Lcom/miui/server/xspace/XSpaceManagerServiceImpl;)Landroid/content/ContentResolver;
    .locals 0

    iget-object p0, p0, Lcom/miui/server/xspace/XSpaceManagerServiceImpl;->mResolver:Landroid/content/ContentResolver;

    return-object p0
.end method

.method static bridge synthetic -$$Nest$fputmIsXSpaceActived(Lcom/miui/server/xspace/XSpaceManagerServiceImpl;Z)V
    .locals 0

    iput-boolean p1, p0, Lcom/miui/server/xspace/XSpaceManagerServiceImpl;->mIsXSpaceActived:Z

    return-void
.end method

.method static bridge synthetic -$$Nest$mgetPackageName(Lcom/miui/server/xspace/XSpaceManagerServiceImpl;Landroid/content/Intent;)Ljava/lang/String;
    .locals 0

    invoke-direct {p0, p1}, Lcom/miui/server/xspace/XSpaceManagerServiceImpl;->getPackageName(Landroid/content/Intent;)Ljava/lang/String;

    move-result-object p0

    return-object p0
.end method

.method static bridge synthetic -$$Nest$monPackageCallback(Lcom/miui/server/xspace/XSpaceManagerServiceImpl;Ljava/lang/String;Landroid/os/UserHandle;Ljava/lang/String;)V
    .locals 0

    invoke-direct {p0, p1, p2, p3}, Lcom/miui/server/xspace/XSpaceManagerServiceImpl;->onPackageCallback(Ljava/lang/String;Landroid/os/UserHandle;Ljava/lang/String;)V

    return-void
.end method

.method static bridge synthetic -$$Nest$sfgetmDialog()Lcom/miui/server/xspace/BaseUserSwitchingDialog;
    .locals 1

    sget-object v0, Lcom/miui/server/xspace/XSpaceManagerServiceImpl;->mDialog:Lcom/miui/server/xspace/BaseUserSwitchingDialog;

    return-object v0
.end method

.method static bridge synthetic -$$Nest$sfputmDialog(Lcom/miui/server/xspace/BaseUserSwitchingDialog;)V
    .locals 0

    sput-object p0, Lcom/miui/server/xspace/XSpaceManagerServiceImpl;->mDialog:Lcom/miui/server/xspace/BaseUserSwitchingDialog;

    return-void
.end method

.method static bridge synthetic -$$Nest$smgetUserManager()Lcom/android/server/pm/UserManagerService;
    .locals 1

    invoke-static {}, Lcom/miui/server/xspace/XSpaceManagerServiceImpl;->getUserManager()Lcom/android/server/pm/UserManagerService;

    move-result-object v0

    return-object v0
.end method

.method static constructor <clinit>()V
    .locals 8

    .line 78
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    sput-object v0, Lcom/miui/server/xspace/XSpaceManagerServiceImpl;->sCrossUserCallingPackagesWhiteList:Ljava/util/ArrayList;

    .line 80
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    sput-object v1, Lcom/miui/server/xspace/XSpaceManagerServiceImpl;->sCrossUserDisableComponentActionWhiteList:Ljava/util/ArrayList;

    .line 81
    new-instance v2, Ljava/util/ArrayList;

    invoke-direct {v2}, Ljava/util/ArrayList;-><init>()V

    sput-object v2, Lcom/miui/server/xspace/XSpaceManagerServiceImpl;->mediaAccessRequired:Ljava/util/List;

    .line 110
    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    .line 111
    .local v3, "stringBuilder":Ljava/lang/StringBuilder;
    const-string v4, "com.eg.android."

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 112
    const-string v4, "AlipayGphone"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 113
    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    sput-object v4, Lcom/miui/server/xspace/XSpaceManagerServiceImpl;->PACKAGE_ALIPAY:Ljava/lang/String;

    .line 115
    const-string v4, "android"

    invoke-virtual {v0, v4}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 116
    const-string v4, "com.android.settings"

    invoke-virtual {v0, v4}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 117
    const-string v4, "com.android.systemui"

    invoke-virtual {v0, v4}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 118
    const-string v4, "com.miui.securitycenter"

    invoke-virtual {v0, v4}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 119
    const-string v4, "com.miui.home"

    invoke-virtual {v0, v4}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 120
    const-string v5, "com.miui.fliphome"

    invoke-virtual {v0, v5}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 121
    const-string v6, "com.android.shell"

    invoke-virtual {v0, v6}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 123
    const-string v6, "com.mi.android.globallauncher"

    invoke-virtual {v0, v6}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 124
    const-string v7, "com.lbe.security.miui"

    invoke-virtual {v0, v7}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 125
    const-string v0, "android.nfc.action.TECH_DISCOVERED"

    invoke-virtual {v1, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 126
    const-string v0, "com.android.fileexplorer"

    invoke-interface {v2, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 127
    const-string v0, "com.miui.huanji"

    invoke-interface {v2, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 129
    .end local v3    # "stringBuilder":Ljava/lang/StringBuilder;
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    sput-object v0, Lcom/miui/server/xspace/XSpaceManagerServiceImpl;->sPublicActionList:Ljava/util/ArrayList;

    .line 131
    const-string v1, "android.intent.action.SEND"

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 132
    const-string v1, "android.intent.action.SEND_MULTIPLE"

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 133
    const-string v1, "android.intent.action.SENDTO"

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 134
    const-string v1, "android.content.pm.action.REQUEST_PERMISSIONS_FOR_OTHER"

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 136
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    sput-object v0, Lcom/miui/server/xspace/XSpaceManagerServiceImpl;->sCrossUserAimPackagesWhiteList:Ljava/util/ArrayList;

    .line 138
    const-string v1, "com.xiaomi.xmsf"

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 139
    sget-object v1, Lmiui/securityspace/XSpaceConstant;->REQUIRED_APPS:Ljava/util/ArrayList;

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->addAll(Ljava/util/Collection;)Z

    .line 140
    const-string v1, "com.xiaomi.gamecenter.sdk.service"

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->remove(Ljava/lang/Object;)Z

    .line 141
    const-string v2, "com.google.android.gsf"

    invoke-virtual {v0, v2}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 142
    const-string v2, "com.google.android.gsf.login"

    invoke-virtual {v0, v2}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 143
    const-string v2, "com.google.android.gms"

    invoke-virtual {v0, v2}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 144
    const-string v2, "com.google.android.play.games"

    invoke-virtual {v0, v2}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 146
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    sput-object v0, Lcom/miui/server/xspace/XSpaceManagerServiceImpl;->sXspaceAnimWhiteList:Ljava/util/ArrayList;

    .line 148
    invoke-virtual {v0, v4}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 149
    invoke-virtual {v0, v5}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 150
    invoke-virtual {v0, v6}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 153
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    sput-object v0, Lcom/miui/server/xspace/XSpaceManagerServiceImpl;->sAddUserPackagesBlackList:Ljava/util/ArrayList;

    .line 155
    const-string v2, "com.android.contacts"

    invoke-virtual {v0, v2}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 157
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    sput-object v0, Lcom/miui/server/xspace/XSpaceManagerServiceImpl;->sXSpaceInstalledPackagesSelfLocked:Ljava/util/ArrayList;

    .line 159
    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 160
    const-string v1, "org.ifaa.aidl.manager"

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 161
    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    sput v0, Lcom/miui/server/xspace/XSpaceManagerServiceImpl;->XSPACE_APP_LIST_INIT_NUMBER:I

    .line 163
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    sput-object v0, Lcom/miui/server/xspace/XSpaceManagerServiceImpl;->sXSpaceApplicationList:Ljava/util/ArrayList;

    .line 165
    const-string v1, "com.android.vending"

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 167
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    sput-object v0, Lcom/miui/server/xspace/XSpaceManagerServiceImpl;->sCachedCallingRelationSelfLocked:Ljava/util/HashMap;

    .line 826
    new-instance v0, Lcom/miui/server/xspace/XSpaceManagerServiceImpl$3;

    invoke-direct {v0}, Lcom/miui/server/xspace/XSpaceManagerServiceImpl$3;-><init>()V

    sput-object v0, Lcom/miui/server/xspace/XSpaceManagerServiceImpl;->mUserSwitchObserver:Landroid/app/UserSwitchObserver;

    return-void
.end method

.method public constructor <init>()V
    .locals 2

    .line 71
    invoke-direct {p0}, Lcom/miui/server/xspace/XSpaceManagerServiceStub;-><init>()V

    .line 103
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/miui/server/xspace/XSpaceManagerServiceImpl;->mIsXSpaceCreated:Z

    .line 104
    iput-boolean v0, p0, Lcom/miui/server/xspace/XSpaceManagerServiceImpl;->mIsXSpaceActived:Z

    .line 108
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/miui/server/xspace/XSpaceManagerServiceImpl;->mAnimStatus:Z

    .line 168
    new-instance v0, Lcom/miui/server/xspace/XSpaceManagerServiceImpl$PackageChangedListener;

    const/4 v1, 0x0

    invoke-direct {v0, p0, v1}, Lcom/miui/server/xspace/XSpaceManagerServiceImpl$PackageChangedListener;-><init>(Lcom/miui/server/xspace/XSpaceManagerServiceImpl;Lcom/miui/server/xspace/XSpaceManagerServiceImpl$PackageChangedListener-IA;)V

    iput-object v0, p0, Lcom/miui/server/xspace/XSpaceManagerServiceImpl;->mPackageChangedListener:Lcom/miui/server/xspace/XSpaceManagerServiceImpl$PackageChangedListener;

    return-void
.end method

.method private checkCallXSpacePermission(Ljava/lang/String;)Z
    .locals 5
    .param p1, "callingPkg"    # Ljava/lang/String;

    .line 428
    const/4 v0, 0x0

    :try_start_0
    invoke-static {}, Landroid/app/AppGlobals;->getPackageManager()Landroid/content/pm/IPackageManager;

    move-result-object v1

    .line 429
    const-wide/16 v2, 0x0

    invoke-interface {v1, p1, v2, v3, v0}, Landroid/content/pm/IPackageManager;->getApplicationInfo(Ljava/lang/String;JI)Landroid/content/pm/ApplicationInfo;

    move-result-object v1

    .line 430
    .local v1, "appInfo":Landroid/content/pm/ApplicationInfo;
    iget v2, v1, Landroid/content/pm/ApplicationInfo;->flags:I

    const/4 v3, 0x1

    and-int/2addr v2, v3

    if-gtz v2, :cond_1

    iget v2, v1, Landroid/content/pm/ApplicationInfo;->uid:I
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    const/16 v4, 0x3e8

    if-gt v2, v4, :cond_0

    goto :goto_0

    .line 435
    .end local v1    # "appInfo":Landroid/content/pm/ApplicationInfo;
    :cond_0
    goto :goto_1

    .line 431
    .restart local v1    # "appInfo":Landroid/content/pm/ApplicationInfo;
    :cond_1
    :goto_0
    return v3

    .line 433
    .end local v1    # "appInfo":Landroid/content/pm/ApplicationInfo;
    :catch_0
    move-exception v1

    .line 434
    .local v1, "e":Landroid/os/RemoteException;
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "Failed to read package info of: "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    const-string v3, "XSpaceManagerServiceImpl"

    invoke-static {v3, v2, v1}, Landroid/util/Slog;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    .line 436
    .end local v1    # "e":Landroid/os/RemoteException;
    :goto_1
    return v0
.end method

.method private creatDualAnimIntent(Landroid/content/Intent;)Landroid/content/Intent;
    .locals 3
    .param p1, "intent"    # Landroid/content/Intent;

    .line 303
    new-instance v0, Landroid/content/Intent;

    const-string v1, "miui.intent.action.START_DUAL_ANIMATION"

    invoke-direct {v0, v1}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 304
    .local v0, "result":Landroid/content/Intent;
    const-string v1, "com.miui.securityadd"

    invoke-virtual {v0, v1}, Landroid/content/Intent;->setPackage(Ljava/lang/String;)Landroid/content/Intent;

    .line 305
    const/high16 v1, 0x800000

    invoke-virtual {v0, v1}, Landroid/content/Intent;->addFlags(I)Landroid/content/Intent;

    .line 307
    const-string v1, "android.intent.extra.xspace_cached_uid"

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    .line 309
    const-string v1, "android.intent.extra.auth_to_call_xspace"

    const/4 v2, 0x1

    invoke-virtual {p1, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    .line 311
    const-string v1, "android.intent.extra.INTENT"

    invoke-virtual {v0, v1, p1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Landroid/os/Parcelable;)Landroid/content/Intent;

    .line 312
    return-object v0
.end method

.method private disableGMSApps()V
    .locals 10

    .line 600
    const-string v0, "XSpaceManagerServiceImpl"

    invoke-static {}, Landroid/app/AppGlobals;->getPackageManager()Landroid/content/pm/IPackageManager;

    move-result-object v7

    .line 601
    .local v7, "iPackageManager":Landroid/content/pm/IPackageManager;
    sget-object v1, Lmiui/securityspace/XSpaceConstant;->GMS_RELATED_APPS:Ljava/util/ArrayList;

    invoke-virtual {v1}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v8

    :goto_0
    invoke-interface {v8}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_1

    invoke-interface {v8}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    move-object v9, v1

    check-cast v9, Ljava/lang/String;

    .line 602
    .local v9, "pkgName":Ljava/lang/String;
    iget-object v1, p0, Lcom/miui/server/xspace/XSpaceManagerServiceImpl;->mContext:Landroid/content/Context;

    invoke-static {v1, v9}, Lmiui/securityspace/XSpaceUserHandle;->isAppInXSpace(Landroid/content/Context;Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 604
    const/4 v3, -0x1

    const/4 v4, 0x0

    const/16 v5, 0x3e7

    const/4 v6, 0x4

    move-object v1, v7

    move-object v2, v9

    :try_start_0
    invoke-interface/range {v1 .. v6}, Landroid/content/pm/IPackageManager;->deletePackageAsUser(Ljava/lang/String;ILandroid/content/pm/IPackageDeleteObserver;II)V
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_1
    .catch Ljava/lang/IllegalArgumentException; {:try_start_0 .. :try_end_0} :catch_0

    .line 609
    :goto_1
    goto :goto_2

    .line 607
    :catch_0
    move-exception v1

    .line 608
    .local v1, "e":Ljava/lang/IllegalArgumentException;
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "Unknown package: "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v0, v2, v1}, Landroid/util/Slog;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    goto :goto_2

    .line 605
    .end local v1    # "e":Ljava/lang/IllegalArgumentException;
    :catch_1
    move-exception v1

    .line 606
    .local v1, "e":Landroid/os/RemoteException;
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "Unable to delete package: "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v0, v2, v1}, Landroid/util/Slog;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    .end local v1    # "e":Landroid/os/RemoteException;
    goto :goto_1

    .line 611
    .end local v9    # "pkgName":Ljava/lang/String;
    :cond_0
    :goto_2
    goto :goto_0

    .line 612
    :cond_1
    return-void
.end method

.method private getAimPkg(Landroid/content/Intent;)Ljava/lang/String;
    .locals 2
    .param p1, "intent"    # Landroid/content/Intent;

    .line 398
    invoke-virtual {p1}, Landroid/content/Intent;->getPackage()Ljava/lang/String;

    move-result-object v0

    .line 399
    .local v0, "aimPkg":Ljava/lang/String;
    if-nez v0, :cond_0

    .line 400
    invoke-virtual {p1}, Landroid/content/Intent;->getComponent()Landroid/content/ComponentName;

    move-result-object v1

    .line 401
    .local v1, "componentName":Landroid/content/ComponentName;
    if-eqz v1, :cond_0

    .line 402
    invoke-virtual {v1}, Landroid/content/ComponentName;->getPackageName()Ljava/lang/String;

    move-result-object v0

    .line 405
    .end local v1    # "componentName":Landroid/content/ComponentName;
    :cond_0
    return-object v0
.end method

.method private getCachedUserId(Landroid/content/Intent;Ljava/lang/String;)I
    .locals 4
    .param p1, "intent"    # Landroid/content/Intent;
    .param p2, "callingPackage"    # Ljava/lang/String;

    .line 333
    const/16 v0, -0x2710

    .line 334
    .local v0, "cachedUserId":I
    invoke-direct {p0, p1}, Lcom/miui/server/xspace/XSpaceManagerServiceImpl;->getAimPkg(Landroid/content/Intent;)Ljava/lang/String;

    move-result-object v1

    .line 335
    .local v1, "aimPkg":Ljava/lang/String;
    invoke-static {v1, p2}, Landroid/text/TextUtils;->equals(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Z

    move-result v2

    if-nez v2, :cond_0

    .line 336
    const-string v2, "android.intent.extra.xspace_cached_uid"

    invoke-virtual {p1, v2}, Landroid/content/Intent;->hasExtra(Ljava/lang/String;)Z

    move-result v3

    if-eqz v3, :cond_0

    .line 337
    const/16 v3, -0x2710

    invoke-virtual {p1, v2, v3}, Landroid/content/Intent;->getIntExtra(Ljava/lang/String;I)I

    move-result v0

    .line 338
    invoke-virtual {p1, v2}, Landroid/content/Intent;->removeExtra(Ljava/lang/String;)V

    .line 340
    :cond_0
    return v0
.end method

.method private getPackageName(Landroid/content/Intent;)Ljava/lang/String;
    .locals 2
    .param p1, "intent"    # Landroid/content/Intent;

    .line 529
    invoke-virtual {p1}, Landroid/content/Intent;->getData()Landroid/net/Uri;

    move-result-object v0

    .line 530
    .local v0, "uri":Landroid/net/Uri;
    if-eqz v0, :cond_0

    invoke-virtual {v0}, Landroid/net/Uri;->getSchemeSpecificPart()Ljava/lang/String;

    move-result-object v1

    goto :goto_0

    :cond_0
    const/4 v1, 0x0

    .line 531
    .local v1, "pkg":Ljava/lang/String;
    :goto_0
    return-object v1
.end method

.method private getResolverActivity(Landroid/content/Intent;Ljava/lang/String;ILjava/lang/String;)Landroid/content/Intent;
    .locals 4
    .param p1, "intent"    # Landroid/content/Intent;
    .param p2, "aimPkg"    # Ljava/lang/String;
    .param p3, "requestCode"    # I
    .param p4, "callingPackage"    # Ljava/lang/String;

    .line 410
    new-instance v0, Landroid/content/Intent;

    const-string v1, "miui.intent.action.ACTION_XSPACE_RESOLVER_ACTIVITY_FROM_CORE"

    invoke-direct {v0, v1}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 411
    .local v0, "resolverActivityIntent":Landroid/content/Intent;
    const/high16 v1, 0x2000000

    if-ltz p3, :cond_0

    .line 412
    invoke-virtual {p1, v1}, Landroid/content/Intent;->addFlags(I)Landroid/content/Intent;

    goto :goto_0

    .line 414
    :cond_0
    invoke-virtual {p1}, Landroid/content/Intent;->getFlags()I

    move-result v2

    and-int/2addr v2, v1

    if-eqz v2, :cond_1

    .line 415
    invoke-virtual {v0, v1}, Landroid/content/Intent;->addFlags(I)Landroid/content/Intent;

    .line 417
    :cond_1
    :goto_0
    const-string v1, "miui.intent.extra.xspace_resolver_activity_calling_package"

    invoke-virtual {p1, v1, p4}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 418
    const-string v1, "android.intent.extra.xspace_userid_selected"

    const/4 v2, 0x1

    invoke-virtual {p1, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    .line 419
    const-string v1, "android.intent.extra.xspace_resolver_activity_original_intent"

    invoke-virtual {v0, v1, p1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Landroid/os/Parcelable;)Landroid/content/Intent;

    .line 420
    const-string v1, "android.intent.extra.xspace_resolver_activity_aim_package"

    invoke-virtual {v0, v1, p2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 421
    const-string v1, "com.miui.securitycore"

    const-string v3, "com.miui.xspace.ui.activity.XSpaceResolveActivity"

    invoke-virtual {v0, v1, v3}, Landroid/content/Intent;->setClassName(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 422
    const-string v1, "android.intent.extra.xspace_resolve_intent_again"

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    .line 423
    return-object v0
.end method

.method private getToUserIdFromCachedCallingRelation(Ljava/lang/String;Ljava/lang/String;)I
    .locals 7
    .param p1, "aimPkg"    # Ljava/lang/String;
    .param p2, "callingPackage"    # Ljava/lang/String;

    .line 383
    const/16 v0, -0x2710

    .line 384
    .local v0, "cachedToUserId":I
    sget-object v1, Lcom/miui/server/xspace/XSpaceManagerServiceImpl;->sCachedCallingRelationSelfLocked:Ljava/util/HashMap;

    monitor-enter v1

    .line 385
    :try_start_0
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v2, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, "@"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    .line 386
    .local v2, "callingRelationKey":Ljava/lang/String;
    invoke-virtual {v1, v2}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/lang/Integer;

    .line 387
    .local v3, "toUserIdObj":Ljava/lang/Integer;
    if-eqz v3, :cond_0

    .line 389
    invoke-virtual {v3}, Ljava/lang/Integer;->intValue()I

    move-result v4

    move v0, v4

    .line 390
    invoke-virtual {v1, v2}, Ljava/util/HashMap;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    .line 391
    const-string v4, "XSpaceManagerServiceImpl"

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "got callingRelationKey :"

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, ", cachedToUserId:"

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5}, Landroid/util/Slog;->w(Ljava/lang/String;Ljava/lang/String;)I

    .line 393
    .end local v2    # "callingRelationKey":Ljava/lang/String;
    .end local v3    # "toUserIdObj":Ljava/lang/Integer;
    :cond_0
    monitor-exit v1

    .line 394
    return v0

    .line 393
    :catchall_0
    move-exception v2

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v2
.end method

.method private static getUserManager()Lcom/android/server/pm/UserManagerService;
    .locals 2

    .line 840
    sget-object v0, Lcom/miui/server/xspace/XSpaceManagerServiceImpl;->mUserManager:Lcom/android/server/pm/UserManagerService;

    if-nez v0, :cond_0

    .line 841
    const-string/jumbo v0, "user"

    invoke-static {v0}, Landroid/os/ServiceManager;->getService(Ljava/lang/String;)Landroid/os/IBinder;

    move-result-object v0

    .line 842
    .local v0, "b":Landroid/os/IBinder;
    invoke-static {v0}, Landroid/os/IUserManager$Stub;->asInterface(Landroid/os/IBinder;)Landroid/os/IUserManager;

    move-result-object v1

    check-cast v1, Lcom/android/server/pm/UserManagerService;

    sput-object v1, Lcom/miui/server/xspace/XSpaceManagerServiceImpl;->mUserManager:Lcom/android/server/pm/UserManagerService;

    .line 844
    .end local v0    # "b":Landroid/os/IBinder;
    :cond_0
    sget-object v0, Lcom/miui/server/xspace/XSpaceManagerServiceImpl;->mUserManager:Lcom/android/server/pm/UserManagerService;

    return-object v0
.end method

.method private initXSpaceAppList()V
    .locals 8

    .line 471
    const/4 v0, 0x0

    .line 473
    .local v0, "slice":Landroid/content/pm/ParceledListSlice;, "Landroid/content/pm/ParceledListSlice<Landroid/content/pm/PackageInfo;>;"
    :try_start_0
    invoke-static {}, Landroid/app/AppGlobals;->getPackageManager()Landroid/content/pm/IPackageManager;

    move-result-object v1

    const-wide/16 v2, 0x0

    const/16 v4, 0x3e7

    invoke-interface {v1, v2, v3, v4}, Landroid/content/pm/IPackageManager;->getInstalledPackages(JI)Landroid/content/pm/ParceledListSlice;

    move-result-object v1
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    move-object v0, v1

    .line 476
    goto :goto_0

    .line 474
    :catch_0
    move-exception v1

    .line 475
    .local v1, "e":Landroid/os/RemoteException;
    invoke-virtual {v1}, Landroid/os/RemoteException;->printStackTrace()V

    .line 477
    .end local v1    # "e":Landroid/os/RemoteException;
    :goto_0
    if-eqz v0, :cond_2

    .line 478
    invoke-virtual {v0}, Landroid/content/pm/ParceledListSlice;->getList()Ljava/util/List;

    move-result-object v1

    .line 479
    .local v1, "appList":Ljava/util/List;, "Ljava/util/List<Landroid/content/pm/PackageInfo;>;"
    sget-object v2, Lcom/miui/server/xspace/XSpaceManagerServiceImpl;->sXSpaceInstalledPackagesSelfLocked:Ljava/util/ArrayList;

    monitor-enter v2

    .line 480
    :try_start_1
    invoke-interface {v1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :goto_1
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v4

    if-eqz v4, :cond_1

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Landroid/content/pm/PackageInfo;

    .line 481
    .local v4, "pkgInfo":Landroid/content/pm/PackageInfo;
    iget-object v5, v4, Landroid/content/pm/PackageInfo;->applicationInfo:Landroid/content/pm/ApplicationInfo;

    .line 482
    .local v5, "appInfo":Landroid/content/pm/ApplicationInfo;
    invoke-direct {p0, v5}, Lcom/miui/server/xspace/XSpaceManagerServiceImpl;->isSystemApp(Landroid/content/pm/ApplicationInfo;)Z

    move-result v6

    if-nez v6, :cond_0

    sget-object v6, Lmiui/securityspace/XSpaceConstant;->GMS_RELATED_APPS:Ljava/util/ArrayList;

    iget-object v7, v4, Landroid/content/pm/PackageInfo;->packageName:Ljava/lang/String;

    invoke-virtual {v6, v7}, Ljava/util/ArrayList;->contains(Ljava/lang/Object;)Z

    move-result v6

    if-nez v6, :cond_0

    .line 483
    sget-object v6, Lcom/miui/server/xspace/XSpaceManagerServiceImpl;->sXSpaceInstalledPackagesSelfLocked:Ljava/util/ArrayList;

    iget-object v7, v4, Landroid/content/pm/PackageInfo;->packageName:Ljava/lang/String;

    invoke-virtual {v6, v7}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 485
    .end local v4    # "pkgInfo":Landroid/content/pm/PackageInfo;
    .end local v5    # "appInfo":Landroid/content/pm/ApplicationInfo;
    :cond_0
    goto :goto_1

    .line 486
    :cond_1
    monitor-exit v2

    goto :goto_2

    :catchall_0
    move-exception v3

    monitor-exit v2
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    throw v3

    .line 488
    .end local v1    # "appList":Ljava/util/List;, "Ljava/util/List<Landroid/content/pm/PackageInfo;>;"
    :cond_2
    :goto_2
    sget-object v1, Lcom/miui/server/xspace/XSpaceManagerServiceImpl;->sXSpaceInstalledPackagesSelfLocked:Ljava/util/ArrayList;

    invoke-virtual {v1}, Ljava/util/ArrayList;->size()I

    move-result v2

    sget v3, Lcom/miui/server/xspace/XSpaceManagerServiceImpl;->XSPACE_APP_LIST_INIT_NUMBER:I

    if-le v2, v3, :cond_3

    .line 489
    const/4 v2, 0x1

    iput-boolean v2, p0, Lcom/miui/server/xspace/XSpaceManagerServiceImpl;->mIsXSpaceActived:Z

    .line 491
    :cond_3
    iget-object v2, p0, Lcom/miui/server/xspace/XSpaceManagerServiceImpl;->mResolver:Landroid/content/ContentResolver;

    const-string/jumbo v4, "xspace_enabled"

    iget-boolean v5, p0, Lcom/miui/server/xspace/XSpaceManagerServiceImpl;->mIsXSpaceActived:Z

    invoke-static {v2, v4, v5}, Landroid/provider/MiuiSettings$Secure;->putBoolean(Landroid/content/ContentResolver;Ljava/lang/String;Z)V

    .line 492
    const-string v2, "XSpaceManagerServiceImpl"

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "initXSpaceAppList sXSpaceInstalledPackagesSelfLocked ="

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v1}, Ljava/util/ArrayList;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v4, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v4, "    XSPACE_APP_LIST_INIT_NUMBER ="

    invoke-virtual {v1, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v2, v1}, Landroid/util/Slog;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 494
    const-string v1, "XSpaceManagerServiceImpl"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "Reset XSpace enable, active:"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget-boolean v3, p0, Lcom/miui/server/xspace/XSpaceManagerServiceImpl;->mIsXSpaceActived:Z

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Slog;->w(Ljava/lang/String;Ljava/lang/String;)I

    .line 495
    return-void
.end method

.method private isComponentEnabled(Landroid/content/pm/ActivityInfo;I)Z
    .locals 5
    .param p1, "info"    # Landroid/content/pm/ActivityInfo;
    .param p2, "userId"    # I

    .line 662
    if-nez p1, :cond_0

    .line 663
    const/4 v0, 0x1

    return v0

    .line 665
    :cond_0
    const/4 v0, 0x0

    .line 666
    .local v0, "enabled":Z
    new-instance v1, Landroid/content/ComponentName;

    iget-object v2, p1, Landroid/content/pm/ActivityInfo;->packageName:Ljava/lang/String;

    iget-object v3, p1, Landroid/content/pm/ActivityInfo;->name:Ljava/lang/String;

    invoke-direct {v1, v2, v3}, Landroid/content/ComponentName;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    .line 672
    .local v1, "compname":Landroid/content/ComponentName;
    :try_start_0
    invoke-static {}, Landroid/app/AppGlobals;->getPackageManager()Landroid/content/pm/IPackageManager;

    move-result-object v2

    const-wide/16 v3, 0x0

    invoke-interface {v2, v1, v3, v4, p2}, Landroid/content/pm/IPackageManager;->getActivityInfo(Landroid/content/ComponentName;JI)Landroid/content/pm/ActivityInfo;

    move-result-object v2
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    if-eqz v2, :cond_1

    .line 673
    const/4 v0, 0x1

    .line 677
    :cond_1
    goto :goto_0

    .line 675
    :catch_0
    move-exception v2

    .line 676
    .local v2, "e":Landroid/os/RemoteException;
    const/4 v0, 0x0

    .line 678
    .end local v2    # "e":Landroid/os/RemoteException;
    :goto_0
    if-nez v0, :cond_2

    .line 679
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "Component not enabled: "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, "  in user "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    const-string v3, "XSpaceManagerServiceImpl"

    invoke-static {v3, v2}, Landroid/util/Slog;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 681
    :cond_2
    return v0
.end method

.method private isPublicIntent(Landroid/content/Intent;Ljava/lang/String;Landroid/content/pm/ActivityInfo;)Z
    .locals 7
    .param p1, "intent"    # Landroid/content/Intent;
    .param p2, "callingPackage"    # Ljava/lang/String;
    .param p3, "aInfo"    # Landroid/content/pm/ActivityInfo;

    .line 347
    invoke-static {p2}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    const/4 v1, 0x0

    if-eqz v0, :cond_0

    .line 349
    return v1

    .line 352
    :cond_0
    if-eqz p3, :cond_1

    .line 353
    iget-object v0, p3, Landroid/content/pm/ActivityInfo;->packageName:Ljava/lang/String;

    .local v0, "aimPkg":Ljava/lang/String;
    goto :goto_0

    .line 355
    .end local v0    # "aimPkg":Ljava/lang/String;
    :cond_1
    invoke-direct {p0, p1}, Lcom/miui/server/xspace/XSpaceManagerServiceImpl;->getAimPkg(Landroid/content/Intent;)Ljava/lang/String;

    move-result-object v0

    .line 357
    .restart local v0    # "aimPkg":Ljava/lang/String;
    :goto_0
    invoke-static {v0, p2}, Landroid/text/TextUtils;->equals(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Z

    move-result v2

    if-eqz v2, :cond_2

    .line 358
    return v1

    .line 360
    :cond_2
    new-instance v2, Landroid/content/Intent;

    invoke-direct {v2, p1}, Landroid/content/Intent;-><init>(Landroid/content/Intent;)V

    .line 362
    .local v2, "newIntent":Landroid/content/Intent;
    const/4 v3, 0x1

    :try_start_0
    invoke-static {v1}, Landroid/os/BaseBundle;->setShouldDefuse(Z)V

    .line 363
    const-string v4, ""

    invoke-virtual {v2, v4}, Landroid/content/Intent;->hasExtra(Ljava/lang/String;)Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 369
    invoke-static {v3}, Landroid/os/BaseBundle;->setShouldDefuse(Z)V

    .line 370
    nop

    .line 371
    return v3

    .line 364
    :catchall_0
    move-exception v4

    .line 366
    .local v4, "e":Ljava/lang/Throwable;
    :try_start_1
    const-string v5, "XSpaceManagerServiceImpl"

    const-string v6, "Private intent"

    invoke-static {v5, v6, v4}, Landroid/util/Slog;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_1

    .line 367
    nop

    .line 369
    invoke-static {v3}, Landroid/os/BaseBundle;->setShouldDefuse(Z)V

    .line 367
    return v1

    .line 369
    .end local v4    # "e":Ljava/lang/Throwable;
    :catchall_1
    move-exception v1

    invoke-static {v3}, Landroid/os/BaseBundle;->setShouldDefuse(Z)V

    .line 370
    throw v1
.end method

.method private isSystemApp(Landroid/content/pm/ApplicationInfo;)Z
    .locals 3
    .param p1, "appInfo"    # Landroid/content/pm/ApplicationInfo;

    .line 498
    iget v0, p1, Landroid/content/pm/ApplicationInfo;->flags:I

    const/4 v1, 0x1

    and-int/2addr v0, v1

    if-gtz v0, :cond_1

    iget v0, p1, Landroid/content/pm/ApplicationInfo;->uid:I

    const/16 v2, 0x3e8

    if-gt v0, v2, :cond_0

    goto :goto_0

    :cond_0
    const/4 v1, 0x0

    :cond_1
    :goto_0
    return v1
.end method

.method private needSetBootXSpaceGuideTaskForCompete(Landroid/content/Context;)Z
    .locals 2
    .param p1, "context"    # Landroid/content/Context;

    .line 651
    const-string v0, "key_xspace_boot_guide_times"

    invoke-static {p1, v0}, Landroid/provider/MiuiSettings$XSpace;->getGuideNotificationTimes(Landroid/content/Context;Ljava/lang/String;)I

    move-result v0

    const/4 v1, 0x1

    if-ge v0, v1, :cond_0

    goto :goto_0

    :cond_0
    const/4 v1, 0x0

    :goto_0
    return v1
.end method

.method private needSetInstallXSpaceGuideTaskForCompete(Landroid/content/Context;)Z
    .locals 2
    .param p1, "context"    # Landroid/content/Context;

    .line 657
    const-string v0, "key_xspace_compete_guide_times"

    invoke-static {p1, v0}, Landroid/provider/MiuiSettings$XSpace;->getGuideNotificationTimes(Landroid/content/Context;Ljava/lang/String;)I

    move-result v0

    const/4 v1, 0x1

    if-ge v0, v1, :cond_0

    goto :goto_0

    :cond_0
    const/4 v1, 0x0

    :goto_0
    return v1
.end method

.method private onPackageCallback(Ljava/lang/String;Landroid/os/UserHandle;Ljava/lang/String;)V
    .locals 6
    .param p1, "packageName"    # Ljava/lang/String;
    .param p2, "user"    # Landroid/os/UserHandle;
    .param p3, "action"    # Ljava/lang/String;

    .line 535
    const-string v0, "XSpaceManagerServiceImpl"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v2, "update XSpace App: packageName:"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ", user:"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ", action:"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Slog;->w(Ljava/lang/String;Ljava/lang/String;)I

    .line 536
    invoke-static {p2}, Lmiui/securityspace/XSpaceUserHandle;->isXSpaceUser(Landroid/os/UserHandle;)Z

    move-result v0

    if-eqz v0, :cond_c

    const-string v0, "android.intent.action.PACKAGE_ADDED"

    .line 537
    invoke-virtual {v0, p3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    const-string v0, "android.intent.action.PACKAGE_REMOVED"

    invoke-virtual {v0, p3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_c

    .line 538
    :cond_0
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/miui/server/xspace/XSpaceManagerServiceImpl;->mIsXSpaceCreated:Z

    .line 539
    const-string v1, "persist.sys.xspace_created"

    const-string v2, "1"

    invoke-static {v1, v2}, Landroid/os/SystemProperties;->set(Ljava/lang/String;Ljava/lang/String;)V

    .line 540
    const-string v1, "android.intent.action.PACKAGE_ADDED"

    invoke-virtual {v1, p3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    const/16 v2, 0x3e7

    if-eqz v1, :cond_6

    .line 541
    invoke-direct {p0}, Lcom/miui/server/xspace/XSpaceManagerServiceImpl;->setXSpaceApplicationHidden()V

    .line 542
    const-string v1, "com.xiaomi.gamecenter.sdk.service"

    invoke-static {v1, p1}, Landroid/text/TextUtils;->equals(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Z

    move-result v1

    if-nez v1, :cond_1

    const-string v1, "org.ifaa.aidl.manager"

    invoke-static {v1, p1}, Landroid/text/TextUtils;->equals(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Z

    move-result v1

    if-eqz v1, :cond_2

    .line 543
    :cond_1
    sget-object v1, Lcom/miui/server/xspace/XSpaceManagerServiceImpl;->sXSpaceInstalledPackagesSelfLocked:Ljava/util/ArrayList;

    monitor-enter v1

    .line 544
    :try_start_0
    invoke-direct {p0, v0}, Lcom/miui/server/xspace/XSpaceManagerServiceImpl;->updateXSpaceStatusLocked(Z)V

    .line 545
    invoke-virtual {v1, p1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 546
    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 548
    :cond_2
    const/4 v1, 0x0

    .line 550
    .local v1, "isSystemApp":Z
    :try_start_1
    invoke-static {}, Landroid/app/AppGlobals;->getPackageManager()Landroid/content/pm/IPackageManager;

    move-result-object v3

    .line 551
    const-wide/16 v4, 0x0

    invoke-interface {v3, p1, v4, v5, v2}, Landroid/content/pm/IPackageManager;->getApplicationInfo(Ljava/lang/String;JI)Landroid/content/pm/ApplicationInfo;

    move-result-object v2

    .line 552
    .local v2, "appInfo":Landroid/content/pm/ApplicationInfo;
    if-nez v2, :cond_3

    .line 553
    return-void

    .line 555
    :cond_3
    invoke-direct {p0, v2}, Lcom/miui/server/xspace/XSpaceManagerServiceImpl;->isSystemApp(Landroid/content/pm/ApplicationInfo;)Z

    move-result v3
    :try_end_1
    .catch Landroid/os/RemoteException; {:try_start_1 .. :try_end_1} :catch_0

    move v1, v3

    .line 558
    .end local v2    # "appInfo":Landroid/content/pm/ApplicationInfo;
    goto :goto_0

    .line 556
    :catch_0
    move-exception v2

    .line 557
    .local v2, "e":Landroid/os/RemoteException;
    const-string v3, "XSpaceManagerServiceImpl"

    const-string v4, "PMS died"

    invoke-static {v3, v4, v2}, Landroid/util/Slog;->d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    .line 559
    .end local v2    # "e":Landroid/os/RemoteException;
    :goto_0
    if-nez v1, :cond_5

    sget-object v2, Lmiui/securityspace/XSpaceConstant;->GMS_RELATED_APPS:Ljava/util/ArrayList;

    invoke-virtual {v2, p1}, Ljava/util/ArrayList;->contains(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_4

    goto :goto_1

    .line 564
    :cond_4
    sget-object v2, Lcom/miui/server/xspace/XSpaceManagerServiceImpl;->sXSpaceInstalledPackagesSelfLocked:Ljava/util/ArrayList;

    monitor-enter v2

    .line 565
    :try_start_2
    invoke-direct {p0, v0}, Lcom/miui/server/xspace/XSpaceManagerServiceImpl;->updateXSpaceStatusLocked(Z)V

    .line 566
    invoke-virtual {v2, p1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 567
    monitor-exit v2

    .line 568
    .end local v1    # "isSystemApp":Z
    goto/16 :goto_5

    .line 567
    .restart local v1    # "isSystemApp":Z
    :catchall_0
    move-exception v0

    monitor-exit v2
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    throw v0

    .line 561
    :cond_5
    :goto_1
    const-string v0, "XSpaceManagerServiceImpl"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "XSpace ignore system or GMS package: "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v0, v2}, Landroid/util/Slog;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 562
    return-void

    .line 546
    .end local v1    # "isSystemApp":Z
    :catchall_1
    move-exception v0

    :try_start_3
    monitor-exit v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    throw v0

    .line 569
    :cond_6
    iget-object v0, p0, Lcom/miui/server/xspace/XSpaceManagerServiceImpl;->mContext:Landroid/content/Context;

    invoke-static {v0, p1}, Landroid/provider/MiuiSettings$XSpace;->resetDefaultSetting(Landroid/content/Context;Ljava/lang/String;)V

    .line 570
    sget-object v0, Lcom/miui/server/xspace/XSpaceManagerServiceImpl;->sXSpaceInstalledPackagesSelfLocked:Ljava/util/ArrayList;

    monitor-enter v0

    .line 571
    :try_start_4
    invoke-virtual {v0, p1}, Ljava/util/ArrayList;->remove(Ljava/lang/Object;)Z

    .line 572
    const/4 v1, 0x0

    invoke-direct {p0, v1}, Lcom/miui/server/xspace/XSpaceManagerServiceImpl;->updateXSpaceStatusLocked(Z)V

    .line 573
    monitor-exit v0
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_2

    .line 574
    iget-object v0, p0, Lcom/miui/server/xspace/XSpaceManagerServiceImpl;->mContext:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v0

    .line 575
    const/16 v1, 0x40

    invoke-virtual {v0, v1, v2}, Landroid/content/pm/PackageManager;->getInstalledPackagesAsUser(II)Ljava/util/List;

    move-result-object v0

    .line 576
    .local v0, "appList":Ljava/util/List;, "Ljava/util/List<Landroid/content/pm/PackageInfo;>;"
    if-eqz v0, :cond_b

    invoke-interface {v0}, Ljava/util/List;->isEmpty()Z

    move-result v1

    if-eqz v1, :cond_7

    goto :goto_4

    .line 579
    :cond_7
    const/4 v1, 0x1

    .line 580
    .local v1, "needRemoveGMS":Z
    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_2
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_9

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Landroid/content/pm/PackageInfo;

    .line 581
    .local v3, "packageInfo":Landroid/content/pm/PackageInfo;
    iget-object v4, p0, Lcom/miui/server/xspace/XSpaceManagerServiceImpl;->mContext:Landroid/content/Context;

    iget-object v5, v3, Landroid/content/pm/PackageInfo;->packageName:Ljava/lang/String;

    invoke-static {v4, v5}, Lmiui/securityspace/XSpaceUtils;->isGMSRequired(Landroid/content/Context;Ljava/lang/String;)Z

    move-result v4

    if-eqz v4, :cond_8

    .line 582
    const/4 v1, 0x0

    .line 583
    goto :goto_3

    .line 585
    .end local v3    # "packageInfo":Landroid/content/pm/PackageInfo;
    :cond_8
    goto :goto_2

    .line 586
    :cond_9
    :goto_3
    if-eqz v1, :cond_a

    .line 587
    invoke-direct {p0}, Lcom/miui/server/xspace/XSpaceManagerServiceImpl;->disableGMSApps()V

    .line 589
    .end local v0    # "appList":Ljava/util/List;, "Ljava/util/List<Landroid/content/pm/PackageInfo;>;"
    .end local v1    # "needRemoveGMS":Z
    :cond_a
    goto :goto_5

    .line 577
    .restart local v0    # "appList":Ljava/util/List;, "Ljava/util/List<Landroid/content/pm/PackageInfo;>;"
    :cond_b
    :goto_4
    return-void

    .line 573
    .end local v0    # "appList":Ljava/util/List;, "Ljava/util/List<Landroid/content/pm/PackageInfo;>;"
    :catchall_2
    move-exception v1

    :try_start_5
    monitor-exit v0
    :try_end_5
    .catchall {:try_start_5 .. :try_end_5} :catchall_2

    throw v1

    .line 590
    :cond_c
    invoke-virtual {p2}, Landroid/os/UserHandle;->getIdentifier()I

    move-result v0

    if-nez v0, :cond_d

    const-string v0, "android.intent.action.PACKAGE_ADDED"

    .line 591
    invoke-virtual {v0, p3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_d

    sget-object v0, Landroid/provider/MiuiSettings$XSpace;->sCompeteXSpaceApps:Ljava/util/ArrayList;

    .line 592
    invoke-virtual {v0, p1}, Ljava/util/ArrayList;->contains(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_d

    iget-object v0, p0, Lcom/miui/server/xspace/XSpaceManagerServiceImpl;->mContext:Landroid/content/Context;

    .line 593
    invoke-direct {p0, v0}, Lcom/miui/server/xspace/XSpaceManagerServiceImpl;->needSetInstallXSpaceGuideTaskForCompete(Landroid/content/Context;)Z

    move-result v0

    if-eqz v0, :cond_d

    iget-object v0, p0, Lcom/miui/server/xspace/XSpaceManagerServiceImpl;->mContext:Landroid/content/Context;

    .line 594
    invoke-static {v0}, Lmiui/securityspace/CrossUserUtils;->hasXSpaceUser(Landroid/content/Context;)Z

    move-result v0

    if-nez v0, :cond_d

    .line 595
    iget-object v0, p0, Lcom/miui/server/xspace/XSpaceManagerServiceImpl;->mContext:Landroid/content/Context;

    const-string v1, "param_intent_value_compete_install_xspace_guide"

    invoke-direct {p0, v0, v1}, Lcom/miui/server/xspace/XSpaceManagerServiceImpl;->startXSpaceService(Landroid/content/Context;Ljava/lang/String;)V

    .line 597
    :cond_d
    :goto_5
    return-void
.end method

.method private putCachedCallingRelation(Ljava/lang/String;Ljava/lang/String;I)V
    .locals 5
    .param p1, "aimPkg"    # Ljava/lang/String;
    .param p2, "callingPackage"    # Ljava/lang/String;
    .param p3, "cachedUserId"    # I

    .line 375
    sget-object v0, Lcom/miui/server/xspace/XSpaceManagerServiceImpl;->sCachedCallingRelationSelfLocked:Ljava/util/HashMap;

    monitor-enter v0

    .line 376
    :try_start_0
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "@"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    .line 377
    .local v1, "callingRelationKey":Ljava/lang/String;
    invoke-static {p3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 378
    const-string v2, "XSpaceManagerServiceImpl"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "putCachedCallingRelationm, callingRelationKey:"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, ", cachedUserId:"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, p3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Landroid/util/Slog;->w(Ljava/lang/String;Ljava/lang/String;)I

    .line 379
    nop

    .end local v1    # "callingRelationKey":Ljava/lang/String;
    monitor-exit v0

    .line 380
    return-void

    .line 379
    :catchall_0
    move-exception v1

    monitor-exit v0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v1
.end method

.method private resolveActivity(Landroid/content/Intent;Lcom/android/server/wm/ActivityTaskSupervisor;Ljava/lang/String;ILandroid/app/ProfilerInfo;III)Landroid/content/pm/ActivityInfo;
    .locals 13
    .param p1, "intent"    # Landroid/content/Intent;
    .param p2, "stack"    # Lcom/android/server/wm/ActivityTaskSupervisor;
    .param p3, "resolvedType"    # Ljava/lang/String;
    .param p4, "startFlags"    # I
    .param p5, "profilerInfo"    # Landroid/app/ProfilerInfo;
    .param p6, "userId"    # I
    .param p7, "callingUid"    # I
    .param p8, "callingPid"    # I

    .line 889
    const/4 v1, 0x0

    .line 891
    .local v1, "activityInfo":Landroid/content/pm/ActivityInfo;
    :try_start_0
    const-class v0, Landroid/content/pm/ActivityInfo;

    const-string v2, "resolveActivity"

    const/4 v3, 0x7

    new-array v4, v3, [Ljava/lang/Class;

    const-class v5, Landroid/content/Intent;

    const/4 v6, 0x0

    aput-object v5, v4, v6

    const-class v5, Ljava/lang/String;

    const/4 v7, 0x1

    aput-object v5, v4, v7

    sget-object v5, Ljava/lang/Integer;->TYPE:Ljava/lang/Class;

    const/4 v8, 0x2

    aput-object v5, v4, v8

    const-class v5, Landroid/app/ProfilerInfo;

    const/4 v9, 0x3

    aput-object v5, v4, v9

    sget-object v5, Ljava/lang/Integer;->TYPE:Ljava/lang/Class;

    const/4 v10, 0x4

    aput-object v5, v4, v10

    sget-object v5, Ljava/lang/Integer;->TYPE:Ljava/lang/Class;

    const/4 v11, 0x5

    aput-object v5, v4, v11

    sget-object v5, Ljava/lang/Integer;->TYPE:Ljava/lang/Class;

    const/4 v12, 0x6

    aput-object v5, v4, v12

    new-array v3, v3, [Ljava/lang/Object;

    aput-object p1, v3, v6

    aput-object p3, v3, v7

    .line 893
    invoke-static/range {p4 .. p4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    aput-object v5, v3, v8

    aput-object p5, v3, v9

    invoke-static/range {p6 .. p6}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    aput-object v5, v3, v10

    invoke-static/range {p7 .. p7}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    aput-object v5, v3, v11

    invoke-static/range {p8 .. p8}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    aput-object v5, v3, v12
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_1

    .line 891
    move-object v5, p2

    :try_start_1
    invoke-static {p2, v0, v2, v4, v3}, Lcom/miui/server/xspace/ReflectUtil;->callObjectMethod(Ljava/lang/Object;Ljava/lang/Class;Ljava/lang/String;[Ljava/lang/Class;[Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/content/pm/ActivityInfo;
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_0

    move-object v1, v0

    .line 896
    goto :goto_1

    .line 894
    :catch_0
    move-exception v0

    goto :goto_0

    :catch_1
    move-exception v0

    move-object v5, p2

    .line 895
    .local v0, "exception":Ljava/lang/Exception;
    :goto_0
    const-string v2, "XSpaceManagerServiceImpl"

    const-string v3, "Call resolveIntent fail."

    invoke-static {v2, v3, v0}, Landroid/util/Slog;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    .line 898
    .end local v0    # "exception":Ljava/lang/Exception;
    :goto_1
    return-object v1
.end method

.method private setSwitchUserCallingUid(I)V
    .locals 0
    .param p1, "uid"    # I

    .line 836
    sput p1, Lcom/miui/server/xspace/XSpaceManagerServiceImpl;->sSwitchUserCallingUid:I

    .line 837
    return-void
.end method

.method private setXSpaceApplicationHidden()V
    .locals 5

    .line 615
    sget-boolean v0, Lmiui/os/Build;->IS_INTERNATIONAL_BUILD:Z

    if-nez v0, :cond_0

    .line 616
    return-void

    .line 619
    :cond_0
    :try_start_0
    sget-object v0, Lcom/miui/server/xspace/XSpaceManagerServiceImpl;->sXSpaceApplicationList:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v0

    :goto_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_2

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/String;

    .line 620
    .local v1, "pkgName":Ljava/lang/String;
    invoke-static {}, Landroid/app/AppGlobals;->getPackageManager()Landroid/content/pm/IPackageManager;

    move-result-object v2

    const/16 v3, 0x3e7

    invoke-interface {v2, v1, v3}, Landroid/content/pm/IPackageManager;->getApplicationHiddenSettingAsUser(Ljava/lang/String;I)Z

    move-result v2

    if-nez v2, :cond_1

    .line 621
    invoke-static {}, Landroid/app/AppGlobals;->getPackageManager()Landroid/content/pm/IPackageManager;

    move-result-object v2

    const/4 v4, 0x1

    invoke-interface {v2, v1, v4, v3}, Landroid/content/pm/IPackageManager;->setApplicationHiddenSettingAsUser(Ljava/lang/String;ZI)Z
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    .line 623
    .end local v1    # "pkgName":Ljava/lang/String;
    :cond_1
    goto :goto_0

    .line 626
    :cond_2
    goto :goto_1

    .line 624
    :catch_0
    move-exception v0

    .line 625
    .local v0, "e":Landroid/os/RemoteException;
    const-string v1, "XSpaceManagerServiceImpl"

    const-string v2, "hidden xspace error"

    invoke-static {v1, v2, v0}, Landroid/util/Slog;->d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    .line 627
    .end local v0    # "e":Landroid/os/RemoteException;
    :goto_1
    return-void
.end method

.method private shouldResolveAgain(Landroid/content/Intent;Ljava/lang/String;)Z
    .locals 6
    .param p1, "intent"    # Landroid/content/Intent;
    .param p2, "callingPackage"    # Ljava/lang/String;

    .line 316
    const-string v0, "android.intent.extra.xspace_resolve_intent_again"

    invoke-direct {p0, p1}, Lcom/miui/server/xspace/XSpaceManagerServiceImpl;->getAimPkg(Landroid/content/Intent;)Ljava/lang/String;

    move-result-object v1

    .line 317
    .local v1, "aimPkg":Ljava/lang/String;
    new-instance v2, Landroid/content/Intent;

    invoke-direct {v2, p1}, Landroid/content/Intent;-><init>(Landroid/content/Intent;)V

    .line 319
    .local v2, "newIntent":Landroid/content/Intent;
    const/4 v3, 0x0

    :try_start_0
    invoke-static {v1, p2}, Landroid/text/TextUtils;->equals(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Z

    move-result v4

    if-nez v4, :cond_0

    .line 320
    invoke-virtual {v2, v0}, Landroid/content/Intent;->hasExtra(Ljava/lang/String;)Z

    move-result v4

    if-eqz v4, :cond_0

    .line 321
    invoke-virtual {p1, v0}, Landroid/content/Intent;->removeExtra(Ljava/lang/String;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 322
    const/4 v0, 0x1

    return v0

    .line 328
    :cond_0
    nop

    .line 329
    return v3

    .line 324
    :catch_0
    move-exception v0

    .line 326
    .local v0, "e":Ljava/lang/Exception;
    const-string v4, "XSpaceManagerServiceImpl"

    const-string v5, "Private intent: "

    invoke-static {v4, v5, v0}, Landroid/util/Slog;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    .line 327
    return v3
.end method

.method private startXSpaceService(Landroid/content/Context;Ljava/lang/String;)V
    .locals 2
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "extra"    # Ljava/lang/String;

    .line 640
    new-instance v0, Landroid/content/Intent;

    invoke-direct {v0}, Landroid/content/Intent;-><init>()V

    .line 641
    .local v0, "intent":Landroid/content/Intent;
    const-string v1, "com.miui.securitycore/com.miui.xspace.service.XSpaceService"

    invoke-static {v1}, Landroid/content/ComponentName;->unflattenFromString(Ljava/lang/String;)Landroid/content/ComponentName;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/content/Intent;->setComponent(Landroid/content/ComponentName;)Landroid/content/Intent;

    .line 643
    if-eqz p2, :cond_0

    .line 644
    const-string v1, "param_intent_key_has_extra"

    invoke-virtual {v0, v1, p2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 646
    :cond_0
    invoke-virtual {p1, v0}, Landroid/content/Context;->startService(Landroid/content/Intent;)Landroid/content/ComponentName;

    .line 647
    return-void
.end method

.method private updateXSpaceStatusLocked(Z)V
    .locals 4
    .param p1, "isXSpaceActive"    # Z

    .line 631
    sget-object v0, Lcom/miui/server/xspace/XSpaceManagerServiceImpl;->sXSpaceInstalledPackagesSelfLocked:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v1

    sget v2, Lcom/miui/server/xspace/XSpaceManagerServiceImpl;->XSPACE_APP_LIST_INIT_NUMBER:I

    if-ne v1, v2, :cond_0

    .line 632
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v3, "updateXSpaceStatusLocked sXSpaceInstalledPackagesSelfLocked ="

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v0}, Ljava/util/ArrayList;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "    XSPACE_APP_LIST_INIT_NUMBER ="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    const-string v1, "XSpaceManagerServiceImpl"

    invoke-static {v1, v0}, Landroid/util/Slog;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 634
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v2, "update XSpace Enable = "

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v1, v0}, Landroid/util/Slog;->w(Ljava/lang/String;Ljava/lang/String;)I

    .line 635
    iget-object v0, p0, Lcom/miui/server/xspace/XSpaceManagerServiceImpl;->mResolver:Landroid/content/ContentResolver;

    const-string/jumbo v1, "xspace_enabled"

    invoke-static {v0, v1, p1}, Landroid/provider/MiuiSettings$Secure;->putBoolean(Landroid/content/ContentResolver;Ljava/lang/String;Z)V

    .line 637
    :cond_0
    return-void
.end method


# virtual methods
.method public checkAndGetXSpaceUserId(II)I
    .locals 1
    .param p1, "flags"    # I
    .param p2, "defUserId"    # I

    .line 767
    invoke-static {p1, p2}, Lmiui/securityspace/XSpaceUserHandle;->checkAndGetXSpaceUserId(II)I

    move-result v0

    return v0
.end method

.method public checkExternalStorageForXSpace(Landroid/content/Context;ILjava/lang/String;)Z
    .locals 4
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "uid"    # I
    .param p3, "pkgName"    # Ljava/lang/String;

    .line 736
    invoke-static {p2}, Lmiui/securityspace/XSpaceUserHandle;->isUidBelongtoXSpace(I)Z

    move-result v0

    const/4 v1, 0x0

    if-nez v0, :cond_0

    .line 737
    return v1

    .line 741
    :cond_0
    :try_start_0
    invoke-virtual {p1}, Landroid/content/Context;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v0

    invoke-virtual {v0, p3, v1}, Landroid/content/pm/PackageManager;->getApplicationInfo(Ljava/lang/String;I)Landroid/content/pm/ApplicationInfo;

    move-result-object v0
    :try_end_0
    .catch Landroid/content/pm/PackageManager$NameNotFoundException; {:try_start_0 .. :try_end_0} :catch_0

    .line 745
    .local v0, "appInfo":Landroid/content/pm/ApplicationInfo;
    nop

    .line 746
    iget v2, v0, Landroid/content/pm/ApplicationInfo;->flags:I

    const/4 v3, 0x1

    and-int/2addr v2, v3

    if-lez v2, :cond_1

    move v1, v3

    :cond_1
    return v1

    .line 742
    .end local v0    # "appInfo":Landroid/content/pm/ApplicationInfo;
    :catch_0
    move-exception v0

    .line 743
    .local v0, "e":Landroid/content/pm/PackageManager$NameNotFoundException;
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "Failed to get package info for "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    const-string v3, "XSpaceManagerServiceImpl"

    invoke-static {v3, v2, v0}, Landroid/util/Slog;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    .line 744
    return v1
.end method

.method public checkXSpaceControl(Landroid/content/Context;Landroid/content/pm/ActivityInfo;Landroid/content/Intent;ZIILjava/lang/String;ILcom/android/server/wm/SafeActivityOptions;)Landroid/content/Intent;
    .locals 14
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "aInfo"    # Landroid/content/pm/ActivityInfo;
    .param p3, "intent"    # Landroid/content/Intent;
    .param p4, "fromActivity"    # Z
    .param p5, "requestCode"    # I
    .param p6, "userId"    # I
    .param p7, "callingPackage"    # Ljava/lang/String;
    .param p8, "callingUserId"    # I
    .param p9, "safeActivityOptions"    # Lcom/android/server/wm/SafeActivityOptions;

    .line 173
    move-object v1, p0

    move-object/from16 v2, p2

    move-object/from16 v3, p3

    move/from16 v4, p6

    move-object/from16 v5, p7

    move/from16 v6, p8

    invoke-direct {p0, v3, v5, v2}, Lcom/miui/server/xspace/XSpaceManagerServiceImpl;->isPublicIntent(Landroid/content/Intent;Ljava/lang/String;Landroid/content/pm/ActivityInfo;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 174
    return-object v3

    .line 178
    :cond_0
    invoke-virtual/range {p3 .. p3}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v7

    .line 179
    .local v7, "action":Ljava/lang/String;
    if-eqz v2, :cond_1

    .line 180
    iget-object v0, v2, Landroid/content/pm/ActivityInfo;->packageName:Ljava/lang/String;

    move-object v8, v0

    .local v0, "aimPkg":Ljava/lang/String;
    goto :goto_0

    .line 182
    .end local v0    # "aimPkg":Ljava/lang/String;
    :cond_1
    invoke-direct {p0, v3}, Lcom/miui/server/xspace/XSpaceManagerServiceImpl;->getAimPkg(Landroid/content/Intent;)Ljava/lang/String;

    move-result-object v0

    move-object v8, v0

    .line 184
    .local v8, "aimPkg":Ljava/lang/String;
    :goto_0
    const-string v0, "XSpaceManagerServiceImpl"

    new-instance v9, Ljava/lang/StringBuilder;

    invoke-direct {v9}, Ljava/lang/StringBuilder;-><init>()V

    const-string v10, "checkXSpaceControl, from:"

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    const-string v10, ", to:"

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    const-string v10, ", with act:"

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    const-string v10, ", callingUserId:"

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9, v6}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v9

    const-string v10, ", toUserId:"

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v9

    invoke-static {v0, v9}, Landroid/util/Slog;->w(Ljava/lang/String;Ljava/lang/String;)I

    .line 187
    const/16 v0, 0x3e7

    if-eqz v6, :cond_2

    if-eq v6, v0, :cond_2

    .line 189
    return-object v3

    .line 191
    :cond_2
    if-nez v6, :cond_3

    if-nez v4, :cond_3

    sget-object v9, Lcom/miui/server/xspace/XSpaceManagerServiceImpl;->sXSpaceInstalledPackagesSelfLocked:Ljava/util/ArrayList;

    .line 192
    invoke-virtual {v9, v8}, Ljava/util/ArrayList;->contains(Ljava/lang/Object;)Z

    move-result v10

    if-nez v10, :cond_3

    .line 193
    invoke-virtual {v9, v5}, Ljava/util/ArrayList;->contains(Ljava/lang/Object;)Z

    move-result v9

    if-nez v9, :cond_3

    .line 195
    return-object v3

    .line 197
    :cond_3
    invoke-virtual/range {p3 .. p3}, Landroid/content/Intent;->getFlags()I

    move-result v9

    const/high16 v10, 0x100000

    and-int/2addr v9, v10

    if-eqz v9, :cond_4

    .line 199
    return-object v3

    .line 201
    :cond_4
    invoke-virtual/range {p3 .. p3}, Landroid/content/Intent;->getComponent()Landroid/content/ComponentName;

    move-result-object v9

    if-eqz v9, :cond_5

    .line 202
    const-class v9, Landroid/content/pm/PackageManagerInternal;

    invoke-static {v9}, Lcom/android/server/LocalServices;->getService(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v9

    check-cast v9, Landroid/content/pm/PackageManagerInternal;

    .line 203
    .local v9, "packageManagerInternal":Landroid/content/pm/PackageManagerInternal;
    invoke-virtual {v9, v8, v0}, Landroid/content/pm/PackageManagerInternal;->getDisabledComponents(Ljava/lang/String;I)Landroid/util/ArraySet;

    move-result-object v10

    .line 204
    .local v10, "disabledComponents":Landroid/util/ArraySet;, "Landroid/util/ArraySet<Ljava/lang/String;>;"
    if-eqz v10, :cond_5

    invoke-virtual/range {p3 .. p3}, Landroid/content/Intent;->getComponent()Landroid/content/ComponentName;

    move-result-object v11

    invoke-virtual {v11}, Landroid/content/ComponentName;->getClassName()Ljava/lang/String;

    move-result-object v11

    invoke-virtual {v10, v11}, Landroid/util/ArraySet;->contains(Ljava/lang/Object;)Z

    move-result v11

    if-eqz v11, :cond_5

    .line 205
    return-object v3

    .line 208
    .end local v9    # "packageManagerInternal":Landroid/content/pm/PackageManagerInternal;
    .end local v10    # "disabledComponents":Landroid/util/ArraySet;, "Landroid/util/ArraySet<Ljava/lang/String;>;"
    :cond_5
    sget-object v9, Lcom/miui/server/xspace/XSpaceManagerServiceImpl;->sCrossUserCallingPackagesWhiteList:Ljava/util/ArrayList;

    invoke-virtual {v9, v5}, Ljava/util/ArrayList;->contains(Ljava/lang/Object;)Z

    move-result v10

    if-eqz v10, :cond_6

    sget-object v10, Lcom/miui/server/xspace/XSpaceManagerServiceImpl;->sPublicActionList:Ljava/util/ArrayList;

    invoke-virtual {v10, v7}, Ljava/util/ArrayList;->contains(Ljava/lang/Object;)Z

    move-result v10

    if-eqz v10, :cond_7

    :cond_6
    sget-object v10, Lcom/miui/server/xspace/XSpaceManagerServiceImpl;->sCrossUserAimPackagesWhiteList:Ljava/util/ArrayList;

    .line 209
    invoke-virtual {v10, v8}, Ljava/util/ArrayList;->contains(Ljava/lang/Object;)Z

    move-result v10

    if-eqz v10, :cond_8

    .line 211
    :cond_7
    return-object v3

    .line 214
    :cond_8
    sget-object v10, Lcom/miui/server/xspace/XSpaceManagerServiceImpl;->sCrossUserDisableComponentActionWhiteList:Ljava/util/ArrayList;

    invoke-virtual {v10, v7}, Ljava/util/ArrayList;->contains(Ljava/lang/Object;)Z

    move-result v10

    if-eqz v10, :cond_9

    .line 215
    invoke-direct {p0, v2, v0}, Lcom/miui/server/xspace/XSpaceManagerServiceImpl;->isComponentEnabled(Landroid/content/pm/ActivityInfo;I)Z

    move-result v10

    if-nez v10, :cond_9

    .line 216
    return-object v3

    .line 219
    :cond_9
    const-string v10, "android.intent.extra.xspace_userid_selected"

    invoke-virtual {v3, v10}, Landroid/content/Intent;->hasExtra(Ljava/lang/String;)Z

    move-result v10

    if-eqz v10, :cond_a

    .line 221
    const-string v0, "XSpaceManagerServiceImpl"

    const-string v9, "from XSpace ResolverActivity"

    invoke-static {v0, v9}, Landroid/util/Slog;->w(Ljava/lang/String;Ljava/lang/String;)I

    .line 222
    const-string v0, "android.intent.extra.xspace_resolve_intent_again"

    invoke-virtual {v3, v0}, Landroid/content/Intent;->removeExtra(Ljava/lang/String;)V

    .line 223
    invoke-static {v3, v6}, Lmiui/securityspace/XSpaceIntentCompat;->prepareToLeaveUser(Landroid/content/Intent;I)V

    .line 224
    invoke-direct {p0, v8, v5, v6}, Lcom/miui/server/xspace/XSpaceManagerServiceImpl;->putCachedCallingRelation(Ljava/lang/String;Ljava/lang/String;I)V

    goto :goto_1

    .line 227
    :cond_a
    invoke-direct {p0, v8, v5}, Lcom/miui/server/xspace/XSpaceManagerServiceImpl;->getToUserIdFromCachedCallingRelation(Ljava/lang/String;Ljava/lang/String;)I

    move-result v10

    .line 228
    .local v10, "cachedToUserId":I
    const/16 v11, -0x2710

    if-eq v10, v11, :cond_b

    .line 230
    const-string v0, "XSpaceManagerServiceImpl"

    const-string/jumbo v9, "using cached calling relation"

    invoke-static {v0, v9}, Landroid/util/Slog;->w(Ljava/lang/String;Ljava/lang/String;)I

    .line 231
    const-string v0, "android.intent.extra.xspace_cached_uid"

    invoke-virtual {v3, v0, v10}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    .line 267
    .end local v10    # "cachedToUserId":I
    :goto_1
    move/from16 v12, p5

    goto/16 :goto_6

    .line 234
    .restart local v10    # "cachedToUserId":I
    :cond_b
    sget-object v11, Lcom/miui/server/xspace/XSpaceManagerServiceImpl;->sXSpaceInstalledPackagesSelfLocked:Ljava/util/ArrayList;

    monitor-enter v11

    .line 235
    :try_start_0
    invoke-virtual {v11, v8}, Ljava/util/ArrayList;->contains(Ljava/lang/Object;)Z

    move-result v12

    if-eqz v12, :cond_14

    .line 236
    const-string v0, "android.intent.extra.auth_to_call_xspace"

    invoke-virtual {v3, v0}, Landroid/content/Intent;->hasExtra(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_d

    .line 237
    invoke-direct {p0, v5}, Lcom/miui/server/xspace/XSpaceManagerServiceImpl;->checkCallXSpacePermission(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_c

    goto :goto_2

    :cond_c
    move/from16 v12, p5

    goto/16 :goto_4

    :cond_d
    :goto_2
    sget-object v0, Lcom/miui/server/xspace/XSpaceManagerServiceImpl;->PACKAGE_ALIPAY:Ljava/lang/String;

    .line 238
    invoke-virtual {v0, v8}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_13

    const-string v0, "com.xiaomi.gamecenter.sdk.service"

    .line 239
    invoke-virtual {v0, v8}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_12

    const-string v0, "com.xiaomi.xmsf"

    .line 240
    invoke-static {v0, v5}, Landroid/text/TextUtils;->equals(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_11

    const-string v0, "com.sina.weibo.sdk.action.ACTION_SDK_REQ_ACTIVITY"

    .line 241
    invoke-static {v0, v7}, Landroid/text/TextUtils;->equals(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_10

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v0, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v9, ".wxapi.WXEntryActivity"

    invoke-virtual {v0, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    .line 242
    invoke-virtual/range {p3 .. p3}, Landroid/content/Intent;->getComponent()Landroid/content/ComponentName;

    move-result-object v9

    if-nez v9, :cond_e

    const-string v9, ""

    goto :goto_3

    :cond_e
    invoke-virtual/range {p3 .. p3}, Landroid/content/Intent;->getComponent()Landroid/content/ComponentName;

    move-result-object v9

    invoke-virtual {v9}, Landroid/content/ComponentName;->getClassName()Ljava/lang/String;

    move-result-object v9

    :goto_3
    invoke-static {v0, v9}, Landroid/text/TextUtils;->equals(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_f

    move/from16 v12, p5

    goto :goto_4

    .line 247
    :cond_f
    const-string v0, "XSpaceManagerServiceImpl"

    const-string v9, "pop up ResolverActivity"

    invoke-static {v0, v9}, Landroid/util/Slog;->w(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 248
    move/from16 v12, p5

    :try_start_1
    invoke-direct {p0, v3, v8, v12, v5}, Lcom/miui/server/xspace/XSpaceManagerServiceImpl;->getResolverActivity(Landroid/content/Intent;Ljava/lang/String;ILjava/lang/String;)Landroid/content/Intent;

    move-result-object v0

    move-object v3, v0

    .end local p3    # "intent":Landroid/content/Intent;
    .local v0, "intent":Landroid/content/Intent;
    goto :goto_5

    .line 241
    .end local v0    # "intent":Landroid/content/Intent;
    .restart local p3    # "intent":Landroid/content/Intent;
    :cond_10
    move/from16 v12, p5

    goto :goto_4

    .line 240
    :cond_11
    move/from16 v12, p5

    goto :goto_4

    .line 239
    :cond_12
    move/from16 v12, p5

    goto :goto_4

    .line 238
    :cond_13
    move/from16 v12, p5

    .line 243
    :goto_4
    const-string v0, "XSpaceManagerServiceImpl"

    const-string v9, "call XSpace directly"

    invoke-static {v0, v9}, Landroid/util/Slog;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 244
    monitor-exit v11

    return-object v3

    .line 249
    :cond_14
    move/from16 v12, p5

    invoke-virtual {v11, v5}, Ljava/util/ArrayList;->contains(Ljava/lang/Object;)Z

    move-result v13

    if-nez v13, :cond_15

    .line 250
    invoke-virtual {v9, v8}, Ljava/util/ArrayList;->contains(Ljava/lang/Object;)Z

    move-result v9

    if-eqz v9, :cond_18

    if-ne v4, v0, :cond_18

    .line 251
    :cond_15
    const-string v0, "XSpaceManagerServiceImpl"

    const-string v9, "XSpace installed App to normal App"

    invoke-static {v0, v9}, Landroid/util/Slog;->w(Ljava/lang/String;Ljava/lang/String;)I

    .line 252
    sget-object v0, Lcom/miui/server/xspace/XSpaceManagerServiceImpl;->sAddUserPackagesBlackList:Ljava/util/ArrayList;

    invoke-virtual {v0, v8}, Ljava/util/ArrayList;->contains(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_16

    .line 253
    invoke-static {v3, v6}, Lmiui/securityspace/XSpaceIntentCompat;->prepareToLeaveUser(Landroid/content/Intent;I)V

    .line 255
    :cond_16
    const-string v0, "android.settings.APPLICATION_DETAILS_SETTINGS"

    invoke-static {v7, v0}, Landroid/text/TextUtils;->equals(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_17

    .line 256
    const-string v0, "miui.intent.extra.USER_ID"

    invoke-virtual {v3, v0, v6}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    .line 258
    :cond_17
    const-string v0, "android.intent.extra.xspace_cached_uid"

    const/4 v9, 0x0

    invoke-virtual {v3, v0, v9}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    .line 260
    const-string/jumbo v0, "userId"

    invoke-virtual {v3, v0, v4}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    .line 262
    const-string v0, "calling_relation"

    const/4 v9, 0x1

    invoke-virtual {v3, v0, v9}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 264
    .end local p3    # "intent":Landroid/content/Intent;
    .local v3, "intent":Landroid/content/Intent;
    :cond_18
    :goto_5
    :try_start_2
    monitor-exit v11

    .line 267
    .end local v10    # "cachedToUserId":I
    :goto_6
    return-object v3

    .line 264
    .end local v3    # "intent":Landroid/content/Intent;
    .restart local v10    # "cachedToUserId":I
    .restart local p3    # "intent":Landroid/content/Intent;
    :catchall_0
    move-exception v0

    goto :goto_7

    :catchall_1
    move-exception v0

    move/from16 v12, p5

    .end local p3    # "intent":Landroid/content/Intent;
    .restart local v3    # "intent":Landroid/content/Intent;
    :goto_7
    monitor-exit v11
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_2

    throw v0

    :catchall_2
    move-exception v0

    goto :goto_7
.end method

.method public computeGids(I[ILjava/lang/String;)[I
    .locals 4
    .param p1, "userId"    # I
    .param p2, "gids"    # [I
    .param p3, "packageName"    # Ljava/lang/String;

    .line 695
    sget-object v0, Lcom/miui/server/xspace/XSpaceManagerServiceImpl;->mediaAccessRequired:Ljava/util/List;

    invoke-interface {v0, p3}, Ljava/util/List;->contains(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    if-eqz p2, :cond_0

    .line 696
    const/16 v0, 0x3ff

    invoke-static {p2, v0}, Lcom/android/internal/util/ArrayUtils;->appendInt([II)[I

    move-result-object p2

    .line 698
    :cond_0
    const/4 v0, 0x0

    if-eqz p1, :cond_2

    invoke-static {p1, p3}, Lcom/miui/server/xspace/SecSpaceManagerService;->isDataTransferProcess(ILjava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_2

    .line 699
    invoke-static {v0}, Landroid/os/UserHandle;->getUserGid(I)I

    move-result v0

    invoke-static {p2, v0}, Lcom/android/internal/util/ArrayUtils;->appendInt([II)[I

    move-result-object p2

    .line 700
    iget-boolean v0, p0, Lcom/miui/server/xspace/XSpaceManagerServiceImpl;->mIsXSpaceCreated:Z

    if-eqz v0, :cond_1

    .line 701
    sget v0, Lmiui/securityspace/XSpaceUserHandle;->XSPACE_SHARED_USER_GID:I

    invoke-static {p2, v0}, Lcom/android/internal/util/ArrayUtils;->appendInt([II)[I

    move-result-object p2

    .line 703
    :cond_1
    return-object p2

    .line 706
    :cond_2
    sget-object v1, Lmiui/securityspace/XSpaceConstant;->XSPACE_PACKAGES_SHARED_GID:Ljava/util/ArrayList;

    invoke-virtual {v1, p3}, Ljava/util/ArrayList;->contains(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_3

    .line 707
    sget v0, Lmiui/securityspace/XSpaceUserHandle;->XSPACE_SHARED_USER_GID:I

    invoke-static {p2, v0}, Lcom/android/internal/util/ArrayUtils;->appendInt([II)[I

    move-result-object p2

    .line 708
    return-object p2

    .line 711
    :cond_3
    iget-boolean v1, p0, Lcom/miui/server/xspace/XSpaceManagerServiceImpl;->mIsXSpaceCreated:Z

    if-eqz v1, :cond_a

    if-nez p2, :cond_4

    goto :goto_4

    .line 714
    :cond_4
    invoke-static {p1}, Lmiui/securityspace/XSpaceUserHandle;->isXSpaceUserId(I)Z

    move-result v1

    if-eqz v1, :cond_7

    .line 715
    array-length v1, p2

    :goto_0
    if-ge v0, v1, :cond_6

    aget v2, p2, v0

    .line 716
    .local v2, "gid":I
    sget v3, Lmiui/securityspace/XSpaceUserHandle;->XSPACE_SHARED_USER_GID:I

    if-ne v2, v3, :cond_5

    .line 717
    sget v0, Lmiui/securityspace/XSpaceUserHandle;->OWNER_SHARED_USER_GID:I

    invoke-static {p2, v0}, Lcom/android/internal/util/ArrayUtils;->appendInt([II)[I

    move-result-object p2

    .line 718
    goto :goto_1

    .line 715
    .end local v2    # "gid":I
    :cond_5
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    :cond_6
    :goto_1
    goto :goto_3

    .line 721
    :cond_7
    if-nez p1, :cond_9

    .line 722
    array-length v1, p2

    :goto_2
    if-ge v0, v1, :cond_9

    aget v2, p2, v0

    .line 723
    .restart local v2    # "gid":I
    sget v3, Lmiui/securityspace/XSpaceUserHandle;->OWNER_SHARED_USER_GID:I

    if-ne v2, v3, :cond_8

    .line 724
    sget v0, Lmiui/securityspace/XSpaceUserHandle;->XSPACE_SHARED_USER_GID:I

    invoke-static {p2, v0}, Lcom/android/internal/util/ArrayUtils;->appendInt([II)[I

    move-result-object p2

    .line 725
    goto :goto_3

    .line 722
    .end local v2    # "gid":I
    :cond_8
    add-int/lit8 v0, v0, 0x1

    goto :goto_2

    .line 729
    :cond_9
    :goto_3
    return-object p2

    .line 712
    :cond_a
    :goto_4
    return-object p2
.end method

.method public getDefaultUserId()I
    .locals 4

    .line 853
    :try_start_0
    const-string/jumbo v0, "user"

    invoke-static {v0}, Landroid/os/ServiceManager;->getService(Ljava/lang/String;)Landroid/os/IBinder;

    move-result-object v0

    invoke-static {v0}, Landroid/os/IUserManager$Stub;->asInterface(Landroid/os/IBinder;)Landroid/os/IUserManager;

    move-result-object v0

    .line 854
    .local v0, "um":Landroid/os/IUserManager;
    const/4 v1, 0x0

    invoke-interface {v0, v1, v1, v1}, Landroid/os/IUserManager;->getUsers(ZZZ)Ljava/util/List;

    move-result-object v1

    invoke-interface {v1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_1

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Landroid/content/pm/UserInfo;

    .line 855
    .local v2, "userInfo":Landroid/content/pm/UserInfo;
    invoke-static {v2}, Lmiui/securityspace/XSpaceUserHandle;->isXSpaceUser(Landroid/content/pm/UserInfo;)Z

    move-result v3

    if-eqz v3, :cond_0

    .line 856
    invoke-static {}, Landroid/app/ActivityManager;->getCurrentUser()I

    move-result v1
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    return v1

    .line 858
    .end local v2    # "userInfo":Landroid/content/pm/UserInfo;
    :cond_0
    goto :goto_0

    .line 861
    .end local v0    # "um":Landroid/os/IUserManager;
    :cond_1
    goto :goto_1

    .line 859
    :catch_0
    move-exception v0

    .line 862
    :goto_1
    const/4 v0, -0x1

    return v0
.end method

.method public getUserTypeCount([I)I
    .locals 4
    .param p1, "userIds"    # [I

    .line 757
    array-length v0, p1

    .line 758
    .local v0, "result":I
    const/4 v1, 0x0

    .local v1, "i":I
    :goto_0
    array-length v2, p1

    if-ge v1, v2, :cond_1

    .line 759
    aget v2, p1, v1

    const/16 v3, 0x3e7

    if-ne v2, v3, :cond_0

    .line 760
    add-int/lit8 v0, v0, -0x1

    .line 758
    :cond_0
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 763
    .end local v1    # "i":I
    :cond_1
    return v0
.end method

.method public getXSpaceUserId()I
    .locals 1

    .line 866
    const/16 v0, 0x3e7

    return v0
.end method

.method public handleWindowManagerAndUserLru(Landroid/content/Context;IIILcom/android/server/wm/WindowManagerService;[I)V
    .locals 5
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "userId"    # I
    .param p3, "userIdOrig"    # I
    .param p4, "oldUserId"    # I
    .param p5, "mWindowManager"    # Lcom/android/server/wm/WindowManagerService;
    .param p6, "mCurrentProfileIds"    # [I

    .line 772
    invoke-virtual {p5, p3}, Lcom/android/server/wm/WindowManagerService;->setCurrentUser(I)V

    .line 773
    invoke-virtual {p1}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    const-string/jumbo v1, "second_user_id"

    const/16 v2, -0x2710

    invoke-static {v0, v1, v2}, Landroid/provider/Settings$Secure;->getInt(Landroid/content/ContentResolver;Ljava/lang/String;I)I

    move-result v0

    .line 775
    .local v0, "privacyUserId":I
    invoke-virtual {p1}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v1

    const-string v3, "kid_user_id"

    invoke-static {v1, v3, v2}, Landroid/provider/Settings$Secure;->getInt(Landroid/content/ContentResolver;Ljava/lang/String;I)I

    move-result v1

    .line 777
    .local v1, "kidSpaceUserId":I
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "privacyUserId :"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, " userId:"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, " userIdOrig:"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, " oldUserId:"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, " kidSpaceUserId:"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    const-string v3, "XSpaceManagerServiceImpl"

    invoke-static {v3, v2}, Landroid/util/Slog;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 779
    sget v2, Lcom/miui/server/xspace/XSpaceManagerServiceImpl;->sSwitchUserCallingUid:I

    invoke-static {v2}, Landroid/os/UserHandle;->getAppId(I)I

    move-result v2

    const/16 v4, 0x3e8

    if-ne v2, v4, :cond_4

    if-ne v0, p4, :cond_0

    if-eqz p2, :cond_3

    :cond_0
    if-nez p4, :cond_1

    if-eq p2, v0, :cond_3

    :cond_1
    if-ne v1, p4, :cond_2

    if-eqz p2, :cond_3

    :cond_2
    if-nez p4, :cond_4

    if-ne p2, v1, :cond_4

    .line 784
    :cond_3
    const-string/jumbo v2, "switch without lock"

    invoke-static {v3, v2}, Landroid/util/Slog;->d(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    .line 788
    :cond_4
    const/4 v2, 0x0

    invoke-virtual {p5, v2}, Lcom/android/server/wm/WindowManagerService;->lockNow(Landroid/os/Bundle;)V

    .line 790
    :goto_0
    return-void
.end method

.method public init(Landroid/content/Context;)V
    .locals 7
    .param p1, "context"    # Landroid/content/Context;

    .line 440
    iput-object p1, p0, Lcom/miui/server/xspace/XSpaceManagerServiceImpl;->mContext:Landroid/content/Context;

    .line 441
    invoke-virtual {p1}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    iput-object v0, p0, Lcom/miui/server/xspace/XSpaceManagerServiceImpl;->mResolver:Landroid/content/ContentResolver;

    .line 442
    invoke-direct {p0}, Lcom/miui/server/xspace/XSpaceManagerServiceImpl;->initXSpaceAppList()V

    .line 443
    iget-object v0, p0, Lcom/miui/server/xspace/XSpaceManagerServiceImpl;->mResolver:Landroid/content/ContentResolver;

    .line 444
    const-string/jumbo v1, "xspace_enabled"

    invoke-static {v1}, Landroid/provider/Settings$Secure;->getUriFor(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v1

    new-instance v2, Lcom/miui/server/xspace/XSpaceManagerServiceImpl$1;

    new-instance v3, Landroid/os/Handler;

    invoke-direct {v3}, Landroid/os/Handler;-><init>()V

    invoke-direct {v2, p0, v3}, Lcom/miui/server/xspace/XSpaceManagerServiceImpl$1;-><init>(Lcom/miui/server/xspace/XSpaceManagerServiceImpl;Landroid/os/Handler;)V

    .line 443
    const/4 v3, 0x0

    invoke-virtual {v0, v1, v3, v2}, Landroid/content/ContentResolver;->registerContentObserver(Landroid/net/Uri;ZLandroid/database/ContentObserver;)V

    .line 455
    new-instance v0, Landroid/content/IntentFilter;

    invoke-direct {v0}, Landroid/content/IntentFilter;-><init>()V

    .line 456
    .local v0, "filter":Landroid/content/IntentFilter;
    const-string v1, "android.intent.action.PACKAGE_ADDED"

    invoke-virtual {v0, v1}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    .line 457
    const-string v1, "android.intent.action.PACKAGE_REMOVED_INTERNAL"

    invoke-virtual {v0, v1}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    .line 458
    const-string v1, "package"

    invoke-virtual {v0, v1}, Landroid/content/IntentFilter;->addDataScheme(Ljava/lang/String;)V

    .line 459
    iget-object v1, p0, Lcom/miui/server/xspace/XSpaceManagerServiceImpl;->mContext:Landroid/content/Context;

    iget-object v2, p0, Lcom/miui/server/xspace/XSpaceManagerServiceImpl;->mPackageChangedListener:Lcom/miui/server/xspace/XSpaceManagerServiceImpl$PackageChangedListener;

    const/16 v3, 0x3e7

    invoke-static {v3}, Landroid/os/UserHandle;->of(I)Landroid/os/UserHandle;

    move-result-object v3

    const/4 v5, 0x0

    const/4 v6, 0x0

    move-object v4, v0

    invoke-virtual/range {v1 .. v6}, Landroid/content/Context;->registerReceiverAsUser(Landroid/content/BroadcastReceiver;Landroid/os/UserHandle;Landroid/content/IntentFilter;Ljava/lang/String;Landroid/os/Handler;)Landroid/content/Intent;

    .line 461
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "XSpace init, active:"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-boolean v2, p0, Lcom/miui/server/xspace/XSpaceManagerServiceImpl;->mIsXSpaceActived:Z

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    const-string v2, "XSpaceManagerServiceImpl"

    invoke-static {v2, v1}, Landroid/util/Slog;->w(Ljava/lang/String;Ljava/lang/String;)I

    .line 462
    invoke-static {p1}, Lmiui/securityspace/CrossUserUtils;->hasXSpaceUser(Landroid/content/Context;)Z

    move-result v1

    iput-boolean v1, p0, Lcom/miui/server/xspace/XSpaceManagerServiceImpl;->mIsXSpaceCreated:Z

    .line 463
    if-eqz v1, :cond_0

    .line 464
    const-string v1, "persist.sys.xspace_created"

    const-string v2, "1"

    invoke-static {v1, v2}, Landroid/os/SystemProperties;->set(Ljava/lang/String;Ljava/lang/String;)V

    .line 465
    const/4 v1, 0x0

    invoke-direct {p0, p1, v1}, Lcom/miui/server/xspace/XSpaceManagerServiceImpl;->startXSpaceService(Landroid/content/Context;Ljava/lang/String;)V

    .line 467
    :cond_0
    invoke-static {p1}, Lcom/miui/server/xspace/SecSpaceManagerService;->init(Landroid/content/Context;)V

    .line 468
    return-void
.end method

.method public isXSpaceActive()Z
    .locals 1

    .line 691
    iget-boolean v0, p0, Lcom/miui/server/xspace/XSpaceManagerServiceImpl;->mIsXSpaceActived:Z

    return v0
.end method

.method public needCheckUser(Landroid/content/pm/ProviderInfo;Ljava/lang/String;IZ)Z
    .locals 1
    .param p1, "cpi"    # Landroid/content/pm/ProviderInfo;
    .param p2, "processName"    # Ljava/lang/String;
    .param p3, "userId"    # I
    .param p4, "checkUser"    # Z

    .line 848
    invoke-static {p1, p2, p3, p4}, Lmiui/securityspace/CrossUserUtils;->needCheckUser(Landroid/content/pm/ProviderInfo;Ljava/lang/String;IZ)Z

    move-result v0

    return v0
.end method

.method public resolveXSpaceIntent(Landroid/content/pm/ActivityInfo;Landroid/content/Intent;Lcom/android/server/wm/ActivityTaskSupervisor;Landroid/app/ProfilerInfo;Ljava/lang/String;IILjava/lang/String;II)Landroid/content/pm/ActivityInfo;
    .locals 18
    .param p1, "aInfo"    # Landroid/content/pm/ActivityInfo;
    .param p2, "intent"    # Landroid/content/Intent;
    .param p3, "stack"    # Lcom/android/server/wm/ActivityTaskSupervisor;
    .param p4, "profilerInfo"    # Landroid/app/ProfilerInfo;
    .param p5, "resolvedType"    # Ljava/lang/String;
    .param p6, "startFlags"    # I
    .param p7, "userId"    # I
    .param p8, "callingPackage"    # Ljava/lang/String;
    .param p9, "callingUserId"    # I
    .param p10, "callingPid"    # I

    .line 273
    move-object/from16 v10, p0

    move-object/from16 v1, p1

    move-object/from16 v11, p2

    move/from16 v12, p7

    move-object/from16 v13, p8

    if-eqz v12, :cond_0

    const/16 v0, 0x3e7

    if-ne v12, v0, :cond_1

    .line 274
    :cond_0
    invoke-direct {v10, v11, v13, v1}, Lcom/miui/server/xspace/XSpaceManagerServiceImpl;->isPublicIntent(Landroid/content/Intent;Ljava/lang/String;Landroid/content/pm/ActivityInfo;)Z

    move-result v0

    if-nez v0, :cond_2

    .line 275
    :cond_1
    return-object v1

    .line 278
    :cond_2
    invoke-direct {v10, v11, v13}, Lcom/miui/server/xspace/XSpaceManagerServiceImpl;->shouldResolveAgain(Landroid/content/Intent;Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_3

    .line 280
    :try_start_0
    invoke-static {}, Landroid/app/AppGlobals;->getPackageManager()Landroid/content/pm/IPackageManager;

    move-result-object v2

    const/4 v4, 0x0

    const-wide/16 v5, -0x1

    move-object/from16 v3, p2

    move/from16 v7, p7

    invoke-interface/range {v2 .. v7}, Landroid/content/pm/IPackageManager;->resolveIntent(Landroid/content/Intent;Ljava/lang/String;JI)Landroid/content/pm/ResolveInfo;

    move-result-object v0

    iget-object v0, v0, Landroid/content/pm/ResolveInfo;->activityInfo:Landroid/content/pm/ActivityInfo;
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    .line 284
    .end local p1    # "aInfo":Landroid/content/pm/ActivityInfo;
    .local v0, "aInfo":Landroid/content/pm/ActivityInfo;
    goto :goto_0

    .line 282
    .end local v0    # "aInfo":Landroid/content/pm/ActivityInfo;
    .restart local p1    # "aInfo":Landroid/content/pm/ActivityInfo;
    :catch_0
    move-exception v0

    .line 283
    .local v0, "e":Landroid/os/RemoteException;
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "Can not get aInfo, intent = "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    const-string v3, "XSpaceManagerServiceImpl"

    invoke-static {v3, v2, v0}, Landroid/util/Slog;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    .line 287
    .end local v0    # "e":Landroid/os/RemoteException;
    :cond_3
    move-object v0, v1

    .end local p1    # "aInfo":Landroid/content/pm/ActivityInfo;
    .local v0, "aInfo":Landroid/content/pm/ActivityInfo;
    :goto_0
    invoke-direct {v10, v11, v13}, Lcom/miui/server/xspace/XSpaceManagerServiceImpl;->getCachedUserId(Landroid/content/Intent;Ljava/lang/String;)I

    move-result v14

    .line 288
    .local v14, "cachedUserId":I
    const/16 v1, -0x2710

    if-eq v14, v1, :cond_5

    .line 290
    invoke-static {}, Landroid/os/Binder;->getCallingUid()I

    move-result v15

    .line 291
    .local v15, "callingUid":I
    invoke-static {}, Landroid/os/Binder;->clearCallingIdentity()J

    move-result-wide v16

    .line 292
    .local v16, "token":J
    move-object/from16 v1, p0

    move-object/from16 v2, p2

    move-object/from16 v3, p3

    move-object/from16 v4, p5

    move/from16 v5, p6

    move-object/from16 v6, p4

    move v7, v14

    move v8, v15

    move/from16 v9, p10

    invoke-direct/range {v1 .. v9}, Lcom/miui/server/xspace/XSpaceManagerServiceImpl;->resolveActivity(Landroid/content/Intent;Lcom/android/server/wm/ActivityTaskSupervisor;Ljava/lang/String;ILandroid/app/ProfilerInfo;III)Landroid/content/pm/ActivityInfo;

    move-result-object v0

    .line 293
    invoke-static/range {v16 .. v17}, Landroid/os/Binder;->restoreCallingIdentity(J)V

    .line 294
    const-string v1, "calling_relation"

    const/4 v2, 0x0

    invoke-virtual {v11, v1, v2}, Landroid/content/Intent;->getBooleanExtra(Ljava/lang/String;Z)Z

    move-result v1

    if-eqz v1, :cond_4

    if-eqz v0, :cond_4

    .line 296
    iget-object v1, v0, Landroid/content/pm/ActivityInfo;->packageName:Ljava/lang/String;

    move/from16 v2, p9

    invoke-direct {v10, v1, v13, v2}, Lcom/miui/server/xspace/XSpaceManagerServiceImpl;->putCachedCallingRelation(Ljava/lang/String;Ljava/lang/String;I)V

    goto :goto_1

    .line 294
    :cond_4
    move/from16 v2, p9

    goto :goto_1

    .line 288
    .end local v15    # "callingUid":I
    .end local v16    # "token":J
    :cond_5
    move/from16 v2, p9

    .line 299
    :goto_1
    return-object v0
.end method

.method public setXSpaceCreated(IZ)V
    .locals 1
    .param p1, "userId"    # I
    .param p2, "isXSpaceCreated"    # Z

    .line 685
    const/16 v0, 0x3e7

    if-ne p1, v0, :cond_0

    .line 686
    iput-boolean p2, p0, Lcom/miui/server/xspace/XSpaceManagerServiceImpl;->mIsXSpaceCreated:Z

    .line 688
    :cond_0
    return-void
.end method

.method public shouldCrossXSpace(Ljava/lang/String;I)Z
    .locals 6
    .param p1, "packageName"    # Ljava/lang/String;
    .param p2, "userId"    # I

    .line 870
    const/16 v0, 0x3e7

    const/4 v1, 0x0

    if-ne p2, v0, :cond_2

    .line 871
    invoke-static {}, Landroid/os/Binder;->clearCallingIdentity()J

    move-result-wide v2

    .line 873
    .local v2, "origId":J
    :try_start_0
    invoke-static {}, Landroid/app/AppGlobals;->getPackageManager()Landroid/content/pm/IPackageManager;

    move-result-object v0

    const-wide/16 v4, 0x0

    invoke-interface {v0, p1, v4, v5, p2}, Landroid/content/pm/IPackageManager;->getPackageInfo(Ljava/lang/String;JI)Landroid/content/pm/PackageInfo;

    move-result-object v0

    if-nez v0, :cond_1

    .line 874
    invoke-static {}, Landroid/app/AppGlobals;->getPackageManager()Landroid/content/pm/IPackageManager;

    move-result-object v0

    invoke-interface {v0, p1, v4, v5, v1}, Landroid/content/pm/IPackageManager;->getPackageInfo(Ljava/lang/String;JI)Landroid/content/pm/PackageInfo;

    move-result-object v0

    .line 875
    .local v0, "packageInfo":Landroid/content/pm/PackageInfo;
    if-eqz v0, :cond_0

    iget-object v4, v0, Landroid/content/pm/PackageInfo;->applicationInfo:Landroid/content/pm/ApplicationInfo;

    if-eqz v4, :cond_0

    iget-object v4, v0, Landroid/content/pm/PackageInfo;->applicationInfo:Landroid/content/pm/ApplicationInfo;

    iget-boolean v4, v4, Landroid/content/pm/ApplicationInfo;->enabled:Z
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    if-eqz v4, :cond_0

    const/4 v1, 0x1

    .line 880
    :cond_0
    invoke-static {v2, v3}, Landroid/os/Binder;->restoreCallingIdentity(J)V

    .line 875
    return v1

    .line 880
    .end local v0    # "packageInfo":Landroid/content/pm/PackageInfo;
    :catchall_0
    move-exception v0

    invoke-static {v2, v3}, Landroid/os/Binder;->restoreCallingIdentity(J)V

    .line 881
    throw v0

    .line 877
    :catch_0
    move-exception v0

    .line 880
    :cond_1
    invoke-static {v2, v3}, Landroid/os/Binder;->restoreCallingIdentity(J)V

    .line 881
    nop

    .line 883
    .end local v2    # "origId":J
    :cond_2
    return v1
.end method

.method public shouldInstallInXSpace(Landroid/os/UserHandle;I)Z
    .locals 1
    .param p1, "installUser"    # Landroid/os/UserHandle;
    .param p2, "userId"    # I

    .line 750
    if-nez p1, :cond_0

    invoke-static {p2}, Lmiui/securityspace/XSpaceUserHandle;->isXSpaceUserId(I)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 751
    const/4 v0, 0x0

    return v0

    .line 753
    :cond_0
    const/4 v0, 0x1

    return v0
.end method

.method public showSwitchingDialog(Lcom/android/server/am/ActivityManagerService;Landroid/content/Context;IILandroid/os/Handler;I)Z
    .locals 11
    .param p1, "ams"    # Lcom/android/server/am/ActivityManagerService;
    .param p2, "context"    # Landroid/content/Context;
    .param p3, "currentUserId"    # I
    .param p4, "targetUserId"    # I
    .param p5, "handler"    # Landroid/os/Handler;
    .param p6, "userSwitchUiMessage"    # I

    .line 793
    invoke-static {}, Landroid/os/Binder;->getCallingUid()I

    move-result v0

    move-object v9, p0

    invoke-direct {p0, v0}, Lcom/miui/server/xspace/XSpaceManagerServiceImpl;->setSwitchUserCallingUid(I)V

    .line 794
    invoke-static {}, Landroid/os/Binder;->getCallingPid()I

    move-result v0

    invoke-static {v0}, Lcom/android/server/am/ProcessUtils;->getPackageNameByPid(I)Ljava/lang/String;

    move-result-object v0

    .line 795
    .local v0, "pkgName":Ljava/lang/String;
    if-eqz v0, :cond_1

    const-string v1, "com.android.systemui"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_0

    const-string v1, "com.android.keyguard"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_1

    .line 796
    :cond_0
    const/4 v1, 0x0

    return v1

    .line 798
    :cond_1
    new-instance v10, Lcom/miui/server/xspace/XSpaceManagerServiceImpl$2;

    move-object v1, v10

    move-object v2, p0

    move-object v3, p2

    move v4, p4

    move v5, p3

    move-object v6, p1

    move-object/from16 v7, p5

    move/from16 v8, p6

    invoke-direct/range {v1 .. v8}, Lcom/miui/server/xspace/XSpaceManagerServiceImpl$2;-><init>(Lcom/miui/server/xspace/XSpaceManagerServiceImpl;Landroid/content/Context;IILcom/android/server/am/ActivityManagerService;Landroid/os/Handler;I)V

    move-object/from16 v1, p5

    invoke-virtual {v1, v10}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    .line 821
    sget-object v2, Lcom/miui/server/xspace/XSpaceManagerServiceImpl;->mUserSwitchObserver:Landroid/app/UserSwitchObserver;

    const-string v3, "XSpaceManagerServiceImpl"

    move-object v4, p1

    invoke-virtual {p1, v2, v3}, Lcom/android/server/am/ActivityManagerService;->registerUserSwitchObserver(Landroid/app/IUserSwitchObserver;Ljava/lang/String;)V

    .line 822
    const/4 v2, 0x1

    return v2
.end method
