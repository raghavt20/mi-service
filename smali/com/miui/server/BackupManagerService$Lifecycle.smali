.class public final Lcom/miui/server/BackupManagerService$Lifecycle;
.super Lcom/android/server/SystemService;
.source "BackupManagerService.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/miui/server/BackupManagerService;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "Lifecycle"
.end annotation


# instance fields
.field private final mService:Lcom/miui/server/BackupManagerService;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 1
    .param p1, "context"    # Landroid/content/Context;

    .line 149
    invoke-direct {p0, p1}, Lcom/android/server/SystemService;-><init>(Landroid/content/Context;)V

    .line 150
    new-instance v0, Lcom/miui/server/BackupManagerService;

    invoke-direct {v0, p1}, Lcom/miui/server/BackupManagerService;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Lcom/miui/server/BackupManagerService$Lifecycle;->mService:Lcom/miui/server/BackupManagerService;

    .line 151
    return-void
.end method


# virtual methods
.method public onBootPhase(I)V
    .locals 3
    .param p1, "phase"    # I

    .line 161
    const/16 v0, 0x226

    if-ne p1, v0, :cond_0

    .line 163
    :try_start_0
    iget-object v0, p0, Lcom/miui/server/BackupManagerService$Lifecycle;->mService:Lcom/miui/server/BackupManagerService;

    invoke-static {}, Lcom/miui/server/BackupManagerService;->-$$Nest$smgetPackageEnableStateFile()Ljava/io/File;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/miui/server/BackupManagerService;->-$$Nest$mrestoreLastPackageEnableState(Lcom/miui/server/BackupManagerService;Ljava/io/File;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 166
    goto :goto_0

    .line 164
    :catch_0
    move-exception v0

    .line 165
    .local v0, "e":Ljava/lang/Exception;
    const-string v1, "Backup:BackupManagerService"

    const-string v2, "restoreLastPackageEnableState"

    invoke-static {v1, v2, v0}, Landroid/util/Slog;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    .line 168
    .end local v0    # "e":Ljava/lang/Exception;
    :cond_0
    :goto_0
    return-void
.end method

.method public onStart()V
    .locals 2

    .line 155
    const-string v0, "Backup:BackupManagerService"

    const-string v1, "init, onStart"

    invoke-static {v0, v1}, Landroid/util/Slog;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 156
    const-string v0, "MiuiBackup"

    iget-object v1, p0, Lcom/miui/server/BackupManagerService$Lifecycle;->mService:Lcom/miui/server/BackupManagerService;

    invoke-virtual {p0, v0, v1}, Lcom/miui/server/BackupManagerService$Lifecycle;->publishBinderService(Ljava/lang/String;Landroid/os/IBinder;)V

    .line 157
    return-void
.end method
