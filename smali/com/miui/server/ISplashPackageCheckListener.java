public abstract class com.miui.server.ISplashPackageCheckListener implements android.os.IInterface {
	 /* .source "ISplashPackageCheckListener.java" */
	 /* # interfaces */
	 /* # annotations */
	 /* .annotation system Ldalvik/annotation/MemberClasses; */
	 /* value = { */
	 /* Lcom/miui/server/ISplashPackageCheckListener$Stub; */
	 /* } */
} // .end annotation
/* # virtual methods */
public abstract void updateSplashPackageCheckInfo ( com.miui.server.SplashPackageCheckInfo p0 ) {
	 /* .annotation system Ldalvik/annotation/Throws; */
	 /* value = { */
	 /* Landroid/os/RemoteException; */
	 /* } */
} // .end annotation
} // .end method
public abstract void updateSplashPackageCheckInfoList ( java.util.List p0 ) {
/* .annotation system Ldalvik/annotation/Signature; */
/* value = { */
/* "(", */
/* "Ljava/util/List<", */
/* "Lcom/miui/server/SplashPackageCheckInfo;", */
/* ">;)V" */
/* } */
} // .end annotation
/* .annotation system Ldalvik/annotation/Throws; */
/* value = { */
/* Landroid/os/RemoteException; */
/* } */
} // .end annotation
} // .end method
