.class Lcom/miui/server/sptm/SpeedTestModeState;
.super Ljava/lang/Object;
.source "SpeedTestModeState.java"


# static fields
.field public static final FAST_SWITCH_OPS:I = 0x3

.field public static final QUICK_SWITCH_OPS:I = 0x2

.field public static final SLOW_SWITCH_OPS:I = 0x1

.field public static final SPTM_ENABLE_APP_LIST:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private static final SPTM_ENABLE_APP_LIST_NEW:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private static final SPTM_ENABLE_APP_LIST_OLD:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field private mFastSwitchCount:I

.field private mQuickSwitchCount:I

.field private mSpecifyAppCount:I

.field private mSpeedTestModeController:Lcom/miui/server/sptm/SpeedTestModeController;

.field private mSpeedTestModeService:Lcom/miui/server/sptm/SpeedTestModeServiceImpl;


# direct methods
.method static constructor <clinit>()V
    .locals 4

    .line 22
    new-instance v0, Ljava/util/LinkedList;

    invoke-direct {v0}, Ljava/util/LinkedList;-><init>()V

    sput-object v0, Lcom/miui/server/sptm/SpeedTestModeState;->SPTM_ENABLE_APP_LIST_OLD:Ljava/util/List;

    .line 24
    const-string v1, "com.tencent.mobileqq"

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 25
    const-string v1, "com.tencent.mm"

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 26
    const-string v1, "com.sina.weibo"

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 27
    const-string v1, "com.taobao.taobao"

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 30
    new-instance v1, Ljava/util/LinkedList;

    invoke-direct {v1}, Ljava/util/LinkedList;-><init>()V

    sput-object v1, Lcom/miui/server/sptm/SpeedTestModeState;->SPTM_ENABLE_APP_LIST_NEW:Ljava/util/List;

    .line 32
    const-string v2, "com.ss.android.ugc.aweme"

    invoke-interface {v1, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 33
    const-string v2, "com.quark.browser"

    invoke-interface {v1, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 34
    const-string v2, "com.xingin.xhs"

    invoke-interface {v1, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 35
    const-string v2, "com.dragon.read"

    invoke-interface {v1, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 37
    const-string v2, "persist.sys.miui_sptm_new.enable"

    const/4 v3, 0x0

    invoke-static {v2, v3}, Landroid/os/SystemProperties;->getBoolean(Ljava/lang/String;Z)Z

    move-result v2

    if-eqz v2, :cond_0

    .line 39
    move-object v0, v1

    goto :goto_0

    :cond_0
    nop

    :goto_0
    sput-object v0, Lcom/miui/server/sptm/SpeedTestModeState;->SPTM_ENABLE_APP_LIST:Ljava/util/List;

    .line 37
    return-void
.end method

.method public constructor <init>(Lcom/miui/server/sptm/SpeedTestModeController;)V
    .locals 1
    .param p1, "controller"    # Lcom/miui/server/sptm/SpeedTestModeController;

    .line 41
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 13
    const/4 v0, 0x0

    iput v0, p0, Lcom/miui/server/sptm/SpeedTestModeState;->mFastSwitchCount:I

    .line 14
    iput v0, p0, Lcom/miui/server/sptm/SpeedTestModeState;->mQuickSwitchCount:I

    .line 15
    iput v0, p0, Lcom/miui/server/sptm/SpeedTestModeState;->mSpecifyAppCount:I

    .line 17
    nop

    .line 18
    invoke-static {}, Lcom/miui/server/sptm/SpeedTestModeServiceImpl;->getInstance()Lcom/miui/server/sptm/SpeedTestModeServiceImpl;

    move-result-object v0

    iput-object v0, p0, Lcom/miui/server/sptm/SpeedTestModeState;->mSpeedTestModeService:Lcom/miui/server/sptm/SpeedTestModeServiceImpl;

    .line 42
    iput-object p1, p0, Lcom/miui/server/sptm/SpeedTestModeState;->mSpeedTestModeController:Lcom/miui/server/sptm/SpeedTestModeController;

    .line 43
    return-void
.end method

.method private reset()V
    .locals 1

    .line 72
    const/4 v0, 0x0

    iput v0, p0, Lcom/miui/server/sptm/SpeedTestModeState;->mFastSwitchCount:I

    .line 73
    iput v0, p0, Lcom/miui/server/sptm/SpeedTestModeState;->mQuickSwitchCount:I

    .line 74
    iput v0, p0, Lcom/miui/server/sptm/SpeedTestModeState;->mSpecifyAppCount:I

    .line 75
    return-void
.end method

.method private setMode(Z)V
    .locals 1
    .param p1, "isSPTMode"    # Z

    .line 78
    iget-object v0, p0, Lcom/miui/server/sptm/SpeedTestModeState;->mSpeedTestModeController:Lcom/miui/server/sptm/SpeedTestModeController;

    invoke-virtual {v0, p1}, Lcom/miui/server/sptm/SpeedTestModeController;->setSpeedTestMode(Z)V

    .line 79
    return-void
.end method


# virtual methods
.method public addAppSwitchOps(I)V
    .locals 2
    .param p1, "ops"    # I

    .line 46
    const/4 v0, 0x3

    const/4 v1, 0x1

    if-ne p1, v0, :cond_0

    .line 47
    iget v0, p0, Lcom/miui/server/sptm/SpeedTestModeState;->mFastSwitchCount:I

    add-int/2addr v0, v1

    iput v0, p0, Lcom/miui/server/sptm/SpeedTestModeState;->mFastSwitchCount:I

    goto :goto_0

    .line 48
    :cond_0
    const/4 v0, 0x2

    if-ne p1, v0, :cond_1

    .line 49
    iget v0, p0, Lcom/miui/server/sptm/SpeedTestModeState;->mQuickSwitchCount:I

    add-int/2addr v0, v1

    iput v0, p0, Lcom/miui/server/sptm/SpeedTestModeState;->mQuickSwitchCount:I

    goto :goto_0

    .line 50
    :cond_1
    if-ne p1, v1, :cond_2

    .line 51
    const/4 v0, 0x0

    invoke-direct {p0, v0}, Lcom/miui/server/sptm/SpeedTestModeState;->setMode(Z)V

    .line 52
    invoke-direct {p0}, Lcom/miui/server/sptm/SpeedTestModeState;->reset()V

    .line 54
    :cond_2
    :goto_0
    return-void
.end method

.method public addAppSwitchOps(Ljava/lang/String;Z)V
    .locals 3
    .param p1, "pkgName"    # Ljava/lang/String;
    .param p2, "isColdStart"    # Z

    .line 58
    iget-object v0, p0, Lcom/miui/server/sptm/SpeedTestModeState;->mSpeedTestModeService:Lcom/miui/server/sptm/SpeedTestModeServiceImpl;

    invoke-virtual {v0}, Lcom/miui/server/sptm/SpeedTestModeServiceImpl;->getSPTMCloudEnable()Z

    move-result v0

    if-nez v0, :cond_0

    .line 59
    return-void

    .line 62
    :cond_0
    sget-object v0, Lcom/miui/server/sptm/SpeedTestModeState;->SPTM_ENABLE_APP_LIST:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v1

    .line 64
    .local v1, "appSize":I
    if-eqz p2, :cond_1

    iget v2, p0, Lcom/miui/server/sptm/SpeedTestModeState;->mSpecifyAppCount:I

    if-ge v2, v1, :cond_1

    .line 65
    invoke-interface {v0, v2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    invoke-virtual {p1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    iget v0, p0, Lcom/miui/server/sptm/SpeedTestModeState;->mSpecifyAppCount:I

    const/4 v2, 0x1

    add-int/2addr v0, v2

    iput v0, p0, Lcom/miui/server/sptm/SpeedTestModeState;->mSpecifyAppCount:I

    if-ne v0, v1, :cond_1

    .line 67
    invoke-direct {p0, v2}, Lcom/miui/server/sptm/SpeedTestModeState;->setMode(Z)V

    .line 69
    :cond_1
    return-void
.end method
