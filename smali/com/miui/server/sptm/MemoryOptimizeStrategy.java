class com.miui.server.sptm.MemoryOptimizeStrategy implements com.miui.server.sptm.SpeedTestModeServiceImpl$Strategy {
	 /* .source "MemoryOptimizeStrategy.java" */
	 /* # interfaces */
	 /* # static fields */
	 private static final Integer MEMORY_RECLAIM_APP_COUNT;
	 private static final java.lang.String SPTM_OOM_MIN_FREE;
	 /* # instance fields */
	 private SPT_MODE_OOM_MIN_FREE;
	 private com.android.server.am.ActivityManagerService mAms;
	 private Integer mAppCounts;
	 private Boolean mIsProcCompactEnable;
	 private java.lang.reflect.Method mMethodSetEnabledNativeSPTMode;
	 private java.lang.reflect.Method mMethodUpdateOomForSPTM;
	 private java.lang.reflect.Method mMethodUpdateOomMinFree;
	 private com.android.server.am.MiuiMemoryServiceInternal mMiuiMemoryService;
	 private com.miui.server.sptm.SpeedTestModeServiceImpl mSpeedTestModeService;
	 /* # direct methods */
	 static com.miui.server.sptm.MemoryOptimizeStrategy ( ) {
		 /* .locals 2 */
		 /* .line 25 */
		 final String v0 = "persist.sys.miui_sptm.min_free"; // const-string v0, "persist.sys.miui_sptm.min_free"
		 final String v1 = "73728,92160,110592,240000,340000,450000"; // const-string v1, "73728,92160,110592,240000,340000,450000"
		 android.os.SystemProperties .get ( v0,v1 );
		 /* .line 27 */
		 final String v0 = "persist.sys.miui_sptm.start_mem_reclaim_app_count"; // const-string v0, "persist.sys.miui_sptm.start_mem_reclaim_app_count"
		 /* const/16 v1, 0xc */
		 v0 = 		 android.os.SystemProperties .getInt ( v0,v1 );
		 return;
	 } // .end method
	 public com.miui.server.sptm.MemoryOptimizeStrategy ( ) {
		 /* .locals 3 */
		 /* .line 41 */
		 /* invoke-direct {p0}, Ljava/lang/Object;-><init>()V */
		 /* .line 22 */
		 /* nop */
		 /* .line 23 */
		 com.miui.server.sptm.SpeedTestModeServiceImpl .getInstance ( );
		 this.mSpeedTestModeService = v0;
		 /* .line 30 */
		 int v0 = 0; // const/4 v0, 0x0
		 this.SPT_MODE_OOM_MIN_FREE = v0;
		 /* .line 31 */
		 int v0 = 0; // const/4 v0, 0x0
		 /* iput v0, p0, Lcom/miui/server/sptm/MemoryOptimizeStrategy;->mAppCounts:I */
		 /* .line 32 */
		 /* iput-boolean v0, p0, Lcom/miui/server/sptm/MemoryOptimizeStrategy;->mIsProcCompactEnable:Z */
		 /* .line 42 */
		 final String v0 = "activity"; // const-string v0, "activity"
		 android.os.ServiceManager .getService ( v0 );
		 /* check-cast v0, Lcom/android/server/am/ActivityManagerService; */
		 this.mAms = v0;
		 /* .line 43 */
		 v0 = com.miui.server.sptm.MemoryOptimizeStrategy.SPTM_OOM_MIN_FREE;
		 com.miui.server.sptm.MemoryOptimizeStrategy .loadProperty ( v0 );
		 this.SPT_MODE_OOM_MIN_FREE = v0;
		 /* .line 45 */
		 v0 = this.mAms;
		 if ( v0 != null) { // if-eqz v0, :cond_0
			 /* .line 48 */
			 /* nop */
			 /* .line 49 */
			 (( java.lang.Object ) v0 ).getClass ( ); // invoke-virtual {v0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;
			 /* const-class v1, [I */
			 /* filled-new-array {v1}, [Ljava/lang/Class; */
			 /* .line 48 */
			 /* const-string/jumbo v2, "updateOomMinFree" */
			 miui.util.ReflectionUtils .tryFindMethodExact ( v0,v2,v1 );
			 this.mMethodUpdateOomMinFree = v0;
			 /* .line 50 */
			 v0 = this.mAms;
			 /* .line 51 */
			 (( java.lang.Object ) v0 ).getClass ( ); // invoke-virtual {v0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;
			 v1 = java.lang.Boolean.TYPE;
			 /* filled-new-array {v1}, [Ljava/lang/Class; */
			 /* .line 50 */
			 /* const-string/jumbo v2, "updateOomForSPTM" */
			 miui.util.ReflectionUtils .tryFindMethodExact ( v0,v2,v1 );
			 this.mMethodUpdateOomForSPTM = v0;
			 /* .line 53 */
		 } // :cond_0
		 return;
	 } // .end method
	 private static loadProperty ( java.lang.String p0 ) {
		 /* .locals 5 */
		 /* .param p0, "oomMinfree" # Ljava/lang/String; */
		 /* .line 119 */
		 v0 = 		 android.text.TextUtils .isEmpty ( p0 );
		 int v1 = 0; // const/4 v1, 0x0
		 if ( v0 != null) { // if-eqz v0, :cond_0
			 /* .line 120 */
			 /* .line 123 */
		 } // :cond_0
		 final String v0 = ","; // const-string v0, ","
		 (( java.lang.String ) p0 ).split ( v0 ); // invoke-virtual {p0, v0}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;
		 /* .line 124 */
		 /* .local v0, "slice":[Ljava/lang/String; */
		 /* array-length v2, v0 */
		 int v3 = 6; // const/4 v3, 0x6
		 /* if-eq v2, v3, :cond_1 */
		 /* .line 125 */
		 /* .line 129 */
	 } // :cond_1
	 try { // :try_start_0
		 /* new-array v2, v3, [I */
		 /* .line 130 */
		 /* .local v2, "res":[I */
		 int v3 = 0; // const/4 v3, 0x0
		 /* .local v3, "i":I */
	 } // :goto_0
	 /* array-length v4, v0 */
	 /* if-ge v3, v4, :cond_2 */
	 /* .line 131 */
	 /* aget-object v4, v0, v3 */
	 v4 = 	 java.lang.Integer .parseInt ( v4 );
	 /* aput v4, v2, v3 */
	 /* :try_end_0 */
	 /* .catch Ljava/lang/Exception; {:try_start_0 ..:try_end_0} :catch_0 */
	 /* .line 130 */
	 /* add-int/lit8 v3, v3, 0x1 */
	 /* .line 133 */
} // .end local v3 # "i":I
} // :cond_2
/* .line 134 */
} // .end local v2 # "res":[I
/* :catch_0 */
/* move-exception v2 */
/* .line 135 */
/* .local v2, "e":Ljava/lang/Exception; */
(( java.lang.Exception ) v2 ).printStackTrace ( ); // invoke-virtual {v2}, Ljava/lang/Exception;->printStackTrace()V
/* .line 136 */
} // .end method
/* # virtual methods */
public void onAppStarted ( com.miui.server.sptm.PreLoadStrategy$AppStartRecord p0 ) {
/* .locals 2 */
/* .param p1, "r" # Lcom/miui/server/sptm/PreLoadStrategy$AppStartRecord; */
/* .line 62 */
v0 = com.miui.server.sptm.SpeedTestModeServiceImpl.SPEED_TEST_APP_LIST;
v0 = v1 = this.packageName;
if ( v0 != null) { // if-eqz v0, :cond_0
/* .line 63 */
v0 = this.mMiuiMemoryService;
/* if-nez v0, :cond_0 */
/* .line 64 */
/* const-class v0, Lcom/android/server/am/MiuiMemoryServiceInternal; */
/* .line 65 */
com.android.server.LocalServices .getService ( v0 );
/* check-cast v0, Lcom/android/server/am/MiuiMemoryServiceInternal; */
this.mMiuiMemoryService = v0;
/* .line 79 */
} // :cond_0
return;
} // .end method
public void onNewEvent ( Integer p0 ) {
/* .locals 0 */
/* .param p1, "eventType" # I */
/* .line 58 */
return;
} // .end method
public void onSpeedTestModeChanged ( Boolean p0 ) {
/* .locals 6 */
/* .param p1, "isEnable" # Z */
/* .line 83 */
v0 = this.SPT_MODE_OOM_MIN_FREE;
if ( v0 != null) { // if-eqz v0, :cond_7
v0 = this.mAms;
/* if-nez v0, :cond_0 */
/* .line 87 */
} // :cond_0
int v0 = 0; // const/4 v0, 0x0
/* if-nez p1, :cond_1 */
/* .line 88 */
/* iput v0, p0, Lcom/miui/server/sptm/MemoryOptimizeStrategy;->mAppCounts:I */
/* .line 89 */
/* iput-boolean v0, p0, Lcom/miui/server/sptm/MemoryOptimizeStrategy;->mIsProcCompactEnable:Z */
/* .line 92 */
} // :cond_1
v1 = com.miui.server.sptm.SpeedTestModeServiceImpl .isLowMemDeviceForSpeedTestMode ( );
/* if-nez v1, :cond_2 */
/* .line 93 */
return;
/* .line 96 */
} // :cond_2
v1 = this.mMethodSetEnabledNativeSPTMode;
int v2 = 1; // const/4 v2, 0x1
if ( v1 != null) { // if-eqz v1, :cond_3
/* .line 98 */
try { // :try_start_0
v3 = this.mAms;
/* new-array v4, v2, [Ljava/lang/Object; */
java.lang.Boolean .valueOf ( p1 );
/* aput-object v5, v4, v0 */
(( java.lang.reflect.Method ) v1 ).invoke ( v3, v4 ); // invoke-virtual {v1, v3, v4}, Ljava/lang/reflect/Method;->invoke(Ljava/lang/Object;[Ljava/lang/Object;)Ljava/lang/Object;
/* :try_end_0 */
/* .catch Ljava/lang/Exception; {:try_start_0 ..:try_end_0} :catch_0 */
/* .line 100 */
/* .line 99 */
/* :catch_0 */
/* move-exception v1 */
/* .line 103 */
} // :cond_3
} // :goto_0
v1 = this.mMethodUpdateOomForSPTM;
if ( v1 != null) { // if-eqz v1, :cond_4
/* .line 105 */
try { // :try_start_1
v3 = this.mAms;
/* new-array v4, v2, [Ljava/lang/Object; */
java.lang.Boolean .valueOf ( p1 );
/* aput-object v5, v4, v0 */
(( java.lang.reflect.Method ) v1 ).invoke ( v3, v4 ); // invoke-virtual {v1, v3, v4}, Ljava/lang/reflect/Method;->invoke(Ljava/lang/Object;[Ljava/lang/Object;)Ljava/lang/Object;
/* :try_end_1 */
/* .catch Ljava/lang/Exception; {:try_start_1 ..:try_end_1} :catch_1 */
/* .line 107 */
/* .line 106 */
/* :catch_1 */
/* move-exception v1 */
/* .line 110 */
} // :cond_4
} // :goto_1
v1 = this.mMethodUpdateOomMinFree;
if ( v1 != null) { // if-eqz v1, :cond_6
/* .line 112 */
try { // :try_start_2
v3 = this.mAms;
/* new-array v2, v2, [Ljava/lang/Object; */
if ( p1 != null) { // if-eqz p1, :cond_5
v4 = this.SPT_MODE_OOM_MIN_FREE;
} // :cond_5
int v4 = 0; // const/4 v4, 0x0
} // :goto_2
/* aput-object v4, v2, v0 */
(( java.lang.reflect.Method ) v1 ).invoke ( v3, v2 ); // invoke-virtual {v1, v3, v2}, Ljava/lang/reflect/Method;->invoke(Ljava/lang/Object;[Ljava/lang/Object;)Ljava/lang/Object;
/* :try_end_2 */
/* .catch Ljava/lang/Exception; {:try_start_2 ..:try_end_2} :catch_2 */
/* .line 114 */
/* .line 113 */
/* :catch_2 */
/* move-exception v0 */
/* .line 116 */
} // :cond_6
} // :goto_3
return;
/* .line 84 */
} // :cond_7
} // :goto_4
return;
} // .end method
