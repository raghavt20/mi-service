class com.miui.server.sptm.SpeedTestModeServiceImpl$Handler extends android.os.Handler {
	 /* .source "SpeedTestModeServiceImpl.java" */
	 /* # annotations */
	 /* .annotation system Ldalvik/annotation/EnclosingClass; */
	 /* value = Lcom/miui/server/sptm/SpeedTestModeServiceImpl; */
} // .end annotation
/* .annotation system Ldalvik/annotation/InnerClass; */
/* accessFlags = 0x2 */
/* name = "Handler" */
} // .end annotation
/* # static fields */
public static final Integer MSG_DISABLE_SPTM;
public static final Integer MSG_ENABLE_SPTM;
public static final Integer MSG_EVENT;
public static final Integer MSG_EXIT_MODE;
/* # instance fields */
final com.miui.server.sptm.SpeedTestModeServiceImpl this$0; //synthetic
/* # direct methods */
public com.miui.server.sptm.SpeedTestModeServiceImpl$Handler ( ) {
/* .locals 0 */
/* .param p2, "looper" # Landroid/os/Looper; */
/* .line 222 */
this.this$0 = p1;
/* .line 223 */
/* invoke-direct {p0, p2}, Landroid/os/Handler;-><init>(Landroid/os/Looper;)V */
/* .line 224 */
return;
} // .end method
/* # virtual methods */
public void handleMessage ( android.os.Message p0 ) {
/* .locals 6 */
/* .param p1, "msg" # Landroid/os/Message; */
/* .line 228 */
/* invoke-super {p0, p1}, Landroid/os/Handler;->handleMessage(Landroid/os/Message;)V */
/* .line 229 */
v0 = this.this$0;
v0 = com.miui.server.sptm.SpeedTestModeServiceImpl .-$$Nest$fgetmIsSpeedTestEnabled ( v0 );
/* if-nez v0, :cond_0 */
/* .line 230 */
return;
/* .line 233 */
} // :cond_0
/* iget v0, p1, Landroid/os/Message;->what:I */
int v1 = 1; // const/4 v1, 0x1
/* if-ne v0, v1, :cond_2 */
/* .line 234 */
(( android.os.Message ) p1 ).getData ( ); // invoke-virtual {p1}, Landroid/os/Message;->getData()Landroid/os/Bundle;
/* .line 235 */
/* .local v0, "bundle":Landroid/os/Bundle; */
if ( v0 != null) { // if-eqz v0, :cond_1
/* .line 236 */
final String v1 = "event"; // const-string v1, "event"
v1 = (( android.os.Bundle ) v0 ).getInt ( v1 ); // invoke-virtual {v0, v1}, Landroid/os/Bundle;->getInt(Ljava/lang/String;)I
/* .line 237 */
/* .local v1, "event":I */
final String v2 = "packageName"; // const-string v2, "packageName"
(( android.os.Bundle ) v0 ).getString ( v2 ); // invoke-virtual {v0, v2}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;
/* .line 238 */
/* .local v2, "packageName":Ljava/lang/String; */
/* const-string/jumbo v3, "time" */
(( android.os.Bundle ) v0 ).getLong ( v3 ); // invoke-virtual {v0, v3}, Landroid/os/Bundle;->getLong(Ljava/lang/String;)J
/* move-result-wide v3 */
/* .line 239 */
/* .local v3, "time":J */
v5 = this.this$0;
com.miui.server.sptm.SpeedTestModeServiceImpl .-$$Nest$mhandleEvent ( v5,v1,v2,v3,v4 );
/* .line 241 */
} // .end local v0 # "bundle":Landroid/os/Bundle;
} // .end local v1 # "event":I
} // .end local v2 # "packageName":Ljava/lang/String;
} // .end local v3 # "time":J
} // :cond_1
} // :cond_2
/* iget v0, p1, Landroid/os/Message;->what:I */
int v2 = 2; // const/4 v2, 0x2
/* if-ne v0, v2, :cond_3 */
/* .line 242 */
v0 = this.this$0;
com.miui.server.sptm.SpeedTestModeServiceImpl .-$$Nest$mhandleUpdateSpeedTestMode ( v0,v1 );
/* .line 243 */
} // :cond_3
/* iget v0, p1, Landroid/os/Message;->what:I */
int v2 = 3; // const/4 v2, 0x3
/* if-ne v0, v2, :cond_4 */
/* .line 244 */
v0 = this.this$0;
int v1 = 0; // const/4 v1, 0x0
com.miui.server.sptm.SpeedTestModeServiceImpl .-$$Nest$mhandleUpdateSpeedTestMode ( v0,v1 );
/* .line 245 */
} // :cond_4
/* iget v0, p1, Landroid/os/Message;->what:I */
int v2 = 4; // const/4 v2, 0x4
/* if-ne v0, v2, :cond_5 */
/* .line 246 */
v0 = this.this$0;
com.miui.server.sptm.SpeedTestModeServiceImpl .-$$Nest$fgetmSpeedTestModeState ( v0 );
(( com.miui.server.sptm.SpeedTestModeState ) v0 ).addAppSwitchOps ( v1 ); // invoke-virtual {v0, v1}, Lcom/miui/server/sptm/SpeedTestModeState;->addAppSwitchOps(I)V
/* .line 247 */
v0 = this.this$0;
com.miui.server.sptm.SpeedTestModeServiceImpl .-$$Nest$fgetmPreLoadStrategy ( v0 );
(( com.miui.server.sptm.PreLoadStrategy ) v0 ).reset ( ); // invoke-virtual {v0}, Lcom/miui/server/sptm/PreLoadStrategy;->reset()V
/* .line 249 */
} // :cond_5
} // :goto_0
return;
} // .end method
