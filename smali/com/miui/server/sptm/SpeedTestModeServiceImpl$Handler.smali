.class Lcom/miui/server/sptm/SpeedTestModeServiceImpl$Handler;
.super Landroid/os/Handler;
.source "SpeedTestModeServiceImpl.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/miui/server/sptm/SpeedTestModeServiceImpl;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "Handler"
.end annotation


# static fields
.field public static final MSG_DISABLE_SPTM:I = 0x3

.field public static final MSG_ENABLE_SPTM:I = 0x2

.field public static final MSG_EVENT:I = 0x1

.field public static final MSG_EXIT_MODE:I = 0x4


# instance fields
.field final synthetic this$0:Lcom/miui/server/sptm/SpeedTestModeServiceImpl;


# direct methods
.method public constructor <init>(Lcom/miui/server/sptm/SpeedTestModeServiceImpl;Landroid/os/Looper;)V
    .locals 0
    .param p2, "looper"    # Landroid/os/Looper;

    .line 222
    iput-object p1, p0, Lcom/miui/server/sptm/SpeedTestModeServiceImpl$Handler;->this$0:Lcom/miui/server/sptm/SpeedTestModeServiceImpl;

    .line 223
    invoke-direct {p0, p2}, Landroid/os/Handler;-><init>(Landroid/os/Looper;)V

    .line 224
    return-void
.end method


# virtual methods
.method public handleMessage(Landroid/os/Message;)V
    .locals 6
    .param p1, "msg"    # Landroid/os/Message;

    .line 228
    invoke-super {p0, p1}, Landroid/os/Handler;->handleMessage(Landroid/os/Message;)V

    .line 229
    iget-object v0, p0, Lcom/miui/server/sptm/SpeedTestModeServiceImpl$Handler;->this$0:Lcom/miui/server/sptm/SpeedTestModeServiceImpl;

    invoke-static {v0}, Lcom/miui/server/sptm/SpeedTestModeServiceImpl;->-$$Nest$fgetmIsSpeedTestEnabled(Lcom/miui/server/sptm/SpeedTestModeServiceImpl;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 230
    return-void

    .line 233
    :cond_0
    iget v0, p1, Landroid/os/Message;->what:I

    const/4 v1, 0x1

    if-ne v0, v1, :cond_2

    .line 234
    invoke-virtual {p1}, Landroid/os/Message;->getData()Landroid/os/Bundle;

    move-result-object v0

    .line 235
    .local v0, "bundle":Landroid/os/Bundle;
    if-eqz v0, :cond_1

    .line 236
    const-string v1, "event"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getInt(Ljava/lang/String;)I

    move-result v1

    .line 237
    .local v1, "event":I
    const-string v2, "packageName"

    invoke-virtual {v0, v2}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    .line 238
    .local v2, "packageName":Ljava/lang/String;
    const-string/jumbo v3, "time"

    invoke-virtual {v0, v3}, Landroid/os/Bundle;->getLong(Ljava/lang/String;)J

    move-result-wide v3

    .line 239
    .local v3, "time":J
    iget-object v5, p0, Lcom/miui/server/sptm/SpeedTestModeServiceImpl$Handler;->this$0:Lcom/miui/server/sptm/SpeedTestModeServiceImpl;

    invoke-static {v5, v1, v2, v3, v4}, Lcom/miui/server/sptm/SpeedTestModeServiceImpl;->-$$Nest$mhandleEvent(Lcom/miui/server/sptm/SpeedTestModeServiceImpl;ILjava/lang/String;J)V

    .line 241
    .end local v0    # "bundle":Landroid/os/Bundle;
    .end local v1    # "event":I
    .end local v2    # "packageName":Ljava/lang/String;
    .end local v3    # "time":J
    :cond_1
    goto :goto_0

    :cond_2
    iget v0, p1, Landroid/os/Message;->what:I

    const/4 v2, 0x2

    if-ne v0, v2, :cond_3

    .line 242
    iget-object v0, p0, Lcom/miui/server/sptm/SpeedTestModeServiceImpl$Handler;->this$0:Lcom/miui/server/sptm/SpeedTestModeServiceImpl;

    invoke-static {v0, v1}, Lcom/miui/server/sptm/SpeedTestModeServiceImpl;->-$$Nest$mhandleUpdateSpeedTestMode(Lcom/miui/server/sptm/SpeedTestModeServiceImpl;Z)V

    goto :goto_0

    .line 243
    :cond_3
    iget v0, p1, Landroid/os/Message;->what:I

    const/4 v2, 0x3

    if-ne v0, v2, :cond_4

    .line 244
    iget-object v0, p0, Lcom/miui/server/sptm/SpeedTestModeServiceImpl$Handler;->this$0:Lcom/miui/server/sptm/SpeedTestModeServiceImpl;

    const/4 v1, 0x0

    invoke-static {v0, v1}, Lcom/miui/server/sptm/SpeedTestModeServiceImpl;->-$$Nest$mhandleUpdateSpeedTestMode(Lcom/miui/server/sptm/SpeedTestModeServiceImpl;Z)V

    goto :goto_0

    .line 245
    :cond_4
    iget v0, p1, Landroid/os/Message;->what:I

    const/4 v2, 0x4

    if-ne v0, v2, :cond_5

    .line 246
    iget-object v0, p0, Lcom/miui/server/sptm/SpeedTestModeServiceImpl$Handler;->this$0:Lcom/miui/server/sptm/SpeedTestModeServiceImpl;

    invoke-static {v0}, Lcom/miui/server/sptm/SpeedTestModeServiceImpl;->-$$Nest$fgetmSpeedTestModeState(Lcom/miui/server/sptm/SpeedTestModeServiceImpl;)Lcom/miui/server/sptm/SpeedTestModeState;

    move-result-object v0

    invoke-virtual {v0, v1}, Lcom/miui/server/sptm/SpeedTestModeState;->addAppSwitchOps(I)V

    .line 247
    iget-object v0, p0, Lcom/miui/server/sptm/SpeedTestModeServiceImpl$Handler;->this$0:Lcom/miui/server/sptm/SpeedTestModeServiceImpl;

    invoke-static {v0}, Lcom/miui/server/sptm/SpeedTestModeServiceImpl;->-$$Nest$fgetmPreLoadStrategy(Lcom/miui/server/sptm/SpeedTestModeServiceImpl;)Lcom/miui/server/sptm/PreLoadStrategy;

    move-result-object v0

    invoke-virtual {v0}, Lcom/miui/server/sptm/PreLoadStrategy;->reset()V

    .line 249
    :cond_5
    :goto_0
    return-void
.end method
