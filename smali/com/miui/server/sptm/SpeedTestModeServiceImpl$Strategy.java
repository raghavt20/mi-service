public abstract class com.miui.server.sptm.SpeedTestModeServiceImpl$Strategy {
	 /* .source "SpeedTestModeServiceImpl.java" */
	 /* # annotations */
	 /* .annotation system Ldalvik/annotation/EnclosingClass; */
	 /* value = Lcom/miui/server/sptm/SpeedTestModeServiceImpl; */
} // .end annotation
/* .annotation system Ldalvik/annotation/InnerClass; */
/* accessFlags = 0x609 */
/* name = "Strategy" */
} // .end annotation
/* # virtual methods */
public abstract void onAppStarted ( com.miui.server.sptm.PreLoadStrategy$AppStartRecord p0 ) {
} // .end method
public abstract void onNewEvent ( Integer p0 ) {
} // .end method
public abstract void onSpeedTestModeChanged ( Boolean p0 ) {
} // .end method
