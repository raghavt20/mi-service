.class public final Lcom/miui/server/sptm/SpeedTestModeServiceImpl$Provider;
.super Ljava/lang/Object;
.source "SpeedTestModeServiceImpl$Provider.java"

# interfaces
.implements Lcom/miui/base/MiuiStubRegistry$ImplProvider;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/miui/server/sptm/SpeedTestModeServiceImpl$Provider$SINGLETON;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Lcom/miui/base/MiuiStubRegistry$ImplProvider<",
        "Lcom/miui/server/sptm/SpeedTestModeServiceImpl;",
        ">;"
    }
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 0

    .line 11
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public provideNewInstance()Lcom/miui/server/sptm/SpeedTestModeServiceImpl;
    .locals 1

    .line 17
    new-instance v0, Lcom/miui/server/sptm/SpeedTestModeServiceImpl;

    invoke-direct {v0}, Lcom/miui/server/sptm/SpeedTestModeServiceImpl;-><init>()V

    return-object v0
.end method

.method public bridge synthetic provideNewInstance()Ljava/lang/Object;
    .locals 1

    .line 10
    invoke-virtual {p0}, Lcom/miui/server/sptm/SpeedTestModeServiceImpl$Provider;->provideNewInstance()Lcom/miui/server/sptm/SpeedTestModeServiceImpl;

    move-result-object v0

    return-object v0
.end method

.method public provideSingleton()Lcom/miui/server/sptm/SpeedTestModeServiceImpl;
    .locals 1

    .line 13
    sget-object v0, Lcom/miui/server/sptm/SpeedTestModeServiceImpl$Provider$SINGLETON;->INSTANCE:Lcom/miui/server/sptm/SpeedTestModeServiceImpl;

    return-object v0
.end method

.method public bridge synthetic provideSingleton()Ljava/lang/Object;
    .locals 1

    .line 10
    invoke-virtual {p0}, Lcom/miui/server/sptm/SpeedTestModeServiceImpl$Provider;->provideSingleton()Lcom/miui/server/sptm/SpeedTestModeServiceImpl;

    move-result-object v0

    return-object v0
.end method
