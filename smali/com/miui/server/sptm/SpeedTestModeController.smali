.class public Lcom/miui/server/sptm/SpeedTestModeController;
.super Ljava/lang/Object;
.source "SpeedTestModeController.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/miui/server/sptm/SpeedTestModeController$OnSpeedTestModeChangeListener;
    }
.end annotation


# instance fields
.field private mIsInSpeedTestMode:Z

.field private mListener:Lcom/miui/server/sptm/SpeedTestModeController$OnSpeedTestModeChangeListener;

.field mSpeedTestModeService:Lcom/miui/server/sptm/SpeedTestModeServiceImpl;


# direct methods
.method public constructor <init>(Lcom/miui/server/sptm/SpeedTestModeController$OnSpeedTestModeChangeListener;)V
    .locals 1
    .param p1, "listener"    # Lcom/miui/server/sptm/SpeedTestModeController$OnSpeedTestModeChangeListener;

    .line 12
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 7
    nop

    .line 8
    invoke-static {}, Lcom/miui/server/sptm/SpeedTestModeServiceImpl;->getInstance()Lcom/miui/server/sptm/SpeedTestModeServiceImpl;

    move-result-object v0

    iput-object v0, p0, Lcom/miui/server/sptm/SpeedTestModeController;->mSpeedTestModeService:Lcom/miui/server/sptm/SpeedTestModeServiceImpl;

    .line 9
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/miui/server/sptm/SpeedTestModeController;->mIsInSpeedTestMode:Z

    .line 13
    iput-object p1, p0, Lcom/miui/server/sptm/SpeedTestModeController;->mListener:Lcom/miui/server/sptm/SpeedTestModeController$OnSpeedTestModeChangeListener;

    .line 14
    return-void
.end method


# virtual methods
.method public isInSpeedTestMode()Z
    .locals 1

    .line 30
    iget-boolean v0, p0, Lcom/miui/server/sptm/SpeedTestModeController;->mIsInSpeedTestMode:Z

    return v0
.end method

.method public setSpeedTestMode(Z)V
    .locals 2
    .param p1, "isEnable"    # Z

    .line 17
    iget-boolean v0, p0, Lcom/miui/server/sptm/SpeedTestModeController;->mIsInSpeedTestMode:Z

    if-ne v0, p1, :cond_0

    .line 18
    return-void

    .line 20
    :cond_0
    iget-object v0, p0, Lcom/miui/server/sptm/SpeedTestModeController;->mListener:Lcom/miui/server/sptm/SpeedTestModeController$OnSpeedTestModeChangeListener;

    if-eqz v0, :cond_1

    .line 21
    iput-boolean p1, p0, Lcom/miui/server/sptm/SpeedTestModeController;->mIsInSpeedTestMode:Z

    .line 22
    invoke-interface {v0, p1}, Lcom/miui/server/sptm/SpeedTestModeController$OnSpeedTestModeChangeListener;->onSpeedTestModeChange(Z)V

    .line 23
    sget-boolean v0, Lcom/miui/server/sptm/SpeedTestModeServiceImpl;->DEBUG:Z

    if-eqz v0, :cond_1

    .line 24
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "handleUpdateSpeedTestMode: enter spt mode = "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-boolean v1, p0, Lcom/miui/server/sptm/SpeedTestModeController;->mIsInSpeedTestMode:Z

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    const-string v1, "SPTM"

    invoke-static {v1, v0}, Landroid/util/Slog;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 27
    :cond_1
    return-void
.end method
