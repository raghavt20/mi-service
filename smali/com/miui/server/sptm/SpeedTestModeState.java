class com.miui.server.sptm.SpeedTestModeState {
	 /* .source "SpeedTestModeState.java" */
	 /* # static fields */
	 public static final Integer FAST_SWITCH_OPS;
	 public static final Integer QUICK_SWITCH_OPS;
	 public static final Integer SLOW_SWITCH_OPS;
	 public static final java.util.List SPTM_ENABLE_APP_LIST;
	 /* .annotation system Ldalvik/annotation/Signature; */
	 /* value = { */
	 /* "Ljava/util/List<", */
	 /* "Ljava/lang/String;", */
	 /* ">;" */
	 /* } */
} // .end annotation
} // .end field
private static final java.util.List SPTM_ENABLE_APP_LIST_NEW;
/* .annotation system Ldalvik/annotation/Signature; */
/* value = { */
/* "Ljava/util/List<", */
/* "Ljava/lang/String;", */
/* ">;" */
/* } */
} // .end annotation
} // .end field
private static final java.util.List SPTM_ENABLE_APP_LIST_OLD;
/* .annotation system Ldalvik/annotation/Signature; */
/* value = { */
/* "Ljava/util/List<", */
/* "Ljava/lang/String;", */
/* ">;" */
/* } */
} // .end annotation
} // .end field
/* # instance fields */
private Integer mFastSwitchCount;
private Integer mQuickSwitchCount;
private Integer mSpecifyAppCount;
private com.miui.server.sptm.SpeedTestModeController mSpeedTestModeController;
private com.miui.server.sptm.SpeedTestModeServiceImpl mSpeedTestModeService;
/* # direct methods */
static com.miui.server.sptm.SpeedTestModeState ( ) {
/* .locals 4 */
/* .line 22 */
/* new-instance v0, Ljava/util/LinkedList; */
/* invoke-direct {v0}, Ljava/util/LinkedList;-><init>()V */
/* .line 24 */
final String v1 = "com.tencent.mobileqq"; // const-string v1, "com.tencent.mobileqq"
/* .line 25 */
final String v1 = "com.tencent.mm"; // const-string v1, "com.tencent.mm"
/* .line 26 */
final String v1 = "com.sina.weibo"; // const-string v1, "com.sina.weibo"
/* .line 27 */
final String v1 = "com.taobao.taobao"; // const-string v1, "com.taobao.taobao"
/* .line 30 */
/* new-instance v1, Ljava/util/LinkedList; */
/* invoke-direct {v1}, Ljava/util/LinkedList;-><init>()V */
/* .line 32 */
final String v2 = "com.ss.android.ugc.aweme"; // const-string v2, "com.ss.android.ugc.aweme"
/* .line 33 */
final String v2 = "com.quark.browser"; // const-string v2, "com.quark.browser"
/* .line 34 */
final String v2 = "com.xingin.xhs"; // const-string v2, "com.xingin.xhs"
/* .line 35 */
final String v2 = "com.dragon.read"; // const-string v2, "com.dragon.read"
/* .line 37 */
final String v2 = "persist.sys.miui_sptm_new.enable"; // const-string v2, "persist.sys.miui_sptm_new.enable"
int v3 = 0; // const/4 v3, 0x0
v2 = android.os.SystemProperties .getBoolean ( v2,v3 );
if ( v2 != null) { // if-eqz v2, :cond_0
/* .line 39 */
/* move-object v0, v1 */
} // :cond_0
/* nop */
} // :goto_0
/* .line 37 */
return;
} // .end method
public com.miui.server.sptm.SpeedTestModeState ( ) {
/* .locals 1 */
/* .param p1, "controller" # Lcom/miui/server/sptm/SpeedTestModeController; */
/* .line 41 */
/* invoke-direct {p0}, Ljava/lang/Object;-><init>()V */
/* .line 13 */
int v0 = 0; // const/4 v0, 0x0
/* iput v0, p0, Lcom/miui/server/sptm/SpeedTestModeState;->mFastSwitchCount:I */
/* .line 14 */
/* iput v0, p0, Lcom/miui/server/sptm/SpeedTestModeState;->mQuickSwitchCount:I */
/* .line 15 */
/* iput v0, p0, Lcom/miui/server/sptm/SpeedTestModeState;->mSpecifyAppCount:I */
/* .line 17 */
/* nop */
/* .line 18 */
com.miui.server.sptm.SpeedTestModeServiceImpl .getInstance ( );
this.mSpeedTestModeService = v0;
/* .line 42 */
this.mSpeedTestModeController = p1;
/* .line 43 */
return;
} // .end method
private void reset ( ) {
/* .locals 1 */
/* .line 72 */
int v0 = 0; // const/4 v0, 0x0
/* iput v0, p0, Lcom/miui/server/sptm/SpeedTestModeState;->mFastSwitchCount:I */
/* .line 73 */
/* iput v0, p0, Lcom/miui/server/sptm/SpeedTestModeState;->mQuickSwitchCount:I */
/* .line 74 */
/* iput v0, p0, Lcom/miui/server/sptm/SpeedTestModeState;->mSpecifyAppCount:I */
/* .line 75 */
return;
} // .end method
private void setMode ( Boolean p0 ) {
/* .locals 1 */
/* .param p1, "isSPTMode" # Z */
/* .line 78 */
v0 = this.mSpeedTestModeController;
(( com.miui.server.sptm.SpeedTestModeController ) v0 ).setSpeedTestMode ( p1 ); // invoke-virtual {v0, p1}, Lcom/miui/server/sptm/SpeedTestModeController;->setSpeedTestMode(Z)V
/* .line 79 */
return;
} // .end method
/* # virtual methods */
public void addAppSwitchOps ( Integer p0 ) {
/* .locals 2 */
/* .param p1, "ops" # I */
/* .line 46 */
int v0 = 3; // const/4 v0, 0x3
int v1 = 1; // const/4 v1, 0x1
/* if-ne p1, v0, :cond_0 */
/* .line 47 */
/* iget v0, p0, Lcom/miui/server/sptm/SpeedTestModeState;->mFastSwitchCount:I */
/* add-int/2addr v0, v1 */
/* iput v0, p0, Lcom/miui/server/sptm/SpeedTestModeState;->mFastSwitchCount:I */
/* .line 48 */
} // :cond_0
int v0 = 2; // const/4 v0, 0x2
/* if-ne p1, v0, :cond_1 */
/* .line 49 */
/* iget v0, p0, Lcom/miui/server/sptm/SpeedTestModeState;->mQuickSwitchCount:I */
/* add-int/2addr v0, v1 */
/* iput v0, p0, Lcom/miui/server/sptm/SpeedTestModeState;->mQuickSwitchCount:I */
/* .line 50 */
} // :cond_1
/* if-ne p1, v1, :cond_2 */
/* .line 51 */
int v0 = 0; // const/4 v0, 0x0
/* invoke-direct {p0, v0}, Lcom/miui/server/sptm/SpeedTestModeState;->setMode(Z)V */
/* .line 52 */
/* invoke-direct {p0}, Lcom/miui/server/sptm/SpeedTestModeState;->reset()V */
/* .line 54 */
} // :cond_2
} // :goto_0
return;
} // .end method
public void addAppSwitchOps ( java.lang.String p0, Boolean p1 ) {
/* .locals 3 */
/* .param p1, "pkgName" # Ljava/lang/String; */
/* .param p2, "isColdStart" # Z */
/* .line 58 */
v0 = this.mSpeedTestModeService;
v0 = (( com.miui.server.sptm.SpeedTestModeServiceImpl ) v0 ).getSPTMCloudEnable ( ); // invoke-virtual {v0}, Lcom/miui/server/sptm/SpeedTestModeServiceImpl;->getSPTMCloudEnable()Z
/* if-nez v0, :cond_0 */
/* .line 59 */
return;
/* .line 62 */
} // :cond_0
v1 = v0 = com.miui.server.sptm.SpeedTestModeState.SPTM_ENABLE_APP_LIST;
/* .line 64 */
/* .local v1, "appSize":I */
if ( p2 != null) { // if-eqz p2, :cond_1
/* iget v2, p0, Lcom/miui/server/sptm/SpeedTestModeState;->mSpecifyAppCount:I */
/* if-ge v2, v1, :cond_1 */
/* .line 65 */
v0 = (( java.lang.String ) p1 ).equals ( v0 ); // invoke-virtual {p1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z
if ( v0 != null) { // if-eqz v0, :cond_1
/* iget v0, p0, Lcom/miui/server/sptm/SpeedTestModeState;->mSpecifyAppCount:I */
int v2 = 1; // const/4 v2, 0x1
/* add-int/2addr v0, v2 */
/* iput v0, p0, Lcom/miui/server/sptm/SpeedTestModeState;->mSpecifyAppCount:I */
/* if-ne v0, v1, :cond_1 */
/* .line 67 */
/* invoke-direct {p0, v2}, Lcom/miui/server/sptm/SpeedTestModeState;->setMode(Z)V */
/* .line 69 */
} // :cond_1
return;
} // .end method
