public class com.miui.server.sptm.SpeedTestModeController {
	 /* .source "SpeedTestModeController.java" */
	 /* # annotations */
	 /* .annotation system Ldalvik/annotation/MemberClasses; */
	 /* value = { */
	 /* Lcom/miui/server/sptm/SpeedTestModeController$OnSpeedTestModeChangeListener; */
	 /* } */
} // .end annotation
/* # instance fields */
private Boolean mIsInSpeedTestMode;
private com.miui.server.sptm.SpeedTestModeController$OnSpeedTestModeChangeListener mListener;
com.miui.server.sptm.SpeedTestModeServiceImpl mSpeedTestModeService;
/* # direct methods */
public com.miui.server.sptm.SpeedTestModeController ( ) {
	 /* .locals 1 */
	 /* .param p1, "listener" # Lcom/miui/server/sptm/SpeedTestModeController$OnSpeedTestModeChangeListener; */
	 /* .line 12 */
	 /* invoke-direct {p0}, Ljava/lang/Object;-><init>()V */
	 /* .line 7 */
	 /* nop */
	 /* .line 8 */
	 com.miui.server.sptm.SpeedTestModeServiceImpl .getInstance ( );
	 this.mSpeedTestModeService = v0;
	 /* .line 9 */
	 int v0 = 0; // const/4 v0, 0x0
	 /* iput-boolean v0, p0, Lcom/miui/server/sptm/SpeedTestModeController;->mIsInSpeedTestMode:Z */
	 /* .line 13 */
	 this.mListener = p1;
	 /* .line 14 */
	 return;
} // .end method
/* # virtual methods */
public Boolean isInSpeedTestMode ( ) {
	 /* .locals 1 */
	 /* .line 30 */
	 /* iget-boolean v0, p0, Lcom/miui/server/sptm/SpeedTestModeController;->mIsInSpeedTestMode:Z */
} // .end method
public void setSpeedTestMode ( Boolean p0 ) {
	 /* .locals 2 */
	 /* .param p1, "isEnable" # Z */
	 /* .line 17 */
	 /* iget-boolean v0, p0, Lcom/miui/server/sptm/SpeedTestModeController;->mIsInSpeedTestMode:Z */
	 /* if-ne v0, p1, :cond_0 */
	 /* .line 18 */
	 return;
	 /* .line 20 */
} // :cond_0
v0 = this.mListener;
if ( v0 != null) { // if-eqz v0, :cond_1
	 /* .line 21 */
	 /* iput-boolean p1, p0, Lcom/miui/server/sptm/SpeedTestModeController;->mIsInSpeedTestMode:Z */
	 /* .line 22 */
	 /* .line 23 */
	 /* sget-boolean v0, Lcom/miui/server/sptm/SpeedTestModeServiceImpl;->DEBUG:Z */
	 if ( v0 != null) { // if-eqz v0, :cond_1
		 /* .line 24 */
		 /* new-instance v0, Ljava/lang/StringBuilder; */
		 /* invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V */
		 final String v1 = "handleUpdateSpeedTestMode: enter spt mode = "; // const-string v1, "handleUpdateSpeedTestMode: enter spt mode = "
		 (( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
		 /* iget-boolean v1, p0, Lcom/miui/server/sptm/SpeedTestModeController;->mIsInSpeedTestMode:Z */
		 (( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;
		 (( java.lang.StringBuilder ) v0 ).toString ( ); // invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
		 final String v1 = "SPTM"; // const-string v1, "SPTM"
		 android.util.Slog .e ( v1,v0 );
		 /* .line 27 */
	 } // :cond_1
	 return;
} // .end method
