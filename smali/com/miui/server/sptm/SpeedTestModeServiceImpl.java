public class com.miui.server.sptm.SpeedTestModeServiceImpl extends com.android.server.am.SpeedTestModeServiceStub implements com.miui.app.SpeedTestModeServiceInternal {
	 /* .source "SpeedTestModeServiceImpl.java" */
	 /* # interfaces */
	 /* # annotations */
	 /* .annotation runtime Lcom/miui/base/annotations/MiuiStubHead; */
	 /* manifestName = "com.miui.server.sptm.SpeedTestModeServiceStub$$" */
} // .end annotation
/* .annotation system Ldalvik/annotation/MemberClasses; */
/* value = { */
/* Lcom/miui/server/sptm/SpeedTestModeServiceImpl$Handler;, */
/* Lcom/miui/server/sptm/SpeedTestModeServiceImpl$AppUsageRecord;, */
/* Lcom/miui/server/sptm/SpeedTestModeServiceImpl$Strategy; */
/* } */
} // .end annotation
/* # static fields */
public static final Long COLD_START_DELAYED_TIME;
public static final Integer ENABLE_SPTM_MIN_MEMORY;
public static final Integer EVENT_TYPE_LOCK_SCREEN;
public static final Integer EVENT_TYPE_ONE_KEY_CLEAN;
public static final Integer EVENT_TYPE_PAUSE;
public static final Integer EVENT_TYPE_PRELOAD_STARTED;
public static final Integer EVENT_TYPE_RESUME;
public static final Integer EVENT_TYPE_START_PROC;
public static final Long FAST_SWITCH_HOME_DURATION;
public static final java.util.List GAME_APPS;
/* .annotation system Ldalvik/annotation/Signature; */
/* value = { */
/* "Ljava/util/List<", */
/* "Ljava/lang/String;", */
/* ">;" */
/* } */
} // .end annotation
} // .end field
public static final Long GAME_COLD_START_DELAYED_TIME;
public static final Long GAME_HOT_START_DELAYED_TIME;
public static final Long HOT_START_DELAYED_TIME;
private static final Boolean IGNORE_CLOUD_ENABLE;
private static final java.util.List PERMISSION_DIALOG_PACKAGE_NAMES;
/* .annotation system Ldalvik/annotation/Signature; */
/* value = { */
/* "Ljava/util/List<", */
/* "Ljava/lang/String;", */
/* ">;" */
/* } */
} // .end annotation
} // .end field
public static final java.util.List PRELOAD_APPS;
/* .annotation system Ldalvik/annotation/Signature; */
/* value = { */
/* "Ljava/util/List<", */
/* "Ljava/lang/String;", */
/* ">;" */
/* } */
} // .end annotation
} // .end field
public static final java.util.List PRELOAD_APPS_NEW;
/* .annotation system Ldalvik/annotation/Signature; */
/* value = { */
/* "Ljava/util/List<", */
/* "Ljava/lang/String;", */
/* ">;" */
/* } */
} // .end annotation
} // .end field
public static final java.util.List PRELOAD_GAME_APPS;
/* .annotation system Ldalvik/annotation/Signature; */
/* value = { */
/* "Ljava/util/List<", */
/* "Ljava/lang/String;", */
/* ">;" */
/* } */
} // .end annotation
} // .end field
public static final Integer PRELOAD_THRESHOLD;
public static final java.util.List SPEED_TEST_APP_LIST;
/* .annotation system Ldalvik/annotation/Signature; */
/* value = { */
/* "Ljava/util/List<", */
/* "Ljava/lang/String;", */
/* ">;" */
/* } */
} // .end annotation
} // .end field
public static final java.util.List SPEED_TEST_APP_LIST_NEW;
/* .annotation system Ldalvik/annotation/Signature; */
/* value = { */
/* "Ljava/util/List<", */
/* "Ljava/lang/String;", */
/* ">;" */
/* } */
} // .end annotation
} // .end field
public static final java.util.List SPEED_TEST_APP_LIST_OLD;
/* .annotation system Ldalvik/annotation/Signature; */
/* value = { */
/* "Ljava/util/List<", */
/* "Ljava/lang/String;", */
/* ">;" */
/* } */
} // .end annotation
} // .end field
private static final java.lang.String SPTM_ANIMATION_CLOUD_ENABLE;
private static final java.lang.String SPTM_APP_LIST;
private static final java.lang.String SPTM_CLOUD_ENABLE_NEW;
private static final java.lang.String SPTM_CLOUD_ENABLE_OLD;
private static final java.lang.String SPTM_KEY;
public static final Integer SPTM_LOW_MEMORY_DEVICE_PRELOAD_CORE;
public static final Integer SPTM_LOW_MEMORY_DEVICE_THRESHOLD;
private static final java.lang.String SPTM_MODULE_KEY;
private static final java.lang.String SPTM_PRELOAD_CLOUD;
public static final Integer STARTED_APPCOUNT;
public static final Long START_PROC_DELAYED_TIME;
public static final java.lang.String TAG;
public static final Long TOTAL_MEMORY;
/* # instance fields */
private java.util.HashMap mAppLastResumedTimes;
/* .annotation system Ldalvik/annotation/Signature; */
/* value = { */
/* "Ljava/util/HashMap<", */
/* "Ljava/lang/String;", */
/* "Ljava/lang/Long;", */
/* ">;" */
/* } */
} // .end annotation
} // .end field
private java.util.LinkedHashMap mAppStartProcTimes;
/* .annotation system Ldalvik/annotation/Signature; */
/* value = { */
/* "Ljava/util/LinkedHashMap<", */
/* "Ljava/lang/String;", */
/* "Ljava/lang/Long;", */
/* ">;" */
/* } */
} // .end annotation
} // .end field
private java.util.LinkedList mAppStartRecords;
/* .annotation system Ldalvik/annotation/Signature; */
/* value = { */
/* "Ljava/util/LinkedList<", */
/* "Lcom/miui/server/sptm/SpeedTestModeServiceImpl$AppUsageRecord;", */
/* ">;" */
/* } */
} // .end annotation
} // .end field
private android.content.Context mContext;
private com.miui.server.sptm.SpeedTestModeServiceImpl$Handler mH;
private android.os.HandlerThread mHandlerThread;
private com.miui.server.sptm.HomeAnimationStrategy mHomeAnimationStrategy;
private java.lang.String mHomePackageName;
private Boolean mIsEnableNew;
private Boolean mIsEnableOld;
private Boolean mIsEnableSPTMAnimation;
private Boolean mIsSpeedTestEnabled;
private Boolean mIsSpeedTestMode;
private com.miui.server.sptm.PreLoadStrategy mPreLoadStrategy;
private Integer mPreloadType;
private com.miui.server.sptm.SpeedTestModeState mSpeedTestModeState;
public java.util.LinkedList mSpeedTestModeStrategies;
/* .annotation system Ldalvik/annotation/Signature; */
/* value = { */
/* "Ljava/util/LinkedList<", */
/* "Lcom/miui/server/sptm/SpeedTestModeServiceImpl$Strategy;", */
/* ">;" */
/* } */
} // .end annotation
} // .end field
/* # direct methods */
public static void $r8$lambda$cWnX9FZHZctoyqOleSKFNet6umw ( com.miui.server.sptm.SpeedTestModeServiceImpl p0, Boolean p1 ) { //synthethic
/* .locals 0 */
/* invoke-direct {p0, p1}, Lcom/miui/server/sptm/SpeedTestModeServiceImpl;->setEnableSpeedTestMode(Z)V */
return;
} // .end method
static Boolean -$$Nest$fgetmIsSpeedTestEnabled ( com.miui.server.sptm.SpeedTestModeServiceImpl p0 ) { //bridge//synthethic
/* .locals 0 */
/* iget-boolean p0, p0, Lcom/miui/server/sptm/SpeedTestModeServiceImpl;->mIsSpeedTestEnabled:Z */
} // .end method
static com.miui.server.sptm.PreLoadStrategy -$$Nest$fgetmPreLoadStrategy ( com.miui.server.sptm.SpeedTestModeServiceImpl p0 ) { //bridge//synthethic
/* .locals 0 */
p0 = this.mPreLoadStrategy;
} // .end method
static com.miui.server.sptm.SpeedTestModeState -$$Nest$fgetmSpeedTestModeState ( com.miui.server.sptm.SpeedTestModeServiceImpl p0 ) { //bridge//synthethic
/* .locals 0 */
p0 = this.mSpeedTestModeState;
} // .end method
static void -$$Nest$mhandleEvent ( com.miui.server.sptm.SpeedTestModeServiceImpl p0, Integer p1, java.lang.String p2, Long p3 ) { //bridge//synthethic
/* .locals 0 */
/* invoke-direct {p0, p1, p2, p3, p4}, Lcom/miui/server/sptm/SpeedTestModeServiceImpl;->handleEvent(ILjava/lang/String;J)V */
return;
} // .end method
static void -$$Nest$mhandleUpdateSpeedTestMode ( com.miui.server.sptm.SpeedTestModeServiceImpl p0, Boolean p1 ) { //bridge//synthethic
/* .locals 0 */
/* invoke-direct {p0, p1}, Lcom/miui/server/sptm/SpeedTestModeServiceImpl;->handleUpdateSpeedTestMode(Z)V */
return;
} // .end method
static void -$$Nest$mupdateCloudControlParas ( com.miui.server.sptm.SpeedTestModeServiceImpl p0 ) { //bridge//synthethic
/* .locals 0 */
/* invoke-direct {p0}, Lcom/miui/server/sptm/SpeedTestModeServiceImpl;->updateCloudControlParas()V */
return;
} // .end method
static com.miui.server.sptm.SpeedTestModeServiceImpl ( ) {
/* .locals 18 */
/* .line 44 */
/* nop */
/* .line 45 */
final String v0 = "persist.sys.miui_sptm.start_proc_delayed"; // const-string v0, "persist.sys.miui_sptm.start_proc_delayed"
/* const-wide/16 v1, 0x5dc */
android.os.SystemProperties .getLong ( v0,v1,v2 );
/* move-result-wide v0 */
/* sput-wide v0, Lcom/miui/server/sptm/SpeedTestModeServiceImpl;->START_PROC_DELAYED_TIME:J */
/* .line 46 */
/* nop */
/* .line 47 */
final String v0 = "persist.sys.miui_sptm.fast_home"; // const-string v0, "persist.sys.miui_sptm.fast_home"
/* const/16 v1, 0x1388 */
v0 = android.os.SystemProperties .getInt ( v0,v1 );
/* int-to-long v0, v0 */
/* sput-wide v0, Lcom/miui/server/sptm/SpeedTestModeServiceImpl;->FAST_SWITCH_HOME_DURATION:J */
/* .line 48 */
/* nop */
/* .line 49 */
final String v0 = "persist.sys.miui_sptm.hot_start_delayed"; // const-string v0, "persist.sys.miui_sptm.hot_start_delayed"
/* const-wide/16 v1, 0x4e20 */
android.os.SystemProperties .getLong ( v0,v1,v2 );
/* move-result-wide v3 */
/* sput-wide v3, Lcom/miui/server/sptm/SpeedTestModeServiceImpl;->HOT_START_DELAYED_TIME:J */
/* .line 50 */
/* nop */
/* .line 51 */
final String v0 = "persist.sys.miui_sptm.cold_start_delayed"; // const-string v0, "persist.sys.miui_sptm.cold_start_delayed"
android.os.SystemProperties .getLong ( v0,v1,v2 );
/* move-result-wide v0 */
/* sput-wide v0, Lcom/miui/server/sptm/SpeedTestModeServiceImpl;->COLD_START_DELAYED_TIME:J */
/* .line 52 */
/* nop */
/* .line 53 */
final String v0 = "persist.sys.miui_sptm.game_hot_start_delayed"; // const-string v0, "persist.sys.miui_sptm.game_hot_start_delayed"
/* const-wide/16 v1, 0x7530 */
android.os.SystemProperties .getLong ( v0,v1,v2 );
/* move-result-wide v0 */
/* sput-wide v0, Lcom/miui/server/sptm/SpeedTestModeServiceImpl;->GAME_HOT_START_DELAYED_TIME:J */
/* .line 54 */
/* nop */
/* .line 55 */
final String v0 = "persist.sys.miui_sptm.game_cold_start_delayed"; // const-string v0, "persist.sys.miui_sptm.game_cold_start_delayed"
/* const-wide/32 v1, 0xea60 */
android.os.SystemProperties .getLong ( v0,v1,v2 );
/* move-result-wide v0 */
/* sput-wide v0, Lcom/miui/server/sptm/SpeedTestModeServiceImpl;->GAME_COLD_START_DELAYED_TIME:J */
/* .line 56 */
/* nop */
/* .line 57 */
final String v0 = "persist.sys.miui_sptm.pl_threshold"; // const-string v0, "persist.sys.miui_sptm.pl_threshold"
int v1 = 6; // const/4 v1, 0x6
v0 = android.os.SystemProperties .getInt ( v0,v1 );
/* .line 58 */
/* nop */
/* .line 59 */
final String v0 = "persist.sys.miui_sptm.strarted_count"; // const-string v0, "persist.sys.miui_sptm.strarted_count"
/* const/16 v1, 0x14 */
v0 = android.os.SystemProperties .getInt ( v0,v1 );
/* .line 60 */
final String v0 = "persist.sys.miui_stpm.low_mem_device"; // const-string v0, "persist.sys.miui_stpm.low_mem_device"
/* const/16 v2, 0xc */
v0 = android.os.SystemProperties .getInt ( v0,v2 );
/* mul-int/lit16 v0, v0, 0x3e8 */
/* .line 63 */
final String v0 = "persist.sys.miui_stpm.low_mem_device_pl_cores"; // const-string v0, "persist.sys.miui_stpm.low_mem_device_pl_cores"
int v2 = 5; // const/4 v2, 0x5
v0 = android.os.SystemProperties .getInt ( v0,v2 );
/* .line 65 */
android.os.Process .getTotalMemory ( );
/* move-result-wide v2 */
/* shr-long v0, v2, v1 */
/* sput-wide v0, Lcom/miui/server/sptm/SpeedTestModeServiceImpl;->TOTAL_MEMORY:J */
/* .line 66 */
/* nop */
/* .line 67 */
final String v0 = "persist.sys.miui_sptm.ignore_cloud_enable"; // const-string v0, "persist.sys.miui_sptm.ignore_cloud_enable"
int v1 = 0; // const/4 v1, 0x0
v0 = android.os.SystemProperties .getBoolean ( v0,v1 );
com.miui.server.sptm.SpeedTestModeServiceImpl.IGNORE_CLOUD_ENABLE = (v0!= 0);
/* .line 92 */
/* new-instance v0, Ljava/util/LinkedList; */
/* invoke-direct {v0}, Ljava/util/LinkedList;-><init>()V */
/* .line 93 */
/* new-instance v2, Ljava/util/LinkedList; */
/* invoke-direct {v2}, Ljava/util/LinkedList;-><init>()V */
/* .line 94 */
final String v3 = "persist.sys.miui_sptm_new.enable"; // const-string v3, "persist.sys.miui_sptm_new.enable"
v1 = android.os.SystemProperties .getBoolean ( v3,v1 );
if ( v1 != null) { // if-eqz v1, :cond_0
/* .line 96 */
/* move-object v1, v2 */
} // :cond_0
/* move-object v1, v0 */
} // :goto_0
/* .line 99 */
final String v1 = "com.ss.android.ugc.aweme"; // const-string v1, "com.ss.android.ugc.aweme"
/* .line 100 */
final String v3 = "com.quark.browser"; // const-string v3, "com.quark.browser"
/* .line 101 */
final String v3 = "com.xingin.xhs"; // const-string v3, "com.xingin.xhs"
/* .line 102 */
final String v4 = "com.dragon.read"; // const-string v4, "com.dragon.read"
/* .line 103 */
final String v4 = "com.autonavi.minimap"; // const-string v4, "com.autonavi.minimap"
/* .line 104 */
final String v5 = "com.jingdong.app.mall"; // const-string v5, "com.jingdong.app.mall"
/* .line 105 */
final String v6 = "com.taobao.idlefish"; // const-string v6, "com.taobao.idlefish"
/* .line 106 */
final String v6 = "com.shizhuang.duapp"; // const-string v6, "com.shizhuang.duapp"
/* .line 107 */
final String v6 = "com.qiyi.video"; // const-string v6, "com.qiyi.video"
/* .line 108 */
final String v7 = "com.Qunar"; // const-string v7, "com.Qunar"
/* .line 109 */
final String v7 = "com.dianping.v1"; // const-string v7, "com.dianping.v1"
/* .line 110 */
final String v7 = "com.sankuai.meituan.takeoutnew"; // const-string v7, "com.sankuai.meituan.takeoutnew"
/* .line 111 */
final String v8 = "com.sina.weibo"; // const-string v8, "com.sina.weibo"
/* .line 112 */
final String v9 = "com.ss.android.article.news"; // const-string v9, "com.ss.android.article.news"
/* .line 113 */
final String v9 = "cn.wps.moffice_eng"; // const-string v9, "cn.wps.moffice_eng"
/* .line 114 */
final String v9 = "com.xt.retouch"; // const-string v9, "com.xt.retouch"
/* .line 115 */
/* const-string/jumbo v9, "tv.danmaku.bili" */
/* .line 116 */
final String v10 = "com.taobao.taobao"; // const-string v10, "com.taobao.taobao"
/* .line 117 */
final String v11 = "com.miHoYo.ys.mi"; // const-string v11, "com.miHoYo.ys.mi"
/* .line 118 */
final String v12 = "com.netease.cloudmusic"; // const-string v12, "com.netease.cloudmusic"
/* .line 119 */
final String v13 = "cn.xiaochuankeji.tieba"; // const-string v13, "cn.xiaochuankeji.tieba"
/* .line 120 */
final String v13 = "com.zhihu.android"; // const-string v13, "com.zhihu.android"
/* .line 121 */
final String v14 = "com.lemon.lv"; // const-string v14, "com.lemon.lv"
/* .line 122 */
final String v15 = "com.tencent.mobileqq"; // const-string v15, "com.tencent.mobileqq"
/* .line 123 */
/* move-object/from16 v16, v14 */
final String v14 = "com.baidu.tieba"; // const-string v14, "com.baidu.tieba"
/* .line 124 */
final String v14 = "com.smile.gifmaker"; // const-string v14, "com.smile.gifmaker"
/* .line 125 */
/* move-object/from16 v17, v14 */
final String v14 = "com.baidu.searchbox"; // const-string v14, "com.baidu.searchbox"
/* .line 126 */
final String v14 = "com.xunmeng.pinduoduo"; // const-string v14, "com.xunmeng.pinduoduo"
/* .line 127 */
final String v14 = "com.manmanbuy.bijia"; // const-string v14, "com.manmanbuy.bijia"
/* .line 131 */
final String v2 = "com.tencent.mm"; // const-string v2, "com.tencent.mm"
/* .line 132 */
/* .line 133 */
/* .line 134 */
final String v2 = "com.eg.android.AlipayGphone"; // const-string v2, "com.eg.android.AlipayGphone"
/* .line 135 */
/* .line 136 */
/* .line 137 */
final String v2 = "com.ss.android.lark"; // const-string v2, "com.ss.android.lark"
/* .line 138 */
/* .line 139 */
final String v2 = "com.tencent.qqmusic"; // const-string v2, "com.tencent.qqmusic"
/* .line 140 */
final String v2 = "com.MobileTicket"; // const-string v2, "com.MobileTicket"
/* .line 141 */
/* .line 142 */
/* .line 143 */
/* .line 144 */
/* .line 145 */
/* .line 146 */
/* .line 147 */
final String v1 = "com.hicorenational.antifraud"; // const-string v1, "com.hicorenational.antifraud"
/* .line 148 */
/* .line 151 */
/* new-instance v0, Ljava/util/LinkedList; */
/* invoke-direct {v0}, Ljava/util/LinkedList;-><init>()V */
/* .line 154 */
final String v1 = "com.tencent.tmgp.sgame"; // const-string v1, "com.tencent.tmgp.sgame"
/* .line 155 */
final String v2 = "com.tencent.tmgp.pubgmhd"; // const-string v2, "com.tencent.tmgp.pubgmhd"
/* .line 159 */
/* new-instance v3, Ljava/util/LinkedList; */
/* invoke-direct {v3}, Ljava/util/LinkedList;-><init>()V */
/* .line 162 */
/* .line 163 */
/* .line 164 */
final String v4 = "com.miHoYo.Yuanshen"; // const-string v4, "com.miHoYo.Yuanshen"
/* .line 167 */
/* new-instance v3, Ljava/util/LinkedList; */
/* invoke-direct {v3}, Ljava/util/LinkedList;-><init>()V */
/* .line 170 */
/* .line 171 */
/* .line 172 */
/* .line 173 */
/* .line 174 */
/* .line 175 */
/* .line 176 */
/* move-object/from16 v1, v16 */
/* .line 177 */
/* move-object/from16 v1, v17 */
/* .line 180 */
/* new-instance v1, Ljava/util/LinkedList; */
/* invoke-direct {v1}, Ljava/util/LinkedList;-><init>()V */
/* .line 183 */
/* .line 184 */
/* .line 187 */
/* new-instance v0, Ljava/util/LinkedList; */
/* invoke-direct {v0}, Ljava/util/LinkedList;-><init>()V */
/* .line 190 */
final String v1 = "com.miui.securitycenter"; // const-string v1, "com.miui.securitycenter"
/* .line 191 */
final String v1 = "com.lbe.security.miui"; // const-string v1, "com.lbe.security.miui"
/* .line 192 */
return;
} // .end method
 com.miui.server.sptm.SpeedTestModeServiceImpl ( ) {
/* .locals 4 */
/* .line 256 */
/* invoke-direct {p0}, Lcom/android/server/am/SpeedTestModeServiceStub;-><init>()V */
/* .line 68 */
/* nop */
/* .line 69 */
final String v0 = "persist.sys.miui_sptm.enable"; // const-string v0, "persist.sys.miui_sptm.enable"
int v1 = 0; // const/4 v1, 0x0
v0 = android.os.SystemProperties .getBoolean ( v0,v1 );
/* iput-boolean v0, p0, Lcom/miui/server/sptm/SpeedTestModeServiceImpl;->mIsEnableOld:Z */
/* .line 70 */
int v2 = 1; // const/4 v2, 0x1
/* if-nez v0, :cond_0 */
/* .line 71 */
final String v0 = "persist.sys.miui_sptm_new.enable"; // const-string v0, "persist.sys.miui_sptm_new.enable"
v0 = android.os.SystemProperties .getBoolean ( v0,v1 );
if ( v0 != null) { // if-eqz v0, :cond_0
/* move v0, v2 */
} // :cond_0
/* move v0, v1 */
} // :goto_0
/* iput-boolean v0, p0, Lcom/miui/server/sptm/SpeedTestModeServiceImpl;->mIsEnableNew:Z */
/* .line 72 */
/* nop */
/* .line 73 */
final String v0 = "persist.sys.miui_sptm.enable_pl_type"; // const-string v0, "persist.sys.miui_sptm.enable_pl_type"
/* const/16 v3, 0x8 */
v0 = android.os.SystemProperties .getInt ( v0,v3 );
/* iput v0, p0, Lcom/miui/server/sptm/SpeedTestModeServiceImpl;->mPreloadType:I */
/* .line 74 */
/* nop */
/* .line 75 */
final String v0 = "persist.sys.miui_sptm_animation.enable"; // const-string v0, "persist.sys.miui_sptm_animation.enable"
v0 = android.os.SystemProperties .getBoolean ( v0,v2 );
/* iput-boolean v0, p0, Lcom/miui/server/sptm/SpeedTestModeServiceImpl;->mIsEnableSPTMAnimation:Z */
/* .line 194 */
final String v0 = "com.miui.home"; // const-string v0, "com.miui.home"
this.mHomePackageName = v0;
/* .line 196 */
/* iput-boolean v2, p0, Lcom/miui/server/sptm/SpeedTestModeServiceImpl;->mIsSpeedTestEnabled:Z */
/* .line 197 */
/* iput-boolean v1, p0, Lcom/miui/server/sptm/SpeedTestModeServiceImpl;->mIsSpeedTestMode:Z */
/* .line 198 */
/* new-instance v0, Ljava/util/LinkedList; */
/* invoke-direct {v0}, Ljava/util/LinkedList;-><init>()V */
this.mSpeedTestModeStrategies = v0;
/* .line 201 */
/* new-instance v0, Ljava/util/LinkedList; */
/* invoke-direct {v0}, Ljava/util/LinkedList;-><init>()V */
this.mAppStartRecords = v0;
/* .line 202 */
/* new-instance v0, Ljava/util/HashMap; */
/* invoke-direct {v0}, Ljava/util/HashMap;-><init>()V */
this.mAppLastResumedTimes = v0;
/* .line 203 */
/* new-instance v0, Lcom/miui/server/sptm/SpeedTestModeServiceImpl$1; */
/* invoke-direct {v0, p0}, Lcom/miui/server/sptm/SpeedTestModeServiceImpl$1;-><init>(Lcom/miui/server/sptm/SpeedTestModeServiceImpl;)V */
this.mAppStartProcTimes = v0;
/* .line 213 */
int v0 = 0; // const/4 v0, 0x0
this.mContext = v0;
/* .line 257 */
/* const-class v0, Lcom/miui/app/SpeedTestModeServiceInternal; */
com.android.server.LocalServices .addService ( v0,p0 );
/* .line 258 */
return;
} // .end method
private static android.os.Bundle createMsgData ( Integer p0, java.lang.String p1, Long p2 ) {
/* .locals 2 */
/* .param p0, "event" # I */
/* .param p1, "packageName" # Ljava/lang/String; */
/* .param p2, "time" # J */
/* .line 631 */
/* new-instance v0, Landroid/os/Bundle; */
/* invoke-direct {v0}, Landroid/os/Bundle;-><init>()V */
/* .line 632 */
/* .local v0, "b":Landroid/os/Bundle; */
final String v1 = "event"; // const-string v1, "event"
(( android.os.Bundle ) v0 ).putInt ( v1, p0 ); // invoke-virtual {v0, v1, p0}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V
/* .line 633 */
final String v1 = "packageName"; // const-string v1, "packageName"
(( android.os.Bundle ) v0 ).putString ( v1, p1 ); // invoke-virtual {v0, v1, p1}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V
/* .line 634 */
/* const-string/jumbo v1, "time" */
(( android.os.Bundle ) v0 ).putLong ( v1, p2, p3 ); // invoke-virtual {v0, v1, p2, p3}, Landroid/os/Bundle;->putLong(Ljava/lang/String;J)V
/* .line 635 */
} // .end method
public static Integer getAmsMaxCachedProcesses ( ) {
/* .locals 4 */
/* .line 674 */
/* sget-wide v0, Lcom/miui/server/sptm/SpeedTestModeServiceImpl;->TOTAL_MEMORY:J */
/* const-wide/16 v2, 0x1770 */
/* cmp-long v0, v0, v2 */
/* if-gtz v0, :cond_0 */
/* .line 675 */
int v0 = -1; // const/4 v0, -0x1
/* .line 677 */
} // :cond_0
/* const/16 v0, 0x3c */
} // .end method
private Integer getAppSwitchingOperation ( java.util.List p0, com.miui.server.sptm.SpeedTestModeServiceImpl$AppUsageRecord p1, com.miui.server.sptm.PreLoadStrategy$AppStartRecord p2 ) {
/* .locals 10 */
/* .param p2, "homeRecord" # Lcom/miui/server/sptm/SpeedTestModeServiceImpl$AppUsageRecord; */
/* .param p3, "outRes" # Lcom/miui/server/sptm/PreLoadStrategy$AppStartRecord; */
/* .annotation system Ldalvik/annotation/Signature; */
/* value = { */
/* "(", */
/* "Ljava/util/List<", */
/* "Lcom/miui/server/sptm/SpeedTestModeServiceImpl$AppUsageRecord;", */
/* ">;", */
/* "Lcom/miui/server/sptm/SpeedTestModeServiceImpl$AppUsageRecord;", */
/* "Lcom/miui/server/sptm/PreLoadStrategy$AppStartRecord;", */
/* ")I" */
/* } */
} // .end annotation
/* .line 552 */
/* .local p1, "appUsageRecord":Ljava/util/List;, "Ljava/util/List<Lcom/miui/server/sptm/SpeedTestModeServiceImpl$AppUsageRecord;>;" */
int v0 = 1; // const/4 v0, 0x1
if ( p2 != null) { // if-eqz p2, :cond_f
(( com.miui.server.sptm.SpeedTestModeServiceImpl$AppUsageRecord ) p2 ).getDuration ( ); // invoke-virtual {p2}, Lcom/miui/server/sptm/SpeedTestModeServiceImpl$AppUsageRecord;->getDuration()J
/* move-result-wide v1 */
/* sget-wide v3, Lcom/miui/server/sptm/SpeedTestModeServiceImpl;->FAST_SWITCH_HOME_DURATION:J */
/* cmp-long v1, v1, v3 */
/* if-ltz v1, :cond_0 */
/* goto/16 :goto_6 */
/* .line 557 */
} // :cond_0
int v1 = 2; // const/4 v1, 0x2
v2 = if ( p1 != null) { // if-eqz p1, :cond_e
/* if-nez v2, :cond_1 */
/* goto/16 :goto_5 */
/* .line 561 */
} // :cond_1
v2 = com.miui.server.sptm.SpeedTestModeServiceImpl.SPEED_TEST_APP_LIST;
int v3 = 0; // const/4 v3, 0x0
/* check-cast v4, Lcom/miui/server/sptm/SpeedTestModeServiceImpl$AppUsageRecord; */
v2 = v4 = this.packageName;
/* if-nez v2, :cond_2 */
v2 = com.miui.server.sptm.SpeedTestModeServiceImpl.GAME_APPS;
/* .line 562 */
/* check-cast v4, Lcom/miui/server/sptm/SpeedTestModeServiceImpl$AppUsageRecord; */
v2 = v4 = this.packageName;
/* if-nez v2, :cond_2 */
/* .line 563 */
/* .line 567 */
} // :cond_2
int v2 = 0; // const/4 v2, 0x0
/* .line 568 */
/* .local v2, "coldStartPackageName":Ljava/lang/String; */
/* const-wide/16 v4, 0x0 */
/* .line 569 */
/* .local v4, "totalDuration":J */
v7 = } // :goto_0
if ( v7 != null) { // if-eqz v7, :cond_4
/* check-cast v7, Lcom/miui/server/sptm/SpeedTestModeServiceImpl$AppUsageRecord; */
/* .line 570 */
/* .local v7, "aur":Lcom/miui/server/sptm/SpeedTestModeServiceImpl$AppUsageRecord; */
(( com.miui.server.sptm.SpeedTestModeServiceImpl$AppUsageRecord ) v7 ).getDuration ( ); // invoke-virtual {v7}, Lcom/miui/server/sptm/SpeedTestModeServiceImpl$AppUsageRecord;->getDuration()J
/* move-result-wide v8 */
/* add-long/2addr v4, v8 */
/* .line 571 */
/* if-nez v2, :cond_3 */
/* iget-boolean v8, v7, Lcom/miui/server/sptm/SpeedTestModeServiceImpl$AppUsageRecord;->isColdStart:Z */
if ( v8 != null) { // if-eqz v8, :cond_3
/* .line 572 */
v2 = this.packageName;
/* .line 576 */
} // :cond_3
v8 = this.packageName;
this.packageName = v8;
/* .line 577 */
/* iput-boolean v3, p3, Lcom/miui/server/sptm/PreLoadStrategy$AppStartRecord;->isColdStart:Z */
/* .line 578 */
} // .end local v7 # "aur":Lcom/miui/server/sptm/SpeedTestModeServiceImpl$AppUsageRecord;
/* .line 579 */
} // :cond_4
/* if-nez v2, :cond_8 */
/* .line 580 */
v3 = com.miui.server.sptm.SpeedTestModeServiceImpl.GAME_APPS;
v3 = v6 = this.packageName;
/* if-nez v3, :cond_6 */
/* .line 581 */
/* sget-wide v6, Lcom/miui/server/sptm/SpeedTestModeServiceImpl;->HOT_START_DELAYED_TIME:J */
/* cmp-long v3, v4, v6 */
/* if-gtz v3, :cond_5 */
/* .line 582 */
/* move v0, v1 */
} // :cond_5
/* nop */
/* .line 581 */
} // :goto_1
/* .line 584 */
} // :cond_6
/* sget-wide v6, Lcom/miui/server/sptm/SpeedTestModeServiceImpl;->GAME_HOT_START_DELAYED_TIME:J */
/* cmp-long v3, v4, v6 */
/* if-gtz v3, :cond_7 */
/* .line 585 */
/* move v0, v1 */
} // :cond_7
/* nop */
/* .line 584 */
} // :goto_2
/* .line 589 */
} // :cond_8
this.packageName = v2;
/* .line 590 */
/* iput-boolean v0, p3, Lcom/miui/server/sptm/PreLoadStrategy$AppStartRecord;->isColdStart:Z */
/* .line 593 */
/* const-wide/16 v3, 0x0 */
/* .line 594 */
} // .end local v4 # "totalDuration":J
/* .local v3, "totalDuration":J */
/* const-wide/16 v5, 0x0 */
/* .line 595 */
/* .local v5, "lastResumedActivityDuration":J */
v7 = } // :goto_3
if ( v7 != null) { // if-eqz v7, :cond_b
/* check-cast v7, Lcom/miui/server/sptm/SpeedTestModeServiceImpl$AppUsageRecord; */
/* .line 596 */
/* .local v7, "ur":Lcom/miui/server/sptm/SpeedTestModeServiceImpl$AppUsageRecord; */
v8 = this.packageName;
v8 = (( java.lang.String ) v2 ).equals ( v8 ); // invoke-virtual {v2, v8}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z
if ( v8 != null) { // if-eqz v8, :cond_a
/* .line 597 */
(( com.miui.server.sptm.SpeedTestModeServiceImpl$AppUsageRecord ) v7 ).getDuration ( ); // invoke-virtual {v7}, Lcom/miui/server/sptm/SpeedTestModeServiceImpl$AppUsageRecord;->getDuration()J
/* move-result-wide v8 */
/* add-long/2addr v3, v8 */
/* .line 600 */
/* iget-boolean v8, v7, Lcom/miui/server/sptm/SpeedTestModeServiceImpl$AppUsageRecord;->isColdStart:Z */
/* if-nez v8, :cond_9 */
(( com.miui.server.sptm.SpeedTestModeServiceImpl$AppUsageRecord ) v7 ).getDuration ( ); // invoke-virtual {v7}, Lcom/miui/server/sptm/SpeedTestModeServiceImpl$AppUsageRecord;->getDuration()J
/* move-result-wide v8 */
} // :cond_9
/* const-wide/16 v8, 0x0 */
} // :goto_4
/* move-wide v5, v8 */
/* .line 602 */
} // .end local v7 # "ur":Lcom/miui/server/sptm/SpeedTestModeServiceImpl$AppUsageRecord;
} // :cond_a
/* .line 604 */
} // :cond_b
v1 = v1 = com.miui.server.sptm.SpeedTestModeServiceImpl.GAME_APPS;
int v7 = 3; // const/4 v7, 0x3
/* if-nez v1, :cond_c */
/* .line 605 */
/* sget-wide v8, Lcom/miui/server/sptm/SpeedTestModeServiceImpl;->COLD_START_DELAYED_TIME:J */
/* cmp-long v1, v3, v8 */
/* if-gtz v1, :cond_d */
/* sget-wide v8, Lcom/miui/server/sptm/SpeedTestModeServiceImpl;->HOT_START_DELAYED_TIME:J */
/* cmp-long v1, v5, v8 */
/* if-gtz v1, :cond_d */
/* .line 607 */
/* .line 610 */
} // :cond_c
/* sget-wide v8, Lcom/miui/server/sptm/SpeedTestModeServiceImpl;->GAME_COLD_START_DELAYED_TIME:J */
/* cmp-long v1, v3, v8 */
/* if-gtz v1, :cond_d */
/* .line 611 */
/* .line 614 */
} // :cond_d
/* .line 558 */
} // .end local v2 # "coldStartPackageName":Ljava/lang/String;
} // .end local v3 # "totalDuration":J
} // .end local v5 # "lastResumedActivityDuration":J
} // :cond_e
} // :goto_5
/* .line 553 */
} // :cond_f
} // :goto_6
} // .end method
public static com.miui.server.sptm.SpeedTestModeServiceImpl getInstance ( ) {
/* .locals 1 */
/* .line 253 */
/* const-class v0, Lcom/android/server/am/SpeedTestModeServiceStub; */
com.miui.base.MiuiStubUtil .getImpl ( v0 );
/* check-cast v0, Lcom/miui/server/sptm/SpeedTestModeServiceImpl; */
} // .end method
private void handleAppSwitchingEvent ( com.miui.server.sptm.SpeedTestModeServiceImpl$AppUsageRecord p0 ) {
/* .locals 7 */
/* .param p1, "appRecord" # Lcom/miui/server/sptm/SpeedTestModeServiceImpl$AppUsageRecord; */
/* .line 512 */
v0 = this.mHomePackageName;
v1 = this.packageName;
v0 = (( java.lang.String ) v0 ).equals ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z
int v1 = 0; // const/4 v1, 0x0
if ( v0 != null) { // if-eqz v0, :cond_4
/* .line 514 */
/* new-instance v0, Lcom/miui/server/sptm/PreLoadStrategy$AppStartRecord; */
/* invoke-direct {v0}, Lcom/miui/server/sptm/PreLoadStrategy$AppStartRecord;-><init>()V */
/* .line 515 */
/* .local v0, "outRes":Lcom/miui/server/sptm/PreLoadStrategy$AppStartRecord; */
v2 = this.mAppStartRecords;
v2 = /* invoke-direct {p0, v2, p1, v0}, Lcom/miui/server/sptm/SpeedTestModeServiceImpl;->getAppSwitchingOperation(Ljava/util/List;Lcom/miui/server/sptm/SpeedTestModeServiceImpl$AppUsageRecord;Lcom/miui/server/sptm/PreLoadStrategy$AppStartRecord;)I */
/* .line 516 */
/* .local v2, "switchOps":I */
v3 = this.mSpeedTestModeState;
(( com.miui.server.sptm.SpeedTestModeState ) v3 ).addAppSwitchOps ( v2 ); // invoke-virtual {v3, v2}, Lcom/miui/server/sptm/SpeedTestModeState;->addAppSwitchOps(I)V
/* .line 517 */
v3 = this.mAppStartRecords;
if ( v3 != null) { // if-eqz v3, :cond_0
v3 = (( java.util.LinkedList ) v3 ).size ( ); // invoke-virtual {v3}, Ljava/util/LinkedList;->size()I
/* if-lez v3, :cond_0 */
/* .line 518 */
v3 = this.mSpeedTestModeState;
v4 = this.mAppStartRecords;
(( java.util.LinkedList ) v4 ).get ( v1 ); // invoke-virtual {v4, v1}, Ljava/util/LinkedList;->get(I)Ljava/lang/Object;
/* check-cast v4, Lcom/miui/server/sptm/SpeedTestModeServiceImpl$AppUsageRecord; */
v4 = this.packageName;
v5 = this.mAppStartRecords;
/* .line 519 */
(( java.util.LinkedList ) v5 ).get ( v1 ); // invoke-virtual {v5, v1}, Ljava/util/LinkedList;->get(I)Ljava/lang/Object;
/* check-cast v1, Lcom/miui/server/sptm/SpeedTestModeServiceImpl$AppUsageRecord; */
/* iget-boolean v1, v1, Lcom/miui/server/sptm/SpeedTestModeServiceImpl$AppUsageRecord;->isColdStart:Z */
/* .line 518 */
(( com.miui.server.sptm.SpeedTestModeState ) v3 ).addAppSwitchOps ( v4, v1 ); // invoke-virtual {v3, v4, v1}, Lcom/miui/server/sptm/SpeedTestModeState;->addAppSwitchOps(Ljava/lang/String;Z)V
/* .line 522 */
} // :cond_0
/* sget-boolean v1, Lcom/miui/server/sptm/SpeedTestModeServiceImpl;->DEBUG:Z */
if ( v1 != null) { // if-eqz v1, :cond_2
/* .line 523 */
/* new-instance v1, Ljava/lang/StringBuilder; */
/* invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V */
final String v3 = "handleAppSwitchingEvent: ops"; // const-string v3, "handleAppSwitchingEvent: ops"
(( java.lang.StringBuilder ) v1 ).append ( v3 ); // invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v1 ).append ( v2 ); // invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v1 ).toString ( ); // invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
final String v3 = "SPTM"; // const-string v3, "SPTM"
android.util.Slog .d ( v3,v1 );
/* .line 524 */
v1 = this.mAppStartRecords;
(( java.util.LinkedList ) v1 ).iterator ( ); // invoke-virtual {v1}, Ljava/util/LinkedList;->iterator()Ljava/util/Iterator;
v4 = } // :goto_0
if ( v4 != null) { // if-eqz v4, :cond_1
/* check-cast v4, Lcom/miui/server/sptm/SpeedTestModeServiceImpl$AppUsageRecord; */
/* .line 525 */
/* .local v4, "r":Lcom/miui/server/sptm/SpeedTestModeServiceImpl$AppUsageRecord; */
/* new-instance v5, Ljava/lang/StringBuilder; */
/* invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V */
final String v6 = "handleAppSwitchingEvent Apps: "; // const-string v6, "handleAppSwitchingEvent Apps: "
(( java.lang.StringBuilder ) v5 ).append ( v6 ); // invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( com.miui.server.sptm.SpeedTestModeServiceImpl$AppUsageRecord ) v4 ).toString ( ); // invoke-virtual {v4}, Lcom/miui/server/sptm/SpeedTestModeServiceImpl$AppUsageRecord;->toString()Ljava/lang/String;
(( java.lang.StringBuilder ) v5 ).append ( v6 ); // invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v5 ).toString ( ); // invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
android.util.Slog .e ( v3,v5 );
/* .line 526 */
} // .end local v4 # "r":Lcom/miui/server/sptm/SpeedTestModeServiceImpl$AppUsageRecord;
/* .line 527 */
} // :cond_1
/* new-instance v1, Ljava/lang/StringBuilder; */
/* invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V */
final String v4 = "handleAppSwitchingEvent Home: "; // const-string v4, "handleAppSwitchingEvent Home: "
(( java.lang.StringBuilder ) v1 ).append ( v4 ); // invoke-virtual {v1, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( com.miui.server.sptm.SpeedTestModeServiceImpl$AppUsageRecord ) p1 ).toString ( ); // invoke-virtual {p1}, Lcom/miui/server/sptm/SpeedTestModeServiceImpl$AppUsageRecord;->toString()Ljava/lang/String;
(( java.lang.StringBuilder ) v1 ).append ( v4 ); // invoke-virtual {v1, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v1 ).toString ( ); // invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
android.util.Slog .e ( v3,v1 );
/* .line 529 */
} // :cond_2
v1 = this.mAppStartRecords;
(( java.util.LinkedList ) v1 ).clear ( ); // invoke-virtual {v1}, Ljava/util/LinkedList;->clear()V
/* .line 531 */
v1 = this.packageName;
if ( v1 != null) { // if-eqz v1, :cond_3
v1 = this.mSpeedTestModeStrategies;
if ( v1 != null) { // if-eqz v1, :cond_3
/* .line 532 */
(( java.util.LinkedList ) v1 ).iterator ( ); // invoke-virtual {v1}, Ljava/util/LinkedList;->iterator()Ljava/util/Iterator;
v3 = } // :goto_1
if ( v3 != null) { // if-eqz v3, :cond_3
/* check-cast v3, Lcom/miui/server/sptm/SpeedTestModeServiceImpl$Strategy; */
/* .line 533 */
/* .local v3, "s":Lcom/miui/server/sptm/SpeedTestModeServiceImpl$Strategy; */
/* .line 534 */
} // .end local v3 # "s":Lcom/miui/server/sptm/SpeedTestModeServiceImpl$Strategy;
/* .line 536 */
} // .end local v0 # "outRes":Lcom/miui/server/sptm/PreLoadStrategy$AppStartRecord;
} // .end local v2 # "switchOps":I
} // :cond_3
/* .line 537 */
} // :cond_4
v0 = this.mAppStartRecords;
v0 = (( java.util.LinkedList ) v0 ).size ( ); // invoke-virtual {v0}, Ljava/util/LinkedList;->size()I
if ( v0 != null) { // if-eqz v0, :cond_5
v0 = this.mAppStartRecords;
/* .line 538 */
(( java.util.LinkedList ) v0 ).get ( v1 ); // invoke-virtual {v0, v1}, Ljava/util/LinkedList;->get(I)Ljava/lang/Object;
/* check-cast v0, Lcom/miui/server/sptm/SpeedTestModeServiceImpl$AppUsageRecord; */
v0 = this.packageName;
v1 = this.packageName;
/* if-ne v0, v1, :cond_6 */
/* .line 539 */
} // :cond_5
v0 = this.mAppStartRecords;
(( java.util.LinkedList ) v0 ).add ( p1 ); // invoke-virtual {v0, p1}, Ljava/util/LinkedList;->add(Ljava/lang/Object;)Z
/* .line 543 */
} // :cond_6
} // :goto_2
v0 = this.mAppStartRecords;
v0 = (( java.util.LinkedList ) v0 ).size ( ); // invoke-virtual {v0}, Ljava/util/LinkedList;->size()I
/* const/16 v1, 0x1e */
/* if-le v0, v1, :cond_7 */
/* .line 544 */
v0 = this.mAppStartRecords;
(( java.util.LinkedList ) v0 ).clear ( ); // invoke-virtual {v0}, Ljava/util/LinkedList;->clear()V
/* .line 546 */
} // :cond_7
return;
} // .end method
private void handleEvent ( Integer p0, java.lang.String p1, Long p2 ) {
/* .locals 8 */
/* .param p1, "event" # I */
/* .param p2, "packageName" # Ljava/lang/String; */
/* .param p3, "time" # J */
/* .line 471 */
int v0 = 2; // const/4 v0, 0x2
/* if-ne p1, v0, :cond_1 */
/* .line 472 */
v0 = this.mAppLastResumedTimes;
java.lang.Long .valueOf ( p3,p4 );
(( java.util.HashMap ) v0 ).put ( p2, v1 ); // invoke-virtual {v0, p2, v1}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;
/* .line 473 */
v0 = v0 = com.miui.server.sptm.SpeedTestModeServiceImpl.GAME_APPS;
if ( v0 != null) { // if-eqz v0, :cond_0
/* .line 474 */
/* sget-wide v0, Lcom/miui/server/sptm/SpeedTestModeServiceImpl;->GAME_COLD_START_DELAYED_TIME:J */
} // :cond_0
/* sget-wide v0, Lcom/miui/server/sptm/SpeedTestModeServiceImpl;->COLD_START_DELAYED_TIME:J */
/* .line 473 */
} // :goto_0
/* invoke-direct {p0, v0, v1}, Lcom/miui/server/sptm/SpeedTestModeServiceImpl;->reportAppSwitchEventWaitTimeout(J)V */
/* goto/16 :goto_3 */
/* .line 475 */
} // :cond_1
int v0 = 1; // const/4 v0, 0x1
/* if-ne p1, v0, :cond_2 */
/* .line 476 */
v0 = this.mAppStartProcTimes;
java.lang.Long .valueOf ( p3,p4 );
(( java.util.LinkedHashMap ) v0 ).put ( p2, v1 ); // invoke-virtual {v0, p2, v1}, Ljava/util/LinkedHashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;
/* goto/16 :goto_3 */
/* .line 477 */
} // :cond_2
int v1 = 3; // const/4 v1, 0x3
/* if-ne p1, v1, :cond_7 */
/* .line 478 */
v1 = this.mAppLastResumedTimes;
(( java.util.HashMap ) v1 ).remove ( p2 ); // invoke-virtual {v1, p2}, Ljava/util/HashMap;->remove(Ljava/lang/Object;)Ljava/lang/Object;
/* check-cast v1, Ljava/lang/Long; */
/* .line 479 */
/* .local v1, "resumedTime":Ljava/lang/Long; */
if ( v1 != null) { // if-eqz v1, :cond_5
/* .line 480 */
/* new-instance v2, Lcom/miui/server/sptm/SpeedTestModeServiceImpl$AppUsageRecord; */
int v3 = 0; // const/4 v3, 0x0
/* invoke-direct {v2, v3}, Lcom/miui/server/sptm/SpeedTestModeServiceImpl$AppUsageRecord;-><init>(Lcom/miui/server/sptm/SpeedTestModeServiceImpl$AppUsageRecord-IA;)V */
/* .line 481 */
/* .local v2, "r":Lcom/miui/server/sptm/SpeedTestModeServiceImpl$AppUsageRecord; */
(( java.lang.Long ) v1 ).longValue ( ); // invoke-virtual {v1}, Ljava/lang/Long;->longValue()J
/* move-result-wide v3 */
/* iput-wide v3, v2, Lcom/miui/server/sptm/SpeedTestModeServiceImpl$AppUsageRecord;->startTime:J */
/* .line 482 */
/* iput-wide p3, v2, Lcom/miui/server/sptm/SpeedTestModeServiceImpl$AppUsageRecord;->endTime:J */
/* .line 483 */
this.packageName = p2;
/* .line 486 */
v3 = this.mAppStartProcTimes;
v4 = this.packageName;
(( java.util.LinkedHashMap ) v3 ).get ( v4 ); // invoke-virtual {v3, v4}, Ljava/util/LinkedHashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;
/* check-cast v3, Ljava/lang/Long; */
/* .line 487 */
/* .local v3, "startProcTime":Ljava/lang/Long; */
if ( v3 != null) { // if-eqz v3, :cond_4
/* .line 488 */
/* iget-wide v4, v2, Lcom/miui/server/sptm/SpeedTestModeServiceImpl$AppUsageRecord;->startTime:J */
(( java.lang.Long ) v3 ).longValue ( ); // invoke-virtual {v3}, Ljava/lang/Long;->longValue()J
/* move-result-wide v6 */
/* sub-long/2addr v4, v6 */
/* sget-wide v6, Lcom/miui/server/sptm/SpeedTestModeServiceImpl;->START_PROC_DELAYED_TIME:J */
/* cmp-long v4, v4, v6 */
/* if-gez v4, :cond_3 */
/* .line 489 */
/* iput-boolean v0, v2, Lcom/miui/server/sptm/SpeedTestModeServiceImpl$AppUsageRecord;->isColdStart:Z */
/* .line 491 */
} // :cond_3
v0 = this.mAppStartProcTimes;
v4 = this.packageName;
(( java.util.LinkedHashMap ) v0 ).remove ( v4 ); // invoke-virtual {v0, v4}, Ljava/util/LinkedHashMap;->remove(Ljava/lang/Object;)Ljava/lang/Object;
/* .line 494 */
} // :cond_4
/* invoke-direct {p0, v2}, Lcom/miui/server/sptm/SpeedTestModeServiceImpl;->handleAppSwitchingEvent(Lcom/miui/server/sptm/SpeedTestModeServiceImpl$AppUsageRecord;)V */
/* .line 495 */
} // .end local v2 # "r":Lcom/miui/server/sptm/SpeedTestModeServiceImpl$AppUsageRecord;
} // .end local v3 # "startProcTime":Ljava/lang/Long;
/* .line 496 */
} // :cond_5
/* sget-boolean v0, Lcom/miui/server/sptm/SpeedTestModeServiceImpl;->DEBUG:Z */
if ( v0 != null) { // if-eqz v0, :cond_6
/* .line 497 */
java.lang.Long .valueOf ( p3,p4 );
/* filled-new-array {p2, v0}, [Ljava/lang/Object; */
final String v2 = "pkg %s has not resumed %s"; // const-string v2, "pkg %s has not resumed %s"
java.lang.String .format ( v2,v0 );
final String v2 = "SPTM"; // const-string v2, "SPTM"
android.util.Slog .e ( v2,v0 );
/* .line 499 */
} // .end local v1 # "resumedTime":Ljava/lang/Long;
} // :cond_6
} // :goto_1
} // :cond_7
int v1 = 5; // const/4 v1, 0x5
/* if-eq p1, v1, :cond_9 */
int v1 = 4; // const/4 v1, 0x4
/* if-ne p1, v1, :cond_8 */
/* .line 502 */
} // :cond_8
int v0 = 6; // const/4 v0, 0x6
/* if-ne p1, v0, :cond_a */
v0 = this.mPreLoadStrategy;
if ( v0 != null) { // if-eqz v0, :cond_a
/* .line 503 */
(( com.miui.server.sptm.PreLoadStrategy ) v0 ).onPreloadAppStarted ( p2 ); // invoke-virtual {v0, p2}, Lcom/miui/server/sptm/PreLoadStrategy;->onPreloadAppStarted(Ljava/lang/String;)V
/* .line 500 */
} // :cond_9
} // :goto_2
v1 = this.mSpeedTestModeState;
(( com.miui.server.sptm.SpeedTestModeState ) v1 ).addAppSwitchOps ( v0 ); // invoke-virtual {v1, v0}, Lcom/miui/server/sptm/SpeedTestModeState;->addAppSwitchOps(I)V
/* .line 501 */
v0 = this.mPreLoadStrategy;
(( com.miui.server.sptm.PreLoadStrategy ) v0 ).reset ( ); // invoke-virtual {v0}, Lcom/miui/server/sptm/PreLoadStrategy;->reset()V
/* .line 506 */
} // :cond_a
} // :goto_3
v0 = this.mSpeedTestModeStrategies;
(( java.util.LinkedList ) v0 ).iterator ( ); // invoke-virtual {v0}, Ljava/util/LinkedList;->iterator()Ljava/util/Iterator;
v1 = } // :goto_4
if ( v1 != null) { // if-eqz v1, :cond_b
/* check-cast v1, Lcom/miui/server/sptm/SpeedTestModeServiceImpl$Strategy; */
/* .line 507 */
/* .local v1, "st":Lcom/miui/server/sptm/SpeedTestModeServiceImpl$Strategy; */
/* .line 508 */
} // .end local v1 # "st":Lcom/miui/server/sptm/SpeedTestModeServiceImpl$Strategy;
/* .line 509 */
} // :cond_b
return;
} // .end method
private void handleUpdateSpeedTestMode ( Boolean p0 ) {
/* .locals 2 */
/* .param p1, "isEnabled" # Z */
/* .line 618 */
/* iput-boolean p1, p0, Lcom/miui/server/sptm/SpeedTestModeServiceImpl;->mIsSpeedTestMode:Z */
/* .line 619 */
v0 = this.mSpeedTestModeStrategies;
if ( v0 != null) { // if-eqz v0, :cond_0
/* .line 620 */
(( java.util.LinkedList ) v0 ).iterator ( ); // invoke-virtual {v0}, Ljava/util/LinkedList;->iterator()Ljava/util/Iterator;
v1 = } // :goto_0
if ( v1 != null) { // if-eqz v1, :cond_0
/* check-cast v1, Lcom/miui/server/sptm/SpeedTestModeServiceImpl$Strategy; */
/* .line 621 */
/* .local v1, "s":Lcom/miui/server/sptm/SpeedTestModeServiceImpl$Strategy; */
/* .line 622 */
} // .end local v1 # "s":Lcom/miui/server/sptm/SpeedTestModeServiceImpl$Strategy;
/* .line 624 */
} // :cond_0
return;
} // .end method
public static Boolean isLowMemDeviceForSpeedTestMode ( ) {
/* .locals 4 */
/* .line 400 */
/* sget-wide v0, Lcom/miui/server/sptm/SpeedTestModeServiceImpl;->TOTAL_MEMORY:J */
/* int-to-long v2, v2 */
/* cmp-long v0, v0, v2 */
/* if-gtz v0, :cond_0 */
int v0 = 1; // const/4 v0, 0x1
} // :cond_0
int v0 = 0; // const/4 v0, 0x0
} // :goto_0
} // .end method
private void registerCloudObserver ( android.content.Context p0 ) {
/* .locals 4 */
/* .param p1, "context" # Landroid/content/Context; */
/* .line 409 */
/* new-instance v0, Lcom/miui/server/sptm/SpeedTestModeServiceImpl$2; */
v1 = this.mH;
/* invoke-direct {v0, p0, v1}, Lcom/miui/server/sptm/SpeedTestModeServiceImpl$2;-><init>(Lcom/miui/server/sptm/SpeedTestModeServiceImpl;Landroid/os/Handler;)V */
/* .line 416 */
/* .local v0, "observer":Landroid/database/ContentObserver; */
(( android.content.Context ) p1 ).getContentResolver ( ); // invoke-virtual {p1}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;
/* .line 417 */
android.provider.MiuiSettings$SettingsCloudData .getCloudDataNotifyUri ( );
/* .line 416 */
int v3 = 0; // const/4 v3, 0x0
(( android.content.ContentResolver ) v1 ).registerContentObserver ( v2, v3, v0 ); // invoke-virtual {v1, v2, v3, v0}, Landroid/content/ContentResolver;->registerContentObserver(Landroid/net/Uri;ZLandroid/database/ContentObserver;)V
/* .line 419 */
return;
} // .end method
private void reportAppSwitchEventWaitTimeout ( Long p0 ) {
/* .locals 3 */
/* .param p1, "timeout" # J */
/* .line 461 */
v0 = this.mH;
/* if-nez v0, :cond_0 */
/* .line 462 */
return;
/* .line 465 */
} // :cond_0
int v1 = 4; // const/4 v1, 0x4
(( com.miui.server.sptm.SpeedTestModeServiceImpl$Handler ) v0 ).obtainMessage ( v1 ); // invoke-virtual {v0, v1}, Lcom/miui/server/sptm/SpeedTestModeServiceImpl$Handler;->obtainMessage(I)Landroid/os/Message;
/* .line 466 */
/* .local v0, "msg":Landroid/os/Message; */
v2 = this.mH;
(( com.miui.server.sptm.SpeedTestModeServiceImpl$Handler ) v2 ).removeMessages ( v1 ); // invoke-virtual {v2, v1}, Lcom/miui/server/sptm/SpeedTestModeServiceImpl$Handler;->removeMessages(I)V
/* .line 467 */
v1 = this.mH;
(( com.miui.server.sptm.SpeedTestModeServiceImpl$Handler ) v1 ).sendMessageDelayed ( v0, p1, p2 ); // invoke-virtual {v1, v0, p1, p2}, Lcom/miui/server/sptm/SpeedTestModeServiceImpl$Handler;->sendMessageDelayed(Landroid/os/Message;J)Z
/* .line 468 */
return;
} // .end method
private void setEnableSpeedTestMode ( Boolean p0 ) {
/* .locals 2 */
/* .param p1, "isEnable" # Z */
/* .line 627 */
v0 = this.mH;
if ( p1 != null) { // if-eqz p1, :cond_0
int v1 = 2; // const/4 v1, 0x2
} // :cond_0
int v1 = 3; // const/4 v1, 0x3
} // :goto_0
(( com.miui.server.sptm.SpeedTestModeServiceImpl$Handler ) v0 ).sendEmptyMessage ( v1 ); // invoke-virtual {v0, v1}, Lcom/miui/server/sptm/SpeedTestModeServiceImpl$Handler;->sendEmptyMessage(I)Z
/* .line 628 */
return;
} // .end method
private void updateCloudControlParas ( ) {
/* .locals 8 */
/* .line 422 */
final String v0 = "SPTM"; // const-string v0, "SPTM"
v1 = this.mContext;
/* .line 423 */
(( android.content.Context ) v1 ).getContentResolver ( ); // invoke-virtual {v1}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;
/* .line 422 */
final String v2 = "perf_shielder"; // const-string v2, "perf_shielder"
final String v3 = "perf_shielder_SPTM"; // const-string v3, "perf_shielder_SPTM"
final String v4 = ""; // const-string v4, ""
android.provider.MiuiSettings$SettingsCloudData .getCloudDataString ( v1,v2,v3,v4 );
/* .line 424 */
/* .local v1, "data":Ljava/lang/String; */
v2 = android.text.TextUtils .isEmpty ( v1 );
/* if-nez v2, :cond_5 */
/* .line 426 */
try { // :try_start_0
/* new-instance v2, Lorg/json/JSONObject; */
/* invoke-direct {v2, v1}, Lorg/json/JSONObject;-><init>(Ljava/lang/String;)V */
/* .line 427 */
/* .local v2, "jsonObject":Lorg/json/JSONObject; */
/* sget-boolean v3, Lcom/miui/server/sptm/SpeedTestModeServiceImpl;->IGNORE_CLOUD_ENABLE:Z */
/* if-nez v3, :cond_0 */
/* .line 428 */
final String v3 = "perf_sptm_enable_old"; // const-string v3, "perf_sptm_enable_old"
int v4 = 0; // const/4 v4, 0x0
v3 = (( org.json.JSONObject ) v2 ).optBoolean ( v3, v4 ); // invoke-virtual {v2, v3, v4}, Lorg/json/JSONObject;->optBoolean(Ljava/lang/String;Z)Z
/* iput-boolean v3, p0, Lcom/miui/server/sptm/SpeedTestModeServiceImpl;->mIsEnableOld:Z */
/* .line 429 */
final String v3 = "perf_sptm_enable_new"; // const-string v3, "perf_sptm_enable_new"
v3 = (( org.json.JSONObject ) v2 ).optBoolean ( v3, v4 ); // invoke-virtual {v2, v3, v4}, Lorg/json/JSONObject;->optBoolean(Ljava/lang/String;Z)Z
/* iput-boolean v3, p0, Lcom/miui/server/sptm/SpeedTestModeServiceImpl;->mIsEnableNew:Z */
/* .line 430 */
/* new-instance v3, Ljava/lang/StringBuilder; */
/* invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V */
final String v5 = "old and new SPTM enable cloud control received="; // const-string v5, "old and new SPTM enable cloud control received="
(( java.lang.StringBuilder ) v3 ).append ( v5 ); // invoke-virtual {v3, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
/* iget-boolean v5, p0, Lcom/miui/server/sptm/SpeedTestModeServiceImpl;->mIsEnableOld:Z */
(( java.lang.StringBuilder ) v3 ).append ( v5 ); // invoke-virtual {v3, v5}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;
final String v5 = " and "; // const-string v5, " and "
(( java.lang.StringBuilder ) v3 ).append ( v5 ); // invoke-virtual {v3, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
/* iget-boolean v5, p0, Lcom/miui/server/sptm/SpeedTestModeServiceImpl;->mIsEnableNew:Z */
(( java.lang.StringBuilder ) v3 ).append ( v5 ); // invoke-virtual {v3, v5}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v3 ).toString ( ); // invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
android.util.Slog .d ( v0,v3 );
/* .line 433 */
final String v3 = "perf_sptm_preload"; // const-string v3, "perf_sptm_preload"
v3 = (( org.json.JSONObject ) v2 ).optInt ( v3, v4 ); // invoke-virtual {v2, v3, v4}, Lorg/json/JSONObject;->optInt(Ljava/lang/String;I)I
/* iput v3, p0, Lcom/miui/server/sptm/SpeedTestModeServiceImpl;->mPreloadType:I */
/* .line 434 */
/* new-instance v3, Ljava/lang/StringBuilder; */
/* invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V */
final String v4 = "SPTM preload cloud control received="; // const-string v4, "SPTM preload cloud control received="
(( java.lang.StringBuilder ) v3 ).append ( v4 ); // invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
/* iget v4, p0, Lcom/miui/server/sptm/SpeedTestModeServiceImpl;->mPreloadType:I */
(( java.lang.StringBuilder ) v3 ).append ( v4 ); // invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v3 ).toString ( ); // invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
android.util.Slog .d ( v0,v3 );
/* .line 436 */
final String v3 = "perf_sptm_animation_enable"; // const-string v3, "perf_sptm_animation_enable"
/* .line 437 */
int v4 = 1; // const/4 v4, 0x1
v3 = (( org.json.JSONObject ) v2 ).optBoolean ( v3, v4 ); // invoke-virtual {v2, v3, v4}, Lorg/json/JSONObject;->optBoolean(Ljava/lang/String;Z)Z
/* iput-boolean v3, p0, Lcom/miui/server/sptm/SpeedTestModeServiceImpl;->mIsEnableSPTMAnimation:Z */
/* .line 438 */
/* new-instance v3, Ljava/lang/StringBuilder; */
/* invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V */
final String v4 = "SPTM animation cloud control received="; // const-string v4, "SPTM animation cloud control received="
(( java.lang.StringBuilder ) v3 ).append ( v4 ); // invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
/* iget-boolean v4, p0, Lcom/miui/server/sptm/SpeedTestModeServiceImpl;->mIsEnableSPTMAnimation:Z */
(( java.lang.StringBuilder ) v3 ).append ( v4 ); // invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v3 ).toString ( ); // invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
android.util.Slog .d ( v0,v3 );
/* .line 441 */
} // :cond_0
final String v3 = "perf_sptm_app_list"; // const-string v3, "perf_sptm_app_list"
(( org.json.JSONObject ) v2 ).optJSONArray ( v3 ); // invoke-virtual {v2, v3}, Lorg/json/JSONObject;->optJSONArray(Ljava/lang/String;)Lorg/json/JSONArray;
/* .line 442 */
/* .local v3, "appListJsonArray":Lorg/json/JSONArray; */
if ( v3 != null) { // if-eqz v3, :cond_4
/* .line 443 */
int v4 = 0; // const/4 v4, 0x0
/* .local v4, "i":I */
} // :goto_0
v5 = (( org.json.JSONArray ) v3 ).length ( ); // invoke-virtual {v3}, Lorg/json/JSONArray;->length()I
/* if-ge v4, v5, :cond_4 */
/* .line 444 */
(( org.json.JSONArray ) v3 ).getString ( v4 ); // invoke-virtual {v3, v4}, Lorg/json/JSONArray;->getString(I)Ljava/lang/String;
/* .line 445 */
/* .local v5, "appPackageName":Ljava/lang/String; */
v6 = android.text.TextUtils .isEmpty ( v5 );
if ( v6 != null) { // if-eqz v6, :cond_1
/* .line 446 */
/* .line 448 */
} // :cond_1
/* sget-boolean v6, Lcom/miui/server/sptm/SpeedTestModeServiceImpl;->DEBUG:Z */
if ( v6 != null) { // if-eqz v6, :cond_2
/* new-instance v6, Ljava/lang/StringBuilder; */
/* invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V */
final String v7 = "SPTM add app package name : "; // const-string v7, "SPTM add app package name : "
(( java.lang.StringBuilder ) v6 ).append ( v7 ); // invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v6 ).append ( v5 ); // invoke-virtual {v6, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v6 ).toString ( ); // invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
android.util.Slog .d ( v0,v6 );
/* .line 449 */
} // :cond_2
v7 = v6 = com.miui.server.sptm.SpeedTestModeServiceImpl.SPEED_TEST_APP_LIST;
/* if-nez v7, :cond_3 */
/* .line 450 */
/* :try_end_0 */
/* .catch Lorg/json/JSONException; {:try_start_0 ..:try_end_0} :catch_0 */
/* .line 443 */
} // .end local v5 # "appPackageName":Ljava/lang/String;
} // :cond_3
} // :goto_1
/* add-int/lit8 v4, v4, 0x1 */
/* .line 456 */
} // .end local v2 # "jsonObject":Lorg/json/JSONObject;
} // .end local v3 # "appListJsonArray":Lorg/json/JSONArray;
} // .end local v4 # "i":I
} // :cond_4
/* .line 454 */
/* :catch_0 */
/* move-exception v2 */
/* .line 455 */
/* .local v2, "e":Lorg/json/JSONException; */
/* const-string/jumbo v3, "updateCloudData error :" */
android.util.Slog .e ( v0,v3,v2 );
/* .line 458 */
} // .end local v2 # "e":Lorg/json/JSONException;
} // :cond_5
} // :goto_2
return;
} // .end method
/* # virtual methods */
public Boolean getAnimationCloudEnable ( ) {
/* .locals 1 */
/* .line 690 */
/* iget-boolean v0, p0, Lcom/miui/server/sptm/SpeedTestModeServiceImpl;->mIsEnableSPTMAnimation:Z */
} // .end method
public Integer getPreloadCloudType ( ) {
/* .locals 1 */
/* .line 682 */
/* iget v0, p0, Lcom/miui/server/sptm/SpeedTestModeServiceImpl;->mPreloadType:I */
} // .end method
public Boolean getSPTMCloudEnable ( ) {
/* .locals 1 */
/* .line 686 */
/* iget-boolean v0, p0, Lcom/miui/server/sptm/SpeedTestModeServiceImpl;->mIsEnableOld:Z */
/* if-nez v0, :cond_1 */
/* iget-boolean v0, p0, Lcom/miui/server/sptm/SpeedTestModeServiceImpl;->mIsEnableNew:Z */
if ( v0 != null) { // if-eqz v0, :cond_0
} // :cond_0
int v0 = 0; // const/4 v0, 0x0
} // :cond_1
} // :goto_0
int v0 = 1; // const/4 v0, 0x1
} // :goto_1
} // .end method
public Boolean getSPTMCloudEnableNew ( ) {
/* .locals 1 */
/* .line 694 */
/* iget-boolean v0, p0, Lcom/miui/server/sptm/SpeedTestModeServiceImpl;->mIsEnableNew:Z */
} // .end method
public Float getWindowStateAnimationScaleOverride ( ) {
/* .locals 1 */
/* .line 309 */
v0 = this.mHomeAnimationStrategy;
if ( v0 != null) { // if-eqz v0, :cond_0
/* .line 310 */
v0 = (( com.miui.server.sptm.HomeAnimationStrategy ) v0 ).getWindowAnimatorDurationOverride ( ); // invoke-virtual {v0}, Lcom/miui/server/sptm/HomeAnimationStrategy;->getWindowAnimatorDurationOverride()F
/* .line 312 */
} // :cond_0
/* const/high16 v0, 0x3f800000 # 1.0f */
} // .end method
public void init ( android.content.Context p0 ) {
/* .locals 4 */
/* .param p1, "context" # Landroid/content/Context; */
/* .line 262 */
/* iget-boolean v0, p0, Lcom/miui/server/sptm/SpeedTestModeServiceImpl;->mIsEnableOld:Z */
/* xor-int/lit8 v0, v0, 0x1 */
/* iget-boolean v1, p0, Lcom/miui/server/sptm/SpeedTestModeServiceImpl;->mIsEnableNew:Z */
/* xor-int/lit8 v1, v1, 0x1 */
/* and-int/2addr v0, v1 */
if ( v0 != null) { // if-eqz v0, :cond_0
/* .line 263 */
return;
/* .line 265 */
} // :cond_0
/* sget-wide v0, Lcom/miui/server/sptm/SpeedTestModeServiceImpl;->TOTAL_MEMORY:J */
/* const-wide/16 v2, 0x1770 */
/* cmp-long v0, v0, v2 */
/* if-gtz v0, :cond_1 */
/* .line 266 */
return;
/* .line 268 */
} // :cond_1
this.mContext = p1;
/* .line 269 */
/* new-instance v0, Lcom/miui/server/sptm/SpeedTestModeController; */
/* new-instance v1, Lcom/miui/server/sptm/SpeedTestModeServiceImpl$$ExternalSyntheticLambda0; */
/* invoke-direct {v1, p0}, Lcom/miui/server/sptm/SpeedTestModeServiceImpl$$ExternalSyntheticLambda0;-><init>(Lcom/miui/server/sptm/SpeedTestModeServiceImpl;)V */
/* invoke-direct {v0, v1}, Lcom/miui/server/sptm/SpeedTestModeController;-><init>(Lcom/miui/server/sptm/SpeedTestModeController$OnSpeedTestModeChangeListener;)V */
/* .line 271 */
/* .local v0, "speedTestModeController":Lcom/miui/server/sptm/SpeedTestModeController; */
/* new-instance v1, Lcom/miui/server/sptm/SpeedTestModeState; */
/* invoke-direct {v1, v0}, Lcom/miui/server/sptm/SpeedTestModeState;-><init>(Lcom/miui/server/sptm/SpeedTestModeController;)V */
this.mSpeedTestModeState = v1;
/* .line 273 */
/* new-instance v1, Landroid/os/HandlerThread; */
final String v2 = "SPTModeServiceTh"; // const-string v2, "SPTModeServiceTh"
int v3 = -2; // const/4 v3, -0x2
/* invoke-direct {v1, v2, v3}, Landroid/os/HandlerThread;-><init>(Ljava/lang/String;I)V */
this.mHandlerThread = v1;
/* .line 274 */
(( android.os.HandlerThread ) v1 ).start ( ); // invoke-virtual {v1}, Landroid/os/HandlerThread;->start()V
/* .line 275 */
/* new-instance v1, Lcom/miui/server/sptm/SpeedTestModeServiceImpl$Handler; */
v2 = this.mHandlerThread;
(( android.os.HandlerThread ) v2 ).getLooper ( ); // invoke-virtual {v2}, Landroid/os/HandlerThread;->getLooper()Landroid/os/Looper;
/* invoke-direct {v1, p0, v2}, Lcom/miui/server/sptm/SpeedTestModeServiceImpl$Handler;-><init>(Lcom/miui/server/sptm/SpeedTestModeServiceImpl;Landroid/os/Looper;)V */
this.mH = v1;
/* .line 277 */
v1 = this.mContext;
/* invoke-direct {p0, v1}, Lcom/miui/server/sptm/SpeedTestModeServiceImpl;->registerCloudObserver(Landroid/content/Context;)V */
/* .line 279 */
v1 = this.mSpeedTestModeStrategies;
v1 = (( java.util.LinkedList ) v1 ).size ( ); // invoke-virtual {v1}, Ljava/util/LinkedList;->size()I
/* if-nez v1, :cond_2 */
/* .line 280 */
/* new-instance v1, Lcom/miui/server/sptm/HomeAnimationStrategy; */
/* invoke-direct {v1, p1}, Lcom/miui/server/sptm/HomeAnimationStrategy;-><init>(Landroid/content/Context;)V */
this.mHomeAnimationStrategy = v1;
/* .line 281 */
v2 = this.mSpeedTestModeStrategies;
(( java.util.LinkedList ) v2 ).add ( v1 ); // invoke-virtual {v2, v1}, Ljava/util/LinkedList;->add(Ljava/lang/Object;)Z
/* .line 282 */
v1 = this.mSpeedTestModeStrategies;
/* new-instance v2, Lcom/miui/server/sptm/MemoryOptimizeStrategy; */
/* invoke-direct {v2}, Lcom/miui/server/sptm/MemoryOptimizeStrategy;-><init>()V */
(( java.util.LinkedList ) v1 ).add ( v2 ); // invoke-virtual {v1, v2}, Ljava/util/LinkedList;->add(Ljava/lang/Object;)Z
/* .line 283 */
v1 = this.mSpeedTestModeStrategies;
/* new-instance v2, Lcom/miui/server/sptm/GreezeStrategy; */
/* invoke-direct {v2}, Lcom/miui/server/sptm/GreezeStrategy;-><init>()V */
(( java.util.LinkedList ) v1 ).add ( v2 ); // invoke-virtual {v1, v2}, Ljava/util/LinkedList;->add(Ljava/lang/Object;)Z
/* .line 284 */
/* new-instance v1, Lcom/miui/server/sptm/PreLoadStrategy; */
/* invoke-direct {v1}, Lcom/miui/server/sptm/PreLoadStrategy;-><init>()V */
this.mPreLoadStrategy = v1;
/* .line 285 */
v2 = this.mSpeedTestModeStrategies;
(( java.util.LinkedList ) v2 ).add ( v1 ); // invoke-virtual {v2, v1}, Ljava/util/LinkedList;->add(Ljava/lang/Object;)Z
/* .line 287 */
} // :cond_2
return;
} // .end method
public Boolean isSpeedTestMode ( ) {
/* .locals 1 */
/* .line 405 */
/* iget-boolean v0, p0, Lcom/miui/server/sptm/SpeedTestModeServiceImpl;->mIsSpeedTestMode:Z */
} // .end method
public void onBootPhase ( ) {
/* .locals 4 */
/* .line 291 */
/* iget-boolean v0, p0, Lcom/miui/server/sptm/SpeedTestModeServiceImpl;->mIsEnableOld:Z */
/* xor-int/lit8 v0, v0, 0x1 */
/* iget-boolean v1, p0, Lcom/miui/server/sptm/SpeedTestModeServiceImpl;->mIsEnableNew:Z */
/* xor-int/lit8 v1, v1, 0x1 */
/* and-int/2addr v0, v1 */
if ( v0 != null) { // if-eqz v0, :cond_0
/* .line 292 */
return;
/* .line 294 */
} // :cond_0
/* sget-wide v0, Lcom/miui/server/sptm/SpeedTestModeServiceImpl;->TOTAL_MEMORY:J */
/* const-wide/16 v2, 0x1770 */
/* cmp-long v0, v0, v2 */
/* if-gtz v0, :cond_1 */
/* .line 295 */
return;
/* .line 297 */
} // :cond_1
/* invoke-direct {p0}, Lcom/miui/server/sptm/SpeedTestModeServiceImpl;->updateCloudControlParas()V */
/* .line 298 */
return;
} // .end method
public void reportAppUsageEvents ( Integer p0, java.lang.String p1 ) {
/* .locals 5 */
/* .param p1, "usageEventCode" # I */
/* .param p2, "packageName" # Ljava/lang/String; */
/* .line 322 */
v0 = this.mH;
if ( v0 != null) { // if-eqz v0, :cond_4
v0 = android.text.TextUtils .isEmpty ( p2 );
if ( v0 != null) { // if-eqz v0, :cond_0
/* .line 325 */
} // :cond_0
v0 = v0 = com.miui.server.sptm.SpeedTestModeServiceImpl.PERMISSION_DIALOG_PACKAGE_NAMES;
if ( v0 != null) { // if-eqz v0, :cond_1
/* .line 326 */
return;
/* .line 330 */
} // :cond_1
/* sparse-switch p1, :sswitch_data_0 */
/* .line 342 */
return;
/* .line 338 */
/* :sswitch_0 */
int v0 = 5; // const/4 v0, 0x5
/* .line 339 */
/* .local v0, "event":I */
/* .line 335 */
} // .end local v0 # "event":I
/* :sswitch_1 */
int v0 = 3; // const/4 v0, 0x3
/* .line 336 */
/* .restart local v0 # "event":I */
/* .line 332 */
} // .end local v0 # "event":I
/* :sswitch_2 */
int v0 = 2; // const/4 v0, 0x2
/* .line 333 */
/* .restart local v0 # "event":I */
/* nop */
/* .line 346 */
} // :goto_0
com.android.server.am.PreloadAppControllerStub .get ( );
/* .line 347 */
/* .local v1, "preloadController":Lcom/android/server/am/PreloadAppControllerStub; */
if ( v1 != null) { // if-eqz v1, :cond_3
v2 = /* .line 348 */
if ( v2 != null) { // if-eqz v2, :cond_3
/* .line 349 */
/* sget-boolean v2, Lcom/miui/server/sptm/SpeedTestModeServiceImpl;->DEBUG:Z */
if ( v2 != null) { // if-eqz v2, :cond_2
/* .line 350 */
/* nop */
/* .line 351 */
java.lang.Integer .valueOf ( v0 );
/* filled-new-array {p2, v2}, [Ljava/lang/Object; */
/* .line 350 */
/* const-string/jumbo v3, "skip %s code: %s because preloading" */
java.lang.String .format ( v3,v2 );
final String v3 = "SPTM"; // const-string v3, "SPTM"
android.util.Slog .e ( v3,v2 );
/* .line 353 */
} // :cond_2
return;
/* .line 356 */
} // :cond_3
v2 = this.mH;
int v3 = 1; // const/4 v3, 0x1
(( com.miui.server.sptm.SpeedTestModeServiceImpl$Handler ) v2 ).obtainMessage ( v3 ); // invoke-virtual {v2, v3}, Lcom/miui/server/sptm/SpeedTestModeServiceImpl$Handler;->obtainMessage(I)Landroid/os/Message;
/* .line 357 */
/* .local v2, "msg":Landroid/os/Message; */
android.os.SystemClock .uptimeMillis ( );
/* move-result-wide v3 */
com.miui.server.sptm.SpeedTestModeServiceImpl .createMsgData ( v0,p2,v3,v4 );
(( android.os.Message ) v2 ).setData ( v3 ); // invoke-virtual {v2, v3}, Landroid/os/Message;->setData(Landroid/os/Bundle;)V
/* .line 358 */
v3 = this.mH;
(( com.miui.server.sptm.SpeedTestModeServiceImpl$Handler ) v3 ).sendMessage ( v2 ); // invoke-virtual {v3, v2}, Lcom/miui/server/sptm/SpeedTestModeServiceImpl$Handler;->sendMessage(Landroid/os/Message;)Z
/* .line 359 */
return;
/* .line 323 */
} // .end local v0 # "event":I
} // .end local v1 # "preloadController":Lcom/android/server/am/PreloadAppControllerStub;
} // .end local v2 # "msg":Landroid/os/Message;
} // :cond_4
} // :goto_1
return;
/* nop */
/* :sswitch_data_0 */
/* .sparse-switch */
/* 0x1 -> :sswitch_2 */
/* 0x2 -> :sswitch_1 */
/* 0x11 -> :sswitch_0 */
} // .end sparse-switch
} // .end method
public void reportOneKeyCleanEvent ( ) {
/* .locals 5 */
/* .line 363 */
v0 = this.mH;
/* if-nez v0, :cond_0 */
/* .line 364 */
return;
/* .line 367 */
} // :cond_0
int v1 = 1; // const/4 v1, 0x1
(( com.miui.server.sptm.SpeedTestModeServiceImpl$Handler ) v0 ).obtainMessage ( v1 ); // invoke-virtual {v0, v1}, Lcom/miui/server/sptm/SpeedTestModeServiceImpl$Handler;->obtainMessage(I)Landroid/os/Message;
/* .line 368 */
/* .local v0, "msg":Landroid/os/Message; */
/* nop */
/* .line 369 */
android.os.SystemClock .uptimeMillis ( );
/* move-result-wide v1 */
/* .line 368 */
int v3 = 4; // const/4 v3, 0x4
final String v4 = ""; // const-string v4, ""
com.miui.server.sptm.SpeedTestModeServiceImpl .createMsgData ( v3,v4,v1,v2 );
(( android.os.Message ) v0 ).setData ( v1 ); // invoke-virtual {v0, v1}, Landroid/os/Message;->setData(Landroid/os/Bundle;)V
/* .line 370 */
v1 = this.mH;
(( com.miui.server.sptm.SpeedTestModeServiceImpl$Handler ) v1 ).sendMessage ( v0 ); // invoke-virtual {v1, v0}, Lcom/miui/server/sptm/SpeedTestModeServiceImpl$Handler;->sendMessage(Landroid/os/Message;)Z
/* .line 371 */
return;
} // .end method
public void reportPreloadAppStart ( java.lang.String p0 ) {
/* .locals 4 */
/* .param p1, "packageName" # Ljava/lang/String; */
/* .line 389 */
v0 = this.mH;
/* if-nez v0, :cond_0 */
/* .line 390 */
return;
/* .line 393 */
} // :cond_0
int v1 = 1; // const/4 v1, 0x1
(( com.miui.server.sptm.SpeedTestModeServiceImpl$Handler ) v0 ).obtainMessage ( v1 ); // invoke-virtual {v0, v1}, Lcom/miui/server/sptm/SpeedTestModeServiceImpl$Handler;->obtainMessage(I)Landroid/os/Message;
/* .line 394 */
/* .local v0, "msg":Landroid/os/Message; */
/* nop */
/* .line 395 */
android.os.SystemClock .uptimeMillis ( );
/* move-result-wide v1 */
/* .line 394 */
int v3 = 6; // const/4 v3, 0x6
com.miui.server.sptm.SpeedTestModeServiceImpl .createMsgData ( v3,p1,v1,v2 );
(( android.os.Message ) v0 ).setData ( v1 ); // invoke-virtual {v0, v1}, Landroid/os/Message;->setData(Landroid/os/Bundle;)V
/* .line 396 */
v1 = this.mH;
(( com.miui.server.sptm.SpeedTestModeServiceImpl$Handler ) v1 ).sendMessage ( v0 ); // invoke-virtual {v1, v0}, Lcom/miui/server/sptm/SpeedTestModeServiceImpl$Handler;->sendMessage(Landroid/os/Message;)Z
/* .line 397 */
return;
} // .end method
public void reportStartProcEvent ( java.lang.String p0, java.lang.String p1 ) {
/* .locals 4 */
/* .param p1, "packageName" # Ljava/lang/String; */
/* .param p2, "procName" # Ljava/lang/String; */
/* .line 375 */
v0 = this.mH;
if ( v0 != null) { // if-eqz v0, :cond_2
v0 = android.text.TextUtils .isEmpty ( p1 );
/* if-nez v0, :cond_2 */
v0 = android.text.TextUtils .isEmpty ( p2 );
if ( v0 != null) { // if-eqz v0, :cond_0
/* .line 380 */
} // :cond_0
v0 = (( java.lang.String ) p1 ).equals ( p2 ); // invoke-virtual {p1, p2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z
if ( v0 != null) { // if-eqz v0, :cond_1
/* .line 381 */
v0 = this.mH;
int v1 = 1; // const/4 v1, 0x1
(( com.miui.server.sptm.SpeedTestModeServiceImpl$Handler ) v0 ).obtainMessage ( v1 ); // invoke-virtual {v0, v1}, Lcom/miui/server/sptm/SpeedTestModeServiceImpl$Handler;->obtainMessage(I)Landroid/os/Message;
/* .line 382 */
/* .local v0, "msg":Landroid/os/Message; */
/* nop */
/* .line 383 */
android.os.SystemClock .uptimeMillis ( );
/* move-result-wide v2 */
/* .line 382 */
com.miui.server.sptm.SpeedTestModeServiceImpl .createMsgData ( v1,p1,v2,v3 );
(( android.os.Message ) v0 ).setData ( v1 ); // invoke-virtual {v0, v1}, Landroid/os/Message;->setData(Landroid/os/Bundle;)V
/* .line 384 */
v1 = this.mH;
(( com.miui.server.sptm.SpeedTestModeServiceImpl$Handler ) v1 ).sendMessage ( v0 ); // invoke-virtual {v1, v0}, Lcom/miui/server/sptm/SpeedTestModeServiceImpl$Handler;->sendMessage(Landroid/os/Message;)Z
/* .line 386 */
} // .end local v0 # "msg":Landroid/os/Message;
} // :cond_1
return;
/* .line 376 */
} // :cond_2
} // :goto_0
return;
} // .end method
public void setSPTModeEnabled ( Boolean p0 ) {
/* .locals 0 */
/* .param p1, "isEnable" # Z */
/* .line 317 */
/* iput-boolean p1, p0, Lcom/miui/server/sptm/SpeedTestModeServiceImpl;->mIsSpeedTestEnabled:Z */
/* .line 318 */
return;
} // .end method
public void updateHomeProcess ( java.lang.String p0 ) {
/* .locals 1 */
/* .param p1, "homePackageName" # Ljava/lang/String; */
/* .line 302 */
v0 = android.text.TextUtils .isEmpty ( p1 );
/* if-nez v0, :cond_0 */
/* .line 303 */
this.mHomePackageName = p1;
/* .line 305 */
} // :cond_0
return;
} // .end method
