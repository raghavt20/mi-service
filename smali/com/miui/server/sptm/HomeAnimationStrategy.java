class com.miui.server.sptm.HomeAnimationStrategy implements com.miui.server.sptm.SpeedTestModeServiceImpl$Strategy {
	 /* .source "HomeAnimationStrategy.java" */
	 /* # interfaces */
	 /* # static fields */
	 private static final Double HOME_ANIMATION_RATIO_DELTA;
	 private static final java.lang.String KEY_ANIMATION_RATIO;
	 private static final Double MAX_HOME_ANIMATION_RATIO;
	 private static final Double MIN_HOME_ANIMATION_RATIO;
	 /* # instance fields */
	 private Integer mAppStartedCountInSPTMode;
	 private android.content.Context mContext;
	 private volatile Integer mContinuesSPTCount;
	 private Double mCurHomeAnimationRatio;
	 private Boolean mIsInSpeedTestMode;
	 private com.miui.server.sptm.SpeedTestModeServiceImpl mSpeedTestModeService;
	 private java.util.HashSet mStartedAppInLastRound;
	 /* .annotation system Ldalvik/annotation/Signature; */
	 /* value = { */
	 /* "Ljava/util/HashSet<", */
	 /* "Ljava/lang/String;", */
	 /* ">;" */
	 /* } */
} // .end annotation
} // .end field
/* # direct methods */
public com.miui.server.sptm.HomeAnimationStrategy ( ) {
/* .locals 3 */
/* .param p1, "context" # Landroid/content/Context; */
/* .line 27 */
/* invoke-direct {p0}, Ljava/lang/Object;-><init>()V */
/* .line 12 */
/* nop */
/* .line 13 */
com.miui.server.sptm.SpeedTestModeServiceImpl .getInstance ( );
this.mSpeedTestModeService = v0;
/* .line 20 */
/* const-wide/high16 v0, 0x3ff0000000000000L # 1.0 */
/* iput-wide v0, p0, Lcom/miui/server/sptm/HomeAnimationStrategy;->mCurHomeAnimationRatio:D */
/* .line 21 */
int v2 = 0; // const/4 v2, 0x0
/* iput v2, p0, Lcom/miui/server/sptm/HomeAnimationStrategy;->mContinuesSPTCount:I */
/* .line 22 */
/* iput v2, p0, Lcom/miui/server/sptm/HomeAnimationStrategy;->mAppStartedCountInSPTMode:I */
/* .line 23 */
/* iput-boolean v2, p0, Lcom/miui/server/sptm/HomeAnimationStrategy;->mIsInSpeedTestMode:Z */
/* .line 25 */
/* new-instance v2, Ljava/util/HashSet; */
/* invoke-direct {v2}, Ljava/util/HashSet;-><init>()V */
this.mStartedAppInLastRound = v2;
/* .line 28 */
this.mContext = p1;
/* .line 29 */
java.lang.Double .valueOf ( v0,v1 );
/* invoke-direct {p0, v0}, Lcom/miui/server/sptm/HomeAnimationStrategy;->setAnimationRatio(Ljava/lang/Double;)V */
/* .line 30 */
return;
} // .end method
private synchronized Integer getContinuesSPTCount ( ) {
/* .locals 1 */
/* monitor-enter p0 */
/* .line 107 */
try { // :try_start_0
	 /* iget v0, p0, Lcom/miui/server/sptm/HomeAnimationStrategy;->mContinuesSPTCount:I */
	 /* :try_end_0 */
	 /* .catchall {:try_start_0 ..:try_end_0} :catchall_0 */
	 /* monitor-exit p0 */
	 /* .line 107 */
} // .end local p0 # "this":Lcom/miui/server/sptm/HomeAnimationStrategy;
/* :catchall_0 */
/* move-exception v0 */
/* monitor-exit p0 */
/* throw v0 */
} // .end method
private static Double limitHomeAnimationRatio ( Double p0 ) {
/* .locals 4 */
/* .param p0, "radio" # D */
/* .line 93 */
/* const-wide/high16 v0, 0x3fe0000000000000L # 0.5 */
java.lang.Math .max ( v0,v1,p0,p1 );
/* move-result-wide v0 */
/* const-wide/high16 v2, 0x3ff0000000000000L # 1.0 */
java.lang.Math .min ( v2,v3,v0,v1 );
/* move-result-wide v0 */
/* return-wide v0 */
} // .end method
private void setAnimationRatio ( java.lang.Double p0 ) {
/* .locals 4 */
/* .param p1, "value" # Ljava/lang/Double; */
/* .line 97 */
v0 = this.mSpeedTestModeService;
v0 = (( com.miui.server.sptm.SpeedTestModeServiceImpl ) v0 ).getAnimationCloudEnable ( ); // invoke-virtual {v0}, Lcom/miui/server/sptm/SpeedTestModeServiceImpl;->getAnimationCloudEnable()Z
/* const-string/jumbo v1, "transition_animation_duration_ratio" */
if ( v0 != null) { // if-eqz v0, :cond_0
	 /* .line 98 */
	 v0 = this.mContext;
	 (( android.content.Context ) v0 ).getContentResolver ( ); // invoke-virtual {v0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;
	 /* .line 99 */
	 java.lang.String .valueOf ( p1 );
	 /* .line 98 */
	 android.provider.Settings$Global .putString ( v0,v1,v2 );
	 /* .line 101 */
} // :cond_0
v0 = this.mContext;
(( android.content.Context ) v0 ).getContentResolver ( ); // invoke-virtual {v0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;
/* .line 102 */
/* const-wide/high16 v2, 0x3ff0000000000000L # 1.0 */
java.lang.String .valueOf ( v2,v3 );
/* .line 101 */
android.provider.Settings$Global .putString ( v0,v1,v2 );
/* .line 104 */
} // :goto_0
return;
} // .end method
private synchronized void setContinuesSPTCount ( Integer p0 ) {
/* .locals 0 */
/* .param p1, "count" # I */
/* monitor-enter p0 */
/* .line 111 */
try { // :try_start_0
/* iput p1, p0, Lcom/miui/server/sptm/HomeAnimationStrategy;->mContinuesSPTCount:I */
/* :try_end_0 */
/* .catchall {:try_start_0 ..:try_end_0} :catchall_0 */
/* .line 112 */
/* monitor-exit p0 */
return;
/* .line 110 */
} // .end local p0 # "this":Lcom/miui/server/sptm/HomeAnimationStrategy;
} // .end local p1 # "count":I
/* :catchall_0 */
/* move-exception p1 */
/* monitor-exit p0 */
/* throw p1 */
} // .end method
/* # virtual methods */
public Float getWindowAnimatorDurationOverride ( ) {
/* .locals 1 */
/* .line 85 */
v0 = /* invoke-direct {p0}, Lcom/miui/server/sptm/HomeAnimationStrategy;->getContinuesSPTCount()I */
/* if-nez v0, :cond_1 */
/* iget-boolean v0, p0, Lcom/miui/server/sptm/HomeAnimationStrategy;->mIsInSpeedTestMode:Z */
if ( v0 != null) { // if-eqz v0, :cond_0
/* .line 88 */
} // :cond_0
/* const/high16 v0, 0x3f800000 # 1.0f */
/* .line 86 */
} // :cond_1
} // :goto_0
/* const v0, 0x3e99999a # 0.3f */
} // .end method
public void onAppStarted ( com.miui.server.sptm.PreLoadStrategy$AppStartRecord p0 ) {
/* .locals 4 */
/* .param p1, "r" # Lcom/miui/server/sptm/PreLoadStrategy$AppStartRecord; */
/* .line 42 */
/* iget-boolean v0, p0, Lcom/miui/server/sptm/HomeAnimationStrategy;->mIsInSpeedTestMode:Z */
if ( v0 != null) { // if-eqz v0, :cond_0
/* .line 43 */
/* iget v0, p0, Lcom/miui/server/sptm/HomeAnimationStrategy;->mAppStartedCountInSPTMode:I */
/* add-int/lit8 v0, v0, 0x1 */
/* iput v0, p0, Lcom/miui/server/sptm/HomeAnimationStrategy;->mAppStartedCountInSPTMode:I */
/* .line 44 */
/* nop */
/* .line 45 */
v1 = /* invoke-direct {p0}, Lcom/miui/server/sptm/HomeAnimationStrategy;->getContinuesSPTCount()I */
/* add-int/2addr v0, v1 */
/* int-to-double v0, v0 */
/* const-wide/high16 v2, 0x3fd0000000000000L # 0.25 */
/* mul-double/2addr v0, v2 */
/* const-wide/high16 v2, 0x3ff0000000000000L # 1.0 */
/* sub-double/2addr v2, v0 */
/* .line 44 */
com.miui.server.sptm.HomeAnimationStrategy .limitHomeAnimationRatio ( v2,v3 );
/* move-result-wide v0 */
/* iput-wide v0, p0, Lcom/miui/server/sptm/HomeAnimationStrategy;->mCurHomeAnimationRatio:D */
/* .line 46 */
java.lang.Double .valueOf ( v0,v1 );
/* invoke-direct {p0, v0}, Lcom/miui/server/sptm/HomeAnimationStrategy;->setAnimationRatio(Ljava/lang/Double;)V */
/* .line 49 */
} // :cond_0
/* iget-boolean v0, p1, Lcom/miui/server/sptm/PreLoadStrategy$AppStartRecord;->isColdStart:Z */
if ( v0 != null) { // if-eqz v0, :cond_1
v0 = com.miui.server.sptm.SpeedTestModeServiceImpl.SPEED_TEST_APP_LIST;
v0 = v1 = this.packageName;
if ( v0 != null) { // if-eqz v0, :cond_1
/* .line 50 */
v0 = this.mStartedAppInLastRound;
v1 = this.packageName;
(( java.util.HashSet ) v0 ).add ( v1 ); // invoke-virtual {v0, v1}, Ljava/util/HashSet;->add(Ljava/lang/Object;)Z
/* .line 52 */
} // :cond_1
return;
} // .end method
public void onNewEvent ( Integer p0 ) {
/* .locals 2 */
/* .param p1, "eventType" # I */
/* .line 34 */
int v0 = 5; // const/4 v0, 0x5
/* if-ne p1, v0, :cond_0 */
v0 = /* invoke-direct {p0}, Lcom/miui/server/sptm/HomeAnimationStrategy;->getContinuesSPTCount()I */
if ( v0 != null) { // if-eqz v0, :cond_0
/* .line 35 */
/* const-wide/high16 v0, 0x3ff0000000000000L # 1.0 */
java.lang.Double .valueOf ( v0,v1 );
/* invoke-direct {p0, v0}, Lcom/miui/server/sptm/HomeAnimationStrategy;->setAnimationRatio(Ljava/lang/Double;)V */
/* .line 36 */
int v0 = 0; // const/4 v0, 0x0
/* invoke-direct {p0, v0}, Lcom/miui/server/sptm/HomeAnimationStrategy;->setContinuesSPTCount(I)V */
/* .line 38 */
} // :cond_0
return;
} // .end method
public void onSpeedTestModeChanged ( Boolean p0 ) {
/* .locals 7 */
/* .param p1, "isEnable" # Z */
/* .line 56 */
int v0 = 0; // const/4 v0, 0x0
/* iput v0, p0, Lcom/miui/server/sptm/HomeAnimationStrategy;->mAppStartedCountInSPTMode:I */
/* .line 57 */
/* iput-boolean p1, p0, Lcom/miui/server/sptm/HomeAnimationStrategy;->mIsInSpeedTestMode:Z */
/* .line 59 */
/* if-nez p1, :cond_4 */
/* .line 61 */
v1 = this.mSpeedTestModeService;
v1 = (( com.miui.server.sptm.SpeedTestModeServiceImpl ) v1 ).getPreloadCloudType ( ); // invoke-virtual {v1}, Lcom/miui/server/sptm/SpeedTestModeServiceImpl;->getPreloadCloudType()I
/* .line 62 */
/* .local v1, "speedTestType":I */
v2 = this.mStartedAppInLastRound;
v2 = (( java.util.HashSet ) v2 ).size ( ); // invoke-virtual {v2}, Ljava/util/HashSet;->size()I
/* .line 63 */
/* .local v2, "startedAppCount":I */
int v3 = 3; // const/4 v3, 0x3
/* if-le v1, v3, :cond_0 */
/* if-ge v2, v4, :cond_1 */
} // :cond_0
/* if-gt v1, v3, :cond_2 */
/* const/16 v3, 0xe */
/* if-lt v2, v3, :cond_2 */
/* .line 66 */
} // :cond_1
v0 = /* invoke-direct {p0}, Lcom/miui/server/sptm/HomeAnimationStrategy;->getContinuesSPTCount()I */
/* add-int/lit8 v0, v0, 0x1 */
/* invoke-direct {p0, v0}, Lcom/miui/server/sptm/HomeAnimationStrategy;->setContinuesSPTCount(I)V */
/* .line 68 */
} // :cond_2
/* invoke-direct {p0, v0}, Lcom/miui/server/sptm/HomeAnimationStrategy;->setContinuesSPTCount(I)V */
/* .line 70 */
} // :goto_0
/* sget-boolean v0, Lcom/miui/server/sptm/SpeedTestModeServiceImpl;->DEBUG:Z */
if ( v0 != null) { // if-eqz v0, :cond_3
/* .line 71 */
v0 = this.mStartedAppInLastRound;
/* .line 73 */
v0 = (( java.util.HashSet ) v0 ).size ( ); // invoke-virtual {v0}, Ljava/util/HashSet;->size()I
java.lang.Integer .valueOf ( v0 );
v3 = /* invoke-direct {p0}, Lcom/miui/server/sptm/HomeAnimationStrategy;->getContinuesSPTCount()I */
java.lang.Integer .valueOf ( v3 );
/* filled-new-array {v0, v3}, [Ljava/lang/Object; */
/* .line 71 */
final String v3 = "In last round, started: %s, curRound: %s"; // const-string v3, "In last round, started: %s, curRound: %s"
java.lang.String .format ( v3,v0 );
final String v3 = "SPTM"; // const-string v3, "SPTM"
android.util.Slog .d ( v3,v0 );
/* .line 75 */
} // :cond_3
v0 = this.mStartedAppInLastRound;
(( java.util.HashSet ) v0 ).clear ( ); // invoke-virtual {v0}, Ljava/util/HashSet;->clear()V
/* .line 78 */
/* nop */
/* .line 79 */
v0 = /* invoke-direct {p0}, Lcom/miui/server/sptm/HomeAnimationStrategy;->getContinuesSPTCount()I */
/* int-to-double v3, v0 */
/* const-wide/high16 v5, 0x3fd0000000000000L # 0.25 */
/* mul-double/2addr v3, v5 */
/* const-wide/high16 v5, 0x3ff0000000000000L # 1.0 */
/* sub-double/2addr v5, v3 */
/* .line 78 */
com.miui.server.sptm.HomeAnimationStrategy .limitHomeAnimationRatio ( v5,v6 );
/* move-result-wide v3 */
/* iput-wide v3, p0, Lcom/miui/server/sptm/HomeAnimationStrategy;->mCurHomeAnimationRatio:D */
/* .line 80 */
java.lang.Double .valueOf ( v3,v4 );
/* invoke-direct {p0, v0}, Lcom/miui/server/sptm/HomeAnimationStrategy;->setAnimationRatio(Ljava/lang/Double;)V */
/* .line 82 */
} // .end local v1 # "speedTestType":I
} // .end local v2 # "startedAppCount":I
} // :cond_4
return;
} // .end method
