.class Lcom/miui/server/sptm/HomeAnimationStrategy;
.super Ljava/lang/Object;
.source "HomeAnimationStrategy.java"

# interfaces
.implements Lcom/miui/server/sptm/SpeedTestModeServiceImpl$Strategy;


# static fields
.field private static final HOME_ANIMATION_RATIO_DELTA:D = 0.25

.field private static final KEY_ANIMATION_RATIO:Ljava/lang/String; = "transition_animation_duration_ratio"

.field private static final MAX_HOME_ANIMATION_RATIO:D = 1.0

.field private static final MIN_HOME_ANIMATION_RATIO:D = 0.5


# instance fields
.field private mAppStartedCountInSPTMode:I

.field private mContext:Landroid/content/Context;

.field private volatile mContinuesSPTCount:I

.field private mCurHomeAnimationRatio:D

.field private mIsInSpeedTestMode:Z

.field private mSpeedTestModeService:Lcom/miui/server/sptm/SpeedTestModeServiceImpl;

.field private mStartedAppInLastRound:Ljava/util/HashSet;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/HashSet<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 3
    .param p1, "context"    # Landroid/content/Context;

    .line 27
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 12
    nop

    .line 13
    invoke-static {}, Lcom/miui/server/sptm/SpeedTestModeServiceImpl;->getInstance()Lcom/miui/server/sptm/SpeedTestModeServiceImpl;

    move-result-object v0

    iput-object v0, p0, Lcom/miui/server/sptm/HomeAnimationStrategy;->mSpeedTestModeService:Lcom/miui/server/sptm/SpeedTestModeServiceImpl;

    .line 20
    const-wide/high16 v0, 0x3ff0000000000000L    # 1.0

    iput-wide v0, p0, Lcom/miui/server/sptm/HomeAnimationStrategy;->mCurHomeAnimationRatio:D

    .line 21
    const/4 v2, 0x0

    iput v2, p0, Lcom/miui/server/sptm/HomeAnimationStrategy;->mContinuesSPTCount:I

    .line 22
    iput v2, p0, Lcom/miui/server/sptm/HomeAnimationStrategy;->mAppStartedCountInSPTMode:I

    .line 23
    iput-boolean v2, p0, Lcom/miui/server/sptm/HomeAnimationStrategy;->mIsInSpeedTestMode:Z

    .line 25
    new-instance v2, Ljava/util/HashSet;

    invoke-direct {v2}, Ljava/util/HashSet;-><init>()V

    iput-object v2, p0, Lcom/miui/server/sptm/HomeAnimationStrategy;->mStartedAppInLastRound:Ljava/util/HashSet;

    .line 28
    iput-object p1, p0, Lcom/miui/server/sptm/HomeAnimationStrategy;->mContext:Landroid/content/Context;

    .line 29
    invoke-static {v0, v1}, Ljava/lang/Double;->valueOf(D)Ljava/lang/Double;

    move-result-object v0

    invoke-direct {p0, v0}, Lcom/miui/server/sptm/HomeAnimationStrategy;->setAnimationRatio(Ljava/lang/Double;)V

    .line 30
    return-void
.end method

.method private declared-synchronized getContinuesSPTCount()I
    .locals 1

    monitor-enter p0

    .line 107
    :try_start_0
    iget v0, p0, Lcom/miui/server/sptm/HomeAnimationStrategy;->mContinuesSPTCount:I
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit p0

    return v0

    .line 107
    .end local p0    # "this":Lcom/miui/server/sptm/HomeAnimationStrategy;
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method private static limitHomeAnimationRatio(D)D
    .locals 4
    .param p0, "radio"    # D

    .line 93
    const-wide/high16 v0, 0x3fe0000000000000L    # 0.5

    invoke-static {v0, v1, p0, p1}, Ljava/lang/Math;->max(DD)D

    move-result-wide v0

    const-wide/high16 v2, 0x3ff0000000000000L    # 1.0

    invoke-static {v2, v3, v0, v1}, Ljava/lang/Math;->min(DD)D

    move-result-wide v0

    return-wide v0
.end method

.method private setAnimationRatio(Ljava/lang/Double;)V
    .locals 4
    .param p1, "value"    # Ljava/lang/Double;

    .line 97
    iget-object v0, p0, Lcom/miui/server/sptm/HomeAnimationStrategy;->mSpeedTestModeService:Lcom/miui/server/sptm/SpeedTestModeServiceImpl;

    invoke-virtual {v0}, Lcom/miui/server/sptm/SpeedTestModeServiceImpl;->getAnimationCloudEnable()Z

    move-result v0

    const-string/jumbo v1, "transition_animation_duration_ratio"

    if-eqz v0, :cond_0

    .line 98
    iget-object v0, p0, Lcom/miui/server/sptm/HomeAnimationStrategy;->mContext:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    .line 99
    invoke-static {p1}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    .line 98
    invoke-static {v0, v1, v2}, Landroid/provider/Settings$Global;->putString(Landroid/content/ContentResolver;Ljava/lang/String;Ljava/lang/String;)Z

    goto :goto_0

    .line 101
    :cond_0
    iget-object v0, p0, Lcom/miui/server/sptm/HomeAnimationStrategy;->mContext:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    .line 102
    const-wide/high16 v2, 0x3ff0000000000000L    # 1.0

    invoke-static {v2, v3}, Ljava/lang/String;->valueOf(D)Ljava/lang/String;

    move-result-object v2

    .line 101
    invoke-static {v0, v1, v2}, Landroid/provider/Settings$Global;->putString(Landroid/content/ContentResolver;Ljava/lang/String;Ljava/lang/String;)Z

    .line 104
    :goto_0
    return-void
.end method

.method private declared-synchronized setContinuesSPTCount(I)V
    .locals 0
    .param p1, "count"    # I

    monitor-enter p0

    .line 111
    :try_start_0
    iput p1, p0, Lcom/miui/server/sptm/HomeAnimationStrategy;->mContinuesSPTCount:I
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 112
    monitor-exit p0

    return-void

    .line 110
    .end local p0    # "this":Lcom/miui/server/sptm/HomeAnimationStrategy;
    .end local p1    # "count":I
    :catchall_0
    move-exception p1

    monitor-exit p0

    throw p1
.end method


# virtual methods
.method public getWindowAnimatorDurationOverride()F
    .locals 1

    .line 85
    invoke-direct {p0}, Lcom/miui/server/sptm/HomeAnimationStrategy;->getContinuesSPTCount()I

    move-result v0

    if-nez v0, :cond_1

    iget-boolean v0, p0, Lcom/miui/server/sptm/HomeAnimationStrategy;->mIsInSpeedTestMode:Z

    if-eqz v0, :cond_0

    goto :goto_0

    .line 88
    :cond_0
    const/high16 v0, 0x3f800000    # 1.0f

    return v0

    .line 86
    :cond_1
    :goto_0
    const v0, 0x3e99999a    # 0.3f

    return v0
.end method

.method public onAppStarted(Lcom/miui/server/sptm/PreLoadStrategy$AppStartRecord;)V
    .locals 4
    .param p1, "r"    # Lcom/miui/server/sptm/PreLoadStrategy$AppStartRecord;

    .line 42
    iget-boolean v0, p0, Lcom/miui/server/sptm/HomeAnimationStrategy;->mIsInSpeedTestMode:Z

    if-eqz v0, :cond_0

    .line 43
    iget v0, p0, Lcom/miui/server/sptm/HomeAnimationStrategy;->mAppStartedCountInSPTMode:I

    add-int/lit8 v0, v0, 0x1

    iput v0, p0, Lcom/miui/server/sptm/HomeAnimationStrategy;->mAppStartedCountInSPTMode:I

    .line 44
    nop

    .line 45
    invoke-direct {p0}, Lcom/miui/server/sptm/HomeAnimationStrategy;->getContinuesSPTCount()I

    move-result v1

    add-int/2addr v0, v1

    int-to-double v0, v0

    const-wide/high16 v2, 0x3fd0000000000000L    # 0.25

    mul-double/2addr v0, v2

    const-wide/high16 v2, 0x3ff0000000000000L    # 1.0

    sub-double/2addr v2, v0

    .line 44
    invoke-static {v2, v3}, Lcom/miui/server/sptm/HomeAnimationStrategy;->limitHomeAnimationRatio(D)D

    move-result-wide v0

    iput-wide v0, p0, Lcom/miui/server/sptm/HomeAnimationStrategy;->mCurHomeAnimationRatio:D

    .line 46
    invoke-static {v0, v1}, Ljava/lang/Double;->valueOf(D)Ljava/lang/Double;

    move-result-object v0

    invoke-direct {p0, v0}, Lcom/miui/server/sptm/HomeAnimationStrategy;->setAnimationRatio(Ljava/lang/Double;)V

    .line 49
    :cond_0
    iget-boolean v0, p1, Lcom/miui/server/sptm/PreLoadStrategy$AppStartRecord;->isColdStart:Z

    if-eqz v0, :cond_1

    sget-object v0, Lcom/miui/server/sptm/SpeedTestModeServiceImpl;->SPEED_TEST_APP_LIST:Ljava/util/List;

    iget-object v1, p1, Lcom/miui/server/sptm/PreLoadStrategy$AppStartRecord;->packageName:Ljava/lang/String;

    invoke-interface {v0, v1}, Ljava/util/List;->contains(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 50
    iget-object v0, p0, Lcom/miui/server/sptm/HomeAnimationStrategy;->mStartedAppInLastRound:Ljava/util/HashSet;

    iget-object v1, p1, Lcom/miui/server/sptm/PreLoadStrategy$AppStartRecord;->packageName:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/util/HashSet;->add(Ljava/lang/Object;)Z

    .line 52
    :cond_1
    return-void
.end method

.method public onNewEvent(I)V
    .locals 2
    .param p1, "eventType"    # I

    .line 34
    const/4 v0, 0x5

    if-ne p1, v0, :cond_0

    invoke-direct {p0}, Lcom/miui/server/sptm/HomeAnimationStrategy;->getContinuesSPTCount()I

    move-result v0

    if-eqz v0, :cond_0

    .line 35
    const-wide/high16 v0, 0x3ff0000000000000L    # 1.0

    invoke-static {v0, v1}, Ljava/lang/Double;->valueOf(D)Ljava/lang/Double;

    move-result-object v0

    invoke-direct {p0, v0}, Lcom/miui/server/sptm/HomeAnimationStrategy;->setAnimationRatio(Ljava/lang/Double;)V

    .line 36
    const/4 v0, 0x0

    invoke-direct {p0, v0}, Lcom/miui/server/sptm/HomeAnimationStrategy;->setContinuesSPTCount(I)V

    .line 38
    :cond_0
    return-void
.end method

.method public onSpeedTestModeChanged(Z)V
    .locals 7
    .param p1, "isEnable"    # Z

    .line 56
    const/4 v0, 0x0

    iput v0, p0, Lcom/miui/server/sptm/HomeAnimationStrategy;->mAppStartedCountInSPTMode:I

    .line 57
    iput-boolean p1, p0, Lcom/miui/server/sptm/HomeAnimationStrategy;->mIsInSpeedTestMode:Z

    .line 59
    if-nez p1, :cond_4

    .line 61
    iget-object v1, p0, Lcom/miui/server/sptm/HomeAnimationStrategy;->mSpeedTestModeService:Lcom/miui/server/sptm/SpeedTestModeServiceImpl;

    invoke-virtual {v1}, Lcom/miui/server/sptm/SpeedTestModeServiceImpl;->getPreloadCloudType()I

    move-result v1

    .line 62
    .local v1, "speedTestType":I
    iget-object v2, p0, Lcom/miui/server/sptm/HomeAnimationStrategy;->mStartedAppInLastRound:Ljava/util/HashSet;

    invoke-virtual {v2}, Ljava/util/HashSet;->size()I

    move-result v2

    .line 63
    .local v2, "startedAppCount":I
    const/4 v3, 0x3

    if-le v1, v3, :cond_0

    sget v4, Lcom/miui/server/sptm/SpeedTestModeServiceImpl;->STARTED_APPCOUNT:I

    if-ge v2, v4, :cond_1

    :cond_0
    if-gt v1, v3, :cond_2

    const/16 v3, 0xe

    if-lt v2, v3, :cond_2

    .line 66
    :cond_1
    invoke-direct {p0}, Lcom/miui/server/sptm/HomeAnimationStrategy;->getContinuesSPTCount()I

    move-result v0

    add-int/lit8 v0, v0, 0x1

    invoke-direct {p0, v0}, Lcom/miui/server/sptm/HomeAnimationStrategy;->setContinuesSPTCount(I)V

    goto :goto_0

    .line 68
    :cond_2
    invoke-direct {p0, v0}, Lcom/miui/server/sptm/HomeAnimationStrategy;->setContinuesSPTCount(I)V

    .line 70
    :goto_0
    sget-boolean v0, Lcom/miui/server/sptm/SpeedTestModeServiceImpl;->DEBUG:Z

    if-eqz v0, :cond_3

    .line 71
    iget-object v0, p0, Lcom/miui/server/sptm/HomeAnimationStrategy;->mStartedAppInLastRound:Ljava/util/HashSet;

    .line 73
    invoke-virtual {v0}, Ljava/util/HashSet;->size()I

    move-result v0

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    invoke-direct {p0}, Lcom/miui/server/sptm/HomeAnimationStrategy;->getContinuesSPTCount()I

    move-result v3

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    filled-new-array {v0, v3}, [Ljava/lang/Object;

    move-result-object v0

    .line 71
    const-string v3, "In last round, started: %s, curRound: %s"

    invoke-static {v3, v0}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    const-string v3, "SPTM"

    invoke-static {v3, v0}, Landroid/util/Slog;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 75
    :cond_3
    iget-object v0, p0, Lcom/miui/server/sptm/HomeAnimationStrategy;->mStartedAppInLastRound:Ljava/util/HashSet;

    invoke-virtual {v0}, Ljava/util/HashSet;->clear()V

    .line 78
    nop

    .line 79
    invoke-direct {p0}, Lcom/miui/server/sptm/HomeAnimationStrategy;->getContinuesSPTCount()I

    move-result v0

    int-to-double v3, v0

    const-wide/high16 v5, 0x3fd0000000000000L    # 0.25

    mul-double/2addr v3, v5

    const-wide/high16 v5, 0x3ff0000000000000L    # 1.0

    sub-double/2addr v5, v3

    .line 78
    invoke-static {v5, v6}, Lcom/miui/server/sptm/HomeAnimationStrategy;->limitHomeAnimationRatio(D)D

    move-result-wide v3

    iput-wide v3, p0, Lcom/miui/server/sptm/HomeAnimationStrategy;->mCurHomeAnimationRatio:D

    .line 80
    invoke-static {v3, v4}, Ljava/lang/Double;->valueOf(D)Ljava/lang/Double;

    move-result-object v0

    invoke-direct {p0, v0}, Lcom/miui/server/sptm/HomeAnimationStrategy;->setAnimationRatio(Ljava/lang/Double;)V

    .line 82
    .end local v1    # "speedTestType":I
    .end local v2    # "startedAppCount":I
    :cond_4
    return-void
.end method
