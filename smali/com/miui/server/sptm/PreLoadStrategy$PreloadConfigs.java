class com.miui.server.sptm.PreLoadStrategy$PreloadConfigs {
	 /* .source "PreLoadStrategy.java" */
	 /* # annotations */
	 /* .annotation system Ldalvik/annotation/EnclosingClass; */
	 /* value = Lcom/miui/server/sptm/PreLoadStrategy; */
} // .end annotation
/* .annotation system Ldalvik/annotation/InnerClass; */
/* accessFlags = 0x2 */
/* name = "PreloadConfigs" */
} // .end annotation
/* # instance fields */
private java.util.HashMap mConfigs;
/* .annotation system Ldalvik/annotation/Signature; */
/* value = { */
/* "Ljava/util/HashMap<", */
/* "Ljava/lang/String;", */
/* "Lmiui/process/LifecycleConfig;", */
/* ">;" */
/* } */
} // .end annotation
} // .end field
final com.miui.server.sptm.PreLoadStrategy this$0; //synthetic
/* # direct methods */
public com.miui.server.sptm.PreLoadStrategy$PreloadConfigs ( ) {
/* .locals 6 */
/* .line 176 */
this.this$0 = p1;
/* invoke-direct {p0}, Ljava/lang/Object;-><init>()V */
/* .line 174 */
/* new-instance v0, Ljava/util/HashMap; */
/* invoke-direct {v0}, Ljava/util/HashMap;-><init>()V */
this.mConfigs = v0;
/* .line 177 */
/* invoke-direct {p0}, Lcom/miui/server/sptm/PreLoadStrategy$PreloadConfigs;->resetPreloadConfigs()V */
/* .line 178 */
v0 = com.miui.server.sptm.PreLoadStrategy .-$$Nest$fgetDEBUG ( p1 );
if ( v0 != null) { // if-eqz v0, :cond_0
/* .line 179 */
v0 = this.mConfigs;
(( java.util.HashMap ) v0 ).entrySet ( ); // invoke-virtual {v0}, Ljava/util/HashMap;->entrySet()Ljava/util/Set;
v1 = } // :goto_0
if ( v1 != null) { // if-eqz v1, :cond_0
/* check-cast v1, Ljava/util/Map$Entry; */
/* .line 180 */
/* .local v1, "entry":Ljava/util/Map$Entry;, "Ljava/util/Map$Entry<Ljava/lang/String;Lmiui/process/LifecycleConfig;>;" */
com.miui.server.sptm.PreLoadStrategy .-$$Nest$fgetTAG ( p1 );
/* new-instance v3, Ljava/lang/StringBuilder; */
/* invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V */
final String v4 = "preload configs:"; // const-string v4, "preload configs:"
(( java.lang.StringBuilder ) v3 ).append ( v4 ); // invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
/* check-cast v4, Ljava/lang/String; */
(( java.lang.StringBuilder ) v3 ).append ( v4 ); // invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
final String v4 = " "; // const-string v4, " "
(( java.lang.StringBuilder ) v3 ).append ( v4 ); // invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
/* .line 181 */
/* check-cast v4, Lmiui/process/LifecycleConfig; */
(( miui.process.LifecycleConfig ) v4 ).getStopTimeout ( ); // invoke-virtual {v4}, Lmiui/process/LifecycleConfig;->getStopTimeout()J
/* move-result-wide v4 */
(( java.lang.StringBuilder ) v3 ).append ( v4, v5 ); // invoke-virtual {v3, v4, v5}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v3 ).toString ( ); // invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
/* .line 180 */
android.util.Slog .d ( v2,v3 );
/* .line 182 */
} // .end local v1 # "entry":Ljava/util/Map$Entry;, "Ljava/util/Map$Entry<Ljava/lang/String;Lmiui/process/LifecycleConfig;>;"
/* .line 184 */
} // :cond_0
return;
} // .end method
private void loadProperty ( ) {
/* .locals 6 */
/* .line 222 */
v0 = this.mConfigs;
(( java.util.HashMap ) v0 ).entrySet ( ); // invoke-virtual {v0}, Ljava/util/HashMap;->entrySet()Ljava/util/Set;
v1 = } // :goto_0
if ( v1 != null) { // if-eqz v1, :cond_1
/* check-cast v1, Ljava/util/Map$Entry; */
/* .line 223 */
/* .local v1, "entry":Ljava/util/Map$Entry;, "Ljava/util/Map$Entry<Ljava/lang/String;Lmiui/process/LifecycleConfig;>;" */
/* new-instance v2, Ljava/lang/StringBuilder; */
/* invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V */
final String v3 = "persist.sys.miui_sptm."; // const-string v3, "persist.sys.miui_sptm."
(( java.lang.StringBuilder ) v2 ).append ( v3 ); // invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
/* .line 224 */
/* check-cast v3, Ljava/lang/String; */
(( java.lang.StringBuilder ) v2 ).append ( v3 ); // invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v2 ).toString ( ); // invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
/* .line 223 */
/* const-wide/16 v3, -0x1 */
android.os.SystemProperties .getLong ( v2,v3,v4 );
/* move-result-wide v2 */
/* .line 225 */
/* .local v2, "stopTimeout":J */
/* const-wide/16 v4, 0x0 */
/* cmp-long v4, v2, v4 */
/* if-lez v4, :cond_0 */
/* .line 226 */
/* check-cast v4, Lmiui/process/LifecycleConfig; */
(( miui.process.LifecycleConfig ) v4 ).setStopTimeout ( v2, v3 ); // invoke-virtual {v4, v2, v3}, Lmiui/process/LifecycleConfig;->setStopTimeout(J)V
/* .line 228 */
} // .end local v1 # "entry":Ljava/util/Map$Entry;, "Ljava/util/Map$Entry<Ljava/lang/String;Lmiui/process/LifecycleConfig;>;"
} // .end local v2 # "stopTimeout":J
} // :cond_0
/* .line 229 */
} // :cond_1
return;
} // .end method
private void resetPreloadConfigs ( ) {
/* .locals 7 */
/* .line 187 */
v0 = this.mConfigs;
(( java.util.HashMap ) v0 ).clear ( ); // invoke-virtual {v0}, Ljava/util/HashMap;->clear()V
/* .line 188 */
v0 = this.this$0;
v0 = com.miui.server.sptm.PreLoadStrategy .-$$Nest$fgetmPreloadType ( v0 );
/* const/16 v1, 0xbb8 */
/* const/16 v2, 0x4e20 */
/* const/16 v3, 0x3e80 */
final String v4 = "com.tencent.tmgp.pubgmhd"; // const-string v4, "com.tencent.tmgp.pubgmhd"
final String v5 = "com.tencent.tmgp.sgame"; // const-string v5, "com.tencent.tmgp.sgame"
int v6 = 3; // const/4 v6, 0x3
/* if-le v0, v6, :cond_1 */
/* .line 189 */
v0 = this.this$0;
v0 = com.miui.server.sptm.PreLoadStrategy .-$$Nest$fgetmIsLowMemoryDevice ( v0 );
/* if-nez v0, :cond_0 */
/* .line 190 */
int v0 = 0; // const/4 v0, 0x0
/* .local v0, "i":I */
} // :goto_0
v6 = this.this$0;
v6 = com.miui.server.sptm.PreLoadStrategy .-$$Nest$fgetmNormalAppCount ( v6 );
/* if-ge v0, v6, :cond_0 */
/* .line 191 */
v6 = this.this$0;
com.miui.server.sptm.PreLoadStrategy .-$$Nest$fgetmSpeedTestModeService ( v6 );
v6 = com.miui.server.sptm.SpeedTestModeServiceImpl.PRELOAD_APPS_NEW;
/* check-cast v6, Ljava/lang/String; */
/* invoke-direct {p0, v6, v1}, Lcom/miui/server/sptm/PreLoadStrategy$PreloadConfigs;->setPreloadAppConfig(Ljava/lang/String;I)V */
/* .line 190 */
/* add-int/lit8 v0, v0, 0x1 */
/* .line 194 */
} // .end local v0 # "i":I
} // :cond_0
/* invoke-direct {p0, v5, v2}, Lcom/miui/server/sptm/PreLoadStrategy$PreloadConfigs;->setPreloadAppConfig(Ljava/lang/String;I)V */
/* .line 195 */
/* invoke-direct {p0, v4, v3}, Lcom/miui/server/sptm/PreLoadStrategy$PreloadConfigs;->setPreloadAppConfig(Ljava/lang/String;I)V */
/* .line 196 */
} // :cond_1
v0 = this.this$0;
v0 = com.miui.server.sptm.PreLoadStrategy .-$$Nest$fgetmPreloadType ( v0 );
/* if-ne v0, v6, :cond_3 */
/* .line 197 */
v0 = this.this$0;
v0 = com.miui.server.sptm.PreLoadStrategy .-$$Nest$fgetmIsLowMemoryDevice ( v0 );
/* if-nez v0, :cond_2 */
/* .line 198 */
int v0 = 0; // const/4 v0, 0x0
/* .restart local v0 # "i":I */
} // :goto_1
v6 = this.this$0;
v6 = com.miui.server.sptm.PreLoadStrategy .-$$Nest$fgetmNormalAppCount ( v6 );
/* if-ge v0, v6, :cond_2 */
/* .line 199 */
v6 = this.this$0;
com.miui.server.sptm.PreLoadStrategy .-$$Nest$fgetmSpeedTestModeService ( v6 );
v6 = com.miui.server.sptm.SpeedTestModeServiceImpl.PRELOAD_APPS;
/* check-cast v6, Ljava/lang/String; */
/* invoke-direct {p0, v6, v1}, Lcom/miui/server/sptm/PreLoadStrategy$PreloadConfigs;->setPreloadAppConfig(Ljava/lang/String;I)V */
/* .line 198 */
/* add-int/lit8 v0, v0, 0x1 */
/* .line 202 */
} // .end local v0 # "i":I
} // :cond_2
/* invoke-direct {p0, v5, v2}, Lcom/miui/server/sptm/PreLoadStrategy$PreloadConfigs;->setPreloadAppConfig(Ljava/lang/String;I)V */
/* .line 203 */
/* invoke-direct {p0, v4, v3}, Lcom/miui/server/sptm/PreLoadStrategy$PreloadConfigs;->setPreloadAppConfig(Ljava/lang/String;I)V */
/* .line 204 */
} // :cond_3
v0 = this.this$0;
v0 = com.miui.server.sptm.PreLoadStrategy .-$$Nest$fgetmPreloadType ( v0 );
int v1 = 2; // const/4 v1, 0x2
/* if-ne v0, v1, :cond_4 */
/* .line 205 */
/* const/16 v0, 0x3a98 */
/* invoke-direct {p0, v5, v0}, Lcom/miui/server/sptm/PreLoadStrategy$PreloadConfigs;->setPreloadAppConfig(Ljava/lang/String;I)V */
/* .line 206 */
/* invoke-direct {p0, v4, v0}, Lcom/miui/server/sptm/PreLoadStrategy$PreloadConfigs;->setPreloadAppConfig(Ljava/lang/String;I)V */
/* .line 207 */
} // :cond_4
v0 = this.this$0;
v0 = com.miui.server.sptm.PreLoadStrategy .-$$Nest$fgetmPreloadType ( v0 );
int v1 = 1; // const/4 v1, 0x1
/* if-ne v0, v1, :cond_5 */
/* .line 208 */
/* invoke-direct {p0, v5, v3}, Lcom/miui/server/sptm/PreLoadStrategy$PreloadConfigs;->setPreloadAppConfig(Ljava/lang/String;I)V */
/* .line 210 */
} // :cond_5
} // :goto_2
/* invoke-direct {p0}, Lcom/miui/server/sptm/PreLoadStrategy$PreloadConfigs;->loadProperty()V */
/* .line 211 */
return;
} // .end method
private void setPreloadAppConfig ( java.lang.String p0, Integer p1 ) {
/* .locals 3 */
/* .param p1, "preloadPkg" # Ljava/lang/String; */
/* .param p2, "timeOut" # I */
/* .line 214 */
int v0 = 1; // const/4 v0, 0x1
miui.process.LifecycleConfig .create ( v0 );
/* .line 215 */
/* .local v0, "config":Lmiui/process/LifecycleConfig; */
/* int-to-long v1, p2 */
(( miui.process.LifecycleConfig ) v0 ).setStopTimeout ( v1, v2 ); // invoke-virtual {v0, v1, v2}, Lmiui/process/LifecycleConfig;->setStopTimeout(J)V
/* .line 216 */
v1 = this.this$0;
com.miui.server.sptm.PreLoadStrategy .-$$Nest$fgetmSpeedTestModeService ( v1 );
int v2 = 0; // const/4 v2, 0x0
(( miui.process.LifecycleConfig ) v0 ).setSchedAffinity ( v2, v1 ); // invoke-virtual {v0, v2, v1}, Lmiui/process/LifecycleConfig;->setSchedAffinity(II)V
/* .line 218 */
v1 = this.mConfigs;
(( java.util.HashMap ) v1 ).put ( p1, v0 ); // invoke-virtual {v1, p1, v0}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;
/* .line 219 */
return;
} // .end method
/* # virtual methods */
public miui.process.LifecycleConfig find ( java.lang.String p0 ) {
/* .locals 1 */
/* .param p1, "packageName" # Ljava/lang/String; */
/* .line 232 */
/* invoke-direct {p0}, Lcom/miui/server/sptm/PreLoadStrategy$PreloadConfigs;->resetPreloadConfigs()V */
/* .line 233 */
v0 = this.mConfigs;
(( java.util.HashMap ) v0 ).get ( p1 ); // invoke-virtual {v0, p1}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;
/* check-cast v0, Lmiui/process/LifecycleConfig; */
/* .line 234 */
/* .local v0, "lifecycleConfig":Lmiui/process/LifecycleConfig; */
} // .end method
