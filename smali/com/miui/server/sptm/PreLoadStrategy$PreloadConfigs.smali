.class Lcom/miui/server/sptm/PreLoadStrategy$PreloadConfigs;
.super Ljava/lang/Object;
.source "PreLoadStrategy.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/miui/server/sptm/PreLoadStrategy;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "PreloadConfigs"
.end annotation


# instance fields
.field private mConfigs:Ljava/util/HashMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/HashMap<",
            "Ljava/lang/String;",
            "Lmiui/process/LifecycleConfig;",
            ">;"
        }
    .end annotation
.end field

.field final synthetic this$0:Lcom/miui/server/sptm/PreLoadStrategy;


# direct methods
.method public constructor <init>(Lcom/miui/server/sptm/PreLoadStrategy;)V
    .locals 6

    .line 176
    iput-object p1, p0, Lcom/miui/server/sptm/PreLoadStrategy$PreloadConfigs;->this$0:Lcom/miui/server/sptm/PreLoadStrategy;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 174
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    iput-object v0, p0, Lcom/miui/server/sptm/PreLoadStrategy$PreloadConfigs;->mConfigs:Ljava/util/HashMap;

    .line 177
    invoke-direct {p0}, Lcom/miui/server/sptm/PreLoadStrategy$PreloadConfigs;->resetPreloadConfigs()V

    .line 178
    invoke-static {p1}, Lcom/miui/server/sptm/PreLoadStrategy;->-$$Nest$fgetDEBUG(Lcom/miui/server/sptm/PreLoadStrategy;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 179
    iget-object v0, p0, Lcom/miui/server/sptm/PreLoadStrategy$PreloadConfigs;->mConfigs:Ljava/util/HashMap;

    invoke-virtual {v0}, Ljava/util/HashMap;->entrySet()Ljava/util/Set;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v0

    :goto_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_0

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/util/Map$Entry;

    .line 180
    .local v1, "entry":Ljava/util/Map$Entry;, "Ljava/util/Map$Entry<Ljava/lang/String;Lmiui/process/LifecycleConfig;>;"
    invoke-static {p1}, Lcom/miui/server/sptm/PreLoadStrategy;->-$$Nest$fgetTAG(Lcom/miui/server/sptm/PreLoadStrategy;)Ljava/lang/String;

    move-result-object v2

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "preload configs:"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-interface {v1}, Ljava/util/Map$Entry;->getKey()Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Ljava/lang/String;

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, " "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    .line 181
    invoke-interface {v1}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lmiui/process/LifecycleConfig;

    invoke-virtual {v4}, Lmiui/process/LifecycleConfig;->getStopTimeout()J

    move-result-wide v4

    invoke-virtual {v3, v4, v5}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    .line 180
    invoke-static {v2, v3}, Landroid/util/Slog;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 182
    .end local v1    # "entry":Ljava/util/Map$Entry;, "Ljava/util/Map$Entry<Ljava/lang/String;Lmiui/process/LifecycleConfig;>;"
    goto :goto_0

    .line 184
    :cond_0
    return-void
.end method

.method private loadProperty()V
    .locals 6

    .line 222
    iget-object v0, p0, Lcom/miui/server/sptm/PreLoadStrategy$PreloadConfigs;->mConfigs:Ljava/util/HashMap;

    invoke-virtual {v0}, Ljava/util/HashMap;->entrySet()Ljava/util/Set;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v0

    :goto_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_1

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/util/Map$Entry;

    .line 223
    .local v1, "entry":Ljava/util/Map$Entry;, "Ljava/util/Map$Entry<Ljava/lang/String;Lmiui/process/LifecycleConfig;>;"
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "persist.sys.miui_sptm."

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    .line 224
    invoke-interface {v1}, Ljava/util/Map$Entry;->getKey()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    .line 223
    const-wide/16 v3, -0x1

    invoke-static {v2, v3, v4}, Landroid/os/SystemProperties;->getLong(Ljava/lang/String;J)J

    move-result-wide v2

    .line 225
    .local v2, "stopTimeout":J
    const-wide/16 v4, 0x0

    cmp-long v4, v2, v4

    if-lez v4, :cond_0

    .line 226
    invoke-interface {v1}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lmiui/process/LifecycleConfig;

    invoke-virtual {v4, v2, v3}, Lmiui/process/LifecycleConfig;->setStopTimeout(J)V

    .line 228
    .end local v1    # "entry":Ljava/util/Map$Entry;, "Ljava/util/Map$Entry<Ljava/lang/String;Lmiui/process/LifecycleConfig;>;"
    .end local v2    # "stopTimeout":J
    :cond_0
    goto :goto_0

    .line 229
    :cond_1
    return-void
.end method

.method private resetPreloadConfigs()V
    .locals 7

    .line 187
    iget-object v0, p0, Lcom/miui/server/sptm/PreLoadStrategy$PreloadConfigs;->mConfigs:Ljava/util/HashMap;

    invoke-virtual {v0}, Ljava/util/HashMap;->clear()V

    .line 188
    iget-object v0, p0, Lcom/miui/server/sptm/PreLoadStrategy$PreloadConfigs;->this$0:Lcom/miui/server/sptm/PreLoadStrategy;

    invoke-static {v0}, Lcom/miui/server/sptm/PreLoadStrategy;->-$$Nest$fgetmPreloadType(Lcom/miui/server/sptm/PreLoadStrategy;)I

    move-result v0

    const/16 v1, 0xbb8

    const/16 v2, 0x4e20

    const/16 v3, 0x3e80

    const-string v4, "com.tencent.tmgp.pubgmhd"

    const-string v5, "com.tencent.tmgp.sgame"

    const/4 v6, 0x3

    if-le v0, v6, :cond_1

    .line 189
    iget-object v0, p0, Lcom/miui/server/sptm/PreLoadStrategy$PreloadConfigs;->this$0:Lcom/miui/server/sptm/PreLoadStrategy;

    invoke-static {v0}, Lcom/miui/server/sptm/PreLoadStrategy;->-$$Nest$fgetmIsLowMemoryDevice(Lcom/miui/server/sptm/PreLoadStrategy;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 190
    const/4 v0, 0x0

    .local v0, "i":I
    :goto_0
    iget-object v6, p0, Lcom/miui/server/sptm/PreLoadStrategy$PreloadConfigs;->this$0:Lcom/miui/server/sptm/PreLoadStrategy;

    invoke-static {v6}, Lcom/miui/server/sptm/PreLoadStrategy;->-$$Nest$fgetmNormalAppCount(Lcom/miui/server/sptm/PreLoadStrategy;)I

    move-result v6

    if-ge v0, v6, :cond_0

    .line 191
    iget-object v6, p0, Lcom/miui/server/sptm/PreLoadStrategy$PreloadConfigs;->this$0:Lcom/miui/server/sptm/PreLoadStrategy;

    invoke-static {v6}, Lcom/miui/server/sptm/PreLoadStrategy;->-$$Nest$fgetmSpeedTestModeService(Lcom/miui/server/sptm/PreLoadStrategy;)Lcom/miui/server/sptm/SpeedTestModeServiceImpl;

    sget-object v6, Lcom/miui/server/sptm/SpeedTestModeServiceImpl;->PRELOAD_APPS_NEW:Ljava/util/List;

    invoke-interface {v6, v0}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v6

    check-cast v6, Ljava/lang/String;

    invoke-direct {p0, v6, v1}, Lcom/miui/server/sptm/PreLoadStrategy$PreloadConfigs;->setPreloadAppConfig(Ljava/lang/String;I)V

    .line 190
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 194
    .end local v0    # "i":I
    :cond_0
    invoke-direct {p0, v5, v2}, Lcom/miui/server/sptm/PreLoadStrategy$PreloadConfigs;->setPreloadAppConfig(Ljava/lang/String;I)V

    .line 195
    invoke-direct {p0, v4, v3}, Lcom/miui/server/sptm/PreLoadStrategy$PreloadConfigs;->setPreloadAppConfig(Ljava/lang/String;I)V

    goto :goto_2

    .line 196
    :cond_1
    iget-object v0, p0, Lcom/miui/server/sptm/PreLoadStrategy$PreloadConfigs;->this$0:Lcom/miui/server/sptm/PreLoadStrategy;

    invoke-static {v0}, Lcom/miui/server/sptm/PreLoadStrategy;->-$$Nest$fgetmPreloadType(Lcom/miui/server/sptm/PreLoadStrategy;)I

    move-result v0

    if-ne v0, v6, :cond_3

    .line 197
    iget-object v0, p0, Lcom/miui/server/sptm/PreLoadStrategy$PreloadConfigs;->this$0:Lcom/miui/server/sptm/PreLoadStrategy;

    invoke-static {v0}, Lcom/miui/server/sptm/PreLoadStrategy;->-$$Nest$fgetmIsLowMemoryDevice(Lcom/miui/server/sptm/PreLoadStrategy;)Z

    move-result v0

    if-nez v0, :cond_2

    .line 198
    const/4 v0, 0x0

    .restart local v0    # "i":I
    :goto_1
    iget-object v6, p0, Lcom/miui/server/sptm/PreLoadStrategy$PreloadConfigs;->this$0:Lcom/miui/server/sptm/PreLoadStrategy;

    invoke-static {v6}, Lcom/miui/server/sptm/PreLoadStrategy;->-$$Nest$fgetmNormalAppCount(Lcom/miui/server/sptm/PreLoadStrategy;)I

    move-result v6

    if-ge v0, v6, :cond_2

    .line 199
    iget-object v6, p0, Lcom/miui/server/sptm/PreLoadStrategy$PreloadConfigs;->this$0:Lcom/miui/server/sptm/PreLoadStrategy;

    invoke-static {v6}, Lcom/miui/server/sptm/PreLoadStrategy;->-$$Nest$fgetmSpeedTestModeService(Lcom/miui/server/sptm/PreLoadStrategy;)Lcom/miui/server/sptm/SpeedTestModeServiceImpl;

    sget-object v6, Lcom/miui/server/sptm/SpeedTestModeServiceImpl;->PRELOAD_APPS:Ljava/util/List;

    invoke-interface {v6, v0}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v6

    check-cast v6, Ljava/lang/String;

    invoke-direct {p0, v6, v1}, Lcom/miui/server/sptm/PreLoadStrategy$PreloadConfigs;->setPreloadAppConfig(Ljava/lang/String;I)V

    .line 198
    add-int/lit8 v0, v0, 0x1

    goto :goto_1

    .line 202
    .end local v0    # "i":I
    :cond_2
    invoke-direct {p0, v5, v2}, Lcom/miui/server/sptm/PreLoadStrategy$PreloadConfigs;->setPreloadAppConfig(Ljava/lang/String;I)V

    .line 203
    invoke-direct {p0, v4, v3}, Lcom/miui/server/sptm/PreLoadStrategy$PreloadConfigs;->setPreloadAppConfig(Ljava/lang/String;I)V

    goto :goto_2

    .line 204
    :cond_3
    iget-object v0, p0, Lcom/miui/server/sptm/PreLoadStrategy$PreloadConfigs;->this$0:Lcom/miui/server/sptm/PreLoadStrategy;

    invoke-static {v0}, Lcom/miui/server/sptm/PreLoadStrategy;->-$$Nest$fgetmPreloadType(Lcom/miui/server/sptm/PreLoadStrategy;)I

    move-result v0

    const/4 v1, 0x2

    if-ne v0, v1, :cond_4

    .line 205
    const/16 v0, 0x3a98

    invoke-direct {p0, v5, v0}, Lcom/miui/server/sptm/PreLoadStrategy$PreloadConfigs;->setPreloadAppConfig(Ljava/lang/String;I)V

    .line 206
    invoke-direct {p0, v4, v0}, Lcom/miui/server/sptm/PreLoadStrategy$PreloadConfigs;->setPreloadAppConfig(Ljava/lang/String;I)V

    goto :goto_2

    .line 207
    :cond_4
    iget-object v0, p0, Lcom/miui/server/sptm/PreLoadStrategy$PreloadConfigs;->this$0:Lcom/miui/server/sptm/PreLoadStrategy;

    invoke-static {v0}, Lcom/miui/server/sptm/PreLoadStrategy;->-$$Nest$fgetmPreloadType(Lcom/miui/server/sptm/PreLoadStrategy;)I

    move-result v0

    const/4 v1, 0x1

    if-ne v0, v1, :cond_5

    .line 208
    invoke-direct {p0, v5, v3}, Lcom/miui/server/sptm/PreLoadStrategy$PreloadConfigs;->setPreloadAppConfig(Ljava/lang/String;I)V

    .line 210
    :cond_5
    :goto_2
    invoke-direct {p0}, Lcom/miui/server/sptm/PreLoadStrategy$PreloadConfigs;->loadProperty()V

    .line 211
    return-void
.end method

.method private setPreloadAppConfig(Ljava/lang/String;I)V
    .locals 3
    .param p1, "preloadPkg"    # Ljava/lang/String;
    .param p2, "timeOut"    # I

    .line 214
    const/4 v0, 0x1

    invoke-static {v0}, Lmiui/process/LifecycleConfig;->create(I)Lmiui/process/LifecycleConfig;

    move-result-object v0

    .line 215
    .local v0, "config":Lmiui/process/LifecycleConfig;
    int-to-long v1, p2

    invoke-virtual {v0, v1, v2}, Lmiui/process/LifecycleConfig;->setStopTimeout(J)V

    .line 216
    iget-object v1, p0, Lcom/miui/server/sptm/PreLoadStrategy$PreloadConfigs;->this$0:Lcom/miui/server/sptm/PreLoadStrategy;

    invoke-static {v1}, Lcom/miui/server/sptm/PreLoadStrategy;->-$$Nest$fgetmSpeedTestModeService(Lcom/miui/server/sptm/PreLoadStrategy;)Lcom/miui/server/sptm/SpeedTestModeServiceImpl;

    sget v1, Lcom/miui/server/sptm/SpeedTestModeServiceImpl;->SPTM_LOW_MEMORY_DEVICE_PRELOAD_CORE:I

    const/4 v2, 0x0

    invoke-virtual {v0, v2, v1}, Lmiui/process/LifecycleConfig;->setSchedAffinity(II)V

    .line 218
    iget-object v1, p0, Lcom/miui/server/sptm/PreLoadStrategy$PreloadConfigs;->mConfigs:Ljava/util/HashMap;

    invoke-virtual {v1, p1, v0}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 219
    return-void
.end method


# virtual methods
.method public find(Ljava/lang/String;)Lmiui/process/LifecycleConfig;
    .locals 1
    .param p1, "packageName"    # Ljava/lang/String;

    .line 232
    invoke-direct {p0}, Lcom/miui/server/sptm/PreLoadStrategy$PreloadConfigs;->resetPreloadConfigs()V

    .line 233
    iget-object v0, p0, Lcom/miui/server/sptm/PreLoadStrategy$PreloadConfigs;->mConfigs:Ljava/util/HashMap;

    invoke-virtual {v0, p1}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lmiui/process/LifecycleConfig;

    .line 234
    .local v0, "lifecycleConfig":Lmiui/process/LifecycleConfig;
    return-object v0
.end method
