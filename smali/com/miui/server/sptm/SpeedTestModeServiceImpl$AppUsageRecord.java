class com.miui.server.sptm.SpeedTestModeServiceImpl$AppUsageRecord {
	 /* .source "SpeedTestModeServiceImpl.java" */
	 /* # annotations */
	 /* .annotation system Ldalvik/annotation/EnclosingClass; */
	 /* value = Lcom/miui/server/sptm/SpeedTestModeServiceImpl; */
} // .end annotation
/* .annotation system Ldalvik/annotation/InnerClass; */
/* accessFlags = 0xa */
/* name = "AppUsageRecord" */
} // .end annotation
/* # instance fields */
Long endTime;
Boolean isColdStart;
java.lang.String packageName;
Long startTime;
/* # direct methods */
private com.miui.server.sptm.SpeedTestModeServiceImpl$AppUsageRecord ( ) {
/* .locals 0 */
/* .line 638 */
/* invoke-direct {p0}, Ljava/lang/Object;-><init>()V */
return;
} // .end method
 com.miui.server.sptm.SpeedTestModeServiceImpl$AppUsageRecord ( ) { //synthethic
/* .locals 0 */
/* invoke-direct {p0}, Lcom/miui/server/sptm/SpeedTestModeServiceImpl$AppUsageRecord;-><init>()V */
return;
} // .end method
/* # virtual methods */
public Long getDuration ( ) {
/* .locals 4 */
/* .line 645 */
/* iget-wide v0, p0, Lcom/miui/server/sptm/SpeedTestModeServiceImpl$AppUsageRecord;->endTime:J */
/* iget-wide v2, p0, Lcom/miui/server/sptm/SpeedTestModeServiceImpl$AppUsageRecord;->startTime:J */
/* sub-long/2addr v0, v2 */
/* return-wide v0 */
} // .end method
public java.lang.String toString ( ) {
/* .locals 3 */
/* .line 650 */
/* new-instance v0, Ljava/lang/StringBuilder; */
/* invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V */
final String v1 = "AppUsageRecord{packageName=\'"; // const-string v1, "AppUsageRecord{packageName=\'"
(( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
v1 = this.packageName;
(( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
/* const/16 v1, 0x27 */
(( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;
final String v1 = ", startTime="; // const-string v1, ", startTime="
(( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
/* iget-wide v1, p0, Lcom/miui/server/sptm/SpeedTestModeServiceImpl$AppUsageRecord;->startTime:J */
(( java.lang.StringBuilder ) v0 ).append ( v1, v2 ); // invoke-virtual {v0, v1, v2}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;
final String v1 = ", endTime="; // const-string v1, ", endTime="
(( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
/* iget-wide v1, p0, Lcom/miui/server/sptm/SpeedTestModeServiceImpl$AppUsageRecord;->endTime:J */
(( java.lang.StringBuilder ) v0 ).append ( v1, v2 ); // invoke-virtual {v0, v1, v2}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;
final String v1 = ", usedTime="; // const-string v1, ", usedTime="
(( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
/* .line 654 */
(( com.miui.server.sptm.SpeedTestModeServiceImpl$AppUsageRecord ) p0 ).getDuration ( ); // invoke-virtual {p0}, Lcom/miui/server/sptm/SpeedTestModeServiceImpl$AppUsageRecord;->getDuration()J
/* move-result-wide v1 */
(( java.lang.StringBuilder ) v0 ).append ( v1, v2 ); // invoke-virtual {v0, v1, v2}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;
final String v1 = ", isColdStart="; // const-string v1, ", isColdStart="
(( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
/* iget-boolean v1, p0, Lcom/miui/server/sptm/SpeedTestModeServiceImpl$AppUsageRecord;->isColdStart:Z */
(( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;
/* const/16 v1, 0x7d */
(( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v0 ).toString ( ); // invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
/* .line 650 */
} // .end method
