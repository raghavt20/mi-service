.class Lcom/miui/server/sptm/PreLoadStrategy;
.super Ljava/lang/Object;
.source "PreLoadStrategy.java"

# interfaces
.implements Lcom/miui/server/sptm/SpeedTestModeServiceImpl$Strategy;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/miui/server/sptm/PreLoadStrategy$PreloadConfigs;,
        Lcom/miui/server/sptm/PreLoadStrategy$AppStartRecord;
    }
.end annotation


# static fields
.field private static final PUBG_PACKAGE_NAME:Ljava/lang/String; = "com.tencent.tmgp.pubgmhd"

.field private static final SGAME_PACKAGE_NAME:Ljava/lang/String; = "com.tencent.tmgp.sgame"

.field private static final STOP_TIME_PACKAGES_PREFIX:Ljava/lang/String; = "persist.sys.miui_sptm."


# instance fields
.field private final DEBUG:Z

.field private final TAG:Ljava/lang/String;

.field private mAppStartedRecords:Ljava/util/HashMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/HashMap<",
            "Ljava/lang/String;",
            "Lcom/miui/server/sptm/PreLoadStrategy$AppStartRecord;",
            ">;"
        }
    .end annotation
.end field

.field private mIsInSpeedTestMode:Z

.field private mIsLowMemoryDevice:Z

.field private mLastPreloadStartedAppCount:I

.field private mNextPreloadTargetIndex:I

.field private mNormalAppCount:I

.field private mNormalAppCountNew:I

.field private mNormalAppCountOld:I

.field private mPMS:Lcom/miui/server/process/ProcessManagerInternal;

.field private mPreloadConfigs:Lcom/miui/server/sptm/PreLoadStrategy$PreloadConfigs;

.field private mPreloadType:I

.field private mSpeedTestModeService:Lcom/miui/server/sptm/SpeedTestModeServiceImpl;


# direct methods
.method static bridge synthetic -$$Nest$fgetDEBUG(Lcom/miui/server/sptm/PreLoadStrategy;)Z
    .locals 0

    iget-boolean p0, p0, Lcom/miui/server/sptm/PreLoadStrategy;->DEBUG:Z

    return p0
.end method

.method static bridge synthetic -$$Nest$fgetTAG(Lcom/miui/server/sptm/PreLoadStrategy;)Ljava/lang/String;
    .locals 0

    iget-object p0, p0, Lcom/miui/server/sptm/PreLoadStrategy;->TAG:Ljava/lang/String;

    return-object p0
.end method

.method static bridge synthetic -$$Nest$fgetmIsLowMemoryDevice(Lcom/miui/server/sptm/PreLoadStrategy;)Z
    .locals 0

    iget-boolean p0, p0, Lcom/miui/server/sptm/PreLoadStrategy;->mIsLowMemoryDevice:Z

    return p0
.end method

.method static bridge synthetic -$$Nest$fgetmNormalAppCount(Lcom/miui/server/sptm/PreLoadStrategy;)I
    .locals 0

    iget p0, p0, Lcom/miui/server/sptm/PreLoadStrategy;->mNormalAppCount:I

    return p0
.end method

.method static bridge synthetic -$$Nest$fgetmPreloadType(Lcom/miui/server/sptm/PreLoadStrategy;)I
    .locals 0

    iget p0, p0, Lcom/miui/server/sptm/PreLoadStrategy;->mPreloadType:I

    return p0
.end method

.method static bridge synthetic -$$Nest$fgetmSpeedTestModeService(Lcom/miui/server/sptm/PreLoadStrategy;)Lcom/miui/server/sptm/SpeedTestModeServiceImpl;
    .locals 0

    iget-object p0, p0, Lcom/miui/server/sptm/PreLoadStrategy;->mSpeedTestModeService:Lcom/miui/server/sptm/SpeedTestModeServiceImpl;

    return-object p0
.end method

.method constructor <init>()V
    .locals 5

    .line 20
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 22
    nop

    .line 23
    invoke-static {}, Lcom/miui/server/sptm/SpeedTestModeServiceImpl;->getInstance()Lcom/miui/server/sptm/SpeedTestModeServiceImpl;

    move-result-object v0

    iput-object v0, p0, Lcom/miui/server/sptm/PreLoadStrategy;->mSpeedTestModeService:Lcom/miui/server/sptm/SpeedTestModeServiceImpl;

    .line 25
    const-string v0, "SPTM"

    iput-object v0, p0, Lcom/miui/server/sptm/PreLoadStrategy;->TAG:Ljava/lang/String;

    .line 26
    sget-boolean v0, Lcom/miui/server/sptm/SpeedTestModeServiceImpl;->DEBUG:Z

    iput-boolean v0, p0, Lcom/miui/server/sptm/PreLoadStrategy;->DEBUG:Z

    .line 31
    new-instance v0, Lcom/miui/server/sptm/PreLoadStrategy$PreloadConfigs;

    invoke-direct {v0, p0}, Lcom/miui/server/sptm/PreLoadStrategy$PreloadConfigs;-><init>(Lcom/miui/server/sptm/PreLoadStrategy;)V

    iput-object v0, p0, Lcom/miui/server/sptm/PreLoadStrategy;->mPreloadConfigs:Lcom/miui/server/sptm/PreLoadStrategy$PreloadConfigs;

    .line 33
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    iput-object v0, p0, Lcom/miui/server/sptm/PreLoadStrategy;->mAppStartedRecords:Ljava/util/HashMap;

    .line 34
    iget-object v0, p0, Lcom/miui/server/sptm/PreLoadStrategy;->mSpeedTestModeService:Lcom/miui/server/sptm/SpeedTestModeServiceImpl;

    invoke-virtual {v0}, Lcom/miui/server/sptm/SpeedTestModeServiceImpl;->getPreloadCloudType()I

    move-result v0

    iput v0, p0, Lcom/miui/server/sptm/PreLoadStrategy;->mPreloadType:I

    .line 36
    sget-object v0, Lcom/miui/server/sptm/SpeedTestModeServiceImpl;->PRELOAD_APPS:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    sget-object v1, Lcom/miui/server/sptm/SpeedTestModeServiceImpl;->PRELOAD_GAME_APPS:Ljava/util/List;

    .line 37
    invoke-interface {v1}, Ljava/util/List;->size()I

    move-result v1

    sub-int/2addr v0, v1

    iput v0, p0, Lcom/miui/server/sptm/PreLoadStrategy;->mNormalAppCountOld:I

    .line 38
    sget-object v0, Lcom/miui/server/sptm/SpeedTestModeServiceImpl;->PRELOAD_APPS_NEW:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    iput v0, p0, Lcom/miui/server/sptm/PreLoadStrategy;->mNormalAppCountNew:I

    .line 39
    iget v1, p0, Lcom/miui/server/sptm/PreLoadStrategy;->mPreloadType:I

    const/4 v2, 0x3

    if-le v1, v2, :cond_0

    goto :goto_0

    :cond_0
    iget v0, p0, Lcom/miui/server/sptm/PreLoadStrategy;->mNormalAppCountOld:I

    :goto_0
    iput v0, p0, Lcom/miui/server/sptm/PreLoadStrategy;->mNormalAppCount:I

    .line 40
    const/4 v0, 0x0

    iput v0, p0, Lcom/miui/server/sptm/PreLoadStrategy;->mLastPreloadStartedAppCount:I

    .line 41
    iput v0, p0, Lcom/miui/server/sptm/PreLoadStrategy;->mNextPreloadTargetIndex:I

    .line 42
    iput-boolean v0, p0, Lcom/miui/server/sptm/PreLoadStrategy;->mIsInSpeedTestMode:Z

    .line 43
    sget-wide v1, Lcom/miui/server/sptm/SpeedTestModeServiceImpl;->TOTAL_MEMORY:J

    const-wide/16 v3, 0x2000

    cmp-long v1, v1, v3

    if-gez v1, :cond_1

    const/4 v0, 0x1

    :cond_1
    iput-boolean v0, p0, Lcom/miui/server/sptm/PreLoadStrategy;->mIsLowMemoryDevice:Z

    return-void
.end method

.method private preloadNextApps()V
    .locals 8

    .line 97
    iget-boolean v0, p0, Lcom/miui/server/sptm/PreLoadStrategy;->mIsInSpeedTestMode:Z

    if-nez v0, :cond_0

    .line 98
    return-void

    .line 101
    :cond_0
    iget-object v0, p0, Lcom/miui/server/sptm/PreLoadStrategy;->mAppStartedRecords:Ljava/util/HashMap;

    invoke-virtual {v0}, Ljava/util/HashMap;->size()I

    move-result v0

    .line 102
    .local v0, "appStartedCount":I
    sget v1, Lcom/miui/server/sptm/SpeedTestModeServiceImpl;->PRELOAD_THRESHOLD:I

    .line 104
    .local v1, "preloadThreshold":I
    iget-boolean v2, p0, Lcom/miui/server/sptm/PreLoadStrategy;->mIsLowMemoryDevice:Z

    const/4 v3, 0x2

    if-nez v2, :cond_1

    iget v2, p0, Lcom/miui/server/sptm/PreLoadStrategy;->mPreloadType:I

    if-le v2, v3, :cond_1

    iget v2, p0, Lcom/miui/server/sptm/PreLoadStrategy;->mNextPreloadTargetIndex:I

    iget v4, p0, Lcom/miui/server/sptm/PreLoadStrategy;->mNormalAppCountNew:I

    if-gt v2, v4, :cond_1

    .line 106
    const/4 v1, 0x2

    .line 108
    :cond_1
    iget v2, p0, Lcom/miui/server/sptm/PreLoadStrategy;->mLastPreloadStartedAppCount:I

    if-eqz v2, :cond_2

    .line 109
    add-int/2addr v2, v1

    goto :goto_0

    .line 110
    :cond_2
    nop

    :goto_0
    nop

    .line 111
    .local v2, "nextPreloadAppStartedThreshold":I
    iget-boolean v4, p0, Lcom/miui/server/sptm/PreLoadStrategy;->DEBUG:Z

    if-eqz v4, :cond_3

    .line 112
    iget-object v4, p0, Lcom/miui/server/sptm/PreLoadStrategy;->TAG:Ljava/lang/String;

    .line 113
    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v6

    iget v7, p0, Lcom/miui/server/sptm/PreLoadStrategy;->mNormalAppCountNew:I

    invoke-static {v7}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v7

    filled-new-array {v5, v6, v7}, [Ljava/lang/Object;

    move-result-object v5

    .line 112
    const-string v6, "preloadNextApps: cur_apps: %s, threshold: %s, pre_apps: %s"

    invoke-static {v6, v5}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5}, Landroid/util/Slog;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 115
    :cond_3
    if-ge v0, v2, :cond_4

    .line 116
    return-void

    .line 118
    :cond_4
    iput v0, p0, Lcom/miui/server/sptm/PreLoadStrategy;->mLastPreloadStartedAppCount:I

    .line 120
    iget v4, p0, Lcom/miui/server/sptm/PreLoadStrategy;->mPreloadType:I

    const/4 v5, 0x3

    if-le v4, v5, :cond_5

    iget-boolean v5, p0, Lcom/miui/server/sptm/PreLoadStrategy;->mIsLowMemoryDevice:Z

    if-nez v5, :cond_5

    .line 121
    iget v3, p0, Lcom/miui/server/sptm/PreLoadStrategy;->mNextPreloadTargetIndex:I

    sget-object v4, Lcom/miui/server/sptm/SpeedTestModeServiceImpl;->PRELOAD_APPS_NEW:Ljava/util/List;

    invoke-interface {v4}, Ljava/util/List;->size()I

    move-result v4

    if-ge v3, v4, :cond_7

    .line 122
    sget-object v3, Lcom/miui/server/sptm/SpeedTestModeServiceImpl;->PRELOAD_APPS_NEW:Ljava/util/List;

    iget v4, p0, Lcom/miui/server/sptm/PreLoadStrategy;->mNextPreloadTargetIndex:I

    add-int/lit8 v5, v4, 0x1

    iput v5, p0, Lcom/miui/server/sptm/PreLoadStrategy;->mNextPreloadTargetIndex:I

    invoke-interface {v3, v4}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/lang/String;

    invoke-direct {p0, v3}, Lcom/miui/server/sptm/PreLoadStrategy;->preloadPackage(Ljava/lang/String;)V

    goto :goto_1

    .line 124
    :cond_5
    if-le v4, v3, :cond_6

    iget-boolean v3, p0, Lcom/miui/server/sptm/PreLoadStrategy;->mIsLowMemoryDevice:Z

    if-nez v3, :cond_6

    .line 125
    iget v3, p0, Lcom/miui/server/sptm/PreLoadStrategy;->mNextPreloadTargetIndex:I

    sget-object v4, Lcom/miui/server/sptm/SpeedTestModeServiceImpl;->PRELOAD_APPS:Ljava/util/List;

    invoke-interface {v4}, Ljava/util/List;->size()I

    move-result v4

    if-ge v3, v4, :cond_7

    .line 126
    sget-object v3, Lcom/miui/server/sptm/SpeedTestModeServiceImpl;->PRELOAD_APPS:Ljava/util/List;

    iget v4, p0, Lcom/miui/server/sptm/PreLoadStrategy;->mNextPreloadTargetIndex:I

    add-int/lit8 v5, v4, 0x1

    iput v5, p0, Lcom/miui/server/sptm/PreLoadStrategy;->mNextPreloadTargetIndex:I

    invoke-interface {v3, v4}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/lang/String;

    invoke-direct {p0, v3}, Lcom/miui/server/sptm/PreLoadStrategy;->preloadPackage(Ljava/lang/String;)V

    goto :goto_1

    .line 128
    :cond_6
    if-lez v4, :cond_7

    iget v3, p0, Lcom/miui/server/sptm/PreLoadStrategy;->mNextPreloadTargetIndex:I

    sget-object v4, Lcom/miui/server/sptm/SpeedTestModeServiceImpl;->PRELOAD_GAME_APPS:Ljava/util/List;

    .line 129
    invoke-interface {v4}, Ljava/util/List;->size()I

    move-result v4

    if-ge v3, v4, :cond_7

    .line 130
    sget-object v3, Lcom/miui/server/sptm/SpeedTestModeServiceImpl;->PRELOAD_GAME_APPS:Ljava/util/List;

    iget v4, p0, Lcom/miui/server/sptm/PreLoadStrategy;->mNextPreloadTargetIndex:I

    add-int/lit8 v5, v4, 0x1

    iput v5, p0, Lcom/miui/server/sptm/PreLoadStrategy;->mNextPreloadTargetIndex:I

    invoke-interface {v3, v4}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/lang/String;

    invoke-direct {p0, v3}, Lcom/miui/server/sptm/PreLoadStrategy;->preloadPackage(Ljava/lang/String;)V

    .line 132
    :cond_7
    :goto_1
    return-void
.end method

.method private preloadPackage(Ljava/lang/String;)V
    .locals 7
    .param p1, "packageName"    # Ljava/lang/String;

    .line 135
    invoke-static {p1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 136
    return-void

    .line 139
    :cond_0
    :try_start_0
    iget-object v0, p0, Lcom/miui/server/sptm/PreLoadStrategy;->mPreloadConfigs:Lcom/miui/server/sptm/PreLoadStrategy$PreloadConfigs;

    invoke-virtual {v0, p1}, Lcom/miui/server/sptm/PreLoadStrategy$PreloadConfigs;->find(Ljava/lang/String;)Lmiui/process/LifecycleConfig;

    move-result-object v0

    .line 140
    .local v0, "lifecycleConfig":Lmiui/process/LifecycleConfig;
    if-eqz v0, :cond_1

    .line 141
    invoke-static {}, Lmiui/process/ProcessManagerNative;->getDefault()Lmiui/process/IProcessManager;

    move-result-object v1

    const/4 v2, 0x0

    const/4 v3, 0x1

    invoke-interface {v1, p1, v3, v2, v0}, Lmiui/process/IProcessManager;->startPreloadApp(Ljava/lang/String;ZZLmiui/process/LifecycleConfig;)I

    move-result v1

    .line 144
    .local v1, "res":I
    iget-boolean v4, p0, Lcom/miui/server/sptm/PreLoadStrategy;->DEBUG:Z

    if-eqz v4, :cond_1

    .line 145
    iget-object v4, p0, Lcom/miui/server/sptm/PreLoadStrategy;->TAG:Ljava/lang/String;

    const-string v5, "preloadNextApps: preload: %s, res=%s"

    const/4 v6, 0x2

    new-array v6, v6, [Ljava/lang/Object;

    aput-object p1, v6, v2

    .line 146
    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    aput-object v2, v6, v3

    .line 145
    invoke-static {v5, v6}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    invoke-static {v4, v2}, Landroid/util/Slog;->d(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    .line 150
    .end local v0    # "lifecycleConfig":Lmiui/process/LifecycleConfig;
    .end local v1    # "res":I
    :cond_1
    goto :goto_0

    .line 148
    :catch_0
    move-exception v0

    .line 149
    .local v0, "e":Landroid/os/RemoteException;
    invoke-virtual {v0}, Landroid/os/RemoteException;->printStackTrace()V

    .line 151
    .end local v0    # "e":Landroid/os/RemoteException;
    :goto_0
    return-void
.end method


# virtual methods
.method public onAppStarted(Lcom/miui/server/sptm/PreLoadStrategy$AppStartRecord;)V
    .locals 2
    .param p1, "r"    # Lcom/miui/server/sptm/PreLoadStrategy$AppStartRecord;

    .line 61
    sget-object v0, Lcom/miui/server/sptm/SpeedTestModeServiceImpl;->SPEED_TEST_APP_LIST:Ljava/util/List;

    iget-object v1, p1, Lcom/miui/server/sptm/PreLoadStrategy$AppStartRecord;->packageName:Ljava/lang/String;

    invoke-interface {v0, v1}, Ljava/util/List;->contains(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget v0, p0, Lcom/miui/server/sptm/PreLoadStrategy;->mPreloadType:I

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/miui/server/sptm/PreLoadStrategy;->mAppStartedRecords:Ljava/util/HashMap;

    iget-object v1, p1, Lcom/miui/server/sptm/PreLoadStrategy$AppStartRecord;->packageName:Ljava/lang/String;

    .line 63
    invoke-virtual {v0, v1}, Ljava/util/HashMap;->containsKey(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 64
    iget-object v0, p0, Lcom/miui/server/sptm/PreLoadStrategy;->mAppStartedRecords:Ljava/util/HashMap;

    iget-object v1, p1, Lcom/miui/server/sptm/PreLoadStrategy$AppStartRecord;->packageName:Ljava/lang/String;

    invoke-virtual {v0, v1, p1}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 65
    invoke-direct {p0}, Lcom/miui/server/sptm/PreLoadStrategy;->preloadNextApps()V

    .line 67
    :cond_0
    return-void
.end method

.method public onNewEvent(I)V
    .locals 2
    .param p1, "eventType"    # I

    .line 54
    const/4 v0, 0x5

    if-ne p1, v0, :cond_0

    iget-object v0, p0, Lcom/miui/server/sptm/PreLoadStrategy;->mPMS:Lcom/miui/server/process/ProcessManagerInternal;

    if-eqz v0, :cond_0

    .line 55
    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/miui/server/process/ProcessManagerInternal;->setSpeedTestState(Z)V

    .line 57
    :cond_0
    return-void
.end method

.method public onPreloadAppStarted(Ljava/lang/String;)V
    .locals 0
    .param p1, "packageName"    # Ljava/lang/String;

    .line 49
    return-void
.end method

.method public onSpeedTestModeChanged(Z)V
    .locals 3
    .param p1, "isEnable"    # Z

    .line 71
    iput-boolean p1, p0, Lcom/miui/server/sptm/PreLoadStrategy;->mIsInSpeedTestMode:Z

    .line 72
    iget-object v0, p0, Lcom/miui/server/sptm/PreLoadStrategy;->mPMS:Lcom/miui/server/process/ProcessManagerInternal;

    if-nez v0, :cond_0

    .line 73
    const-class v0, Lcom/miui/server/process/ProcessManagerInternal;

    invoke-static {v0}, Lcom/android/server/LocalServices;->getService(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/miui/server/process/ProcessManagerInternal;

    iput-object v0, p0, Lcom/miui/server/sptm/PreLoadStrategy;->mPMS:Lcom/miui/server/process/ProcessManagerInternal;

    .line 75
    :cond_0
    iget v0, p0, Lcom/miui/server/sptm/PreLoadStrategy;->mPreloadType:I

    if-nez v0, :cond_1

    .line 76
    return-void

    .line 79
    :cond_1
    if-eqz p1, :cond_2

    .line 80
    invoke-direct {p0}, Lcom/miui/server/sptm/PreLoadStrategy;->preloadNextApps()V

    .line 81
    iget-object v0, p0, Lcom/miui/server/sptm/PreLoadStrategy;->mPMS:Lcom/miui/server/process/ProcessManagerInternal;

    if-eqz v0, :cond_6

    .line 82
    invoke-virtual {v0, p1}, Lcom/miui/server/process/ProcessManagerInternal;->setSpeedTestState(Z)V

    goto :goto_0

    .line 85
    :cond_2
    const/4 v1, 0x3

    if-le v0, v1, :cond_3

    iget-object v0, p0, Lcom/miui/server/sptm/PreLoadStrategy;->mPMS:Lcom/miui/server/process/ProcessManagerInternal;

    if-eqz v0, :cond_3

    iget-object v0, p0, Lcom/miui/server/sptm/PreLoadStrategy;->mAppStartedRecords:Ljava/util/HashMap;

    .line 86
    invoke-virtual {v0}, Ljava/util/HashMap;->size()I

    move-result v0

    sget v2, Lcom/miui/server/sptm/SpeedTestModeServiceImpl;->STARTED_APPCOUNT:I

    if-lt v0, v2, :cond_4

    :cond_3
    iget v0, p0, Lcom/miui/server/sptm/PreLoadStrategy;->mPreloadType:I

    if-gt v0, v1, :cond_5

    iget-object v0, p0, Lcom/miui/server/sptm/PreLoadStrategy;->mPMS:Lcom/miui/server/process/ProcessManagerInternal;

    if-eqz v0, :cond_5

    iget-object v0, p0, Lcom/miui/server/sptm/PreLoadStrategy;->mAppStartedRecords:Ljava/util/HashMap;

    .line 88
    invoke-virtual {v0}, Ljava/util/HashMap;->size()I

    move-result v0

    const/16 v1, 0xe

    if-ge v0, v1, :cond_5

    .line 90
    :cond_4
    iget-object v0, p0, Lcom/miui/server/sptm/PreLoadStrategy;->mPMS:Lcom/miui/server/process/ProcessManagerInternal;

    invoke-virtual {v0, p1}, Lcom/miui/server/process/ProcessManagerInternal;->setSpeedTestState(Z)V

    .line 92
    :cond_5
    invoke-virtual {p0}, Lcom/miui/server/sptm/PreLoadStrategy;->reset()V

    .line 94
    :cond_6
    :goto_0
    return-void
.end method

.method public reset()V
    .locals 1

    .line 154
    iget-object v0, p0, Lcom/miui/server/sptm/PreLoadStrategy;->mAppStartedRecords:Ljava/util/HashMap;

    invoke-virtual {v0}, Ljava/util/HashMap;->clear()V

    .line 155
    const/4 v0, 0x0

    iput v0, p0, Lcom/miui/server/sptm/PreLoadStrategy;->mLastPreloadStartedAppCount:I

    .line 156
    iput v0, p0, Lcom/miui/server/sptm/PreLoadStrategy;->mNextPreloadTargetIndex:I

    .line 157
    return-void
.end method
