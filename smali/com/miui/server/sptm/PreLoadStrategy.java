class com.miui.server.sptm.PreLoadStrategy implements com.miui.server.sptm.SpeedTestModeServiceImpl$Strategy {
	 /* .source "PreLoadStrategy.java" */
	 /* # interfaces */
	 /* # annotations */
	 /* .annotation system Ldalvik/annotation/MemberClasses; */
	 /* value = { */
	 /* Lcom/miui/server/sptm/PreLoadStrategy$PreloadConfigs;, */
	 /* Lcom/miui/server/sptm/PreLoadStrategy$AppStartRecord; */
	 /* } */
} // .end annotation
/* # static fields */
private static final java.lang.String PUBG_PACKAGE_NAME;
private static final java.lang.String SGAME_PACKAGE_NAME;
private static final java.lang.String STOP_TIME_PACKAGES_PREFIX;
/* # instance fields */
private final Boolean DEBUG;
private final java.lang.String TAG;
private java.util.HashMap mAppStartedRecords;
/* .annotation system Ldalvik/annotation/Signature; */
/* value = { */
/* "Ljava/util/HashMap<", */
/* "Ljava/lang/String;", */
/* "Lcom/miui/server/sptm/PreLoadStrategy$AppStartRecord;", */
/* ">;" */
/* } */
} // .end annotation
} // .end field
private Boolean mIsInSpeedTestMode;
private Boolean mIsLowMemoryDevice;
private Integer mLastPreloadStartedAppCount;
private Integer mNextPreloadTargetIndex;
private Integer mNormalAppCount;
private Integer mNormalAppCountNew;
private Integer mNormalAppCountOld;
private com.miui.server.process.ProcessManagerInternal mPMS;
private com.miui.server.sptm.PreLoadStrategy$PreloadConfigs mPreloadConfigs;
private Integer mPreloadType;
private com.miui.server.sptm.SpeedTestModeServiceImpl mSpeedTestModeService;
/* # direct methods */
static Boolean -$$Nest$fgetDEBUG ( com.miui.server.sptm.PreLoadStrategy p0 ) { //bridge//synthethic
/* .locals 0 */
/* iget-boolean p0, p0, Lcom/miui/server/sptm/PreLoadStrategy;->DEBUG:Z */
} // .end method
static java.lang.String -$$Nest$fgetTAG ( com.miui.server.sptm.PreLoadStrategy p0 ) { //bridge//synthethic
/* .locals 0 */
p0 = this.TAG;
} // .end method
static Boolean -$$Nest$fgetmIsLowMemoryDevice ( com.miui.server.sptm.PreLoadStrategy p0 ) { //bridge//synthethic
/* .locals 0 */
/* iget-boolean p0, p0, Lcom/miui/server/sptm/PreLoadStrategy;->mIsLowMemoryDevice:Z */
} // .end method
static Integer -$$Nest$fgetmNormalAppCount ( com.miui.server.sptm.PreLoadStrategy p0 ) { //bridge//synthethic
/* .locals 0 */
/* iget p0, p0, Lcom/miui/server/sptm/PreLoadStrategy;->mNormalAppCount:I */
} // .end method
static Integer -$$Nest$fgetmPreloadType ( com.miui.server.sptm.PreLoadStrategy p0 ) { //bridge//synthethic
/* .locals 0 */
/* iget p0, p0, Lcom/miui/server/sptm/PreLoadStrategy;->mPreloadType:I */
} // .end method
static com.miui.server.sptm.SpeedTestModeServiceImpl -$$Nest$fgetmSpeedTestModeService ( com.miui.server.sptm.PreLoadStrategy p0 ) { //bridge//synthethic
/* .locals 0 */
p0 = this.mSpeedTestModeService;
} // .end method
 com.miui.server.sptm.PreLoadStrategy ( ) {
/* .locals 5 */
/* .line 20 */
/* invoke-direct {p0}, Ljava/lang/Object;-><init>()V */
/* .line 22 */
/* nop */
/* .line 23 */
com.miui.server.sptm.SpeedTestModeServiceImpl .getInstance ( );
this.mSpeedTestModeService = v0;
/* .line 25 */
final String v0 = "SPTM"; // const-string v0, "SPTM"
this.TAG = v0;
/* .line 26 */
/* sget-boolean v0, Lcom/miui/server/sptm/SpeedTestModeServiceImpl;->DEBUG:Z */
/* iput-boolean v0, p0, Lcom/miui/server/sptm/PreLoadStrategy;->DEBUG:Z */
/* .line 31 */
/* new-instance v0, Lcom/miui/server/sptm/PreLoadStrategy$PreloadConfigs; */
/* invoke-direct {v0, p0}, Lcom/miui/server/sptm/PreLoadStrategy$PreloadConfigs;-><init>(Lcom/miui/server/sptm/PreLoadStrategy;)V */
this.mPreloadConfigs = v0;
/* .line 33 */
/* new-instance v0, Ljava/util/HashMap; */
/* invoke-direct {v0}, Ljava/util/HashMap;-><init>()V */
this.mAppStartedRecords = v0;
/* .line 34 */
v0 = this.mSpeedTestModeService;
v0 = (( com.miui.server.sptm.SpeedTestModeServiceImpl ) v0 ).getPreloadCloudType ( ); // invoke-virtual {v0}, Lcom/miui/server/sptm/SpeedTestModeServiceImpl;->getPreloadCloudType()I
/* iput v0, p0, Lcom/miui/server/sptm/PreLoadStrategy;->mPreloadType:I */
/* .line 36 */
v0 = v0 = com.miui.server.sptm.SpeedTestModeServiceImpl.PRELOAD_APPS;
v1 = com.miui.server.sptm.SpeedTestModeServiceImpl.PRELOAD_GAME_APPS;
v1 = /* .line 37 */
/* sub-int/2addr v0, v1 */
/* iput v0, p0, Lcom/miui/server/sptm/PreLoadStrategy;->mNormalAppCountOld:I */
/* .line 38 */
v0 = v0 = com.miui.server.sptm.SpeedTestModeServiceImpl.PRELOAD_APPS_NEW;
/* iput v0, p0, Lcom/miui/server/sptm/PreLoadStrategy;->mNormalAppCountNew:I */
/* .line 39 */
/* iget v1, p0, Lcom/miui/server/sptm/PreLoadStrategy;->mPreloadType:I */
int v2 = 3; // const/4 v2, 0x3
/* if-le v1, v2, :cond_0 */
} // :cond_0
/* iget v0, p0, Lcom/miui/server/sptm/PreLoadStrategy;->mNormalAppCountOld:I */
} // :goto_0
/* iput v0, p0, Lcom/miui/server/sptm/PreLoadStrategy;->mNormalAppCount:I */
/* .line 40 */
int v0 = 0; // const/4 v0, 0x0
/* iput v0, p0, Lcom/miui/server/sptm/PreLoadStrategy;->mLastPreloadStartedAppCount:I */
/* .line 41 */
/* iput v0, p0, Lcom/miui/server/sptm/PreLoadStrategy;->mNextPreloadTargetIndex:I */
/* .line 42 */
/* iput-boolean v0, p0, Lcom/miui/server/sptm/PreLoadStrategy;->mIsInSpeedTestMode:Z */
/* .line 43 */
/* sget-wide v1, Lcom/miui/server/sptm/SpeedTestModeServiceImpl;->TOTAL_MEMORY:J */
/* const-wide/16 v3, 0x2000 */
/* cmp-long v1, v1, v3 */
/* if-gez v1, :cond_1 */
int v0 = 1; // const/4 v0, 0x1
} // :cond_1
/* iput-boolean v0, p0, Lcom/miui/server/sptm/PreLoadStrategy;->mIsLowMemoryDevice:Z */
return;
} // .end method
private void preloadNextApps ( ) {
/* .locals 8 */
/* .line 97 */
/* iget-boolean v0, p0, Lcom/miui/server/sptm/PreLoadStrategy;->mIsInSpeedTestMode:Z */
/* if-nez v0, :cond_0 */
/* .line 98 */
return;
/* .line 101 */
} // :cond_0
v0 = this.mAppStartedRecords;
v0 = (( java.util.HashMap ) v0 ).size ( ); // invoke-virtual {v0}, Ljava/util/HashMap;->size()I
/* .line 102 */
/* .local v0, "appStartedCount":I */
/* .line 104 */
/* .local v1, "preloadThreshold":I */
/* iget-boolean v2, p0, Lcom/miui/server/sptm/PreLoadStrategy;->mIsLowMemoryDevice:Z */
int v3 = 2; // const/4 v3, 0x2
/* if-nez v2, :cond_1 */
/* iget v2, p0, Lcom/miui/server/sptm/PreLoadStrategy;->mPreloadType:I */
/* if-le v2, v3, :cond_1 */
/* iget v2, p0, Lcom/miui/server/sptm/PreLoadStrategy;->mNextPreloadTargetIndex:I */
/* iget v4, p0, Lcom/miui/server/sptm/PreLoadStrategy;->mNormalAppCountNew:I */
/* if-gt v2, v4, :cond_1 */
/* .line 106 */
int v1 = 2; // const/4 v1, 0x2
/* .line 108 */
} // :cond_1
/* iget v2, p0, Lcom/miui/server/sptm/PreLoadStrategy;->mLastPreloadStartedAppCount:I */
if ( v2 != null) { // if-eqz v2, :cond_2
/* .line 109 */
/* add-int/2addr v2, v1 */
/* .line 110 */
} // :cond_2
/* nop */
} // :goto_0
/* nop */
/* .line 111 */
/* .local v2, "nextPreloadAppStartedThreshold":I */
/* iget-boolean v4, p0, Lcom/miui/server/sptm/PreLoadStrategy;->DEBUG:Z */
if ( v4 != null) { // if-eqz v4, :cond_3
/* .line 112 */
v4 = this.TAG;
/* .line 113 */
java.lang.Integer .valueOf ( v0 );
java.lang.Integer .valueOf ( v2 );
/* iget v7, p0, Lcom/miui/server/sptm/PreLoadStrategy;->mNormalAppCountNew:I */
java.lang.Integer .valueOf ( v7 );
/* filled-new-array {v5, v6, v7}, [Ljava/lang/Object; */
/* .line 112 */
final String v6 = "preloadNextApps: cur_apps: %s, threshold: %s, pre_apps: %s"; // const-string v6, "preloadNextApps: cur_apps: %s, threshold: %s, pre_apps: %s"
java.lang.String .format ( v6,v5 );
android.util.Slog .d ( v4,v5 );
/* .line 115 */
} // :cond_3
/* if-ge v0, v2, :cond_4 */
/* .line 116 */
return;
/* .line 118 */
} // :cond_4
/* iput v0, p0, Lcom/miui/server/sptm/PreLoadStrategy;->mLastPreloadStartedAppCount:I */
/* .line 120 */
/* iget v4, p0, Lcom/miui/server/sptm/PreLoadStrategy;->mPreloadType:I */
int v5 = 3; // const/4 v5, 0x3
/* if-le v4, v5, :cond_5 */
/* iget-boolean v5, p0, Lcom/miui/server/sptm/PreLoadStrategy;->mIsLowMemoryDevice:Z */
/* if-nez v5, :cond_5 */
/* .line 121 */
/* iget v3, p0, Lcom/miui/server/sptm/PreLoadStrategy;->mNextPreloadTargetIndex:I */
v4 = v4 = com.miui.server.sptm.SpeedTestModeServiceImpl.PRELOAD_APPS_NEW;
/* if-ge v3, v4, :cond_7 */
/* .line 122 */
v3 = com.miui.server.sptm.SpeedTestModeServiceImpl.PRELOAD_APPS_NEW;
/* iget v4, p0, Lcom/miui/server/sptm/PreLoadStrategy;->mNextPreloadTargetIndex:I */
/* add-int/lit8 v5, v4, 0x1 */
/* iput v5, p0, Lcom/miui/server/sptm/PreLoadStrategy;->mNextPreloadTargetIndex:I */
/* check-cast v3, Ljava/lang/String; */
/* invoke-direct {p0, v3}, Lcom/miui/server/sptm/PreLoadStrategy;->preloadPackage(Ljava/lang/String;)V */
/* .line 124 */
} // :cond_5
/* if-le v4, v3, :cond_6 */
/* iget-boolean v3, p0, Lcom/miui/server/sptm/PreLoadStrategy;->mIsLowMemoryDevice:Z */
/* if-nez v3, :cond_6 */
/* .line 125 */
/* iget v3, p0, Lcom/miui/server/sptm/PreLoadStrategy;->mNextPreloadTargetIndex:I */
v4 = v4 = com.miui.server.sptm.SpeedTestModeServiceImpl.PRELOAD_APPS;
/* if-ge v3, v4, :cond_7 */
/* .line 126 */
v3 = com.miui.server.sptm.SpeedTestModeServiceImpl.PRELOAD_APPS;
/* iget v4, p0, Lcom/miui/server/sptm/PreLoadStrategy;->mNextPreloadTargetIndex:I */
/* add-int/lit8 v5, v4, 0x1 */
/* iput v5, p0, Lcom/miui/server/sptm/PreLoadStrategy;->mNextPreloadTargetIndex:I */
/* check-cast v3, Ljava/lang/String; */
/* invoke-direct {p0, v3}, Lcom/miui/server/sptm/PreLoadStrategy;->preloadPackage(Ljava/lang/String;)V */
/* .line 128 */
} // :cond_6
/* if-lez v4, :cond_7 */
/* iget v3, p0, Lcom/miui/server/sptm/PreLoadStrategy;->mNextPreloadTargetIndex:I */
v4 = com.miui.server.sptm.SpeedTestModeServiceImpl.PRELOAD_GAME_APPS;
v4 = /* .line 129 */
/* if-ge v3, v4, :cond_7 */
/* .line 130 */
v3 = com.miui.server.sptm.SpeedTestModeServiceImpl.PRELOAD_GAME_APPS;
/* iget v4, p0, Lcom/miui/server/sptm/PreLoadStrategy;->mNextPreloadTargetIndex:I */
/* add-int/lit8 v5, v4, 0x1 */
/* iput v5, p0, Lcom/miui/server/sptm/PreLoadStrategy;->mNextPreloadTargetIndex:I */
/* check-cast v3, Ljava/lang/String; */
/* invoke-direct {p0, v3}, Lcom/miui/server/sptm/PreLoadStrategy;->preloadPackage(Ljava/lang/String;)V */
/* .line 132 */
} // :cond_7
} // :goto_1
return;
} // .end method
private void preloadPackage ( java.lang.String p0 ) {
/* .locals 7 */
/* .param p1, "packageName" # Ljava/lang/String; */
/* .line 135 */
v0 = android.text.TextUtils .isEmpty ( p1 );
if ( v0 != null) { // if-eqz v0, :cond_0
/* .line 136 */
return;
/* .line 139 */
} // :cond_0
try { // :try_start_0
v0 = this.mPreloadConfigs;
(( com.miui.server.sptm.PreLoadStrategy$PreloadConfigs ) v0 ).find ( p1 ); // invoke-virtual {v0, p1}, Lcom/miui/server/sptm/PreLoadStrategy$PreloadConfigs;->find(Ljava/lang/String;)Lmiui/process/LifecycleConfig;
/* .line 140 */
/* .local v0, "lifecycleConfig":Lmiui/process/LifecycleConfig; */
if ( v0 != null) { // if-eqz v0, :cond_1
/* .line 141 */
miui.process.ProcessManagerNative .getDefault ( );
int v2 = 0; // const/4 v2, 0x0
v1 = int v3 = 1; // const/4 v3, 0x1
/* .line 144 */
/* .local v1, "res":I */
/* iget-boolean v4, p0, Lcom/miui/server/sptm/PreLoadStrategy;->DEBUG:Z */
if ( v4 != null) { // if-eqz v4, :cond_1
/* .line 145 */
v4 = this.TAG;
final String v5 = "preloadNextApps: preload: %s, res=%s"; // const-string v5, "preloadNextApps: preload: %s, res=%s"
int v6 = 2; // const/4 v6, 0x2
/* new-array v6, v6, [Ljava/lang/Object; */
/* aput-object p1, v6, v2 */
/* .line 146 */
java.lang.Integer .valueOf ( v1 );
/* aput-object v2, v6, v3 */
/* .line 145 */
java.lang.String .format ( v5,v6 );
android.util.Slog .d ( v4,v2 );
/* :try_end_0 */
/* .catch Landroid/os/RemoteException; {:try_start_0 ..:try_end_0} :catch_0 */
/* .line 150 */
} // .end local v0 # "lifecycleConfig":Lmiui/process/LifecycleConfig;
} // .end local v1 # "res":I
} // :cond_1
/* .line 148 */
/* :catch_0 */
/* move-exception v0 */
/* .line 149 */
/* .local v0, "e":Landroid/os/RemoteException; */
(( android.os.RemoteException ) v0 ).printStackTrace ( ); // invoke-virtual {v0}, Landroid/os/RemoteException;->printStackTrace()V
/* .line 151 */
} // .end local v0 # "e":Landroid/os/RemoteException;
} // :goto_0
return;
} // .end method
/* # virtual methods */
public void onAppStarted ( com.miui.server.sptm.PreLoadStrategy$AppStartRecord p0 ) {
/* .locals 2 */
/* .param p1, "r" # Lcom/miui/server/sptm/PreLoadStrategy$AppStartRecord; */
/* .line 61 */
v0 = com.miui.server.sptm.SpeedTestModeServiceImpl.SPEED_TEST_APP_LIST;
v0 = v1 = this.packageName;
if ( v0 != null) { // if-eqz v0, :cond_0
/* iget v0, p0, Lcom/miui/server/sptm/PreLoadStrategy;->mPreloadType:I */
if ( v0 != null) { // if-eqz v0, :cond_0
v0 = this.mAppStartedRecords;
v1 = this.packageName;
/* .line 63 */
v0 = (( java.util.HashMap ) v0 ).containsKey ( v1 ); // invoke-virtual {v0, v1}, Ljava/util/HashMap;->containsKey(Ljava/lang/Object;)Z
/* if-nez v0, :cond_0 */
/* .line 64 */
v0 = this.mAppStartedRecords;
v1 = this.packageName;
(( java.util.HashMap ) v0 ).put ( v1, p1 ); // invoke-virtual {v0, v1, p1}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;
/* .line 65 */
/* invoke-direct {p0}, Lcom/miui/server/sptm/PreLoadStrategy;->preloadNextApps()V */
/* .line 67 */
} // :cond_0
return;
} // .end method
public void onNewEvent ( Integer p0 ) {
/* .locals 2 */
/* .param p1, "eventType" # I */
/* .line 54 */
int v0 = 5; // const/4 v0, 0x5
/* if-ne p1, v0, :cond_0 */
v0 = this.mPMS;
if ( v0 != null) { // if-eqz v0, :cond_0
/* .line 55 */
int v1 = 0; // const/4 v1, 0x0
(( com.miui.server.process.ProcessManagerInternal ) v0 ).setSpeedTestState ( v1 ); // invoke-virtual {v0, v1}, Lcom/miui/server/process/ProcessManagerInternal;->setSpeedTestState(Z)V
/* .line 57 */
} // :cond_0
return;
} // .end method
public void onPreloadAppStarted ( java.lang.String p0 ) {
/* .locals 0 */
/* .param p1, "packageName" # Ljava/lang/String; */
/* .line 49 */
return;
} // .end method
public void onSpeedTestModeChanged ( Boolean p0 ) {
/* .locals 3 */
/* .param p1, "isEnable" # Z */
/* .line 71 */
/* iput-boolean p1, p0, Lcom/miui/server/sptm/PreLoadStrategy;->mIsInSpeedTestMode:Z */
/* .line 72 */
v0 = this.mPMS;
/* if-nez v0, :cond_0 */
/* .line 73 */
/* const-class v0, Lcom/miui/server/process/ProcessManagerInternal; */
com.android.server.LocalServices .getService ( v0 );
/* check-cast v0, Lcom/miui/server/process/ProcessManagerInternal; */
this.mPMS = v0;
/* .line 75 */
} // :cond_0
/* iget v0, p0, Lcom/miui/server/sptm/PreLoadStrategy;->mPreloadType:I */
/* if-nez v0, :cond_1 */
/* .line 76 */
return;
/* .line 79 */
} // :cond_1
if ( p1 != null) { // if-eqz p1, :cond_2
/* .line 80 */
/* invoke-direct {p0}, Lcom/miui/server/sptm/PreLoadStrategy;->preloadNextApps()V */
/* .line 81 */
v0 = this.mPMS;
if ( v0 != null) { // if-eqz v0, :cond_6
/* .line 82 */
(( com.miui.server.process.ProcessManagerInternal ) v0 ).setSpeedTestState ( p1 ); // invoke-virtual {v0, p1}, Lcom/miui/server/process/ProcessManagerInternal;->setSpeedTestState(Z)V
/* .line 85 */
} // :cond_2
int v1 = 3; // const/4 v1, 0x3
/* if-le v0, v1, :cond_3 */
v0 = this.mPMS;
if ( v0 != null) { // if-eqz v0, :cond_3
v0 = this.mAppStartedRecords;
/* .line 86 */
v0 = (( java.util.HashMap ) v0 ).size ( ); // invoke-virtual {v0}, Ljava/util/HashMap;->size()I
/* if-lt v0, v2, :cond_4 */
} // :cond_3
/* iget v0, p0, Lcom/miui/server/sptm/PreLoadStrategy;->mPreloadType:I */
/* if-gt v0, v1, :cond_5 */
v0 = this.mPMS;
if ( v0 != null) { // if-eqz v0, :cond_5
v0 = this.mAppStartedRecords;
/* .line 88 */
v0 = (( java.util.HashMap ) v0 ).size ( ); // invoke-virtual {v0}, Ljava/util/HashMap;->size()I
/* const/16 v1, 0xe */
/* if-ge v0, v1, :cond_5 */
/* .line 90 */
} // :cond_4
v0 = this.mPMS;
(( com.miui.server.process.ProcessManagerInternal ) v0 ).setSpeedTestState ( p1 ); // invoke-virtual {v0, p1}, Lcom/miui/server/process/ProcessManagerInternal;->setSpeedTestState(Z)V
/* .line 92 */
} // :cond_5
(( com.miui.server.sptm.PreLoadStrategy ) p0 ).reset ( ); // invoke-virtual {p0}, Lcom/miui/server/sptm/PreLoadStrategy;->reset()V
/* .line 94 */
} // :cond_6
} // :goto_0
return;
} // .end method
public void reset ( ) {
/* .locals 1 */
/* .line 154 */
v0 = this.mAppStartedRecords;
(( java.util.HashMap ) v0 ).clear ( ); // invoke-virtual {v0}, Ljava/util/HashMap;->clear()V
/* .line 155 */
int v0 = 0; // const/4 v0, 0x0
/* iput v0, p0, Lcom/miui/server/sptm/PreLoadStrategy;->mLastPreloadStartedAppCount:I */
/* .line 156 */
/* iput v0, p0, Lcom/miui/server/sptm/PreLoadStrategy;->mNextPreloadTargetIndex:I */
/* .line 157 */
return;
} // .end method
