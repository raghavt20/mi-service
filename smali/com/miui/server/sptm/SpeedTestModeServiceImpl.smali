.class public Lcom/miui/server/sptm/SpeedTestModeServiceImpl;
.super Lcom/android/server/am/SpeedTestModeServiceStub;
.source "SpeedTestModeServiceImpl.java"

# interfaces
.implements Lcom/miui/app/SpeedTestModeServiceInternal;


# annotations
.annotation runtime Lcom/miui/base/annotations/MiuiStubHead;
    manifestName = "com.miui.server.sptm.SpeedTestModeServiceStub$$"
.end annotation

.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/miui/server/sptm/SpeedTestModeServiceImpl$Handler;,
        Lcom/miui/server/sptm/SpeedTestModeServiceImpl$AppUsageRecord;,
        Lcom/miui/server/sptm/SpeedTestModeServiceImpl$Strategy;
    }
.end annotation


# static fields
.field public static final COLD_START_DELAYED_TIME:J

.field public static final ENABLE_SPTM_MIN_MEMORY:I = 0x1770

.field public static final EVENT_TYPE_LOCK_SCREEN:I = 0x5

.field public static final EVENT_TYPE_ONE_KEY_CLEAN:I = 0x4

.field public static final EVENT_TYPE_PAUSE:I = 0x3

.field public static final EVENT_TYPE_PRELOAD_STARTED:I = 0x6

.field public static final EVENT_TYPE_RESUME:I = 0x2

.field public static final EVENT_TYPE_START_PROC:I = 0x1

.field public static final FAST_SWITCH_HOME_DURATION:J

.field public static final GAME_APPS:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field public static final GAME_COLD_START_DELAYED_TIME:J

.field public static final GAME_HOT_START_DELAYED_TIME:J

.field public static final HOT_START_DELAYED_TIME:J

.field private static final IGNORE_CLOUD_ENABLE:Z

.field private static final PERMISSION_DIALOG_PACKAGE_NAMES:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field public static final PRELOAD_APPS:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field public static final PRELOAD_APPS_NEW:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field public static final PRELOAD_GAME_APPS:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field public static final PRELOAD_THRESHOLD:I

.field public static final SPEED_TEST_APP_LIST:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field public static final SPEED_TEST_APP_LIST_NEW:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field public static final SPEED_TEST_APP_LIST_OLD:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private static final SPTM_ANIMATION_CLOUD_ENABLE:Ljava/lang/String; = "perf_sptm_animation_enable"

.field private static final SPTM_APP_LIST:Ljava/lang/String; = "perf_sptm_app_list"

.field private static final SPTM_CLOUD_ENABLE_NEW:Ljava/lang/String; = "perf_sptm_enable_new"

.field private static final SPTM_CLOUD_ENABLE_OLD:Ljava/lang/String; = "perf_sptm_enable_old"

.field private static final SPTM_KEY:Ljava/lang/String; = "perf_shielder_SPTM"

.field public static final SPTM_LOW_MEMORY_DEVICE_PRELOAD_CORE:I

.field public static final SPTM_LOW_MEMORY_DEVICE_THRESHOLD:I

.field private static final SPTM_MODULE_KEY:Ljava/lang/String; = "perf_shielder"

.field private static final SPTM_PRELOAD_CLOUD:Ljava/lang/String; = "perf_sptm_preload"

.field public static final STARTED_APPCOUNT:I

.field public static final START_PROC_DELAYED_TIME:J

.field public static final TAG:Ljava/lang/String; = "SPTM"

.field public static final TOTAL_MEMORY:J


# instance fields
.field private mAppLastResumedTimes:Ljava/util/HashMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/HashMap<",
            "Ljava/lang/String;",
            "Ljava/lang/Long;",
            ">;"
        }
    .end annotation
.end field

.field private mAppStartProcTimes:Ljava/util/LinkedHashMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/LinkedHashMap<",
            "Ljava/lang/String;",
            "Ljava/lang/Long;",
            ">;"
        }
    .end annotation
.end field

.field private mAppStartRecords:Ljava/util/LinkedList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/LinkedList<",
            "Lcom/miui/server/sptm/SpeedTestModeServiceImpl$AppUsageRecord;",
            ">;"
        }
    .end annotation
.end field

.field private mContext:Landroid/content/Context;

.field private mH:Lcom/miui/server/sptm/SpeedTestModeServiceImpl$Handler;

.field private mHandlerThread:Landroid/os/HandlerThread;

.field private mHomeAnimationStrategy:Lcom/miui/server/sptm/HomeAnimationStrategy;

.field private mHomePackageName:Ljava/lang/String;

.field private mIsEnableNew:Z

.field private mIsEnableOld:Z

.field private mIsEnableSPTMAnimation:Z

.field private mIsSpeedTestEnabled:Z

.field private mIsSpeedTestMode:Z

.field private mPreLoadStrategy:Lcom/miui/server/sptm/PreLoadStrategy;

.field private mPreloadType:I

.field private mSpeedTestModeState:Lcom/miui/server/sptm/SpeedTestModeState;

.field public mSpeedTestModeStrategies:Ljava/util/LinkedList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/LinkedList<",
            "Lcom/miui/server/sptm/SpeedTestModeServiceImpl$Strategy;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public static synthetic $r8$lambda$cWnX9FZHZctoyqOleSKFNet6umw(Lcom/miui/server/sptm/SpeedTestModeServiceImpl;Z)V
    .locals 0

    invoke-direct {p0, p1}, Lcom/miui/server/sptm/SpeedTestModeServiceImpl;->setEnableSpeedTestMode(Z)V

    return-void
.end method

.method static bridge synthetic -$$Nest$fgetmIsSpeedTestEnabled(Lcom/miui/server/sptm/SpeedTestModeServiceImpl;)Z
    .locals 0

    iget-boolean p0, p0, Lcom/miui/server/sptm/SpeedTestModeServiceImpl;->mIsSpeedTestEnabled:Z

    return p0
.end method

.method static bridge synthetic -$$Nest$fgetmPreLoadStrategy(Lcom/miui/server/sptm/SpeedTestModeServiceImpl;)Lcom/miui/server/sptm/PreLoadStrategy;
    .locals 0

    iget-object p0, p0, Lcom/miui/server/sptm/SpeedTestModeServiceImpl;->mPreLoadStrategy:Lcom/miui/server/sptm/PreLoadStrategy;

    return-object p0
.end method

.method static bridge synthetic -$$Nest$fgetmSpeedTestModeState(Lcom/miui/server/sptm/SpeedTestModeServiceImpl;)Lcom/miui/server/sptm/SpeedTestModeState;
    .locals 0

    iget-object p0, p0, Lcom/miui/server/sptm/SpeedTestModeServiceImpl;->mSpeedTestModeState:Lcom/miui/server/sptm/SpeedTestModeState;

    return-object p0
.end method

.method static bridge synthetic -$$Nest$mhandleEvent(Lcom/miui/server/sptm/SpeedTestModeServiceImpl;ILjava/lang/String;J)V
    .locals 0

    invoke-direct {p0, p1, p2, p3, p4}, Lcom/miui/server/sptm/SpeedTestModeServiceImpl;->handleEvent(ILjava/lang/String;J)V

    return-void
.end method

.method static bridge synthetic -$$Nest$mhandleUpdateSpeedTestMode(Lcom/miui/server/sptm/SpeedTestModeServiceImpl;Z)V
    .locals 0

    invoke-direct {p0, p1}, Lcom/miui/server/sptm/SpeedTestModeServiceImpl;->handleUpdateSpeedTestMode(Z)V

    return-void
.end method

.method static bridge synthetic -$$Nest$mupdateCloudControlParas(Lcom/miui/server/sptm/SpeedTestModeServiceImpl;)V
    .locals 0

    invoke-direct {p0}, Lcom/miui/server/sptm/SpeedTestModeServiceImpl;->updateCloudControlParas()V

    return-void
.end method

.method static constructor <clinit>()V
    .locals 18

    .line 44
    nop

    .line 45
    const-string v0, "persist.sys.miui_sptm.start_proc_delayed"

    const-wide/16 v1, 0x5dc

    invoke-static {v0, v1, v2}, Landroid/os/SystemProperties;->getLong(Ljava/lang/String;J)J

    move-result-wide v0

    sput-wide v0, Lcom/miui/server/sptm/SpeedTestModeServiceImpl;->START_PROC_DELAYED_TIME:J

    .line 46
    nop

    .line 47
    const-string v0, "persist.sys.miui_sptm.fast_home"

    const/16 v1, 0x1388

    invoke-static {v0, v1}, Landroid/os/SystemProperties;->getInt(Ljava/lang/String;I)I

    move-result v0

    int-to-long v0, v0

    sput-wide v0, Lcom/miui/server/sptm/SpeedTestModeServiceImpl;->FAST_SWITCH_HOME_DURATION:J

    .line 48
    nop

    .line 49
    const-string v0, "persist.sys.miui_sptm.hot_start_delayed"

    const-wide/16 v1, 0x4e20

    invoke-static {v0, v1, v2}, Landroid/os/SystemProperties;->getLong(Ljava/lang/String;J)J

    move-result-wide v3

    sput-wide v3, Lcom/miui/server/sptm/SpeedTestModeServiceImpl;->HOT_START_DELAYED_TIME:J

    .line 50
    nop

    .line 51
    const-string v0, "persist.sys.miui_sptm.cold_start_delayed"

    invoke-static {v0, v1, v2}, Landroid/os/SystemProperties;->getLong(Ljava/lang/String;J)J

    move-result-wide v0

    sput-wide v0, Lcom/miui/server/sptm/SpeedTestModeServiceImpl;->COLD_START_DELAYED_TIME:J

    .line 52
    nop

    .line 53
    const-string v0, "persist.sys.miui_sptm.game_hot_start_delayed"

    const-wide/16 v1, 0x7530

    invoke-static {v0, v1, v2}, Landroid/os/SystemProperties;->getLong(Ljava/lang/String;J)J

    move-result-wide v0

    sput-wide v0, Lcom/miui/server/sptm/SpeedTestModeServiceImpl;->GAME_HOT_START_DELAYED_TIME:J

    .line 54
    nop

    .line 55
    const-string v0, "persist.sys.miui_sptm.game_cold_start_delayed"

    const-wide/32 v1, 0xea60

    invoke-static {v0, v1, v2}, Landroid/os/SystemProperties;->getLong(Ljava/lang/String;J)J

    move-result-wide v0

    sput-wide v0, Lcom/miui/server/sptm/SpeedTestModeServiceImpl;->GAME_COLD_START_DELAYED_TIME:J

    .line 56
    nop

    .line 57
    const-string v0, "persist.sys.miui_sptm.pl_threshold"

    const/4 v1, 0x6

    invoke-static {v0, v1}, Landroid/os/SystemProperties;->getInt(Ljava/lang/String;I)I

    move-result v0

    sput v0, Lcom/miui/server/sptm/SpeedTestModeServiceImpl;->PRELOAD_THRESHOLD:I

    .line 58
    nop

    .line 59
    const-string v0, "persist.sys.miui_sptm.strarted_count"

    const/16 v1, 0x14

    invoke-static {v0, v1}, Landroid/os/SystemProperties;->getInt(Ljava/lang/String;I)I

    move-result v0

    sput v0, Lcom/miui/server/sptm/SpeedTestModeServiceImpl;->STARTED_APPCOUNT:I

    .line 60
    const-string v0, "persist.sys.miui_stpm.low_mem_device"

    const/16 v2, 0xc

    invoke-static {v0, v2}, Landroid/os/SystemProperties;->getInt(Ljava/lang/String;I)I

    move-result v0

    mul-int/lit16 v0, v0, 0x3e8

    sput v0, Lcom/miui/server/sptm/SpeedTestModeServiceImpl;->SPTM_LOW_MEMORY_DEVICE_THRESHOLD:I

    .line 63
    const-string v0, "persist.sys.miui_stpm.low_mem_device_pl_cores"

    const/4 v2, 0x5

    invoke-static {v0, v2}, Landroid/os/SystemProperties;->getInt(Ljava/lang/String;I)I

    move-result v0

    sput v0, Lcom/miui/server/sptm/SpeedTestModeServiceImpl;->SPTM_LOW_MEMORY_DEVICE_PRELOAD_CORE:I

    .line 65
    invoke-static {}, Landroid/os/Process;->getTotalMemory()J

    move-result-wide v2

    shr-long v0, v2, v1

    sput-wide v0, Lcom/miui/server/sptm/SpeedTestModeServiceImpl;->TOTAL_MEMORY:J

    .line 66
    nop

    .line 67
    const-string v0, "persist.sys.miui_sptm.ignore_cloud_enable"

    const/4 v1, 0x0

    invoke-static {v0, v1}, Landroid/os/SystemProperties;->getBoolean(Ljava/lang/String;Z)Z

    move-result v0

    sput-boolean v0, Lcom/miui/server/sptm/SpeedTestModeServiceImpl;->IGNORE_CLOUD_ENABLE:Z

    .line 92
    new-instance v0, Ljava/util/LinkedList;

    invoke-direct {v0}, Ljava/util/LinkedList;-><init>()V

    sput-object v0, Lcom/miui/server/sptm/SpeedTestModeServiceImpl;->SPEED_TEST_APP_LIST_OLD:Ljava/util/List;

    .line 93
    new-instance v2, Ljava/util/LinkedList;

    invoke-direct {v2}, Ljava/util/LinkedList;-><init>()V

    sput-object v2, Lcom/miui/server/sptm/SpeedTestModeServiceImpl;->SPEED_TEST_APP_LIST_NEW:Ljava/util/List;

    .line 94
    const-string v3, "persist.sys.miui_sptm_new.enable"

    invoke-static {v3, v1}, Landroid/os/SystemProperties;->getBoolean(Ljava/lang/String;Z)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 96
    move-object v1, v2

    goto :goto_0

    :cond_0
    move-object v1, v0

    :goto_0
    sput-object v1, Lcom/miui/server/sptm/SpeedTestModeServiceImpl;->SPEED_TEST_APP_LIST:Ljava/util/List;

    .line 99
    const-string v1, "com.ss.android.ugc.aweme"

    invoke-interface {v2, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 100
    const-string v3, "com.quark.browser"

    invoke-interface {v2, v3}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 101
    const-string v3, "com.xingin.xhs"

    invoke-interface {v2, v3}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 102
    const-string v4, "com.dragon.read"

    invoke-interface {v2, v4}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 103
    const-string v4, "com.autonavi.minimap"

    invoke-interface {v2, v4}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 104
    const-string v5, "com.jingdong.app.mall"

    invoke-interface {v2, v5}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 105
    const-string v6, "com.taobao.idlefish"

    invoke-interface {v2, v6}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 106
    const-string v6, "com.shizhuang.duapp"

    invoke-interface {v2, v6}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 107
    const-string v6, "com.qiyi.video"

    invoke-interface {v2, v6}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 108
    const-string v7, "com.Qunar"

    invoke-interface {v2, v7}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 109
    const-string v7, "com.dianping.v1"

    invoke-interface {v2, v7}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 110
    const-string v7, "com.sankuai.meituan.takeoutnew"

    invoke-interface {v2, v7}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 111
    const-string v8, "com.sina.weibo"

    invoke-interface {v2, v8}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 112
    const-string v9, "com.ss.android.article.news"

    invoke-interface {v2, v9}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 113
    const-string v9, "cn.wps.moffice_eng"

    invoke-interface {v2, v9}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 114
    const-string v9, "com.xt.retouch"

    invoke-interface {v2, v9}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 115
    const-string/jumbo v9, "tv.danmaku.bili"

    invoke-interface {v2, v9}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 116
    const-string v10, "com.taobao.taobao"

    invoke-interface {v2, v10}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 117
    const-string v11, "com.miHoYo.ys.mi"

    invoke-interface {v2, v11}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 118
    const-string v12, "com.netease.cloudmusic"

    invoke-interface {v2, v12}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 119
    const-string v13, "cn.xiaochuankeji.tieba"

    invoke-interface {v2, v13}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 120
    const-string v13, "com.zhihu.android"

    invoke-interface {v2, v13}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 121
    const-string v14, "com.lemon.lv"

    invoke-interface {v2, v14}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 122
    const-string v15, "com.tencent.mobileqq"

    invoke-interface {v2, v15}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 123
    move-object/from16 v16, v14

    const-string v14, "com.baidu.tieba"

    invoke-interface {v2, v14}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 124
    const-string v14, "com.smile.gifmaker"

    invoke-interface {v2, v14}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 125
    move-object/from16 v17, v14

    const-string v14, "com.baidu.searchbox"

    invoke-interface {v2, v14}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 126
    const-string v14, "com.xunmeng.pinduoduo"

    invoke-interface {v2, v14}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 127
    const-string v14, "com.manmanbuy.bijia"

    invoke-interface {v2, v14}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 131
    const-string v2, "com.tencent.mm"

    invoke-interface {v0, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 132
    invoke-interface {v0, v15}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 133
    invoke-interface {v0, v8}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 134
    const-string v2, "com.eg.android.AlipayGphone"

    invoke-interface {v0, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 135
    invoke-interface {v0, v10}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 136
    invoke-interface {v0, v5}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 137
    const-string v2, "com.ss.android.lark"

    invoke-interface {v0, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 138
    invoke-interface {v0, v3}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 139
    const-string v2, "com.tencent.qqmusic"

    invoke-interface {v0, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 140
    const-string v2, "com.MobileTicket"

    invoke-interface {v0, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 141
    invoke-interface {v0, v6}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 142
    invoke-interface {v0, v12}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 143
    invoke-interface {v0, v13}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 144
    invoke-interface {v0, v4}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 145
    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 146
    invoke-interface {v0, v9}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 147
    const-string v1, "com.hicorenational.antifraud"

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 148
    invoke-interface {v0, v11}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 151
    new-instance v0, Ljava/util/LinkedList;

    invoke-direct {v0}, Ljava/util/LinkedList;-><init>()V

    sput-object v0, Lcom/miui/server/sptm/SpeedTestModeServiceImpl;->PRELOAD_GAME_APPS:Ljava/util/List;

    .line 154
    const-string v1, "com.tencent.tmgp.sgame"

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 155
    const-string v2, "com.tencent.tmgp.pubgmhd"

    invoke-interface {v0, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 159
    new-instance v3, Ljava/util/LinkedList;

    invoke-direct {v3}, Ljava/util/LinkedList;-><init>()V

    sput-object v3, Lcom/miui/server/sptm/SpeedTestModeServiceImpl;->GAME_APPS:Ljava/util/List;

    .line 162
    invoke-interface {v3, v0}, Ljava/util/List;->addAll(Ljava/util/Collection;)Z

    .line 163
    invoke-interface {v3, v11}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 164
    const-string v4, "com.miHoYo.Yuanshen"

    invoke-interface {v3, v4}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 167
    new-instance v3, Ljava/util/LinkedList;

    invoke-direct {v3}, Ljava/util/LinkedList;-><init>()V

    sput-object v3, Lcom/miui/server/sptm/SpeedTestModeServiceImpl;->PRELOAD_APPS_NEW:Ljava/util/List;

    .line 170
    invoke-interface {v3, v6}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 171
    invoke-interface {v3, v7}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 172
    invoke-interface {v3, v9}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 173
    invoke-interface {v3, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 174
    invoke-interface {v3, v12}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 175
    invoke-interface {v3, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 176
    move-object/from16 v1, v16

    invoke-interface {v3, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 177
    move-object/from16 v1, v17

    invoke-interface {v3, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 180
    new-instance v1, Ljava/util/LinkedList;

    invoke-direct {v1}, Ljava/util/LinkedList;-><init>()V

    sput-object v1, Lcom/miui/server/sptm/SpeedTestModeServiceImpl;->PRELOAD_APPS:Ljava/util/List;

    .line 183
    invoke-interface {v1, v12}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 184
    invoke-interface {v1, v0}, Ljava/util/List;->addAll(Ljava/util/Collection;)Z

    .line 187
    new-instance v0, Ljava/util/LinkedList;

    invoke-direct {v0}, Ljava/util/LinkedList;-><init>()V

    sput-object v0, Lcom/miui/server/sptm/SpeedTestModeServiceImpl;->PERMISSION_DIALOG_PACKAGE_NAMES:Ljava/util/List;

    .line 190
    const-string v1, "com.miui.securitycenter"

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 191
    const-string v1, "com.lbe.security.miui"

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 192
    return-void
.end method

.method constructor <init>()V
    .locals 4

    .line 256
    invoke-direct {p0}, Lcom/android/server/am/SpeedTestModeServiceStub;-><init>()V

    .line 68
    nop

    .line 69
    const-string v0, "persist.sys.miui_sptm.enable"

    const/4 v1, 0x0

    invoke-static {v0, v1}, Landroid/os/SystemProperties;->getBoolean(Ljava/lang/String;Z)Z

    move-result v0

    iput-boolean v0, p0, Lcom/miui/server/sptm/SpeedTestModeServiceImpl;->mIsEnableOld:Z

    .line 70
    const/4 v2, 0x1

    if-nez v0, :cond_0

    .line 71
    const-string v0, "persist.sys.miui_sptm_new.enable"

    invoke-static {v0, v1}, Landroid/os/SystemProperties;->getBoolean(Ljava/lang/String;Z)Z

    move-result v0

    if-eqz v0, :cond_0

    move v0, v2

    goto :goto_0

    :cond_0
    move v0, v1

    :goto_0
    iput-boolean v0, p0, Lcom/miui/server/sptm/SpeedTestModeServiceImpl;->mIsEnableNew:Z

    .line 72
    nop

    .line 73
    const-string v0, "persist.sys.miui_sptm.enable_pl_type"

    const/16 v3, 0x8

    invoke-static {v0, v3}, Landroid/os/SystemProperties;->getInt(Ljava/lang/String;I)I

    move-result v0

    iput v0, p0, Lcom/miui/server/sptm/SpeedTestModeServiceImpl;->mPreloadType:I

    .line 74
    nop

    .line 75
    const-string v0, "persist.sys.miui_sptm_animation.enable"

    invoke-static {v0, v2}, Landroid/os/SystemProperties;->getBoolean(Ljava/lang/String;Z)Z

    move-result v0

    iput-boolean v0, p0, Lcom/miui/server/sptm/SpeedTestModeServiceImpl;->mIsEnableSPTMAnimation:Z

    .line 194
    const-string v0, "com.miui.home"

    iput-object v0, p0, Lcom/miui/server/sptm/SpeedTestModeServiceImpl;->mHomePackageName:Ljava/lang/String;

    .line 196
    iput-boolean v2, p0, Lcom/miui/server/sptm/SpeedTestModeServiceImpl;->mIsSpeedTestEnabled:Z

    .line 197
    iput-boolean v1, p0, Lcom/miui/server/sptm/SpeedTestModeServiceImpl;->mIsSpeedTestMode:Z

    .line 198
    new-instance v0, Ljava/util/LinkedList;

    invoke-direct {v0}, Ljava/util/LinkedList;-><init>()V

    iput-object v0, p0, Lcom/miui/server/sptm/SpeedTestModeServiceImpl;->mSpeedTestModeStrategies:Ljava/util/LinkedList;

    .line 201
    new-instance v0, Ljava/util/LinkedList;

    invoke-direct {v0}, Ljava/util/LinkedList;-><init>()V

    iput-object v0, p0, Lcom/miui/server/sptm/SpeedTestModeServiceImpl;->mAppStartRecords:Ljava/util/LinkedList;

    .line 202
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    iput-object v0, p0, Lcom/miui/server/sptm/SpeedTestModeServiceImpl;->mAppLastResumedTimes:Ljava/util/HashMap;

    .line 203
    new-instance v0, Lcom/miui/server/sptm/SpeedTestModeServiceImpl$1;

    invoke-direct {v0, p0}, Lcom/miui/server/sptm/SpeedTestModeServiceImpl$1;-><init>(Lcom/miui/server/sptm/SpeedTestModeServiceImpl;)V

    iput-object v0, p0, Lcom/miui/server/sptm/SpeedTestModeServiceImpl;->mAppStartProcTimes:Ljava/util/LinkedHashMap;

    .line 213
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/miui/server/sptm/SpeedTestModeServiceImpl;->mContext:Landroid/content/Context;

    .line 257
    const-class v0, Lcom/miui/app/SpeedTestModeServiceInternal;

    invoke-static {v0, p0}, Lcom/android/server/LocalServices;->addService(Ljava/lang/Class;Ljava/lang/Object;)V

    .line 258
    return-void
.end method

.method private static createMsgData(ILjava/lang/String;J)Landroid/os/Bundle;
    .locals 2
    .param p0, "event"    # I
    .param p1, "packageName"    # Ljava/lang/String;
    .param p2, "time"    # J

    .line 631
    new-instance v0, Landroid/os/Bundle;

    invoke-direct {v0}, Landroid/os/Bundle;-><init>()V

    .line 632
    .local v0, "b":Landroid/os/Bundle;
    const-string v1, "event"

    invoke-virtual {v0, v1, p0}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    .line 633
    const-string v1, "packageName"

    invoke-virtual {v0, v1, p1}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 634
    const-string/jumbo v1, "time"

    invoke-virtual {v0, v1, p2, p3}, Landroid/os/Bundle;->putLong(Ljava/lang/String;J)V

    .line 635
    return-object v0
.end method

.method public static getAmsMaxCachedProcesses()I
    .locals 4

    .line 674
    sget-wide v0, Lcom/miui/server/sptm/SpeedTestModeServiceImpl;->TOTAL_MEMORY:J

    const-wide/16 v2, 0x1770

    cmp-long v0, v0, v2

    if-gtz v0, :cond_0

    .line 675
    const/4 v0, -0x1

    return v0

    .line 677
    :cond_0
    const/16 v0, 0x3c

    return v0
.end method

.method private getAppSwitchingOperation(Ljava/util/List;Lcom/miui/server/sptm/SpeedTestModeServiceImpl$AppUsageRecord;Lcom/miui/server/sptm/PreLoadStrategy$AppStartRecord;)I
    .locals 10
    .param p2, "homeRecord"    # Lcom/miui/server/sptm/SpeedTestModeServiceImpl$AppUsageRecord;
    .param p3, "outRes"    # Lcom/miui/server/sptm/PreLoadStrategy$AppStartRecord;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List<",
            "Lcom/miui/server/sptm/SpeedTestModeServiceImpl$AppUsageRecord;",
            ">;",
            "Lcom/miui/server/sptm/SpeedTestModeServiceImpl$AppUsageRecord;",
            "Lcom/miui/server/sptm/PreLoadStrategy$AppStartRecord;",
            ")I"
        }
    .end annotation

    .line 552
    .local p1, "appUsageRecord":Ljava/util/List;, "Ljava/util/List<Lcom/miui/server/sptm/SpeedTestModeServiceImpl$AppUsageRecord;>;"
    const/4 v0, 0x1

    if-eqz p2, :cond_f

    invoke-virtual {p2}, Lcom/miui/server/sptm/SpeedTestModeServiceImpl$AppUsageRecord;->getDuration()J

    move-result-wide v1

    sget-wide v3, Lcom/miui/server/sptm/SpeedTestModeServiceImpl;->FAST_SWITCH_HOME_DURATION:J

    cmp-long v1, v1, v3

    if-ltz v1, :cond_0

    goto/16 :goto_6

    .line 557
    :cond_0
    const/4 v1, 0x2

    if-eqz p1, :cond_e

    invoke-interface {p1}, Ljava/util/List;->size()I

    move-result v2

    if-nez v2, :cond_1

    goto/16 :goto_5

    .line 561
    :cond_1
    sget-object v2, Lcom/miui/server/sptm/SpeedTestModeServiceImpl;->SPEED_TEST_APP_LIST:Ljava/util/List;

    const/4 v3, 0x0

    invoke-interface {p1, v3}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lcom/miui/server/sptm/SpeedTestModeServiceImpl$AppUsageRecord;

    iget-object v4, v4, Lcom/miui/server/sptm/SpeedTestModeServiceImpl$AppUsageRecord;->packageName:Ljava/lang/String;

    invoke-interface {v2, v4}, Ljava/util/List;->contains(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_2

    sget-object v2, Lcom/miui/server/sptm/SpeedTestModeServiceImpl;->GAME_APPS:Ljava/util/List;

    .line 562
    invoke-interface {p1, v3}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lcom/miui/server/sptm/SpeedTestModeServiceImpl$AppUsageRecord;

    iget-object v4, v4, Lcom/miui/server/sptm/SpeedTestModeServiceImpl$AppUsageRecord;->packageName:Ljava/lang/String;

    invoke-interface {v2, v4}, Ljava/util/List;->contains(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_2

    .line 563
    return v0

    .line 567
    :cond_2
    const/4 v2, 0x0

    .line 568
    .local v2, "coldStartPackageName":Ljava/lang/String;
    const-wide/16 v4, 0x0

    .line 569
    .local v4, "totalDuration":J
    invoke-interface {p1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v6

    :goto_0
    invoke-interface {v6}, Ljava/util/Iterator;->hasNext()Z

    move-result v7

    if-eqz v7, :cond_4

    invoke-interface {v6}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v7

    check-cast v7, Lcom/miui/server/sptm/SpeedTestModeServiceImpl$AppUsageRecord;

    .line 570
    .local v7, "aur":Lcom/miui/server/sptm/SpeedTestModeServiceImpl$AppUsageRecord;
    invoke-virtual {v7}, Lcom/miui/server/sptm/SpeedTestModeServiceImpl$AppUsageRecord;->getDuration()J

    move-result-wide v8

    add-long/2addr v4, v8

    .line 571
    if-nez v2, :cond_3

    iget-boolean v8, v7, Lcom/miui/server/sptm/SpeedTestModeServiceImpl$AppUsageRecord;->isColdStart:Z

    if-eqz v8, :cond_3

    .line 572
    iget-object v2, v7, Lcom/miui/server/sptm/SpeedTestModeServiceImpl$AppUsageRecord;->packageName:Ljava/lang/String;

    .line 576
    :cond_3
    iget-object v8, v7, Lcom/miui/server/sptm/SpeedTestModeServiceImpl$AppUsageRecord;->packageName:Ljava/lang/String;

    iput-object v8, p3, Lcom/miui/server/sptm/PreLoadStrategy$AppStartRecord;->packageName:Ljava/lang/String;

    .line 577
    iput-boolean v3, p3, Lcom/miui/server/sptm/PreLoadStrategy$AppStartRecord;->isColdStart:Z

    .line 578
    .end local v7    # "aur":Lcom/miui/server/sptm/SpeedTestModeServiceImpl$AppUsageRecord;
    goto :goto_0

    .line 579
    :cond_4
    if-nez v2, :cond_8

    .line 580
    sget-object v3, Lcom/miui/server/sptm/SpeedTestModeServiceImpl;->GAME_APPS:Ljava/util/List;

    iget-object v6, p3, Lcom/miui/server/sptm/PreLoadStrategy$AppStartRecord;->packageName:Ljava/lang/String;

    invoke-interface {v3, v6}, Ljava/util/List;->contains(Ljava/lang/Object;)Z

    move-result v3

    if-nez v3, :cond_6

    .line 581
    sget-wide v6, Lcom/miui/server/sptm/SpeedTestModeServiceImpl;->HOT_START_DELAYED_TIME:J

    cmp-long v3, v4, v6

    if-gtz v3, :cond_5

    .line 582
    move v0, v1

    goto :goto_1

    :cond_5
    nop

    .line 581
    :goto_1
    return v0

    .line 584
    :cond_6
    sget-wide v6, Lcom/miui/server/sptm/SpeedTestModeServiceImpl;->GAME_HOT_START_DELAYED_TIME:J

    cmp-long v3, v4, v6

    if-gtz v3, :cond_7

    .line 585
    move v0, v1

    goto :goto_2

    :cond_7
    nop

    .line 584
    :goto_2
    return v0

    .line 589
    :cond_8
    iput-object v2, p3, Lcom/miui/server/sptm/PreLoadStrategy$AppStartRecord;->packageName:Ljava/lang/String;

    .line 590
    iput-boolean v0, p3, Lcom/miui/server/sptm/PreLoadStrategy$AppStartRecord;->isColdStart:Z

    .line 593
    const-wide/16 v3, 0x0

    .line 594
    .end local v4    # "totalDuration":J
    .local v3, "totalDuration":J
    const-wide/16 v5, 0x0

    .line 595
    .local v5, "lastResumedActivityDuration":J
    invoke-interface {p1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_3
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v7

    if-eqz v7, :cond_b

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v7

    check-cast v7, Lcom/miui/server/sptm/SpeedTestModeServiceImpl$AppUsageRecord;

    .line 596
    .local v7, "ur":Lcom/miui/server/sptm/SpeedTestModeServiceImpl$AppUsageRecord;
    iget-object v8, v7, Lcom/miui/server/sptm/SpeedTestModeServiceImpl$AppUsageRecord;->packageName:Ljava/lang/String;

    invoke-virtual {v2, v8}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v8

    if-eqz v8, :cond_a

    .line 597
    invoke-virtual {v7}, Lcom/miui/server/sptm/SpeedTestModeServiceImpl$AppUsageRecord;->getDuration()J

    move-result-wide v8

    add-long/2addr v3, v8

    .line 600
    iget-boolean v8, v7, Lcom/miui/server/sptm/SpeedTestModeServiceImpl$AppUsageRecord;->isColdStart:Z

    if-nez v8, :cond_9

    invoke-virtual {v7}, Lcom/miui/server/sptm/SpeedTestModeServiceImpl$AppUsageRecord;->getDuration()J

    move-result-wide v8

    goto :goto_4

    :cond_9
    const-wide/16 v8, 0x0

    :goto_4
    move-wide v5, v8

    .line 602
    .end local v7    # "ur":Lcom/miui/server/sptm/SpeedTestModeServiceImpl$AppUsageRecord;
    :cond_a
    goto :goto_3

    .line 604
    :cond_b
    sget-object v1, Lcom/miui/server/sptm/SpeedTestModeServiceImpl;->GAME_APPS:Ljava/util/List;

    invoke-interface {v1, v2}, Ljava/util/List;->contains(Ljava/lang/Object;)Z

    move-result v1

    const/4 v7, 0x3

    if-nez v1, :cond_c

    .line 605
    sget-wide v8, Lcom/miui/server/sptm/SpeedTestModeServiceImpl;->COLD_START_DELAYED_TIME:J

    cmp-long v1, v3, v8

    if-gtz v1, :cond_d

    sget-wide v8, Lcom/miui/server/sptm/SpeedTestModeServiceImpl;->HOT_START_DELAYED_TIME:J

    cmp-long v1, v5, v8

    if-gtz v1, :cond_d

    .line 607
    return v7

    .line 610
    :cond_c
    sget-wide v8, Lcom/miui/server/sptm/SpeedTestModeServiceImpl;->GAME_COLD_START_DELAYED_TIME:J

    cmp-long v1, v3, v8

    if-gtz v1, :cond_d

    .line 611
    return v7

    .line 614
    :cond_d
    return v0

    .line 558
    .end local v2    # "coldStartPackageName":Ljava/lang/String;
    .end local v3    # "totalDuration":J
    .end local v5    # "lastResumedActivityDuration":J
    :cond_e
    :goto_5
    return v1

    .line 553
    :cond_f
    :goto_6
    return v0
.end method

.method public static getInstance()Lcom/miui/server/sptm/SpeedTestModeServiceImpl;
    .locals 1

    .line 253
    const-class v0, Lcom/android/server/am/SpeedTestModeServiceStub;

    invoke-static {v0}, Lcom/miui/base/MiuiStubUtil;->getImpl(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/miui/server/sptm/SpeedTestModeServiceImpl;

    return-object v0
.end method

.method private handleAppSwitchingEvent(Lcom/miui/server/sptm/SpeedTestModeServiceImpl$AppUsageRecord;)V
    .locals 7
    .param p1, "appRecord"    # Lcom/miui/server/sptm/SpeedTestModeServiceImpl$AppUsageRecord;

    .line 512
    iget-object v0, p0, Lcom/miui/server/sptm/SpeedTestModeServiceImpl;->mHomePackageName:Ljava/lang/String;

    iget-object v1, p1, Lcom/miui/server/sptm/SpeedTestModeServiceImpl$AppUsageRecord;->packageName:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    const/4 v1, 0x0

    if-eqz v0, :cond_4

    .line 514
    new-instance v0, Lcom/miui/server/sptm/PreLoadStrategy$AppStartRecord;

    invoke-direct {v0}, Lcom/miui/server/sptm/PreLoadStrategy$AppStartRecord;-><init>()V

    .line 515
    .local v0, "outRes":Lcom/miui/server/sptm/PreLoadStrategy$AppStartRecord;
    iget-object v2, p0, Lcom/miui/server/sptm/SpeedTestModeServiceImpl;->mAppStartRecords:Ljava/util/LinkedList;

    invoke-direct {p0, v2, p1, v0}, Lcom/miui/server/sptm/SpeedTestModeServiceImpl;->getAppSwitchingOperation(Ljava/util/List;Lcom/miui/server/sptm/SpeedTestModeServiceImpl$AppUsageRecord;Lcom/miui/server/sptm/PreLoadStrategy$AppStartRecord;)I

    move-result v2

    .line 516
    .local v2, "switchOps":I
    iget-object v3, p0, Lcom/miui/server/sptm/SpeedTestModeServiceImpl;->mSpeedTestModeState:Lcom/miui/server/sptm/SpeedTestModeState;

    invoke-virtual {v3, v2}, Lcom/miui/server/sptm/SpeedTestModeState;->addAppSwitchOps(I)V

    .line 517
    iget-object v3, p0, Lcom/miui/server/sptm/SpeedTestModeServiceImpl;->mAppStartRecords:Ljava/util/LinkedList;

    if-eqz v3, :cond_0

    invoke-virtual {v3}, Ljava/util/LinkedList;->size()I

    move-result v3

    if-lez v3, :cond_0

    .line 518
    iget-object v3, p0, Lcom/miui/server/sptm/SpeedTestModeServiceImpl;->mSpeedTestModeState:Lcom/miui/server/sptm/SpeedTestModeState;

    iget-object v4, p0, Lcom/miui/server/sptm/SpeedTestModeServiceImpl;->mAppStartRecords:Ljava/util/LinkedList;

    invoke-virtual {v4, v1}, Ljava/util/LinkedList;->get(I)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lcom/miui/server/sptm/SpeedTestModeServiceImpl$AppUsageRecord;

    iget-object v4, v4, Lcom/miui/server/sptm/SpeedTestModeServiceImpl$AppUsageRecord;->packageName:Ljava/lang/String;

    iget-object v5, p0, Lcom/miui/server/sptm/SpeedTestModeServiceImpl;->mAppStartRecords:Ljava/util/LinkedList;

    .line 519
    invoke-virtual {v5, v1}, Ljava/util/LinkedList;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/miui/server/sptm/SpeedTestModeServiceImpl$AppUsageRecord;

    iget-boolean v1, v1, Lcom/miui/server/sptm/SpeedTestModeServiceImpl$AppUsageRecord;->isColdStart:Z

    .line 518
    invoke-virtual {v3, v4, v1}, Lcom/miui/server/sptm/SpeedTestModeState;->addAppSwitchOps(Ljava/lang/String;Z)V

    .line 522
    :cond_0
    sget-boolean v1, Lcom/miui/server/sptm/SpeedTestModeServiceImpl;->DEBUG:Z

    if-eqz v1, :cond_2

    .line 523
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "handleAppSwitchingEvent: ops"

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    const-string v3, "SPTM"

    invoke-static {v3, v1}, Landroid/util/Slog;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 524
    iget-object v1, p0, Lcom/miui/server/sptm/SpeedTestModeServiceImpl;->mAppStartRecords:Ljava/util/LinkedList;

    invoke-virtual {v1}, Ljava/util/LinkedList;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v4

    if-eqz v4, :cond_1

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lcom/miui/server/sptm/SpeedTestModeServiceImpl$AppUsageRecord;

    .line 525
    .local v4, "r":Lcom/miui/server/sptm/SpeedTestModeServiceImpl$AppUsageRecord;
    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "handleAppSwitchingEvent Apps: "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v4}, Lcom/miui/server/sptm/SpeedTestModeServiceImpl$AppUsageRecord;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v3, v5}, Landroid/util/Slog;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 526
    .end local v4    # "r":Lcom/miui/server/sptm/SpeedTestModeServiceImpl$AppUsageRecord;
    goto :goto_0

    .line 527
    :cond_1
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "handleAppSwitchingEvent Home: "

    invoke-virtual {v1, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {p1}, Lcom/miui/server/sptm/SpeedTestModeServiceImpl$AppUsageRecord;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v1, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v3, v1}, Landroid/util/Slog;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 529
    :cond_2
    iget-object v1, p0, Lcom/miui/server/sptm/SpeedTestModeServiceImpl;->mAppStartRecords:Ljava/util/LinkedList;

    invoke-virtual {v1}, Ljava/util/LinkedList;->clear()V

    .line 531
    iget-object v1, v0, Lcom/miui/server/sptm/PreLoadStrategy$AppStartRecord;->packageName:Ljava/lang/String;

    if-eqz v1, :cond_3

    iget-object v1, p0, Lcom/miui/server/sptm/SpeedTestModeServiceImpl;->mSpeedTestModeStrategies:Ljava/util/LinkedList;

    if-eqz v1, :cond_3

    .line 532
    invoke-virtual {v1}, Ljava/util/LinkedList;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_1
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_3

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/miui/server/sptm/SpeedTestModeServiceImpl$Strategy;

    .line 533
    .local v3, "s":Lcom/miui/server/sptm/SpeedTestModeServiceImpl$Strategy;
    invoke-interface {v3, v0}, Lcom/miui/server/sptm/SpeedTestModeServiceImpl$Strategy;->onAppStarted(Lcom/miui/server/sptm/PreLoadStrategy$AppStartRecord;)V

    .line 534
    .end local v3    # "s":Lcom/miui/server/sptm/SpeedTestModeServiceImpl$Strategy;
    goto :goto_1

    .line 536
    .end local v0    # "outRes":Lcom/miui/server/sptm/PreLoadStrategy$AppStartRecord;
    .end local v2    # "switchOps":I
    :cond_3
    goto :goto_2

    .line 537
    :cond_4
    iget-object v0, p0, Lcom/miui/server/sptm/SpeedTestModeServiceImpl;->mAppStartRecords:Ljava/util/LinkedList;

    invoke-virtual {v0}, Ljava/util/LinkedList;->size()I

    move-result v0

    if-eqz v0, :cond_5

    iget-object v0, p0, Lcom/miui/server/sptm/SpeedTestModeServiceImpl;->mAppStartRecords:Ljava/util/LinkedList;

    .line 538
    invoke-virtual {v0, v1}, Ljava/util/LinkedList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/miui/server/sptm/SpeedTestModeServiceImpl$AppUsageRecord;

    iget-object v0, v0, Lcom/miui/server/sptm/SpeedTestModeServiceImpl$AppUsageRecord;->packageName:Ljava/lang/String;

    iget-object v1, p1, Lcom/miui/server/sptm/SpeedTestModeServiceImpl$AppUsageRecord;->packageName:Ljava/lang/String;

    if-ne v0, v1, :cond_6

    .line 539
    :cond_5
    iget-object v0, p0, Lcom/miui/server/sptm/SpeedTestModeServiceImpl;->mAppStartRecords:Ljava/util/LinkedList;

    invoke-virtual {v0, p1}, Ljava/util/LinkedList;->add(Ljava/lang/Object;)Z

    .line 543
    :cond_6
    :goto_2
    iget-object v0, p0, Lcom/miui/server/sptm/SpeedTestModeServiceImpl;->mAppStartRecords:Ljava/util/LinkedList;

    invoke-virtual {v0}, Ljava/util/LinkedList;->size()I

    move-result v0

    const/16 v1, 0x1e

    if-le v0, v1, :cond_7

    .line 544
    iget-object v0, p0, Lcom/miui/server/sptm/SpeedTestModeServiceImpl;->mAppStartRecords:Ljava/util/LinkedList;

    invoke-virtual {v0}, Ljava/util/LinkedList;->clear()V

    .line 546
    :cond_7
    return-void
.end method

.method private handleEvent(ILjava/lang/String;J)V
    .locals 8
    .param p1, "event"    # I
    .param p2, "packageName"    # Ljava/lang/String;
    .param p3, "time"    # J

    .line 471
    const/4 v0, 0x2

    if-ne p1, v0, :cond_1

    .line 472
    iget-object v0, p0, Lcom/miui/server/sptm/SpeedTestModeServiceImpl;->mAppLastResumedTimes:Ljava/util/HashMap;

    invoke-static {p3, p4}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v1

    invoke-virtual {v0, p2, v1}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 473
    sget-object v0, Lcom/miui/server/sptm/SpeedTestModeServiceImpl;->GAME_APPS:Ljava/util/List;

    invoke-interface {v0, p2}, Ljava/util/List;->contains(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 474
    sget-wide v0, Lcom/miui/server/sptm/SpeedTestModeServiceImpl;->GAME_COLD_START_DELAYED_TIME:J

    goto :goto_0

    :cond_0
    sget-wide v0, Lcom/miui/server/sptm/SpeedTestModeServiceImpl;->COLD_START_DELAYED_TIME:J

    .line 473
    :goto_0
    invoke-direct {p0, v0, v1}, Lcom/miui/server/sptm/SpeedTestModeServiceImpl;->reportAppSwitchEventWaitTimeout(J)V

    goto/16 :goto_3

    .line 475
    :cond_1
    const/4 v0, 0x1

    if-ne p1, v0, :cond_2

    .line 476
    iget-object v0, p0, Lcom/miui/server/sptm/SpeedTestModeServiceImpl;->mAppStartProcTimes:Ljava/util/LinkedHashMap;

    invoke-static {p3, p4}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v1

    invoke-virtual {v0, p2, v1}, Ljava/util/LinkedHashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    goto/16 :goto_3

    .line 477
    :cond_2
    const/4 v1, 0x3

    if-ne p1, v1, :cond_7

    .line 478
    iget-object v1, p0, Lcom/miui/server/sptm/SpeedTestModeServiceImpl;->mAppLastResumedTimes:Ljava/util/HashMap;

    invoke-virtual {v1, p2}, Ljava/util/HashMap;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/Long;

    .line 479
    .local v1, "resumedTime":Ljava/lang/Long;
    if-eqz v1, :cond_5

    .line 480
    new-instance v2, Lcom/miui/server/sptm/SpeedTestModeServiceImpl$AppUsageRecord;

    const/4 v3, 0x0

    invoke-direct {v2, v3}, Lcom/miui/server/sptm/SpeedTestModeServiceImpl$AppUsageRecord;-><init>(Lcom/miui/server/sptm/SpeedTestModeServiceImpl$AppUsageRecord-IA;)V

    .line 481
    .local v2, "r":Lcom/miui/server/sptm/SpeedTestModeServiceImpl$AppUsageRecord;
    invoke-virtual {v1}, Ljava/lang/Long;->longValue()J

    move-result-wide v3

    iput-wide v3, v2, Lcom/miui/server/sptm/SpeedTestModeServiceImpl$AppUsageRecord;->startTime:J

    .line 482
    iput-wide p3, v2, Lcom/miui/server/sptm/SpeedTestModeServiceImpl$AppUsageRecord;->endTime:J

    .line 483
    iput-object p2, v2, Lcom/miui/server/sptm/SpeedTestModeServiceImpl$AppUsageRecord;->packageName:Ljava/lang/String;

    .line 486
    iget-object v3, p0, Lcom/miui/server/sptm/SpeedTestModeServiceImpl;->mAppStartProcTimes:Ljava/util/LinkedHashMap;

    iget-object v4, v2, Lcom/miui/server/sptm/SpeedTestModeServiceImpl$AppUsageRecord;->packageName:Ljava/lang/String;

    invoke-virtual {v3, v4}, Ljava/util/LinkedHashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/lang/Long;

    .line 487
    .local v3, "startProcTime":Ljava/lang/Long;
    if-eqz v3, :cond_4

    .line 488
    iget-wide v4, v2, Lcom/miui/server/sptm/SpeedTestModeServiceImpl$AppUsageRecord;->startTime:J

    invoke-virtual {v3}, Ljava/lang/Long;->longValue()J

    move-result-wide v6

    sub-long/2addr v4, v6

    sget-wide v6, Lcom/miui/server/sptm/SpeedTestModeServiceImpl;->START_PROC_DELAYED_TIME:J

    cmp-long v4, v4, v6

    if-gez v4, :cond_3

    .line 489
    iput-boolean v0, v2, Lcom/miui/server/sptm/SpeedTestModeServiceImpl$AppUsageRecord;->isColdStart:Z

    .line 491
    :cond_3
    iget-object v0, p0, Lcom/miui/server/sptm/SpeedTestModeServiceImpl;->mAppStartProcTimes:Ljava/util/LinkedHashMap;

    iget-object v4, v2, Lcom/miui/server/sptm/SpeedTestModeServiceImpl$AppUsageRecord;->packageName:Ljava/lang/String;

    invoke-virtual {v0, v4}, Ljava/util/LinkedHashMap;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    .line 494
    :cond_4
    invoke-direct {p0, v2}, Lcom/miui/server/sptm/SpeedTestModeServiceImpl;->handleAppSwitchingEvent(Lcom/miui/server/sptm/SpeedTestModeServiceImpl$AppUsageRecord;)V

    .line 495
    .end local v2    # "r":Lcom/miui/server/sptm/SpeedTestModeServiceImpl$AppUsageRecord;
    .end local v3    # "startProcTime":Ljava/lang/Long;
    goto :goto_1

    .line 496
    :cond_5
    sget-boolean v0, Lcom/miui/server/sptm/SpeedTestModeServiceImpl;->DEBUG:Z

    if-eqz v0, :cond_6

    .line 497
    invoke-static {p3, p4}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v0

    filled-new-array {p2, v0}, [Ljava/lang/Object;

    move-result-object v0

    const-string v2, "pkg %s has not resumed %s"

    invoke-static {v2, v0}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    const-string v2, "SPTM"

    invoke-static {v2, v0}, Landroid/util/Slog;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 499
    .end local v1    # "resumedTime":Ljava/lang/Long;
    :cond_6
    :goto_1
    goto :goto_3

    :cond_7
    const/4 v1, 0x5

    if-eq p1, v1, :cond_9

    const/4 v1, 0x4

    if-ne p1, v1, :cond_8

    goto :goto_2

    .line 502
    :cond_8
    const/4 v0, 0x6

    if-ne p1, v0, :cond_a

    iget-object v0, p0, Lcom/miui/server/sptm/SpeedTestModeServiceImpl;->mPreLoadStrategy:Lcom/miui/server/sptm/PreLoadStrategy;

    if-eqz v0, :cond_a

    .line 503
    invoke-virtual {v0, p2}, Lcom/miui/server/sptm/PreLoadStrategy;->onPreloadAppStarted(Ljava/lang/String;)V

    goto :goto_3

    .line 500
    :cond_9
    :goto_2
    iget-object v1, p0, Lcom/miui/server/sptm/SpeedTestModeServiceImpl;->mSpeedTestModeState:Lcom/miui/server/sptm/SpeedTestModeState;

    invoke-virtual {v1, v0}, Lcom/miui/server/sptm/SpeedTestModeState;->addAppSwitchOps(I)V

    .line 501
    iget-object v0, p0, Lcom/miui/server/sptm/SpeedTestModeServiceImpl;->mPreLoadStrategy:Lcom/miui/server/sptm/PreLoadStrategy;

    invoke-virtual {v0}, Lcom/miui/server/sptm/PreLoadStrategy;->reset()V

    .line 506
    :cond_a
    :goto_3
    iget-object v0, p0, Lcom/miui/server/sptm/SpeedTestModeServiceImpl;->mSpeedTestModeStrategies:Ljava/util/LinkedList;

    invoke-virtual {v0}, Ljava/util/LinkedList;->iterator()Ljava/util/Iterator;

    move-result-object v0

    :goto_4
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_b

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/miui/server/sptm/SpeedTestModeServiceImpl$Strategy;

    .line 507
    .local v1, "st":Lcom/miui/server/sptm/SpeedTestModeServiceImpl$Strategy;
    invoke-interface {v1, p1}, Lcom/miui/server/sptm/SpeedTestModeServiceImpl$Strategy;->onNewEvent(I)V

    .line 508
    .end local v1    # "st":Lcom/miui/server/sptm/SpeedTestModeServiceImpl$Strategy;
    goto :goto_4

    .line 509
    :cond_b
    return-void
.end method

.method private handleUpdateSpeedTestMode(Z)V
    .locals 2
    .param p1, "isEnabled"    # Z

    .line 618
    iput-boolean p1, p0, Lcom/miui/server/sptm/SpeedTestModeServiceImpl;->mIsSpeedTestMode:Z

    .line 619
    iget-object v0, p0, Lcom/miui/server/sptm/SpeedTestModeServiceImpl;->mSpeedTestModeStrategies:Ljava/util/LinkedList;

    if-eqz v0, :cond_0

    .line 620
    invoke-virtual {v0}, Ljava/util/LinkedList;->iterator()Ljava/util/Iterator;

    move-result-object v0

    :goto_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_0

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/miui/server/sptm/SpeedTestModeServiceImpl$Strategy;

    .line 621
    .local v1, "s":Lcom/miui/server/sptm/SpeedTestModeServiceImpl$Strategy;
    invoke-interface {v1, p1}, Lcom/miui/server/sptm/SpeedTestModeServiceImpl$Strategy;->onSpeedTestModeChanged(Z)V

    .line 622
    .end local v1    # "s":Lcom/miui/server/sptm/SpeedTestModeServiceImpl$Strategy;
    goto :goto_0

    .line 624
    :cond_0
    return-void
.end method

.method public static isLowMemDeviceForSpeedTestMode()Z
    .locals 4

    .line 400
    sget-wide v0, Lcom/miui/server/sptm/SpeedTestModeServiceImpl;->TOTAL_MEMORY:J

    sget v2, Lcom/miui/server/sptm/SpeedTestModeServiceImpl;->SPTM_LOW_MEMORY_DEVICE_THRESHOLD:I

    int-to-long v2, v2

    cmp-long v0, v0, v2

    if-gtz v0, :cond_0

    const/4 v0, 0x1

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    :goto_0
    return v0
.end method

.method private registerCloudObserver(Landroid/content/Context;)V
    .locals 4
    .param p1, "context"    # Landroid/content/Context;

    .line 409
    new-instance v0, Lcom/miui/server/sptm/SpeedTestModeServiceImpl$2;

    iget-object v1, p0, Lcom/miui/server/sptm/SpeedTestModeServiceImpl;->mH:Lcom/miui/server/sptm/SpeedTestModeServiceImpl$Handler;

    invoke-direct {v0, p0, v1}, Lcom/miui/server/sptm/SpeedTestModeServiceImpl$2;-><init>(Lcom/miui/server/sptm/SpeedTestModeServiceImpl;Landroid/os/Handler;)V

    .line 416
    .local v0, "observer":Landroid/database/ContentObserver;
    invoke-virtual {p1}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v1

    .line 417
    invoke-static {}, Landroid/provider/MiuiSettings$SettingsCloudData;->getCloudDataNotifyUri()Landroid/net/Uri;

    move-result-object v2

    .line 416
    const/4 v3, 0x0

    invoke-virtual {v1, v2, v3, v0}, Landroid/content/ContentResolver;->registerContentObserver(Landroid/net/Uri;ZLandroid/database/ContentObserver;)V

    .line 419
    return-void
.end method

.method private reportAppSwitchEventWaitTimeout(J)V
    .locals 3
    .param p1, "timeout"    # J

    .line 461
    iget-object v0, p0, Lcom/miui/server/sptm/SpeedTestModeServiceImpl;->mH:Lcom/miui/server/sptm/SpeedTestModeServiceImpl$Handler;

    if-nez v0, :cond_0

    .line 462
    return-void

    .line 465
    :cond_0
    const/4 v1, 0x4

    invoke-virtual {v0, v1}, Lcom/miui/server/sptm/SpeedTestModeServiceImpl$Handler;->obtainMessage(I)Landroid/os/Message;

    move-result-object v0

    .line 466
    .local v0, "msg":Landroid/os/Message;
    iget-object v2, p0, Lcom/miui/server/sptm/SpeedTestModeServiceImpl;->mH:Lcom/miui/server/sptm/SpeedTestModeServiceImpl$Handler;

    invoke-virtual {v2, v1}, Lcom/miui/server/sptm/SpeedTestModeServiceImpl$Handler;->removeMessages(I)V

    .line 467
    iget-object v1, p0, Lcom/miui/server/sptm/SpeedTestModeServiceImpl;->mH:Lcom/miui/server/sptm/SpeedTestModeServiceImpl$Handler;

    invoke-virtual {v1, v0, p1, p2}, Lcom/miui/server/sptm/SpeedTestModeServiceImpl$Handler;->sendMessageDelayed(Landroid/os/Message;J)Z

    .line 468
    return-void
.end method

.method private setEnableSpeedTestMode(Z)V
    .locals 2
    .param p1, "isEnable"    # Z

    .line 627
    iget-object v0, p0, Lcom/miui/server/sptm/SpeedTestModeServiceImpl;->mH:Lcom/miui/server/sptm/SpeedTestModeServiceImpl$Handler;

    if-eqz p1, :cond_0

    const/4 v1, 0x2

    goto :goto_0

    :cond_0
    const/4 v1, 0x3

    :goto_0
    invoke-virtual {v0, v1}, Lcom/miui/server/sptm/SpeedTestModeServiceImpl$Handler;->sendEmptyMessage(I)Z

    .line 628
    return-void
.end method

.method private updateCloudControlParas()V
    .locals 8

    .line 422
    const-string v0, "SPTM"

    iget-object v1, p0, Lcom/miui/server/sptm/SpeedTestModeServiceImpl;->mContext:Landroid/content/Context;

    .line 423
    invoke-virtual {v1}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v1

    .line 422
    const-string v2, "perf_shielder"

    const-string v3, "perf_shielder_SPTM"

    const-string v4, ""

    invoke-static {v1, v2, v3, v4}, Landroid/provider/MiuiSettings$SettingsCloudData;->getCloudDataString(Landroid/content/ContentResolver;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 424
    .local v1, "data":Ljava/lang/String;
    invoke-static {v1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v2

    if-nez v2, :cond_5

    .line 426
    :try_start_0
    new-instance v2, Lorg/json/JSONObject;

    invoke-direct {v2, v1}, Lorg/json/JSONObject;-><init>(Ljava/lang/String;)V

    .line 427
    .local v2, "jsonObject":Lorg/json/JSONObject;
    sget-boolean v3, Lcom/miui/server/sptm/SpeedTestModeServiceImpl;->IGNORE_CLOUD_ENABLE:Z

    if-nez v3, :cond_0

    .line 428
    const-string v3, "perf_sptm_enable_old"

    const/4 v4, 0x0

    invoke-virtual {v2, v3, v4}, Lorg/json/JSONObject;->optBoolean(Ljava/lang/String;Z)Z

    move-result v3

    iput-boolean v3, p0, Lcom/miui/server/sptm/SpeedTestModeServiceImpl;->mIsEnableOld:Z

    .line 429
    const-string v3, "perf_sptm_enable_new"

    invoke-virtual {v2, v3, v4}, Lorg/json/JSONObject;->optBoolean(Ljava/lang/String;Z)Z

    move-result v3

    iput-boolean v3, p0, Lcom/miui/server/sptm/SpeedTestModeServiceImpl;->mIsEnableNew:Z

    .line 430
    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "old and new SPTM enable cloud control received="

    invoke-virtual {v3, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    iget-boolean v5, p0, Lcom/miui/server/sptm/SpeedTestModeServiceImpl;->mIsEnableOld:Z

    invoke-virtual {v3, v5}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v5, " and "

    invoke-virtual {v3, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    iget-boolean v5, p0, Lcom/miui/server/sptm/SpeedTestModeServiceImpl;->mIsEnableNew:Z

    invoke-virtual {v3, v5}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v0, v3}, Landroid/util/Slog;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 433
    const-string v3, "perf_sptm_preload"

    invoke-virtual {v2, v3, v4}, Lorg/json/JSONObject;->optInt(Ljava/lang/String;I)I

    move-result v3

    iput v3, p0, Lcom/miui/server/sptm/SpeedTestModeServiceImpl;->mPreloadType:I

    .line 434
    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "SPTM preload cloud control received="

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    iget v4, p0, Lcom/miui/server/sptm/SpeedTestModeServiceImpl;->mPreloadType:I

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v0, v3}, Landroid/util/Slog;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 436
    const-string v3, "perf_sptm_animation_enable"

    .line 437
    const/4 v4, 0x1

    invoke-virtual {v2, v3, v4}, Lorg/json/JSONObject;->optBoolean(Ljava/lang/String;Z)Z

    move-result v3

    iput-boolean v3, p0, Lcom/miui/server/sptm/SpeedTestModeServiceImpl;->mIsEnableSPTMAnimation:Z

    .line 438
    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "SPTM animation cloud control received="

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    iget-boolean v4, p0, Lcom/miui/server/sptm/SpeedTestModeServiceImpl;->mIsEnableSPTMAnimation:Z

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v0, v3}, Landroid/util/Slog;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 441
    :cond_0
    const-string v3, "perf_sptm_app_list"

    invoke-virtual {v2, v3}, Lorg/json/JSONObject;->optJSONArray(Ljava/lang/String;)Lorg/json/JSONArray;

    move-result-object v3

    .line 442
    .local v3, "appListJsonArray":Lorg/json/JSONArray;
    if-eqz v3, :cond_4

    .line 443
    const/4 v4, 0x0

    .local v4, "i":I
    :goto_0
    invoke-virtual {v3}, Lorg/json/JSONArray;->length()I

    move-result v5

    if-ge v4, v5, :cond_4

    .line 444
    invoke-virtual {v3, v4}, Lorg/json/JSONArray;->getString(I)Ljava/lang/String;

    move-result-object v5

    .line 445
    .local v5, "appPackageName":Ljava/lang/String;
    invoke-static {v5}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v6

    if-eqz v6, :cond_1

    .line 446
    goto :goto_1

    .line 448
    :cond_1
    sget-boolean v6, Lcom/miui/server/sptm/SpeedTestModeServiceImpl;->DEBUG:Z

    if-eqz v6, :cond_2

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string v7, "SPTM add app package name : "

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-static {v0, v6}, Landroid/util/Slog;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 449
    :cond_2
    sget-object v6, Lcom/miui/server/sptm/SpeedTestModeServiceImpl;->SPEED_TEST_APP_LIST:Ljava/util/List;

    invoke-interface {v6, v5}, Ljava/util/List;->contains(Ljava/lang/Object;)Z

    move-result v7

    if-nez v7, :cond_3

    .line 450
    invoke-interface {v6, v5}, Ljava/util/List;->add(Ljava/lang/Object;)Z
    :try_end_0
    .catch Lorg/json/JSONException; {:try_start_0 .. :try_end_0} :catch_0

    .line 443
    .end local v5    # "appPackageName":Ljava/lang/String;
    :cond_3
    :goto_1
    add-int/lit8 v4, v4, 0x1

    goto :goto_0

    .line 456
    .end local v2    # "jsonObject":Lorg/json/JSONObject;
    .end local v3    # "appListJsonArray":Lorg/json/JSONArray;
    .end local v4    # "i":I
    :cond_4
    goto :goto_2

    .line 454
    :catch_0
    move-exception v2

    .line 455
    .local v2, "e":Lorg/json/JSONException;
    const-string/jumbo v3, "updateCloudData error :"

    invoke-static {v0, v3, v2}, Landroid/util/Slog;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    .line 458
    .end local v2    # "e":Lorg/json/JSONException;
    :cond_5
    :goto_2
    return-void
.end method


# virtual methods
.method public getAnimationCloudEnable()Z
    .locals 1

    .line 690
    iget-boolean v0, p0, Lcom/miui/server/sptm/SpeedTestModeServiceImpl;->mIsEnableSPTMAnimation:Z

    return v0
.end method

.method public getPreloadCloudType()I
    .locals 1

    .line 682
    iget v0, p0, Lcom/miui/server/sptm/SpeedTestModeServiceImpl;->mPreloadType:I

    return v0
.end method

.method public getSPTMCloudEnable()Z
    .locals 1

    .line 686
    iget-boolean v0, p0, Lcom/miui/server/sptm/SpeedTestModeServiceImpl;->mIsEnableOld:Z

    if-nez v0, :cond_1

    iget-boolean v0, p0, Lcom/miui/server/sptm/SpeedTestModeServiceImpl;->mIsEnableNew:Z

    if-eqz v0, :cond_0

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    goto :goto_1

    :cond_1
    :goto_0
    const/4 v0, 0x1

    :goto_1
    return v0
.end method

.method public getSPTMCloudEnableNew()Z
    .locals 1

    .line 694
    iget-boolean v0, p0, Lcom/miui/server/sptm/SpeedTestModeServiceImpl;->mIsEnableNew:Z

    return v0
.end method

.method public getWindowStateAnimationScaleOverride()F
    .locals 1

    .line 309
    iget-object v0, p0, Lcom/miui/server/sptm/SpeedTestModeServiceImpl;->mHomeAnimationStrategy:Lcom/miui/server/sptm/HomeAnimationStrategy;

    if-eqz v0, :cond_0

    .line 310
    invoke-virtual {v0}, Lcom/miui/server/sptm/HomeAnimationStrategy;->getWindowAnimatorDurationOverride()F

    move-result v0

    return v0

    .line 312
    :cond_0
    const/high16 v0, 0x3f800000    # 1.0f

    return v0
.end method

.method public init(Landroid/content/Context;)V
    .locals 4
    .param p1, "context"    # Landroid/content/Context;

    .line 262
    iget-boolean v0, p0, Lcom/miui/server/sptm/SpeedTestModeServiceImpl;->mIsEnableOld:Z

    xor-int/lit8 v0, v0, 0x1

    iget-boolean v1, p0, Lcom/miui/server/sptm/SpeedTestModeServiceImpl;->mIsEnableNew:Z

    xor-int/lit8 v1, v1, 0x1

    and-int/2addr v0, v1

    if-eqz v0, :cond_0

    .line 263
    return-void

    .line 265
    :cond_0
    sget-wide v0, Lcom/miui/server/sptm/SpeedTestModeServiceImpl;->TOTAL_MEMORY:J

    const-wide/16 v2, 0x1770

    cmp-long v0, v0, v2

    if-gtz v0, :cond_1

    .line 266
    return-void

    .line 268
    :cond_1
    iput-object p1, p0, Lcom/miui/server/sptm/SpeedTestModeServiceImpl;->mContext:Landroid/content/Context;

    .line 269
    new-instance v0, Lcom/miui/server/sptm/SpeedTestModeController;

    new-instance v1, Lcom/miui/server/sptm/SpeedTestModeServiceImpl$$ExternalSyntheticLambda0;

    invoke-direct {v1, p0}, Lcom/miui/server/sptm/SpeedTestModeServiceImpl$$ExternalSyntheticLambda0;-><init>(Lcom/miui/server/sptm/SpeedTestModeServiceImpl;)V

    invoke-direct {v0, v1}, Lcom/miui/server/sptm/SpeedTestModeController;-><init>(Lcom/miui/server/sptm/SpeedTestModeController$OnSpeedTestModeChangeListener;)V

    .line 271
    .local v0, "speedTestModeController":Lcom/miui/server/sptm/SpeedTestModeController;
    new-instance v1, Lcom/miui/server/sptm/SpeedTestModeState;

    invoke-direct {v1, v0}, Lcom/miui/server/sptm/SpeedTestModeState;-><init>(Lcom/miui/server/sptm/SpeedTestModeController;)V

    iput-object v1, p0, Lcom/miui/server/sptm/SpeedTestModeServiceImpl;->mSpeedTestModeState:Lcom/miui/server/sptm/SpeedTestModeState;

    .line 273
    new-instance v1, Landroid/os/HandlerThread;

    const-string v2, "SPTModeServiceTh"

    const/4 v3, -0x2

    invoke-direct {v1, v2, v3}, Landroid/os/HandlerThread;-><init>(Ljava/lang/String;I)V

    iput-object v1, p0, Lcom/miui/server/sptm/SpeedTestModeServiceImpl;->mHandlerThread:Landroid/os/HandlerThread;

    .line 274
    invoke-virtual {v1}, Landroid/os/HandlerThread;->start()V

    .line 275
    new-instance v1, Lcom/miui/server/sptm/SpeedTestModeServiceImpl$Handler;

    iget-object v2, p0, Lcom/miui/server/sptm/SpeedTestModeServiceImpl;->mHandlerThread:Landroid/os/HandlerThread;

    invoke-virtual {v2}, Landroid/os/HandlerThread;->getLooper()Landroid/os/Looper;

    move-result-object v2

    invoke-direct {v1, p0, v2}, Lcom/miui/server/sptm/SpeedTestModeServiceImpl$Handler;-><init>(Lcom/miui/server/sptm/SpeedTestModeServiceImpl;Landroid/os/Looper;)V

    iput-object v1, p0, Lcom/miui/server/sptm/SpeedTestModeServiceImpl;->mH:Lcom/miui/server/sptm/SpeedTestModeServiceImpl$Handler;

    .line 277
    iget-object v1, p0, Lcom/miui/server/sptm/SpeedTestModeServiceImpl;->mContext:Landroid/content/Context;

    invoke-direct {p0, v1}, Lcom/miui/server/sptm/SpeedTestModeServiceImpl;->registerCloudObserver(Landroid/content/Context;)V

    .line 279
    iget-object v1, p0, Lcom/miui/server/sptm/SpeedTestModeServiceImpl;->mSpeedTestModeStrategies:Ljava/util/LinkedList;

    invoke-virtual {v1}, Ljava/util/LinkedList;->size()I

    move-result v1

    if-nez v1, :cond_2

    .line 280
    new-instance v1, Lcom/miui/server/sptm/HomeAnimationStrategy;

    invoke-direct {v1, p1}, Lcom/miui/server/sptm/HomeAnimationStrategy;-><init>(Landroid/content/Context;)V

    iput-object v1, p0, Lcom/miui/server/sptm/SpeedTestModeServiceImpl;->mHomeAnimationStrategy:Lcom/miui/server/sptm/HomeAnimationStrategy;

    .line 281
    iget-object v2, p0, Lcom/miui/server/sptm/SpeedTestModeServiceImpl;->mSpeedTestModeStrategies:Ljava/util/LinkedList;

    invoke-virtual {v2, v1}, Ljava/util/LinkedList;->add(Ljava/lang/Object;)Z

    .line 282
    iget-object v1, p0, Lcom/miui/server/sptm/SpeedTestModeServiceImpl;->mSpeedTestModeStrategies:Ljava/util/LinkedList;

    new-instance v2, Lcom/miui/server/sptm/MemoryOptimizeStrategy;

    invoke-direct {v2}, Lcom/miui/server/sptm/MemoryOptimizeStrategy;-><init>()V

    invoke-virtual {v1, v2}, Ljava/util/LinkedList;->add(Ljava/lang/Object;)Z

    .line 283
    iget-object v1, p0, Lcom/miui/server/sptm/SpeedTestModeServiceImpl;->mSpeedTestModeStrategies:Ljava/util/LinkedList;

    new-instance v2, Lcom/miui/server/sptm/GreezeStrategy;

    invoke-direct {v2}, Lcom/miui/server/sptm/GreezeStrategy;-><init>()V

    invoke-virtual {v1, v2}, Ljava/util/LinkedList;->add(Ljava/lang/Object;)Z

    .line 284
    new-instance v1, Lcom/miui/server/sptm/PreLoadStrategy;

    invoke-direct {v1}, Lcom/miui/server/sptm/PreLoadStrategy;-><init>()V

    iput-object v1, p0, Lcom/miui/server/sptm/SpeedTestModeServiceImpl;->mPreLoadStrategy:Lcom/miui/server/sptm/PreLoadStrategy;

    .line 285
    iget-object v2, p0, Lcom/miui/server/sptm/SpeedTestModeServiceImpl;->mSpeedTestModeStrategies:Ljava/util/LinkedList;

    invoke-virtual {v2, v1}, Ljava/util/LinkedList;->add(Ljava/lang/Object;)Z

    .line 287
    :cond_2
    return-void
.end method

.method public isSpeedTestMode()Z
    .locals 1

    .line 405
    iget-boolean v0, p0, Lcom/miui/server/sptm/SpeedTestModeServiceImpl;->mIsSpeedTestMode:Z

    return v0
.end method

.method public onBootPhase()V
    .locals 4

    .line 291
    iget-boolean v0, p0, Lcom/miui/server/sptm/SpeedTestModeServiceImpl;->mIsEnableOld:Z

    xor-int/lit8 v0, v0, 0x1

    iget-boolean v1, p0, Lcom/miui/server/sptm/SpeedTestModeServiceImpl;->mIsEnableNew:Z

    xor-int/lit8 v1, v1, 0x1

    and-int/2addr v0, v1

    if-eqz v0, :cond_0

    .line 292
    return-void

    .line 294
    :cond_0
    sget-wide v0, Lcom/miui/server/sptm/SpeedTestModeServiceImpl;->TOTAL_MEMORY:J

    const-wide/16 v2, 0x1770

    cmp-long v0, v0, v2

    if-gtz v0, :cond_1

    .line 295
    return-void

    .line 297
    :cond_1
    invoke-direct {p0}, Lcom/miui/server/sptm/SpeedTestModeServiceImpl;->updateCloudControlParas()V

    .line 298
    return-void
.end method

.method public reportAppUsageEvents(ILjava/lang/String;)V
    .locals 5
    .param p1, "usageEventCode"    # I
    .param p2, "packageName"    # Ljava/lang/String;

    .line 322
    iget-object v0, p0, Lcom/miui/server/sptm/SpeedTestModeServiceImpl;->mH:Lcom/miui/server/sptm/SpeedTestModeServiceImpl$Handler;

    if-eqz v0, :cond_4

    invoke-static {p2}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_0

    goto :goto_1

    .line 325
    :cond_0
    sget-object v0, Lcom/miui/server/sptm/SpeedTestModeServiceImpl;->PERMISSION_DIALOG_PACKAGE_NAMES:Ljava/util/List;

    invoke-interface {v0, p2}, Ljava/util/List;->contains(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 326
    return-void

    .line 330
    :cond_1
    sparse-switch p1, :sswitch_data_0

    .line 342
    return-void

    .line 338
    :sswitch_0
    const/4 v0, 0x5

    .line 339
    .local v0, "event":I
    goto :goto_0

    .line 335
    .end local v0    # "event":I
    :sswitch_1
    const/4 v0, 0x3

    .line 336
    .restart local v0    # "event":I
    goto :goto_0

    .line 332
    .end local v0    # "event":I
    :sswitch_2
    const/4 v0, 0x2

    .line 333
    .restart local v0    # "event":I
    nop

    .line 346
    :goto_0
    invoke-static {}, Lcom/android/server/am/PreloadAppControllerStub;->get()Lcom/android/server/am/PreloadAppControllerStub;

    move-result-object v1

    .line 347
    .local v1, "preloadController":Lcom/android/server/am/PreloadAppControllerStub;
    if-eqz v1, :cond_3

    .line 348
    invoke-interface {v1}, Lcom/android/server/am/PreloadAppControllerStub;->getPreloadingApps()Ljava/util/Set;

    move-result-object v2

    invoke-interface {v2, p2}, Ljava/util/Set;->contains(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_3

    .line 349
    sget-boolean v2, Lcom/miui/server/sptm/SpeedTestModeServiceImpl;->DEBUG:Z

    if-eqz v2, :cond_2

    .line 350
    nop

    .line 351
    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    filled-new-array {p2, v2}, [Ljava/lang/Object;

    move-result-object v2

    .line 350
    const-string/jumbo v3, "skip %s code: %s because preloading"

    invoke-static {v3, v2}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    const-string v3, "SPTM"

    invoke-static {v3, v2}, Landroid/util/Slog;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 353
    :cond_2
    return-void

    .line 356
    :cond_3
    iget-object v2, p0, Lcom/miui/server/sptm/SpeedTestModeServiceImpl;->mH:Lcom/miui/server/sptm/SpeedTestModeServiceImpl$Handler;

    const/4 v3, 0x1

    invoke-virtual {v2, v3}, Lcom/miui/server/sptm/SpeedTestModeServiceImpl$Handler;->obtainMessage(I)Landroid/os/Message;

    move-result-object v2

    .line 357
    .local v2, "msg":Landroid/os/Message;
    invoke-static {}, Landroid/os/SystemClock;->uptimeMillis()J

    move-result-wide v3

    invoke-static {v0, p2, v3, v4}, Lcom/miui/server/sptm/SpeedTestModeServiceImpl;->createMsgData(ILjava/lang/String;J)Landroid/os/Bundle;

    move-result-object v3

    invoke-virtual {v2, v3}, Landroid/os/Message;->setData(Landroid/os/Bundle;)V

    .line 358
    iget-object v3, p0, Lcom/miui/server/sptm/SpeedTestModeServiceImpl;->mH:Lcom/miui/server/sptm/SpeedTestModeServiceImpl$Handler;

    invoke-virtual {v3, v2}, Lcom/miui/server/sptm/SpeedTestModeServiceImpl$Handler;->sendMessage(Landroid/os/Message;)Z

    .line 359
    return-void

    .line 323
    .end local v0    # "event":I
    .end local v1    # "preloadController":Lcom/android/server/am/PreloadAppControllerStub;
    .end local v2    # "msg":Landroid/os/Message;
    :cond_4
    :goto_1
    return-void

    nop

    :sswitch_data_0
    .sparse-switch
        0x1 -> :sswitch_2
        0x2 -> :sswitch_1
        0x11 -> :sswitch_0
    .end sparse-switch
.end method

.method public reportOneKeyCleanEvent()V
    .locals 5

    .line 363
    iget-object v0, p0, Lcom/miui/server/sptm/SpeedTestModeServiceImpl;->mH:Lcom/miui/server/sptm/SpeedTestModeServiceImpl$Handler;

    if-nez v0, :cond_0

    .line 364
    return-void

    .line 367
    :cond_0
    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Lcom/miui/server/sptm/SpeedTestModeServiceImpl$Handler;->obtainMessage(I)Landroid/os/Message;

    move-result-object v0

    .line 368
    .local v0, "msg":Landroid/os/Message;
    nop

    .line 369
    invoke-static {}, Landroid/os/SystemClock;->uptimeMillis()J

    move-result-wide v1

    .line 368
    const/4 v3, 0x4

    const-string v4, ""

    invoke-static {v3, v4, v1, v2}, Lcom/miui/server/sptm/SpeedTestModeServiceImpl;->createMsgData(ILjava/lang/String;J)Landroid/os/Bundle;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/os/Message;->setData(Landroid/os/Bundle;)V

    .line 370
    iget-object v1, p0, Lcom/miui/server/sptm/SpeedTestModeServiceImpl;->mH:Lcom/miui/server/sptm/SpeedTestModeServiceImpl$Handler;

    invoke-virtual {v1, v0}, Lcom/miui/server/sptm/SpeedTestModeServiceImpl$Handler;->sendMessage(Landroid/os/Message;)Z

    .line 371
    return-void
.end method

.method public reportPreloadAppStart(Ljava/lang/String;)V
    .locals 4
    .param p1, "packageName"    # Ljava/lang/String;

    .line 389
    iget-object v0, p0, Lcom/miui/server/sptm/SpeedTestModeServiceImpl;->mH:Lcom/miui/server/sptm/SpeedTestModeServiceImpl$Handler;

    if-nez v0, :cond_0

    .line 390
    return-void

    .line 393
    :cond_0
    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Lcom/miui/server/sptm/SpeedTestModeServiceImpl$Handler;->obtainMessage(I)Landroid/os/Message;

    move-result-object v0

    .line 394
    .local v0, "msg":Landroid/os/Message;
    nop

    .line 395
    invoke-static {}, Landroid/os/SystemClock;->uptimeMillis()J

    move-result-wide v1

    .line 394
    const/4 v3, 0x6

    invoke-static {v3, p1, v1, v2}, Lcom/miui/server/sptm/SpeedTestModeServiceImpl;->createMsgData(ILjava/lang/String;J)Landroid/os/Bundle;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/os/Message;->setData(Landroid/os/Bundle;)V

    .line 396
    iget-object v1, p0, Lcom/miui/server/sptm/SpeedTestModeServiceImpl;->mH:Lcom/miui/server/sptm/SpeedTestModeServiceImpl$Handler;

    invoke-virtual {v1, v0}, Lcom/miui/server/sptm/SpeedTestModeServiceImpl$Handler;->sendMessage(Landroid/os/Message;)Z

    .line 397
    return-void
.end method

.method public reportStartProcEvent(Ljava/lang/String;Ljava/lang/String;)V
    .locals 4
    .param p1, "packageName"    # Ljava/lang/String;
    .param p2, "procName"    # Ljava/lang/String;

    .line 375
    iget-object v0, p0, Lcom/miui/server/sptm/SpeedTestModeServiceImpl;->mH:Lcom/miui/server/sptm/SpeedTestModeServiceImpl$Handler;

    if-eqz v0, :cond_2

    invoke-static {p1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_2

    invoke-static {p2}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_0

    goto :goto_0

    .line 380
    :cond_0
    invoke-virtual {p1, p2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 381
    iget-object v0, p0, Lcom/miui/server/sptm/SpeedTestModeServiceImpl;->mH:Lcom/miui/server/sptm/SpeedTestModeServiceImpl$Handler;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Lcom/miui/server/sptm/SpeedTestModeServiceImpl$Handler;->obtainMessage(I)Landroid/os/Message;

    move-result-object v0

    .line 382
    .local v0, "msg":Landroid/os/Message;
    nop

    .line 383
    invoke-static {}, Landroid/os/SystemClock;->uptimeMillis()J

    move-result-wide v2

    .line 382
    invoke-static {v1, p1, v2, v3}, Lcom/miui/server/sptm/SpeedTestModeServiceImpl;->createMsgData(ILjava/lang/String;J)Landroid/os/Bundle;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/os/Message;->setData(Landroid/os/Bundle;)V

    .line 384
    iget-object v1, p0, Lcom/miui/server/sptm/SpeedTestModeServiceImpl;->mH:Lcom/miui/server/sptm/SpeedTestModeServiceImpl$Handler;

    invoke-virtual {v1, v0}, Lcom/miui/server/sptm/SpeedTestModeServiceImpl$Handler;->sendMessage(Landroid/os/Message;)Z

    .line 386
    .end local v0    # "msg":Landroid/os/Message;
    :cond_1
    return-void

    .line 376
    :cond_2
    :goto_0
    return-void
.end method

.method public setSPTModeEnabled(Z)V
    .locals 0
    .param p1, "isEnable"    # Z

    .line 317
    iput-boolean p1, p0, Lcom/miui/server/sptm/SpeedTestModeServiceImpl;->mIsSpeedTestEnabled:Z

    .line 318
    return-void
.end method

.method public updateHomeProcess(Ljava/lang/String;)V
    .locals 1
    .param p1, "homePackageName"    # Ljava/lang/String;

    .line 302
    invoke-static {p1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 303
    iput-object p1, p0, Lcom/miui/server/sptm/SpeedTestModeServiceImpl;->mHomePackageName:Ljava/lang/String;

    .line 305
    :cond_0
    return-void
.end method
