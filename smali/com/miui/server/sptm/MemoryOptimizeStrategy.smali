.class Lcom/miui/server/sptm/MemoryOptimizeStrategy;
.super Ljava/lang/Object;
.source "MemoryOptimizeStrategy.java"

# interfaces
.implements Lcom/miui/server/sptm/SpeedTestModeServiceImpl$Strategy;


# static fields
.field private static final MEMORY_RECLAIM_APP_COUNT:I

.field private static final SPTM_OOM_MIN_FREE:Ljava/lang/String;


# instance fields
.field private SPT_MODE_OOM_MIN_FREE:[I

.field private mAms:Lcom/android/server/am/ActivityManagerService;

.field private mAppCounts:I

.field private mIsProcCompactEnable:Z

.field private mMethodSetEnabledNativeSPTMode:Ljava/lang/reflect/Method;

.field private mMethodUpdateOomForSPTM:Ljava/lang/reflect/Method;

.field private mMethodUpdateOomMinFree:Ljava/lang/reflect/Method;

.field private mMiuiMemoryService:Lcom/android/server/am/MiuiMemoryServiceInternal;

.field private mSpeedTestModeService:Lcom/miui/server/sptm/SpeedTestModeServiceImpl;


# direct methods
.method static constructor <clinit>()V
    .locals 2

    .line 25
    const-string v0, "persist.sys.miui_sptm.min_free"

    const-string v1, "73728,92160,110592,240000,340000,450000"

    invoke-static {v0, v1}, Landroid/os/SystemProperties;->get(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/miui/server/sptm/MemoryOptimizeStrategy;->SPTM_OOM_MIN_FREE:Ljava/lang/String;

    .line 27
    const-string v0, "persist.sys.miui_sptm.start_mem_reclaim_app_count"

    const/16 v1, 0xc

    invoke-static {v0, v1}, Landroid/os/SystemProperties;->getInt(Ljava/lang/String;I)I

    move-result v0

    sput v0, Lcom/miui/server/sptm/MemoryOptimizeStrategy;->MEMORY_RECLAIM_APP_COUNT:I

    return-void
.end method

.method public constructor <init>()V
    .locals 3

    .line 41
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 22
    nop

    .line 23
    invoke-static {}, Lcom/miui/server/sptm/SpeedTestModeServiceImpl;->getInstance()Lcom/miui/server/sptm/SpeedTestModeServiceImpl;

    move-result-object v0

    iput-object v0, p0, Lcom/miui/server/sptm/MemoryOptimizeStrategy;->mSpeedTestModeService:Lcom/miui/server/sptm/SpeedTestModeServiceImpl;

    .line 30
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/miui/server/sptm/MemoryOptimizeStrategy;->SPT_MODE_OOM_MIN_FREE:[I

    .line 31
    const/4 v0, 0x0

    iput v0, p0, Lcom/miui/server/sptm/MemoryOptimizeStrategy;->mAppCounts:I

    .line 32
    iput-boolean v0, p0, Lcom/miui/server/sptm/MemoryOptimizeStrategy;->mIsProcCompactEnable:Z

    .line 42
    const-string v0, "activity"

    invoke-static {v0}, Landroid/os/ServiceManager;->getService(Ljava/lang/String;)Landroid/os/IBinder;

    move-result-object v0

    check-cast v0, Lcom/android/server/am/ActivityManagerService;

    iput-object v0, p0, Lcom/miui/server/sptm/MemoryOptimizeStrategy;->mAms:Lcom/android/server/am/ActivityManagerService;

    .line 43
    sget-object v0, Lcom/miui/server/sptm/MemoryOptimizeStrategy;->SPTM_OOM_MIN_FREE:Ljava/lang/String;

    invoke-static {v0}, Lcom/miui/server/sptm/MemoryOptimizeStrategy;->loadProperty(Ljava/lang/String;)[I

    move-result-object v0

    iput-object v0, p0, Lcom/miui/server/sptm/MemoryOptimizeStrategy;->SPT_MODE_OOM_MIN_FREE:[I

    .line 45
    iget-object v0, p0, Lcom/miui/server/sptm/MemoryOptimizeStrategy;->mAms:Lcom/android/server/am/ActivityManagerService;

    if-eqz v0, :cond_0

    .line 48
    nop

    .line 49
    invoke-virtual {v0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v0

    const-class v1, [I

    filled-new-array {v1}, [Ljava/lang/Class;

    move-result-object v1

    .line 48
    const-string/jumbo v2, "updateOomMinFree"

    invoke-static {v0, v2, v1}, Lmiui/util/ReflectionUtils;->tryFindMethodExact(Ljava/lang/Class;Ljava/lang/String;[Ljava/lang/Class;)Ljava/lang/reflect/Method;

    move-result-object v0

    iput-object v0, p0, Lcom/miui/server/sptm/MemoryOptimizeStrategy;->mMethodUpdateOomMinFree:Ljava/lang/reflect/Method;

    .line 50
    iget-object v0, p0, Lcom/miui/server/sptm/MemoryOptimizeStrategy;->mAms:Lcom/android/server/am/ActivityManagerService;

    .line 51
    invoke-virtual {v0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v0

    sget-object v1, Ljava/lang/Boolean;->TYPE:Ljava/lang/Class;

    filled-new-array {v1}, [Ljava/lang/Class;

    move-result-object v1

    .line 50
    const-string/jumbo v2, "updateOomForSPTM"

    invoke-static {v0, v2, v1}, Lmiui/util/ReflectionUtils;->tryFindMethodExact(Ljava/lang/Class;Ljava/lang/String;[Ljava/lang/Class;)Ljava/lang/reflect/Method;

    move-result-object v0

    iput-object v0, p0, Lcom/miui/server/sptm/MemoryOptimizeStrategy;->mMethodUpdateOomForSPTM:Ljava/lang/reflect/Method;

    .line 53
    :cond_0
    return-void
.end method

.method private static loadProperty(Ljava/lang/String;)[I
    .locals 5
    .param p0, "oomMinfree"    # Ljava/lang/String;

    .line 119
    invoke-static {p0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    const/4 v1, 0x0

    if-eqz v0, :cond_0

    .line 120
    return-object v1

    .line 123
    :cond_0
    const-string v0, ","

    invoke-virtual {p0, v0}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v0

    .line 124
    .local v0, "slice":[Ljava/lang/String;
    array-length v2, v0

    const/4 v3, 0x6

    if-eq v2, v3, :cond_1

    .line 125
    return-object v1

    .line 129
    :cond_1
    :try_start_0
    new-array v2, v3, [I

    .line 130
    .local v2, "res":[I
    const/4 v3, 0x0

    .local v3, "i":I
    :goto_0
    array-length v4, v0

    if-ge v3, v4, :cond_2

    .line 131
    aget-object v4, v0, v3

    invoke-static {v4}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v4

    aput v4, v2, v3
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 130
    add-int/lit8 v3, v3, 0x1

    goto :goto_0

    .line 133
    .end local v3    # "i":I
    :cond_2
    return-object v2

    .line 134
    .end local v2    # "res":[I
    :catch_0
    move-exception v2

    .line 135
    .local v2, "e":Ljava/lang/Exception;
    invoke-virtual {v2}, Ljava/lang/Exception;->printStackTrace()V

    .line 136
    return-object v1
.end method


# virtual methods
.method public onAppStarted(Lcom/miui/server/sptm/PreLoadStrategy$AppStartRecord;)V
    .locals 2
    .param p1, "r"    # Lcom/miui/server/sptm/PreLoadStrategy$AppStartRecord;

    .line 62
    sget-object v0, Lcom/miui/server/sptm/SpeedTestModeServiceImpl;->SPEED_TEST_APP_LIST:Ljava/util/List;

    iget-object v1, p1, Lcom/miui/server/sptm/PreLoadStrategy$AppStartRecord;->packageName:Ljava/lang/String;

    invoke-interface {v0, v1}, Ljava/util/List;->contains(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 63
    iget-object v0, p0, Lcom/miui/server/sptm/MemoryOptimizeStrategy;->mMiuiMemoryService:Lcom/android/server/am/MiuiMemoryServiceInternal;

    if-nez v0, :cond_0

    .line 64
    const-class v0, Lcom/android/server/am/MiuiMemoryServiceInternal;

    .line 65
    invoke-static {v0}, Lcom/android/server/LocalServices;->getService(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/android/server/am/MiuiMemoryServiceInternal;

    iput-object v0, p0, Lcom/miui/server/sptm/MemoryOptimizeStrategy;->mMiuiMemoryService:Lcom/android/server/am/MiuiMemoryServiceInternal;

    .line 79
    :cond_0
    return-void
.end method

.method public onNewEvent(I)V
    .locals 0
    .param p1, "eventType"    # I

    .line 58
    return-void
.end method

.method public onSpeedTestModeChanged(Z)V
    .locals 6
    .param p1, "isEnable"    # Z

    .line 83
    iget-object v0, p0, Lcom/miui/server/sptm/MemoryOptimizeStrategy;->SPT_MODE_OOM_MIN_FREE:[I

    if-eqz v0, :cond_7

    iget-object v0, p0, Lcom/miui/server/sptm/MemoryOptimizeStrategy;->mAms:Lcom/android/server/am/ActivityManagerService;

    if-nez v0, :cond_0

    goto :goto_4

    .line 87
    :cond_0
    const/4 v0, 0x0

    if-nez p1, :cond_1

    .line 88
    iput v0, p0, Lcom/miui/server/sptm/MemoryOptimizeStrategy;->mAppCounts:I

    .line 89
    iput-boolean v0, p0, Lcom/miui/server/sptm/MemoryOptimizeStrategy;->mIsProcCompactEnable:Z

    .line 92
    :cond_1
    invoke-static {}, Lcom/miui/server/sptm/SpeedTestModeServiceImpl;->isLowMemDeviceForSpeedTestMode()Z

    move-result v1

    if-nez v1, :cond_2

    .line 93
    return-void

    .line 96
    :cond_2
    iget-object v1, p0, Lcom/miui/server/sptm/MemoryOptimizeStrategy;->mMethodSetEnabledNativeSPTMode:Ljava/lang/reflect/Method;

    const/4 v2, 0x1

    if-eqz v1, :cond_3

    .line 98
    :try_start_0
    iget-object v3, p0, Lcom/miui/server/sptm/MemoryOptimizeStrategy;->mAms:Lcom/android/server/am/ActivityManagerService;

    new-array v4, v2, [Ljava/lang/Object;

    invoke-static {p1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v5

    aput-object v5, v4, v0

    invoke-virtual {v1, v3, v4}, Ljava/lang/reflect/Method;->invoke(Ljava/lang/Object;[Ljava/lang/Object;)Ljava/lang/Object;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 100
    goto :goto_0

    .line 99
    :catch_0
    move-exception v1

    .line 103
    :cond_3
    :goto_0
    iget-object v1, p0, Lcom/miui/server/sptm/MemoryOptimizeStrategy;->mMethodUpdateOomForSPTM:Ljava/lang/reflect/Method;

    if-eqz v1, :cond_4

    .line 105
    :try_start_1
    iget-object v3, p0, Lcom/miui/server/sptm/MemoryOptimizeStrategy;->mAms:Lcom/android/server/am/ActivityManagerService;

    new-array v4, v2, [Ljava/lang/Object;

    invoke-static {p1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v5

    aput-object v5, v4, v0

    invoke-virtual {v1, v3, v4}, Ljava/lang/reflect/Method;->invoke(Ljava/lang/Object;[Ljava/lang/Object;)Ljava/lang/Object;
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_1

    .line 107
    goto :goto_1

    .line 106
    :catch_1
    move-exception v1

    .line 110
    :cond_4
    :goto_1
    iget-object v1, p0, Lcom/miui/server/sptm/MemoryOptimizeStrategy;->mMethodUpdateOomMinFree:Ljava/lang/reflect/Method;

    if-eqz v1, :cond_6

    .line 112
    :try_start_2
    iget-object v3, p0, Lcom/miui/server/sptm/MemoryOptimizeStrategy;->mAms:Lcom/android/server/am/ActivityManagerService;

    new-array v2, v2, [Ljava/lang/Object;

    if-eqz p1, :cond_5

    iget-object v4, p0, Lcom/miui/server/sptm/MemoryOptimizeStrategy;->SPT_MODE_OOM_MIN_FREE:[I

    goto :goto_2

    :cond_5
    const/4 v4, 0x0

    :goto_2
    aput-object v4, v2, v0

    invoke-virtual {v1, v3, v2}, Ljava/lang/reflect/Method;->invoke(Ljava/lang/Object;[Ljava/lang/Object;)Ljava/lang/Object;
    :try_end_2
    .catch Ljava/lang/Exception; {:try_start_2 .. :try_end_2} :catch_2

    .line 114
    goto :goto_3

    .line 113
    :catch_2
    move-exception v0

    .line 116
    :cond_6
    :goto_3
    return-void

    .line 84
    :cond_7
    :goto_4
    return-void
.end method
