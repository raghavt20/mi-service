.class Lcom/miui/server/SplashScreenServiceDelegate$5;
.super Ljava/lang/Object;
.source "SplashScreenServiceDelegate.java"

# interfaces
.implements Ljava/util/concurrent/Callable;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/miui/server/SplashScreenServiceDelegate;->requestSplashScreen(Landroid/content/Intent;Landroid/content/pm/ActivityInfo;)Landroid/content/Intent;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Ljava/util/concurrent/Callable<",
        "Landroid/content/Intent;",
        ">;"
    }
.end annotation


# instance fields
.field final synthetic this$0:Lcom/miui/server/SplashScreenServiceDelegate;

.field final synthetic val$aInfo:Landroid/content/pm/ActivityInfo;

.field final synthetic val$intent:Landroid/content/Intent;

.field final synthetic val$sss:Lcom/miui/server/ISplashScreenService;


# direct methods
.method constructor <init>(Lcom/miui/server/SplashScreenServiceDelegate;Lcom/miui/server/ISplashScreenService;Landroid/content/Intent;Landroid/content/pm/ActivityInfo;)V
    .locals 0
    .param p1, "this$0"    # Lcom/miui/server/SplashScreenServiceDelegate;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .line 348
    iput-object p1, p0, Lcom/miui/server/SplashScreenServiceDelegate$5;->this$0:Lcom/miui/server/SplashScreenServiceDelegate;

    iput-object p2, p0, Lcom/miui/server/SplashScreenServiceDelegate$5;->val$sss:Lcom/miui/server/ISplashScreenService;

    iput-object p3, p0, Lcom/miui/server/SplashScreenServiceDelegate$5;->val$intent:Landroid/content/Intent;

    iput-object p4, p0, Lcom/miui/server/SplashScreenServiceDelegate$5;->val$aInfo:Landroid/content/pm/ActivityInfo;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public call()Landroid/content/Intent;
    .locals 4
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    .line 351
    const/4 v0, 0x0

    .line 352
    .local v0, "finalIntent":Landroid/content/Intent;
    iget-object v1, p0, Lcom/miui/server/SplashScreenServiceDelegate$5;->val$sss:Lcom/miui/server/ISplashScreenService;

    iget-object v2, p0, Lcom/miui/server/SplashScreenServiceDelegate$5;->val$intent:Landroid/content/Intent;

    iget-object v3, p0, Lcom/miui/server/SplashScreenServiceDelegate$5;->val$aInfo:Landroid/content/pm/ActivityInfo;

    invoke-interface {v1, v2, v3}, Lcom/miui/server/ISplashScreenService;->requestSplashScreen(Landroid/content/Intent;Landroid/content/pm/ActivityInfo;)Landroid/content/Intent;

    move-result-object v1

    move-object v0, v1

    if-eqz v1, :cond_0

    .line 353
    return-object v0

    .line 355
    :cond_0
    iget-object v1, p0, Lcom/miui/server/SplashScreenServiceDelegate$5;->val$intent:Landroid/content/Intent;

    return-object v1
.end method

.method public bridge synthetic call()Ljava/lang/Object;
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    .line 348
    invoke-virtual {p0}, Lcom/miui/server/SplashScreenServiceDelegate$5;->call()Landroid/content/Intent;

    move-result-object v0

    return-object v0
.end method
