.class Lcom/miui/server/SecurityManagerService$MyPackageObserver;
.super Ljava/lang/Object;
.source "SecurityManagerService.java"

# interfaces
.implements Landroid/content/pm/PackageManagerInternal$PackageListObserver;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/miui/server/SecurityManagerService;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x8
    name = "MyPackageObserver"
.end annotation


# instance fields
.field private final mContext:Landroid/content/Context;

.field private final mDefaultBrowserImpl:Lcom/miui/server/security/DefaultBrowserImpl;


# direct methods
.method constructor <init>(Landroid/content/Context;Lcom/miui/server/security/DefaultBrowserImpl;)V
    .locals 0
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "defaultBrowserImpl"    # Lcom/miui/server/security/DefaultBrowserImpl;

    .line 163
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 164
    iput-object p1, p0, Lcom/miui/server/SecurityManagerService$MyPackageObserver;->mContext:Landroid/content/Context;

    .line 165
    iput-object p2, p0, Lcom/miui/server/SecurityManagerService$MyPackageObserver;->mDefaultBrowserImpl:Lcom/miui/server/security/DefaultBrowserImpl;

    .line 166
    return-void
.end method


# virtual methods
.method public onPackageAdded(Ljava/lang/String;I)V
    .locals 2
    .param p1, "packageName"    # Ljava/lang/String;
    .param p2, "uid"    # I

    .line 169
    invoke-static {}, Lmiui/security/WakePathChecker;->getInstance()Lmiui/security/WakePathChecker;

    move-result-object v0

    iget-object v1, p0, Lcom/miui/server/SecurityManagerService$MyPackageObserver;->mContext:Landroid/content/Context;

    invoke-virtual {v0, v1}, Lmiui/security/WakePathChecker;->onPackageAdded(Landroid/content/Context;)V

    .line 170
    iget-object v0, p0, Lcom/miui/server/SecurityManagerService$MyPackageObserver;->mDefaultBrowserImpl:Lcom/miui/server/security/DefaultBrowserImpl;

    invoke-virtual {v0, p1}, Lcom/miui/server/security/DefaultBrowserImpl;->checkDefaultBrowser(Ljava/lang/String;)V

    .line 171
    return-void
.end method

.method public onPackageChanged(Ljava/lang/String;I)V
    .locals 1
    .param p1, "packageName"    # Ljava/lang/String;
    .param p2, "uid"    # I

    .line 180
    iget-object v0, p0, Lcom/miui/server/SecurityManagerService$MyPackageObserver;->mDefaultBrowserImpl:Lcom/miui/server/security/DefaultBrowserImpl;

    invoke-virtual {v0, p1}, Lcom/miui/server/security/DefaultBrowserImpl;->checkDefaultBrowser(Ljava/lang/String;)V

    .line 181
    return-void
.end method

.method public onPackageRemoved(Ljava/lang/String;I)V
    .locals 1
    .param p1, "packageName"    # Ljava/lang/String;
    .param p2, "uid"    # I

    .line 175
    iget-object v0, p0, Lcom/miui/server/SecurityManagerService$MyPackageObserver;->mDefaultBrowserImpl:Lcom/miui/server/security/DefaultBrowserImpl;

    invoke-virtual {v0, p1}, Lcom/miui/server/security/DefaultBrowserImpl;->checkDefaultBrowser(Ljava/lang/String;)V

    .line 176
    return-void
.end method
