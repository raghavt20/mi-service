public abstract class com.miui.server.ISplashScreenService$Stub extends android.os.Binder implements com.miui.server.ISplashScreenService {
	 /* .source "ISplashScreenService.java" */
	 /* # interfaces */
	 /* # annotations */
	 /* .annotation system Ldalvik/annotation/EnclosingClass; */
	 /* value = Lcom/miui/server/ISplashScreenService; */
} // .end annotation
/* .annotation system Ldalvik/annotation/InnerClass; */
/* accessFlags = 0x409 */
/* name = "Stub" */
} // .end annotation
/* .annotation system Ldalvik/annotation/MemberClasses; */
/* value = { */
/* Lcom/miui/server/ISplashScreenService$Stub$Proxy; */
/* } */
} // .end annotation
/* # static fields */
private static final java.lang.String DESCRIPTOR;
static final Integer TRANSACTION_activityIdle;
static final Integer TRANSACTION_destroyActivity;
static final Integer TRANSACTION_requestSplashScreen;
static final Integer TRANSACTION_setSplashPackageListener;
/* # direct methods */
public com.miui.server.ISplashScreenService$Stub ( ) {
/* .locals 1 */
/* .line 107 */
/* invoke-direct {p0}, Landroid/os/Binder;-><init>()V */
/* .line 108 */
final String v0 = "com.miui.server.ISplashScreenService"; // const-string v0, "com.miui.server.ISplashScreenService"
(( com.miui.server.ISplashScreenService$Stub ) p0 ).attachInterface ( p0, v0 ); // invoke-virtual {p0, p0, v0}, Lcom/miui/server/ISplashScreenService$Stub;->attachInterface(Landroid/os/IInterface;Ljava/lang/String;)V
/* .line 109 */
return;
} // .end method
public static com.miui.server.ISplashScreenService asInterface ( android.os.IBinder p0 ) {
/* .locals 2 */
/* .param p0, "obj" # Landroid/os/IBinder; */
/* .line 116 */
/* if-nez p0, :cond_0 */
/* .line 117 */
int v0 = 0; // const/4 v0, 0x0
/* .line 119 */
} // :cond_0
final String v0 = "com.miui.server.ISplashScreenService"; // const-string v0, "com.miui.server.ISplashScreenService"
/* .line 120 */
/* .local v0, "iin":Landroid/os/IInterface; */
if ( v0 != null) { // if-eqz v0, :cond_1
/* instance-of v1, v0, Lcom/miui/server/ISplashScreenService; */
if ( v1 != null) { // if-eqz v1, :cond_1
/* .line 121 */
/* move-object v1, v0 */
/* check-cast v1, Lcom/miui/server/ISplashScreenService; */
/* .line 123 */
} // :cond_1
/* new-instance v1, Lcom/miui/server/ISplashScreenService$Stub$Proxy; */
/* invoke-direct {v1, p0}, Lcom/miui/server/ISplashScreenService$Stub$Proxy;-><init>(Landroid/os/IBinder;)V */
} // .end method
/* # virtual methods */
public android.os.IBinder asBinder ( ) {
/* .locals 0 */
/* .line 128 */
} // .end method
public Boolean onTransact ( Integer p0, android.os.Parcel p1, android.os.Parcel p2, Integer p3 ) {
/* .locals 5 */
/* .param p1, "code" # I */
/* .param p2, "data" # Landroid/os/Parcel; */
/* .param p3, "reply" # Landroid/os/Parcel; */
/* .param p4, "flags" # I */
/* .annotation system Ldalvik/annotation/Throws; */
/* value = { */
/* Landroid/os/RemoteException; */
/* } */
} // .end annotation
/* .line 134 */
final String v0 = "com.miui.server.ISplashScreenService"; // const-string v0, "com.miui.server.ISplashScreenService"
int v1 = 1; // const/4 v1, 0x1
/* sparse-switch p1, :sswitch_data_0 */
/* .line 179 */
v0 = /* invoke-super {p0, p1, p2, p3, p4}, Landroid/os/Binder;->onTransact(ILandroid/os/Parcel;Landroid/os/Parcel;I)Z */
/* .line 136 */
/* :sswitch_0 */
(( android.os.Parcel ) p3 ).writeString ( v0 ); // invoke-virtual {p3, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V
/* .line 137 */
/* .line 172 */
/* :sswitch_1 */
(( android.os.Parcel ) p2 ).enforceInterface ( v0 ); // invoke-virtual {p2, v0}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V
/* .line 174 */
(( android.os.Parcel ) p2 ).readStrongBinder ( ); // invoke-virtual {p2}, Landroid/os/Parcel;->readStrongBinder()Landroid/os/IBinder;
com.miui.server.ISplashPackageCheckListener$Stub .asInterface ( v0 );
/* .line 175 */
/* .local v0, "_arg0":Lcom/miui/server/ISplashPackageCheckListener; */
(( com.miui.server.ISplashScreenService$Stub ) p0 ).setSplashPackageListener ( v0 ); // invoke-virtual {p0, v0}, Lcom/miui/server/ISplashScreenService$Stub;->setSplashPackageListener(Lcom/miui/server/ISplashPackageCheckListener;)V
/* .line 176 */
/* .line 164 */
} // .end local v0 # "_arg0":Lcom/miui/server/ISplashPackageCheckListener;
/* :sswitch_2 */
(( android.os.Parcel ) p2 ).enforceInterface ( v0 ); // invoke-virtual {p2, v0}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V
/* .line 165 */
v0 = android.content.pm.ActivityInfo.CREATOR;
/* check-cast v0, Landroid/content/pm/ActivityInfo; */
/* .line 166 */
/* .local v0, "aInfo":Landroid/content/pm/ActivityInfo; */
(( com.miui.server.ISplashScreenService$Stub ) p0 ).destroyActivity ( v0 ); // invoke-virtual {p0, v0}, Lcom/miui/server/ISplashScreenService$Stub;->destroyActivity(Landroid/content/pm/ActivityInfo;)V
/* .line 167 */
/* .line 156 */
} // .end local v0 # "aInfo":Landroid/content/pm/ActivityInfo;
/* :sswitch_3 */
(( android.os.Parcel ) p2 ).enforceInterface ( v0 ); // invoke-virtual {p2, v0}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V
/* .line 157 */
v0 = android.content.pm.ActivityInfo.CREATOR;
/* check-cast v0, Landroid/content/pm/ActivityInfo; */
/* .line 158 */
/* .restart local v0 # "aInfo":Landroid/content/pm/ActivityInfo; */
(( com.miui.server.ISplashScreenService$Stub ) p0 ).activityIdle ( v0 ); // invoke-virtual {p0, v0}, Lcom/miui/server/ISplashScreenService$Stub;->activityIdle(Landroid/content/pm/ActivityInfo;)V
/* .line 159 */
(( android.os.Parcel ) p3 ).writeNoException ( ); // invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V
/* .line 160 */
/* .line 142 */
} // .end local v0 # "aInfo":Landroid/content/pm/ActivityInfo;
/* :sswitch_4 */
(( android.os.Parcel ) p2 ).enforceInterface ( v0 ); // invoke-virtual {p2, v0}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V
/* .line 143 */
v0 = android.content.Intent.CREATOR;
/* check-cast v0, Landroid/content/Intent; */
/* .line 144 */
/* .local v0, "intent":Landroid/content/Intent; */
v2 = android.content.pm.ActivityInfo.CREATOR;
/* check-cast v2, Landroid/content/pm/ActivityInfo; */
/* .line 145 */
/* .local v2, "aInfo":Landroid/content/pm/ActivityInfo; */
(( com.miui.server.ISplashScreenService$Stub ) p0 ).requestSplashScreen ( v0, v2 ); // invoke-virtual {p0, v0, v2}, Lcom/miui/server/ISplashScreenService$Stub;->requestSplashScreen(Landroid/content/Intent;Landroid/content/pm/ActivityInfo;)Landroid/content/Intent;
/* .line 146 */
/* .local v3, "_result":Landroid/content/Intent; */
(( android.os.Parcel ) p3 ).writeNoException ( ); // invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V
/* .line 147 */
if ( v3 != null) { // if-eqz v3, :cond_0
/* .line 148 */
(( android.os.Parcel ) p3 ).writeInt ( v1 ); // invoke-virtual {p3, v1}, Landroid/os/Parcel;->writeInt(I)V
/* .line 149 */
(( android.content.Intent ) v3 ).writeToParcel ( p3, v1 ); // invoke-virtual {v3, p3, v1}, Landroid/content/Intent;->writeToParcel(Landroid/os/Parcel;I)V
/* .line 151 */
} // :cond_0
int v4 = 0; // const/4 v4, 0x0
(( android.os.Parcel ) p3 ).writeInt ( v4 ); // invoke-virtual {p3, v4}, Landroid/os/Parcel;->writeInt(I)V
/* .line 153 */
} // :goto_0
/* nop */
/* :sswitch_data_0 */
/* .sparse-switch */
/* 0x1 -> :sswitch_4 */
/* 0x2 -> :sswitch_3 */
/* 0x3 -> :sswitch_2 */
/* 0x4 -> :sswitch_1 */
/* 0x5f4e5446 -> :sswitch_0 */
} // .end sparse-switch
} // .end method
