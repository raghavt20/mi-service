class com.miui.server.MiuiUsbService$UsbDebuggingManager$UsbDebuggingHandler extends android.os.Handler {
	 /* .source "MiuiUsbService.java" */
	 /* # annotations */
	 /* .annotation system Ldalvik/annotation/EnclosingClass; */
	 /* value = Lcom/miui/server/MiuiUsbService$UsbDebuggingManager; */
} // .end annotation
/* .annotation system Ldalvik/annotation/InnerClass; */
/* accessFlags = 0x0 */
/* name = "UsbDebuggingHandler" */
} // .end annotation
/* # static fields */
private static final Integer MESSAGE_ADB_ALLOW;
private static final Integer MESSAGE_ADB_CONFIRM;
private static final Integer MESSAGE_ADB_DENY;
private static final Integer MESSAGE_ADB_DISABLED;
private static final Integer MESSAGE_ADB_ENABLED;
/* # instance fields */
final com.miui.server.MiuiUsbService$UsbDebuggingManager this$1; //synthetic
/* # direct methods */
public com.miui.server.MiuiUsbService$UsbDebuggingManager$UsbDebuggingHandler ( ) {
/* .locals 0 */
/* .param p1, "this$1" # Lcom/miui/server/MiuiUsbService$UsbDebuggingManager; */
/* .param p2, "looper" # Landroid/os/Looper; */
/* .line 206 */
this.this$1 = p1;
/* .line 207 */
/* invoke-direct {p0, p2}, Landroid/os/Handler;-><init>(Landroid/os/Looper;)V */
/* .line 208 */
return;
} // .end method
/* # virtual methods */
public void handleMessage ( android.os.Message p0 ) {
/* .locals 4 */
/* .param p1, "msg" # Landroid/os/Message; */
/* .line 211 */
/* iget v0, p1, Landroid/os/Message;->what:I */
int v1 = 1; // const/4 v1, 0x1
/* packed-switch v0, :pswitch_data_0 */
/* goto/16 :goto_1 */
/* .line 263 */
/* :pswitch_0 */
v0 = this.obj;
/* check-cast v0, Ljava/lang/String; */
/* .line 264 */
/* .local v0, "key":Ljava/lang/String; */
v1 = this.this$1;
com.miui.server.MiuiUsbService$UsbDebuggingManager .-$$Nest$mgetFingerprints ( v1,v0 );
com.miui.server.MiuiUsbService$UsbDebuggingManager .-$$Nest$fputmFingerprints ( v1,v2 );
/* .line 265 */
v1 = this.this$1;
com.miui.server.MiuiUsbService$UsbDebuggingManager .-$$Nest$fgetmFingerprints ( v1 );
com.miui.server.MiuiUsbService$UsbDebuggingManager .-$$Nest$mshowConfirmationDialog ( v1,v0,v2 );
/* .line 266 */
/* goto/16 :goto_1 */
/* .line 259 */
} // .end local v0 # "key":Ljava/lang/String;
/* :pswitch_1 */
v0 = this.this$1;
final String v1 = "NO"; // const-string v1, "NO"
(( com.miui.server.MiuiUsbService$UsbDebuggingManager ) v0 ).sendResponse ( v1 ); // invoke-virtual {v0, v1}, Lcom/miui/server/MiuiUsbService$UsbDebuggingManager;->sendResponse(Ljava/lang/String;)V
/* .line 260 */
/* goto/16 :goto_1 */
/* .line 241 */
/* :pswitch_2 */
v0 = this.obj;
/* check-cast v0, Ljava/lang/String; */
/* .line 242 */
/* .restart local v0 # "key":Ljava/lang/String; */
v2 = this.this$1;
com.miui.server.MiuiUsbService$UsbDebuggingManager .-$$Nest$mgetFingerprints ( v2,v0 );
/* .line 244 */
/* .local v2, "fingerprints":Ljava/lang/String; */
v3 = this.this$1;
com.miui.server.MiuiUsbService$UsbDebuggingManager .-$$Nest$fgetmFingerprints ( v3 );
v3 = (( java.lang.String ) v2 ).equals ( v3 ); // invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z
/* if-nez v3, :cond_0 */
/* .line 245 */
/* new-instance v1, Ljava/lang/StringBuilder; */
/* invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V */
final String v3 = "Fingerprints do not match.Got "; // const-string v3, "Fingerprints do not match.Got "
(( java.lang.StringBuilder ) v1 ).append ( v3 ); // invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v1 ).append ( v2 ); // invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
final String v3 = ", expected "; // const-string v3, ", expected "
(( java.lang.StringBuilder ) v1 ).append ( v3 ); // invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
v3 = this.this$1;
com.miui.server.MiuiUsbService$UsbDebuggingManager .-$$Nest$fgetmFingerprints ( v3 );
(( java.lang.StringBuilder ) v1 ).append ( v3 ); // invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v1 ).toString ( ); // invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
final String v3 = "MiuiUsbService"; // const-string v3, "MiuiUsbService"
android.util.Slog .e ( v3,v1 );
/* .line 247 */
/* .line 250 */
} // :cond_0
/* iget v3, p1, Landroid/os/Message;->arg1:I */
/* if-ne v3, v1, :cond_1 */
/* .line 251 */
v1 = this.this$1;
com.miui.server.MiuiUsbService$UsbDebuggingManager .-$$Nest$mwriteKey ( v1,v0 );
/* .line 254 */
} // :cond_1
v1 = this.this$1;
final String v3 = "OK"; // const-string v3, "OK"
(( com.miui.server.MiuiUsbService$UsbDebuggingManager ) v1 ).sendResponse ( v3 ); // invoke-virtual {v1, v3}, Lcom/miui/server/MiuiUsbService$UsbDebuggingManager;->sendResponse(Ljava/lang/String;)V
/* .line 255 */
/* .line 224 */
} // .end local v0 # "key":Ljava/lang/String;
} // .end local v2 # "fingerprints":Ljava/lang/String;
/* :pswitch_3 */
v0 = this.this$1;
v0 = com.miui.server.MiuiUsbService$UsbDebuggingManager .-$$Nest$fgetmAdbEnabled ( v0 );
/* if-nez v0, :cond_2 */
/* .line 225 */
/* .line 227 */
} // :cond_2
v0 = this.this$1;
int v1 = 0; // const/4 v1, 0x0
com.miui.server.MiuiUsbService$UsbDebuggingManager .-$$Nest$fputmAdbEnabled ( v0,v1 );
/* .line 228 */
v0 = this.this$1;
(( com.miui.server.MiuiUsbService$UsbDebuggingManager ) v0 ).closeSocket ( ); // invoke-virtual {v0}, Lcom/miui/server/MiuiUsbService$UsbDebuggingManager;->closeSocket()V
/* .line 231 */
try { // :try_start_0
v0 = this.this$1;
com.miui.server.MiuiUsbService$UsbDebuggingManager .-$$Nest$fgetmThread ( v0 );
(( java.lang.Thread ) v0 ).join ( ); // invoke-virtual {v0}, Ljava/lang/Thread;->join()V
/* :try_end_0 */
/* .catch Ljava/lang/Exception; {:try_start_0 ..:try_end_0} :catch_0 */
/* .line 233 */
/* .line 232 */
/* :catch_0 */
/* move-exception v0 */
/* .line 235 */
} // :goto_0
v0 = this.this$1;
int v1 = 0; // const/4 v1, 0x0
com.miui.server.MiuiUsbService$UsbDebuggingManager .-$$Nest$fputmThread ( v0,v1 );
/* .line 236 */
v0 = this.this$1;
this.mOutputStream = v1;
/* .line 237 */
v0 = this.this$1;
this.mSocket = v1;
/* .line 238 */
/* .line 213 */
/* :pswitch_4 */
v0 = this.this$1;
v0 = com.miui.server.MiuiUsbService$UsbDebuggingManager .-$$Nest$fgetmAdbEnabled ( v0 );
if ( v0 != null) { // if-eqz v0, :cond_3
/* .line 214 */
/* .line 216 */
} // :cond_3
v0 = this.this$1;
com.miui.server.MiuiUsbService$UsbDebuggingManager .-$$Nest$fputmAdbEnabled ( v0,v1 );
/* .line 218 */
v0 = this.this$1;
/* new-instance v1, Ljava/lang/Thread; */
v2 = this.this$1;
/* invoke-direct {v1, v2}, Ljava/lang/Thread;-><init>(Ljava/lang/Runnable;)V */
com.miui.server.MiuiUsbService$UsbDebuggingManager .-$$Nest$fputmThread ( v0,v1 );
/* .line 219 */
v0 = this.this$1;
com.miui.server.MiuiUsbService$UsbDebuggingManager .-$$Nest$fgetmThread ( v0 );
(( java.lang.Thread ) v0 ).start ( ); // invoke-virtual {v0}, Ljava/lang/Thread;->start()V
/* .line 221 */
/* nop */
/* .line 269 */
} // :goto_1
return;
/* :pswitch_data_0 */
/* .packed-switch 0x1 */
/* :pswitch_4 */
/* :pswitch_3 */
/* :pswitch_2 */
/* :pswitch_1 */
/* :pswitch_0 */
} // .end packed-switch
} // .end method
