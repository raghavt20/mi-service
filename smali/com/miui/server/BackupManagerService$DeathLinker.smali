.class Lcom/miui/server/BackupManagerService$DeathLinker;
.super Ljava/lang/Object;
.source "BackupManagerService.java"

# interfaces
.implements Landroid/os/IBinder$DeathRecipient;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/miui/server/BackupManagerService;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "DeathLinker"
.end annotation


# instance fields
.field final synthetic this$0:Lcom/miui/server/BackupManagerService;


# direct methods
.method private constructor <init>(Lcom/miui/server/BackupManagerService;)V
    .locals 0

    .line 886
    iput-object p1, p0, Lcom/miui/server/BackupManagerService$DeathLinker;->this$0:Lcom/miui/server/BackupManagerService;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method synthetic constructor <init>(Lcom/miui/server/BackupManagerService;Lcom/miui/server/BackupManagerService$DeathLinker-IA;)V
    .locals 0

    invoke-direct {p0, p1}, Lcom/miui/server/BackupManagerService$DeathLinker;-><init>(Lcom/miui/server/BackupManagerService;)V

    return-void
.end method


# virtual methods
.method public binderDied()V
    .locals 4

    .line 889
    const-string v0, "Client binder has died. Start canceling..."

    const-string v1, "Backup:BackupManagerService"

    invoke-static {v1, v0}, Landroid/util/Slog;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 890
    iget-object v0, p0, Lcom/miui/server/BackupManagerService$DeathLinker;->this$0:Lcom/miui/server/BackupManagerService;

    const/4 v2, 0x1

    invoke-static {v0, v2}, Lcom/miui/server/BackupManagerService;->-$$Nest$fputmIsCanceling(Lcom/miui/server/BackupManagerService;Z)V

    .line 891
    iget-object v0, p0, Lcom/miui/server/BackupManagerService$DeathLinker;->this$0:Lcom/miui/server/BackupManagerService;

    invoke-static {v0}, Lcom/miui/server/BackupManagerService;->-$$Nest$mscheduleReleaseResource(Lcom/miui/server/BackupManagerService;)V

    .line 892
    iget-object v0, p0, Lcom/miui/server/BackupManagerService$DeathLinker;->this$0:Lcom/miui/server/BackupManagerService;

    invoke-static {v0}, Lcom/miui/server/BackupManagerService;->-$$Nest$mwaitForTheLastWorkingTask(Lcom/miui/server/BackupManagerService;)V

    .line 893
    iget-object v0, p0, Lcom/miui/server/BackupManagerService$DeathLinker;->this$0:Lcom/miui/server/BackupManagerService;

    const/4 v2, 0x0

    invoke-static {v0, v2}, Lcom/miui/server/BackupManagerService;->-$$Nest$fputmIsCanceling(Lcom/miui/server/BackupManagerService;Z)V

    .line 895
    iget-object v0, p0, Lcom/miui/server/BackupManagerService$DeathLinker;->this$0:Lcom/miui/server/BackupManagerService;

    const/4 v3, -0x1

    invoke-static {v0, v3}, Lcom/miui/server/BackupManagerService;->-$$Nest$fputmOwnerPid(Lcom/miui/server/BackupManagerService;I)V

    .line 896
    iget-object v0, p0, Lcom/miui/server/BackupManagerService$DeathLinker;->this$0:Lcom/miui/server/BackupManagerService;

    invoke-static {v0}, Lcom/miui/server/BackupManagerService;->-$$Nest$fgetmICaller(Lcom/miui/server/BackupManagerService;)Landroid/os/IBinder;

    move-result-object v0

    iget-object v3, p0, Lcom/miui/server/BackupManagerService$DeathLinker;->this$0:Lcom/miui/server/BackupManagerService;

    invoke-static {v3}, Lcom/miui/server/BackupManagerService;->-$$Nest$fgetmDeathLinker(Lcom/miui/server/BackupManagerService;)Lcom/miui/server/BackupManagerService$DeathLinker;

    move-result-object v3

    invoke-interface {v0, v3, v2}, Landroid/os/IBinder;->unlinkToDeath(Landroid/os/IBinder$DeathRecipient;I)Z

    .line 897
    iget-object v0, p0, Lcom/miui/server/BackupManagerService$DeathLinker;->this$0:Lcom/miui/server/BackupManagerService;

    const/4 v2, 0x0

    invoke-static {v0, v2}, Lcom/miui/server/BackupManagerService;->-$$Nest$fputmICaller(Lcom/miui/server/BackupManagerService;Landroid/os/IBinder;)V

    .line 898
    iget-object v0, p0, Lcom/miui/server/BackupManagerService$DeathLinker;->this$0:Lcom/miui/server/BackupManagerService;

    invoke-static {v0, v2}, Lcom/miui/server/BackupManagerService;->-$$Nest$fputmPreviousWorkingPkg(Lcom/miui/server/BackupManagerService;Ljava/lang/String;)V

    .line 900
    :try_start_0
    iget-object v0, p0, Lcom/miui/server/BackupManagerService$DeathLinker;->this$0:Lcom/miui/server/BackupManagerService;

    invoke-static {v0}, Lcom/miui/server/BackupManagerService;->-$$Nest$mbroadcastServiceIdle(Lcom/miui/server/BackupManagerService;)V
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    .line 903
    goto :goto_0

    .line 901
    :catch_0
    move-exception v0

    .line 902
    .local v0, "e":Landroid/os/RemoteException;
    invoke-virtual {v0}, Landroid/os/RemoteException;->printStackTrace()V

    .line 904
    .end local v0    # "e":Landroid/os/RemoteException;
    :goto_0
    const-string v0, "Client binder has died. Cancel completed!"

    invoke-static {v1, v0}, Landroid/util/Slog;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 905
    return-void
.end method
