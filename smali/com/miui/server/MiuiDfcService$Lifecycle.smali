.class public final Lcom/miui/server/MiuiDfcService$Lifecycle;
.super Lcom/android/server/SystemService;
.source "MiuiDfcService.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/miui/server/MiuiDfcService;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "Lifecycle"
.end annotation


# instance fields
.field private final mService:Lcom/miui/server/MiuiDfcService;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 1
    .param p1, "context"    # Landroid/content/Context;

    .line 50
    invoke-direct {p0, p1}, Lcom/android/server/SystemService;-><init>(Landroid/content/Context;)V

    .line 51
    invoke-static {}, Lcom/miui/server/MiuiDfcService;->getInstance()Lcom/miui/server/MiuiDfcService;

    move-result-object v0

    invoke-virtual {v0, p1}, Lcom/miui/server/MiuiDfcService;->forDfcInitialization(Landroid/content/Context;)Lcom/miui/server/MiuiDfcService;

    move-result-object v0

    iput-object v0, p0, Lcom/miui/server/MiuiDfcService$Lifecycle;->mService:Lcom/miui/server/MiuiDfcService;

    .line 52
    return-void
.end method


# virtual methods
.method public onStart()V
    .locals 2

    .line 56
    const-string v0, "miui.dfc.service"

    iget-object v1, p0, Lcom/miui/server/MiuiDfcService$Lifecycle;->mService:Lcom/miui/server/MiuiDfcService;

    invoke-virtual {p0, v0, v1}, Lcom/miui/server/MiuiDfcService$Lifecycle;->publishBinderService(Ljava/lang/String;Landroid/os/IBinder;)V

    .line 57
    return-void
.end method
