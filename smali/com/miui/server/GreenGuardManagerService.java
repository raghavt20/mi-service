public class com.miui.server.GreenGuardManagerService {
	 /* .source "GreenGuardManagerService.java" */
	 /* # annotations */
	 /* .annotation system Ldalvik/annotation/MemberClasses; */
	 /* value = { */
	 /* Lcom/miui/server/GreenGuardManagerService$GreenGuardServiceConn; */
	 /* } */
} // .end annotation
/* # static fields */
public static final java.lang.String GREEN_KID_AGENT_PKG_NAME;
public static final java.lang.String GREEN_KID_SERVICE;
private static final java.lang.String TAG;
private static android.content.ServiceConnection mGreenGuardServiceConnection;
/* # direct methods */
static void -$$Nest$smstartWatchGreenGuardProcessInner ( android.content.Context p0 ) { //bridge//synthethic
	 /* .locals 0 */
	 com.miui.server.GreenGuardManagerService .startWatchGreenGuardProcessInner ( p0 );
	 return;
} // .end method
public com.miui.server.GreenGuardManagerService ( ) {
	 /* .locals 0 */
	 /* .line 22 */
	 /* invoke-direct {p0}, Ljava/lang/Object;-><init>()V */
	 return;
} // .end method
private static void disableAgentProcess ( android.content.Context p0 ) {
	 /* .locals 5 */
	 /* .param p0, "context" # Landroid/content/Context; */
	 /* .line 37 */
	 final String v0 = "GreenKidManagerService"; // const-string v0, "GreenKidManagerService"
	 final String v1 = "com.miui.greenguard"; // const-string v1, "com.miui.greenguard"
	 (( android.content.Context ) p0 ).getPackageManager ( ); // invoke-virtual {p0}, Landroid/content/Context;->getPackageManager()Landroid/content/pm/PackageManager;
	 /* .line 39 */
	 /* .local v2, "packageManager":Landroid/content/pm/PackageManager; */
	 /* const/16 v3, 0x2000 */
	 try { // :try_start_0
		 (( android.content.pm.PackageManager ) v2 ).getApplicationInfo ( v1, v3 ); // invoke-virtual {v2, v1, v3}, Landroid/content/pm/PackageManager;->getApplicationInfo(Ljava/lang/String;I)Landroid/content/pm/ApplicationInfo;
		 if ( v3 != null) { // if-eqz v3, :cond_0
			 /* .line 40 */
			 int v3 = 2; // const/4 v3, 0x2
			 int v4 = 0; // const/4 v4, 0x0
			 (( android.content.pm.PackageManager ) v2 ).setApplicationEnabledSetting ( v1, v3, v4 ); // invoke-virtual {v2, v1, v3, v4}, Landroid/content/pm/PackageManager;->setApplicationEnabledSetting(Ljava/lang/String;II)V
			 /* .line 41 */
			 final String v1 = "Disable GreenGuard agent : [ com.miui.greenguard] ."; // const-string v1, "Disable GreenGuard agent : [ com.miui.greenguard] ."
			 android.util.Slog .i ( v0,v1 );
			 /* :try_end_0 */
			 /* .catch Ljava/lang/Exception; {:try_start_0 ..:try_end_0} :catch_0 */
			 /* .line 45 */
		 } // :cond_0
		 /* .line 43 */
		 /* :catch_0 */
		 /* move-exception v1 */
		 /* .line 44 */
		 /* .local v1, "e":Ljava/lang/Exception; */
		 final String v3 = "Disable greenGuard agent : [ com.miui.greenguard] failed , package not install"; // const-string v3, "Disable greenGuard agent : [ com.miui.greenguard] failed , package not install"
		 android.util.Slog .e ( v0,v3,v1 );
		 /* .line 46 */
	 } // .end local v1 # "e":Ljava/lang/Exception;
} // :goto_0
return;
} // .end method
public static void init ( android.content.Context p0 ) {
/* .locals 1 */
/* .param p0, "context" # Landroid/content/Context; */
/* .line 29 */
v0 = com.miui.server.GreenGuardManagerService .isGreenKidActive ( p0 );
/* if-nez v0, :cond_0 */
v0 = com.miui.server.GreenGuardManagerService .isGreenKidNeedWipe ( p0 );
/* if-nez v0, :cond_0 */
/* .line 30 */
com.miui.server.GreenGuardManagerService .disableAgentProcess ( p0 );
/* .line 32 */
} // :cond_0
/* new-instance v0, Lcom/miui/server/GreenGuardManagerService$GreenGuardServiceConn; */
/* invoke-direct {v0, p0}, Lcom/miui/server/GreenGuardManagerService$GreenGuardServiceConn;-><init>(Landroid/content/Context;)V */
/* .line 34 */
} // :goto_0
return;
} // .end method
private static Boolean isGreenKidActive ( android.content.Context p0 ) {
/* .locals 1 */
/* .param p0, "context" # Landroid/content/Context; */
/* .line 49 */
(( android.content.Context ) p0 ).getContentResolver ( ); // invoke-virtual {p0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;
v0 = android.provider.MiuiSettings$Secure .isGreenKidActive ( v0 );
} // .end method
private static Boolean isGreenKidNeedWipe ( android.content.Context p0 ) {
/* .locals 3 */
/* .param p0, "context" # Landroid/content/Context; */
/* .line 53 */
(( android.content.Context ) p0 ).getContentResolver ( ); // invoke-virtual {p0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;
final String v1 = "key_url_green_guard_sdk_need_clear_data"; // const-string v1, "key_url_green_guard_sdk_need_clear_data"
int v2 = 0; // const/4 v2, 0x0
v0 = android.provider.Settings$Secure .getInt ( v0,v1,v2 );
int v1 = 1; // const/4 v1, 0x1
/* if-ne v0, v1, :cond_0 */
/* move v2, v1 */
} // :cond_0
} // .end method
public static void startWatchGreenGuardProcess ( android.content.Context p0 ) {
/* .locals 5 */
/* .param p0, "context" # Landroid/content/Context; */
/* .line 58 */
v0 = android.os.Binder .getCallingUid ( );
/* .line 59 */
/* .local v0, "callingUid":I */
v1 = android.os.Binder .getCallingPid ( );
/* .line 60 */
/* .local v1, "callingPid":I */
com.android.server.am.ProcessUtils .getPackageNameByPid ( v1 );
/* .line 61 */
/* .local v2, "callingPackageName":Ljava/lang/String; */
v3 = android.os.UserHandle .getAppId ( v0 );
/* const/16 v4, 0x3e8 */
/* if-eq v3, v4, :cond_1 */
/* .line 62 */
final String v3 = "com.miui.greenguard"; // const-string v3, "com.miui.greenguard"
v3 = (( java.lang.String ) v3 ).equals ( v2 ); // invoke-virtual {v3, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z
/* if-nez v3, :cond_1 */
/* .line 63 */
/* new-instance v3, Ljava/lang/StringBuilder; */
/* invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V */
final String v4 = "Permission Denial from pid="; // const-string v4, "Permission Denial from pid="
(( java.lang.StringBuilder ) v3 ).append ( v4 ); // invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v3 ).append ( v1 ); // invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;
final String v4 = ", uid="; // const-string v4, ", uid="
(( java.lang.StringBuilder ) v3 ).append ( v4 ); // invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v3 ).append ( v0 ); // invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;
final String v4 = "callingPkg:"; // const-string v4, "callingPkg:"
(( java.lang.StringBuilder ) v3 ).append ( v4 ); // invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
/* .line 64 */
/* if-nez v2, :cond_0 */
final String v4 = ""; // const-string v4, ""
} // :cond_0
/* move-object v4, v2 */
} // :goto_0
(( java.lang.StringBuilder ) v3 ).append ( v4 ); // invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v3 ).toString ( ); // invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
/* .line 65 */
/* .local v3, "msg":Ljava/lang/String; */
final String v4 = "GreenKidManagerService"; // const-string v4, "GreenKidManagerService"
android.util.Slog .w ( v4,v3 );
/* .line 66 */
/* new-instance v4, Ljava/lang/SecurityException; */
/* invoke-direct {v4, v3}, Ljava/lang/SecurityException;-><init>(Ljava/lang/String;)V */
/* throw v4 */
/* .line 68 */
} // .end local v3 # "msg":Ljava/lang/String;
} // :cond_1
com.miui.server.GreenGuardManagerService .startWatchGreenGuardProcessInner ( p0 );
/* .line 69 */
return;
} // .end method
private static synchronized void startWatchGreenGuardProcessInner ( android.content.Context p0 ) {
/* .locals 4 */
/* .param p0, "context" # Landroid/content/Context; */
/* const-class v0, Lcom/miui/server/GreenGuardManagerService; */
/* monitor-enter v0 */
/* .line 72 */
try { // :try_start_0
final String v1 = "GreenKidManagerService"; // const-string v1, "GreenKidManagerService"
/* const-string/jumbo v2, "startWatchGreenGuardProcess" */
android.util.Log .d ( v1,v2 );
/* .line 73 */
/* new-instance v1, Landroid/content/Intent; */
/* invoke-direct {v1}, Landroid/content/Intent;-><init>()V */
/* .line 74 */
/* .local v1, "intent":Landroid/content/Intent; */
final String v2 = "com.miui.greenguard"; // const-string v2, "com.miui.greenguard"
final String v3 = "com.miui.greenguard.service.GreenKidService"; // const-string v3, "com.miui.greenguard.service.GreenKidService"
(( android.content.Intent ) v1 ).setClassName ( v2, v3 ); // invoke-virtual {v1, v2, v3}, Landroid/content/Intent;->setClassName(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;
/* .line 75 */
v2 = com.miui.server.GreenGuardManagerService.mGreenGuardServiceConnection;
int v3 = 1; // const/4 v3, 0x1
(( android.content.Context ) p0 ).bindService ( v1, v2, v3 ); // invoke-virtual {p0, v1, v2, v3}, Landroid/content/Context;->bindService(Landroid/content/Intent;Landroid/content/ServiceConnection;I)Z
/* :try_end_0 */
/* .catchall {:try_start_0 ..:try_end_0} :catchall_0 */
/* .line 76 */
/* monitor-exit v0 */
return;
/* .line 71 */
} // .end local v1 # "intent":Landroid/content/Intent;
} // .end local p0 # "context":Landroid/content/Context;
/* :catchall_0 */
/* move-exception p0 */
/* monitor-exit v0 */
/* throw p0 */
} // .end method
