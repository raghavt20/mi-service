public abstract class com.miui.server.ISplashPackageCheckListener$Stub extends android.os.Binder implements com.miui.server.ISplashPackageCheckListener {
	 /* .source "ISplashPackageCheckListener.java" */
	 /* # interfaces */
	 /* # annotations */
	 /* .annotation system Ldalvik/annotation/EnclosingClass; */
	 /* value = Lcom/miui/server/ISplashPackageCheckListener; */
} // .end annotation
/* .annotation system Ldalvik/annotation/InnerClass; */
/* accessFlags = 0x409 */
/* name = "Stub" */
} // .end annotation
/* .annotation system Ldalvik/annotation/MemberClasses; */
/* value = { */
/* Lcom/miui/server/ISplashPackageCheckListener$Stub$Proxy; */
/* } */
} // .end annotation
/* # static fields */
private static final java.lang.String DESCRIPTOR;
static final Integer TRANSACTION_updateSplashPackageCheckInfo;
static final Integer TRANSACTION_updateSplashPackageCheckInfoList;
/* # direct methods */
public com.miui.server.ISplashPackageCheckListener$Stub ( ) {
/* .locals 1 */
/* .line 14 */
/* invoke-direct {p0}, Landroid/os/Binder;-><init>()V */
/* .line 15 */
final String v0 = "com.miui.server.ISplashPackageCheckListener"; // const-string v0, "com.miui.server.ISplashPackageCheckListener"
(( com.miui.server.ISplashPackageCheckListener$Stub ) p0 ).attachInterface ( p0, v0 ); // invoke-virtual {p0, p0, v0}, Lcom/miui/server/ISplashPackageCheckListener$Stub;->attachInterface(Landroid/os/IInterface;Ljava/lang/String;)V
/* .line 16 */
return;
} // .end method
public static com.miui.server.ISplashPackageCheckListener asInterface ( android.os.IBinder p0 ) {
/* .locals 2 */
/* .param p0, "obj" # Landroid/os/IBinder; */
/* .line 23 */
/* if-nez p0, :cond_0 */
/* .line 24 */
int v0 = 0; // const/4 v0, 0x0
/* .line 26 */
} // :cond_0
final String v0 = "com.miui.server.ISplashPackageCheckListener"; // const-string v0, "com.miui.server.ISplashPackageCheckListener"
/* .line 27 */
/* .local v0, "iin":Landroid/os/IInterface; */
if ( v0 != null) { // if-eqz v0, :cond_1
/* instance-of v1, v0, Lcom/miui/server/ISplashPackageCheckListener; */
if ( v1 != null) { // if-eqz v1, :cond_1
/* .line 28 */
/* move-object v1, v0 */
/* check-cast v1, Lcom/miui/server/ISplashPackageCheckListener; */
/* .line 30 */
} // :cond_1
/* new-instance v1, Lcom/miui/server/ISplashPackageCheckListener$Stub$Proxy; */
/* invoke-direct {v1, p0}, Lcom/miui/server/ISplashPackageCheckListener$Stub$Proxy;-><init>(Landroid/os/IBinder;)V */
} // .end method
/* # virtual methods */
public android.os.IBinder asBinder ( ) {
/* .locals 0 */
/* .line 34 */
} // .end method
public Boolean onTransact ( Integer p0, android.os.Parcel p1, android.os.Parcel p2, Integer p3 ) {
/* .locals 2 */
/* .param p1, "code" # I */
/* .param p2, "data" # Landroid/os/Parcel; */
/* .param p3, "reply" # Landroid/os/Parcel; */
/* .param p4, "flags" # I */
/* .annotation system Ldalvik/annotation/Throws; */
/* value = { */
/* Landroid/os/RemoteException; */
/* } */
} // .end annotation
/* .line 38 */
int v0 = 1; // const/4 v0, 0x1
final String v1 = "com.miui.server.ISplashPackageCheckListener"; // const-string v1, "com.miui.server.ISplashPackageCheckListener"
/* sparse-switch p1, :sswitch_data_0 */
/* .line 67 */
v0 = /* invoke-super {p0, p1, p2, p3, p4}, Landroid/os/Binder;->onTransact(ILandroid/os/Parcel;Landroid/os/Parcel;I)Z */
/* .line 42 */
/* :sswitch_0 */
(( android.os.Parcel ) p3 ).writeString ( v1 ); // invoke-virtual {p3, v1}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V
/* .line 43 */
/* .line 55 */
/* :sswitch_1 */
(( android.os.Parcel ) p2 ).enforceInterface ( v1 ); // invoke-virtual {p2, v1}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V
/* .line 57 */
v1 = (( android.os.Parcel ) p2 ).readInt ( ); // invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I
if ( v1 != null) { // if-eqz v1, :cond_0
/* .line 58 */
v1 = com.miui.server.SplashPackageCheckInfo.CREATOR;
/* check-cast v1, Lcom/miui/server/SplashPackageCheckInfo; */
/* .local v1, "_arg0":Lcom/miui/server/SplashPackageCheckInfo; */
/* .line 61 */
} // .end local v1 # "_arg0":Lcom/miui/server/SplashPackageCheckInfo;
} // :cond_0
int v1 = 0; // const/4 v1, 0x0
/* .line 63 */
/* .restart local v1 # "_arg0":Lcom/miui/server/SplashPackageCheckInfo; */
} // :goto_0
(( com.miui.server.ISplashPackageCheckListener$Stub ) p0 ).updateSplashPackageCheckInfo ( v1 ); // invoke-virtual {p0, v1}, Lcom/miui/server/ISplashPackageCheckListener$Stub;->updateSplashPackageCheckInfo(Lcom/miui/server/SplashPackageCheckInfo;)V
/* .line 64 */
/* .line 47 */
} // .end local v1 # "_arg0":Lcom/miui/server/SplashPackageCheckInfo;
/* :sswitch_2 */
(( android.os.Parcel ) p2 ).enforceInterface ( v1 ); // invoke-virtual {p2, v1}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V
/* .line 49 */
v1 = com.miui.server.SplashPackageCheckInfo.CREATOR;
(( android.os.Parcel ) p2 ).createTypedArrayList ( v1 ); // invoke-virtual {p2, v1}, Landroid/os/Parcel;->createTypedArrayList(Landroid/os/Parcelable$Creator;)Ljava/util/ArrayList;
/* .line 50 */
/* .local v1, "_arg0":Ljava/util/List;, "Ljava/util/List<Lcom/miui/server/SplashPackageCheckInfo;>;" */
(( com.miui.server.ISplashPackageCheckListener$Stub ) p0 ).updateSplashPackageCheckInfoList ( v1 ); // invoke-virtual {p0, v1}, Lcom/miui/server/ISplashPackageCheckListener$Stub;->updateSplashPackageCheckInfoList(Ljava/util/List;)V
/* .line 51 */
/* nop */
/* :sswitch_data_0 */
/* .sparse-switch */
/* 0x1 -> :sswitch_2 */
/* 0x2 -> :sswitch_1 */
/* 0x5f4e5446 -> :sswitch_0 */
} // .end sparse-switch
} // .end method
