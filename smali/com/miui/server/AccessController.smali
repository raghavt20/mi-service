.class public Lcom/miui/server/AccessController;
.super Ljava/lang/Object;
.source "AccessController.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/miui/server/AccessController$WorkHandler;
    }
.end annotation


# static fields
.field private static final ACCESS_CONTROL:Ljava/lang/String; = "access_control.key"

.field private static final ACCESS_CONTROL_KEYSTORE:Ljava/lang/String; = "access_control_keystore.key"

.field private static final ACCESS_CONTROL_PASSWORD_TYPE_KEY:Ljava/lang/String; = "access_control_password_type.key"

.field private static final APPLOCK_WHILTE:Ljava/lang/String; = "applock_whilte"

.field public static final APP_LOCK_CLASSNAME:Ljava/lang/String; = "com.miui.securitycenter/com.miui.applicationlock.ConfirmAccessControl"

.field public static final DEBUG:Z = false

.field private static final GAMEBOOSTER_ANTIMSG:Ljava/lang/String; = "gamebooster_antimsg"

.field private static final GAMEBOOSTER_PAY:Ljava/lang/String; = "gamebooster_pay"

.field public static final PACKAGE_CAMERA:Ljava/lang/String; = "com.android.camera"

.field public static final PACKAGE_GALLERY:Ljava/lang/String; = "com.miui.gallery"

.field public static final PACKAGE_MEITU_CAMERA:Ljava/lang/String; = "com.mlab.cam"

.field public static final PACKAGE_SYSTEMUI:Ljava/lang/String; = "com.android.systemui"

.field private static final PASSWORD_TYPE_PATTERN:Ljava/lang/String; = "pattern"

.field public static final SKIP_INTERCEPT_ACTIVITY_GALLERY_EDIT:Ljava/lang/String; = "com.miui.gallery.editor.photo.screen.home.ScreenEditorActivity"

.field public static final SKIP_INTERCEPT_ACTIVITY_GALLERY_EXTRA:Ljava/lang/String; = "com.miui.gallery.activity.ExternalPhotoPageActivity"

.field public static final SKIP_INTERCEPT_ACTIVITY_GALLERY_EXTRA_TRANSLUCENT:Ljava/lang/String; = "com.miui.gallery.activity.TranslucentExternalPhotoPageActivity"

.field private static final SYSTEM_DIRECTORY:Ljava/lang/String; = "/system/"

.field private static final TAG:Ljava/lang/String; = "AccessController"

.field private static final UPDATE_EVERY_DELAY:J = 0x2932e00L

.field private static final UPDATE_FIRT_DELAY:J = 0x2bf20L

.field private static final UPDATE_WHITE_LIST:I = 0x1

.field private static final WECHAT_VIDEO_ACTIVITY_CLASSNAME:Ljava/lang/String; = "com.tencent.mm.plugin.voip.ui.VideoActivity"

.field private static mAntimsgInterceptList:Landroid/util/ArrayMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/util/ArrayMap<",
            "Ljava/lang/String;",
            "Ljava/util/ArrayList<",
            "Landroid/content/Intent;",
            ">;>;"
        }
    .end annotation
.end field

.field private static mForceLaunchList:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private static mPasswordToHash:Ljava/lang/reflect/Method;

.field private static mPayInterceptList:Landroid/util/ArrayMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/util/ArrayMap<",
            "Ljava/lang/String;",
            "Ljava/util/ArrayList<",
            "Landroid/content/Intent;",
            ">;>;"
        }
    .end annotation
.end field

.field private static mSkipList:Landroid/util/ArrayMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/util/ArrayMap<",
            "Ljava/lang/String;",
            "Ljava/util/ArrayList<",
            "Landroid/content/Intent;",
            ">;>;"
        }
    .end annotation
.end field


# instance fields
.field private mContext:Landroid/content/Context;

.field private final mFileWriteLock:Ljava/lang/Object;

.field private final mKeystoreHelper:Landroid/security/AccessControlKeystoreHelper;

.field private mLockPatternUtils:Lcom/android/internal/widget/LockPatternUtils;

.field private mWorkHandler:Lcom/miui/server/AccessController$WorkHandler;


# direct methods
.method static constructor <clinit>()V
    .locals 13

    .line 87
    const-string v0, "AccessController"

    new-instance v1, Landroid/util/ArrayMap;

    invoke-direct {v1}, Landroid/util/ArrayMap;-><init>()V

    sput-object v1, Lcom/miui/server/AccessController;->mSkipList:Landroid/util/ArrayMap;

    .line 89
    new-instance v1, Landroid/util/ArrayMap;

    invoke-direct {v1}, Landroid/util/ArrayMap;-><init>()V

    sput-object v1, Lcom/miui/server/AccessController;->mAntimsgInterceptList:Landroid/util/ArrayMap;

    .line 92
    new-instance v1, Landroid/util/ArrayMap;

    invoke-direct {v1}, Landroid/util/ArrayMap;-><init>()V

    sput-object v1, Lcom/miui/server/AccessController;->mPayInterceptList:Landroid/util/ArrayMap;

    .line 99
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    sput-object v1, Lcom/miui/server/AccessController;->mForceLaunchList:Ljava/util/List;

    .line 109
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    .line 110
    .local v1, "passList":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Landroid/util/Pair<Ljava/lang/String;Ljava/lang/String;>;>;"
    new-instance v2, Landroid/util/Pair;

    const-string v3, "com.tencent.av.ui.VideoInviteLock"

    const-string v4, "com.tencent.mobileqq"

    invoke-direct {v2, v4, v3}, Landroid/util/Pair;-><init>(Ljava/lang/Object;Ljava/lang/Object;)V

    invoke-virtual {v1, v2}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 111
    new-instance v2, Landroid/util/Pair;

    const-string v3, "com.tencent.av.ui.VideoInviteFull"

    invoke-direct {v2, v4, v3}, Landroid/util/Pair;-><init>(Ljava/lang/Object;Ljava/lang/Object;)V

    invoke-virtual {v1, v2}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 112
    new-instance v2, Landroid/util/Pair;

    const-string v3, "com.tencent.av.ui.VideoInviteActivity"

    invoke-direct {v2, v4, v3}, Landroid/util/Pair;-><init>(Ljava/lang/Object;Ljava/lang/Object;)V

    invoke-virtual {v1, v2}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 113
    new-instance v2, Landroid/util/Pair;

    const-string v3, "com.tencent.mm.plugin.voip.ui.VideoActivity"

    const-string v5, "com.tencent.mm"

    invoke-direct {v2, v5, v3}, Landroid/util/Pair;-><init>(Ljava/lang/Object;Ljava/lang/Object;)V

    invoke-virtual {v1, v2}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 114
    new-instance v2, Landroid/util/Pair;

    const-string v3, "com.tencent.mm.plugin.multitalk.ui.MultiTalkMainUI"

    invoke-direct {v2, v5, v3}, Landroid/util/Pair;-><init>(Ljava/lang/Object;Ljava/lang/Object;)V

    invoke-virtual {v1, v2}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 115
    new-instance v2, Landroid/util/Pair;

    const-string v3, "com.tencent.mm.plugin.base.stub.UIEntryStub"

    invoke-direct {v2, v5, v3}, Landroid/util/Pair;-><init>(Ljava/lang/Object;Ljava/lang/Object;)V

    invoke-virtual {v1, v2}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 116
    new-instance v2, Landroid/util/Pair;

    const-string v3, "com.tencent.mm.plugin.webview.ui.tools.SDKOAuthUI"

    invoke-direct {v2, v5, v3}, Landroid/util/Pair;-><init>(Ljava/lang/Object;Ljava/lang/Object;)V

    invoke-virtual {v1, v2}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 117
    new-instance v2, Landroid/util/Pair;

    const-string v3, "com.tencent.mm.plugin.base.stub.WXPayEntryActivity"

    invoke-direct {v2, v5, v3}, Landroid/util/Pair;-><init>(Ljava/lang/Object;Ljava/lang/Object;)V

    invoke-virtual {v1, v2}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 118
    new-instance v2, Landroid/util/Pair;

    const-string v6, "com.tencent.mm.plugin.wallet_index.ui.OrderHandlerUI"

    invoke-direct {v2, v5, v6}, Landroid/util/Pair;-><init>(Ljava/lang/Object;Ljava/lang/Object;)V

    invoke-virtual {v1, v2}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 119
    new-instance v2, Landroid/util/Pair;

    const-string v6, "com.whatsapp.VoipActivity"

    const-string v7, "com.whatsapp"

    invoke-direct {v2, v7, v6}, Landroid/util/Pair;-><init>(Ljava/lang/Object;Ljava/lang/Object;)V

    invoke-virtual {v1, v2}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 120
    new-instance v2, Landroid/util/Pair;

    const-string v6, "com.whatsapp.voipcalling.VoipActivityV2"

    invoke-direct {v2, v7, v6}, Landroid/util/Pair;-><init>(Ljava/lang/Object;Ljava/lang/Object;)V

    invoke-virtual {v1, v2}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 121
    new-instance v2, Landroid/util/Pair;

    const-string v6, "jp.naver.line.android"

    const-string v7, "jp.naver.line.android.freecall.FreeCallActivity"

    invoke-direct {v2, v6, v7}, Landroid/util/Pair;-><init>(Ljava/lang/Object;Ljava/lang/Object;)V

    invoke-virtual {v1, v2}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 123
    new-instance v2, Landroid/util/Pair;

    const-string v6, "com.bbm"

    const-string v7, "com.bbm.ui.voice.activities.IncomingCallActivity"

    invoke-direct {v2, v6, v7}, Landroid/util/Pair;-><init>(Ljava/lang/Object;Ljava/lang/Object;)V

    invoke-virtual {v1, v2}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 124
    new-instance v2, Landroid/util/Pair;

    const-string v6, "com.xiaomi.channel"

    const-string v7, "com.xiaomi.channel.voip.VoipCallActivity"

    invoke-direct {v2, v6, v7}, Landroid/util/Pair;-><init>(Ljava/lang/Object;Ljava/lang/Object;)V

    invoke-virtual {v1, v2}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 125
    new-instance v2, Landroid/util/Pair;

    const-string v6, "com.facebook.orca"

    const-string v7, "com.facebook.rtc.activities.WebrtcIncallActivity"

    invoke-direct {v2, v6, v7}, Landroid/util/Pair;-><init>(Ljava/lang/Object;Ljava/lang/Object;)V

    invoke-virtual {v1, v2}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 126
    new-instance v2, Landroid/util/Pair;

    const-string v6, "com.bsb.hike"

    const-string v7, "com.bsb.hike.voip.view.VoIPActivity"

    invoke-direct {v2, v6, v7}, Landroid/util/Pair;-><init>(Ljava/lang/Object;Ljava/lang/Object;)V

    invoke-virtual {v1, v2}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 127
    new-instance v2, Landroid/util/Pair;

    const-string v6, "com.alipay.android.app.TransProcessPayActivity"

    const-string v7, "com.eg.android.AlipayGphone"

    invoke-direct {v2, v7, v6}, Landroid/util/Pair;-><init>(Ljava/lang/Object;Ljava/lang/Object;)V

    invoke-virtual {v1, v2}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 129
    new-instance v2, Landroid/util/Pair;

    const-string v6, "com.alipay.mobile.security.login.ui.AlipayUserLoginActivity"

    invoke-direct {v2, v7, v6}, Landroid/util/Pair;-><init>(Ljava/lang/Object;Ljava/lang/Object;)V

    invoke-virtual {v1, v2}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 131
    new-instance v2, Landroid/util/Pair;

    const-string v6, "com.alipay.mobile.bill.detail.ui.EmptyActivity_"

    invoke-direct {v2, v7, v6}, Landroid/util/Pair;-><init>(Ljava/lang/Object;Ljava/lang/Object;)V

    invoke-virtual {v1, v2}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 133
    new-instance v2, Landroid/util/Pair;

    const-string v6, "com.xiaomi.smarthome"

    const-string v8, "com.xiaomi.smarthome.miio.activity.ClientAllLockedActivity"

    invoke-direct {v2, v6, v8}, Landroid/util/Pair;-><init>(Ljava/lang/Object;Ljava/lang/Object;)V

    invoke-virtual {v1, v2}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 135
    new-instance v2, Landroid/util/Pair;

    const-string v6, "com.google.android.dialer"

    const-string v8, "com.android.dialer.incall.activity.ui.InCallActivity"

    invoke-direct {v2, v6, v8}, Landroid/util/Pair;-><init>(Ljava/lang/Object;Ljava/lang/Object;)V

    invoke-virtual {v1, v2}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 136
    new-instance v2, Landroid/util/Pair;

    const-string v6, "com.android.settings.FallbackHome"

    const-string v8, "com.android.settings"

    invoke-direct {v2, v8, v6}, Landroid/util/Pair;-><init>(Ljava/lang/Object;Ljava/lang/Object;)V

    invoke-virtual {v1, v2}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 137
    new-instance v2, Landroid/util/Pair;

    const-string v6, "com.android.mms.ui.DummyActivity"

    const-string v9, "com.android.mms"

    invoke-direct {v2, v9, v6}, Landroid/util/Pair;-><init>(Ljava/lang/Object;Ljava/lang/Object;)V

    invoke-virtual {v1, v2}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 138
    new-instance v2, Landroid/util/Pair;

    const-string v6, "com.android.mms.ui.ComposeMessageRouterActivity"

    invoke-direct {v2, v9, v6}, Landroid/util/Pair;-><init>(Ljava/lang/Object;Ljava/lang/Object;)V

    invoke-virtual {v1, v2}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 139
    new-instance v2, Landroid/util/Pair;

    const-string v6, "com.xiaomi.jr"

    const-string v9, "com.xiaomi.jr.EntryActivity"

    invoke-direct {v2, v6, v9}, Landroid/util/Pair;-><init>(Ljava/lang/Object;Ljava/lang/Object;)V

    invoke-virtual {v1, v2}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 140
    new-instance v2, Landroid/util/Pair;

    const-string v6, "com.android.settings.MiuiConfirmCommonPassword"

    invoke-direct {v2, v8, v6}, Landroid/util/Pair;-><init>(Ljava/lang/Object;Ljava/lang/Object;)V

    invoke-virtual {v1, v2}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 142
    invoke-virtual {v1}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v6

    const/4 v8, 0x1

    if-eqz v6, :cond_1

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v6

    check-cast v6, Landroid/util/Pair;

    .line 143
    .local v6, "pair":Landroid/util/Pair;, "Landroid/util/Pair<Ljava/lang/String;Ljava/lang/String;>;"
    sget-object v9, Lcom/miui/server/AccessController;->mSkipList:Landroid/util/ArrayMap;

    iget-object v10, v6, Landroid/util/Pair;->first:Ljava/lang/Object;

    invoke-virtual {v9, v10}, Landroid/util/ArrayMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v9

    check-cast v9, Ljava/util/ArrayList;

    .line 144
    .local v9, "intents":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Landroid/content/Intent;>;"
    if-nez v9, :cond_0

    .line 145
    new-instance v10, Ljava/util/ArrayList;

    invoke-direct {v10, v8}, Ljava/util/ArrayList;-><init>(I)V

    move-object v9, v10

    .line 146
    sget-object v8, Lcom/miui/server/AccessController;->mSkipList:Landroid/util/ArrayMap;

    iget-object v10, v6, Landroid/util/Pair;->first:Ljava/lang/Object;

    check-cast v10, Ljava/lang/String;

    invoke-virtual {v8, v10, v9}, Landroid/util/ArrayMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 148
    :cond_0
    new-instance v8, Landroid/content/Intent;

    invoke-direct {v8}, Landroid/content/Intent;-><init>()V

    .line 149
    .local v8, "intent":Landroid/content/Intent;
    new-instance v10, Landroid/content/ComponentName;

    iget-object v11, v6, Landroid/util/Pair;->first:Ljava/lang/Object;

    check-cast v11, Ljava/lang/String;

    iget-object v12, v6, Landroid/util/Pair;->second:Ljava/lang/Object;

    check-cast v12, Ljava/lang/String;

    invoke-direct {v10, v11, v12}, Landroid/content/ComponentName;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {v8, v10}, Landroid/content/Intent;->setComponent(Landroid/content/ComponentName;)Landroid/content/Intent;

    .line 150
    invoke-virtual {v9, v8}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 151
    .end local v6    # "pair":Landroid/util/Pair;, "Landroid/util/Pair<Ljava/lang/String;Ljava/lang/String;>;"
    .end local v8    # "intent":Landroid/content/Intent;
    .end local v9    # "intents":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Landroid/content/Intent;>;"
    goto :goto_0

    .line 154
    :cond_1
    new-instance v2, Ljava/util/ArrayList;

    invoke-direct {v2}, Ljava/util/ArrayList;-><init>()V

    .line 155
    .local v2, "payList":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Landroid/util/Pair<Ljava/lang/String;Ljava/lang/String;>;>;"
    new-instance v6, Landroid/util/Pair;

    invoke-direct {v6, v5, v3}, Landroid/util/Pair;-><init>(Ljava/lang/Object;Ljava/lang/Object;)V

    invoke-virtual {v2, v6}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 156
    new-instance v3, Landroid/util/Pair;

    const-string v6, "com.tencent.mm.plugin.base.stub.WXEntryActivity"

    invoke-direct {v3, v5, v6}, Landroid/util/Pair;-><init>(Ljava/lang/Object;Ljava/lang/Object;)V

    invoke-virtual {v2, v3}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 157
    new-instance v3, Landroid/util/Pair;

    const-string v6, "com.tencent.mm.plugin.base.stub.WXCustomSchemeEntryActivity"

    invoke-direct {v3, v5, v6}, Landroid/util/Pair;-><init>(Ljava/lang/Object;Ljava/lang/Object;)V

    invoke-virtual {v2, v3}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 158
    new-instance v3, Landroid/util/Pair;

    const-string v5, "cooperation.qwallet.open.AppPayActivity"

    invoke-direct {v3, v4, v5}, Landroid/util/Pair;-><init>(Ljava/lang/Object;Ljava/lang/Object;)V

    invoke-virtual {v2, v3}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 159
    new-instance v3, Landroid/util/Pair;

    const-string v5, "com.tencent.open.agent.AgentActivity"

    invoke-direct {v3, v4, v5}, Landroid/util/Pair;-><init>(Ljava/lang/Object;Ljava/lang/Object;)V

    invoke-virtual {v2, v3}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 160
    new-instance v3, Landroid/util/Pair;

    const-string v5, "com.tencent.mobileqq.activity.JumpActivity"

    invoke-direct {v3, v4, v5}, Landroid/util/Pair;-><init>(Ljava/lang/Object;Ljava/lang/Object;)V

    invoke-virtual {v2, v3}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 161
    new-instance v3, Landroid/util/Pair;

    const-string v5, "com.sina.weibo"

    const-string v6, "com.sina.weibo.composerinde.ComposerDispatchActivity"

    invoke-direct {v3, v5, v6}, Landroid/util/Pair;-><init>(Ljava/lang/Object;Ljava/lang/Object;)V

    invoke-virtual {v2, v3}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 162
    new-instance v3, Landroid/util/Pair;

    const-string v5, "com.alipay.mobile.quinox.SchemeLauncherActivity"

    invoke-direct {v3, v7, v5}, Landroid/util/Pair;-><init>(Ljava/lang/Object;Ljava/lang/Object;)V

    invoke-virtual {v2, v3}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 164
    new-instance v3, Landroid/util/Pair;

    const-string v5, "com.ali.user.mobile.loginupgrade.activity.GuideActivity"

    invoke-direct {v3, v7, v5}, Landroid/util/Pair;-><init>(Ljava/lang/Object;Ljava/lang/Object;)V

    invoke-virtual {v2, v3}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 165
    new-instance v3, Landroid/util/Pair;

    const-string v5, "com.ali.user.mobile.loginupgrade.activity.LoginActivity"

    invoke-direct {v3, v7, v5}, Landroid/util/Pair;-><init>(Ljava/lang/Object;Ljava/lang/Object;)V

    invoke-virtual {v2, v3}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 166
    new-instance v3, Landroid/util/Pair;

    const-string v5, "com.alipay.mobile.security.login.ui.RecommandAlipayUserLoginActivity"

    invoke-direct {v3, v7, v5}, Landroid/util/Pair;-><init>(Ljava/lang/Object;Ljava/lang/Object;)V

    invoke-virtual {v2, v3}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 168
    new-instance v3, Landroid/util/Pair;

    const-string v5, "com.tencent.mobileqq.activity.LoginActivity"

    invoke-direct {v3, v4, v5}, Landroid/util/Pair;-><init>(Ljava/lang/Object;Ljava/lang/Object;)V

    invoke-virtual {v2, v3}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 170
    invoke-virtual {v2}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :goto_1
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v4

    if-eqz v4, :cond_3

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Landroid/util/Pair;

    .line 171
    .local v4, "pair":Landroid/util/Pair;, "Landroid/util/Pair<Ljava/lang/String;Ljava/lang/String;>;"
    sget-object v5, Lcom/miui/server/AccessController;->mPayInterceptList:Landroid/util/ArrayMap;

    iget-object v6, v4, Landroid/util/Pair;->first:Ljava/lang/Object;

    invoke-virtual {v5, v6}, Landroid/util/ArrayMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Ljava/util/ArrayList;

    .line 172
    .local v5, "intents":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Landroid/content/Intent;>;"
    if-nez v5, :cond_2

    .line 173
    new-instance v6, Ljava/util/ArrayList;

    invoke-direct {v6, v8}, Ljava/util/ArrayList;-><init>(I)V

    move-object v5, v6

    .line 174
    sget-object v6, Lcom/miui/server/AccessController;->mPayInterceptList:Landroid/util/ArrayMap;

    iget-object v7, v4, Landroid/util/Pair;->first:Ljava/lang/Object;

    check-cast v7, Ljava/lang/String;

    invoke-virtual {v6, v7, v5}, Landroid/util/ArrayMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 176
    :cond_2
    new-instance v6, Landroid/content/Intent;

    invoke-direct {v6}, Landroid/content/Intent;-><init>()V

    .line 177
    .local v6, "intent":Landroid/content/Intent;
    new-instance v7, Landroid/content/ComponentName;

    iget-object v9, v4, Landroid/util/Pair;->first:Ljava/lang/Object;

    check-cast v9, Ljava/lang/String;

    iget-object v10, v4, Landroid/util/Pair;->second:Ljava/lang/Object;

    check-cast v10, Ljava/lang/String;

    invoke-direct {v7, v9, v10}, Landroid/content/ComponentName;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {v6, v7}, Landroid/content/Intent;->setComponent(Landroid/content/ComponentName;)Landroid/content/Intent;

    .line 178
    invoke-virtual {v5, v6}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 179
    .end local v4    # "pair":Landroid/util/Pair;, "Landroid/util/Pair<Ljava/lang/String;Ljava/lang/String;>;"
    .end local v5    # "intents":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Landroid/content/Intent;>;"
    .end local v6    # "intent":Landroid/content/Intent;
    goto :goto_1

    .line 182
    :cond_3
    nop

    .line 183
    :try_start_0
    const-string v3, "Nowhere to call this method, passwordToHash should not init"

    invoke-static {v0, v3}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 196
    goto :goto_2

    .line 194
    :catch_0
    move-exception v3

    .line 195
    .local v3, "e":Ljava/lang/Exception;
    const-string v4, " passwordToHash static invoke error"

    invoke-static {v0, v4, v3}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    .line 198
    .end local v3    # "e":Ljava/lang/Exception;
    :goto_2
    sget-object v0, Lcom/miui/server/AccessController;->mForceLaunchList:Ljava/util/List;

    const-string v3, "com.tencent.mobileqq/.activity.JumpActivity"

    invoke-interface {v0, v3}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 199
    sget-object v0, Lcom/miui/server/AccessController;->mForceLaunchList:Ljava/util/List;

    const-string v3, "com.tencent.mm/.plugin.base.stub.WXCustomSchemeEntryActivity"

    invoke-interface {v0, v3}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 200
    sget-object v0, Lcom/miui/server/AccessController;->mForceLaunchList:Ljava/util/List;

    const-string v3, "com.sina.weibo/.composerinde.ComposerDispatchActivity"

    invoke-interface {v0, v3}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 201
    sget-object v0, Lcom/miui/server/AccessController;->mForceLaunchList:Ljava/util/List;

    const-string v3, "com.tencent.mobileqq/com.tencent.open.agent.AgentActivity"

    invoke-interface {v0, v3}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 202
    sget-object v0, Lcom/miui/server/AccessController;->mForceLaunchList:Ljava/util/List;

    const-string v3, "com.eg.android.AlipayGphone/com.alipay.android.msp.ui.views.MspContainerActivity"

    invoke-interface {v0, v3}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 203
    .end local v1    # "passList":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Landroid/util/Pair<Ljava/lang/String;Ljava/lang/String;>;>;"
    .end local v2    # "payList":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Landroid/util/Pair<Ljava/lang/String;Ljava/lang/String;>;>;"
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/os/Looper;)V
    .locals 4
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "looper"    # Landroid/os/Looper;

    .line 220
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 85
    new-instance v0, Ljava/lang/Object;

    invoke-direct {v0}, Ljava/lang/Object;-><init>()V

    iput-object v0, p0, Lcom/miui/server/AccessController;->mFileWriteLock:Ljava/lang/Object;

    .line 221
    iput-object p1, p0, Lcom/miui/server/AccessController;->mContext:Landroid/content/Context;

    .line 222
    new-instance v0, Lcom/miui/server/AccessController$WorkHandler;

    invoke-direct {v0, p0, p2}, Lcom/miui/server/AccessController$WorkHandler;-><init>(Lcom/miui/server/AccessController;Landroid/os/Looper;)V

    iput-object v0, p0, Lcom/miui/server/AccessController;->mWorkHandler:Lcom/miui/server/AccessController$WorkHandler;

    .line 223
    const/4 v1, 0x1

    const-wide/32 v2, 0x2bf20

    invoke-virtual {v0, v1, v2, v3}, Lcom/miui/server/AccessController$WorkHandler;->sendEmptyMessageDelayed(IJ)Z

    .line 224
    new-instance v0, Lcom/android/internal/widget/LockPatternUtils;

    invoke-direct {v0, p1}, Lcom/android/internal/widget/LockPatternUtils;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Lcom/miui/server/AccessController;->mLockPatternUtils:Lcom/android/internal/widget/LockPatternUtils;

    .line 225
    new-instance v0, Landroid/security/AccessControlKeystoreHelper;

    invoke-direct {v0}, Landroid/security/AccessControlKeystoreHelper;-><init>()V

    iput-object v0, p0, Lcom/miui/server/AccessController;->mKeystoreHelper:Landroid/security/AccessControlKeystoreHelper;

    .line 226
    return-void
.end method

.method private checkAccessControlPasswordByKeystore(Ljava/lang/String;I)Z
    .locals 5
    .param p1, "password"    # Ljava/lang/String;
    .param p2, "userId"    # I

    .line 373
    iget-object v0, p0, Lcom/miui/server/AccessController;->mKeystoreHelper:Landroid/security/AccessControlKeystoreHelper;

    iget-object v1, p0, Lcom/miui/server/AccessController;->mContext:Landroid/content/Context;

    invoke-virtual {v0, v1}, Landroid/security/AccessControlKeystoreHelper;->disableEncryptByKeystore(Landroid/content/Context;)Z

    move-result v0

    const/4 v1, 0x0

    if-eqz v0, :cond_0

    .line 374
    return v1

    .line 376
    :cond_0
    const-string v0, "access_control_keystore.key"

    invoke-direct {p0, p2, v0}, Lcom/miui/server/AccessController;->getFilePathForUser(ILjava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 377
    .local v0, "filePath":Ljava/lang/String;
    invoke-direct {p0, v0}, Lcom/miui/server/AccessController;->readFile(Ljava/lang/String;)[B

    move-result-object v2

    .line 378
    .local v2, "readFile":[B
    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "checkAccessControlPasswordByKeystore, "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    const-string v4, "AccessController"

    invoke-static {v4, v3}, Landroid/util/Slog;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 379
    if-eqz v2, :cond_2

    array-length v3, v2

    if-nez v3, :cond_1

    goto :goto_0

    .line 383
    :cond_1
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "alias_app_lock_"

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    .line 384
    .local v1, "alias":Ljava/lang/String;
    iget-object v3, p0, Lcom/miui/server/AccessController;->mKeystoreHelper:Landroid/security/AccessControlKeystoreHelper;

    invoke-virtual {v3, v1, p1}, Landroid/security/AccessControlKeystoreHelper;->encrypt(Ljava/lang/String;Ljava/lang/String;)[B

    move-result-object v3

    .line 385
    .local v3, "cipherText":[B
    invoke-static {v3, v2}, Ljava/util/Arrays;->equals([B[B)Z

    move-result v4

    return v4

    .line 380
    .end local v1    # "alias":Ljava/lang/String;
    .end local v3    # "cipherText":[B
    :cond_2
    :goto_0
    const-string v3, "readFile error!"

    invoke-static {v4, v3}, Landroid/util/Slog;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 381
    return v1
.end method

.method private checkAccessControlPattern(Ljava/lang/String;I)Z
    .locals 5
    .param p1, "pattern"    # Ljava/lang/String;
    .param p2, "userId"    # I

    .line 415
    if-nez p1, :cond_0

    .line 416
    const/4 v0, 0x0

    return v0

    .line 418
    :cond_0
    invoke-static {p1}, Landroid/security/MiuiLockPatternUtils;->stringToPattern(Ljava/lang/String;)Ljava/util/List;

    move-result-object v0

    .line 419
    .local v0, "stringToPattern":Ljava/util/List;, "Ljava/util/List<Lcom/android/internal/widget/LockPatternView$Cell;>;"
    invoke-static {v0}, Landroid/security/MiuiLockPatternUtils;->patternToHash(Ljava/util/List;)[B

    move-result-object v1

    .line 420
    .local v1, "hash":[B
    const-string v2, "access_control.key"

    invoke-direct {p0, p2, v2}, Lcom/miui/server/AccessController;->getFilePathForUser(ILjava/lang/String;)Ljava/lang/String;

    move-result-object v2

    .line 421
    .local v2, "filePath":Ljava/lang/String;
    invoke-direct {p0, v2}, Lcom/miui/server/AccessController;->readFile(Ljava/lang/String;)[B

    move-result-object v3

    .line 422
    .local v3, "readFile":[B
    invoke-static {v3, v1}, Ljava/util/Arrays;->equals([B[B)Z

    move-result v4

    return v4
.end method

.method private getFilePathForUser(ILjava/lang/String;)Ljava/lang/String;
    .locals 3
    .param p1, "userId"    # I
    .param p2, "fileName"    # Ljava/lang/String;

    .line 590
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    invoke-static {}, Landroid/os/Environment;->getDataDirectory()Ljava/io/File;

    move-result-object v1

    invoke-virtual {v1}, Ljava/io/File;->getAbsolutePath()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "/system/"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    .line 591
    .local v0, "dataSystemDirectory":Ljava/lang/String;
    if-nez p1, :cond_0

    .line 593
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    return-object v1

    .line 595
    :cond_0
    new-instance v1, Ljava/io/File;

    invoke-static {p1}, Landroid/os/Environment;->getUserSystemDirectory(I)Ljava/io/File;

    move-result-object v2

    invoke-direct {v1, v2, p2}, Ljava/io/File;-><init>(Ljava/io/File;Ljava/lang/String;)V

    invoke-virtual {v1}, Ljava/io/File;->getAbsolutePath()Ljava/lang/String;

    move-result-object v1

    return-object v1
.end method

.method private haveAccessControlPasswordType(I)Z
    .locals 7
    .param p1, "userId"    # I

    .line 483
    const-string v0, "access_control_password_type.key"

    invoke-direct {p0, p1, v0}, Lcom/miui/server/AccessController;->getFilePathForUser(ILjava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 484
    .local v0, "filePath":Ljava/lang/String;
    iget-object v1, p0, Lcom/miui/server/AccessController;->mFileWriteLock:Ljava/lang/Object;

    monitor-enter v1

    .line 485
    :try_start_0
    new-instance v2, Ljava/io/File;

    invoke-direct {v2, v0}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 486
    .local v2, "file":Ljava/io/File;
    invoke-virtual {v2}, Ljava/io/File;->exists()Z

    move-result v3

    if-eqz v3, :cond_0

    invoke-virtual {v2}, Ljava/io/File;->length()J

    move-result-wide v3

    const-wide/16 v5, 0x0

    cmp-long v3, v3, v5

    if-lez v3, :cond_0

    const/4 v3, 0x1

    goto :goto_0

    :cond_0
    const/4 v3, 0x0

    :goto_0
    monitor-exit v1

    return v3

    .line 487
    .end local v2    # "file":Ljava/io/File;
    :catchall_0
    move-exception v2

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v2
.end method

.method private haveAccessControlPattern(I)Z
    .locals 7
    .param p1, "userId"    # I

    .line 444
    const-string v0, "access_control.key"

    invoke-direct {p0, p1, v0}, Lcom/miui/server/AccessController;->getFilePathForUser(ILjava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 445
    .local v0, "filePath":Ljava/lang/String;
    iget-object v1, p0, Lcom/miui/server/AccessController;->mFileWriteLock:Ljava/lang/Object;

    monitor-enter v1

    .line 446
    :try_start_0
    new-instance v2, Ljava/io/File;

    invoke-direct {v2, v0}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 447
    .local v2, "file":Ljava/io/File;
    invoke-virtual {v2}, Ljava/io/File;->exists()Z

    move-result v3

    if-eqz v3, :cond_0

    invoke-virtual {v2}, Ljava/io/File;->length()J

    move-result-wide v3

    const-wide/16 v5, 0x0

    cmp-long v3, v3, v5

    if-lez v3, :cond_0

    const/4 v3, 0x1

    goto :goto_0

    :cond_0
    const/4 v3, 0x0

    :goto_0
    monitor-exit v1

    return v3

    .line 448
    .end local v2    # "file":Ljava/io/File;
    :catchall_0
    move-exception v2

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v2
.end method

.method private static isOpenedActivity(Ljava/lang/String;)Z
    .locals 1
    .param p0, "activity"    # Ljava/lang/String;

    .line 351
    const-string v0, "com.miui.gallery.activity.ExternalPhotoPageActivity"

    invoke-virtual {v0, p0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_1

    .line 352
    const-string v0, "com.miui.gallery.activity.TranslucentExternalPhotoPageActivity"

    invoke-virtual {v0, p0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_1

    .line 353
    const-string v0, "com.miui.gallery.editor.photo.screen.home.ScreenEditorActivity"

    invoke-virtual {v0, p0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    goto :goto_1

    :cond_1
    :goto_0
    const/4 v0, 0x1

    .line 351
    :goto_1
    return v0
.end method

.method private static isOpenedPkg(Ljava/lang/String;)Z
    .locals 1
    .param p0, "callingPkg"    # Ljava/lang/String;

    .line 346
    const-string v0, "com.miui.gallery"

    invoke-virtual {v0, p0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_1

    const-string v0, "com.android.systemui"

    invoke-virtual {v0, p0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_1

    .line 347
    const-string v0, "com.android.camera"

    invoke-virtual {v0, p0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_1

    const-string v0, "com.mlab.cam"

    invoke-virtual {v0, p0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    goto :goto_1

    :cond_1
    :goto_0
    const/4 v0, 0x1

    .line 346
    :goto_1
    return v0
.end method

.method private passwordToHash(Ljava/lang/String;I)[B
    .locals 7
    .param p1, "password"    # Ljava/lang/String;
    .param p2, "userId"    # I

    .line 675
    invoke-static {p1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    const/4 v1, 0x0

    if-eqz v0, :cond_0

    .line 676
    return-object v1

    .line 680
    :cond_0
    nop

    .line 681
    :try_start_0
    const-class v0, Lcom/android/internal/widget/LockPatternUtils;

    const-string v2, "getSalt"

    const/4 v3, 0x1

    new-array v4, v3, [Ljava/lang/Class;

    sget-object v5, Ljava/lang/Integer;->TYPE:Ljava/lang/Class;

    const/4 v6, 0x0

    aput-object v5, v4, v6

    invoke-virtual {v0, v2, v4}, Ljava/lang/Class;->getDeclaredMethod(Ljava/lang/String;[Ljava/lang/Class;)Ljava/lang/reflect/Method;

    move-result-object v0

    .line 682
    .local v0, "getSalt":Ljava/lang/reflect/Method;
    invoke-virtual {v0, v3}, Ljava/lang/reflect/Method;->setAccessible(Z)V

    .line 683
    iget-object v2, p0, Lcom/miui/server/AccessController;->mLockPatternUtils:Lcom/android/internal/widget/LockPatternUtils;

    new-array v3, v3, [Ljava/lang/Object;

    invoke-static {p2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    aput-object v4, v3, v6

    invoke-virtual {v0, v2, v3}, Ljava/lang/reflect/Method;->invoke(Ljava/lang/Object;[Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/lang/String;

    .line 684
    .local v2, "salt":Ljava/lang/String;
    invoke-virtual {p1}, Ljava/lang/String;->getBytes()[B

    move-result-object v3

    .line 685
    invoke-virtual {v2}, Ljava/lang/String;->getBytes()[B

    move-result-object v4

    .line 684
    invoke-static {v3, v4}, Lcom/android/internal/widget/LockscreenCredential;->legacyPasswordToHash([B[B)Ljava/lang/String;

    move-result-object v3

    move-object v0, v3

    .line 686
    .end local v2    # "salt":Ljava/lang/String;
    .local v0, "hash":Ljava/lang/Object;
    nop

    .line 691
    if-eqz v0, :cond_1

    .line 692
    nop

    .line 693
    move-object v2, v0

    check-cast v2, Ljava/lang/String;

    sget-object v3, Ljava/nio/charset/StandardCharsets;->UTF_8:Ljava/nio/charset/Charset;

    invoke-virtual {v2, v3}, Ljava/lang/String;->getBytes(Ljava/nio/charset/Charset;)[B

    move-result-object v1
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    return-object v1

    .line 700
    .end local v0    # "hash":Ljava/lang/Object;
    :cond_1
    goto :goto_0

    .line 698
    :catch_0
    move-exception v0

    .line 699
    .local v0, "e":Ljava/lang/Exception;
    const-string v2, "AccessController"

    const-string v3, " passwordToHash invoke error"

    invoke-static {v2, v3, v0}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    .line 701
    .end local v0    # "e":Ljava/lang/Exception;
    :goto_0
    return-object v1
.end method

.method private readFile(Ljava/lang/String;)[B
    .locals 8
    .param p1, "name"    # Ljava/lang/String;

    .line 491
    iget-object v0, p0, Lcom/miui/server/AccessController;->mFileWriteLock:Ljava/lang/Object;

    monitor-enter v0

    .line 492
    const/4 v1, 0x0

    .line 493
    .local v1, "raf":Ljava/io/RandomAccessFile;
    const/4 v2, 0x0

    .line 495
    .local v2, "stored":[B
    :try_start_0
    new-instance v3, Ljava/io/RandomAccessFile;

    const-string v4, "r"

    invoke-direct {v3, p1, v4}, Ljava/io/RandomAccessFile;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    move-object v1, v3

    .line 496
    invoke-virtual {v1}, Ljava/io/RandomAccessFile;->length()J

    move-result-wide v3

    long-to-int v3, v3

    new-array v3, v3, [B

    move-object v2, v3

    .line 497
    array-length v3, v2

    const/4 v4, 0x0

    invoke-virtual {v1, v2, v4, v3}, Ljava/io/RandomAccessFile;->readFully([BII)V

    .line 498
    invoke-virtual {v1}, Ljava/io/RandomAccessFile;->close()V
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_1
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 502
    nop

    .line 504
    :try_start_1
    invoke-virtual {v1}, Ljava/io/RandomAccessFile;->close()V
    :try_end_1
    .catch Ljava/io/IOException; {:try_start_1 .. :try_end_1} :catch_0
    .catchall {:try_start_1 .. :try_end_1} :catchall_1

    .line 507
    :goto_0
    goto :goto_2

    .line 505
    :catch_0
    move-exception v3

    .line 506
    .local v3, "e":Ljava/io/IOException;
    :try_start_2
    const-string v4, "AccessController"

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "Error closing file "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    :goto_1
    invoke-static {v4, v5}, Landroid/util/Slog;->e(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 507
    nop

    .end local v3    # "e":Ljava/io/IOException;
    goto :goto_2

    .line 502
    :catchall_0
    move-exception v3

    goto :goto_3

    .line 499
    :catch_1
    move-exception v3

    .line 500
    .restart local v3    # "e":Ljava/io/IOException;
    :try_start_3
    const-string v4, "AccessController"

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "Cannot read file "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5}, Landroid/util/Slog;->e(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    .line 502
    nop

    .end local v3    # "e":Ljava/io/IOException;
    if-eqz v1, :cond_0

    .line 504
    :try_start_4
    invoke-virtual {v1}, Ljava/io/RandomAccessFile;->close()V
    :try_end_4
    .catch Ljava/io/IOException; {:try_start_4 .. :try_end_4} :catch_2
    .catchall {:try_start_4 .. :try_end_4} :catchall_1

    goto :goto_0

    .line 505
    :catch_2
    move-exception v3

    .line 506
    .restart local v3    # "e":Ljava/io/IOException;
    :try_start_5
    const-string v4, "AccessController"

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "Error closing file "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    goto :goto_1

    .line 510
    .end local v3    # "e":Ljava/io/IOException;
    :cond_0
    :goto_2
    monitor-exit v0
    :try_end_5
    .catchall {:try_start_5 .. :try_end_5} :catchall_1

    return-object v2

    .line 502
    :goto_3
    if-eqz v1, :cond_1

    .line 504
    :try_start_6
    invoke-virtual {v1}, Ljava/io/RandomAccessFile;->close()V
    :try_end_6
    .catch Ljava/io/IOException; {:try_start_6 .. :try_end_6} :catch_3
    .catchall {:try_start_6 .. :try_end_6} :catchall_1

    .line 507
    goto :goto_4

    .line 505
    :catch_3
    move-exception v4

    .line 506
    .local v4, "e":Ljava/io/IOException;
    :try_start_7
    const-string v5, "AccessController"

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string v7, "Error closing file "

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-static {v5, v6}, Landroid/util/Slog;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 509
    .end local v4    # "e":Ljava/io/IOException;
    :cond_1
    :goto_4
    nop

    .end local p0    # "this":Lcom/miui/server/AccessController;
    .end local p1    # "name":Ljava/lang/String;
    throw v3

    .line 511
    .end local v1    # "raf":Ljava/io/RandomAccessFile;
    .end local v2    # "stored":[B
    .restart local p0    # "this":Lcom/miui/server/AccessController;
    .restart local p1    # "name":Ljava/lang/String;
    :catchall_1
    move-exception v1

    monitor-exit v0
    :try_end_7
    .catchall {:try_start_7 .. :try_end_7} :catchall_1

    throw v1
.end method

.method private readTypeFile(Ljava/lang/String;)Ljava/lang/String;
    .locals 8
    .param p1, "name"    # Ljava/lang/String;

    .line 515
    iget-object v0, p0, Lcom/miui/server/AccessController;->mFileWriteLock:Ljava/lang/Object;

    monitor-enter v0

    .line 516
    const/4 v1, 0x0

    .line 517
    .local v1, "raf":Ljava/io/RandomAccessFile;
    const/4 v2, 0x0

    .line 519
    .local v2, "stored":Ljava/lang/String;
    :try_start_0
    new-instance v3, Ljava/io/RandomAccessFile;

    const-string v4, "r"

    invoke-direct {v3, p1, v4}, Ljava/io/RandomAccessFile;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    move-object v1, v3

    .line 520
    invoke-virtual {v1}, Ljava/io/RandomAccessFile;->readLine()Ljava/lang/String;

    move-result-object v3

    move-object v2, v3

    .line 521
    invoke-virtual {v1}, Ljava/io/RandomAccessFile;->close()V
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_1
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 525
    nop

    .line 527
    :try_start_1
    invoke-virtual {v1}, Ljava/io/RandomAccessFile;->close()V
    :try_end_1
    .catch Ljava/io/IOException; {:try_start_1 .. :try_end_1} :catch_0
    .catchall {:try_start_1 .. :try_end_1} :catchall_1

    .line 530
    :goto_0
    goto :goto_2

    .line 528
    :catch_0
    move-exception v3

    .line 529
    .local v3, "e":Ljava/io/IOException;
    :try_start_2
    const-string v4, "AccessController"

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "Error closing file "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    :goto_1
    invoke-static {v4, v5}, Landroid/util/Slog;->e(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 530
    nop

    .end local v3    # "e":Ljava/io/IOException;
    goto :goto_2

    .line 525
    :catchall_0
    move-exception v3

    goto :goto_3

    .line 522
    :catch_1
    move-exception v3

    .line 523
    .restart local v3    # "e":Ljava/io/IOException;
    :try_start_3
    const-string v4, "AccessController"

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "Cannot read file "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5}, Landroid/util/Slog;->e(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    .line 525
    nop

    .end local v3    # "e":Ljava/io/IOException;
    if-eqz v1, :cond_0

    .line 527
    :try_start_4
    invoke-virtual {v1}, Ljava/io/RandomAccessFile;->close()V
    :try_end_4
    .catch Ljava/io/IOException; {:try_start_4 .. :try_end_4} :catch_2
    .catchall {:try_start_4 .. :try_end_4} :catchall_1

    goto :goto_0

    .line 528
    :catch_2
    move-exception v3

    .line 529
    .restart local v3    # "e":Ljava/io/IOException;
    :try_start_5
    const-string v4, "AccessController"

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "Error closing file "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    goto :goto_1

    .line 533
    .end local v3    # "e":Ljava/io/IOException;
    :cond_0
    :goto_2
    monitor-exit v0
    :try_end_5
    .catchall {:try_start_5 .. :try_end_5} :catchall_1

    return-object v2

    .line 525
    :goto_3
    if-eqz v1, :cond_1

    .line 527
    :try_start_6
    invoke-virtual {v1}, Ljava/io/RandomAccessFile;->close()V
    :try_end_6
    .catch Ljava/io/IOException; {:try_start_6 .. :try_end_6} :catch_3
    .catchall {:try_start_6 .. :try_end_6} :catchall_1

    .line 530
    goto :goto_4

    .line 528
    :catch_3
    move-exception v4

    .line 529
    .local v4, "e":Ljava/io/IOException;
    :try_start_7
    const-string v5, "AccessController"

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string v7, "Error closing file "

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-static {v5, v6}, Landroid/util/Slog;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 532
    .end local v4    # "e":Ljava/io/IOException;
    :cond_1
    :goto_4
    nop

    .end local p0    # "this":Lcom/miui/server/AccessController;
    .end local p1    # "name":Ljava/lang/String;
    throw v3

    .line 534
    .end local v1    # "raf":Ljava/io/RandomAccessFile;
    .end local v2    # "stored":Ljava/lang/String;
    .restart local p0    # "this":Lcom/miui/server/AccessController;
    .restart local p1    # "name":Ljava/lang/String;
    :catchall_1
    move-exception v1

    monitor-exit v0
    :try_end_7
    .catchall {:try_start_7 .. :try_end_7} :catchall_1

    throw v1
.end method

.method private saveAccessControlPasswordByKeystore(Ljava/lang/String;I)V
    .locals 3
    .param p1, "password"    # Ljava/lang/String;
    .param p2, "userId"    # I

    .line 357
    iget-object v0, p0, Lcom/miui/server/AccessController;->mKeystoreHelper:Landroid/security/AccessControlKeystoreHelper;

    iget-object v1, p0, Lcom/miui/server/AccessController;->mContext:Landroid/content/Context;

    invoke-virtual {v0, v1}, Landroid/security/AccessControlKeystoreHelper;->disableEncryptByKeystore(Landroid/content/Context;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 358
    return-void

    .line 360
    :cond_0
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "saveAccessControlPasswordByKeystore "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    const-string v1, "AccessController"

    invoke-static {v1, v0}, Landroid/util/Slog;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 361
    const-string v0, "access_control_keystore.key"

    invoke-direct {p0, p2, v0}, Lcom/miui/server/AccessController;->getFilePathForUser(ILjava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 363
    .local v0, "filePath":Ljava/lang/String;
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "alias_app_lock_"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    .line 364
    .local v1, "alias":Ljava/lang/String;
    if-nez p1, :cond_1

    .line 365
    iget-object v2, p0, Lcom/miui/server/AccessController;->mKeystoreHelper:Landroid/security/AccessControlKeystoreHelper;

    invoke-virtual {v2, v1}, Landroid/security/AccessControlKeystoreHelper;->resetPassword(Ljava/lang/String;)[B

    move-result-object v2

    .local v2, "cipherText":[B
    goto :goto_0

    .line 367
    .end local v2    # "cipherText":[B
    :cond_1
    iget-object v2, p0, Lcom/miui/server/AccessController;->mKeystoreHelper:Landroid/security/AccessControlKeystoreHelper;

    invoke-virtual {v2, v1, p1}, Landroid/security/AccessControlKeystoreHelper;->encrypt(Ljava/lang/String;Ljava/lang/String;)[B

    move-result-object v2

    .line 369
    .restart local v2    # "cipherText":[B
    :goto_0
    invoke-direct {p0, v0, v2}, Lcom/miui/server/AccessController;->writeFile(Ljava/lang/String;[B)V

    .line 370
    return-void
.end method

.method private setAccessControlPasswordType(Ljava/lang/String;I)V
    .locals 1
    .param p1, "passwordType"    # Ljava/lang/String;
    .param p2, "userId"    # I

    .line 478
    const-string v0, "access_control_password_type.key"

    invoke-direct {p0, p2, v0}, Lcom/miui/server/AccessController;->getFilePathForUser(ILjava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 479
    .local v0, "filePath":Ljava/lang/String;
    invoke-direct {p0, v0, p1}, Lcom/miui/server/AccessController;->writeTypeFile(Ljava/lang/String;Ljava/lang/String;)V

    .line 480
    return-void
.end method

.method private setAccessControlPattern(Ljava/lang/String;I)V
    .locals 2
    .param p1, "pattern"    # Ljava/lang/String;
    .param p2, "userId"    # I

    .line 389
    const/4 v0, 0x0

    .line 390
    .local v0, "hash":[B
    if-eqz p1, :cond_0

    .line 391
    invoke-static {p1}, Landroid/security/MiuiLockPatternUtils;->stringToPattern(Ljava/lang/String;)Ljava/util/List;

    move-result-object v1

    .line 392
    .local v1, "stringToPattern":Ljava/util/List;, "Ljava/util/List<Lcom/android/internal/widget/LockPatternView$Cell;>;"
    invoke-static {v1}, Landroid/security/MiuiLockPatternUtils;->patternToHash(Ljava/util/List;)[B

    move-result-object v0

    .line 394
    .end local v1    # "stringToPattern":Ljava/util/List;, "Ljava/util/List<Lcom/android/internal/widget/LockPatternView$Cell;>;"
    :cond_0
    const-string v1, "access_control.key"

    invoke-direct {p0, p2, v1}, Lcom/miui/server/AccessController;->getFilePathForUser(ILjava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 395
    .local v1, "filePath":Ljava/lang/String;
    invoke-direct {p0, v1, v0}, Lcom/miui/server/AccessController;->writeFile(Ljava/lang/String;[B)V

    .line 396
    return-void
.end method

.method private updateWhiteList(Ljava/util/List;Landroid/util/ArrayMap;)V
    .locals 12
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List<",
            "Landroid/provider/MiuiSettings$SettingsCloudData$CloudData;",
            ">;",
            "Landroid/util/ArrayMap<",
            "Ljava/lang/String;",
            "Ljava/util/ArrayList<",
            "Landroid/content/Intent;",
            ">;>;)V"
        }
    .end annotation

    .line 628
    .local p1, "dataList":Ljava/util/List;, "Ljava/util/List<Landroid/provider/MiuiSettings$SettingsCloudData$CloudData;>;"
    .local p2, "list":Landroid/util/ArrayMap;, "Landroid/util/ArrayMap<Ljava/lang/String;Ljava/util/ArrayList<Landroid/content/Intent;>;>;"
    if-eqz p1, :cond_6

    :try_start_0
    invoke-interface {p1}, Ljava/util/List;->size()I

    move-result v0

    if-nez v0, :cond_0

    goto/16 :goto_4

    .line 631
    :cond_0
    new-instance v0, Landroid/util/ArrayMap;

    invoke-direct {v0}, Landroid/util/ArrayMap;-><init>()V

    .line 632
    .local v0, "cloudList":Landroid/util/ArrayMap;, "Landroid/util/ArrayMap<Ljava/lang/String;Ljava/util/ArrayList<Landroid/content/Intent;>;>;"
    invoke-interface {p1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_4

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Landroid/provider/MiuiSettings$SettingsCloudData$CloudData;

    .line 633
    .local v2, "data":Landroid/provider/MiuiSettings$SettingsCloudData$CloudData;
    invoke-virtual {v2}, Landroid/provider/MiuiSettings$SettingsCloudData$CloudData;->toString()Ljava/lang/String;

    move-result-object v3

    .line 634
    .local v3, "json":Ljava/lang/String;
    invoke-static {v3}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v4

    if-eqz v4, :cond_1

    .line 635
    goto :goto_0

    .line 638
    :cond_1
    new-instance v4, Lorg/json/JSONObject;

    invoke-direct {v4, v3}, Lorg/json/JSONObject;-><init>(Ljava/lang/String;)V

    .line 639
    .local v4, "jsonObject":Lorg/json/JSONObject;
    const-string v5, "pkg"

    invoke-virtual {v4, v5}, Lorg/json/JSONObject;->optString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v5

    .line 640
    .local v5, "pkg":Ljava/lang/String;
    const-string v6, "cls"

    invoke-virtual {v4, v6}, Lorg/json/JSONObject;->optString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v6

    .line 641
    .local v6, "cls":Ljava/lang/String;
    const-string v7, "act"

    invoke-virtual {v4, v7}, Lorg/json/JSONObject;->optString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v7

    .line 643
    .local v7, "action":Ljava/lang/String;
    new-instance v8, Landroid/content/Intent;

    invoke-direct {v8}, Landroid/content/Intent;-><init>()V

    .line 644
    .local v8, "intent":Landroid/content/Intent;
    invoke-static {v7}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v9

    if-nez v9, :cond_2

    .line 645
    invoke-virtual {v8, v7}, Landroid/content/Intent;->setAction(Ljava/lang/String;)Landroid/content/Intent;

    goto :goto_1

    .line 647
    :cond_2
    new-instance v9, Landroid/content/ComponentName;

    invoke-direct {v9, v5, v6}, Landroid/content/ComponentName;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {v8, v9}, Landroid/content/Intent;->setComponent(Landroid/content/ComponentName;)Landroid/content/Intent;

    .line 650
    :goto_1
    invoke-virtual {v0, v5}, Landroid/util/ArrayMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v9

    check-cast v9, Ljava/util/ArrayList;

    .line 651
    .local v9, "intents":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Landroid/content/Intent;>;"
    if-nez v9, :cond_3

    .line 652
    new-instance v10, Ljava/util/ArrayList;

    const/4 v11, 0x1

    invoke-direct {v10, v11}, Ljava/util/ArrayList;-><init>(I)V

    move-object v9, v10

    .line 653
    invoke-virtual {v0, v5, v9}, Landroid/util/ArrayMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 655
    :cond_3
    invoke-virtual {v9, v8}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 656
    nop

    .end local v2    # "data":Landroid/provider/MiuiSettings$SettingsCloudData$CloudData;
    .end local v3    # "json":Ljava/lang/String;
    .end local v4    # "jsonObject":Lorg/json/JSONObject;
    .end local v5    # "pkg":Ljava/lang/String;
    .end local v6    # "cls":Ljava/lang/String;
    .end local v7    # "action":Ljava/lang/String;
    .end local v8    # "intent":Landroid/content/Intent;
    .end local v9    # "intents":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Landroid/content/Intent;>;"
    goto :goto_0

    .line 657
    :cond_4
    invoke-virtual {v0}, Landroid/util/ArrayMap;->size()I

    move-result v1

    if-lez v1, :cond_5

    .line 658
    monitor-enter p0
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 659
    :try_start_1
    invoke-virtual {p2}, Landroid/util/ArrayMap;->clear()V

    .line 660
    invoke-virtual {p2, v0}, Landroid/util/ArrayMap;->putAll(Landroid/util/ArrayMap;)V

    .line 661
    monitor-exit p0

    goto :goto_2

    :catchall_0
    move-exception v1

    monitor-exit p0
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .end local p0    # "this":Lcom/miui/server/AccessController;
    .end local p1    # "dataList":Ljava/util/List;, "Ljava/util/List<Landroid/provider/MiuiSettings$SettingsCloudData$CloudData;>;"
    .end local p2    # "list":Landroid/util/ArrayMap;, "Landroid/util/ArrayMap<Ljava/lang/String;Ljava/util/ArrayList<Landroid/content/Intent;>;>;"
    :try_start_2
    throw v1
    :try_end_2
    .catch Ljava/lang/Exception; {:try_start_2 .. :try_end_2} :catch_0

    .line 665
    .end local v0    # "cloudList":Landroid/util/ArrayMap;, "Landroid/util/ArrayMap<Ljava/lang/String;Ljava/util/ArrayList<Landroid/content/Intent;>;>;"
    .restart local p0    # "this":Lcom/miui/server/AccessController;
    .restart local p1    # "dataList":Ljava/util/List;, "Ljava/util/List<Landroid/provider/MiuiSettings$SettingsCloudData$CloudData;>;"
    .restart local p2    # "list":Landroid/util/ArrayMap;, "Landroid/util/ArrayMap<Ljava/lang/String;Ljava/util/ArrayList<Landroid/content/Intent;>;>;"
    :cond_5
    :goto_2
    goto :goto_3

    .line 663
    :catch_0
    move-exception v0

    .line 664
    .local v0, "e":Ljava/lang/Exception;
    invoke-virtual {v0}, Ljava/lang/Exception;->printStackTrace()V

    .line 666
    .end local v0    # "e":Ljava/lang/Exception;
    :goto_3
    return-void

    .line 629
    :cond_6
    :goto_4
    return-void
.end method

.method private writeFile(Ljava/lang/String;[B)V
    .locals 7
    .param p1, "name"    # Ljava/lang/String;
    .param p2, "hash"    # [B

    .line 538
    iget-object v0, p0, Lcom/miui/server/AccessController;->mFileWriteLock:Ljava/lang/Object;

    monitor-enter v0

    .line 539
    const/4 v1, 0x0

    .line 542
    .local v1, "raf":Ljava/io/RandomAccessFile;
    :try_start_0
    new-instance v2, Ljava/io/RandomAccessFile;

    const-string v3, "rw"

    invoke-direct {v2, p1, v3}, Ljava/io/RandomAccessFile;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    move-object v1, v2

    .line 544
    const-wide/16 v2, 0x0

    invoke-virtual {v1, v2, v3}, Ljava/io/RandomAccessFile;->setLength(J)V

    .line 545
    if-eqz p2, :cond_0

    .line 546
    array-length v2, p2

    const/4 v3, 0x0

    invoke-virtual {v1, p2, v3, v2}, Ljava/io/RandomAccessFile;->write([BII)V

    .line 548
    :cond_0
    invoke-virtual {v1}, Ljava/io/RandomAccessFile;->close()V
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_1
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 552
    nop

    .line 554
    :try_start_1
    invoke-virtual {v1}, Ljava/io/RandomAccessFile;->close()V
    :try_end_1
    .catch Ljava/io/IOException; {:try_start_1 .. :try_end_1} :catch_0
    .catchall {:try_start_1 .. :try_end_1} :catchall_1

    .line 557
    :goto_0
    goto :goto_2

    .line 555
    :catch_0
    move-exception v2

    .line 556
    .local v2, "e":Ljava/io/IOException;
    :try_start_2
    const-string v3, "AccessController"

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "Error closing file "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    :goto_1
    invoke-static {v3, v4}, Landroid/util/Slog;->e(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 557
    nop

    .end local v2    # "e":Ljava/io/IOException;
    goto :goto_2

    .line 552
    :catchall_0
    move-exception v2

    goto :goto_3

    .line 549
    :catch_1
    move-exception v2

    .line 550
    .restart local v2    # "e":Ljava/io/IOException;
    :try_start_3
    const-string v3, "AccessController"

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "Error writing to file "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Landroid/util/Slog;->e(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    .line 552
    nop

    .end local v2    # "e":Ljava/io/IOException;
    if-eqz v1, :cond_1

    .line 554
    :try_start_4
    invoke-virtual {v1}, Ljava/io/RandomAccessFile;->close()V
    :try_end_4
    .catch Ljava/io/IOException; {:try_start_4 .. :try_end_4} :catch_2
    .catchall {:try_start_4 .. :try_end_4} :catchall_1

    goto :goto_0

    .line 555
    :catch_2
    move-exception v2

    .line 556
    .restart local v2    # "e":Ljava/io/IOException;
    :try_start_5
    const-string v3, "AccessController"

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "Error closing file "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    goto :goto_1

    .line 560
    .end local v1    # "raf":Ljava/io/RandomAccessFile;
    .end local v2    # "e":Ljava/io/IOException;
    :cond_1
    :goto_2
    monitor-exit v0
    :try_end_5
    .catchall {:try_start_5 .. :try_end_5} :catchall_1

    .line 561
    return-void

    .line 552
    .restart local v1    # "raf":Ljava/io/RandomAccessFile;
    :goto_3
    if-eqz v1, :cond_2

    .line 554
    :try_start_6
    invoke-virtual {v1}, Ljava/io/RandomAccessFile;->close()V
    :try_end_6
    .catch Ljava/io/IOException; {:try_start_6 .. :try_end_6} :catch_3
    .catchall {:try_start_6 .. :try_end_6} :catchall_1

    .line 557
    goto :goto_4

    .line 555
    :catch_3
    move-exception v3

    .line 556
    .local v3, "e":Ljava/io/IOException;
    :try_start_7
    const-string v4, "AccessController"

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "Error closing file "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5}, Landroid/util/Slog;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 559
    .end local v3    # "e":Ljava/io/IOException;
    :cond_2
    :goto_4
    nop

    .end local p0    # "this":Lcom/miui/server/AccessController;
    .end local p1    # "name":Ljava/lang/String;
    .end local p2    # "hash":[B
    throw v2

    .line 560
    .end local v1    # "raf":Ljava/io/RandomAccessFile;
    .restart local p0    # "this":Lcom/miui/server/AccessController;
    .restart local p1    # "name":Ljava/lang/String;
    .restart local p2    # "hash":[B
    :catchall_1
    move-exception v1

    monitor-exit v0
    :try_end_7
    .catchall {:try_start_7 .. :try_end_7} :catchall_1

    throw v1
.end method

.method private writeTypeFile(Ljava/lang/String;Ljava/lang/String;)V
    .locals 7
    .param p1, "name"    # Ljava/lang/String;
    .param p2, "passwordType"    # Ljava/lang/String;

    .line 564
    iget-object v0, p0, Lcom/miui/server/AccessController;->mFileWriteLock:Ljava/lang/Object;

    monitor-enter v0

    .line 565
    const/4 v1, 0x0

    .line 568
    .local v1, "raf":Ljava/io/RandomAccessFile;
    :try_start_0
    new-instance v2, Ljava/io/RandomAccessFile;

    const-string v3, "rw"

    invoke-direct {v2, p1, v3}, Ljava/io/RandomAccessFile;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    move-object v1, v2

    .line 570
    const-wide/16 v2, 0x0

    invoke-virtual {v1, v2, v3}, Ljava/io/RandomAccessFile;->setLength(J)V

    .line 571
    if-eqz p2, :cond_0

    .line 572
    invoke-virtual {v1, p2}, Ljava/io/RandomAccessFile;->writeBytes(Ljava/lang/String;)V

    .line 574
    :cond_0
    invoke-virtual {v1}, Ljava/io/RandomAccessFile;->close()V
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_1
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 578
    nop

    .line 580
    :try_start_1
    invoke-virtual {v1}, Ljava/io/RandomAccessFile;->close()V
    :try_end_1
    .catch Ljava/io/IOException; {:try_start_1 .. :try_end_1} :catch_0
    .catchall {:try_start_1 .. :try_end_1} :catchall_1

    .line 583
    :goto_0
    goto :goto_2

    .line 581
    :catch_0
    move-exception v2

    .line 582
    .local v2, "e":Ljava/io/IOException;
    :try_start_2
    const-string v3, "AccessController"

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "Error closing type file "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    :goto_1
    invoke-static {v3, v4}, Landroid/util/Slog;->e(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 583
    nop

    .end local v2    # "e":Ljava/io/IOException;
    goto :goto_2

    .line 578
    :catchall_0
    move-exception v2

    goto :goto_3

    .line 575
    :catch_1
    move-exception v2

    .line 576
    .restart local v2    # "e":Ljava/io/IOException;
    :try_start_3
    const-string v3, "AccessController"

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "Error writing type to file "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Landroid/util/Slog;->e(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    .line 578
    nop

    .end local v2    # "e":Ljava/io/IOException;
    if-eqz v1, :cond_1

    .line 580
    :try_start_4
    invoke-virtual {v1}, Ljava/io/RandomAccessFile;->close()V
    :try_end_4
    .catch Ljava/io/IOException; {:try_start_4 .. :try_end_4} :catch_2
    .catchall {:try_start_4 .. :try_end_4} :catchall_1

    goto :goto_0

    .line 581
    :catch_2
    move-exception v2

    .line 582
    .restart local v2    # "e":Ljava/io/IOException;
    :try_start_5
    const-string v3, "AccessController"

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "Error closing type file "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    goto :goto_1

    .line 586
    .end local v1    # "raf":Ljava/io/RandomAccessFile;
    .end local v2    # "e":Ljava/io/IOException;
    :cond_1
    :goto_2
    monitor-exit v0
    :try_end_5
    .catchall {:try_start_5 .. :try_end_5} :catchall_1

    .line 587
    return-void

    .line 578
    .restart local v1    # "raf":Ljava/io/RandomAccessFile;
    :goto_3
    if-eqz v1, :cond_2

    .line 580
    :try_start_6
    invoke-virtual {v1}, Ljava/io/RandomAccessFile;->close()V
    :try_end_6
    .catch Ljava/io/IOException; {:try_start_6 .. :try_end_6} :catch_3
    .catchall {:try_start_6 .. :try_end_6} :catchall_1

    .line 583
    goto :goto_4

    .line 581
    :catch_3
    move-exception v3

    .line 582
    .local v3, "e":Ljava/io/IOException;
    :try_start_7
    const-string v4, "AccessController"

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "Error closing type file "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5}, Landroid/util/Slog;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 585
    .end local v3    # "e":Ljava/io/IOException;
    :cond_2
    :goto_4
    nop

    .end local p0    # "this":Lcom/miui/server/AccessController;
    .end local p1    # "name":Ljava/lang/String;
    .end local p2    # "passwordType":Ljava/lang/String;
    throw v2

    .line 586
    .end local v1    # "raf":Ljava/io/RandomAccessFile;
    .restart local p0    # "this":Lcom/miui/server/AccessController;
    .restart local p1    # "name":Ljava/lang/String;
    .restart local p2    # "passwordType":Ljava/lang/String;
    :catchall_1
    move-exception v1

    monitor-exit v0
    :try_end_7
    .catchall {:try_start_7 .. :try_end_7} :catchall_1

    throw v1
.end method


# virtual methods
.method checkAccessControlPassword(Ljava/lang/String;Ljava/lang/String;I)Z
    .locals 4
    .param p1, "passwordType"    # Ljava/lang/String;
    .param p2, "password"    # Ljava/lang/String;
    .param p3, "userId"    # I

    .line 426
    if-eqz p2, :cond_3

    if-nez p1, :cond_0

    goto :goto_0

    .line 429
    :cond_0
    invoke-direct {p0, p2, p3}, Lcom/miui/server/AccessController;->checkAccessControlPasswordByKeystore(Ljava/lang/String;I)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 430
    const/4 v0, 0x1

    return v0

    .line 432
    :cond_1
    const-string v0, "AccessController"

    const-string v1, "checkAccessControlPassword!"

    invoke-static {v0, v1}, Landroid/util/Slog;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 434
    const-string v0, "pattern"

    invoke-virtual {v0, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 435
    invoke-direct {p0, p2, p3}, Lcom/miui/server/AccessController;->checkAccessControlPattern(Ljava/lang/String;I)Z

    move-result v0

    return v0

    .line 437
    :cond_2
    invoke-direct {p0, p2, p3}, Lcom/miui/server/AccessController;->passwordToHash(Ljava/lang/String;I)[B

    move-result-object v0

    .line 438
    .local v0, "hash":[B
    const-string v1, "access_control.key"

    invoke-direct {p0, p3, v1}, Lcom/miui/server/AccessController;->getFilePathForUser(ILjava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 439
    .local v1, "filePath":Ljava/lang/String;
    invoke-direct {p0, v1}, Lcom/miui/server/AccessController;->readFile(Ljava/lang/String;)[B

    move-result-object v2

    .line 440
    .local v2, "readFile":[B
    invoke-static {v2, v0}, Ljava/util/Arrays;->equals([B[B)Z

    move-result v3

    return v3

    .line 427
    .end local v0    # "hash":[B
    .end local v1    # "filePath":Ljava/lang/String;
    .end local v2    # "readFile":[B
    :cond_3
    :goto_0
    const/4 v0, 0x0

    return v0
.end method

.method public filterIntentLocked(ZLjava/lang/String;Landroid/content/Intent;)Z
    .locals 1
    .param p1, "isSkipIntent"    # Z
    .param p2, "packageName"    # Ljava/lang/String;
    .param p3, "intent"    # Landroid/content/Intent;

    .line 229
    const/4 v0, 0x0

    invoke-virtual {p0, p1, v0, p2, p3}, Lcom/miui/server/AccessController;->filterIntentLocked(ZZLjava/lang/String;Landroid/content/Intent;)Z

    move-result v0

    return v0
.end method

.method public filterIntentLocked(ZZLjava/lang/String;Landroid/content/Intent;)Z
    .locals 15
    .param p1, "isSkipIntent"    # Z
    .param p2, "pay"    # Z
    .param p3, "packageName"    # Ljava/lang/String;
    .param p4, "intent"    # Landroid/content/Intent;

    .line 237
    move-object v1, p0

    move-object/from16 v2, p3

    move-object/from16 v3, p4

    const/4 v0, 0x0

    if-nez v3, :cond_0

    .line 238
    return v0

    .line 240
    :cond_0
    monitor-enter p0

    .line 241
    const/4 v4, 0x1

    if-eqz p2, :cond_4

    .line 242
    :try_start_0
    const-string v5, "android.intent.extra.INTENT"

    invoke-virtual {v3, v5}, Landroid/content/Intent;->getParcelableExtra(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v5

    check-cast v5, Landroid/content/Intent;

    .line 243
    .local v5, "rawIntent":Landroid/content/Intent;
    const/4 v6, 0x0

    .line 244
    .local v6, "rawPackageName":Ljava/lang/String;
    if-eqz v5, :cond_1

    invoke-virtual {v5}, Landroid/content/Intent;->getComponent()Landroid/content/ComponentName;

    move-result-object v7

    if-eqz v7, :cond_1

    const-string v7, "com.miui.securitycenter/com.miui.applicationlock.ConfirmAccessControl"

    .line 245
    invoke-virtual/range {p4 .. p4}, Landroid/content/Intent;->getComponent()Landroid/content/ComponentName;

    move-result-object v8

    invoke-virtual {v8}, Landroid/content/ComponentName;->flattenToShortString()Ljava/lang/String;

    move-result-object v8

    invoke-static {v7, v8}, Landroid/text/TextUtils;->equals(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Z

    move-result v7

    if-eqz v7, :cond_1

    .line 246
    invoke-virtual {v5}, Landroid/content/Intent;->getComponent()Landroid/content/ComponentName;

    move-result-object v7

    invoke-virtual {v7}, Landroid/content/ComponentName;->getPackageName()Ljava/lang/String;

    move-result-object v7

    move-object v6, v7

    goto :goto_0

    .line 248
    :cond_1
    move-object/from16 v6, p3

    .line 249
    move-object/from16 v5, p4

    .line 251
    :goto_0
    iget-object v7, v1, Lcom/miui/server/AccessController;->mContext:Landroid/content/Context;

    invoke-static {v7, v6}, Lcom/miui/server/security/DefaultBrowserImpl;->isDefaultBrowser(Landroid/content/Context;Ljava/lang/String;)Z

    move-result v7

    if-eqz v7, :cond_4

    .line 252
    invoke-virtual {v5}, Landroid/content/Intent;->getData()Landroid/net/Uri;

    move-result-object v7

    .line 253
    .local v7, "uri":Landroid/net/Uri;
    if-eqz v7, :cond_3

    invoke-virtual {v7}, Landroid/net/Uri;->getHost()Ljava/lang/String;

    move-result-object v8

    if-eqz v8, :cond_3

    invoke-virtual {v7}, Landroid/net/Uri;->getScheme()Ljava/lang/String;

    move-result-object v8

    if-eqz v8, :cond_3

    .line 254
    invoke-virtual {v5}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v8

    if-eqz v8, :cond_3

    .line 255
    invoke-virtual {v7}, Landroid/net/Uri;->getScheme()Ljava/lang/String;

    move-result-object v8

    const-string v9, "http"

    invoke-virtual {v8, v9}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v8

    if-nez v8, :cond_2

    .line 256
    invoke-virtual {v7}, Landroid/net/Uri;->getScheme()Ljava/lang/String;

    move-result-object v8

    const-string v9, "https"

    invoke-virtual {v8, v9}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v8

    if-eqz v8, :cond_3

    .line 257
    :cond_2
    monitor-exit p0

    return v4

    .line 259
    :cond_3
    monitor-exit p0

    return v0

    .line 320
    .end local v5    # "rawIntent":Landroid/content/Intent;
    .end local v6    # "rawPackageName":Ljava/lang/String;
    .end local v7    # "uri":Landroid/net/Uri;
    :catchall_0
    move-exception v0

    goto/16 :goto_6

    .line 263
    :cond_4
    move-object/from16 v5, p4

    .line 265
    .local v5, "oriIntent":Landroid/content/Intent;
    if-eqz p1, :cond_5

    .line 266
    sget-object v6, Lcom/miui/server/AccessController;->mSkipList:Landroid/util/ArrayMap;

    invoke-virtual {v6, v2}, Landroid/util/ArrayMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v6

    check-cast v6, Ljava/util/ArrayList;

    .local v6, "intents":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Landroid/content/Intent;>;"
    goto :goto_2

    .line 267
    .end local v6    # "intents":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Landroid/content/Intent;>;"
    :cond_5
    if-eqz p2, :cond_8

    .line 268
    const-string v6, "android.intent.extra.INTENT"

    invoke-virtual {v3, v6}, Landroid/content/Intent;->getParcelableExtra(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v6

    check-cast v6, Landroid/content/Intent;

    .line 269
    .local v6, "rawIntent":Landroid/content/Intent;
    const/4 v7, 0x0

    .line 270
    .local v7, "rawPackageName":Ljava/lang/String;
    if-eqz v6, :cond_6

    invoke-virtual {v6}, Landroid/content/Intent;->getComponent()Landroid/content/ComponentName;

    move-result-object v8

    if-eqz v8, :cond_6

    .line 271
    invoke-virtual {v6}, Landroid/content/Intent;->getComponent()Landroid/content/ComponentName;

    move-result-object v8

    invoke-virtual {v8}, Landroid/content/ComponentName;->getPackageName()Ljava/lang/String;

    move-result-object v8

    move-object v7, v8

    .line 273
    :cond_6
    invoke-virtual/range {p4 .. p4}, Landroid/content/Intent;->getComponent()Landroid/content/ComponentName;

    move-result-object v8

    if-eqz v8, :cond_7

    const-string v8, "com.miui.securitycenter/com.miui.applicationlock.ConfirmAccessControl"

    invoke-virtual/range {p4 .. p4}, Landroid/content/Intent;->getComponent()Landroid/content/ComponentName;

    move-result-object v9

    invoke-virtual {v9}, Landroid/content/ComponentName;->flattenToShortString()Ljava/lang/String;

    move-result-object v9

    invoke-static {v8, v9}, Landroid/text/TextUtils;->equals(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Z

    move-result v8

    if-eqz v8, :cond_7

    sget-object v8, Lcom/miui/server/AccessController;->mPayInterceptList:Landroid/util/ArrayMap;

    invoke-virtual {v8, v7}, Landroid/util/ArrayMap;->containsKey(Ljava/lang/Object;)Z

    move-result v8

    if-eqz v8, :cond_7

    .line 274
    move-object v5, v6

    .line 275
    sget-object v8, Lcom/miui/server/AccessController;->mPayInterceptList:Landroid/util/ArrayMap;

    invoke-virtual {v8, v7}, Landroid/util/ArrayMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v8

    check-cast v8, Ljava/util/ArrayList;

    move-object v6, v8

    .local v8, "intents":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Landroid/content/Intent;>;"
    goto :goto_1

    .line 277
    .end local v8    # "intents":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Landroid/content/Intent;>;"
    :cond_7
    sget-object v8, Lcom/miui/server/AccessController;->mPayInterceptList:Landroid/util/ArrayMap;

    invoke-virtual {v8, v2}, Landroid/util/ArrayMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v8

    check-cast v8, Ljava/util/ArrayList;

    move-object v6, v8

    .line 279
    .end local v7    # "rawPackageName":Ljava/lang/String;
    .local v6, "intents":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Landroid/content/Intent;>;"
    :goto_1
    goto :goto_2

    .line 280
    .end local v6    # "intents":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Landroid/content/Intent;>;"
    :cond_8
    sget-object v6, Lcom/miui/server/AccessController;->mAntimsgInterceptList:Landroid/util/ArrayMap;

    invoke-virtual {v6, v2}, Landroid/util/ArrayMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v6

    check-cast v6, Ljava/util/ArrayList;

    .line 282
    .restart local v6    # "intents":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Landroid/content/Intent;>;"
    :goto_2
    if-nez v6, :cond_9

    .line 283
    monitor-exit p0

    return v0

    .line 286
    :cond_9
    invoke-virtual {v5}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v7

    .line 287
    .local v7, "action":Ljava/lang/String;
    invoke-virtual {v5}, Landroid/content/Intent;->getComponent()Landroid/content/ComponentName;

    move-result-object v8

    .line 288
    .local v8, "component":Landroid/content/ComponentName;
    if-eqz v7, :cond_b

    .line 289
    invoke-virtual {v6}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v9

    :goto_3
    invoke-interface {v9}, Ljava/util/Iterator;->hasNext()Z

    move-result v10

    if-eqz v10, :cond_b

    invoke-interface {v9}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v10

    check-cast v10, Landroid/content/Intent;

    .line 290
    .local v10, "i":Landroid/content/Intent;
    invoke-virtual {v10}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v11

    invoke-virtual {v7, v11}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v11

    if-eqz v11, :cond_a

    .line 291
    monitor-exit p0

    return v4

    .line 293
    .end local v10    # "i":Landroid/content/Intent;
    :cond_a
    goto :goto_3

    .line 296
    :cond_b
    if-eqz v8, :cond_10

    .line 298
    invoke-virtual {v8}, Landroid/content/ComponentName;->getClassName()Ljava/lang/String;

    move-result-object v9

    .line 299
    .local v9, "cls":Ljava/lang/String;
    if-nez v9, :cond_c

    .line 300
    monitor-exit p0

    return v0

    .line 302
    :cond_c
    invoke-virtual {v9, v0}, Ljava/lang/String;->charAt(I)C

    move-result v10

    const/16 v11, 0x2e

    if-ne v10, v11, :cond_d

    .line 304
    new-instance v10, Ljava/lang/StringBuilder;

    invoke-direct {v10}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v8}, Landroid/content/ComponentName;->getPackageName()Ljava/lang/String;

    move-result-object v11

    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-virtual {v10, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-virtual {v10}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v10

    .local v10, "fullName":Ljava/lang/String;
    goto :goto_4

    .line 307
    .end local v10    # "fullName":Ljava/lang/String;
    :cond_d
    move-object v10, v9

    .line 310
    .restart local v10    # "fullName":Ljava/lang/String;
    :goto_4
    if-nez p1, :cond_e

    const-string v11, "com.tencent.mm.plugin.voip.ui.VideoActivity"

    invoke-virtual {v11, v9}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v11

    if-eqz v11, :cond_e

    invoke-virtual/range {p4 .. p4}, Landroid/content/Intent;->getFlags()I

    move-result v11

    const v12, -0x10000001

    and-int/2addr v11, v12

    if-nez v11, :cond_e

    .line 311
    monitor-exit p0

    return v0

    .line 313
    :cond_e
    invoke-virtual {v6}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v11

    :goto_5
    invoke-interface {v11}, Ljava/util/Iterator;->hasNext()Z

    move-result v12

    if-eqz v12, :cond_10

    invoke-interface {v11}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v12

    check-cast v12, Landroid/content/Intent;

    .line 314
    .local v12, "i":Landroid/content/Intent;
    invoke-virtual {v12}, Landroid/content/Intent;->getComponent()Landroid/content/ComponentName;

    move-result-object v13

    .line 315
    .local v13, "c":Landroid/content/ComponentName;
    if-eqz v13, :cond_f

    invoke-virtual {v13}, Landroid/content/ComponentName;->getClassName()Ljava/lang/String;

    move-result-object v14

    invoke-virtual {v10, v14}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v14

    if-eqz v14, :cond_f

    .line 316
    monitor-exit p0

    return v4

    .line 318
    .end local v12    # "i":Landroid/content/Intent;
    .end local v13    # "c":Landroid/content/ComponentName;
    :cond_f
    goto :goto_5

    .line 320
    .end local v5    # "oriIntent":Landroid/content/Intent;
    .end local v6    # "intents":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Landroid/content/Intent;>;"
    .end local v7    # "action":Ljava/lang/String;
    .end local v8    # "component":Landroid/content/ComponentName;
    .end local v9    # "cls":Ljava/lang/String;
    .end local v10    # "fullName":Ljava/lang/String;
    :cond_10
    monitor-exit p0

    .line 322
    return v0

    .line 320
    :goto_6
    monitor-exit p0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method

.method getAccessControlPasswordType(I)Ljava/lang/String;
    .locals 2
    .param p1, "userId"    # I

    .line 463
    const-string v0, "access_control_password_type.key"

    invoke-direct {p0, p1, v0}, Lcom/miui/server/AccessController;->getFilePathForUser(ILjava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 464
    .local v0, "filePath":Ljava/lang/String;
    if-nez v0, :cond_0

    .line 465
    const-string v1, "pattern"

    return-object v1

    .line 467
    :cond_0
    invoke-direct {p0, v0}, Lcom/miui/server/AccessController;->readTypeFile(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    return-object v1
.end method

.method public getForceLaunchList()Ljava/util/List;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .line 705
    sget-object v0, Lcom/miui/server/AccessController;->mForceLaunchList:Ljava/util/List;

    return-object v0
.end method

.method haveAccessControlPassword(I)Z
    .locals 9
    .param p1, "userId"    # I

    .line 452
    const-string v0, "access_control_password_type.key"

    invoke-direct {p0, p1, v0}, Lcom/miui/server/AccessController;->getFilePathForUser(ILjava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 453
    .local v0, "filePathType":Ljava/lang/String;
    const-string v1, "access_control.key"

    invoke-direct {p0, p1, v1}, Lcom/miui/server/AccessController;->getFilePathForUser(ILjava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 454
    .local v1, "filePathPassword":Ljava/lang/String;
    iget-object v2, p0, Lcom/miui/server/AccessController;->mFileWriteLock:Ljava/lang/Object;

    monitor-enter v2

    .line 455
    :try_start_0
    new-instance v3, Ljava/io/File;

    invoke-direct {v3, v0}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 456
    .local v3, "fileType":Ljava/io/File;
    new-instance v4, Ljava/io/File;

    invoke-direct {v4, v1}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 457
    .local v4, "filePassword":Ljava/io/File;
    invoke-virtual {v3}, Ljava/io/File;->exists()Z

    move-result v5

    if-eqz v5, :cond_0

    invoke-virtual {v4}, Ljava/io/File;->exists()Z

    move-result v5

    if-eqz v5, :cond_0

    .line 458
    invoke-virtual {v3}, Ljava/io/File;->length()J

    move-result-wide v5

    const-wide/16 v7, 0x0

    cmp-long v5, v5, v7

    if-lez v5, :cond_0

    invoke-virtual {v4}, Ljava/io/File;->length()J

    move-result-wide v5

    cmp-long v5, v5, v7

    if-lez v5, :cond_0

    const/4 v5, 0x1

    goto :goto_0

    :cond_0
    const/4 v5, 0x0

    :goto_0
    monitor-exit v2

    .line 457
    return v5

    .line 459
    .end local v3    # "fileType":Ljava/io/File;
    .end local v4    # "filePassword":Ljava/io/File;
    :catchall_0
    move-exception v3

    monitor-exit v2
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v3
.end method

.method setAccessControlPassword(Ljava/lang/String;Ljava/lang/String;I)V
    .locals 2
    .param p1, "passwordType"    # Ljava/lang/String;
    .param p2, "password"    # Ljava/lang/String;
    .param p3, "userId"    # I

    .line 399
    invoke-direct {p0, p2, p3}, Lcom/miui/server/AccessController;->saveAccessControlPasswordByKeystore(Ljava/lang/String;I)V

    .line 401
    const-string v0, "pattern"

    invoke-virtual {v0, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 402
    invoke-direct {p0, p2, p3}, Lcom/miui/server/AccessController;->setAccessControlPattern(Ljava/lang/String;I)V

    goto :goto_0

    .line 404
    :cond_0
    const/4 v0, 0x0

    .line 405
    .local v0, "hash":[B
    if-eqz p2, :cond_1

    .line 406
    invoke-direct {p0, p2, p3}, Lcom/miui/server/AccessController;->passwordToHash(Ljava/lang/String;I)[B

    move-result-object v0

    .line 408
    :cond_1
    const-string v1, "access_control.key"

    invoke-direct {p0, p3, v1}, Lcom/miui/server/AccessController;->getFilePathForUser(ILjava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 409
    .local v1, "filePath":Ljava/lang/String;
    invoke-direct {p0, v1, v0}, Lcom/miui/server/AccessController;->writeFile(Ljava/lang/String;[B)V

    .line 411
    .end local v0    # "hash":[B
    .end local v1    # "filePath":Ljava/lang/String;
    :goto_0
    invoke-direct {p0, p1, p3}, Lcom/miui/server/AccessController;->setAccessControlPasswordType(Ljava/lang/String;I)V

    .line 412
    return-void
.end method

.method public skipActivity(Landroid/content/Intent;Ljava/lang/String;)Z
    .locals 5
    .param p1, "intent"    # Landroid/content/Intent;
    .param p2, "callingPkg"    # Ljava/lang/String;

    .line 326
    const/4 v0, 0x0

    if-eqz p1, :cond_2

    .line 328
    :try_start_0
    invoke-virtual {p1}, Landroid/content/Intent;->getComponent()Landroid/content/ComponentName;

    move-result-object v1

    .line 329
    .local v1, "componentName":Landroid/content/ComponentName;
    if-eqz v1, :cond_1

    .line 330
    invoke-virtual {v1}, Landroid/content/ComponentName;->getPackageName()Ljava/lang/String;

    move-result-object v2

    .line 331
    .local v2, "packageName":Ljava/lang/String;
    invoke-virtual {v1}, Landroid/content/ComponentName;->getClassName()Ljava/lang/String;

    move-result-object v3

    .line 332
    .local v3, "activity":Ljava/lang/String;
    invoke-static {p2}, Lcom/miui/server/AccessController;->isOpenedPkg(Ljava/lang/String;)Z

    move-result v4

    if-eqz v4, :cond_1

    invoke-static {v2}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v4

    if-nez v4, :cond_1

    invoke-static {v3}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v4

    if-nez v4, :cond_1

    .line 333
    const-string v4, "com.miui.gallery"

    invoke-virtual {v4, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_0

    .line 334
    invoke-static {v3}, Lcom/miui/server/AccessController;->isOpenedActivity(Ljava/lang/String;)Z

    move-result v4

    if-eqz v4, :cond_0

    const-string/jumbo v4, "skip_interception"

    invoke-virtual {p1, v4, v0}, Landroid/content/Intent;->getBooleanExtra(Ljava/lang/String;Z)Z

    move-result v4
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    if-eqz v4, :cond_0

    const/4 v0, 0x1

    goto :goto_0

    :cond_0
    nop

    .line 333
    :goto_0
    return v0

    .line 340
    .end local v1    # "componentName":Landroid/content/ComponentName;
    .end local v2    # "packageName":Ljava/lang/String;
    .end local v3    # "activity":Ljava/lang/String;
    :cond_1
    goto :goto_1

    .line 338
    :catchall_0
    move-exception v1

    .line 339
    .local v1, "e":Ljava/lang/Throwable;
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "can not getStringExtra"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    const-string v3, "AccessController"

    invoke-static {v3, v2}, Landroid/util/Slog;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 342
    .end local v1    # "e":Ljava/lang/Throwable;
    :cond_2
    :goto_1
    return v0
.end method

.method updatePasswordTypeForPattern(I)V
    .locals 2
    .param p1, "userId"    # I

    .line 471
    invoke-direct {p0, p1}, Lcom/miui/server/AccessController;->haveAccessControlPattern(I)Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-direct {p0, p1}, Lcom/miui/server/AccessController;->haveAccessControlPasswordType(I)Z

    move-result v0

    if-nez v0, :cond_0

    .line 472
    const-string v0, "pattern"

    invoke-direct {p0, v0, p1}, Lcom/miui/server/AccessController;->setAccessControlPasswordType(Ljava/lang/String;I)V

    .line 473
    const-string v0, "AccessController"

    const-string/jumbo v1, "update password type succeed"

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 475
    :cond_0
    return-void
.end method

.method public updateWhiteList()V
    .locals 5

    .line 609
    :try_start_0
    iget-object v0, p0, Lcom/miui/server/AccessController;->mContext:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    .line 610
    .local v0, "resolver":Landroid/content/ContentResolver;
    iget-object v1, p0, Lcom/miui/server/AccessController;->mWorkHandler:Lcom/miui/server/AccessController$WorkHandler;

    const/4 v2, 0x1

    invoke-virtual {v1, v2}, Lcom/miui/server/AccessController$WorkHandler;->removeMessages(I)V

    .line 611
    iget-object v1, p0, Lcom/miui/server/AccessController;->mWorkHandler:Lcom/miui/server/AccessController$WorkHandler;

    const-wide/32 v3, 0x2932e00

    invoke-virtual {v1, v2, v3, v4}, Lcom/miui/server/AccessController$WorkHandler;->sendEmptyMessageDelayed(IJ)Z

    .line 612
    const-string v1, "applock_whilte"

    invoke-static {v0, v1}, Landroid/provider/MiuiSettings$SettingsCloudData;->getCloudDataList(Landroid/content/ContentResolver;Ljava/lang/String;)Ljava/util/List;

    move-result-object v1

    .line 613
    .local v1, "appLockList":Ljava/util/List;, "Ljava/util/List<Landroid/provider/MiuiSettings$SettingsCloudData$CloudData;>;"
    const-string v2, "gamebooster_antimsg"

    invoke-static {v0, v2}, Landroid/provider/MiuiSettings$SettingsCloudData;->getCloudDataList(Landroid/content/ContentResolver;Ljava/lang/String;)Ljava/util/List;

    move-result-object v2

    .line 615
    .local v2, "gameAntimsgList":Ljava/util/List;, "Ljava/util/List<Landroid/provider/MiuiSettings$SettingsCloudData$CloudData;>;"
    const-string v3, "gamebooster_pay"

    invoke-static {v0, v3}, Landroid/provider/MiuiSettings$SettingsCloudData;->getCloudDataList(Landroid/content/ContentResolver;Ljava/lang/String;)Ljava/util/List;

    move-result-object v3

    .line 616
    .local v3, "gamePayList":Ljava/util/List;, "Ljava/util/List<Landroid/provider/MiuiSettings$SettingsCloudData$CloudData;>;"
    sget-object v4, Lcom/miui/server/AccessController;->mSkipList:Landroid/util/ArrayMap;

    invoke-direct {p0, v1, v4}, Lcom/miui/server/AccessController;->updateWhiteList(Ljava/util/List;Landroid/util/ArrayMap;)V

    .line 617
    sget-object v4, Lcom/miui/server/AccessController;->mPayInterceptList:Landroid/util/ArrayMap;

    invoke-direct {p0, v3, v4}, Lcom/miui/server/AccessController;->updateWhiteList(Ljava/util/List;Landroid/util/ArrayMap;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 620
    .end local v0    # "resolver":Landroid/content/ContentResolver;
    .end local v1    # "appLockList":Ljava/util/List;, "Ljava/util/List<Landroid/provider/MiuiSettings$SettingsCloudData$CloudData;>;"
    .end local v2    # "gameAntimsgList":Ljava/util/List;, "Ljava/util/List<Landroid/provider/MiuiSettings$SettingsCloudData$CloudData;>;"
    .end local v3    # "gamePayList":Ljava/util/List;, "Ljava/util/List<Landroid/provider/MiuiSettings$SettingsCloudData$CloudData;>;"
    goto :goto_0

    .line 618
    :catch_0
    move-exception v0

    .line 619
    .local v0, "e":Ljava/lang/Exception;
    invoke-virtual {v0}, Ljava/lang/Exception;->printStackTrace()V

    .line 621
    .end local v0    # "e":Ljava/lang/Exception;
    :goto_0
    return-void
.end method
