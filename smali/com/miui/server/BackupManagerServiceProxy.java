class com.miui.server.BackupManagerServiceProxy {
	 /* .source "BackupManagerServiceProxy.java" */
	 /* # static fields */
	 public static final java.lang.String TAG;
	 /* # direct methods */
	 com.miui.server.BackupManagerServiceProxy ( ) {
		 /* .locals 0 */
		 /* .line 18 */
		 /* invoke-direct {p0}, Ljava/lang/Object;-><init>()V */
		 return;
	 } // .end method
	 static void fullBackup ( android.os.ParcelFileDescriptor p0, java.lang.String[] p1, Boolean p2 ) {
		 /* .locals 13 */
		 /* .param p0, "outFileDescriptor" # Landroid/os/ParcelFileDescriptor; */
		 /* .param p1, "pkgs" # [Ljava/lang/String; */
		 /* .param p2, "includeApk" # Z */
		 /* .annotation system Ldalvik/annotation/Throws; */
		 /* value = { */
		 /* Landroid/os/RemoteException; */
		 /* } */
	 } // .end annotation
	 /* .line 21 */
	 final String v0 = "backup"; // const-string v0, "backup"
	 android.os.ServiceManager .getService ( v0 );
	 /* check-cast v0, Landroid/app/backup/IBackupManager; */
	 /* .line 22 */
	 /* .local v0, "bm":Landroid/app/backup/IBackupManager; */
	 int v2 = 0; // const/4 v2, 0x0
	 int v5 = 1; // const/4 v5, 0x1
	 int v6 = 0; // const/4 v6, 0x0
	 int v7 = 0; // const/4 v7, 0x0
	 int v8 = 0; // const/4 v8, 0x0
	 int v9 = 0; // const/4 v9, 0x0
	 int v10 = 0; // const/4 v10, 0x0
	 int v11 = 0; // const/4 v11, 0x0
	 /* move-object v1, v0 */
	 /* move-object v3, p0 */
	 /* move v4, p2 */
	 /* move-object v12, p1 */
	 /* invoke-interface/range {v1 ..v12}, Landroid/app/backup/IBackupManager;->adbBackup(ILandroid/os/ParcelFileDescriptor;ZZZZZZZZ[Ljava/lang/String;)V */
	 /* .line 23 */
	 return;
} // .end method
static void fullCancel ( ) {
	 /* .locals 2 */
	 /* .annotation system Ldalvik/annotation/Throws; */
	 /* value = { */
	 /* Landroid/os/RemoteException; */
	 /* } */
} // .end annotation
/* .line 31 */
final String v0 = "backup"; // const-string v0, "backup"
android.os.ServiceManager .getService ( v0 );
/* check-cast v0, Landroid/app/backup/IBackupManager; */
/* .line 32 */
/* .local v0, "bm":Landroid/app/backup/IBackupManager; */
int v1 = 0; // const/4 v1, 0x0
/* .line 33 */
return;
} // .end method
static void fullRestore ( android.os.ParcelFileDescriptor p0 ) {
/* .locals 2 */
/* .param p0, "fd" # Landroid/os/ParcelFileDescriptor; */
/* .annotation system Ldalvik/annotation/Throws; */
/* value = { */
/* Landroid/os/RemoteException; */
/* } */
} // .end annotation
/* .line 26 */
final String v0 = "backup"; // const-string v0, "backup"
android.os.ServiceManager .getService ( v0 );
/* check-cast v0, Landroid/app/backup/IBackupManager; */
/* .line 27 */
/* .local v0, "bm":Landroid/app/backup/IBackupManager; */
int v1 = 0; // const/4 v1, 0x0
/* .line 28 */
return;
} // .end method
public static void getPackageSizeInfo ( android.content.Context p0, android.content.pm.PackageManager p1, java.lang.String p2, Integer p3, android.content.pm.IPackageStatsObserver p4 ) {
/* .locals 10 */
/* .param p0, "context" # Landroid/content/Context; */
/* .param p1, "pm" # Landroid/content/pm/PackageManager; */
/* .param p2, "pkg" # Ljava/lang/String; */
/* .param p3, "userId" # I */
/* .param p4, "observer" # Landroid/content/pm/IPackageStatsObserver; */
/* .line 37 */
/* nop */
/* .line 38 */
/* const-string/jumbo v0, "storagestats" */
(( android.content.Context ) p0 ).getSystemService ( v0 ); // invoke-virtual {p0, v0}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;
/* check-cast v0, Landroid/app/usage/StorageStatsManager; */
/* .line 39 */
/* .local v0, "ssm":Landroid/app/usage/StorageStatsManager; */
android.os.Binder .clearCallingIdentity ( );
/* move-result-wide v1 */
/* .line 41 */
/* .local v1, "oldId":J */
int v3 = 0; // const/4 v3, 0x0
try { // :try_start_0
(( android.content.pm.PackageManager ) p1 ).getPackageInfoAsUser ( p2, v3, p3 ); // invoke-virtual {p1, p2, v3, p3}, Landroid/content/pm/PackageManager;->getPackageInfoAsUser(Ljava/lang/String;II)Landroid/content/pm/PackageInfo;
/* .line 42 */
/* .local v4, "pInfo":Landroid/content/pm/PackageInfo; */
v5 = this.applicationInfo;
/* .line 43 */
/* .local v5, "appInfo":Landroid/content/pm/ApplicationInfo; */
v6 = this.storageUuid;
v7 = this.packageName;
/* iget v8, v5, Landroid/content/pm/ApplicationInfo;->uid:I */
/* .line 44 */
android.os.UserHandle .getUserHandleForUid ( v8 );
/* .line 43 */
(( android.app.usage.StorageStatsManager ) v0 ).queryStatsForPackage ( v6, v7, v8 ); // invoke-virtual {v0, v6, v7, v8}, Landroid/app/usage/StorageStatsManager;->queryStatsForPackage(Ljava/util/UUID;Ljava/lang/String;Landroid/os/UserHandle;)Landroid/app/usage/StorageStats;
/* .line 45 */
/* .local v6, "stats":Landroid/app/usage/StorageStats; */
/* new-instance v7, Landroid/content/pm/PackageStats; */
/* iget v8, v5, Landroid/content/pm/ApplicationInfo;->uid:I */
/* invoke-direct {v7, p2, v8}, Landroid/content/pm/PackageStats;-><init>(Ljava/lang/String;I)V */
/* .line 46 */
/* .local v7, "legacy":Landroid/content/pm/PackageStats; */
(( android.app.usage.StorageStats ) v6 ).getAppBytes ( ); // invoke-virtual {v6}, Landroid/app/usage/StorageStats;->getAppBytes()J
/* move-result-wide v8 */
/* iput-wide v8, v7, Landroid/content/pm/PackageStats;->codeSize:J */
/* .line 47 */
(( android.app.usage.StorageStats ) v6 ).getDataBytes ( ); // invoke-virtual {v6}, Landroid/app/usage/StorageStats;->getDataBytes()J
/* move-result-wide v8 */
/* iput-wide v8, v7, Landroid/content/pm/PackageStats;->dataSize:J */
/* .line 48 */
(( android.app.usage.StorageStats ) v6 ).getCacheBytes ( ); // invoke-virtual {v6}, Landroid/app/usage/StorageStats;->getCacheBytes()J
/* move-result-wide v8 */
/* iput-wide v8, v7, Landroid/content/pm/PackageStats;->cacheSize:J */
/* :try_end_0 */
/* .catch Ljava/lang/Exception; {:try_start_0 ..:try_end_0} :catch_1 */
/* .catchall {:try_start_0 ..:try_end_0} :catchall_0 */
/* .line 50 */
int v8 = 1; // const/4 v8, 0x1
try { // :try_start_1
	 /* :try_end_1 */
	 /* .catch Landroid/os/RemoteException; {:try_start_1 ..:try_end_1} :catch_0 */
	 /* .catch Ljava/lang/Exception; {:try_start_1 ..:try_end_1} :catch_1 */
	 /* .catchall {:try_start_1 ..:try_end_1} :catchall_0 */
	 /* .line 52 */
	 /* .line 51 */
	 /* :catch_0 */
	 /* move-exception v3 */
	 /* .line 61 */
} // .end local v4 # "pInfo":Landroid/content/pm/PackageInfo;
} // .end local v5 # "appInfo":Landroid/content/pm/ApplicationInfo;
} // .end local v6 # "stats":Landroid/app/usage/StorageStats;
} // .end local v7 # "legacy":Landroid/content/pm/PackageStats;
/* :catchall_0 */
/* move-exception v3 */
/* .line 53 */
/* :catch_1 */
/* move-exception v4 */
/* .line 54 */
/* .local v4, "e":Ljava/lang/Exception; */
try { // :try_start_2
final String v5 = "BackupManagerServiceProxy"; // const-string v5, "BackupManagerServiceProxy"
final String v6 = "getPackageSizeInfo error"; // const-string v6, "getPackageSizeInfo error"
android.util.Slog .e ( v5,v6,v4 );
/* :try_end_2 */
/* .catchall {:try_start_2 ..:try_end_2} :catchall_0 */
/* .line 56 */
try { // :try_start_3
/* new-instance v5, Landroid/content/pm/PackageStats; */
/* invoke-direct {v5, p2, p3}, Landroid/content/pm/PackageStats;-><init>(Ljava/lang/String;I)V */
/* .line 57 */
/* .local v5, "legacy":Landroid/content/pm/PackageStats; */
/* :try_end_3 */
/* .catch Landroid/os/RemoteException; {:try_start_3 ..:try_end_3} :catch_2 */
/* .catchall {:try_start_3 ..:try_end_3} :catchall_0 */
/* .line 59 */
} // .end local v5 # "legacy":Landroid/content/pm/PackageStats;
/* .line 58 */
/* :catch_2 */
/* move-exception v3 */
/* .line 61 */
} // .end local v4 # "e":Ljava/lang/Exception;
} // :goto_0
android.os.Binder .restoreCallingIdentity ( v1,v2 );
/* .line 62 */
/* nop */
/* .line 63 */
return;
/* .line 61 */
} // :goto_1
android.os.Binder .restoreCallingIdentity ( v1,v2 );
/* .line 62 */
/* throw v3 */
} // .end method
public static Boolean isPackageStateProtected ( android.content.pm.PackageManager p0, java.lang.String p1, Integer p2 ) {
/* .locals 3 */
/* .param p0, "pm" # Landroid/content/pm/PackageManager; */
/* .param p1, "packageName" # Ljava/lang/String; */
/* .param p2, "userId" # I */
/* .line 66 */
int v0 = 0; // const/4 v0, 0x0
/* .line 68 */
/* .local v0, "isProtected":Z */
try { // :try_start_0
v1 = (( android.content.pm.PackageManager ) p0 ).isPackageStateProtected ( p1, p2 ); // invoke-virtual {p0, p1, p2}, Landroid/content/pm/PackageManager;->isPackageStateProtected(Ljava/lang/String;I)Z
/* :try_end_0 */
/* .catch Ljava/lang/UnsupportedOperationException; {:try_start_0 ..:try_end_0} :catch_0 */
/* move v0, v1 */
/* .line 71 */
/* .line 69 */
/* :catch_0 */
/* move-exception v1 */
/* .line 70 */
/* .local v1, "e":Ljava/lang/UnsupportedOperationException; */
(( java.lang.UnsupportedOperationException ) v1 ).printStackTrace ( ); // invoke-virtual {v1}, Ljava/lang/UnsupportedOperationException;->printStackTrace()V
/* .line 72 */
} // .end local v1 # "e":Ljava/lang/UnsupportedOperationException;
} // :goto_0
/* new-instance v1, Ljava/lang/StringBuilder; */
/* invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V */
final String v2 = "isPackageStateProtected, packageName:"; // const-string v2, "isPackageStateProtected, packageName:"
(( java.lang.StringBuilder ) v1 ).append ( v2 ); // invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v1 ).append ( p1 ); // invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
final String v2 = " userId:"; // const-string v2, " userId:"
(( java.lang.StringBuilder ) v1 ).append ( v2 ); // invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v1 ).append ( p2 ); // invoke-virtual {v1, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;
final String v2 = " isProtected:"; // const-string v2, " isProtected:"
(( java.lang.StringBuilder ) v1 ).append ( v2 ); // invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v1 ).append ( v0 ); // invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v1 ).toString ( ); // invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
final String v2 = "BackupManagerServiceProxy"; // const-string v2, "BackupManagerServiceProxy"
android.util.Slog .d ( v2,v1 );
/* .line 74 */
} // .end method
