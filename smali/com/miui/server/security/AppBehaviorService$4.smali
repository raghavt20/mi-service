.class Lcom/miui/server/security/AppBehaviorService$4;
.super Ljava/lang/Object;
.source "AppBehaviorService.java"

# interfaces
.implements Landroid/content/ServiceConnection;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/miui/server/security/AppBehaviorService;->persistBehaviorRecord()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/miui/server/security/AppBehaviorService;


# direct methods
.method constructor <init>(Lcom/miui/server/security/AppBehaviorService;)V
    .locals 0
    .param p1, "this$0"    # Lcom/miui/server/security/AppBehaviorService;

    .line 276
    iput-object p1, p0, Lcom/miui/server/security/AppBehaviorService$4;->this$0:Lcom/miui/server/security/AppBehaviorService;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onServiceConnected(Landroid/content/ComponentName;Landroid/os/IBinder;)V
    .locals 2
    .param p1, "name"    # Landroid/content/ComponentName;
    .param p2, "service"    # Landroid/os/IBinder;

    .line 279
    const-string v0, "AppBehaviorService"

    const-string v1, "on persistBehaviorRecord ServiceConnected"

    invoke-static {v0, v1}, Landroid/util/Slog;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 280
    iget-object v0, p0, Lcom/miui/server/security/AppBehaviorService$4;->this$0:Lcom/miui/server/security/AppBehaviorService;

    new-instance v1, Landroid/os/Messenger;

    invoke-direct {v1, p2}, Landroid/os/Messenger;-><init>(Landroid/os/IBinder;)V

    invoke-static {v0, v1}, Lcom/miui/server/security/AppBehaviorService;->-$$Nest$fputmPersistMessenger(Lcom/miui/server/security/AppBehaviorService;Landroid/os/Messenger;)V

    .line 281
    iget-object v0, p0, Lcom/miui/server/security/AppBehaviorService$4;->this$0:Lcom/miui/server/security/AppBehaviorService;

    invoke-static {v0}, Lcom/miui/server/security/AppBehaviorService;->-$$Nest$mschedulePersist(Lcom/miui/server/security/AppBehaviorService;)V

    .line 282
    iget-object v0, p0, Lcom/miui/server/security/AppBehaviorService$4;->this$0:Lcom/miui/server/security/AppBehaviorService;

    invoke-static {v0}, Lcom/miui/server/security/AppBehaviorService;->-$$Nest$fgetmWorkHandler(Lcom/miui/server/security/AppBehaviorService;)Landroid/os/Handler;

    move-result-object v0

    const/16 v1, 0xe15

    invoke-virtual {v0, v1}, Landroid/os/Handler;->removeMessages(I)V

    .line 283
    return-void
.end method

.method public onServiceDisconnected(Landroid/content/ComponentName;)V
    .locals 2
    .param p1, "name"    # Landroid/content/ComponentName;

    .line 287
    iget-object v0, p0, Lcom/miui/server/security/AppBehaviorService$4;->this$0:Lcom/miui/server/security/AppBehaviorService;

    const/4 v1, 0x0

    invoke-static {v0, v1}, Lcom/miui/server/security/AppBehaviorService;->-$$Nest$fputmPersistMessenger(Lcom/miui/server/security/AppBehaviorService;Landroid/os/Messenger;)V

    .line 288
    return-void
.end method
