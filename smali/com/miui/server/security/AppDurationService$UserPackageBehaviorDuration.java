class com.miui.server.security.AppDurationService$UserPackageBehaviorDuration {
	 /* .source "AppDurationService.java" */
	 /* # annotations */
	 /* .annotation system Ldalvik/annotation/EnclosingClass; */
	 /* value = Lcom/miui/server/security/AppDurationService; */
} // .end annotation
/* .annotation system Ldalvik/annotation/InnerClass; */
/* accessFlags = 0xa */
/* name = "UserPackageBehaviorDuration" */
} // .end annotation
/* # instance fields */
private android.util.SparseArray behaviorDurationMap;
/* .annotation system Ldalvik/annotation/Signature; */
/* value = { */
/* "Landroid/util/SparseArray<", */
/* "Lcom/miui/server/security/AppDurationService$TokenAndStartTime;", */
/* ">;" */
/* } */
} // .end annotation
} // .end field
/* # direct methods */
static android.util.SparseArray -$$Nest$fgetbehaviorDurationMap ( com.miui.server.security.AppDurationService$UserPackageBehaviorDuration p0 ) { //bridge//synthethic
/* .locals 0 */
p0 = this.behaviorDurationMap;
} // .end method
private com.miui.server.security.AppDurationService$UserPackageBehaviorDuration ( ) {
/* .locals 0 */
/* .line 150 */
/* invoke-direct {p0}, Ljava/lang/Object;-><init>()V */
return;
} // .end method
 com.miui.server.security.AppDurationService$UserPackageBehaviorDuration ( ) { //synthethic
/* .locals 0 */
/* invoke-direct {p0}, Lcom/miui/server/security/AppDurationService$UserPackageBehaviorDuration;-><init>()V */
return;
} // .end method
/* # virtual methods */
public Boolean recordBehavior ( Integer p0, android.os.IBinder p1, Integer p2 ) {
/* .locals 3 */
/* .param p1, "behavior" # I */
/* .param p2, "binder" # Landroid/os/IBinder; */
/* .param p3, "uid" # I */
/* .line 154 */
v0 = this.behaviorDurationMap;
/* if-nez v0, :cond_0 */
/* .line 155 */
/* new-instance v0, Landroid/util/SparseArray; */
/* invoke-direct {v0}, Landroid/util/SparseArray;-><init>()V */
this.behaviorDurationMap = v0;
/* .line 157 */
} // :cond_0
v0 = this.behaviorDurationMap;
(( android.util.SparseArray ) v0 ).get ( p1 ); // invoke-virtual {v0, p1}, Landroid/util/SparseArray;->get(I)Ljava/lang/Object;
/* check-cast v0, Lcom/miui/server/security/AppDurationService$TokenAndStartTime; */
/* .line 158 */
/* .local v0, "tokenAndStartTime":Lcom/miui/server/security/AppDurationService$TokenAndStartTime; */
/* if-nez v0, :cond_1 */
/* .line 159 */
/* new-instance v1, Lcom/miui/server/security/AppDurationService$TokenAndStartTime; */
int v2 = 0; // const/4 v2, 0x0
/* invoke-direct {v1, v2}, Lcom/miui/server/security/AppDurationService$TokenAndStartTime;-><init>(Lcom/miui/server/security/AppDurationService$TokenAndStartTime-IA;)V */
/* move-object v0, v1 */
/* .line 160 */
v1 = this.behaviorDurationMap;
(( android.util.SparseArray ) v1 ).put ( p1, v0 ); // invoke-virtual {v1, p1, v0}, Landroid/util/SparseArray;->put(ILjava/lang/Object;)V
/* .line 162 */
} // :cond_1
if ( p2 != null) { // if-eqz p2, :cond_2
/* .line 163 */
v1 = (( com.miui.server.security.AppDurationService$TokenAndStartTime ) v0 ).recordBinderTime ( p2 ); // invoke-virtual {v0, p2}, Lcom/miui/server/security/AppDurationService$TokenAndStartTime;->recordBinderTime(Landroid/os/IBinder;)Z
/* .line 165 */
} // :cond_2
v1 = (( com.miui.server.security.AppDurationService$TokenAndStartTime ) v0 ).recordUidTime ( p3 ); // invoke-virtual {v0, p3}, Lcom/miui/server/security/AppDurationService$TokenAndStartTime;->recordUidTime(I)Z
} // .end method
public Long stopRecordDuration ( Integer p0, android.os.IBinder p1, Integer p2 ) {
/* .locals 3 */
/* .param p1, "behavior" # I */
/* .param p2, "binder" # Landroid/os/IBinder; */
/* .param p3, "uid" # I */
/* .line 170 */
v0 = this.behaviorDurationMap;
/* const-wide/16 v1, 0x0 */
if ( v0 != null) { // if-eqz v0, :cond_3
/* .line 171 */
v0 = (( android.util.SparseArray ) v0 ).size ( ); // invoke-virtual {v0}, Landroid/util/SparseArray;->size()I
/* if-nez v0, :cond_0 */
/* .line 174 */
} // :cond_0
v0 = this.behaviorDurationMap;
(( android.util.SparseArray ) v0 ).get ( p1 ); // invoke-virtual {v0, p1}, Landroid/util/SparseArray;->get(I)Ljava/lang/Object;
/* check-cast v0, Lcom/miui/server/security/AppDurationService$TokenAndStartTime; */
/* .line 175 */
/* .local v0, "tokenAndStartTime":Lcom/miui/server/security/AppDurationService$TokenAndStartTime; */
/* if-nez v0, :cond_1 */
/* .line 176 */
/* return-wide v1 */
/* .line 178 */
} // :cond_1
if ( p2 != null) { // if-eqz p2, :cond_2
(( com.miui.server.security.AppDurationService$TokenAndStartTime ) v0 ).stopBinderTime ( p2 ); // invoke-virtual {v0, p2}, Lcom/miui/server/security/AppDurationService$TokenAndStartTime;->stopBinderTime(Landroid/os/IBinder;)J
/* move-result-wide v1 */
/* .line 179 */
} // :cond_2
(( com.miui.server.security.AppDurationService$TokenAndStartTime ) v0 ).stopUidTime ( p3 ); // invoke-virtual {v0, p3}, Lcom/miui/server/security/AppDurationService$TokenAndStartTime;->stopUidTime(I)J
/* move-result-wide v1 */
/* .line 178 */
} // :goto_0
/* return-wide v1 */
/* .line 172 */
} // .end local v0 # "tokenAndStartTime":Lcom/miui/server/security/AppDurationService$TokenAndStartTime;
} // :cond_3
} // :goto_1
/* return-wide v1 */
} // .end method
