.class public Lcom/miui/server/security/SecurityPackageSettings;
.super Ljava/lang/Object;
.source "SecurityPackageSettings.java"


# instance fields
.field public accessControl:Z

.field public childrenControl:Z

.field public isDarkModeChecked:Z

.field public isGameStorageApp:Z

.field public isPrivacyApp:Z

.field public isRelaunchWhenFolded:Z

.field public isRemindForRelaunch:Z

.field public isScRelaunchConfirm:Z

.field public maskNotification:Z

.field public name:Ljava/lang/String;


# direct methods
.method public constructor <init>(Ljava/lang/String;)V
    .locals 2
    .param p1, "name"    # Ljava/lang/String;

    .line 18
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 19
    iput-object p1, p0, Lcom/miui/server/security/SecurityPackageSettings;->name:Ljava/lang/String;

    .line 20
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/miui/server/security/SecurityPackageSettings;->accessControl:Z

    .line 21
    iput-boolean v0, p0, Lcom/miui/server/security/SecurityPackageSettings;->childrenControl:Z

    .line 22
    iput-boolean v0, p0, Lcom/miui/server/security/SecurityPackageSettings;->maskNotification:Z

    .line 23
    iput-boolean v0, p0, Lcom/miui/server/security/SecurityPackageSettings;->isPrivacyApp:Z

    .line 24
    invoke-static {}, Lcom/android/server/MiuiUiModeManagerServiceStub;->getInstance()Lcom/android/server/MiuiUiModeManagerServiceStub;

    move-result-object v1

    invoke-virtual {v1, p1}, Lcom/android/server/MiuiUiModeManagerServiceStub;->getForceDarkAppDefaultEnable(Ljava/lang/String;)Z

    move-result v1

    iput-boolean v1, p0, Lcom/miui/server/security/SecurityPackageSettings;->isDarkModeChecked:Z

    .line 25
    iput-boolean v0, p0, Lcom/miui/server/security/SecurityPackageSettings;->isGameStorageApp:Z

    .line 26
    const/4 v1, 0x1

    iput-boolean v1, p0, Lcom/miui/server/security/SecurityPackageSettings;->isRemindForRelaunch:Z

    .line 27
    iput-boolean v0, p0, Lcom/miui/server/security/SecurityPackageSettings;->isRelaunchWhenFolded:Z

    .line 28
    iput-boolean v1, p0, Lcom/miui/server/security/SecurityPackageSettings;->isScRelaunchConfirm:Z

    .line 29
    return-void
.end method
