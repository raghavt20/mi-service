.class public Lcom/miui/server/security/GameBoosterImpl;
.super Ljava/lang/Object;
.source "GameBoosterImpl.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/miui/server/security/GameBoosterImpl$GameBoosterServiceDeath;
    }
.end annotation


# static fields
.field private static final TAG:Ljava/lang/String; = "GameBoosterImpl"


# instance fields
.field private final mService:Lcom/miui/server/SecurityManagerService;


# direct methods
.method static bridge synthetic -$$Nest$fgetmService(Lcom/miui/server/security/GameBoosterImpl;)Lcom/miui/server/SecurityManagerService;
    .locals 0

    iget-object p0, p0, Lcom/miui/server/security/GameBoosterImpl;->mService:Lcom/miui/server/SecurityManagerService;

    return-object p0
.end method

.method public constructor <init>(Lcom/miui/server/SecurityManagerService;)V
    .locals 0
    .param p1, "service"    # Lcom/miui/server/SecurityManagerService;

    .line 21
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 22
    iput-object p1, p0, Lcom/miui/server/security/GameBoosterImpl;->mService:Lcom/miui/server/SecurityManagerService;

    .line 23
    return-void
.end method


# virtual methods
.method public getAllGameStorageApps(I)Ljava/util/List;
    .locals 10
    .param p1, "userId"    # I
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(I)",
            "Ljava/util/List<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .line 84
    iget-object v0, p0, Lcom/miui/server/security/GameBoosterImpl;->mService:Lcom/miui/server/SecurityManagerService;

    iget-object v0, v0, Lcom/miui/server/SecurityManagerService;->mUserStateLock:Ljava/lang/Object;

    monitor-enter v0

    .line 85
    :try_start_0
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    .line 86
    .local v1, "storageAppsList":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;"
    iget-object v2, p0, Lcom/miui/server/security/GameBoosterImpl;->mService:Lcom/miui/server/SecurityManagerService;

    invoke-virtual {v2, p1}, Lcom/miui/server/SecurityManagerService;->getUserStateLocked(I)Lcom/miui/server/security/SecurityUserState;

    move-result-object v2

    .line 87
    .local v2, "userState":Lcom/miui/server/security/SecurityUserState;
    iget-object v3, v2, Lcom/miui/server/security/SecurityUserState;->mPackages:Ljava/util/HashMap;

    .line 88
    .local v3, "packages":Ljava/util/HashMap;, "Ljava/util/HashMap<Ljava/lang/String;Lcom/miui/server/security/SecurityPackageSettings;>;"
    invoke-virtual {v3}, Ljava/util/HashMap;->keySet()Ljava/util/Set;

    move-result-object v4

    .line 89
    .local v4, "pkgNames":Ljava/util/Set;, "Ljava/util/Set<Ljava/lang/String;>;"
    invoke-interface {v4}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v5

    :goto_0
    invoke-interface {v5}, Ljava/util/Iterator;->hasNext()Z

    move-result v6

    if-eqz v6, :cond_1

    invoke-interface {v5}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v6

    check-cast v6, Ljava/lang/String;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 91
    .local v6, "pkgName":Ljava/lang/String;
    :try_start_1
    iget-object v7, p0, Lcom/miui/server/security/GameBoosterImpl;->mService:Lcom/miui/server/SecurityManagerService;

    iget-object v8, v2, Lcom/miui/server/security/SecurityUserState;->mPackages:Ljava/util/HashMap;

    invoke-virtual {v7, v8, v6}, Lcom/miui/server/SecurityManagerService;->getPackageSetting(Ljava/util/HashMap;Ljava/lang/String;)Lcom/miui/server/security/SecurityPackageSettings;

    move-result-object v7

    .line 93
    .local v7, "ps":Lcom/miui/server/security/SecurityPackageSettings;
    iget-boolean v8, v7, Lcom/miui/server/security/SecurityPackageSettings;->isGameStorageApp:Z

    if-eqz v8, :cond_0

    .line 94
    invoke-interface {v1, v6}, Ljava/util/List;->add(Ljava/lang/Object;)Z
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_0
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 98
    .end local v7    # "ps":Lcom/miui/server/security/SecurityPackageSettings;
    :cond_0
    goto :goto_1

    .line 96
    :catch_0
    move-exception v7

    .line 97
    .local v7, "e":Ljava/lang/Exception;
    :try_start_2
    const-string v8, "GameBoosterImpl"

    const-string v9, "get game storage all apps failed"

    invoke-static {v8, v9, v7}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    .line 99
    .end local v6    # "pkgName":Ljava/lang/String;
    .end local v7    # "e":Ljava/lang/Exception;
    :goto_1
    goto :goto_0

    .line 100
    :cond_1
    monitor-exit v0

    return-object v1

    .line 101
    .end local v1    # "storageAppsList":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;"
    .end local v2    # "userState":Lcom/miui/server/security/SecurityUserState;
    .end local v3    # "packages":Ljava/util/HashMap;, "Ljava/util/HashMap<Ljava/lang/String;Lcom/miui/server/security/SecurityPackageSettings;>;"
    .end local v4    # "pkgNames":Ljava/util/Set;, "Ljava/util/Set<Ljava/lang/String;>;"
    :catchall_0
    move-exception v1

    monitor-exit v0
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    throw v1
.end method

.method public getGameMode(I)Z
    .locals 3
    .param p1, "userId"    # I

    .line 47
    iget-object v0, p0, Lcom/miui/server/security/GameBoosterImpl;->mService:Lcom/miui/server/SecurityManagerService;

    iget-object v0, v0, Lcom/miui/server/SecurityManagerService;->mUserStateLock:Ljava/lang/Object;

    monitor-enter v0

    .line 48
    :try_start_0
    invoke-static {p1}, Lmiui/security/SecurityManager;->getUserHandle(I)I

    move-result v1

    move p1, v1

    .line 49
    iget-object v1, p0, Lcom/miui/server/security/GameBoosterImpl;->mService:Lcom/miui/server/SecurityManagerService;

    invoke-virtual {v1, p1}, Lcom/miui/server/SecurityManagerService;->getUserStateLocked(I)Lcom/miui/server/security/SecurityUserState;

    move-result-object v1

    .line 50
    .local v1, "userState":Lcom/miui/server/security/SecurityUserState;
    iget-boolean v2, v1, Lcom/miui/server/security/SecurityUserState;->mIsGameMode:Z

    monitor-exit v0

    return v2

    .line 51
    .end local v1    # "userState":Lcom/miui/server/security/SecurityUserState;
    :catchall_0
    move-exception v1

    monitor-exit v0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v1
.end method

.method public isGameStorageApp(Ljava/lang/String;I)Z
    .locals 5
    .param p1, "packageName"    # Ljava/lang/String;
    .param p2, "userId"    # I

    .line 71
    iget-object v0, p0, Lcom/miui/server/security/GameBoosterImpl;->mService:Lcom/miui/server/SecurityManagerService;

    iget-object v0, v0, Lcom/miui/server/SecurityManagerService;->mUserStateLock:Ljava/lang/Object;

    monitor-enter v0

    .line 72
    :try_start_0
    iget-object v1, p0, Lcom/miui/server/security/GameBoosterImpl;->mService:Lcom/miui/server/SecurityManagerService;

    invoke-virtual {v1, p2}, Lcom/miui/server/SecurityManagerService;->getUserStateLocked(I)Lcom/miui/server/security/SecurityUserState;

    move-result-object v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 74
    .local v1, "userState":Lcom/miui/server/security/SecurityUserState;
    :try_start_1
    iget-object v2, p0, Lcom/miui/server/security/GameBoosterImpl;->mService:Lcom/miui/server/SecurityManagerService;

    iget-object v3, v1, Lcom/miui/server/security/SecurityUserState;->mPackages:Ljava/util/HashMap;

    invoke-virtual {v2, v3, p1}, Lcom/miui/server/SecurityManagerService;->getPackageSetting(Ljava/util/HashMap;Ljava/lang/String;)Lcom/miui/server/security/SecurityPackageSettings;

    move-result-object v2

    .line 75
    .local v2, "ps":Lcom/miui/server/security/SecurityPackageSettings;
    iget-boolean v3, v2, Lcom/miui/server/security/SecurityPackageSettings;->isGameStorageApp:Z
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_0
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    :try_start_2
    monitor-exit v0

    return v3

    .line 76
    .end local v2    # "ps":Lcom/miui/server/security/SecurityPackageSettings;
    :catch_0
    move-exception v2

    .line 77
    .local v2, "e":Ljava/lang/Exception;
    const-string v3, "GameBoosterImpl"

    const-string v4, "get app is game storage failed"

    invoke-static {v3, v4, v2}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    .line 78
    monitor-exit v0

    const/4 v0, 0x0

    return v0

    .line 80
    .end local v1    # "userState":Lcom/miui/server/security/SecurityUserState;
    .end local v2    # "e":Ljava/lang/Exception;
    :catchall_0
    move-exception v1

    monitor-exit v0
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    throw v1
.end method

.method public isVtbMode(Landroid/content/Context;)Z
    .locals 5
    .param p1, "context"    # Landroid/content/Context;

    .line 55
    iget-object v0, p0, Lcom/miui/server/security/GameBoosterImpl;->mService:Lcom/miui/server/SecurityManagerService;

    iget-object v0, v0, Lcom/miui/server/SecurityManagerService;->mUserStateLock:Ljava/lang/Object;

    monitor-enter v0

    .line 56
    :try_start_0
    invoke-virtual {p1}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v1

    const-string/jumbo v2, "vtb_boosting"

    const/4 v3, -0x2

    const/4 v4, 0x0

    invoke-static {v1, v2, v4, v3}, Landroid/provider/Settings$Secure;->getIntForUser(Landroid/content/ContentResolver;Ljava/lang/String;II)I

    move-result v1

    const/4 v2, 0x1

    if-ne v1, v2, :cond_0

    move v4, v2

    :cond_0
    monitor-exit v0

    return v4

    .line 58
    :catchall_0
    move-exception v1

    monitor-exit v0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v1
.end method

.method public setGameBoosterIBinder(Landroid/os/IBinder;IZ)V
    .locals 5
    .param p1, "gameBooster"    # Landroid/os/IBinder;
    .param p2, "userId"    # I
    .param p3, "isGameMode"    # Z

    .line 26
    iget-object v0, p0, Lcom/miui/server/security/GameBoosterImpl;->mService:Lcom/miui/server/SecurityManagerService;

    iget-object v0, v0, Lcom/miui/server/SecurityManagerService;->mUserStateLock:Ljava/lang/Object;

    monitor-enter v0

    .line 27
    :try_start_0
    invoke-static {p2}, Lmiui/security/SecurityManager;->getUserHandle(I)I

    move-result v1

    move p2, v1

    .line 28
    iget-object v1, p0, Lcom/miui/server/security/GameBoosterImpl;->mService:Lcom/miui/server/SecurityManagerService;

    invoke-virtual {v1, p2}, Lcom/miui/server/SecurityManagerService;->getUserStateLocked(I)Lcom/miui/server/security/SecurityUserState;

    move-result-object v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 30
    .local v1, "userState":Lcom/miui/server/security/SecurityUserState;
    :try_start_1
    iget-object v2, v1, Lcom/miui/server/security/SecurityUserState;->gameBoosterServiceDeath:Lcom/miui/server/security/GameBoosterImpl$GameBoosterServiceDeath;

    const/4 v3, 0x0

    if-nez v2, :cond_0

    .line 31
    new-instance v2, Lcom/miui/server/security/GameBoosterImpl$GameBoosterServiceDeath;

    invoke-direct {v2, p0, v1, p1}, Lcom/miui/server/security/GameBoosterImpl$GameBoosterServiceDeath;-><init>(Lcom/miui/server/security/GameBoosterImpl;Lcom/miui/server/security/SecurityUserState;Landroid/os/IBinder;)V

    iput-object v2, v1, Lcom/miui/server/security/SecurityUserState;->gameBoosterServiceDeath:Lcom/miui/server/security/GameBoosterImpl$GameBoosterServiceDeath;

    .line 32
    iget-object v2, v1, Lcom/miui/server/security/SecurityUserState;->gameBoosterServiceDeath:Lcom/miui/server/security/GameBoosterImpl$GameBoosterServiceDeath;

    invoke-interface {p1, v2, v3}, Landroid/os/IBinder;->linkToDeath(Landroid/os/IBinder$DeathRecipient;I)V

    goto :goto_0

    .line 33
    :cond_0
    iget-object v2, v1, Lcom/miui/server/security/SecurityUserState;->gameBoosterServiceDeath:Lcom/miui/server/security/GameBoosterImpl$GameBoosterServiceDeath;

    iget-object v2, v2, Lcom/miui/server/security/GameBoosterImpl$GameBoosterServiceDeath;->mGameBoosterService:Landroid/os/IBinder;

    if-eq p1, v2, :cond_1

    .line 34
    iget-object v2, v1, Lcom/miui/server/security/SecurityUserState;->gameBoosterServiceDeath:Lcom/miui/server/security/GameBoosterImpl$GameBoosterServiceDeath;

    iget-object v2, v2, Lcom/miui/server/security/GameBoosterImpl$GameBoosterServiceDeath;->mGameBoosterService:Landroid/os/IBinder;

    iget-object v4, v1, Lcom/miui/server/security/SecurityUserState;->gameBoosterServiceDeath:Lcom/miui/server/security/GameBoosterImpl$GameBoosterServiceDeath;

    invoke-interface {v2, v4, v3}, Landroid/os/IBinder;->unlinkToDeath(Landroid/os/IBinder$DeathRecipient;I)Z

    .line 36
    new-instance v2, Lcom/miui/server/security/GameBoosterImpl$GameBoosterServiceDeath;

    invoke-direct {v2, p0, v1, p1}, Lcom/miui/server/security/GameBoosterImpl$GameBoosterServiceDeath;-><init>(Lcom/miui/server/security/GameBoosterImpl;Lcom/miui/server/security/SecurityUserState;Landroid/os/IBinder;)V

    iput-object v2, v1, Lcom/miui/server/security/SecurityUserState;->gameBoosterServiceDeath:Lcom/miui/server/security/GameBoosterImpl$GameBoosterServiceDeath;

    .line 37
    iget-object v2, v1, Lcom/miui/server/security/SecurityUserState;->gameBoosterServiceDeath:Lcom/miui/server/security/GameBoosterImpl$GameBoosterServiceDeath;

    invoke-interface {p1, v2, v3}, Landroid/os/IBinder;->linkToDeath(Landroid/os/IBinder$DeathRecipient;I)V

    .line 39
    :cond_1
    :goto_0
    iput-boolean p3, v1, Lcom/miui/server/security/SecurityUserState;->mIsGameMode:Z
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_0
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 42
    goto :goto_1

    .line 40
    :catch_0
    move-exception v2

    .line 41
    .local v2, "e":Ljava/lang/Exception;
    :try_start_2
    const-string v3, "GameBoosterImpl"

    const-string/jumbo v4, "setGameBoosterIBinder"

    invoke-static {v3, v4, v2}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    .line 43
    .end local v1    # "userState":Lcom/miui/server/security/SecurityUserState;
    .end local v2    # "e":Ljava/lang/Exception;
    :goto_1
    monitor-exit v0

    .line 44
    return-void

    .line 43
    :catchall_0
    move-exception v1

    monitor-exit v0
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    throw v1
.end method

.method public setGameStorageApp(Ljava/lang/String;IZ)V
    .locals 4
    .param p1, "packageName"    # Ljava/lang/String;
    .param p2, "userId"    # I
    .param p3, "isStorage"    # Z

    .line 62
    iget-object v0, p0, Lcom/miui/server/security/GameBoosterImpl;->mService:Lcom/miui/server/SecurityManagerService;

    iget-object v0, v0, Lcom/miui/server/SecurityManagerService;->mUserStateLock:Ljava/lang/Object;

    monitor-enter v0

    .line 63
    :try_start_0
    iget-object v1, p0, Lcom/miui/server/security/GameBoosterImpl;->mService:Lcom/miui/server/SecurityManagerService;

    invoke-virtual {v1, p2}, Lcom/miui/server/SecurityManagerService;->getUserStateLocked(I)Lcom/miui/server/security/SecurityUserState;

    move-result-object v1

    .line 64
    .local v1, "userStateLocked":Lcom/miui/server/security/SecurityUserState;
    iget-object v2, p0, Lcom/miui/server/security/GameBoosterImpl;->mService:Lcom/miui/server/SecurityManagerService;

    iget-object v3, v1, Lcom/miui/server/security/SecurityUserState;->mPackages:Ljava/util/HashMap;

    invoke-virtual {v2, v3, p1}, Lcom/miui/server/SecurityManagerService;->getPackageSetting(Ljava/util/HashMap;Ljava/lang/String;)Lcom/miui/server/security/SecurityPackageSettings;

    move-result-object v2

    .line 65
    .local v2, "ps":Lcom/miui/server/security/SecurityPackageSettings;
    iput-boolean p3, v2, Lcom/miui/server/security/SecurityPackageSettings;->isGameStorageApp:Z

    .line 66
    iget-object v3, p0, Lcom/miui/server/security/GameBoosterImpl;->mService:Lcom/miui/server/SecurityManagerService;

    invoke-virtual {v3}, Lcom/miui/server/SecurityManagerService;->scheduleWriteSettings()V

    .line 67
    .end local v1    # "userStateLocked":Lcom/miui/server/security/SecurityUserState;
    .end local v2    # "ps":Lcom/miui/server/security/SecurityPackageSettings;
    monitor-exit v0

    .line 68
    return-void

    .line 67
    :catchall_0
    move-exception v1

    monitor-exit v0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v1
.end method
