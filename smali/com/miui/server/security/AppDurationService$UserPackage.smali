.class Lcom/miui/server/security/AppDurationService$UserPackage;
.super Ljava/lang/Object;
.source "AppDurationService.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/miui/server/security/AppDurationService;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0xa
    name = "UserPackage"
.end annotation


# instance fields
.field private final pkgName:Ljava/lang/String;

.field private final uid:I


# direct methods
.method static bridge synthetic -$$Nest$fgetpkgName(Lcom/miui/server/security/AppDurationService$UserPackage;)Ljava/lang/String;
    .locals 0

    iget-object p0, p0, Lcom/miui/server/security/AppDurationService$UserPackage;->pkgName:Ljava/lang/String;

    return-object p0
.end method

.method public constructor <init>(ILjava/lang/String;)V
    .locals 0
    .param p1, "uid"    # I
    .param p2, "pkgName"    # Ljava/lang/String;

    .line 131
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 132
    iput p1, p0, Lcom/miui/server/security/AppDurationService$UserPackage;->uid:I

    .line 133
    iput-object p2, p0, Lcom/miui/server/security/AppDurationService$UserPackage;->pkgName:Ljava/lang/String;

    .line 134
    return-void
.end method


# virtual methods
.method public equals(Ljava/lang/Object;)Z
    .locals 5
    .param p1, "o"    # Ljava/lang/Object;

    .line 138
    const/4 v0, 0x1

    if-ne p0, p1, :cond_0

    return v0

    .line 139
    :cond_0
    const/4 v1, 0x0

    if-eqz p1, :cond_3

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v2

    invoke-virtual {p1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v3

    if-eq v2, v3, :cond_1

    goto :goto_1

    .line 140
    :cond_1
    move-object v2, p1

    check-cast v2, Lcom/miui/server/security/AppDurationService$UserPackage;

    .line 141
    .local v2, "that":Lcom/miui/server/security/AppDurationService$UserPackage;
    iget v3, p0, Lcom/miui/server/security/AppDurationService$UserPackage;->uid:I

    iget v4, v2, Lcom/miui/server/security/AppDurationService$UserPackage;->uid:I

    if-ne v3, v4, :cond_2

    iget-object v3, p0, Lcom/miui/server/security/AppDurationService$UserPackage;->pkgName:Ljava/lang/String;

    iget-object v4, v2, Lcom/miui/server/security/AppDurationService$UserPackage;->pkgName:Ljava/lang/String;

    invoke-static {v3, v4}, Ljava/util/Objects;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_2

    goto :goto_0

    :cond_2
    move v0, v1

    :goto_0
    return v0

    .line 139
    .end local v2    # "that":Lcom/miui/server/security/AppDurationService$UserPackage;
    :cond_3
    :goto_1
    return v1
.end method

.method public hashCode()I
    .locals 2

    .line 146
    iget v0, p0, Lcom/miui/server/security/AppDurationService$UserPackage;->uid:I

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    iget-object v1, p0, Lcom/miui/server/security/AppDurationService$UserPackage;->pkgName:Ljava/lang/String;

    filled-new-array {v0, v1}, [Ljava/lang/Object;

    move-result-object v0

    invoke-static {v0}, Ljava/util/Objects;->hash([Ljava/lang/Object;)I

    move-result v0

    return v0
.end method
