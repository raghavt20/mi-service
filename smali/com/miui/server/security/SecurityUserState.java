public class com.miui.server.security.SecurityUserState {
	 /* .source "SecurityUserState.java" */
	 /* # instance fields */
	 public com.miui.server.security.GameBoosterImpl$GameBoosterServiceDeath gameBoosterServiceDeath;
	 public final android.util.ArraySet mAccessControlCanceled;
	 /* .annotation system Ldalvik/annotation/Signature; */
	 /* value = { */
	 /* "Landroid/util/ArraySet<", */
	 /* "Ljava/lang/String;", */
	 /* ">;" */
	 /* } */
} // .end annotation
} // .end field
public Boolean mAccessControlEnabled;
public final android.util.ArrayMap mAccessControlLastCheck;
/* .annotation system Ldalvik/annotation/Signature; */
/* value = { */
/* "Landroid/util/ArrayMap<", */
/* "Ljava/lang/String;", */
/* "Ljava/lang/Long;", */
/* ">;" */
/* } */
} // .end annotation
} // .end field
public Boolean mAccessControlLockConvenient;
public Integer mAccessControlLockMode;
public final java.util.HashSet mAccessControlPassPackages;
/* .annotation system Ldalvik/annotation/Signature; */
/* value = { */
/* "Ljava/util/HashSet<", */
/* "Ljava/lang/String;", */
/* ">;" */
/* } */
} // .end annotation
} // .end field
public Boolean mAccessControlSettingInit;
public Boolean mIsGameMode;
public java.lang.String mLastResumePackage;
public final java.util.HashMap mPackages;
/* .annotation system Ldalvik/annotation/Signature; */
/* value = { */
/* "Ljava/util/HashMap<", */
/* "Ljava/lang/String;", */
/* "Lcom/miui/server/security/SecurityPackageSettings;", */
/* ">;" */
/* } */
} // .end annotation
} // .end field
public Integer userHandle;
/* # direct methods */
public com.miui.server.security.SecurityUserState ( ) {
/* .locals 1 */
/* .line 11 */
/* invoke-direct {p0}, Ljava/lang/Object;-><init>()V */
/* .line 14 */
/* new-instance v0, Ljava/util/HashSet; */
/* invoke-direct {v0}, Ljava/util/HashSet;-><init>()V */
this.mAccessControlPassPackages = v0;
/* .line 15 */
/* new-instance v0, Ljava/util/HashMap; */
/* invoke-direct {v0}, Ljava/util/HashMap;-><init>()V */
this.mPackages = v0;
/* .line 16 */
/* new-instance v0, Landroid/util/ArraySet; */
/* invoke-direct {v0}, Landroid/util/ArraySet;-><init>()V */
this.mAccessControlCanceled = v0;
/* .line 17 */
/* new-instance v0, Landroid/util/ArrayMap; */
/* invoke-direct {v0}, Landroid/util/ArrayMap;-><init>()V */
this.mAccessControlLastCheck = v0;
/* .line 35 */
int v0 = 0; // const/4 v0, 0x0
/* iput v0, p0, Lcom/miui/server/security/SecurityUserState;->mAccessControlLockMode:I */
return;
} // .end method
