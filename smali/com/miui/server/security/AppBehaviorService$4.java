class com.miui.server.security.AppBehaviorService$4 implements android.content.ServiceConnection {
	 /* .source "AppBehaviorService.java" */
	 /* # interfaces */
	 /* # annotations */
	 /* .annotation system Ldalvik/annotation/EnclosingMethod; */
	 /* value = Lcom/miui/server/security/AppBehaviorService;->persistBehaviorRecord()V */
} // .end annotation
/* .annotation system Ldalvik/annotation/InnerClass; */
/* accessFlags = 0x0 */
/* name = null */
} // .end annotation
/* # instance fields */
final com.miui.server.security.AppBehaviorService this$0; //synthetic
/* # direct methods */
 com.miui.server.security.AppBehaviorService$4 ( ) {
/* .locals 0 */
/* .param p1, "this$0" # Lcom/miui/server/security/AppBehaviorService; */
/* .line 276 */
this.this$0 = p1;
/* invoke-direct {p0}, Ljava/lang/Object;-><init>()V */
return;
} // .end method
/* # virtual methods */
public void onServiceConnected ( android.content.ComponentName p0, android.os.IBinder p1 ) {
/* .locals 2 */
/* .param p1, "name" # Landroid/content/ComponentName; */
/* .param p2, "service" # Landroid/os/IBinder; */
/* .line 279 */
final String v0 = "AppBehaviorService"; // const-string v0, "AppBehaviorService"
final String v1 = "on persistBehaviorRecord ServiceConnected"; // const-string v1, "on persistBehaviorRecord ServiceConnected"
android.util.Slog .i ( v0,v1 );
/* .line 280 */
v0 = this.this$0;
/* new-instance v1, Landroid/os/Messenger; */
/* invoke-direct {v1, p2}, Landroid/os/Messenger;-><init>(Landroid/os/IBinder;)V */
com.miui.server.security.AppBehaviorService .-$$Nest$fputmPersistMessenger ( v0,v1 );
/* .line 281 */
v0 = this.this$0;
com.miui.server.security.AppBehaviorService .-$$Nest$mschedulePersist ( v0 );
/* .line 282 */
v0 = this.this$0;
com.miui.server.security.AppBehaviorService .-$$Nest$fgetmWorkHandler ( v0 );
/* const/16 v1, 0xe15 */
(( android.os.Handler ) v0 ).removeMessages ( v1 ); // invoke-virtual {v0, v1}, Landroid/os/Handler;->removeMessages(I)V
/* .line 283 */
return;
} // .end method
public void onServiceDisconnected ( android.content.ComponentName p0 ) {
/* .locals 2 */
/* .param p1, "name" # Landroid/content/ComponentName; */
/* .line 287 */
v0 = this.this$0;
int v1 = 0; // const/4 v1, 0x0
com.miui.server.security.AppBehaviorService .-$$Nest$fputmPersistMessenger ( v0,v1 );
/* .line 288 */
return;
} // .end method
