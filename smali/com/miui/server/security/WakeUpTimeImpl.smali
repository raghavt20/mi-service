.class public Lcom/miui/server/security/WakeUpTimeImpl;
.super Ljava/lang/Object;
.source "WakeUpTimeImpl.java"


# static fields
.field private static final CLASS_NAME:Ljava/lang/String; = "classname"

.field private static final CLASS_NAMES:Ljava/lang/String; = "classnames"

.field private static final NAME:Ljava/lang/String; = "name"

.field private static final TAG:Ljava/lang/String; = "WakeUpTimeImpl"

.field private static final TIME:Ljava/lang/String; = "time"


# instance fields
.field private final mHandler:Landroid/os/Handler;

.field private mWakeTime:J

.field private final mWakeUpFile:Landroid/util/AtomicFile;

.field private final mWakeUpTime:Ljava/util/HashMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/HashMap<",
            "Ljava/lang/String;",
            "Ljava/lang/Long;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Lcom/miui/server/SecurityManagerService;)V
    .locals 4
    .param p1, "service"    # Lcom/miui/server/SecurityManagerService;

    .line 33
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 29
    new-instance v0, Landroid/util/AtomicFile;

    new-instance v1, Ljava/io/File;

    invoke-static {}, Landroid/os/Environment;->getDataSystemDirectory()Ljava/io/File;

    move-result-object v2

    const-string v3, "miui-wakeuptime.xml"

    invoke-direct {v1, v2, v3}, Ljava/io/File;-><init>(Ljava/io/File;Ljava/lang/String;)V

    invoke-direct {v0, v1}, Landroid/util/AtomicFile;-><init>(Ljava/io/File;)V

    iput-object v0, p0, Lcom/miui/server/security/WakeUpTimeImpl;->mWakeUpFile:Landroid/util/AtomicFile;

    .line 30
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    iput-object v0, p0, Lcom/miui/server/security/WakeUpTimeImpl;->mWakeUpTime:Ljava/util/HashMap;

    .line 31
    const-wide/16 v0, 0x0

    iput-wide v0, p0, Lcom/miui/server/security/WakeUpTimeImpl;->mWakeTime:J

    .line 34
    iget-object v0, p1, Lcom/miui/server/SecurityManagerService;->mSecurityWriteHandler:Lcom/miui/server/security/SecurityWriteHandler;

    iput-object v0, p0, Lcom/miui/server/security/WakeUpTimeImpl;->mHandler:Landroid/os/Handler;

    .line 35
    return-void
.end method

.method private minWakeUpTime(J)V
    .locals 10
    .param p1, "nowtime"    # J

    .line 137
    const-wide/16 v0, 0x0

    .line 138
    .local v0, "min":J
    const-wide/16 v2, 0x12c

    add-long/2addr v2, p1

    .line 139
    .local v2, "rightBorder":J
    iget-object v4, p0, Lcom/miui/server/security/WakeUpTimeImpl;->mWakeUpTime:Ljava/util/HashMap;

    invoke-virtual {v4}, Ljava/util/HashMap;->keySet()Ljava/util/Set;

    move-result-object v4

    invoke-interface {v4}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v4

    :goto_0
    invoke-interface {v4}, Ljava/util/Iterator;->hasNext()Z

    move-result v5

    if-eqz v5, :cond_2

    invoke-interface {v4}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Ljava/lang/String;

    .line 140
    .local v5, "componentName":Ljava/lang/String;
    invoke-virtual {p0, v5}, Lcom/miui/server/security/WakeUpTimeImpl;->getBootTimeFromMap(Ljava/lang/String;)J

    move-result-wide v6

    .line 143
    .local v6, "tmp":J
    cmp-long v8, v6, p1

    if-ltz v8, :cond_1

    cmp-long v8, v6, v0

    if-ltz v8, :cond_0

    const-wide/16 v8, 0x0

    cmp-long v8, v0, v8

    if-nez v8, :cond_1

    .line 144
    :cond_0
    invoke-static {v6, v7, v2, v3}, Ljava/lang/Math;->max(JJ)J

    move-result-wide v0

    .line 146
    .end local v5    # "componentName":Ljava/lang/String;
    .end local v6    # "tmp":J
    :cond_1
    goto :goto_0

    .line 147
    :cond_2
    iput-wide v0, p0, Lcom/miui/server/security/WakeUpTimeImpl;->mWakeTime:J

    .line 148
    return-void
.end method

.method private putBootTimeToMap(Ljava/lang/String;J)V
    .locals 3
    .param p1, "componentName"    # Ljava/lang/String;
    .param p2, "time"    # J

    .line 118
    iget-object v0, p0, Lcom/miui/server/security/WakeUpTimeImpl;->mWakeUpTime:Ljava/util/HashMap;

    monitor-enter v0

    .line 119
    :try_start_0
    iget-object v1, p0, Lcom/miui/server/security/WakeUpTimeImpl;->mWakeUpTime:Ljava/util/HashMap;

    invoke-static {p2, p3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v2

    invoke-virtual {v1, p1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 120
    monitor-exit v0

    .line 121
    return-void

    .line 120
    :catchall_0
    move-exception v1

    monitor-exit v0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v1
.end method

.method private readWakeUpTime(Ljava/io/FileInputStream;)V
    .locals 9
    .param p1, "fis"    # Ljava/io/FileInputStream;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    .line 93
    invoke-static {}, Landroid/util/Xml;->newPullParser()Lorg/xmlpull/v1/XmlPullParser;

    move-result-object v0

    .line 94
    .local v0, "parser":Lorg/xmlpull/v1/XmlPullParser;
    const/4 v1, 0x0

    invoke-interface {v0, p1, v1}, Lorg/xmlpull/v1/XmlPullParser;->setInput(Ljava/io/InputStream;Ljava/lang/String;)V

    .line 96
    invoke-interface {v0}, Lorg/xmlpull/v1/XmlPullParser;->getEventType()I

    move-result v2

    .line 97
    .local v2, "eventType":I
    :goto_0
    const/4 v3, 0x1

    const/4 v4, 0x2

    if-eq v2, v4, :cond_0

    if-eq v2, v3, :cond_0

    .line 98
    invoke-interface {v0}, Lorg/xmlpull/v1/XmlPullParser;->next()I

    move-result v2

    goto :goto_0

    .line 100
    :cond_0
    invoke-interface {v0}, Lorg/xmlpull/v1/XmlPullParser;->getName()Ljava/lang/String;

    move-result-object v5

    .line 101
    .local v5, "tagName":Ljava/lang/String;
    const-string v6, "classnames"

    invoke-virtual {v6, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-eqz v6, :cond_3

    .line 102
    invoke-interface {v0}, Lorg/xmlpull/v1/XmlPullParser;->next()I

    move-result v2

    .line 104
    :cond_1
    if-ne v2, v4, :cond_2

    invoke-interface {v0}, Lorg/xmlpull/v1/XmlPullParser;->getDepth()I

    move-result v6

    if-ne v6, v4, :cond_2

    .line 105
    invoke-interface {v0}, Lorg/xmlpull/v1/XmlPullParser;->getName()Ljava/lang/String;

    move-result-object v5

    .line 106
    const-string v6, "classname"

    invoke-virtual {v6, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-eqz v6, :cond_2

    .line 107
    const-string v6, "name"

    invoke-interface {v0, v1, v6}, Lorg/xmlpull/v1/XmlPullParser;->getAttributeValue(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v6

    .line 108
    .local v6, "componentName":Ljava/lang/String;
    const-string/jumbo v7, "time"

    invoke-interface {v0, v1, v7}, Lorg/xmlpull/v1/XmlPullParser;->getAttributeValue(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v7

    invoke-static {v7}, Ljava/lang/Long;->parseLong(Ljava/lang/String;)J

    move-result-wide v7

    .line 109
    .local v7, "time":J
    invoke-direct {p0, v6, v7, v8}, Lcom/miui/server/security/WakeUpTimeImpl;->putBootTimeToMap(Ljava/lang/String;J)V

    .line 112
    .end local v6    # "componentName":Ljava/lang/String;
    .end local v7    # "time":J
    :cond_2
    invoke-interface {v0}, Lorg/xmlpull/v1/XmlPullParser;->next()I

    move-result v2

    .line 113
    if-ne v2, v3, :cond_1

    .line 115
    :cond_3
    return-void
.end method

.method private scheduleWriteBootTime()V
    .locals 3

    .line 129
    iget-object v0, p0, Lcom/miui/server/security/WakeUpTimeImpl;->mHandler:Landroid/os/Handler;

    const/4 v1, 0x3

    invoke-virtual {v0, v1}, Landroid/os/Handler;->hasMessages(I)Z

    move-result v0

    if-eqz v0, :cond_0

    return-void

    .line 130
    :cond_0
    iget-object v0, p0, Lcom/miui/server/security/WakeUpTimeImpl;->mHandler:Landroid/os/Handler;

    invoke-virtual {v0, v1}, Landroid/os/Handler;->obtainMessage(I)Landroid/os/Message;

    move-result-object v0

    .line 131
    .local v0, "msg":Landroid/os/Message;
    iget-wide v1, p0, Lcom/miui/server/security/WakeUpTimeImpl;->mWakeTime:J

    invoke-static {v1, v2}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v1

    iput-object v1, v0, Landroid/os/Message;->obj:Ljava/lang/Object;

    .line 132
    iget-object v1, p0, Lcom/miui/server/security/WakeUpTimeImpl;->mHandler:Landroid/os/Handler;

    invoke-virtual {v1, v0}, Landroid/os/Handler;->sendMessage(Landroid/os/Message;)Z

    .line 133
    return-void
.end method

.method private scheduleWriteWakeUpTime()V
    .locals 2

    .line 124
    iget-object v0, p0, Lcom/miui/server/security/WakeUpTimeImpl;->mHandler:Landroid/os/Handler;

    const/4 v1, 0x2

    invoke-virtual {v0, v1}, Landroid/os/Handler;->hasMessages(I)Z

    move-result v0

    if-eqz v0, :cond_0

    return-void

    .line 125
    :cond_0
    iget-object v0, p0, Lcom/miui/server/security/WakeUpTimeImpl;->mHandler:Landroid/os/Handler;

    invoke-virtual {v0, v1}, Landroid/os/Handler;->sendEmptyMessage(I)Z

    .line 126
    return-void
.end method

.method private setTimeBoot()V
    .locals 4

    .line 151
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v0

    const-wide/16 v2, 0x3e8

    div-long/2addr v0, v2

    .line 152
    .local v0, "now_time":J
    iget-object v2, p0, Lcom/miui/server/security/WakeUpTimeImpl;->mWakeUpTime:Ljava/util/HashMap;

    monitor-enter v2

    .line 153
    :try_start_0
    invoke-direct {p0, v0, v1}, Lcom/miui/server/security/WakeUpTimeImpl;->minWakeUpTime(J)V

    .line 154
    monitor-exit v2
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 155
    invoke-direct {p0}, Lcom/miui/server/security/WakeUpTimeImpl;->scheduleWriteBootTime()V

    .line 156
    return-void

    .line 154
    :catchall_0
    move-exception v3

    :try_start_1
    monitor-exit v2
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    throw v3
.end method


# virtual methods
.method public getBootTimeFromMap(Ljava/lang/String;)J
    .locals 3
    .param p1, "componentName"    # Ljava/lang/String;

    .line 87
    iget-object v0, p0, Lcom/miui/server/security/WakeUpTimeImpl;->mWakeUpTime:Ljava/util/HashMap;

    monitor-enter v0

    .line 88
    :try_start_0
    iget-object v1, p0, Lcom/miui/server/security/WakeUpTimeImpl;->mWakeUpTime:Ljava/util/HashMap;

    invoke-virtual {v1, p1}, Ljava/util/HashMap;->containsKey(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    iget-object v1, p0, Lcom/miui/server/security/WakeUpTimeImpl;->mWakeUpTime:Ljava/util/HashMap;

    invoke-virtual {v1, p1}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/Long;

    invoke-virtual {v1}, Ljava/lang/Long;->longValue()J

    move-result-wide v1

    goto :goto_0

    :cond_0
    const-wide/16 v1, 0x0

    :goto_0
    monitor-exit v0

    return-wide v1

    .line 89
    :catchall_0
    move-exception v1

    monitor-exit v0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v1
.end method

.method public readWakeUpTime()V
    .locals 3

    .line 73
    iget-object v0, p0, Lcom/miui/server/security/WakeUpTimeImpl;->mWakeUpTime:Ljava/util/HashMap;

    monitor-enter v0

    .line 74
    :try_start_0
    iget-object v1, p0, Lcom/miui/server/security/WakeUpTimeImpl;->mWakeUpTime:Ljava/util/HashMap;

    invoke-virtual {v1}, Ljava/util/HashMap;->clear()V

    .line 75
    monitor-exit v0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_2

    .line 76
    iget-object v0, p0, Lcom/miui/server/security/WakeUpTimeImpl;->mWakeUpFile:Landroid/util/AtomicFile;

    invoke-virtual {v0}, Landroid/util/AtomicFile;->getBaseFile()Ljava/io/File;

    move-result-object v0

    invoke-virtual {v0}, Ljava/io/File;->exists()Z

    move-result v0

    if-nez v0, :cond_0

    .line 77
    return-void

    .line 79
    :cond_0
    :try_start_1
    iget-object v0, p0, Lcom/miui/server/security/WakeUpTimeImpl;->mWakeUpFile:Landroid/util/AtomicFile;

    invoke-virtual {v0}, Landroid/util/AtomicFile;->openRead()Ljava/io/FileInputStream;

    move-result-object v0
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_0

    .line 80
    .local v0, "fis":Ljava/io/FileInputStream;
    :try_start_2
    invoke-direct {p0, v0}, Lcom/miui/server/security/WakeUpTimeImpl;->readWakeUpTime(Ljava/io/FileInputStream;)V
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    .line 81
    if-eqz v0, :cond_1

    :try_start_3
    invoke-virtual {v0}, Ljava/io/FileInputStream;->close()V
    :try_end_3
    .catch Ljava/lang/Exception; {:try_start_3 .. :try_end_3} :catch_0

    .line 83
    .end local v0    # "fis":Ljava/io/FileInputStream;
    :cond_1
    goto :goto_1

    .line 79
    .restart local v0    # "fis":Ljava/io/FileInputStream;
    :catchall_0
    move-exception v1

    if-eqz v0, :cond_2

    :try_start_4
    invoke-virtual {v0}, Ljava/io/FileInputStream;->close()V
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_1

    goto :goto_0

    :catchall_1
    move-exception v2

    :try_start_5
    invoke-virtual {v1, v2}, Ljava/lang/Throwable;->addSuppressed(Ljava/lang/Throwable;)V

    .end local p0    # "this":Lcom/miui/server/security/WakeUpTimeImpl;
    :cond_2
    :goto_0
    throw v1
    :try_end_5
    .catch Ljava/lang/Exception; {:try_start_5 .. :try_end_5} :catch_0

    .line 81
    .end local v0    # "fis":Ljava/io/FileInputStream;
    .restart local p0    # "this":Lcom/miui/server/security/WakeUpTimeImpl;
    :catch_0
    move-exception v0

    .line 82
    .local v0, "e":Ljava/lang/Exception;
    iget-object v1, p0, Lcom/miui/server/security/WakeUpTimeImpl;->mWakeUpFile:Landroid/util/AtomicFile;

    invoke-virtual {v1}, Landroid/util/AtomicFile;->getBaseFile()Ljava/io/File;

    move-result-object v1

    invoke-virtual {v1}, Ljava/io/File;->delete()Z

    .line 84
    .end local v0    # "e":Ljava/lang/Exception;
    :goto_1
    return-void

    .line 75
    :catchall_2
    move-exception v1

    :try_start_6
    monitor-exit v0
    :try_end_6
    .catchall {:try_start_6 .. :try_end_6} :catchall_2

    throw v1
.end method

.method public setWakeUpTime(Ljava/lang/String;J)V
    .locals 0
    .param p1, "componentName"    # Ljava/lang/String;
    .param p2, "timeInSeconds"    # J

    .line 38
    invoke-direct {p0, p1, p2, p3}, Lcom/miui/server/security/WakeUpTimeImpl;->putBootTimeToMap(Ljava/lang/String;J)V

    .line 39
    invoke-direct {p0}, Lcom/miui/server/security/WakeUpTimeImpl;->scheduleWriteWakeUpTime()V

    .line 40
    invoke-direct {p0}, Lcom/miui/server/security/WakeUpTimeImpl;->setTimeBoot()V

    .line 41
    return-void
.end method

.method public writeWakeUpTime()V
    .locals 10

    .line 44
    const/4 v0, 0x0

    .line 46
    .local v0, "fos":Ljava/io/FileOutputStream;
    :try_start_0
    iget-object v1, p0, Lcom/miui/server/security/WakeUpTimeImpl;->mWakeUpFile:Landroid/util/AtomicFile;

    invoke-virtual {v1}, Landroid/util/AtomicFile;->startWrite()Ljava/io/FileOutputStream;

    move-result-object v1

    move-object v0, v1

    .line 47
    new-instance v1, Lcom/android/internal/util/FastXmlSerializer;

    invoke-direct {v1}, Lcom/android/internal/util/FastXmlSerializer;-><init>()V

    .line 48
    .local v1, "out":Lorg/xmlpull/v1/XmlSerializer;
    const-string/jumbo v2, "utf-8"

    invoke-interface {v1, v0, v2}, Lorg/xmlpull/v1/XmlSerializer;->setOutput(Ljava/io/OutputStream;Ljava/lang/String;)V

    .line 49
    const/4 v2, 0x1

    invoke-static {v2}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v3

    const/4 v4, 0x0

    invoke-interface {v1, v4, v3}, Lorg/xmlpull/v1/XmlSerializer;->startDocument(Ljava/lang/String;Ljava/lang/Boolean;)V

    .line 50
    const-string v3, "http://xmlpull.org/v1/doc/features.html#indent-output"

    invoke-interface {v1, v3, v2}, Lorg/xmlpull/v1/XmlSerializer;->setFeature(Ljava/lang/String;Z)V

    .line 51
    const-string v2, "classnames"

    invoke-interface {v1, v4, v2}, Lorg/xmlpull/v1/XmlSerializer;->startTag(Ljava/lang/String;Ljava/lang/String;)Lorg/xmlpull/v1/XmlSerializer;

    .line 52
    iget-object v2, p0, Lcom/miui/server/security/WakeUpTimeImpl;->mWakeUpTime:Ljava/util/HashMap;

    monitor-enter v2
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    .line 53
    :try_start_1
    iget-object v3, p0, Lcom/miui/server/security/WakeUpTimeImpl;->mWakeUpTime:Ljava/util/HashMap;

    invoke-virtual {v3}, Ljava/util/HashMap;->keySet()Ljava/util/Set;

    move-result-object v3

    invoke-interface {v3}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :goto_0
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v5

    if-eqz v5, :cond_1

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Ljava/lang/String;

    .line 54
    .local v5, "componentName":Ljava/lang/String;
    invoke-virtual {p0, v5}, Lcom/miui/server/security/WakeUpTimeImpl;->getBootTimeFromMap(Ljava/lang/String;)J

    move-result-wide v6

    const-wide/16 v8, 0x0

    cmp-long v6, v6, v8

    if-eqz v6, :cond_0

    .line 55
    const-string v6, "classname"

    invoke-interface {v1, v4, v6}, Lorg/xmlpull/v1/XmlSerializer;->startTag(Ljava/lang/String;Ljava/lang/String;)Lorg/xmlpull/v1/XmlSerializer;

    .line 56
    const-string v6, "name"

    invoke-interface {v1, v4, v6, v5}, Lorg/xmlpull/v1/XmlSerializer;->attribute(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Lorg/xmlpull/v1/XmlSerializer;

    .line 57
    const-string/jumbo v6, "time"

    invoke-virtual {p0, v5}, Lcom/miui/server/security/WakeUpTimeImpl;->getBootTimeFromMap(Ljava/lang/String;)J

    move-result-wide v7

    invoke-static {v7, v8}, Ljava/lang/String;->valueOf(J)Ljava/lang/String;

    move-result-object v7

    invoke-interface {v1, v4, v6, v7}, Lorg/xmlpull/v1/XmlSerializer;->attribute(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Lorg/xmlpull/v1/XmlSerializer;

    .line 58
    const-string v6, "classname"

    invoke-interface {v1, v4, v6}, Lorg/xmlpull/v1/XmlSerializer;->endTag(Ljava/lang/String;Ljava/lang/String;)Lorg/xmlpull/v1/XmlSerializer;

    .line 60
    .end local v5    # "componentName":Ljava/lang/String;
    :cond_0
    goto :goto_0

    .line 61
    :cond_1
    monitor-exit v2
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 62
    :try_start_2
    const-string v2, "classnames"

    invoke-interface {v1, v4, v2}, Lorg/xmlpull/v1/XmlSerializer;->endTag(Ljava/lang/String;Ljava/lang/String;)Lorg/xmlpull/v1/XmlSerializer;

    .line 63
    invoke-interface {v1}, Lorg/xmlpull/v1/XmlSerializer;->endDocument()V

    .line 64
    iget-object v2, p0, Lcom/miui/server/security/WakeUpTimeImpl;->mWakeUpFile:Landroid/util/AtomicFile;

    invoke-virtual {v2, v0}, Landroid/util/AtomicFile;->finishWrite(Ljava/io/FileOutputStream;)V
    :try_end_2
    .catch Ljava/io/IOException; {:try_start_2 .. :try_end_2} :catch_0

    .line 69
    .end local v1    # "out":Lorg/xmlpull/v1/XmlSerializer;
    goto :goto_1

    .line 61
    .restart local v1    # "out":Lorg/xmlpull/v1/XmlSerializer;
    :catchall_0
    move-exception v3

    :try_start_3
    monitor-exit v2
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    .end local v0    # "fos":Ljava/io/FileOutputStream;
    .end local p0    # "this":Lcom/miui/server/security/WakeUpTimeImpl;
    :try_start_4
    throw v3
    :try_end_4
    .catch Ljava/io/IOException; {:try_start_4 .. :try_end_4} :catch_0

    .line 65
    .end local v1    # "out":Lorg/xmlpull/v1/XmlSerializer;
    .restart local v0    # "fos":Ljava/io/FileOutputStream;
    .restart local p0    # "this":Lcom/miui/server/security/WakeUpTimeImpl;
    :catch_0
    move-exception v1

    .line 66
    .local v1, "e1":Ljava/io/IOException;
    if-eqz v0, :cond_2

    .line 67
    iget-object v2, p0, Lcom/miui/server/security/WakeUpTimeImpl;->mWakeUpFile:Landroid/util/AtomicFile;

    invoke-virtual {v2, v0}, Landroid/util/AtomicFile;->failWrite(Ljava/io/FileOutputStream;)V

    .line 70
    .end local v1    # "e1":Ljava/io/IOException;
    :cond_2
    :goto_1
    return-void
.end method
