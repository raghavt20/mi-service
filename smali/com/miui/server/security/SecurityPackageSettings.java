public class com.miui.server.security.SecurityPackageSettings {
	 /* .source "SecurityPackageSettings.java" */
	 /* # instance fields */
	 public Boolean accessControl;
	 public Boolean childrenControl;
	 public Boolean isDarkModeChecked;
	 public Boolean isGameStorageApp;
	 public Boolean isPrivacyApp;
	 public Boolean isRelaunchWhenFolded;
	 public Boolean isRemindForRelaunch;
	 public Boolean isScRelaunchConfirm;
	 public Boolean maskNotification;
	 public java.lang.String name;
	 /* # direct methods */
	 public com.miui.server.security.SecurityPackageSettings ( ) {
		 /* .locals 2 */
		 /* .param p1, "name" # Ljava/lang/String; */
		 /* .line 18 */
		 /* invoke-direct {p0}, Ljava/lang/Object;-><init>()V */
		 /* .line 19 */
		 this.name = p1;
		 /* .line 20 */
		 int v0 = 0; // const/4 v0, 0x0
		 /* iput-boolean v0, p0, Lcom/miui/server/security/SecurityPackageSettings;->accessControl:Z */
		 /* .line 21 */
		 /* iput-boolean v0, p0, Lcom/miui/server/security/SecurityPackageSettings;->childrenControl:Z */
		 /* .line 22 */
		 /* iput-boolean v0, p0, Lcom/miui/server/security/SecurityPackageSettings;->maskNotification:Z */
		 /* .line 23 */
		 /* iput-boolean v0, p0, Lcom/miui/server/security/SecurityPackageSettings;->isPrivacyApp:Z */
		 /* .line 24 */
		 com.android.server.MiuiUiModeManagerServiceStub .getInstance ( );
		 v1 = 		 (( com.android.server.MiuiUiModeManagerServiceStub ) v1 ).getForceDarkAppDefaultEnable ( p1 ); // invoke-virtual {v1, p1}, Lcom/android/server/MiuiUiModeManagerServiceStub;->getForceDarkAppDefaultEnable(Ljava/lang/String;)Z
		 /* iput-boolean v1, p0, Lcom/miui/server/security/SecurityPackageSettings;->isDarkModeChecked:Z */
		 /* .line 25 */
		 /* iput-boolean v0, p0, Lcom/miui/server/security/SecurityPackageSettings;->isGameStorageApp:Z */
		 /* .line 26 */
		 int v1 = 1; // const/4 v1, 0x1
		 /* iput-boolean v1, p0, Lcom/miui/server/security/SecurityPackageSettings;->isRemindForRelaunch:Z */
		 /* .line 27 */
		 /* iput-boolean v0, p0, Lcom/miui/server/security/SecurityPackageSettings;->isRelaunchWhenFolded:Z */
		 /* .line 28 */
		 /* iput-boolean v1, p0, Lcom/miui/server/security/SecurityPackageSettings;->isScRelaunchConfirm:Z */
		 /* .line 29 */
		 return;
	 } // .end method
