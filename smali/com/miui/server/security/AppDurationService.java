public class com.miui.server.security.AppDurationService {
	 /* .source "AppDurationService.java" */
	 /* # annotations */
	 /* .annotation system Ldalvik/annotation/MemberClasses; */
	 /* value = { */
	 /* Lcom/miui/server/security/AppDurationService$UserPackage;, */
	 /* Lcom/miui/server/security/AppDurationService$UserPackageBehaviorDuration;, */
	 /* Lcom/miui/server/security/AppDurationService$TokenAndStartTime; */
	 /* } */
} // .end annotation
/* # static fields */
private static final java.lang.String TAG;
/* # instance fields */
private final com.miui.server.security.AppBehaviorService appBehavior;
private final java.lang.Object mDurationLock;
private final android.util.ArrayMap userPackageStartTimeAndTokenArrayMap;
/* .annotation system Ldalvik/annotation/Signature; */
/* value = { */
/* "Landroid/util/ArrayMap<", */
/* "Lcom/miui/server/security/AppDurationService$UserPackage;", */
/* "Lcom/miui/server/security/AppDurationService$UserPackageBehaviorDuration;", */
/* ">;" */
/* } */
} // .end annotation
} // .end field
/* # direct methods */
public com.miui.server.security.AppDurationService ( ) {
/* .locals 3 */
/* .param p1, "appBehavior" # Lcom/miui/server/security/AppBehaviorService; */
/* .line 24 */
/* invoke-direct {p0}, Ljava/lang/Object;-><init>()V */
/* .line 20 */
/* new-instance v0, Ljava/lang/Object; */
/* invoke-direct {v0}, Ljava/lang/Object;-><init>()V */
this.mDurationLock = v0;
/* .line 25 */
/* new-instance v0, Landroid/util/ArrayMap; */
/* invoke-direct {v0}, Landroid/util/ArrayMap;-><init>()V */
this.userPackageStartTimeAndTokenArrayMap = v0;
/* .line 26 */
this.appBehavior = p1;
/* .line 28 */
try { // :try_start_0
/* new-instance v0, Lcom/miui/server/security/AppDurationService$1; */
/* invoke-direct {v0, p0, p1}, Lcom/miui/server/security/AppDurationService$1;-><init>(Lcom/miui/server/security/AppDurationService;Lcom/miui/server/security/AppBehaviorService;)V */
/* .line 35 */
/* .local v0, "foregroundServiceObserver":Landroid/app/IForegroundServiceObserver$Stub; */
android.app.ActivityManager .getService ( );
/* :try_end_0 */
/* .catch Landroid/os/RemoteException; {:try_start_0 ..:try_end_0} :catch_0 */
/* .line 38 */
/* nop */
} // .end local v0 # "foregroundServiceObserver":Landroid/app/IForegroundServiceObserver$Stub;
/* .line 36 */
/* :catch_0 */
/* move-exception v0 */
/* .line 37 */
/* .local v0, "exception":Landroid/os/RemoteException; */
final String v1 = "AppDurationService"; // const-string v1, "AppDurationService"
final String v2 = "AppDurationService registerForegroundServiceObserver"; // const-string v2, "AppDurationService registerForegroundServiceObserver"
android.util.Slog .e ( v1,v2,v0 );
/* .line 39 */
} // .end local v0 # "exception":Landroid/os/RemoteException;
} // :goto_0
return;
} // .end method
/* # virtual methods */
public void checkIfTooLongDuration ( ) {
/* .locals 17 */
/* .line 91 */
/* move-object/from16 v1, p0 */
final String v0 = "AppDurationService"; // const-string v0, "AppDurationService"
final String v2 = "checkIfTooLongDuration"; // const-string v2, "checkIfTooLongDuration"
android.util.Slog .i ( v0,v2 );
/* .line 92 */
v2 = this.mDurationLock;
/* monitor-enter v2 */
/* .line 93 */
try { // :try_start_0
android.os.SystemClock .elapsedRealtime ( );
/* move-result-wide v3 */
/* .line 94 */
/* .local v3, "current":J */
int v0 = 0; // const/4 v0, 0x0
/* .local v0, "i":I */
} // :goto_0
v5 = this.userPackageStartTimeAndTokenArrayMap;
v5 = (( android.util.ArrayMap ) v5 ).size ( ); // invoke-virtual {v5}, Landroid/util/ArrayMap;->size()I
/* if-ge v0, v5, :cond_6 */
/* .line 95 */
v5 = this.userPackageStartTimeAndTokenArrayMap;
(( android.util.ArrayMap ) v5 ).keyAt ( v0 ); // invoke-virtual {v5, v0}, Landroid/util/ArrayMap;->keyAt(I)Ljava/lang/Object;
/* check-cast v5, Lcom/miui/server/security/AppDurationService$UserPackage; */
com.miui.server.security.AppDurationService$UserPackage .-$$Nest$fgetpkgName ( v5 );
/* .line 96 */
/* .local v5, "pkgName":Ljava/lang/String; */
v6 = this.userPackageStartTimeAndTokenArrayMap;
(( android.util.ArrayMap ) v6 ).valueAt ( v0 ); // invoke-virtual {v6, v0}, Landroid/util/ArrayMap;->valueAt(I)Ljava/lang/Object;
/* check-cast v6, Lcom/miui/server/security/AppDurationService$UserPackageBehaviorDuration; */
/* .line 97 */
/* .local v6, "userPackageBehaviorDuration":Lcom/miui/server/security/AppDurationService$UserPackageBehaviorDuration; */
int v7 = 0; // const/4 v7, 0x0
/* .local v7, "j":I */
} // :goto_1
com.miui.server.security.AppDurationService$UserPackageBehaviorDuration .-$$Nest$fgetbehaviorDurationMap ( v6 );
v8 = (( android.util.SparseArray ) v8 ).size ( ); // invoke-virtual {v8}, Landroid/util/SparseArray;->size()I
/* if-ge v7, v8, :cond_5 */
/* .line 98 */
com.miui.server.security.AppDurationService$UserPackageBehaviorDuration .-$$Nest$fgetbehaviorDurationMap ( v6 );
v8 = (( android.util.SparseArray ) v8 ).keyAt ( v7 ); // invoke-virtual {v8, v7}, Landroid/util/SparseArray;->keyAt(I)I
/* .line 99 */
/* .local v8, "curBehavior":I */
com.miui.server.security.AppDurationService$UserPackageBehaviorDuration .-$$Nest$fgetbehaviorDurationMap ( v6 );
(( android.util.SparseArray ) v9 ).valueAt ( v7 ); // invoke-virtual {v9, v7}, Landroid/util/SparseArray;->valueAt(I)Ljava/lang/Object;
/* check-cast v9, Lcom/miui/server/security/AppDurationService$TokenAndStartTime; */
/* .line 100 */
/* .local v9, "tokenAndStartTime":Lcom/miui/server/security/AppDurationService$TokenAndStartTime; */
com.miui.server.security.AppDurationService$TokenAndStartTime .-$$Nest$fgetbinderStartTimeMap ( v9 );
if ( v10 != null) { // if-eqz v10, :cond_2
com.miui.server.security.AppDurationService$TokenAndStartTime .-$$Nest$fgetbinderStartTimeMap ( v9 );
v10 = (( android.util.ArrayMap ) v10 ).size ( ); // invoke-virtual {v10}, Landroid/util/ArrayMap;->size()I
/* if-lez v10, :cond_2 */
/* .line 101 */
int v10 = 0; // const/4 v10, 0x0
/* .local v10, "k":I */
} // :goto_2
com.miui.server.security.AppDurationService$TokenAndStartTime .-$$Nest$fgetbinderStartTimeMap ( v9 );
v12 = (( android.util.ArrayMap ) v12 ).size ( ); // invoke-virtual {v12}, Landroid/util/ArrayMap;->size()I
/* if-ge v10, v12, :cond_1 */
/* .line 102 */
com.miui.server.security.AppDurationService$TokenAndStartTime .-$$Nest$fgetbinderStartTimeMap ( v9 );
(( android.util.ArrayMap ) v12 ).valueAt ( v10 ); // invoke-virtual {v12, v10}, Landroid/util/ArrayMap;->valueAt(I)Ljava/lang/Object;
/* check-cast v12, Ljava/lang/Long; */
(( java.lang.Long ) v12 ).longValue ( ); // invoke-virtual {v12}, Ljava/lang/Long;->longValue()J
/* move-result-wide v12 */
/* sub-long v12, v3, v12 */
/* .line 103 */
/* .local v12, "duration":J */
v14 = this.appBehavior;
v14 = (( com.miui.server.security.AppBehaviorService ) v14 ).moreThanThreshold ( v8, v12, v13 ); // invoke-virtual {v14, v8, v12, v13}, Lcom/miui/server/security/AppBehaviorService;->moreThanThreshold(IJ)Z
/* .line 104 */
/* .local v14, "moreThanThreshold":Z */
if ( v14 != null) { // if-eqz v14, :cond_0
/* .line 105 */
final String v15 = "AppDurationService"; // const-string v15, "AppDurationService"
/* new-instance v11, Ljava/lang/StringBuilder; */
/* invoke-direct {v11}, Ljava/lang/StringBuilder;-><init>()V */
(( java.lang.StringBuilder ) v11 ).append ( v5 ); // invoke-virtual {v11, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
/* move-object/from16 v16, v6 */
} // .end local v6 # "userPackageBehaviorDuration":Lcom/miui/server/security/AppDurationService$UserPackageBehaviorDuration;
/* .local v16, "userPackageBehaviorDuration":Lcom/miui/server/security/AppDurationService$UserPackageBehaviorDuration; */
final String v6 = ",behavior:"; // const-string v6, ",behavior:"
(( java.lang.StringBuilder ) v11 ).append ( v6 ); // invoke-virtual {v11, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v6 ).append ( v8 ); // invoke-virtual {v6, v8}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;
final String v11 = " has running too long.record and restart"; // const-string v11, " has running too long.record and restart"
(( java.lang.StringBuilder ) v6 ).append ( v11 ); // invoke-virtual {v6, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v6 ).toString ( ); // invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
android.util.Slog .i ( v15,v6 );
/* .line 106 */
com.miui.server.security.AppDurationService$TokenAndStartTime .-$$Nest$fgetbinderStartTimeMap ( v9 );
java.lang.Long .valueOf ( v3,v4 );
(( android.util.ArrayMap ) v6 ).setValueAt ( v10, v11 ); // invoke-virtual {v6, v10, v11}, Landroid/util/ArrayMap;->setValueAt(ILjava/lang/Object;)Ljava/lang/Object;
/* .line 107 */
v6 = this.appBehavior;
int v11 = 0; // const/4 v11, 0x0
miui.security.AppBehavior .buildCountEvent ( v8,v5,v12,v13,v11 );
(( com.miui.server.security.AppBehaviorService ) v6 ).recordAppBehaviorAsync ( v15 ); // invoke-virtual {v6, v15}, Lcom/miui/server/security/AppBehaviorService;->recordAppBehaviorAsync(Lmiui/security/AppBehavior;)V
/* .line 104 */
} // .end local v16 # "userPackageBehaviorDuration":Lcom/miui/server/security/AppDurationService$UserPackageBehaviorDuration;
/* .restart local v6 # "userPackageBehaviorDuration":Lcom/miui/server/security/AppDurationService$UserPackageBehaviorDuration; */
} // :cond_0
/* move-object/from16 v16, v6 */
/* .line 101 */
} // .end local v6 # "userPackageBehaviorDuration":Lcom/miui/server/security/AppDurationService$UserPackageBehaviorDuration;
} // .end local v12 # "duration":J
} // .end local v14 # "moreThanThreshold":Z
/* .restart local v16 # "userPackageBehaviorDuration":Lcom/miui/server/security/AppDurationService$UserPackageBehaviorDuration; */
} // :goto_3
/* add-int/lit8 v10, v10, 0x1 */
/* move-object/from16 v6, v16 */
} // .end local v16 # "userPackageBehaviorDuration":Lcom/miui/server/security/AppDurationService$UserPackageBehaviorDuration;
/* .restart local v6 # "userPackageBehaviorDuration":Lcom/miui/server/security/AppDurationService$UserPackageBehaviorDuration; */
} // :cond_1
/* move-object/from16 v16, v6 */
} // .end local v6 # "userPackageBehaviorDuration":Lcom/miui/server/security/AppDurationService$UserPackageBehaviorDuration;
/* .restart local v16 # "userPackageBehaviorDuration":Lcom/miui/server/security/AppDurationService$UserPackageBehaviorDuration; */
/* .line 100 */
} // .end local v10 # "k":I
} // .end local v16 # "userPackageBehaviorDuration":Lcom/miui/server/security/AppDurationService$UserPackageBehaviorDuration;
/* .restart local v6 # "userPackageBehaviorDuration":Lcom/miui/server/security/AppDurationService$UserPackageBehaviorDuration; */
} // :cond_2
/* move-object/from16 v16, v6 */
/* .line 111 */
} // .end local v6 # "userPackageBehaviorDuration":Lcom/miui/server/security/AppDurationService$UserPackageBehaviorDuration;
/* .restart local v16 # "userPackageBehaviorDuration":Lcom/miui/server/security/AppDurationService$UserPackageBehaviorDuration; */
} // :goto_4
com.miui.server.security.AppDurationService$TokenAndStartTime .-$$Nest$fgetuidStartTimeMap ( v9 );
if ( v6 != null) { // if-eqz v6, :cond_4
com.miui.server.security.AppDurationService$TokenAndStartTime .-$$Nest$fgetuidStartTimeMap ( v9 );
v6 = (( android.util.SparseArray ) v6 ).size ( ); // invoke-virtual {v6}, Landroid/util/SparseArray;->size()I
/* if-lez v6, :cond_4 */
/* .line 112 */
int v6 = 0; // const/4 v6, 0x0
/* .local v6, "k":I */
} // :goto_5
com.miui.server.security.AppDurationService$TokenAndStartTime .-$$Nest$fgetuidStartTimeMap ( v9 );
v10 = (( android.util.SparseArray ) v10 ).size ( ); // invoke-virtual {v10}, Landroid/util/SparseArray;->size()I
/* if-ge v6, v10, :cond_4 */
/* .line 113 */
com.miui.server.security.AppDurationService$TokenAndStartTime .-$$Nest$fgetuidStartTimeMap ( v9 );
(( android.util.SparseArray ) v10 ).valueAt ( v6 ); // invoke-virtual {v10, v6}, Landroid/util/SparseArray;->valueAt(I)Ljava/lang/Object;
/* check-cast v10, Ljava/lang/Long; */
(( java.lang.Long ) v10 ).longValue ( ); // invoke-virtual {v10}, Ljava/lang/Long;->longValue()J
/* move-result-wide v10 */
/* sub-long v10, v3, v10 */
/* .line 114 */
/* .local v10, "duration":J */
v12 = this.appBehavior;
v12 = (( com.miui.server.security.AppBehaviorService ) v12 ).moreThanThreshold ( v8, v10, v11 ); // invoke-virtual {v12, v8, v10, v11}, Lcom/miui/server/security/AppBehaviorService;->moreThanThreshold(IJ)Z
/* .line 115 */
/* .local v12, "moreThanThreshold":Z */
if ( v12 != null) { // if-eqz v12, :cond_3
/* .line 116 */
final String v13 = "AppDurationService"; // const-string v13, "AppDurationService"
/* new-instance v14, Ljava/lang/StringBuilder; */
/* invoke-direct {v14}, Ljava/lang/StringBuilder;-><init>()V */
(( java.lang.StringBuilder ) v14 ).append ( v5 ); // invoke-virtual {v14, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
final String v15 = ",behavior:"; // const-string v15, ",behavior:"
(( java.lang.StringBuilder ) v14 ).append ( v15 ); // invoke-virtual {v14, v15}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v14 ).append ( v8 ); // invoke-virtual {v14, v8}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;
final String v15 = " has running too long.record and restart"; // const-string v15, " has running too long.record and restart"
(( java.lang.StringBuilder ) v14 ).append ( v15 ); // invoke-virtual {v14, v15}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v14 ).toString ( ); // invoke-virtual {v14}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
android.util.Slog .i ( v13,v14 );
/* .line 117 */
com.miui.server.security.AppDurationService$TokenAndStartTime .-$$Nest$fgetuidStartTimeMap ( v9 );
java.lang.Long .valueOf ( v3,v4 );
(( android.util.SparseArray ) v13 ).setValueAt ( v6, v14 ); // invoke-virtual {v13, v6, v14}, Landroid/util/SparseArray;->setValueAt(ILjava/lang/Object;)V
/* .line 118 */
v13 = this.appBehavior;
int v14 = 0; // const/4 v14, 0x0
miui.security.AppBehavior .buildCountEvent ( v8,v5,v10,v11,v14 );
(( com.miui.server.security.AppBehaviorService ) v13 ).recordAppBehaviorAsync ( v15 ); // invoke-virtual {v13, v15}, Lcom/miui/server/security/AppBehaviorService;->recordAppBehaviorAsync(Lmiui/security/AppBehavior;)V
/* .line 115 */
} // :cond_3
int v14 = 0; // const/4 v14, 0x0
/* .line 112 */
} // .end local v10 # "duration":J
} // .end local v12 # "moreThanThreshold":Z
} // :goto_6
/* add-int/lit8 v6, v6, 0x1 */
/* .line 97 */
} // .end local v6 # "k":I
} // .end local v8 # "curBehavior":I
} // .end local v9 # "tokenAndStartTime":Lcom/miui/server/security/AppDurationService$TokenAndStartTime;
} // :cond_4
/* add-int/lit8 v7, v7, 0x1 */
/* move-object/from16 v6, v16 */
/* goto/16 :goto_1 */
} // .end local v16 # "userPackageBehaviorDuration":Lcom/miui/server/security/AppDurationService$UserPackageBehaviorDuration;
/* .local v6, "userPackageBehaviorDuration":Lcom/miui/server/security/AppDurationService$UserPackageBehaviorDuration; */
} // :cond_5
/* move-object/from16 v16, v6 */
/* .line 94 */
} // .end local v5 # "pkgName":Ljava/lang/String;
} // .end local v6 # "userPackageBehaviorDuration":Lcom/miui/server/security/AppDurationService$UserPackageBehaviorDuration;
} // .end local v7 # "j":I
/* add-int/lit8 v0, v0, 0x1 */
/* goto/16 :goto_0 */
/* .line 124 */
} // .end local v0 # "i":I
} // .end local v3 # "current":J
} // :cond_6
/* monitor-exit v2 */
/* .line 125 */
return;
/* .line 124 */
/* :catchall_0 */
/* move-exception v0 */
/* monitor-exit v2 */
/* :try_end_0 */
/* .catchall {:try_start_0 ..:try_end_0} :catchall_0 */
/* throw v0 */
} // .end method
public void onDurationEvent ( Integer p0, android.os.IBinder p1, Integer p2, java.lang.String p3, Boolean p4 ) {
/* .locals 8 */
/* .param p1, "behavior" # I */
/* .param p2, "binder" # Landroid/os/IBinder; */
/* .param p3, "uid" # I */
/* .param p4, "pkgName" # Ljava/lang/String; */
/* .param p5, "start" # Z */
/* .line 42 */
v0 = this.appBehavior;
v0 = (( com.miui.server.security.AppBehaviorService ) v0 ).canRecordBehavior ( p1, p4 ); // invoke-virtual {v0, p1, p4}, Lcom/miui/server/security/AppBehaviorService;->canRecordBehavior(ILjava/lang/String;)Z
/* if-nez v0, :cond_0 */
/* .line 43 */
return;
/* .line 45 */
} // :cond_0
/* new-instance v0, Lcom/miui/server/security/AppDurationService$UserPackage; */
/* invoke-direct {v0, p3, p4}, Lcom/miui/server/security/AppDurationService$UserPackage;-><init>(ILjava/lang/String;)V */
/* .line 46 */
/* .local v0, "userPackage":Lcom/miui/server/security/AppDurationService$UserPackage; */
v1 = this.mDurationLock;
/* monitor-enter v1 */
/* .line 47 */
try { // :try_start_0
v2 = this.userPackageStartTimeAndTokenArrayMap;
(( android.util.ArrayMap ) v2 ).get ( v0 ); // invoke-virtual {v2, v0}, Landroid/util/ArrayMap;->get(Ljava/lang/Object;)Ljava/lang/Object;
/* check-cast v2, Lcom/miui/server/security/AppDurationService$UserPackageBehaviorDuration; */
/* .line 48 */
/* .local v2, "userPackageBehaviorDuration":Lcom/miui/server/security/AppDurationService$UserPackageBehaviorDuration; */
/* if-nez p5, :cond_1 */
/* if-nez v2, :cond_1 */
/* .line 49 */
/* monitor-exit v1 */
return;
/* .line 51 */
} // :cond_1
int v3 = 0; // const/4 v3, 0x0
/* if-nez v2, :cond_2 */
/* .line 52 */
/* new-instance v4, Lcom/miui/server/security/AppDurationService$UserPackageBehaviorDuration; */
/* invoke-direct {v4, v3}, Lcom/miui/server/security/AppDurationService$UserPackageBehaviorDuration;-><init>(Lcom/miui/server/security/AppDurationService$UserPackageBehaviorDuration-IA;)V */
/* move-object v2, v4 */
/* .line 53 */
v4 = this.userPackageStartTimeAndTokenArrayMap;
(( android.util.ArrayMap ) v4 ).put ( v0, v2 ); // invoke-virtual {v4, v0, v2}, Landroid/util/ArrayMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;
/* .line 55 */
} // :cond_2
if ( p5 != null) { // if-eqz p5, :cond_3
/* .line 56 */
(( com.miui.server.security.AppDurationService$UserPackageBehaviorDuration ) v2 ).recordBehavior ( p1, p2, p3 ); // invoke-virtual {v2, p1, p2, p3}, Lcom/miui/server/security/AppDurationService$UserPackageBehaviorDuration;->recordBehavior(ILandroid/os/IBinder;I)Z
/* .line 62 */
} // :cond_3
(( com.miui.server.security.AppDurationService$UserPackageBehaviorDuration ) v2 ).stopRecordDuration ( p1, p2, p3 ); // invoke-virtual {v2, p1, p2, p3}, Lcom/miui/server/security/AppDurationService$UserPackageBehaviorDuration;->stopRecordDuration(ILandroid/os/IBinder;I)J
/* move-result-wide v4 */
/* .line 63 */
/* .local v4, "duration":J */
/* const-wide/16 v6, 0x0 */
/* cmp-long v6, v4, v6 */
/* if-lez v6, :cond_4 */
/* .line 64 */
v6 = this.appBehavior;
miui.security.AppBehavior .buildCountEvent ( p1,p4,v4,v5,v3 );
(( com.miui.server.security.AppBehaviorService ) v6 ).recordAppBehaviorAsync ( v3 ); // invoke-virtual {v6, v3}, Lcom/miui/server/security/AppBehaviorService;->recordAppBehaviorAsync(Lmiui/security/AppBehavior;)V
/* .line 71 */
} // .end local v2 # "userPackageBehaviorDuration":Lcom/miui/server/security/AppDurationService$UserPackageBehaviorDuration;
} // .end local v4 # "duration":J
} // :cond_4
} // :goto_0
/* monitor-exit v1 */
/* .line 72 */
return;
/* .line 71 */
/* :catchall_0 */
/* move-exception v2 */
/* monitor-exit v1 */
/* :try_end_0 */
/* .catchall {:try_start_0 ..:try_end_0} :catchall_0 */
/* throw v2 */
} // .end method
public void removeDurationEvent ( Integer p0, Integer p1, java.lang.String p2 ) {
/* .locals 4 */
/* .param p1, "behavior" # I */
/* .param p2, "uid" # I */
/* .param p3, "pkgName" # Ljava/lang/String; */
/* .line 75 */
/* new-instance v0, Lcom/miui/server/security/AppDurationService$UserPackage; */
/* invoke-direct {v0, p2, p3}, Lcom/miui/server/security/AppDurationService$UserPackage;-><init>(ILjava/lang/String;)V */
/* .line 76 */
/* .local v0, "userPackage":Lcom/miui/server/security/AppDurationService$UserPackage; */
v1 = this.mDurationLock;
/* monitor-enter v1 */
/* .line 77 */
try { // :try_start_0
v2 = this.userPackageStartTimeAndTokenArrayMap;
(( android.util.ArrayMap ) v2 ).get ( v0 ); // invoke-virtual {v2, v0}, Landroid/util/ArrayMap;->get(Ljava/lang/Object;)Ljava/lang/Object;
/* check-cast v2, Lcom/miui/server/security/AppDurationService$UserPackageBehaviorDuration; */
/* .line 78 */
/* .local v2, "userPackageBehaviorDuration":Lcom/miui/server/security/AppDurationService$UserPackageBehaviorDuration; */
/* if-nez v2, :cond_0 */
/* .line 79 */
/* monitor-exit v1 */
return;
/* .line 81 */
} // :cond_0
com.miui.server.security.AppDurationService$UserPackageBehaviorDuration .-$$Nest$fgetbehaviorDurationMap ( v2 );
if ( v3 != null) { // if-eqz v3, :cond_1
/* .line 82 */
com.miui.server.security.AppDurationService$UserPackageBehaviorDuration .-$$Nest$fgetbehaviorDurationMap ( v2 );
(( android.util.SparseArray ) v3 ).remove ( p1 ); // invoke-virtual {v3, p1}, Landroid/util/SparseArray;->remove(I)V
/* .line 83 */
com.miui.server.security.AppDurationService$UserPackageBehaviorDuration .-$$Nest$fgetbehaviorDurationMap ( v2 );
v3 = (( android.util.SparseArray ) v3 ).size ( ); // invoke-virtual {v3}, Landroid/util/SparseArray;->size()I
/* if-nez v3, :cond_1 */
/* .line 84 */
v3 = this.userPackageStartTimeAndTokenArrayMap;
(( android.util.ArrayMap ) v3 ).remove ( v0 ); // invoke-virtual {v3, v0}, Landroid/util/ArrayMap;->remove(Ljava/lang/Object;)Ljava/lang/Object;
/* .line 87 */
} // .end local v2 # "userPackageBehaviorDuration":Lcom/miui/server/security/AppDurationService$UserPackageBehaviorDuration;
} // :cond_1
/* monitor-exit v1 */
/* .line 88 */
return;
/* .line 87 */
/* :catchall_0 */
/* move-exception v2 */
/* monitor-exit v1 */
/* :try_end_0 */
/* .catchall {:try_start_0 ..:try_end_0} :catchall_0 */
/* throw v2 */
} // .end method
