public class com.miui.server.security.GameBoosterImpl$GameBoosterServiceDeath implements android.os.IBinder$DeathRecipient {
	 /* .source "GameBoosterImpl.java" */
	 /* # interfaces */
	 /* # annotations */
	 /* .annotation system Ldalvik/annotation/EnclosingClass; */
	 /* value = Lcom/miui/server/security/GameBoosterImpl; */
} // .end annotation
/* .annotation system Ldalvik/annotation/InnerClass; */
/* accessFlags = 0x1 */
/* name = "GameBoosterServiceDeath" */
} // .end annotation
/* # instance fields */
public final android.os.IBinder mGameBoosterService;
private final com.miui.server.security.SecurityUserState mUserState;
final com.miui.server.security.GameBoosterImpl this$0; //synthetic
/* # direct methods */
public com.miui.server.security.GameBoosterImpl$GameBoosterServiceDeath ( ) {
/* .locals 0 */
/* .param p1, "this$0" # Lcom/miui/server/security/GameBoosterImpl; */
/* .param p2, "userState" # Lcom/miui/server/security/SecurityUserState; */
/* .param p3, "gameBoosterService" # Landroid/os/IBinder; */
/* .line 108 */
this.this$0 = p1;
/* invoke-direct {p0}, Ljava/lang/Object;-><init>()V */
/* .line 109 */
this.mUserState = p2;
/* .line 110 */
this.mGameBoosterService = p3;
/* .line 111 */
return;
} // .end method
/* # virtual methods */
public void binderDied ( ) {
/* .locals 4 */
/* .line 115 */
v0 = this.this$0;
com.miui.server.security.GameBoosterImpl .-$$Nest$fgetmService ( v0 );
v0 = this.mUserStateLock;
/* monitor-enter v0 */
/* .line 117 */
try { // :try_start_0
	 v1 = this.mGameBoosterService;
	 int v2 = 0; // const/4 v2, 0x0
	 /* .line 118 */
	 v1 = this.mUserState;
	 /* iput-boolean v2, v1, Lcom/miui/server/security/SecurityUserState;->mIsGameMode:Z */
	 /* .line 119 */
	 v1 = this.mUserState;
	 int v2 = 0; // const/4 v2, 0x0
	 this.gameBoosterServiceDeath = v2;
	 /* :try_end_0 */
	 /* .catch Ljava/lang/Exception; {:try_start_0 ..:try_end_0} :catch_0 */
	 /* .catchall {:try_start_0 ..:try_end_0} :catchall_0 */
	 /* .line 122 */
	 /* .line 123 */
	 /* :catchall_0 */
	 /* move-exception v1 */
	 /* .line 120 */
	 /* :catch_0 */
	 /* move-exception v1 */
	 /* .line 121 */
	 /* .local v1, "e":Ljava/lang/Exception; */
	 try { // :try_start_1
		 final String v2 = "GameBoosterImpl"; // const-string v2, "GameBoosterImpl"
		 final String v3 = "GameBoosterServiceDeath"; // const-string v3, "GameBoosterServiceDeath"
		 android.util.Log .e ( v2,v3,v1 );
		 /* .line 123 */
	 } // .end local v1 # "e":Ljava/lang/Exception;
} // :goto_0
/* monitor-exit v0 */
/* .line 124 */
return;
/* .line 123 */
} // :goto_1
/* monitor-exit v0 */
/* :try_end_1 */
/* .catchall {:try_start_1 ..:try_end_1} :catchall_0 */
/* throw v1 */
} // .end method
