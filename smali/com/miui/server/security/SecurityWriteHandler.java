public class com.miui.server.security.SecurityWriteHandler extends android.os.Handler {
	 /* .source "SecurityWriteHandler.java" */
	 /* # static fields */
	 public static final Integer REMOVE_AC_PACKAGE;
	 private static final java.lang.String TAG;
	 public static final Integer WRITE_BOOT_TIME;
	 public static final Integer WRITE_SETTINGS;
	 public static final Integer WRITE_WAKE_UP_TIME;
	 /* # instance fields */
	 private final android.content.Context mContext;
	 private final com.miui.server.SecurityManagerService mService;
	 /* # direct methods */
	 public com.miui.server.security.SecurityWriteHandler ( ) {
		 /* .locals 1 */
		 /* .param p1, "service" # Lcom/miui/server/SecurityManagerService; */
		 /* .param p2, "looper" # Landroid/os/Looper; */
		 /* .line 26 */
		 /* invoke-direct {p0, p2}, Landroid/os/Handler;-><init>(Landroid/os/Looper;)V */
		 /* .line 27 */
		 this.mService = p1;
		 /* .line 28 */
		 v0 = this.mContext;
		 this.mContext = v0;
		 /* .line 29 */
		 return;
	 } // .end method
	 /* # virtual methods */
	 public void handleMessage ( android.os.Message p0 ) {
		 /* .locals 7 */
		 /* .param p1, "msg" # Landroid/os/Message; */
		 /* .line 32 */
		 /* iget v0, p1, Landroid/os/Message;->what:I */
		 /* const/16 v1, 0xa */
		 int v2 = 0; // const/4 v2, 0x0
		 /* packed-switch v0, :pswitch_data_0 */
		 /* goto/16 :goto_1 */
		 /* .line 61 */
		 /* :pswitch_0 */
		 v0 = this.mService;
		 v0 = this.mUserStateLock;
		 /* monitor-enter v0 */
		 /* .line 62 */
		 try { // :try_start_0
			 /* iget v1, p1, Landroid/os/Message;->arg1:I */
			 /* .line 63 */
			 /* .local v1, "userId":I */
			 v2 = this.obj;
			 /* check-cast v2, Ljava/lang/String; */
			 /* .line 64 */
			 /* .local v2, "packageName":Ljava/lang/String; */
			 v3 = this.mService;
			 (( com.miui.server.SecurityManagerService ) v3 ).getUserStateLocked ( v1 ); // invoke-virtual {v3, v1}, Lcom/miui/server/SecurityManagerService;->getUserStateLocked(I)Lcom/miui/server/security/SecurityUserState;
			 /* .line 65 */
			 /* .local v3, "userState":Lcom/miui/server/security/SecurityUserState; */
			 v4 = this.mAccessControlCanceled;
			 (( android.util.ArraySet ) v4 ).remove ( v2 ); // invoke-virtual {v4, v2}, Landroid/util/ArraySet;->remove(Ljava/lang/Object;)Z
			 /* .line 66 */
			 /* nop */
		 } // .end local v1 # "userId":I
	 } // .end local v2 # "packageName":Ljava/lang/String;
} // .end local v3 # "userState":Lcom/miui/server/security/SecurityUserState;
/* monitor-exit v0 */
/* goto/16 :goto_1 */
/* :catchall_0 */
/* move-exception v1 */
/* monitor-exit v0 */
/* :try_end_0 */
/* .catchall {:try_start_0 ..:try_end_0} :catchall_0 */
/* throw v1 */
/* .line 48 */
/* :pswitch_1 */
android.os.Process .setThreadPriority ( v2 );
/* .line 49 */
int v0 = 3; // const/4 v0, 0x3
(( com.miui.server.security.SecurityWriteHandler ) p0 ).removeMessages ( v0 ); // invoke-virtual {p0, v0}, Lcom/miui/server/security/SecurityWriteHandler;->removeMessages(I)V
/* .line 50 */
/* const-string/jumbo v2, "vendor" */
v0 = miui.util.FeatureParser .hasFeature ( v2,v0 );
if ( v0 != null) { // if-eqz v0, :cond_0
	 /* .line 51 */
	 /* const-string/jumbo v0, "vendor" */
	 miui.util.FeatureParser .getString ( v0 );
	 /* .line 52 */
	 /* .local v0, "vendor":Ljava/lang/String; */
	 v2 = this.obj;
	 /* check-cast v2, Ljava/lang/Long; */
	 (( java.lang.Long ) v2 ).longValue ( ); // invoke-virtual {v2}, Ljava/lang/Long;->longValue()J
	 /* move-result-wide v2 */
	 /* .line 53 */
	 /* .local v2, "wakeTime":J */
	 v4 = this.mContext;
	 miui.security.SecurityManagerCompat .writeBootTime ( v4,v0,v2,v3 );
	 /* .line 54 */
	 final String v4 = "SecurityWriteHandler"; // const-string v4, "SecurityWriteHandler"
	 /* new-instance v5, Ljava/lang/StringBuilder; */
	 /* invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V */
	 final String v6 = "Wake up time updated "; // const-string v6, "Wake up time updated "
	 (( java.lang.StringBuilder ) v5 ).append ( v6 ); // invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
	 (( java.lang.StringBuilder ) v5 ).append ( v2, v3 ); // invoke-virtual {v5, v2, v3}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;
	 (( java.lang.StringBuilder ) v5 ).toString ( ); // invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
	 android.util.Log .d ( v4,v5 );
	 /* .line 55 */
} // .end local v0 # "vendor":Ljava/lang/String;
} // .end local v2 # "wakeTime":J
/* .line 56 */
} // :cond_0
final String v0 = "SecurityWriteHandler"; // const-string v0, "SecurityWriteHandler"
final String v2 = "There is no corresponding feature!"; // const-string v2, "There is no corresponding feature!"
android.util.Log .w ( v0,v2 );
/* .line 58 */
} // :goto_0
android.os.Process .setThreadPriority ( v1 );
/* .line 59 */
/* .line 42 */
/* :pswitch_2 */
android.os.Process .setThreadPriority ( v2 );
/* .line 43 */
int v0 = 2; // const/4 v0, 0x2
(( com.miui.server.security.SecurityWriteHandler ) p0 ).removeMessages ( v0 ); // invoke-virtual {p0, v0}, Lcom/miui/server/security/SecurityWriteHandler;->removeMessages(I)V
/* .line 44 */
v0 = this.mService;
v0 = this.mWakeUpTimeImpl;
(( com.miui.server.security.WakeUpTimeImpl ) v0 ).writeWakeUpTime ( ); // invoke-virtual {v0}, Lcom/miui/server/security/WakeUpTimeImpl;->writeWakeUpTime()V
/* .line 45 */
android.os.Process .setThreadPriority ( v1 );
/* .line 46 */
/* .line 34 */
/* :pswitch_3 */
android.os.Process .setThreadPriority ( v2 );
/* .line 35 */
v0 = this.mService;
v0 = this.mSettingsFile;
/* monitor-enter v0 */
/* .line 36 */
int v2 = 1; // const/4 v2, 0x1
try { // :try_start_1
(( com.miui.server.security.SecurityWriteHandler ) p0 ).removeMessages ( v2 ); // invoke-virtual {p0, v2}, Lcom/miui/server/security/SecurityWriteHandler;->removeMessages(I)V
/* .line 37 */
v2 = this.mService;
(( com.miui.server.SecurityManagerService ) v2 ).writeSettings ( ); // invoke-virtual {v2}, Lcom/miui/server/SecurityManagerService;->writeSettings()V
/* .line 38 */
/* monitor-exit v0 */
/* :try_end_1 */
/* .catchall {:try_start_1 ..:try_end_1} :catchall_1 */
/* .line 39 */
android.os.Process .setThreadPriority ( v1 );
/* .line 40 */
/* .line 38 */
/* :catchall_1 */
/* move-exception v1 */
try { // :try_start_2
/* monitor-exit v0 */
/* :try_end_2 */
/* .catchall {:try_start_2 ..:try_end_2} :catchall_1 */
/* throw v1 */
/* .line 69 */
} // :goto_1
return;
/* nop */
/* :pswitch_data_0 */
/* .packed-switch 0x1 */
/* :pswitch_3 */
/* :pswitch_2 */
/* :pswitch_1 */
/* :pswitch_0 */
} // .end packed-switch
} // .end method
