.class public Lcom/miui/server/security/AppBehaviorService;
.super Ljava/lang/Object;
.source "AppBehaviorService.java"


# static fields
.field private static final ACCESSIBILITY_URI:Landroid/net/Uri;

.field private static final COMPONENT_NAME_SEPARATOR:Ljava/lang/String; = ":"

.field public static final DEBUG:Z = false

.field private static final MAX_CACHE_COUNT:I = 0xc8

.field private static final MSG_BIND_SEC:I = 0xe15

.field private static final MSG_DURATION_TOO_LONG_CHECK:I = 0xe12

.field private static final MSG_FOREGROUND_CHECK:I = 0xe10

.field private static final MSG_TRANSPORT_END:I = 0xe14

.field private static final MSG_TRANSPORT_INFO:I = 0xe13

.field private static final TAG:Ljava/lang/String; = "AppBehaviorService"


# instance fields
.field private final mAlwaysRecordBehavior:Landroid/util/SparseBooleanArray;

.field private mAppBehaviorDuration:Lcom/miui/server/security/AppDurationService;

.field private final mAppOps:Landroid/app/AppOpsManager;

.field private final mBehaviorThreshold:Landroid/util/SparseLongArray;

.field private final mContext:Landroid/content/Context;

.field private final mDateFormat:Ljava/text/SimpleDateFormat;

.field private final mForegroundInfoListener:Lmiui/process/IForegroundInfoListener;

.field private final mForegroundRecorder:Landroid/util/ArrayMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/util/ArrayMap<",
            "Ljava/lang/String;",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation
.end field

.field private final mLastEnableAccessibility:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private final mPackageBehavior:Landroid/util/ArrayMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/util/ArrayMap<",
            "Ljava/lang/String;",
            "Lcom/miui/server/security/model/PackageBehaviorBean;",
            ">;"
        }
    .end annotation
.end field

.field protected final mPackageManagerInt:Landroid/content/pm/PackageManagerInternal;

.field private final mPackageRecordBehavior:Landroid/util/ArrayMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/util/ArrayMap<",
            "Ljava/lang/String;",
            "Ljava/util/Set<",
            "Ljava/lang/Integer;",
            ">;>;"
        }
    .end annotation
.end field

.field private mPersistMessenger:Landroid/os/Messenger;

.field private final mRecordEnableLock:Ljava/lang/Object;

.field private mRecordSize:I

.field private mRegisterAccessibility:Z

.field private mRegisterForeground:Z

.field private final mSettingsObserver:Landroid/database/ContentObserver;

.field private final mUidBehaviorLock:Ljava/lang/Object;

.field private final mWorkHandler:Landroid/os/Handler;


# direct methods
.method public static synthetic $r8$lambda$PFkDXwjlFgsN3peI9JXFMLsJK6U(Lcom/miui/server/security/AppBehaviorService;Lmiui/security/AppBehavior;)V
    .locals 0

    invoke-direct {p0, p1}, Lcom/miui/server/security/AppBehaviorService;->lambda$recordAppBehaviorAsync$0(Lmiui/security/AppBehavior;)V

    return-void
.end method

.method static bridge synthetic -$$Nest$fgetmAppOps(Lcom/miui/server/security/AppBehaviorService;)Landroid/app/AppOpsManager;
    .locals 0

    iget-object p0, p0, Lcom/miui/server/security/AppBehaviorService;->mAppOps:Landroid/app/AppOpsManager;

    return-object p0
.end method

.method static bridge synthetic -$$Nest$fgetmForegroundRecorder(Lcom/miui/server/security/AppBehaviorService;)Landroid/util/ArrayMap;
    .locals 0

    iget-object p0, p0, Lcom/miui/server/security/AppBehaviorService;->mForegroundRecorder:Landroid/util/ArrayMap;

    return-object p0
.end method

.method static bridge synthetic -$$Nest$fgetmWorkHandler(Lcom/miui/server/security/AppBehaviorService;)Landroid/os/Handler;
    .locals 0

    iget-object p0, p0, Lcom/miui/server/security/AppBehaviorService;->mWorkHandler:Landroid/os/Handler;

    return-object p0
.end method

.method static bridge synthetic -$$Nest$fputmPersistMessenger(Lcom/miui/server/security/AppBehaviorService;Landroid/os/Messenger;)V
    .locals 0

    iput-object p1, p0, Lcom/miui/server/security/AppBehaviorService;->mPersistMessenger:Landroid/os/Messenger;

    return-void
.end method

.method static bridge synthetic -$$Nest$mcheckForegroundCount(Lcom/miui/server/security/AppBehaviorService;)V
    .locals 0

    invoke-direct {p0}, Lcom/miui/server/security/AppBehaviorService;->checkForegroundCount()V

    return-void
.end method

.method static bridge synthetic -$$Nest$mcheckNewEnableAccessibility(Lcom/miui/server/security/AppBehaviorService;)V
    .locals 0

    invoke-direct {p0}, Lcom/miui/server/security/AppBehaviorService;->checkNewEnableAccessibility()V

    return-void
.end method

.method static bridge synthetic -$$Nest$mgetApplicationInfo(Lcom/miui/server/security/AppBehaviorService;Ljava/lang/String;I)Landroid/content/pm/ApplicationInfo;
    .locals 0

    invoke-direct {p0, p1, p2}, Lcom/miui/server/security/AppBehaviorService;->getApplicationInfo(Ljava/lang/String;I)Landroid/content/pm/ApplicationInfo;

    move-result-object p0

    return-object p0
.end method

.method static bridge synthetic -$$Nest$mscanDurationTooLong(Lcom/miui/server/security/AppBehaviorService;)V
    .locals 0

    invoke-direct {p0}, Lcom/miui/server/security/AppBehaviorService;->scanDurationTooLong()V

    return-void
.end method

.method static bridge synthetic -$$Nest$mschedulePersist(Lcom/miui/server/security/AppBehaviorService;)V
    .locals 0

    invoke-direct {p0}, Lcom/miui/server/security/AppBehaviorService;->schedulePersist()V

    return-void
.end method

.method static bridge synthetic -$$Nest$sfgetACCESSIBILITY_URI()Landroid/net/Uri;
    .locals 1

    sget-object v0, Lcom/miui/server/security/AppBehaviorService;->ACCESSIBILITY_URI:Landroid/net/Uri;

    return-object v0
.end method

.method static constructor <clinit>()V
    .locals 1

    .line 67
    const-string v0, "enabled_accessibility_services"

    invoke-static {v0}, Landroid/provider/Settings$Secure;->getUriFor(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v0

    sput-object v0, Lcom/miui/server/security/AppBehaviorService;->ACCESSIBILITY_URI:Landroid/net/Uri;

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/os/HandlerThread;)V
    .locals 4
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "thread"    # Landroid/os/HandlerThread;

    .line 115
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 78
    new-instance v0, Ljava/lang/Object;

    invoke-direct {v0}, Ljava/lang/Object;-><init>()V

    iput-object v0, p0, Lcom/miui/server/security/AppBehaviorService;->mUidBehaviorLock:Ljava/lang/Object;

    .line 79
    new-instance v0, Ljava/lang/Object;

    invoke-direct {v0}, Ljava/lang/Object;-><init>()V

    iput-object v0, p0, Lcom/miui/server/security/AppBehaviorService;->mRecordEnableLock:Ljava/lang/Object;

    .line 82
    new-instance v0, Ljava/text/SimpleDateFormat;

    const-string/jumbo v1, "yyyy-MM-dd HH:mm:ss"

    invoke-direct {v0, v1}, Ljava/text/SimpleDateFormat;-><init>(Ljava/lang/String;)V

    iput-object v0, p0, Lcom/miui/server/security/AppBehaviorService;->mDateFormat:Ljava/text/SimpleDateFormat;

    .line 88
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/miui/server/security/AppBehaviorService;->mRegisterForeground:Z

    .line 89
    iput-boolean v0, p0, Lcom/miui/server/security/AppBehaviorService;->mRegisterAccessibility:Z

    .line 93
    new-instance v0, Lcom/miui/server/security/AppBehaviorService$1;

    invoke-direct {v0, p0}, Lcom/miui/server/security/AppBehaviorService$1;-><init>(Lcom/miui/server/security/AppBehaviorService;)V

    iput-object v0, p0, Lcom/miui/server/security/AppBehaviorService;->mForegroundInfoListener:Lmiui/process/IForegroundInfoListener;

    .line 116
    iput-object p1, p0, Lcom/miui/server/security/AppBehaviorService;->mContext:Landroid/content/Context;

    .line 117
    const-string v0, "appops"

    invoke-virtual {p1, v0}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/app/AppOpsManager;

    iput-object v0, p0, Lcom/miui/server/security/AppBehaviorService;->mAppOps:Landroid/app/AppOpsManager;

    .line 118
    const-class v0, Landroid/content/pm/PackageManagerInternal;

    invoke-static {v0}, Lcom/android/server/LocalServices;->getService(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/content/pm/PackageManagerInternal;

    iput-object v0, p0, Lcom/miui/server/security/AppBehaviorService;->mPackageManagerInt:Landroid/content/pm/PackageManagerInternal;

    .line 119
    new-instance v0, Landroid/util/ArrayMap;

    invoke-direct {v0}, Landroid/util/ArrayMap;-><init>()V

    iput-object v0, p0, Lcom/miui/server/security/AppBehaviorService;->mForegroundRecorder:Landroid/util/ArrayMap;

    .line 120
    new-instance v0, Landroid/util/ArrayMap;

    invoke-direct {v0}, Landroid/util/ArrayMap;-><init>()V

    iput-object v0, p0, Lcom/miui/server/security/AppBehaviorService;->mPackageBehavior:Landroid/util/ArrayMap;

    .line 121
    new-instance v0, Landroid/util/SparseLongArray;

    invoke-direct {v0}, Landroid/util/SparseLongArray;-><init>()V

    iput-object v0, p0, Lcom/miui/server/security/AppBehaviorService;->mBehaviorThreshold:Landroid/util/SparseLongArray;

    .line 122
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    iput-object v1, p0, Lcom/miui/server/security/AppBehaviorService;->mLastEnableAccessibility:Ljava/util/List;

    .line 123
    new-instance v1, Landroid/util/SparseBooleanArray;

    invoke-direct {v1}, Landroid/util/SparseBooleanArray;-><init>()V

    iput-object v1, p0, Lcom/miui/server/security/AppBehaviorService;->mAlwaysRecordBehavior:Landroid/util/SparseBooleanArray;

    .line 124
    new-instance v1, Landroid/util/ArrayMap;

    invoke-direct {v1}, Landroid/util/ArrayMap;-><init>()V

    iput-object v1, p0, Lcom/miui/server/security/AppBehaviorService;->mPackageRecordBehavior:Landroid/util/ArrayMap;

    .line 125
    new-instance v1, Lcom/miui/server/security/AppBehaviorService$2;

    invoke-virtual {p2}, Landroid/os/HandlerThread;->getLooper()Landroid/os/Looper;

    move-result-object v2

    invoke-direct {v1, p0, v2}, Lcom/miui/server/security/AppBehaviorService$2;-><init>(Lcom/miui/server/security/AppBehaviorService;Landroid/os/Looper;)V

    iput-object v1, p0, Lcom/miui/server/security/AppBehaviorService;->mWorkHandler:Landroid/os/Handler;

    .line 137
    new-instance v2, Lcom/miui/server/security/AppBehaviorService$3;

    invoke-direct {v2, p0, v1}, Lcom/miui/server/security/AppBehaviorService$3;-><init>(Lcom/miui/server/security/AppBehaviorService;Landroid/os/Handler;)V

    iput-object v2, p0, Lcom/miui/server/security/AppBehaviorService;->mSettingsObserver:Landroid/database/ContentObserver;

    .line 146
    const/16 v1, 0x23

    const-wide/16 v2, 0x14

    invoke-virtual {v0, v1, v2, v3}, Landroid/util/SparseLongArray;->put(IJ)V

    .line 147
    const/16 v1, 0x25

    const-wide/16 v2, 0xa

    invoke-virtual {v0, v1, v2, v3}, Landroid/util/SparseLongArray;->put(IJ)V

    .line 148
    return-void
.end method

.method private checkForegroundCount()V
    .locals 7

    .line 503
    const-string v0, "AppBehaviorService"

    const-string/jumbo v1, "start checkForegroundPackageRecord with threshold"

    invoke-static {v0, v1}, Landroid/util/Slog;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 504
    iget-object v0, p0, Lcom/miui/server/security/AppBehaviorService;->mForegroundRecorder:Landroid/util/ArrayMap;

    monitor-enter v0

    .line 505
    const/4 v1, 0x0

    .local v1, "i":I
    :goto_0
    :try_start_0
    iget-object v2, p0, Lcom/miui/server/security/AppBehaviorService;->mForegroundRecorder:Landroid/util/ArrayMap;

    invoke-virtual {v2}, Landroid/util/ArrayMap;->size()I

    move-result v2

    if-ge v1, v2, :cond_1

    .line 506
    iget-object v2, p0, Lcom/miui/server/security/AppBehaviorService;->mForegroundRecorder:Landroid/util/ArrayMap;

    invoke-virtual {v2, v1}, Landroid/util/ArrayMap;->valueAt(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/lang/Integer;

    invoke-virtual {v2}, Ljava/lang/Integer;->intValue()I

    move-result v2

    int-to-long v2, v2

    const/16 v4, 0x23

    invoke-virtual {p0, v4, v2, v3}, Lcom/miui/server/security/AppBehaviorService;->moreThanThreshold(IJ)Z

    move-result v2

    if-eqz v2, :cond_0

    .line 507
    iget-object v2, p0, Lcom/miui/server/security/AppBehaviorService;->mForegroundRecorder:Landroid/util/ArrayMap;

    .line 508
    invoke-virtual {v2, v1}, Landroid/util/ArrayMap;->keyAt(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/lang/String;

    .line 507
    const-wide/16 v5, 0x1

    const/4 v3, 0x0

    invoke-static {v4, v2, v5, v6, v3}, Lmiui/security/AppBehavior;->buildCountEvent(ILjava/lang/String;JLjava/lang/String;)Lmiui/security/AppBehavior;

    move-result-object v2

    invoke-virtual {p0, v2}, Lcom/miui/server/security/AppBehaviorService;->recordAppBehaviorAsync(Lmiui/security/AppBehavior;)V

    .line 505
    :cond_0
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 511
    .end local v1    # "i":I
    :cond_1
    iget-object v1, p0, Lcom/miui/server/security/AppBehaviorService;->mForegroundRecorder:Landroid/util/ArrayMap;

    invoke-virtual {v1}, Landroid/util/ArrayMap;->clear()V

    .line 512
    monitor-exit v0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 513
    iget-object v0, p0, Lcom/miui/server/security/AppBehaviorService;->mWorkHandler:Landroid/os/Handler;

    const/16 v1, 0xe10

    const-wide/32 v2, 0x36ee80

    invoke-virtual {v0, v1, v2, v3}, Landroid/os/Handler;->sendEmptyMessageDelayed(IJ)Z

    .line 514
    return-void

    .line 512
    :catchall_0
    move-exception v1

    :try_start_1
    monitor-exit v0
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    throw v1
.end method

.method private checkNewEnableAccessibility()V
    .locals 12

    .line 529
    iget-object v0, p0, Lcom/miui/server/security/AppBehaviorService;->mContext:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    const-string v1, "enabled_accessibility_services"

    invoke-static {v0, v1}, Landroid/provider/Settings$Secure;->getString(Landroid/content/ContentResolver;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 530
    .local v0, "components":Ljava/lang/String;
    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-nez v1, :cond_3

    .line 531
    const-string v1, ":"

    invoke-virtual {v0, v1}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v1

    .line 532
    .local v1, "componentsArray":[Ljava/lang/String;
    array-length v2, v1

    const/4 v3, 0x0

    :goto_0
    if-ge v3, v2, :cond_2

    aget-object v4, v1, v3

    .line 533
    .local v4, "component":Ljava/lang/String;
    iget-object v5, p0, Lcom/miui/server/security/AppBehaviorService;->mLastEnableAccessibility:Ljava/util/List;

    invoke-interface {v5, v4}, Ljava/util/List;->contains(Ljava/lang/Object;)Z

    move-result v5

    .line 537
    .local v5, "alreadyHave":Z
    if-eqz v5, :cond_0

    .line 538
    goto :goto_1

    .line 540
    :cond_0
    invoke-static {v4}, Landroid/content/ComponentName;->unflattenFromString(Ljava/lang/String;)Landroid/content/ComponentName;

    move-result-object v6

    .line 541
    .local v6, "service":Landroid/content/ComponentName;
    if-eqz v6, :cond_1

    .line 542
    nop

    .line 544
    invoke-virtual {v6}, Landroid/content/ComponentName;->getPackageName()Ljava/lang/String;

    move-result-object v7

    .line 545
    invoke-virtual {v6}, Landroid/content/ComponentName;->flattenToShortString()Ljava/lang/String;

    move-result-object v8

    .line 542
    const/16 v9, 0x1d

    const-wide/16 v10, 0x1

    invoke-static {v9, v7, v10, v11, v8}, Lmiui/security/AppBehavior;->buildCountEvent(ILjava/lang/String;JLjava/lang/String;)Lmiui/security/AppBehavior;

    move-result-object v7

    invoke-virtual {p0, v7}, Lcom/miui/server/security/AppBehaviorService;->recordAppBehaviorAsync(Lmiui/security/AppBehavior;)V

    .line 532
    .end local v4    # "component":Ljava/lang/String;
    .end local v5    # "alreadyHave":Z
    .end local v6    # "service":Landroid/content/ComponentName;
    :cond_1
    :goto_1
    add-int/lit8 v3, v3, 0x1

    goto :goto_0

    .line 548
    :cond_2
    iget-object v2, p0, Lcom/miui/server/security/AppBehaviorService;->mLastEnableAccessibility:Ljava/util/List;

    invoke-interface {v2}, Ljava/util/List;->clear()V

    .line 549
    iget-object v2, p0, Lcom/miui/server/security/AppBehaviorService;->mLastEnableAccessibility:Ljava/util/List;

    invoke-static {v1}, Ljava/util/Arrays;->asList([Ljava/lang/Object;)Ljava/util/List;

    move-result-object v3

    invoke-interface {v2, v3}, Ljava/util/List;->addAll(Ljava/util/Collection;)Z

    .line 550
    .end local v1    # "componentsArray":[Ljava/lang/String;
    goto :goto_2

    .line 551
    :cond_3
    iget-object v1, p0, Lcom/miui/server/security/AppBehaviorService;->mLastEnableAccessibility:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->clear()V

    .line 553
    :goto_2
    return-void
.end method

.method private getApplicationInfo(Ljava/lang/String;I)Landroid/content/pm/ApplicationInfo;
    .locals 8
    .param p1, "pkgName"    # Ljava/lang/String;
    .param p2, "userId"    # I

    .line 454
    invoke-static {}, Landroid/os/Binder;->clearCallingIdentity()J

    move-result-wide v0

    .line 456
    .local v0, "callingIdentity":J
    :try_start_0
    iget-object v2, p0, Lcom/miui/server/security/AppBehaviorService;->mPackageManagerInt:Landroid/content/pm/PackageManagerInternal;

    const-wide/16 v4, 0x0

    const/16 v6, 0x3e8

    move-object v3, p1

    move v7, p2

    invoke-virtual/range {v2 .. v7}, Landroid/content/pm/PackageManagerInternal;->getApplicationInfo(Ljava/lang/String;JII)Landroid/content/pm/ApplicationInfo;

    move-result-object v2
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 458
    .local v2, "info":Landroid/content/pm/ApplicationInfo;
    invoke-static {v0, v1}, Landroid/os/Binder;->restoreCallingIdentity(J)V

    .line 459
    nop

    .line 460
    return-object v2

    .line 458
    .end local v2    # "info":Landroid/content/pm/ApplicationInfo;
    :catchall_0
    move-exception v2

    invoke-static {v0, v1}, Landroid/os/Binder;->restoreCallingIdentity(J)V

    .line 459
    throw v2
.end method

.method public static isSystem(Landroid/content/pm/ApplicationInfo;)Z
    .locals 2
    .param p0, "applicationInfo"    # Landroid/content/pm/ApplicationInfo;

    .line 464
    if-eqz p0, :cond_1

    invoke-virtual {p0}, Landroid/content/pm/ApplicationInfo;->isSystemApp()Z

    move-result v0

    if-nez v0, :cond_0

    iget v0, p0, Landroid/content/pm/ApplicationInfo;->uid:I

    .line 465
    invoke-static {v0}, Landroid/os/UserHandle;->getAppId(I)I

    move-result v0

    const/16 v1, 0x2710

    if-ge v0, v1, :cond_1

    :cond_0
    const/4 v0, 0x1

    goto :goto_0

    :cond_1
    const/4 v0, 0x0

    .line 464
    :goto_0
    return v0
.end method

.method private synthetic lambda$recordAppBehaviorAsync$0(Lmiui/security/AppBehavior;)V
    .locals 0
    .param p1, "appBehavior"    # Lmiui/security/AppBehavior;

    .line 347
    invoke-virtual {p0, p1}, Lcom/miui/server/security/AppBehaviorService;->recordAppBehavior(Lmiui/security/AppBehavior;)V

    return-void
.end method

.method private registerAccessibilityListener(Z)V
    .locals 4
    .param p1, "register"    # Z

    .line 517
    const/4 v0, 0x0

    if-eqz p1, :cond_0

    iget-boolean v1, p0, Lcom/miui/server/security/AppBehaviorService;->mRegisterAccessibility:Z

    if-nez v1, :cond_0

    .line 518
    const/4 v1, 0x1

    iput-boolean v1, p0, Lcom/miui/server/security/AppBehaviorService;->mRegisterAccessibility:Z

    .line 519
    iget-object v1, p0, Lcom/miui/server/security/AppBehaviorService;->mContext:Landroid/content/Context;

    invoke-virtual {v1}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v1

    sget-object v2, Lcom/miui/server/security/AppBehaviorService;->ACCESSIBILITY_URI:Landroid/net/Uri;

    iget-object v3, p0, Lcom/miui/server/security/AppBehaviorService;->mSettingsObserver:Landroid/database/ContentObserver;

    invoke-virtual {v1, v2, v0, v3}, Landroid/content/ContentResolver;->registerContentObserver(Landroid/net/Uri;ZLandroid/database/ContentObserver;)V

    .line 520
    invoke-direct {p0}, Lcom/miui/server/security/AppBehaviorService;->checkNewEnableAccessibility()V

    goto :goto_0

    .line 521
    :cond_0
    iget-boolean v1, p0, Lcom/miui/server/security/AppBehaviorService;->mRegisterAccessibility:Z

    if-eqz v1, :cond_1

    if-nez p1, :cond_1

    .line 522
    iget-object v1, p0, Lcom/miui/server/security/AppBehaviorService;->mContext:Landroid/content/Context;

    invoke-virtual {v1}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v1

    iget-object v2, p0, Lcom/miui/server/security/AppBehaviorService;->mSettingsObserver:Landroid/database/ContentObserver;

    invoke-virtual {v1, v2}, Landroid/content/ContentResolver;->unregisterContentObserver(Landroid/database/ContentObserver;)V

    .line 523
    iget-object v1, p0, Lcom/miui/server/security/AppBehaviorService;->mLastEnableAccessibility:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->clear()V

    .line 524
    iput-boolean v0, p0, Lcom/miui/server/security/AppBehaviorService;->mRegisterAccessibility:Z

    .line 526
    :cond_1
    :goto_0
    return-void
.end method

.method private registerForegroundRecordTask(Z)V
    .locals 4
    .param p1, "register"    # Z

    .line 486
    const/16 v0, 0xe10

    if-eqz p1, :cond_0

    .line 487
    iget-boolean v1, p0, Lcom/miui/server/security/AppBehaviorService;->mRegisterForeground:Z

    if-nez v1, :cond_1

    .line 488
    iget-object v1, p0, Lcom/miui/server/security/AppBehaviorService;->mWorkHandler:Landroid/os/Handler;

    const-wide/32 v2, 0x36ee80

    invoke-virtual {v1, v0, v2, v3}, Landroid/os/Handler;->sendEmptyMessageDelayed(IJ)Z

    .line 489
    iget-object v0, p0, Lcom/miui/server/security/AppBehaviorService;->mForegroundInfoListener:Lmiui/process/IForegroundInfoListener;

    invoke-static {v0}, Lmiui/process/ProcessManager;->registerForegroundInfoListener(Lmiui/process/IForegroundInfoListener;)V

    .line 490
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/miui/server/security/AppBehaviorService;->mRegisterForeground:Z

    goto :goto_0

    .line 493
    :cond_0
    iget-boolean v1, p0, Lcom/miui/server/security/AppBehaviorService;->mRegisterForeground:Z

    if-eqz v1, :cond_1

    .line 494
    iget-object v1, p0, Lcom/miui/server/security/AppBehaviorService;->mForegroundRecorder:Landroid/util/ArrayMap;

    invoke-virtual {v1}, Landroid/util/ArrayMap;->clear()V

    .line 495
    iget-object v1, p0, Lcom/miui/server/security/AppBehaviorService;->mWorkHandler:Landroid/os/Handler;

    invoke-virtual {v1, v0}, Landroid/os/Handler;->removeMessages(I)V

    .line 496
    iget-object v0, p0, Lcom/miui/server/security/AppBehaviorService;->mForegroundInfoListener:Lmiui/process/IForegroundInfoListener;

    invoke-static {v0}, Lmiui/process/ProcessManager;->unregisterForegroundInfoListener(Lmiui/process/IForegroundInfoListener;)V

    .line 497
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/miui/server/security/AppBehaviorService;->mRegisterForeground:Z

    .line 500
    :cond_1
    :goto_0
    return-void
.end method

.method private registerListenerForBehavior(IZ)V
    .locals 1
    .param p1, "behavior"    # I
    .param p2, "register"    # Z

    .line 473
    const/16 v0, 0x23

    if-ne v0, p1, :cond_0

    .line 474
    invoke-direct {p0, p2}, Lcom/miui/server/security/AppBehaviorService;->registerForegroundRecordTask(Z)V

    goto :goto_0

    .line 475
    :cond_0
    const/16 v0, 0x1d

    if-ne v0, p1, :cond_1

    .line 476
    invoke-direct {p0, p2}, Lcom/miui/server/security/AppBehaviorService;->registerAccessibilityListener(Z)V

    goto :goto_0

    .line 477
    :cond_1
    const/16 v0, 0x20

    if-eq v0, p1, :cond_2

    const/16 v0, 0x16

    if-eq v0, p1, :cond_2

    const/16 v0, 0x17

    if-eq v0, p1, :cond_2

    const/16 v0, 0x19

    if-ne v0, p1, :cond_3

    .line 481
    :cond_2
    invoke-direct {p0, p2}, Lcom/miui/server/security/AppBehaviorService;->registerScanDurationTooLong(Z)V

    .line 483
    :cond_3
    :goto_0
    return-void
.end method

.method private registerScanDurationTooLong(Z)V
    .locals 4
    .param p1, "register"    # Z

    .line 556
    const/16 v0, 0xe12

    if-eqz p1, :cond_0

    iget-object v1, p0, Lcom/miui/server/security/AppBehaviorService;->mWorkHandler:Landroid/os/Handler;

    invoke-virtual {v1, v0}, Landroid/os/Handler;->hasMessages(I)Z

    move-result v1

    if-nez v1, :cond_0

    .line 557
    iget-object v1, p0, Lcom/miui/server/security/AppBehaviorService;->mWorkHandler:Landroid/os/Handler;

    const-wide/32 v2, 0x2932e00

    invoke-virtual {v1, v0, v2, v3}, Landroid/os/Handler;->sendEmptyMessageDelayed(IJ)Z

    goto :goto_0

    .line 558
    :cond_0
    if-nez p1, :cond_1

    .line 559
    iget-object v1, p0, Lcom/miui/server/security/AppBehaviorService;->mWorkHandler:Landroid/os/Handler;

    invoke-virtual {v1, v0}, Landroid/os/Handler;->removeMessages(I)V

    .line 561
    :cond_1
    :goto_0
    return-void
.end method

.method private scanDurationTooLong()V
    .locals 4

    .line 564
    iget-object v0, p0, Lcom/miui/server/security/AppBehaviorService;->mWorkHandler:Landroid/os/Handler;

    const/16 v1, 0xe12

    invoke-virtual {v0, v1}, Landroid/os/Handler;->removeMessages(I)V

    .line 565
    iget-object v0, p0, Lcom/miui/server/security/AppBehaviorService;->mWorkHandler:Landroid/os/Handler;

    const-wide/32 v2, 0x2932e00

    invoke-virtual {v0, v1, v2, v3}, Landroid/os/Handler;->sendEmptyMessageDelayed(IJ)Z

    .line 566
    iget-object v0, p0, Lcom/miui/server/security/AppBehaviorService;->mAppBehaviorDuration:Lcom/miui/server/security/AppDurationService;

    invoke-virtual {v0}, Lcom/miui/server/security/AppDurationService;->checkIfTooLongDuration()V

    .line 567
    return-void
.end method

.method private schedulePersist()V
    .locals 11

    .line 297
    const-string v0, "AppBehaviorService"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v2, "start schedulePersist, size may be:"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget v2, p0, Lcom/miui/server/security/AppBehaviorService;->mRecordSize:I

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Slog;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 298
    iget-object v0, p0, Lcom/miui/server/security/AppBehaviorService;->mPersistMessenger:Landroid/os/Messenger;

    if-eqz v0, :cond_3

    .line 299
    iget-object v0, p0, Lcom/miui/server/security/AppBehaviorService;->mUidBehaviorLock:Ljava/lang/Object;

    monitor-enter v0

    .line 300
    :try_start_0
    iget-object v1, p0, Lcom/miui/server/security/AppBehaviorService;->mDateFormat:Ljava/text/SimpleDateFormat;

    new-instance v2, Ljava/util/Date;

    invoke-direct {v2}, Ljava/util/Date;-><init>()V

    invoke-virtual {v1, v2}, Ljava/text/SimpleDateFormat;->format(Ljava/util/Date;)Ljava/lang/String;

    move-result-object v1

    .line 301
    .local v1, "fileName":Ljava/lang/String;
    const/4 v2, 0x0

    .local v2, "i":I
    :goto_0
    iget-object v3, p0, Lcom/miui/server/security/AppBehaviorService;->mPackageBehavior:Landroid/util/ArrayMap;

    invoke-virtual {v3}, Landroid/util/ArrayMap;->size()I

    move-result v3

    if-ge v2, v3, :cond_2

    .line 302
    iget-object v3, p0, Lcom/miui/server/security/AppBehaviorService;->mPackageBehavior:Landroid/util/ArrayMap;

    invoke-virtual {v3, v2}, Landroid/util/ArrayMap;->keyAt(I)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/lang/String;

    .line 303
    .local v3, "pkgName":Ljava/lang/String;
    iget-object v4, p0, Lcom/miui/server/security/AppBehaviorService;->mPackageBehavior:Landroid/util/ArrayMap;

    invoke-virtual {v4, v3}, Landroid/util/ArrayMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lcom/miui/server/security/model/PackageBehaviorBean;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 305
    .local v4, "packageBehaviorBean":Lcom/miui/server/security/model/PackageBehaviorBean;
    if-eqz v4, :cond_0

    .line 306
    :try_start_1
    invoke-virtual {v4}, Lcom/miui/server/security/model/PackageBehaviorBean;->toJson()Lorg/json/JSONObject;

    move-result-object v5

    .line 307
    .local v5, "record":Lorg/json/JSONObject;
    iget v6, p0, Lcom/miui/server/security/AppBehaviorService;->mRecordSize:I

    invoke-virtual {v4}, Lcom/miui/server/security/model/PackageBehaviorBean;->init()I

    move-result v7

    sub-int/2addr v6, v7

    iput v6, p0, Lcom/miui/server/security/AppBehaviorService;->mRecordSize:I

    .line 308
    const/4 v7, 0x0

    invoke-static {v6, v7}, Ljava/lang/Math;->max(II)I

    move-result v6

    iput v6, p0, Lcom/miui/server/security/AppBehaviorService;->mRecordSize:I

    .line 312
    if-eqz v5, :cond_0

    invoke-virtual {v5}, Lorg/json/JSONObject;->length()I

    move-result v6

    if-lez v6, :cond_0

    .line 314
    new-instance v6, Lorg/json/JSONObject;

    invoke-direct {v6}, Lorg/json/JSONObject;-><init>()V

    .line 315
    .local v6, "packageRecord":Lorg/json/JSONObject;
    invoke-virtual {v6, v3, v5}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;

    .line 316
    invoke-static {}, Landroid/os/Message;->obtain()Landroid/os/Message;

    move-result-object v7

    .line 317
    .local v7, "message":Landroid/os/Message;
    const/16 v8, 0xe13

    iput v8, v7, Landroid/os/Message;->what:I

    .line 318
    new-instance v8, Landroid/os/Bundle;

    invoke-direct {v8}, Landroid/os/Bundle;-><init>()V

    .line 319
    .local v8, "bundle":Landroid/os/Bundle;
    const-string v9, "android.intent.extra.TITLE"

    invoke-virtual {v8, v9, v1}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 320
    const-string v9, "android.intent.extra.RETURN_RESULT"

    invoke-virtual {v6}, Lorg/json/JSONObject;->toString()Ljava/lang/String;

    move-result-object v10

    invoke-virtual {v8, v9, v10}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 321
    invoke-virtual {v7, v8}, Landroid/os/Message;->setData(Landroid/os/Bundle;)V

    .line 322
    iget-object v9, p0, Lcom/miui/server/security/AppBehaviorService;->mPersistMessenger:Landroid/os/Messenger;

    invoke-virtual {v9, v7}, Landroid/os/Messenger;->send(Landroid/os/Message;)V
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_0
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_1

    .line 325
    .end local v5    # "record":Lorg/json/JSONObject;
    .end local v6    # "packageRecord":Lorg/json/JSONObject;
    .end local v7    # "message":Landroid/os/Message;
    .end local v8    # "bundle":Landroid/os/Bundle;
    :catch_0
    move-exception v5

    .line 326
    .local v5, "e":Ljava/lang/Exception;
    :try_start_2
    const-string v6, "AppBehaviorService"

    const-string v7, "persist exception!"

    invoke-static {v6, v7, v5}, Landroid/util/Slog;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    .line 327
    iget-object v6, p0, Lcom/miui/server/security/AppBehaviorService;->mPersistMessenger:Landroid/os/Messenger;

    if-nez v6, :cond_1

    .line 328
    invoke-virtual {p0}, Lcom/miui/server/security/AppBehaviorService;->persistBehaviorRecord()V

    .line 329
    monitor-exit v0

    return-void

    .line 331
    .end local v5    # "e":Ljava/lang/Exception;
    :cond_0
    :goto_1
    nop

    .line 301
    .end local v3    # "pkgName":Ljava/lang/String;
    .end local v4    # "packageBehaviorBean":Lcom/miui/server/security/model/PackageBehaviorBean;
    :cond_1
    add-int/lit8 v2, v2, 0x1

    goto :goto_0

    .line 333
    .end local v2    # "i":I
    :cond_2
    invoke-static {}, Landroid/os/Message;->obtain()Landroid/os/Message;

    move-result-object v2

    .line 334
    .local v2, "message":Landroid/os/Message;
    const/16 v3, 0xe14

    iput v3, v2, Landroid/os/Message;->what:I
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    .line 336
    :try_start_3
    iget-object v3, p0, Lcom/miui/server/security/AppBehaviorService;->mPersistMessenger:Landroid/os/Messenger;

    invoke-virtual {v3, v2}, Landroid/os/Messenger;->send(Landroid/os/Message;)V
    :try_end_3
    .catch Ljava/lang/Exception; {:try_start_3 .. :try_end_3} :catch_1
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    .line 338
    goto :goto_2

    .line 337
    :catch_1
    move-exception v3

    .line 339
    .end local v1    # "fileName":Ljava/lang/String;
    .end local v2    # "message":Landroid/os/Message;
    :goto_2
    :try_start_4
    monitor-exit v0

    goto :goto_3

    :catchall_0
    move-exception v1

    monitor-exit v0
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_0

    throw v1

    .line 341
    :cond_3
    :goto_3
    return-void
.end method

.method private scheduleRecord(ILjava/lang/String;Ljava/lang/String;J)V
    .locals 5
    .param p1, "behavior"    # I
    .param p2, "willBeRecordPackageName"    # Ljava/lang/String;
    .param p3, "willBeRecordData"    # Ljava/lang/String;
    .param p4, "willBeRecordCount"    # J

    .line 399
    iget-object v0, p0, Lcom/miui/server/security/AppBehaviorService;->mUidBehaviorLock:Ljava/lang/Object;

    monitor-enter v0

    .line 400
    :try_start_0
    iget-object v1, p0, Lcom/miui/server/security/AppBehaviorService;->mPackageBehavior:Landroid/util/ArrayMap;

    invoke-virtual {v1, p2}, Landroid/util/ArrayMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/miui/server/security/model/PackageBehaviorBean;

    .line 401
    .local v1, "packageBehaviorBean":Lcom/miui/server/security/model/PackageBehaviorBean;
    if-nez v1, :cond_0

    .line 402
    new-instance v2, Lcom/miui/server/security/model/PackageBehaviorBean;

    invoke-direct {v2}, Lcom/miui/server/security/model/PackageBehaviorBean;-><init>()V

    move-object v1, v2

    .line 403
    iget-object v2, p0, Lcom/miui/server/security/AppBehaviorService;->mPackageBehavior:Landroid/util/ArrayMap;

    invoke-virtual {v2, p2, v1}, Landroid/util/ArrayMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 405
    :cond_0
    invoke-virtual {v1, p1, p3, p4, p5}, Lcom/miui/server/security/model/PackageBehaviorBean;->recordAppBehavior(ILjava/lang/String;J)Z

    move-result v2

    .line 406
    .local v2, "addCount":Z
    if-eqz v2, :cond_1

    .line 407
    iget v3, p0, Lcom/miui/server/security/AppBehaviorService;->mRecordSize:I

    add-int/lit8 v3, v3, 0x1

    iput v3, p0, Lcom/miui/server/security/AppBehaviorService;->mRecordSize:I

    .line 417
    :cond_1
    iget v3, p0, Lcom/miui/server/security/AppBehaviorService;->mRecordSize:I

    const/16 v4, 0xc8

    if-lt v3, v4, :cond_2

    .line 418
    invoke-virtual {p0}, Lcom/miui/server/security/AppBehaviorService;->persistBehaviorRecord()V

    .line 420
    .end local v1    # "packageBehaviorBean":Lcom/miui/server/security/model/PackageBehaviorBean;
    .end local v2    # "addCount":Z
    :cond_2
    monitor-exit v0

    .line 421
    return-void

    .line 420
    :catchall_0
    move-exception v1

    monitor-exit v0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v1
.end method


# virtual methods
.method protected canRecordBehavior(ILjava/lang/String;)Z
    .locals 5
    .param p1, "behavior"    # I
    .param p2, "pkgName"    # Ljava/lang/String;

    .line 248
    iget-object v0, p0, Lcom/miui/server/security/AppBehaviorService;->mRecordEnableLock:Ljava/lang/Object;

    monitor-enter v0

    .line 249
    :try_start_0
    iget-object v1, p0, Lcom/miui/server/security/AppBehaviorService;->mAlwaysRecordBehavior:Landroid/util/SparseBooleanArray;

    invoke-virtual {v1, p1}, Landroid/util/SparseBooleanArray;->indexOfKey(I)I

    move-result v1

    const/4 v2, 0x0

    if-ltz v1, :cond_2

    .line 250
    iget-object v1, p0, Lcom/miui/server/security/AppBehaviorService;->mAlwaysRecordBehavior:Landroid/util/SparseBooleanArray;

    invoke-virtual {v1, p1}, Landroid/util/SparseBooleanArray;->get(I)Z

    move-result v1

    const/4 v3, 0x1

    if-eqz v1, :cond_0

    .line 251
    monitor-exit v0

    return v3

    .line 253
    :cond_0
    invoke-direct {p0, p2, v2}, Lcom/miui/server/security/AppBehaviorService;->getApplicationInfo(Ljava/lang/String;I)Landroid/content/pm/ApplicationInfo;

    move-result-object v1

    invoke-static {v1}, Lcom/miui/server/security/AppBehaviorService;->isSystem(Landroid/content/pm/ApplicationInfo;)Z

    move-result v1

    if-nez v1, :cond_1

    move v2, v3

    :cond_1
    monitor-exit v0

    return v2

    .line 255
    :cond_2
    invoke-direct {p0, p2, v2}, Lcom/miui/server/security/AppBehaviorService;->getApplicationInfo(Ljava/lang/String;I)Landroid/content/pm/ApplicationInfo;

    move-result-object v1

    .line 256
    .local v1, "applicationInfo":Landroid/content/pm/ApplicationInfo;
    if-nez v1, :cond_3

    .line 257
    monitor-exit v0

    return v2

    .line 259
    :cond_3
    iget-object v3, p0, Lcom/miui/server/security/AppBehaviorService;->mPackageRecordBehavior:Landroid/util/ArrayMap;

    invoke-virtual {v3, p2}, Landroid/util/ArrayMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/util/Set;

    .line 260
    .local v3, "behaviorWatching":Ljava/util/Set;, "Ljava/util/Set<Ljava/lang/Integer;>;"
    if-eqz v3, :cond_5

    invoke-interface {v3}, Ljava/util/Set;->isEmpty()Z

    move-result v4

    if-eqz v4, :cond_4

    goto :goto_0

    .line 263
    :cond_4
    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-interface {v3, v2}, Ljava/util/Set;->contains(Ljava/lang/Object;)Z

    move-result v2

    monitor-exit v0

    return v2

    .line 261
    :cond_5
    :goto_0
    monitor-exit v0

    return v2

    .line 264
    .end local v1    # "applicationInfo":Landroid/content/pm/ApplicationInfo;
    .end local v3    # "behaviorWatching":Ljava/util/Set;, "Ljava/util/Set<Ljava/lang/Integer;>;"
    :catchall_0
    move-exception v1

    monitor-exit v0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v1
.end method

.method public dump(Ljava/io/PrintWriter;)V
    .locals 7
    .param p1, "pw"    # Ljava/io/PrintWriter;

    .line 424
    const-string v0, "==================Dump AppBehaviorService start=================="

    invoke-virtual {p1, v0}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V

    .line 425
    iget-object v0, p0, Lcom/miui/server/security/AppBehaviorService;->mRecordEnableLock:Ljava/lang/Object;

    monitor-enter v0

    .line 426
    :try_start_0
    const-string v1, "AlwaysRecordBehavior :"

    invoke-virtual {p1, v1}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V

    .line 427
    const/4 v1, 0x0

    .local v1, "i":I
    :goto_0
    iget-object v2, p0, Lcom/miui/server/security/AppBehaviorService;->mAlwaysRecordBehavior:Landroid/util/SparseBooleanArray;

    invoke-virtual {v2}, Landroid/util/SparseBooleanArray;->size()I

    move-result v2

    if-ge v1, v2, :cond_0

    .line 428
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "    behavior:"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget-object v3, p0, Lcom/miui/server/security/AppBehaviorService;->mAlwaysRecordBehavior:Landroid/util/SparseBooleanArray;

    invoke-virtual {v3, v1}, Landroid/util/SparseBooleanArray;->keyAt(I)I

    move-result v3

    invoke-static {v3}, Lmiui/security/AppBehavior;->behaviorToName(I)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, " include system:"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget-object v3, p0, Lcom/miui/server/security/AppBehaviorService;->mAlwaysRecordBehavior:Landroid/util/SparseBooleanArray;

    invoke-virtual {v3, v1}, Landroid/util/SparseBooleanArray;->valueAt(I)Z

    move-result v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {p1, v2}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V

    .line 427
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 430
    .end local v1    # "i":I
    :cond_0
    const-string v1, "PackageRecordBehavior :"

    invoke-virtual {p1, v1}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V

    .line 431
    const/4 v1, 0x0

    .restart local v1    # "i":I
    :goto_1
    iget-object v2, p0, Lcom/miui/server/security/AppBehaviorService;->mPackageRecordBehavior:Landroid/util/ArrayMap;

    invoke-virtual {v2}, Landroid/util/ArrayMap;->size()I

    move-result v2

    if-ge v1, v2, :cond_2

    .line 432
    iget-object v2, p0, Lcom/miui/server/security/AppBehaviorService;->mPackageRecordBehavior:Landroid/util/ArrayMap;

    invoke-virtual {v2, v1}, Landroid/util/ArrayMap;->valueAt(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/util/Set;

    .line 433
    .local v2, "watchingBehavior":Ljava/util/Set;, "Ljava/util/Set<Ljava/lang/Integer;>;"
    if-eqz v2, :cond_1

    invoke-interface {v2}, Ljava/util/Set;->size()I

    move-result v3

    if-lez v3, :cond_1

    .line 434
    invoke-interface {v2}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :goto_2
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v4

    if-eqz v4, :cond_1

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Ljava/lang/Integer;

    invoke-virtual {v4}, Ljava/lang/Integer;->intValue()I

    move-result v4

    .line 435
    .local v4, "behavior":I
    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "    package:"

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    iget-object v6, p0, Lcom/miui/server/security/AppBehaviorService;->mPackageRecordBehavior:Landroid/util/ArrayMap;

    invoke-virtual {v6, v1}, Landroid/util/ArrayMap;->keyAt(I)Ljava/lang/Object;

    move-result-object v6

    check-cast v6, Ljava/lang/String;

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, " behavior:"

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-static {v4}, Lmiui/security/AppBehavior;->behaviorToName(I)Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {p1, v5}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V

    .line 436
    .end local v4    # "behavior":I
    goto :goto_2

    .line 431
    .end local v2    # "watchingBehavior":Ljava/util/Set;, "Ljava/util/Set<Ljava/lang/Integer;>;"
    :cond_1
    add-int/lit8 v1, v1, 0x1

    goto :goto_1

    .line 438
    .end local v1    # "i":I
    :cond_2
    monitor-exit v0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 439
    iget-object v1, p0, Lcom/miui/server/security/AppBehaviorService;->mUidBehaviorLock:Ljava/lang/Object;

    monitor-enter v1

    .line 440
    :try_start_1
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "dump current cache size:"

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget v2, p0, Lcom/miui/server/security/AppBehaviorService;->mRecordSize:I

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v0}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V

    .line 441
    const/4 v0, 0x0

    .local v0, "i":I
    :goto_3
    iget-object v2, p0, Lcom/miui/server/security/AppBehaviorService;->mPackageBehavior:Landroid/util/ArrayMap;

    invoke-virtual {v2}, Landroid/util/ArrayMap;->size()I

    move-result v2

    if-ge v0, v2, :cond_4

    .line 442
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "dump pkg:"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget-object v3, p0, Lcom/miui/server/security/AppBehaviorService;->mPackageBehavior:Landroid/util/ArrayMap;

    invoke-virtual {v3, v0}, Landroid/util/ArrayMap;->keyAt(I)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {p1, v2}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V

    .line 443
    iget-object v2, p0, Lcom/miui/server/security/AppBehaviorService;->mPackageBehavior:Landroid/util/ArrayMap;

    invoke-virtual {v2, v0}, Landroid/util/ArrayMap;->keyAt(I)Ljava/lang/Object;

    move-result-object v3

    invoke-virtual {v2, v3}, Landroid/util/ArrayMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/miui/server/security/model/PackageBehaviorBean;

    .line 444
    .local v2, "packageBehaviorBean":Lcom/miui/server/security/model/PackageBehaviorBean;
    if-eqz v2, :cond_3

    .line 445
    invoke-virtual {v2, p1}, Lcom/miui/server/security/model/PackageBehaviorBean;->dump(Ljava/io/PrintWriter;)V

    .line 441
    .end local v2    # "packageBehaviorBean":Lcom/miui/server/security/model/PackageBehaviorBean;
    :cond_3
    add-int/lit8 v0, v0, 0x1

    goto :goto_3

    .line 448
    .end local v0    # "i":I
    :cond_4
    const-string v0, "==================Dump AppBehaviorService end=================="

    invoke-virtual {p1, v0}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V

    .line 449
    monitor-exit v1

    .line 450
    return-void

    .line 449
    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    throw v0

    .line 438
    :catchall_1
    move-exception v1

    :try_start_2
    monitor-exit v0
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    throw v1
.end method

.method public getThreshold(I)J
    .locals 3
    .param p1, "behavior"    # I

    .line 161
    iget-object v0, p0, Lcom/miui/server/security/AppBehaviorService;->mBehaviorThreshold:Landroid/util/SparseLongArray;

    const-wide/16 v1, 0x0

    invoke-virtual {v0, p1, v1, v2}, Landroid/util/SparseLongArray;->get(IJ)J

    move-result-wide v0

    return-wide v0
.end method

.method public hasAppBehaviorWatching()Z
    .locals 2

    .line 151
    iget-object v0, p0, Lcom/miui/server/security/AppBehaviorService;->mRecordEnableLock:Ljava/lang/Object;

    monitor-enter v0

    .line 152
    :try_start_0
    iget-object v1, p0, Lcom/miui/server/security/AppBehaviorService;->mAlwaysRecordBehavior:Landroid/util/SparseBooleanArray;

    invoke-virtual {v1}, Landroid/util/SparseBooleanArray;->size()I

    move-result v1

    if-gtz v1, :cond_1

    iget-object v1, p0, Lcom/miui/server/security/AppBehaviorService;->mPackageRecordBehavior:Landroid/util/ArrayMap;

    invoke-virtual {v1}, Landroid/util/ArrayMap;->size()I

    move-result v1

    if-lez v1, :cond_0

    goto :goto_0

    :cond_0
    const/4 v1, 0x0

    goto :goto_1

    :cond_1
    :goto_0
    const/4 v1, 0x1

    :goto_1
    monitor-exit v0

    return v1

    .line 153
    :catchall_0
    move-exception v1

    monitor-exit v0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v1
.end method

.method public moreThanThreshold(IJ)Z
    .locals 2
    .param p1, "behavior"    # I
    .param p2, "curCount"    # J

    .line 165
    invoke-virtual {p0, p1}, Lcom/miui/server/security/AppBehaviorService;->getThreshold(I)J

    move-result-wide v0

    cmp-long v0, p2, v0

    if-ltz v0, :cond_0

    const/4 v0, 0x1

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    :goto_0
    return v0
.end method

.method public persistBehaviorRecord()V
    .locals 5

    .line 268
    iget-object v0, p0, Lcom/miui/server/security/AppBehaviorService;->mPersistMessenger:Landroid/os/Messenger;

    if-nez v0, :cond_1

    .line 269
    iget-object v0, p0, Lcom/miui/server/security/AppBehaviorService;->mWorkHandler:Landroid/os/Handler;

    const/16 v1, 0xe15

    invoke-virtual {v0, v1}, Landroid/os/Handler;->hasMessages(I)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 270
    return-void

    .line 272
    :cond_0
    new-instance v0, Landroid/content/Intent;

    invoke-direct {v0}, Landroid/content/Intent;-><init>()V

    .line 273
    .local v0, "intent":Landroid/content/Intent;
    new-instance v2, Landroid/content/ComponentName;

    const-string v3, "com.miui.securitycenter"

    const-string v4, "com.miui.permcenter.monitor.AppUsagePersistService"

    invoke-direct {v2, v3, v4}, Landroid/content/ComponentName;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {v0, v2}, Landroid/content/Intent;->setComponent(Landroid/content/ComponentName;)Landroid/content/Intent;

    .line 275
    const-string v2, "AppBehaviorService"

    const-string/jumbo v3, "start persistBehaviorRecord, try bind service."

    invoke-static {v2, v3}, Landroid/util/Slog;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 276
    iget-object v2, p0, Lcom/miui/server/security/AppBehaviorService;->mContext:Landroid/content/Context;

    new-instance v3, Lcom/miui/server/security/AppBehaviorService$4;

    invoke-direct {v3, p0}, Lcom/miui/server/security/AppBehaviorService$4;-><init>(Lcom/miui/server/security/AppBehaviorService;)V

    const/4 v4, 0x1

    invoke-virtual {v2, v0, v3, v4}, Landroid/content/Context;->bindService(Landroid/content/Intent;Landroid/content/ServiceConnection;I)Z

    .line 290
    iget-object v2, p0, Lcom/miui/server/security/AppBehaviorService;->mWorkHandler:Landroid/os/Handler;

    const-wide/16 v3, 0x2710

    invoke-virtual {v2, v1, v3, v4}, Landroid/os/Handler;->sendEmptyMessageDelayed(IJ)Z

    .line 291
    .end local v0    # "intent":Landroid/content/Intent;
    goto :goto_0

    .line 292
    :cond_1
    invoke-direct {p0}, Lcom/miui/server/security/AppBehaviorService;->schedulePersist()V

    .line 294
    :goto_0
    return-void
.end method

.method public recordAppBehavior(Lmiui/security/AppBehavior;)V
    .locals 12
    .param p1, "appBehavior"    # Lmiui/security/AppBehavior;

    .line 351
    if-eqz p1, :cond_c

    invoke-virtual {p1}, Lmiui/security/AppBehavior;->isValid()Z

    move-result v0

    if-nez v0, :cond_0

    goto/16 :goto_4

    .line 355
    :cond_0
    invoke-virtual {p1}, Lmiui/security/AppBehavior;->getCallerPkg()Ljava/lang/String;

    move-result-object v0

    const/4 v1, 0x0

    invoke-direct {p0, v0, v1}, Lcom/miui/server/security/AppBehaviorService;->getApplicationInfo(Ljava/lang/String;I)Landroid/content/pm/ApplicationInfo;

    move-result-object v6

    .line 356
    .local v6, "callerPkg":Landroid/content/pm/ApplicationInfo;
    if-nez v6, :cond_1

    .line 357
    return-void

    .line 359
    :cond_1
    invoke-virtual {p1}, Lmiui/security/AppBehavior;->getBehavior()I

    move-result v0

    invoke-static {v0}, Lmiui/security/AppBehavior;->getBehaviorType(I)I

    move-result v7

    .line 360
    .local v7, "behaviorType":I
    if-nez v7, :cond_2

    .line 361
    return-void

    .line 363
    :cond_2
    const/4 v0, 0x4

    const/4 v2, 0x3

    const/4 v3, 0x2

    if-ne v7, v3, :cond_8

    .line 364
    invoke-virtual {p1}, Lmiui/security/AppBehavior;->getCalleePkg()Ljava/lang/String;

    move-result-object v4

    invoke-direct {p0, v4, v1}, Lcom/miui/server/security/AppBehaviorService;->getApplicationInfo(Ljava/lang/String;I)Landroid/content/pm/ApplicationInfo;

    move-result-object v8

    .line 365
    .local v8, "calleePkg":Landroid/content/pm/ApplicationInfo;
    if-nez v8, :cond_3

    .line 366
    return-void

    .line 368
    :cond_3
    invoke-virtual {p1}, Lmiui/security/AppBehavior;->getBehavior()I

    move-result v1

    if-ne v1, v3, :cond_9

    .line 369
    invoke-static {v6}, Lcom/miui/server/security/AppBehaviorService;->isSystem(Landroid/content/pm/ApplicationInfo;)Z

    move-result v9

    .line 370
    .local v9, "callerSystem":Z
    invoke-static {v8}, Lcom/miui/server/security/AppBehaviorService;->isSystem(Landroid/content/pm/ApplicationInfo;)Z

    move-result v10

    .line 371
    .local v10, "calleeSystem":Z
    if-eqz v9, :cond_4

    if-eqz v10, :cond_4

    .line 372
    return-void

    .line 374
    :cond_4
    if-eq v9, v10, :cond_6

    .line 375
    if-eqz v9, :cond_5

    invoke-virtual {p1}, Lmiui/security/AppBehavior;->getCalleePkg()Ljava/lang/String;

    move-result-object v1

    goto :goto_0

    :cond_5
    invoke-virtual {p1}, Lmiui/security/AppBehavior;->getCallerPkg()Ljava/lang/String;

    move-result-object v1

    :goto_0
    move-object v11, v1

    .line 376
    .local v11, "willBeCheckPkg":Ljava/lang/String;
    const/16 v1, 0x1c

    invoke-virtual {p0, v1, v11}, Lcom/miui/server/security/AppBehaviorService;->canRecordBehavior(ILjava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_6

    .line 377
    const/16 v1, 0x1c

    .line 378
    invoke-virtual {p1}, Lmiui/security/AppBehavior;->getCallerPkg()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {p1}, Lmiui/security/AppBehavior;->getChain()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {p1}, Lmiui/security/AppBehavior;->getCount()J

    move-result-wide v4

    .line 377
    move-object v0, p0

    invoke-direct/range {v0 .. v5}, Lcom/miui/server/security/AppBehaviorService;->scheduleRecord(ILjava/lang/String;Ljava/lang/String;J)V

    .line 379
    return-void

    .line 382
    .end local v11    # "willBeCheckPkg":Ljava/lang/String;
    :cond_6
    invoke-virtual {p1}, Lmiui/security/AppBehavior;->getCallerPkg()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p0, v0, v1}, Lcom/miui/server/security/AppBehaviorService;->canRecordBehavior(ILjava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_7

    .line 383
    invoke-virtual {p1}, Lmiui/security/AppBehavior;->getCalleePkg()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v2, v0}, Lcom/miui/server/security/AppBehaviorService;->canRecordBehavior(ILjava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_9

    .line 384
    :cond_7
    const/4 v1, 0x4

    .line 385
    invoke-virtual {p1}, Lmiui/security/AppBehavior;->getCallerPkg()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {p1}, Lmiui/security/AppBehavior;->getChain()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {p1}, Lmiui/security/AppBehavior;->getCount()J

    move-result-wide v4

    .line 384
    move-object v0, p0

    invoke-direct/range {v0 .. v5}, Lcom/miui/server/security/AppBehaviorService;->scheduleRecord(ILjava/lang/String;Ljava/lang/String;J)V

    goto :goto_1

    .line 388
    .end local v8    # "calleePkg":Landroid/content/pm/ApplicationInfo;
    .end local v9    # "callerSystem":Z
    .end local v10    # "calleeSystem":Z
    :cond_8
    const/4 v1, 0x1

    if-eq v7, v1, :cond_a

    if-eq v7, v2, :cond_a

    if-ne v7, v0, :cond_9

    goto :goto_2

    :cond_9
    :goto_1
    goto :goto_3

    .line 391
    :cond_a
    :goto_2
    invoke-virtual {p1}, Lmiui/security/AppBehavior;->getBehavior()I

    move-result v0

    invoke-virtual {p1}, Lmiui/security/AppBehavior;->getCallerPkg()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p0, v0, v1}, Lcom/miui/server/security/AppBehaviorService;->canRecordBehavior(ILjava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_b

    .line 392
    invoke-virtual {p1}, Lmiui/security/AppBehavior;->getBehavior()I

    move-result v1

    invoke-virtual {p1}, Lmiui/security/AppBehavior;->getCallerPkg()Ljava/lang/String;

    move-result-object v2

    .line 393
    invoke-virtual {p1}, Lmiui/security/AppBehavior;->getExtra()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {p1}, Lmiui/security/AppBehavior;->getCount()J

    move-result-wide v4

    .line 392
    move-object v0, p0

    invoke-direct/range {v0 .. v5}, Lcom/miui/server/security/AppBehaviorService;->scheduleRecord(ILjava/lang/String;Ljava/lang/String;J)V

    .line 396
    :cond_b
    :goto_3
    return-void

    .line 352
    .end local v6    # "callerPkg":Landroid/content/pm/ApplicationInfo;
    .end local v7    # "behaviorType":I
    :cond_c
    :goto_4
    return-void
.end method

.method public recordAppBehaviorAsync(Lmiui/security/AppBehavior;)V
    .locals 2
    .param p1, "appBehavior"    # Lmiui/security/AppBehavior;

    .line 344
    invoke-static {}, Lmiui/mqsas/scout/ScoutUtils;->isLibraryTest()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 345
    return-void

    .line 347
    :cond_0
    iget-object v0, p0, Lcom/miui/server/security/AppBehaviorService;->mWorkHandler:Landroid/os/Handler;

    new-instance v1, Lcom/miui/server/security/AppBehaviorService$$ExternalSyntheticLambda0;

    invoke-direct {v1, p0, p1}, Lcom/miui/server/security/AppBehaviorService$$ExternalSyntheticLambda0;-><init>(Lcom/miui/server/security/AppBehaviorService;Lmiui/security/AppBehavior;)V

    invoke-virtual {v0, v1}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    .line 348
    return-void
.end method

.method public setAppBehaviorDuration(Lcom/miui/server/security/AppDurationService;)V
    .locals 0
    .param p1, "appDurationService"    # Lcom/miui/server/security/AppDurationService;

    .line 469
    iput-object p1, p0, Lcom/miui/server/security/AppBehaviorService;->mAppBehaviorDuration:Lcom/miui/server/security/AppDurationService;

    .line 470
    return-void
.end method

.method public startWatchingAppBehavior(IZ)V
    .locals 4
    .param p1, "behavior"    # I
    .param p2, "includeSystem"    # Z

    .line 172
    iget-object v0, p0, Lcom/miui/server/security/AppBehaviorService;->mRecordEnableLock:Ljava/lang/Object;

    monitor-enter v0

    .line 173
    :try_start_0
    iget-object v1, p0, Lcom/miui/server/security/AppBehaviorService;->mAlwaysRecordBehavior:Landroid/util/SparseBooleanArray;

    invoke-virtual {v1, p1, p2}, Landroid/util/SparseBooleanArray;->put(IZ)V

    .line 174
    const/4 v1, 0x1

    invoke-direct {p0, p1, v1}, Lcom/miui/server/security/AppBehaviorService;->registerListenerForBehavior(IZ)V

    .line 175
    const-string v1, "AppBehaviorService"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v3, "startWatching:"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, ",includeSystem:"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p2}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Slog;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 176
    monitor-exit v0

    .line 177
    return-void

    .line 176
    :catchall_0
    move-exception v1

    monitor-exit v0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v1
.end method

.method public startWatchingAppBehavior(I[Ljava/lang/String;Z)V
    .locals 10
    .param p1, "behavior"    # I
    .param p2, "pkgNames"    # [Ljava/lang/String;
    .param p3, "includeSystem"    # Z

    .line 204
    iget-object v0, p0, Lcom/miui/server/security/AppBehaviorService;->mRecordEnableLock:Ljava/lang/Object;

    monitor-enter v0

    .line 205
    :try_start_0
    array-length v1, p2

    const/4 v2, 0x0

    move v3, v2

    :goto_0
    if-ge v3, v1, :cond_3

    aget-object v4, p2, v3

    .line 206
    .local v4, "tmpPkgName":Ljava/lang/String;
    invoke-direct {p0, v4, v2}, Lcom/miui/server/security/AppBehaviorService;->getApplicationInfo(Ljava/lang/String;I)Landroid/content/pm/ApplicationInfo;

    move-result-object v5

    .line 207
    .local v5, "applicationInfo":Landroid/content/pm/ApplicationInfo;
    if-nez v5, :cond_0

    .line 208
    goto :goto_1

    .line 210
    :cond_0
    if-nez p3, :cond_1

    invoke-static {v5}, Lcom/miui/server/security/AppBehaviorService;->isSystem(Landroid/content/pm/ApplicationInfo;)Z

    move-result v6

    if-eqz v6, :cond_1

    .line 211
    goto :goto_1

    .line 213
    :cond_1
    iget-object v6, p0, Lcom/miui/server/security/AppBehaviorService;->mPackageRecordBehavior:Landroid/util/ArrayMap;

    invoke-virtual {v6, v4}, Landroid/util/ArrayMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v6

    check-cast v6, Ljava/util/Set;

    .line 214
    .local v6, "behaviorWatching":Ljava/util/Set;, "Ljava/util/Set<Ljava/lang/Integer;>;"
    if-nez v6, :cond_2

    .line 215
    new-instance v7, Landroid/util/ArraySet;

    invoke-direct {v7}, Landroid/util/ArraySet;-><init>()V

    move-object v6, v7

    .line 216
    iget-object v7, p0, Lcom/miui/server/security/AppBehaviorService;->mPackageRecordBehavior:Landroid/util/ArrayMap;

    invoke-virtual {v7, v4, v6}, Landroid/util/ArrayMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 217
    const-string v7, "AppBehaviorService"

    new-instance v8, Ljava/lang/StringBuilder;

    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v9, "startWatching:"

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v8

    const-string v9, ",pkg:"

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    invoke-static {v7, v8}, Landroid/util/Slog;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 219
    :cond_2
    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v7

    invoke-interface {v6, v7}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    .line 205
    .end local v4    # "tmpPkgName":Ljava/lang/String;
    .end local v5    # "applicationInfo":Landroid/content/pm/ApplicationInfo;
    .end local v6    # "behaviorWatching":Ljava/util/Set;, "Ljava/util/Set<Ljava/lang/Integer;>;"
    :goto_1
    add-int/lit8 v3, v3, 0x1

    goto :goto_0

    .line 221
    :cond_3
    const/4 v1, 0x1

    invoke-direct {p0, p1, v1}, Lcom/miui/server/security/AppBehaviorService;->registerListenerForBehavior(IZ)V

    .line 222
    monitor-exit v0

    .line 223
    return-void

    .line 222
    :catchall_0
    move-exception v1

    monitor-exit v0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v1
.end method

.method public stopWatchingAppBehavior(I)V
    .locals 5
    .param p1, "behavior"    # I

    .line 183
    iget-object v0, p0, Lcom/miui/server/security/AppBehaviorService;->mRecordEnableLock:Ljava/lang/Object;

    monitor-enter v0

    .line 184
    const/4 v1, 0x1

    if-ne p1, v1, :cond_0

    .line 185
    :try_start_0
    iget-object v1, p0, Lcom/miui/server/security/AppBehaviorService;->mAlwaysRecordBehavior:Landroid/util/SparseBooleanArray;

    invoke-virtual {v1}, Landroid/util/SparseBooleanArray;->clear()V

    .line 186
    iget-object v1, p0, Lcom/miui/server/security/AppBehaviorService;->mPackageRecordBehavior:Landroid/util/ArrayMap;

    invoke-virtual {v1}, Landroid/util/ArrayMap;->clear()V

    goto :goto_1

    .line 188
    :cond_0
    iget-object v1, p0, Lcom/miui/server/security/AppBehaviorService;->mAlwaysRecordBehavior:Landroid/util/SparseBooleanArray;

    invoke-virtual {v1, p1}, Landroid/util/SparseBooleanArray;->delete(I)V

    .line 189
    iget-object v1, p0, Lcom/miui/server/security/AppBehaviorService;->mPackageRecordBehavior:Landroid/util/ArrayMap;

    invoke-virtual {v1}, Landroid/util/ArrayMap;->entrySet()Ljava/util/Set;

    move-result-object v1

    invoke-interface {v1}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v1

    .local v1, "it":Ljava/util/Iterator;, "Ljava/util/Iterator<Ljava/util/Map$Entry<Ljava/lang/String;Ljava/util/Set<Ljava/lang/Integer;>;>;>;"
    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_2

    .line 190
    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/util/Map$Entry;

    .line 191
    .local v2, "item":Ljava/util/Map$Entry;, "Ljava/util/Map$Entry<Ljava/lang/String;Ljava/util/Set<Ljava/lang/Integer;>;>;"
    invoke-interface {v2}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/util/Set;

    .line 192
    .local v3, "itemWatching":Ljava/util/Set;, "Ljava/util/Set<Ljava/lang/Integer;>;"
    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    invoke-interface {v3, v4}, Ljava/util/Set;->remove(Ljava/lang/Object;)Z

    .line 193
    invoke-interface {v3}, Ljava/util/Set;->isEmpty()Z

    move-result v4

    if-eqz v4, :cond_1

    .line 194
    invoke-interface {v1}, Ljava/util/Iterator;->remove()V

    .line 196
    .end local v2    # "item":Ljava/util/Map$Entry;, "Ljava/util/Map$Entry<Ljava/lang/String;Ljava/util/Set<Ljava/lang/Integer;>;>;"
    .end local v3    # "itemWatching":Ljava/util/Set;, "Ljava/util/Set<Ljava/lang/Integer;>;"
    :cond_1
    goto :goto_0

    .line 198
    .end local v1    # "it":Ljava/util/Iterator;, "Ljava/util/Iterator<Ljava/util/Map$Entry<Ljava/lang/String;Ljava/util/Set<Ljava/lang/Integer;>;>;>;"
    :cond_2
    :goto_1
    const/4 v1, 0x0

    invoke-direct {p0, p1, v1}, Lcom/miui/server/security/AppBehaviorService;->registerListenerForBehavior(IZ)V

    .line 199
    const-string v1, "AppBehaviorService"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v3, "stopWatching for all:"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Slog;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 200
    monitor-exit v0

    .line 201
    return-void

    .line 200
    :catchall_0
    move-exception v1

    monitor-exit v0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v1
.end method

.method public stopWatchingAppBehavior(I[Ljava/lang/String;)V
    .locals 10
    .param p1, "behavior"    # I
    .param p2, "pkgNames"    # [Ljava/lang/String;

    .line 226
    iget-object v0, p0, Lcom/miui/server/security/AppBehaviorService;->mRecordEnableLock:Ljava/lang/Object;

    monitor-enter v0

    .line 227
    :try_start_0
    array-length v1, p2

    const/4 v2, 0x0

    move v3, v2

    :goto_0
    if-ge v3, v1, :cond_2

    aget-object v4, p2, v3

    .line 228
    .local v4, "tmpPkgName":Ljava/lang/String;
    iget-object v5, p0, Lcom/miui/server/security/AppBehaviorService;->mPackageRecordBehavior:Landroid/util/ArrayMap;

    invoke-virtual {v5, v4}, Landroid/util/ArrayMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Ljava/util/Set;

    .line 229
    .local v5, "watchingBehavior":Ljava/util/Set;, "Ljava/util/Set<Ljava/lang/Integer;>;"
    const-string v6, "AppBehaviorService"

    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v8, "stopWatching"

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v7

    const-string v8, ",pkg:"

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-static {v6, v7}, Landroid/util/Slog;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 230
    if-eqz v5, :cond_0

    .line 231
    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v6

    invoke-interface {v5, v6}, Ljava/util/Set;->remove(Ljava/lang/Object;)Z

    .line 232
    invoke-interface {v5}, Ljava/util/Set;->isEmpty()Z

    move-result v6

    if-eqz v6, :cond_0

    .line 233
    iget-object v6, p0, Lcom/miui/server/security/AppBehaviorService;->mPackageRecordBehavior:Landroid/util/ArrayMap;

    invoke-virtual {v6, v4}, Landroid/util/ArrayMap;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    .line 237
    :cond_0
    iget-object v6, p0, Lcom/miui/server/security/AppBehaviorService;->mAppBehaviorDuration:Lcom/miui/server/security/AppDurationService;

    if-eqz v6, :cond_1

    .line 238
    invoke-direct {p0, v4, v2}, Lcom/miui/server/security/AppBehaviorService;->getApplicationInfo(Ljava/lang/String;I)Landroid/content/pm/ApplicationInfo;

    move-result-object v6

    .line 239
    .local v6, "applicationInfo":Landroid/content/pm/ApplicationInfo;
    if-eqz v6, :cond_1

    .line 240
    iget-object v7, p0, Lcom/miui/server/security/AppBehaviorService;->mAppBehaviorDuration:Lcom/miui/server/security/AppDurationService;

    iget v8, v6, Landroid/content/pm/ApplicationInfo;->uid:I

    iget-object v9, v6, Landroid/content/pm/ApplicationInfo;->packageName:Ljava/lang/String;

    invoke-virtual {v7, p1, v8, v9}, Lcom/miui/server/security/AppDurationService;->removeDurationEvent(IILjava/lang/String;)V

    .line 227
    .end local v4    # "tmpPkgName":Ljava/lang/String;
    .end local v5    # "watchingBehavior":Ljava/util/Set;, "Ljava/util/Set<Ljava/lang/Integer;>;"
    .end local v6    # "applicationInfo":Landroid/content/pm/ApplicationInfo;
    :cond_1
    add-int/lit8 v3, v3, 0x1

    goto :goto_0

    .line 244
    :cond_2
    monitor-exit v0

    .line 245
    return-void

    .line 244
    :catchall_0
    move-exception v1

    monitor-exit v0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v1
.end method

.method public updateBehaviorThreshold(IJ)V
    .locals 1
    .param p1, "behavior"    # I
    .param p2, "threshold"    # J

    .line 157
    iget-object v0, p0, Lcom/miui/server/security/AppBehaviorService;->mBehaviorThreshold:Landroid/util/SparseLongArray;

    invoke-virtual {v0, p1, p2, p3}, Landroid/util/SparseLongArray;->put(IJ)V

    .line 158
    return-void
.end method
