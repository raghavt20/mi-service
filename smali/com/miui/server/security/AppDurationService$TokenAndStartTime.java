class com.miui.server.security.AppDurationService$TokenAndStartTime {
	 /* .source "AppDurationService.java" */
	 /* # annotations */
	 /* .annotation system Ldalvik/annotation/EnclosingClass; */
	 /* value = Lcom/miui/server/security/AppDurationService; */
} // .end annotation
/* .annotation system Ldalvik/annotation/InnerClass; */
/* accessFlags = 0xa */
/* name = "TokenAndStartTime" */
} // .end annotation
/* # instance fields */
private android.util.ArrayMap binderStartTimeMap;
/* .annotation system Ldalvik/annotation/Signature; */
/* value = { */
/* "Landroid/util/ArrayMap<", */
/* "Landroid/os/IBinder;", */
/* "Ljava/lang/Long;", */
/* ">;" */
/* } */
} // .end annotation
} // .end field
private android.util.SparseArray uidStartTimeMap;
/* .annotation system Ldalvik/annotation/Signature; */
/* value = { */
/* "Landroid/util/SparseArray<", */
/* "Ljava/lang/Long;", */
/* ">;" */
/* } */
} // .end annotation
} // .end field
/* # direct methods */
static android.util.ArrayMap -$$Nest$fgetbinderStartTimeMap ( com.miui.server.security.AppDurationService$TokenAndStartTime p0 ) { //bridge//synthethic
/* .locals 0 */
p0 = this.binderStartTimeMap;
} // .end method
static android.util.SparseArray -$$Nest$fgetuidStartTimeMap ( com.miui.server.security.AppDurationService$TokenAndStartTime p0 ) { //bridge//synthethic
/* .locals 0 */
p0 = this.uidStartTimeMap;
} // .end method
private com.miui.server.security.AppDurationService$TokenAndStartTime ( ) {
/* .locals 0 */
/* .line 183 */
/* invoke-direct {p0}, Ljava/lang/Object;-><init>()V */
return;
} // .end method
 com.miui.server.security.AppDurationService$TokenAndStartTime ( ) { //synthethic
/* .locals 0 */
/* invoke-direct {p0}, Lcom/miui/server/security/AppDurationService$TokenAndStartTime;-><init>()V */
return;
} // .end method
/* # virtual methods */
public Boolean recordBinderTime ( android.os.IBinder p0 ) {
/* .locals 3 */
/* .param p1, "binder" # Landroid/os/IBinder; */
/* .line 188 */
v0 = this.binderStartTimeMap;
/* if-nez v0, :cond_0 */
/* .line 189 */
/* new-instance v0, Landroid/util/ArrayMap; */
/* invoke-direct {v0}, Landroid/util/ArrayMap;-><init>()V */
this.binderStartTimeMap = v0;
/* .line 191 */
} // :cond_0
v0 = this.binderStartTimeMap;
v0 = (( android.util.ArrayMap ) v0 ).containsKey ( p1 ); // invoke-virtual {v0, p1}, Landroid/util/ArrayMap;->containsKey(Ljava/lang/Object;)Z
/* if-nez v0, :cond_1 */
/* .line 192 */
v0 = this.binderStartTimeMap;
android.os.SystemClock .elapsedRealtime ( );
/* move-result-wide v1 */
java.lang.Long .valueOf ( v1,v2 );
(( android.util.ArrayMap ) v0 ).put ( p1, v1 ); // invoke-virtual {v0, p1, v1}, Landroid/util/ArrayMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;
/* .line 193 */
int v0 = 1; // const/4 v0, 0x1
/* .line 195 */
} // :cond_1
int v0 = 0; // const/4 v0, 0x0
} // .end method
public Boolean recordUidTime ( Integer p0 ) {
/* .locals 3 */
/* .param p1, "uid" # I */
/* .line 211 */
v0 = this.uidStartTimeMap;
/* if-nez v0, :cond_0 */
/* .line 212 */
/* new-instance v0, Landroid/util/SparseArray; */
/* invoke-direct {v0}, Landroid/util/SparseArray;-><init>()V */
this.uidStartTimeMap = v0;
/* .line 214 */
} // :cond_0
v0 = this.uidStartTimeMap;
v0 = (( android.util.SparseArray ) v0 ).contains ( p1 ); // invoke-virtual {v0, p1}, Landroid/util/SparseArray;->contains(I)Z
/* if-nez v0, :cond_1 */
/* .line 215 */
v0 = this.uidStartTimeMap;
android.os.SystemClock .elapsedRealtime ( );
/* move-result-wide v1 */
java.lang.Long .valueOf ( v1,v2 );
(( android.util.SparseArray ) v0 ).put ( p1, v1 ); // invoke-virtual {v0, p1, v1}, Landroid/util/SparseArray;->put(ILjava/lang/Object;)V
/* .line 216 */
int v0 = 1; // const/4 v0, 0x1
/* .line 218 */
} // :cond_1
int v0 = 0; // const/4 v0, 0x0
} // .end method
public Long stopBinderTime ( android.os.IBinder p0 ) {
/* .locals 6 */
/* .param p1, "binder" # Landroid/os/IBinder; */
/* .line 199 */
if ( p1 != null) { // if-eqz p1, :cond_1
v0 = this.binderStartTimeMap;
if ( v0 != null) { // if-eqz v0, :cond_1
/* .line 201 */
v0 = (( android.util.ArrayMap ) v0 ).size ( ); // invoke-virtual {v0}, Landroid/util/ArrayMap;->size()I
/* if-nez v0, :cond_0 */
/* .line 204 */
} // :cond_0
android.os.SystemClock .elapsedRealtime ( );
/* move-result-wide v0 */
/* .line 205 */
/* .local v0, "curTime":J */
v2 = this.binderStartTimeMap;
java.lang.Long .valueOf ( v0,v1 );
(( android.util.ArrayMap ) v2 ).getOrDefault ( p1, v3 ); // invoke-virtual {v2, p1, v3}, Landroid/util/ArrayMap;->getOrDefault(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;
/* check-cast v2, Ljava/lang/Long; */
(( java.lang.Long ) v2 ).longValue ( ); // invoke-virtual {v2}, Ljava/lang/Long;->longValue()J
/* move-result-wide v2 */
/* .line 206 */
/* .local v2, "startTime":J */
v4 = this.binderStartTimeMap;
(( android.util.ArrayMap ) v4 ).remove ( p1 ); // invoke-virtual {v4, p1}, Landroid/util/ArrayMap;->remove(Ljava/lang/Object;)Ljava/lang/Object;
/* .line 207 */
/* sub-long v4, v0, v2 */
/* return-wide v4 */
/* .line 202 */
} // .end local v0 # "curTime":J
} // .end local v2 # "startTime":J
} // :cond_1
} // :goto_0
/* const-wide/16 v0, 0x0 */
/* return-wide v0 */
} // .end method
public Long stopUidTime ( Integer p0 ) {
/* .locals 6 */
/* .param p1, "uid" # I */
/* .line 222 */
v0 = this.uidStartTimeMap;
if ( v0 != null) { // if-eqz v0, :cond_1
/* .line 223 */
v0 = (( android.util.SparseArray ) v0 ).size ( ); // invoke-virtual {v0}, Landroid/util/SparseArray;->size()I
/* if-nez v0, :cond_0 */
/* .line 226 */
} // :cond_0
android.os.SystemClock .elapsedRealtime ( );
/* move-result-wide v0 */
/* .line 227 */
/* .local v0, "curTime":J */
v2 = this.uidStartTimeMap;
java.lang.Long .valueOf ( v0,v1 );
(( android.util.SparseArray ) v2 ).get ( p1, v3 ); // invoke-virtual {v2, p1, v3}, Landroid/util/SparseArray;->get(ILjava/lang/Object;)Ljava/lang/Object;
/* check-cast v2, Ljava/lang/Long; */
(( java.lang.Long ) v2 ).longValue ( ); // invoke-virtual {v2}, Ljava/lang/Long;->longValue()J
/* move-result-wide v2 */
/* .line 228 */
/* .local v2, "startTime":J */
v4 = this.uidStartTimeMap;
(( android.util.SparseArray ) v4 ).remove ( p1 ); // invoke-virtual {v4, p1}, Landroid/util/SparseArray;->remove(I)V
/* .line 229 */
/* sub-long v4, v0, v2 */
/* return-wide v4 */
/* .line 224 */
} // .end local v0 # "curTime":J
} // .end local v2 # "startTime":J
} // :cond_1
} // :goto_0
/* const-wide/16 v0, 0x0 */
/* return-wide v0 */
} // .end method
