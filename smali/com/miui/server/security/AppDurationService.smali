.class public Lcom/miui/server/security/AppDurationService;
.super Ljava/lang/Object;
.source "AppDurationService.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/miui/server/security/AppDurationService$UserPackage;,
        Lcom/miui/server/security/AppDurationService$UserPackageBehaviorDuration;,
        Lcom/miui/server/security/AppDurationService$TokenAndStartTime;
    }
.end annotation


# static fields
.field private static final TAG:Ljava/lang/String; = "AppDurationService"


# instance fields
.field private final appBehavior:Lcom/miui/server/security/AppBehaviorService;

.field private final mDurationLock:Ljava/lang/Object;

.field private final userPackageStartTimeAndTokenArrayMap:Landroid/util/ArrayMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/util/ArrayMap<",
            "Lcom/miui/server/security/AppDurationService$UserPackage;",
            "Lcom/miui/server/security/AppDurationService$UserPackageBehaviorDuration;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Lcom/miui/server/security/AppBehaviorService;)V
    .locals 3
    .param p1, "appBehavior"    # Lcom/miui/server/security/AppBehaviorService;

    .line 24
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 20
    new-instance v0, Ljava/lang/Object;

    invoke-direct {v0}, Ljava/lang/Object;-><init>()V

    iput-object v0, p0, Lcom/miui/server/security/AppDurationService;->mDurationLock:Ljava/lang/Object;

    .line 25
    new-instance v0, Landroid/util/ArrayMap;

    invoke-direct {v0}, Landroid/util/ArrayMap;-><init>()V

    iput-object v0, p0, Lcom/miui/server/security/AppDurationService;->userPackageStartTimeAndTokenArrayMap:Landroid/util/ArrayMap;

    .line 26
    iput-object p1, p0, Lcom/miui/server/security/AppDurationService;->appBehavior:Lcom/miui/server/security/AppBehaviorService;

    .line 28
    :try_start_0
    new-instance v0, Lcom/miui/server/security/AppDurationService$1;

    invoke-direct {v0, p0, p1}, Lcom/miui/server/security/AppDurationService$1;-><init>(Lcom/miui/server/security/AppDurationService;Lcom/miui/server/security/AppBehaviorService;)V

    .line 35
    .local v0, "foregroundServiceObserver":Landroid/app/IForegroundServiceObserver$Stub;
    invoke-static {}, Landroid/app/ActivityManager;->getService()Landroid/app/IActivityManager;

    move-result-object v1

    invoke-interface {v1, v0}, Landroid/app/IActivityManager;->registerForegroundServiceObserver(Landroid/app/IForegroundServiceObserver;)Z
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    .line 38
    nop

    .end local v0    # "foregroundServiceObserver":Landroid/app/IForegroundServiceObserver$Stub;
    goto :goto_0

    .line 36
    :catch_0
    move-exception v0

    .line 37
    .local v0, "exception":Landroid/os/RemoteException;
    const-string v1, "AppDurationService"

    const-string v2, "AppDurationService registerForegroundServiceObserver"

    invoke-static {v1, v2, v0}, Landroid/util/Slog;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    .line 39
    .end local v0    # "exception":Landroid/os/RemoteException;
    :goto_0
    return-void
.end method


# virtual methods
.method public checkIfTooLongDuration()V
    .locals 17

    .line 91
    move-object/from16 v1, p0

    const-string v0, "AppDurationService"

    const-string v2, "checkIfTooLongDuration"

    invoke-static {v0, v2}, Landroid/util/Slog;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 92
    iget-object v2, v1, Lcom/miui/server/security/AppDurationService;->mDurationLock:Ljava/lang/Object;

    monitor-enter v2

    .line 93
    :try_start_0
    invoke-static {}, Landroid/os/SystemClock;->elapsedRealtime()J

    move-result-wide v3

    .line 94
    .local v3, "current":J
    const/4 v0, 0x0

    .local v0, "i":I
    :goto_0
    iget-object v5, v1, Lcom/miui/server/security/AppDurationService;->userPackageStartTimeAndTokenArrayMap:Landroid/util/ArrayMap;

    invoke-virtual {v5}, Landroid/util/ArrayMap;->size()I

    move-result v5

    if-ge v0, v5, :cond_6

    .line 95
    iget-object v5, v1, Lcom/miui/server/security/AppDurationService;->userPackageStartTimeAndTokenArrayMap:Landroid/util/ArrayMap;

    invoke-virtual {v5, v0}, Landroid/util/ArrayMap;->keyAt(I)Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Lcom/miui/server/security/AppDurationService$UserPackage;

    invoke-static {v5}, Lcom/miui/server/security/AppDurationService$UserPackage;->-$$Nest$fgetpkgName(Lcom/miui/server/security/AppDurationService$UserPackage;)Ljava/lang/String;

    move-result-object v5

    .line 96
    .local v5, "pkgName":Ljava/lang/String;
    iget-object v6, v1, Lcom/miui/server/security/AppDurationService;->userPackageStartTimeAndTokenArrayMap:Landroid/util/ArrayMap;

    invoke-virtual {v6, v0}, Landroid/util/ArrayMap;->valueAt(I)Ljava/lang/Object;

    move-result-object v6

    check-cast v6, Lcom/miui/server/security/AppDurationService$UserPackageBehaviorDuration;

    .line 97
    .local v6, "userPackageBehaviorDuration":Lcom/miui/server/security/AppDurationService$UserPackageBehaviorDuration;
    const/4 v7, 0x0

    .local v7, "j":I
    :goto_1
    invoke-static {v6}, Lcom/miui/server/security/AppDurationService$UserPackageBehaviorDuration;->-$$Nest$fgetbehaviorDurationMap(Lcom/miui/server/security/AppDurationService$UserPackageBehaviorDuration;)Landroid/util/SparseArray;

    move-result-object v8

    invoke-virtual {v8}, Landroid/util/SparseArray;->size()I

    move-result v8

    if-ge v7, v8, :cond_5

    .line 98
    invoke-static {v6}, Lcom/miui/server/security/AppDurationService$UserPackageBehaviorDuration;->-$$Nest$fgetbehaviorDurationMap(Lcom/miui/server/security/AppDurationService$UserPackageBehaviorDuration;)Landroid/util/SparseArray;

    move-result-object v8

    invoke-virtual {v8, v7}, Landroid/util/SparseArray;->keyAt(I)I

    move-result v8

    .line 99
    .local v8, "curBehavior":I
    invoke-static {v6}, Lcom/miui/server/security/AppDurationService$UserPackageBehaviorDuration;->-$$Nest$fgetbehaviorDurationMap(Lcom/miui/server/security/AppDurationService$UserPackageBehaviorDuration;)Landroid/util/SparseArray;

    move-result-object v9

    invoke-virtual {v9, v7}, Landroid/util/SparseArray;->valueAt(I)Ljava/lang/Object;

    move-result-object v9

    check-cast v9, Lcom/miui/server/security/AppDurationService$TokenAndStartTime;

    .line 100
    .local v9, "tokenAndStartTime":Lcom/miui/server/security/AppDurationService$TokenAndStartTime;
    invoke-static {v9}, Lcom/miui/server/security/AppDurationService$TokenAndStartTime;->-$$Nest$fgetbinderStartTimeMap(Lcom/miui/server/security/AppDurationService$TokenAndStartTime;)Landroid/util/ArrayMap;

    move-result-object v10

    if-eqz v10, :cond_2

    invoke-static {v9}, Lcom/miui/server/security/AppDurationService$TokenAndStartTime;->-$$Nest$fgetbinderStartTimeMap(Lcom/miui/server/security/AppDurationService$TokenAndStartTime;)Landroid/util/ArrayMap;

    move-result-object v10

    invoke-virtual {v10}, Landroid/util/ArrayMap;->size()I

    move-result v10

    if-lez v10, :cond_2

    .line 101
    const/4 v10, 0x0

    .local v10, "k":I
    :goto_2
    invoke-static {v9}, Lcom/miui/server/security/AppDurationService$TokenAndStartTime;->-$$Nest$fgetbinderStartTimeMap(Lcom/miui/server/security/AppDurationService$TokenAndStartTime;)Landroid/util/ArrayMap;

    move-result-object v12

    invoke-virtual {v12}, Landroid/util/ArrayMap;->size()I

    move-result v12

    if-ge v10, v12, :cond_1

    .line 102
    invoke-static {v9}, Lcom/miui/server/security/AppDurationService$TokenAndStartTime;->-$$Nest$fgetbinderStartTimeMap(Lcom/miui/server/security/AppDurationService$TokenAndStartTime;)Landroid/util/ArrayMap;

    move-result-object v12

    invoke-virtual {v12, v10}, Landroid/util/ArrayMap;->valueAt(I)Ljava/lang/Object;

    move-result-object v12

    check-cast v12, Ljava/lang/Long;

    invoke-virtual {v12}, Ljava/lang/Long;->longValue()J

    move-result-wide v12

    sub-long v12, v3, v12

    .line 103
    .local v12, "duration":J
    iget-object v14, v1, Lcom/miui/server/security/AppDurationService;->appBehavior:Lcom/miui/server/security/AppBehaviorService;

    invoke-virtual {v14, v8, v12, v13}, Lcom/miui/server/security/AppBehaviorService;->moreThanThreshold(IJ)Z

    move-result v14

    .line 104
    .local v14, "moreThanThreshold":Z
    if-eqz v14, :cond_0

    .line 105
    const-string v15, "AppDurationService"

    new-instance v11, Ljava/lang/StringBuilder;

    invoke-direct {v11}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v11, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v11

    move-object/from16 v16, v6

    .end local v6    # "userPackageBehaviorDuration":Lcom/miui/server/security/AppDurationService$UserPackageBehaviorDuration;
    .local v16, "userPackageBehaviorDuration":Lcom/miui/server/security/AppDurationService$UserPackageBehaviorDuration;
    const-string v6, ",behavior:"

    invoke-virtual {v11, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6, v8}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v6

    const-string v11, " has running too long. record and restart"

    invoke-virtual {v6, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-static {v15, v6}, Landroid/util/Slog;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 106
    invoke-static {v9}, Lcom/miui/server/security/AppDurationService$TokenAndStartTime;->-$$Nest$fgetbinderStartTimeMap(Lcom/miui/server/security/AppDurationService$TokenAndStartTime;)Landroid/util/ArrayMap;

    move-result-object v6

    invoke-static {v3, v4}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v11

    invoke-virtual {v6, v10, v11}, Landroid/util/ArrayMap;->setValueAt(ILjava/lang/Object;)Ljava/lang/Object;

    .line 107
    iget-object v6, v1, Lcom/miui/server/security/AppDurationService;->appBehavior:Lcom/miui/server/security/AppBehaviorService;

    const/4 v11, 0x0

    invoke-static {v8, v5, v12, v13, v11}, Lmiui/security/AppBehavior;->buildCountEvent(ILjava/lang/String;JLjava/lang/String;)Lmiui/security/AppBehavior;

    move-result-object v15

    invoke-virtual {v6, v15}, Lcom/miui/server/security/AppBehaviorService;->recordAppBehaviorAsync(Lmiui/security/AppBehavior;)V

    goto :goto_3

    .line 104
    .end local v16    # "userPackageBehaviorDuration":Lcom/miui/server/security/AppDurationService$UserPackageBehaviorDuration;
    .restart local v6    # "userPackageBehaviorDuration":Lcom/miui/server/security/AppDurationService$UserPackageBehaviorDuration;
    :cond_0
    move-object/from16 v16, v6

    .line 101
    .end local v6    # "userPackageBehaviorDuration":Lcom/miui/server/security/AppDurationService$UserPackageBehaviorDuration;
    .end local v12    # "duration":J
    .end local v14    # "moreThanThreshold":Z
    .restart local v16    # "userPackageBehaviorDuration":Lcom/miui/server/security/AppDurationService$UserPackageBehaviorDuration;
    :goto_3
    add-int/lit8 v10, v10, 0x1

    move-object/from16 v6, v16

    goto :goto_2

    .end local v16    # "userPackageBehaviorDuration":Lcom/miui/server/security/AppDurationService$UserPackageBehaviorDuration;
    .restart local v6    # "userPackageBehaviorDuration":Lcom/miui/server/security/AppDurationService$UserPackageBehaviorDuration;
    :cond_1
    move-object/from16 v16, v6

    .end local v6    # "userPackageBehaviorDuration":Lcom/miui/server/security/AppDurationService$UserPackageBehaviorDuration;
    .restart local v16    # "userPackageBehaviorDuration":Lcom/miui/server/security/AppDurationService$UserPackageBehaviorDuration;
    goto :goto_4

    .line 100
    .end local v10    # "k":I
    .end local v16    # "userPackageBehaviorDuration":Lcom/miui/server/security/AppDurationService$UserPackageBehaviorDuration;
    .restart local v6    # "userPackageBehaviorDuration":Lcom/miui/server/security/AppDurationService$UserPackageBehaviorDuration;
    :cond_2
    move-object/from16 v16, v6

    .line 111
    .end local v6    # "userPackageBehaviorDuration":Lcom/miui/server/security/AppDurationService$UserPackageBehaviorDuration;
    .restart local v16    # "userPackageBehaviorDuration":Lcom/miui/server/security/AppDurationService$UserPackageBehaviorDuration;
    :goto_4
    invoke-static {v9}, Lcom/miui/server/security/AppDurationService$TokenAndStartTime;->-$$Nest$fgetuidStartTimeMap(Lcom/miui/server/security/AppDurationService$TokenAndStartTime;)Landroid/util/SparseArray;

    move-result-object v6

    if-eqz v6, :cond_4

    invoke-static {v9}, Lcom/miui/server/security/AppDurationService$TokenAndStartTime;->-$$Nest$fgetuidStartTimeMap(Lcom/miui/server/security/AppDurationService$TokenAndStartTime;)Landroid/util/SparseArray;

    move-result-object v6

    invoke-virtual {v6}, Landroid/util/SparseArray;->size()I

    move-result v6

    if-lez v6, :cond_4

    .line 112
    const/4 v6, 0x0

    .local v6, "k":I
    :goto_5
    invoke-static {v9}, Lcom/miui/server/security/AppDurationService$TokenAndStartTime;->-$$Nest$fgetuidStartTimeMap(Lcom/miui/server/security/AppDurationService$TokenAndStartTime;)Landroid/util/SparseArray;

    move-result-object v10

    invoke-virtual {v10}, Landroid/util/SparseArray;->size()I

    move-result v10

    if-ge v6, v10, :cond_4

    .line 113
    invoke-static {v9}, Lcom/miui/server/security/AppDurationService$TokenAndStartTime;->-$$Nest$fgetuidStartTimeMap(Lcom/miui/server/security/AppDurationService$TokenAndStartTime;)Landroid/util/SparseArray;

    move-result-object v10

    invoke-virtual {v10, v6}, Landroid/util/SparseArray;->valueAt(I)Ljava/lang/Object;

    move-result-object v10

    check-cast v10, Ljava/lang/Long;

    invoke-virtual {v10}, Ljava/lang/Long;->longValue()J

    move-result-wide v10

    sub-long v10, v3, v10

    .line 114
    .local v10, "duration":J
    iget-object v12, v1, Lcom/miui/server/security/AppDurationService;->appBehavior:Lcom/miui/server/security/AppBehaviorService;

    invoke-virtual {v12, v8, v10, v11}, Lcom/miui/server/security/AppBehaviorService;->moreThanThreshold(IJ)Z

    move-result v12

    .line 115
    .local v12, "moreThanThreshold":Z
    if-eqz v12, :cond_3

    .line 116
    const-string v13, "AppDurationService"

    new-instance v14, Ljava/lang/StringBuilder;

    invoke-direct {v14}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v14, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v14

    const-string v15, ",behavior:"

    invoke-virtual {v14, v15}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v14

    invoke-virtual {v14, v8}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v14

    const-string v15, " has running too long. record and restart"

    invoke-virtual {v14, v15}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v14

    invoke-virtual {v14}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v14

    invoke-static {v13, v14}, Landroid/util/Slog;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 117
    invoke-static {v9}, Lcom/miui/server/security/AppDurationService$TokenAndStartTime;->-$$Nest$fgetuidStartTimeMap(Lcom/miui/server/security/AppDurationService$TokenAndStartTime;)Landroid/util/SparseArray;

    move-result-object v13

    invoke-static {v3, v4}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v14

    invoke-virtual {v13, v6, v14}, Landroid/util/SparseArray;->setValueAt(ILjava/lang/Object;)V

    .line 118
    iget-object v13, v1, Lcom/miui/server/security/AppDurationService;->appBehavior:Lcom/miui/server/security/AppBehaviorService;

    const/4 v14, 0x0

    invoke-static {v8, v5, v10, v11, v14}, Lmiui/security/AppBehavior;->buildCountEvent(ILjava/lang/String;JLjava/lang/String;)Lmiui/security/AppBehavior;

    move-result-object v15

    invoke-virtual {v13, v15}, Lcom/miui/server/security/AppBehaviorService;->recordAppBehaviorAsync(Lmiui/security/AppBehavior;)V

    goto :goto_6

    .line 115
    :cond_3
    const/4 v14, 0x0

    .line 112
    .end local v10    # "duration":J
    .end local v12    # "moreThanThreshold":Z
    :goto_6
    add-int/lit8 v6, v6, 0x1

    goto :goto_5

    .line 97
    .end local v6    # "k":I
    .end local v8    # "curBehavior":I
    .end local v9    # "tokenAndStartTime":Lcom/miui/server/security/AppDurationService$TokenAndStartTime;
    :cond_4
    add-int/lit8 v7, v7, 0x1

    move-object/from16 v6, v16

    goto/16 :goto_1

    .end local v16    # "userPackageBehaviorDuration":Lcom/miui/server/security/AppDurationService$UserPackageBehaviorDuration;
    .local v6, "userPackageBehaviorDuration":Lcom/miui/server/security/AppDurationService$UserPackageBehaviorDuration;
    :cond_5
    move-object/from16 v16, v6

    .line 94
    .end local v5    # "pkgName":Ljava/lang/String;
    .end local v6    # "userPackageBehaviorDuration":Lcom/miui/server/security/AppDurationService$UserPackageBehaviorDuration;
    .end local v7    # "j":I
    add-int/lit8 v0, v0, 0x1

    goto/16 :goto_0

    .line 124
    .end local v0    # "i":I
    .end local v3    # "current":J
    :cond_6
    monitor-exit v2

    .line 125
    return-void

    .line 124
    :catchall_0
    move-exception v0

    monitor-exit v2
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method

.method public onDurationEvent(ILandroid/os/IBinder;ILjava/lang/String;Z)V
    .locals 8
    .param p1, "behavior"    # I
    .param p2, "binder"    # Landroid/os/IBinder;
    .param p3, "uid"    # I
    .param p4, "pkgName"    # Ljava/lang/String;
    .param p5, "start"    # Z

    .line 42
    iget-object v0, p0, Lcom/miui/server/security/AppDurationService;->appBehavior:Lcom/miui/server/security/AppBehaviorService;

    invoke-virtual {v0, p1, p4}, Lcom/miui/server/security/AppBehaviorService;->canRecordBehavior(ILjava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 43
    return-void

    .line 45
    :cond_0
    new-instance v0, Lcom/miui/server/security/AppDurationService$UserPackage;

    invoke-direct {v0, p3, p4}, Lcom/miui/server/security/AppDurationService$UserPackage;-><init>(ILjava/lang/String;)V

    .line 46
    .local v0, "userPackage":Lcom/miui/server/security/AppDurationService$UserPackage;
    iget-object v1, p0, Lcom/miui/server/security/AppDurationService;->mDurationLock:Ljava/lang/Object;

    monitor-enter v1

    .line 47
    :try_start_0
    iget-object v2, p0, Lcom/miui/server/security/AppDurationService;->userPackageStartTimeAndTokenArrayMap:Landroid/util/ArrayMap;

    invoke-virtual {v2, v0}, Landroid/util/ArrayMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/miui/server/security/AppDurationService$UserPackageBehaviorDuration;

    .line 48
    .local v2, "userPackageBehaviorDuration":Lcom/miui/server/security/AppDurationService$UserPackageBehaviorDuration;
    if-nez p5, :cond_1

    if-nez v2, :cond_1

    .line 49
    monitor-exit v1

    return-void

    .line 51
    :cond_1
    const/4 v3, 0x0

    if-nez v2, :cond_2

    .line 52
    new-instance v4, Lcom/miui/server/security/AppDurationService$UserPackageBehaviorDuration;

    invoke-direct {v4, v3}, Lcom/miui/server/security/AppDurationService$UserPackageBehaviorDuration;-><init>(Lcom/miui/server/security/AppDurationService$UserPackageBehaviorDuration-IA;)V

    move-object v2, v4

    .line 53
    iget-object v4, p0, Lcom/miui/server/security/AppDurationService;->userPackageStartTimeAndTokenArrayMap:Landroid/util/ArrayMap;

    invoke-virtual {v4, v0, v2}, Landroid/util/ArrayMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 55
    :cond_2
    if-eqz p5, :cond_3

    .line 56
    invoke-virtual {v2, p1, p2, p3}, Lcom/miui/server/security/AppDurationService$UserPackageBehaviorDuration;->recordBehavior(ILandroid/os/IBinder;I)Z

    goto :goto_0

    .line 62
    :cond_3
    invoke-virtual {v2, p1, p2, p3}, Lcom/miui/server/security/AppDurationService$UserPackageBehaviorDuration;->stopRecordDuration(ILandroid/os/IBinder;I)J

    move-result-wide v4

    .line 63
    .local v4, "duration":J
    const-wide/16 v6, 0x0

    cmp-long v6, v4, v6

    if-lez v6, :cond_4

    .line 64
    iget-object v6, p0, Lcom/miui/server/security/AppDurationService;->appBehavior:Lcom/miui/server/security/AppBehaviorService;

    invoke-static {p1, p4, v4, v5, v3}, Lmiui/security/AppBehavior;->buildCountEvent(ILjava/lang/String;JLjava/lang/String;)Lmiui/security/AppBehavior;

    move-result-object v3

    invoke-virtual {v6, v3}, Lcom/miui/server/security/AppBehaviorService;->recordAppBehaviorAsync(Lmiui/security/AppBehavior;)V

    .line 71
    .end local v2    # "userPackageBehaviorDuration":Lcom/miui/server/security/AppDurationService$UserPackageBehaviorDuration;
    .end local v4    # "duration":J
    :cond_4
    :goto_0
    monitor-exit v1

    .line 72
    return-void

    .line 71
    :catchall_0
    move-exception v2

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v2
.end method

.method public removeDurationEvent(IILjava/lang/String;)V
    .locals 4
    .param p1, "behavior"    # I
    .param p2, "uid"    # I
    .param p3, "pkgName"    # Ljava/lang/String;

    .line 75
    new-instance v0, Lcom/miui/server/security/AppDurationService$UserPackage;

    invoke-direct {v0, p2, p3}, Lcom/miui/server/security/AppDurationService$UserPackage;-><init>(ILjava/lang/String;)V

    .line 76
    .local v0, "userPackage":Lcom/miui/server/security/AppDurationService$UserPackage;
    iget-object v1, p0, Lcom/miui/server/security/AppDurationService;->mDurationLock:Ljava/lang/Object;

    monitor-enter v1

    .line 77
    :try_start_0
    iget-object v2, p0, Lcom/miui/server/security/AppDurationService;->userPackageStartTimeAndTokenArrayMap:Landroid/util/ArrayMap;

    invoke-virtual {v2, v0}, Landroid/util/ArrayMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/miui/server/security/AppDurationService$UserPackageBehaviorDuration;

    .line 78
    .local v2, "userPackageBehaviorDuration":Lcom/miui/server/security/AppDurationService$UserPackageBehaviorDuration;
    if-nez v2, :cond_0

    .line 79
    monitor-exit v1

    return-void

    .line 81
    :cond_0
    invoke-static {v2}, Lcom/miui/server/security/AppDurationService$UserPackageBehaviorDuration;->-$$Nest$fgetbehaviorDurationMap(Lcom/miui/server/security/AppDurationService$UserPackageBehaviorDuration;)Landroid/util/SparseArray;

    move-result-object v3

    if-eqz v3, :cond_1

    .line 82
    invoke-static {v2}, Lcom/miui/server/security/AppDurationService$UserPackageBehaviorDuration;->-$$Nest$fgetbehaviorDurationMap(Lcom/miui/server/security/AppDurationService$UserPackageBehaviorDuration;)Landroid/util/SparseArray;

    move-result-object v3

    invoke-virtual {v3, p1}, Landroid/util/SparseArray;->remove(I)V

    .line 83
    invoke-static {v2}, Lcom/miui/server/security/AppDurationService$UserPackageBehaviorDuration;->-$$Nest$fgetbehaviorDurationMap(Lcom/miui/server/security/AppDurationService$UserPackageBehaviorDuration;)Landroid/util/SparseArray;

    move-result-object v3

    invoke-virtual {v3}, Landroid/util/SparseArray;->size()I

    move-result v3

    if-nez v3, :cond_1

    .line 84
    iget-object v3, p0, Lcom/miui/server/security/AppDurationService;->userPackageStartTimeAndTokenArrayMap:Landroid/util/ArrayMap;

    invoke-virtual {v3, v0}, Landroid/util/ArrayMap;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    .line 87
    .end local v2    # "userPackageBehaviorDuration":Lcom/miui/server/security/AppDurationService$UserPackageBehaviorDuration;
    :cond_1
    monitor-exit v1

    .line 88
    return-void

    .line 87
    :catchall_0
    move-exception v2

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v2
.end method
