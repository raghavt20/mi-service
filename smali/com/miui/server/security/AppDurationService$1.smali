.class Lcom/miui/server/security/AppDurationService$1;
.super Landroid/app/IForegroundServiceObserver$Stub;
.source "AppDurationService.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/miui/server/security/AppDurationService;-><init>(Lcom/miui/server/security/AppBehaviorService;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/miui/server/security/AppDurationService;

.field final synthetic val$appBehavior:Lcom/miui/server/security/AppBehaviorService;


# direct methods
.method constructor <init>(Lcom/miui/server/security/AppDurationService;Lcom/miui/server/security/AppBehaviorService;)V
    .locals 0
    .param p1, "this$0"    # Lcom/miui/server/security/AppDurationService;

    .line 28
    iput-object p1, p0, Lcom/miui/server/security/AppDurationService$1;->this$0:Lcom/miui/server/security/AppDurationService;

    iput-object p2, p0, Lcom/miui/server/security/AppDurationService$1;->val$appBehavior:Lcom/miui/server/security/AppBehaviorService;

    invoke-direct {p0}, Landroid/app/IForegroundServiceObserver$Stub;-><init>()V

    return-void
.end method


# virtual methods
.method public onForegroundStateChanged(Landroid/os/IBinder;Ljava/lang/String;IZ)V
    .locals 9
    .param p1, "serviceToken"    # Landroid/os/IBinder;
    .param p2, "packageName"    # Ljava/lang/String;
    .param p3, "userId"    # I
    .param p4, "isForeground"    # Z

    .line 31
    iget-object v0, p0, Lcom/miui/server/security/AppDurationService$1;->val$appBehavior:Lcom/miui/server/security/AppBehaviorService;

    iget-object v0, v0, Lcom/miui/server/security/AppBehaviorService;->mPackageManagerInt:Landroid/content/pm/PackageManagerInternal;

    const-wide/16 v1, 0x0

    invoke-virtual {v0, p2, v1, v2, p3}, Landroid/content/pm/PackageManagerInternal;->getPackageUid(Ljava/lang/String;JI)I

    move-result v0

    .line 32
    .local v0, "uid":I
    iget-object v3, p0, Lcom/miui/server/security/AppDurationService$1;->this$0:Lcom/miui/server/security/AppDurationService;

    const/16 v4, 0x19

    move-object v5, p1

    move v6, v0

    move-object v7, p2

    move v8, p4

    invoke-virtual/range {v3 .. v8}, Lcom/miui/server/security/AppDurationService;->onDurationEvent(ILandroid/os/IBinder;ILjava/lang/String;Z)V

    .line 33
    return-void
.end method
