public class com.miui.server.security.WakeUpTimeImpl {
	 /* .source "WakeUpTimeImpl.java" */
	 /* # static fields */
	 private static final java.lang.String CLASS_NAME;
	 private static final java.lang.String CLASS_NAMES;
	 private static final java.lang.String NAME;
	 private static final java.lang.String TAG;
	 private static final java.lang.String TIME;
	 /* # instance fields */
	 private final android.os.Handler mHandler;
	 private Long mWakeTime;
	 private final android.util.AtomicFile mWakeUpFile;
	 private final java.util.HashMap mWakeUpTime;
	 /* .annotation system Ldalvik/annotation/Signature; */
	 /* value = { */
	 /* "Ljava/util/HashMap<", */
	 /* "Ljava/lang/String;", */
	 /* "Ljava/lang/Long;", */
	 /* ">;" */
	 /* } */
} // .end annotation
} // .end field
/* # direct methods */
public com.miui.server.security.WakeUpTimeImpl ( ) {
/* .locals 4 */
/* .param p1, "service" # Lcom/miui/server/SecurityManagerService; */
/* .line 33 */
/* invoke-direct {p0}, Ljava/lang/Object;-><init>()V */
/* .line 29 */
/* new-instance v0, Landroid/util/AtomicFile; */
/* new-instance v1, Ljava/io/File; */
android.os.Environment .getDataSystemDirectory ( );
final String v3 = "miui-wakeuptime.xml"; // const-string v3, "miui-wakeuptime.xml"
/* invoke-direct {v1, v2, v3}, Ljava/io/File;-><init>(Ljava/io/File;Ljava/lang/String;)V */
/* invoke-direct {v0, v1}, Landroid/util/AtomicFile;-><init>(Ljava/io/File;)V */
this.mWakeUpFile = v0;
/* .line 30 */
/* new-instance v0, Ljava/util/HashMap; */
/* invoke-direct {v0}, Ljava/util/HashMap;-><init>()V */
this.mWakeUpTime = v0;
/* .line 31 */
/* const-wide/16 v0, 0x0 */
/* iput-wide v0, p0, Lcom/miui/server/security/WakeUpTimeImpl;->mWakeTime:J */
/* .line 34 */
v0 = this.mSecurityWriteHandler;
this.mHandler = v0;
/* .line 35 */
return;
} // .end method
private void minWakeUpTime ( Long p0 ) {
/* .locals 10 */
/* .param p1, "nowtime" # J */
/* .line 137 */
/* const-wide/16 v0, 0x0 */
/* .line 138 */
/* .local v0, "min":J */
/* const-wide/16 v2, 0x12c */
/* add-long/2addr v2, p1 */
/* .line 139 */
/* .local v2, "rightBorder":J */
v4 = this.mWakeUpTime;
(( java.util.HashMap ) v4 ).keySet ( ); // invoke-virtual {v4}, Ljava/util/HashMap;->keySet()Ljava/util/Set;
v5 = } // :goto_0
if ( v5 != null) { // if-eqz v5, :cond_2
/* check-cast v5, Ljava/lang/String; */
/* .line 140 */
/* .local v5, "componentName":Ljava/lang/String; */
(( com.miui.server.security.WakeUpTimeImpl ) p0 ).getBootTimeFromMap ( v5 ); // invoke-virtual {p0, v5}, Lcom/miui/server/security/WakeUpTimeImpl;->getBootTimeFromMap(Ljava/lang/String;)J
/* move-result-wide v6 */
/* .line 143 */
/* .local v6, "tmp":J */
/* cmp-long v8, v6, p1 */
/* if-ltz v8, :cond_1 */
/* cmp-long v8, v6, v0 */
/* if-ltz v8, :cond_0 */
/* const-wide/16 v8, 0x0 */
/* cmp-long v8, v0, v8 */
/* if-nez v8, :cond_1 */
/* .line 144 */
} // :cond_0
java.lang.Math .max ( v6,v7,v2,v3 );
/* move-result-wide v0 */
/* .line 146 */
} // .end local v5 # "componentName":Ljava/lang/String;
} // .end local v6 # "tmp":J
} // :cond_1
/* .line 147 */
} // :cond_2
/* iput-wide v0, p0, Lcom/miui/server/security/WakeUpTimeImpl;->mWakeTime:J */
/* .line 148 */
return;
} // .end method
private void putBootTimeToMap ( java.lang.String p0, Long p1 ) {
/* .locals 3 */
/* .param p1, "componentName" # Ljava/lang/String; */
/* .param p2, "time" # J */
/* .line 118 */
v0 = this.mWakeUpTime;
/* monitor-enter v0 */
/* .line 119 */
try { // :try_start_0
v1 = this.mWakeUpTime;
java.lang.Long .valueOf ( p2,p3 );
(( java.util.HashMap ) v1 ).put ( p1, v2 ); // invoke-virtual {v1, p1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;
/* .line 120 */
/* monitor-exit v0 */
/* .line 121 */
return;
/* .line 120 */
/* :catchall_0 */
/* move-exception v1 */
/* monitor-exit v0 */
/* :try_end_0 */
/* .catchall {:try_start_0 ..:try_end_0} :catchall_0 */
/* throw v1 */
} // .end method
private void readWakeUpTime ( java.io.FileInputStream p0 ) {
/* .locals 9 */
/* .param p1, "fis" # Ljava/io/FileInputStream; */
/* .annotation system Ldalvik/annotation/Throws; */
/* value = { */
/* Ljava/lang/Exception; */
/* } */
} // .end annotation
/* .line 93 */
android.util.Xml .newPullParser ( );
/* .line 94 */
/* .local v0, "parser":Lorg/xmlpull/v1/XmlPullParser; */
int v1 = 0; // const/4 v1, 0x0
v2 = /* .line 96 */
/* .line 97 */
/* .local v2, "eventType":I */
} // :goto_0
int v3 = 1; // const/4 v3, 0x1
int v4 = 2; // const/4 v4, 0x2
/* if-eq v2, v4, :cond_0 */
/* if-eq v2, v3, :cond_0 */
v2 = /* .line 98 */
/* .line 100 */
} // :cond_0
/* .line 101 */
/* .local v5, "tagName":Ljava/lang/String; */
final String v6 = "classnames"; // const-string v6, "classnames"
v6 = (( java.lang.String ) v6 ).equals ( v5 ); // invoke-virtual {v6, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z
if ( v6 != null) { // if-eqz v6, :cond_3
v2 = /* .line 102 */
/* .line 104 */
} // :cond_1
v6 = /* if-ne v2, v4, :cond_2 */
/* if-ne v6, v4, :cond_2 */
/* .line 105 */
/* .line 106 */
final String v6 = "classname"; // const-string v6, "classname"
v6 = (( java.lang.String ) v6 ).equals ( v5 ); // invoke-virtual {v6, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z
if ( v6 != null) { // if-eqz v6, :cond_2
/* .line 107 */
final String v6 = "name"; // const-string v6, "name"
/* .line 108 */
/* .local v6, "componentName":Ljava/lang/String; */
/* const-string/jumbo v7, "time" */
java.lang.Long .parseLong ( v7 );
/* move-result-wide v7 */
/* .line 109 */
/* .local v7, "time":J */
/* invoke-direct {p0, v6, v7, v8}, Lcom/miui/server/security/WakeUpTimeImpl;->putBootTimeToMap(Ljava/lang/String;J)V */
/* .line 112 */
} // .end local v6 # "componentName":Ljava/lang/String;
} // .end local v7 # "time":J
v2 = } // :cond_2
/* .line 113 */
/* if-ne v2, v3, :cond_1 */
/* .line 115 */
} // :cond_3
return;
} // .end method
private void scheduleWriteBootTime ( ) {
/* .locals 3 */
/* .line 129 */
v0 = this.mHandler;
int v1 = 3; // const/4 v1, 0x3
v0 = (( android.os.Handler ) v0 ).hasMessages ( v1 ); // invoke-virtual {v0, v1}, Landroid/os/Handler;->hasMessages(I)Z
if ( v0 != null) { // if-eqz v0, :cond_0
return;
/* .line 130 */
} // :cond_0
v0 = this.mHandler;
(( android.os.Handler ) v0 ).obtainMessage ( v1 ); // invoke-virtual {v0, v1}, Landroid/os/Handler;->obtainMessage(I)Landroid/os/Message;
/* .line 131 */
/* .local v0, "msg":Landroid/os/Message; */
/* iget-wide v1, p0, Lcom/miui/server/security/WakeUpTimeImpl;->mWakeTime:J */
java.lang.Long .valueOf ( v1,v2 );
this.obj = v1;
/* .line 132 */
v1 = this.mHandler;
(( android.os.Handler ) v1 ).sendMessage ( v0 ); // invoke-virtual {v1, v0}, Landroid/os/Handler;->sendMessage(Landroid/os/Message;)Z
/* .line 133 */
return;
} // .end method
private void scheduleWriteWakeUpTime ( ) {
/* .locals 2 */
/* .line 124 */
v0 = this.mHandler;
int v1 = 2; // const/4 v1, 0x2
v0 = (( android.os.Handler ) v0 ).hasMessages ( v1 ); // invoke-virtual {v0, v1}, Landroid/os/Handler;->hasMessages(I)Z
if ( v0 != null) { // if-eqz v0, :cond_0
return;
/* .line 125 */
} // :cond_0
v0 = this.mHandler;
(( android.os.Handler ) v0 ).sendEmptyMessage ( v1 ); // invoke-virtual {v0, v1}, Landroid/os/Handler;->sendEmptyMessage(I)Z
/* .line 126 */
return;
} // .end method
private void setTimeBoot ( ) {
/* .locals 4 */
/* .line 151 */
java.lang.System .currentTimeMillis ( );
/* move-result-wide v0 */
/* const-wide/16 v2, 0x3e8 */
/* div-long/2addr v0, v2 */
/* .line 152 */
/* .local v0, "now_time":J */
v2 = this.mWakeUpTime;
/* monitor-enter v2 */
/* .line 153 */
try { // :try_start_0
/* invoke-direct {p0, v0, v1}, Lcom/miui/server/security/WakeUpTimeImpl;->minWakeUpTime(J)V */
/* .line 154 */
/* monitor-exit v2 */
/* :try_end_0 */
/* .catchall {:try_start_0 ..:try_end_0} :catchall_0 */
/* .line 155 */
/* invoke-direct {p0}, Lcom/miui/server/security/WakeUpTimeImpl;->scheduleWriteBootTime()V */
/* .line 156 */
return;
/* .line 154 */
/* :catchall_0 */
/* move-exception v3 */
try { // :try_start_1
/* monitor-exit v2 */
/* :try_end_1 */
/* .catchall {:try_start_1 ..:try_end_1} :catchall_0 */
/* throw v3 */
} // .end method
/* # virtual methods */
public Long getBootTimeFromMap ( java.lang.String p0 ) {
/* .locals 3 */
/* .param p1, "componentName" # Ljava/lang/String; */
/* .line 87 */
v0 = this.mWakeUpTime;
/* monitor-enter v0 */
/* .line 88 */
try { // :try_start_0
v1 = this.mWakeUpTime;
v1 = (( java.util.HashMap ) v1 ).containsKey ( p1 ); // invoke-virtual {v1, p1}, Ljava/util/HashMap;->containsKey(Ljava/lang/Object;)Z
if ( v1 != null) { // if-eqz v1, :cond_0
v1 = this.mWakeUpTime;
(( java.util.HashMap ) v1 ).get ( p1 ); // invoke-virtual {v1, p1}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;
/* check-cast v1, Ljava/lang/Long; */
(( java.lang.Long ) v1 ).longValue ( ); // invoke-virtual {v1}, Ljava/lang/Long;->longValue()J
/* move-result-wide v1 */
} // :cond_0
/* const-wide/16 v1, 0x0 */
} // :goto_0
/* monitor-exit v0 */
/* return-wide v1 */
/* .line 89 */
/* :catchall_0 */
/* move-exception v1 */
/* monitor-exit v0 */
/* :try_end_0 */
/* .catchall {:try_start_0 ..:try_end_0} :catchall_0 */
/* throw v1 */
} // .end method
public void readWakeUpTime ( ) {
/* .locals 3 */
/* .line 73 */
v0 = this.mWakeUpTime;
/* monitor-enter v0 */
/* .line 74 */
try { // :try_start_0
v1 = this.mWakeUpTime;
(( java.util.HashMap ) v1 ).clear ( ); // invoke-virtual {v1}, Ljava/util/HashMap;->clear()V
/* .line 75 */
/* monitor-exit v0 */
/* :try_end_0 */
/* .catchall {:try_start_0 ..:try_end_0} :catchall_2 */
/* .line 76 */
v0 = this.mWakeUpFile;
(( android.util.AtomicFile ) v0 ).getBaseFile ( ); // invoke-virtual {v0}, Landroid/util/AtomicFile;->getBaseFile()Ljava/io/File;
v0 = (( java.io.File ) v0 ).exists ( ); // invoke-virtual {v0}, Ljava/io/File;->exists()Z
/* if-nez v0, :cond_0 */
/* .line 77 */
return;
/* .line 79 */
} // :cond_0
try { // :try_start_1
v0 = this.mWakeUpFile;
(( android.util.AtomicFile ) v0 ).openRead ( ); // invoke-virtual {v0}, Landroid/util/AtomicFile;->openRead()Ljava/io/FileInputStream;
/* :try_end_1 */
/* .catch Ljava/lang/Exception; {:try_start_1 ..:try_end_1} :catch_0 */
/* .line 80 */
/* .local v0, "fis":Ljava/io/FileInputStream; */
try { // :try_start_2
/* invoke-direct {p0, v0}, Lcom/miui/server/security/WakeUpTimeImpl;->readWakeUpTime(Ljava/io/FileInputStream;)V */
/* :try_end_2 */
/* .catchall {:try_start_2 ..:try_end_2} :catchall_0 */
/* .line 81 */
if ( v0 != null) { // if-eqz v0, :cond_1
try { // :try_start_3
(( java.io.FileInputStream ) v0 ).close ( ); // invoke-virtual {v0}, Ljava/io/FileInputStream;->close()V
/* :try_end_3 */
/* .catch Ljava/lang/Exception; {:try_start_3 ..:try_end_3} :catch_0 */
/* .line 83 */
} // .end local v0 # "fis":Ljava/io/FileInputStream;
} // :cond_1
/* .line 79 */
/* .restart local v0 # "fis":Ljava/io/FileInputStream; */
/* :catchall_0 */
/* move-exception v1 */
if ( v0 != null) { // if-eqz v0, :cond_2
try { // :try_start_4
(( java.io.FileInputStream ) v0 ).close ( ); // invoke-virtual {v0}, Ljava/io/FileInputStream;->close()V
/* :try_end_4 */
/* .catchall {:try_start_4 ..:try_end_4} :catchall_1 */
/* :catchall_1 */
/* move-exception v2 */
try { // :try_start_5
(( java.lang.Throwable ) v1 ).addSuppressed ( v2 ); // invoke-virtual {v1, v2}, Ljava/lang/Throwable;->addSuppressed(Ljava/lang/Throwable;)V
} // .end local p0 # "this":Lcom/miui/server/security/WakeUpTimeImpl;
} // :cond_2
} // :goto_0
/* throw v1 */
/* :try_end_5 */
/* .catch Ljava/lang/Exception; {:try_start_5 ..:try_end_5} :catch_0 */
/* .line 81 */
} // .end local v0 # "fis":Ljava/io/FileInputStream;
/* .restart local p0 # "this":Lcom/miui/server/security/WakeUpTimeImpl; */
/* :catch_0 */
/* move-exception v0 */
/* .line 82 */
/* .local v0, "e":Ljava/lang/Exception; */
v1 = this.mWakeUpFile;
(( android.util.AtomicFile ) v1 ).getBaseFile ( ); // invoke-virtual {v1}, Landroid/util/AtomicFile;->getBaseFile()Ljava/io/File;
(( java.io.File ) v1 ).delete ( ); // invoke-virtual {v1}, Ljava/io/File;->delete()Z
/* .line 84 */
} // .end local v0 # "e":Ljava/lang/Exception;
} // :goto_1
return;
/* .line 75 */
/* :catchall_2 */
/* move-exception v1 */
try { // :try_start_6
/* monitor-exit v0 */
/* :try_end_6 */
/* .catchall {:try_start_6 ..:try_end_6} :catchall_2 */
/* throw v1 */
} // .end method
public void setWakeUpTime ( java.lang.String p0, Long p1 ) {
/* .locals 0 */
/* .param p1, "componentName" # Ljava/lang/String; */
/* .param p2, "timeInSeconds" # J */
/* .line 38 */
/* invoke-direct {p0, p1, p2, p3}, Lcom/miui/server/security/WakeUpTimeImpl;->putBootTimeToMap(Ljava/lang/String;J)V */
/* .line 39 */
/* invoke-direct {p0}, Lcom/miui/server/security/WakeUpTimeImpl;->scheduleWriteWakeUpTime()V */
/* .line 40 */
/* invoke-direct {p0}, Lcom/miui/server/security/WakeUpTimeImpl;->setTimeBoot()V */
/* .line 41 */
return;
} // .end method
public void writeWakeUpTime ( ) {
/* .locals 10 */
/* .line 44 */
int v0 = 0; // const/4 v0, 0x0
/* .line 46 */
/* .local v0, "fos":Ljava/io/FileOutputStream; */
try { // :try_start_0
v1 = this.mWakeUpFile;
(( android.util.AtomicFile ) v1 ).startWrite ( ); // invoke-virtual {v1}, Landroid/util/AtomicFile;->startWrite()Ljava/io/FileOutputStream;
/* move-object v0, v1 */
/* .line 47 */
/* new-instance v1, Lcom/android/internal/util/FastXmlSerializer; */
/* invoke-direct {v1}, Lcom/android/internal/util/FastXmlSerializer;-><init>()V */
/* .line 48 */
/* .local v1, "out":Lorg/xmlpull/v1/XmlSerializer; */
/* const-string/jumbo v2, "utf-8" */
/* .line 49 */
int v2 = 1; // const/4 v2, 0x1
java.lang.Boolean .valueOf ( v2 );
int v4 = 0; // const/4 v4, 0x0
/* .line 50 */
final String v3 = "http://xmlpull.org/v1/doc/features.html#indent-output"; // const-string v3, "http://xmlpull.org/v1/doc/features.html#indent-output"
/* .line 51 */
final String v2 = "classnames"; // const-string v2, "classnames"
/* .line 52 */
v2 = this.mWakeUpTime;
/* monitor-enter v2 */
/* :try_end_0 */
/* .catch Ljava/io/IOException; {:try_start_0 ..:try_end_0} :catch_0 */
/* .line 53 */
try { // :try_start_1
v3 = this.mWakeUpTime;
(( java.util.HashMap ) v3 ).keySet ( ); // invoke-virtual {v3}, Ljava/util/HashMap;->keySet()Ljava/util/Set;
v5 = } // :goto_0
if ( v5 != null) { // if-eqz v5, :cond_1
/* check-cast v5, Ljava/lang/String; */
/* .line 54 */
/* .local v5, "componentName":Ljava/lang/String; */
(( com.miui.server.security.WakeUpTimeImpl ) p0 ).getBootTimeFromMap ( v5 ); // invoke-virtual {p0, v5}, Lcom/miui/server/security/WakeUpTimeImpl;->getBootTimeFromMap(Ljava/lang/String;)J
/* move-result-wide v6 */
/* const-wide/16 v8, 0x0 */
/* cmp-long v6, v6, v8 */
if ( v6 != null) { // if-eqz v6, :cond_0
/* .line 55 */
final String v6 = "classname"; // const-string v6, "classname"
/* .line 56 */
final String v6 = "name"; // const-string v6, "name"
/* .line 57 */
/* const-string/jumbo v6, "time" */
(( com.miui.server.security.WakeUpTimeImpl ) p0 ).getBootTimeFromMap ( v5 ); // invoke-virtual {p0, v5}, Lcom/miui/server/security/WakeUpTimeImpl;->getBootTimeFromMap(Ljava/lang/String;)J
/* move-result-wide v7 */
java.lang.String .valueOf ( v7,v8 );
/* .line 58 */
final String v6 = "classname"; // const-string v6, "classname"
/* .line 60 */
} // .end local v5 # "componentName":Ljava/lang/String;
} // :cond_0
/* .line 61 */
} // :cond_1
/* monitor-exit v2 */
/* :try_end_1 */
/* .catchall {:try_start_1 ..:try_end_1} :catchall_0 */
/* .line 62 */
try { // :try_start_2
final String v2 = "classnames"; // const-string v2, "classnames"
/* .line 63 */
/* .line 64 */
v2 = this.mWakeUpFile;
(( android.util.AtomicFile ) v2 ).finishWrite ( v0 ); // invoke-virtual {v2, v0}, Landroid/util/AtomicFile;->finishWrite(Ljava/io/FileOutputStream;)V
/* :try_end_2 */
/* .catch Ljava/io/IOException; {:try_start_2 ..:try_end_2} :catch_0 */
/* .line 69 */
} // .end local v1 # "out":Lorg/xmlpull/v1/XmlSerializer;
/* .line 61 */
/* .restart local v1 # "out":Lorg/xmlpull/v1/XmlSerializer; */
/* :catchall_0 */
/* move-exception v3 */
try { // :try_start_3
/* monitor-exit v2 */
/* :try_end_3 */
/* .catchall {:try_start_3 ..:try_end_3} :catchall_0 */
} // .end local v0 # "fos":Ljava/io/FileOutputStream;
} // .end local p0 # "this":Lcom/miui/server/security/WakeUpTimeImpl;
try { // :try_start_4
/* throw v3 */
/* :try_end_4 */
/* .catch Ljava/io/IOException; {:try_start_4 ..:try_end_4} :catch_0 */
/* .line 65 */
} // .end local v1 # "out":Lorg/xmlpull/v1/XmlSerializer;
/* .restart local v0 # "fos":Ljava/io/FileOutputStream; */
/* .restart local p0 # "this":Lcom/miui/server/security/WakeUpTimeImpl; */
/* :catch_0 */
/* move-exception v1 */
/* .line 66 */
/* .local v1, "e1":Ljava/io/IOException; */
if ( v0 != null) { // if-eqz v0, :cond_2
/* .line 67 */
v2 = this.mWakeUpFile;
(( android.util.AtomicFile ) v2 ).failWrite ( v0 ); // invoke-virtual {v2, v0}, Landroid/util/AtomicFile;->failWrite(Ljava/io/FileOutputStream;)V
/* .line 70 */
} // .end local v1 # "e1":Ljava/io/IOException;
} // :cond_2
} // :goto_1
return;
} // .end method
