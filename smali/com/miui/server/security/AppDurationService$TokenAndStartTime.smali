.class Lcom/miui/server/security/AppDurationService$TokenAndStartTime;
.super Ljava/lang/Object;
.source "AppDurationService.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/miui/server/security/AppDurationService;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0xa
    name = "TokenAndStartTime"
.end annotation


# instance fields
.field private binderStartTimeMap:Landroid/util/ArrayMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/util/ArrayMap<",
            "Landroid/os/IBinder;",
            "Ljava/lang/Long;",
            ">;"
        }
    .end annotation
.end field

.field private uidStartTimeMap:Landroid/util/SparseArray;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/util/SparseArray<",
            "Ljava/lang/Long;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method static bridge synthetic -$$Nest$fgetbinderStartTimeMap(Lcom/miui/server/security/AppDurationService$TokenAndStartTime;)Landroid/util/ArrayMap;
    .locals 0

    iget-object p0, p0, Lcom/miui/server/security/AppDurationService$TokenAndStartTime;->binderStartTimeMap:Landroid/util/ArrayMap;

    return-object p0
.end method

.method static bridge synthetic -$$Nest$fgetuidStartTimeMap(Lcom/miui/server/security/AppDurationService$TokenAndStartTime;)Landroid/util/SparseArray;
    .locals 0

    iget-object p0, p0, Lcom/miui/server/security/AppDurationService$TokenAndStartTime;->uidStartTimeMap:Landroid/util/SparseArray;

    return-object p0
.end method

.method private constructor <init>()V
    .locals 0

    .line 183
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method synthetic constructor <init>(Lcom/miui/server/security/AppDurationService$TokenAndStartTime-IA;)V
    .locals 0

    invoke-direct {p0}, Lcom/miui/server/security/AppDurationService$TokenAndStartTime;-><init>()V

    return-void
.end method


# virtual methods
.method public recordBinderTime(Landroid/os/IBinder;)Z
    .locals 3
    .param p1, "binder"    # Landroid/os/IBinder;

    .line 188
    iget-object v0, p0, Lcom/miui/server/security/AppDurationService$TokenAndStartTime;->binderStartTimeMap:Landroid/util/ArrayMap;

    if-nez v0, :cond_0

    .line 189
    new-instance v0, Landroid/util/ArrayMap;

    invoke-direct {v0}, Landroid/util/ArrayMap;-><init>()V

    iput-object v0, p0, Lcom/miui/server/security/AppDurationService$TokenAndStartTime;->binderStartTimeMap:Landroid/util/ArrayMap;

    .line 191
    :cond_0
    iget-object v0, p0, Lcom/miui/server/security/AppDurationService$TokenAndStartTime;->binderStartTimeMap:Landroid/util/ArrayMap;

    invoke-virtual {v0, p1}, Landroid/util/ArrayMap;->containsKey(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_1

    .line 192
    iget-object v0, p0, Lcom/miui/server/security/AppDurationService$TokenAndStartTime;->binderStartTimeMap:Landroid/util/ArrayMap;

    invoke-static {}, Landroid/os/SystemClock;->elapsedRealtime()J

    move-result-wide v1

    invoke-static {v1, v2}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v1

    invoke-virtual {v0, p1, v1}, Landroid/util/ArrayMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 193
    const/4 v0, 0x1

    return v0

    .line 195
    :cond_1
    const/4 v0, 0x0

    return v0
.end method

.method public recordUidTime(I)Z
    .locals 3
    .param p1, "uid"    # I

    .line 211
    iget-object v0, p0, Lcom/miui/server/security/AppDurationService$TokenAndStartTime;->uidStartTimeMap:Landroid/util/SparseArray;

    if-nez v0, :cond_0

    .line 212
    new-instance v0, Landroid/util/SparseArray;

    invoke-direct {v0}, Landroid/util/SparseArray;-><init>()V

    iput-object v0, p0, Lcom/miui/server/security/AppDurationService$TokenAndStartTime;->uidStartTimeMap:Landroid/util/SparseArray;

    .line 214
    :cond_0
    iget-object v0, p0, Lcom/miui/server/security/AppDurationService$TokenAndStartTime;->uidStartTimeMap:Landroid/util/SparseArray;

    invoke-virtual {v0, p1}, Landroid/util/SparseArray;->contains(I)Z

    move-result v0

    if-nez v0, :cond_1

    .line 215
    iget-object v0, p0, Lcom/miui/server/security/AppDurationService$TokenAndStartTime;->uidStartTimeMap:Landroid/util/SparseArray;

    invoke-static {}, Landroid/os/SystemClock;->elapsedRealtime()J

    move-result-wide v1

    invoke-static {v1, v2}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v1

    invoke-virtual {v0, p1, v1}, Landroid/util/SparseArray;->put(ILjava/lang/Object;)V

    .line 216
    const/4 v0, 0x1

    return v0

    .line 218
    :cond_1
    const/4 v0, 0x0

    return v0
.end method

.method public stopBinderTime(Landroid/os/IBinder;)J
    .locals 6
    .param p1, "binder"    # Landroid/os/IBinder;

    .line 199
    if-eqz p1, :cond_1

    iget-object v0, p0, Lcom/miui/server/security/AppDurationService$TokenAndStartTime;->binderStartTimeMap:Landroid/util/ArrayMap;

    if-eqz v0, :cond_1

    .line 201
    invoke-virtual {v0}, Landroid/util/ArrayMap;->size()I

    move-result v0

    if-nez v0, :cond_0

    goto :goto_0

    .line 204
    :cond_0
    invoke-static {}, Landroid/os/SystemClock;->elapsedRealtime()J

    move-result-wide v0

    .line 205
    .local v0, "curTime":J
    iget-object v2, p0, Lcom/miui/server/security/AppDurationService$TokenAndStartTime;->binderStartTimeMap:Landroid/util/ArrayMap;

    invoke-static {v0, v1}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v3

    invoke-virtual {v2, p1, v3}, Landroid/util/ArrayMap;->getOrDefault(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/lang/Long;

    invoke-virtual {v2}, Ljava/lang/Long;->longValue()J

    move-result-wide v2

    .line 206
    .local v2, "startTime":J
    iget-object v4, p0, Lcom/miui/server/security/AppDurationService$TokenAndStartTime;->binderStartTimeMap:Landroid/util/ArrayMap;

    invoke-virtual {v4, p1}, Landroid/util/ArrayMap;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    .line 207
    sub-long v4, v0, v2

    return-wide v4

    .line 202
    .end local v0    # "curTime":J
    .end local v2    # "startTime":J
    :cond_1
    :goto_0
    const-wide/16 v0, 0x0

    return-wide v0
.end method

.method public stopUidTime(I)J
    .locals 6
    .param p1, "uid"    # I

    .line 222
    iget-object v0, p0, Lcom/miui/server/security/AppDurationService$TokenAndStartTime;->uidStartTimeMap:Landroid/util/SparseArray;

    if-eqz v0, :cond_1

    .line 223
    invoke-virtual {v0}, Landroid/util/SparseArray;->size()I

    move-result v0

    if-nez v0, :cond_0

    goto :goto_0

    .line 226
    :cond_0
    invoke-static {}, Landroid/os/SystemClock;->elapsedRealtime()J

    move-result-wide v0

    .line 227
    .local v0, "curTime":J
    iget-object v2, p0, Lcom/miui/server/security/AppDurationService$TokenAndStartTime;->uidStartTimeMap:Landroid/util/SparseArray;

    invoke-static {v0, v1}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v3

    invoke-virtual {v2, p1, v3}, Landroid/util/SparseArray;->get(ILjava/lang/Object;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/lang/Long;

    invoke-virtual {v2}, Ljava/lang/Long;->longValue()J

    move-result-wide v2

    .line 228
    .local v2, "startTime":J
    iget-object v4, p0, Lcom/miui/server/security/AppDurationService$TokenAndStartTime;->uidStartTimeMap:Landroid/util/SparseArray;

    invoke-virtual {v4, p1}, Landroid/util/SparseArray;->remove(I)V

    .line 229
    sub-long v4, v0, v2

    return-wide v4

    .line 224
    .end local v0    # "curTime":J
    .end local v2    # "startTime":J
    :cond_1
    :goto_0
    const-wide/16 v0, 0x0

    return-wide v0
.end method
