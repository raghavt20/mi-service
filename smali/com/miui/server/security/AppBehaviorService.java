public class com.miui.server.security.AppBehaviorService {
	 /* .source "AppBehaviorService.java" */
	 /* # static fields */
	 private static final android.net.Uri ACCESSIBILITY_URI;
	 private static final java.lang.String COMPONENT_NAME_SEPARATOR;
	 public static final Boolean DEBUG;
	 private static final Integer MAX_CACHE_COUNT;
	 private static final Integer MSG_BIND_SEC;
	 private static final Integer MSG_DURATION_TOO_LONG_CHECK;
	 private static final Integer MSG_FOREGROUND_CHECK;
	 private static final Integer MSG_TRANSPORT_END;
	 private static final Integer MSG_TRANSPORT_INFO;
	 private static final java.lang.String TAG;
	 /* # instance fields */
	 private final android.util.SparseBooleanArray mAlwaysRecordBehavior;
	 private com.miui.server.security.AppDurationService mAppBehaviorDuration;
	 private final android.app.AppOpsManager mAppOps;
	 private final android.util.SparseLongArray mBehaviorThreshold;
	 private final android.content.Context mContext;
	 private final java.text.SimpleDateFormat mDateFormat;
	 private final miui.process.IForegroundInfoListener mForegroundInfoListener;
	 private final android.util.ArrayMap mForegroundRecorder;
	 /* .annotation system Ldalvik/annotation/Signature; */
	 /* value = { */
	 /* "Landroid/util/ArrayMap<", */
	 /* "Ljava/lang/String;", */
	 /* "Ljava/lang/Integer;", */
	 /* ">;" */
	 /* } */
} // .end annotation
} // .end field
private final java.util.List mLastEnableAccessibility;
/* .annotation system Ldalvik/annotation/Signature; */
/* value = { */
/* "Ljava/util/List<", */
/* "Ljava/lang/String;", */
/* ">;" */
/* } */
} // .end annotation
} // .end field
private final android.util.ArrayMap mPackageBehavior;
/* .annotation system Ldalvik/annotation/Signature; */
/* value = { */
/* "Landroid/util/ArrayMap<", */
/* "Ljava/lang/String;", */
/* "Lcom/miui/server/security/model/PackageBehaviorBean;", */
/* ">;" */
/* } */
} // .end annotation
} // .end field
protected final android.content.pm.PackageManagerInternal mPackageManagerInt;
private final android.util.ArrayMap mPackageRecordBehavior;
/* .annotation system Ldalvik/annotation/Signature; */
/* value = { */
/* "Landroid/util/ArrayMap<", */
/* "Ljava/lang/String;", */
/* "Ljava/util/Set<", */
/* "Ljava/lang/Integer;", */
/* ">;>;" */
/* } */
} // .end annotation
} // .end field
private android.os.Messenger mPersistMessenger;
private final java.lang.Object mRecordEnableLock;
private Integer mRecordSize;
private Boolean mRegisterAccessibility;
private Boolean mRegisterForeground;
private final android.database.ContentObserver mSettingsObserver;
private final java.lang.Object mUidBehaviorLock;
private final android.os.Handler mWorkHandler;
/* # direct methods */
public static void $r8$lambda$PFkDXwjlFgsN3peI9JXFMLsJK6U ( com.miui.server.security.AppBehaviorService p0, miui.security.AppBehavior p1 ) { //synthethic
/* .locals 0 */
/* invoke-direct {p0, p1}, Lcom/miui/server/security/AppBehaviorService;->lambda$recordAppBehaviorAsync$0(Lmiui/security/AppBehavior;)V */
return;
} // .end method
static android.app.AppOpsManager -$$Nest$fgetmAppOps ( com.miui.server.security.AppBehaviorService p0 ) { //bridge//synthethic
/* .locals 0 */
p0 = this.mAppOps;
} // .end method
static android.util.ArrayMap -$$Nest$fgetmForegroundRecorder ( com.miui.server.security.AppBehaviorService p0 ) { //bridge//synthethic
/* .locals 0 */
p0 = this.mForegroundRecorder;
} // .end method
static android.os.Handler -$$Nest$fgetmWorkHandler ( com.miui.server.security.AppBehaviorService p0 ) { //bridge//synthethic
/* .locals 0 */
p0 = this.mWorkHandler;
} // .end method
static void -$$Nest$fputmPersistMessenger ( com.miui.server.security.AppBehaviorService p0, android.os.Messenger p1 ) { //bridge//synthethic
/* .locals 0 */
this.mPersistMessenger = p1;
return;
} // .end method
static void -$$Nest$mcheckForegroundCount ( com.miui.server.security.AppBehaviorService p0 ) { //bridge//synthethic
/* .locals 0 */
/* invoke-direct {p0}, Lcom/miui/server/security/AppBehaviorService;->checkForegroundCount()V */
return;
} // .end method
static void -$$Nest$mcheckNewEnableAccessibility ( com.miui.server.security.AppBehaviorService p0 ) { //bridge//synthethic
/* .locals 0 */
/* invoke-direct {p0}, Lcom/miui/server/security/AppBehaviorService;->checkNewEnableAccessibility()V */
return;
} // .end method
static android.content.pm.ApplicationInfo -$$Nest$mgetApplicationInfo ( com.miui.server.security.AppBehaviorService p0, java.lang.String p1, Integer p2 ) { //bridge//synthethic
/* .locals 0 */
/* invoke-direct {p0, p1, p2}, Lcom/miui/server/security/AppBehaviorService;->getApplicationInfo(Ljava/lang/String;I)Landroid/content/pm/ApplicationInfo; */
} // .end method
static void -$$Nest$mscanDurationTooLong ( com.miui.server.security.AppBehaviorService p0 ) { //bridge//synthethic
/* .locals 0 */
/* invoke-direct {p0}, Lcom/miui/server/security/AppBehaviorService;->scanDurationTooLong()V */
return;
} // .end method
static void -$$Nest$mschedulePersist ( com.miui.server.security.AppBehaviorService p0 ) { //bridge//synthethic
/* .locals 0 */
/* invoke-direct {p0}, Lcom/miui/server/security/AppBehaviorService;->schedulePersist()V */
return;
} // .end method
static android.net.Uri -$$Nest$sfgetACCESSIBILITY_URI ( ) { //bridge//synthethic
/* .locals 1 */
v0 = com.miui.server.security.AppBehaviorService.ACCESSIBILITY_URI;
} // .end method
static com.miui.server.security.AppBehaviorService ( ) {
/* .locals 1 */
/* .line 67 */
final String v0 = "enabled_accessibility_services"; // const-string v0, "enabled_accessibility_services"
android.provider.Settings$Secure .getUriFor ( v0 );
return;
} // .end method
public com.miui.server.security.AppBehaviorService ( ) {
/* .locals 4 */
/* .param p1, "context" # Landroid/content/Context; */
/* .param p2, "thread" # Landroid/os/HandlerThread; */
/* .line 115 */
/* invoke-direct {p0}, Ljava/lang/Object;-><init>()V */
/* .line 78 */
/* new-instance v0, Ljava/lang/Object; */
/* invoke-direct {v0}, Ljava/lang/Object;-><init>()V */
this.mUidBehaviorLock = v0;
/* .line 79 */
/* new-instance v0, Ljava/lang/Object; */
/* invoke-direct {v0}, Ljava/lang/Object;-><init>()V */
this.mRecordEnableLock = v0;
/* .line 82 */
/* new-instance v0, Ljava/text/SimpleDateFormat; */
/* const-string/jumbo v1, "yyyy-MM-dd HH:mm:ss" */
/* invoke-direct {v0, v1}, Ljava/text/SimpleDateFormat;-><init>(Ljava/lang/String;)V */
this.mDateFormat = v0;
/* .line 88 */
int v0 = 0; // const/4 v0, 0x0
/* iput-boolean v0, p0, Lcom/miui/server/security/AppBehaviorService;->mRegisterForeground:Z */
/* .line 89 */
/* iput-boolean v0, p0, Lcom/miui/server/security/AppBehaviorService;->mRegisterAccessibility:Z */
/* .line 93 */
/* new-instance v0, Lcom/miui/server/security/AppBehaviorService$1; */
/* invoke-direct {v0, p0}, Lcom/miui/server/security/AppBehaviorService$1;-><init>(Lcom/miui/server/security/AppBehaviorService;)V */
this.mForegroundInfoListener = v0;
/* .line 116 */
this.mContext = p1;
/* .line 117 */
final String v0 = "appops"; // const-string v0, "appops"
(( android.content.Context ) p1 ).getSystemService ( v0 ); // invoke-virtual {p1, v0}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;
/* check-cast v0, Landroid/app/AppOpsManager; */
this.mAppOps = v0;
/* .line 118 */
/* const-class v0, Landroid/content/pm/PackageManagerInternal; */
com.android.server.LocalServices .getService ( v0 );
/* check-cast v0, Landroid/content/pm/PackageManagerInternal; */
this.mPackageManagerInt = v0;
/* .line 119 */
/* new-instance v0, Landroid/util/ArrayMap; */
/* invoke-direct {v0}, Landroid/util/ArrayMap;-><init>()V */
this.mForegroundRecorder = v0;
/* .line 120 */
/* new-instance v0, Landroid/util/ArrayMap; */
/* invoke-direct {v0}, Landroid/util/ArrayMap;-><init>()V */
this.mPackageBehavior = v0;
/* .line 121 */
/* new-instance v0, Landroid/util/SparseLongArray; */
/* invoke-direct {v0}, Landroid/util/SparseLongArray;-><init>()V */
this.mBehaviorThreshold = v0;
/* .line 122 */
/* new-instance v1, Ljava/util/ArrayList; */
/* invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V */
this.mLastEnableAccessibility = v1;
/* .line 123 */
/* new-instance v1, Landroid/util/SparseBooleanArray; */
/* invoke-direct {v1}, Landroid/util/SparseBooleanArray;-><init>()V */
this.mAlwaysRecordBehavior = v1;
/* .line 124 */
/* new-instance v1, Landroid/util/ArrayMap; */
/* invoke-direct {v1}, Landroid/util/ArrayMap;-><init>()V */
this.mPackageRecordBehavior = v1;
/* .line 125 */
/* new-instance v1, Lcom/miui/server/security/AppBehaviorService$2; */
(( android.os.HandlerThread ) p2 ).getLooper ( ); // invoke-virtual {p2}, Landroid/os/HandlerThread;->getLooper()Landroid/os/Looper;
/* invoke-direct {v1, p0, v2}, Lcom/miui/server/security/AppBehaviorService$2;-><init>(Lcom/miui/server/security/AppBehaviorService;Landroid/os/Looper;)V */
this.mWorkHandler = v1;
/* .line 137 */
/* new-instance v2, Lcom/miui/server/security/AppBehaviorService$3; */
/* invoke-direct {v2, p0, v1}, Lcom/miui/server/security/AppBehaviorService$3;-><init>(Lcom/miui/server/security/AppBehaviorService;Landroid/os/Handler;)V */
this.mSettingsObserver = v2;
/* .line 146 */
/* const/16 v1, 0x23 */
/* const-wide/16 v2, 0x14 */
(( android.util.SparseLongArray ) v0 ).put ( v1, v2, v3 ); // invoke-virtual {v0, v1, v2, v3}, Landroid/util/SparseLongArray;->put(IJ)V
/* .line 147 */
/* const/16 v1, 0x25 */
/* const-wide/16 v2, 0xa */
(( android.util.SparseLongArray ) v0 ).put ( v1, v2, v3 ); // invoke-virtual {v0, v1, v2, v3}, Landroid/util/SparseLongArray;->put(IJ)V
/* .line 148 */
return;
} // .end method
private void checkForegroundCount ( ) {
/* .locals 7 */
/* .line 503 */
final String v0 = "AppBehaviorService"; // const-string v0, "AppBehaviorService"
/* const-string/jumbo v1, "start checkForegroundPackageRecord with threshold" */
android.util.Slog .i ( v0,v1 );
/* .line 504 */
v0 = this.mForegroundRecorder;
/* monitor-enter v0 */
/* .line 505 */
int v1 = 0; // const/4 v1, 0x0
/* .local v1, "i":I */
} // :goto_0
try { // :try_start_0
v2 = this.mForegroundRecorder;
v2 = (( android.util.ArrayMap ) v2 ).size ( ); // invoke-virtual {v2}, Landroid/util/ArrayMap;->size()I
/* if-ge v1, v2, :cond_1 */
/* .line 506 */
v2 = this.mForegroundRecorder;
(( android.util.ArrayMap ) v2 ).valueAt ( v1 ); // invoke-virtual {v2, v1}, Landroid/util/ArrayMap;->valueAt(I)Ljava/lang/Object;
/* check-cast v2, Ljava/lang/Integer; */
v2 = (( java.lang.Integer ) v2 ).intValue ( ); // invoke-virtual {v2}, Ljava/lang/Integer;->intValue()I
/* int-to-long v2, v2 */
/* const/16 v4, 0x23 */
v2 = (( com.miui.server.security.AppBehaviorService ) p0 ).moreThanThreshold ( v4, v2, v3 ); // invoke-virtual {p0, v4, v2, v3}, Lcom/miui/server/security/AppBehaviorService;->moreThanThreshold(IJ)Z
if ( v2 != null) { // if-eqz v2, :cond_0
/* .line 507 */
v2 = this.mForegroundRecorder;
/* .line 508 */
(( android.util.ArrayMap ) v2 ).keyAt ( v1 ); // invoke-virtual {v2, v1}, Landroid/util/ArrayMap;->keyAt(I)Ljava/lang/Object;
/* check-cast v2, Ljava/lang/String; */
/* .line 507 */
/* const-wide/16 v5, 0x1 */
int v3 = 0; // const/4 v3, 0x0
miui.security.AppBehavior .buildCountEvent ( v4,v2,v5,v6,v3 );
(( com.miui.server.security.AppBehaviorService ) p0 ).recordAppBehaviorAsync ( v2 ); // invoke-virtual {p0, v2}, Lcom/miui/server/security/AppBehaviorService;->recordAppBehaviorAsync(Lmiui/security/AppBehavior;)V
/* .line 505 */
} // :cond_0
/* add-int/lit8 v1, v1, 0x1 */
/* .line 511 */
} // .end local v1 # "i":I
} // :cond_1
v1 = this.mForegroundRecorder;
(( android.util.ArrayMap ) v1 ).clear ( ); // invoke-virtual {v1}, Landroid/util/ArrayMap;->clear()V
/* .line 512 */
/* monitor-exit v0 */
/* :try_end_0 */
/* .catchall {:try_start_0 ..:try_end_0} :catchall_0 */
/* .line 513 */
v0 = this.mWorkHandler;
/* const/16 v1, 0xe10 */
/* const-wide/32 v2, 0x36ee80 */
(( android.os.Handler ) v0 ).sendEmptyMessageDelayed ( v1, v2, v3 ); // invoke-virtual {v0, v1, v2, v3}, Landroid/os/Handler;->sendEmptyMessageDelayed(IJ)Z
/* .line 514 */
return;
/* .line 512 */
/* :catchall_0 */
/* move-exception v1 */
try { // :try_start_1
/* monitor-exit v0 */
/* :try_end_1 */
/* .catchall {:try_start_1 ..:try_end_1} :catchall_0 */
/* throw v1 */
} // .end method
private void checkNewEnableAccessibility ( ) {
/* .locals 12 */
/* .line 529 */
v0 = this.mContext;
(( android.content.Context ) v0 ).getContentResolver ( ); // invoke-virtual {v0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;
final String v1 = "enabled_accessibility_services"; // const-string v1, "enabled_accessibility_services"
android.provider.Settings$Secure .getString ( v0,v1 );
/* .line 530 */
/* .local v0, "components":Ljava/lang/String; */
v1 = android.text.TextUtils .isEmpty ( v0 );
/* if-nez v1, :cond_3 */
/* .line 531 */
final String v1 = ":"; // const-string v1, ":"
(( java.lang.String ) v0 ).split ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;
/* .line 532 */
/* .local v1, "componentsArray":[Ljava/lang/String; */
/* array-length v2, v1 */
int v3 = 0; // const/4 v3, 0x0
} // :goto_0
/* if-ge v3, v2, :cond_2 */
/* aget-object v4, v1, v3 */
/* .line 533 */
/* .local v4, "component":Ljava/lang/String; */
v5 = v5 = this.mLastEnableAccessibility;
/* .line 537 */
/* .local v5, "alreadyHave":Z */
if ( v5 != null) { // if-eqz v5, :cond_0
/* .line 538 */
/* .line 540 */
} // :cond_0
android.content.ComponentName .unflattenFromString ( v4 );
/* .line 541 */
/* .local v6, "service":Landroid/content/ComponentName; */
if ( v6 != null) { // if-eqz v6, :cond_1
/* .line 542 */
/* nop */
/* .line 544 */
(( android.content.ComponentName ) v6 ).getPackageName ( ); // invoke-virtual {v6}, Landroid/content/ComponentName;->getPackageName()Ljava/lang/String;
/* .line 545 */
(( android.content.ComponentName ) v6 ).flattenToShortString ( ); // invoke-virtual {v6}, Landroid/content/ComponentName;->flattenToShortString()Ljava/lang/String;
/* .line 542 */
/* const/16 v9, 0x1d */
/* const-wide/16 v10, 0x1 */
miui.security.AppBehavior .buildCountEvent ( v9,v7,v10,v11,v8 );
(( com.miui.server.security.AppBehaviorService ) p0 ).recordAppBehaviorAsync ( v7 ); // invoke-virtual {p0, v7}, Lcom/miui/server/security/AppBehaviorService;->recordAppBehaviorAsync(Lmiui/security/AppBehavior;)V
/* .line 532 */
} // .end local v4 # "component":Ljava/lang/String;
} // .end local v5 # "alreadyHave":Z
} // .end local v6 # "service":Landroid/content/ComponentName;
} // :cond_1
} // :goto_1
/* add-int/lit8 v3, v3, 0x1 */
/* .line 548 */
} // :cond_2
v2 = this.mLastEnableAccessibility;
/* .line 549 */
v2 = this.mLastEnableAccessibility;
java.util.Arrays .asList ( v1 );
/* .line 550 */
} // .end local v1 # "componentsArray":[Ljava/lang/String;
/* .line 551 */
} // :cond_3
v1 = this.mLastEnableAccessibility;
/* .line 553 */
} // :goto_2
return;
} // .end method
private android.content.pm.ApplicationInfo getApplicationInfo ( java.lang.String p0, Integer p1 ) {
/* .locals 8 */
/* .param p1, "pkgName" # Ljava/lang/String; */
/* .param p2, "userId" # I */
/* .line 454 */
android.os.Binder .clearCallingIdentity ( );
/* move-result-wide v0 */
/* .line 456 */
/* .local v0, "callingIdentity":J */
try { // :try_start_0
v2 = this.mPackageManagerInt;
/* const-wide/16 v4, 0x0 */
/* const/16 v6, 0x3e8 */
/* move-object v3, p1 */
/* move v7, p2 */
/* invoke-virtual/range {v2 ..v7}, Landroid/content/pm/PackageManagerInternal;->getApplicationInfo(Ljava/lang/String;JII)Landroid/content/pm/ApplicationInfo; */
/* :try_end_0 */
/* .catchall {:try_start_0 ..:try_end_0} :catchall_0 */
/* .line 458 */
/* .local v2, "info":Landroid/content/pm/ApplicationInfo; */
android.os.Binder .restoreCallingIdentity ( v0,v1 );
/* .line 459 */
/* nop */
/* .line 460 */
/* .line 458 */
} // .end local v2 # "info":Landroid/content/pm/ApplicationInfo;
/* :catchall_0 */
/* move-exception v2 */
android.os.Binder .restoreCallingIdentity ( v0,v1 );
/* .line 459 */
/* throw v2 */
} // .end method
public static Boolean isSystem ( android.content.pm.ApplicationInfo p0 ) {
/* .locals 2 */
/* .param p0, "applicationInfo" # Landroid/content/pm/ApplicationInfo; */
/* .line 464 */
if ( p0 != null) { // if-eqz p0, :cond_1
v0 = (( android.content.pm.ApplicationInfo ) p0 ).isSystemApp ( ); // invoke-virtual {p0}, Landroid/content/pm/ApplicationInfo;->isSystemApp()Z
/* if-nez v0, :cond_0 */
/* iget v0, p0, Landroid/content/pm/ApplicationInfo;->uid:I */
/* .line 465 */
v0 = android.os.UserHandle .getAppId ( v0 );
/* const/16 v1, 0x2710 */
/* if-ge v0, v1, :cond_1 */
} // :cond_0
int v0 = 1; // const/4 v0, 0x1
} // :cond_1
int v0 = 0; // const/4 v0, 0x0
/* .line 464 */
} // :goto_0
} // .end method
private void lambda$recordAppBehaviorAsync$0 ( miui.security.AppBehavior p0 ) { //synthethic
/* .locals 0 */
/* .param p1, "appBehavior" # Lmiui/security/AppBehavior; */
/* .line 347 */
(( com.miui.server.security.AppBehaviorService ) p0 ).recordAppBehavior ( p1 ); // invoke-virtual {p0, p1}, Lcom/miui/server/security/AppBehaviorService;->recordAppBehavior(Lmiui/security/AppBehavior;)V
return;
} // .end method
private void registerAccessibilityListener ( Boolean p0 ) {
/* .locals 4 */
/* .param p1, "register" # Z */
/* .line 517 */
int v0 = 0; // const/4 v0, 0x0
if ( p1 != null) { // if-eqz p1, :cond_0
/* iget-boolean v1, p0, Lcom/miui/server/security/AppBehaviorService;->mRegisterAccessibility:Z */
/* if-nez v1, :cond_0 */
/* .line 518 */
int v1 = 1; // const/4 v1, 0x1
/* iput-boolean v1, p0, Lcom/miui/server/security/AppBehaviorService;->mRegisterAccessibility:Z */
/* .line 519 */
v1 = this.mContext;
(( android.content.Context ) v1 ).getContentResolver ( ); // invoke-virtual {v1}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;
v2 = com.miui.server.security.AppBehaviorService.ACCESSIBILITY_URI;
v3 = this.mSettingsObserver;
(( android.content.ContentResolver ) v1 ).registerContentObserver ( v2, v0, v3 ); // invoke-virtual {v1, v2, v0, v3}, Landroid/content/ContentResolver;->registerContentObserver(Landroid/net/Uri;ZLandroid/database/ContentObserver;)V
/* .line 520 */
/* invoke-direct {p0}, Lcom/miui/server/security/AppBehaviorService;->checkNewEnableAccessibility()V */
/* .line 521 */
} // :cond_0
/* iget-boolean v1, p0, Lcom/miui/server/security/AppBehaviorService;->mRegisterAccessibility:Z */
if ( v1 != null) { // if-eqz v1, :cond_1
/* if-nez p1, :cond_1 */
/* .line 522 */
v1 = this.mContext;
(( android.content.Context ) v1 ).getContentResolver ( ); // invoke-virtual {v1}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;
v2 = this.mSettingsObserver;
(( android.content.ContentResolver ) v1 ).unregisterContentObserver ( v2 ); // invoke-virtual {v1, v2}, Landroid/content/ContentResolver;->unregisterContentObserver(Landroid/database/ContentObserver;)V
/* .line 523 */
v1 = this.mLastEnableAccessibility;
/* .line 524 */
/* iput-boolean v0, p0, Lcom/miui/server/security/AppBehaviorService;->mRegisterAccessibility:Z */
/* .line 526 */
} // :cond_1
} // :goto_0
return;
} // .end method
private void registerForegroundRecordTask ( Boolean p0 ) {
/* .locals 4 */
/* .param p1, "register" # Z */
/* .line 486 */
/* const/16 v0, 0xe10 */
if ( p1 != null) { // if-eqz p1, :cond_0
/* .line 487 */
/* iget-boolean v1, p0, Lcom/miui/server/security/AppBehaviorService;->mRegisterForeground:Z */
/* if-nez v1, :cond_1 */
/* .line 488 */
v1 = this.mWorkHandler;
/* const-wide/32 v2, 0x36ee80 */
(( android.os.Handler ) v1 ).sendEmptyMessageDelayed ( v0, v2, v3 ); // invoke-virtual {v1, v0, v2, v3}, Landroid/os/Handler;->sendEmptyMessageDelayed(IJ)Z
/* .line 489 */
v0 = this.mForegroundInfoListener;
miui.process.ProcessManager .registerForegroundInfoListener ( v0 );
/* .line 490 */
int v0 = 1; // const/4 v0, 0x1
/* iput-boolean v0, p0, Lcom/miui/server/security/AppBehaviorService;->mRegisterForeground:Z */
/* .line 493 */
} // :cond_0
/* iget-boolean v1, p0, Lcom/miui/server/security/AppBehaviorService;->mRegisterForeground:Z */
if ( v1 != null) { // if-eqz v1, :cond_1
/* .line 494 */
v1 = this.mForegroundRecorder;
(( android.util.ArrayMap ) v1 ).clear ( ); // invoke-virtual {v1}, Landroid/util/ArrayMap;->clear()V
/* .line 495 */
v1 = this.mWorkHandler;
(( android.os.Handler ) v1 ).removeMessages ( v0 ); // invoke-virtual {v1, v0}, Landroid/os/Handler;->removeMessages(I)V
/* .line 496 */
v0 = this.mForegroundInfoListener;
miui.process.ProcessManager .unregisterForegroundInfoListener ( v0 );
/* .line 497 */
int v0 = 0; // const/4 v0, 0x0
/* iput-boolean v0, p0, Lcom/miui/server/security/AppBehaviorService;->mRegisterForeground:Z */
/* .line 500 */
} // :cond_1
} // :goto_0
return;
} // .end method
private void registerListenerForBehavior ( Integer p0, Boolean p1 ) {
/* .locals 1 */
/* .param p1, "behavior" # I */
/* .param p2, "register" # Z */
/* .line 473 */
/* const/16 v0, 0x23 */
/* if-ne v0, p1, :cond_0 */
/* .line 474 */
/* invoke-direct {p0, p2}, Lcom/miui/server/security/AppBehaviorService;->registerForegroundRecordTask(Z)V */
/* .line 475 */
} // :cond_0
/* const/16 v0, 0x1d */
/* if-ne v0, p1, :cond_1 */
/* .line 476 */
/* invoke-direct {p0, p2}, Lcom/miui/server/security/AppBehaviorService;->registerAccessibilityListener(Z)V */
/* .line 477 */
} // :cond_1
/* const/16 v0, 0x20 */
/* if-eq v0, p1, :cond_2 */
/* const/16 v0, 0x16 */
/* if-eq v0, p1, :cond_2 */
/* const/16 v0, 0x17 */
/* if-eq v0, p1, :cond_2 */
/* const/16 v0, 0x19 */
/* if-ne v0, p1, :cond_3 */
/* .line 481 */
} // :cond_2
/* invoke-direct {p0, p2}, Lcom/miui/server/security/AppBehaviorService;->registerScanDurationTooLong(Z)V */
/* .line 483 */
} // :cond_3
} // :goto_0
return;
} // .end method
private void registerScanDurationTooLong ( Boolean p0 ) {
/* .locals 4 */
/* .param p1, "register" # Z */
/* .line 556 */
/* const/16 v0, 0xe12 */
if ( p1 != null) { // if-eqz p1, :cond_0
v1 = this.mWorkHandler;
v1 = (( android.os.Handler ) v1 ).hasMessages ( v0 ); // invoke-virtual {v1, v0}, Landroid/os/Handler;->hasMessages(I)Z
/* if-nez v1, :cond_0 */
/* .line 557 */
v1 = this.mWorkHandler;
/* const-wide/32 v2, 0x2932e00 */
(( android.os.Handler ) v1 ).sendEmptyMessageDelayed ( v0, v2, v3 ); // invoke-virtual {v1, v0, v2, v3}, Landroid/os/Handler;->sendEmptyMessageDelayed(IJ)Z
/* .line 558 */
} // :cond_0
/* if-nez p1, :cond_1 */
/* .line 559 */
v1 = this.mWorkHandler;
(( android.os.Handler ) v1 ).removeMessages ( v0 ); // invoke-virtual {v1, v0}, Landroid/os/Handler;->removeMessages(I)V
/* .line 561 */
} // :cond_1
} // :goto_0
return;
} // .end method
private void scanDurationTooLong ( ) {
/* .locals 4 */
/* .line 564 */
v0 = this.mWorkHandler;
/* const/16 v1, 0xe12 */
(( android.os.Handler ) v0 ).removeMessages ( v1 ); // invoke-virtual {v0, v1}, Landroid/os/Handler;->removeMessages(I)V
/* .line 565 */
v0 = this.mWorkHandler;
/* const-wide/32 v2, 0x2932e00 */
(( android.os.Handler ) v0 ).sendEmptyMessageDelayed ( v1, v2, v3 ); // invoke-virtual {v0, v1, v2, v3}, Landroid/os/Handler;->sendEmptyMessageDelayed(IJ)Z
/* .line 566 */
v0 = this.mAppBehaviorDuration;
(( com.miui.server.security.AppDurationService ) v0 ).checkIfTooLongDuration ( ); // invoke-virtual {v0}, Lcom/miui/server/security/AppDurationService;->checkIfTooLongDuration()V
/* .line 567 */
return;
} // .end method
private void schedulePersist ( ) {
/* .locals 11 */
/* .line 297 */
final String v0 = "AppBehaviorService"; // const-string v0, "AppBehaviorService"
/* new-instance v1, Ljava/lang/StringBuilder; */
/* invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V */
/* const-string/jumbo v2, "start schedulePersist, size may be:" */
(( java.lang.StringBuilder ) v1 ).append ( v2 ); // invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
/* iget v2, p0, Lcom/miui/server/security/AppBehaviorService;->mRecordSize:I */
(( java.lang.StringBuilder ) v1 ).append ( v2 ); // invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v1 ).toString ( ); // invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
android.util.Slog .i ( v0,v1 );
/* .line 298 */
v0 = this.mPersistMessenger;
if ( v0 != null) { // if-eqz v0, :cond_3
/* .line 299 */
v0 = this.mUidBehaviorLock;
/* monitor-enter v0 */
/* .line 300 */
try { // :try_start_0
v1 = this.mDateFormat;
/* new-instance v2, Ljava/util/Date; */
/* invoke-direct {v2}, Ljava/util/Date;-><init>()V */
(( java.text.SimpleDateFormat ) v1 ).format ( v2 ); // invoke-virtual {v1, v2}, Ljava/text/SimpleDateFormat;->format(Ljava/util/Date;)Ljava/lang/String;
/* .line 301 */
/* .local v1, "fileName":Ljava/lang/String; */
int v2 = 0; // const/4 v2, 0x0
/* .local v2, "i":I */
} // :goto_0
v3 = this.mPackageBehavior;
v3 = (( android.util.ArrayMap ) v3 ).size ( ); // invoke-virtual {v3}, Landroid/util/ArrayMap;->size()I
/* if-ge v2, v3, :cond_2 */
/* .line 302 */
v3 = this.mPackageBehavior;
(( android.util.ArrayMap ) v3 ).keyAt ( v2 ); // invoke-virtual {v3, v2}, Landroid/util/ArrayMap;->keyAt(I)Ljava/lang/Object;
/* check-cast v3, Ljava/lang/String; */
/* .line 303 */
/* .local v3, "pkgName":Ljava/lang/String; */
v4 = this.mPackageBehavior;
(( android.util.ArrayMap ) v4 ).get ( v3 ); // invoke-virtual {v4, v3}, Landroid/util/ArrayMap;->get(Ljava/lang/Object;)Ljava/lang/Object;
/* check-cast v4, Lcom/miui/server/security/model/PackageBehaviorBean; */
/* :try_end_0 */
/* .catchall {:try_start_0 ..:try_end_0} :catchall_0 */
/* .line 305 */
/* .local v4, "packageBehaviorBean":Lcom/miui/server/security/model/PackageBehaviorBean; */
if ( v4 != null) { // if-eqz v4, :cond_0
/* .line 306 */
try { // :try_start_1
(( com.miui.server.security.model.PackageBehaviorBean ) v4 ).toJson ( ); // invoke-virtual {v4}, Lcom/miui/server/security/model/PackageBehaviorBean;->toJson()Lorg/json/JSONObject;
/* .line 307 */
/* .local v5, "record":Lorg/json/JSONObject; */
/* iget v6, p0, Lcom/miui/server/security/AppBehaviorService;->mRecordSize:I */
v7 = (( com.miui.server.security.model.PackageBehaviorBean ) v4 ).init ( ); // invoke-virtual {v4}, Lcom/miui/server/security/model/PackageBehaviorBean;->init()I
/* sub-int/2addr v6, v7 */
/* iput v6, p0, Lcom/miui/server/security/AppBehaviorService;->mRecordSize:I */
/* .line 308 */
int v7 = 0; // const/4 v7, 0x0
v6 = java.lang.Math .max ( v6,v7 );
/* iput v6, p0, Lcom/miui/server/security/AppBehaviorService;->mRecordSize:I */
/* .line 312 */
if ( v5 != null) { // if-eqz v5, :cond_0
v6 = (( org.json.JSONObject ) v5 ).length ( ); // invoke-virtual {v5}, Lorg/json/JSONObject;->length()I
/* if-lez v6, :cond_0 */
/* .line 314 */
/* new-instance v6, Lorg/json/JSONObject; */
/* invoke-direct {v6}, Lorg/json/JSONObject;-><init>()V */
/* .line 315 */
/* .local v6, "packageRecord":Lorg/json/JSONObject; */
(( org.json.JSONObject ) v6 ).put ( v3, v5 ); // invoke-virtual {v6, v3, v5}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;
/* .line 316 */
android.os.Message .obtain ( );
/* .line 317 */
/* .local v7, "message":Landroid/os/Message; */
/* const/16 v8, 0xe13 */
/* iput v8, v7, Landroid/os/Message;->what:I */
/* .line 318 */
/* new-instance v8, Landroid/os/Bundle; */
/* invoke-direct {v8}, Landroid/os/Bundle;-><init>()V */
/* .line 319 */
/* .local v8, "bundle":Landroid/os/Bundle; */
final String v9 = "android.intent.extra.TITLE"; // const-string v9, "android.intent.extra.TITLE"
(( android.os.Bundle ) v8 ).putString ( v9, v1 ); // invoke-virtual {v8, v9, v1}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V
/* .line 320 */
final String v9 = "android.intent.extra.RETURN_RESULT"; // const-string v9, "android.intent.extra.RETURN_RESULT"
(( org.json.JSONObject ) v6 ).toString ( ); // invoke-virtual {v6}, Lorg/json/JSONObject;->toString()Ljava/lang/String;
(( android.os.Bundle ) v8 ).putString ( v9, v10 ); // invoke-virtual {v8, v9, v10}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V
/* .line 321 */
(( android.os.Message ) v7 ).setData ( v8 ); // invoke-virtual {v7, v8}, Landroid/os/Message;->setData(Landroid/os/Bundle;)V
/* .line 322 */
v9 = this.mPersistMessenger;
(( android.os.Messenger ) v9 ).send ( v7 ); // invoke-virtual {v9, v7}, Landroid/os/Messenger;->send(Landroid/os/Message;)V
/* :try_end_1 */
/* .catch Ljava/lang/Exception; {:try_start_1 ..:try_end_1} :catch_0 */
/* .catchall {:try_start_1 ..:try_end_1} :catchall_0 */
/* .line 325 */
} // .end local v5 # "record":Lorg/json/JSONObject;
} // .end local v6 # "packageRecord":Lorg/json/JSONObject;
} // .end local v7 # "message":Landroid/os/Message;
} // .end local v8 # "bundle":Landroid/os/Bundle;
/* :catch_0 */
/* move-exception v5 */
/* .line 326 */
/* .local v5, "e":Ljava/lang/Exception; */
try { // :try_start_2
final String v6 = "AppBehaviorService"; // const-string v6, "AppBehaviorService"
final String v7 = "persist exception!"; // const-string v7, "persist exception!"
android.util.Slog .e ( v6,v7,v5 );
/* .line 327 */
v6 = this.mPersistMessenger;
/* if-nez v6, :cond_1 */
/* .line 328 */
(( com.miui.server.security.AppBehaviorService ) p0 ).persistBehaviorRecord ( ); // invoke-virtual {p0}, Lcom/miui/server/security/AppBehaviorService;->persistBehaviorRecord()V
/* .line 329 */
/* monitor-exit v0 */
return;
/* .line 331 */
} // .end local v5 # "e":Ljava/lang/Exception;
} // :cond_0
} // :goto_1
/* nop */
/* .line 301 */
} // .end local v3 # "pkgName":Ljava/lang/String;
} // .end local v4 # "packageBehaviorBean":Lcom/miui/server/security/model/PackageBehaviorBean;
} // :cond_1
/* add-int/lit8 v2, v2, 0x1 */
/* .line 333 */
} // .end local v2 # "i":I
} // :cond_2
android.os.Message .obtain ( );
/* .line 334 */
/* .local v2, "message":Landroid/os/Message; */
/* const/16 v3, 0xe14 */
/* iput v3, v2, Landroid/os/Message;->what:I */
/* :try_end_2 */
/* .catchall {:try_start_2 ..:try_end_2} :catchall_0 */
/* .line 336 */
try { // :try_start_3
v3 = this.mPersistMessenger;
(( android.os.Messenger ) v3 ).send ( v2 ); // invoke-virtual {v3, v2}, Landroid/os/Messenger;->send(Landroid/os/Message;)V
/* :try_end_3 */
/* .catch Ljava/lang/Exception; {:try_start_3 ..:try_end_3} :catch_1 */
/* .catchall {:try_start_3 ..:try_end_3} :catchall_0 */
/* .line 338 */
/* .line 337 */
/* :catch_1 */
/* move-exception v3 */
/* .line 339 */
} // .end local v1 # "fileName":Ljava/lang/String;
} // .end local v2 # "message":Landroid/os/Message;
} // :goto_2
try { // :try_start_4
/* monitor-exit v0 */
/* :catchall_0 */
/* move-exception v1 */
/* monitor-exit v0 */
/* :try_end_4 */
/* .catchall {:try_start_4 ..:try_end_4} :catchall_0 */
/* throw v1 */
/* .line 341 */
} // :cond_3
} // :goto_3
return;
} // .end method
private void scheduleRecord ( Integer p0, java.lang.String p1, java.lang.String p2, Long p3 ) {
/* .locals 5 */
/* .param p1, "behavior" # I */
/* .param p2, "willBeRecordPackageName" # Ljava/lang/String; */
/* .param p3, "willBeRecordData" # Ljava/lang/String; */
/* .param p4, "willBeRecordCount" # J */
/* .line 399 */
v0 = this.mUidBehaviorLock;
/* monitor-enter v0 */
/* .line 400 */
try { // :try_start_0
v1 = this.mPackageBehavior;
(( android.util.ArrayMap ) v1 ).get ( p2 ); // invoke-virtual {v1, p2}, Landroid/util/ArrayMap;->get(Ljava/lang/Object;)Ljava/lang/Object;
/* check-cast v1, Lcom/miui/server/security/model/PackageBehaviorBean; */
/* .line 401 */
/* .local v1, "packageBehaviorBean":Lcom/miui/server/security/model/PackageBehaviorBean; */
/* if-nez v1, :cond_0 */
/* .line 402 */
/* new-instance v2, Lcom/miui/server/security/model/PackageBehaviorBean; */
/* invoke-direct {v2}, Lcom/miui/server/security/model/PackageBehaviorBean;-><init>()V */
/* move-object v1, v2 */
/* .line 403 */
v2 = this.mPackageBehavior;
(( android.util.ArrayMap ) v2 ).put ( p2, v1 ); // invoke-virtual {v2, p2, v1}, Landroid/util/ArrayMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;
/* .line 405 */
} // :cond_0
v2 = (( com.miui.server.security.model.PackageBehaviorBean ) v1 ).recordAppBehavior ( p1, p3, p4, p5 ); // invoke-virtual {v1, p1, p3, p4, p5}, Lcom/miui/server/security/model/PackageBehaviorBean;->recordAppBehavior(ILjava/lang/String;J)Z
/* .line 406 */
/* .local v2, "addCount":Z */
if ( v2 != null) { // if-eqz v2, :cond_1
/* .line 407 */
/* iget v3, p0, Lcom/miui/server/security/AppBehaviorService;->mRecordSize:I */
/* add-int/lit8 v3, v3, 0x1 */
/* iput v3, p0, Lcom/miui/server/security/AppBehaviorService;->mRecordSize:I */
/* .line 417 */
} // :cond_1
/* iget v3, p0, Lcom/miui/server/security/AppBehaviorService;->mRecordSize:I */
/* const/16 v4, 0xc8 */
/* if-lt v3, v4, :cond_2 */
/* .line 418 */
(( com.miui.server.security.AppBehaviorService ) p0 ).persistBehaviorRecord ( ); // invoke-virtual {p0}, Lcom/miui/server/security/AppBehaviorService;->persistBehaviorRecord()V
/* .line 420 */
} // .end local v1 # "packageBehaviorBean":Lcom/miui/server/security/model/PackageBehaviorBean;
} // .end local v2 # "addCount":Z
} // :cond_2
/* monitor-exit v0 */
/* .line 421 */
return;
/* .line 420 */
/* :catchall_0 */
/* move-exception v1 */
/* monitor-exit v0 */
/* :try_end_0 */
/* .catchall {:try_start_0 ..:try_end_0} :catchall_0 */
/* throw v1 */
} // .end method
/* # virtual methods */
protected Boolean canRecordBehavior ( Integer p0, java.lang.String p1 ) {
/* .locals 5 */
/* .param p1, "behavior" # I */
/* .param p2, "pkgName" # Ljava/lang/String; */
/* .line 248 */
v0 = this.mRecordEnableLock;
/* monitor-enter v0 */
/* .line 249 */
try { // :try_start_0
v1 = this.mAlwaysRecordBehavior;
v1 = (( android.util.SparseBooleanArray ) v1 ).indexOfKey ( p1 ); // invoke-virtual {v1, p1}, Landroid/util/SparseBooleanArray;->indexOfKey(I)I
int v2 = 0; // const/4 v2, 0x0
/* if-ltz v1, :cond_2 */
/* .line 250 */
v1 = this.mAlwaysRecordBehavior;
v1 = (( android.util.SparseBooleanArray ) v1 ).get ( p1 ); // invoke-virtual {v1, p1}, Landroid/util/SparseBooleanArray;->get(I)Z
int v3 = 1; // const/4 v3, 0x1
if ( v1 != null) { // if-eqz v1, :cond_0
/* .line 251 */
/* monitor-exit v0 */
/* .line 253 */
} // :cond_0
/* invoke-direct {p0, p2, v2}, Lcom/miui/server/security/AppBehaviorService;->getApplicationInfo(Ljava/lang/String;I)Landroid/content/pm/ApplicationInfo; */
v1 = com.miui.server.security.AppBehaviorService .isSystem ( v1 );
/* if-nez v1, :cond_1 */
/* move v2, v3 */
} // :cond_1
/* monitor-exit v0 */
/* .line 255 */
} // :cond_2
/* invoke-direct {p0, p2, v2}, Lcom/miui/server/security/AppBehaviorService;->getApplicationInfo(Ljava/lang/String;I)Landroid/content/pm/ApplicationInfo; */
/* .line 256 */
/* .local v1, "applicationInfo":Landroid/content/pm/ApplicationInfo; */
/* if-nez v1, :cond_3 */
/* .line 257 */
/* monitor-exit v0 */
/* .line 259 */
} // :cond_3
v3 = this.mPackageRecordBehavior;
(( android.util.ArrayMap ) v3 ).get ( p2 ); // invoke-virtual {v3, p2}, Landroid/util/ArrayMap;->get(Ljava/lang/Object;)Ljava/lang/Object;
/* check-cast v3, Ljava/util/Set; */
/* .line 260 */
/* .local v3, "behaviorWatching":Ljava/util/Set;, "Ljava/util/Set<Ljava/lang/Integer;>;" */
v4 = if ( v3 != null) { // if-eqz v3, :cond_5
if ( v4 != null) { // if-eqz v4, :cond_4
/* .line 263 */
} // :cond_4
v2 = java.lang.Integer .valueOf ( p1 );
/* monitor-exit v0 */
/* .line 261 */
} // :cond_5
} // :goto_0
/* monitor-exit v0 */
/* .line 264 */
} // .end local v1 # "applicationInfo":Landroid/content/pm/ApplicationInfo;
} // .end local v3 # "behaviorWatching":Ljava/util/Set;, "Ljava/util/Set<Ljava/lang/Integer;>;"
/* :catchall_0 */
/* move-exception v1 */
/* monitor-exit v0 */
/* :try_end_0 */
/* .catchall {:try_start_0 ..:try_end_0} :catchall_0 */
/* throw v1 */
} // .end method
public void dump ( java.io.PrintWriter p0 ) {
/* .locals 7 */
/* .param p1, "pw" # Ljava/io/PrintWriter; */
/* .line 424 */
final String v0 = "==================Dump AppBehaviorService start=================="; // const-string v0, "==================Dump AppBehaviorService start=================="
(( java.io.PrintWriter ) p1 ).println ( v0 ); // invoke-virtual {p1, v0}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V
/* .line 425 */
v0 = this.mRecordEnableLock;
/* monitor-enter v0 */
/* .line 426 */
try { // :try_start_0
final String v1 = "AlwaysRecordBehavior :"; // const-string v1, "AlwaysRecordBehavior :"
(( java.io.PrintWriter ) p1 ).println ( v1 ); // invoke-virtual {p1, v1}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V
/* .line 427 */
int v1 = 0; // const/4 v1, 0x0
/* .local v1, "i":I */
} // :goto_0
v2 = this.mAlwaysRecordBehavior;
v2 = (( android.util.SparseBooleanArray ) v2 ).size ( ); // invoke-virtual {v2}, Landroid/util/SparseBooleanArray;->size()I
/* if-ge v1, v2, :cond_0 */
/* .line 428 */
/* new-instance v2, Ljava/lang/StringBuilder; */
/* invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V */
final String v3 = " behavior:"; // const-string v3, " behavior:"
(( java.lang.StringBuilder ) v2 ).append ( v3 ); // invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
v3 = this.mAlwaysRecordBehavior;
v3 = (( android.util.SparseBooleanArray ) v3 ).keyAt ( v1 ); // invoke-virtual {v3, v1}, Landroid/util/SparseBooleanArray;->keyAt(I)I
miui.security.AppBehavior .behaviorToName ( v3 );
(( java.lang.StringBuilder ) v2 ).append ( v3 ); // invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
final String v3 = " include system:"; // const-string v3, " include system:"
(( java.lang.StringBuilder ) v2 ).append ( v3 ); // invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
v3 = this.mAlwaysRecordBehavior;
v3 = (( android.util.SparseBooleanArray ) v3 ).valueAt ( v1 ); // invoke-virtual {v3, v1}, Landroid/util/SparseBooleanArray;->valueAt(I)Z
(( java.lang.StringBuilder ) v2 ).append ( v3 ); // invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v2 ).toString ( ); // invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
(( java.io.PrintWriter ) p1 ).println ( v2 ); // invoke-virtual {p1, v2}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V
/* .line 427 */
/* add-int/lit8 v1, v1, 0x1 */
/* .line 430 */
} // .end local v1 # "i":I
} // :cond_0
final String v1 = "PackageRecordBehavior :"; // const-string v1, "PackageRecordBehavior :"
(( java.io.PrintWriter ) p1 ).println ( v1 ); // invoke-virtual {p1, v1}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V
/* .line 431 */
int v1 = 0; // const/4 v1, 0x0
/* .restart local v1 # "i":I */
} // :goto_1
v2 = this.mPackageRecordBehavior;
v2 = (( android.util.ArrayMap ) v2 ).size ( ); // invoke-virtual {v2}, Landroid/util/ArrayMap;->size()I
/* if-ge v1, v2, :cond_2 */
/* .line 432 */
v2 = this.mPackageRecordBehavior;
(( android.util.ArrayMap ) v2 ).valueAt ( v1 ); // invoke-virtual {v2, v1}, Landroid/util/ArrayMap;->valueAt(I)Ljava/lang/Object;
/* check-cast v2, Ljava/util/Set; */
/* .line 433 */
/* .local v2, "watchingBehavior":Ljava/util/Set;, "Ljava/util/Set<Ljava/lang/Integer;>;" */
v3 = if ( v2 != null) { // if-eqz v2, :cond_1
/* if-lez v3, :cond_1 */
/* .line 434 */
v4 = } // :goto_2
if ( v4 != null) { // if-eqz v4, :cond_1
/* check-cast v4, Ljava/lang/Integer; */
v4 = (( java.lang.Integer ) v4 ).intValue ( ); // invoke-virtual {v4}, Ljava/lang/Integer;->intValue()I
/* .line 435 */
/* .local v4, "behavior":I */
/* new-instance v5, Ljava/lang/StringBuilder; */
/* invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V */
final String v6 = " package:"; // const-string v6, " package:"
(( java.lang.StringBuilder ) v5 ).append ( v6 ); // invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
v6 = this.mPackageRecordBehavior;
(( android.util.ArrayMap ) v6 ).keyAt ( v1 ); // invoke-virtual {v6, v1}, Landroid/util/ArrayMap;->keyAt(I)Ljava/lang/Object;
/* check-cast v6, Ljava/lang/String; */
(( java.lang.StringBuilder ) v5 ).append ( v6 ); // invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
final String v6 = " behavior:"; // const-string v6, " behavior:"
(( java.lang.StringBuilder ) v5 ).append ( v6 ); // invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
miui.security.AppBehavior .behaviorToName ( v4 );
(( java.lang.StringBuilder ) v5 ).append ( v6 ); // invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v5 ).toString ( ); // invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
(( java.io.PrintWriter ) p1 ).println ( v5 ); // invoke-virtual {p1, v5}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V
/* .line 436 */
} // .end local v4 # "behavior":I
/* .line 431 */
} // .end local v2 # "watchingBehavior":Ljava/util/Set;, "Ljava/util/Set<Ljava/lang/Integer;>;"
} // :cond_1
/* add-int/lit8 v1, v1, 0x1 */
/* .line 438 */
} // .end local v1 # "i":I
} // :cond_2
/* monitor-exit v0 */
/* :try_end_0 */
/* .catchall {:try_start_0 ..:try_end_0} :catchall_1 */
/* .line 439 */
v1 = this.mUidBehaviorLock;
/* monitor-enter v1 */
/* .line 440 */
try { // :try_start_1
/* new-instance v0, Ljava/lang/StringBuilder; */
/* invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V */
final String v2 = "dump current cache size:"; // const-string v2, "dump current cache size:"
(( java.lang.StringBuilder ) v0 ).append ( v2 ); // invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
/* iget v2, p0, Lcom/miui/server/security/AppBehaviorService;->mRecordSize:I */
(( java.lang.StringBuilder ) v0 ).append ( v2 ); // invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v0 ).toString ( ); // invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
(( java.io.PrintWriter ) p1 ).println ( v0 ); // invoke-virtual {p1, v0}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V
/* .line 441 */
int v0 = 0; // const/4 v0, 0x0
/* .local v0, "i":I */
} // :goto_3
v2 = this.mPackageBehavior;
v2 = (( android.util.ArrayMap ) v2 ).size ( ); // invoke-virtual {v2}, Landroid/util/ArrayMap;->size()I
/* if-ge v0, v2, :cond_4 */
/* .line 442 */
/* new-instance v2, Ljava/lang/StringBuilder; */
/* invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V */
final String v3 = "dump pkg:"; // const-string v3, "dump pkg:"
(( java.lang.StringBuilder ) v2 ).append ( v3 ); // invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
v3 = this.mPackageBehavior;
(( android.util.ArrayMap ) v3 ).keyAt ( v0 ); // invoke-virtual {v3, v0}, Landroid/util/ArrayMap;->keyAt(I)Ljava/lang/Object;
/* check-cast v3, Ljava/lang/String; */
(( java.lang.StringBuilder ) v2 ).append ( v3 ); // invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v2 ).toString ( ); // invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
(( java.io.PrintWriter ) p1 ).println ( v2 ); // invoke-virtual {p1, v2}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V
/* .line 443 */
v2 = this.mPackageBehavior;
(( android.util.ArrayMap ) v2 ).keyAt ( v0 ); // invoke-virtual {v2, v0}, Landroid/util/ArrayMap;->keyAt(I)Ljava/lang/Object;
(( android.util.ArrayMap ) v2 ).get ( v3 ); // invoke-virtual {v2, v3}, Landroid/util/ArrayMap;->get(Ljava/lang/Object;)Ljava/lang/Object;
/* check-cast v2, Lcom/miui/server/security/model/PackageBehaviorBean; */
/* .line 444 */
/* .local v2, "packageBehaviorBean":Lcom/miui/server/security/model/PackageBehaviorBean; */
if ( v2 != null) { // if-eqz v2, :cond_3
/* .line 445 */
(( com.miui.server.security.model.PackageBehaviorBean ) v2 ).dump ( p1 ); // invoke-virtual {v2, p1}, Lcom/miui/server/security/model/PackageBehaviorBean;->dump(Ljava/io/PrintWriter;)V
/* .line 441 */
} // .end local v2 # "packageBehaviorBean":Lcom/miui/server/security/model/PackageBehaviorBean;
} // :cond_3
/* add-int/lit8 v0, v0, 0x1 */
/* .line 448 */
} // .end local v0 # "i":I
} // :cond_4
final String v0 = "==================Dump AppBehaviorService end=================="; // const-string v0, "==================Dump AppBehaviorService end=================="
(( java.io.PrintWriter ) p1 ).println ( v0 ); // invoke-virtual {p1, v0}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V
/* .line 449 */
/* monitor-exit v1 */
/* .line 450 */
return;
/* .line 449 */
/* :catchall_0 */
/* move-exception v0 */
/* monitor-exit v1 */
/* :try_end_1 */
/* .catchall {:try_start_1 ..:try_end_1} :catchall_0 */
/* throw v0 */
/* .line 438 */
/* :catchall_1 */
/* move-exception v1 */
try { // :try_start_2
/* monitor-exit v0 */
/* :try_end_2 */
/* .catchall {:try_start_2 ..:try_end_2} :catchall_1 */
/* throw v1 */
} // .end method
public Long getThreshold ( Integer p0 ) {
/* .locals 3 */
/* .param p1, "behavior" # I */
/* .line 161 */
v0 = this.mBehaviorThreshold;
/* const-wide/16 v1, 0x0 */
(( android.util.SparseLongArray ) v0 ).get ( p1, v1, v2 ); // invoke-virtual {v0, p1, v1, v2}, Landroid/util/SparseLongArray;->get(IJ)J
/* move-result-wide v0 */
/* return-wide v0 */
} // .end method
public Boolean hasAppBehaviorWatching ( ) {
/* .locals 2 */
/* .line 151 */
v0 = this.mRecordEnableLock;
/* monitor-enter v0 */
/* .line 152 */
try { // :try_start_0
v1 = this.mAlwaysRecordBehavior;
v1 = (( android.util.SparseBooleanArray ) v1 ).size ( ); // invoke-virtual {v1}, Landroid/util/SparseBooleanArray;->size()I
/* if-gtz v1, :cond_1 */
v1 = this.mPackageRecordBehavior;
v1 = (( android.util.ArrayMap ) v1 ).size ( ); // invoke-virtual {v1}, Landroid/util/ArrayMap;->size()I
/* if-lez v1, :cond_0 */
} // :cond_0
int v1 = 0; // const/4 v1, 0x0
} // :cond_1
} // :goto_0
int v1 = 1; // const/4 v1, 0x1
} // :goto_1
/* monitor-exit v0 */
/* .line 153 */
/* :catchall_0 */
/* move-exception v1 */
/* monitor-exit v0 */
/* :try_end_0 */
/* .catchall {:try_start_0 ..:try_end_0} :catchall_0 */
/* throw v1 */
} // .end method
public Boolean moreThanThreshold ( Integer p0, Long p1 ) {
/* .locals 2 */
/* .param p1, "behavior" # I */
/* .param p2, "curCount" # J */
/* .line 165 */
(( com.miui.server.security.AppBehaviorService ) p0 ).getThreshold ( p1 ); // invoke-virtual {p0, p1}, Lcom/miui/server/security/AppBehaviorService;->getThreshold(I)J
/* move-result-wide v0 */
/* cmp-long v0, p2, v0 */
/* if-ltz v0, :cond_0 */
int v0 = 1; // const/4 v0, 0x1
} // :cond_0
int v0 = 0; // const/4 v0, 0x0
} // :goto_0
} // .end method
public void persistBehaviorRecord ( ) {
/* .locals 5 */
/* .line 268 */
v0 = this.mPersistMessenger;
/* if-nez v0, :cond_1 */
/* .line 269 */
v0 = this.mWorkHandler;
/* const/16 v1, 0xe15 */
v0 = (( android.os.Handler ) v0 ).hasMessages ( v1 ); // invoke-virtual {v0, v1}, Landroid/os/Handler;->hasMessages(I)Z
if ( v0 != null) { // if-eqz v0, :cond_0
/* .line 270 */
return;
/* .line 272 */
} // :cond_0
/* new-instance v0, Landroid/content/Intent; */
/* invoke-direct {v0}, Landroid/content/Intent;-><init>()V */
/* .line 273 */
/* .local v0, "intent":Landroid/content/Intent; */
/* new-instance v2, Landroid/content/ComponentName; */
final String v3 = "com.miui.securitycenter"; // const-string v3, "com.miui.securitycenter"
final String v4 = "com.miui.permcenter.monitor.AppUsagePersistService"; // const-string v4, "com.miui.permcenter.monitor.AppUsagePersistService"
/* invoke-direct {v2, v3, v4}, Landroid/content/ComponentName;-><init>(Ljava/lang/String;Ljava/lang/String;)V */
(( android.content.Intent ) v0 ).setComponent ( v2 ); // invoke-virtual {v0, v2}, Landroid/content/Intent;->setComponent(Landroid/content/ComponentName;)Landroid/content/Intent;
/* .line 275 */
final String v2 = "AppBehaviorService"; // const-string v2, "AppBehaviorService"
/* const-string/jumbo v3, "start persistBehaviorRecord, try bind service." */
android.util.Slog .i ( v2,v3 );
/* .line 276 */
v2 = this.mContext;
/* new-instance v3, Lcom/miui/server/security/AppBehaviorService$4; */
/* invoke-direct {v3, p0}, Lcom/miui/server/security/AppBehaviorService$4;-><init>(Lcom/miui/server/security/AppBehaviorService;)V */
int v4 = 1; // const/4 v4, 0x1
(( android.content.Context ) v2 ).bindService ( v0, v3, v4 ); // invoke-virtual {v2, v0, v3, v4}, Landroid/content/Context;->bindService(Landroid/content/Intent;Landroid/content/ServiceConnection;I)Z
/* .line 290 */
v2 = this.mWorkHandler;
/* const-wide/16 v3, 0x2710 */
(( android.os.Handler ) v2 ).sendEmptyMessageDelayed ( v1, v3, v4 ); // invoke-virtual {v2, v1, v3, v4}, Landroid/os/Handler;->sendEmptyMessageDelayed(IJ)Z
/* .line 291 */
} // .end local v0 # "intent":Landroid/content/Intent;
/* .line 292 */
} // :cond_1
/* invoke-direct {p0}, Lcom/miui/server/security/AppBehaviorService;->schedulePersist()V */
/* .line 294 */
} // :goto_0
return;
} // .end method
public void recordAppBehavior ( miui.security.AppBehavior p0 ) {
/* .locals 12 */
/* .param p1, "appBehavior" # Lmiui/security/AppBehavior; */
/* .line 351 */
if ( p1 != null) { // if-eqz p1, :cond_c
v0 = (( miui.security.AppBehavior ) p1 ).isValid ( ); // invoke-virtual {p1}, Lmiui/security/AppBehavior;->isValid()Z
/* if-nez v0, :cond_0 */
/* goto/16 :goto_4 */
/* .line 355 */
} // :cond_0
(( miui.security.AppBehavior ) p1 ).getCallerPkg ( ); // invoke-virtual {p1}, Lmiui/security/AppBehavior;->getCallerPkg()Ljava/lang/String;
int v1 = 0; // const/4 v1, 0x0
/* invoke-direct {p0, v0, v1}, Lcom/miui/server/security/AppBehaviorService;->getApplicationInfo(Ljava/lang/String;I)Landroid/content/pm/ApplicationInfo; */
/* .line 356 */
/* .local v6, "callerPkg":Landroid/content/pm/ApplicationInfo; */
/* if-nez v6, :cond_1 */
/* .line 357 */
return;
/* .line 359 */
} // :cond_1
v0 = (( miui.security.AppBehavior ) p1 ).getBehavior ( ); // invoke-virtual {p1}, Lmiui/security/AppBehavior;->getBehavior()I
v7 = miui.security.AppBehavior .getBehaviorType ( v0 );
/* .line 360 */
/* .local v7, "behaviorType":I */
/* if-nez v7, :cond_2 */
/* .line 361 */
return;
/* .line 363 */
} // :cond_2
int v0 = 4; // const/4 v0, 0x4
int v2 = 3; // const/4 v2, 0x3
int v3 = 2; // const/4 v3, 0x2
/* if-ne v7, v3, :cond_8 */
/* .line 364 */
(( miui.security.AppBehavior ) p1 ).getCalleePkg ( ); // invoke-virtual {p1}, Lmiui/security/AppBehavior;->getCalleePkg()Ljava/lang/String;
/* invoke-direct {p0, v4, v1}, Lcom/miui/server/security/AppBehaviorService;->getApplicationInfo(Ljava/lang/String;I)Landroid/content/pm/ApplicationInfo; */
/* .line 365 */
/* .local v8, "calleePkg":Landroid/content/pm/ApplicationInfo; */
/* if-nez v8, :cond_3 */
/* .line 366 */
return;
/* .line 368 */
} // :cond_3
v1 = (( miui.security.AppBehavior ) p1 ).getBehavior ( ); // invoke-virtual {p1}, Lmiui/security/AppBehavior;->getBehavior()I
/* if-ne v1, v3, :cond_9 */
/* .line 369 */
v9 = com.miui.server.security.AppBehaviorService .isSystem ( v6 );
/* .line 370 */
/* .local v9, "callerSystem":Z */
v10 = com.miui.server.security.AppBehaviorService .isSystem ( v8 );
/* .line 371 */
/* .local v10, "calleeSystem":Z */
if ( v9 != null) { // if-eqz v9, :cond_4
if ( v10 != null) { // if-eqz v10, :cond_4
/* .line 372 */
return;
/* .line 374 */
} // :cond_4
/* if-eq v9, v10, :cond_6 */
/* .line 375 */
if ( v9 != null) { // if-eqz v9, :cond_5
(( miui.security.AppBehavior ) p1 ).getCalleePkg ( ); // invoke-virtual {p1}, Lmiui/security/AppBehavior;->getCalleePkg()Ljava/lang/String;
} // :cond_5
(( miui.security.AppBehavior ) p1 ).getCallerPkg ( ); // invoke-virtual {p1}, Lmiui/security/AppBehavior;->getCallerPkg()Ljava/lang/String;
} // :goto_0
/* move-object v11, v1 */
/* .line 376 */
/* .local v11, "willBeCheckPkg":Ljava/lang/String; */
/* const/16 v1, 0x1c */
v1 = (( com.miui.server.security.AppBehaviorService ) p0 ).canRecordBehavior ( v1, v11 ); // invoke-virtual {p0, v1, v11}, Lcom/miui/server/security/AppBehaviorService;->canRecordBehavior(ILjava/lang/String;)Z
if ( v1 != null) { // if-eqz v1, :cond_6
/* .line 377 */
/* const/16 v1, 0x1c */
/* .line 378 */
(( miui.security.AppBehavior ) p1 ).getCallerPkg ( ); // invoke-virtual {p1}, Lmiui/security/AppBehavior;->getCallerPkg()Ljava/lang/String;
(( miui.security.AppBehavior ) p1 ).getChain ( ); // invoke-virtual {p1}, Lmiui/security/AppBehavior;->getChain()Ljava/lang/String;
(( miui.security.AppBehavior ) p1 ).getCount ( ); // invoke-virtual {p1}, Lmiui/security/AppBehavior;->getCount()J
/* move-result-wide v4 */
/* .line 377 */
/* move-object v0, p0 */
/* invoke-direct/range {v0 ..v5}, Lcom/miui/server/security/AppBehaviorService;->scheduleRecord(ILjava/lang/String;Ljava/lang/String;J)V */
/* .line 379 */
return;
/* .line 382 */
} // .end local v11 # "willBeCheckPkg":Ljava/lang/String;
} // :cond_6
(( miui.security.AppBehavior ) p1 ).getCallerPkg ( ); // invoke-virtual {p1}, Lmiui/security/AppBehavior;->getCallerPkg()Ljava/lang/String;
v0 = (( com.miui.server.security.AppBehaviorService ) p0 ).canRecordBehavior ( v0, v1 ); // invoke-virtual {p0, v0, v1}, Lcom/miui/server/security/AppBehaviorService;->canRecordBehavior(ILjava/lang/String;)Z
/* if-nez v0, :cond_7 */
/* .line 383 */
(( miui.security.AppBehavior ) p1 ).getCalleePkg ( ); // invoke-virtual {p1}, Lmiui/security/AppBehavior;->getCalleePkg()Ljava/lang/String;
v0 = (( com.miui.server.security.AppBehaviorService ) p0 ).canRecordBehavior ( v2, v0 ); // invoke-virtual {p0, v2, v0}, Lcom/miui/server/security/AppBehaviorService;->canRecordBehavior(ILjava/lang/String;)Z
if ( v0 != null) { // if-eqz v0, :cond_9
/* .line 384 */
} // :cond_7
int v1 = 4; // const/4 v1, 0x4
/* .line 385 */
(( miui.security.AppBehavior ) p1 ).getCallerPkg ( ); // invoke-virtual {p1}, Lmiui/security/AppBehavior;->getCallerPkg()Ljava/lang/String;
(( miui.security.AppBehavior ) p1 ).getChain ( ); // invoke-virtual {p1}, Lmiui/security/AppBehavior;->getChain()Ljava/lang/String;
(( miui.security.AppBehavior ) p1 ).getCount ( ); // invoke-virtual {p1}, Lmiui/security/AppBehavior;->getCount()J
/* move-result-wide v4 */
/* .line 384 */
/* move-object v0, p0 */
/* invoke-direct/range {v0 ..v5}, Lcom/miui/server/security/AppBehaviorService;->scheduleRecord(ILjava/lang/String;Ljava/lang/String;J)V */
/* .line 388 */
} // .end local v8 # "calleePkg":Landroid/content/pm/ApplicationInfo;
} // .end local v9 # "callerSystem":Z
} // .end local v10 # "calleeSystem":Z
} // :cond_8
int v1 = 1; // const/4 v1, 0x1
/* if-eq v7, v1, :cond_a */
/* if-eq v7, v2, :cond_a */
/* if-ne v7, v0, :cond_9 */
} // :cond_9
} // :goto_1
/* .line 391 */
} // :cond_a
} // :goto_2
v0 = (( miui.security.AppBehavior ) p1 ).getBehavior ( ); // invoke-virtual {p1}, Lmiui/security/AppBehavior;->getBehavior()I
(( miui.security.AppBehavior ) p1 ).getCallerPkg ( ); // invoke-virtual {p1}, Lmiui/security/AppBehavior;->getCallerPkg()Ljava/lang/String;
v0 = (( com.miui.server.security.AppBehaviorService ) p0 ).canRecordBehavior ( v0, v1 ); // invoke-virtual {p0, v0, v1}, Lcom/miui/server/security/AppBehaviorService;->canRecordBehavior(ILjava/lang/String;)Z
if ( v0 != null) { // if-eqz v0, :cond_b
/* .line 392 */
v1 = (( miui.security.AppBehavior ) p1 ).getBehavior ( ); // invoke-virtual {p1}, Lmiui/security/AppBehavior;->getBehavior()I
(( miui.security.AppBehavior ) p1 ).getCallerPkg ( ); // invoke-virtual {p1}, Lmiui/security/AppBehavior;->getCallerPkg()Ljava/lang/String;
/* .line 393 */
(( miui.security.AppBehavior ) p1 ).getExtra ( ); // invoke-virtual {p1}, Lmiui/security/AppBehavior;->getExtra()Ljava/lang/String;
(( miui.security.AppBehavior ) p1 ).getCount ( ); // invoke-virtual {p1}, Lmiui/security/AppBehavior;->getCount()J
/* move-result-wide v4 */
/* .line 392 */
/* move-object v0, p0 */
/* invoke-direct/range {v0 ..v5}, Lcom/miui/server/security/AppBehaviorService;->scheduleRecord(ILjava/lang/String;Ljava/lang/String;J)V */
/* .line 396 */
} // :cond_b
} // :goto_3
return;
/* .line 352 */
} // .end local v6 # "callerPkg":Landroid/content/pm/ApplicationInfo;
} // .end local v7 # "behaviorType":I
} // :cond_c
} // :goto_4
return;
} // .end method
public void recordAppBehaviorAsync ( miui.security.AppBehavior p0 ) {
/* .locals 2 */
/* .param p1, "appBehavior" # Lmiui/security/AppBehavior; */
/* .line 344 */
v0 = miui.mqsas.scout.ScoutUtils .isLibraryTest ( );
if ( v0 != null) { // if-eqz v0, :cond_0
/* .line 345 */
return;
/* .line 347 */
} // :cond_0
v0 = this.mWorkHandler;
/* new-instance v1, Lcom/miui/server/security/AppBehaviorService$$ExternalSyntheticLambda0; */
/* invoke-direct {v1, p0, p1}, Lcom/miui/server/security/AppBehaviorService$$ExternalSyntheticLambda0;-><init>(Lcom/miui/server/security/AppBehaviorService;Lmiui/security/AppBehavior;)V */
(( android.os.Handler ) v0 ).post ( v1 ); // invoke-virtual {v0, v1}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z
/* .line 348 */
return;
} // .end method
public void setAppBehaviorDuration ( com.miui.server.security.AppDurationService p0 ) {
/* .locals 0 */
/* .param p1, "appDurationService" # Lcom/miui/server/security/AppDurationService; */
/* .line 469 */
this.mAppBehaviorDuration = p1;
/* .line 470 */
return;
} // .end method
public void startWatchingAppBehavior ( Integer p0, Boolean p1 ) {
/* .locals 4 */
/* .param p1, "behavior" # I */
/* .param p2, "includeSystem" # Z */
/* .line 172 */
v0 = this.mRecordEnableLock;
/* monitor-enter v0 */
/* .line 173 */
try { // :try_start_0
v1 = this.mAlwaysRecordBehavior;
(( android.util.SparseBooleanArray ) v1 ).put ( p1, p2 ); // invoke-virtual {v1, p1, p2}, Landroid/util/SparseBooleanArray;->put(IZ)V
/* .line 174 */
int v1 = 1; // const/4 v1, 0x1
/* invoke-direct {p0, p1, v1}, Lcom/miui/server/security/AppBehaviorService;->registerListenerForBehavior(IZ)V */
/* .line 175 */
final String v1 = "AppBehaviorService"; // const-string v1, "AppBehaviorService"
/* new-instance v2, Ljava/lang/StringBuilder; */
/* invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V */
/* const-string/jumbo v3, "startWatching:" */
(( java.lang.StringBuilder ) v2 ).append ( v3 ); // invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v2 ).append ( p1 ); // invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;
final String v3 = ",includeSystem:"; // const-string v3, ",includeSystem:"
(( java.lang.StringBuilder ) v2 ).append ( v3 ); // invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v2 ).append ( p2 ); // invoke-virtual {v2, p2}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v2 ).toString ( ); // invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
android.util.Slog .i ( v1,v2 );
/* .line 176 */
/* monitor-exit v0 */
/* .line 177 */
return;
/* .line 176 */
/* :catchall_0 */
/* move-exception v1 */
/* monitor-exit v0 */
/* :try_end_0 */
/* .catchall {:try_start_0 ..:try_end_0} :catchall_0 */
/* throw v1 */
} // .end method
public void startWatchingAppBehavior ( Integer p0, java.lang.String[] p1, Boolean p2 ) {
/* .locals 10 */
/* .param p1, "behavior" # I */
/* .param p2, "pkgNames" # [Ljava/lang/String; */
/* .param p3, "includeSystem" # Z */
/* .line 204 */
v0 = this.mRecordEnableLock;
/* monitor-enter v0 */
/* .line 205 */
try { // :try_start_0
/* array-length v1, p2 */
int v2 = 0; // const/4 v2, 0x0
/* move v3, v2 */
} // :goto_0
/* if-ge v3, v1, :cond_3 */
/* aget-object v4, p2, v3 */
/* .line 206 */
/* .local v4, "tmpPkgName":Ljava/lang/String; */
/* invoke-direct {p0, v4, v2}, Lcom/miui/server/security/AppBehaviorService;->getApplicationInfo(Ljava/lang/String;I)Landroid/content/pm/ApplicationInfo; */
/* .line 207 */
/* .local v5, "applicationInfo":Landroid/content/pm/ApplicationInfo; */
/* if-nez v5, :cond_0 */
/* .line 208 */
/* .line 210 */
} // :cond_0
/* if-nez p3, :cond_1 */
v6 = com.miui.server.security.AppBehaviorService .isSystem ( v5 );
if ( v6 != null) { // if-eqz v6, :cond_1
/* .line 211 */
/* .line 213 */
} // :cond_1
v6 = this.mPackageRecordBehavior;
(( android.util.ArrayMap ) v6 ).get ( v4 ); // invoke-virtual {v6, v4}, Landroid/util/ArrayMap;->get(Ljava/lang/Object;)Ljava/lang/Object;
/* check-cast v6, Ljava/util/Set; */
/* .line 214 */
/* .local v6, "behaviorWatching":Ljava/util/Set;, "Ljava/util/Set<Ljava/lang/Integer;>;" */
/* if-nez v6, :cond_2 */
/* .line 215 */
/* new-instance v7, Landroid/util/ArraySet; */
/* invoke-direct {v7}, Landroid/util/ArraySet;-><init>()V */
/* move-object v6, v7 */
/* .line 216 */
v7 = this.mPackageRecordBehavior;
(( android.util.ArrayMap ) v7 ).put ( v4, v6 ); // invoke-virtual {v7, v4, v6}, Landroid/util/ArrayMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;
/* .line 217 */
final String v7 = "AppBehaviorService"; // const-string v7, "AppBehaviorService"
/* new-instance v8, Ljava/lang/StringBuilder; */
/* invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V */
/* const-string/jumbo v9, "startWatching:" */
(( java.lang.StringBuilder ) v8 ).append ( v9 ); // invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v8 ).append ( p1 ); // invoke-virtual {v8, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;
final String v9 = ",pkg:"; // const-string v9, ",pkg:"
(( java.lang.StringBuilder ) v8 ).append ( v9 ); // invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v8 ).append ( v4 ); // invoke-virtual {v8, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v8 ).toString ( ); // invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
android.util.Slog .i ( v7,v8 );
/* .line 219 */
} // :cond_2
java.lang.Integer .valueOf ( p1 );
/* .line 205 */
} // .end local v4 # "tmpPkgName":Ljava/lang/String;
} // .end local v5 # "applicationInfo":Landroid/content/pm/ApplicationInfo;
} // .end local v6 # "behaviorWatching":Ljava/util/Set;, "Ljava/util/Set<Ljava/lang/Integer;>;"
} // :goto_1
/* add-int/lit8 v3, v3, 0x1 */
/* .line 221 */
} // :cond_3
int v1 = 1; // const/4 v1, 0x1
/* invoke-direct {p0, p1, v1}, Lcom/miui/server/security/AppBehaviorService;->registerListenerForBehavior(IZ)V */
/* .line 222 */
/* monitor-exit v0 */
/* .line 223 */
return;
/* .line 222 */
/* :catchall_0 */
/* move-exception v1 */
/* monitor-exit v0 */
/* :try_end_0 */
/* .catchall {:try_start_0 ..:try_end_0} :catchall_0 */
/* throw v1 */
} // .end method
public void stopWatchingAppBehavior ( Integer p0 ) {
/* .locals 5 */
/* .param p1, "behavior" # I */
/* .line 183 */
v0 = this.mRecordEnableLock;
/* monitor-enter v0 */
/* .line 184 */
int v1 = 1; // const/4 v1, 0x1
/* if-ne p1, v1, :cond_0 */
/* .line 185 */
try { // :try_start_0
v1 = this.mAlwaysRecordBehavior;
(( android.util.SparseBooleanArray ) v1 ).clear ( ); // invoke-virtual {v1}, Landroid/util/SparseBooleanArray;->clear()V
/* .line 186 */
v1 = this.mPackageRecordBehavior;
(( android.util.ArrayMap ) v1 ).clear ( ); // invoke-virtual {v1}, Landroid/util/ArrayMap;->clear()V
/* .line 188 */
} // :cond_0
v1 = this.mAlwaysRecordBehavior;
(( android.util.SparseBooleanArray ) v1 ).delete ( p1 ); // invoke-virtual {v1, p1}, Landroid/util/SparseBooleanArray;->delete(I)V
/* .line 189 */
v1 = this.mPackageRecordBehavior;
(( android.util.ArrayMap ) v1 ).entrySet ( ); // invoke-virtual {v1}, Landroid/util/ArrayMap;->entrySet()Ljava/util/Set;
/* .local v1, "it":Ljava/util/Iterator;, "Ljava/util/Iterator<Ljava/util/Map$Entry<Ljava/lang/String;Ljava/util/Set<Ljava/lang/Integer;>;>;>;" */
v2 = } // :goto_0
if ( v2 != null) { // if-eqz v2, :cond_2
/* .line 190 */
/* check-cast v2, Ljava/util/Map$Entry; */
/* .line 191 */
/* .local v2, "item":Ljava/util/Map$Entry;, "Ljava/util/Map$Entry<Ljava/lang/String;Ljava/util/Set<Ljava/lang/Integer;>;>;" */
/* check-cast v3, Ljava/util/Set; */
/* .line 192 */
/* .local v3, "itemWatching":Ljava/util/Set;, "Ljava/util/Set<Ljava/lang/Integer;>;" */
java.lang.Integer .valueOf ( p1 );
v4 = /* .line 193 */
if ( v4 != null) { // if-eqz v4, :cond_1
/* .line 194 */
/* .line 196 */
} // .end local v2 # "item":Ljava/util/Map$Entry;, "Ljava/util/Map$Entry<Ljava/lang/String;Ljava/util/Set<Ljava/lang/Integer;>;>;"
} // .end local v3 # "itemWatching":Ljava/util/Set;, "Ljava/util/Set<Ljava/lang/Integer;>;"
} // :cond_1
/* .line 198 */
} // .end local v1 # "it":Ljava/util/Iterator;, "Ljava/util/Iterator<Ljava/util/Map$Entry<Ljava/lang/String;Ljava/util/Set<Ljava/lang/Integer;>;>;>;"
} // :cond_2
} // :goto_1
int v1 = 0; // const/4 v1, 0x0
/* invoke-direct {p0, p1, v1}, Lcom/miui/server/security/AppBehaviorService;->registerListenerForBehavior(IZ)V */
/* .line 199 */
final String v1 = "AppBehaviorService"; // const-string v1, "AppBehaviorService"
/* new-instance v2, Ljava/lang/StringBuilder; */
/* invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V */
/* const-string/jumbo v3, "stopWatching for all:" */
(( java.lang.StringBuilder ) v2 ).append ( v3 ); // invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v2 ).append ( p1 ); // invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v2 ).toString ( ); // invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
android.util.Slog .i ( v1,v2 );
/* .line 200 */
/* monitor-exit v0 */
/* .line 201 */
return;
/* .line 200 */
/* :catchall_0 */
/* move-exception v1 */
/* monitor-exit v0 */
/* :try_end_0 */
/* .catchall {:try_start_0 ..:try_end_0} :catchall_0 */
/* throw v1 */
} // .end method
public void stopWatchingAppBehavior ( Integer p0, java.lang.String[] p1 ) {
/* .locals 10 */
/* .param p1, "behavior" # I */
/* .param p2, "pkgNames" # [Ljava/lang/String; */
/* .line 226 */
v0 = this.mRecordEnableLock;
/* monitor-enter v0 */
/* .line 227 */
try { // :try_start_0
/* array-length v1, p2 */
int v2 = 0; // const/4 v2, 0x0
/* move v3, v2 */
} // :goto_0
/* if-ge v3, v1, :cond_2 */
/* aget-object v4, p2, v3 */
/* .line 228 */
/* .local v4, "tmpPkgName":Ljava/lang/String; */
v5 = this.mPackageRecordBehavior;
(( android.util.ArrayMap ) v5 ).get ( v4 ); // invoke-virtual {v5, v4}, Landroid/util/ArrayMap;->get(Ljava/lang/Object;)Ljava/lang/Object;
/* check-cast v5, Ljava/util/Set; */
/* .line 229 */
/* .local v5, "watchingBehavior":Ljava/util/Set;, "Ljava/util/Set<Ljava/lang/Integer;>;" */
final String v6 = "AppBehaviorService"; // const-string v6, "AppBehaviorService"
/* new-instance v7, Ljava/lang/StringBuilder; */
/* invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V */
/* const-string/jumbo v8, "stopWatching" */
(( java.lang.StringBuilder ) v7 ).append ( v8 ); // invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v7 ).append ( p1 ); // invoke-virtual {v7, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;
final String v8 = ",pkg:"; // const-string v8, ",pkg:"
(( java.lang.StringBuilder ) v7 ).append ( v8 ); // invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v7 ).append ( v4 ); // invoke-virtual {v7, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v7 ).toString ( ); // invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
android.util.Slog .i ( v6,v7 );
/* .line 230 */
if ( v5 != null) { // if-eqz v5, :cond_0
/* .line 231 */
java.lang.Integer .valueOf ( p1 );
v6 = /* .line 232 */
if ( v6 != null) { // if-eqz v6, :cond_0
/* .line 233 */
v6 = this.mPackageRecordBehavior;
(( android.util.ArrayMap ) v6 ).remove ( v4 ); // invoke-virtual {v6, v4}, Landroid/util/ArrayMap;->remove(Ljava/lang/Object;)Ljava/lang/Object;
/* .line 237 */
} // :cond_0
v6 = this.mAppBehaviorDuration;
if ( v6 != null) { // if-eqz v6, :cond_1
/* .line 238 */
/* invoke-direct {p0, v4, v2}, Lcom/miui/server/security/AppBehaviorService;->getApplicationInfo(Ljava/lang/String;I)Landroid/content/pm/ApplicationInfo; */
/* .line 239 */
/* .local v6, "applicationInfo":Landroid/content/pm/ApplicationInfo; */
if ( v6 != null) { // if-eqz v6, :cond_1
/* .line 240 */
v7 = this.mAppBehaviorDuration;
/* iget v8, v6, Landroid/content/pm/ApplicationInfo;->uid:I */
v9 = this.packageName;
(( com.miui.server.security.AppDurationService ) v7 ).removeDurationEvent ( p1, v8, v9 ); // invoke-virtual {v7, p1, v8, v9}, Lcom/miui/server/security/AppDurationService;->removeDurationEvent(IILjava/lang/String;)V
/* .line 227 */
} // .end local v4 # "tmpPkgName":Ljava/lang/String;
} // .end local v5 # "watchingBehavior":Ljava/util/Set;, "Ljava/util/Set<Ljava/lang/Integer;>;"
} // .end local v6 # "applicationInfo":Landroid/content/pm/ApplicationInfo;
} // :cond_1
/* add-int/lit8 v3, v3, 0x1 */
/* .line 244 */
} // :cond_2
/* monitor-exit v0 */
/* .line 245 */
return;
/* .line 244 */
/* :catchall_0 */
/* move-exception v1 */
/* monitor-exit v0 */
/* :try_end_0 */
/* .catchall {:try_start_0 ..:try_end_0} :catchall_0 */
/* throw v1 */
} // .end method
public void updateBehaviorThreshold ( Integer p0, Long p1 ) {
/* .locals 1 */
/* .param p1, "behavior" # I */
/* .param p2, "threshold" # J */
/* .line 157 */
v0 = this.mBehaviorThreshold;
(( android.util.SparseLongArray ) v0 ).put ( p1, p2, p3 ); // invoke-virtual {v0, p1, p2, p3}, Landroid/util/SparseLongArray;->put(IJ)V
/* .line 158 */
return;
} // .end method
