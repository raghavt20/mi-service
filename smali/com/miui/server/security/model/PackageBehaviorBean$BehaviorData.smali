.class Lcom/miui/server/security/model/PackageBehaviorBean$BehaviorData;
.super Ljava/lang/Object;
.source "PackageBehaviorBean.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/miui/server/security/model/PackageBehaviorBean;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0xa
    name = "BehaviorData"
.end annotation


# instance fields
.field private data:J

.field private stringIntegerArrayMap:Landroid/util/ArrayMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/util/ArrayMap<",
            "Ljava/lang/String;",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method static bridge synthetic -$$Nest$fgetdata(Lcom/miui/server/security/model/PackageBehaviorBean$BehaviorData;)J
    .locals 2

    iget-wide v0, p0, Lcom/miui/server/security/model/PackageBehaviorBean$BehaviorData;->data:J

    return-wide v0
.end method

.method static bridge synthetic -$$Nest$fgetstringIntegerArrayMap(Lcom/miui/server/security/model/PackageBehaviorBean$BehaviorData;)Landroid/util/ArrayMap;
    .locals 0

    iget-object p0, p0, Lcom/miui/server/security/model/PackageBehaviorBean$BehaviorData;->stringIntegerArrayMap:Landroid/util/ArrayMap;

    return-object p0
.end method

.method private constructor <init>()V
    .locals 0

    .line 86
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method synthetic constructor <init>(Lcom/miui/server/security/model/PackageBehaviorBean$BehaviorData-IA;)V
    .locals 0

    invoke-direct {p0}, Lcom/miui/server/security/model/PackageBehaviorBean$BehaviorData;-><init>()V

    return-void
.end method


# virtual methods
.method public appendData(J)Z
    .locals 7
    .param p1, "val"    # J

    .line 120
    const-wide/16 v0, 0x0

    cmp-long v2, p1, v0

    if-gtz v2, :cond_0

    .line 121
    const/4 v0, 0x0

    return v0

    .line 123
    :cond_0
    const/4 v2, 0x0

    .line 124
    .local v2, "addCount":Z
    iget-wide v3, p0, Lcom/miui/server/security/model/PackageBehaviorBean$BehaviorData;->data:J

    const-wide/16 v5, -0x1

    cmp-long v3, v3, v5

    if-nez v3, :cond_1

    .line 125
    iput-wide v0, p0, Lcom/miui/server/security/model/PackageBehaviorBean$BehaviorData;->data:J

    .line 126
    const/4 v2, 0x1

    .line 128
    :cond_1
    iget-wide v0, p0, Lcom/miui/server/security/model/PackageBehaviorBean$BehaviorData;->data:J

    add-long/2addr v0, p1

    iput-wide v0, p0, Lcom/miui/server/security/model/PackageBehaviorBean$BehaviorData;->data:J

    .line 129
    return v2
.end method

.method public appendList(Ljava/lang/String;)Z
    .locals 4
    .param p1, "val"    # Ljava/lang/String;

    .line 104
    invoke-static {p1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    const/4 v1, 0x0

    if-eqz v0, :cond_0

    .line 105
    return v1

    .line 107
    :cond_0
    iget-object v0, p0, Lcom/miui/server/security/model/PackageBehaviorBean$BehaviorData;->stringIntegerArrayMap:Landroid/util/ArrayMap;

    if-nez v0, :cond_1

    .line 108
    new-instance v0, Landroid/util/ArrayMap;

    invoke-direct {v0}, Landroid/util/ArrayMap;-><init>()V

    iput-object v0, p0, Lcom/miui/server/security/model/PackageBehaviorBean$BehaviorData;->stringIntegerArrayMap:Landroid/util/ArrayMap;

    .line 110
    :cond_1
    iget-object v0, p0, Lcom/miui/server/security/model/PackageBehaviorBean$BehaviorData;->stringIntegerArrayMap:Landroid/util/ArrayMap;

    invoke-virtual {v0, p1}, Landroid/util/ArrayMap;->containsKey(Ljava/lang/Object;)Z

    move-result v0

    const/4 v2, 0x1

    if-eqz v0, :cond_2

    .line 111
    iget-object v0, p0, Lcom/miui/server/security/model/PackageBehaviorBean$BehaviorData;->stringIntegerArrayMap:Landroid/util/ArrayMap;

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    invoke-virtual {v0, p1, v3}, Landroid/util/ArrayMap;->getOrDefault(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    .line 112
    .local v0, "curCount":I
    iget-object v3, p0, Lcom/miui/server/security/model/PackageBehaviorBean$BehaviorData;->stringIntegerArrayMap:Landroid/util/ArrayMap;

    add-int/2addr v0, v2

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v3, p1, v2}, Landroid/util/ArrayMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 113
    return v1

    .line 115
    .end local v0    # "curCount":I
    :cond_2
    iget-object v0, p0, Lcom/miui/server/security/model/PackageBehaviorBean$BehaviorData;->stringIntegerArrayMap:Landroid/util/ArrayMap;

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-virtual {v0, p1, v1}, Landroid/util/ArrayMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 116
    return v2
.end method

.method public dump(Ljava/io/PrintWriter;)V
    .locals 4
    .param p1, "pw"    # Ljava/io/PrintWriter;

    .line 133
    iget-object v0, p0, Lcom/miui/server/security/model/PackageBehaviorBean$BehaviorData;->stringIntegerArrayMap:Landroid/util/ArrayMap;

    if-eqz v0, :cond_0

    invoke-virtual {v0}, Landroid/util/ArrayMap;->size()I

    move-result v0

    if-lez v0, :cond_0

    .line 134
    iget-object v0, p0, Lcom/miui/server/security/model/PackageBehaviorBean$BehaviorData;->stringIntegerArrayMap:Landroid/util/ArrayMap;

    invoke-virtual {v0}, Landroid/util/ArrayMap;->keySet()Ljava/util/Set;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v0

    :goto_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_0

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/String;

    .line 135
    .local v1, "tmpString":Ljava/lang/String;
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "            value:"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {p1, v2}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V

    .line 136
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "                count:"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget-object v3, p0, Lcom/miui/server/security/model/PackageBehaviorBean$BehaviorData;->stringIntegerArrayMap:Landroid/util/ArrayMap;

    invoke-virtual {v3, v1}, Landroid/util/ArrayMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {p1, v2}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V

    .line 137
    .end local v1    # "tmpString":Ljava/lang/String;
    goto :goto_0

    .line 139
    :cond_0
    iget-wide v0, p0, Lcom/miui/server/security/model/PackageBehaviorBean$BehaviorData;->data:J

    const-wide/16 v2, -0x1

    cmp-long v0, v0, v2

    if-eqz v0, :cond_1

    .line 140
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "            data:"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-wide v1, p0, Lcom/miui/server/security/model/PackageBehaviorBean$BehaviorData;->data:J

    invoke-virtual {v0, v1, v2}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v0}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V

    .line 142
    :cond_1
    return-void
.end method

.method public init()I
    .locals 5

    .line 91
    const/4 v0, 0x0

    .line 92
    .local v0, "clearSize":I
    iget-object v1, p0, Lcom/miui/server/security/model/PackageBehaviorBean$BehaviorData;->stringIntegerArrayMap:Landroid/util/ArrayMap;

    if-eqz v1, :cond_0

    invoke-virtual {v1}, Landroid/util/ArrayMap;->size()I

    move-result v1

    if-lez v1, :cond_0

    .line 93
    iget-object v1, p0, Lcom/miui/server/security/model/PackageBehaviorBean$BehaviorData;->stringIntegerArrayMap:Landroid/util/ArrayMap;

    invoke-virtual {v1}, Landroid/util/ArrayMap;->size()I

    move-result v0

    .line 94
    iget-object v1, p0, Lcom/miui/server/security/model/PackageBehaviorBean$BehaviorData;->stringIntegerArrayMap:Landroid/util/ArrayMap;

    invoke-virtual {v1}, Landroid/util/ArrayMap;->clear()V

    .line 96
    :cond_0
    iget-wide v1, p0, Lcom/miui/server/security/model/PackageBehaviorBean$BehaviorData;->data:J

    const-wide/16 v3, -0x1

    cmp-long v1, v1, v3

    if-eqz v1, :cond_1

    .line 97
    add-int/lit8 v0, v0, 0x1

    .line 98
    iput-wide v3, p0, Lcom/miui/server/security/model/PackageBehaviorBean$BehaviorData;->data:J

    .line 100
    :cond_1
    return v0
.end method
