public class com.miui.server.security.model.PackageBehaviorBean {
	 /* .source "PackageBehaviorBean.java" */
	 /* # annotations */
	 /* .annotation system Ldalvik/annotation/MemberClasses; */
	 /* value = { */
	 /* Lcom/miui/server/security/model/PackageBehaviorBean$BehaviorData; */
	 /* } */
} // .end annotation
/* # instance fields */
private final android.util.SparseArray mBehaviors;
/* .annotation system Ldalvik/annotation/Signature; */
/* value = { */
/* "Landroid/util/SparseArray<", */
/* "Lcom/miui/server/security/model/PackageBehaviorBean$BehaviorData;", */
/* ">;" */
/* } */
} // .end annotation
} // .end field
/* # direct methods */
public com.miui.server.security.model.PackageBehaviorBean ( ) {
/* .locals 1 */
/* .line 18 */
/* invoke-direct {p0}, Ljava/lang/Object;-><init>()V */
/* .line 19 */
/* new-instance v0, Landroid/util/SparseArray; */
/* invoke-direct {v0}, Landroid/util/SparseArray;-><init>()V */
this.mBehaviors = v0;
return;
} // .end method
/* # virtual methods */
public void dump ( java.io.PrintWriter p0 ) {
/* .locals 4 */
/* .param p1, "pw" # Ljava/io/PrintWriter; */
/* .line 79 */
int v0 = 0; // const/4 v0, 0x0
/* .local v0, "i":I */
} // :goto_0
v1 = this.mBehaviors;
v1 = (( android.util.SparseArray ) v1 ).size ( ); // invoke-virtual {v1}, Landroid/util/SparseArray;->size()I
/* if-ge v0, v1, :cond_0 */
/* .line 80 */
v1 = this.mBehaviors;
v1 = (( android.util.SparseArray ) v1 ).keyAt ( v0 ); // invoke-virtual {v1, v0}, Landroid/util/SparseArray;->keyAt(I)I
/* .line 81 */
/* .local v1, "behavior":I */
/* new-instance v2, Ljava/lang/StringBuilder; */
/* invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V */
final String v3 = " behavior:"; // const-string v3, " behavior:"
(( java.lang.StringBuilder ) v2 ).append ( v3 ); // invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
miui.security.AppBehavior .behaviorToName ( v1 );
(( java.lang.StringBuilder ) v2 ).append ( v3 ); // invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v2 ).toString ( ); // invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
(( java.io.PrintWriter ) p1 ).println ( v2 ); // invoke-virtual {p1, v2}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V
/* .line 82 */
v2 = this.mBehaviors;
(( android.util.SparseArray ) v2 ).get ( v1 ); // invoke-virtual {v2, v1}, Landroid/util/SparseArray;->get(I)Ljava/lang/Object;
/* check-cast v2, Lcom/miui/server/security/model/PackageBehaviorBean$BehaviorData; */
(( com.miui.server.security.model.PackageBehaviorBean$BehaviorData ) v2 ).dump ( p1 ); // invoke-virtual {v2, p1}, Lcom/miui/server/security/model/PackageBehaviorBean$BehaviorData;->dump(Ljava/io/PrintWriter;)V
/* .line 79 */
} // .end local v1 # "behavior":I
/* add-int/lit8 v0, v0, 0x1 */
/* .line 84 */
} // .end local v0 # "i":I
} // :cond_0
return;
} // .end method
public Integer init ( ) {
/* .locals 4 */
/* .line 65 */
int v0 = 0; // const/4 v0, 0x0
/* .line 66 */
/* .local v0, "clearSize":I */
v1 = (( com.miui.server.security.model.PackageBehaviorBean ) p0 ).isEmpty ( ); // invoke-virtual {p0}, Lcom/miui/server/security/model/PackageBehaviorBean;->isEmpty()Z
if ( v1 != null) { // if-eqz v1, :cond_0
/* .line 67 */
/* .line 69 */
} // :cond_0
int v1 = 0; // const/4 v1, 0x0
/* .local v1, "i":I */
} // :goto_0
v2 = this.mBehaviors;
v2 = (( android.util.SparseArray ) v2 ).size ( ); // invoke-virtual {v2}, Landroid/util/SparseArray;->size()I
/* if-ge v1, v2, :cond_2 */
/* .line 70 */
v2 = this.mBehaviors;
(( android.util.SparseArray ) v2 ).valueAt ( v1 ); // invoke-virtual {v2, v1}, Landroid/util/SparseArray;->valueAt(I)Ljava/lang/Object;
/* check-cast v2, Lcom/miui/server/security/model/PackageBehaviorBean$BehaviorData; */
/* .line 71 */
/* .local v2, "behaviorData":Lcom/miui/server/security/model/PackageBehaviorBean$BehaviorData; */
if ( v2 != null) { // if-eqz v2, :cond_1
/* .line 72 */
v3 = (( com.miui.server.security.model.PackageBehaviorBean$BehaviorData ) v2 ).init ( ); // invoke-virtual {v2}, Lcom/miui/server/security/model/PackageBehaviorBean$BehaviorData;->init()I
/* add-int/2addr v0, v3 */
/* .line 69 */
} // .end local v2 # "behaviorData":Lcom/miui/server/security/model/PackageBehaviorBean$BehaviorData;
} // :cond_1
/* add-int/lit8 v1, v1, 0x1 */
/* .line 75 */
} // .end local v1 # "i":I
} // :cond_2
} // .end method
public Boolean isEmpty ( ) {
/* .locals 1 */
/* .line 36 */
v0 = this.mBehaviors;
v0 = (( android.util.SparseArray ) v0 ).size ( ); // invoke-virtual {v0}, Landroid/util/SparseArray;->size()I
/* if-nez v0, :cond_0 */
int v0 = 1; // const/4 v0, 0x1
} // :cond_0
int v0 = 0; // const/4 v0, 0x0
} // :goto_0
} // .end method
public Boolean recordAppBehavior ( Integer p0, java.lang.String p1, Long p2 ) {
/* .locals 2 */
/* .param p1, "behavior" # I */
/* .param p2, "stringValue" # Ljava/lang/String; */
/* .param p3, "longValue" # J */
/* .line 23 */
v0 = this.mBehaviors;
v0 = (( android.util.SparseArray ) v0 ).contains ( p1 ); // invoke-virtual {v0, p1}, Landroid/util/SparseArray;->contains(I)Z
/* if-nez v0, :cond_0 */
/* .line 24 */
/* new-instance v0, Lcom/miui/server/security/model/PackageBehaviorBean$BehaviorData; */
int v1 = 0; // const/4 v1, 0x0
/* invoke-direct {v0, v1}, Lcom/miui/server/security/model/PackageBehaviorBean$BehaviorData;-><init>(Lcom/miui/server/security/model/PackageBehaviorBean$BehaviorData-IA;)V */
/* .line 25 */
/* .local v0, "behaviorData":Lcom/miui/server/security/model/PackageBehaviorBean$BehaviorData; */
(( com.miui.server.security.model.PackageBehaviorBean$BehaviorData ) v0 ).init ( ); // invoke-virtual {v0}, Lcom/miui/server/security/model/PackageBehaviorBean$BehaviorData;->init()I
/* .line 26 */
v1 = this.mBehaviors;
(( android.util.SparseArray ) v1 ).put ( p1, v0 ); // invoke-virtual {v1, p1, v0}, Landroid/util/SparseArray;->put(ILjava/lang/Object;)V
/* .line 28 */
} // .end local v0 # "behaviorData":Lcom/miui/server/security/model/PackageBehaviorBean$BehaviorData;
} // :cond_0
v0 = android.text.TextUtils .isEmpty ( p2 );
/* if-nez v0, :cond_1 */
/* .line 29 */
v0 = this.mBehaviors;
(( android.util.SparseArray ) v0 ).get ( p1 ); // invoke-virtual {v0, p1}, Landroid/util/SparseArray;->get(I)Ljava/lang/Object;
/* check-cast v0, Lcom/miui/server/security/model/PackageBehaviorBean$BehaviorData; */
v0 = (( com.miui.server.security.model.PackageBehaviorBean$BehaviorData ) v0 ).appendList ( p2 ); // invoke-virtual {v0, p2}, Lcom/miui/server/security/model/PackageBehaviorBean$BehaviorData;->appendList(Ljava/lang/String;)Z
/* .line 31 */
} // :cond_1
v0 = this.mBehaviors;
(( android.util.SparseArray ) v0 ).get ( p1 ); // invoke-virtual {v0, p1}, Landroid/util/SparseArray;->get(I)Ljava/lang/Object;
/* check-cast v0, Lcom/miui/server/security/model/PackageBehaviorBean$BehaviorData; */
v0 = (( com.miui.server.security.model.PackageBehaviorBean$BehaviorData ) v0 ).appendData ( p3, p4 ); // invoke-virtual {v0, p3, p4}, Lcom/miui/server/security/model/PackageBehaviorBean$BehaviorData;->appendData(J)Z
} // .end method
public org.json.JSONObject toJson ( ) {
/* .locals 8 */
/* .annotation system Ldalvik/annotation/Throws; */
/* value = { */
/* Lorg/json/JSONException; */
/* } */
} // .end annotation
/* .line 40 */
v0 = (( com.miui.server.security.model.PackageBehaviorBean ) p0 ).isEmpty ( ); // invoke-virtual {p0}, Lcom/miui/server/security/model/PackageBehaviorBean;->isEmpty()Z
if ( v0 != null) { // if-eqz v0, :cond_0
/* .line 41 */
int v0 = 0; // const/4 v0, 0x0
/* .line 43 */
} // :cond_0
/* new-instance v0, Lorg/json/JSONObject; */
/* invoke-direct {v0}, Lorg/json/JSONObject;-><init>()V */
/* .line 44 */
/* .local v0, "packageWithBehaviors":Lorg/json/JSONObject; */
int v1 = 0; // const/4 v1, 0x0
/* .local v1, "i":I */
} // :goto_0
v2 = this.mBehaviors;
v2 = (( android.util.SparseArray ) v2 ).size ( ); // invoke-virtual {v2}, Landroid/util/SparseArray;->size()I
/* if-ge v1, v2, :cond_4 */
/* .line 45 */
v2 = this.mBehaviors;
v2 = (( android.util.SparseArray ) v2 ).keyAt ( v1 ); // invoke-virtual {v2, v1}, Landroid/util/SparseArray;->keyAt(I)I
/* .line 46 */
/* .local v2, "recordBehavior":I */
v3 = this.mBehaviors;
(( android.util.SparseArray ) v3 ).valueAt ( v1 ); // invoke-virtual {v3, v1}, Landroid/util/SparseArray;->valueAt(I)Ljava/lang/Object;
/* check-cast v3, Lcom/miui/server/security/model/PackageBehaviorBean$BehaviorData; */
/* .line 47 */
/* .local v3, "behaviorData":Lcom/miui/server/security/model/PackageBehaviorBean$BehaviorData; */
if ( v3 != null) { // if-eqz v3, :cond_3
/* .line 48 */
com.miui.server.security.model.PackageBehaviorBean$BehaviorData .-$$Nest$fgetstringIntegerArrayMap ( v3 );
if ( v4 != null) { // if-eqz v4, :cond_2
com.miui.server.security.model.PackageBehaviorBean$BehaviorData .-$$Nest$fgetstringIntegerArrayMap ( v3 );
v4 = (( android.util.ArrayMap ) v4 ).size ( ); // invoke-virtual {v4}, Landroid/util/ArrayMap;->size()I
/* if-lez v4, :cond_2 */
/* .line 49 */
com.miui.server.security.model.PackageBehaviorBean$BehaviorData .-$$Nest$fgetstringIntegerArrayMap ( v3 );
v4 = (( android.util.ArrayMap ) v4 ).size ( ); // invoke-virtual {v4}, Landroid/util/ArrayMap;->size()I
/* new-array v4, v4, [Ljava/lang/String; */
/* .line 50 */
/* .local v4, "spliceData":[Ljava/lang/String; */
int v5 = 0; // const/4 v5, 0x0
/* .local v5, "k":I */
} // :goto_1
com.miui.server.security.model.PackageBehaviorBean$BehaviorData .-$$Nest$fgetstringIntegerArrayMap ( v3 );
v6 = (( android.util.ArrayMap ) v6 ).size ( ); // invoke-virtual {v6}, Landroid/util/ArrayMap;->size()I
/* if-ge v5, v6, :cond_1 */
/* .line 51 */
/* new-instance v6, Ljava/lang/StringBuilder; */
/* invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V */
com.miui.server.security.model.PackageBehaviorBean$BehaviorData .-$$Nest$fgetstringIntegerArrayMap ( v3 );
(( android.util.ArrayMap ) v7 ).keyAt ( v5 ); // invoke-virtual {v7, v5}, Landroid/util/ArrayMap;->keyAt(I)Ljava/lang/Object;
/* check-cast v7, Ljava/lang/String; */
(( java.lang.StringBuilder ) v6 ).append ( v7 ); // invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
final String v7 = "@"; // const-string v7, "@"
(( java.lang.StringBuilder ) v6 ).append ( v7 ); // invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
com.miui.server.security.model.PackageBehaviorBean$BehaviorData .-$$Nest$fgetstringIntegerArrayMap ( v3 );
(( android.util.ArrayMap ) v7 ).valueAt ( v5 ); // invoke-virtual {v7, v5}, Landroid/util/ArrayMap;->valueAt(I)Ljava/lang/Object;
(( java.lang.StringBuilder ) v6 ).append ( v7 ); // invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v6 ).toString ( ); // invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
/* aput-object v6, v4, v5 */
/* .line 50 */
/* add-int/lit8 v5, v5, 0x1 */
/* .line 53 */
} // .end local v5 # "k":I
} // :cond_1
/* new-instance v5, Lorg/json/JSONArray; */
/* invoke-direct {v5, v4}, Lorg/json/JSONArray;-><init>(Ljava/lang/Object;)V */
/* .line 54 */
/* .local v5, "jsonArray":Lorg/json/JSONArray; */
java.lang.String .valueOf ( v2 );
(( org.json.JSONObject ) v0 ).put ( v6, v5 ); // invoke-virtual {v0, v6, v5}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;
/* .line 56 */
} // .end local v4 # "spliceData":[Ljava/lang/String;
} // .end local v5 # "jsonArray":Lorg/json/JSONArray;
} // :cond_2
com.miui.server.security.model.PackageBehaviorBean$BehaviorData .-$$Nest$fgetdata ( v3 );
/* move-result-wide v4 */
/* const-wide/16 v6, 0x0 */
/* cmp-long v4, v4, v6 */
/* if-lez v4, :cond_3 */
/* .line 57 */
java.lang.String .valueOf ( v2 );
com.miui.server.security.model.PackageBehaviorBean$BehaviorData .-$$Nest$fgetdata ( v3 );
/* move-result-wide v5 */
(( org.json.JSONObject ) v0 ).put ( v4, v5, v6 ); // invoke-virtual {v0, v4, v5, v6}, Lorg/json/JSONObject;->put(Ljava/lang/String;J)Lorg/json/JSONObject;
/* .line 44 */
} // .end local v2 # "recordBehavior":I
} // .end local v3 # "behaviorData":Lcom/miui/server/security/model/PackageBehaviorBean$BehaviorData;
} // :cond_3
/* add-int/lit8 v1, v1, 0x1 */
/* goto/16 :goto_0 */
/* .line 61 */
} // .end local v1 # "i":I
} // :cond_4
} // .end method
