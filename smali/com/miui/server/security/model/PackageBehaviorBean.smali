.class public Lcom/miui/server/security/model/PackageBehaviorBean;
.super Ljava/lang/Object;
.source "PackageBehaviorBean.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/miui/server/security/model/PackageBehaviorBean$BehaviorData;
    }
.end annotation


# instance fields
.field private final mBehaviors:Landroid/util/SparseArray;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/util/SparseArray<",
            "Lcom/miui/server/security/model/PackageBehaviorBean$BehaviorData;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>()V
    .locals 1

    .line 18
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 19
    new-instance v0, Landroid/util/SparseArray;

    invoke-direct {v0}, Landroid/util/SparseArray;-><init>()V

    iput-object v0, p0, Lcom/miui/server/security/model/PackageBehaviorBean;->mBehaviors:Landroid/util/SparseArray;

    return-void
.end method


# virtual methods
.method public dump(Ljava/io/PrintWriter;)V
    .locals 4
    .param p1, "pw"    # Ljava/io/PrintWriter;

    .line 79
    const/4 v0, 0x0

    .local v0, "i":I
    :goto_0
    iget-object v1, p0, Lcom/miui/server/security/model/PackageBehaviorBean;->mBehaviors:Landroid/util/SparseArray;

    invoke-virtual {v1}, Landroid/util/SparseArray;->size()I

    move-result v1

    if-ge v0, v1, :cond_0

    .line 80
    iget-object v1, p0, Lcom/miui/server/security/model/PackageBehaviorBean;->mBehaviors:Landroid/util/SparseArray;

    invoke-virtual {v1, v0}, Landroid/util/SparseArray;->keyAt(I)I

    move-result v1

    .line 81
    .local v1, "behavior":I
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "        behavior:"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-static {v1}, Lmiui/security/AppBehavior;->behaviorToName(I)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {p1, v2}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V

    .line 82
    iget-object v2, p0, Lcom/miui/server/security/model/PackageBehaviorBean;->mBehaviors:Landroid/util/SparseArray;

    invoke-virtual {v2, v1}, Landroid/util/SparseArray;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/miui/server/security/model/PackageBehaviorBean$BehaviorData;

    invoke-virtual {v2, p1}, Lcom/miui/server/security/model/PackageBehaviorBean$BehaviorData;->dump(Ljava/io/PrintWriter;)V

    .line 79
    .end local v1    # "behavior":I
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 84
    .end local v0    # "i":I
    :cond_0
    return-void
.end method

.method public init()I
    .locals 4

    .line 65
    const/4 v0, 0x0

    .line 66
    .local v0, "clearSize":I
    invoke-virtual {p0}, Lcom/miui/server/security/model/PackageBehaviorBean;->isEmpty()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 67
    return v0

    .line 69
    :cond_0
    const/4 v1, 0x0

    .local v1, "i":I
    :goto_0
    iget-object v2, p0, Lcom/miui/server/security/model/PackageBehaviorBean;->mBehaviors:Landroid/util/SparseArray;

    invoke-virtual {v2}, Landroid/util/SparseArray;->size()I

    move-result v2

    if-ge v1, v2, :cond_2

    .line 70
    iget-object v2, p0, Lcom/miui/server/security/model/PackageBehaviorBean;->mBehaviors:Landroid/util/SparseArray;

    invoke-virtual {v2, v1}, Landroid/util/SparseArray;->valueAt(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/miui/server/security/model/PackageBehaviorBean$BehaviorData;

    .line 71
    .local v2, "behaviorData":Lcom/miui/server/security/model/PackageBehaviorBean$BehaviorData;
    if-eqz v2, :cond_1

    .line 72
    invoke-virtual {v2}, Lcom/miui/server/security/model/PackageBehaviorBean$BehaviorData;->init()I

    move-result v3

    add-int/2addr v0, v3

    .line 69
    .end local v2    # "behaviorData":Lcom/miui/server/security/model/PackageBehaviorBean$BehaviorData;
    :cond_1
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 75
    .end local v1    # "i":I
    :cond_2
    return v0
.end method

.method public isEmpty()Z
    .locals 1

    .line 36
    iget-object v0, p0, Lcom/miui/server/security/model/PackageBehaviorBean;->mBehaviors:Landroid/util/SparseArray;

    invoke-virtual {v0}, Landroid/util/SparseArray;->size()I

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    :goto_0
    return v0
.end method

.method public recordAppBehavior(ILjava/lang/String;J)Z
    .locals 2
    .param p1, "behavior"    # I
    .param p2, "stringValue"    # Ljava/lang/String;
    .param p3, "longValue"    # J

    .line 23
    iget-object v0, p0, Lcom/miui/server/security/model/PackageBehaviorBean;->mBehaviors:Landroid/util/SparseArray;

    invoke-virtual {v0, p1}, Landroid/util/SparseArray;->contains(I)Z

    move-result v0

    if-nez v0, :cond_0

    .line 24
    new-instance v0, Lcom/miui/server/security/model/PackageBehaviorBean$BehaviorData;

    const/4 v1, 0x0

    invoke-direct {v0, v1}, Lcom/miui/server/security/model/PackageBehaviorBean$BehaviorData;-><init>(Lcom/miui/server/security/model/PackageBehaviorBean$BehaviorData-IA;)V

    .line 25
    .local v0, "behaviorData":Lcom/miui/server/security/model/PackageBehaviorBean$BehaviorData;
    invoke-virtual {v0}, Lcom/miui/server/security/model/PackageBehaviorBean$BehaviorData;->init()I

    .line 26
    iget-object v1, p0, Lcom/miui/server/security/model/PackageBehaviorBean;->mBehaviors:Landroid/util/SparseArray;

    invoke-virtual {v1, p1, v0}, Landroid/util/SparseArray;->put(ILjava/lang/Object;)V

    .line 28
    .end local v0    # "behaviorData":Lcom/miui/server/security/model/PackageBehaviorBean$BehaviorData;
    :cond_0
    invoke-static {p2}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_1

    .line 29
    iget-object v0, p0, Lcom/miui/server/security/model/PackageBehaviorBean;->mBehaviors:Landroid/util/SparseArray;

    invoke-virtual {v0, p1}, Landroid/util/SparseArray;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/miui/server/security/model/PackageBehaviorBean$BehaviorData;

    invoke-virtual {v0, p2}, Lcom/miui/server/security/model/PackageBehaviorBean$BehaviorData;->appendList(Ljava/lang/String;)Z

    move-result v0

    return v0

    .line 31
    :cond_1
    iget-object v0, p0, Lcom/miui/server/security/model/PackageBehaviorBean;->mBehaviors:Landroid/util/SparseArray;

    invoke-virtual {v0, p1}, Landroid/util/SparseArray;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/miui/server/security/model/PackageBehaviorBean$BehaviorData;

    invoke-virtual {v0, p3, p4}, Lcom/miui/server/security/model/PackageBehaviorBean$BehaviorData;->appendData(J)Z

    move-result v0

    return v0
.end method

.method public toJson()Lorg/json/JSONObject;
    .locals 8
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lorg/json/JSONException;
        }
    .end annotation

    .line 40
    invoke-virtual {p0}, Lcom/miui/server/security/model/PackageBehaviorBean;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 41
    const/4 v0, 0x0

    return-object v0

    .line 43
    :cond_0
    new-instance v0, Lorg/json/JSONObject;

    invoke-direct {v0}, Lorg/json/JSONObject;-><init>()V

    .line 44
    .local v0, "packageWithBehaviors":Lorg/json/JSONObject;
    const/4 v1, 0x0

    .local v1, "i":I
    :goto_0
    iget-object v2, p0, Lcom/miui/server/security/model/PackageBehaviorBean;->mBehaviors:Landroid/util/SparseArray;

    invoke-virtual {v2}, Landroid/util/SparseArray;->size()I

    move-result v2

    if-ge v1, v2, :cond_4

    .line 45
    iget-object v2, p0, Lcom/miui/server/security/model/PackageBehaviorBean;->mBehaviors:Landroid/util/SparseArray;

    invoke-virtual {v2, v1}, Landroid/util/SparseArray;->keyAt(I)I

    move-result v2

    .line 46
    .local v2, "recordBehavior":I
    iget-object v3, p0, Lcom/miui/server/security/model/PackageBehaviorBean;->mBehaviors:Landroid/util/SparseArray;

    invoke-virtual {v3, v1}, Landroid/util/SparseArray;->valueAt(I)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/miui/server/security/model/PackageBehaviorBean$BehaviorData;

    .line 47
    .local v3, "behaviorData":Lcom/miui/server/security/model/PackageBehaviorBean$BehaviorData;
    if-eqz v3, :cond_3

    .line 48
    invoke-static {v3}, Lcom/miui/server/security/model/PackageBehaviorBean$BehaviorData;->-$$Nest$fgetstringIntegerArrayMap(Lcom/miui/server/security/model/PackageBehaviorBean$BehaviorData;)Landroid/util/ArrayMap;

    move-result-object v4

    if-eqz v4, :cond_2

    invoke-static {v3}, Lcom/miui/server/security/model/PackageBehaviorBean$BehaviorData;->-$$Nest$fgetstringIntegerArrayMap(Lcom/miui/server/security/model/PackageBehaviorBean$BehaviorData;)Landroid/util/ArrayMap;

    move-result-object v4

    invoke-virtual {v4}, Landroid/util/ArrayMap;->size()I

    move-result v4

    if-lez v4, :cond_2

    .line 49
    invoke-static {v3}, Lcom/miui/server/security/model/PackageBehaviorBean$BehaviorData;->-$$Nest$fgetstringIntegerArrayMap(Lcom/miui/server/security/model/PackageBehaviorBean$BehaviorData;)Landroid/util/ArrayMap;

    move-result-object v4

    invoke-virtual {v4}, Landroid/util/ArrayMap;->size()I

    move-result v4

    new-array v4, v4, [Ljava/lang/String;

    .line 50
    .local v4, "spliceData":[Ljava/lang/String;
    const/4 v5, 0x0

    .local v5, "k":I
    :goto_1
    invoke-static {v3}, Lcom/miui/server/security/model/PackageBehaviorBean$BehaviorData;->-$$Nest$fgetstringIntegerArrayMap(Lcom/miui/server/security/model/PackageBehaviorBean$BehaviorData;)Landroid/util/ArrayMap;

    move-result-object v6

    invoke-virtual {v6}, Landroid/util/ArrayMap;->size()I

    move-result v6

    if-ge v5, v6, :cond_1

    .line 51
    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    invoke-static {v3}, Lcom/miui/server/security/model/PackageBehaviorBean$BehaviorData;->-$$Nest$fgetstringIntegerArrayMap(Lcom/miui/server/security/model/PackageBehaviorBean$BehaviorData;)Landroid/util/ArrayMap;

    move-result-object v7

    invoke-virtual {v7, v5}, Landroid/util/ArrayMap;->keyAt(I)Ljava/lang/Object;

    move-result-object v7

    check-cast v7, Ljava/lang/String;

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    const-string v7, "@"

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-static {v3}, Lcom/miui/server/security/model/PackageBehaviorBean$BehaviorData;->-$$Nest$fgetstringIntegerArrayMap(Lcom/miui/server/security/model/PackageBehaviorBean$BehaviorData;)Landroid/util/ArrayMap;

    move-result-object v7

    invoke-virtual {v7, v5}, Landroid/util/ArrayMap;->valueAt(I)Ljava/lang/Object;

    move-result-object v7

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    aput-object v6, v4, v5

    .line 50
    add-int/lit8 v5, v5, 0x1

    goto :goto_1

    .line 53
    .end local v5    # "k":I
    :cond_1
    new-instance v5, Lorg/json/JSONArray;

    invoke-direct {v5, v4}, Lorg/json/JSONArray;-><init>(Ljava/lang/Object;)V

    .line 54
    .local v5, "jsonArray":Lorg/json/JSONArray;
    invoke-static {v2}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v0, v6, v5}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;

    .line 56
    .end local v4    # "spliceData":[Ljava/lang/String;
    .end local v5    # "jsonArray":Lorg/json/JSONArray;
    :cond_2
    invoke-static {v3}, Lcom/miui/server/security/model/PackageBehaviorBean$BehaviorData;->-$$Nest$fgetdata(Lcom/miui/server/security/model/PackageBehaviorBean$BehaviorData;)J

    move-result-wide v4

    const-wide/16 v6, 0x0

    cmp-long v4, v4, v6

    if-lez v4, :cond_3

    .line 57
    invoke-static {v2}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v4

    invoke-static {v3}, Lcom/miui/server/security/model/PackageBehaviorBean$BehaviorData;->-$$Nest$fgetdata(Lcom/miui/server/security/model/PackageBehaviorBean$BehaviorData;)J

    move-result-wide v5

    invoke-virtual {v0, v4, v5, v6}, Lorg/json/JSONObject;->put(Ljava/lang/String;J)Lorg/json/JSONObject;

    .line 44
    .end local v2    # "recordBehavior":I
    .end local v3    # "behaviorData":Lcom/miui/server/security/model/PackageBehaviorBean$BehaviorData;
    :cond_3
    add-int/lit8 v1, v1, 0x1

    goto/16 :goto_0

    .line 61
    .end local v1    # "i":I
    :cond_4
    return-object v0
.end method
