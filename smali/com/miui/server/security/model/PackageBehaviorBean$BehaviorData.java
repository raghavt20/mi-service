class com.miui.server.security.model.PackageBehaviorBean$BehaviorData {
	 /* .source "PackageBehaviorBean.java" */
	 /* # annotations */
	 /* .annotation system Ldalvik/annotation/EnclosingClass; */
	 /* value = Lcom/miui/server/security/model/PackageBehaviorBean; */
} // .end annotation
/* .annotation system Ldalvik/annotation/InnerClass; */
/* accessFlags = 0xa */
/* name = "BehaviorData" */
} // .end annotation
/* # instance fields */
private Long data;
private android.util.ArrayMap stringIntegerArrayMap;
/* .annotation system Ldalvik/annotation/Signature; */
/* value = { */
/* "Landroid/util/ArrayMap<", */
/* "Ljava/lang/String;", */
/* "Ljava/lang/Integer;", */
/* ">;" */
/* } */
} // .end annotation
} // .end field
/* # direct methods */
static Long -$$Nest$fgetdata ( com.miui.server.security.model.PackageBehaviorBean$BehaviorData p0 ) { //bridge//synthethic
/* .locals 2 */
/* iget-wide v0, p0, Lcom/miui/server/security/model/PackageBehaviorBean$BehaviorData;->data:J */
/* return-wide v0 */
} // .end method
static android.util.ArrayMap -$$Nest$fgetstringIntegerArrayMap ( com.miui.server.security.model.PackageBehaviorBean$BehaviorData p0 ) { //bridge//synthethic
/* .locals 0 */
p0 = this.stringIntegerArrayMap;
} // .end method
private com.miui.server.security.model.PackageBehaviorBean$BehaviorData ( ) {
/* .locals 0 */
/* .line 86 */
/* invoke-direct {p0}, Ljava/lang/Object;-><init>()V */
return;
} // .end method
 com.miui.server.security.model.PackageBehaviorBean$BehaviorData ( ) { //synthethic
/* .locals 0 */
/* invoke-direct {p0}, Lcom/miui/server/security/model/PackageBehaviorBean$BehaviorData;-><init>()V */
return;
} // .end method
/* # virtual methods */
public Boolean appendData ( Long p0 ) {
/* .locals 7 */
/* .param p1, "val" # J */
/* .line 120 */
/* const-wide/16 v0, 0x0 */
/* cmp-long v2, p1, v0 */
/* if-gtz v2, :cond_0 */
/* .line 121 */
int v0 = 0; // const/4 v0, 0x0
/* .line 123 */
} // :cond_0
int v2 = 0; // const/4 v2, 0x0
/* .line 124 */
/* .local v2, "addCount":Z */
/* iget-wide v3, p0, Lcom/miui/server/security/model/PackageBehaviorBean$BehaviorData;->data:J */
/* const-wide/16 v5, -0x1 */
/* cmp-long v3, v3, v5 */
/* if-nez v3, :cond_1 */
/* .line 125 */
/* iput-wide v0, p0, Lcom/miui/server/security/model/PackageBehaviorBean$BehaviorData;->data:J */
/* .line 126 */
int v2 = 1; // const/4 v2, 0x1
/* .line 128 */
} // :cond_1
/* iget-wide v0, p0, Lcom/miui/server/security/model/PackageBehaviorBean$BehaviorData;->data:J */
/* add-long/2addr v0, p1 */
/* iput-wide v0, p0, Lcom/miui/server/security/model/PackageBehaviorBean$BehaviorData;->data:J */
/* .line 129 */
} // .end method
public Boolean appendList ( java.lang.String p0 ) {
/* .locals 4 */
/* .param p1, "val" # Ljava/lang/String; */
/* .line 104 */
v0 = android.text.TextUtils .isEmpty ( p1 );
int v1 = 0; // const/4 v1, 0x0
if ( v0 != null) { // if-eqz v0, :cond_0
/* .line 105 */
/* .line 107 */
} // :cond_0
v0 = this.stringIntegerArrayMap;
/* if-nez v0, :cond_1 */
/* .line 108 */
/* new-instance v0, Landroid/util/ArrayMap; */
/* invoke-direct {v0}, Landroid/util/ArrayMap;-><init>()V */
this.stringIntegerArrayMap = v0;
/* .line 110 */
} // :cond_1
v0 = this.stringIntegerArrayMap;
v0 = (( android.util.ArrayMap ) v0 ).containsKey ( p1 ); // invoke-virtual {v0, p1}, Landroid/util/ArrayMap;->containsKey(Ljava/lang/Object;)Z
int v2 = 1; // const/4 v2, 0x1
if ( v0 != null) { // if-eqz v0, :cond_2
/* .line 111 */
v0 = this.stringIntegerArrayMap;
java.lang.Integer .valueOf ( v2 );
(( android.util.ArrayMap ) v0 ).getOrDefault ( p1, v3 ); // invoke-virtual {v0, p1, v3}, Landroid/util/ArrayMap;->getOrDefault(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;
/* check-cast v0, Ljava/lang/Integer; */
v0 = (( java.lang.Integer ) v0 ).intValue ( ); // invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I
/* .line 112 */
/* .local v0, "curCount":I */
v3 = this.stringIntegerArrayMap;
/* add-int/2addr v0, v2 */
java.lang.Integer .valueOf ( v0 );
(( android.util.ArrayMap ) v3 ).put ( p1, v2 ); // invoke-virtual {v3, p1, v2}, Landroid/util/ArrayMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;
/* .line 113 */
/* .line 115 */
} // .end local v0 # "curCount":I
} // :cond_2
v0 = this.stringIntegerArrayMap;
java.lang.Integer .valueOf ( v2 );
(( android.util.ArrayMap ) v0 ).put ( p1, v1 ); // invoke-virtual {v0, p1, v1}, Landroid/util/ArrayMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;
/* .line 116 */
} // .end method
public void dump ( java.io.PrintWriter p0 ) {
/* .locals 4 */
/* .param p1, "pw" # Ljava/io/PrintWriter; */
/* .line 133 */
v0 = this.stringIntegerArrayMap;
if ( v0 != null) { // if-eqz v0, :cond_0
v0 = (( android.util.ArrayMap ) v0 ).size ( ); // invoke-virtual {v0}, Landroid/util/ArrayMap;->size()I
/* if-lez v0, :cond_0 */
/* .line 134 */
v0 = this.stringIntegerArrayMap;
(( android.util.ArrayMap ) v0 ).keySet ( ); // invoke-virtual {v0}, Landroid/util/ArrayMap;->keySet()Ljava/util/Set;
v1 = } // :goto_0
if ( v1 != null) { // if-eqz v1, :cond_0
/* check-cast v1, Ljava/lang/String; */
/* .line 135 */
/* .local v1, "tmpString":Ljava/lang/String; */
/* new-instance v2, Ljava/lang/StringBuilder; */
/* invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V */
final String v3 = " value:"; // const-string v3, " value:"
(( java.lang.StringBuilder ) v2 ).append ( v3 ); // invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v2 ).append ( v1 ); // invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v2 ).toString ( ); // invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
(( java.io.PrintWriter ) p1 ).println ( v2 ); // invoke-virtual {p1, v2}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V
/* .line 136 */
/* new-instance v2, Ljava/lang/StringBuilder; */
/* invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V */
final String v3 = " count:"; // const-string v3, " count:"
(( java.lang.StringBuilder ) v2 ).append ( v3 ); // invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
v3 = this.stringIntegerArrayMap;
(( android.util.ArrayMap ) v3 ).get ( v1 ); // invoke-virtual {v3, v1}, Landroid/util/ArrayMap;->get(Ljava/lang/Object;)Ljava/lang/Object;
(( java.lang.StringBuilder ) v2 ).append ( v3 ); // invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v2 ).toString ( ); // invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
(( java.io.PrintWriter ) p1 ).println ( v2 ); // invoke-virtual {p1, v2}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V
/* .line 137 */
} // .end local v1 # "tmpString":Ljava/lang/String;
/* .line 139 */
} // :cond_0
/* iget-wide v0, p0, Lcom/miui/server/security/model/PackageBehaviorBean$BehaviorData;->data:J */
/* const-wide/16 v2, -0x1 */
/* cmp-long v0, v0, v2 */
if ( v0 != null) { // if-eqz v0, :cond_1
/* .line 140 */
/* new-instance v0, Ljava/lang/StringBuilder; */
/* invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V */
final String v1 = " data:"; // const-string v1, " data:"
(( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
/* iget-wide v1, p0, Lcom/miui/server/security/model/PackageBehaviorBean$BehaviorData;->data:J */
(( java.lang.StringBuilder ) v0 ).append ( v1, v2 ); // invoke-virtual {v0, v1, v2}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v0 ).toString ( ); // invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
(( java.io.PrintWriter ) p1 ).println ( v0 ); // invoke-virtual {p1, v0}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V
/* .line 142 */
} // :cond_1
return;
} // .end method
public Integer init ( ) {
/* .locals 5 */
/* .line 91 */
int v0 = 0; // const/4 v0, 0x0
/* .line 92 */
/* .local v0, "clearSize":I */
v1 = this.stringIntegerArrayMap;
if ( v1 != null) { // if-eqz v1, :cond_0
v1 = (( android.util.ArrayMap ) v1 ).size ( ); // invoke-virtual {v1}, Landroid/util/ArrayMap;->size()I
/* if-lez v1, :cond_0 */
/* .line 93 */
v1 = this.stringIntegerArrayMap;
v0 = (( android.util.ArrayMap ) v1 ).size ( ); // invoke-virtual {v1}, Landroid/util/ArrayMap;->size()I
/* .line 94 */
v1 = this.stringIntegerArrayMap;
(( android.util.ArrayMap ) v1 ).clear ( ); // invoke-virtual {v1}, Landroid/util/ArrayMap;->clear()V
/* .line 96 */
} // :cond_0
/* iget-wide v1, p0, Lcom/miui/server/security/model/PackageBehaviorBean$BehaviorData;->data:J */
/* const-wide/16 v3, -0x1 */
/* cmp-long v1, v1, v3 */
if ( v1 != null) { // if-eqz v1, :cond_1
/* .line 97 */
/* add-int/lit8 v0, v0, 0x1 */
/* .line 98 */
/* iput-wide v3, p0, Lcom/miui/server/security/model/PackageBehaviorBean$BehaviorData;->data:J */
/* .line 100 */
} // :cond_1
} // .end method
