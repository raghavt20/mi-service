.class public Lcom/miui/server/security/DefaultBrowserImpl;
.super Ljava/lang/Object;
.source "DefaultBrowserImpl.java"


# static fields
.field private static final DEF_BROWSER_COUNT:Ljava/lang/String; = "miui.sec.defBrowser"

.field private static final ONE_MINUTE:J = 0xea60L

.field private static final PKG_BROWSER:Ljava/lang/String;

.field private static final TAG:Ljava/lang/String; = "DefaultBrowserImpl"


# instance fields
.field private final mContext:Landroid/content/Context;

.field private final mHandler:Landroid/os/Handler;


# direct methods
.method public static synthetic $r8$lambda$CeYczOnGWjfSbrQXETrQDhlbiN0(Lcom/miui/server/security/DefaultBrowserImpl;)V
    .locals 0

    invoke-direct {p0}, Lcom/miui/server/security/DefaultBrowserImpl;->lambda$resetDefaultBrowser$1()V

    return-void
.end method

.method public static synthetic $r8$lambda$FhVo5iOOWtgKNtMsTOS5PFA65hQ(Lcom/miui/server/security/DefaultBrowserImpl;Ljava/lang/String;)V
    .locals 0

    invoke-direct {p0, p1}, Lcom/miui/server/security/DefaultBrowserImpl;->lambda$checkDefaultBrowser$0(Ljava/lang/String;)V

    return-void
.end method

.method static constructor <clinit>()V
    .locals 1

    .line 31
    sget-boolean v0, Lmiui/os/Build;->IS_INTERNATIONAL_BUILD:Z

    if-eqz v0, :cond_0

    const-string v0, "com.mi.globalbrowser"

    goto :goto_0

    :cond_0
    const-string v0, "com.android.browser"

    :goto_0
    sput-object v0, Lcom/miui/server/security/DefaultBrowserImpl;->PKG_BROWSER:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>(Lcom/miui/server/SecurityManagerService;)V
    .locals 1
    .param p1, "service"    # Lcom/miui/server/SecurityManagerService;

    .line 38
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 39
    iget-object v0, p1, Lcom/miui/server/SecurityManagerService;->mContext:Landroid/content/Context;

    iput-object v0, p0, Lcom/miui/server/security/DefaultBrowserImpl;->mContext:Landroid/content/Context;

    .line 40
    iget-object v0, p1, Lcom/miui/server/SecurityManagerService;->mSecurityWriteHandler:Lcom/miui/server/security/SecurityWriteHandler;

    iput-object v0, p0, Lcom/miui/server/security/DefaultBrowserImpl;->mHandler:Landroid/os/Handler;

    .line 41
    return-void
.end method

.method private checkIntentFilterVerifications()V
    .locals 1

    .line 82
    const/4 v0, 0x0

    invoke-direct {p0, v0}, Lcom/miui/server/security/DefaultBrowserImpl;->checkIntentFilterVerifications(Ljava/lang/String;)V

    .line 83
    return-void
.end method

.method private checkIntentFilterVerifications(Ljava/lang/String;)V
    .locals 26
    .param p1, "packageName"    # Ljava/lang/String;

    .line 86
    move-object/from16 v1, p0

    move-object/from16 v2, p1

    const-string v0, "android.intent.action.VIEW"

    iget-object v3, v1, Lcom/miui/server/security/DefaultBrowserImpl;->mContext:Landroid/content/Context;

    invoke-virtual {v3}, Landroid/content/Context;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v3

    .line 89
    .local v3, "pm":Landroid/content/pm/PackageManager;
    const/4 v4, 0x0

    if-nez v2, :cond_0

    .line 90
    const/16 v5, 0x2000

    :try_start_0
    invoke-virtual {v3, v5}, Landroid/content/pm/PackageManager;->getInstalledPackages(I)Ljava/util/List;

    move-result-object v5

    .local v5, "applications":Ljava/util/List;, "Ljava/util/List<Landroid/content/pm/PackageInfo;>;"
    goto :goto_0

    .line 92
    .end local v5    # "applications":Ljava/util/List;, "Ljava/util/List<Landroid/content/pm/PackageInfo;>;"
    :cond_0
    invoke-virtual {v3, v2, v4}, Landroid/content/pm/PackageManager;->getPackageInfo(Ljava/lang/String;I)Landroid/content/pm/PackageInfo;

    move-result-object v5

    invoke-static {v5}, Ljava/util/Collections;->singletonList(Ljava/lang/Object;)Ljava/util/List;

    move-result-object v5

    .line 94
    .restart local v5    # "applications":Ljava/util/List;, "Ljava/util/List<Landroid/content/pm/PackageInfo;>;"
    :goto_0
    new-instance v6, Landroid/content/Intent;

    invoke-direct {v6, v0}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    const-string v7, "android.intent.category.BROWSABLE"

    .line 95
    invoke-virtual {v6, v7}, Landroid/content/Intent;->addCategory(Ljava/lang/String;)Landroid/content/Intent;

    move-result-object v6

    const-string v7, "http://"

    .line 96
    invoke-static {v7}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v7

    invoke-virtual {v6, v7}, Landroid/content/Intent;->setData(Landroid/net/Uri;)Landroid/content/Intent;

    move-result-object v6

    .line 97
    .local v6, "browserIntent":Landroid/content/Intent;
    new-instance v7, Landroid/content/Intent;

    invoke-direct {v7, v0}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    const-string v8, "http://www.xiaomi.com"

    .line 98
    invoke-static {v8}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v8

    invoke-virtual {v7, v8}, Landroid/content/Intent;->setData(Landroid/net/Uri;)Landroid/content/Intent;

    move-result-object v7

    .line 99
    .local v7, "httpIntent":Landroid/content/Intent;
    new-instance v8, Landroid/content/Intent;

    invoke-direct {v8, v0}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    const-string v9, "https://www.xiaomi.com"

    .line 100
    invoke-static {v9}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v9

    invoke-virtual {v8, v9}, Landroid/content/Intent;->setData(Landroid/net/Uri;)Landroid/content/Intent;

    move-result-object v8

    .line 101
    .local v8, "httpsIntent":Landroid/content/Intent;
    if-eqz v2, :cond_1

    .line 102
    invoke-virtual {v6, v2}, Landroid/content/Intent;->setPackage(Ljava/lang/String;)Landroid/content/Intent;

    .line 103
    invoke-virtual {v7, v2}, Landroid/content/Intent;->setPackage(Ljava/lang/String;)Landroid/content/Intent;

    .line 104
    invoke-virtual {v8, v2}, Landroid/content/Intent;->setPackage(Ljava/lang/String;)Landroid/content/Intent;

    .line 106
    :cond_1
    const/4 v9, 0x0

    .line 107
    .local v9, "userId":I
    const/4 v10, 0x1

    invoke-direct {v1, v3, v6, v10, v9}, Lcom/miui/server/security/DefaultBrowserImpl;->queryIntentPackages(Landroid/content/pm/PackageManager;Landroid/content/Intent;ZI)Ljava/util/Set;

    move-result-object v11

    .line 108
    .local v11, "browsers":Ljava/util/Set;, "Ljava/util/Set<Ljava/lang/String;>;"
    invoke-direct {v1, v3, v7, v4, v9}, Lcom/miui/server/security/DefaultBrowserImpl;->queryIntentPackages(Landroid/content/pm/PackageManager;Landroid/content/Intent;ZI)Ljava/util/Set;

    move-result-object v12

    .line 109
    .local v12, "httpPackages":Ljava/util/Set;, "Ljava/util/Set<Ljava/lang/String;>;"
    invoke-direct {v1, v3, v8, v4, v9}, Lcom/miui/server/security/DefaultBrowserImpl;->queryIntentPackages(Landroid/content/pm/PackageManager;Landroid/content/Intent;ZI)Ljava/util/Set;

    move-result-object v13

    .line 110
    .local v13, "httpsPackages":Ljava/util/Set;, "Ljava/util/Set<Ljava/lang/String;>;"
    invoke-interface {v12, v13}, Ljava/util/Set;->addAll(Ljava/util/Collection;)Z

    .line 112
    new-instance v14, Landroid/util/ArraySet;

    invoke-direct {v14}, Landroid/util/ArraySet;-><init>()V

    .line 113
    .local v14, "rejectPks":Landroid/util/ArraySet;, "Landroid/util/ArraySet<Ljava/lang/String;>;"
    invoke-interface {v5}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v15

    :goto_1
    invoke-interface {v15}, Ljava/util/Iterator;->hasNext()Z

    move-result v16

    if-eqz v16, :cond_13

    invoke-interface {v15}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v16

    check-cast v16, Landroid/content/pm/PackageInfo;

    move-object/from16 v17, v16

    .line 114
    .local v17, "info":Landroid/content/pm/PackageInfo;
    move-object/from16 v10, v17

    .end local v17    # "info":Landroid/content/pm/PackageInfo;
    .local v10, "info":Landroid/content/pm/PackageInfo;
    iget-object v4, v10, Landroid/content/pm/PackageInfo;->applicationInfo:Landroid/content/pm/ApplicationInfo;

    iget v4, v4, Landroid/content/pm/ApplicationInfo;->uid:I

    invoke-static {v4}, Landroid/os/UserHandle;->getAppId(I)I

    move-result v4

    const/16 v1, 0x2710

    if-lt v4, v1, :cond_12

    iget-object v1, v10, Landroid/content/pm/PackageInfo;->applicationInfo:Landroid/content/pm/ApplicationInfo;

    .line 115
    invoke-virtual {v1}, Landroid/content/pm/ApplicationInfo;->isSystemApp()Z

    move-result v1

    if-eqz v1, :cond_2

    .line 116
    const/4 v4, 0x0

    const/4 v10, 0x1

    move-object/from16 v1, p0

    goto :goto_1

    .line 118
    :cond_2
    iget-object v1, v10, Landroid/content/pm/PackageInfo;->applicationInfo:Landroid/content/pm/ApplicationInfo;

    iget-object v1, v1, Landroid/content/pm/ApplicationInfo;->packageName:Ljava/lang/String;

    .line 119
    .local v1, "pkg":Ljava/lang/String;
    invoke-interface {v11, v1}, Ljava/util/Set;->contains(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_3

    .line 120
    const/4 v4, 0x0

    const/4 v10, 0x1

    move-object/from16 v1, p0

    goto :goto_1

    .line 122
    :cond_3
    invoke-interface {v12, v1}, Ljava/util/Set;->contains(Ljava/lang/Object;)Z

    move-result v4

    if-nez v4, :cond_4

    .line 123
    const/4 v4, 0x0

    const/4 v10, 0x1

    move-object/from16 v1, p0

    goto :goto_1

    .line 125
    :cond_4
    invoke-virtual {v3, v1}, Landroid/content/pm/PackageManager;->getAllIntentFilters(Ljava/lang/String;)Ljava/util/List;

    move-result-object v4

    .line 126
    .local v4, "filters":Ljava/util/List;, "Ljava/util/List<Landroid/content/IntentFilter;>;"
    const/16 v18, 0x0

    .line 127
    .local v18, "add":Z
    if-eqz v4, :cond_e

    invoke-interface {v4}, Ljava/util/List;->size()I

    move-result v19

    if-lez v19, :cond_e

    .line 128
    invoke-interface {v4}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v19

    :goto_2
    invoke-interface/range {v19 .. v19}, Ljava/util/Iterator;->hasNext()Z

    move-result v20

    if-eqz v20, :cond_d

    invoke-interface/range {v19 .. v19}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v20

    check-cast v20, Landroid/content/IntentFilter;

    move-object/from16 v21, v20

    .line 129
    .local v21, "filter":Landroid/content/IntentFilter;
    move-object/from16 v2, v21

    .end local v21    # "filter":Landroid/content/IntentFilter;
    .local v2, "filter":Landroid/content/IntentFilter;
    invoke-virtual {v2, v0}, Landroid/content/IntentFilter;->hasAction(Ljava/lang/String;)Z

    move-result v20

    if-nez v20, :cond_5

    .line 130
    move-object/from16 v2, p1

    goto :goto_2

    .line 132
    :cond_5
    move-object/from16 v20, v0

    const-string v0, "http"

    invoke-virtual {v2, v0}, Landroid/content/IntentFilter;->hasDataScheme(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_7

    const-string v0, "https"

    .line 133
    invoke-virtual {v2, v0}, Landroid/content/IntentFilter;->hasDataScheme(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_6

    goto :goto_3

    :cond_6
    move-object/from16 v21, v4

    goto :goto_7

    .line 134
    :cond_7
    :goto_3
    invoke-virtual {v2}, Landroid/content/IntentFilter;->getHostsList()Ljava/util/ArrayList;

    move-result-object v0

    .line 135
    .local v0, "hostList":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/String;>;"
    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v21

    if-eqz v21, :cond_8

    move-object/from16 v21, v4

    .end local v4    # "filters":Ljava/util/List;, "Ljava/util/List<Landroid/content/IntentFilter;>;"
    .local v21, "filters":Ljava/util/List;, "Ljava/util/List<Landroid/content/IntentFilter;>;"
    const-string v4, "*"

    invoke-virtual {v0, v4}, Ljava/util/ArrayList;->contains(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_c

    goto :goto_4

    .end local v21    # "filters":Ljava/util/List;, "Ljava/util/List<Landroid/content/IntentFilter;>;"
    .restart local v4    # "filters":Ljava/util/List;, "Ljava/util/List<Landroid/content/IntentFilter;>;"
    :cond_8
    move-object/from16 v21, v4

    .line 136
    .end local v4    # "filters":Ljava/util/List;, "Ljava/util/List<Landroid/content/IntentFilter;>;"
    .restart local v21    # "filters":Ljava/util/List;, "Ljava/util/List<Landroid/content/IntentFilter;>;"
    :goto_4
    invoke-virtual {v2}, Landroid/content/IntentFilter;->countDataPaths()I

    move-result v4

    .line 137
    .local v4, "dataPathsCount":I
    if-lez v4, :cond_b

    .line 138
    const/16 v22, 0x0

    move-object/from16 v23, v0

    move/from16 v0, v22

    .local v0, "i":I
    .local v23, "hostList":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/String;>;"
    :goto_5
    if-ge v0, v4, :cond_a

    .line 139
    move/from16 v22, v4

    .end local v4    # "dataPathsCount":I
    .local v22, "dataPathsCount":I
    const-string v4, ".*"

    invoke-virtual {v2, v0}, Landroid/content/IntentFilter;->getDataPath(I)Landroid/os/PatternMatcher;

    move-result-object v24

    move-object/from16 v25, v2

    .end local v2    # "filter":Landroid/content/IntentFilter;
    .local v25, "filter":Landroid/content/IntentFilter;
    invoke-virtual/range {v24 .. v24}, Landroid/os/PatternMatcher;->getPath()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v4, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_9

    .line 140
    const/4 v2, 0x1

    .line 141
    .end local v18    # "add":Z
    .local v2, "add":Z
    move/from16 v18, v2

    goto :goto_6

    .line 138
    .end local v2    # "add":Z
    .restart local v18    # "add":Z
    :cond_9
    add-int/lit8 v0, v0, 0x1

    move/from16 v4, v22

    move-object/from16 v2, v25

    goto :goto_5

    .end local v22    # "dataPathsCount":I
    .end local v25    # "filter":Landroid/content/IntentFilter;
    .local v2, "filter":Landroid/content/IntentFilter;
    .restart local v4    # "dataPathsCount":I
    :cond_a
    move-object/from16 v25, v2

    move/from16 v22, v4

    .end local v0    # "i":I
    .end local v2    # "filter":Landroid/content/IntentFilter;
    .end local v4    # "dataPathsCount":I
    .restart local v22    # "dataPathsCount":I
    .restart local v25    # "filter":Landroid/content/IntentFilter;
    :goto_6
    goto :goto_7

    .line 145
    .end local v22    # "dataPathsCount":I
    .end local v23    # "hostList":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/String;>;"
    .end local v25    # "filter":Landroid/content/IntentFilter;
    .local v0, "hostList":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/String;>;"
    .restart local v2    # "filter":Landroid/content/IntentFilter;
    .restart local v4    # "dataPathsCount":I
    :cond_b
    move-object/from16 v23, v0

    move-object/from16 v25, v2

    move/from16 v22, v4

    .end local v0    # "hostList":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/String;>;"
    .end local v2    # "filter":Landroid/content/IntentFilter;
    .end local v4    # "dataPathsCount":I
    .restart local v22    # "dataPathsCount":I
    .restart local v23    # "hostList":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/String;>;"
    .restart local v25    # "filter":Landroid/content/IntentFilter;
    const/16 v18, 0x1

    .line 149
    .end local v22    # "dataPathsCount":I
    .end local v23    # "hostList":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/String;>;"
    .end local v25    # "filter":Landroid/content/IntentFilter;
    :cond_c
    :goto_7
    move-object/from16 v2, p1

    move-object/from16 v0, v20

    move-object/from16 v4, v21

    goto/16 :goto_2

    .line 128
    .end local v21    # "filters":Ljava/util/List;, "Ljava/util/List<Landroid/content/IntentFilter;>;"
    .local v4, "filters":Ljava/util/List;, "Ljava/util/List<Landroid/content/IntentFilter;>;"
    :cond_d
    move-object/from16 v20, v0

    move-object/from16 v21, v4

    .end local v4    # "filters":Ljava/util/List;, "Ljava/util/List<Landroid/content/IntentFilter;>;"
    .restart local v21    # "filters":Ljava/util/List;, "Ljava/util/List<Landroid/content/IntentFilter;>;"
    goto :goto_8

    .line 127
    .end local v21    # "filters":Ljava/util/List;, "Ljava/util/List<Landroid/content/IntentFilter;>;"
    .restart local v4    # "filters":Ljava/util/List;, "Ljava/util/List<Landroid/content/IntentFilter;>;"
    :cond_e
    move-object/from16 v20, v0

    move-object/from16 v21, v4

    .line 152
    .end local v4    # "filters":Ljava/util/List;, "Ljava/util/List<Landroid/content/IntentFilter;>;"
    .restart local v21    # "filters":Ljava/util/List;, "Ljava/util/List<Landroid/content/IntentFilter;>;"
    :goto_8
    if-eqz v18, :cond_10

    .line 153
    const/4 v0, 0x0

    invoke-virtual {v3, v1, v0}, Landroid/content/pm/PackageManager;->getIntentVerificationStatusAsUser(Ljava/lang/String;I)I

    move-result v2

    .line 154
    .local v2, "status":I
    if-eqz v2, :cond_f

    const/4 v4, 0x1

    if-ne v2, v4, :cond_11

    goto :goto_9

    :cond_f
    const/4 v4, 0x1

    .line 156
    :goto_9
    invoke-virtual {v14, v1}, Landroid/util/ArraySet;->add(Ljava/lang/Object;)Z

    goto :goto_a

    .line 152
    .end local v2    # "status":I
    :cond_10
    const/4 v0, 0x0

    const/4 v4, 0x1

    .line 159
    .end local v1    # "pkg":Ljava/lang/String;
    .end local v10    # "info":Landroid/content/pm/PackageInfo;
    .end local v18    # "add":Z
    .end local v21    # "filters":Ljava/util/List;, "Ljava/util/List<Landroid/content/IntentFilter;>;"
    :cond_11
    :goto_a
    move-object/from16 v1, p0

    move-object/from16 v2, p1

    move v10, v4

    move v4, v0

    move-object/from16 v0, v20

    goto/16 :goto_1

    .line 114
    .restart local v10    # "info":Landroid/content/pm/PackageInfo;
    :cond_12
    move-object/from16 v20, v0

    const/4 v0, 0x0

    const/4 v4, 0x1

    move-object/from16 v1, p0

    move-object/from16 v2, p1

    move v10, v4

    move v4, v0

    move-object/from16 v0, v20

    goto/16 :goto_1

    .line 160
    .end local v10    # "info":Landroid/content/pm/PackageInfo;
    :cond_13
    invoke-virtual {v14}, Landroid/util/ArraySet;->iterator()Ljava/util/Iterator;

    move-result-object v0

    :goto_b
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_14

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/String;

    .line 161
    .restart local v1    # "pkg":Ljava/lang/String;
    const/4 v2, 0x3

    invoke-virtual {v3, v1, v2, v9}, Landroid/content/pm/PackageManager;->updateIntentVerificationStatusAsUser(Ljava/lang/String;II)Z
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 162
    nop

    .end local v1    # "pkg":Ljava/lang/String;
    goto :goto_b

    .line 165
    .end local v5    # "applications":Ljava/util/List;, "Ljava/util/List<Landroid/content/pm/PackageInfo;>;"
    .end local v6    # "browserIntent":Landroid/content/Intent;
    .end local v7    # "httpIntent":Landroid/content/Intent;
    .end local v8    # "httpsIntent":Landroid/content/Intent;
    .end local v9    # "userId":I
    .end local v11    # "browsers":Ljava/util/Set;, "Ljava/util/Set<Ljava/lang/String;>;"
    .end local v12    # "httpPackages":Ljava/util/Set;, "Ljava/util/Set<Ljava/lang/String;>;"
    .end local v13    # "httpsPackages":Ljava/util/Set;, "Ljava/util/Set<Ljava/lang/String;>;"
    .end local v14    # "rejectPks":Landroid/util/ArraySet;, "Landroid/util/ArraySet<Ljava/lang/String;>;"
    :cond_14
    goto :goto_c

    .line 163
    :catch_0
    move-exception v0

    .line 164
    .local v0, "e":Ljava/lang/Exception;
    invoke-virtual {v0}, Ljava/lang/Exception;->printStackTrace()V

    .line 166
    .end local v0    # "e":Ljava/lang/Exception;
    :goto_c
    return-void
.end method

.method public static isDefaultBrowser(Landroid/content/Context;Ljava/lang/String;)Z
    .locals 6
    .param p0, "context"    # Landroid/content/Context;
    .param p1, "packageName"    # Ljava/lang/String;

    .line 200
    const-string v0, "DefaultBrowserImpl"

    invoke-static {}, Landroid/os/Binder;->clearCallingIdentity()J

    move-result-wide v1

    .line 202
    .local v1, "identity":J
    nop

    .line 203
    :try_start_0
    invoke-virtual {p0}, Landroid/content/Context;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v3

    .line 204
    invoke-static {}, Landroid/os/UserHandle;->myUserId()I

    move-result v4

    .line 203
    invoke-virtual {v3, v4}, Landroid/content/pm/PackageManager;->getDefaultBrowserPackageNameAsUser(I)Ljava/lang/String;

    move-result-object v3

    .line 205
    .local v3, "defaultBrowserPackage":Ljava/lang/String;
    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "isDefaultBrowser: packageName= "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, " defaultBrowserPackage= "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v0, v4}, Landroid/util/Slog;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 206
    invoke-static {p1, v3}, Landroid/text/TextUtils;->equals(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Z

    move-result v0
    :try_end_0
    .catch Ljava/lang/SecurityException; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 210
    invoke-static {v1, v2}, Landroid/os/Binder;->restoreCallingIdentity(J)V

    .line 206
    return v0

    .line 210
    .end local v3    # "defaultBrowserPackage":Ljava/lang/String;
    :catchall_0
    move-exception v0

    goto :goto_0

    .line 207
    :catch_0
    move-exception v3

    .line 208
    .local v3, "exception":Ljava/lang/SecurityException;
    :try_start_1
    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "isDefaultBrowser failed: "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v0, v4}, Landroid/util/Slog;->d(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 210
    nop

    .end local v3    # "exception":Ljava/lang/SecurityException;
    invoke-static {v1, v2}, Landroid/os/Binder;->restoreCallingIdentity(J)V

    .line 211
    nop

    .line 212
    const/4 v0, 0x0

    return v0

    .line 210
    :goto_0
    invoke-static {v1, v2}, Landroid/os/Binder;->restoreCallingIdentity(J)V

    .line 211
    throw v0
.end method

.method private synthetic lambda$checkDefaultBrowser$0(Ljava/lang/String;)V
    .locals 5
    .param p1, "packageName"    # Ljava/lang/String;

    .line 49
    const-string v0, "miui.sec.defBrowser"

    iget-object v1, p0, Lcom/miui/server/security/DefaultBrowserImpl;->mContext:Landroid/content/Context;

    invoke-virtual {v1}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v1

    .line 51
    .local v1, "cr":Landroid/content/ContentResolver;
    :try_start_0
    invoke-direct {p0, p1}, Lcom/miui/server/security/DefaultBrowserImpl;->checkIntentFilterVerifications(Ljava/lang/String;)V

    .line 52
    const/4 v2, -0x1

    invoke-static {v1, v0, v2}, Landroid/provider/Settings$Secure;->getInt(Landroid/content/ContentResolver;Ljava/lang/String;I)I

    move-result v2

    .line 53
    .local v2, "defBrowserCount":I
    const/4 v3, 0x1

    .line 54
    .local v3, "allow":Z
    const/16 v4, 0xa

    if-lt v2, v4, :cond_0

    const/16 v4, 0x64

    if-ge v2, v4, :cond_0

    .line 55
    add-int/lit8 v4, v2, 0x1

    invoke-static {v1, v0, v4}, Landroid/provider/Settings$Secure;->putInt(Landroid/content/ContentResolver;Ljava/lang/String;I)Z

    .line 56
    const/4 v3, 0x0

    .line 58
    :cond_0
    if-eqz v3, :cond_1

    .line 59
    iget-object v0, p0, Lcom/miui/server/security/DefaultBrowserImpl;->mContext:Landroid/content/Context;

    invoke-static {v0}, Lcom/miui/server/security/DefaultBrowserImpl;->setDefaultBrowser(Landroid/content/Context;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 63
    .end local v2    # "defBrowserCount":I
    .end local v3    # "allow":Z
    :cond_1
    goto :goto_0

    .line 61
    :catch_0
    move-exception v0

    .line 62
    .local v0, "e":Ljava/lang/Exception;
    const-string v2, "DefaultBrowserImpl"

    const-string v3, "checkDefaultBrowser"

    invoke-static {v2, v3, v0}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    .line 64
    .end local v0    # "e":Ljava/lang/Exception;
    :goto_0
    return-void
.end method

.method private synthetic lambda$resetDefaultBrowser$1()V
    .locals 3

    .line 73
    :try_start_0
    invoke-direct {p0}, Lcom/miui/server/security/DefaultBrowserImpl;->checkIntentFilterVerifications()V

    .line 74
    iget-object v0, p0, Lcom/miui/server/security/DefaultBrowserImpl;->mContext:Landroid/content/Context;

    invoke-static {v0}, Lcom/miui/server/security/DefaultBrowserImpl;->setDefaultBrowser(Landroid/content/Context;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 77
    goto :goto_0

    .line 75
    :catch_0
    move-exception v0

    .line 76
    .local v0, "e":Ljava/lang/Exception;
    const-string v1, "DefaultBrowserImpl"

    const-string v2, "resetDefaultBrowser exception"

    invoke-static {v1, v2, v0}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    .line 78
    .end local v0    # "e":Ljava/lang/Exception;
    :goto_0
    return-void
.end method

.method private queryIntentPackages(Landroid/content/pm/PackageManager;Landroid/content/Intent;ZI)Ljava/util/Set;
    .locals 7
    .param p1, "pm"    # Landroid/content/pm/PackageManager;
    .param p2, "intent"    # Landroid/content/Intent;
    .param p3, "allweb"    # Z
    .param p4, "userId"    # I
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/pm/PackageManager;",
            "Landroid/content/Intent;",
            "ZI)",
            "Ljava/util/Set<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .line 182
    const/high16 v0, 0x20000

    invoke-virtual {p1, p2, v0, p4}, Landroid/content/pm/PackageManager;->queryIntentActivitiesAsUser(Landroid/content/Intent;II)Ljava/util/List;

    move-result-object v0

    .line 183
    .local v0, "list":Ljava/util/List;, "Ljava/util/List<Landroid/content/pm/ResolveInfo;>;"
    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v1

    .line 184
    .local v1, "count":I
    new-instance v2, Landroid/util/ArraySet;

    invoke-direct {v2}, Landroid/util/ArraySet;-><init>()V

    .line 185
    .local v2, "packages":Ljava/util/Set;, "Ljava/util/Set<Ljava/lang/String;>;"
    const/4 v3, 0x0

    .local v3, "i":I
    :goto_0
    if-ge v3, v1, :cond_3

    .line 186
    invoke-interface {v0, v3}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Landroid/content/pm/ResolveInfo;

    .line 187
    .local v4, "info":Landroid/content/pm/ResolveInfo;
    iget-object v5, v4, Landroid/content/pm/ResolveInfo;->activityInfo:Landroid/content/pm/ActivityInfo;

    if-eqz v5, :cond_2

    if-eqz p3, :cond_0

    iget-boolean v5, v4, Landroid/content/pm/ResolveInfo;->handleAllWebDataURI:Z

    if-nez v5, :cond_0

    .line 188
    goto :goto_1

    .line 190
    :cond_0
    iget-object v5, v4, Landroid/content/pm/ResolveInfo;->activityInfo:Landroid/content/pm/ActivityInfo;

    iget-object v5, v5, Landroid/content/pm/ActivityInfo;->packageName:Ljava/lang/String;

    .line 191
    .local v5, "packageName":Ljava/lang/String;
    invoke-interface {v2, v5}, Ljava/util/Set;->contains(Ljava/lang/Object;)Z

    move-result v6

    if-eqz v6, :cond_1

    .line 192
    goto :goto_1

    .line 194
    :cond_1
    invoke-interface {v2, v5}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    .line 185
    .end local v4    # "info":Landroid/content/pm/ResolveInfo;
    .end local v5    # "packageName":Ljava/lang/String;
    :cond_2
    :goto_1
    add-int/lit8 v3, v3, 0x1

    goto :goto_0

    .line 196
    .end local v3    # "i":I
    :cond_3
    return-object v2
.end method

.method public static setDefaultBrowser(Landroid/content/Context;)V
    .locals 4
    .param p0, "context"    # Landroid/content/Context;

    .line 169
    invoke-virtual {p0}, Landroid/content/Context;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v0

    .line 171
    .local v0, "pm":Landroid/content/pm/PackageManager;
    const/4 v1, 0x0

    :try_start_0
    invoke-virtual {v0, v1}, Landroid/content/pm/PackageManager;->getDefaultBrowserPackageNameAsUser(I)Ljava/lang/String;

    move-result-object v2

    .line 172
    .local v2, "defaultBrowser":Ljava/lang/String;
    invoke-static {v2}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v3

    if-eqz v3, :cond_0

    .line 173
    sget-object v3, Lcom/miui/server/security/DefaultBrowserImpl;->PKG_BROWSER:Ljava/lang/String;

    invoke-virtual {v0, v3, v1}, Landroid/content/pm/PackageManager;->setDefaultBrowserPackageNameAsUser(Ljava/lang/String;I)Z
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 177
    .end local v2    # "defaultBrowser":Ljava/lang/String;
    :cond_0
    goto :goto_0

    .line 175
    :catch_0
    move-exception v1

    .line 176
    .local v1, "e":Ljava/lang/Exception;
    const-string v2, "DefaultBrowserImpl"

    const-string/jumbo v3, "setDefaultBrowser"

    invoke-static {v2, v3, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    .line 178
    .end local v1    # "e":Ljava/lang/Exception;
    :goto_0
    return-void
.end method


# virtual methods
.method public checkDefaultBrowser(Ljava/lang/String;)V
    .locals 4
    .param p1, "packageName"    # Ljava/lang/String;

    .line 45
    sget-boolean v0, Lmiui/os/Build;->IS_INTERNATIONAL_BUILD:Z

    if-nez v0, :cond_1

    invoke-static {}, Landroid/miui/AppOpsUtils;->isXOptMode()Z

    move-result v0

    if-eqz v0, :cond_0

    goto :goto_0

    .line 48
    :cond_0
    iget-object v0, p0, Lcom/miui/server/security/DefaultBrowserImpl;->mHandler:Landroid/os/Handler;

    new-instance v1, Lcom/miui/server/security/DefaultBrowserImpl$$ExternalSyntheticLambda1;

    invoke-direct {v1, p0, p1}, Lcom/miui/server/security/DefaultBrowserImpl$$ExternalSyntheticLambda1;-><init>(Lcom/miui/server/security/DefaultBrowserImpl;Ljava/lang/String;)V

    const-wide/16 v2, 0x12c

    invoke-virtual {v0, v1, v2, v3}, Landroid/os/Handler;->postDelayed(Ljava/lang/Runnable;J)Z

    .line 65
    return-void

    .line 46
    :cond_1
    :goto_0
    return-void
.end method

.method public resetDefaultBrowser()V
    .locals 4

    .line 68
    sget-boolean v0, Lmiui/os/Build;->IS_INTERNATIONAL_BUILD:Z

    if-nez v0, :cond_1

    invoke-static {}, Landroid/miui/AppOpsUtils;->isXOptMode()Z

    move-result v0

    if-eqz v0, :cond_0

    goto :goto_0

    .line 71
    :cond_0
    iget-object v0, p0, Lcom/miui/server/security/DefaultBrowserImpl;->mHandler:Landroid/os/Handler;

    new-instance v1, Lcom/miui/server/security/DefaultBrowserImpl$$ExternalSyntheticLambda0;

    invoke-direct {v1, p0}, Lcom/miui/server/security/DefaultBrowserImpl$$ExternalSyntheticLambda0;-><init>(Lcom/miui/server/security/DefaultBrowserImpl;)V

    const-wide/32 v2, 0xea60

    invoke-virtual {v0, v1, v2, v3}, Landroid/os/Handler;->postDelayed(Ljava/lang/Runnable;J)Z

    .line 79
    return-void

    .line 69
    :cond_1
    :goto_0
    return-void
.end method
