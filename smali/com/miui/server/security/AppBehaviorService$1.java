class com.miui.server.security.AppBehaviorService$1 extends miui.process.IForegroundInfoListener$Stub {
	 /* .source "AppBehaviorService.java" */
	 /* # annotations */
	 /* .annotation system Ldalvik/annotation/EnclosingClass; */
	 /* value = Lcom/miui/server/security/AppBehaviorService; */
} // .end annotation
/* .annotation system Ldalvik/annotation/InnerClass; */
/* accessFlags = 0x0 */
/* name = null */
} // .end annotation
/* # instance fields */
final com.miui.server.security.AppBehaviorService this$0; //synthetic
/* # direct methods */
 com.miui.server.security.AppBehaviorService$1 ( ) {
/* .locals 0 */
/* .param p1, "this$0" # Lcom/miui/server/security/AppBehaviorService; */
/* .line 93 */
this.this$0 = p1;
/* invoke-direct {p0}, Lmiui/process/IForegroundInfoListener$Stub;-><init>()V */
return;
} // .end method
/* # virtual methods */
public void onForegroundInfoChanged ( miui.process.ForegroundInfo p0 ) {
/* .locals 6 */
/* .param p1, "foregroundInfo" # Lmiui/process/ForegroundInfo; */
/* .line 96 */
if ( p1 != null) { // if-eqz p1, :cond_1
	 v0 = this.mForegroundPackageName;
	 v1 = this.mLastForegroundPackageName;
	 /* .line 97 */
	 v0 = 	 android.text.TextUtils .equals ( v0,v1 );
	 /* if-nez v0, :cond_1 */
	 /* iget v0, p1, Lmiui/process/ForegroundInfo;->mForegroundUid:I */
	 /* .line 98 */
	 v0 = 	 android.os.UserHandle .getAppId ( v0 );
	 /* const/16 v1, 0x2710 */
	 /* if-lt v0, v1, :cond_1 */
	 v0 = this.this$0;
	 v1 = this.mForegroundPackageName;
	 /* iget v2, p1, Lmiui/process/ForegroundInfo;->mForegroundUid:I */
	 /* .line 99 */
	 v2 = 	 android.os.UserHandle .getUserId ( v2 );
	 com.miui.server.security.AppBehaviorService .-$$Nest$mgetApplicationInfo ( v0,v1,v2 );
	 v0 = 	 com.miui.server.security.AppBehaviorService .isSystem ( v0 );
	 /* if-nez v0, :cond_1 */
	 v0 = this.this$0;
	 com.miui.server.security.AppBehaviorService .-$$Nest$fgetmAppOps ( v0 );
	 /* iget v1, p1, Lmiui/process/ForegroundInfo;->mForegroundUid:I */
	 v2 = this.mForegroundPackageName;
	 /* .line 100 */
	 /* const/16 v3, 0x2725 */
	 v0 = 	 (( android.app.AppOpsManager ) v0 ).checkOpNoThrow ( v3, v1, v2 ); // invoke-virtual {v0, v3, v1, v2}, Landroid/app/AppOpsManager;->checkOpNoThrow(IILjava/lang/String;)I
	 /* if-nez v0, :cond_0 */
	 /* .line 105 */
} // :cond_0
v0 = this.this$0;
com.miui.server.security.AppBehaviorService .-$$Nest$fgetmForegroundRecorder ( v0 );
/* monitor-enter v0 */
/* .line 109 */
try { // :try_start_0
	 v1 = this.this$0;
	 com.miui.server.security.AppBehaviorService .-$$Nest$fgetmForegroundRecorder ( v1 );
	 v2 = this.mForegroundPackageName;
	 v3 = this.this$0;
	 com.miui.server.security.AppBehaviorService .-$$Nest$fgetmForegroundRecorder ( v3 );
	 v4 = this.mForegroundPackageName;
	 /* .line 110 */
	 int v5 = 0; // const/4 v5, 0x0
	 java.lang.Integer .valueOf ( v5 );
	 (( android.util.ArrayMap ) v3 ).getOrDefault ( v4, v5 ); // invoke-virtual {v3, v4, v5}, Landroid/util/ArrayMap;->getOrDefault(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;
	 /* check-cast v3, Ljava/lang/Integer; */
	 v3 = 	 (( java.lang.Integer ) v3 ).intValue ( ); // invoke-virtual {v3}, Ljava/lang/Integer;->intValue()I
	 /* add-int/lit8 v3, v3, 0x1 */
	 java.lang.Integer .valueOf ( v3 );
	 /* .line 109 */
	 (( android.util.ArrayMap ) v1 ).put ( v2, v3 ); // invoke-virtual {v1, v2, v3}, Landroid/util/ArrayMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;
	 /* .line 111 */
	 /* monitor-exit v0 */
	 /* .line 112 */
	 return;
	 /* .line 111 */
	 /* :catchall_0 */
	 /* move-exception v1 */
	 /* monitor-exit v0 */
	 /* :try_end_0 */
	 /* .catchall {:try_start_0 ..:try_end_0} :catchall_0 */
	 /* throw v1 */
	 /* .line 102 */
} // :cond_1
} // :goto_0
return;
} // .end method
