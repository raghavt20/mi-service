.class Lcom/miui/server/security/AppBehaviorService$1;
.super Lmiui/process/IForegroundInfoListener$Stub;
.source "AppBehaviorService.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/miui/server/security/AppBehaviorService;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/miui/server/security/AppBehaviorService;


# direct methods
.method constructor <init>(Lcom/miui/server/security/AppBehaviorService;)V
    .locals 0
    .param p1, "this$0"    # Lcom/miui/server/security/AppBehaviorService;

    .line 93
    iput-object p1, p0, Lcom/miui/server/security/AppBehaviorService$1;->this$0:Lcom/miui/server/security/AppBehaviorService;

    invoke-direct {p0}, Lmiui/process/IForegroundInfoListener$Stub;-><init>()V

    return-void
.end method


# virtual methods
.method public onForegroundInfoChanged(Lmiui/process/ForegroundInfo;)V
    .locals 6
    .param p1, "foregroundInfo"    # Lmiui/process/ForegroundInfo;

    .line 96
    if-eqz p1, :cond_1

    iget-object v0, p1, Lmiui/process/ForegroundInfo;->mForegroundPackageName:Ljava/lang/String;

    iget-object v1, p1, Lmiui/process/ForegroundInfo;->mLastForegroundPackageName:Ljava/lang/String;

    .line 97
    invoke-static {v0, v1}, Landroid/text/TextUtils;->equals(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_1

    iget v0, p1, Lmiui/process/ForegroundInfo;->mForegroundUid:I

    .line 98
    invoke-static {v0}, Landroid/os/UserHandle;->getAppId(I)I

    move-result v0

    const/16 v1, 0x2710

    if-lt v0, v1, :cond_1

    iget-object v0, p0, Lcom/miui/server/security/AppBehaviorService$1;->this$0:Lcom/miui/server/security/AppBehaviorService;

    iget-object v1, p1, Lmiui/process/ForegroundInfo;->mForegroundPackageName:Ljava/lang/String;

    iget v2, p1, Lmiui/process/ForegroundInfo;->mForegroundUid:I

    .line 99
    invoke-static {v2}, Landroid/os/UserHandle;->getUserId(I)I

    move-result v2

    invoke-static {v0, v1, v2}, Lcom/miui/server/security/AppBehaviorService;->-$$Nest$mgetApplicationInfo(Lcom/miui/server/security/AppBehaviorService;Ljava/lang/String;I)Landroid/content/pm/ApplicationInfo;

    move-result-object v0

    invoke-static {v0}, Lcom/miui/server/security/AppBehaviorService;->isSystem(Landroid/content/pm/ApplicationInfo;)Z

    move-result v0

    if-nez v0, :cond_1

    iget-object v0, p0, Lcom/miui/server/security/AppBehaviorService$1;->this$0:Lcom/miui/server/security/AppBehaviorService;

    invoke-static {v0}, Lcom/miui/server/security/AppBehaviorService;->-$$Nest$fgetmAppOps(Lcom/miui/server/security/AppBehaviorService;)Landroid/app/AppOpsManager;

    move-result-object v0

    iget v1, p1, Lmiui/process/ForegroundInfo;->mForegroundUid:I

    iget-object v2, p1, Lmiui/process/ForegroundInfo;->mForegroundPackageName:Ljava/lang/String;

    .line 100
    const/16 v3, 0x2725

    invoke-virtual {v0, v3, v1, v2}, Landroid/app/AppOpsManager;->checkOpNoThrow(IILjava/lang/String;)I

    move-result v0

    if-nez v0, :cond_0

    goto :goto_0

    .line 105
    :cond_0
    iget-object v0, p0, Lcom/miui/server/security/AppBehaviorService$1;->this$0:Lcom/miui/server/security/AppBehaviorService;

    invoke-static {v0}, Lcom/miui/server/security/AppBehaviorService;->-$$Nest$fgetmForegroundRecorder(Lcom/miui/server/security/AppBehaviorService;)Landroid/util/ArrayMap;

    move-result-object v0

    monitor-enter v0

    .line 109
    :try_start_0
    iget-object v1, p0, Lcom/miui/server/security/AppBehaviorService$1;->this$0:Lcom/miui/server/security/AppBehaviorService;

    invoke-static {v1}, Lcom/miui/server/security/AppBehaviorService;->-$$Nest$fgetmForegroundRecorder(Lcom/miui/server/security/AppBehaviorService;)Landroid/util/ArrayMap;

    move-result-object v1

    iget-object v2, p1, Lmiui/process/ForegroundInfo;->mForegroundPackageName:Ljava/lang/String;

    iget-object v3, p0, Lcom/miui/server/security/AppBehaviorService$1;->this$0:Lcom/miui/server/security/AppBehaviorService;

    invoke-static {v3}, Lcom/miui/server/security/AppBehaviorService;->-$$Nest$fgetmForegroundRecorder(Lcom/miui/server/security/AppBehaviorService;)Landroid/util/ArrayMap;

    move-result-object v3

    iget-object v4, p1, Lmiui/process/ForegroundInfo;->mForegroundPackageName:Ljava/lang/String;

    .line 110
    const/4 v5, 0x0

    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    invoke-virtual {v3, v4, v5}, Landroid/util/ArrayMap;->getOrDefault(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/lang/Integer;

    invoke-virtual {v3}, Ljava/lang/Integer;->intValue()I

    move-result v3

    add-int/lit8 v3, v3, 0x1

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    .line 109
    invoke-virtual {v1, v2, v3}, Landroid/util/ArrayMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 111
    monitor-exit v0

    .line 112
    return-void

    .line 111
    :catchall_0
    move-exception v1

    monitor-exit v0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v1

    .line 102
    :cond_1
    :goto_0
    return-void
.end method
