.class Lcom/miui/server/security/AppDurationService$UserPackageBehaviorDuration;
.super Ljava/lang/Object;
.source "AppDurationService.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/miui/server/security/AppDurationService;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0xa
    name = "UserPackageBehaviorDuration"
.end annotation


# instance fields
.field private behaviorDurationMap:Landroid/util/SparseArray;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/util/SparseArray<",
            "Lcom/miui/server/security/AppDurationService$TokenAndStartTime;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method static bridge synthetic -$$Nest$fgetbehaviorDurationMap(Lcom/miui/server/security/AppDurationService$UserPackageBehaviorDuration;)Landroid/util/SparseArray;
    .locals 0

    iget-object p0, p0, Lcom/miui/server/security/AppDurationService$UserPackageBehaviorDuration;->behaviorDurationMap:Landroid/util/SparseArray;

    return-object p0
.end method

.method private constructor <init>()V
    .locals 0

    .line 150
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method synthetic constructor <init>(Lcom/miui/server/security/AppDurationService$UserPackageBehaviorDuration-IA;)V
    .locals 0

    invoke-direct {p0}, Lcom/miui/server/security/AppDurationService$UserPackageBehaviorDuration;-><init>()V

    return-void
.end method


# virtual methods
.method public recordBehavior(ILandroid/os/IBinder;I)Z
    .locals 3
    .param p1, "behavior"    # I
    .param p2, "binder"    # Landroid/os/IBinder;
    .param p3, "uid"    # I

    .line 154
    iget-object v0, p0, Lcom/miui/server/security/AppDurationService$UserPackageBehaviorDuration;->behaviorDurationMap:Landroid/util/SparseArray;

    if-nez v0, :cond_0

    .line 155
    new-instance v0, Landroid/util/SparseArray;

    invoke-direct {v0}, Landroid/util/SparseArray;-><init>()V

    iput-object v0, p0, Lcom/miui/server/security/AppDurationService$UserPackageBehaviorDuration;->behaviorDurationMap:Landroid/util/SparseArray;

    .line 157
    :cond_0
    iget-object v0, p0, Lcom/miui/server/security/AppDurationService$UserPackageBehaviorDuration;->behaviorDurationMap:Landroid/util/SparseArray;

    invoke-virtual {v0, p1}, Landroid/util/SparseArray;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/miui/server/security/AppDurationService$TokenAndStartTime;

    .line 158
    .local v0, "tokenAndStartTime":Lcom/miui/server/security/AppDurationService$TokenAndStartTime;
    if-nez v0, :cond_1

    .line 159
    new-instance v1, Lcom/miui/server/security/AppDurationService$TokenAndStartTime;

    const/4 v2, 0x0

    invoke-direct {v1, v2}, Lcom/miui/server/security/AppDurationService$TokenAndStartTime;-><init>(Lcom/miui/server/security/AppDurationService$TokenAndStartTime-IA;)V

    move-object v0, v1

    .line 160
    iget-object v1, p0, Lcom/miui/server/security/AppDurationService$UserPackageBehaviorDuration;->behaviorDurationMap:Landroid/util/SparseArray;

    invoke-virtual {v1, p1, v0}, Landroid/util/SparseArray;->put(ILjava/lang/Object;)V

    .line 162
    :cond_1
    if-eqz p2, :cond_2

    .line 163
    invoke-virtual {v0, p2}, Lcom/miui/server/security/AppDurationService$TokenAndStartTime;->recordBinderTime(Landroid/os/IBinder;)Z

    move-result v1

    return v1

    .line 165
    :cond_2
    invoke-virtual {v0, p3}, Lcom/miui/server/security/AppDurationService$TokenAndStartTime;->recordUidTime(I)Z

    move-result v1

    return v1
.end method

.method public stopRecordDuration(ILandroid/os/IBinder;I)J
    .locals 3
    .param p1, "behavior"    # I
    .param p2, "binder"    # Landroid/os/IBinder;
    .param p3, "uid"    # I

    .line 170
    iget-object v0, p0, Lcom/miui/server/security/AppDurationService$UserPackageBehaviorDuration;->behaviorDurationMap:Landroid/util/SparseArray;

    const-wide/16 v1, 0x0

    if-eqz v0, :cond_3

    .line 171
    invoke-virtual {v0}, Landroid/util/SparseArray;->size()I

    move-result v0

    if-nez v0, :cond_0

    goto :goto_1

    .line 174
    :cond_0
    iget-object v0, p0, Lcom/miui/server/security/AppDurationService$UserPackageBehaviorDuration;->behaviorDurationMap:Landroid/util/SparseArray;

    invoke-virtual {v0, p1}, Landroid/util/SparseArray;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/miui/server/security/AppDurationService$TokenAndStartTime;

    .line 175
    .local v0, "tokenAndStartTime":Lcom/miui/server/security/AppDurationService$TokenAndStartTime;
    if-nez v0, :cond_1

    .line 176
    return-wide v1

    .line 178
    :cond_1
    if-eqz p2, :cond_2

    invoke-virtual {v0, p2}, Lcom/miui/server/security/AppDurationService$TokenAndStartTime;->stopBinderTime(Landroid/os/IBinder;)J

    move-result-wide v1

    goto :goto_0

    .line 179
    :cond_2
    invoke-virtual {v0, p3}, Lcom/miui/server/security/AppDurationService$TokenAndStartTime;->stopUidTime(I)J

    move-result-wide v1

    .line 178
    :goto_0
    return-wide v1

    .line 172
    .end local v0    # "tokenAndStartTime":Lcom/miui/server/security/AppDurationService$TokenAndStartTime;
    :cond_3
    :goto_1
    return-wide v1
.end method
