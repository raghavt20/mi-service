class com.miui.server.security.AppDurationService$1 extends android.app.IForegroundServiceObserver$Stub {
	 /* .source "AppDurationService.java" */
	 /* # annotations */
	 /* .annotation system Ldalvik/annotation/EnclosingMethod; */
	 /* value = Lcom/miui/server/security/AppDurationService;-><init>(Lcom/miui/server/security/AppBehaviorService;)V */
} // .end annotation
/* .annotation system Ldalvik/annotation/InnerClass; */
/* accessFlags = 0x0 */
/* name = null */
} // .end annotation
/* # instance fields */
final com.miui.server.security.AppDurationService this$0; //synthetic
final com.miui.server.security.AppBehaviorService val$appBehavior; //synthetic
/* # direct methods */
 com.miui.server.security.AppDurationService$1 ( ) {
/* .locals 0 */
/* .param p1, "this$0" # Lcom/miui/server/security/AppDurationService; */
/* .line 28 */
this.this$0 = p1;
this.val$appBehavior = p2;
/* invoke-direct {p0}, Landroid/app/IForegroundServiceObserver$Stub;-><init>()V */
return;
} // .end method
/* # virtual methods */
public void onForegroundStateChanged ( android.os.IBinder p0, java.lang.String p1, Integer p2, Boolean p3 ) {
/* .locals 9 */
/* .param p1, "serviceToken" # Landroid/os/IBinder; */
/* .param p2, "packageName" # Ljava/lang/String; */
/* .param p3, "userId" # I */
/* .param p4, "isForeground" # Z */
/* .line 31 */
v0 = this.val$appBehavior;
v0 = this.mPackageManagerInt;
/* const-wide/16 v1, 0x0 */
v0 = (( android.content.pm.PackageManagerInternal ) v0 ).getPackageUid ( p2, v1, v2, p3 ); // invoke-virtual {v0, p2, v1, v2, p3}, Landroid/content/pm/PackageManagerInternal;->getPackageUid(Ljava/lang/String;JI)I
/* .line 32 */
/* .local v0, "uid":I */
v3 = this.this$0;
/* const/16 v4, 0x19 */
/* move-object v5, p1 */
/* move v6, v0 */
/* move-object v7, p2 */
/* move v8, p4 */
/* invoke-virtual/range {v3 ..v8}, Lcom/miui/server/security/AppDurationService;->onDurationEvent(ILandroid/os/IBinder;ILjava/lang/String;Z)V */
/* .line 33 */
return;
} // .end method
