.class public Lcom/miui/server/security/GameBoosterImpl$GameBoosterServiceDeath;
.super Ljava/lang/Object;
.source "GameBoosterImpl.java"

# interfaces
.implements Landroid/os/IBinder$DeathRecipient;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/miui/server/security/GameBoosterImpl;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x1
    name = "GameBoosterServiceDeath"
.end annotation


# instance fields
.field public final mGameBoosterService:Landroid/os/IBinder;

.field private final mUserState:Lcom/miui/server/security/SecurityUserState;

.field final synthetic this$0:Lcom/miui/server/security/GameBoosterImpl;


# direct methods
.method public constructor <init>(Lcom/miui/server/security/GameBoosterImpl;Lcom/miui/server/security/SecurityUserState;Landroid/os/IBinder;)V
    .locals 0
    .param p1, "this$0"    # Lcom/miui/server/security/GameBoosterImpl;
    .param p2, "userState"    # Lcom/miui/server/security/SecurityUserState;
    .param p3, "gameBoosterService"    # Landroid/os/IBinder;

    .line 108
    iput-object p1, p0, Lcom/miui/server/security/GameBoosterImpl$GameBoosterServiceDeath;->this$0:Lcom/miui/server/security/GameBoosterImpl;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 109
    iput-object p2, p0, Lcom/miui/server/security/GameBoosterImpl$GameBoosterServiceDeath;->mUserState:Lcom/miui/server/security/SecurityUserState;

    .line 110
    iput-object p3, p0, Lcom/miui/server/security/GameBoosterImpl$GameBoosterServiceDeath;->mGameBoosterService:Landroid/os/IBinder;

    .line 111
    return-void
.end method


# virtual methods
.method public binderDied()V
    .locals 4

    .line 115
    iget-object v0, p0, Lcom/miui/server/security/GameBoosterImpl$GameBoosterServiceDeath;->this$0:Lcom/miui/server/security/GameBoosterImpl;

    invoke-static {v0}, Lcom/miui/server/security/GameBoosterImpl;->-$$Nest$fgetmService(Lcom/miui/server/security/GameBoosterImpl;)Lcom/miui/server/SecurityManagerService;

    move-result-object v0

    iget-object v0, v0, Lcom/miui/server/SecurityManagerService;->mUserStateLock:Ljava/lang/Object;

    monitor-enter v0

    .line 117
    :try_start_0
    iget-object v1, p0, Lcom/miui/server/security/GameBoosterImpl$GameBoosterServiceDeath;->mGameBoosterService:Landroid/os/IBinder;

    const/4 v2, 0x0

    invoke-interface {v1, p0, v2}, Landroid/os/IBinder;->unlinkToDeath(Landroid/os/IBinder$DeathRecipient;I)Z

    .line 118
    iget-object v1, p0, Lcom/miui/server/security/GameBoosterImpl$GameBoosterServiceDeath;->mUserState:Lcom/miui/server/security/SecurityUserState;

    iput-boolean v2, v1, Lcom/miui/server/security/SecurityUserState;->mIsGameMode:Z

    .line 119
    iget-object v1, p0, Lcom/miui/server/security/GameBoosterImpl$GameBoosterServiceDeath;->mUserState:Lcom/miui/server/security/SecurityUserState;

    const/4 v2, 0x0

    iput-object v2, v1, Lcom/miui/server/security/SecurityUserState;->gameBoosterServiceDeath:Lcom/miui/server/security/GameBoosterImpl$GameBoosterServiceDeath;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 122
    goto :goto_0

    .line 123
    :catchall_0
    move-exception v1

    goto :goto_1

    .line 120
    :catch_0
    move-exception v1

    .line 121
    .local v1, "e":Ljava/lang/Exception;
    :try_start_1
    const-string v2, "GameBoosterImpl"

    const-string v3, "GameBoosterServiceDeath"

    invoke-static {v2, v3, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    .line 123
    .end local v1    # "e":Ljava/lang/Exception;
    :goto_0
    monitor-exit v0

    .line 124
    return-void

    .line 123
    :goto_1
    monitor-exit v0
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    throw v1
.end method
