public class com.miui.server.security.DefaultBrowserImpl {
	 /* .source "DefaultBrowserImpl.java" */
	 /* # static fields */
	 private static final java.lang.String DEF_BROWSER_COUNT;
	 private static final Long ONE_MINUTE;
	 private static final java.lang.String PKG_BROWSER;
	 private static final java.lang.String TAG;
	 /* # instance fields */
	 private final android.content.Context mContext;
	 private final android.os.Handler mHandler;
	 /* # direct methods */
	 public static void $r8$lambda$CeYczOnGWjfSbrQXETrQDhlbiN0 ( com.miui.server.security.DefaultBrowserImpl p0 ) { //synthethic
		 /* .locals 0 */
		 /* invoke-direct {p0}, Lcom/miui/server/security/DefaultBrowserImpl;->lambda$resetDefaultBrowser$1()V */
		 return;
	 } // .end method
	 public static void $r8$lambda$FhVo5iOOWtgKNtMsTOS5PFA65hQ ( com.miui.server.security.DefaultBrowserImpl p0, java.lang.String p1 ) { //synthethic
		 /* .locals 0 */
		 /* invoke-direct {p0, p1}, Lcom/miui/server/security/DefaultBrowserImpl;->lambda$checkDefaultBrowser$0(Ljava/lang/String;)V */
		 return;
	 } // .end method
	 static com.miui.server.security.DefaultBrowserImpl ( ) {
		 /* .locals 1 */
		 /* .line 31 */
		 /* sget-boolean v0, Lmiui/os/Build;->IS_INTERNATIONAL_BUILD:Z */
		 if ( v0 != null) { // if-eqz v0, :cond_0
			 final String v0 = "com.mi.globalbrowser"; // const-string v0, "com.mi.globalbrowser"
		 } // :cond_0
		 final String v0 = "com.android.browser"; // const-string v0, "com.android.browser"
	 } // :goto_0
	 return;
} // .end method
public com.miui.server.security.DefaultBrowserImpl ( ) {
	 /* .locals 1 */
	 /* .param p1, "service" # Lcom/miui/server/SecurityManagerService; */
	 /* .line 38 */
	 /* invoke-direct {p0}, Ljava/lang/Object;-><init>()V */
	 /* .line 39 */
	 v0 = this.mContext;
	 this.mContext = v0;
	 /* .line 40 */
	 v0 = this.mSecurityWriteHandler;
	 this.mHandler = v0;
	 /* .line 41 */
	 return;
} // .end method
private void checkIntentFilterVerifications ( ) {
	 /* .locals 1 */
	 /* .line 82 */
	 int v0 = 0; // const/4 v0, 0x0
	 /* invoke-direct {p0, v0}, Lcom/miui/server/security/DefaultBrowserImpl;->checkIntentFilterVerifications(Ljava/lang/String;)V */
	 /* .line 83 */
	 return;
} // .end method
private void checkIntentFilterVerifications ( java.lang.String p0 ) {
	 /* .locals 26 */
	 /* .param p1, "packageName" # Ljava/lang/String; */
	 /* .line 86 */
	 /* move-object/from16 v1, p0 */
	 /* move-object/from16 v2, p1 */
	 final String v0 = "android.intent.action.VIEW"; // const-string v0, "android.intent.action.VIEW"
	 v3 = this.mContext;
	 (( android.content.Context ) v3 ).getPackageManager ( ); // invoke-virtual {v3}, Landroid/content/Context;->getPackageManager()Landroid/content/pm/PackageManager;
	 /* .line 89 */
	 /* .local v3, "pm":Landroid/content/pm/PackageManager; */
	 int v4 = 0; // const/4 v4, 0x0
	 /* if-nez v2, :cond_0 */
	 /* .line 90 */
	 /* const/16 v5, 0x2000 */
	 try { // :try_start_0
		 (( android.content.pm.PackageManager ) v3 ).getInstalledPackages ( v5 ); // invoke-virtual {v3, v5}, Landroid/content/pm/PackageManager;->getInstalledPackages(I)Ljava/util/List;
		 /* .local v5, "applications":Ljava/util/List;, "Ljava/util/List<Landroid/content/pm/PackageInfo;>;" */
		 /* .line 92 */
	 } // .end local v5 # "applications":Ljava/util/List;, "Ljava/util/List<Landroid/content/pm/PackageInfo;>;"
} // :cond_0
(( android.content.pm.PackageManager ) v3 ).getPackageInfo ( v2, v4 ); // invoke-virtual {v3, v2, v4}, Landroid/content/pm/PackageManager;->getPackageInfo(Ljava/lang/String;I)Landroid/content/pm/PackageInfo;
java.util.Collections .singletonList ( v5 );
/* .line 94 */
/* .restart local v5 # "applications":Ljava/util/List;, "Ljava/util/List<Landroid/content/pm/PackageInfo;>;" */
} // :goto_0
/* new-instance v6, Landroid/content/Intent; */
/* invoke-direct {v6, v0}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V */
final String v7 = "android.intent.category.BROWSABLE"; // const-string v7, "android.intent.category.BROWSABLE"
/* .line 95 */
(( android.content.Intent ) v6 ).addCategory ( v7 ); // invoke-virtual {v6, v7}, Landroid/content/Intent;->addCategory(Ljava/lang/String;)Landroid/content/Intent;
final String v7 = "http://"; // const-string v7, "http://"
/* .line 96 */
android.net.Uri .parse ( v7 );
(( android.content.Intent ) v6 ).setData ( v7 ); // invoke-virtual {v6, v7}, Landroid/content/Intent;->setData(Landroid/net/Uri;)Landroid/content/Intent;
/* .line 97 */
/* .local v6, "browserIntent":Landroid/content/Intent; */
/* new-instance v7, Landroid/content/Intent; */
/* invoke-direct {v7, v0}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V */
final String v8 = "http://www.xiaomi.com"; // const-string v8, "http://www.xiaomi.com"
/* .line 98 */
android.net.Uri .parse ( v8 );
(( android.content.Intent ) v7 ).setData ( v8 ); // invoke-virtual {v7, v8}, Landroid/content/Intent;->setData(Landroid/net/Uri;)Landroid/content/Intent;
/* .line 99 */
/* .local v7, "httpIntent":Landroid/content/Intent; */
/* new-instance v8, Landroid/content/Intent; */
/* invoke-direct {v8, v0}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V */
final String v9 = "https://www.xiaomi.com"; // const-string v9, "https://www.xiaomi.com"
/* .line 100 */
android.net.Uri .parse ( v9 );
(( android.content.Intent ) v8 ).setData ( v9 ); // invoke-virtual {v8, v9}, Landroid/content/Intent;->setData(Landroid/net/Uri;)Landroid/content/Intent;
/* .line 101 */
/* .local v8, "httpsIntent":Landroid/content/Intent; */
if ( v2 != null) { // if-eqz v2, :cond_1
/* .line 102 */
(( android.content.Intent ) v6 ).setPackage ( v2 ); // invoke-virtual {v6, v2}, Landroid/content/Intent;->setPackage(Ljava/lang/String;)Landroid/content/Intent;
/* .line 103 */
(( android.content.Intent ) v7 ).setPackage ( v2 ); // invoke-virtual {v7, v2}, Landroid/content/Intent;->setPackage(Ljava/lang/String;)Landroid/content/Intent;
/* .line 104 */
(( android.content.Intent ) v8 ).setPackage ( v2 ); // invoke-virtual {v8, v2}, Landroid/content/Intent;->setPackage(Ljava/lang/String;)Landroid/content/Intent;
/* .line 106 */
} // :cond_1
int v9 = 0; // const/4 v9, 0x0
/* .line 107 */
/* .local v9, "userId":I */
int v10 = 1; // const/4 v10, 0x1
/* invoke-direct {v1, v3, v6, v10, v9}, Lcom/miui/server/security/DefaultBrowserImpl;->queryIntentPackages(Landroid/content/pm/PackageManager;Landroid/content/Intent;ZI)Ljava/util/Set; */
/* .line 108 */
/* .local v11, "browsers":Ljava/util/Set;, "Ljava/util/Set<Ljava/lang/String;>;" */
/* invoke-direct {v1, v3, v7, v4, v9}, Lcom/miui/server/security/DefaultBrowserImpl;->queryIntentPackages(Landroid/content/pm/PackageManager;Landroid/content/Intent;ZI)Ljava/util/Set; */
/* .line 109 */
/* .local v12, "httpPackages":Ljava/util/Set;, "Ljava/util/Set<Ljava/lang/String;>;" */
/* invoke-direct {v1, v3, v8, v4, v9}, Lcom/miui/server/security/DefaultBrowserImpl;->queryIntentPackages(Landroid/content/pm/PackageManager;Landroid/content/Intent;ZI)Ljava/util/Set; */
/* .line 110 */
/* .local v13, "httpsPackages":Ljava/util/Set;, "Ljava/util/Set<Ljava/lang/String;>;" */
/* .line 112 */
/* new-instance v14, Landroid/util/ArraySet; */
/* invoke-direct {v14}, Landroid/util/ArraySet;-><init>()V */
/* .line 113 */
/* .local v14, "rejectPks":Landroid/util/ArraySet;, "Landroid/util/ArraySet<Ljava/lang/String;>;" */
v16 = } // :goto_1
if ( v16 != null) { // if-eqz v16, :cond_13
/* check-cast v16, Landroid/content/pm/PackageInfo; */
/* move-object/from16 v17, v16 */
/* .line 114 */
/* .local v17, "info":Landroid/content/pm/PackageInfo; */
/* move-object/from16 v10, v17 */
} // .end local v17 # "info":Landroid/content/pm/PackageInfo;
/* .local v10, "info":Landroid/content/pm/PackageInfo; */
v4 = this.applicationInfo;
/* iget v4, v4, Landroid/content/pm/ApplicationInfo;->uid:I */
v4 = android.os.UserHandle .getAppId ( v4 );
/* const/16 v1, 0x2710 */
/* if-lt v4, v1, :cond_12 */
v1 = this.applicationInfo;
/* .line 115 */
v1 = (( android.content.pm.ApplicationInfo ) v1 ).isSystemApp ( ); // invoke-virtual {v1}, Landroid/content/pm/ApplicationInfo;->isSystemApp()Z
if ( v1 != null) { // if-eqz v1, :cond_2
/* .line 116 */
int v4 = 0; // const/4 v4, 0x0
int v10 = 1; // const/4 v10, 0x1
/* move-object/from16 v1, p0 */
/* .line 118 */
} // :cond_2
v1 = this.applicationInfo;
v1 = this.packageName;
/* .line 119 */
v4 = /* .local v1, "pkg":Ljava/lang/String; */
if ( v4 != null) { // if-eqz v4, :cond_3
/* .line 120 */
int v4 = 0; // const/4 v4, 0x0
int v10 = 1; // const/4 v10, 0x1
/* move-object/from16 v1, p0 */
/* .line 122 */
v4 = } // :cond_3
/* if-nez v4, :cond_4 */
/* .line 123 */
int v4 = 0; // const/4 v4, 0x0
int v10 = 1; // const/4 v10, 0x1
/* move-object/from16 v1, p0 */
/* .line 125 */
} // :cond_4
(( android.content.pm.PackageManager ) v3 ).getAllIntentFilters ( v1 ); // invoke-virtual {v3, v1}, Landroid/content/pm/PackageManager;->getAllIntentFilters(Ljava/lang/String;)Ljava/util/List;
/* .line 126 */
/* .local v4, "filters":Ljava/util/List;, "Ljava/util/List<Landroid/content/IntentFilter;>;" */
/* const/16 v18, 0x0 */
/* .line 127 */
/* .local v18, "add":Z */
v19 = if ( v4 != null) { // if-eqz v4, :cond_e
/* if-lez v19, :cond_e */
/* .line 128 */
} // :goto_2
v20 = /* invoke-interface/range {v19 ..v19}, Ljava/util/Iterator;->hasNext()Z */
if ( v20 != null) { // if-eqz v20, :cond_d
/* invoke-interface/range {v19 ..v19}, Ljava/util/Iterator;->next()Ljava/lang/Object; */
/* check-cast v20, Landroid/content/IntentFilter; */
/* move-object/from16 v21, v20 */
/* .line 129 */
/* .local v21, "filter":Landroid/content/IntentFilter; */
/* move-object/from16 v2, v21 */
} // .end local v21 # "filter":Landroid/content/IntentFilter;
/* .local v2, "filter":Landroid/content/IntentFilter; */
v20 = (( android.content.IntentFilter ) v2 ).hasAction ( v0 ); // invoke-virtual {v2, v0}, Landroid/content/IntentFilter;->hasAction(Ljava/lang/String;)Z
/* if-nez v20, :cond_5 */
/* .line 130 */
/* move-object/from16 v2, p1 */
/* .line 132 */
} // :cond_5
/* move-object/from16 v20, v0 */
final String v0 = "http"; // const-string v0, "http"
v0 = (( android.content.IntentFilter ) v2 ).hasDataScheme ( v0 ); // invoke-virtual {v2, v0}, Landroid/content/IntentFilter;->hasDataScheme(Ljava/lang/String;)Z
/* if-nez v0, :cond_7 */
final String v0 = "https"; // const-string v0, "https"
/* .line 133 */
v0 = (( android.content.IntentFilter ) v2 ).hasDataScheme ( v0 ); // invoke-virtual {v2, v0}, Landroid/content/IntentFilter;->hasDataScheme(Ljava/lang/String;)Z
if ( v0 != null) { // if-eqz v0, :cond_6
} // :cond_6
/* move-object/from16 v21, v4 */
/* .line 134 */
} // :cond_7
} // :goto_3
(( android.content.IntentFilter ) v2 ).getHostsList ( ); // invoke-virtual {v2}, Landroid/content/IntentFilter;->getHostsList()Ljava/util/ArrayList;
/* .line 135 */
/* .local v0, "hostList":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/String;>;" */
v21 = (( java.util.ArrayList ) v0 ).size ( ); // invoke-virtual {v0}, Ljava/util/ArrayList;->size()I
if ( v21 != null) { // if-eqz v21, :cond_8
/* move-object/from16 v21, v4 */
} // .end local v4 # "filters":Ljava/util/List;, "Ljava/util/List<Landroid/content/IntentFilter;>;"
/* .local v21, "filters":Ljava/util/List;, "Ljava/util/List<Landroid/content/IntentFilter;>;" */
final String v4 = "*"; // const-string v4, "*"
v4 = (( java.util.ArrayList ) v0 ).contains ( v4 ); // invoke-virtual {v0, v4}, Ljava/util/ArrayList;->contains(Ljava/lang/Object;)Z
if ( v4 != null) { // if-eqz v4, :cond_c
} // .end local v21 # "filters":Ljava/util/List;, "Ljava/util/List<Landroid/content/IntentFilter;>;"
/* .restart local v4 # "filters":Ljava/util/List;, "Ljava/util/List<Landroid/content/IntentFilter;>;" */
} // :cond_8
/* move-object/from16 v21, v4 */
/* .line 136 */
} // .end local v4 # "filters":Ljava/util/List;, "Ljava/util/List<Landroid/content/IntentFilter;>;"
/* .restart local v21 # "filters":Ljava/util/List;, "Ljava/util/List<Landroid/content/IntentFilter;>;" */
} // :goto_4
v4 = (( android.content.IntentFilter ) v2 ).countDataPaths ( ); // invoke-virtual {v2}, Landroid/content/IntentFilter;->countDataPaths()I
/* .line 137 */
/* .local v4, "dataPathsCount":I */
/* if-lez v4, :cond_b */
/* .line 138 */
/* const/16 v22, 0x0 */
/* move-object/from16 v23, v0 */
/* move/from16 v0, v22 */
/* .local v0, "i":I */
/* .local v23, "hostList":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/String;>;" */
} // :goto_5
/* if-ge v0, v4, :cond_a */
/* .line 139 */
/* move/from16 v22, v4 */
} // .end local v4 # "dataPathsCount":I
/* .local v22, "dataPathsCount":I */
final String v4 = ".*"; // const-string v4, ".*"
(( android.content.IntentFilter ) v2 ).getDataPath ( v0 ); // invoke-virtual {v2, v0}, Landroid/content/IntentFilter;->getDataPath(I)Landroid/os/PatternMatcher;
/* move-object/from16 v25, v2 */
} // .end local v2 # "filter":Landroid/content/IntentFilter;
/* .local v25, "filter":Landroid/content/IntentFilter; */
/* invoke-virtual/range {v24 ..v24}, Landroid/os/PatternMatcher;->getPath()Ljava/lang/String; */
v2 = (( java.lang.String ) v4 ).equals ( v2 ); // invoke-virtual {v4, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z
if ( v2 != null) { // if-eqz v2, :cond_9
/* .line 140 */
int v2 = 1; // const/4 v2, 0x1
/* .line 141 */
} // .end local v18 # "add":Z
/* .local v2, "add":Z */
/* move/from16 v18, v2 */
/* .line 138 */
} // .end local v2 # "add":Z
/* .restart local v18 # "add":Z */
} // :cond_9
/* add-int/lit8 v0, v0, 0x1 */
/* move/from16 v4, v22 */
/* move-object/from16 v2, v25 */
} // .end local v22 # "dataPathsCount":I
} // .end local v25 # "filter":Landroid/content/IntentFilter;
/* .local v2, "filter":Landroid/content/IntentFilter; */
/* .restart local v4 # "dataPathsCount":I */
} // :cond_a
/* move-object/from16 v25, v2 */
/* move/from16 v22, v4 */
} // .end local v0 # "i":I
} // .end local v2 # "filter":Landroid/content/IntentFilter;
} // .end local v4 # "dataPathsCount":I
/* .restart local v22 # "dataPathsCount":I */
/* .restart local v25 # "filter":Landroid/content/IntentFilter; */
} // :goto_6
/* .line 145 */
} // .end local v22 # "dataPathsCount":I
} // .end local v23 # "hostList":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/String;>;"
} // .end local v25 # "filter":Landroid/content/IntentFilter;
/* .local v0, "hostList":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/String;>;" */
/* .restart local v2 # "filter":Landroid/content/IntentFilter; */
/* .restart local v4 # "dataPathsCount":I */
} // :cond_b
/* move-object/from16 v23, v0 */
/* move-object/from16 v25, v2 */
/* move/from16 v22, v4 */
} // .end local v0 # "hostList":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/String;>;"
} // .end local v2 # "filter":Landroid/content/IntentFilter;
} // .end local v4 # "dataPathsCount":I
/* .restart local v22 # "dataPathsCount":I */
/* .restart local v23 # "hostList":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/String;>;" */
/* .restart local v25 # "filter":Landroid/content/IntentFilter; */
/* const/16 v18, 0x1 */
/* .line 149 */
} // .end local v22 # "dataPathsCount":I
} // .end local v23 # "hostList":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/String;>;"
} // .end local v25 # "filter":Landroid/content/IntentFilter;
} // :cond_c
} // :goto_7
/* move-object/from16 v2, p1 */
/* move-object/from16 v0, v20 */
/* move-object/from16 v4, v21 */
/* goto/16 :goto_2 */
/* .line 128 */
} // .end local v21 # "filters":Ljava/util/List;, "Ljava/util/List<Landroid/content/IntentFilter;>;"
/* .local v4, "filters":Ljava/util/List;, "Ljava/util/List<Landroid/content/IntentFilter;>;" */
} // :cond_d
/* move-object/from16 v20, v0 */
/* move-object/from16 v21, v4 */
} // .end local v4 # "filters":Ljava/util/List;, "Ljava/util/List<Landroid/content/IntentFilter;>;"
/* .restart local v21 # "filters":Ljava/util/List;, "Ljava/util/List<Landroid/content/IntentFilter;>;" */
/* .line 127 */
} // .end local v21 # "filters":Ljava/util/List;, "Ljava/util/List<Landroid/content/IntentFilter;>;"
/* .restart local v4 # "filters":Ljava/util/List;, "Ljava/util/List<Landroid/content/IntentFilter;>;" */
} // :cond_e
/* move-object/from16 v20, v0 */
/* move-object/from16 v21, v4 */
/* .line 152 */
} // .end local v4 # "filters":Ljava/util/List;, "Ljava/util/List<Landroid/content/IntentFilter;>;"
/* .restart local v21 # "filters":Ljava/util/List;, "Ljava/util/List<Landroid/content/IntentFilter;>;" */
} // :goto_8
if ( v18 != null) { // if-eqz v18, :cond_10
/* .line 153 */
int v0 = 0; // const/4 v0, 0x0
v2 = (( android.content.pm.PackageManager ) v3 ).getIntentVerificationStatusAsUser ( v1, v0 ); // invoke-virtual {v3, v1, v0}, Landroid/content/pm/PackageManager;->getIntentVerificationStatusAsUser(Ljava/lang/String;I)I
/* .line 154 */
/* .local v2, "status":I */
if ( v2 != null) { // if-eqz v2, :cond_f
int v4 = 1; // const/4 v4, 0x1
/* if-ne v2, v4, :cond_11 */
} // :cond_f
int v4 = 1; // const/4 v4, 0x1
/* .line 156 */
} // :goto_9
(( android.util.ArraySet ) v14 ).add ( v1 ); // invoke-virtual {v14, v1}, Landroid/util/ArraySet;->add(Ljava/lang/Object;)Z
/* .line 152 */
} // .end local v2 # "status":I
} // :cond_10
int v0 = 0; // const/4 v0, 0x0
int v4 = 1; // const/4 v4, 0x1
/* .line 159 */
} // .end local v1 # "pkg":Ljava/lang/String;
} // .end local v10 # "info":Landroid/content/pm/PackageInfo;
} // .end local v18 # "add":Z
} // .end local v21 # "filters":Ljava/util/List;, "Ljava/util/List<Landroid/content/IntentFilter;>;"
} // :cond_11
} // :goto_a
/* move-object/from16 v1, p0 */
/* move-object/from16 v2, p1 */
/* move v10, v4 */
/* move v4, v0 */
/* move-object/from16 v0, v20 */
/* goto/16 :goto_1 */
/* .line 114 */
/* .restart local v10 # "info":Landroid/content/pm/PackageInfo; */
} // :cond_12
/* move-object/from16 v20, v0 */
int v0 = 0; // const/4 v0, 0x0
int v4 = 1; // const/4 v4, 0x1
/* move-object/from16 v1, p0 */
/* move-object/from16 v2, p1 */
/* move v10, v4 */
/* move v4, v0 */
/* move-object/from16 v0, v20 */
/* goto/16 :goto_1 */
/* .line 160 */
} // .end local v10 # "info":Landroid/content/pm/PackageInfo;
} // :cond_13
(( android.util.ArraySet ) v14 ).iterator ( ); // invoke-virtual {v14}, Landroid/util/ArraySet;->iterator()Ljava/util/Iterator;
v1 = } // :goto_b
if ( v1 != null) { // if-eqz v1, :cond_14
/* check-cast v1, Ljava/lang/String; */
/* .line 161 */
/* .restart local v1 # "pkg":Ljava/lang/String; */
int v2 = 3; // const/4 v2, 0x3
(( android.content.pm.PackageManager ) v3 ).updateIntentVerificationStatusAsUser ( v1, v2, v9 ); // invoke-virtual {v3, v1, v2, v9}, Landroid/content/pm/PackageManager;->updateIntentVerificationStatusAsUser(Ljava/lang/String;II)Z
/* :try_end_0 */
/* .catch Ljava/lang/Exception; {:try_start_0 ..:try_end_0} :catch_0 */
/* .line 162 */
/* nop */
} // .end local v1 # "pkg":Ljava/lang/String;
/* .line 165 */
} // .end local v5 # "applications":Ljava/util/List;, "Ljava/util/List<Landroid/content/pm/PackageInfo;>;"
} // .end local v6 # "browserIntent":Landroid/content/Intent;
} // .end local v7 # "httpIntent":Landroid/content/Intent;
} // .end local v8 # "httpsIntent":Landroid/content/Intent;
} // .end local v9 # "userId":I
} // .end local v11 # "browsers":Ljava/util/Set;, "Ljava/util/Set<Ljava/lang/String;>;"
} // .end local v12 # "httpPackages":Ljava/util/Set;, "Ljava/util/Set<Ljava/lang/String;>;"
} // .end local v13 # "httpsPackages":Ljava/util/Set;, "Ljava/util/Set<Ljava/lang/String;>;"
} // .end local v14 # "rejectPks":Landroid/util/ArraySet;, "Landroid/util/ArraySet<Ljava/lang/String;>;"
} // :cond_14
/* .line 163 */
/* :catch_0 */
/* move-exception v0 */
/* .line 164 */
/* .local v0, "e":Ljava/lang/Exception; */
(( java.lang.Exception ) v0 ).printStackTrace ( ); // invoke-virtual {v0}, Ljava/lang/Exception;->printStackTrace()V
/* .line 166 */
} // .end local v0 # "e":Ljava/lang/Exception;
} // :goto_c
return;
} // .end method
public static Boolean isDefaultBrowser ( android.content.Context p0, java.lang.String p1 ) {
/* .locals 6 */
/* .param p0, "context" # Landroid/content/Context; */
/* .param p1, "packageName" # Ljava/lang/String; */
/* .line 200 */
final String v0 = "DefaultBrowserImpl"; // const-string v0, "DefaultBrowserImpl"
android.os.Binder .clearCallingIdentity ( );
/* move-result-wide v1 */
/* .line 202 */
/* .local v1, "identity":J */
/* nop */
/* .line 203 */
try { // :try_start_0
(( android.content.Context ) p0 ).getPackageManager ( ); // invoke-virtual {p0}, Landroid/content/Context;->getPackageManager()Landroid/content/pm/PackageManager;
/* .line 204 */
v4 = android.os.UserHandle .myUserId ( );
/* .line 203 */
(( android.content.pm.PackageManager ) v3 ).getDefaultBrowserPackageNameAsUser ( v4 ); // invoke-virtual {v3, v4}, Landroid/content/pm/PackageManager;->getDefaultBrowserPackageNameAsUser(I)Ljava/lang/String;
/* .line 205 */
/* .local v3, "defaultBrowserPackage":Ljava/lang/String; */
/* new-instance v4, Ljava/lang/StringBuilder; */
/* invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V */
final String v5 = "isDefaultBrowser: packageName= "; // const-string v5, "isDefaultBrowser: packageName= "
(( java.lang.StringBuilder ) v4 ).append ( v5 ); // invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v4 ).append ( p1 ); // invoke-virtual {v4, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
final String v5 = " defaultBrowserPackage= "; // const-string v5, " defaultBrowserPackage= "
(( java.lang.StringBuilder ) v4 ).append ( v5 ); // invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v4 ).append ( v3 ); // invoke-virtual {v4, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v4 ).toString ( ); // invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
android.util.Slog .d ( v0,v4 );
/* .line 206 */
v0 = android.text.TextUtils .equals ( p1,v3 );
/* :try_end_0 */
/* .catch Ljava/lang/SecurityException; {:try_start_0 ..:try_end_0} :catch_0 */
/* .catchall {:try_start_0 ..:try_end_0} :catchall_0 */
/* .line 210 */
android.os.Binder .restoreCallingIdentity ( v1,v2 );
/* .line 206 */
/* .line 210 */
} // .end local v3 # "defaultBrowserPackage":Ljava/lang/String;
/* :catchall_0 */
/* move-exception v0 */
/* .line 207 */
/* :catch_0 */
/* move-exception v3 */
/* .line 208 */
/* .local v3, "exception":Ljava/lang/SecurityException; */
try { // :try_start_1
/* new-instance v4, Ljava/lang/StringBuilder; */
/* invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V */
final String v5 = "isDefaultBrowser failed: "; // const-string v5, "isDefaultBrowser failed: "
(( java.lang.StringBuilder ) v4 ).append ( v5 ); // invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v4 ).append ( v3 ); // invoke-virtual {v4, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v4 ).toString ( ); // invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
android.util.Slog .d ( v0,v4 );
/* :try_end_1 */
/* .catchall {:try_start_1 ..:try_end_1} :catchall_0 */
/* .line 210 */
/* nop */
} // .end local v3 # "exception":Ljava/lang/SecurityException;
android.os.Binder .restoreCallingIdentity ( v1,v2 );
/* .line 211 */
/* nop */
/* .line 212 */
int v0 = 0; // const/4 v0, 0x0
/* .line 210 */
} // :goto_0
android.os.Binder .restoreCallingIdentity ( v1,v2 );
/* .line 211 */
/* throw v0 */
} // .end method
private void lambda$checkDefaultBrowser$0 ( java.lang.String p0 ) { //synthethic
/* .locals 5 */
/* .param p1, "packageName" # Ljava/lang/String; */
/* .line 49 */
final String v0 = "miui.sec.defBrowser"; // const-string v0, "miui.sec.defBrowser"
v1 = this.mContext;
(( android.content.Context ) v1 ).getContentResolver ( ); // invoke-virtual {v1}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;
/* .line 51 */
/* .local v1, "cr":Landroid/content/ContentResolver; */
try { // :try_start_0
/* invoke-direct {p0, p1}, Lcom/miui/server/security/DefaultBrowserImpl;->checkIntentFilterVerifications(Ljava/lang/String;)V */
/* .line 52 */
int v2 = -1; // const/4 v2, -0x1
v2 = android.provider.Settings$Secure .getInt ( v1,v0,v2 );
/* .line 53 */
/* .local v2, "defBrowserCount":I */
int v3 = 1; // const/4 v3, 0x1
/* .line 54 */
/* .local v3, "allow":Z */
/* const/16 v4, 0xa */
/* if-lt v2, v4, :cond_0 */
/* const/16 v4, 0x64 */
/* if-ge v2, v4, :cond_0 */
/* .line 55 */
/* add-int/lit8 v4, v2, 0x1 */
android.provider.Settings$Secure .putInt ( v1,v0,v4 );
/* .line 56 */
int v3 = 0; // const/4 v3, 0x0
/* .line 58 */
} // :cond_0
if ( v3 != null) { // if-eqz v3, :cond_1
/* .line 59 */
v0 = this.mContext;
com.miui.server.security.DefaultBrowserImpl .setDefaultBrowser ( v0 );
/* :try_end_0 */
/* .catch Ljava/lang/Exception; {:try_start_0 ..:try_end_0} :catch_0 */
/* .line 63 */
} // .end local v2 # "defBrowserCount":I
} // .end local v3 # "allow":Z
} // :cond_1
/* .line 61 */
/* :catch_0 */
/* move-exception v0 */
/* .line 62 */
/* .local v0, "e":Ljava/lang/Exception; */
final String v2 = "DefaultBrowserImpl"; // const-string v2, "DefaultBrowserImpl"
final String v3 = "checkDefaultBrowser"; // const-string v3, "checkDefaultBrowser"
android.util.Log .e ( v2,v3,v0 );
/* .line 64 */
} // .end local v0 # "e":Ljava/lang/Exception;
} // :goto_0
return;
} // .end method
private void lambda$resetDefaultBrowser$1 ( ) { //synthethic
/* .locals 3 */
/* .line 73 */
try { // :try_start_0
/* invoke-direct {p0}, Lcom/miui/server/security/DefaultBrowserImpl;->checkIntentFilterVerifications()V */
/* .line 74 */
v0 = this.mContext;
com.miui.server.security.DefaultBrowserImpl .setDefaultBrowser ( v0 );
/* :try_end_0 */
/* .catch Ljava/lang/Exception; {:try_start_0 ..:try_end_0} :catch_0 */
/* .line 77 */
/* .line 75 */
/* :catch_0 */
/* move-exception v0 */
/* .line 76 */
/* .local v0, "e":Ljava/lang/Exception; */
final String v1 = "DefaultBrowserImpl"; // const-string v1, "DefaultBrowserImpl"
final String v2 = "resetDefaultBrowser exception"; // const-string v2, "resetDefaultBrowser exception"
android.util.Log .e ( v1,v2,v0 );
/* .line 78 */
} // .end local v0 # "e":Ljava/lang/Exception;
} // :goto_0
return;
} // .end method
private java.util.Set queryIntentPackages ( android.content.pm.PackageManager p0, android.content.Intent p1, Boolean p2, Integer p3 ) {
/* .locals 7 */
/* .param p1, "pm" # Landroid/content/pm/PackageManager; */
/* .param p2, "intent" # Landroid/content/Intent; */
/* .param p3, "allweb" # Z */
/* .param p4, "userId" # I */
/* .annotation system Ldalvik/annotation/Signature; */
/* value = { */
/* "(", */
/* "Landroid/content/pm/PackageManager;", */
/* "Landroid/content/Intent;", */
/* "ZI)", */
/* "Ljava/util/Set<", */
/* "Ljava/lang/String;", */
/* ">;" */
/* } */
} // .end annotation
/* .line 182 */
/* const/high16 v0, 0x20000 */
(( android.content.pm.PackageManager ) p1 ).queryIntentActivitiesAsUser ( p2, v0, p4 ); // invoke-virtual {p1, p2, v0, p4}, Landroid/content/pm/PackageManager;->queryIntentActivitiesAsUser(Landroid/content/Intent;II)Ljava/util/List;
/* .line 183 */
v1 = /* .local v0, "list":Ljava/util/List;, "Ljava/util/List<Landroid/content/pm/ResolveInfo;>;" */
/* .line 184 */
/* .local v1, "count":I */
/* new-instance v2, Landroid/util/ArraySet; */
/* invoke-direct {v2}, Landroid/util/ArraySet;-><init>()V */
/* .line 185 */
/* .local v2, "packages":Ljava/util/Set;, "Ljava/util/Set<Ljava/lang/String;>;" */
int v3 = 0; // const/4 v3, 0x0
/* .local v3, "i":I */
} // :goto_0
/* if-ge v3, v1, :cond_3 */
/* .line 186 */
/* check-cast v4, Landroid/content/pm/ResolveInfo; */
/* .line 187 */
/* .local v4, "info":Landroid/content/pm/ResolveInfo; */
v5 = this.activityInfo;
if ( v5 != null) { // if-eqz v5, :cond_2
if ( p3 != null) { // if-eqz p3, :cond_0
/* iget-boolean v5, v4, Landroid/content/pm/ResolveInfo;->handleAllWebDataURI:Z */
/* if-nez v5, :cond_0 */
/* .line 188 */
/* .line 190 */
} // :cond_0
v5 = this.activityInfo;
v5 = this.packageName;
/* .line 191 */
v6 = /* .local v5, "packageName":Ljava/lang/String; */
if ( v6 != null) { // if-eqz v6, :cond_1
/* .line 192 */
/* .line 194 */
} // :cond_1
/* .line 185 */
} // .end local v4 # "info":Landroid/content/pm/ResolveInfo;
} // .end local v5 # "packageName":Ljava/lang/String;
} // :cond_2
} // :goto_1
/* add-int/lit8 v3, v3, 0x1 */
/* .line 196 */
} // .end local v3 # "i":I
} // :cond_3
} // .end method
public static void setDefaultBrowser ( android.content.Context p0 ) {
/* .locals 4 */
/* .param p0, "context" # Landroid/content/Context; */
/* .line 169 */
(( android.content.Context ) p0 ).getPackageManager ( ); // invoke-virtual {p0}, Landroid/content/Context;->getPackageManager()Landroid/content/pm/PackageManager;
/* .line 171 */
/* .local v0, "pm":Landroid/content/pm/PackageManager; */
int v1 = 0; // const/4 v1, 0x0
try { // :try_start_0
(( android.content.pm.PackageManager ) v0 ).getDefaultBrowserPackageNameAsUser ( v1 ); // invoke-virtual {v0, v1}, Landroid/content/pm/PackageManager;->getDefaultBrowserPackageNameAsUser(I)Ljava/lang/String;
/* .line 172 */
/* .local v2, "defaultBrowser":Ljava/lang/String; */
v3 = android.text.TextUtils .isEmpty ( v2 );
if ( v3 != null) { // if-eqz v3, :cond_0
/* .line 173 */
v3 = com.miui.server.security.DefaultBrowserImpl.PKG_BROWSER;
(( android.content.pm.PackageManager ) v0 ).setDefaultBrowserPackageNameAsUser ( v3, v1 ); // invoke-virtual {v0, v3, v1}, Landroid/content/pm/PackageManager;->setDefaultBrowserPackageNameAsUser(Ljava/lang/String;I)Z
/* :try_end_0 */
/* .catch Ljava/lang/Exception; {:try_start_0 ..:try_end_0} :catch_0 */
/* .line 177 */
} // .end local v2 # "defaultBrowser":Ljava/lang/String;
} // :cond_0
/* .line 175 */
/* :catch_0 */
/* move-exception v1 */
/* .line 176 */
/* .local v1, "e":Ljava/lang/Exception; */
final String v2 = "DefaultBrowserImpl"; // const-string v2, "DefaultBrowserImpl"
/* const-string/jumbo v3, "setDefaultBrowser" */
android.util.Log .e ( v2,v3,v1 );
/* .line 178 */
} // .end local v1 # "e":Ljava/lang/Exception;
} // :goto_0
return;
} // .end method
/* # virtual methods */
public void checkDefaultBrowser ( java.lang.String p0 ) {
/* .locals 4 */
/* .param p1, "packageName" # Ljava/lang/String; */
/* .line 45 */
/* sget-boolean v0, Lmiui/os/Build;->IS_INTERNATIONAL_BUILD:Z */
/* if-nez v0, :cond_1 */
v0 = android.miui.AppOpsUtils .isXOptMode ( );
if ( v0 != null) { // if-eqz v0, :cond_0
/* .line 48 */
} // :cond_0
v0 = this.mHandler;
/* new-instance v1, Lcom/miui/server/security/DefaultBrowserImpl$$ExternalSyntheticLambda1; */
/* invoke-direct {v1, p0, p1}, Lcom/miui/server/security/DefaultBrowserImpl$$ExternalSyntheticLambda1;-><init>(Lcom/miui/server/security/DefaultBrowserImpl;Ljava/lang/String;)V */
/* const-wide/16 v2, 0x12c */
(( android.os.Handler ) v0 ).postDelayed ( v1, v2, v3 ); // invoke-virtual {v0, v1, v2, v3}, Landroid/os/Handler;->postDelayed(Ljava/lang/Runnable;J)Z
/* .line 65 */
return;
/* .line 46 */
} // :cond_1
} // :goto_0
return;
} // .end method
public void resetDefaultBrowser ( ) {
/* .locals 4 */
/* .line 68 */
/* sget-boolean v0, Lmiui/os/Build;->IS_INTERNATIONAL_BUILD:Z */
/* if-nez v0, :cond_1 */
v0 = android.miui.AppOpsUtils .isXOptMode ( );
if ( v0 != null) { // if-eqz v0, :cond_0
/* .line 71 */
} // :cond_0
v0 = this.mHandler;
/* new-instance v1, Lcom/miui/server/security/DefaultBrowserImpl$$ExternalSyntheticLambda0; */
/* invoke-direct {v1, p0}, Lcom/miui/server/security/DefaultBrowserImpl$$ExternalSyntheticLambda0;-><init>(Lcom/miui/server/security/DefaultBrowserImpl;)V */
/* const-wide/32 v2, 0xea60 */
(( android.os.Handler ) v0 ).postDelayed ( v1, v2, v3 ); // invoke-virtual {v0, v1, v2, v3}, Landroid/os/Handler;->postDelayed(Ljava/lang/Runnable;J)Z
/* .line 79 */
return;
/* .line 69 */
} // :cond_1
} // :goto_0
return;
} // .end method
