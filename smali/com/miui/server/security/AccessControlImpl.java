public class com.miui.server.security.AccessControlImpl {
	 /* .source "AccessControlImpl.java" */
	 /* # static fields */
	 private static final java.lang.String APPLOCK_MASK_NOTIFY;
	 public static final Long LOCK_TIME_OUT;
	 private static final java.lang.String TAG;
	 /* # instance fields */
	 private final android.content.Context mContext;
	 private final android.os.Handler mHandler;
	 private final com.miui.server.SecurityManagerService mService;
	 /* # direct methods */
	 public com.miui.server.security.AccessControlImpl ( ) {
		 /* .locals 1 */
		 /* .param p1, "service" # Lcom/miui/server/SecurityManagerService; */
		 /* .line 48 */
		 /* invoke-direct {p0}, Ljava/lang/Object;-><init>()V */
		 /* .line 49 */
		 this.mService = p1;
		 /* .line 50 */
		 v0 = this.mContext;
		 this.mContext = v0;
		 /* .line 51 */
		 v0 = this.mSecurityWriteHandler;
		 this.mHandler = v0;
		 /* .line 52 */
		 return;
	 } // .end method
	 private com.miui.server.security.SecurityUserState changeUserState ( com.miui.server.security.SecurityUserState p0 ) {
		 /* .locals 2 */
		 /* .param p1, "userState" # Lcom/miui/server/security/SecurityUserState; */
		 /* .line 419 */
		 /* iget v0, p1, Lcom/miui/server/security/SecurityUserState;->userHandle:I */
		 v0 = 		 miui.security.SecurityManager .getUserHandle ( v0 );
		 /* .line 420 */
		 /* .local v0, "userId":I */
		 v1 = this.mService;
		 (( com.miui.server.SecurityManagerService ) v1 ).getUserStateLocked ( v0 ); // invoke-virtual {v1, v0}, Lcom/miui/server/SecurityManagerService;->getUserStateLocked(I)Lcom/miui/server/security/SecurityUserState;
	 } // .end method
	 private Boolean checkAccessControlPassLockedCore ( com.miui.server.security.SecurityUserState p0, java.lang.String p1, android.content.Intent p2 ) {
		 /* .locals 9 */
		 /* .param p1, "userState" # Lcom/miui/server/security/SecurityUserState; */
		 /* .param p2, "packageName" # Ljava/lang/String; */
		 /* .param p3, "intent" # Landroid/content/Intent; */
		 /* .line 351 */
		 v0 = 		 /* invoke-direct {p0, p1}, Lcom/miui/server/security/AccessControlImpl;->getAccessControlLockMode(Lcom/miui/server/security/SecurityUserState;)I */
		 /* .line 353 */
		 /* .local v0, "lockMode":I */
		 v1 = this.mAccessControlPassPackages;
		 v1 = 		 (( java.util.HashSet ) v1 ).contains ( p2 ); // invoke-virtual {v1, p2}, Ljava/util/HashSet;->contains(Ljava/lang/Object;)Z
		 /* .line 354 */
		 /* .local v1, "pass":Z */
		 if ( v1 != null) { // if-eqz v1, :cond_1
			 int v2 = 2; // const/4 v2, 0x2
			 /* if-ne v0, v2, :cond_1 */
			 /* .line 355 */
			 v2 = this.mAccessControlLastCheck;
			 (( android.util.ArrayMap ) v2 ).get ( p2 ); // invoke-virtual {v2, p2}, Landroid/util/ArrayMap;->get(Ljava/lang/Object;)Ljava/lang/Object;
			 /* check-cast v2, Ljava/lang/Long; */
			 /* .line 356 */
			 /* .local v2, "lastTime":Ljava/lang/Long; */
			 if ( v2 != null) { // if-eqz v2, :cond_0
				 /* .line 357 */
				 android.os.SystemClock .elapsedRealtime ( );
				 /* move-result-wide v3 */
				 /* .line 358 */
				 /* .local v3, "realtime":J */
				 (( java.lang.Long ) v2 ).longValue ( ); // invoke-virtual {v2}, Ljava/lang/Long;->longValue()J
				 /* move-result-wide v5 */
				 /* sub-long v5, v3, v5 */
				 /* const-wide/32 v7, 0xea60 */
				 /* cmp-long v5, v5, v7 */
				 /* if-lez v5, :cond_0 */
				 /* .line 359 */
				 int v1 = 0; // const/4 v1, 0x0
				 /* .line 362 */
			 } // .end local v3 # "realtime":J
		 } // :cond_0
		 if ( v1 != null) { // if-eqz v1, :cond_1
			 /* .line 363 */
			 v3 = 			 android.os.Binder .getCallingPid ( );
			 com.android.server.am.ProcessUtils .getPackageNameByPid ( v3 );
			 final String v4 = "com.android.systemui"; // const-string v4, "com.android.systemui"
			 v3 = 			 (( java.lang.String ) v4 ).equals ( v3 ); // invoke-virtual {v4, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z
			 /* if-nez v3, :cond_1 */
			 /* .line 364 */
			 v3 = this.mAccessControlLastCheck;
			 android.os.SystemClock .elapsedRealtime ( );
			 /* move-result-wide v4 */
			 java.lang.Long .valueOf ( v4,v5 );
			 (( android.util.ArrayMap ) v3 ).put ( p2, v4 ); // invoke-virtual {v3, p2, v4}, Landroid/util/ArrayMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;
			 /* .line 365 */
			 /* iget v3, p1, Lcom/miui/server/security/SecurityUserState;->userHandle:I */
			 /* invoke-direct {p0, p2, v3}, Lcom/miui/server/security/AccessControlImpl;->scheduleForMaskObserver(Ljava/lang/String;I)V */
			 /* .line 369 */
		 } // .end local v2 # "lastTime":Ljava/lang/Long;
	 } // :cond_1
	 int v2 = 1; // const/4 v2, 0x1
	 /* if-nez v1, :cond_2 */
	 /* if-ne v0, v2, :cond_2 */
	 /* .line 370 */
	 v3 = 	 /* invoke-direct {p0, p1}, Lcom/miui/server/security/AccessControlImpl;->getAccessControlLockConvenient(Lcom/miui/server/security/SecurityUserState;)Z */
	 if ( v3 != null) { // if-eqz v3, :cond_2
		 /* .line 371 */
		 v3 = 		 /* invoke-direct {p0, p1}, Lcom/miui/server/security/AccessControlImpl;->isPackageAccessControlPass(Lcom/miui/server/security/SecurityUserState;)Z */
		 if ( v3 != null) { // if-eqz v3, :cond_2
			 /* .line 372 */
			 int v1 = 1; // const/4 v1, 0x1
			 /* .line 375 */
		 } // :cond_2
		 /* if-nez v1, :cond_3 */
		 v3 = this.mService;
		 v3 = this.mAccessController;
		 /* .line 376 */
		 v4 = 		 android.os.Binder .getCallingPid ( );
		 com.android.server.am.ProcessUtils .getPackageNameByPid ( v4 );
		 /* .line 375 */
		 v3 = 		 (( com.miui.server.AccessController ) v3 ).skipActivity ( p3, v4 ); // invoke-virtual {v3, p3, v4}, Lcom/miui/server/AccessController;->skipActivity(Landroid/content/Intent;Ljava/lang/String;)Z
		 if ( v3 != null) { // if-eqz v3, :cond_3
			 /* .line 377 */
			 int v1 = 1; // const/4 v1, 0x1
			 /* .line 379 */
		 } // :cond_3
		 /* if-nez v1, :cond_4 */
		 v3 = this.mService;
		 v3 = this.mAccessController;
		 v2 = 		 (( com.miui.server.AccessController ) v3 ).filterIntentLocked ( v2, p2, p3 ); // invoke-virtual {v3, v2, p2, p3}, Lcom/miui/server/AccessController;->filterIntentLocked(ZLjava/lang/String;Landroid/content/Intent;)Z
		 if ( v2 != null) { // if-eqz v2, :cond_4
			 /* .line 380 */
			 int v1 = 1; // const/4 v1, 0x1
			 /* .line 382 */
		 } // :cond_4
	 } // .end method
	 private void clearPassPackages ( Integer p0 ) {
		 /* .locals 4 */
		 /* .param p1, "userId" # I */
		 /* .line 435 */
		 v0 = 		 miui.securityspace.ConfigUtils .isSupportXSpace ( );
		 if ( v0 != null) { // if-eqz v0, :cond_1
			 /* const/16 v0, 0x3e7 */
			 if ( p1 != null) { // if-eqz p1, :cond_0
				 /* if-ne v0, p1, :cond_1 */
				 /* .line 437 */
			 } // :cond_0
			 v1 = this.mService;
			 int v2 = 0; // const/4 v2, 0x0
			 (( com.miui.server.SecurityManagerService ) v1 ).getUserStateLocked ( v2 ); // invoke-virtual {v1, v2}, Lcom/miui/server/SecurityManagerService;->getUserStateLocked(I)Lcom/miui/server/security/SecurityUserState;
			 /* .line 438 */
			 /* .local v1, "userStateOwner":Lcom/miui/server/security/SecurityUserState; */
			 v2 = this.mService;
			 (( com.miui.server.SecurityManagerService ) v2 ).getUserStateLocked ( v0 ); // invoke-virtual {v2, v0}, Lcom/miui/server/SecurityManagerService;->getUserStateLocked(I)Lcom/miui/server/security/SecurityUserState;
			 /* .line 439 */
			 /* .local v0, "userStateXSpace":Lcom/miui/server/security/SecurityUserState; */
			 v2 = this.mAccessControlPassPackages;
			 /* .line 440 */
			 /* .local v2, "passPackagesOwner":Ljava/util/HashSet;, "Ljava/util/HashSet<Ljava/lang/String;>;" */
			 v3 = this.mAccessControlPassPackages;
			 /* .line 441 */
			 /* .local v3, "passPackagesXSpace":Ljava/util/HashSet;, "Ljava/util/HashSet<Ljava/lang/String;>;" */
			 (( java.util.HashSet ) v2 ).clear ( ); // invoke-virtual {v2}, Ljava/util/HashSet;->clear()V
			 /* .line 442 */
			 (( java.util.HashSet ) v3 ).clear ( ); // invoke-virtual {v3}, Ljava/util/HashSet;->clear()V
			 /* .line 443 */
		 } // .end local v0 # "userStateXSpace":Lcom/miui/server/security/SecurityUserState;
	 } // .end local v1 # "userStateOwner":Lcom/miui/server/security/SecurityUserState;
} // .end local v2 # "passPackagesOwner":Ljava/util/HashSet;, "Ljava/util/HashSet<Ljava/lang/String;>;"
} // .end local v3 # "passPackagesXSpace":Ljava/util/HashSet;, "Ljava/util/HashSet<Ljava/lang/String;>;"
/* .line 444 */
} // :cond_1
v0 = this.mService;
(( com.miui.server.SecurityManagerService ) v0 ).getUserStateLocked ( p1 ); // invoke-virtual {v0, p1}, Lcom/miui/server/SecurityManagerService;->getUserStateLocked(I)Lcom/miui/server/security/SecurityUserState;
v0 = this.mAccessControlPassPackages;
/* .line 445 */
/* .local v0, "passPackages":Ljava/util/HashSet;, "Ljava/util/HashSet<Ljava/lang/String;>;" */
(( java.util.HashSet ) v0 ).clear ( ); // invoke-virtual {v0}, Ljava/util/HashSet;->clear()V
/* .line 447 */
} // .end local v0 # "passPackages":Ljava/util/HashSet;, "Ljava/util/HashSet<Ljava/lang/String;>;"
} // :goto_0
(( com.miui.server.security.AccessControlImpl ) p0 ).updateMaskObserverValues ( ); // invoke-virtual {p0}, Lcom/miui/server/security/AccessControlImpl;->updateMaskObserverValues()V
/* .line 448 */
return;
} // .end method
private Boolean getAccessControlLockConvenient ( com.miui.server.security.SecurityUserState p0 ) {
/* .locals 2 */
/* .param p1, "userState" # Lcom/miui/server/security/SecurityUserState; */
/* .line 413 */
/* invoke-direct {p0, p1}, Lcom/miui/server/security/AccessControlImpl;->changeUserState(Lcom/miui/server/security/SecurityUserState;)Lcom/miui/server/security/SecurityUserState; */
/* .line 414 */
/* .local v0, "transferUserState":Lcom/miui/server/security/SecurityUserState; */
v1 = this.mService;
v1 = this.mSettingsObserver;
(( com.miui.server.security.SecuritySettingsObserver ) v1 ).initAccessControlSettingsLocked ( v0 ); // invoke-virtual {v1, v0}, Lcom/miui/server/security/SecuritySettingsObserver;->initAccessControlSettingsLocked(Lcom/miui/server/security/SecurityUserState;)V
/* .line 415 */
/* iget-boolean v1, v0, Lcom/miui/server/security/SecurityUserState;->mAccessControlLockConvenient:Z */
} // .end method
private Integer getAccessControlLockMode ( com.miui.server.security.SecurityUserState p0 ) {
/* .locals 2 */
/* .param p1, "userState" # Lcom/miui/server/security/SecurityUserState; */
/* .line 407 */
/* invoke-direct {p0, p1}, Lcom/miui/server/security/AccessControlImpl;->changeUserState(Lcom/miui/server/security/SecurityUserState;)Lcom/miui/server/security/SecurityUserState; */
/* .line 408 */
/* .local v0, "transferUserState":Lcom/miui/server/security/SecurityUserState; */
v1 = this.mService;
v1 = this.mSettingsObserver;
(( com.miui.server.security.SecuritySettingsObserver ) v1 ).initAccessControlSettingsLocked ( v0 ); // invoke-virtual {v1, v0}, Lcom/miui/server/security/SecuritySettingsObserver;->initAccessControlSettingsLocked(Lcom/miui/server/security/SecurityUserState;)V
/* .line 409 */
/* iget v1, v0, Lcom/miui/server/security/SecurityUserState;->mAccessControlLockMode:I */
} // .end method
private Boolean isPackageAccessControlPass ( com.miui.server.security.SecurityUserState p0 ) {
/* .locals 6 */
/* .param p1, "userState" # Lcom/miui/server/security/SecurityUserState; */
/* .line 386 */
v0 = miui.securityspace.ConfigUtils .isSupportXSpace ( );
int v1 = 1; // const/4 v1, 0x1
int v2 = 0; // const/4 v2, 0x0
if ( v0 != null) { // if-eqz v0, :cond_2
/* iget v0, p1, Lcom/miui/server/security/SecurityUserState;->userHandle:I */
/* const/16 v3, 0x3e7 */
/* if-eq v0, v3, :cond_0 */
/* iget v0, p1, Lcom/miui/server/security/SecurityUserState;->userHandle:I */
/* if-nez v0, :cond_2 */
/* .line 388 */
} // :cond_0
v0 = this.mService;
(( com.miui.server.SecurityManagerService ) v0 ).getUserStateLocked ( v2 ); // invoke-virtual {v0, v2}, Lcom/miui/server/SecurityManagerService;->getUserStateLocked(I)Lcom/miui/server/security/SecurityUserState;
/* .line 389 */
/* .local v0, "userStateOwner":Lcom/miui/server/security/SecurityUserState; */
v4 = this.mService;
(( com.miui.server.SecurityManagerService ) v4 ).getUserStateLocked ( v3 ); // invoke-virtual {v4, v3}, Lcom/miui/server/SecurityManagerService;->getUserStateLocked(I)Lcom/miui/server/security/SecurityUserState;
/* .line 390 */
/* .local v3, "userStateXSpace":Lcom/miui/server/security/SecurityUserState; */
v4 = this.mAccessControlPassPackages;
v4 = (( java.util.HashSet ) v4 ).size ( ); // invoke-virtual {v4}, Ljava/util/HashSet;->size()I
v5 = this.mAccessControlPassPackages;
v5 = (( java.util.HashSet ) v5 ).size ( ); // invoke-virtual {v5}, Ljava/util/HashSet;->size()I
/* add-int/2addr v4, v5 */
/* if-lez v4, :cond_1 */
} // :cond_1
/* move v1, v2 */
} // :goto_0
/* .line 392 */
} // .end local v0 # "userStateOwner":Lcom/miui/server/security/SecurityUserState;
} // .end local v3 # "userStateXSpace":Lcom/miui/server/security/SecurityUserState;
} // :cond_2
v0 = this.mAccessControlPassPackages;
v0 = (( java.util.HashSet ) v0 ).size ( ); // invoke-virtual {v0}, Ljava/util/HashSet;->size()I
/* if-lez v0, :cond_3 */
} // :cond_3
/* move v1, v2 */
} // :goto_1
} // .end method
private void removeAccessControlPassLocked ( com.miui.server.security.SecurityUserState p0, java.lang.String p1 ) {
/* .locals 1 */
/* .param p1, "userState" # Lcom/miui/server/security/SecurityUserState; */
/* .param p2, "packageName" # Ljava/lang/String; */
/* .line 397 */
final String v0 = "*"; // const-string v0, "*"
v0 = (( java.lang.String ) v0 ).equals ( p2 ); // invoke-virtual {v0, p2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z
if ( v0 != null) { // if-eqz v0, :cond_0
/* .line 398 */
v0 = this.mAccessControlPassPackages;
(( java.util.HashSet ) v0 ).clear ( ); // invoke-virtual {v0}, Ljava/util/HashSet;->clear()V
/* .line 399 */
v0 = this.mAccessControlLastCheck;
(( android.util.ArrayMap ) v0 ).clear ( ); // invoke-virtual {v0}, Landroid/util/ArrayMap;->clear()V
/* .line 401 */
} // :cond_0
v0 = this.mAccessControlPassPackages;
(( java.util.HashSet ) v0 ).remove ( p2 ); // invoke-virtual {v0, p2}, Ljava/util/HashSet;->remove(Ljava/lang/Object;)Z
/* .line 403 */
} // :goto_0
(( com.miui.server.security.AccessControlImpl ) p0 ).updateMaskObserverValues ( ); // invoke-virtual {p0}, Lcom/miui/server/security/AccessControlImpl;->updateMaskObserverValues()V
/* .line 404 */
return;
} // .end method
private void scheduleForMaskObserver ( java.lang.String p0, Integer p1 ) {
/* .locals 5 */
/* .param p1, "pkg" # Ljava/lang/String; */
/* .param p2, "userHandle" # I */
/* .line 429 */
/* new-instance v0, Ljava/lang/StringBuilder; */
/* invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V */
(( java.lang.StringBuilder ) v0 ).append ( p1 ); // invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
final String v1 = "_"; // const-string v1, "_"
(( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v0 ).append ( p2 ); // invoke-virtual {v0, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v0 ).toString ( ); // invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
/* .line 430 */
/* .local v0, "token":Ljava/lang/String; */
v1 = this.mHandler;
(( android.os.Handler ) v1 ).removeCallbacksAndEqualMessages ( v0 ); // invoke-virtual {v1, v0}, Landroid/os/Handler;->removeCallbacksAndEqualMessages(Ljava/lang/Object;)V
/* .line 431 */
v1 = this.mHandler;
/* new-instance v2, Lcom/miui/server/security/AccessControlImpl$$ExternalSyntheticLambda0; */
/* invoke-direct {v2, p0}, Lcom/miui/server/security/AccessControlImpl$$ExternalSyntheticLambda0;-><init>(Lcom/miui/server/security/AccessControlImpl;)V */
/* const-wide/32 v3, 0xea60 */
(( android.os.Handler ) v1 ).postDelayed ( v2, v0, v3, v4 ); // invoke-virtual {v1, v2, v0, v3, v4}, Landroid/os/Handler;->postDelayed(Ljava/lang/Runnable;Ljava/lang/Object;J)Z
/* .line 432 */
return;
} // .end method
/* # virtual methods */
public Integer activityResume ( android.content.Intent p0 ) {
/* .locals 22 */
/* .param p1, "intent" # Landroid/content/Intent; */
/* .line 106 */
/* move-object/from16 v1, p0 */
/* move-object/from16 v2, p1 */
int v0 = 0; // const/4 v0, 0x0
/* if-nez v2, :cond_0 */
/* .line 107 */
/* .line 110 */
} // :cond_0
/* invoke-virtual/range {p1 ..p1}, Landroid/content/Intent;->getComponent()Landroid/content/ComponentName; */
/* .line 111 */
/* .local v3, "componentName":Landroid/content/ComponentName; */
/* if-nez v3, :cond_1 */
/* .line 112 */
/* .line 115 */
} // :cond_1
(( android.content.ComponentName ) v3 ).getPackageName ( ); // invoke-virtual {v3}, Landroid/content/ComponentName;->getPackageName()Ljava/lang/String;
/* .line 116 */
/* .local v4, "packageName":Ljava/lang/String; */
/* if-nez v4, :cond_2 */
/* .line 117 */
/* .line 120 */
} // :cond_2
v5 = android.os.Binder .getCallingUid ( );
/* .line 121 */
/* .local v5, "callingUid":I */
v6 = android.os.UserHandle .getUserId ( v5 );
/* .line 122 */
/* .local v6, "userId":I */
v7 = this.mService;
v7 = this.mUserStateLock;
/* monitor-enter v7 */
/* .line 123 */
try { // :try_start_0
v8 = this.mService;
(( com.miui.server.SecurityManagerService ) v8 ).getUserStateLocked ( v6 ); // invoke-virtual {v8, v6}, Lcom/miui/server/SecurityManagerService;->getUserStateLocked(I)Lcom/miui/server/security/SecurityUserState;
/* .line 124 */
/* .local v8, "userState":Lcom/miui/server/security/SecurityUserState; */
v9 = (( com.miui.server.security.AccessControlImpl ) v1 ).getAccessControlEnabledLocked ( v8 ); // invoke-virtual {v1, v8}, Lcom/miui/server/security/AccessControlImpl;->getAccessControlEnabledLocked(Lcom/miui/server/security/SecurityUserState;)Z
/* .line 126 */
/* .local v9, "enabled":Z */
/* if-nez v9, :cond_3 */
/* .line 127 */
/* monitor-exit v7 */
/* .line 129 */
} // :cond_3
/* const-class v10, Landroid/content/pm/PackageManagerInternal; */
com.android.server.LocalServices .getService ( v10 );
/* check-cast v10, Landroid/content/pm/PackageManagerInternal; */
/* const-wide/16 v11, 0x0 */
v10 = (( android.content.pm.PackageManagerInternal ) v10 ).getPackageUid ( v4, v11, v12, v6 ); // invoke-virtual {v10, v4, v11, v12, v6}, Landroid/content/pm/PackageManagerInternal;->getPackageUid(Ljava/lang/String;JI)I
/* .line 130 */
/* .local v10, "packageUid":I */
/* if-eq v5, v10, :cond_4 */
/* .line 131 */
/* monitor-exit v7 */
/* .line 134 */
} // :cond_4
int v0 = 1; // const/4 v0, 0x1
/* .line 135 */
/* .local v0, "result":I */
v11 = /* invoke-direct {v1, v8}, Lcom/miui/server/security/AccessControlImpl;->getAccessControlLockMode(Lcom/miui/server/security/SecurityUserState;)I */
/* .line 137 */
/* .local v11, "lockMode":I */
v12 = this.mLastResumePackage;
/* .line 138 */
/* .local v12, "oldResumePackage":Ljava/lang/String; */
this.mLastResumePackage = v4;
/* .line 139 */
v13 = this.mAccessControlPassPackages;
/* .line 140 */
/* .local v13, "passPackages":Ljava/util/HashSet;, "Ljava/util/HashSet<Ljava/lang/String;>;" */
int v14 = 2; // const/4 v14, 0x2
/* if-ne v11, v14, :cond_5 */
/* .line 141 */
if ( v12 != null) { // if-eqz v12, :cond_5
v15 = (( java.util.HashSet ) v13 ).contains ( v12 ); // invoke-virtual {v13, v12}, Ljava/util/HashSet;->contains(Ljava/lang/Object;)Z
if ( v15 != null) { // if-eqz v15, :cond_5
/* .line 142 */
v15 = this.mAccessControlLastCheck;
android.os.SystemClock .elapsedRealtime ( );
/* move-result-wide v16 */
/* invoke-static/range {v16 ..v17}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long; */
(( android.util.ArrayMap ) v15 ).put ( v12, v14 ); // invoke-virtual {v15, v12, v14}, Landroid/util/ArrayMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;
/* .line 143 */
/* invoke-direct {v1, v4, v6}, Lcom/miui/server/security/AccessControlImpl;->scheduleForMaskObserver(Ljava/lang/String;I)V */
/* .line 146 */
} // :cond_5
v14 = this.mService;
v15 = this.mPackages;
(( com.miui.server.SecurityManagerService ) v14 ).getPackageSetting ( v15, v4 ); // invoke-virtual {v14, v15, v4}, Lcom/miui/server/SecurityManagerService;->getPackageSetting(Ljava/util/HashMap;Ljava/lang/String;)Lcom/miui/server/security/SecurityPackageSettings;
/* .line 147 */
/* .local v14, "ps":Lcom/miui/server/security/SecurityPackageSettings; */
/* iget-boolean v15, v14, Lcom/miui/server/security/SecurityPackageSettings;->accessControl:Z */
/* if-nez v15, :cond_7 */
/* .line 148 */
/* if-nez v11, :cond_6 */
/* .line 149 */
/* invoke-direct {v1, v6}, Lcom/miui/server/security/AccessControlImpl;->clearPassPackages(I)V */
/* .line 151 */
} // :cond_6
/* monitor-exit v7 */
/* .line 153 */
} // :cond_7
int v15 = 2; // const/4 v15, 0x2
/* or-int/2addr v0, v15 */
/* .line 155 */
v16 = (( java.util.HashSet ) v13 ).contains ( v4 ); // invoke-virtual {v13, v4}, Ljava/util/HashSet;->contains(Ljava/lang/Object;)Z
if ( v16 != null) { // if-eqz v16, :cond_b
/* .line 156 */
/* if-ne v11, v15, :cond_9 */
/* .line 157 */
v15 = this.mAccessControlLastCheck;
(( android.util.ArrayMap ) v15 ).get ( v4 ); // invoke-virtual {v15, v4}, Landroid/util/ArrayMap;->get(Ljava/lang/Object;)Ljava/lang/Object;
/* check-cast v15, Ljava/lang/Long; */
/* .line 158 */
/* .local v15, "lastTime":Ljava/lang/Long; */
if ( v15 != null) { // if-eqz v15, :cond_8
/* .line 159 */
android.os.SystemClock .elapsedRealtime ( );
/* move-result-wide v16 */
/* .line 160 */
/* .local v16, "realtime":J */
(( java.lang.Long ) v15 ).longValue ( ); // invoke-virtual {v15}, Ljava/lang/Long;->longValue()J
/* move-result-wide v18 */
/* sub-long v18, v16, v18 */
/* const-wide/32 v20, 0xea60 */
/* cmp-long v18, v18, v20 */
/* if-gez v18, :cond_8 */
/* .line 161 */
/* or-int/lit8 v0, v0, 0x4 */
/* .line 162 */
/* monitor-exit v7 */
/* .line 165 */
} // .end local v16 # "realtime":J
} // :cond_8
(( java.util.HashSet ) v13 ).remove ( v4 ); // invoke-virtual {v13, v4}, Ljava/util/HashSet;->remove(Ljava/lang/Object;)Z
/* .line 166 */
/* invoke-virtual/range {p0 ..p0}, Lcom/miui/server/security/AccessControlImpl;->updateMaskObserverValues()V */
/* .line 167 */
} // .end local v15 # "lastTime":Ljava/lang/Long;
/* .line 168 */
} // :cond_9
/* or-int/lit8 v0, v0, 0x4 */
/* .line 169 */
/* if-nez v11, :cond_a */
/* .line 170 */
/* invoke-direct {v1, v6}, Lcom/miui/server/security/AccessControlImpl;->clearPassPackages(I)V */
/* .line 171 */
(( java.util.HashSet ) v13 ).add ( v4 ); // invoke-virtual {v13, v4}, Ljava/util/HashSet;->add(Ljava/lang/Object;)Z
/* .line 172 */
/* invoke-virtual/range {p0 ..p0}, Lcom/miui/server/security/AccessControlImpl;->updateMaskObserverValues()V */
/* .line 174 */
} // :cond_a
/* monitor-exit v7 */
/* .line 178 */
} // :cond_b
} // :goto_0
/* if-nez v11, :cond_c */
/* .line 179 */
/* invoke-direct {v1, v6}, Lcom/miui/server/security/AccessControlImpl;->clearPassPackages(I)V */
/* .line 181 */
} // :cond_c
v15 = this.mAccessControlCanceled;
v15 = (( android.util.ArraySet ) v15 ).contains ( v4 ); // invoke-virtual {v15, v4}, Landroid/util/ArraySet;->contains(Ljava/lang/Object;)Z
if ( v15 != null) { // if-eqz v15, :cond_d
/* .line 182 */
/* or-int/lit8 v0, v0, 0x8 */
/* .line 183 */
/* monitor-exit v7 */
/* .line 186 */
} // :cond_d
int v15 = 1; // const/4 v15, 0x1
/* if-ne v11, v15, :cond_e */
/* .line 187 */
v16 = /* invoke-direct {v1, v8}, Lcom/miui/server/security/AccessControlImpl;->getAccessControlLockConvenient(Lcom/miui/server/security/SecurityUserState;)Z */
if ( v16 != null) { // if-eqz v16, :cond_e
/* .line 188 */
v16 = /* invoke-direct {v1, v8}, Lcom/miui/server/security/AccessControlImpl;->isPackageAccessControlPass(Lcom/miui/server/security/SecurityUserState;)Z */
/* if-nez v16, :cond_f */
} // :cond_e
v15 = this.mService;
v15 = this.mAccessController;
/* .line 189 */
v15 = (( com.miui.server.AccessController ) v15 ).skipActivity ( v2, v4 ); // invoke-virtual {v15, v2, v4}, Lcom/miui/server/AccessController;->skipActivity(Landroid/content/Intent;Ljava/lang/String;)Z
/* if-nez v15, :cond_f */
v15 = this.mService;
v15 = this.mAccessController;
/* .line 190 */
int v1 = 1; // const/4 v1, 0x1
v1 = (( com.miui.server.AccessController ) v15 ).filterIntentLocked ( v1, v4, v2 ); // invoke-virtual {v15, v1, v4, v2}, Lcom/miui/server/AccessController;->filterIntentLocked(ZLjava/lang/String;Landroid/content/Intent;)Z
if ( v1 != null) { // if-eqz v1, :cond_10
/* .line 191 */
} // :cond_f
/* or-int/lit8 v0, v0, 0x4 */
/* .line 194 */
} // :cond_10
/* monitor-exit v7 */
/* .line 195 */
} // .end local v0 # "result":I
} // .end local v8 # "userState":Lcom/miui/server/security/SecurityUserState;
} // .end local v9 # "enabled":Z
} // .end local v10 # "packageUid":I
} // .end local v11 # "lockMode":I
} // .end local v12 # "oldResumePackage":Ljava/lang/String;
} // .end local v13 # "passPackages":Ljava/util/HashSet;, "Ljava/util/HashSet<Ljava/lang/String;>;"
} // .end local v14 # "ps":Lcom/miui/server/security/SecurityPackageSettings;
/* :catchall_0 */
/* move-exception v0 */
/* monitor-exit v7 */
/* :try_end_0 */
/* .catchall {:try_start_0 ..:try_end_0} :catchall_0 */
/* throw v0 */
} // .end method
public void addAccessControlPassForUser ( java.lang.String p0, Integer p1 ) {
/* .locals 6 */
/* .param p1, "packageName" # Ljava/lang/String; */
/* .param p2, "userId" # I */
/* .line 199 */
v0 = this.mService;
v0 = this.mUserStateLock;
/* monitor-enter v0 */
/* .line 200 */
try { // :try_start_0
v1 = this.mService;
(( com.miui.server.SecurityManagerService ) v1 ).getUserStateLocked ( p2 ); // invoke-virtual {v1, p2}, Lcom/miui/server/SecurityManagerService;->getUserStateLocked(I)Lcom/miui/server/security/SecurityUserState;
/* .line 201 */
/* .local v1, "userState":Lcom/miui/server/security/SecurityUserState; */
v2 = /* invoke-direct {p0, v1}, Lcom/miui/server/security/AccessControlImpl;->getAccessControlLockMode(Lcom/miui/server/security/SecurityUserState;)I */
/* .line 202 */
/* .local v2, "lockMode":I */
int v3 = 2; // const/4 v3, 0x2
/* if-ne v2, v3, :cond_0 */
/* .line 203 */
v3 = this.mAccessControlLastCheck;
android.os.SystemClock .elapsedRealtime ( );
/* move-result-wide v4 */
java.lang.Long .valueOf ( v4,v5 );
(( android.util.ArrayMap ) v3 ).put ( p1, v4 ); // invoke-virtual {v3, p1, v4}, Landroid/util/ArrayMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;
/* .line 204 */
/* invoke-direct {p0, p1, p2}, Lcom/miui/server/security/AccessControlImpl;->scheduleForMaskObserver(Ljava/lang/String;I)V */
/* .line 206 */
} // :cond_0
(( com.miui.server.security.AccessControlImpl ) p0 ).updateMaskObserverValues ( ); // invoke-virtual {p0}, Lcom/miui/server/security/AccessControlImpl;->updateMaskObserverValues()V
/* .line 208 */
} // :goto_0
v3 = this.mAccessControlPassPackages;
(( java.util.HashSet ) v3 ).add ( p1 ); // invoke-virtual {v3, p1}, Ljava/util/HashSet;->add(Ljava/lang/Object;)Z
/* .line 209 */
/* nop */
} // .end local v1 # "userState":Lcom/miui/server/security/SecurityUserState;
} // .end local v2 # "lockMode":I
/* monitor-exit v0 */
/* .line 210 */
return;
/* .line 209 */
/* :catchall_0 */
/* move-exception v1 */
/* monitor-exit v0 */
/* :try_end_0 */
/* .catchall {:try_start_0 ..:try_end_0} :catchall_0 */
/* throw v1 */
} // .end method
public Boolean checkAccessControlPassLocked ( java.lang.String p0, android.content.Intent p1, Integer p2 ) {
/* .locals 4 */
/* .param p1, "packageName" # Ljava/lang/String; */
/* .param p2, "intent" # Landroid/content/Intent; */
/* .param p3, "userId" # I */
/* .line 235 */
v0 = this.mService;
v0 = this.mUserStateLock;
/* monitor-enter v0 */
/* .line 236 */
try { // :try_start_0
v1 = this.mService;
(( com.miui.server.SecurityManagerService ) v1 ).getUserStateLocked ( p3 ); // invoke-virtual {v1, p3}, Lcom/miui/server/SecurityManagerService;->getUserStateLocked(I)Lcom/miui/server/security/SecurityUserState;
/* .line 237 */
/* .local v1, "userState":Lcom/miui/server/security/SecurityUserState; */
v2 = this.mService;
v3 = this.mPackages;
(( com.miui.server.SecurityManagerService ) v2 ).getPackageSetting ( v3, p1 ); // invoke-virtual {v2, v3, p1}, Lcom/miui/server/SecurityManagerService;->getPackageSetting(Ljava/util/HashMap;Ljava/lang/String;)Lcom/miui/server/security/SecurityPackageSettings;
/* .line 238 */
/* .local v2, "ps":Lcom/miui/server/security/SecurityPackageSettings; */
/* iget-boolean v3, v2, Lcom/miui/server/security/SecurityPackageSettings;->accessControl:Z */
/* if-nez v3, :cond_0 */
/* .line 239 */
/* monitor-exit v0 */
int v0 = 1; // const/4 v0, 0x1
/* .line 241 */
} // :cond_0
v3 = /* invoke-direct {p0, v1, p1, p2}, Lcom/miui/server/security/AccessControlImpl;->checkAccessControlPassLockedCore(Lcom/miui/server/security/SecurityUserState;Ljava/lang/String;Landroid/content/Intent;)Z */
/* monitor-exit v0 */
/* .line 242 */
} // .end local v1 # "userState":Lcom/miui/server/security/SecurityUserState;
} // .end local v2 # "ps":Lcom/miui/server/security/SecurityPackageSettings;
/* :catchall_0 */
/* move-exception v1 */
/* monitor-exit v0 */
/* :try_end_0 */
/* .catchall {:try_start_0 ..:try_end_0} :catchall_0 */
/* throw v1 */
} // .end method
public void finishAccessControl ( java.lang.String p0, Integer p1 ) {
/* .locals 6 */
/* .param p1, "packageName" # Ljava/lang/String; */
/* .param p2, "userId" # I */
/* .line 330 */
/* if-nez p1, :cond_0 */
/* .line 331 */
return;
/* .line 333 */
} // :cond_0
v0 = this.mService;
v0 = this.mUserStateLock;
/* monitor-enter v0 */
/* .line 334 */
try { // :try_start_0
v1 = this.mService;
(( com.miui.server.SecurityManagerService ) v1 ).getUserStateLocked ( p2 ); // invoke-virtual {v1, p2}, Lcom/miui/server/SecurityManagerService;->getUserStateLocked(I)Lcom/miui/server/security/SecurityUserState;
/* .line 335 */
/* .local v1, "userState":Lcom/miui/server/security/SecurityUserState; */
v2 = this.mAccessControlCanceled;
(( android.util.ArraySet ) v2 ).add ( p1 ); // invoke-virtual {v2, p1}, Landroid/util/ArraySet;->add(Ljava/lang/Object;)Z
/* .line 336 */
v2 = this.mHandler;
int v3 = 4; // const/4 v3, 0x4
(( android.os.Handler ) v2 ).obtainMessage ( v3 ); // invoke-virtual {v2, v3}, Landroid/os/Handler;->obtainMessage(I)Landroid/os/Message;
/* .line 337 */
/* .local v2, "msg":Landroid/os/Message; */
/* iput p2, v2, Landroid/os/Message;->arg1:I */
/* .line 338 */
this.obj = p1;
/* .line 339 */
v3 = this.mHandler;
/* const-wide/16 v4, 0x1f4 */
(( android.os.Handler ) v3 ).sendMessageDelayed ( v2, v4, v5 ); // invoke-virtual {v3, v2, v4, v5}, Landroid/os/Handler;->sendMessageDelayed(Landroid/os/Message;J)Z
/* .line 340 */
/* nop */
} // .end local v1 # "userState":Lcom/miui/server/security/SecurityUserState;
} // .end local v2 # "msg":Landroid/os/Message;
/* monitor-exit v0 */
/* .line 341 */
return;
/* .line 340 */
/* :catchall_0 */
/* move-exception v1 */
/* monitor-exit v0 */
/* :try_end_0 */
/* .catchall {:try_start_0 ..:try_end_0} :catchall_0 */
/* throw v1 */
} // .end method
public Boolean getAccessControlEnabledLocked ( com.miui.server.security.SecurityUserState p0 ) {
/* .locals 2 */
/* .param p1, "userState" # Lcom/miui/server/security/SecurityUserState; */
/* .line 344 */
/* invoke-direct {p0, p1}, Lcom/miui/server/security/AccessControlImpl;->changeUserState(Lcom/miui/server/security/SecurityUserState;)Lcom/miui/server/security/SecurityUserState; */
/* .line 345 */
/* .local v0, "transferUserState":Lcom/miui/server/security/SecurityUserState; */
v1 = this.mService;
v1 = this.mSettingsObserver;
(( com.miui.server.security.SecuritySettingsObserver ) v1 ).initAccessControlSettingsLocked ( v0 ); // invoke-virtual {v1, v0}, Lcom/miui/server/security/SecuritySettingsObserver;->initAccessControlSettingsLocked(Lcom/miui/server/security/SecurityUserState;)V
/* .line 346 */
/* iget-boolean v1, v0, Lcom/miui/server/security/SecurityUserState;->mAccessControlEnabled:Z */
} // .end method
public Boolean getApplicationAccessControlEnabledLocked ( java.lang.String p0, Integer p1 ) {
/* .locals 4 */
/* .param p1, "packageName" # Ljava/lang/String; */
/* .param p2, "userId" # I */
/* .line 213 */
v0 = this.mService;
v0 = this.mUserStateLock;
/* monitor-enter v0 */
/* .line 214 */
try { // :try_start_0
v1 = this.mService;
(( com.miui.server.SecurityManagerService ) v1 ).getUserStateLocked ( p2 ); // invoke-virtual {v1, p2}, Lcom/miui/server/SecurityManagerService;->getUserStateLocked(I)Lcom/miui/server/security/SecurityUserState;
/* :try_end_0 */
/* .catchall {:try_start_0 ..:try_end_0} :catchall_0 */
/* .line 216 */
/* .local v1, "userState":Lcom/miui/server/security/SecurityUserState; */
try { // :try_start_1
v2 = this.mService;
v3 = this.mPackages;
(( com.miui.server.SecurityManagerService ) v2 ).getPackageSetting ( v3, p1 ); // invoke-virtual {v2, v3, p1}, Lcom/miui/server/SecurityManagerService;->getPackageSetting(Ljava/util/HashMap;Ljava/lang/String;)Lcom/miui/server/security/SecurityPackageSettings;
/* .line 217 */
/* .local v2, "ps":Lcom/miui/server/security/SecurityPackageSettings; */
/* iget-boolean v3, v2, Lcom/miui/server/security/SecurityPackageSettings;->accessControl:Z */
/* :try_end_1 */
/* .catch Ljava/lang/Exception; {:try_start_1 ..:try_end_1} :catch_0 */
/* .catchall {:try_start_1 ..:try_end_1} :catchall_0 */
try { // :try_start_2
/* monitor-exit v0 */
/* .line 218 */
} // .end local v2 # "ps":Lcom/miui/server/security/SecurityPackageSettings;
/* :catch_0 */
/* move-exception v2 */
/* .line 219 */
/* .local v2, "e":Ljava/lang/Exception; */
/* monitor-exit v0 */
int v0 = 0; // const/4 v0, 0x0
/* .line 221 */
} // .end local v1 # "userState":Lcom/miui/server/security/SecurityUserState;
} // .end local v2 # "e":Ljava/lang/Exception;
/* :catchall_0 */
/* move-exception v1 */
/* monitor-exit v0 */
/* :try_end_2 */
/* .catchall {:try_start_2 ..:try_end_2} :catchall_0 */
/* throw v1 */
} // .end method
public Boolean getApplicationMaskNotificationEnabledLocked ( java.lang.String p0, Integer p1 ) {
/* .locals 4 */
/* .param p1, "packageName" # Ljava/lang/String; */
/* .param p2, "userId" # I */
/* .line 295 */
v0 = this.mService;
v0 = this.mUserStateLock;
/* monitor-enter v0 */
/* .line 296 */
try { // :try_start_0
v1 = this.mService;
(( com.miui.server.SecurityManagerService ) v1 ).getUserStateLocked ( p2 ); // invoke-virtual {v1, p2}, Lcom/miui/server/SecurityManagerService;->getUserStateLocked(I)Lcom/miui/server/security/SecurityUserState;
/* :try_end_0 */
/* .catchall {:try_start_0 ..:try_end_0} :catchall_0 */
/* .line 298 */
/* .local v1, "userState":Lcom/miui/server/security/SecurityUserState; */
try { // :try_start_1
v2 = this.mService;
v3 = this.mPackages;
(( com.miui.server.SecurityManagerService ) v2 ).getPackageSetting ( v3, p1 ); // invoke-virtual {v2, v3, p1}, Lcom/miui/server/SecurityManagerService;->getPackageSetting(Ljava/util/HashMap;Ljava/lang/String;)Lcom/miui/server/security/SecurityPackageSettings;
/* .line 299 */
/* .local v2, "ps":Lcom/miui/server/security/SecurityPackageSettings; */
/* iget-boolean v3, v2, Lcom/miui/server/security/SecurityPackageSettings;->maskNotification:Z */
/* :try_end_1 */
/* .catch Ljava/lang/Exception; {:try_start_1 ..:try_end_1} :catch_0 */
/* .catchall {:try_start_1 ..:try_end_1} :catchall_0 */
try { // :try_start_2
/* monitor-exit v0 */
/* .line 300 */
} // .end local v2 # "ps":Lcom/miui/server/security/SecurityPackageSettings;
/* :catch_0 */
/* move-exception v2 */
/* .line 301 */
/* .local v2, "e":Ljava/lang/Exception; */
/* monitor-exit v0 */
int v0 = 0; // const/4 v0, 0x0
/* .line 303 */
} // .end local v1 # "userState":Lcom/miui/server/security/SecurityUserState;
} // .end local v2 # "e":Ljava/lang/Exception;
/* :catchall_0 */
/* move-exception v1 */
/* monitor-exit v0 */
/* :try_end_2 */
/* .catchall {:try_start_2 ..:try_end_2} :catchall_0 */
/* throw v1 */
} // .end method
public java.lang.String getShouldMaskApps ( ) {
/* .locals 11 */
/* .line 246 */
v0 = this.mService;
v0 = this.mUserStateLock;
/* monitor-enter v0 */
/* .line 248 */
int v1 = 0; // const/4 v1, 0x0
try { // :try_start_0
/* new-instance v2, Lorg/json/JSONArray; */
/* invoke-direct {v2}, Lorg/json/JSONArray;-><init>()V */
/* .line 249 */
/* .local v2, "maskArray":Lorg/json/JSONArray; */
int v3 = 0; // const/4 v3, 0x0
/* .local v3, "i":I */
} // :goto_0
v4 = this.mService;
v4 = this.mUserStates;
v4 = (( android.util.SparseArray ) v4 ).size ( ); // invoke-virtual {v4}, Landroid/util/SparseArray;->size()I
/* if-ge v3, v4, :cond_4 */
/* .line 250 */
v4 = this.mService;
v4 = this.mUserStates;
(( android.util.SparseArray ) v4 ).valueAt ( v3 ); // invoke-virtual {v4, v3}, Landroid/util/SparseArray;->valueAt(I)Ljava/lang/Object;
/* check-cast v4, Lcom/miui/server/security/SecurityUserState; */
/* .line 251 */
/* .local v4, "userState":Lcom/miui/server/security/SecurityUserState; */
/* new-instance v5, Lorg/json/JSONObject; */
/* invoke-direct {v5}, Lorg/json/JSONObject;-><init>()V */
/* .line 252 */
/* .local v5, "userStateObj":Lorg/json/JSONObject; */
/* const-string/jumbo v6, "userId" */
/* iget v7, v4, Lcom/miui/server/security/SecurityUserState;->userHandle:I */
(( org.json.JSONObject ) v5 ).put ( v6, v7 ); // invoke-virtual {v5, v6, v7}, Lorg/json/JSONObject;->put(Ljava/lang/String;I)Lorg/json/JSONObject;
/* .line 253 */
/* new-instance v6, Lorg/json/JSONArray; */
/* invoke-direct {v6}, Lorg/json/JSONArray;-><init>()V */
/* .line 254 */
/* .local v6, "itemArray":Lorg/json/JSONArray; */
/* iget-boolean v7, v4, Lcom/miui/server/security/SecurityUserState;->mAccessControlEnabled:Z */
/* .line 256 */
/* .local v7, "enabled":Z */
/* iget v8, v4, Lcom/miui/server/security/SecurityUserState;->userHandle:I */
/* const/16 v9, 0x3e7 */
/* if-ne v8, v9, :cond_0 */
/* .line 257 */
v8 = this.mService;
int v9 = 0; // const/4 v9, 0x0
(( com.miui.server.SecurityManagerService ) v8 ).getUserStateLocked ( v9 ); // invoke-virtual {v8, v9}, Lcom/miui/server/SecurityManagerService;->getUserStateLocked(I)Lcom/miui/server/security/SecurityUserState;
/* iget-boolean v8, v8, Lcom/miui/server/security/SecurityUserState;->mAccessControlEnabled:Z */
/* move v7, v8 */
/* .line 259 */
} // :cond_0
if ( v7 != null) { // if-eqz v7, :cond_3
/* .line 260 */
v8 = this.mPackages;
(( java.util.HashMap ) v8 ).values ( ); // invoke-virtual {v8}, Ljava/util/HashMap;->values()Ljava/util/Collection;
} // :cond_1
v9 = } // :goto_1
if ( v9 != null) { // if-eqz v9, :cond_3
/* check-cast v9, Lcom/miui/server/security/SecurityPackageSettings; */
/* .line 261 */
/* .local v9, "ps":Lcom/miui/server/security/SecurityPackageSettings; */
v10 = this.name;
v10 = android.text.TextUtils .isEmpty ( v10 );
/* if-nez v10, :cond_1 */
/* iget-boolean v10, v9, Lcom/miui/server/security/SecurityPackageSettings;->accessControl:Z */
if ( v10 != null) { // if-eqz v10, :cond_1
/* iget-boolean v10, v9, Lcom/miui/server/security/SecurityPackageSettings;->maskNotification:Z */
if ( v10 != null) { // if-eqz v10, :cond_1
v10 = this.name;
/* .line 262 */
v10 = /* invoke-direct {p0, v4, v10, v1}, Lcom/miui/server/security/AccessControlImpl;->checkAccessControlPassLockedCore(Lcom/miui/server/security/SecurityUserState;Ljava/lang/String;Landroid/content/Intent;)Z */
if ( v10 != null) { // if-eqz v10, :cond_2
/* .line 263 */
/* .line 265 */
} // :cond_2
v10 = this.name;
(( org.json.JSONArray ) v6 ).put ( v10 ); // invoke-virtual {v6, v10}, Lorg/json/JSONArray;->put(Ljava/lang/Object;)Lorg/json/JSONArray;
/* .line 266 */
/* nop */
} // .end local v9 # "ps":Lcom/miui/server/security/SecurityPackageSettings;
/* .line 268 */
} // :cond_3
/* const-string/jumbo v8, "shouldMaskApps" */
(( org.json.JSONObject ) v5 ).put ( v8, v6 ); // invoke-virtual {v5, v8, v6}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;
/* .line 269 */
(( org.json.JSONArray ) v2 ).put ( v5 ); // invoke-virtual {v2, v5}, Lorg/json/JSONArray;->put(Ljava/lang/Object;)Lorg/json/JSONArray;
/* .line 249 */
/* nop */
} // .end local v4 # "userState":Lcom/miui/server/security/SecurityUserState;
} // .end local v5 # "userStateObj":Lorg/json/JSONObject;
} // .end local v6 # "itemArray":Lorg/json/JSONArray;
} // .end local v7 # "enabled":Z
/* add-int/lit8 v3, v3, 0x1 */
/* .line 271 */
} // .end local v3 # "i":I
} // :cond_4
(( org.json.JSONArray ) v2 ).toString ( ); // invoke-virtual {v2}, Lorg/json/JSONArray;->toString()Ljava/lang/String;
/* :try_end_0 */
/* .catch Ljava/lang/Exception; {:try_start_0 ..:try_end_0} :catch_0 */
/* .catchall {:try_start_0 ..:try_end_0} :catchall_0 */
try { // :try_start_1
/* monitor-exit v0 */
/* .line 276 */
} // .end local v2 # "maskArray":Lorg/json/JSONArray;
/* :catchall_0 */
/* move-exception v1 */
/* .line 272 */
/* :catch_0 */
/* move-exception v2 */
/* .line 273 */
/* .local v2, "e":Ljava/lang/Exception; */
final String v3 = "AccessControlImpl"; // const-string v3, "AccessControlImpl"
final String v4 = "getShouldMaskApps failed."; // const-string v4, "getShouldMaskApps failed."
android.util.Log .e ( v3,v4,v2 );
/* .line 275 */
/* nop */
} // .end local v2 # "e":Ljava/lang/Exception;
/* monitor-exit v0 */
/* .line 276 */
} // :goto_2
/* monitor-exit v0 */
/* :try_end_1 */
/* .catchall {:try_start_1 ..:try_end_1} :catchall_0 */
/* throw v1 */
} // .end method
public Boolean needFinishAccessControl ( android.os.IBinder p0 ) {
/* .locals 6 */
/* .param p1, "token" # Landroid/os/IBinder; */
/* .line 317 */
com.android.server.wm.WindowProcessUtils .getTaskIntentForToken ( p1 );
/* .line 318 */
/* .local v0, "taskIntent":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Landroid/content/Intent;>;" */
if ( v0 != null) { // if-eqz v0, :cond_0
v1 = (( java.util.ArrayList ) v0 ).size ( ); // invoke-virtual {v0}, Ljava/util/ArrayList;->size()I
int v2 = 1; // const/4 v2, 0x1
/* if-le v1, v2, :cond_0 */
/* .line 319 */
(( java.util.ArrayList ) v0 ).get ( v2 ); // invoke-virtual {v0, v2}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;
/* check-cast v1, Landroid/content/Intent; */
/* .line 320 */
/* .local v1, "intent":Landroid/content/Intent; */
(( android.content.Intent ) v1 ).getComponent ( ); // invoke-virtual {v1}, Landroid/content/Intent;->getComponent()Landroid/content/ComponentName;
/* .line 321 */
/* .local v3, "component":Landroid/content/ComponentName; */
if ( v3 != null) { // if-eqz v3, :cond_0
/* .line 322 */
v4 = this.mService;
v4 = this.mAccessController;
/* .line 323 */
(( android.content.ComponentName ) v3 ).getPackageName ( ); // invoke-virtual {v3}, Landroid/content/ComponentName;->getPackageName()Ljava/lang/String;
/* .line 322 */
v2 = (( com.miui.server.AccessController ) v4 ).filterIntentLocked ( v2, v5, v1 ); // invoke-virtual {v4, v2, v5, v1}, Lcom/miui/server/AccessController;->filterIntentLocked(ZLjava/lang/String;Landroid/content/Intent;)Z
/* .line 326 */
} // .end local v1 # "intent":Landroid/content/Intent;
} // .end local v3 # "component":Landroid/content/ComponentName;
} // :cond_0
int v1 = 0; // const/4 v1, 0x0
} // .end method
public void removeAccessControlPassAsUser ( java.lang.String p0, Integer p1 ) {
/* .locals 18 */
/* .param p1, "packageName" # Ljava/lang/String; */
/* .param p2, "userId" # I */
/* .line 55 */
/* move-object/from16 v1, p0 */
/* move-object/from16 v2, p1 */
/* move/from16 v3, p2 */
int v4 = 0; // const/4 v4, 0x0
/* .line 56 */
/* .local v4, "pkgName":Ljava/lang/String; */
int v5 = 0; // const/4 v5, 0x0
/* .line 57 */
/* .local v5, "token":Landroid/os/IBinder; */
int v0 = 0; // const/4 v0, 0x0
java.lang.Integer .valueOf ( v0 );
/* .line 58 */
/* .local v6, "activityUserId":Ljava/lang/Integer; */
int v7 = 0; // const/4 v7, 0x0
/* .line 60 */
/* .local v7, "checkAccessControlPass":Z */
int v0 = 0; // const/4 v0, 0x0
/* .line 61 */
/* .local v0, "topActivity":Ljava/util/HashMap;, "Ljava/util/HashMap<Ljava/lang/String;Ljava/lang/Object;>;" */
int v8 = -1; // const/4 v8, -0x1
/* if-ne v3, v8, :cond_0 */
/* .line 62 */
com.android.server.wm.WindowProcessUtils .getTopRunningActivityInfo ( );
/* move-object v9, v0 */
/* .line 61 */
} // :cond_0
/* move-object v9, v0 */
/* .line 65 */
} // .end local v0 # "topActivity":Ljava/util/HashMap;, "Ljava/util/HashMap<Ljava/lang/String;Ljava/lang/Object;>;"
/* .local v9, "topActivity":Ljava/util/HashMap;, "Ljava/util/HashMap<Ljava/lang/String;Ljava/lang/Object;>;" */
} // :goto_0
v0 = this.mService;
v10 = this.mUserStateLock;
/* monitor-enter v10 */
/* .line 66 */
/* if-ne v3, v8, :cond_4 */
/* .line 67 */
try { // :try_start_0
v0 = this.mService;
v0 = this.mUserStates;
v0 = (( android.util.SparseArray ) v0 ).size ( ); // invoke-virtual {v0}, Landroid/util/SparseArray;->size()I
/* .line 68 */
/* .local v0, "size":I */
int v11 = 0; // const/4 v11, 0x0
/* .local v11, "i":I */
} // :goto_1
/* if-ge v11, v0, :cond_1 */
/* .line 69 */
v12 = this.mService;
v12 = this.mUserStates;
(( android.util.SparseArray ) v12 ).valueAt ( v11 ); // invoke-virtual {v12, v11}, Landroid/util/SparseArray;->valueAt(I)Ljava/lang/Object;
/* check-cast v12, Lcom/miui/server/security/SecurityUserState; */
/* .line 70 */
/* .local v12, "userState":Lcom/miui/server/security/SecurityUserState; */
/* invoke-direct {v1, v12, v2}, Lcom/miui/server/security/AccessControlImpl;->removeAccessControlPassLocked(Lcom/miui/server/security/SecurityUserState;Ljava/lang/String;)V */
/* .line 68 */
} // .end local v12 # "userState":Lcom/miui/server/security/SecurityUserState;
/* add-int/lit8 v11, v11, 0x1 */
/* .line 72 */
} // .end local v11 # "i":I
} // :cond_1
v11 = com.android.server.am.ProcessUtils .getCurrentUserId ( );
/* .line 73 */
/* .local v11, "currentUserId":I */
v12 = this.mService;
(( com.miui.server.SecurityManagerService ) v12 ).getUserStateLocked ( v11 ); // invoke-virtual {v12, v11}, Lcom/miui/server/SecurityManagerService;->getUserStateLocked(I)Lcom/miui/server/security/SecurityUserState;
/* .line 74 */
/* .restart local v12 # "userState":Lcom/miui/server/security/SecurityUserState; */
v13 = (( com.miui.server.security.AccessControlImpl ) v1 ).getAccessControlEnabledLocked ( v12 ); // invoke-virtual {v1, v12}, Lcom/miui/server/security/AccessControlImpl;->getAccessControlEnabledLocked(Lcom/miui/server/security/SecurityUserState;)Z
/* .line 75 */
/* .local v13, "enabled":Z */
/* if-nez v13, :cond_2 */
/* .line 76 */
/* monitor-exit v10 */
return;
/* .line 78 */
} // :cond_2
if ( v9 != null) { // if-eqz v9, :cond_3
/* .line 79 */
final String v14 = "packageName"; // const-string v14, "packageName"
(( java.util.HashMap ) v9 ).get ( v14 ); // invoke-virtual {v9, v14}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;
/* check-cast v14, Ljava/lang/String; */
/* move-object v4, v14 */
/* .line 80 */
/* const-string/jumbo v14, "token" */
(( java.util.HashMap ) v9 ).get ( v14 ); // invoke-virtual {v9, v14}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;
/* check-cast v14, Landroid/os/IBinder; */
/* move-object v5, v14 */
/* .line 81 */
/* const-string/jumbo v14, "userId" */
(( java.util.HashMap ) v9 ).get ( v14 ); // invoke-virtual {v9, v14}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;
/* check-cast v14, Ljava/lang/Integer; */
/* move-object v6, v14 */
/* .line 82 */
v14 = (( java.lang.Integer ) v6 ).intValue ( ); // invoke-virtual {v6}, Ljava/lang/Integer;->intValue()I
int v15 = 0; // const/4 v15, 0x0
v14 = (( com.miui.server.security.AccessControlImpl ) v1 ).checkAccessControlPassLocked ( v4, v15, v14 ); // invoke-virtual {v1, v4, v15, v14}, Lcom/miui/server/security/AccessControlImpl;->checkAccessControlPassLocked(Ljava/lang/String;Landroid/content/Intent;I)Z
/* move v7, v14 */
/* .line 84 */
} // .end local v0 # "size":I
} // .end local v11 # "currentUserId":I
} // .end local v12 # "userState":Lcom/miui/server/security/SecurityUserState;
} // .end local v13 # "enabled":Z
} // :cond_3
/* .line 85 */
} // :cond_4
v0 = this.mService;
(( com.miui.server.SecurityManagerService ) v0 ).getUserStateLocked ( v3 ); // invoke-virtual {v0, v3}, Lcom/miui/server/SecurityManagerService;->getUserStateLocked(I)Lcom/miui/server/security/SecurityUserState;
/* .line 86 */
/* .local v0, "userState":Lcom/miui/server/security/SecurityUserState; */
/* invoke-direct {v1, v0, v2}, Lcom/miui/server/security/AccessControlImpl;->removeAccessControlPassLocked(Lcom/miui/server/security/SecurityUserState;Ljava/lang/String;)V */
/* .line 88 */
} // .end local v0 # "userState":Lcom/miui/server/security/SecurityUserState;
} // :goto_2
/* monitor-exit v10 */
/* :try_end_0 */
/* .catchall {:try_start_0 ..:try_end_0} :catchall_0 */
/* .line 90 */
/* if-ne v3, v8, :cond_5 */
if ( v9 != null) { // if-eqz v9, :cond_5
/* .line 91 */
/* if-nez v7, :cond_5 */
/* .line 93 */
int v11 = 1; // const/4 v11, 0x1
int v13 = 0; // const/4 v13, 0x0
int v14 = -1; // const/4 v14, -0x1
int v15 = 1; // const/4 v15, 0x1
/* .line 94 */
try { // :try_start_1
v16 = (( java.lang.Integer ) v6 ).intValue ( ); // invoke-virtual {v6}, Ljava/lang/Integer;->intValue()I
/* const/16 v17, 0x0 */
/* .line 93 */
/* move-object v12, v4 */
/* invoke-static/range {v11 ..v17}, Lmiui/security/SecurityManager;->getCheckAccessIntent(ZLjava/lang/String;Landroid/content/Intent;IZILandroid/os/Bundle;)Landroid/content/Intent; */
/* .line 95 */
/* .local v0, "intent":Landroid/content/Intent; */
final String v8 = "miui.KEYGUARD_LOCKED"; // const-string v8, "miui.KEYGUARD_LOCKED"
int v10 = 1; // const/4 v10, 0x1
(( android.content.Intent ) v0 ).putExtra ( v8, v10 ); // invoke-virtual {v0, v8, v10}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;
/* .line 96 */
v11 = this.mContext;
int v12 = 0; // const/4 v12, 0x0
int v14 = 0; // const/4 v14, 0x0
/* .line 97 */
v16 = (( java.lang.Integer ) v6 ).intValue ( ); // invoke-virtual {v6}, Ljava/lang/Integer;->intValue()I
/* .line 96 */
/* move-object v13, v5 */
/* move-object v15, v0 */
/* invoke-static/range {v11 ..v16}, Lmiui/security/SecurityManagerCompat;->startActvityAsUser(Landroid/content/Context;Landroid/app/IApplicationThread;Landroid/os/IBinder;Ljava/lang/String;Landroid/content/Intent;I)V */
/* :try_end_1 */
/* .catch Ljava/lang/Exception; {:try_start_1 ..:try_end_1} :catch_0 */
/* .line 100 */
} // .end local v0 # "intent":Landroid/content/Intent;
/* .line 98 */
/* :catch_0 */
/* move-exception v0 */
/* .line 99 */
/* .local v0, "e":Ljava/lang/Exception; */
final String v8 = "AccessControlImpl"; // const-string v8, "AccessControlImpl"
final String v10 = "removeAccessControlPassAsUser startActivityAsUser error "; // const-string v10, "removeAccessControlPassAsUser startActivityAsUser error "
android.util.Log .e ( v8,v10,v0 );
/* .line 103 */
} // .end local v0 # "e":Ljava/lang/Exception;
} // :cond_5
} // :goto_3
return;
/* .line 88 */
/* :catchall_0 */
/* move-exception v0 */
try { // :try_start_2
/* monitor-exit v10 */
/* :try_end_2 */
/* .catchall {:try_start_2 ..:try_end_2} :catchall_0 */
/* throw v0 */
} // .end method
public void setApplicationAccessControlEnabledForUser ( java.lang.String p0, Boolean p1, Integer p2 ) {
/* .locals 4 */
/* .param p1, "packageName" # Ljava/lang/String; */
/* .param p2, "enabled" # Z */
/* .param p3, "userId" # I */
/* .line 225 */
v0 = this.mService;
v0 = this.mUserStateLock;
/* monitor-enter v0 */
/* .line 226 */
try { // :try_start_0
v1 = this.mService;
(( com.miui.server.SecurityManagerService ) v1 ).getUserStateLocked ( p3 ); // invoke-virtual {v1, p3}, Lcom/miui/server/SecurityManagerService;->getUserStateLocked(I)Lcom/miui/server/security/SecurityUserState;
/* .line 227 */
/* .local v1, "userStateLocked":Lcom/miui/server/security/SecurityUserState; */
v2 = this.mService;
v3 = this.mPackages;
(( com.miui.server.SecurityManagerService ) v2 ).getPackageSetting ( v3, p1 ); // invoke-virtual {v2, v3, p1}, Lcom/miui/server/SecurityManagerService;->getPackageSetting(Ljava/util/HashMap;Ljava/lang/String;)Lcom/miui/server/security/SecurityPackageSettings;
/* .line 228 */
/* .local v2, "ps":Lcom/miui/server/security/SecurityPackageSettings; */
/* iput-boolean p2, v2, Lcom/miui/server/security/SecurityPackageSettings;->accessControl:Z */
/* .line 229 */
v3 = this.mService;
(( com.miui.server.SecurityManagerService ) v3 ).scheduleWriteSettings ( ); // invoke-virtual {v3}, Lcom/miui/server/SecurityManagerService;->scheduleWriteSettings()V
/* .line 230 */
(( com.miui.server.security.AccessControlImpl ) p0 ).updateMaskObserverValues ( ); // invoke-virtual {p0}, Lcom/miui/server/security/AccessControlImpl;->updateMaskObserverValues()V
/* .line 231 */
} // .end local v1 # "userStateLocked":Lcom/miui/server/security/SecurityUserState;
} // .end local v2 # "ps":Lcom/miui/server/security/SecurityPackageSettings;
/* monitor-exit v0 */
/* .line 232 */
return;
/* .line 231 */
/* :catchall_0 */
/* move-exception v1 */
/* monitor-exit v0 */
/* :try_end_0 */
/* .catchall {:try_start_0 ..:try_end_0} :catchall_0 */
/* throw v1 */
} // .end method
public void setApplicationMaskNotificationEnabledForUser ( java.lang.String p0, Boolean p1, Integer p2 ) {
/* .locals 4 */
/* .param p1, "packageName" # Ljava/lang/String; */
/* .param p2, "enabled" # Z */
/* .param p3, "userId" # I */
/* .line 307 */
v0 = this.mService;
v0 = this.mUserStateLock;
/* monitor-enter v0 */
/* .line 308 */
try { // :try_start_0
v1 = this.mService;
(( com.miui.server.SecurityManagerService ) v1 ).getUserStateLocked ( p3 ); // invoke-virtual {v1, p3}, Lcom/miui/server/SecurityManagerService;->getUserStateLocked(I)Lcom/miui/server/security/SecurityUserState;
/* .line 309 */
/* .local v1, "userStateLocked":Lcom/miui/server/security/SecurityUserState; */
v2 = this.mService;
v3 = this.mPackages;
(( com.miui.server.SecurityManagerService ) v2 ).getPackageSetting ( v3, p1 ); // invoke-virtual {v2, v3, p1}, Lcom/miui/server/SecurityManagerService;->getPackageSetting(Ljava/util/HashMap;Ljava/lang/String;)Lcom/miui/server/security/SecurityPackageSettings;
/* .line 310 */
/* .local v2, "ps":Lcom/miui/server/security/SecurityPackageSettings; */
/* iput-boolean p2, v2, Lcom/miui/server/security/SecurityPackageSettings;->maskNotification:Z */
/* .line 311 */
v3 = this.mService;
(( com.miui.server.SecurityManagerService ) v3 ).scheduleWriteSettings ( ); // invoke-virtual {v3}, Lcom/miui/server/SecurityManagerService;->scheduleWriteSettings()V
/* .line 312 */
(( com.miui.server.security.AccessControlImpl ) p0 ).updateMaskObserverValues ( ); // invoke-virtual {p0}, Lcom/miui/server/security/AccessControlImpl;->updateMaskObserverValues()V
/* .line 313 */
} // .end local v1 # "userStateLocked":Lcom/miui/server/security/SecurityUserState;
} // .end local v2 # "ps":Lcom/miui/server/security/SecurityPackageSettings;
/* monitor-exit v0 */
/* .line 314 */
return;
/* .line 313 */
/* :catchall_0 */
/* move-exception v1 */
/* monitor-exit v0 */
/* :try_end_0 */
/* .catchall {:try_start_0 ..:try_end_0} :catchall_0 */
/* throw v1 */
} // .end method
public void updateMaskObserverValues ( ) {
/* .locals 6 */
/* .line 283 */
final String v0 = "applock_mask_notify"; // const-string v0, "applock_mask_notify"
android.os.Binder .clearCallingIdentity ( );
/* move-result-wide v1 */
/* .line 285 */
/* .local v1, "origId":J */
try { // :try_start_0
v3 = this.mContext;
(( android.content.Context ) v3 ).getContentResolver ( ); // invoke-virtual {v3}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;
int v4 = 0; // const/4 v4, 0x0
v3 = android.provider.Settings$Secure .getInt ( v3,v0,v4 );
/* .line 287 */
/* .local v3, "oldValue":I */
v4 = this.mContext;
(( android.content.Context ) v4 ).getContentResolver ( ); // invoke-virtual {v4}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;
/* xor-int/lit8 v5, v3, 0x1 */
android.provider.Settings$Secure .putInt ( v4,v0,v5 );
/* :try_end_0 */
/* .catch Ljava/lang/Exception; {:try_start_0 ..:try_end_0} :catch_0 */
/* .line 290 */
/* nop */
} // .end local v3 # "oldValue":I
/* .line 288 */
/* :catch_0 */
/* move-exception v0 */
/* .line 289 */
/* .local v0, "e":Ljava/lang/Exception; */
final String v3 = "AccessControlImpl"; // const-string v3, "AccessControlImpl"
/* const-string/jumbo v4, "write setting secure failed." */
android.util.Log .e ( v3,v4,v0 );
/* .line 291 */
} // .end local v0 # "e":Ljava/lang/Exception;
} // :goto_0
android.os.Binder .restoreCallingIdentity ( v1,v2 );
/* .line 292 */
return;
} // .end method
