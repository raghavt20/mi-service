public class com.miui.server.security.SafetyDetectManagerImpl extends com.miui.server.security.SafetyDetectManagerStub {
	 /* .source "SafetyDetectManagerImpl.java" */
	 /* # annotations */
	 /* .annotation runtime Lcom/miui/base/annotations/MiuiStubHead; */
	 /* manifestName = "com.miui.server.security.SafetyDetectManagerStub$$" */
} // .end annotation
/* # static fields */
private static final java.lang.String TAG;
private static final java.lang.String TRUE;
/* # instance fields */
private Boolean mDetectAccessibilityTouch;
private Boolean mDetectAdbInput;
private Long mLastSimulatedTouchTime;
private Integer mLastSimulatedTouchUid;
/* # direct methods */
public com.miui.server.security.SafetyDetectManagerImpl ( ) {
	 /* .locals 1 */
	 /* .line 18 */
	 /* invoke-direct {p0}, Lcom/miui/server/security/SafetyDetectManagerStub;-><init>()V */
	 /* .line 22 */
	 int v0 = 1; // const/4 v0, 0x1
	 /* iput-boolean v0, p0, Lcom/miui/server/security/SafetyDetectManagerImpl;->mDetectAccessibilityTouch:Z */
	 /* .line 23 */
	 /* iput-boolean v0, p0, Lcom/miui/server/security/SafetyDetectManagerImpl;->mDetectAdbInput:Z */
	 return;
} // .end method
/* # virtual methods */
public java.util.Map getSimulatedTouchInfo ( ) {
	 /* .locals 3 */
	 /* .annotation system Ldalvik/annotation/Signature; */
	 /* value = { */
	 /* "()", */
	 /* "Ljava/util/Map<", */
	 /* "Ljava/lang/String;", */
	 /* "Ljava/lang/String;", */
	 /* ">;" */
	 /* } */
} // .end annotation
/* .line 58 */
/* new-instance v0, Ljava/util/HashMap; */
/* invoke-direct {v0}, Ljava/util/HashMap;-><init>()V */
/* .line 59 */
/* .local v0, "simulatedTouchInfo":Ljava/util/Map;, "Ljava/util/Map<Ljava/lang/String;Ljava/lang/String;>;" */
/* iget-boolean v1, p0, Lcom/miui/server/security/SafetyDetectManagerImpl;->mDetectAccessibilityTouch:Z */
/* if-nez v1, :cond_0 */
/* iget-boolean v1, p0, Lcom/miui/server/security/SafetyDetectManagerImpl;->mDetectAdbInput:Z */
if ( v1 != null) { // if-eqz v1, :cond_1
	 /* .line 60 */
} // :cond_0
/* iget-wide v1, p0, Lcom/miui/server/security/SafetyDetectManagerImpl;->mLastSimulatedTouchTime:J */
java.lang.String .valueOf ( v1,v2 );
final String v2 = "lastSimulatedTouchTime"; // const-string v2, "lastSimulatedTouchTime"
/* .line 61 */
/* iget v1, p0, Lcom/miui/server/security/SafetyDetectManagerImpl;->mLastSimulatedTouchUid:I */
java.lang.String .valueOf ( v1 );
final String v2 = "lastSimulatedTouchUid"; // const-string v2, "lastSimulatedTouchUid"
/* .line 63 */
} // :cond_1
} // .end method
public void stampAccessibilityDispatchGesture ( Integer p0 ) {
/* .locals 2 */
/* .param p1, "uid" # I */
/* .line 40 */
/* iget-boolean v0, p0, Lcom/miui/server/security/SafetyDetectManagerImpl;->mDetectAccessibilityTouch:Z */
/* if-nez v0, :cond_0 */
/* .line 41 */
return;
/* .line 43 */
} // :cond_0
android.os.SystemClock .uptimeMillis ( );
/* move-result-wide v0 */
/* iput-wide v0, p0, Lcom/miui/server/security/SafetyDetectManagerImpl;->mLastSimulatedTouchTime:J */
/* .line 44 */
/* iput p1, p0, Lcom/miui/server/security/SafetyDetectManagerImpl;->mLastSimulatedTouchUid:I */
/* .line 45 */
return;
} // .end method
public void stampPerformAccessibilityAction ( Integer p0 ) {
/* .locals 2 */
/* .param p1, "uid" # I */
/* .line 49 */
/* iget-boolean v0, p0, Lcom/miui/server/security/SafetyDetectManagerImpl;->mDetectAccessibilityTouch:Z */
/* if-nez v0, :cond_0 */
/* .line 50 */
return;
/* .line 52 */
} // :cond_0
android.os.SystemClock .uptimeMillis ( );
/* move-result-wide v0 */
/* iput-wide v0, p0, Lcom/miui/server/security/SafetyDetectManagerImpl;->mLastSimulatedTouchTime:J */
/* .line 53 */
/* iput p1, p0, Lcom/miui/server/security/SafetyDetectManagerImpl;->mLastSimulatedTouchUid:I */
/* .line 54 */
return;
} // .end method
public void stampShellInjectInputEvent ( Long p0 ) {
/* .locals 1 */
/* .param p1, "downTime" # J */
/* .line 30 */
/* iget-boolean v0, p0, Lcom/miui/server/security/SafetyDetectManagerImpl;->mDetectAdbInput:Z */
/* if-nez v0, :cond_0 */
/* .line 31 */
return;
/* .line 33 */
} // :cond_0
v0 = android.os.Binder .getCallingUid ( );
/* .line 34 */
/* .local v0, "callingUid":I */
/* iput-wide p1, p0, Lcom/miui/server/security/SafetyDetectManagerImpl;->mLastSimulatedTouchTime:J */
/* .line 35 */
/* iput v0, p0, Lcom/miui/server/security/SafetyDetectManagerImpl;->mLastSimulatedTouchUid:I */
/* .line 36 */
return;
} // .end method
public void switchSimulatedTouchDetect ( java.util.Map p0 ) {
/* .locals 4 */
/* .annotation system Ldalvik/annotation/Signature; */
/* value = { */
/* "(", */
/* "Ljava/util/Map<", */
/* "Ljava/lang/String;", */
/* "Ljava/lang/String;", */
/* ">;)V" */
/* } */
} // .end annotation
/* .line 69 */
/* .local p1, "config":Ljava/util/Map;, "Ljava/util/Map<Ljava/lang/String;Ljava/lang/String;>;" */
final String v0 = "SafetyDetectManagerImpl"; // const-string v0, "SafetyDetectManagerImpl"
/* const-string/jumbo v1, "true" */
try { // :try_start_0
final String v2 = "detectAdpInput"; // const-string v2, "detectAdpInput"
/* check-cast v2, Ljava/lang/String; */
v2 = java.lang.Boolean .parseBoolean ( v2 );
/* iput-boolean v2, p0, Lcom/miui/server/security/SafetyDetectManagerImpl;->mDetectAdbInput:Z */
/* .line 70 */
final String v2 = "detectAccessibilityTouch"; // const-string v2, "detectAccessibilityTouch"
/* check-cast v1, Ljava/lang/String; */
v1 = java.lang.Boolean .parseBoolean ( v1 );
/* iput-boolean v1, p0, Lcom/miui/server/security/SafetyDetectManagerImpl;->mDetectAccessibilityTouch:Z */
/* :try_end_0 */
/* .catch Ljava/lang/Exception; {:try_start_0 ..:try_end_0} :catch_0 */
/* .line 73 */
/* .line 71 */
/* :catch_0 */
/* move-exception v1 */
/* .line 72 */
/* .local v1, "e":Ljava/lang/Exception; */
/* new-instance v2, Ljava/lang/StringBuilder; */
/* invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V */
/* const-string/jumbo v3, "switchSimulatedTouchDetect exception: " */
(( java.lang.StringBuilder ) v2 ).append ( v3 ); // invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v2 ).append ( v1 ); // invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v2 ).toString ( ); // invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
android.util.Slog .e ( v0,v2 );
/* .line 74 */
} // .end local v1 # "e":Ljava/lang/Exception;
} // :goto_0
/* new-instance v1, Ljava/lang/StringBuilder; */
/* invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V */
final String v2 = "mDetectAdbInput = "; // const-string v2, "mDetectAdbInput = "
(( java.lang.StringBuilder ) v1 ).append ( v2 ); // invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
/* iget-boolean v2, p0, Lcom/miui/server/security/SafetyDetectManagerImpl;->mDetectAdbInput:Z */
(( java.lang.StringBuilder ) v1 ).append ( v2 ); // invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;
final String v2 = "\tmDetectAccessibilityTouch = "; // const-string v2, "\tmDetectAccessibilityTouch = "
(( java.lang.StringBuilder ) v1 ).append ( v2 ); // invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
/* iget-boolean v2, p0, Lcom/miui/server/security/SafetyDetectManagerImpl;->mDetectAccessibilityTouch:Z */
(( java.lang.StringBuilder ) v1 ).append ( v2 ); // invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v1 ).toString ( ); // invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
android.util.Slog .i ( v0,v1 );
/* .line 75 */
return;
} // .end method
