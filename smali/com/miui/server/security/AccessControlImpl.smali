.class public Lcom/miui/server/security/AccessControlImpl;
.super Ljava/lang/Object;
.source "AccessControlImpl.java"


# static fields
.field private static final APPLOCK_MASK_NOTIFY:Ljava/lang/String; = "applock_mask_notify"

.field public static final LOCK_TIME_OUT:J = 0xea60L

.field private static final TAG:Ljava/lang/String; = "AccessControlImpl"


# instance fields
.field private final mContext:Landroid/content/Context;

.field private final mHandler:Landroid/os/Handler;

.field private final mService:Lcom/miui/server/SecurityManagerService;


# direct methods
.method public constructor <init>(Lcom/miui/server/SecurityManagerService;)V
    .locals 1
    .param p1, "service"    # Lcom/miui/server/SecurityManagerService;

    .line 48
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 49
    iput-object p1, p0, Lcom/miui/server/security/AccessControlImpl;->mService:Lcom/miui/server/SecurityManagerService;

    .line 50
    iget-object v0, p1, Lcom/miui/server/SecurityManagerService;->mContext:Landroid/content/Context;

    iput-object v0, p0, Lcom/miui/server/security/AccessControlImpl;->mContext:Landroid/content/Context;

    .line 51
    iget-object v0, p1, Lcom/miui/server/SecurityManagerService;->mSecurityWriteHandler:Lcom/miui/server/security/SecurityWriteHandler;

    iput-object v0, p0, Lcom/miui/server/security/AccessControlImpl;->mHandler:Landroid/os/Handler;

    .line 52
    return-void
.end method

.method private changeUserState(Lcom/miui/server/security/SecurityUserState;)Lcom/miui/server/security/SecurityUserState;
    .locals 2
    .param p1, "userState"    # Lcom/miui/server/security/SecurityUserState;

    .line 419
    iget v0, p1, Lcom/miui/server/security/SecurityUserState;->userHandle:I

    invoke-static {v0}, Lmiui/security/SecurityManager;->getUserHandle(I)I

    move-result v0

    .line 420
    .local v0, "userId":I
    iget-object v1, p0, Lcom/miui/server/security/AccessControlImpl;->mService:Lcom/miui/server/SecurityManagerService;

    invoke-virtual {v1, v0}, Lcom/miui/server/SecurityManagerService;->getUserStateLocked(I)Lcom/miui/server/security/SecurityUserState;

    move-result-object v1

    return-object v1
.end method

.method private checkAccessControlPassLockedCore(Lcom/miui/server/security/SecurityUserState;Ljava/lang/String;Landroid/content/Intent;)Z
    .locals 9
    .param p1, "userState"    # Lcom/miui/server/security/SecurityUserState;
    .param p2, "packageName"    # Ljava/lang/String;
    .param p3, "intent"    # Landroid/content/Intent;

    .line 351
    invoke-direct {p0, p1}, Lcom/miui/server/security/AccessControlImpl;->getAccessControlLockMode(Lcom/miui/server/security/SecurityUserState;)I

    move-result v0

    .line 353
    .local v0, "lockMode":I
    iget-object v1, p1, Lcom/miui/server/security/SecurityUserState;->mAccessControlPassPackages:Ljava/util/HashSet;

    invoke-virtual {v1, p2}, Ljava/util/HashSet;->contains(Ljava/lang/Object;)Z

    move-result v1

    .line 354
    .local v1, "pass":Z
    if-eqz v1, :cond_1

    const/4 v2, 0x2

    if-ne v0, v2, :cond_1

    .line 355
    iget-object v2, p1, Lcom/miui/server/security/SecurityUserState;->mAccessControlLastCheck:Landroid/util/ArrayMap;

    invoke-virtual {v2, p2}, Landroid/util/ArrayMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/lang/Long;

    .line 356
    .local v2, "lastTime":Ljava/lang/Long;
    if-eqz v2, :cond_0

    .line 357
    invoke-static {}, Landroid/os/SystemClock;->elapsedRealtime()J

    move-result-wide v3

    .line 358
    .local v3, "realtime":J
    invoke-virtual {v2}, Ljava/lang/Long;->longValue()J

    move-result-wide v5

    sub-long v5, v3, v5

    const-wide/32 v7, 0xea60

    cmp-long v5, v5, v7

    if-lez v5, :cond_0

    .line 359
    const/4 v1, 0x0

    .line 362
    .end local v3    # "realtime":J
    :cond_0
    if-eqz v1, :cond_1

    .line 363
    invoke-static {}, Landroid/os/Binder;->getCallingPid()I

    move-result v3

    invoke-static {v3}, Lcom/android/server/am/ProcessUtils;->getPackageNameByPid(I)Ljava/lang/String;

    move-result-object v3

    const-string v4, "com.android.systemui"

    invoke-virtual {v4, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-nez v3, :cond_1

    .line 364
    iget-object v3, p1, Lcom/miui/server/security/SecurityUserState;->mAccessControlLastCheck:Landroid/util/ArrayMap;

    invoke-static {}, Landroid/os/SystemClock;->elapsedRealtime()J

    move-result-wide v4

    invoke-static {v4, v5}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v4

    invoke-virtual {v3, p2, v4}, Landroid/util/ArrayMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 365
    iget v3, p1, Lcom/miui/server/security/SecurityUserState;->userHandle:I

    invoke-direct {p0, p2, v3}, Lcom/miui/server/security/AccessControlImpl;->scheduleForMaskObserver(Ljava/lang/String;I)V

    .line 369
    .end local v2    # "lastTime":Ljava/lang/Long;
    :cond_1
    const/4 v2, 0x1

    if-nez v1, :cond_2

    if-ne v0, v2, :cond_2

    .line 370
    invoke-direct {p0, p1}, Lcom/miui/server/security/AccessControlImpl;->getAccessControlLockConvenient(Lcom/miui/server/security/SecurityUserState;)Z

    move-result v3

    if-eqz v3, :cond_2

    .line 371
    invoke-direct {p0, p1}, Lcom/miui/server/security/AccessControlImpl;->isPackageAccessControlPass(Lcom/miui/server/security/SecurityUserState;)Z

    move-result v3

    if-eqz v3, :cond_2

    .line 372
    const/4 v1, 0x1

    .line 375
    :cond_2
    if-nez v1, :cond_3

    iget-object v3, p0, Lcom/miui/server/security/AccessControlImpl;->mService:Lcom/miui/server/SecurityManagerService;

    iget-object v3, v3, Lcom/miui/server/SecurityManagerService;->mAccessController:Lcom/miui/server/AccessController;

    .line 376
    invoke-static {}, Landroid/os/Binder;->getCallingPid()I

    move-result v4

    invoke-static {v4}, Lcom/android/server/am/ProcessUtils;->getPackageNameByPid(I)Ljava/lang/String;

    move-result-object v4

    .line 375
    invoke-virtual {v3, p3, v4}, Lcom/miui/server/AccessController;->skipActivity(Landroid/content/Intent;Ljava/lang/String;)Z

    move-result v3

    if-eqz v3, :cond_3

    .line 377
    const/4 v1, 0x1

    .line 379
    :cond_3
    if-nez v1, :cond_4

    iget-object v3, p0, Lcom/miui/server/security/AccessControlImpl;->mService:Lcom/miui/server/SecurityManagerService;

    iget-object v3, v3, Lcom/miui/server/SecurityManagerService;->mAccessController:Lcom/miui/server/AccessController;

    invoke-virtual {v3, v2, p2, p3}, Lcom/miui/server/AccessController;->filterIntentLocked(ZLjava/lang/String;Landroid/content/Intent;)Z

    move-result v2

    if-eqz v2, :cond_4

    .line 380
    const/4 v1, 0x1

    .line 382
    :cond_4
    return v1
.end method

.method private clearPassPackages(I)V
    .locals 4
    .param p1, "userId"    # I

    .line 435
    invoke-static {}, Lmiui/securityspace/ConfigUtils;->isSupportXSpace()Z

    move-result v0

    if-eqz v0, :cond_1

    const/16 v0, 0x3e7

    if-eqz p1, :cond_0

    if-ne v0, p1, :cond_1

    .line 437
    :cond_0
    iget-object v1, p0, Lcom/miui/server/security/AccessControlImpl;->mService:Lcom/miui/server/SecurityManagerService;

    const/4 v2, 0x0

    invoke-virtual {v1, v2}, Lcom/miui/server/SecurityManagerService;->getUserStateLocked(I)Lcom/miui/server/security/SecurityUserState;

    move-result-object v1

    .line 438
    .local v1, "userStateOwner":Lcom/miui/server/security/SecurityUserState;
    iget-object v2, p0, Lcom/miui/server/security/AccessControlImpl;->mService:Lcom/miui/server/SecurityManagerService;

    invoke-virtual {v2, v0}, Lcom/miui/server/SecurityManagerService;->getUserStateLocked(I)Lcom/miui/server/security/SecurityUserState;

    move-result-object v0

    .line 439
    .local v0, "userStateXSpace":Lcom/miui/server/security/SecurityUserState;
    iget-object v2, v1, Lcom/miui/server/security/SecurityUserState;->mAccessControlPassPackages:Ljava/util/HashSet;

    .line 440
    .local v2, "passPackagesOwner":Ljava/util/HashSet;, "Ljava/util/HashSet<Ljava/lang/String;>;"
    iget-object v3, v0, Lcom/miui/server/security/SecurityUserState;->mAccessControlPassPackages:Ljava/util/HashSet;

    .line 441
    .local v3, "passPackagesXSpace":Ljava/util/HashSet;, "Ljava/util/HashSet<Ljava/lang/String;>;"
    invoke-virtual {v2}, Ljava/util/HashSet;->clear()V

    .line 442
    invoke-virtual {v3}, Ljava/util/HashSet;->clear()V

    .line 443
    .end local v0    # "userStateXSpace":Lcom/miui/server/security/SecurityUserState;
    .end local v1    # "userStateOwner":Lcom/miui/server/security/SecurityUserState;
    .end local v2    # "passPackagesOwner":Ljava/util/HashSet;, "Ljava/util/HashSet<Ljava/lang/String;>;"
    .end local v3    # "passPackagesXSpace":Ljava/util/HashSet;, "Ljava/util/HashSet<Ljava/lang/String;>;"
    goto :goto_0

    .line 444
    :cond_1
    iget-object v0, p0, Lcom/miui/server/security/AccessControlImpl;->mService:Lcom/miui/server/SecurityManagerService;

    invoke-virtual {v0, p1}, Lcom/miui/server/SecurityManagerService;->getUserStateLocked(I)Lcom/miui/server/security/SecurityUserState;

    move-result-object v0

    iget-object v0, v0, Lcom/miui/server/security/SecurityUserState;->mAccessControlPassPackages:Ljava/util/HashSet;

    .line 445
    .local v0, "passPackages":Ljava/util/HashSet;, "Ljava/util/HashSet<Ljava/lang/String;>;"
    invoke-virtual {v0}, Ljava/util/HashSet;->clear()V

    .line 447
    .end local v0    # "passPackages":Ljava/util/HashSet;, "Ljava/util/HashSet<Ljava/lang/String;>;"
    :goto_0
    invoke-virtual {p0}, Lcom/miui/server/security/AccessControlImpl;->updateMaskObserverValues()V

    .line 448
    return-void
.end method

.method private getAccessControlLockConvenient(Lcom/miui/server/security/SecurityUserState;)Z
    .locals 2
    .param p1, "userState"    # Lcom/miui/server/security/SecurityUserState;

    .line 413
    invoke-direct {p0, p1}, Lcom/miui/server/security/AccessControlImpl;->changeUserState(Lcom/miui/server/security/SecurityUserState;)Lcom/miui/server/security/SecurityUserState;

    move-result-object v0

    .line 414
    .local v0, "transferUserState":Lcom/miui/server/security/SecurityUserState;
    iget-object v1, p0, Lcom/miui/server/security/AccessControlImpl;->mService:Lcom/miui/server/SecurityManagerService;

    iget-object v1, v1, Lcom/miui/server/SecurityManagerService;->mSettingsObserver:Lcom/miui/server/security/SecuritySettingsObserver;

    invoke-virtual {v1, v0}, Lcom/miui/server/security/SecuritySettingsObserver;->initAccessControlSettingsLocked(Lcom/miui/server/security/SecurityUserState;)V

    .line 415
    iget-boolean v1, v0, Lcom/miui/server/security/SecurityUserState;->mAccessControlLockConvenient:Z

    return v1
.end method

.method private getAccessControlLockMode(Lcom/miui/server/security/SecurityUserState;)I
    .locals 2
    .param p1, "userState"    # Lcom/miui/server/security/SecurityUserState;

    .line 407
    invoke-direct {p0, p1}, Lcom/miui/server/security/AccessControlImpl;->changeUserState(Lcom/miui/server/security/SecurityUserState;)Lcom/miui/server/security/SecurityUserState;

    move-result-object v0

    .line 408
    .local v0, "transferUserState":Lcom/miui/server/security/SecurityUserState;
    iget-object v1, p0, Lcom/miui/server/security/AccessControlImpl;->mService:Lcom/miui/server/SecurityManagerService;

    iget-object v1, v1, Lcom/miui/server/SecurityManagerService;->mSettingsObserver:Lcom/miui/server/security/SecuritySettingsObserver;

    invoke-virtual {v1, v0}, Lcom/miui/server/security/SecuritySettingsObserver;->initAccessControlSettingsLocked(Lcom/miui/server/security/SecurityUserState;)V

    .line 409
    iget v1, v0, Lcom/miui/server/security/SecurityUserState;->mAccessControlLockMode:I

    return v1
.end method

.method private isPackageAccessControlPass(Lcom/miui/server/security/SecurityUserState;)Z
    .locals 6
    .param p1, "userState"    # Lcom/miui/server/security/SecurityUserState;

    .line 386
    invoke-static {}, Lmiui/securityspace/ConfigUtils;->isSupportXSpace()Z

    move-result v0

    const/4 v1, 0x1

    const/4 v2, 0x0

    if-eqz v0, :cond_2

    iget v0, p1, Lcom/miui/server/security/SecurityUserState;->userHandle:I

    const/16 v3, 0x3e7

    if-eq v0, v3, :cond_0

    iget v0, p1, Lcom/miui/server/security/SecurityUserState;->userHandle:I

    if-nez v0, :cond_2

    .line 388
    :cond_0
    iget-object v0, p0, Lcom/miui/server/security/AccessControlImpl;->mService:Lcom/miui/server/SecurityManagerService;

    invoke-virtual {v0, v2}, Lcom/miui/server/SecurityManagerService;->getUserStateLocked(I)Lcom/miui/server/security/SecurityUserState;

    move-result-object v0

    .line 389
    .local v0, "userStateOwner":Lcom/miui/server/security/SecurityUserState;
    iget-object v4, p0, Lcom/miui/server/security/AccessControlImpl;->mService:Lcom/miui/server/SecurityManagerService;

    invoke-virtual {v4, v3}, Lcom/miui/server/SecurityManagerService;->getUserStateLocked(I)Lcom/miui/server/security/SecurityUserState;

    move-result-object v3

    .line 390
    .local v3, "userStateXSpace":Lcom/miui/server/security/SecurityUserState;
    iget-object v4, v0, Lcom/miui/server/security/SecurityUserState;->mAccessControlPassPackages:Ljava/util/HashSet;

    invoke-virtual {v4}, Ljava/util/HashSet;->size()I

    move-result v4

    iget-object v5, v3, Lcom/miui/server/security/SecurityUserState;->mAccessControlPassPackages:Ljava/util/HashSet;

    invoke-virtual {v5}, Ljava/util/HashSet;->size()I

    move-result v5

    add-int/2addr v4, v5

    if-lez v4, :cond_1

    goto :goto_0

    :cond_1
    move v1, v2

    :goto_0
    return v1

    .line 392
    .end local v0    # "userStateOwner":Lcom/miui/server/security/SecurityUserState;
    .end local v3    # "userStateXSpace":Lcom/miui/server/security/SecurityUserState;
    :cond_2
    iget-object v0, p1, Lcom/miui/server/security/SecurityUserState;->mAccessControlPassPackages:Ljava/util/HashSet;

    invoke-virtual {v0}, Ljava/util/HashSet;->size()I

    move-result v0

    if-lez v0, :cond_3

    goto :goto_1

    :cond_3
    move v1, v2

    :goto_1
    return v1
.end method

.method private removeAccessControlPassLocked(Lcom/miui/server/security/SecurityUserState;Ljava/lang/String;)V
    .locals 1
    .param p1, "userState"    # Lcom/miui/server/security/SecurityUserState;
    .param p2, "packageName"    # Ljava/lang/String;

    .line 397
    const-string v0, "*"

    invoke-virtual {v0, p2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 398
    iget-object v0, p1, Lcom/miui/server/security/SecurityUserState;->mAccessControlPassPackages:Ljava/util/HashSet;

    invoke-virtual {v0}, Ljava/util/HashSet;->clear()V

    .line 399
    iget-object v0, p1, Lcom/miui/server/security/SecurityUserState;->mAccessControlLastCheck:Landroid/util/ArrayMap;

    invoke-virtual {v0}, Landroid/util/ArrayMap;->clear()V

    goto :goto_0

    .line 401
    :cond_0
    iget-object v0, p1, Lcom/miui/server/security/SecurityUserState;->mAccessControlPassPackages:Ljava/util/HashSet;

    invoke-virtual {v0, p2}, Ljava/util/HashSet;->remove(Ljava/lang/Object;)Z

    .line 403
    :goto_0
    invoke-virtual {p0}, Lcom/miui/server/security/AccessControlImpl;->updateMaskObserverValues()V

    .line 404
    return-void
.end method

.method private scheduleForMaskObserver(Ljava/lang/String;I)V
    .locals 5
    .param p1, "pkg"    # Ljava/lang/String;
    .param p2, "userHandle"    # I

    .line 429
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "_"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    .line 430
    .local v0, "token":Ljava/lang/String;
    iget-object v1, p0, Lcom/miui/server/security/AccessControlImpl;->mHandler:Landroid/os/Handler;

    invoke-virtual {v1, v0}, Landroid/os/Handler;->removeCallbacksAndEqualMessages(Ljava/lang/Object;)V

    .line 431
    iget-object v1, p0, Lcom/miui/server/security/AccessControlImpl;->mHandler:Landroid/os/Handler;

    new-instance v2, Lcom/miui/server/security/AccessControlImpl$$ExternalSyntheticLambda0;

    invoke-direct {v2, p0}, Lcom/miui/server/security/AccessControlImpl$$ExternalSyntheticLambda0;-><init>(Lcom/miui/server/security/AccessControlImpl;)V

    const-wide/32 v3, 0xea60

    invoke-virtual {v1, v2, v0, v3, v4}, Landroid/os/Handler;->postDelayed(Ljava/lang/Runnable;Ljava/lang/Object;J)Z

    .line 432
    return-void
.end method


# virtual methods
.method public activityResume(Landroid/content/Intent;)I
    .locals 22
    .param p1, "intent"    # Landroid/content/Intent;

    .line 106
    move-object/from16 v1, p0

    move-object/from16 v2, p1

    const/4 v0, 0x0

    if-nez v2, :cond_0

    .line 107
    return v0

    .line 110
    :cond_0
    invoke-virtual/range {p1 .. p1}, Landroid/content/Intent;->getComponent()Landroid/content/ComponentName;

    move-result-object v3

    .line 111
    .local v3, "componentName":Landroid/content/ComponentName;
    if-nez v3, :cond_1

    .line 112
    return v0

    .line 115
    :cond_1
    invoke-virtual {v3}, Landroid/content/ComponentName;->getPackageName()Ljava/lang/String;

    move-result-object v4

    .line 116
    .local v4, "packageName":Ljava/lang/String;
    if-nez v4, :cond_2

    .line 117
    return v0

    .line 120
    :cond_2
    invoke-static {}, Landroid/os/Binder;->getCallingUid()I

    move-result v5

    .line 121
    .local v5, "callingUid":I
    invoke-static {v5}, Landroid/os/UserHandle;->getUserId(I)I

    move-result v6

    .line 122
    .local v6, "userId":I
    iget-object v7, v1, Lcom/miui/server/security/AccessControlImpl;->mService:Lcom/miui/server/SecurityManagerService;

    iget-object v7, v7, Lcom/miui/server/SecurityManagerService;->mUserStateLock:Ljava/lang/Object;

    monitor-enter v7

    .line 123
    :try_start_0
    iget-object v8, v1, Lcom/miui/server/security/AccessControlImpl;->mService:Lcom/miui/server/SecurityManagerService;

    invoke-virtual {v8, v6}, Lcom/miui/server/SecurityManagerService;->getUserStateLocked(I)Lcom/miui/server/security/SecurityUserState;

    move-result-object v8

    .line 124
    .local v8, "userState":Lcom/miui/server/security/SecurityUserState;
    invoke-virtual {v1, v8}, Lcom/miui/server/security/AccessControlImpl;->getAccessControlEnabledLocked(Lcom/miui/server/security/SecurityUserState;)Z

    move-result v9

    .line 126
    .local v9, "enabled":Z
    if-nez v9, :cond_3

    .line 127
    monitor-exit v7

    return v0

    .line 129
    :cond_3
    const-class v10, Landroid/content/pm/PackageManagerInternal;

    invoke-static {v10}, Lcom/android/server/LocalServices;->getService(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v10

    check-cast v10, Landroid/content/pm/PackageManagerInternal;

    const-wide/16 v11, 0x0

    invoke-virtual {v10, v4, v11, v12, v6}, Landroid/content/pm/PackageManagerInternal;->getPackageUid(Ljava/lang/String;JI)I

    move-result v10

    .line 130
    .local v10, "packageUid":I
    if-eq v5, v10, :cond_4

    .line 131
    monitor-exit v7

    return v0

    .line 134
    :cond_4
    const/4 v0, 0x1

    .line 135
    .local v0, "result":I
    invoke-direct {v1, v8}, Lcom/miui/server/security/AccessControlImpl;->getAccessControlLockMode(Lcom/miui/server/security/SecurityUserState;)I

    move-result v11

    .line 137
    .local v11, "lockMode":I
    iget-object v12, v8, Lcom/miui/server/security/SecurityUserState;->mLastResumePackage:Ljava/lang/String;

    .line 138
    .local v12, "oldResumePackage":Ljava/lang/String;
    iput-object v4, v8, Lcom/miui/server/security/SecurityUserState;->mLastResumePackage:Ljava/lang/String;

    .line 139
    iget-object v13, v8, Lcom/miui/server/security/SecurityUserState;->mAccessControlPassPackages:Ljava/util/HashSet;

    .line 140
    .local v13, "passPackages":Ljava/util/HashSet;, "Ljava/util/HashSet<Ljava/lang/String;>;"
    const/4 v14, 0x2

    if-ne v11, v14, :cond_5

    .line 141
    if-eqz v12, :cond_5

    invoke-virtual {v13, v12}, Ljava/util/HashSet;->contains(Ljava/lang/Object;)Z

    move-result v15

    if-eqz v15, :cond_5

    .line 142
    iget-object v15, v8, Lcom/miui/server/security/SecurityUserState;->mAccessControlLastCheck:Landroid/util/ArrayMap;

    invoke-static {}, Landroid/os/SystemClock;->elapsedRealtime()J

    move-result-wide v16

    invoke-static/range {v16 .. v17}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v14

    invoke-virtual {v15, v12, v14}, Landroid/util/ArrayMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 143
    invoke-direct {v1, v4, v6}, Lcom/miui/server/security/AccessControlImpl;->scheduleForMaskObserver(Ljava/lang/String;I)V

    .line 146
    :cond_5
    iget-object v14, v1, Lcom/miui/server/security/AccessControlImpl;->mService:Lcom/miui/server/SecurityManagerService;

    iget-object v15, v8, Lcom/miui/server/security/SecurityUserState;->mPackages:Ljava/util/HashMap;

    invoke-virtual {v14, v15, v4}, Lcom/miui/server/SecurityManagerService;->getPackageSetting(Ljava/util/HashMap;Ljava/lang/String;)Lcom/miui/server/security/SecurityPackageSettings;

    move-result-object v14

    .line 147
    .local v14, "ps":Lcom/miui/server/security/SecurityPackageSettings;
    iget-boolean v15, v14, Lcom/miui/server/security/SecurityPackageSettings;->accessControl:Z

    if-nez v15, :cond_7

    .line 148
    if-nez v11, :cond_6

    .line 149
    invoke-direct {v1, v6}, Lcom/miui/server/security/AccessControlImpl;->clearPassPackages(I)V

    .line 151
    :cond_6
    monitor-exit v7

    return v0

    .line 153
    :cond_7
    const/4 v15, 0x2

    or-int/2addr v0, v15

    .line 155
    invoke-virtual {v13, v4}, Ljava/util/HashSet;->contains(Ljava/lang/Object;)Z

    move-result v16

    if-eqz v16, :cond_b

    .line 156
    if-ne v11, v15, :cond_9

    .line 157
    iget-object v15, v8, Lcom/miui/server/security/SecurityUserState;->mAccessControlLastCheck:Landroid/util/ArrayMap;

    invoke-virtual {v15, v4}, Landroid/util/ArrayMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v15

    check-cast v15, Ljava/lang/Long;

    .line 158
    .local v15, "lastTime":Ljava/lang/Long;
    if-eqz v15, :cond_8

    .line 159
    invoke-static {}, Landroid/os/SystemClock;->elapsedRealtime()J

    move-result-wide v16

    .line 160
    .local v16, "realtime":J
    invoke-virtual {v15}, Ljava/lang/Long;->longValue()J

    move-result-wide v18

    sub-long v18, v16, v18

    const-wide/32 v20, 0xea60

    cmp-long v18, v18, v20

    if-gez v18, :cond_8

    .line 161
    or-int/lit8 v0, v0, 0x4

    .line 162
    monitor-exit v7

    return v0

    .line 165
    .end local v16    # "realtime":J
    :cond_8
    invoke-virtual {v13, v4}, Ljava/util/HashSet;->remove(Ljava/lang/Object;)Z

    .line 166
    invoke-virtual/range {p0 .. p0}, Lcom/miui/server/security/AccessControlImpl;->updateMaskObserverValues()V

    .line 167
    .end local v15    # "lastTime":Ljava/lang/Long;
    goto :goto_0

    .line 168
    :cond_9
    or-int/lit8 v0, v0, 0x4

    .line 169
    if-nez v11, :cond_a

    .line 170
    invoke-direct {v1, v6}, Lcom/miui/server/security/AccessControlImpl;->clearPassPackages(I)V

    .line 171
    invoke-virtual {v13, v4}, Ljava/util/HashSet;->add(Ljava/lang/Object;)Z

    .line 172
    invoke-virtual/range {p0 .. p0}, Lcom/miui/server/security/AccessControlImpl;->updateMaskObserverValues()V

    .line 174
    :cond_a
    monitor-exit v7

    return v0

    .line 178
    :cond_b
    :goto_0
    if-nez v11, :cond_c

    .line 179
    invoke-direct {v1, v6}, Lcom/miui/server/security/AccessControlImpl;->clearPassPackages(I)V

    .line 181
    :cond_c
    iget-object v15, v8, Lcom/miui/server/security/SecurityUserState;->mAccessControlCanceled:Landroid/util/ArraySet;

    invoke-virtual {v15, v4}, Landroid/util/ArraySet;->contains(Ljava/lang/Object;)Z

    move-result v15

    if-eqz v15, :cond_d

    .line 182
    or-int/lit8 v0, v0, 0x8

    .line 183
    monitor-exit v7

    return v0

    .line 186
    :cond_d
    const/4 v15, 0x1

    if-ne v11, v15, :cond_e

    .line 187
    invoke-direct {v1, v8}, Lcom/miui/server/security/AccessControlImpl;->getAccessControlLockConvenient(Lcom/miui/server/security/SecurityUserState;)Z

    move-result v16

    if-eqz v16, :cond_e

    .line 188
    invoke-direct {v1, v8}, Lcom/miui/server/security/AccessControlImpl;->isPackageAccessControlPass(Lcom/miui/server/security/SecurityUserState;)Z

    move-result v16

    if-nez v16, :cond_f

    :cond_e
    iget-object v15, v1, Lcom/miui/server/security/AccessControlImpl;->mService:Lcom/miui/server/SecurityManagerService;

    iget-object v15, v15, Lcom/miui/server/SecurityManagerService;->mAccessController:Lcom/miui/server/AccessController;

    .line 189
    invoke-virtual {v15, v2, v4}, Lcom/miui/server/AccessController;->skipActivity(Landroid/content/Intent;Ljava/lang/String;)Z

    move-result v15

    if-nez v15, :cond_f

    iget-object v15, v1, Lcom/miui/server/security/AccessControlImpl;->mService:Lcom/miui/server/SecurityManagerService;

    iget-object v15, v15, Lcom/miui/server/SecurityManagerService;->mAccessController:Lcom/miui/server/AccessController;

    .line 190
    const/4 v1, 0x1

    invoke-virtual {v15, v1, v4, v2}, Lcom/miui/server/AccessController;->filterIntentLocked(ZLjava/lang/String;Landroid/content/Intent;)Z

    move-result v1

    if-eqz v1, :cond_10

    .line 191
    :cond_f
    or-int/lit8 v0, v0, 0x4

    .line 194
    :cond_10
    monitor-exit v7

    return v0

    .line 195
    .end local v0    # "result":I
    .end local v8    # "userState":Lcom/miui/server/security/SecurityUserState;
    .end local v9    # "enabled":Z
    .end local v10    # "packageUid":I
    .end local v11    # "lockMode":I
    .end local v12    # "oldResumePackage":Ljava/lang/String;
    .end local v13    # "passPackages":Ljava/util/HashSet;, "Ljava/util/HashSet<Ljava/lang/String;>;"
    .end local v14    # "ps":Lcom/miui/server/security/SecurityPackageSettings;
    :catchall_0
    move-exception v0

    monitor-exit v7
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method

.method public addAccessControlPassForUser(Ljava/lang/String;I)V
    .locals 6
    .param p1, "packageName"    # Ljava/lang/String;
    .param p2, "userId"    # I

    .line 199
    iget-object v0, p0, Lcom/miui/server/security/AccessControlImpl;->mService:Lcom/miui/server/SecurityManagerService;

    iget-object v0, v0, Lcom/miui/server/SecurityManagerService;->mUserStateLock:Ljava/lang/Object;

    monitor-enter v0

    .line 200
    :try_start_0
    iget-object v1, p0, Lcom/miui/server/security/AccessControlImpl;->mService:Lcom/miui/server/SecurityManagerService;

    invoke-virtual {v1, p2}, Lcom/miui/server/SecurityManagerService;->getUserStateLocked(I)Lcom/miui/server/security/SecurityUserState;

    move-result-object v1

    .line 201
    .local v1, "userState":Lcom/miui/server/security/SecurityUserState;
    invoke-direct {p0, v1}, Lcom/miui/server/security/AccessControlImpl;->getAccessControlLockMode(Lcom/miui/server/security/SecurityUserState;)I

    move-result v2

    .line 202
    .local v2, "lockMode":I
    const/4 v3, 0x2

    if-ne v2, v3, :cond_0

    .line 203
    iget-object v3, v1, Lcom/miui/server/security/SecurityUserState;->mAccessControlLastCheck:Landroid/util/ArrayMap;

    invoke-static {}, Landroid/os/SystemClock;->elapsedRealtime()J

    move-result-wide v4

    invoke-static {v4, v5}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v4

    invoke-virtual {v3, p1, v4}, Landroid/util/ArrayMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 204
    invoke-direct {p0, p1, p2}, Lcom/miui/server/security/AccessControlImpl;->scheduleForMaskObserver(Ljava/lang/String;I)V

    goto :goto_0

    .line 206
    :cond_0
    invoke-virtual {p0}, Lcom/miui/server/security/AccessControlImpl;->updateMaskObserverValues()V

    .line 208
    :goto_0
    iget-object v3, v1, Lcom/miui/server/security/SecurityUserState;->mAccessControlPassPackages:Ljava/util/HashSet;

    invoke-virtual {v3, p1}, Ljava/util/HashSet;->add(Ljava/lang/Object;)Z

    .line 209
    nop

    .end local v1    # "userState":Lcom/miui/server/security/SecurityUserState;
    .end local v2    # "lockMode":I
    monitor-exit v0

    .line 210
    return-void

    .line 209
    :catchall_0
    move-exception v1

    monitor-exit v0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v1
.end method

.method public checkAccessControlPassLocked(Ljava/lang/String;Landroid/content/Intent;I)Z
    .locals 4
    .param p1, "packageName"    # Ljava/lang/String;
    .param p2, "intent"    # Landroid/content/Intent;
    .param p3, "userId"    # I

    .line 235
    iget-object v0, p0, Lcom/miui/server/security/AccessControlImpl;->mService:Lcom/miui/server/SecurityManagerService;

    iget-object v0, v0, Lcom/miui/server/SecurityManagerService;->mUserStateLock:Ljava/lang/Object;

    monitor-enter v0

    .line 236
    :try_start_0
    iget-object v1, p0, Lcom/miui/server/security/AccessControlImpl;->mService:Lcom/miui/server/SecurityManagerService;

    invoke-virtual {v1, p3}, Lcom/miui/server/SecurityManagerService;->getUserStateLocked(I)Lcom/miui/server/security/SecurityUserState;

    move-result-object v1

    .line 237
    .local v1, "userState":Lcom/miui/server/security/SecurityUserState;
    iget-object v2, p0, Lcom/miui/server/security/AccessControlImpl;->mService:Lcom/miui/server/SecurityManagerService;

    iget-object v3, v1, Lcom/miui/server/security/SecurityUserState;->mPackages:Ljava/util/HashMap;

    invoke-virtual {v2, v3, p1}, Lcom/miui/server/SecurityManagerService;->getPackageSetting(Ljava/util/HashMap;Ljava/lang/String;)Lcom/miui/server/security/SecurityPackageSettings;

    move-result-object v2

    .line 238
    .local v2, "ps":Lcom/miui/server/security/SecurityPackageSettings;
    iget-boolean v3, v2, Lcom/miui/server/security/SecurityPackageSettings;->accessControl:Z

    if-nez v3, :cond_0

    .line 239
    monitor-exit v0

    const/4 v0, 0x1

    return v0

    .line 241
    :cond_0
    invoke-direct {p0, v1, p1, p2}, Lcom/miui/server/security/AccessControlImpl;->checkAccessControlPassLockedCore(Lcom/miui/server/security/SecurityUserState;Ljava/lang/String;Landroid/content/Intent;)Z

    move-result v3

    monitor-exit v0

    return v3

    .line 242
    .end local v1    # "userState":Lcom/miui/server/security/SecurityUserState;
    .end local v2    # "ps":Lcom/miui/server/security/SecurityPackageSettings;
    :catchall_0
    move-exception v1

    monitor-exit v0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v1
.end method

.method public finishAccessControl(Ljava/lang/String;I)V
    .locals 6
    .param p1, "packageName"    # Ljava/lang/String;
    .param p2, "userId"    # I

    .line 330
    if-nez p1, :cond_0

    .line 331
    return-void

    .line 333
    :cond_0
    iget-object v0, p0, Lcom/miui/server/security/AccessControlImpl;->mService:Lcom/miui/server/SecurityManagerService;

    iget-object v0, v0, Lcom/miui/server/SecurityManagerService;->mUserStateLock:Ljava/lang/Object;

    monitor-enter v0

    .line 334
    :try_start_0
    iget-object v1, p0, Lcom/miui/server/security/AccessControlImpl;->mService:Lcom/miui/server/SecurityManagerService;

    invoke-virtual {v1, p2}, Lcom/miui/server/SecurityManagerService;->getUserStateLocked(I)Lcom/miui/server/security/SecurityUserState;

    move-result-object v1

    .line 335
    .local v1, "userState":Lcom/miui/server/security/SecurityUserState;
    iget-object v2, v1, Lcom/miui/server/security/SecurityUserState;->mAccessControlCanceled:Landroid/util/ArraySet;

    invoke-virtual {v2, p1}, Landroid/util/ArraySet;->add(Ljava/lang/Object;)Z

    .line 336
    iget-object v2, p0, Lcom/miui/server/security/AccessControlImpl;->mHandler:Landroid/os/Handler;

    const/4 v3, 0x4

    invoke-virtual {v2, v3}, Landroid/os/Handler;->obtainMessage(I)Landroid/os/Message;

    move-result-object v2

    .line 337
    .local v2, "msg":Landroid/os/Message;
    iput p2, v2, Landroid/os/Message;->arg1:I

    .line 338
    iput-object p1, v2, Landroid/os/Message;->obj:Ljava/lang/Object;

    .line 339
    iget-object v3, p0, Lcom/miui/server/security/AccessControlImpl;->mHandler:Landroid/os/Handler;

    const-wide/16 v4, 0x1f4

    invoke-virtual {v3, v2, v4, v5}, Landroid/os/Handler;->sendMessageDelayed(Landroid/os/Message;J)Z

    .line 340
    nop

    .end local v1    # "userState":Lcom/miui/server/security/SecurityUserState;
    .end local v2    # "msg":Landroid/os/Message;
    monitor-exit v0

    .line 341
    return-void

    .line 340
    :catchall_0
    move-exception v1

    monitor-exit v0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v1
.end method

.method public getAccessControlEnabledLocked(Lcom/miui/server/security/SecurityUserState;)Z
    .locals 2
    .param p1, "userState"    # Lcom/miui/server/security/SecurityUserState;

    .line 344
    invoke-direct {p0, p1}, Lcom/miui/server/security/AccessControlImpl;->changeUserState(Lcom/miui/server/security/SecurityUserState;)Lcom/miui/server/security/SecurityUserState;

    move-result-object v0

    .line 345
    .local v0, "transferUserState":Lcom/miui/server/security/SecurityUserState;
    iget-object v1, p0, Lcom/miui/server/security/AccessControlImpl;->mService:Lcom/miui/server/SecurityManagerService;

    iget-object v1, v1, Lcom/miui/server/SecurityManagerService;->mSettingsObserver:Lcom/miui/server/security/SecuritySettingsObserver;

    invoke-virtual {v1, v0}, Lcom/miui/server/security/SecuritySettingsObserver;->initAccessControlSettingsLocked(Lcom/miui/server/security/SecurityUserState;)V

    .line 346
    iget-boolean v1, v0, Lcom/miui/server/security/SecurityUserState;->mAccessControlEnabled:Z

    return v1
.end method

.method public getApplicationAccessControlEnabledLocked(Ljava/lang/String;I)Z
    .locals 4
    .param p1, "packageName"    # Ljava/lang/String;
    .param p2, "userId"    # I

    .line 213
    iget-object v0, p0, Lcom/miui/server/security/AccessControlImpl;->mService:Lcom/miui/server/SecurityManagerService;

    iget-object v0, v0, Lcom/miui/server/SecurityManagerService;->mUserStateLock:Ljava/lang/Object;

    monitor-enter v0

    .line 214
    :try_start_0
    iget-object v1, p0, Lcom/miui/server/security/AccessControlImpl;->mService:Lcom/miui/server/SecurityManagerService;

    invoke-virtual {v1, p2}, Lcom/miui/server/SecurityManagerService;->getUserStateLocked(I)Lcom/miui/server/security/SecurityUserState;

    move-result-object v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 216
    .local v1, "userState":Lcom/miui/server/security/SecurityUserState;
    :try_start_1
    iget-object v2, p0, Lcom/miui/server/security/AccessControlImpl;->mService:Lcom/miui/server/SecurityManagerService;

    iget-object v3, v1, Lcom/miui/server/security/SecurityUserState;->mPackages:Ljava/util/HashMap;

    invoke-virtual {v2, v3, p1}, Lcom/miui/server/SecurityManagerService;->getPackageSetting(Ljava/util/HashMap;Ljava/lang/String;)Lcom/miui/server/security/SecurityPackageSettings;

    move-result-object v2

    .line 217
    .local v2, "ps":Lcom/miui/server/security/SecurityPackageSettings;
    iget-boolean v3, v2, Lcom/miui/server/security/SecurityPackageSettings;->accessControl:Z
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_0
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    :try_start_2
    monitor-exit v0

    return v3

    .line 218
    .end local v2    # "ps":Lcom/miui/server/security/SecurityPackageSettings;
    :catch_0
    move-exception v2

    .line 219
    .local v2, "e":Ljava/lang/Exception;
    monitor-exit v0

    const/4 v0, 0x0

    return v0

    .line 221
    .end local v1    # "userState":Lcom/miui/server/security/SecurityUserState;
    .end local v2    # "e":Ljava/lang/Exception;
    :catchall_0
    move-exception v1

    monitor-exit v0
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    throw v1
.end method

.method public getApplicationMaskNotificationEnabledLocked(Ljava/lang/String;I)Z
    .locals 4
    .param p1, "packageName"    # Ljava/lang/String;
    .param p2, "userId"    # I

    .line 295
    iget-object v0, p0, Lcom/miui/server/security/AccessControlImpl;->mService:Lcom/miui/server/SecurityManagerService;

    iget-object v0, v0, Lcom/miui/server/SecurityManagerService;->mUserStateLock:Ljava/lang/Object;

    monitor-enter v0

    .line 296
    :try_start_0
    iget-object v1, p0, Lcom/miui/server/security/AccessControlImpl;->mService:Lcom/miui/server/SecurityManagerService;

    invoke-virtual {v1, p2}, Lcom/miui/server/SecurityManagerService;->getUserStateLocked(I)Lcom/miui/server/security/SecurityUserState;

    move-result-object v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 298
    .local v1, "userState":Lcom/miui/server/security/SecurityUserState;
    :try_start_1
    iget-object v2, p0, Lcom/miui/server/security/AccessControlImpl;->mService:Lcom/miui/server/SecurityManagerService;

    iget-object v3, v1, Lcom/miui/server/security/SecurityUserState;->mPackages:Ljava/util/HashMap;

    invoke-virtual {v2, v3, p1}, Lcom/miui/server/SecurityManagerService;->getPackageSetting(Ljava/util/HashMap;Ljava/lang/String;)Lcom/miui/server/security/SecurityPackageSettings;

    move-result-object v2

    .line 299
    .local v2, "ps":Lcom/miui/server/security/SecurityPackageSettings;
    iget-boolean v3, v2, Lcom/miui/server/security/SecurityPackageSettings;->maskNotification:Z
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_0
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    :try_start_2
    monitor-exit v0

    return v3

    .line 300
    .end local v2    # "ps":Lcom/miui/server/security/SecurityPackageSettings;
    :catch_0
    move-exception v2

    .line 301
    .local v2, "e":Ljava/lang/Exception;
    monitor-exit v0

    const/4 v0, 0x0

    return v0

    .line 303
    .end local v1    # "userState":Lcom/miui/server/security/SecurityUserState;
    .end local v2    # "e":Ljava/lang/Exception;
    :catchall_0
    move-exception v1

    monitor-exit v0
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    throw v1
.end method

.method public getShouldMaskApps()Ljava/lang/String;
    .locals 11

    .line 246
    iget-object v0, p0, Lcom/miui/server/security/AccessControlImpl;->mService:Lcom/miui/server/SecurityManagerService;

    iget-object v0, v0, Lcom/miui/server/SecurityManagerService;->mUserStateLock:Ljava/lang/Object;

    monitor-enter v0

    .line 248
    const/4 v1, 0x0

    :try_start_0
    new-instance v2, Lorg/json/JSONArray;

    invoke-direct {v2}, Lorg/json/JSONArray;-><init>()V

    .line 249
    .local v2, "maskArray":Lorg/json/JSONArray;
    const/4 v3, 0x0

    .local v3, "i":I
    :goto_0
    iget-object v4, p0, Lcom/miui/server/security/AccessControlImpl;->mService:Lcom/miui/server/SecurityManagerService;

    iget-object v4, v4, Lcom/miui/server/SecurityManagerService;->mUserStates:Landroid/util/SparseArray;

    invoke-virtual {v4}, Landroid/util/SparseArray;->size()I

    move-result v4

    if-ge v3, v4, :cond_4

    .line 250
    iget-object v4, p0, Lcom/miui/server/security/AccessControlImpl;->mService:Lcom/miui/server/SecurityManagerService;

    iget-object v4, v4, Lcom/miui/server/SecurityManagerService;->mUserStates:Landroid/util/SparseArray;

    invoke-virtual {v4, v3}, Landroid/util/SparseArray;->valueAt(I)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lcom/miui/server/security/SecurityUserState;

    .line 251
    .local v4, "userState":Lcom/miui/server/security/SecurityUserState;
    new-instance v5, Lorg/json/JSONObject;

    invoke-direct {v5}, Lorg/json/JSONObject;-><init>()V

    .line 252
    .local v5, "userStateObj":Lorg/json/JSONObject;
    const-string/jumbo v6, "userId"

    iget v7, v4, Lcom/miui/server/security/SecurityUserState;->userHandle:I

    invoke-virtual {v5, v6, v7}, Lorg/json/JSONObject;->put(Ljava/lang/String;I)Lorg/json/JSONObject;

    .line 253
    new-instance v6, Lorg/json/JSONArray;

    invoke-direct {v6}, Lorg/json/JSONArray;-><init>()V

    .line 254
    .local v6, "itemArray":Lorg/json/JSONArray;
    iget-boolean v7, v4, Lcom/miui/server/security/SecurityUserState;->mAccessControlEnabled:Z

    .line 256
    .local v7, "enabled":Z
    iget v8, v4, Lcom/miui/server/security/SecurityUserState;->userHandle:I

    const/16 v9, 0x3e7

    if-ne v8, v9, :cond_0

    .line 257
    iget-object v8, p0, Lcom/miui/server/security/AccessControlImpl;->mService:Lcom/miui/server/SecurityManagerService;

    const/4 v9, 0x0

    invoke-virtual {v8, v9}, Lcom/miui/server/SecurityManagerService;->getUserStateLocked(I)Lcom/miui/server/security/SecurityUserState;

    move-result-object v8

    iget-boolean v8, v8, Lcom/miui/server/security/SecurityUserState;->mAccessControlEnabled:Z

    move v7, v8

    .line 259
    :cond_0
    if-eqz v7, :cond_3

    .line 260
    iget-object v8, v4, Lcom/miui/server/security/SecurityUserState;->mPackages:Ljava/util/HashMap;

    invoke-virtual {v8}, Ljava/util/HashMap;->values()Ljava/util/Collection;

    move-result-object v8

    invoke-interface {v8}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v8

    :cond_1
    :goto_1
    invoke-interface {v8}, Ljava/util/Iterator;->hasNext()Z

    move-result v9

    if-eqz v9, :cond_3

    invoke-interface {v8}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v9

    check-cast v9, Lcom/miui/server/security/SecurityPackageSettings;

    .line 261
    .local v9, "ps":Lcom/miui/server/security/SecurityPackageSettings;
    iget-object v10, v9, Lcom/miui/server/security/SecurityPackageSettings;->name:Ljava/lang/String;

    invoke-static {v10}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v10

    if-nez v10, :cond_1

    iget-boolean v10, v9, Lcom/miui/server/security/SecurityPackageSettings;->accessControl:Z

    if-eqz v10, :cond_1

    iget-boolean v10, v9, Lcom/miui/server/security/SecurityPackageSettings;->maskNotification:Z

    if-eqz v10, :cond_1

    iget-object v10, v9, Lcom/miui/server/security/SecurityPackageSettings;->name:Ljava/lang/String;

    .line 262
    invoke-direct {p0, v4, v10, v1}, Lcom/miui/server/security/AccessControlImpl;->checkAccessControlPassLockedCore(Lcom/miui/server/security/SecurityUserState;Ljava/lang/String;Landroid/content/Intent;)Z

    move-result v10

    if-eqz v10, :cond_2

    .line 263
    goto :goto_1

    .line 265
    :cond_2
    iget-object v10, v9, Lcom/miui/server/security/SecurityPackageSettings;->name:Ljava/lang/String;

    invoke-virtual {v6, v10}, Lorg/json/JSONArray;->put(Ljava/lang/Object;)Lorg/json/JSONArray;

    .line 266
    nop

    .end local v9    # "ps":Lcom/miui/server/security/SecurityPackageSettings;
    goto :goto_1

    .line 268
    :cond_3
    const-string/jumbo v8, "shouldMaskApps"

    invoke-virtual {v5, v8, v6}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;

    .line 269
    invoke-virtual {v2, v5}, Lorg/json/JSONArray;->put(Ljava/lang/Object;)Lorg/json/JSONArray;

    .line 249
    nop

    .end local v4    # "userState":Lcom/miui/server/security/SecurityUserState;
    .end local v5    # "userStateObj":Lorg/json/JSONObject;
    .end local v6    # "itemArray":Lorg/json/JSONArray;
    .end local v7    # "enabled":Z
    add-int/lit8 v3, v3, 0x1

    goto :goto_0

    .line 271
    .end local v3    # "i":I
    :cond_4
    invoke-virtual {v2}, Lorg/json/JSONArray;->toString()Ljava/lang/String;

    move-result-object v1
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    :try_start_1
    monitor-exit v0

    return-object v1

    .line 276
    .end local v2    # "maskArray":Lorg/json/JSONArray;
    :catchall_0
    move-exception v1

    goto :goto_2

    .line 272
    :catch_0
    move-exception v2

    .line 273
    .local v2, "e":Ljava/lang/Exception;
    const-string v3, "AccessControlImpl"

    const-string v4, "getShouldMaskApps failed. "

    invoke-static {v3, v4, v2}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    .line 275
    nop

    .end local v2    # "e":Ljava/lang/Exception;
    monitor-exit v0

    return-object v1

    .line 276
    :goto_2
    monitor-exit v0
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    throw v1
.end method

.method public needFinishAccessControl(Landroid/os/IBinder;)Z
    .locals 6
    .param p1, "token"    # Landroid/os/IBinder;

    .line 317
    invoke-static {p1}, Lcom/android/server/wm/WindowProcessUtils;->getTaskIntentForToken(Landroid/os/IBinder;)Ljava/util/ArrayList;

    move-result-object v0

    .line 318
    .local v0, "taskIntent":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Landroid/content/Intent;>;"
    if-eqz v0, :cond_0

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v1

    const/4 v2, 0x1

    if-le v1, v2, :cond_0

    .line 319
    invoke-virtual {v0, v2}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/content/Intent;

    .line 320
    .local v1, "intent":Landroid/content/Intent;
    invoke-virtual {v1}, Landroid/content/Intent;->getComponent()Landroid/content/ComponentName;

    move-result-object v3

    .line 321
    .local v3, "component":Landroid/content/ComponentName;
    if-eqz v3, :cond_0

    .line 322
    iget-object v4, p0, Lcom/miui/server/security/AccessControlImpl;->mService:Lcom/miui/server/SecurityManagerService;

    iget-object v4, v4, Lcom/miui/server/SecurityManagerService;->mAccessController:Lcom/miui/server/AccessController;

    .line 323
    invoke-virtual {v3}, Landroid/content/ComponentName;->getPackageName()Ljava/lang/String;

    move-result-object v5

    .line 322
    invoke-virtual {v4, v2, v5, v1}, Lcom/miui/server/AccessController;->filterIntentLocked(ZLjava/lang/String;Landroid/content/Intent;)Z

    move-result v2

    return v2

    .line 326
    .end local v1    # "intent":Landroid/content/Intent;
    .end local v3    # "component":Landroid/content/ComponentName;
    :cond_0
    const/4 v1, 0x0

    return v1
.end method

.method public removeAccessControlPassAsUser(Ljava/lang/String;I)V
    .locals 18
    .param p1, "packageName"    # Ljava/lang/String;
    .param p2, "userId"    # I

    .line 55
    move-object/from16 v1, p0

    move-object/from16 v2, p1

    move/from16 v3, p2

    const/4 v4, 0x0

    .line 56
    .local v4, "pkgName":Ljava/lang/String;
    const/4 v5, 0x0

    .line 57
    .local v5, "token":Landroid/os/IBinder;
    const/4 v0, 0x0

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v6

    .line 58
    .local v6, "activityUserId":Ljava/lang/Integer;
    const/4 v7, 0x0

    .line 60
    .local v7, "checkAccessControlPass":Z
    const/4 v0, 0x0

    .line 61
    .local v0, "topActivity":Ljava/util/HashMap;, "Ljava/util/HashMap<Ljava/lang/String;Ljava/lang/Object;>;"
    const/4 v8, -0x1

    if-ne v3, v8, :cond_0

    .line 62
    invoke-static {}, Lcom/android/server/wm/WindowProcessUtils;->getTopRunningActivityInfo()Ljava/util/HashMap;

    move-result-object v0

    move-object v9, v0

    goto :goto_0

    .line 61
    :cond_0
    move-object v9, v0

    .line 65
    .end local v0    # "topActivity":Ljava/util/HashMap;, "Ljava/util/HashMap<Ljava/lang/String;Ljava/lang/Object;>;"
    .local v9, "topActivity":Ljava/util/HashMap;, "Ljava/util/HashMap<Ljava/lang/String;Ljava/lang/Object;>;"
    :goto_0
    iget-object v0, v1, Lcom/miui/server/security/AccessControlImpl;->mService:Lcom/miui/server/SecurityManagerService;

    iget-object v10, v0, Lcom/miui/server/SecurityManagerService;->mUserStateLock:Ljava/lang/Object;

    monitor-enter v10

    .line 66
    if-ne v3, v8, :cond_4

    .line 67
    :try_start_0
    iget-object v0, v1, Lcom/miui/server/security/AccessControlImpl;->mService:Lcom/miui/server/SecurityManagerService;

    iget-object v0, v0, Lcom/miui/server/SecurityManagerService;->mUserStates:Landroid/util/SparseArray;

    invoke-virtual {v0}, Landroid/util/SparseArray;->size()I

    move-result v0

    .line 68
    .local v0, "size":I
    const/4 v11, 0x0

    .local v11, "i":I
    :goto_1
    if-ge v11, v0, :cond_1

    .line 69
    iget-object v12, v1, Lcom/miui/server/security/AccessControlImpl;->mService:Lcom/miui/server/SecurityManagerService;

    iget-object v12, v12, Lcom/miui/server/SecurityManagerService;->mUserStates:Landroid/util/SparseArray;

    invoke-virtual {v12, v11}, Landroid/util/SparseArray;->valueAt(I)Ljava/lang/Object;

    move-result-object v12

    check-cast v12, Lcom/miui/server/security/SecurityUserState;

    .line 70
    .local v12, "userState":Lcom/miui/server/security/SecurityUserState;
    invoke-direct {v1, v12, v2}, Lcom/miui/server/security/AccessControlImpl;->removeAccessControlPassLocked(Lcom/miui/server/security/SecurityUserState;Ljava/lang/String;)V

    .line 68
    .end local v12    # "userState":Lcom/miui/server/security/SecurityUserState;
    add-int/lit8 v11, v11, 0x1

    goto :goto_1

    .line 72
    .end local v11    # "i":I
    :cond_1
    invoke-static {}, Lcom/android/server/am/ProcessUtils;->getCurrentUserId()I

    move-result v11

    .line 73
    .local v11, "currentUserId":I
    iget-object v12, v1, Lcom/miui/server/security/AccessControlImpl;->mService:Lcom/miui/server/SecurityManagerService;

    invoke-virtual {v12, v11}, Lcom/miui/server/SecurityManagerService;->getUserStateLocked(I)Lcom/miui/server/security/SecurityUserState;

    move-result-object v12

    .line 74
    .restart local v12    # "userState":Lcom/miui/server/security/SecurityUserState;
    invoke-virtual {v1, v12}, Lcom/miui/server/security/AccessControlImpl;->getAccessControlEnabledLocked(Lcom/miui/server/security/SecurityUserState;)Z

    move-result v13

    .line 75
    .local v13, "enabled":Z
    if-nez v13, :cond_2

    .line 76
    monitor-exit v10

    return-void

    .line 78
    :cond_2
    if-eqz v9, :cond_3

    .line 79
    const-string v14, "packageName"

    invoke-virtual {v9, v14}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v14

    check-cast v14, Ljava/lang/String;

    move-object v4, v14

    .line 80
    const-string/jumbo v14, "token"

    invoke-virtual {v9, v14}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v14

    check-cast v14, Landroid/os/IBinder;

    move-object v5, v14

    .line 81
    const-string/jumbo v14, "userId"

    invoke-virtual {v9, v14}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v14

    check-cast v14, Ljava/lang/Integer;

    move-object v6, v14

    .line 82
    invoke-virtual {v6}, Ljava/lang/Integer;->intValue()I

    move-result v14

    const/4 v15, 0x0

    invoke-virtual {v1, v4, v15, v14}, Lcom/miui/server/security/AccessControlImpl;->checkAccessControlPassLocked(Ljava/lang/String;Landroid/content/Intent;I)Z

    move-result v14

    move v7, v14

    .line 84
    .end local v0    # "size":I
    .end local v11    # "currentUserId":I
    .end local v12    # "userState":Lcom/miui/server/security/SecurityUserState;
    .end local v13    # "enabled":Z
    :cond_3
    goto :goto_2

    .line 85
    :cond_4
    iget-object v0, v1, Lcom/miui/server/security/AccessControlImpl;->mService:Lcom/miui/server/SecurityManagerService;

    invoke-virtual {v0, v3}, Lcom/miui/server/SecurityManagerService;->getUserStateLocked(I)Lcom/miui/server/security/SecurityUserState;

    move-result-object v0

    .line 86
    .local v0, "userState":Lcom/miui/server/security/SecurityUserState;
    invoke-direct {v1, v0, v2}, Lcom/miui/server/security/AccessControlImpl;->removeAccessControlPassLocked(Lcom/miui/server/security/SecurityUserState;Ljava/lang/String;)V

    .line 88
    .end local v0    # "userState":Lcom/miui/server/security/SecurityUserState;
    :goto_2
    monitor-exit v10
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 90
    if-ne v3, v8, :cond_5

    if-eqz v9, :cond_5

    .line 91
    if-nez v7, :cond_5

    .line 93
    const/4 v11, 0x1

    const/4 v13, 0x0

    const/4 v14, -0x1

    const/4 v15, 0x1

    .line 94
    :try_start_1
    invoke-virtual {v6}, Ljava/lang/Integer;->intValue()I

    move-result v16

    const/16 v17, 0x0

    .line 93
    move-object v12, v4

    invoke-static/range {v11 .. v17}, Lmiui/security/SecurityManager;->getCheckAccessIntent(ZLjava/lang/String;Landroid/content/Intent;IZILandroid/os/Bundle;)Landroid/content/Intent;

    move-result-object v0

    .line 95
    .local v0, "intent":Landroid/content/Intent;
    const-string v8, "miui.KEYGUARD_LOCKED"

    const/4 v10, 0x1

    invoke-virtual {v0, v8, v10}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    .line 96
    iget-object v11, v1, Lcom/miui/server/security/AccessControlImpl;->mContext:Landroid/content/Context;

    const/4 v12, 0x0

    const/4 v14, 0x0

    .line 97
    invoke-virtual {v6}, Ljava/lang/Integer;->intValue()I

    move-result v16

    .line 96
    move-object v13, v5

    move-object v15, v0

    invoke-static/range {v11 .. v16}, Lmiui/security/SecurityManagerCompat;->startActvityAsUser(Landroid/content/Context;Landroid/app/IApplicationThread;Landroid/os/IBinder;Ljava/lang/String;Landroid/content/Intent;I)V
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_0

    .line 100
    .end local v0    # "intent":Landroid/content/Intent;
    goto :goto_3

    .line 98
    :catch_0
    move-exception v0

    .line 99
    .local v0, "e":Ljava/lang/Exception;
    const-string v8, "AccessControlImpl"

    const-string v10, "removeAccessControlPassAsUser startActivityAsUser error "

    invoke-static {v8, v10, v0}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    .line 103
    .end local v0    # "e":Ljava/lang/Exception;
    :cond_5
    :goto_3
    return-void

    .line 88
    :catchall_0
    move-exception v0

    :try_start_2
    monitor-exit v10
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    throw v0
.end method

.method public setApplicationAccessControlEnabledForUser(Ljava/lang/String;ZI)V
    .locals 4
    .param p1, "packageName"    # Ljava/lang/String;
    .param p2, "enabled"    # Z
    .param p3, "userId"    # I

    .line 225
    iget-object v0, p0, Lcom/miui/server/security/AccessControlImpl;->mService:Lcom/miui/server/SecurityManagerService;

    iget-object v0, v0, Lcom/miui/server/SecurityManagerService;->mUserStateLock:Ljava/lang/Object;

    monitor-enter v0

    .line 226
    :try_start_0
    iget-object v1, p0, Lcom/miui/server/security/AccessControlImpl;->mService:Lcom/miui/server/SecurityManagerService;

    invoke-virtual {v1, p3}, Lcom/miui/server/SecurityManagerService;->getUserStateLocked(I)Lcom/miui/server/security/SecurityUserState;

    move-result-object v1

    .line 227
    .local v1, "userStateLocked":Lcom/miui/server/security/SecurityUserState;
    iget-object v2, p0, Lcom/miui/server/security/AccessControlImpl;->mService:Lcom/miui/server/SecurityManagerService;

    iget-object v3, v1, Lcom/miui/server/security/SecurityUserState;->mPackages:Ljava/util/HashMap;

    invoke-virtual {v2, v3, p1}, Lcom/miui/server/SecurityManagerService;->getPackageSetting(Ljava/util/HashMap;Ljava/lang/String;)Lcom/miui/server/security/SecurityPackageSettings;

    move-result-object v2

    .line 228
    .local v2, "ps":Lcom/miui/server/security/SecurityPackageSettings;
    iput-boolean p2, v2, Lcom/miui/server/security/SecurityPackageSettings;->accessControl:Z

    .line 229
    iget-object v3, p0, Lcom/miui/server/security/AccessControlImpl;->mService:Lcom/miui/server/SecurityManagerService;

    invoke-virtual {v3}, Lcom/miui/server/SecurityManagerService;->scheduleWriteSettings()V

    .line 230
    invoke-virtual {p0}, Lcom/miui/server/security/AccessControlImpl;->updateMaskObserverValues()V

    .line 231
    .end local v1    # "userStateLocked":Lcom/miui/server/security/SecurityUserState;
    .end local v2    # "ps":Lcom/miui/server/security/SecurityPackageSettings;
    monitor-exit v0

    .line 232
    return-void

    .line 231
    :catchall_0
    move-exception v1

    monitor-exit v0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v1
.end method

.method public setApplicationMaskNotificationEnabledForUser(Ljava/lang/String;ZI)V
    .locals 4
    .param p1, "packageName"    # Ljava/lang/String;
    .param p2, "enabled"    # Z
    .param p3, "userId"    # I

    .line 307
    iget-object v0, p0, Lcom/miui/server/security/AccessControlImpl;->mService:Lcom/miui/server/SecurityManagerService;

    iget-object v0, v0, Lcom/miui/server/SecurityManagerService;->mUserStateLock:Ljava/lang/Object;

    monitor-enter v0

    .line 308
    :try_start_0
    iget-object v1, p0, Lcom/miui/server/security/AccessControlImpl;->mService:Lcom/miui/server/SecurityManagerService;

    invoke-virtual {v1, p3}, Lcom/miui/server/SecurityManagerService;->getUserStateLocked(I)Lcom/miui/server/security/SecurityUserState;

    move-result-object v1

    .line 309
    .local v1, "userStateLocked":Lcom/miui/server/security/SecurityUserState;
    iget-object v2, p0, Lcom/miui/server/security/AccessControlImpl;->mService:Lcom/miui/server/SecurityManagerService;

    iget-object v3, v1, Lcom/miui/server/security/SecurityUserState;->mPackages:Ljava/util/HashMap;

    invoke-virtual {v2, v3, p1}, Lcom/miui/server/SecurityManagerService;->getPackageSetting(Ljava/util/HashMap;Ljava/lang/String;)Lcom/miui/server/security/SecurityPackageSettings;

    move-result-object v2

    .line 310
    .local v2, "ps":Lcom/miui/server/security/SecurityPackageSettings;
    iput-boolean p2, v2, Lcom/miui/server/security/SecurityPackageSettings;->maskNotification:Z

    .line 311
    iget-object v3, p0, Lcom/miui/server/security/AccessControlImpl;->mService:Lcom/miui/server/SecurityManagerService;

    invoke-virtual {v3}, Lcom/miui/server/SecurityManagerService;->scheduleWriteSettings()V

    .line 312
    invoke-virtual {p0}, Lcom/miui/server/security/AccessControlImpl;->updateMaskObserverValues()V

    .line 313
    .end local v1    # "userStateLocked":Lcom/miui/server/security/SecurityUserState;
    .end local v2    # "ps":Lcom/miui/server/security/SecurityPackageSettings;
    monitor-exit v0

    .line 314
    return-void

    .line 313
    :catchall_0
    move-exception v1

    monitor-exit v0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v1
.end method

.method public updateMaskObserverValues()V
    .locals 6

    .line 283
    const-string v0, "applock_mask_notify"

    invoke-static {}, Landroid/os/Binder;->clearCallingIdentity()J

    move-result-wide v1

    .line 285
    .local v1, "origId":J
    :try_start_0
    iget-object v3, p0, Lcom/miui/server/security/AccessControlImpl;->mContext:Landroid/content/Context;

    invoke-virtual {v3}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v3

    const/4 v4, 0x0

    invoke-static {v3, v0, v4}, Landroid/provider/Settings$Secure;->getInt(Landroid/content/ContentResolver;Ljava/lang/String;I)I

    move-result v3

    .line 287
    .local v3, "oldValue":I
    iget-object v4, p0, Lcom/miui/server/security/AccessControlImpl;->mContext:Landroid/content/Context;

    invoke-virtual {v4}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v4

    xor-int/lit8 v5, v3, 0x1

    invoke-static {v4, v0, v5}, Landroid/provider/Settings$Secure;->putInt(Landroid/content/ContentResolver;Ljava/lang/String;I)Z
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 290
    nop

    .end local v3    # "oldValue":I
    goto :goto_0

    .line 288
    :catch_0
    move-exception v0

    .line 289
    .local v0, "e":Ljava/lang/Exception;
    const-string v3, "AccessControlImpl"

    const-string/jumbo v4, "write setting secure failed."

    invoke-static {v3, v4, v0}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    .line 291
    .end local v0    # "e":Ljava/lang/Exception;
    :goto_0
    invoke-static {v1, v2}, Landroid/os/Binder;->restoreCallingIdentity(J)V

    .line 292
    return-void
.end method
