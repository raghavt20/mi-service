.class public Lcom/miui/server/security/SecurityUserState;
.super Ljava/lang/Object;
.source "SecurityUserState.java"


# instance fields
.field public gameBoosterServiceDeath:Lcom/miui/server/security/GameBoosterImpl$GameBoosterServiceDeath;

.field public final mAccessControlCanceled:Landroid/util/ArraySet;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/util/ArraySet<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field public mAccessControlEnabled:Z

.field public final mAccessControlLastCheck:Landroid/util/ArrayMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/util/ArrayMap<",
            "Ljava/lang/String;",
            "Ljava/lang/Long;",
            ">;"
        }
    .end annotation
.end field

.field public mAccessControlLockConvenient:Z

.field public mAccessControlLockMode:I

.field public final mAccessControlPassPackages:Ljava/util/HashSet;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/HashSet<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field public mAccessControlSettingInit:Z

.field public mIsGameMode:Z

.field public mLastResumePackage:Ljava/lang/String;

.field public final mPackages:Ljava/util/HashMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/HashMap<",
            "Ljava/lang/String;",
            "Lcom/miui/server/security/SecurityPackageSettings;",
            ">;"
        }
    .end annotation
.end field

.field public userHandle:I


# direct methods
.method public constructor <init>()V
    .locals 1

    .line 11
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 14
    new-instance v0, Ljava/util/HashSet;

    invoke-direct {v0}, Ljava/util/HashSet;-><init>()V

    iput-object v0, p0, Lcom/miui/server/security/SecurityUserState;->mAccessControlPassPackages:Ljava/util/HashSet;

    .line 15
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    iput-object v0, p0, Lcom/miui/server/security/SecurityUserState;->mPackages:Ljava/util/HashMap;

    .line 16
    new-instance v0, Landroid/util/ArraySet;

    invoke-direct {v0}, Landroid/util/ArraySet;-><init>()V

    iput-object v0, p0, Lcom/miui/server/security/SecurityUserState;->mAccessControlCanceled:Landroid/util/ArraySet;

    .line 17
    new-instance v0, Landroid/util/ArrayMap;

    invoke-direct {v0}, Landroid/util/ArrayMap;-><init>()V

    iput-object v0, p0, Lcom/miui/server/security/SecurityUserState;->mAccessControlLastCheck:Landroid/util/ArrayMap;

    .line 35
    const/4 v0, 0x0

    iput v0, p0, Lcom/miui/server/security/SecurityUserState;->mAccessControlLockMode:I

    return-void
.end method
