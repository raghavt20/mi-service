public class com.miui.server.security.SecuritySettingsObserver extends android.database.ContentObserver {
	 /* .source "SecuritySettingsObserver.java" */
	 /* # instance fields */
	 private final android.net.Uri mAccessControlLockConvenientUri;
	 private final android.net.Uri mAccessControlLockEnabledUri;
	 private final android.net.Uri mAccessControlLockModeUri;
	 private final android.net.Uri mAccessMiuiOptimizationUri;
	 private final android.content.Context mContext;
	 private final com.miui.server.SecurityManagerService mService;
	 /* # direct methods */
	 public com.miui.server.security.SecuritySettingsObserver ( ) {
		 /* .locals 1 */
		 /* .param p1, "service" # Lcom/miui/server/SecurityManagerService; */
		 /* .param p2, "handler" # Landroid/os/Handler; */
		 /* .line 33 */
		 /* invoke-direct {p0, p2}, Landroid/database/ContentObserver;-><init>(Landroid/os/Handler;)V */
		 /* .line 27 */
		 final String v0 = "access_control_lock_enabled"; // const-string v0, "access_control_lock_enabled"
		 android.provider.Settings$Secure .getUriFor ( v0 );
		 this.mAccessControlLockEnabledUri = v0;
		 /* .line 28 */
		 final String v0 = "access_control_lock_mode"; // const-string v0, "access_control_lock_mode"
		 android.provider.Settings$Secure .getUriFor ( v0 );
		 this.mAccessControlLockModeUri = v0;
		 /* .line 29 */
		 final String v0 = "access_control_lock_convenient"; // const-string v0, "access_control_lock_convenient"
		 android.provider.Settings$Secure .getUriFor ( v0 );
		 this.mAccessControlLockConvenientUri = v0;
		 /* .line 30 */
		 final String v0 = "miui_optimization"; // const-string v0, "miui_optimization"
		 android.provider.Settings$Secure .getUriFor ( v0 );
		 this.mAccessMiuiOptimizationUri = v0;
		 /* .line 34 */
		 this.mService = p1;
		 /* .line 35 */
		 v0 = this.mContext;
		 this.mContext = v0;
		 /* .line 36 */
		 return;
	 } // .end method
	 private void updateAccessControlEnabledLocked ( com.miui.server.security.SecurityUserState p0 ) {
		 /* .locals 4 */
		 /* .param p1, "userState" # Lcom/miui/server/security/SecurityUserState; */
		 /* .line 66 */
		 v0 = this.mContext;
		 (( android.content.Context ) v0 ).getContentResolver ( ); // invoke-virtual {v0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;
		 /* iget v1, p1, Lcom/miui/server/security/SecurityUserState;->userHandle:I */
		 final String v2 = "access_control_lock_enabled"; // const-string v2, "access_control_lock_enabled"
		 int v3 = 0; // const/4 v3, 0x0
		 v0 = 		 android.provider.Settings$Secure .getIntForUser ( v0,v2,v3,v1 );
		 int v1 = 1; // const/4 v1, 0x1
		 /* if-ne v0, v1, :cond_0 */
		 /* move v3, v1 */
	 } // :cond_0
	 /* iput-boolean v3, p1, Lcom/miui/server/security/SecurityUserState;->mAccessControlEnabled:Z */
	 /* .line 68 */
	 return;
} // .end method
private void updateAccessControlLockConvenientLocked ( com.miui.server.security.SecurityUserState p0 ) {
	 /* .locals 4 */
	 /* .param p1, "userState" # Lcom/miui/server/security/SecurityUserState; */
	 /* .line 76 */
	 v0 = this.mContext;
	 (( android.content.Context ) v0 ).getContentResolver ( ); // invoke-virtual {v0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;
	 /* iget v1, p1, Lcom/miui/server/security/SecurityUserState;->userHandle:I */
	 final String v2 = "access_control_lock_convenient"; // const-string v2, "access_control_lock_convenient"
	 int v3 = 0; // const/4 v3, 0x0
	 v0 = 	 android.provider.Settings$Secure .getIntForUser ( v0,v2,v3,v1 );
	 int v1 = 1; // const/4 v1, 0x1
	 /* if-ne v0, v1, :cond_0 */
	 /* move v3, v1 */
} // :cond_0
/* iput-boolean v3, p1, Lcom/miui/server/security/SecurityUserState;->mAccessControlLockConvenient:Z */
/* .line 78 */
return;
} // .end method
private void updateAccessControlLockModeLocked ( com.miui.server.security.SecurityUserState p0 ) {
/* .locals 4 */
/* .param p1, "userState" # Lcom/miui/server/security/SecurityUserState; */
/* .line 71 */
v0 = this.mContext;
(( android.content.Context ) v0 ).getContentResolver ( ); // invoke-virtual {v0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;
int v1 = 1; // const/4 v1, 0x1
/* iget v2, p1, Lcom/miui/server/security/SecurityUserState;->userHandle:I */
final String v3 = "access_control_lock_mode"; // const-string v3, "access_control_lock_mode"
v0 = android.provider.Settings$Secure .getIntForUser ( v0,v3,v1,v2 );
/* iput v0, p1, Lcom/miui/server/security/SecurityUserState;->mAccessControlLockMode:I */
/* .line 73 */
return;
} // .end method
private void updateAccessMiuiOptUri ( ) {
/* .locals 3 */
/* .line 92 */
v0 = android.miui.AppOpsUtils .isXOptMode ( );
if ( v0 != null) { // if-eqz v0, :cond_0
	 /* .line 95 */
	 try { // :try_start_0
		 v0 = this.mContext;
		 (( android.content.Context ) v0 ).getPackageManager ( ); // invoke-virtual {v0}, Landroid/content/Context;->getPackageManager()Landroid/content/pm/PackageManager;
		 /* .line 96 */
		 /* .local v0, "pm":Landroid/content/pm/PackageManager; */
		 final String v1 = ""; // const-string v1, ""
		 int v2 = 0; // const/4 v2, 0x0
		 (( android.content.pm.PackageManager ) v0 ).setDefaultBrowserPackageNameAsUser ( v1, v2 ); // invoke-virtual {v0, v1, v2}, Landroid/content/pm/PackageManager;->setDefaultBrowserPackageNameAsUser(Ljava/lang/String;I)Z
		 /* :try_end_0 */
		 /* .catch Ljava/lang/Exception; {:try_start_0 ..:try_end_0} :catch_0 */
		 /* .line 99 */
		 /* nop */
	 } // .end local v0 # "pm":Landroid/content/pm/PackageManager;
	 /* .line 97 */
	 /* :catch_0 */
	 /* move-exception v0 */
	 /* .line 98 */
	 /* .local v0, "e":Ljava/lang/Exception; */
	 (( java.lang.Exception ) v0 ).printStackTrace ( ); // invoke-virtual {v0}, Ljava/lang/Exception;->printStackTrace()V
	 /* .line 101 */
} // .end local v0 # "e":Ljava/lang/Exception;
} // :cond_0
} // :goto_0
com.android.server.pm.PackageManagerServiceStub .get ( );
(( com.android.server.pm.PackageManagerServiceStub ) v0 ).switchPackageInstaller ( ); // invoke-virtual {v0}, Lcom/android/server/pm/PackageManagerServiceStub;->switchPackageInstaller()V
/* .line 103 */
/* sget-boolean v0, Lmiui/os/Build;->IS_INTERNATIONAL_BUILD:Z */
if ( v0 != null) { // if-eqz v0, :cond_1
/* .line 104 */
com.android.server.pm.permission.DefaultPermissionGrantPolicyStub .get ( );
(( com.android.server.pm.permission.DefaultPermissionGrantPolicyStub ) v0 ).revokeAllPermssions ( ); // invoke-virtual {v0}, Lcom/android/server/pm/permission/DefaultPermissionGrantPolicyStub;->revokeAllPermssions()V
/* .line 106 */
} // :cond_1
v0 = android.miui.AppOpsUtils .isXOptMode ( );
/* if-nez v0, :cond_2 */
/* .line 107 */
com.android.server.pm.permission.DefaultPermissionGrantPolicyStub .get ( );
(( com.android.server.pm.permission.DefaultPermissionGrantPolicyStub ) v0 ).grantMiuiPackageInstallerPermssions ( ); // invoke-virtual {v0}, Lcom/android/server/pm/permission/DefaultPermissionGrantPolicyStub;->grantMiuiPackageInstallerPermssions()V
/* .line 108 */
v0 = this.mContext;
com.miui.server.security.DefaultBrowserImpl .setDefaultBrowser ( v0 );
/* .line 110 */
} // :cond_2
return;
} // .end method
/* # virtual methods */
public void initAccessControlSettingsLocked ( com.miui.server.security.SecurityUserState p0 ) {
/* .locals 1 */
/* .param p1, "userState" # Lcom/miui/server/security/SecurityUserState; */
/* .line 81 */
/* iget-boolean v0, p1, Lcom/miui/server/security/SecurityUserState;->mAccessControlSettingInit:Z */
if ( v0 != null) { // if-eqz v0, :cond_0
/* .line 82 */
return;
/* .line 84 */
} // :cond_0
/* invoke-direct {p0, p1}, Lcom/miui/server/security/SecuritySettingsObserver;->updateAccessControlEnabledLocked(Lcom/miui/server/security/SecurityUserState;)V */
/* .line 85 */
/* invoke-direct {p0, p1}, Lcom/miui/server/security/SecuritySettingsObserver;->updateAccessControlLockModeLocked(Lcom/miui/server/security/SecurityUserState;)V */
/* .line 86 */
/* invoke-direct {p0, p1}, Lcom/miui/server/security/SecuritySettingsObserver;->updateAccessControlLockConvenientLocked(Lcom/miui/server/security/SecurityUserState;)V */
/* .line 87 */
int v0 = 1; // const/4 v0, 0x1
/* iput-boolean v0, p1, Lcom/miui/server/security/SecurityUserState;->mAccessControlSettingInit:Z */
/* .line 88 */
return;
} // .end method
public void onChange ( Boolean p0, android.net.Uri p1, Integer p2 ) {
/* .locals 3 */
/* .param p1, "selfChange" # Z */
/* .param p2, "uri" # Landroid/net/Uri; */
/* .param p3, "userId" # I */
/* .line 48 */
v0 = this.mAccessMiuiOptimizationUri;
v0 = (( android.net.Uri ) v0 ).equals ( p2 ); // invoke-virtual {v0, p2}, Landroid/net/Uri;->equals(Ljava/lang/Object;)Z
if ( v0 != null) { // if-eqz v0, :cond_0
/* .line 49 */
/* invoke-direct {p0}, Lcom/miui/server/security/SecuritySettingsObserver;->updateAccessMiuiOptUri()V */
/* .line 51 */
} // :cond_0
v0 = this.mService;
v0 = this.mUserStateLock;
/* monitor-enter v0 */
/* .line 52 */
try { // :try_start_0
v1 = this.mService;
(( com.miui.server.SecurityManagerService ) v1 ).getUserStateLocked ( p3 ); // invoke-virtual {v1, p3}, Lcom/miui/server/SecurityManagerService;->getUserStateLocked(I)Lcom/miui/server/security/SecurityUserState;
/* .line 53 */
/* .local v1, "userState":Lcom/miui/server/security/SecurityUserState; */
v2 = this.mAccessControlLockEnabledUri;
v2 = (( android.net.Uri ) v2 ).equals ( p2 ); // invoke-virtual {v2, p2}, Landroid/net/Uri;->equals(Ljava/lang/Object;)Z
if ( v2 != null) { // if-eqz v2, :cond_1
/* .line 54 */
/* invoke-direct {p0, v1}, Lcom/miui/server/security/SecuritySettingsObserver;->updateAccessControlEnabledLocked(Lcom/miui/server/security/SecurityUserState;)V */
/* .line 55 */
} // :cond_1
v2 = this.mAccessControlLockModeUri;
v2 = (( android.net.Uri ) v2 ).equals ( p2 ); // invoke-virtual {v2, p2}, Landroid/net/Uri;->equals(Ljava/lang/Object;)Z
if ( v2 != null) { // if-eqz v2, :cond_2
/* .line 56 */
/* invoke-direct {p0, v1}, Lcom/miui/server/security/SecuritySettingsObserver;->updateAccessControlLockModeLocked(Lcom/miui/server/security/SecurityUserState;)V */
/* .line 57 */
} // :cond_2
v2 = this.mAccessControlLockConvenientUri;
v2 = (( android.net.Uri ) v2 ).equals ( p2 ); // invoke-virtual {v2, p2}, Landroid/net/Uri;->equals(Ljava/lang/Object;)Z
if ( v2 != null) { // if-eqz v2, :cond_3
/* .line 58 */
/* invoke-direct {p0, v1}, Lcom/miui/server/security/SecuritySettingsObserver;->updateAccessControlLockConvenientLocked(Lcom/miui/server/security/SecurityUserState;)V */
/* .line 60 */
} // :cond_3
} // :goto_0
v2 = this.mService;
v2 = this.mAccessControlImpl;
(( com.miui.server.security.AccessControlImpl ) v2 ).updateMaskObserverValues ( ); // invoke-virtual {v2}, Lcom/miui/server/security/AccessControlImpl;->updateMaskObserverValues()V
/* .line 61 */
} // .end local v1 # "userState":Lcom/miui/server/security/SecurityUserState;
/* monitor-exit v0 */
/* .line 63 */
} // :goto_1
return;
/* .line 61 */
/* :catchall_0 */
/* move-exception v1 */
/* monitor-exit v0 */
/* :try_end_0 */
/* .catchall {:try_start_0 ..:try_end_0} :catchall_0 */
/* throw v1 */
} // .end method
public void registerForSettingsChanged ( ) {
/* .locals 4 */
/* .line 39 */
v0 = this.mService;
v0 = this.mContext;
(( android.content.Context ) v0 ).getContentResolver ( ); // invoke-virtual {v0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;
/* .line 40 */
/* .local v0, "resolver":Landroid/content/ContentResolver; */
v1 = this.mAccessControlLockEnabledUri;
int v2 = 0; // const/4 v2, 0x0
int v3 = -1; // const/4 v3, -0x1
(( android.content.ContentResolver ) v0 ).registerContentObserver ( v1, v2, p0, v3 ); // invoke-virtual {v0, v1, v2, p0, v3}, Landroid/content/ContentResolver;->registerContentObserver(Landroid/net/Uri;ZLandroid/database/ContentObserver;I)V
/* .line 41 */
v1 = this.mAccessControlLockModeUri;
(( android.content.ContentResolver ) v0 ).registerContentObserver ( v1, v2, p0, v3 ); // invoke-virtual {v0, v1, v2, p0, v3}, Landroid/content/ContentResolver;->registerContentObserver(Landroid/net/Uri;ZLandroid/database/ContentObserver;I)V
/* .line 42 */
v1 = this.mAccessControlLockConvenientUri;
(( android.content.ContentResolver ) v0 ).registerContentObserver ( v1, v2, p0, v3 ); // invoke-virtual {v0, v1, v2, p0, v3}, Landroid/content/ContentResolver;->registerContentObserver(Landroid/net/Uri;ZLandroid/database/ContentObserver;I)V
/* .line 43 */
v1 = this.mAccessMiuiOptimizationUri;
(( android.content.ContentResolver ) v0 ).registerContentObserver ( v1, v2, p0, v3 ); // invoke-virtual {v0, v1, v2, p0, v3}, Landroid/content/ContentResolver;->registerContentObserver(Landroid/net/Uri;ZLandroid/database/ContentObserver;I)V
/* .line 44 */
return;
} // .end method
