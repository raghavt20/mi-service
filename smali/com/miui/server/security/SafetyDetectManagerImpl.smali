.class public Lcom/miui/server/security/SafetyDetectManagerImpl;
.super Lcom/miui/server/security/SafetyDetectManagerStub;
.source "SafetyDetectManagerImpl.java"


# annotations
.annotation runtime Lcom/miui/base/annotations/MiuiStubHead;
    manifestName = "com.miui.server.security.SafetyDetectManagerStub$$"
.end annotation


# static fields
.field private static final TAG:Ljava/lang/String; = "SafetyDetectManagerImpl"

.field private static final TRUE:Ljava/lang/String; = "true"


# instance fields
.field private mDetectAccessibilityTouch:Z

.field private mDetectAdbInput:Z

.field private mLastSimulatedTouchTime:J

.field private mLastSimulatedTouchUid:I


# direct methods
.method public constructor <init>()V
    .locals 1

    .line 18
    invoke-direct {p0}, Lcom/miui/server/security/SafetyDetectManagerStub;-><init>()V

    .line 22
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/miui/server/security/SafetyDetectManagerImpl;->mDetectAccessibilityTouch:Z

    .line 23
    iput-boolean v0, p0, Lcom/miui/server/security/SafetyDetectManagerImpl;->mDetectAdbInput:Z

    return-void
.end method


# virtual methods
.method public getSimulatedTouchInfo()Ljava/util/Map;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/Map<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .line 58
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    .line 59
    .local v0, "simulatedTouchInfo":Ljava/util/Map;, "Ljava/util/Map<Ljava/lang/String;Ljava/lang/String;>;"
    iget-boolean v1, p0, Lcom/miui/server/security/SafetyDetectManagerImpl;->mDetectAccessibilityTouch:Z

    if-nez v1, :cond_0

    iget-boolean v1, p0, Lcom/miui/server/security/SafetyDetectManagerImpl;->mDetectAdbInput:Z

    if-eqz v1, :cond_1

    .line 60
    :cond_0
    iget-wide v1, p0, Lcom/miui/server/security/SafetyDetectManagerImpl;->mLastSimulatedTouchTime:J

    invoke-static {v1, v2}, Ljava/lang/String;->valueOf(J)Ljava/lang/String;

    move-result-object v1

    const-string v2, "lastSimulatedTouchTime"

    invoke-interface {v0, v2, v1}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 61
    iget v1, p0, Lcom/miui/server/security/SafetyDetectManagerImpl;->mLastSimulatedTouchUid:I

    invoke-static {v1}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v1

    const-string v2, "lastSimulatedTouchUid"

    invoke-interface {v0, v2, v1}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 63
    :cond_1
    return-object v0
.end method

.method public stampAccessibilityDispatchGesture(I)V
    .locals 2
    .param p1, "uid"    # I

    .line 40
    iget-boolean v0, p0, Lcom/miui/server/security/SafetyDetectManagerImpl;->mDetectAccessibilityTouch:Z

    if-nez v0, :cond_0

    .line 41
    return-void

    .line 43
    :cond_0
    invoke-static {}, Landroid/os/SystemClock;->uptimeMillis()J

    move-result-wide v0

    iput-wide v0, p0, Lcom/miui/server/security/SafetyDetectManagerImpl;->mLastSimulatedTouchTime:J

    .line 44
    iput p1, p0, Lcom/miui/server/security/SafetyDetectManagerImpl;->mLastSimulatedTouchUid:I

    .line 45
    return-void
.end method

.method public stampPerformAccessibilityAction(I)V
    .locals 2
    .param p1, "uid"    # I

    .line 49
    iget-boolean v0, p0, Lcom/miui/server/security/SafetyDetectManagerImpl;->mDetectAccessibilityTouch:Z

    if-nez v0, :cond_0

    .line 50
    return-void

    .line 52
    :cond_0
    invoke-static {}, Landroid/os/SystemClock;->uptimeMillis()J

    move-result-wide v0

    iput-wide v0, p0, Lcom/miui/server/security/SafetyDetectManagerImpl;->mLastSimulatedTouchTime:J

    .line 53
    iput p1, p0, Lcom/miui/server/security/SafetyDetectManagerImpl;->mLastSimulatedTouchUid:I

    .line 54
    return-void
.end method

.method public stampShellInjectInputEvent(J)V
    .locals 1
    .param p1, "downTime"    # J

    .line 30
    iget-boolean v0, p0, Lcom/miui/server/security/SafetyDetectManagerImpl;->mDetectAdbInput:Z

    if-nez v0, :cond_0

    .line 31
    return-void

    .line 33
    :cond_0
    invoke-static {}, Landroid/os/Binder;->getCallingUid()I

    move-result v0

    .line 34
    .local v0, "callingUid":I
    iput-wide p1, p0, Lcom/miui/server/security/SafetyDetectManagerImpl;->mLastSimulatedTouchTime:J

    .line 35
    iput v0, p0, Lcom/miui/server/security/SafetyDetectManagerImpl;->mLastSimulatedTouchUid:I

    .line 36
    return-void
.end method

.method public switchSimulatedTouchDetect(Ljava/util/Map;)V
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/Map<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;)V"
        }
    .end annotation

    .line 69
    .local p1, "config":Ljava/util/Map;, "Ljava/util/Map<Ljava/lang/String;Ljava/lang/String;>;"
    const-string v0, "SafetyDetectManagerImpl"

    const-string/jumbo v1, "true"

    :try_start_0
    const-string v2, "detectAdpInput"

    invoke-interface {p1, v2, v1}, Ljava/util/Map;->getOrDefault(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/lang/String;

    invoke-static {v2}, Ljava/lang/Boolean;->parseBoolean(Ljava/lang/String;)Z

    move-result v2

    iput-boolean v2, p0, Lcom/miui/server/security/SafetyDetectManagerImpl;->mDetectAdbInput:Z

    .line 70
    const-string v2, "detectAccessibilityTouch"

    invoke-interface {p1, v2, v1}, Ljava/util/Map;->getOrDefault(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/String;

    invoke-static {v1}, Ljava/lang/Boolean;->parseBoolean(Ljava/lang/String;)Z

    move-result v1

    iput-boolean v1, p0, Lcom/miui/server/security/SafetyDetectManagerImpl;->mDetectAccessibilityTouch:Z
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 73
    goto :goto_0

    .line 71
    :catch_0
    move-exception v1

    .line 72
    .local v1, "e":Ljava/lang/Exception;
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v3, "switchSimulatedTouchDetect exception: "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v0, v2}, Landroid/util/Slog;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 74
    .end local v1    # "e":Ljava/lang/Exception;
    :goto_0
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "mDetectAdbInput = "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-boolean v2, p0, Lcom/miui/server/security/SafetyDetectManagerImpl;->mDetectAdbInput:Z

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "\tmDetectAccessibilityTouch = "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-boolean v2, p0, Lcom/miui/server/security/SafetyDetectManagerImpl;->mDetectAccessibilityTouch:Z

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Slog;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 75
    return-void
.end method
