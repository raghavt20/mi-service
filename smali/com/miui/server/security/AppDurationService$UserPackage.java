class com.miui.server.security.AppDurationService$UserPackage {
	 /* .source "AppDurationService.java" */
	 /* # annotations */
	 /* .annotation system Ldalvik/annotation/EnclosingClass; */
	 /* value = Lcom/miui/server/security/AppDurationService; */
} // .end annotation
/* .annotation system Ldalvik/annotation/InnerClass; */
/* accessFlags = 0xa */
/* name = "UserPackage" */
} // .end annotation
/* # instance fields */
private final java.lang.String pkgName;
private final Integer uid;
/* # direct methods */
static java.lang.String -$$Nest$fgetpkgName ( com.miui.server.security.AppDurationService$UserPackage p0 ) { //bridge//synthethic
/* .locals 0 */
p0 = this.pkgName;
} // .end method
public com.miui.server.security.AppDurationService$UserPackage ( ) {
/* .locals 0 */
/* .param p1, "uid" # I */
/* .param p2, "pkgName" # Ljava/lang/String; */
/* .line 131 */
/* invoke-direct {p0}, Ljava/lang/Object;-><init>()V */
/* .line 132 */
/* iput p1, p0, Lcom/miui/server/security/AppDurationService$UserPackage;->uid:I */
/* .line 133 */
this.pkgName = p2;
/* .line 134 */
return;
} // .end method
/* # virtual methods */
public Boolean equals ( java.lang.Object p0 ) {
/* .locals 5 */
/* .param p1, "o" # Ljava/lang/Object; */
/* .line 138 */
int v0 = 1; // const/4 v0, 0x1
/* if-ne p0, p1, :cond_0 */
/* .line 139 */
} // :cond_0
int v1 = 0; // const/4 v1, 0x0
if ( p1 != null) { // if-eqz p1, :cond_3
(( java.lang.Object ) p0 ).getClass ( ); // invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;
(( java.lang.Object ) p1 ).getClass ( ); // invoke-virtual {p1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;
/* if-eq v2, v3, :cond_1 */
/* .line 140 */
} // :cond_1
/* move-object v2, p1 */
/* check-cast v2, Lcom/miui/server/security/AppDurationService$UserPackage; */
/* .line 141 */
/* .local v2, "that":Lcom/miui/server/security/AppDurationService$UserPackage; */
/* iget v3, p0, Lcom/miui/server/security/AppDurationService$UserPackage;->uid:I */
/* iget v4, v2, Lcom/miui/server/security/AppDurationService$UserPackage;->uid:I */
/* if-ne v3, v4, :cond_2 */
v3 = this.pkgName;
v4 = this.pkgName;
v3 = java.util.Objects .equals ( v3,v4 );
if ( v3 != null) { // if-eqz v3, :cond_2
} // :cond_2
/* move v0, v1 */
} // :goto_0
/* .line 139 */
} // .end local v2 # "that":Lcom/miui/server/security/AppDurationService$UserPackage;
} // :cond_3
} // :goto_1
} // .end method
public Integer hashCode ( ) {
/* .locals 2 */
/* .line 146 */
/* iget v0, p0, Lcom/miui/server/security/AppDurationService$UserPackage;->uid:I */
java.lang.Integer .valueOf ( v0 );
v1 = this.pkgName;
/* filled-new-array {v0, v1}, [Ljava/lang/Object; */
v0 = java.util.Objects .hash ( v0 );
} // .end method
