.class public Lcom/miui/server/security/SecuritySettingsObserver;
.super Landroid/database/ContentObserver;
.source "SecuritySettingsObserver.java"


# instance fields
.field private final mAccessControlLockConvenientUri:Landroid/net/Uri;

.field private final mAccessControlLockEnabledUri:Landroid/net/Uri;

.field private final mAccessControlLockModeUri:Landroid/net/Uri;

.field private final mAccessMiuiOptimizationUri:Landroid/net/Uri;

.field private final mContext:Landroid/content/Context;

.field private final mService:Lcom/miui/server/SecurityManagerService;


# direct methods
.method public constructor <init>(Lcom/miui/server/SecurityManagerService;Landroid/os/Handler;)V
    .locals 1
    .param p1, "service"    # Lcom/miui/server/SecurityManagerService;
    .param p2, "handler"    # Landroid/os/Handler;

    .line 33
    invoke-direct {p0, p2}, Landroid/database/ContentObserver;-><init>(Landroid/os/Handler;)V

    .line 27
    const-string v0, "access_control_lock_enabled"

    invoke-static {v0}, Landroid/provider/Settings$Secure;->getUriFor(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v0

    iput-object v0, p0, Lcom/miui/server/security/SecuritySettingsObserver;->mAccessControlLockEnabledUri:Landroid/net/Uri;

    .line 28
    const-string v0, "access_control_lock_mode"

    invoke-static {v0}, Landroid/provider/Settings$Secure;->getUriFor(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v0

    iput-object v0, p0, Lcom/miui/server/security/SecuritySettingsObserver;->mAccessControlLockModeUri:Landroid/net/Uri;

    .line 29
    const-string v0, "access_control_lock_convenient"

    invoke-static {v0}, Landroid/provider/Settings$Secure;->getUriFor(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v0

    iput-object v0, p0, Lcom/miui/server/security/SecuritySettingsObserver;->mAccessControlLockConvenientUri:Landroid/net/Uri;

    .line 30
    const-string v0, "miui_optimization"

    invoke-static {v0}, Landroid/provider/Settings$Secure;->getUriFor(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v0

    iput-object v0, p0, Lcom/miui/server/security/SecuritySettingsObserver;->mAccessMiuiOptimizationUri:Landroid/net/Uri;

    .line 34
    iput-object p1, p0, Lcom/miui/server/security/SecuritySettingsObserver;->mService:Lcom/miui/server/SecurityManagerService;

    .line 35
    iget-object v0, p1, Lcom/miui/server/SecurityManagerService;->mContext:Landroid/content/Context;

    iput-object v0, p0, Lcom/miui/server/security/SecuritySettingsObserver;->mContext:Landroid/content/Context;

    .line 36
    return-void
.end method

.method private updateAccessControlEnabledLocked(Lcom/miui/server/security/SecurityUserState;)V
    .locals 4
    .param p1, "userState"    # Lcom/miui/server/security/SecurityUserState;

    .line 66
    iget-object v0, p0, Lcom/miui/server/security/SecuritySettingsObserver;->mContext:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    iget v1, p1, Lcom/miui/server/security/SecurityUserState;->userHandle:I

    const-string v2, "access_control_lock_enabled"

    const/4 v3, 0x0

    invoke-static {v0, v2, v3, v1}, Landroid/provider/Settings$Secure;->getIntForUser(Landroid/content/ContentResolver;Ljava/lang/String;II)I

    move-result v0

    const/4 v1, 0x1

    if-ne v0, v1, :cond_0

    move v3, v1

    :cond_0
    iput-boolean v3, p1, Lcom/miui/server/security/SecurityUserState;->mAccessControlEnabled:Z

    .line 68
    return-void
.end method

.method private updateAccessControlLockConvenientLocked(Lcom/miui/server/security/SecurityUserState;)V
    .locals 4
    .param p1, "userState"    # Lcom/miui/server/security/SecurityUserState;

    .line 76
    iget-object v0, p0, Lcom/miui/server/security/SecuritySettingsObserver;->mContext:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    iget v1, p1, Lcom/miui/server/security/SecurityUserState;->userHandle:I

    const-string v2, "access_control_lock_convenient"

    const/4 v3, 0x0

    invoke-static {v0, v2, v3, v1}, Landroid/provider/Settings$Secure;->getIntForUser(Landroid/content/ContentResolver;Ljava/lang/String;II)I

    move-result v0

    const/4 v1, 0x1

    if-ne v0, v1, :cond_0

    move v3, v1

    :cond_0
    iput-boolean v3, p1, Lcom/miui/server/security/SecurityUserState;->mAccessControlLockConvenient:Z

    .line 78
    return-void
.end method

.method private updateAccessControlLockModeLocked(Lcom/miui/server/security/SecurityUserState;)V
    .locals 4
    .param p1, "userState"    # Lcom/miui/server/security/SecurityUserState;

    .line 71
    iget-object v0, p0, Lcom/miui/server/security/SecuritySettingsObserver;->mContext:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    const/4 v1, 0x1

    iget v2, p1, Lcom/miui/server/security/SecurityUserState;->userHandle:I

    const-string v3, "access_control_lock_mode"

    invoke-static {v0, v3, v1, v2}, Landroid/provider/Settings$Secure;->getIntForUser(Landroid/content/ContentResolver;Ljava/lang/String;II)I

    move-result v0

    iput v0, p1, Lcom/miui/server/security/SecurityUserState;->mAccessControlLockMode:I

    .line 73
    return-void
.end method

.method private updateAccessMiuiOptUri()V
    .locals 3

    .line 92
    invoke-static {}, Landroid/miui/AppOpsUtils;->isXOptMode()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 95
    :try_start_0
    iget-object v0, p0, Lcom/miui/server/security/SecuritySettingsObserver;->mContext:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v0

    .line 96
    .local v0, "pm":Landroid/content/pm/PackageManager;
    const-string v1, ""

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Landroid/content/pm/PackageManager;->setDefaultBrowserPackageNameAsUser(Ljava/lang/String;I)Z
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 99
    nop

    .end local v0    # "pm":Landroid/content/pm/PackageManager;
    goto :goto_0

    .line 97
    :catch_0
    move-exception v0

    .line 98
    .local v0, "e":Ljava/lang/Exception;
    invoke-virtual {v0}, Ljava/lang/Exception;->printStackTrace()V

    .line 101
    .end local v0    # "e":Ljava/lang/Exception;
    :cond_0
    :goto_0
    invoke-static {}, Lcom/android/server/pm/PackageManagerServiceStub;->get()Lcom/android/server/pm/PackageManagerServiceStub;

    move-result-object v0

    invoke-virtual {v0}, Lcom/android/server/pm/PackageManagerServiceStub;->switchPackageInstaller()V

    .line 103
    sget-boolean v0, Lmiui/os/Build;->IS_INTERNATIONAL_BUILD:Z

    if-eqz v0, :cond_1

    .line 104
    invoke-static {}, Lcom/android/server/pm/permission/DefaultPermissionGrantPolicyStub;->get()Lcom/android/server/pm/permission/DefaultPermissionGrantPolicyStub;

    move-result-object v0

    invoke-virtual {v0}, Lcom/android/server/pm/permission/DefaultPermissionGrantPolicyStub;->revokeAllPermssions()V

    .line 106
    :cond_1
    invoke-static {}, Landroid/miui/AppOpsUtils;->isXOptMode()Z

    move-result v0

    if-nez v0, :cond_2

    .line 107
    invoke-static {}, Lcom/android/server/pm/permission/DefaultPermissionGrantPolicyStub;->get()Lcom/android/server/pm/permission/DefaultPermissionGrantPolicyStub;

    move-result-object v0

    invoke-virtual {v0}, Lcom/android/server/pm/permission/DefaultPermissionGrantPolicyStub;->grantMiuiPackageInstallerPermssions()V

    .line 108
    iget-object v0, p0, Lcom/miui/server/security/SecuritySettingsObserver;->mContext:Landroid/content/Context;

    invoke-static {v0}, Lcom/miui/server/security/DefaultBrowserImpl;->setDefaultBrowser(Landroid/content/Context;)V

    .line 110
    :cond_2
    return-void
.end method


# virtual methods
.method public initAccessControlSettingsLocked(Lcom/miui/server/security/SecurityUserState;)V
    .locals 1
    .param p1, "userState"    # Lcom/miui/server/security/SecurityUserState;

    .line 81
    iget-boolean v0, p1, Lcom/miui/server/security/SecurityUserState;->mAccessControlSettingInit:Z

    if-eqz v0, :cond_0

    .line 82
    return-void

    .line 84
    :cond_0
    invoke-direct {p0, p1}, Lcom/miui/server/security/SecuritySettingsObserver;->updateAccessControlEnabledLocked(Lcom/miui/server/security/SecurityUserState;)V

    .line 85
    invoke-direct {p0, p1}, Lcom/miui/server/security/SecuritySettingsObserver;->updateAccessControlLockModeLocked(Lcom/miui/server/security/SecurityUserState;)V

    .line 86
    invoke-direct {p0, p1}, Lcom/miui/server/security/SecuritySettingsObserver;->updateAccessControlLockConvenientLocked(Lcom/miui/server/security/SecurityUserState;)V

    .line 87
    const/4 v0, 0x1

    iput-boolean v0, p1, Lcom/miui/server/security/SecurityUserState;->mAccessControlSettingInit:Z

    .line 88
    return-void
.end method

.method public onChange(ZLandroid/net/Uri;I)V
    .locals 3
    .param p1, "selfChange"    # Z
    .param p2, "uri"    # Landroid/net/Uri;
    .param p3, "userId"    # I

    .line 48
    iget-object v0, p0, Lcom/miui/server/security/SecuritySettingsObserver;->mAccessMiuiOptimizationUri:Landroid/net/Uri;

    invoke-virtual {v0, p2}, Landroid/net/Uri;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 49
    invoke-direct {p0}, Lcom/miui/server/security/SecuritySettingsObserver;->updateAccessMiuiOptUri()V

    goto :goto_1

    .line 51
    :cond_0
    iget-object v0, p0, Lcom/miui/server/security/SecuritySettingsObserver;->mService:Lcom/miui/server/SecurityManagerService;

    iget-object v0, v0, Lcom/miui/server/SecurityManagerService;->mUserStateLock:Ljava/lang/Object;

    monitor-enter v0

    .line 52
    :try_start_0
    iget-object v1, p0, Lcom/miui/server/security/SecuritySettingsObserver;->mService:Lcom/miui/server/SecurityManagerService;

    invoke-virtual {v1, p3}, Lcom/miui/server/SecurityManagerService;->getUserStateLocked(I)Lcom/miui/server/security/SecurityUserState;

    move-result-object v1

    .line 53
    .local v1, "userState":Lcom/miui/server/security/SecurityUserState;
    iget-object v2, p0, Lcom/miui/server/security/SecuritySettingsObserver;->mAccessControlLockEnabledUri:Landroid/net/Uri;

    invoke-virtual {v2, p2}, Landroid/net/Uri;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_1

    .line 54
    invoke-direct {p0, v1}, Lcom/miui/server/security/SecuritySettingsObserver;->updateAccessControlEnabledLocked(Lcom/miui/server/security/SecurityUserState;)V

    goto :goto_0

    .line 55
    :cond_1
    iget-object v2, p0, Lcom/miui/server/security/SecuritySettingsObserver;->mAccessControlLockModeUri:Landroid/net/Uri;

    invoke-virtual {v2, p2}, Landroid/net/Uri;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_2

    .line 56
    invoke-direct {p0, v1}, Lcom/miui/server/security/SecuritySettingsObserver;->updateAccessControlLockModeLocked(Lcom/miui/server/security/SecurityUserState;)V

    goto :goto_0

    .line 57
    :cond_2
    iget-object v2, p0, Lcom/miui/server/security/SecuritySettingsObserver;->mAccessControlLockConvenientUri:Landroid/net/Uri;

    invoke-virtual {v2, p2}, Landroid/net/Uri;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_3

    .line 58
    invoke-direct {p0, v1}, Lcom/miui/server/security/SecuritySettingsObserver;->updateAccessControlLockConvenientLocked(Lcom/miui/server/security/SecurityUserState;)V

    .line 60
    :cond_3
    :goto_0
    iget-object v2, p0, Lcom/miui/server/security/SecuritySettingsObserver;->mService:Lcom/miui/server/SecurityManagerService;

    iget-object v2, v2, Lcom/miui/server/SecurityManagerService;->mAccessControlImpl:Lcom/miui/server/security/AccessControlImpl;

    invoke-virtual {v2}, Lcom/miui/server/security/AccessControlImpl;->updateMaskObserverValues()V

    .line 61
    .end local v1    # "userState":Lcom/miui/server/security/SecurityUserState;
    monitor-exit v0

    .line 63
    :goto_1
    return-void

    .line 61
    :catchall_0
    move-exception v1

    monitor-exit v0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v1
.end method

.method public registerForSettingsChanged()V
    .locals 4

    .line 39
    iget-object v0, p0, Lcom/miui/server/security/SecuritySettingsObserver;->mService:Lcom/miui/server/SecurityManagerService;

    iget-object v0, v0, Lcom/miui/server/SecurityManagerService;->mContext:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    .line 40
    .local v0, "resolver":Landroid/content/ContentResolver;
    iget-object v1, p0, Lcom/miui/server/security/SecuritySettingsObserver;->mAccessControlLockEnabledUri:Landroid/net/Uri;

    const/4 v2, 0x0

    const/4 v3, -0x1

    invoke-virtual {v0, v1, v2, p0, v3}, Landroid/content/ContentResolver;->registerContentObserver(Landroid/net/Uri;ZLandroid/database/ContentObserver;I)V

    .line 41
    iget-object v1, p0, Lcom/miui/server/security/SecuritySettingsObserver;->mAccessControlLockModeUri:Landroid/net/Uri;

    invoke-virtual {v0, v1, v2, p0, v3}, Landroid/content/ContentResolver;->registerContentObserver(Landroid/net/Uri;ZLandroid/database/ContentObserver;I)V

    .line 42
    iget-object v1, p0, Lcom/miui/server/security/SecuritySettingsObserver;->mAccessControlLockConvenientUri:Landroid/net/Uri;

    invoke-virtual {v0, v1, v2, p0, v3}, Landroid/content/ContentResolver;->registerContentObserver(Landroid/net/Uri;ZLandroid/database/ContentObserver;I)V

    .line 43
    iget-object v1, p0, Lcom/miui/server/security/SecuritySettingsObserver;->mAccessMiuiOptimizationUri:Landroid/net/Uri;

    invoke-virtual {v0, v1, v2, p0, v3}, Landroid/content/ContentResolver;->registerContentObserver(Landroid/net/Uri;ZLandroid/database/ContentObserver;I)V

    .line 44
    return-void
.end method
