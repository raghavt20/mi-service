public class com.miui.server.security.GameBoosterImpl {
	 /* .source "GameBoosterImpl.java" */
	 /* # annotations */
	 /* .annotation system Ldalvik/annotation/MemberClasses; */
	 /* value = { */
	 /* Lcom/miui/server/security/GameBoosterImpl$GameBoosterServiceDeath; */
	 /* } */
} // .end annotation
/* # static fields */
private static final java.lang.String TAG;
/* # instance fields */
private final com.miui.server.SecurityManagerService mService;
/* # direct methods */
static com.miui.server.SecurityManagerService -$$Nest$fgetmService ( com.miui.server.security.GameBoosterImpl p0 ) { //bridge//synthethic
	 /* .locals 0 */
	 p0 = this.mService;
} // .end method
public com.miui.server.security.GameBoosterImpl ( ) {
	 /* .locals 0 */
	 /* .param p1, "service" # Lcom/miui/server/SecurityManagerService; */
	 /* .line 21 */
	 /* invoke-direct {p0}, Ljava/lang/Object;-><init>()V */
	 /* .line 22 */
	 this.mService = p1;
	 /* .line 23 */
	 return;
} // .end method
/* # virtual methods */
public java.util.List getAllGameStorageApps ( Integer p0 ) {
	 /* .locals 10 */
	 /* .param p1, "userId" # I */
	 /* .annotation system Ldalvik/annotation/Signature; */
	 /* value = { */
	 /* "(I)", */
	 /* "Ljava/util/List<", */
	 /* "Ljava/lang/String;", */
	 /* ">;" */
	 /* } */
} // .end annotation
/* .line 84 */
v0 = this.mService;
v0 = this.mUserStateLock;
/* monitor-enter v0 */
/* .line 85 */
try { // :try_start_0
	 /* new-instance v1, Ljava/util/ArrayList; */
	 /* invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V */
	 /* .line 86 */
	 /* .local v1, "storageAppsList":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;" */
	 v2 = this.mService;
	 (( com.miui.server.SecurityManagerService ) v2 ).getUserStateLocked ( p1 ); // invoke-virtual {v2, p1}, Lcom/miui/server/SecurityManagerService;->getUserStateLocked(I)Lcom/miui/server/security/SecurityUserState;
	 /* .line 87 */
	 /* .local v2, "userState":Lcom/miui/server/security/SecurityUserState; */
	 v3 = this.mPackages;
	 /* .line 88 */
	 /* .local v3, "packages":Ljava/util/HashMap;, "Ljava/util/HashMap<Ljava/lang/String;Lcom/miui/server/security/SecurityPackageSettings;>;" */
	 (( java.util.HashMap ) v3 ).keySet ( ); // invoke-virtual {v3}, Ljava/util/HashMap;->keySet()Ljava/util/Set;
	 /* .line 89 */
	 /* .local v4, "pkgNames":Ljava/util/Set;, "Ljava/util/Set<Ljava/lang/String;>;" */
v6 = } // :goto_0
if ( v6 != null) { // if-eqz v6, :cond_1
	 /* check-cast v6, Ljava/lang/String; */
	 /* :try_end_0 */
	 /* .catchall {:try_start_0 ..:try_end_0} :catchall_0 */
	 /* .line 91 */
	 /* .local v6, "pkgName":Ljava/lang/String; */
	 try { // :try_start_1
		 v7 = this.mService;
		 v8 = this.mPackages;
		 (( com.miui.server.SecurityManagerService ) v7 ).getPackageSetting ( v8, v6 ); // invoke-virtual {v7, v8, v6}, Lcom/miui/server/SecurityManagerService;->getPackageSetting(Ljava/util/HashMap;Ljava/lang/String;)Lcom/miui/server/security/SecurityPackageSettings;
		 /* .line 93 */
		 /* .local v7, "ps":Lcom/miui/server/security/SecurityPackageSettings; */
		 /* iget-boolean v8, v7, Lcom/miui/server/security/SecurityPackageSettings;->isGameStorageApp:Z */
		 if ( v8 != null) { // if-eqz v8, :cond_0
			 /* .line 94 */
			 /* :try_end_1 */
			 /* .catch Ljava/lang/Exception; {:try_start_1 ..:try_end_1} :catch_0 */
			 /* .catchall {:try_start_1 ..:try_end_1} :catchall_0 */
			 /* .line 98 */
		 } // .end local v7 # "ps":Lcom/miui/server/security/SecurityPackageSettings;
	 } // :cond_0
	 /* .line 96 */
	 /* :catch_0 */
	 /* move-exception v7 */
	 /* .line 97 */
	 /* .local v7, "e":Ljava/lang/Exception; */
	 try { // :try_start_2
		 final String v8 = "GameBoosterImpl"; // const-string v8, "GameBoosterImpl"
		 final String v9 = "get game storage all apps failed"; // const-string v9, "get game storage all apps failed"
		 android.util.Log .e ( v8,v9,v7 );
		 /* .line 99 */
	 } // .end local v6 # "pkgName":Ljava/lang/String;
} // .end local v7 # "e":Ljava/lang/Exception;
} // :goto_1
/* .line 100 */
} // :cond_1
/* monitor-exit v0 */
/* .line 101 */
} // .end local v1 # "storageAppsList":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;"
} // .end local v2 # "userState":Lcom/miui/server/security/SecurityUserState;
} // .end local v3 # "packages":Ljava/util/HashMap;, "Ljava/util/HashMap<Ljava/lang/String;Lcom/miui/server/security/SecurityPackageSettings;>;"
} // .end local v4 # "pkgNames":Ljava/util/Set;, "Ljava/util/Set<Ljava/lang/String;>;"
/* :catchall_0 */
/* move-exception v1 */
/* monitor-exit v0 */
/* :try_end_2 */
/* .catchall {:try_start_2 ..:try_end_2} :catchall_0 */
/* throw v1 */
} // .end method
public Boolean getGameMode ( Integer p0 ) {
/* .locals 3 */
/* .param p1, "userId" # I */
/* .line 47 */
v0 = this.mService;
v0 = this.mUserStateLock;
/* monitor-enter v0 */
/* .line 48 */
try { // :try_start_0
v1 = miui.security.SecurityManager .getUserHandle ( p1 );
/* move p1, v1 */
/* .line 49 */
v1 = this.mService;
(( com.miui.server.SecurityManagerService ) v1 ).getUserStateLocked ( p1 ); // invoke-virtual {v1, p1}, Lcom/miui/server/SecurityManagerService;->getUserStateLocked(I)Lcom/miui/server/security/SecurityUserState;
/* .line 50 */
/* .local v1, "userState":Lcom/miui/server/security/SecurityUserState; */
/* iget-boolean v2, v1, Lcom/miui/server/security/SecurityUserState;->mIsGameMode:Z */
/* monitor-exit v0 */
/* .line 51 */
} // .end local v1 # "userState":Lcom/miui/server/security/SecurityUserState;
/* :catchall_0 */
/* move-exception v1 */
/* monitor-exit v0 */
/* :try_end_0 */
/* .catchall {:try_start_0 ..:try_end_0} :catchall_0 */
/* throw v1 */
} // .end method
public Boolean isGameStorageApp ( java.lang.String p0, Integer p1 ) {
/* .locals 5 */
/* .param p1, "packageName" # Ljava/lang/String; */
/* .param p2, "userId" # I */
/* .line 71 */
v0 = this.mService;
v0 = this.mUserStateLock;
/* monitor-enter v0 */
/* .line 72 */
try { // :try_start_0
v1 = this.mService;
(( com.miui.server.SecurityManagerService ) v1 ).getUserStateLocked ( p2 ); // invoke-virtual {v1, p2}, Lcom/miui/server/SecurityManagerService;->getUserStateLocked(I)Lcom/miui/server/security/SecurityUserState;
/* :try_end_0 */
/* .catchall {:try_start_0 ..:try_end_0} :catchall_0 */
/* .line 74 */
/* .local v1, "userState":Lcom/miui/server/security/SecurityUserState; */
try { // :try_start_1
v2 = this.mService;
v3 = this.mPackages;
(( com.miui.server.SecurityManagerService ) v2 ).getPackageSetting ( v3, p1 ); // invoke-virtual {v2, v3, p1}, Lcom/miui/server/SecurityManagerService;->getPackageSetting(Ljava/util/HashMap;Ljava/lang/String;)Lcom/miui/server/security/SecurityPackageSettings;
/* .line 75 */
/* .local v2, "ps":Lcom/miui/server/security/SecurityPackageSettings; */
/* iget-boolean v3, v2, Lcom/miui/server/security/SecurityPackageSettings;->isGameStorageApp:Z */
/* :try_end_1 */
/* .catch Ljava/lang/Exception; {:try_start_1 ..:try_end_1} :catch_0 */
/* .catchall {:try_start_1 ..:try_end_1} :catchall_0 */
try { // :try_start_2
/* monitor-exit v0 */
/* .line 76 */
} // .end local v2 # "ps":Lcom/miui/server/security/SecurityPackageSettings;
/* :catch_0 */
/* move-exception v2 */
/* .line 77 */
/* .local v2, "e":Ljava/lang/Exception; */
final String v3 = "GameBoosterImpl"; // const-string v3, "GameBoosterImpl"
final String v4 = "get app is game storage failed"; // const-string v4, "get app is game storage failed"
android.util.Log .e ( v3,v4,v2 );
/* .line 78 */
/* monitor-exit v0 */
int v0 = 0; // const/4 v0, 0x0
/* .line 80 */
} // .end local v1 # "userState":Lcom/miui/server/security/SecurityUserState;
} // .end local v2 # "e":Ljava/lang/Exception;
/* :catchall_0 */
/* move-exception v1 */
/* monitor-exit v0 */
/* :try_end_2 */
/* .catchall {:try_start_2 ..:try_end_2} :catchall_0 */
/* throw v1 */
} // .end method
public Boolean isVtbMode ( android.content.Context p0 ) {
/* .locals 5 */
/* .param p1, "context" # Landroid/content/Context; */
/* .line 55 */
v0 = this.mService;
v0 = this.mUserStateLock;
/* monitor-enter v0 */
/* .line 56 */
try { // :try_start_0
(( android.content.Context ) p1 ).getContentResolver ( ); // invoke-virtual {p1}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;
/* const-string/jumbo v2, "vtb_boosting" */
int v3 = -2; // const/4 v3, -0x2
int v4 = 0; // const/4 v4, 0x0
v1 = android.provider.Settings$Secure .getIntForUser ( v1,v2,v4,v3 );
int v2 = 1; // const/4 v2, 0x1
/* if-ne v1, v2, :cond_0 */
/* move v4, v2 */
} // :cond_0
/* monitor-exit v0 */
/* .line 58 */
/* :catchall_0 */
/* move-exception v1 */
/* monitor-exit v0 */
/* :try_end_0 */
/* .catchall {:try_start_0 ..:try_end_0} :catchall_0 */
/* throw v1 */
} // .end method
public void setGameBoosterIBinder ( android.os.IBinder p0, Integer p1, Boolean p2 ) {
/* .locals 5 */
/* .param p1, "gameBooster" # Landroid/os/IBinder; */
/* .param p2, "userId" # I */
/* .param p3, "isGameMode" # Z */
/* .line 26 */
v0 = this.mService;
v0 = this.mUserStateLock;
/* monitor-enter v0 */
/* .line 27 */
try { // :try_start_0
v1 = miui.security.SecurityManager .getUserHandle ( p2 );
/* move p2, v1 */
/* .line 28 */
v1 = this.mService;
(( com.miui.server.SecurityManagerService ) v1 ).getUserStateLocked ( p2 ); // invoke-virtual {v1, p2}, Lcom/miui/server/SecurityManagerService;->getUserStateLocked(I)Lcom/miui/server/security/SecurityUserState;
/* :try_end_0 */
/* .catchall {:try_start_0 ..:try_end_0} :catchall_0 */
/* .line 30 */
/* .local v1, "userState":Lcom/miui/server/security/SecurityUserState; */
try { // :try_start_1
v2 = this.gameBoosterServiceDeath;
int v3 = 0; // const/4 v3, 0x0
/* if-nez v2, :cond_0 */
/* .line 31 */
/* new-instance v2, Lcom/miui/server/security/GameBoosterImpl$GameBoosterServiceDeath; */
/* invoke-direct {v2, p0, v1, p1}, Lcom/miui/server/security/GameBoosterImpl$GameBoosterServiceDeath;-><init>(Lcom/miui/server/security/GameBoosterImpl;Lcom/miui/server/security/SecurityUserState;Landroid/os/IBinder;)V */
this.gameBoosterServiceDeath = v2;
/* .line 32 */
v2 = this.gameBoosterServiceDeath;
/* .line 33 */
} // :cond_0
v2 = this.gameBoosterServiceDeath;
v2 = this.mGameBoosterService;
/* if-eq p1, v2, :cond_1 */
/* .line 34 */
v2 = this.gameBoosterServiceDeath;
v2 = this.mGameBoosterService;
v4 = this.gameBoosterServiceDeath;
/* .line 36 */
/* new-instance v2, Lcom/miui/server/security/GameBoosterImpl$GameBoosterServiceDeath; */
/* invoke-direct {v2, p0, v1, p1}, Lcom/miui/server/security/GameBoosterImpl$GameBoosterServiceDeath;-><init>(Lcom/miui/server/security/GameBoosterImpl;Lcom/miui/server/security/SecurityUserState;Landroid/os/IBinder;)V */
this.gameBoosterServiceDeath = v2;
/* .line 37 */
v2 = this.gameBoosterServiceDeath;
/* .line 39 */
} // :cond_1
} // :goto_0
/* iput-boolean p3, v1, Lcom/miui/server/security/SecurityUserState;->mIsGameMode:Z */
/* :try_end_1 */
/* .catch Ljava/lang/Exception; {:try_start_1 ..:try_end_1} :catch_0 */
/* .catchall {:try_start_1 ..:try_end_1} :catchall_0 */
/* .line 42 */
/* .line 40 */
/* :catch_0 */
/* move-exception v2 */
/* .line 41 */
/* .local v2, "e":Ljava/lang/Exception; */
try { // :try_start_2
final String v3 = "GameBoosterImpl"; // const-string v3, "GameBoosterImpl"
/* const-string/jumbo v4, "setGameBoosterIBinder" */
android.util.Log .e ( v3,v4,v2 );
/* .line 43 */
} // .end local v1 # "userState":Lcom/miui/server/security/SecurityUserState;
} // .end local v2 # "e":Ljava/lang/Exception;
} // :goto_1
/* monitor-exit v0 */
/* .line 44 */
return;
/* .line 43 */
/* :catchall_0 */
/* move-exception v1 */
/* monitor-exit v0 */
/* :try_end_2 */
/* .catchall {:try_start_2 ..:try_end_2} :catchall_0 */
/* throw v1 */
} // .end method
public void setGameStorageApp ( java.lang.String p0, Integer p1, Boolean p2 ) {
/* .locals 4 */
/* .param p1, "packageName" # Ljava/lang/String; */
/* .param p2, "userId" # I */
/* .param p3, "isStorage" # Z */
/* .line 62 */
v0 = this.mService;
v0 = this.mUserStateLock;
/* monitor-enter v0 */
/* .line 63 */
try { // :try_start_0
v1 = this.mService;
(( com.miui.server.SecurityManagerService ) v1 ).getUserStateLocked ( p2 ); // invoke-virtual {v1, p2}, Lcom/miui/server/SecurityManagerService;->getUserStateLocked(I)Lcom/miui/server/security/SecurityUserState;
/* .line 64 */
/* .local v1, "userStateLocked":Lcom/miui/server/security/SecurityUserState; */
v2 = this.mService;
v3 = this.mPackages;
(( com.miui.server.SecurityManagerService ) v2 ).getPackageSetting ( v3, p1 ); // invoke-virtual {v2, v3, p1}, Lcom/miui/server/SecurityManagerService;->getPackageSetting(Ljava/util/HashMap;Ljava/lang/String;)Lcom/miui/server/security/SecurityPackageSettings;
/* .line 65 */
/* .local v2, "ps":Lcom/miui/server/security/SecurityPackageSettings; */
/* iput-boolean p3, v2, Lcom/miui/server/security/SecurityPackageSettings;->isGameStorageApp:Z */
/* .line 66 */
v3 = this.mService;
(( com.miui.server.SecurityManagerService ) v3 ).scheduleWriteSettings ( ); // invoke-virtual {v3}, Lcom/miui/server/SecurityManagerService;->scheduleWriteSettings()V
/* .line 67 */
} // .end local v1 # "userStateLocked":Lcom/miui/server/security/SecurityUserState;
} // .end local v2 # "ps":Lcom/miui/server/security/SecurityPackageSettings;
/* monitor-exit v0 */
/* .line 68 */
return;
/* .line 67 */
/* :catchall_0 */
/* move-exception v1 */
/* monitor-exit v0 */
/* :try_end_0 */
/* .catchall {:try_start_0 ..:try_end_0} :catchall_0 */
/* throw v1 */
} // .end method
