.class public Lcom/miui/server/security/SecurityWriteHandler;
.super Landroid/os/Handler;
.source "SecurityWriteHandler.java"


# static fields
.field public static final REMOVE_AC_PACKAGE:I = 0x4

.field private static final TAG:Ljava/lang/String; = "SecurityWriteHandler"

.field public static final WRITE_BOOT_TIME:I = 0x3

.field public static final WRITE_SETTINGS:I = 0x1

.field public static final WRITE_WAKE_UP_TIME:I = 0x2


# instance fields
.field private final mContext:Landroid/content/Context;

.field private final mService:Lcom/miui/server/SecurityManagerService;


# direct methods
.method public constructor <init>(Lcom/miui/server/SecurityManagerService;Landroid/os/Looper;)V
    .locals 1
    .param p1, "service"    # Lcom/miui/server/SecurityManagerService;
    .param p2, "looper"    # Landroid/os/Looper;

    .line 26
    invoke-direct {p0, p2}, Landroid/os/Handler;-><init>(Landroid/os/Looper;)V

    .line 27
    iput-object p1, p0, Lcom/miui/server/security/SecurityWriteHandler;->mService:Lcom/miui/server/SecurityManagerService;

    .line 28
    iget-object v0, p1, Lcom/miui/server/SecurityManagerService;->mContext:Landroid/content/Context;

    iput-object v0, p0, Lcom/miui/server/security/SecurityWriteHandler;->mContext:Landroid/content/Context;

    .line 29
    return-void
.end method


# virtual methods
.method public handleMessage(Landroid/os/Message;)V
    .locals 7
    .param p1, "msg"    # Landroid/os/Message;

    .line 32
    iget v0, p1, Landroid/os/Message;->what:I

    const/16 v1, 0xa

    const/4 v2, 0x0

    packed-switch v0, :pswitch_data_0

    goto/16 :goto_1

    .line 61
    :pswitch_0
    iget-object v0, p0, Lcom/miui/server/security/SecurityWriteHandler;->mService:Lcom/miui/server/SecurityManagerService;

    iget-object v0, v0, Lcom/miui/server/SecurityManagerService;->mUserStateLock:Ljava/lang/Object;

    monitor-enter v0

    .line 62
    :try_start_0
    iget v1, p1, Landroid/os/Message;->arg1:I

    .line 63
    .local v1, "userId":I
    iget-object v2, p1, Landroid/os/Message;->obj:Ljava/lang/Object;

    check-cast v2, Ljava/lang/String;

    .line 64
    .local v2, "packageName":Ljava/lang/String;
    iget-object v3, p0, Lcom/miui/server/security/SecurityWriteHandler;->mService:Lcom/miui/server/SecurityManagerService;

    invoke-virtual {v3, v1}, Lcom/miui/server/SecurityManagerService;->getUserStateLocked(I)Lcom/miui/server/security/SecurityUserState;

    move-result-object v3

    .line 65
    .local v3, "userState":Lcom/miui/server/security/SecurityUserState;
    iget-object v4, v3, Lcom/miui/server/security/SecurityUserState;->mAccessControlCanceled:Landroid/util/ArraySet;

    invoke-virtual {v4, v2}, Landroid/util/ArraySet;->remove(Ljava/lang/Object;)Z

    .line 66
    nop

    .end local v1    # "userId":I
    .end local v2    # "packageName":Ljava/lang/String;
    .end local v3    # "userState":Lcom/miui/server/security/SecurityUserState;
    monitor-exit v0

    goto/16 :goto_1

    :catchall_0
    move-exception v1

    monitor-exit v0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v1

    .line 48
    :pswitch_1
    invoke-static {v2}, Landroid/os/Process;->setThreadPriority(I)V

    .line 49
    const/4 v0, 0x3

    invoke-virtual {p0, v0}, Lcom/miui/server/security/SecurityWriteHandler;->removeMessages(I)V

    .line 50
    const-string/jumbo v2, "vendor"

    invoke-static {v2, v0}, Lmiui/util/FeatureParser;->hasFeature(Ljava/lang/String;I)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 51
    const-string/jumbo v0, "vendor"

    invoke-static {v0}, Lmiui/util/FeatureParser;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 52
    .local v0, "vendor":Ljava/lang/String;
    iget-object v2, p1, Landroid/os/Message;->obj:Ljava/lang/Object;

    check-cast v2, Ljava/lang/Long;

    invoke-virtual {v2}, Ljava/lang/Long;->longValue()J

    move-result-wide v2

    .line 53
    .local v2, "wakeTime":J
    iget-object v4, p0, Lcom/miui/server/security/SecurityWriteHandler;->mContext:Landroid/content/Context;

    invoke-static {v4, v0, v2, v3}, Lmiui/security/SecurityManagerCompat;->writeBootTime(Landroid/content/Context;Ljava/lang/String;J)V

    .line 54
    const-string v4, "SecurityWriteHandler"

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "Wake up time updated "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, v2, v3}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 55
    .end local v0    # "vendor":Ljava/lang/String;
    .end local v2    # "wakeTime":J
    goto :goto_0

    .line 56
    :cond_0
    const-string v0, "SecurityWriteHandler"

    const-string v2, "There is no corresponding feature!"

    invoke-static {v0, v2}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    .line 58
    :goto_0
    invoke-static {v1}, Landroid/os/Process;->setThreadPriority(I)V

    .line 59
    goto :goto_1

    .line 42
    :pswitch_2
    invoke-static {v2}, Landroid/os/Process;->setThreadPriority(I)V

    .line 43
    const/4 v0, 0x2

    invoke-virtual {p0, v0}, Lcom/miui/server/security/SecurityWriteHandler;->removeMessages(I)V

    .line 44
    iget-object v0, p0, Lcom/miui/server/security/SecurityWriteHandler;->mService:Lcom/miui/server/SecurityManagerService;

    iget-object v0, v0, Lcom/miui/server/SecurityManagerService;->mWakeUpTimeImpl:Lcom/miui/server/security/WakeUpTimeImpl;

    invoke-virtual {v0}, Lcom/miui/server/security/WakeUpTimeImpl;->writeWakeUpTime()V

    .line 45
    invoke-static {v1}, Landroid/os/Process;->setThreadPriority(I)V

    .line 46
    goto :goto_1

    .line 34
    :pswitch_3
    invoke-static {v2}, Landroid/os/Process;->setThreadPriority(I)V

    .line 35
    iget-object v0, p0, Lcom/miui/server/security/SecurityWriteHandler;->mService:Lcom/miui/server/SecurityManagerService;

    iget-object v0, v0, Lcom/miui/server/SecurityManagerService;->mSettingsFile:Landroid/util/AtomicFile;

    monitor-enter v0

    .line 36
    const/4 v2, 0x1

    :try_start_1
    invoke-virtual {p0, v2}, Lcom/miui/server/security/SecurityWriteHandler;->removeMessages(I)V

    .line 37
    iget-object v2, p0, Lcom/miui/server/security/SecurityWriteHandler;->mService:Lcom/miui/server/SecurityManagerService;

    invoke-virtual {v2}, Lcom/miui/server/SecurityManagerService;->writeSettings()V

    .line 38
    monitor-exit v0
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_1

    .line 39
    invoke-static {v1}, Landroid/os/Process;->setThreadPriority(I)V

    .line 40
    goto :goto_1

    .line 38
    :catchall_1
    move-exception v1

    :try_start_2
    monitor-exit v0
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    throw v1

    .line 69
    :goto_1
    return-void

    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_3
        :pswitch_2
        :pswitch_1
        :pswitch_0
    .end packed-switch
.end method
