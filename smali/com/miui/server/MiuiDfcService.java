public class com.miui.server.MiuiDfcService extends miui.dfc.IDfcService$Stub {
	 /* .source "MiuiDfcService.java" */
	 /* # annotations */
	 /* .annotation system Ldalvik/annotation/MemberClasses; */
	 /* value = { */
	 /* Lcom/miui/server/MiuiDfcService$UsbStatusReceiver;, */
	 /* Lcom/miui/server/MiuiDfcService$Lifecycle; */
	 /* } */
} // .end annotation
/* # static fields */
public static final java.lang.String ACTION_TEST_USB_STATE;
public static final java.lang.String BUILD_TYPE;
public static final Boolean DFC_DEBUG;
private static final java.lang.String DFC_NATIVE_SERVICE;
public static final java.lang.String IS_FRONT_SCENE;
public static final Boolean IS_USERDEBUG;
public static final java.lang.String SERVICE_NAME;
public static final java.lang.String START_DFC_PROP;
public static final java.lang.String STOP_COMPRESS_PROP;
public static final java.lang.String STOP_SCAN_PROP;
private static final java.lang.String TAG;
private static volatile com.miui.server.MiuiDfcService sInstance;
/* # instance fields */
private android.content.Context mContext;
android.os.IBinder$DeathRecipient mDeathHandler;
private volatile miui.dfc.IDfc mService;
private Boolean mUsbConnect;
/* # direct methods */
static void -$$Nest$fputmService ( com.miui.server.MiuiDfcService p0, miui.dfc.IDfc p1 ) { //bridge//synthethic
	 /* .locals 0 */
	 this.mService = p1;
	 return;
} // .end method
static void -$$Nest$fputmUsbConnect ( com.miui.server.MiuiDfcService p0, Boolean p1 ) { //bridge//synthethic
	 /* .locals 0 */
	 /* iput-boolean p1, p0, Lcom/miui/server/MiuiDfcService;->mUsbConnect:Z */
	 return;
} // .end method
static com.miui.server.MiuiDfcService ( ) {
	 /* .locals 2 */
	 /* .line 29 */
	 /* nop */
	 /* .line 30 */
	 final String v0 = "persist.sys.miui_dfc_debug"; // const-string v0, "persist.sys.miui_dfc_debug"
	 int v1 = 0; // const/4 v1, 0x0
	 v0 = 	 android.os.SystemProperties .getBoolean ( v0,v1 );
	 com.miui.server.MiuiDfcService.DFC_DEBUG = (v0!= 0);
	 /* .line 37 */
	 final String v0 = "ro.build.type"; // const-string v0, "ro.build.type"
	 final String v1 = ""; // const-string v1, ""
	 android.os.SystemProperties .get ( v0,v1 );
	 /* .line 38 */
	 /* const-string/jumbo v1, "userdebug" */
	 v0 = 	 (( java.lang.String ) v1 ).equals ( v0 ); // invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z
	 com.miui.server.MiuiDfcService.IS_USERDEBUG = (v0!= 0);
	 return;
} // .end method
private com.miui.server.MiuiDfcService ( ) {
	 /* .locals 2 */
	 /* .line 122 */
	 /* invoke-direct {p0}, Lmiui/dfc/IDfcService$Stub;-><init>()V */
	 /* .line 41 */
	 int v0 = 0; // const/4 v0, 0x0
	 /* iput-boolean v0, p0, Lcom/miui/server/MiuiDfcService;->mUsbConnect:Z */
	 /* .line 114 */
	 /* new-instance v0, Lcom/miui/server/MiuiDfcService$1; */
	 /* invoke-direct {v0, p0}, Lcom/miui/server/MiuiDfcService$1;-><init>(Lcom/miui/server/MiuiDfcService;)V */
	 this.mDeathHandler = v0;
	 /* .line 123 */
	 final String v0 = "MiuiDfcService"; // const-string v0, "MiuiDfcService"
	 final String v1 = "Create MiuiDfcService"; // const-string v1, "Create MiuiDfcService"
	 android.util.Slog .d ( v0,v1 );
	 /* .line 124 */
	 return;
} // .end method
private Boolean checkIsSystemUid ( ) {
	 /* .locals 2 */
	 /* .line 315 */
	 /* const/16 v0, 0x3e8 */
	 v1 = 	 android.os.Binder .getCallingUid ( );
	 /* if-ne v0, v1, :cond_0 */
	 int v0 = 1; // const/4 v0, 0x1
} // :cond_0
int v0 = 0; // const/4 v0, 0x0
} // :goto_0
} // .end method
public static com.miui.server.MiuiDfcService getInstance ( ) {
/* .locals 2 */
/* .line 70 */
v0 = com.miui.server.MiuiDfcService.sInstance;
/* if-nez v0, :cond_1 */
/* .line 71 */
/* const-class v0, Lcom/miui/server/MiuiDfcService; */
/* monitor-enter v0 */
/* .line 72 */
try { // :try_start_0
v1 = com.miui.server.MiuiDfcService.sInstance;
/* if-nez v1, :cond_0 */
/* .line 73 */
/* new-instance v1, Lcom/miui/server/MiuiDfcService; */
/* invoke-direct {v1}, Lcom/miui/server/MiuiDfcService;-><init>()V */
/* .line 75 */
} // :cond_0
/* monitor-exit v0 */
/* :catchall_0 */
/* move-exception v1 */
/* monitor-exit v0 */
/* :try_end_0 */
/* .catchall {:try_start_0 ..:try_end_0} :catchall_0 */
/* throw v1 */
/* .line 77 */
} // :cond_1
} // :goto_0
v0 = com.miui.server.MiuiDfcService.sInstance;
} // .end method
/* # virtual methods */
public void compressDuplicateFiles ( java.lang.String p0, miui.dfc.IDupCompressCallback p1 ) {
/* .locals 8 */
/* .param p1, "data" # Ljava/lang/String; */
/* .param p2, "callback" # Lmiui/dfc/IDupCompressCallback; */
/* .line 206 */
v0 = /* invoke-direct {p0}, Lcom/miui/server/MiuiDfcService;->checkIsSystemUid()Z */
final String v1 = "MiuiDfcService"; // const-string v1, "MiuiDfcService"
/* if-nez v0, :cond_0 */
/* .line 207 */
final String v0 = "calling permission denied"; // const-string v0, "calling permission denied"
android.util.Slog .d ( v1,v0 );
/* .line 208 */
return;
/* .line 210 */
} // :cond_0
final String v0 = "compressDuplicateFiles!"; // const-string v0, "compressDuplicateFiles!"
android.util.Slog .d ( v1,v0 );
/* .line 211 */
final String v0 = ""; // const-string v0, ""
/* .line 213 */
/* .local v0, "result":Ljava/lang/String; */
try { // :try_start_0
(( com.miui.server.MiuiDfcService ) p0 ).getDfcNativeService ( ); // invoke-virtual {p0}, Lcom/miui/server/MiuiDfcService;->getDfcNativeService()Lmiui/dfc/IDfc;
/* .line 214 */
/* .local v2, "service":Lmiui/dfc/IDfc; */
if ( v2 != null) { // if-eqz v2, :cond_3
/* .line 215 */
/* const-string/jumbo v3, "sys.dfcservice.stop_compress" */
final String v4 = "false"; // const-string v4, "false"
android.os.SystemProperties .set ( v3,v4 );
/* .line 216 */
/* move-object v0, v3 */
/* .line 217 */
v3 = (( com.miui.server.MiuiDfcService ) p0 ).isDfcDebug ( ); // invoke-virtual {p0}, Lcom/miui/server/MiuiDfcService;->isDfcDebug()Z
if ( v3 != null) { // if-eqz v3, :cond_1
/* new-instance v3, Ljava/lang/StringBuilder; */
/* invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V */
final String v4 = "Dfc_compressDuplicateFiles, result: "; // const-string v4, "Dfc_compressDuplicateFiles, result: "
(( java.lang.StringBuilder ) v3 ).append ( v4 ); // invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v3 ).append ( v0 ); // invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v3 ).toString ( ); // invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
android.util.Slog .d ( v1,v3 );
/* .line 218 */
} // :cond_1
final String v3 = ","; // const-string v3, ","
(( java.lang.String ) v0 ).split ( v3 ); // invoke-virtual {v0, v3}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;
/* .line 220 */
/* .local v3, "countSizeArray":[Ljava/lang/String; */
if ( p2 != null) { // if-eqz p2, :cond_2
if ( v3 != null) { // if-eqz v3, :cond_2
/* array-length v4, v3 */
int v5 = 2; // const/4 v5, 0x2
/* if-ne v4, v5, :cond_2 */
/* .line 221 */
int v4 = 0; // const/4 v4, 0x0
/* aget-object v4, v3, v4 */
(( java.lang.String ) v4 ).trim ( ); // invoke-virtual {v4}, Ljava/lang/String;->trim()Ljava/lang/String;
java.lang.Long .parseLong ( v4 );
/* move-result-wide v4 */
int v6 = 1; // const/4 v6, 0x1
/* aget-object v6, v3, v6 */
(( java.lang.String ) v6 ).trim ( ); // invoke-virtual {v6}, Ljava/lang/String;->trim()Ljava/lang/String;
java.lang.Long .parseLong ( v6 );
/* move-result-wide v6 */
/* .line 222 */
return;
/* .line 224 */
} // .end local v3 # "countSizeArray":[Ljava/lang/String;
} // :cond_2
/* .line 225 */
} // :cond_3
v3 = (( com.miui.server.MiuiDfcService ) p0 ).isDfcDebug ( ); // invoke-virtual {p0}, Lcom/miui/server/MiuiDfcService;->isDfcDebug()Z
if ( v3 != null) { // if-eqz v3, :cond_4
final String v3 = "compressDuplicateFiles getDfcNativeService is null"; // const-string v3, "compressDuplicateFiles getDfcNativeService is null"
android.util.Slog .d ( v1,v3 );
/* :try_end_0 */
/* .catch Ljava/lang/Exception; {:try_start_0 ..:try_end_0} :catch_0 */
/* .line 229 */
} // .end local v2 # "service":Lmiui/dfc/IDfc;
} // :cond_4
} // :goto_0
/* .line 227 */
/* :catch_0 */
/* move-exception v2 */
/* .line 228 */
/* .local v2, "e":Ljava/lang/Exception; */
/* new-instance v3, Ljava/lang/StringBuilder; */
/* invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V */
final String v4 = "Dfc_compressDuplicateFiles error: "; // const-string v4, "Dfc_compressDuplicateFiles error: "
(( java.lang.StringBuilder ) v3 ).append ( v4 ); // invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.Exception ) v2 ).toString ( ); // invoke-virtual {v2}, Ljava/lang/Exception;->toString()Ljava/lang/String;
(( java.lang.StringBuilder ) v3 ).append ( v4 ); // invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v3 ).toString ( ); // invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
android.util.Slog .e ( v1,v3 );
/* .line 232 */
} // .end local v2 # "e":Ljava/lang/Exception;
} // :goto_1
if ( p2 != null) { // if-eqz p2, :cond_5
/* .line 233 */
/* const-wide/16 v2, 0x0 */
try { // :try_start_1
/* :try_end_1 */
/* .catch Ljava/lang/Exception; {:try_start_1 ..:try_end_1} :catch_1 */
/* .line 235 */
/* :catch_1 */
/* move-exception v2 */
/* .line 236 */
/* .restart local v2 # "e":Ljava/lang/Exception; */
/* new-instance v3, Ljava/lang/StringBuilder; */
/* invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V */
final String v4 = "onFinish error: "; // const-string v4, "onFinish error: "
(( java.lang.StringBuilder ) v3 ).append ( v4 ); // invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.Exception ) v2 ).toString ( ); // invoke-virtual {v2}, Ljava/lang/Exception;->toString()Ljava/lang/String;
(( java.lang.StringBuilder ) v3 ).append ( v4 ); // invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v3 ).toString ( ); // invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
android.util.Slog .e ( v1,v3 );
/* .line 237 */
} // .end local v2 # "e":Ljava/lang/Exception;
} // :cond_5
} // :goto_2
/* nop */
/* .line 238 */
} // :goto_3
return;
} // .end method
public com.miui.server.MiuiDfcService forDfcInitialization ( android.content.Context p0 ) {
/* .locals 3 */
/* .param p1, "context" # Landroid/content/Context; */
/* .line 61 */
this.mContext = p1;
/* .line 62 */
v0 = (( com.miui.server.MiuiDfcService ) p0 ).isDfcDebug ( ); // invoke-virtual {p0}, Lcom/miui/server/MiuiDfcService;->isDfcDebug()Z
if ( v0 != null) { // if-eqz v0, :cond_0
/* new-instance v0, Ljava/lang/StringBuilder; */
/* invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V */
/* const-string/jumbo v1, "use MiuiDfcService(Context context) mContext" */
(( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
v1 = this.mContext;
(( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v0 ).toString ( ); // invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
final String v1 = "MiuiDfcService"; // const-string v1, "MiuiDfcService"
android.util.Slog .d ( v1,v0 );
/* .line 63 */
} // :cond_0
/* new-instance v0, Landroid/content/IntentFilter; */
/* invoke-direct {v0}, Landroid/content/IntentFilter;-><init>()V */
/* .line 64 */
/* .local v0, "filter":Landroid/content/IntentFilter; */
final String v1 = "android.hardware.usb.action.USB_STATE"; // const-string v1, "android.hardware.usb.action.USB_STATE"
(( android.content.IntentFilter ) v0 ).addAction ( v1 ); // invoke-virtual {v0, v1}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V
/* .line 65 */
v1 = this.mContext;
/* new-instance v2, Lcom/miui/server/MiuiDfcService$UsbStatusReceiver; */
/* invoke-direct {v2, p0}, Lcom/miui/server/MiuiDfcService$UsbStatusReceiver;-><init>(Lcom/miui/server/MiuiDfcService;)V */
(( android.content.Context ) v1 ).registerReceiver ( v2, v0 ); // invoke-virtual {v1, v2, v0}, Landroid/content/Context;->registerReceiver(Landroid/content/BroadcastReceiver;Landroid/content/IntentFilter;)Landroid/content/Intent;
/* .line 66 */
com.miui.server.MiuiDfcService .getInstance ( );
} // .end method
public Integer getDFCVersion ( ) {
/* .locals 2 */
/* .line 128 */
v0 = /* invoke-direct {p0}, Lcom/miui/server/MiuiDfcService;->checkIsSystemUid()Z */
final String v1 = "MiuiDfcService"; // const-string v1, "MiuiDfcService"
/* if-nez v0, :cond_0 */
/* .line 129 */
final String v0 = "calling permission denied"; // const-string v0, "calling permission denied"
android.util.Slog .d ( v1,v0 );
/* .line 130 */
int v0 = -1; // const/4 v0, -0x1
/* .line 132 */
} // :cond_0
v0 = (( com.miui.server.MiuiDfcService ) p0 ).isDfcDebug ( ); // invoke-virtual {p0}, Lcom/miui/server/MiuiDfcService;->isDfcDebug()Z
if ( v0 != null) { // if-eqz v0, :cond_1
final String v0 = "getDFCVersion!"; // const-string v0, "getDFCVersion!"
android.util.Slog .d ( v1,v0 );
/* .line 133 */
} // :cond_1
int v0 = 1; // const/4 v0, 0x1
} // .end method
protected miui.dfc.IDfc getDfcNativeService ( ) {
/* .locals 5 */
/* .line 81 */
/* const-string/jumbo v0, "sys.dfcservice.is_front_scene" */
int v1 = 0; // const/4 v1, 0x0
v0 = android.os.SystemProperties .getInt ( v0,v1 );
/* .line 82 */
/* .local v0, "isScene":I */
/* iget-boolean v2, p0, Lcom/miui/server/MiuiDfcService;->mUsbConnect:Z */
if ( v2 != null) { // if-eqz v2, :cond_0
/* if-nez v0, :cond_0 */
/* .line 83 */
final String v1 = "MiuiDfcService"; // const-string v1, "MiuiDfcService"
/* new-instance v2, Ljava/lang/StringBuilder; */
/* invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V */
final String v3 = "Usb connect, no need to get DfcNativeService, isScene:"; // const-string v3, "Usb connect, no need to get DfcNativeService, isScene:"
(( java.lang.StringBuilder ) v2 ).append ( v3 ); // invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v2 ).append ( v0 ); // invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v2 ).toString ( ); // invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
android.util.Slog .d ( v1,v2 );
/* .line 84 */
/* const-string/jumbo v1, "sys.dfcservice.ctrl" */
final String v2 = "false"; // const-string v2, "false"
android.os.SystemProperties .set ( v1,v2 );
/* .line 85 */
int v1 = 0; // const/4 v1, 0x0
/* .line 87 */
} // :cond_0
v2 = this.mService;
/* if-nez v2, :cond_4 */
/* .line 88 */
/* const-class v2, Lcom/miui/server/MiuiDfcService; */
/* monitor-enter v2 */
/* .line 89 */
try { // :try_start_0
v3 = this.mService;
/* if-nez v3, :cond_3 */
/* .line 90 */
/* const-string/jumbo v3, "sys.dfcservice.ctrl" */
v3 = android.os.SystemProperties .getBoolean ( v3,v1 );
/* if-nez v3, :cond_1 */
/* .line 91 */
/* const-string/jumbo v3, "sys.dfcservice.ctrl" */
/* const-string/jumbo v4, "true" */
android.os.SystemProperties .set ( v3,v4 );
/* :try_end_0 */
/* .catchall {:try_start_0 ..:try_end_0} :catchall_0 */
/* .line 93 */
/* const-wide/16 v3, 0x1f4 */
try { // :try_start_1
java.lang.Thread .sleep ( v3,v4 );
/* :try_end_1 */
/* .catch Ljava/lang/InterruptedException; {:try_start_1 ..:try_end_1} :catch_0 */
/* .catchall {:try_start_1 ..:try_end_1} :catchall_0 */
/* .line 96 */
/* .line 94 */
/* :catch_0 */
/* move-exception v3 */
/* .line 95 */
/* .local v3, "e":Ljava/lang/InterruptedException; */
try { // :try_start_2
(( java.lang.InterruptedException ) v3 ).printStackTrace ( ); // invoke-virtual {v3}, Ljava/lang/InterruptedException;->printStackTrace()V
/* .line 98 */
} // .end local v3 # "e":Ljava/lang/InterruptedException;
} // :cond_1
} // :goto_0
final String v3 = "DfcNativeService"; // const-string v3, "DfcNativeService"
android.os.ServiceManager .getService ( v3 );
miui.dfc.IDfc$Stub .asInterface ( v3 );
this.mService = v3;
/* .line 99 */
v3 = this.mService;
/* :try_end_2 */
/* .catchall {:try_start_2 ..:try_end_2} :catchall_0 */
if ( v3 != null) { // if-eqz v3, :cond_2
/* .line 101 */
try { // :try_start_3
v3 = this.mService;
v4 = this.mDeathHandler;
/* :try_end_3 */
/* .catch Ljava/lang/Exception; {:try_start_3 ..:try_end_3} :catch_1 */
/* .catchall {:try_start_3 ..:try_end_3} :catchall_0 */
/* .line 102 */
/* :catch_1 */
/* move-exception v1 */
/* .line 103 */
/* .local v1, "e":Ljava/lang/Exception; */
try { // :try_start_4
(( java.lang.Exception ) v1 ).printStackTrace ( ); // invoke-virtual {v1}, Ljava/lang/Exception;->printStackTrace()V
/* .line 104 */
} // .end local v1 # "e":Ljava/lang/Exception;
} // :goto_1
/* .line 106 */
} // :cond_2
final String v1 = "MiuiDfcService"; // const-string v1, "MiuiDfcService"
final String v3 = "failed to get DfcNativeService."; // const-string v3, "failed to get DfcNativeService."
android.util.Slog .e ( v1,v3 );
/* .line 109 */
} // :cond_3
} // :goto_2
/* monitor-exit v2 */
/* :catchall_0 */
/* move-exception v1 */
/* monitor-exit v2 */
/* :try_end_4 */
/* .catchall {:try_start_4 ..:try_end_4} :catchall_0 */
/* throw v1 */
/* .line 111 */
} // :cond_4
} // :goto_3
v1 = this.mService;
} // .end method
public void getDuplicateFiles ( java.lang.String p0, miui.dfc.IGetDuplicateFilesCallback p1 ) {
/* .locals 5 */
/* .param p1, "data" # Ljava/lang/String; */
/* .param p2, "callback" # Lmiui/dfc/IGetDuplicateFilesCallback; */
/* .line 178 */
v0 = /* invoke-direct {p0}, Lcom/miui/server/MiuiDfcService;->checkIsSystemUid()Z */
final String v1 = "MiuiDfcService"; // const-string v1, "MiuiDfcService"
/* if-nez v0, :cond_0 */
/* .line 179 */
final String v0 = "calling permission denied"; // const-string v0, "calling permission denied"
android.util.Slog .d ( v1,v0 );
/* .line 180 */
return;
/* .line 182 */
} // :cond_0
v0 = (( com.miui.server.MiuiDfcService ) p0 ).isDfcDebug ( ); // invoke-virtual {p0}, Lcom/miui/server/MiuiDfcService;->isDfcDebug()Z
if ( v0 != null) { // if-eqz v0, :cond_1
/* new-instance v0, Ljava/lang/StringBuilder; */
/* invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V */
final String v2 = "getDuplicateFiles! data: "; // const-string v2, "getDuplicateFiles! data: "
(( java.lang.StringBuilder ) v0 ).append ( v2 ); // invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v0 ).append ( p1 ); // invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v0 ).toString ( ); // invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
android.util.Slog .d ( v1,v0 );
/* .line 183 */
} // :cond_1
final String v0 = ""; // const-string v0, ""
/* .line 185 */
/* .local v0, "result":Ljava/lang/String; */
try { // :try_start_0
(( com.miui.server.MiuiDfcService ) p0 ).getDfcNativeService ( ); // invoke-virtual {p0}, Lcom/miui/server/MiuiDfcService;->getDfcNativeService()Lmiui/dfc/IDfc;
/* .line 186 */
/* .local v2, "service":Lmiui/dfc/IDfc; */
if ( v2 != null) { // if-eqz v2, :cond_2
/* .line 187 */
/* move-object v0, v3 */
/* .line 188 */
v3 = (( com.miui.server.MiuiDfcService ) p0 ).isDfcDebug ( ); // invoke-virtual {p0}, Lcom/miui/server/MiuiDfcService;->isDfcDebug()Z
if ( v3 != null) { // if-eqz v3, :cond_3
/* new-instance v3, Ljava/lang/StringBuilder; */
/* invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V */
final String v4 = "Dfc_getDuplicateFiles, result: "; // const-string v4, "Dfc_getDuplicateFiles, result: "
(( java.lang.StringBuilder ) v3 ).append ( v4 ); // invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v3 ).append ( v0 ); // invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v3 ).toString ( ); // invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
android.util.Slog .d ( v1,v3 );
/* .line 190 */
} // :cond_2
v3 = (( com.miui.server.MiuiDfcService ) p0 ).isDfcDebug ( ); // invoke-virtual {p0}, Lcom/miui/server/MiuiDfcService;->isDfcDebug()Z
if ( v3 != null) { // if-eqz v3, :cond_3
final String v3 = "getDuplicateFiles getDfcNativeService is null"; // const-string v3, "getDuplicateFiles getDfcNativeService is null"
android.util.Slog .d ( v1,v3 );
/* :try_end_0 */
/* .catch Ljava/lang/Exception; {:try_start_0 ..:try_end_0} :catch_0 */
/* .line 194 */
} // .end local v2 # "service":Lmiui/dfc/IDfc;
} // :cond_3
} // :goto_0
/* .line 192 */
/* :catch_0 */
/* move-exception v2 */
/* .line 193 */
/* .local v2, "e":Ljava/lang/Exception; */
/* new-instance v3, Ljava/lang/StringBuilder; */
/* invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V */
final String v4 = "Dfc_getDuplicateFiles error: "; // const-string v4, "Dfc_getDuplicateFiles error: "
(( java.lang.StringBuilder ) v3 ).append ( v4 ); // invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.Exception ) v2 ).toString ( ); // invoke-virtual {v2}, Ljava/lang/Exception;->toString()Ljava/lang/String;
(( java.lang.StringBuilder ) v3 ).append ( v4 ); // invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v3 ).toString ( ); // invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
android.util.Slog .e ( v1,v3 );
/* .line 196 */
} // .end local v2 # "e":Ljava/lang/Exception;
} // :goto_1
if ( p2 != null) { // if-eqz p2, :cond_4
/* .line 197 */
try { // :try_start_1
/* :try_end_1 */
/* .catch Ljava/lang/Exception; {:try_start_1 ..:try_end_1} :catch_1 */
/* .line 199 */
/* :catch_1 */
/* move-exception v2 */
/* .line 200 */
/* .restart local v2 # "e":Ljava/lang/Exception; */
/* new-instance v3, Ljava/lang/StringBuilder; */
/* invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V */
final String v4 = "IGetDuplicateFilesCallback callback error: "; // const-string v4, "IGetDuplicateFilesCallback callback error: "
(( java.lang.StringBuilder ) v3 ).append ( v4 ); // invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.Exception ) v2 ).toString ( ); // invoke-virtual {v2}, Ljava/lang/Exception;->toString()Ljava/lang/String;
(( java.lang.StringBuilder ) v3 ).append ( v4 ); // invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v3 ).toString ( ); // invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
android.util.Slog .e ( v1,v3 );
/* .line 201 */
} // .end local v2 # "e":Ljava/lang/Exception;
} // :cond_4
} // :goto_2
/* nop */
/* .line 202 */
} // :goto_3
return;
} // .end method
public Boolean isDfcDebug ( ) {
/* .locals 1 */
/* .line 319 */
/* sget-boolean v0, Lcom/miui/server/MiuiDfcService;->IS_USERDEBUG:Z */
/* if-nez v0, :cond_1 */
/* sget-boolean v0, Lcom/miui/server/MiuiDfcService;->DFC_DEBUG:Z */
if ( v0 != null) { // if-eqz v0, :cond_0
} // :cond_0
int v0 = 0; // const/4 v0, 0x0
} // :cond_1
} // :goto_0
int v0 = 1; // const/4 v0, 0x1
} // :goto_1
} // .end method
public Boolean isSupportDFC ( ) {
/* .locals 5 */
/* .line 138 */
v0 = /* invoke-direct {p0}, Lcom/miui/server/MiuiDfcService;->checkIsSystemUid()Z */
final String v1 = "MiuiDfcService"; // const-string v1, "MiuiDfcService"
int v2 = 0; // const/4 v2, 0x0
/* if-nez v0, :cond_0 */
/* .line 139 */
final String v0 = "calling permission denied"; // const-string v0, "calling permission denied"
android.util.Slog .d ( v1,v0 );
/* .line 140 */
/* .line 142 */
} // :cond_0
/* const-string/jumbo v0, "sys.dfcservice.is_front_scene" */
v0 = android.os.SystemProperties .getInt ( v0,v2 );
/* .line 143 */
/* .local v0, "isScene":I */
v3 = (( com.miui.server.MiuiDfcService ) p0 ).isDfcDebug ( ); // invoke-virtual {p0}, Lcom/miui/server/MiuiDfcService;->isDfcDebug()Z
if ( v3 != null) { // if-eqz v3, :cond_1
/* new-instance v3, Ljava/lang/StringBuilder; */
/* invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V */
final String v4 = "isSupportDFC! isScene:"; // const-string v4, "isSupportDFC! isScene:"
(( java.lang.StringBuilder ) v3 ).append ( v4 ); // invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v3 ).append ( v0 ); // invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v3 ).toString ( ); // invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
android.util.Slog .d ( v1,v3 );
/* .line 144 */
} // :cond_1
int v1 = 1; // const/4 v1, 0x1
/* if-eq v0, v1, :cond_2 */
/* iget-boolean v3, p0, Lcom/miui/server/MiuiDfcService;->mUsbConnect:Z */
/* if-nez v3, :cond_3 */
} // :cond_2
/* move v2, v1 */
} // :cond_3
} // .end method
public Long restoreDuplicateFiles ( java.lang.String p0 ) {
/* .locals 6 */
/* .param p1, "data" # Ljava/lang/String; */
/* .line 261 */
v0 = /* invoke-direct {p0}, Lcom/miui/server/MiuiDfcService;->checkIsSystemUid()Z */
final String v1 = "MiuiDfcService"; // const-string v1, "MiuiDfcService"
/* if-nez v0, :cond_0 */
/* .line 262 */
final String v0 = "calling permission denied"; // const-string v0, "calling permission denied"
android.util.Slog .d ( v1,v0 );
/* .line 263 */
/* const-wide/16 v0, 0x0 */
/* return-wide v0 */
/* .line 265 */
} // :cond_0
v0 = (( com.miui.server.MiuiDfcService ) p0 ).isDfcDebug ( ); // invoke-virtual {p0}, Lcom/miui/server/MiuiDfcService;->isDfcDebug()Z
if ( v0 != null) { // if-eqz v0, :cond_1
/* new-instance v0, Ljava/lang/StringBuilder; */
/* invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V */
final String v2 = "restoreDuplicateFiles!, data:"; // const-string v2, "restoreDuplicateFiles!, data:"
(( java.lang.StringBuilder ) v0 ).append ( v2 ); // invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v0 ).append ( p1 ); // invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v0 ).toString ( ); // invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
android.util.Slog .d ( v1,v0 );
/* .line 266 */
} // :cond_1
/* const-wide/16 v2, 0x0 */
/* .line 268 */
/* .local v2, "result":J */
try { // :try_start_0
(( com.miui.server.MiuiDfcService ) p0 ).getDfcNativeService ( ); // invoke-virtual {p0}, Lcom/miui/server/MiuiDfcService;->getDfcNativeService()Lmiui/dfc/IDfc;
/* .line 269 */
/* .local v0, "service":Lmiui/dfc/IDfc; */
if ( v0 != null) { // if-eqz v0, :cond_2
/* .line 270 */
/* move-result-wide v4 */
/* move-wide v2, v4 */
/* .line 271 */
v4 = (( com.miui.server.MiuiDfcService ) p0 ).isDfcDebug ( ); // invoke-virtual {p0}, Lcom/miui/server/MiuiDfcService;->isDfcDebug()Z
if ( v4 != null) { // if-eqz v4, :cond_3
/* new-instance v4, Ljava/lang/StringBuilder; */
/* invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V */
final String v5 = "Dfc_restoreDuplicateFiles, result:"; // const-string v5, "Dfc_restoreDuplicateFiles, result:"
(( java.lang.StringBuilder ) v4 ).append ( v5 ); // invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v4 ).append ( v2, v3 ); // invoke-virtual {v4, v2, v3}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v4 ).toString ( ); // invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
android.util.Slog .d ( v1,v4 );
/* .line 273 */
} // :cond_2
v4 = (( com.miui.server.MiuiDfcService ) p0 ).isDfcDebug ( ); // invoke-virtual {p0}, Lcom/miui/server/MiuiDfcService;->isDfcDebug()Z
if ( v4 != null) { // if-eqz v4, :cond_3
final String v4 = "restoreDuplicateFiles getDfcNativeService is null"; // const-string v4, "restoreDuplicateFiles getDfcNativeService is null"
android.util.Slog .d ( v1,v4 );
/* :try_end_0 */
/* .catch Ljava/lang/Exception; {:try_start_0 ..:try_end_0} :catch_0 */
/* .line 277 */
} // .end local v0 # "service":Lmiui/dfc/IDfc;
} // :cond_3
} // :goto_0
/* .line 275 */
/* :catch_0 */
/* move-exception v0 */
/* .line 276 */
/* .local v0, "e":Ljava/lang/Exception; */
/* new-instance v4, Ljava/lang/StringBuilder; */
/* invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V */
final String v5 = "Dfc_restoreDuplicateFiles error: "; // const-string v5, "Dfc_restoreDuplicateFiles error: "
(( java.lang.StringBuilder ) v4 ).append ( v5 ); // invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.Exception ) v0 ).toString ( ); // invoke-virtual {v0}, Ljava/lang/Exception;->toString()Ljava/lang/String;
(( java.lang.StringBuilder ) v4 ).append ( v5 ); // invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v4 ).toString ( ); // invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
android.util.Slog .e ( v1,v4 );
/* .line 278 */
} // .end local v0 # "e":Ljava/lang/Exception;
} // :goto_1
/* return-wide v2 */
} // .end method
public void scanDuplicateFiles ( java.lang.String p0, miui.dfc.IDuplicateFileScanCallback p1, Integer p2 ) {
/* .locals 4 */
/* .param p1, "data" # Ljava/lang/String; */
/* .param p2, "callback" # Lmiui/dfc/IDuplicateFileScanCallback; */
/* .param p3, "flags" # I */
/* .line 149 */
v0 = /* invoke-direct {p0}, Lcom/miui/server/MiuiDfcService;->checkIsSystemUid()Z */
final String v1 = "MiuiDfcService"; // const-string v1, "MiuiDfcService"
/* if-nez v0, :cond_0 */
/* .line 150 */
final String v0 = "calling permission denied"; // const-string v0, "calling permission denied"
android.util.Slog .d ( v1,v0 );
/* .line 151 */
return;
/* .line 153 */
} // :cond_0
final String v0 = "scanDuplicateFiles!"; // const-string v0, "scanDuplicateFiles!"
android.util.Slog .d ( v1,v0 );
/* .line 155 */
try { // :try_start_0
(( com.miui.server.MiuiDfcService ) p0 ).getDfcNativeService ( ); // invoke-virtual {p0}, Lcom/miui/server/MiuiDfcService;->getDfcNativeService()Lmiui/dfc/IDfc;
/* .line 156 */
/* .local v0, "service":Lmiui/dfc/IDfc; */
if ( v0 != null) { // if-eqz v0, :cond_1
/* .line 157 */
/* const-string/jumbo v2, "sys.dfcservice.stop_scan" */
final String v3 = "false"; // const-string v3, "false"
android.os.SystemProperties .set ( v2,v3 );
/* .line 158 */
/* .line 159 */
v2 = (( com.miui.server.MiuiDfcService ) p0 ).isDfcDebug ( ); // invoke-virtual {p0}, Lcom/miui/server/MiuiDfcService;->isDfcDebug()Z
if ( v2 != null) { // if-eqz v2, :cond_2
final String v2 = "Dfc_scanDuplicateFiles done"; // const-string v2, "Dfc_scanDuplicateFiles done"
android.util.Slog .d ( v1,v2 );
/* .line 161 */
} // :cond_1
v2 = (( com.miui.server.MiuiDfcService ) p0 ).isDfcDebug ( ); // invoke-virtual {p0}, Lcom/miui/server/MiuiDfcService;->isDfcDebug()Z
if ( v2 != null) { // if-eqz v2, :cond_2
final String v2 = "scanDuplicateFiles getDfcNativeService is null"; // const-string v2, "scanDuplicateFiles getDfcNativeService is null"
android.util.Slog .d ( v1,v2 );
/* :try_end_0 */
/* .catch Ljava/lang/Exception; {:try_start_0 ..:try_end_0} :catch_0 */
/* .line 165 */
} // .end local v0 # "service":Lmiui/dfc/IDfc;
} // :cond_2
} // :goto_0
/* .line 163 */
/* :catch_0 */
/* move-exception v0 */
/* .line 164 */
/* .local v0, "e":Ljava/lang/Exception; */
/* new-instance v2, Ljava/lang/StringBuilder; */
/* invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V */
final String v3 = "scanDuplicateFiles error: "; // const-string v3, "scanDuplicateFiles error: "
(( java.lang.StringBuilder ) v2 ).append ( v3 ); // invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.Exception ) v0 ).toString ( ); // invoke-virtual {v0}, Ljava/lang/Exception;->toString()Ljava/lang/String;
(( java.lang.StringBuilder ) v2 ).append ( v3 ); // invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v2 ).toString ( ); // invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
android.util.Slog .e ( v1,v2 );
/* .line 168 */
} // .end local v0 # "e":Ljava/lang/Exception;
} // :goto_1
if ( p2 != null) { // if-eqz p2, :cond_3
/* .line 169 */
try { // :try_start_1
/* :try_end_1 */
/* .catch Ljava/lang/Exception; {:try_start_1 ..:try_end_1} :catch_1 */
/* .line 171 */
/* :catch_1 */
/* move-exception v0 */
/* .line 172 */
/* .restart local v0 # "e":Ljava/lang/Exception; */
/* new-instance v2, Ljava/lang/StringBuilder; */
/* invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V */
final String v3 = "onDumplicateFileScanfinished error: "; // const-string v3, "onDumplicateFileScanfinished error: "
(( java.lang.StringBuilder ) v2 ).append ( v3 ); // invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.Exception ) v0 ).toString ( ); // invoke-virtual {v0}, Ljava/lang/Exception;->toString()Ljava/lang/String;
(( java.lang.StringBuilder ) v2 ).append ( v3 ); // invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v2 ).toString ( ); // invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
android.util.Slog .e ( v1,v2 );
/* .line 173 */
} // .end local v0 # "e":Ljava/lang/Exception;
} // :cond_3
} // :goto_2
/* nop */
/* .line 174 */
} // :goto_3
return;
} // .end method
public void setDFCExceptRules ( java.lang.String p0 ) {
/* .locals 4 */
/* .param p1, "data" # Ljava/lang/String; */
/* .line 242 */
v0 = /* invoke-direct {p0}, Lcom/miui/server/MiuiDfcService;->checkIsSystemUid()Z */
final String v1 = "MiuiDfcService"; // const-string v1, "MiuiDfcService"
/* if-nez v0, :cond_0 */
/* .line 243 */
final String v0 = "calling permission denied"; // const-string v0, "calling permission denied"
android.util.Slog .d ( v1,v0 );
/* .line 244 */
return;
/* .line 246 */
} // :cond_0
v0 = (( com.miui.server.MiuiDfcService ) p0 ).isDfcDebug ( ); // invoke-virtual {p0}, Lcom/miui/server/MiuiDfcService;->isDfcDebug()Z
if ( v0 != null) { // if-eqz v0, :cond_1
/* new-instance v0, Ljava/lang/StringBuilder; */
/* invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V */
/* const-string/jumbo v2, "setDFCExceptRules!, data:" */
(( java.lang.StringBuilder ) v0 ).append ( v2 ); // invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v0 ).append ( p1 ); // invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v0 ).toString ( ); // invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
android.util.Slog .d ( v1,v0 );
/* .line 248 */
} // :cond_1
try { // :try_start_0
(( com.miui.server.MiuiDfcService ) p0 ).getDfcNativeService ( ); // invoke-virtual {p0}, Lcom/miui/server/MiuiDfcService;->getDfcNativeService()Lmiui/dfc/IDfc;
/* .line 249 */
/* .local v0, "service":Lmiui/dfc/IDfc; */
if ( v0 != null) { // if-eqz v0, :cond_2
/* .line 250 */
/* .line 252 */
} // :cond_2
v2 = (( com.miui.server.MiuiDfcService ) p0 ).isDfcDebug ( ); // invoke-virtual {p0}, Lcom/miui/server/MiuiDfcService;->isDfcDebug()Z
if ( v2 != null) { // if-eqz v2, :cond_3
/* const-string/jumbo v2, "setDFCExceptRules getDfcNativeService is null" */
android.util.Slog .d ( v1,v2 );
/* :try_end_0 */
/* .catch Ljava/lang/Exception; {:try_start_0 ..:try_end_0} :catch_0 */
/* .line 256 */
} // .end local v0 # "service":Lmiui/dfc/IDfc;
} // :cond_3
} // :goto_0
/* .line 254 */
/* :catch_0 */
/* move-exception v0 */
/* .line 255 */
/* .local v0, "e":Ljava/lang/Exception; */
/* new-instance v2, Ljava/lang/StringBuilder; */
/* invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V */
final String v3 = "Dfc_setDFCExceptRules error: "; // const-string v3, "Dfc_setDFCExceptRules error: "
(( java.lang.StringBuilder ) v2 ).append ( v3 ); // invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.Exception ) v0 ).toString ( ); // invoke-virtual {v0}, Ljava/lang/Exception;->toString()Ljava/lang/String;
(( java.lang.StringBuilder ) v2 ).append ( v3 ); // invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v2 ).toString ( ); // invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
android.util.Slog .e ( v1,v2 );
/* .line 257 */
} // .end local v0 # "e":Ljava/lang/Exception;
} // :goto_1
return;
} // .end method
