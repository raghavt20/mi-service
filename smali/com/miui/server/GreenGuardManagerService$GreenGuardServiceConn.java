class com.miui.server.GreenGuardManagerService$GreenGuardServiceConn implements android.content.ServiceConnection {
	 /* .source "GreenGuardManagerService.java" */
	 /* # interfaces */
	 /* # annotations */
	 /* .annotation system Ldalvik/annotation/EnclosingClass; */
	 /* value = Lcom/miui/server/GreenGuardManagerService; */
} // .end annotation
/* .annotation system Ldalvik/annotation/InnerClass; */
/* accessFlags = 0x8 */
/* name = "GreenGuardServiceConn" */
} // .end annotation
/* # instance fields */
private final android.content.Context mContext;
/* # direct methods */
public static void $r8$lambda$d7pOZ-1HlkJK0vHz3FOHR4cpU1s ( com.miui.server.GreenGuardManagerService$GreenGuardServiceConn p0 ) { //synthethic
/* .locals 0 */
/* invoke-direct {p0}, Lcom/miui/server/GreenGuardManagerService$GreenGuardServiceConn;->lambda$onServiceDisconnected$0()V */
return;
} // .end method
public com.miui.server.GreenGuardManagerService$GreenGuardServiceConn ( ) {
/* .locals 0 */
/* .param p1, "context" # Landroid/content/Context; */
/* .line 80 */
/* invoke-direct {p0}, Ljava/lang/Object;-><init>()V */
/* .line 81 */
this.mContext = p1;
/* .line 82 */
return;
} // .end method
private void lambda$onServiceDisconnected$0 ( ) { //synthethic
/* .locals 1 */
/* .line 93 */
v0 = this.mContext;
com.miui.server.GreenGuardManagerService .-$$Nest$smstartWatchGreenGuardProcessInner ( v0 );
return;
} // .end method
/* # virtual methods */
public void onServiceConnected ( android.content.ComponentName p0, android.os.IBinder p1 ) {
/* .locals 2 */
/* .param p1, "name" # Landroid/content/ComponentName; */
/* .param p2, "service" # Landroid/os/IBinder; */
/* .line 86 */
final String v0 = "GreenKidManagerService"; // const-string v0, "GreenKidManagerService"
final String v1 = "On GreenKidService Connected"; // const-string v1, "On GreenKidService Connected"
android.util.Log .d ( v0,v1 );
/* .line 87 */
return;
} // .end method
public void onServiceDisconnected ( android.content.ComponentName p0 ) {
/* .locals 4 */
/* .param p1, "name" # Landroid/content/ComponentName; */
/* .line 91 */
final String v0 = "GreenKidManagerService"; // const-string v0, "GreenKidManagerService"
final String v1 = "On GreenKidService Disconnected , schedule restart it in 10s."; // const-string v1, "On GreenKidService Disconnected , schedule restart it in 10s."
android.util.Log .d ( v0,v1 );
/* .line 92 */
com.android.internal.os.BackgroundThread .getHandler ( );
/* new-instance v1, Lcom/miui/server/GreenGuardManagerService$GreenGuardServiceConn$$ExternalSyntheticLambda0; */
/* invoke-direct {v1, p0}, Lcom/miui/server/GreenGuardManagerService$GreenGuardServiceConn$$ExternalSyntheticLambda0;-><init>(Lcom/miui/server/GreenGuardManagerService$GreenGuardServiceConn;)V */
/* const-wide/16 v2, 0x2710 */
(( android.os.Handler ) v0 ).postDelayed ( v1, v2, v3 ); // invoke-virtual {v0, v1, v2, v3}, Landroid/os/Handler;->postDelayed(Ljava/lang/Runnable;J)Z
/* .line 94 */
return;
} // .end method
