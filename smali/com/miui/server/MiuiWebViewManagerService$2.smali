.class Lcom/miui/server/MiuiWebViewManagerService$2;
.super Landroid/os/Handler;
.source "MiuiWebViewManagerService.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/miui/server/MiuiWebViewManagerService;-><init>(Landroid/content/Context;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/miui/server/MiuiWebViewManagerService;


# direct methods
.method constructor <init>(Lcom/miui/server/MiuiWebViewManagerService;Landroid/os/Looper;)V
    .locals 0
    .param p1, "this$0"    # Lcom/miui/server/MiuiWebViewManagerService;
    .param p2, "looper"    # Landroid/os/Looper;

    .line 44
    iput-object p1, p0, Lcom/miui/server/MiuiWebViewManagerService$2;->this$0:Lcom/miui/server/MiuiWebViewManagerService;

    invoke-direct {p0, p2}, Landroid/os/Handler;-><init>(Landroid/os/Looper;)V

    return-void
.end method


# virtual methods
.method public handleMessage(Landroid/os/Message;)V
    .locals 13
    .param p1, "msg"    # Landroid/os/Message;

    .line 47
    iget v0, p1, Landroid/os/Message;->what:I

    iget-object v1, p0, Lcom/miui/server/MiuiWebViewManagerService$2;->this$0:Lcom/miui/server/MiuiWebViewManagerService;

    invoke-static {v1}, Lcom/miui/server/MiuiWebViewManagerService;->-$$Nest$fgetMSG_RESTART_WEBVIEW(Lcom/miui/server/MiuiWebViewManagerService;)I

    move-result v1

    if-ne v0, v1, :cond_3

    .line 49
    invoke-virtual {p1}, Landroid/os/Message;->getData()Landroid/os/Bundle;

    move-result-object v0

    const-string/jumbo v1, "sendTime"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getLong(Ljava/lang/String;)J

    move-result-wide v0

    .line 50
    .local v0, "send":J
    const-string v2, "activity"

    invoke-static {v2}, Landroid/os/ServiceManager;->getService(Ljava/lang/String;)Landroid/os/IBinder;

    move-result-object v2

    check-cast v2, Lcom/android/server/am/ActivityManagerService;

    .line 51
    .local v2, "am":Lcom/android/server/am/ActivityManagerService;
    iget-object v3, p0, Lcom/miui/server/MiuiWebViewManagerService$2;->this$0:Lcom/miui/server/MiuiWebViewManagerService;

    invoke-static {v3, v2}, Lcom/miui/server/MiuiWebViewManagerService;->-$$Nest$mcollectWebViewProcesses(Lcom/miui/server/MiuiWebViewManagerService;Lcom/android/server/am/ActivityManagerService;)Ljava/util/List;

    move-result-object v3

    .line 52
    .local v3, "pkgs":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;"
    const/4 v4, 0x0

    .line 53
    .local v4, "killed":I
    if-eqz v3, :cond_3

    .line 54
    const/4 v5, 0x0

    .local v5, "i":I
    :goto_0
    invoke-interface {v3}, Ljava/util/List;->size()I

    move-result v6

    const-string v7, "MiuiWebViewManagerService"

    if-ge v5, v6, :cond_2

    .line 55
    invoke-interface {v3, v5}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v6

    check-cast v6, Ljava/lang/String;

    .line 56
    .local v6, "pkgName":Ljava/lang/String;
    if-nez v6, :cond_0

    .line 57
    goto :goto_1

    .line 59
    :cond_0
    const-string v8, "#"

    invoke-virtual {v6, v8}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v8

    .line 60
    .local v8, "splitInfo":[Ljava/lang/String;
    const/4 v9, 0x0

    aget-object v9, v8, v9

    .line 61
    .local v9, "barePkgName":Ljava/lang/String;
    const/4 v10, 0x1

    aget-object v10, v8, v10

    invoke-static {v10}, Ljava/lang/Integer;->valueOf(Ljava/lang/String;)Ljava/lang/Integer;

    move-result-object v10

    invoke-virtual {v10}, Ljava/lang/Integer;->intValue()I

    move-result v10

    .line 62
    .local v10, "pid":I
    invoke-static {}, Lcom/miui/server/MiuiWebViewManagerService;->-$$Nest$sfgetEXEMPT_APPS()Ljava/util/List;

    move-result-object v11

    invoke-interface {v11, v9}, Ljava/util/List;->contains(Ljava/lang/Object;)Z

    move-result v11

    if-eqz v11, :cond_1

    .line 63
    goto :goto_1

    .line 65
    :cond_1
    iget-object v11, p0, Lcom/miui/server/MiuiWebViewManagerService$2;->this$0:Lcom/miui/server/MiuiWebViewManagerService;

    invoke-static {v11}, Lcom/miui/server/MiuiWebViewManagerService;->-$$Nest$fgetmContext(Lcom/miui/server/MiuiWebViewManagerService;)Landroid/content/Context;

    move-result-object v11

    invoke-virtual {v11}, Landroid/content/Context;->getUserId()I

    move-result v11

    invoke-virtual {v2, v9, v11}, Lcom/android/server/am/ActivityManagerService;->forceStopPackage(Ljava/lang/String;I)V

    .line 66
    new-instance v11, Ljava/lang/StringBuilder;

    invoke-direct {v11}, Ljava/lang/StringBuilder;-><init>()V

    const-string v12, "kill pkgName: "

    invoke-virtual {v11, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v11

    invoke-virtual {v11, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v11

    const-string v12, " pid: "

    invoke-virtual {v11, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v11

    invoke-virtual {v11, v10}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v11

    invoke-virtual {v11}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v11

    invoke-static {v7, v11}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 67
    add-int/lit8 v4, v4, 0x1

    .line 54
    .end local v6    # "pkgName":Ljava/lang/String;
    .end local v8    # "splitInfo":[Ljava/lang/String;
    .end local v9    # "barePkgName":Ljava/lang/String;
    .end local v10    # "pid":I
    :goto_1
    add-int/lit8 v5, v5, 0x1

    goto :goto_0

    .line 70
    .end local v5    # "i":I
    :cond_2
    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "restart webview procs: "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-interface {v3}, Ljava/util/List;->size()I

    move-result v6

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, " with timeUsage: "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    .line 71
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v8

    sub-long/2addr v8, v0

    invoke-virtual {v5, v8, v9}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, "ms killed: "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    .line 70
    invoke-static {v7, v5}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 74
    .end local v0    # "send":J
    .end local v2    # "am":Lcom/android/server/am/ActivityManagerService;
    .end local v3    # "pkgs":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;"
    .end local v4    # "killed":I
    :cond_3
    return-void
.end method
