.class Lcom/miui/server/MiuiUsbService$UsbDebuggingManager$UsbDebuggingHandler;
.super Landroid/os/Handler;
.source "MiuiUsbService.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/miui/server/MiuiUsbService$UsbDebuggingManager;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = "UsbDebuggingHandler"
.end annotation


# static fields
.field private static final MESSAGE_ADB_ALLOW:I = 0x3

.field private static final MESSAGE_ADB_CONFIRM:I = 0x5

.field private static final MESSAGE_ADB_DENY:I = 0x4

.field private static final MESSAGE_ADB_DISABLED:I = 0x2

.field private static final MESSAGE_ADB_ENABLED:I = 0x1


# instance fields
.field final synthetic this$1:Lcom/miui/server/MiuiUsbService$UsbDebuggingManager;


# direct methods
.method public constructor <init>(Lcom/miui/server/MiuiUsbService$UsbDebuggingManager;Landroid/os/Looper;)V
    .locals 0
    .param p1, "this$1"    # Lcom/miui/server/MiuiUsbService$UsbDebuggingManager;
    .param p2, "looper"    # Landroid/os/Looper;

    .line 206
    iput-object p1, p0, Lcom/miui/server/MiuiUsbService$UsbDebuggingManager$UsbDebuggingHandler;->this$1:Lcom/miui/server/MiuiUsbService$UsbDebuggingManager;

    .line 207
    invoke-direct {p0, p2}, Landroid/os/Handler;-><init>(Landroid/os/Looper;)V

    .line 208
    return-void
.end method


# virtual methods
.method public handleMessage(Landroid/os/Message;)V
    .locals 4
    .param p1, "msg"    # Landroid/os/Message;

    .line 211
    iget v0, p1, Landroid/os/Message;->what:I

    const/4 v1, 0x1

    packed-switch v0, :pswitch_data_0

    goto/16 :goto_1

    .line 263
    :pswitch_0
    iget-object v0, p1, Landroid/os/Message;->obj:Ljava/lang/Object;

    check-cast v0, Ljava/lang/String;

    .line 264
    .local v0, "key":Ljava/lang/String;
    iget-object v1, p0, Lcom/miui/server/MiuiUsbService$UsbDebuggingManager$UsbDebuggingHandler;->this$1:Lcom/miui/server/MiuiUsbService$UsbDebuggingManager;

    invoke-static {v1, v0}, Lcom/miui/server/MiuiUsbService$UsbDebuggingManager;->-$$Nest$mgetFingerprints(Lcom/miui/server/MiuiUsbService$UsbDebuggingManager;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/miui/server/MiuiUsbService$UsbDebuggingManager;->-$$Nest$fputmFingerprints(Lcom/miui/server/MiuiUsbService$UsbDebuggingManager;Ljava/lang/String;)V

    .line 265
    iget-object v1, p0, Lcom/miui/server/MiuiUsbService$UsbDebuggingManager$UsbDebuggingHandler;->this$1:Lcom/miui/server/MiuiUsbService$UsbDebuggingManager;

    invoke-static {v1}, Lcom/miui/server/MiuiUsbService$UsbDebuggingManager;->-$$Nest$fgetmFingerprints(Lcom/miui/server/MiuiUsbService$UsbDebuggingManager;)Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v0, v2}, Lcom/miui/server/MiuiUsbService$UsbDebuggingManager;->-$$Nest$mshowConfirmationDialog(Lcom/miui/server/MiuiUsbService$UsbDebuggingManager;Ljava/lang/String;Ljava/lang/String;)V

    .line 266
    goto/16 :goto_1

    .line 259
    .end local v0    # "key":Ljava/lang/String;
    :pswitch_1
    iget-object v0, p0, Lcom/miui/server/MiuiUsbService$UsbDebuggingManager$UsbDebuggingHandler;->this$1:Lcom/miui/server/MiuiUsbService$UsbDebuggingManager;

    const-string v1, "NO"

    invoke-virtual {v0, v1}, Lcom/miui/server/MiuiUsbService$UsbDebuggingManager;->sendResponse(Ljava/lang/String;)V

    .line 260
    goto/16 :goto_1

    .line 241
    :pswitch_2
    iget-object v0, p1, Landroid/os/Message;->obj:Ljava/lang/Object;

    check-cast v0, Ljava/lang/String;

    .line 242
    .restart local v0    # "key":Ljava/lang/String;
    iget-object v2, p0, Lcom/miui/server/MiuiUsbService$UsbDebuggingManager$UsbDebuggingHandler;->this$1:Lcom/miui/server/MiuiUsbService$UsbDebuggingManager;

    invoke-static {v2, v0}, Lcom/miui/server/MiuiUsbService$UsbDebuggingManager;->-$$Nest$mgetFingerprints(Lcom/miui/server/MiuiUsbService$UsbDebuggingManager;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    .line 244
    .local v2, "fingerprints":Ljava/lang/String;
    iget-object v3, p0, Lcom/miui/server/MiuiUsbService$UsbDebuggingManager$UsbDebuggingHandler;->this$1:Lcom/miui/server/MiuiUsbService$UsbDebuggingManager;

    invoke-static {v3}, Lcom/miui/server/MiuiUsbService$UsbDebuggingManager;->-$$Nest$fgetmFingerprints(Lcom/miui/server/MiuiUsbService$UsbDebuggingManager;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-nez v3, :cond_0

    .line 245
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "Fingerprints do not match. Got "

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v3, ", expected "

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v3, p0, Lcom/miui/server/MiuiUsbService$UsbDebuggingManager$UsbDebuggingHandler;->this$1:Lcom/miui/server/MiuiUsbService$UsbDebuggingManager;

    invoke-static {v3}, Lcom/miui/server/MiuiUsbService$UsbDebuggingManager;->-$$Nest$fgetmFingerprints(Lcom/miui/server/MiuiUsbService$UsbDebuggingManager;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    const-string v3, "MiuiUsbService"

    invoke-static {v3, v1}, Landroid/util/Slog;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 247
    goto :goto_1

    .line 250
    :cond_0
    iget v3, p1, Landroid/os/Message;->arg1:I

    if-ne v3, v1, :cond_1

    .line 251
    iget-object v1, p0, Lcom/miui/server/MiuiUsbService$UsbDebuggingManager$UsbDebuggingHandler;->this$1:Lcom/miui/server/MiuiUsbService$UsbDebuggingManager;

    invoke-static {v1, v0}, Lcom/miui/server/MiuiUsbService$UsbDebuggingManager;->-$$Nest$mwriteKey(Lcom/miui/server/MiuiUsbService$UsbDebuggingManager;Ljava/lang/String;)V

    .line 254
    :cond_1
    iget-object v1, p0, Lcom/miui/server/MiuiUsbService$UsbDebuggingManager$UsbDebuggingHandler;->this$1:Lcom/miui/server/MiuiUsbService$UsbDebuggingManager;

    const-string v3, "OK"

    invoke-virtual {v1, v3}, Lcom/miui/server/MiuiUsbService$UsbDebuggingManager;->sendResponse(Ljava/lang/String;)V

    .line 255
    goto :goto_1

    .line 224
    .end local v0    # "key":Ljava/lang/String;
    .end local v2    # "fingerprints":Ljava/lang/String;
    :pswitch_3
    iget-object v0, p0, Lcom/miui/server/MiuiUsbService$UsbDebuggingManager$UsbDebuggingHandler;->this$1:Lcom/miui/server/MiuiUsbService$UsbDebuggingManager;

    invoke-static {v0}, Lcom/miui/server/MiuiUsbService$UsbDebuggingManager;->-$$Nest$fgetmAdbEnabled(Lcom/miui/server/MiuiUsbService$UsbDebuggingManager;)Z

    move-result v0

    if-nez v0, :cond_2

    .line 225
    goto :goto_1

    .line 227
    :cond_2
    iget-object v0, p0, Lcom/miui/server/MiuiUsbService$UsbDebuggingManager$UsbDebuggingHandler;->this$1:Lcom/miui/server/MiuiUsbService$UsbDebuggingManager;

    const/4 v1, 0x0

    invoke-static {v0, v1}, Lcom/miui/server/MiuiUsbService$UsbDebuggingManager;->-$$Nest$fputmAdbEnabled(Lcom/miui/server/MiuiUsbService$UsbDebuggingManager;Z)V

    .line 228
    iget-object v0, p0, Lcom/miui/server/MiuiUsbService$UsbDebuggingManager$UsbDebuggingHandler;->this$1:Lcom/miui/server/MiuiUsbService$UsbDebuggingManager;

    invoke-virtual {v0}, Lcom/miui/server/MiuiUsbService$UsbDebuggingManager;->closeSocket()V

    .line 231
    :try_start_0
    iget-object v0, p0, Lcom/miui/server/MiuiUsbService$UsbDebuggingManager$UsbDebuggingHandler;->this$1:Lcom/miui/server/MiuiUsbService$UsbDebuggingManager;

    invoke-static {v0}, Lcom/miui/server/MiuiUsbService$UsbDebuggingManager;->-$$Nest$fgetmThread(Lcom/miui/server/MiuiUsbService$UsbDebuggingManager;)Ljava/lang/Thread;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Thread;->join()V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 233
    goto :goto_0

    .line 232
    :catch_0
    move-exception v0

    .line 235
    :goto_0
    iget-object v0, p0, Lcom/miui/server/MiuiUsbService$UsbDebuggingManager$UsbDebuggingHandler;->this$1:Lcom/miui/server/MiuiUsbService$UsbDebuggingManager;

    const/4 v1, 0x0

    invoke-static {v0, v1}, Lcom/miui/server/MiuiUsbService$UsbDebuggingManager;->-$$Nest$fputmThread(Lcom/miui/server/MiuiUsbService$UsbDebuggingManager;Ljava/lang/Thread;)V

    .line 236
    iget-object v0, p0, Lcom/miui/server/MiuiUsbService$UsbDebuggingManager$UsbDebuggingHandler;->this$1:Lcom/miui/server/MiuiUsbService$UsbDebuggingManager;

    iput-object v1, v0, Lcom/miui/server/MiuiUsbService$UsbDebuggingManager;->mOutputStream:Ljava/io/OutputStream;

    .line 237
    iget-object v0, p0, Lcom/miui/server/MiuiUsbService$UsbDebuggingManager$UsbDebuggingHandler;->this$1:Lcom/miui/server/MiuiUsbService$UsbDebuggingManager;

    iput-object v1, v0, Lcom/miui/server/MiuiUsbService$UsbDebuggingManager;->mSocket:Landroid/net/LocalSocket;

    .line 238
    goto :goto_1

    .line 213
    :pswitch_4
    iget-object v0, p0, Lcom/miui/server/MiuiUsbService$UsbDebuggingManager$UsbDebuggingHandler;->this$1:Lcom/miui/server/MiuiUsbService$UsbDebuggingManager;

    invoke-static {v0}, Lcom/miui/server/MiuiUsbService$UsbDebuggingManager;->-$$Nest$fgetmAdbEnabled(Lcom/miui/server/MiuiUsbService$UsbDebuggingManager;)Z

    move-result v0

    if-eqz v0, :cond_3

    .line 214
    goto :goto_1

    .line 216
    :cond_3
    iget-object v0, p0, Lcom/miui/server/MiuiUsbService$UsbDebuggingManager$UsbDebuggingHandler;->this$1:Lcom/miui/server/MiuiUsbService$UsbDebuggingManager;

    invoke-static {v0, v1}, Lcom/miui/server/MiuiUsbService$UsbDebuggingManager;->-$$Nest$fputmAdbEnabled(Lcom/miui/server/MiuiUsbService$UsbDebuggingManager;Z)V

    .line 218
    iget-object v0, p0, Lcom/miui/server/MiuiUsbService$UsbDebuggingManager$UsbDebuggingHandler;->this$1:Lcom/miui/server/MiuiUsbService$UsbDebuggingManager;

    new-instance v1, Ljava/lang/Thread;

    iget-object v2, p0, Lcom/miui/server/MiuiUsbService$UsbDebuggingManager$UsbDebuggingHandler;->this$1:Lcom/miui/server/MiuiUsbService$UsbDebuggingManager;

    invoke-direct {v1, v2}, Ljava/lang/Thread;-><init>(Ljava/lang/Runnable;)V

    invoke-static {v0, v1}, Lcom/miui/server/MiuiUsbService$UsbDebuggingManager;->-$$Nest$fputmThread(Lcom/miui/server/MiuiUsbService$UsbDebuggingManager;Ljava/lang/Thread;)V

    .line 219
    iget-object v0, p0, Lcom/miui/server/MiuiUsbService$UsbDebuggingManager$UsbDebuggingHandler;->this$1:Lcom/miui/server/MiuiUsbService$UsbDebuggingManager;

    invoke-static {v0}, Lcom/miui/server/MiuiUsbService$UsbDebuggingManager;->-$$Nest$fgetmThread(Lcom/miui/server/MiuiUsbService$UsbDebuggingManager;)Ljava/lang/Thread;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Thread;->start()V

    .line 221
    nop

    .line 269
    :goto_1
    return-void

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_4
        :pswitch_3
        :pswitch_2
        :pswitch_1
        :pswitch_0
    .end packed-switch
.end method
