class com.miui.server.smartpower.PowerFrozenManager$FrozenInfo {
	 /* .source "PowerFrozenManager.java" */
	 /* # annotations */
	 /* .annotation system Ldalvik/annotation/EnclosingClass; */
	 /* value = Lcom/miui/server/smartpower/PowerFrozenManager; */
} // .end annotation
/* .annotation system Ldalvik/annotation/InnerClass; */
/* accessFlags = 0x8 */
/* name = "FrozenInfo" */
} // .end annotation
/* # instance fields */
private java.util.List mFrozenReasons;
/* .annotation system Ldalvik/annotation/Signature; */
/* value = { */
/* "Ljava/util/List<", */
/* "Ljava/lang/String;", */
/* ">;" */
/* } */
} // .end annotation
} // .end field
private java.util.List mFrozenTimes;
/* .annotation system Ldalvik/annotation/Signature; */
/* value = { */
/* "Ljava/util/List<", */
/* "Ljava/lang/Long;", */
/* ">;" */
/* } */
} // .end annotation
} // .end field
private Integer mPid;
private java.lang.String mProcessName;
private java.lang.String mThawReason;
private Long mThawTime;
private Long mThawUptime;
private Integer mUid;
/* # direct methods */
static java.util.List -$$Nest$fgetmFrozenReasons ( com.miui.server.smartpower.PowerFrozenManager$FrozenInfo p0 ) { //bridge//synthethic
/* .locals 0 */
p0 = this.mFrozenReasons;
} // .end method
static java.util.List -$$Nest$fgetmFrozenTimes ( com.miui.server.smartpower.PowerFrozenManager$FrozenInfo p0 ) { //bridge//synthethic
/* .locals 0 */
p0 = this.mFrozenTimes;
} // .end method
static Integer -$$Nest$fgetmPid ( com.miui.server.smartpower.PowerFrozenManager$FrozenInfo p0 ) { //bridge//synthethic
/* .locals 0 */
/* iget p0, p0, Lcom/miui/server/smartpower/PowerFrozenManager$FrozenInfo;->mPid:I */
} // .end method
static java.lang.String -$$Nest$fgetmThawReason ( com.miui.server.smartpower.PowerFrozenManager$FrozenInfo p0 ) { //bridge//synthethic
/* .locals 0 */
p0 = this.mThawReason;
} // .end method
static Long -$$Nest$fgetmThawTime ( com.miui.server.smartpower.PowerFrozenManager$FrozenInfo p0 ) { //bridge//synthethic
/* .locals 2 */
/* iget-wide v0, p0, Lcom/miui/server/smartpower/PowerFrozenManager$FrozenInfo;->mThawTime:J */
/* return-wide v0 */
} // .end method
static Integer -$$Nest$fgetmUid ( com.miui.server.smartpower.PowerFrozenManager$FrozenInfo p0 ) { //bridge//synthethic
/* .locals 0 */
/* iget p0, p0, Lcom/miui/server/smartpower/PowerFrozenManager$FrozenInfo;->mUid:I */
} // .end method
static void -$$Nest$fputmThawReason ( com.miui.server.smartpower.PowerFrozenManager$FrozenInfo p0, java.lang.String p1 ) { //bridge//synthethic
/* .locals 0 */
this.mThawReason = p1;
return;
} // .end method
static void -$$Nest$fputmThawTime ( com.miui.server.smartpower.PowerFrozenManager$FrozenInfo p0, Long p1 ) { //bridge//synthethic
/* .locals 0 */
/* iput-wide p1, p0, Lcom/miui/server/smartpower/PowerFrozenManager$FrozenInfo;->mThawTime:J */
return;
} // .end method
static void -$$Nest$fputmThawUptime ( com.miui.server.smartpower.PowerFrozenManager$FrozenInfo p0, Long p1 ) { //bridge//synthethic
/* .locals 0 */
/* iput-wide p1, p0, Lcom/miui/server/smartpower/PowerFrozenManager$FrozenInfo;->mThawUptime:J */
return;
} // .end method
static void -$$Nest$maddFrozenInfo ( com.miui.server.smartpower.PowerFrozenManager$FrozenInfo p0, Long p1, java.lang.String p2 ) { //bridge//synthethic
/* .locals 0 */
/* invoke-direct {p0, p1, p2, p3}, Lcom/miui/server/smartpower/PowerFrozenManager$FrozenInfo;->addFrozenInfo(JLjava/lang/String;)V */
return;
} // .end method
static Long -$$Nest$mgetFrozenDuration ( com.miui.server.smartpower.PowerFrozenManager$FrozenInfo p0 ) { //bridge//synthethic
/* .locals 2 */
/* invoke-direct {p0}, Lcom/miui/server/smartpower/PowerFrozenManager$FrozenInfo;->getFrozenDuration()J */
/* move-result-wide v0 */
/* return-wide v0 */
} // .end method
private com.miui.server.smartpower.PowerFrozenManager$FrozenInfo ( ) {
/* .locals 2 */
/* .param p1, "uid" # I */
/* .param p2, "pid" # I */
/* .param p3, "processName" # Ljava/lang/String; */
/* .line 497 */
/* invoke-direct {p0}, Ljava/lang/Object;-><init>()V */
/* .line 494 */
/* new-instance v0, Ljava/util/ArrayList; */
/* const/16 v1, 0x10 */
/* invoke-direct {v0, v1}, Ljava/util/ArrayList;-><init>(I)V */
this.mFrozenTimes = v0;
/* .line 495 */
/* new-instance v0, Ljava/util/ArrayList; */
/* invoke-direct {v0, v1}, Ljava/util/ArrayList;-><init>(I)V */
this.mFrozenReasons = v0;
/* .line 498 */
/* iput p1, p0, Lcom/miui/server/smartpower/PowerFrozenManager$FrozenInfo;->mUid:I */
/* .line 499 */
/* iput p2, p0, Lcom/miui/server/smartpower/PowerFrozenManager$FrozenInfo;->mPid:I */
/* .line 500 */
this.mProcessName = p3;
/* .line 501 */
return;
} // .end method
 com.miui.server.smartpower.PowerFrozenManager$FrozenInfo ( ) { //synthethic
/* .locals 0 */
/* invoke-direct {p0, p1, p2, p3}, Lcom/miui/server/smartpower/PowerFrozenManager$FrozenInfo;-><init>(IILjava/lang/String;)V */
return;
} // .end method
private void addFrozenInfo ( Long p0, java.lang.String p1 ) {
/* .locals 2 */
/* .param p1, "curTime" # J */
/* .param p3, "reason" # Ljava/lang/String; */
/* .line 504 */
v0 = this.mFrozenTimes;
java.lang.Long .valueOf ( p1,p2 );
/* .line 505 */
v0 = this.mFrozenReasons;
/* .line 506 */
return;
} // .end method
private Long getEndTime ( ) {
/* .locals 2 */
/* .line 517 */
/* iget-wide v0, p0, Lcom/miui/server/smartpower/PowerFrozenManager$FrozenInfo;->mThawTime:J */
/* return-wide v0 */
} // .end method
private Long getFrozenDuration ( ) {
/* .locals 4 */
/* .line 521 */
/* invoke-direct {p0}, Lcom/miui/server/smartpower/PowerFrozenManager$FrozenInfo;->getStartTime()J */
/* move-result-wide v0 */
/* invoke-direct {p0}, Lcom/miui/server/smartpower/PowerFrozenManager$FrozenInfo;->getEndTime()J */
/* move-result-wide v2 */
/* cmp-long v0, v0, v2 */
/* if-gez v0, :cond_0 */
/* .line 522 */
/* invoke-direct {p0}, Lcom/miui/server/smartpower/PowerFrozenManager$FrozenInfo;->getEndTime()J */
/* move-result-wide v0 */
/* invoke-direct {p0}, Lcom/miui/server/smartpower/PowerFrozenManager$FrozenInfo;->getStartTime()J */
/* move-result-wide v2 */
/* sub-long/2addr v0, v2 */
/* return-wide v0 */
/* .line 524 */
} // :cond_0
/* const-wide/16 v0, 0x0 */
/* return-wide v0 */
} // .end method
private Long getStartTime ( ) {
/* .locals 2 */
/* .line 509 */
v0 = v0 = this.mFrozenTimes;
/* if-nez v0, :cond_0 */
/* .line 510 */
/* const-wide/16 v0, 0x0 */
/* return-wide v0 */
/* .line 512 */
} // :cond_0
v0 = this.mFrozenTimes;
int v1 = 0; // const/4 v1, 0x0
/* check-cast v0, Ljava/lang/Long; */
(( java.lang.Long ) v0 ).longValue ( ); // invoke-virtual {v0}, Ljava/lang/Long;->longValue()J
/* move-result-wide v0 */
/* return-wide v0 */
} // .end method
/* # virtual methods */
public java.lang.String toString ( ) {
/* .locals 2 */
/* .line 529 */
/* new-instance v0, Ljava/lang/StringBuilder; */
/* invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V */
final String v1 = "("; // const-string v1, "("
(( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
/* iget v1, p0, Lcom/miui/server/smartpower/PowerFrozenManager$FrozenInfo;->mPid:I */
(( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;
final String v1 = ")"; // const-string v1, ")"
(( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
v1 = this.mProcessName;
(( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
final String v1 = "/"; // const-string v1, "/"
(( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
/* iget v1, p0, Lcom/miui/server/smartpower/PowerFrozenManager$FrozenInfo;->mUid:I */
(( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v0 ).toString ( ); // invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
} // .end method
