.class Lcom/miui/server/smartpower/SmartWindowPolicyManager$MultiTaskPolicyController;
.super Ljava/lang/Object;
.source "SmartWindowPolicyManager.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/miui/server/smartpower/SmartWindowPolicyManager;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "MultiTaskPolicyController"
.end annotation

.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/miui/server/smartpower/SmartWindowPolicyManager$MultiTaskPolicyController$MultiTaskActionListenerDelegate;
    }
.end annotation


# instance fields
.field private final mLock:Ljava/lang/Object;

.field private final mMultiTaskActionListeners:Landroid/util/SparseArray;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/util/SparseArray<",
            "Lcom/miui/server/smartpower/SmartWindowPolicyManager$MultiTaskPolicyController$MultiTaskActionListenerDelegate;",
            ">;"
        }
    .end annotation
.end field

.field final synthetic this$0:Lcom/miui/server/smartpower/SmartWindowPolicyManager;


# direct methods
.method private constructor <init>(Lcom/miui/server/smartpower/SmartWindowPolicyManager;)V
    .locals 1

    .line 176
    iput-object p1, p0, Lcom/miui/server/smartpower/SmartWindowPolicyManager$MultiTaskPolicyController;->this$0:Lcom/miui/server/smartpower/SmartWindowPolicyManager;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 171
    new-instance p1, Ljava/lang/Object;

    invoke-direct {p1}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lcom/miui/server/smartpower/SmartWindowPolicyManager$MultiTaskPolicyController;->mLock:Ljava/lang/Object;

    .line 173
    new-instance p1, Landroid/util/SparseArray;

    const/4 v0, 0x4

    invoke-direct {p1, v0}, Landroid/util/SparseArray;-><init>(I)V

    iput-object p1, p0, Lcom/miui/server/smartpower/SmartWindowPolicyManager$MultiTaskPolicyController;->mMultiTaskActionListeners:Landroid/util/SparseArray;

    .line 177
    return-void
.end method

.method synthetic constructor <init>(Lcom/miui/server/smartpower/SmartWindowPolicyManager;Lcom/miui/server/smartpower/SmartWindowPolicyManager$MultiTaskPolicyController-IA;)V
    .locals 0

    invoke-direct {p0, p1}, Lcom/miui/server/smartpower/SmartWindowPolicyManager$MultiTaskPolicyController;-><init>(Lcom/miui/server/smartpower/SmartWindowPolicyManager;)V

    return-void
.end method

.method private isValidMultiTaskListenerFlag(I)Z
    .locals 1
    .param p1, "listenerFlag"    # I

    .line 268
    if-lez p1, :cond_0

    const/4 v0, 0x3

    if-ge p1, v0, :cond_0

    const/4 v0, 0x1

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    :goto_0
    return v0
.end method

.method private listenerFlagToString(I)Ljava/lang/String;
    .locals 1
    .param p1, "listenerFlag"    # I

    .line 273
    packed-switch p1, :pswitch_data_0

    .line 279
    const-string/jumbo v0, "unknown listener"

    return-object v0

    .line 277
    :pswitch_0
    const-string v0, "multi scene"

    return-object v0

    .line 275
    :pswitch_1
    const-string v0, "sched boost"

    return-object v0

    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_1
        :pswitch_0
    .end packed-switch
.end method


# virtual methods
.method public dispatchMultiTaskActionEvent(ILmiui/smartpower/MultiTaskActionManager$ActionInfo;)V
    .locals 4
    .param p1, "event"    # I
    .param p2, "actionInfo"    # Lmiui/smartpower/MultiTaskActionManager$ActionInfo;

    .line 258
    iget-object v0, p0, Lcom/miui/server/smartpower/SmartWindowPolicyManager$MultiTaskPolicyController;->mLock:Ljava/lang/Object;

    monitor-enter v0

    .line 259
    :try_start_0
    iget-object v1, p0, Lcom/miui/server/smartpower/SmartWindowPolicyManager$MultiTaskPolicyController;->mMultiTaskActionListeners:Landroid/util/SparseArray;

    invoke-virtual {v1}, Landroid/util/SparseArray;->size()I

    move-result v1

    .line 260
    .local v1, "numListeners":I
    const/4 v2, 0x0

    .local v2, "i":I
    :goto_0
    if-ge v2, v1, :cond_0

    .line 261
    iget-object v3, p0, Lcom/miui/server/smartpower/SmartWindowPolicyManager$MultiTaskPolicyController;->mMultiTaskActionListeners:Landroid/util/SparseArray;

    invoke-virtual {v3, v2}, Landroid/util/SparseArray;->valueAt(I)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/miui/server/smartpower/SmartWindowPolicyManager$MultiTaskPolicyController$MultiTaskActionListenerDelegate;

    .line 262
    invoke-virtual {v3, p1, p2}, Lcom/miui/server/smartpower/SmartWindowPolicyManager$MultiTaskPolicyController$MultiTaskActionListenerDelegate;->sendMultiTaskActionEvent(ILmiui/smartpower/MultiTaskActionManager$ActionInfo;)V

    .line 260
    add-int/lit8 v2, v2, 0x1

    goto :goto_0

    .line 264
    .end local v1    # "numListeners":I
    .end local v2    # "i":I
    :cond_0
    monitor-exit v0

    .line 265
    return-void

    .line 264
    :catchall_0
    move-exception v1

    monitor-exit v0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v1
.end method

.method public dump(Ljava/io/PrintWriter;)V
    .locals 3
    .param p1, "pw"    # Ljava/io/PrintWriter;

    .line 331
    const-string v0, "MultiTaskPolicyController Dump:"

    invoke-virtual {p1, v0}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V

    .line 332
    const-string v0, "    Multi-Task Action Listeners:"

    invoke-virtual {p1, v0}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V

    .line 333
    iget-object v0, p0, Lcom/miui/server/smartpower/SmartWindowPolicyManager$MultiTaskPolicyController;->mMultiTaskActionListeners:Landroid/util/SparseArray;

    invoke-virtual {v0}, Landroid/util/SparseArray;->size()I

    move-result v0

    if-nez v0, :cond_0

    .line 334
    const-string v0, "    None"

    invoke-virtual {p1, v0}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V

    goto :goto_1

    .line 336
    :cond_0
    const/4 v0, 0x0

    .local v0, "i":I
    :goto_0
    iget-object v1, p0, Lcom/miui/server/smartpower/SmartWindowPolicyManager$MultiTaskPolicyController;->mMultiTaskActionListeners:Landroid/util/SparseArray;

    invoke-virtual {v1}, Landroid/util/SparseArray;->size()I

    move-result v1

    if-ge v0, v1, :cond_1

    .line 337
    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    iget-object v2, p0, Lcom/miui/server/smartpower/SmartWindowPolicyManager$MultiTaskPolicyController;->mMultiTaskActionListeners:Landroid/util/SparseArray;

    .line 338
    invoke-virtual {v2, v0}, Landroid/util/SparseArray;->valueAt(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/miui/server/smartpower/SmartWindowPolicyManager$MultiTaskPolicyController$MultiTaskActionListenerDelegate;

    invoke-virtual {v2}, Lcom/miui/server/smartpower/SmartWindowPolicyManager$MultiTaskPolicyController$MultiTaskActionListenerDelegate;->getSourceName()Ljava/lang/String;

    move-result-object v2

    filled-new-array {v1, v2}, [Ljava/lang/Object;

    move-result-object v1

    .line 337
    const-string v2, "     [%d]: %s\n"

    invoke-virtual {p1, v2, v1}, Ljava/io/PrintWriter;->printf(Ljava/lang/String;[Ljava/lang/Object;)Ljava/io/PrintWriter;

    .line 336
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 341
    .end local v0    # "i":I
    :cond_1
    :goto_1
    return-void
.end method

.method public notifyMultiTaskActionChanged(ILmiui/smartpower/MultiTaskActionManager$ActionInfo;)V
    .locals 4
    .param p1, "event"    # I
    .param p2, "actionInfo"    # Lmiui/smartpower/MultiTaskActionManager$ActionInfo;

    .line 238
    invoke-virtual {p2}, Lmiui/smartpower/MultiTaskActionManager$ActionInfo;->getDrawnTids()[I

    move-result-object v0

    .line 239
    .local v0, "tids":[I
    const/4 v1, 0x1

    .line 240
    .local v1, "boostType":I
    invoke-virtual {p2}, Lmiui/smartpower/MultiTaskActionManager$ActionInfo;->getCallingPid()I

    move-result v2

    .line 242
    .local v2, "callingPid":I
    const/4 v3, 0x1

    if-ne p1, v3, :cond_0

    .line 244
    iget-object v3, p0, Lcom/miui/server/smartpower/SmartWindowPolicyManager$MultiTaskPolicyController;->this$0:Lcom/miui/server/smartpower/SmartWindowPolicyManager;

    invoke-static {v3}, Lcom/miui/server/smartpower/SmartWindowPolicyManager;->-$$Nest$fgetmSmartDisplayPolicyManager(Lcom/miui/server/smartpower/SmartWindowPolicyManager;)Lcom/miui/server/smartpower/SmartDisplayPolicyManager;

    move-result-object v3

    invoke-virtual {v3, p2}, Lcom/miui/server/smartpower/SmartDisplayPolicyManager;->notifyMultiTaskActionStart(Lmiui/smartpower/MultiTaskActionManager$ActionInfo;)V

    .line 246
    iget-object v3, p0, Lcom/miui/server/smartpower/SmartWindowPolicyManager$MultiTaskPolicyController;->this$0:Lcom/miui/server/smartpower/SmartWindowPolicyManager;

    invoke-static {v3}, Lcom/miui/server/smartpower/SmartWindowPolicyManager;->-$$Nest$fgetmSmartBoostPolicyManager(Lcom/miui/server/smartpower/SmartWindowPolicyManager;)Lcom/miui/server/smartpower/SmartBoostPolicyManager;

    move-result-object v3

    invoke-virtual {v3, v0, v2, v1}, Lcom/miui/server/smartpower/SmartBoostPolicyManager;->beginSchedThreads([III)V

    goto :goto_0

    .line 247
    :cond_0
    const/4 v3, 0x2

    if-ne p1, v3, :cond_1

    .line 249
    iget-object v3, p0, Lcom/miui/server/smartpower/SmartWindowPolicyManager$MultiTaskPolicyController;->this$0:Lcom/miui/server/smartpower/SmartWindowPolicyManager;

    invoke-static {v3}, Lcom/miui/server/smartpower/SmartWindowPolicyManager;->-$$Nest$fgetmSmartDisplayPolicyManager(Lcom/miui/server/smartpower/SmartWindowPolicyManager;)Lcom/miui/server/smartpower/SmartDisplayPolicyManager;

    move-result-object v3

    invoke-virtual {v3, p2}, Lcom/miui/server/smartpower/SmartDisplayPolicyManager;->notifyMultiTaskActionEnd(Lmiui/smartpower/MultiTaskActionManager$ActionInfo;)V

    .line 251
    iget-object v3, p0, Lcom/miui/server/smartpower/SmartWindowPolicyManager$MultiTaskPolicyController;->this$0:Lcom/miui/server/smartpower/SmartWindowPolicyManager;

    invoke-static {v3}, Lcom/miui/server/smartpower/SmartWindowPolicyManager;->-$$Nest$fgetmSmartBoostPolicyManager(Lcom/miui/server/smartpower/SmartWindowPolicyManager;)Lcom/miui/server/smartpower/SmartBoostPolicyManager;

    move-result-object v3

    invoke-virtual {v3, v0, v2, v1}, Lcom/miui/server/smartpower/SmartBoostPolicyManager;->stopCurrentSchedBoost([III)V

    .line 254
    :cond_1
    :goto_0
    invoke-virtual {p0, p1, p2}, Lcom/miui/server/smartpower/SmartWindowPolicyManager$MultiTaskPolicyController;->dispatchMultiTaskActionEvent(ILmiui/smartpower/MultiTaskActionManager$ActionInfo;)V

    .line 255
    return-void
.end method

.method public registerMultiTaskActionListener(ILandroid/os/Handler;Lcom/miui/app/smartpower/SmartPowerServiceInternal$MultiTaskActionListener;)Z
    .locals 5
    .param p1, "listenerFlag"    # I
    .param p2, "handler"    # Landroid/os/Handler;
    .param p3, "listener"    # Lcom/miui/app/smartpower/SmartPowerServiceInternal$MultiTaskActionListener;

    .line 181
    iget-object v0, p0, Lcom/miui/server/smartpower/SmartWindowPolicyManager$MultiTaskPolicyController;->mLock:Ljava/lang/Object;

    monitor-enter v0

    .line 182
    :try_start_0
    invoke-direct {p0, p1}, Lcom/miui/server/smartpower/SmartWindowPolicyManager$MultiTaskPolicyController;->isValidMultiTaskListenerFlag(I)Z

    move-result v1

    const/4 v2, 0x0

    if-nez v1, :cond_0

    .line 183
    const-string v1, "SmartPower.WindowPolicy"

    const-string/jumbo v3, "the listener flag is invalid."

    invoke-static {v1, v3}, Landroid/util/Slog;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 184
    monitor-exit v0

    return v2

    .line 187
    :cond_0
    if-eqz p2, :cond_3

    if-nez p3, :cond_1

    goto :goto_0

    .line 192
    :cond_1
    invoke-virtual {p2}, Landroid/os/Handler;->getLooper()Landroid/os/Looper;

    move-result-object v1

    .line 193
    .local v1, "looper":Landroid/os/Looper;
    if-nez v1, :cond_2

    .line 194
    const-string v3, "SmartPower.WindowPolicy"

    const-string/jumbo v4, "unable to obtain the loop corresponding to the handler."

    invoke-static {v3, v4}, Landroid/util/Slog;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 195
    monitor-exit v0

    return v2

    .line 198
    :cond_2
    new-instance v2, Lcom/miui/server/smartpower/SmartWindowPolicyManager$MultiTaskPolicyController$MultiTaskActionListenerDelegate;

    .line 200
    invoke-direct {p0, p1}, Lcom/miui/server/smartpower/SmartWindowPolicyManager$MultiTaskPolicyController;->listenerFlagToString(I)Ljava/lang/String;

    move-result-object v3

    invoke-direct {v2, v3, v1, p3}, Lcom/miui/server/smartpower/SmartWindowPolicyManager$MultiTaskPolicyController$MultiTaskActionListenerDelegate;-><init>(Ljava/lang/String;Landroid/os/Looper;Lcom/miui/app/smartpower/SmartPowerServiceInternal$MultiTaskActionListener;)V

    .line 203
    .local v2, "listenerDelegate":Lcom/miui/server/smartpower/SmartWindowPolicyManager$MultiTaskPolicyController$MultiTaskActionListenerDelegate;
    iget-object v3, p0, Lcom/miui/server/smartpower/SmartWindowPolicyManager$MultiTaskPolicyController;->mMultiTaskActionListeners:Landroid/util/SparseArray;

    invoke-virtual {v3, p1, v2}, Landroid/util/SparseArray;->put(ILjava/lang/Object;)V

    .line 204
    monitor-exit v0

    const/4 v0, 0x1

    return v0

    .line 188
    .end local v1    # "looper":Landroid/os/Looper;
    .end local v2    # "listenerDelegate":Lcom/miui/server/smartpower/SmartWindowPolicyManager$MultiTaskPolicyController$MultiTaskActionListenerDelegate;
    :cond_3
    :goto_0
    const-string v1, "SmartPower.WindowPolicy"

    const-string v3, "listener or handler must not be null."

    invoke-static {v1, v3}, Landroid/util/Slog;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 189
    monitor-exit v0

    return v2

    .line 205
    :catchall_0
    move-exception v1

    monitor-exit v0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v1
.end method

.method public unregisterMultiTaskActionListener(ILcom/miui/app/smartpower/SmartPowerServiceInternal$MultiTaskActionListener;)Z
    .locals 5
    .param p1, "listenerFlag"    # I
    .param p2, "listener"    # Lcom/miui/app/smartpower/SmartPowerServiceInternal$MultiTaskActionListener;

    .line 210
    iget-object v0, p0, Lcom/miui/server/smartpower/SmartWindowPolicyManager$MultiTaskPolicyController;->mLock:Ljava/lang/Object;

    monitor-enter v0

    .line 211
    :try_start_0
    invoke-direct {p0, p1}, Lcom/miui/server/smartpower/SmartWindowPolicyManager$MultiTaskPolicyController;->isValidMultiTaskListenerFlag(I)Z

    move-result v1

    const/4 v2, 0x0

    if-nez v1, :cond_0

    .line 212
    const-string v1, "SmartPower.WindowPolicy"

    const-string/jumbo v3, "the listener flag is invalid."

    invoke-static {v1, v3}, Landroid/util/Slog;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 213
    monitor-exit v0

    return v2

    .line 216
    :cond_0
    if-nez p2, :cond_1

    .line 217
    const-string v1, "SmartPower.WindowPolicy"

    const-string v3, "listener must not be null."

    invoke-static {v1, v3}, Landroid/util/Slog;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 218
    monitor-exit v0

    return v2

    .line 221
    :cond_1
    iget-object v1, p0, Lcom/miui/server/smartpower/SmartWindowPolicyManager$MultiTaskPolicyController;->mMultiTaskActionListeners:Landroid/util/SparseArray;

    .line 222
    invoke-virtual {v1, p1}, Landroid/util/SparseArray;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/miui/server/smartpower/SmartWindowPolicyManager$MultiTaskPolicyController$MultiTaskActionListenerDelegate;

    .line 223
    .local v1, "listenerDelegate":Lcom/miui/server/smartpower/SmartWindowPolicyManager$MultiTaskPolicyController$MultiTaskActionListenerDelegate;
    if-nez v1, :cond_2

    .line 224
    monitor-exit v0

    return v2

    .line 226
    :cond_2
    invoke-virtual {v1}, Lcom/miui/server/smartpower/SmartWindowPolicyManager$MultiTaskPolicyController$MultiTaskActionListenerDelegate;->getListener()Lcom/miui/app/smartpower/SmartPowerServiceInternal$MultiTaskActionListener;

    move-result-object v3

    if-eq v3, p2, :cond_3

    .line 227
    const-string v3, "SmartPower.WindowPolicy"

    const-string/jumbo v4, "the listener flag is not match the listener."

    invoke-static {v3, v4}, Landroid/util/Slog;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 228
    monitor-exit v0

    return v2

    .line 231
    :cond_3
    invoke-virtual {v1}, Lcom/miui/server/smartpower/SmartWindowPolicyManager$MultiTaskPolicyController$MultiTaskActionListenerDelegate;->clearEvents()V

    .line 232
    iget-object v2, p0, Lcom/miui/server/smartpower/SmartWindowPolicyManager$MultiTaskPolicyController;->mMultiTaskActionListeners:Landroid/util/SparseArray;

    invoke-virtual {v2, p1}, Landroid/util/SparseArray;->remove(I)V

    .line 233
    monitor-exit v0

    const/4 v0, 0x1

    return v0

    .line 234
    .end local v1    # "listenerDelegate":Lcom/miui/server/smartpower/SmartWindowPolicyManager$MultiTaskPolicyController$MultiTaskActionListenerDelegate;
    :catchall_0
    move-exception v1

    monitor-exit v0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v1
.end method
