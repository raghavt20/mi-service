class com.miui.server.smartpower.SmartDisplayPolicyManager$RefreshRatePolicyController$SettingRefreshRateObserver extends android.database.ContentObserver {
	 /* .source "SmartDisplayPolicyManager.java" */
	 /* # annotations */
	 /* .annotation system Ldalvik/annotation/EnclosingClass; */
	 /* value = Lcom/miui/server/smartpower/SmartDisplayPolicyManager$RefreshRatePolicyController; */
} // .end annotation
/* .annotation system Ldalvik/annotation/InnerClass; */
/* accessFlags = 0x2 */
/* name = "SettingRefreshRateObserver" */
} // .end annotation
/* # instance fields */
private final android.content.ContentResolver mContentResolver;
private final android.content.Context mContext;
private final android.net.Uri mMiuiDefaultRefreshRateSetting;
private final android.net.Uri mMiuiRefreshRateSetting;
private final android.net.Uri mMiuiUserRefreshRateSetting;
private final android.net.Uri mThermalRefreshRateSetting;
final com.miui.server.smartpower.SmartDisplayPolicyManager$RefreshRatePolicyController this$1; //synthetic
/* # direct methods */
public com.miui.server.smartpower.SmartDisplayPolicyManager$RefreshRatePolicyController$SettingRefreshRateObserver ( ) {
/* .locals 5 */
/* .param p2, "context" # Landroid/content/Context; */
/* .param p3, "handler" # Landroid/os/Handler; */
/* .line 1479 */
this.this$1 = p1;
/* .line 1480 */
/* invoke-direct {p0, p3}, Landroid/database/ContentObserver;-><init>(Landroid/os/Handler;)V */
/* .line 1468 */
/* nop */
/* .line 1469 */
final String p1 = "is_smart_fps"; // const-string p1, "is_smart_fps"
android.provider.Settings$System .getUriFor ( p1 );
this.mMiuiDefaultRefreshRateSetting = p1;
/* .line 1472 */
/* nop */
/* .line 1473 */
/* const-string/jumbo v0, "user_refresh_rate" */
android.provider.Settings$Secure .getUriFor ( v0 );
this.mMiuiUserRefreshRateSetting = v0;
/* .line 1474 */
/* nop */
/* .line 1475 */
final String v1 = "miui_refresh_rate"; // const-string v1, "miui_refresh_rate"
android.provider.Settings$Secure .getUriFor ( v1 );
this.mMiuiRefreshRateSetting = v1;
/* .line 1476 */
/* nop */
/* .line 1477 */
/* const-string/jumbo v2, "thermal_limit_refresh_rate" */
android.provider.Settings$System .getUriFor ( v2 );
this.mThermalRefreshRateSetting = v2;
/* .line 1481 */
this.mContext = p2;
/* .line 1482 */
(( android.content.Context ) p2 ).getContentResolver ( ); // invoke-virtual {p2}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;
this.mContentResolver = v3;
/* .line 1484 */
int v4 = 0; // const/4 v4, 0x0
(( android.content.ContentResolver ) v3 ).registerContentObserver ( p1, v4, p0, v4 ); // invoke-virtual {v3, p1, v4, p0, v4}, Landroid/content/ContentResolver;->registerContentObserver(Landroid/net/Uri;ZLandroid/database/ContentObserver;I)V
/* .line 1486 */
(( android.content.ContentResolver ) v3 ).registerContentObserver ( v0, v4, p0, v4 ); // invoke-virtual {v3, v0, v4, p0, v4}, Landroid/content/ContentResolver;->registerContentObserver(Landroid/net/Uri;ZLandroid/database/ContentObserver;I)V
/* .line 1488 */
(( android.content.ContentResolver ) v3 ).registerContentObserver ( v1, v4, p0, v4 ); // invoke-virtual {v3, v1, v4, p0, v4}, Landroid/content/ContentResolver;->registerContentObserver(Landroid/net/Uri;ZLandroid/database/ContentObserver;I)V
/* .line 1490 */
(( android.content.ContentResolver ) v3 ).registerContentObserver ( v2, v4, p0, v4 ); // invoke-virtual {v3, v2, v4, p0, v4}, Landroid/content/ContentResolver;->registerContentObserver(Landroid/net/Uri;ZLandroid/database/ContentObserver;I)V
/* .line 1492 */
return;
} // .end method
private void updateMiuiRefreshRateStatus ( ) {
/* .locals 4 */
/* .line 1512 */
v0 = this.this$1;
v1 = this.mContentResolver;
final String v2 = "miui_refresh_rate"; // const-string v2, "miui_refresh_rate"
int v3 = 0; // const/4 v3, 0x0
v1 = android.provider.Settings$Secure .getInt ( v1,v2,v3 );
com.miui.server.smartpower.SmartDisplayPolicyManager$RefreshRatePolicyController .-$$Nest$fputmMiuiRefreshRate ( v0,v1 );
/* .line 1513 */
return;
} // .end method
private void updateSettingRefreshRateStatus ( ) {
/* .locals 1 */
/* .line 1526 */
v0 = this.this$1;
com.miui.server.smartpower.SmartDisplayPolicyManager$RefreshRatePolicyController .-$$Nest$mupdateAppropriateRefreshRate ( v0 );
/* .line 1527 */
return;
} // .end method
private void updateThermalRefreshRateStatus ( ) {
/* .locals 4 */
/* .line 1516 */
v0 = this.mContentResolver;
/* const-string/jumbo v1, "thermal_limit_refresh_rate" */
int v2 = 0; // const/4 v2, 0x0
v0 = android.provider.Settings$System .getInt ( v0,v1,v2 );
/* .line 1518 */
/* .local v0, "thermalRefreshRate":I */
v1 = this.this$1;
/* const/16 v3, 0x3c */
/* if-lt v0, v3, :cond_0 */
int v2 = 1; // const/4 v2, 0x1
} // :cond_0
com.miui.server.smartpower.SmartDisplayPolicyManager$RefreshRatePolicyController .-$$Nest$fputisThermalWarning ( v1,v2 );
/* .line 1520 */
/* sget-boolean v1, Lcom/miui/server/smartpower/SmartDisplayPolicyManager;->DEBUG:Z */
if ( v1 != null) { // if-eqz v1, :cond_1
/* .line 1521 */
/* new-instance v1, Ljava/lang/StringBuilder; */
/* invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V */
/* const-string/jumbo v2, "thermal warning changed: " */
(( java.lang.StringBuilder ) v1 ).append ( v2 ); // invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
v2 = this.this$1;
v2 = com.miui.server.smartpower.SmartDisplayPolicyManager$RefreshRatePolicyController .-$$Nest$fgetisThermalWarning ( v2 );
(( java.lang.StringBuilder ) v1 ).append ( v2 ); // invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v1 ).toString ( ); // invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
final String v2 = "SmartPower.DisplayPolicy"; // const-string v2, "SmartPower.DisplayPolicy"
android.util.Slog .d ( v2,v1 );
/* .line 1523 */
} // :cond_1
return;
} // .end method
/* # virtual methods */
public void onChange ( Boolean p0, android.net.Uri p1 ) {
/* .locals 1 */
/* .param p1, "selfChange" # Z */
/* .param p2, "uri" # Landroid/net/Uri; */
/* .line 1496 */
/* invoke-super {p0, p1, p2}, Landroid/database/ContentObserver;->onChange(ZLandroid/net/Uri;)V */
/* .line 1497 */
v0 = this.this$1;
v0 = this.this$0;
v0 = com.miui.server.smartpower.SmartDisplayPolicyManager .-$$Nest$misSupportSmartDisplayPolicy ( v0 );
/* if-nez v0, :cond_0 */
/* .line 1498 */
return;
/* .line 1501 */
} // :cond_0
v0 = this.mThermalRefreshRateSetting;
v0 = (( android.net.Uri ) v0 ).equals ( p2 ); // invoke-virtual {v0, p2}, Landroid/net/Uri;->equals(Ljava/lang/Object;)Z
if ( v0 != null) { // if-eqz v0, :cond_1
/* .line 1502 */
/* invoke-direct {p0}, Lcom/miui/server/smartpower/SmartDisplayPolicyManager$RefreshRatePolicyController$SettingRefreshRateObserver;->updateThermalRefreshRateStatus()V */
/* .line 1503 */
} // :cond_1
v0 = this.mMiuiRefreshRateSetting;
v0 = (( android.net.Uri ) v0 ).equals ( p2 ); // invoke-virtual {v0, p2}, Landroid/net/Uri;->equals(Ljava/lang/Object;)Z
if ( v0 != null) { // if-eqz v0, :cond_2
/* .line 1504 */
/* invoke-direct {p0}, Lcom/miui/server/smartpower/SmartDisplayPolicyManager$RefreshRatePolicyController$SettingRefreshRateObserver;->updateMiuiRefreshRateStatus()V */
/* .line 1505 */
} // :cond_2
v0 = this.mMiuiDefaultRefreshRateSetting;
v0 = (( android.net.Uri ) v0 ).equals ( p2 ); // invoke-virtual {v0, p2}, Landroid/net/Uri;->equals(Ljava/lang/Object;)Z
/* if-nez v0, :cond_3 */
v0 = this.mMiuiUserRefreshRateSetting;
/* .line 1506 */
v0 = (( android.net.Uri ) v0 ).equals ( p2 ); // invoke-virtual {v0, p2}, Landroid/net/Uri;->equals(Ljava/lang/Object;)Z
if ( v0 != null) { // if-eqz v0, :cond_4
/* .line 1507 */
} // :cond_3
/* invoke-direct {p0}, Lcom/miui/server/smartpower/SmartDisplayPolicyManager$RefreshRatePolicyController$SettingRefreshRateObserver;->updateSettingRefreshRateStatus()V */
/* .line 1509 */
} // :cond_4
} // :goto_0
return;
} // .end method
