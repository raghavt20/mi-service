class com.miui.server.smartpower.SmartDisplayPolicyManager$ThermalTempListenerCallback implements com.android.server.am.ThermalTempListener {
	 /* .source "SmartDisplayPolicyManager.java" */
	 /* # interfaces */
	 /* # annotations */
	 /* .annotation system Ldalvik/annotation/EnclosingClass; */
	 /* value = Lcom/miui/server/smartpower/SmartDisplayPolicyManager; */
} // .end annotation
/* .annotation system Ldalvik/annotation/InnerClass; */
/* accessFlags = 0x2 */
/* name = "ThermalTempListenerCallback" */
} // .end annotation
/* # instance fields */
final com.miui.server.smartpower.SmartDisplayPolicyManager this$0; //synthetic
/* # direct methods */
private com.miui.server.smartpower.SmartDisplayPolicyManager$ThermalTempListenerCallback ( ) {
/* .locals 0 */
/* .line 336 */
this.this$0 = p1;
/* invoke-direct {p0}, Ljava/lang/Object;-><init>()V */
return;
} // .end method
 com.miui.server.smartpower.SmartDisplayPolicyManager$ThermalTempListenerCallback ( ) { //synthethic
/* .locals 0 */
/* invoke-direct {p0, p1}, Lcom/miui/server/smartpower/SmartDisplayPolicyManager$ThermalTempListenerCallback;-><init>(Lcom/miui/server/smartpower/SmartDisplayPolicyManager;)V */
return;
} // .end method
/* # virtual methods */
public void onThermalTempChange ( Integer p0 ) {
/* .locals 2 */
/* .param p1, "temp" # I */
/* .line 339 */
v0 = this.this$0;
com.miui.server.smartpower.SmartDisplayPolicyManager .-$$Nest$fputmCurrentTemp ( v0,p1 );
/* .line 340 */
v0 = this.this$0;
v1 = com.miui.server.smartpower.SmartDisplayPolicyManager .-$$Nest$sfgetTHERMAL_TEMP_THRESHOLD ( );
/* if-le p1, v1, :cond_0 */
int v1 = 1; // const/4 v1, 0x1
} // :cond_0
int v1 = 0; // const/4 v1, 0x0
} // :goto_0
com.miui.server.smartpower.SmartDisplayPolicyManager .-$$Nest$fputisTempWarning ( v0,v1 );
/* .line 342 */
/* sget-boolean v0, Lcom/miui/server/smartpower/SmartDisplayPolicyManager;->DEBUG:Z */
if ( v0 != null) { // if-eqz v0, :cond_1
v0 = this.this$0;
v0 = com.miui.server.smartpower.SmartDisplayPolicyManager .-$$Nest$fgetisTempWarning ( v0 );
if ( v0 != null) { // if-eqz v0, :cond_1
/* .line 343 */
/* new-instance v0, Ljava/lang/StringBuilder; */
/* invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V */
final String v1 = "onThermalTempChange, warning temp: "; // const-string v1, "onThermalTempChange, warning temp: "
(( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v0 ).append ( p1 ); // invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v0 ).toString ( ); // invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
final String v1 = "SmartPower.DisplayPolicy"; // const-string v1, "SmartPower.DisplayPolicy"
android.util.Slog .d ( v1,v0 );
/* .line 345 */
} // :cond_1
return;
} // .end method
