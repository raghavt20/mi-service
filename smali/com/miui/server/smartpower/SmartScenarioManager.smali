.class public Lcom/miui/server/smartpower/SmartScenarioManager;
.super Ljava/lang/Object;
.source "SmartScenarioManager.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/miui/server/smartpower/SmartScenarioManager$ScenarioItem;,
        Lcom/miui/server/smartpower/SmartScenarioManager$ClientConfig;,
        Lcom/miui/server/smartpower/SmartScenarioManager$ISmartScenarioCallback;,
        Lcom/miui/server/smartpower/SmartScenarioManager$ScenarioIdInfo;,
        Lcom/miui/server/smartpower/SmartScenarioManager$ScenarioAppChild;
    }
.end annotation


# static fields
.field public static final DEBUG:Z

.field public static final INTERACTIVE_ACTION_TYPE_BACKGROUND:I = 0x8

.field public static final INTERACTIVE_ACTION_TYPE_CAMERA:I = 0x10000

.field public static final INTERACTIVE_ACTION_TYPE_CHARGING:I = 0x800

.field public static final INTERACTIVE_ACTION_TYPE_DOWNLOAD:I = 0x100

.field public static final INTERACTIVE_ACTION_TYPE_EXTERNAL_CASTING:I = 0x4000

.field public static final INTERACTIVE_ACTION_TYPE_FREEFORM:I = 0x10

.field public static final INTERACTIVE_ACTION_TYPE_MUSICPLAY:I = 0x200

.field public static final INTERACTIVE_ACTION_TYPE_NAVIGATION:I = 0x1000

.field public static final INTERACTIVE_ACTION_TYPE_OVERLAY:I = 0x20

.field public static final INTERACTIVE_ACTION_TYPE_SPLIT:I = 0x8000

.field public static final INTERACTIVE_ACTION_TYPE_TOPAPP:I = 0x2

.field public static final INTERACTIVE_ACTION_TYPE_TOPGAME:I = 0x4

.field public static final INTERACTIVE_ACTION_TYPE_VIDEOCALL:I = 0x80

.field public static final INTERACTIVE_ACTION_TYPE_VIDEOPLAY:I = 0x400

.field public static final INTERACTIVE_ACTION_TYPE_VOICECALL:I = 0x40

.field public static final INTERACTIVE_ACTION_TYPE_WIFI_CASTING:I = 0x2000

.field public static final INTERACTIVE_ADDITIONAL_SCENARIO_ACTION_MASK:I = 0x1fff8

.field public static final INTERACTIVE_MAIN_SCENARIO_ACTION_MASK:I = 0x6

.field private static final RESOURCE_SCENARIO_CAMERA:I = 0x20

.field private static final RESOURCE_SCENARIO_DOWNLOAD:I = 0x80

.field private static final RESOURCE_SCENARIO_EXTERNAL_CASTING:I = 0x200

.field private static final RESOURCE_SCENARIO_MUSIC:I = 0x2

.field private static final RESOURCE_SCENARIO_NAVIGATION:I = 0x40

.field private static final RESOURCE_SCENARIO_VIDEO:I = 0x404

.field private static final RESOURCE_SCENARIO_VIDEOCALL:I = 0x30

.field private static final RESOURCE_SCENARIO_VOICECALL:I = 0x10

.field private static final RESOURCE_SCENARIO_WIFI_CASTING:I = 0x100

.field public static final TAG:Ljava/lang/String; = "SmartPower.Scene"


# instance fields
.field private mCharging:Z

.field private final mClients:Landroid/util/ArraySet;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/util/ArraySet<",
            "Lcom/miui/server/smartpower/SmartScenarioManager$ClientConfig;",
            ">;"
        }
    .end annotation
.end field

.field private mContext:Landroid/content/Context;

.field private final mCurrentScenarios:Landroid/util/SparseArray;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/util/SparseArray<",
            "Lcom/miui/server/smartpower/SmartScenarioManager$ScenarioItem;",
            ">;"
        }
    .end annotation
.end field

.field private mHandler:Landroid/os/Handler;

.field private final mInterestBgPkgs:Landroid/util/ArraySet;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/util/ArraySet<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private final mReceiver:Landroid/content/BroadcastReceiver;


# direct methods
.method static bridge synthetic -$$Nest$fgetmCharging(Lcom/miui/server/smartpower/SmartScenarioManager;)Z
    .locals 0

    iget-boolean p0, p0, Lcom/miui/server/smartpower/SmartScenarioManager;->mCharging:Z

    return p0
.end method

.method static bridge synthetic -$$Nest$fgetmClients(Lcom/miui/server/smartpower/SmartScenarioManager;)Landroid/util/ArraySet;
    .locals 0

    iget-object p0, p0, Lcom/miui/server/smartpower/SmartScenarioManager;->mClients:Landroid/util/ArraySet;

    return-object p0
.end method

.method static bridge synthetic -$$Nest$fgetmHandler(Lcom/miui/server/smartpower/SmartScenarioManager;)Landroid/os/Handler;
    .locals 0

    iget-object p0, p0, Lcom/miui/server/smartpower/SmartScenarioManager;->mHandler:Landroid/os/Handler;

    return-object p0
.end method

.method static bridge synthetic -$$Nest$fgetmInterestBgPkgs(Lcom/miui/server/smartpower/SmartScenarioManager;)Landroid/util/ArraySet;
    .locals 0

    iget-object p0, p0, Lcom/miui/server/smartpower/SmartScenarioManager;->mInterestBgPkgs:Landroid/util/ArraySet;

    return-object p0
.end method

.method static bridge synthetic -$$Nest$fputmCharging(Lcom/miui/server/smartpower/SmartScenarioManager;Z)V
    .locals 0

    iput-boolean p1, p0, Lcom/miui/server/smartpower/SmartScenarioManager;->mCharging:Z

    return-void
.end method

.method static constructor <clinit>()V
    .locals 1

    .line 19
    sget-boolean v0, Lcom/android/server/am/SmartPowerService;->DEBUG:Z

    sput-boolean v0, Lcom/miui/server/smartpower/SmartScenarioManager;->DEBUG:Z

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/os/Looper;)V
    .locals 3
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "looper"    # Landroid/os/Looper;

    .line 176
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 154
    new-instance v0, Landroid/util/SparseArray;

    invoke-direct {v0}, Landroid/util/SparseArray;-><init>()V

    iput-object v0, p0, Lcom/miui/server/smartpower/SmartScenarioManager;->mCurrentScenarios:Landroid/util/SparseArray;

    .line 155
    new-instance v0, Landroid/util/ArraySet;

    invoke-direct {v0}, Landroid/util/ArraySet;-><init>()V

    iput-object v0, p0, Lcom/miui/server/smartpower/SmartScenarioManager;->mClients:Landroid/util/ArraySet;

    .line 156
    new-instance v0, Landroid/util/ArraySet;

    invoke-direct {v0}, Landroid/util/ArraySet;-><init>()V

    iput-object v0, p0, Lcom/miui/server/smartpower/SmartScenarioManager;->mInterestBgPkgs:Landroid/util/ArraySet;

    .line 159
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/miui/server/smartpower/SmartScenarioManager;->mCharging:Z

    .line 161
    new-instance v0, Lcom/miui/server/smartpower/SmartScenarioManager$1;

    invoke-direct {v0, p0}, Lcom/miui/server/smartpower/SmartScenarioManager$1;-><init>(Lcom/miui/server/smartpower/SmartScenarioManager;)V

    iput-object v0, p0, Lcom/miui/server/smartpower/SmartScenarioManager;->mReceiver:Landroid/content/BroadcastReceiver;

    .line 177
    iput-object p1, p0, Lcom/miui/server/smartpower/SmartScenarioManager;->mContext:Landroid/content/Context;

    .line 178
    new-instance v1, Landroid/os/Handler;

    invoke-direct {v1, p2}, Landroid/os/Handler;-><init>(Landroid/os/Looper;)V

    iput-object v1, p0, Lcom/miui/server/smartpower/SmartScenarioManager;->mHandler:Landroid/os/Handler;

    .line 179
    new-instance v1, Landroid/content/IntentFilter;

    invoke-direct {v1}, Landroid/content/IntentFilter;-><init>()V

    .line 180
    .local v1, "filter":Landroid/content/IntentFilter;
    const-string v2, "android.intent.action.BATTERY_CHANGED"

    invoke-virtual {v1, v2}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    .line 181
    iget-object v2, p0, Lcom/miui/server/smartpower/SmartScenarioManager;->mContext:Landroid/content/Context;

    invoke-virtual {v2, v0, v1}, Landroid/content/Context;->registerReceiver(Landroid/content/BroadcastReceiver;Landroid/content/IntentFilter;)Landroid/content/Intent;

    .line 182
    return-void
.end method

.method public static actionTypeToString(I)Ljava/lang/String;
    .locals 3
    .param p0, "action"    # I

    .line 47
    const-string v0, ""

    .line 48
    .local v0, "typeStr":Ljava/lang/String;
    and-int/lit8 v1, p0, 0x2

    if-eqz v1, :cond_0

    .line 49
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string/jumbo v2, "top app, "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    .line 51
    :cond_0
    and-int/lit8 v1, p0, 0x4

    if-eqz v1, :cond_1

    .line 52
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string/jumbo v2, "top game, "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    .line 54
    :cond_1
    and-int/lit8 v1, p0, 0x8

    if-eqz v1, :cond_2

    .line 55
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "background, "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    .line 57
    :cond_2
    and-int/lit8 v1, p0, 0x10

    if-eqz v1, :cond_3

    .line 58
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "freeform, "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    .line 60
    :cond_3
    and-int/lit8 v1, p0, 0x20

    if-eqz v1, :cond_4

    .line 61
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "overlay, "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    .line 63
    :cond_4
    and-int/lit8 v1, p0, 0x40

    if-eqz v1, :cond_5

    .line 64
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string/jumbo v2, "voice call, "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    .line 66
    :cond_5
    and-int/lit16 v1, p0, 0x80

    if-eqz v1, :cond_6

    .line 67
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string/jumbo v2, "video call, "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    .line 69
    :cond_6
    and-int/lit16 v1, p0, 0x100

    if-eqz v1, :cond_7

    .line 70
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "download, "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    .line 72
    :cond_7
    and-int/lit16 v1, p0, 0x200

    if-eqz v1, :cond_8

    .line 73
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "music play, "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    .line 75
    :cond_8
    and-int/lit16 v1, p0, 0x400

    if-eqz v1, :cond_9

    .line 76
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string/jumbo v2, "video play, "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    .line 78
    :cond_9
    and-int/lit16 v1, p0, 0x800

    if-eqz v1, :cond_a

    .line 79
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "charging, "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    .line 81
    :cond_a
    and-int/lit16 v1, p0, 0x1000

    if-eqz v1, :cond_b

    .line 82
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "navigation, "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    .line 84
    :cond_b
    and-int/lit16 v1, p0, 0x2000

    if-eqz v1, :cond_c

    .line 85
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string/jumbo v2, "wifi casting, "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    .line 87
    :cond_c
    and-int/lit16 v1, p0, 0x4000

    if-eqz v1, :cond_d

    .line 88
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "external casting, "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    .line 90
    :cond_d
    const v1, 0x8000

    and-int/2addr v1, p0

    if-eqz v1, :cond_e

    .line 91
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string/jumbo v2, "split screen, "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    .line 93
    :cond_e
    const/high16 v1, 0x10000

    and-int/2addr v1, p0

    if-eqz v1, :cond_f

    .line 94
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "camera, "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    .line 96
    :cond_f
    return-object v0
.end method

.method private static behavierContainsScenario(II)Z
    .locals 1
    .param p0, "behavier"    # I
    .param p1, "scenario"    # I

    .line 147
    and-int v0, p0, p1

    if-ne v0, p1, :cond_0

    const/4 v0, 0x1

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    :goto_0
    return v0
.end method

.method public static calculateScenarioAction(I)I
    .locals 2
    .param p0, "behavier"    # I

    .line 121
    const/4 v0, 0x0

    .line 122
    .local v0, "actions":I
    const/16 v1, 0x100

    invoke-static {p0, v1}, Lcom/miui/server/smartpower/SmartScenarioManager;->behavierContainsScenario(II)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 123
    or-int/lit16 v0, v0, 0x2000

    goto :goto_0

    .line 124
    :cond_0
    const/16 v1, 0x200

    invoke-static {p0, v1}, Lcom/miui/server/smartpower/SmartScenarioManager;->behavierContainsScenario(II)Z

    move-result v1

    if-eqz v1, :cond_1

    .line 125
    or-int/lit16 v0, v0, 0x4000

    goto :goto_0

    .line 126
    :cond_1
    const/16 v1, 0x30

    invoke-static {p0, v1}, Lcom/miui/server/smartpower/SmartScenarioManager;->behavierContainsScenario(II)Z

    move-result v1

    if-eqz v1, :cond_2

    .line 127
    or-int/lit16 v0, v0, 0x80

    goto :goto_0

    .line 128
    :cond_2
    const/16 v1, 0x10

    invoke-static {p0, v1}, Lcom/miui/server/smartpower/SmartScenarioManager;->behavierContainsScenario(II)Z

    move-result v1

    if-eqz v1, :cond_3

    .line 129
    or-int/lit8 v0, v0, 0x40

    goto :goto_0

    .line 130
    :cond_3
    const/16 v1, 0x404

    invoke-static {p0, v1}, Lcom/miui/server/smartpower/SmartScenarioManager;->behavierContainsScenario(II)Z

    move-result v1

    if-eqz v1, :cond_4

    .line 131
    or-int/lit16 v0, v0, 0x400

    goto :goto_0

    .line 132
    :cond_4
    const/4 v1, 0x2

    invoke-static {p0, v1}, Lcom/miui/server/smartpower/SmartScenarioManager;->behavierContainsScenario(II)Z

    move-result v1

    if-eqz v1, :cond_5

    .line 133
    or-int/lit16 v0, v0, 0x200

    goto :goto_0

    .line 134
    :cond_5
    const/16 v1, 0x80

    invoke-static {p0, v1}, Lcom/miui/server/smartpower/SmartScenarioManager;->behavierContainsScenario(II)Z

    move-result v1

    if-eqz v1, :cond_6

    .line 135
    or-int/lit16 v0, v0, 0x100

    .line 137
    :cond_6
    :goto_0
    const/16 v1, 0x20

    invoke-static {p0, v1}, Lcom/miui/server/smartpower/SmartScenarioManager;->behavierContainsScenario(II)Z

    move-result v1

    if-eqz v1, :cond_7

    .line 138
    const/high16 v1, 0x10000

    or-int/2addr v0, v1

    .line 140
    :cond_7
    const/16 v1, 0x40

    invoke-static {p0, v1}, Lcom/miui/server/smartpower/SmartScenarioManager;->behavierContainsScenario(II)Z

    move-result v1

    if-eqz v1, :cond_8

    .line 141
    or-int/lit16 v0, v0, 0x1000

    .line 143
    :cond_8
    return v0
.end method

.method static synthetic lambda$onAppActionChanged$0(IZLcom/miui/server/smartpower/SmartScenarioManager$ClientConfig;)V
    .locals 1
    .param p0, "action"    # I
    .param p1, "add"    # Z
    .param p2, "v"    # Lcom/miui/server/smartpower/SmartScenarioManager$ClientConfig;

    .line 212
    const/4 v0, 0x0

    invoke-virtual {p2, p0, v0, p1}, Lcom/miui/server/smartpower/SmartScenarioManager$ClientConfig;->onAppActionChanged(ILjava/lang/String;Z)V

    .line 213
    return-void
.end method


# virtual methods
.method public createClientConfig(Ljava/lang/String;Lcom/miui/server/smartpower/SmartScenarioManager$ISmartScenarioCallback;)Lcom/miui/server/smartpower/SmartScenarioManager$ClientConfig;
    .locals 1
    .param p1, "clientName"    # Ljava/lang/String;
    .param p2, "callback"    # Lcom/miui/server/smartpower/SmartScenarioManager$ISmartScenarioCallback;

    .line 219
    new-instance v0, Lcom/miui/server/smartpower/SmartScenarioManager$ClientConfig;

    invoke-direct {v0, p0, p1, p2}, Lcom/miui/server/smartpower/SmartScenarioManager$ClientConfig;-><init>(Lcom/miui/server/smartpower/SmartScenarioManager;Ljava/lang/String;Lcom/miui/server/smartpower/SmartScenarioManager$ISmartScenarioCallback;)V

    return-object v0
.end method

.method public dump(Ljava/io/PrintWriter;[Ljava/lang/String;I)V
    .locals 4
    .param p1, "pw"    # Ljava/io/PrintWriter;
    .param p2, "args"    # [Ljava/lang/String;
    .param p3, "opti"    # I

    .line 239
    const-string v0, "interactive apps:"

    invoke-virtual {p1, v0}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V

    .line 240
    iget-object v0, p0, Lcom/miui/server/smartpower/SmartScenarioManager;->mCurrentScenarios:Landroid/util/SparseArray;

    monitor-enter v0

    .line 241
    const/4 v1, 0x0

    .local v1, "i":I
    :goto_0
    :try_start_0
    iget-object v2, p0, Lcom/miui/server/smartpower/SmartScenarioManager;->mCurrentScenarios:Landroid/util/SparseArray;

    invoke-virtual {v2}, Landroid/util/SparseArray;->size()I

    move-result v2

    if-ge v1, v2, :cond_0

    .line 242
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "        "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget-object v3, p0, Lcom/miui/server/smartpower/SmartScenarioManager;->mCurrentScenarios:Landroid/util/SparseArray;

    invoke-virtual {v3, v1}, Landroid/util/SparseArray;->valueAt(I)Ljava/lang/Object;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {p1, v2}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V

    .line 241
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 244
    .end local v1    # "i":I
    :cond_0
    monitor-exit v0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 246
    const-string v0, ""

    invoke-virtual {p1, v0}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V

    .line 247
    iget-object v1, p0, Lcom/miui/server/smartpower/SmartScenarioManager;->mClients:Landroid/util/ArraySet;

    monitor-enter v1

    .line 248
    const/4 v0, 0x0

    .local v0, "i":I
    :goto_1
    :try_start_1
    iget-object v2, p0, Lcom/miui/server/smartpower/SmartScenarioManager;->mClients:Landroid/util/ArraySet;

    invoke-virtual {v2}, Landroid/util/ArraySet;->size()I

    move-result v2

    if-ge v0, v2, :cond_1

    .line 249
    iget-object v2, p0, Lcom/miui/server/smartpower/SmartScenarioManager;->mClients:Landroid/util/ArraySet;

    invoke-virtual {v2, v0}, Landroid/util/ArraySet;->valueAt(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/miui/server/smartpower/SmartScenarioManager$ClientConfig;

    invoke-virtual {v2, p1, p2, p3}, Lcom/miui/server/smartpower/SmartScenarioManager$ClientConfig;->dump(Ljava/io/PrintWriter;[Ljava/lang/String;I)V

    .line 248
    add-int/lit8 v0, v0, 0x1

    goto :goto_1

    .line 251
    .end local v0    # "i":I
    :cond_1
    monitor-exit v1

    .line 252
    return-void

    .line 251
    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    throw v0

    .line 244
    :catchall_1
    move-exception v1

    :try_start_2
    monitor-exit v0
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    throw v1
.end method

.method public onAppActionChanged(ILcom/android/server/am/AppStateManager$AppState;Z)V
    .locals 5
    .param p1, "action"    # I
    .param p2, "app"    # Lcom/android/server/am/AppStateManager$AppState;
    .param p3, "add"    # Z

    .line 185
    iget-object v0, p0, Lcom/miui/server/smartpower/SmartScenarioManager;->mInterestBgPkgs:Landroid/util/ArraySet;

    monitor-enter v0

    .line 186
    if-eqz p2, :cond_0

    const/16 v1, 0x8

    if-ne p1, v1, :cond_0

    :try_start_0
    iget-object v1, p0, Lcom/miui/server/smartpower/SmartScenarioManager;->mInterestBgPkgs:Landroid/util/ArraySet;

    .line 187
    invoke-virtual {p2}, Lcom/android/server/am/AppStateManager$AppState;->getPackageName()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Landroid/util/ArraySet;->contains(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_0

    .line 188
    monitor-exit v0

    return-void

    .line 190
    :cond_0
    monitor-exit v0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_2

    .line 192
    const/4 v0, 0x0

    .line 193
    .local v0, "changed":Z
    iget-object v1, p0, Lcom/miui/server/smartpower/SmartScenarioManager;->mCurrentScenarios:Landroid/util/SparseArray;

    monitor-enter v1

    .line 194
    :try_start_1
    iget-object v2, p0, Lcom/miui/server/smartpower/SmartScenarioManager;->mCurrentScenarios:Landroid/util/SparseArray;

    invoke-virtual {v2, p1}, Landroid/util/SparseArray;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/miui/server/smartpower/SmartScenarioManager$ScenarioItem;

    .line 195
    .local v2, "scenarioItem":Lcom/miui/server/smartpower/SmartScenarioManager$ScenarioItem;
    if-eqz p3, :cond_1

    if-nez v2, :cond_1

    .line 196
    new-instance v3, Lcom/miui/server/smartpower/SmartScenarioManager$ScenarioItem;

    const/4 v4, 0x0

    invoke-direct {v3, p0, p1, v4}, Lcom/miui/server/smartpower/SmartScenarioManager$ScenarioItem;-><init>(Lcom/miui/server/smartpower/SmartScenarioManager;ILcom/miui/server/smartpower/SmartScenarioManager$ScenarioItem-IA;)V

    move-object v2, v3

    .line 197
    iget-object v3, p0, Lcom/miui/server/smartpower/SmartScenarioManager;->mCurrentScenarios:Landroid/util/SparseArray;

    invoke-virtual {v3, p1, v2}, Landroid/util/SparseArray;->append(ILjava/lang/Object;)V

    .line 198
    const/4 v0, 0x1

    .line 201
    :cond_1
    if-eqz v2, :cond_2

    .line 202
    invoke-virtual {v2, p2, p3}, Lcom/miui/server/smartpower/SmartScenarioManager$ScenarioItem;->onAppActionChanged(Lcom/android/server/am/AppStateManager$AppState;Z)V

    .line 203
    if-nez p3, :cond_2

    invoke-static {v2}, Lcom/miui/server/smartpower/SmartScenarioManager$ScenarioItem;->-$$Nest$fgetmInteractiveApps(Lcom/miui/server/smartpower/SmartScenarioManager$ScenarioItem;)Ljava/util/ArrayList;

    move-result-object v3

    invoke-virtual {v3}, Ljava/util/ArrayList;->size()I

    move-result v3

    if-nez v3, :cond_2

    .line 204
    iget-object v3, p0, Lcom/miui/server/smartpower/SmartScenarioManager;->mCurrentScenarios:Landroid/util/SparseArray;

    invoke-virtual {v3, p1}, Landroid/util/SparseArray;->remove(I)V

    .line 205
    const/4 v0, 0x1

    .line 208
    .end local v2    # "scenarioItem":Lcom/miui/server/smartpower/SmartScenarioManager$ScenarioItem;
    :cond_2
    monitor-exit v1
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_1

    .line 209
    if-eqz v0, :cond_3

    .line 210
    iget-object v1, p0, Lcom/miui/server/smartpower/SmartScenarioManager;->mClients:Landroid/util/ArraySet;

    monitor-enter v1

    .line 211
    :try_start_2
    iget-object v2, p0, Lcom/miui/server/smartpower/SmartScenarioManager;->mClients:Landroid/util/ArraySet;

    new-instance v3, Lcom/miui/server/smartpower/SmartScenarioManager$$ExternalSyntheticLambda0;

    invoke-direct {v3, p1, p3}, Lcom/miui/server/smartpower/SmartScenarioManager$$ExternalSyntheticLambda0;-><init>(IZ)V

    invoke-virtual {v2, v3}, Landroid/util/ArraySet;->forEach(Ljava/util/function/Consumer;)V

    .line 214
    monitor-exit v1

    goto :goto_0

    :catchall_0
    move-exception v2

    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    throw v2

    .line 216
    :cond_3
    :goto_0
    return-void

    .line 208
    :catchall_1
    move-exception v2

    :try_start_3
    monitor-exit v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    throw v2

    .line 190
    .end local v0    # "changed":Z
    :catchall_2
    move-exception v1

    :try_start_4
    monitor-exit v0
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_2

    throw v1
.end method

.method public parseCurrentScenarioId(Lcom/miui/server/smartpower/SmartScenarioManager$ClientConfig;)V
    .locals 3
    .param p1, "client"    # Lcom/miui/server/smartpower/SmartScenarioManager$ClientConfig;

    .line 230
    iget-object v0, p0, Lcom/miui/server/smartpower/SmartScenarioManager;->mCurrentScenarios:Landroid/util/SparseArray;

    monitor-enter v0

    .line 231
    const/4 v1, 0x0

    .local v1, "i":I
    :goto_0
    :try_start_0
    iget-object v2, p0, Lcom/miui/server/smartpower/SmartScenarioManager;->mCurrentScenarios:Landroid/util/SparseArray;

    invoke-virtual {v2}, Landroid/util/SparseArray;->size()I

    move-result v2

    if-ge v1, v2, :cond_0

    .line 232
    iget-object v2, p0, Lcom/miui/server/smartpower/SmartScenarioManager;->mCurrentScenarios:Landroid/util/SparseArray;

    invoke-virtual {v2, v1}, Landroid/util/SparseArray;->valueAt(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/miui/server/smartpower/SmartScenarioManager$ScenarioItem;

    .line 233
    .local v2, "scenarioItem":Lcom/miui/server/smartpower/SmartScenarioManager$ScenarioItem;
    invoke-virtual {v2, p1}, Lcom/miui/server/smartpower/SmartScenarioManager$ScenarioItem;->parseCurrentScenarioId(Lcom/miui/server/smartpower/SmartScenarioManager$ClientConfig;)V

    .line 231
    .end local v2    # "scenarioItem":Lcom/miui/server/smartpower/SmartScenarioManager$ScenarioItem;
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 235
    .end local v1    # "i":I
    :cond_0
    monitor-exit v0

    .line 236
    return-void

    .line 235
    :catchall_0
    move-exception v1

    monitor-exit v0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v1
.end method

.method public registClientConfig(Lcom/miui/server/smartpower/SmartScenarioManager$ClientConfig;)V
    .locals 2
    .param p1, "client"    # Lcom/miui/server/smartpower/SmartScenarioManager$ClientConfig;

    .line 223
    iget-object v0, p0, Lcom/miui/server/smartpower/SmartScenarioManager;->mClients:Landroid/util/ArraySet;

    monitor-enter v0

    .line 224
    :try_start_0
    iget-object v1, p0, Lcom/miui/server/smartpower/SmartScenarioManager;->mClients:Landroid/util/ArraySet;

    invoke-virtual {v1, p1}, Landroid/util/ArraySet;->add(Ljava/lang/Object;)Z

    .line 225
    monitor-exit v0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 226
    invoke-virtual {p0, p1}, Lcom/miui/server/smartpower/SmartScenarioManager;->parseCurrentScenarioId(Lcom/miui/server/smartpower/SmartScenarioManager$ClientConfig;)V

    .line 227
    return-void

    .line 225
    :catchall_0
    move-exception v1

    :try_start_1
    monitor-exit v0
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    throw v1
.end method
