.class public Lcom/miui/server/smartpower/SmartScenarioManager$ScenarioIdInfo;
.super Ljava/lang/Object;
.source "SmartScenarioManager.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/miui/server/smartpower/SmartScenarioManager;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x1
    name = "ScenarioIdInfo"
.end annotation


# instance fields
.field private mAction:I

.field private mId:J

.field private mPackageName:Ljava/lang/String;

.field final synthetic this$0:Lcom/miui/server/smartpower/SmartScenarioManager;


# direct methods
.method static bridge synthetic -$$Nest$fgetmAction(Lcom/miui/server/smartpower/SmartScenarioManager$ScenarioIdInfo;)I
    .locals 0

    iget p0, p0, Lcom/miui/server/smartpower/SmartScenarioManager$ScenarioIdInfo;->mAction:I

    return p0
.end method

.method static bridge synthetic -$$Nest$fgetmId(Lcom/miui/server/smartpower/SmartScenarioManager$ScenarioIdInfo;)J
    .locals 2

    iget-wide v0, p0, Lcom/miui/server/smartpower/SmartScenarioManager$ScenarioIdInfo;->mId:J

    return-wide v0
.end method

.method static bridge synthetic -$$Nest$fgetmPackageName(Lcom/miui/server/smartpower/SmartScenarioManager$ScenarioIdInfo;)Ljava/lang/String;
    .locals 0

    iget-object p0, p0, Lcom/miui/server/smartpower/SmartScenarioManager$ScenarioIdInfo;->mPackageName:Ljava/lang/String;

    return-object p0
.end method

.method public constructor <init>(Lcom/miui/server/smartpower/SmartScenarioManager;Ljava/lang/String;IJ)V
    .locals 0
    .param p1, "this$0"    # Lcom/miui/server/smartpower/SmartScenarioManager;
    .param p2, "packageName"    # Ljava/lang/String;
    .param p3, "action"    # I
    .param p4, "id"    # J

    .line 463
    iput-object p1, p0, Lcom/miui/server/smartpower/SmartScenarioManager$ScenarioIdInfo;->this$0:Lcom/miui/server/smartpower/SmartScenarioManager;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 464
    iput-object p2, p0, Lcom/miui/server/smartpower/SmartScenarioManager$ScenarioIdInfo;->mPackageName:Ljava/lang/String;

    .line 465
    iput p3, p0, Lcom/miui/server/smartpower/SmartScenarioManager$ScenarioIdInfo;->mAction:I

    .line 466
    iput-wide p4, p0, Lcom/miui/server/smartpower/SmartScenarioManager$ScenarioIdInfo;->mId:J

    .line 467
    return-void
.end method


# virtual methods
.method public toString()Ljava/lang/String;
    .locals 2

    .line 471
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    iget v1, p0, Lcom/miui/server/smartpower/SmartScenarioManager$ScenarioIdInfo;->mAction:I

    invoke-static {v1}, Lcom/miui/server/smartpower/SmartScenarioManager;->actionTypeToString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, " "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lcom/miui/server/smartpower/SmartScenarioManager$ScenarioIdInfo;->mPackageName:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
