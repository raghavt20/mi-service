.class Lcom/miui/server/smartpower/AppWakelockResource;
.super Lcom/miui/server/smartpower/AppPowerResource;
.source "AppWakelockResource.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/miui/server/smartpower/AppWakelockResource$MyHandler;,
        Lcom/miui/server/smartpower/AppWakelockResource$WakeLock;
    }
.end annotation


# static fields
.field private static final BLOOEAN_DISABLE:I = 0x0

.field private static final BLOOEAN_ENABLE:I = 0x1

.field private static final MSG_SET_WAKELOCK_STATE:I = 0x1


# instance fields
.field private mHandler:Landroid/os/Handler;

.field private final mLock:Ljava/lang/Object;

.field private mPackageManagerInt:Landroid/content/pm/PackageManagerInternal;

.field private mSecurityManagerInt:Lmiui/security/SecurityManagerInternal;

.field private final mWakeLocks:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList<",
            "Lcom/miui/server/smartpower/AppWakelockResource$WakeLock;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public static synthetic $r8$lambda$475vpAqpCkbCJYlMumLxJqPqZLo(Lcom/miui/server/smartpower/AppWakelockResource;Landroid/os/IBinder;I)V
    .locals 0

    invoke-direct {p0, p1, p2}, Lcom/miui/server/smartpower/AppWakelockResource;->lambda$acquireWakelock$0(Landroid/os/IBinder;I)V

    return-void
.end method

.method public static synthetic $r8$lambda$QFyfXSZD-mFgmShxIG9ApR412g8(Lcom/miui/server/smartpower/AppWakelockResource;Lcom/miui/server/smartpower/AppWakelockResource$WakeLock;)V
    .locals 0

    invoke-direct {p0, p1}, Lcom/miui/server/smartpower/AppWakelockResource;->lambda$removeWakeLockLocked$1(Lcom/miui/server/smartpower/AppWakelockResource$WakeLock;)V

    return-void
.end method

.method static bridge synthetic -$$Nest$mhandleWakeLockDeath(Lcom/miui/server/smartpower/AppWakelockResource;Lcom/miui/server/smartpower/AppWakelockResource$WakeLock;)V
    .locals 0

    invoke-direct {p0, p1}, Lcom/miui/server/smartpower/AppWakelockResource;->handleWakeLockDeath(Lcom/miui/server/smartpower/AppWakelockResource$WakeLock;)V

    return-void
.end method

.method static bridge synthetic -$$Nest$msetWakeLockState(Lcom/miui/server/smartpower/AppWakelockResource;IZ)V
    .locals 0

    invoke-direct {p0, p1, p2}, Lcom/miui/server/smartpower/AppWakelockResource;->setWakeLockState(IZ)V

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/os/Looper;)V
    .locals 1
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "looper"    # Landroid/os/Looper;

    .line 41
    invoke-direct {p0}, Lcom/miui/server/smartpower/AppPowerResource;-><init>()V

    .line 33
    new-instance v0, Ljava/lang/Object;

    invoke-direct {v0}, Ljava/lang/Object;-><init>()V

    iput-object v0, p0, Lcom/miui/server/smartpower/AppWakelockResource;->mLock:Ljava/lang/Object;

    .line 36
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/miui/server/smartpower/AppWakelockResource;->mWakeLocks:Ljava/util/ArrayList;

    .line 42
    new-instance v0, Lcom/miui/server/smartpower/AppWakelockResource$MyHandler;

    invoke-direct {v0, p0, p2}, Lcom/miui/server/smartpower/AppWakelockResource$MyHandler;-><init>(Lcom/miui/server/smartpower/AppWakelockResource;Landroid/os/Looper;)V

    iput-object v0, p0, Lcom/miui/server/smartpower/AppWakelockResource;->mHandler:Landroid/os/Handler;

    .line 43
    const/4 v0, 0x4

    iput v0, p0, Lcom/miui/server/smartpower/AppWakelockResource;->mType:I

    .line 44
    const-class v0, Lmiui/security/SecurityManagerInternal;

    invoke-static {v0}, Lcom/android/server/LocalServices;->getService(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lmiui/security/SecurityManagerInternal;

    iput-object v0, p0, Lcom/miui/server/smartpower/AppWakelockResource;->mSecurityManagerInt:Lmiui/security/SecurityManagerInternal;

    .line 45
    const-class v0, Landroid/content/pm/PackageManagerInternal;

    invoke-static {v0}, Lcom/android/server/LocalServices;->getService(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/content/pm/PackageManagerInternal;

    iput-object v0, p0, Lcom/miui/server/smartpower/AppWakelockResource;->mPackageManagerInt:Landroid/content/pm/PackageManagerInternal;

    .line 46
    return-void
.end method

.method private findWakeLockIndexLocked(Landroid/os/IBinder;)I
    .locals 3
    .param p1, "lock"    # Landroid/os/IBinder;

    .line 170
    iget-object v0, p0, Lcom/miui/server/smartpower/AppWakelockResource;->mWakeLocks:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    .line 171
    .local v0, "count":I
    const/4 v1, 0x0

    .local v1, "i":I
    :goto_0
    if-ge v1, v0, :cond_1

    .line 172
    iget-object v2, p0, Lcom/miui/server/smartpower/AppWakelockResource;->mWakeLocks:Ljava/util/ArrayList;

    invoke-virtual {v2, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/miui/server/smartpower/AppWakelockResource$WakeLock;

    iget-object v2, v2, Lcom/miui/server/smartpower/AppWakelockResource$WakeLock;->mLock:Landroid/os/IBinder;

    if-ne v2, p1, :cond_0

    .line 173
    return v1

    .line 171
    :cond_0
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 176
    .end local v1    # "i":I
    :cond_1
    const/4 v1, -0x1

    return v1
.end method

.method private handleWakeLockDeath(Lcom/miui/server/smartpower/AppWakelockResource$WakeLock;)V
    .locals 2
    .param p1, "wakeLock"    # Lcom/miui/server/smartpower/AppWakelockResource$WakeLock;

    .line 180
    iget-object v0, p0, Lcom/miui/server/smartpower/AppWakelockResource;->mLock:Ljava/lang/Object;

    monitor-enter v0

    .line 181
    :try_start_0
    iget-object v1, p0, Lcom/miui/server/smartpower/AppWakelockResource;->mWakeLocks:Ljava/util/ArrayList;

    invoke-virtual {v1, p1}, Ljava/util/ArrayList;->indexOf(Ljava/lang/Object;)I

    move-result v1

    .line 182
    .local v1, "index":I
    if-gez v1, :cond_0

    .line 183
    monitor-exit v0

    return-void

    .line 185
    :cond_0
    invoke-direct {p0, p1, v1}, Lcom/miui/server/smartpower/AppWakelockResource;->removeWakeLockLocked(Lcom/miui/server/smartpower/AppWakelockResource$WakeLock;I)V

    .line 186
    .end local v1    # "index":I
    monitor-exit v0

    .line 187
    return-void

    .line 186
    :catchall_0
    move-exception v1

    monitor-exit v0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v1
.end method

.method private synthetic lambda$acquireWakelock$0(Landroid/os/IBinder;I)V
    .locals 1
    .param p1, "lock"    # Landroid/os/IBinder;
    .param p2, "ownerUid"    # I

    .line 145
    const/4 v0, 0x1

    invoke-virtual {p0, p1, p2, v0}, Lcom/miui/server/smartpower/AppWakelockResource;->onAppBehaviorEvent(Landroid/os/IBinder;IZ)V

    return-void
.end method

.method private synthetic lambda$removeWakeLockLocked$1(Lcom/miui/server/smartpower/AppWakelockResource$WakeLock;)V
    .locals 3
    .param p1, "wakeLock"    # Lcom/miui/server/smartpower/AppWakelockResource$WakeLock;

    .line 191
    iget-object v0, p1, Lcom/miui/server/smartpower/AppWakelockResource$WakeLock;->mLock:Landroid/os/IBinder;

    iget v1, p1, Lcom/miui/server/smartpower/AppWakelockResource$WakeLock;->mOwnerUid:I

    const/4 v2, 0x0

    invoke-virtual {p0, v0, v1, v2}, Lcom/miui/server/smartpower/AppWakelockResource;->onAppBehaviorEvent(Landroid/os/IBinder;IZ)V

    return-void
.end method

.method private removeWakeLockLocked(Lcom/miui/server/smartpower/AppWakelockResource$WakeLock;I)V
    .locals 2
    .param p1, "wakeLock"    # Lcom/miui/server/smartpower/AppWakelockResource$WakeLock;
    .param p2, "index"    # I

    .line 190
    iget-object v0, p0, Lcom/miui/server/smartpower/AppWakelockResource;->mWakeLocks:Ljava/util/ArrayList;

    invoke-virtual {v0, p2}, Ljava/util/ArrayList;->remove(I)Ljava/lang/Object;

    .line 191
    iget-object v0, p0, Lcom/miui/server/smartpower/AppWakelockResource;->mHandler:Landroid/os/Handler;

    new-instance v1, Lcom/miui/server/smartpower/AppWakelockResource$$ExternalSyntheticLambda1;

    invoke-direct {v1, p0, p1}, Lcom/miui/server/smartpower/AppWakelockResource$$ExternalSyntheticLambda1;-><init>(Lcom/miui/server/smartpower/AppWakelockResource;Lcom/miui/server/smartpower/AppWakelockResource$WakeLock;)V

    invoke-virtual {v0, v1}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    .line 192
    return-void
.end method

.method private setWakeLockState(IZ)V
    .locals 5
    .param p1, "uid"    # I
    .param p2, "enable"    # Z

    .line 105
    :try_start_0
    new-instance v0, Landroid/util/ArraySet;

    invoke-direct {v0}, Landroid/util/ArraySet;-><init>()V

    .line 106
    .local v0, "targetTags":Landroid/util/ArraySet;, "Landroid/util/ArraySet<Ljava/lang/String;>;"
    iget-object v1, p0, Lcom/miui/server/smartpower/AppWakelockResource;->mLock:Ljava/lang/Object;

    monitor-enter v1
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 107
    :try_start_1
    iget-object v2, p0, Lcom/miui/server/smartpower/AppWakelockResource;->mWakeLocks:Ljava/util/ArrayList;

    invoke-virtual {v2}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_1

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/miui/server/smartpower/AppWakelockResource$WakeLock;

    .line 108
    .local v3, "wakelock":Lcom/miui/server/smartpower/AppWakelockResource$WakeLock;
    iget v4, v3, Lcom/miui/server/smartpower/AppWakelockResource$WakeLock;->mOwnerUid:I

    if-ne v4, p1, :cond_0

    .line 109
    iget-object v4, v3, Lcom/miui/server/smartpower/AppWakelockResource$WakeLock;->mTag:Ljava/lang/String;

    invoke-virtual {v0, v4}, Landroid/util/ArraySet;->add(Ljava/lang/Object;)Z

    .line 111
    .end local v3    # "wakelock":Lcom/miui/server/smartpower/AppWakelockResource$WakeLock;
    :cond_0
    goto :goto_0

    .line 112
    :cond_1
    monitor-exit v1
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 113
    :try_start_2
    invoke-virtual {v0}, Landroid/util/ArraySet;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_1
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_3

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/lang/String;

    .line 114
    .local v2, "tag":Ljava/lang/String;
    invoke-static {}, Lcom/android/server/power/PowerManagerServiceStub;->get()Lcom/android/server/power/PowerManagerServiceStub;

    move-result-object v3

    if-nez p2, :cond_2

    const/4 v4, 0x1

    goto :goto_2

    :cond_2
    const/4 v4, 0x0

    :goto_2
    invoke-virtual {v3, p1, v2, v4}, Lcom/android/server/power/PowerManagerServiceStub;->setUidPartialWakeLockDisabledState(ILjava/lang/String;Z)V

    .line 115
    .end local v2    # "tag":Ljava/lang/String;
    goto :goto_1

    .line 116
    :cond_3
    if-eqz p2, :cond_4

    const-string v1, "resume"

    goto :goto_3

    :cond_4
    const-string v1, "release"

    .line 117
    .local v1, "reason":Ljava/lang/String;
    :goto_3
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v3, "wakelock u:"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, " s:"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    const v3, 0x15ff2

    invoke-static {v3, v2}, Landroid/util/EventLog;->writeEvent(ILjava/lang/String;)I

    .line 119
    sget-boolean v2, Lcom/miui/server/smartpower/AppWakelockResource;->DEBUG:Z

    if-eqz v2, :cond_5

    const-string v2, "SmartPower.AppResource"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v4, "setWakeLockState "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, " s:"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Landroid/util/Slog;->d(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_2
    .catch Ljava/lang/Exception; {:try_start_2 .. :try_end_2} :catch_0

    .line 122
    .end local v0    # "targetTags":Landroid/util/ArraySet;, "Landroid/util/ArraySet<Ljava/lang/String;>;"
    .end local v1    # "reason":Ljava/lang/String;
    :cond_5
    goto :goto_4

    .line 112
    .restart local v0    # "targetTags":Landroid/util/ArraySet;, "Landroid/util/ArraySet<Ljava/lang/String;>;"
    :catchall_0
    move-exception v2

    :try_start_3
    monitor-exit v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    .end local p0    # "this":Lcom/miui/server/smartpower/AppWakelockResource;
    .end local p1    # "uid":I
    .end local p2    # "enable":Z
    :try_start_4
    throw v2
    :try_end_4
    .catch Ljava/lang/Exception; {:try_start_4 .. :try_end_4} :catch_0

    .line 120
    .end local v0    # "targetTags":Landroid/util/ArraySet;, "Landroid/util/ArraySet<Ljava/lang/String;>;"
    .restart local p0    # "this":Lcom/miui/server/smartpower/AppWakelockResource;
    .restart local p1    # "uid":I
    .restart local p2    # "enable":Z
    :catch_0
    move-exception v0

    .line 121
    .local v0, "e":Ljava/lang/Exception;
    const-string v1, "SmartPower.AppResource"

    const-string v2, "releaseAppPowerResource"

    invoke-static {v1, v2, v0}, Landroid/util/Slog;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    .line 123
    .end local v0    # "e":Ljava/lang/Exception;
    :goto_4
    return-void
.end method


# virtual methods
.method public acquireWakelock(Landroid/os/IBinder;ILjava/lang/String;II)V
    .locals 10
    .param p1, "lock"    # Landroid/os/IBinder;
    .param p2, "flags"    # I
    .param p3, "tag"    # Ljava/lang/String;
    .param p4, "ownerUid"    # I
    .param p5, "ownerPid"    # I

    .line 126
    iget-object v0, p0, Lcom/miui/server/smartpower/AppWakelockResource;->mLock:Ljava/lang/Object;

    monitor-enter v0

    .line 128
    :try_start_0
    invoke-direct {p0, p1}, Lcom/miui/server/smartpower/AppWakelockResource;->findWakeLockIndexLocked(Landroid/os/IBinder;)I

    move-result v1

    .line 129
    .local v1, "index":I
    if-ltz v1, :cond_0

    .line 130
    iget-object v2, p0, Lcom/miui/server/smartpower/AppWakelockResource;->mWakeLocks:Ljava/util/ArrayList;

    invoke-virtual {v2, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/miui/server/smartpower/AppWakelockResource$WakeLock;

    .line 131
    .local v2, "wakeLock":Lcom/miui/server/smartpower/AppWakelockResource$WakeLock;
    invoke-virtual {v2, p2, p3}, Lcom/miui/server/smartpower/AppWakelockResource$WakeLock;->updateProperties(ILjava/lang/String;)V

    goto :goto_0

    .line 133
    .end local v2    # "wakeLock":Lcom/miui/server/smartpower/AppWakelockResource$WakeLock;
    :cond_0
    new-instance v9, Lcom/miui/server/smartpower/AppWakelockResource$WakeLock;

    move-object v2, v9

    move-object v3, p0

    move-object v4, p1

    move v5, p2

    move-object v6, p3

    move v7, p4

    move v8, p5

    invoke-direct/range {v2 .. v8}, Lcom/miui/server/smartpower/AppWakelockResource$WakeLock;-><init>(Lcom/miui/server/smartpower/AppWakelockResource;Landroid/os/IBinder;ILjava/lang/String;II)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-object v2, v9

    .line 135
    .restart local v2    # "wakeLock":Lcom/miui/server/smartpower/AppWakelockResource$WakeLock;
    const/4 v3, 0x0

    :try_start_1
    invoke-interface {p1, v2, v3}, Landroid/os/IBinder;->linkToDeath(Landroid/os/IBinder$DeathRecipient;I)V
    :try_end_1
    .catch Landroid/os/RemoteException; {:try_start_1 .. :try_end_1} :catch_0
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 138
    nop

    .line 139
    :try_start_2
    iget-object v3, p0, Lcom/miui/server/smartpower/AppWakelockResource;->mWakeLocks:Ljava/util/ArrayList;

    invoke-virtual {v3, v2}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 141
    :goto_0
    sget-boolean v3, Lcom/miui/server/smartpower/AppWakelockResource;->DEBUG:Z

    if-eqz v3, :cond_1

    .line 142
    const-string v3, "SmartPower.AppResource"

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "onAcquireWakelock: "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v2}, Lcom/miui/server/smartpower/AppWakelockResource$WakeLock;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Landroid/util/Slog;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 144
    .end local v1    # "index":I
    .end local v2    # "wakeLock":Lcom/miui/server/smartpower/AppWakelockResource$WakeLock;
    :cond_1
    monitor-exit v0
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    .line 145
    iget-object v0, p0, Lcom/miui/server/smartpower/AppWakelockResource;->mHandler:Landroid/os/Handler;

    new-instance v1, Lcom/miui/server/smartpower/AppWakelockResource$$ExternalSyntheticLambda0;

    invoke-direct {v1, p0, p1, p4}, Lcom/miui/server/smartpower/AppWakelockResource$$ExternalSyntheticLambda0;-><init>(Lcom/miui/server/smartpower/AppWakelockResource;Landroid/os/IBinder;I)V

    invoke-virtual {v0, v1}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    .line 146
    return-void

    .line 136
    .restart local v1    # "index":I
    .restart local v2    # "wakeLock":Lcom/miui/server/smartpower/AppWakelockResource$WakeLock;
    :catch_0
    move-exception v3

    .line 137
    .local v3, "ex":Landroid/os/RemoteException;
    :try_start_3
    new-instance v4, Ljava/lang/IllegalArgumentException;

    const-string v5, "Wake lock is already dead."

    invoke-direct {v4, v5}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    .end local p0    # "this":Lcom/miui/server/smartpower/AppWakelockResource;
    .end local p1    # "lock":Landroid/os/IBinder;
    .end local p2    # "flags":I
    .end local p3    # "tag":Ljava/lang/String;
    .end local p4    # "ownerUid":I
    .end local p5    # "ownerPid":I
    throw v4

    .line 144
    .end local v1    # "index":I
    .end local v2    # "wakeLock":Lcom/miui/server/smartpower/AppWakelockResource$WakeLock;
    .end local v3    # "ex":Landroid/os/RemoteException;
    .restart local p0    # "this":Lcom/miui/server/smartpower/AppWakelockResource;
    .restart local p1    # "lock":Landroid/os/IBinder;
    .restart local p2    # "flags":I
    .restart local p3    # "tag":Ljava/lang/String;
    .restart local p4    # "ownerUid":I
    .restart local p5    # "ownerPid":I
    :catchall_0
    move-exception v1

    monitor-exit v0
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    throw v1
.end method

.method public getActiveUids()Ljava/util/ArrayList;
    .locals 5

    .line 50
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    .line 51
    .local v0, "uids":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/Integer;>;"
    iget-object v1, p0, Lcom/miui/server/smartpower/AppWakelockResource;->mLock:Ljava/lang/Object;

    monitor-enter v1

    .line 52
    :try_start_0
    iget-object v2, p0, Lcom/miui/server/smartpower/AppWakelockResource;->mWakeLocks:Ljava/util/ArrayList;

    invoke-virtual {v2}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_0

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/miui/server/smartpower/AppWakelockResource$WakeLock;

    .line 53
    .local v3, "wakeLock":Lcom/miui/server/smartpower/AppWakelockResource$WakeLock;
    iget v4, v3, Lcom/miui/server/smartpower/AppWakelockResource$WakeLock;->mOwnerUid:I

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    invoke-virtual {v0, v4}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 54
    nop

    .end local v3    # "wakeLock":Lcom/miui/server/smartpower/AppWakelockResource$WakeLock;
    goto :goto_0

    .line 55
    :cond_0
    monitor-exit v1

    .line 56
    return-object v0

    .line 55
    :catchall_0
    move-exception v2

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v2
.end method

.method public isAppResourceActive(I)Z
    .locals 4
    .param p1, "uid"    # I

    .line 61
    iget-object v0, p0, Lcom/miui/server/smartpower/AppWakelockResource;->mLock:Ljava/lang/Object;

    monitor-enter v0

    .line 62
    :try_start_0
    iget-object v1, p0, Lcom/miui/server/smartpower/AppWakelockResource;->mWakeLocks:Ljava/util/ArrayList;

    invoke-virtual {v1}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_1

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/miui/server/smartpower/AppWakelockResource$WakeLock;

    .line 63
    .local v2, "wakeLock":Lcom/miui/server/smartpower/AppWakelockResource$WakeLock;
    iget v3, v2, Lcom/miui/server/smartpower/AppWakelockResource$WakeLock;->mOwnerUid:I

    if-ne v3, p1, :cond_0

    .line 64
    monitor-exit v0

    const/4 v0, 0x1

    return v0

    .line 66
    .end local v2    # "wakeLock":Lcom/miui/server/smartpower/AppWakelockResource$WakeLock;
    :cond_0
    goto :goto_0

    .line 67
    :cond_1
    monitor-exit v0

    .line 68
    const/4 v0, 0x0

    return v0

    .line 67
    :catchall_0
    move-exception v1

    monitor-exit v0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v1
.end method

.method public isAppResourceActive(II)Z
    .locals 4
    .param p1, "uid"    # I
    .param p2, "pid"    # I

    .line 73
    iget-object v0, p0, Lcom/miui/server/smartpower/AppWakelockResource;->mLock:Ljava/lang/Object;

    monitor-enter v0

    .line 74
    :try_start_0
    iget-object v1, p0, Lcom/miui/server/smartpower/AppWakelockResource;->mWakeLocks:Ljava/util/ArrayList;

    invoke-virtual {v1}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_1

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/miui/server/smartpower/AppWakelockResource$WakeLock;

    .line 75
    .local v2, "wakeLock":Lcom/miui/server/smartpower/AppWakelockResource$WakeLock;
    iget v3, v2, Lcom/miui/server/smartpower/AppWakelockResource$WakeLock;->mOwnerPid:I

    if-ne v3, p2, :cond_0

    .line 76
    monitor-exit v0

    const/4 v0, 0x1

    return v0

    .line 78
    .end local v2    # "wakeLock":Lcom/miui/server/smartpower/AppWakelockResource$WakeLock;
    :cond_0
    goto :goto_0

    .line 79
    :cond_1
    monitor-exit v0

    .line 80
    const/4 v0, 0x0

    return v0

    .line 79
    :catchall_0
    move-exception v1

    monitor-exit v0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v1
.end method

.method public onAppBehaviorEvent(Landroid/os/IBinder;IZ)V
    .locals 9
    .param p1, "binder"    # Landroid/os/IBinder;
    .param p2, "uid"    # I
    .param p3, "state"    # Z

    .line 287
    invoke-static {p2}, Landroid/os/UserHandle;->getAppId(I)I

    move-result v0

    const/16 v1, 0x2710

    if-ge v0, v1, :cond_0

    .line 288
    return-void

    .line 290
    :cond_0
    iget-object v0, p0, Lcom/miui/server/smartpower/AppWakelockResource;->mPackageManagerInt:Landroid/content/pm/PackageManagerInternal;

    invoke-virtual {v0, p2}, Landroid/content/pm/PackageManagerInternal;->getNameForUid(I)Ljava/lang/String;

    move-result-object v0

    .line 291
    .local v0, "packageName":Ljava/lang/String;
    iget-object v1, p0, Lcom/miui/server/smartpower/AppWakelockResource;->mPackageManagerInt:Landroid/content/pm/PackageManagerInternal;

    const-wide/16 v3, 0x0

    const/16 v5, 0x3e8

    .line 292
    invoke-static {p2}, Landroid/os/UserHandle;->getUserId(I)I

    move-result v6

    .line 291
    move-object v2, v0

    invoke-virtual/range {v1 .. v6}, Landroid/content/pm/PackageManagerInternal;->getApplicationInfo(Ljava/lang/String;JII)Landroid/content/pm/ApplicationInfo;

    move-result-object v1

    .line 293
    .local v1, "applicationInfo":Landroid/content/pm/ApplicationInfo;
    if-eqz v1, :cond_2

    invoke-static {v1}, Lcom/android/server/am/ProcessUtils;->isSystem(Landroid/content/pm/ApplicationInfo;)Z

    move-result v2

    if-eqz v2, :cond_1

    goto :goto_0

    .line 296
    :cond_1
    iget-object v3, p0, Lcom/miui/server/smartpower/AppWakelockResource;->mSecurityManagerInt:Lmiui/security/SecurityManagerInternal;

    const/16 v4, 0x1a

    iget-object v7, v1, Landroid/content/pm/ApplicationInfo;->packageName:Ljava/lang/String;

    move-object v5, p1

    move v6, p2

    move v8, p3

    invoke-virtual/range {v3 .. v8}, Lmiui/security/SecurityManagerInternal;->recordDurationInfo(ILandroid/os/IBinder;ILjava/lang/String;Z)V

    .line 298
    return-void

    .line 294
    :cond_2
    :goto_0
    return-void
.end method

.method public releaseAppPowerResource(I)V
    .locals 2
    .param p1, "uid"    # I

    .line 85
    invoke-static {p1}, Landroid/os/UserHandle;->isApp(I)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 86
    iget-object v0, p0, Lcom/miui/server/smartpower/AppWakelockResource;->mHandler:Landroid/os/Handler;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Landroid/os/Handler;->obtainMessage(I)Landroid/os/Message;

    move-result-object v0

    .line 87
    .local v0, "msg":Landroid/os/Message;
    iput p1, v0, Landroid/os/Message;->arg1:I

    .line 88
    const/4 v1, 0x0

    iput v1, v0, Landroid/os/Message;->arg2:I

    .line 89
    iget-object v1, p0, Lcom/miui/server/smartpower/AppWakelockResource;->mHandler:Landroid/os/Handler;

    invoke-virtual {v1, v0}, Landroid/os/Handler;->sendMessage(Landroid/os/Message;)Z

    .line 91
    .end local v0    # "msg":Landroid/os/Message;
    :cond_0
    return-void
.end method

.method public releaseWakelock(Landroid/os/IBinder;I)V
    .locals 7
    .param p1, "lock"    # Landroid/os/IBinder;
    .param p2, "flags"    # I

    .line 149
    iget-object v0, p0, Lcom/miui/server/smartpower/AppWakelockResource;->mLock:Ljava/lang/Object;

    monitor-enter v0

    .line 150
    :try_start_0
    invoke-direct {p0, p1}, Lcom/miui/server/smartpower/AppWakelockResource;->findWakeLockIndexLocked(Landroid/os/IBinder;)I

    move-result v1

    .line 151
    .local v1, "index":I
    if-gez v1, :cond_0

    .line 152
    monitor-exit v0

    return-void

    .line 155
    :cond_0
    iget-object v2, p0, Lcom/miui/server/smartpower/AppWakelockResource;->mWakeLocks:Ljava/util/ArrayList;

    invoke-virtual {v2, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/miui/server/smartpower/AppWakelockResource$WakeLock;

    .line 156
    .local v2, "wakeLock":Lcom/miui/server/smartpower/AppWakelockResource$WakeLock;
    sget-boolean v3, Lcom/miui/server/smartpower/AppWakelockResource;->DEBUG:Z

    if-eqz v3, :cond_1

    .line 157
    const-string v3, "SmartPower.AppResource"

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "onReleaseWakelock: "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v2}, Lcom/miui/server/smartpower/AppWakelockResource$WakeLock;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Landroid/util/Slog;->d(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 160
    :cond_1
    :try_start_1
    iget-object v3, v2, Lcom/miui/server/smartpower/AppWakelockResource$WakeLock;->mLock:Landroid/os/IBinder;

    const/4 v4, 0x0

    invoke-interface {v3, v2, v4}, Landroid/os/IBinder;->unlinkToDeath(Landroid/os/IBinder$DeathRecipient;I)Z
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_0
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 164
    goto :goto_0

    .line 161
    :catch_0
    move-exception v3

    .line 162
    .local v3, "e":Ljava/lang/Exception;
    :try_start_2
    const-string v4, "SmartPower.AppResource"

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v6, "unlinkToDeath failed wakelock="

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    .line 163
    invoke-virtual {v2}, Lcom/miui/server/smartpower/AppWakelockResource$WakeLock;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, " lock="

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    .line 162
    invoke-static {v4, v5, v3}, Landroid/util/Slog;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    .line 165
    .end local v3    # "e":Ljava/lang/Exception;
    :goto_0
    invoke-direct {p0, v2, v1}, Lcom/miui/server/smartpower/AppWakelockResource;->removeWakeLockLocked(Lcom/miui/server/smartpower/AppWakelockResource$WakeLock;I)V

    .line 166
    .end local v1    # "index":I
    .end local v2    # "wakeLock":Lcom/miui/server/smartpower/AppWakelockResource$WakeLock;
    monitor-exit v0

    .line 167
    return-void

    .line 166
    :catchall_0
    move-exception v1

    monitor-exit v0
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    throw v1
.end method

.method public resumeAppPowerResource(I)V
    .locals 2
    .param p1, "uid"    # I

    .line 95
    invoke-static {p1}, Landroid/os/UserHandle;->isApp(I)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 96
    iget-object v0, p0, Lcom/miui/server/smartpower/AppWakelockResource;->mHandler:Landroid/os/Handler;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Landroid/os/Handler;->obtainMessage(I)Landroid/os/Message;

    move-result-object v0

    .line 97
    .local v0, "msg":Landroid/os/Message;
    iput p1, v0, Landroid/os/Message;->arg1:I

    .line 98
    iput v1, v0, Landroid/os/Message;->arg2:I

    .line 99
    iget-object v1, p0, Lcom/miui/server/smartpower/AppWakelockResource;->mHandler:Landroid/os/Handler;

    invoke-virtual {v1, v0}, Landroid/os/Handler;->sendMessage(Landroid/os/Message;)Z

    .line 101
    .end local v0    # "msg":Landroid/os/Message;
    :cond_0
    return-void
.end method
