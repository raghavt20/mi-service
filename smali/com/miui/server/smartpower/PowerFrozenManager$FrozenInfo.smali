.class Lcom/miui/server/smartpower/PowerFrozenManager$FrozenInfo;
.super Ljava/lang/Object;
.source "PowerFrozenManager.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/miui/server/smartpower/PowerFrozenManager;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x8
    name = "FrozenInfo"
.end annotation


# instance fields
.field private mFrozenReasons:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private mFrozenTimes:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List<",
            "Ljava/lang/Long;",
            ">;"
        }
    .end annotation
.end field

.field private mPid:I

.field private mProcessName:Ljava/lang/String;

.field private mThawReason:Ljava/lang/String;

.field private mThawTime:J

.field private mThawUptime:J

.field private mUid:I


# direct methods
.method static bridge synthetic -$$Nest$fgetmFrozenReasons(Lcom/miui/server/smartpower/PowerFrozenManager$FrozenInfo;)Ljava/util/List;
    .locals 0

    iget-object p0, p0, Lcom/miui/server/smartpower/PowerFrozenManager$FrozenInfo;->mFrozenReasons:Ljava/util/List;

    return-object p0
.end method

.method static bridge synthetic -$$Nest$fgetmFrozenTimes(Lcom/miui/server/smartpower/PowerFrozenManager$FrozenInfo;)Ljava/util/List;
    .locals 0

    iget-object p0, p0, Lcom/miui/server/smartpower/PowerFrozenManager$FrozenInfo;->mFrozenTimes:Ljava/util/List;

    return-object p0
.end method

.method static bridge synthetic -$$Nest$fgetmPid(Lcom/miui/server/smartpower/PowerFrozenManager$FrozenInfo;)I
    .locals 0

    iget p0, p0, Lcom/miui/server/smartpower/PowerFrozenManager$FrozenInfo;->mPid:I

    return p0
.end method

.method static bridge synthetic -$$Nest$fgetmThawReason(Lcom/miui/server/smartpower/PowerFrozenManager$FrozenInfo;)Ljava/lang/String;
    .locals 0

    iget-object p0, p0, Lcom/miui/server/smartpower/PowerFrozenManager$FrozenInfo;->mThawReason:Ljava/lang/String;

    return-object p0
.end method

.method static bridge synthetic -$$Nest$fgetmThawTime(Lcom/miui/server/smartpower/PowerFrozenManager$FrozenInfo;)J
    .locals 2

    iget-wide v0, p0, Lcom/miui/server/smartpower/PowerFrozenManager$FrozenInfo;->mThawTime:J

    return-wide v0
.end method

.method static bridge synthetic -$$Nest$fgetmUid(Lcom/miui/server/smartpower/PowerFrozenManager$FrozenInfo;)I
    .locals 0

    iget p0, p0, Lcom/miui/server/smartpower/PowerFrozenManager$FrozenInfo;->mUid:I

    return p0
.end method

.method static bridge synthetic -$$Nest$fputmThawReason(Lcom/miui/server/smartpower/PowerFrozenManager$FrozenInfo;Ljava/lang/String;)V
    .locals 0

    iput-object p1, p0, Lcom/miui/server/smartpower/PowerFrozenManager$FrozenInfo;->mThawReason:Ljava/lang/String;

    return-void
.end method

.method static bridge synthetic -$$Nest$fputmThawTime(Lcom/miui/server/smartpower/PowerFrozenManager$FrozenInfo;J)V
    .locals 0

    iput-wide p1, p0, Lcom/miui/server/smartpower/PowerFrozenManager$FrozenInfo;->mThawTime:J

    return-void
.end method

.method static bridge synthetic -$$Nest$fputmThawUptime(Lcom/miui/server/smartpower/PowerFrozenManager$FrozenInfo;J)V
    .locals 0

    iput-wide p1, p0, Lcom/miui/server/smartpower/PowerFrozenManager$FrozenInfo;->mThawUptime:J

    return-void
.end method

.method static bridge synthetic -$$Nest$maddFrozenInfo(Lcom/miui/server/smartpower/PowerFrozenManager$FrozenInfo;JLjava/lang/String;)V
    .locals 0

    invoke-direct {p0, p1, p2, p3}, Lcom/miui/server/smartpower/PowerFrozenManager$FrozenInfo;->addFrozenInfo(JLjava/lang/String;)V

    return-void
.end method

.method static bridge synthetic -$$Nest$mgetFrozenDuration(Lcom/miui/server/smartpower/PowerFrozenManager$FrozenInfo;)J
    .locals 2

    invoke-direct {p0}, Lcom/miui/server/smartpower/PowerFrozenManager$FrozenInfo;->getFrozenDuration()J

    move-result-wide v0

    return-wide v0
.end method

.method private constructor <init>(IILjava/lang/String;)V
    .locals 2
    .param p1, "uid"    # I
    .param p2, "pid"    # I
    .param p3, "processName"    # Ljava/lang/String;

    .line 497
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 494
    new-instance v0, Ljava/util/ArrayList;

    const/16 v1, 0x10

    invoke-direct {v0, v1}, Ljava/util/ArrayList;-><init>(I)V

    iput-object v0, p0, Lcom/miui/server/smartpower/PowerFrozenManager$FrozenInfo;->mFrozenTimes:Ljava/util/List;

    .line 495
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0, v1}, Ljava/util/ArrayList;-><init>(I)V

    iput-object v0, p0, Lcom/miui/server/smartpower/PowerFrozenManager$FrozenInfo;->mFrozenReasons:Ljava/util/List;

    .line 498
    iput p1, p0, Lcom/miui/server/smartpower/PowerFrozenManager$FrozenInfo;->mUid:I

    .line 499
    iput p2, p0, Lcom/miui/server/smartpower/PowerFrozenManager$FrozenInfo;->mPid:I

    .line 500
    iput-object p3, p0, Lcom/miui/server/smartpower/PowerFrozenManager$FrozenInfo;->mProcessName:Ljava/lang/String;

    .line 501
    return-void
.end method

.method synthetic constructor <init>(IILjava/lang/String;Lcom/miui/server/smartpower/PowerFrozenManager$FrozenInfo-IA;)V
    .locals 0

    invoke-direct {p0, p1, p2, p3}, Lcom/miui/server/smartpower/PowerFrozenManager$FrozenInfo;-><init>(IILjava/lang/String;)V

    return-void
.end method

.method private addFrozenInfo(JLjava/lang/String;)V
    .locals 2
    .param p1, "curTime"    # J
    .param p3, "reason"    # Ljava/lang/String;

    .line 504
    iget-object v0, p0, Lcom/miui/server/smartpower/PowerFrozenManager$FrozenInfo;->mFrozenTimes:Ljava/util/List;

    invoke-static {p1, p2}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 505
    iget-object v0, p0, Lcom/miui/server/smartpower/PowerFrozenManager$FrozenInfo;->mFrozenReasons:Ljava/util/List;

    invoke-interface {v0, p3}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 506
    return-void
.end method

.method private getEndTime()J
    .locals 2

    .line 517
    iget-wide v0, p0, Lcom/miui/server/smartpower/PowerFrozenManager$FrozenInfo;->mThawTime:J

    return-wide v0
.end method

.method private getFrozenDuration()J
    .locals 4

    .line 521
    invoke-direct {p0}, Lcom/miui/server/smartpower/PowerFrozenManager$FrozenInfo;->getStartTime()J

    move-result-wide v0

    invoke-direct {p0}, Lcom/miui/server/smartpower/PowerFrozenManager$FrozenInfo;->getEndTime()J

    move-result-wide v2

    cmp-long v0, v0, v2

    if-gez v0, :cond_0

    .line 522
    invoke-direct {p0}, Lcom/miui/server/smartpower/PowerFrozenManager$FrozenInfo;->getEndTime()J

    move-result-wide v0

    invoke-direct {p0}, Lcom/miui/server/smartpower/PowerFrozenManager$FrozenInfo;->getStartTime()J

    move-result-wide v2

    sub-long/2addr v0, v2

    return-wide v0

    .line 524
    :cond_0
    const-wide/16 v0, 0x0

    return-wide v0
.end method

.method private getStartTime()J
    .locals 2

    .line 509
    iget-object v0, p0, Lcom/miui/server/smartpower/PowerFrozenManager$FrozenInfo;->mFrozenTimes:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    if-nez v0, :cond_0

    .line 510
    const-wide/16 v0, 0x0

    return-wide v0

    .line 512
    :cond_0
    iget-object v0, p0, Lcom/miui/server/smartpower/PowerFrozenManager$FrozenInfo;->mFrozenTimes:Ljava/util/List;

    const/4 v1, 0x0

    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Long;

    invoke-virtual {v0}, Ljava/lang/Long;->longValue()J

    move-result-wide v0

    return-wide v0
.end method


# virtual methods
.method public toString()Ljava/lang/String;
    .locals 2

    .line 529
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "("

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget v1, p0, Lcom/miui/server/smartpower/PowerFrozenManager$FrozenInfo;->mPid:I

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ")"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lcom/miui/server/smartpower/PowerFrozenManager$FrozenInfo;->mProcessName:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "/"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget v1, p0, Lcom/miui/server/smartpower/PowerFrozenManager$FrozenInfo;->mUid:I

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
