.class Lcom/miui/server/smartpower/AppAudioResource;
.super Lcom/miui/server/smartpower/AppPowerResource;
.source "AppAudioResource.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/miui/server/smartpower/AppAudioResource$AudioRecord;
    }
.end annotation


# static fields
.field private static final AUDIO_INACTIVE_DELAY_TIME:J = 0x7d0L

.field public static final PLAYER_STATE_ZERO_PLAYER:I = 0x64


# instance fields
.field private final mActivePidsMap:Landroid/util/SparseArray;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/util/SparseArray<",
            "Lcom/miui/server/smartpower/AppAudioResource$AudioRecord;",
            ">;"
        }
    .end annotation
.end field

.field private mAudioManager:Landroid/media/AudioManager;

.field private mHandler:Landroid/os/Handler;

.field private mLastMusicPlayPid:J

.field private mLastMusicPlayTimeStamp:J


# direct methods
.method static bridge synthetic -$$Nest$fgetmAudioManager(Lcom/miui/server/smartpower/AppAudioResource;)Landroid/media/AudioManager;
    .locals 0

    iget-object p0, p0, Lcom/miui/server/smartpower/AppAudioResource;->mAudioManager:Landroid/media/AudioManager;

    return-object p0
.end method

.method static bridge synthetic -$$Nest$fgetmHandler(Lcom/miui/server/smartpower/AppAudioResource;)Landroid/os/Handler;
    .locals 0

    iget-object p0, p0, Lcom/miui/server/smartpower/AppAudioResource;->mHandler:Landroid/os/Handler;

    return-object p0
.end method

.method static bridge synthetic -$$Nest$fputmLastMusicPlayPid(Lcom/miui/server/smartpower/AppAudioResource;J)V
    .locals 0

    iput-wide p1, p0, Lcom/miui/server/smartpower/AppAudioResource;->mLastMusicPlayPid:J

    return-void
.end method

.method static bridge synthetic -$$Nest$fputmLastMusicPlayTimeStamp(Lcom/miui/server/smartpower/AppAudioResource;J)V
    .locals 0

    iput-wide p1, p0, Lcom/miui/server/smartpower/AppAudioResource;->mLastMusicPlayTimeStamp:J

    return-void
.end method

.method static bridge synthetic -$$Nest$mupdateUidStatus(Lcom/miui/server/smartpower/AppAudioResource;II)V
    .locals 0

    invoke-direct {p0, p1, p2}, Lcom/miui/server/smartpower/AppAudioResource;->updateUidStatus(II)V

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/os/Looper;)V
    .locals 1
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "looper"    # Landroid/os/Looper;

    .line 32
    invoke-direct {p0}, Lcom/miui/server/smartpower/AppPowerResource;-><init>()V

    .line 22
    new-instance v0, Landroid/util/SparseArray;

    invoke-direct {v0}, Landroid/util/SparseArray;-><init>()V

    iput-object v0, p0, Lcom/miui/server/smartpower/AppAudioResource;->mActivePidsMap:Landroid/util/SparseArray;

    .line 33
    const/4 v0, 0x1

    iput v0, p0, Lcom/miui/server/smartpower/AppAudioResource;->mType:I

    .line 34
    const-string v0, "audio"

    invoke-virtual {p1, v0}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/media/AudioManager;

    iput-object v0, p0, Lcom/miui/server/smartpower/AppAudioResource;->mAudioManager:Landroid/media/AudioManager;

    .line 35
    new-instance v0, Landroid/os/Handler;

    invoke-direct {v0, p2}, Landroid/os/Handler;-><init>(Landroid/os/Looper;)V

    iput-object v0, p0, Lcom/miui/server/smartpower/AppAudioResource;->mHandler:Landroid/os/Handler;

    .line 36
    return-void
.end method

.method private getAudioRecord(I)Lcom/miui/server/smartpower/AppAudioResource$AudioRecord;
    .locals 2
    .param p1, "pid"    # I

    .line 39
    iget-object v0, p0, Lcom/miui/server/smartpower/AppAudioResource;->mActivePidsMap:Landroid/util/SparseArray;

    monitor-enter v0

    .line 40
    :try_start_0
    iget-object v1, p0, Lcom/miui/server/smartpower/AppAudioResource;->mActivePidsMap:Landroid/util/SparseArray;

    invoke-virtual {v1, p1}, Landroid/util/SparseArray;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/miui/server/smartpower/AppAudioResource$AudioRecord;

    monitor-exit v0

    return-object v1

    .line 41
    :catchall_0
    move-exception v1

    monitor-exit v0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v1
.end method

.method private getAudioRecord(II)Lcom/miui/server/smartpower/AppAudioResource$AudioRecord;
    .locals 4
    .param p1, "uid"    # I
    .param p2, "riid"    # I

    .line 57
    iget-object v0, p0, Lcom/miui/server/smartpower/AppAudioResource;->mActivePidsMap:Landroid/util/SparseArray;

    monitor-enter v0

    .line 58
    const/4 v1, 0x0

    .local v1, "i":I
    :goto_0
    :try_start_0
    iget-object v2, p0, Lcom/miui/server/smartpower/AppAudioResource;->mActivePidsMap:Landroid/util/SparseArray;

    invoke-virtual {v2}, Landroid/util/SparseArray;->size()I

    move-result v2

    if-ge v1, v2, :cond_1

    .line 59
    iget-object v2, p0, Lcom/miui/server/smartpower/AppAudioResource;->mActivePidsMap:Landroid/util/SparseArray;

    invoke-virtual {v2, v1}, Landroid/util/SparseArray;->valueAt(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/miui/server/smartpower/AppAudioResource$AudioRecord;

    .line 60
    .local v2, "record":Lcom/miui/server/smartpower/AppAudioResource$AudioRecord;
    iget v3, v2, Lcom/miui/server/smartpower/AppAudioResource$AudioRecord;->mOwnerUid:I

    if-ne v3, p1, :cond_0

    invoke-virtual {v2, p2}, Lcom/miui/server/smartpower/AppAudioResource$AudioRecord;->containsRecorder(I)Z

    move-result v3

    if-eqz v3, :cond_0

    .line 61
    monitor-exit v0

    return-object v2

    .line 58
    .end local v2    # "record":Lcom/miui/server/smartpower/AppAudioResource$AudioRecord;
    :cond_0
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 64
    .end local v1    # "i":I
    :cond_1
    monitor-exit v0

    .line 65
    const/4 v0, 0x0

    return-object v0

    .line 64
    :catchall_0
    move-exception v1

    monitor-exit v0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v1
.end method

.method private getAudioRecord(ILjava/lang/String;)Lcom/miui/server/smartpower/AppAudioResource$AudioRecord;
    .locals 4
    .param p1, "uid"    # I
    .param p2, "clientId"    # Ljava/lang/String;

    .line 45
    iget-object v0, p0, Lcom/miui/server/smartpower/AppAudioResource;->mActivePidsMap:Landroid/util/SparseArray;

    monitor-enter v0

    .line 46
    const/4 v1, 0x0

    .local v1, "i":I
    :goto_0
    :try_start_0
    iget-object v2, p0, Lcom/miui/server/smartpower/AppAudioResource;->mActivePidsMap:Landroid/util/SparseArray;

    invoke-virtual {v2}, Landroid/util/SparseArray;->size()I

    move-result v2

    if-ge v1, v2, :cond_1

    .line 47
    iget-object v2, p0, Lcom/miui/server/smartpower/AppAudioResource;->mActivePidsMap:Landroid/util/SparseArray;

    invoke-virtual {v2, v1}, Landroid/util/SparseArray;->valueAt(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/miui/server/smartpower/AppAudioResource$AudioRecord;

    .line 48
    .local v2, "record":Lcom/miui/server/smartpower/AppAudioResource$AudioRecord;
    iget v3, v2, Lcom/miui/server/smartpower/AppAudioResource$AudioRecord;->mOwnerUid:I

    if-ne v3, p1, :cond_0

    invoke-virtual {v2, p2}, Lcom/miui/server/smartpower/AppAudioResource$AudioRecord;->containsClientId(Ljava/lang/String;)Z

    move-result v3

    if-eqz v3, :cond_0

    .line 49
    monitor-exit v0

    return-object v2

    .line 46
    .end local v2    # "record":Lcom/miui/server/smartpower/AppAudioResource$AudioRecord;
    :cond_0
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 52
    .end local v1    # "i":I
    :cond_1
    monitor-exit v0

    .line 53
    const/4 v0, 0x0

    return-object v0

    .line 52
    :catchall_0
    move-exception v1

    monitor-exit v0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v1
.end method

.method private getOrCreateAudioRecord(II)Lcom/miui/server/smartpower/AppAudioResource$AudioRecord;
    .locals 3
    .param p1, "uid"    # I
    .param p2, "pid"    # I

    .line 69
    iget-object v0, p0, Lcom/miui/server/smartpower/AppAudioResource;->mActivePidsMap:Landroid/util/SparseArray;

    monitor-enter v0

    .line 70
    :try_start_0
    invoke-direct {p0, p2}, Lcom/miui/server/smartpower/AppAudioResource;->getAudioRecord(I)Lcom/miui/server/smartpower/AppAudioResource$AudioRecord;

    move-result-object v1

    .line 71
    .local v1, "record":Lcom/miui/server/smartpower/AppAudioResource$AudioRecord;
    if-nez v1, :cond_0

    .line 72
    new-instance v2, Lcom/miui/server/smartpower/AppAudioResource$AudioRecord;

    invoke-direct {v2, p0, p1, p2}, Lcom/miui/server/smartpower/AppAudioResource$AudioRecord;-><init>(Lcom/miui/server/smartpower/AppAudioResource;II)V

    move-object v1, v2

    .line 73
    iget-object v2, p0, Lcom/miui/server/smartpower/AppAudioResource;->mActivePidsMap:Landroid/util/SparseArray;

    invoke-virtual {v2, p2, v1}, Landroid/util/SparseArray;->put(ILjava/lang/Object;)V

    .line 75
    :cond_0
    monitor-exit v0

    return-object v1

    .line 76
    .end local v1    # "record":Lcom/miui/server/smartpower/AppAudioResource$AudioRecord;
    :catchall_0
    move-exception v1

    monitor-exit v0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v1
.end method

.method private updateUidStatus(II)V
    .locals 1
    .param p1, "uid"    # I
    .param p2, "behavier"    # I

    .line 211
    invoke-virtual {p0, p1}, Lcom/miui/server/smartpower/AppAudioResource;->isAppResourceActive(I)Z

    move-result v0

    .line 212
    .local v0, "active":Z
    invoke-virtual {p0, p1, v0, p2}, Lcom/miui/server/smartpower/AppAudioResource;->reportResourceStatus(IZI)V

    .line 213
    return-void
.end method


# virtual methods
.method public getActiveUids()Ljava/util/ArrayList;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/ArrayList<",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation

    .line 81
    const/4 v0, 0x0

    return-object v0
.end method

.method public getLastMusicPlayTimeStamp(I)J
    .locals 4
    .param p1, "pid"    # I

    .line 108
    iget-wide v0, p0, Lcom/miui/server/smartpower/AppAudioResource;->mLastMusicPlayPid:J

    int-to-long v2, p1

    cmp-long v0, v0, v2

    if-nez v0, :cond_0

    .line 109
    iget-wide v0, p0, Lcom/miui/server/smartpower/AppAudioResource;->mLastMusicPlayTimeStamp:J

    return-wide v0

    .line 111
    :cond_0
    const-wide/16 v0, 0x0

    return-wide v0
.end method

.method public isAppResourceActive(I)Z
    .locals 5
    .param p1, "uid"    # I

    .line 86
    const/4 v0, 0x0

    .line 87
    .local v0, "active":Z
    iget-object v1, p0, Lcom/miui/server/smartpower/AppAudioResource;->mActivePidsMap:Landroid/util/SparseArray;

    monitor-enter v1

    .line 88
    const/4 v2, 0x0

    .local v2, "i":I
    :goto_0
    :try_start_0
    iget-object v3, p0, Lcom/miui/server/smartpower/AppAudioResource;->mActivePidsMap:Landroid/util/SparseArray;

    invoke-virtual {v3}, Landroid/util/SparseArray;->size()I

    move-result v3

    if-ge v2, v3, :cond_1

    .line 89
    iget-object v3, p0, Lcom/miui/server/smartpower/AppAudioResource;->mActivePidsMap:Landroid/util/SparseArray;

    invoke-virtual {v3, v2}, Landroid/util/SparseArray;->valueAt(I)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/miui/server/smartpower/AppAudioResource$AudioRecord;

    .line 90
    .local v3, "record":Lcom/miui/server/smartpower/AppAudioResource$AudioRecord;
    if-eqz v3, :cond_0

    iget v4, v3, Lcom/miui/server/smartpower/AppAudioResource$AudioRecord;->mOwnerUid:I

    if-ne v4, p1, :cond_0

    iget-boolean v4, v3, Lcom/miui/server/smartpower/AppAudioResource$AudioRecord;->mActive:Z

    if-eqz v4, :cond_0

    .line 91
    monitor-exit v1

    const/4 v1, 0x1

    return v1

    .line 88
    .end local v3    # "record":Lcom/miui/server/smartpower/AppAudioResource$AudioRecord;
    :cond_0
    add-int/lit8 v2, v2, 0x1

    goto :goto_0

    .line 94
    .end local v2    # "i":I
    :cond_1
    monitor-exit v1

    .line 95
    return v0

    .line 94
    :catchall_0
    move-exception v2

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v2
.end method

.method public isAppResourceActive(II)Z
    .locals 2
    .param p1, "uid"    # I
    .param p2, "pid"    # I

    .line 100
    invoke-direct {p0, p2}, Lcom/miui/server/smartpower/AppAudioResource;->getAudioRecord(I)Lcom/miui/server/smartpower/AppAudioResource$AudioRecord;

    move-result-object v0

    .line 101
    .local v0, "record":Lcom/miui/server/smartpower/AppAudioResource$AudioRecord;
    if-eqz v0, :cond_0

    .line 102
    iget-boolean v1, v0, Lcom/miui/server/smartpower/AppAudioResource$AudioRecord;->mActive:Z

    return v1

    .line 104
    :cond_0
    const/4 v1, 0x0

    return v1
.end method

.method public onPlayerEvent(IIII)V
    .locals 1
    .param p1, "uid"    # I
    .param p2, "pid"    # I
    .param p3, "piid"    # I
    .param p4, "event"    # I

    .line 163
    invoke-direct {p0, p2}, Lcom/miui/server/smartpower/AppAudioResource;->getAudioRecord(I)Lcom/miui/server/smartpower/AppAudioResource$AudioRecord;

    move-result-object v0

    .line 164
    .local v0, "record":Lcom/miui/server/smartpower/AppAudioResource$AudioRecord;
    if-eqz v0, :cond_0

    .line 165
    invoke-virtual {v0, p3, p4}, Lcom/miui/server/smartpower/AppAudioResource$AudioRecord;->onPlayerEvent(II)V

    .line 167
    :cond_0
    return-void
.end method

.method public onPlayerRlease(III)V
    .locals 1
    .param p1, "uid"    # I
    .param p2, "pid"    # I
    .param p3, "piid"    # I

    .line 156
    invoke-direct {p0, p2}, Lcom/miui/server/smartpower/AppAudioResource;->getAudioRecord(I)Lcom/miui/server/smartpower/AppAudioResource$AudioRecord;

    move-result-object v0

    .line 157
    .local v0, "record":Lcom/miui/server/smartpower/AppAudioResource$AudioRecord;
    if-eqz v0, :cond_0

    .line 158
    invoke-virtual {v0, p3}, Lcom/miui/server/smartpower/AppAudioResource$AudioRecord;->onPlayerRlease(I)V

    .line 160
    :cond_0
    return-void
.end method

.method public onPlayerTrack(IIII)V
    .locals 1
    .param p1, "uid"    # I
    .param p2, "pid"    # I
    .param p3, "piid"    # I
    .param p4, "sessionId"    # I

    .line 151
    invoke-direct {p0, p1, p2}, Lcom/miui/server/smartpower/AppAudioResource;->getOrCreateAudioRecord(II)Lcom/miui/server/smartpower/AppAudioResource$AudioRecord;

    move-result-object v0

    .line 152
    .local v0, "record":Lcom/miui/server/smartpower/AppAudioResource$AudioRecord;
    invoke-virtual {v0, p3, p4}, Lcom/miui/server/smartpower/AppAudioResource$AudioRecord;->onPlayerTrack(II)V

    .line 153
    return-void
.end method

.method public onRecorderEvent(III)V
    .locals 1
    .param p1, "uid"    # I
    .param p2, "riid"    # I
    .param p3, "event"    # I

    .line 182
    invoke-direct {p0, p1, p2}, Lcom/miui/server/smartpower/AppAudioResource;->getAudioRecord(II)Lcom/miui/server/smartpower/AppAudioResource$AudioRecord;

    move-result-object v0

    .line 183
    .local v0, "record":Lcom/miui/server/smartpower/AppAudioResource$AudioRecord;
    if-eqz v0, :cond_0

    .line 184
    invoke-virtual {v0, p2, p3}, Lcom/miui/server/smartpower/AppAudioResource$AudioRecord;->onRecorderEvent(II)V

    .line 186
    :cond_0
    return-void
.end method

.method public onRecorderRlease(II)V
    .locals 1
    .param p1, "uid"    # I
    .param p2, "riid"    # I

    .line 175
    invoke-direct {p0, p1, p2}, Lcom/miui/server/smartpower/AppAudioResource;->getAudioRecord(II)Lcom/miui/server/smartpower/AppAudioResource$AudioRecord;

    move-result-object v0

    .line 176
    .local v0, "record":Lcom/miui/server/smartpower/AppAudioResource$AudioRecord;
    if-eqz v0, :cond_0

    .line 177
    invoke-virtual {v0, p2}, Lcom/miui/server/smartpower/AppAudioResource$AudioRecord;->onRecorderRlease(I)V

    .line 179
    :cond_0
    return-void
.end method

.method public onRecorderTrack(III)V
    .locals 1
    .param p1, "uid"    # I
    .param p2, "pid"    # I
    .param p3, "riid"    # I

    .line 170
    invoke-direct {p0, p1, p2}, Lcom/miui/server/smartpower/AppAudioResource;->getOrCreateAudioRecord(II)Lcom/miui/server/smartpower/AppAudioResource$AudioRecord;

    move-result-object v0

    .line 171
    .local v0, "record":Lcom/miui/server/smartpower/AppAudioResource$AudioRecord;
    invoke-virtual {v0, p3}, Lcom/miui/server/smartpower/AppAudioResource$AudioRecord;->onRecorderTrack(I)V

    .line 172
    return-void
.end method

.method public playbackStateChanged(IIII)V
    .locals 1
    .param p1, "uid"    # I
    .param p2, "pid"    # I
    .param p3, "oldState"    # I
    .param p4, "newState"    # I

    .line 132
    invoke-direct {p0, p2}, Lcom/miui/server/smartpower/AppAudioResource;->getAudioRecord(I)Lcom/miui/server/smartpower/AppAudioResource$AudioRecord;

    move-result-object v0

    .line 133
    .local v0, "record":Lcom/miui/server/smartpower/AppAudioResource$AudioRecord;
    if-eqz v0, :cond_0

    .line 134
    invoke-virtual {v0, p3, p4}, Lcom/miui/server/smartpower/AppAudioResource$AudioRecord;->playbackStateChanged(II)V

    .line 136
    :cond_0
    return-void
.end method

.method public recordAudioFocus(IILjava/lang/String;Z)V
    .locals 1
    .param p1, "uid"    # I
    .param p2, "pid"    # I
    .param p3, "clientId"    # Ljava/lang/String;
    .param p4, "request"    # Z

    .line 139
    invoke-direct {p0, p1, p2}, Lcom/miui/server/smartpower/AppAudioResource;->getOrCreateAudioRecord(II)Lcom/miui/server/smartpower/AppAudioResource$AudioRecord;

    move-result-object v0

    .line 140
    .local v0, "record":Lcom/miui/server/smartpower/AppAudioResource$AudioRecord;
    invoke-virtual {v0, p4, p3}, Lcom/miui/server/smartpower/AppAudioResource$AudioRecord;->recordAudioFocus(ZLjava/lang/String;)V

    .line 141
    return-void
.end method

.method public recordAudioFocusLoss(ILjava/lang/String;I)V
    .locals 1
    .param p1, "uid"    # I
    .param p2, "clientId"    # Ljava/lang/String;
    .param p3, "focusLoss"    # I

    .line 144
    invoke-direct {p0, p1, p2}, Lcom/miui/server/smartpower/AppAudioResource;->getAudioRecord(ILjava/lang/String;)Lcom/miui/server/smartpower/AppAudioResource$AudioRecord;

    move-result-object v0

    .line 145
    .local v0, "record":Lcom/miui/server/smartpower/AppAudioResource$AudioRecord;
    if-eqz v0, :cond_0

    .line 146
    invoke-virtual {v0, p2, p3}, Lcom/miui/server/smartpower/AppAudioResource$AudioRecord;->recordAudioFocusLoss(Ljava/lang/String;I)V

    .line 148
    :cond_0
    return-void
.end method

.method public releaseAppPowerResource(I)V
    .locals 4
    .param p1, "uid"    # I

    .line 116
    iget-object v0, p0, Lcom/miui/server/smartpower/AppAudioResource;->mActivePidsMap:Landroid/util/SparseArray;

    monitor-enter v0

    .line 117
    const/4 v1, 0x0

    .local v1, "i":I
    :goto_0
    :try_start_0
    iget-object v2, p0, Lcom/miui/server/smartpower/AppAudioResource;->mActivePidsMap:Landroid/util/SparseArray;

    invoke-virtual {v2}, Landroid/util/SparseArray;->size()I

    move-result v2

    if-ge v1, v2, :cond_1

    .line 118
    iget-object v2, p0, Lcom/miui/server/smartpower/AppAudioResource;->mActivePidsMap:Landroid/util/SparseArray;

    invoke-virtual {v2, v1}, Landroid/util/SparseArray;->valueAt(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/miui/server/smartpower/AppAudioResource$AudioRecord;

    .line 119
    .local v2, "record":Lcom/miui/server/smartpower/AppAudioResource$AudioRecord;
    if-eqz v2, :cond_0

    iget v3, v2, Lcom/miui/server/smartpower/AppAudioResource$AudioRecord;->mOwnerUid:I

    if-ne v3, p1, :cond_0

    iget-boolean v3, v2, Lcom/miui/server/smartpower/AppAudioResource$AudioRecord;->mActive:Z

    if-nez v3, :cond_0

    .line 120
    invoke-static {v2}, Lcom/miui/server/smartpower/AppAudioResource$AudioRecord;->-$$Nest$mpauseZeroAudioTrack(Lcom/miui/server/smartpower/AppAudioResource$AudioRecord;)V

    .line 117
    .end local v2    # "record":Lcom/miui/server/smartpower/AppAudioResource$AudioRecord;
    :cond_0
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 123
    .end local v1    # "i":I
    :cond_1
    monitor-exit v0

    .line 124
    return-void

    .line 123
    :catchall_0
    move-exception v1

    monitor-exit v0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v1
.end method

.method public reportTrackStatus(IIIZ)V
    .locals 1
    .param p1, "uid"    # I
    .param p2, "pid"    # I
    .param p3, "sessionId"    # I
    .param p4, "isMuted"    # Z

    .line 189
    invoke-direct {p0, p2}, Lcom/miui/server/smartpower/AppAudioResource;->getAudioRecord(I)Lcom/miui/server/smartpower/AppAudioResource$AudioRecord;

    move-result-object v0

    .line 190
    .local v0, "record":Lcom/miui/server/smartpower/AppAudioResource$AudioRecord;
    if-eqz v0, :cond_0

    .line 191
    invoke-virtual {v0, p3, p4}, Lcom/miui/server/smartpower/AppAudioResource$AudioRecord;->reportTrackStatus(IZ)V

    .line 193
    :cond_0
    return-void
.end method

.method public resumeAppPowerResource(I)V
    .locals 0
    .param p1, "uid"    # I

    .line 129
    return-void
.end method

.method public uidAudioStatusChanged(IZ)V
    .locals 4
    .param p1, "uid"    # I
    .param p2, "active"    # Z

    .line 196
    if-nez p2, :cond_2

    .line 197
    iget-object v0, p0, Lcom/miui/server/smartpower/AppAudioResource;->mActivePidsMap:Landroid/util/SparseArray;

    monitor-enter v0

    .line 198
    const/4 v1, 0x0

    .local v1, "i":I
    :goto_0
    :try_start_0
    iget-object v2, p0, Lcom/miui/server/smartpower/AppAudioResource;->mActivePidsMap:Landroid/util/SparseArray;

    invoke-virtual {v2}, Landroid/util/SparseArray;->size()I

    move-result v2

    if-ge v1, v2, :cond_1

    .line 199
    iget-object v2, p0, Lcom/miui/server/smartpower/AppAudioResource;->mActivePidsMap:Landroid/util/SparseArray;

    invoke-virtual {v2, v1}, Landroid/util/SparseArray;->valueAt(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/miui/server/smartpower/AppAudioResource$AudioRecord;

    .line 200
    .local v2, "record":Lcom/miui/server/smartpower/AppAudioResource$AudioRecord;
    if-eqz v2, :cond_0

    iget v3, v2, Lcom/miui/server/smartpower/AppAudioResource$AudioRecord;->mOwnerUid:I

    if-ne v3, p1, :cond_0

    .line 201
    invoke-virtual {v2, p2}, Lcom/miui/server/smartpower/AppAudioResource$AudioRecord;->uidAudioStatusChanged(Z)V

    .line 198
    .end local v2    # "record":Lcom/miui/server/smartpower/AppAudioResource$AudioRecord;
    :cond_0
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 204
    .end local v1    # "i":I
    :cond_1
    monitor-exit v0

    goto :goto_1

    :catchall_0
    move-exception v1

    monitor-exit v0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v1

    .line 206
    :cond_2
    :goto_1
    return-void
.end method

.method public uidVideoStatusChanged(IZ)V
    .locals 0
    .param p1, "uid"    # I
    .param p2, "active"    # Z

    .line 208
    return-void
.end method
