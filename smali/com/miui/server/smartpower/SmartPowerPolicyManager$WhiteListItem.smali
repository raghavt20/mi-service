.class public Lcom/miui/server/smartpower/SmartPowerPolicyManager$WhiteListItem;
.super Ljava/lang/Object;
.source "SmartPowerPolicyManager.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/miui/server/smartpower/SmartPowerPolicyManager;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x1
    name = "WhiteListItem"
.end annotation


# instance fields
.field private mActions:I

.field private mName:Ljava/lang/String;

.field private mPid:I

.field private mTypes:I

.field private mUid:I

.field final synthetic this$0:Lcom/miui/server/smartpower/SmartPowerPolicyManager;


# direct methods
.method static bridge synthetic -$$Nest$fgetmActions(Lcom/miui/server/smartpower/SmartPowerPolicyManager$WhiteListItem;)I
    .locals 0

    iget p0, p0, Lcom/miui/server/smartpower/SmartPowerPolicyManager$WhiteListItem;->mActions:I

    return p0
.end method

.method static bridge synthetic -$$Nest$fgetmName(Lcom/miui/server/smartpower/SmartPowerPolicyManager$WhiteListItem;)Ljava/lang/String;
    .locals 0

    iget-object p0, p0, Lcom/miui/server/smartpower/SmartPowerPolicyManager$WhiteListItem;->mName:Ljava/lang/String;

    return-object p0
.end method

.method static bridge synthetic -$$Nest$fgetmPid(Lcom/miui/server/smartpower/SmartPowerPolicyManager$WhiteListItem;)I
    .locals 0

    iget p0, p0, Lcom/miui/server/smartpower/SmartPowerPolicyManager$WhiteListItem;->mPid:I

    return p0
.end method

.method static bridge synthetic -$$Nest$fgetmTypes(Lcom/miui/server/smartpower/SmartPowerPolicyManager$WhiteListItem;)I
    .locals 0

    iget p0, p0, Lcom/miui/server/smartpower/SmartPowerPolicyManager$WhiteListItem;->mTypes:I

    return p0
.end method

.method static bridge synthetic -$$Nest$fgetmUid(Lcom/miui/server/smartpower/SmartPowerPolicyManager$WhiteListItem;)I
    .locals 0

    iget p0, p0, Lcom/miui/server/smartpower/SmartPowerPolicyManager$WhiteListItem;->mUid:I

    return p0
.end method

.method static bridge synthetic -$$Nest$fputmActions(Lcom/miui/server/smartpower/SmartPowerPolicyManager$WhiteListItem;I)V
    .locals 0

    iput p1, p0, Lcom/miui/server/smartpower/SmartPowerPolicyManager$WhiteListItem;->mActions:I

    return-void
.end method

.method static bridge synthetic -$$Nest$fputmPid(Lcom/miui/server/smartpower/SmartPowerPolicyManager$WhiteListItem;I)V
    .locals 0

    iput p1, p0, Lcom/miui/server/smartpower/SmartPowerPolicyManager$WhiteListItem;->mPid:I

    return-void
.end method

.method static bridge synthetic -$$Nest$fputmTypes(Lcom/miui/server/smartpower/SmartPowerPolicyManager$WhiteListItem;I)V
    .locals 0

    iput p1, p0, Lcom/miui/server/smartpower/SmartPowerPolicyManager$WhiteListItem;->mTypes:I

    return-void
.end method

.method static bridge synthetic -$$Nest$fputmUid(Lcom/miui/server/smartpower/SmartPowerPolicyManager$WhiteListItem;I)V
    .locals 0

    iput p1, p0, Lcom/miui/server/smartpower/SmartPowerPolicyManager$WhiteListItem;->mUid:I

    return-void
.end method

.method constructor <init>(Lcom/miui/server/smartpower/SmartPowerPolicyManager;Ljava/lang/String;II)V
    .locals 1
    .param p1, "this$0"    # Lcom/miui/server/smartpower/SmartPowerPolicyManager;
    .param p2, "name"    # Ljava/lang/String;
    .param p3, "type"    # I
    .param p4, "action"    # I

    .line 1548
    iput-object p1, p0, Lcom/miui/server/smartpower/SmartPowerPolicyManager$WhiteListItem;->this$0:Lcom/miui/server/smartpower/SmartPowerPolicyManager;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1542
    const/4 v0, -0x1

    iput v0, p0, Lcom/miui/server/smartpower/SmartPowerPolicyManager$WhiteListItem;->mUid:I

    .line 1543
    iput v0, p0, Lcom/miui/server/smartpower/SmartPowerPolicyManager$WhiteListItem;->mPid:I

    .line 1549
    iput-object p2, p0, Lcom/miui/server/smartpower/SmartPowerPolicyManager$WhiteListItem;->mName:Ljava/lang/String;

    .line 1550
    iput p3, p0, Lcom/miui/server/smartpower/SmartPowerPolicyManager$WhiteListItem;->mTypes:I

    .line 1551
    iput p4, p0, Lcom/miui/server/smartpower/SmartPowerPolicyManager$WhiteListItem;->mActions:I

    .line 1552
    return-void
.end method


# virtual methods
.method public hasAction(I)Z
    .locals 1
    .param p1, "action"    # I

    .line 1559
    iget v0, p0, Lcom/miui/server/smartpower/SmartPowerPolicyManager$WhiteListItem;->mActions:I

    and-int/2addr v0, p1

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    :goto_0
    return v0
.end method

.method public hasWhiteListType(I)Z
    .locals 1
    .param p1, "type"    # I

    .line 1555
    iget v0, p0, Lcom/miui/server/smartpower/SmartPowerPolicyManager$WhiteListItem;->mTypes:I

    and-int/2addr v0, p1

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    :goto_0
    return v0
.end method

.method public toString()Ljava/lang/String;
    .locals 3

    .line 1564
    new-instance v0, Ljava/lang/StringBuilder;

    const/16 v1, 0x80

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(I)V

    .line 1565
    .local v0, "sb":Ljava/lang/StringBuilder;
    iget-object v1, p0, Lcom/miui/server/smartpower/SmartPowerPolicyManager$WhiteListItem;->mName:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1566
    iget v1, p0, Lcom/miui/server/smartpower/SmartPowerPolicyManager$WhiteListItem;->mUid:I

    const/4 v2, -0x1

    if-eq v1, v2, :cond_1

    .line 1567
    const-string v1, "(u:"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1568
    iget v1, p0, Lcom/miui/server/smartpower/SmartPowerPolicyManager$WhiteListItem;->mUid:I

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    .line 1569
    iget v1, p0, Lcom/miui/server/smartpower/SmartPowerPolicyManager$WhiteListItem;->mPid:I

    if-eq v1, v2, :cond_0

    .line 1570
    const-string v1, ", p:"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1571
    iget v1, p0, Lcom/miui/server/smartpower/SmartPowerPolicyManager$WhiteListItem;->mPid:I

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    .line 1573
    :cond_0
    const-string v1, ")"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1575
    :cond_1
    const-string v1, " {"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1576
    iget v1, p0, Lcom/miui/server/smartpower/SmartPowerPolicyManager$WhiteListItem;->mTypes:I

    invoke-static {v1}, Lcom/miui/server/smartpower/SmartPowerPolicyManager;->-$$Nest$smwhiteListTypeToString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1577
    const-string/jumbo v1, "} {"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1578
    iget v1, p0, Lcom/miui/server/smartpower/SmartPowerPolicyManager$WhiteListItem;->mActions:I

    invoke-static {v1}, Lcom/miui/server/smartpower/SmartPowerPolicyManager;->-$$Nest$smwhiteListActionToString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1579
    const-string/jumbo v1, "}"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1580
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    return-object v1
.end method
