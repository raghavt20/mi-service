class com.miui.server.smartpower.SmartPowerPolicyManager$PowerSavingStrategyControl$PolicyChangeObserver extends android.database.ContentObserver {
	 /* .source "SmartPowerPolicyManager.java" */
	 /* # annotations */
	 /* .annotation system Ldalvik/annotation/EnclosingClass; */
	 /* value = Lcom/miui/server/smartpower/SmartPowerPolicyManager$PowerSavingStrategyControl; */
} // .end annotation
/* .annotation system Ldalvik/annotation/InnerClass; */
/* accessFlags = 0x2 */
/* name = "PolicyChangeObserver" */
} // .end annotation
/* # instance fields */
final com.miui.server.smartpower.SmartPowerPolicyManager$PowerSavingStrategyControl this$1; //synthetic
/* # direct methods */
public com.miui.server.smartpower.SmartPowerPolicyManager$PowerSavingStrategyControl$PolicyChangeObserver ( ) {
/* .locals 0 */
/* .param p2, "handler" # Landroid/os/Handler; */
/* .line 1690 */
this.this$1 = p1;
/* .line 1691 */
/* invoke-direct {p0, p2}, Landroid/database/ContentObserver;-><init>(Landroid/os/Handler;)V */
/* .line 1692 */
return;
} // .end method
/* # virtual methods */
public void onChange ( Boolean p0, android.net.Uri p1 ) {
/* .locals 8 */
/* .param p1, "selfChange" # Z */
/* .param p2, "uri" # Landroid/net/Uri; */
/* .line 1696 */
/* invoke-super {p0, p1, p2}, Landroid/database/ContentObserver;->onChange(ZLandroid/net/Uri;)V */
/* .line 1697 */
v0 = this.this$1;
v0 = this.this$0;
com.miui.server.smartpower.SmartPowerPolicyManager .-$$Nest$fgetmContext ( v0 );
v1 = android.os.UserHandle.SYSTEM;
(( android.content.Context ) v0 ).getContentResolverForUser ( v1 ); // invoke-virtual {v0, v1}, Landroid/content/Context;->getContentResolverForUser(Landroid/os/UserHandle;)Landroid/content/ContentResolver;
/* .line 1698 */
/* .local v0, "cr":Landroid/content/ContentResolver; */
int v1 = 0; // const/4 v1, 0x0
/* .line 1700 */
/* .local v1, "cursor":Landroid/database/Cursor; */
int v4 = 0; // const/4 v4, 0x0
int v5 = 0; // const/4 v5, 0x0
int v6 = 0; // const/4 v6, 0x0
int v7 = 0; // const/4 v7, 0x0
/* move-object v2, v0 */
/* move-object v3, p2 */
try { // :try_start_0
	 /* invoke-virtual/range {v2 ..v7}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor; */
	 /* :try_end_0 */
	 /* .catch Ljava/lang/Exception; {:try_start_0 ..:try_end_0} :catch_0 */
	 /* .catchall {:try_start_0 ..:try_end_0} :catchall_0 */
	 /* move-object v1, v2 */
	 /* .line 1701 */
	 /* if-nez v1, :cond_1 */
	 /* .line 1720 */
	 if ( v1 != null) { // if-eqz v1, :cond_0
		 /* .line 1721 */
		 /* .line 1702 */
	 } // :cond_0
	 return;
	 /* .line 1704 */
} // :cond_1
	 v2 = try { // :try_start_1
	 /* if-lez v2, :cond_3 */
	 /* .line 1705 */
	 v2 = 	 final String v2 = "pkgName"; // const-string v2, "pkgName"
	 /* .line 1706 */
	 /* .local v2, "pkgIndex":I */
	 v3 = 	 final String v3 = "bgControl"; // const-string v3, "bgControl"
	 /* .line 1707 */
	 /* .local v3, "bgControlIndex":I */
	 v4 = 	 /* .line 1708 */
	 /* if-nez v4, :cond_3 */
	 /* .line 1709 */
	 /* .line 1710 */
	 /* .local v4, "pkg":Ljava/lang/String; */
	 /* .line 1711 */
	 /* .local v5, "bgControl":Ljava/lang/String; */
	 final String v6 = "noRestrict"; // const-string v6, "noRestrict"
	 v6 = 	 (( java.lang.String ) v6 ).equals ( v5 ); // invoke-virtual {v6, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z
	 if ( v6 != null) { // if-eqz v6, :cond_2
		 /* .line 1712 */
		 v6 = this.this$1;
		 com.miui.server.smartpower.SmartPowerPolicyManager$PowerSavingStrategyControl .-$$Nest$fgetmNoRestrictApps ( v6 );
		 (( android.util.ArraySet ) v6 ).add ( v4 ); // invoke-virtual {v6, v4}, Landroid/util/ArraySet;->add(Ljava/lang/Object;)Z
		 /* .line 1714 */
	 } // :cond_2
	 v6 = this.this$1;
	 com.miui.server.smartpower.SmartPowerPolicyManager$PowerSavingStrategyControl .-$$Nest$fgetmNoRestrictApps ( v6 );
	 (( android.util.ArraySet ) v6 ).remove ( v4 ); // invoke-virtual {v6, v4}, Landroid/util/ArraySet;->remove(Ljava/lang/Object;)Z
	 /* :try_end_1 */
	 /* .catch Ljava/lang/Exception; {:try_start_1 ..:try_end_1} :catch_0 */
	 /* .catchall {:try_start_1 ..:try_end_1} :catchall_0 */
	 /* .line 1720 */
} // .end local v2 # "pkgIndex":I
} // .end local v3 # "bgControlIndex":I
} // .end local v4 # "pkg":Ljava/lang/String;
} // .end local v5 # "bgControl":Ljava/lang/String;
} // :cond_3
} // :goto_0
if ( v1 != null) { // if-eqz v1, :cond_5
/* .line 1721 */
} // :goto_1
/* .line 1720 */
/* :catchall_0 */
/* move-exception v2 */
if ( v1 != null) { // if-eqz v1, :cond_4
/* .line 1721 */
/* .line 1723 */
} // :cond_4
/* throw v2 */
/* .line 1718 */
/* :catch_0 */
/* move-exception v2 */
/* .line 1720 */
if ( v1 != null) { // if-eqz v1, :cond_5
/* .line 1721 */
/* .line 1725 */
} // :cond_5
} // :goto_2
return;
} // .end method
