public class com.miui.server.smartpower.PowerFrozenManager {
	 /* .source "PowerFrozenManager.java" */
	 /* # annotations */
	 /* .annotation system Ldalvik/annotation/MemberClasses; */
	 /* value = { */
	 /* Lcom/miui/server/smartpower/PowerFrozenManager$FrozenInfo;, */
	 /* Lcom/miui/server/smartpower/PowerFrozenManager$H;, */
	 /* Lcom/miui/server/smartpower/PowerFrozenManager$IFrozenReportCallback; */
	 /* } */
} // .end annotation
/* # static fields */
private static final Boolean DEBUG;
private static final Integer HISTORY_SIZE;
private static final Integer MSG_ASYNC_THAW_PID;
public static final java.lang.String TAG;
private static Boolean sEnable;
/* # instance fields */
private final com.miui.server.greeze.GreezeManagerInternal gzInternal;
private final android.util.SparseArray mAllFrozenPids;
/* .annotation system Ldalvik/annotation/Signature; */
/* value = { */
/* "Landroid/util/SparseArray<", */
/* "Ljava/lang/Integer;", */
/* ">;" */
/* } */
} // .end annotation
} // .end field
private java.util.List mFrozenCallbacks;
/* .annotation system Ldalvik/annotation/Signature; */
/* value = { */
/* "Ljava/util/List<", */
/* "Lcom/miui/server/smartpower/PowerFrozenManager$IFrozenReportCallback;", */
/* ">;" */
/* } */
} // .end annotation
} // .end field
private com.miui.server.smartpower.PowerFrozenManager$FrozenInfo mFrozenHistory;
private final android.util.SparseArray mFrozenList;
/* .annotation system Ldalvik/annotation/Signature; */
/* value = { */
/* "Landroid/util/SparseArray<", */
/* "Lcom/miui/server/smartpower/PowerFrozenManager$FrozenInfo;", */
/* ">;" */
/* } */
} // .end annotation
} // .end field
private com.miui.server.smartpower.PowerFrozenManager$H mHandler;
private android.os.HandlerThread mHandlerTh;
private Integer mHistoryIndexNext;
/* # direct methods */
static com.miui.server.smartpower.PowerFrozenManager ( ) {
/* .locals 1 */
/* .line 23 */
/* sget-boolean v0, Lcom/miui/app/smartpower/SmartPowerSettings;->DEBUG_ALL:Z */
com.miui.server.smartpower.PowerFrozenManager.DEBUG = (v0!= 0);
/* .line 29 */
/* sget-boolean v0, Lcom/miui/app/smartpower/SmartPowerSettings;->PROP_FROZEN_ENABLE:Z */
com.miui.server.smartpower.PowerFrozenManager.sEnable = (v0!= 0);
return;
} // .end method
public com.miui.server.smartpower.PowerFrozenManager ( ) {
/* .locals 3 */
/* .line 59 */
/* invoke-direct {p0}, Ljava/lang/Object;-><init>()V */
/* .line 35 */
/* new-instance v0, Landroid/util/SparseArray; */
/* invoke-direct {v0}, Landroid/util/SparseArray;-><init>()V */
this.mAllFrozenPids = v0;
/* .line 40 */
/* new-instance v0, Landroid/util/SparseArray; */
/* invoke-direct {v0}, Landroid/util/SparseArray;-><init>()V */
this.mFrozenList = v0;
/* .line 47 */
/* const/16 v0, 0x1000 */
/* new-array v0, v0, [Lcom/miui/server/smartpower/PowerFrozenManager$FrozenInfo; */
this.mFrozenHistory = v0;
/* .line 48 */
int v0 = 0; // const/4 v0, 0x0
/* iput v0, p0, Lcom/miui/server/smartpower/PowerFrozenManager;->mHistoryIndexNext:I */
/* .line 53 */
/* new-instance v0, Ljava/util/ArrayList; */
/* invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V */
this.mFrozenCallbacks = v0;
/* .line 56 */
/* new-instance v0, Landroid/os/HandlerThread; */
final String v1 = "PowerFrozenTh"; // const-string v1, "PowerFrozenTh"
int v2 = -2; // const/4 v2, -0x2
/* invoke-direct {v0, v1, v2}, Landroid/os/HandlerThread;-><init>(Ljava/lang/String;I)V */
this.mHandlerTh = v0;
/* .line 60 */
(( android.os.HandlerThread ) v0 ).start ( ); // invoke-virtual {v0}, Landroid/os/HandlerThread;->start()V
/* .line 61 */
/* new-instance v0, Lcom/miui/server/smartpower/PowerFrozenManager$H; */
v1 = this.mHandlerTh;
(( android.os.HandlerThread ) v1 ).getLooper ( ); // invoke-virtual {v1}, Landroid/os/HandlerThread;->getLooper()Landroid/os/Looper;
/* invoke-direct {v0, p0, v1}, Lcom/miui/server/smartpower/PowerFrozenManager$H;-><init>(Lcom/miui/server/smartpower/PowerFrozenManager;Landroid/os/Looper;)V */
this.mHandler = v0;
/* .line 62 */
v0 = this.mHandlerTh;
/* .line 63 */
v0 = (( android.os.HandlerThread ) v0 ).getThreadId ( ); // invoke-virtual {v0}, Landroid/os/HandlerThread;->getThreadId()I
/* .line 62 */
int v1 = 1; // const/4 v1, 0x1
android.os.Process .setThreadGroupAndCpuset ( v0,v1 );
/* .line 64 */
com.miui.server.greeze.GreezeManagerInternal .getInstance ( );
this.gzInternal = v0;
/* .line 65 */
return;
} // .end method
private void addFrozenPid ( Integer p0, Integer p1, Boolean p2 ) {
/* .locals 4 */
/* .param p1, "uid" # I */
/* .param p2, "pid" # I */
/* .param p3, "isSmartPower" # Z */
/* .line 252 */
v0 = this.mAllFrozenPids;
/* monitor-enter v0 */
/* .line 253 */
try { // :try_start_0
/* sget-boolean v1, Lcom/miui/server/smartpower/PowerFrozenManager;->DEBUG:Z */
if ( v1 != null) { // if-eqz v1, :cond_0
final String v1 = "PowerFrozen"; // const-string v1, "PowerFrozen"
/* new-instance v2, Ljava/lang/StringBuilder; */
/* invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V */
final String v3 = "add p:"; // const-string v3, "add p:"
(( java.lang.StringBuilder ) v2 ).append ( v3 ); // invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v2 ).append ( p2 ); // invoke-virtual {v2, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;
final String v3 = "/u:"; // const-string v3, "/u:"
(( java.lang.StringBuilder ) v2 ).append ( v3 ); // invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v2 ).append ( p1 ); // invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;
final String v3 = " isMy:"; // const-string v3, " isMy:"
(( java.lang.StringBuilder ) v2 ).append ( v3 ); // invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v2 ).append ( p3 ); // invoke-virtual {v2, p3}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v2 ).toString ( ); // invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
android.util.Slog .d ( v1,v2 );
/* .line 254 */
} // :cond_0
v1 = this.mHandler;
java.lang.Integer .valueOf ( p2 );
int v3 = 1; // const/4 v3, 0x1
v1 = (( com.miui.server.smartpower.PowerFrozenManager$H ) v1 ).hasMessages ( v3, v2 ); // invoke-virtual {v1, v3, v2}, Lcom/miui/server/smartpower/PowerFrozenManager$H;->hasMessages(ILjava/lang/Object;)Z
if ( v1 != null) { // if-eqz v1, :cond_1
/* .line 255 */
v1 = this.mHandler;
java.lang.Integer .valueOf ( p2 );
(( com.miui.server.smartpower.PowerFrozenManager$H ) v1 ).removeMessages ( v3, v2 ); // invoke-virtual {v1, v3, v2}, Lcom/miui/server/smartpower/PowerFrozenManager$H;->removeMessages(ILjava/lang/Object;)V
/* .line 257 */
} // :cond_1
v1 = this.mAllFrozenPids;
java.lang.Integer .valueOf ( p1 );
(( android.util.SparseArray ) v1 ).put ( p2, v2 ); // invoke-virtual {v1, p2, v2}, Landroid/util/SparseArray;->put(ILjava/lang/Object;)V
/* .line 258 */
/* monitor-exit v0 */
/* .line 259 */
return;
/* .line 258 */
/* :catchall_0 */
/* move-exception v1 */
/* monitor-exit v0 */
/* :try_end_0 */
/* .catchall {:try_start_0 ..:try_end_0} :catchall_0 */
/* throw v1 */
} // .end method
private void addHistoryInfo ( com.miui.server.smartpower.PowerFrozenManager$FrozenInfo p0 ) {
/* .locals 3 */
/* .param p1, "info" # Lcom/miui/server/smartpower/PowerFrozenManager$FrozenInfo; */
/* .line 350 */
v0 = this.mFrozenHistory;
/* iget v1, p0, Lcom/miui/server/smartpower/PowerFrozenManager;->mHistoryIndexNext:I */
/* aput-object p1, v0, v1 */
/* .line 351 */
int v0 = 1; // const/4 v0, 0x1
/* const/16 v2, 0x1000 */
v0 = com.miui.server.smartpower.PowerFrozenManager .ringAdvance ( v1,v0,v2 );
/* iput v0, p0, Lcom/miui/server/smartpower/PowerFrozenManager;->mHistoryIndexNext:I */
/* .line 352 */
return;
} // .end method
private void dumpFrozen ( java.io.PrintWriter p0, java.lang.String[] p1, Integer p2, Integer p3 ) {
/* .locals 9 */
/* .param p1, "pw" # Ljava/io/PrintWriter; */
/* .param p2, "args" # [Ljava/lang/String; */
/* .param p3, "opti" # I */
/* .param p4, "dpUid" # I */
/* .line 408 */
final String v0 = "frozen all: "; // const-string v0, "frozen all: "
(( java.io.PrintWriter ) p1 ).println ( v0 ); // invoke-virtual {p1, v0}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V
/* .line 409 */
v0 = this.mAllFrozenPids;
/* monitor-enter v0 */
/* .line 410 */
try { // :try_start_0
v1 = this.mAllFrozenPids;
v1 = (( android.util.SparseArray ) v1 ).size ( ); // invoke-virtual {v1}, Landroid/util/SparseArray;->size()I
/* .line 411 */
/* .local v1, "n":I */
int v2 = 0; // const/4 v2, 0x0
/* .local v2, "i":I */
} // :goto_0
/* if-ge v2, v1, :cond_0 */
/* .line 412 */
v3 = this.mAllFrozenPids;
v3 = (( android.util.SparseArray ) v3 ).keyAt ( v2 ); // invoke-virtual {v3, v2}, Landroid/util/SparseArray;->keyAt(I)I
/* .line 413 */
/* .local v3, "pid":I */
v4 = this.mAllFrozenPids;
(( android.util.SparseArray ) v4 ).valueAt ( v2 ); // invoke-virtual {v4, v2}, Landroid/util/SparseArray;->valueAt(I)Ljava/lang/Object;
/* check-cast v4, Ljava/lang/Integer; */
v4 = (( java.lang.Integer ) v4 ).intValue ( ); // invoke-virtual {v4}, Ljava/lang/Integer;->intValue()I
/* .line 414 */
/* .local v4, "uid":I */
/* new-instance v5, Ljava/lang/StringBuilder; */
/* invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V */
final String v6 = " p:"; // const-string v6, " p:"
(( java.lang.StringBuilder ) v5 ).append ( v6 ); // invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v5 ).append ( v3 ); // invoke-virtual {v5, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;
final String v6 = "/u:"; // const-string v6, "/u:"
(( java.lang.StringBuilder ) v5 ).append ( v6 ); // invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v5 ).append ( v4 ); // invoke-virtual {v5, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v5 ).toString ( ); // invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
(( java.io.PrintWriter ) p1 ).print ( v5 ); // invoke-virtual {p1, v5}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V
/* .line 411 */
} // .end local v3 # "pid":I
} // .end local v4 # "uid":I
/* add-int/lit8 v2, v2, 0x1 */
/* .line 416 */
} // .end local v1 # "n":I
} // .end local v2 # "i":I
} // :cond_0
/* monitor-exit v0 */
/* :try_end_0 */
/* .catchall {:try_start_0 ..:try_end_0} :catchall_1 */
/* .line 417 */
final String v0 = ""; // const-string v0, ""
(( java.io.PrintWriter ) p1 ).println ( v0 ); // invoke-virtual {p1, v0}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V
/* .line 418 */
/* new-instance v0, Ljava/text/SimpleDateFormat; */
final String v1 = "HH:mm:ss.SSS"; // const-string v1, "HH:mm:ss.SSS"
/* invoke-direct {v0, v1}, Ljava/text/SimpleDateFormat;-><init>(Ljava/lang/String;)V */
/* move-object v1, v0 */
/* .line 419 */
/* .local v1, "formater":Ljava/text/SimpleDateFormat; */
/* const-string/jumbo v0, "smart power frozen:" */
(( java.io.PrintWriter ) p1 ).println ( v0 ); // invoke-virtual {p1, v0}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V
/* .line 420 */
v2 = this.mFrozenList;
/* monitor-enter v2 */
/* .line 421 */
try { // :try_start_1
v0 = this.mFrozenList;
v0 = (( android.util.SparseArray ) v0 ).size ( ); // invoke-virtual {v0}, Landroid/util/SparseArray;->size()I
/* .line 422 */
/* .local v0, "n":I */
int v3 = 0; // const/4 v3, 0x0
/* .local v3, "i":I */
} // :goto_1
/* if-ge v3, v0, :cond_3 */
/* .line 423 */
v4 = this.mFrozenList;
(( android.util.SparseArray ) v4 ).valueAt ( v3 ); // invoke-virtual {v4, v3}, Landroid/util/SparseArray;->valueAt(I)Ljava/lang/Object;
/* check-cast v4, Lcom/miui/server/smartpower/PowerFrozenManager$FrozenInfo; */
/* .line 424 */
/* .local v4, "info":Lcom/miui/server/smartpower/PowerFrozenManager$FrozenInfo; */
if ( p4 != null) { // if-eqz p4, :cond_1
v5 = com.miui.server.smartpower.PowerFrozenManager$FrozenInfo .-$$Nest$fgetmUid ( v4 );
/* if-eq v5, p4, :cond_1 */
/* .line 425 */
/* goto/16 :goto_3 */
/* .line 427 */
} // :cond_1
final String v5 = " "; // const-string v5, " "
(( java.io.PrintWriter ) p1 ).print ( v5 ); // invoke-virtual {p1, v5}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V
/* .line 428 */
/* new-instance v5, Ljava/lang/StringBuilder; */
/* invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V */
final String v6 = "#"; // const-string v6, "#"
(( java.lang.StringBuilder ) v5 ).append ( v6 ); // invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
/* add-int/lit8 v6, v3, 0x1 */
(( java.lang.StringBuilder ) v5 ).append ( v6 ); // invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v5 ).toString ( ); // invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
(( java.io.PrintWriter ) p1 ).print ( v5 ); // invoke-virtual {p1, v5}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V
/* .line 429 */
/* new-instance v5, Ljava/lang/StringBuilder; */
/* invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V */
final String v6 = " "; // const-string v6, " "
(( java.lang.StringBuilder ) v5 ).append ( v6 ); // invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v5 ).append ( v4 ); // invoke-virtual {v5, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v5 ).toString ( ); // invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
(( java.io.PrintWriter ) p1 ).println ( v5 ); // invoke-virtual {p1, v5}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V
/* .line 430 */
int v5 = 0; // const/4 v5, 0x0
/* .local v5, "index":I */
} // :goto_2
v6 = com.miui.server.smartpower.PowerFrozenManager$FrozenInfo .-$$Nest$fgetmFrozenTimes ( v4 );
/* if-ge v5, v6, :cond_2 */
/* .line 431 */
final String v6 = " "; // const-string v6, " "
(( java.io.PrintWriter ) p1 ).print ( v6 ); // invoke-virtual {p1, v6}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V
/* .line 432 */
final String v6 = "fz: "; // const-string v6, "fz: "
(( java.io.PrintWriter ) p1 ).print ( v6 ); // invoke-virtual {p1, v6}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V
/* .line 433 */
/* new-instance v6, Ljava/util/Date; */
com.miui.server.smartpower.PowerFrozenManager$FrozenInfo .-$$Nest$fgetmFrozenTimes ( v4 );
/* check-cast v7, Ljava/lang/Long; */
(( java.lang.Long ) v7 ).longValue ( ); // invoke-virtual {v7}, Ljava/lang/Long;->longValue()J
/* move-result-wide v7 */
/* invoke-direct {v6, v7, v8}, Ljava/util/Date;-><init>(J)V */
(( java.text.SimpleDateFormat ) v1 ).format ( v6 ); // invoke-virtual {v1, v6}, Ljava/text/SimpleDateFormat;->format(Ljava/util/Date;)Ljava/lang/String;
(( java.io.PrintWriter ) p1 ).print ( v6 ); // invoke-virtual {p1, v6}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V
/* .line 434 */
/* new-instance v6, Ljava/lang/StringBuilder; */
/* invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V */
final String v7 = " "; // const-string v7, " "
(( java.lang.StringBuilder ) v6 ).append ( v7 ); // invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
com.miui.server.smartpower.PowerFrozenManager$FrozenInfo .-$$Nest$fgetmFrozenReasons ( v4 );
/* check-cast v7, Ljava/lang/String; */
(( java.lang.StringBuilder ) v6 ).append ( v7 ); // invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v6 ).toString ( ); // invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
(( java.io.PrintWriter ) p1 ).print ( v6 ); // invoke-virtual {p1, v6}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V
/* .line 435 */
final String v6 = ""; // const-string v6, ""
(( java.io.PrintWriter ) p1 ).println ( v6 ); // invoke-virtual {p1, v6}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V
/* .line 430 */
/* add-int/lit8 v5, v5, 0x1 */
/* .line 422 */
} // .end local v4 # "info":Lcom/miui/server/smartpower/PowerFrozenManager$FrozenInfo;
} // .end local v5 # "index":I
} // :cond_2
} // :goto_3
/* add-int/lit8 v3, v3, 0x1 */
/* goto/16 :goto_1 */
/* .line 438 */
} // .end local v0 # "n":I
} // .end local v3 # "i":I
} // :cond_3
/* monitor-exit v2 */
/* :try_end_1 */
/* .catchall {:try_start_1 ..:try_end_1} :catchall_0 */
/* .line 439 */
final String v0 = ""; // const-string v0, ""
(( java.io.PrintWriter ) p1 ).println ( v0 ); // invoke-virtual {p1, v0}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V
/* .line 440 */
return;
/* .line 438 */
/* :catchall_0 */
/* move-exception v0 */
try { // :try_start_2
/* monitor-exit v2 */
/* :try_end_2 */
/* .catchall {:try_start_2 ..:try_end_2} :catchall_0 */
/* throw v0 */
/* .line 416 */
} // .end local v1 # "formater":Ljava/text/SimpleDateFormat;
/* :catchall_1 */
/* move-exception v1 */
try { // :try_start_3
/* monitor-exit v0 */
/* :try_end_3 */
/* .catchall {:try_start_3 ..:try_end_3} :catchall_1 */
/* throw v1 */
} // .end method
private void dumpHistory ( java.io.PrintWriter p0, java.lang.String[] p1, Integer p2, Integer p3 ) {
/* .locals 10 */
/* .param p1, "pw" # Ljava/io/PrintWriter; */
/* .param p2, "args" # [Ljava/lang/String; */
/* .param p3, "opti" # I */
/* .param p4, "dpUid" # I */
/* .line 443 */
/* const-string/jumbo v0, "smart power frozen in history:" */
(( java.io.PrintWriter ) p1 ).println ( v0 ); // invoke-virtual {p1, v0}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V
/* .line 444 */
/* nop */
/* .line 445 */
android.os.SystemClock .uptimeMillis ( );
/* move-result-wide v0 */
/* sget-wide v2, Lcom/miui/app/smartpower/SmartPowerSettings;->MAX_HISTORY_REPORT_DURATION:J */
/* sub-long/2addr v0, v2 */
/* .line 444 */
/* invoke-direct {p0, v0, v1}, Lcom/miui/server/smartpower/PowerFrozenManager;->getHistoryInfos(J)Ljava/util/List; */
/* .line 446 */
/* .local v0, "infos":Ljava/util/List;, "Ljava/util/List<Lcom/miui/server/smartpower/PowerFrozenManager$FrozenInfo;>;" */
int v1 = 1; // const/4 v1, 0x1
/* .line 447 */
/* .local v1, "index":I */
/* new-instance v2, Ljava/text/SimpleDateFormat; */
final String v3 = "HH:mm:ss.SSS"; // const-string v3, "HH:mm:ss.SSS"
/* invoke-direct {v2, v3}, Ljava/text/SimpleDateFormat;-><init>(Ljava/lang/String;)V */
/* .line 448 */
/* .local v2, "formater":Ljava/text/SimpleDateFormat; */
v4 = } // :goto_0
if ( v4 != null) { // if-eqz v4, :cond_2
/* check-cast v4, Lcom/miui/server/smartpower/PowerFrozenManager$FrozenInfo; */
/* .line 449 */
/* .local v4, "info":Lcom/miui/server/smartpower/PowerFrozenManager$FrozenInfo; */
if ( p4 != null) { // if-eqz p4, :cond_0
v5 = com.miui.server.smartpower.PowerFrozenManager$FrozenInfo .-$$Nest$fgetmUid ( v4 );
/* if-eq v5, p4, :cond_0 */
/* .line 450 */
/* .line 452 */
} // :cond_0
final String v5 = " "; // const-string v5, " "
(( java.io.PrintWriter ) p1 ).print ( v5 ); // invoke-virtual {p1, v5}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V
/* .line 453 */
/* new-instance v5, Ljava/lang/StringBuilder; */
/* invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V */
final String v6 = "#"; // const-string v6, "#"
(( java.lang.StringBuilder ) v5 ).append ( v6 ); // invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
/* add-int/lit8 v6, v1, 0x1 */
} // .end local v1 # "index":I
/* .local v6, "index":I */
(( java.lang.StringBuilder ) v5 ).append ( v1 ); // invoke-virtual {v5, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v1 ).toString ( ); // invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
(( java.io.PrintWriter ) p1 ).print ( v1 ); // invoke-virtual {p1, v1}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V
/* .line 454 */
/* new-instance v1, Ljava/lang/StringBuilder; */
/* invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V */
final String v5 = " "; // const-string v5, " "
(( java.lang.StringBuilder ) v1 ).append ( v5 ); // invoke-virtual {v1, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
/* new-instance v7, Ljava/util/Date; */
com.miui.server.smartpower.PowerFrozenManager$FrozenInfo .-$$Nest$fgetmThawTime ( v4 );
/* move-result-wide v8 */
/* invoke-direct {v7, v8, v9}, Ljava/util/Date;-><init>(J)V */
(( java.text.SimpleDateFormat ) v2 ).format ( v7 ); // invoke-virtual {v2, v7}, Ljava/text/SimpleDateFormat;->format(Ljava/util/Date;)Ljava/lang/String;
(( java.lang.StringBuilder ) v1 ).append ( v7 ); // invoke-virtual {v1, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v1 ).toString ( ); // invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
(( java.io.PrintWriter ) p1 ).print ( v1 ); // invoke-virtual {p1, v1}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V
/* .line 455 */
/* new-instance v1, Ljava/lang/StringBuilder; */
/* invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V */
(( java.lang.StringBuilder ) v1 ).append ( v5 ); // invoke-virtual {v1, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v1 ).append ( v4 ); // invoke-virtual {v1, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v1 ).toString ( ); // invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
(( java.io.PrintWriter ) p1 ).print ( v1 ); // invoke-virtual {p1, v1}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V
/* .line 456 */
/* new-instance v1, Ljava/lang/StringBuilder; */
/* invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V */
(( java.lang.StringBuilder ) v1 ).append ( v5 ); // invoke-virtual {v1, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
com.miui.server.smartpower.PowerFrozenManager$FrozenInfo .-$$Nest$mgetFrozenDuration ( v4 );
/* move-result-wide v7 */
(( java.lang.StringBuilder ) v1 ).append ( v7, v8 ); // invoke-virtual {v1, v7, v8}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;
final String v7 = "ms"; // const-string v7, "ms"
(( java.lang.StringBuilder ) v1 ).append ( v7 ); // invoke-virtual {v1, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v1 ).toString ( ); // invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
(( java.io.PrintWriter ) p1 ).println ( v1 ); // invoke-virtual {p1, v1}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V
/* .line 457 */
int v1 = 0; // const/4 v1, 0x0
/* .local v1, "i":I */
} // :goto_1
v7 = com.miui.server.smartpower.PowerFrozenManager$FrozenInfo .-$$Nest$fgetmFrozenTimes ( v4 );
final String v8 = " "; // const-string v8, " "
/* if-ge v1, v7, :cond_1 */
/* .line 458 */
(( java.io.PrintWriter ) p1 ).print ( v8 ); // invoke-virtual {p1, v8}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V
/* .line 459 */
final String v7 = "fz: "; // const-string v7, "fz: "
(( java.io.PrintWriter ) p1 ).print ( v7 ); // invoke-virtual {p1, v7}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V
/* .line 460 */
/* new-instance v7, Ljava/util/Date; */
com.miui.server.smartpower.PowerFrozenManager$FrozenInfo .-$$Nest$fgetmFrozenTimes ( v4 );
/* check-cast v8, Ljava/lang/Long; */
(( java.lang.Long ) v8 ).longValue ( ); // invoke-virtual {v8}, Ljava/lang/Long;->longValue()J
/* move-result-wide v8 */
/* invoke-direct {v7, v8, v9}, Ljava/util/Date;-><init>(J)V */
(( java.text.SimpleDateFormat ) v2 ).format ( v7 ); // invoke-virtual {v2, v7}, Ljava/text/SimpleDateFormat;->format(Ljava/util/Date;)Ljava/lang/String;
(( java.io.PrintWriter ) p1 ).print ( v7 ); // invoke-virtual {p1, v7}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V
/* .line 461 */
/* new-instance v7, Ljava/lang/StringBuilder; */
/* invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V */
(( java.lang.StringBuilder ) v7 ).append ( v5 ); // invoke-virtual {v7, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
com.miui.server.smartpower.PowerFrozenManager$FrozenInfo .-$$Nest$fgetmFrozenReasons ( v4 );
/* check-cast v8, Ljava/lang/String; */
(( java.lang.StringBuilder ) v7 ).append ( v8 ); // invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v7 ).toString ( ); // invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
(( java.io.PrintWriter ) p1 ).print ( v7 ); // invoke-virtual {p1, v7}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V
/* .line 462 */
final String v7 = ""; // const-string v7, ""
(( java.io.PrintWriter ) p1 ).println ( v7 ); // invoke-virtual {p1, v7}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V
/* .line 457 */
/* add-int/lit8 v1, v1, 0x1 */
/* .line 464 */
} // .end local v1 # "i":I
} // :cond_1
(( java.io.PrintWriter ) p1 ).print ( v8 ); // invoke-virtual {p1, v8}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V
/* .line 465 */
/* const-string/jumbo v1, "th: " */
(( java.io.PrintWriter ) p1 ).print ( v1 ); // invoke-virtual {p1, v1}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V
/* .line 466 */
/* new-instance v1, Ljava/util/Date; */
com.miui.server.smartpower.PowerFrozenManager$FrozenInfo .-$$Nest$fgetmThawTime ( v4 );
/* move-result-wide v7 */
/* invoke-direct {v1, v7, v8}, Ljava/util/Date;-><init>(J)V */
(( java.text.SimpleDateFormat ) v2 ).format ( v1 ); // invoke-virtual {v2, v1}, Ljava/text/SimpleDateFormat;->format(Ljava/util/Date;)Ljava/lang/String;
(( java.io.PrintWriter ) p1 ).print ( v1 ); // invoke-virtual {p1, v1}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V
/* .line 467 */
/* new-instance v1, Ljava/lang/StringBuilder; */
/* invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V */
(( java.lang.StringBuilder ) v1 ).append ( v5 ); // invoke-virtual {v1, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
com.miui.server.smartpower.PowerFrozenManager$FrozenInfo .-$$Nest$fgetmThawReason ( v4 );
(( java.lang.StringBuilder ) v1 ).append ( v5 ); // invoke-virtual {v1, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v1 ).toString ( ); // invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
(( java.io.PrintWriter ) p1 ).println ( v1 ); // invoke-virtual {p1, v1}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V
/* .line 468 */
} // .end local v4 # "info":Lcom/miui/server/smartpower/PowerFrozenManager$FrozenInfo;
/* move v1, v6 */
/* goto/16 :goto_0 */
/* .line 469 */
} // .end local v6 # "index":I
/* .local v1, "index":I */
} // :cond_2
return;
} // .end method
private void dumpSettings ( java.io.PrintWriter p0, java.lang.String[] p1, Integer p2 ) {
/* .locals 2 */
/* .param p1, "pw" # Ljava/io/PrintWriter; */
/* .param p2, "args" # [Ljava/lang/String; */
/* .param p3, "opti" # I */
/* .line 392 */
final String v0 = "Settings:"; // const-string v0, "Settings:"
(( java.io.PrintWriter ) p1 ).println ( v0 ); // invoke-virtual {p1, v0}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V
/* .line 393 */
/* new-instance v0, Ljava/lang/StringBuilder; */
/* invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V */
final String v1 = " enable="; // const-string v1, " enable="
(( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
/* sget-boolean v1, Lcom/miui/server/smartpower/PowerFrozenManager;->sEnable:Z */
(( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;
final String v1 = " def("; // const-string v1, " def("
(( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
/* sget-boolean v1, Lcom/miui/app/smartpower/SmartPowerSettings;->PROP_FROZEN_ENABLE:Z */
(( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;
final String v1 = ")"; // const-string v1, ")"
(( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v0 ).toString ( ); // invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
(( java.io.PrintWriter ) p1 ).println ( v0 ); // invoke-virtual {p1, v0}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V
/* .line 395 */
return;
} // .end method
private void dumpUid ( java.io.PrintWriter p0, java.lang.String[] p1, Integer p2 ) {
/* .locals 2 */
/* .param p1, "pw" # Ljava/io/PrintWriter; */
/* .param p2, "args" # [Ljava/lang/String; */
/* .param p3, "opti" # I */
/* .line 398 */
int v0 = 0; // const/4 v0, 0x0
/* .line 399 */
/* .local v0, "dpUid":I */
/* array-length v1, p2 */
/* if-ge p3, v1, :cond_0 */
/* .line 400 */
/* aget-object v1, p2, p3 */
/* .line 401 */
/* .local v1, "uidStr":Ljava/lang/String; */
v0 = java.lang.Integer .parseInt ( v1 );
/* .line 402 */
/* invoke-direct {p0, p1, p2, p3, v0}, Lcom/miui/server/smartpower/PowerFrozenManager;->dumpFrozen(Ljava/io/PrintWriter;[Ljava/lang/String;II)V */
/* .line 403 */
/* invoke-direct {p0, p1, p2, p3, v0}, Lcom/miui/server/smartpower/PowerFrozenManager;->dumpHistory(Ljava/io/PrintWriter;[Ljava/lang/String;II)V */
/* .line 405 */
} // .end local v1 # "uidStr":Ljava/lang/String;
} // :cond_0
return;
} // .end method
private java.util.List getHistoryInfos ( Long p0 ) {
/* .locals 7 */
/* .param p1, "sinceUptime" # J */
/* .annotation system Ldalvik/annotation/Signature; */
/* value = { */
/* "(J)", */
/* "Ljava/util/List<", */
/* "Lcom/miui/server/smartpower/PowerFrozenManager$FrozenInfo;", */
/* ">;" */
/* } */
} // .end annotation
/* .line 355 */
/* new-instance v0, Ljava/util/ArrayList; */
/* invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V */
/* .line 356 */
/* .local v0, "ret":Ljava/util/List;, "Ljava/util/List<Lcom/miui/server/smartpower/PowerFrozenManager$FrozenInfo;>;" */
/* iget v1, p0, Lcom/miui/server/smartpower/PowerFrozenManager;->mHistoryIndexNext:I */
int v2 = -1; // const/4 v2, -0x1
/* const/16 v3, 0x1000 */
v1 = com.miui.server.smartpower.PowerFrozenManager .ringAdvance ( v1,v2,v3 );
/* .line 358 */
/* .local v1, "index":I */
int v4 = 0; // const/4 v4, 0x0
/* .local v4, "i":I */
} // :goto_0
/* if-ge v4, v3, :cond_1 */
/* .line 359 */
v5 = this.mFrozenHistory;
/* aget-object v5, v5, v1 */
if ( v5 != null) { // if-eqz v5, :cond_1
com.miui.server.smartpower.PowerFrozenManager$FrozenInfo .-$$Nest$fgetmThawTime ( v5 );
/* move-result-wide v5 */
/* cmp-long v5, v5, p1 */
/* if-gez v5, :cond_0 */
/* .line 361 */
/* .line 363 */
} // :cond_0
v5 = this.mFrozenHistory;
/* aget-object v5, v5, v1 */
/* .line 364 */
v1 = com.miui.server.smartpower.PowerFrozenManager .ringAdvance ( v1,v2,v3 );
/* .line 358 */
/* add-int/lit8 v4, v4, 0x1 */
/* .line 366 */
} // .end local v4 # "i":I
} // :cond_1
} // :goto_1
} // .end method
public static Boolean isEnable ( ) {
/* .locals 1 */
/* .line 341 */
/* sget-boolean v0, Lcom/miui/server/smartpower/PowerFrozenManager;->sEnable:Z */
} // .end method
private void removeFrozenPid ( Integer p0, Integer p1, Boolean p2 ) {
/* .locals 4 */
/* .param p1, "uid" # I */
/* .param p2, "pid" # I */
/* .param p3, "isSmartPower" # Z */
/* .line 266 */
v0 = this.mAllFrozenPids;
/* monitor-enter v0 */
/* .line 267 */
try { // :try_start_0
/* sget-boolean v1, Lcom/miui/server/smartpower/PowerFrozenManager;->DEBUG:Z */
if ( v1 != null) { // if-eqz v1, :cond_0
final String v1 = "PowerFrozen"; // const-string v1, "PowerFrozen"
/* new-instance v2, Ljava/lang/StringBuilder; */
/* invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V */
final String v3 = "remove p:"; // const-string v3, "remove p:"
(( java.lang.StringBuilder ) v2 ).append ( v3 ); // invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v2 ).append ( p2 ); // invoke-virtual {v2, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;
final String v3 = "/u:"; // const-string v3, "/u:"
(( java.lang.StringBuilder ) v2 ).append ( v3 ); // invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v2 ).append ( p1 ); // invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;
final String v3 = " isMy:"; // const-string v3, " isMy:"
(( java.lang.StringBuilder ) v2 ).append ( v3 ); // invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v2 ).append ( p3 ); // invoke-virtual {v2, p3}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v2 ).toString ( ); // invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
android.util.Slog .d ( v1,v2 );
/* .line 268 */
} // :cond_0
v1 = this.mAllFrozenPids;
(( android.util.SparseArray ) v1 ).remove ( p2 ); // invoke-virtual {v1, p2}, Landroid/util/SparseArray;->remove(I)V
/* .line 269 */
v1 = this.mHandler;
java.lang.Integer .valueOf ( p2 );
int v3 = 1; // const/4 v3, 0x1
v1 = (( com.miui.server.smartpower.PowerFrozenManager$H ) v1 ).hasMessages ( v3, v2 ); // invoke-virtual {v1, v3, v2}, Lcom/miui/server/smartpower/PowerFrozenManager$H;->hasMessages(ILjava/lang/Object;)Z
if ( v1 != null) { // if-eqz v1, :cond_1
/* .line 270 */
v1 = this.mHandler;
java.lang.Integer .valueOf ( p2 );
(( com.miui.server.smartpower.PowerFrozenManager$H ) v1 ).removeMessages ( v3, v2 ); // invoke-virtual {v1, v3, v2}, Lcom/miui/server/smartpower/PowerFrozenManager$H;->removeMessages(ILjava/lang/Object;)V
/* .line 272 */
} // :cond_1
/* monitor-exit v0 */
/* .line 273 */
return;
/* .line 272 */
/* :catchall_0 */
/* move-exception v1 */
/* monitor-exit v0 */
/* :try_end_0 */
/* .catchall {:try_start_0 ..:try_end_0} :catchall_0 */
/* throw v1 */
} // .end method
private static Integer ringAdvance ( Integer p0, Integer p1, Integer p2 ) {
/* .locals 2 */
/* .param p0, "origin" # I */
/* .param p1, "increment" # I */
/* .param p2, "size" # I */
/* .line 345 */
/* add-int v0, p0, p1 */
/* rem-int/2addr v0, p2 */
/* .line 346 */
/* .local v0, "index":I */
/* if-gez v0, :cond_0 */
/* add-int v1, v0, p2 */
} // :cond_0
/* move v1, v0 */
} // :goto_0
} // .end method
/* # virtual methods */
public void addFrozenPid ( Integer p0, Integer p1 ) {
/* .locals 1 */
/* .param p1, "uid" # I */
/* .param p2, "pid" # I */
/* .line 248 */
int v0 = 0; // const/4 v0, 0x0
/* invoke-direct {p0, p1, p2, v0}, Lcom/miui/server/smartpower/PowerFrozenManager;->addFrozenPid(IIZ)V */
/* .line 249 */
return;
} // .end method
public void dump ( java.io.PrintWriter p0, java.lang.String[] p1, Integer p2 ) {
/* .locals 3 */
/* .param p1, "pw" # Ljava/io/PrintWriter; */
/* .param p2, "args" # [Ljava/lang/String; */
/* .param p3, "opti" # I */
/* .line 371 */
try { // :try_start_0
/* array-length v0, p2 */
int v1 = 0; // const/4 v1, 0x0
/* if-ge p3, v0, :cond_6 */
/* .line 372 */
/* aget-object v0, p2, p3 */
/* .line 373 */
/* .local v0, "parm":Ljava/lang/String; */
/* add-int/lit8 p3, p3, 0x1 */
/* .line 374 */
final String v2 = "history"; // const-string v2, "history"
v2 = (( java.lang.String ) v0 ).contains ( v2 ); // invoke-virtual {v0, v2}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z
/* if-nez v2, :cond_4 */
final String v2 = "-h"; // const-string v2, "-h"
v2 = (( java.lang.String ) v0 ).contains ( v2 ); // invoke-virtual {v0, v2}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z
if ( v2 != null) { // if-eqz v2, :cond_0
/* .line 376 */
} // :cond_0
final String v2 = "current"; // const-string v2, "current"
v2 = (( java.lang.String ) v0 ).contains ( v2 ); // invoke-virtual {v0, v2}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z
/* if-nez v2, :cond_3 */
final String v2 = "-c"; // const-string v2, "-c"
v2 = (( java.lang.String ) v0 ).contains ( v2 ); // invoke-virtual {v0, v2}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z
if ( v2 != null) { // if-eqz v2, :cond_1
/* .line 378 */
} // :cond_1
/* const-string/jumbo v1, "uid" */
v1 = (( java.lang.String ) v0 ).contains ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z
/* if-nez v1, :cond_2 */
final String v1 = "-u"; // const-string v1, "-u"
v1 = (( java.lang.String ) v0 ).contains ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z
if ( v1 != null) { // if-eqz v1, :cond_5
/* .line 379 */
} // :cond_2
/* invoke-direct {p0, p1, p2, p3}, Lcom/miui/server/smartpower/PowerFrozenManager;->dumpUid(Ljava/io/PrintWriter;[Ljava/lang/String;I)V */
/* .line 377 */
} // :cond_3
} // :goto_0
/* invoke-direct {p0, p1, p2, p3, v1}, Lcom/miui/server/smartpower/PowerFrozenManager;->dumpFrozen(Ljava/io/PrintWriter;[Ljava/lang/String;II)V */
/* .line 375 */
} // :cond_4
} // :goto_1
/* invoke-direct {p0, p1, p2, p3, v1}, Lcom/miui/server/smartpower/PowerFrozenManager;->dumpHistory(Ljava/io/PrintWriter;[Ljava/lang/String;II)V */
/* .line 381 */
} // .end local v0 # "parm":Ljava/lang/String;
} // :cond_5
} // :goto_2
/* .line 382 */
} // :cond_6
/* invoke-direct {p0, p1, p2, p3}, Lcom/miui/server/smartpower/PowerFrozenManager;->dumpSettings(Ljava/io/PrintWriter;[Ljava/lang/String;I)V */
/* .line 383 */
/* invoke-direct {p0, p1, p2, p3, v1}, Lcom/miui/server/smartpower/PowerFrozenManager;->dumpFrozen(Ljava/io/PrintWriter;[Ljava/lang/String;II)V */
/* .line 384 */
/* invoke-direct {p0, p1, p2, p3, v1}, Lcom/miui/server/smartpower/PowerFrozenManager;->dumpHistory(Ljava/io/PrintWriter;[Ljava/lang/String;II)V */
/* :try_end_0 */
/* .catch Ljava/lang/Exception; {:try_start_0 ..:try_end_0} :catch_0 */
/* .line 388 */
} // :goto_3
/* .line 386 */
/* :catch_0 */
/* move-exception v0 */
/* .line 387 */
/* .local v0, "e":Ljava/lang/Exception; */
(( java.lang.Exception ) v0 ).printStackTrace ( ); // invoke-virtual {v0}, Ljava/lang/Exception;->printStackTrace()V
/* .line 389 */
} // .end local v0 # "e":Ljava/lang/Exception;
} // :goto_4
return;
} // .end method
public Boolean frozenProcess ( Integer p0, Integer p1, java.lang.String p2, java.lang.String p3 ) {
/* .locals 7 */
/* .param p1, "uid" # I */
/* .param p2, "pid" # I */
/* .param p3, "processName" # Ljava/lang/String; */
/* .param p4, "reason" # Ljava/lang/String; */
/* .line 81 */
/* const-wide/16 v5, 0x0 */
/* move-object v0, p0 */
/* move v1, p1 */
/* move v2, p2 */
/* move-object v3, p3 */
/* move-object v4, p4 */
v0 = /* invoke-virtual/range {v0 ..v6}, Lcom/miui/server/smartpower/PowerFrozenManager;->frozenProcess(IILjava/lang/String;Ljava/lang/String;J)Z */
} // .end method
public Boolean frozenProcess ( Integer p0, Integer p1, java.lang.String p2, java.lang.String p3, Long p4 ) {
/* .locals 7 */
/* .param p1, "uid" # I */
/* .param p2, "pid" # I */
/* .param p3, "processName" # Ljava/lang/String; */
/* .param p4, "reason" # Ljava/lang/String; */
/* .param p5, "timeOut" # J */
/* .line 90 */
v0 = com.miui.server.smartpower.PowerFrozenManager .isEnable ( );
int v1 = 0; // const/4 v1, 0x0
/* if-nez v0, :cond_0 */
/* .line 91 */
/* .line 93 */
} // :cond_0
/* if-lez p2, :cond_6 */
v0 = android.os.Process .myPid ( );
/* if-eq v0, p2, :cond_6 */
v0 = android.os.Process .getUidForPid ( p2 );
/* if-eq v0, p1, :cond_1 */
/* goto/16 :goto_1 */
/* .line 98 */
} // :cond_1
/* sget-boolean v0, Lcom/miui/app/smartpower/SmartPowerSettings;->PROP_FROZEN_CGROUPV1_ENABLE:Z */
if ( v0 != null) { // if-eqz v0, :cond_2
/* .line 99 */
v0 = this.gzInternal;
v0 = (( com.miui.server.greeze.GreezeManagerInternal ) v0 ).freezePid ( p2 ); // invoke-virtual {v0, p2}, Lcom/miui/server/greeze/GreezeManagerInternal;->freezePid(I)Z
/* .local v0, "done":Z */
/* .line 101 */
} // .end local v0 # "done":Z
} // :cond_2
v0 = this.gzInternal;
v0 = (( com.miui.server.greeze.GreezeManagerInternal ) v0 ).freezePid ( p2, p1 ); // invoke-virtual {v0, p2, p1}, Lcom/miui/server/greeze/GreezeManagerInternal;->freezePid(II)Z
/* .line 102 */
/* .restart local v0 # "done":Z */
} // :goto_0
int v1 = 1; // const/4 v1, 0x1
/* invoke-direct {p0, p1, p2, v1}, Lcom/miui/server/smartpower/PowerFrozenManager;->addFrozenPid(IIZ)V */
/* .line 103 */
v2 = this.mFrozenList;
/* monitor-enter v2 */
/* .line 104 */
try { // :try_start_0
v3 = this.mFrozenList;
(( android.util.SparseArray ) v3 ).get ( p2 ); // invoke-virtual {v3, p2}, Landroid/util/SparseArray;->get(I)Ljava/lang/Object;
/* check-cast v3, Lcom/miui/server/smartpower/PowerFrozenManager$FrozenInfo; */
/* .line 105 */
/* .local v3, "info":Lcom/miui/server/smartpower/PowerFrozenManager$FrozenInfo; */
/* if-nez v3, :cond_3 */
/* .line 106 */
/* new-instance v4, Lcom/miui/server/smartpower/PowerFrozenManager$FrozenInfo; */
int v5 = 0; // const/4 v5, 0x0
/* invoke-direct {v4, p1, p2, p3, v5}, Lcom/miui/server/smartpower/PowerFrozenManager$FrozenInfo;-><init>(IILjava/lang/String;Lcom/miui/server/smartpower/PowerFrozenManager$FrozenInfo-IA;)V */
/* move-object v3, v4 */
/* .line 107 */
v4 = this.mFrozenList;
(( android.util.SparseArray ) v4 ).put ( p2, v3 ); // invoke-virtual {v4, p2, v3}, Landroid/util/SparseArray;->put(ILjava/lang/Object;)V
/* .line 109 */
} // :cond_3
java.lang.System .currentTimeMillis ( );
/* move-result-wide v4 */
com.miui.server.smartpower.PowerFrozenManager$FrozenInfo .-$$Nest$maddFrozenInfo ( v3,v4,v5,p4 );
/* .line 110 */
/* sget-boolean v4, Lcom/miui/server/smartpower/PowerFrozenManager;->DEBUG:Z */
if ( v4 != null) { // if-eqz v4, :cond_4
/* .line 111 */
final String v4 = "PowerFrozen"; // const-string v4, "PowerFrozen"
/* new-instance v5, Ljava/lang/StringBuilder; */
/* invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V */
final String v6 = "Frozen "; // const-string v6, "Frozen "
(( java.lang.StringBuilder ) v5 ).append ( v6 ); // invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v5 ).append ( v3 ); // invoke-virtual {v5, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;
final String v6 = " reason:"; // const-string v6, " reason:"
(( java.lang.StringBuilder ) v5 ).append ( v6 ); // invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v5 ).append ( p4 ); // invoke-virtual {v5, p4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v5 ).toString ( ); // invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
android.util.Slog .d ( v4,v5 );
/* .line 113 */
} // :cond_4
/* new-instance v4, Ljava/lang/StringBuilder; */
/* invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V */
final String v5 = "frozen "; // const-string v5, "frozen "
(( java.lang.StringBuilder ) v4 ).append ( v5 ); // invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v4 ).append ( v3 ); // invoke-virtual {v4, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v4 ).toString ( ); // invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
/* const v5, 0x15ff2 */
android.util.EventLog .writeEvent ( v5,v4 );
/* .line 114 */
/* const-wide/16 v4, 0x0 */
/* cmp-long v4, p5, v4 */
if ( v4 != null) { // if-eqz v4, :cond_5
/* .line 115 */
v4 = this.mHandler;
v5 = com.miui.server.smartpower.PowerFrozenManager$FrozenInfo .-$$Nest$fgetmPid ( v3 );
java.lang.Integer .valueOf ( v5 );
(( com.miui.server.smartpower.PowerFrozenManager$H ) v4 ).obtainMessage ( v1, v5 ); // invoke-virtual {v4, v1, v5}, Lcom/miui/server/smartpower/PowerFrozenManager$H;->obtainMessage(ILjava/lang/Object;)Landroid/os/Message;
/* .line 116 */
/* .local v1, "msg":Landroid/os/Message; */
v4 = com.miui.server.smartpower.PowerFrozenManager$FrozenInfo .-$$Nest$fgetmUid ( v3 );
/* iput v4, v1, Landroid/os/Message;->arg1:I */
/* .line 117 */
v4 = com.miui.server.smartpower.PowerFrozenManager$FrozenInfo .-$$Nest$fgetmPid ( v3 );
/* iput v4, v1, Landroid/os/Message;->arg2:I */
/* .line 118 */
v4 = this.mHandler;
(( com.miui.server.smartpower.PowerFrozenManager$H ) v4 ).sendMessageDelayed ( v1, p5, p6 ); // invoke-virtual {v4, v1, p5, p6}, Lcom/miui/server/smartpower/PowerFrozenManager$H;->sendMessageDelayed(Landroid/os/Message;J)Z
/* .line 120 */
} // .end local v1 # "msg":Landroid/os/Message;
} // .end local v3 # "info":Lcom/miui/server/smartpower/PowerFrozenManager$FrozenInfo;
} // :cond_5
/* monitor-exit v2 */
/* .line 121 */
/* .line 120 */
/* :catchall_0 */
/* move-exception v1 */
/* monitor-exit v2 */
/* :try_end_0 */
/* .catchall {:try_start_0 ..:try_end_0} :catchall_0 */
/* throw v1 */
/* .line 94 */
} // .end local v0 # "done":Z
} // :cond_6
} // :goto_1
} // .end method
public java.util.ArrayList getFrozenPids ( Integer p0 ) {
/* .locals 4 */
/* .param p1, "uid" # I */
/* .annotation system Ldalvik/annotation/Signature; */
/* value = { */
/* "(I)", */
/* "Ljava/util/ArrayList<", */
/* "Ljava/lang/Integer;", */
/* ">;" */
/* } */
} // .end annotation
/* .line 175 */
/* new-instance v0, Ljava/util/ArrayList; */
/* invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V */
/* .line 176 */
/* .local v0, "frozenPids":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/Integer;>;" */
v1 = this.mAllFrozenPids;
/* monitor-enter v1 */
/* .line 177 */
try { // :try_start_0
v2 = this.mAllFrozenPids;
v2 = (( android.util.SparseArray ) v2 ).size ( ); // invoke-virtual {v2}, Landroid/util/SparseArray;->size()I
/* add-int/lit8 v2, v2, -0x1 */
/* .local v2, "i":I */
} // :goto_0
/* if-ltz v2, :cond_1 */
/* .line 178 */
v3 = this.mAllFrozenPids;
(( android.util.SparseArray ) v3 ).valueAt ( v2 ); // invoke-virtual {v3, v2}, Landroid/util/SparseArray;->valueAt(I)Ljava/lang/Object;
/* check-cast v3, Ljava/lang/Integer; */
v3 = (( java.lang.Integer ) v3 ).intValue ( ); // invoke-virtual {v3}, Ljava/lang/Integer;->intValue()I
/* if-ne v3, p1, :cond_0 */
/* .line 179 */
v3 = this.mAllFrozenPids;
v3 = (( android.util.SparseArray ) v3 ).keyAt ( v2 ); // invoke-virtual {v3, v2}, Landroid/util/SparseArray;->keyAt(I)I
java.lang.Integer .valueOf ( v3 );
(( java.util.ArrayList ) v0 ).add ( v3 ); // invoke-virtual {v0, v3}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z
/* .line 177 */
} // :cond_0
/* add-int/lit8 v2, v2, -0x1 */
/* .line 182 */
} // .end local v2 # "i":I
} // :cond_1
/* monitor-exit v1 */
/* .line 183 */
/* .line 182 */
/* :catchall_0 */
/* move-exception v2 */
/* monitor-exit v1 */
/* :try_end_0 */
/* .catchall {:try_start_0 ..:try_end_0} :catchall_0 */
/* throw v2 */
} // .end method
public Boolean isAllFrozenForUid ( Integer p0 ) {
/* .locals 3 */
/* .param p1, "uid" # I */
/* .line 204 */
v0 = this.mAllFrozenPids;
/* monitor-enter v0 */
/* .line 205 */
int v1 = 0; // const/4 v1, 0x0
/* .local v1, "i":I */
} // :goto_0
try { // :try_start_0
v2 = this.mAllFrozenPids;
v2 = (( android.util.SparseArray ) v2 ).size ( ); // invoke-virtual {v2}, Landroid/util/SparseArray;->size()I
/* if-ge v1, v2, :cond_1 */
/* .line 206 */
v2 = this.mAllFrozenPids;
(( android.util.SparseArray ) v2 ).valueAt ( v1 ); // invoke-virtual {v2, v1}, Landroid/util/SparseArray;->valueAt(I)Ljava/lang/Object;
/* check-cast v2, Ljava/lang/Integer; */
v2 = (( java.lang.Integer ) v2 ).intValue ( ); // invoke-virtual {v2}, Ljava/lang/Integer;->intValue()I
/* if-ne v2, p1, :cond_0 */
/* .line 207 */
/* monitor-exit v0 */
int v0 = 1; // const/4 v0, 0x1
/* .line 205 */
} // :cond_0
/* add-int/lit8 v1, v1, 0x1 */
/* .line 210 */
} // .end local v1 # "i":I
} // :cond_1
/* monitor-exit v0 */
/* .line 211 */
int v0 = 0; // const/4 v0, 0x0
/* .line 210 */
/* :catchall_0 */
/* move-exception v1 */
/* monitor-exit v0 */
/* :try_end_0 */
/* .catchall {:try_start_0 ..:try_end_0} :catchall_0 */
/* throw v1 */
} // .end method
public Boolean isAllFrozenPid ( Integer p0 ) {
/* .locals 2 */
/* .param p1, "pid" # I */
/* .line 218 */
v0 = this.mAllFrozenPids;
/* monitor-enter v0 */
/* .line 219 */
try { // :try_start_0
v1 = this.mAllFrozenPids;
(( android.util.SparseArray ) v1 ).get ( p1 ); // invoke-virtual {v1, p1}, Landroid/util/SparseArray;->get(I)Ljava/lang/Object;
if ( v1 != null) { // if-eqz v1, :cond_0
int v1 = 1; // const/4 v1, 0x1
} // :cond_0
int v1 = 0; // const/4 v1, 0x0
} // :goto_0
/* monitor-exit v0 */
/* .line 220 */
/* :catchall_0 */
/* move-exception v1 */
/* monitor-exit v0 */
/* :try_end_0 */
/* .catchall {:try_start_0 ..:try_end_0} :catchall_0 */
/* throw v1 */
} // .end method
public Boolean isFrozenForUid ( Integer p0 ) {
/* .locals 4 */
/* .param p1, "uid" # I */
/* .line 227 */
v0 = this.mFrozenList;
/* monitor-enter v0 */
/* .line 228 */
int v1 = 0; // const/4 v1, 0x0
/* .local v1, "i":I */
} // :goto_0
try { // :try_start_0
v2 = this.mFrozenList;
v2 = (( android.util.SparseArray ) v2 ).size ( ); // invoke-virtual {v2}, Landroid/util/SparseArray;->size()I
/* if-ge v1, v2, :cond_1 */
/* .line 229 */
v2 = this.mFrozenList;
(( android.util.SparseArray ) v2 ).valueAt ( v1 ); // invoke-virtual {v2, v1}, Landroid/util/SparseArray;->valueAt(I)Ljava/lang/Object;
/* check-cast v2, Lcom/miui/server/smartpower/PowerFrozenManager$FrozenInfo; */
/* .line 230 */
/* .local v2, "info":Lcom/miui/server/smartpower/PowerFrozenManager$FrozenInfo; */
if ( v2 != null) { // if-eqz v2, :cond_0
v3 = com.miui.server.smartpower.PowerFrozenManager$FrozenInfo .-$$Nest$fgetmUid ( v2 );
/* if-ne v3, p1, :cond_0 */
/* .line 231 */
/* monitor-exit v0 */
int v0 = 1; // const/4 v0, 0x1
/* .line 228 */
} // .end local v2 # "info":Lcom/miui/server/smartpower/PowerFrozenManager$FrozenInfo;
} // :cond_0
/* add-int/lit8 v1, v1, 0x1 */
/* .line 234 */
} // .end local v1 # "i":I
} // :cond_1
/* monitor-exit v0 */
/* .line 235 */
int v0 = 0; // const/4 v0, 0x0
/* .line 234 */
/* :catchall_0 */
/* move-exception v1 */
/* monitor-exit v0 */
/* :try_end_0 */
/* .catchall {:try_start_0 ..:try_end_0} :catchall_0 */
/* throw v1 */
} // .end method
public Boolean isFrozenPid ( Integer p0 ) {
/* .locals 2 */
/* .param p1, "pid" # I */
/* .line 242 */
v0 = this.mFrozenList;
/* monitor-enter v0 */
/* .line 243 */
try { // :try_start_0
v1 = this.mFrozenList;
(( android.util.SparseArray ) v1 ).get ( p1 ); // invoke-virtual {v1, p1}, Landroid/util/SparseArray;->get(I)Ljava/lang/Object;
if ( v1 != null) { // if-eqz v1, :cond_0
int v1 = 1; // const/4 v1, 0x1
} // :cond_0
int v1 = 0; // const/4 v1, 0x0
} // :goto_0
/* monitor-exit v0 */
/* .line 244 */
/* :catchall_0 */
/* move-exception v1 */
/* monitor-exit v0 */
/* :try_end_0 */
/* .catchall {:try_start_0 ..:try_end_0} :catchall_0 */
/* throw v1 */
} // .end method
public void registerFrozenCallback ( com.miui.server.smartpower.PowerFrozenManager$IFrozenReportCallback p0 ) {
/* .locals 1 */
/* .param p1, "callback" # Lcom/miui/server/smartpower/PowerFrozenManager$IFrozenReportCallback; */
/* .line 276 */
v0 = this.mFrozenCallbacks;
/* .line 277 */
return;
} // .end method
public void removeFrozenPid ( Integer p0, Integer p1 ) {
/* .locals 1 */
/* .param p1, "uid" # I */
/* .param p2, "pid" # I */
/* .line 262 */
int v0 = 0; // const/4 v0, 0x0
/* invoke-direct {p0, p1, p2, v0}, Lcom/miui/server/smartpower/PowerFrozenManager;->removeFrozenPid(IIZ)V */
/* .line 263 */
return;
} // .end method
public void reportBinderState ( Integer p0, Integer p1, Integer p2, Integer p3, Long p4 ) {
/* .locals 9 */
/* .param p1, "uid" # I */
/* .param p2, "pid" # I */
/* .param p3, "tid" # I */
/* .param p4, "binderState" # I */
/* .param p5, "now" # J */
/* .line 323 */
v0 = com.miui.server.smartpower.PowerFrozenManager .isEnable ( );
/* if-nez v0, :cond_0 */
/* .line 324 */
return;
/* .line 326 */
} // :cond_0
v0 = this.mFrozenCallbacks;
v1 = } // :goto_0
if ( v1 != null) { // if-eqz v1, :cond_1
/* check-cast v1, Lcom/miui/server/smartpower/PowerFrozenManager$IFrozenReportCallback; */
/* .line 327 */
/* .local v1, "callback":Lcom/miui/server/smartpower/PowerFrozenManager$IFrozenReportCallback; */
/* move-object v2, v1 */
/* move v3, p1 */
/* move v4, p2 */
/* move v5, p3 */
/* move v6, p4 */
/* move-wide v7, p5 */
/* invoke-interface/range {v2 ..v8}, Lcom/miui/server/smartpower/PowerFrozenManager$IFrozenReportCallback;->reportBinderState(IIIIJ)V */
/* .line 328 */
} // .end local v1 # "callback":Lcom/miui/server/smartpower/PowerFrozenManager$IFrozenReportCallback;
/* .line 329 */
} // :cond_1
return;
} // .end method
public void reportBinderTrans ( Integer p0, Integer p1, Integer p2, Integer p3, Integer p4, Boolean p5, Long p6, Long p7 ) {
/* .locals 14 */
/* .param p1, "dstUid" # I */
/* .param p2, "dstPid" # I */
/* .param p3, "callerUid" # I */
/* .param p4, "callerPid" # I */
/* .param p5, "callerTid" # I */
/* .param p6, "isOneway" # Z */
/* .param p7, "now" # J */
/* .param p9, "buffer" # J */
/* .line 304 */
v0 = com.miui.server.smartpower.PowerFrozenManager .isEnable ( );
/* if-nez v0, :cond_0 */
/* .line 305 */
return;
/* .line 307 */
} // :cond_0
/* move-object v0, p0 */
v1 = this.mFrozenCallbacks;
v2 = } // :goto_0
if ( v2 != null) { // if-eqz v2, :cond_1
/* check-cast v2, Lcom/miui/server/smartpower/PowerFrozenManager$IFrozenReportCallback; */
/* .line 308 */
/* .local v2, "callback":Lcom/miui/server/smartpower/PowerFrozenManager$IFrozenReportCallback; */
/* move-object v3, v2 */
/* move v4, p1 */
/* move/from16 v5, p2 */
/* move/from16 v6, p3 */
/* move/from16 v7, p4 */
/* move/from16 v8, p5 */
/* move/from16 v9, p6 */
/* move-wide/from16 v10, p7 */
/* move-wide/from16 v12, p9 */
/* invoke-interface/range {v3 ..v13}, Lcom/miui/server/smartpower/PowerFrozenManager$IFrozenReportCallback;->reportBinderTrans(IIIIIZJJ)V */
/* .line 310 */
} // .end local v2 # "callback":Lcom/miui/server/smartpower/PowerFrozenManager$IFrozenReportCallback;
/* .line 311 */
} // :cond_1
return;
} // .end method
public void reportNet ( Integer p0, Long p1 ) {
/* .locals 2 */
/* .param p1, "uid" # I */
/* .param p2, "now" # J */
/* .line 293 */
v0 = com.miui.server.smartpower.PowerFrozenManager .isEnable ( );
/* if-nez v0, :cond_0 */
/* .line 294 */
return;
/* .line 296 */
} // :cond_0
v0 = this.mFrozenCallbacks;
v1 = } // :goto_0
if ( v1 != null) { // if-eqz v1, :cond_1
/* check-cast v1, Lcom/miui/server/smartpower/PowerFrozenManager$IFrozenReportCallback; */
/* .line 297 */
/* .local v1, "callback":Lcom/miui/server/smartpower/PowerFrozenManager$IFrozenReportCallback; */
/* .line 298 */
} // .end local v1 # "callback":Lcom/miui/server/smartpower/PowerFrozenManager$IFrozenReportCallback;
/* .line 299 */
} // :cond_1
return;
} // .end method
public void reportSignal ( Integer p0, Integer p1, Long p2 ) {
/* .locals 2 */
/* .param p1, "uid" # I */
/* .param p2, "pid" # I */
/* .param p3, "now" # J */
/* .line 284 */
v0 = com.miui.server.smartpower.PowerFrozenManager .isEnable ( );
/* if-nez v0, :cond_0 */
/* .line 285 */
return;
/* .line 287 */
} // :cond_0
v0 = this.mFrozenCallbacks;
v1 = } // :goto_0
if ( v1 != null) { // if-eqz v1, :cond_1
/* check-cast v1, Lcom/miui/server/smartpower/PowerFrozenManager$IFrozenReportCallback; */
/* .line 288 */
/* .local v1, "callback":Lcom/miui/server/smartpower/PowerFrozenManager$IFrozenReportCallback; */
/* .line 289 */
} // .end local v1 # "callback":Lcom/miui/server/smartpower/PowerFrozenManager$IFrozenReportCallback;
/* .line 290 */
} // :cond_1
return;
} // .end method
public void serviceReady ( Boolean p0 ) {
/* .locals 3 */
/* .param p1, "ready" # Z */
/* .line 314 */
if ( p1 != null) { // if-eqz p1, :cond_0
/* sget-boolean v0, Lcom/miui/app/smartpower/SmartPowerSettings;->PROP_FROZEN_ENABLE:Z */
if ( v0 != null) { // if-eqz v0, :cond_0
int v0 = 1; // const/4 v0, 0x1
} // :cond_0
int v0 = 0; // const/4 v0, 0x0
} // :goto_0
com.miui.server.smartpower.PowerFrozenManager.sEnable = (v0!= 0);
/* .line 315 */
/* new-instance v0, Ljava/lang/StringBuilder; */
/* invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V */
/* const-string/jumbo v1, "serviceReady millet:" */
(( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v0 ).append ( p1 ); // invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;
final String v1 = " frozen:"; // const-string v1, " frozen:"
(( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
/* sget-boolean v1, Lcom/miui/server/smartpower/PowerFrozenManager;->sEnable:Z */
(( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v0 ).toString ( ); // invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
final String v1 = "PowerFrozen"; // const-string v1, "PowerFrozen"
android.util.Slog .d ( v1,v0 );
/* .line 316 */
v0 = this.mFrozenCallbacks;
v1 = } // :goto_1
if ( v1 != null) { // if-eqz v1, :cond_1
/* check-cast v1, Lcom/miui/server/smartpower/PowerFrozenManager$IFrozenReportCallback; */
/* .line 317 */
/* .local v1, "callback":Lcom/miui/server/smartpower/PowerFrozenManager$IFrozenReportCallback; */
/* sget-boolean v2, Lcom/miui/server/smartpower/PowerFrozenManager;->sEnable:Z */
/* .line 318 */
} // .end local v1 # "callback":Lcom/miui/server/smartpower/PowerFrozenManager$IFrozenReportCallback;
/* .line 319 */
} // :cond_1
return;
} // .end method
public void syncCloudControlSettings ( Boolean p0 ) {
/* .locals 1 */
/* .param p1, "frozenEnable" # Z */
/* .line 71 */
/* if-nez p1, :cond_0 */
/* sget-boolean v0, Lcom/miui/server/smartpower/PowerFrozenManager;->sEnable:Z */
if ( v0 != null) { // if-eqz v0, :cond_0
/* .line 72 */
final String v0 = "disable frozen from cloud control"; // const-string v0, "disable frozen from cloud control"
(( com.miui.server.smartpower.PowerFrozenManager ) p0 ).thawAll ( v0 ); // invoke-virtual {p0, v0}, Lcom/miui/server/smartpower/PowerFrozenManager;->thawAll(Ljava/lang/String;)V
/* .line 74 */
} // :cond_0
return;
} // .end method
public void thawAll ( java.lang.String p0 ) {
/* .locals 6 */
/* .param p1, "reason" # Ljava/lang/String; */
/* .line 187 */
v0 = this.mAllFrozenPids;
/* monitor-enter v0 */
/* .line 188 */
try { // :try_start_0
/* new-instance v1, Ljava/util/ArrayList; */
/* invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V */
/* .line 189 */
/* .local v1, "frozenUids":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/Integer;>;" */
/* new-instance v2, Ljava/util/ArrayList; */
/* invoke-direct {v2}, Ljava/util/ArrayList;-><init>()V */
/* .line 190 */
/* .local v2, "frozenPids":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/Integer;>;" */
v3 = this.mAllFrozenPids;
v3 = (( android.util.SparseArray ) v3 ).size ( ); // invoke-virtual {v3}, Landroid/util/SparseArray;->size()I
/* add-int/lit8 v3, v3, -0x1 */
/* .local v3, "i":I */
} // :goto_0
/* if-ltz v3, :cond_0 */
/* .line 191 */
v4 = this.mAllFrozenPids;
(( android.util.SparseArray ) v4 ).valueAt ( v3 ); // invoke-virtual {v4, v3}, Landroid/util/SparseArray;->valueAt(I)Ljava/lang/Object;
/* check-cast v4, Ljava/lang/Integer; */
(( java.util.ArrayList ) v1 ).add ( v4 ); // invoke-virtual {v1, v4}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z
/* .line 192 */
v4 = this.mAllFrozenPids;
v4 = (( android.util.SparseArray ) v4 ).keyAt ( v3 ); // invoke-virtual {v4, v3}, Landroid/util/SparseArray;->keyAt(I)I
java.lang.Integer .valueOf ( v4 );
(( java.util.ArrayList ) v2 ).add ( v4 ); // invoke-virtual {v2, v4}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z
/* .line 190 */
/* add-int/lit8 v3, v3, -0x1 */
/* .line 194 */
} // .end local v3 # "i":I
} // :cond_0
v3 = (( java.util.ArrayList ) v2 ).size ( ); // invoke-virtual {v2}, Ljava/util/ArrayList;->size()I
/* add-int/lit8 v3, v3, -0x1 */
/* .restart local v3 # "i":I */
} // :goto_1
/* if-ltz v3, :cond_1 */
/* .line 195 */
(( java.util.ArrayList ) v1 ).get ( v3 ); // invoke-virtual {v1, v3}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;
/* check-cast v4, Ljava/lang/Integer; */
v4 = (( java.lang.Integer ) v4 ).intValue ( ); // invoke-virtual {v4}, Ljava/lang/Integer;->intValue()I
(( java.util.ArrayList ) v2 ).get ( v3 ); // invoke-virtual {v2, v3}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;
/* check-cast v5, Ljava/lang/Integer; */
v5 = (( java.lang.Integer ) v5 ).intValue ( ); // invoke-virtual {v5}, Ljava/lang/Integer;->intValue()I
(( com.miui.server.smartpower.PowerFrozenManager ) p0 ).thawProcess ( v4, v5, p1 ); // invoke-virtual {p0, v4, v5, p1}, Lcom/miui/server/smartpower/PowerFrozenManager;->thawProcess(IILjava/lang/String;)Z
/* .line 194 */
/* add-int/lit8 v3, v3, -0x1 */
/* .line 197 */
} // .end local v1 # "frozenUids":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/Integer;>;"
} // .end local v2 # "frozenPids":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/Integer;>;"
} // .end local v3 # "i":I
} // :cond_1
/* monitor-exit v0 */
/* .line 198 */
return;
/* .line 197 */
/* :catchall_0 */
/* move-exception v1 */
/* monitor-exit v0 */
/* :try_end_0 */
/* .catchall {:try_start_0 ..:try_end_0} :catchall_0 */
/* throw v1 */
} // .end method
public Boolean thawProcess ( Integer p0, Integer p1, java.lang.String p2 ) {
/* .locals 7 */
/* .param p1, "uid" # I */
/* .param p2, "pid" # I */
/* .param p3, "reason" # Ljava/lang/String; */
/* .line 129 */
v0 = com.miui.server.smartpower.PowerFrozenManager .isEnable ( );
/* if-nez v0, :cond_0 */
/* .line 130 */
int v0 = 0; // const/4 v0, 0x0
/* .line 133 */
} // :cond_0
int v0 = 0; // const/4 v0, 0x0
/* .line 134 */
/* .local v0, "done":Z */
/* sget-boolean v1, Lcom/miui/app/smartpower/SmartPowerSettings;->PROP_FROZEN_CGROUPV1_ENABLE:Z */
if ( v1 != null) { // if-eqz v1, :cond_1
/* .line 135 */
v1 = this.gzInternal;
v0 = (( com.miui.server.greeze.GreezeManagerInternal ) v1 ).thawPid ( p2 ); // invoke-virtual {v1, p2}, Lcom/miui/server/greeze/GreezeManagerInternal;->thawPid(I)Z
/* .line 137 */
} // :cond_1
v1 = this.gzInternal;
v0 = (( com.miui.server.greeze.GreezeManagerInternal ) v1 ).thawPid ( p2, p1 ); // invoke-virtual {v1, p2, p1}, Lcom/miui/server/greeze/GreezeManagerInternal;->thawPid(II)Z
/* .line 138 */
} // :goto_0
int v1 = 1; // const/4 v1, 0x1
/* invoke-direct {p0, p1, p2, v1}, Lcom/miui/server/smartpower/PowerFrozenManager;->removeFrozenPid(IIZ)V */
/* .line 139 */
v1 = this.mFrozenList;
/* monitor-enter v1 */
/* .line 140 */
try { // :try_start_0
v2 = this.mFrozenList;
(( android.util.SparseArray ) v2 ).get ( p2 ); // invoke-virtual {v2, p2}, Landroid/util/SparseArray;->get(I)Ljava/lang/Object;
/* check-cast v2, Lcom/miui/server/smartpower/PowerFrozenManager$FrozenInfo; */
/* .line 141 */
/* .local v2, "info":Lcom/miui/server/smartpower/PowerFrozenManager$FrozenInfo; */
/* if-nez v2, :cond_2 */
/* .line 142 */
/* new-instance v3, Lcom/miui/server/smartpower/PowerFrozenManager$FrozenInfo; */
/* const-string/jumbo v4, "unknown" */
int v5 = 0; // const/4 v5, 0x0
/* invoke-direct {v3, p1, p2, v4, v5}, Lcom/miui/server/smartpower/PowerFrozenManager$FrozenInfo;-><init>(IILjava/lang/String;Lcom/miui/server/smartpower/PowerFrozenManager$FrozenInfo-IA;)V */
/* move-object v2, v3 */
/* .line 144 */
} // :cond_2
java.lang.System .currentTimeMillis ( );
/* move-result-wide v3 */
com.miui.server.smartpower.PowerFrozenManager$FrozenInfo .-$$Nest$fputmThawTime ( v2,v3,v4 );
/* .line 145 */
android.os.SystemClock .uptimeMillis ( );
/* move-result-wide v3 */
com.miui.server.smartpower.PowerFrozenManager$FrozenInfo .-$$Nest$fputmThawUptime ( v2,v3,v4 );
/* .line 146 */
com.miui.server.smartpower.PowerFrozenManager$FrozenInfo .-$$Nest$fputmThawReason ( v2,p3 );
/* .line 147 */
v3 = this.mFrozenList;
(( android.util.SparseArray ) v3 ).remove ( p2 ); // invoke-virtual {v3, p2}, Landroid/util/SparseArray;->remove(I)V
/* .line 148 */
/* invoke-direct {p0, v2}, Lcom/miui/server/smartpower/PowerFrozenManager;->addHistoryInfo(Lcom/miui/server/smartpower/PowerFrozenManager$FrozenInfo;)V */
/* .line 149 */
/* sget-boolean v3, Lcom/miui/server/smartpower/PowerFrozenManager;->DEBUG:Z */
if ( v3 != null) { // if-eqz v3, :cond_3
/* .line 150 */
final String v3 = "PowerFrozen"; // const-string v3, "PowerFrozen"
/* new-instance v4, Ljava/lang/StringBuilder; */
/* invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V */
final String v5 = "Thaw "; // const-string v5, "Thaw "
(( java.lang.StringBuilder ) v4 ).append ( v5 ); // invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v4 ).append ( v2 ); // invoke-virtual {v4, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;
final String v5 = " "; // const-string v5, " "
(( java.lang.StringBuilder ) v4 ).append ( v5 ); // invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
/* .line 151 */
com.miui.server.smartpower.PowerFrozenManager$FrozenInfo .-$$Nest$mgetFrozenDuration ( v2 );
/* move-result-wide v5 */
(( java.lang.StringBuilder ) v4 ).append ( v5, v6 ); // invoke-virtual {v4, v5, v6}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;
final String v5 = "ms reason:"; // const-string v5, "ms reason:"
(( java.lang.StringBuilder ) v4 ).append ( v5 ); // invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v4 ).append ( p3 ); // invoke-virtual {v4, p3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v4 ).toString ( ); // invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
/* .line 150 */
android.util.Slog .d ( v3,v4 );
/* .line 153 */
} // :cond_3
/* new-instance v3, Ljava/lang/StringBuilder; */
/* invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V */
/* const-string/jumbo v4, "thaw " */
(( java.lang.StringBuilder ) v3 ).append ( v4 ); // invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v3 ).append ( v2 ); // invoke-virtual {v3, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v3 ).toString ( ); // invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
/* const v4, 0x15ff2 */
android.util.EventLog .writeEvent ( v4,v3 );
/* .line 154 */
/* nop */
} // .end local v2 # "info":Lcom/miui/server/smartpower/PowerFrozenManager$FrozenInfo;
/* monitor-exit v1 */
/* .line 155 */
/* .line 154 */
/* :catchall_0 */
/* move-exception v2 */
/* monitor-exit v1 */
/* :try_end_0 */
/* .catchall {:try_start_0 ..:try_end_0} :catchall_0 */
/* throw v2 */
} // .end method
public void thawUid ( Integer p0, java.lang.String p1 ) {
/* .locals 4 */
/* .param p1, "uid" # I */
/* .param p2, "reason" # Ljava/lang/String; */
/* .line 163 */
v0 = this.mAllFrozenPids;
/* monitor-enter v0 */
/* .line 164 */
try { // :try_start_0
(( com.miui.server.smartpower.PowerFrozenManager ) p0 ).getFrozenPids ( p1 ); // invoke-virtual {p0, p1}, Lcom/miui/server/smartpower/PowerFrozenManager;->getFrozenPids(I)Ljava/util/ArrayList;
/* .line 165 */
/* .local v1, "frozenPids":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/Integer;>;" */
v2 = (( java.util.ArrayList ) v1 ).size ( ); // invoke-virtual {v1}, Ljava/util/ArrayList;->size()I
/* add-int/lit8 v2, v2, -0x1 */
/* .local v2, "i":I */
} // :goto_0
/* if-ltz v2, :cond_0 */
/* .line 166 */
(( java.util.ArrayList ) v1 ).get ( v2 ); // invoke-virtual {v1, v2}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;
/* check-cast v3, Ljava/lang/Integer; */
v3 = (( java.lang.Integer ) v3 ).intValue ( ); // invoke-virtual {v3}, Ljava/lang/Integer;->intValue()I
(( com.miui.server.smartpower.PowerFrozenManager ) p0 ).thawProcess ( p1, v3, p2 ); // invoke-virtual {p0, p1, v3, p2}, Lcom/miui/server/smartpower/PowerFrozenManager;->thawProcess(IILjava/lang/String;)Z
/* .line 165 */
/* add-int/lit8 v2, v2, -0x1 */
/* .line 168 */
} // .end local v1 # "frozenPids":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/Integer;>;"
} // .end local v2 # "i":I
} // :cond_0
/* monitor-exit v0 */
/* .line 169 */
return;
/* .line 168 */
/* :catchall_0 */
/* move-exception v1 */
/* monitor-exit v0 */
/* :try_end_0 */
/* .catchall {:try_start_0 ..:try_end_0} :catchall_0 */
/* throw v1 */
} // .end method
public void thawedByOther ( Integer p0, Integer p1 ) {
/* .locals 2 */
/* .param p1, "uid" # I */
/* .param p2, "pid" # I */
/* .line 332 */
v0 = com.miui.server.smartpower.PowerFrozenManager .isEnable ( );
/* if-nez v0, :cond_0 */
/* .line 333 */
return;
/* .line 335 */
} // :cond_0
v0 = this.mFrozenCallbacks;
v1 = } // :goto_0
if ( v1 != null) { // if-eqz v1, :cond_1
/* check-cast v1, Lcom/miui/server/smartpower/PowerFrozenManager$IFrozenReportCallback; */
/* .line 336 */
/* .local v1, "callback":Lcom/miui/server/smartpower/PowerFrozenManager$IFrozenReportCallback; */
/* .line 337 */
} // .end local v1 # "callback":Lcom/miui/server/smartpower/PowerFrozenManager$IFrozenReportCallback;
/* .line 338 */
} // :cond_1
return;
} // .end method
public void unRegisterFrozenCallback ( com.miui.server.smartpower.PowerFrozenManager$IFrozenReportCallback p0 ) {
/* .locals 1 */
/* .param p1, "callback" # Lcom/miui/server/smartpower/PowerFrozenManager$IFrozenReportCallback; */
/* .line 280 */
v0 = this.mFrozenCallbacks;
/* .line 281 */
return;
} // .end method
