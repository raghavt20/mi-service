.class public Lcom/miui/server/smartpower/SmartDisplayPolicyManager$ComingInteraction;
.super Ljava/lang/Object;
.source "SmartDisplayPolicyManager.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/miui/server/smartpower/SmartDisplayPolicyManager;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x9
    name = "ComingInteraction"
.end annotation


# instance fields
.field private mDuration:J

.field private mInteractType:I

.field private mMaxRefreshRate:I

.field private mMinRefreshRate:I


# direct methods
.method static bridge synthetic -$$Nest$fgetmDuration(Lcom/miui/server/smartpower/SmartDisplayPolicyManager$ComingInteraction;)J
    .locals 2

    iget-wide v0, p0, Lcom/miui/server/smartpower/SmartDisplayPolicyManager$ComingInteraction;->mDuration:J

    return-wide v0
.end method

.method static bridge synthetic -$$Nest$fgetmInteractType(Lcom/miui/server/smartpower/SmartDisplayPolicyManager$ComingInteraction;)I
    .locals 0

    iget p0, p0, Lcom/miui/server/smartpower/SmartDisplayPolicyManager$ComingInteraction;->mInteractType:I

    return p0
.end method

.method static bridge synthetic -$$Nest$fgetmMaxRefreshRate(Lcom/miui/server/smartpower/SmartDisplayPolicyManager$ComingInteraction;)I
    .locals 0

    iget p0, p0, Lcom/miui/server/smartpower/SmartDisplayPolicyManager$ComingInteraction;->mMaxRefreshRate:I

    return p0
.end method

.method static bridge synthetic -$$Nest$fgetmMinRefreshRate(Lcom/miui/server/smartpower/SmartDisplayPolicyManager$ComingInteraction;)I
    .locals 0

    iget p0, p0, Lcom/miui/server/smartpower/SmartDisplayPolicyManager$ComingInteraction;->mMinRefreshRate:I

    return p0
.end method

.method private constructor <init>(IIIJ)V
    .locals 0
    .param p1, "mMinRefreshRate"    # I
    .param p2, "mMaxRefreshRate"    # I
    .param p3, "mInteractType"    # I
    .param p4, "mDuration"    # J

    .line 1538
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1539
    iput p1, p0, Lcom/miui/server/smartpower/SmartDisplayPolicyManager$ComingInteraction;->mMinRefreshRate:I

    .line 1540
    iput p2, p0, Lcom/miui/server/smartpower/SmartDisplayPolicyManager$ComingInteraction;->mMaxRefreshRate:I

    .line 1541
    iput p3, p0, Lcom/miui/server/smartpower/SmartDisplayPolicyManager$ComingInteraction;->mInteractType:I

    .line 1542
    iput-wide p4, p0, Lcom/miui/server/smartpower/SmartDisplayPolicyManager$ComingInteraction;->mDuration:J

    .line 1543
    return-void
.end method

.method synthetic constructor <init>(IIIJLcom/miui/server/smartpower/SmartDisplayPolicyManager$ComingInteraction-IA;)V
    .locals 0

    invoke-direct/range {p0 .. p5}, Lcom/miui/server/smartpower/SmartDisplayPolicyManager$ComingInteraction;-><init>(IIIJ)V

    return-void
.end method
