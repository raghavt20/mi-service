.class Lcom/miui/server/smartpower/AppWakelockResource$MyHandler;
.super Landroid/os/Handler;
.source "AppWakelockResource.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/miui/server/smartpower/AppWakelockResource;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "MyHandler"
.end annotation


# instance fields
.field final synthetic this$0:Lcom/miui/server/smartpower/AppWakelockResource;


# direct methods
.method constructor <init>(Lcom/miui/server/smartpower/AppWakelockResource;Landroid/os/Looper;)V
    .locals 0
    .param p2, "looper"    # Landroid/os/Looper;

    .line 271
    iput-object p1, p0, Lcom/miui/server/smartpower/AppWakelockResource$MyHandler;->this$0:Lcom/miui/server/smartpower/AppWakelockResource;

    .line 272
    invoke-direct {p0, p2}, Landroid/os/Handler;-><init>(Landroid/os/Looper;)V

    .line 273
    return-void
.end method


# virtual methods
.method public handleMessage(Landroid/os/Message;)V
    .locals 3
    .param p1, "msg"    # Landroid/os/Message;

    .line 277
    invoke-super {p0, p1}, Landroid/os/Handler;->handleMessage(Landroid/os/Message;)V

    .line 278
    iget v0, p1, Landroid/os/Message;->what:I

    packed-switch v0, :pswitch_data_0

    goto :goto_1

    .line 280
    :pswitch_0
    iget-object v0, p0, Lcom/miui/server/smartpower/AppWakelockResource$MyHandler;->this$0:Lcom/miui/server/smartpower/AppWakelockResource;

    iget v1, p1, Landroid/os/Message;->arg1:I

    iget v2, p1, Landroid/os/Message;->arg2:I

    if-eqz v2, :cond_0

    const/4 v2, 0x1

    goto :goto_0

    :cond_0
    const/4 v2, 0x0

    :goto_0
    invoke-static {v0, v1, v2}, Lcom/miui/server/smartpower/AppWakelockResource;->-$$Nest$msetWakeLockState(Lcom/miui/server/smartpower/AppWakelockResource;IZ)V

    .line 283
    :goto_1
    return-void

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
    .end packed-switch
.end method
