.class Lcom/miui/server/smartpower/AppDisplayResource$1;
.super Ljava/lang/Object;
.source "AppDisplayResource.java"

# interfaces
.implements Ljava/lang/Runnable;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/miui/server/smartpower/AppDisplayResource;->registerCallback(Lcom/miui/server/smartpower/AppPowerResource$IAppPowerResourceCallback;I)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/miui/server/smartpower/AppDisplayResource;

.field final synthetic val$uid:I


# direct methods
.method constructor <init>(Lcom/miui/server/smartpower/AppDisplayResource;I)V
    .locals 0
    .param p1, "this$0"    # Lcom/miui/server/smartpower/AppDisplayResource;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .line 56
    iput-object p1, p0, Lcom/miui/server/smartpower/AppDisplayResource$1;->this$0:Lcom/miui/server/smartpower/AppDisplayResource;

    iput p2, p0, Lcom/miui/server/smartpower/AppDisplayResource$1;->val$uid:I

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public run()V
    .locals 6

    .line 59
    iget-object v0, p0, Lcom/miui/server/smartpower/AppDisplayResource$1;->this$0:Lcom/miui/server/smartpower/AppDisplayResource;

    invoke-static {v0}, Lcom/miui/server/smartpower/AppDisplayResource;->-$$Nest$fgetmDisplayRecordMap(Lcom/miui/server/smartpower/AppDisplayResource;)Landroid/util/ArrayMap;

    move-result-object v0

    monitor-enter v0

    .line 60
    :try_start_0
    iget-object v1, p0, Lcom/miui/server/smartpower/AppDisplayResource$1;->this$0:Lcom/miui/server/smartpower/AppDisplayResource;

    invoke-static {v1}, Lcom/miui/server/smartpower/AppDisplayResource;->-$$Nest$fgetmDisplayRecordMap(Lcom/miui/server/smartpower/AppDisplayResource;)Landroid/util/ArrayMap;

    move-result-object v1

    iget v2, p0, Lcom/miui/server/smartpower/AppDisplayResource$1;->val$uid:I

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v1, v2}, Landroid/util/ArrayMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/miui/server/smartpower/AppDisplayResource$DisplayRecord;

    .line 61
    .local v1, "record":Lcom/miui/server/smartpower/AppDisplayResource$DisplayRecord;
    if-eqz v1, :cond_0

    invoke-virtual {v1}, Lcom/miui/server/smartpower/AppDisplayResource$DisplayRecord;->isActive()Z

    move-result v2

    if-eqz v2, :cond_0

    .line 62
    iget-object v2, p0, Lcom/miui/server/smartpower/AppDisplayResource$1;->this$0:Lcom/miui/server/smartpower/AppDisplayResource;

    iget v3, p0, Lcom/miui/server/smartpower/AppDisplayResource$1;->val$uid:I

    invoke-static {v1}, Lcom/miui/server/smartpower/AppDisplayResource$DisplayRecord;->-$$Nest$fgetmBehavier(Lcom/miui/server/smartpower/AppDisplayResource$DisplayRecord;)I

    move-result v4

    const/4 v5, 0x1

    invoke-virtual {v2, v3, v5, v4}, Lcom/miui/server/smartpower/AppDisplayResource;->reportResourceStatus(IZI)V

    .line 64
    .end local v1    # "record":Lcom/miui/server/smartpower/AppDisplayResource$DisplayRecord;
    :cond_0
    monitor-exit v0

    .line 65
    return-void

    .line 64
    :catchall_0
    move-exception v1

    monitor-exit v0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v1
.end method
