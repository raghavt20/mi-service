public abstract class com.miui.server.smartpower.PowerFrozenManager$IFrozenReportCallback {
	 /* .source "PowerFrozenManager.java" */
	 /* # annotations */
	 /* .annotation system Ldalvik/annotation/EnclosingClass; */
	 /* value = Lcom/miui/server/smartpower/PowerFrozenManager; */
} // .end annotation
/* .annotation system Ldalvik/annotation/InnerClass; */
/* accessFlags = 0x609 */
/* name = "IFrozenReportCallback" */
} // .end annotation
/* # virtual methods */
public abstract void reportBinderState ( Integer p0, Integer p1, Integer p2, Integer p3, Long p4 ) {
} // .end method
public abstract void reportBinderTrans ( Integer p0, Integer p1, Integer p2, Integer p3, Integer p4, Boolean p5, Long p6, Long p7 ) {
} // .end method
public abstract void reportNet ( Integer p0, Long p1 ) {
} // .end method
public abstract void reportSignal ( Integer p0, Integer p1, Long p2 ) {
} // .end method
public abstract void serviceReady ( Boolean p0 ) {
} // .end method
public abstract void thawedByOther ( Integer p0, Integer p1 ) {
} // .end method
