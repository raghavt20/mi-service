class com.miui.server.smartpower.SmartPowerPolicyManager$1 extends android.content.BroadcastReceiver {
	 /* .source "SmartPowerPolicyManager.java" */
	 /* # annotations */
	 /* .annotation system Ldalvik/annotation/EnclosingClass; */
	 /* value = Lcom/miui/server/smartpower/SmartPowerPolicyManager; */
} // .end annotation
/* .annotation system Ldalvik/annotation/InnerClass; */
/* accessFlags = 0x0 */
/* name = null */
} // .end annotation
/* # instance fields */
final com.miui.server.smartpower.SmartPowerPolicyManager this$0; //synthetic
/* # direct methods */
 com.miui.server.smartpower.SmartPowerPolicyManager$1 ( ) {
/* .locals 0 */
/* .param p1, "this$0" # Lcom/miui/server/smartpower/SmartPowerPolicyManager; */
/* .line 215 */
this.this$0 = p1;
/* invoke-direct {p0}, Landroid/content/BroadcastReceiver;-><init>()V */
return;
} // .end method
/* # virtual methods */
public void onReceive ( android.content.Context p0, android.content.Intent p1 ) {
/* .locals 2 */
/* .param p1, "context" # Landroid/content/Context; */
/* .param p2, "intent" # Landroid/content/Intent; */
/* .line 218 */
final String v0 = "android.intent.action.SCREEN_OFF"; // const-string v0, "android.intent.action.SCREEN_OFF"
(( android.content.Intent ) p2 ).getAction ( ); // invoke-virtual {p2}, Landroid/content/Intent;->getAction()Ljava/lang/String;
v0 = (( java.lang.String ) v0 ).equals ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z
if ( v0 != null) { // if-eqz v0, :cond_0
	 /* .line 219 */
	 v0 = this.this$0;
	 int v1 = 1; // const/4 v1, 0x1
	 com.miui.server.smartpower.SmartPowerPolicyManager .-$$Nest$fputmScreenOff ( v0,v1 );
	 /* .line 220 */
} // :cond_0
final String v0 = "android.intent.action.SCREEN_ON"; // const-string v0, "android.intent.action.SCREEN_ON"
(( android.content.Intent ) p2 ).getAction ( ); // invoke-virtual {p2}, Landroid/content/Intent;->getAction()Ljava/lang/String;
v0 = (( java.lang.String ) v0 ).equals ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z
if ( v0 != null) { // if-eqz v0, :cond_1
	 /* .line 221 */
	 v0 = this.this$0;
	 int v1 = 0; // const/4 v1, 0x0
	 com.miui.server.smartpower.SmartPowerPolicyManager .-$$Nest$fputmScreenOff ( v0,v1 );
	 /* .line 222 */
} // :cond_1
final String v0 = "android.intent.action.BOOT_COMPLETED"; // const-string v0, "android.intent.action.BOOT_COMPLETED"
(( android.content.Intent ) p2 ).getAction ( ); // invoke-virtual {p2}, Landroid/content/Intent;->getAction()Ljava/lang/String;
v0 = (( java.lang.String ) v0 ).equals ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z
if ( v0 != null) { // if-eqz v0, :cond_2
	 /* .line 223 */
	 v0 = this.this$0;
	 com.miui.server.smartpower.SmartPowerPolicyManager .-$$Nest$fgetmPowerSavingStrategyControl ( v0 );
	 v0 = 	 com.miui.server.smartpower.SmartPowerPolicyManager$PowerSavingStrategyControl .-$$Nest$fgetmIsInit ( v0 );
	 /* if-nez v0, :cond_2 */
	 /* .line 224 */
	 v0 = this.this$0;
	 com.miui.server.smartpower.SmartPowerPolicyManager .-$$Nest$fgetmPowerSavingStrategyControl ( v0 );
	 com.miui.server.smartpower.SmartPowerPolicyManager$PowerSavingStrategyControl .-$$Nest$mupdateAllNoRestrictApps ( v0 );
	 /* .line 227 */
} // :cond_2
} // :goto_0
return;
} // .end method
