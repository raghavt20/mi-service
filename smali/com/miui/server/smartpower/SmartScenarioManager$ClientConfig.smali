.class public Lcom/miui/server/smartpower/SmartScenarioManager$ClientConfig;
.super Ljava/lang/Object;
.source "SmartScenarioManager.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/miui/server/smartpower/SmartScenarioManager;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x1
    name = "ClientConfig"
.end annotation


# instance fields
.field private mAdditionalScenarioDescription:J

.field private final mAdditionalScenarioIdMap:Landroid/util/LongSparseArray;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/util/LongSparseArray<",
            "Lcom/miui/server/smartpower/SmartScenarioManager$ScenarioIdInfo;",
            ">;"
        }
    .end annotation
.end field

.field private mCallback:Lcom/miui/server/smartpower/SmartScenarioManager$ISmartScenarioCallback;

.field mClientName:Ljava/lang/String;

.field private final mMainScenarioIdMap:Landroid/util/LongSparseArray;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/util/LongSparseArray<",
            "Lcom/miui/server/smartpower/SmartScenarioManager$ScenarioIdInfo;",
            ">;"
        }
    .end annotation
.end field

.field private mMainSenarioDescription:J

.field final synthetic this$0:Lcom/miui/server/smartpower/SmartScenarioManager;


# direct methods
.method static bridge synthetic -$$Nest$fgetmAdditionalScenarioDescription(Lcom/miui/server/smartpower/SmartScenarioManager$ClientConfig;)J
    .locals 2

    iget-wide v0, p0, Lcom/miui/server/smartpower/SmartScenarioManager$ClientConfig;->mAdditionalScenarioDescription:J

    return-wide v0
.end method

.method static bridge synthetic -$$Nest$fgetmCallback(Lcom/miui/server/smartpower/SmartScenarioManager$ClientConfig;)Lcom/miui/server/smartpower/SmartScenarioManager$ISmartScenarioCallback;
    .locals 0

    iget-object p0, p0, Lcom/miui/server/smartpower/SmartScenarioManager$ClientConfig;->mCallback:Lcom/miui/server/smartpower/SmartScenarioManager$ISmartScenarioCallback;

    return-object p0
.end method

.method static bridge synthetic -$$Nest$fgetmMainSenarioDescription(Lcom/miui/server/smartpower/SmartScenarioManager$ClientConfig;)J
    .locals 2

    iget-wide v0, p0, Lcom/miui/server/smartpower/SmartScenarioManager$ClientConfig;->mMainSenarioDescription:J

    return-wide v0
.end method

.method public constructor <init>(Lcom/miui/server/smartpower/SmartScenarioManager;Ljava/lang/String;Lcom/miui/server/smartpower/SmartScenarioManager$ISmartScenarioCallback;)V
    .locals 1
    .param p1, "this$0"    # Lcom/miui/server/smartpower/SmartScenarioManager;
    .param p2, "clientName"    # Ljava/lang/String;
    .param p3, "callback"    # Lcom/miui/server/smartpower/SmartScenarioManager$ISmartScenarioCallback;

    .line 341
    iput-object p1, p0, Lcom/miui/server/smartpower/SmartScenarioManager$ClientConfig;->this$0:Lcom/miui/server/smartpower/SmartScenarioManager;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 337
    new-instance v0, Landroid/util/LongSparseArray;

    invoke-direct {v0}, Landroid/util/LongSparseArray;-><init>()V

    iput-object v0, p0, Lcom/miui/server/smartpower/SmartScenarioManager$ClientConfig;->mMainScenarioIdMap:Landroid/util/LongSparseArray;

    .line 338
    new-instance v0, Landroid/util/LongSparseArray;

    invoke-direct {v0}, Landroid/util/LongSparseArray;-><init>()V

    iput-object v0, p0, Lcom/miui/server/smartpower/SmartScenarioManager$ClientConfig;->mAdditionalScenarioIdMap:Landroid/util/LongSparseArray;

    .line 342
    iput-object p2, p0, Lcom/miui/server/smartpower/SmartScenarioManager$ClientConfig;->mClientName:Ljava/lang/String;

    .line 343
    iput-object p3, p0, Lcom/miui/server/smartpower/SmartScenarioManager$ClientConfig;->mCallback:Lcom/miui/server/smartpower/SmartScenarioManager$ISmartScenarioCallback;

    .line 344
    return-void
.end method

.method private onAdditionalScenarioChanged(ILjava/lang/String;Z)V
    .locals 7
    .param p1, "action"    # I
    .param p2, "packageName"    # Ljava/lang/String;
    .param p3, "add"    # Z

    .line 409
    iget-wide v0, p0, Lcom/miui/server/smartpower/SmartScenarioManager$ClientConfig;->mAdditionalScenarioDescription:J

    .line 410
    .local v0, "currentScenarioDescription":J
    iget-object v2, p0, Lcom/miui/server/smartpower/SmartScenarioManager$ClientConfig;->mAdditionalScenarioIdMap:Landroid/util/LongSparseArray;

    monitor-enter v2

    .line 411
    const/4 v3, 0x0

    .local v3, "i":I
    :goto_0
    :try_start_0
    iget-object v4, p0, Lcom/miui/server/smartpower/SmartScenarioManager$ClientConfig;->mAdditionalScenarioIdMap:Landroid/util/LongSparseArray;

    invoke-virtual {v4}, Landroid/util/LongSparseArray;->size()I

    move-result v4

    if-ge v3, v4, :cond_4

    .line 412
    iget-object v4, p0, Lcom/miui/server/smartpower/SmartScenarioManager$ClientConfig;->mAdditionalScenarioIdMap:Landroid/util/LongSparseArray;

    invoke-virtual {v4, v3}, Landroid/util/LongSparseArray;->valueAt(I)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lcom/miui/server/smartpower/SmartScenarioManager$ScenarioIdInfo;

    .line 413
    .local v4, "info":Lcom/miui/server/smartpower/SmartScenarioManager$ScenarioIdInfo;
    invoke-static {v4}, Lcom/miui/server/smartpower/SmartScenarioManager$ScenarioIdInfo;->-$$Nest$fgetmAction(Lcom/miui/server/smartpower/SmartScenarioManager$ScenarioIdInfo;)I

    move-result v5

    and-int/2addr v5, p1

    if-eqz v5, :cond_3

    if-eqz p2, :cond_0

    invoke-static {v4}, Lcom/miui/server/smartpower/SmartScenarioManager$ScenarioIdInfo;->-$$Nest$fgetmPackageName(Lcom/miui/server/smartpower/SmartScenarioManager$ScenarioIdInfo;)Ljava/lang/String;

    move-result-object v5

    .line 414
    invoke-virtual {p2, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-nez v5, :cond_1

    :cond_0
    if-nez p2, :cond_3

    invoke-static {v4}, Lcom/miui/server/smartpower/SmartScenarioManager$ScenarioIdInfo;->-$$Nest$fgetmPackageName(Lcom/miui/server/smartpower/SmartScenarioManager$ScenarioIdInfo;)Ljava/lang/String;

    move-result-object v5

    if-nez v5, :cond_3

    .line 416
    :cond_1
    if-eqz p3, :cond_2

    .line 417
    invoke-static {v4}, Lcom/miui/server/smartpower/SmartScenarioManager$ScenarioIdInfo;->-$$Nest$fgetmId(Lcom/miui/server/smartpower/SmartScenarioManager$ScenarioIdInfo;)J

    move-result-wide v5

    or-long/2addr v0, v5

    goto :goto_1

    .line 419
    :cond_2
    invoke-static {v4}, Lcom/miui/server/smartpower/SmartScenarioManager$ScenarioIdInfo;->-$$Nest$fgetmId(Lcom/miui/server/smartpower/SmartScenarioManager$ScenarioIdInfo;)J

    move-result-wide v5

    not-long v5, v5

    and-long/2addr v0, v5

    .line 411
    .end local v4    # "info":Lcom/miui/server/smartpower/SmartScenarioManager$ScenarioIdInfo;
    :cond_3
    :goto_1
    add-int/lit8 v3, v3, 0x1

    goto :goto_0

    .line 423
    .end local v3    # "i":I
    :cond_4
    monitor-exit v2
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 424
    iget-wide v2, p0, Lcom/miui/server/smartpower/SmartScenarioManager$ClientConfig;->mAdditionalScenarioDescription:J

    cmp-long v2, v0, v2

    if-eqz v2, :cond_5

    .line 425
    iput-wide v0, p0, Lcom/miui/server/smartpower/SmartScenarioManager$ClientConfig;->mAdditionalScenarioDescription:J

    .line 426
    iget-wide v2, p0, Lcom/miui/server/smartpower/SmartScenarioManager$ClientConfig;->mMainSenarioDescription:J

    invoke-direct {p0, v2, v3, v0, v1}, Lcom/miui/server/smartpower/SmartScenarioManager$ClientConfig;->reportSenarioChanged(JJ)V

    .line 428
    :cond_5
    return-void

    .line 423
    :catchall_0
    move-exception v3

    :try_start_1
    monitor-exit v2
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    throw v3
.end method

.method private onMainScenarioChanged(ILjava/lang/String;Z)V
    .locals 7
    .param p1, "action"    # I
    .param p2, "packageName"    # Ljava/lang/String;
    .param p3, "add"    # Z

    .line 372
    iget-wide v0, p0, Lcom/miui/server/smartpower/SmartScenarioManager$ClientConfig;->mMainSenarioDescription:J

    .line 373
    .local v0, "currentScenarioDescription":J
    iget-object v2, p0, Lcom/miui/server/smartpower/SmartScenarioManager$ClientConfig;->mMainScenarioIdMap:Landroid/util/LongSparseArray;

    monitor-enter v2

    .line 374
    const/4 v3, 0x0

    .local v3, "i":I
    :goto_0
    :try_start_0
    iget-object v4, p0, Lcom/miui/server/smartpower/SmartScenarioManager$ClientConfig;->mMainScenarioIdMap:Landroid/util/LongSparseArray;

    invoke-virtual {v4}, Landroid/util/LongSparseArray;->size()I

    move-result v4

    if-ge v3, v4, :cond_4

    .line 375
    iget-object v4, p0, Lcom/miui/server/smartpower/SmartScenarioManager$ClientConfig;->mMainScenarioIdMap:Landroid/util/LongSparseArray;

    invoke-virtual {v4, v3}, Landroid/util/LongSparseArray;->valueAt(I)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lcom/miui/server/smartpower/SmartScenarioManager$ScenarioIdInfo;

    .line 376
    .local v4, "info":Lcom/miui/server/smartpower/SmartScenarioManager$ScenarioIdInfo;
    invoke-static {v4}, Lcom/miui/server/smartpower/SmartScenarioManager$ScenarioIdInfo;->-$$Nest$fgetmAction(Lcom/miui/server/smartpower/SmartScenarioManager$ScenarioIdInfo;)I

    move-result v5

    and-int/2addr v5, p1

    if-eqz v5, :cond_3

    if-eqz p2, :cond_0

    invoke-static {v4}, Lcom/miui/server/smartpower/SmartScenarioManager$ScenarioIdInfo;->-$$Nest$fgetmPackageName(Lcom/miui/server/smartpower/SmartScenarioManager$ScenarioIdInfo;)Ljava/lang/String;

    move-result-object v5

    .line 377
    invoke-virtual {p2, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-nez v5, :cond_1

    :cond_0
    if-nez p2, :cond_3

    invoke-static {v4}, Lcom/miui/server/smartpower/SmartScenarioManager$ScenarioIdInfo;->-$$Nest$fgetmPackageName(Lcom/miui/server/smartpower/SmartScenarioManager$ScenarioIdInfo;)Ljava/lang/String;

    move-result-object v5

    if-nez v5, :cond_3

    .line 379
    :cond_1
    if-eqz p3, :cond_2

    .line 380
    invoke-static {v4}, Lcom/miui/server/smartpower/SmartScenarioManager$ScenarioIdInfo;->-$$Nest$fgetmId(Lcom/miui/server/smartpower/SmartScenarioManager$ScenarioIdInfo;)J

    move-result-wide v5

    or-long/2addr v0, v5

    goto :goto_1

    .line 382
    :cond_2
    invoke-static {v4}, Lcom/miui/server/smartpower/SmartScenarioManager$ScenarioIdInfo;->-$$Nest$fgetmId(Lcom/miui/server/smartpower/SmartScenarioManager$ScenarioIdInfo;)J

    move-result-wide v5

    not-long v5, v5

    and-long/2addr v0, v5

    .line 374
    .end local v4    # "info":Lcom/miui/server/smartpower/SmartScenarioManager$ScenarioIdInfo;
    :cond_3
    :goto_1
    add-int/lit8 v3, v3, 0x1

    goto :goto_0

    .line 386
    .end local v3    # "i":I
    :cond_4
    monitor-exit v2
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 387
    iget-wide v2, p0, Lcom/miui/server/smartpower/SmartScenarioManager$ClientConfig;->mMainSenarioDescription:J

    cmp-long v2, v0, v2

    if-eqz v2, :cond_5

    .line 388
    iput-wide v0, p0, Lcom/miui/server/smartpower/SmartScenarioManager$ClientConfig;->mMainSenarioDescription:J

    .line 389
    iget-wide v2, p0, Lcom/miui/server/smartpower/SmartScenarioManager$ClientConfig;->mAdditionalScenarioDescription:J

    invoke-direct {p0, v0, v1, v2, v3}, Lcom/miui/server/smartpower/SmartScenarioManager$ClientConfig;->reportSenarioChanged(JJ)V

    .line 391
    :cond_5
    return-void

    .line 386
    :catchall_0
    move-exception v3

    :try_start_1
    monitor-exit v2
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    throw v3
.end method

.method private reportSenarioChanged(JJ)V
    .locals 8
    .param p1, "mainScenario"    # J
    .param p3, "additionalScenario"    # J

    .line 394
    iget-object v0, p0, Lcom/miui/server/smartpower/SmartScenarioManager$ClientConfig;->this$0:Lcom/miui/server/smartpower/SmartScenarioManager;

    invoke-static {v0}, Lcom/miui/server/smartpower/SmartScenarioManager;->-$$Nest$fgetmHandler(Lcom/miui/server/smartpower/SmartScenarioManager;)Landroid/os/Handler;

    move-result-object v0

    new-instance v7, Lcom/miui/server/smartpower/SmartScenarioManager$ClientConfig$1;

    move-object v1, v7

    move-object v2, p0

    move-wide v3, p1

    move-wide v5, p3

    invoke-direct/range {v1 .. v6}, Lcom/miui/server/smartpower/SmartScenarioManager$ClientConfig$1;-><init>(Lcom/miui/server/smartpower/SmartScenarioManager$ClientConfig;JJ)V

    invoke-virtual {v0, v7}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    .line 406
    return-void
.end method


# virtual methods
.method public addAdditionalScenarioIdConfig(JLjava/lang/String;I)V
    .locals 9
    .param p1, "id"    # J
    .param p3, "packageName"    # Ljava/lang/String;
    .param p4, "action"    # I

    .line 353
    const/16 v0, 0x8

    if-ne p4, v0, :cond_0

    .line 354
    iget-object v0, p0, Lcom/miui/server/smartpower/SmartScenarioManager$ClientConfig;->this$0:Lcom/miui/server/smartpower/SmartScenarioManager;

    invoke-static {v0}, Lcom/miui/server/smartpower/SmartScenarioManager;->-$$Nest$fgetmInterestBgPkgs(Lcom/miui/server/smartpower/SmartScenarioManager;)Landroid/util/ArraySet;

    move-result-object v0

    monitor-enter v0

    .line 355
    :try_start_0
    iget-object v1, p0, Lcom/miui/server/smartpower/SmartScenarioManager$ClientConfig;->this$0:Lcom/miui/server/smartpower/SmartScenarioManager;

    invoke-static {v1}, Lcom/miui/server/smartpower/SmartScenarioManager;->-$$Nest$fgetmInterestBgPkgs(Lcom/miui/server/smartpower/SmartScenarioManager;)Landroid/util/ArraySet;

    move-result-object v1

    invoke-virtual {v1, p3}, Landroid/util/ArraySet;->add(Ljava/lang/Object;)Z

    .line 356
    monitor-exit v0

    goto :goto_0

    :catchall_0
    move-exception v1

    monitor-exit v0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v1

    .line 358
    :cond_0
    :goto_0
    iget-object v0, p0, Lcom/miui/server/smartpower/SmartScenarioManager$ClientConfig;->mAdditionalScenarioIdMap:Landroid/util/LongSparseArray;

    monitor-enter v0

    .line 359
    :try_start_1
    iget-object v1, p0, Lcom/miui/server/smartpower/SmartScenarioManager$ClientConfig;->mAdditionalScenarioIdMap:Landroid/util/LongSparseArray;

    new-instance v8, Lcom/miui/server/smartpower/SmartScenarioManager$ScenarioIdInfo;

    iget-object v3, p0, Lcom/miui/server/smartpower/SmartScenarioManager$ClientConfig;->this$0:Lcom/miui/server/smartpower/SmartScenarioManager;

    move-object v2, v8

    move-object v4, p3

    move v5, p4

    move-wide v6, p1

    invoke-direct/range {v2 .. v7}, Lcom/miui/server/smartpower/SmartScenarioManager$ScenarioIdInfo;-><init>(Lcom/miui/server/smartpower/SmartScenarioManager;Ljava/lang/String;IJ)V

    invoke-virtual {v1, p1, p2, v8}, Landroid/util/LongSparseArray;->put(JLjava/lang/Object;)V

    .line 360
    monitor-exit v0

    .line 361
    return-void

    .line 360
    :catchall_1
    move-exception v1

    monitor-exit v0
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_1

    throw v1
.end method

.method public addMainScenarioIdConfig(JLjava/lang/String;I)V
    .locals 9
    .param p1, "id"    # J
    .param p3, "packageName"    # Ljava/lang/String;
    .param p4, "action"    # I

    .line 347
    iget-object v0, p0, Lcom/miui/server/smartpower/SmartScenarioManager$ClientConfig;->mMainScenarioIdMap:Landroid/util/LongSparseArray;

    monitor-enter v0

    .line 348
    :try_start_0
    iget-object v1, p0, Lcom/miui/server/smartpower/SmartScenarioManager$ClientConfig;->mMainScenarioIdMap:Landroid/util/LongSparseArray;

    new-instance v8, Lcom/miui/server/smartpower/SmartScenarioManager$ScenarioIdInfo;

    iget-object v3, p0, Lcom/miui/server/smartpower/SmartScenarioManager$ClientConfig;->this$0:Lcom/miui/server/smartpower/SmartScenarioManager;

    move-object v2, v8

    move-object v4, p3

    move v5, p4

    move-wide v6, p1

    invoke-direct/range {v2 .. v7}, Lcom/miui/server/smartpower/SmartScenarioManager$ScenarioIdInfo;-><init>(Lcom/miui/server/smartpower/SmartScenarioManager;Ljava/lang/String;IJ)V

    invoke-virtual {v1, p1, p2, v8}, Landroid/util/LongSparseArray;->put(JLjava/lang/Object;)V

    .line 349
    monitor-exit v0

    .line 350
    return-void

    .line 349
    :catchall_0
    move-exception v1

    monitor-exit v0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v1
.end method

.method public dump(Ljava/io/PrintWriter;[Ljava/lang/String;I)V
    .locals 9
    .param p1, "pw"    # Ljava/io/PrintWriter;
    .param p2, "args"    # [Ljava/lang/String;
    .param p3, "opti"    # I

    .line 431
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "client "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lcom/miui/server/smartpower/SmartScenarioManager$ClientConfig;->mClientName:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ":"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v0}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V

    .line 432
    const-string v0, "    main scenario:"

    invoke-virtual {p1, v0}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V

    .line 433
    iget-object v0, p0, Lcom/miui/server/smartpower/SmartScenarioManager$ClientConfig;->mMainScenarioIdMap:Landroid/util/LongSparseArray;

    monitor-enter v0

    .line 434
    const/4 v1, 0x0

    .local v1, "i":I
    :goto_0
    :try_start_0
    iget-object v2, p0, Lcom/miui/server/smartpower/SmartScenarioManager$ClientConfig;->mMainScenarioIdMap:Landroid/util/LongSparseArray;

    invoke-virtual {v2}, Landroid/util/LongSparseArray;->size()I

    move-result v2

    const-wide/16 v3, 0x0

    if-ge v1, v2, :cond_1

    .line 435
    iget-object v2, p0, Lcom/miui/server/smartpower/SmartScenarioManager$ClientConfig;->mMainScenarioIdMap:Landroid/util/LongSparseArray;

    invoke-virtual {v2, v1}, Landroid/util/LongSparseArray;->valueAt(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/miui/server/smartpower/SmartScenarioManager$ScenarioIdInfo;

    .line 436
    .local v2, "info":Lcom/miui/server/smartpower/SmartScenarioManager$ScenarioIdInfo;
    invoke-static {v2}, Lcom/miui/server/smartpower/SmartScenarioManager$ScenarioIdInfo;->-$$Nest$fgetmId(Lcom/miui/server/smartpower/SmartScenarioManager$ScenarioIdInfo;)J

    move-result-wide v5

    iget-wide v7, p0, Lcom/miui/server/smartpower/SmartScenarioManager$ClientConfig;->mMainSenarioDescription:J

    and-long/2addr v5, v7

    cmp-long v3, v5, v3

    if-eqz v3, :cond_0

    .line 437
    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "        "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {p1, v3}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V

    .line 434
    .end local v2    # "info":Lcom/miui/server/smartpower/SmartScenarioManager$ScenarioIdInfo;
    :cond_0
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 440
    .end local v1    # "i":I
    :cond_1
    monitor-exit v0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 442
    const-string v0, "    addtional scenario:"

    invoke-virtual {p1, v0}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V

    .line 443
    iget-object v1, p0, Lcom/miui/server/smartpower/SmartScenarioManager$ClientConfig;->mAdditionalScenarioIdMap:Landroid/util/LongSparseArray;

    monitor-enter v1

    .line 444
    const/4 v0, 0x0

    .local v0, "i":I
    :goto_1
    :try_start_1
    iget-object v2, p0, Lcom/miui/server/smartpower/SmartScenarioManager$ClientConfig;->mAdditionalScenarioIdMap:Landroid/util/LongSparseArray;

    invoke-virtual {v2}, Landroid/util/LongSparseArray;->size()I

    move-result v2

    if-ge v0, v2, :cond_3

    .line 445
    iget-object v2, p0, Lcom/miui/server/smartpower/SmartScenarioManager$ClientConfig;->mAdditionalScenarioIdMap:Landroid/util/LongSparseArray;

    invoke-virtual {v2, v0}, Landroid/util/LongSparseArray;->valueAt(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/miui/server/smartpower/SmartScenarioManager$ScenarioIdInfo;

    .line 446
    .restart local v2    # "info":Lcom/miui/server/smartpower/SmartScenarioManager$ScenarioIdInfo;
    invoke-static {v2}, Lcom/miui/server/smartpower/SmartScenarioManager$ScenarioIdInfo;->-$$Nest$fgetmId(Lcom/miui/server/smartpower/SmartScenarioManager$ScenarioIdInfo;)J

    move-result-wide v5

    iget-wide v7, p0, Lcom/miui/server/smartpower/SmartScenarioManager$ClientConfig;->mAdditionalScenarioDescription:J

    and-long/2addr v5, v7

    cmp-long v5, v5, v3

    if-eqz v5, :cond_2

    .line 447
    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "        "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {p1, v5}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V

    .line 444
    .end local v2    # "info":Lcom/miui/server/smartpower/SmartScenarioManager$ScenarioIdInfo;
    :cond_2
    add-int/lit8 v0, v0, 0x1

    goto :goto_1

    .line 450
    .end local v0    # "i":I
    :cond_3
    monitor-exit v1

    .line 451
    return-void

    .line 450
    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    throw v0

    .line 440
    :catchall_1
    move-exception v1

    :try_start_2
    monitor-exit v0
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    throw v1
.end method

.method public onAppActionChanged(ILjava/lang/String;Z)V
    .locals 1
    .param p1, "action"    # I
    .param p2, "packageName"    # Ljava/lang/String;
    .param p3, "add"    # Z

    .line 364
    and-int/lit8 v0, p1, 0x6

    if-eqz v0, :cond_0

    .line 365
    invoke-direct {p0, p1, p2, p3}, Lcom/miui/server/smartpower/SmartScenarioManager$ClientConfig;->onMainScenarioChanged(ILjava/lang/String;Z)V

    goto :goto_0

    .line 366
    :cond_0
    const v0, 0x1fff8

    and-int/2addr v0, p1

    if-eqz v0, :cond_1

    .line 367
    invoke-direct {p0, p1, p2, p3}, Lcom/miui/server/smartpower/SmartScenarioManager$ClientConfig;->onAdditionalScenarioChanged(ILjava/lang/String;Z)V

    .line 369
    :cond_1
    :goto_0
    return-void
.end method
