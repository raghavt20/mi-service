.class Lcom/miui/server/smartpower/AppGPSResource$GpsRecord;
.super Ljava/lang/Object;
.source "AppGPSResource.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/miui/server/smartpower/AppGPSResource;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = "GpsRecord"
.end annotation


# instance fields
.field private mLocationListeners:Landroid/util/ArraySet;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/util/ArraySet<",
            "Landroid/location/ILocationListener;",
            ">;"
        }
    .end annotation
.end field

.field private final mPid:I

.field private final mUid:I

.field final synthetic this$0:Lcom/miui/server/smartpower/AppGPSResource;


# direct methods
.method static bridge synthetic -$$Nest$fgetmUid(Lcom/miui/server/smartpower/AppGPSResource$GpsRecord;)I
    .locals 0

    iget p0, p0, Lcom/miui/server/smartpower/AppGPSResource$GpsRecord;->mUid:I

    return p0
.end method

.method constructor <init>(Lcom/miui/server/smartpower/AppGPSResource;II)V
    .locals 1
    .param p1, "this$0"    # Lcom/miui/server/smartpower/AppGPSResource;
    .param p2, "uid"    # I
    .param p3, "pid"    # I

    .line 100
    iput-object p1, p0, Lcom/miui/server/smartpower/AppGPSResource$GpsRecord;->this$0:Lcom/miui/server/smartpower/AppGPSResource;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 98
    new-instance v0, Landroid/util/ArraySet;

    invoke-direct {v0}, Landroid/util/ArraySet;-><init>()V

    iput-object v0, p0, Lcom/miui/server/smartpower/AppGPSResource$GpsRecord;->mLocationListeners:Landroid/util/ArraySet;

    .line 101
    iput p2, p0, Lcom/miui/server/smartpower/AppGPSResource$GpsRecord;->mUid:I

    .line 102
    iput p3, p0, Lcom/miui/server/smartpower/AppGPSResource$GpsRecord;->mPid:I

    .line 103
    return-void
.end method


# virtual methods
.method getPid()I
    .locals 1

    .line 110
    iget v0, p0, Lcom/miui/server/smartpower/AppGPSResource$GpsRecord;->mPid:I

    return v0
.end method

.method getUid()I
    .locals 1

    .line 114
    iget v0, p0, Lcom/miui/server/smartpower/AppGPSResource$GpsRecord;->mUid:I

    return v0
.end method

.method isActive()Z
    .locals 1

    .line 106
    iget-object v0, p0, Lcom/miui/server/smartpower/AppGPSResource$GpsRecord;->mLocationListeners:Landroid/util/ArraySet;

    invoke-virtual {v0}, Landroid/util/ArraySet;->size()I

    move-result v0

    if-lez v0, :cond_0

    const/4 v0, 0x1

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    :goto_0
    return v0
.end method

.method onAquireLocation(Landroid/location/ILocationListener;)V
    .locals 4
    .param p1, "listener"    # Landroid/location/ILocationListener;

    .line 118
    const/4 v0, 0x0

    .local v0, "i":I
    :goto_0
    iget-object v1, p0, Lcom/miui/server/smartpower/AppGPSResource$GpsRecord;->mLocationListeners:Landroid/util/ArraySet;

    invoke-virtual {v1}, Landroid/util/ArraySet;->size()I

    move-result v1

    if-ge v0, v1, :cond_1

    .line 119
    iget-object v1, p0, Lcom/miui/server/smartpower/AppGPSResource$GpsRecord;->mLocationListeners:Landroid/util/ArraySet;

    invoke-virtual {v1, v0}, Landroid/util/ArraySet;->valueAt(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/location/ILocationListener;

    .line 120
    .local v1, "item":Landroid/location/ILocationListener;
    invoke-interface {v1}, Landroid/location/ILocationListener;->asBinder()Landroid/os/IBinder;

    move-result-object v2

    invoke-interface {p1}, Landroid/location/ILocationListener;->asBinder()Landroid/os/IBinder;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_0

    .line 121
    return-void

    .line 118
    .end local v1    # "item":Landroid/location/ILocationListener;
    :cond_0
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 124
    .end local v0    # "i":I
    :cond_1
    iget-object v0, p0, Lcom/miui/server/smartpower/AppGPSResource$GpsRecord;->mLocationListeners:Landroid/util/ArraySet;

    invoke-virtual {v0, p1}, Landroid/util/ArraySet;->add(Ljava/lang/Object;)Z

    .line 125
    return-void
.end method

.method onReleaseLocation(Landroid/location/ILocationListener;)V
    .locals 4
    .param p1, "listener"    # Landroid/location/ILocationListener;

    .line 128
    const/4 v0, 0x0

    .local v0, "i":I
    :goto_0
    iget-object v1, p0, Lcom/miui/server/smartpower/AppGPSResource$GpsRecord;->mLocationListeners:Landroid/util/ArraySet;

    invoke-virtual {v1}, Landroid/util/ArraySet;->size()I

    move-result v1

    if-ge v0, v1, :cond_1

    .line 129
    iget-object v1, p0, Lcom/miui/server/smartpower/AppGPSResource$GpsRecord;->mLocationListeners:Landroid/util/ArraySet;

    invoke-virtual {v1, v0}, Landroid/util/ArraySet;->valueAt(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/location/ILocationListener;

    .line 130
    .local v1, "item":Landroid/location/ILocationListener;
    invoke-interface {v1}, Landroid/location/ILocationListener;->asBinder()Landroid/os/IBinder;

    move-result-object v2

    invoke-interface {p1}, Landroid/location/ILocationListener;->asBinder()Landroid/os/IBinder;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_0

    .line 131
    iget-object v2, p0, Lcom/miui/server/smartpower/AppGPSResource$GpsRecord;->mLocationListeners:Landroid/util/ArraySet;

    invoke-virtual {v2, v1}, Landroid/util/ArraySet;->remove(Ljava/lang/Object;)Z

    .line 128
    .end local v1    # "item":Landroid/location/ILocationListener;
    :cond_0
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 134
    .end local v0    # "i":I
    :cond_1
    return-void
.end method
