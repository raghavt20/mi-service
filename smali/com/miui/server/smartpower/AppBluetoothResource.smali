.class Lcom/miui/server/smartpower/AppBluetoothResource;
.super Lcom/miui/server/smartpower/AppPowerResource;
.source "AppBluetoothResource.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/miui/server/smartpower/AppBluetoothResource$BleRecord;
    }
.end annotation


# instance fields
.field private final mActivePidsMap:Landroid/util/SparseArray;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/util/SparseArray<",
            "Lcom/miui/server/smartpower/AppBluetoothResource$BleRecord;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Landroid/content/Context;Landroid/os/Looper;)V
    .locals 1
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "looper"    # Landroid/os/Looper;

    .line 21
    invoke-direct {p0}, Lcom/miui/server/smartpower/AppPowerResource;-><init>()V

    .line 19
    new-instance v0, Landroid/util/SparseArray;

    invoke-direct {v0}, Landroid/util/SparseArray;-><init>()V

    iput-object v0, p0, Lcom/miui/server/smartpower/AppBluetoothResource;->mActivePidsMap:Landroid/util/SparseArray;

    .line 22
    const/4 v0, 0x5

    iput v0, p0, Lcom/miui/server/smartpower/AppBluetoothResource;->mType:I

    .line 23
    return-void
.end method

.method private getBleRecord(I)Lcom/miui/server/smartpower/AppBluetoothResource$BleRecord;
    .locals 2
    .param p1, "pid"    # I

    .line 26
    iget-object v0, p0, Lcom/miui/server/smartpower/AppBluetoothResource;->mActivePidsMap:Landroid/util/SparseArray;

    monitor-enter v0

    .line 27
    :try_start_0
    iget-object v1, p0, Lcom/miui/server/smartpower/AppBluetoothResource;->mActivePidsMap:Landroid/util/SparseArray;

    invoke-virtual {v1, p1}, Landroid/util/SparseArray;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/miui/server/smartpower/AppBluetoothResource$BleRecord;

    monitor-exit v0

    return-object v1

    .line 28
    :catchall_0
    move-exception v1

    monitor-exit v0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v1
.end method

.method private getOrCreateBleRecord(II)Lcom/miui/server/smartpower/AppBluetoothResource$BleRecord;
    .locals 3
    .param p1, "uid"    # I
    .param p2, "pid"    # I

    .line 32
    iget-object v0, p0, Lcom/miui/server/smartpower/AppBluetoothResource;->mActivePidsMap:Landroid/util/SparseArray;

    monitor-enter v0

    .line 33
    :try_start_0
    iget-object v1, p0, Lcom/miui/server/smartpower/AppBluetoothResource;->mActivePidsMap:Landroid/util/SparseArray;

    invoke-virtual {v1, p2}, Landroid/util/SparseArray;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/miui/server/smartpower/AppBluetoothResource$BleRecord;

    .line 34
    .local v1, "record":Lcom/miui/server/smartpower/AppBluetoothResource$BleRecord;
    if-nez v1, :cond_0

    .line 35
    new-instance v2, Lcom/miui/server/smartpower/AppBluetoothResource$BleRecord;

    invoke-direct {v2, p0, p1, p2}, Lcom/miui/server/smartpower/AppBluetoothResource$BleRecord;-><init>(Lcom/miui/server/smartpower/AppBluetoothResource;II)V

    move-object v1, v2

    .line 36
    iget-object v2, p0, Lcom/miui/server/smartpower/AppBluetoothResource;->mActivePidsMap:Landroid/util/SparseArray;

    invoke-virtual {v2, p2, v1}, Landroid/util/SparseArray;->put(ILjava/lang/Object;)V

    .line 38
    :cond_0
    monitor-exit v0

    return-object v1

    .line 39
    .end local v1    # "record":Lcom/miui/server/smartpower/AppBluetoothResource$BleRecord;
    :catchall_0
    move-exception v1

    monitor-exit v0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v1
.end method


# virtual methods
.method public getActiveUids()Ljava/util/ArrayList;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/ArrayList<",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation

    .line 44
    const/4 v0, 0x0

    return-object v0
.end method

.method public isAppResourceActive(I)Z
    .locals 5
    .param p1, "uid"    # I

    .line 49
    const/4 v0, 0x0

    .line 50
    .local v0, "active":Z
    iget-object v1, p0, Lcom/miui/server/smartpower/AppBluetoothResource;->mActivePidsMap:Landroid/util/SparseArray;

    monitor-enter v1

    .line 51
    const/4 v2, 0x0

    .local v2, "i":I
    :goto_0
    :try_start_0
    iget-object v3, p0, Lcom/miui/server/smartpower/AppBluetoothResource;->mActivePidsMap:Landroid/util/SparseArray;

    invoke-virtual {v3}, Landroid/util/SparseArray;->size()I

    move-result v3

    if-ge v2, v3, :cond_1

    .line 52
    iget-object v3, p0, Lcom/miui/server/smartpower/AppBluetoothResource;->mActivePidsMap:Landroid/util/SparseArray;

    invoke-virtual {v3, v2}, Landroid/util/SparseArray;->valueAt(I)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/miui/server/smartpower/AppBluetoothResource$BleRecord;

    .line 53
    .local v3, "record":Lcom/miui/server/smartpower/AppBluetoothResource$BleRecord;
    if-eqz v3, :cond_0

    iget v4, v3, Lcom/miui/server/smartpower/AppBluetoothResource$BleRecord;->mOwnerUid:I

    if-ne v4, p1, :cond_0

    invoke-virtual {v3}, Lcom/miui/server/smartpower/AppBluetoothResource$BleRecord;->isActive()Z

    move-result v4

    if-eqz v4, :cond_0

    .line 54
    monitor-exit v1

    const/4 v1, 0x1

    return v1

    .line 51
    .end local v3    # "record":Lcom/miui/server/smartpower/AppBluetoothResource$BleRecord;
    :cond_0
    add-int/lit8 v2, v2, 0x1

    goto :goto_0

    .line 57
    .end local v2    # "i":I
    :cond_1
    monitor-exit v1

    .line 58
    return v0

    .line 57
    :catchall_0
    move-exception v2

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v2
.end method

.method public isAppResourceActive(II)Z
    .locals 2
    .param p1, "uid"    # I
    .param p2, "pid"    # I

    .line 63
    invoke-direct {p0, p2}, Lcom/miui/server/smartpower/AppBluetoothResource;->getBleRecord(I)Lcom/miui/server/smartpower/AppBluetoothResource$BleRecord;

    move-result-object v0

    .line 64
    .local v0, "record":Lcom/miui/server/smartpower/AppBluetoothResource$BleRecord;
    if-eqz v0, :cond_0

    .line 65
    invoke-virtual {v0}, Lcom/miui/server/smartpower/AppBluetoothResource$BleRecord;->isActive()Z

    move-result v1

    return v1

    .line 67
    :cond_0
    const/4 v1, 0x0

    return v1
.end method

.method public onBluetoothEvent(ZIIII)V
    .locals 3
    .param p1, "isConnect"    # Z
    .param p2, "bleType"    # I
    .param p3, "uid"    # I
    .param p4, "pid"    # I
    .param p5, "flag"    # I

    .line 106
    sget-boolean v0, Lcom/miui/server/smartpower/AppBluetoothResource;->DEBUG:Z

    if-eqz v0, :cond_0

    .line 107
    const-string v0, "SmartPower.AppResource"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "bluttooth: connect="

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " bleType="

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " uid="

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " pid="

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " bleType="

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " flag="

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p5}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Slog;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 114
    :cond_0
    if-eqz p1, :cond_1

    .line 115
    invoke-direct {p0, p3, p4}, Lcom/miui/server/smartpower/AppBluetoothResource;->getOrCreateBleRecord(II)Lcom/miui/server/smartpower/AppBluetoothResource$BleRecord;

    move-result-object v0

    .line 116
    .local v0, "record":Lcom/miui/server/smartpower/AppBluetoothResource$BleRecord;
    invoke-virtual {v0, p2, p5}, Lcom/miui/server/smartpower/AppBluetoothResource$BleRecord;->onBluetoothConnect(II)V

    .line 117
    .end local v0    # "record":Lcom/miui/server/smartpower/AppBluetoothResource$BleRecord;
    goto :goto_0

    .line 118
    :cond_1
    invoke-direct {p0, p4}, Lcom/miui/server/smartpower/AppBluetoothResource;->getBleRecord(I)Lcom/miui/server/smartpower/AppBluetoothResource$BleRecord;

    move-result-object v0

    .line 119
    .restart local v0    # "record":Lcom/miui/server/smartpower/AppBluetoothResource$BleRecord;
    if-eqz v0, :cond_2

    .line 120
    invoke-virtual {v0, p2, p5}, Lcom/miui/server/smartpower/AppBluetoothResource$BleRecord;->onBluetoothDisconnect(II)V

    .line 121
    invoke-virtual {v0}, Lcom/miui/server/smartpower/AppBluetoothResource$BleRecord;->isActive()Z

    move-result v1

    if-nez v1, :cond_2

    .line 122
    iget-object v1, p0, Lcom/miui/server/smartpower/AppBluetoothResource;->mActivePidsMap:Landroid/util/SparseArray;

    monitor-enter v1

    .line 123
    :try_start_0
    iget-object v2, p0, Lcom/miui/server/smartpower/AppBluetoothResource;->mActivePidsMap:Landroid/util/SparseArray;

    invoke-virtual {v2, p4}, Landroid/util/SparseArray;->remove(I)V

    .line 124
    monitor-exit v1

    goto :goto_0

    :catchall_0
    move-exception v2

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v2

    .line 128
    .end local v0    # "record":Lcom/miui/server/smartpower/AppBluetoothResource$BleRecord;
    :cond_2
    :goto_0
    return-void
.end method

.method public registerCallback(Lcom/miui/server/smartpower/AppPowerResource$IAppPowerResourceCallback;I)V
    .locals 0
    .param p1, "callback"    # Lcom/miui/server/smartpower/AppPowerResource$IAppPowerResourceCallback;
    .param p2, "uid"    # I

    .line 82
    invoke-super {p0, p1, p2}, Lcom/miui/server/smartpower/AppPowerResource;->registerCallback(Lcom/miui/server/smartpower/AppPowerResource$IAppPowerResourceCallback;I)V

    .line 83
    return-void
.end method

.method public registerCallback(Lcom/miui/server/smartpower/AppPowerResource$IAppPowerResourceCallback;II)V
    .locals 0
    .param p1, "callback"    # Lcom/miui/server/smartpower/AppPowerResource$IAppPowerResourceCallback;
    .param p2, "uid"    # I
    .param p3, "pid"    # I

    .line 92
    invoke-super {p0, p1, p2, p3}, Lcom/miui/server/smartpower/AppPowerResource;->registerCallback(Lcom/miui/server/smartpower/AppPowerResource$IAppPowerResourceCallback;II)V

    .line 93
    return-void
.end method

.method public releaseAppPowerResource(I)V
    .locals 0
    .param p1, "uid"    # I

    .line 73
    return-void
.end method

.method public resumeAppPowerResource(I)V
    .locals 0
    .param p1, "uid"    # I

    .line 78
    return-void
.end method

.method public unRegisterCallback(Lcom/miui/server/smartpower/AppPowerResource$IAppPowerResourceCallback;I)V
    .locals 0
    .param p1, "callback"    # Lcom/miui/server/smartpower/AppPowerResource$IAppPowerResourceCallback;
    .param p2, "uid"    # I

    .line 87
    invoke-super {p0, p1, p2}, Lcom/miui/server/smartpower/AppPowerResource;->unRegisterCallback(Lcom/miui/server/smartpower/AppPowerResource$IAppPowerResourceCallback;I)V

    .line 88
    return-void
.end method

.method public unRegisterCallback(Lcom/miui/server/smartpower/AppPowerResource$IAppPowerResourceCallback;II)V
    .locals 2
    .param p1, "callback"    # Lcom/miui/server/smartpower/AppPowerResource$IAppPowerResourceCallback;
    .param p2, "uid"    # I
    .param p3, "pid"    # I

    .line 97
    iget-object v0, p0, Lcom/miui/server/smartpower/AppBluetoothResource;->mActivePidsMap:Landroid/util/SparseArray;

    monitor-enter v0

    .line 98
    :try_start_0
    iget-object v1, p0, Lcom/miui/server/smartpower/AppBluetoothResource;->mActivePidsMap:Landroid/util/SparseArray;

    invoke-virtual {v1, p3}, Landroid/util/SparseArray;->get(I)Ljava/lang/Object;

    move-result-object v1

    if-eqz v1, :cond_0

    .line 99
    iget-object v1, p0, Lcom/miui/server/smartpower/AppBluetoothResource;->mActivePidsMap:Landroid/util/SparseArray;

    invoke-virtual {v1, p3}, Landroid/util/SparseArray;->remove(I)V

    .line 101
    :cond_0
    monitor-exit v0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 102
    invoke-super {p0, p1, p2, p3}, Lcom/miui/server/smartpower/AppPowerResource;->unRegisterCallback(Lcom/miui/server/smartpower/AppPowerResource$IAppPowerResourceCallback;II)V

    .line 103
    return-void

    .line 101
    :catchall_0
    move-exception v1

    :try_start_1
    monitor-exit v0
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    throw v1
.end method
