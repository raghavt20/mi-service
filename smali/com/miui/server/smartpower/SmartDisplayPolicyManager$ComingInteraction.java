public class com.miui.server.smartpower.SmartDisplayPolicyManager$ComingInteraction {
	 /* .source "SmartDisplayPolicyManager.java" */
	 /* # annotations */
	 /* .annotation system Ldalvik/annotation/EnclosingClass; */
	 /* value = Lcom/miui/server/smartpower/SmartDisplayPolicyManager; */
} // .end annotation
/* .annotation system Ldalvik/annotation/InnerClass; */
/* accessFlags = 0x9 */
/* name = "ComingInteraction" */
} // .end annotation
/* # instance fields */
private Long mDuration;
private Integer mInteractType;
private Integer mMaxRefreshRate;
private Integer mMinRefreshRate;
/* # direct methods */
static Long -$$Nest$fgetmDuration ( com.miui.server.smartpower.SmartDisplayPolicyManager$ComingInteraction p0 ) { //bridge//synthethic
/* .locals 2 */
/* iget-wide v0, p0, Lcom/miui/server/smartpower/SmartDisplayPolicyManager$ComingInteraction;->mDuration:J */
/* return-wide v0 */
} // .end method
static Integer -$$Nest$fgetmInteractType ( com.miui.server.smartpower.SmartDisplayPolicyManager$ComingInteraction p0 ) { //bridge//synthethic
/* .locals 0 */
/* iget p0, p0, Lcom/miui/server/smartpower/SmartDisplayPolicyManager$ComingInteraction;->mInteractType:I */
} // .end method
static Integer -$$Nest$fgetmMaxRefreshRate ( com.miui.server.smartpower.SmartDisplayPolicyManager$ComingInteraction p0 ) { //bridge//synthethic
/* .locals 0 */
/* iget p0, p0, Lcom/miui/server/smartpower/SmartDisplayPolicyManager$ComingInteraction;->mMaxRefreshRate:I */
} // .end method
static Integer -$$Nest$fgetmMinRefreshRate ( com.miui.server.smartpower.SmartDisplayPolicyManager$ComingInteraction p0 ) { //bridge//synthethic
/* .locals 0 */
/* iget p0, p0, Lcom/miui/server/smartpower/SmartDisplayPolicyManager$ComingInteraction;->mMinRefreshRate:I */
} // .end method
private com.miui.server.smartpower.SmartDisplayPolicyManager$ComingInteraction ( ) {
/* .locals 0 */
/* .param p1, "mMinRefreshRate" # I */
/* .param p2, "mMaxRefreshRate" # I */
/* .param p3, "mInteractType" # I */
/* .param p4, "mDuration" # J */
/* .line 1538 */
/* invoke-direct {p0}, Ljava/lang/Object;-><init>()V */
/* .line 1539 */
/* iput p1, p0, Lcom/miui/server/smartpower/SmartDisplayPolicyManager$ComingInteraction;->mMinRefreshRate:I */
/* .line 1540 */
/* iput p2, p0, Lcom/miui/server/smartpower/SmartDisplayPolicyManager$ComingInteraction;->mMaxRefreshRate:I */
/* .line 1541 */
/* iput p3, p0, Lcom/miui/server/smartpower/SmartDisplayPolicyManager$ComingInteraction;->mInteractType:I */
/* .line 1542 */
/* iput-wide p4, p0, Lcom/miui/server/smartpower/SmartDisplayPolicyManager$ComingInteraction;->mDuration:J */
/* .line 1543 */
return;
} // .end method
 com.miui.server.smartpower.SmartDisplayPolicyManager$ComingInteraction ( ) { //synthethic
/* .locals 0 */
/* invoke-direct/range {p0 ..p5}, Lcom/miui/server/smartpower/SmartDisplayPolicyManager$ComingInteraction;-><init>(IIIJ)V */
return;
} // .end method
