class com.miui.server.smartpower.SmartScenarioManager$ClientConfig$1 implements java.lang.Runnable {
	 /* .source "SmartScenarioManager.java" */
	 /* # interfaces */
	 /* # annotations */
	 /* .annotation system Ldalvik/annotation/EnclosingMethod; */
	 /* value = Lcom/miui/server/smartpower/SmartScenarioManager$ClientConfig;->reportSenarioChanged(JJ)V */
} // .end annotation
/* .annotation system Ldalvik/annotation/InnerClass; */
/* accessFlags = 0x0 */
/* name = null */
} // .end annotation
/* # instance fields */
final com.miui.server.smartpower.SmartScenarioManager$ClientConfig this$1; //synthetic
final Long val$additionalScenario; //synthetic
final Long val$mainScenario; //synthetic
/* # direct methods */
 com.miui.server.smartpower.SmartScenarioManager$ClientConfig$1 ( ) {
/* .locals 0 */
/* .param p1, "this$1" # Lcom/miui/server/smartpower/SmartScenarioManager$ClientConfig; */
/* .annotation system Ldalvik/annotation/Signature; */
/* value = { */
/* "()V" */
/* } */
} // .end annotation
/* .line 394 */
this.this$1 = p1;
/* iput-wide p2, p0, Lcom/miui/server/smartpower/SmartScenarioManager$ClientConfig$1;->val$mainScenario:J */
/* iput-wide p4, p0, Lcom/miui/server/smartpower/SmartScenarioManager$ClientConfig$1;->val$additionalScenario:J */
/* invoke-direct {p0}, Ljava/lang/Object;-><init>()V */
return;
} // .end method
/* # virtual methods */
public void run ( ) {
/* .locals 5 */
/* .line 397 */
v0 = this.this$1;
com.miui.server.smartpower.SmartScenarioManager$ClientConfig .-$$Nest$fgetmMainSenarioDescription ( v0 );
/* move-result-wide v0 */
/* iget-wide v2, p0, Lcom/miui/server/smartpower/SmartScenarioManager$ClientConfig$1;->val$mainScenario:J */
/* cmp-long v0, v0, v2 */
/* if-nez v0, :cond_1 */
v0 = this.this$1;
com.miui.server.smartpower.SmartScenarioManager$ClientConfig .-$$Nest$fgetmAdditionalScenarioDescription ( v0 );
/* move-result-wide v0 */
/* iget-wide v2, p0, Lcom/miui/server/smartpower/SmartScenarioManager$ClientConfig$1;->val$additionalScenario:J */
/* cmp-long v0, v0, v2 */
if ( v0 != null) { // if-eqz v0, :cond_0
/* .line 401 */
} // :cond_0
v0 = this.this$1;
com.miui.server.smartpower.SmartScenarioManager$ClientConfig .-$$Nest$fgetmCallback ( v0 );
v1 = this.this$1;
com.miui.server.smartpower.SmartScenarioManager$ClientConfig .-$$Nest$fgetmMainSenarioDescription ( v1 );
/* move-result-wide v1 */
v3 = this.this$1;
com.miui.server.smartpower.SmartScenarioManager$ClientConfig .-$$Nest$fgetmAdditionalScenarioDescription ( v3 );
/* move-result-wide v3 */
/* .line 403 */
return;
/* .line 399 */
} // :cond_1
} // :goto_0
return;
} // .end method
