.class Lcom/miui/server/smartpower/SmartDisplayPolicyManager$GesturesEventListenerCallback;
.super Ljava/lang/Object;
.source "SmartDisplayPolicyManager.java"

# interfaces
.implements Lcom/android/server/wm/SchedBoostGesturesEvent$GesturesEventListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/miui/server/smartpower/SmartDisplayPolicyManager;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "GesturesEventListenerCallback"
.end annotation


# instance fields
.field final synthetic this$0:Lcom/miui/server/smartpower/SmartDisplayPolicyManager;


# direct methods
.method private constructor <init>(Lcom/miui/server/smartpower/SmartDisplayPolicyManager;)V
    .locals 0

    .line 293
    iput-object p1, p0, Lcom/miui/server/smartpower/SmartDisplayPolicyManager$GesturesEventListenerCallback;->this$0:Lcom/miui/server/smartpower/SmartDisplayPolicyManager;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method synthetic constructor <init>(Lcom/miui/server/smartpower/SmartDisplayPolicyManager;Lcom/miui/server/smartpower/SmartDisplayPolicyManager$GesturesEventListenerCallback-IA;)V
    .locals 0

    invoke-direct {p0, p1}, Lcom/miui/server/smartpower/SmartDisplayPolicyManager$GesturesEventListenerCallback;-><init>(Lcom/miui/server/smartpower/SmartDisplayPolicyManager;)V

    return-void
.end method

.method private isVerticalFling(II)Z
    .locals 2
    .param p1, "velocityX"    # I
    .param p2, "velocityY"    # I

    .line 319
    invoke-static {p2}, Ljava/lang/Math;->abs(I)I

    move-result v0

    invoke-static {p1}, Ljava/lang/Math;->abs(I)I

    move-result v1

    sub-int/2addr v0, v1

    if-lez v0, :cond_0

    const/4 v0, 0x1

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    :goto_0
    return v0
.end method


# virtual methods
.method public onDown()V
    .locals 2

    .line 310
    iget-object v0, p0, Lcom/miui/server/smartpower/SmartDisplayPolicyManager$GesturesEventListenerCallback;->this$0:Lcom/miui/server/smartpower/SmartDisplayPolicyManager;

    const/16 v1, 0x8

    invoke-static {v0, v1}, Lcom/miui/server/smartpower/SmartDisplayPolicyManager;->-$$Nest$mnotifyInputEventReceived(Lcom/miui/server/smartpower/SmartDisplayPolicyManager;I)V

    .line 311
    return-void
.end method

.method public onFling(FFI)V
    .locals 4
    .param p1, "velocityX"    # F
    .param p2, "velocityY"    # F
    .param p3, "durationMs"    # I

    .line 298
    float-to-int v0, p1

    float-to-int v1, p2

    invoke-direct {p0, v0, v1}, Lcom/miui/server/smartpower/SmartDisplayPolicyManager$GesturesEventListenerCallback;->isVerticalFling(II)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 299
    iget-object v0, p0, Lcom/miui/server/smartpower/SmartDisplayPolicyManager$GesturesEventListenerCallback;->this$0:Lcom/miui/server/smartpower/SmartDisplayPolicyManager;

    .line 300
    const/16 v1, 0xbb8

    invoke-static {p3, v1}, Ljava/lang/Math;->min(II)I

    move-result v1

    int-to-long v1, v1

    .line 299
    const/16 v3, 0xa

    invoke-static {v0, v3, v1, v2}, Lcom/miui/server/smartpower/SmartDisplayPolicyManager;->-$$Nest$mnotifyInputEventReceived(Lcom/miui/server/smartpower/SmartDisplayPolicyManager;IJ)V

    .line 302
    :cond_0
    return-void
.end method

.method public onMove()V
    .locals 2

    .line 315
    iget-object v0, p0, Lcom/miui/server/smartpower/SmartDisplayPolicyManager$GesturesEventListenerCallback;->this$0:Lcom/miui/server/smartpower/SmartDisplayPolicyManager;

    const/16 v1, 0x9

    invoke-static {v0, v1}, Lcom/miui/server/smartpower/SmartDisplayPolicyManager;->-$$Nest$mnotifyInputEventReceived(Lcom/miui/server/smartpower/SmartDisplayPolicyManager;I)V

    .line 316
    return-void
.end method

.method public onScroll(Z)V
    .locals 0
    .param p1, "started"    # Z

    .line 306
    return-void
.end method
