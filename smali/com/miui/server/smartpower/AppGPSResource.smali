.class Lcom/miui/server/smartpower/AppGPSResource;
.super Lcom/miui/server/smartpower/AppPowerResource;
.source "AppGPSResource.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/miui/server/smartpower/AppGPSResource$GpsRecord;
    }
.end annotation


# instance fields
.field private final mGpsRecordMap:Landroid/util/ArrayMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/util/ArrayMap<",
            "Ljava/lang/Integer;",
            "Lcom/miui/server/smartpower/AppGPSResource$GpsRecord;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Landroid/content/Context;Landroid/os/Looper;)V
    .locals 1
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "looper"    # Landroid/os/Looper;

    .line 18
    invoke-direct {p0}, Lcom/miui/server/smartpower/AppPowerResource;-><init>()V

    .line 16
    new-instance v0, Landroid/util/ArrayMap;

    invoke-direct {v0}, Landroid/util/ArrayMap;-><init>()V

    iput-object v0, p0, Lcom/miui/server/smartpower/AppGPSResource;->mGpsRecordMap:Landroid/util/ArrayMap;

    .line 19
    const/4 v0, 0x3

    iput v0, p0, Lcom/miui/server/smartpower/AppGPSResource;->mType:I

    .line 20
    return-void
.end method


# virtual methods
.method public getActiveUids()Ljava/util/ArrayList;
    .locals 3

    .line 24
    iget-object v0, p0, Lcom/miui/server/smartpower/AppGPSResource;->mGpsRecordMap:Landroid/util/ArrayMap;

    monitor-enter v0

    .line 25
    :try_start_0
    new-instance v1, Ljava/util/ArrayList;

    iget-object v2, p0, Lcom/miui/server/smartpower/AppGPSResource;->mGpsRecordMap:Landroid/util/ArrayMap;

    invoke-virtual {v2}, Landroid/util/ArrayMap;->keySet()Ljava/util/Set;

    move-result-object v2

    invoke-direct {v1, v2}, Ljava/util/ArrayList;-><init>(Ljava/util/Collection;)V

    monitor-exit v0

    return-object v1

    .line 26
    :catchall_0
    move-exception v1

    monitor-exit v0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v1
.end method

.method public isAppResourceActive(I)Z
    .locals 4
    .param p1, "uid"    # I

    .line 31
    iget-object v0, p0, Lcom/miui/server/smartpower/AppGPSResource;->mGpsRecordMap:Landroid/util/ArrayMap;

    monitor-enter v0

    .line 32
    const/4 v1, 0x0

    .local v1, "i":I
    :goto_0
    :try_start_0
    iget-object v2, p0, Lcom/miui/server/smartpower/AppGPSResource;->mGpsRecordMap:Landroid/util/ArrayMap;

    invoke-virtual {v2}, Landroid/util/ArrayMap;->size()I

    move-result v2

    if-ge v1, v2, :cond_1

    .line 33
    iget-object v2, p0, Lcom/miui/server/smartpower/AppGPSResource;->mGpsRecordMap:Landroid/util/ArrayMap;

    invoke-virtual {v2, v1}, Landroid/util/ArrayMap;->valueAt(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/miui/server/smartpower/AppGPSResource$GpsRecord;

    .line 34
    .local v2, "record":Lcom/miui/server/smartpower/AppGPSResource$GpsRecord;
    if-eqz v2, :cond_0

    invoke-static {v2}, Lcom/miui/server/smartpower/AppGPSResource$GpsRecord;->-$$Nest$fgetmUid(Lcom/miui/server/smartpower/AppGPSResource$GpsRecord;)I

    move-result v3

    if-ne v3, p1, :cond_0

    invoke-virtual {v2}, Lcom/miui/server/smartpower/AppGPSResource$GpsRecord;->isActive()Z

    move-result v3

    if-eqz v3, :cond_0

    .line 35
    monitor-exit v0

    const/4 v0, 0x1

    return v0

    .line 32
    .end local v2    # "record":Lcom/miui/server/smartpower/AppGPSResource$GpsRecord;
    :cond_0
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 38
    .end local v1    # "i":I
    :cond_1
    monitor-exit v0

    .line 39
    const/4 v0, 0x0

    return v0

    .line 38
    :catchall_0
    move-exception v1

    monitor-exit v0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v1
.end method

.method public isAppResourceActive(II)Z
    .locals 3
    .param p1, "uid"    # I
    .param p2, "pid"    # I

    .line 44
    iget-object v0, p0, Lcom/miui/server/smartpower/AppGPSResource;->mGpsRecordMap:Landroid/util/ArrayMap;

    monitor-enter v0

    .line 45
    :try_start_0
    iget-object v1, p0, Lcom/miui/server/smartpower/AppGPSResource;->mGpsRecordMap:Landroid/util/ArrayMap;

    invoke-static {p2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v1, v2}, Landroid/util/ArrayMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/miui/server/smartpower/AppGPSResource$GpsRecord;

    .line 46
    .local v1, "record":Lcom/miui/server/smartpower/AppGPSResource$GpsRecord;
    if-eqz v1, :cond_0

    .line 47
    invoke-virtual {v1}, Lcom/miui/server/smartpower/AppGPSResource$GpsRecord;->isActive()Z

    move-result v2

    monitor-exit v0

    return v2

    .line 49
    .end local v1    # "record":Lcom/miui/server/smartpower/AppGPSResource$GpsRecord;
    :cond_0
    monitor-exit v0

    .line 50
    const/4 v0, 0x0

    return v0

    .line 49
    :catchall_0
    move-exception v1

    monitor-exit v0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v1
.end method

.method public onAquireLocation(IILandroid/location/ILocationListener;)V
    .locals 4
    .param p1, "uid"    # I
    .param p2, "pid"    # I
    .param p3, "listener"    # Landroid/location/ILocationListener;

    .line 60
    if-nez p3, :cond_0

    return-void

    .line 61
    :cond_0
    iget-object v0, p0, Lcom/miui/server/smartpower/AppGPSResource;->mGpsRecordMap:Landroid/util/ArrayMap;

    monitor-enter v0

    .line 62
    :try_start_0
    iget-object v1, p0, Lcom/miui/server/smartpower/AppGPSResource;->mGpsRecordMap:Landroid/util/ArrayMap;

    invoke-static {p2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v1, v2}, Landroid/util/ArrayMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/miui/server/smartpower/AppGPSResource$GpsRecord;

    .line 63
    .local v1, "gpsRecord":Lcom/miui/server/smartpower/AppGPSResource$GpsRecord;
    if-nez v1, :cond_1

    .line 64
    new-instance v2, Lcom/miui/server/smartpower/AppGPSResource$GpsRecord;

    invoke-direct {v2, p0, p1, p2}, Lcom/miui/server/smartpower/AppGPSResource$GpsRecord;-><init>(Lcom/miui/server/smartpower/AppGPSResource;II)V

    move-object v1, v2

    .line 65
    iget-object v2, p0, Lcom/miui/server/smartpower/AppGPSResource;->mGpsRecordMap:Landroid/util/ArrayMap;

    invoke-static {p2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    invoke-virtual {v2, v3, v1}, Landroid/util/ArrayMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 66
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "gps u:"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, " p:"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, " s:true"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    const v3, 0x15ff2

    invoke-static {v3, v2}, Landroid/util/EventLog;->writeEvent(ILjava/lang/String;)I

    .line 68
    const/4 v2, 0x1

    const/16 v3, 0x40

    invoke-virtual {p0, p1, p2, v2, v3}, Lcom/miui/server/smartpower/AppGPSResource;->reportResourceStatus(IIZI)V

    .line 71
    :cond_1
    invoke-virtual {v1, p3}, Lcom/miui/server/smartpower/AppGPSResource$GpsRecord;->onAquireLocation(Landroid/location/ILocationListener;)V

    .line 72
    .end local v1    # "gpsRecord":Lcom/miui/server/smartpower/AppGPSResource$GpsRecord;
    monitor-exit v0

    .line 73
    return-void

    .line 72
    :catchall_0
    move-exception v1

    monitor-exit v0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v1
.end method

.method public onReleaseLocation(IILandroid/location/ILocationListener;)V
    .locals 4
    .param p1, "uid"    # I
    .param p2, "pid"    # I
    .param p3, "listener"    # Landroid/location/ILocationListener;

    .line 76
    if-nez p3, :cond_0

    return-void

    .line 77
    :cond_0
    iget-object v0, p0, Lcom/miui/server/smartpower/AppGPSResource;->mGpsRecordMap:Landroid/util/ArrayMap;

    monitor-enter v0

    .line 78
    :try_start_0
    iget-object v1, p0, Lcom/miui/server/smartpower/AppGPSResource;->mGpsRecordMap:Landroid/util/ArrayMap;

    invoke-static {p2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v1, v2}, Landroid/util/ArrayMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/miui/server/smartpower/AppGPSResource$GpsRecord;

    .line 79
    .local v1, "gpsRecord":Lcom/miui/server/smartpower/AppGPSResource$GpsRecord;
    if-nez v1, :cond_1

    monitor-exit v0

    return-void

    .line 80
    :cond_1
    invoke-virtual {v1, p3}, Lcom/miui/server/smartpower/AppGPSResource$GpsRecord;->onReleaseLocation(Landroid/location/ILocationListener;)V

    .line 81
    invoke-virtual {v1}, Lcom/miui/server/smartpower/AppGPSResource$GpsRecord;->isActive()Z

    move-result v2

    if-nez v2, :cond_2

    .line 82
    iget-object v2, p0, Lcom/miui/server/smartpower/AppGPSResource;->mGpsRecordMap:Landroid/util/ArrayMap;

    invoke-static {p2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    invoke-virtual {v2, v3}, Landroid/util/ArrayMap;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    .line 83
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "gps u:"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, " p:"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, " s:false"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    const v3, 0x15ff2

    invoke-static {v3, v2}, Landroid/util/EventLog;->writeEvent(ILjava/lang/String;)I

    .line 85
    const/4 v2, 0x0

    const/16 v3, 0x40

    invoke-virtual {p0, p1, p2, v2, v3}, Lcom/miui/server/smartpower/AppGPSResource;->reportResourceStatus(IIZI)V

    .line 88
    .end local v1    # "gpsRecord":Lcom/miui/server/smartpower/AppGPSResource$GpsRecord;
    :cond_2
    monitor-exit v0

    .line 89
    return-void

    .line 88
    :catchall_0
    move-exception v1

    monitor-exit v0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v1
.end method

.method public releaseAppPowerResource(I)V
    .locals 0
    .param p1, "pid"    # I

    .line 54
    return-void
.end method

.method public resumeAppPowerResource(I)V
    .locals 0
    .param p1, "pid"    # I

    .line 57
    return-void
.end method
