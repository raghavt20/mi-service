.class public interface abstract Lcom/miui/server/smartpower/PowerFrozenManager$IFrozenReportCallback;
.super Ljava/lang/Object;
.source "PowerFrozenManager.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/miui/server/smartpower/PowerFrozenManager;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x609
    name = "IFrozenReportCallback"
.end annotation


# virtual methods
.method public abstract reportBinderState(IIIIJ)V
.end method

.method public abstract reportBinderTrans(IIIIIZJJ)V
.end method

.method public abstract reportNet(IJ)V
.end method

.method public abstract reportSignal(IIJ)V
.end method

.method public abstract serviceReady(Z)V
.end method

.method public abstract thawedByOther(II)V
.end method
