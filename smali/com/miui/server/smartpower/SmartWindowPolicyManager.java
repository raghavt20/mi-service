public class com.miui.server.smartpower.SmartWindowPolicyManager {
	 /* .source "SmartWindowPolicyManager.java" */
	 /* # annotations */
	 /* .annotation system Ldalvik/annotation/MemberClasses; */
	 /* value = { */
	 /* Lcom/miui/server/smartpower/SmartWindowPolicyManager$MultiTaskPolicyController;, */
	 /* Lcom/miui/server/smartpower/SmartWindowPolicyManager$H; */
	 /* } */
} // .end annotation
/* # static fields */
public static final Boolean DEBUG;
private static final Integer EVENT_MULTI_TASK_ACTION_END;
private static final Integer EVENT_MULTI_TASK_ACTION_START;
private static final Integer MSG_MULTI_TASK_ACTION_CHANGED;
public static final java.lang.String TAG;
public static Boolean sEnable;
/* # instance fields */
private final android.content.Context mContext;
private final com.miui.server.smartpower.SmartWindowPolicyManager$H mHandler;
private com.miui.server.smartpower.SmartWindowPolicyManager$MultiTaskPolicyController mMultiTaskPolicyController;
private com.miui.server.smartpower.SmartBoostPolicyManager mSmartBoostPolicyManager;
private com.miui.server.smartpower.SmartDisplayPolicyManager mSmartDisplayPolicyManager;
private final com.android.server.wm.WindowManagerService mWMS;
/* # direct methods */
static com.miui.server.smartpower.SmartBoostPolicyManager -$$Nest$fgetmSmartBoostPolicyManager ( com.miui.server.smartpower.SmartWindowPolicyManager p0 ) { //bridge//synthethic
	 /* .locals 0 */
	 p0 = this.mSmartBoostPolicyManager;
} // .end method
static com.miui.server.smartpower.SmartDisplayPolicyManager -$$Nest$fgetmSmartDisplayPolicyManager ( com.miui.server.smartpower.SmartWindowPolicyManager p0 ) { //bridge//synthethic
	 /* .locals 0 */
	 p0 = this.mSmartDisplayPolicyManager;
} // .end method
static void -$$Nest$mnotifyMultiTaskActionChanged ( com.miui.server.smartpower.SmartWindowPolicyManager p0, Integer p1, miui.smartpower.MultiTaskActionManager$ActionInfo p2 ) { //bridge//synthethic
	 /* .locals 0 */
	 /* invoke-direct {p0, p1, p2}, Lcom/miui/server/smartpower/SmartWindowPolicyManager;->notifyMultiTaskActionChanged(ILmiui/smartpower/MultiTaskActionManager$ActionInfo;)V */
	 return;
} // .end method
static com.miui.server.smartpower.SmartWindowPolicyManager ( ) {
	 /* .locals 1 */
	 /* .line 24 */
	 /* sget-boolean v0, Lcom/android/server/am/SmartPowerService;->DEBUG:Z */
	 com.miui.server.smartpower.SmartWindowPolicyManager.DEBUG = (v0!= 0);
	 /* .line 26 */
	 /* sget-boolean v0, Lcom/miui/app/smartpower/SmartPowerSettings;->WINDOW_POLICY_ENABLE:Z */
	 com.miui.server.smartpower.SmartWindowPolicyManager.sEnable = (v0!= 0);
	 return;
} // .end method
public com.miui.server.smartpower.SmartWindowPolicyManager ( ) {
	 /* .locals 2 */
	 /* .param p1, "context" # Landroid/content/Context; */
	 /* .param p2, "looper" # Landroid/os/Looper; */
	 /* .param p3, "wms" # Lcom/android/server/wm/WindowManagerService; */
	 /* .line 41 */
	 /* invoke-direct {p0}, Ljava/lang/Object;-><init>()V */
	 /* .line 37 */
	 int v0 = 0; // const/4 v0, 0x0
	 this.mMultiTaskPolicyController = v0;
	 /* .line 38 */
	 this.mSmartDisplayPolicyManager = v0;
	 /* .line 39 */
	 this.mSmartBoostPolicyManager = v0;
	 /* .line 42 */
	 this.mContext = p1;
	 /* .line 43 */
	 /* new-instance v1, Lcom/miui/server/smartpower/SmartWindowPolicyManager$H; */
	 /* invoke-direct {v1, p0, p2}, Lcom/miui/server/smartpower/SmartWindowPolicyManager$H;-><init>(Lcom/miui/server/smartpower/SmartWindowPolicyManager;Landroid/os/Looper;)V */
	 this.mHandler = v1;
	 /* .line 44 */
	 this.mWMS = p3;
	 /* .line 46 */
	 /* new-instance v1, Lcom/miui/server/smartpower/SmartWindowPolicyManager$MultiTaskPolicyController; */
	 /* invoke-direct {v1, p0, v0}, Lcom/miui/server/smartpower/SmartWindowPolicyManager$MultiTaskPolicyController;-><init>(Lcom/miui/server/smartpower/SmartWindowPolicyManager;Lcom/miui/server/smartpower/SmartWindowPolicyManager$MultiTaskPolicyController-IA;)V */
	 this.mMultiTaskPolicyController = v1;
	 /* .line 47 */
	 return;
} // .end method
private Boolean isEnableWindowPolicy ( ) {
	 /* .locals 1 */
	 /* .line 138 */
	 /* sget-boolean v0, Lcom/miui/server/smartpower/SmartWindowPolicyManager;->sEnable:Z */
} // .end method
private void notifyMultiTaskActionChanged ( Integer p0, miui.smartpower.MultiTaskActionManager$ActionInfo p1 ) {
	 /* .locals 1 */
	 /* .param p1, "event" # I */
	 /* .param p2, "actionInfo" # Lmiui/smartpower/MultiTaskActionManager$ActionInfo; */
	 /* .line 134 */
	 v0 = this.mMultiTaskPolicyController;
	 (( com.miui.server.smartpower.SmartWindowPolicyManager$MultiTaskPolicyController ) v0 ).notifyMultiTaskActionChanged ( p1, p2 ); // invoke-virtual {v0, p1, p2}, Lcom/miui/server/smartpower/SmartWindowPolicyManager$MultiTaskPolicyController;->notifyMultiTaskActionChanged(ILmiui/smartpower/MultiTaskActionManager$ActionInfo;)V
	 /* .line 135 */
	 return;
} // .end method
private void onMultiTaskActionChanged ( Integer p0, miui.smartpower.MultiTaskActionManager$ActionInfo p1 ) {
	 /* .locals 2 */
	 /* .param p1, "event" # I */
	 /* .param p2, "actionInfo" # Lmiui/smartpower/MultiTaskActionManager$ActionInfo; */
	 /* .line 127 */
	 v0 = this.mHandler;
	 int v1 = 1; // const/4 v1, 0x1
	 (( com.miui.server.smartpower.SmartWindowPolicyManager$H ) v0 ).obtainMessage ( v1 ); // invoke-virtual {v0, v1}, Lcom/miui/server/smartpower/SmartWindowPolicyManager$H;->obtainMessage(I)Landroid/os/Message;
	 /* .line 128 */
	 /* .local v0, "message":Landroid/os/Message; */
	 /* iput p1, v0, Landroid/os/Message;->arg1:I */
	 /* .line 129 */
	 this.obj = p2;
	 /* .line 130 */
	 v1 = this.mHandler;
	 (( com.miui.server.smartpower.SmartWindowPolicyManager$H ) v1 ).sendMessage ( v0 ); // invoke-virtual {v1, v0}, Lcom/miui/server/smartpower/SmartWindowPolicyManager$H;->sendMessage(Landroid/os/Message;)Z
	 /* .line 131 */
	 return;
} // .end method
/* # virtual methods */
public void dump ( java.io.PrintWriter p0, java.lang.String[] p1, Integer p2 ) {
	 /* .locals 2 */
	 /* .param p1, "pw" # Ljava/io/PrintWriter; */
	 /* .param p2, "args" # [Ljava/lang/String; */
	 /* .param p3, "opti" # I */
	 /* .line 161 */
	 final String v0 = "SmartWindowPolicyManager Dump:"; // const-string v0, "SmartWindowPolicyManager Dump:"
	 (( java.io.PrintWriter ) p1 ).println ( v0 ); // invoke-virtual {p1, v0}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V
	 /* .line 162 */
	 /* new-instance v0, Ljava/lang/StringBuilder; */
	 /* invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V */
	 final String v1 = "Enable: "; // const-string v1, "Enable: "
	 (( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
	 v1 = 	 /* invoke-direct {p0}, Lcom/miui/server/smartpower/SmartWindowPolicyManager;->isEnableWindowPolicy()Z */
	 (( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;
	 (( java.lang.StringBuilder ) v0 ).toString ( ); // invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
	 (( java.io.PrintWriter ) p1 ).println ( v0 ); // invoke-virtual {p1, v0}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V
	 /* .line 164 */
	 v0 = this.mMultiTaskPolicyController;
	 if ( v0 != null) { // if-eqz v0, :cond_0
		 /* .line 165 */
		 (( com.miui.server.smartpower.SmartWindowPolicyManager$MultiTaskPolicyController ) v0 ).dump ( p1 ); // invoke-virtual {v0, p1}, Lcom/miui/server/smartpower/SmartWindowPolicyManager$MultiTaskPolicyController;->dump(Ljava/io/PrintWriter;)V
		 /* .line 167 */
	 } // :cond_0
	 return;
} // .end method
public void init ( com.miui.server.smartpower.SmartDisplayPolicyManager p0, com.miui.server.smartpower.SmartBoostPolicyManager p1 ) {
	 /* .locals 0 */
	 /* .param p1, "smartDisplayPolicyManager" # Lcom/miui/server/smartpower/SmartDisplayPolicyManager; */
	 /* .param p2, "smartBoostPolicyManager" # Lcom/miui/server/smartpower/SmartBoostPolicyManager; */
	 /* .line 51 */
	 this.mSmartDisplayPolicyManager = p1;
	 /* .line 52 */
	 this.mSmartBoostPolicyManager = p2;
	 /* .line 53 */
	 return;
} // .end method
public void onMultiTaskActionEnd ( miui.smartpower.MultiTaskActionManager$ActionInfo p0, Integer p1 ) {
	 /* .locals 5 */
	 /* .param p1, "actionInfo" # Lmiui/smartpower/MultiTaskActionManager$ActionInfo; */
	 /* .param p2, "callingPid" # I */
	 /* .line 105 */
	 v0 = 	 /* invoke-direct {p0}, Lcom/miui/server/smartpower/SmartWindowPolicyManager;->isEnableWindowPolicy()Z */
	 /* if-nez v0, :cond_0 */
	 /* .line 106 */
	 return;
	 /* .line 109 */
} // :cond_0
final String v0 = "SmartPower.WindowPolicy"; // const-string v0, "SmartPower.WindowPolicy"
/* if-nez p1, :cond_1 */
/* .line 110 */
final String v1 = "action info must not be null"; // const-string v1, "action info must not be null"
android.util.Slog .w ( v0,v1 );
/* .line 111 */
return;
/* .line 114 */
} // :cond_1
/* sget-boolean v1, Lcom/miui/server/smartpower/SmartWindowPolicyManager;->DEBUG:Z */
if ( v1 != null) { // if-eqz v1, :cond_2
/* .line 115 */
/* new-instance v1, Ljava/lang/StringBuilder; */
/* invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V */
final String v2 = "onMultiTaskActionEnd: "; // const-string v2, "onMultiTaskActionEnd: "
(( java.lang.StringBuilder ) v1 ).append ( v2 ); // invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v1 ).append ( p1 ); // invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v1 ).toString ( ); // invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
android.util.Slog .d ( v0,v1 );
/* .line 118 */
} // :cond_2
v0 = (( miui.smartpower.MultiTaskActionManager$ActionInfo ) p1 ).getType ( ); // invoke-virtual {p1}, Lmiui/smartpower/MultiTaskActionManager$ActionInfo;->getType()I
miui.smartpower.MultiTaskActionManager .actionTypeToString ( v0 );
/* .line 119 */
/* .local v0, "typeString":Ljava/lang/String; */
/* new-instance v1, Ljava/lang/StringBuilder; */
/* invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V */
final String v2 = "Multi task "; // const-string v2, "Multi task "
(( java.lang.StringBuilder ) v1 ).append ( v2 ); // invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v1 ).append ( v0 ); // invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v1 ).toString ( ); // invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
int v2 = 0; // const/4 v2, 0x0
/* const-wide/16 v3, 0x40 */
android.os.Trace .asyncTraceEnd ( v3,v4,v1,v2 );
/* .line 122 */
(( miui.smartpower.MultiTaskActionManager$ActionInfo ) p1 ).setCallingPid ( p2 ); // invoke-virtual {p1, p2}, Lmiui/smartpower/MultiTaskActionManager$ActionInfo;->setCallingPid(I)V
/* .line 123 */
int v1 = 2; // const/4 v1, 0x2
/* invoke-direct {p0, v1, p1}, Lcom/miui/server/smartpower/SmartWindowPolicyManager;->onMultiTaskActionChanged(ILmiui/smartpower/MultiTaskActionManager$ActionInfo;)V */
/* .line 124 */
return;
} // .end method
public void onMultiTaskActionStart ( miui.smartpower.MultiTaskActionManager$ActionInfo p0, Integer p1 ) {
/* .locals 5 */
/* .param p1, "actionInfo" # Lmiui/smartpower/MultiTaskActionManager$ActionInfo; */
/* .param p2, "callingPid" # I */
/* .line 83 */
v0 = /* invoke-direct {p0}, Lcom/miui/server/smartpower/SmartWindowPolicyManager;->isEnableWindowPolicy()Z */
/* if-nez v0, :cond_0 */
/* .line 84 */
return;
/* .line 87 */
} // :cond_0
final String v0 = "SmartPower.WindowPolicy"; // const-string v0, "SmartPower.WindowPolicy"
/* if-nez p1, :cond_1 */
/* .line 88 */
final String v1 = "action info must not be null"; // const-string v1, "action info must not be null"
android.util.Slog .w ( v0,v1 );
/* .line 89 */
return;
/* .line 92 */
} // :cond_1
/* sget-boolean v1, Lcom/miui/server/smartpower/SmartWindowPolicyManager;->DEBUG:Z */
if ( v1 != null) { // if-eqz v1, :cond_2
/* .line 93 */
/* new-instance v1, Ljava/lang/StringBuilder; */
/* invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V */
final String v2 = "onMultiTaskActionStart: "; // const-string v2, "onMultiTaskActionStart: "
(( java.lang.StringBuilder ) v1 ).append ( v2 ); // invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v1 ).append ( p1 ); // invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v1 ).toString ( ); // invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
android.util.Slog .d ( v0,v1 );
/* .line 96 */
} // :cond_2
v0 = (( miui.smartpower.MultiTaskActionManager$ActionInfo ) p1 ).getType ( ); // invoke-virtual {p1}, Lmiui/smartpower/MultiTaskActionManager$ActionInfo;->getType()I
miui.smartpower.MultiTaskActionManager .actionTypeToString ( v0 );
/* .line 97 */
/* .local v0, "typeString":Ljava/lang/String; */
/* new-instance v1, Ljava/lang/StringBuilder; */
/* invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V */
final String v2 = "Multi task "; // const-string v2, "Multi task "
(( java.lang.StringBuilder ) v1 ).append ( v2 ); // invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v1 ).append ( v0 ); // invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v1 ).toString ( ); // invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
int v2 = 0; // const/4 v2, 0x0
/* const-wide/16 v3, 0x20 */
android.os.Trace .asyncTraceBegin ( v3,v4,v1,v2 );
/* .line 100 */
(( miui.smartpower.MultiTaskActionManager$ActionInfo ) p1 ).setCallingPid ( p2 ); // invoke-virtual {p1, p2}, Lmiui/smartpower/MultiTaskActionManager$ActionInfo;->setCallingPid(I)V
/* .line 101 */
int v1 = 1; // const/4 v1, 0x1
/* invoke-direct {p0, v1, p1}, Lcom/miui/server/smartpower/SmartWindowPolicyManager;->onMultiTaskActionChanged(ILmiui/smartpower/MultiTaskActionManager$ActionInfo;)V */
/* .line 102 */
return;
} // .end method
public Boolean registerMultiTaskActionListener ( Integer p0, android.os.Handler p1, com.miui.app.smartpower.SmartPowerServiceInternal$MultiTaskActionListener p2 ) {
/* .locals 1 */
/* .param p1, "listenerFlag" # I */
/* .param p2, "handler" # Landroid/os/Handler; */
/* .param p3, "listener" # Lcom/miui/app/smartpower/SmartPowerServiceInternal$MultiTaskActionListener; */
/* .line 57 */
v0 = /* invoke-direct {p0}, Lcom/miui/server/smartpower/SmartWindowPolicyManager;->isEnableWindowPolicy()Z */
/* if-nez v0, :cond_0 */
/* .line 58 */
int v0 = 0; // const/4 v0, 0x0
/* .line 61 */
} // :cond_0
v0 = this.mMultiTaskPolicyController;
v0 = (( com.miui.server.smartpower.SmartWindowPolicyManager$MultiTaskPolicyController ) v0 ).registerMultiTaskActionListener ( p1, p2, p3 ); // invoke-virtual {v0, p1, p2, p3}, Lcom/miui/server/smartpower/SmartWindowPolicyManager$MultiTaskPolicyController;->registerMultiTaskActionListener(ILandroid/os/Handler;Lcom/miui/app/smartpower/SmartPowerServiceInternal$MultiTaskActionListener;)Z
} // .end method
public Boolean unregisterMultiTaskActionListener ( Integer p0, com.miui.app.smartpower.SmartPowerServiceInternal$MultiTaskActionListener p1 ) {
/* .locals 1 */
/* .param p1, "listenerFlag" # I */
/* .param p2, "listener" # Lcom/miui/app/smartpower/SmartPowerServiceInternal$MultiTaskActionListener; */
/* .line 67 */
v0 = /* invoke-direct {p0}, Lcom/miui/server/smartpower/SmartWindowPolicyManager;->isEnableWindowPolicy()Z */
/* if-nez v0, :cond_0 */
/* .line 68 */
int v0 = 0; // const/4 v0, 0x0
/* .line 71 */
} // :cond_0
v0 = this.mMultiTaskPolicyController;
v0 = (( com.miui.server.smartpower.SmartWindowPolicyManager$MultiTaskPolicyController ) v0 ).unregisterMultiTaskActionListener ( p1, p2 ); // invoke-virtual {v0, p1, p2}, Lcom/miui/server/smartpower/SmartWindowPolicyManager$MultiTaskPolicyController;->unregisterMultiTaskActionListener(ILcom/miui/app/smartpower/SmartPowerServiceInternal$MultiTaskActionListener;)Z
} // .end method
