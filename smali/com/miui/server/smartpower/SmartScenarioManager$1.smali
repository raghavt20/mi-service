.class Lcom/miui/server/smartpower/SmartScenarioManager$1;
.super Landroid/content/BroadcastReceiver;
.source "SmartScenarioManager.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/miui/server/smartpower/SmartScenarioManager;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/miui/server/smartpower/SmartScenarioManager;


# direct methods
.method constructor <init>(Lcom/miui/server/smartpower/SmartScenarioManager;)V
    .locals 0
    .param p1, "this$0"    # Lcom/miui/server/smartpower/SmartScenarioManager;

    .line 161
    iput-object p1, p0, Lcom/miui/server/smartpower/SmartScenarioManager$1;->this$0:Lcom/miui/server/smartpower/SmartScenarioManager;

    invoke-direct {p0}, Landroid/content/BroadcastReceiver;-><init>()V

    return-void
.end method


# virtual methods
.method public onReceive(Landroid/content/Context;Landroid/content/Intent;)V
    .locals 7
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "intent"    # Landroid/content/Intent;

    .line 164
    invoke-virtual {p2}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v0

    const-string v1, "android.intent.action.BATTERY_CHANGED"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 165
    const-string v0, "present"

    const/4 v1, 0x1

    invoke-virtual {p2, v0, v1}, Landroid/content/Intent;->getBooleanExtra(Ljava/lang/String;Z)Z

    move-result v0

    .line 166
    .local v0, "present":Z
    const-string v2, "plugged"

    const/4 v3, 0x0

    invoke-virtual {p2, v2, v3}, Landroid/content/Intent;->getIntExtra(Ljava/lang/String;I)I

    move-result v2

    if-eqz v2, :cond_0

    move v2, v1

    goto :goto_0

    :cond_0
    move v2, v3

    .line 167
    .local v2, "plugged":Z
    :goto_0
    if-eqz v0, :cond_1

    if-eqz v2, :cond_1

    goto :goto_1

    :cond_1
    move v1, v3

    .line 168
    .local v1, "charging":Z
    :goto_1
    iget-object v3, p0, Lcom/miui/server/smartpower/SmartScenarioManager$1;->this$0:Lcom/miui/server/smartpower/SmartScenarioManager;

    invoke-static {v3}, Lcom/miui/server/smartpower/SmartScenarioManager;->-$$Nest$fgetmCharging(Lcom/miui/server/smartpower/SmartScenarioManager;)Z

    move-result v3

    if-eq v1, v3, :cond_2

    .line 169
    iget-object v3, p0, Lcom/miui/server/smartpower/SmartScenarioManager$1;->this$0:Lcom/miui/server/smartpower/SmartScenarioManager;

    invoke-static {v3, v1}, Lcom/miui/server/smartpower/SmartScenarioManager;->-$$Nest$fputmCharging(Lcom/miui/server/smartpower/SmartScenarioManager;Z)V

    .line 170
    iget-object v3, p0, Lcom/miui/server/smartpower/SmartScenarioManager$1;->this$0:Lcom/miui/server/smartpower/SmartScenarioManager;

    const/4 v4, 0x0

    invoke-static {v3}, Lcom/miui/server/smartpower/SmartScenarioManager;->-$$Nest$fgetmCharging(Lcom/miui/server/smartpower/SmartScenarioManager;)Z

    move-result v5

    const/16 v6, 0x800

    invoke-virtual {v3, v6, v4, v5}, Lcom/miui/server/smartpower/SmartScenarioManager;->onAppActionChanged(ILcom/android/server/am/AppStateManager$AppState;Z)V

    .line 173
    .end local v0    # "present":Z
    .end local v1    # "charging":Z
    .end local v2    # "plugged":Z
    :cond_2
    return-void
.end method
