class com.miui.server.smartpower.SmartPowerPolicyManager$2 implements java.lang.Runnable {
	 /* .source "SmartPowerPolicyManager.java" */
	 /* # interfaces */
	 /* # annotations */
	 /* .annotation system Ldalvik/annotation/EnclosingMethod; */
	 /* value = Lcom/miui/server/smartpower/SmartPowerPolicyManager;->hibernationProcess(Lcom/android/server/am/AppStateManager$AppState$RunningProcess;Ljava/lang/String;)V */
} // .end annotation
/* .annotation system Ldalvik/annotation/InnerClass; */
/* accessFlags = 0x0 */
/* name = null */
} // .end annotation
/* # instance fields */
final com.miui.server.smartpower.SmartPowerPolicyManager this$0; //synthetic
final com.android.server.am.AppStateManager$AppState$RunningProcess val$proc; //synthetic
final java.lang.String val$reason; //synthetic
/* # direct methods */
 com.miui.server.smartpower.SmartPowerPolicyManager$2 ( ) {
/* .locals 0 */
/* .param p1, "this$0" # Lcom/miui/server/smartpower/SmartPowerPolicyManager; */
/* .annotation system Ldalvik/annotation/Signature; */
/* value = { */
/* "()V" */
/* } */
} // .end annotation
/* .line 846 */
this.this$0 = p1;
this.val$proc = p2;
this.val$reason = p3;
/* invoke-direct {p0}, Ljava/lang/Object;-><init>()V */
return;
} // .end method
/* # virtual methods */
public void run ( ) {
/* .locals 7 */
/* .line 849 */
final String v0 = "hibernationProcess"; // const-string v0, "hibernationProcess"
/* const-wide/32 v1, 0x20000 */
android.os.Trace .traceBegin ( v1,v2,v0 );
/* .line 850 */
v0 = this.val$proc;
(( com.android.server.am.AppStateManager$AppState$RunningProcess ) v0 ).updatePss ( ); // invoke-virtual {v0}, Lcom/android/server/am/AppStateManager$AppState$RunningProcess;->updatePss()V
/* .line 851 */
v0 = this.this$0;
v3 = this.val$proc;
v3 = (( com.android.server.am.AppStateManager$AppState$RunningProcess ) v3 ).getUid ( ); // invoke-virtual {v3}, Lcom/android/server/am/AppStateManager$AppState$RunningProcess;->getUid()I
v4 = this.val$proc;
v4 = (( com.android.server.am.AppStateManager$AppState$RunningProcess ) v4 ).getPid ( ); // invoke-virtual {v4}, Lcom/android/server/am/AppStateManager$AppState$RunningProcess;->getPid()I
v5 = this.val$proc;
(( com.android.server.am.AppStateManager$AppState$RunningProcess ) v5 ).getProcessName ( ); // invoke-virtual {v5}, Lcom/android/server/am/AppStateManager$AppState$RunningProcess;->getProcessName()Ljava/lang/String;
v6 = this.val$reason;
v0 = com.miui.server.smartpower.SmartPowerPolicyManager .-$$Nest$mforzenProcess ( v0,v3,v4,v5,v6 );
if ( v0 != null) { // if-eqz v0, :cond_0
/* .line 852 */
/* new-instance v0, Ljava/lang/StringBuilder; */
/* invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V */
final String v3 = "power hibernation ("; // const-string v3, "power hibernation ("
(( java.lang.StringBuilder ) v0 ).append ( v3 ); // invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
v3 = this.val$proc;
/* .line 853 */
v3 = (( com.android.server.am.AppStateManager$AppState$RunningProcess ) v3 ).getPid ( ); // invoke-virtual {v3}, Lcom/android/server/am/AppStateManager$AppState$RunningProcess;->getPid()I
(( java.lang.StringBuilder ) v0 ).append ( v3 ); // invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;
final String v3 = ")"; // const-string v3, ")"
(( java.lang.StringBuilder ) v0 ).append ( v3 ); // invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
v3 = this.val$proc;
/* .line 854 */
(( com.android.server.am.AppStateManager$AppState$RunningProcess ) v3 ).getProcessName ( ); // invoke-virtual {v3}, Lcom/android/server/am/AppStateManager$AppState$RunningProcess;->getProcessName()Ljava/lang/String;
(( java.lang.StringBuilder ) v0 ).append ( v3 ); // invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
final String v3 = "/"; // const-string v3, "/"
(( java.lang.StringBuilder ) v0 ).append ( v3 ); // invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
v3 = this.val$proc;
v3 = (( com.android.server.am.AppStateManager$AppState$RunningProcess ) v3 ).getUid ( ); // invoke-virtual {v3}, Lcom/android/server/am/AppStateManager$AppState$RunningProcess;->getUid()I
(( java.lang.StringBuilder ) v0 ).append ( v3 ); // invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;
final String v3 = " adj "; // const-string v3, " adj "
(( java.lang.StringBuilder ) v0 ).append ( v3 ); // invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
v3 = this.val$proc;
/* .line 855 */
v3 = (( com.android.server.am.AppStateManager$AppState$RunningProcess ) v3 ).getAdj ( ); // invoke-virtual {v3}, Lcom/android/server/am/AppStateManager$AppState$RunningProcess;->getAdj()I
(( java.lang.StringBuilder ) v0 ).append ( v3 ); // invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v0 ).toString ( ); // invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
/* .line 852 */
/* const v3, 0x15ff2 */
android.util.EventLog .writeEvent ( v3,v0 );
/* .line 857 */
} // :cond_0
v0 = this.this$0;
v3 = this.val$proc;
(( com.android.server.am.AppStateManager$AppState$RunningProcess ) v3 ).getPackageName ( ); // invoke-virtual {v3}, Lcom/android/server/am/AppStateManager$AppState$RunningProcess;->getPackageName()Ljava/lang/String;
v4 = this.val$proc;
com.miui.server.smartpower.SmartPowerPolicyManager .-$$Nest$mcompactProcess ( v0,v3,v4 );
/* .line 858 */
android.os.Trace .traceEnd ( v1,v2 );
/* .line 859 */
return;
} // .end method
