.class Lcom/miui/server/smartpower/AppBluetoothResource$BleRecord;
.super Ljava/lang/Object;
.source "AppBluetoothResource.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/miui/server/smartpower/AppBluetoothResource;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "BleRecord"
.end annotation

.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/miui/server/smartpower/AppBluetoothResource$BleRecord$ProfileRecord;
    }
.end annotation


# instance fields
.field mOwnerPid:I

.field mOwnerUid:I

.field private final mProfileRecords:Landroid/util/SparseArray;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/util/SparseArray<",
            "Lcom/miui/server/smartpower/AppBluetoothResource$BleRecord$ProfileRecord;",
            ">;"
        }
    .end annotation
.end field

.field final synthetic this$0:Lcom/miui/server/smartpower/AppBluetoothResource;


# direct methods
.method constructor <init>(Lcom/miui/server/smartpower/AppBluetoothResource;II)V
    .locals 0
    .param p2, "uid"    # I
    .param p3, "pid"    # I

    .line 139
    iput-object p1, p0, Lcom/miui/server/smartpower/AppBluetoothResource$BleRecord;->this$0:Lcom/miui/server/smartpower/AppBluetoothResource;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 137
    new-instance p1, Landroid/util/SparseArray;

    invoke-direct {p1}, Landroid/util/SparseArray;-><init>()V

    iput-object p1, p0, Lcom/miui/server/smartpower/AppBluetoothResource$BleRecord;->mProfileRecords:Landroid/util/SparseArray;

    .line 140
    iput p2, p0, Lcom/miui/server/smartpower/AppBluetoothResource$BleRecord;->mOwnerUid:I

    .line 141
    iput p3, p0, Lcom/miui/server/smartpower/AppBluetoothResource$BleRecord;->mOwnerPid:I

    .line 142
    return-void
.end method


# virtual methods
.method isActive()Z
    .locals 1

    .line 175
    iget-object v0, p0, Lcom/miui/server/smartpower/AppBluetoothResource$BleRecord;->mProfileRecords:Landroid/util/SparseArray;

    invoke-virtual {v0}, Landroid/util/SparseArray;->size()I

    move-result v0

    if-lez v0, :cond_0

    const/4 v0, 0x1

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    :goto_0
    return v0
.end method

.method onBluetoothConnect(II)V
    .locals 7
    .param p1, "bleType"    # I
    .param p2, "flag"    # I

    .line 146
    iget-object v0, p0, Lcom/miui/server/smartpower/AppBluetoothResource$BleRecord;->mProfileRecords:Landroid/util/SparseArray;

    monitor-enter v0

    .line 147
    :try_start_0
    iget-object v1, p0, Lcom/miui/server/smartpower/AppBluetoothResource$BleRecord;->mProfileRecords:Landroid/util/SparseArray;

    invoke-virtual {v1, p1}, Landroid/util/SparseArray;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/miui/server/smartpower/AppBluetoothResource$BleRecord$ProfileRecord;

    .line 148
    .local v1, "profileRecord":Lcom/miui/server/smartpower/AppBluetoothResource$BleRecord$ProfileRecord;
    if-nez v1, :cond_0

    .line 149
    new-instance v2, Lcom/miui/server/smartpower/AppBluetoothResource$BleRecord$ProfileRecord;

    invoke-direct {v2, p0, p1}, Lcom/miui/server/smartpower/AppBluetoothResource$BleRecord$ProfileRecord;-><init>(Lcom/miui/server/smartpower/AppBluetoothResource$BleRecord;I)V

    move-object v1, v2

    .line 150
    iget-object v2, p0, Lcom/miui/server/smartpower/AppBluetoothResource$BleRecord;->mProfileRecords:Landroid/util/SparseArray;

    invoke-virtual {v2, p1, v1}, Landroid/util/SparseArray;->put(ILjava/lang/Object;)V

    .line 152
    :cond_0
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "bluetooth u:"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget v3, p0, Lcom/miui/server/smartpower/AppBluetoothResource$BleRecord;->mOwnerUid:I

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, " p:"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget v3, p0, Lcom/miui/server/smartpower/AppBluetoothResource$BleRecord;->mOwnerPid:I

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, " s:true"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    const v3, 0x15ff2

    invoke-static {v3, v2}, Landroid/util/EventLog;->writeEvent(ILjava/lang/String;)I

    .line 154
    iget-object v2, p0, Lcom/miui/server/smartpower/AppBluetoothResource$BleRecord;->this$0:Lcom/miui/server/smartpower/AppBluetoothResource;

    iget v3, p0, Lcom/miui/server/smartpower/AppBluetoothResource$BleRecord;->mOwnerUid:I

    iget v4, p0, Lcom/miui/server/smartpower/AppBluetoothResource$BleRecord;->mOwnerPid:I

    const/4 v5, 0x1

    const/4 v6, 0x0

    invoke-virtual {v2, v3, v4, v5, v6}, Lcom/miui/server/smartpower/AppBluetoothResource;->reportResourceStatus(IIZI)V

    .line 155
    monitor-exit v0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 156
    invoke-virtual {v1, p2}, Lcom/miui/server/smartpower/AppBluetoothResource$BleRecord$ProfileRecord;->bluetoothConnect(I)V

    .line 157
    return-void

    .line 155
    .end local v1    # "profileRecord":Lcom/miui/server/smartpower/AppBluetoothResource$BleRecord$ProfileRecord;
    :catchall_0
    move-exception v1

    :try_start_1
    monitor-exit v0
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    throw v1
.end method

.method onBluetoothDisconnect(II)V
    .locals 6
    .param p1, "bleType"    # I
    .param p2, "flag"    # I

    .line 160
    iget-object v0, p0, Lcom/miui/server/smartpower/AppBluetoothResource$BleRecord;->mProfileRecords:Landroid/util/SparseArray;

    monitor-enter v0

    .line 161
    :try_start_0
    iget-object v1, p0, Lcom/miui/server/smartpower/AppBluetoothResource$BleRecord;->mProfileRecords:Landroid/util/SparseArray;

    invoke-virtual {v1, p1}, Landroid/util/SparseArray;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/miui/server/smartpower/AppBluetoothResource$BleRecord$ProfileRecord;

    .line 162
    .local v1, "profileRecord":Lcom/miui/server/smartpower/AppBluetoothResource$BleRecord$ProfileRecord;
    if-eqz v1, :cond_0

    .line 163
    invoke-virtual {v1, p2}, Lcom/miui/server/smartpower/AppBluetoothResource$BleRecord$ProfileRecord;->bluetoothDisconnect(I)V

    .line 164
    invoke-virtual {v1}, Lcom/miui/server/smartpower/AppBluetoothResource$BleRecord$ProfileRecord;->isActive()Z

    move-result v2

    if-nez v2, :cond_0

    .line 165
    iget-object v2, p0, Lcom/miui/server/smartpower/AppBluetoothResource$BleRecord;->mProfileRecords:Landroid/util/SparseArray;

    invoke-virtual {v2, p1}, Landroid/util/SparseArray;->remove(I)V

    .line 166
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "bluetooth u:"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget v3, p0, Lcom/miui/server/smartpower/AppBluetoothResource$BleRecord;->mOwnerUid:I

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, " p:"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget v3, p0, Lcom/miui/server/smartpower/AppBluetoothResource$BleRecord;->mOwnerPid:I

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, " s:false"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    const v3, 0x15ff2

    invoke-static {v3, v2}, Landroid/util/EventLog;->writeEvent(ILjava/lang/String;)I

    .line 168
    iget-object v2, p0, Lcom/miui/server/smartpower/AppBluetoothResource$BleRecord;->this$0:Lcom/miui/server/smartpower/AppBluetoothResource;

    iget v3, p0, Lcom/miui/server/smartpower/AppBluetoothResource$BleRecord;->mOwnerUid:I

    iget v4, p0, Lcom/miui/server/smartpower/AppBluetoothResource$BleRecord;->mOwnerPid:I

    const/4 v5, 0x0

    invoke-virtual {v2, v3, v4, v5, v5}, Lcom/miui/server/smartpower/AppBluetoothResource;->reportResourceStatus(IIZI)V

    .line 171
    .end local v1    # "profileRecord":Lcom/miui/server/smartpower/AppBluetoothResource$BleRecord$ProfileRecord;
    :cond_0
    monitor-exit v0

    .line 172
    return-void

    .line 171
    :catchall_0
    move-exception v1

    monitor-exit v0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v1
.end method
