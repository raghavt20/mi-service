.class Lcom/miui/server/smartpower/SmartDisplayPolicyManager$RefreshRatePolicyController$SettingRefreshRateObserver;
.super Landroid/database/ContentObserver;
.source "SmartDisplayPolicyManager.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/miui/server/smartpower/SmartDisplayPolicyManager$RefreshRatePolicyController;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "SettingRefreshRateObserver"
.end annotation


# instance fields
.field private final mContentResolver:Landroid/content/ContentResolver;

.field private final mContext:Landroid/content/Context;

.field private final mMiuiDefaultRefreshRateSetting:Landroid/net/Uri;

.field private final mMiuiRefreshRateSetting:Landroid/net/Uri;

.field private final mMiuiUserRefreshRateSetting:Landroid/net/Uri;

.field private final mThermalRefreshRateSetting:Landroid/net/Uri;

.field final synthetic this$1:Lcom/miui/server/smartpower/SmartDisplayPolicyManager$RefreshRatePolicyController;


# direct methods
.method public constructor <init>(Lcom/miui/server/smartpower/SmartDisplayPolicyManager$RefreshRatePolicyController;Landroid/content/Context;Landroid/os/Handler;)V
    .locals 5
    .param p2, "context"    # Landroid/content/Context;
    .param p3, "handler"    # Landroid/os/Handler;

    .line 1479
    iput-object p1, p0, Lcom/miui/server/smartpower/SmartDisplayPolicyManager$RefreshRatePolicyController$SettingRefreshRateObserver;->this$1:Lcom/miui/server/smartpower/SmartDisplayPolicyManager$RefreshRatePolicyController;

    .line 1480
    invoke-direct {p0, p3}, Landroid/database/ContentObserver;-><init>(Landroid/os/Handler;)V

    .line 1468
    nop

    .line 1469
    const-string p1, "is_smart_fps"

    invoke-static {p1}, Landroid/provider/Settings$System;->getUriFor(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object p1

    iput-object p1, p0, Lcom/miui/server/smartpower/SmartDisplayPolicyManager$RefreshRatePolicyController$SettingRefreshRateObserver;->mMiuiDefaultRefreshRateSetting:Landroid/net/Uri;

    .line 1472
    nop

    .line 1473
    const-string/jumbo v0, "user_refresh_rate"

    invoke-static {v0}, Landroid/provider/Settings$Secure;->getUriFor(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v0

    iput-object v0, p0, Lcom/miui/server/smartpower/SmartDisplayPolicyManager$RefreshRatePolicyController$SettingRefreshRateObserver;->mMiuiUserRefreshRateSetting:Landroid/net/Uri;

    .line 1474
    nop

    .line 1475
    const-string v1, "miui_refresh_rate"

    invoke-static {v1}, Landroid/provider/Settings$Secure;->getUriFor(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v1

    iput-object v1, p0, Lcom/miui/server/smartpower/SmartDisplayPolicyManager$RefreshRatePolicyController$SettingRefreshRateObserver;->mMiuiRefreshRateSetting:Landroid/net/Uri;

    .line 1476
    nop

    .line 1477
    const-string/jumbo v2, "thermal_limit_refresh_rate"

    invoke-static {v2}, Landroid/provider/Settings$System;->getUriFor(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v2

    iput-object v2, p0, Lcom/miui/server/smartpower/SmartDisplayPolicyManager$RefreshRatePolicyController$SettingRefreshRateObserver;->mThermalRefreshRateSetting:Landroid/net/Uri;

    .line 1481
    iput-object p2, p0, Lcom/miui/server/smartpower/SmartDisplayPolicyManager$RefreshRatePolicyController$SettingRefreshRateObserver;->mContext:Landroid/content/Context;

    .line 1482
    invoke-virtual {p2}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v3

    iput-object v3, p0, Lcom/miui/server/smartpower/SmartDisplayPolicyManager$RefreshRatePolicyController$SettingRefreshRateObserver;->mContentResolver:Landroid/content/ContentResolver;

    .line 1484
    const/4 v4, 0x0

    invoke-virtual {v3, p1, v4, p0, v4}, Landroid/content/ContentResolver;->registerContentObserver(Landroid/net/Uri;ZLandroid/database/ContentObserver;I)V

    .line 1486
    invoke-virtual {v3, v0, v4, p0, v4}, Landroid/content/ContentResolver;->registerContentObserver(Landroid/net/Uri;ZLandroid/database/ContentObserver;I)V

    .line 1488
    invoke-virtual {v3, v1, v4, p0, v4}, Landroid/content/ContentResolver;->registerContentObserver(Landroid/net/Uri;ZLandroid/database/ContentObserver;I)V

    .line 1490
    invoke-virtual {v3, v2, v4, p0, v4}, Landroid/content/ContentResolver;->registerContentObserver(Landroid/net/Uri;ZLandroid/database/ContentObserver;I)V

    .line 1492
    return-void
.end method

.method private updateMiuiRefreshRateStatus()V
    .locals 4

    .line 1512
    iget-object v0, p0, Lcom/miui/server/smartpower/SmartDisplayPolicyManager$RefreshRatePolicyController$SettingRefreshRateObserver;->this$1:Lcom/miui/server/smartpower/SmartDisplayPolicyManager$RefreshRatePolicyController;

    iget-object v1, p0, Lcom/miui/server/smartpower/SmartDisplayPolicyManager$RefreshRatePolicyController$SettingRefreshRateObserver;->mContentResolver:Landroid/content/ContentResolver;

    const-string v2, "miui_refresh_rate"

    const/4 v3, 0x0

    invoke-static {v1, v2, v3}, Landroid/provider/Settings$Secure;->getInt(Landroid/content/ContentResolver;Ljava/lang/String;I)I

    move-result v1

    invoke-static {v0, v1}, Lcom/miui/server/smartpower/SmartDisplayPolicyManager$RefreshRatePolicyController;->-$$Nest$fputmMiuiRefreshRate(Lcom/miui/server/smartpower/SmartDisplayPolicyManager$RefreshRatePolicyController;I)V

    .line 1513
    return-void
.end method

.method private updateSettingRefreshRateStatus()V
    .locals 1

    .line 1526
    iget-object v0, p0, Lcom/miui/server/smartpower/SmartDisplayPolicyManager$RefreshRatePolicyController$SettingRefreshRateObserver;->this$1:Lcom/miui/server/smartpower/SmartDisplayPolicyManager$RefreshRatePolicyController;

    invoke-static {v0}, Lcom/miui/server/smartpower/SmartDisplayPolicyManager$RefreshRatePolicyController;->-$$Nest$mupdateAppropriateRefreshRate(Lcom/miui/server/smartpower/SmartDisplayPolicyManager$RefreshRatePolicyController;)V

    .line 1527
    return-void
.end method

.method private updateThermalRefreshRateStatus()V
    .locals 4

    .line 1516
    iget-object v0, p0, Lcom/miui/server/smartpower/SmartDisplayPolicyManager$RefreshRatePolicyController$SettingRefreshRateObserver;->mContentResolver:Landroid/content/ContentResolver;

    const-string/jumbo v1, "thermal_limit_refresh_rate"

    const/4 v2, 0x0

    invoke-static {v0, v1, v2}, Landroid/provider/Settings$System;->getInt(Landroid/content/ContentResolver;Ljava/lang/String;I)I

    move-result v0

    .line 1518
    .local v0, "thermalRefreshRate":I
    iget-object v1, p0, Lcom/miui/server/smartpower/SmartDisplayPolicyManager$RefreshRatePolicyController$SettingRefreshRateObserver;->this$1:Lcom/miui/server/smartpower/SmartDisplayPolicyManager$RefreshRatePolicyController;

    const/16 v3, 0x3c

    if-lt v0, v3, :cond_0

    const/4 v2, 0x1

    :cond_0
    invoke-static {v1, v2}, Lcom/miui/server/smartpower/SmartDisplayPolicyManager$RefreshRatePolicyController;->-$$Nest$fputisThermalWarning(Lcom/miui/server/smartpower/SmartDisplayPolicyManager$RefreshRatePolicyController;Z)V

    .line 1520
    sget-boolean v1, Lcom/miui/server/smartpower/SmartDisplayPolicyManager;->DEBUG:Z

    if-eqz v1, :cond_1

    .line 1521
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v2, "thermal warning changed: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lcom/miui/server/smartpower/SmartDisplayPolicyManager$RefreshRatePolicyController$SettingRefreshRateObserver;->this$1:Lcom/miui/server/smartpower/SmartDisplayPolicyManager$RefreshRatePolicyController;

    invoke-static {v2}, Lcom/miui/server/smartpower/SmartDisplayPolicyManager$RefreshRatePolicyController;->-$$Nest$fgetisThermalWarning(Lcom/miui/server/smartpower/SmartDisplayPolicyManager$RefreshRatePolicyController;)Z

    move-result v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    const-string v2, "SmartPower.DisplayPolicy"

    invoke-static {v2, v1}, Landroid/util/Slog;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 1523
    :cond_1
    return-void
.end method


# virtual methods
.method public onChange(ZLandroid/net/Uri;)V
    .locals 1
    .param p1, "selfChange"    # Z
    .param p2, "uri"    # Landroid/net/Uri;

    .line 1496
    invoke-super {p0, p1, p2}, Landroid/database/ContentObserver;->onChange(ZLandroid/net/Uri;)V

    .line 1497
    iget-object v0, p0, Lcom/miui/server/smartpower/SmartDisplayPolicyManager$RefreshRatePolicyController$SettingRefreshRateObserver;->this$1:Lcom/miui/server/smartpower/SmartDisplayPolicyManager$RefreshRatePolicyController;

    iget-object v0, v0, Lcom/miui/server/smartpower/SmartDisplayPolicyManager$RefreshRatePolicyController;->this$0:Lcom/miui/server/smartpower/SmartDisplayPolicyManager;

    invoke-static {v0}, Lcom/miui/server/smartpower/SmartDisplayPolicyManager;->-$$Nest$misSupportSmartDisplayPolicy(Lcom/miui/server/smartpower/SmartDisplayPolicyManager;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 1498
    return-void

    .line 1501
    :cond_0
    iget-object v0, p0, Lcom/miui/server/smartpower/SmartDisplayPolicyManager$RefreshRatePolicyController$SettingRefreshRateObserver;->mThermalRefreshRateSetting:Landroid/net/Uri;

    invoke-virtual {v0, p2}, Landroid/net/Uri;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 1502
    invoke-direct {p0}, Lcom/miui/server/smartpower/SmartDisplayPolicyManager$RefreshRatePolicyController$SettingRefreshRateObserver;->updateThermalRefreshRateStatus()V

    goto :goto_0

    .line 1503
    :cond_1
    iget-object v0, p0, Lcom/miui/server/smartpower/SmartDisplayPolicyManager$RefreshRatePolicyController$SettingRefreshRateObserver;->mMiuiRefreshRateSetting:Landroid/net/Uri;

    invoke-virtual {v0, p2}, Landroid/net/Uri;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 1504
    invoke-direct {p0}, Lcom/miui/server/smartpower/SmartDisplayPolicyManager$RefreshRatePolicyController$SettingRefreshRateObserver;->updateMiuiRefreshRateStatus()V

    goto :goto_0

    .line 1505
    :cond_2
    iget-object v0, p0, Lcom/miui/server/smartpower/SmartDisplayPolicyManager$RefreshRatePolicyController$SettingRefreshRateObserver;->mMiuiDefaultRefreshRateSetting:Landroid/net/Uri;

    invoke-virtual {v0, p2}, Landroid/net/Uri;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_3

    iget-object v0, p0, Lcom/miui/server/smartpower/SmartDisplayPolicyManager$RefreshRatePolicyController$SettingRefreshRateObserver;->mMiuiUserRefreshRateSetting:Landroid/net/Uri;

    .line 1506
    invoke-virtual {v0, p2}, Landroid/net/Uri;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_4

    .line 1507
    :cond_3
    invoke-direct {p0}, Lcom/miui/server/smartpower/SmartDisplayPolicyManager$RefreshRatePolicyController$SettingRefreshRateObserver;->updateSettingRefreshRateStatus()V

    .line 1509
    :cond_4
    :goto_0
    return-void
.end method
