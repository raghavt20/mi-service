public class com.miui.server.smartpower.SmartBoostPolicyManager {
	 /* .source "SmartBoostPolicyManager.java" */
	 /* # annotations */
	 /* .annotation system Ldalvik/annotation/MemberClasses; */
	 /* value = { */
	 /* Lcom/miui/server/smartpower/SmartBoostPolicyManager$SchedBoostController; */
	 /* } */
} // .end annotation
/* # static fields */
public static final Integer BOOST_TYPE_APP_TRANSITION;
public static final Integer BOOST_TYPE_MULTI_TASK;
public static final Boolean DEBUG;
public static final java.lang.String TAG;
/* # instance fields */
private final android.content.Context mContext;
private final android.os.Handler mHandler;
private com.miui.server.process.ProcessManagerInternal mProcessManagerInternal;
private com.miui.server.smartpower.SmartBoostPolicyManager$SchedBoostController mSchedBoostController;
/* # direct methods */
static java.lang.String -$$Nest$mboostTypeToString ( com.miui.server.smartpower.SmartBoostPolicyManager p0, Integer p1 ) { //bridge//synthethic
	 /* .locals 0 */
	 /* invoke-direct {p0, p1}, Lcom/miui/server/smartpower/SmartBoostPolicyManager;->boostTypeToString(I)Ljava/lang/String; */
} // .end method
static java.lang.String -$$Nest$mgetProcessNameByPid ( com.miui.server.smartpower.SmartBoostPolicyManager p0, Integer p1 ) { //bridge//synthethic
	 /* .locals 0 */
	 /* invoke-direct {p0, p1}, Lcom/miui/server/smartpower/SmartBoostPolicyManager;->getProcessNameByPid(I)Ljava/lang/String; */
} // .end method
static com.miui.server.smartpower.SmartBoostPolicyManager ( ) {
	 /* .locals 1 */
	 /* .line 19 */
	 /* sget-boolean v0, Lcom/android/server/am/SmartPowerService;->DEBUG:Z */
	 com.miui.server.smartpower.SmartBoostPolicyManager.DEBUG = (v0!= 0);
	 return;
} // .end method
public com.miui.server.smartpower.SmartBoostPolicyManager ( ) {
	 /* .locals 1 */
	 /* .param p1, "context" # Landroid/content/Context; */
	 /* .param p2, "looper" # Landroid/os/Looper; */
	 /* .line 32 */
	 /* invoke-direct {p0}, Ljava/lang/Object;-><init>()V */
	 /* .line 33 */
	 this.mContext = p1;
	 /* .line 34 */
	 /* new-instance v0, Landroid/os/Handler; */
	 /* invoke-direct {v0, p2}, Landroid/os/Handler;-><init>(Landroid/os/Looper;)V */
	 this.mHandler = v0;
	 /* .line 35 */
	 return;
} // .end method
private java.lang.String boostTypeToString ( Integer p0 ) {
	 /* .locals 1 */
	 /* .param p1, "boostType" # I */
	 /* .line 61 */
	 /* packed-switch p1, :pswitch_data_0 */
	 /* .line 67 */
	 final String v0 = "UNKNOWN"; // const-string v0, "UNKNOWN"
	 /* .line 65 */
	 /* :pswitch_0 */
	 final String v0 = "APP_TRANSITION"; // const-string v0, "APP_TRANSITION"
	 /* .line 63 */
	 /* :pswitch_1 */
	 final String v0 = "MULTI_TASK"; // const-string v0, "MULTI_TASK"
	 /* :pswitch_data_0 */
	 /* .packed-switch 0x1 */
	 /* :pswitch_1 */
	 /* :pswitch_0 */
} // .end packed-switch
} // .end method
private java.lang.String getProcessNameByPid ( Integer p0 ) {
/* .locals 2 */
/* .param p1, "pid" # I */
/* .line 76 */
v0 = this.mProcessManagerInternal;
(( com.miui.server.process.ProcessManagerInternal ) v0 ).getProcessRecordByPid ( p1 ); // invoke-virtual {v0, p1}, Lcom/miui/server/process/ProcessManagerInternal;->getProcessRecordByPid(I)Lcom/android/server/am/ProcessRecord;
/* .line 77 */
/* .local v0, "record":Lcom/android/server/am/ProcessRecord; */
if ( v0 != null) { // if-eqz v0, :cond_0
	 v1 = this.processName;
} // :cond_0
final String v1 = ""; // const-string v1, ""
} // :goto_0
} // .end method
/* # virtual methods */
public void beginSchedThreads ( Integer[] p0, Integer p1, Integer p2 ) {
/* .locals 8 */
/* .param p1, "tids" # [I */
/* .param p2, "callingPid" # I */
/* .param p3, "boostType" # I */
/* .line 47 */
/* const-wide/16 v6, 0x7d0 */
/* .line 48 */
/* .local v6, "defaultDuration":J */
v0 = this.mSchedBoostController;
/* move-object v1, p1 */
/* move v2, p2 */
/* move-wide v3, v6 */
/* move v5, p3 */
/* invoke-virtual/range {v0 ..v5}, Lcom/miui/server/smartpower/SmartBoostPolicyManager$SchedBoostController;->beginSchedThreads([IIJI)V */
/* .line 49 */
return;
} // .end method
public void beginSchedThreads ( Integer[] p0, Integer p1, Long p2, Integer p3 ) {
/* .locals 6 */
/* .param p1, "tids" # [I */
/* .param p2, "callingPid" # I */
/* .param p3, "duration" # J */
/* .param p5, "boostType" # I */
/* .line 52 */
v0 = this.mSchedBoostController;
/* move-object v1, p1 */
/* move v2, p2 */
/* move-wide v3, p3 */
/* move v5, p5 */
/* invoke-virtual/range {v0 ..v5}, Lcom/miui/server/smartpower/SmartBoostPolicyManager$SchedBoostController;->beginSchedThreads([IIJI)V */
/* .line 53 */
return;
} // .end method
public void init ( ) {
/* .locals 0 */
/* .line 38 */
return;
} // .end method
public void setMultiSenceEnable ( Boolean p0 ) {
/* .locals 0 */
/* .param p1, "enable" # Z */
/* .line 72 */
com.miui.server.smartpower.SmartBoostPolicyManager$SchedBoostController.MULTI_SENCE_ENABLE = (p1!= 0);
/* .line 73 */
return;
} // .end method
public void stopCurrentSchedBoost ( Integer[] p0, Integer p1, Integer p2 ) {
/* .locals 8 */
/* .param p1, "tids" # [I */
/* .param p2, "callingPid" # I */
/* .param p3, "boostType" # I */
/* .line 56 */
/* const-wide/16 v6, 0x0 */
/* .line 57 */
/* .local v6, "stopDuration":J */
v0 = this.mSchedBoostController;
/* move-object v1, p1 */
/* move v2, p2 */
/* move-wide v3, v6 */
/* move v5, p3 */
/* invoke-virtual/range {v0 ..v5}, Lcom/miui/server/smartpower/SmartBoostPolicyManager$SchedBoostController;->beginSchedThreads([IIJI)V */
/* .line 58 */
return;
} // .end method
public void systemReady ( ) {
/* .locals 1 */
/* .line 41 */
/* const-class v0, Lcom/miui/server/process/ProcessManagerInternal; */
com.android.server.LocalServices .getService ( v0 );
/* check-cast v0, Lcom/miui/server/process/ProcessManagerInternal; */
this.mProcessManagerInternal = v0;
/* .line 43 */
/* new-instance v0, Lcom/miui/server/smartpower/SmartBoostPolicyManager$SchedBoostController; */
/* invoke-direct {v0, p0}, Lcom/miui/server/smartpower/SmartBoostPolicyManager$SchedBoostController;-><init>(Lcom/miui/server/smartpower/SmartBoostPolicyManager;)V */
this.mSchedBoostController = v0;
/* .line 44 */
return;
} // .end method
