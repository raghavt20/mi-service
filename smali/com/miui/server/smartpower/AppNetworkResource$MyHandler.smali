.class Lcom/miui/server/smartpower/AppNetworkResource$MyHandler;
.super Landroid/os/Handler;
.source "AppNetworkResource.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/miui/server/smartpower/AppNetworkResource;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "MyHandler"
.end annotation


# instance fields
.field final synthetic this$0:Lcom/miui/server/smartpower/AppNetworkResource;


# direct methods
.method constructor <init>(Lcom/miui/server/smartpower/AppNetworkResource;Landroid/os/Looper;)V
    .locals 0
    .param p2, "looper"    # Landroid/os/Looper;

    .line 149
    iput-object p1, p0, Lcom/miui/server/smartpower/AppNetworkResource$MyHandler;->this$0:Lcom/miui/server/smartpower/AppNetworkResource;

    .line 150
    invoke-direct {p0, p2}, Landroid/os/Handler;-><init>(Landroid/os/Looper;)V

    .line 151
    return-void
.end method


# virtual methods
.method public handleMessage(Landroid/os/Message;)V
    .locals 6
    .param p1, "msg"    # Landroid/os/Message;

    .line 155
    invoke-super {p0, p1}, Landroid/os/Handler;->handleMessage(Landroid/os/Message;)V

    .line 156
    iget v0, p1, Landroid/os/Message;->what:I

    const/4 v1, 0x1

    if-ne v0, v1, :cond_1

    .line 157
    iget-object v0, p1, Landroid/os/Message;->obj:Ljava/lang/Object;

    check-cast v0, Lcom/miui/server/smartpower/AppNetworkResource$NetworkMonitor;

    .line 158
    .local v0, "monitor":Lcom/miui/server/smartpower/AppNetworkResource$NetworkMonitor;
    iget-object v2, p0, Lcom/miui/server/smartpower/AppNetworkResource$MyHandler;->this$0:Lcom/miui/server/smartpower/AppNetworkResource;

    invoke-static {v2}, Lcom/miui/server/smartpower/AppNetworkResource;->-$$Nest$fgetmNetworkMonitorMap(Lcom/miui/server/smartpower/AppNetworkResource;)Ljava/util/HashMap;

    move-result-object v2

    monitor-enter v2

    .line 159
    :try_start_0
    iget-object v3, p0, Lcom/miui/server/smartpower/AppNetworkResource$MyHandler;->this$0:Lcom/miui/server/smartpower/AppNetworkResource;

    invoke-static {v3}, Lcom/miui/server/smartpower/AppNetworkResource;->-$$Nest$fgetmNetworkMonitorMap(Lcom/miui/server/smartpower/AppNetworkResource;)Ljava/util/HashMap;

    move-result-object v3

    invoke-virtual {v3, v0}, Ljava/util/HashMap;->containsValue(Ljava/lang/Object;)Z

    move-result v3

    if-nez v3, :cond_0

    .line 160
    monitor-exit v2

    return-void

    .line 162
    :cond_0
    invoke-static {v0}, Lcom/miui/server/smartpower/AppNetworkResource$NetworkMonitor;->-$$Nest$mupdateNetworkStatus(Lcom/miui/server/smartpower/AppNetworkResource$NetworkMonitor;)V

    .line 163
    iget-object v3, p0, Lcom/miui/server/smartpower/AppNetworkResource$MyHandler;->this$0:Lcom/miui/server/smartpower/AppNetworkResource;

    invoke-static {v3}, Lcom/miui/server/smartpower/AppNetworkResource;->-$$Nest$fgetmHandler(Lcom/miui/server/smartpower/AppNetworkResource;)Landroid/os/Handler;

    move-result-object v3

    invoke-virtual {v3, v1, v0}, Landroid/os/Handler;->obtainMessage(ILjava/lang/Object;)Landroid/os/Message;

    move-result-object v1

    .line 164
    .local v1, "message":Landroid/os/Message;
    iget-object v3, p0, Lcom/miui/server/smartpower/AppNetworkResource$MyHandler;->this$0:Lcom/miui/server/smartpower/AppNetworkResource;

    invoke-static {v3}, Lcom/miui/server/smartpower/AppNetworkResource;->-$$Nest$fgetmHandler(Lcom/miui/server/smartpower/AppNetworkResource;)Landroid/os/Handler;

    move-result-object v3

    const-wide/16 v4, 0x3e8

    invoke-virtual {v3, v1, v4, v5}, Landroid/os/Handler;->sendMessageDelayed(Landroid/os/Message;J)Z

    .line 165
    nop

    .end local v1    # "message":Landroid/os/Message;
    monitor-exit v2

    goto :goto_0

    :catchall_0
    move-exception v1

    monitor-exit v2
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v1

    .line 167
    .end local v0    # "monitor":Lcom/miui/server/smartpower/AppNetworkResource$NetworkMonitor;
    :cond_1
    :goto_0
    return-void
.end method
