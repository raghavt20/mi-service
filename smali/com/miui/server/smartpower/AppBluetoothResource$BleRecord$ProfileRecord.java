class com.miui.server.smartpower.AppBluetoothResource$BleRecord$ProfileRecord {
	 /* .source "AppBluetoothResource.java" */
	 /* # annotations */
	 /* .annotation system Ldalvik/annotation/EnclosingClass; */
	 /* value = Lcom/miui/server/smartpower/AppBluetoothResource$BleRecord; */
} // .end annotation
/* .annotation system Ldalvik/annotation/InnerClass; */
/* accessFlags = 0x0 */
/* name = "ProfileRecord" */
} // .end annotation
/* # instance fields */
Integer mBleType;
private final android.util.ArraySet mFlags;
/* .annotation system Ldalvik/annotation/Signature; */
/* value = { */
/* "Landroid/util/ArraySet<", */
/* "Ljava/lang/Integer;", */
/* ">;" */
/* } */
} // .end annotation
} // .end field
final com.miui.server.smartpower.AppBluetoothResource$BleRecord this$1; //synthetic
/* # direct methods */
 com.miui.server.smartpower.AppBluetoothResource$BleRecord$ProfileRecord ( ) {
/* .locals 1 */
/* .param p1, "this$1" # Lcom/miui/server/smartpower/AppBluetoothResource$BleRecord; */
/* .param p2, "bleType" # I */
/* .line 183 */
this.this$1 = p1;
/* invoke-direct {p0}, Ljava/lang/Object;-><init>()V */
/* .line 181 */
/* new-instance v0, Landroid/util/ArraySet; */
/* invoke-direct {v0}, Landroid/util/ArraySet;-><init>()V */
this.mFlags = v0;
/* .line 184 */
/* iput p2, p0, Lcom/miui/server/smartpower/AppBluetoothResource$BleRecord$ProfileRecord;->mBleType:I */
/* .line 185 */
return;
} // .end method
/* # virtual methods */
void bluetoothConnect ( Integer p0 ) {
/* .locals 2 */
/* .param p1, "flag" # I */
/* .line 188 */
v0 = this.mFlags;
java.lang.Integer .valueOf ( p1 );
(( android.util.ArraySet ) v0 ).add ( v1 ); // invoke-virtual {v0, v1}, Landroid/util/ArraySet;->add(Ljava/lang/Object;)Z
/* .line 189 */
return;
} // .end method
void bluetoothDisconnect ( Integer p0 ) {
/* .locals 2 */
/* .param p1, "flag" # I */
/* .line 192 */
v0 = this.mFlags;
java.lang.Integer .valueOf ( p1 );
(( android.util.ArraySet ) v0 ).remove ( v1 ); // invoke-virtual {v0, v1}, Landroid/util/ArraySet;->remove(Ljava/lang/Object;)Z
/* .line 193 */
return;
} // .end method
Boolean isActive ( ) {
/* .locals 1 */
/* .line 196 */
v0 = this.mFlags;
v0 = (( android.util.ArraySet ) v0 ).size ( ); // invoke-virtual {v0}, Landroid/util/ArraySet;->size()I
/* if-lez v0, :cond_0 */
int v0 = 1; // const/4 v0, 0x1
} // :cond_0
int v0 = 0; // const/4 v0, 0x0
} // :goto_0
} // .end method
