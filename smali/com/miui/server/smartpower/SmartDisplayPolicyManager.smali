.class public Lcom/miui/server/smartpower/SmartDisplayPolicyManager;
.super Ljava/lang/Object;
.source "SmartDisplayPolicyManager.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/miui/server/smartpower/SmartDisplayPolicyManager$RefreshRatePolicyController;,
        Lcom/miui/server/smartpower/SmartDisplayPolicyManager$H;,
        Lcom/miui/server/smartpower/SmartDisplayPolicyManager$FrameInsertionObserver;,
        Lcom/miui/server/smartpower/SmartDisplayPolicyManager$GesturesEventListenerCallback;,
        Lcom/miui/server/smartpower/SmartDisplayPolicyManager$SmartScenarioCallback;,
        Lcom/miui/server/smartpower/SmartDisplayPolicyManager$ThermalTempListenerCallback;,
        Lcom/miui/server/smartpower/SmartDisplayPolicyManager$ComingInteraction;
    }
.end annotation


# static fields
.field private static final CAMERA_PACKAGE_NAME:Ljava/lang/String; = "com.android.camera"

.field private static final CAMERA_REFRESH_RATE_ENABLE:Z

.field public static final DEBUG:Z

.field private static final ENABLE:Z

.field private static final INSET_IME_HIDE:I = 0x0

.field private static final INSET_IME_SHOW:I = 0x1

.field private static final INTERACT_APP_TRANSITION:I = 0x2

.field private static final INTERACT_BASE_TYPE:I = 0x1

.field public static final INTERACT_CVW_GESTURE:I = 0xe

.field private static final INTERACT_FOCUS_WINDOW_CHANGE:I = 0x7

.field public static final INTERACT_FREE_FORM:I = 0xc

.field private static final INTERACT_INPUT_DOWN:I = 0x8

.field private static final INTERACT_INPUT_FLING:I = 0xa

.field private static final INTERACT_INPUT_MOVE:I = 0x9

.field private static final INTERACT_INSET_ANIMATION:I = 0x4

.field public static final INTERACT_MULTI_WINDOW:I = 0xf

.field private static final INTERACT_NONE_TYPE:I = 0x0

.field private static final INTERACT_RECENT_ANIMATION:I = 0x5

.field private static final INTERACT_REMOTE_ANIMATION:I = 0x6

.field public static final INTERACT_SPLIT_SCREEN:I = 0xd

.field private static final INTERACT_STOP_BASE_TYPE:I = -0x64

.field private static final INTERACT_STOP_CAMERA:I = -0x63

.field public static final INTERACT_TRANSITION:I = 0x10

.field private static final INTERACT_WINDOW_ANIMATION:I = 0x3

.field private static final INTERACT_WINDOW_VISIBLE_CHANGED:I = 0xb

.field private static final INVALID_APP_TRANSITION_DUR:I = 0x64

.field private static final INVALID_DEFAULT_DUR:I = 0x64

.field private static final INVALID_SF_MODE_ID:I = -0x1

.field private static final INVALID_WINDOW_ANIMATION_DUR:I = 0xc8

.field private static final MAX_INPUT_FLING_DURATION:I = 0xbb8

.field private static final MODULE_SF_INTERACTION:I = 0x10c

.field private static final MSG_DOWN_REFRESHRATE:I = 0x2

.field private static final MSG_UPDATE_DISPLAY_DEVICE:I = 0x3

.field private static final MSG_UP_REFRESHRATE:I = 0x1

.field private static final SCENARIO_TYPE_EXTERNAL_CASTING:I = 0x4000

.field private static final SCENARIO_TYPE_TOPAPP:I = 0x2

.field private static final SF_INTERACTION_END:I = 0x0

.field private static final SF_INTERACTION_START:I = 0x1

.field private static final SF_PERF_INTERACTION:Ljava/lang/String; = "perf_interaction"

.field public static final TAG:Ljava/lang/String; = "SmartPower.DisplayPolicy"

.field private static final THERMAL_TEMP_THRESHOLD:I

.field private static final TRANSACTION_SF_DISPLAY_FEATURE_IDLE:I = 0x7983


# instance fields
.field private isTempWarning:Z

.field private final mContext:Landroid/content/Context;

.field private mCurrentTemp:I

.field private final mDisplayLock:Ljava/lang/Object;

.field private final mDisplayManagerServiceStub:Lcom/android/server/display/DisplayManagerServiceStub;

.field private final mDisplayModeDirectorStub:Lcom/android/server/display/mode/DisplayModeDirectorStub;

.field private mDisplayModeHeight:I

.field private mDisplayModeWidth:I

.field private mExternalDisplayConnected:Z

.field private mFrameInsertScene:Ljava/lang/String;

.field private mFrameInsertingForGame:Z

.field private mFrameInsertingForVideo:Z

.field private final mFrameInsertionObserver:Lcom/miui/server/smartpower/SmartDisplayPolicyManager$FrameInsertionObserver;

.field private final mHandler:Lcom/miui/server/smartpower/SmartDisplayPolicyManager$H;

.field private final mHandlerTh:Landroid/os/HandlerThread;

.field private mIsCameraInForeground:Z

.field private mLastFocusPackage:Ljava/lang/String;

.field private mLastInputMethodStatus:I

.field private mRefreshRatePolicyController:Lcom/miui/server/smartpower/SmartDisplayPolicyManager$RefreshRatePolicyController;

.field private mSchedBoostGesturesEvent:Lcom/android/server/wm/SchedBoostGesturesEvent;

.field private mSmartScenarioManager:Lcom/miui/server/smartpower/SmartScenarioManager;

.field private mSmartWindowPolicyManager:Lcom/miui/server/smartpower/SmartWindowPolicyManager;

.field private mSupportedDisplayModes:[Landroid/view/SurfaceControl$DisplayMode;

.field private final mSurfaceFlinger:Landroid/os/IBinder;

.field private final mWMS:Lcom/android/server/wm/WindowManagerService;


# direct methods
.method static bridge synthetic -$$Nest$fgetisTempWarning(Lcom/miui/server/smartpower/SmartDisplayPolicyManager;)Z
    .locals 0

    iget-boolean p0, p0, Lcom/miui/server/smartpower/SmartDisplayPolicyManager;->isTempWarning:Z

    return p0
.end method

.method static bridge synthetic -$$Nest$fgetmCurrentTemp(Lcom/miui/server/smartpower/SmartDisplayPolicyManager;)I
    .locals 0

    iget p0, p0, Lcom/miui/server/smartpower/SmartDisplayPolicyManager;->mCurrentTemp:I

    return p0
.end method

.method static bridge synthetic -$$Nest$fgetmExternalDisplayConnected(Lcom/miui/server/smartpower/SmartDisplayPolicyManager;)Z
    .locals 0

    iget-boolean p0, p0, Lcom/miui/server/smartpower/SmartDisplayPolicyManager;->mExternalDisplayConnected:Z

    return p0
.end method

.method static bridge synthetic -$$Nest$fgetmFrameInsertScene(Lcom/miui/server/smartpower/SmartDisplayPolicyManager;)Ljava/lang/String;
    .locals 0

    iget-object p0, p0, Lcom/miui/server/smartpower/SmartDisplayPolicyManager;->mFrameInsertScene:Ljava/lang/String;

    return-object p0
.end method

.method static bridge synthetic -$$Nest$fgetmFrameInsertingForGame(Lcom/miui/server/smartpower/SmartDisplayPolicyManager;)Z
    .locals 0

    iget-boolean p0, p0, Lcom/miui/server/smartpower/SmartDisplayPolicyManager;->mFrameInsertingForGame:Z

    return p0
.end method

.method static bridge synthetic -$$Nest$fgetmFrameInsertingForVideo(Lcom/miui/server/smartpower/SmartDisplayPolicyManager;)Z
    .locals 0

    iget-boolean p0, p0, Lcom/miui/server/smartpower/SmartDisplayPolicyManager;->mFrameInsertingForVideo:Z

    return p0
.end method

.method static bridge synthetic -$$Nest$fgetmIsCameraInForeground(Lcom/miui/server/smartpower/SmartDisplayPolicyManager;)Z
    .locals 0

    iget-boolean p0, p0, Lcom/miui/server/smartpower/SmartDisplayPolicyManager;->mIsCameraInForeground:Z

    return p0
.end method

.method static bridge synthetic -$$Nest$fgetmSupportedDisplayModes(Lcom/miui/server/smartpower/SmartDisplayPolicyManager;)[Landroid/view/SurfaceControl$DisplayMode;
    .locals 0

    iget-object p0, p0, Lcom/miui/server/smartpower/SmartDisplayPolicyManager;->mSupportedDisplayModes:[Landroid/view/SurfaceControl$DisplayMode;

    return-object p0
.end method

.method static bridge synthetic -$$Nest$fputisTempWarning(Lcom/miui/server/smartpower/SmartDisplayPolicyManager;Z)V
    .locals 0

    iput-boolean p1, p0, Lcom/miui/server/smartpower/SmartDisplayPolicyManager;->isTempWarning:Z

    return-void
.end method

.method static bridge synthetic -$$Nest$fputmCurrentTemp(Lcom/miui/server/smartpower/SmartDisplayPolicyManager;I)V
    .locals 0

    iput p1, p0, Lcom/miui/server/smartpower/SmartDisplayPolicyManager;->mCurrentTemp:I

    return-void
.end method

.method static bridge synthetic -$$Nest$fputmExternalDisplayConnected(Lcom/miui/server/smartpower/SmartDisplayPolicyManager;Z)V
    .locals 0

    iput-boolean p1, p0, Lcom/miui/server/smartpower/SmartDisplayPolicyManager;->mExternalDisplayConnected:Z

    return-void
.end method

.method static bridge synthetic -$$Nest$fputmFrameInsertScene(Lcom/miui/server/smartpower/SmartDisplayPolicyManager;Ljava/lang/String;)V
    .locals 0

    iput-object p1, p0, Lcom/miui/server/smartpower/SmartDisplayPolicyManager;->mFrameInsertScene:Ljava/lang/String;

    return-void
.end method

.method static bridge synthetic -$$Nest$fputmFrameInsertingForGame(Lcom/miui/server/smartpower/SmartDisplayPolicyManager;Z)V
    .locals 0

    iput-boolean p1, p0, Lcom/miui/server/smartpower/SmartDisplayPolicyManager;->mFrameInsertingForGame:Z

    return-void
.end method

.method static bridge synthetic -$$Nest$fputmFrameInsertingForVideo(Lcom/miui/server/smartpower/SmartDisplayPolicyManager;Z)V
    .locals 0

    iput-boolean p1, p0, Lcom/miui/server/smartpower/SmartDisplayPolicyManager;->mFrameInsertingForVideo:Z

    return-void
.end method

.method static bridge synthetic -$$Nest$mgetForegroundPackageName(Lcom/miui/server/smartpower/SmartDisplayPolicyManager;)Ljava/lang/String;
    .locals 0

    invoke-direct {p0}, Lcom/miui/server/smartpower/SmartDisplayPolicyManager;->getForegroundPackageName()Ljava/lang/String;

    move-result-object p0

    return-object p0
.end method

.method static bridge synthetic -$$Nest$misSupportSmartDisplayPolicy(Lcom/miui/server/smartpower/SmartDisplayPolicyManager;)Z
    .locals 0

    invoke-direct {p0}, Lcom/miui/server/smartpower/SmartDisplayPolicyManager;->isSupportSmartDisplayPolicy()Z

    move-result p0

    return p0
.end method

.method static bridge synthetic -$$Nest$mnotifyInputEventReceived(Lcom/miui/server/smartpower/SmartDisplayPolicyManager;I)V
    .locals 0

    invoke-direct {p0, p1}, Lcom/miui/server/smartpower/SmartDisplayPolicyManager;->notifyInputEventReceived(I)V

    return-void
.end method

.method static bridge synthetic -$$Nest$mnotifyInputEventReceived(Lcom/miui/server/smartpower/SmartDisplayPolicyManager;IJ)V
    .locals 0

    invoke-direct {p0, p1, p2, p3}, Lcom/miui/server/smartpower/SmartDisplayPolicyManager;->notifyInputEventReceived(IJ)V

    return-void
.end method

.method static bridge synthetic -$$Nest$mshouldDownRefreshRate(Lcom/miui/server/smartpower/SmartDisplayPolicyManager;)V
    .locals 0

    invoke-direct {p0}, Lcom/miui/server/smartpower/SmartDisplayPolicyManager;->shouldDownRefreshRate()V

    return-void
.end method

.method static bridge synthetic -$$Nest$mshouldUpRefreshRate(Lcom/miui/server/smartpower/SmartDisplayPolicyManager;Lcom/miui/server/smartpower/SmartDisplayPolicyManager$ComingInteraction;)V
    .locals 0

    invoke-direct {p0, p1}, Lcom/miui/server/smartpower/SmartDisplayPolicyManager;->shouldUpRefreshRate(Lcom/miui/server/smartpower/SmartDisplayPolicyManager$ComingInteraction;)V

    return-void
.end method

.method static bridge synthetic -$$Nest$mshouldUpdateDisplayDevice(Lcom/miui/server/smartpower/SmartDisplayPolicyManager;)V
    .locals 0

    invoke-direct {p0}, Lcom/miui/server/smartpower/SmartDisplayPolicyManager;->shouldUpdateDisplayDevice()V

    return-void
.end method

.method static bridge synthetic -$$Nest$mupdateCameraInForegroundStatus(Lcom/miui/server/smartpower/SmartDisplayPolicyManager;Z)V
    .locals 0

    invoke-direct {p0, p1}, Lcom/miui/server/smartpower/SmartDisplayPolicyManager;->updateCameraInForegroundStatus(Z)V

    return-void
.end method

.method static bridge synthetic -$$Nest$sfgetENABLE()Z
    .locals 1

    sget-boolean v0, Lcom/miui/server/smartpower/SmartDisplayPolicyManager;->ENABLE:Z

    return v0
.end method

.method static bridge synthetic -$$Nest$sfgetTHERMAL_TEMP_THRESHOLD()I
    .locals 1

    sget v0, Lcom/miui/server/smartpower/SmartDisplayPolicyManager;->THERMAL_TEMP_THRESHOLD:I

    return v0
.end method

.method static bridge synthetic -$$Nest$sminteractionTypeToString(I)Ljava/lang/String;
    .locals 0

    invoke-static {p0}, Lcom/miui/server/smartpower/SmartDisplayPolicyManager;->interactionTypeToString(I)Ljava/lang/String;

    move-result-object p0

    return-object p0
.end method

.method static constructor <clinit>()V
    .locals 1

    .line 68
    sget-boolean v0, Lcom/android/server/am/SmartPowerService;->DEBUG:Z

    sput-boolean v0, Lcom/miui/server/smartpower/SmartDisplayPolicyManager;->DEBUG:Z

    .line 70
    sget-boolean v0, Lcom/miui/app/smartpower/SmartPowerSettings;->DISPLAY_POLICY_ENABLE:Z

    sput-boolean v0, Lcom/miui/server/smartpower/SmartDisplayPolicyManager;->ENABLE:Z

    .line 71
    sget-boolean v0, Lcom/miui/app/smartpower/SmartPowerSettings;->PROP_CAMERA_REFRESH_RATE_ENABLE:Z

    sput-boolean v0, Lcom/miui/server/smartpower/SmartDisplayPolicyManager;->CAMERA_REFRESH_RATE_ENABLE:Z

    .line 74
    sget v0, Lcom/miui/app/smartpower/SmartPowerSettings;->PROP_THERMAL_TEMP_THRESHOLD:I

    sput v0, Lcom/miui/server/smartpower/SmartDisplayPolicyManager;->THERMAL_TEMP_THRESHOLD:I

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Lcom/android/server/wm/WindowManagerService;)V
    .locals 4
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "wms"    # Lcom/android/server/wm/WindowManagerService;

    .line 250
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 207
    new-instance v0, Landroid/os/HandlerThread;

    const-string v1, "SmartPowerDisplayTh"

    const/4 v2, -0x2

    invoke-direct {v0, v1, v2}, Landroid/os/HandlerThread;-><init>(Ljava/lang/String;I)V

    iput-object v0, p0, Lcom/miui/server/smartpower/SmartDisplayPolicyManager;->mHandlerTh:Landroid/os/HandlerThread;

    .line 210
    new-instance v1, Ljava/lang/Object;

    invoke-direct {v1}, Ljava/lang/Object;-><init>()V

    iput-object v1, p0, Lcom/miui/server/smartpower/SmartDisplayPolicyManager;->mDisplayLock:Ljava/lang/Object;

    .line 229
    const/4 v1, 0x0

    iput-object v1, p0, Lcom/miui/server/smartpower/SmartDisplayPolicyManager;->mRefreshRatePolicyController:Lcom/miui/server/smartpower/SmartDisplayPolicyManager$RefreshRatePolicyController;

    .line 231
    iput-object v1, p0, Lcom/miui/server/smartpower/SmartDisplayPolicyManager;->mSchedBoostGesturesEvent:Lcom/android/server/wm/SchedBoostGesturesEvent;

    .line 236
    const/4 v2, 0x0

    iput v2, p0, Lcom/miui/server/smartpower/SmartDisplayPolicyManager;->mLastInputMethodStatus:I

    .line 237
    const-string v3, ""

    iput-object v3, p0, Lcom/miui/server/smartpower/SmartDisplayPolicyManager;->mLastFocusPackage:Ljava/lang/String;

    .line 239
    iput-boolean v2, p0, Lcom/miui/server/smartpower/SmartDisplayPolicyManager;->mExternalDisplayConnected:Z

    .line 241
    iput-boolean v2, p0, Lcom/miui/server/smartpower/SmartDisplayPolicyManager;->mFrameInsertingForGame:Z

    .line 242
    iput-boolean v2, p0, Lcom/miui/server/smartpower/SmartDisplayPolicyManager;->mFrameInsertingForVideo:Z

    .line 243
    iput-object v3, p0, Lcom/miui/server/smartpower/SmartDisplayPolicyManager;->mFrameInsertScene:Ljava/lang/String;

    .line 245
    iput-boolean v2, p0, Lcom/miui/server/smartpower/SmartDisplayPolicyManager;->mIsCameraInForeground:Z

    .line 247
    iput-boolean v2, p0, Lcom/miui/server/smartpower/SmartDisplayPolicyManager;->isTempWarning:Z

    .line 248
    iput v2, p0, Lcom/miui/server/smartpower/SmartDisplayPolicyManager;->mCurrentTemp:I

    .line 251
    iput-object p1, p0, Lcom/miui/server/smartpower/SmartDisplayPolicyManager;->mContext:Landroid/content/Context;

    .line 252
    iput-object p2, p0, Lcom/miui/server/smartpower/SmartDisplayPolicyManager;->mWMS:Lcom/android/server/wm/WindowManagerService;

    .line 253
    const-string v2, "SurfaceFlinger"

    invoke-static {v2}, Landroid/os/ServiceManager;->getService(Ljava/lang/String;)Landroid/os/IBinder;

    move-result-object v2

    iput-object v2, p0, Lcom/miui/server/smartpower/SmartDisplayPolicyManager;->mSurfaceFlinger:Landroid/os/IBinder;

    .line 255
    invoke-static {}, Lcom/android/server/display/DisplayManagerServiceStub;->getInstance()Lcom/android/server/display/DisplayManagerServiceStub;

    move-result-object v2

    iput-object v2, p0, Lcom/miui/server/smartpower/SmartDisplayPolicyManager;->mDisplayManagerServiceStub:Lcom/android/server/display/DisplayManagerServiceStub;

    .line 256
    invoke-static {}, Lcom/android/server/display/mode/DisplayModeDirectorStub;->getInstance()Lcom/android/server/display/mode/DisplayModeDirectorStub;

    move-result-object v2

    iput-object v2, p0, Lcom/miui/server/smartpower/SmartDisplayPolicyManager;->mDisplayModeDirectorStub:Lcom/android/server/display/mode/DisplayModeDirectorStub;

    .line 258
    invoke-virtual {v0}, Landroid/os/HandlerThread;->start()V

    .line 259
    new-instance v2, Lcom/miui/server/smartpower/SmartDisplayPolicyManager$H;

    invoke-virtual {v0}, Landroid/os/HandlerThread;->getLooper()Landroid/os/Looper;

    move-result-object v3

    invoke-direct {v2, p0, v3}, Lcom/miui/server/smartpower/SmartDisplayPolicyManager$H;-><init>(Lcom/miui/server/smartpower/SmartDisplayPolicyManager;Landroid/os/Looper;)V

    iput-object v2, p0, Lcom/miui/server/smartpower/SmartDisplayPolicyManager;->mHandler:Lcom/miui/server/smartpower/SmartDisplayPolicyManager$H;

    .line 260
    nop

    .line 261
    invoke-virtual {v0}, Landroid/os/HandlerThread;->getThreadId()I

    move-result v0

    .line 260
    const/4 v3, 0x1

    invoke-static {v0, v3}, Landroid/os/Process;->setThreadGroupAndCpuset(II)V

    .line 263
    new-instance v0, Lcom/miui/server/smartpower/SmartDisplayPolicyManager$FrameInsertionObserver;

    invoke-direct {v0, p0, p1, v2}, Lcom/miui/server/smartpower/SmartDisplayPolicyManager$FrameInsertionObserver;-><init>(Lcom/miui/server/smartpower/SmartDisplayPolicyManager;Landroid/content/Context;Landroid/os/Handler;)V

    iput-object v0, p0, Lcom/miui/server/smartpower/SmartDisplayPolicyManager;->mFrameInsertionObserver:Lcom/miui/server/smartpower/SmartDisplayPolicyManager$FrameInsertionObserver;

    .line 265
    new-instance v0, Lcom/miui/server/smartpower/SmartDisplayPolicyManager$RefreshRatePolicyController;

    invoke-direct {v0, p0, p1, v2, v1}, Lcom/miui/server/smartpower/SmartDisplayPolicyManager$RefreshRatePolicyController;-><init>(Lcom/miui/server/smartpower/SmartDisplayPolicyManager;Landroid/content/Context;Landroid/os/Handler;Lcom/miui/server/smartpower/SmartDisplayPolicyManager$RefreshRatePolicyController-IA;)V

    iput-object v0, p0, Lcom/miui/server/smartpower/SmartDisplayPolicyManager;->mRefreshRatePolicyController:Lcom/miui/server/smartpower/SmartDisplayPolicyManager$RefreshRatePolicyController;

    .line 266
    return-void
.end method

.method private buildRefreshRateRange(FF)Landroid/view/SurfaceControl$RefreshRateRange;
    .locals 1
    .param p1, "min"    # F
    .param p2, "max"    # F

    .line 796
    new-instance v0, Landroid/view/SurfaceControl$RefreshRateRange;

    invoke-direct {v0, p1, p2}, Landroid/view/SurfaceControl$RefreshRateRange;-><init>(FF)V

    return-object v0
.end method

.method private endControlRefreshRatePolicy(I)V
    .locals 12
    .param p1, "interactType"    # I

    .line 719
    iget-object v0, p0, Lcom/miui/server/smartpower/SmartDisplayPolicyManager;->mRefreshRatePolicyController:Lcom/miui/server/smartpower/SmartDisplayPolicyManager$RefreshRatePolicyController;

    invoke-static {v0}, Lcom/miui/server/smartpower/SmartDisplayPolicyManager$RefreshRatePolicyController;->-$$Nest$mgetMinRefreshRateInPolicy(Lcom/miui/server/smartpower/SmartDisplayPolicyManager$RefreshRatePolicyController;)I

    move-result v0

    .line 720
    .local v0, "minRefreshRateInPolicy":I
    invoke-direct {p0, p1}, Lcom/miui/server/smartpower/SmartDisplayPolicyManager;->getDelayDownTimeForInteraction(I)J

    move-result-wide v8

    .line 722
    .local v8, "delayDownMs":J
    iget-object v10, p0, Lcom/miui/server/smartpower/SmartDisplayPolicyManager;->mRefreshRatePolicyController:Lcom/miui/server/smartpower/SmartDisplayPolicyManager$RefreshRatePolicyController;

    new-instance v11, Lcom/miui/server/smartpower/SmartDisplayPolicyManager$ComingInteraction;

    const/4 v7, 0x0

    move-object v1, v11

    move v2, v0

    move v3, v0

    move v4, p1

    move-wide v5, v8

    invoke-direct/range {v1 .. v7}, Lcom/miui/server/smartpower/SmartDisplayPolicyManager$ComingInteraction;-><init>(IIIJLcom/miui/server/smartpower/SmartDisplayPolicyManager$ComingInteraction-IA;)V

    invoke-static {v10, v11}, Lcom/miui/server/smartpower/SmartDisplayPolicyManager$RefreshRatePolicyController;->-$$Nest$mnotifyInteractionEnd(Lcom/miui/server/smartpower/SmartDisplayPolicyManager$RefreshRatePolicyController;Lcom/miui/server/smartpower/SmartDisplayPolicyManager$ComingInteraction;)V

    .line 724
    return-void
.end method

.method private findBaseSfModeId(F)I
    .locals 4
    .param p1, "upRefreshRate"    # F

    .line 856
    iget-object v0, p0, Lcom/miui/server/smartpower/SmartDisplayPolicyManager;->mDisplayLock:Ljava/lang/Object;

    monitor-enter v0

    .line 857
    :try_start_0
    invoke-direct {p0}, Lcom/miui/server/smartpower/SmartDisplayPolicyManager;->isSwitchingResolution()Z

    move-result v1

    if-eqz v1, :cond_0

    monitor-exit v0

    const/4 v0, -0x1

    return v0

    .line 859
    :cond_0
    iget v1, p0, Lcom/miui/server/smartpower/SmartDisplayPolicyManager;->mDisplayModeWidth:I

    if-eqz v1, :cond_1

    iget v1, p0, Lcom/miui/server/smartpower/SmartDisplayPolicyManager;->mDisplayModeHeight:I

    if-nez v1, :cond_2

    .line 860
    :cond_1
    new-instance v1, Landroid/graphics/Point;

    invoke-direct {v1}, Landroid/graphics/Point;-><init>()V

    .line 861
    .local v1, "displaySize":Landroid/graphics/Point;
    iget-object v2, p0, Lcom/miui/server/smartpower/SmartDisplayPolicyManager;->mWMS:Lcom/android/server/wm/WindowManagerService;

    const/4 v3, 0x0

    invoke-virtual {v2, v3, v1}, Lcom/android/server/wm/WindowManagerService;->getInitialDisplaySize(ILandroid/graphics/Point;)V

    .line 862
    iget v2, v1, Landroid/graphics/Point;->x:I

    iget v3, v1, Landroid/graphics/Point;->y:I

    invoke-direct {p0, v2, v3}, Lcom/miui/server/smartpower/SmartDisplayPolicyManager;->setDisplaySize(II)V

    .line 864
    .end local v1    # "displaySize":Landroid/graphics/Point;
    :cond_2
    invoke-direct {p0, p1}, Lcom/miui/server/smartpower/SmartDisplayPolicyManager;->findBaseSfModeIdLocked(F)I

    move-result v1

    monitor-exit v0

    return v1

    .line 865
    :catchall_0
    move-exception v1

    monitor-exit v0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v1
.end method

.method private findBaseSfModeIdLocked(F)I
    .locals 6
    .param p1, "upRefreshRate"    # F

    .line 869
    iget-object v0, p0, Lcom/miui/server/smartpower/SmartDisplayPolicyManager;->mSupportedDisplayModes:[Landroid/view/SurfaceControl$DisplayMode;

    array-length v1, v0

    const/4 v2, 0x0

    :goto_0
    if-ge v2, v1, :cond_2

    aget-object v3, v0, v2

    .line 870
    .local v3, "mode":Landroid/view/SurfaceControl$DisplayMode;
    iget v4, v3, Landroid/view/SurfaceControl$DisplayMode;->refreshRate:F

    cmpl-float v4, p1, v4

    if-nez v4, :cond_1

    iget v4, p0, Lcom/miui/server/smartpower/SmartDisplayPolicyManager;->mDisplayModeWidth:I

    iget v5, v3, Landroid/view/SurfaceControl$DisplayMode;->width:I

    if-ne v4, v5, :cond_1

    iget v4, p0, Lcom/miui/server/smartpower/SmartDisplayPolicyManager;->mDisplayModeHeight:I

    iget v5, v3, Landroid/view/SurfaceControl$DisplayMode;->height:I

    if-ne v4, v5, :cond_1

    .line 872
    sget-boolean v0, Lcom/miui/server/smartpower/SmartDisplayPolicyManager;->DEBUG:Z

    if-eqz v0, :cond_0

    .line 873
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v1, "select mode: "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    const-string v1, "SmartPower.DisplayPolicy"

    invoke-static {v1, v0}, Landroid/util/Slog;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 875
    :cond_0
    iget v0, v3, Landroid/view/SurfaceControl$DisplayMode;->id:I

    return v0

    .line 869
    .end local v3    # "mode":Landroid/view/SurfaceControl$DisplayMode;
    :cond_1
    add-int/lit8 v2, v2, 0x1

    goto :goto_0

    .line 878
    :cond_2
    const/4 v0, -0x1

    return v0
.end method

.method private getCurrentFocusPackageName()Ljava/lang/String;
    .locals 2

    .line 843
    invoke-static {}, Lcom/android/server/wm/RealTimeModeControllerStub;->get()Lcom/android/server/wm/RealTimeModeControllerStub;

    move-result-object v0

    invoke-interface {v0}, Lcom/android/server/wm/RealTimeModeControllerStub;->getAppPackageName()Ljava/lang/String;

    move-result-object v0

    .line 844
    .local v0, "focusedPackage":Ljava/lang/String;
    if-eqz v0, :cond_0

    move-object v1, v0

    goto :goto_0

    :cond_0
    const-string v1, ""

    :goto_0
    return-object v1
.end method

.method private getDelayDownTimeForInteraction(I)J
    .locals 2
    .param p1, "interactType"    # I

    .line 731
    iget-object v0, p0, Lcom/miui/server/smartpower/SmartDisplayPolicyManager;->mRefreshRatePolicyController:Lcom/miui/server/smartpower/SmartDisplayPolicyManager$RefreshRatePolicyController;

    invoke-static {v0, p1}, Lcom/miui/server/smartpower/SmartDisplayPolicyManager$RefreshRatePolicyController;->-$$Nest$mgetDelayDownTimeForInteraction(Lcom/miui/server/smartpower/SmartDisplayPolicyManager$RefreshRatePolicyController;I)J

    move-result-wide v0

    return-wide v0
.end method

.method private getDurationForInteraction(I)J
    .locals 2
    .param p1, "interactType"    # I

    .line 727
    iget-object v0, p0, Lcom/miui/server/smartpower/SmartDisplayPolicyManager;->mRefreshRatePolicyController:Lcom/miui/server/smartpower/SmartDisplayPolicyManager$RefreshRatePolicyController;

    invoke-static {v0, p1}, Lcom/miui/server/smartpower/SmartDisplayPolicyManager$RefreshRatePolicyController;->-$$Nest$mgetDurationForInteraction(Lcom/miui/server/smartpower/SmartDisplayPolicyManager$RefreshRatePolicyController;I)J

    move-result-wide v0

    return-wide v0
.end method

.method private getForegroundPackageName()Ljava/lang/String;
    .locals 1

    .line 848
    invoke-static {}, Lcom/android/server/am/SystemPressureControllerStub;->getInstance()Lcom/android/server/am/SystemPressureControllerStub;

    move-result-object v0

    invoke-virtual {v0}, Lcom/android/server/am/SystemPressureControllerStub;->getForegroundPackageName()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method private getInteractTypeForMultiTaskAction(I)I
    .locals 1
    .param p1, "type"    # I

    .line 608
    invoke-static {p1}, Lmiui/smartpower/MultiTaskActionManager;->isFreeFormAction(I)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 609
    const/16 v0, 0xc

    return v0

    .line 610
    :cond_0
    invoke-static {p1}, Lmiui/smartpower/MultiTaskActionManager;->isSplictScreenAction(I)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 611
    const/16 v0, 0xd

    return v0

    .line 612
    :cond_1
    invoke-static {p1}, Lmiui/smartpower/MultiTaskActionManager;->isCVWAction(I)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 613
    const/16 v0, 0xe

    return v0

    .line 614
    :cond_2
    invoke-static {p1}, Lmiui/smartpower/MultiTaskActionManager;->isMultiWindowSwitchAction(I)Z

    move-result v0

    if-eqz v0, :cond_3

    .line 615
    const/16 v0, 0xf

    return v0

    .line 617
    :cond_3
    const/4 v0, 0x0

    return v0
.end method

.method private getSupportDisplayModes()[Landroid/view/SurfaceControl$DisplayMode;
    .locals 5

    .line 381
    iget-object v0, p0, Lcom/miui/server/smartpower/SmartDisplayPolicyManager;->mDisplayManagerServiceStub:Lcom/android/server/display/DisplayManagerServiceStub;

    .line 382
    invoke-virtual {v0}, Lcom/android/server/display/DisplayManagerServiceStub;->updateDefaultDisplaySupportMode()Landroid/view/SurfaceControl$DynamicDisplayInfo;

    move-result-object v0

    .line 383
    .local v0, "dynamicDisplayInfo":Landroid/view/SurfaceControl$DynamicDisplayInfo;
    const-string v1, "SmartPower.DisplayPolicy"

    if-nez v0, :cond_0

    .line 384
    const-string v2, "get dynamic display info error"

    invoke-static {v1, v2}, Landroid/util/Slog;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 385
    const/4 v1, 0x0

    return-object v1

    .line 387
    :cond_0
    iget-object v2, v0, Landroid/view/SurfaceControl$DynamicDisplayInfo;->supportedDisplayModes:[Landroid/view/SurfaceControl$DisplayMode;

    .line 388
    .local v2, "supportedDisplayModes":[Landroid/view/SurfaceControl$DisplayMode;
    if-eqz v2, :cond_1

    .line 389
    sget-boolean v3, Lcom/miui/server/smartpower/SmartDisplayPolicyManager;->DEBUG:Z

    if-eqz v3, :cond_1

    .line 390
    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v4, "update display modes: "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-static {v2}, Ljava/util/Arrays;->toString([Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v1, v3}, Landroid/util/Slog;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 393
    :cond_1
    return-object v2
.end method

.method private static interactionTypeToString(I)Ljava/lang/String;
    .locals 1
    .param p0, "type"    # I

    .line 1547
    packed-switch p0, :pswitch_data_0

    .line 1579
    :pswitch_0
    invoke-static {p0}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v0

    return-object v0

    .line 1577
    :pswitch_1
    const-string v0, "multi task: multi window"

    return-object v0

    .line 1575
    :pswitch_2
    const-string v0, "multi task: cvw gesture"

    return-object v0

    .line 1573
    :pswitch_3
    const-string v0, "multi task: split screen"

    return-object v0

    .line 1571
    :pswitch_4
    const-string v0, "multi task: free form"

    return-object v0

    .line 1569
    :pswitch_5
    const-string/jumbo v0, "window visible changed"

    return-object v0

    .line 1567
    :pswitch_6
    const-string v0, "input: fling"

    return-object v0

    .line 1565
    :pswitch_7
    const-string v0, "input: move"

    return-object v0

    .line 1563
    :pswitch_8
    const-string v0, "input: down"

    return-object v0

    .line 1561
    :pswitch_9
    const-string v0, "focus window changed"

    return-object v0

    .line 1559
    :pswitch_a
    const-string v0, "remote animation"

    return-object v0

    .line 1557
    :pswitch_b
    const-string v0, "recent animation"

    return-object v0

    .line 1555
    :pswitch_c
    const-string/jumbo v0, "window animation"

    return-object v0

    .line 1553
    :pswitch_d
    const-string v0, "app transition"

    return-object v0

    .line 1551
    :pswitch_e
    const-string v0, "base"

    return-object v0

    .line 1549
    :pswitch_f
    const-string v0, "none"

    return-object v0

    nop

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_f
        :pswitch_e
        :pswitch_d
        :pswitch_c
        :pswitch_0
        :pswitch_b
        :pswitch_a
        :pswitch_9
        :pswitch_8
        :pswitch_7
        :pswitch_6
        :pswitch_5
        :pswitch_4
        :pswitch_3
        :pswitch_2
        :pswitch_1
    .end packed-switch
.end method

.method private isSupportSmartDisplayPolicy()Z
    .locals 2

    .line 833
    sget-boolean v0, Lcom/miui/server/smartpower/SmartDisplayPolicyManager;->ENABLE:Z

    const/4 v1, 0x0

    if-nez v0, :cond_0

    .line 834
    return v1

    .line 836
    :cond_0
    iget-object v0, p0, Lcom/miui/server/smartpower/SmartDisplayPolicyManager;->mSupportedDisplayModes:[Landroid/view/SurfaceControl$DisplayMode;

    if-nez v0, :cond_1

    .line 837
    invoke-direct {p0}, Lcom/miui/server/smartpower/SmartDisplayPolicyManager;->updateSupportDisplayModes()V

    .line 839
    :cond_1
    iget-object v0, p0, Lcom/miui/server/smartpower/SmartDisplayPolicyManager;->mSupportedDisplayModes:[Landroid/view/SurfaceControl$DisplayMode;

    if-eqz v0, :cond_2

    const/4 v1, 0x1

    :cond_2
    return v1
.end method

.method private isSwitchingResolution()Z
    .locals 1

    .line 852
    invoke-static {}, Lcom/android/server/wm/WindowManagerServiceStub;->get()Lcom/android/server/wm/WindowManagerServiceStub;

    move-result-object v0

    invoke-interface {v0}, Lcom/android/server/wm/WindowManagerServiceStub;->isPendingSwitchResolution()Z

    move-result v0

    return v0
.end method

.method private notifyDisplayModeSpecsChanged()V
    .locals 1

    .line 803
    const/4 v0, 0x0

    invoke-direct {p0, v0}, Lcom/miui/server/smartpower/SmartDisplayPolicyManager;->notifySurfaceFlingerInteractionChanged(I)V

    .line 804
    iget-object v0, p0, Lcom/miui/server/smartpower/SmartDisplayPolicyManager;->mDisplayModeDirectorStub:Lcom/android/server/display/mode/DisplayModeDirectorStub;

    invoke-interface {v0}, Lcom/android/server/display/mode/DisplayModeDirectorStub;->notifyDisplayModeSpecsChanged()V

    .line 805
    return-void
.end method

.method private notifyInputEventReceived(I)V
    .locals 2
    .param p1, "inputType"    # I

    .line 544
    invoke-direct {p0, p1}, Lcom/miui/server/smartpower/SmartDisplayPolicyManager;->getDurationForInteraction(I)J

    move-result-wide v0

    invoke-direct {p0, p1, v0, v1}, Lcom/miui/server/smartpower/SmartDisplayPolicyManager;->notifyInputEventReceived(IJ)V

    .line 545
    return-void
.end method

.method private notifyInputEventReceived(IJ)V
    .locals 3
    .param p1, "inputType"    # I
    .param p2, "durationMs"    # J

    .line 548
    invoke-direct {p0}, Lcom/miui/server/smartpower/SmartDisplayPolicyManager;->isSupportSmartDisplayPolicy()Z

    move-result v0

    if-nez v0, :cond_0

    return-void

    .line 550
    :cond_0
    invoke-direct {p0}, Lcom/miui/server/smartpower/SmartDisplayPolicyManager;->getCurrentFocusPackageName()Ljava/lang/String;

    move-result-object v0

    .line 551
    .local v0, "focusedPackage":Ljava/lang/String;
    iget-object v1, p0, Lcom/miui/server/smartpower/SmartDisplayPolicyManager;->mRefreshRatePolicyController:Lcom/miui/server/smartpower/SmartDisplayPolicyManager$RefreshRatePolicyController;

    invoke-static {v1, v0}, Lcom/miui/server/smartpower/SmartDisplayPolicyManager$RefreshRatePolicyController;->-$$Nest$misInputPackageWhiteList(Lcom/miui/server/smartpower/SmartDisplayPolicyManager$RefreshRatePolicyController;Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_2

    .line 552
    sget-boolean v1, Lcom/miui/server/smartpower/SmartDisplayPolicyManager;->DEBUG:Z

    if-eqz v1, :cond_1

    .line 553
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "notifyInputEventReceived, "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-static {p1}, Lcom/miui/server/smartpower/SmartDisplayPolicyManager;->interactionTypeToString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ", durationMs: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p2, p3}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ", focus: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    const-string v2, "SmartPower.DisplayPolicy"

    invoke-static {v2, v1}, Landroid/util/Slog;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 556
    :cond_1
    invoke-direct {p0, p1, p2, p3}, Lcom/miui/server/smartpower/SmartDisplayPolicyManager;->startControlRefreshRatePolicy(IJ)V

    .line 558
    :cond_2
    return-void
.end method

.method private notifyInputMethodAnimationStart(I)V
    .locals 2
    .param p1, "status"    # I

    .line 468
    const/4 v0, 0x1

    if-ne p1, v0, :cond_0

    iget v1, p0, Lcom/miui/server/smartpower/SmartDisplayPolicyManager;->mLastInputMethodStatus:I

    if-ne v1, v0, :cond_0

    .line 469
    return-void

    .line 474
    :cond_0
    if-eq p1, v0, :cond_1

    if-nez p1, :cond_2

    iget-object v0, p0, Lcom/miui/server/smartpower/SmartDisplayPolicyManager;->mLastFocusPackage:Ljava/lang/String;

    .line 475
    invoke-direct {p0}, Lcom/miui/server/smartpower/SmartDisplayPolicyManager;->getCurrentFocusPackageName()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 476
    :cond_1
    const/4 v0, 0x4

    invoke-direct {p0, v0}, Lcom/miui/server/smartpower/SmartDisplayPolicyManager;->startControlRefreshRatePolicy(I)V

    .line 478
    :cond_2
    iput p1, p0, Lcom/miui/server/smartpower/SmartDisplayPolicyManager;->mLastInputMethodStatus:I

    .line 479
    invoke-direct {p0}, Lcom/miui/server/smartpower/SmartDisplayPolicyManager;->getCurrentFocusPackageName()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/miui/server/smartpower/SmartDisplayPolicyManager;->mLastFocusPackage:Ljava/lang/String;

    .line 480
    return-void
.end method

.method private notifySurfaceFlingerInteractionChanged(I)V
    .locals 5
    .param p1, "actionType"    # I

    .line 808
    iget-object v0, p0, Lcom/miui/server/smartpower/SmartDisplayPolicyManager;->mSurfaceFlinger:Landroid/os/IBinder;

    if-nez v0, :cond_0

    return-void

    .line 810
    :cond_0
    invoke-static {}, Landroid/os/Parcel;->obtain()Landroid/os/Parcel;

    move-result-object v0

    .line 811
    .local v0, "data":Landroid/os/Parcel;
    const-string v1, "android.ui.ISurfaceComposer"

    invoke-virtual {v0, v1}, Landroid/os/Parcel;->writeInterfaceToken(Ljava/lang/String;)V

    .line 812
    const/16 v1, 0x10c

    invoke-virtual {v0, v1}, Landroid/os/Parcel;->writeInt(I)V

    .line 813
    invoke-virtual {v0, p1}, Landroid/os/Parcel;->writeInt(I)V

    .line 814
    const-string v1, "perf_interaction"

    invoke-virtual {v0, v1}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 816
    :try_start_0
    iget-object v1, p0, Lcom/miui/server/smartpower/SmartDisplayPolicyManager;->mSurfaceFlinger:Landroid/os/IBinder;

    const/4 v2, 0x0

    const/4 v3, 0x1

    const/16 v4, 0x7983

    invoke-interface {v1, v4, v0, v2, v3}, Landroid/os/IBinder;->transact(ILandroid/os/Parcel;Landroid/os/Parcel;I)Z
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/lang/SecurityException; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 821
    nop

    :goto_0
    invoke-virtual {v0}, Landroid/os/Parcel;->recycle()V

    .line 822
    goto :goto_1

    .line 821
    :catchall_0
    move-exception v1

    goto :goto_2

    .line 818
    :catch_0
    move-exception v1

    .line 819
    .local v1, "ex":Ljava/lang/Exception;
    :try_start_1
    const-string v2, "SmartPower.DisplayPolicy"

    const-string v3, "Failed to notify idle to SurfaceFlinger"

    invoke-static {v2, v3, v1}, Landroid/util/Slog;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 821
    nop

    .end local v1    # "ex":Ljava/lang/Exception;
    goto :goto_0

    .line 823
    :goto_1
    return-void

    .line 821
    :goto_2
    invoke-virtual {v0}, Landroid/os/Parcel;->recycle()V

    .line 822
    throw v1
.end method

.method private realControlRefreshRate(IZFF)V
    .locals 6
    .param p1, "baseSfModeId"    # I
    .param p2, "allowGroupSwitching"    # Z
    .param p3, "minUpRefreshRate"    # F
    .param p4, "maxUpRefreshRate"    # F

    .line 782
    const/4 v0, 0x1

    invoke-direct {p0, v0}, Lcom/miui/server/smartpower/SmartDisplayPolicyManager;->notifySurfaceFlingerInteractionChanged(I)V

    .line 784
    const/4 v0, 0x0

    const/high16 v1, 0x7f800000    # Float.POSITIVE_INFINITY

    invoke-direct {p0, v0, v1}, Lcom/miui/server/smartpower/SmartDisplayPolicyManager;->buildRefreshRateRange(FF)Landroid/view/SurfaceControl$RefreshRateRange;

    move-result-object v0

    .line 785
    .local v0, "physical":Landroid/view/SurfaceControl$RefreshRateRange;
    invoke-direct {p0, p3, p4}, Lcom/miui/server/smartpower/SmartDisplayPolicyManager;->buildRefreshRateRange(FF)Landroid/view/SurfaceControl$RefreshRateRange;

    move-result-object v1

    .line 787
    .local v1, "render":Landroid/view/SurfaceControl$RefreshRateRange;
    new-instance v2, Landroid/view/SurfaceControl$RefreshRateRanges;

    invoke-direct {v2, v0, v1}, Landroid/view/SurfaceControl$RefreshRateRanges;-><init>(Landroid/view/SurfaceControl$RefreshRateRange;Landroid/view/SurfaceControl$RefreshRateRange;)V

    .line 788
    .local v2, "primaryRanges":Landroid/view/SurfaceControl$RefreshRateRanges;
    new-instance v3, Landroid/view/SurfaceControl$RefreshRateRanges;

    invoke-direct {v3, v0, v1}, Landroid/view/SurfaceControl$RefreshRateRanges;-><init>(Landroid/view/SurfaceControl$RefreshRateRange;Landroid/view/SurfaceControl$RefreshRateRange;)V

    .line 790
    .local v3, "appRequestRanges":Landroid/view/SurfaceControl$RefreshRateRanges;
    new-instance v4, Landroid/view/SurfaceControl$DesiredDisplayModeSpecs;

    invoke-direct {v4, p1, p2, v2, v3}, Landroid/view/SurfaceControl$DesiredDisplayModeSpecs;-><init>(IZLandroid/view/SurfaceControl$RefreshRateRanges;Landroid/view/SurfaceControl$RefreshRateRanges;)V

    .line 792
    .local v4, "desired":Landroid/view/SurfaceControl$DesiredDisplayModeSpecs;
    iget-object v5, p0, Lcom/miui/server/smartpower/SmartDisplayPolicyManager;->mDisplayManagerServiceStub:Lcom/android/server/display/DisplayManagerServiceStub;

    invoke-virtual {v5, v4}, Lcom/android/server/display/DisplayManagerServiceStub;->shouldUpdateDisplayModeSpecs(Landroid/view/SurfaceControl$DesiredDisplayModeSpecs;)V

    .line 793
    return-void
.end method

.method private resetDisplaySize()V
    .locals 2

    .line 361
    iget-object v0, p0, Lcom/miui/server/smartpower/SmartDisplayPolicyManager;->mDisplayLock:Ljava/lang/Object;

    monitor-enter v0

    .line 362
    const/4 v1, 0x0

    :try_start_0
    iput v1, p0, Lcom/miui/server/smartpower/SmartDisplayPolicyManager;->mDisplayModeWidth:I

    .line 363
    iput v1, p0, Lcom/miui/server/smartpower/SmartDisplayPolicyManager;->mDisplayModeHeight:I

    .line 364
    monitor-exit v0

    .line 365
    return-void

    .line 364
    :catchall_0
    move-exception v1

    monitor-exit v0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v1
.end method

.method private setDisplaySize(II)V
    .locals 4
    .param p1, "width"    # I
    .param p2, "height"    # I

    .line 349
    iget-object v0, p0, Lcom/miui/server/smartpower/SmartDisplayPolicyManager;->mDisplayLock:Ljava/lang/Object;

    monitor-enter v0

    .line 350
    :try_start_0
    iput p1, p0, Lcom/miui/server/smartpower/SmartDisplayPolicyManager;->mDisplayModeWidth:I

    .line 351
    iput p2, p0, Lcom/miui/server/smartpower/SmartDisplayPolicyManager;->mDisplayModeHeight:I

    .line 353
    sget-boolean v1, Lcom/miui/server/smartpower/SmartDisplayPolicyManager;->DEBUG:Z

    if-eqz v1, :cond_0

    .line 354
    const-string v1, "SmartPower.DisplayPolicy"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v3, "update display size: "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget v3, p0, Lcom/miui/server/smartpower/SmartDisplayPolicyManager;->mDisplayModeWidth:I

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, " * "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget v3, p0, Lcom/miui/server/smartpower/SmartDisplayPolicyManager;->mDisplayModeHeight:I

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Slog;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 357
    :cond_0
    monitor-exit v0

    .line 358
    return-void

    .line 357
    :catchall_0
    move-exception v1

    monitor-exit v0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v1
.end method

.method private shouldDownRefreshRate()V
    .locals 2

    .line 767
    iget-object v0, p0, Lcom/miui/server/smartpower/SmartDisplayPolicyManager;->mHandler:Lcom/miui/server/smartpower/SmartDisplayPolicyManager$H;

    const/4 v1, 0x2

    invoke-virtual {v0, v1}, Lcom/miui/server/smartpower/SmartDisplayPolicyManager$H;->removeMessages(I)V

    .line 769
    iget-object v0, p0, Lcom/miui/server/smartpower/SmartDisplayPolicyManager;->mRefreshRatePolicyController:Lcom/miui/server/smartpower/SmartDisplayPolicyManager$RefreshRatePolicyController;

    invoke-static {v0}, Lcom/miui/server/smartpower/SmartDisplayPolicyManager$RefreshRatePolicyController;->-$$Nest$mreset(Lcom/miui/server/smartpower/SmartDisplayPolicyManager$RefreshRatePolicyController;)V

    .line 770
    invoke-direct {p0}, Lcom/miui/server/smartpower/SmartDisplayPolicyManager;->notifyDisplayModeSpecsChanged()V

    .line 772
    sget-boolean v0, Lcom/miui/server/smartpower/SmartDisplayPolicyManager;->DEBUG:Z

    if-eqz v0, :cond_0

    .line 773
    const-string v0, "SmartPower.DisplayPolicy"

    const-string v1, "down refreshrate"

    invoke-static {v0, v1}, Landroid/util/Slog;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 775
    :cond_0
    return-void
.end method

.method private shouldUpRefreshRate(Lcom/miui/server/smartpower/SmartDisplayPolicyManager$ComingInteraction;)V
    .locals 9
    .param p1, "comingInteraction"    # Lcom/miui/server/smartpower/SmartDisplayPolicyManager$ComingInteraction;

    .line 739
    invoke-static {p1}, Lcom/miui/server/smartpower/SmartDisplayPolicyManager$ComingInteraction;->-$$Nest$fgetmMaxRefreshRate(Lcom/miui/server/smartpower/SmartDisplayPolicyManager$ComingInteraction;)I

    move-result v0

    int-to-float v0, v0

    .line 740
    .local v0, "maxUpRefreshRate":F
    invoke-static {p1}, Lcom/miui/server/smartpower/SmartDisplayPolicyManager$ComingInteraction;->-$$Nest$fgetmMinRefreshRate(Lcom/miui/server/smartpower/SmartDisplayPolicyManager$ComingInteraction;)I

    move-result v1

    int-to-float v1, v1

    .line 741
    .local v1, "minUpRefreshRate":F
    invoke-static {p1}, Lcom/miui/server/smartpower/SmartDisplayPolicyManager$ComingInteraction;->-$$Nest$fgetmDuration(Lcom/miui/server/smartpower/SmartDisplayPolicyManager$ComingInteraction;)J

    move-result-wide v2

    .line 743
    .local v2, "interactDuration":J
    invoke-direct {p0, v0}, Lcom/miui/server/smartpower/SmartDisplayPolicyManager;->findBaseSfModeId(F)I

    move-result v4

    .line 744
    .local v4, "baseSfModeId":I
    const/4 v5, -0x1

    const-string v6, "SmartPower.DisplayPolicy"

    if-eq v4, v5, :cond_2

    invoke-direct {p0}, Lcom/miui/server/smartpower/SmartDisplayPolicyManager;->isSwitchingResolution()Z

    move-result v5

    if-eqz v5, :cond_0

    goto :goto_0

    .line 752
    :cond_0
    const/4 v5, 0x0

    invoke-direct {p0, v4, v5, v1, v0}, Lcom/miui/server/smartpower/SmartDisplayPolicyManager;->realControlRefreshRate(IZFF)V

    .line 754
    iget-object v5, p0, Lcom/miui/server/smartpower/SmartDisplayPolicyManager;->mHandler:Lcom/miui/server/smartpower/SmartDisplayPolicyManager$H;

    const/4 v7, 0x2

    invoke-virtual {v5, v7}, Lcom/miui/server/smartpower/SmartDisplayPolicyManager$H;->obtainMessage(I)Landroid/os/Message;

    move-result-object v5

    .line 755
    .local v5, "msg":Landroid/os/Message;
    iget-object v7, p0, Lcom/miui/server/smartpower/SmartDisplayPolicyManager;->mHandler:Lcom/miui/server/smartpower/SmartDisplayPolicyManager$H;

    invoke-virtual {v7, v5, v2, v3}, Lcom/miui/server/smartpower/SmartDisplayPolicyManager$H;->sendMessageDelayed(Landroid/os/Message;J)Z

    .line 757
    sget-boolean v7, Lcom/miui/server/smartpower/SmartDisplayPolicyManager;->DEBUG:Z

    if-eqz v7, :cond_1

    .line 758
    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v8, "up refreshrate: "

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7, v0}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-static {v6, v7}, Landroid/util/Slog;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 760
    :cond_1
    return-void

    .line 745
    .end local v5    # "msg":Landroid/os/Message;
    :cond_2
    :goto_0
    iget-object v5, p0, Lcom/miui/server/smartpower/SmartDisplayPolicyManager;->mRefreshRatePolicyController:Lcom/miui/server/smartpower/SmartDisplayPolicyManager$RefreshRatePolicyController;

    invoke-static {v5}, Lcom/miui/server/smartpower/SmartDisplayPolicyManager$RefreshRatePolicyController;->-$$Nest$mreset(Lcom/miui/server/smartpower/SmartDisplayPolicyManager$RefreshRatePolicyController;)V

    .line 746
    sget-boolean v5, Lcom/miui/server/smartpower/SmartDisplayPolicyManager;->DEBUG:Z

    if-eqz v5, :cond_3

    .line 747
    const-string v5, "invalid baseSfModeId or switching resolution now"

    invoke-static {v6, v5}, Landroid/util/Slog;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 749
    :cond_3
    return-void
.end method

.method private shouldUpdateDisplayDevice()V
    .locals 0

    .line 668
    invoke-direct {p0}, Lcom/miui/server/smartpower/SmartDisplayPolicyManager;->updateSupportDisplayModes()V

    .line 669
    invoke-direct {p0}, Lcom/miui/server/smartpower/SmartDisplayPolicyManager;->resetDisplaySize()V

    .line 670
    return-void
.end method

.method private startControlRefreshRatePolicy(I)V
    .locals 2
    .param p1, "interactType"    # I

    .line 707
    invoke-direct {p0, p1}, Lcom/miui/server/smartpower/SmartDisplayPolicyManager;->getDurationForInteraction(I)J

    move-result-wide v0

    invoke-direct {p0, p1, v0, v1}, Lcom/miui/server/smartpower/SmartDisplayPolicyManager;->startControlRefreshRatePolicy(IJ)V

    .line 708
    return-void
.end method

.method private startControlRefreshRatePolicy(IJ)V
    .locals 11
    .param p1, "interactType"    # I
    .param p2, "durationMs"    # J

    .line 711
    iget-object v0, p0, Lcom/miui/server/smartpower/SmartDisplayPolicyManager;->mRefreshRatePolicyController:Lcom/miui/server/smartpower/SmartDisplayPolicyManager$RefreshRatePolicyController;

    invoke-static {v0}, Lcom/miui/server/smartpower/SmartDisplayPolicyManager$RefreshRatePolicyController;->-$$Nest$mgetMaxRefreshRateInPolicy(Lcom/miui/server/smartpower/SmartDisplayPolicyManager$RefreshRatePolicyController;)I

    move-result v0

    .line 712
    .local v0, "maxRefreshRateInPolicy":I
    iget-object v1, p0, Lcom/miui/server/smartpower/SmartDisplayPolicyManager;->mRefreshRatePolicyController:Lcom/miui/server/smartpower/SmartDisplayPolicyManager$RefreshRatePolicyController;

    invoke-static {v1}, Lcom/miui/server/smartpower/SmartDisplayPolicyManager$RefreshRatePolicyController;->-$$Nest$mgetMinRefreshRateInPolicy(Lcom/miui/server/smartpower/SmartDisplayPolicyManager$RefreshRatePolicyController;)I

    move-result v8

    .line 714
    .local v8, "minRefreshRateInPolicy":I
    iget-object v9, p0, Lcom/miui/server/smartpower/SmartDisplayPolicyManager;->mRefreshRatePolicyController:Lcom/miui/server/smartpower/SmartDisplayPolicyManager$RefreshRatePolicyController;

    new-instance v10, Lcom/miui/server/smartpower/SmartDisplayPolicyManager$ComingInteraction;

    const/4 v7, 0x0

    move-object v1, v10

    move v2, v8

    move v3, v0

    move v4, p1

    move-wide v5, p2

    invoke-direct/range {v1 .. v7}, Lcom/miui/server/smartpower/SmartDisplayPolicyManager$ComingInteraction;-><init>(IIIJLcom/miui/server/smartpower/SmartDisplayPolicyManager$ComingInteraction-IA;)V

    invoke-static {v9, v10}, Lcom/miui/server/smartpower/SmartDisplayPolicyManager$RefreshRatePolicyController;->-$$Nest$mnotifyInteractionStart(Lcom/miui/server/smartpower/SmartDisplayPolicyManager$RefreshRatePolicyController;Lcom/miui/server/smartpower/SmartDisplayPolicyManager$ComingInteraction;)V

    .line 716
    return-void
.end method

.method private updateCameraInForegroundStatus(Z)V
    .locals 1
    .param p1, "isInForeground"    # Z

    .line 409
    sget-boolean v0, Lcom/miui/server/smartpower/SmartDisplayPolicyManager;->CAMERA_REFRESH_RATE_ENABLE:Z

    if-nez v0, :cond_0

    .line 410
    return-void

    .line 412
    :cond_0
    iput-boolean p1, p0, Lcom/miui/server/smartpower/SmartDisplayPolicyManager;->mIsCameraInForeground:Z

    .line 413
    if-eqz p1, :cond_1

    .line 414
    const/16 v0, -0x63

    invoke-direct {p0, v0}, Lcom/miui/server/smartpower/SmartDisplayPolicyManager;->endControlRefreshRatePolicy(I)V

    .line 416
    :cond_1
    return-void
.end method

.method private updatePolicyControllerLocked()V
    .locals 1

    .line 397
    iget-object v0, p0, Lcom/miui/server/smartpower/SmartDisplayPolicyManager;->mRefreshRatePolicyController:Lcom/miui/server/smartpower/SmartDisplayPolicyManager$RefreshRatePolicyController;

    invoke-static {v0}, Lcom/miui/server/smartpower/SmartDisplayPolicyManager$RefreshRatePolicyController;->-$$Nest$mupdateControllerLocked(Lcom/miui/server/smartpower/SmartDisplayPolicyManager$RefreshRatePolicyController;)V

    .line 398
    return-void
.end method

.method private updateSupportDisplayModes()V
    .locals 3

    .line 368
    invoke-direct {p0}, Lcom/miui/server/smartpower/SmartDisplayPolicyManager;->getSupportDisplayModes()[Landroid/view/SurfaceControl$DisplayMode;

    move-result-object v0

    .line 369
    .local v0, "displayModes":[Landroid/view/SurfaceControl$DisplayMode;
    iget-object v1, p0, Lcom/miui/server/smartpower/SmartDisplayPolicyManager;->mDisplayLock:Ljava/lang/Object;

    monitor-enter v1

    .line 370
    :try_start_0
    iput-object v0, p0, Lcom/miui/server/smartpower/SmartDisplayPolicyManager;->mSupportedDisplayModes:[Landroid/view/SurfaceControl$DisplayMode;

    .line 371
    invoke-direct {p0}, Lcom/miui/server/smartpower/SmartDisplayPolicyManager;->updatePolicyControllerLocked()V

    .line 372
    monitor-exit v1

    .line 373
    return-void

    .line 372
    :catchall_0
    move-exception v2

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v2
.end method


# virtual methods
.method public dump(Ljava/io/PrintWriter;[Ljava/lang/String;I)V
    .locals 2
    .param p1, "pw"    # Ljava/io/PrintWriter;
    .param p2, "args"    # [Ljava/lang/String;
    .param p3, "opti"    # I

    .line 882
    const-string v0, "SmartDisplayPolicyManager"

    invoke-virtual {p1, v0}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V

    .line 883
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "enable: "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-direct {p0}, Lcom/miui/server/smartpower/SmartDisplayPolicyManager;->isSupportSmartDisplayPolicy()Z

    move-result v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v0}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V

    .line 884
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v1, "supportedDisplayModes: "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lcom/miui/server/smartpower/SmartDisplayPolicyManager;->mSupportedDisplayModes:[Landroid/view/SurfaceControl$DisplayMode;

    invoke-static {v1}, Ljava/util/Arrays;->toString([Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v0}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V

    .line 885
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "camera refresh rate enable: "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    sget-boolean v1, Lcom/miui/server/smartpower/SmartDisplayPolicyManager;->CAMERA_REFRESH_RATE_ENABLE:Z

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v0}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V

    .line 886
    iget-object v0, p0, Lcom/miui/server/smartpower/SmartDisplayPolicyManager;->mRefreshRatePolicyController:Lcom/miui/server/smartpower/SmartDisplayPolicyManager$RefreshRatePolicyController;

    if-eqz v0, :cond_0

    .line 887
    invoke-static {v0, p1, p2, p3}, Lcom/miui/server/smartpower/SmartDisplayPolicyManager$RefreshRatePolicyController;->-$$Nest$mdump(Lcom/miui/server/smartpower/SmartDisplayPolicyManager$RefreshRatePolicyController;Ljava/io/PrintWriter;[Ljava/lang/String;I)V

    .line 889
    :cond_0
    return-void
.end method

.method public init(Lcom/miui/server/smartpower/SmartScenarioManager;Lcom/miui/server/smartpower/SmartWindowPolicyManager;)V
    .locals 0
    .param p1, "smartScenarioManager"    # Lcom/miui/server/smartpower/SmartScenarioManager;
    .param p2, "smartWindowPolicyManager"    # Lcom/miui/server/smartpower/SmartWindowPolicyManager;

    .line 270
    iput-object p1, p0, Lcom/miui/server/smartpower/SmartDisplayPolicyManager;->mSmartScenarioManager:Lcom/miui/server/smartpower/SmartScenarioManager;

    .line 271
    iput-object p2, p0, Lcom/miui/server/smartpower/SmartDisplayPolicyManager;->mSmartWindowPolicyManager:Lcom/miui/server/smartpower/SmartWindowPolicyManager;

    .line 272
    return-void
.end method

.method public notifyActivityStartUnchecked(Ljava/lang/String;IILjava/lang/String;IILjava/lang/String;Z)V
    .locals 1
    .param p1, "name"    # Ljava/lang/String;
    .param p2, "uid"    # I
    .param p3, "pid"    # I
    .param p4, "packageName"    # Ljava/lang/String;
    .param p5, "launchedFromUid"    # I
    .param p6, "launchedFromPid"    # I
    .param p7, "launchedFromPackage"    # Ljava/lang/String;
    .param p8, "isColdStart"    # Z

    .line 403
    const-string v0, "com.android.camera"

    invoke-virtual {v0, p4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 404
    const/4 v0, 0x1

    invoke-direct {p0, v0}, Lcom/miui/server/smartpower/SmartDisplayPolicyManager;->updateCameraInForegroundStatus(Z)V

    .line 406
    :cond_0
    return-void
.end method

.method public notifyAppTransitionStartLocked(J)V
    .locals 3
    .param p1, "animDuration"    # J

    .line 419
    invoke-direct {p0}, Lcom/miui/server/smartpower/SmartDisplayPolicyManager;->isSupportSmartDisplayPolicy()Z

    move-result v0

    if-eqz v0, :cond_2

    const-wide/16 v0, 0x64

    cmp-long v0, p1, v0

    if-gtz v0, :cond_0

    goto :goto_0

    .line 421
    :cond_0
    const/4 v0, 0x2

    invoke-direct {p0, v0}, Lcom/miui/server/smartpower/SmartDisplayPolicyManager;->getDurationForInteraction(I)J

    move-result-wide v1

    add-long/2addr p1, v1

    .line 423
    sget-boolean v1, Lcom/miui/server/smartpower/SmartDisplayPolicyManager;->DEBUG:Z

    if-eqz v1, :cond_1

    .line 424
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "notifyAppTransitionStart animDuration: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1, p2}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    const-string v2, "SmartPower.DisplayPolicy"

    invoke-static {v2, v1}, Landroid/util/Slog;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 427
    :cond_1
    invoke-direct {p0, v0, p1, p2}, Lcom/miui/server/smartpower/SmartDisplayPolicyManager;->startControlRefreshRatePolicy(IJ)V

    .line 428
    return-void

    .line 419
    :cond_2
    :goto_0
    return-void
.end method

.method public notifyDisplayDeviceStateChangeLocked(I)V
    .locals 2
    .param p1, "deviceState"    # I

    .line 655
    sget-boolean v0, Lcom/miui/server/smartpower/SmartDisplayPolicyManager;->ENABLE:Z

    if-eqz v0, :cond_0

    const/4 v0, -0x1

    if-eq p1, v0, :cond_0

    .line 659
    iget-object v0, p0, Lcom/miui/server/smartpower/SmartDisplayPolicyManager;->mHandler:Lcom/miui/server/smartpower/SmartDisplayPolicyManager$H;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Lcom/miui/server/smartpower/SmartDisplayPolicyManager$H;->removeMessages(I)V

    .line 660
    iget-object v0, p0, Lcom/miui/server/smartpower/SmartDisplayPolicyManager;->mHandler:Lcom/miui/server/smartpower/SmartDisplayPolicyManager$H;

    const/4 v1, 0x2

    invoke-virtual {v0, v1}, Lcom/miui/server/smartpower/SmartDisplayPolicyManager$H;->removeMessages(I)V

    .line 662
    iget-object v0, p0, Lcom/miui/server/smartpower/SmartDisplayPolicyManager;->mHandler:Lcom/miui/server/smartpower/SmartDisplayPolicyManager$H;

    const/4 v1, 0x3

    invoke-virtual {v0, v1}, Lcom/miui/server/smartpower/SmartDisplayPolicyManager$H;->obtainMessage(I)Landroid/os/Message;

    move-result-object v0

    .line 663
    .local v0, "msg":Landroid/os/Message;
    iget-object v1, p0, Lcom/miui/server/smartpower/SmartDisplayPolicyManager;->mHandler:Lcom/miui/server/smartpower/SmartDisplayPolicyManager$H;

    invoke-virtual {v1, v0}, Lcom/miui/server/smartpower/SmartDisplayPolicyManager$H;->sendMessage(Landroid/os/Message;)Z

    .line 665
    .end local v0    # "msg":Landroid/os/Message;
    :cond_0
    return-void
.end method

.method public notifyDisplaySwitchResolutionLocked(III)V
    .locals 1
    .param p1, "width"    # I
    .param p2, "height"    # I
    .param p3, "density"    # I

    .line 682
    sget-boolean v0, Lcom/miui/server/smartpower/SmartDisplayPolicyManager;->ENABLE:Z

    if-eqz v0, :cond_0

    .line 683
    invoke-direct {p0}, Lcom/miui/server/smartpower/SmartDisplayPolicyManager;->resetDisplaySize()V

    .line 685
    :cond_0
    return-void
.end method

.method public notifyFocusedWindowChangeLocked(Ljava/lang/String;Ljava/lang/String;)V
    .locals 4
    .param p1, "oldFocusPackage"    # Ljava/lang/String;
    .param p2, "newFocusPackage"    # Ljava/lang/String;

    .line 523
    invoke-direct {p0}, Lcom/miui/server/smartpower/SmartDisplayPolicyManager;->isSupportSmartDisplayPolicy()Z

    move-result v0

    if-eqz v0, :cond_5

    if-eqz p1, :cond_5

    if-eqz p2, :cond_5

    .line 526
    invoke-virtual {p1, p2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    goto :goto_1

    .line 530
    :cond_0
    iget-object v0, p0, Lcom/miui/server/smartpower/SmartDisplayPolicyManager;->mRefreshRatePolicyController:Lcom/miui/server/smartpower/SmartDisplayPolicyManager$RefreshRatePolicyController;

    invoke-static {v0, p2}, Lcom/miui/server/smartpower/SmartDisplayPolicyManager$RefreshRatePolicyController;->-$$Nest$misFocusedPackageWhiteList(Lcom/miui/server/smartpower/SmartDisplayPolicyManager$RefreshRatePolicyController;Ljava/lang/String;)Z

    move-result v0

    const-string v1, "SmartPower.DisplayPolicy"

    const/4 v2, 0x7

    if-eqz v0, :cond_2

    .line 531
    sget-boolean v0, Lcom/miui/server/smartpower/SmartDisplayPolicyManager;->DEBUG:Z

    if-eqz v0, :cond_1

    .line 532
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "notifyFocusedWindowChanged, new: "

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v1, v0}, Landroid/util/Slog;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 534
    :cond_1
    invoke-direct {p0, v2}, Lcom/miui/server/smartpower/SmartDisplayPolicyManager;->startControlRefreshRatePolicy(I)V

    goto :goto_0

    .line 535
    :cond_2
    iget-object v0, p0, Lcom/miui/server/smartpower/SmartDisplayPolicyManager;->mRefreshRatePolicyController:Lcom/miui/server/smartpower/SmartDisplayPolicyManager$RefreshRatePolicyController;

    invoke-static {v0, p1}, Lcom/miui/server/smartpower/SmartDisplayPolicyManager$RefreshRatePolicyController;->-$$Nest$misFocusedPackageWhiteList(Lcom/miui/server/smartpower/SmartDisplayPolicyManager$RefreshRatePolicyController;Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_4

    .line 536
    sget-boolean v0, Lcom/miui/server/smartpower/SmartDisplayPolicyManager;->DEBUG:Z

    if-eqz v0, :cond_3

    .line 537
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "notifyFocusedWindowChanged, old: "

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v1, v0}, Landroid/util/Slog;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 539
    :cond_3
    invoke-direct {p0, v2}, Lcom/miui/server/smartpower/SmartDisplayPolicyManager;->endControlRefreshRatePolicy(I)V

    .line 541
    :cond_4
    :goto_0
    return-void

    .line 527
    :cond_5
    :goto_1
    return-void
.end method

.method public notifyInsetAnimationHide(I)V
    .locals 2
    .param p1, "types"    # I

    .line 455
    invoke-direct {p0}, Lcom/miui/server/smartpower/SmartDisplayPolicyManager;->isSupportSmartDisplayPolicy()Z

    move-result v0

    if-nez v0, :cond_0

    return-void

    .line 457
    :cond_0
    sget-boolean v0, Lcom/miui/server/smartpower/SmartDisplayPolicyManager;->DEBUG:Z

    if-eqz v0, :cond_1

    .line 458
    const-string v0, "SmartPower.DisplayPolicy"

    const-string v1, "notifyInsetAnimationHide"

    invoke-static {v0, v1}, Landroid/util/Slog;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 461
    :cond_1
    invoke-static {}, Landroid/view/WindowInsets$Type;->ime()I

    move-result v0

    and-int/2addr v0, p1

    if-eqz v0, :cond_2

    .line 462
    const/4 v0, 0x0

    invoke-direct {p0, v0}, Lcom/miui/server/smartpower/SmartDisplayPolicyManager;->notifyInputMethodAnimationStart(I)V

    .line 464
    :cond_2
    return-void
.end method

.method public notifyInsetAnimationShow(I)V
    .locals 2
    .param p1, "types"    # I

    .line 443
    invoke-direct {p0}, Lcom/miui/server/smartpower/SmartDisplayPolicyManager;->isSupportSmartDisplayPolicy()Z

    move-result v0

    if-nez v0, :cond_0

    return-void

    .line 445
    :cond_0
    sget-boolean v0, Lcom/miui/server/smartpower/SmartDisplayPolicyManager;->DEBUG:Z

    if-eqz v0, :cond_1

    .line 446
    const-string v0, "SmartPower.DisplayPolicy"

    const-string v1, "notifyInsetAnimationShow"

    invoke-static {v0, v1}, Landroid/util/Slog;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 449
    :cond_1
    invoke-static {}, Landroid/view/WindowInsets$Type;->ime()I

    move-result v0

    and-int/2addr v0, p1

    if-eqz v0, :cond_2

    .line 450
    const/4 v0, 0x1

    invoke-direct {p0, v0}, Lcom/miui/server/smartpower/SmartDisplayPolicyManager;->notifyInputMethodAnimationStart(I)V

    .line 452
    :cond_2
    return-void
.end method

.method public notifyMultiTaskActionEnd(Lmiui/smartpower/MultiTaskActionManager$ActionInfo;)V
    .locals 3
    .param p1, "actionInfo"    # Lmiui/smartpower/MultiTaskActionManager$ActionInfo;

    .line 590
    invoke-direct {p0}, Lcom/miui/server/smartpower/SmartDisplayPolicyManager;->isSupportSmartDisplayPolicy()Z

    move-result v0

    if-eqz v0, :cond_3

    if-nez p1, :cond_0

    goto :goto_0

    .line 592
    :cond_0
    invoke-virtual {p1}, Lmiui/smartpower/MultiTaskActionManager$ActionInfo;->getType()I

    move-result v0

    .line 594
    .local v0, "actionType":I
    sget-boolean v1, Lcom/miui/server/smartpower/SmartDisplayPolicyManager;->DEBUG:Z

    if-eqz v1, :cond_1

    .line 595
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "notifyMultiTaskActionEnd: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    .line 596
    invoke-static {v0}, Lmiui/smartpower/MultiTaskActionManager;->actionTypeToString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    .line 595
    const-string v2, "SmartPower.DisplayPolicy"

    invoke-static {v2, v1}, Landroid/util/Slog;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 599
    :cond_1
    invoke-direct {p0, v0}, Lcom/miui/server/smartpower/SmartDisplayPolicyManager;->getInteractTypeForMultiTaskAction(I)I

    move-result v1

    .line 600
    .local v1, "interactType":I
    if-nez v1, :cond_2

    .line 601
    return-void

    .line 604
    :cond_2
    invoke-direct {p0, v1}, Lcom/miui/server/smartpower/SmartDisplayPolicyManager;->endControlRefreshRatePolicy(I)V

    .line 605
    return-void

    .line 590
    .end local v0    # "actionType":I
    .end local v1    # "interactType":I
    :cond_3
    :goto_0
    return-void
.end method

.method public notifyMultiTaskActionStart(Lmiui/smartpower/MultiTaskActionManager$ActionInfo;)V
    .locals 3
    .param p1, "actionInfo"    # Lmiui/smartpower/MultiTaskActionManager$ActionInfo;

    .line 574
    invoke-direct {p0}, Lcom/miui/server/smartpower/SmartDisplayPolicyManager;->isSupportSmartDisplayPolicy()Z

    move-result v0

    if-eqz v0, :cond_3

    if-nez p1, :cond_0

    goto :goto_0

    .line 576
    :cond_0
    invoke-virtual {p1}, Lmiui/smartpower/MultiTaskActionManager$ActionInfo;->getType()I

    move-result v0

    .line 578
    .local v0, "actionType":I
    sget-boolean v1, Lcom/miui/server/smartpower/SmartDisplayPolicyManager;->DEBUG:Z

    if-eqz v1, :cond_1

    .line 579
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "notifyMultiTaskActionStart: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    .line 580
    invoke-static {v0}, Lmiui/smartpower/MultiTaskActionManager;->actionTypeToString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    .line 579
    const-string v2, "SmartPower.DisplayPolicy"

    invoke-static {v2, v1}, Landroid/util/Slog;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 582
    :cond_1
    invoke-direct {p0, v0}, Lcom/miui/server/smartpower/SmartDisplayPolicyManager;->getInteractTypeForMultiTaskAction(I)I

    move-result v1

    .line 583
    .local v1, "interactType":I
    if-nez v1, :cond_2

    .line 584
    return-void

    .line 586
    :cond_2
    invoke-direct {p0, v1}, Lcom/miui/server/smartpower/SmartDisplayPolicyManager;->startControlRefreshRatePolicy(I)V

    .line 587
    return-void

    .line 574
    .end local v0    # "actionType":I
    .end local v1    # "interactType":I
    :cond_3
    :goto_0
    return-void
.end method

.method public notifyRecentsAnimationEnd()V
    .locals 2

    .line 493
    invoke-direct {p0}, Lcom/miui/server/smartpower/SmartDisplayPolicyManager;->isSupportSmartDisplayPolicy()Z

    move-result v0

    if-nez v0, :cond_0

    return-void

    .line 495
    :cond_0
    sget-boolean v0, Lcom/miui/server/smartpower/SmartDisplayPolicyManager;->DEBUG:Z

    if-eqz v0, :cond_1

    .line 496
    const-string v0, "SmartPower.DisplayPolicy"

    const-string v1, "notifyRecentsAnimationEnd"

    invoke-static {v0, v1}, Landroid/util/Slog;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 499
    :cond_1
    const/4 v0, 0x5

    invoke-direct {p0, v0}, Lcom/miui/server/smartpower/SmartDisplayPolicyManager;->endControlRefreshRatePolicy(I)V

    .line 500
    return-void
.end method

.method public notifyRecentsAnimationStart(Lcom/android/server/wm/ActivityRecord;)V
    .locals 2
    .param p1, "targetActivity"    # Lcom/android/server/wm/ActivityRecord;

    .line 483
    invoke-direct {p0}, Lcom/miui/server/smartpower/SmartDisplayPolicyManager;->isSupportSmartDisplayPolicy()Z

    move-result v0

    if-nez v0, :cond_0

    return-void

    .line 485
    :cond_0
    sget-boolean v0, Lcom/miui/server/smartpower/SmartDisplayPolicyManager;->DEBUG:Z

    if-eqz v0, :cond_1

    .line 486
    const-string v0, "SmartPower.DisplayPolicy"

    const-string v1, "notifyRecentsAnimationStart"

    invoke-static {v0, v1}, Landroid/util/Slog;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 489
    :cond_1
    const/4 v0, 0x5

    invoke-direct {p0, v0}, Lcom/miui/server/smartpower/SmartDisplayPolicyManager;->startControlRefreshRatePolicy(I)V

    .line 490
    return-void
.end method

.method public notifyRemoteAnimationEnd()V
    .locals 2

    .line 513
    invoke-direct {p0}, Lcom/miui/server/smartpower/SmartDisplayPolicyManager;->isSupportSmartDisplayPolicy()Z

    move-result v0

    if-nez v0, :cond_0

    return-void

    .line 515
    :cond_0
    sget-boolean v0, Lcom/miui/server/smartpower/SmartDisplayPolicyManager;->DEBUG:Z

    if-eqz v0, :cond_1

    .line 516
    const-string v0, "SmartPower.DisplayPolicy"

    const-string v1, "notifyRemoteAnimationEnd"

    invoke-static {v0, v1}, Landroid/util/Slog;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 519
    :cond_1
    const/4 v0, 0x6

    invoke-direct {p0, v0}, Lcom/miui/server/smartpower/SmartDisplayPolicyManager;->endControlRefreshRatePolicy(I)V

    .line 520
    return-void
.end method

.method public notifyRemoteAnimationStart([Landroid/view/RemoteAnimationTarget;)V
    .locals 2
    .param p1, "appTargets"    # [Landroid/view/RemoteAnimationTarget;

    .line 503
    invoke-direct {p0}, Lcom/miui/server/smartpower/SmartDisplayPolicyManager;->isSupportSmartDisplayPolicy()Z

    move-result v0

    if-nez v0, :cond_0

    return-void

    .line 505
    :cond_0
    sget-boolean v0, Lcom/miui/server/smartpower/SmartDisplayPolicyManager;->DEBUG:Z

    if-eqz v0, :cond_1

    .line 506
    const-string v0, "SmartPower.DisplayPolicy"

    const-string v1, "notifyRemoteAnimationStart"

    invoke-static {v0, v1}, Landroid/util/Slog;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 509
    :cond_1
    const/4 v0, 0x6

    invoke-direct {p0, v0}, Lcom/miui/server/smartpower/SmartDisplayPolicyManager;->startControlRefreshRatePolicy(I)V

    .line 510
    return-void
.end method

.method public notifyTransitionEnd(III)V
    .locals 2
    .param p1, "syncId"    # I
    .param p2, "type"    # I
    .param p3, "flags"    # I

    .line 636
    invoke-direct {p0}, Lcom/miui/server/smartpower/SmartDisplayPolicyManager;->isSupportSmartDisplayPolicy()Z

    move-result v0

    if-nez v0, :cond_0

    return-void

    .line 639
    :cond_0
    const/16 v0, 0xc

    if-ne p2, v0, :cond_1

    .line 640
    return-void

    .line 643
    :cond_1
    sget-boolean v0, Lcom/miui/server/smartpower/SmartDisplayPolicyManager;->DEBUG:Z

    if-eqz v0, :cond_2

    .line 644
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "notifyTransitionFinish: "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-static {p2}, Landroid/view/WindowManager;->transitTypeToString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    const-string v1, "SmartPower.DisplayPolicy"

    invoke-static {v1, v0}, Landroid/util/Slog;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 646
    :cond_2
    const/16 v0, 0x10

    invoke-direct {p0, v0}, Lcom/miui/server/smartpower/SmartDisplayPolicyManager;->endControlRefreshRatePolicy(I)V

    .line 647
    return-void
.end method

.method public notifyTransitionStart(Landroid/window/TransitionInfo;)V
    .locals 2
    .param p1, "info"    # Landroid/window/TransitionInfo;

    .line 621
    invoke-direct {p0}, Lcom/miui/server/smartpower/SmartDisplayPolicyManager;->isSupportSmartDisplayPolicy()Z

    move-result v0

    if-nez v0, :cond_0

    return-void

    .line 624
    :cond_0
    invoke-virtual {p1}, Landroid/window/TransitionInfo;->getType()I

    move-result v0

    const/16 v1, 0xc

    if-ne v0, v1, :cond_1

    .line 625
    return-void

    .line 628
    :cond_1
    sget-boolean v0, Lcom/miui/server/smartpower/SmartDisplayPolicyManager;->DEBUG:Z

    if-eqz v0, :cond_2

    .line 629
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "notifyTransitionReady: "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {p1}, Landroid/window/TransitionInfo;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    const-string v1, "SmartPower.DisplayPolicy"

    invoke-static {v1, v0}, Landroid/util/Slog;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 631
    :cond_2
    const/16 v0, 0x10

    invoke-direct {p0, v0}, Lcom/miui/server/smartpower/SmartDisplayPolicyManager;->startControlRefreshRatePolicy(I)V

    .line 632
    return-void
.end method

.method public notifyWindowAnimationStartLocked(JLandroid/view/SurfaceControl;)V
    .locals 3
    .param p1, "animDuration"    # J
    .param p3, "animationLeash"    # Landroid/view/SurfaceControl;

    .line 431
    invoke-direct {p0}, Lcom/miui/server/smartpower/SmartDisplayPolicyManager;->isSupportSmartDisplayPolicy()Z

    move-result v0

    if-eqz v0, :cond_2

    const-wide/16 v0, 0xc8

    cmp-long v0, p1, v0

    if-gtz v0, :cond_0

    goto :goto_0

    .line 433
    :cond_0
    const/4 v0, 0x3

    invoke-direct {p0, v0}, Lcom/miui/server/smartpower/SmartDisplayPolicyManager;->getDurationForInteraction(I)J

    move-result-wide v1

    add-long/2addr p1, v1

    .line 435
    sget-boolean v1, Lcom/miui/server/smartpower/SmartDisplayPolicyManager;->DEBUG:Z

    if-eqz v1, :cond_1

    .line 436
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "notifyWindowAnimationStart animDuration: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1, p2}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    const-string v2, "SmartPower.DisplayPolicy"

    invoke-static {v2, v1}, Landroid/util/Slog;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 439
    :cond_1
    invoke-direct {p0, v0, p1, p2}, Lcom/miui/server/smartpower/SmartDisplayPolicyManager;->startControlRefreshRatePolicy(IJ)V

    .line 440
    return-void

    .line 431
    :cond_2
    :goto_0
    return-void
.end method

.method public notifyWindowVisibilityChangedLocked(Lcom/android/server/policy/WindowManagerPolicy$WindowState;Landroid/view/WindowManager$LayoutParams;Z)V
    .locals 2
    .param p1, "win"    # Lcom/android/server/policy/WindowManagerPolicy$WindowState;
    .param p2, "attrs"    # Landroid/view/WindowManager$LayoutParams;
    .param p3, "visible"    # Z

    .line 563
    invoke-direct {p0}, Lcom/miui/server/smartpower/SmartDisplayPolicyManager;->isSupportSmartDisplayPolicy()Z

    move-result v0

    if-nez v0, :cond_0

    return-void

    .line 565
    :cond_0
    if-eqz p3, :cond_2

    iget-object v0, p0, Lcom/miui/server/smartpower/SmartDisplayPolicyManager;->mRefreshRatePolicyController:Lcom/miui/server/smartpower/SmartDisplayPolicyManager$RefreshRatePolicyController;

    invoke-static {v0, p1, p2}, Lcom/miui/server/smartpower/SmartDisplayPolicyManager$RefreshRatePolicyController;->-$$Nest$misWindowVisibleWhiteList(Lcom/miui/server/smartpower/SmartDisplayPolicyManager$RefreshRatePolicyController;Lcom/android/server/policy/WindowManagerPolicy$WindowState;Landroid/view/WindowManager$LayoutParams;)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 566
    sget-boolean v0, Lcom/miui/server/smartpower/SmartDisplayPolicyManager;->DEBUG:Z

    if-eqz v0, :cond_1

    .line 567
    const-string v0, "SmartPower.DisplayPolicy"

    const-string v1, "notifyWindowVisibilityChangedLocked"

    invoke-static {v0, v1}, Landroid/util/Slog;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 569
    :cond_1
    const/16 v0, 0xb

    invoke-direct {p0, v0}, Lcom/miui/server/smartpower/SmartDisplayPolicyManager;->startControlRefreshRatePolicy(I)V

    .line 571
    :cond_2
    return-void
.end method

.method public shouldInterceptUpdateDisplayModeSpecs(ILcom/android/server/display/mode/DisplayModeDirector$DesiredDisplayModeSpecs;)Z
    .locals 2
    .param p1, "displayId"    # I
    .param p2, "modeSpecs"    # Lcom/android/server/display/mode/DisplayModeDirector$DesiredDisplayModeSpecs;

    .line 699
    invoke-direct {p0}, Lcom/miui/server/smartpower/SmartDisplayPolicyManager;->isSupportSmartDisplayPolicy()Z

    move-result v0

    const/4 v1, 0x0

    if-nez v0, :cond_0

    return v1

    .line 701
    :cond_0
    if-eqz p1, :cond_1

    return v1

    .line 703
    :cond_1
    iget-object v0, p0, Lcom/miui/server/smartpower/SmartDisplayPolicyManager;->mRefreshRatePolicyController:Lcom/miui/server/smartpower/SmartDisplayPolicyManager$RefreshRatePolicyController;

    invoke-static {v0, p2}, Lcom/miui/server/smartpower/SmartDisplayPolicyManager$RefreshRatePolicyController;->-$$Nest$mshouldInterceptUpdateDisplayModeSpecs(Lcom/miui/server/smartpower/SmartDisplayPolicyManager$RefreshRatePolicyController;Lcom/android/server/display/mode/DisplayModeDirector$DesiredDisplayModeSpecs;)Z

    move-result v0

    return v0
.end method

.method public systemReady()V
    .locals 6

    .line 276
    new-instance v0, Lcom/android/server/wm/SchedBoostGesturesEvent;

    iget-object v1, p0, Lcom/miui/server/smartpower/SmartDisplayPolicyManager;->mHandlerTh:Landroid/os/HandlerThread;

    invoke-virtual {v1}, Landroid/os/HandlerThread;->getLooper()Landroid/os/Looper;

    move-result-object v1

    invoke-direct {v0, v1}, Lcom/android/server/wm/SchedBoostGesturesEvent;-><init>(Landroid/os/Looper;)V

    iput-object v0, p0, Lcom/miui/server/smartpower/SmartDisplayPolicyManager;->mSchedBoostGesturesEvent:Lcom/android/server/wm/SchedBoostGesturesEvent;

    .line 277
    iget-object v1, p0, Lcom/miui/server/smartpower/SmartDisplayPolicyManager;->mContext:Landroid/content/Context;

    invoke-virtual {v0, v1}, Lcom/android/server/wm/SchedBoostGesturesEvent;->init(Landroid/content/Context;)V

    .line 278
    iget-object v0, p0, Lcom/miui/server/smartpower/SmartDisplayPolicyManager;->mSchedBoostGesturesEvent:Lcom/android/server/wm/SchedBoostGesturesEvent;

    new-instance v1, Lcom/miui/server/smartpower/SmartDisplayPolicyManager$GesturesEventListenerCallback;

    const/4 v2, 0x0

    invoke-direct {v1, p0, v2}, Lcom/miui/server/smartpower/SmartDisplayPolicyManager$GesturesEventListenerCallback;-><init>(Lcom/miui/server/smartpower/SmartDisplayPolicyManager;Lcom/miui/server/smartpower/SmartDisplayPolicyManager$GesturesEventListenerCallback-IA;)V

    invoke-virtual {v0, v1}, Lcom/android/server/wm/SchedBoostGesturesEvent;->setGesturesEventListener(Lcom/android/server/wm/SchedBoostGesturesEvent$GesturesEventListener;)V

    .line 281
    iget-object v0, p0, Lcom/miui/server/smartpower/SmartDisplayPolicyManager;->mSmartScenarioManager:Lcom/miui/server/smartpower/SmartScenarioManager;

    new-instance v1, Lcom/miui/server/smartpower/SmartDisplayPolicyManager$SmartScenarioCallback;

    invoke-direct {v1, p0, v2}, Lcom/miui/server/smartpower/SmartDisplayPolicyManager$SmartScenarioCallback;-><init>(Lcom/miui/server/smartpower/SmartDisplayPolicyManager;Lcom/miui/server/smartpower/SmartDisplayPolicyManager$SmartScenarioCallback-IA;)V

    .line 282
    const-string v3, "SmartPower.DisplayPolicy"

    invoke-virtual {v0, v3, v1}, Lcom/miui/server/smartpower/SmartScenarioManager;->createClientConfig(Ljava/lang/String;Lcom/miui/server/smartpower/SmartScenarioManager$ISmartScenarioCallback;)Lcom/miui/server/smartpower/SmartScenarioManager$ClientConfig;

    move-result-object v0

    .line 283
    .local v0, "config":Lcom/miui/server/smartpower/SmartScenarioManager$ClientConfig;
    const-string v1, "com.android.camera"

    const/4 v3, 0x2

    const-wide/16 v4, 0x2

    invoke-virtual {v0, v4, v5, v1, v3}, Lcom/miui/server/smartpower/SmartScenarioManager$ClientConfig;->addMainScenarioIdConfig(JLjava/lang/String;I)V

    .line 284
    const-wide/16 v3, 0x4000

    const/16 v1, 0x4000

    invoke-virtual {v0, v3, v4, v2, v1}, Lcom/miui/server/smartpower/SmartScenarioManager$ClientConfig;->addAdditionalScenarioIdConfig(JLjava/lang/String;I)V

    .line 286
    iget-object v1, p0, Lcom/miui/server/smartpower/SmartDisplayPolicyManager;->mSmartScenarioManager:Lcom/miui/server/smartpower/SmartScenarioManager;

    invoke-virtual {v1, v0}, Lcom/miui/server/smartpower/SmartScenarioManager;->registClientConfig(Lcom/miui/server/smartpower/SmartScenarioManager$ClientConfig;)V

    .line 289
    invoke-static {}, Lcom/android/server/am/SystemPressureControllerStub;->getInstance()Lcom/android/server/am/SystemPressureControllerStub;

    move-result-object v1

    new-instance v3, Lcom/miui/server/smartpower/SmartDisplayPolicyManager$ThermalTempListenerCallback;

    invoke-direct {v3, p0, v2}, Lcom/miui/server/smartpower/SmartDisplayPolicyManager$ThermalTempListenerCallback;-><init>(Lcom/miui/server/smartpower/SmartDisplayPolicyManager;Lcom/miui/server/smartpower/SmartDisplayPolicyManager$ThermalTempListenerCallback-IA;)V

    invoke-virtual {v1, v3}, Lcom/android/server/am/SystemPressureControllerStub;->registerThermalTempListener(Lcom/android/server/am/ThermalTempListener;)V

    .line 291
    return-void
.end method
