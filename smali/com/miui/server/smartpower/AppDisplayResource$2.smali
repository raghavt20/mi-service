.class Lcom/miui/server/smartpower/AppDisplayResource$2;
.super Landroid/content/BroadcastReceiver;
.source "AppDisplayResource.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/miui/server/smartpower/AppDisplayResource;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/miui/server/smartpower/AppDisplayResource;


# direct methods
.method constructor <init>(Lcom/miui/server/smartpower/AppDisplayResource;)V
    .locals 0
    .param p1, "this$0"    # Lcom/miui/server/smartpower/AppDisplayResource;

    .line 153
    iput-object p1, p0, Lcom/miui/server/smartpower/AppDisplayResource$2;->this$0:Lcom/miui/server/smartpower/AppDisplayResource;

    invoke-direct {p0}, Landroid/content/BroadcastReceiver;-><init>()V

    return-void
.end method


# virtual methods
.method public onReceive(Landroid/content/Context;Landroid/content/Intent;)V
    .locals 6
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "intent"    # Landroid/content/Intent;

    .line 156
    invoke-virtual {p2}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v0

    .line 157
    .local v0, "action":Ljava/lang/String;
    const-string v1, "android.net.wifi.p2p.CONNECTION_STATE_CHANGE"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    .line 158
    const-string v1, "networkInfo"

    invoke-virtual {p2, v1}, Landroid/content/Intent;->getParcelableExtra(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v1

    check-cast v1, Landroid/net/NetworkInfo;

    .line 160
    .local v1, "networkInfo":Landroid/net/NetworkInfo;
    if-eqz v1, :cond_2

    .line 161
    sget-boolean v2, Lcom/miui/server/smartpower/AppPowerResource;->DEBUG:Z

    if-eqz v2, :cond_0

    .line 162
    const-string v2, "SmartPower.AppResource"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "Received WIFI_P2P_CONNECTION_CHANGED_ACTION: networkInfo="

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Landroid/util/Slog;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 165
    :cond_0
    invoke-virtual {v1}, Landroid/net/NetworkInfo;->isConnected()Z

    move-result v2

    .line 166
    .local v2, "connected":Z
    iget-object v3, p0, Lcom/miui/server/smartpower/AppDisplayResource$2;->this$0:Lcom/miui/server/smartpower/AppDisplayResource;

    invoke-static {v3}, Lcom/miui/server/smartpower/AppDisplayResource;->-$$Nest$fgetmWifiP2pConnected(Lcom/miui/server/smartpower/AppDisplayResource;)Z

    move-result v3

    if-eq v2, v3, :cond_2

    .line 167
    iget-object v3, p0, Lcom/miui/server/smartpower/AppDisplayResource$2;->this$0:Lcom/miui/server/smartpower/AppDisplayResource;

    invoke-static {v3, v2}, Lcom/miui/server/smartpower/AppDisplayResource;->-$$Nest$fputmWifiP2pConnected(Lcom/miui/server/smartpower/AppDisplayResource;Z)V

    .line 168
    iget-object v3, p0, Lcom/miui/server/smartpower/AppDisplayResource$2;->this$0:Lcom/miui/server/smartpower/AppDisplayResource;

    invoke-static {v3}, Lcom/miui/server/smartpower/AppDisplayResource;->-$$Nest$fgetmDisplayRecordMap(Lcom/miui/server/smartpower/AppDisplayResource;)Landroid/util/ArrayMap;

    move-result-object v3

    monitor-enter v3

    .line 169
    :try_start_0
    iget-object v4, p0, Lcom/miui/server/smartpower/AppDisplayResource$2;->this$0:Lcom/miui/server/smartpower/AppDisplayResource;

    invoke-static {v4}, Lcom/miui/server/smartpower/AppDisplayResource;->-$$Nest$fgetmDisplayRecordMap(Lcom/miui/server/smartpower/AppDisplayResource;)Landroid/util/ArrayMap;

    move-result-object v4

    invoke-virtual {v4}, Landroid/util/ArrayMap;->values()Ljava/util/Collection;

    move-result-object v4

    invoke-interface {v4}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v4

    :goto_0
    invoke-interface {v4}, Ljava/util/Iterator;->hasNext()Z

    move-result v5

    if-eqz v5, :cond_1

    invoke-interface {v4}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Lcom/miui/server/smartpower/AppDisplayResource$DisplayRecord;

    .line 170
    .local v5, "record":Lcom/miui/server/smartpower/AppDisplayResource$DisplayRecord;
    invoke-static {v5}, Lcom/miui/server/smartpower/AppDisplayResource$DisplayRecord;->-$$Nest$mupdateCurrentStatus(Lcom/miui/server/smartpower/AppDisplayResource$DisplayRecord;)V

    .line 171
    .end local v5    # "record":Lcom/miui/server/smartpower/AppDisplayResource$DisplayRecord;
    goto :goto_0

    .line 172
    :cond_1
    monitor-exit v3

    goto :goto_1

    :catchall_0
    move-exception v4

    monitor-exit v3
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v4

    .line 176
    .end local v1    # "networkInfo":Landroid/net/NetworkInfo;
    .end local v2    # "connected":Z
    :cond_2
    :goto_1
    return-void
.end method
