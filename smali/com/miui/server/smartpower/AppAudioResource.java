class com.miui.server.smartpower.AppAudioResource extends com.miui.server.smartpower.AppPowerResource {
	 /* .source "AppAudioResource.java" */
	 /* # annotations */
	 /* .annotation system Ldalvik/annotation/MemberClasses; */
	 /* value = { */
	 /* Lcom/miui/server/smartpower/AppAudioResource$AudioRecord; */
	 /* } */
} // .end annotation
/* # static fields */
private static final Long AUDIO_INACTIVE_DELAY_TIME;
public static final Integer PLAYER_STATE_ZERO_PLAYER;
/* # instance fields */
private final android.util.SparseArray mActivePidsMap;
/* .annotation system Ldalvik/annotation/Signature; */
/* value = { */
/* "Landroid/util/SparseArray<", */
/* "Lcom/miui/server/smartpower/AppAudioResource$AudioRecord;", */
/* ">;" */
/* } */
} // .end annotation
} // .end field
private android.media.AudioManager mAudioManager;
private android.os.Handler mHandler;
private Long mLastMusicPlayPid;
private Long mLastMusicPlayTimeStamp;
/* # direct methods */
static android.media.AudioManager -$$Nest$fgetmAudioManager ( com.miui.server.smartpower.AppAudioResource p0 ) { //bridge//synthethic
/* .locals 0 */
p0 = this.mAudioManager;
} // .end method
static android.os.Handler -$$Nest$fgetmHandler ( com.miui.server.smartpower.AppAudioResource p0 ) { //bridge//synthethic
/* .locals 0 */
p0 = this.mHandler;
} // .end method
static void -$$Nest$fputmLastMusicPlayPid ( com.miui.server.smartpower.AppAudioResource p0, Long p1 ) { //bridge//synthethic
/* .locals 0 */
/* iput-wide p1, p0, Lcom/miui/server/smartpower/AppAudioResource;->mLastMusicPlayPid:J */
return;
} // .end method
static void -$$Nest$fputmLastMusicPlayTimeStamp ( com.miui.server.smartpower.AppAudioResource p0, Long p1 ) { //bridge//synthethic
/* .locals 0 */
/* iput-wide p1, p0, Lcom/miui/server/smartpower/AppAudioResource;->mLastMusicPlayTimeStamp:J */
return;
} // .end method
static void -$$Nest$mupdateUidStatus ( com.miui.server.smartpower.AppAudioResource p0, Integer p1, Integer p2 ) { //bridge//synthethic
/* .locals 0 */
/* invoke-direct {p0, p1, p2}, Lcom/miui/server/smartpower/AppAudioResource;->updateUidStatus(II)V */
return;
} // .end method
public com.miui.server.smartpower.AppAudioResource ( ) {
/* .locals 1 */
/* .param p1, "context" # Landroid/content/Context; */
/* .param p2, "looper" # Landroid/os/Looper; */
/* .line 32 */
/* invoke-direct {p0}, Lcom/miui/server/smartpower/AppPowerResource;-><init>()V */
/* .line 22 */
/* new-instance v0, Landroid/util/SparseArray; */
/* invoke-direct {v0}, Landroid/util/SparseArray;-><init>()V */
this.mActivePidsMap = v0;
/* .line 33 */
int v0 = 1; // const/4 v0, 0x1
/* iput v0, p0, Lcom/miui/server/smartpower/AppAudioResource;->mType:I */
/* .line 34 */
final String v0 = "audio"; // const-string v0, "audio"
(( android.content.Context ) p1 ).getSystemService ( v0 ); // invoke-virtual {p1, v0}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;
/* check-cast v0, Landroid/media/AudioManager; */
this.mAudioManager = v0;
/* .line 35 */
/* new-instance v0, Landroid/os/Handler; */
/* invoke-direct {v0, p2}, Landroid/os/Handler;-><init>(Landroid/os/Looper;)V */
this.mHandler = v0;
/* .line 36 */
return;
} // .end method
private com.miui.server.smartpower.AppAudioResource$AudioRecord getAudioRecord ( Integer p0 ) {
/* .locals 2 */
/* .param p1, "pid" # I */
/* .line 39 */
v0 = this.mActivePidsMap;
/* monitor-enter v0 */
/* .line 40 */
try { // :try_start_0
v1 = this.mActivePidsMap;
(( android.util.SparseArray ) v1 ).get ( p1 ); // invoke-virtual {v1, p1}, Landroid/util/SparseArray;->get(I)Ljava/lang/Object;
/* check-cast v1, Lcom/miui/server/smartpower/AppAudioResource$AudioRecord; */
/* monitor-exit v0 */
/* .line 41 */
/* :catchall_0 */
/* move-exception v1 */
/* monitor-exit v0 */
/* :try_end_0 */
/* .catchall {:try_start_0 ..:try_end_0} :catchall_0 */
/* throw v1 */
} // .end method
private com.miui.server.smartpower.AppAudioResource$AudioRecord getAudioRecord ( Integer p0, Integer p1 ) {
/* .locals 4 */
/* .param p1, "uid" # I */
/* .param p2, "riid" # I */
/* .line 57 */
v0 = this.mActivePidsMap;
/* monitor-enter v0 */
/* .line 58 */
int v1 = 0; // const/4 v1, 0x0
/* .local v1, "i":I */
} // :goto_0
try { // :try_start_0
v2 = this.mActivePidsMap;
v2 = (( android.util.SparseArray ) v2 ).size ( ); // invoke-virtual {v2}, Landroid/util/SparseArray;->size()I
/* if-ge v1, v2, :cond_1 */
/* .line 59 */
v2 = this.mActivePidsMap;
(( android.util.SparseArray ) v2 ).valueAt ( v1 ); // invoke-virtual {v2, v1}, Landroid/util/SparseArray;->valueAt(I)Ljava/lang/Object;
/* check-cast v2, Lcom/miui/server/smartpower/AppAudioResource$AudioRecord; */
/* .line 60 */
/* .local v2, "record":Lcom/miui/server/smartpower/AppAudioResource$AudioRecord; */
/* iget v3, v2, Lcom/miui/server/smartpower/AppAudioResource$AudioRecord;->mOwnerUid:I */
/* if-ne v3, p1, :cond_0 */
v3 = (( com.miui.server.smartpower.AppAudioResource$AudioRecord ) v2 ).containsRecorder ( p2 ); // invoke-virtual {v2, p2}, Lcom/miui/server/smartpower/AppAudioResource$AudioRecord;->containsRecorder(I)Z
if ( v3 != null) { // if-eqz v3, :cond_0
	 /* .line 61 */
	 /* monitor-exit v0 */
	 /* .line 58 */
} // .end local v2 # "record":Lcom/miui/server/smartpower/AppAudioResource$AudioRecord;
} // :cond_0
/* add-int/lit8 v1, v1, 0x1 */
/* .line 64 */
} // .end local v1 # "i":I
} // :cond_1
/* monitor-exit v0 */
/* .line 65 */
int v0 = 0; // const/4 v0, 0x0
/* .line 64 */
/* :catchall_0 */
/* move-exception v1 */
/* monitor-exit v0 */
/* :try_end_0 */
/* .catchall {:try_start_0 ..:try_end_0} :catchall_0 */
/* throw v1 */
} // .end method
private com.miui.server.smartpower.AppAudioResource$AudioRecord getAudioRecord ( Integer p0, java.lang.String p1 ) {
/* .locals 4 */
/* .param p1, "uid" # I */
/* .param p2, "clientId" # Ljava/lang/String; */
/* .line 45 */
v0 = this.mActivePidsMap;
/* monitor-enter v0 */
/* .line 46 */
int v1 = 0; // const/4 v1, 0x0
/* .local v1, "i":I */
} // :goto_0
try { // :try_start_0
v2 = this.mActivePidsMap;
v2 = (( android.util.SparseArray ) v2 ).size ( ); // invoke-virtual {v2}, Landroid/util/SparseArray;->size()I
/* if-ge v1, v2, :cond_1 */
/* .line 47 */
v2 = this.mActivePidsMap;
(( android.util.SparseArray ) v2 ).valueAt ( v1 ); // invoke-virtual {v2, v1}, Landroid/util/SparseArray;->valueAt(I)Ljava/lang/Object;
/* check-cast v2, Lcom/miui/server/smartpower/AppAudioResource$AudioRecord; */
/* .line 48 */
/* .local v2, "record":Lcom/miui/server/smartpower/AppAudioResource$AudioRecord; */
/* iget v3, v2, Lcom/miui/server/smartpower/AppAudioResource$AudioRecord;->mOwnerUid:I */
/* if-ne v3, p1, :cond_0 */
v3 = (( com.miui.server.smartpower.AppAudioResource$AudioRecord ) v2 ).containsClientId ( p2 ); // invoke-virtual {v2, p2}, Lcom/miui/server/smartpower/AppAudioResource$AudioRecord;->containsClientId(Ljava/lang/String;)Z
if ( v3 != null) { // if-eqz v3, :cond_0
/* .line 49 */
/* monitor-exit v0 */
/* .line 46 */
} // .end local v2 # "record":Lcom/miui/server/smartpower/AppAudioResource$AudioRecord;
} // :cond_0
/* add-int/lit8 v1, v1, 0x1 */
/* .line 52 */
} // .end local v1 # "i":I
} // :cond_1
/* monitor-exit v0 */
/* .line 53 */
int v0 = 0; // const/4 v0, 0x0
/* .line 52 */
/* :catchall_0 */
/* move-exception v1 */
/* monitor-exit v0 */
/* :try_end_0 */
/* .catchall {:try_start_0 ..:try_end_0} :catchall_0 */
/* throw v1 */
} // .end method
private com.miui.server.smartpower.AppAudioResource$AudioRecord getOrCreateAudioRecord ( Integer p0, Integer p1 ) {
/* .locals 3 */
/* .param p1, "uid" # I */
/* .param p2, "pid" # I */
/* .line 69 */
v0 = this.mActivePidsMap;
/* monitor-enter v0 */
/* .line 70 */
try { // :try_start_0
/* invoke-direct {p0, p2}, Lcom/miui/server/smartpower/AppAudioResource;->getAudioRecord(I)Lcom/miui/server/smartpower/AppAudioResource$AudioRecord; */
/* .line 71 */
/* .local v1, "record":Lcom/miui/server/smartpower/AppAudioResource$AudioRecord; */
/* if-nez v1, :cond_0 */
/* .line 72 */
/* new-instance v2, Lcom/miui/server/smartpower/AppAudioResource$AudioRecord; */
/* invoke-direct {v2, p0, p1, p2}, Lcom/miui/server/smartpower/AppAudioResource$AudioRecord;-><init>(Lcom/miui/server/smartpower/AppAudioResource;II)V */
/* move-object v1, v2 */
/* .line 73 */
v2 = this.mActivePidsMap;
(( android.util.SparseArray ) v2 ).put ( p2, v1 ); // invoke-virtual {v2, p2, v1}, Landroid/util/SparseArray;->put(ILjava/lang/Object;)V
/* .line 75 */
} // :cond_0
/* monitor-exit v0 */
/* .line 76 */
} // .end local v1 # "record":Lcom/miui/server/smartpower/AppAudioResource$AudioRecord;
/* :catchall_0 */
/* move-exception v1 */
/* monitor-exit v0 */
/* :try_end_0 */
/* .catchall {:try_start_0 ..:try_end_0} :catchall_0 */
/* throw v1 */
} // .end method
private void updateUidStatus ( Integer p0, Integer p1 ) {
/* .locals 1 */
/* .param p1, "uid" # I */
/* .param p2, "behavier" # I */
/* .line 211 */
v0 = (( com.miui.server.smartpower.AppAudioResource ) p0 ).isAppResourceActive ( p1 ); // invoke-virtual {p0, p1}, Lcom/miui/server/smartpower/AppAudioResource;->isAppResourceActive(I)Z
/* .line 212 */
/* .local v0, "active":Z */
(( com.miui.server.smartpower.AppAudioResource ) p0 ).reportResourceStatus ( p1, v0, p2 ); // invoke-virtual {p0, p1, v0, p2}, Lcom/miui/server/smartpower/AppAudioResource;->reportResourceStatus(IZI)V
/* .line 213 */
return;
} // .end method
/* # virtual methods */
public java.util.ArrayList getActiveUids ( ) {
/* .locals 1 */
/* .annotation system Ldalvik/annotation/Signature; */
/* value = { */
/* "()", */
/* "Ljava/util/ArrayList<", */
/* "Ljava/lang/Integer;", */
/* ">;" */
/* } */
} // .end annotation
/* .line 81 */
int v0 = 0; // const/4 v0, 0x0
} // .end method
public Long getLastMusicPlayTimeStamp ( Integer p0 ) {
/* .locals 4 */
/* .param p1, "pid" # I */
/* .line 108 */
/* iget-wide v0, p0, Lcom/miui/server/smartpower/AppAudioResource;->mLastMusicPlayPid:J */
/* int-to-long v2, p1 */
/* cmp-long v0, v0, v2 */
/* if-nez v0, :cond_0 */
/* .line 109 */
/* iget-wide v0, p0, Lcom/miui/server/smartpower/AppAudioResource;->mLastMusicPlayTimeStamp:J */
/* return-wide v0 */
/* .line 111 */
} // :cond_0
/* const-wide/16 v0, 0x0 */
/* return-wide v0 */
} // .end method
public Boolean isAppResourceActive ( Integer p0 ) {
/* .locals 5 */
/* .param p1, "uid" # I */
/* .line 86 */
int v0 = 0; // const/4 v0, 0x0
/* .line 87 */
/* .local v0, "active":Z */
v1 = this.mActivePidsMap;
/* monitor-enter v1 */
/* .line 88 */
int v2 = 0; // const/4 v2, 0x0
/* .local v2, "i":I */
} // :goto_0
try { // :try_start_0
v3 = this.mActivePidsMap;
v3 = (( android.util.SparseArray ) v3 ).size ( ); // invoke-virtual {v3}, Landroid/util/SparseArray;->size()I
/* if-ge v2, v3, :cond_1 */
/* .line 89 */
v3 = this.mActivePidsMap;
(( android.util.SparseArray ) v3 ).valueAt ( v2 ); // invoke-virtual {v3, v2}, Landroid/util/SparseArray;->valueAt(I)Ljava/lang/Object;
/* check-cast v3, Lcom/miui/server/smartpower/AppAudioResource$AudioRecord; */
/* .line 90 */
/* .local v3, "record":Lcom/miui/server/smartpower/AppAudioResource$AudioRecord; */
if ( v3 != null) { // if-eqz v3, :cond_0
/* iget v4, v3, Lcom/miui/server/smartpower/AppAudioResource$AudioRecord;->mOwnerUid:I */
/* if-ne v4, p1, :cond_0 */
/* iget-boolean v4, v3, Lcom/miui/server/smartpower/AppAudioResource$AudioRecord;->mActive:Z */
if ( v4 != null) { // if-eqz v4, :cond_0
/* .line 91 */
/* monitor-exit v1 */
int v1 = 1; // const/4 v1, 0x1
/* .line 88 */
} // .end local v3 # "record":Lcom/miui/server/smartpower/AppAudioResource$AudioRecord;
} // :cond_0
/* add-int/lit8 v2, v2, 0x1 */
/* .line 94 */
} // .end local v2 # "i":I
} // :cond_1
/* monitor-exit v1 */
/* .line 95 */
/* .line 94 */
/* :catchall_0 */
/* move-exception v2 */
/* monitor-exit v1 */
/* :try_end_0 */
/* .catchall {:try_start_0 ..:try_end_0} :catchall_0 */
/* throw v2 */
} // .end method
public Boolean isAppResourceActive ( Integer p0, Integer p1 ) {
/* .locals 2 */
/* .param p1, "uid" # I */
/* .param p2, "pid" # I */
/* .line 100 */
/* invoke-direct {p0, p2}, Lcom/miui/server/smartpower/AppAudioResource;->getAudioRecord(I)Lcom/miui/server/smartpower/AppAudioResource$AudioRecord; */
/* .line 101 */
/* .local v0, "record":Lcom/miui/server/smartpower/AppAudioResource$AudioRecord; */
if ( v0 != null) { // if-eqz v0, :cond_0
/* .line 102 */
/* iget-boolean v1, v0, Lcom/miui/server/smartpower/AppAudioResource$AudioRecord;->mActive:Z */
/* .line 104 */
} // :cond_0
int v1 = 0; // const/4 v1, 0x0
} // .end method
public void onPlayerEvent ( Integer p0, Integer p1, Integer p2, Integer p3 ) {
/* .locals 1 */
/* .param p1, "uid" # I */
/* .param p2, "pid" # I */
/* .param p3, "piid" # I */
/* .param p4, "event" # I */
/* .line 163 */
/* invoke-direct {p0, p2}, Lcom/miui/server/smartpower/AppAudioResource;->getAudioRecord(I)Lcom/miui/server/smartpower/AppAudioResource$AudioRecord; */
/* .line 164 */
/* .local v0, "record":Lcom/miui/server/smartpower/AppAudioResource$AudioRecord; */
if ( v0 != null) { // if-eqz v0, :cond_0
/* .line 165 */
(( com.miui.server.smartpower.AppAudioResource$AudioRecord ) v0 ).onPlayerEvent ( p3, p4 ); // invoke-virtual {v0, p3, p4}, Lcom/miui/server/smartpower/AppAudioResource$AudioRecord;->onPlayerEvent(II)V
/* .line 167 */
} // :cond_0
return;
} // .end method
public void onPlayerRlease ( Integer p0, Integer p1, Integer p2 ) {
/* .locals 1 */
/* .param p1, "uid" # I */
/* .param p2, "pid" # I */
/* .param p3, "piid" # I */
/* .line 156 */
/* invoke-direct {p0, p2}, Lcom/miui/server/smartpower/AppAudioResource;->getAudioRecord(I)Lcom/miui/server/smartpower/AppAudioResource$AudioRecord; */
/* .line 157 */
/* .local v0, "record":Lcom/miui/server/smartpower/AppAudioResource$AudioRecord; */
if ( v0 != null) { // if-eqz v0, :cond_0
/* .line 158 */
(( com.miui.server.smartpower.AppAudioResource$AudioRecord ) v0 ).onPlayerRlease ( p3 ); // invoke-virtual {v0, p3}, Lcom/miui/server/smartpower/AppAudioResource$AudioRecord;->onPlayerRlease(I)V
/* .line 160 */
} // :cond_0
return;
} // .end method
public void onPlayerTrack ( Integer p0, Integer p1, Integer p2, Integer p3 ) {
/* .locals 1 */
/* .param p1, "uid" # I */
/* .param p2, "pid" # I */
/* .param p3, "piid" # I */
/* .param p4, "sessionId" # I */
/* .line 151 */
/* invoke-direct {p0, p1, p2}, Lcom/miui/server/smartpower/AppAudioResource;->getOrCreateAudioRecord(II)Lcom/miui/server/smartpower/AppAudioResource$AudioRecord; */
/* .line 152 */
/* .local v0, "record":Lcom/miui/server/smartpower/AppAudioResource$AudioRecord; */
(( com.miui.server.smartpower.AppAudioResource$AudioRecord ) v0 ).onPlayerTrack ( p3, p4 ); // invoke-virtual {v0, p3, p4}, Lcom/miui/server/smartpower/AppAudioResource$AudioRecord;->onPlayerTrack(II)V
/* .line 153 */
return;
} // .end method
public void onRecorderEvent ( Integer p0, Integer p1, Integer p2 ) {
/* .locals 1 */
/* .param p1, "uid" # I */
/* .param p2, "riid" # I */
/* .param p3, "event" # I */
/* .line 182 */
/* invoke-direct {p0, p1, p2}, Lcom/miui/server/smartpower/AppAudioResource;->getAudioRecord(II)Lcom/miui/server/smartpower/AppAudioResource$AudioRecord; */
/* .line 183 */
/* .local v0, "record":Lcom/miui/server/smartpower/AppAudioResource$AudioRecord; */
if ( v0 != null) { // if-eqz v0, :cond_0
/* .line 184 */
(( com.miui.server.smartpower.AppAudioResource$AudioRecord ) v0 ).onRecorderEvent ( p2, p3 ); // invoke-virtual {v0, p2, p3}, Lcom/miui/server/smartpower/AppAudioResource$AudioRecord;->onRecorderEvent(II)V
/* .line 186 */
} // :cond_0
return;
} // .end method
public void onRecorderRlease ( Integer p0, Integer p1 ) {
/* .locals 1 */
/* .param p1, "uid" # I */
/* .param p2, "riid" # I */
/* .line 175 */
/* invoke-direct {p0, p1, p2}, Lcom/miui/server/smartpower/AppAudioResource;->getAudioRecord(II)Lcom/miui/server/smartpower/AppAudioResource$AudioRecord; */
/* .line 176 */
/* .local v0, "record":Lcom/miui/server/smartpower/AppAudioResource$AudioRecord; */
if ( v0 != null) { // if-eqz v0, :cond_0
/* .line 177 */
(( com.miui.server.smartpower.AppAudioResource$AudioRecord ) v0 ).onRecorderRlease ( p2 ); // invoke-virtual {v0, p2}, Lcom/miui/server/smartpower/AppAudioResource$AudioRecord;->onRecorderRlease(I)V
/* .line 179 */
} // :cond_0
return;
} // .end method
public void onRecorderTrack ( Integer p0, Integer p1, Integer p2 ) {
/* .locals 1 */
/* .param p1, "uid" # I */
/* .param p2, "pid" # I */
/* .param p3, "riid" # I */
/* .line 170 */
/* invoke-direct {p0, p1, p2}, Lcom/miui/server/smartpower/AppAudioResource;->getOrCreateAudioRecord(II)Lcom/miui/server/smartpower/AppAudioResource$AudioRecord; */
/* .line 171 */
/* .local v0, "record":Lcom/miui/server/smartpower/AppAudioResource$AudioRecord; */
(( com.miui.server.smartpower.AppAudioResource$AudioRecord ) v0 ).onRecorderTrack ( p3 ); // invoke-virtual {v0, p3}, Lcom/miui/server/smartpower/AppAudioResource$AudioRecord;->onRecorderTrack(I)V
/* .line 172 */
return;
} // .end method
public void playbackStateChanged ( Integer p0, Integer p1, Integer p2, Integer p3 ) {
/* .locals 1 */
/* .param p1, "uid" # I */
/* .param p2, "pid" # I */
/* .param p3, "oldState" # I */
/* .param p4, "newState" # I */
/* .line 132 */
/* invoke-direct {p0, p2}, Lcom/miui/server/smartpower/AppAudioResource;->getAudioRecord(I)Lcom/miui/server/smartpower/AppAudioResource$AudioRecord; */
/* .line 133 */
/* .local v0, "record":Lcom/miui/server/smartpower/AppAudioResource$AudioRecord; */
if ( v0 != null) { // if-eqz v0, :cond_0
/* .line 134 */
(( com.miui.server.smartpower.AppAudioResource$AudioRecord ) v0 ).playbackStateChanged ( p3, p4 ); // invoke-virtual {v0, p3, p4}, Lcom/miui/server/smartpower/AppAudioResource$AudioRecord;->playbackStateChanged(II)V
/* .line 136 */
} // :cond_0
return;
} // .end method
public void recordAudioFocus ( Integer p0, Integer p1, java.lang.String p2, Boolean p3 ) {
/* .locals 1 */
/* .param p1, "uid" # I */
/* .param p2, "pid" # I */
/* .param p3, "clientId" # Ljava/lang/String; */
/* .param p4, "request" # Z */
/* .line 139 */
/* invoke-direct {p0, p1, p2}, Lcom/miui/server/smartpower/AppAudioResource;->getOrCreateAudioRecord(II)Lcom/miui/server/smartpower/AppAudioResource$AudioRecord; */
/* .line 140 */
/* .local v0, "record":Lcom/miui/server/smartpower/AppAudioResource$AudioRecord; */
(( com.miui.server.smartpower.AppAudioResource$AudioRecord ) v0 ).recordAudioFocus ( p4, p3 ); // invoke-virtual {v0, p4, p3}, Lcom/miui/server/smartpower/AppAudioResource$AudioRecord;->recordAudioFocus(ZLjava/lang/String;)V
/* .line 141 */
return;
} // .end method
public void recordAudioFocusLoss ( Integer p0, java.lang.String p1, Integer p2 ) {
/* .locals 1 */
/* .param p1, "uid" # I */
/* .param p2, "clientId" # Ljava/lang/String; */
/* .param p3, "focusLoss" # I */
/* .line 144 */
/* invoke-direct {p0, p1, p2}, Lcom/miui/server/smartpower/AppAudioResource;->getAudioRecord(ILjava/lang/String;)Lcom/miui/server/smartpower/AppAudioResource$AudioRecord; */
/* .line 145 */
/* .local v0, "record":Lcom/miui/server/smartpower/AppAudioResource$AudioRecord; */
if ( v0 != null) { // if-eqz v0, :cond_0
/* .line 146 */
(( com.miui.server.smartpower.AppAudioResource$AudioRecord ) v0 ).recordAudioFocusLoss ( p2, p3 ); // invoke-virtual {v0, p2, p3}, Lcom/miui/server/smartpower/AppAudioResource$AudioRecord;->recordAudioFocusLoss(Ljava/lang/String;I)V
/* .line 148 */
} // :cond_0
return;
} // .end method
public void releaseAppPowerResource ( Integer p0 ) {
/* .locals 4 */
/* .param p1, "uid" # I */
/* .line 116 */
v0 = this.mActivePidsMap;
/* monitor-enter v0 */
/* .line 117 */
int v1 = 0; // const/4 v1, 0x0
/* .local v1, "i":I */
} // :goto_0
try { // :try_start_0
v2 = this.mActivePidsMap;
v2 = (( android.util.SparseArray ) v2 ).size ( ); // invoke-virtual {v2}, Landroid/util/SparseArray;->size()I
/* if-ge v1, v2, :cond_1 */
/* .line 118 */
v2 = this.mActivePidsMap;
(( android.util.SparseArray ) v2 ).valueAt ( v1 ); // invoke-virtual {v2, v1}, Landroid/util/SparseArray;->valueAt(I)Ljava/lang/Object;
/* check-cast v2, Lcom/miui/server/smartpower/AppAudioResource$AudioRecord; */
/* .line 119 */
/* .local v2, "record":Lcom/miui/server/smartpower/AppAudioResource$AudioRecord; */
if ( v2 != null) { // if-eqz v2, :cond_0
/* iget v3, v2, Lcom/miui/server/smartpower/AppAudioResource$AudioRecord;->mOwnerUid:I */
/* if-ne v3, p1, :cond_0 */
/* iget-boolean v3, v2, Lcom/miui/server/smartpower/AppAudioResource$AudioRecord;->mActive:Z */
/* if-nez v3, :cond_0 */
/* .line 120 */
com.miui.server.smartpower.AppAudioResource$AudioRecord .-$$Nest$mpauseZeroAudioTrack ( v2 );
/* .line 117 */
} // .end local v2 # "record":Lcom/miui/server/smartpower/AppAudioResource$AudioRecord;
} // :cond_0
/* add-int/lit8 v1, v1, 0x1 */
/* .line 123 */
} // .end local v1 # "i":I
} // :cond_1
/* monitor-exit v0 */
/* .line 124 */
return;
/* .line 123 */
/* :catchall_0 */
/* move-exception v1 */
/* monitor-exit v0 */
/* :try_end_0 */
/* .catchall {:try_start_0 ..:try_end_0} :catchall_0 */
/* throw v1 */
} // .end method
public void reportTrackStatus ( Integer p0, Integer p1, Integer p2, Boolean p3 ) {
/* .locals 1 */
/* .param p1, "uid" # I */
/* .param p2, "pid" # I */
/* .param p3, "sessionId" # I */
/* .param p4, "isMuted" # Z */
/* .line 189 */
/* invoke-direct {p0, p2}, Lcom/miui/server/smartpower/AppAudioResource;->getAudioRecord(I)Lcom/miui/server/smartpower/AppAudioResource$AudioRecord; */
/* .line 190 */
/* .local v0, "record":Lcom/miui/server/smartpower/AppAudioResource$AudioRecord; */
if ( v0 != null) { // if-eqz v0, :cond_0
/* .line 191 */
(( com.miui.server.smartpower.AppAudioResource$AudioRecord ) v0 ).reportTrackStatus ( p3, p4 ); // invoke-virtual {v0, p3, p4}, Lcom/miui/server/smartpower/AppAudioResource$AudioRecord;->reportTrackStatus(IZ)V
/* .line 193 */
} // :cond_0
return;
} // .end method
public void resumeAppPowerResource ( Integer p0 ) {
/* .locals 0 */
/* .param p1, "uid" # I */
/* .line 129 */
return;
} // .end method
public void uidAudioStatusChanged ( Integer p0, Boolean p1 ) {
/* .locals 4 */
/* .param p1, "uid" # I */
/* .param p2, "active" # Z */
/* .line 196 */
/* if-nez p2, :cond_2 */
/* .line 197 */
v0 = this.mActivePidsMap;
/* monitor-enter v0 */
/* .line 198 */
int v1 = 0; // const/4 v1, 0x0
/* .local v1, "i":I */
} // :goto_0
try { // :try_start_0
v2 = this.mActivePidsMap;
v2 = (( android.util.SparseArray ) v2 ).size ( ); // invoke-virtual {v2}, Landroid/util/SparseArray;->size()I
/* if-ge v1, v2, :cond_1 */
/* .line 199 */
v2 = this.mActivePidsMap;
(( android.util.SparseArray ) v2 ).valueAt ( v1 ); // invoke-virtual {v2, v1}, Landroid/util/SparseArray;->valueAt(I)Ljava/lang/Object;
/* check-cast v2, Lcom/miui/server/smartpower/AppAudioResource$AudioRecord; */
/* .line 200 */
/* .local v2, "record":Lcom/miui/server/smartpower/AppAudioResource$AudioRecord; */
if ( v2 != null) { // if-eqz v2, :cond_0
/* iget v3, v2, Lcom/miui/server/smartpower/AppAudioResource$AudioRecord;->mOwnerUid:I */
/* if-ne v3, p1, :cond_0 */
/* .line 201 */
(( com.miui.server.smartpower.AppAudioResource$AudioRecord ) v2 ).uidAudioStatusChanged ( p2 ); // invoke-virtual {v2, p2}, Lcom/miui/server/smartpower/AppAudioResource$AudioRecord;->uidAudioStatusChanged(Z)V
/* .line 198 */
} // .end local v2 # "record":Lcom/miui/server/smartpower/AppAudioResource$AudioRecord;
} // :cond_0
/* add-int/lit8 v1, v1, 0x1 */
/* .line 204 */
} // .end local v1 # "i":I
} // :cond_1
/* monitor-exit v0 */
/* :catchall_0 */
/* move-exception v1 */
/* monitor-exit v0 */
/* :try_end_0 */
/* .catchall {:try_start_0 ..:try_end_0} :catchall_0 */
/* throw v1 */
/* .line 206 */
} // :cond_2
} // :goto_1
return;
} // .end method
public void uidVideoStatusChanged ( Integer p0, Boolean p1 ) {
/* .locals 0 */
/* .param p1, "uid" # I */
/* .param p2, "active" # Z */
/* .line 208 */
return;
} // .end method
