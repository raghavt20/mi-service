.class Lcom/miui/server/smartpower/AppBluetoothResource$BleRecord$ProfileRecord;
.super Ljava/lang/Object;
.source "AppBluetoothResource.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/miui/server/smartpower/AppBluetoothResource$BleRecord;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = "ProfileRecord"
.end annotation


# instance fields
.field mBleType:I

.field private final mFlags:Landroid/util/ArraySet;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/util/ArraySet<",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation
.end field

.field final synthetic this$1:Lcom/miui/server/smartpower/AppBluetoothResource$BleRecord;


# direct methods
.method constructor <init>(Lcom/miui/server/smartpower/AppBluetoothResource$BleRecord;I)V
    .locals 1
    .param p1, "this$1"    # Lcom/miui/server/smartpower/AppBluetoothResource$BleRecord;
    .param p2, "bleType"    # I

    .line 183
    iput-object p1, p0, Lcom/miui/server/smartpower/AppBluetoothResource$BleRecord$ProfileRecord;->this$1:Lcom/miui/server/smartpower/AppBluetoothResource$BleRecord;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 181
    new-instance v0, Landroid/util/ArraySet;

    invoke-direct {v0}, Landroid/util/ArraySet;-><init>()V

    iput-object v0, p0, Lcom/miui/server/smartpower/AppBluetoothResource$BleRecord$ProfileRecord;->mFlags:Landroid/util/ArraySet;

    .line 184
    iput p2, p0, Lcom/miui/server/smartpower/AppBluetoothResource$BleRecord$ProfileRecord;->mBleType:I

    .line 185
    return-void
.end method


# virtual methods
.method bluetoothConnect(I)V
    .locals 2
    .param p1, "flag"    # I

    .line 188
    iget-object v0, p0, Lcom/miui/server/smartpower/AppBluetoothResource$BleRecord$ProfileRecord;->mFlags:Landroid/util/ArraySet;

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/util/ArraySet;->add(Ljava/lang/Object;)Z

    .line 189
    return-void
.end method

.method bluetoothDisconnect(I)V
    .locals 2
    .param p1, "flag"    # I

    .line 192
    iget-object v0, p0, Lcom/miui/server/smartpower/AppBluetoothResource$BleRecord$ProfileRecord;->mFlags:Landroid/util/ArraySet;

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/util/ArraySet;->remove(Ljava/lang/Object;)Z

    .line 193
    return-void
.end method

.method isActive()Z
    .locals 1

    .line 196
    iget-object v0, p0, Lcom/miui/server/smartpower/AppBluetoothResource$BleRecord$ProfileRecord;->mFlags:Landroid/util/ArraySet;

    invoke-virtual {v0}, Landroid/util/ArraySet;->size()I

    move-result v0

    if-lez v0, :cond_0

    const/4 v0, 0x1

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    :goto_0
    return v0
.end method
