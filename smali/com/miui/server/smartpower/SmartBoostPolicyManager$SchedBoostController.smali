.class final Lcom/miui/server/smartpower/SmartBoostPolicyManager$SchedBoostController;
.super Ljava/lang/Object;
.source "SmartBoostPolicyManager.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/miui/server/smartpower/SmartBoostPolicyManager;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x12
    name = "SchedBoostController"
.end annotation


# static fields
.field public static MULTI_SENCE_ENABLE:Z = false

.field private static final SCHED_BOOST_DEFAULT_DURATION:J = 0x7d0L

.field private static final SCHED_BOOST_STOP_DURATION:J


# instance fields
.field private final mSchedBoostManagerInternal:Lcom/miui/server/rtboost/SchedBoostManagerInternal;

.field final synthetic this$0:Lcom/miui/server/smartpower/SmartBoostPolicyManager;


# direct methods
.method static constructor <clinit>()V
    .locals 2

    .line 81
    const-string v0, "persist.multisence.enable"

    const/4 v1, 0x0

    invoke-static {v0, v1}, Landroid/os/SystemProperties;->getBoolean(Ljava/lang/String;Z)Z

    move-result v0

    sput-boolean v0, Lcom/miui/server/smartpower/SmartBoostPolicyManager$SchedBoostController;->MULTI_SENCE_ENABLE:Z

    return-void
.end method

.method public constructor <init>(Lcom/miui/server/smartpower/SmartBoostPolicyManager;)V
    .locals 0

    .line 90
    iput-object p1, p0, Lcom/miui/server/smartpower/SmartBoostPolicyManager$SchedBoostController;->this$0:Lcom/miui/server/smartpower/SmartBoostPolicyManager;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 91
    const-class p1, Lcom/miui/server/rtboost/SchedBoostManagerInternal;

    invoke-static {p1}, Lcom/android/server/LocalServices;->getService(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Lcom/miui/server/rtboost/SchedBoostManagerInternal;

    iput-object p1, p0, Lcom/miui/server/smartpower/SmartBoostPolicyManager$SchedBoostController;->mSchedBoostManagerInternal:Lcom/miui/server/rtboost/SchedBoostManagerInternal;

    .line 92
    return-void
.end method


# virtual methods
.method public beginSchedThreads([IIJI)V
    .locals 8
    .param p1, "tids"    # [I
    .param p2, "callingPid"    # I
    .param p3, "duration"    # J
    .param p5, "boostType"    # I

    .line 100
    invoke-virtual {p0}, Lcom/miui/server/smartpower/SmartBoostPolicyManager$SchedBoostController;->isEanble()Z

    move-result v0

    if-nez v0, :cond_0

    .line 101
    return-void

    .line 103
    :cond_0
    invoke-virtual {p0, p5}, Lcom/miui/server/smartpower/SmartBoostPolicyManager$SchedBoostController;->getScheduleBoostMode(I)I

    move-result v0

    .line 104
    .local v0, "schedBoostMode":I
    iget-object v1, p0, Lcom/miui/server/smartpower/SmartBoostPolicyManager$SchedBoostController;->this$0:Lcom/miui/server/smartpower/SmartBoostPolicyManager;

    invoke-static {v1, p2}, Lcom/miui/server/smartpower/SmartBoostPolicyManager;->-$$Nest$mgetProcessNameByPid(Lcom/miui/server/smartpower/SmartBoostPolicyManager;I)Ljava/lang/String;

    move-result-object v7

    .line 105
    .local v7, "procName":Ljava/lang/String;
    sget-boolean v1, Lcom/miui/server/smartpower/SmartBoostPolicyManager;->DEBUG:Z

    if-eqz v1, :cond_1

    .line 106
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "beginSchedThreads: tids="

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ", duration="

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p3, p4}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ", boostType="

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lcom/miui/server/smartpower/SmartBoostPolicyManager$SchedBoostController;->this$0:Lcom/miui/server/smartpower/SmartBoostPolicyManager;

    .line 107
    invoke-static {v2, p5}, Lcom/miui/server/smartpower/SmartBoostPolicyManager;->-$$Nest$mboostTypeToString(Lcom/miui/server/smartpower/SmartBoostPolicyManager;I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ", schedBoostMode="

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ", procName="

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    .line 106
    const-string v2, "SmartPower.BoostPolicy"

    invoke-static {v2, v1}, Landroid/util/Slog;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 110
    :cond_1
    iget-object v1, p0, Lcom/miui/server/smartpower/SmartBoostPolicyManager$SchedBoostController;->mSchedBoostManagerInternal:Lcom/miui/server/rtboost/SchedBoostManagerInternal;

    move-object v2, p1

    move-wide v3, p3

    move-object v5, v7

    move v6, v0

    invoke-interface/range {v1 .. v6}, Lcom/miui/server/rtboost/SchedBoostManagerInternal;->beginSchedThreads([IJLjava/lang/String;I)V

    .line 111
    return-void
.end method

.method public getScheduleBoostMode(I)I
    .locals 1
    .param p1, "boostType"    # I

    .line 114
    packed-switch p1, :pswitch_data_0

    .line 116
    const/4 v0, 0x0

    goto :goto_0

    .line 115
    :pswitch_0
    const/16 v0, 0x9

    .line 114
    :goto_0
    return v0

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
    .end packed-switch
.end method

.method public isEanble()Z
    .locals 1

    .line 96
    sget-boolean v0, Lcom/miui/server/smartpower/SmartBoostPolicyManager$SchedBoostController;->MULTI_SENCE_ENABLE:Z

    xor-int/lit8 v0, v0, 0x1

    return v0
.end method
