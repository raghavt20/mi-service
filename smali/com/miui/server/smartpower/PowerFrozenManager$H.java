class com.miui.server.smartpower.PowerFrozenManager$H extends android.os.Handler {
	 /* .source "PowerFrozenManager.java" */
	 /* # annotations */
	 /* .annotation system Ldalvik/annotation/EnclosingClass; */
	 /* value = Lcom/miui/server/smartpower/PowerFrozenManager; */
} // .end annotation
/* .annotation system Ldalvik/annotation/InnerClass; */
/* accessFlags = 0x2 */
/* name = "H" */
} // .end annotation
/* # instance fields */
final com.miui.server.smartpower.PowerFrozenManager this$0; //synthetic
/* # direct methods */
public com.miui.server.smartpower.PowerFrozenManager$H ( ) {
/* .locals 0 */
/* .param p2, "looper" # Landroid/os/Looper; */
/* .line 535 */
this.this$0 = p1;
/* .line 536 */
/* invoke-direct {p0, p2}, Landroid/os/Handler;-><init>(Landroid/os/Looper;)V */
/* .line 537 */
return;
} // .end method
/* # virtual methods */
public void handleMessage ( android.os.Message p0 ) {
/* .locals 5 */
/* .param p1, "msg" # Landroid/os/Message; */
/* .line 541 */
/* invoke-super {p0, p1}, Landroid/os/Handler;->handleMessage(Landroid/os/Message;)V */
/* .line 542 */
/* iget v0, p1, Landroid/os/Message;->what:I */
/* packed-switch v0, :pswitch_data_0 */
/* .line 545 */
/* :pswitch_0 */
try { // :try_start_0
	 v0 = this.this$0;
	 /* iget v1, p1, Landroid/os/Message;->arg2:I */
	 v0 = 	 (( com.miui.server.smartpower.PowerFrozenManager ) v0 ).isFrozenPid ( v1 ); // invoke-virtual {v0, v1}, Lcom/miui/server/smartpower/PowerFrozenManager;->isFrozenPid(I)Z
	 if ( v0 != null) { // if-eqz v0, :cond_0
		 /* .line 546 */
		 /* iget v0, p1, Landroid/os/Message;->arg1:I */
		 /* .line 547 */
		 /* .local v0, "uid":I */
		 /* iget v1, p1, Landroid/os/Message;->arg2:I */
		 /* .line 548 */
		 /* .local v1, "pid":I */
		 v2 = this.this$0;
		 /* new-instance v3, Ljava/lang/StringBuilder; */
		 /* invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V */
		 final String v4 = "Timeout pid "; // const-string v4, "Timeout pid "
		 (( java.lang.StringBuilder ) v3 ).append ( v4 ); // invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
		 (( java.lang.StringBuilder ) v3 ).append ( v1 ); // invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;
		 (( java.lang.StringBuilder ) v3 ).toString ( ); // invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
		 (( com.miui.server.smartpower.PowerFrozenManager ) v2 ).thawProcess ( v0, v1, v3 ); // invoke-virtual {v2, v0, v1, v3}, Lcom/miui/server/smartpower/PowerFrozenManager;->thawProcess(IILjava/lang/String;)Z
		 /* :try_end_0 */
		 /* .catch Ljava/lang/Exception; {:try_start_0 ..:try_end_0} :catch_0 */
		 /* .line 552 */
	 } // .end local v0 # "uid":I
} // .end local v1 # "pid":I
} // :cond_0
/* .line 550 */
/* :catch_0 */
/* move-exception v0 */
/* .line 551 */
/* .local v0, "e":Ljava/lang/Exception; */
(( java.lang.Exception ) v0 ).printStackTrace ( ); // invoke-virtual {v0}, Ljava/lang/Exception;->printStackTrace()V
/* .line 553 */
} // .end local v0 # "e":Ljava/lang/Exception;
/* nop */
/* .line 557 */
} // :goto_0
return;
/* :pswitch_data_0 */
/* .packed-switch 0x1 */
/* :pswitch_0 */
} // .end packed-switch
} // .end method
