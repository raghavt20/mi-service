class com.miui.server.smartpower.AppDisplayResource$2 extends android.content.BroadcastReceiver {
	 /* .source "AppDisplayResource.java" */
	 /* # annotations */
	 /* .annotation system Ldalvik/annotation/EnclosingClass; */
	 /* value = Lcom/miui/server/smartpower/AppDisplayResource; */
} // .end annotation
/* .annotation system Ldalvik/annotation/InnerClass; */
/* accessFlags = 0x0 */
/* name = null */
} // .end annotation
/* # instance fields */
final com.miui.server.smartpower.AppDisplayResource this$0; //synthetic
/* # direct methods */
 com.miui.server.smartpower.AppDisplayResource$2 ( ) {
/* .locals 0 */
/* .param p1, "this$0" # Lcom/miui/server/smartpower/AppDisplayResource; */
/* .line 153 */
this.this$0 = p1;
/* invoke-direct {p0}, Landroid/content/BroadcastReceiver;-><init>()V */
return;
} // .end method
/* # virtual methods */
public void onReceive ( android.content.Context p0, android.content.Intent p1 ) {
/* .locals 6 */
/* .param p1, "context" # Landroid/content/Context; */
/* .param p2, "intent" # Landroid/content/Intent; */
/* .line 156 */
(( android.content.Intent ) p2 ).getAction ( ); // invoke-virtual {p2}, Landroid/content/Intent;->getAction()Ljava/lang/String;
/* .line 157 */
/* .local v0, "action":Ljava/lang/String; */
final String v1 = "android.net.wifi.p2p.CONNECTION_STATE_CHANGE"; // const-string v1, "android.net.wifi.p2p.CONNECTION_STATE_CHANGE"
v1 = (( java.lang.String ) v0 ).equals ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z
if ( v1 != null) { // if-eqz v1, :cond_2
	 /* .line 158 */
	 final String v1 = "networkInfo"; // const-string v1, "networkInfo"
	 (( android.content.Intent ) p2 ).getParcelableExtra ( v1 ); // invoke-virtual {p2, v1}, Landroid/content/Intent;->getParcelableExtra(Ljava/lang/String;)Landroid/os/Parcelable;
	 /* check-cast v1, Landroid/net/NetworkInfo; */
	 /* .line 160 */
	 /* .local v1, "networkInfo":Landroid/net/NetworkInfo; */
	 if ( v1 != null) { // if-eqz v1, :cond_2
		 /* .line 161 */
		 /* sget-boolean v2, Lcom/miui/server/smartpower/AppPowerResource;->DEBUG:Z */
		 if ( v2 != null) { // if-eqz v2, :cond_0
			 /* .line 162 */
			 final String v2 = "SmartPower.AppResource"; // const-string v2, "SmartPower.AppResource"
			 /* new-instance v3, Ljava/lang/StringBuilder; */
			 /* invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V */
			 final String v4 = "Received WIFI_P2P_CONNECTION_CHANGED_ACTION: networkInfo="; // const-string v4, "Received WIFI_P2P_CONNECTION_CHANGED_ACTION: networkInfo="
			 (( java.lang.StringBuilder ) v3 ).append ( v4 ); // invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
			 (( java.lang.StringBuilder ) v3 ).append ( v1 ); // invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;
			 (( java.lang.StringBuilder ) v3 ).toString ( ); // invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
			 android.util.Slog .d ( v2,v3 );
			 /* .line 165 */
		 } // :cond_0
		 v2 = 		 (( android.net.NetworkInfo ) v1 ).isConnected ( ); // invoke-virtual {v1}, Landroid/net/NetworkInfo;->isConnected()Z
		 /* .line 166 */
		 /* .local v2, "connected":Z */
		 v3 = this.this$0;
		 v3 = 		 com.miui.server.smartpower.AppDisplayResource .-$$Nest$fgetmWifiP2pConnected ( v3 );
		 /* if-eq v2, v3, :cond_2 */
		 /* .line 167 */
		 v3 = this.this$0;
		 com.miui.server.smartpower.AppDisplayResource .-$$Nest$fputmWifiP2pConnected ( v3,v2 );
		 /* .line 168 */
		 v3 = this.this$0;
		 com.miui.server.smartpower.AppDisplayResource .-$$Nest$fgetmDisplayRecordMap ( v3 );
		 /* monitor-enter v3 */
		 /* .line 169 */
		 try { // :try_start_0
			 v4 = this.this$0;
			 com.miui.server.smartpower.AppDisplayResource .-$$Nest$fgetmDisplayRecordMap ( v4 );
			 (( android.util.ArrayMap ) v4 ).values ( ); // invoke-virtual {v4}, Landroid/util/ArrayMap;->values()Ljava/util/Collection;
		 v5 = 		 } // :goto_0
		 if ( v5 != null) { // if-eqz v5, :cond_1
			 /* check-cast v5, Lcom/miui/server/smartpower/AppDisplayResource$DisplayRecord; */
			 /* .line 170 */
			 /* .local v5, "record":Lcom/miui/server/smartpower/AppDisplayResource$DisplayRecord; */
			 com.miui.server.smartpower.AppDisplayResource$DisplayRecord .-$$Nest$mupdateCurrentStatus ( v5 );
			 /* .line 171 */
		 } // .end local v5 # "record":Lcom/miui/server/smartpower/AppDisplayResource$DisplayRecord;
		 /* .line 172 */
	 } // :cond_1
	 /* monitor-exit v3 */
	 /* :catchall_0 */
	 /* move-exception v4 */
	 /* monitor-exit v3 */
	 /* :try_end_0 */
	 /* .catchall {:try_start_0 ..:try_end_0} :catchall_0 */
	 /* throw v4 */
	 /* .line 176 */
} // .end local v1 # "networkInfo":Landroid/net/NetworkInfo;
} // .end local v2 # "connected":Z
} // :cond_2
} // :goto_1
return;
} // .end method
