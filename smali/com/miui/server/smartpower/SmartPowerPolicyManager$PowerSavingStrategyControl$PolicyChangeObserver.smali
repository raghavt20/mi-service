.class Lcom/miui/server/smartpower/SmartPowerPolicyManager$PowerSavingStrategyControl$PolicyChangeObserver;
.super Landroid/database/ContentObserver;
.source "SmartPowerPolicyManager.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/miui/server/smartpower/SmartPowerPolicyManager$PowerSavingStrategyControl;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "PolicyChangeObserver"
.end annotation


# instance fields
.field final synthetic this$1:Lcom/miui/server/smartpower/SmartPowerPolicyManager$PowerSavingStrategyControl;


# direct methods
.method public constructor <init>(Lcom/miui/server/smartpower/SmartPowerPolicyManager$PowerSavingStrategyControl;Landroid/os/Handler;)V
    .locals 0
    .param p2, "handler"    # Landroid/os/Handler;

    .line 1690
    iput-object p1, p0, Lcom/miui/server/smartpower/SmartPowerPolicyManager$PowerSavingStrategyControl$PolicyChangeObserver;->this$1:Lcom/miui/server/smartpower/SmartPowerPolicyManager$PowerSavingStrategyControl;

    .line 1691
    invoke-direct {p0, p2}, Landroid/database/ContentObserver;-><init>(Landroid/os/Handler;)V

    .line 1692
    return-void
.end method


# virtual methods
.method public onChange(ZLandroid/net/Uri;)V
    .locals 8
    .param p1, "selfChange"    # Z
    .param p2, "uri"    # Landroid/net/Uri;

    .line 1696
    invoke-super {p0, p1, p2}, Landroid/database/ContentObserver;->onChange(ZLandroid/net/Uri;)V

    .line 1697
    iget-object v0, p0, Lcom/miui/server/smartpower/SmartPowerPolicyManager$PowerSavingStrategyControl$PolicyChangeObserver;->this$1:Lcom/miui/server/smartpower/SmartPowerPolicyManager$PowerSavingStrategyControl;

    iget-object v0, v0, Lcom/miui/server/smartpower/SmartPowerPolicyManager$PowerSavingStrategyControl;->this$0:Lcom/miui/server/smartpower/SmartPowerPolicyManager;

    invoke-static {v0}, Lcom/miui/server/smartpower/SmartPowerPolicyManager;->-$$Nest$fgetmContext(Lcom/miui/server/smartpower/SmartPowerPolicyManager;)Landroid/content/Context;

    move-result-object v0

    sget-object v1, Landroid/os/UserHandle;->SYSTEM:Landroid/os/UserHandle;

    invoke-virtual {v0, v1}, Landroid/content/Context;->getContentResolverForUser(Landroid/os/UserHandle;)Landroid/content/ContentResolver;

    move-result-object v0

    .line 1698
    .local v0, "cr":Landroid/content/ContentResolver;
    const/4 v1, 0x0

    .line 1700
    .local v1, "cursor":Landroid/database/Cursor;
    const/4 v4, 0x0

    const/4 v5, 0x0

    const/4 v6, 0x0

    const/4 v7, 0x0

    move-object v2, v0

    move-object v3, p2

    :try_start_0
    invoke-virtual/range {v2 .. v7}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v2
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-object v1, v2

    .line 1701
    if-nez v1, :cond_1

    .line 1720
    if-eqz v1, :cond_0

    .line 1721
    invoke-interface {v1}, Landroid/database/Cursor;->close()V

    .line 1702
    :cond_0
    return-void

    .line 1704
    :cond_1
    :try_start_1
    invoke-interface {v1}, Landroid/database/Cursor;->getCount()I

    move-result v2

    if-lez v2, :cond_3

    .line 1705
    const-string v2, "pkgName"

    invoke-interface {v1, v2}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v2

    .line 1706
    .local v2, "pkgIndex":I
    const-string v3, "bgControl"

    invoke-interface {v1, v3}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v3

    .line 1707
    .local v3, "bgControlIndex":I
    invoke-interface {v1}, Landroid/database/Cursor;->moveToFirst()Z

    .line 1708
    invoke-interface {v1, v3}, Landroid/database/Cursor;->isNull(I)Z

    move-result v4

    if-nez v4, :cond_3

    .line 1709
    invoke-interface {v1, v2}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v4

    .line 1710
    .local v4, "pkg":Ljava/lang/String;
    invoke-interface {v1, v3}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v5

    .line 1711
    .local v5, "bgControl":Ljava/lang/String;
    const-string v6, "noRestrict"

    invoke-virtual {v6, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-eqz v6, :cond_2

    .line 1712
    iget-object v6, p0, Lcom/miui/server/smartpower/SmartPowerPolicyManager$PowerSavingStrategyControl$PolicyChangeObserver;->this$1:Lcom/miui/server/smartpower/SmartPowerPolicyManager$PowerSavingStrategyControl;

    invoke-static {v6}, Lcom/miui/server/smartpower/SmartPowerPolicyManager$PowerSavingStrategyControl;->-$$Nest$fgetmNoRestrictApps(Lcom/miui/server/smartpower/SmartPowerPolicyManager$PowerSavingStrategyControl;)Landroid/util/ArraySet;

    move-result-object v6

    invoke-virtual {v6, v4}, Landroid/util/ArraySet;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 1714
    :cond_2
    iget-object v6, p0, Lcom/miui/server/smartpower/SmartPowerPolicyManager$PowerSavingStrategyControl$PolicyChangeObserver;->this$1:Lcom/miui/server/smartpower/SmartPowerPolicyManager$PowerSavingStrategyControl;

    invoke-static {v6}, Lcom/miui/server/smartpower/SmartPowerPolicyManager$PowerSavingStrategyControl;->-$$Nest$fgetmNoRestrictApps(Lcom/miui/server/smartpower/SmartPowerPolicyManager$PowerSavingStrategyControl;)Landroid/util/ArraySet;

    move-result-object v6

    invoke-virtual {v6, v4}, Landroid/util/ArraySet;->remove(Ljava/lang/Object;)Z
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_0
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 1720
    .end local v2    # "pkgIndex":I
    .end local v3    # "bgControlIndex":I
    .end local v4    # "pkg":Ljava/lang/String;
    .end local v5    # "bgControl":Ljava/lang/String;
    :cond_3
    :goto_0
    if-eqz v1, :cond_5

    .line 1721
    :goto_1
    invoke-interface {v1}, Landroid/database/Cursor;->close()V

    goto :goto_2

    .line 1720
    :catchall_0
    move-exception v2

    if-eqz v1, :cond_4

    .line 1721
    invoke-interface {v1}, Landroid/database/Cursor;->close()V

    .line 1723
    :cond_4
    throw v2

    .line 1718
    :catch_0
    move-exception v2

    .line 1720
    if-eqz v1, :cond_5

    .line 1721
    goto :goto_1

    .line 1725
    :cond_5
    :goto_2
    return-void
.end method
