.class public Lcom/miui/server/smartpower/PowerFrozenManager;
.super Ljava/lang/Object;
.source "PowerFrozenManager.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/miui/server/smartpower/PowerFrozenManager$FrozenInfo;,
        Lcom/miui/server/smartpower/PowerFrozenManager$H;,
        Lcom/miui/server/smartpower/PowerFrozenManager$IFrozenReportCallback;
    }
.end annotation


# static fields
.field private static final DEBUG:Z

.field private static final HISTORY_SIZE:I = 0x1000

.field private static final MSG_ASYNC_THAW_PID:I = 0x1

.field public static final TAG:Ljava/lang/String; = "PowerFrozen"

.field private static sEnable:Z


# instance fields
.field private final gzInternal:Lcom/miui/server/greeze/GreezeManagerInternal;

.field private final mAllFrozenPids:Landroid/util/SparseArray;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/util/SparseArray<",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation
.end field

.field private mFrozenCallbacks:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List<",
            "Lcom/miui/server/smartpower/PowerFrozenManager$IFrozenReportCallback;",
            ">;"
        }
    .end annotation
.end field

.field private mFrozenHistory:[Lcom/miui/server/smartpower/PowerFrozenManager$FrozenInfo;

.field private final mFrozenList:Landroid/util/SparseArray;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/util/SparseArray<",
            "Lcom/miui/server/smartpower/PowerFrozenManager$FrozenInfo;",
            ">;"
        }
    .end annotation
.end field

.field private mHandler:Lcom/miui/server/smartpower/PowerFrozenManager$H;

.field private mHandlerTh:Landroid/os/HandlerThread;

.field private mHistoryIndexNext:I


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .line 23
    sget-boolean v0, Lcom/miui/app/smartpower/SmartPowerSettings;->DEBUG_ALL:Z

    sput-boolean v0, Lcom/miui/server/smartpower/PowerFrozenManager;->DEBUG:Z

    .line 29
    sget-boolean v0, Lcom/miui/app/smartpower/SmartPowerSettings;->PROP_FROZEN_ENABLE:Z

    sput-boolean v0, Lcom/miui/server/smartpower/PowerFrozenManager;->sEnable:Z

    return-void
.end method

.method public constructor <init>()V
    .locals 3

    .line 59
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 35
    new-instance v0, Landroid/util/SparseArray;

    invoke-direct {v0}, Landroid/util/SparseArray;-><init>()V

    iput-object v0, p0, Lcom/miui/server/smartpower/PowerFrozenManager;->mAllFrozenPids:Landroid/util/SparseArray;

    .line 40
    new-instance v0, Landroid/util/SparseArray;

    invoke-direct {v0}, Landroid/util/SparseArray;-><init>()V

    iput-object v0, p0, Lcom/miui/server/smartpower/PowerFrozenManager;->mFrozenList:Landroid/util/SparseArray;

    .line 47
    const/16 v0, 0x1000

    new-array v0, v0, [Lcom/miui/server/smartpower/PowerFrozenManager$FrozenInfo;

    iput-object v0, p0, Lcom/miui/server/smartpower/PowerFrozenManager;->mFrozenHistory:[Lcom/miui/server/smartpower/PowerFrozenManager$FrozenInfo;

    .line 48
    const/4 v0, 0x0

    iput v0, p0, Lcom/miui/server/smartpower/PowerFrozenManager;->mHistoryIndexNext:I

    .line 53
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/miui/server/smartpower/PowerFrozenManager;->mFrozenCallbacks:Ljava/util/List;

    .line 56
    new-instance v0, Landroid/os/HandlerThread;

    const-string v1, "PowerFrozenTh"

    const/4 v2, -0x2

    invoke-direct {v0, v1, v2}, Landroid/os/HandlerThread;-><init>(Ljava/lang/String;I)V

    iput-object v0, p0, Lcom/miui/server/smartpower/PowerFrozenManager;->mHandlerTh:Landroid/os/HandlerThread;

    .line 60
    invoke-virtual {v0}, Landroid/os/HandlerThread;->start()V

    .line 61
    new-instance v0, Lcom/miui/server/smartpower/PowerFrozenManager$H;

    iget-object v1, p0, Lcom/miui/server/smartpower/PowerFrozenManager;->mHandlerTh:Landroid/os/HandlerThread;

    invoke-virtual {v1}, Landroid/os/HandlerThread;->getLooper()Landroid/os/Looper;

    move-result-object v1

    invoke-direct {v0, p0, v1}, Lcom/miui/server/smartpower/PowerFrozenManager$H;-><init>(Lcom/miui/server/smartpower/PowerFrozenManager;Landroid/os/Looper;)V

    iput-object v0, p0, Lcom/miui/server/smartpower/PowerFrozenManager;->mHandler:Lcom/miui/server/smartpower/PowerFrozenManager$H;

    .line 62
    iget-object v0, p0, Lcom/miui/server/smartpower/PowerFrozenManager;->mHandlerTh:Landroid/os/HandlerThread;

    .line 63
    invoke-virtual {v0}, Landroid/os/HandlerThread;->getThreadId()I

    move-result v0

    .line 62
    const/4 v1, 0x1

    invoke-static {v0, v1}, Landroid/os/Process;->setThreadGroupAndCpuset(II)V

    .line 64
    invoke-static {}, Lcom/miui/server/greeze/GreezeManagerInternal;->getInstance()Lcom/miui/server/greeze/GreezeManagerInternal;

    move-result-object v0

    iput-object v0, p0, Lcom/miui/server/smartpower/PowerFrozenManager;->gzInternal:Lcom/miui/server/greeze/GreezeManagerInternal;

    .line 65
    return-void
.end method

.method private addFrozenPid(IIZ)V
    .locals 4
    .param p1, "uid"    # I
    .param p2, "pid"    # I
    .param p3, "isSmartPower"    # Z

    .line 252
    iget-object v0, p0, Lcom/miui/server/smartpower/PowerFrozenManager;->mAllFrozenPids:Landroid/util/SparseArray;

    monitor-enter v0

    .line 253
    :try_start_0
    sget-boolean v1, Lcom/miui/server/smartpower/PowerFrozenManager;->DEBUG:Z

    if-eqz v1, :cond_0

    const-string v1, "PowerFrozen"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "add p:"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, "/u:"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, " isMy:"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p3}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Slog;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 254
    :cond_0
    iget-object v1, p0, Lcom/miui/server/smartpower/PowerFrozenManager;->mHandler:Lcom/miui/server/smartpower/PowerFrozenManager$H;

    invoke-static {p2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    const/4 v3, 0x1

    invoke-virtual {v1, v3, v2}, Lcom/miui/server/smartpower/PowerFrozenManager$H;->hasMessages(ILjava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_1

    .line 255
    iget-object v1, p0, Lcom/miui/server/smartpower/PowerFrozenManager;->mHandler:Lcom/miui/server/smartpower/PowerFrozenManager$H;

    invoke-static {p2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v1, v3, v2}, Lcom/miui/server/smartpower/PowerFrozenManager$H;->removeMessages(ILjava/lang/Object;)V

    .line 257
    :cond_1
    iget-object v1, p0, Lcom/miui/server/smartpower/PowerFrozenManager;->mAllFrozenPids:Landroid/util/SparseArray;

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v1, p2, v2}, Landroid/util/SparseArray;->put(ILjava/lang/Object;)V

    .line 258
    monitor-exit v0

    .line 259
    return-void

    .line 258
    :catchall_0
    move-exception v1

    monitor-exit v0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v1
.end method

.method private addHistoryInfo(Lcom/miui/server/smartpower/PowerFrozenManager$FrozenInfo;)V
    .locals 3
    .param p1, "info"    # Lcom/miui/server/smartpower/PowerFrozenManager$FrozenInfo;

    .line 350
    iget-object v0, p0, Lcom/miui/server/smartpower/PowerFrozenManager;->mFrozenHistory:[Lcom/miui/server/smartpower/PowerFrozenManager$FrozenInfo;

    iget v1, p0, Lcom/miui/server/smartpower/PowerFrozenManager;->mHistoryIndexNext:I

    aput-object p1, v0, v1

    .line 351
    const/4 v0, 0x1

    const/16 v2, 0x1000

    invoke-static {v1, v0, v2}, Lcom/miui/server/smartpower/PowerFrozenManager;->ringAdvance(III)I

    move-result v0

    iput v0, p0, Lcom/miui/server/smartpower/PowerFrozenManager;->mHistoryIndexNext:I

    .line 352
    return-void
.end method

.method private dumpFrozen(Ljava/io/PrintWriter;[Ljava/lang/String;II)V
    .locals 9
    .param p1, "pw"    # Ljava/io/PrintWriter;
    .param p2, "args"    # [Ljava/lang/String;
    .param p3, "opti"    # I
    .param p4, "dpUid"    # I

    .line 408
    const-string v0, "frozen all: "

    invoke-virtual {p1, v0}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V

    .line 409
    iget-object v0, p0, Lcom/miui/server/smartpower/PowerFrozenManager;->mAllFrozenPids:Landroid/util/SparseArray;

    monitor-enter v0

    .line 410
    :try_start_0
    iget-object v1, p0, Lcom/miui/server/smartpower/PowerFrozenManager;->mAllFrozenPids:Landroid/util/SparseArray;

    invoke-virtual {v1}, Landroid/util/SparseArray;->size()I

    move-result v1

    .line 411
    .local v1, "n":I
    const/4 v2, 0x0

    .local v2, "i":I
    :goto_0
    if-ge v2, v1, :cond_0

    .line 412
    iget-object v3, p0, Lcom/miui/server/smartpower/PowerFrozenManager;->mAllFrozenPids:Landroid/util/SparseArray;

    invoke-virtual {v3, v2}, Landroid/util/SparseArray;->keyAt(I)I

    move-result v3

    .line 413
    .local v3, "pid":I
    iget-object v4, p0, Lcom/miui/server/smartpower/PowerFrozenManager;->mAllFrozenPids:Landroid/util/SparseArray;

    invoke-virtual {v4, v2}, Landroid/util/SparseArray;->valueAt(I)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Ljava/lang/Integer;

    invoke-virtual {v4}, Ljava/lang/Integer;->intValue()I

    move-result v4

    .line 414
    .local v4, "uid":I
    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, " p:"

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, "/u:"

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {p1, v5}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    .line 411
    .end local v3    # "pid":I
    .end local v4    # "uid":I
    add-int/lit8 v2, v2, 0x1

    goto :goto_0

    .line 416
    .end local v1    # "n":I
    .end local v2    # "i":I
    :cond_0
    monitor-exit v0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 417
    const-string v0, ""

    invoke-virtual {p1, v0}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V

    .line 418
    new-instance v0, Ljava/text/SimpleDateFormat;

    const-string v1, "HH:mm:ss.SSS"

    invoke-direct {v0, v1}, Ljava/text/SimpleDateFormat;-><init>(Ljava/lang/String;)V

    move-object v1, v0

    .line 419
    .local v1, "formater":Ljava/text/SimpleDateFormat;
    const-string/jumbo v0, "smart power frozen:"

    invoke-virtual {p1, v0}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V

    .line 420
    iget-object v2, p0, Lcom/miui/server/smartpower/PowerFrozenManager;->mFrozenList:Landroid/util/SparseArray;

    monitor-enter v2

    .line 421
    :try_start_1
    iget-object v0, p0, Lcom/miui/server/smartpower/PowerFrozenManager;->mFrozenList:Landroid/util/SparseArray;

    invoke-virtual {v0}, Landroid/util/SparseArray;->size()I

    move-result v0

    .line 422
    .local v0, "n":I
    const/4 v3, 0x0

    .local v3, "i":I
    :goto_1
    if-ge v3, v0, :cond_3

    .line 423
    iget-object v4, p0, Lcom/miui/server/smartpower/PowerFrozenManager;->mFrozenList:Landroid/util/SparseArray;

    invoke-virtual {v4, v3}, Landroid/util/SparseArray;->valueAt(I)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lcom/miui/server/smartpower/PowerFrozenManager$FrozenInfo;

    .line 424
    .local v4, "info":Lcom/miui/server/smartpower/PowerFrozenManager$FrozenInfo;
    if-eqz p4, :cond_1

    invoke-static {v4}, Lcom/miui/server/smartpower/PowerFrozenManager$FrozenInfo;->-$$Nest$fgetmUid(Lcom/miui/server/smartpower/PowerFrozenManager$FrozenInfo;)I

    move-result v5

    if-eq v5, p4, :cond_1

    .line 425
    goto/16 :goto_3

    .line 427
    :cond_1
    const-string v5, "  "

    invoke-virtual {p1, v5}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    .line 428
    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "#"

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    add-int/lit8 v6, v3, 0x1

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {p1, v5}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    .line 429
    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, " "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {p1, v5}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V

    .line 430
    const/4 v5, 0x0

    .local v5, "index":I
    :goto_2
    invoke-static {v4}, Lcom/miui/server/smartpower/PowerFrozenManager$FrozenInfo;->-$$Nest$fgetmFrozenTimes(Lcom/miui/server/smartpower/PowerFrozenManager$FrozenInfo;)Ljava/util/List;

    move-result-object v6

    invoke-interface {v6}, Ljava/util/List;->size()I

    move-result v6

    if-ge v5, v6, :cond_2

    .line 431
    const-string v6, "    "

    invoke-virtual {p1, v6}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    .line 432
    const-string v6, "fz: "

    invoke-virtual {p1, v6}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    .line 433
    new-instance v6, Ljava/util/Date;

    invoke-static {v4}, Lcom/miui/server/smartpower/PowerFrozenManager$FrozenInfo;->-$$Nest$fgetmFrozenTimes(Lcom/miui/server/smartpower/PowerFrozenManager$FrozenInfo;)Ljava/util/List;

    move-result-object v7

    invoke-interface {v7, v5}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v7

    check-cast v7, Ljava/lang/Long;

    invoke-virtual {v7}, Ljava/lang/Long;->longValue()J

    move-result-wide v7

    invoke-direct {v6, v7, v8}, Ljava/util/Date;-><init>(J)V

    invoke-virtual {v1, v6}, Ljava/text/SimpleDateFormat;->format(Ljava/util/Date;)Ljava/lang/String;

    move-result-object v6

    invoke-virtual {p1, v6}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    .line 434
    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string v7, " "

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-static {v4}, Lcom/miui/server/smartpower/PowerFrozenManager$FrozenInfo;->-$$Nest$fgetmFrozenReasons(Lcom/miui/server/smartpower/PowerFrozenManager$FrozenInfo;)Ljava/util/List;

    move-result-object v7

    invoke-interface {v7, v5}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v7

    check-cast v7, Ljava/lang/String;

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {p1, v6}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    .line 435
    const-string v6, ""

    invoke-virtual {p1, v6}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V

    .line 430
    add-int/lit8 v5, v5, 0x1

    goto :goto_2

    .line 422
    .end local v4    # "info":Lcom/miui/server/smartpower/PowerFrozenManager$FrozenInfo;
    .end local v5    # "index":I
    :cond_2
    :goto_3
    add-int/lit8 v3, v3, 0x1

    goto/16 :goto_1

    .line 438
    .end local v0    # "n":I
    .end local v3    # "i":I
    :cond_3
    monitor-exit v2
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 439
    const-string v0, ""

    invoke-virtual {p1, v0}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V

    .line 440
    return-void

    .line 438
    :catchall_0
    move-exception v0

    :try_start_2
    monitor-exit v2
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    throw v0

    .line 416
    .end local v1    # "formater":Ljava/text/SimpleDateFormat;
    :catchall_1
    move-exception v1

    :try_start_3
    monitor-exit v0
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    throw v1
.end method

.method private dumpHistory(Ljava/io/PrintWriter;[Ljava/lang/String;II)V
    .locals 10
    .param p1, "pw"    # Ljava/io/PrintWriter;
    .param p2, "args"    # [Ljava/lang/String;
    .param p3, "opti"    # I
    .param p4, "dpUid"    # I

    .line 443
    const-string/jumbo v0, "smart power frozen in history:"

    invoke-virtual {p1, v0}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V

    .line 444
    nop

    .line 445
    invoke-static {}, Landroid/os/SystemClock;->uptimeMillis()J

    move-result-wide v0

    sget-wide v2, Lcom/miui/app/smartpower/SmartPowerSettings;->MAX_HISTORY_REPORT_DURATION:J

    sub-long/2addr v0, v2

    .line 444
    invoke-direct {p0, v0, v1}, Lcom/miui/server/smartpower/PowerFrozenManager;->getHistoryInfos(J)Ljava/util/List;

    move-result-object v0

    .line 446
    .local v0, "infos":Ljava/util/List;, "Ljava/util/List<Lcom/miui/server/smartpower/PowerFrozenManager$FrozenInfo;>;"
    const/4 v1, 0x1

    .line 447
    .local v1, "index":I
    new-instance v2, Ljava/text/SimpleDateFormat;

    const-string v3, "HH:mm:ss.SSS"

    invoke-direct {v2, v3}, Ljava/text/SimpleDateFormat;-><init>(Ljava/lang/String;)V

    .line 448
    .local v2, "formater":Ljava/text/SimpleDateFormat;
    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :goto_0
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v4

    if-eqz v4, :cond_2

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lcom/miui/server/smartpower/PowerFrozenManager$FrozenInfo;

    .line 449
    .local v4, "info":Lcom/miui/server/smartpower/PowerFrozenManager$FrozenInfo;
    if-eqz p4, :cond_0

    invoke-static {v4}, Lcom/miui/server/smartpower/PowerFrozenManager$FrozenInfo;->-$$Nest$fgetmUid(Lcom/miui/server/smartpower/PowerFrozenManager$FrozenInfo;)I

    move-result v5

    if-eq v5, p4, :cond_0

    .line 450
    goto :goto_0

    .line 452
    :cond_0
    const-string v5, "  "

    invoke-virtual {p1, v5}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    .line 453
    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "#"

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    add-int/lit8 v6, v1, 0x1

    .end local v1    # "index":I
    .local v6, "index":I
    invoke-virtual {v5, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p1, v1}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    .line 454
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, " "

    invoke-virtual {v1, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    new-instance v7, Ljava/util/Date;

    invoke-static {v4}, Lcom/miui/server/smartpower/PowerFrozenManager$FrozenInfo;->-$$Nest$fgetmThawTime(Lcom/miui/server/smartpower/PowerFrozenManager$FrozenInfo;)J

    move-result-wide v8

    invoke-direct {v7, v8, v9}, Ljava/util/Date;-><init>(J)V

    invoke-virtual {v2, v7}, Ljava/text/SimpleDateFormat;->format(Ljava/util/Date;)Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v1, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p1, v1}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    .line 455
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v1, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p1, v1}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    .line 456
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v1, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-static {v4}, Lcom/miui/server/smartpower/PowerFrozenManager$FrozenInfo;->-$$Nest$mgetFrozenDuration(Lcom/miui/server/smartpower/PowerFrozenManager$FrozenInfo;)J

    move-result-wide v7

    invoke-virtual {v1, v7, v8}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v7, "ms"

    invoke-virtual {v1, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p1, v1}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V

    .line 457
    const/4 v1, 0x0

    .local v1, "i":I
    :goto_1
    invoke-static {v4}, Lcom/miui/server/smartpower/PowerFrozenManager$FrozenInfo;->-$$Nest$fgetmFrozenTimes(Lcom/miui/server/smartpower/PowerFrozenManager$FrozenInfo;)Ljava/util/List;

    move-result-object v7

    invoke-interface {v7}, Ljava/util/List;->size()I

    move-result v7

    const-string v8, "    "

    if-ge v1, v7, :cond_1

    .line 458
    invoke-virtual {p1, v8}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    .line 459
    const-string v7, "fz: "

    invoke-virtual {p1, v7}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    .line 460
    new-instance v7, Ljava/util/Date;

    invoke-static {v4}, Lcom/miui/server/smartpower/PowerFrozenManager$FrozenInfo;->-$$Nest$fgetmFrozenTimes(Lcom/miui/server/smartpower/PowerFrozenManager$FrozenInfo;)Ljava/util/List;

    move-result-object v8

    invoke-interface {v8, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v8

    check-cast v8, Ljava/lang/Long;

    invoke-virtual {v8}, Ljava/lang/Long;->longValue()J

    move-result-wide v8

    invoke-direct {v7, v8, v9}, Ljava/util/Date;-><init>(J)V

    invoke-virtual {v2, v7}, Ljava/text/SimpleDateFormat;->format(Ljava/util/Date;)Ljava/lang/String;

    move-result-object v7

    invoke-virtual {p1, v7}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    .line 461
    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v7, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-static {v4}, Lcom/miui/server/smartpower/PowerFrozenManager$FrozenInfo;->-$$Nest$fgetmFrozenReasons(Lcom/miui/server/smartpower/PowerFrozenManager$FrozenInfo;)Ljava/util/List;

    move-result-object v8

    invoke-interface {v8, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v8

    check-cast v8, Ljava/lang/String;

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-virtual {p1, v7}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    .line 462
    const-string v7, ""

    invoke-virtual {p1, v7}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V

    .line 457
    add-int/lit8 v1, v1, 0x1

    goto :goto_1

    .line 464
    .end local v1    # "i":I
    :cond_1
    invoke-virtual {p1, v8}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    .line 465
    const-string/jumbo v1, "th: "

    invoke-virtual {p1, v1}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    .line 466
    new-instance v1, Ljava/util/Date;

    invoke-static {v4}, Lcom/miui/server/smartpower/PowerFrozenManager$FrozenInfo;->-$$Nest$fgetmThawTime(Lcom/miui/server/smartpower/PowerFrozenManager$FrozenInfo;)J

    move-result-wide v7

    invoke-direct {v1, v7, v8}, Ljava/util/Date;-><init>(J)V

    invoke-virtual {v2, v1}, Ljava/text/SimpleDateFormat;->format(Ljava/util/Date;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p1, v1}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    .line 467
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v1, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-static {v4}, Lcom/miui/server/smartpower/PowerFrozenManager$FrozenInfo;->-$$Nest$fgetmThawReason(Lcom/miui/server/smartpower/PowerFrozenManager$FrozenInfo;)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v1, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p1, v1}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V

    .line 468
    .end local v4    # "info":Lcom/miui/server/smartpower/PowerFrozenManager$FrozenInfo;
    move v1, v6

    goto/16 :goto_0

    .line 469
    .end local v6    # "index":I
    .local v1, "index":I
    :cond_2
    return-void
.end method

.method private dumpSettings(Ljava/io/PrintWriter;[Ljava/lang/String;I)V
    .locals 2
    .param p1, "pw"    # Ljava/io/PrintWriter;
    .param p2, "args"    # [Ljava/lang/String;
    .param p3, "opti"    # I

    .line 392
    const-string v0, "Settings:"

    invoke-virtual {p1, v0}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V

    .line 393
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "  enable="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    sget-boolean v1, Lcom/miui/server/smartpower/PowerFrozenManager;->sEnable:Z

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, " def("

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    sget-boolean v1, Lcom/miui/app/smartpower/SmartPowerSettings;->PROP_FROZEN_ENABLE:Z

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ")"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v0}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V

    .line 395
    return-void
.end method

.method private dumpUid(Ljava/io/PrintWriter;[Ljava/lang/String;I)V
    .locals 2
    .param p1, "pw"    # Ljava/io/PrintWriter;
    .param p2, "args"    # [Ljava/lang/String;
    .param p3, "opti"    # I

    .line 398
    const/4 v0, 0x0

    .line 399
    .local v0, "dpUid":I
    array-length v1, p2

    if-ge p3, v1, :cond_0

    .line 400
    aget-object v1, p2, p3

    .line 401
    .local v1, "uidStr":Ljava/lang/String;
    invoke-static {v1}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v0

    .line 402
    invoke-direct {p0, p1, p2, p3, v0}, Lcom/miui/server/smartpower/PowerFrozenManager;->dumpFrozen(Ljava/io/PrintWriter;[Ljava/lang/String;II)V

    .line 403
    invoke-direct {p0, p1, p2, p3, v0}, Lcom/miui/server/smartpower/PowerFrozenManager;->dumpHistory(Ljava/io/PrintWriter;[Ljava/lang/String;II)V

    .line 405
    .end local v1    # "uidStr":Ljava/lang/String;
    :cond_0
    return-void
.end method

.method private getHistoryInfos(J)Ljava/util/List;
    .locals 7
    .param p1, "sinceUptime"    # J
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(J)",
            "Ljava/util/List<",
            "Lcom/miui/server/smartpower/PowerFrozenManager$FrozenInfo;",
            ">;"
        }
    .end annotation

    .line 355
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    .line 356
    .local v0, "ret":Ljava/util/List;, "Ljava/util/List<Lcom/miui/server/smartpower/PowerFrozenManager$FrozenInfo;>;"
    iget v1, p0, Lcom/miui/server/smartpower/PowerFrozenManager;->mHistoryIndexNext:I

    const/4 v2, -0x1

    const/16 v3, 0x1000

    invoke-static {v1, v2, v3}, Lcom/miui/server/smartpower/PowerFrozenManager;->ringAdvance(III)I

    move-result v1

    .line 358
    .local v1, "index":I
    const/4 v4, 0x0

    .local v4, "i":I
    :goto_0
    if-ge v4, v3, :cond_1

    .line 359
    iget-object v5, p0, Lcom/miui/server/smartpower/PowerFrozenManager;->mFrozenHistory:[Lcom/miui/server/smartpower/PowerFrozenManager$FrozenInfo;

    aget-object v5, v5, v1

    if-eqz v5, :cond_1

    invoke-static {v5}, Lcom/miui/server/smartpower/PowerFrozenManager$FrozenInfo;->-$$Nest$fgetmThawTime(Lcom/miui/server/smartpower/PowerFrozenManager$FrozenInfo;)J

    move-result-wide v5

    cmp-long v5, v5, p1

    if-gez v5, :cond_0

    .line 361
    goto :goto_1

    .line 363
    :cond_0
    iget-object v5, p0, Lcom/miui/server/smartpower/PowerFrozenManager;->mFrozenHistory:[Lcom/miui/server/smartpower/PowerFrozenManager$FrozenInfo;

    aget-object v5, v5, v1

    invoke-interface {v0, v5}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 364
    invoke-static {v1, v2, v3}, Lcom/miui/server/smartpower/PowerFrozenManager;->ringAdvance(III)I

    move-result v1

    .line 358
    add-int/lit8 v4, v4, 0x1

    goto :goto_0

    .line 366
    .end local v4    # "i":I
    :cond_1
    :goto_1
    return-object v0
.end method

.method public static isEnable()Z
    .locals 1

    .line 341
    sget-boolean v0, Lcom/miui/server/smartpower/PowerFrozenManager;->sEnable:Z

    return v0
.end method

.method private removeFrozenPid(IIZ)V
    .locals 4
    .param p1, "uid"    # I
    .param p2, "pid"    # I
    .param p3, "isSmartPower"    # Z

    .line 266
    iget-object v0, p0, Lcom/miui/server/smartpower/PowerFrozenManager;->mAllFrozenPids:Landroid/util/SparseArray;

    monitor-enter v0

    .line 267
    :try_start_0
    sget-boolean v1, Lcom/miui/server/smartpower/PowerFrozenManager;->DEBUG:Z

    if-eqz v1, :cond_0

    const-string v1, "PowerFrozen"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "remove p:"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, "/u:"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, " isMy:"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p3}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Slog;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 268
    :cond_0
    iget-object v1, p0, Lcom/miui/server/smartpower/PowerFrozenManager;->mAllFrozenPids:Landroid/util/SparseArray;

    invoke-virtual {v1, p2}, Landroid/util/SparseArray;->remove(I)V

    .line 269
    iget-object v1, p0, Lcom/miui/server/smartpower/PowerFrozenManager;->mHandler:Lcom/miui/server/smartpower/PowerFrozenManager$H;

    invoke-static {p2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    const/4 v3, 0x1

    invoke-virtual {v1, v3, v2}, Lcom/miui/server/smartpower/PowerFrozenManager$H;->hasMessages(ILjava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_1

    .line 270
    iget-object v1, p0, Lcom/miui/server/smartpower/PowerFrozenManager;->mHandler:Lcom/miui/server/smartpower/PowerFrozenManager$H;

    invoke-static {p2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v1, v3, v2}, Lcom/miui/server/smartpower/PowerFrozenManager$H;->removeMessages(ILjava/lang/Object;)V

    .line 272
    :cond_1
    monitor-exit v0

    .line 273
    return-void

    .line 272
    :catchall_0
    move-exception v1

    monitor-exit v0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v1
.end method

.method private static ringAdvance(III)I
    .locals 2
    .param p0, "origin"    # I
    .param p1, "increment"    # I
    .param p2, "size"    # I

    .line 345
    add-int v0, p0, p1

    rem-int/2addr v0, p2

    .line 346
    .local v0, "index":I
    if-gez v0, :cond_0

    add-int v1, v0, p2

    goto :goto_0

    :cond_0
    move v1, v0

    :goto_0
    return v1
.end method


# virtual methods
.method public addFrozenPid(II)V
    .locals 1
    .param p1, "uid"    # I
    .param p2, "pid"    # I

    .line 248
    const/4 v0, 0x0

    invoke-direct {p0, p1, p2, v0}, Lcom/miui/server/smartpower/PowerFrozenManager;->addFrozenPid(IIZ)V

    .line 249
    return-void
.end method

.method public dump(Ljava/io/PrintWriter;[Ljava/lang/String;I)V
    .locals 3
    .param p1, "pw"    # Ljava/io/PrintWriter;
    .param p2, "args"    # [Ljava/lang/String;
    .param p3, "opti"    # I

    .line 371
    :try_start_0
    array-length v0, p2

    const/4 v1, 0x0

    if-ge p3, v0, :cond_6

    .line 372
    aget-object v0, p2, p3

    .line 373
    .local v0, "parm":Ljava/lang/String;
    add-int/lit8 p3, p3, 0x1

    .line 374
    const-string v2, "history"

    invoke-virtual {v0, v2}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v2

    if-nez v2, :cond_4

    const-string v2, "-h"

    invoke-virtual {v0, v2}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v2

    if-eqz v2, :cond_0

    goto :goto_1

    .line 376
    :cond_0
    const-string v2, "current"

    invoke-virtual {v0, v2}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v2

    if-nez v2, :cond_3

    const-string v2, "-c"

    invoke-virtual {v0, v2}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v2

    if-eqz v2, :cond_1

    goto :goto_0

    .line 378
    :cond_1
    const-string/jumbo v1, "uid"

    invoke-virtual {v0, v1}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v1

    if-nez v1, :cond_2

    const-string v1, "-u"

    invoke-virtual {v0, v1}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v1

    if-eqz v1, :cond_5

    .line 379
    :cond_2
    invoke-direct {p0, p1, p2, p3}, Lcom/miui/server/smartpower/PowerFrozenManager;->dumpUid(Ljava/io/PrintWriter;[Ljava/lang/String;I)V

    goto :goto_2

    .line 377
    :cond_3
    :goto_0
    invoke-direct {p0, p1, p2, p3, v1}, Lcom/miui/server/smartpower/PowerFrozenManager;->dumpFrozen(Ljava/io/PrintWriter;[Ljava/lang/String;II)V

    goto :goto_2

    .line 375
    :cond_4
    :goto_1
    invoke-direct {p0, p1, p2, p3, v1}, Lcom/miui/server/smartpower/PowerFrozenManager;->dumpHistory(Ljava/io/PrintWriter;[Ljava/lang/String;II)V

    .line 381
    .end local v0    # "parm":Ljava/lang/String;
    :cond_5
    :goto_2
    goto :goto_3

    .line 382
    :cond_6
    invoke-direct {p0, p1, p2, p3}, Lcom/miui/server/smartpower/PowerFrozenManager;->dumpSettings(Ljava/io/PrintWriter;[Ljava/lang/String;I)V

    .line 383
    invoke-direct {p0, p1, p2, p3, v1}, Lcom/miui/server/smartpower/PowerFrozenManager;->dumpFrozen(Ljava/io/PrintWriter;[Ljava/lang/String;II)V

    .line 384
    invoke-direct {p0, p1, p2, p3, v1}, Lcom/miui/server/smartpower/PowerFrozenManager;->dumpHistory(Ljava/io/PrintWriter;[Ljava/lang/String;II)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 388
    :goto_3
    goto :goto_4

    .line 386
    :catch_0
    move-exception v0

    .line 387
    .local v0, "e":Ljava/lang/Exception;
    invoke-virtual {v0}, Ljava/lang/Exception;->printStackTrace()V

    .line 389
    .end local v0    # "e":Ljava/lang/Exception;
    :goto_4
    return-void
.end method

.method public frozenProcess(IILjava/lang/String;Ljava/lang/String;)Z
    .locals 7
    .param p1, "uid"    # I
    .param p2, "pid"    # I
    .param p3, "processName"    # Ljava/lang/String;
    .param p4, "reason"    # Ljava/lang/String;

    .line 81
    const-wide/16 v5, 0x0

    move-object v0, p0

    move v1, p1

    move v2, p2

    move-object v3, p3

    move-object v4, p4

    invoke-virtual/range {v0 .. v6}, Lcom/miui/server/smartpower/PowerFrozenManager;->frozenProcess(IILjava/lang/String;Ljava/lang/String;J)Z

    move-result v0

    return v0
.end method

.method public frozenProcess(IILjava/lang/String;Ljava/lang/String;J)Z
    .locals 7
    .param p1, "uid"    # I
    .param p2, "pid"    # I
    .param p3, "processName"    # Ljava/lang/String;
    .param p4, "reason"    # Ljava/lang/String;
    .param p5, "timeOut"    # J

    .line 90
    invoke-static {}, Lcom/miui/server/smartpower/PowerFrozenManager;->isEnable()Z

    move-result v0

    const/4 v1, 0x0

    if-nez v0, :cond_0

    .line 91
    return v1

    .line 93
    :cond_0
    if-lez p2, :cond_6

    invoke-static {}, Landroid/os/Process;->myPid()I

    move-result v0

    if-eq v0, p2, :cond_6

    invoke-static {p2}, Landroid/os/Process;->getUidForPid(I)I

    move-result v0

    if-eq v0, p1, :cond_1

    goto/16 :goto_1

    .line 98
    :cond_1
    sget-boolean v0, Lcom/miui/app/smartpower/SmartPowerSettings;->PROP_FROZEN_CGROUPV1_ENABLE:Z

    if-eqz v0, :cond_2

    .line 99
    iget-object v0, p0, Lcom/miui/server/smartpower/PowerFrozenManager;->gzInternal:Lcom/miui/server/greeze/GreezeManagerInternal;

    invoke-virtual {v0, p2}, Lcom/miui/server/greeze/GreezeManagerInternal;->freezePid(I)Z

    move-result v0

    .local v0, "done":Z
    goto :goto_0

    .line 101
    .end local v0    # "done":Z
    :cond_2
    iget-object v0, p0, Lcom/miui/server/smartpower/PowerFrozenManager;->gzInternal:Lcom/miui/server/greeze/GreezeManagerInternal;

    invoke-virtual {v0, p2, p1}, Lcom/miui/server/greeze/GreezeManagerInternal;->freezePid(II)Z

    move-result v0

    .line 102
    .restart local v0    # "done":Z
    :goto_0
    const/4 v1, 0x1

    invoke-direct {p0, p1, p2, v1}, Lcom/miui/server/smartpower/PowerFrozenManager;->addFrozenPid(IIZ)V

    .line 103
    iget-object v2, p0, Lcom/miui/server/smartpower/PowerFrozenManager;->mFrozenList:Landroid/util/SparseArray;

    monitor-enter v2

    .line 104
    :try_start_0
    iget-object v3, p0, Lcom/miui/server/smartpower/PowerFrozenManager;->mFrozenList:Landroid/util/SparseArray;

    invoke-virtual {v3, p2}, Landroid/util/SparseArray;->get(I)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/miui/server/smartpower/PowerFrozenManager$FrozenInfo;

    .line 105
    .local v3, "info":Lcom/miui/server/smartpower/PowerFrozenManager$FrozenInfo;
    if-nez v3, :cond_3

    .line 106
    new-instance v4, Lcom/miui/server/smartpower/PowerFrozenManager$FrozenInfo;

    const/4 v5, 0x0

    invoke-direct {v4, p1, p2, p3, v5}, Lcom/miui/server/smartpower/PowerFrozenManager$FrozenInfo;-><init>(IILjava/lang/String;Lcom/miui/server/smartpower/PowerFrozenManager$FrozenInfo-IA;)V

    move-object v3, v4

    .line 107
    iget-object v4, p0, Lcom/miui/server/smartpower/PowerFrozenManager;->mFrozenList:Landroid/util/SparseArray;

    invoke-virtual {v4, p2, v3}, Landroid/util/SparseArray;->put(ILjava/lang/Object;)V

    .line 109
    :cond_3
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v4

    invoke-static {v3, v4, v5, p4}, Lcom/miui/server/smartpower/PowerFrozenManager$FrozenInfo;->-$$Nest$maddFrozenInfo(Lcom/miui/server/smartpower/PowerFrozenManager$FrozenInfo;JLjava/lang/String;)V

    .line 110
    sget-boolean v4, Lcom/miui/server/smartpower/PowerFrozenManager;->DEBUG:Z

    if-eqz v4, :cond_4

    .line 111
    const-string v4, "PowerFrozen"

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "Frozen "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, " reason:"

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, p4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5}, Landroid/util/Slog;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 113
    :cond_4
    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "frozen "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    const v5, 0x15ff2

    invoke-static {v5, v4}, Landroid/util/EventLog;->writeEvent(ILjava/lang/String;)I

    .line 114
    const-wide/16 v4, 0x0

    cmp-long v4, p5, v4

    if-eqz v4, :cond_5

    .line 115
    iget-object v4, p0, Lcom/miui/server/smartpower/PowerFrozenManager;->mHandler:Lcom/miui/server/smartpower/PowerFrozenManager$H;

    invoke-static {v3}, Lcom/miui/server/smartpower/PowerFrozenManager$FrozenInfo;->-$$Nest$fgetmPid(Lcom/miui/server/smartpower/PowerFrozenManager$FrozenInfo;)I

    move-result v5

    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    invoke-virtual {v4, v1, v5}, Lcom/miui/server/smartpower/PowerFrozenManager$H;->obtainMessage(ILjava/lang/Object;)Landroid/os/Message;

    move-result-object v1

    .line 116
    .local v1, "msg":Landroid/os/Message;
    invoke-static {v3}, Lcom/miui/server/smartpower/PowerFrozenManager$FrozenInfo;->-$$Nest$fgetmUid(Lcom/miui/server/smartpower/PowerFrozenManager$FrozenInfo;)I

    move-result v4

    iput v4, v1, Landroid/os/Message;->arg1:I

    .line 117
    invoke-static {v3}, Lcom/miui/server/smartpower/PowerFrozenManager$FrozenInfo;->-$$Nest$fgetmPid(Lcom/miui/server/smartpower/PowerFrozenManager$FrozenInfo;)I

    move-result v4

    iput v4, v1, Landroid/os/Message;->arg2:I

    .line 118
    iget-object v4, p0, Lcom/miui/server/smartpower/PowerFrozenManager;->mHandler:Lcom/miui/server/smartpower/PowerFrozenManager$H;

    invoke-virtual {v4, v1, p5, p6}, Lcom/miui/server/smartpower/PowerFrozenManager$H;->sendMessageDelayed(Landroid/os/Message;J)Z

    .line 120
    .end local v1    # "msg":Landroid/os/Message;
    .end local v3    # "info":Lcom/miui/server/smartpower/PowerFrozenManager$FrozenInfo;
    :cond_5
    monitor-exit v2

    .line 121
    return v0

    .line 120
    :catchall_0
    move-exception v1

    monitor-exit v2
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v1

    .line 94
    .end local v0    # "done":Z
    :cond_6
    :goto_1
    return v1
.end method

.method public getFrozenPids(I)Ljava/util/ArrayList;
    .locals 4
    .param p1, "uid"    # I
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(I)",
            "Ljava/util/ArrayList<",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation

    .line 175
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    .line 176
    .local v0, "frozenPids":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/Integer;>;"
    iget-object v1, p0, Lcom/miui/server/smartpower/PowerFrozenManager;->mAllFrozenPids:Landroid/util/SparseArray;

    monitor-enter v1

    .line 177
    :try_start_0
    iget-object v2, p0, Lcom/miui/server/smartpower/PowerFrozenManager;->mAllFrozenPids:Landroid/util/SparseArray;

    invoke-virtual {v2}, Landroid/util/SparseArray;->size()I

    move-result v2

    add-int/lit8 v2, v2, -0x1

    .local v2, "i":I
    :goto_0
    if-ltz v2, :cond_1

    .line 178
    iget-object v3, p0, Lcom/miui/server/smartpower/PowerFrozenManager;->mAllFrozenPids:Landroid/util/SparseArray;

    invoke-virtual {v3, v2}, Landroid/util/SparseArray;->valueAt(I)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/lang/Integer;

    invoke-virtual {v3}, Ljava/lang/Integer;->intValue()I

    move-result v3

    if-ne v3, p1, :cond_0

    .line 179
    iget-object v3, p0, Lcom/miui/server/smartpower/PowerFrozenManager;->mAllFrozenPids:Landroid/util/SparseArray;

    invoke-virtual {v3, v2}, Landroid/util/SparseArray;->keyAt(I)I

    move-result v3

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    invoke-virtual {v0, v3}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 177
    :cond_0
    add-int/lit8 v2, v2, -0x1

    goto :goto_0

    .line 182
    .end local v2    # "i":I
    :cond_1
    monitor-exit v1

    .line 183
    return-object v0

    .line 182
    :catchall_0
    move-exception v2

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v2
.end method

.method public isAllFrozenForUid(I)Z
    .locals 3
    .param p1, "uid"    # I

    .line 204
    iget-object v0, p0, Lcom/miui/server/smartpower/PowerFrozenManager;->mAllFrozenPids:Landroid/util/SparseArray;

    monitor-enter v0

    .line 205
    const/4 v1, 0x0

    .local v1, "i":I
    :goto_0
    :try_start_0
    iget-object v2, p0, Lcom/miui/server/smartpower/PowerFrozenManager;->mAllFrozenPids:Landroid/util/SparseArray;

    invoke-virtual {v2}, Landroid/util/SparseArray;->size()I

    move-result v2

    if-ge v1, v2, :cond_1

    .line 206
    iget-object v2, p0, Lcom/miui/server/smartpower/PowerFrozenManager;->mAllFrozenPids:Landroid/util/SparseArray;

    invoke-virtual {v2, v1}, Landroid/util/SparseArray;->valueAt(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/lang/Integer;

    invoke-virtual {v2}, Ljava/lang/Integer;->intValue()I

    move-result v2

    if-ne v2, p1, :cond_0

    .line 207
    monitor-exit v0

    const/4 v0, 0x1

    return v0

    .line 205
    :cond_0
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 210
    .end local v1    # "i":I
    :cond_1
    monitor-exit v0

    .line 211
    const/4 v0, 0x0

    return v0

    .line 210
    :catchall_0
    move-exception v1

    monitor-exit v0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v1
.end method

.method public isAllFrozenPid(I)Z
    .locals 2
    .param p1, "pid"    # I

    .line 218
    iget-object v0, p0, Lcom/miui/server/smartpower/PowerFrozenManager;->mAllFrozenPids:Landroid/util/SparseArray;

    monitor-enter v0

    .line 219
    :try_start_0
    iget-object v1, p0, Lcom/miui/server/smartpower/PowerFrozenManager;->mAllFrozenPids:Landroid/util/SparseArray;

    invoke-virtual {v1, p1}, Landroid/util/SparseArray;->get(I)Ljava/lang/Object;

    move-result-object v1

    if-eqz v1, :cond_0

    const/4 v1, 0x1

    goto :goto_0

    :cond_0
    const/4 v1, 0x0

    :goto_0
    monitor-exit v0

    return v1

    .line 220
    :catchall_0
    move-exception v1

    monitor-exit v0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v1
.end method

.method public isFrozenForUid(I)Z
    .locals 4
    .param p1, "uid"    # I

    .line 227
    iget-object v0, p0, Lcom/miui/server/smartpower/PowerFrozenManager;->mFrozenList:Landroid/util/SparseArray;

    monitor-enter v0

    .line 228
    const/4 v1, 0x0

    .local v1, "i":I
    :goto_0
    :try_start_0
    iget-object v2, p0, Lcom/miui/server/smartpower/PowerFrozenManager;->mFrozenList:Landroid/util/SparseArray;

    invoke-virtual {v2}, Landroid/util/SparseArray;->size()I

    move-result v2

    if-ge v1, v2, :cond_1

    .line 229
    iget-object v2, p0, Lcom/miui/server/smartpower/PowerFrozenManager;->mFrozenList:Landroid/util/SparseArray;

    invoke-virtual {v2, v1}, Landroid/util/SparseArray;->valueAt(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/miui/server/smartpower/PowerFrozenManager$FrozenInfo;

    .line 230
    .local v2, "info":Lcom/miui/server/smartpower/PowerFrozenManager$FrozenInfo;
    if-eqz v2, :cond_0

    invoke-static {v2}, Lcom/miui/server/smartpower/PowerFrozenManager$FrozenInfo;->-$$Nest$fgetmUid(Lcom/miui/server/smartpower/PowerFrozenManager$FrozenInfo;)I

    move-result v3

    if-ne v3, p1, :cond_0

    .line 231
    monitor-exit v0

    const/4 v0, 0x1

    return v0

    .line 228
    .end local v2    # "info":Lcom/miui/server/smartpower/PowerFrozenManager$FrozenInfo;
    :cond_0
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 234
    .end local v1    # "i":I
    :cond_1
    monitor-exit v0

    .line 235
    const/4 v0, 0x0

    return v0

    .line 234
    :catchall_0
    move-exception v1

    monitor-exit v0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v1
.end method

.method public isFrozenPid(I)Z
    .locals 2
    .param p1, "pid"    # I

    .line 242
    iget-object v0, p0, Lcom/miui/server/smartpower/PowerFrozenManager;->mFrozenList:Landroid/util/SparseArray;

    monitor-enter v0

    .line 243
    :try_start_0
    iget-object v1, p0, Lcom/miui/server/smartpower/PowerFrozenManager;->mFrozenList:Landroid/util/SparseArray;

    invoke-virtual {v1, p1}, Landroid/util/SparseArray;->get(I)Ljava/lang/Object;

    move-result-object v1

    if-eqz v1, :cond_0

    const/4 v1, 0x1

    goto :goto_0

    :cond_0
    const/4 v1, 0x0

    :goto_0
    monitor-exit v0

    return v1

    .line 244
    :catchall_0
    move-exception v1

    monitor-exit v0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v1
.end method

.method public registerFrozenCallback(Lcom/miui/server/smartpower/PowerFrozenManager$IFrozenReportCallback;)V
    .locals 1
    .param p1, "callback"    # Lcom/miui/server/smartpower/PowerFrozenManager$IFrozenReportCallback;

    .line 276
    iget-object v0, p0, Lcom/miui/server/smartpower/PowerFrozenManager;->mFrozenCallbacks:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 277
    return-void
.end method

.method public removeFrozenPid(II)V
    .locals 1
    .param p1, "uid"    # I
    .param p2, "pid"    # I

    .line 262
    const/4 v0, 0x0

    invoke-direct {p0, p1, p2, v0}, Lcom/miui/server/smartpower/PowerFrozenManager;->removeFrozenPid(IIZ)V

    .line 263
    return-void
.end method

.method public reportBinderState(IIIIJ)V
    .locals 9
    .param p1, "uid"    # I
    .param p2, "pid"    # I
    .param p3, "tid"    # I
    .param p4, "binderState"    # I
    .param p5, "now"    # J

    .line 323
    invoke-static {}, Lcom/miui/server/smartpower/PowerFrozenManager;->isEnable()Z

    move-result v0

    if-nez v0, :cond_0

    .line 324
    return-void

    .line 326
    :cond_0
    iget-object v0, p0, Lcom/miui/server/smartpower/PowerFrozenManager;->mFrozenCallbacks:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v0

    :goto_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_1

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/miui/server/smartpower/PowerFrozenManager$IFrozenReportCallback;

    .line 327
    .local v1, "callback":Lcom/miui/server/smartpower/PowerFrozenManager$IFrozenReportCallback;
    move-object v2, v1

    move v3, p1

    move v4, p2

    move v5, p3

    move v6, p4

    move-wide v7, p5

    invoke-interface/range {v2 .. v8}, Lcom/miui/server/smartpower/PowerFrozenManager$IFrozenReportCallback;->reportBinderState(IIIIJ)V

    .line 328
    .end local v1    # "callback":Lcom/miui/server/smartpower/PowerFrozenManager$IFrozenReportCallback;
    goto :goto_0

    .line 329
    :cond_1
    return-void
.end method

.method public reportBinderTrans(IIIIIZJJ)V
    .locals 14
    .param p1, "dstUid"    # I
    .param p2, "dstPid"    # I
    .param p3, "callerUid"    # I
    .param p4, "callerPid"    # I
    .param p5, "callerTid"    # I
    .param p6, "isOneway"    # Z
    .param p7, "now"    # J
    .param p9, "buffer"    # J

    .line 304
    invoke-static {}, Lcom/miui/server/smartpower/PowerFrozenManager;->isEnable()Z

    move-result v0

    if-nez v0, :cond_0

    .line 305
    return-void

    .line 307
    :cond_0
    move-object v0, p0

    iget-object v1, v0, Lcom/miui/server/smartpower/PowerFrozenManager;->mFrozenCallbacks:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_1

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/miui/server/smartpower/PowerFrozenManager$IFrozenReportCallback;

    .line 308
    .local v2, "callback":Lcom/miui/server/smartpower/PowerFrozenManager$IFrozenReportCallback;
    move-object v3, v2

    move v4, p1

    move/from16 v5, p2

    move/from16 v6, p3

    move/from16 v7, p4

    move/from16 v8, p5

    move/from16 v9, p6

    move-wide/from16 v10, p7

    move-wide/from16 v12, p9

    invoke-interface/range {v3 .. v13}, Lcom/miui/server/smartpower/PowerFrozenManager$IFrozenReportCallback;->reportBinderTrans(IIIIIZJJ)V

    .line 310
    .end local v2    # "callback":Lcom/miui/server/smartpower/PowerFrozenManager$IFrozenReportCallback;
    goto :goto_0

    .line 311
    :cond_1
    return-void
.end method

.method public reportNet(IJ)V
    .locals 2
    .param p1, "uid"    # I
    .param p2, "now"    # J

    .line 293
    invoke-static {}, Lcom/miui/server/smartpower/PowerFrozenManager;->isEnable()Z

    move-result v0

    if-nez v0, :cond_0

    .line 294
    return-void

    .line 296
    :cond_0
    iget-object v0, p0, Lcom/miui/server/smartpower/PowerFrozenManager;->mFrozenCallbacks:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v0

    :goto_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_1

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/miui/server/smartpower/PowerFrozenManager$IFrozenReportCallback;

    .line 297
    .local v1, "callback":Lcom/miui/server/smartpower/PowerFrozenManager$IFrozenReportCallback;
    invoke-interface {v1, p1, p2, p3}, Lcom/miui/server/smartpower/PowerFrozenManager$IFrozenReportCallback;->reportNet(IJ)V

    .line 298
    .end local v1    # "callback":Lcom/miui/server/smartpower/PowerFrozenManager$IFrozenReportCallback;
    goto :goto_0

    .line 299
    :cond_1
    return-void
.end method

.method public reportSignal(IIJ)V
    .locals 2
    .param p1, "uid"    # I
    .param p2, "pid"    # I
    .param p3, "now"    # J

    .line 284
    invoke-static {}, Lcom/miui/server/smartpower/PowerFrozenManager;->isEnable()Z

    move-result v0

    if-nez v0, :cond_0

    .line 285
    return-void

    .line 287
    :cond_0
    iget-object v0, p0, Lcom/miui/server/smartpower/PowerFrozenManager;->mFrozenCallbacks:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v0

    :goto_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_1

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/miui/server/smartpower/PowerFrozenManager$IFrozenReportCallback;

    .line 288
    .local v1, "callback":Lcom/miui/server/smartpower/PowerFrozenManager$IFrozenReportCallback;
    invoke-interface {v1, p1, p2, p3, p4}, Lcom/miui/server/smartpower/PowerFrozenManager$IFrozenReportCallback;->reportSignal(IIJ)V

    .line 289
    .end local v1    # "callback":Lcom/miui/server/smartpower/PowerFrozenManager$IFrozenReportCallback;
    goto :goto_0

    .line 290
    :cond_1
    return-void
.end method

.method public serviceReady(Z)V
    .locals 3
    .param p1, "ready"    # Z

    .line 314
    if-eqz p1, :cond_0

    sget-boolean v0, Lcom/miui/app/smartpower/SmartPowerSettings;->PROP_FROZEN_ENABLE:Z

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    :goto_0
    sput-boolean v0, Lcom/miui/server/smartpower/PowerFrozenManager;->sEnable:Z

    .line 315
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v1, "serviceReady millet:"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, " frozen:"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    sget-boolean v1, Lcom/miui/server/smartpower/PowerFrozenManager;->sEnable:Z

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    const-string v1, "PowerFrozen"

    invoke-static {v1, v0}, Landroid/util/Slog;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 316
    iget-object v0, p0, Lcom/miui/server/smartpower/PowerFrozenManager;->mFrozenCallbacks:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v0

    :goto_1
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_1

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/miui/server/smartpower/PowerFrozenManager$IFrozenReportCallback;

    .line 317
    .local v1, "callback":Lcom/miui/server/smartpower/PowerFrozenManager$IFrozenReportCallback;
    sget-boolean v2, Lcom/miui/server/smartpower/PowerFrozenManager;->sEnable:Z

    invoke-interface {v1, v2}, Lcom/miui/server/smartpower/PowerFrozenManager$IFrozenReportCallback;->serviceReady(Z)V

    .line 318
    .end local v1    # "callback":Lcom/miui/server/smartpower/PowerFrozenManager$IFrozenReportCallback;
    goto :goto_1

    .line 319
    :cond_1
    return-void
.end method

.method public syncCloudControlSettings(Z)V
    .locals 1
    .param p1, "frozenEnable"    # Z

    .line 71
    if-nez p1, :cond_0

    sget-boolean v0, Lcom/miui/server/smartpower/PowerFrozenManager;->sEnable:Z

    if-eqz v0, :cond_0

    .line 72
    const-string v0, "disable frozen from cloud control"

    invoke-virtual {p0, v0}, Lcom/miui/server/smartpower/PowerFrozenManager;->thawAll(Ljava/lang/String;)V

    .line 74
    :cond_0
    return-void
.end method

.method public thawAll(Ljava/lang/String;)V
    .locals 6
    .param p1, "reason"    # Ljava/lang/String;

    .line 187
    iget-object v0, p0, Lcom/miui/server/smartpower/PowerFrozenManager;->mAllFrozenPids:Landroid/util/SparseArray;

    monitor-enter v0

    .line 188
    :try_start_0
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    .line 189
    .local v1, "frozenUids":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/Integer;>;"
    new-instance v2, Ljava/util/ArrayList;

    invoke-direct {v2}, Ljava/util/ArrayList;-><init>()V

    .line 190
    .local v2, "frozenPids":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/Integer;>;"
    iget-object v3, p0, Lcom/miui/server/smartpower/PowerFrozenManager;->mAllFrozenPids:Landroid/util/SparseArray;

    invoke-virtual {v3}, Landroid/util/SparseArray;->size()I

    move-result v3

    add-int/lit8 v3, v3, -0x1

    .local v3, "i":I
    :goto_0
    if-ltz v3, :cond_0

    .line 191
    iget-object v4, p0, Lcom/miui/server/smartpower/PowerFrozenManager;->mAllFrozenPids:Landroid/util/SparseArray;

    invoke-virtual {v4, v3}, Landroid/util/SparseArray;->valueAt(I)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Ljava/lang/Integer;

    invoke-virtual {v1, v4}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 192
    iget-object v4, p0, Lcom/miui/server/smartpower/PowerFrozenManager;->mAllFrozenPids:Landroid/util/SparseArray;

    invoke-virtual {v4, v3}, Landroid/util/SparseArray;->keyAt(I)I

    move-result v4

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    invoke-virtual {v2, v4}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 190
    add-int/lit8 v3, v3, -0x1

    goto :goto_0

    .line 194
    .end local v3    # "i":I
    :cond_0
    invoke-virtual {v2}, Ljava/util/ArrayList;->size()I

    move-result v3

    add-int/lit8 v3, v3, -0x1

    .restart local v3    # "i":I
    :goto_1
    if-ltz v3, :cond_1

    .line 195
    invoke-virtual {v1, v3}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Ljava/lang/Integer;

    invoke-virtual {v4}, Ljava/lang/Integer;->intValue()I

    move-result v4

    invoke-virtual {v2, v3}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Ljava/lang/Integer;

    invoke-virtual {v5}, Ljava/lang/Integer;->intValue()I

    move-result v5

    invoke-virtual {p0, v4, v5, p1}, Lcom/miui/server/smartpower/PowerFrozenManager;->thawProcess(IILjava/lang/String;)Z

    .line 194
    add-int/lit8 v3, v3, -0x1

    goto :goto_1

    .line 197
    .end local v1    # "frozenUids":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/Integer;>;"
    .end local v2    # "frozenPids":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/Integer;>;"
    .end local v3    # "i":I
    :cond_1
    monitor-exit v0

    .line 198
    return-void

    .line 197
    :catchall_0
    move-exception v1

    monitor-exit v0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v1
.end method

.method public thawProcess(IILjava/lang/String;)Z
    .locals 7
    .param p1, "uid"    # I
    .param p2, "pid"    # I
    .param p3, "reason"    # Ljava/lang/String;

    .line 129
    invoke-static {}, Lcom/miui/server/smartpower/PowerFrozenManager;->isEnable()Z

    move-result v0

    if-nez v0, :cond_0

    .line 130
    const/4 v0, 0x0

    return v0

    .line 133
    :cond_0
    const/4 v0, 0x0

    .line 134
    .local v0, "done":Z
    sget-boolean v1, Lcom/miui/app/smartpower/SmartPowerSettings;->PROP_FROZEN_CGROUPV1_ENABLE:Z

    if-eqz v1, :cond_1

    .line 135
    iget-object v1, p0, Lcom/miui/server/smartpower/PowerFrozenManager;->gzInternal:Lcom/miui/server/greeze/GreezeManagerInternal;

    invoke-virtual {v1, p2}, Lcom/miui/server/greeze/GreezeManagerInternal;->thawPid(I)Z

    move-result v0

    goto :goto_0

    .line 137
    :cond_1
    iget-object v1, p0, Lcom/miui/server/smartpower/PowerFrozenManager;->gzInternal:Lcom/miui/server/greeze/GreezeManagerInternal;

    invoke-virtual {v1, p2, p1}, Lcom/miui/server/greeze/GreezeManagerInternal;->thawPid(II)Z

    move-result v0

    .line 138
    :goto_0
    const/4 v1, 0x1

    invoke-direct {p0, p1, p2, v1}, Lcom/miui/server/smartpower/PowerFrozenManager;->removeFrozenPid(IIZ)V

    .line 139
    iget-object v1, p0, Lcom/miui/server/smartpower/PowerFrozenManager;->mFrozenList:Landroid/util/SparseArray;

    monitor-enter v1

    .line 140
    :try_start_0
    iget-object v2, p0, Lcom/miui/server/smartpower/PowerFrozenManager;->mFrozenList:Landroid/util/SparseArray;

    invoke-virtual {v2, p2}, Landroid/util/SparseArray;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/miui/server/smartpower/PowerFrozenManager$FrozenInfo;

    .line 141
    .local v2, "info":Lcom/miui/server/smartpower/PowerFrozenManager$FrozenInfo;
    if-nez v2, :cond_2

    .line 142
    new-instance v3, Lcom/miui/server/smartpower/PowerFrozenManager$FrozenInfo;

    const-string/jumbo v4, "unknown"

    const/4 v5, 0x0

    invoke-direct {v3, p1, p2, v4, v5}, Lcom/miui/server/smartpower/PowerFrozenManager$FrozenInfo;-><init>(IILjava/lang/String;Lcom/miui/server/smartpower/PowerFrozenManager$FrozenInfo-IA;)V

    move-object v2, v3

    .line 144
    :cond_2
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v3

    invoke-static {v2, v3, v4}, Lcom/miui/server/smartpower/PowerFrozenManager$FrozenInfo;->-$$Nest$fputmThawTime(Lcom/miui/server/smartpower/PowerFrozenManager$FrozenInfo;J)V

    .line 145
    invoke-static {}, Landroid/os/SystemClock;->uptimeMillis()J

    move-result-wide v3

    invoke-static {v2, v3, v4}, Lcom/miui/server/smartpower/PowerFrozenManager$FrozenInfo;->-$$Nest$fputmThawUptime(Lcom/miui/server/smartpower/PowerFrozenManager$FrozenInfo;J)V

    .line 146
    invoke-static {v2, p3}, Lcom/miui/server/smartpower/PowerFrozenManager$FrozenInfo;->-$$Nest$fputmThawReason(Lcom/miui/server/smartpower/PowerFrozenManager$FrozenInfo;Ljava/lang/String;)V

    .line 147
    iget-object v3, p0, Lcom/miui/server/smartpower/PowerFrozenManager;->mFrozenList:Landroid/util/SparseArray;

    invoke-virtual {v3, p2}, Landroid/util/SparseArray;->remove(I)V

    .line 148
    invoke-direct {p0, v2}, Lcom/miui/server/smartpower/PowerFrozenManager;->addHistoryInfo(Lcom/miui/server/smartpower/PowerFrozenManager$FrozenInfo;)V

    .line 149
    sget-boolean v3, Lcom/miui/server/smartpower/PowerFrozenManager;->DEBUG:Z

    if-eqz v3, :cond_3

    .line 150
    const-string v3, "PowerFrozen"

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "Thaw "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, " "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    .line 151
    invoke-static {v2}, Lcom/miui/server/smartpower/PowerFrozenManager$FrozenInfo;->-$$Nest$mgetFrozenDuration(Lcom/miui/server/smartpower/PowerFrozenManager$FrozenInfo;)J

    move-result-wide v5

    invoke-virtual {v4, v5, v6}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, "ms reason:"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, p3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    .line 150
    invoke-static {v3, v4}, Landroid/util/Slog;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 153
    :cond_3
    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v4, "thaw "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    const v4, 0x15ff2

    invoke-static {v4, v3}, Landroid/util/EventLog;->writeEvent(ILjava/lang/String;)I

    .line 154
    nop

    .end local v2    # "info":Lcom/miui/server/smartpower/PowerFrozenManager$FrozenInfo;
    monitor-exit v1

    .line 155
    return v0

    .line 154
    :catchall_0
    move-exception v2

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v2
.end method

.method public thawUid(ILjava/lang/String;)V
    .locals 4
    .param p1, "uid"    # I
    .param p2, "reason"    # Ljava/lang/String;

    .line 163
    iget-object v0, p0, Lcom/miui/server/smartpower/PowerFrozenManager;->mAllFrozenPids:Landroid/util/SparseArray;

    monitor-enter v0

    .line 164
    :try_start_0
    invoke-virtual {p0, p1}, Lcom/miui/server/smartpower/PowerFrozenManager;->getFrozenPids(I)Ljava/util/ArrayList;

    move-result-object v1

    .line 165
    .local v1, "frozenPids":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/Integer;>;"
    invoke-virtual {v1}, Ljava/util/ArrayList;->size()I

    move-result v2

    add-int/lit8 v2, v2, -0x1

    .local v2, "i":I
    :goto_0
    if-ltz v2, :cond_0

    .line 166
    invoke-virtual {v1, v2}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/lang/Integer;

    invoke-virtual {v3}, Ljava/lang/Integer;->intValue()I

    move-result v3

    invoke-virtual {p0, p1, v3, p2}, Lcom/miui/server/smartpower/PowerFrozenManager;->thawProcess(IILjava/lang/String;)Z

    .line 165
    add-int/lit8 v2, v2, -0x1

    goto :goto_0

    .line 168
    .end local v1    # "frozenPids":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/Integer;>;"
    .end local v2    # "i":I
    :cond_0
    monitor-exit v0

    .line 169
    return-void

    .line 168
    :catchall_0
    move-exception v1

    monitor-exit v0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v1
.end method

.method public thawedByOther(II)V
    .locals 2
    .param p1, "uid"    # I
    .param p2, "pid"    # I

    .line 332
    invoke-static {}, Lcom/miui/server/smartpower/PowerFrozenManager;->isEnable()Z

    move-result v0

    if-nez v0, :cond_0

    .line 333
    return-void

    .line 335
    :cond_0
    iget-object v0, p0, Lcom/miui/server/smartpower/PowerFrozenManager;->mFrozenCallbacks:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v0

    :goto_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_1

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/miui/server/smartpower/PowerFrozenManager$IFrozenReportCallback;

    .line 336
    .local v1, "callback":Lcom/miui/server/smartpower/PowerFrozenManager$IFrozenReportCallback;
    invoke-interface {v1, p1, p2}, Lcom/miui/server/smartpower/PowerFrozenManager$IFrozenReportCallback;->thawedByOther(II)V

    .line 337
    .end local v1    # "callback":Lcom/miui/server/smartpower/PowerFrozenManager$IFrozenReportCallback;
    goto :goto_0

    .line 338
    :cond_1
    return-void
.end method

.method public unRegisterFrozenCallback(Lcom/miui/server/smartpower/PowerFrozenManager$IFrozenReportCallback;)V
    .locals 1
    .param p1, "callback"    # Lcom/miui/server/smartpower/PowerFrozenManager$IFrozenReportCallback;

    .line 280
    iget-object v0, p0, Lcom/miui/server/smartpower/PowerFrozenManager;->mFrozenCallbacks:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->remove(Ljava/lang/Object;)Z

    .line 281
    return-void
.end method
