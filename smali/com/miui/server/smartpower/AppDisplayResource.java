class com.miui.server.smartpower.AppDisplayResource extends com.miui.server.smartpower.AppPowerResource implements android.hardware.display.DisplayManager$DisplayListener {
	 /* .source "AppDisplayResource.java" */
	 /* # interfaces */
	 /* # annotations */
	 /* .annotation system Ldalvik/annotation/MemberClasses; */
	 /* value = { */
	 /* Lcom/miui/server/smartpower/AppDisplayResource$DisplayRecord; */
	 /* } */
} // .end annotation
/* # instance fields */
private android.util.ArraySet mCastingWhitelists;
/* .annotation system Ldalvik/annotation/Signature; */
/* value = { */
/* "Landroid/util/ArraySet<", */
/* "Ljava/lang/String;", */
/* ">;" */
/* } */
} // .end annotation
} // .end field
private final android.util.ArrayMap mDisplayRecordMap;
/* .annotation system Ldalvik/annotation/Signature; */
/* value = { */
/* "Landroid/util/ArrayMap<", */
/* "Ljava/lang/Integer;", */
/* "Lcom/miui/server/smartpower/AppDisplayResource$DisplayRecord;", */
/* ">;" */
/* } */
} // .end annotation
} // .end field
private android.hardware.display.DisplayManager mDms;
private android.os.Handler mHandler;
private Boolean mWifiP2pConnected;
private final android.content.BroadcastReceiver mWifiP2pReceiver;
/* # direct methods */
static android.util.ArraySet -$$Nest$fgetmCastingWhitelists ( com.miui.server.smartpower.AppDisplayResource p0 ) { //bridge//synthethic
/* .locals 0 */
p0 = this.mCastingWhitelists;
} // .end method
static android.util.ArrayMap -$$Nest$fgetmDisplayRecordMap ( com.miui.server.smartpower.AppDisplayResource p0 ) { //bridge//synthethic
/* .locals 0 */
p0 = this.mDisplayRecordMap;
} // .end method
static Boolean -$$Nest$fgetmWifiP2pConnected ( com.miui.server.smartpower.AppDisplayResource p0 ) { //bridge//synthethic
/* .locals 0 */
/* iget-boolean p0, p0, Lcom/miui/server/smartpower/AppDisplayResource;->mWifiP2pConnected:Z */
} // .end method
static void -$$Nest$fputmWifiP2pConnected ( com.miui.server.smartpower.AppDisplayResource p0, Boolean p1 ) { //bridge//synthethic
/* .locals 0 */
/* iput-boolean p1, p0, Lcom/miui/server/smartpower/AppDisplayResource;->mWifiP2pConnected:Z */
return;
} // .end method
public com.miui.server.smartpower.AppDisplayResource ( ) {
/* .locals 4 */
/* .param p1, "context" # Landroid/content/Context; */
/* .param p2, "looper" # Landroid/os/Looper; */
/* .line 33 */
/* invoke-direct {p0}, Lcom/miui/server/smartpower/AppPowerResource;-><init>()V */
/* .line 27 */
/* new-instance v0, Landroid/util/ArrayMap; */
/* invoke-direct {v0}, Landroid/util/ArrayMap;-><init>()V */
this.mDisplayRecordMap = v0;
/* .line 28 */
int v0 = 0; // const/4 v0, 0x0
this.mDms = v0;
/* .line 30 */
/* new-instance v0, Landroid/util/ArraySet; */
/* invoke-direct {v0}, Landroid/util/ArraySet;-><init>()V */
this.mCastingWhitelists = v0;
/* .line 31 */
int v0 = 0; // const/4 v0, 0x0
/* iput-boolean v0, p0, Lcom/miui/server/smartpower/AppDisplayResource;->mWifiP2pConnected:Z */
/* .line 153 */
/* new-instance v0, Lcom/miui/server/smartpower/AppDisplayResource$2; */
/* invoke-direct {v0, p0}, Lcom/miui/server/smartpower/AppDisplayResource$2;-><init>(Lcom/miui/server/smartpower/AppDisplayResource;)V */
this.mWifiP2pReceiver = v0;
/* .line 34 */
int v1 = 7; // const/4 v1, 0x7
/* iput v1, p0, Lcom/miui/server/smartpower/AppDisplayResource;->mType:I */
/* .line 35 */
/* new-instance v1, Landroid/os/Handler; */
/* invoke-direct {v1, p2}, Landroid/os/Handler;-><init>(Landroid/os/Looper;)V */
this.mHandler = v1;
/* .line 36 */
final String v1 = "display"; // const-string v1, "display"
(( android.content.Context ) p1 ).getSystemService ( v1 ); // invoke-virtual {p1, v1}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;
/* check-cast v1, Landroid/hardware/display/DisplayManager; */
this.mDms = v1;
/* .line 37 */
/* new-instance v2, Landroid/os/Handler; */
/* invoke-direct {v2, p2}, Landroid/os/Handler;-><init>(Landroid/os/Looper;)V */
(( android.hardware.display.DisplayManager ) v1 ).registerDisplayListener ( p0, v2 ); // invoke-virtual {v1, p0, v2}, Landroid/hardware/display/DisplayManager;->registerDisplayListener(Landroid/hardware/display/DisplayManager$DisplayListener;Landroid/os/Handler;)V
/* .line 39 */
(( android.content.Context ) p1 ).getResources ( ); // invoke-virtual {p1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;
/* const v2, 0x110300cb */
(( android.content.res.Resources ) v1 ).getStringArray ( v2 ); // invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getStringArray(I)[Ljava/lang/String;
/* .line 41 */
/* .local v1, "packges":[Ljava/lang/String; */
v2 = this.mCastingWhitelists;
java.util.Arrays .asList ( v1 );
(( android.util.ArraySet ) v2 ).addAll ( v3 ); // invoke-virtual {v2, v3}, Landroid/util/ArraySet;->addAll(Ljava/util/Collection;)Z
/* .line 43 */
/* new-instance v2, Landroid/content/IntentFilter; */
/* invoke-direct {v2}, Landroid/content/IntentFilter;-><init>()V */
/* .line 44 */
/* .local v2, "intentFilter":Landroid/content/IntentFilter; */
final String v3 = "android.net.wifi.p2p.CONNECTION_STATE_CHANGE"; // const-string v3, "android.net.wifi.p2p.CONNECTION_STATE_CHANGE"
(( android.content.IntentFilter ) v2 ).addAction ( v3 ); // invoke-virtual {v2, v3}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V
/* .line 45 */
(( android.content.Context ) p1 ).registerReceiver ( v0, v2 ); // invoke-virtual {p1, v0, v2}, Landroid/content/Context;->registerReceiver(Landroid/content/BroadcastReceiver;Landroid/content/IntentFilter;)Landroid/content/Intent;
/* .line 46 */
return;
} // .end method
private void initializeCurrentDisplays ( ) {
/* .locals 4 */
/* .line 126 */
v0 = this.mDms;
(( android.hardware.display.DisplayManager ) v0 ).getDisplays ( ); // invoke-virtual {v0}, Landroid/hardware/display/DisplayManager;->getDisplays()[Landroid/view/Display;
/* .line 127 */
/* .local v0, "displays":[Landroid/view/Display; */
/* array-length v1, v0 */
int v2 = 0; // const/4 v2, 0x0
} // :goto_0
/* if-ge v2, v1, :cond_0 */
/* aget-object v3, v0, v2 */
/* .line 128 */
/* .local v3, "display":Landroid/view/Display; */
/* invoke-direct {p0, v3}, Lcom/miui/server/smartpower/AppDisplayResource;->onDisplayAdded(Landroid/view/Display;)V */
/* .line 127 */
} // .end local v3 # "display":Landroid/view/Display;
/* add-int/lit8 v2, v2, 0x1 */
/* .line 130 */
} // :cond_0
return;
} // .end method
private void onDisplayAdded ( android.view.Display p0 ) {
/* .locals 5 */
/* .param p1, "display" # Landroid/view/Display; */
/* .line 133 */
/* sget-boolean v0, Lcom/miui/server/smartpower/AppDisplayResource;->DEBUG:Z */
if ( v0 != null) { // if-eqz v0, :cond_0
/* .line 134 */
final String v0 = "SmartPower.AppResource"; // const-string v0, "SmartPower.AppResource"
/* new-instance v1, Ljava/lang/StringBuilder; */
/* invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V */
final String v2 = "Display added "; // const-string v2, "Display added "
(( java.lang.StringBuilder ) v1 ).append ( v2 ); // invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v1 ).append ( p1 ); // invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v1 ).toString ( ); // invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
android.util.Slog .w ( v0,v1 );
/* .line 136 */
} // :cond_0
if ( p1 != null) { // if-eqz p1, :cond_3
/* .line 137 */
v0 = this.mDisplayRecordMap;
/* monitor-enter v0 */
/* .line 138 */
try { // :try_start_0
v1 = (( android.view.Display ) p1 ).getOwnerUid ( ); // invoke-virtual {p1}, Landroid/view/Display;->getOwnerUid()I
/* .line 139 */
/* .local v1, "uid":I */
/* if-nez v1, :cond_1 */
/* .line 140 */
/* const/16 v1, 0x3e8 */
/* .line 142 */
} // :cond_1
v2 = this.mDisplayRecordMap;
java.lang.Integer .valueOf ( v1 );
(( android.util.ArrayMap ) v2 ).get ( v3 ); // invoke-virtual {v2, v3}, Landroid/util/ArrayMap;->get(Ljava/lang/Object;)Ljava/lang/Object;
/* check-cast v2, Lcom/miui/server/smartpower/AppDisplayResource$DisplayRecord; */
/* .line 143 */
/* .local v2, "displayRecord":Lcom/miui/server/smartpower/AppDisplayResource$DisplayRecord; */
/* if-nez v2, :cond_2 */
/* .line 144 */
/* new-instance v3, Lcom/miui/server/smartpower/AppDisplayResource$DisplayRecord; */
/* invoke-direct {v3, p0, v1}, Lcom/miui/server/smartpower/AppDisplayResource$DisplayRecord;-><init>(Lcom/miui/server/smartpower/AppDisplayResource;I)V */
/* move-object v2, v3 */
/* .line 145 */
v3 = this.mDisplayRecordMap;
java.lang.Integer .valueOf ( v1 );
(( android.util.ArrayMap ) v3 ).put ( v4, v2 ); // invoke-virtual {v3, v4, v2}, Landroid/util/ArrayMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;
/* .line 147 */
} // :cond_2
v3 = (( android.view.Display ) p1 ).getDisplayId ( ); // invoke-virtual {p1}, Landroid/view/Display;->getDisplayId()I
(( com.miui.server.smartpower.AppDisplayResource$DisplayRecord ) v2 ).onDisplayAdded ( v3, p1 ); // invoke-virtual {v2, v3, p1}, Lcom/miui/server/smartpower/AppDisplayResource$DisplayRecord;->onDisplayAdded(ILandroid/view/Display;)V
/* .line 148 */
} // .end local v1 # "uid":I
} // .end local v2 # "displayRecord":Lcom/miui/server/smartpower/AppDisplayResource$DisplayRecord;
/* monitor-exit v0 */
/* :catchall_0 */
/* move-exception v1 */
/* monitor-exit v0 */
/* :try_end_0 */
/* .catchall {:try_start_0 ..:try_end_0} :catchall_0 */
/* throw v1 */
/* .line 151 */
} // :cond_3
} // :goto_0
return;
} // .end method
/* # virtual methods */
public java.util.ArrayList getActiveUids ( ) {
/* .locals 3 */
/* .line 71 */
v0 = this.mDisplayRecordMap;
/* monitor-enter v0 */
/* .line 72 */
try { // :try_start_0
/* new-instance v1, Ljava/util/ArrayList; */
v2 = this.mDisplayRecordMap;
(( android.util.ArrayMap ) v2 ).keySet ( ); // invoke-virtual {v2}, Landroid/util/ArrayMap;->keySet()Ljava/util/Set;
/* invoke-direct {v1, v2}, Ljava/util/ArrayList;-><init>(Ljava/util/Collection;)V */
/* monitor-exit v0 */
/* .line 73 */
/* :catchall_0 */
/* move-exception v1 */
/* monitor-exit v0 */
/* :try_end_0 */
/* .catchall {:try_start_0 ..:try_end_0} :catchall_0 */
/* throw v1 */
} // .end method
public void init ( ) {
/* .locals 0 */
/* .line 50 */
/* invoke-direct {p0}, Lcom/miui/server/smartpower/AppDisplayResource;->initializeCurrentDisplays()V */
/* .line 51 */
return;
} // .end method
public Boolean isAppResourceActive ( Integer p0 ) {
/* .locals 3 */
/* .param p1, "uid" # I */
/* .line 78 */
v0 = this.mDisplayRecordMap;
/* monitor-enter v0 */
/* .line 79 */
try { // :try_start_0
v1 = this.mDisplayRecordMap;
java.lang.Integer .valueOf ( p1 );
(( android.util.ArrayMap ) v1 ).get ( v2 ); // invoke-virtual {v1, v2}, Landroid/util/ArrayMap;->get(Ljava/lang/Object;)Ljava/lang/Object;
/* check-cast v1, Lcom/miui/server/smartpower/AppDisplayResource$DisplayRecord; */
/* .line 80 */
/* .local v1, "record":Lcom/miui/server/smartpower/AppDisplayResource$DisplayRecord; */
if ( v1 != null) { // if-eqz v1, :cond_0
v2 = (( com.miui.server.smartpower.AppDisplayResource$DisplayRecord ) v1 ).isActive ( ); // invoke-virtual {v1}, Lcom/miui/server/smartpower/AppDisplayResource$DisplayRecord;->isActive()Z
if ( v2 != null) { // if-eqz v2, :cond_0
/* .line 81 */
/* monitor-exit v0 */
int v0 = 1; // const/4 v0, 0x1
/* .line 83 */
} // .end local v1 # "record":Lcom/miui/server/smartpower/AppDisplayResource$DisplayRecord;
} // :cond_0
/* monitor-exit v0 */
/* .line 84 */
int v0 = 0; // const/4 v0, 0x0
/* .line 83 */
/* :catchall_0 */
/* move-exception v1 */
/* monitor-exit v0 */
/* :try_end_0 */
/* .catchall {:try_start_0 ..:try_end_0} :catchall_0 */
/* throw v1 */
} // .end method
public Boolean isAppResourceActive ( Integer p0, Integer p1 ) {
/* .locals 1 */
/* .param p1, "uid" # I */
/* .param p2, "pid" # I */
/* .line 89 */
v0 = (( com.miui.server.smartpower.AppDisplayResource ) p0 ).isAppResourceActive ( p1 ); // invoke-virtual {p0, p1}, Lcom/miui/server/smartpower/AppDisplayResource;->isAppResourceActive(I)Z
} // .end method
public void onDisplayAdded ( Integer p0 ) {
/* .locals 1 */
/* .param p1, "displayId" # I */
/* .line 100 */
v0 = this.mDms;
(( android.hardware.display.DisplayManager ) v0 ).getDisplay ( p1 ); // invoke-virtual {v0, p1}, Landroid/hardware/display/DisplayManager;->getDisplay(I)Landroid/view/Display;
/* .line 101 */
/* .local v0, "display":Landroid/view/Display; */
/* invoke-direct {p0, v0}, Lcom/miui/server/smartpower/AppDisplayResource;->onDisplayAdded(Landroid/view/Display;)V */
/* .line 102 */
return;
} // .end method
public void onDisplayChanged ( Integer p0 ) {
/* .locals 0 */
/* .param p1, "displayId" # I */
/* .line 123 */
return;
} // .end method
public void onDisplayRemoved ( Integer p0 ) {
/* .locals 5 */
/* .param p1, "displayId" # I */
/* .line 106 */
v0 = this.mDms;
(( android.hardware.display.DisplayManager ) v0 ).getDisplay ( p1 ); // invoke-virtual {v0, p1}, Landroid/hardware/display/DisplayManager;->getDisplay(I)Landroid/view/Display;
/* .line 107 */
/* .local v0, "display":Landroid/view/Display; */
/* sget-boolean v1, Lcom/miui/server/smartpower/AppDisplayResource;->DEBUG:Z */
if ( v1 != null) { // if-eqz v1, :cond_0
/* .line 108 */
final String v1 = "SmartPower.AppResource"; // const-string v1, "SmartPower.AppResource"
/* new-instance v2, Ljava/lang/StringBuilder; */
/* invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V */
final String v3 = "Display removed "; // const-string v3, "Display removed "
(( java.lang.StringBuilder ) v2 ).append ( v3 ); // invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v2 ).append ( v0 ); // invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v2 ).toString ( ); // invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
android.util.Slog .d ( v1,v2 );
/* .line 110 */
} // :cond_0
v1 = this.mDisplayRecordMap;
/* monitor-enter v1 */
/* .line 111 */
try { // :try_start_0
v2 = this.mDisplayRecordMap;
(( android.util.ArrayMap ) v2 ).values ( ); // invoke-virtual {v2}, Landroid/util/ArrayMap;->values()Ljava/util/Collection;
v3 = } // :goto_0
if ( v3 != null) { // if-eqz v3, :cond_3
/* check-cast v3, Lcom/miui/server/smartpower/AppDisplayResource$DisplayRecord; */
/* .line 112 */
/* .local v3, "record":Lcom/miui/server/smartpower/AppDisplayResource$DisplayRecord; */
v4 = (( com.miui.server.smartpower.AppDisplayResource$DisplayRecord ) v3 ).onDisplayRemoved ( p1 ); // invoke-virtual {v3, p1}, Lcom/miui/server/smartpower/AppDisplayResource$DisplayRecord;->onDisplayRemoved(I)Z
if ( v4 != null) { // if-eqz v4, :cond_2
/* .line 113 */
v2 = (( com.miui.server.smartpower.AppDisplayResource$DisplayRecord ) v3 ).isActive ( ); // invoke-virtual {v3}, Lcom/miui/server/smartpower/AppDisplayResource$DisplayRecord;->isActive()Z
/* if-nez v2, :cond_1 */
/* .line 114 */
v2 = this.mDisplayRecordMap;
v4 = com.miui.server.smartpower.AppDisplayResource$DisplayRecord .-$$Nest$fgetmUid ( v3 );
java.lang.Integer .valueOf ( v4 );
(( android.util.ArrayMap ) v2 ).remove ( v4 ); // invoke-virtual {v2, v4}, Landroid/util/ArrayMap;->remove(Ljava/lang/Object;)Ljava/lang/Object;
/* .line 116 */
} // :cond_1
/* monitor-exit v1 */
return;
/* .line 118 */
} // .end local v3 # "record":Lcom/miui/server/smartpower/AppDisplayResource$DisplayRecord;
} // :cond_2
/* .line 119 */
} // :cond_3
/* monitor-exit v1 */
/* .line 120 */
return;
/* .line 119 */
/* :catchall_0 */
/* move-exception v2 */
/* monitor-exit v1 */
/* :try_end_0 */
/* .catchall {:try_start_0 ..:try_end_0} :catchall_0 */
/* throw v2 */
} // .end method
public void registerCallback ( com.miui.server.smartpower.AppPowerResource$IAppPowerResourceCallback p0, Integer p1 ) {
/* .locals 2 */
/* .param p1, "callback" # Lcom/miui/server/smartpower/AppPowerResource$IAppPowerResourceCallback; */
/* .param p2, "uid" # I */
/* .line 55 */
/* invoke-super {p0, p1, p2}, Lcom/miui/server/smartpower/AppPowerResource;->registerCallback(Lcom/miui/server/smartpower/AppPowerResource$IAppPowerResourceCallback;I)V */
/* .line 56 */
v0 = this.mHandler;
/* new-instance v1, Lcom/miui/server/smartpower/AppDisplayResource$1; */
/* invoke-direct {v1, p0, p2}, Lcom/miui/server/smartpower/AppDisplayResource$1;-><init>(Lcom/miui/server/smartpower/AppDisplayResource;I)V */
(( android.os.Handler ) v0 ).post ( v1 ); // invoke-virtual {v0, v1}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z
/* .line 67 */
return;
} // .end method
public void releaseAppPowerResource ( Integer p0 ) {
/* .locals 0 */
/* .param p1, "pid" # I */
/* .line 93 */
return;
} // .end method
public void resumeAppPowerResource ( Integer p0 ) {
/* .locals 0 */
/* .param p1, "pid" # I */
/* .line 96 */
return;
} // .end method
