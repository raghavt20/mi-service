.class Lcom/miui/server/smartpower/SmartDisplayPolicyManager$SmartScenarioCallback;
.super Ljava/lang/Object;
.source "SmartDisplayPolicyManager.java"

# interfaces
.implements Lcom/miui/server/smartpower/SmartScenarioManager$ISmartScenarioCallback;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/miui/server/smartpower/SmartDisplayPolicyManager;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "SmartScenarioCallback"
.end annotation


# instance fields
.field final synthetic this$0:Lcom/miui/server/smartpower/SmartDisplayPolicyManager;


# direct methods
.method private constructor <init>(Lcom/miui/server/smartpower/SmartDisplayPolicyManager;)V
    .locals 0

    .line 323
    iput-object p1, p0, Lcom/miui/server/smartpower/SmartDisplayPolicyManager$SmartScenarioCallback;->this$0:Lcom/miui/server/smartpower/SmartDisplayPolicyManager;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method synthetic constructor <init>(Lcom/miui/server/smartpower/SmartDisplayPolicyManager;Lcom/miui/server/smartpower/SmartDisplayPolicyManager$SmartScenarioCallback-IA;)V
    .locals 0

    invoke-direct {p0, p1}, Lcom/miui/server/smartpower/SmartDisplayPolicyManager$SmartScenarioCallback;-><init>(Lcom/miui/server/smartpower/SmartDisplayPolicyManager;)V

    return-void
.end method


# virtual methods
.method public onCurrentScenarioChanged(JJ)V
    .locals 6
    .param p1, "mainSenarioId"    # J
    .param p3, "additionalSenarioId"    # J

    .line 327
    iget-object v0, p0, Lcom/miui/server/smartpower/SmartDisplayPolicyManager$SmartScenarioCallback;->this$0:Lcom/miui/server/smartpower/SmartDisplayPolicyManager;

    const-wide/16 v1, 0x4000

    and-long/2addr v1, p3

    const-wide/16 v3, 0x0

    cmp-long v1, v1, v3

    const/4 v2, 0x1

    const/4 v5, 0x0

    if-eqz v1, :cond_0

    move v1, v2

    goto :goto_0

    :cond_0
    move v1, v5

    :goto_0
    invoke-static {v0, v1}, Lcom/miui/server/smartpower/SmartDisplayPolicyManager;->-$$Nest$fputmExternalDisplayConnected(Lcom/miui/server/smartpower/SmartDisplayPolicyManager;Z)V

    .line 330
    const-wide/16 v0, 0x2

    and-long/2addr v0, p1

    cmp-long v0, v0, v3

    if-eqz v0, :cond_1

    goto :goto_1

    :cond_1
    move v2, v5

    :goto_1
    move v0, v2

    .line 331
    .local v0, "cameraIsInForeground":Z
    iget-object v1, p0, Lcom/miui/server/smartpower/SmartDisplayPolicyManager$SmartScenarioCallback;->this$0:Lcom/miui/server/smartpower/SmartDisplayPolicyManager;

    invoke-static {v1, v0}, Lcom/miui/server/smartpower/SmartDisplayPolicyManager;->-$$Nest$mupdateCameraInForegroundStatus(Lcom/miui/server/smartpower/SmartDisplayPolicyManager;Z)V

    .line 332
    return-void
.end method
