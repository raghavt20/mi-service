.class Lcom/miui/server/smartpower/SmartWindowPolicyManager$H;
.super Landroid/os/Handler;
.source "SmartWindowPolicyManager.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/miui/server/smartpower/SmartWindowPolicyManager;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "H"
.end annotation


# instance fields
.field final synthetic this$0:Lcom/miui/server/smartpower/SmartWindowPolicyManager;


# direct methods
.method constructor <init>(Lcom/miui/server/smartpower/SmartWindowPolicyManager;Landroid/os/Looper;)V
    .locals 0
    .param p2, "looper"    # Landroid/os/Looper;

    .line 142
    iput-object p1, p0, Lcom/miui/server/smartpower/SmartWindowPolicyManager$H;->this$0:Lcom/miui/server/smartpower/SmartWindowPolicyManager;

    .line 143
    invoke-direct {p0, p2}, Landroid/os/Handler;-><init>(Landroid/os/Looper;)V

    .line 144
    return-void
.end method


# virtual methods
.method public handleMessage(Landroid/os/Message;)V
    .locals 3
    .param p1, "msg"    # Landroid/os/Message;

    .line 148
    iget v0, p1, Landroid/os/Message;->what:I

    packed-switch v0, :pswitch_data_0

    goto :goto_0

    .line 150
    :pswitch_0
    iget v0, p1, Landroid/os/Message;->arg1:I

    .line 151
    .local v0, "event":I
    iget-object v1, p1, Landroid/os/Message;->obj:Ljava/lang/Object;

    check-cast v1, Lmiui/smartpower/MultiTaskActionManager$ActionInfo;

    .line 152
    .local v1, "actionInfo":Lmiui/smartpower/MultiTaskActionManager$ActionInfo;
    iget-object v2, p0, Lcom/miui/server/smartpower/SmartWindowPolicyManager$H;->this$0:Lcom/miui/server/smartpower/SmartWindowPolicyManager;

    invoke-static {v2, v0, v1}, Lcom/miui/server/smartpower/SmartWindowPolicyManager;->-$$Nest$mnotifyMultiTaskActionChanged(Lcom/miui/server/smartpower/SmartWindowPolicyManager;ILmiui/smartpower/MultiTaskActionManager$ActionInfo;)V

    .line 153
    nop

    .line 157
    .end local v0    # "event":I
    .end local v1    # "actionInfo":Lmiui/smartpower/MultiTaskActionManager$ActionInfo;
    :goto_0
    return-void

    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
    .end packed-switch
.end method
