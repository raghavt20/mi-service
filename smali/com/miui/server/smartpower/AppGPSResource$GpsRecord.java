class com.miui.server.smartpower.AppGPSResource$GpsRecord {
	 /* .source "AppGPSResource.java" */
	 /* # annotations */
	 /* .annotation system Ldalvik/annotation/EnclosingClass; */
	 /* value = Lcom/miui/server/smartpower/AppGPSResource; */
} // .end annotation
/* .annotation system Ldalvik/annotation/InnerClass; */
/* accessFlags = 0x0 */
/* name = "GpsRecord" */
} // .end annotation
/* # instance fields */
private android.util.ArraySet mLocationListeners;
/* .annotation system Ldalvik/annotation/Signature; */
/* value = { */
/* "Landroid/util/ArraySet<", */
/* "Landroid/location/ILocationListener;", */
/* ">;" */
/* } */
} // .end annotation
} // .end field
private final Integer mPid;
private final Integer mUid;
final com.miui.server.smartpower.AppGPSResource this$0; //synthetic
/* # direct methods */
static Integer -$$Nest$fgetmUid ( com.miui.server.smartpower.AppGPSResource$GpsRecord p0 ) { //bridge//synthethic
/* .locals 0 */
/* iget p0, p0, Lcom/miui/server/smartpower/AppGPSResource$GpsRecord;->mUid:I */
} // .end method
 com.miui.server.smartpower.AppGPSResource$GpsRecord ( ) {
/* .locals 1 */
/* .param p1, "this$0" # Lcom/miui/server/smartpower/AppGPSResource; */
/* .param p2, "uid" # I */
/* .param p3, "pid" # I */
/* .line 100 */
this.this$0 = p1;
/* invoke-direct {p0}, Ljava/lang/Object;-><init>()V */
/* .line 98 */
/* new-instance v0, Landroid/util/ArraySet; */
/* invoke-direct {v0}, Landroid/util/ArraySet;-><init>()V */
this.mLocationListeners = v0;
/* .line 101 */
/* iput p2, p0, Lcom/miui/server/smartpower/AppGPSResource$GpsRecord;->mUid:I */
/* .line 102 */
/* iput p3, p0, Lcom/miui/server/smartpower/AppGPSResource$GpsRecord;->mPid:I */
/* .line 103 */
return;
} // .end method
/* # virtual methods */
Integer getPid ( ) {
/* .locals 1 */
/* .line 110 */
/* iget v0, p0, Lcom/miui/server/smartpower/AppGPSResource$GpsRecord;->mPid:I */
} // .end method
Integer getUid ( ) {
/* .locals 1 */
/* .line 114 */
/* iget v0, p0, Lcom/miui/server/smartpower/AppGPSResource$GpsRecord;->mUid:I */
} // .end method
Boolean isActive ( ) {
/* .locals 1 */
/* .line 106 */
v0 = this.mLocationListeners;
v0 = (( android.util.ArraySet ) v0 ).size ( ); // invoke-virtual {v0}, Landroid/util/ArraySet;->size()I
/* if-lez v0, :cond_0 */
int v0 = 1; // const/4 v0, 0x1
} // :cond_0
int v0 = 0; // const/4 v0, 0x0
} // :goto_0
} // .end method
void onAquireLocation ( android.location.ILocationListener p0 ) {
/* .locals 4 */
/* .param p1, "listener" # Landroid/location/ILocationListener; */
/* .line 118 */
int v0 = 0; // const/4 v0, 0x0
/* .local v0, "i":I */
} // :goto_0
v1 = this.mLocationListeners;
v1 = (( android.util.ArraySet ) v1 ).size ( ); // invoke-virtual {v1}, Landroid/util/ArraySet;->size()I
/* if-ge v0, v1, :cond_1 */
/* .line 119 */
v1 = this.mLocationListeners;
(( android.util.ArraySet ) v1 ).valueAt ( v0 ); // invoke-virtual {v1, v0}, Landroid/util/ArraySet;->valueAt(I)Ljava/lang/Object;
/* check-cast v1, Landroid/location/ILocationListener; */
/* .line 120 */
/* .local v1, "item":Landroid/location/ILocationListener; */
v2 = (( java.lang.Object ) v2 ).equals ( v3 ); // invoke-virtual {v2, v3}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z
if ( v2 != null) { // if-eqz v2, :cond_0
/* .line 121 */
return;
/* .line 118 */
} // .end local v1 # "item":Landroid/location/ILocationListener;
} // :cond_0
/* add-int/lit8 v0, v0, 0x1 */
/* .line 124 */
} // .end local v0 # "i":I
} // :cond_1
v0 = this.mLocationListeners;
(( android.util.ArraySet ) v0 ).add ( p1 ); // invoke-virtual {v0, p1}, Landroid/util/ArraySet;->add(Ljava/lang/Object;)Z
/* .line 125 */
return;
} // .end method
void onReleaseLocation ( android.location.ILocationListener p0 ) {
/* .locals 4 */
/* .param p1, "listener" # Landroid/location/ILocationListener; */
/* .line 128 */
int v0 = 0; // const/4 v0, 0x0
/* .local v0, "i":I */
} // :goto_0
v1 = this.mLocationListeners;
v1 = (( android.util.ArraySet ) v1 ).size ( ); // invoke-virtual {v1}, Landroid/util/ArraySet;->size()I
/* if-ge v0, v1, :cond_1 */
/* .line 129 */
v1 = this.mLocationListeners;
(( android.util.ArraySet ) v1 ).valueAt ( v0 ); // invoke-virtual {v1, v0}, Landroid/util/ArraySet;->valueAt(I)Ljava/lang/Object;
/* check-cast v1, Landroid/location/ILocationListener; */
/* .line 130 */
/* .local v1, "item":Landroid/location/ILocationListener; */
v2 = (( java.lang.Object ) v2 ).equals ( v3 ); // invoke-virtual {v2, v3}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z
if ( v2 != null) { // if-eqz v2, :cond_0
/* .line 131 */
v2 = this.mLocationListeners;
(( android.util.ArraySet ) v2 ).remove ( v1 ); // invoke-virtual {v2, v1}, Landroid/util/ArraySet;->remove(Ljava/lang/Object;)Z
/* .line 128 */
} // .end local v1 # "item":Landroid/location/ILocationListener;
} // :cond_0
/* add-int/lit8 v0, v0, 0x1 */
/* .line 134 */
} // .end local v0 # "i":I
} // :cond_1
return;
} // .end method
