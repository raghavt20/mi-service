.class Lcom/miui/server/smartpower/SmartScenarioManager$ScenarioItem;
.super Ljava/lang/Object;
.source "SmartScenarioManager.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/miui/server/smartpower/SmartScenarioManager;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "ScenarioItem"
.end annotation


# instance fields
.field private mAction:I

.field private final mInteractiveApps:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList<",
            "Lcom/miui/server/smartpower/SmartScenarioManager$ScenarioAppChild;",
            ">;"
        }
    .end annotation
.end field

.field final synthetic this$0:Lcom/miui/server/smartpower/SmartScenarioManager;


# direct methods
.method public static synthetic $r8$lambda$4RW5aKrS5LtWGq1m1VF_V2aaOOw(Lcom/miui/server/smartpower/SmartScenarioManager$ScenarioItem;Lcom/android/server/am/AppStateManager$AppState;ZLcom/miui/server/smartpower/SmartScenarioManager$ClientConfig;)V
    .locals 0

    invoke-direct {p0, p1, p2, p3}, Lcom/miui/server/smartpower/SmartScenarioManager$ScenarioItem;->lambda$onAppActionChanged$1(Lcom/android/server/am/AppStateManager$AppState;ZLcom/miui/server/smartpower/SmartScenarioManager$ClientConfig;)V

    return-void
.end method

.method static bridge synthetic -$$Nest$fgetmInteractiveApps(Lcom/miui/server/smartpower/SmartScenarioManager$ScenarioItem;)Ljava/util/ArrayList;
    .locals 0

    iget-object p0, p0, Lcom/miui/server/smartpower/SmartScenarioManager$ScenarioItem;->mInteractiveApps:Ljava/util/ArrayList;

    return-object p0
.end method

.method private constructor <init>(Lcom/miui/server/smartpower/SmartScenarioManager;I)V
    .locals 0
    .param p2, "action"    # I

    .line 258
    iput-object p1, p0, Lcom/miui/server/smartpower/SmartScenarioManager$ScenarioItem;->this$0:Lcom/miui/server/smartpower/SmartScenarioManager;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 255
    new-instance p1, Ljava/util/ArrayList;

    invoke-direct {p1}, Ljava/util/ArrayList;-><init>()V

    iput-object p1, p0, Lcom/miui/server/smartpower/SmartScenarioManager$ScenarioItem;->mInteractiveApps:Ljava/util/ArrayList;

    .line 259
    iput p2, p0, Lcom/miui/server/smartpower/SmartScenarioManager$ScenarioItem;->mAction:I

    .line 260
    return-void
.end method

.method synthetic constructor <init>(Lcom/miui/server/smartpower/SmartScenarioManager;ILcom/miui/server/smartpower/SmartScenarioManager$ScenarioItem-IA;)V
    .locals 0

    invoke-direct {p0, p1, p2}, Lcom/miui/server/smartpower/SmartScenarioManager$ScenarioItem;-><init>(Lcom/miui/server/smartpower/SmartScenarioManager;I)V

    return-void
.end method

.method static synthetic lambda$onAppActionChanged$0(Lcom/android/server/am/AppStateManager$AppState;Lcom/miui/server/smartpower/SmartScenarioManager$ScenarioAppChild;)Z
    .locals 1
    .param p0, "app"    # Lcom/android/server/am/AppStateManager$AppState;
    .param p1, "item"    # Lcom/miui/server/smartpower/SmartScenarioManager$ScenarioAppChild;

    .line 290
    invoke-static {p1}, Lcom/miui/server/smartpower/SmartScenarioManager$ScenarioAppChild;->-$$Nest$fgetmApp(Lcom/miui/server/smartpower/SmartScenarioManager$ScenarioAppChild;)Lcom/android/server/am/AppStateManager$AppState;

    move-result-object v0

    if-ne v0, p0, :cond_0

    const/4 v0, 0x1

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    :goto_0
    return v0
.end method

.method private synthetic lambda$onAppActionChanged$1(Lcom/android/server/am/AppStateManager$AppState;ZLcom/miui/server/smartpower/SmartScenarioManager$ClientConfig;)V
    .locals 2
    .param p1, "app"    # Lcom/android/server/am/AppStateManager$AppState;
    .param p2, "add"    # Z
    .param p3, "v"    # Lcom/miui/server/smartpower/SmartScenarioManager$ClientConfig;

    .line 297
    if-nez p1, :cond_0

    const/4 v0, 0x0

    goto :goto_0

    :cond_0
    invoke-virtual {p1}, Lcom/android/server/am/AppStateManager$AppState;->getPackageName()Ljava/lang/String;

    move-result-object v0

    .line 298
    .local v0, "packageName":Ljava/lang/String;
    :goto_0
    iget v1, p0, Lcom/miui/server/smartpower/SmartScenarioManager$ScenarioItem;->mAction:I

    invoke-virtual {p3, v1, v0, p2}, Lcom/miui/server/smartpower/SmartScenarioManager$ClientConfig;->onAppActionChanged(ILjava/lang/String;Z)V

    .line 299
    return-void
.end method


# virtual methods
.method public getAction()I
    .locals 1

    .line 263
    iget v0, p0, Lcom/miui/server/smartpower/SmartScenarioManager$ScenarioItem;->mAction:I

    return v0
.end method

.method public onAppActionChanged(Lcom/android/server/am/AppStateManager$AppState;Z)V
    .locals 7
    .param p1, "app"    # Lcom/android/server/am/AppStateManager$AppState;
    .param p2, "add"    # Z

    .line 276
    const/4 v0, 0x0

    .line 277
    .local v0, "changed":Z
    iget-object v1, p0, Lcom/miui/server/smartpower/SmartScenarioManager$ScenarioItem;->mInteractiveApps:Ljava/util/ArrayList;

    monitor-enter v1

    .line 278
    if-eqz p2, :cond_2

    .line 279
    const/4 v2, 0x0

    .line 280
    .local v2, "contains":Z
    :try_start_0
    iget-object v3, p0, Lcom/miui/server/smartpower/SmartScenarioManager$ScenarioItem;->mInteractiveApps:Ljava/util/ArrayList;

    invoke-virtual {v3}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :goto_0
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v4

    if-eqz v4, :cond_1

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lcom/miui/server/smartpower/SmartScenarioManager$ScenarioAppChild;

    .line 281
    .local v4, "child":Lcom/miui/server/smartpower/SmartScenarioManager$ScenarioAppChild;
    invoke-static {v4}, Lcom/miui/server/smartpower/SmartScenarioManager$ScenarioAppChild;->-$$Nest$fgetmApp(Lcom/miui/server/smartpower/SmartScenarioManager$ScenarioAppChild;)Lcom/android/server/am/AppStateManager$AppState;

    move-result-object v5

    if-ne v5, p1, :cond_0

    .line 282
    const/4 v2, 0x1

    .line 283
    goto :goto_1

    .line 285
    .end local v4    # "child":Lcom/miui/server/smartpower/SmartScenarioManager$ScenarioAppChild;
    :cond_0
    goto :goto_0

    .line 286
    :cond_1
    :goto_1
    if-nez v2, :cond_3

    .line 287
    iget-object v3, p0, Lcom/miui/server/smartpower/SmartScenarioManager$ScenarioItem;->mInteractiveApps:Ljava/util/ArrayList;

    new-instance v4, Lcom/miui/server/smartpower/SmartScenarioManager$ScenarioAppChild;

    iget-object v5, p0, Lcom/miui/server/smartpower/SmartScenarioManager$ScenarioItem;->this$0:Lcom/miui/server/smartpower/SmartScenarioManager;

    const/4 v6, 0x0

    invoke-direct {v4, v5, p1, v6}, Lcom/miui/server/smartpower/SmartScenarioManager$ScenarioAppChild;-><init>(Lcom/miui/server/smartpower/SmartScenarioManager;Lcom/android/server/am/AppStateManager$AppState;Lcom/miui/server/smartpower/SmartScenarioManager$ScenarioAppChild-IA;)V

    invoke-virtual {v3, v4}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 288
    const/4 v0, 0x1

    goto :goto_2

    .line 290
    .end local v2    # "contains":Z
    :cond_2
    iget-object v2, p0, Lcom/miui/server/smartpower/SmartScenarioManager$ScenarioItem;->mInteractiveApps:Ljava/util/ArrayList;

    new-instance v3, Lcom/miui/server/smartpower/SmartScenarioManager$ScenarioItem$$ExternalSyntheticLambda0;

    invoke-direct {v3, p1}, Lcom/miui/server/smartpower/SmartScenarioManager$ScenarioItem$$ExternalSyntheticLambda0;-><init>(Lcom/android/server/am/AppStateManager$AppState;)V

    invoke-virtual {v2, v3}, Ljava/util/ArrayList;->removeIf(Ljava/util/function/Predicate;)Z

    move-result v2

    if-eqz v2, :cond_3

    .line 291
    const/4 v0, 0x1

    goto :goto_3

    .line 290
    :cond_3
    :goto_2
    nop

    .line 293
    :goto_3
    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 294
    if-eqz v0, :cond_4

    .line 295
    iget-object v1, p0, Lcom/miui/server/smartpower/SmartScenarioManager$ScenarioItem;->this$0:Lcom/miui/server/smartpower/SmartScenarioManager;

    invoke-static {v1}, Lcom/miui/server/smartpower/SmartScenarioManager;->-$$Nest$fgetmClients(Lcom/miui/server/smartpower/SmartScenarioManager;)Landroid/util/ArraySet;

    move-result-object v1

    monitor-enter v1

    .line 296
    :try_start_1
    iget-object v2, p0, Lcom/miui/server/smartpower/SmartScenarioManager$ScenarioItem;->this$0:Lcom/miui/server/smartpower/SmartScenarioManager;

    invoke-static {v2}, Lcom/miui/server/smartpower/SmartScenarioManager;->-$$Nest$fgetmClients(Lcom/miui/server/smartpower/SmartScenarioManager;)Landroid/util/ArraySet;

    move-result-object v2

    new-instance v3, Lcom/miui/server/smartpower/SmartScenarioManager$ScenarioItem$$ExternalSyntheticLambda1;

    invoke-direct {v3, p0, p1, p2}, Lcom/miui/server/smartpower/SmartScenarioManager$ScenarioItem$$ExternalSyntheticLambda1;-><init>(Lcom/miui/server/smartpower/SmartScenarioManager$ScenarioItem;Lcom/android/server/am/AppStateManager$AppState;Z)V

    invoke-virtual {v2, v3}, Landroid/util/ArraySet;->forEach(Ljava/util/function/Consumer;)V

    .line 300
    monitor-exit v1

    goto :goto_4

    :catchall_0
    move-exception v2

    monitor-exit v1
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    throw v2

    .line 302
    :cond_4
    :goto_4
    return-void

    .line 293
    :catchall_1
    move-exception v2

    :try_start_2
    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    throw v2
.end method

.method public parseCurrentScenarioId(Lcom/miui/server/smartpower/SmartScenarioManager$ClientConfig;)V
    .locals 6
    .param p1, "client"    # Lcom/miui/server/smartpower/SmartScenarioManager$ClientConfig;

    .line 267
    iget v0, p0, Lcom/miui/server/smartpower/SmartScenarioManager$ScenarioItem;->mAction:I

    const/4 v1, 0x0

    const/4 v2, 0x1

    invoke-virtual {p1, v0, v1, v2}, Lcom/miui/server/smartpower/SmartScenarioManager$ClientConfig;->onAppActionChanged(ILjava/lang/String;Z)V

    .line 268
    iget-object v0, p0, Lcom/miui/server/smartpower/SmartScenarioManager$ScenarioItem;->mInteractiveApps:Ljava/util/ArrayList;

    monitor-enter v0

    .line 269
    :try_start_0
    iget-object v1, p0, Lcom/miui/server/smartpower/SmartScenarioManager$ScenarioItem;->mInteractiveApps:Ljava/util/ArrayList;

    invoke-virtual {v1}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/miui/server/smartpower/SmartScenarioManager$ScenarioAppChild;

    .line 270
    .local v3, "app":Lcom/miui/server/smartpower/SmartScenarioManager$ScenarioAppChild;
    iget v4, p0, Lcom/miui/server/smartpower/SmartScenarioManager$ScenarioItem;->mAction:I

    invoke-virtual {v3}, Lcom/miui/server/smartpower/SmartScenarioManager$ScenarioAppChild;->getPackageName()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {p1, v4, v5, v2}, Lcom/miui/server/smartpower/SmartScenarioManager$ClientConfig;->onAppActionChanged(ILjava/lang/String;Z)V

    .line 271
    .end local v3    # "app":Lcom/miui/server/smartpower/SmartScenarioManager$ScenarioAppChild;
    goto :goto_0

    .line 272
    :cond_0
    monitor-exit v0

    .line 273
    return-void

    .line 272
    :catchall_0
    move-exception v1

    monitor-exit v0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v1
.end method

.method public toString()Ljava/lang/String;
    .locals 2

    .line 306
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    iget v1, p0, Lcom/miui/server/smartpower/SmartScenarioManager$ScenarioItem;->mAction:I

    invoke-static {v1}, Lcom/miui/server/smartpower/SmartScenarioManager;->actionTypeToString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, " : "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lcom/miui/server/smartpower/SmartScenarioManager$ScenarioItem;->mInteractiveApps:Ljava/util/ArrayList;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
