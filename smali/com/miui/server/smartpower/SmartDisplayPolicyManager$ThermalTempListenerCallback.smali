.class Lcom/miui/server/smartpower/SmartDisplayPolicyManager$ThermalTempListenerCallback;
.super Ljava/lang/Object;
.source "SmartDisplayPolicyManager.java"

# interfaces
.implements Lcom/android/server/am/ThermalTempListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/miui/server/smartpower/SmartDisplayPolicyManager;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "ThermalTempListenerCallback"
.end annotation


# instance fields
.field final synthetic this$0:Lcom/miui/server/smartpower/SmartDisplayPolicyManager;


# direct methods
.method private constructor <init>(Lcom/miui/server/smartpower/SmartDisplayPolicyManager;)V
    .locals 0

    .line 336
    iput-object p1, p0, Lcom/miui/server/smartpower/SmartDisplayPolicyManager$ThermalTempListenerCallback;->this$0:Lcom/miui/server/smartpower/SmartDisplayPolicyManager;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method synthetic constructor <init>(Lcom/miui/server/smartpower/SmartDisplayPolicyManager;Lcom/miui/server/smartpower/SmartDisplayPolicyManager$ThermalTempListenerCallback-IA;)V
    .locals 0

    invoke-direct {p0, p1}, Lcom/miui/server/smartpower/SmartDisplayPolicyManager$ThermalTempListenerCallback;-><init>(Lcom/miui/server/smartpower/SmartDisplayPolicyManager;)V

    return-void
.end method


# virtual methods
.method public onThermalTempChange(I)V
    .locals 2
    .param p1, "temp"    # I

    .line 339
    iget-object v0, p0, Lcom/miui/server/smartpower/SmartDisplayPolicyManager$ThermalTempListenerCallback;->this$0:Lcom/miui/server/smartpower/SmartDisplayPolicyManager;

    invoke-static {v0, p1}, Lcom/miui/server/smartpower/SmartDisplayPolicyManager;->-$$Nest$fputmCurrentTemp(Lcom/miui/server/smartpower/SmartDisplayPolicyManager;I)V

    .line 340
    iget-object v0, p0, Lcom/miui/server/smartpower/SmartDisplayPolicyManager$ThermalTempListenerCallback;->this$0:Lcom/miui/server/smartpower/SmartDisplayPolicyManager;

    invoke-static {}, Lcom/miui/server/smartpower/SmartDisplayPolicyManager;->-$$Nest$sfgetTHERMAL_TEMP_THRESHOLD()I

    move-result v1

    if-le p1, v1, :cond_0

    const/4 v1, 0x1

    goto :goto_0

    :cond_0
    const/4 v1, 0x0

    :goto_0
    invoke-static {v0, v1}, Lcom/miui/server/smartpower/SmartDisplayPolicyManager;->-$$Nest$fputisTempWarning(Lcom/miui/server/smartpower/SmartDisplayPolicyManager;Z)V

    .line 342
    sget-boolean v0, Lcom/miui/server/smartpower/SmartDisplayPolicyManager;->DEBUG:Z

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/miui/server/smartpower/SmartDisplayPolicyManager$ThermalTempListenerCallback;->this$0:Lcom/miui/server/smartpower/SmartDisplayPolicyManager;

    invoke-static {v0}, Lcom/miui/server/smartpower/SmartDisplayPolicyManager;->-$$Nest$fgetisTempWarning(Lcom/miui/server/smartpower/SmartDisplayPolicyManager;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 343
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "onThermalTempChange, warning temp: "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    const-string v1, "SmartPower.DisplayPolicy"

    invoke-static {v1, v0}, Landroid/util/Slog;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 345
    :cond_1
    return-void
.end method
