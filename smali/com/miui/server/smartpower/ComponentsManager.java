public class com.miui.server.smartpower.ComponentsManager {
	 /* .source "ComponentsManager.java" */
	 /* # static fields */
	 public static final Boolean DEBUG;
	 public static final java.lang.String TAG;
	 /* # instance fields */
	 private com.android.server.am.AppStateManager mAppStateManager;
	 private final android.util.ArraySet mPendingUids;
	 /* .annotation system Ldalvik/annotation/Signature; */
	 /* value = { */
	 /* "Landroid/util/ArraySet<", */
	 /* "Ljava/lang/Integer;", */
	 /* ">;" */
	 /* } */
} // .end annotation
} // .end field
private com.miui.server.smartpower.SmartPowerPolicyManager mSmartPowerPolicyManager;
/* # direct methods */
static com.miui.server.smartpower.ComponentsManager ( ) {
/* .locals 1 */
/* .line 21 */
/* sget-boolean v0, Lcom/android/server/am/SmartPowerService;->DEBUG:Z */
com.miui.server.smartpower.ComponentsManager.DEBUG = (v0!= 0);
return;
} // .end method
public com.miui.server.smartpower.ComponentsManager ( ) {
/* .locals 1 */
/* .param p1, "context" # Landroid/content/Context; */
/* .param p2, "looper" # Landroid/os/Looper; */
/* .line 29 */
/* invoke-direct {p0}, Ljava/lang/Object;-><init>()V */
/* .line 22 */
int v0 = 0; // const/4 v0, 0x0
this.mAppStateManager = v0;
/* .line 23 */
this.mSmartPowerPolicyManager = v0;
/* .line 27 */
/* new-instance v0, Landroid/util/ArraySet; */
/* invoke-direct {v0}, Landroid/util/ArraySet;-><init>()V */
this.mPendingUids = v0;
/* .line 30 */
return;
} // .end method
static java.lang.String serviceExecutionToString ( Integer p0 ) {
/* .locals 1 */
/* .param p0, "execution" # I */
/* .line 146 */
/* packed-switch p0, :pswitch_data_0 */
/* .line 160 */
java.lang.Integer .toString ( p0 );
/* .line 158 */
/* :pswitch_0 */
/* const-string/jumbo v0, "unbind" */
/* .line 156 */
/* :pswitch_1 */
final String v0 = "destroy"; // const-string v0, "destroy"
/* .line 154 */
/* :pswitch_2 */
/* const-string/jumbo v0, "start" */
/* .line 152 */
/* :pswitch_3 */
final String v0 = "create"; // const-string v0, "create"
/* .line 150 */
/* :pswitch_4 */
final String v0 = "bind"; // const-string v0, "bind"
/* .line 148 */
/* :pswitch_5 */
final String v0 = "end"; // const-string v0, "end"
/* :pswitch_data_0 */
/* .packed-switch 0x0 */
/* :pswitch_5 */
/* :pswitch_4 */
/* :pswitch_3 */
/* :pswitch_2 */
/* :pswitch_1 */
/* :pswitch_0 */
} // .end packed-switch
} // .end method
/* # virtual methods */
public void activityStartBeforeLocked ( java.lang.String p0, Integer p1, Integer p2, java.lang.String p3, Boolean p4 ) {
/* .locals 6 */
/* .param p1, "name" # Ljava/lang/String; */
/* .param p2, "fromPid" # I */
/* .param p3, "toUid" # I */
/* .param p4, "packageName" # Ljava/lang/String; */
/* .param p5, "isColdStart" # Z */
/* .line 68 */
v0 = this.mAppStateManager;
/* move-object v1, p1 */
/* move v2, p2 */
/* move v3, p3 */
/* move-object v4, p4 */
/* move v5, p5 */
/* invoke-virtual/range {v0 ..v5}, Lcom/android/server/am/AppStateManager;->activityStartBeforeLocked(Ljava/lang/String;IILjava/lang/String;Z)V */
/* .line 69 */
return;
} // .end method
public void activityVisibilityChangedLocked ( Integer p0, java.lang.String p1, com.android.server.wm.ActivityRecord p2, Boolean p3 ) {
/* .locals 3 */
/* .param p1, "uid" # I */
/* .param p2, "processName" # Ljava/lang/String; */
/* .param p3, "record" # Lcom/android/server/wm/ActivityRecord; */
/* .param p4, "visible" # Z */
/* .line 48 */
v0 = this.mPendingUids;
/* monitor-enter v0 */
/* .line 49 */
try { // :try_start_0
v1 = this.mPendingUids;
java.lang.Integer .valueOf ( p1 );
(( android.util.ArraySet ) v1 ).add ( v2 ); // invoke-virtual {v1, v2}, Landroid/util/ArraySet;->add(Ljava/lang/Object;)Z
/* .line 50 */
v1 = this.mAppStateManager;
(( com.android.server.am.AppStateManager ) v1 ).setActivityVisible ( p1, p2, p3, p4 ); // invoke-virtual {v1, p1, p2, p3, p4}, Lcom/android/server/am/AppStateManager;->setActivityVisible(ILjava/lang/String;Lcom/android/server/wm/ActivityRecord;Z)V
/* .line 51 */
if ( p4 != null) { // if-eqz p4, :cond_0
	 /* .line 52 */
	 (( com.miui.server.smartpower.ComponentsManager ) p0 ).updateActivitiesVisibilityLocked ( ); // invoke-virtual {p0}, Lcom/miui/server/smartpower/ComponentsManager;->updateActivitiesVisibilityLocked()V
	 /* .line 54 */
} // :cond_0
/* monitor-exit v0 */
/* .line 55 */
return;
/* .line 54 */
/* :catchall_0 */
/* move-exception v1 */
/* monitor-exit v0 */
/* :try_end_0 */
/* .catchall {:try_start_0 ..:try_end_0} :catchall_0 */
/* throw v1 */
} // .end method
public void alarmStatusChangedLocked ( Integer p0, Boolean p1 ) {
/* .locals 2 */
/* .param p1, "uid" # I */
/* .param p2, "active" # Z */
/* .line 123 */
if ( p2 != null) { // if-eqz p2, :cond_0
	 /* .line 124 */
	 final String v0 = "alarm start"; // const-string v0, "alarm start"
	 /* .line 125 */
	 /* .local v0, "content":Ljava/lang/String; */
	 v1 = this.mAppStateManager;
	 (( com.android.server.am.AppStateManager ) v1 ).componentStartLocked ( p1, v0 ); // invoke-virtual {v1, p1, v0}, Lcom/android/server/am/AppStateManager;->componentStartLocked(ILjava/lang/String;)V
	 /* .line 126 */
} // .end local v0 # "content":Ljava/lang/String;
/* .line 127 */
} // :cond_0
final String v0 = "alarm end"; // const-string v0, "alarm end"
/* .line 128 */
/* .restart local v0 # "content":Ljava/lang/String; */
v1 = this.mAppStateManager;
(( com.android.server.am.AppStateManager ) v1 ).componentEndLocked ( p1, v0 ); // invoke-virtual {v1, p1, v0}, Lcom/android/server/am/AppStateManager;->componentEndLocked(ILjava/lang/String;)V
/* .line 130 */
} // .end local v0 # "content":Ljava/lang/String;
} // :goto_0
return;
} // .end method
public void broadcastStatusChangedLocked ( com.android.server.am.ProcessRecord p0, Boolean p1, android.content.Intent p2 ) {
/* .locals 2 */
/* .param p1, "processRecord" # Lcom/android/server/am/ProcessRecord; */
/* .param p2, "active" # Z */
/* .param p3, "intent" # Landroid/content/Intent; */
/* .line 80 */
if ( p2 != null) { // if-eqz p2, :cond_0
/* .line 81 */
/* new-instance v0, Ljava/lang/StringBuilder; */
/* invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V */
final String v1 = "broadcast start "; // const-string v1, "broadcast start "
(( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( android.content.Intent ) p3 ).toString ( ); // invoke-virtual {p3}, Landroid/content/Intent;->toString()Ljava/lang/String;
(( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v0 ).toString ( ); // invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
/* .line 82 */
/* .local v0, "content":Ljava/lang/String; */
v1 = this.mAppStateManager;
(( com.android.server.am.AppStateManager ) v1 ).componentStartLocked ( p1, v0 ); // invoke-virtual {v1, p1, v0}, Lcom/android/server/am/AppStateManager;->componentStartLocked(Lcom/android/server/am/ProcessRecord;Ljava/lang/String;)V
/* .line 83 */
} // .end local v0 # "content":Ljava/lang/String;
/* .line 84 */
} // :cond_0
/* new-instance v0, Ljava/lang/StringBuilder; */
/* invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V */
final String v1 = "broadcast end "; // const-string v1, "broadcast end "
(( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( android.content.Intent ) p3 ).toString ( ); // invoke-virtual {p3}, Landroid/content/Intent;->toString()Ljava/lang/String;
(( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v0 ).toString ( ); // invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
/* .line 85 */
/* .restart local v0 # "content":Ljava/lang/String; */
v1 = this.mAppStateManager;
(( com.android.server.am.AppStateManager ) v1 ).componentEndLocked ( p1, v0 ); // invoke-virtual {v1, p1, v0}, Lcom/android/server/am/AppStateManager;->componentEndLocked(Lcom/android/server/am/ProcessRecord;Ljava/lang/String;)V
/* .line 87 */
} // .end local v0 # "content":Ljava/lang/String;
} // :goto_0
return;
} // .end method
public void contentProviderStatusChangedLocked ( com.android.server.am.ProcessRecord p0, java.lang.String p1 ) {
/* .locals 2 */
/* .param p1, "hostingProc" # Lcom/android/server/am/ProcessRecord; */
/* .param p2, "name" # Ljava/lang/String; */
/* .line 105 */
/* if-nez p1, :cond_0 */
/* .line 106 */
return;
/* .line 108 */
} // :cond_0
/* new-instance v0, Ljava/lang/StringBuilder; */
/* invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V */
final String v1 = "content provider start "; // const-string v1, "content provider start "
(( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v0 ).append ( p2 ); // invoke-virtual {v0, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v0 ).toString ( ); // invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
/* .line 109 */
/* .local v0, "content":Ljava/lang/String; */
v1 = this.mAppStateManager;
(( com.android.server.am.AppStateManager ) v1 ).componentStartLocked ( p1, v0 ); // invoke-virtual {v1, p1, v0}, Lcom/android/server/am/AppStateManager;->componentStartLocked(Lcom/android/server/am/ProcessRecord;Ljava/lang/String;)V
/* .line 110 */
return;
} // .end method
public void init ( com.android.server.am.AppStateManager p0, com.miui.server.smartpower.SmartPowerPolicyManager p1 ) {
/* .locals 0 */
/* .param p1, "appStateManager" # Lcom/android/server/am/AppStateManager; */
/* .param p2, "smartPowerPolicyManager" # Lcom/miui/server/smartpower/SmartPowerPolicyManager; */
/* .line 34 */
this.mAppStateManager = p1;
/* .line 35 */
this.mSmartPowerPolicyManager = p2;
/* .line 36 */
return;
} // .end method
public void onApplyOomAdjLocked ( com.android.server.am.ProcessRecord p0 ) {
/* .locals 1 */
/* .param p1, "app" # Lcom/android/server/am/ProcessRecord; */
/* .line 133 */
v0 = this.mAppStateManager;
(( com.android.server.am.AppStateManager ) v0 ).onApplyOomAdjLocked ( p1 ); // invoke-virtual {v0, p1}, Lcom/android/server/am/AppStateManager;->onApplyOomAdjLocked(Lcom/android/server/am/ProcessRecord;)V
/* .line 134 */
return;
} // .end method
public void onForegroundActivityChangedLocked ( Integer p0, Integer p1, Integer p2, java.lang.String p3, Integer p4, Integer p5, java.lang.String p6 ) {
/* .locals 8 */
/* .param p1, "fromPid" # I */
/* .param p2, "toUid" # I */
/* .param p3, "toPid" # I */
/* .param p4, "name" # Ljava/lang/String; */
/* .param p5, "taskId" # I */
/* .param p6, "callingUid" # I */
/* .param p7, "callingPkg" # Ljava/lang/String; */
/* .line 74 */
v0 = this.mAppStateManager;
/* move v1, p1 */
/* move v2, p2 */
/* move v3, p3 */
/* move-object v4, p4 */
/* move v5, p5 */
/* move v6, p6 */
/* move-object v7, p7 */
/* invoke-virtual/range {v0 ..v7}, Lcom/android/server/am/AppStateManager;->onForegroundActivityChangedLocked(IIILjava/lang/String;IILjava/lang/String;)V */
/* .line 76 */
return;
} // .end method
public void onSendPendingIntent ( Integer p0, java.lang.String p1 ) {
/* .locals 1 */
/* .param p1, "uid" # I */
/* .param p2, "typeName" # Ljava/lang/String; */
/* .line 142 */
v0 = this.mAppStateManager;
(( com.android.server.am.AppStateManager ) v0 ).onSendPendingIntent ( p1, p2 ); // invoke-virtual {v0, p1, p2}, Lcom/android/server/am/AppStateManager;->onSendPendingIntent(ILjava/lang/String;)V
/* .line 143 */
return;
} // .end method
public void providerConnectionChangedLocked ( com.android.server.am.ProcessRecord p0, com.android.server.am.ProcessRecord p1, Boolean p2 ) {
/* .locals 1 */
/* .param p1, "client" # Lcom/android/server/am/ProcessRecord; */
/* .param p2, "host" # Lcom/android/server/am/ProcessRecord; */
/* .param p3, "isConnect" # Z */
/* .line 119 */
v0 = this.mAppStateManager;
(( com.android.server.am.AppStateManager ) v0 ).providerConnectionChangedLocked ( p1, p2, p3 ); // invoke-virtual {v0, p1, p2, p3}, Lcom/android/server/am/AppStateManager;->providerConnectionChangedLocked(Lcom/android/server/am/ProcessRecord;Lcom/android/server/am/ProcessRecord;Z)V
/* .line 120 */
return;
} // .end method
public void serviceConnectionChangedLocked ( com.android.server.am.ProcessRecord p0, com.android.server.am.ProcessRecord p1, Boolean p2, Long p3 ) {
/* .locals 6 */
/* .param p1, "client" # Lcom/android/server/am/ProcessRecord; */
/* .param p2, "service" # Lcom/android/server/am/ProcessRecord; */
/* .param p3, "isConnect" # Z */
/* .param p4, "flags" # J */
/* .line 114 */
v0 = this.mAppStateManager;
/* move-object v1, p1 */
/* move-object v2, p2 */
/* move v3, p3 */
/* move-wide v4, p4 */
/* invoke-virtual/range {v0 ..v5}, Lcom/android/server/am/AppStateManager;->serviceConnectionChangedLocked(Lcom/android/server/am/ProcessRecord;Lcom/android/server/am/ProcessRecord;ZJ)V */
/* .line 115 */
return;
} // .end method
public void serviceStatusChangedLocked ( com.android.server.am.ProcessRecord p0, Boolean p1, java.lang.String p2, Integer p3 ) {
/* .locals 4 */
/* .param p1, "processRecord" # Lcom/android/server/am/ProcessRecord; */
/* .param p2, "active" # Z */
/* .param p3, "name" # Ljava/lang/String; */
/* .param p4, "execution" # I */
/* .line 91 */
/* new-instance v0, Ljava/lang/StringBuilder; */
/* invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V */
/* const-string/jumbo v1, "service " */
(( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
/* .line 92 */
com.miui.server.smartpower.ComponentsManager .serviceExecutionToString ( p4 );
(( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
final String v1 = " "; // const-string v1, " "
(( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v0 ).append ( p3 ); // invoke-virtual {v0, p3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v0 ).toString ( ); // invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
/* .line 93 */
/* .local v0, "content":Ljava/lang/String; */
if ( p2 != null) { // if-eqz p2, :cond_1
/* .line 94 */
v1 = this.mAppStateManager;
/* iget v2, p1, Lcom/android/server/am/ProcessRecord;->uid:I */
v3 = this.processName;
v1 = (( com.android.server.am.AppStateManager ) v1 ).getProcessState ( v2, v3 ); // invoke-virtual {v1, v2, v3}, Lcom/android/server/am/AppStateManager;->getProcessState(ILjava/lang/String;)I
/* .line 96 */
/* .local v1, "state":I */
int v2 = 6; // const/4 v2, 0x6
/* if-le v1, v2, :cond_0 */
/* .line 97 */
v2 = this.mAppStateManager;
(( com.android.server.am.AppStateManager ) v2 ).componentStartLocked ( p1, v0 ); // invoke-virtual {v2, p1, v0}, Lcom/android/server/am/AppStateManager;->componentStartLocked(Lcom/android/server/am/ProcessRecord;Ljava/lang/String;)V
/* .line 99 */
} // .end local v1 # "state":I
} // :cond_0
/* .line 100 */
} // :cond_1
v1 = this.mAppStateManager;
(( com.android.server.am.AppStateManager ) v1 ).componentEndLocked ( p1, v0 ); // invoke-virtual {v1, p1, v0}, Lcom/android/server/am/AppStateManager;->componentEndLocked(Lcom/android/server/am/ProcessRecord;Ljava/lang/String;)V
/* .line 102 */
} // :goto_0
return;
} // .end method
public void updateActivitiesVisibilityLocked ( ) {
/* .locals 3 */
/* .line 40 */
v0 = this.mPendingUids;
/* monitor-enter v0 */
/* .line 41 */
try { // :try_start_0
v1 = this.mAppStateManager;
v2 = this.mPendingUids;
(( com.android.server.am.AppStateManager ) v1 ).updateVisibilityLocked ( v2 ); // invoke-virtual {v1, v2}, Lcom/android/server/am/AppStateManager;->updateVisibilityLocked(Landroid/util/ArraySet;)V
/* .line 42 */
v1 = this.mPendingUids;
(( android.util.ArraySet ) v1 ).clear ( ); // invoke-virtual {v1}, Landroid/util/ArraySet;->clear()V
/* .line 43 */
/* monitor-exit v0 */
/* .line 44 */
return;
/* .line 43 */
/* :catchall_0 */
/* move-exception v1 */
/* monitor-exit v0 */
/* :try_end_0 */
/* .catchall {:try_start_0 ..:try_end_0} :catchall_0 */
/* throw v1 */
} // .end method
public void windowVisibilityChangedLocked ( Integer p0, Integer p1, com.android.server.policy.WindowManagerPolicy$WindowState p2, android.view.WindowManager$LayoutParams p3, Boolean p4 ) {
/* .locals 9 */
/* .param p1, "uid" # I */
/* .param p2, "pid" # I */
/* .param p3, "win" # Lcom/android/server/policy/WindowManagerPolicy$WindowState; */
/* .param p4, "attrs" # Landroid/view/WindowManager$LayoutParams; */
/* .param p5, "visible" # Z */
/* .line 59 */
v0 = this.mPendingUids;
/* monitor-enter v0 */
/* .line 60 */
try { // :try_start_0
v1 = this.mPendingUids;
java.lang.Integer .valueOf ( p1 );
(( android.util.ArraySet ) v1 ).add ( v2 ); // invoke-virtual {v1, v2}, Landroid/util/ArraySet;->add(Ljava/lang/Object;)Z
/* .line 61 */
v3 = this.mAppStateManager;
/* move v4, p1 */
/* move v5, p2 */
/* move-object v6, p3 */
/* move-object v7, p4 */
/* move v8, p5 */
/* invoke-virtual/range {v3 ..v8}, Lcom/android/server/am/AppStateManager;->setWindowVisible(IILcom/android/server/policy/WindowManagerPolicy$WindowState;Landroid/view/WindowManager$LayoutParams;Z)V */
/* .line 62 */
(( com.miui.server.smartpower.ComponentsManager ) p0 ).updateActivitiesVisibilityLocked ( ); // invoke-virtual {p0}, Lcom/miui/server/smartpower/ComponentsManager;->updateActivitiesVisibilityLocked()V
/* .line 63 */
/* monitor-exit v0 */
/* .line 64 */
return;
/* .line 63 */
/* :catchall_0 */
/* move-exception v1 */
/* monitor-exit v0 */
/* :try_end_0 */
/* .catchall {:try_start_0 ..:try_end_0} :catchall_0 */
/* throw v1 */
} // .end method
