public class com.miui.server.smartpower.SmartDisplayPolicyManager {
	 /* .source "SmartDisplayPolicyManager.java" */
	 /* # annotations */
	 /* .annotation system Ldalvik/annotation/MemberClasses; */
	 /* value = { */
	 /* Lcom/miui/server/smartpower/SmartDisplayPolicyManager$RefreshRatePolicyController;, */
	 /* Lcom/miui/server/smartpower/SmartDisplayPolicyManager$H;, */
	 /* Lcom/miui/server/smartpower/SmartDisplayPolicyManager$FrameInsertionObserver;, */
	 /* Lcom/miui/server/smartpower/SmartDisplayPolicyManager$GesturesEventListenerCallback;, */
	 /* Lcom/miui/server/smartpower/SmartDisplayPolicyManager$SmartScenarioCallback;, */
	 /* Lcom/miui/server/smartpower/SmartDisplayPolicyManager$ThermalTempListenerCallback;, */
	 /* Lcom/miui/server/smartpower/SmartDisplayPolicyManager$ComingInteraction; */
	 /* } */
} // .end annotation
/* # static fields */
private static final java.lang.String CAMERA_PACKAGE_NAME;
private static final Boolean CAMERA_REFRESH_RATE_ENABLE;
public static final Boolean DEBUG;
private static final Boolean ENABLE;
private static final Integer INSET_IME_HIDE;
private static final Integer INSET_IME_SHOW;
private static final Integer INTERACT_APP_TRANSITION;
private static final Integer INTERACT_BASE_TYPE;
public static final Integer INTERACT_CVW_GESTURE;
private static final Integer INTERACT_FOCUS_WINDOW_CHANGE;
public static final Integer INTERACT_FREE_FORM;
private static final Integer INTERACT_INPUT_DOWN;
private static final Integer INTERACT_INPUT_FLING;
private static final Integer INTERACT_INPUT_MOVE;
private static final Integer INTERACT_INSET_ANIMATION;
public static final Integer INTERACT_MULTI_WINDOW;
private static final Integer INTERACT_NONE_TYPE;
private static final Integer INTERACT_RECENT_ANIMATION;
private static final Integer INTERACT_REMOTE_ANIMATION;
public static final Integer INTERACT_SPLIT_SCREEN;
private static final Integer INTERACT_STOP_BASE_TYPE;
private static final Integer INTERACT_STOP_CAMERA;
public static final Integer INTERACT_TRANSITION;
private static final Integer INTERACT_WINDOW_ANIMATION;
private static final Integer INTERACT_WINDOW_VISIBLE_CHANGED;
private static final Integer INVALID_APP_TRANSITION_DUR;
private static final Integer INVALID_DEFAULT_DUR;
private static final Integer INVALID_SF_MODE_ID;
private static final Integer INVALID_WINDOW_ANIMATION_DUR;
private static final Integer MAX_INPUT_FLING_DURATION;
private static final Integer MODULE_SF_INTERACTION;
private static final Integer MSG_DOWN_REFRESHRATE;
private static final Integer MSG_UPDATE_DISPLAY_DEVICE;
private static final Integer MSG_UP_REFRESHRATE;
private static final Integer SCENARIO_TYPE_EXTERNAL_CASTING;
private static final Integer SCENARIO_TYPE_TOPAPP;
private static final Integer SF_INTERACTION_END;
private static final Integer SF_INTERACTION_START;
private static final java.lang.String SF_PERF_INTERACTION;
public static final java.lang.String TAG;
private static final Integer THERMAL_TEMP_THRESHOLD;
private static final Integer TRANSACTION_SF_DISPLAY_FEATURE_IDLE;
/* # instance fields */
private Boolean isTempWarning;
private final android.content.Context mContext;
private Integer mCurrentTemp;
private final java.lang.Object mDisplayLock;
private final com.android.server.display.DisplayManagerServiceStub mDisplayManagerServiceStub;
private final com.android.server.display.mode.DisplayModeDirectorStub mDisplayModeDirectorStub;
private Integer mDisplayModeHeight;
private Integer mDisplayModeWidth;
private Boolean mExternalDisplayConnected;
private java.lang.String mFrameInsertScene;
private Boolean mFrameInsertingForGame;
private Boolean mFrameInsertingForVideo;
private final com.miui.server.smartpower.SmartDisplayPolicyManager$FrameInsertionObserver mFrameInsertionObserver;
private final com.miui.server.smartpower.SmartDisplayPolicyManager$H mHandler;
private final android.os.HandlerThread mHandlerTh;
private Boolean mIsCameraInForeground;
private java.lang.String mLastFocusPackage;
private Integer mLastInputMethodStatus;
private com.miui.server.smartpower.SmartDisplayPolicyManager$RefreshRatePolicyController mRefreshRatePolicyController;
private com.android.server.wm.SchedBoostGesturesEvent mSchedBoostGesturesEvent;
private com.miui.server.smartpower.SmartScenarioManager mSmartScenarioManager;
private com.miui.server.smartpower.SmartWindowPolicyManager mSmartWindowPolicyManager;
private android.view.SurfaceControl$DisplayMode mSupportedDisplayModes;
private final android.os.IBinder mSurfaceFlinger;
private final com.android.server.wm.WindowManagerService mWMS;
/* # direct methods */
static Boolean -$$Nest$fgetisTempWarning ( com.miui.server.smartpower.SmartDisplayPolicyManager p0 ) { //bridge//synthethic
	 /* .locals 0 */
	 /* iget-boolean p0, p0, Lcom/miui/server/smartpower/SmartDisplayPolicyManager;->isTempWarning:Z */
} // .end method
static Integer -$$Nest$fgetmCurrentTemp ( com.miui.server.smartpower.SmartDisplayPolicyManager p0 ) { //bridge//synthethic
	 /* .locals 0 */
	 /* iget p0, p0, Lcom/miui/server/smartpower/SmartDisplayPolicyManager;->mCurrentTemp:I */
} // .end method
static Boolean -$$Nest$fgetmExternalDisplayConnected ( com.miui.server.smartpower.SmartDisplayPolicyManager p0 ) { //bridge//synthethic
	 /* .locals 0 */
	 /* iget-boolean p0, p0, Lcom/miui/server/smartpower/SmartDisplayPolicyManager;->mExternalDisplayConnected:Z */
} // .end method
static java.lang.String -$$Nest$fgetmFrameInsertScene ( com.miui.server.smartpower.SmartDisplayPolicyManager p0 ) { //bridge//synthethic
	 /* .locals 0 */
	 p0 = this.mFrameInsertScene;
} // .end method
static Boolean -$$Nest$fgetmFrameInsertingForGame ( com.miui.server.smartpower.SmartDisplayPolicyManager p0 ) { //bridge//synthethic
	 /* .locals 0 */
	 /* iget-boolean p0, p0, Lcom/miui/server/smartpower/SmartDisplayPolicyManager;->mFrameInsertingForGame:Z */
} // .end method
static Boolean -$$Nest$fgetmFrameInsertingForVideo ( com.miui.server.smartpower.SmartDisplayPolicyManager p0 ) { //bridge//synthethic
	 /* .locals 0 */
	 /* iget-boolean p0, p0, Lcom/miui/server/smartpower/SmartDisplayPolicyManager;->mFrameInsertingForVideo:Z */
} // .end method
static Boolean -$$Nest$fgetmIsCameraInForeground ( com.miui.server.smartpower.SmartDisplayPolicyManager p0 ) { //bridge//synthethic
	 /* .locals 0 */
	 /* iget-boolean p0, p0, Lcom/miui/server/smartpower/SmartDisplayPolicyManager;->mIsCameraInForeground:Z */
} // .end method
static android.view.SurfaceControl$DisplayMode -$$Nest$fgetmSupportedDisplayModes ( com.miui.server.smartpower.SmartDisplayPolicyManager p0 ) { //bridge//synthethic
	 /* .locals 0 */
	 p0 = this.mSupportedDisplayModes;
} // .end method
static void -$$Nest$fputisTempWarning ( com.miui.server.smartpower.SmartDisplayPolicyManager p0, Boolean p1 ) { //bridge//synthethic
	 /* .locals 0 */
	 /* iput-boolean p1, p0, Lcom/miui/server/smartpower/SmartDisplayPolicyManager;->isTempWarning:Z */
	 return;
} // .end method
static void -$$Nest$fputmCurrentTemp ( com.miui.server.smartpower.SmartDisplayPolicyManager p0, Integer p1 ) { //bridge//synthethic
	 /* .locals 0 */
	 /* iput p1, p0, Lcom/miui/server/smartpower/SmartDisplayPolicyManager;->mCurrentTemp:I */
	 return;
} // .end method
static void -$$Nest$fputmExternalDisplayConnected ( com.miui.server.smartpower.SmartDisplayPolicyManager p0, Boolean p1 ) { //bridge//synthethic
	 /* .locals 0 */
	 /* iput-boolean p1, p0, Lcom/miui/server/smartpower/SmartDisplayPolicyManager;->mExternalDisplayConnected:Z */
	 return;
} // .end method
static void -$$Nest$fputmFrameInsertScene ( com.miui.server.smartpower.SmartDisplayPolicyManager p0, java.lang.String p1 ) { //bridge//synthethic
	 /* .locals 0 */
	 this.mFrameInsertScene = p1;
	 return;
} // .end method
static void -$$Nest$fputmFrameInsertingForGame ( com.miui.server.smartpower.SmartDisplayPolicyManager p0, Boolean p1 ) { //bridge//synthethic
	 /* .locals 0 */
	 /* iput-boolean p1, p0, Lcom/miui/server/smartpower/SmartDisplayPolicyManager;->mFrameInsertingForGame:Z */
	 return;
} // .end method
static void -$$Nest$fputmFrameInsertingForVideo ( com.miui.server.smartpower.SmartDisplayPolicyManager p0, Boolean p1 ) { //bridge//synthethic
	 /* .locals 0 */
	 /* iput-boolean p1, p0, Lcom/miui/server/smartpower/SmartDisplayPolicyManager;->mFrameInsertingForVideo:Z */
	 return;
} // .end method
static java.lang.String -$$Nest$mgetForegroundPackageName ( com.miui.server.smartpower.SmartDisplayPolicyManager p0 ) { //bridge//synthethic
	 /* .locals 0 */
	 /* invoke-direct {p0}, Lcom/miui/server/smartpower/SmartDisplayPolicyManager;->getForegroundPackageName()Ljava/lang/String; */
} // .end method
static Boolean -$$Nest$misSupportSmartDisplayPolicy ( com.miui.server.smartpower.SmartDisplayPolicyManager p0 ) { //bridge//synthethic
	 /* .locals 0 */
	 p0 = 	 /* invoke-direct {p0}, Lcom/miui/server/smartpower/SmartDisplayPolicyManager;->isSupportSmartDisplayPolicy()Z */
} // .end method
static void -$$Nest$mnotifyInputEventReceived ( com.miui.server.smartpower.SmartDisplayPolicyManager p0, Integer p1 ) { //bridge//synthethic
	 /* .locals 0 */
	 /* invoke-direct {p0, p1}, Lcom/miui/server/smartpower/SmartDisplayPolicyManager;->notifyInputEventReceived(I)V */
	 return;
} // .end method
static void -$$Nest$mnotifyInputEventReceived ( com.miui.server.smartpower.SmartDisplayPolicyManager p0, Integer p1, Long p2 ) { //bridge//synthethic
	 /* .locals 0 */
	 /* invoke-direct {p0, p1, p2, p3}, Lcom/miui/server/smartpower/SmartDisplayPolicyManager;->notifyInputEventReceived(IJ)V */
	 return;
} // .end method
static void -$$Nest$mshouldDownRefreshRate ( com.miui.server.smartpower.SmartDisplayPolicyManager p0 ) { //bridge//synthethic
	 /* .locals 0 */
	 /* invoke-direct {p0}, Lcom/miui/server/smartpower/SmartDisplayPolicyManager;->shouldDownRefreshRate()V */
	 return;
} // .end method
static void -$$Nest$mshouldUpRefreshRate ( com.miui.server.smartpower.SmartDisplayPolicyManager p0, com.miui.server.smartpower.SmartDisplayPolicyManager$ComingInteraction p1 ) { //bridge//synthethic
	 /* .locals 0 */
	 /* invoke-direct {p0, p1}, Lcom/miui/server/smartpower/SmartDisplayPolicyManager;->shouldUpRefreshRate(Lcom/miui/server/smartpower/SmartDisplayPolicyManager$ComingInteraction;)V */
	 return;
} // .end method
static void -$$Nest$mshouldUpdateDisplayDevice ( com.miui.server.smartpower.SmartDisplayPolicyManager p0 ) { //bridge//synthethic
	 /* .locals 0 */
	 /* invoke-direct {p0}, Lcom/miui/server/smartpower/SmartDisplayPolicyManager;->shouldUpdateDisplayDevice()V */
	 return;
} // .end method
static void -$$Nest$mupdateCameraInForegroundStatus ( com.miui.server.smartpower.SmartDisplayPolicyManager p0, Boolean p1 ) { //bridge//synthethic
	 /* .locals 0 */
	 /* invoke-direct {p0, p1}, Lcom/miui/server/smartpower/SmartDisplayPolicyManager;->updateCameraInForegroundStatus(Z)V */
	 return;
} // .end method
static Boolean -$$Nest$sfgetENABLE ( ) { //bridge//synthethic
	 /* .locals 1 */
	 /* sget-boolean v0, Lcom/miui/server/smartpower/SmartDisplayPolicyManager;->ENABLE:Z */
} // .end method
static Integer -$$Nest$sfgetTHERMAL_TEMP_THRESHOLD ( ) { //bridge//synthethic
	 /* .locals 1 */
} // .end method
static java.lang.String -$$Nest$sminteractionTypeToString ( Integer p0 ) { //bridge//synthethic
	 /* .locals 0 */
	 com.miui.server.smartpower.SmartDisplayPolicyManager .interactionTypeToString ( p0 );
} // .end method
static com.miui.server.smartpower.SmartDisplayPolicyManager ( ) {
	 /* .locals 1 */
	 /* .line 68 */
	 /* sget-boolean v0, Lcom/android/server/am/SmartPowerService;->DEBUG:Z */
	 com.miui.server.smartpower.SmartDisplayPolicyManager.DEBUG = (v0!= 0);
	 /* .line 70 */
	 /* sget-boolean v0, Lcom/miui/app/smartpower/SmartPowerSettings;->DISPLAY_POLICY_ENABLE:Z */
	 com.miui.server.smartpower.SmartDisplayPolicyManager.ENABLE = (v0!= 0);
	 /* .line 71 */
	 /* sget-boolean v0, Lcom/miui/app/smartpower/SmartPowerSettings;->PROP_CAMERA_REFRESH_RATE_ENABLE:Z */
	 com.miui.server.smartpower.SmartDisplayPolicyManager.CAMERA_REFRESH_RATE_ENABLE = (v0!= 0);
	 /* .line 74 */
	 return;
} // .end method
public com.miui.server.smartpower.SmartDisplayPolicyManager ( ) {
	 /* .locals 4 */
	 /* .param p1, "context" # Landroid/content/Context; */
	 /* .param p2, "wms" # Lcom/android/server/wm/WindowManagerService; */
	 /* .line 250 */
	 /* invoke-direct {p0}, Ljava/lang/Object;-><init>()V */
	 /* .line 207 */
	 /* new-instance v0, Landroid/os/HandlerThread; */
	 final String v1 = "SmartPowerDisplayTh"; // const-string v1, "SmartPowerDisplayTh"
	 int v2 = -2; // const/4 v2, -0x2
	 /* invoke-direct {v0, v1, v2}, Landroid/os/HandlerThread;-><init>(Ljava/lang/String;I)V */
	 this.mHandlerTh = v0;
	 /* .line 210 */
	 /* new-instance v1, Ljava/lang/Object; */
	 /* invoke-direct {v1}, Ljava/lang/Object;-><init>()V */
	 this.mDisplayLock = v1;
	 /* .line 229 */
	 int v1 = 0; // const/4 v1, 0x0
	 this.mRefreshRatePolicyController = v1;
	 /* .line 231 */
	 this.mSchedBoostGesturesEvent = v1;
	 /* .line 236 */
	 int v2 = 0; // const/4 v2, 0x0
	 /* iput v2, p0, Lcom/miui/server/smartpower/SmartDisplayPolicyManager;->mLastInputMethodStatus:I */
	 /* .line 237 */
	 final String v3 = ""; // const-string v3, ""
	 this.mLastFocusPackage = v3;
	 /* .line 239 */
	 /* iput-boolean v2, p0, Lcom/miui/server/smartpower/SmartDisplayPolicyManager;->mExternalDisplayConnected:Z */
	 /* .line 241 */
	 /* iput-boolean v2, p0, Lcom/miui/server/smartpower/SmartDisplayPolicyManager;->mFrameInsertingForGame:Z */
	 /* .line 242 */
	 /* iput-boolean v2, p0, Lcom/miui/server/smartpower/SmartDisplayPolicyManager;->mFrameInsertingForVideo:Z */
	 /* .line 243 */
	 this.mFrameInsertScene = v3;
	 /* .line 245 */
	 /* iput-boolean v2, p0, Lcom/miui/server/smartpower/SmartDisplayPolicyManager;->mIsCameraInForeground:Z */
	 /* .line 247 */
	 /* iput-boolean v2, p0, Lcom/miui/server/smartpower/SmartDisplayPolicyManager;->isTempWarning:Z */
	 /* .line 248 */
	 /* iput v2, p0, Lcom/miui/server/smartpower/SmartDisplayPolicyManager;->mCurrentTemp:I */
	 /* .line 251 */
	 this.mContext = p1;
	 /* .line 252 */
	 this.mWMS = p2;
	 /* .line 253 */
	 final String v2 = "SurfaceFlinger"; // const-string v2, "SurfaceFlinger"
	 android.os.ServiceManager .getService ( v2 );
	 this.mSurfaceFlinger = v2;
	 /* .line 255 */
	 com.android.server.display.DisplayManagerServiceStub .getInstance ( );
	 this.mDisplayManagerServiceStub = v2;
	 /* .line 256 */
	 com.android.server.display.mode.DisplayModeDirectorStub .getInstance ( );
	 this.mDisplayModeDirectorStub = v2;
	 /* .line 258 */
	 (( android.os.HandlerThread ) v0 ).start ( ); // invoke-virtual {v0}, Landroid/os/HandlerThread;->start()V
	 /* .line 259 */
	 /* new-instance v2, Lcom/miui/server/smartpower/SmartDisplayPolicyManager$H; */
	 (( android.os.HandlerThread ) v0 ).getLooper ( ); // invoke-virtual {v0}, Landroid/os/HandlerThread;->getLooper()Landroid/os/Looper;
	 /* invoke-direct {v2, p0, v3}, Lcom/miui/server/smartpower/SmartDisplayPolicyManager$H;-><init>(Lcom/miui/server/smartpower/SmartDisplayPolicyManager;Landroid/os/Looper;)V */
	 this.mHandler = v2;
	 /* .line 260 */
	 /* nop */
	 /* .line 261 */
	 v0 = 	 (( android.os.HandlerThread ) v0 ).getThreadId ( ); // invoke-virtual {v0}, Landroid/os/HandlerThread;->getThreadId()I
	 /* .line 260 */
	 int v3 = 1; // const/4 v3, 0x1
	 android.os.Process .setThreadGroupAndCpuset ( v0,v3 );
	 /* .line 263 */
	 /* new-instance v0, Lcom/miui/server/smartpower/SmartDisplayPolicyManager$FrameInsertionObserver; */
	 /* invoke-direct {v0, p0, p1, v2}, Lcom/miui/server/smartpower/SmartDisplayPolicyManager$FrameInsertionObserver;-><init>(Lcom/miui/server/smartpower/SmartDisplayPolicyManager;Landroid/content/Context;Landroid/os/Handler;)V */
	 this.mFrameInsertionObserver = v0;
	 /* .line 265 */
	 /* new-instance v0, Lcom/miui/server/smartpower/SmartDisplayPolicyManager$RefreshRatePolicyController; */
	 /* invoke-direct {v0, p0, p1, v2, v1}, Lcom/miui/server/smartpower/SmartDisplayPolicyManager$RefreshRatePolicyController;-><init>(Lcom/miui/server/smartpower/SmartDisplayPolicyManager;Landroid/content/Context;Landroid/os/Handler;Lcom/miui/server/smartpower/SmartDisplayPolicyManager$RefreshRatePolicyController-IA;)V */
	 this.mRefreshRatePolicyController = v0;
	 /* .line 266 */
	 return;
} // .end method
private android.view.SurfaceControl$RefreshRateRange buildRefreshRateRange ( Float p0, Float p1 ) {
	 /* .locals 1 */
	 /* .param p1, "min" # F */
	 /* .param p2, "max" # F */
	 /* .line 796 */
	 /* new-instance v0, Landroid/view/SurfaceControl$RefreshRateRange; */
	 /* invoke-direct {v0, p1, p2}, Landroid/view/SurfaceControl$RefreshRateRange;-><init>(FF)V */
} // .end method
private void endControlRefreshRatePolicy ( Integer p0 ) {
	 /* .locals 12 */
	 /* .param p1, "interactType" # I */
	 /* .line 719 */
	 v0 = this.mRefreshRatePolicyController;
	 v0 = 	 com.miui.server.smartpower.SmartDisplayPolicyManager$RefreshRatePolicyController .-$$Nest$mgetMinRefreshRateInPolicy ( v0 );
	 /* .line 720 */
	 /* .local v0, "minRefreshRateInPolicy":I */
	 /* invoke-direct {p0, p1}, Lcom/miui/server/smartpower/SmartDisplayPolicyManager;->getDelayDownTimeForInteraction(I)J */
	 /* move-result-wide v8 */
	 /* .line 722 */
	 /* .local v8, "delayDownMs":J */
	 v10 = this.mRefreshRatePolicyController;
	 /* new-instance v11, Lcom/miui/server/smartpower/SmartDisplayPolicyManager$ComingInteraction; */
	 int v7 = 0; // const/4 v7, 0x0
	 /* move-object v1, v11 */
	 /* move v2, v0 */
	 /* move v3, v0 */
	 /* move v4, p1 */
	 /* move-wide v5, v8 */
	 /* invoke-direct/range {v1 ..v7}, Lcom/miui/server/smartpower/SmartDisplayPolicyManager$ComingInteraction;-><init>(IIIJLcom/miui/server/smartpower/SmartDisplayPolicyManager$ComingInteraction-IA;)V */
	 com.miui.server.smartpower.SmartDisplayPolicyManager$RefreshRatePolicyController .-$$Nest$mnotifyInteractionEnd ( v10,v11 );
	 /* .line 724 */
	 return;
} // .end method
private Integer findBaseSfModeId ( Float p0 ) {
	 /* .locals 4 */
	 /* .param p1, "upRefreshRate" # F */
	 /* .line 856 */
	 v0 = this.mDisplayLock;
	 /* monitor-enter v0 */
	 /* .line 857 */
	 try { // :try_start_0
		 v1 = 		 /* invoke-direct {p0}, Lcom/miui/server/smartpower/SmartDisplayPolicyManager;->isSwitchingResolution()Z */
		 if ( v1 != null) { // if-eqz v1, :cond_0
			 /* monitor-exit v0 */
			 int v0 = -1; // const/4 v0, -0x1
			 /* .line 859 */
		 } // :cond_0
		 /* iget v1, p0, Lcom/miui/server/smartpower/SmartDisplayPolicyManager;->mDisplayModeWidth:I */
		 if ( v1 != null) { // if-eqz v1, :cond_1
			 /* iget v1, p0, Lcom/miui/server/smartpower/SmartDisplayPolicyManager;->mDisplayModeHeight:I */
			 /* if-nez v1, :cond_2 */
			 /* .line 860 */
		 } // :cond_1
		 /* new-instance v1, Landroid/graphics/Point; */
		 /* invoke-direct {v1}, Landroid/graphics/Point;-><init>()V */
		 /* .line 861 */
		 /* .local v1, "displaySize":Landroid/graphics/Point; */
		 v2 = this.mWMS;
		 int v3 = 0; // const/4 v3, 0x0
		 (( com.android.server.wm.WindowManagerService ) v2 ).getInitialDisplaySize ( v3, v1 ); // invoke-virtual {v2, v3, v1}, Lcom/android/server/wm/WindowManagerService;->getInitialDisplaySize(ILandroid/graphics/Point;)V
		 /* .line 862 */
		 /* iget v2, v1, Landroid/graphics/Point;->x:I */
		 /* iget v3, v1, Landroid/graphics/Point;->y:I */
		 /* invoke-direct {p0, v2, v3}, Lcom/miui/server/smartpower/SmartDisplayPolicyManager;->setDisplaySize(II)V */
		 /* .line 864 */
	 } // .end local v1 # "displaySize":Landroid/graphics/Point;
} // :cond_2
v1 = /* invoke-direct {p0, p1}, Lcom/miui/server/smartpower/SmartDisplayPolicyManager;->findBaseSfModeIdLocked(F)I */
/* monitor-exit v0 */
/* .line 865 */
/* :catchall_0 */
/* move-exception v1 */
/* monitor-exit v0 */
/* :try_end_0 */
/* .catchall {:try_start_0 ..:try_end_0} :catchall_0 */
/* throw v1 */
} // .end method
private Integer findBaseSfModeIdLocked ( Float p0 ) {
/* .locals 6 */
/* .param p1, "upRefreshRate" # F */
/* .line 869 */
v0 = this.mSupportedDisplayModes;
/* array-length v1, v0 */
int v2 = 0; // const/4 v2, 0x0
} // :goto_0
/* if-ge v2, v1, :cond_2 */
/* aget-object v3, v0, v2 */
/* .line 870 */
/* .local v3, "mode":Landroid/view/SurfaceControl$DisplayMode; */
/* iget v4, v3, Landroid/view/SurfaceControl$DisplayMode;->refreshRate:F */
/* cmpl-float v4, p1, v4 */
/* if-nez v4, :cond_1 */
/* iget v4, p0, Lcom/miui/server/smartpower/SmartDisplayPolicyManager;->mDisplayModeWidth:I */
/* iget v5, v3, Landroid/view/SurfaceControl$DisplayMode;->width:I */
/* if-ne v4, v5, :cond_1 */
/* iget v4, p0, Lcom/miui/server/smartpower/SmartDisplayPolicyManager;->mDisplayModeHeight:I */
/* iget v5, v3, Landroid/view/SurfaceControl$DisplayMode;->height:I */
/* if-ne v4, v5, :cond_1 */
/* .line 872 */
/* sget-boolean v0, Lcom/miui/server/smartpower/SmartDisplayPolicyManager;->DEBUG:Z */
if ( v0 != null) { // if-eqz v0, :cond_0
/* .line 873 */
/* new-instance v0, Ljava/lang/StringBuilder; */
/* invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V */
/* const-string/jumbo v1, "select mode: " */
(( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v0 ).append ( v3 ); // invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v0 ).toString ( ); // invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
final String v1 = "SmartPower.DisplayPolicy"; // const-string v1, "SmartPower.DisplayPolicy"
android.util.Slog .d ( v1,v0 );
/* .line 875 */
} // :cond_0
/* iget v0, v3, Landroid/view/SurfaceControl$DisplayMode;->id:I */
/* .line 869 */
} // .end local v3 # "mode":Landroid/view/SurfaceControl$DisplayMode;
} // :cond_1
/* add-int/lit8 v2, v2, 0x1 */
/* .line 878 */
} // :cond_2
int v0 = -1; // const/4 v0, -0x1
} // .end method
private java.lang.String getCurrentFocusPackageName ( ) {
/* .locals 2 */
/* .line 843 */
com.android.server.wm.RealTimeModeControllerStub .get ( );
/* .line 844 */
/* .local v0, "focusedPackage":Ljava/lang/String; */
if ( v0 != null) { // if-eqz v0, :cond_0
/* move-object v1, v0 */
} // :cond_0
final String v1 = ""; // const-string v1, ""
} // :goto_0
} // .end method
private Long getDelayDownTimeForInteraction ( Integer p0 ) {
/* .locals 2 */
/* .param p1, "interactType" # I */
/* .line 731 */
v0 = this.mRefreshRatePolicyController;
com.miui.server.smartpower.SmartDisplayPolicyManager$RefreshRatePolicyController .-$$Nest$mgetDelayDownTimeForInteraction ( v0,p1 );
/* move-result-wide v0 */
/* return-wide v0 */
} // .end method
private Long getDurationForInteraction ( Integer p0 ) {
/* .locals 2 */
/* .param p1, "interactType" # I */
/* .line 727 */
v0 = this.mRefreshRatePolicyController;
com.miui.server.smartpower.SmartDisplayPolicyManager$RefreshRatePolicyController .-$$Nest$mgetDurationForInteraction ( v0,p1 );
/* move-result-wide v0 */
/* return-wide v0 */
} // .end method
private java.lang.String getForegroundPackageName ( ) {
/* .locals 1 */
/* .line 848 */
com.android.server.am.SystemPressureControllerStub .getInstance ( );
(( com.android.server.am.SystemPressureControllerStub ) v0 ).getForegroundPackageName ( ); // invoke-virtual {v0}, Lcom/android/server/am/SystemPressureControllerStub;->getForegroundPackageName()Ljava/lang/String;
} // .end method
private Integer getInteractTypeForMultiTaskAction ( Integer p0 ) {
/* .locals 1 */
/* .param p1, "type" # I */
/* .line 608 */
v0 = miui.smartpower.MultiTaskActionManager .isFreeFormAction ( p1 );
if ( v0 != null) { // if-eqz v0, :cond_0
/* .line 609 */
/* const/16 v0, 0xc */
/* .line 610 */
} // :cond_0
v0 = miui.smartpower.MultiTaskActionManager .isSplictScreenAction ( p1 );
if ( v0 != null) { // if-eqz v0, :cond_1
/* .line 611 */
/* const/16 v0, 0xd */
/* .line 612 */
} // :cond_1
v0 = miui.smartpower.MultiTaskActionManager .isCVWAction ( p1 );
if ( v0 != null) { // if-eqz v0, :cond_2
/* .line 613 */
/* const/16 v0, 0xe */
/* .line 614 */
} // :cond_2
v0 = miui.smartpower.MultiTaskActionManager .isMultiWindowSwitchAction ( p1 );
if ( v0 != null) { // if-eqz v0, :cond_3
/* .line 615 */
/* const/16 v0, 0xf */
/* .line 617 */
} // :cond_3
int v0 = 0; // const/4 v0, 0x0
} // .end method
private android.view.SurfaceControl$DisplayMode getSupportDisplayModes ( ) {
/* .locals 5 */
/* .line 381 */
v0 = this.mDisplayManagerServiceStub;
/* .line 382 */
(( com.android.server.display.DisplayManagerServiceStub ) v0 ).updateDefaultDisplaySupportMode ( ); // invoke-virtual {v0}, Lcom/android/server/display/DisplayManagerServiceStub;->updateDefaultDisplaySupportMode()Landroid/view/SurfaceControl$DynamicDisplayInfo;
/* .line 383 */
/* .local v0, "dynamicDisplayInfo":Landroid/view/SurfaceControl$DynamicDisplayInfo; */
final String v1 = "SmartPower.DisplayPolicy"; // const-string v1, "SmartPower.DisplayPolicy"
/* if-nez v0, :cond_0 */
/* .line 384 */
final String v2 = "get dynamic display info error"; // const-string v2, "get dynamic display info error"
android.util.Slog .e ( v1,v2 );
/* .line 385 */
int v1 = 0; // const/4 v1, 0x0
/* .line 387 */
} // :cond_0
v2 = this.supportedDisplayModes;
/* .line 388 */
/* .local v2, "supportedDisplayModes":[Landroid/view/SurfaceControl$DisplayMode; */
if ( v2 != null) { // if-eqz v2, :cond_1
/* .line 389 */
/* sget-boolean v3, Lcom/miui/server/smartpower/SmartDisplayPolicyManager;->DEBUG:Z */
if ( v3 != null) { // if-eqz v3, :cond_1
/* .line 390 */
/* new-instance v3, Ljava/lang/StringBuilder; */
/* invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V */
/* const-string/jumbo v4, "update display modes: " */
(( java.lang.StringBuilder ) v3 ).append ( v4 ); // invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
java.util.Arrays .toString ( v2 );
(( java.lang.StringBuilder ) v3 ).append ( v4 ); // invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v3 ).toString ( ); // invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
android.util.Slog .d ( v1,v3 );
/* .line 393 */
} // :cond_1
} // .end method
private static java.lang.String interactionTypeToString ( Integer p0 ) {
/* .locals 1 */
/* .param p0, "type" # I */
/* .line 1547 */
/* packed-switch p0, :pswitch_data_0 */
/* .line 1579 */
/* :pswitch_0 */
java.lang.String .valueOf ( p0 );
/* .line 1577 */
/* :pswitch_1 */
final String v0 = "multi task: multi window"; // const-string v0, "multi task: multi window"
/* .line 1575 */
/* :pswitch_2 */
final String v0 = "multi task: cvw gesture"; // const-string v0, "multi task: cvw gesture"
/* .line 1573 */
/* :pswitch_3 */
final String v0 = "multi task: split screen"; // const-string v0, "multi task: split screen"
/* .line 1571 */
/* :pswitch_4 */
final String v0 = "multi task: free form"; // const-string v0, "multi task: free form"
/* .line 1569 */
/* :pswitch_5 */
/* const-string/jumbo v0, "window visible changed" */
/* .line 1567 */
/* :pswitch_6 */
final String v0 = "input: fling"; // const-string v0, "input: fling"
/* .line 1565 */
/* :pswitch_7 */
final String v0 = "input: move"; // const-string v0, "input: move"
/* .line 1563 */
/* :pswitch_8 */
final String v0 = "input: down"; // const-string v0, "input: down"
/* .line 1561 */
/* :pswitch_9 */
final String v0 = "focus window changed"; // const-string v0, "focus window changed"
/* .line 1559 */
/* :pswitch_a */
final String v0 = "remote animation"; // const-string v0, "remote animation"
/* .line 1557 */
/* :pswitch_b */
final String v0 = "recent animation"; // const-string v0, "recent animation"
/* .line 1555 */
/* :pswitch_c */
/* const-string/jumbo v0, "window animation" */
/* .line 1553 */
/* :pswitch_d */
final String v0 = "app transition"; // const-string v0, "app transition"
/* .line 1551 */
/* :pswitch_e */
final String v0 = "base"; // const-string v0, "base"
/* .line 1549 */
/* :pswitch_f */
final String v0 = "none"; // const-string v0, "none"
/* nop */
/* :pswitch_data_0 */
/* .packed-switch 0x0 */
/* :pswitch_f */
/* :pswitch_e */
/* :pswitch_d */
/* :pswitch_c */
/* :pswitch_0 */
/* :pswitch_b */
/* :pswitch_a */
/* :pswitch_9 */
/* :pswitch_8 */
/* :pswitch_7 */
/* :pswitch_6 */
/* :pswitch_5 */
/* :pswitch_4 */
/* :pswitch_3 */
/* :pswitch_2 */
/* :pswitch_1 */
} // .end packed-switch
} // .end method
private Boolean isSupportSmartDisplayPolicy ( ) {
/* .locals 2 */
/* .line 833 */
/* sget-boolean v0, Lcom/miui/server/smartpower/SmartDisplayPolicyManager;->ENABLE:Z */
int v1 = 0; // const/4 v1, 0x0
/* if-nez v0, :cond_0 */
/* .line 834 */
/* .line 836 */
} // :cond_0
v0 = this.mSupportedDisplayModes;
/* if-nez v0, :cond_1 */
/* .line 837 */
/* invoke-direct {p0}, Lcom/miui/server/smartpower/SmartDisplayPolicyManager;->updateSupportDisplayModes()V */
/* .line 839 */
} // :cond_1
v0 = this.mSupportedDisplayModes;
if ( v0 != null) { // if-eqz v0, :cond_2
int v1 = 1; // const/4 v1, 0x1
} // :cond_2
} // .end method
private Boolean isSwitchingResolution ( ) {
/* .locals 1 */
/* .line 852 */
v0 = com.android.server.wm.WindowManagerServiceStub .get ( );
} // .end method
private void notifyDisplayModeSpecsChanged ( ) {
/* .locals 1 */
/* .line 803 */
int v0 = 0; // const/4 v0, 0x0
/* invoke-direct {p0, v0}, Lcom/miui/server/smartpower/SmartDisplayPolicyManager;->notifySurfaceFlingerInteractionChanged(I)V */
/* .line 804 */
v0 = this.mDisplayModeDirectorStub;
/* .line 805 */
return;
} // .end method
private void notifyInputEventReceived ( Integer p0 ) {
/* .locals 2 */
/* .param p1, "inputType" # I */
/* .line 544 */
/* invoke-direct {p0, p1}, Lcom/miui/server/smartpower/SmartDisplayPolicyManager;->getDurationForInteraction(I)J */
/* move-result-wide v0 */
/* invoke-direct {p0, p1, v0, v1}, Lcom/miui/server/smartpower/SmartDisplayPolicyManager;->notifyInputEventReceived(IJ)V */
/* .line 545 */
return;
} // .end method
private void notifyInputEventReceived ( Integer p0, Long p1 ) {
/* .locals 3 */
/* .param p1, "inputType" # I */
/* .param p2, "durationMs" # J */
/* .line 548 */
v0 = /* invoke-direct {p0}, Lcom/miui/server/smartpower/SmartDisplayPolicyManager;->isSupportSmartDisplayPolicy()Z */
/* if-nez v0, :cond_0 */
return;
/* .line 550 */
} // :cond_0
/* invoke-direct {p0}, Lcom/miui/server/smartpower/SmartDisplayPolicyManager;->getCurrentFocusPackageName()Ljava/lang/String; */
/* .line 551 */
/* .local v0, "focusedPackage":Ljava/lang/String; */
v1 = this.mRefreshRatePolicyController;
v1 = com.miui.server.smartpower.SmartDisplayPolicyManager$RefreshRatePolicyController .-$$Nest$misInputPackageWhiteList ( v1,v0 );
if ( v1 != null) { // if-eqz v1, :cond_2
/* .line 552 */
/* sget-boolean v1, Lcom/miui/server/smartpower/SmartDisplayPolicyManager;->DEBUG:Z */
if ( v1 != null) { // if-eqz v1, :cond_1
/* .line 553 */
/* new-instance v1, Ljava/lang/StringBuilder; */
/* invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V */
final String v2 = "notifyInputEventReceived, "; // const-string v2, "notifyInputEventReceived, "
(( java.lang.StringBuilder ) v1 ).append ( v2 ); // invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
com.miui.server.smartpower.SmartDisplayPolicyManager .interactionTypeToString ( p1 );
(( java.lang.StringBuilder ) v1 ).append ( v2 ); // invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
final String v2 = ", durationMs: "; // const-string v2, ", durationMs: "
(( java.lang.StringBuilder ) v1 ).append ( v2 ); // invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v1 ).append ( p2, p3 ); // invoke-virtual {v1, p2, p3}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;
final String v2 = ", focus: "; // const-string v2, ", focus: "
(( java.lang.StringBuilder ) v1 ).append ( v2 ); // invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v1 ).append ( v0 ); // invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v1 ).toString ( ); // invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
final String v2 = "SmartPower.DisplayPolicy"; // const-string v2, "SmartPower.DisplayPolicy"
android.util.Slog .d ( v2,v1 );
/* .line 556 */
} // :cond_1
/* invoke-direct {p0, p1, p2, p3}, Lcom/miui/server/smartpower/SmartDisplayPolicyManager;->startControlRefreshRatePolicy(IJ)V */
/* .line 558 */
} // :cond_2
return;
} // .end method
private void notifyInputMethodAnimationStart ( Integer p0 ) {
/* .locals 2 */
/* .param p1, "status" # I */
/* .line 468 */
int v0 = 1; // const/4 v0, 0x1
/* if-ne p1, v0, :cond_0 */
/* iget v1, p0, Lcom/miui/server/smartpower/SmartDisplayPolicyManager;->mLastInputMethodStatus:I */
/* if-ne v1, v0, :cond_0 */
/* .line 469 */
return;
/* .line 474 */
} // :cond_0
/* if-eq p1, v0, :cond_1 */
/* if-nez p1, :cond_2 */
v0 = this.mLastFocusPackage;
/* .line 475 */
/* invoke-direct {p0}, Lcom/miui/server/smartpower/SmartDisplayPolicyManager;->getCurrentFocusPackageName()Ljava/lang/String; */
v0 = (( java.lang.String ) v0 ).equals ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z
if ( v0 != null) { // if-eqz v0, :cond_2
/* .line 476 */
} // :cond_1
int v0 = 4; // const/4 v0, 0x4
/* invoke-direct {p0, v0}, Lcom/miui/server/smartpower/SmartDisplayPolicyManager;->startControlRefreshRatePolicy(I)V */
/* .line 478 */
} // :cond_2
/* iput p1, p0, Lcom/miui/server/smartpower/SmartDisplayPolicyManager;->mLastInputMethodStatus:I */
/* .line 479 */
/* invoke-direct {p0}, Lcom/miui/server/smartpower/SmartDisplayPolicyManager;->getCurrentFocusPackageName()Ljava/lang/String; */
this.mLastFocusPackage = v0;
/* .line 480 */
return;
} // .end method
private void notifySurfaceFlingerInteractionChanged ( Integer p0 ) {
/* .locals 5 */
/* .param p1, "actionType" # I */
/* .line 808 */
v0 = this.mSurfaceFlinger;
/* if-nez v0, :cond_0 */
return;
/* .line 810 */
} // :cond_0
android.os.Parcel .obtain ( );
/* .line 811 */
/* .local v0, "data":Landroid/os/Parcel; */
final String v1 = "android.ui.ISurfaceComposer"; // const-string v1, "android.ui.ISurfaceComposer"
(( android.os.Parcel ) v0 ).writeInterfaceToken ( v1 ); // invoke-virtual {v0, v1}, Landroid/os/Parcel;->writeInterfaceToken(Ljava/lang/String;)V
/* .line 812 */
/* const/16 v1, 0x10c */
(( android.os.Parcel ) v0 ).writeInt ( v1 ); // invoke-virtual {v0, v1}, Landroid/os/Parcel;->writeInt(I)V
/* .line 813 */
(( android.os.Parcel ) v0 ).writeInt ( p1 ); // invoke-virtual {v0, p1}, Landroid/os/Parcel;->writeInt(I)V
/* .line 814 */
final String v1 = "perf_interaction"; // const-string v1, "perf_interaction"
(( android.os.Parcel ) v0 ).writeString ( v1 ); // invoke-virtual {v0, v1}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V
/* .line 816 */
try { // :try_start_0
v1 = this.mSurfaceFlinger;
int v2 = 0; // const/4 v2, 0x0
int v3 = 1; // const/4 v3, 0x1
/* const/16 v4, 0x7983 */
/* :try_end_0 */
/* .catch Landroid/os/RemoteException; {:try_start_0 ..:try_end_0} :catch_0 */
/* .catch Ljava/lang/SecurityException; {:try_start_0 ..:try_end_0} :catch_0 */
/* .catchall {:try_start_0 ..:try_end_0} :catchall_0 */
/* .line 821 */
/* nop */
} // :goto_0
(( android.os.Parcel ) v0 ).recycle ( ); // invoke-virtual {v0}, Landroid/os/Parcel;->recycle()V
/* .line 822 */
/* .line 821 */
/* :catchall_0 */
/* move-exception v1 */
/* .line 818 */
/* :catch_0 */
/* move-exception v1 */
/* .line 819 */
/* .local v1, "ex":Ljava/lang/Exception; */
try { // :try_start_1
final String v2 = "SmartPower.DisplayPolicy"; // const-string v2, "SmartPower.DisplayPolicy"
final String v3 = "Failed to notify idle to SurfaceFlinger"; // const-string v3, "Failed to notify idle to SurfaceFlinger"
android.util.Slog .e ( v2,v3,v1 );
/* :try_end_1 */
/* .catchall {:try_start_1 ..:try_end_1} :catchall_0 */
/* .line 821 */
/* nop */
} // .end local v1 # "ex":Ljava/lang/Exception;
/* .line 823 */
} // :goto_1
return;
/* .line 821 */
} // :goto_2
(( android.os.Parcel ) v0 ).recycle ( ); // invoke-virtual {v0}, Landroid/os/Parcel;->recycle()V
/* .line 822 */
/* throw v1 */
} // .end method
private void realControlRefreshRate ( Integer p0, Boolean p1, Float p2, Float p3 ) {
/* .locals 6 */
/* .param p1, "baseSfModeId" # I */
/* .param p2, "allowGroupSwitching" # Z */
/* .param p3, "minUpRefreshRate" # F */
/* .param p4, "maxUpRefreshRate" # F */
/* .line 782 */
int v0 = 1; // const/4 v0, 0x1
/* invoke-direct {p0, v0}, Lcom/miui/server/smartpower/SmartDisplayPolicyManager;->notifySurfaceFlingerInteractionChanged(I)V */
/* .line 784 */
int v0 = 0; // const/4 v0, 0x0
/* const/high16 v1, 0x7f800000 # Float.POSITIVE_INFINITY */
/* invoke-direct {p0, v0, v1}, Lcom/miui/server/smartpower/SmartDisplayPolicyManager;->buildRefreshRateRange(FF)Landroid/view/SurfaceControl$RefreshRateRange; */
/* .line 785 */
/* .local v0, "physical":Landroid/view/SurfaceControl$RefreshRateRange; */
/* invoke-direct {p0, p3, p4}, Lcom/miui/server/smartpower/SmartDisplayPolicyManager;->buildRefreshRateRange(FF)Landroid/view/SurfaceControl$RefreshRateRange; */
/* .line 787 */
/* .local v1, "render":Landroid/view/SurfaceControl$RefreshRateRange; */
/* new-instance v2, Landroid/view/SurfaceControl$RefreshRateRanges; */
/* invoke-direct {v2, v0, v1}, Landroid/view/SurfaceControl$RefreshRateRanges;-><init>(Landroid/view/SurfaceControl$RefreshRateRange;Landroid/view/SurfaceControl$RefreshRateRange;)V */
/* .line 788 */
/* .local v2, "primaryRanges":Landroid/view/SurfaceControl$RefreshRateRanges; */
/* new-instance v3, Landroid/view/SurfaceControl$RefreshRateRanges; */
/* invoke-direct {v3, v0, v1}, Landroid/view/SurfaceControl$RefreshRateRanges;-><init>(Landroid/view/SurfaceControl$RefreshRateRange;Landroid/view/SurfaceControl$RefreshRateRange;)V */
/* .line 790 */
/* .local v3, "appRequestRanges":Landroid/view/SurfaceControl$RefreshRateRanges; */
/* new-instance v4, Landroid/view/SurfaceControl$DesiredDisplayModeSpecs; */
/* invoke-direct {v4, p1, p2, v2, v3}, Landroid/view/SurfaceControl$DesiredDisplayModeSpecs;-><init>(IZLandroid/view/SurfaceControl$RefreshRateRanges;Landroid/view/SurfaceControl$RefreshRateRanges;)V */
/* .line 792 */
/* .local v4, "desired":Landroid/view/SurfaceControl$DesiredDisplayModeSpecs; */
v5 = this.mDisplayManagerServiceStub;
(( com.android.server.display.DisplayManagerServiceStub ) v5 ).shouldUpdateDisplayModeSpecs ( v4 ); // invoke-virtual {v5, v4}, Lcom/android/server/display/DisplayManagerServiceStub;->shouldUpdateDisplayModeSpecs(Landroid/view/SurfaceControl$DesiredDisplayModeSpecs;)V
/* .line 793 */
return;
} // .end method
private void resetDisplaySize ( ) {
/* .locals 2 */
/* .line 361 */
v0 = this.mDisplayLock;
/* monitor-enter v0 */
/* .line 362 */
int v1 = 0; // const/4 v1, 0x0
try { // :try_start_0
/* iput v1, p0, Lcom/miui/server/smartpower/SmartDisplayPolicyManager;->mDisplayModeWidth:I */
/* .line 363 */
/* iput v1, p0, Lcom/miui/server/smartpower/SmartDisplayPolicyManager;->mDisplayModeHeight:I */
/* .line 364 */
/* monitor-exit v0 */
/* .line 365 */
return;
/* .line 364 */
/* :catchall_0 */
/* move-exception v1 */
/* monitor-exit v0 */
/* :try_end_0 */
/* .catchall {:try_start_0 ..:try_end_0} :catchall_0 */
/* throw v1 */
} // .end method
private void setDisplaySize ( Integer p0, Integer p1 ) {
/* .locals 4 */
/* .param p1, "width" # I */
/* .param p2, "height" # I */
/* .line 349 */
v0 = this.mDisplayLock;
/* monitor-enter v0 */
/* .line 350 */
try { // :try_start_0
/* iput p1, p0, Lcom/miui/server/smartpower/SmartDisplayPolicyManager;->mDisplayModeWidth:I */
/* .line 351 */
/* iput p2, p0, Lcom/miui/server/smartpower/SmartDisplayPolicyManager;->mDisplayModeHeight:I */
/* .line 353 */
/* sget-boolean v1, Lcom/miui/server/smartpower/SmartDisplayPolicyManager;->DEBUG:Z */
if ( v1 != null) { // if-eqz v1, :cond_0
/* .line 354 */
final String v1 = "SmartPower.DisplayPolicy"; // const-string v1, "SmartPower.DisplayPolicy"
/* new-instance v2, Ljava/lang/StringBuilder; */
/* invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V */
/* const-string/jumbo v3, "update display size: " */
(( java.lang.StringBuilder ) v2 ).append ( v3 ); // invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
/* iget v3, p0, Lcom/miui/server/smartpower/SmartDisplayPolicyManager;->mDisplayModeWidth:I */
(( java.lang.StringBuilder ) v2 ).append ( v3 ); // invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;
final String v3 = " * "; // const-string v3, " * "
(( java.lang.StringBuilder ) v2 ).append ( v3 ); // invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
/* iget v3, p0, Lcom/miui/server/smartpower/SmartDisplayPolicyManager;->mDisplayModeHeight:I */
(( java.lang.StringBuilder ) v2 ).append ( v3 ); // invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v2 ).toString ( ); // invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
android.util.Slog .d ( v1,v2 );
/* .line 357 */
} // :cond_0
/* monitor-exit v0 */
/* .line 358 */
return;
/* .line 357 */
/* :catchall_0 */
/* move-exception v1 */
/* monitor-exit v0 */
/* :try_end_0 */
/* .catchall {:try_start_0 ..:try_end_0} :catchall_0 */
/* throw v1 */
} // .end method
private void shouldDownRefreshRate ( ) {
/* .locals 2 */
/* .line 767 */
v0 = this.mHandler;
int v1 = 2; // const/4 v1, 0x2
(( com.miui.server.smartpower.SmartDisplayPolicyManager$H ) v0 ).removeMessages ( v1 ); // invoke-virtual {v0, v1}, Lcom/miui/server/smartpower/SmartDisplayPolicyManager$H;->removeMessages(I)V
/* .line 769 */
v0 = this.mRefreshRatePolicyController;
com.miui.server.smartpower.SmartDisplayPolicyManager$RefreshRatePolicyController .-$$Nest$mreset ( v0 );
/* .line 770 */
/* invoke-direct {p0}, Lcom/miui/server/smartpower/SmartDisplayPolicyManager;->notifyDisplayModeSpecsChanged()V */
/* .line 772 */
/* sget-boolean v0, Lcom/miui/server/smartpower/SmartDisplayPolicyManager;->DEBUG:Z */
if ( v0 != null) { // if-eqz v0, :cond_0
/* .line 773 */
final String v0 = "SmartPower.DisplayPolicy"; // const-string v0, "SmartPower.DisplayPolicy"
final String v1 = "down refreshrate"; // const-string v1, "down refreshrate"
android.util.Slog .d ( v0,v1 );
/* .line 775 */
} // :cond_0
return;
} // .end method
private void shouldUpRefreshRate ( com.miui.server.smartpower.SmartDisplayPolicyManager$ComingInteraction p0 ) {
/* .locals 9 */
/* .param p1, "comingInteraction" # Lcom/miui/server/smartpower/SmartDisplayPolicyManager$ComingInteraction; */
/* .line 739 */
v0 = com.miui.server.smartpower.SmartDisplayPolicyManager$ComingInteraction .-$$Nest$fgetmMaxRefreshRate ( p1 );
/* int-to-float v0, v0 */
/* .line 740 */
/* .local v0, "maxUpRefreshRate":F */
v1 = com.miui.server.smartpower.SmartDisplayPolicyManager$ComingInteraction .-$$Nest$fgetmMinRefreshRate ( p1 );
/* int-to-float v1, v1 */
/* .line 741 */
/* .local v1, "minUpRefreshRate":F */
com.miui.server.smartpower.SmartDisplayPolicyManager$ComingInteraction .-$$Nest$fgetmDuration ( p1 );
/* move-result-wide v2 */
/* .line 743 */
/* .local v2, "interactDuration":J */
v4 = /* invoke-direct {p0, v0}, Lcom/miui/server/smartpower/SmartDisplayPolicyManager;->findBaseSfModeId(F)I */
/* .line 744 */
/* .local v4, "baseSfModeId":I */
int v5 = -1; // const/4 v5, -0x1
final String v6 = "SmartPower.DisplayPolicy"; // const-string v6, "SmartPower.DisplayPolicy"
/* if-eq v4, v5, :cond_2 */
v5 = /* invoke-direct {p0}, Lcom/miui/server/smartpower/SmartDisplayPolicyManager;->isSwitchingResolution()Z */
if ( v5 != null) { // if-eqz v5, :cond_0
/* .line 752 */
} // :cond_0
int v5 = 0; // const/4 v5, 0x0
/* invoke-direct {p0, v4, v5, v1, v0}, Lcom/miui/server/smartpower/SmartDisplayPolicyManager;->realControlRefreshRate(IZFF)V */
/* .line 754 */
v5 = this.mHandler;
int v7 = 2; // const/4 v7, 0x2
(( com.miui.server.smartpower.SmartDisplayPolicyManager$H ) v5 ).obtainMessage ( v7 ); // invoke-virtual {v5, v7}, Lcom/miui/server/smartpower/SmartDisplayPolicyManager$H;->obtainMessage(I)Landroid/os/Message;
/* .line 755 */
/* .local v5, "msg":Landroid/os/Message; */
v7 = this.mHandler;
(( com.miui.server.smartpower.SmartDisplayPolicyManager$H ) v7 ).sendMessageDelayed ( v5, v2, v3 ); // invoke-virtual {v7, v5, v2, v3}, Lcom/miui/server/smartpower/SmartDisplayPolicyManager$H;->sendMessageDelayed(Landroid/os/Message;J)Z
/* .line 757 */
/* sget-boolean v7, Lcom/miui/server/smartpower/SmartDisplayPolicyManager;->DEBUG:Z */
if ( v7 != null) { // if-eqz v7, :cond_1
/* .line 758 */
/* new-instance v7, Ljava/lang/StringBuilder; */
/* invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V */
/* const-string/jumbo v8, "up refreshrate: " */
(( java.lang.StringBuilder ) v7 ).append ( v8 ); // invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v7 ).append ( v0 ); // invoke-virtual {v7, v0}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v7 ).toString ( ); // invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
android.util.Slog .d ( v6,v7 );
/* .line 760 */
} // :cond_1
return;
/* .line 745 */
} // .end local v5 # "msg":Landroid/os/Message;
} // :cond_2
} // :goto_0
v5 = this.mRefreshRatePolicyController;
com.miui.server.smartpower.SmartDisplayPolicyManager$RefreshRatePolicyController .-$$Nest$mreset ( v5 );
/* .line 746 */
/* sget-boolean v5, Lcom/miui/server/smartpower/SmartDisplayPolicyManager;->DEBUG:Z */
if ( v5 != null) { // if-eqz v5, :cond_3
/* .line 747 */
final String v5 = "invalid baseSfModeId or switching resolution now"; // const-string v5, "invalid baseSfModeId or switching resolution now"
android.util.Slog .d ( v6,v5 );
/* .line 749 */
} // :cond_3
return;
} // .end method
private void shouldUpdateDisplayDevice ( ) {
/* .locals 0 */
/* .line 668 */
/* invoke-direct {p0}, Lcom/miui/server/smartpower/SmartDisplayPolicyManager;->updateSupportDisplayModes()V */
/* .line 669 */
/* invoke-direct {p0}, Lcom/miui/server/smartpower/SmartDisplayPolicyManager;->resetDisplaySize()V */
/* .line 670 */
return;
} // .end method
private void startControlRefreshRatePolicy ( Integer p0 ) {
/* .locals 2 */
/* .param p1, "interactType" # I */
/* .line 707 */
/* invoke-direct {p0, p1}, Lcom/miui/server/smartpower/SmartDisplayPolicyManager;->getDurationForInteraction(I)J */
/* move-result-wide v0 */
/* invoke-direct {p0, p1, v0, v1}, Lcom/miui/server/smartpower/SmartDisplayPolicyManager;->startControlRefreshRatePolicy(IJ)V */
/* .line 708 */
return;
} // .end method
private void startControlRefreshRatePolicy ( Integer p0, Long p1 ) {
/* .locals 11 */
/* .param p1, "interactType" # I */
/* .param p2, "durationMs" # J */
/* .line 711 */
v0 = this.mRefreshRatePolicyController;
v0 = com.miui.server.smartpower.SmartDisplayPolicyManager$RefreshRatePolicyController .-$$Nest$mgetMaxRefreshRateInPolicy ( v0 );
/* .line 712 */
/* .local v0, "maxRefreshRateInPolicy":I */
v1 = this.mRefreshRatePolicyController;
v8 = com.miui.server.smartpower.SmartDisplayPolicyManager$RefreshRatePolicyController .-$$Nest$mgetMinRefreshRateInPolicy ( v1 );
/* .line 714 */
/* .local v8, "minRefreshRateInPolicy":I */
v9 = this.mRefreshRatePolicyController;
/* new-instance v10, Lcom/miui/server/smartpower/SmartDisplayPolicyManager$ComingInteraction; */
int v7 = 0; // const/4 v7, 0x0
/* move-object v1, v10 */
/* move v2, v8 */
/* move v3, v0 */
/* move v4, p1 */
/* move-wide v5, p2 */
/* invoke-direct/range {v1 ..v7}, Lcom/miui/server/smartpower/SmartDisplayPolicyManager$ComingInteraction;-><init>(IIIJLcom/miui/server/smartpower/SmartDisplayPolicyManager$ComingInteraction-IA;)V */
com.miui.server.smartpower.SmartDisplayPolicyManager$RefreshRatePolicyController .-$$Nest$mnotifyInteractionStart ( v9,v10 );
/* .line 716 */
return;
} // .end method
private void updateCameraInForegroundStatus ( Boolean p0 ) {
/* .locals 1 */
/* .param p1, "isInForeground" # Z */
/* .line 409 */
/* sget-boolean v0, Lcom/miui/server/smartpower/SmartDisplayPolicyManager;->CAMERA_REFRESH_RATE_ENABLE:Z */
/* if-nez v0, :cond_0 */
/* .line 410 */
return;
/* .line 412 */
} // :cond_0
/* iput-boolean p1, p0, Lcom/miui/server/smartpower/SmartDisplayPolicyManager;->mIsCameraInForeground:Z */
/* .line 413 */
if ( p1 != null) { // if-eqz p1, :cond_1
/* .line 414 */
/* const/16 v0, -0x63 */
/* invoke-direct {p0, v0}, Lcom/miui/server/smartpower/SmartDisplayPolicyManager;->endControlRefreshRatePolicy(I)V */
/* .line 416 */
} // :cond_1
return;
} // .end method
private void updatePolicyControllerLocked ( ) {
/* .locals 1 */
/* .line 397 */
v0 = this.mRefreshRatePolicyController;
com.miui.server.smartpower.SmartDisplayPolicyManager$RefreshRatePolicyController .-$$Nest$mupdateControllerLocked ( v0 );
/* .line 398 */
return;
} // .end method
private void updateSupportDisplayModes ( ) {
/* .locals 3 */
/* .line 368 */
/* invoke-direct {p0}, Lcom/miui/server/smartpower/SmartDisplayPolicyManager;->getSupportDisplayModes()[Landroid/view/SurfaceControl$DisplayMode; */
/* .line 369 */
/* .local v0, "displayModes":[Landroid/view/SurfaceControl$DisplayMode; */
v1 = this.mDisplayLock;
/* monitor-enter v1 */
/* .line 370 */
try { // :try_start_0
this.mSupportedDisplayModes = v0;
/* .line 371 */
/* invoke-direct {p0}, Lcom/miui/server/smartpower/SmartDisplayPolicyManager;->updatePolicyControllerLocked()V */
/* .line 372 */
/* monitor-exit v1 */
/* .line 373 */
return;
/* .line 372 */
/* :catchall_0 */
/* move-exception v2 */
/* monitor-exit v1 */
/* :try_end_0 */
/* .catchall {:try_start_0 ..:try_end_0} :catchall_0 */
/* throw v2 */
} // .end method
/* # virtual methods */
public void dump ( java.io.PrintWriter p0, java.lang.String[] p1, Integer p2 ) {
/* .locals 2 */
/* .param p1, "pw" # Ljava/io/PrintWriter; */
/* .param p2, "args" # [Ljava/lang/String; */
/* .param p3, "opti" # I */
/* .line 882 */
final String v0 = "SmartDisplayPolicyManager"; // const-string v0, "SmartDisplayPolicyManager"
(( java.io.PrintWriter ) p1 ).println ( v0 ); // invoke-virtual {p1, v0}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V
/* .line 883 */
/* new-instance v0, Ljava/lang/StringBuilder; */
/* invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V */
final String v1 = "enable: "; // const-string v1, "enable: "
(( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
v1 = /* invoke-direct {p0}, Lcom/miui/server/smartpower/SmartDisplayPolicyManager;->isSupportSmartDisplayPolicy()Z */
(( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v0 ).toString ( ); // invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
(( java.io.PrintWriter ) p1 ).println ( v0 ); // invoke-virtual {p1, v0}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V
/* .line 884 */
/* new-instance v0, Ljava/lang/StringBuilder; */
/* invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V */
/* const-string/jumbo v1, "supportedDisplayModes: " */
(( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
v1 = this.mSupportedDisplayModes;
java.util.Arrays .toString ( v1 );
(( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v0 ).toString ( ); // invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
(( java.io.PrintWriter ) p1 ).println ( v0 ); // invoke-virtual {p1, v0}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V
/* .line 885 */
/* new-instance v0, Ljava/lang/StringBuilder; */
/* invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V */
final String v1 = "camera refresh rate enable: "; // const-string v1, "camera refresh rate enable: "
(( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
/* sget-boolean v1, Lcom/miui/server/smartpower/SmartDisplayPolicyManager;->CAMERA_REFRESH_RATE_ENABLE:Z */
(( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v0 ).toString ( ); // invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
(( java.io.PrintWriter ) p1 ).println ( v0 ); // invoke-virtual {p1, v0}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V
/* .line 886 */
v0 = this.mRefreshRatePolicyController;
if ( v0 != null) { // if-eqz v0, :cond_0
/* .line 887 */
com.miui.server.smartpower.SmartDisplayPolicyManager$RefreshRatePolicyController .-$$Nest$mdump ( v0,p1,p2,p3 );
/* .line 889 */
} // :cond_0
return;
} // .end method
public void init ( com.miui.server.smartpower.SmartScenarioManager p0, com.miui.server.smartpower.SmartWindowPolicyManager p1 ) {
/* .locals 0 */
/* .param p1, "smartScenarioManager" # Lcom/miui/server/smartpower/SmartScenarioManager; */
/* .param p2, "smartWindowPolicyManager" # Lcom/miui/server/smartpower/SmartWindowPolicyManager; */
/* .line 270 */
this.mSmartScenarioManager = p1;
/* .line 271 */
this.mSmartWindowPolicyManager = p2;
/* .line 272 */
return;
} // .end method
public void notifyActivityStartUnchecked ( java.lang.String p0, Integer p1, Integer p2, java.lang.String p3, Integer p4, Integer p5, java.lang.String p6, Boolean p7 ) {
/* .locals 1 */
/* .param p1, "name" # Ljava/lang/String; */
/* .param p2, "uid" # I */
/* .param p3, "pid" # I */
/* .param p4, "packageName" # Ljava/lang/String; */
/* .param p5, "launchedFromUid" # I */
/* .param p6, "launchedFromPid" # I */
/* .param p7, "launchedFromPackage" # Ljava/lang/String; */
/* .param p8, "isColdStart" # Z */
/* .line 403 */
final String v0 = "com.android.camera"; // const-string v0, "com.android.camera"
v0 = (( java.lang.String ) v0 ).equals ( p4 ); // invoke-virtual {v0, p4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z
if ( v0 != null) { // if-eqz v0, :cond_0
/* .line 404 */
int v0 = 1; // const/4 v0, 0x1
/* invoke-direct {p0, v0}, Lcom/miui/server/smartpower/SmartDisplayPolicyManager;->updateCameraInForegroundStatus(Z)V */
/* .line 406 */
} // :cond_0
return;
} // .end method
public void notifyAppTransitionStartLocked ( Long p0 ) {
/* .locals 3 */
/* .param p1, "animDuration" # J */
/* .line 419 */
v0 = /* invoke-direct {p0}, Lcom/miui/server/smartpower/SmartDisplayPolicyManager;->isSupportSmartDisplayPolicy()Z */
if ( v0 != null) { // if-eqz v0, :cond_2
/* const-wide/16 v0, 0x64 */
/* cmp-long v0, p1, v0 */
/* if-gtz v0, :cond_0 */
/* .line 421 */
} // :cond_0
int v0 = 2; // const/4 v0, 0x2
/* invoke-direct {p0, v0}, Lcom/miui/server/smartpower/SmartDisplayPolicyManager;->getDurationForInteraction(I)J */
/* move-result-wide v1 */
/* add-long/2addr p1, v1 */
/* .line 423 */
/* sget-boolean v1, Lcom/miui/server/smartpower/SmartDisplayPolicyManager;->DEBUG:Z */
if ( v1 != null) { // if-eqz v1, :cond_1
/* .line 424 */
/* new-instance v1, Ljava/lang/StringBuilder; */
/* invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V */
final String v2 = "notifyAppTransitionStart animDuration: "; // const-string v2, "notifyAppTransitionStart animDuration: "
(( java.lang.StringBuilder ) v1 ).append ( v2 ); // invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v1 ).append ( p1, p2 ); // invoke-virtual {v1, p1, p2}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v1 ).toString ( ); // invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
final String v2 = "SmartPower.DisplayPolicy"; // const-string v2, "SmartPower.DisplayPolicy"
android.util.Slog .d ( v2,v1 );
/* .line 427 */
} // :cond_1
/* invoke-direct {p0, v0, p1, p2}, Lcom/miui/server/smartpower/SmartDisplayPolicyManager;->startControlRefreshRatePolicy(IJ)V */
/* .line 428 */
return;
/* .line 419 */
} // :cond_2
} // :goto_0
return;
} // .end method
public void notifyDisplayDeviceStateChangeLocked ( Integer p0 ) {
/* .locals 2 */
/* .param p1, "deviceState" # I */
/* .line 655 */
/* sget-boolean v0, Lcom/miui/server/smartpower/SmartDisplayPolicyManager;->ENABLE:Z */
if ( v0 != null) { // if-eqz v0, :cond_0
int v0 = -1; // const/4 v0, -0x1
/* if-eq p1, v0, :cond_0 */
/* .line 659 */
v0 = this.mHandler;
int v1 = 1; // const/4 v1, 0x1
(( com.miui.server.smartpower.SmartDisplayPolicyManager$H ) v0 ).removeMessages ( v1 ); // invoke-virtual {v0, v1}, Lcom/miui/server/smartpower/SmartDisplayPolicyManager$H;->removeMessages(I)V
/* .line 660 */
v0 = this.mHandler;
int v1 = 2; // const/4 v1, 0x2
(( com.miui.server.smartpower.SmartDisplayPolicyManager$H ) v0 ).removeMessages ( v1 ); // invoke-virtual {v0, v1}, Lcom/miui/server/smartpower/SmartDisplayPolicyManager$H;->removeMessages(I)V
/* .line 662 */
v0 = this.mHandler;
int v1 = 3; // const/4 v1, 0x3
(( com.miui.server.smartpower.SmartDisplayPolicyManager$H ) v0 ).obtainMessage ( v1 ); // invoke-virtual {v0, v1}, Lcom/miui/server/smartpower/SmartDisplayPolicyManager$H;->obtainMessage(I)Landroid/os/Message;
/* .line 663 */
/* .local v0, "msg":Landroid/os/Message; */
v1 = this.mHandler;
(( com.miui.server.smartpower.SmartDisplayPolicyManager$H ) v1 ).sendMessage ( v0 ); // invoke-virtual {v1, v0}, Lcom/miui/server/smartpower/SmartDisplayPolicyManager$H;->sendMessage(Landroid/os/Message;)Z
/* .line 665 */
} // .end local v0 # "msg":Landroid/os/Message;
} // :cond_0
return;
} // .end method
public void notifyDisplaySwitchResolutionLocked ( Integer p0, Integer p1, Integer p2 ) {
/* .locals 1 */
/* .param p1, "width" # I */
/* .param p2, "height" # I */
/* .param p3, "density" # I */
/* .line 682 */
/* sget-boolean v0, Lcom/miui/server/smartpower/SmartDisplayPolicyManager;->ENABLE:Z */
if ( v0 != null) { // if-eqz v0, :cond_0
/* .line 683 */
/* invoke-direct {p0}, Lcom/miui/server/smartpower/SmartDisplayPolicyManager;->resetDisplaySize()V */
/* .line 685 */
} // :cond_0
return;
} // .end method
public void notifyFocusedWindowChangeLocked ( java.lang.String p0, java.lang.String p1 ) {
/* .locals 4 */
/* .param p1, "oldFocusPackage" # Ljava/lang/String; */
/* .param p2, "newFocusPackage" # Ljava/lang/String; */
/* .line 523 */
v0 = /* invoke-direct {p0}, Lcom/miui/server/smartpower/SmartDisplayPolicyManager;->isSupportSmartDisplayPolicy()Z */
if ( v0 != null) { // if-eqz v0, :cond_5
if ( p1 != null) { // if-eqz p1, :cond_5
if ( p2 != null) { // if-eqz p2, :cond_5
/* .line 526 */
v0 = (( java.lang.String ) p1 ).equals ( p2 ); // invoke-virtual {p1, p2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z
if ( v0 != null) { // if-eqz v0, :cond_0
/* .line 530 */
} // :cond_0
v0 = this.mRefreshRatePolicyController;
v0 = com.miui.server.smartpower.SmartDisplayPolicyManager$RefreshRatePolicyController .-$$Nest$misFocusedPackageWhiteList ( v0,p2 );
final String v1 = "SmartPower.DisplayPolicy"; // const-string v1, "SmartPower.DisplayPolicy"
int v2 = 7; // const/4 v2, 0x7
if ( v0 != null) { // if-eqz v0, :cond_2
/* .line 531 */
/* sget-boolean v0, Lcom/miui/server/smartpower/SmartDisplayPolicyManager;->DEBUG:Z */
if ( v0 != null) { // if-eqz v0, :cond_1
/* .line 532 */
/* new-instance v0, Ljava/lang/StringBuilder; */
/* invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V */
final String v3 = "notifyFocusedWindowChanged, new: "; // const-string v3, "notifyFocusedWindowChanged, new: "
(( java.lang.StringBuilder ) v0 ).append ( v3 ); // invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v0 ).append ( p2 ); // invoke-virtual {v0, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v0 ).toString ( ); // invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
android.util.Slog .d ( v1,v0 );
/* .line 534 */
} // :cond_1
/* invoke-direct {p0, v2}, Lcom/miui/server/smartpower/SmartDisplayPolicyManager;->startControlRefreshRatePolicy(I)V */
/* .line 535 */
} // :cond_2
v0 = this.mRefreshRatePolicyController;
v0 = com.miui.server.smartpower.SmartDisplayPolicyManager$RefreshRatePolicyController .-$$Nest$misFocusedPackageWhiteList ( v0,p1 );
if ( v0 != null) { // if-eqz v0, :cond_4
/* .line 536 */
/* sget-boolean v0, Lcom/miui/server/smartpower/SmartDisplayPolicyManager;->DEBUG:Z */
if ( v0 != null) { // if-eqz v0, :cond_3
/* .line 537 */
/* new-instance v0, Ljava/lang/StringBuilder; */
/* invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V */
final String v3 = "notifyFocusedWindowChanged, old: "; // const-string v3, "notifyFocusedWindowChanged, old: "
(( java.lang.StringBuilder ) v0 ).append ( v3 ); // invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v0 ).append ( p1 ); // invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v0 ).toString ( ); // invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
android.util.Slog .d ( v1,v0 );
/* .line 539 */
} // :cond_3
/* invoke-direct {p0, v2}, Lcom/miui/server/smartpower/SmartDisplayPolicyManager;->endControlRefreshRatePolicy(I)V */
/* .line 541 */
} // :cond_4
} // :goto_0
return;
/* .line 527 */
} // :cond_5
} // :goto_1
return;
} // .end method
public void notifyInsetAnimationHide ( Integer p0 ) {
/* .locals 2 */
/* .param p1, "types" # I */
/* .line 455 */
v0 = /* invoke-direct {p0}, Lcom/miui/server/smartpower/SmartDisplayPolicyManager;->isSupportSmartDisplayPolicy()Z */
/* if-nez v0, :cond_0 */
return;
/* .line 457 */
} // :cond_0
/* sget-boolean v0, Lcom/miui/server/smartpower/SmartDisplayPolicyManager;->DEBUG:Z */
if ( v0 != null) { // if-eqz v0, :cond_1
/* .line 458 */
final String v0 = "SmartPower.DisplayPolicy"; // const-string v0, "SmartPower.DisplayPolicy"
final String v1 = "notifyInsetAnimationHide"; // const-string v1, "notifyInsetAnimationHide"
android.util.Slog .d ( v0,v1 );
/* .line 461 */
} // :cond_1
v0 = android.view.WindowInsets$Type .ime ( );
/* and-int/2addr v0, p1 */
if ( v0 != null) { // if-eqz v0, :cond_2
/* .line 462 */
int v0 = 0; // const/4 v0, 0x0
/* invoke-direct {p0, v0}, Lcom/miui/server/smartpower/SmartDisplayPolicyManager;->notifyInputMethodAnimationStart(I)V */
/* .line 464 */
} // :cond_2
return;
} // .end method
public void notifyInsetAnimationShow ( Integer p0 ) {
/* .locals 2 */
/* .param p1, "types" # I */
/* .line 443 */
v0 = /* invoke-direct {p0}, Lcom/miui/server/smartpower/SmartDisplayPolicyManager;->isSupportSmartDisplayPolicy()Z */
/* if-nez v0, :cond_0 */
return;
/* .line 445 */
} // :cond_0
/* sget-boolean v0, Lcom/miui/server/smartpower/SmartDisplayPolicyManager;->DEBUG:Z */
if ( v0 != null) { // if-eqz v0, :cond_1
/* .line 446 */
final String v0 = "SmartPower.DisplayPolicy"; // const-string v0, "SmartPower.DisplayPolicy"
final String v1 = "notifyInsetAnimationShow"; // const-string v1, "notifyInsetAnimationShow"
android.util.Slog .d ( v0,v1 );
/* .line 449 */
} // :cond_1
v0 = android.view.WindowInsets$Type .ime ( );
/* and-int/2addr v0, p1 */
if ( v0 != null) { // if-eqz v0, :cond_2
/* .line 450 */
int v0 = 1; // const/4 v0, 0x1
/* invoke-direct {p0, v0}, Lcom/miui/server/smartpower/SmartDisplayPolicyManager;->notifyInputMethodAnimationStart(I)V */
/* .line 452 */
} // :cond_2
return;
} // .end method
public void notifyMultiTaskActionEnd ( miui.smartpower.MultiTaskActionManager$ActionInfo p0 ) {
/* .locals 3 */
/* .param p1, "actionInfo" # Lmiui/smartpower/MultiTaskActionManager$ActionInfo; */
/* .line 590 */
v0 = /* invoke-direct {p0}, Lcom/miui/server/smartpower/SmartDisplayPolicyManager;->isSupportSmartDisplayPolicy()Z */
if ( v0 != null) { // if-eqz v0, :cond_3
/* if-nez p1, :cond_0 */
/* .line 592 */
} // :cond_0
v0 = (( miui.smartpower.MultiTaskActionManager$ActionInfo ) p1 ).getType ( ); // invoke-virtual {p1}, Lmiui/smartpower/MultiTaskActionManager$ActionInfo;->getType()I
/* .line 594 */
/* .local v0, "actionType":I */
/* sget-boolean v1, Lcom/miui/server/smartpower/SmartDisplayPolicyManager;->DEBUG:Z */
if ( v1 != null) { // if-eqz v1, :cond_1
/* .line 595 */
/* new-instance v1, Ljava/lang/StringBuilder; */
/* invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V */
final String v2 = "notifyMultiTaskActionEnd: "; // const-string v2, "notifyMultiTaskActionEnd: "
(( java.lang.StringBuilder ) v1 ).append ( v2 ); // invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
/* .line 596 */
miui.smartpower.MultiTaskActionManager .actionTypeToString ( v0 );
(( java.lang.StringBuilder ) v1 ).append ( v2 ); // invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v1 ).toString ( ); // invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
/* .line 595 */
final String v2 = "SmartPower.DisplayPolicy"; // const-string v2, "SmartPower.DisplayPolicy"
android.util.Slog .d ( v2,v1 );
/* .line 599 */
} // :cond_1
v1 = /* invoke-direct {p0, v0}, Lcom/miui/server/smartpower/SmartDisplayPolicyManager;->getInteractTypeForMultiTaskAction(I)I */
/* .line 600 */
/* .local v1, "interactType":I */
/* if-nez v1, :cond_2 */
/* .line 601 */
return;
/* .line 604 */
} // :cond_2
/* invoke-direct {p0, v1}, Lcom/miui/server/smartpower/SmartDisplayPolicyManager;->endControlRefreshRatePolicy(I)V */
/* .line 605 */
return;
/* .line 590 */
} // .end local v0 # "actionType":I
} // .end local v1 # "interactType":I
} // :cond_3
} // :goto_0
return;
} // .end method
public void notifyMultiTaskActionStart ( miui.smartpower.MultiTaskActionManager$ActionInfo p0 ) {
/* .locals 3 */
/* .param p1, "actionInfo" # Lmiui/smartpower/MultiTaskActionManager$ActionInfo; */
/* .line 574 */
v0 = /* invoke-direct {p0}, Lcom/miui/server/smartpower/SmartDisplayPolicyManager;->isSupportSmartDisplayPolicy()Z */
if ( v0 != null) { // if-eqz v0, :cond_3
/* if-nez p1, :cond_0 */
/* .line 576 */
} // :cond_0
v0 = (( miui.smartpower.MultiTaskActionManager$ActionInfo ) p1 ).getType ( ); // invoke-virtual {p1}, Lmiui/smartpower/MultiTaskActionManager$ActionInfo;->getType()I
/* .line 578 */
/* .local v0, "actionType":I */
/* sget-boolean v1, Lcom/miui/server/smartpower/SmartDisplayPolicyManager;->DEBUG:Z */
if ( v1 != null) { // if-eqz v1, :cond_1
/* .line 579 */
/* new-instance v1, Ljava/lang/StringBuilder; */
/* invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V */
final String v2 = "notifyMultiTaskActionStart: "; // const-string v2, "notifyMultiTaskActionStart: "
(( java.lang.StringBuilder ) v1 ).append ( v2 ); // invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
/* .line 580 */
miui.smartpower.MultiTaskActionManager .actionTypeToString ( v0 );
(( java.lang.StringBuilder ) v1 ).append ( v2 ); // invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v1 ).toString ( ); // invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
/* .line 579 */
final String v2 = "SmartPower.DisplayPolicy"; // const-string v2, "SmartPower.DisplayPolicy"
android.util.Slog .d ( v2,v1 );
/* .line 582 */
} // :cond_1
v1 = /* invoke-direct {p0, v0}, Lcom/miui/server/smartpower/SmartDisplayPolicyManager;->getInteractTypeForMultiTaskAction(I)I */
/* .line 583 */
/* .local v1, "interactType":I */
/* if-nez v1, :cond_2 */
/* .line 584 */
return;
/* .line 586 */
} // :cond_2
/* invoke-direct {p0, v1}, Lcom/miui/server/smartpower/SmartDisplayPolicyManager;->startControlRefreshRatePolicy(I)V */
/* .line 587 */
return;
/* .line 574 */
} // .end local v0 # "actionType":I
} // .end local v1 # "interactType":I
} // :cond_3
} // :goto_0
return;
} // .end method
public void notifyRecentsAnimationEnd ( ) {
/* .locals 2 */
/* .line 493 */
v0 = /* invoke-direct {p0}, Lcom/miui/server/smartpower/SmartDisplayPolicyManager;->isSupportSmartDisplayPolicy()Z */
/* if-nez v0, :cond_0 */
return;
/* .line 495 */
} // :cond_0
/* sget-boolean v0, Lcom/miui/server/smartpower/SmartDisplayPolicyManager;->DEBUG:Z */
if ( v0 != null) { // if-eqz v0, :cond_1
/* .line 496 */
final String v0 = "SmartPower.DisplayPolicy"; // const-string v0, "SmartPower.DisplayPolicy"
final String v1 = "notifyRecentsAnimationEnd"; // const-string v1, "notifyRecentsAnimationEnd"
android.util.Slog .d ( v0,v1 );
/* .line 499 */
} // :cond_1
int v0 = 5; // const/4 v0, 0x5
/* invoke-direct {p0, v0}, Lcom/miui/server/smartpower/SmartDisplayPolicyManager;->endControlRefreshRatePolicy(I)V */
/* .line 500 */
return;
} // .end method
public void notifyRecentsAnimationStart ( com.android.server.wm.ActivityRecord p0 ) {
/* .locals 2 */
/* .param p1, "targetActivity" # Lcom/android/server/wm/ActivityRecord; */
/* .line 483 */
v0 = /* invoke-direct {p0}, Lcom/miui/server/smartpower/SmartDisplayPolicyManager;->isSupportSmartDisplayPolicy()Z */
/* if-nez v0, :cond_0 */
return;
/* .line 485 */
} // :cond_0
/* sget-boolean v0, Lcom/miui/server/smartpower/SmartDisplayPolicyManager;->DEBUG:Z */
if ( v0 != null) { // if-eqz v0, :cond_1
/* .line 486 */
final String v0 = "SmartPower.DisplayPolicy"; // const-string v0, "SmartPower.DisplayPolicy"
final String v1 = "notifyRecentsAnimationStart"; // const-string v1, "notifyRecentsAnimationStart"
android.util.Slog .d ( v0,v1 );
/* .line 489 */
} // :cond_1
int v0 = 5; // const/4 v0, 0x5
/* invoke-direct {p0, v0}, Lcom/miui/server/smartpower/SmartDisplayPolicyManager;->startControlRefreshRatePolicy(I)V */
/* .line 490 */
return;
} // .end method
public void notifyRemoteAnimationEnd ( ) {
/* .locals 2 */
/* .line 513 */
v0 = /* invoke-direct {p0}, Lcom/miui/server/smartpower/SmartDisplayPolicyManager;->isSupportSmartDisplayPolicy()Z */
/* if-nez v0, :cond_0 */
return;
/* .line 515 */
} // :cond_0
/* sget-boolean v0, Lcom/miui/server/smartpower/SmartDisplayPolicyManager;->DEBUG:Z */
if ( v0 != null) { // if-eqz v0, :cond_1
/* .line 516 */
final String v0 = "SmartPower.DisplayPolicy"; // const-string v0, "SmartPower.DisplayPolicy"
final String v1 = "notifyRemoteAnimationEnd"; // const-string v1, "notifyRemoteAnimationEnd"
android.util.Slog .d ( v0,v1 );
/* .line 519 */
} // :cond_1
int v0 = 6; // const/4 v0, 0x6
/* invoke-direct {p0, v0}, Lcom/miui/server/smartpower/SmartDisplayPolicyManager;->endControlRefreshRatePolicy(I)V */
/* .line 520 */
return;
} // .end method
public void notifyRemoteAnimationStart ( android.view.RemoteAnimationTarget[] p0 ) {
/* .locals 2 */
/* .param p1, "appTargets" # [Landroid/view/RemoteAnimationTarget; */
/* .line 503 */
v0 = /* invoke-direct {p0}, Lcom/miui/server/smartpower/SmartDisplayPolicyManager;->isSupportSmartDisplayPolicy()Z */
/* if-nez v0, :cond_0 */
return;
/* .line 505 */
} // :cond_0
/* sget-boolean v0, Lcom/miui/server/smartpower/SmartDisplayPolicyManager;->DEBUG:Z */
if ( v0 != null) { // if-eqz v0, :cond_1
/* .line 506 */
final String v0 = "SmartPower.DisplayPolicy"; // const-string v0, "SmartPower.DisplayPolicy"
final String v1 = "notifyRemoteAnimationStart"; // const-string v1, "notifyRemoteAnimationStart"
android.util.Slog .d ( v0,v1 );
/* .line 509 */
} // :cond_1
int v0 = 6; // const/4 v0, 0x6
/* invoke-direct {p0, v0}, Lcom/miui/server/smartpower/SmartDisplayPolicyManager;->startControlRefreshRatePolicy(I)V */
/* .line 510 */
return;
} // .end method
public void notifyTransitionEnd ( Integer p0, Integer p1, Integer p2 ) {
/* .locals 2 */
/* .param p1, "syncId" # I */
/* .param p2, "type" # I */
/* .param p3, "flags" # I */
/* .line 636 */
v0 = /* invoke-direct {p0}, Lcom/miui/server/smartpower/SmartDisplayPolicyManager;->isSupportSmartDisplayPolicy()Z */
/* if-nez v0, :cond_0 */
return;
/* .line 639 */
} // :cond_0
/* const/16 v0, 0xc */
/* if-ne p2, v0, :cond_1 */
/* .line 640 */
return;
/* .line 643 */
} // :cond_1
/* sget-boolean v0, Lcom/miui/server/smartpower/SmartDisplayPolicyManager;->DEBUG:Z */
if ( v0 != null) { // if-eqz v0, :cond_2
/* .line 644 */
/* new-instance v0, Ljava/lang/StringBuilder; */
/* invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V */
final String v1 = "notifyTransitionFinish: "; // const-string v1, "notifyTransitionFinish: "
(( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
android.view.WindowManager .transitTypeToString ( p2 );
(( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v0 ).toString ( ); // invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
final String v1 = "SmartPower.DisplayPolicy"; // const-string v1, "SmartPower.DisplayPolicy"
android.util.Slog .d ( v1,v0 );
/* .line 646 */
} // :cond_2
/* const/16 v0, 0x10 */
/* invoke-direct {p0, v0}, Lcom/miui/server/smartpower/SmartDisplayPolicyManager;->endControlRefreshRatePolicy(I)V */
/* .line 647 */
return;
} // .end method
public void notifyTransitionStart ( android.window.TransitionInfo p0 ) {
/* .locals 2 */
/* .param p1, "info" # Landroid/window/TransitionInfo; */
/* .line 621 */
v0 = /* invoke-direct {p0}, Lcom/miui/server/smartpower/SmartDisplayPolicyManager;->isSupportSmartDisplayPolicy()Z */
/* if-nez v0, :cond_0 */
return;
/* .line 624 */
} // :cond_0
v0 = (( android.window.TransitionInfo ) p1 ).getType ( ); // invoke-virtual {p1}, Landroid/window/TransitionInfo;->getType()I
/* const/16 v1, 0xc */
/* if-ne v0, v1, :cond_1 */
/* .line 625 */
return;
/* .line 628 */
} // :cond_1
/* sget-boolean v0, Lcom/miui/server/smartpower/SmartDisplayPolicyManager;->DEBUG:Z */
if ( v0 != null) { // if-eqz v0, :cond_2
/* .line 629 */
/* new-instance v0, Ljava/lang/StringBuilder; */
/* invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V */
final String v1 = "notifyTransitionReady: "; // const-string v1, "notifyTransitionReady: "
(( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( android.window.TransitionInfo ) p1 ).toString ( ); // invoke-virtual {p1}, Landroid/window/TransitionInfo;->toString()Ljava/lang/String;
(( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v0 ).toString ( ); // invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
final String v1 = "SmartPower.DisplayPolicy"; // const-string v1, "SmartPower.DisplayPolicy"
android.util.Slog .d ( v1,v0 );
/* .line 631 */
} // :cond_2
/* const/16 v0, 0x10 */
/* invoke-direct {p0, v0}, Lcom/miui/server/smartpower/SmartDisplayPolicyManager;->startControlRefreshRatePolicy(I)V */
/* .line 632 */
return;
} // .end method
public void notifyWindowAnimationStartLocked ( Long p0, android.view.SurfaceControl p1 ) {
/* .locals 3 */
/* .param p1, "animDuration" # J */
/* .param p3, "animationLeash" # Landroid/view/SurfaceControl; */
/* .line 431 */
v0 = /* invoke-direct {p0}, Lcom/miui/server/smartpower/SmartDisplayPolicyManager;->isSupportSmartDisplayPolicy()Z */
if ( v0 != null) { // if-eqz v0, :cond_2
/* const-wide/16 v0, 0xc8 */
/* cmp-long v0, p1, v0 */
/* if-gtz v0, :cond_0 */
/* .line 433 */
} // :cond_0
int v0 = 3; // const/4 v0, 0x3
/* invoke-direct {p0, v0}, Lcom/miui/server/smartpower/SmartDisplayPolicyManager;->getDurationForInteraction(I)J */
/* move-result-wide v1 */
/* add-long/2addr p1, v1 */
/* .line 435 */
/* sget-boolean v1, Lcom/miui/server/smartpower/SmartDisplayPolicyManager;->DEBUG:Z */
if ( v1 != null) { // if-eqz v1, :cond_1
/* .line 436 */
/* new-instance v1, Ljava/lang/StringBuilder; */
/* invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V */
final String v2 = "notifyWindowAnimationStart animDuration: "; // const-string v2, "notifyWindowAnimationStart animDuration: "
(( java.lang.StringBuilder ) v1 ).append ( v2 ); // invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v1 ).append ( p1, p2 ); // invoke-virtual {v1, p1, p2}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v1 ).toString ( ); // invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
final String v2 = "SmartPower.DisplayPolicy"; // const-string v2, "SmartPower.DisplayPolicy"
android.util.Slog .d ( v2,v1 );
/* .line 439 */
} // :cond_1
/* invoke-direct {p0, v0, p1, p2}, Lcom/miui/server/smartpower/SmartDisplayPolicyManager;->startControlRefreshRatePolicy(IJ)V */
/* .line 440 */
return;
/* .line 431 */
} // :cond_2
} // :goto_0
return;
} // .end method
public void notifyWindowVisibilityChangedLocked ( com.android.server.policy.WindowManagerPolicy$WindowState p0, android.view.WindowManager$LayoutParams p1, Boolean p2 ) {
/* .locals 2 */
/* .param p1, "win" # Lcom/android/server/policy/WindowManagerPolicy$WindowState; */
/* .param p2, "attrs" # Landroid/view/WindowManager$LayoutParams; */
/* .param p3, "visible" # Z */
/* .line 563 */
v0 = /* invoke-direct {p0}, Lcom/miui/server/smartpower/SmartDisplayPolicyManager;->isSupportSmartDisplayPolicy()Z */
/* if-nez v0, :cond_0 */
return;
/* .line 565 */
} // :cond_0
if ( p3 != null) { // if-eqz p3, :cond_2
v0 = this.mRefreshRatePolicyController;
v0 = com.miui.server.smartpower.SmartDisplayPolicyManager$RefreshRatePolicyController .-$$Nest$misWindowVisibleWhiteList ( v0,p1,p2 );
if ( v0 != null) { // if-eqz v0, :cond_2
/* .line 566 */
/* sget-boolean v0, Lcom/miui/server/smartpower/SmartDisplayPolicyManager;->DEBUG:Z */
if ( v0 != null) { // if-eqz v0, :cond_1
/* .line 567 */
final String v0 = "SmartPower.DisplayPolicy"; // const-string v0, "SmartPower.DisplayPolicy"
final String v1 = "notifyWindowVisibilityChangedLocked"; // const-string v1, "notifyWindowVisibilityChangedLocked"
android.util.Slog .d ( v0,v1 );
/* .line 569 */
} // :cond_1
/* const/16 v0, 0xb */
/* invoke-direct {p0, v0}, Lcom/miui/server/smartpower/SmartDisplayPolicyManager;->startControlRefreshRatePolicy(I)V */
/* .line 571 */
} // :cond_2
return;
} // .end method
public Boolean shouldInterceptUpdateDisplayModeSpecs ( Integer p0, com.android.server.display.mode.DisplayModeDirector$DesiredDisplayModeSpecs p1 ) {
/* .locals 2 */
/* .param p1, "displayId" # I */
/* .param p2, "modeSpecs" # Lcom/android/server/display/mode/DisplayModeDirector$DesiredDisplayModeSpecs; */
/* .line 699 */
v0 = /* invoke-direct {p0}, Lcom/miui/server/smartpower/SmartDisplayPolicyManager;->isSupportSmartDisplayPolicy()Z */
int v1 = 0; // const/4 v1, 0x0
/* if-nez v0, :cond_0 */
/* .line 701 */
} // :cond_0
if ( p1 != null) { // if-eqz p1, :cond_1
/* .line 703 */
} // :cond_1
v0 = this.mRefreshRatePolicyController;
v0 = com.miui.server.smartpower.SmartDisplayPolicyManager$RefreshRatePolicyController .-$$Nest$mshouldInterceptUpdateDisplayModeSpecs ( v0,p2 );
} // .end method
public void systemReady ( ) {
/* .locals 6 */
/* .line 276 */
/* new-instance v0, Lcom/android/server/wm/SchedBoostGesturesEvent; */
v1 = this.mHandlerTh;
(( android.os.HandlerThread ) v1 ).getLooper ( ); // invoke-virtual {v1}, Landroid/os/HandlerThread;->getLooper()Landroid/os/Looper;
/* invoke-direct {v0, v1}, Lcom/android/server/wm/SchedBoostGesturesEvent;-><init>(Landroid/os/Looper;)V */
this.mSchedBoostGesturesEvent = v0;
/* .line 277 */
v1 = this.mContext;
(( com.android.server.wm.SchedBoostGesturesEvent ) v0 ).init ( v1 ); // invoke-virtual {v0, v1}, Lcom/android/server/wm/SchedBoostGesturesEvent;->init(Landroid/content/Context;)V
/* .line 278 */
v0 = this.mSchedBoostGesturesEvent;
/* new-instance v1, Lcom/miui/server/smartpower/SmartDisplayPolicyManager$GesturesEventListenerCallback; */
int v2 = 0; // const/4 v2, 0x0
/* invoke-direct {v1, p0, v2}, Lcom/miui/server/smartpower/SmartDisplayPolicyManager$GesturesEventListenerCallback;-><init>(Lcom/miui/server/smartpower/SmartDisplayPolicyManager;Lcom/miui/server/smartpower/SmartDisplayPolicyManager$GesturesEventListenerCallback-IA;)V */
(( com.android.server.wm.SchedBoostGesturesEvent ) v0 ).setGesturesEventListener ( v1 ); // invoke-virtual {v0, v1}, Lcom/android/server/wm/SchedBoostGesturesEvent;->setGesturesEventListener(Lcom/android/server/wm/SchedBoostGesturesEvent$GesturesEventListener;)V
/* .line 281 */
v0 = this.mSmartScenarioManager;
/* new-instance v1, Lcom/miui/server/smartpower/SmartDisplayPolicyManager$SmartScenarioCallback; */
/* invoke-direct {v1, p0, v2}, Lcom/miui/server/smartpower/SmartDisplayPolicyManager$SmartScenarioCallback;-><init>(Lcom/miui/server/smartpower/SmartDisplayPolicyManager;Lcom/miui/server/smartpower/SmartDisplayPolicyManager$SmartScenarioCallback-IA;)V */
/* .line 282 */
final String v3 = "SmartPower.DisplayPolicy"; // const-string v3, "SmartPower.DisplayPolicy"
(( com.miui.server.smartpower.SmartScenarioManager ) v0 ).createClientConfig ( v3, v1 ); // invoke-virtual {v0, v3, v1}, Lcom/miui/server/smartpower/SmartScenarioManager;->createClientConfig(Ljava/lang/String;Lcom/miui/server/smartpower/SmartScenarioManager$ISmartScenarioCallback;)Lcom/miui/server/smartpower/SmartScenarioManager$ClientConfig;
/* .line 283 */
/* .local v0, "config":Lcom/miui/server/smartpower/SmartScenarioManager$ClientConfig; */
final String v1 = "com.android.camera"; // const-string v1, "com.android.camera"
int v3 = 2; // const/4 v3, 0x2
/* const-wide/16 v4, 0x2 */
(( com.miui.server.smartpower.SmartScenarioManager$ClientConfig ) v0 ).addMainScenarioIdConfig ( v4, v5, v1, v3 ); // invoke-virtual {v0, v4, v5, v1, v3}, Lcom/miui/server/smartpower/SmartScenarioManager$ClientConfig;->addMainScenarioIdConfig(JLjava/lang/String;I)V
/* .line 284 */
/* const-wide/16 v3, 0x4000 */
/* const/16 v1, 0x4000 */
(( com.miui.server.smartpower.SmartScenarioManager$ClientConfig ) v0 ).addAdditionalScenarioIdConfig ( v3, v4, v2, v1 ); // invoke-virtual {v0, v3, v4, v2, v1}, Lcom/miui/server/smartpower/SmartScenarioManager$ClientConfig;->addAdditionalScenarioIdConfig(JLjava/lang/String;I)V
/* .line 286 */
v1 = this.mSmartScenarioManager;
(( com.miui.server.smartpower.SmartScenarioManager ) v1 ).registClientConfig ( v0 ); // invoke-virtual {v1, v0}, Lcom/miui/server/smartpower/SmartScenarioManager;->registClientConfig(Lcom/miui/server/smartpower/SmartScenarioManager$ClientConfig;)V
/* .line 289 */
com.android.server.am.SystemPressureControllerStub .getInstance ( );
/* new-instance v3, Lcom/miui/server/smartpower/SmartDisplayPolicyManager$ThermalTempListenerCallback; */
/* invoke-direct {v3, p0, v2}, Lcom/miui/server/smartpower/SmartDisplayPolicyManager$ThermalTempListenerCallback;-><init>(Lcom/miui/server/smartpower/SmartDisplayPolicyManager;Lcom/miui/server/smartpower/SmartDisplayPolicyManager$ThermalTempListenerCallback-IA;)V */
(( com.android.server.am.SystemPressureControllerStub ) v1 ).registerThermalTempListener ( v3 ); // invoke-virtual {v1, v3}, Lcom/android/server/am/SystemPressureControllerStub;->registerThermalTempListener(Lcom/android/server/am/ThermalTempListener;)V
/* .line 291 */
return;
} // .end method
