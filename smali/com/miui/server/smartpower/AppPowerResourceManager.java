public class com.miui.server.smartpower.AppPowerResourceManager {
	 /* .source "AppPowerResourceManager.java" */
	 /* # static fields */
	 public static final Boolean DEBUG;
	 public static final Integer RESOURCE_BEHAVIOR_CAMERA;
	 public static final Integer RESOURCE_BEHAVIOR_EXTERNAL_CASTING;
	 public static final Integer RESOURCE_BEHAVIOR_GPS;
	 public static final Integer RESOURCE_BEHAVIOR_HOLD_SCREEN;
	 public static final Integer RESOURCE_BEHAVIOR_INCALL;
	 public static final Integer RESOURCE_BEHAVIOR_NETWORK;
	 public static final Integer RESOURCE_BEHAVIOR_PLAYER;
	 public static final Integer RESOURCE_BEHAVIOR_PLAYER_PLAYBACK;
	 public static final Integer RESOURCE_BEHAVIOR_RECORDER;
	 public static final Integer RESOURCE_BEHAVIOR_WIFI_CASTING;
	 public static final Integer RES_TYPE_AUDIO;
	 public static final Integer RES_TYPE_BLE;
	 public static final Integer RES_TYPE_CAMERA;
	 public static final Integer RES_TYPE_DISPLAY;
	 public static final Integer RES_TYPE_GPS;
	 public static final Integer RES_TYPE_NET;
	 public static final Integer RES_TYPE_WAKELOCK;
	 public static final java.lang.String TAG;
	 /* # instance fields */
	 private com.miui.server.smartpower.AppAudioResource mAudioResource;
	 private com.miui.server.smartpower.AppBluetoothResource mBluetoothResource;
	 private com.miui.server.smartpower.AppCameraResource mCameraResource;
	 private java.util.HashMap mDeviceResouceMap;
	 /* .annotation system Ldalvik/annotation/Signature; */
	 /* value = { */
	 /* "Ljava/util/HashMap<", */
	 /* "Ljava/lang/Integer;", */
	 /* "Lcom/miui/server/smartpower/AppPowerResource;", */
	 /* ">;" */
	 /* } */
} // .end annotation
} // .end field
private com.miui.server.smartpower.AppDisplayResource mDisplayResource;
private com.miui.server.smartpower.AppGPSResource mGPSResource;
private com.miui.server.smartpower.AppNetworkResource mNetWorkResource;
private com.miui.server.smartpower.AppWakelockResource mWakelockResource;
/* # direct methods */
static com.miui.server.smartpower.AppPowerResourceManager ( ) {
/* .locals 1 */
/* .line 16 */
/* sget-boolean v0, Lcom/android/server/am/SmartPowerService;->DEBUG:Z */
com.miui.server.smartpower.AppPowerResourceManager.DEBUG = (v0!= 0);
return;
} // .end method
public com.miui.server.smartpower.AppPowerResourceManager ( ) {
/* .locals 2 */
/* .param p1, "context" # Landroid/content/Context; */
/* .param p2, "looper" # Landroid/os/Looper; */
/* .line 51 */
/* invoke-direct {p0}, Ljava/lang/Object;-><init>()V */
/* .line 41 */
/* new-instance v0, Ljava/util/HashMap; */
/* invoke-direct {v0}, Ljava/util/HashMap;-><init>()V */
this.mDeviceResouceMap = v0;
/* .line 52 */
/* new-instance v0, Lcom/miui/server/smartpower/AppAudioResource; */
/* invoke-direct {v0, p1, p2}, Lcom/miui/server/smartpower/AppAudioResource;-><init>(Landroid/content/Context;Landroid/os/Looper;)V */
this.mAudioResource = v0;
/* .line 53 */
/* new-instance v0, Lcom/miui/server/smartpower/AppNetworkResource; */
/* invoke-direct {v0, p1, p2}, Lcom/miui/server/smartpower/AppNetworkResource;-><init>(Landroid/content/Context;Landroid/os/Looper;)V */
this.mNetWorkResource = v0;
/* .line 54 */
/* new-instance v0, Lcom/miui/server/smartpower/AppGPSResource; */
/* invoke-direct {v0, p1, p2}, Lcom/miui/server/smartpower/AppGPSResource;-><init>(Landroid/content/Context;Landroid/os/Looper;)V */
this.mGPSResource = v0;
/* .line 55 */
/* new-instance v0, Lcom/miui/server/smartpower/AppWakelockResource; */
/* invoke-direct {v0, p1, p2}, Lcom/miui/server/smartpower/AppWakelockResource;-><init>(Landroid/content/Context;Landroid/os/Looper;)V */
this.mWakelockResource = v0;
/* .line 56 */
/* new-instance v0, Lcom/miui/server/smartpower/AppBluetoothResource; */
/* invoke-direct {v0, p1, p2}, Lcom/miui/server/smartpower/AppBluetoothResource;-><init>(Landroid/content/Context;Landroid/os/Looper;)V */
this.mBluetoothResource = v0;
/* .line 57 */
/* new-instance v0, Lcom/miui/server/smartpower/AppCameraResource; */
/* invoke-direct {v0, p1, p2}, Lcom/miui/server/smartpower/AppCameraResource;-><init>(Landroid/content/Context;Landroid/os/Looper;)V */
this.mCameraResource = v0;
/* .line 58 */
/* new-instance v0, Lcom/miui/server/smartpower/AppDisplayResource; */
/* invoke-direct {v0, p1, p2}, Lcom/miui/server/smartpower/AppDisplayResource;-><init>(Landroid/content/Context;Landroid/os/Looper;)V */
this.mDisplayResource = v0;
/* .line 60 */
v0 = this.mAudioResource;
/* iget v0, v0, Lcom/miui/server/smartpower/AppAudioResource;->mType:I */
v1 = this.mAudioResource;
(( com.miui.server.smartpower.AppPowerResourceManager ) p0 ).addDeviceResource ( v0, v1 ); // invoke-virtual {p0, v0, v1}, Lcom/miui/server/smartpower/AppPowerResourceManager;->addDeviceResource(ILcom/miui/server/smartpower/AppPowerResource;)V
/* .line 61 */
v0 = this.mNetWorkResource;
/* iget v0, v0, Lcom/miui/server/smartpower/AppNetworkResource;->mType:I */
v1 = this.mNetWorkResource;
(( com.miui.server.smartpower.AppPowerResourceManager ) p0 ).addDeviceResource ( v0, v1 ); // invoke-virtual {p0, v0, v1}, Lcom/miui/server/smartpower/AppPowerResourceManager;->addDeviceResource(ILcom/miui/server/smartpower/AppPowerResource;)V
/* .line 62 */
v0 = this.mGPSResource;
/* iget v0, v0, Lcom/miui/server/smartpower/AppGPSResource;->mType:I */
v1 = this.mGPSResource;
(( com.miui.server.smartpower.AppPowerResourceManager ) p0 ).addDeviceResource ( v0, v1 ); // invoke-virtual {p0, v0, v1}, Lcom/miui/server/smartpower/AppPowerResourceManager;->addDeviceResource(ILcom/miui/server/smartpower/AppPowerResource;)V
/* .line 63 */
v0 = this.mWakelockResource;
/* iget v0, v0, Lcom/miui/server/smartpower/AppWakelockResource;->mType:I */
v1 = this.mWakelockResource;
(( com.miui.server.smartpower.AppPowerResourceManager ) p0 ).addDeviceResource ( v0, v1 ); // invoke-virtual {p0, v0, v1}, Lcom/miui/server/smartpower/AppPowerResourceManager;->addDeviceResource(ILcom/miui/server/smartpower/AppPowerResource;)V
/* .line 64 */
v0 = this.mBluetoothResource;
/* iget v0, v0, Lcom/miui/server/smartpower/AppBluetoothResource;->mType:I */
v1 = this.mBluetoothResource;
(( com.miui.server.smartpower.AppPowerResourceManager ) p0 ).addDeviceResource ( v0, v1 ); // invoke-virtual {p0, v0, v1}, Lcom/miui/server/smartpower/AppPowerResourceManager;->addDeviceResource(ILcom/miui/server/smartpower/AppPowerResource;)V
/* .line 65 */
v0 = this.mCameraResource;
/* iget v0, v0, Lcom/miui/server/smartpower/AppCameraResource;->mType:I */
v1 = this.mCameraResource;
(( com.miui.server.smartpower.AppPowerResourceManager ) p0 ).addDeviceResource ( v0, v1 ); // invoke-virtual {p0, v0, v1}, Lcom/miui/server/smartpower/AppPowerResourceManager;->addDeviceResource(ILcom/miui/server/smartpower/AppPowerResource;)V
/* .line 66 */
v0 = this.mDisplayResource;
/* iget v0, v0, Lcom/miui/server/smartpower/AppDisplayResource;->mType:I */
v1 = this.mDisplayResource;
(( com.miui.server.smartpower.AppPowerResourceManager ) p0 ).addDeviceResource ( v0, v1 ); // invoke-virtual {p0, v0, v1}, Lcom/miui/server/smartpower/AppPowerResourceManager;->addDeviceResource(ILcom/miui/server/smartpower/AppPowerResource;)V
/* .line 67 */
return;
} // .end method
public static java.lang.String typeToString ( Integer p0 ) {
/* .locals 1 */
/* .param p0, "type" # I */
/* .line 263 */
/* packed-switch p0, :pswitch_data_0 */
/* .line 279 */
java.lang.Integer .toString ( p0 );
/* .line 277 */
/* :pswitch_0 */
final String v0 = "display"; // const-string v0, "display"
/* .line 275 */
/* :pswitch_1 */
final String v0 = "camera"; // const-string v0, "camera"
/* .line 273 */
/* :pswitch_2 */
final String v0 = "bluetooth"; // const-string v0, "bluetooth"
/* .line 271 */
/* :pswitch_3 */
/* const-string/jumbo v0, "wakelock" */
/* .line 269 */
/* :pswitch_4 */
final String v0 = "gps"; // const-string v0, "gps"
/* .line 267 */
/* :pswitch_5 */
final String v0 = "net"; // const-string v0, "net"
/* .line 265 */
/* :pswitch_6 */
final String v0 = "audio"; // const-string v0, "audio"
/* :pswitch_data_0 */
/* .packed-switch 0x1 */
/* :pswitch_6 */
/* :pswitch_5 */
/* :pswitch_4 */
/* :pswitch_3 */
/* :pswitch_2 */
/* :pswitch_1 */
/* :pswitch_0 */
} // .end packed-switch
} // .end method
/* # virtual methods */
protected void addDeviceResource ( Integer p0, com.miui.server.smartpower.AppPowerResource p1 ) {
/* .locals 2 */
/* .param p1, "type" # I */
/* .param p2, "resource" # Lcom/miui/server/smartpower/AppPowerResource; */
/* .line 78 */
v0 = this.mDeviceResouceMap;
java.lang.Integer .valueOf ( p1 );
(( java.util.HashMap ) v0 ).put ( v1, p2 ); // invoke-virtual {v0, v1, p2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;
/* .line 79 */
return;
} // .end method
public java.util.ArrayList getActiveUids ( Integer p0 ) {
/* .locals 2 */
/* .param p1, "resourceType" # I */
/* .line 222 */
v0 = this.mDeviceResouceMap;
java.lang.Integer .valueOf ( p1 );
(( java.util.HashMap ) v0 ).get ( v1 ); // invoke-virtual {v0, v1}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;
/* check-cast v0, Lcom/miui/server/smartpower/AppPowerResource; */
/* .line 223 */
/* .local v0, "res":Lcom/miui/server/smartpower/AppPowerResource; */
if ( v0 != null) { // if-eqz v0, :cond_0
/* .line 224 */
(( com.miui.server.smartpower.AppPowerResource ) v0 ).getActiveUids ( ); // invoke-virtual {v0}, Lcom/miui/server/smartpower/AppPowerResource;->getActiveUids()Ljava/util/ArrayList;
/* .line 226 */
} // :cond_0
int v1 = 0; // const/4 v1, 0x0
} // .end method
public Long getLastMusicPlayTimeStamp ( Integer p0 ) {
/* .locals 2 */
/* .param p1, "pid" # I */
/* .line 231 */
v0 = this.mAudioResource;
(( com.miui.server.smartpower.AppAudioResource ) v0 ).getLastMusicPlayTimeStamp ( p1 ); // invoke-virtual {v0, p1}, Lcom/miui/server/smartpower/AppAudioResource;->getLastMusicPlayTimeStamp(I)J
/* move-result-wide v0 */
/* return-wide v0 */
} // .end method
public void init ( ) {
/* .locals 2 */
/* .line 70 */
v0 = this.mDeviceResouceMap;
(( java.util.HashMap ) v0 ).values ( ); // invoke-virtual {v0}, Ljava/util/HashMap;->values()Ljava/util/Collection;
v1 = } // :goto_0
if ( v1 != null) { // if-eqz v1, :cond_1
/* check-cast v1, Lcom/miui/server/smartpower/AppPowerResource; */
/* .line 71 */
/* .local v1, "res":Lcom/miui/server/smartpower/AppPowerResource; */
if ( v1 != null) { // if-eqz v1, :cond_0
/* .line 72 */
(( com.miui.server.smartpower.AppPowerResource ) v1 ).init ( ); // invoke-virtual {v1}, Lcom/miui/server/smartpower/AppPowerResource;->init()V
/* .line 74 */
} // .end local v1 # "res":Lcom/miui/server/smartpower/AppPowerResource;
} // :cond_0
/* .line 75 */
} // :cond_1
return;
} // .end method
public Boolean isAppResActive ( Integer p0, Integer p1 ) {
/* .locals 2 */
/* .param p1, "uid" # I */
/* .param p2, "resourceType" # I */
/* .line 204 */
v0 = this.mDeviceResouceMap;
java.lang.Integer .valueOf ( p2 );
(( java.util.HashMap ) v0 ).get ( v1 ); // invoke-virtual {v0, v1}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;
/* check-cast v0, Lcom/miui/server/smartpower/AppPowerResource; */
/* .line 205 */
/* .local v0, "res":Lcom/miui/server/smartpower/AppPowerResource; */
if ( v0 != null) { // if-eqz v0, :cond_0
/* .line 206 */
v1 = (( com.miui.server.smartpower.AppPowerResource ) v0 ).isAppResourceActive ( p1 ); // invoke-virtual {v0, p1}, Lcom/miui/server/smartpower/AppPowerResource;->isAppResourceActive(I)Z
/* .line 208 */
} // :cond_0
int v1 = 0; // const/4 v1, 0x0
} // .end method
public Boolean isAppResActive ( Integer p0, Integer p1, Integer p2 ) {
/* .locals 2 */
/* .param p1, "uid" # I */
/* .param p2, "pid" # I */
/* .param p3, "resourceType" # I */
/* .line 213 */
v0 = this.mDeviceResouceMap;
java.lang.Integer .valueOf ( p3 );
(( java.util.HashMap ) v0 ).get ( v1 ); // invoke-virtual {v0, v1}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;
/* check-cast v0, Lcom/miui/server/smartpower/AppPowerResource; */
/* .line 214 */
/* .local v0, "res":Lcom/miui/server/smartpower/AppPowerResource; */
if ( v0 != null) { // if-eqz v0, :cond_0
/* .line 215 */
v1 = (( com.miui.server.smartpower.AppPowerResource ) v0 ).isAppResourceActive ( p1, p2 ); // invoke-virtual {v0, p1, p2}, Lcom/miui/server/smartpower/AppPowerResource;->isAppResourceActive(II)Z
/* .line 217 */
} // :cond_0
int v1 = 0; // const/4 v1, 0x0
} // .end method
public void notifyCameraForegroundState ( java.lang.String p0, Boolean p1, java.lang.String p2, Integer p3, Integer p4 ) {
/* .locals 6 */
/* .param p1, "cameraId" # Ljava/lang/String; */
/* .param p2, "isForeground" # Z */
/* .param p3, "caller" # Ljava/lang/String; */
/* .param p4, "callerUid" # I */
/* .param p5, "callerPid" # I */
/* .line 153 */
v0 = this.mCameraResource;
/* move-object v1, p1 */
/* move v2, p2 */
/* move-object v3, p3 */
/* move v4, p4 */
/* move v5, p5 */
/* invoke-virtual/range {v0 ..v5}, Lcom/miui/server/smartpower/AppCameraResource;->notifyCameraForegroundState(Ljava/lang/String;ZLjava/lang/String;II)V */
/* .line 155 */
return;
} // .end method
public void onAcquireWakelock ( android.os.IBinder p0, Integer p1, java.lang.String p2, Integer p3, Integer p4 ) {
/* .locals 6 */
/* .param p1, "lock" # Landroid/os/IBinder; */
/* .param p2, "flags" # I */
/* .param p3, "tag" # Ljava/lang/String; */
/* .param p4, "ownerUid" # I */
/* .param p5, "ownerPid" # I */
/* .line 135 */
v0 = this.mWakelockResource;
/* move-object v1, p1 */
/* move v2, p2 */
/* move-object v3, p3 */
/* move v4, p4 */
/* move v5, p5 */
/* invoke-virtual/range {v0 ..v5}, Lcom/miui/server/smartpower/AppWakelockResource;->acquireWakelock(Landroid/os/IBinder;ILjava/lang/String;II)V */
/* .line 136 */
return;
} // .end method
public void onAquireLocation ( Integer p0, Integer p1, android.location.ILocationListener p2 ) {
/* .locals 1 */
/* .param p1, "uid" # I */
/* .param p2, "pid" # I */
/* .param p3, "listener" # Landroid/location/ILocationListener; */
/* .line 126 */
v0 = this.mGPSResource;
(( com.miui.server.smartpower.AppGPSResource ) v0 ).onAquireLocation ( p1, p2, p3 ); // invoke-virtual {v0, p1, p2, p3}, Lcom/miui/server/smartpower/AppGPSResource;->onAquireLocation(IILandroid/location/ILocationListener;)V
/* .line 127 */
return;
} // .end method
public void onBluetoothEvent ( Boolean p0, Integer p1, Integer p2, Integer p3, Integer p4 ) {
/* .locals 6 */
/* .param p1, "isConnect" # Z */
/* .param p2, "bleType" # I */
/* .param p3, "uid" # I */
/* .param p4, "pid" # I */
/* .param p5, "data" # I */
/* .line 144 */
v0 = this.mBluetoothResource;
/* move v1, p1 */
/* move v2, p2 */
/* move v3, p3 */
/* move v4, p4 */
/* move v5, p5 */
/* invoke-virtual/range {v0 ..v5}, Lcom/miui/server/smartpower/AppBluetoothResource;->onBluetoothEvent(ZIIII)V */
/* .line 145 */
return;
} // .end method
public void onPlayerEvent ( Integer p0, Integer p1, Integer p2, Integer p3 ) {
/* .locals 1 */
/* .param p1, "uid" # I */
/* .param p2, "pid" # I */
/* .param p3, "piid" # I */
/* .param p4, "event" # I */
/* .line 102 */
v0 = this.mAudioResource;
(( com.miui.server.smartpower.AppAudioResource ) v0 ).onPlayerEvent ( p1, p2, p3, p4 ); // invoke-virtual {v0, p1, p2, p3, p4}, Lcom/miui/server/smartpower/AppAudioResource;->onPlayerEvent(IIII)V
/* .line 103 */
return;
} // .end method
public void onPlayerRlease ( Integer p0, Integer p1, Integer p2 ) {
/* .locals 1 */
/* .param p1, "uid" # I */
/* .param p2, "pid" # I */
/* .param p3, "piid" # I */
/* .line 98 */
v0 = this.mAudioResource;
(( com.miui.server.smartpower.AppAudioResource ) v0 ).onPlayerRlease ( p1, p2, p3 ); // invoke-virtual {v0, p1, p2, p3}, Lcom/miui/server/smartpower/AppAudioResource;->onPlayerRlease(III)V
/* .line 99 */
return;
} // .end method
public void onPlayerTrack ( Integer p0, Integer p1, Integer p2, Integer p3 ) {
/* .locals 1 */
/* .param p1, "uid" # I */
/* .param p2, "pid" # I */
/* .param p3, "piid" # I */
/* .param p4, "sessionId" # I */
/* .line 94 */
v0 = this.mAudioResource;
(( com.miui.server.smartpower.AppAudioResource ) v0 ).onPlayerTrack ( p1, p2, p3, p4 ); // invoke-virtual {v0, p1, p2, p3, p4}, Lcom/miui/server/smartpower/AppAudioResource;->onPlayerTrack(IIII)V
/* .line 95 */
return;
} // .end method
public void onRecorderEvent ( Integer p0, Integer p1, Integer p2 ) {
/* .locals 1 */
/* .param p1, "uid" # I */
/* .param p2, "riid" # I */
/* .param p3, "event" # I */
/* .line 114 */
v0 = this.mAudioResource;
(( com.miui.server.smartpower.AppAudioResource ) v0 ).onRecorderEvent ( p1, p2, p3 ); // invoke-virtual {v0, p1, p2, p3}, Lcom/miui/server/smartpower/AppAudioResource;->onRecorderEvent(III)V
/* .line 115 */
return;
} // .end method
public void onRecorderRlease ( Integer p0, Integer p1 ) {
/* .locals 1 */
/* .param p1, "uid" # I */
/* .param p2, "riid" # I */
/* .line 110 */
v0 = this.mAudioResource;
(( com.miui.server.smartpower.AppAudioResource ) v0 ).onRecorderRlease ( p1, p2 ); // invoke-virtual {v0, p1, p2}, Lcom/miui/server/smartpower/AppAudioResource;->onRecorderRlease(II)V
/* .line 111 */
return;
} // .end method
public void onRecorderTrack ( Integer p0, Integer p1, Integer p2 ) {
/* .locals 1 */
/* .param p1, "uid" # I */
/* .param p2, "pid" # I */
/* .param p3, "riid" # I */
/* .line 106 */
v0 = this.mAudioResource;
(( com.miui.server.smartpower.AppAudioResource ) v0 ).onRecorderTrack ( p1, p2, p3 ); // invoke-virtual {v0, p1, p2, p3}, Lcom/miui/server/smartpower/AppAudioResource;->onRecorderTrack(III)V
/* .line 107 */
return;
} // .end method
public void onReleaseLocation ( Integer p0, Integer p1, android.location.ILocationListener p2 ) {
/* .locals 1 */
/* .param p1, "uid" # I */
/* .param p2, "pid" # I */
/* .param p3, "listener" # Landroid/location/ILocationListener; */
/* .line 130 */
v0 = this.mGPSResource;
(( com.miui.server.smartpower.AppGPSResource ) v0 ).onReleaseLocation ( p1, p2, p3 ); // invoke-virtual {v0, p1, p2, p3}, Lcom/miui/server/smartpower/AppGPSResource;->onReleaseLocation(IILandroid/location/ILocationListener;)V
/* .line 131 */
return;
} // .end method
public void onReleaseWakelock ( android.os.IBinder p0, Integer p1 ) {
/* .locals 1 */
/* .param p1, "lock" # Landroid/os/IBinder; */
/* .param p2, "flags" # I */
/* .line 139 */
v0 = this.mWakelockResource;
(( com.miui.server.smartpower.AppWakelockResource ) v0 ).releaseWakelock ( p1, p2 ); // invoke-virtual {v0, p1, p2}, Lcom/miui/server/smartpower/AppWakelockResource;->releaseWakelock(Landroid/os/IBinder;I)V
/* .line 140 */
return;
} // .end method
public void playbackStateChanged ( Integer p0, Integer p1, Integer p2, Integer p3 ) {
/* .locals 1 */
/* .param p1, "uid" # I */
/* .param p2, "pid" # I */
/* .param p3, "oldState" # I */
/* .param p4, "newState" # I */
/* .line 82 */
v0 = this.mAudioResource;
(( com.miui.server.smartpower.AppAudioResource ) v0 ).playbackStateChanged ( p1, p2, p3, p4 ); // invoke-virtual {v0, p1, p2, p3, p4}, Lcom/miui/server/smartpower/AppAudioResource;->playbackStateChanged(IIII)V
/* .line 83 */
return;
} // .end method
public void recordAudioFocus ( Integer p0, Integer p1, java.lang.String p2, Boolean p3 ) {
/* .locals 1 */
/* .param p1, "uid" # I */
/* .param p2, "pid" # I */
/* .param p3, "clientId" # Ljava/lang/String; */
/* .param p4, "request" # Z */
/* .line 86 */
v0 = this.mAudioResource;
(( com.miui.server.smartpower.AppAudioResource ) v0 ).recordAudioFocus ( p1, p2, p3, p4 ); // invoke-virtual {v0, p1, p2, p3, p4}, Lcom/miui/server/smartpower/AppAudioResource;->recordAudioFocus(IILjava/lang/String;Z)V
/* .line 87 */
return;
} // .end method
public void recordAudioFocusLoss ( Integer p0, java.lang.String p1, Integer p2 ) {
/* .locals 1 */
/* .param p1, "uid" # I */
/* .param p2, "clientId" # Ljava/lang/String; */
/* .param p3, "focusLoss" # I */
/* .line 90 */
v0 = this.mAudioResource;
(( com.miui.server.smartpower.AppAudioResource ) v0 ).recordAudioFocusLoss ( p1, p2, p3 ); // invoke-virtual {v0, p1, p2, p3}, Lcom/miui/server/smartpower/AppAudioResource;->recordAudioFocusLoss(ILjava/lang/String;I)V
/* .line 91 */
return;
} // .end method
public void registerCallback ( Integer p0, com.miui.server.smartpower.AppPowerResource$IAppPowerResourceCallback p1, Integer p2 ) {
/* .locals 2 */
/* .param p1, "type" # I */
/* .param p2, "callback" # Lcom/miui/server/smartpower/AppPowerResource$IAppPowerResourceCallback; */
/* .param p3, "uid" # I */
/* .line 235 */
v0 = this.mDeviceResouceMap;
java.lang.Integer .valueOf ( p1 );
(( java.util.HashMap ) v0 ).get ( v1 ); // invoke-virtual {v0, v1}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;
/* check-cast v0, Lcom/miui/server/smartpower/AppPowerResource; */
/* .line 236 */
/* .local v0, "res":Lcom/miui/server/smartpower/AppPowerResource; */
if ( v0 != null) { // if-eqz v0, :cond_0
/* .line 237 */
(( com.miui.server.smartpower.AppPowerResource ) v0 ).registerCallback ( p2, p3 ); // invoke-virtual {v0, p2, p3}, Lcom/miui/server/smartpower/AppPowerResource;->registerCallback(Lcom/miui/server/smartpower/AppPowerResource$IAppPowerResourceCallback;I)V
/* .line 239 */
} // :cond_0
return;
} // .end method
public void registerCallback ( Integer p0, com.miui.server.smartpower.AppPowerResource$IAppPowerResourceCallback p1, Integer p2, Integer p3 ) {
/* .locals 2 */
/* .param p1, "type" # I */
/* .param p2, "callback" # Lcom/miui/server/smartpower/AppPowerResource$IAppPowerResourceCallback; */
/* .param p3, "uid" # I */
/* .param p4, "pid" # I */
/* .line 249 */
v0 = this.mDeviceResouceMap;
java.lang.Integer .valueOf ( p1 );
(( java.util.HashMap ) v0 ).get ( v1 ); // invoke-virtual {v0, v1}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;
/* check-cast v0, Lcom/miui/server/smartpower/AppPowerResource; */
/* .line 250 */
/* .local v0, "res":Lcom/miui/server/smartpower/AppPowerResource; */
if ( v0 != null) { // if-eqz v0, :cond_0
/* .line 251 */
(( com.miui.server.smartpower.AppPowerResource ) v0 ).registerCallback ( p2, p3, p4 ); // invoke-virtual {v0, p2, p3, p4}, Lcom/miui/server/smartpower/AppPowerResource;->registerCallback(Lcom/miui/server/smartpower/AppPowerResource$IAppPowerResourceCallback;II)V
/* .line 253 */
} // :cond_0
return;
} // .end method
public void releaseAppAllPowerResources ( Integer p0 ) {
/* .locals 2 */
/* .param p1, "uid" # I */
/* .line 185 */
v0 = this.mDeviceResouceMap;
(( java.util.HashMap ) v0 ).values ( ); // invoke-virtual {v0}, Ljava/util/HashMap;->values()Ljava/util/Collection;
v1 = } // :goto_0
if ( v1 != null) { // if-eqz v1, :cond_1
/* check-cast v1, Lcom/miui/server/smartpower/AppPowerResource; */
/* .line 186 */
/* .local v1, "res":Lcom/miui/server/smartpower/AppPowerResource; */
if ( v1 != null) { // if-eqz v1, :cond_0
/* .line 187 */
(( com.miui.server.smartpower.AppPowerResource ) v1 ).releaseAppPowerResource ( p1 ); // invoke-virtual {v1, p1}, Lcom/miui/server/smartpower/AppPowerResource;->releaseAppPowerResource(I)V
/* .line 189 */
} // .end local v1 # "res":Lcom/miui/server/smartpower/AppPowerResource;
} // :cond_0
/* .line 190 */
} // :cond_1
return;
} // .end method
public void releaseAppPowerResources ( Integer p0, java.util.ArrayList p1 ) {
/* .locals 4 */
/* .param p1, "uid" # I */
/* .annotation system Ldalvik/annotation/Signature; */
/* value = { */
/* "(I", */
/* "Ljava/util/ArrayList<", */
/* "Ljava/lang/Integer;", */
/* ">;)V" */
/* } */
} // .end annotation
/* .line 161 */
/* .local p2, "resourceTypes":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/Integer;>;" */
(( java.util.ArrayList ) p2 ).iterator ( ); // invoke-virtual {p2}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;
v1 = } // :goto_0
if ( v1 != null) { // if-eqz v1, :cond_1
/* check-cast v1, Ljava/lang/Integer; */
v1 = (( java.lang.Integer ) v1 ).intValue ( ); // invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I
/* .line 162 */
/* .local v1, "type":I */
v2 = this.mDeviceResouceMap;
java.lang.Integer .valueOf ( v1 );
(( java.util.HashMap ) v2 ).get ( v3 ); // invoke-virtual {v2, v3}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;
/* check-cast v2, Lcom/miui/server/smartpower/AppPowerResource; */
/* .line 163 */
/* .local v2, "res":Lcom/miui/server/smartpower/AppPowerResource; */
if ( v2 != null) { // if-eqz v2, :cond_0
/* .line 164 */
(( com.miui.server.smartpower.AppPowerResource ) v2 ).releaseAppPowerResource ( p1 ); // invoke-virtual {v2, p1}, Lcom/miui/server/smartpower/AppPowerResource;->releaseAppPowerResource(I)V
/* .line 166 */
} // .end local v1 # "type":I
} // .end local v2 # "res":Lcom/miui/server/smartpower/AppPowerResource;
} // :cond_0
/* .line 167 */
} // :cond_1
return;
} // .end method
public void reportTrackStatus ( Integer p0, Integer p1, Integer p2, Boolean p3 ) {
/* .locals 1 */
/* .param p1, "uid" # I */
/* .param p2, "pid" # I */
/* .param p3, "sessionId" # I */
/* .param p4, "isMuted" # Z */
/* .line 148 */
v0 = this.mAudioResource;
(( com.miui.server.smartpower.AppAudioResource ) v0 ).reportTrackStatus ( p1, p2, p3, p4 ); // invoke-virtual {v0, p1, p2, p3, p4}, Lcom/miui/server/smartpower/AppAudioResource;->reportTrackStatus(IIIZ)V
/* .line 149 */
return;
} // .end method
public void resumeAppAllPowerResources ( Integer p0 ) {
/* .locals 2 */
/* .param p1, "uid" # I */
/* .line 196 */
v0 = this.mDeviceResouceMap;
(( java.util.HashMap ) v0 ).values ( ); // invoke-virtual {v0}, Ljava/util/HashMap;->values()Ljava/util/Collection;
v1 = } // :goto_0
if ( v1 != null) { // if-eqz v1, :cond_1
/* check-cast v1, Lcom/miui/server/smartpower/AppPowerResource; */
/* .line 197 */
/* .local v1, "res":Lcom/miui/server/smartpower/AppPowerResource; */
if ( v1 != null) { // if-eqz v1, :cond_0
/* .line 198 */
(( com.miui.server.smartpower.AppPowerResource ) v1 ).resumeAppPowerResource ( p1 ); // invoke-virtual {v1, p1}, Lcom/miui/server/smartpower/AppPowerResource;->resumeAppPowerResource(I)V
/* .line 200 */
} // .end local v1 # "res":Lcom/miui/server/smartpower/AppPowerResource;
} // :cond_0
/* .line 201 */
} // :cond_1
return;
} // .end method
public void resumeAppPowerResources ( Integer p0, java.util.ArrayList p1 ) {
/* .locals 4 */
/* .param p1, "uid" # I */
/* .annotation system Ldalvik/annotation/Signature; */
/* value = { */
/* "(I", */
/* "Ljava/util/ArrayList<", */
/* "Ljava/lang/Integer;", */
/* ">;)V" */
/* } */
} // .end annotation
/* .line 173 */
/* .local p2, "resourceTypes":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/Integer;>;" */
(( java.util.ArrayList ) p2 ).iterator ( ); // invoke-virtual {p2}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;
v1 = } // :goto_0
if ( v1 != null) { // if-eqz v1, :cond_1
/* check-cast v1, Ljava/lang/Integer; */
v1 = (( java.lang.Integer ) v1 ).intValue ( ); // invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I
/* .line 174 */
/* .local v1, "type":I */
v2 = this.mDeviceResouceMap;
java.lang.Integer .valueOf ( v1 );
(( java.util.HashMap ) v2 ).get ( v3 ); // invoke-virtual {v2, v3}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;
/* check-cast v2, Lcom/miui/server/smartpower/AppPowerResource; */
/* .line 175 */
/* .local v2, "res":Lcom/miui/server/smartpower/AppPowerResource; */
if ( v2 != null) { // if-eqz v2, :cond_0
/* .line 176 */
(( com.miui.server.smartpower.AppPowerResource ) v2 ).resumeAppPowerResource ( p1 ); // invoke-virtual {v2, p1}, Lcom/miui/server/smartpower/AppPowerResource;->resumeAppPowerResource(I)V
/* .line 178 */
} // .end local v1 # "type":I
} // .end local v2 # "res":Lcom/miui/server/smartpower/AppPowerResource;
} // :cond_0
/* .line 179 */
} // :cond_1
return;
} // .end method
public void uidAudioStatusChanged ( Integer p0, Boolean p1 ) {
/* .locals 1 */
/* .param p1, "uid" # I */
/* .param p2, "active" # Z */
/* .line 118 */
v0 = this.mAudioResource;
(( com.miui.server.smartpower.AppAudioResource ) v0 ).uidAudioStatusChanged ( p1, p2 ); // invoke-virtual {v0, p1, p2}, Lcom/miui/server/smartpower/AppAudioResource;->uidAudioStatusChanged(IZ)V
/* .line 119 */
return;
} // .end method
public void uidVideoStatusChanged ( Integer p0, Boolean p1 ) {
/* .locals 1 */
/* .param p1, "uid" # I */
/* .param p2, "active" # Z */
/* .line 122 */
v0 = this.mAudioResource;
(( com.miui.server.smartpower.AppAudioResource ) v0 ).uidVideoStatusChanged ( p1, p2 ); // invoke-virtual {v0, p1, p2}, Lcom/miui/server/smartpower/AppAudioResource;->uidVideoStatusChanged(IZ)V
/* .line 123 */
return;
} // .end method
public void unRegisterCallback ( Integer p0, com.miui.server.smartpower.AppPowerResource$IAppPowerResourceCallback p1, Integer p2 ) {
/* .locals 2 */
/* .param p1, "type" # I */
/* .param p2, "callback" # Lcom/miui/server/smartpower/AppPowerResource$IAppPowerResourceCallback; */
/* .param p3, "uid" # I */
/* .line 242 */
v0 = this.mDeviceResouceMap;
java.lang.Integer .valueOf ( p1 );
(( java.util.HashMap ) v0 ).get ( v1 ); // invoke-virtual {v0, v1}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;
/* check-cast v0, Lcom/miui/server/smartpower/AppPowerResource; */
/* .line 243 */
/* .local v0, "res":Lcom/miui/server/smartpower/AppPowerResource; */
if ( v0 != null) { // if-eqz v0, :cond_0
/* .line 244 */
(( com.miui.server.smartpower.AppPowerResource ) v0 ).unRegisterCallback ( p2, p3 ); // invoke-virtual {v0, p2, p3}, Lcom/miui/server/smartpower/AppPowerResource;->unRegisterCallback(Lcom/miui/server/smartpower/AppPowerResource$IAppPowerResourceCallback;I)V
/* .line 246 */
} // :cond_0
return;
} // .end method
public void unRegisterCallback ( Integer p0, com.miui.server.smartpower.AppPowerResource$IAppPowerResourceCallback p1, Integer p2, Integer p3 ) {
/* .locals 2 */
/* .param p1, "type" # I */
/* .param p2, "callback" # Lcom/miui/server/smartpower/AppPowerResource$IAppPowerResourceCallback; */
/* .param p3, "uid" # I */
/* .param p4, "pid" # I */
/* .line 256 */
v0 = this.mDeviceResouceMap;
java.lang.Integer .valueOf ( p1 );
(( java.util.HashMap ) v0 ).get ( v1 ); // invoke-virtual {v0, v1}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;
/* check-cast v0, Lcom/miui/server/smartpower/AppPowerResource; */
/* .line 257 */
/* .local v0, "res":Lcom/miui/server/smartpower/AppPowerResource; */
if ( v0 != null) { // if-eqz v0, :cond_0
/* .line 258 */
(( com.miui.server.smartpower.AppPowerResource ) v0 ).unRegisterCallback ( p2, p3, p4 ); // invoke-virtual {v0, p2, p3, p4}, Lcom/miui/server/smartpower/AppPowerResource;->unRegisterCallback(Lcom/miui/server/smartpower/AppPowerResource$IAppPowerResourceCallback;II)V
/* .line 260 */
} // :cond_0
return;
} // .end method
