.class Lcom/miui/server/smartpower/AppDisplayResource$DisplayRecord;
.super Ljava/lang/Object;
.source "AppDisplayResource.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/miui/server/smartpower/AppDisplayResource;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = "DisplayRecord"
.end annotation


# instance fields
.field private mActive:Z

.field private mBehavier:I

.field private final mDisplays:Landroid/util/ArrayMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/util/ArrayMap<",
            "Ljava/lang/Integer;",
            "Landroid/view/Display;",
            ">;"
        }
    .end annotation
.end field

.field private final mUid:I

.field final synthetic this$0:Lcom/miui/server/smartpower/AppDisplayResource;


# direct methods
.method static bridge synthetic -$$Nest$fgetmBehavier(Lcom/miui/server/smartpower/AppDisplayResource$DisplayRecord;)I
    .locals 0

    iget p0, p0, Lcom/miui/server/smartpower/AppDisplayResource$DisplayRecord;->mBehavier:I

    return p0
.end method

.method static bridge synthetic -$$Nest$fgetmUid(Lcom/miui/server/smartpower/AppDisplayResource$DisplayRecord;)I
    .locals 0

    iget p0, p0, Lcom/miui/server/smartpower/AppDisplayResource$DisplayRecord;->mUid:I

    return p0
.end method

.method static bridge synthetic -$$Nest$mupdateCurrentStatus(Lcom/miui/server/smartpower/AppDisplayResource$DisplayRecord;)V
    .locals 0

    invoke-direct {p0}, Lcom/miui/server/smartpower/AppDisplayResource$DisplayRecord;->updateCurrentStatus()V

    return-void
.end method

.method constructor <init>(Lcom/miui/server/smartpower/AppDisplayResource;I)V
    .locals 1
    .param p1, "this$0"    # Lcom/miui/server/smartpower/AppDisplayResource;
    .param p2, "uid"    # I

    .line 190
    iput-object p1, p0, Lcom/miui/server/smartpower/AppDisplayResource$DisplayRecord;->this$0:Lcom/miui/server/smartpower/AppDisplayResource;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 188
    new-instance v0, Landroid/util/ArrayMap;

    invoke-direct {v0}, Landroid/util/ArrayMap;-><init>()V

    iput-object v0, p0, Lcom/miui/server/smartpower/AppDisplayResource$DisplayRecord;->mDisplays:Landroid/util/ArrayMap;

    .line 191
    iput p2, p0, Lcom/miui/server/smartpower/AppDisplayResource$DisplayRecord;->mUid:I

    .line 192
    return-void
.end method

.method private updateCurrentStatus()V
    .locals 7

    .line 219
    const/4 v0, 0x0

    .line 220
    .local v0, "behavier":I
    const/4 v1, 0x0

    .line 221
    .local v1, "active":Z
    iget-object v2, p0, Lcom/miui/server/smartpower/AppDisplayResource$DisplayRecord;->mDisplays:Landroid/util/ArrayMap;

    monitor-enter v2

    .line 222
    :try_start_0
    iget-object v3, p0, Lcom/miui/server/smartpower/AppDisplayResource$DisplayRecord;->mDisplays:Landroid/util/ArrayMap;

    invoke-virtual {v3}, Landroid/util/ArrayMap;->size()I

    move-result v3

    if-lez v3, :cond_0

    const/4 v3, 0x1

    goto :goto_0

    :cond_0
    const/4 v3, 0x0

    :goto_0
    move v1, v3

    .line 223
    iget-object v3, p0, Lcom/miui/server/smartpower/AppDisplayResource$DisplayRecord;->mDisplays:Landroid/util/ArrayMap;

    invoke-virtual {v3}, Landroid/util/ArrayMap;->values()Ljava/util/Collection;

    move-result-object v3

    invoke-interface {v3}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :goto_1
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v4

    if-eqz v4, :cond_2

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Landroid/view/Display;

    .line 224
    .local v4, "display":Landroid/view/Display;
    invoke-virtual {v4}, Landroid/view/Display;->getType()I

    move-result v5

    packed-switch v5, :pswitch_data_0

    :pswitch_0
    goto :goto_2

    .line 232
    :pswitch_1
    iget-object v5, p0, Lcom/miui/server/smartpower/AppDisplayResource$DisplayRecord;->this$0:Lcom/miui/server/smartpower/AppDisplayResource;

    invoke-static {v5}, Lcom/miui/server/smartpower/AppDisplayResource;->-$$Nest$fgetmWifiP2pConnected(Lcom/miui/server/smartpower/AppDisplayResource;)Z

    move-result v5

    if-eqz v5, :cond_1

    iget-object v5, p0, Lcom/miui/server/smartpower/AppDisplayResource$DisplayRecord;->this$0:Lcom/miui/server/smartpower/AppDisplayResource;

    invoke-static {v5}, Lcom/miui/server/smartpower/AppDisplayResource;->-$$Nest$fgetmCastingWhitelists(Lcom/miui/server/smartpower/AppDisplayResource;)Landroid/util/ArraySet;

    move-result-object v5

    .line 233
    invoke-virtual {v4}, Landroid/view/Display;->getOwnerPackageName()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6}, Landroid/util/ArraySet;->contains(Ljava/lang/Object;)Z

    move-result v5

    if-eqz v5, :cond_1

    .line 234
    or-int/lit16 v0, v0, 0x100

    goto :goto_2

    .line 226
    :pswitch_2
    or-int/lit16 v0, v0, 0x100

    .line 227
    goto :goto_2

    .line 229
    :pswitch_3
    or-int/lit16 v0, v0, 0x200

    .line 230
    nop

    .line 238
    .end local v4    # "display":Landroid/view/Display;
    :cond_1
    :goto_2
    goto :goto_1

    .line 239
    :cond_2
    monitor-exit v2
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 240
    iget-boolean v2, p0, Lcom/miui/server/smartpower/AppDisplayResource$DisplayRecord;->mActive:Z

    if-ne v1, v2, :cond_3

    iget v2, p0, Lcom/miui/server/smartpower/AppDisplayResource$DisplayRecord;->mBehavier:I

    if-eq v0, v2, :cond_5

    .line 241
    :cond_3
    sget-boolean v2, Lcom/miui/server/smartpower/AppPowerResource;->DEBUG:Z

    if-eqz v2, :cond_4

    .line 242
    const-string v2, "SmartPower.AppResource"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    iget v4, p0, Lcom/miui/server/smartpower/AppDisplayResource$DisplayRecord;->mUid:I

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, " display active "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    iget-boolean v4, p0, Lcom/miui/server/smartpower/AppDisplayResource$DisplayRecord;->mActive:Z

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, " "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    iget v4, p0, Lcom/miui/server/smartpower/AppDisplayResource$DisplayRecord;->mBehavier:I

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Landroid/util/Slog;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 244
    :cond_4
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "display u:"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget v3, p0, Lcom/miui/server/smartpower/AppDisplayResource$DisplayRecord;->mUid:I

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, " s:"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    const v3, 0x15ff2

    invoke-static {v3, v2}, Landroid/util/EventLog;->writeEvent(ILjava/lang/String;)I

    .line 246
    iget-object v2, p0, Lcom/miui/server/smartpower/AppDisplayResource$DisplayRecord;->this$0:Lcom/miui/server/smartpower/AppDisplayResource;

    iget v3, p0, Lcom/miui/server/smartpower/AppDisplayResource$DisplayRecord;->mUid:I

    invoke-virtual {v2, v3, v1, v0}, Lcom/miui/server/smartpower/AppDisplayResource;->reportResourceStatus(IZI)V

    .line 247
    iput-boolean v1, p0, Lcom/miui/server/smartpower/AppDisplayResource$DisplayRecord;->mActive:Z

    .line 248
    iput v0, p0, Lcom/miui/server/smartpower/AppDisplayResource$DisplayRecord;->mBehavier:I

    .line 250
    :cond_5
    return-void

    .line 239
    :catchall_0
    move-exception v3

    :try_start_1
    monitor-exit v2
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    throw v3

    :pswitch_data_0
    .packed-switch 0x2
        :pswitch_3
        :pswitch_2
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method


# virtual methods
.method isActive()Z
    .locals 1

    .line 195
    iget-boolean v0, p0, Lcom/miui/server/smartpower/AppDisplayResource$DisplayRecord;->mActive:Z

    return v0
.end method

.method public onDisplayAdded(ILandroid/view/Display;)V
    .locals 4
    .param p1, "displayId"    # I
    .param p2, "display"    # Landroid/view/Display;

    .line 199
    iget-object v0, p0, Lcom/miui/server/smartpower/AppDisplayResource$DisplayRecord;->mDisplays:Landroid/util/ArrayMap;

    monitor-enter v0

    .line 200
    :try_start_0
    iget-object v1, p0, Lcom/miui/server/smartpower/AppDisplayResource$DisplayRecord;->mDisplays:Landroid/util/ArrayMap;

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v1, v2}, Landroid/util/ArrayMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/view/Display;

    .line 201
    .local v1, "displayRecord":Landroid/view/Display;
    if-nez v1, :cond_0

    .line 202
    iget-object v2, p0, Lcom/miui/server/smartpower/AppDisplayResource$DisplayRecord;->mDisplays:Landroid/util/ArrayMap;

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    invoke-virtual {v2, v3, p2}, Landroid/util/ArrayMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 204
    .end local v1    # "displayRecord":Landroid/view/Display;
    :cond_0
    monitor-exit v0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 205
    invoke-direct {p0}, Lcom/miui/server/smartpower/AppDisplayResource$DisplayRecord;->updateCurrentStatus()V

    .line 206
    return-void

    .line 204
    :catchall_0
    move-exception v1

    :try_start_1
    monitor-exit v0
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    throw v1
.end method

.method public onDisplayRemoved(I)Z
    .locals 3
    .param p1, "displayId"    # I

    .line 209
    iget-object v0, p0, Lcom/miui/server/smartpower/AppDisplayResource$DisplayRecord;->mDisplays:Landroid/util/ArrayMap;

    monitor-enter v0

    .line 210
    :try_start_0
    iget-object v1, p0, Lcom/miui/server/smartpower/AppDisplayResource$DisplayRecord;->mDisplays:Landroid/util/ArrayMap;

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v1, v2}, Landroid/util/ArrayMap;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    if-nez v1, :cond_0

    .line 211
    monitor-exit v0

    const/4 v0, 0x0

    return v0

    .line 213
    :cond_0
    monitor-exit v0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 214
    invoke-direct {p0}, Lcom/miui/server/smartpower/AppDisplayResource$DisplayRecord;->updateCurrentStatus()V

    .line 215
    const/4 v0, 0x1

    return v0

    .line 213
    :catchall_0
    move-exception v1

    :try_start_1
    monitor-exit v0
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    throw v1
.end method
