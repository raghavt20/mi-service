public class com.miui.server.smartpower.SmartPowerPolicyManager implements com.android.server.am.AppStateManager$IProcessStateCallback {
	 /* .source "SmartPowerPolicyManager.java" */
	 /* # interfaces */
	 /* # annotations */
	 /* .annotation system Ldalvik/annotation/MemberClasses; */
	 /* value = { */
	 /* Lcom/miui/server/smartpower/SmartPowerPolicyManager$MyHandler;, */
	 /* Lcom/miui/server/smartpower/SmartPowerPolicyManager$PowerSavingStrategyControl;, */
	 /* Lcom/miui/server/smartpower/SmartPowerPolicyManager$WhiteListItem;, */
	 /* Lcom/miui/server/smartpower/SmartPowerPolicyManager$UsageStatsInfo; */
	 /* } */
} // .end annotation
/* # static fields */
public static final Boolean DEBUG;
public static final Integer INVALID_PID;
public static final Integer MSG_UPDATE_USAGESTATS;
public static final Long NEVER_PERIODIC_ACTIVE;
public static final Integer PERIODIC_ACTIVE_THRESHOLD_HIGH;
public static final java.lang.String REASON;
public static final java.lang.String TAG;
public static final Long UPDATE_USAGESTATS_DURATION;
public static final Integer WHITE_LIST_ACTION_HIBERNATION;
public static final Integer WHITE_LIST_ACTION_INTERCEPT_ALARM;
public static final Integer WHITE_LIST_ACTION_INTERCEPT_PROVIDER;
public static final Integer WHITE_LIST_ACTION_INTERCEPT_SERVICE;
public static final Integer WHITE_LIST_ACTION_SCREENON_HIBERNATION;
public static final Integer WHITE_LIST_TYPE_ALARM_CLOUDCONTROL;
public static final Integer WHITE_LIST_TYPE_ALARM_DEFAULT;
private static final Integer WHITE_LIST_TYPE_ALARM_MASK;
private static final Integer WHITE_LIST_TYPE_ALARM_MAX;
private static final Integer WHITE_LIST_TYPE_ALARM_MIN;
public static final Integer WHITE_LIST_TYPE_BACKUP;
public static final Integer WHITE_LIST_TYPE_CLOUDCONTROL;
public static final Integer WHITE_LIST_TYPE_CTS;
public static final Integer WHITE_LIST_TYPE_DEFAULT;
public static final Integer WHITE_LIST_TYPE_DEPEND;
public static final Integer WHITE_LIST_TYPE_EXTAUDIO;
public static final Integer WHITE_LIST_TYPE_FREQUENTTHAW;
private static final Integer WHITE_LIST_TYPE_HIBERNATION_MASK;
public static final Integer WHITE_LIST_TYPE_PROVIDER_CLOUDCONTROL;
public static final Integer WHITE_LIST_TYPE_PROVIDER_DEFAULT;
private static final Integer WHITE_LIST_TYPE_PROVIDER_MASK;
private static final Integer WHITE_LIST_TYPE_PROVIDER_MAX;
private static final Integer WHITE_LIST_TYPE_PROVIDER_MIN;
public static final Integer WHITE_LIST_TYPE_SCREENON_CLOUDCONTROL;
public static final Integer WHITE_LIST_TYPE_SCREENON_DEFAULT;
private static final Integer WHITE_LIST_TYPE_SCREENON_MASK;
private static final Integer WHITE_LIST_TYPE_SCREENON_MAX;
public static final Integer WHITE_LIST_TYPE_SERVICE_CLOUDCONTROL;
public static final Integer WHITE_LIST_TYPE_SERVICE_DEFAULT;
private static final Integer WHITE_LIST_TYPE_SERVICE_MASK;
private static final Integer WHITE_LIST_TYPE_SERVICE_MAX;
private static final Integer WHITE_LIST_TYPE_SERVICE_MIN;
public static final Integer WHITE_LIST_TYPE_USB;
public static final Integer WHITE_LIST_TYPE_VPN;
public static final Integer WHITE_LIST_TYPE_WALLPAPER;
private static final android.util.ArraySet sInterceptPackageList;
/* .annotation system Ldalvik/annotation/Signature; */
/* value = { */
/* "Landroid/util/ArraySet<", */
/* "Ljava/lang/String;", */
/* ">;" */
/* } */
} // .end annotation
} // .end field
/* # instance fields */
private com.android.server.am.ActivityManagerService mAMS;
private com.miui.server.smartpower.AppPowerResourceManager mAppPowerResourceManager;
private com.android.server.am.AppStateManager mAppStateManager;
private final android.util.ArraySet mBroadcastBlackList;
/* .annotation system Ldalvik/annotation/Signature; */
/* value = { */
/* "Landroid/util/ArraySet<", */
/* "Ljava/lang/String;", */
/* ">;" */
/* } */
} // .end annotation
} // .end field
private final android.util.ArraySet mBroadcastProcessActionWhiteList;
/* .annotation system Ldalvik/annotation/Signature; */
/* value = { */
/* "Landroid/util/ArraySet<", */
/* "Ljava/lang/String;", */
/* ">;" */
/* } */
} // .end annotation
} // .end field
private final android.util.ArraySet mBroadcastWhiteList;
/* .annotation system Ldalvik/annotation/Signature; */
/* value = { */
/* "Landroid/util/ArraySet<", */
/* "Ljava/lang/String;", */
/* ">;" */
/* } */
} // .end annotation
} // .end field
private android.content.Context mContext;
private final android.util.ArrayMap mDependencyMap;
/* .annotation system Ldalvik/annotation/Signature; */
/* value = { */
/* "Landroid/util/ArrayMap<", */
/* "Ljava/lang/Integer;", */
/* "Landroid/util/ArraySet<", */
/* "Ljava/lang/String;", */
/* ">;>;" */
/* } */
} // .end annotation
} // .end field
private android.os.Handler mHandler;
private com.android.server.DeviceIdleInternal mLocalDeviceIdleController;
private final android.util.ArrayMap mPackageWhiteList;
/* .annotation system Ldalvik/annotation/Signature; */
/* value = { */
/* "Landroid/util/ArrayMap<", */
/* "Ljava/lang/String;", */
/* "Lcom/miui/server/smartpower/SmartPowerPolicyManager$WhiteListItem;", */
/* ">;" */
/* } */
} // .end annotation
} // .end field
private final android.util.ArrayMap mPidWhiteList;
/* .annotation system Ldalvik/annotation/Signature; */
/* value = { */
/* "Landroid/util/ArrayMap<", */
/* "Ljava/lang/Integer;", */
/* "Lcom/miui/server/smartpower/SmartPowerPolicyManager$WhiteListItem;", */
/* ">;" */
/* } */
} // .end annotation
} // .end field
private com.miui.server.smartpower.PowerFrozenManager mPowerFrozenManager;
private com.miui.server.smartpower.SmartPowerPolicyManager$PowerSavingStrategyControl mPowerSavingStrategyControl;
private final android.util.ArrayMap mProcessWhiteList;
/* .annotation system Ldalvik/annotation/Signature; */
/* value = { */
/* "Landroid/util/ArrayMap<", */
/* "Ljava/lang/String;", */
/* "Lcom/miui/server/smartpower/SmartPowerPolicyManager$WhiteListItem;", */
/* ">;" */
/* } */
} // .end annotation
} // .end field
private final android.util.ArraySet mProviderPkgBlackList;
/* .annotation system Ldalvik/annotation/Signature; */
/* value = { */
/* "Landroid/util/ArraySet<", */
/* "Ljava/lang/String;", */
/* ">;" */
/* } */
} // .end annotation
} // .end field
private Integer mRunning32ProcCount;
private Boolean mScreenOff;
private final android.util.ArraySet mServicePkgBlackList;
/* .annotation system Ldalvik/annotation/Signature; */
/* value = { */
/* "Landroid/util/ArraySet<", */
/* "Ljava/lang/String;", */
/* ">;" */
/* } */
} // .end annotation
} // .end field
private final android.content.BroadcastReceiver mSysStatusReceiver;
private final android.util.ArrayMap mUidWhiteList;
/* .annotation system Ldalvik/annotation/Signature; */
/* value = { */
/* "Landroid/util/ArrayMap<", */
/* "Ljava/lang/Integer;", */
/* "Lcom/miui/server/smartpower/SmartPowerPolicyManager$WhiteListItem;", */
/* ">;" */
/* } */
} // .end annotation
} // .end field
private android.app.usage.UsageStatsManagerInternal mUsageStats;
private final java.util.HashMap mUsageStatsMap;
/* .annotation system Ldalvik/annotation/Signature; */
/* value = { */
/* "Ljava/util/HashMap<", */
/* "Ljava/lang/String;", */
/* "Lcom/miui/server/smartpower/SmartPowerPolicyManager$UsageStatsInfo;", */
/* ">;" */
/* } */
} // .end annotation
} // .end field
/* # direct methods */
public static void $r8$lambda$Qz5QQMFII7qrSmYkw_CQ3bKW_mQ ( com.miui.server.smartpower.SmartPowerPolicyManager p0, com.android.server.am.AppStateManager$AppState p1 ) { //synthethic
/* .locals 0 */
/* invoke-direct {p0, p1}, Lcom/miui/server/smartpower/SmartPowerPolicyManager;->lambda$idleApp$0(Lcom/android/server/am/AppStateManager$AppState;)V */
return;
} // .end method
static com.android.server.am.AppStateManager -$$Nest$fgetmAppStateManager ( com.miui.server.smartpower.SmartPowerPolicyManager p0 ) { //bridge//synthethic
/* .locals 0 */
p0 = this.mAppStateManager;
} // .end method
static android.content.Context -$$Nest$fgetmContext ( com.miui.server.smartpower.SmartPowerPolicyManager p0 ) { //bridge//synthethic
/* .locals 0 */
p0 = this.mContext;
} // .end method
static android.os.Handler -$$Nest$fgetmHandler ( com.miui.server.smartpower.SmartPowerPolicyManager p0 ) { //bridge//synthethic
/* .locals 0 */
p0 = this.mHandler;
} // .end method
static com.miui.server.smartpower.SmartPowerPolicyManager$PowerSavingStrategyControl -$$Nest$fgetmPowerSavingStrategyControl ( com.miui.server.smartpower.SmartPowerPolicyManager p0 ) { //bridge//synthethic
/* .locals 0 */
p0 = this.mPowerSavingStrategyControl;
} // .end method
static void -$$Nest$fputmScreenOff ( com.miui.server.smartpower.SmartPowerPolicyManager p0, Boolean p1 ) { //bridge//synthethic
/* .locals 0 */
/* iput-boolean p1, p0, Lcom/miui/server/smartpower/SmartPowerPolicyManager;->mScreenOff:Z */
return;
} // .end method
static void -$$Nest$mcompactProcess ( com.miui.server.smartpower.SmartPowerPolicyManager p0, java.lang.String p1, com.android.server.am.AppStateManager$AppState$RunningProcess p2 ) { //bridge//synthethic
/* .locals 0 */
/* invoke-direct {p0, p1, p2}, Lcom/miui/server/smartpower/SmartPowerPolicyManager;->compactProcess(Ljava/lang/String;Lcom/android/server/am/AppStateManager$AppState$RunningProcess;)V */
return;
} // .end method
static Boolean -$$Nest$mforzenProcess ( com.miui.server.smartpower.SmartPowerPolicyManager p0, Integer p1, Integer p2, java.lang.String p3, java.lang.String p4 ) { //bridge//synthethic
/* .locals 0 */
p0 = /* invoke-direct {p0, p1, p2, p3, p4}, Lcom/miui/server/smartpower/SmartPowerPolicyManager;->forzenProcess(IILjava/lang/String;Ljava/lang/String;)Z */
} // .end method
static void -$$Nest$mupdateUsageStats ( com.miui.server.smartpower.SmartPowerPolicyManager p0 ) { //bridge//synthethic
/* .locals 0 */
/* invoke-direct {p0}, Lcom/miui/server/smartpower/SmartPowerPolicyManager;->updateUsageStats()V */
return;
} // .end method
static java.lang.String -$$Nest$smwhiteListActionToString ( Integer p0 ) { //bridge//synthethic
/* .locals 0 */
com.miui.server.smartpower.SmartPowerPolicyManager .whiteListActionToString ( p0 );
} // .end method
static java.lang.String -$$Nest$smwhiteListTypeToString ( Integer p0 ) { //bridge//synthethic
/* .locals 0 */
com.miui.server.smartpower.SmartPowerPolicyManager .whiteListTypeToString ( p0 );
} // .end method
static com.miui.server.smartpower.SmartPowerPolicyManager ( ) {
/* .locals 1 */
/* .line 63 */
/* sget-boolean v0, Lcom/android/server/am/SmartPowerService;->DEBUG:Z */
com.miui.server.smartpower.SmartPowerPolicyManager.DEBUG = (v0!= 0);
/* .line 199 */
/* new-instance v0, Landroid/util/ArraySet; */
/* invoke-direct {v0}, Landroid/util/ArraySet;-><init>()V */
return;
} // .end method
public com.miui.server.smartpower.SmartPowerPolicyManager ( ) {
/* .locals 5 */
/* .param p1, "context" # Landroid/content/Context; */
/* .param p2, "looper" # Landroid/os/Looper; */
/* .param p3, "ams" # Lcom/android/server/am/ActivityManagerService; */
/* .param p4, "powerFrozenManager" # Lcom/miui/server/smartpower/PowerFrozenManager; */
/* .line 231 */
/* invoke-direct {p0}, Ljava/lang/Object;-><init>()V */
/* .line 159 */
/* new-instance v0, Landroid/util/ArrayMap; */
/* invoke-direct {v0}, Landroid/util/ArrayMap;-><init>()V */
this.mPackageWhiteList = v0;
/* .line 160 */
/* new-instance v0, Landroid/util/ArrayMap; */
/* invoke-direct {v0}, Landroid/util/ArrayMap;-><init>()V */
this.mProcessWhiteList = v0;
/* .line 165 */
/* new-instance v0, Landroid/util/ArrayMap; */
/* invoke-direct {v0}, Landroid/util/ArrayMap;-><init>()V */
this.mUidWhiteList = v0;
/* .line 166 */
/* new-instance v0, Landroid/util/ArrayMap; */
/* invoke-direct {v0}, Landroid/util/ArrayMap;-><init>()V */
this.mPidWhiteList = v0;
/* .line 172 */
/* new-instance v0, Landroid/util/ArrayMap; */
/* invoke-direct {v0}, Landroid/util/ArrayMap;-><init>()V */
this.mDependencyMap = v0;
/* .line 174 */
int v0 = 0; // const/4 v0, 0x0
this.mAppStateManager = v0;
/* .line 175 */
this.mAppPowerResourceManager = v0;
/* .line 179 */
/* new-instance v0, Landroid/util/ArraySet; */
/* invoke-direct {v0}, Landroid/util/ArraySet;-><init>()V */
this.mBroadcastBlackList = v0;
/* .line 184 */
/* new-instance v0, Landroid/util/ArraySet; */
/* invoke-direct {v0}, Landroid/util/ArraySet;-><init>()V */
this.mProviderPkgBlackList = v0;
/* .line 189 */
/* new-instance v0, Landroid/util/ArraySet; */
/* invoke-direct {v0}, Landroid/util/ArraySet;-><init>()V */
this.mServicePkgBlackList = v0;
/* .line 193 */
/* new-instance v0, Landroid/util/ArraySet; */
/* invoke-direct {v0}, Landroid/util/ArraySet;-><init>()V */
this.mBroadcastWhiteList = v0;
/* .line 194 */
/* new-instance v0, Landroid/util/ArraySet; */
/* invoke-direct {v0}, Landroid/util/ArraySet;-><init>()V */
this.mBroadcastProcessActionWhiteList = v0;
/* .line 208 */
/* new-instance v0, Ljava/util/HashMap; */
/* invoke-direct {v0}, Ljava/util/HashMap;-><init>()V */
this.mUsageStatsMap = v0;
/* .line 211 */
int v0 = 0; // const/4 v0, 0x0
/* iput-boolean v0, p0, Lcom/miui/server/smartpower/SmartPowerPolicyManager;->mScreenOff:Z */
/* .line 213 */
/* iput v0, p0, Lcom/miui/server/smartpower/SmartPowerPolicyManager;->mRunning32ProcCount:I */
/* .line 215 */
/* new-instance v0, Lcom/miui/server/smartpower/SmartPowerPolicyManager$1; */
/* invoke-direct {v0, p0}, Lcom/miui/server/smartpower/SmartPowerPolicyManager$1;-><init>(Lcom/miui/server/smartpower/SmartPowerPolicyManager;)V */
this.mSysStatusReceiver = v0;
/* .line 232 */
/* new-instance v0, Lcom/miui/server/smartpower/SmartPowerPolicyManager$MyHandler; */
/* invoke-direct {v0, p0, p2}, Lcom/miui/server/smartpower/SmartPowerPolicyManager$MyHandler;-><init>(Lcom/miui/server/smartpower/SmartPowerPolicyManager;Landroid/os/Looper;)V */
this.mHandler = v0;
/* .line 233 */
this.mAMS = p3;
/* .line 234 */
this.mContext = p1;
/* .line 235 */
this.mPowerFrozenManager = p4;
/* .line 236 */
/* const-class v0, Landroid/app/usage/UsageStatsManagerInternal; */
com.android.server.LocalServices .getService ( v0 );
/* check-cast v0, Landroid/app/usage/UsageStatsManagerInternal; */
this.mUsageStats = v0;
/* .line 237 */
/* const-class v0, Lcom/android/server/DeviceIdleInternal; */
com.android.server.LocalServices .getService ( v0 );
/* check-cast v0, Lcom/android/server/DeviceIdleInternal; */
this.mLocalDeviceIdleController = v0;
/* .line 238 */
(( android.content.Context ) p1 ).getResources ( ); // invoke-virtual {p1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;
/* const v1, 0x110300cd */
(( android.content.res.Resources ) v0 ).getStringArray ( v1 ); // invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getStringArray(I)[Ljava/lang/String;
/* .line 240 */
/* .local v0, "packges":[Ljava/lang/String; */
int v1 = 0; // const/4 v1, 0x0
/* .local v1, "i":I */
} // :goto_0
/* array-length v2, v0 */
int v3 = 2; // const/4 v3, 0x2
/* if-ge v1, v2, :cond_0 */
/* .line 241 */
/* aget-object v2, v0, v1 */
/* invoke-direct {p0, v2, v3}, Lcom/miui/server/smartpower/SmartPowerPolicyManager;->addPackageWhiteList(Ljava/lang/String;I)V */
/* .line 240 */
/* add-int/lit8 v1, v1, 0x1 */
/* .line 244 */
} // .end local v1 # "i":I
} // :cond_0
(( android.content.Context ) p1 ).getResources ( ); // invoke-virtual {p1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;
/* const v2, 0x110300ce */
(( android.content.res.Resources ) v1 ).getStringArray ( v2 ); // invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getStringArray(I)[Ljava/lang/String;
/* .line 246 */
/* .local v1, "processes":[Ljava/lang/String; */
int v2 = 0; // const/4 v2, 0x0
/* .local v2, "i":I */
} // :goto_1
/* array-length v4, v1 */
/* if-ge v2, v4, :cond_1 */
/* .line 247 */
/* aget-object v4, v1, v2 */
/* invoke-direct {p0, v4, v3}, Lcom/miui/server/smartpower/SmartPowerPolicyManager;->addProcessWhiteList(Ljava/lang/String;I)V */
/* .line 246 */
/* add-int/lit8 v2, v2, 0x1 */
/* .line 250 */
} // .end local v2 # "i":I
} // :cond_1
(( android.content.Context ) p1 ).getResources ( ); // invoke-virtual {p1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;
/* const v3, 0x110300c7 */
(( android.content.res.Resources ) v2 ).getStringArray ( v3 ); // invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getStringArray(I)[Ljava/lang/String;
/* .line 252 */
int v2 = 0; // const/4 v2, 0x0
/* .restart local v2 # "i":I */
} // :goto_2
/* array-length v3, v0 */
/* if-ge v2, v3, :cond_2 */
/* .line 253 */
/* aget-object v3, v0, v2 */
/* invoke-direct {p0, v3}, Lcom/miui/server/smartpower/SmartPowerPolicyManager;->addAlarmPackageWhiteList(Ljava/lang/String;)V */
/* .line 252 */
/* add-int/lit8 v2, v2, 0x1 */
/* .line 256 */
} // .end local v2 # "i":I
} // :cond_2
(( android.content.Context ) p1 ).getResources ( ); // invoke-virtual {p1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;
/* const v3, 0x110300d0 */
(( android.content.res.Resources ) v2 ).getStringArray ( v3 ); // invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getStringArray(I)[Ljava/lang/String;
/* .line 258 */
int v2 = 0; // const/4 v2, 0x0
/* .restart local v2 # "i":I */
} // :goto_3
/* array-length v3, v0 */
/* if-ge v2, v3, :cond_3 */
/* .line 259 */
/* aget-object v3, v0, v2 */
/* invoke-direct {p0, v3}, Lcom/miui/server/smartpower/SmartPowerPolicyManager;->addServicePackageWhiteList(Ljava/lang/String;)V */
/* .line 258 */
/* add-int/lit8 v2, v2, 0x1 */
/* .line 262 */
} // .end local v2 # "i":I
} // :cond_3
(( android.content.Context ) p1 ).getResources ( ); // invoke-virtual {p1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;
/* const v3, 0x110300cf */
(( android.content.res.Resources ) v2 ).getStringArray ( v3 ); // invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getStringArray(I)[Ljava/lang/String;
/* .line 264 */
int v2 = 0; // const/4 v2, 0x0
/* .restart local v2 # "i":I */
} // :goto_4
/* array-length v3, v0 */
/* if-ge v2, v3, :cond_4 */
/* .line 265 */
/* aget-object v3, v0, v2 */
/* invoke-direct {p0, v3}, Lcom/miui/server/smartpower/SmartPowerPolicyManager;->addProviderPackageWhiteList(Ljava/lang/String;)V */
/* .line 264 */
/* add-int/lit8 v2, v2, 0x1 */
/* .line 268 */
} // .end local v2 # "i":I
} // :cond_4
(( android.content.Context ) p1 ).getResources ( ); // invoke-virtual {p1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;
/* const v3, 0x110300c9 */
(( android.content.res.Resources ) v2 ).getStringArray ( v3 ); // invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getStringArray(I)[Ljava/lang/String;
/* .line 270 */
int v2 = 0; // const/4 v2, 0x0
/* .restart local v2 # "i":I */
} // :goto_5
/* array-length v3, v0 */
/* if-ge v2, v3, :cond_5 */
/* .line 271 */
/* aget-object v3, v0, v2 */
/* invoke-direct {p0, v3}, Lcom/miui/server/smartpower/SmartPowerPolicyManager;->addBroadcastProcessActionWhiteList(Ljava/lang/String;)V */
/* .line 270 */
/* add-int/lit8 v2, v2, 0x1 */
/* .line 274 */
} // .end local v2 # "i":I
} // :cond_5
v2 = this.mBroadcastBlackList;
(( android.content.Context ) p1 ).getResources ( ); // invoke-virtual {p1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;
/* const v4, 0x110300c8 */
(( android.content.res.Resources ) v3 ).getStringArray ( v4 ); // invoke-virtual {v3, v4}, Landroid/content/res/Resources;->getStringArray(I)[Ljava/lang/String;
java.util.Arrays .asList ( v3 );
(( android.util.ArraySet ) v2 ).addAll ( v3 ); // invoke-virtual {v2, v3}, Landroid/util/ArraySet;->addAll(Ljava/util/Collection;)Z
/* .line 277 */
v2 = this.mBroadcastWhiteList;
(( android.content.Context ) p1 ).getResources ( ); // invoke-virtual {p1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;
/* const v4, 0x110300ca */
(( android.content.res.Resources ) v3 ).getStringArray ( v4 ); // invoke-virtual {v3, v4}, Landroid/content/res/Resources;->getStringArray(I)[Ljava/lang/String;
java.util.Arrays .asList ( v3 );
(( android.util.ArraySet ) v2 ).addAll ( v3 ); // invoke-virtual {v2, v3}, Landroid/util/ArraySet;->addAll(Ljava/util/Collection;)Z
/* .line 280 */
v2 = com.miui.server.smartpower.SmartPowerPolicyManager.sInterceptPackageList;
/* monitor-enter v2 */
/* .line 281 */
try { // :try_start_0
(( android.content.Context ) p1 ).getResources ( ); // invoke-virtual {p1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;
/* const v4, 0x110300cc */
(( android.content.res.Resources ) v3 ).getStringArray ( v4 ); // invoke-virtual {v3, v4}, Landroid/content/res/Resources;->getStringArray(I)[Ljava/lang/String;
java.util.Arrays .asList ( v3 );
(( android.util.ArraySet ) v2 ).addAll ( v3 ); // invoke-virtual {v2, v3}, Landroid/util/ArraySet;->addAll(Ljava/util/Collection;)Z
/* .line 283 */
/* monitor-exit v2 */
/* :try_end_0 */
/* .catchall {:try_start_0 ..:try_end_0} :catchall_0 */
/* .line 285 */
/* new-instance v2, Landroid/content/IntentFilter; */
/* invoke-direct {v2}, Landroid/content/IntentFilter;-><init>()V */
/* .line 286 */
/* .local v2, "filter":Landroid/content/IntentFilter; */
final String v3 = "android.intent.action.SCREEN_OFF"; // const-string v3, "android.intent.action.SCREEN_OFF"
(( android.content.IntentFilter ) v2 ).addAction ( v3 ); // invoke-virtual {v2, v3}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V
/* .line 287 */
final String v3 = "android.intent.action.SCREEN_ON"; // const-string v3, "android.intent.action.SCREEN_ON"
(( android.content.IntentFilter ) v2 ).addAction ( v3 ); // invoke-virtual {v2, v3}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V
/* .line 288 */
final String v3 = "android.intent.action.BOOT_COMPLETED"; // const-string v3, "android.intent.action.BOOT_COMPLETED"
(( android.content.IntentFilter ) v2 ).addAction ( v3 ); // invoke-virtual {v2, v3}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V
/* .line 289 */
v3 = this.mSysStatusReceiver;
(( android.content.Context ) p1 ).registerReceiver ( v3, v2 ); // invoke-virtual {p1, v3, v2}, Landroid/content/Context;->registerReceiver(Landroid/content/BroadcastReceiver;Landroid/content/IntentFilter;)Landroid/content/Intent;
/* .line 290 */
/* new-instance v3, Lcom/miui/server/smartpower/SmartPowerPolicyManager$PowerSavingStrategyControl; */
/* invoke-direct {v3, p0}, Lcom/miui/server/smartpower/SmartPowerPolicyManager$PowerSavingStrategyControl;-><init>(Lcom/miui/server/smartpower/SmartPowerPolicyManager;)V */
this.mPowerSavingStrategyControl = v3;
/* .line 291 */
/* invoke-direct {p0}, Lcom/miui/server/smartpower/SmartPowerPolicyManager;->registerTestSuitSpecificObserver()V */
/* .line 292 */
return;
/* .line 283 */
} // .end local v2 # "filter":Landroid/content/IntentFilter;
/* :catchall_0 */
/* move-exception v3 */
try { // :try_start_1
/* monitor-exit v2 */
/* :try_end_1 */
/* .catchall {:try_start_1 ..:try_end_1} :catchall_0 */
/* throw v3 */
} // .end method
private void activeApp ( com.android.server.am.AppStateManager$AppState p0, java.lang.String p1 ) {
/* .locals 2 */
/* .param p1, "app" # Lcom/android/server/am/AppStateManager$AppState; */
/* .param p2, "reason" # Ljava/lang/String; */
/* .line 864 */
v0 = /* invoke-direct {p0}, Lcom/miui/server/smartpower/SmartPowerPolicyManager;->isEnableFrozen()Z */
if ( v0 != null) { // if-eqz v0, :cond_0
/* .line 865 */
/* new-instance v0, Ljava/lang/StringBuilder; */
/* invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V */
final String v1 = "power active "; // const-string v1, "power active "
(( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
/* .line 866 */
(( com.android.server.am.AppStateManager$AppState ) p1 ).getPackageName ( ); // invoke-virtual {p1}, Lcom/android/server/am/AppStateManager$AppState;->getPackageName()Ljava/lang/String;
(( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
final String v1 = "/"; // const-string v1, "/"
(( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
v1 = (( com.android.server.am.AppStateManager$AppState ) p1 ).getUid ( ); // invoke-virtual {p1}, Lcom/android/server/am/AppStateManager$AppState;->getUid()I
(( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v0 ).toString ( ); // invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
/* .line 865 */
/* const v1, 0x15ff2 */
android.util.EventLog .writeEvent ( v1,v0 );
/* .line 867 */
v0 = this.mAppPowerResourceManager;
v1 = (( com.android.server.am.AppStateManager$AppState ) p1 ).getUid ( ); // invoke-virtual {p1}, Lcom/android/server/am/AppStateManager$AppState;->getUid()I
(( com.miui.server.smartpower.AppPowerResourceManager ) v0 ).resumeAppAllPowerResources ( v1 ); // invoke-virtual {v0, v1}, Lcom/miui/server/smartpower/AppPowerResourceManager;->resumeAppAllPowerResources(I)V
/* .line 869 */
} // :cond_0
return;
} // .end method
private void activeProcess ( com.android.server.am.AppStateManager$AppState$RunningProcess p0, java.lang.String p1 ) {
/* .locals 4 */
/* .param p1, "proc" # Lcom/android/server/am/AppStateManager$AppState$RunningProcess; */
/* .param p2, "reason" # Ljava/lang/String; */
/* .line 872 */
final String v0 = "activeProcess"; // const-string v0, "activeProcess"
/* const-wide/32 v1, 0x20000 */
android.os.Trace .traceBegin ( v1,v2,v0 );
/* .line 873 */
v0 = (( com.android.server.am.AppStateManager$AppState$RunningProcess ) p1 ).getUid ( ); // invoke-virtual {p1}, Lcom/android/server/am/AppStateManager$AppState$RunningProcess;->getUid()I
v3 = (( com.android.server.am.AppStateManager$AppState$RunningProcess ) p1 ).getPid ( ); // invoke-virtual {p1}, Lcom/android/server/am/AppStateManager$AppState$RunningProcess;->getPid()I
v0 = /* invoke-direct {p0, v0, v3, p2}, Lcom/miui/server/smartpower/SmartPowerPolicyManager;->thawProcess(IILjava/lang/String;)Z */
if ( v0 != null) { // if-eqz v0, :cond_0
/* .line 874 */
/* new-instance v0, Ljava/lang/StringBuilder; */
/* invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V */
final String v3 = "power active ("; // const-string v3, "power active ("
(( java.lang.StringBuilder ) v0 ).append ( v3 ); // invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
/* .line 875 */
v3 = (( com.android.server.am.AppStateManager$AppState$RunningProcess ) p1 ).getPid ( ); // invoke-virtual {p1}, Lcom/android/server/am/AppStateManager$AppState$RunningProcess;->getPid()I
(( java.lang.StringBuilder ) v0 ).append ( v3 ); // invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;
final String v3 = ")"; // const-string v3, ")"
(( java.lang.StringBuilder ) v0 ).append ( v3 ); // invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
/* .line 876 */
(( com.android.server.am.AppStateManager$AppState$RunningProcess ) p1 ).getProcessName ( ); // invoke-virtual {p1}, Lcom/android/server/am/AppStateManager$AppState$RunningProcess;->getProcessName()Ljava/lang/String;
(( java.lang.StringBuilder ) v0 ).append ( v3 ); // invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
final String v3 = "/"; // const-string v3, "/"
(( java.lang.StringBuilder ) v0 ).append ( v3 ); // invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
v3 = (( com.android.server.am.AppStateManager$AppState$RunningProcess ) p1 ).getUid ( ); // invoke-virtual {p1}, Lcom/android/server/am/AppStateManager$AppState$RunningProcess;->getUid()I
(( java.lang.StringBuilder ) v0 ).append ( v3 ); // invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v0 ).toString ( ); // invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
/* .line 874 */
/* const v3, 0x15ff2 */
android.util.EventLog .writeEvent ( v3,v0 );
/* .line 878 */
} // :cond_0
android.os.Trace .traceEnd ( v1,v2 );
/* .line 879 */
return;
} // .end method
private void addAlarmPackageWhiteList ( java.lang.String p0 ) {
/* .locals 4 */
/* .param p1, "pkgname" # Ljava/lang/String; */
/* .line 1231 */
v0 = this.mPackageWhiteList;
/* monitor-enter v0 */
/* .line 1232 */
try { // :try_start_0
v1 = this.mPackageWhiteList;
/* const v2, 0x8000 */
/* const/16 v3, 0x8 */
/* invoke-direct {p0, v1, p1, v2, v3}, Lcom/miui/server/smartpower/SmartPowerPolicyManager;->addStaticWhiteListLocked(Landroid/util/ArrayMap;Ljava/lang/String;II)V */
/* .line 1234 */
/* monitor-exit v0 */
/* .line 1235 */
return;
/* .line 1234 */
/* :catchall_0 */
/* move-exception v1 */
/* monitor-exit v0 */
/* :try_end_0 */
/* .catchall {:try_start_0 ..:try_end_0} :catchall_0 */
/* throw v1 */
} // .end method
private void addBroadcastBlackList ( java.lang.String p0 ) {
/* .locals 2 */
/* .param p1, "intent" # Ljava/lang/String; */
/* .line 1300 */
v0 = this.mBroadcastBlackList;
/* monitor-enter v0 */
/* .line 1301 */
try { // :try_start_0
v1 = this.mBroadcastBlackList;
(( android.util.ArraySet ) v1 ).add ( p1 ); // invoke-virtual {v1, p1}, Landroid/util/ArraySet;->add(Ljava/lang/Object;)Z
/* .line 1302 */
/* monitor-exit v0 */
/* .line 1303 */
return;
/* .line 1302 */
/* :catchall_0 */
/* move-exception v1 */
/* monitor-exit v0 */
/* :try_end_0 */
/* .catchall {:try_start_0 ..:try_end_0} :catchall_0 */
/* throw v1 */
} // .end method
private void addBroadcastProcessActionWhiteList ( java.lang.String p0 ) {
/* .locals 2 */
/* .param p1, "processAction" # Ljava/lang/String; */
/* .line 1294 */
v0 = this.mBroadcastProcessActionWhiteList;
/* monitor-enter v0 */
/* .line 1295 */
try { // :try_start_0
v1 = this.mBroadcastProcessActionWhiteList;
(( android.util.ArraySet ) v1 ).add ( p1 ); // invoke-virtual {v1, p1}, Landroid/util/ArraySet;->add(Ljava/lang/Object;)Z
/* .line 1296 */
/* monitor-exit v0 */
/* .line 1297 */
return;
/* .line 1296 */
/* :catchall_0 */
/* move-exception v1 */
/* monitor-exit v0 */
/* :try_end_0 */
/* .catchall {:try_start_0 ..:try_end_0} :catchall_0 */
/* throw v1 */
} // .end method
private void addBroadcastProcessActionWhiteList ( java.lang.String p0, java.lang.String p1 ) {
/* .locals 1 */
/* .param p1, "processName" # Ljava/lang/String; */
/* .param p2, "action" # Ljava/lang/String; */
/* .line 1290 */
/* new-instance v0, Ljava/lang/StringBuilder; */
/* invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V */
(( java.lang.StringBuilder ) v0 ).append ( p1 ); // invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v0 ).append ( p2 ); // invoke-virtual {v0, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v0 ).toString ( ); // invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
/* invoke-direct {p0, v0}, Lcom/miui/server/smartpower/SmartPowerPolicyManager;->addBroadcastProcessActionWhiteList(Ljava/lang/String;)V */
/* .line 1291 */
return;
} // .end method
private void addBroadcastWhiteList ( java.lang.String p0 ) {
/* .locals 2 */
/* .param p1, "intent" # Ljava/lang/String; */
/* .line 1336 */
v0 = this.mBroadcastWhiteList;
/* monitor-enter v0 */
/* .line 1337 */
try { // :try_start_0
v1 = this.mBroadcastWhiteList;
(( android.util.ArraySet ) v1 ).add ( p1 ); // invoke-virtual {v1, p1}, Landroid/util/ArraySet;->add(Ljava/lang/Object;)Z
/* .line 1338 */
/* monitor-exit v0 */
/* .line 1339 */
return;
/* .line 1338 */
/* :catchall_0 */
/* move-exception v1 */
/* monitor-exit v0 */
/* :try_end_0 */
/* .catchall {:try_start_0 ..:try_end_0} :catchall_0 */
/* throw v1 */
} // .end method
private com.miui.server.smartpower.SmartPowerPolicyManager$WhiteListItem addDynamicWhiteListLocked ( android.util.ArrayMap p0, java.lang.Integer p1, java.lang.String p2, Integer p3, Integer p4 ) {
/* .locals 2 */
/* .param p2, "key" # Ljava/lang/Integer; */
/* .param p3, "name" # Ljava/lang/String; */
/* .param p4, "type" # I */
/* .param p5, "action" # I */
/* .annotation system Ldalvik/annotation/Signature; */
/* value = { */
/* "(", */
/* "Landroid/util/ArrayMap<", */
/* "Ljava/lang/Integer;", */
/* "Lcom/miui/server/smartpower/SmartPowerPolicyManager$WhiteListItem;", */
/* ">;", */
/* "Ljava/lang/Integer;", */
/* "Ljava/lang/String;", */
/* "II)", */
/* "Lcom/miui/server/smartpower/SmartPowerPolicyManager$WhiteListItem;" */
/* } */
} // .end annotation
/* .line 1261 */
/* .local p1, "whiteList":Landroid/util/ArrayMap;, "Landroid/util/ArrayMap<Ljava/lang/Integer;Lcom/miui/server/smartpower/SmartPowerPolicyManager$WhiteListItem;>;" */
(( android.util.ArrayMap ) p1 ).get ( p2 ); // invoke-virtual {p1, p2}, Landroid/util/ArrayMap;->get(Ljava/lang/Object;)Ljava/lang/Object;
/* check-cast v0, Lcom/miui/server/smartpower/SmartPowerPolicyManager$WhiteListItem; */
/* .line 1262 */
/* .local v0, "item":Lcom/miui/server/smartpower/SmartPowerPolicyManager$WhiteListItem; */
/* if-nez v0, :cond_0 */
/* .line 1263 */
/* new-instance v1, Lcom/miui/server/smartpower/SmartPowerPolicyManager$WhiteListItem; */
/* invoke-direct {v1, p0, p3, p4, p5}, Lcom/miui/server/smartpower/SmartPowerPolicyManager$WhiteListItem;-><init>(Lcom/miui/server/smartpower/SmartPowerPolicyManager;Ljava/lang/String;II)V */
/* move-object v0, v1 */
/* .line 1264 */
(( android.util.ArrayMap ) p1 ).put ( p2, v0 ); // invoke-virtual {p1, p2, v0}, Landroid/util/ArrayMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;
/* .line 1265 */
/* .line 1267 */
} // :cond_0
v1 = com.miui.server.smartpower.SmartPowerPolicyManager$WhiteListItem .-$$Nest$fgetmTypes ( v0 );
/* or-int/2addr v1, p4 */
com.miui.server.smartpower.SmartPowerPolicyManager$WhiteListItem .-$$Nest$fputmTypes ( v0,v1 );
/* .line 1268 */
v1 = com.miui.server.smartpower.SmartPowerPolicyManager$WhiteListItem .-$$Nest$fgetmActions ( v0 );
/* or-int/2addr v1, p5 */
com.miui.server.smartpower.SmartPowerPolicyManager$WhiteListItem .-$$Nest$fputmActions ( v0,v1 );
/* .line 1270 */
int v1 = 0; // const/4 v1, 0x0
} // .end method
private void addPackageWhiteList ( java.lang.String p0, Integer p1 ) {
/* .locals 3 */
/* .param p1, "pkgName" # Ljava/lang/String; */
/* .param p2, "type" # I */
/* .line 1142 */
v0 = this.mPackageWhiteList;
/* monitor-enter v0 */
/* .line 1143 */
try { // :try_start_0
v1 = this.mPackageWhiteList;
int v2 = 2; // const/4 v2, 0x2
/* invoke-direct {p0, v1, p1, p2, v2}, Lcom/miui/server/smartpower/SmartPowerPolicyManager;->addStaticWhiteListLocked(Landroid/util/ArrayMap;Ljava/lang/String;II)V */
/* .line 1145 */
/* monitor-exit v0 */
/* .line 1146 */
return;
/* .line 1145 */
/* :catchall_0 */
/* move-exception v1 */
/* monitor-exit v0 */
/* :try_end_0 */
/* .catchall {:try_start_0 ..:try_end_0} :catchall_0 */
/* throw v1 */
} // .end method
private void addPidWhiteList ( Integer p0, Integer p1, java.lang.String p2, Integer p3 ) {
/* .locals 7 */
/* .param p1, "uid" # I */
/* .param p2, "pid" # I */
/* .param p3, "procName" # Ljava/lang/String; */
/* .param p4, "type" # I */
/* .line 1150 */
v0 = this.mPidWhiteList;
/* monitor-enter v0 */
/* .line 1151 */
try { // :try_start_0
v2 = this.mPidWhiteList;
/* .line 1152 */
java.lang.Integer .valueOf ( p2 );
int v6 = 2; // const/4 v6, 0x2
/* .line 1151 */
/* move-object v1, p0 */
/* move-object v4, p3 */
/* move v5, p4 */
/* invoke-direct/range {v1 ..v6}, Lcom/miui/server/smartpower/SmartPowerPolicyManager;->addDynamicWhiteListLocked(Landroid/util/ArrayMap;Ljava/lang/Integer;Ljava/lang/String;II)Lcom/miui/server/smartpower/SmartPowerPolicyManager$WhiteListItem; */
/* .line 1153 */
/* .local v1, "item":Lcom/miui/server/smartpower/SmartPowerPolicyManager$WhiteListItem; */
/* monitor-exit v0 */
/* :try_end_0 */
/* .catchall {:try_start_0 ..:try_end_0} :catchall_0 */
/* .line 1154 */
if ( v1 != null) { // if-eqz v1, :cond_0
/* .line 1155 */
com.miui.server.smartpower.SmartPowerPolicyManager$WhiteListItem .-$$Nest$fputmUid ( v1,p1 );
/* .line 1156 */
com.miui.server.smartpower.SmartPowerPolicyManager$WhiteListItem .-$$Nest$fputmPid ( v1,p2 );
/* .line 1157 */
v0 = this.mHandler;
/* new-instance v2, Lcom/miui/server/smartpower/SmartPowerPolicyManager$5; */
/* invoke-direct {v2, p0, p1, p2}, Lcom/miui/server/smartpower/SmartPowerPolicyManager$5;-><init>(Lcom/miui/server/smartpower/SmartPowerPolicyManager;II)V */
(( android.os.Handler ) v0 ).post ( v2 ); // invoke-virtual {v0, v2}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z
/* .line 1164 */
} // :cond_0
return;
/* .line 1153 */
} // .end local v1 # "item":Lcom/miui/server/smartpower/SmartPowerPolicyManager$WhiteListItem;
/* :catchall_0 */
/* move-exception v1 */
try { // :try_start_1
/* monitor-exit v0 */
/* :try_end_1 */
/* .catchall {:try_start_1 ..:try_end_1} :catchall_0 */
/* throw v1 */
} // .end method
private void addProcessWhiteList ( java.lang.String p0, Integer p1 ) {
/* .locals 3 */
/* .param p1, "procName" # Ljava/lang/String; */
/* .param p2, "type" # I */
/* .line 1183 */
v0 = this.mProcessWhiteList;
/* monitor-enter v0 */
/* .line 1184 */
try { // :try_start_0
v1 = this.mProcessWhiteList;
int v2 = 2; // const/4 v2, 0x2
/* invoke-direct {p0, v1, p1, p2, v2}, Lcom/miui/server/smartpower/SmartPowerPolicyManager;->addStaticWhiteListLocked(Landroid/util/ArrayMap;Ljava/lang/String;II)V */
/* .line 1186 */
/* monitor-exit v0 */
/* .line 1187 */
return;
/* .line 1186 */
/* :catchall_0 */
/* move-exception v1 */
/* monitor-exit v0 */
/* :try_end_0 */
/* .catchall {:try_start_0 ..:try_end_0} :catchall_0 */
/* throw v1 */
} // .end method
private void addProviderPackageWhiteList ( java.lang.String p0 ) {
/* .locals 4 */
/* .param p1, "pkgName" # Ljava/lang/String; */
/* .line 1214 */
v0 = this.mPackageWhiteList;
/* monitor-enter v0 */
/* .line 1215 */
try { // :try_start_0
v1 = this.mPackageWhiteList;
/* const/high16 v2, 0x200000 */
/* const/16 v3, 0x20 */
/* invoke-direct {p0, v1, p1, v2, v3}, Lcom/miui/server/smartpower/SmartPowerPolicyManager;->addStaticWhiteListLocked(Landroid/util/ArrayMap;Ljava/lang/String;II)V */
/* .line 1217 */
/* monitor-exit v0 */
/* .line 1218 */
return;
/* .line 1217 */
/* :catchall_0 */
/* move-exception v1 */
/* monitor-exit v0 */
/* :try_end_0 */
/* .catchall {:try_start_0 ..:try_end_0} :catchall_0 */
/* throw v1 */
} // .end method
private void addProviderPkgBlackList ( java.lang.String p0 ) {
/* .locals 2 */
/* .param p1, "pkgName" # Ljava/lang/String; */
/* .line 1312 */
v0 = this.mProviderPkgBlackList;
/* monitor-enter v0 */
/* .line 1313 */
try { // :try_start_0
v1 = this.mProviderPkgBlackList;
(( android.util.ArraySet ) v1 ).add ( p1 ); // invoke-virtual {v1, p1}, Landroid/util/ArraySet;->add(Ljava/lang/Object;)Z
/* .line 1314 */
/* monitor-exit v0 */
/* .line 1315 */
return;
/* .line 1314 */
/* :catchall_0 */
/* move-exception v1 */
/* monitor-exit v0 */
/* :try_end_0 */
/* .catchall {:try_start_0 ..:try_end_0} :catchall_0 */
/* throw v1 */
} // .end method
private void addServicePackageWhiteList ( java.lang.String p0 ) {
/* .locals 4 */
/* .param p1, "pkgName" # Ljava/lang/String; */
/* .line 1197 */
v0 = this.mPackageWhiteList;
/* monitor-enter v0 */
/* .line 1198 */
try { // :try_start_0
v1 = this.mPackageWhiteList;
/* const/high16 v2, 0x40000 */
/* const/16 v3, 0x10 */
/* invoke-direct {p0, v1, p1, v2, v3}, Lcom/miui/server/smartpower/SmartPowerPolicyManager;->addStaticWhiteListLocked(Landroid/util/ArrayMap;Ljava/lang/String;II)V */
/* .line 1200 */
/* monitor-exit v0 */
/* .line 1201 */
return;
/* .line 1200 */
/* :catchall_0 */
/* move-exception v1 */
/* monitor-exit v0 */
/* :try_end_0 */
/* .catchall {:try_start_0 ..:try_end_0} :catchall_0 */
/* throw v1 */
} // .end method
private void addServicePkgBlackList ( java.lang.String p0 ) {
/* .locals 2 */
/* .param p1, "pkgName" # Ljava/lang/String; */
/* .line 1324 */
v0 = this.mServicePkgBlackList;
/* monitor-enter v0 */
/* .line 1325 */
try { // :try_start_0
v1 = this.mServicePkgBlackList;
(( android.util.ArraySet ) v1 ).add ( p1 ); // invoke-virtual {v1, p1}, Landroid/util/ArraySet;->add(Ljava/lang/Object;)Z
/* .line 1326 */
/* monitor-exit v0 */
/* .line 1327 */
return;
/* .line 1326 */
/* :catchall_0 */
/* move-exception v1 */
/* monitor-exit v0 */
/* :try_end_0 */
/* .catchall {:try_start_0 ..:try_end_0} :catchall_0 */
/* throw v1 */
} // .end method
private void addSreenonWhiteList ( java.lang.String p0, Integer p1 ) {
/* .locals 3 */
/* .param p1, "pkgName" # Ljava/lang/String; */
/* .param p2, "type" # I */
/* .line 1190 */
v0 = this.mPackageWhiteList;
/* monitor-enter v0 */
/* .line 1191 */
try { // :try_start_0
v1 = this.mPackageWhiteList;
int v2 = 4; // const/4 v2, 0x4
/* invoke-direct {p0, v1, p1, p2, v2}, Lcom/miui/server/smartpower/SmartPowerPolicyManager;->addStaticWhiteListLocked(Landroid/util/ArrayMap;Ljava/lang/String;II)V */
/* .line 1193 */
/* monitor-exit v0 */
/* .line 1194 */
return;
/* .line 1193 */
/* :catchall_0 */
/* move-exception v1 */
/* monitor-exit v0 */
/* :try_end_0 */
/* .catchall {:try_start_0 ..:try_end_0} :catchall_0 */
/* throw v1 */
} // .end method
private void addStaticWhiteListLocked ( android.util.ArrayMap p0, java.lang.String p1, Integer p2, Integer p3 ) {
/* .locals 2 */
/* .param p2, "name" # Ljava/lang/String; */
/* .param p3, "type" # I */
/* .param p4, "action" # I */
/* .annotation system Ldalvik/annotation/Signature; */
/* value = { */
/* "(", */
/* "Landroid/util/ArrayMap<", */
/* "Ljava/lang/String;", */
/* "Lcom/miui/server/smartpower/SmartPowerPolicyManager$WhiteListItem;", */
/* ">;", */
/* "Ljava/lang/String;", */
/* "II)V" */
/* } */
} // .end annotation
/* .line 1249 */
/* .local p1, "whiteList":Landroid/util/ArrayMap;, "Landroid/util/ArrayMap<Ljava/lang/String;Lcom/miui/server/smartpower/SmartPowerPolicyManager$WhiteListItem;>;" */
(( android.util.ArrayMap ) p1 ).get ( p2 ); // invoke-virtual {p1, p2}, Landroid/util/ArrayMap;->get(Ljava/lang/Object;)Ljava/lang/Object;
/* check-cast v0, Lcom/miui/server/smartpower/SmartPowerPolicyManager$WhiteListItem; */
/* .line 1250 */
/* .local v0, "item":Lcom/miui/server/smartpower/SmartPowerPolicyManager$WhiteListItem; */
/* if-nez v0, :cond_0 */
/* .line 1251 */
/* new-instance v1, Lcom/miui/server/smartpower/SmartPowerPolicyManager$WhiteListItem; */
/* invoke-direct {v1, p0, p2, p3, p4}, Lcom/miui/server/smartpower/SmartPowerPolicyManager$WhiteListItem;-><init>(Lcom/miui/server/smartpower/SmartPowerPolicyManager;Ljava/lang/String;II)V */
/* move-object v0, v1 */
/* .line 1252 */
(( android.util.ArrayMap ) p1 ).put ( p2, v0 ); // invoke-virtual {p1, p2, v0}, Landroid/util/ArrayMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;
/* .line 1254 */
} // :cond_0
v1 = com.miui.server.smartpower.SmartPowerPolicyManager$WhiteListItem .-$$Nest$fgetmTypes ( v0 );
/* or-int/2addr v1, p3 */
com.miui.server.smartpower.SmartPowerPolicyManager$WhiteListItem .-$$Nest$fputmTypes ( v0,v1 );
/* .line 1255 */
v1 = com.miui.server.smartpower.SmartPowerPolicyManager$WhiteListItem .-$$Nest$fgetmActions ( v0 );
/* or-int/2addr v1, p4 */
com.miui.server.smartpower.SmartPowerPolicyManager$WhiteListItem .-$$Nest$fputmActions ( v0,v1 );
/* .line 1257 */
} // :goto_0
return;
} // .end method
private void addUidWhiteList ( Integer p0, java.lang.String p1, Integer p2 ) {
/* .locals 7 */
/* .param p1, "uid" # I */
/* .param p2, "pkgName" # Ljava/lang/String; */
/* .param p3, "type" # I */
/* .line 1110 */
v0 = this.mUidWhiteList;
/* monitor-enter v0 */
/* .line 1111 */
try { // :try_start_0
v2 = this.mUidWhiteList;
/* .line 1112 */
java.lang.Integer .valueOf ( p1 );
int v6 = 2; // const/4 v6, 0x2
/* .line 1111 */
/* move-object v1, p0 */
/* move-object v4, p2 */
/* move v5, p3 */
/* invoke-direct/range {v1 ..v6}, Lcom/miui/server/smartpower/SmartPowerPolicyManager;->addDynamicWhiteListLocked(Landroid/util/ArrayMap;Ljava/lang/Integer;Ljava/lang/String;II)Lcom/miui/server/smartpower/SmartPowerPolicyManager$WhiteListItem; */
/* .line 1113 */
/* .local v1, "item":Lcom/miui/server/smartpower/SmartPowerPolicyManager$WhiteListItem; */
/* monitor-exit v0 */
/* :try_end_0 */
/* .catchall {:try_start_0 ..:try_end_0} :catchall_0 */
/* .line 1114 */
if ( v1 != null) { // if-eqz v1, :cond_0
/* .line 1115 */
com.miui.server.smartpower.SmartPowerPolicyManager$WhiteListItem .-$$Nest$fputmUid ( v1,p1 );
/* .line 1116 */
v0 = this.mHandler;
/* new-instance v2, Lcom/miui/server/smartpower/SmartPowerPolicyManager$3; */
/* invoke-direct {v2, p0, p1}, Lcom/miui/server/smartpower/SmartPowerPolicyManager$3;-><init>(Lcom/miui/server/smartpower/SmartPowerPolicyManager;I)V */
(( android.os.Handler ) v0 ).post ( v2 ); // invoke-virtual {v0, v2}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z
/* .line 1123 */
} // :cond_0
return;
/* .line 1113 */
} // .end local v1 # "item":Lcom/miui/server/smartpower/SmartPowerPolicyManager$WhiteListItem;
/* :catchall_0 */
/* move-exception v1 */
try { // :try_start_1
/* monitor-exit v0 */
/* :try_end_1 */
/* .catchall {:try_start_1 ..:try_end_1} :catchall_0 */
/* throw v1 */
} // .end method
private void compactProcess ( java.lang.String p0, com.android.server.am.AppStateManager$AppState$RunningProcess p1 ) {
/* .locals 2 */
/* .param p1, "packageName" # Ljava/lang/String; */
/* .param p2, "proc" # Lcom/android/server/am/AppStateManager$AppState$RunningProcess; */
/* .line 882 */
(( com.android.server.am.AppStateManager$AppState$RunningProcess ) p2 ).getProcessRecord ( ); // invoke-virtual {p2}, Lcom/android/server/am/AppStateManager$AppState$RunningProcess;->getProcessRecord()Lcom/android/server/am/ProcessRecord;
v0 = com.android.server.am.AppStateManager .isSystemApp ( v0 );
if ( v0 != null) { // if-eqz v0, :cond_0
/* .line 883 */
return;
/* .line 885 */
} // :cond_0
v0 = (( com.android.server.am.AppStateManager$AppState$RunningProcess ) p2 ).getAdj ( ); // invoke-virtual {p2}, Lcom/android/server/am/AppStateManager$AppState$RunningProcess;->getAdj()I
/* const/16 v1, 0x2bc */
/* if-gt v0, v1, :cond_1 */
/* .line 886 */
com.android.server.am.SystemPressureControllerStub .getInstance ( );
int v1 = 1; // const/4 v1, 0x1
(( com.android.server.am.SystemPressureControllerStub ) v0 ).compactBackgroundProcess ( p2, v1 ); // invoke-virtual {v0, p2, v1}, Lcom/android/server/am/SystemPressureControllerStub;->compactBackgroundProcess(Lcom/miui/server/smartpower/IAppState$IRunningProcess;Z)V
/* .line 888 */
} // :cond_1
com.android.server.am.SystemPressureControllerStub .getInstance ( );
int v1 = 0; // const/4 v1, 0x0
(( com.android.server.am.SystemPressureControllerStub ) v0 ).compactBackgroundProcess ( p2, v1 ); // invoke-virtual {v0, p2, v1}, Lcom/android/server/am/SystemPressureControllerStub;->compactBackgroundProcess(Lcom/miui/server/smartpower/IAppState$IRunningProcess;Z)V
/* .line 890 */
} // :goto_0
return;
} // .end method
private void diedProcess ( com.android.server.am.AppStateManager$AppState$RunningProcess p0 ) {
/* .locals 2 */
/* .param p1, "proc" # Lcom/android/server/am/AppStateManager$AppState$RunningProcess; */
/* .line 763 */
v0 = (( com.android.server.am.AppStateManager$AppState$RunningProcess ) p1 ).is64BitProcess ( ); // invoke-virtual {p1}, Lcom/android/server/am/AppStateManager$AppState$RunningProcess;->is64BitProcess()Z
/* if-nez v0, :cond_1 */
/* .line 764 */
/* iget v0, p0, Lcom/miui/server/smartpower/SmartPowerPolicyManager;->mRunning32ProcCount:I */
/* if-gtz v0, :cond_0 */
int v0 = 0; // const/4 v0, 0x0
} // :cond_0
/* add-int/lit8 v0, v0, -0x1 */
} // :goto_0
/* iput v0, p0, Lcom/miui/server/smartpower/SmartPowerPolicyManager;->mRunning32ProcCount:I */
/* .line 765 */
/* if-gtz v0, :cond_1 */
/* .line 766 */
/* const-string/jumbo v0, "sys.mi_zygote32.start" */
final String v1 = "false"; // const-string v1, "false"
android.os.SystemProperties .set ( v0,v1 );
/* .line 769 */
} // :cond_1
return;
} // .end method
private Boolean forzenProcess ( Integer p0, Integer p1, java.lang.String p2, java.lang.String p3 ) {
/* .locals 2 */
/* .param p1, "uid" # I */
/* .param p2, "pid" # I */
/* .param p3, "processName" # Ljava/lang/String; */
/* .param p4, "reason" # Ljava/lang/String; */
/* .line 901 */
int v0 = 0; // const/4 v0, 0x0
/* .line 902 */
/* .local v0, "done":Z */
/* if-lez p2, :cond_0 */
v1 = this.mPowerFrozenManager;
v1 = (( com.miui.server.smartpower.PowerFrozenManager ) v1 ).isAllFrozenPid ( p2 ); // invoke-virtual {v1, p2}, Lcom/miui/server/smartpower/PowerFrozenManager;->isAllFrozenPid(I)Z
/* if-nez v1, :cond_0 */
/* .line 903 */
v1 = this.mPowerFrozenManager;
v0 = (( com.miui.server.smartpower.PowerFrozenManager ) v1 ).frozenProcess ( p1, p2, p3, p4 ); // invoke-virtual {v1, p1, p2, p3, p4}, Lcom/miui/server/smartpower/PowerFrozenManager;->frozenProcess(IILjava/lang/String;Ljava/lang/String;)Z
/* .line 905 */
} // :cond_0
} // .end method
private static Integer getWhiteListTypeMask ( Integer p0 ) {
/* .locals 1 */
/* .param p0, "action" # I */
/* .line 1391 */
/* sparse-switch p0, :sswitch_data_0 */
/* .line 1398 */
int v0 = 0; // const/4 v0, 0x0
/* .line 1396 */
/* :sswitch_0 */
/* const/high16 v0, 0x700000 */
/* .line 1395 */
/* :sswitch_1 */
/* const/high16 v0, 0xe0000 */
/* .line 1394 */
/* :sswitch_2 */
/* const v0, 0x1c000 */
/* .line 1393 */
/* :sswitch_3 */
/* const/16 v0, 0x3800 */
/* .line 1392 */
/* :sswitch_4 */
/* const/16 v0, 0x7fe */
/* nop */
/* :sswitch_data_0 */
/* .sparse-switch */
/* 0x2 -> :sswitch_4 */
/* 0x4 -> :sswitch_3 */
/* 0x8 -> :sswitch_2 */
/* 0x10 -> :sswitch_1 */
/* 0x20 -> :sswitch_0 */
} // .end sparse-switch
} // .end method
private Boolean hasPidWhiteList ( Integer p0 ) {
/* .locals 4 */
/* .param p1, "uid" # I */
/* .line 1010 */
v0 = this.mPidWhiteList;
/* monitor-enter v0 */
/* .line 1011 */
try { // :try_start_0
v1 = this.mPidWhiteList;
(( android.util.ArrayMap ) v1 ).values ( ); // invoke-virtual {v1}, Landroid/util/ArrayMap;->values()Ljava/util/Collection;
v2 = } // :goto_0
if ( v2 != null) { // if-eqz v2, :cond_1
/* check-cast v2, Lcom/miui/server/smartpower/SmartPowerPolicyManager$WhiteListItem; */
/* .line 1012 */
/* .local v2, "item":Lcom/miui/server/smartpower/SmartPowerPolicyManager$WhiteListItem; */
v3 = com.miui.server.smartpower.SmartPowerPolicyManager$WhiteListItem .-$$Nest$fgetmUid ( v2 );
/* if-ne v3, p1, :cond_0 */
int v3 = 2; // const/4 v3, 0x2
v3 = (( com.miui.server.smartpower.SmartPowerPolicyManager$WhiteListItem ) v2 ).hasAction ( v3 ); // invoke-virtual {v2, v3}, Lcom/miui/server/smartpower/SmartPowerPolicyManager$WhiteListItem;->hasAction(I)Z
if ( v3 != null) { // if-eqz v3, :cond_0
/* .line 1013 */
/* monitor-exit v0 */
int v0 = 1; // const/4 v0, 0x1
/* .line 1015 */
} // .end local v2 # "item":Lcom/miui/server/smartpower/SmartPowerPolicyManager$WhiteListItem;
} // :cond_0
/* .line 1016 */
} // :cond_1
/* monitor-exit v0 */
/* .line 1017 */
int v0 = 0; // const/4 v0, 0x0
/* .line 1016 */
/* :catchall_0 */
/* move-exception v1 */
/* monitor-exit v0 */
/* :try_end_0 */
/* .catchall {:try_start_0 ..:try_end_0} :catchall_0 */
/* throw v1 */
} // .end method
private void hibernationApp ( com.android.server.am.AppStateManager$AppState p0, java.lang.String p1 ) {
/* .locals 4 */
/* .param p1, "app" # Lcom/android/server/am/AppStateManager$AppState; */
/* .param p2, "reason" # Ljava/lang/String; */
/* .line 833 */
v0 = /* invoke-direct {p0}, Lcom/miui/server/smartpower/SmartPowerPolicyManager;->isEnableFrozen()Z */
if ( v0 != null) { // if-eqz v0, :cond_0
/* .line 834 */
final String v0 = "hibernationApp"; // const-string v0, "hibernationApp"
/* const-wide/32 v1, 0x20000 */
android.os.Trace .traceBegin ( v1,v2,v0 );
/* .line 835 */
/* new-instance v0, Ljava/lang/StringBuilder; */
/* invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V */
final String v3 = "power hibernation "; // const-string v3, "power hibernation "
(( java.lang.StringBuilder ) v0 ).append ( v3 ); // invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
/* .line 836 */
(( com.android.server.am.AppStateManager$AppState ) p1 ).getPackageName ( ); // invoke-virtual {p1}, Lcom/android/server/am/AppStateManager$AppState;->getPackageName()Ljava/lang/String;
(( java.lang.StringBuilder ) v0 ).append ( v3 ); // invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
final String v3 = "/"; // const-string v3, "/"
(( java.lang.StringBuilder ) v0 ).append ( v3 ); // invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
v3 = (( com.android.server.am.AppStateManager$AppState ) p1 ).getUid ( ); // invoke-virtual {p1}, Lcom/android/server/am/AppStateManager$AppState;->getUid()I
(( java.lang.StringBuilder ) v0 ).append ( v3 ); // invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v0 ).toString ( ); // invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
/* .line 835 */
/* const v3, 0x15ff2 */
android.util.EventLog .writeEvent ( v3,v0 );
/* .line 837 */
v0 = this.mAppPowerResourceManager;
v3 = (( com.android.server.am.AppStateManager$AppState ) p1 ).getUid ( ); // invoke-virtual {p1}, Lcom/android/server/am/AppStateManager$AppState;->getUid()I
(( com.miui.server.smartpower.AppPowerResourceManager ) v0 ).releaseAppAllPowerResources ( v3 ); // invoke-virtual {v0, v3}, Lcom/miui/server/smartpower/AppPowerResourceManager;->releaseAppAllPowerResources(I)V
/* .line 838 */
android.os.Trace .traceEnd ( v1,v2 );
/* .line 840 */
} // :cond_0
return;
} // .end method
private void hibernationProcess ( com.android.server.am.AppStateManager$AppState$RunningProcess p0, java.lang.String p1 ) {
/* .locals 2 */
/* .param p1, "proc" # Lcom/android/server/am/AppStateManager$AppState$RunningProcess; */
/* .param p2, "reason" # Ljava/lang/String; */
/* .line 843 */
(( com.android.server.am.AppStateManager$AppState$RunningProcess ) p1 ).getProcessRecord ( ); // invoke-virtual {p1}, Lcom/android/server/am/AppStateManager$AppState$RunningProcess;->getProcessRecord()Lcom/android/server/am/ProcessRecord;
/* if-nez v0, :cond_0 */
/* .line 844 */
return;
/* .line 846 */
} // :cond_0
v0 = this.mHandler;
/* new-instance v1, Lcom/miui/server/smartpower/SmartPowerPolicyManager$2; */
/* invoke-direct {v1, p0, p1, p2}, Lcom/miui/server/smartpower/SmartPowerPolicyManager$2;-><init>(Lcom/miui/server/smartpower/SmartPowerPolicyManager;Lcom/android/server/am/AppStateManager$AppState$RunningProcess;Ljava/lang/String;)V */
(( android.os.Handler ) v0 ).post ( v1 ); // invoke-virtual {v0, v1}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z
/* .line 861 */
return;
} // .end method
private void idleApp ( com.android.server.am.AppStateManager$AppState p0 ) {
/* .locals 4 */
/* .param p1, "app" # Lcom/android/server/am/AppStateManager$AppState; */
/* .line 806 */
v0 = /* invoke-direct {p0}, Lcom/miui/server/smartpower/SmartPowerPolicyManager;->isEnableIntercept()Z */
if ( v0 != null) { // if-eqz v0, :cond_1
/* .line 807 */
(( com.android.server.am.AppStateManager$AppState ) p1 ).getPackageName ( ); // invoke-virtual {p1}, Lcom/android/server/am/AppStateManager$AppState;->getPackageName()Ljava/lang/String;
v0 = com.miui.server.process.ProcessManagerInternal .checkCtsProcess ( v0 );
/* if-nez v0, :cond_1 */
/* .line 808 */
/* new-instance v0, Lcom/miui/server/smartpower/SmartPowerPolicyManager$$ExternalSyntheticLambda0; */
/* invoke-direct {v0, p0, p1}, Lcom/miui/server/smartpower/SmartPowerPolicyManager$$ExternalSyntheticLambda0;-><init>(Lcom/miui/server/smartpower/SmartPowerPolicyManager;Lcom/android/server/am/AppStateManager$AppState;)V */
/* .line 819 */
/* .local v0, "idleRunnable":Ljava/lang/Runnable; */
v1 = (( com.android.server.am.AppStateManager$AppState ) p1 ).getMinAmsProcState ( ); // invoke-virtual {p1}, Lcom/android/server/am/AppStateManager$AppState;->getMinAmsProcState()I
int v2 = 6; // const/4 v2, 0x6
/* if-le v1, v2, :cond_0 */
/* .line 820 */
v1 = this.mHandler;
(( android.os.Handler ) v1 ).post ( v0 ); // invoke-virtual {v1, v0}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z
/* .line 822 */
} // :cond_0
v1 = this.mHandler;
/* const-wide/16 v2, 0x4e20 */
(( android.os.Handler ) v1 ).postDelayed ( v0, v2, v3 ); // invoke-virtual {v1, v0, v2, v3}, Landroid/os/Handler;->postDelayed(Ljava/lang/Runnable;J)Z
/* .line 825 */
} // .end local v0 # "idleRunnable":Ljava/lang/Runnable;
} // :cond_1
} // :goto_0
return;
} // .end method
private void idleProcess ( com.android.server.am.AppStateManager$AppState$RunningProcess p0 ) {
/* .locals 0 */
/* .param p1, "proc" # Lcom/android/server/am/AppStateManager$AppState$RunningProcess; */
/* .line 803 */
return;
} // .end method
private Boolean isBroadcastProcessWhiteList ( java.lang.String p0, java.lang.String p1 ) {
/* .locals 3 */
/* .param p1, "processName" # Ljava/lang/String; */
/* .param p2, "action" # Ljava/lang/String; */
/* .line 1054 */
v0 = this.mBroadcastProcessActionWhiteList;
/* monitor-enter v0 */
/* .line 1055 */
try { // :try_start_0
v1 = this.mBroadcastProcessActionWhiteList;
/* new-instance v2, Ljava/lang/StringBuilder; */
/* invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V */
(( java.lang.StringBuilder ) v2 ).append ( p1 ); // invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v2 ).append ( p2 ); // invoke-virtual {v2, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v2 ).toString ( ); // invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
v1 = (( android.util.ArraySet ) v1 ).contains ( v2 ); // invoke-virtual {v1, v2}, Landroid/util/ArraySet;->contains(Ljava/lang/Object;)Z
/* monitor-exit v0 */
/* .line 1056 */
/* :catchall_0 */
/* move-exception v1 */
/* monitor-exit v0 */
/* :try_end_0 */
/* .catchall {:try_start_0 ..:try_end_0} :catchall_0 */
/* throw v1 */
} // .end method
private Boolean isDeviceIdleWhiteList ( Integer p0 ) {
/* .locals 2 */
/* .param p1, "uid" # I */
/* .line 828 */
v0 = this.mLocalDeviceIdleController;
if ( v0 != null) { // if-eqz v0, :cond_0
/* .line 829 */
v0 = v1 = android.os.UserHandle .getAppId ( p1 );
if ( v0 != null) { // if-eqz v0, :cond_0
int v0 = 1; // const/4 v0, 0x1
} // :cond_0
int v0 = 0; // const/4 v0, 0x0
/* .line 828 */
} // :goto_0
} // .end method
private Boolean isEnableFrozen ( ) {
/* .locals 1 */
/* .line 939 */
v0 = com.miui.server.smartpower.PowerFrozenManager .isEnable ( );
} // .end method
private Boolean isEnableIntercept ( ) {
/* .locals 1 */
/* .line 943 */
/* sget-boolean v0, Lcom/miui/app/smartpower/SmartPowerPolicyConstants;->TESTSUITSPECIFIC:Z */
/* if-nez v0, :cond_1 */
/* sget-boolean v0, Lcom/miui/app/smartpower/SmartPowerSettings;->PROP_INTERCEPT_ENABLE:Z */
/* if-nez v0, :cond_0 */
/* .line 944 */
v0 = com.miui.server.smartpower.SmartPowerPolicyManager .isSpeedTestMode ( );
/* if-nez v0, :cond_0 */
v0 = /* invoke-direct {p0}, Lcom/miui/server/smartpower/SmartPowerPolicyManager;->isEnableFrozen()Z */
if ( v0 != null) { // if-eqz v0, :cond_1
} // :cond_0
int v0 = 1; // const/4 v0, 0x1
} // :cond_1
int v0 = 0; // const/4 v0, 0x0
/* .line 943 */
} // :goto_0
} // .end method
public static Boolean isPackageInterceptList ( java.lang.String p0 ) {
/* .locals 2 */
/* .param p0, "pkgName" # Ljava/lang/String; */
/* .line 1021 */
v0 = com.miui.server.smartpower.SmartPowerPolicyManager.sInterceptPackageList;
/* monitor-enter v0 */
/* .line 1022 */
try { // :try_start_0
v1 = (( android.util.ArraySet ) v0 ).contains ( p0 ); // invoke-virtual {v0, p0}, Landroid/util/ArraySet;->contains(Ljava/lang/Object;)Z
/* monitor-exit v0 */
/* .line 1023 */
/* :catchall_0 */
/* move-exception v1 */
/* monitor-exit v0 */
/* :try_end_0 */
/* .catchall {:try_start_0 ..:try_end_0} :catchall_0 */
/* throw v1 */
} // .end method
private Boolean isPackageWhiteList ( java.lang.String p0 ) {
/* .locals 3 */
/* .param p1, "packageName" # Ljava/lang/String; */
/* .line 948 */
v0 = this.mPackageWhiteList;
/* monitor-enter v0 */
/* .line 949 */
try { // :try_start_0
v1 = this.mPackageWhiteList;
(( android.util.ArrayMap ) v1 ).get ( p1 ); // invoke-virtual {v1, p1}, Landroid/util/ArrayMap;->get(Ljava/lang/Object;)Ljava/lang/Object;
/* check-cast v1, Lcom/miui/server/smartpower/SmartPowerPolicyManager$WhiteListItem; */
/* .line 950 */
/* .local v1, "item":Lcom/miui/server/smartpower/SmartPowerPolicyManager$WhiteListItem; */
if ( v1 != null) { // if-eqz v1, :cond_1
/* .line 951 */
int v2 = 2; // const/4 v2, 0x2
v2 = (( com.miui.server.smartpower.SmartPowerPolicyManager$WhiteListItem ) v1 ).hasAction ( v2 ); // invoke-virtual {v1, v2}, Lcom/miui/server/smartpower/SmartPowerPolicyManager$WhiteListItem;->hasAction(I)Z
/* if-nez v2, :cond_0 */
/* iget-boolean v2, p0, Lcom/miui/server/smartpower/SmartPowerPolicyManager;->mScreenOff:Z */
/* if-nez v2, :cond_1 */
/* .line 952 */
int v2 = 4; // const/4 v2, 0x4
v2 = (( com.miui.server.smartpower.SmartPowerPolicyManager$WhiteListItem ) v1 ).hasAction ( v2 ); // invoke-virtual {v1, v2}, Lcom/miui/server/smartpower/SmartPowerPolicyManager$WhiteListItem;->hasAction(I)Z
if ( v2 != null) { // if-eqz v2, :cond_1
/* .line 953 */
} // :cond_0
/* monitor-exit v0 */
int v0 = 1; // const/4 v0, 0x1
/* .line 955 */
} // .end local v1 # "item":Lcom/miui/server/smartpower/SmartPowerPolicyManager$WhiteListItem;
} // :cond_1
/* monitor-exit v0 */
/* .line 956 */
int v0 = 0; // const/4 v0, 0x0
/* .line 955 */
/* :catchall_0 */
/* move-exception v1 */
/* monitor-exit v0 */
/* :try_end_0 */
/* .catchall {:try_start_0 ..:try_end_0} :catchall_0 */
/* throw v1 */
} // .end method
private Boolean isPidWhiteList ( Integer p0 ) {
/* .locals 3 */
/* .param p1, "pid" # I */
/* .line 1000 */
v0 = this.mPidWhiteList;
/* monitor-enter v0 */
/* .line 1001 */
try { // :try_start_0
v1 = this.mPidWhiteList;
java.lang.Integer .valueOf ( p1 );
(( android.util.ArrayMap ) v1 ).get ( v2 ); // invoke-virtual {v1, v2}, Landroid/util/ArrayMap;->get(Ljava/lang/Object;)Ljava/lang/Object;
/* check-cast v1, Lcom/miui/server/smartpower/SmartPowerPolicyManager$WhiteListItem; */
/* .line 1002 */
/* .local v1, "item":Lcom/miui/server/smartpower/SmartPowerPolicyManager$WhiteListItem; */
if ( v1 != null) { // if-eqz v1, :cond_0
int v2 = 2; // const/4 v2, 0x2
v2 = (( com.miui.server.smartpower.SmartPowerPolicyManager$WhiteListItem ) v1 ).hasAction ( v2 ); // invoke-virtual {v1, v2}, Lcom/miui/server/smartpower/SmartPowerPolicyManager$WhiteListItem;->hasAction(I)Z
if ( v2 != null) { // if-eqz v2, :cond_0
/* .line 1003 */
/* monitor-exit v0 */
int v0 = 1; // const/4 v0, 0x1
/* .line 1005 */
} // .end local v1 # "item":Lcom/miui/server/smartpower/SmartPowerPolicyManager$WhiteListItem;
} // :cond_0
/* monitor-exit v0 */
/* .line 1006 */
int v0 = 0; // const/4 v0, 0x0
/* .line 1005 */
/* :catchall_0 */
/* move-exception v1 */
/* monitor-exit v0 */
/* :try_end_0 */
/* .catchall {:try_start_0 ..:try_end_0} :catchall_0 */
/* throw v1 */
} // .end method
private Boolean isProcessWhiteList ( java.lang.String p0 ) {
/* .locals 3 */
/* .param p1, "procName" # Ljava/lang/String; */
/* .line 990 */
v0 = this.mProcessWhiteList;
/* monitor-enter v0 */
/* .line 991 */
try { // :try_start_0
v1 = this.mProcessWhiteList;
(( android.util.ArrayMap ) v1 ).get ( p1 ); // invoke-virtual {v1, p1}, Landroid/util/ArrayMap;->get(Ljava/lang/Object;)Ljava/lang/Object;
/* check-cast v1, Lcom/miui/server/smartpower/SmartPowerPolicyManager$WhiteListItem; */
/* .line 992 */
/* .local v1, "item":Lcom/miui/server/smartpower/SmartPowerPolicyManager$WhiteListItem; */
if ( v1 != null) { // if-eqz v1, :cond_0
int v2 = 2; // const/4 v2, 0x2
v2 = (( com.miui.server.smartpower.SmartPowerPolicyManager$WhiteListItem ) v1 ).hasAction ( v2 ); // invoke-virtual {v1, v2}, Lcom/miui/server/smartpower/SmartPowerPolicyManager$WhiteListItem;->hasAction(I)Z
if ( v2 != null) { // if-eqz v2, :cond_0
/* .line 993 */
/* monitor-exit v0 */
int v0 = 1; // const/4 v0, 0x1
/* .line 995 */
} // .end local v1 # "item":Lcom/miui/server/smartpower/SmartPowerPolicyManager$WhiteListItem;
} // :cond_0
/* monitor-exit v0 */
/* .line 996 */
int v0 = 0; // const/4 v0, 0x0
/* .line 995 */
/* :catchall_0 */
/* move-exception v1 */
/* monitor-exit v0 */
/* :try_end_0 */
/* .catchall {:try_start_0 ..:try_end_0} :catchall_0 */
/* throw v1 */
} // .end method
public static Boolean isSpeedTestMode ( ) {
/* .locals 1 */
/* .line 679 */
/* const-class v0, Lcom/miui/app/SpeedTestModeServiceInternal; */
com.android.server.LocalServices .getService ( v0 );
v0 = /* check-cast v0, Lcom/miui/app/SpeedTestModeServiceInternal; */
} // .end method
private Boolean isUidWhiteList ( Integer p0 ) {
/* .locals 3 */
/* .param p1, "uid" # I */
/* .line 960 */
v0 = this.mUidWhiteList;
/* monitor-enter v0 */
/* .line 961 */
try { // :try_start_0
v1 = this.mUidWhiteList;
java.lang.Integer .valueOf ( p1 );
(( android.util.ArrayMap ) v1 ).get ( v2 ); // invoke-virtual {v1, v2}, Landroid/util/ArrayMap;->get(Ljava/lang/Object;)Ljava/lang/Object;
/* check-cast v1, Lcom/miui/server/smartpower/SmartPowerPolicyManager$WhiteListItem; */
/* .line 962 */
/* .local v1, "item":Lcom/miui/server/smartpower/SmartPowerPolicyManager$WhiteListItem; */
if ( v1 != null) { // if-eqz v1, :cond_0
int v2 = 2; // const/4 v2, 0x2
v2 = (( com.miui.server.smartpower.SmartPowerPolicyManager$WhiteListItem ) v1 ).hasAction ( v2 ); // invoke-virtual {v1, v2}, Lcom/miui/server/smartpower/SmartPowerPolicyManager$WhiteListItem;->hasAction(I)Z
if ( v2 != null) { // if-eqz v2, :cond_0
/* .line 963 */
/* monitor-exit v0 */
int v0 = 1; // const/4 v0, 0x1
/* .line 965 */
} // .end local v1 # "item":Lcom/miui/server/smartpower/SmartPowerPolicyManager$WhiteListItem;
} // :cond_0
/* monitor-exit v0 */
/* .line 966 */
int v0 = 0; // const/4 v0, 0x0
/* .line 965 */
/* :catchall_0 */
/* move-exception v1 */
/* monitor-exit v0 */
/* :try_end_0 */
/* .catchall {:try_start_0 ..:try_end_0} :catchall_0 */
/* throw v1 */
} // .end method
private void lambda$idleApp$0 ( com.android.server.am.AppStateManager$AppState p0 ) { //synthethic
/* .locals 6 */
/* .param p1, "app" # Lcom/android/server/am/AppStateManager$AppState; */
/* .line 809 */
/* new-instance v0, Ljava/lang/StringBuilder; */
/* invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V */
final String v1 = "power idle "; // const-string v1, "power idle "
(( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( com.android.server.am.AppStateManager$AppState ) p1 ).getPackageName ( ); // invoke-virtual {p1}, Lcom/android/server/am/AppStateManager$AppState;->getPackageName()Ljava/lang/String;
(( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
final String v1 = "/"; // const-string v1, "/"
(( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
v1 = (( com.android.server.am.AppStateManager$AppState ) p1 ).getUid ( ); // invoke-virtual {p1}, Lcom/android/server/am/AppStateManager$AppState;->getUid()I
(( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v0 ).toString ( ); // invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
/* .line 810 */
/* .local v0, "event":Ljava/lang/String; */
/* const-wide/32 v1, 0x20000 */
android.os.Trace .traceBegin ( v1,v2,v0 );
/* .line 811 */
v3 = (( com.android.server.am.AppStateManager$AppState ) p1 ).isAlive ( ); // invoke-virtual {p1}, Lcom/android/server/am/AppStateManager$AppState;->isAlive()Z
if ( v3 != null) { // if-eqz v3, :cond_0
v3 = (( com.android.server.am.AppStateManager$AppState ) p1 ).isIdle ( ); // invoke-virtual {p1}, Lcom/android/server/am/AppStateManager$AppState;->isIdle()Z
if ( v3 != null) { // if-eqz v3, :cond_0
v3 = (( com.android.server.am.AppStateManager$AppState ) p1 ).getUid ( ); // invoke-virtual {p1}, Lcom/android/server/am/AppStateManager$AppState;->getUid()I
v3 = /* invoke-direct {p0, v3}, Lcom/miui/server/smartpower/SmartPowerPolicyManager;->isDeviceIdleWhiteList(I)Z */
/* if-nez v3, :cond_0 */
/* .line 813 */
try { // :try_start_0
v3 = this.mAMS;
(( com.android.server.am.AppStateManager$AppState ) p1 ).getPackageName ( ); // invoke-virtual {p1}, Lcom/android/server/am/AppStateManager$AppState;->getPackageName()Ljava/lang/String;
int v5 = -2; // const/4 v5, -0x2
(( com.android.server.am.ActivityManagerService ) v3 ).makePackageIdle ( v4, v5 ); // invoke-virtual {v3, v4, v5}, Lcom/android/server/am/ActivityManagerService;->makePackageIdle(Ljava/lang/String;I)V
/* .line 814 */
/* const v3, 0x15ff2 */
android.util.EventLog .writeEvent ( v3,v0 );
/* :try_end_0 */
/* .catch Ljava/lang/Exception; {:try_start_0 ..:try_end_0} :catch_0 */
/* .line 815 */
/* :catch_0 */
/* move-exception v3 */
} // :goto_0
/* nop */
/* .line 817 */
} // :cond_0
android.os.Trace .traceEnd ( v1,v2 );
/* .line 818 */
return;
} // .end method
private void lunchProcess ( com.android.server.am.AppStateManager$AppState$RunningProcess p0 ) {
/* .locals 1 */
/* .param p1, "proc" # Lcom/android/server/am/AppStateManager$AppState$RunningProcess; */
/* .line 757 */
v0 = (( com.android.server.am.AppStateManager$AppState$RunningProcess ) p1 ).is64BitProcess ( ); // invoke-virtual {p1}, Lcom/android/server/am/AppStateManager$AppState$RunningProcess;->is64BitProcess()Z
/* if-nez v0, :cond_0 */
/* .line 758 */
/* iget v0, p0, Lcom/miui/server/smartpower/SmartPowerPolicyManager;->mRunning32ProcCount:I */
/* add-int/lit8 v0, v0, 0x1 */
/* iput v0, p0, Lcom/miui/server/smartpower/SmartPowerPolicyManager;->mRunning32ProcCount:I */
/* .line 760 */
} // :cond_0
return;
} // .end method
private java.util.List queryUsageStats ( ) {
/* .locals 12 */
/* .annotation system Ldalvik/annotation/Signature; */
/* value = { */
/* "()", */
/* "Ljava/util/List<", */
/* "Landroid/app/usage/UsageStats;", */
/* ">;" */
/* } */
} // .end annotation
/* .line 1102 */
java.lang.System .currentTimeMillis ( );
/* move-result-wide v8 */
/* .line 1103 */
/* .local v8, "endTime":J */
/* const-wide/32 v0, 0x240c8400 */
/* sub-long v10, v8, v0 */
/* .line 1104 */
/* .local v10, "beginTime":J */
v0 = this.mUsageStats;
v1 = android.os.UserHandle .getCallingUserId ( );
int v2 = 1; // const/4 v2, 0x1
int v7 = 0; // const/4 v7, 0x0
/* move-wide v3, v10 */
/* move-wide v5, v8 */
/* invoke-virtual/range {v0 ..v7}, Landroid/app/usage/UsageStatsManagerInternal;->queryUsageStatsForUser(IIJJZ)Ljava/util/List; */
} // .end method
private void registerTestSuitSpecificObserver ( ) {
/* .locals 5 */
/* .line 1513 */
/* new-instance v0, Lcom/miui/server/smartpower/SmartPowerPolicyManager$7; */
int v1 = 0; // const/4 v1, 0x0
/* invoke-direct {v0, p0, v1}, Lcom/miui/server/smartpower/SmartPowerPolicyManager$7;-><init>(Lcom/miui/server/smartpower/SmartPowerPolicyManager;Landroid/os/Handler;)V */
/* .line 1519 */
/* .local v0, "observer":Landroid/database/ContentObserver; */
v1 = this.mContext;
(( android.content.Context ) v1 ).getContentResolver ( ); // invoke-virtual {v1}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;
v2 = android.provider.MiuiSettings$Secure.MIUI_OPTIMIZATION;
/* .line 1520 */
android.provider.Settings$Secure .getUriFor ( v2 );
/* .line 1519 */
int v3 = 0; // const/4 v3, 0x0
int v4 = -2; // const/4 v4, -0x2
(( android.content.ContentResolver ) v1 ).registerContentObserver ( v2, v3, v0, v4 ); // invoke-virtual {v1, v2, v3, v0, v4}, Landroid/content/ContentResolver;->registerContentObserver(Landroid/net/Uri;ZLandroid/database/ContentObserver;I)V
/* .line 1522 */
(( android.database.ContentObserver ) v0 ).onChange ( v3 ); // invoke-virtual {v0, v3}, Landroid/database/ContentObserver;->onChange(Z)V
/* .line 1523 */
return;
} // .end method
private com.miui.server.smartpower.SmartPowerPolicyManager$WhiteListItem removeDynamicWhiteListLocked ( android.util.ArrayMap p0, java.lang.Integer p1, java.lang.String p2, Integer p3, Integer p4 ) {
/* .locals 3 */
/* .param p2, "key" # Ljava/lang/Integer; */
/* .param p3, "name" # Ljava/lang/String; */
/* .param p4, "type" # I */
/* .param p5, "action" # I */
/* .annotation system Ldalvik/annotation/Signature; */
/* value = { */
/* "(", */
/* "Landroid/util/ArrayMap<", */
/* "Ljava/lang/Integer;", */
/* "Lcom/miui/server/smartpower/SmartPowerPolicyManager$WhiteListItem;", */
/* ">;", */
/* "Ljava/lang/Integer;", */
/* "Ljava/lang/String;", */
/* "II)", */
/* "Lcom/miui/server/smartpower/SmartPowerPolicyManager$WhiteListItem;" */
/* } */
} // .end annotation
/* .line 1275 */
/* .local p1, "whiteList":Landroid/util/ArrayMap;, "Landroid/util/ArrayMap<Ljava/lang/Integer;Lcom/miui/server/smartpower/SmartPowerPolicyManager$WhiteListItem;>;" */
(( android.util.ArrayMap ) p1 ).get ( p2 ); // invoke-virtual {p1, p2}, Landroid/util/ArrayMap;->get(Ljava/lang/Object;)Ljava/lang/Object;
/* check-cast v0, Lcom/miui/server/smartpower/SmartPowerPolicyManager$WhiteListItem; */
/* .line 1276 */
/* .local v0, "item":Lcom/miui/server/smartpower/SmartPowerPolicyManager$WhiteListItem; */
if ( v0 != null) { // if-eqz v0, :cond_1
/* .line 1277 */
v1 = com.miui.server.smartpower.SmartPowerPolicyManager$WhiteListItem .-$$Nest$fgetmTypes ( v0 );
/* not-int v2, p4 */
/* and-int/2addr v1, v2 */
com.miui.server.smartpower.SmartPowerPolicyManager$WhiteListItem .-$$Nest$fputmTypes ( v0,v1 );
/* .line 1278 */
v1 = com.miui.server.smartpower.SmartPowerPolicyManager$WhiteListItem .-$$Nest$fgetmTypes ( v0 );
v2 = com.miui.server.smartpower.SmartPowerPolicyManager .getWhiteListTypeMask ( p5 );
/* and-int/2addr v1, v2 */
/* if-nez v1, :cond_0 */
/* .line 1279 */
v1 = com.miui.server.smartpower.SmartPowerPolicyManager$WhiteListItem .-$$Nest$fgetmActions ( v0 );
/* not-int v2, p5 */
/* and-int/2addr v1, v2 */
com.miui.server.smartpower.SmartPowerPolicyManager$WhiteListItem .-$$Nest$fputmActions ( v0,v1 );
/* .line 1281 */
} // :cond_0
v1 = com.miui.server.smartpower.SmartPowerPolicyManager$WhiteListItem .-$$Nest$fgetmTypes ( v0 );
/* if-nez v1, :cond_1 */
v1 = com.miui.server.smartpower.SmartPowerPolicyManager$WhiteListItem .-$$Nest$fgetmActions ( v0 );
/* if-nez v1, :cond_1 */
/* .line 1282 */
(( android.util.ArrayMap ) p1 ).remove ( p2 ); // invoke-virtual {p1, p2}, Landroid/util/ArrayMap;->remove(Ljava/lang/Object;)Ljava/lang/Object;
/* .line 1283 */
/* .line 1286 */
} // :cond_1
int v1 = 0; // const/4 v1, 0x0
} // .end method
private void removePidWhiteList ( Integer p0, Integer p1, java.lang.String p2, Integer p3 ) {
/* .locals 7 */
/* .param p1, "uid" # I */
/* .param p2, "pid" # I */
/* .param p3, "procName" # Ljava/lang/String; */
/* .param p4, "type" # I */
/* .line 1168 */
v0 = this.mPidWhiteList;
/* monitor-enter v0 */
/* .line 1169 */
try { // :try_start_0
v2 = this.mPidWhiteList;
java.lang.Integer .valueOf ( p2 );
int v6 = 2; // const/4 v6, 0x2
/* move-object v1, p0 */
/* move-object v4, p3 */
/* move v5, p4 */
/* invoke-direct/range {v1 ..v6}, Lcom/miui/server/smartpower/SmartPowerPolicyManager;->removeDynamicWhiteListLocked(Landroid/util/ArrayMap;Ljava/lang/Integer;Ljava/lang/String;II)Lcom/miui/server/smartpower/SmartPowerPolicyManager$WhiteListItem; */
/* .line 1171 */
/* .local v1, "item":Lcom/miui/server/smartpower/SmartPowerPolicyManager$WhiteListItem; */
/* monitor-exit v0 */
/* :try_end_0 */
/* .catchall {:try_start_0 ..:try_end_0} :catchall_0 */
/* .line 1172 */
if ( v1 != null) { // if-eqz v1, :cond_0
/* .line 1173 */
v0 = this.mHandler;
/* new-instance v2, Lcom/miui/server/smartpower/SmartPowerPolicyManager$6; */
/* invoke-direct {v2, p0, p1, p2}, Lcom/miui/server/smartpower/SmartPowerPolicyManager$6;-><init>(Lcom/miui/server/smartpower/SmartPowerPolicyManager;II)V */
(( android.os.Handler ) v0 ).post ( v2 ); // invoke-virtual {v0, v2}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z
/* .line 1180 */
} // :cond_0
return;
/* .line 1171 */
} // .end local v1 # "item":Lcom/miui/server/smartpower/SmartPowerPolicyManager$WhiteListItem;
/* :catchall_0 */
/* move-exception v1 */
try { // :try_start_1
/* monitor-exit v0 */
/* :try_end_1 */
/* .catchall {:try_start_1 ..:try_end_1} :catchall_0 */
/* throw v1 */
} // .end method
private void removeUidWhiteList ( Integer p0, java.lang.String p1, Integer p2 ) {
/* .locals 7 */
/* .param p1, "uid" # I */
/* .param p2, "pkgName" # Ljava/lang/String; */
/* .param p3, "type" # I */
/* .line 1127 */
v0 = this.mUidWhiteList;
/* monitor-enter v0 */
/* .line 1128 */
try { // :try_start_0
v2 = this.mUidWhiteList;
/* .line 1129 */
java.lang.Integer .valueOf ( p1 );
int v6 = 2; // const/4 v6, 0x2
/* .line 1128 */
/* move-object v1, p0 */
/* move-object v4, p2 */
/* move v5, p3 */
/* invoke-direct/range {v1 ..v6}, Lcom/miui/server/smartpower/SmartPowerPolicyManager;->removeDynamicWhiteListLocked(Landroid/util/ArrayMap;Ljava/lang/Integer;Ljava/lang/String;II)Lcom/miui/server/smartpower/SmartPowerPolicyManager$WhiteListItem; */
/* .line 1130 */
/* .local v1, "item":Lcom/miui/server/smartpower/SmartPowerPolicyManager$WhiteListItem; */
/* monitor-exit v0 */
/* :try_end_0 */
/* .catchall {:try_start_0 ..:try_end_0} :catchall_0 */
/* .line 1131 */
if ( v1 != null) { // if-eqz v1, :cond_0
/* .line 1132 */
v0 = this.mHandler;
/* new-instance v2, Lcom/miui/server/smartpower/SmartPowerPolicyManager$4; */
/* invoke-direct {v2, p0, p1}, Lcom/miui/server/smartpower/SmartPowerPolicyManager$4;-><init>(Lcom/miui/server/smartpower/SmartPowerPolicyManager;I)V */
(( android.os.Handler ) v0 ).post ( v2 ); // invoke-virtual {v0, v2}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z
/* .line 1139 */
} // :cond_0
return;
/* .line 1130 */
} // .end local v1 # "item":Lcom/miui/server/smartpower/SmartPowerPolicyManager$WhiteListItem;
/* :catchall_0 */
/* move-exception v1 */
try { // :try_start_1
/* monitor-exit v0 */
/* :try_end_1 */
/* .catchall {:try_start_1 ..:try_end_1} :catchall_0 */
/* throw v1 */
} // .end method
private Boolean thawProcess ( Integer p0, Integer p1, java.lang.String p2 ) {
/* .locals 2 */
/* .param p1, "uid" # I */
/* .param p2, "pid" # I */
/* .param p3, "reason" # Ljava/lang/String; */
/* .line 893 */
int v0 = 0; // const/4 v0, 0x0
/* .line 894 */
/* .local v0, "done":Z */
/* if-lez p2, :cond_0 */
v1 = this.mPowerFrozenManager;
v1 = (( com.miui.server.smartpower.PowerFrozenManager ) v1 ).isAllFrozenPid ( p2 ); // invoke-virtual {v1, p2}, Lcom/miui/server/smartpower/PowerFrozenManager;->isAllFrozenPid(I)Z
if ( v1 != null) { // if-eqz v1, :cond_0
/* .line 895 */
v1 = this.mPowerFrozenManager;
v0 = (( com.miui.server.smartpower.PowerFrozenManager ) v1 ).thawProcess ( p1, p2, p3 ); // invoke-virtual {v1, p1, p2, p3}, Lcom/miui/server/smartpower/PowerFrozenManager;->thawProcess(IILjava/lang/String;)Z
/* .line 897 */
} // :cond_0
} // .end method
private void updateUsageStats ( ) {
/* .locals 8 */
/* .line 1075 */
/* const-string/jumbo v0, "updateUsageStats" */
/* const-wide/32 v1, 0x20000 */
android.os.Trace .traceBegin ( v1,v2,v0 );
/* .line 1076 */
/* invoke-direct {p0}, Lcom/miui/server/smartpower/SmartPowerPolicyManager;->queryUsageStats()Ljava/util/List; */
/* .line 1077 */
/* .local v0, "usageStats":Ljava/util/List;, "Ljava/util/List<Landroid/app/usage/UsageStats;>;" */
v3 = if ( v0 != null) { // if-eqz v0, :cond_3
if ( v3 != null) { // if-eqz v3, :cond_0
/* .line 1081 */
} // :cond_0
/* new-instance v3, Ljava/util/HashMap; */
/* invoke-direct {v3}, Ljava/util/HashMap;-><init>()V */
/* .line 1082 */
/* .local v3, "usageStatsMap":Ljava/util/HashMap;, "Ljava/util/HashMap<Ljava/lang/String;Lcom/miui/server/smartpower/SmartPowerPolicyManager$UsageStatsInfo;>;" */
v5 = } // :goto_0
if ( v5 != null) { // if-eqz v5, :cond_2
/* check-cast v5, Landroid/app/usage/UsageStats; */
/* .line 1083 */
/* .local v5, "st":Landroid/app/usage/UsageStats; */
v6 = this.mPackageName;
(( java.util.HashMap ) v3 ).get ( v6 ); // invoke-virtual {v3, v6}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;
/* check-cast v6, Lcom/miui/server/smartpower/SmartPowerPolicyManager$UsageStatsInfo; */
/* .line 1084 */
/* .local v6, "info":Lcom/miui/server/smartpower/SmartPowerPolicyManager$UsageStatsInfo; */
/* if-nez v6, :cond_1 */
/* .line 1085 */
/* new-instance v7, Lcom/miui/server/smartpower/SmartPowerPolicyManager$UsageStatsInfo; */
/* invoke-direct {v7, p0, v5}, Lcom/miui/server/smartpower/SmartPowerPolicyManager$UsageStatsInfo;-><init>(Lcom/miui/server/smartpower/SmartPowerPolicyManager;Landroid/app/usage/UsageStats;)V */
/* move-object v6, v7 */
/* .line 1086 */
v7 = this.mPackageName;
(( java.util.HashMap ) v3 ).put ( v7, v6 ); // invoke-virtual {v3, v7, v6}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;
/* .line 1088 */
} // :cond_1
(( com.miui.server.smartpower.SmartPowerPolicyManager$UsageStatsInfo ) v6 ).updateStatsInfo ( v5 ); // invoke-virtual {v6, v5}, Lcom/miui/server/smartpower/SmartPowerPolicyManager$UsageStatsInfo;->updateStatsInfo(Landroid/app/usage/UsageStats;)V
/* .line 1090 */
} // .end local v5 # "st":Landroid/app/usage/UsageStats;
} // .end local v6 # "info":Lcom/miui/server/smartpower/SmartPowerPolicyManager$UsageStatsInfo;
} // :goto_1
/* .line 1091 */
} // :cond_2
v4 = this.mUsageStatsMap;
/* monitor-enter v4 */
/* .line 1092 */
try { // :try_start_0
v5 = this.mUsageStatsMap;
(( java.util.HashMap ) v5 ).clear ( ); // invoke-virtual {v5}, Ljava/util/HashMap;->clear()V
/* .line 1093 */
v5 = this.mUsageStatsMap;
(( java.util.HashMap ) v5 ).putAll ( v3 ); // invoke-virtual {v5, v3}, Ljava/util/HashMap;->putAll(Ljava/util/Map;)V
/* .line 1094 */
/* monitor-exit v4 */
/* :try_end_0 */
/* .catchall {:try_start_0 ..:try_end_0} :catchall_0 */
/* .line 1095 */
v4 = this.mAppStateManager;
(( com.android.server.am.AppStateManager ) v4 ).updateAllAppUsageStats ( ); // invoke-virtual {v4}, Lcom/android/server/am/AppStateManager;->updateAllAppUsageStats()Ljava/util/ArrayList;
/* .line 1096 */
v4 = this.mHandler;
int v5 = 1; // const/4 v5, 0x1
(( android.os.Handler ) v4 ).removeMessages ( v5 ); // invoke-virtual {v4, v5}, Landroid/os/Handler;->removeMessages(I)V
/* .line 1097 */
v4 = this.mHandler;
/* const-wide/32 v6, 0x1b7740 */
(( android.os.Handler ) v4 ).sendEmptyMessageDelayed ( v5, v6, v7 ); // invoke-virtual {v4, v5, v6, v7}, Landroid/os/Handler;->sendEmptyMessageDelayed(IJ)Z
/* .line 1098 */
android.os.Trace .traceEnd ( v1,v2 );
/* .line 1099 */
return;
/* .line 1094 */
/* :catchall_0 */
/* move-exception v1 */
try { // :try_start_1
/* monitor-exit v4 */
/* :try_end_1 */
/* .catchall {:try_start_1 ..:try_end_1} :catchall_0 */
/* throw v1 */
/* .line 1078 */
} // .end local v3 # "usageStatsMap":Ljava/util/HashMap;, "Ljava/util/HashMap<Ljava/lang/String;Lcom/miui/server/smartpower/SmartPowerPolicyManager$UsageStatsInfo;>;"
} // :cond_3
} // :goto_2
android.os.Trace .traceEnd ( v1,v2 );
/* .line 1079 */
return;
} // .end method
private static java.lang.String whiteListActionToString ( Integer p0 ) {
/* .locals 3 */
/* .param p0, "action" # I */
/* .line 1402 */
final String v0 = ""; // const-string v0, ""
/* .line 1403 */
/* .local v0, "actionStr":Ljava/lang/String; */
/* and-int/lit8 v1, p0, 0x2 */
if ( v1 != null) { // if-eqz v1, :cond_0
/* .line 1404 */
/* new-instance v1, Ljava/lang/StringBuilder; */
/* invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V */
(( java.lang.StringBuilder ) v1 ).append ( v0 ); // invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
final String v2 = "hibernation, "; // const-string v2, "hibernation, "
(( java.lang.StringBuilder ) v1 ).append ( v2 ); // invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v1 ).toString ( ); // invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
/* .line 1406 */
} // :cond_0
/* and-int/lit8 v1, p0, 0x4 */
if ( v1 != null) { // if-eqz v1, :cond_1
/* .line 1407 */
/* new-instance v1, Ljava/lang/StringBuilder; */
/* invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V */
(( java.lang.StringBuilder ) v1 ).append ( v0 ); // invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
/* const-string/jumbo v2, "screenon, " */
(( java.lang.StringBuilder ) v1 ).append ( v2 ); // invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v1 ).toString ( ); // invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
/* .line 1409 */
} // :cond_1
/* and-int/lit8 v1, p0, 0x8 */
if ( v1 != null) { // if-eqz v1, :cond_2
/* .line 1410 */
/* new-instance v1, Ljava/lang/StringBuilder; */
/* invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V */
(( java.lang.StringBuilder ) v1 ).append ( v0 ); // invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
final String v2 = "alarm, "; // const-string v2, "alarm, "
(( java.lang.StringBuilder ) v1 ).append ( v2 ); // invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v1 ).toString ( ); // invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
/* .line 1412 */
} // :cond_2
/* and-int/lit8 v1, p0, 0x10 */
if ( v1 != null) { // if-eqz v1, :cond_3
/* .line 1413 */
/* new-instance v1, Ljava/lang/StringBuilder; */
/* invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V */
(( java.lang.StringBuilder ) v1 ).append ( v0 ); // invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
/* const-string/jumbo v2, "service, " */
(( java.lang.StringBuilder ) v1 ).append ( v2 ); // invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v1 ).toString ( ); // invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
/* .line 1415 */
} // :cond_3
/* and-int/lit8 v1, p0, 0x20 */
if ( v1 != null) { // if-eqz v1, :cond_4
/* .line 1416 */
/* new-instance v1, Ljava/lang/StringBuilder; */
/* invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V */
(( java.lang.StringBuilder ) v1 ).append ( v0 ); // invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
final String v2 = "provider, "; // const-string v2, "provider, "
(( java.lang.StringBuilder ) v1 ).append ( v2 ); // invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v1 ).toString ( ); // invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
/* .line 1418 */
} // :cond_4
} // .end method
private static java.lang.String whiteListTypeToString ( Integer p0 ) {
/* .locals 3 */
/* .param p0, "type" # I */
/* .line 1348 */
final String v0 = ""; // const-string v0, ""
/* .line 1349 */
/* .local v0, "typeStr":Ljava/lang/String; */
/* and-int/lit8 v1, p0, 0x2 */
/* if-nez v1, :cond_0 */
/* and-int/lit16 v1, p0, 0x1000 */
/* if-nez v1, :cond_0 */
/* const v1, 0x8000 */
/* and-int/2addr v1, p0 */
/* if-nez v1, :cond_0 */
/* const/high16 v1, 0x40000 */
/* and-int/2addr v1, p0 */
/* if-nez v1, :cond_0 */
/* const/high16 v1, 0x200000 */
/* and-int/2addr v1, p0 */
if ( v1 != null) { // if-eqz v1, :cond_1
/* .line 1354 */
} // :cond_0
/* new-instance v1, Ljava/lang/StringBuilder; */
/* invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V */
(( java.lang.StringBuilder ) v1 ).append ( v0 ); // invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
final String v2 = "default, "; // const-string v2, "default, "
(( java.lang.StringBuilder ) v1 ).append ( v2 ); // invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v1 ).toString ( ); // invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
/* .line 1356 */
} // :cond_1
/* and-int/lit8 v1, p0, 0x4 */
/* if-nez v1, :cond_2 */
/* and-int/lit16 v1, p0, 0x2000 */
/* if-nez v1, :cond_2 */
/* const/high16 v1, 0x10000 */
/* and-int/2addr v1, p0 */
/* if-nez v1, :cond_2 */
/* const/high16 v1, 0x80000 */
/* and-int/2addr v1, p0 */
/* if-nez v1, :cond_2 */
/* const/high16 v1, 0x400000 */
/* and-int/2addr v1, p0 */
if ( v1 != null) { // if-eqz v1, :cond_3
/* .line 1361 */
} // :cond_2
/* new-instance v1, Ljava/lang/StringBuilder; */
/* invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V */
(( java.lang.StringBuilder ) v1 ).append ( v0 ); // invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
final String v2 = "cloud control, "; // const-string v2, "cloud control, "
(( java.lang.StringBuilder ) v1 ).append ( v2 ); // invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v1 ).toString ( ); // invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
/* .line 1363 */
} // :cond_3
/* and-int/lit8 v1, p0, 0x8 */
if ( v1 != null) { // if-eqz v1, :cond_4
/* .line 1364 */
/* new-instance v1, Ljava/lang/StringBuilder; */
/* invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V */
(( java.lang.StringBuilder ) v1 ).append ( v0 ); // invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
/* const-string/jumbo v2, "vpn, " */
(( java.lang.StringBuilder ) v1 ).append ( v2 ); // invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v1 ).toString ( ); // invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
/* .line 1366 */
} // :cond_4
/* and-int/lit8 v1, p0, 0x10 */
if ( v1 != null) { // if-eqz v1, :cond_5
/* .line 1367 */
/* new-instance v1, Ljava/lang/StringBuilder; */
/* invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V */
(( java.lang.StringBuilder ) v1 ).append ( v0 ); // invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
/* const-string/jumbo v2, "wallpaper, " */
(( java.lang.StringBuilder ) v1 ).append ( v2 ); // invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v1 ).toString ( ); // invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
/* .line 1369 */
} // :cond_5
/* and-int/lit8 v1, p0, 0x20 */
if ( v1 != null) { // if-eqz v1, :cond_6
/* .line 1370 */
/* new-instance v1, Ljava/lang/StringBuilder; */
/* invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V */
(( java.lang.StringBuilder ) v1 ).append ( v0 ); // invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
final String v2 = "frequent thaw, "; // const-string v2, "frequent thaw, "
(( java.lang.StringBuilder ) v1 ).append ( v2 ); // invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v1 ).toString ( ); // invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
/* .line 1372 */
} // :cond_6
/* and-int/lit8 v1, p0, 0x40 */
if ( v1 != null) { // if-eqz v1, :cond_7
/* .line 1373 */
/* new-instance v1, Ljava/lang/StringBuilder; */
/* invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V */
(( java.lang.StringBuilder ) v1 ).append ( v0 ); // invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
final String v2 = "backup, "; // const-string v2, "backup, "
(( java.lang.StringBuilder ) v1 ).append ( v2 ); // invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v1 ).toString ( ); // invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
/* .line 1375 */
} // :cond_7
/* and-int/lit16 v1, p0, 0x80 */
if ( v1 != null) { // if-eqz v1, :cond_8
/* .line 1376 */
/* new-instance v1, Ljava/lang/StringBuilder; */
/* invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V */
(( java.lang.StringBuilder ) v1 ).append ( v0 ); // invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
/* const-string/jumbo v2, "usb, " */
(( java.lang.StringBuilder ) v1 ).append ( v2 ); // invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v1 ).toString ( ); // invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
/* .line 1378 */
} // :cond_8
/* and-int/lit16 v1, p0, 0x100 */
if ( v1 != null) { // if-eqz v1, :cond_9
/* .line 1379 */
/* new-instance v1, Ljava/lang/StringBuilder; */
/* invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V */
(( java.lang.StringBuilder ) v1 ).append ( v0 ); // invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
final String v2 = "depend, "; // const-string v2, "depend, "
(( java.lang.StringBuilder ) v1 ).append ( v2 ); // invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v1 ).toString ( ); // invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
/* .line 1381 */
} // :cond_9
/* and-int/lit16 v1, p0, 0x200 */
if ( v1 != null) { // if-eqz v1, :cond_a
/* .line 1382 */
/* new-instance v1, Ljava/lang/StringBuilder; */
/* invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V */
(( java.lang.StringBuilder ) v1 ).append ( v0 ); // invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
final String v2 = "external audio focus policy, "; // const-string v2, "external audio focus policy, "
(( java.lang.StringBuilder ) v1 ).append ( v2 ); // invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v1 ).toString ( ); // invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
/* .line 1384 */
} // :cond_a
/* and-int/lit16 v1, p0, 0x400 */
if ( v1 != null) { // if-eqz v1, :cond_b
/* .line 1385 */
/* new-instance v1, Ljava/lang/StringBuilder; */
/* invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V */
(( java.lang.StringBuilder ) v1 ).append ( v0 ); // invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
final String v2 = "cts, "; // const-string v2, "cts, "
(( java.lang.StringBuilder ) v1 ).append ( v2 ); // invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v1 ).toString ( ); // invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
/* .line 1387 */
} // :cond_b
} // .end method
/* # virtual methods */
public void dump ( java.io.PrintWriter p0, java.lang.String[] p1, Integer p2 ) {
/* .locals 5 */
/* .param p1, "pw" # Ljava/io/PrintWriter; */
/* .param p2, "args" # [Ljava/lang/String; */
/* .param p3, "opti" # I */
/* .line 1422 */
/* add-int/lit8 v0, p3, 0x1 */
/* array-length v1, p2 */
/* if-ge v0, v1, :cond_8 */
/* .line 1423 */
final String v0 = "add white list:"; // const-string v0, "add white list:"
(( java.io.PrintWriter ) p1 ).println ( v0 ); // invoke-virtual {p1, v0}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V
/* .line 1424 */
/* add-int/lit8 v0, p3, 0x1 */
} // .end local p3 # "opti":I
/* .local v0, "opti":I */
/* aget-object p3, p2, p3 */
/* .line 1425 */
/* .local p3, "parm":Ljava/lang/String; */
/* add-int/lit8 v1, v0, 0x1 */
} // .end local v0 # "opti":I
/* .local v1, "opti":I */
/* aget-object v0, p2, v0 */
/* .line 1426 */
/* .local v0, "value":Ljava/lang/String; */
final String v2 = "addProviderPkg"; // const-string v2, "addProviderPkg"
v2 = (( java.lang.String ) p3 ).contains ( v2 ); // invoke-virtual {p3, v2}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z
if ( v2 != null) { // if-eqz v2, :cond_0
/* .line 1427 */
/* invoke-direct {p0, v0}, Lcom/miui/server/smartpower/SmartPowerPolicyManager;->addProviderPackageWhiteList(Ljava/lang/String;)V */
/* .line 1428 */
} // :cond_0
final String v2 = "addServicePkg"; // const-string v2, "addServicePkg"
v2 = (( java.lang.String ) p3 ).contains ( v2 ); // invoke-virtual {p3, v2}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z
if ( v2 != null) { // if-eqz v2, :cond_1
/* .line 1429 */
/* invoke-direct {p0, v0}, Lcom/miui/server/smartpower/SmartPowerPolicyManager;->addServicePackageWhiteList(Ljava/lang/String;)V */
/* .line 1430 */
} // :cond_1
final String v2 = "addAlarmPkg"; // const-string v2, "addAlarmPkg"
v2 = (( java.lang.String ) p3 ).contains ( v2 ); // invoke-virtual {p3, v2}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z
if ( v2 != null) { // if-eqz v2, :cond_2
/* .line 1431 */
/* invoke-direct {p0, v0}, Lcom/miui/server/smartpower/SmartPowerPolicyManager;->addAlarmPackageWhiteList(Ljava/lang/String;)V */
/* .line 1432 */
} // :cond_2
final String v2 = "addBroadcastProc"; // const-string v2, "addBroadcastProc"
v2 = (( java.lang.String ) p3 ).contains ( v2 ); // invoke-virtual {p3, v2}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z
if ( v2 != null) { // if-eqz v2, :cond_3
/* .line 1433 */
/* invoke-direct {p0, v0}, Lcom/miui/server/smartpower/SmartPowerPolicyManager;->addBroadcastProcessActionWhiteList(Ljava/lang/String;)V */
/* .line 1434 */
} // :cond_3
final String v2 = "addFrozenPkg"; // const-string v2, "addFrozenPkg"
v2 = (( java.lang.String ) p3 ).contains ( v2 ); // invoke-virtual {p3, v2}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z
if ( v2 != null) { // if-eqz v2, :cond_4
/* .line 1435 */
int v2 = 4; // const/4 v2, 0x4
/* invoke-direct {p0, v0, v2}, Lcom/miui/server/smartpower/SmartPowerPolicyManager;->addPackageWhiteList(Ljava/lang/String;I)V */
/* .line 1436 */
} // :cond_4
final String v2 = "addBroadcastBlackAction"; // const-string v2, "addBroadcastBlackAction"
v2 = (( java.lang.String ) p3 ).contains ( v2 ); // invoke-virtual {p3, v2}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z
if ( v2 != null) { // if-eqz v2, :cond_5
/* .line 1437 */
/* invoke-direct {p0, v0}, Lcom/miui/server/smartpower/SmartPowerPolicyManager;->addBroadcastBlackList(Ljava/lang/String;)V */
/* .line 1438 */
} // :cond_5
final String v2 = "addServiceBlackPkg"; // const-string v2, "addServiceBlackPkg"
v2 = (( java.lang.String ) p3 ).contains ( v2 ); // invoke-virtual {p3, v2}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z
if ( v2 != null) { // if-eqz v2, :cond_6
/* .line 1439 */
/* invoke-direct {p0, v0}, Lcom/miui/server/smartpower/SmartPowerPolicyManager;->addServicePkgBlackList(Ljava/lang/String;)V */
/* .line 1440 */
} // :cond_6
final String v2 = "addProviderBlackPkg"; // const-string v2, "addProviderBlackPkg"
v2 = (( java.lang.String ) p3 ).contains ( v2 ); // invoke-virtual {p3, v2}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z
if ( v2 != null) { // if-eqz v2, :cond_7
/* .line 1441 */
/* invoke-direct {p0, v0}, Lcom/miui/server/smartpower/SmartPowerPolicyManager;->addProviderPkgBlackList(Ljava/lang/String;)V */
/* .line 1447 */
} // .end local v0 # "value":Ljava/lang/String;
} // .end local p3 # "parm":Ljava/lang/String;
} // :goto_0
/* move p3, v1 */
/* .line 1443 */
/* .restart local v0 # "value":Ljava/lang/String; */
/* .restart local p3 # "parm":Ljava/lang/String; */
} // :cond_7
final String v2 = "The parameter is invalid"; // const-string v2, "The parameter is invalid"
(( java.io.PrintWriter ) p1 ).println ( v2 ); // invoke-virtual {p1, v2}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V
/* .line 1444 */
return;
/* .line 1447 */
} // .end local v0 # "value":Ljava/lang/String;
} // .end local v1 # "opti":I
/* .local p3, "opti":I */
} // :cond_8
} // :goto_1
final String v0 = "PackageWhiteList:"; // const-string v0, "PackageWhiteList:"
(( java.io.PrintWriter ) p1 ).println ( v0 ); // invoke-virtual {p1, v0}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V
/* .line 1448 */
v0 = this.mPackageWhiteList;
/* monitor-enter v0 */
/* .line 1449 */
try { // :try_start_0
v1 = this.mPackageWhiteList;
(( android.util.ArrayMap ) v1 ).entrySet ( ); // invoke-virtual {v1}, Landroid/util/ArrayMap;->entrySet()Ljava/util/Set;
v2 = } // :goto_2
if ( v2 != null) { // if-eqz v2, :cond_9
/* check-cast v2, Ljava/util/Map$Entry; */
/* .line 1450 */
/* .local v2, "entry":Ljava/util/Map$Entry;, "Ljava/util/Map$Entry<Ljava/lang/String;Lcom/miui/server/smartpower/SmartPowerPolicyManager$WhiteListItem;>;" */
/* new-instance v3, Ljava/lang/StringBuilder; */
/* invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V */
final String v4 = " "; // const-string v4, " "
(( java.lang.StringBuilder ) v3 ).append ( v4 ); // invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v3 ).append ( v4 ); // invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v3 ).toString ( ); // invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
(( java.io.PrintWriter ) p1 ).println ( v3 ); // invoke-virtual {p1, v3}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V
/* .line 1451 */
} // .end local v2 # "entry":Ljava/util/Map$Entry;, "Ljava/util/Map$Entry<Ljava/lang/String;Lcom/miui/server/smartpower/SmartPowerPolicyManager$WhiteListItem;>;"
/* .line 1452 */
} // :cond_9
/* monitor-exit v0 */
/* :try_end_0 */
/* .catchall {:try_start_0 ..:try_end_0} :catchall_8 */
/* .line 1453 */
final String v0 = "ProcessWhiteList:"; // const-string v0, "ProcessWhiteList:"
(( java.io.PrintWriter ) p1 ).println ( v0 ); // invoke-virtual {p1, v0}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V
/* .line 1454 */
v1 = this.mProcessWhiteList;
/* monitor-enter v1 */
/* .line 1455 */
try { // :try_start_1
v0 = this.mProcessWhiteList;
(( android.util.ArrayMap ) v0 ).entrySet ( ); // invoke-virtual {v0}, Landroid/util/ArrayMap;->entrySet()Ljava/util/Set;
v2 = } // :goto_3
if ( v2 != null) { // if-eqz v2, :cond_a
/* check-cast v2, Ljava/util/Map$Entry; */
/* .line 1456 */
/* .restart local v2 # "entry":Ljava/util/Map$Entry;, "Ljava/util/Map$Entry<Ljava/lang/String;Lcom/miui/server/smartpower/SmartPowerPolicyManager$WhiteListItem;>;" */
/* new-instance v3, Ljava/lang/StringBuilder; */
/* invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V */
final String v4 = " "; // const-string v4, " "
(( java.lang.StringBuilder ) v3 ).append ( v4 ); // invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v3 ).append ( v4 ); // invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v3 ).toString ( ); // invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
(( java.io.PrintWriter ) p1 ).println ( v3 ); // invoke-virtual {p1, v3}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V
/* .line 1457 */
} // .end local v2 # "entry":Ljava/util/Map$Entry;, "Ljava/util/Map$Entry<Ljava/lang/String;Lcom/miui/server/smartpower/SmartPowerPolicyManager$WhiteListItem;>;"
/* .line 1458 */
} // :cond_a
/* monitor-exit v1 */
/* :try_end_1 */
/* .catchall {:try_start_1 ..:try_end_1} :catchall_7 */
/* .line 1459 */
final String v0 = "UidWhiteList:"; // const-string v0, "UidWhiteList:"
(( java.io.PrintWriter ) p1 ).println ( v0 ); // invoke-virtual {p1, v0}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V
/* .line 1460 */
v0 = this.mUidWhiteList;
/* monitor-enter v0 */
/* .line 1461 */
try { // :try_start_2
v1 = this.mUidWhiteList;
(( android.util.ArrayMap ) v1 ).entrySet ( ); // invoke-virtual {v1}, Landroid/util/ArrayMap;->entrySet()Ljava/util/Set;
v2 = } // :goto_4
if ( v2 != null) { // if-eqz v2, :cond_b
/* check-cast v2, Ljava/util/Map$Entry; */
/* .line 1462 */
/* .local v2, "entry":Ljava/util/Map$Entry;, "Ljava/util/Map$Entry<Ljava/lang/Integer;Lcom/miui/server/smartpower/SmartPowerPolicyManager$WhiteListItem;>;" */
/* new-instance v3, Ljava/lang/StringBuilder; */
/* invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V */
final String v4 = " "; // const-string v4, " "
(( java.lang.StringBuilder ) v3 ).append ( v4 ); // invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v3 ).append ( v4 ); // invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v3 ).toString ( ); // invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
(( java.io.PrintWriter ) p1 ).println ( v3 ); // invoke-virtual {p1, v3}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V
/* .line 1463 */
} // .end local v2 # "entry":Ljava/util/Map$Entry;, "Ljava/util/Map$Entry<Ljava/lang/Integer;Lcom/miui/server/smartpower/SmartPowerPolicyManager$WhiteListItem;>;"
/* .line 1464 */
} // :cond_b
/* monitor-exit v0 */
/* :try_end_2 */
/* .catchall {:try_start_2 ..:try_end_2} :catchall_6 */
/* .line 1465 */
final String v0 = "PidWhiteList:"; // const-string v0, "PidWhiteList:"
(( java.io.PrintWriter ) p1 ).println ( v0 ); // invoke-virtual {p1, v0}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V
/* .line 1466 */
v1 = this.mPidWhiteList;
/* monitor-enter v1 */
/* .line 1467 */
try { // :try_start_3
v0 = this.mPidWhiteList;
(( android.util.ArrayMap ) v0 ).entrySet ( ); // invoke-virtual {v0}, Landroid/util/ArrayMap;->entrySet()Ljava/util/Set;
v2 = } // :goto_5
if ( v2 != null) { // if-eqz v2, :cond_c
/* check-cast v2, Ljava/util/Map$Entry; */
/* .line 1468 */
/* .restart local v2 # "entry":Ljava/util/Map$Entry;, "Ljava/util/Map$Entry<Ljava/lang/Integer;Lcom/miui/server/smartpower/SmartPowerPolicyManager$WhiteListItem;>;" */
/* new-instance v3, Ljava/lang/StringBuilder; */
/* invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V */
final String v4 = " "; // const-string v4, " "
(( java.lang.StringBuilder ) v3 ).append ( v4 ); // invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v3 ).append ( v4 ); // invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v3 ).toString ( ); // invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
(( java.io.PrintWriter ) p1 ).println ( v3 ); // invoke-virtual {p1, v3}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V
/* .line 1469 */
} // .end local v2 # "entry":Ljava/util/Map$Entry;, "Ljava/util/Map$Entry<Ljava/lang/Integer;Lcom/miui/server/smartpower/SmartPowerPolicyManager$WhiteListItem;>;"
/* .line 1470 */
} // :cond_c
/* monitor-exit v1 */
/* :try_end_3 */
/* .catchall {:try_start_3 ..:try_end_3} :catchall_5 */
/* .line 1472 */
final String v0 = "BroadcastWhiteList:"; // const-string v0, "BroadcastWhiteList:"
(( java.io.PrintWriter ) p1 ).println ( v0 ); // invoke-virtual {p1, v0}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V
/* .line 1473 */
v0 = this.mBroadcastWhiteList;
/* monitor-enter v0 */
/* .line 1474 */
int v1 = 0; // const/4 v1, 0x0
/* .local v1, "i":I */
} // :goto_6
try { // :try_start_4
v2 = this.mBroadcastWhiteList;
v2 = (( android.util.ArraySet ) v2 ).size ( ); // invoke-virtual {v2}, Landroid/util/ArraySet;->size()I
/* if-ge v1, v2, :cond_d */
/* .line 1475 */
/* new-instance v2, Ljava/lang/StringBuilder; */
/* invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V */
final String v3 = " "; // const-string v3, " "
(( java.lang.StringBuilder ) v2 ).append ( v3 ); // invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
v3 = this.mBroadcastWhiteList;
(( android.util.ArraySet ) v3 ).valueAt ( v1 ); // invoke-virtual {v3, v1}, Landroid/util/ArraySet;->valueAt(I)Ljava/lang/Object;
/* check-cast v3, Ljava/lang/String; */
(( java.lang.StringBuilder ) v2 ).append ( v3 ); // invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v2 ).toString ( ); // invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
(( java.io.PrintWriter ) p1 ).println ( v2 ); // invoke-virtual {p1, v2}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V
/* .line 1474 */
/* add-int/lit8 v1, v1, 0x1 */
/* .line 1477 */
} // .end local v1 # "i":I
} // :cond_d
/* monitor-exit v0 */
/* :try_end_4 */
/* .catchall {:try_start_4 ..:try_end_4} :catchall_4 */
/* .line 1479 */
final String v0 = "BroadcastProcessWhiteList:"; // const-string v0, "BroadcastProcessWhiteList:"
(( java.io.PrintWriter ) p1 ).println ( v0 ); // invoke-virtual {p1, v0}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V
/* .line 1480 */
v1 = this.mBroadcastProcessActionWhiteList;
/* monitor-enter v1 */
/* .line 1481 */
int v0 = 0; // const/4 v0, 0x0
/* .local v0, "i":I */
} // :goto_7
try { // :try_start_5
v2 = this.mBroadcastProcessActionWhiteList;
v2 = (( android.util.ArraySet ) v2 ).size ( ); // invoke-virtual {v2}, Landroid/util/ArraySet;->size()I
/* if-ge v0, v2, :cond_e */
/* .line 1482 */
/* new-instance v2, Ljava/lang/StringBuilder; */
/* invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V */
final String v3 = " "; // const-string v3, " "
(( java.lang.StringBuilder ) v2 ).append ( v3 ); // invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
v3 = this.mBroadcastProcessActionWhiteList;
(( android.util.ArraySet ) v3 ).valueAt ( v0 ); // invoke-virtual {v3, v0}, Landroid/util/ArraySet;->valueAt(I)Ljava/lang/Object;
/* check-cast v3, Ljava/lang/String; */
(( java.lang.StringBuilder ) v2 ).append ( v3 ); // invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v2 ).toString ( ); // invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
(( java.io.PrintWriter ) p1 ).println ( v2 ); // invoke-virtual {p1, v2}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V
/* .line 1481 */
/* add-int/lit8 v0, v0, 0x1 */
/* .line 1484 */
} // .end local v0 # "i":I
} // :cond_e
/* monitor-exit v1 */
/* :try_end_5 */
/* .catchall {:try_start_5 ..:try_end_5} :catchall_3 */
/* .line 1486 */
final String v0 = "InterceptPackageList:"; // const-string v0, "InterceptPackageList:"
(( java.io.PrintWriter ) p1 ).println ( v0 ); // invoke-virtual {p1, v0}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V
/* .line 1487 */
v0 = com.miui.server.smartpower.SmartPowerPolicyManager.sInterceptPackageList;
/* monitor-enter v0 */
/* .line 1488 */
int v1 = 0; // const/4 v1, 0x0
/* .restart local v1 # "i":I */
} // :goto_8
try { // :try_start_6
v2 = com.miui.server.smartpower.SmartPowerPolicyManager.sInterceptPackageList;
v3 = (( android.util.ArraySet ) v2 ).size ( ); // invoke-virtual {v2}, Landroid/util/ArraySet;->size()I
/* if-ge v1, v3, :cond_f */
/* .line 1489 */
/* new-instance v3, Ljava/lang/StringBuilder; */
/* invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V */
final String v4 = " "; // const-string v4, " "
(( java.lang.StringBuilder ) v3 ).append ( v4 ); // invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( android.util.ArraySet ) v2 ).valueAt ( v1 ); // invoke-virtual {v2, v1}, Landroid/util/ArraySet;->valueAt(I)Ljava/lang/Object;
/* check-cast v2, Ljava/lang/String; */
(( java.lang.StringBuilder ) v3 ).append ( v2 ); // invoke-virtual {v3, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v2 ).toString ( ); // invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
(( java.io.PrintWriter ) p1 ).println ( v2 ); // invoke-virtual {p1, v2}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V
/* .line 1488 */
/* add-int/lit8 v1, v1, 0x1 */
/* .line 1491 */
} // .end local v1 # "i":I
} // :cond_f
/* monitor-exit v0 */
/* :try_end_6 */
/* .catchall {:try_start_6 ..:try_end_6} :catchall_2 */
/* .line 1493 */
final String v0 = "Dependency:"; // const-string v0, "Dependency:"
(( java.io.PrintWriter ) p1 ).println ( v0 ); // invoke-virtual {p1, v0}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V
/* .line 1494 */
v1 = this.mDependencyMap;
/* monitor-enter v1 */
/* .line 1495 */
try { // :try_start_7
v0 = this.mDependencyMap;
(( android.util.ArrayMap ) v0 ).entrySet ( ); // invoke-virtual {v0}, Landroid/util/ArrayMap;->entrySet()Ljava/util/Set;
v2 = } // :goto_9
if ( v2 != null) { // if-eqz v2, :cond_10
/* check-cast v2, Ljava/util/Map$Entry; */
/* .line 1496 */
/* .local v2, "entry":Ljava/util/Map$Entry;, "Ljava/util/Map$Entry<Ljava/lang/Integer;Landroid/util/ArraySet<Ljava/lang/String;>;>;" */
/* new-instance v3, Ljava/lang/StringBuilder; */
/* invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V */
final String v4 = " "; // const-string v4, " "
(( java.lang.StringBuilder ) v3 ).append ( v4 ); // invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v3 ).append ( v4 ); // invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;
final String v4 = " "; // const-string v4, " "
(( java.lang.StringBuilder ) v3 ).append ( v4 ); // invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v3 ).append ( v4 ); // invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v3 ).toString ( ); // invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
(( java.io.PrintWriter ) p1 ).println ( v3 ); // invoke-virtual {p1, v3}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V
/* .line 1497 */
} // .end local v2 # "entry":Ljava/util/Map$Entry;, "Ljava/util/Map$Entry<Ljava/lang/Integer;Landroid/util/ArraySet<Ljava/lang/String;>;>;"
/* .line 1498 */
} // :cond_10
/* monitor-exit v1 */
/* :try_end_7 */
/* .catchall {:try_start_7 ..:try_end_7} :catchall_1 */
/* .line 1500 */
final String v0 = "UsageStats:"; // const-string v0, "UsageStats:"
(( java.io.PrintWriter ) p1 ).println ( v0 ); // invoke-virtual {p1, v0}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V
/* .line 1501 */
v0 = this.mUsageStatsMap;
/* monitor-enter v0 */
/* .line 1502 */
try { // :try_start_8
v1 = this.mUsageStatsMap;
(( java.util.HashMap ) v1 ).values ( ); // invoke-virtual {v1}, Ljava/util/HashMap;->values()Ljava/util/Collection;
v2 = } // :goto_a
if ( v2 != null) { // if-eqz v2, :cond_11
/* check-cast v2, Lcom/miui/server/smartpower/SmartPowerPolicyManager$UsageStatsInfo; */
/* .line 1503 */
/* .local v2, "info":Lcom/miui/server/smartpower/SmartPowerPolicyManager$UsageStatsInfo; */
(( com.miui.server.smartpower.SmartPowerPolicyManager$UsageStatsInfo ) v2 ).dump ( p1, p2, p3 ); // invoke-virtual {v2, p1, p2, p3}, Lcom/miui/server/smartpower/SmartPowerPolicyManager$UsageStatsInfo;->dump(Ljava/io/PrintWriter;[Ljava/lang/String;I)V
/* .line 1504 */
} // .end local v2 # "info":Lcom/miui/server/smartpower/SmartPowerPolicyManager$UsageStatsInfo;
/* .line 1505 */
} // :cond_11
/* monitor-exit v0 */
/* .line 1506 */
return;
/* .line 1505 */
/* :catchall_0 */
/* move-exception v1 */
/* monitor-exit v0 */
/* :try_end_8 */
/* .catchall {:try_start_8 ..:try_end_8} :catchall_0 */
/* throw v1 */
/* .line 1498 */
/* :catchall_1 */
/* move-exception v0 */
try { // :try_start_9
/* monitor-exit v1 */
/* :try_end_9 */
/* .catchall {:try_start_9 ..:try_end_9} :catchall_1 */
/* throw v0 */
/* .line 1491 */
/* :catchall_2 */
/* move-exception v1 */
try { // :try_start_a
/* monitor-exit v0 */
/* :try_end_a */
/* .catchall {:try_start_a ..:try_end_a} :catchall_2 */
/* throw v1 */
/* .line 1484 */
/* :catchall_3 */
/* move-exception v0 */
try { // :try_start_b
/* monitor-exit v1 */
/* :try_end_b */
/* .catchall {:try_start_b ..:try_end_b} :catchall_3 */
/* throw v0 */
/* .line 1477 */
/* :catchall_4 */
/* move-exception v1 */
try { // :try_start_c
/* monitor-exit v0 */
/* :try_end_c */
/* .catchall {:try_start_c ..:try_end_c} :catchall_4 */
/* throw v1 */
/* .line 1470 */
/* :catchall_5 */
/* move-exception v0 */
try { // :try_start_d
/* monitor-exit v1 */
/* :try_end_d */
/* .catchall {:try_start_d ..:try_end_d} :catchall_5 */
/* throw v0 */
/* .line 1464 */
/* :catchall_6 */
/* move-exception v1 */
try { // :try_start_e
/* monitor-exit v0 */
/* :try_end_e */
/* .catchall {:try_start_e ..:try_end_e} :catchall_6 */
/* throw v1 */
/* .line 1458 */
/* :catchall_7 */
/* move-exception v0 */
try { // :try_start_f
/* monitor-exit v1 */
/* :try_end_f */
/* .catchall {:try_start_f ..:try_end_f} :catchall_7 */
/* throw v0 */
/* .line 1452 */
/* :catchall_8 */
/* move-exception v1 */
try { // :try_start_10
/* monitor-exit v0 */
/* :try_end_10 */
/* .catchall {:try_start_10 ..:try_end_10} :catchall_8 */
/* throw v1 */
} // .end method
public Long getHibernateDuration ( com.android.server.am.AppStateManager$AppState p0 ) {
/* .locals 3 */
/* .param p1, "app" # Lcom/android/server/am/AppStateManager$AppState; */
/* .line 776 */
v0 = (( com.android.server.am.AppStateManager$AppState ) p1 ).getUsageLevel ( ); // invoke-virtual {p1}, Lcom/android/server/am/AppStateManager$AppState;->getUsageLevel()I
/* .line 777 */
/* .local v0, "usageLevel":I */
int v1 = 5; // const/4 v1, 0x5
/* if-ne v0, v1, :cond_0 */
/* .line 778 */
v1 = (( com.android.server.am.AppStateManager$AppState ) p1 ).isAutoStartApp ( ); // invoke-virtual {p1}, Lcom/android/server/am/AppStateManager$AppState;->isAutoStartApp()Z
if ( v1 != null) { // if-eqz v1, :cond_0
v1 = (( com.android.server.am.AppStateManager$AppState ) p1 ).hasProtectProcess ( ); // invoke-virtual {p1}, Lcom/android/server/am/AppStateManager$AppState;->hasProtectProcess()Z
/* if-nez v1, :cond_0 */
/* .line 779 */
/* sget-wide v1, Lcom/miui/app/smartpower/SmartPowerSettings;->HIBERNATE_DURATION:J */
/* return-wide v1 */
/* .line 781 */
} // :cond_0
/* const-wide/16 v1, 0x0 */
/* return-wide v1 */
} // .end method
public Long getIdleDur ( java.lang.String p0 ) {
/* .locals 2 */
/* .param p1, "packageName" # Ljava/lang/String; */
/* .line 772 */
/* sget-wide v0, Lcom/miui/app/smartpower/SmartPowerSettings;->IDLE_DURATION:J */
/* return-wide v0 */
} // .end method
public Long getInactiveDuration ( java.lang.String p0, Integer p1 ) {
/* .locals 2 */
/* .param p1, "packageName" # Ljava/lang/String; */
/* .param p2, "uid" # I */
/* .line 789 */
v0 = com.miui.server.smartpower.SmartPowerPolicyManager .isSpeedTestMode ( );
if ( v0 != null) { // if-eqz v0, :cond_0
/* .line 790 */
/* sget-wide v0, Lcom/miui/app/smartpower/SmartPowerSettings;->INACTIVE_SPTM_DURATION:J */
/* return-wide v0 */
/* .line 792 */
} // :cond_0
/* sget-wide v0, Lcom/miui/app/smartpower/SmartPowerSettings;->INACTIVE_DURATION:J */
/* return-wide v0 */
} // .end method
public Long getMaintenanceDur ( java.lang.String p0 ) {
/* .locals 2 */
/* .param p1, "packageName" # Ljava/lang/String; */
/* .line 785 */
/* sget-wide v0, Lcom/miui/app/smartpower/SmartPowerSettings;->MAINTENANCE_DURATION:J */
/* return-wide v0 */
} // .end method
public com.miui.server.smartpower.SmartPowerPolicyManager$UsageStatsInfo getUsageStatsInfo ( java.lang.String p0 ) {
/* .locals 3 */
/* .param p1, "packageName" # Ljava/lang/String; */
/* .line 1061 */
v0 = this.mUsageStatsMap;
/* monitor-enter v0 */
/* .line 1062 */
try { // :try_start_0
v1 = this.mUsageStatsMap;
(( java.util.HashMap ) v1 ).get ( p1 ); // invoke-virtual {v1, p1}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;
/* check-cast v1, Lcom/miui/server/smartpower/SmartPowerPolicyManager$UsageStatsInfo; */
/* .line 1063 */
/* .local v1, "info":Lcom/miui/server/smartpower/SmartPowerPolicyManager$UsageStatsInfo; */
/* monitor-exit v0 */
/* :try_end_0 */
/* .catchall {:try_start_0 ..:try_end_0} :catchall_0 */
/* .line 1064 */
/* if-nez v1, :cond_1 */
/* .line 1065 */
v0 = this.mUsageStatsMap;
v0 = (( java.util.HashMap ) v0 ).isEmpty ( ); // invoke-virtual {v0}, Ljava/util/HashMap;->isEmpty()Z
if ( v0 != null) { // if-eqz v0, :cond_0
/* .line 1066 */
v0 = this.mHandler;
int v2 = 1; // const/4 v2, 0x1
(( android.os.Handler ) v0 ).removeMessages ( v2 ); // invoke-virtual {v0, v2}, Landroid/os/Handler;->removeMessages(I)V
/* .line 1067 */
v0 = this.mHandler;
(( android.os.Handler ) v0 ).sendEmptyMessage ( v2 ); // invoke-virtual {v0, v2}, Landroid/os/Handler;->sendEmptyMessage(I)Z
/* .line 1069 */
} // :cond_0
/* new-instance v0, Lcom/miui/server/smartpower/SmartPowerPolicyManager$UsageStatsInfo; */
/* invoke-direct {v0, p0, p1}, Lcom/miui/server/smartpower/SmartPowerPolicyManager$UsageStatsInfo;-><init>(Lcom/miui/server/smartpower/SmartPowerPolicyManager;Ljava/lang/String;)V */
/* move-object v1, v0 */
/* .line 1071 */
} // :cond_1
/* .line 1063 */
} // .end local v1 # "info":Lcom/miui/server/smartpower/SmartPowerPolicyManager$UsageStatsInfo;
/* :catchall_0 */
/* move-exception v1 */
try { // :try_start_1
/* monitor-exit v0 */
/* :try_end_1 */
/* .catchall {:try_start_1 ..:try_end_1} :catchall_0 */
/* throw v1 */
} // .end method
public void init ( com.android.server.am.AppStateManager p0, com.miui.server.smartpower.AppPowerResourceManager p1 ) {
/* .locals 0 */
/* .param p1, "appStateManager" # Lcom/android/server/am/AppStateManager; */
/* .param p2, "appPowerResourceManager" # Lcom/miui/server/smartpower/AppPowerResourceManager; */
/* .line 296 */
this.mAppStateManager = p1;
/* .line 297 */
(( com.android.server.am.AppStateManager ) p1 ).registerAppStateListener ( p0 ); // invoke-virtual {p1, p0}, Lcom/android/server/am/AppStateManager;->registerAppStateListener(Lcom/android/server/am/AppStateManager$IProcessStateCallback;)Z
/* .line 298 */
this.mAppPowerResourceManager = p2;
/* .line 299 */
return;
} // .end method
public Boolean isAlarmPackageWhiteList ( java.lang.String p0 ) {
/* .locals 3 */
/* .param p1, "pkgname" # Ljava/lang/String; */
/* .line 1238 */
v0 = this.mPackageWhiteList;
/* monitor-enter v0 */
/* .line 1239 */
try { // :try_start_0
v1 = this.mPackageWhiteList;
(( android.util.ArrayMap ) v1 ).get ( p1 ); // invoke-virtual {v1, p1}, Landroid/util/ArrayMap;->get(Ljava/lang/Object;)Ljava/lang/Object;
/* check-cast v1, Lcom/miui/server/smartpower/SmartPowerPolicyManager$WhiteListItem; */
/* .line 1240 */
/* .local v1, "item":Lcom/miui/server/smartpower/SmartPowerPolicyManager$WhiteListItem; */
if ( v1 != null) { // if-eqz v1, :cond_0
/* const/16 v2, 0x8 */
v2 = (( com.miui.server.smartpower.SmartPowerPolicyManager$WhiteListItem ) v1 ).hasAction ( v2 ); // invoke-virtual {v1, v2}, Lcom/miui/server/smartpower/SmartPowerPolicyManager$WhiteListItem;->hasAction(I)Z
if ( v2 != null) { // if-eqz v2, :cond_0
/* .line 1241 */
/* monitor-exit v0 */
int v0 = 1; // const/4 v0, 0x1
/* .line 1243 */
} // .end local v1 # "item":Lcom/miui/server/smartpower/SmartPowerPolicyManager$WhiteListItem;
} // :cond_0
/* monitor-exit v0 */
/* .line 1244 */
int v0 = 0; // const/4 v0, 0x0
/* .line 1243 */
/* :catchall_0 */
/* move-exception v1 */
/* monitor-exit v0 */
/* :try_end_0 */
/* .catchall {:try_start_0 ..:try_end_0} :catchall_0 */
/* throw v1 */
} // .end method
public Boolean isAppHibernationWhiteList ( Integer p0, java.lang.String p1 ) {
/* .locals 1 */
/* .param p1, "uid" # I */
/* .param p2, "packageName" # Ljava/lang/String; */
/* .line 929 */
v0 = android.os.Process .isCoreUid ( p1 );
/* if-nez v0, :cond_1 */
/* .line 930 */
v0 = /* invoke-direct {p0, p2}, Lcom/miui/server/smartpower/SmartPowerPolicyManager;->isPackageWhiteList(Ljava/lang/String;)Z */
/* if-nez v0, :cond_1 */
/* .line 931 */
v0 = /* invoke-direct {p0, p1}, Lcom/miui/server/smartpower/SmartPowerPolicyManager;->isUidWhiteList(I)Z */
/* if-nez v0, :cond_1 */
v0 = this.mPowerSavingStrategyControl;
/* .line 932 */
v0 = com.miui.server.smartpower.SmartPowerPolicyManager$PowerSavingStrategyControl .-$$Nest$misNoRestrictApp ( v0,p2 );
if ( v0 != null) { // if-eqz v0, :cond_0
/* .line 935 */
} // :cond_0
int v0 = 0; // const/4 v0, 0x0
/* .line 933 */
} // :cond_1
} // :goto_0
int v0 = 1; // const/4 v0, 0x1
} // .end method
public Boolean isAppHibernationWhiteList ( com.android.server.am.AppStateManager$AppState p0 ) {
/* .locals 2 */
/* .param p1, "appState" # Lcom/android/server/am/AppStateManager$AppState; */
/* .line 914 */
v0 = (( com.android.server.am.AppStateManager$AppState ) p1 ).getUid ( ); // invoke-virtual {p1}, Lcom/android/server/am/AppStateManager$AppState;->getUid()I
(( com.android.server.am.AppStateManager$AppState ) p1 ).getPackageName ( ); // invoke-virtual {p1}, Lcom/android/server/am/AppStateManager$AppState;->getPackageName()Ljava/lang/String;
v0 = (( com.miui.server.smartpower.SmartPowerPolicyManager ) p0 ).isAppHibernationWhiteList ( v0, v1 ); // invoke-virtual {p0, v0, v1}, Lcom/miui/server/smartpower/SmartPowerPolicyManager;->isAppHibernationWhiteList(ILjava/lang/String;)Z
} // .end method
public Boolean isBroadcastBlackList ( java.lang.String p0 ) {
/* .locals 2 */
/* .param p1, "intent" # Ljava/lang/String; */
/* .line 1306 */
v0 = this.mBroadcastBlackList;
/* monitor-enter v0 */
/* .line 1307 */
try { // :try_start_0
v1 = this.mBroadcastBlackList;
v1 = (( android.util.ArraySet ) v1 ).contains ( p1 ); // invoke-virtual {v1, p1}, Landroid/util/ArraySet;->contains(Ljava/lang/Object;)Z
/* monitor-exit v0 */
/* .line 1308 */
/* :catchall_0 */
/* move-exception v1 */
/* monitor-exit v0 */
/* :try_end_0 */
/* .catchall {:try_start_0 ..:try_end_0} :catchall_0 */
/* throw v1 */
} // .end method
public Boolean isBroadcastWhiteList ( java.lang.String p0 ) {
/* .locals 2 */
/* .param p1, "intent" # Ljava/lang/String; */
/* .line 1342 */
v0 = this.mBroadcastWhiteList;
/* monitor-enter v0 */
/* .line 1343 */
try { // :try_start_0
v1 = this.mBroadcastWhiteList;
v1 = (( android.util.ArraySet ) v1 ).contains ( p1 ); // invoke-virtual {v1, p1}, Landroid/util/ArraySet;->contains(Ljava/lang/Object;)Z
/* monitor-exit v0 */
/* .line 1344 */
/* :catchall_0 */
/* move-exception v1 */
/* monitor-exit v0 */
/* :try_end_0 */
/* .catchall {:try_start_0 ..:try_end_0} :catchall_0 */
/* throw v1 */
} // .end method
public Boolean isDependedProcess ( Integer p0 ) {
/* .locals 3 */
/* .param p1, "pid" # I */
/* .line 359 */
v0 = this.mDependencyMap;
/* monitor-enter v0 */
/* .line 360 */
try { // :try_start_0
v1 = this.mDependencyMap;
java.lang.Integer .valueOf ( p1 );
v1 = (( android.util.ArrayMap ) v1 ).containsKey ( v2 ); // invoke-virtual {v1, v2}, Landroid/util/ArrayMap;->containsKey(Ljava/lang/Object;)Z
/* monitor-exit v0 */
/* .line 361 */
/* :catchall_0 */
/* move-exception v1 */
/* monitor-exit v0 */
/* :try_end_0 */
/* .catchall {:try_start_0 ..:try_end_0} :catchall_0 */
/* throw v1 */
} // .end method
public Boolean isNoRestrictApp ( java.lang.String p0 ) {
/* .locals 1 */
/* .param p1, "packageName" # Ljava/lang/String; */
/* .line 1509 */
v0 = this.mPowerSavingStrategyControl;
v0 = com.miui.server.smartpower.SmartPowerPolicyManager$PowerSavingStrategyControl .-$$Nest$misNoRestrictApp ( v0,p1 );
} // .end method
public Boolean isPackageWhiteList ( Integer p0, java.lang.String p1 ) {
/* .locals 5 */
/* .param p1, "flag" # I */
/* .param p2, "packageName" # Ljava/lang/String; */
/* .line 970 */
v0 = this.mPackageWhiteList;
/* monitor-enter v0 */
/* .line 971 */
try { // :try_start_0
v1 = this.mPackageWhiteList;
(( android.util.ArrayMap ) v1 ).get ( p2 ); // invoke-virtual {v1, p2}, Landroid/util/ArrayMap;->get(Ljava/lang/Object;)Ljava/lang/Object;
/* check-cast v1, Lcom/miui/server/smartpower/SmartPowerPolicyManager$WhiteListItem; */
/* .line 972 */
/* .local v1, "item":Lcom/miui/server/smartpower/SmartPowerPolicyManager$WhiteListItem; */
int v2 = 1; // const/4 v2, 0x1
if ( v1 != null) { // if-eqz v1, :cond_0
v3 = com.miui.server.smartpower.SmartPowerPolicyManager$WhiteListItem .-$$Nest$fgetmTypes ( v1 );
/* and-int/2addr v3, p1 */
if ( v3 != null) { // if-eqz v3, :cond_0
/* .line 973 */
/* monitor-exit v0 */
/* .line 975 */
} // .end local v1 # "item":Lcom/miui/server/smartpower/SmartPowerPolicyManager$WhiteListItem;
} // :cond_0
/* monitor-exit v0 */
/* :try_end_0 */
/* .catchall {:try_start_0 ..:try_end_0} :catchall_1 */
/* .line 976 */
v1 = this.mUidWhiteList;
/* monitor-enter v1 */
/* .line 977 */
try { // :try_start_1
v0 = this.mUidWhiteList;
(( android.util.ArrayMap ) v0 ).values ( ); // invoke-virtual {v0}, Landroid/util/ArrayMap;->values()Ljava/util/Collection;
v3 = } // :goto_0
if ( v3 != null) { // if-eqz v3, :cond_2
/* check-cast v3, Lcom/miui/server/smartpower/SmartPowerPolicyManager$WhiteListItem; */
/* .line 978 */
/* .local v3, "item":Lcom/miui/server/smartpower/SmartPowerPolicyManager$WhiteListItem; */
com.miui.server.smartpower.SmartPowerPolicyManager$WhiteListItem .-$$Nest$fgetmName ( v3 );
v4 = (( java.lang.String ) v4 ).equals ( p2 ); // invoke-virtual {v4, p2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z
if ( v4 != null) { // if-eqz v4, :cond_1
/* .line 979 */
v0 = (( com.miui.server.smartpower.SmartPowerPolicyManager$WhiteListItem ) v3 ).hasWhiteListType ( p1 ); // invoke-virtual {v3, p1}, Lcom/miui/server/smartpower/SmartPowerPolicyManager$WhiteListItem;->hasWhiteListType(I)Z
if ( v0 != null) { // if-eqz v0, :cond_2
/* .line 980 */
/* monitor-exit v1 */
/* .line 984 */
} // .end local v3 # "item":Lcom/miui/server/smartpower/SmartPowerPolicyManager$WhiteListItem;
} // :cond_1
/* .line 985 */
} // :cond_2
/* monitor-exit v1 */
/* .line 986 */
int v0 = 0; // const/4 v0, 0x0
/* .line 985 */
/* :catchall_0 */
/* move-exception v0 */
/* monitor-exit v1 */
/* :try_end_1 */
/* .catchall {:try_start_1 ..:try_end_1} :catchall_0 */
/* throw v0 */
/* .line 975 */
/* :catchall_1 */
/* move-exception v1 */
try { // :try_start_2
/* monitor-exit v0 */
/* :try_end_2 */
/* .catchall {:try_start_2 ..:try_end_2} :catchall_1 */
/* throw v1 */
} // .end method
public Boolean isProcessHibernationWhiteList ( Integer p0, Integer p1, java.lang.String p2, java.lang.String p3 ) {
/* .locals 1 */
/* .param p1, "uid" # I */
/* .param p2, "pid" # I */
/* .param p3, "packageName" # Ljava/lang/String; */
/* .param p4, "processName" # Ljava/lang/String; */
/* .line 919 */
v0 = (( com.miui.server.smartpower.SmartPowerPolicyManager ) p0 ).isAppHibernationWhiteList ( p1, p3 ); // invoke-virtual {p0, p1, p3}, Lcom/miui/server/smartpower/SmartPowerPolicyManager;->isAppHibernationWhiteList(ILjava/lang/String;)Z
/* if-nez v0, :cond_1 */
/* .line 920 */
v0 = /* invoke-direct {p0, p4}, Lcom/miui/server/smartpower/SmartPowerPolicyManager;->isProcessWhiteList(Ljava/lang/String;)Z */
/* if-nez v0, :cond_1 */
/* .line 921 */
v0 = com.miui.server.process.ProcessManagerInternal .checkCtsProcess ( p4 );
/* if-nez v0, :cond_1 */
/* .line 922 */
v0 = /* invoke-direct {p0, p2}, Lcom/miui/server/smartpower/SmartPowerPolicyManager;->isPidWhiteList(I)Z */
if ( v0 != null) { // if-eqz v0, :cond_0
/* .line 925 */
} // :cond_0
int v0 = 0; // const/4 v0, 0x0
/* .line 923 */
} // :cond_1
} // :goto_0
int v0 = 1; // const/4 v0, 0x1
} // .end method
public Boolean isProcessHibernationWhiteList ( com.android.server.am.AppStateManager$AppState$RunningProcess p0 ) {
/* .locals 4 */
/* .param p1, "proc" # Lcom/android/server/am/AppStateManager$AppState$RunningProcess; */
/* .line 909 */
v0 = (( com.android.server.am.AppStateManager$AppState$RunningProcess ) p1 ).getUid ( ); // invoke-virtual {p1}, Lcom/android/server/am/AppStateManager$AppState$RunningProcess;->getUid()I
v1 = (( com.android.server.am.AppStateManager$AppState$RunningProcess ) p1 ).getPid ( ); // invoke-virtual {p1}, Lcom/android/server/am/AppStateManager$AppState$RunningProcess;->getPid()I
/* .line 910 */
(( com.android.server.am.AppStateManager$AppState$RunningProcess ) p1 ).getPackageName ( ); // invoke-virtual {p1}, Lcom/android/server/am/AppStateManager$AppState$RunningProcess;->getPackageName()Ljava/lang/String;
(( com.android.server.am.AppStateManager$AppState$RunningProcess ) p1 ).getProcessName ( ); // invoke-virtual {p1}, Lcom/android/server/am/AppStateManager$AppState$RunningProcess;->getProcessName()Ljava/lang/String;
/* .line 909 */
v0 = (( com.miui.server.smartpower.SmartPowerPolicyManager ) p0 ).isProcessHibernationWhiteList ( v0, v1, v2, v3 ); // invoke-virtual {p0, v0, v1, v2, v3}, Lcom/miui/server/smartpower/SmartPowerPolicyManager;->isProcessHibernationWhiteList(IILjava/lang/String;Ljava/lang/String;)Z
} // .end method
public Boolean isProcessWhiteList ( Integer p0, java.lang.String p1 ) {
/* .locals 5 */
/* .param p1, "flag" # I */
/* .param p2, "procName" # Ljava/lang/String; */
/* .line 1027 */
v0 = this.mProcessWhiteList;
/* monitor-enter v0 */
/* .line 1028 */
try { // :try_start_0
v1 = this.mProcessWhiteList;
(( android.util.ArrayMap ) v1 ).get ( p2 ); // invoke-virtual {v1, p2}, Landroid/util/ArrayMap;->get(Ljava/lang/Object;)Ljava/lang/Object;
/* check-cast v1, Lcom/miui/server/smartpower/SmartPowerPolicyManager$WhiteListItem; */
/* .line 1029 */
/* .local v1, "item":Lcom/miui/server/smartpower/SmartPowerPolicyManager$WhiteListItem; */
int v2 = 1; // const/4 v2, 0x1
if ( v1 != null) { // if-eqz v1, :cond_0
v3 = com.miui.server.smartpower.SmartPowerPolicyManager$WhiteListItem .-$$Nest$fgetmTypes ( v1 );
/* and-int/2addr v3, p1 */
if ( v3 != null) { // if-eqz v3, :cond_0
/* .line 1030 */
/* monitor-exit v0 */
/* .line 1032 */
} // .end local v1 # "item":Lcom/miui/server/smartpower/SmartPowerPolicyManager$WhiteListItem;
} // :cond_0
/* monitor-exit v0 */
/* :try_end_0 */
/* .catchall {:try_start_0 ..:try_end_0} :catchall_1 */
/* .line 1033 */
v1 = this.mPidWhiteList;
/* monitor-enter v1 */
/* .line 1034 */
try { // :try_start_1
v0 = this.mPidWhiteList;
(( android.util.ArrayMap ) v0 ).values ( ); // invoke-virtual {v0}, Landroid/util/ArrayMap;->values()Ljava/util/Collection;
v3 = } // :goto_0
if ( v3 != null) { // if-eqz v3, :cond_2
/* check-cast v3, Lcom/miui/server/smartpower/SmartPowerPolicyManager$WhiteListItem; */
/* .line 1035 */
/* .local v3, "item":Lcom/miui/server/smartpower/SmartPowerPolicyManager$WhiteListItem; */
com.miui.server.smartpower.SmartPowerPolicyManager$WhiteListItem .-$$Nest$fgetmName ( v3 );
v4 = (( java.lang.String ) v4 ).equals ( p2 ); // invoke-virtual {v4, p2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z
if ( v4 != null) { // if-eqz v4, :cond_1
/* .line 1036 */
v0 = (( com.miui.server.smartpower.SmartPowerPolicyManager$WhiteListItem ) v3 ).hasWhiteListType ( p1 ); // invoke-virtual {v3, p1}, Lcom/miui/server/smartpower/SmartPowerPolicyManager$WhiteListItem;->hasWhiteListType(I)Z
if ( v0 != null) { // if-eqz v0, :cond_2
/* .line 1037 */
/* monitor-exit v1 */
/* .line 1041 */
} // .end local v3 # "item":Lcom/miui/server/smartpower/SmartPowerPolicyManager$WhiteListItem;
} // :cond_1
/* .line 1042 */
} // :cond_2
/* monitor-exit v1 */
/* .line 1043 */
int v0 = 0; // const/4 v0, 0x0
/* .line 1042 */
/* :catchall_0 */
/* move-exception v0 */
/* monitor-exit v1 */
/* :try_end_1 */
/* .catchall {:try_start_1 ..:try_end_1} :catchall_0 */
/* throw v0 */
/* .line 1032 */
/* :catchall_1 */
/* move-exception v1 */
try { // :try_start_2
/* monitor-exit v0 */
/* :try_end_2 */
/* .catchall {:try_start_2 ..:try_end_2} :catchall_1 */
/* throw v1 */
} // .end method
public Boolean isProcessWhiteList ( Integer p0, java.lang.String p1, java.lang.String p2 ) {
/* .locals 1 */
/* .param p1, "flag" # I */
/* .param p2, "packageName" # Ljava/lang/String; */
/* .param p3, "procName" # Ljava/lang/String; */
/* .line 1047 */
v0 = (( com.miui.server.smartpower.SmartPowerPolicyManager ) p0 ).isPackageWhiteList ( p1, p2 ); // invoke-virtual {p0, p1, p2}, Lcom/miui/server/smartpower/SmartPowerPolicyManager;->isPackageWhiteList(ILjava/lang/String;)Z
if ( v0 != null) { // if-eqz v0, :cond_0
/* .line 1048 */
int v0 = 1; // const/4 v0, 0x1
/* .line 1050 */
} // :cond_0
v0 = (( com.miui.server.smartpower.SmartPowerPolicyManager ) p0 ).isProcessWhiteList ( p1, p3 ); // invoke-virtual {p0, p1, p3}, Lcom/miui/server/smartpower/SmartPowerPolicyManager;->isProcessWhiteList(ILjava/lang/String;)Z
} // .end method
public Boolean isProviderPackageWhiteList ( java.lang.String p0 ) {
/* .locals 3 */
/* .param p1, "pkgName" # Ljava/lang/String; */
/* .line 1221 */
v0 = this.mPackageWhiteList;
/* monitor-enter v0 */
/* .line 1222 */
try { // :try_start_0
v1 = this.mPackageWhiteList;
(( android.util.ArrayMap ) v1 ).get ( p1 ); // invoke-virtual {v1, p1}, Landroid/util/ArrayMap;->get(Ljava/lang/Object;)Ljava/lang/Object;
/* check-cast v1, Lcom/miui/server/smartpower/SmartPowerPolicyManager$WhiteListItem; */
/* .line 1223 */
/* .local v1, "item":Lcom/miui/server/smartpower/SmartPowerPolicyManager$WhiteListItem; */
if ( v1 != null) { // if-eqz v1, :cond_0
/* const/16 v2, 0x20 */
v2 = (( com.miui.server.smartpower.SmartPowerPolicyManager$WhiteListItem ) v1 ).hasAction ( v2 ); // invoke-virtual {v1, v2}, Lcom/miui/server/smartpower/SmartPowerPolicyManager$WhiteListItem;->hasAction(I)Z
if ( v2 != null) { // if-eqz v2, :cond_0
/* .line 1224 */
/* monitor-exit v0 */
int v0 = 1; // const/4 v0, 0x1
/* .line 1226 */
} // .end local v1 # "item":Lcom/miui/server/smartpower/SmartPowerPolicyManager$WhiteListItem;
} // :cond_0
/* monitor-exit v0 */
/* .line 1227 */
int v0 = 0; // const/4 v0, 0x0
/* .line 1226 */
/* :catchall_0 */
/* move-exception v1 */
/* monitor-exit v0 */
/* :try_end_0 */
/* .catchall {:try_start_0 ..:try_end_0} :catchall_0 */
/* throw v1 */
} // .end method
public Boolean isProviderPkgBlackList ( java.lang.String p0 ) {
/* .locals 2 */
/* .param p1, "pkgName" # Ljava/lang/String; */
/* .line 1318 */
v0 = this.mProviderPkgBlackList;
/* monitor-enter v0 */
/* .line 1319 */
try { // :try_start_0
v1 = this.mProviderPkgBlackList;
v1 = (( android.util.ArraySet ) v1 ).contains ( p1 ); // invoke-virtual {v1, p1}, Landroid/util/ArraySet;->contains(Ljava/lang/Object;)Z
/* monitor-exit v0 */
/* .line 1320 */
/* :catchall_0 */
/* move-exception v1 */
/* monitor-exit v0 */
/* :try_end_0 */
/* .catchall {:try_start_0 ..:try_end_0} :catchall_0 */
/* throw v1 */
} // .end method
public Boolean isScreenOff ( ) {
/* .locals 1 */
/* .line 683 */
/* iget-boolean v0, p0, Lcom/miui/server/smartpower/SmartPowerPolicyManager;->mScreenOff:Z */
} // .end method
public Boolean isServicePackageWhiteList ( java.lang.String p0 ) {
/* .locals 3 */
/* .param p1, "pkgName" # Ljava/lang/String; */
/* .line 1204 */
v0 = this.mPackageWhiteList;
/* monitor-enter v0 */
/* .line 1205 */
try { // :try_start_0
v1 = this.mPackageWhiteList;
(( android.util.ArrayMap ) v1 ).get ( p1 ); // invoke-virtual {v1, p1}, Landroid/util/ArrayMap;->get(Ljava/lang/Object;)Ljava/lang/Object;
/* check-cast v1, Lcom/miui/server/smartpower/SmartPowerPolicyManager$WhiteListItem; */
/* .line 1206 */
/* .local v1, "item":Lcom/miui/server/smartpower/SmartPowerPolicyManager$WhiteListItem; */
if ( v1 != null) { // if-eqz v1, :cond_0
/* const/16 v2, 0x10 */
v2 = (( com.miui.server.smartpower.SmartPowerPolicyManager$WhiteListItem ) v1 ).hasAction ( v2 ); // invoke-virtual {v1, v2}, Lcom/miui/server/smartpower/SmartPowerPolicyManager$WhiteListItem;->hasAction(I)Z
if ( v2 != null) { // if-eqz v2, :cond_0
/* .line 1207 */
/* monitor-exit v0 */
int v0 = 1; // const/4 v0, 0x1
/* .line 1209 */
} // .end local v1 # "item":Lcom/miui/server/smartpower/SmartPowerPolicyManager$WhiteListItem;
} // :cond_0
/* monitor-exit v0 */
/* .line 1210 */
int v0 = 0; // const/4 v0, 0x0
/* .line 1209 */
/* :catchall_0 */
/* move-exception v1 */
/* monitor-exit v0 */
/* :try_end_0 */
/* .catchall {:try_start_0 ..:try_end_0} :catchall_0 */
/* throw v1 */
} // .end method
public Boolean isServicePkgBlackList ( java.lang.String p0 ) {
/* .locals 2 */
/* .param p1, "pkgName" # Ljava/lang/String; */
/* .line 1330 */
v0 = this.mServicePkgBlackList;
/* monitor-enter v0 */
/* .line 1331 */
try { // :try_start_0
v1 = this.mServicePkgBlackList;
v1 = (( android.util.ArraySet ) v1 ).contains ( p1 ); // invoke-virtual {v1, p1}, Landroid/util/ArraySet;->contains(Ljava/lang/Object;)Z
/* monitor-exit v0 */
/* .line 1332 */
/* :catchall_0 */
/* move-exception v1 */
/* monitor-exit v0 */
/* :try_end_0 */
/* .catchall {:try_start_0 ..:try_end_0} :catchall_0 */
/* throw v1 */
} // .end method
public Boolean needCheckNet ( java.lang.String p0, Integer p1 ) {
/* .locals 3 */
/* .param p1, "packageName" # Ljava/lang/String; */
/* .param p2, "uid" # I */
/* .line 796 */
(( com.miui.server.smartpower.SmartPowerPolicyManager ) p0 ).getUsageStatsInfo ( p1 ); // invoke-virtual {p0, p1}, Lcom/miui/server/smartpower/SmartPowerPolicyManager;->getUsageStatsInfo(Ljava/lang/String;)Lcom/miui/server/smartpower/SmartPowerPolicyManager$UsageStatsInfo;
/* .line 797 */
/* .local v0, "info":Lcom/miui/server/smartpower/SmartPowerPolicyManager$UsageStatsInfo; */
int v1 = 0; // const/4 v1, 0x0
if ( v0 != null) { // if-eqz v0, :cond_1
/* .line 798 */
v2 = com.miui.server.smartpower.SmartPowerPolicyManager$UsageStatsInfo .-$$Nest$fgetmAppLaunchCount ( v0 );
/* if-lez v2, :cond_0 */
int v1 = 1; // const/4 v1, 0x1
} // :cond_0
/* .line 800 */
} // :cond_1
} // .end method
public void onAppStateChanged ( com.android.server.am.AppStateManager$AppState p0, Integer p1, Integer p2, Boolean p3, Boolean p4, java.lang.String p5 ) {
/* .locals 5 */
/* .param p1, "app" # Lcom/android/server/am/AppStateManager$AppState; */
/* .param p2, "oldState" # I */
/* .param p3, "newState" # I */
/* .param p4, "oldProtectState" # Z */
/* .param p5, "newProtectState" # Z */
/* .param p6, "reason" # Ljava/lang/String; */
/* .line 732 */
final String v0 = "onAppStateChanged"; // const-string v0, "onAppStateChanged"
/* const-wide/32 v1, 0x20000 */
android.os.Trace .traceBegin ( v1,v2,v0 );
/* .line 733 */
/* if-nez p5, :cond_0 */
int v0 = 5; // const/4 v0, 0x5
/* if-lt p3, v0, :cond_0 */
/* if-ge p2, v0, :cond_0 */
/* .line 735 */
/* invoke-direct {p0, p1}, Lcom/miui/server/smartpower/SmartPowerPolicyManager;->idleApp(Lcom/android/server/am/AppStateManager$AppState;)V */
/* .line 738 */
} // :cond_0
int v0 = 6; // const/4 v0, 0x6
/* if-nez p5, :cond_1 */
/* if-lt p3, v0, :cond_1 */
/* if-ge p2, v0, :cond_1 */
/* .line 740 */
/* invoke-direct {p0, p1, p6}, Lcom/miui/server/smartpower/SmartPowerPolicyManager;->hibernationApp(Lcom/android/server/am/AppStateManager$AppState;Ljava/lang/String;)V */
/* .line 743 */
} // :cond_1
/* if-lt p3, v0, :cond_2 */
if ( p5 != null) { // if-eqz p5, :cond_3
} // :cond_2
/* if-ne p2, v0, :cond_3 */
/* if-nez p4, :cond_3 */
/* .line 745 */
/* invoke-direct {p0, p1, p6}, Lcom/miui/server/smartpower/SmartPowerPolicyManager;->activeApp(Lcom/android/server/am/AppStateManager$AppState;Ljava/lang/String;)V */
/* .line 748 */
} // :cond_3
/* if-nez p3, :cond_4 */
/* .line 749 */
v0 = this.mUidWhiteList;
/* monitor-enter v0 */
/* .line 750 */
try { // :try_start_0
v3 = this.mUidWhiteList;
v4 = (( com.android.server.am.AppStateManager$AppState ) p1 ).getUid ( ); // invoke-virtual {p1}, Lcom/android/server/am/AppStateManager$AppState;->getUid()I
java.lang.Integer .valueOf ( v4 );
(( android.util.ArrayMap ) v3 ).remove ( v4 ); // invoke-virtual {v3, v4}, Landroid/util/ArrayMap;->remove(Ljava/lang/Object;)Ljava/lang/Object;
/* .line 751 */
/* monitor-exit v0 */
/* :catchall_0 */
/* move-exception v1 */
/* monitor-exit v0 */
/* :try_end_0 */
/* .catchall {:try_start_0 ..:try_end_0} :catchall_0 */
/* throw v1 */
/* .line 753 */
} // :cond_4
} // :goto_0
android.os.Trace .traceEnd ( v1,v2 );
/* .line 754 */
return;
} // .end method
public void onBackupChanged ( Boolean p0, Integer p1, java.lang.String p2 ) {
/* .locals 1 */
/* .param p1, "active" # Z */
/* .param p2, "uid" # I */
/* .param p3, "packageName" # Ljava/lang/String; */
/* .line 325 */
/* const/16 v0, 0x40 */
if ( p1 != null) { // if-eqz p1, :cond_0
/* .line 326 */
/* invoke-direct {p0, p2, p3, v0}, Lcom/miui/server/smartpower/SmartPowerPolicyManager;->addUidWhiteList(ILjava/lang/String;I)V */
/* .line 328 */
} // :cond_0
/* invoke-direct {p0, p2, p3, v0}, Lcom/miui/server/smartpower/SmartPowerPolicyManager;->removeUidWhiteList(ILjava/lang/String;I)V */
/* .line 330 */
} // :goto_0
return;
} // .end method
public void onDependChanged ( Boolean p0, java.lang.String p1, java.lang.String p2, Integer p3, Integer p4 ) {
/* .locals 5 */
/* .param p1, "isConnected" # Z */
/* .param p2, "processName" # Ljava/lang/String; */
/* .param p3, "dependProcName" # Ljava/lang/String; */
/* .param p4, "uid" # I */
/* .param p5, "pid" # I */
/* .line 336 */
v0 = this.mDependencyMap;
/* monitor-enter v0 */
/* .line 337 */
try { // :try_start_0
v1 = this.mDependencyMap;
java.lang.Integer .valueOf ( p5 );
(( android.util.ArrayMap ) v1 ).get ( v2 ); // invoke-virtual {v1, v2}, Landroid/util/ArrayMap;->get(Ljava/lang/Object;)Ljava/lang/Object;
/* check-cast v1, Landroid/util/ArraySet; */
/* .line 338 */
/* .local v1, "dependList":Landroid/util/ArraySet;, "Landroid/util/ArraySet<Ljava/lang/String;>;" */
/* const/16 v2, 0x100 */
if ( p1 != null) { // if-eqz p1, :cond_1
/* .line 339 */
/* if-nez v1, :cond_0 */
/* .line 340 */
/* new-instance v3, Landroid/util/ArraySet; */
/* invoke-direct {v3}, Landroid/util/ArraySet;-><init>()V */
/* move-object v1, v3 */
/* .line 341 */
v3 = this.mDependencyMap;
java.lang.Integer .valueOf ( p5 );
(( android.util.ArrayMap ) v3 ).put ( v4, v1 ); // invoke-virtual {v3, v4, v1}, Landroid/util/ArrayMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;
/* .line 342 */
/* invoke-direct {p0, p4, p5, p3, v2}, Lcom/miui/server/smartpower/SmartPowerPolicyManager;->addPidWhiteList(IILjava/lang/String;I)V */
/* .line 344 */
} // :cond_0
(( android.util.ArraySet ) v1 ).add ( p2 ); // invoke-virtual {v1, p2}, Landroid/util/ArraySet;->add(Ljava/lang/Object;)Z
/* .line 346 */
} // :cond_1
if ( v1 != null) { // if-eqz v1, :cond_2
/* .line 347 */
(( android.util.ArraySet ) v1 ).remove ( p2 ); // invoke-virtual {v1, p2}, Landroid/util/ArraySet;->remove(Ljava/lang/Object;)Z
/* .line 348 */
v3 = (( android.util.ArraySet ) v1 ).size ( ); // invoke-virtual {v1}, Landroid/util/ArraySet;->size()I
/* if-nez v3, :cond_2 */
/* .line 349 */
v3 = this.mDependencyMap;
java.lang.Integer .valueOf ( p5 );
(( android.util.ArrayMap ) v3 ).remove ( v4 ); // invoke-virtual {v3, v4}, Landroid/util/ArrayMap;->remove(Ljava/lang/Object;)Ljava/lang/Object;
/* .line 350 */
/* invoke-direct {p0, p4, p5, p3, v2}, Lcom/miui/server/smartpower/SmartPowerPolicyManager;->removePidWhiteList(IILjava/lang/String;I)V */
/* .line 355 */
} // .end local v1 # "dependList":Landroid/util/ArraySet;, "Landroid/util/ArraySet<Ljava/lang/String;>;"
} // :cond_2
} // :goto_0
/* monitor-exit v0 */
/* .line 356 */
return;
/* .line 355 */
/* :catchall_0 */
/* move-exception v1 */
/* monitor-exit v0 */
/* :try_end_0 */
/* .catchall {:try_start_0 ..:try_end_0} :catchall_0 */
/* throw v1 */
} // .end method
public void onExternalAudioRegister ( Integer p0, Integer p1 ) {
/* .locals 3 */
/* .param p1, "uid" # I */
/* .param p2, "pid" # I */
/* .line 302 */
v0 = this.mAppStateManager;
(( com.android.server.am.AppStateManager ) v0 ).getRunningProcess ( p1, p2 ); // invoke-virtual {v0, p1, p2}, Lcom/android/server/am/AppStateManager;->getRunningProcess(II)Lcom/android/server/am/AppStateManager$AppState$RunningProcess;
/* .line 303 */
/* .local v0, "proc":Lcom/android/server/am/AppStateManager$AppState$RunningProcess; */
if ( v0 != null) { // if-eqz v0, :cond_0
/* .line 304 */
(( com.android.server.am.AppStateManager$AppState$RunningProcess ) v0 ).getProcessName ( ); // invoke-virtual {v0}, Lcom/android/server/am/AppStateManager$AppState$RunningProcess;->getProcessName()Ljava/lang/String;
/* const/16 v2, 0x200 */
/* invoke-direct {p0, p1, p2, v1, v2}, Lcom/miui/server/smartpower/SmartPowerPolicyManager;->addPidWhiteList(IILjava/lang/String;I)V */
/* .line 306 */
} // :cond_0
return;
} // .end method
public void onProcessStateChanged ( com.android.server.am.AppStateManager$AppState$RunningProcess p0, Integer p1, Integer p2, java.lang.String p3 ) {
/* .locals 6 */
/* .param p1, "proc" # Lcom/android/server/am/AppStateManager$AppState$RunningProcess; */
/* .param p2, "oldState" # I */
/* .param p3, "newState" # I */
/* .param p4, "reason" # Ljava/lang/String; */
/* .line 689 */
final String v0 = "onProcessStateChanged"; // const-string v0, "onProcessStateChanged"
/* const-wide/32 v1, 0x20000 */
android.os.Trace .traceBegin ( v1,v2,v0 );
/* .line 690 */
int v0 = 6; // const/4 v0, 0x6
/* if-lt p3, v0, :cond_0 */
/* if-ge p2, v0, :cond_0 */
/* .line 692 */
/* invoke-direct {p0, p1}, Lcom/miui/server/smartpower/SmartPowerPolicyManager;->idleProcess(Lcom/android/server/am/AppStateManager$AppState$RunningProcess;)V */
/* .line 695 */
} // :cond_0
int v0 = 7; // const/4 v0, 0x7
/* if-lt p3, v0, :cond_1 */
/* if-ge p2, v0, :cond_1 */
/* .line 697 */
/* invoke-direct {p0, p1, p4}, Lcom/miui/server/smartpower/SmartPowerPolicyManager;->hibernationProcess(Lcom/android/server/am/AppStateManager$AppState$RunningProcess;Ljava/lang/String;)V */
/* .line 700 */
} // :cond_1
/* if-ge p3, v0, :cond_2 */
/* if-ne p2, v0, :cond_2 */
/* .line 702 */
/* invoke-direct {p0, p1, p4}, Lcom/miui/server/smartpower/SmartPowerPolicyManager;->activeProcess(Lcom/android/server/am/AppStateManager$AppState$RunningProcess;Ljava/lang/String;)V */
/* .line 705 */
} // :cond_2
/* if-gtz p2, :cond_3 */
/* if-lez p3, :cond_3 */
/* .line 707 */
/* invoke-direct {p0, p1}, Lcom/miui/server/smartpower/SmartPowerPolicyManager;->lunchProcess(Lcom/android/server/am/AppStateManager$AppState$RunningProcess;)V */
/* .line 710 */
} // :cond_3
/* if-nez p3, :cond_4 */
/* .line 711 */
/* invoke-direct {p0, p1}, Lcom/miui/server/smartpower/SmartPowerPolicyManager;->diedProcess(Lcom/android/server/am/AppStateManager$AppState$RunningProcess;)V */
/* .line 714 */
} // :cond_4
/* if-nez p3, :cond_5 */
/* .line 715 */
int v0 = 0; // const/4 v0, 0x0
/* .line 716 */
/* .local v0, "item":Lcom/miui/server/smartpower/SmartPowerPolicyManager$WhiteListItem; */
v3 = this.mPidWhiteList;
/* monitor-enter v3 */
/* .line 717 */
try { // :try_start_0
v4 = this.mPidWhiteList;
v5 = (( com.android.server.am.AppStateManager$AppState$RunningProcess ) p1 ).getPid ( ); // invoke-virtual {p1}, Lcom/android/server/am/AppStateManager$AppState$RunningProcess;->getPid()I
java.lang.Integer .valueOf ( v5 );
(( android.util.ArrayMap ) v4 ).remove ( v5 ); // invoke-virtual {v4, v5}, Landroid/util/ArrayMap;->remove(Ljava/lang/Object;)Ljava/lang/Object;
/* check-cast v4, Lcom/miui/server/smartpower/SmartPowerPolicyManager$WhiteListItem; */
/* move-object v0, v4 */
/* .line 718 */
/* monitor-exit v3 */
/* :try_end_0 */
/* .catchall {:try_start_0 ..:try_end_0} :catchall_1 */
/* .line 719 */
if ( v0 != null) { // if-eqz v0, :cond_5
/* .line 720 */
v3 = this.mDependencyMap;
/* monitor-enter v3 */
/* .line 721 */
try { // :try_start_1
v4 = this.mDependencyMap;
v5 = com.miui.server.smartpower.SmartPowerPolicyManager$WhiteListItem .-$$Nest$fgetmPid ( v0 );
java.lang.Integer .valueOf ( v5 );
(( android.util.ArrayMap ) v4 ).remove ( v5 ); // invoke-virtual {v4, v5}, Landroid/util/ArrayMap;->remove(Ljava/lang/Object;)Ljava/lang/Object;
/* .line 722 */
/* monitor-exit v3 */
/* :catchall_0 */
/* move-exception v1 */
/* monitor-exit v3 */
/* :try_end_1 */
/* .catchall {:try_start_1 ..:try_end_1} :catchall_0 */
/* throw v1 */
/* .line 718 */
/* :catchall_1 */
/* move-exception v1 */
try { // :try_start_2
/* monitor-exit v3 */
/* :try_end_2 */
/* .catchall {:try_start_2 ..:try_end_2} :catchall_1 */
/* throw v1 */
/* .line 726 */
} // .end local v0 # "item":Lcom/miui/server/smartpower/SmartPowerPolicyManager$WhiteListItem;
} // :cond_5
} // :goto_0
android.os.Trace .traceEnd ( v1,v2 );
/* .line 727 */
return;
} // .end method
public void onUsbStateChanged ( Boolean p0, Boolean p1 ) {
/* .locals 0 */
/* .param p1, "isConnected" # Z */
/* .param p2, "isDataTransfer" # Z */
/* .line 332 */
return;
} // .end method
public void onVpnChanged ( Boolean p0, Integer p1, java.lang.String p2 ) {
/* .locals 1 */
/* .param p1, "active" # Z */
/* .param p2, "uid" # I */
/* .param p3, "packageName" # Ljava/lang/String; */
/* .line 317 */
/* const/16 v0, 0x8 */
if ( p1 != null) { // if-eqz p1, :cond_0
/* .line 318 */
/* invoke-direct {p0, p2, p3, v0}, Lcom/miui/server/smartpower/SmartPowerPolicyManager;->addUidWhiteList(ILjava/lang/String;I)V */
/* .line 320 */
} // :cond_0
/* invoke-direct {p0, p2, p3, v0}, Lcom/miui/server/smartpower/SmartPowerPolicyManager;->removeUidWhiteList(ILjava/lang/String;I)V */
/* .line 322 */
} // :goto_0
return;
} // .end method
public void onWallpaperComponentChangedLocked ( Boolean p0, Integer p1, java.lang.String p2 ) {
/* .locals 1 */
/* .param p1, "active" # Z */
/* .param p2, "uid" # I */
/* .param p3, "packageName" # Ljava/lang/String; */
/* .line 309 */
/* const/16 v0, 0x10 */
if ( p1 != null) { // if-eqz p1, :cond_0
/* .line 310 */
/* invoke-direct {p0, p2, p3, v0}, Lcom/miui/server/smartpower/SmartPowerPolicyManager;->addUidWhiteList(ILjava/lang/String;I)V */
/* .line 312 */
} // :cond_0
/* invoke-direct {p0, p2, p3, v0}, Lcom/miui/server/smartpower/SmartPowerPolicyManager;->removeUidWhiteList(ILjava/lang/String;I)V */
/* .line 314 */
} // :goto_0
return;
} // .end method
public Boolean shouldInterceptAlarmLocked ( Integer p0, Integer p1 ) {
/* .locals 4 */
/* .param p1, "uid" # I */
/* .param p2, "type" # I */
/* .line 565 */
v0 = /* invoke-direct {p0}, Lcom/miui/server/smartpower/SmartPowerPolicyManager;->isEnableIntercept()Z */
int v1 = 0; // const/4 v1, 0x0
/* if-nez v0, :cond_0 */
/* .line 566 */
/* .line 568 */
} // :cond_0
v0 = this.mAppStateManager;
(( com.android.server.am.AppStateManager ) v0 ).getAppState ( p1 ); // invoke-virtual {v0, p1}, Lcom/android/server/am/AppStateManager;->getAppState(I)Lcom/android/server/am/AppStateManager$AppState;
/* .line 569 */
/* .local v0, "appState":Lcom/android/server/am/AppStateManager$AppState; */
if ( v0 != null) { // if-eqz v0, :cond_6
/* .line 570 */
v2 = (( com.android.server.am.AppStateManager$AppState ) v0 ).isProcessPerceptible ( ); // invoke-virtual {v0}, Lcom/android/server/am/AppStateManager$AppState;->isProcessPerceptible()Z
/* if-nez v2, :cond_5 */
/* .line 571 */
(( com.android.server.am.AppStateManager$AppState ) v0 ).getPackageName ( ); // invoke-virtual {v0}, Lcom/android/server/am/AppStateManager$AppState;->getPackageName()Ljava/lang/String;
v2 = (( com.miui.server.smartpower.SmartPowerPolicyManager ) p0 ).isAppHibernationWhiteList ( p1, v2 ); // invoke-virtual {p0, p1, v2}, Lcom/miui/server/smartpower/SmartPowerPolicyManager;->isAppHibernationWhiteList(ILjava/lang/String;)Z
/* if-nez v2, :cond_5 */
/* .line 572 */
v2 = (( com.android.server.am.AppStateManager$AppState ) v0 ).isSystemApp ( ); // invoke-virtual {v0}, Lcom/android/server/am/AppStateManager$AppState;->isSystemApp()Z
if ( v2 != null) { // if-eqz v2, :cond_1
/* .line 575 */
} // :cond_1
v2 = com.miui.server.smartpower.SmartPowerPolicyManager .isSpeedTestMode ( );
int v3 = 1; // const/4 v3, 0x1
if ( v2 != null) { // if-eqz v2, :cond_2
/* .line 576 */
/* .line 578 */
} // :cond_2
(( com.android.server.am.AppStateManager$AppState ) v0 ).getPackageName ( ); // invoke-virtual {v0}, Lcom/android/server/am/AppStateManager$AppState;->getPackageName()Ljava/lang/String;
v2 = com.miui.server.smartpower.SmartPowerPolicyManager .isPackageInterceptList ( v2 );
if ( v2 != null) { // if-eqz v2, :cond_4
if ( p2 != null) { // if-eqz p2, :cond_4
int v2 = 2; // const/4 v2, 0x2
/* if-eq p2, v2, :cond_4 */
/* .line 581 */
(( com.android.server.am.AppStateManager$AppState ) v0 ).getPackageName ( ); // invoke-virtual {v0}, Lcom/android/server/am/AppStateManager$AppState;->getPackageName()Ljava/lang/String;
v2 = (( com.miui.server.smartpower.SmartPowerPolicyManager ) p0 ).isAlarmPackageWhiteList ( v2 ); // invoke-virtual {p0, v2}, Lcom/miui/server/smartpower/SmartPowerPolicyManager;->isAlarmPackageWhiteList(Ljava/lang/String;)Z
if ( v2 != null) { // if-eqz v2, :cond_3
/* .line 584 */
} // :cond_3
/* new-instance v1, Ljava/lang/StringBuilder; */
/* invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V */
final String v2 = "power alarm h:{ u:"; // const-string v2, "power alarm h:{ u:"
(( java.lang.StringBuilder ) v1 ).append ( v2 ); // invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v1 ).append ( p1 ); // invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;
final String v2 = " t:"; // const-string v2, " t:"
(( java.lang.StringBuilder ) v1 ).append ( v2 ); // invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v1 ).append ( p2 ); // invoke-virtual {v1, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;
/* const-string/jumbo v2, "}" */
(( java.lang.StringBuilder ) v1 ).append ( v2 ); // invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
/* .line 585 */
(( com.android.server.am.AppStateManager$AppState ) v0 ).getPackageName ( ); // invoke-virtual {v0}, Lcom/android/server/am/AppStateManager$AppState;->getPackageName()Ljava/lang/String;
(( java.lang.StringBuilder ) v1 ).append ( v2 ); // invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v1 ).toString ( ); // invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
/* .line 584 */
/* const v2, 0x15ff2 */
android.util.EventLog .writeEvent ( v2,v1 );
/* .line 586 */
/* .line 582 */
} // :cond_4
} // :goto_0
/* .line 573 */
} // :cond_5
} // :goto_1
/* .line 588 */
} // :cond_6
} // .end method
public Boolean shouldInterceptBroadcastLocked ( Integer p0, Integer p1, java.lang.String p2, com.android.server.am.ProcessRecord p3, android.content.Intent p4, Boolean p5, Boolean p6 ) {
/* .locals 14 */
/* .param p1, "callingUid" # I */
/* .param p2, "callingPid" # I */
/* .param p3, "callingPkg" # Ljava/lang/String; */
/* .param p4, "recProc" # Lcom/android/server/am/ProcessRecord; */
/* .param p5, "intent" # Landroid/content/Intent; */
/* .param p6, "ordered" # Z */
/* .param p7, "sticky" # Z */
/* .line 498 */
/* move-object v0, p0 */
/* move v1, p1 */
/* move-object/from16 v2, p4 */
v3 = /* invoke-direct {p0}, Lcom/miui/server/smartpower/SmartPowerPolicyManager;->isEnableIntercept()Z */
int v4 = 0; // const/4 v4, 0x0
if ( v3 != null) { // if-eqz v3, :cond_17
if ( v2 != null) { // if-eqz v2, :cond_17
/* .line 499 */
/* invoke-virtual/range {p5 ..p5}, Landroid/content/Intent;->getAction()Ljava/lang/String; */
v3 = (( com.miui.server.smartpower.SmartPowerPolicyManager ) p0 ).isBroadcastWhiteList ( v3 ); // invoke-virtual {p0, v3}, Lcom/miui/server/smartpower/SmartPowerPolicyManager;->isBroadcastWhiteList(Ljava/lang/String;)Z
/* if-nez v3, :cond_16 */
v3 = this.processName;
/* .line 500 */
/* invoke-virtual/range {p5 ..p5}, Landroid/content/Intent;->getAction()Ljava/lang/String; */
v3 = /* invoke-direct {p0, v3, v5}, Lcom/miui/server/smartpower/SmartPowerPolicyManager;->isBroadcastProcessWhiteList(Ljava/lang/String;Ljava/lang/String;)Z */
if ( v3 != null) { // if-eqz v3, :cond_0
/* move/from16 v10, p2 */
/* move-object/from16 v13, p3 */
/* goto/16 :goto_4 */
/* .line 504 */
} // :cond_0
v3 = this.mAppStateManager;
/* iget v5, v2, Lcom/android/server/am/ProcessRecord;->uid:I */
/* iget v6, v2, Lcom/android/server/am/ProcessRecord;->mPid:I */
/* .line 505 */
(( com.android.server.am.AppStateManager ) v3 ).getRunningProcess ( v5, v6 ); // invoke-virtual {v3, v5, v6}, Lcom/android/server/am/AppStateManager;->getRunningProcess(II)Lcom/android/server/am/AppStateManager$AppState$RunningProcess;
/* .line 507 */
/* .local v3, "recProcessState":Lcom/android/server/am/AppStateManager$AppState$RunningProcess; */
/* iget v5, v2, Lcom/android/server/am/ProcessRecord;->uid:I */
v5 = android.os.Process .isCoreUid ( v5 );
/* if-nez v5, :cond_15 */
if ( v3 != null) { // if-eqz v3, :cond_15
/* .line 508 */
v5 = (( com.android.server.am.AppStateManager$AppState$RunningProcess ) v3 ).isForeground ( ); // invoke-virtual {v3}, Lcom/android/server/am/AppStateManager$AppState$RunningProcess;->isForeground()Z
/* if-nez v5, :cond_14 */
v5 = (( com.android.server.am.AppStateManager$AppState$RunningProcess ) v3 ).canHibernate ( ); // invoke-virtual {v3}, Lcom/android/server/am/AppStateManager$AppState$RunningProcess;->canHibernate()Z
/* if-nez v5, :cond_1 */
/* move/from16 v10, p2 */
/* move-object/from16 v13, p3 */
/* goto/16 :goto_3 */
/* .line 513 */
} // :cond_1
v5 = this.mAppStateManager;
/* iget v6, v2, Lcom/android/server/am/ProcessRecord;->uid:I */
(( com.android.server.am.AppStateManager ) v5 ).getAppState ( v6 ); // invoke-virtual {v5, v6}, Lcom/android/server/am/AppStateManager;->getAppState(I)Lcom/android/server/am/AppStateManager$AppState;
/* .line 514 */
/* .local v5, "appState":Lcom/android/server/am/AppStateManager$AppState; */
if ( v5 != null) { // if-eqz v5, :cond_2
v6 = (( com.android.server.am.AppStateManager$AppState ) v5 ).isProcessPerceptible ( ); // invoke-virtual {v5}, Lcom/android/server/am/AppStateManager$AppState;->isProcessPerceptible()Z
if ( v6 != null) { // if-eqz v6, :cond_2
/* .line 515 */
/* .line 518 */
} // :cond_2
v6 = com.miui.server.smartpower.SmartPowerPolicyManager .isSpeedTestMode ( );
int v7 = 1; // const/4 v7, 0x1
if ( v6 != null) { // if-eqz v6, :cond_3
/* .line 519 */
/* .line 522 */
} // :cond_3
(( com.android.server.am.AppStateManager$AppState$RunningProcess ) v3 ).getPackageName ( ); // invoke-virtual {v3}, Lcom/android/server/am/AppStateManager$AppState$RunningProcess;->getPackageName()Ljava/lang/String;
v6 = com.miui.server.smartpower.SmartPowerPolicyManager .isPackageInterceptList ( v6 );
/* if-nez v6, :cond_4 */
/* .line 523 */
/* .line 526 */
} // :cond_4
/* invoke-virtual/range {p5 ..p5}, Landroid/content/Intent;->getAction()Ljava/lang/String; */
v6 = (( com.miui.server.smartpower.SmartPowerPolicyManager ) p0 ).isBroadcastBlackList ( v6 ); // invoke-virtual {p0, v6}, Lcom/miui/server/smartpower/SmartPowerPolicyManager;->isBroadcastBlackList(Ljava/lang/String;)Z
/* .line 528 */
/* .local v6, "isBlackList":Z */
v8 = (( com.android.server.am.AppStateManager$AppState$RunningProcess ) v3 ).isSystemApp ( ); // invoke-virtual {v3}, Lcom/android/server/am/AppStateManager$AppState$RunningProcess;->isSystemApp()Z
/* if-nez v8, :cond_12 */
v8 = (( com.android.server.am.AppStateManager$AppState$RunningProcess ) v3 ).isSystemSignature ( ); // invoke-virtual {v3}, Lcom/android/server/am/AppStateManager$AppState$RunningProcess;->isSystemSignature()Z
if ( v8 != null) { // if-eqz v8, :cond_5
/* move/from16 v10, p2 */
/* move-object/from16 v13, p3 */
/* goto/16 :goto_2 */
/* .line 531 */
} // :cond_5
if ( v6 != null) { // if-eqz v6, :cond_6
/* .line 532 */
/* .line 536 */
} // :cond_6
/* nop */
/* .line 537 */
/* invoke-virtual/range {p5 ..p5}, Landroid/content/Intent;->getAction()Ljava/lang/String; */
if ( v8 != null) { // if-eqz v8, :cond_7
/* invoke-virtual/range {p5 ..p5}, Landroid/content/Intent;->getAction()Ljava/lang/String; */
final String v9 = "APPWIDGET"; // const-string v9, "APPWIDGET"
v8 = (( java.lang.String ) v8 ).contains ( v9 ); // invoke-virtual {v8, v9}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z
if ( v8 != null) { // if-eqz v8, :cond_7
/* move v8, v7 */
} // :cond_7
/* move v8, v4 */
/* .line 538 */
/* .local v8, "isWidgetAction":Z */
} // :goto_0
/* iget v9, v2, Lcom/android/server/am/ProcessRecord;->uid:I */
/* if-eq v1, v9, :cond_8 */
if ( v8 != null) { // if-eqz v8, :cond_9
} // :cond_8
v9 = this.processName;
/* .line 539 */
final String v10 = ":widgetProvider"; // const-string v10, ":widgetProvider"
v9 = (( java.lang.String ) v9 ).endsWith ( v10 ); // invoke-virtual {v9, v10}, Ljava/lang/String;->endsWith(Ljava/lang/String;)Z
if ( v9 != null) { // if-eqz v9, :cond_9
/* .line 540 */
/* .line 543 */
} // :cond_9
v9 = this.mAppStateManager;
/* .line 544 */
/* move/from16 v10, p2 */
v9 = (( com.android.server.am.AppStateManager ) v9 ).isProcessPerceptible ( p1, v10 ); // invoke-virtual {v9, p1, v10}, Lcom/android/server/am/AppStateManager;->isProcessPerceptible(II)Z
/* .line 545 */
/* .local v9, "isPerceptibleCalling":Z */
/* invoke-virtual/range {p5 ..p5}, Landroid/content/Intent;->getComponent()Landroid/content/ComponentName; */
if ( v11 != null) { // if-eqz v11, :cond_a
/* move v11, v7 */
} // :cond_a
/* move v11, v4 */
/* .line 547 */
/* .local v11, "isExplicitBroadcast":Z */
} // :goto_1
v12 = (( com.android.server.am.AppStateManager$AppState$RunningProcess ) v3 ).isKilled ( ); // invoke-virtual {v3}, Lcom/android/server/am/AppStateManager$AppState$RunningProcess;->isKilled()Z
if ( v12 != null) { // if-eqz v12, :cond_c
/* .line 548 */
v12 = (( com.android.server.am.AppStateManager$AppState$RunningProcess ) v3 ).isAutoStartApp ( ); // invoke-virtual {v3}, Lcom/android/server/am/AppStateManager$AppState$RunningProcess;->isAutoStartApp()Z
/* if-nez v12, :cond_b */
/* if-nez v11, :cond_b */
/* if-nez v9, :cond_b */
/* move v4, v7 */
} // :cond_b
/* .line 552 */
} // :cond_c
v12 = (( com.android.server.am.AppStateManager$AppState$RunningProcess ) v3 ).isIdle ( ); // invoke-virtual {v3}, Lcom/android/server/am/AppStateManager$AppState$RunningProcess;->isIdle()Z
/* if-nez v12, :cond_f */
/* .line 553 */
/* if-nez p6, :cond_d */
if ( p7 != null) { // if-eqz p7, :cond_e
} // :cond_d
/* move v4, v7 */
} // :cond_e
/* .line 556 */
} // :cond_f
(( com.android.server.am.AppStateManager$AppState$RunningProcess ) v3 ).getPackageName ( ); // invoke-virtual {v3}, Lcom/android/server/am/AppStateManager$AppState$RunningProcess;->getPackageName()Ljava/lang/String;
/* move-object/from16 v13, p3 */
v12 = (( java.lang.String ) v12 ).equals ( v13 ); // invoke-virtual {v12, v13}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z
if ( v12 != null) { // if-eqz v12, :cond_10
/* if-nez v11, :cond_11 */
/* if-nez v9, :cond_11 */
} // :cond_10
/* move v4, v7 */
} // :cond_11
/* .line 528 */
} // .end local v8 # "isWidgetAction":Z
} // .end local v9 # "isPerceptibleCalling":Z
} // .end local v11 # "isExplicitBroadcast":Z
} // :cond_12
/* move/from16 v10, p2 */
/* move-object/from16 v13, p3 */
/* .line 529 */
} // :goto_2
if ( v6 != null) { // if-eqz v6, :cond_13
v8 = (( com.android.server.am.AppStateManager$AppState$RunningProcess ) v3 ).getAdj ( ); // invoke-virtual {v3}, Lcom/android/server/am/AppStateManager$AppState$RunningProcess;->getAdj()I
/* const/16 v9, 0xc8 */
/* if-le v8, v9, :cond_13 */
/* move v4, v7 */
} // :cond_13
/* .line 508 */
} // .end local v5 # "appState":Lcom/android/server/am/AppStateManager$AppState;
} // .end local v6 # "isBlackList":Z
} // :cond_14
/* move/from16 v10, p2 */
/* move-object/from16 v13, p3 */
/* .line 507 */
} // :cond_15
/* move/from16 v10, p2 */
/* move-object/from16 v13, p3 */
/* .line 509 */
} // :goto_3
/* .line 499 */
} // .end local v3 # "recProcessState":Lcom/android/server/am/AppStateManager$AppState$RunningProcess;
} // :cond_16
/* move/from16 v10, p2 */
/* move-object/from16 v13, p3 */
/* .line 498 */
} // :cond_17
/* move/from16 v10, p2 */
/* move-object/from16 v13, p3 */
/* .line 501 */
} // :goto_4
} // .end method
public Boolean shouldInterceptProviderLocked ( Integer p0, com.android.server.am.ProcessRecord p1, com.android.server.am.ProcessRecord p2, Boolean p3 ) {
/* .locals 5 */
/* .param p1, "callingUid" # I */
/* .param p2, "callingProc" # Lcom/android/server/am/ProcessRecord; */
/* .param p3, "hostingProc" # Lcom/android/server/am/ProcessRecord; */
/* .param p4, "isRunning" # Z */
/* .line 439 */
int v0 = 0; // const/4 v0, 0x0
if ( p4 != null) { // if-eqz p4, :cond_9
v1 = /* invoke-direct {p0}, Lcom/miui/server/smartpower/SmartPowerPolicyManager;->isEnableIntercept()Z */
if ( v1 != null) { // if-eqz v1, :cond_9
/* .line 440 */
v1 = this.mAppStateManager;
/* iget v2, p3, Lcom/android/server/am/ProcessRecord;->uid:I */
/* iget v3, p3, Lcom/android/server/am/ProcessRecord;->mPid:I */
/* .line 441 */
(( com.android.server.am.AppStateManager ) v1 ).getRunningProcess ( v2, v3 ); // invoke-virtual {v1, v2, v3}, Lcom/android/server/am/AppStateManager;->getRunningProcess(II)Lcom/android/server/am/AppStateManager$AppState$RunningProcess;
/* .line 443 */
/* .local v1, "hostingProcessState":Lcom/android/server/am/AppStateManager$AppState$RunningProcess; */
if ( v1 != null) { // if-eqz v1, :cond_8
/* .line 444 */
(( com.android.server.am.AppStateManager$AppState$RunningProcess ) v1 ).getPackageName ( ); // invoke-virtual {v1}, Lcom/android/server/am/AppStateManager$AppState$RunningProcess;->getPackageName()Ljava/lang/String;
v2 = com.miui.server.smartpower.SmartPowerPolicyManager .isPackageInterceptList ( v2 );
if ( v2 != null) { // if-eqz v2, :cond_8
/* .line 445 */
(( com.android.server.am.AppStateManager$AppState$RunningProcess ) v1 ).getPackageName ( ); // invoke-virtual {v1}, Lcom/android/server/am/AppStateManager$AppState$RunningProcess;->getPackageName()Ljava/lang/String;
v2 = (( com.miui.server.smartpower.SmartPowerPolicyManager ) p0 ).isProviderPkgBlackList ( v2 ); // invoke-virtual {p0, v2}, Lcom/miui/server/smartpower/SmartPowerPolicyManager;->isProviderPkgBlackList(Ljava/lang/String;)Z
if ( v2 != null) { // if-eqz v2, :cond_0
/* .line 446 */
v2 = (( com.android.server.am.AppStateManager$AppState$RunningProcess ) v1 ).canHibernate ( ); // invoke-virtual {v1}, Lcom/android/server/am/AppStateManager$AppState$RunningProcess;->canHibernate()Z
/* if-nez v2, :cond_1 */
/* goto/16 :goto_3 */
} // :cond_0
v2 = (( com.android.server.am.AppStateManager$AppState$RunningProcess ) v1 ).isIdle ( ); // invoke-virtual {v1}, Lcom/android/server/am/AppStateManager$AppState$RunningProcess;->isIdle()Z
if ( v2 != null) { // if-eqz v2, :cond_8
/* .line 447 */
} // :cond_1
v2 = (( com.android.server.am.AppStateManager$AppState$RunningProcess ) v1 ).isSystemApp ( ); // invoke-virtual {v1}, Lcom/android/server/am/AppStateManager$AppState$RunningProcess;->isSystemApp()Z
/* if-nez v2, :cond_8 */
/* .line 448 */
v2 = (( com.android.server.am.AppStateManager$AppState$RunningProcess ) v1 ).isSystemSignature ( ); // invoke-virtual {v1}, Lcom/android/server/am/AppStateManager$AppState$RunningProcess;->isSystemSignature()Z
/* if-nez v2, :cond_8 */
/* .line 449 */
(( com.android.server.am.AppStateManager$AppState$RunningProcess ) v1 ).getPackageName ( ); // invoke-virtual {v1}, Lcom/android/server/am/AppStateManager$AppState$RunningProcess;->getPackageName()Ljava/lang/String;
v2 = (( com.miui.server.smartpower.SmartPowerPolicyManager ) p0 ).isProviderPackageWhiteList ( v2 ); // invoke-virtual {p0, v2}, Lcom/miui/server/smartpower/SmartPowerPolicyManager;->isProviderPackageWhiteList(Ljava/lang/String;)Z
if ( v2 != null) { // if-eqz v2, :cond_2
/* goto/16 :goto_3 */
/* .line 453 */
} // :cond_2
if ( p2 != null) { // if-eqz p2, :cond_5
/* .line 454 */
v2 = this.mAppStateManager;
/* iget v3, p2, Lcom/android/server/am/ProcessRecord;->uid:I */
/* iget v4, p2, Lcom/android/server/am/ProcessRecord;->mPid:I */
/* .line 455 */
(( com.android.server.am.AppStateManager ) v2 ).getRunningProcess ( v3, v4 ); // invoke-virtual {v2, v3, v4}, Lcom/android/server/am/AppStateManager;->getRunningProcess(II)Lcom/android/server/am/AppStateManager$AppState$RunningProcess;
/* .line 456 */
/* .local v2, "callingProcessState":Lcom/android/server/am/AppStateManager$AppState$RunningProcess; */
if ( v2 != null) { // if-eqz v2, :cond_4
/* .line 457 */
v3 = (( com.android.server.am.AppStateManager$AppState$RunningProcess ) v2 ).isSystemApp ( ); // invoke-virtual {v2}, Lcom/android/server/am/AppStateManager$AppState$RunningProcess;->isSystemApp()Z
/* if-nez v3, :cond_4 */
/* iget v3, p2, Lcom/android/server/am/ProcessRecord;->mPid:I */
/* iget v4, p3, Lcom/android/server/am/ProcessRecord;->mPid:I */
/* if-eq v3, v4, :cond_4 */
/* .line 459 */
v3 = (( com.android.server.am.AppStateManager$AppState$RunningProcess ) v2 ).canHibernate ( ); // invoke-virtual {v2}, Lcom/android/server/am/AppStateManager$AppState$RunningProcess;->canHibernate()Z
/* if-nez v3, :cond_3 */
/* .line 462 */
} // .end local v2 # "callingProcessState":Lcom/android/server/am/AppStateManager$AppState$RunningProcess;
} // :cond_3
/* .line 460 */
/* .restart local v2 # "callingProcessState":Lcom/android/server/am/AppStateManager$AppState$RunningProcess; */
} // :cond_4
} // :goto_0
/* .line 463 */
} // .end local v2 # "callingProcessState":Lcom/android/server/am/AppStateManager$AppState$RunningProcess;
} // :cond_5
v2 = this.mAppStateManager;
(( com.android.server.am.AppStateManager ) v2 ).getAppState ( p1 ); // invoke-virtual {v2, p1}, Lcom/android/server/am/AppStateManager;->getAppState(I)Lcom/android/server/am/AppStateManager$AppState;
/* .line 464 */
/* .local v2, "callingAppState":Lcom/android/server/am/AppStateManager$AppState; */
if ( v2 != null) { // if-eqz v2, :cond_7
v3 = (( com.android.server.am.AppStateManager$AppState ) v2 ).isVsible ( ); // invoke-virtual {v2}, Lcom/android/server/am/AppStateManager$AppState;->isVsible()Z
/* if-nez v3, :cond_7 */
/* .line 465 */
v3 = (( com.miui.server.smartpower.SmartPowerPolicyManager ) p0 ).isAppHibernationWhiteList ( v2 ); // invoke-virtual {p0, v2}, Lcom/miui/server/smartpower/SmartPowerPolicyManager;->isAppHibernationWhiteList(Lcom/android/server/am/AppStateManager$AppState;)Z
if ( v3 != null) { // if-eqz v3, :cond_6
/* .line 469 */
} // .end local v2 # "callingAppState":Lcom/android/server/am/AppStateManager$AppState;
} // :cond_6
} // :goto_1
/* new-instance v0, Ljava/lang/StringBuilder; */
/* invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V */
final String v2 = "power prv h:{ u:"; // const-string v2, "power prv h:{ u:"
(( java.lang.StringBuilder ) v0 ).append ( v2 ); // invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
/* iget v2, p3, Lcom/android/server/am/ProcessRecord;->uid:I */
(( java.lang.StringBuilder ) v0 ).append ( v2 ); // invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;
final String v2 = " p:"; // const-string v2, " p:"
(( java.lang.StringBuilder ) v0 ).append ( v2 ); // invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
v2 = this.processName;
(( java.lang.StringBuilder ) v0 ).append ( v2 ); // invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
final String v2 = " s:"; // const-string v2, " s:"
(( java.lang.StringBuilder ) v0 ).append ( v2 ); // invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
/* .line 471 */
v2 = (( com.android.server.am.AppStateManager$AppState$RunningProcess ) v1 ).getCurrentState ( ); // invoke-virtual {v1}, Lcom/android/server/am/AppStateManager$AppState$RunningProcess;->getCurrentState()I
(( java.lang.StringBuilder ) v0 ).append ( v2 ); // invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;
/* const-string/jumbo v2, "} c:" */
(( java.lang.StringBuilder ) v0 ).append ( v2 ); // invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v0 ).append ( p1 ); // invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v0 ).toString ( ); // invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
/* .line 469 */
/* const v2, 0x15ff2 */
android.util.EventLog .writeEvent ( v2,v0 );
/* .line 473 */
int v0 = 1; // const/4 v0, 0x1
/* .line 466 */
/* .restart local v2 # "callingAppState":Lcom/android/server/am/AppStateManager$AppState; */
} // :cond_7
} // :goto_2
/* .line 450 */
} // .end local v2 # "callingAppState":Lcom/android/server/am/AppStateManager$AppState;
} // :cond_8
} // :goto_3
/* .line 475 */
} // .end local v1 # "hostingProcessState":Lcom/android/server/am/AppStateManager$AppState$RunningProcess;
} // :cond_9
} // .end method
public Boolean shouldInterceptService ( android.content.Intent p0, miui.security.CallerInfo p1, android.content.pm.ServiceInfo p2 ) {
/* .locals 12 */
/* .param p1, "service" # Landroid/content/Intent; */
/* .param p2, "callingInfo" # Lmiui/security/CallerInfo; */
/* .param p3, "serviceInfo" # Landroid/content/pm/ServiceInfo; */
/* .line 629 */
v0 = /* invoke-direct {p0}, Lcom/miui/server/smartpower/SmartPowerPolicyManager;->isEnableIntercept()Z */
int v1 = 0; // const/4 v1, 0x0
if ( v0 != null) { // if-eqz v0, :cond_7
v0 = this.applicationInfo;
v0 = this.packageName;
/* .line 630 */
v0 = com.miui.server.smartpower.SmartPowerPolicyManager .isPackageInterceptList ( v0 );
/* if-nez v0, :cond_0 */
/* goto/16 :goto_3 */
/* .line 633 */
} // :cond_0
v0 = this.mAppStateManager;
/* iget v2, p2, Lmiui/security/CallerInfo;->callerUid:I */
/* iget v3, p2, Lmiui/security/CallerInfo;->callerPid:I */
/* .line 634 */
(( com.android.server.am.AppStateManager ) v0 ).getRunningProcess ( v2, v3 ); // invoke-virtual {v0, v2, v3}, Lcom/android/server/am/AppStateManager;->getRunningProcess(II)Lcom/android/server/am/AppStateManager$AppState$RunningProcess;
/* .line 635 */
/* .local v0, "callingProcessState":Lcom/android/server/am/AppStateManager$AppState$RunningProcess; */
v2 = this.applicationInfo;
/* iget v2, v2, Landroid/content/pm/ApplicationInfo;->uid:I */
/* .line 636 */
/* .local v2, "serviceUid":I */
v3 = this.applicationInfo;
v3 = this.packageName;
/* .line 637 */
/* .local v3, "servicePckName":Ljava/lang/String; */
v4 = this.processName;
/* .line 638 */
/* .local v4, "serviceProcName":Ljava/lang/String; */
v5 = this.callerProcessName;
v5 = (( java.lang.String ) v4 ).equals ( v5 ); // invoke-virtual {v4, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z
/* .line 639 */
/* .local v5, "isSelfProc":Z */
int v6 = 1; // const/4 v6, 0x1
if ( v5 != null) { // if-eqz v5, :cond_1
final String v7 = ":widgetProvider"; // const-string v7, ":widgetProvider"
v7 = (( java.lang.String ) v4 ).endsWith ( v7 ); // invoke-virtual {v4, v7}, Ljava/lang/String;->endsWith(Ljava/lang/String;)Z
if ( v7 != null) { // if-eqz v7, :cond_1
/* move v7, v6 */
} // :cond_1
/* move v7, v1 */
/* .line 640 */
/* .local v7, "isWidgetSelf":Z */
} // :goto_0
if ( v0 != null) { // if-eqz v0, :cond_6
/* .line 641 */
if ( v5 != null) { // if-eqz v5, :cond_2
v8 = (( com.android.server.am.AppStateManager$AppState$RunningProcess ) v0 ).isSystemSignature ( ); // invoke-virtual {v0}, Lcom/android/server/am/AppStateManager$AppState$RunningProcess;->isSystemSignature()Z
if ( v8 != null) { // if-eqz v8, :cond_2
/* move v8, v6 */
} // :cond_2
/* move v8, v1 */
/* .line 642 */
/* .local v8, "isSystemSignSelf":Z */
} // :goto_1
/* iget v9, p2, Lmiui/security/CallerInfo;->callerUid:I */
/* if-ne v9, v2, :cond_6 */
/* .line 643 */
v9 = (( com.android.server.am.AppStateManager$AppState$RunningProcess ) v0 ).isSystemApp ( ); // invoke-virtual {v0}, Lcom/android/server/am/AppStateManager$AppState$RunningProcess;->isSystemApp()Z
/* if-nez v9, :cond_6 */
/* .line 644 */
v9 = (( com.android.server.am.AppStateManager$AppState$RunningProcess ) v0 ).isAutoStartApp ( ); // invoke-virtual {v0}, Lcom/android/server/am/AppStateManager$AppState$RunningProcess;->isAutoStartApp()Z
/* if-nez v9, :cond_6 */
/* if-nez v7, :cond_6 */
/* if-nez v8, :cond_6 */
/* .line 647 */
v9 = (( com.android.server.am.AppStateManager$AppState$RunningProcess ) v0 ).canHibernate ( ); // invoke-virtual {v0}, Lcom/android/server/am/AppStateManager$AppState$RunningProcess;->canHibernate()Z
if ( v9 != null) { // if-eqz v9, :cond_6
/* .line 648 */
v9 = (( com.miui.server.smartpower.SmartPowerPolicyManager ) p0 ).isServicePackageWhiteList ( v3 ); // invoke-virtual {p0, v3}, Lcom/miui/server/smartpower/SmartPowerPolicyManager;->isServicePackageWhiteList(Ljava/lang/String;)Z
/* if-nez v9, :cond_6 */
/* .line 649 */
v9 = this.mAppStateManager;
/* .line 650 */
(( com.android.server.am.AppStateManager ) v9 ).getRunningProcess ( v2, v4 ); // invoke-virtual {v9, v2, v4}, Lcom/android/server/am/AppStateManager;->getRunningProcess(ILjava/lang/String;)Lcom/android/server/am/AppStateManager$AppState$RunningProcess;
/* .line 651 */
/* .local v9, "serverProcessState":Lcom/android/server/am/AppStateManager$AppState$RunningProcess; */
if ( v9 != null) { // if-eqz v9, :cond_6
/* .line 652 */
v10 = (( com.miui.server.smartpower.SmartPowerPolicyManager ) p0 ).isServicePkgBlackList ( v3 ); // invoke-virtual {p0, v3}, Lcom/miui/server/smartpower/SmartPowerPolicyManager;->isServicePkgBlackList(Ljava/lang/String;)Z
if ( v10 != null) { // if-eqz v10, :cond_3
/* .line 653 */
v10 = (( com.android.server.am.AppStateManager$AppState$RunningProcess ) v9 ).canHibernate ( ); // invoke-virtual {v9}, Lcom/android/server/am/AppStateManager$AppState$RunningProcess;->canHibernate()Z
if ( v10 != null) { // if-eqz v10, :cond_6
} // :cond_3
v10 = (( com.android.server.am.AppStateManager$AppState$RunningProcess ) v9 ).isIdle ( ); // invoke-virtual {v9}, Lcom/android/server/am/AppStateManager$AppState$RunningProcess;->isIdle()Z
if ( v10 != null) { // if-eqz v10, :cond_6
/* .line 654 */
} // :goto_2
v10 = (( com.android.server.am.AppStateManager$AppState$RunningProcess ) v9 ).getPid ( ); // invoke-virtual {v9}, Lcom/android/server/am/AppStateManager$AppState$RunningProcess;->getPid()I
/* iget v11, p2, Lmiui/security/CallerInfo;->callerPid:I */
/* if-ne v10, v11, :cond_4 */
/* iget v10, p2, Lmiui/security/CallerInfo;->callerUid:I */
/* .line 655 */
v10 = /* invoke-direct {p0, v10}, Lcom/miui/server/smartpower/SmartPowerPolicyManager;->hasPidWhiteList(I)Z */
/* if-nez v10, :cond_6 */
/* .line 656 */
} // :cond_4
/* new-instance v1, Ljava/lang/StringBuilder; */
/* invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V */
final String v10 = "power ser h:{ u:"; // const-string v10, "power ser h:{ u:"
(( java.lang.StringBuilder ) v1 ).append ( v10 ); // invoke-virtual {v1, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v1 ).append ( v2 ); // invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;
final String v10 = " p:"; // const-string v10, " p:"
(( java.lang.StringBuilder ) v1 ).append ( v10 ); // invoke-virtual {v1, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v1 ).append ( v4 ); // invoke-virtual {v1, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
final String v10 = "/"; // const-string v10, "/"
(( java.lang.StringBuilder ) v1 ).append ( v10 ); // invoke-virtual {v1, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
/* .line 658 */
v11 = (( com.android.server.am.AppStateManager$AppState$RunningProcess ) v9 ).getPid ( ); // invoke-virtual {v9}, Lcom/android/server/am/AppStateManager$AppState$RunningProcess;->getPid()I
(( java.lang.StringBuilder ) v1 ).append ( v11 ); // invoke-virtual {v1, v11}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;
final String v11 = " s:"; // const-string v11, " s:"
(( java.lang.StringBuilder ) v1 ).append ( v11 ); // invoke-virtual {v1, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
/* .line 659 */
v11 = (( com.android.server.am.AppStateManager$AppState$RunningProcess ) v9 ).getCurrentState ( ); // invoke-virtual {v9}, Lcom/android/server/am/AppStateManager$AppState$RunningProcess;->getCurrentState()I
(( java.lang.StringBuilder ) v1 ).append ( v11 ); // invoke-virtual {v1, v11}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;
/* const-string/jumbo v11, "} c:" */
(( java.lang.StringBuilder ) v1 ).append ( v11 ); // invoke-virtual {v1, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
/* iget v11, p2, Lmiui/security/CallerInfo;->callerUid:I */
(( java.lang.StringBuilder ) v1 ).append ( v11 ); // invoke-virtual {v1, v11}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;
final String v11 = ":"; // const-string v11, ":"
(( java.lang.StringBuilder ) v1 ).append ( v11 ); // invoke-virtual {v1, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
/* iget v11, p2, Lmiui/security/CallerInfo;->callerPid:I */
(( java.lang.StringBuilder ) v1 ).append ( v11 ); // invoke-virtual {v1, v11}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v1 ).toString ( ); // invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
/* .line 656 */
/* const v11, 0x15ff2 */
android.util.EventLog .writeEvent ( v11,v1 );
/* .line 662 */
/* sget-boolean v1, Lcom/miui/server/smartpower/SmartPowerPolicyManager;->DEBUG:Z */
if ( v1 != null) { // if-eqz v1, :cond_5
/* .line 663 */
/* new-instance v1, Ljava/lang/StringBuilder; */
/* invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V */
final String v11 = "Intercept: service calling uid "; // const-string v11, "Intercept: service calling uid "
(( java.lang.StringBuilder ) v1 ).append ( v11 ); // invoke-virtual {v1, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
/* iget v11, p2, Lmiui/security/CallerInfo;->callerUid:I */
(( java.lang.StringBuilder ) v1 ).append ( v11 ); // invoke-virtual {v1, v11}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v1 ).append ( v10 ); // invoke-virtual {v1, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
v10 = this.callerProcessName;
(( java.lang.StringBuilder ) v1 ).append ( v10 ); // invoke-virtual {v1, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
final String v10 = " serviceState:"; // const-string v10, " serviceState:"
(( java.lang.StringBuilder ) v1 ).append ( v10 ); // invoke-virtual {v1, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
/* .line 665 */
v10 = (( com.android.server.am.AppStateManager$AppState$RunningProcess ) v0 ).getCurrentState ( ); // invoke-virtual {v0}, Lcom/android/server/am/AppStateManager$AppState$RunningProcess;->getCurrentState()I
(( java.lang.StringBuilder ) v1 ).append ( v10 ); // invoke-virtual {v1, v10}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;
final String v10 = " serviceInfo( processName="; // const-string v10, " serviceInfo( processName="
(( java.lang.StringBuilder ) v1 ).append ( v10 ); // invoke-virtual {v1, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v1 ).append ( v4 ); // invoke-virtual {v1, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
final String v10 = " getComponentName="; // const-string v10, " getComponentName="
(( java.lang.StringBuilder ) v1 ).append ( v10 ); // invoke-virtual {v1, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
/* .line 667 */
(( android.content.pm.ServiceInfo ) p3 ).getComponentName ( ); // invoke-virtual {p3}, Landroid/content/pm/ServiceInfo;->getComponentName()Landroid/content/ComponentName;
(( java.lang.StringBuilder ) v1 ).append ( v10 ); // invoke-virtual {v1, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;
final String v10 = " uid="; // const-string v10, " uid="
(( java.lang.StringBuilder ) v1 ).append ( v10 ); // invoke-virtual {v1, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v1 ).append ( v2 ); // invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;
final String v10 = ") Intent="; // const-string v10, ") Intent="
(( java.lang.StringBuilder ) v1 ).append ( v10 ); // invoke-virtual {v1, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v1 ).append ( p1 ); // invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v1 ).toString ( ); // invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
/* .line 663 */
final String v10 = "SmartPower.PowerPolicy"; // const-string v10, "SmartPower.PowerPolicy"
android.util.Slog .d ( v10,v1 );
/* .line 671 */
} // :cond_5
/* .line 675 */
} // .end local v8 # "isSystemSignSelf":Z
} // .end local v9 # "serverProcessState":Lcom/android/server/am/AppStateManager$AppState$RunningProcess;
} // :cond_6
/* .line 631 */
} // .end local v0 # "callingProcessState":Lcom/android/server/am/AppStateManager$AppState$RunningProcess;
} // .end local v2 # "serviceUid":I
} // .end local v3 # "servicePckName":Ljava/lang/String;
} // .end local v4 # "serviceProcName":Ljava/lang/String;
} // .end local v5 # "isSelfProc":Z
} // .end local v7 # "isWidgetSelf":Z
} // :cond_7
} // :goto_3
} // .end method
public Boolean skipFrozenAppAnr ( android.content.pm.ApplicationInfo p0, Integer p1, java.lang.String p2 ) {
/* .locals 6 */
/* .param p1, "info" # Landroid/content/pm/ApplicationInfo; */
/* .param p2, "uid" # I */
/* .param p3, "report" # Ljava/lang/String; */
/* .line 592 */
v0 = /* invoke-direct {p0}, Lcom/miui/server/smartpower/SmartPowerPolicyManager;->isEnableFrozen()Z */
int v1 = 0; // const/4 v1, 0x0
/* if-nez v0, :cond_0 */
/* .line 593 */
/* .line 595 */
} // :cond_0
v0 = this.mPowerFrozenManager;
v0 = (( com.miui.server.smartpower.PowerFrozenManager ) v0 ).isFrozenForUid ( p2 ); // invoke-virtual {v0, p2}, Lcom/miui/server/smartpower/PowerFrozenManager;->isFrozenForUid(I)Z
if ( v0 != null) { // if-eqz v0, :cond_1
/* .line 596 */
int v0 = 1; // const/4 v0, 0x1
/* .line 598 */
} // :cond_1
v0 = android.text.TextUtils .isEmpty ( p3 );
if ( v0 != null) { // if-eqz v0, :cond_2
/* .line 599 */
/* .line 601 */
} // :cond_2
int v0 = 0; // const/4 v0, 0x0
/* .line 602 */
/* .local v0, "timeout":I */
final String v2 = "Broadcast of"; // const-string v2, "Broadcast of"
v2 = (( java.lang.String ) p3 ).startsWith ( v2 ); // invoke-virtual {p3, v2}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z
if ( v2 != null) { // if-eqz v2, :cond_3
/* .line 603 */
/* const/16 v0, 0x4e20 */
/* .line 604 */
} // :cond_3
final String v2 = "executing service"; // const-string v2, "executing service"
v2 = (( java.lang.String ) p3 ).startsWith ( v2 ); // invoke-virtual {p3, v2}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z
if ( v2 != null) { // if-eqz v2, :cond_4
/* .line 605 */
/* const/16 v0, 0x4e20 */
/* .line 606 */
} // :cond_4
final String v2 = "ContentProvider not"; // const-string v2, "ContentProvider not"
v2 = (( java.lang.String ) p3 ).startsWith ( v2 ); // invoke-virtual {p3, v2}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z
if ( v2 != null) { // if-eqz v2, :cond_5
/* .line 607 */
/* const/16 v0, 0x2710 */
/* .line 608 */
} // :cond_5
final String v2 = "Input dispatching"; // const-string v2, "Input dispatching"
v2 = (( java.lang.String ) p3 ).startsWith ( v2 ); // invoke-virtual {p3, v2}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z
if ( v2 != null) { // if-eqz v2, :cond_6
/* .line 609 */
/* const/16 v0, 0x1388 */
/* .line 613 */
} // :goto_0
v1 = this.mAppStateManager;
java.lang.System .currentTimeMillis ( );
/* move-result-wide v2 */
/* int-to-long v4, v0 */
/* sub-long/2addr v2, v4 */
v1 = (( com.android.server.am.AppStateManager ) v1 ).isRecentThawed ( p2, v2, v3 ); // invoke-virtual {v1, p2, v2, v3}, Lcom/android/server/am/AppStateManager;->isRecentThawed(IJ)Z
/* .line 611 */
} // :cond_6
} // .end method
public void updateCloudAlarmWhiteLit ( java.lang.String p0 ) {
/* .locals 4 */
/* .param p1, "procList" # Ljava/lang/String; */
/* .line 404 */
v0 = android.text.TextUtils .isEmpty ( p1 );
/* if-nez v0, :cond_0 */
/* .line 405 */
final String v0 = ","; // const-string v0, ","
(( java.lang.String ) p1 ).split ( v0 ); // invoke-virtual {p1, v0}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;
/* .line 406 */
/* .local v0, "procArray":[Ljava/lang/String; */
/* array-length v1, v0 */
int v2 = 0; // const/4 v2, 0x0
} // :goto_0
/* if-ge v2, v1, :cond_0 */
/* aget-object v3, v0, v2 */
/* .line 407 */
/* .local v3, "proc":Ljava/lang/String; */
/* invoke-direct {p0, v3}, Lcom/miui/server/smartpower/SmartPowerPolicyManager;->addAlarmPackageWhiteList(Ljava/lang/String;)V */
/* .line 406 */
} // .end local v3 # "proc":Ljava/lang/String;
/* add-int/lit8 v2, v2, 0x1 */
/* .line 410 */
} // .end local v0 # "procArray":[Ljava/lang/String;
} // :cond_0
return;
} // .end method
public void updateCloudBroadcastWhiteLit ( java.lang.String p0 ) {
/* .locals 4 */
/* .param p1, "procList" # Ljava/lang/String; */
/* .line 395 */
v0 = android.text.TextUtils .isEmpty ( p1 );
/* if-nez v0, :cond_0 */
/* .line 396 */
final String v0 = ","; // const-string v0, ","
(( java.lang.String ) p1 ).split ( v0 ); // invoke-virtual {p1, v0}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;
/* .line 397 */
/* .local v0, "procArray":[Ljava/lang/String; */
/* array-length v1, v0 */
int v2 = 0; // const/4 v2, 0x0
} // :goto_0
/* if-ge v2, v1, :cond_0 */
/* aget-object v3, v0, v2 */
/* .line 398 */
/* .local v3, "procAction":Ljava/lang/String; */
/* invoke-direct {p0, v3}, Lcom/miui/server/smartpower/SmartPowerPolicyManager;->addBroadcastProcessActionWhiteList(Ljava/lang/String;)V */
/* .line 397 */
} // .end local v3 # "procAction":Ljava/lang/String;
/* add-int/lit8 v2, v2, 0x1 */
/* .line 401 */
} // .end local v0 # "procArray":[Ljava/lang/String;
} // :cond_0
return;
} // .end method
public void updateCloudPackageWhiteList ( java.lang.String p0 ) {
/* .locals 5 */
/* .param p1, "pkgList" # Ljava/lang/String; */
/* .line 365 */
v0 = android.text.TextUtils .isEmpty ( p1 );
/* if-nez v0, :cond_0 */
/* .line 366 */
final String v0 = ","; // const-string v0, ","
(( java.lang.String ) p1 ).split ( v0 ); // invoke-virtual {p1, v0}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;
/* .line 367 */
/* .local v0, "pkgArray":[Ljava/lang/String; */
/* array-length v1, v0 */
int v2 = 0; // const/4 v2, 0x0
} // :goto_0
/* if-ge v2, v1, :cond_0 */
/* aget-object v3, v0, v2 */
/* .line 368 */
/* .local v3, "packageName":Ljava/lang/String; */
int v4 = 4; // const/4 v4, 0x4
/* invoke-direct {p0, v3, v4}, Lcom/miui/server/smartpower/SmartPowerPolicyManager;->addPackageWhiteList(Ljava/lang/String;I)V */
/* .line 367 */
} // .end local v3 # "packageName":Ljava/lang/String;
/* add-int/lit8 v2, v2, 0x1 */
/* .line 371 */
} // .end local v0 # "pkgArray":[Ljava/lang/String;
} // :cond_0
return;
} // .end method
public void updateCloudProcessWhiteList ( java.lang.String p0 ) {
/* .locals 5 */
/* .param p1, "procList" # Ljava/lang/String; */
/* .line 374 */
v0 = android.text.TextUtils .isEmpty ( p1 );
/* if-nez v0, :cond_0 */
/* .line 375 */
final String v0 = ","; // const-string v0, ","
(( java.lang.String ) p1 ).split ( v0 ); // invoke-virtual {p1, v0}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;
/* .line 376 */
/* .local v0, "procArray":[Ljava/lang/String; */
/* array-length v1, v0 */
int v2 = 0; // const/4 v2, 0x0
} // :goto_0
/* if-ge v2, v1, :cond_0 */
/* aget-object v3, v0, v2 */
/* .line 377 */
/* .local v3, "procName":Ljava/lang/String; */
int v4 = 4; // const/4 v4, 0x4
/* invoke-direct {p0, v3, v4}, Lcom/miui/server/smartpower/SmartPowerPolicyManager;->addProcessWhiteList(Ljava/lang/String;I)V */
/* .line 376 */
} // .end local v3 # "procName":Ljava/lang/String;
/* add-int/lit8 v2, v2, 0x1 */
/* .line 380 */
} // .end local v0 # "procArray":[Ljava/lang/String;
} // :cond_0
return;
} // .end method
public void updateCloudProviderWhiteLit ( java.lang.String p0 ) {
/* .locals 4 */
/* .param p1, "procList" # Ljava/lang/String; */
/* .line 413 */
v0 = android.text.TextUtils .isEmpty ( p1 );
/* if-nez v0, :cond_0 */
/* .line 414 */
final String v0 = ","; // const-string v0, ","
(( java.lang.String ) p1 ).split ( v0 ); // invoke-virtual {p1, v0}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;
/* .line 415 */
/* .local v0, "procArray":[Ljava/lang/String; */
/* array-length v1, v0 */
int v2 = 0; // const/4 v2, 0x0
} // :goto_0
/* if-ge v2, v1, :cond_0 */
/* aget-object v3, v0, v2 */
/* .line 416 */
/* .local v3, "proc":Ljava/lang/String; */
/* invoke-direct {p0, v3}, Lcom/miui/server/smartpower/SmartPowerPolicyManager;->addProviderPackageWhiteList(Ljava/lang/String;)V */
/* .line 415 */
} // .end local v3 # "proc":Ljava/lang/String;
/* add-int/lit8 v2, v2, 0x1 */
/* .line 419 */
} // .end local v0 # "procArray":[Ljava/lang/String;
} // :cond_0
return;
} // .end method
public void updateCloudServiceWhiteLit ( java.lang.String p0 ) {
/* .locals 4 */
/* .param p1, "procList" # Ljava/lang/String; */
/* .line 422 */
v0 = android.text.TextUtils .isEmpty ( p1 );
/* if-nez v0, :cond_0 */
/* .line 423 */
final String v0 = ","; // const-string v0, ","
(( java.lang.String ) p1 ).split ( v0 ); // invoke-virtual {p1, v0}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;
/* .line 424 */
/* .local v0, "procArray":[Ljava/lang/String; */
/* array-length v1, v0 */
int v2 = 0; // const/4 v2, 0x0
} // :goto_0
/* if-ge v2, v1, :cond_0 */
/* aget-object v3, v0, v2 */
/* .line 425 */
/* .local v3, "proc":Ljava/lang/String; */
/* invoke-direct {p0, v3}, Lcom/miui/server/smartpower/SmartPowerPolicyManager;->addServicePackageWhiteList(Ljava/lang/String;)V */
/* .line 424 */
} // .end local v3 # "proc":Ljava/lang/String;
/* add-int/lit8 v2, v2, 0x1 */
/* .line 428 */
} // .end local v0 # "procArray":[Ljava/lang/String;
} // :cond_0
return;
} // .end method
public void updateCloudSreenonWhiteList ( java.lang.String p0 ) {
/* .locals 5 */
/* .param p1, "sreenonList" # Ljava/lang/String; */
/* .line 383 */
v0 = android.text.TextUtils .isEmpty ( p1 );
/* if-nez v0, :cond_0 */
/* .line 384 */
final String v0 = ","; // const-string v0, ","
(( java.lang.String ) p1 ).split ( v0 ); // invoke-virtual {p1, v0}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;
/* .line 385 */
/* .local v0, "screenAppArray":[Ljava/lang/String; */
/* array-length v1, v0 */
int v2 = 0; // const/4 v2, 0x0
} // :goto_0
/* if-ge v2, v1, :cond_0 */
/* aget-object v3, v0, v2 */
/* .line 386 */
/* .local v3, "packageName":Ljava/lang/String; */
/* const/16 v4, 0x2000 */
/* invoke-direct {p0, v3, v4}, Lcom/miui/server/smartpower/SmartPowerPolicyManager;->addSreenonWhiteList(Ljava/lang/String;I)V */
/* .line 385 */
} // .end local v3 # "packageName":Ljava/lang/String;
/* add-int/lit8 v2, v2, 0x1 */
/* .line 389 */
} // .end local v0 # "screenAppArray":[Ljava/lang/String;
} // :cond_0
return;
} // .end method
