public class com.miui.server.smartpower.SmartThermalPolicyManager implements com.miui.server.smartpower.SmartScenarioManager$ISmartScenarioCallback {
	 /* .source "SmartThermalPolicyManager.java" */
	 /* # interfaces */
	 /* # instance fields */
	 private miui.smartpower.IScenarioCallback mCallBack;
	 private com.miui.server.smartpower.SmartScenarioManager$ClientConfig mConfig;
	 private com.miui.server.smartpower.SmartScenarioManager mSmartScenarioManager;
	 /* # direct methods */
	 public com.miui.server.smartpower.SmartThermalPolicyManager ( ) {
		 /* .locals 0 */
		 /* .param p1, "smartScenarioManager" # Lcom/miui/server/smartpower/SmartScenarioManager; */
		 /* .line 12 */
		 /* invoke-direct {p0}, Ljava/lang/Object;-><init>()V */
		 /* .line 13 */
		 this.mSmartScenarioManager = p1;
		 /* .line 14 */
		 return;
	 } // .end method
	 private void init ( ) {
		 /* .locals 15 */
		 /* .line 17 */
		 /* move-object v0, p0 */
		 v1 = this.mSmartScenarioManager;
		 /* const-string/jumbo v2, "thermal" */
		 (( com.miui.server.smartpower.SmartScenarioManager ) v1 ).createClientConfig ( v2, p0 ); // invoke-virtual {v1, v2, p0}, Lcom/miui/server/smartpower/SmartScenarioManager;->createClientConfig(Ljava/lang/String;Lcom/miui/server/smartpower/SmartScenarioManager$ISmartScenarioCallback;)Lcom/miui/server/smartpower/SmartScenarioManager$ClientConfig;
		 this.mConfig = v1;
		 /* .line 19 */
		 /* const-wide/16 v2, 0x2 */
		 final String v4 = "com.tencent.mm"; // const-string v4, "com.tencent.mm"
		 int v5 = 2; // const/4 v5, 0x2
		 (( com.miui.server.smartpower.SmartScenarioManager$ClientConfig ) v1 ).addMainScenarioIdConfig ( v2, v3, v4, v5 ); // invoke-virtual {v1, v2, v3, v4, v5}, Lcom/miui/server/smartpower/SmartScenarioManager$ClientConfig;->addMainScenarioIdConfig(JLjava/lang/String;I)V
		 /* .line 21 */
		 v1 = this.mConfig;
		 final String v6 = "com.sina.weibo"; // const-string v6, "com.sina.weibo"
		 /* const-wide/16 v7, 0x4 */
		 (( com.miui.server.smartpower.SmartScenarioManager$ClientConfig ) v1 ).addMainScenarioIdConfig ( v7, v8, v6, v5 ); // invoke-virtual {v1, v7, v8, v6, v5}, Lcom/miui/server/smartpower/SmartScenarioManager$ClientConfig;->addMainScenarioIdConfig(JLjava/lang/String;I)V
		 /* .line 23 */
		 v1 = this.mConfig;
		 final String v6 = "com.ss.android.ugc.aweme"; // const-string v6, "com.ss.android.ugc.aweme"
		 /* const-wide/16 v9, 0x8 */
		 (( com.miui.server.smartpower.SmartScenarioManager$ClientConfig ) v1 ).addMainScenarioIdConfig ( v9, v10, v6, v5 ); // invoke-virtual {v1, v9, v10, v6, v5}, Lcom/miui/server/smartpower/SmartScenarioManager$ClientConfig;->addMainScenarioIdConfig(JLjava/lang/String;I)V
		 /* .line 25 */
		 v1 = this.mConfig;
		 final String v6 = "com.smile.gifmaker"; // const-string v6, "com.smile.gifmaker"
		 /* const-wide/16 v11, 0x10 */
		 (( com.miui.server.smartpower.SmartScenarioManager$ClientConfig ) v1 ).addMainScenarioIdConfig ( v11, v12, v6, v5 ); // invoke-virtual {v1, v11, v12, v6, v5}, Lcom/miui/server/smartpower/SmartScenarioManager$ClientConfig;->addMainScenarioIdConfig(JLjava/lang/String;I)V
		 /* .line 27 */
		 v1 = this.mConfig;
		 final String v6 = "com.ss.android.article.news"; // const-string v6, "com.ss.android.article.news"
		 /* const-wide/16 v13, 0x20 */
		 (( com.miui.server.smartpower.SmartScenarioManager$ClientConfig ) v1 ).addMainScenarioIdConfig ( v13, v14, v6, v5 ); // invoke-virtual {v1, v13, v14, v6, v5}, Lcom/miui/server/smartpower/SmartScenarioManager$ClientConfig;->addMainScenarioIdConfig(JLjava/lang/String;I)V
		 /* .line 29 */
		 v1 = this.mConfig;
		 final String v6 = "com.taobao.taobao"; // const-string v6, "com.taobao.taobao"
		 /* const-wide/16 v13, 0x40 */
		 (( com.miui.server.smartpower.SmartScenarioManager$ClientConfig ) v1 ).addMainScenarioIdConfig ( v13, v14, v6, v5 ); // invoke-virtual {v1, v13, v14, v6, v5}, Lcom/miui/server/smartpower/SmartScenarioManager$ClientConfig;->addMainScenarioIdConfig(JLjava/lang/String;I)V
		 /* .line 31 */
		 v1 = this.mConfig;
		 /* const-string/jumbo v6, "tv.danmaku.bili" */
		 /* const-wide/16 v13, 0x80 */
		 (( com.miui.server.smartpower.SmartScenarioManager$ClientConfig ) v1 ).addMainScenarioIdConfig ( v13, v14, v6, v5 ); // invoke-virtual {v1, v13, v14, v6, v5}, Lcom/miui/server/smartpower/SmartScenarioManager$ClientConfig;->addMainScenarioIdConfig(JLjava/lang/String;I)V
		 /* .line 33 */
		 v1 = this.mConfig;
		 final String v6 = "com.autonavi.minimap"; // const-string v6, "com.autonavi.minimap"
		 /* const-wide/16 v13, 0x100 */
		 (( com.miui.server.smartpower.SmartScenarioManager$ClientConfig ) v1 ).addMainScenarioIdConfig ( v13, v14, v6, v5 ); // invoke-virtual {v1, v13, v14, v6, v5}, Lcom/miui/server/smartpower/SmartScenarioManager$ClientConfig;->addMainScenarioIdConfig(JLjava/lang/String;I)V
		 /* .line 35 */
		 v1 = this.mConfig;
		 /* const-wide/16 v13, 0x200 */
		 final String v6 = "com.android.camera"; // const-string v6, "com.android.camera"
		 (( com.miui.server.smartpower.SmartScenarioManager$ClientConfig ) v1 ).addMainScenarioIdConfig ( v13, v14, v6, v5 ); // invoke-virtual {v1, v13, v14, v6, v5}, Lcom/miui/server/smartpower/SmartScenarioManager$ClientConfig;->addMainScenarioIdConfig(JLjava/lang/String;I)V
		 /* .line 38 */
		 v1 = this.mConfig;
		 /* const-wide/32 v13, 0x100000 */
		 final String v6 = "com.tencent.tmgp.sgame"; // const-string v6, "com.tencent.tmgp.sgame"
		 (( com.miui.server.smartpower.SmartScenarioManager$ClientConfig ) v1 ).addMainScenarioIdConfig ( v13, v14, v6, v5 ); // invoke-virtual {v1, v13, v14, v6, v5}, Lcom/miui/server/smartpower/SmartScenarioManager$ClientConfig;->addMainScenarioIdConfig(JLjava/lang/String;I)V
		 /* .line 40 */
		 v1 = this.mConfig;
		 final String v6 = "com.miHoYo.Yuanshen"; // const-string v6, "com.miHoYo.Yuanshen"
		 /* const-wide/32 v13, 0x200000 */
		 (( com.miui.server.smartpower.SmartScenarioManager$ClientConfig ) v1 ).addMainScenarioIdConfig ( v13, v14, v6, v5 ); // invoke-virtual {v1, v13, v14, v6, v5}, Lcom/miui/server/smartpower/SmartScenarioManager$ClientConfig;->addMainScenarioIdConfig(JLjava/lang/String;I)V
		 /* .line 44 */
		 v1 = this.mConfig;
		 int v5 = 0; // const/4 v5, 0x0
		 /* const/16 v6, 0x40 */
		 (( com.miui.server.smartpower.SmartScenarioManager$ClientConfig ) v1 ).addAdditionalScenarioIdConfig ( v2, v3, v5, v6 ); // invoke-virtual {v1, v2, v3, v5, v6}, Lcom/miui/server/smartpower/SmartScenarioManager$ClientConfig;->addAdditionalScenarioIdConfig(JLjava/lang/String;I)V
		 /* .line 46 */
		 v1 = this.mConfig;
		 (( com.miui.server.smartpower.SmartScenarioManager$ClientConfig ) v1 ).addAdditionalScenarioIdConfig ( v7, v8, v4, v6 ); // invoke-virtual {v1, v7, v8, v4, v6}, Lcom/miui/server/smartpower/SmartScenarioManager$ClientConfig;->addAdditionalScenarioIdConfig(JLjava/lang/String;I)V
		 /* .line 48 */
		 v1 = this.mConfig;
		 final String v2 = "com.tencent.mobileqq"; // const-string v2, "com.tencent.mobileqq"
		 (( com.miui.server.smartpower.SmartScenarioManager$ClientConfig ) v1 ).addAdditionalScenarioIdConfig ( v9, v10, v2, v6 ); // invoke-virtual {v1, v9, v10, v2, v6}, Lcom/miui/server/smartpower/SmartScenarioManager$ClientConfig;->addAdditionalScenarioIdConfig(JLjava/lang/String;I)V
		 /* .line 50 */
		 v1 = this.mConfig;
		 final String v3 = "com.ss.android.lark.kami"; // const-string v3, "com.ss.android.lark.kami"
		 (( com.miui.server.smartpower.SmartScenarioManager$ClientConfig ) v1 ).addAdditionalScenarioIdConfig ( v11, v12, v3, v6 ); // invoke-virtual {v1, v11, v12, v3, v6}, Lcom/miui/server/smartpower/SmartScenarioManager$ClientConfig;->addAdditionalScenarioIdConfig(JLjava/lang/String;I)V
		 /* .line 52 */
		 v1 = this.mConfig;
		 final String v7 = "com.alibaba.android.rimet"; // const-string v7, "com.alibaba.android.rimet"
		 /* const-wide/16 v8, 0x20 */
		 (( com.miui.server.smartpower.SmartScenarioManager$ClientConfig ) v1 ).addAdditionalScenarioIdConfig ( v8, v9, v7, v6 ); // invoke-virtual {v1, v8, v9, v7, v6}, Lcom/miui/server/smartpower/SmartScenarioManager$ClientConfig;->addAdditionalScenarioIdConfig(JLjava/lang/String;I)V
		 /* .line 54 */
		 v1 = this.mConfig;
		 final String v8 = "com.tencent.wework"; // const-string v8, "com.tencent.wework"
		 /* const-wide/16 v9, 0x40 */
		 (( com.miui.server.smartpower.SmartScenarioManager$ClientConfig ) v1 ).addAdditionalScenarioIdConfig ( v9, v10, v8, v6 ); // invoke-virtual {v1, v9, v10, v8, v6}, Lcom/miui/server/smartpower/SmartScenarioManager$ClientConfig;->addAdditionalScenarioIdConfig(JLjava/lang/String;I)V
		 /* .line 56 */
		 v1 = this.mConfig;
		 final String v9 = "com.whatsapp"; // const-string v9, "com.whatsapp"
		 /* const-wide/16 v10, 0x80 */
		 (( com.miui.server.smartpower.SmartScenarioManager$ClientConfig ) v1 ).addAdditionalScenarioIdConfig ( v10, v11, v9, v6 ); // invoke-virtual {v1, v10, v11, v9, v6}, Lcom/miui/server/smartpower/SmartScenarioManager$ClientConfig;->addAdditionalScenarioIdConfig(JLjava/lang/String;I)V
		 /* .line 58 */
		 v1 = this.mConfig;
		 final String v10 = "com.ss.android.lark"; // const-string v10, "com.ss.android.lark"
		 /* const-wide/16 v11, 0x100 */
		 (( com.miui.server.smartpower.SmartScenarioManager$ClientConfig ) v1 ).addAdditionalScenarioIdConfig ( v11, v12, v10, v6 ); // invoke-virtual {v1, v11, v12, v10, v6}, Lcom/miui/server/smartpower/SmartScenarioManager$ClientConfig;->addAdditionalScenarioIdConfig(JLjava/lang/String;I)V
		 /* .line 61 */
		 v1 = this.mConfig;
		 /* const-wide/16 v11, 0x800 */
		 /* const/16 v6, 0x80 */
		 (( com.miui.server.smartpower.SmartScenarioManager$ClientConfig ) v1 ).addAdditionalScenarioIdConfig ( v11, v12, v5, v6 ); // invoke-virtual {v1, v11, v12, v5, v6}, Lcom/miui/server/smartpower/SmartScenarioManager$ClientConfig;->addAdditionalScenarioIdConfig(JLjava/lang/String;I)V
		 /* .line 63 */
		 v1 = this.mConfig;
		 /* const-wide/16 v11, 0x1000 */
		 (( com.miui.server.smartpower.SmartScenarioManager$ClientConfig ) v1 ).addAdditionalScenarioIdConfig ( v11, v12, v4, v6 ); // invoke-virtual {v1, v11, v12, v4, v6}, Lcom/miui/server/smartpower/SmartScenarioManager$ClientConfig;->addAdditionalScenarioIdConfig(JLjava/lang/String;I)V
		 /* .line 65 */
		 v1 = this.mConfig;
		 /* const-wide/16 v11, 0x2000 */
		 (( com.miui.server.smartpower.SmartScenarioManager$ClientConfig ) v1 ).addAdditionalScenarioIdConfig ( v11, v12, v2, v6 ); // invoke-virtual {v1, v11, v12, v2, v6}, Lcom/miui/server/smartpower/SmartScenarioManager$ClientConfig;->addAdditionalScenarioIdConfig(JLjava/lang/String;I)V
		 /* .line 67 */
		 v1 = this.mConfig;
		 /* const-wide/16 v11, 0x4000 */
		 (( com.miui.server.smartpower.SmartScenarioManager$ClientConfig ) v1 ).addAdditionalScenarioIdConfig ( v11, v12, v3, v6 ); // invoke-virtual {v1, v11, v12, v3, v6}, Lcom/miui/server/smartpower/SmartScenarioManager$ClientConfig;->addAdditionalScenarioIdConfig(JLjava/lang/String;I)V
		 /* .line 69 */
		 v1 = this.mConfig;
		 /* const-wide/32 v11, 0x8000 */
		 (( com.miui.server.smartpower.SmartScenarioManager$ClientConfig ) v1 ).addAdditionalScenarioIdConfig ( v11, v12, v7, v6 ); // invoke-virtual {v1, v11, v12, v7, v6}, Lcom/miui/server/smartpower/SmartScenarioManager$ClientConfig;->addAdditionalScenarioIdConfig(JLjava/lang/String;I)V
		 /* .line 71 */
		 v1 = this.mConfig;
		 /* const-wide/32 v11, 0x10000 */
		 (( com.miui.server.smartpower.SmartScenarioManager$ClientConfig ) v1 ).addAdditionalScenarioIdConfig ( v11, v12, v8, v6 ); // invoke-virtual {v1, v11, v12, v8, v6}, Lcom/miui/server/smartpower/SmartScenarioManager$ClientConfig;->addAdditionalScenarioIdConfig(JLjava/lang/String;I)V
		 /* .line 73 */
		 v1 = this.mConfig;
		 /* const-wide/32 v11, 0x20000 */
		 (( com.miui.server.smartpower.SmartScenarioManager$ClientConfig ) v1 ).addAdditionalScenarioIdConfig ( v11, v12, v9, v6 ); // invoke-virtual {v1, v11, v12, v9, v6}, Lcom/miui/server/smartpower/SmartScenarioManager$ClientConfig;->addAdditionalScenarioIdConfig(JLjava/lang/String;I)V
		 /* .line 75 */
		 v1 = this.mConfig;
		 /* const-wide/32 v8, 0x40000 */
		 (( com.miui.server.smartpower.SmartScenarioManager$ClientConfig ) v1 ).addAdditionalScenarioIdConfig ( v8, v9, v10, v6 ); // invoke-virtual {v1, v8, v9, v10, v6}, Lcom/miui/server/smartpower/SmartScenarioManager$ClientConfig;->addAdditionalScenarioIdConfig(JLjava/lang/String;I)V
		 /* .line 78 */
		 v1 = this.mConfig;
		 /* const/16 v2, 0x100 */
		 (( com.miui.server.smartpower.SmartScenarioManager$ClientConfig ) v1 ).addAdditionalScenarioIdConfig ( v13, v14, v5, v2 ); // invoke-virtual {v1, v13, v14, v5, v2}, Lcom/miui/server/smartpower/SmartScenarioManager$ClientConfig;->addAdditionalScenarioIdConfig(JLjava/lang/String;I)V
		 /* .line 80 */
		 v1 = this.mConfig;
		 /* const-wide/32 v8, 0x400000 */
		 final String v4 = "com.xiaomi.market"; // const-string v4, "com.xiaomi.market"
		 (( com.miui.server.smartpower.SmartScenarioManager$ClientConfig ) v1 ).addAdditionalScenarioIdConfig ( v8, v9, v4, v2 ); // invoke-virtual {v1, v8, v9, v4, v2}, Lcom/miui/server/smartpower/SmartScenarioManager$ClientConfig;->addAdditionalScenarioIdConfig(JLjava/lang/String;I)V
		 /* .line 82 */
		 v1 = this.mConfig;
		 /* const-wide/32 v8, 0x800000 */
		 final String v4 = "com.xunlei.downloadprovider"; // const-string v4, "com.xunlei.downloadprovider"
		 (( com.miui.server.smartpower.SmartScenarioManager$ClientConfig ) v1 ).addAdditionalScenarioIdConfig ( v8, v9, v4, v2 ); // invoke-virtual {v1, v8, v9, v4, v2}, Lcom/miui/server/smartpower/SmartScenarioManager$ClientConfig;->addAdditionalScenarioIdConfig(JLjava/lang/String;I)V
		 /* .line 85 */
		 v1 = this.mConfig;
		 /* const-wide v8, 0x80000000L */
		 /* const/16 v2, 0x10 */
		 (( com.miui.server.smartpower.SmartScenarioManager$ClientConfig ) v1 ).addAdditionalScenarioIdConfig ( v8, v9, v5, v2 ); // invoke-virtual {v1, v8, v9, v5, v2}, Lcom/miui/server/smartpower/SmartScenarioManager$ClientConfig;->addAdditionalScenarioIdConfig(JLjava/lang/String;I)V
		 /* .line 88 */
		 v1 = this.mConfig;
		 /* const-wide v8, 0x1000000000L */
		 /* const/16 v2, 0x20 */
		 (( com.miui.server.smartpower.SmartScenarioManager$ClientConfig ) v1 ).addAdditionalScenarioIdConfig ( v8, v9, v5, v2 ); // invoke-virtual {v1, v8, v9, v5, v2}, Lcom/miui/server/smartpower/SmartScenarioManager$ClientConfig;->addAdditionalScenarioIdConfig(JLjava/lang/String;I)V
		 /* .line 91 */
		 v1 = this.mConfig;
		 /* const-wide v8, 0x20000000000L */
		 /* const/16 v2, 0x8 */
		 (( com.miui.server.smartpower.SmartScenarioManager$ClientConfig ) v1 ).addAdditionalScenarioIdConfig ( v8, v9, v3, v2 ); // invoke-virtual {v1, v8, v9, v3, v2}, Lcom/miui/server/smartpower/SmartScenarioManager$ClientConfig;->addAdditionalScenarioIdConfig(JLjava/lang/String;I)V
		 /* .line 93 */
		 v1 = this.mConfig;
		 /* const-wide v3, 0x40000000000L */
		 (( com.miui.server.smartpower.SmartScenarioManager$ClientConfig ) v1 ).addAdditionalScenarioIdConfig ( v3, v4, v7, v2 ); // invoke-virtual {v1, v3, v4, v7, v2}, Lcom/miui/server/smartpower/SmartScenarioManager$ClientConfig;->addAdditionalScenarioIdConfig(JLjava/lang/String;I)V
		 /* .line 96 */
		 v1 = this.mConfig;
		 /* const-wide v2, 0x800000000000L */
		 /* const/high16 v4, 0x10000 */
		 (( com.miui.server.smartpower.SmartScenarioManager$ClientConfig ) v1 ).addAdditionalScenarioIdConfig ( v2, v3, v5, v4 ); // invoke-virtual {v1, v2, v3, v5, v4}, Lcom/miui/server/smartpower/SmartScenarioManager$ClientConfig;->addAdditionalScenarioIdConfig(JLjava/lang/String;I)V
		 /* .line 99 */
		 v1 = this.mConfig;
		 /* const-wide/high16 v2, 0x1000000000000L */
		 /* const v4, 0x8000 */
		 (( com.miui.server.smartpower.SmartScenarioManager$ClientConfig ) v1 ).addAdditionalScenarioIdConfig ( v2, v3, v5, v4 ); // invoke-virtual {v1, v2, v3, v5, v4}, Lcom/miui/server/smartpower/SmartScenarioManager$ClientConfig;->addAdditionalScenarioIdConfig(JLjava/lang/String;I)V
		 /* .line 102 */
		 v1 = this.mConfig;
		 /* const-wide/high16 v2, 0x2000000000000L */
		 /* const/16 v4, 0x2000 */
		 (( com.miui.server.smartpower.SmartScenarioManager$ClientConfig ) v1 ).addAdditionalScenarioIdConfig ( v2, v3, v5, v4 ); // invoke-virtual {v1, v2, v3, v5, v4}, Lcom/miui/server/smartpower/SmartScenarioManager$ClientConfig;->addAdditionalScenarioIdConfig(JLjava/lang/String;I)V
		 /* .line 105 */
		 v1 = this.mConfig;
		 /* const-wide/high16 v2, 0x4000000000000L */
		 /* const/16 v4, 0x800 */
		 (( com.miui.server.smartpower.SmartScenarioManager$ClientConfig ) v1 ).addAdditionalScenarioIdConfig ( v2, v3, v5, v4 ); // invoke-virtual {v1, v2, v3, v5, v4}, Lcom/miui/server/smartpower/SmartScenarioManager$ClientConfig;->addAdditionalScenarioIdConfig(JLjava/lang/String;I)V
		 /* .line 108 */
		 v1 = this.mConfig;
		 /* const-wide/high16 v2, 0x8000000000000L */
		 /* const/16 v4, 0x200 */
		 (( com.miui.server.smartpower.SmartScenarioManager$ClientConfig ) v1 ).addAdditionalScenarioIdConfig ( v2, v3, v5, v4 ); // invoke-virtual {v1, v2, v3, v5, v4}, Lcom/miui/server/smartpower/SmartScenarioManager$ClientConfig;->addAdditionalScenarioIdConfig(JLjava/lang/String;I)V
		 /* .line 111 */
		 v1 = this.mConfig;
		 /* const-wide/high16 v2, 0x100000000000000L */
		 /* const/16 v4, 0x400 */
		 (( com.miui.server.smartpower.SmartScenarioManager$ClientConfig ) v1 ).addAdditionalScenarioIdConfig ( v2, v3, v5, v4 ); // invoke-virtual {v1, v2, v3, v5, v4}, Lcom/miui/server/smartpower/SmartScenarioManager$ClientConfig;->addAdditionalScenarioIdConfig(JLjava/lang/String;I)V
		 /* .line 114 */
		 v1 = this.mConfig;
		 /* const-wide/high16 v2, 0x2000000000000000L */
		 /* const/16 v4, 0x1000 */
		 (( com.miui.server.smartpower.SmartScenarioManager$ClientConfig ) v1 ).addAdditionalScenarioIdConfig ( v2, v3, v5, v4 ); // invoke-virtual {v1, v2, v3, v5, v4}, Lcom/miui/server/smartpower/SmartScenarioManager$ClientConfig;->addAdditionalScenarioIdConfig(JLjava/lang/String;I)V
		 /* .line 117 */
		 v1 = this.mSmartScenarioManager;
		 v2 = this.mConfig;
		 (( com.miui.server.smartpower.SmartScenarioManager ) v1 ).registClientConfig ( v2 ); // invoke-virtual {v1, v2}, Lcom/miui/server/smartpower/SmartScenarioManager;->registClientConfig(Lcom/miui/server/smartpower/SmartScenarioManager$ClientConfig;)V
		 /* .line 118 */
		 return;
	 } // .end method
	 /* # virtual methods */
	 public void onCurrentScenarioChanged ( Long p0, Long p1 ) {
		 /* .locals 1 */
		 /* .param p1, "mainSenarioId" # J */
		 /* .param p3, "additionalSenarioId" # J */
		 /* .line 127 */
		 v0 = this.mCallBack;
		 if ( v0 != null) { // if-eqz v0, :cond_0
			 /* .line 129 */
			 try { // :try_start_0
				 /* :try_end_0 */
				 /* .catch Landroid/os/RemoteException; {:try_start_0 ..:try_end_0} :catch_0 */
				 /* .line 130 */
				 /* :catch_0 */
				 /* move-exception v0 */
			 } // :goto_0
			 /* nop */
			 /* .line 132 */
		 } // :cond_0
		 return;
	 } // .end method
	 public void registThermalScenarioCallback ( miui.smartpower.IScenarioCallback p0 ) {
		 /* .locals 0 */
		 /* .param p1, "callback" # Lmiui/smartpower/IScenarioCallback; */
		 /* .line 121 */
		 this.mCallBack = p1;
		 /* .line 122 */
		 /* invoke-direct {p0}, Lcom/miui/server/smartpower/SmartThermalPolicyManager;->init()V */
		 /* .line 123 */
		 return;
	 } // .end method
