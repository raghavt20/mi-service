class com.miui.server.smartpower.SmartWindowPolicyManager$MultiTaskPolicyController {
	 /* .source "SmartWindowPolicyManager.java" */
	 /* # annotations */
	 /* .annotation system Ldalvik/annotation/EnclosingClass; */
	 /* value = Lcom/miui/server/smartpower/SmartWindowPolicyManager; */
} // .end annotation
/* .annotation system Ldalvik/annotation/InnerClass; */
/* accessFlags = 0x2 */
/* name = "MultiTaskPolicyController" */
} // .end annotation
/* .annotation system Ldalvik/annotation/MemberClasses; */
/* value = { */
/* Lcom/miui/server/smartpower/SmartWindowPolicyManager$MultiTaskPolicyController$MultiTaskActionListenerDelegate; */
/* } */
} // .end annotation
/* # instance fields */
private final java.lang.Object mLock;
private final android.util.SparseArray mMultiTaskActionListeners;
/* .annotation system Ldalvik/annotation/Signature; */
/* value = { */
/* "Landroid/util/SparseArray<", */
/* "Lcom/miui/server/smartpower/SmartWindowPolicyManager$MultiTaskPolicyController$MultiTaskActionListenerDelegate;", */
/* ">;" */
/* } */
} // .end annotation
} // .end field
final com.miui.server.smartpower.SmartWindowPolicyManager this$0; //synthetic
/* # direct methods */
private com.miui.server.smartpower.SmartWindowPolicyManager$MultiTaskPolicyController ( ) {
/* .locals 1 */
/* .line 176 */
this.this$0 = p1;
/* invoke-direct {p0}, Ljava/lang/Object;-><init>()V */
/* .line 171 */
/* new-instance p1, Ljava/lang/Object; */
/* invoke-direct {p1}, Ljava/lang/Object;-><init>()V */
this.mLock = p1;
/* .line 173 */
/* new-instance p1, Landroid/util/SparseArray; */
int v0 = 4; // const/4 v0, 0x4
/* invoke-direct {p1, v0}, Landroid/util/SparseArray;-><init>(I)V */
this.mMultiTaskActionListeners = p1;
/* .line 177 */
return;
} // .end method
 com.miui.server.smartpower.SmartWindowPolicyManager$MultiTaskPolicyController ( ) { //synthethic
/* .locals 0 */
/* invoke-direct {p0, p1}, Lcom/miui/server/smartpower/SmartWindowPolicyManager$MultiTaskPolicyController;-><init>(Lcom/miui/server/smartpower/SmartWindowPolicyManager;)V */
return;
} // .end method
private Boolean isValidMultiTaskListenerFlag ( Integer p0 ) {
/* .locals 1 */
/* .param p1, "listenerFlag" # I */
/* .line 268 */
/* if-lez p1, :cond_0 */
int v0 = 3; // const/4 v0, 0x3
/* if-ge p1, v0, :cond_0 */
int v0 = 1; // const/4 v0, 0x1
} // :cond_0
int v0 = 0; // const/4 v0, 0x0
} // :goto_0
} // .end method
private java.lang.String listenerFlagToString ( Integer p0 ) {
/* .locals 1 */
/* .param p1, "listenerFlag" # I */
/* .line 273 */
/* packed-switch p1, :pswitch_data_0 */
/* .line 279 */
/* const-string/jumbo v0, "unknown listener" */
/* .line 277 */
/* :pswitch_0 */
final String v0 = "multi scene"; // const-string v0, "multi scene"
/* .line 275 */
/* :pswitch_1 */
final String v0 = "sched boost"; // const-string v0, "sched boost"
/* nop */
/* :pswitch_data_0 */
/* .packed-switch 0x1 */
/* :pswitch_1 */
/* :pswitch_0 */
} // .end packed-switch
} // .end method
/* # virtual methods */
public void dispatchMultiTaskActionEvent ( Integer p0, miui.smartpower.MultiTaskActionManager$ActionInfo p1 ) {
/* .locals 4 */
/* .param p1, "event" # I */
/* .param p2, "actionInfo" # Lmiui/smartpower/MultiTaskActionManager$ActionInfo; */
/* .line 258 */
v0 = this.mLock;
/* monitor-enter v0 */
/* .line 259 */
try { // :try_start_0
v1 = this.mMultiTaskActionListeners;
v1 = (( android.util.SparseArray ) v1 ).size ( ); // invoke-virtual {v1}, Landroid/util/SparseArray;->size()I
/* .line 260 */
/* .local v1, "numListeners":I */
int v2 = 0; // const/4 v2, 0x0
/* .local v2, "i":I */
} // :goto_0
/* if-ge v2, v1, :cond_0 */
/* .line 261 */
v3 = this.mMultiTaskActionListeners;
(( android.util.SparseArray ) v3 ).valueAt ( v2 ); // invoke-virtual {v3, v2}, Landroid/util/SparseArray;->valueAt(I)Ljava/lang/Object;
/* check-cast v3, Lcom/miui/server/smartpower/SmartWindowPolicyManager$MultiTaskPolicyController$MultiTaskActionListenerDelegate; */
/* .line 262 */
(( com.miui.server.smartpower.SmartWindowPolicyManager$MultiTaskPolicyController$MultiTaskActionListenerDelegate ) v3 ).sendMultiTaskActionEvent ( p1, p2 ); // invoke-virtual {v3, p1, p2}, Lcom/miui/server/smartpower/SmartWindowPolicyManager$MultiTaskPolicyController$MultiTaskActionListenerDelegate;->sendMultiTaskActionEvent(ILmiui/smartpower/MultiTaskActionManager$ActionInfo;)V
/* .line 260 */
/* add-int/lit8 v2, v2, 0x1 */
/* .line 264 */
} // .end local v1 # "numListeners":I
} // .end local v2 # "i":I
} // :cond_0
/* monitor-exit v0 */
/* .line 265 */
return;
/* .line 264 */
/* :catchall_0 */
/* move-exception v1 */
/* monitor-exit v0 */
/* :try_end_0 */
/* .catchall {:try_start_0 ..:try_end_0} :catchall_0 */
/* throw v1 */
} // .end method
public void dump ( java.io.PrintWriter p0 ) {
/* .locals 3 */
/* .param p1, "pw" # Ljava/io/PrintWriter; */
/* .line 331 */
final String v0 = "MultiTaskPolicyController Dump:"; // const-string v0, "MultiTaskPolicyController Dump:"
(( java.io.PrintWriter ) p1 ).println ( v0 ); // invoke-virtual {p1, v0}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V
/* .line 332 */
final String v0 = " Multi-Task Action Listeners:"; // const-string v0, " Multi-Task Action Listeners:"
(( java.io.PrintWriter ) p1 ).println ( v0 ); // invoke-virtual {p1, v0}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V
/* .line 333 */
v0 = this.mMultiTaskActionListeners;
v0 = (( android.util.SparseArray ) v0 ).size ( ); // invoke-virtual {v0}, Landroid/util/SparseArray;->size()I
/* if-nez v0, :cond_0 */
/* .line 334 */
final String v0 = " None"; // const-string v0, " None"
(( java.io.PrintWriter ) p1 ).println ( v0 ); // invoke-virtual {p1, v0}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V
/* .line 336 */
} // :cond_0
int v0 = 0; // const/4 v0, 0x0
/* .local v0, "i":I */
} // :goto_0
v1 = this.mMultiTaskActionListeners;
v1 = (( android.util.SparseArray ) v1 ).size ( ); // invoke-virtual {v1}, Landroid/util/SparseArray;->size()I
/* if-ge v0, v1, :cond_1 */
/* .line 337 */
java.lang.Integer .valueOf ( v0 );
v2 = this.mMultiTaskActionListeners;
/* .line 338 */
(( android.util.SparseArray ) v2 ).valueAt ( v0 ); // invoke-virtual {v2, v0}, Landroid/util/SparseArray;->valueAt(I)Ljava/lang/Object;
/* check-cast v2, Lcom/miui/server/smartpower/SmartWindowPolicyManager$MultiTaskPolicyController$MultiTaskActionListenerDelegate; */
(( com.miui.server.smartpower.SmartWindowPolicyManager$MultiTaskPolicyController$MultiTaskActionListenerDelegate ) v2 ).getSourceName ( ); // invoke-virtual {v2}, Lcom/miui/server/smartpower/SmartWindowPolicyManager$MultiTaskPolicyController$MultiTaskActionListenerDelegate;->getSourceName()Ljava/lang/String;
/* filled-new-array {v1, v2}, [Ljava/lang/Object; */
/* .line 337 */
final String v2 = " [%d]: %s\n"; // const-string v2, " [%d]: %s\n"
(( java.io.PrintWriter ) p1 ).printf ( v2, v1 ); // invoke-virtual {p1, v2, v1}, Ljava/io/PrintWriter;->printf(Ljava/lang/String;[Ljava/lang/Object;)Ljava/io/PrintWriter;
/* .line 336 */
/* add-int/lit8 v0, v0, 0x1 */
/* .line 341 */
} // .end local v0 # "i":I
} // :cond_1
} // :goto_1
return;
} // .end method
public void notifyMultiTaskActionChanged ( Integer p0, miui.smartpower.MultiTaskActionManager$ActionInfo p1 ) {
/* .locals 4 */
/* .param p1, "event" # I */
/* .param p2, "actionInfo" # Lmiui/smartpower/MultiTaskActionManager$ActionInfo; */
/* .line 238 */
(( miui.smartpower.MultiTaskActionManager$ActionInfo ) p2 ).getDrawnTids ( ); // invoke-virtual {p2}, Lmiui/smartpower/MultiTaskActionManager$ActionInfo;->getDrawnTids()[I
/* .line 239 */
/* .local v0, "tids":[I */
int v1 = 1; // const/4 v1, 0x1
/* .line 240 */
/* .local v1, "boostType":I */
v2 = (( miui.smartpower.MultiTaskActionManager$ActionInfo ) p2 ).getCallingPid ( ); // invoke-virtual {p2}, Lmiui/smartpower/MultiTaskActionManager$ActionInfo;->getCallingPid()I
/* .line 242 */
/* .local v2, "callingPid":I */
int v3 = 1; // const/4 v3, 0x1
/* if-ne p1, v3, :cond_0 */
/* .line 244 */
v3 = this.this$0;
com.miui.server.smartpower.SmartWindowPolicyManager .-$$Nest$fgetmSmartDisplayPolicyManager ( v3 );
(( com.miui.server.smartpower.SmartDisplayPolicyManager ) v3 ).notifyMultiTaskActionStart ( p2 ); // invoke-virtual {v3, p2}, Lcom/miui/server/smartpower/SmartDisplayPolicyManager;->notifyMultiTaskActionStart(Lmiui/smartpower/MultiTaskActionManager$ActionInfo;)V
/* .line 246 */
v3 = this.this$0;
com.miui.server.smartpower.SmartWindowPolicyManager .-$$Nest$fgetmSmartBoostPolicyManager ( v3 );
(( com.miui.server.smartpower.SmartBoostPolicyManager ) v3 ).beginSchedThreads ( v0, v2, v1 ); // invoke-virtual {v3, v0, v2, v1}, Lcom/miui/server/smartpower/SmartBoostPolicyManager;->beginSchedThreads([III)V
/* .line 247 */
} // :cond_0
int v3 = 2; // const/4 v3, 0x2
/* if-ne p1, v3, :cond_1 */
/* .line 249 */
v3 = this.this$0;
com.miui.server.smartpower.SmartWindowPolicyManager .-$$Nest$fgetmSmartDisplayPolicyManager ( v3 );
(( com.miui.server.smartpower.SmartDisplayPolicyManager ) v3 ).notifyMultiTaskActionEnd ( p2 ); // invoke-virtual {v3, p2}, Lcom/miui/server/smartpower/SmartDisplayPolicyManager;->notifyMultiTaskActionEnd(Lmiui/smartpower/MultiTaskActionManager$ActionInfo;)V
/* .line 251 */
v3 = this.this$0;
com.miui.server.smartpower.SmartWindowPolicyManager .-$$Nest$fgetmSmartBoostPolicyManager ( v3 );
(( com.miui.server.smartpower.SmartBoostPolicyManager ) v3 ).stopCurrentSchedBoost ( v0, v2, v1 ); // invoke-virtual {v3, v0, v2, v1}, Lcom/miui/server/smartpower/SmartBoostPolicyManager;->stopCurrentSchedBoost([III)V
/* .line 254 */
} // :cond_1
} // :goto_0
(( com.miui.server.smartpower.SmartWindowPolicyManager$MultiTaskPolicyController ) p0 ).dispatchMultiTaskActionEvent ( p1, p2 ); // invoke-virtual {p0, p1, p2}, Lcom/miui/server/smartpower/SmartWindowPolicyManager$MultiTaskPolicyController;->dispatchMultiTaskActionEvent(ILmiui/smartpower/MultiTaskActionManager$ActionInfo;)V
/* .line 255 */
return;
} // .end method
public Boolean registerMultiTaskActionListener ( Integer p0, android.os.Handler p1, com.miui.app.smartpower.SmartPowerServiceInternal$MultiTaskActionListener p2 ) {
/* .locals 5 */
/* .param p1, "listenerFlag" # I */
/* .param p2, "handler" # Landroid/os/Handler; */
/* .param p3, "listener" # Lcom/miui/app/smartpower/SmartPowerServiceInternal$MultiTaskActionListener; */
/* .line 181 */
v0 = this.mLock;
/* monitor-enter v0 */
/* .line 182 */
try { // :try_start_0
v1 = /* invoke-direct {p0, p1}, Lcom/miui/server/smartpower/SmartWindowPolicyManager$MultiTaskPolicyController;->isValidMultiTaskListenerFlag(I)Z */
int v2 = 0; // const/4 v2, 0x0
/* if-nez v1, :cond_0 */
/* .line 183 */
final String v1 = "SmartPower.WindowPolicy"; // const-string v1, "SmartPower.WindowPolicy"
/* const-string/jumbo v3, "the listener flag is invalid." */
android.util.Slog .d ( v1,v3 );
/* .line 184 */
/* monitor-exit v0 */
/* .line 187 */
} // :cond_0
if ( p2 != null) { // if-eqz p2, :cond_3
/* if-nez p3, :cond_1 */
/* .line 192 */
} // :cond_1
(( android.os.Handler ) p2 ).getLooper ( ); // invoke-virtual {p2}, Landroid/os/Handler;->getLooper()Landroid/os/Looper;
/* .line 193 */
/* .local v1, "looper":Landroid/os/Looper; */
/* if-nez v1, :cond_2 */
/* .line 194 */
final String v3 = "SmartPower.WindowPolicy"; // const-string v3, "SmartPower.WindowPolicy"
/* const-string/jumbo v4, "unable to obtain the loop corresponding to the handler." */
android.util.Slog .d ( v3,v4 );
/* .line 195 */
/* monitor-exit v0 */
/* .line 198 */
} // :cond_2
/* new-instance v2, Lcom/miui/server/smartpower/SmartWindowPolicyManager$MultiTaskPolicyController$MultiTaskActionListenerDelegate; */
/* .line 200 */
/* invoke-direct {p0, p1}, Lcom/miui/server/smartpower/SmartWindowPolicyManager$MultiTaskPolicyController;->listenerFlagToString(I)Ljava/lang/String; */
/* invoke-direct {v2, v3, v1, p3}, Lcom/miui/server/smartpower/SmartWindowPolicyManager$MultiTaskPolicyController$MultiTaskActionListenerDelegate;-><init>(Ljava/lang/String;Landroid/os/Looper;Lcom/miui/app/smartpower/SmartPowerServiceInternal$MultiTaskActionListener;)V */
/* .line 203 */
/* .local v2, "listenerDelegate":Lcom/miui/server/smartpower/SmartWindowPolicyManager$MultiTaskPolicyController$MultiTaskActionListenerDelegate; */
v3 = this.mMultiTaskActionListeners;
(( android.util.SparseArray ) v3 ).put ( p1, v2 ); // invoke-virtual {v3, p1, v2}, Landroid/util/SparseArray;->put(ILjava/lang/Object;)V
/* .line 204 */
/* monitor-exit v0 */
int v0 = 1; // const/4 v0, 0x1
/* .line 188 */
} // .end local v1 # "looper":Landroid/os/Looper;
} // .end local v2 # "listenerDelegate":Lcom/miui/server/smartpower/SmartWindowPolicyManager$MultiTaskPolicyController$MultiTaskActionListenerDelegate;
} // :cond_3
} // :goto_0
final String v1 = "SmartPower.WindowPolicy"; // const-string v1, "SmartPower.WindowPolicy"
final String v3 = "listener or handler must not be null."; // const-string v3, "listener or handler must not be null."
android.util.Slog .d ( v1,v3 );
/* .line 189 */
/* monitor-exit v0 */
/* .line 205 */
/* :catchall_0 */
/* move-exception v1 */
/* monitor-exit v0 */
/* :try_end_0 */
/* .catchall {:try_start_0 ..:try_end_0} :catchall_0 */
/* throw v1 */
} // .end method
public Boolean unregisterMultiTaskActionListener ( Integer p0, com.miui.app.smartpower.SmartPowerServiceInternal$MultiTaskActionListener p1 ) {
/* .locals 5 */
/* .param p1, "listenerFlag" # I */
/* .param p2, "listener" # Lcom/miui/app/smartpower/SmartPowerServiceInternal$MultiTaskActionListener; */
/* .line 210 */
v0 = this.mLock;
/* monitor-enter v0 */
/* .line 211 */
try { // :try_start_0
v1 = /* invoke-direct {p0, p1}, Lcom/miui/server/smartpower/SmartWindowPolicyManager$MultiTaskPolicyController;->isValidMultiTaskListenerFlag(I)Z */
int v2 = 0; // const/4 v2, 0x0
/* if-nez v1, :cond_0 */
/* .line 212 */
final String v1 = "SmartPower.WindowPolicy"; // const-string v1, "SmartPower.WindowPolicy"
/* const-string/jumbo v3, "the listener flag is invalid." */
android.util.Slog .d ( v1,v3 );
/* .line 213 */
/* monitor-exit v0 */
/* .line 216 */
} // :cond_0
/* if-nez p2, :cond_1 */
/* .line 217 */
final String v1 = "SmartPower.WindowPolicy"; // const-string v1, "SmartPower.WindowPolicy"
final String v3 = "listener must not be null."; // const-string v3, "listener must not be null."
android.util.Slog .d ( v1,v3 );
/* .line 218 */
/* monitor-exit v0 */
/* .line 221 */
} // :cond_1
v1 = this.mMultiTaskActionListeners;
/* .line 222 */
(( android.util.SparseArray ) v1 ).get ( p1 ); // invoke-virtual {v1, p1}, Landroid/util/SparseArray;->get(I)Ljava/lang/Object;
/* check-cast v1, Lcom/miui/server/smartpower/SmartWindowPolicyManager$MultiTaskPolicyController$MultiTaskActionListenerDelegate; */
/* .line 223 */
/* .local v1, "listenerDelegate":Lcom/miui/server/smartpower/SmartWindowPolicyManager$MultiTaskPolicyController$MultiTaskActionListenerDelegate; */
/* if-nez v1, :cond_2 */
/* .line 224 */
/* monitor-exit v0 */
/* .line 226 */
} // :cond_2
(( com.miui.server.smartpower.SmartWindowPolicyManager$MultiTaskPolicyController$MultiTaskActionListenerDelegate ) v1 ).getListener ( ); // invoke-virtual {v1}, Lcom/miui/server/smartpower/SmartWindowPolicyManager$MultiTaskPolicyController$MultiTaskActionListenerDelegate;->getListener()Lcom/miui/app/smartpower/SmartPowerServiceInternal$MultiTaskActionListener;
/* if-eq v3, p2, :cond_3 */
/* .line 227 */
final String v3 = "SmartPower.WindowPolicy"; // const-string v3, "SmartPower.WindowPolicy"
/* const-string/jumbo v4, "the listener flag is not match the listener." */
android.util.Slog .d ( v3,v4 );
/* .line 228 */
/* monitor-exit v0 */
/* .line 231 */
} // :cond_3
(( com.miui.server.smartpower.SmartWindowPolicyManager$MultiTaskPolicyController$MultiTaskActionListenerDelegate ) v1 ).clearEvents ( ); // invoke-virtual {v1}, Lcom/miui/server/smartpower/SmartWindowPolicyManager$MultiTaskPolicyController$MultiTaskActionListenerDelegate;->clearEvents()V
/* .line 232 */
v2 = this.mMultiTaskActionListeners;
(( android.util.SparseArray ) v2 ).remove ( p1 ); // invoke-virtual {v2, p1}, Landroid/util/SparseArray;->remove(I)V
/* .line 233 */
/* monitor-exit v0 */
int v0 = 1; // const/4 v0, 0x1
/* .line 234 */
} // .end local v1 # "listenerDelegate":Lcom/miui/server/smartpower/SmartWindowPolicyManager$MultiTaskPolicyController$MultiTaskActionListenerDelegate;
/* :catchall_0 */
/* move-exception v1 */
/* monitor-exit v0 */
/* :try_end_0 */
/* .catchall {:try_start_0 ..:try_end_0} :catchall_0 */
/* throw v1 */
} // .end method
