class com.miui.server.smartpower.SmartDisplayPolicyManager$RefreshRatePolicyController {
	 /* .source "SmartDisplayPolicyManager.java" */
	 /* # annotations */
	 /* .annotation system Ldalvik/annotation/EnclosingClass; */
	 /* value = Lcom/miui/server/smartpower/SmartDisplayPolicyManager; */
} // .end annotation
/* .annotation system Ldalvik/annotation/InnerClass; */
/* accessFlags = 0x2 */
/* name = "RefreshRatePolicyController" */
} // .end annotation
/* .annotation system Ldalvik/annotation/MemberClasses; */
/* value = { */
/* Lcom/miui/server/smartpower/SmartDisplayPolicyManager$RefreshRatePolicyController$SettingRefreshRateObserver; */
/* } */
} // .end annotation
/* # static fields */
private static final Integer DEFAULT_MIN_REFRESH_RATE;
private static final Long DELAY_DOWN_FOCUS_WINDOW_CHANGED_TIME;
private static final Long DELAY_DOWN_MULTI_TASK_TIME;
private static final Long DELAY_DOWN_RECENT_ANIMATION_TIME;
private static final Long DELAY_DOWN_REMOTE_ANIMATION_TIME;
private static final Long DELAY_DOWN_TIME;
private static final Long INTERACT_APP_TRANSITION_DUR;
private static final Long INTERACT_DEFAULT_DUR;
private static final Long INTERACT_INSET_ANIMATION_DUR;
private static final Long INTERACT_MULTI_TASK_DUR;
private static final Long INTERACT_RECENT_ANIMATION_DUR;
private static final Long INTERACT_REMOTE_ANIMATION_DUR;
private static final Long INTERACT_WINDOW_ANIMATION_DUR;
private static final Integer INVALID_REFRESH_RATE;
private static final java.lang.String MIUI_DEFAULT_REFRESH_RATE;
private static final java.lang.String MIUI_REFRESH_RATE;
private static final java.lang.String MIUI_THERMAL_LIMIT_REFRESH_RATE;
private static final java.lang.String MIUI_USER_REFRESH_RATE;
private static final Integer NORMAL_MAX_REFRESH_RATE;
private static final Integer SOFTWARE_REFRESH_RATE;
/* # instance fields */
private final Integer LIMIT_MAX_REFRESH_RATE;
private Boolean isThermalWarning;
private final android.content.Context mContext;
private Long mCurrentInteractDuration;
private Long mCurrentInteractEndTime;
private Integer mCurrentInteractRefreshRate;
private Integer mCurrentInteractType;
private Integer mCurrentUserRefreshRate;
private Boolean mDefaultRefreshRateEnable;
private final java.util.Set mFocusedPackageWhiteList;
/* .annotation system Ldalvik/annotation/Signature; */
/* value = { */
/* "Ljava/util/Set<", */
/* "Ljava/lang/String;", */
/* ">;" */
/* } */
} // .end annotation
} // .end field
private final android.os.Handler mHandler;
private final java.util.Set mInputPackageWhiteList;
/* .annotation system Ldalvik/annotation/Signature; */
/* value = { */
/* "Ljava/util/Set<", */
/* "Ljava/lang/String;", */
/* ">;" */
/* } */
} // .end annotation
} // .end field
private final android.util.SparseArray mInteractDelayDownTimes;
/* .annotation system Ldalvik/annotation/Signature; */
/* value = { */
/* "Landroid/util/SparseArray<", */
/* "Ljava/lang/Long;", */
/* ">;" */
/* } */
} // .end annotation
} // .end field
private final android.util.SparseArray mInteractDurations;
/* .annotation system Ldalvik/annotation/Signature; */
/* value = { */
/* "Landroid/util/SparseArray<", */
/* "Ljava/lang/Long;", */
/* ">;" */
/* } */
} // .end annotation
} // .end field
private final java.lang.Object mLock;
private Integer mMaxRefreshRateInPolicy;
private Integer mMaxRefreshRateSupport;
private Integer mMinRefreshRateInPolicy;
private Integer mMinRefreshRateSupport;
private Integer mMiuiRefreshRate;
private com.miui.server.smartpower.SmartDisplayPolicyManager$RefreshRatePolicyController$SettingRefreshRateObserver mSettingRefreshRateObserver;
private final java.util.Set mSupportRefreshRates;
/* .annotation system Ldalvik/annotation/Signature; */
/* value = { */
/* "Ljava/util/Set<", */
/* "Ljava/lang/Integer;", */
/* ">;" */
/* } */
} // .end annotation
} // .end field
private final java.util.Set mSurfaceTextureBlackList;
/* .annotation system Ldalvik/annotation/Signature; */
/* value = { */
/* "Ljava/util/Set<", */
/* "Ljava/lang/String;", */
/* ">;" */
/* } */
} // .end annotation
} // .end field
private final android.util.ArrayMap mWindowVisibleWhiteList;
/* .annotation system Ldalvik/annotation/Signature; */
/* value = { */
/* "Landroid/util/ArrayMap<", */
/* "Ljava/lang/String;", */
/* "Ljava/util/List<", */
/* "Ljava/lang/String;", */
/* ">;>;" */
/* } */
} // .end annotation
} // .end field
final com.miui.server.smartpower.SmartDisplayPolicyManager this$0; //synthetic
/* # direct methods */
static Boolean -$$Nest$fgetisThermalWarning ( com.miui.server.smartpower.SmartDisplayPolicyManager$RefreshRatePolicyController p0 ) { //bridge//synthethic
/* .locals 0 */
/* iget-boolean p0, p0, Lcom/miui/server/smartpower/SmartDisplayPolicyManager$RefreshRatePolicyController;->isThermalWarning:Z */
} // .end method
static void -$$Nest$fputisThermalWarning ( com.miui.server.smartpower.SmartDisplayPolicyManager$RefreshRatePolicyController p0, Boolean p1 ) { //bridge//synthethic
/* .locals 0 */
/* iput-boolean p1, p0, Lcom/miui/server/smartpower/SmartDisplayPolicyManager$RefreshRatePolicyController;->isThermalWarning:Z */
return;
} // .end method
static void -$$Nest$fputmMiuiRefreshRate ( com.miui.server.smartpower.SmartDisplayPolicyManager$RefreshRatePolicyController p0, Integer p1 ) { //bridge//synthethic
/* .locals 0 */
/* iput p1, p0, Lcom/miui/server/smartpower/SmartDisplayPolicyManager$RefreshRatePolicyController;->mMiuiRefreshRate:I */
return;
} // .end method
static void -$$Nest$mdump ( com.miui.server.smartpower.SmartDisplayPolicyManager$RefreshRatePolicyController p0, java.io.PrintWriter p1, java.lang.String[] p2, Integer p3 ) { //bridge//synthethic
/* .locals 0 */
/* invoke-direct {p0, p1, p2, p3}, Lcom/miui/server/smartpower/SmartDisplayPolicyManager$RefreshRatePolicyController;->dump(Ljava/io/PrintWriter;[Ljava/lang/String;I)V */
return;
} // .end method
static Long -$$Nest$mgetDelayDownTimeForInteraction ( com.miui.server.smartpower.SmartDisplayPolicyManager$RefreshRatePolicyController p0, Integer p1 ) { //bridge//synthethic
/* .locals 0 */
/* invoke-direct {p0, p1}, Lcom/miui/server/smartpower/SmartDisplayPolicyManager$RefreshRatePolicyController;->getDelayDownTimeForInteraction(I)J */
/* move-result-wide p0 */
/* return-wide p0 */
} // .end method
static Long -$$Nest$mgetDurationForInteraction ( com.miui.server.smartpower.SmartDisplayPolicyManager$RefreshRatePolicyController p0, Integer p1 ) { //bridge//synthethic
/* .locals 0 */
/* invoke-direct {p0, p1}, Lcom/miui/server/smartpower/SmartDisplayPolicyManager$RefreshRatePolicyController;->getDurationForInteraction(I)J */
/* move-result-wide p0 */
/* return-wide p0 */
} // .end method
static Integer -$$Nest$mgetMaxRefreshRateInPolicy ( com.miui.server.smartpower.SmartDisplayPolicyManager$RefreshRatePolicyController p0 ) { //bridge//synthethic
/* .locals 0 */
p0 = /* invoke-direct {p0}, Lcom/miui/server/smartpower/SmartDisplayPolicyManager$RefreshRatePolicyController;->getMaxRefreshRateInPolicy()I */
} // .end method
static Integer -$$Nest$mgetMinRefreshRateInPolicy ( com.miui.server.smartpower.SmartDisplayPolicyManager$RefreshRatePolicyController p0 ) { //bridge//synthethic
/* .locals 0 */
p0 = /* invoke-direct {p0}, Lcom/miui/server/smartpower/SmartDisplayPolicyManager$RefreshRatePolicyController;->getMinRefreshRateInPolicy()I */
} // .end method
static Boolean -$$Nest$misFocusedPackageWhiteList ( com.miui.server.smartpower.SmartDisplayPolicyManager$RefreshRatePolicyController p0, java.lang.String p1 ) { //bridge//synthethic
/* .locals 0 */
p0 = /* invoke-direct {p0, p1}, Lcom/miui/server/smartpower/SmartDisplayPolicyManager$RefreshRatePolicyController;->isFocusedPackageWhiteList(Ljava/lang/String;)Z */
} // .end method
static Boolean -$$Nest$misInputPackageWhiteList ( com.miui.server.smartpower.SmartDisplayPolicyManager$RefreshRatePolicyController p0, java.lang.String p1 ) { //bridge//synthethic
/* .locals 0 */
p0 = /* invoke-direct {p0, p1}, Lcom/miui/server/smartpower/SmartDisplayPolicyManager$RefreshRatePolicyController;->isInputPackageWhiteList(Ljava/lang/String;)Z */
} // .end method
static Boolean -$$Nest$misWindowVisibleWhiteList ( com.miui.server.smartpower.SmartDisplayPolicyManager$RefreshRatePolicyController p0, com.android.server.policy.WindowManagerPolicy$WindowState p1, android.view.WindowManager$LayoutParams p2 ) { //bridge//synthethic
/* .locals 0 */
p0 = /* invoke-direct {p0, p1, p2}, Lcom/miui/server/smartpower/SmartDisplayPolicyManager$RefreshRatePolicyController;->isWindowVisibleWhiteList(Lcom/android/server/policy/WindowManagerPolicy$WindowState;Landroid/view/WindowManager$LayoutParams;)Z */
} // .end method
static void -$$Nest$mnotifyInteractionEnd ( com.miui.server.smartpower.SmartDisplayPolicyManager$RefreshRatePolicyController p0, com.miui.server.smartpower.SmartDisplayPolicyManager$ComingInteraction p1 ) { //bridge//synthethic
/* .locals 0 */
/* invoke-direct {p0, p1}, Lcom/miui/server/smartpower/SmartDisplayPolicyManager$RefreshRatePolicyController;->notifyInteractionEnd(Lcom/miui/server/smartpower/SmartDisplayPolicyManager$ComingInteraction;)V */
return;
} // .end method
static void -$$Nest$mnotifyInteractionStart ( com.miui.server.smartpower.SmartDisplayPolicyManager$RefreshRatePolicyController p0, com.miui.server.smartpower.SmartDisplayPolicyManager$ComingInteraction p1 ) { //bridge//synthethic
/* .locals 0 */
/* invoke-direct {p0, p1}, Lcom/miui/server/smartpower/SmartDisplayPolicyManager$RefreshRatePolicyController;->notifyInteractionStart(Lcom/miui/server/smartpower/SmartDisplayPolicyManager$ComingInteraction;)V */
return;
} // .end method
static void -$$Nest$mreset ( com.miui.server.smartpower.SmartDisplayPolicyManager$RefreshRatePolicyController p0 ) { //bridge//synthethic
/* .locals 0 */
/* invoke-direct {p0}, Lcom/miui/server/smartpower/SmartDisplayPolicyManager$RefreshRatePolicyController;->reset()V */
return;
} // .end method
static Boolean -$$Nest$mshouldInterceptUpdateDisplayModeSpecs ( com.miui.server.smartpower.SmartDisplayPolicyManager$RefreshRatePolicyController p0, com.android.server.display.mode.DisplayModeDirector$DesiredDisplayModeSpecs p1 ) { //bridge//synthethic
/* .locals 0 */
p0 = /* invoke-direct {p0, p1}, Lcom/miui/server/smartpower/SmartDisplayPolicyManager$RefreshRatePolicyController;->shouldInterceptUpdateDisplayModeSpecs(Lcom/android/server/display/mode/DisplayModeDirector$DesiredDisplayModeSpecs;)Z */
} // .end method
static void -$$Nest$mupdateAppropriateRefreshRate ( com.miui.server.smartpower.SmartDisplayPolicyManager$RefreshRatePolicyController p0 ) { //bridge//synthethic
/* .locals 0 */
/* invoke-direct {p0}, Lcom/miui/server/smartpower/SmartDisplayPolicyManager$RefreshRatePolicyController;->updateAppropriateRefreshRate()V */
return;
} // .end method
static void -$$Nest$mupdateControllerLocked ( com.miui.server.smartpower.SmartDisplayPolicyManager$RefreshRatePolicyController p0 ) { //bridge//synthethic
/* .locals 0 */
/* invoke-direct {p0}, Lcom/miui/server/smartpower/SmartDisplayPolicyManager$RefreshRatePolicyController;->updateControllerLocked()V */
return;
} // .end method
private com.miui.server.smartpower.SmartDisplayPolicyManager$RefreshRatePolicyController ( ) {
/* .locals 10 */
/* .param p2, "context" # Landroid/content/Context; */
/* .param p3, "handler" # Landroid/os/Handler; */
/* .line 1111 */
this.this$0 = p1;
/* invoke-direct {p0}, Ljava/lang/Object;-><init>()V */
/* .line 1011 */
/* new-instance p1, Ljava/lang/Object; */
/* invoke-direct {p1}, Ljava/lang/Object;-><init>()V */
this.mLock = p1;
/* .line 1060 */
/* iput p1, p0, Lcom/miui/server/smartpower/SmartDisplayPolicyManager$RefreshRatePolicyController;->LIMIT_MAX_REFRESH_RATE:I */
/* .line 1065 */
/* new-instance p1, Ljava/util/HashSet; */
/* invoke-direct {p1}, Ljava/util/HashSet;-><init>()V */
this.mFocusedPackageWhiteList = p1;
/* .line 1069 */
/* new-instance v0, Ljava/util/HashSet; */
/* invoke-direct {v0}, Ljava/util/HashSet;-><init>()V */
this.mInputPackageWhiteList = v0;
/* .line 1073 */
/* new-instance v1, Ljava/util/HashSet; */
/* invoke-direct {v1}, Ljava/util/HashSet;-><init>()V */
this.mSurfaceTextureBlackList = v1;
/* .line 1078 */
/* new-instance v2, Landroid/util/ArrayMap; */
/* invoke-direct {v2}, Landroid/util/ArrayMap;-><init>()V */
this.mWindowVisibleWhiteList = v2;
/* .line 1080 */
/* new-instance v3, Ljava/util/HashSet; */
/* invoke-direct {v3}, Ljava/util/HashSet;-><init>()V */
this.mSupportRefreshRates = v3;
/* .line 1082 */
/* new-instance v4, Landroid/util/SparseArray; */
/* const/16 v5, 0x8 */
/* invoke-direct {v4, v5}, Landroid/util/SparseArray;-><init>(I)V */
this.mInteractDurations = v4;
/* .line 1083 */
/* new-instance v5, Landroid/util/SparseArray; */
int v6 = 4; // const/4 v6, 0x4
/* invoke-direct {v5, v6}, Landroid/util/SparseArray;-><init>(I)V */
this.mInteractDelayDownTimes = v5;
/* .line 1085 */
/* const/16 v7, 0x3c */
/* iput v7, p0, Lcom/miui/server/smartpower/SmartDisplayPolicyManager$RefreshRatePolicyController;->mMaxRefreshRateSupport:I */
/* .line 1086 */
/* iput v7, p0, Lcom/miui/server/smartpower/SmartDisplayPolicyManager$RefreshRatePolicyController;->mMinRefreshRateSupport:I */
/* .line 1091 */
/* iput v7, p0, Lcom/miui/server/smartpower/SmartDisplayPolicyManager$RefreshRatePolicyController;->mMaxRefreshRateInPolicy:I */
/* .line 1092 */
int v8 = 0; // const/4 v8, 0x0
/* iput v8, p0, Lcom/miui/server/smartpower/SmartDisplayPolicyManager$RefreshRatePolicyController;->mMinRefreshRateInPolicy:I */
/* .line 1094 */
int v9 = 1; // const/4 v9, 0x1
/* iput-boolean v9, p0, Lcom/miui/server/smartpower/SmartDisplayPolicyManager$RefreshRatePolicyController;->mDefaultRefreshRateEnable:Z */
/* .line 1096 */
/* iput v7, p0, Lcom/miui/server/smartpower/SmartDisplayPolicyManager$RefreshRatePolicyController;->mCurrentUserRefreshRate:I */
/* .line 1101 */
int v9 = 0; // const/4 v9, 0x0
this.mSettingRefreshRateObserver = v9;
/* .line 1108 */
/* iput v7, p0, Lcom/miui/server/smartpower/SmartDisplayPolicyManager$RefreshRatePolicyController;->mMiuiRefreshRate:I */
/* .line 1109 */
/* iput-boolean v8, p0, Lcom/miui/server/smartpower/SmartDisplayPolicyManager$RefreshRatePolicyController;->isThermalWarning:Z */
/* .line 1112 */
this.mContext = p2;
/* .line 1113 */
this.mHandler = p3;
/* .line 1115 */
java.lang.Integer .valueOf ( v7 );
/* .line 1117 */
/* new-instance v3, Lcom/miui/server/smartpower/SmartDisplayPolicyManager$RefreshRatePolicyController$SettingRefreshRateObserver; */
/* invoke-direct {v3, p0, p2, p3}, Lcom/miui/server/smartpower/SmartDisplayPolicyManager$RefreshRatePolicyController$SettingRefreshRateObserver;-><init>(Lcom/miui/server/smartpower/SmartDisplayPolicyManager$RefreshRatePolicyController;Landroid/content/Context;Landroid/os/Handler;)V */
this.mSettingRefreshRateObserver = v3;
/* .line 1119 */
final String v3 = "com.android.systemui"; // const-string v3, "com.android.systemui"
/* .line 1121 */
/* .line 1122 */
final String p1 = "com.miui.home"; // const-string p1, "com.miui.home"
/* .line 1123 */
final String p1 = "com.mi.android.globallauncher"; // const-string p1, "com.mi.android.globallauncher"
/* .line 1125 */
final String p1 = "com.miui.mediaviewer"; // const-string p1, "com.miui.mediaviewer"
/* .line 1128 */
final String p1 = "ScreenshotAnimation"; // const-string p1, "ScreenshotAnimation"
java.util.List .of ( p1 );
final String v0 = "com.miui.screenshot"; // const-string v0, "com.miui.screenshot"
(( android.util.ArrayMap ) v2 ).put ( v0, p1 ); // invoke-virtual {v2, v0, p1}, Landroid/util/ArrayMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;
/* .line 1130 */
/* const-wide/16 v0, 0x14 */
java.lang.Long .valueOf ( v0,v1 );
int v0 = 2; // const/4 v0, 0x2
(( android.util.SparseArray ) v4 ).put ( v0, p1 ); // invoke-virtual {v4, v0, p1}, Landroid/util/SparseArray;->put(ILjava/lang/Object;)V
/* .line 1131 */
int v0 = 3; // const/4 v0, 0x3
(( android.util.SparseArray ) v4 ).put ( v0, p1 ); // invoke-virtual {v4, v0, p1}, Landroid/util/SparseArray;->put(ILjava/lang/Object;)V
/* .line 1132 */
/* const-wide/16 v0, 0x12c */
java.lang.Long .valueOf ( v0,v1 );
(( android.util.SparseArray ) v4 ).put ( v6, p1 ); // invoke-virtual {v4, v6, p1}, Landroid/util/SparseArray;->put(ILjava/lang/Object;)V
/* .line 1133 */
/* const-wide/16 v0, 0x7530 */
java.lang.Long .valueOf ( v0,v1 );
int v0 = 5; // const/4 v0, 0x5
(( android.util.SparseArray ) v4 ).put ( v0, p1 ); // invoke-virtual {v4, v0, p1}, Landroid/util/SparseArray;->put(ILjava/lang/Object;)V
/* .line 1134 */
int v1 = 6; // const/4 v1, 0x6
(( android.util.SparseArray ) v4 ).put ( v1, p1 ); // invoke-virtual {v4, v1, p1}, Landroid/util/SparseArray;->put(ILjava/lang/Object;)V
/* .line 1135 */
/* const/16 v2, 0xc */
(( android.util.SparseArray ) v4 ).put ( v2, p1 ); // invoke-virtual {v4, v2, p1}, Landroid/util/SparseArray;->put(ILjava/lang/Object;)V
/* .line 1136 */
/* const/16 v3, 0xd */
(( android.util.SparseArray ) v4 ).put ( v3, p1 ); // invoke-virtual {v4, v3, p1}, Landroid/util/SparseArray;->put(ILjava/lang/Object;)V
/* .line 1137 */
/* const/16 v6, 0xe */
(( android.util.SparseArray ) v4 ).put ( v6, p1 ); // invoke-virtual {v4, v6, p1}, Landroid/util/SparseArray;->put(ILjava/lang/Object;)V
/* .line 1138 */
/* const/16 v7, 0xf */
(( android.util.SparseArray ) v4 ).put ( v7, p1 ); // invoke-virtual {v4, v7, p1}, Landroid/util/SparseArray;->put(ILjava/lang/Object;)V
/* .line 1140 */
/* nop */
/* .line 1141 */
/* const-wide/16 v8, 0x1f4 */
java.lang.Long .valueOf ( v8,v9 );
/* .line 1140 */
(( android.util.SparseArray ) v5 ).put ( v0, p1 ); // invoke-virtual {v5, v0, p1}, Landroid/util/SparseArray;->put(ILjava/lang/Object;)V
/* .line 1142 */
/* nop */
/* .line 1143 */
/* nop */
/* .line 1142 */
(( android.util.SparseArray ) v5 ).put ( v1, p1 ); // invoke-virtual {v5, v1, p1}, Landroid/util/SparseArray;->put(ILjava/lang/Object;)V
/* .line 1144 */
/* nop */
/* .line 1145 */
/* nop */
/* .line 1144 */
int v0 = 7; // const/4 v0, 0x7
(( android.util.SparseArray ) v5 ).put ( v0, p1 ); // invoke-virtual {v5, v0, p1}, Landroid/util/SparseArray;->put(ILjava/lang/Object;)V
/* .line 1146 */
(( android.util.SparseArray ) v5 ).put ( v2, p1 ); // invoke-virtual {v5, v2, p1}, Landroid/util/SparseArray;->put(ILjava/lang/Object;)V
/* .line 1147 */
(( android.util.SparseArray ) v5 ).put ( v3, p1 ); // invoke-virtual {v5, v3, p1}, Landroid/util/SparseArray;->put(ILjava/lang/Object;)V
/* .line 1148 */
(( android.util.SparseArray ) v5 ).put ( v6, p1 ); // invoke-virtual {v5, v6, p1}, Landroid/util/SparseArray;->put(ILjava/lang/Object;)V
/* .line 1149 */
(( android.util.SparseArray ) v5 ).put ( v7, p1 ); // invoke-virtual {v5, v7, p1}, Landroid/util/SparseArray;->put(ILjava/lang/Object;)V
/* .line 1150 */
return;
} // .end method
 com.miui.server.smartpower.SmartDisplayPolicyManager$RefreshRatePolicyController ( ) { //synthethic
/* .locals 0 */
/* invoke-direct {p0, p1, p2, p3}, Lcom/miui/server/smartpower/SmartDisplayPolicyManager$RefreshRatePolicyController;-><init>(Lcom/miui/server/smartpower/SmartDisplayPolicyManager;Landroid/content/Context;Landroid/os/Handler;)V */
return;
} // .end method
private void dropCurrentInteraction ( ) {
/* .locals 2 */
/* .line 1379 */
v0 = this.mHandler;
int v1 = 1; // const/4 v1, 0x1
(( android.os.Handler ) v0 ).removeMessages ( v1 ); // invoke-virtual {v0, v1}, Landroid/os/Handler;->removeMessages(I)V
/* .line 1380 */
v0 = this.mHandler;
int v1 = 2; // const/4 v1, 0x2
(( android.os.Handler ) v0 ).removeMessages ( v1 ); // invoke-virtual {v0, v1}, Landroid/os/Handler;->removeMessages(I)V
/* .line 1381 */
return;
} // .end method
private void dump ( java.io.PrintWriter p0, java.lang.String[] p1, Integer p2 ) {
/* .locals 3 */
/* .param p1, "pw" # Ljava/io/PrintWriter; */
/* .param p2, "args" # [Ljava/lang/String; */
/* .param p3, "opti" # I */
/* .line 1443 */
final String v0 = "RefreshRatePolicyController"; // const-string v0, "RefreshRatePolicyController"
(( java.io.PrintWriter ) p1 ).println ( v0 ); // invoke-virtual {p1, v0}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V
/* .line 1444 */
/* new-instance v0, Ljava/lang/StringBuilder; */
/* invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V */
/* const-string/jumbo v1, "status: " */
(( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
v1 = /* invoke-direct {p0}, Lcom/miui/server/smartpower/SmartDisplayPolicyManager$RefreshRatePolicyController;->isEnable()Z */
(( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v0 ).toString ( ); // invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
(( java.io.PrintWriter ) p1 ).println ( v0 ); // invoke-virtual {p1, v0}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V
/* .line 1445 */
/* new-instance v0, Ljava/lang/StringBuilder; */
/* invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V */
final String v1 = "refresh rate support: "; // const-string v1, "refresh rate support: "
(( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
v1 = this.mSupportRefreshRates;
(( java.lang.Object ) v1 ).toString ( ); // invoke-virtual {v1}, Ljava/lang/Object;->toString()Ljava/lang/String;
(( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v0 ).toString ( ); // invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
(( java.io.PrintWriter ) p1 ).println ( v0 ); // invoke-virtual {p1, v0}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V
/* .line 1446 */
/* new-instance v0, Ljava/lang/StringBuilder; */
/* invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V */
final String v1 = "default refresh rate: "; // const-string v1, "default refresh rate: "
(( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
/* iget-boolean v1, p0, Lcom/miui/server/smartpower/SmartDisplayPolicyManager$RefreshRatePolicyController;->mDefaultRefreshRateEnable:Z */
(( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v0 ).toString ( ); // invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
(( java.io.PrintWriter ) p1 ).println ( v0 ); // invoke-virtual {p1, v0}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V
/* .line 1447 */
/* new-instance v0, Ljava/lang/StringBuilder; */
/* invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V */
/* const-string/jumbo v1, "user refresh rate: " */
(( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
/* iget v1, p0, Lcom/miui/server/smartpower/SmartDisplayPolicyManager$RefreshRatePolicyController;->mCurrentUserRefreshRate:I */
(( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v0 ).toString ( ); // invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
(( java.io.PrintWriter ) p1 ).println ( v0 ); // invoke-virtual {p1, v0}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V
/* .line 1448 */
/* new-instance v0, Ljava/lang/StringBuilder; */
/* invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V */
final String v1 = "max refresh rate in policy: "; // const-string v1, "max refresh rate in policy: "
(( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
/* iget v1, p0, Lcom/miui/server/smartpower/SmartDisplayPolicyManager$RefreshRatePolicyController;->mMaxRefreshRateInPolicy:I */
(( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v0 ).toString ( ); // invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
(( java.io.PrintWriter ) p1 ).println ( v0 ); // invoke-virtual {p1, v0}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V
/* .line 1449 */
/* new-instance v0, Ljava/lang/StringBuilder; */
/* invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V */
final String v1 = "min refresh rate in policy: "; // const-string v1, "min refresh rate in policy: "
(( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
/* iget v1, p0, Lcom/miui/server/smartpower/SmartDisplayPolicyManager$RefreshRatePolicyController;->mMinRefreshRateInPolicy:I */
(( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v0 ).toString ( ); // invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
(( java.io.PrintWriter ) p1 ).println ( v0 ); // invoke-virtual {p1, v0}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V
/* .line 1450 */
/* new-instance v0, Ljava/lang/StringBuilder; */
/* invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V */
final String v1 = "current type: "; // const-string v1, "current type: "
(( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
/* iget v1, p0, Lcom/miui/server/smartpower/SmartDisplayPolicyManager$RefreshRatePolicyController;->mCurrentInteractType:I */
com.miui.server.smartpower.SmartDisplayPolicyManager .-$$Nest$sminteractionTypeToString ( v1 );
(( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v0 ).toString ( ); // invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
(( java.io.PrintWriter ) p1 ).println ( v0 ); // invoke-virtual {p1, v0}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V
/* .line 1451 */
/* new-instance v0, Ljava/lang/StringBuilder; */
/* invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V */
final String v1 = "current interact refresh rate: "; // const-string v1, "current interact refresh rate: "
(( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
/* iget v1, p0, Lcom/miui/server/smartpower/SmartDisplayPolicyManager$RefreshRatePolicyController;->mCurrentInteractRefreshRate:I */
(( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v0 ).toString ( ); // invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
(( java.io.PrintWriter ) p1 ).println ( v0 ); // invoke-virtual {p1, v0}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V
/* .line 1452 */
/* new-instance v0, Ljava/lang/StringBuilder; */
/* invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V */
final String v1 = "current interact duration: "; // const-string v1, "current interact duration: "
(( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
/* iget-wide v1, p0, Lcom/miui/server/smartpower/SmartDisplayPolicyManager$RefreshRatePolicyController;->mCurrentInteractDuration:J */
(( java.lang.StringBuilder ) v0 ).append ( v1, v2 ); // invoke-virtual {v0, v1, v2}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v0 ).toString ( ); // invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
(( java.io.PrintWriter ) p1 ).println ( v0 ); // invoke-virtual {p1, v0}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V
/* .line 1453 */
/* new-instance v0, Ljava/lang/StringBuilder; */
/* invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V */
final String v1 = "mCurrentInteractEndTime: "; // const-string v1, "mCurrentInteractEndTime: "
(( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
/* iget-wide v1, p0, Lcom/miui/server/smartpower/SmartDisplayPolicyManager$RefreshRatePolicyController;->mCurrentInteractEndTime:J */
(( java.lang.StringBuilder ) v0 ).append ( v1, v2 ); // invoke-virtual {v0, v1, v2}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v0 ).toString ( ); // invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
(( java.io.PrintWriter ) p1 ).println ( v0 ); // invoke-virtual {p1, v0}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V
/* .line 1454 */
return;
} // .end method
private Long getDelayDownTimeForInteraction ( Integer p0 ) {
/* .locals 3 */
/* .param p1, "interactType" # I */
/* .line 1374 */
v0 = this.mInteractDelayDownTimes;
(( android.util.SparseArray ) v0 ).get ( p1 ); // invoke-virtual {v0, p1}, Landroid/util/SparseArray;->get(I)Ljava/lang/Object;
/* check-cast v0, Ljava/lang/Long; */
/* .line 1375 */
/* .local v0, "interactDelayDownTime":Ljava/lang/Long; */
if ( v0 != null) { // if-eqz v0, :cond_0
(( java.lang.Long ) v0 ).longValue ( ); // invoke-virtual {v0}, Ljava/lang/Long;->longValue()J
/* move-result-wide v1 */
} // :cond_0
/* const-wide/16 v1, 0x0 */
} // :goto_0
/* return-wide v1 */
} // .end method
private Long getDurationForInteraction ( Integer p0 ) {
/* .locals 3 */
/* .param p1, "interactType" # I */
/* .line 1369 */
v0 = this.mInteractDurations;
(( android.util.SparseArray ) v0 ).get ( p1 ); // invoke-virtual {v0, p1}, Landroid/util/SparseArray;->get(I)Ljava/lang/Object;
/* check-cast v0, Ljava/lang/Long; */
/* .line 1370 */
/* .local v0, "interactDuration":Ljava/lang/Long; */
if ( v0 != null) { // if-eqz v0, :cond_0
(( java.lang.Long ) v0 ).longValue ( ); // invoke-virtual {v0}, Ljava/lang/Long;->longValue()J
/* move-result-wide v1 */
} // :cond_0
/* const-wide/16 v1, 0x3e8 */
} // :goto_0
/* return-wide v1 */
} // .end method
private Integer getMaxRefreshRateInPolicy ( ) {
/* .locals 1 */
/* .line 1398 */
/* iget v0, p0, Lcom/miui/server/smartpower/SmartDisplayPolicyManager$RefreshRatePolicyController;->mMaxRefreshRateInPolicy:I */
} // .end method
private Integer getMinRefreshRateInPolicy ( ) {
/* .locals 1 */
/* .line 1402 */
/* iget v0, p0, Lcom/miui/server/smartpower/SmartDisplayPolicyManager$RefreshRatePolicyController;->mMinRefreshRateInPolicy:I */
} // .end method
private Boolean isAniminting ( ) {
/* .locals 4 */
/* .line 1365 */
/* iget-wide v0, p0, Lcom/miui/server/smartpower/SmartDisplayPolicyManager$RefreshRatePolicyController;->mCurrentInteractEndTime:J */
/* const-wide/16 v2, 0x0 */
/* cmp-long v0, v0, v2 */
if ( v0 != null) { // if-eqz v0, :cond_0
int v0 = 1; // const/4 v0, 0x1
} // :cond_0
int v0 = 0; // const/4 v0, 0x0
} // :goto_0
} // .end method
private Boolean isEnable ( ) {
/* .locals 2 */
/* .line 1393 */
v0 = com.miui.server.smartpower.SmartDisplayPolicyManager .-$$Nest$sfgetENABLE ( );
if ( v0 != null) { // if-eqz v0, :cond_0
/* iget v0, p0, Lcom/miui/server/smartpower/SmartDisplayPolicyManager$RefreshRatePolicyController;->mMaxRefreshRateInPolicy:I */
/* iget v1, p0, Lcom/miui/server/smartpower/SmartDisplayPolicyManager$RefreshRatePolicyController;->mMinRefreshRateSupport:I */
/* if-eq v0, v1, :cond_0 */
int v0 = 1; // const/4 v0, 0x1
} // :cond_0
int v0 = 0; // const/4 v0, 0x0
} // :goto_0
} // .end method
private Boolean isFocusedPackageWhiteList ( java.lang.String p0 ) {
/* .locals 1 */
/* .param p1, "packageName" # Ljava/lang/String; */
/* .line 1195 */
v0 = v0 = this.mFocusedPackageWhiteList;
} // .end method
private Boolean isInputPackageWhiteList ( java.lang.String p0 ) {
/* .locals 1 */
/* .param p1, "packageName" # Ljava/lang/String; */
/* .line 1191 */
v0 = v0 = this.mInputPackageWhiteList;
} // .end method
private Boolean isWindowVisibleWhiteList ( com.android.server.policy.WindowManagerPolicy$WindowState p0, android.view.WindowManager$LayoutParams p1 ) {
/* .locals 2 */
/* .param p1, "win" # Lcom/android/server/policy/WindowManagerPolicy$WindowState; */
/* .param p2, "attrs" # Landroid/view/WindowManager$LayoutParams; */
/* .line 1200 */
v0 = this.mWindowVisibleWhiteList;
(( android.util.ArrayMap ) v0 ).get ( v1 ); // invoke-virtual {v0, v1}, Landroid/util/ArrayMap;->get(Ljava/lang/Object;)Ljava/lang/Object;
/* check-cast v0, Ljava/util/List; */
/* .line 1201 */
/* .local v0, "windowVisibleList":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;" */
if ( v0 != null) { // if-eqz v0, :cond_0
/* .line 1202 */
(( android.view.WindowManager$LayoutParams ) p2 ).getTitle ( ); // invoke-virtual {p2}, Landroid/view/WindowManager$LayoutParams;->getTitle()Ljava/lang/CharSequence;
v1 = java.lang.String .valueOf ( v1 );
if ( v1 != null) { // if-eqz v1, :cond_0
int v1 = 1; // const/4 v1, 0x1
} // :cond_0
int v1 = 0; // const/4 v1, 0x0
/* .line 1201 */
} // :goto_0
} // .end method
private void notifyInteractionEnd ( com.miui.server.smartpower.SmartDisplayPolicyManager$ComingInteraction p0 ) {
/* .locals 7 */
/* .param p1, "comingInteraction" # Lcom/miui/server/smartpower/SmartDisplayPolicyManager$ComingInteraction; */
/* .line 1228 */
v0 = this.mLock;
/* monitor-enter v0 */
/* .line 1229 */
try { // :try_start_0
v1 = /* invoke-direct {p0}, Lcom/miui/server/smartpower/SmartDisplayPolicyManager$RefreshRatePolicyController;->isEnable()Z */
/* if-nez v1, :cond_0 */
/* .line 1230 */
/* monitor-exit v0 */
return;
/* .line 1233 */
} // :cond_0
com.miui.server.smartpower.SmartDisplayPolicyManager$ComingInteraction .-$$Nest$fgetmDuration ( p1 );
/* move-result-wide v1 */
/* .line 1234 */
/* .local v1, "delayDuration":J */
java.lang.System .currentTimeMillis ( );
/* move-result-wide v3 */
/* add-long/2addr v3, v1 */
/* .line 1235 */
/* .local v3, "comingInteractEndTime":J */
/* invoke-direct {p0, p1, v3, v4}, Lcom/miui/server/smartpower/SmartDisplayPolicyManager$RefreshRatePolicyController;->updateCurrentInteractionLocked(Lcom/miui/server/smartpower/SmartDisplayPolicyManager$ComingInteraction;J)V */
/* .line 1238 */
v5 = this.mHandler;
int v6 = 2; // const/4 v6, 0x2
(( android.os.Handler ) v5 ).obtainMessage ( v6 ); // invoke-virtual {v5, v6}, Landroid/os/Handler;->obtainMessage(I)Landroid/os/Message;
/* .line 1239 */
/* .local v5, "msg":Landroid/os/Message; */
v6 = this.mHandler;
(( android.os.Handler ) v6 ).sendMessageDelayed ( v5, v1, v2 ); // invoke-virtual {v6, v5, v1, v2}, Landroid/os/Handler;->sendMessageDelayed(Landroid/os/Message;J)Z
/* .line 1240 */
/* nop */
} // .end local v1 # "delayDuration":J
} // .end local v3 # "comingInteractEndTime":J
} // .end local v5 # "msg":Landroid/os/Message;
/* monitor-exit v0 */
/* .line 1241 */
return;
/* .line 1240 */
/* :catchall_0 */
/* move-exception v1 */
/* monitor-exit v0 */
/* :try_end_0 */
/* .catchall {:try_start_0 ..:try_end_0} :catchall_0 */
/* throw v1 */
} // .end method
private void notifyInteractionStart ( com.miui.server.smartpower.SmartDisplayPolicyManager$ComingInteraction p0 ) {
/* .locals 5 */
/* .param p1, "comingInteraction" # Lcom/miui/server/smartpower/SmartDisplayPolicyManager$ComingInteraction; */
/* .line 1206 */
v0 = this.mLock;
/* monitor-enter v0 */
/* .line 1207 */
try { // :try_start_0
v1 = /* invoke-direct {p0}, Lcom/miui/server/smartpower/SmartDisplayPolicyManager$RefreshRatePolicyController;->isEnable()Z */
if ( v1 != null) { // if-eqz v1, :cond_2
v1 = /* invoke-direct {p0, p1}, Lcom/miui/server/smartpower/SmartDisplayPolicyManager$RefreshRatePolicyController;->shouldInterceptAdjustRefreshRate(Lcom/miui/server/smartpower/SmartDisplayPolicyManager$ComingInteraction;)Z */
if ( v1 != null) { // if-eqz v1, :cond_0
/* .line 1211 */
} // :cond_0
java.lang.System .currentTimeMillis ( );
/* move-result-wide v1 */
com.miui.server.smartpower.SmartDisplayPolicyManager$ComingInteraction .-$$Nest$fgetmDuration ( p1 );
/* move-result-wide v3 */
/* add-long/2addr v1, v3 */
/* .line 1214 */
/* .local v1, "comingInteractEndTime":J */
v3 = /* invoke-direct {p0, p1, v1, v2}, Lcom/miui/server/smartpower/SmartDisplayPolicyManager$RefreshRatePolicyController;->shouldInterceptComingInteractLocked(Lcom/miui/server/smartpower/SmartDisplayPolicyManager$ComingInteraction;J)Z */
if ( v3 != null) { // if-eqz v3, :cond_1
/* .line 1215 */
/* monitor-exit v0 */
return;
/* .line 1218 */
} // :cond_1
/* invoke-direct {p0, p1, v1, v2}, Lcom/miui/server/smartpower/SmartDisplayPolicyManager$RefreshRatePolicyController;->updateCurrentInteractionLocked(Lcom/miui/server/smartpower/SmartDisplayPolicyManager$ComingInteraction;J)V */
/* .line 1219 */
/* invoke-direct {p0}, Lcom/miui/server/smartpower/SmartDisplayPolicyManager$RefreshRatePolicyController;->dropCurrentInteraction()V */
/* .line 1222 */
v3 = this.mHandler;
int v4 = 1; // const/4 v4, 0x1
(( android.os.Handler ) v3 ).obtainMessage ( v4, p1 ); // invoke-virtual {v3, v4, p1}, Landroid/os/Handler;->obtainMessage(ILjava/lang/Object;)Landroid/os/Message;
/* .line 1223 */
/* .local v3, "msg":Landroid/os/Message; */
v4 = this.mHandler;
(( android.os.Handler ) v4 ).sendMessage ( v3 ); // invoke-virtual {v4, v3}, Landroid/os/Handler;->sendMessage(Landroid/os/Message;)Z
/* .line 1224 */
/* nop */
} // .end local v1 # "comingInteractEndTime":J
} // .end local v3 # "msg":Landroid/os/Message;
/* monitor-exit v0 */
/* .line 1225 */
return;
/* .line 1208 */
} // :cond_2
} // :goto_0
/* monitor-exit v0 */
return;
/* .line 1224 */
/* :catchall_0 */
/* move-exception v1 */
/* monitor-exit v0 */
/* :try_end_0 */
/* .catchall {:try_start_0 ..:try_end_0} :catchall_0 */
/* throw v1 */
} // .end method
private void reset ( ) {
/* .locals 3 */
/* .line 1384 */
v0 = this.mLock;
/* monitor-enter v0 */
/* .line 1385 */
int v1 = 0; // const/4 v1, 0x0
try { // :try_start_0
/* iput v1, p0, Lcom/miui/server/smartpower/SmartDisplayPolicyManager$RefreshRatePolicyController;->mCurrentInteractRefreshRate:I */
/* .line 1386 */
/* iput v1, p0, Lcom/miui/server/smartpower/SmartDisplayPolicyManager$RefreshRatePolicyController;->mCurrentInteractType:I */
/* .line 1387 */
/* const-wide/16 v1, 0x0 */
/* iput-wide v1, p0, Lcom/miui/server/smartpower/SmartDisplayPolicyManager$RefreshRatePolicyController;->mCurrentInteractDuration:J */
/* .line 1388 */
/* iput-wide v1, p0, Lcom/miui/server/smartpower/SmartDisplayPolicyManager$RefreshRatePolicyController;->mCurrentInteractEndTime:J */
/* .line 1389 */
/* monitor-exit v0 */
/* .line 1390 */
return;
/* .line 1389 */
/* :catchall_0 */
/* move-exception v1 */
/* monitor-exit v0 */
/* :try_end_0 */
/* .catchall {:try_start_0 ..:try_end_0} :catchall_0 */
/* throw v1 */
} // .end method
private void resetControllerLocked ( ) {
/* .locals 3 */
/* .line 1165 */
v0 = this.mSupportRefreshRates;
/* .line 1166 */
v0 = this.mSupportRefreshRates;
/* const/16 v1, 0x3c */
java.lang.Integer .valueOf ( v1 );
/* .line 1168 */
/* iput v1, p0, Lcom/miui/server/smartpower/SmartDisplayPolicyManager$RefreshRatePolicyController;->mMaxRefreshRateSupport:I */
/* .line 1169 */
/* iput v1, p0, Lcom/miui/server/smartpower/SmartDisplayPolicyManager$RefreshRatePolicyController;->mMinRefreshRateSupport:I */
/* .line 1171 */
/* iput v1, p0, Lcom/miui/server/smartpower/SmartDisplayPolicyManager$RefreshRatePolicyController;->mMaxRefreshRateInPolicy:I */
/* .line 1172 */
int v0 = 0; // const/4 v0, 0x0
/* iput v0, p0, Lcom/miui/server/smartpower/SmartDisplayPolicyManager$RefreshRatePolicyController;->mMinRefreshRateInPolicy:I */
/* .line 1174 */
int v0 = 1; // const/4 v0, 0x1
/* iput-boolean v0, p0, Lcom/miui/server/smartpower/SmartDisplayPolicyManager$RefreshRatePolicyController;->mDefaultRefreshRateEnable:Z */
/* .line 1175 */
/* iput v1, p0, Lcom/miui/server/smartpower/SmartDisplayPolicyManager$RefreshRatePolicyController;->mCurrentUserRefreshRate:I */
/* .line 1176 */
return;
} // .end method
private Boolean shouldInterceptAdjustRefreshRate ( com.miui.server.smartpower.SmartDisplayPolicyManager$ComingInteraction p0 ) {
/* .locals 1 */
/* .param p1, "comingInteraction" # Lcom/miui/server/smartpower/SmartDisplayPolicyManager$ComingInteraction; */
/* .line 1263 */
v0 = /* invoke-direct {p0}, Lcom/miui/server/smartpower/SmartDisplayPolicyManager$RefreshRatePolicyController;->shouldInterceptForDisplayStatus()Z */
/* if-nez v0, :cond_1 */
/* .line 1264 */
v0 = /* invoke-direct {p0, p1}, Lcom/miui/server/smartpower/SmartDisplayPolicyManager$RefreshRatePolicyController;->shouldInterceptForSoftwareRefreshRate(Lcom/miui/server/smartpower/SmartDisplayPolicyManager$ComingInteraction;)Z */
/* if-nez v0, :cond_1 */
/* .line 1265 */
v0 = /* invoke-direct {p0, p1}, Lcom/miui/server/smartpower/SmartDisplayPolicyManager$RefreshRatePolicyController;->shouldInterceptForFrameInsertion(Lcom/miui/server/smartpower/SmartDisplayPolicyManager$ComingInteraction;)Z */
/* if-nez v0, :cond_1 */
/* .line 1266 */
v0 = /* invoke-direct {p0, p1}, Lcom/miui/server/smartpower/SmartDisplayPolicyManager$RefreshRatePolicyController;->shouldInterceptForSurfaceTextureVideo(Lcom/miui/server/smartpower/SmartDisplayPolicyManager$ComingInteraction;)Z */
if ( v0 != null) { // if-eqz v0, :cond_0
} // :cond_0
int v0 = 0; // const/4 v0, 0x0
} // :cond_1
} // :goto_0
int v0 = 1; // const/4 v0, 0x1
/* .line 1263 */
} // :goto_1
} // .end method
private Boolean shouldInterceptComingInteractLocked ( com.miui.server.smartpower.SmartDisplayPolicyManager$ComingInteraction p0, Long p1 ) {
/* .locals 5 */
/* .param p1, "comingInteraction" # Lcom/miui/server/smartpower/SmartDisplayPolicyManager$ComingInteraction; */
/* .param p2, "comingInteractEndTime" # J */
/* .line 1248 */
v0 = /* invoke-direct {p0}, Lcom/miui/server/smartpower/SmartDisplayPolicyManager$RefreshRatePolicyController;->isAniminting()Z */
/* if-nez v0, :cond_0 */
/* .line 1249 */
int v0 = 0; // const/4 v0, 0x0
/* .line 1252 */
} // :cond_0
/* iget-wide v0, p0, Lcom/miui/server/smartpower/SmartDisplayPolicyManager$RefreshRatePolicyController;->mCurrentInteractEndTime:J */
/* cmp-long v0, p2, v0 */
int v1 = 1; // const/4 v1, 0x1
/* if-gtz v0, :cond_1 */
/* .line 1253 */
/* .line 1256 */
} // :cond_1
/* invoke-direct {p0, p1, p2, p3}, Lcom/miui/server/smartpower/SmartDisplayPolicyManager$RefreshRatePolicyController;->updateCurrentInteractionLocked(Lcom/miui/server/smartpower/SmartDisplayPolicyManager$ComingInteraction;J)V */
/* .line 1257 */
v0 = this.mHandler;
int v2 = 2; // const/4 v2, 0x2
(( android.os.Handler ) v0 ).removeMessages ( v2 ); // invoke-virtual {v0, v2}, Landroid/os/Handler;->removeMessages(I)V
/* .line 1258 */
v0 = this.mHandler;
com.miui.server.smartpower.SmartDisplayPolicyManager$ComingInteraction .-$$Nest$fgetmDuration ( p1 );
/* move-result-wide v3 */
(( android.os.Handler ) v0 ).sendEmptyMessageDelayed ( v2, v3, v4 ); // invoke-virtual {v0, v2, v3, v4}, Landroid/os/Handler;->sendEmptyMessageDelayed(IJ)Z
/* .line 1259 */
} // .end method
private Boolean shouldInterceptForDisplayStatus ( ) {
/* .locals 4 */
/* .line 1270 */
v0 = this.this$0;
v0 = com.miui.server.smartpower.SmartDisplayPolicyManager .-$$Nest$fgetmExternalDisplayConnected ( v0 );
int v1 = 1; // const/4 v1, 0x1
final String v2 = "SmartPower.DisplayPolicy"; // const-string v2, "SmartPower.DisplayPolicy"
if ( v0 != null) { // if-eqz v0, :cond_0
/* .line 1271 */
/* const-string/jumbo v0, "should intercept for external display connecting" */
android.util.Slog .d ( v2,v0 );
/* .line 1272 */
/* .line 1275 */
} // :cond_0
v0 = this.this$0;
v0 = com.miui.server.smartpower.SmartDisplayPolicyManager .-$$Nest$fgetisTempWarning ( v0 );
if ( v0 != null) { // if-eqz v0, :cond_1
/* .line 1276 */
/* new-instance v0, Ljava/lang/StringBuilder; */
/* invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V */
/* const-string/jumbo v3, "should intercept for thermal temp warning: " */
(( java.lang.StringBuilder ) v0 ).append ( v3 ); // invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
v3 = this.this$0;
v3 = com.miui.server.smartpower.SmartDisplayPolicyManager .-$$Nest$fgetmCurrentTemp ( v3 );
(( java.lang.StringBuilder ) v0 ).append ( v3 ); // invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v0 ).toString ( ); // invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
android.util.Slog .d ( v2,v0 );
/* .line 1277 */
/* .line 1280 */
} // :cond_1
v0 = this.this$0;
v0 = com.miui.server.smartpower.SmartDisplayPolicyManager .-$$Nest$fgetmIsCameraInForeground ( v0 );
if ( v0 != null) { // if-eqz v0, :cond_2
/* .line 1281 */
final String v0 = "camera in foreground or activity starting"; // const-string v0, "camera in foreground or activity starting"
android.util.Slog .d ( v2,v0 );
/* .line 1282 */
/* .line 1285 */
} // :cond_2
int v0 = 0; // const/4 v0, 0x0
} // .end method
private Boolean shouldInterceptForFrameInsertion ( com.miui.server.smartpower.SmartDisplayPolicyManager$ComingInteraction p0 ) {
/* .locals 5 */
/* .param p1, "comingInteraction" # Lcom/miui/server/smartpower/SmartDisplayPolicyManager$ComingInteraction; */
/* .line 1316 */
v0 = this.this$0;
v0 = com.miui.server.smartpower.SmartDisplayPolicyManager .-$$Nest$fgetmFrameInsertingForGame ( v0 );
int v1 = 0; // const/4 v1, 0x0
int v2 = 1; // const/4 v2, 0x1
/* if-nez v0, :cond_1 */
v0 = this.this$0;
v0 = com.miui.server.smartpower.SmartDisplayPolicyManager .-$$Nest$fgetmFrameInsertingForVideo ( v0 );
if ( v0 != null) { // if-eqz v0, :cond_0
} // :cond_0
/* move v0, v1 */
} // :cond_1
} // :goto_0
/* move v0, v2 */
/* .line 1318 */
/* .local v0, "isFrameInserting":Z */
} // :goto_1
if ( v0 != null) { // if-eqz v0, :cond_2
v3 = com.miui.server.smartpower.SmartDisplayPolicyManager$ComingInteraction .-$$Nest$fgetmInteractType ( p1 );
int v4 = 5; // const/4 v4, 0x5
/* if-eq v3, v4, :cond_2 */
v3 = com.miui.server.smartpower.SmartDisplayPolicyManager$ComingInteraction .-$$Nest$fgetmInteractType ( p1 );
int v4 = 6; // const/4 v4, 0x6
/* if-eq v3, v4, :cond_2 */
/* move v1, v2 */
/* .line 1321 */
/* .local v1, "intercept":Z */
} // :cond_2
if ( v1 != null) { // if-eqz v1, :cond_3
/* .line 1322 */
/* new-instance v2, Ljava/lang/StringBuilder; */
/* invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V */
/* const-string/jumbo v3, "should intercept for frame insertion in " */
(( java.lang.StringBuilder ) v2 ).append ( v3 ); // invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
v3 = this.this$0;
com.miui.server.smartpower.SmartDisplayPolicyManager .-$$Nest$fgetmFrameInsertScene ( v3 );
(( java.lang.StringBuilder ) v2 ).append ( v3 ); // invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v2 ).toString ( ); // invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
final String v3 = "SmartPower.DisplayPolicy"; // const-string v3, "SmartPower.DisplayPolicy"
android.util.Slog .d ( v3,v2 );
/* .line 1324 */
} // :cond_3
} // .end method
private Boolean shouldInterceptForSoftwareRefreshRate ( com.miui.server.smartpower.SmartDisplayPolicyManager$ComingInteraction p0 ) {
/* .locals 3 */
/* .param p1, "comingInteraction" # Lcom/miui/server/smartpower/SmartDisplayPolicyManager$ComingInteraction; */
/* .line 1299 */
/* iget v0, p0, Lcom/miui/server/smartpower/SmartDisplayPolicyManager$RefreshRatePolicyController;->mMaxRefreshRateSupport:I */
/* const/16 v1, 0x78 */
int v2 = 0; // const/4 v2, 0x0
/* if-ne v0, v1, :cond_2 */
/* .line 1300 */
/* iget v0, p0, Lcom/miui/server/smartpower/SmartDisplayPolicyManager$RefreshRatePolicyController;->mMiuiRefreshRate:I */
/* const/16 v1, 0x5a */
/* if-ne v0, v1, :cond_0 */
v0 = com.miui.server.smartpower.SmartDisplayPolicyManager$ComingInteraction .-$$Nest$fgetmInteractType ( p1 );
int v1 = 5; // const/4 v1, 0x5
/* if-eq v0, v1, :cond_0 */
v0 = com.miui.server.smartpower.SmartDisplayPolicyManager$ComingInteraction .-$$Nest$fgetmInteractType ( p1 );
int v1 = 6; // const/4 v1, 0x6
/* if-eq v0, v1, :cond_0 */
int v2 = 1; // const/4 v2, 0x1
} // :cond_0
/* move v0, v2 */
/* .line 1303 */
/* .local v0, "intercept":Z */
if ( v0 != null) { // if-eqz v0, :cond_1
/* sget-boolean v1, Lcom/miui/server/smartpower/SmartDisplayPolicyManager;->DEBUG:Z */
if ( v1 != null) { // if-eqz v1, :cond_1
/* .line 1304 */
final String v1 = "SmartPower.DisplayPolicy"; // const-string v1, "SmartPower.DisplayPolicy"
/* const-string/jumbo v2, "should intercept for software refresh rate" */
android.util.Slog .d ( v1,v2 );
/* .line 1306 */
} // :cond_1
/* .line 1308 */
} // .end local v0 # "intercept":Z
} // :cond_2
} // .end method
private Boolean shouldInterceptForSurfaceTextureVideo ( com.miui.server.smartpower.SmartDisplayPolicyManager$ComingInteraction p0 ) {
/* .locals 4 */
/* .param p1, "comingInteraction" # Lcom/miui/server/smartpower/SmartDisplayPolicyManager$ComingInteraction; */
/* .line 1328 */
v0 = this.this$0;
com.miui.server.smartpower.SmartDisplayPolicyManager .-$$Nest$mgetForegroundPackageName ( v0 );
/* .line 1329 */
/* .local v0, "packageName":Ljava/lang/String; */
v1 = v1 = this.mSurfaceTextureBlackList;
if ( v1 != null) { // if-eqz v1, :cond_0
v1 = com.miui.server.smartpower.SmartDisplayPolicyManager$ComingInteraction .-$$Nest$fgetmInteractType ( p1 );
int v2 = 5; // const/4 v2, 0x5
/* if-eq v1, v2, :cond_0 */
v1 = com.miui.server.smartpower.SmartDisplayPolicyManager$ComingInteraction .-$$Nest$fgetmInteractType ( p1 );
int v2 = 6; // const/4 v2, 0x6
/* if-eq v1, v2, :cond_0 */
int v1 = 1; // const/4 v1, 0x1
} // :cond_0
int v1 = 0; // const/4 v1, 0x0
/* .line 1332 */
/* .local v1, "intercept":Z */
} // :goto_0
if ( v1 != null) { // if-eqz v1, :cond_1
/* .line 1333 */
final String v2 = "SmartPower.DisplayPolicy"; // const-string v2, "SmartPower.DisplayPolicy"
/* const-string/jumbo v3, "should intercept for surface texture video" */
android.util.Slog .d ( v2,v3 );
/* .line 1335 */
} // :cond_1
} // .end method
private Boolean shouldInterceptUpdateDisplayModeSpecs ( com.android.server.display.mode.DisplayModeDirector$DesiredDisplayModeSpecs p0 ) {
/* .locals 4 */
/* .param p1, "modeSpecs" # Lcom/android/server/display/mode/DisplayModeDirector$DesiredDisplayModeSpecs; */
/* .line 1343 */
v0 = /* invoke-direct {p0}, Lcom/miui/server/smartpower/SmartDisplayPolicyManager$RefreshRatePolicyController;->isAniminting()Z */
int v1 = 0; // const/4 v1, 0x0
/* if-nez v0, :cond_0 */
/* .line 1344 */
/* .line 1347 */
} // :cond_0
v0 = this.primary;
v0 = this.render;
/* iget v0, v0, Landroid/view/SurfaceControl$RefreshRateRange;->max:F */
v2 = this.appRequest;
v2 = this.render;
/* iget v2, v2, Landroid/view/SurfaceControl$RefreshRateRange;->max:F */
v0 = java.lang.Math .max ( v0,v2 );
/* .line 1349 */
/* .local v0, "maxRefreshRate":F */
/* iget v2, p0, Lcom/miui/server/smartpower/SmartDisplayPolicyManager$RefreshRatePolicyController;->mMaxRefreshRateInPolicy:I */
/* int-to-float v2, v2 */
/* cmpg-float v2, v0, v2 */
/* if-gez v2, :cond_1 */
int v1 = 1; // const/4 v1, 0x1
/* .line 1350 */
/* .local v1, "interceptOrNot":Z */
} // :cond_1
/* sget-boolean v2, Lcom/miui/server/smartpower/SmartDisplayPolicyManager;->DEBUG:Z */
if ( v2 != null) { // if-eqz v2, :cond_2
/* .line 1351 */
/* new-instance v2, Ljava/lang/StringBuilder; */
/* invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V */
final String v3 = "intercept update mode specs: "; // const-string v3, "intercept update mode specs: "
(( java.lang.StringBuilder ) v2 ).append ( v3 ); // invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v2 ).append ( v1 ); // invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v2 ).toString ( ); // invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
final String v3 = "SmartPower.DisplayPolicy"; // const-string v3, "SmartPower.DisplayPolicy"
android.util.Slog .d ( v3,v2 );
/* .line 1353 */
} // :cond_2
} // .end method
private void updateAppropriateRefreshRate ( ) {
/* .locals 4 */
/* .line 1406 */
v0 = this.mLock;
/* monitor-enter v0 */
/* .line 1407 */
try { // :try_start_0
v1 = (( com.miui.server.smartpower.SmartDisplayPolicyManager$RefreshRatePolicyController ) p0 ).isDefaultRefreshRateEnable ( ); // invoke-virtual {p0}, Lcom/miui/server/smartpower/SmartDisplayPolicyManager$RefreshRatePolicyController;->isDefaultRefreshRateEnable()Z
/* iput-boolean v1, p0, Lcom/miui/server/smartpower/SmartDisplayPolicyManager$RefreshRatePolicyController;->mDefaultRefreshRateEnable:Z */
/* .line 1408 */
v1 = (( com.miui.server.smartpower.SmartDisplayPolicyManager$RefreshRatePolicyController ) p0 ).getCurrentUserRefreshRate ( ); // invoke-virtual {p0}, Lcom/miui/server/smartpower/SmartDisplayPolicyManager$RefreshRatePolicyController;->getCurrentUserRefreshRate()I
/* iput v1, p0, Lcom/miui/server/smartpower/SmartDisplayPolicyManager$RefreshRatePolicyController;->mCurrentUserRefreshRate:I */
/* .line 1410 */
/* iget-boolean v2, p0, Lcom/miui/server/smartpower/SmartDisplayPolicyManager$RefreshRatePolicyController;->mDefaultRefreshRateEnable:Z */
if ( v2 != null) { // if-eqz v2, :cond_0
/* .line 1411 */
v1 = this.mSupportRefreshRates;
/* new-instance v2, Lcom/miui/server/smartpower/SmartDisplayPolicyManager$RefreshRatePolicyController$$ExternalSyntheticLambda0; */
/* invoke-direct {v2}, Lcom/miui/server/smartpower/SmartDisplayPolicyManager$RefreshRatePolicyController$$ExternalSyntheticLambda0;-><init>()V */
/* .line 1412 */
/* .line 1413 */
(( java.util.Optional ) v1 ).get ( ); // invoke-virtual {v1}, Ljava/util/Optional;->get()Ljava/lang/Object;
/* check-cast v1, Ljava/lang/Integer; */
v1 = (( java.lang.Integer ) v1 ).intValue ( ); // invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I
/* iput v1, p0, Lcom/miui/server/smartpower/SmartDisplayPolicyManager$RefreshRatePolicyController;->mMaxRefreshRateInPolicy:I */
/* .line 1415 */
/* iget v1, p0, Lcom/miui/server/smartpower/SmartDisplayPolicyManager$RefreshRatePolicyController;->LIMIT_MAX_REFRESH_RATE:I */
int v2 = -1; // const/4 v2, -0x1
/* if-eq v1, v2, :cond_1 */
v2 = this.mSupportRefreshRates;
/* .line 1416 */
v1 = java.lang.Integer .valueOf ( v1 );
if ( v1 != null) { // if-eqz v1, :cond_1
/* .line 1417 */
/* iget v1, p0, Lcom/miui/server/smartpower/SmartDisplayPolicyManager$RefreshRatePolicyController;->LIMIT_MAX_REFRESH_RATE:I */
/* iput v1, p0, Lcom/miui/server/smartpower/SmartDisplayPolicyManager$RefreshRatePolicyController;->mMaxRefreshRateInPolicy:I */
/* .line 1419 */
} // :cond_0
v2 = this.mSupportRefreshRates;
v1 = java.lang.Integer .valueOf ( v1 );
if ( v1 != null) { // if-eqz v1, :cond_1
/* .line 1420 */
/* iget v1, p0, Lcom/miui/server/smartpower/SmartDisplayPolicyManager$RefreshRatePolicyController;->mCurrentUserRefreshRate:I */
/* iput v1, p0, Lcom/miui/server/smartpower/SmartDisplayPolicyManager$RefreshRatePolicyController;->mMaxRefreshRateInPolicy:I */
/* .line 1423 */
} // :cond_1
} // :goto_0
/* sget-boolean v1, Lcom/miui/server/smartpower/SmartDisplayPolicyManager;->DEBUG:Z */
if ( v1 != null) { // if-eqz v1, :cond_2
/* .line 1424 */
final String v1 = "SmartPower.DisplayPolicy"; // const-string v1, "SmartPower.DisplayPolicy"
/* new-instance v2, Ljava/lang/StringBuilder; */
/* invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V */
final String v3 = "default enable: "; // const-string v3, "default enable: "
(( java.lang.StringBuilder ) v2 ).append ( v3 ); // invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
/* iget-boolean v3, p0, Lcom/miui/server/smartpower/SmartDisplayPolicyManager$RefreshRatePolicyController;->mDefaultRefreshRateEnable:Z */
(( java.lang.StringBuilder ) v2 ).append ( v3 ); // invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;
final String v3 = ", refresh rate: "; // const-string v3, ", refresh rate: "
(( java.lang.StringBuilder ) v2 ).append ( v3 ); // invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
/* iget v3, p0, Lcom/miui/server/smartpower/SmartDisplayPolicyManager$RefreshRatePolicyController;->mMaxRefreshRateInPolicy:I */
(( java.lang.StringBuilder ) v2 ).append ( v3 ); // invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v2 ).toString ( ); // invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
android.util.Slog .d ( v1,v2 );
/* .line 1427 */
} // :cond_2
/* monitor-exit v0 */
/* .line 1428 */
return;
/* .line 1427 */
/* :catchall_0 */
/* move-exception v1 */
/* monitor-exit v0 */
/* :try_end_0 */
/* .catchall {:try_start_0 ..:try_end_0} :catchall_0 */
/* throw v1 */
} // .end method
private void updateControllerLocked ( ) {
/* .locals 2 */
/* .line 1153 */
v0 = this.mLock;
/* monitor-enter v0 */
/* .line 1155 */
try { // :try_start_0
/* invoke-direct {p0}, Lcom/miui/server/smartpower/SmartDisplayPolicyManager$RefreshRatePolicyController;->resetControllerLocked()V */
/* .line 1158 */
/* invoke-direct {p0}, Lcom/miui/server/smartpower/SmartDisplayPolicyManager$RefreshRatePolicyController;->updateSupportRefreshRatesLocked()V */
/* .line 1160 */
/* invoke-direct {p0}, Lcom/miui/server/smartpower/SmartDisplayPolicyManager$RefreshRatePolicyController;->updateAppropriateRefreshRate()V */
/* .line 1161 */
/* monitor-exit v0 */
/* .line 1162 */
return;
/* .line 1161 */
/* :catchall_0 */
/* move-exception v1 */
/* monitor-exit v0 */
/* :try_end_0 */
/* .catchall {:try_start_0 ..:try_end_0} :catchall_0 */
/* throw v1 */
} // .end method
private void updateCurrentInteractionLocked ( com.miui.server.smartpower.SmartDisplayPolicyManager$ComingInteraction p0, Long p1 ) {
/* .locals 2 */
/* .param p1, "comingInteraction" # Lcom/miui/server/smartpower/SmartDisplayPolicyManager$ComingInteraction; */
/* .param p2, "comingInteractEndTime" # J */
/* .line 1358 */
v0 = com.miui.server.smartpower.SmartDisplayPolicyManager$ComingInteraction .-$$Nest$fgetmMaxRefreshRate ( p1 );
/* iput v0, p0, Lcom/miui/server/smartpower/SmartDisplayPolicyManager$RefreshRatePolicyController;->mCurrentInteractRefreshRate:I */
/* .line 1359 */
v0 = com.miui.server.smartpower.SmartDisplayPolicyManager$ComingInteraction .-$$Nest$fgetmInteractType ( p1 );
/* iput v0, p0, Lcom/miui/server/smartpower/SmartDisplayPolicyManager$RefreshRatePolicyController;->mCurrentInteractType:I */
/* .line 1360 */
com.miui.server.smartpower.SmartDisplayPolicyManager$ComingInteraction .-$$Nest$fgetmDuration ( p1 );
/* move-result-wide v0 */
/* iput-wide v0, p0, Lcom/miui/server/smartpower/SmartDisplayPolicyManager$RefreshRatePolicyController;->mCurrentInteractDuration:J */
/* .line 1361 */
/* iput-wide p2, p0, Lcom/miui/server/smartpower/SmartDisplayPolicyManager$RefreshRatePolicyController;->mCurrentInteractEndTime:J */
/* .line 1362 */
return;
} // .end method
private void updateSupportRefreshRatesLocked ( ) {
/* .locals 7 */
/* .line 1179 */
v0 = this.this$0;
com.miui.server.smartpower.SmartDisplayPolicyManager .-$$Nest$fgetmSupportedDisplayModes ( v0 );
if ( v0 != null) { // if-eqz v0, :cond_0
/* .line 1180 */
v0 = this.this$0;
com.miui.server.smartpower.SmartDisplayPolicyManager .-$$Nest$fgetmSupportedDisplayModes ( v0 );
/* array-length v1, v0 */
int v2 = 0; // const/4 v2, 0x0
} // :goto_0
/* if-ge v2, v1, :cond_0 */
/* aget-object v3, v0, v2 */
/* .line 1182 */
/* .local v3, "mode":Landroid/view/SurfaceControl$DisplayMode; */
/* iget v4, v3, Landroid/view/SurfaceControl$DisplayMode;->refreshRate:F */
/* const/high16 v5, 0x41200000 # 10.0f */
/* mul-float/2addr v4, v5 */
v4 = java.lang.Math .round ( v4 );
/* div-int/lit8 v4, v4, 0xa */
/* .line 1183 */
/* .local v4, "realRefreshRate":I */
/* int-to-float v5, v4 */
/* iput v5, v3, Landroid/view/SurfaceControl$DisplayMode;->refreshRate:F */
/* .line 1184 */
v5 = this.mSupportRefreshRates;
java.lang.Integer .valueOf ( v4 );
/* .line 1185 */
/* iget v5, p0, Lcom/miui/server/smartpower/SmartDisplayPolicyManager$RefreshRatePolicyController;->mMaxRefreshRateSupport:I */
v5 = java.lang.Math .max ( v5,v4 );
/* iput v5, p0, Lcom/miui/server/smartpower/SmartDisplayPolicyManager$RefreshRatePolicyController;->mMaxRefreshRateSupport:I */
/* .line 1180 */
} // .end local v3 # "mode":Landroid/view/SurfaceControl$DisplayMode;
} // .end local v4 # "realRefreshRate":I
/* add-int/lit8 v2, v2, 0x1 */
/* .line 1188 */
} // :cond_0
return;
} // .end method
/* # virtual methods */
public Integer getCurrentUserRefreshRate ( ) {
/* .locals 3 */
/* .line 1438 */
v0 = this.mContext;
/* .line 1439 */
(( android.content.Context ) v0 ).getContentResolver ( ); // invoke-virtual {v0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;
/* .line 1438 */
/* const-string/jumbo v1, "user_refresh_rate" */
int v2 = 0; // const/4 v2, 0x0
v0 = android.provider.Settings$Secure .getInt ( v0,v1,v2 );
} // .end method
public Boolean isDefaultRefreshRateEnable ( ) {
/* .locals 3 */
/* .line 1431 */
v0 = this.mContext;
/* .line 1432 */
(( android.content.Context ) v0 ).getContentResolver ( ); // invoke-virtual {v0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;
/* .line 1431 */
final String v1 = "is_smart_fps"; // const-string v1, "is_smart_fps"
int v2 = 0; // const/4 v2, 0x0
v0 = android.provider.Settings$System .getInt ( v0,v1,v2 );
int v1 = 1; // const/4 v1, 0x1
/* if-ne v0, v1, :cond_0 */
/* move v2, v1 */
} // :cond_0
} // .end method
