.class Lcom/miui/server/smartpower/SmartScenarioManager$ScenarioAppChild;
.super Ljava/lang/Object;
.source "SmartScenarioManager.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/miui/server/smartpower/SmartScenarioManager;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "ScenarioAppChild"
.end annotation


# instance fields
.field private mApp:Lcom/android/server/am/AppStateManager$AppState;

.field private mPackageName:Ljava/lang/String;

.field final synthetic this$0:Lcom/miui/server/smartpower/SmartScenarioManager;


# direct methods
.method static bridge synthetic -$$Nest$fgetmApp(Lcom/miui/server/smartpower/SmartScenarioManager$ScenarioAppChild;)Lcom/android/server/am/AppStateManager$AppState;
    .locals 0

    iget-object p0, p0, Lcom/miui/server/smartpower/SmartScenarioManager$ScenarioAppChild;->mApp:Lcom/android/server/am/AppStateManager$AppState;

    return-object p0
.end method

.method private constructor <init>(Lcom/miui/server/smartpower/SmartScenarioManager;Lcom/android/server/am/AppStateManager$AppState;)V
    .locals 0
    .param p2, "app"    # Lcom/android/server/am/AppStateManager$AppState;

    .line 314
    iput-object p1, p0, Lcom/miui/server/smartpower/SmartScenarioManager$ScenarioAppChild;->this$0:Lcom/miui/server/smartpower/SmartScenarioManager;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 312
    const/4 p1, 0x0

    iput-object p1, p0, Lcom/miui/server/smartpower/SmartScenarioManager$ScenarioAppChild;->mPackageName:Ljava/lang/String;

    .line 315
    iput-object p2, p0, Lcom/miui/server/smartpower/SmartScenarioManager$ScenarioAppChild;->mApp:Lcom/android/server/am/AppStateManager$AppState;

    .line 316
    if-eqz p2, :cond_0

    .line 317
    invoke-virtual {p2}, Lcom/android/server/am/AppStateManager$AppState;->getPackageName()Ljava/lang/String;

    move-result-object p1

    iput-object p1, p0, Lcom/miui/server/smartpower/SmartScenarioManager$ScenarioAppChild;->mPackageName:Ljava/lang/String;

    .line 319
    :cond_0
    return-void
.end method

.method synthetic constructor <init>(Lcom/miui/server/smartpower/SmartScenarioManager;Lcom/android/server/am/AppStateManager$AppState;Lcom/miui/server/smartpower/SmartScenarioManager$ScenarioAppChild-IA;)V
    .locals 0

    invoke-direct {p0, p1, p2}, Lcom/miui/server/smartpower/SmartScenarioManager$ScenarioAppChild;-><init>(Lcom/miui/server/smartpower/SmartScenarioManager;Lcom/android/server/am/AppStateManager$AppState;)V

    return-void
.end method


# virtual methods
.method public getPackageName()Ljava/lang/String;
    .locals 1

    .line 322
    iget-object v0, p0, Lcom/miui/server/smartpower/SmartScenarioManager$ScenarioAppChild;->mPackageName:Ljava/lang/String;

    return-object v0
.end method

.method public toString()Ljava/lang/String;
    .locals 1

    .line 327
    iget-object v0, p0, Lcom/miui/server/smartpower/SmartScenarioManager$ScenarioAppChild;->mPackageName:Ljava/lang/String;

    return-object v0
.end method
