.class Lcom/miui/server/smartpower/SmartDisplayPolicyManager$H;
.super Landroid/os/Handler;
.source "SmartDisplayPolicyManager.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/miui/server/smartpower/SmartDisplayPolicyManager;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "H"
.end annotation


# instance fields
.field final synthetic this$0:Lcom/miui/server/smartpower/SmartDisplayPolicyManager;


# direct methods
.method constructor <init>(Lcom/miui/server/smartpower/SmartDisplayPolicyManager;Landroid/os/Looper;)V
    .locals 0
    .param p2, "looper"    # Landroid/os/Looper;

    .line 987
    iput-object p1, p0, Lcom/miui/server/smartpower/SmartDisplayPolicyManager$H;->this$0:Lcom/miui/server/smartpower/SmartDisplayPolicyManager;

    .line 988
    invoke-direct {p0, p2}, Landroid/os/Handler;-><init>(Landroid/os/Looper;)V

    .line 989
    return-void
.end method


# virtual methods
.method public handleMessage(Landroid/os/Message;)V
    .locals 2
    .param p1, "msg"    # Landroid/os/Message;

    .line 993
    invoke-super {p0, p1}, Landroid/os/Handler;->handleMessage(Landroid/os/Message;)V

    .line 994
    iget v0, p1, Landroid/os/Message;->what:I

    packed-switch v0, :pswitch_data_0

    goto :goto_0

    .line 1003
    :pswitch_0
    iget-object v0, p0, Lcom/miui/server/smartpower/SmartDisplayPolicyManager$H;->this$0:Lcom/miui/server/smartpower/SmartDisplayPolicyManager;

    invoke-static {v0}, Lcom/miui/server/smartpower/SmartDisplayPolicyManager;->-$$Nest$mshouldUpdateDisplayDevice(Lcom/miui/server/smartpower/SmartDisplayPolicyManager;)V

    goto :goto_0

    .line 1000
    :pswitch_1
    iget-object v0, p0, Lcom/miui/server/smartpower/SmartDisplayPolicyManager$H;->this$0:Lcom/miui/server/smartpower/SmartDisplayPolicyManager;

    invoke-static {v0}, Lcom/miui/server/smartpower/SmartDisplayPolicyManager;->-$$Nest$mshouldDownRefreshRate(Lcom/miui/server/smartpower/SmartDisplayPolicyManager;)V

    .line 1001
    goto :goto_0

    .line 996
    :pswitch_2
    iget-object v0, p1, Landroid/os/Message;->obj:Ljava/lang/Object;

    check-cast v0, Lcom/miui/server/smartpower/SmartDisplayPolicyManager$ComingInteraction;

    .line 997
    .local v0, "obj":Lcom/miui/server/smartpower/SmartDisplayPolicyManager$ComingInteraction;
    iget-object v1, p0, Lcom/miui/server/smartpower/SmartDisplayPolicyManager$H;->this$0:Lcom/miui/server/smartpower/SmartDisplayPolicyManager;

    invoke-static {v1, v0}, Lcom/miui/server/smartpower/SmartDisplayPolicyManager;->-$$Nest$mshouldUpRefreshRate(Lcom/miui/server/smartpower/SmartDisplayPolicyManager;Lcom/miui/server/smartpower/SmartDisplayPolicyManager$ComingInteraction;)V

    .line 998
    nop

    .line 1007
    .end local v0    # "obj":Lcom/miui/server/smartpower/SmartDisplayPolicyManager$ComingInteraction;
    :goto_0
    return-void

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_2
        :pswitch_1
        :pswitch_0
    .end packed-switch
.end method
