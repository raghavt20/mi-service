.class Lcom/miui/server/smartpower/AppCameraResource$CameraRecord;
.super Ljava/lang/Object;
.source "AppCameraResource.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/miui/server/smartpower/AppCameraResource;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "CameraRecord"
.end annotation


# instance fields
.field private mCallerPackage:Ljava/lang/String;

.field private mCallerPid:I

.field private mCallerUid:I

.field final synthetic this$0:Lcom/miui/server/smartpower/AppCameraResource;


# direct methods
.method static bridge synthetic -$$Nest$fgetmCallerUid(Lcom/miui/server/smartpower/AppCameraResource$CameraRecord;)I
    .locals 0

    iget p0, p0, Lcom/miui/server/smartpower/AppCameraResource$CameraRecord;->mCallerUid:I

    return p0
.end method

.method public constructor <init>(Lcom/miui/server/smartpower/AppCameraResource;Ljava/lang/String;II)V
    .locals 0
    .param p2, "caller"    # Ljava/lang/String;
    .param p3, "callerUid"    # I
    .param p4, "callerPid"    # I

    .line 85
    iput-object p1, p0, Lcom/miui/server/smartpower/AppCameraResource$CameraRecord;->this$0:Lcom/miui/server/smartpower/AppCameraResource;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 86
    iput p3, p0, Lcom/miui/server/smartpower/AppCameraResource$CameraRecord;->mCallerUid:I

    .line 87
    iput p4, p0, Lcom/miui/server/smartpower/AppCameraResource$CameraRecord;->mCallerPid:I

    .line 88
    iput-object p2, p0, Lcom/miui/server/smartpower/AppCameraResource$CameraRecord;->mCallerPackage:Ljava/lang/String;

    .line 89
    return-void
.end method


# virtual methods
.method public notifyCameraForegroundState(Ljava/lang/String;Z)V
    .locals 4
    .param p1, "cameraId"    # Ljava/lang/String;
    .param p2, "isForeground"    # Z

    .line 92
    sget-boolean v0, Lcom/miui/server/smartpower/AppPowerResource;->DEBUG:Z

    if-eqz v0, :cond_0

    .line 93
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "camera: active="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p2}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, " cameraId="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, " pkg="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lcom/miui/server/smartpower/AppCameraResource$CameraRecord;->mCallerPackage:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, " uid="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget v1, p0, Lcom/miui/server/smartpower/AppCameraResource$CameraRecord;->mCallerUid:I

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, " pid="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget v1, p0, Lcom/miui/server/smartpower/AppCameraResource$CameraRecord;->mCallerPid:I

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    const-string v1, "SmartPower.AppResource"

    invoke-static {v1, v0}, Landroid/util/Slog;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 99
    :cond_0
    iget-object v0, p0, Lcom/miui/server/smartpower/AppCameraResource$CameraRecord;->this$0:Lcom/miui/server/smartpower/AppCameraResource;

    iget v1, p0, Lcom/miui/server/smartpower/AppCameraResource$CameraRecord;->mCallerUid:I

    iget v2, p0, Lcom/miui/server/smartpower/AppCameraResource$CameraRecord;->mCallerPid:I

    const/16 v3, 0x20

    invoke-virtual {v0, v1, v2, p2, v3}, Lcom/miui/server/smartpower/AppCameraResource;->reportResourceStatus(IIZI)V

    .line 101
    iget-object v0, p0, Lcom/miui/server/smartpower/AppCameraResource$CameraRecord;->this$0:Lcom/miui/server/smartpower/AppCameraResource;

    iget v1, p0, Lcom/miui/server/smartpower/AppCameraResource$CameraRecord;->mCallerUid:I

    invoke-virtual {v0, v1, p2, v3}, Lcom/miui/server/smartpower/AppCameraResource;->reportResourceStatus(IZI)V

    .line 103
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "camera u:"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget v1, p0, Lcom/miui/server/smartpower/AppCameraResource$CameraRecord;->mCallerUid:I

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, " p:"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget v1, p0, Lcom/miui/server/smartpower/AppCameraResource$CameraRecord;->mCallerPid:I

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, " s:"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p2}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    const v1, 0x15ff2

    invoke-static {v1, v0}, Landroid/util/EventLog;->writeEvent(ILjava/lang/String;)I

    .line 105
    return-void
.end method
