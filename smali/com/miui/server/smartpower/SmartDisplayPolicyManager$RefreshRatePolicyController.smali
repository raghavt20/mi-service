.class Lcom/miui/server/smartpower/SmartDisplayPolicyManager$RefreshRatePolicyController;
.super Ljava/lang/Object;
.source "SmartDisplayPolicyManager.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/miui/server/smartpower/SmartDisplayPolicyManager;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "RefreshRatePolicyController"
.end annotation

.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/miui/server/smartpower/SmartDisplayPolicyManager$RefreshRatePolicyController$SettingRefreshRateObserver;
    }
.end annotation


# static fields
.field private static final DEFAULT_MIN_REFRESH_RATE:I = 0x3c

.field private static final DELAY_DOWN_FOCUS_WINDOW_CHANGED_TIME:J = 0x1f4L

.field private static final DELAY_DOWN_MULTI_TASK_TIME:J = 0x1f4L

.field private static final DELAY_DOWN_RECENT_ANIMATION_TIME:J = 0x1f4L

.field private static final DELAY_DOWN_REMOTE_ANIMATION_TIME:J = 0x1f4L

.field private static final DELAY_DOWN_TIME:J = 0x1f4L

.field private static final INTERACT_APP_TRANSITION_DUR:J = 0x14L

.field private static final INTERACT_DEFAULT_DUR:J = 0x3e8L

.field private static final INTERACT_INSET_ANIMATION_DUR:J = 0x12cL

.field private static final INTERACT_MULTI_TASK_DUR:J = 0x7530L

.field private static final INTERACT_RECENT_ANIMATION_DUR:J = 0x7530L

.field private static final INTERACT_REMOTE_ANIMATION_DUR:J = 0x7530L

.field private static final INTERACT_WINDOW_ANIMATION_DUR:J = 0x14L

.field private static final INVALID_REFRESH_RATE:I = -0x1

.field private static final MIUI_DEFAULT_REFRESH_RATE:Ljava/lang/String; = "is_smart_fps"

.field private static final MIUI_REFRESH_RATE:Ljava/lang/String; = "miui_refresh_rate"

.field private static final MIUI_THERMAL_LIMIT_REFRESH_RATE:Ljava/lang/String; = "thermal_limit_refresh_rate"

.field private static final MIUI_USER_REFRESH_RATE:Ljava/lang/String; = "user_refresh_rate"

.field private static final NORMAL_MAX_REFRESH_RATE:I = 0x78

.field private static final SOFTWARE_REFRESH_RATE:I = 0x5a


# instance fields
.field private final LIMIT_MAX_REFRESH_RATE:I

.field private isThermalWarning:Z

.field private final mContext:Landroid/content/Context;

.field private mCurrentInteractDuration:J

.field private mCurrentInteractEndTime:J

.field private mCurrentInteractRefreshRate:I

.field private mCurrentInteractType:I

.field private mCurrentUserRefreshRate:I

.field private mDefaultRefreshRateEnable:Z

.field private final mFocusedPackageWhiteList:Ljava/util/Set;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Set<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private final mHandler:Landroid/os/Handler;

.field private final mInputPackageWhiteList:Ljava/util/Set;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Set<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private final mInteractDelayDownTimes:Landroid/util/SparseArray;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/util/SparseArray<",
            "Ljava/lang/Long;",
            ">;"
        }
    .end annotation
.end field

.field private final mInteractDurations:Landroid/util/SparseArray;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/util/SparseArray<",
            "Ljava/lang/Long;",
            ">;"
        }
    .end annotation
.end field

.field private final mLock:Ljava/lang/Object;

.field private mMaxRefreshRateInPolicy:I

.field private mMaxRefreshRateSupport:I

.field private mMinRefreshRateInPolicy:I

.field private mMinRefreshRateSupport:I

.field private mMiuiRefreshRate:I

.field private mSettingRefreshRateObserver:Lcom/miui/server/smartpower/SmartDisplayPolicyManager$RefreshRatePolicyController$SettingRefreshRateObserver;

.field private final mSupportRefreshRates:Ljava/util/Set;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Set<",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation
.end field

.field private final mSurfaceTextureBlackList:Ljava/util/Set;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Set<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private final mWindowVisibleWhiteList:Landroid/util/ArrayMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/util/ArrayMap<",
            "Ljava/lang/String;",
            "Ljava/util/List<",
            "Ljava/lang/String;",
            ">;>;"
        }
    .end annotation
.end field

.field final synthetic this$0:Lcom/miui/server/smartpower/SmartDisplayPolicyManager;


# direct methods
.method static bridge synthetic -$$Nest$fgetisThermalWarning(Lcom/miui/server/smartpower/SmartDisplayPolicyManager$RefreshRatePolicyController;)Z
    .locals 0

    iget-boolean p0, p0, Lcom/miui/server/smartpower/SmartDisplayPolicyManager$RefreshRatePolicyController;->isThermalWarning:Z

    return p0
.end method

.method static bridge synthetic -$$Nest$fputisThermalWarning(Lcom/miui/server/smartpower/SmartDisplayPolicyManager$RefreshRatePolicyController;Z)V
    .locals 0

    iput-boolean p1, p0, Lcom/miui/server/smartpower/SmartDisplayPolicyManager$RefreshRatePolicyController;->isThermalWarning:Z

    return-void
.end method

.method static bridge synthetic -$$Nest$fputmMiuiRefreshRate(Lcom/miui/server/smartpower/SmartDisplayPolicyManager$RefreshRatePolicyController;I)V
    .locals 0

    iput p1, p0, Lcom/miui/server/smartpower/SmartDisplayPolicyManager$RefreshRatePolicyController;->mMiuiRefreshRate:I

    return-void
.end method

.method static bridge synthetic -$$Nest$mdump(Lcom/miui/server/smartpower/SmartDisplayPolicyManager$RefreshRatePolicyController;Ljava/io/PrintWriter;[Ljava/lang/String;I)V
    .locals 0

    invoke-direct {p0, p1, p2, p3}, Lcom/miui/server/smartpower/SmartDisplayPolicyManager$RefreshRatePolicyController;->dump(Ljava/io/PrintWriter;[Ljava/lang/String;I)V

    return-void
.end method

.method static bridge synthetic -$$Nest$mgetDelayDownTimeForInteraction(Lcom/miui/server/smartpower/SmartDisplayPolicyManager$RefreshRatePolicyController;I)J
    .locals 0

    invoke-direct {p0, p1}, Lcom/miui/server/smartpower/SmartDisplayPolicyManager$RefreshRatePolicyController;->getDelayDownTimeForInteraction(I)J

    move-result-wide p0

    return-wide p0
.end method

.method static bridge synthetic -$$Nest$mgetDurationForInteraction(Lcom/miui/server/smartpower/SmartDisplayPolicyManager$RefreshRatePolicyController;I)J
    .locals 0

    invoke-direct {p0, p1}, Lcom/miui/server/smartpower/SmartDisplayPolicyManager$RefreshRatePolicyController;->getDurationForInteraction(I)J

    move-result-wide p0

    return-wide p0
.end method

.method static bridge synthetic -$$Nest$mgetMaxRefreshRateInPolicy(Lcom/miui/server/smartpower/SmartDisplayPolicyManager$RefreshRatePolicyController;)I
    .locals 0

    invoke-direct {p0}, Lcom/miui/server/smartpower/SmartDisplayPolicyManager$RefreshRatePolicyController;->getMaxRefreshRateInPolicy()I

    move-result p0

    return p0
.end method

.method static bridge synthetic -$$Nest$mgetMinRefreshRateInPolicy(Lcom/miui/server/smartpower/SmartDisplayPolicyManager$RefreshRatePolicyController;)I
    .locals 0

    invoke-direct {p0}, Lcom/miui/server/smartpower/SmartDisplayPolicyManager$RefreshRatePolicyController;->getMinRefreshRateInPolicy()I

    move-result p0

    return p0
.end method

.method static bridge synthetic -$$Nest$misFocusedPackageWhiteList(Lcom/miui/server/smartpower/SmartDisplayPolicyManager$RefreshRatePolicyController;Ljava/lang/String;)Z
    .locals 0

    invoke-direct {p0, p1}, Lcom/miui/server/smartpower/SmartDisplayPolicyManager$RefreshRatePolicyController;->isFocusedPackageWhiteList(Ljava/lang/String;)Z

    move-result p0

    return p0
.end method

.method static bridge synthetic -$$Nest$misInputPackageWhiteList(Lcom/miui/server/smartpower/SmartDisplayPolicyManager$RefreshRatePolicyController;Ljava/lang/String;)Z
    .locals 0

    invoke-direct {p0, p1}, Lcom/miui/server/smartpower/SmartDisplayPolicyManager$RefreshRatePolicyController;->isInputPackageWhiteList(Ljava/lang/String;)Z

    move-result p0

    return p0
.end method

.method static bridge synthetic -$$Nest$misWindowVisibleWhiteList(Lcom/miui/server/smartpower/SmartDisplayPolicyManager$RefreshRatePolicyController;Lcom/android/server/policy/WindowManagerPolicy$WindowState;Landroid/view/WindowManager$LayoutParams;)Z
    .locals 0

    invoke-direct {p0, p1, p2}, Lcom/miui/server/smartpower/SmartDisplayPolicyManager$RefreshRatePolicyController;->isWindowVisibleWhiteList(Lcom/android/server/policy/WindowManagerPolicy$WindowState;Landroid/view/WindowManager$LayoutParams;)Z

    move-result p0

    return p0
.end method

.method static bridge synthetic -$$Nest$mnotifyInteractionEnd(Lcom/miui/server/smartpower/SmartDisplayPolicyManager$RefreshRatePolicyController;Lcom/miui/server/smartpower/SmartDisplayPolicyManager$ComingInteraction;)V
    .locals 0

    invoke-direct {p0, p1}, Lcom/miui/server/smartpower/SmartDisplayPolicyManager$RefreshRatePolicyController;->notifyInteractionEnd(Lcom/miui/server/smartpower/SmartDisplayPolicyManager$ComingInteraction;)V

    return-void
.end method

.method static bridge synthetic -$$Nest$mnotifyInteractionStart(Lcom/miui/server/smartpower/SmartDisplayPolicyManager$RefreshRatePolicyController;Lcom/miui/server/smartpower/SmartDisplayPolicyManager$ComingInteraction;)V
    .locals 0

    invoke-direct {p0, p1}, Lcom/miui/server/smartpower/SmartDisplayPolicyManager$RefreshRatePolicyController;->notifyInteractionStart(Lcom/miui/server/smartpower/SmartDisplayPolicyManager$ComingInteraction;)V

    return-void
.end method

.method static bridge synthetic -$$Nest$mreset(Lcom/miui/server/smartpower/SmartDisplayPolicyManager$RefreshRatePolicyController;)V
    .locals 0

    invoke-direct {p0}, Lcom/miui/server/smartpower/SmartDisplayPolicyManager$RefreshRatePolicyController;->reset()V

    return-void
.end method

.method static bridge synthetic -$$Nest$mshouldInterceptUpdateDisplayModeSpecs(Lcom/miui/server/smartpower/SmartDisplayPolicyManager$RefreshRatePolicyController;Lcom/android/server/display/mode/DisplayModeDirector$DesiredDisplayModeSpecs;)Z
    .locals 0

    invoke-direct {p0, p1}, Lcom/miui/server/smartpower/SmartDisplayPolicyManager$RefreshRatePolicyController;->shouldInterceptUpdateDisplayModeSpecs(Lcom/android/server/display/mode/DisplayModeDirector$DesiredDisplayModeSpecs;)Z

    move-result p0

    return p0
.end method

.method static bridge synthetic -$$Nest$mupdateAppropriateRefreshRate(Lcom/miui/server/smartpower/SmartDisplayPolicyManager$RefreshRatePolicyController;)V
    .locals 0

    invoke-direct {p0}, Lcom/miui/server/smartpower/SmartDisplayPolicyManager$RefreshRatePolicyController;->updateAppropriateRefreshRate()V

    return-void
.end method

.method static bridge synthetic -$$Nest$mupdateControllerLocked(Lcom/miui/server/smartpower/SmartDisplayPolicyManager$RefreshRatePolicyController;)V
    .locals 0

    invoke-direct {p0}, Lcom/miui/server/smartpower/SmartDisplayPolicyManager$RefreshRatePolicyController;->updateControllerLocked()V

    return-void
.end method

.method private constructor <init>(Lcom/miui/server/smartpower/SmartDisplayPolicyManager;Landroid/content/Context;Landroid/os/Handler;)V
    .locals 10
    .param p2, "context"    # Landroid/content/Context;
    .param p3, "handler"    # Landroid/os/Handler;

    .line 1111
    iput-object p1, p0, Lcom/miui/server/smartpower/SmartDisplayPolicyManager$RefreshRatePolicyController;->this$0:Lcom/miui/server/smartpower/SmartDisplayPolicyManager;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1011
    new-instance p1, Ljava/lang/Object;

    invoke-direct {p1}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lcom/miui/server/smartpower/SmartDisplayPolicyManager$RefreshRatePolicyController;->mLock:Ljava/lang/Object;

    .line 1060
    sget p1, Lcom/miui/app/smartpower/SmartPowerSettings;->PROP_LIMIT_MAX_REFRESH_RATE:I

    iput p1, p0, Lcom/miui/server/smartpower/SmartDisplayPolicyManager$RefreshRatePolicyController;->LIMIT_MAX_REFRESH_RATE:I

    .line 1065
    new-instance p1, Ljava/util/HashSet;

    invoke-direct {p1}, Ljava/util/HashSet;-><init>()V

    iput-object p1, p0, Lcom/miui/server/smartpower/SmartDisplayPolicyManager$RefreshRatePolicyController;->mFocusedPackageWhiteList:Ljava/util/Set;

    .line 1069
    new-instance v0, Ljava/util/HashSet;

    invoke-direct {v0}, Ljava/util/HashSet;-><init>()V

    iput-object v0, p0, Lcom/miui/server/smartpower/SmartDisplayPolicyManager$RefreshRatePolicyController;->mInputPackageWhiteList:Ljava/util/Set;

    .line 1073
    new-instance v1, Ljava/util/HashSet;

    invoke-direct {v1}, Ljava/util/HashSet;-><init>()V

    iput-object v1, p0, Lcom/miui/server/smartpower/SmartDisplayPolicyManager$RefreshRatePolicyController;->mSurfaceTextureBlackList:Ljava/util/Set;

    .line 1078
    new-instance v2, Landroid/util/ArrayMap;

    invoke-direct {v2}, Landroid/util/ArrayMap;-><init>()V

    iput-object v2, p0, Lcom/miui/server/smartpower/SmartDisplayPolicyManager$RefreshRatePolicyController;->mWindowVisibleWhiteList:Landroid/util/ArrayMap;

    .line 1080
    new-instance v3, Ljava/util/HashSet;

    invoke-direct {v3}, Ljava/util/HashSet;-><init>()V

    iput-object v3, p0, Lcom/miui/server/smartpower/SmartDisplayPolicyManager$RefreshRatePolicyController;->mSupportRefreshRates:Ljava/util/Set;

    .line 1082
    new-instance v4, Landroid/util/SparseArray;

    const/16 v5, 0x8

    invoke-direct {v4, v5}, Landroid/util/SparseArray;-><init>(I)V

    iput-object v4, p0, Lcom/miui/server/smartpower/SmartDisplayPolicyManager$RefreshRatePolicyController;->mInteractDurations:Landroid/util/SparseArray;

    .line 1083
    new-instance v5, Landroid/util/SparseArray;

    const/4 v6, 0x4

    invoke-direct {v5, v6}, Landroid/util/SparseArray;-><init>(I)V

    iput-object v5, p0, Lcom/miui/server/smartpower/SmartDisplayPolicyManager$RefreshRatePolicyController;->mInteractDelayDownTimes:Landroid/util/SparseArray;

    .line 1085
    const/16 v7, 0x3c

    iput v7, p0, Lcom/miui/server/smartpower/SmartDisplayPolicyManager$RefreshRatePolicyController;->mMaxRefreshRateSupport:I

    .line 1086
    iput v7, p0, Lcom/miui/server/smartpower/SmartDisplayPolicyManager$RefreshRatePolicyController;->mMinRefreshRateSupport:I

    .line 1091
    iput v7, p0, Lcom/miui/server/smartpower/SmartDisplayPolicyManager$RefreshRatePolicyController;->mMaxRefreshRateInPolicy:I

    .line 1092
    const/4 v8, 0x0

    iput v8, p0, Lcom/miui/server/smartpower/SmartDisplayPolicyManager$RefreshRatePolicyController;->mMinRefreshRateInPolicy:I

    .line 1094
    const/4 v9, 0x1

    iput-boolean v9, p0, Lcom/miui/server/smartpower/SmartDisplayPolicyManager$RefreshRatePolicyController;->mDefaultRefreshRateEnable:Z

    .line 1096
    iput v7, p0, Lcom/miui/server/smartpower/SmartDisplayPolicyManager$RefreshRatePolicyController;->mCurrentUserRefreshRate:I

    .line 1101
    const/4 v9, 0x0

    iput-object v9, p0, Lcom/miui/server/smartpower/SmartDisplayPolicyManager$RefreshRatePolicyController;->mSettingRefreshRateObserver:Lcom/miui/server/smartpower/SmartDisplayPolicyManager$RefreshRatePolicyController$SettingRefreshRateObserver;

    .line 1108
    iput v7, p0, Lcom/miui/server/smartpower/SmartDisplayPolicyManager$RefreshRatePolicyController;->mMiuiRefreshRate:I

    .line 1109
    iput-boolean v8, p0, Lcom/miui/server/smartpower/SmartDisplayPolicyManager$RefreshRatePolicyController;->isThermalWarning:Z

    .line 1112
    iput-object p2, p0, Lcom/miui/server/smartpower/SmartDisplayPolicyManager$RefreshRatePolicyController;->mContext:Landroid/content/Context;

    .line 1113
    iput-object p3, p0, Lcom/miui/server/smartpower/SmartDisplayPolicyManager$RefreshRatePolicyController;->mHandler:Landroid/os/Handler;

    .line 1115
    invoke-static {v7}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v7

    invoke-interface {v3, v7}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    .line 1117
    new-instance v3, Lcom/miui/server/smartpower/SmartDisplayPolicyManager$RefreshRatePolicyController$SettingRefreshRateObserver;

    invoke-direct {v3, p0, p2, p3}, Lcom/miui/server/smartpower/SmartDisplayPolicyManager$RefreshRatePolicyController$SettingRefreshRateObserver;-><init>(Lcom/miui/server/smartpower/SmartDisplayPolicyManager$RefreshRatePolicyController;Landroid/content/Context;Landroid/os/Handler;)V

    iput-object v3, p0, Lcom/miui/server/smartpower/SmartDisplayPolicyManager$RefreshRatePolicyController;->mSettingRefreshRateObserver:Lcom/miui/server/smartpower/SmartDisplayPolicyManager$RefreshRatePolicyController$SettingRefreshRateObserver;

    .line 1119
    const-string v3, "com.android.systemui"

    invoke-interface {p1, v3}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    .line 1121
    invoke-interface {v0, v3}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    .line 1122
    const-string p1, "com.miui.home"

    invoke-interface {v0, p1}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    .line 1123
    const-string p1, "com.mi.android.globallauncher"

    invoke-interface {v0, p1}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    .line 1125
    const-string p1, "com.miui.mediaviewer"

    invoke-interface {v1, p1}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    .line 1128
    const-string p1, "ScreenshotAnimation"

    invoke-static {p1}, Ljava/util/List;->of(Ljava/lang/Object;)Ljava/util/List;

    move-result-object p1

    const-string v0, "com.miui.screenshot"

    invoke-virtual {v2, v0, p1}, Landroid/util/ArrayMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 1130
    const-wide/16 v0, 0x14

    invoke-static {v0, v1}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object p1

    const/4 v0, 0x2

    invoke-virtual {v4, v0, p1}, Landroid/util/SparseArray;->put(ILjava/lang/Object;)V

    .line 1131
    const/4 v0, 0x3

    invoke-virtual {v4, v0, p1}, Landroid/util/SparseArray;->put(ILjava/lang/Object;)V

    .line 1132
    const-wide/16 v0, 0x12c

    invoke-static {v0, v1}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object p1

    invoke-virtual {v4, v6, p1}, Landroid/util/SparseArray;->put(ILjava/lang/Object;)V

    .line 1133
    const-wide/16 v0, 0x7530

    invoke-static {v0, v1}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object p1

    const/4 v0, 0x5

    invoke-virtual {v4, v0, p1}, Landroid/util/SparseArray;->put(ILjava/lang/Object;)V

    .line 1134
    const/4 v1, 0x6

    invoke-virtual {v4, v1, p1}, Landroid/util/SparseArray;->put(ILjava/lang/Object;)V

    .line 1135
    const/16 v2, 0xc

    invoke-virtual {v4, v2, p1}, Landroid/util/SparseArray;->put(ILjava/lang/Object;)V

    .line 1136
    const/16 v3, 0xd

    invoke-virtual {v4, v3, p1}, Landroid/util/SparseArray;->put(ILjava/lang/Object;)V

    .line 1137
    const/16 v6, 0xe

    invoke-virtual {v4, v6, p1}, Landroid/util/SparseArray;->put(ILjava/lang/Object;)V

    .line 1138
    const/16 v7, 0xf

    invoke-virtual {v4, v7, p1}, Landroid/util/SparseArray;->put(ILjava/lang/Object;)V

    .line 1140
    nop

    .line 1141
    const-wide/16 v8, 0x1f4

    invoke-static {v8, v9}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object p1

    .line 1140
    invoke-virtual {v5, v0, p1}, Landroid/util/SparseArray;->put(ILjava/lang/Object;)V

    .line 1142
    nop

    .line 1143
    nop

    .line 1142
    invoke-virtual {v5, v1, p1}, Landroid/util/SparseArray;->put(ILjava/lang/Object;)V

    .line 1144
    nop

    .line 1145
    nop

    .line 1144
    const/4 v0, 0x7

    invoke-virtual {v5, v0, p1}, Landroid/util/SparseArray;->put(ILjava/lang/Object;)V

    .line 1146
    invoke-virtual {v5, v2, p1}, Landroid/util/SparseArray;->put(ILjava/lang/Object;)V

    .line 1147
    invoke-virtual {v5, v3, p1}, Landroid/util/SparseArray;->put(ILjava/lang/Object;)V

    .line 1148
    invoke-virtual {v5, v6, p1}, Landroid/util/SparseArray;->put(ILjava/lang/Object;)V

    .line 1149
    invoke-virtual {v5, v7, p1}, Landroid/util/SparseArray;->put(ILjava/lang/Object;)V

    .line 1150
    return-void
.end method

.method synthetic constructor <init>(Lcom/miui/server/smartpower/SmartDisplayPolicyManager;Landroid/content/Context;Landroid/os/Handler;Lcom/miui/server/smartpower/SmartDisplayPolicyManager$RefreshRatePolicyController-IA;)V
    .locals 0

    invoke-direct {p0, p1, p2, p3}, Lcom/miui/server/smartpower/SmartDisplayPolicyManager$RefreshRatePolicyController;-><init>(Lcom/miui/server/smartpower/SmartDisplayPolicyManager;Landroid/content/Context;Landroid/os/Handler;)V

    return-void
.end method

.method private dropCurrentInteraction()V
    .locals 2

    .line 1379
    iget-object v0, p0, Lcom/miui/server/smartpower/SmartDisplayPolicyManager$RefreshRatePolicyController;->mHandler:Landroid/os/Handler;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Landroid/os/Handler;->removeMessages(I)V

    .line 1380
    iget-object v0, p0, Lcom/miui/server/smartpower/SmartDisplayPolicyManager$RefreshRatePolicyController;->mHandler:Landroid/os/Handler;

    const/4 v1, 0x2

    invoke-virtual {v0, v1}, Landroid/os/Handler;->removeMessages(I)V

    .line 1381
    return-void
.end method

.method private dump(Ljava/io/PrintWriter;[Ljava/lang/String;I)V
    .locals 3
    .param p1, "pw"    # Ljava/io/PrintWriter;
    .param p2, "args"    # [Ljava/lang/String;
    .param p3, "opti"    # I

    .line 1443
    const-string v0, "RefreshRatePolicyController"

    invoke-virtual {p1, v0}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V

    .line 1444
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v1, "status: "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-direct {p0}, Lcom/miui/server/smartpower/SmartDisplayPolicyManager$RefreshRatePolicyController;->isEnable()Z

    move-result v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v0}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V

    .line 1445
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "refresh rate support: "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lcom/miui/server/smartpower/SmartDisplayPolicyManager$RefreshRatePolicyController;->mSupportRefreshRates:Ljava/util/Set;

    invoke-virtual {v1}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v0}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V

    .line 1446
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "default refresh rate: "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-boolean v1, p0, Lcom/miui/server/smartpower/SmartDisplayPolicyManager$RefreshRatePolicyController;->mDefaultRefreshRateEnable:Z

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v0}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V

    .line 1447
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v1, "user refresh rate: "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget v1, p0, Lcom/miui/server/smartpower/SmartDisplayPolicyManager$RefreshRatePolicyController;->mCurrentUserRefreshRate:I

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v0}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V

    .line 1448
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "max refresh rate in policy: "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget v1, p0, Lcom/miui/server/smartpower/SmartDisplayPolicyManager$RefreshRatePolicyController;->mMaxRefreshRateInPolicy:I

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v0}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V

    .line 1449
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "min refresh rate in policy: "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget v1, p0, Lcom/miui/server/smartpower/SmartDisplayPolicyManager$RefreshRatePolicyController;->mMinRefreshRateInPolicy:I

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v0}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V

    .line 1450
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "current type: "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget v1, p0, Lcom/miui/server/smartpower/SmartDisplayPolicyManager$RefreshRatePolicyController;->mCurrentInteractType:I

    invoke-static {v1}, Lcom/miui/server/smartpower/SmartDisplayPolicyManager;->-$$Nest$sminteractionTypeToString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v0}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V

    .line 1451
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "current interact refresh rate: "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget v1, p0, Lcom/miui/server/smartpower/SmartDisplayPolicyManager$RefreshRatePolicyController;->mCurrentInteractRefreshRate:I

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v0}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V

    .line 1452
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "current interact duration: "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-wide v1, p0, Lcom/miui/server/smartpower/SmartDisplayPolicyManager$RefreshRatePolicyController;->mCurrentInteractDuration:J

    invoke-virtual {v0, v1, v2}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v0}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V

    .line 1453
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "mCurrentInteractEndTime: "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-wide v1, p0, Lcom/miui/server/smartpower/SmartDisplayPolicyManager$RefreshRatePolicyController;->mCurrentInteractEndTime:J

    invoke-virtual {v0, v1, v2}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v0}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V

    .line 1454
    return-void
.end method

.method private getDelayDownTimeForInteraction(I)J
    .locals 3
    .param p1, "interactType"    # I

    .line 1374
    iget-object v0, p0, Lcom/miui/server/smartpower/SmartDisplayPolicyManager$RefreshRatePolicyController;->mInteractDelayDownTimes:Landroid/util/SparseArray;

    invoke-virtual {v0, p1}, Landroid/util/SparseArray;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Long;

    .line 1375
    .local v0, "interactDelayDownTime":Ljava/lang/Long;
    if-eqz v0, :cond_0

    invoke-virtual {v0}, Ljava/lang/Long;->longValue()J

    move-result-wide v1

    goto :goto_0

    :cond_0
    const-wide/16 v1, 0x0

    :goto_0
    return-wide v1
.end method

.method private getDurationForInteraction(I)J
    .locals 3
    .param p1, "interactType"    # I

    .line 1369
    iget-object v0, p0, Lcom/miui/server/smartpower/SmartDisplayPolicyManager$RefreshRatePolicyController;->mInteractDurations:Landroid/util/SparseArray;

    invoke-virtual {v0, p1}, Landroid/util/SparseArray;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Long;

    .line 1370
    .local v0, "interactDuration":Ljava/lang/Long;
    if-eqz v0, :cond_0

    invoke-virtual {v0}, Ljava/lang/Long;->longValue()J

    move-result-wide v1

    goto :goto_0

    :cond_0
    const-wide/16 v1, 0x3e8

    :goto_0
    return-wide v1
.end method

.method private getMaxRefreshRateInPolicy()I
    .locals 1

    .line 1398
    iget v0, p0, Lcom/miui/server/smartpower/SmartDisplayPolicyManager$RefreshRatePolicyController;->mMaxRefreshRateInPolicy:I

    return v0
.end method

.method private getMinRefreshRateInPolicy()I
    .locals 1

    .line 1402
    iget v0, p0, Lcom/miui/server/smartpower/SmartDisplayPolicyManager$RefreshRatePolicyController;->mMinRefreshRateInPolicy:I

    return v0
.end method

.method private isAniminting()Z
    .locals 4

    .line 1365
    iget-wide v0, p0, Lcom/miui/server/smartpower/SmartDisplayPolicyManager$RefreshRatePolicyController;->mCurrentInteractEndTime:J

    const-wide/16 v2, 0x0

    cmp-long v0, v0, v2

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    :goto_0
    return v0
.end method

.method private isEnable()Z
    .locals 2

    .line 1393
    invoke-static {}, Lcom/miui/server/smartpower/SmartDisplayPolicyManager;->-$$Nest$sfgetENABLE()Z

    move-result v0

    if-eqz v0, :cond_0

    iget v0, p0, Lcom/miui/server/smartpower/SmartDisplayPolicyManager$RefreshRatePolicyController;->mMaxRefreshRateInPolicy:I

    iget v1, p0, Lcom/miui/server/smartpower/SmartDisplayPolicyManager$RefreshRatePolicyController;->mMinRefreshRateSupport:I

    if-eq v0, v1, :cond_0

    const/4 v0, 0x1

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    :goto_0
    return v0
.end method

.method private isFocusedPackageWhiteList(Ljava/lang/String;)Z
    .locals 1
    .param p1, "packageName"    # Ljava/lang/String;

    .line 1195
    iget-object v0, p0, Lcom/miui/server/smartpower/SmartDisplayPolicyManager$RefreshRatePolicyController;->mFocusedPackageWhiteList:Ljava/util/Set;

    invoke-interface {v0, p1}, Ljava/util/Set;->contains(Ljava/lang/Object;)Z

    move-result v0

    return v0
.end method

.method private isInputPackageWhiteList(Ljava/lang/String;)Z
    .locals 1
    .param p1, "packageName"    # Ljava/lang/String;

    .line 1191
    iget-object v0, p0, Lcom/miui/server/smartpower/SmartDisplayPolicyManager$RefreshRatePolicyController;->mInputPackageWhiteList:Ljava/util/Set;

    invoke-interface {v0, p1}, Ljava/util/Set;->contains(Ljava/lang/Object;)Z

    move-result v0

    return v0
.end method

.method private isWindowVisibleWhiteList(Lcom/android/server/policy/WindowManagerPolicy$WindowState;Landroid/view/WindowManager$LayoutParams;)Z
    .locals 2
    .param p1, "win"    # Lcom/android/server/policy/WindowManagerPolicy$WindowState;
    .param p2, "attrs"    # Landroid/view/WindowManager$LayoutParams;

    .line 1200
    iget-object v0, p0, Lcom/miui/server/smartpower/SmartDisplayPolicyManager$RefreshRatePolicyController;->mWindowVisibleWhiteList:Landroid/util/ArrayMap;

    invoke-interface {p1}, Lcom/android/server/policy/WindowManagerPolicy$WindowState;->getOwningPackage()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/util/ArrayMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/List;

    .line 1201
    .local v0, "windowVisibleList":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;"
    if-eqz v0, :cond_0

    .line 1202
    invoke-virtual {p2}, Landroid/view/WindowManager$LayoutParams;->getTitle()Ljava/lang/CharSequence;

    move-result-object v1

    invoke-static {v1}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/List;->contains(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    const/4 v1, 0x1

    goto :goto_0

    :cond_0
    const/4 v1, 0x0

    .line 1201
    :goto_0
    return v1
.end method

.method private notifyInteractionEnd(Lcom/miui/server/smartpower/SmartDisplayPolicyManager$ComingInteraction;)V
    .locals 7
    .param p1, "comingInteraction"    # Lcom/miui/server/smartpower/SmartDisplayPolicyManager$ComingInteraction;

    .line 1228
    iget-object v0, p0, Lcom/miui/server/smartpower/SmartDisplayPolicyManager$RefreshRatePolicyController;->mLock:Ljava/lang/Object;

    monitor-enter v0

    .line 1229
    :try_start_0
    invoke-direct {p0}, Lcom/miui/server/smartpower/SmartDisplayPolicyManager$RefreshRatePolicyController;->isEnable()Z

    move-result v1

    if-nez v1, :cond_0

    .line 1230
    monitor-exit v0

    return-void

    .line 1233
    :cond_0
    invoke-static {p1}, Lcom/miui/server/smartpower/SmartDisplayPolicyManager$ComingInteraction;->-$$Nest$fgetmDuration(Lcom/miui/server/smartpower/SmartDisplayPolicyManager$ComingInteraction;)J

    move-result-wide v1

    .line 1234
    .local v1, "delayDuration":J
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v3

    add-long/2addr v3, v1

    .line 1235
    .local v3, "comingInteractEndTime":J
    invoke-direct {p0, p1, v3, v4}, Lcom/miui/server/smartpower/SmartDisplayPolicyManager$RefreshRatePolicyController;->updateCurrentInteractionLocked(Lcom/miui/server/smartpower/SmartDisplayPolicyManager$ComingInteraction;J)V

    .line 1238
    iget-object v5, p0, Lcom/miui/server/smartpower/SmartDisplayPolicyManager$RefreshRatePolicyController;->mHandler:Landroid/os/Handler;

    const/4 v6, 0x2

    invoke-virtual {v5, v6}, Landroid/os/Handler;->obtainMessage(I)Landroid/os/Message;

    move-result-object v5

    .line 1239
    .local v5, "msg":Landroid/os/Message;
    iget-object v6, p0, Lcom/miui/server/smartpower/SmartDisplayPolicyManager$RefreshRatePolicyController;->mHandler:Landroid/os/Handler;

    invoke-virtual {v6, v5, v1, v2}, Landroid/os/Handler;->sendMessageDelayed(Landroid/os/Message;J)Z

    .line 1240
    nop

    .end local v1    # "delayDuration":J
    .end local v3    # "comingInteractEndTime":J
    .end local v5    # "msg":Landroid/os/Message;
    monitor-exit v0

    .line 1241
    return-void

    .line 1240
    :catchall_0
    move-exception v1

    monitor-exit v0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v1
.end method

.method private notifyInteractionStart(Lcom/miui/server/smartpower/SmartDisplayPolicyManager$ComingInteraction;)V
    .locals 5
    .param p1, "comingInteraction"    # Lcom/miui/server/smartpower/SmartDisplayPolicyManager$ComingInteraction;

    .line 1206
    iget-object v0, p0, Lcom/miui/server/smartpower/SmartDisplayPolicyManager$RefreshRatePolicyController;->mLock:Ljava/lang/Object;

    monitor-enter v0

    .line 1207
    :try_start_0
    invoke-direct {p0}, Lcom/miui/server/smartpower/SmartDisplayPolicyManager$RefreshRatePolicyController;->isEnable()Z

    move-result v1

    if-eqz v1, :cond_2

    invoke-direct {p0, p1}, Lcom/miui/server/smartpower/SmartDisplayPolicyManager$RefreshRatePolicyController;->shouldInterceptAdjustRefreshRate(Lcom/miui/server/smartpower/SmartDisplayPolicyManager$ComingInteraction;)Z

    move-result v1

    if-eqz v1, :cond_0

    goto :goto_0

    .line 1211
    :cond_0
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v1

    invoke-static {p1}, Lcom/miui/server/smartpower/SmartDisplayPolicyManager$ComingInteraction;->-$$Nest$fgetmDuration(Lcom/miui/server/smartpower/SmartDisplayPolicyManager$ComingInteraction;)J

    move-result-wide v3

    add-long/2addr v1, v3

    .line 1214
    .local v1, "comingInteractEndTime":J
    invoke-direct {p0, p1, v1, v2}, Lcom/miui/server/smartpower/SmartDisplayPolicyManager$RefreshRatePolicyController;->shouldInterceptComingInteractLocked(Lcom/miui/server/smartpower/SmartDisplayPolicyManager$ComingInteraction;J)Z

    move-result v3

    if-eqz v3, :cond_1

    .line 1215
    monitor-exit v0

    return-void

    .line 1218
    :cond_1
    invoke-direct {p0, p1, v1, v2}, Lcom/miui/server/smartpower/SmartDisplayPolicyManager$RefreshRatePolicyController;->updateCurrentInteractionLocked(Lcom/miui/server/smartpower/SmartDisplayPolicyManager$ComingInteraction;J)V

    .line 1219
    invoke-direct {p0}, Lcom/miui/server/smartpower/SmartDisplayPolicyManager$RefreshRatePolicyController;->dropCurrentInteraction()V

    .line 1222
    iget-object v3, p0, Lcom/miui/server/smartpower/SmartDisplayPolicyManager$RefreshRatePolicyController;->mHandler:Landroid/os/Handler;

    const/4 v4, 0x1

    invoke-virtual {v3, v4, p1}, Landroid/os/Handler;->obtainMessage(ILjava/lang/Object;)Landroid/os/Message;

    move-result-object v3

    .line 1223
    .local v3, "msg":Landroid/os/Message;
    iget-object v4, p0, Lcom/miui/server/smartpower/SmartDisplayPolicyManager$RefreshRatePolicyController;->mHandler:Landroid/os/Handler;

    invoke-virtual {v4, v3}, Landroid/os/Handler;->sendMessage(Landroid/os/Message;)Z

    .line 1224
    nop

    .end local v1    # "comingInteractEndTime":J
    .end local v3    # "msg":Landroid/os/Message;
    monitor-exit v0

    .line 1225
    return-void

    .line 1208
    :cond_2
    :goto_0
    monitor-exit v0

    return-void

    .line 1224
    :catchall_0
    move-exception v1

    monitor-exit v0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v1
.end method

.method private reset()V
    .locals 3

    .line 1384
    iget-object v0, p0, Lcom/miui/server/smartpower/SmartDisplayPolicyManager$RefreshRatePolicyController;->mLock:Ljava/lang/Object;

    monitor-enter v0

    .line 1385
    const/4 v1, 0x0

    :try_start_0
    iput v1, p0, Lcom/miui/server/smartpower/SmartDisplayPolicyManager$RefreshRatePolicyController;->mCurrentInteractRefreshRate:I

    .line 1386
    iput v1, p0, Lcom/miui/server/smartpower/SmartDisplayPolicyManager$RefreshRatePolicyController;->mCurrentInteractType:I

    .line 1387
    const-wide/16 v1, 0x0

    iput-wide v1, p0, Lcom/miui/server/smartpower/SmartDisplayPolicyManager$RefreshRatePolicyController;->mCurrentInteractDuration:J

    .line 1388
    iput-wide v1, p0, Lcom/miui/server/smartpower/SmartDisplayPolicyManager$RefreshRatePolicyController;->mCurrentInteractEndTime:J

    .line 1389
    monitor-exit v0

    .line 1390
    return-void

    .line 1389
    :catchall_0
    move-exception v1

    monitor-exit v0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v1
.end method

.method private resetControllerLocked()V
    .locals 3

    .line 1165
    iget-object v0, p0, Lcom/miui/server/smartpower/SmartDisplayPolicyManager$RefreshRatePolicyController;->mSupportRefreshRates:Ljava/util/Set;

    invoke-interface {v0}, Ljava/util/Set;->clear()V

    .line 1166
    iget-object v0, p0, Lcom/miui/server/smartpower/SmartDisplayPolicyManager$RefreshRatePolicyController;->mSupportRefreshRates:Ljava/util/Set;

    const/16 v1, 0x3c

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-interface {v0, v2}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    .line 1168
    iput v1, p0, Lcom/miui/server/smartpower/SmartDisplayPolicyManager$RefreshRatePolicyController;->mMaxRefreshRateSupport:I

    .line 1169
    iput v1, p0, Lcom/miui/server/smartpower/SmartDisplayPolicyManager$RefreshRatePolicyController;->mMinRefreshRateSupport:I

    .line 1171
    iput v1, p0, Lcom/miui/server/smartpower/SmartDisplayPolicyManager$RefreshRatePolicyController;->mMaxRefreshRateInPolicy:I

    .line 1172
    const/4 v0, 0x0

    iput v0, p0, Lcom/miui/server/smartpower/SmartDisplayPolicyManager$RefreshRatePolicyController;->mMinRefreshRateInPolicy:I

    .line 1174
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/miui/server/smartpower/SmartDisplayPolicyManager$RefreshRatePolicyController;->mDefaultRefreshRateEnable:Z

    .line 1175
    iput v1, p0, Lcom/miui/server/smartpower/SmartDisplayPolicyManager$RefreshRatePolicyController;->mCurrentUserRefreshRate:I

    .line 1176
    return-void
.end method

.method private shouldInterceptAdjustRefreshRate(Lcom/miui/server/smartpower/SmartDisplayPolicyManager$ComingInteraction;)Z
    .locals 1
    .param p1, "comingInteraction"    # Lcom/miui/server/smartpower/SmartDisplayPolicyManager$ComingInteraction;

    .line 1263
    invoke-direct {p0}, Lcom/miui/server/smartpower/SmartDisplayPolicyManager$RefreshRatePolicyController;->shouldInterceptForDisplayStatus()Z

    move-result v0

    if-nez v0, :cond_1

    .line 1264
    invoke-direct {p0, p1}, Lcom/miui/server/smartpower/SmartDisplayPolicyManager$RefreshRatePolicyController;->shouldInterceptForSoftwareRefreshRate(Lcom/miui/server/smartpower/SmartDisplayPolicyManager$ComingInteraction;)Z

    move-result v0

    if-nez v0, :cond_1

    .line 1265
    invoke-direct {p0, p1}, Lcom/miui/server/smartpower/SmartDisplayPolicyManager$RefreshRatePolicyController;->shouldInterceptForFrameInsertion(Lcom/miui/server/smartpower/SmartDisplayPolicyManager$ComingInteraction;)Z

    move-result v0

    if-nez v0, :cond_1

    .line 1266
    invoke-direct {p0, p1}, Lcom/miui/server/smartpower/SmartDisplayPolicyManager$RefreshRatePolicyController;->shouldInterceptForSurfaceTextureVideo(Lcom/miui/server/smartpower/SmartDisplayPolicyManager$ComingInteraction;)Z

    move-result v0

    if-eqz v0, :cond_0

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    goto :goto_1

    :cond_1
    :goto_0
    const/4 v0, 0x1

    .line 1263
    :goto_1
    return v0
.end method

.method private shouldInterceptComingInteractLocked(Lcom/miui/server/smartpower/SmartDisplayPolicyManager$ComingInteraction;J)Z
    .locals 5
    .param p1, "comingInteraction"    # Lcom/miui/server/smartpower/SmartDisplayPolicyManager$ComingInteraction;
    .param p2, "comingInteractEndTime"    # J

    .line 1248
    invoke-direct {p0}, Lcom/miui/server/smartpower/SmartDisplayPolicyManager$RefreshRatePolicyController;->isAniminting()Z

    move-result v0

    if-nez v0, :cond_0

    .line 1249
    const/4 v0, 0x0

    return v0

    .line 1252
    :cond_0
    iget-wide v0, p0, Lcom/miui/server/smartpower/SmartDisplayPolicyManager$RefreshRatePolicyController;->mCurrentInteractEndTime:J

    cmp-long v0, p2, v0

    const/4 v1, 0x1

    if-gtz v0, :cond_1

    .line 1253
    return v1

    .line 1256
    :cond_1
    invoke-direct {p0, p1, p2, p3}, Lcom/miui/server/smartpower/SmartDisplayPolicyManager$RefreshRatePolicyController;->updateCurrentInteractionLocked(Lcom/miui/server/smartpower/SmartDisplayPolicyManager$ComingInteraction;J)V

    .line 1257
    iget-object v0, p0, Lcom/miui/server/smartpower/SmartDisplayPolicyManager$RefreshRatePolicyController;->mHandler:Landroid/os/Handler;

    const/4 v2, 0x2

    invoke-virtual {v0, v2}, Landroid/os/Handler;->removeMessages(I)V

    .line 1258
    iget-object v0, p0, Lcom/miui/server/smartpower/SmartDisplayPolicyManager$RefreshRatePolicyController;->mHandler:Landroid/os/Handler;

    invoke-static {p1}, Lcom/miui/server/smartpower/SmartDisplayPolicyManager$ComingInteraction;->-$$Nest$fgetmDuration(Lcom/miui/server/smartpower/SmartDisplayPolicyManager$ComingInteraction;)J

    move-result-wide v3

    invoke-virtual {v0, v2, v3, v4}, Landroid/os/Handler;->sendEmptyMessageDelayed(IJ)Z

    .line 1259
    return v1
.end method

.method private shouldInterceptForDisplayStatus()Z
    .locals 4

    .line 1270
    iget-object v0, p0, Lcom/miui/server/smartpower/SmartDisplayPolicyManager$RefreshRatePolicyController;->this$0:Lcom/miui/server/smartpower/SmartDisplayPolicyManager;

    invoke-static {v0}, Lcom/miui/server/smartpower/SmartDisplayPolicyManager;->-$$Nest$fgetmExternalDisplayConnected(Lcom/miui/server/smartpower/SmartDisplayPolicyManager;)Z

    move-result v0

    const/4 v1, 0x1

    const-string v2, "SmartPower.DisplayPolicy"

    if-eqz v0, :cond_0

    .line 1271
    const-string/jumbo v0, "should intercept for external display connecting"

    invoke-static {v2, v0}, Landroid/util/Slog;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 1272
    return v1

    .line 1275
    :cond_0
    iget-object v0, p0, Lcom/miui/server/smartpower/SmartDisplayPolicyManager$RefreshRatePolicyController;->this$0:Lcom/miui/server/smartpower/SmartDisplayPolicyManager;

    invoke-static {v0}, Lcom/miui/server/smartpower/SmartDisplayPolicyManager;->-$$Nest$fgetisTempWarning(Lcom/miui/server/smartpower/SmartDisplayPolicyManager;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 1276
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v3, "should intercept for thermal temp warning: "

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v3, p0, Lcom/miui/server/smartpower/SmartDisplayPolicyManager$RefreshRatePolicyController;->this$0:Lcom/miui/server/smartpower/SmartDisplayPolicyManager;

    invoke-static {v3}, Lcom/miui/server/smartpower/SmartDisplayPolicyManager;->-$$Nest$fgetmCurrentTemp(Lcom/miui/server/smartpower/SmartDisplayPolicyManager;)I

    move-result v3

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v2, v0}, Landroid/util/Slog;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 1277
    return v1

    .line 1280
    :cond_1
    iget-object v0, p0, Lcom/miui/server/smartpower/SmartDisplayPolicyManager$RefreshRatePolicyController;->this$0:Lcom/miui/server/smartpower/SmartDisplayPolicyManager;

    invoke-static {v0}, Lcom/miui/server/smartpower/SmartDisplayPolicyManager;->-$$Nest$fgetmIsCameraInForeground(Lcom/miui/server/smartpower/SmartDisplayPolicyManager;)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 1281
    const-string v0, "camera in foreground or activity starting"

    invoke-static {v2, v0}, Landroid/util/Slog;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 1282
    return v1

    .line 1285
    :cond_2
    const/4 v0, 0x0

    return v0
.end method

.method private shouldInterceptForFrameInsertion(Lcom/miui/server/smartpower/SmartDisplayPolicyManager$ComingInteraction;)Z
    .locals 5
    .param p1, "comingInteraction"    # Lcom/miui/server/smartpower/SmartDisplayPolicyManager$ComingInteraction;

    .line 1316
    iget-object v0, p0, Lcom/miui/server/smartpower/SmartDisplayPolicyManager$RefreshRatePolicyController;->this$0:Lcom/miui/server/smartpower/SmartDisplayPolicyManager;

    invoke-static {v0}, Lcom/miui/server/smartpower/SmartDisplayPolicyManager;->-$$Nest$fgetmFrameInsertingForGame(Lcom/miui/server/smartpower/SmartDisplayPolicyManager;)Z

    move-result v0

    const/4 v1, 0x0

    const/4 v2, 0x1

    if-nez v0, :cond_1

    iget-object v0, p0, Lcom/miui/server/smartpower/SmartDisplayPolicyManager$RefreshRatePolicyController;->this$0:Lcom/miui/server/smartpower/SmartDisplayPolicyManager;

    invoke-static {v0}, Lcom/miui/server/smartpower/SmartDisplayPolicyManager;->-$$Nest$fgetmFrameInsertingForVideo(Lcom/miui/server/smartpower/SmartDisplayPolicyManager;)Z

    move-result v0

    if-eqz v0, :cond_0

    goto :goto_0

    :cond_0
    move v0, v1

    goto :goto_1

    :cond_1
    :goto_0
    move v0, v2

    .line 1318
    .local v0, "isFrameInserting":Z
    :goto_1
    if-eqz v0, :cond_2

    invoke-static {p1}, Lcom/miui/server/smartpower/SmartDisplayPolicyManager$ComingInteraction;->-$$Nest$fgetmInteractType(Lcom/miui/server/smartpower/SmartDisplayPolicyManager$ComingInteraction;)I

    move-result v3

    const/4 v4, 0x5

    if-eq v3, v4, :cond_2

    invoke-static {p1}, Lcom/miui/server/smartpower/SmartDisplayPolicyManager$ComingInteraction;->-$$Nest$fgetmInteractType(Lcom/miui/server/smartpower/SmartDisplayPolicyManager$ComingInteraction;)I

    move-result v3

    const/4 v4, 0x6

    if-eq v3, v4, :cond_2

    move v1, v2

    .line 1321
    .local v1, "intercept":Z
    :cond_2
    if-eqz v1, :cond_3

    .line 1322
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v3, "should intercept for frame insertion in "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget-object v3, p0, Lcom/miui/server/smartpower/SmartDisplayPolicyManager$RefreshRatePolicyController;->this$0:Lcom/miui/server/smartpower/SmartDisplayPolicyManager;

    invoke-static {v3}, Lcom/miui/server/smartpower/SmartDisplayPolicyManager;->-$$Nest$fgetmFrameInsertScene(Lcom/miui/server/smartpower/SmartDisplayPolicyManager;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    const-string v3, "SmartPower.DisplayPolicy"

    invoke-static {v3, v2}, Landroid/util/Slog;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 1324
    :cond_3
    return v1
.end method

.method private shouldInterceptForSoftwareRefreshRate(Lcom/miui/server/smartpower/SmartDisplayPolicyManager$ComingInteraction;)Z
    .locals 3
    .param p1, "comingInteraction"    # Lcom/miui/server/smartpower/SmartDisplayPolicyManager$ComingInteraction;

    .line 1299
    iget v0, p0, Lcom/miui/server/smartpower/SmartDisplayPolicyManager$RefreshRatePolicyController;->mMaxRefreshRateSupport:I

    const/16 v1, 0x78

    const/4 v2, 0x0

    if-ne v0, v1, :cond_2

    .line 1300
    iget v0, p0, Lcom/miui/server/smartpower/SmartDisplayPolicyManager$RefreshRatePolicyController;->mMiuiRefreshRate:I

    const/16 v1, 0x5a

    if-ne v0, v1, :cond_0

    invoke-static {p1}, Lcom/miui/server/smartpower/SmartDisplayPolicyManager$ComingInteraction;->-$$Nest$fgetmInteractType(Lcom/miui/server/smartpower/SmartDisplayPolicyManager$ComingInteraction;)I

    move-result v0

    const/4 v1, 0x5

    if-eq v0, v1, :cond_0

    invoke-static {p1}, Lcom/miui/server/smartpower/SmartDisplayPolicyManager$ComingInteraction;->-$$Nest$fgetmInteractType(Lcom/miui/server/smartpower/SmartDisplayPolicyManager$ComingInteraction;)I

    move-result v0

    const/4 v1, 0x6

    if-eq v0, v1, :cond_0

    const/4 v2, 0x1

    :cond_0
    move v0, v2

    .line 1303
    .local v0, "intercept":Z
    if-eqz v0, :cond_1

    sget-boolean v1, Lcom/miui/server/smartpower/SmartDisplayPolicyManager;->DEBUG:Z

    if-eqz v1, :cond_1

    .line 1304
    const-string v1, "SmartPower.DisplayPolicy"

    const-string/jumbo v2, "should intercept for software refresh rate"

    invoke-static {v1, v2}, Landroid/util/Slog;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 1306
    :cond_1
    return v0

    .line 1308
    .end local v0    # "intercept":Z
    :cond_2
    return v2
.end method

.method private shouldInterceptForSurfaceTextureVideo(Lcom/miui/server/smartpower/SmartDisplayPolicyManager$ComingInteraction;)Z
    .locals 4
    .param p1, "comingInteraction"    # Lcom/miui/server/smartpower/SmartDisplayPolicyManager$ComingInteraction;

    .line 1328
    iget-object v0, p0, Lcom/miui/server/smartpower/SmartDisplayPolicyManager$RefreshRatePolicyController;->this$0:Lcom/miui/server/smartpower/SmartDisplayPolicyManager;

    invoke-static {v0}, Lcom/miui/server/smartpower/SmartDisplayPolicyManager;->-$$Nest$mgetForegroundPackageName(Lcom/miui/server/smartpower/SmartDisplayPolicyManager;)Ljava/lang/String;

    move-result-object v0

    .line 1329
    .local v0, "packageName":Ljava/lang/String;
    iget-object v1, p0, Lcom/miui/server/smartpower/SmartDisplayPolicyManager$RefreshRatePolicyController;->mSurfaceTextureBlackList:Ljava/util/Set;

    invoke-interface {v1, v0}, Ljava/util/Set;->contains(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    invoke-static {p1}, Lcom/miui/server/smartpower/SmartDisplayPolicyManager$ComingInteraction;->-$$Nest$fgetmInteractType(Lcom/miui/server/smartpower/SmartDisplayPolicyManager$ComingInteraction;)I

    move-result v1

    const/4 v2, 0x5

    if-eq v1, v2, :cond_0

    invoke-static {p1}, Lcom/miui/server/smartpower/SmartDisplayPolicyManager$ComingInteraction;->-$$Nest$fgetmInteractType(Lcom/miui/server/smartpower/SmartDisplayPolicyManager$ComingInteraction;)I

    move-result v1

    const/4 v2, 0x6

    if-eq v1, v2, :cond_0

    const/4 v1, 0x1

    goto :goto_0

    :cond_0
    const/4 v1, 0x0

    .line 1332
    .local v1, "intercept":Z
    :goto_0
    if-eqz v1, :cond_1

    .line 1333
    const-string v2, "SmartPower.DisplayPolicy"

    const-string/jumbo v3, "should intercept for surface texture video"

    invoke-static {v2, v3}, Landroid/util/Slog;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 1335
    :cond_1
    return v1
.end method

.method private shouldInterceptUpdateDisplayModeSpecs(Lcom/android/server/display/mode/DisplayModeDirector$DesiredDisplayModeSpecs;)Z
    .locals 4
    .param p1, "modeSpecs"    # Lcom/android/server/display/mode/DisplayModeDirector$DesiredDisplayModeSpecs;

    .line 1343
    invoke-direct {p0}, Lcom/miui/server/smartpower/SmartDisplayPolicyManager$RefreshRatePolicyController;->isAniminting()Z

    move-result v0

    const/4 v1, 0x0

    if-nez v0, :cond_0

    .line 1344
    return v1

    .line 1347
    :cond_0
    iget-object v0, p1, Lcom/android/server/display/mode/DisplayModeDirector$DesiredDisplayModeSpecs;->primary:Landroid/view/SurfaceControl$RefreshRateRanges;

    iget-object v0, v0, Landroid/view/SurfaceControl$RefreshRateRanges;->render:Landroid/view/SurfaceControl$RefreshRateRange;

    iget v0, v0, Landroid/view/SurfaceControl$RefreshRateRange;->max:F

    iget-object v2, p1, Lcom/android/server/display/mode/DisplayModeDirector$DesiredDisplayModeSpecs;->appRequest:Landroid/view/SurfaceControl$RefreshRateRanges;

    iget-object v2, v2, Landroid/view/SurfaceControl$RefreshRateRanges;->render:Landroid/view/SurfaceControl$RefreshRateRange;

    iget v2, v2, Landroid/view/SurfaceControl$RefreshRateRange;->max:F

    invoke-static {v0, v2}, Ljava/lang/Math;->max(FF)F

    move-result v0

    .line 1349
    .local v0, "maxRefreshRate":F
    iget v2, p0, Lcom/miui/server/smartpower/SmartDisplayPolicyManager$RefreshRatePolicyController;->mMaxRefreshRateInPolicy:I

    int-to-float v2, v2

    cmpg-float v2, v0, v2

    if-gez v2, :cond_1

    const/4 v1, 0x1

    .line 1350
    .local v1, "interceptOrNot":Z
    :cond_1
    sget-boolean v2, Lcom/miui/server/smartpower/SmartDisplayPolicyManager;->DEBUG:Z

    if-eqz v2, :cond_2

    .line 1351
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "intercept update mode specs: "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    const-string v3, "SmartPower.DisplayPolicy"

    invoke-static {v3, v2}, Landroid/util/Slog;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 1353
    :cond_2
    return v1
.end method

.method private updateAppropriateRefreshRate()V
    .locals 4

    .line 1406
    iget-object v0, p0, Lcom/miui/server/smartpower/SmartDisplayPolicyManager$RefreshRatePolicyController;->mLock:Ljava/lang/Object;

    monitor-enter v0

    .line 1407
    :try_start_0
    invoke-virtual {p0}, Lcom/miui/server/smartpower/SmartDisplayPolicyManager$RefreshRatePolicyController;->isDefaultRefreshRateEnable()Z

    move-result v1

    iput-boolean v1, p0, Lcom/miui/server/smartpower/SmartDisplayPolicyManager$RefreshRatePolicyController;->mDefaultRefreshRateEnable:Z

    .line 1408
    invoke-virtual {p0}, Lcom/miui/server/smartpower/SmartDisplayPolicyManager$RefreshRatePolicyController;->getCurrentUserRefreshRate()I

    move-result v1

    iput v1, p0, Lcom/miui/server/smartpower/SmartDisplayPolicyManager$RefreshRatePolicyController;->mCurrentUserRefreshRate:I

    .line 1410
    iget-boolean v2, p0, Lcom/miui/server/smartpower/SmartDisplayPolicyManager$RefreshRatePolicyController;->mDefaultRefreshRateEnable:Z

    if-eqz v2, :cond_0

    .line 1411
    iget-object v1, p0, Lcom/miui/server/smartpower/SmartDisplayPolicyManager$RefreshRatePolicyController;->mSupportRefreshRates:Ljava/util/Set;

    invoke-interface {v1}, Ljava/util/Set;->stream()Ljava/util/stream/Stream;

    move-result-object v1

    new-instance v2, Lcom/miui/server/smartpower/SmartDisplayPolicyManager$RefreshRatePolicyController$$ExternalSyntheticLambda0;

    invoke-direct {v2}, Lcom/miui/server/smartpower/SmartDisplayPolicyManager$RefreshRatePolicyController$$ExternalSyntheticLambda0;-><init>()V

    .line 1412
    invoke-interface {v1, v2}, Ljava/util/stream/Stream;->max(Ljava/util/Comparator;)Ljava/util/Optional;

    move-result-object v1

    .line 1413
    invoke-virtual {v1}, Ljava/util/Optional;->get()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/Integer;

    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v1

    iput v1, p0, Lcom/miui/server/smartpower/SmartDisplayPolicyManager$RefreshRatePolicyController;->mMaxRefreshRateInPolicy:I

    .line 1415
    iget v1, p0, Lcom/miui/server/smartpower/SmartDisplayPolicyManager$RefreshRatePolicyController;->LIMIT_MAX_REFRESH_RATE:I

    const/4 v2, -0x1

    if-eq v1, v2, :cond_1

    iget-object v2, p0, Lcom/miui/server/smartpower/SmartDisplayPolicyManager$RefreshRatePolicyController;->mSupportRefreshRates:Ljava/util/Set;

    .line 1416
    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-interface {v2, v1}, Ljava/util/Set;->contains(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_1

    .line 1417
    iget v1, p0, Lcom/miui/server/smartpower/SmartDisplayPolicyManager$RefreshRatePolicyController;->LIMIT_MAX_REFRESH_RATE:I

    iput v1, p0, Lcom/miui/server/smartpower/SmartDisplayPolicyManager$RefreshRatePolicyController;->mMaxRefreshRateInPolicy:I

    goto :goto_0

    .line 1419
    :cond_0
    iget-object v2, p0, Lcom/miui/server/smartpower/SmartDisplayPolicyManager$RefreshRatePolicyController;->mSupportRefreshRates:Ljava/util/Set;

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-interface {v2, v1}, Ljava/util/Set;->contains(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_1

    .line 1420
    iget v1, p0, Lcom/miui/server/smartpower/SmartDisplayPolicyManager$RefreshRatePolicyController;->mCurrentUserRefreshRate:I

    iput v1, p0, Lcom/miui/server/smartpower/SmartDisplayPolicyManager$RefreshRatePolicyController;->mMaxRefreshRateInPolicy:I

    .line 1423
    :cond_1
    :goto_0
    sget-boolean v1, Lcom/miui/server/smartpower/SmartDisplayPolicyManager;->DEBUG:Z

    if-eqz v1, :cond_2

    .line 1424
    const-string v1, "SmartPower.DisplayPolicy"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "default enable: "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget-boolean v3, p0, Lcom/miui/server/smartpower/SmartDisplayPolicyManager$RefreshRatePolicyController;->mDefaultRefreshRateEnable:Z

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, ", refresh rate: "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget v3, p0, Lcom/miui/server/smartpower/SmartDisplayPolicyManager$RefreshRatePolicyController;->mMaxRefreshRateInPolicy:I

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Slog;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 1427
    :cond_2
    monitor-exit v0

    .line 1428
    return-void

    .line 1427
    :catchall_0
    move-exception v1

    monitor-exit v0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v1
.end method

.method private updateControllerLocked()V
    .locals 2

    .line 1153
    iget-object v0, p0, Lcom/miui/server/smartpower/SmartDisplayPolicyManager$RefreshRatePolicyController;->mLock:Ljava/lang/Object;

    monitor-enter v0

    .line 1155
    :try_start_0
    invoke-direct {p0}, Lcom/miui/server/smartpower/SmartDisplayPolicyManager$RefreshRatePolicyController;->resetControllerLocked()V

    .line 1158
    invoke-direct {p0}, Lcom/miui/server/smartpower/SmartDisplayPolicyManager$RefreshRatePolicyController;->updateSupportRefreshRatesLocked()V

    .line 1160
    invoke-direct {p0}, Lcom/miui/server/smartpower/SmartDisplayPolicyManager$RefreshRatePolicyController;->updateAppropriateRefreshRate()V

    .line 1161
    monitor-exit v0

    .line 1162
    return-void

    .line 1161
    :catchall_0
    move-exception v1

    monitor-exit v0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v1
.end method

.method private updateCurrentInteractionLocked(Lcom/miui/server/smartpower/SmartDisplayPolicyManager$ComingInteraction;J)V
    .locals 2
    .param p1, "comingInteraction"    # Lcom/miui/server/smartpower/SmartDisplayPolicyManager$ComingInteraction;
    .param p2, "comingInteractEndTime"    # J

    .line 1358
    invoke-static {p1}, Lcom/miui/server/smartpower/SmartDisplayPolicyManager$ComingInteraction;->-$$Nest$fgetmMaxRefreshRate(Lcom/miui/server/smartpower/SmartDisplayPolicyManager$ComingInteraction;)I

    move-result v0

    iput v0, p0, Lcom/miui/server/smartpower/SmartDisplayPolicyManager$RefreshRatePolicyController;->mCurrentInteractRefreshRate:I

    .line 1359
    invoke-static {p1}, Lcom/miui/server/smartpower/SmartDisplayPolicyManager$ComingInteraction;->-$$Nest$fgetmInteractType(Lcom/miui/server/smartpower/SmartDisplayPolicyManager$ComingInteraction;)I

    move-result v0

    iput v0, p0, Lcom/miui/server/smartpower/SmartDisplayPolicyManager$RefreshRatePolicyController;->mCurrentInteractType:I

    .line 1360
    invoke-static {p1}, Lcom/miui/server/smartpower/SmartDisplayPolicyManager$ComingInteraction;->-$$Nest$fgetmDuration(Lcom/miui/server/smartpower/SmartDisplayPolicyManager$ComingInteraction;)J

    move-result-wide v0

    iput-wide v0, p0, Lcom/miui/server/smartpower/SmartDisplayPolicyManager$RefreshRatePolicyController;->mCurrentInteractDuration:J

    .line 1361
    iput-wide p2, p0, Lcom/miui/server/smartpower/SmartDisplayPolicyManager$RefreshRatePolicyController;->mCurrentInteractEndTime:J

    .line 1362
    return-void
.end method

.method private updateSupportRefreshRatesLocked()V
    .locals 7

    .line 1179
    iget-object v0, p0, Lcom/miui/server/smartpower/SmartDisplayPolicyManager$RefreshRatePolicyController;->this$0:Lcom/miui/server/smartpower/SmartDisplayPolicyManager;

    invoke-static {v0}, Lcom/miui/server/smartpower/SmartDisplayPolicyManager;->-$$Nest$fgetmSupportedDisplayModes(Lcom/miui/server/smartpower/SmartDisplayPolicyManager;)[Landroid/view/SurfaceControl$DisplayMode;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 1180
    iget-object v0, p0, Lcom/miui/server/smartpower/SmartDisplayPolicyManager$RefreshRatePolicyController;->this$0:Lcom/miui/server/smartpower/SmartDisplayPolicyManager;

    invoke-static {v0}, Lcom/miui/server/smartpower/SmartDisplayPolicyManager;->-$$Nest$fgetmSupportedDisplayModes(Lcom/miui/server/smartpower/SmartDisplayPolicyManager;)[Landroid/view/SurfaceControl$DisplayMode;

    move-result-object v0

    array-length v1, v0

    const/4 v2, 0x0

    :goto_0
    if-ge v2, v1, :cond_0

    aget-object v3, v0, v2

    .line 1182
    .local v3, "mode":Landroid/view/SurfaceControl$DisplayMode;
    iget v4, v3, Landroid/view/SurfaceControl$DisplayMode;->refreshRate:F

    const/high16 v5, 0x41200000    # 10.0f

    mul-float/2addr v4, v5

    invoke-static {v4}, Ljava/lang/Math;->round(F)I

    move-result v4

    div-int/lit8 v4, v4, 0xa

    .line 1183
    .local v4, "realRefreshRate":I
    int-to-float v5, v4

    iput v5, v3, Landroid/view/SurfaceControl$DisplayMode;->refreshRate:F

    .line 1184
    iget-object v5, p0, Lcom/miui/server/smartpower/SmartDisplayPolicyManager$RefreshRatePolicyController;->mSupportRefreshRates:Ljava/util/Set;

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v6

    invoke-interface {v5, v6}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    .line 1185
    iget v5, p0, Lcom/miui/server/smartpower/SmartDisplayPolicyManager$RefreshRatePolicyController;->mMaxRefreshRateSupport:I

    invoke-static {v5, v4}, Ljava/lang/Math;->max(II)I

    move-result v5

    iput v5, p0, Lcom/miui/server/smartpower/SmartDisplayPolicyManager$RefreshRatePolicyController;->mMaxRefreshRateSupport:I

    .line 1180
    .end local v3    # "mode":Landroid/view/SurfaceControl$DisplayMode;
    .end local v4    # "realRefreshRate":I
    add-int/lit8 v2, v2, 0x1

    goto :goto_0

    .line 1188
    :cond_0
    return-void
.end method


# virtual methods
.method public getCurrentUserRefreshRate()I
    .locals 3

    .line 1438
    iget-object v0, p0, Lcom/miui/server/smartpower/SmartDisplayPolicyManager$RefreshRatePolicyController;->mContext:Landroid/content/Context;

    .line 1439
    invoke-virtual {v0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    .line 1438
    const-string/jumbo v1, "user_refresh_rate"

    const/4 v2, 0x0

    invoke-static {v0, v1, v2}, Landroid/provider/Settings$Secure;->getInt(Landroid/content/ContentResolver;Ljava/lang/String;I)I

    move-result v0

    return v0
.end method

.method public isDefaultRefreshRateEnable()Z
    .locals 3

    .line 1431
    iget-object v0, p0, Lcom/miui/server/smartpower/SmartDisplayPolicyManager$RefreshRatePolicyController;->mContext:Landroid/content/Context;

    .line 1432
    invoke-virtual {v0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    .line 1431
    const-string v1, "is_smart_fps"

    const/4 v2, 0x0

    invoke-static {v0, v1, v2}, Landroid/provider/Settings$System;->getInt(Landroid/content/ContentResolver;Ljava/lang/String;I)I

    move-result v0

    const/4 v1, 0x1

    if-ne v0, v1, :cond_0

    move v2, v1

    :cond_0
    return v2
.end method
