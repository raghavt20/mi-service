.class Lcom/miui/server/smartpower/SmartDisplayPolicyManager$FrameInsertionObserver;
.super Landroid/database/ContentObserver;
.source "SmartDisplayPolicyManager.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/miui/server/smartpower/SmartDisplayPolicyManager;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "FrameInsertionObserver"
.end annotation


# static fields
.field private static final IRIS_GAME_OFF:I = 0x0

.field private static final IRIS_GAME_ON_EXTERNAL_FRAME:I = 0x3

.field private static final IRIS_GAME_ON_SUPER_RESOLUTION:I = 0x2

.field private static final IRIS_GAME_ON_WITHIN_FRAME:I = 0x1

.field private static final IRIS_GAME_ON_WITHIN_FRAME_SUPER_RESOLUTION:I = 0x4

.field public static final IRIS_GAME_STATUS:Ljava/lang/String; = "game_iris_status"

.field private static final IRIS_VIDEO_OFF:I = 0x0

.field private static final IRIS_VIDEO_ON:I = 0x1

.field public static final IRIS_VIDEO_STATUS:Ljava/lang/String; = "video_iris_status"


# instance fields
.field private final mContentResolver:Landroid/content/ContentResolver;

.field private final mContext:Landroid/content/Context;

.field private final mIrisGameStatusUri:Landroid/net/Uri;

.field private final mIrisVideoStatusUri:Landroid/net/Uri;

.field final synthetic this$0:Lcom/miui/server/smartpower/SmartDisplayPolicyManager;


# direct methods
.method public constructor <init>(Lcom/miui/server/smartpower/SmartDisplayPolicyManager;Landroid/content/Context;Landroid/os/Handler;)V
    .locals 3
    .param p2, "context"    # Landroid/content/Context;
    .param p3, "handler"    # Landroid/os/Handler;

    .line 941
    iput-object p1, p0, Lcom/miui/server/smartpower/SmartDisplayPolicyManager$FrameInsertionObserver;->this$0:Lcom/miui/server/smartpower/SmartDisplayPolicyManager;

    .line 942
    invoke-direct {p0, p3}, Landroid/database/ContentObserver;-><init>(Landroid/os/Handler;)V

    .line 917
    const-string p1, "game_iris_status"

    invoke-static {p1}, Landroid/provider/Settings$System;->getUriFor(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object p1

    iput-object p1, p0, Lcom/miui/server/smartpower/SmartDisplayPolicyManager$FrameInsertionObserver;->mIrisGameStatusUri:Landroid/net/Uri;

    .line 918
    const-string/jumbo v0, "video_iris_status"

    invoke-static {v0}, Landroid/provider/Settings$System;->getUriFor(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v0

    iput-object v0, p0, Lcom/miui/server/smartpower/SmartDisplayPolicyManager$FrameInsertionObserver;->mIrisVideoStatusUri:Landroid/net/Uri;

    .line 943
    iput-object p2, p0, Lcom/miui/server/smartpower/SmartDisplayPolicyManager$FrameInsertionObserver;->mContext:Landroid/content/Context;

    .line 944
    invoke-virtual {p2}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v1

    iput-object v1, p0, Lcom/miui/server/smartpower/SmartDisplayPolicyManager$FrameInsertionObserver;->mContentResolver:Landroid/content/ContentResolver;

    .line 946
    const/4 v2, 0x0

    invoke-virtual {v1, p1, v2, p0, v2}, Landroid/content/ContentResolver;->registerContentObserver(Landroid/net/Uri;ZLandroid/database/ContentObserver;I)V

    .line 948
    invoke-virtual {v1, v0, v2, p0, v2}, Landroid/content/ContentResolver;->registerContentObserver(Landroid/net/Uri;ZLandroid/database/ContentObserver;I)V

    .line 950
    return-void
.end method

.method private updateFrameInsertScene()V
    .locals 2

    .line 978
    iget-object v0, p0, Lcom/miui/server/smartpower/SmartDisplayPolicyManager$FrameInsertionObserver;->this$0:Lcom/miui/server/smartpower/SmartDisplayPolicyManager;

    invoke-static {v0}, Lcom/miui/server/smartpower/SmartDisplayPolicyManager;->-$$Nest$fgetmFrameInsertingForGame(Lcom/miui/server/smartpower/SmartDisplayPolicyManager;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 979
    iget-object v0, p0, Lcom/miui/server/smartpower/SmartDisplayPolicyManager$FrameInsertionObserver;->this$0:Lcom/miui/server/smartpower/SmartDisplayPolicyManager;

    const-string v1, "game"

    invoke-static {v0, v1}, Lcom/miui/server/smartpower/SmartDisplayPolicyManager;->-$$Nest$fputmFrameInsertScene(Lcom/miui/server/smartpower/SmartDisplayPolicyManager;Ljava/lang/String;)V

    goto :goto_0

    .line 980
    :cond_0
    iget-object v0, p0, Lcom/miui/server/smartpower/SmartDisplayPolicyManager$FrameInsertionObserver;->this$0:Lcom/miui/server/smartpower/SmartDisplayPolicyManager;

    invoke-static {v0}, Lcom/miui/server/smartpower/SmartDisplayPolicyManager;->-$$Nest$fgetmFrameInsertingForVideo(Lcom/miui/server/smartpower/SmartDisplayPolicyManager;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 981
    iget-object v0, p0, Lcom/miui/server/smartpower/SmartDisplayPolicyManager$FrameInsertionObserver;->this$0:Lcom/miui/server/smartpower/SmartDisplayPolicyManager;

    const-string/jumbo v1, "video"

    invoke-static {v0, v1}, Lcom/miui/server/smartpower/SmartDisplayPolicyManager;->-$$Nest$fputmFrameInsertScene(Lcom/miui/server/smartpower/SmartDisplayPolicyManager;Ljava/lang/String;)V

    .line 983
    :cond_1
    :goto_0
    return-void
.end method

.method private updateIrisGameStatus()V
    .locals 5

    .line 962
    iget-object v0, p0, Lcom/miui/server/smartpower/SmartDisplayPolicyManager$FrameInsertionObserver;->mContext:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    const-string v1, "game_iris_status"

    const/4 v2, 0x0

    invoke-static {v0, v1, v2}, Landroid/provider/Settings$System;->getInt(Landroid/content/ContentResolver;Ljava/lang/String;I)I

    move-result v0

    .line 964
    .local v0, "gameStatus":I
    iget-object v1, p0, Lcom/miui/server/smartpower/SmartDisplayPolicyManager$FrameInsertionObserver;->this$0:Lcom/miui/server/smartpower/SmartDisplayPolicyManager;

    const/4 v3, 0x1

    if-eq v0, v3, :cond_0

    const/4 v4, 0x3

    if-eq v0, v4, :cond_0

    const/4 v4, 0x4

    if-ne v0, v4, :cond_1

    :cond_0
    move v2, v3

    :cond_1
    invoke-static {v1, v2}, Lcom/miui/server/smartpower/SmartDisplayPolicyManager;->-$$Nest$fputmFrameInsertingForGame(Lcom/miui/server/smartpower/SmartDisplayPolicyManager;Z)V

    .line 967
    invoke-direct {p0}, Lcom/miui/server/smartpower/SmartDisplayPolicyManager$FrameInsertionObserver;->updateFrameInsertScene()V

    .line 968
    return-void
.end method

.method private updateIrisVideoStatus()V
    .locals 4

    .line 971
    iget-object v0, p0, Lcom/miui/server/smartpower/SmartDisplayPolicyManager$FrameInsertionObserver;->mContext:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    const-string/jumbo v1, "video_iris_status"

    const/4 v2, 0x0

    invoke-static {v0, v1, v2}, Landroid/provider/Settings$System;->getInt(Landroid/content/ContentResolver;Ljava/lang/String;I)I

    move-result v0

    .line 973
    .local v0, "videoStatus":I
    iget-object v1, p0, Lcom/miui/server/smartpower/SmartDisplayPolicyManager$FrameInsertionObserver;->this$0:Lcom/miui/server/smartpower/SmartDisplayPolicyManager;

    const/4 v3, 0x1

    if-ne v0, v3, :cond_0

    move v2, v3

    :cond_0
    invoke-static {v1, v2}, Lcom/miui/server/smartpower/SmartDisplayPolicyManager;->-$$Nest$fputmFrameInsertingForVideo(Lcom/miui/server/smartpower/SmartDisplayPolicyManager;Z)V

    .line 974
    invoke-direct {p0}, Lcom/miui/server/smartpower/SmartDisplayPolicyManager$FrameInsertionObserver;->updateFrameInsertScene()V

    .line 975
    return-void
.end method


# virtual methods
.method public onChange(ZLandroid/net/Uri;I)V
    .locals 1
    .param p1, "selfChange"    # Z
    .param p2, "uri"    # Landroid/net/Uri;
    .param p3, "flags"    # I

    .line 954
    iget-object v0, p0, Lcom/miui/server/smartpower/SmartDisplayPolicyManager$FrameInsertionObserver;->mIrisGameStatusUri:Landroid/net/Uri;

    invoke-virtual {v0, p2}, Landroid/net/Uri;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 955
    invoke-direct {p0}, Lcom/miui/server/smartpower/SmartDisplayPolicyManager$FrameInsertionObserver;->updateIrisGameStatus()V

    goto :goto_0

    .line 956
    :cond_0
    iget-object v0, p0, Lcom/miui/server/smartpower/SmartDisplayPolicyManager$FrameInsertionObserver;->mIrisVideoStatusUri:Landroid/net/Uri;

    invoke-virtual {v0, p2}, Landroid/net/Uri;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 957
    invoke-direct {p0}, Lcom/miui/server/smartpower/SmartDisplayPolicyManager$FrameInsertionObserver;->updateIrisVideoStatus()V

    .line 959
    :cond_1
    :goto_0
    return-void
.end method
