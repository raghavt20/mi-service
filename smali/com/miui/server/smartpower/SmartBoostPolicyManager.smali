.class public Lcom/miui/server/smartpower/SmartBoostPolicyManager;
.super Ljava/lang/Object;
.source "SmartBoostPolicyManager.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/miui/server/smartpower/SmartBoostPolicyManager$SchedBoostController;
    }
.end annotation


# static fields
.field public static final BOOST_TYPE_APP_TRANSITION:I = 0x2

.field public static final BOOST_TYPE_MULTI_TASK:I = 0x1

.field public static final DEBUG:Z

.field public static final TAG:Ljava/lang/String; = "SmartPower.BoostPolicy"


# instance fields
.field private final mContext:Landroid/content/Context;

.field private final mHandler:Landroid/os/Handler;

.field private mProcessManagerInternal:Lcom/miui/server/process/ProcessManagerInternal;

.field private mSchedBoostController:Lcom/miui/server/smartpower/SmartBoostPolicyManager$SchedBoostController;


# direct methods
.method static bridge synthetic -$$Nest$mboostTypeToString(Lcom/miui/server/smartpower/SmartBoostPolicyManager;I)Ljava/lang/String;
    .locals 0

    invoke-direct {p0, p1}, Lcom/miui/server/smartpower/SmartBoostPolicyManager;->boostTypeToString(I)Ljava/lang/String;

    move-result-object p0

    return-object p0
.end method

.method static bridge synthetic -$$Nest$mgetProcessNameByPid(Lcom/miui/server/smartpower/SmartBoostPolicyManager;I)Ljava/lang/String;
    .locals 0

    invoke-direct {p0, p1}, Lcom/miui/server/smartpower/SmartBoostPolicyManager;->getProcessNameByPid(I)Ljava/lang/String;

    move-result-object p0

    return-object p0
.end method

.method static constructor <clinit>()V
    .locals 1

    .line 19
    sget-boolean v0, Lcom/android/server/am/SmartPowerService;->DEBUG:Z

    sput-boolean v0, Lcom/miui/server/smartpower/SmartBoostPolicyManager;->DEBUG:Z

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/os/Looper;)V
    .locals 1
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "looper"    # Landroid/os/Looper;

    .line 32
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 33
    iput-object p1, p0, Lcom/miui/server/smartpower/SmartBoostPolicyManager;->mContext:Landroid/content/Context;

    .line 34
    new-instance v0, Landroid/os/Handler;

    invoke-direct {v0, p2}, Landroid/os/Handler;-><init>(Landroid/os/Looper;)V

    iput-object v0, p0, Lcom/miui/server/smartpower/SmartBoostPolicyManager;->mHandler:Landroid/os/Handler;

    .line 35
    return-void
.end method

.method private boostTypeToString(I)Ljava/lang/String;
    .locals 1
    .param p1, "boostType"    # I

    .line 61
    packed-switch p1, :pswitch_data_0

    .line 67
    const-string v0, "UNKNOWN"

    return-object v0

    .line 65
    :pswitch_0
    const-string v0, "APP_TRANSITION"

    return-object v0

    .line 63
    :pswitch_1
    const-string v0, "MULTI_TASK"

    return-object v0

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_1
        :pswitch_0
    .end packed-switch
.end method

.method private getProcessNameByPid(I)Ljava/lang/String;
    .locals 2
    .param p1, "pid"    # I

    .line 76
    iget-object v0, p0, Lcom/miui/server/smartpower/SmartBoostPolicyManager;->mProcessManagerInternal:Lcom/miui/server/process/ProcessManagerInternal;

    invoke-virtual {v0, p1}, Lcom/miui/server/process/ProcessManagerInternal;->getProcessRecordByPid(I)Lcom/android/server/am/ProcessRecord;

    move-result-object v0

    .line 77
    .local v0, "record":Lcom/android/server/am/ProcessRecord;
    if-eqz v0, :cond_0

    iget-object v1, v0, Lcom/android/server/am/ProcessRecord;->processName:Ljava/lang/String;

    goto :goto_0

    :cond_0
    const-string v1, ""

    :goto_0
    return-object v1
.end method


# virtual methods
.method public beginSchedThreads([III)V
    .locals 8
    .param p1, "tids"    # [I
    .param p2, "callingPid"    # I
    .param p3, "boostType"    # I

    .line 47
    const-wide/16 v6, 0x7d0

    .line 48
    .local v6, "defaultDuration":J
    iget-object v0, p0, Lcom/miui/server/smartpower/SmartBoostPolicyManager;->mSchedBoostController:Lcom/miui/server/smartpower/SmartBoostPolicyManager$SchedBoostController;

    move-object v1, p1

    move v2, p2

    move-wide v3, v6

    move v5, p3

    invoke-virtual/range {v0 .. v5}, Lcom/miui/server/smartpower/SmartBoostPolicyManager$SchedBoostController;->beginSchedThreads([IIJI)V

    .line 49
    return-void
.end method

.method public beginSchedThreads([IIJI)V
    .locals 6
    .param p1, "tids"    # [I
    .param p2, "callingPid"    # I
    .param p3, "duration"    # J
    .param p5, "boostType"    # I

    .line 52
    iget-object v0, p0, Lcom/miui/server/smartpower/SmartBoostPolicyManager;->mSchedBoostController:Lcom/miui/server/smartpower/SmartBoostPolicyManager$SchedBoostController;

    move-object v1, p1

    move v2, p2

    move-wide v3, p3

    move v5, p5

    invoke-virtual/range {v0 .. v5}, Lcom/miui/server/smartpower/SmartBoostPolicyManager$SchedBoostController;->beginSchedThreads([IIJI)V

    .line 53
    return-void
.end method

.method public init()V
    .locals 0

    .line 38
    return-void
.end method

.method public setMultiSenceEnable(Z)V
    .locals 0
    .param p1, "enable"    # Z

    .line 72
    sput-boolean p1, Lcom/miui/server/smartpower/SmartBoostPolicyManager$SchedBoostController;->MULTI_SENCE_ENABLE:Z

    .line 73
    return-void
.end method

.method public stopCurrentSchedBoost([III)V
    .locals 8
    .param p1, "tids"    # [I
    .param p2, "callingPid"    # I
    .param p3, "boostType"    # I

    .line 56
    const-wide/16 v6, 0x0

    .line 57
    .local v6, "stopDuration":J
    iget-object v0, p0, Lcom/miui/server/smartpower/SmartBoostPolicyManager;->mSchedBoostController:Lcom/miui/server/smartpower/SmartBoostPolicyManager$SchedBoostController;

    move-object v1, p1

    move v2, p2

    move-wide v3, v6

    move v5, p3

    invoke-virtual/range {v0 .. v5}, Lcom/miui/server/smartpower/SmartBoostPolicyManager$SchedBoostController;->beginSchedThreads([IIJI)V

    .line 58
    return-void
.end method

.method public systemReady()V
    .locals 1

    .line 41
    const-class v0, Lcom/miui/server/process/ProcessManagerInternal;

    invoke-static {v0}, Lcom/android/server/LocalServices;->getService(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/miui/server/process/ProcessManagerInternal;

    iput-object v0, p0, Lcom/miui/server/smartpower/SmartBoostPolicyManager;->mProcessManagerInternal:Lcom/miui/server/process/ProcessManagerInternal;

    .line 43
    new-instance v0, Lcom/miui/server/smartpower/SmartBoostPolicyManager$SchedBoostController;

    invoke-direct {v0, p0}, Lcom/miui/server/smartpower/SmartBoostPolicyManager$SchedBoostController;-><init>(Lcom/miui/server/smartpower/SmartBoostPolicyManager;)V

    iput-object v0, p0, Lcom/miui/server/smartpower/SmartBoostPolicyManager;->mSchedBoostController:Lcom/miui/server/smartpower/SmartBoostPolicyManager$SchedBoostController;

    .line 44
    return-void
.end method
