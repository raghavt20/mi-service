.class public Lcom/miui/server/smartpower/ComponentsManager;
.super Ljava/lang/Object;
.source "ComponentsManager.java"


# static fields
.field public static final DEBUG:Z

.field public static final TAG:Ljava/lang/String; = "SmartPower"


# instance fields
.field private mAppStateManager:Lcom/android/server/am/AppStateManager;

.field private final mPendingUids:Landroid/util/ArraySet;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/util/ArraySet<",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation
.end field

.field private mSmartPowerPolicyManager:Lcom/miui/server/smartpower/SmartPowerPolicyManager;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .line 21
    sget-boolean v0, Lcom/android/server/am/SmartPowerService;->DEBUG:Z

    sput-boolean v0, Lcom/miui/server/smartpower/ComponentsManager;->DEBUG:Z

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/os/Looper;)V
    .locals 1
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "looper"    # Landroid/os/Looper;

    .line 29
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 22
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/miui/server/smartpower/ComponentsManager;->mAppStateManager:Lcom/android/server/am/AppStateManager;

    .line 23
    iput-object v0, p0, Lcom/miui/server/smartpower/ComponentsManager;->mSmartPowerPolicyManager:Lcom/miui/server/smartpower/SmartPowerPolicyManager;

    .line 27
    new-instance v0, Landroid/util/ArraySet;

    invoke-direct {v0}, Landroid/util/ArraySet;-><init>()V

    iput-object v0, p0, Lcom/miui/server/smartpower/ComponentsManager;->mPendingUids:Landroid/util/ArraySet;

    .line 30
    return-void
.end method

.method static serviceExecutionToString(I)Ljava/lang/String;
    .locals 1
    .param p0, "execution"    # I

    .line 146
    packed-switch p0, :pswitch_data_0

    .line 160
    invoke-static {p0}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v0

    return-object v0

    .line 158
    :pswitch_0
    const-string/jumbo v0, "unbind"

    return-object v0

    .line 156
    :pswitch_1
    const-string v0, "destroy"

    return-object v0

    .line 154
    :pswitch_2
    const-string/jumbo v0, "start"

    return-object v0

    .line 152
    :pswitch_3
    const-string v0, "create"

    return-object v0

    .line 150
    :pswitch_4
    const-string v0, "bind"

    return-object v0

    .line 148
    :pswitch_5
    const-string v0, "end"

    return-object v0

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_5
        :pswitch_4
        :pswitch_3
        :pswitch_2
        :pswitch_1
        :pswitch_0
    .end packed-switch
.end method


# virtual methods
.method public activityStartBeforeLocked(Ljava/lang/String;IILjava/lang/String;Z)V
    .locals 6
    .param p1, "name"    # Ljava/lang/String;
    .param p2, "fromPid"    # I
    .param p3, "toUid"    # I
    .param p4, "packageName"    # Ljava/lang/String;
    .param p5, "isColdStart"    # Z

    .line 68
    iget-object v0, p0, Lcom/miui/server/smartpower/ComponentsManager;->mAppStateManager:Lcom/android/server/am/AppStateManager;

    move-object v1, p1

    move v2, p2

    move v3, p3

    move-object v4, p4

    move v5, p5

    invoke-virtual/range {v0 .. v5}, Lcom/android/server/am/AppStateManager;->activityStartBeforeLocked(Ljava/lang/String;IILjava/lang/String;Z)V

    .line 69
    return-void
.end method

.method public activityVisibilityChangedLocked(ILjava/lang/String;Lcom/android/server/wm/ActivityRecord;Z)V
    .locals 3
    .param p1, "uid"    # I
    .param p2, "processName"    # Ljava/lang/String;
    .param p3, "record"    # Lcom/android/server/wm/ActivityRecord;
    .param p4, "visible"    # Z

    .line 48
    iget-object v0, p0, Lcom/miui/server/smartpower/ComponentsManager;->mPendingUids:Landroid/util/ArraySet;

    monitor-enter v0

    .line 49
    :try_start_0
    iget-object v1, p0, Lcom/miui/server/smartpower/ComponentsManager;->mPendingUids:Landroid/util/ArraySet;

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v1, v2}, Landroid/util/ArraySet;->add(Ljava/lang/Object;)Z

    .line 50
    iget-object v1, p0, Lcom/miui/server/smartpower/ComponentsManager;->mAppStateManager:Lcom/android/server/am/AppStateManager;

    invoke-virtual {v1, p1, p2, p3, p4}, Lcom/android/server/am/AppStateManager;->setActivityVisible(ILjava/lang/String;Lcom/android/server/wm/ActivityRecord;Z)V

    .line 51
    if-eqz p4, :cond_0

    .line 52
    invoke-virtual {p0}, Lcom/miui/server/smartpower/ComponentsManager;->updateActivitiesVisibilityLocked()V

    .line 54
    :cond_0
    monitor-exit v0

    .line 55
    return-void

    .line 54
    :catchall_0
    move-exception v1

    monitor-exit v0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v1
.end method

.method public alarmStatusChangedLocked(IZ)V
    .locals 2
    .param p1, "uid"    # I
    .param p2, "active"    # Z

    .line 123
    if-eqz p2, :cond_0

    .line 124
    const-string v0, "alarm start"

    .line 125
    .local v0, "content":Ljava/lang/String;
    iget-object v1, p0, Lcom/miui/server/smartpower/ComponentsManager;->mAppStateManager:Lcom/android/server/am/AppStateManager;

    invoke-virtual {v1, p1, v0}, Lcom/android/server/am/AppStateManager;->componentStartLocked(ILjava/lang/String;)V

    .line 126
    .end local v0    # "content":Ljava/lang/String;
    goto :goto_0

    .line 127
    :cond_0
    const-string v0, "alarm end"

    .line 128
    .restart local v0    # "content":Ljava/lang/String;
    iget-object v1, p0, Lcom/miui/server/smartpower/ComponentsManager;->mAppStateManager:Lcom/android/server/am/AppStateManager;

    invoke-virtual {v1, p1, v0}, Lcom/android/server/am/AppStateManager;->componentEndLocked(ILjava/lang/String;)V

    .line 130
    .end local v0    # "content":Ljava/lang/String;
    :goto_0
    return-void
.end method

.method public broadcastStatusChangedLocked(Lcom/android/server/am/ProcessRecord;ZLandroid/content/Intent;)V
    .locals 2
    .param p1, "processRecord"    # Lcom/android/server/am/ProcessRecord;
    .param p2, "active"    # Z
    .param p3, "intent"    # Landroid/content/Intent;

    .line 80
    if-eqz p2, :cond_0

    .line 81
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "broadcast start "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {p3}, Landroid/content/Intent;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    .line 82
    .local v0, "content":Ljava/lang/String;
    iget-object v1, p0, Lcom/miui/server/smartpower/ComponentsManager;->mAppStateManager:Lcom/android/server/am/AppStateManager;

    invoke-virtual {v1, p1, v0}, Lcom/android/server/am/AppStateManager;->componentStartLocked(Lcom/android/server/am/ProcessRecord;Ljava/lang/String;)V

    .line 83
    .end local v0    # "content":Ljava/lang/String;
    goto :goto_0

    .line 84
    :cond_0
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "broadcast end "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {p3}, Landroid/content/Intent;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    .line 85
    .restart local v0    # "content":Ljava/lang/String;
    iget-object v1, p0, Lcom/miui/server/smartpower/ComponentsManager;->mAppStateManager:Lcom/android/server/am/AppStateManager;

    invoke-virtual {v1, p1, v0}, Lcom/android/server/am/AppStateManager;->componentEndLocked(Lcom/android/server/am/ProcessRecord;Ljava/lang/String;)V

    .line 87
    .end local v0    # "content":Ljava/lang/String;
    :goto_0
    return-void
.end method

.method public contentProviderStatusChangedLocked(Lcom/android/server/am/ProcessRecord;Ljava/lang/String;)V
    .locals 2
    .param p1, "hostingProc"    # Lcom/android/server/am/ProcessRecord;
    .param p2, "name"    # Ljava/lang/String;

    .line 105
    if-nez p1, :cond_0

    .line 106
    return-void

    .line 108
    :cond_0
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "content provider start "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    .line 109
    .local v0, "content":Ljava/lang/String;
    iget-object v1, p0, Lcom/miui/server/smartpower/ComponentsManager;->mAppStateManager:Lcom/android/server/am/AppStateManager;

    invoke-virtual {v1, p1, v0}, Lcom/android/server/am/AppStateManager;->componentStartLocked(Lcom/android/server/am/ProcessRecord;Ljava/lang/String;)V

    .line 110
    return-void
.end method

.method public init(Lcom/android/server/am/AppStateManager;Lcom/miui/server/smartpower/SmartPowerPolicyManager;)V
    .locals 0
    .param p1, "appStateManager"    # Lcom/android/server/am/AppStateManager;
    .param p2, "smartPowerPolicyManager"    # Lcom/miui/server/smartpower/SmartPowerPolicyManager;

    .line 34
    iput-object p1, p0, Lcom/miui/server/smartpower/ComponentsManager;->mAppStateManager:Lcom/android/server/am/AppStateManager;

    .line 35
    iput-object p2, p0, Lcom/miui/server/smartpower/ComponentsManager;->mSmartPowerPolicyManager:Lcom/miui/server/smartpower/SmartPowerPolicyManager;

    .line 36
    return-void
.end method

.method public onApplyOomAdjLocked(Lcom/android/server/am/ProcessRecord;)V
    .locals 1
    .param p1, "app"    # Lcom/android/server/am/ProcessRecord;

    .line 133
    iget-object v0, p0, Lcom/miui/server/smartpower/ComponentsManager;->mAppStateManager:Lcom/android/server/am/AppStateManager;

    invoke-virtual {v0, p1}, Lcom/android/server/am/AppStateManager;->onApplyOomAdjLocked(Lcom/android/server/am/ProcessRecord;)V

    .line 134
    return-void
.end method

.method public onForegroundActivityChangedLocked(IIILjava/lang/String;IILjava/lang/String;)V
    .locals 8
    .param p1, "fromPid"    # I
    .param p2, "toUid"    # I
    .param p3, "toPid"    # I
    .param p4, "name"    # Ljava/lang/String;
    .param p5, "taskId"    # I
    .param p6, "callingUid"    # I
    .param p7, "callingPkg"    # Ljava/lang/String;

    .line 74
    iget-object v0, p0, Lcom/miui/server/smartpower/ComponentsManager;->mAppStateManager:Lcom/android/server/am/AppStateManager;

    move v1, p1

    move v2, p2

    move v3, p3

    move-object v4, p4

    move v5, p5

    move v6, p6

    move-object v7, p7

    invoke-virtual/range {v0 .. v7}, Lcom/android/server/am/AppStateManager;->onForegroundActivityChangedLocked(IIILjava/lang/String;IILjava/lang/String;)V

    .line 76
    return-void
.end method

.method public onSendPendingIntent(ILjava/lang/String;)V
    .locals 1
    .param p1, "uid"    # I
    .param p2, "typeName"    # Ljava/lang/String;

    .line 142
    iget-object v0, p0, Lcom/miui/server/smartpower/ComponentsManager;->mAppStateManager:Lcom/android/server/am/AppStateManager;

    invoke-virtual {v0, p1, p2}, Lcom/android/server/am/AppStateManager;->onSendPendingIntent(ILjava/lang/String;)V

    .line 143
    return-void
.end method

.method public providerConnectionChangedLocked(Lcom/android/server/am/ProcessRecord;Lcom/android/server/am/ProcessRecord;Z)V
    .locals 1
    .param p1, "client"    # Lcom/android/server/am/ProcessRecord;
    .param p2, "host"    # Lcom/android/server/am/ProcessRecord;
    .param p3, "isConnect"    # Z

    .line 119
    iget-object v0, p0, Lcom/miui/server/smartpower/ComponentsManager;->mAppStateManager:Lcom/android/server/am/AppStateManager;

    invoke-virtual {v0, p1, p2, p3}, Lcom/android/server/am/AppStateManager;->providerConnectionChangedLocked(Lcom/android/server/am/ProcessRecord;Lcom/android/server/am/ProcessRecord;Z)V

    .line 120
    return-void
.end method

.method public serviceConnectionChangedLocked(Lcom/android/server/am/ProcessRecord;Lcom/android/server/am/ProcessRecord;ZJ)V
    .locals 6
    .param p1, "client"    # Lcom/android/server/am/ProcessRecord;
    .param p2, "service"    # Lcom/android/server/am/ProcessRecord;
    .param p3, "isConnect"    # Z
    .param p4, "flags"    # J

    .line 114
    iget-object v0, p0, Lcom/miui/server/smartpower/ComponentsManager;->mAppStateManager:Lcom/android/server/am/AppStateManager;

    move-object v1, p1

    move-object v2, p2

    move v3, p3

    move-wide v4, p4

    invoke-virtual/range {v0 .. v5}, Lcom/android/server/am/AppStateManager;->serviceConnectionChangedLocked(Lcom/android/server/am/ProcessRecord;Lcom/android/server/am/ProcessRecord;ZJ)V

    .line 115
    return-void
.end method

.method public serviceStatusChangedLocked(Lcom/android/server/am/ProcessRecord;ZLjava/lang/String;I)V
    .locals 4
    .param p1, "processRecord"    # Lcom/android/server/am/ProcessRecord;
    .param p2, "active"    # Z
    .param p3, "name"    # Ljava/lang/String;
    .param p4, "execution"    # I

    .line 91
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v1, "service "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    .line 92
    invoke-static {p4}, Lcom/miui/server/smartpower/ComponentsManager;->serviceExecutionToString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, " "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    .line 93
    .local v0, "content":Ljava/lang/String;
    if-eqz p2, :cond_1

    .line 94
    iget-object v1, p0, Lcom/miui/server/smartpower/ComponentsManager;->mAppStateManager:Lcom/android/server/am/AppStateManager;

    iget v2, p1, Lcom/android/server/am/ProcessRecord;->uid:I

    iget-object v3, p1, Lcom/android/server/am/ProcessRecord;->processName:Ljava/lang/String;

    invoke-virtual {v1, v2, v3}, Lcom/android/server/am/AppStateManager;->getProcessState(ILjava/lang/String;)I

    move-result v1

    .line 96
    .local v1, "state":I
    const/4 v2, 0x6

    if-le v1, v2, :cond_0

    .line 97
    iget-object v2, p0, Lcom/miui/server/smartpower/ComponentsManager;->mAppStateManager:Lcom/android/server/am/AppStateManager;

    invoke-virtual {v2, p1, v0}, Lcom/android/server/am/AppStateManager;->componentStartLocked(Lcom/android/server/am/ProcessRecord;Ljava/lang/String;)V

    .line 99
    .end local v1    # "state":I
    :cond_0
    goto :goto_0

    .line 100
    :cond_1
    iget-object v1, p0, Lcom/miui/server/smartpower/ComponentsManager;->mAppStateManager:Lcom/android/server/am/AppStateManager;

    invoke-virtual {v1, p1, v0}, Lcom/android/server/am/AppStateManager;->componentEndLocked(Lcom/android/server/am/ProcessRecord;Ljava/lang/String;)V

    .line 102
    :goto_0
    return-void
.end method

.method public updateActivitiesVisibilityLocked()V
    .locals 3

    .line 40
    iget-object v0, p0, Lcom/miui/server/smartpower/ComponentsManager;->mPendingUids:Landroid/util/ArraySet;

    monitor-enter v0

    .line 41
    :try_start_0
    iget-object v1, p0, Lcom/miui/server/smartpower/ComponentsManager;->mAppStateManager:Lcom/android/server/am/AppStateManager;

    iget-object v2, p0, Lcom/miui/server/smartpower/ComponentsManager;->mPendingUids:Landroid/util/ArraySet;

    invoke-virtual {v1, v2}, Lcom/android/server/am/AppStateManager;->updateVisibilityLocked(Landroid/util/ArraySet;)V

    .line 42
    iget-object v1, p0, Lcom/miui/server/smartpower/ComponentsManager;->mPendingUids:Landroid/util/ArraySet;

    invoke-virtual {v1}, Landroid/util/ArraySet;->clear()V

    .line 43
    monitor-exit v0

    .line 44
    return-void

    .line 43
    :catchall_0
    move-exception v1

    monitor-exit v0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v1
.end method

.method public windowVisibilityChangedLocked(IILcom/android/server/policy/WindowManagerPolicy$WindowState;Landroid/view/WindowManager$LayoutParams;Z)V
    .locals 9
    .param p1, "uid"    # I
    .param p2, "pid"    # I
    .param p3, "win"    # Lcom/android/server/policy/WindowManagerPolicy$WindowState;
    .param p4, "attrs"    # Landroid/view/WindowManager$LayoutParams;
    .param p5, "visible"    # Z

    .line 59
    iget-object v0, p0, Lcom/miui/server/smartpower/ComponentsManager;->mPendingUids:Landroid/util/ArraySet;

    monitor-enter v0

    .line 60
    :try_start_0
    iget-object v1, p0, Lcom/miui/server/smartpower/ComponentsManager;->mPendingUids:Landroid/util/ArraySet;

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v1, v2}, Landroid/util/ArraySet;->add(Ljava/lang/Object;)Z

    .line 61
    iget-object v3, p0, Lcom/miui/server/smartpower/ComponentsManager;->mAppStateManager:Lcom/android/server/am/AppStateManager;

    move v4, p1

    move v5, p2

    move-object v6, p3

    move-object v7, p4

    move v8, p5

    invoke-virtual/range {v3 .. v8}, Lcom/android/server/am/AppStateManager;->setWindowVisible(IILcom/android/server/policy/WindowManagerPolicy$WindowState;Landroid/view/WindowManager$LayoutParams;Z)V

    .line 62
    invoke-virtual {p0}, Lcom/miui/server/smartpower/ComponentsManager;->updateActivitiesVisibilityLocked()V

    .line 63
    monitor-exit v0

    .line 64
    return-void

    .line 63
    :catchall_0
    move-exception v1

    monitor-exit v0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v1
.end method
