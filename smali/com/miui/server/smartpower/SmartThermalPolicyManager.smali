.class public Lcom/miui/server/smartpower/SmartThermalPolicyManager;
.super Ljava/lang/Object;
.source "SmartThermalPolicyManager.java"

# interfaces
.implements Lcom/miui/server/smartpower/SmartScenarioManager$ISmartScenarioCallback;


# instance fields
.field private mCallBack:Lmiui/smartpower/IScenarioCallback;

.field private mConfig:Lcom/miui/server/smartpower/SmartScenarioManager$ClientConfig;

.field private mSmartScenarioManager:Lcom/miui/server/smartpower/SmartScenarioManager;


# direct methods
.method public constructor <init>(Lcom/miui/server/smartpower/SmartScenarioManager;)V
    .locals 0
    .param p1, "smartScenarioManager"    # Lcom/miui/server/smartpower/SmartScenarioManager;

    .line 12
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 13
    iput-object p1, p0, Lcom/miui/server/smartpower/SmartThermalPolicyManager;->mSmartScenarioManager:Lcom/miui/server/smartpower/SmartScenarioManager;

    .line 14
    return-void
.end method

.method private init()V
    .locals 15

    .line 17
    move-object v0, p0

    iget-object v1, v0, Lcom/miui/server/smartpower/SmartThermalPolicyManager;->mSmartScenarioManager:Lcom/miui/server/smartpower/SmartScenarioManager;

    const-string/jumbo v2, "thermal"

    invoke-virtual {v1, v2, p0}, Lcom/miui/server/smartpower/SmartScenarioManager;->createClientConfig(Ljava/lang/String;Lcom/miui/server/smartpower/SmartScenarioManager$ISmartScenarioCallback;)Lcom/miui/server/smartpower/SmartScenarioManager$ClientConfig;

    move-result-object v1

    iput-object v1, v0, Lcom/miui/server/smartpower/SmartThermalPolicyManager;->mConfig:Lcom/miui/server/smartpower/SmartScenarioManager$ClientConfig;

    .line 19
    const-wide/16 v2, 0x2

    const-string v4, "com.tencent.mm"

    const/4 v5, 0x2

    invoke-virtual {v1, v2, v3, v4, v5}, Lcom/miui/server/smartpower/SmartScenarioManager$ClientConfig;->addMainScenarioIdConfig(JLjava/lang/String;I)V

    .line 21
    iget-object v1, v0, Lcom/miui/server/smartpower/SmartThermalPolicyManager;->mConfig:Lcom/miui/server/smartpower/SmartScenarioManager$ClientConfig;

    const-string v6, "com.sina.weibo"

    const-wide/16 v7, 0x4

    invoke-virtual {v1, v7, v8, v6, v5}, Lcom/miui/server/smartpower/SmartScenarioManager$ClientConfig;->addMainScenarioIdConfig(JLjava/lang/String;I)V

    .line 23
    iget-object v1, v0, Lcom/miui/server/smartpower/SmartThermalPolicyManager;->mConfig:Lcom/miui/server/smartpower/SmartScenarioManager$ClientConfig;

    const-string v6, "com.ss.android.ugc.aweme"

    const-wide/16 v9, 0x8

    invoke-virtual {v1, v9, v10, v6, v5}, Lcom/miui/server/smartpower/SmartScenarioManager$ClientConfig;->addMainScenarioIdConfig(JLjava/lang/String;I)V

    .line 25
    iget-object v1, v0, Lcom/miui/server/smartpower/SmartThermalPolicyManager;->mConfig:Lcom/miui/server/smartpower/SmartScenarioManager$ClientConfig;

    const-string v6, "com.smile.gifmaker"

    const-wide/16 v11, 0x10

    invoke-virtual {v1, v11, v12, v6, v5}, Lcom/miui/server/smartpower/SmartScenarioManager$ClientConfig;->addMainScenarioIdConfig(JLjava/lang/String;I)V

    .line 27
    iget-object v1, v0, Lcom/miui/server/smartpower/SmartThermalPolicyManager;->mConfig:Lcom/miui/server/smartpower/SmartScenarioManager$ClientConfig;

    const-string v6, "com.ss.android.article.news"

    const-wide/16 v13, 0x20

    invoke-virtual {v1, v13, v14, v6, v5}, Lcom/miui/server/smartpower/SmartScenarioManager$ClientConfig;->addMainScenarioIdConfig(JLjava/lang/String;I)V

    .line 29
    iget-object v1, v0, Lcom/miui/server/smartpower/SmartThermalPolicyManager;->mConfig:Lcom/miui/server/smartpower/SmartScenarioManager$ClientConfig;

    const-string v6, "com.taobao.taobao"

    const-wide/16 v13, 0x40

    invoke-virtual {v1, v13, v14, v6, v5}, Lcom/miui/server/smartpower/SmartScenarioManager$ClientConfig;->addMainScenarioIdConfig(JLjava/lang/String;I)V

    .line 31
    iget-object v1, v0, Lcom/miui/server/smartpower/SmartThermalPolicyManager;->mConfig:Lcom/miui/server/smartpower/SmartScenarioManager$ClientConfig;

    const-string/jumbo v6, "tv.danmaku.bili"

    const-wide/16 v13, 0x80

    invoke-virtual {v1, v13, v14, v6, v5}, Lcom/miui/server/smartpower/SmartScenarioManager$ClientConfig;->addMainScenarioIdConfig(JLjava/lang/String;I)V

    .line 33
    iget-object v1, v0, Lcom/miui/server/smartpower/SmartThermalPolicyManager;->mConfig:Lcom/miui/server/smartpower/SmartScenarioManager$ClientConfig;

    const-string v6, "com.autonavi.minimap"

    const-wide/16 v13, 0x100

    invoke-virtual {v1, v13, v14, v6, v5}, Lcom/miui/server/smartpower/SmartScenarioManager$ClientConfig;->addMainScenarioIdConfig(JLjava/lang/String;I)V

    .line 35
    iget-object v1, v0, Lcom/miui/server/smartpower/SmartThermalPolicyManager;->mConfig:Lcom/miui/server/smartpower/SmartScenarioManager$ClientConfig;

    const-wide/16 v13, 0x200

    const-string v6, "com.android.camera"

    invoke-virtual {v1, v13, v14, v6, v5}, Lcom/miui/server/smartpower/SmartScenarioManager$ClientConfig;->addMainScenarioIdConfig(JLjava/lang/String;I)V

    .line 38
    iget-object v1, v0, Lcom/miui/server/smartpower/SmartThermalPolicyManager;->mConfig:Lcom/miui/server/smartpower/SmartScenarioManager$ClientConfig;

    const-wide/32 v13, 0x100000

    const-string v6, "com.tencent.tmgp.sgame"

    invoke-virtual {v1, v13, v14, v6, v5}, Lcom/miui/server/smartpower/SmartScenarioManager$ClientConfig;->addMainScenarioIdConfig(JLjava/lang/String;I)V

    .line 40
    iget-object v1, v0, Lcom/miui/server/smartpower/SmartThermalPolicyManager;->mConfig:Lcom/miui/server/smartpower/SmartScenarioManager$ClientConfig;

    const-string v6, "com.miHoYo.Yuanshen"

    const-wide/32 v13, 0x200000

    invoke-virtual {v1, v13, v14, v6, v5}, Lcom/miui/server/smartpower/SmartScenarioManager$ClientConfig;->addMainScenarioIdConfig(JLjava/lang/String;I)V

    .line 44
    iget-object v1, v0, Lcom/miui/server/smartpower/SmartThermalPolicyManager;->mConfig:Lcom/miui/server/smartpower/SmartScenarioManager$ClientConfig;

    const/4 v5, 0x0

    const/16 v6, 0x40

    invoke-virtual {v1, v2, v3, v5, v6}, Lcom/miui/server/smartpower/SmartScenarioManager$ClientConfig;->addAdditionalScenarioIdConfig(JLjava/lang/String;I)V

    .line 46
    iget-object v1, v0, Lcom/miui/server/smartpower/SmartThermalPolicyManager;->mConfig:Lcom/miui/server/smartpower/SmartScenarioManager$ClientConfig;

    invoke-virtual {v1, v7, v8, v4, v6}, Lcom/miui/server/smartpower/SmartScenarioManager$ClientConfig;->addAdditionalScenarioIdConfig(JLjava/lang/String;I)V

    .line 48
    iget-object v1, v0, Lcom/miui/server/smartpower/SmartThermalPolicyManager;->mConfig:Lcom/miui/server/smartpower/SmartScenarioManager$ClientConfig;

    const-string v2, "com.tencent.mobileqq"

    invoke-virtual {v1, v9, v10, v2, v6}, Lcom/miui/server/smartpower/SmartScenarioManager$ClientConfig;->addAdditionalScenarioIdConfig(JLjava/lang/String;I)V

    .line 50
    iget-object v1, v0, Lcom/miui/server/smartpower/SmartThermalPolicyManager;->mConfig:Lcom/miui/server/smartpower/SmartScenarioManager$ClientConfig;

    const-string v3, "com.ss.android.lark.kami"

    invoke-virtual {v1, v11, v12, v3, v6}, Lcom/miui/server/smartpower/SmartScenarioManager$ClientConfig;->addAdditionalScenarioIdConfig(JLjava/lang/String;I)V

    .line 52
    iget-object v1, v0, Lcom/miui/server/smartpower/SmartThermalPolicyManager;->mConfig:Lcom/miui/server/smartpower/SmartScenarioManager$ClientConfig;

    const-string v7, "com.alibaba.android.rimet"

    const-wide/16 v8, 0x20

    invoke-virtual {v1, v8, v9, v7, v6}, Lcom/miui/server/smartpower/SmartScenarioManager$ClientConfig;->addAdditionalScenarioIdConfig(JLjava/lang/String;I)V

    .line 54
    iget-object v1, v0, Lcom/miui/server/smartpower/SmartThermalPolicyManager;->mConfig:Lcom/miui/server/smartpower/SmartScenarioManager$ClientConfig;

    const-string v8, "com.tencent.wework"

    const-wide/16 v9, 0x40

    invoke-virtual {v1, v9, v10, v8, v6}, Lcom/miui/server/smartpower/SmartScenarioManager$ClientConfig;->addAdditionalScenarioIdConfig(JLjava/lang/String;I)V

    .line 56
    iget-object v1, v0, Lcom/miui/server/smartpower/SmartThermalPolicyManager;->mConfig:Lcom/miui/server/smartpower/SmartScenarioManager$ClientConfig;

    const-string v9, "com.whatsapp"

    const-wide/16 v10, 0x80

    invoke-virtual {v1, v10, v11, v9, v6}, Lcom/miui/server/smartpower/SmartScenarioManager$ClientConfig;->addAdditionalScenarioIdConfig(JLjava/lang/String;I)V

    .line 58
    iget-object v1, v0, Lcom/miui/server/smartpower/SmartThermalPolicyManager;->mConfig:Lcom/miui/server/smartpower/SmartScenarioManager$ClientConfig;

    const-string v10, "com.ss.android.lark"

    const-wide/16 v11, 0x100

    invoke-virtual {v1, v11, v12, v10, v6}, Lcom/miui/server/smartpower/SmartScenarioManager$ClientConfig;->addAdditionalScenarioIdConfig(JLjava/lang/String;I)V

    .line 61
    iget-object v1, v0, Lcom/miui/server/smartpower/SmartThermalPolicyManager;->mConfig:Lcom/miui/server/smartpower/SmartScenarioManager$ClientConfig;

    const-wide/16 v11, 0x800

    const/16 v6, 0x80

    invoke-virtual {v1, v11, v12, v5, v6}, Lcom/miui/server/smartpower/SmartScenarioManager$ClientConfig;->addAdditionalScenarioIdConfig(JLjava/lang/String;I)V

    .line 63
    iget-object v1, v0, Lcom/miui/server/smartpower/SmartThermalPolicyManager;->mConfig:Lcom/miui/server/smartpower/SmartScenarioManager$ClientConfig;

    const-wide/16 v11, 0x1000

    invoke-virtual {v1, v11, v12, v4, v6}, Lcom/miui/server/smartpower/SmartScenarioManager$ClientConfig;->addAdditionalScenarioIdConfig(JLjava/lang/String;I)V

    .line 65
    iget-object v1, v0, Lcom/miui/server/smartpower/SmartThermalPolicyManager;->mConfig:Lcom/miui/server/smartpower/SmartScenarioManager$ClientConfig;

    const-wide/16 v11, 0x2000

    invoke-virtual {v1, v11, v12, v2, v6}, Lcom/miui/server/smartpower/SmartScenarioManager$ClientConfig;->addAdditionalScenarioIdConfig(JLjava/lang/String;I)V

    .line 67
    iget-object v1, v0, Lcom/miui/server/smartpower/SmartThermalPolicyManager;->mConfig:Lcom/miui/server/smartpower/SmartScenarioManager$ClientConfig;

    const-wide/16 v11, 0x4000

    invoke-virtual {v1, v11, v12, v3, v6}, Lcom/miui/server/smartpower/SmartScenarioManager$ClientConfig;->addAdditionalScenarioIdConfig(JLjava/lang/String;I)V

    .line 69
    iget-object v1, v0, Lcom/miui/server/smartpower/SmartThermalPolicyManager;->mConfig:Lcom/miui/server/smartpower/SmartScenarioManager$ClientConfig;

    const-wide/32 v11, 0x8000

    invoke-virtual {v1, v11, v12, v7, v6}, Lcom/miui/server/smartpower/SmartScenarioManager$ClientConfig;->addAdditionalScenarioIdConfig(JLjava/lang/String;I)V

    .line 71
    iget-object v1, v0, Lcom/miui/server/smartpower/SmartThermalPolicyManager;->mConfig:Lcom/miui/server/smartpower/SmartScenarioManager$ClientConfig;

    const-wide/32 v11, 0x10000

    invoke-virtual {v1, v11, v12, v8, v6}, Lcom/miui/server/smartpower/SmartScenarioManager$ClientConfig;->addAdditionalScenarioIdConfig(JLjava/lang/String;I)V

    .line 73
    iget-object v1, v0, Lcom/miui/server/smartpower/SmartThermalPolicyManager;->mConfig:Lcom/miui/server/smartpower/SmartScenarioManager$ClientConfig;

    const-wide/32 v11, 0x20000

    invoke-virtual {v1, v11, v12, v9, v6}, Lcom/miui/server/smartpower/SmartScenarioManager$ClientConfig;->addAdditionalScenarioIdConfig(JLjava/lang/String;I)V

    .line 75
    iget-object v1, v0, Lcom/miui/server/smartpower/SmartThermalPolicyManager;->mConfig:Lcom/miui/server/smartpower/SmartScenarioManager$ClientConfig;

    const-wide/32 v8, 0x40000

    invoke-virtual {v1, v8, v9, v10, v6}, Lcom/miui/server/smartpower/SmartScenarioManager$ClientConfig;->addAdditionalScenarioIdConfig(JLjava/lang/String;I)V

    .line 78
    iget-object v1, v0, Lcom/miui/server/smartpower/SmartThermalPolicyManager;->mConfig:Lcom/miui/server/smartpower/SmartScenarioManager$ClientConfig;

    const/16 v2, 0x100

    invoke-virtual {v1, v13, v14, v5, v2}, Lcom/miui/server/smartpower/SmartScenarioManager$ClientConfig;->addAdditionalScenarioIdConfig(JLjava/lang/String;I)V

    .line 80
    iget-object v1, v0, Lcom/miui/server/smartpower/SmartThermalPolicyManager;->mConfig:Lcom/miui/server/smartpower/SmartScenarioManager$ClientConfig;

    const-wide/32 v8, 0x400000

    const-string v4, "com.xiaomi.market"

    invoke-virtual {v1, v8, v9, v4, v2}, Lcom/miui/server/smartpower/SmartScenarioManager$ClientConfig;->addAdditionalScenarioIdConfig(JLjava/lang/String;I)V

    .line 82
    iget-object v1, v0, Lcom/miui/server/smartpower/SmartThermalPolicyManager;->mConfig:Lcom/miui/server/smartpower/SmartScenarioManager$ClientConfig;

    const-wide/32 v8, 0x800000

    const-string v4, "com.xunlei.downloadprovider"

    invoke-virtual {v1, v8, v9, v4, v2}, Lcom/miui/server/smartpower/SmartScenarioManager$ClientConfig;->addAdditionalScenarioIdConfig(JLjava/lang/String;I)V

    .line 85
    iget-object v1, v0, Lcom/miui/server/smartpower/SmartThermalPolicyManager;->mConfig:Lcom/miui/server/smartpower/SmartScenarioManager$ClientConfig;

    const-wide v8, 0x80000000L

    const/16 v2, 0x10

    invoke-virtual {v1, v8, v9, v5, v2}, Lcom/miui/server/smartpower/SmartScenarioManager$ClientConfig;->addAdditionalScenarioIdConfig(JLjava/lang/String;I)V

    .line 88
    iget-object v1, v0, Lcom/miui/server/smartpower/SmartThermalPolicyManager;->mConfig:Lcom/miui/server/smartpower/SmartScenarioManager$ClientConfig;

    const-wide v8, 0x1000000000L

    const/16 v2, 0x20

    invoke-virtual {v1, v8, v9, v5, v2}, Lcom/miui/server/smartpower/SmartScenarioManager$ClientConfig;->addAdditionalScenarioIdConfig(JLjava/lang/String;I)V

    .line 91
    iget-object v1, v0, Lcom/miui/server/smartpower/SmartThermalPolicyManager;->mConfig:Lcom/miui/server/smartpower/SmartScenarioManager$ClientConfig;

    const-wide v8, 0x20000000000L

    const/16 v2, 0x8

    invoke-virtual {v1, v8, v9, v3, v2}, Lcom/miui/server/smartpower/SmartScenarioManager$ClientConfig;->addAdditionalScenarioIdConfig(JLjava/lang/String;I)V

    .line 93
    iget-object v1, v0, Lcom/miui/server/smartpower/SmartThermalPolicyManager;->mConfig:Lcom/miui/server/smartpower/SmartScenarioManager$ClientConfig;

    const-wide v3, 0x40000000000L

    invoke-virtual {v1, v3, v4, v7, v2}, Lcom/miui/server/smartpower/SmartScenarioManager$ClientConfig;->addAdditionalScenarioIdConfig(JLjava/lang/String;I)V

    .line 96
    iget-object v1, v0, Lcom/miui/server/smartpower/SmartThermalPolicyManager;->mConfig:Lcom/miui/server/smartpower/SmartScenarioManager$ClientConfig;

    const-wide v2, 0x800000000000L

    const/high16 v4, 0x10000

    invoke-virtual {v1, v2, v3, v5, v4}, Lcom/miui/server/smartpower/SmartScenarioManager$ClientConfig;->addAdditionalScenarioIdConfig(JLjava/lang/String;I)V

    .line 99
    iget-object v1, v0, Lcom/miui/server/smartpower/SmartThermalPolicyManager;->mConfig:Lcom/miui/server/smartpower/SmartScenarioManager$ClientConfig;

    const-wide/high16 v2, 0x1000000000000L

    const v4, 0x8000

    invoke-virtual {v1, v2, v3, v5, v4}, Lcom/miui/server/smartpower/SmartScenarioManager$ClientConfig;->addAdditionalScenarioIdConfig(JLjava/lang/String;I)V

    .line 102
    iget-object v1, v0, Lcom/miui/server/smartpower/SmartThermalPolicyManager;->mConfig:Lcom/miui/server/smartpower/SmartScenarioManager$ClientConfig;

    const-wide/high16 v2, 0x2000000000000L

    const/16 v4, 0x2000

    invoke-virtual {v1, v2, v3, v5, v4}, Lcom/miui/server/smartpower/SmartScenarioManager$ClientConfig;->addAdditionalScenarioIdConfig(JLjava/lang/String;I)V

    .line 105
    iget-object v1, v0, Lcom/miui/server/smartpower/SmartThermalPolicyManager;->mConfig:Lcom/miui/server/smartpower/SmartScenarioManager$ClientConfig;

    const-wide/high16 v2, 0x4000000000000L

    const/16 v4, 0x800

    invoke-virtual {v1, v2, v3, v5, v4}, Lcom/miui/server/smartpower/SmartScenarioManager$ClientConfig;->addAdditionalScenarioIdConfig(JLjava/lang/String;I)V

    .line 108
    iget-object v1, v0, Lcom/miui/server/smartpower/SmartThermalPolicyManager;->mConfig:Lcom/miui/server/smartpower/SmartScenarioManager$ClientConfig;

    const-wide/high16 v2, 0x8000000000000L

    const/16 v4, 0x200

    invoke-virtual {v1, v2, v3, v5, v4}, Lcom/miui/server/smartpower/SmartScenarioManager$ClientConfig;->addAdditionalScenarioIdConfig(JLjava/lang/String;I)V

    .line 111
    iget-object v1, v0, Lcom/miui/server/smartpower/SmartThermalPolicyManager;->mConfig:Lcom/miui/server/smartpower/SmartScenarioManager$ClientConfig;

    const-wide/high16 v2, 0x100000000000000L

    const/16 v4, 0x400

    invoke-virtual {v1, v2, v3, v5, v4}, Lcom/miui/server/smartpower/SmartScenarioManager$ClientConfig;->addAdditionalScenarioIdConfig(JLjava/lang/String;I)V

    .line 114
    iget-object v1, v0, Lcom/miui/server/smartpower/SmartThermalPolicyManager;->mConfig:Lcom/miui/server/smartpower/SmartScenarioManager$ClientConfig;

    const-wide/high16 v2, 0x2000000000000000L

    const/16 v4, 0x1000

    invoke-virtual {v1, v2, v3, v5, v4}, Lcom/miui/server/smartpower/SmartScenarioManager$ClientConfig;->addAdditionalScenarioIdConfig(JLjava/lang/String;I)V

    .line 117
    iget-object v1, v0, Lcom/miui/server/smartpower/SmartThermalPolicyManager;->mSmartScenarioManager:Lcom/miui/server/smartpower/SmartScenarioManager;

    iget-object v2, v0, Lcom/miui/server/smartpower/SmartThermalPolicyManager;->mConfig:Lcom/miui/server/smartpower/SmartScenarioManager$ClientConfig;

    invoke-virtual {v1, v2}, Lcom/miui/server/smartpower/SmartScenarioManager;->registClientConfig(Lcom/miui/server/smartpower/SmartScenarioManager$ClientConfig;)V

    .line 118
    return-void
.end method


# virtual methods
.method public onCurrentScenarioChanged(JJ)V
    .locals 1
    .param p1, "mainSenarioId"    # J
    .param p3, "additionalSenarioId"    # J

    .line 127
    iget-object v0, p0, Lcom/miui/server/smartpower/SmartThermalPolicyManager;->mCallBack:Lmiui/smartpower/IScenarioCallback;

    if-eqz v0, :cond_0

    .line 129
    :try_start_0
    invoke-interface {v0, p1, p2, p3, p4}, Lmiui/smartpower/IScenarioCallback;->onCurrentScenarioChanged(JJ)V
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    .line 130
    :catch_0
    move-exception v0

    :goto_0
    nop

    .line 132
    :cond_0
    return-void
.end method

.method public registThermalScenarioCallback(Lmiui/smartpower/IScenarioCallback;)V
    .locals 0
    .param p1, "callback"    # Lmiui/smartpower/IScenarioCallback;

    .line 121
    iput-object p1, p0, Lcom/miui/server/smartpower/SmartThermalPolicyManager;->mCallBack:Lmiui/smartpower/IScenarioCallback;

    .line 122
    invoke-direct {p0}, Lcom/miui/server/smartpower/SmartThermalPolicyManager;->init()V

    .line 123
    return-void
.end method
