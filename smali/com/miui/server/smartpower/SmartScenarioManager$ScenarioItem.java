class com.miui.server.smartpower.SmartScenarioManager$ScenarioItem {
	 /* .source "SmartScenarioManager.java" */
	 /* # annotations */
	 /* .annotation system Ldalvik/annotation/EnclosingClass; */
	 /* value = Lcom/miui/server/smartpower/SmartScenarioManager; */
} // .end annotation
/* .annotation system Ldalvik/annotation/InnerClass; */
/* accessFlags = 0x2 */
/* name = "ScenarioItem" */
} // .end annotation
/* # instance fields */
private Integer mAction;
private final java.util.ArrayList mInteractiveApps;
/* .annotation system Ldalvik/annotation/Signature; */
/* value = { */
/* "Ljava/util/ArrayList<", */
/* "Lcom/miui/server/smartpower/SmartScenarioManager$ScenarioAppChild;", */
/* ">;" */
/* } */
} // .end annotation
} // .end field
final com.miui.server.smartpower.SmartScenarioManager this$0; //synthetic
/* # direct methods */
public static void $r8$lambda$4RW5aKrS5LtWGq1m1VF_V2aaOOw ( com.miui.server.smartpower.SmartScenarioManager$ScenarioItem p0, com.android.server.am.AppStateManager$AppState p1, Boolean p2, com.miui.server.smartpower.SmartScenarioManager$ClientConfig p3 ) { //synthethic
/* .locals 0 */
/* invoke-direct {p0, p1, p2, p3}, Lcom/miui/server/smartpower/SmartScenarioManager$ScenarioItem;->lambda$onAppActionChanged$1(Lcom/android/server/am/AppStateManager$AppState;ZLcom/miui/server/smartpower/SmartScenarioManager$ClientConfig;)V */
return;
} // .end method
static java.util.ArrayList -$$Nest$fgetmInteractiveApps ( com.miui.server.smartpower.SmartScenarioManager$ScenarioItem p0 ) { //bridge//synthethic
/* .locals 0 */
p0 = this.mInteractiveApps;
} // .end method
private com.miui.server.smartpower.SmartScenarioManager$ScenarioItem ( ) {
/* .locals 0 */
/* .param p2, "action" # I */
/* .line 258 */
this.this$0 = p1;
/* invoke-direct {p0}, Ljava/lang/Object;-><init>()V */
/* .line 255 */
/* new-instance p1, Ljava/util/ArrayList; */
/* invoke-direct {p1}, Ljava/util/ArrayList;-><init>()V */
this.mInteractiveApps = p1;
/* .line 259 */
/* iput p2, p0, Lcom/miui/server/smartpower/SmartScenarioManager$ScenarioItem;->mAction:I */
/* .line 260 */
return;
} // .end method
 com.miui.server.smartpower.SmartScenarioManager$ScenarioItem ( ) { //synthethic
/* .locals 0 */
/* invoke-direct {p0, p1, p2}, Lcom/miui/server/smartpower/SmartScenarioManager$ScenarioItem;-><init>(Lcom/miui/server/smartpower/SmartScenarioManager;I)V */
return;
} // .end method
static Boolean lambda$onAppActionChanged$0 ( com.android.server.am.AppStateManager$AppState p0, com.miui.server.smartpower.SmartScenarioManager$ScenarioAppChild p1 ) { //synthethic
/* .locals 1 */
/* .param p0, "app" # Lcom/android/server/am/AppStateManager$AppState; */
/* .param p1, "item" # Lcom/miui/server/smartpower/SmartScenarioManager$ScenarioAppChild; */
/* .line 290 */
com.miui.server.smartpower.SmartScenarioManager$ScenarioAppChild .-$$Nest$fgetmApp ( p1 );
/* if-ne v0, p0, :cond_0 */
int v0 = 1; // const/4 v0, 0x1
} // :cond_0
int v0 = 0; // const/4 v0, 0x0
} // :goto_0
} // .end method
private void lambda$onAppActionChanged$1 ( com.android.server.am.AppStateManager$AppState p0, Boolean p1, com.miui.server.smartpower.SmartScenarioManager$ClientConfig p2 ) { //synthethic
/* .locals 2 */
/* .param p1, "app" # Lcom/android/server/am/AppStateManager$AppState; */
/* .param p2, "add" # Z */
/* .param p3, "v" # Lcom/miui/server/smartpower/SmartScenarioManager$ClientConfig; */
/* .line 297 */
/* if-nez p1, :cond_0 */
int v0 = 0; // const/4 v0, 0x0
} // :cond_0
(( com.android.server.am.AppStateManager$AppState ) p1 ).getPackageName ( ); // invoke-virtual {p1}, Lcom/android/server/am/AppStateManager$AppState;->getPackageName()Ljava/lang/String;
/* .line 298 */
/* .local v0, "packageName":Ljava/lang/String; */
} // :goto_0
/* iget v1, p0, Lcom/miui/server/smartpower/SmartScenarioManager$ScenarioItem;->mAction:I */
(( com.miui.server.smartpower.SmartScenarioManager$ClientConfig ) p3 ).onAppActionChanged ( v1, v0, p2 ); // invoke-virtual {p3, v1, v0, p2}, Lcom/miui/server/smartpower/SmartScenarioManager$ClientConfig;->onAppActionChanged(ILjava/lang/String;Z)V
/* .line 299 */
return;
} // .end method
/* # virtual methods */
public Integer getAction ( ) {
/* .locals 1 */
/* .line 263 */
/* iget v0, p0, Lcom/miui/server/smartpower/SmartScenarioManager$ScenarioItem;->mAction:I */
} // .end method
public void onAppActionChanged ( com.android.server.am.AppStateManager$AppState p0, Boolean p1 ) {
/* .locals 7 */
/* .param p1, "app" # Lcom/android/server/am/AppStateManager$AppState; */
/* .param p2, "add" # Z */
/* .line 276 */
int v0 = 0; // const/4 v0, 0x0
/* .line 277 */
/* .local v0, "changed":Z */
v1 = this.mInteractiveApps;
/* monitor-enter v1 */
/* .line 278 */
if ( p2 != null) { // if-eqz p2, :cond_2
/* .line 279 */
int v2 = 0; // const/4 v2, 0x0
/* .line 280 */
/* .local v2, "contains":Z */
try { // :try_start_0
v3 = this.mInteractiveApps;
(( java.util.ArrayList ) v3 ).iterator ( ); // invoke-virtual {v3}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;
v4 = } // :goto_0
if ( v4 != null) { // if-eqz v4, :cond_1
/* check-cast v4, Lcom/miui/server/smartpower/SmartScenarioManager$ScenarioAppChild; */
/* .line 281 */
/* .local v4, "child":Lcom/miui/server/smartpower/SmartScenarioManager$ScenarioAppChild; */
com.miui.server.smartpower.SmartScenarioManager$ScenarioAppChild .-$$Nest$fgetmApp ( v4 );
/* if-ne v5, p1, :cond_0 */
/* .line 282 */
int v2 = 1; // const/4 v2, 0x1
/* .line 283 */
/* .line 285 */
} // .end local v4 # "child":Lcom/miui/server/smartpower/SmartScenarioManager$ScenarioAppChild;
} // :cond_0
/* .line 286 */
} // :cond_1
} // :goto_1
/* if-nez v2, :cond_3 */
/* .line 287 */
v3 = this.mInteractiveApps;
/* new-instance v4, Lcom/miui/server/smartpower/SmartScenarioManager$ScenarioAppChild; */
v5 = this.this$0;
int v6 = 0; // const/4 v6, 0x0
/* invoke-direct {v4, v5, p1, v6}, Lcom/miui/server/smartpower/SmartScenarioManager$ScenarioAppChild;-><init>(Lcom/miui/server/smartpower/SmartScenarioManager;Lcom/android/server/am/AppStateManager$AppState;Lcom/miui/server/smartpower/SmartScenarioManager$ScenarioAppChild-IA;)V */
(( java.util.ArrayList ) v3 ).add ( v4 ); // invoke-virtual {v3, v4}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z
/* .line 288 */
int v0 = 1; // const/4 v0, 0x1
/* .line 290 */
} // .end local v2 # "contains":Z
} // :cond_2
v2 = this.mInteractiveApps;
/* new-instance v3, Lcom/miui/server/smartpower/SmartScenarioManager$ScenarioItem$$ExternalSyntheticLambda0; */
/* invoke-direct {v3, p1}, Lcom/miui/server/smartpower/SmartScenarioManager$ScenarioItem$$ExternalSyntheticLambda0;-><init>(Lcom/android/server/am/AppStateManager$AppState;)V */
v2 = (( java.util.ArrayList ) v2 ).removeIf ( v3 ); // invoke-virtual {v2, v3}, Ljava/util/ArrayList;->removeIf(Ljava/util/function/Predicate;)Z
if ( v2 != null) { // if-eqz v2, :cond_3
/* .line 291 */
int v0 = 1; // const/4 v0, 0x1
/* .line 290 */
} // :cond_3
} // :goto_2
/* nop */
/* .line 293 */
} // :goto_3
/* monitor-exit v1 */
/* :try_end_0 */
/* .catchall {:try_start_0 ..:try_end_0} :catchall_1 */
/* .line 294 */
if ( v0 != null) { // if-eqz v0, :cond_4
/* .line 295 */
v1 = this.this$0;
com.miui.server.smartpower.SmartScenarioManager .-$$Nest$fgetmClients ( v1 );
/* monitor-enter v1 */
/* .line 296 */
try { // :try_start_1
v2 = this.this$0;
com.miui.server.smartpower.SmartScenarioManager .-$$Nest$fgetmClients ( v2 );
/* new-instance v3, Lcom/miui/server/smartpower/SmartScenarioManager$ScenarioItem$$ExternalSyntheticLambda1; */
/* invoke-direct {v3, p0, p1, p2}, Lcom/miui/server/smartpower/SmartScenarioManager$ScenarioItem$$ExternalSyntheticLambda1;-><init>(Lcom/miui/server/smartpower/SmartScenarioManager$ScenarioItem;Lcom/android/server/am/AppStateManager$AppState;Z)V */
(( android.util.ArraySet ) v2 ).forEach ( v3 ); // invoke-virtual {v2, v3}, Landroid/util/ArraySet;->forEach(Ljava/util/function/Consumer;)V
/* .line 300 */
/* monitor-exit v1 */
/* :catchall_0 */
/* move-exception v2 */
/* monitor-exit v1 */
/* :try_end_1 */
/* .catchall {:try_start_1 ..:try_end_1} :catchall_0 */
/* throw v2 */
/* .line 302 */
} // :cond_4
} // :goto_4
return;
/* .line 293 */
/* :catchall_1 */
/* move-exception v2 */
try { // :try_start_2
/* monitor-exit v1 */
/* :try_end_2 */
/* .catchall {:try_start_2 ..:try_end_2} :catchall_1 */
/* throw v2 */
} // .end method
public void parseCurrentScenarioId ( com.miui.server.smartpower.SmartScenarioManager$ClientConfig p0 ) {
/* .locals 6 */
/* .param p1, "client" # Lcom/miui/server/smartpower/SmartScenarioManager$ClientConfig; */
/* .line 267 */
/* iget v0, p0, Lcom/miui/server/smartpower/SmartScenarioManager$ScenarioItem;->mAction:I */
int v1 = 0; // const/4 v1, 0x0
int v2 = 1; // const/4 v2, 0x1
(( com.miui.server.smartpower.SmartScenarioManager$ClientConfig ) p1 ).onAppActionChanged ( v0, v1, v2 ); // invoke-virtual {p1, v0, v1, v2}, Lcom/miui/server/smartpower/SmartScenarioManager$ClientConfig;->onAppActionChanged(ILjava/lang/String;Z)V
/* .line 268 */
v0 = this.mInteractiveApps;
/* monitor-enter v0 */
/* .line 269 */
try { // :try_start_0
v1 = this.mInteractiveApps;
(( java.util.ArrayList ) v1 ).iterator ( ); // invoke-virtual {v1}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;
v3 = } // :goto_0
if ( v3 != null) { // if-eqz v3, :cond_0
/* check-cast v3, Lcom/miui/server/smartpower/SmartScenarioManager$ScenarioAppChild; */
/* .line 270 */
/* .local v3, "app":Lcom/miui/server/smartpower/SmartScenarioManager$ScenarioAppChild; */
/* iget v4, p0, Lcom/miui/server/smartpower/SmartScenarioManager$ScenarioItem;->mAction:I */
(( com.miui.server.smartpower.SmartScenarioManager$ScenarioAppChild ) v3 ).getPackageName ( ); // invoke-virtual {v3}, Lcom/miui/server/smartpower/SmartScenarioManager$ScenarioAppChild;->getPackageName()Ljava/lang/String;
(( com.miui.server.smartpower.SmartScenarioManager$ClientConfig ) p1 ).onAppActionChanged ( v4, v5, v2 ); // invoke-virtual {p1, v4, v5, v2}, Lcom/miui/server/smartpower/SmartScenarioManager$ClientConfig;->onAppActionChanged(ILjava/lang/String;Z)V
/* .line 271 */
} // .end local v3 # "app":Lcom/miui/server/smartpower/SmartScenarioManager$ScenarioAppChild;
/* .line 272 */
} // :cond_0
/* monitor-exit v0 */
/* .line 273 */
return;
/* .line 272 */
/* :catchall_0 */
/* move-exception v1 */
/* monitor-exit v0 */
/* :try_end_0 */
/* .catchall {:try_start_0 ..:try_end_0} :catchall_0 */
/* throw v1 */
} // .end method
public java.lang.String toString ( ) {
/* .locals 2 */
/* .line 306 */
/* new-instance v0, Ljava/lang/StringBuilder; */
/* invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V */
/* iget v1, p0, Lcom/miui/server/smartpower/SmartScenarioManager$ScenarioItem;->mAction:I */
com.miui.server.smartpower.SmartScenarioManager .actionTypeToString ( v1 );
(( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
final String v1 = " : "; // const-string v1, " : "
(( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
v1 = this.mInteractiveApps;
(( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v0 ).toString ( ); // invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
} // .end method
