.class public abstract Lcom/miui/server/smartpower/AppPowerResource;
.super Ljava/lang/Object;
.source "AppPowerResource.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/miui/server/smartpower/AppPowerResource$IAppPowerResourceCallback;
    }
.end annotation


# static fields
.field protected static final DEBUG:Z

.field protected static final TAG:Ljava/lang/String; = "SmartPower.AppResource"


# instance fields
.field final mResourceCallbacksByPid:Landroid/util/SparseArray;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/util/SparseArray<",
            "Ljava/util/List<",
            "Lcom/miui/server/smartpower/AppPowerResource$IAppPowerResourceCallback;",
            ">;>;"
        }
    .end annotation
.end field

.field final mResourceCallbacksByUid:Landroid/util/SparseArray;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/util/SparseArray<",
            "Ljava/util/List<",
            "Lcom/miui/server/smartpower/AppPowerResource$IAppPowerResourceCallback;",
            ">;>;"
        }
    .end annotation
.end field

.field public mType:I


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .line 11
    sget-boolean v0, Lcom/miui/server/smartpower/AppPowerResourceManager;->DEBUG:Z

    sput-boolean v0, Lcom/miui/server/smartpower/AppPowerResource;->DEBUG:Z

    return-void
.end method

.method public constructor <init>()V
    .locals 1

    .line 9
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 14
    new-instance v0, Landroid/util/SparseArray;

    invoke-direct {v0}, Landroid/util/SparseArray;-><init>()V

    iput-object v0, p0, Lcom/miui/server/smartpower/AppPowerResource;->mResourceCallbacksByUid:Landroid/util/SparseArray;

    .line 16
    new-instance v0, Landroid/util/SparseArray;

    invoke-direct {v0}, Landroid/util/SparseArray;-><init>()V

    iput-object v0, p0, Lcom/miui/server/smartpower/AppPowerResource;->mResourceCallbacksByPid:Landroid/util/SparseArray;

    return-void
.end method


# virtual methods
.method public abstract getActiveUids()Ljava/util/ArrayList;
.end method

.method public init()V
    .locals 0

    .line 29
    return-void
.end method

.method public abstract isAppResourceActive(I)Z
.end method

.method public abstract isAppResourceActive(II)Z
.end method

.method public registerCallback(Lcom/miui/server/smartpower/AppPowerResource$IAppPowerResourceCallback;I)V
    .locals 3
    .param p1, "callback"    # Lcom/miui/server/smartpower/AppPowerResource$IAppPowerResourceCallback;
    .param p2, "uid"    # I

    .line 32
    iget-object v0, p0, Lcom/miui/server/smartpower/AppPowerResource;->mResourceCallbacksByUid:Landroid/util/SparseArray;

    monitor-enter v0

    .line 33
    :try_start_0
    iget-object v1, p0, Lcom/miui/server/smartpower/AppPowerResource;->mResourceCallbacksByUid:Landroid/util/SparseArray;

    invoke-virtual {v1, p2}, Landroid/util/SparseArray;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/util/List;

    .line 34
    .local v1, "callbacks":Ljava/util/List;, "Ljava/util/List<Lcom/miui/server/smartpower/AppPowerResource$IAppPowerResourceCallback;>;"
    if-nez v1, :cond_0

    .line 35
    new-instance v2, Ljava/util/ArrayList;

    invoke-direct {v2}, Ljava/util/ArrayList;-><init>()V

    move-object v1, v2

    .line 36
    iget-object v2, p0, Lcom/miui/server/smartpower/AppPowerResource;->mResourceCallbacksByUid:Landroid/util/SparseArray;

    invoke-virtual {v2, p2, v1}, Landroid/util/SparseArray;->put(ILjava/lang/Object;)V

    .line 38
    :cond_0
    invoke-interface {v1, p1}, Ljava/util/List;->contains(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_1

    .line 39
    invoke-interface {v1, p1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 41
    .end local v1    # "callbacks":Ljava/util/List;, "Ljava/util/List<Lcom/miui/server/smartpower/AppPowerResource$IAppPowerResourceCallback;>;"
    :cond_1
    monitor-exit v0

    .line 42
    return-void

    .line 41
    :catchall_0
    move-exception v1

    monitor-exit v0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v1
.end method

.method public registerCallback(Lcom/miui/server/smartpower/AppPowerResource$IAppPowerResourceCallback;II)V
    .locals 3
    .param p1, "callback"    # Lcom/miui/server/smartpower/AppPowerResource$IAppPowerResourceCallback;
    .param p2, "uid"    # I
    .param p3, "pid"    # I

    .line 72
    iget-object v0, p0, Lcom/miui/server/smartpower/AppPowerResource;->mResourceCallbacksByPid:Landroid/util/SparseArray;

    monitor-enter v0

    .line 73
    :try_start_0
    iget-object v1, p0, Lcom/miui/server/smartpower/AppPowerResource;->mResourceCallbacksByPid:Landroid/util/SparseArray;

    invoke-virtual {v1, p3}, Landroid/util/SparseArray;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/util/List;

    .line 74
    .local v1, "callbacks":Ljava/util/List;, "Ljava/util/List<Lcom/miui/server/smartpower/AppPowerResource$IAppPowerResourceCallback;>;"
    if-nez v1, :cond_0

    .line 75
    new-instance v2, Ljava/util/ArrayList;

    invoke-direct {v2}, Ljava/util/ArrayList;-><init>()V

    move-object v1, v2

    .line 76
    iget-object v2, p0, Lcom/miui/server/smartpower/AppPowerResource;->mResourceCallbacksByPid:Landroid/util/SparseArray;

    invoke-virtual {v2, p3, v1}, Landroid/util/SparseArray;->put(ILjava/lang/Object;)V

    .line 78
    :cond_0
    invoke-interface {v1, p1}, Ljava/util/List;->contains(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_1

    .line 79
    invoke-interface {v1, p1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 81
    .end local v1    # "callbacks":Ljava/util/List;, "Ljava/util/List<Lcom/miui/server/smartpower/AppPowerResource$IAppPowerResourceCallback;>;"
    :cond_1
    monitor-exit v0

    .line 82
    return-void

    .line 81
    :catchall_0
    move-exception v1

    monitor-exit v0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v1
.end method

.method public abstract releaseAppPowerResource(I)V
.end method

.method reportResourceStatus(IIZI)V
    .locals 5
    .param p1, "uid"    # I
    .param p2, "pid"    # I
    .param p3, "active"    # Z
    .param p4, "behavier"    # I

    .line 97
    iget-object v0, p0, Lcom/miui/server/smartpower/AppPowerResource;->mResourceCallbacksByPid:Landroid/util/SparseArray;

    monitor-enter v0

    .line 98
    :try_start_0
    iget-object v1, p0, Lcom/miui/server/smartpower/AppPowerResource;->mResourceCallbacksByPid:Landroid/util/SparseArray;

    invoke-virtual {v1, p2}, Landroid/util/SparseArray;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/util/List;

    .line 99
    .local v1, "callbacks":Ljava/util/List;, "Ljava/util/List<Lcom/miui/server/smartpower/AppPowerResource$IAppPowerResourceCallback;>;"
    if-eqz v1, :cond_1

    .line 100
    invoke-interface {v1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_1

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/miui/server/smartpower/AppPowerResource$IAppPowerResourceCallback;

    .line 101
    .local v3, "callback":Lcom/miui/server/smartpower/AppPowerResource$IAppPowerResourceCallback;
    if-eqz p3, :cond_0

    .line 102
    iget v4, p0, Lcom/miui/server/smartpower/AppPowerResource;->mType:I

    invoke-interface {v3, v4, p4}, Lcom/miui/server/smartpower/AppPowerResource$IAppPowerResourceCallback;->noteResourceActive(II)V

    goto :goto_1

    .line 104
    :cond_0
    iget v4, p0, Lcom/miui/server/smartpower/AppPowerResource;->mType:I

    invoke-interface {v3, v4, p4}, Lcom/miui/server/smartpower/AppPowerResource$IAppPowerResourceCallback;->noteResourceInactive(II)V

    .line 106
    .end local v3    # "callback":Lcom/miui/server/smartpower/AppPowerResource$IAppPowerResourceCallback;
    :goto_1
    goto :goto_0

    .line 108
    .end local v1    # "callbacks":Ljava/util/List;, "Ljava/util/List<Lcom/miui/server/smartpower/AppPowerResource$IAppPowerResourceCallback;>;"
    :cond_1
    monitor-exit v0

    .line 109
    return-void

    .line 108
    :catchall_0
    move-exception v1

    monitor-exit v0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v1
.end method

.method reportResourceStatus(IZI)V
    .locals 5
    .param p1, "uid"    # I
    .param p2, "active"    # Z
    .param p3, "behavier"    # I

    .line 57
    iget-object v0, p0, Lcom/miui/server/smartpower/AppPowerResource;->mResourceCallbacksByUid:Landroid/util/SparseArray;

    monitor-enter v0

    .line 58
    :try_start_0
    iget-object v1, p0, Lcom/miui/server/smartpower/AppPowerResource;->mResourceCallbacksByUid:Landroid/util/SparseArray;

    invoke-virtual {v1, p1}, Landroid/util/SparseArray;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/util/List;

    .line 59
    .local v1, "callbacks":Ljava/util/List;, "Ljava/util/List<Lcom/miui/server/smartpower/AppPowerResource$IAppPowerResourceCallback;>;"
    if-eqz v1, :cond_1

    .line 60
    invoke-interface {v1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_1

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/miui/server/smartpower/AppPowerResource$IAppPowerResourceCallback;

    .line 61
    .local v3, "callback":Lcom/miui/server/smartpower/AppPowerResource$IAppPowerResourceCallback;
    if-eqz p2, :cond_0

    .line 62
    iget v4, p0, Lcom/miui/server/smartpower/AppPowerResource;->mType:I

    invoke-interface {v3, v4, p3}, Lcom/miui/server/smartpower/AppPowerResource$IAppPowerResourceCallback;->noteResourceActive(II)V

    goto :goto_1

    .line 64
    :cond_0
    iget v4, p0, Lcom/miui/server/smartpower/AppPowerResource;->mType:I

    invoke-interface {v3, v4, p3}, Lcom/miui/server/smartpower/AppPowerResource$IAppPowerResourceCallback;->noteResourceInactive(II)V

    .line 66
    .end local v3    # "callback":Lcom/miui/server/smartpower/AppPowerResource$IAppPowerResourceCallback;
    :goto_1
    goto :goto_0

    .line 68
    .end local v1    # "callbacks":Ljava/util/List;, "Ljava/util/List<Lcom/miui/server/smartpower/AppPowerResource$IAppPowerResourceCallback;>;"
    :cond_1
    monitor-exit v0

    .line 69
    return-void

    .line 68
    :catchall_0
    move-exception v1

    monitor-exit v0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v1
.end method

.method public abstract resumeAppPowerResource(I)V
.end method

.method public unRegisterCallback(Lcom/miui/server/smartpower/AppPowerResource$IAppPowerResourceCallback;I)V
    .locals 3
    .param p1, "callback"    # Lcom/miui/server/smartpower/AppPowerResource$IAppPowerResourceCallback;
    .param p2, "uid"    # I

    .line 45
    iget-object v0, p0, Lcom/miui/server/smartpower/AppPowerResource;->mResourceCallbacksByUid:Landroid/util/SparseArray;

    monitor-enter v0

    .line 46
    :try_start_0
    iget-object v1, p0, Lcom/miui/server/smartpower/AppPowerResource;->mResourceCallbacksByUid:Landroid/util/SparseArray;

    invoke-virtual {v1, p2}, Landroid/util/SparseArray;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/util/List;

    .line 47
    .local v1, "callbacks":Ljava/util/List;, "Ljava/util/List<Lcom/miui/server/smartpower/AppPowerResource$IAppPowerResourceCallback;>;"
    if-eqz v1, :cond_0

    invoke-interface {v1, p1}, Ljava/util/List;->contains(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_0

    .line 48
    invoke-interface {v1, p1}, Ljava/util/List;->remove(Ljava/lang/Object;)Z

    .line 49
    invoke-interface {v1}, Ljava/util/List;->size()I

    move-result v2

    if-nez v2, :cond_0

    .line 50
    iget-object v2, p0, Lcom/miui/server/smartpower/AppPowerResource;->mResourceCallbacksByUid:Landroid/util/SparseArray;

    invoke-virtual {v2, p2}, Landroid/util/SparseArray;->remove(I)V

    .line 53
    .end local v1    # "callbacks":Ljava/util/List;, "Ljava/util/List<Lcom/miui/server/smartpower/AppPowerResource$IAppPowerResourceCallback;>;"
    :cond_0
    monitor-exit v0

    .line 54
    return-void

    .line 53
    :catchall_0
    move-exception v1

    monitor-exit v0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v1
.end method

.method public unRegisterCallback(Lcom/miui/server/smartpower/AppPowerResource$IAppPowerResourceCallback;II)V
    .locals 3
    .param p1, "callback"    # Lcom/miui/server/smartpower/AppPowerResource$IAppPowerResourceCallback;
    .param p2, "uid"    # I
    .param p3, "pid"    # I

    .line 85
    iget-object v0, p0, Lcom/miui/server/smartpower/AppPowerResource;->mResourceCallbacksByPid:Landroid/util/SparseArray;

    monitor-enter v0

    .line 86
    :try_start_0
    iget-object v1, p0, Lcom/miui/server/smartpower/AppPowerResource;->mResourceCallbacksByPid:Landroid/util/SparseArray;

    invoke-virtual {v1, p3}, Landroid/util/SparseArray;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/util/List;

    .line 87
    .local v1, "callbacks":Ljava/util/List;, "Ljava/util/List<Lcom/miui/server/smartpower/AppPowerResource$IAppPowerResourceCallback;>;"
    if-eqz v1, :cond_0

    invoke-interface {v1, p1}, Ljava/util/List;->contains(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_0

    .line 88
    invoke-interface {v1, p1}, Ljava/util/List;->remove(Ljava/lang/Object;)Z

    .line 89
    invoke-interface {v1}, Ljava/util/List;->size()I

    move-result v2

    if-nez v2, :cond_0

    .line 90
    iget-object v2, p0, Lcom/miui/server/smartpower/AppPowerResource;->mResourceCallbacksByPid:Landroid/util/SparseArray;

    invoke-virtual {v2, p3}, Landroid/util/SparseArray;->remove(I)V

    .line 93
    .end local v1    # "callbacks":Ljava/util/List;, "Ljava/util/List<Lcom/miui/server/smartpower/AppPowerResource$IAppPowerResourceCallback;>;"
    :cond_0
    monitor-exit v0

    .line 94
    return-void

    .line 93
    :catchall_0
    move-exception v1

    monitor-exit v0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v1
.end method
