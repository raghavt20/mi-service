.class Lcom/miui/server/smartpower/PowerFrozenManager$H;
.super Landroid/os/Handler;
.source "PowerFrozenManager.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/miui/server/smartpower/PowerFrozenManager;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "H"
.end annotation


# instance fields
.field final synthetic this$0:Lcom/miui/server/smartpower/PowerFrozenManager;


# direct methods
.method public constructor <init>(Lcom/miui/server/smartpower/PowerFrozenManager;Landroid/os/Looper;)V
    .locals 0
    .param p2, "looper"    # Landroid/os/Looper;

    .line 535
    iput-object p1, p0, Lcom/miui/server/smartpower/PowerFrozenManager$H;->this$0:Lcom/miui/server/smartpower/PowerFrozenManager;

    .line 536
    invoke-direct {p0, p2}, Landroid/os/Handler;-><init>(Landroid/os/Looper;)V

    .line 537
    return-void
.end method


# virtual methods
.method public handleMessage(Landroid/os/Message;)V
    .locals 5
    .param p1, "msg"    # Landroid/os/Message;

    .line 541
    invoke-super {p0, p1}, Landroid/os/Handler;->handleMessage(Landroid/os/Message;)V

    .line 542
    iget v0, p1, Landroid/os/Message;->what:I

    packed-switch v0, :pswitch_data_0

    goto :goto_0

    .line 545
    :pswitch_0
    :try_start_0
    iget-object v0, p0, Lcom/miui/server/smartpower/PowerFrozenManager$H;->this$0:Lcom/miui/server/smartpower/PowerFrozenManager;

    iget v1, p1, Landroid/os/Message;->arg2:I

    invoke-virtual {v0, v1}, Lcom/miui/server/smartpower/PowerFrozenManager;->isFrozenPid(I)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 546
    iget v0, p1, Landroid/os/Message;->arg1:I

    .line 547
    .local v0, "uid":I
    iget v1, p1, Landroid/os/Message;->arg2:I

    .line 548
    .local v1, "pid":I
    iget-object v2, p0, Lcom/miui/server/smartpower/PowerFrozenManager$H;->this$0:Lcom/miui/server/smartpower/PowerFrozenManager;

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "Timeout pid "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v0, v1, v3}, Lcom/miui/server/smartpower/PowerFrozenManager;->thawProcess(IILjava/lang/String;)Z
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 552
    .end local v0    # "uid":I
    .end local v1    # "pid":I
    :cond_0
    goto :goto_0

    .line 550
    :catch_0
    move-exception v0

    .line 551
    .local v0, "e":Ljava/lang/Exception;
    invoke-virtual {v0}, Ljava/lang/Exception;->printStackTrace()V

    .line 553
    .end local v0    # "e":Ljava/lang/Exception;
    nop

    .line 557
    :goto_0
    return-void

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
    .end packed-switch
.end method
