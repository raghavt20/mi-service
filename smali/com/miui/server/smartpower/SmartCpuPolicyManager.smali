.class public Lcom/miui/server/smartpower/SmartCpuPolicyManager;
.super Ljava/lang/Object;
.source "SmartCpuPolicyManager.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/miui/server/smartpower/SmartCpuPolicyManager$H;
    }
.end annotation


# static fields
.field private static final CPU_EXCEPTION_HANDLE_THRESHOLD:I = 0x7530

.field private static final CPU_EXCEPTION_KILL_REASON:Ljava/lang/String; = "cpu exception"

.field public static final DEBUG:Z

.field public static final DEFAULT_BACKGROUND_CPU_CORE_NUM:I = 0x4

.field private static final MONITOR_CPU_MIN_TIME:I = 0x2710

.field static final MONITOR_THREAD_CPU_USAGE:Z = true

.field private static final MSG_CPU_EXCEPTION:I = 0x7

.field public static final TAG:Ljava/lang/String; = "SmartPower.CpuPolicy"


# instance fields
.field private mAMS:Lcom/android/server/am/ActivityManagerService;

.field private mAppStateManager:Lcom/android/server/am/AppStateManager;

.field private mBackgroundCpuCoreNum:I

.field private mHandler:Lcom/miui/server/smartpower/SmartCpuPolicyManager$H;

.field private mHandlerTh:Landroid/os/HandlerThread;

.field private mLashHandleCpuException:J

.field private final mLastUpdateCpuTime:Ljava/util/concurrent/atomic/AtomicLong;

.field private final mProcessCpuTracker:Lcom/android/internal/os/ProcessCpuTracker;

.field private mSmartPowerPolicyManager:Lcom/miui/server/smartpower/SmartPowerPolicyManager;


# direct methods
.method public static synthetic $r8$lambda$hrw7akw63cq1a8LnnBAW_4RwzFw(Lcom/miui/server/smartpower/SmartCpuPolicyManager;ZLcom/android/internal/os/ProcessCpuTracker$Stats;)V
    .locals 0

    invoke-direct {p0, p1, p2}, Lcom/miui/server/smartpower/SmartCpuPolicyManager;->lambda$updateCpuStatsNow$0(ZLcom/android/internal/os/ProcessCpuTracker$Stats;)V

    return-void
.end method

.method static bridge synthetic -$$Nest$mhandleLimitCpuException(Lcom/miui/server/smartpower/SmartCpuPolicyManager;I)V
    .locals 0

    invoke-direct {p0, p1}, Lcom/miui/server/smartpower/SmartCpuPolicyManager;->handleLimitCpuException(I)V

    return-void
.end method

.method static constructor <clinit>()V
    .locals 1

    .line 31
    sget-boolean v0, Landroid/os/spc/PressureStateSettings;->DEBUG_ALL:Z

    sput-boolean v0, Lcom/miui/server/smartpower/SmartCpuPolicyManager;->DEBUG:Z

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Lcom/android/server/am/ActivityManagerService;)V
    .locals 3
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "ams"    # Lcom/android/server/am/ActivityManagerService;

    .line 53
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 40
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/miui/server/smartpower/SmartCpuPolicyManager;->mAppStateManager:Lcom/android/server/am/AppStateManager;

    .line 43
    new-instance v0, Landroid/os/HandlerThread;

    const-string v1, "CpuPolicyTh"

    const/4 v2, -0x2

    invoke-direct {v0, v1, v2}, Landroid/os/HandlerThread;-><init>(Ljava/lang/String;I)V

    iput-object v0, p0, Lcom/miui/server/smartpower/SmartCpuPolicyManager;->mHandlerTh:Landroid/os/HandlerThread;

    .line 45
    const/4 v0, 0x4

    iput v0, p0, Lcom/miui/server/smartpower/SmartCpuPolicyManager;->mBackgroundCpuCoreNum:I

    .line 46
    const-wide/16 v0, 0x0

    iput-wide v0, p0, Lcom/miui/server/smartpower/SmartCpuPolicyManager;->mLashHandleCpuException:J

    .line 47
    new-instance v2, Ljava/util/concurrent/atomic/AtomicLong;

    invoke-direct {v2, v0, v1}, Ljava/util/concurrent/atomic/AtomicLong;-><init>(J)V

    iput-object v2, p0, Lcom/miui/server/smartpower/SmartCpuPolicyManager;->mLastUpdateCpuTime:Ljava/util/concurrent/atomic/AtomicLong;

    .line 50
    new-instance v0, Lcom/android/internal/os/ProcessCpuTracker;

    const/4 v1, 0x1

    invoke-direct {v0, v1}, Lcom/android/internal/os/ProcessCpuTracker;-><init>(Z)V

    iput-object v0, p0, Lcom/miui/server/smartpower/SmartCpuPolicyManager;->mProcessCpuTracker:Lcom/android/internal/os/ProcessCpuTracker;

    .line 54
    iput-object p2, p0, Lcom/miui/server/smartpower/SmartCpuPolicyManager;->mAMS:Lcom/android/server/am/ActivityManagerService;

    .line 56
    return-void
.end method

.method private checkProcessRecord(Lcom/miui/server/smartpower/IAppState$IRunningProcess;)Z
    .locals 2
    .param p1, "proc"    # Lcom/miui/server/smartpower/IAppState$IRunningProcess;

    .line 223
    if-eqz p1, :cond_0

    invoke-virtual {p1}, Lcom/miui/server/smartpower/IAppState$IRunningProcess;->getAdj()I

    move-result v0

    const/16 v1, 0xc8

    if-le v0, v1, :cond_0

    .line 224
    invoke-virtual {p1}, Lcom/miui/server/smartpower/IAppState$IRunningProcess;->getPid()I

    move-result v0

    if-lez v0, :cond_0

    invoke-virtual {p1}, Lcom/miui/server/smartpower/IAppState$IRunningProcess;->getUid()I

    move-result v0

    invoke-static {v0}, Landroid/os/UserHandle;->isApp(I)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 225
    invoke-virtual {p1}, Lcom/miui/server/smartpower/IAppState$IRunningProcess;->isKilled()Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    .line 223
    :goto_0
    return v0
.end method

.method private forAllCpuStats(Ljava/util/function/Consumer;)V
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/function/Consumer<",
            "Lcom/android/internal/os/ProcessCpuTracker$Stats;",
            ">;)V"
        }
    .end annotation

    .line 175
    .local p1, "consumer":Ljava/util/function/Consumer;, "Ljava/util/function/Consumer<Lcom/android/internal/os/ProcessCpuTracker$Stats;>;"
    iget-object v0, p0, Lcom/miui/server/smartpower/SmartCpuPolicyManager;->mProcessCpuTracker:Lcom/android/internal/os/ProcessCpuTracker;

    monitor-enter v0

    .line 176
    :try_start_0
    iget-object v1, p0, Lcom/miui/server/smartpower/SmartCpuPolicyManager;->mProcessCpuTracker:Lcom/android/internal/os/ProcessCpuTracker;

    invoke-virtual {v1}, Lcom/android/internal/os/ProcessCpuTracker;->countStats()I

    move-result v1

    .line 177
    .local v1, "numOfStats":I
    const/4 v2, 0x0

    .local v2, "i":I
    :goto_0
    if-ge v2, v1, :cond_0

    .line 178
    iget-object v3, p0, Lcom/miui/server/smartpower/SmartCpuPolicyManager;->mProcessCpuTracker:Lcom/android/internal/os/ProcessCpuTracker;

    invoke-virtual {v3, v2}, Lcom/android/internal/os/ProcessCpuTracker;->getStats(I)Lcom/android/internal/os/ProcessCpuTracker$Stats;

    move-result-object v3

    invoke-interface {p1, v3}, Ljava/util/function/Consumer;->accept(Ljava/lang/Object;)V

    .line 177
    add-int/lit8 v2, v2, 0x1

    goto :goto_0

    .line 180
    .end local v1    # "numOfStats":I
    .end local v2    # "i":I
    :cond_0
    monitor-exit v0

    .line 181
    return-void

    .line 180
    :catchall_0
    move-exception v1

    monitor-exit v0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v1
.end method

.method private handleLimitCpuException(I)V
    .locals 23
    .param p1, "type"    # I

    .line 127
    move-object/from16 v0, p0

    invoke-static {}, Landroid/os/SystemClock;->uptimeMillis()J

    move-result-wide v1

    iput-wide v1, v0, Lcom/miui/server/smartpower/SmartCpuPolicyManager;->mLashHandleCpuException:J

    .line 128
    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/miui/server/smartpower/SmartCpuPolicyManager;->updateCpuStatsNow(Z)J

    move-result-wide v2

    .line 129
    .local v2, "uptimeSince":J
    const-wide/16 v4, 0x0

    cmp-long v6, v2, v4

    if-gtz v6, :cond_0

    .line 130
    return-void

    .line 132
    :cond_0
    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string v7, "HandleLimitCpuException: type="

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    move/from16 v7, p1

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v6

    const-string v8, " bgcpu="

    invoke-virtual {v6, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    iget v8, v0, Lcom/miui/server/smartpower/SmartCpuPolicyManager;->mBackgroundCpuCoreNum:I

    invoke-virtual {v6, v8}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v6

    const-string v8, " over:"

    invoke-virtual {v6, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    .line 134
    invoke-static {v2, v3}, Landroid/util/TimeUtils;->formatDuration(J)Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v6, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    .line 132
    const-string v8, "SmartPower.CpuPolicy"

    invoke-static {v8, v6}, Landroid/util/Slog;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 135
    const/16 v6, 0x1d8

    .line 140
    .local v6, "flag":I
    iget-object v8, v0, Lcom/miui/server/smartpower/SmartCpuPolicyManager;->mAppStateManager:Lcom/android/server/am/AppStateManager;

    invoke-virtual {v8}, Lcom/android/server/am/AppStateManager;->getAllAppState()Ljava/util/ArrayList;

    move-result-object v8

    .line 141
    .local v8, "appStateList":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/miui/server/smartpower/IAppState;>;"
    invoke-virtual {v8}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v9

    :goto_0
    invoke-interface {v9}, Ljava/util/Iterator;->hasNext()Z

    move-result v10

    if-eqz v10, :cond_9

    invoke-interface {v9}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v10

    check-cast v10, Lcom/miui/server/smartpower/IAppState;

    .line 142
    .local v10, "appState":Lcom/miui/server/smartpower/IAppState;
    iget-wide v11, v0, Lcom/miui/server/smartpower/SmartCpuPolicyManager;->mLashHandleCpuException:J

    invoke-virtual {v10}, Lcom/miui/server/smartpower/IAppState;->getLastTopTime()J

    move-result-wide v13

    sub-long/2addr v11, v13

    .line 143
    .local v11, "backgroundUpdateTime":J
    invoke-virtual {v10}, Lcom/miui/server/smartpower/IAppState;->isVsible()Z

    move-result v13

    if-nez v13, :cond_8

    const-wide/16 v13, 0x753a

    cmp-long v13, v11, v13

    if-gez v13, :cond_1

    .line 145
    goto :goto_0

    .line 147
    :cond_1
    invoke-virtual {v10}, Lcom/miui/server/smartpower/IAppState;->getRunningProcessList()Ljava/util/ArrayList;

    move-result-object v13

    invoke-virtual {v13}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v13

    :goto_1
    invoke-interface {v13}, Ljava/util/Iterator;->hasNext()Z

    move-result v14

    if-eqz v14, :cond_7

    invoke-interface {v13}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v14

    check-cast v14, Lcom/miui/server/smartpower/IAppState$IRunningProcess;

    .line 148
    .local v14, "runningProc":Lcom/miui/server/smartpower/IAppState$IRunningProcess;
    move-wide/from16 v16, v2

    .end local v2    # "uptimeSince":J
    .local v16, "uptimeSince":J
    invoke-virtual {v14}, Lcom/miui/server/smartpower/IAppState$IRunningProcess;->getCurCpuTime()J

    move-result-wide v1

    .line 149
    .local v1, "curCpuTime":J
    invoke-virtual {v14}, Lcom/miui/server/smartpower/IAppState$IRunningProcess;->getLastCpuTime()J

    move-result-wide v18

    .line 150
    .local v18, "lastCpuTime":J
    invoke-virtual {v14, v1, v2}, Lcom/miui/server/smartpower/IAppState$IRunningProcess;->setLastCpuTime(J)V

    .line 152
    cmp-long v3, v18, v4

    if-lez v3, :cond_6

    .line 153
    invoke-direct {v0, v14}, Lcom/miui/server/smartpower/SmartCpuPolicyManager;->checkProcessRecord(Lcom/miui/server/smartpower/IAppState$IRunningProcess;)Z

    move-result v3

    if-eqz v3, :cond_5

    .line 154
    invoke-virtual {v14}, Lcom/miui/server/smartpower/IAppState$IRunningProcess;->isProcessPerceptible()Z

    move-result v3

    if-nez v3, :cond_4

    iget-object v3, v0, Lcom/miui/server/smartpower/SmartCpuPolicyManager;->mSmartPowerPolicyManager:Lcom/miui/server/smartpower/SmartPowerPolicyManager;

    .line 156
    invoke-virtual {v14}, Lcom/miui/server/smartpower/IAppState$IRunningProcess;->getPackageName()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v14}, Lcom/miui/server/smartpower/IAppState$IRunningProcess;->getProcessName()Ljava/lang/String;

    move-result-object v5

    .line 155
    invoke-virtual {v3, v6, v4, v5}, Lcom/miui/server/smartpower/SmartPowerPolicyManager;->isProcessWhiteList(ILjava/lang/String;Ljava/lang/String;)Z

    move-result v3

    if-eqz v3, :cond_2

    .line 157
    move-wide/from16 v2, v16

    const/4 v1, 0x0

    const-wide/16 v4, 0x0

    goto :goto_1

    .line 159
    :cond_2
    sub-long v3, v1, v18

    iget v5, v0, Lcom/miui/server/smartpower/SmartCpuPolicyManager;->mBackgroundCpuCoreNum:I

    move/from16 v20, v6

    .end local v6    # "flag":I
    .local v20, "flag":I
    int-to-long v5, v5

    div-long/2addr v3, v5

    .line 160
    .local v3, "cpuTimeUsed":J
    long-to-float v5, v3

    const/high16 v6, 0x42c80000    # 100.0f

    mul-float/2addr v5, v6

    move-wide/from16 v6, v16

    .end local v16    # "uptimeSince":J
    .local v6, "uptimeSince":J
    long-to-float v15, v6

    div-float/2addr v5, v15

    sget v15, Landroid/os/spc/PressureStateSettings;->PROC_CPU_EXCEPTION_THRESHOLD:I

    int-to-float v15, v15

    cmpl-float v5, v5, v15

    if-ltz v5, :cond_3

    .line 162
    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v15, "cpu exception over "

    invoke-virtual {v5, v15}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    .line 163
    invoke-static {v6, v7}, Landroid/util/TimeUtils;->formatDuration(J)Ljava/lang/String;

    move-result-object v15

    invoke-virtual {v5, v15}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v15, " used "

    invoke-virtual {v5, v15}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    .line 164
    invoke-static {v3, v4}, Landroid/util/TimeUtils;->formatDuration(J)Ljava/lang/String;

    move-result-object v15

    invoke-virtual {v5, v15}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v15, " ("

    invoke-virtual {v5, v15}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    long-to-float v15, v3

    const/high16 v17, 0x42c80000    # 100.0f

    mul-float v15, v15, v17

    long-to-float v0, v6

    div-float/2addr v15, v0

    invoke-virtual {v5, v15}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v5, "%)"

    invoke-virtual {v0, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    .line 166
    .local v0, "reason":Ljava/lang/String;
    invoke-static {}, Lcom/android/server/am/SystemPressureControllerStub;->getInstance()Lcom/android/server/am/SystemPressureControllerStub;

    move-result-object v5

    .line 167
    invoke-virtual {v14}, Lcom/miui/server/smartpower/IAppState$IRunningProcess;->getProcessRecord()Lcom/android/server/am/ProcessRecord;

    move-result-object v15

    .line 166
    move-wide/from16 v21, v3

    const/4 v3, 0x0

    .end local v3    # "cpuTimeUsed":J
    .local v21, "cpuTimeUsed":J
    invoke-virtual {v5, v15, v3, v0}, Lcom/android/server/am/SystemPressureControllerStub;->killProcess(Lcom/android/server/am/ProcessRecord;ILjava/lang/String;)J

    goto :goto_2

    .line 160
    .end local v0    # "reason":Ljava/lang/String;
    .end local v21    # "cpuTimeUsed":J
    .restart local v3    # "cpuTimeUsed":J
    :cond_3
    move-wide/from16 v21, v3

    const/4 v3, 0x0

    .line 169
    .end local v3    # "cpuTimeUsed":J
    .restart local v21    # "cpuTimeUsed":J
    :goto_2
    invoke-virtual {v14, v1, v2}, Lcom/miui/server/smartpower/IAppState$IRunningProcess;->setLastCpuTime(J)V

    .line 170
    .end local v1    # "curCpuTime":J
    .end local v14    # "runningProc":Lcom/miui/server/smartpower/IAppState$IRunningProcess;
    .end local v18    # "lastCpuTime":J
    .end local v21    # "cpuTimeUsed":J
    move-object/from16 v0, p0

    move v1, v3

    move-wide v2, v6

    move/from16 v6, v20

    const-wide/16 v4, 0x0

    move/from16 v7, p1

    goto/16 :goto_1

    .line 154
    .end local v20    # "flag":I
    .restart local v1    # "curCpuTime":J
    .local v6, "flag":I
    .restart local v14    # "runningProc":Lcom/miui/server/smartpower/IAppState$IRunningProcess;
    .restart local v16    # "uptimeSince":J
    .restart local v18    # "lastCpuTime":J
    :cond_4
    move/from16 v20, v6

    move-wide/from16 v6, v16

    const/4 v3, 0x0

    .end local v16    # "uptimeSince":J
    .local v6, "uptimeSince":J
    .restart local v20    # "flag":I
    move-object/from16 v0, p0

    move v1, v3

    move-wide v2, v6

    move/from16 v6, v20

    const-wide/16 v4, 0x0

    move/from16 v7, p1

    goto/16 :goto_1

    .line 153
    .end local v20    # "flag":I
    .local v6, "flag":I
    .restart local v16    # "uptimeSince":J
    :cond_5
    move/from16 v20, v6

    move-wide/from16 v6, v16

    const/4 v3, 0x0

    .end local v16    # "uptimeSince":J
    .local v6, "uptimeSince":J
    .restart local v20    # "flag":I
    move-object/from16 v0, p0

    move v1, v3

    move-wide v2, v6

    move/from16 v6, v20

    const-wide/16 v4, 0x0

    move/from16 v7, p1

    goto/16 :goto_1

    .line 152
    .end local v20    # "flag":I
    .local v6, "flag":I
    .restart local v16    # "uptimeSince":J
    :cond_6
    move/from16 v20, v6

    move-wide/from16 v6, v16

    const/4 v3, 0x0

    .end local v16    # "uptimeSince":J
    .local v6, "uptimeSince":J
    .restart local v20    # "flag":I
    move-object/from16 v0, p0

    move v1, v3

    move-wide v2, v6

    move/from16 v6, v20

    const-wide/16 v4, 0x0

    move/from16 v7, p1

    goto/16 :goto_1

    .line 147
    .end local v1    # "curCpuTime":J
    .end local v14    # "runningProc":Lcom/miui/server/smartpower/IAppState$IRunningProcess;
    .end local v18    # "lastCpuTime":J
    .end local v20    # "flag":I
    .restart local v2    # "uptimeSince":J
    .local v6, "flag":I
    :cond_7
    move/from16 v20, v6

    move-wide v6, v2

    move v3, v1

    .line 171
    .end local v2    # "uptimeSince":J
    .end local v10    # "appState":Lcom/miui/server/smartpower/IAppState;
    .end local v11    # "backgroundUpdateTime":J
    .local v6, "uptimeSince":J
    .restart local v20    # "flag":I
    move-object/from16 v0, p0

    move-wide v2, v6

    move/from16 v6, v20

    const-wide/16 v4, 0x0

    move/from16 v7, p1

    goto/16 :goto_0

    .line 143
    .end local v20    # "flag":I
    .restart local v2    # "uptimeSince":J
    .local v6, "flag":I
    .restart local v10    # "appState":Lcom/miui/server/smartpower/IAppState;
    .restart local v11    # "backgroundUpdateTime":J
    :cond_8
    move/from16 v20, v6

    move-wide v6, v2

    move v3, v1

    .end local v2    # "uptimeSince":J
    .local v6, "uptimeSince":J
    .restart local v20    # "flag":I
    move-object/from16 v0, p0

    move-wide v2, v6

    move/from16 v6, v20

    const-wide/16 v4, 0x0

    move/from16 v7, p1

    goto/16 :goto_0

    .line 172
    .end local v10    # "appState":Lcom/miui/server/smartpower/IAppState;
    .end local v11    # "backgroundUpdateTime":J
    .end local v20    # "flag":I
    .restart local v2    # "uptimeSince":J
    .local v6, "flag":I
    :cond_9
    return-void
.end method

.method public static isEnableCpuLimit()Z
    .locals 1

    .line 67
    const/4 v0, 0x0

    return v0
.end method

.method private synthetic lambda$updateCpuStatsNow$0(ZLcom/android/internal/os/ProcessCpuTracker$Stats;)V
    .locals 5
    .param p1, "isReset"    # Z
    .param p2, "st"    # Lcom/android/internal/os/ProcessCpuTracker$Stats;

    .line 199
    iget-boolean v0, p2, Lcom/android/internal/os/ProcessCpuTracker$Stats;->working:Z

    if-eqz v0, :cond_4

    iget v0, p2, Lcom/android/internal/os/ProcessCpuTracker$Stats;->uid:I

    const/16 v1, 0x3e8

    if-le v0, v1, :cond_4

    iget v0, p2, Lcom/android/internal/os/ProcessCpuTracker$Stats;->pid:I

    if-gtz v0, :cond_0

    goto :goto_2

    .line 202
    :cond_0
    iget-object v0, p0, Lcom/miui/server/smartpower/SmartCpuPolicyManager;->mAppStateManager:Lcom/android/server/am/AppStateManager;

    iget v1, p2, Lcom/android/internal/os/ProcessCpuTracker$Stats;->uid:I

    iget v2, p2, Lcom/android/internal/os/ProcessCpuTracker$Stats;->pid:I

    invoke-virtual {v0, v1, v2}, Lcom/android/server/am/AppStateManager;->getRunningProcess(II)Lcom/android/server/am/AppStateManager$AppState$RunningProcess;

    move-result-object v0

    .line 203
    .local v0, "proc":Lcom/android/server/am/AppStateManager$AppState$RunningProcess;
    if-eqz v0, :cond_3

    invoke-virtual {v0}, Lcom/android/server/am/AppStateManager$AppState$RunningProcess;->isKilled()Z

    move-result v1

    if-eqz v1, :cond_1

    goto :goto_1

    .line 206
    :cond_1
    iget v1, p2, Lcom/android/internal/os/ProcessCpuTracker$Stats;->rel_utime:I

    iget v2, p2, Lcom/android/internal/os/ProcessCpuTracker$Stats;->rel_stime:I

    add-int/2addr v1, v2

    int-to-long v1, v1

    invoke-virtual {v0, v1, v2}, Lcom/android/server/am/AppStateManager$AppState$RunningProcess;->addAndGetCurCpuTime(J)J

    move-result-wide v1

    .line 207
    .local v1, "curCpuTime":J
    if-eqz p1, :cond_2

    .line 208
    invoke-virtual {v0, v1, v2}, Lcom/android/server/am/AppStateManager$AppState$RunningProcess;->setLastCpuTime(J)V

    goto :goto_0

    .line 210
    :cond_2
    const-wide/16 v3, 0x0

    invoke-virtual {v0, v3, v4, v1, v2}, Lcom/android/server/am/AppStateManager$AppState$RunningProcess;->compareAndSetLastCpuTime(JJ)Z

    .line 212
    :goto_0
    return-void

    .line 204
    .end local v1    # "curCpuTime":J
    :cond_3
    :goto_1
    return-void

    .line 200
    .end local v0    # "proc":Lcom/android/server/am/AppStateManager$AppState$RunningProcess;
    :cond_4
    :goto_2
    return-void
.end method

.method public static parseCpuCores(Ljava/lang/String;)Landroid/util/IntArray;
    .locals 9
    .param p0, "cpuContent"    # Ljava/lang/String;

    .line 86
    const-string v0, "SmartPower.CpuPolicy"

    const-string v1, ","

    new-instance v2, Landroid/util/IntArray;

    const/4 v3, 0x0

    invoke-direct {v2, v3}, Landroid/util/IntArray;-><init>(I)V

    .line 88
    .local v2, "cpuCores":Landroid/util/IntArray;
    :try_start_0
    invoke-virtual {p0, v1}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v4

    if-eqz v4, :cond_0

    invoke-virtual {p0}, Ljava/lang/String;->trim()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v4, v1}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v1

    goto :goto_0

    .line 89
    :cond_0
    invoke-virtual {p0}, Ljava/lang/String;->trim()Ljava/lang/String;

    move-result-object v1

    const-string v4, " "

    invoke-virtual {v1, v4}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v1

    :goto_0
    nop

    .line 90
    .local v1, "pairs":[Ljava/lang/String;
    const/4 v4, 0x0

    .local v4, "j":I
    :goto_1
    array-length v5, v1

    if-ge v4, v5, :cond_5

    .line 91
    aget-object v5, v1, v4

    const-string v6, "-"

    invoke-virtual {v5, v6}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v5

    .line 92
    .local v5, "minMaxPairs":[Ljava/lang/String;
    array-length v6, v5

    const/4 v7, 0x2

    const/4 v8, 0x1

    if-lt v6, v7, :cond_3

    .line 93
    aget-object v6, v5, v3

    invoke-static {v6}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v6

    .line 94
    .local v6, "min":I
    aget-object v7, v5, v8

    invoke-static {v7}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v7

    .line 95
    .local v7, "max":I
    if-le v6, v7, :cond_1

    .line 96
    goto :goto_3

    .line 98
    :cond_1
    move v8, v6

    .local v8, "id":I
    :goto_2
    if-gt v8, v7, :cond_2

    .line 99
    invoke-virtual {v2, v8}, Landroid/util/IntArray;->add(I)V

    .line 98
    add-int/lit8 v8, v8, 0x1

    goto :goto_2

    .line 101
    .end local v6    # "min":I
    .end local v7    # "max":I
    .end local v8    # "id":I
    :cond_2
    goto :goto_3

    :cond_3
    array-length v6, v5

    if-ne v6, v8, :cond_4

    .line 102
    aget-object v6, v5, v3

    invoke-static {v6}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v6

    invoke-virtual {v2, v6}, Landroid/util/IntArray;->add(I)V

    goto :goto_3

    .line 104
    :cond_4
    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string v7, "Invalid CPU core range format "

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    aget-object v7, v1, v4

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-static {v0, v6}, Landroid/util/Slog;->i(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 90
    .end local v5    # "minMaxPairs":[Ljava/lang/String;
    :goto_3
    add-int/lit8 v4, v4, 0x1

    goto :goto_1

    .line 109
    .end local v1    # "pairs":[Ljava/lang/String;
    .end local v4    # "j":I
    :cond_5
    goto :goto_4

    .line 107
    :catch_0
    move-exception v1

    .line 108
    .local v1, "e":Ljava/lang/Exception;
    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "Failed to parse CPU cores from "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v0, v3}, Landroid/util/Slog;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 110
    .end local v1    # "e":Ljava/lang/Exception;
    :goto_4
    return-object v2
.end method


# virtual methods
.method public cpuExceptionEvents(I)V
    .locals 4
    .param p1, "type"    # I

    .line 117
    sget-boolean v0, Landroid/os/spc/PressureStateSettings;->PROC_CPU_EXCEPTION_ENABLE:Z

    if-eqz v0, :cond_0

    .line 118
    invoke-static {}, Landroid/os/SystemClock;->uptimeMillis()J

    move-result-wide v0

    iget-wide v2, p0, Lcom/miui/server/smartpower/SmartCpuPolicyManager;->mLashHandleCpuException:J

    sub-long/2addr v0, v2

    const-wide/16 v2, 0x7530

    cmp-long v0, v0, v2

    if-lez v0, :cond_0

    iget-object v0, p0, Lcom/miui/server/smartpower/SmartCpuPolicyManager;->mHandler:Lcom/miui/server/smartpower/SmartCpuPolicyManager$H;

    if-eqz v0, :cond_0

    .line 119
    const/4 v1, 0x7

    invoke-virtual {v0, v1}, Lcom/miui/server/smartpower/SmartCpuPolicyManager$H;->hasMessages(I)Z

    move-result v0

    if-nez v0, :cond_0

    .line 120
    iget-object v0, p0, Lcom/miui/server/smartpower/SmartCpuPolicyManager;->mHandler:Lcom/miui/server/smartpower/SmartCpuPolicyManager$H;

    invoke-virtual {v0, v1}, Lcom/miui/server/smartpower/SmartCpuPolicyManager$H;->obtainMessage(I)Landroid/os/Message;

    move-result-object v0

    .line 121
    .local v0, "msg":Landroid/os/Message;
    iput p1, v0, Landroid/os/Message;->arg1:I

    .line 122
    iget-object v1, p0, Lcom/miui/server/smartpower/SmartCpuPolicyManager;->mHandler:Lcom/miui/server/smartpower/SmartCpuPolicyManager$H;

    invoke-virtual {v1, v0}, Lcom/miui/server/smartpower/SmartCpuPolicyManager$H;->sendMessage(Landroid/os/Message;)Z

    .line 124
    .end local v0    # "msg":Landroid/os/Message;
    :cond_0
    return-void
.end method

.method public cpuPressureEvents(I)V
    .locals 0
    .param p1, "level"    # I

    .line 114
    return-void
.end method

.method public getBackgroundCpuCoreNum()I
    .locals 1

    .line 72
    iget v0, p0, Lcom/miui/server/smartpower/SmartCpuPolicyManager;->mBackgroundCpuCoreNum:I

    return v0
.end method

.method public init(Lcom/android/server/am/AppStateManager;Lcom/miui/server/smartpower/SmartPowerPolicyManager;)V
    .locals 2
    .param p1, "appStateManager"    # Lcom/android/server/am/AppStateManager;
    .param p2, "smartPowerPolicyManager"    # Lcom/miui/server/smartpower/SmartPowerPolicyManager;

    .line 60
    iput-object p1, p0, Lcom/miui/server/smartpower/SmartCpuPolicyManager;->mAppStateManager:Lcom/android/server/am/AppStateManager;

    .line 61
    iput-object p2, p0, Lcom/miui/server/smartpower/SmartCpuPolicyManager;->mSmartPowerPolicyManager:Lcom/miui/server/smartpower/SmartPowerPolicyManager;

    .line 62
    iget-object v0, p0, Lcom/miui/server/smartpower/SmartCpuPolicyManager;->mHandlerTh:Landroid/os/HandlerThread;

    invoke-virtual {v0}, Landroid/os/HandlerThread;->start()V

    .line 63
    new-instance v0, Lcom/miui/server/smartpower/SmartCpuPolicyManager$H;

    iget-object v1, p0, Lcom/miui/server/smartpower/SmartCpuPolicyManager;->mHandlerTh:Landroid/os/HandlerThread;

    invoke-virtual {v1}, Landroid/os/HandlerThread;->getLooper()Landroid/os/Looper;

    move-result-object v1

    invoke-direct {v0, p0, v1}, Lcom/miui/server/smartpower/SmartCpuPolicyManager$H;-><init>(Lcom/miui/server/smartpower/SmartCpuPolicyManager;Landroid/os/Looper;)V

    iput-object v0, p0, Lcom/miui/server/smartpower/SmartCpuPolicyManager;->mHandler:Lcom/miui/server/smartpower/SmartCpuPolicyManager$H;

    .line 64
    return-void
.end method

.method public updateBackgroundCpuCoreNum()V
    .locals 4

    .line 76
    invoke-static {}, Lcom/android/server/am/SystemPressureControllerStub;->getInstance()Lcom/android/server/am/SystemPressureControllerStub;

    move-result-object v0

    invoke-virtual {v0}, Lcom/android/server/am/SystemPressureControllerStub;->getBackgroundCpuPolicy()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/String;->trim()Ljava/lang/String;

    move-result-object v0

    .line 77
    .local v0, "bgCpu":Ljava/lang/String;
    invoke-static {v0}, Lcom/miui/server/smartpower/SmartCpuPolicyManager;->parseCpuCores(Ljava/lang/String;)Landroid/util/IntArray;

    move-result-object v1

    .line 78
    .local v1, "cpuCores":Landroid/util/IntArray;
    if-eqz v1, :cond_1

    invoke-virtual {v1}, Landroid/util/IntArray;->size()I

    move-result v2

    if-nez v2, :cond_0

    goto :goto_0

    .line 82
    :cond_0
    invoke-virtual {v1}, Landroid/util/IntArray;->size()I

    move-result v2

    iput v2, p0, Lcom/miui/server/smartpower/SmartCpuPolicyManager;->mBackgroundCpuCoreNum:I

    .line 83
    return-void

    .line 79
    :cond_1
    :goto_0
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "Failed to parse CPU cores from "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    const-string v3, "SmartPower.CpuPolicy"

    invoke-static {v3, v2}, Landroid/util/Slog;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 80
    return-void
.end method

.method public updateCpuStatsNow(Z)J
    .locals 9
    .param p1, "isReset"    # Z

    .line 184
    const-wide/16 v0, 0x0

    .line 185
    .local v0, "uptimeSince":J
    iget-object v2, p0, Lcom/miui/server/smartpower/SmartCpuPolicyManager;->mProcessCpuTracker:Lcom/android/internal/os/ProcessCpuTracker;

    monitor-enter v2

    .line 186
    :try_start_0
    invoke-static {}, Landroid/os/SystemClock;->uptimeMillis()J

    move-result-wide v3

    .line 187
    .local v3, "nowTime":J
    iget-object v5, p0, Lcom/miui/server/smartpower/SmartCpuPolicyManager;->mLastUpdateCpuTime:Ljava/util/concurrent/atomic/AtomicLong;

    invoke-virtual {v5}, Ljava/util/concurrent/atomic/AtomicLong;->get()J

    move-result-wide v5

    sub-long v5, v3, v5

    const-wide/16 v7, 0x2710

    cmp-long v5, v5, v7

    if-gez v5, :cond_0

    .line 188
    monitor-exit v2

    return-wide v0

    .line 190
    :cond_0
    const-string v5, "SmartPower.CpuPolicy"

    const-string/jumbo v6, "update process cpu time"

    invoke-static {v5, v6}, Landroid/util/Slog;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 191
    invoke-virtual {p0}, Lcom/miui/server/smartpower/SmartCpuPolicyManager;->updateBackgroundCpuCoreNum()V

    .line 192
    iget-object v5, p0, Lcom/miui/server/smartpower/SmartCpuPolicyManager;->mProcessCpuTracker:Lcom/android/internal/os/ProcessCpuTracker;

    invoke-virtual {v5}, Lcom/android/internal/os/ProcessCpuTracker;->update()V

    .line 193
    iget-object v5, p0, Lcom/miui/server/smartpower/SmartCpuPolicyManager;->mProcessCpuTracker:Lcom/android/internal/os/ProcessCpuTracker;

    invoke-virtual {v5}, Lcom/android/internal/os/ProcessCpuTracker;->hasGoodLastStats()Z

    move-result v5

    if-nez v5, :cond_1

    .line 194
    monitor-exit v2

    return-wide v0

    .line 196
    :cond_1
    iget-object v5, p0, Lcom/miui/server/smartpower/SmartCpuPolicyManager;->mLastUpdateCpuTime:Ljava/util/concurrent/atomic/AtomicLong;

    invoke-virtual {v5}, Ljava/util/concurrent/atomic/AtomicLong;->get()J

    move-result-wide v5

    sub-long v0, v3, v5

    .line 197
    iget-object v5, p0, Lcom/miui/server/smartpower/SmartCpuPolicyManager;->mLastUpdateCpuTime:Ljava/util/concurrent/atomic/AtomicLong;

    invoke-virtual {v5, v3, v4}, Ljava/util/concurrent/atomic/AtomicLong;->set(J)V

    .line 198
    new-instance v5, Lcom/miui/server/smartpower/SmartCpuPolicyManager$$ExternalSyntheticLambda0;

    invoke-direct {v5, p0, p1}, Lcom/miui/server/smartpower/SmartCpuPolicyManager$$ExternalSyntheticLambda0;-><init>(Lcom/miui/server/smartpower/SmartCpuPolicyManager;Z)V

    invoke-direct {p0, v5}, Lcom/miui/server/smartpower/SmartCpuPolicyManager;->forAllCpuStats(Ljava/util/function/Consumer;)V

    .line 213
    .end local v3    # "nowTime":J
    monitor-exit v2

    .line 214
    return-wide v0

    .line 213
    :catchall_0
    move-exception v3

    monitor-exit v2
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v3
.end method
