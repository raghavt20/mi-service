class com.miui.server.smartpower.SmartDisplayPolicyManager$H extends android.os.Handler {
	 /* .source "SmartDisplayPolicyManager.java" */
	 /* # annotations */
	 /* .annotation system Ldalvik/annotation/EnclosingClass; */
	 /* value = Lcom/miui/server/smartpower/SmartDisplayPolicyManager; */
} // .end annotation
/* .annotation system Ldalvik/annotation/InnerClass; */
/* accessFlags = 0x2 */
/* name = "H" */
} // .end annotation
/* # instance fields */
final com.miui.server.smartpower.SmartDisplayPolicyManager this$0; //synthetic
/* # direct methods */
 com.miui.server.smartpower.SmartDisplayPolicyManager$H ( ) {
/* .locals 0 */
/* .param p2, "looper" # Landroid/os/Looper; */
/* .line 987 */
this.this$0 = p1;
/* .line 988 */
/* invoke-direct {p0, p2}, Landroid/os/Handler;-><init>(Landroid/os/Looper;)V */
/* .line 989 */
return;
} // .end method
/* # virtual methods */
public void handleMessage ( android.os.Message p0 ) {
/* .locals 2 */
/* .param p1, "msg" # Landroid/os/Message; */
/* .line 993 */
/* invoke-super {p0, p1}, Landroid/os/Handler;->handleMessage(Landroid/os/Message;)V */
/* .line 994 */
/* iget v0, p1, Landroid/os/Message;->what:I */
/* packed-switch v0, :pswitch_data_0 */
/* .line 1003 */
/* :pswitch_0 */
v0 = this.this$0;
com.miui.server.smartpower.SmartDisplayPolicyManager .-$$Nest$mshouldUpdateDisplayDevice ( v0 );
/* .line 1000 */
/* :pswitch_1 */
v0 = this.this$0;
com.miui.server.smartpower.SmartDisplayPolicyManager .-$$Nest$mshouldDownRefreshRate ( v0 );
/* .line 1001 */
/* .line 996 */
/* :pswitch_2 */
v0 = this.obj;
/* check-cast v0, Lcom/miui/server/smartpower/SmartDisplayPolicyManager$ComingInteraction; */
/* .line 997 */
/* .local v0, "obj":Lcom/miui/server/smartpower/SmartDisplayPolicyManager$ComingInteraction; */
v1 = this.this$0;
com.miui.server.smartpower.SmartDisplayPolicyManager .-$$Nest$mshouldUpRefreshRate ( v1,v0 );
/* .line 998 */
/* nop */
/* .line 1007 */
} // .end local v0 # "obj":Lcom/miui/server/smartpower/SmartDisplayPolicyManager$ComingInteraction;
} // :goto_0
return;
/* :pswitch_data_0 */
/* .packed-switch 0x1 */
/* :pswitch_2 */
/* :pswitch_1 */
/* :pswitch_0 */
} // .end packed-switch
} // .end method
