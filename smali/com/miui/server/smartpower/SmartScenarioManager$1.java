class com.miui.server.smartpower.SmartScenarioManager$1 extends android.content.BroadcastReceiver {
	 /* .source "SmartScenarioManager.java" */
	 /* # annotations */
	 /* .annotation system Ldalvik/annotation/EnclosingClass; */
	 /* value = Lcom/miui/server/smartpower/SmartScenarioManager; */
} // .end annotation
/* .annotation system Ldalvik/annotation/InnerClass; */
/* accessFlags = 0x0 */
/* name = null */
} // .end annotation
/* # instance fields */
final com.miui.server.smartpower.SmartScenarioManager this$0; //synthetic
/* # direct methods */
 com.miui.server.smartpower.SmartScenarioManager$1 ( ) {
/* .locals 0 */
/* .param p1, "this$0" # Lcom/miui/server/smartpower/SmartScenarioManager; */
/* .line 161 */
this.this$0 = p1;
/* invoke-direct {p0}, Landroid/content/BroadcastReceiver;-><init>()V */
return;
} // .end method
/* # virtual methods */
public void onReceive ( android.content.Context p0, android.content.Intent p1 ) {
/* .locals 7 */
/* .param p1, "context" # Landroid/content/Context; */
/* .param p2, "intent" # Landroid/content/Intent; */
/* .line 164 */
(( android.content.Intent ) p2 ).getAction ( ); // invoke-virtual {p2}, Landroid/content/Intent;->getAction()Ljava/lang/String;
final String v1 = "android.intent.action.BATTERY_CHANGED"; // const-string v1, "android.intent.action.BATTERY_CHANGED"
v0 = (( java.lang.String ) v0 ).equals ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z
if ( v0 != null) { // if-eqz v0, :cond_2
	 /* .line 165 */
	 final String v0 = "present"; // const-string v0, "present"
	 int v1 = 1; // const/4 v1, 0x1
	 v0 = 	 (( android.content.Intent ) p2 ).getBooleanExtra ( v0, v1 ); // invoke-virtual {p2, v0, v1}, Landroid/content/Intent;->getBooleanExtra(Ljava/lang/String;Z)Z
	 /* .line 166 */
	 /* .local v0, "present":Z */
	 final String v2 = "plugged"; // const-string v2, "plugged"
	 int v3 = 0; // const/4 v3, 0x0
	 v2 = 	 (( android.content.Intent ) p2 ).getIntExtra ( v2, v3 ); // invoke-virtual {p2, v2, v3}, Landroid/content/Intent;->getIntExtra(Ljava/lang/String;I)I
	 if ( v2 != null) { // if-eqz v2, :cond_0
		 /* move v2, v1 */
	 } // :cond_0
	 /* move v2, v3 */
	 /* .line 167 */
	 /* .local v2, "plugged":Z */
} // :goto_0
if ( v0 != null) { // if-eqz v0, :cond_1
	 if ( v2 != null) { // if-eqz v2, :cond_1
	 } // :cond_1
	 /* move v1, v3 */
	 /* .line 168 */
	 /* .local v1, "charging":Z */
} // :goto_1
v3 = this.this$0;
v3 = com.miui.server.smartpower.SmartScenarioManager .-$$Nest$fgetmCharging ( v3 );
/* if-eq v1, v3, :cond_2 */
/* .line 169 */
v3 = this.this$0;
com.miui.server.smartpower.SmartScenarioManager .-$$Nest$fputmCharging ( v3,v1 );
/* .line 170 */
v3 = this.this$0;
int v4 = 0; // const/4 v4, 0x0
v5 = com.miui.server.smartpower.SmartScenarioManager .-$$Nest$fgetmCharging ( v3 );
/* const/16 v6, 0x800 */
(( com.miui.server.smartpower.SmartScenarioManager ) v3 ).onAppActionChanged ( v6, v4, v5 ); // invoke-virtual {v3, v6, v4, v5}, Lcom/miui/server/smartpower/SmartScenarioManager;->onAppActionChanged(ILcom/android/server/am/AppStateManager$AppState;Z)V
/* .line 173 */
} // .end local v0 # "present":Z
} // .end local v1 # "charging":Z
} // .end local v2 # "plugged":Z
} // :cond_2
return;
} // .end method
