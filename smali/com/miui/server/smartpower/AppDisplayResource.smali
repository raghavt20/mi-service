.class Lcom/miui/server/smartpower/AppDisplayResource;
.super Lcom/miui/server/smartpower/AppPowerResource;
.source "AppDisplayResource.java"

# interfaces
.implements Landroid/hardware/display/DisplayManager$DisplayListener;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/miui/server/smartpower/AppDisplayResource$DisplayRecord;
    }
.end annotation


# instance fields
.field private mCastingWhitelists:Landroid/util/ArraySet;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/util/ArraySet<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private final mDisplayRecordMap:Landroid/util/ArrayMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/util/ArrayMap<",
            "Ljava/lang/Integer;",
            "Lcom/miui/server/smartpower/AppDisplayResource$DisplayRecord;",
            ">;"
        }
    .end annotation
.end field

.field private mDms:Landroid/hardware/display/DisplayManager;

.field private mHandler:Landroid/os/Handler;

.field private mWifiP2pConnected:Z

.field private final mWifiP2pReceiver:Landroid/content/BroadcastReceiver;


# direct methods
.method static bridge synthetic -$$Nest$fgetmCastingWhitelists(Lcom/miui/server/smartpower/AppDisplayResource;)Landroid/util/ArraySet;
    .locals 0

    iget-object p0, p0, Lcom/miui/server/smartpower/AppDisplayResource;->mCastingWhitelists:Landroid/util/ArraySet;

    return-object p0
.end method

.method static bridge synthetic -$$Nest$fgetmDisplayRecordMap(Lcom/miui/server/smartpower/AppDisplayResource;)Landroid/util/ArrayMap;
    .locals 0

    iget-object p0, p0, Lcom/miui/server/smartpower/AppDisplayResource;->mDisplayRecordMap:Landroid/util/ArrayMap;

    return-object p0
.end method

.method static bridge synthetic -$$Nest$fgetmWifiP2pConnected(Lcom/miui/server/smartpower/AppDisplayResource;)Z
    .locals 0

    iget-boolean p0, p0, Lcom/miui/server/smartpower/AppDisplayResource;->mWifiP2pConnected:Z

    return p0
.end method

.method static bridge synthetic -$$Nest$fputmWifiP2pConnected(Lcom/miui/server/smartpower/AppDisplayResource;Z)V
    .locals 0

    iput-boolean p1, p0, Lcom/miui/server/smartpower/AppDisplayResource;->mWifiP2pConnected:Z

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/os/Looper;)V
    .locals 4
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "looper"    # Landroid/os/Looper;

    .line 33
    invoke-direct {p0}, Lcom/miui/server/smartpower/AppPowerResource;-><init>()V

    .line 27
    new-instance v0, Landroid/util/ArrayMap;

    invoke-direct {v0}, Landroid/util/ArrayMap;-><init>()V

    iput-object v0, p0, Lcom/miui/server/smartpower/AppDisplayResource;->mDisplayRecordMap:Landroid/util/ArrayMap;

    .line 28
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/miui/server/smartpower/AppDisplayResource;->mDms:Landroid/hardware/display/DisplayManager;

    .line 30
    new-instance v0, Landroid/util/ArraySet;

    invoke-direct {v0}, Landroid/util/ArraySet;-><init>()V

    iput-object v0, p0, Lcom/miui/server/smartpower/AppDisplayResource;->mCastingWhitelists:Landroid/util/ArraySet;

    .line 31
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/miui/server/smartpower/AppDisplayResource;->mWifiP2pConnected:Z

    .line 153
    new-instance v0, Lcom/miui/server/smartpower/AppDisplayResource$2;

    invoke-direct {v0, p0}, Lcom/miui/server/smartpower/AppDisplayResource$2;-><init>(Lcom/miui/server/smartpower/AppDisplayResource;)V

    iput-object v0, p0, Lcom/miui/server/smartpower/AppDisplayResource;->mWifiP2pReceiver:Landroid/content/BroadcastReceiver;

    .line 34
    const/4 v1, 0x7

    iput v1, p0, Lcom/miui/server/smartpower/AppDisplayResource;->mType:I

    .line 35
    new-instance v1, Landroid/os/Handler;

    invoke-direct {v1, p2}, Landroid/os/Handler;-><init>(Landroid/os/Looper;)V

    iput-object v1, p0, Lcom/miui/server/smartpower/AppDisplayResource;->mHandler:Landroid/os/Handler;

    .line 36
    const-string v1, "display"

    invoke-virtual {p1, v1}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/hardware/display/DisplayManager;

    iput-object v1, p0, Lcom/miui/server/smartpower/AppDisplayResource;->mDms:Landroid/hardware/display/DisplayManager;

    .line 37
    new-instance v2, Landroid/os/Handler;

    invoke-direct {v2, p2}, Landroid/os/Handler;-><init>(Landroid/os/Looper;)V

    invoke-virtual {v1, p0, v2}, Landroid/hardware/display/DisplayManager;->registerDisplayListener(Landroid/hardware/display/DisplayManager$DisplayListener;Landroid/os/Handler;)V

    .line 39
    invoke-virtual {p1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x110300cb

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getStringArray(I)[Ljava/lang/String;

    move-result-object v1

    .line 41
    .local v1, "packges":[Ljava/lang/String;
    iget-object v2, p0, Lcom/miui/server/smartpower/AppDisplayResource;->mCastingWhitelists:Landroid/util/ArraySet;

    invoke-static {v1}, Ljava/util/Arrays;->asList([Ljava/lang/Object;)Ljava/util/List;

    move-result-object v3

    invoke-virtual {v2, v3}, Landroid/util/ArraySet;->addAll(Ljava/util/Collection;)Z

    .line 43
    new-instance v2, Landroid/content/IntentFilter;

    invoke-direct {v2}, Landroid/content/IntentFilter;-><init>()V

    .line 44
    .local v2, "intentFilter":Landroid/content/IntentFilter;
    const-string v3, "android.net.wifi.p2p.CONNECTION_STATE_CHANGE"

    invoke-virtual {v2, v3}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    .line 45
    invoke-virtual {p1, v0, v2}, Landroid/content/Context;->registerReceiver(Landroid/content/BroadcastReceiver;Landroid/content/IntentFilter;)Landroid/content/Intent;

    .line 46
    return-void
.end method

.method private initializeCurrentDisplays()V
    .locals 4

    .line 126
    iget-object v0, p0, Lcom/miui/server/smartpower/AppDisplayResource;->mDms:Landroid/hardware/display/DisplayManager;

    invoke-virtual {v0}, Landroid/hardware/display/DisplayManager;->getDisplays()[Landroid/view/Display;

    move-result-object v0

    .line 127
    .local v0, "displays":[Landroid/view/Display;
    array-length v1, v0

    const/4 v2, 0x0

    :goto_0
    if-ge v2, v1, :cond_0

    aget-object v3, v0, v2

    .line 128
    .local v3, "display":Landroid/view/Display;
    invoke-direct {p0, v3}, Lcom/miui/server/smartpower/AppDisplayResource;->onDisplayAdded(Landroid/view/Display;)V

    .line 127
    .end local v3    # "display":Landroid/view/Display;
    add-int/lit8 v2, v2, 0x1

    goto :goto_0

    .line 130
    :cond_0
    return-void
.end method

.method private onDisplayAdded(Landroid/view/Display;)V
    .locals 5
    .param p1, "display"    # Landroid/view/Display;

    .line 133
    sget-boolean v0, Lcom/miui/server/smartpower/AppDisplayResource;->DEBUG:Z

    if-eqz v0, :cond_0

    .line 134
    const-string v0, "SmartPower.AppResource"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Display added "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Slog;->w(Ljava/lang/String;Ljava/lang/String;)I

    .line 136
    :cond_0
    if-eqz p1, :cond_3

    .line 137
    iget-object v0, p0, Lcom/miui/server/smartpower/AppDisplayResource;->mDisplayRecordMap:Landroid/util/ArrayMap;

    monitor-enter v0

    .line 138
    :try_start_0
    invoke-virtual {p1}, Landroid/view/Display;->getOwnerUid()I

    move-result v1

    .line 139
    .local v1, "uid":I
    if-nez v1, :cond_1

    .line 140
    const/16 v1, 0x3e8

    .line 142
    :cond_1
    iget-object v2, p0, Lcom/miui/server/smartpower/AppDisplayResource;->mDisplayRecordMap:Landroid/util/ArrayMap;

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    invoke-virtual {v2, v3}, Landroid/util/ArrayMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/miui/server/smartpower/AppDisplayResource$DisplayRecord;

    .line 143
    .local v2, "displayRecord":Lcom/miui/server/smartpower/AppDisplayResource$DisplayRecord;
    if-nez v2, :cond_2

    .line 144
    new-instance v3, Lcom/miui/server/smartpower/AppDisplayResource$DisplayRecord;

    invoke-direct {v3, p0, v1}, Lcom/miui/server/smartpower/AppDisplayResource$DisplayRecord;-><init>(Lcom/miui/server/smartpower/AppDisplayResource;I)V

    move-object v2, v3

    .line 145
    iget-object v3, p0, Lcom/miui/server/smartpower/AppDisplayResource;->mDisplayRecordMap:Landroid/util/ArrayMap;

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    invoke-virtual {v3, v4, v2}, Landroid/util/ArrayMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 147
    :cond_2
    invoke-virtual {p1}, Landroid/view/Display;->getDisplayId()I

    move-result v3

    invoke-virtual {v2, v3, p1}, Lcom/miui/server/smartpower/AppDisplayResource$DisplayRecord;->onDisplayAdded(ILandroid/view/Display;)V

    .line 148
    .end local v1    # "uid":I
    .end local v2    # "displayRecord":Lcom/miui/server/smartpower/AppDisplayResource$DisplayRecord;
    monitor-exit v0

    goto :goto_0

    :catchall_0
    move-exception v1

    monitor-exit v0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v1

    .line 151
    :cond_3
    :goto_0
    return-void
.end method


# virtual methods
.method public getActiveUids()Ljava/util/ArrayList;
    .locals 3

    .line 71
    iget-object v0, p0, Lcom/miui/server/smartpower/AppDisplayResource;->mDisplayRecordMap:Landroid/util/ArrayMap;

    monitor-enter v0

    .line 72
    :try_start_0
    new-instance v1, Ljava/util/ArrayList;

    iget-object v2, p0, Lcom/miui/server/smartpower/AppDisplayResource;->mDisplayRecordMap:Landroid/util/ArrayMap;

    invoke-virtual {v2}, Landroid/util/ArrayMap;->keySet()Ljava/util/Set;

    move-result-object v2

    invoke-direct {v1, v2}, Ljava/util/ArrayList;-><init>(Ljava/util/Collection;)V

    monitor-exit v0

    return-object v1

    .line 73
    :catchall_0
    move-exception v1

    monitor-exit v0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v1
.end method

.method public init()V
    .locals 0

    .line 50
    invoke-direct {p0}, Lcom/miui/server/smartpower/AppDisplayResource;->initializeCurrentDisplays()V

    .line 51
    return-void
.end method

.method public isAppResourceActive(I)Z
    .locals 3
    .param p1, "uid"    # I

    .line 78
    iget-object v0, p0, Lcom/miui/server/smartpower/AppDisplayResource;->mDisplayRecordMap:Landroid/util/ArrayMap;

    monitor-enter v0

    .line 79
    :try_start_0
    iget-object v1, p0, Lcom/miui/server/smartpower/AppDisplayResource;->mDisplayRecordMap:Landroid/util/ArrayMap;

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v1, v2}, Landroid/util/ArrayMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/miui/server/smartpower/AppDisplayResource$DisplayRecord;

    .line 80
    .local v1, "record":Lcom/miui/server/smartpower/AppDisplayResource$DisplayRecord;
    if-eqz v1, :cond_0

    invoke-virtual {v1}, Lcom/miui/server/smartpower/AppDisplayResource$DisplayRecord;->isActive()Z

    move-result v2

    if-eqz v2, :cond_0

    .line 81
    monitor-exit v0

    const/4 v0, 0x1

    return v0

    .line 83
    .end local v1    # "record":Lcom/miui/server/smartpower/AppDisplayResource$DisplayRecord;
    :cond_0
    monitor-exit v0

    .line 84
    const/4 v0, 0x0

    return v0

    .line 83
    :catchall_0
    move-exception v1

    monitor-exit v0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v1
.end method

.method public isAppResourceActive(II)Z
    .locals 1
    .param p1, "uid"    # I
    .param p2, "pid"    # I

    .line 89
    invoke-virtual {p0, p1}, Lcom/miui/server/smartpower/AppDisplayResource;->isAppResourceActive(I)Z

    move-result v0

    return v0
.end method

.method public onDisplayAdded(I)V
    .locals 1
    .param p1, "displayId"    # I

    .line 100
    iget-object v0, p0, Lcom/miui/server/smartpower/AppDisplayResource;->mDms:Landroid/hardware/display/DisplayManager;

    invoke-virtual {v0, p1}, Landroid/hardware/display/DisplayManager;->getDisplay(I)Landroid/view/Display;

    move-result-object v0

    .line 101
    .local v0, "display":Landroid/view/Display;
    invoke-direct {p0, v0}, Lcom/miui/server/smartpower/AppDisplayResource;->onDisplayAdded(Landroid/view/Display;)V

    .line 102
    return-void
.end method

.method public onDisplayChanged(I)V
    .locals 0
    .param p1, "displayId"    # I

    .line 123
    return-void
.end method

.method public onDisplayRemoved(I)V
    .locals 5
    .param p1, "displayId"    # I

    .line 106
    iget-object v0, p0, Lcom/miui/server/smartpower/AppDisplayResource;->mDms:Landroid/hardware/display/DisplayManager;

    invoke-virtual {v0, p1}, Landroid/hardware/display/DisplayManager;->getDisplay(I)Landroid/view/Display;

    move-result-object v0

    .line 107
    .local v0, "display":Landroid/view/Display;
    sget-boolean v1, Lcom/miui/server/smartpower/AppDisplayResource;->DEBUG:Z

    if-eqz v1, :cond_0

    .line 108
    const-string v1, "SmartPower.AppResource"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "Display removed "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Slog;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 110
    :cond_0
    iget-object v1, p0, Lcom/miui/server/smartpower/AppDisplayResource;->mDisplayRecordMap:Landroid/util/ArrayMap;

    monitor-enter v1

    .line 111
    :try_start_0
    iget-object v2, p0, Lcom/miui/server/smartpower/AppDisplayResource;->mDisplayRecordMap:Landroid/util/ArrayMap;

    invoke-virtual {v2}, Landroid/util/ArrayMap;->values()Ljava/util/Collection;

    move-result-object v2

    invoke-interface {v2}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_3

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/miui/server/smartpower/AppDisplayResource$DisplayRecord;

    .line 112
    .local v3, "record":Lcom/miui/server/smartpower/AppDisplayResource$DisplayRecord;
    invoke-virtual {v3, p1}, Lcom/miui/server/smartpower/AppDisplayResource$DisplayRecord;->onDisplayRemoved(I)Z

    move-result v4

    if-eqz v4, :cond_2

    .line 113
    invoke-virtual {v3}, Lcom/miui/server/smartpower/AppDisplayResource$DisplayRecord;->isActive()Z

    move-result v2

    if-nez v2, :cond_1

    .line 114
    iget-object v2, p0, Lcom/miui/server/smartpower/AppDisplayResource;->mDisplayRecordMap:Landroid/util/ArrayMap;

    invoke-static {v3}, Lcom/miui/server/smartpower/AppDisplayResource$DisplayRecord;->-$$Nest$fgetmUid(Lcom/miui/server/smartpower/AppDisplayResource$DisplayRecord;)I

    move-result v4

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    invoke-virtual {v2, v4}, Landroid/util/ArrayMap;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    .line 116
    :cond_1
    monitor-exit v1

    return-void

    .line 118
    .end local v3    # "record":Lcom/miui/server/smartpower/AppDisplayResource$DisplayRecord;
    :cond_2
    goto :goto_0

    .line 119
    :cond_3
    monitor-exit v1

    .line 120
    return-void

    .line 119
    :catchall_0
    move-exception v2

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v2
.end method

.method public registerCallback(Lcom/miui/server/smartpower/AppPowerResource$IAppPowerResourceCallback;I)V
    .locals 2
    .param p1, "callback"    # Lcom/miui/server/smartpower/AppPowerResource$IAppPowerResourceCallback;
    .param p2, "uid"    # I

    .line 55
    invoke-super {p0, p1, p2}, Lcom/miui/server/smartpower/AppPowerResource;->registerCallback(Lcom/miui/server/smartpower/AppPowerResource$IAppPowerResourceCallback;I)V

    .line 56
    iget-object v0, p0, Lcom/miui/server/smartpower/AppDisplayResource;->mHandler:Landroid/os/Handler;

    new-instance v1, Lcom/miui/server/smartpower/AppDisplayResource$1;

    invoke-direct {v1, p0, p2}, Lcom/miui/server/smartpower/AppDisplayResource$1;-><init>(Lcom/miui/server/smartpower/AppDisplayResource;I)V

    invoke-virtual {v0, v1}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    .line 67
    return-void
.end method

.method public releaseAppPowerResource(I)V
    .locals 0
    .param p1, "pid"    # I

    .line 93
    return-void
.end method

.method public resumeAppPowerResource(I)V
    .locals 0
    .param p1, "pid"    # I

    .line 96
    return-void
.end method
