class com.miui.server.smartpower.AppGPSResource extends com.miui.server.smartpower.AppPowerResource {
	 /* .source "AppGPSResource.java" */
	 /* # annotations */
	 /* .annotation system Ldalvik/annotation/MemberClasses; */
	 /* value = { */
	 /* Lcom/miui/server/smartpower/AppGPSResource$GpsRecord; */
	 /* } */
} // .end annotation
/* # instance fields */
private final android.util.ArrayMap mGpsRecordMap;
/* .annotation system Ldalvik/annotation/Signature; */
/* value = { */
/* "Landroid/util/ArrayMap<", */
/* "Ljava/lang/Integer;", */
/* "Lcom/miui/server/smartpower/AppGPSResource$GpsRecord;", */
/* ">;" */
/* } */
} // .end annotation
} // .end field
/* # direct methods */
public com.miui.server.smartpower.AppGPSResource ( ) {
/* .locals 1 */
/* .param p1, "context" # Landroid/content/Context; */
/* .param p2, "looper" # Landroid/os/Looper; */
/* .line 18 */
/* invoke-direct {p0}, Lcom/miui/server/smartpower/AppPowerResource;-><init>()V */
/* .line 16 */
/* new-instance v0, Landroid/util/ArrayMap; */
/* invoke-direct {v0}, Landroid/util/ArrayMap;-><init>()V */
this.mGpsRecordMap = v0;
/* .line 19 */
int v0 = 3; // const/4 v0, 0x3
/* iput v0, p0, Lcom/miui/server/smartpower/AppGPSResource;->mType:I */
/* .line 20 */
return;
} // .end method
/* # virtual methods */
public java.util.ArrayList getActiveUids ( ) {
/* .locals 3 */
/* .line 24 */
v0 = this.mGpsRecordMap;
/* monitor-enter v0 */
/* .line 25 */
try { // :try_start_0
/* new-instance v1, Ljava/util/ArrayList; */
v2 = this.mGpsRecordMap;
(( android.util.ArrayMap ) v2 ).keySet ( ); // invoke-virtual {v2}, Landroid/util/ArrayMap;->keySet()Ljava/util/Set;
/* invoke-direct {v1, v2}, Ljava/util/ArrayList;-><init>(Ljava/util/Collection;)V */
/* monitor-exit v0 */
/* .line 26 */
/* :catchall_0 */
/* move-exception v1 */
/* monitor-exit v0 */
/* :try_end_0 */
/* .catchall {:try_start_0 ..:try_end_0} :catchall_0 */
/* throw v1 */
} // .end method
public Boolean isAppResourceActive ( Integer p0 ) {
/* .locals 4 */
/* .param p1, "uid" # I */
/* .line 31 */
v0 = this.mGpsRecordMap;
/* monitor-enter v0 */
/* .line 32 */
int v1 = 0; // const/4 v1, 0x0
/* .local v1, "i":I */
} // :goto_0
try { // :try_start_0
v2 = this.mGpsRecordMap;
v2 = (( android.util.ArrayMap ) v2 ).size ( ); // invoke-virtual {v2}, Landroid/util/ArrayMap;->size()I
/* if-ge v1, v2, :cond_1 */
/* .line 33 */
v2 = this.mGpsRecordMap;
(( android.util.ArrayMap ) v2 ).valueAt ( v1 ); // invoke-virtual {v2, v1}, Landroid/util/ArrayMap;->valueAt(I)Ljava/lang/Object;
/* check-cast v2, Lcom/miui/server/smartpower/AppGPSResource$GpsRecord; */
/* .line 34 */
/* .local v2, "record":Lcom/miui/server/smartpower/AppGPSResource$GpsRecord; */
if ( v2 != null) { // if-eqz v2, :cond_0
	 v3 = 	 com.miui.server.smartpower.AppGPSResource$GpsRecord .-$$Nest$fgetmUid ( v2 );
	 /* if-ne v3, p1, :cond_0 */
	 v3 = 	 (( com.miui.server.smartpower.AppGPSResource$GpsRecord ) v2 ).isActive ( ); // invoke-virtual {v2}, Lcom/miui/server/smartpower/AppGPSResource$GpsRecord;->isActive()Z
	 if ( v3 != null) { // if-eqz v3, :cond_0
		 /* .line 35 */
		 /* monitor-exit v0 */
		 int v0 = 1; // const/4 v0, 0x1
		 /* .line 32 */
	 } // .end local v2 # "record":Lcom/miui/server/smartpower/AppGPSResource$GpsRecord;
} // :cond_0
/* add-int/lit8 v1, v1, 0x1 */
/* .line 38 */
} // .end local v1 # "i":I
} // :cond_1
/* monitor-exit v0 */
/* .line 39 */
int v0 = 0; // const/4 v0, 0x0
/* .line 38 */
/* :catchall_0 */
/* move-exception v1 */
/* monitor-exit v0 */
/* :try_end_0 */
/* .catchall {:try_start_0 ..:try_end_0} :catchall_0 */
/* throw v1 */
} // .end method
public Boolean isAppResourceActive ( Integer p0, Integer p1 ) {
/* .locals 3 */
/* .param p1, "uid" # I */
/* .param p2, "pid" # I */
/* .line 44 */
v0 = this.mGpsRecordMap;
/* monitor-enter v0 */
/* .line 45 */
try { // :try_start_0
v1 = this.mGpsRecordMap;
java.lang.Integer .valueOf ( p2 );
(( android.util.ArrayMap ) v1 ).get ( v2 ); // invoke-virtual {v1, v2}, Landroid/util/ArrayMap;->get(Ljava/lang/Object;)Ljava/lang/Object;
/* check-cast v1, Lcom/miui/server/smartpower/AppGPSResource$GpsRecord; */
/* .line 46 */
/* .local v1, "record":Lcom/miui/server/smartpower/AppGPSResource$GpsRecord; */
if ( v1 != null) { // if-eqz v1, :cond_0
/* .line 47 */
v2 = (( com.miui.server.smartpower.AppGPSResource$GpsRecord ) v1 ).isActive ( ); // invoke-virtual {v1}, Lcom/miui/server/smartpower/AppGPSResource$GpsRecord;->isActive()Z
/* monitor-exit v0 */
/* .line 49 */
} // .end local v1 # "record":Lcom/miui/server/smartpower/AppGPSResource$GpsRecord;
} // :cond_0
/* monitor-exit v0 */
/* .line 50 */
int v0 = 0; // const/4 v0, 0x0
/* .line 49 */
/* :catchall_0 */
/* move-exception v1 */
/* monitor-exit v0 */
/* :try_end_0 */
/* .catchall {:try_start_0 ..:try_end_0} :catchall_0 */
/* throw v1 */
} // .end method
public void onAquireLocation ( Integer p0, Integer p1, android.location.ILocationListener p2 ) {
/* .locals 4 */
/* .param p1, "uid" # I */
/* .param p2, "pid" # I */
/* .param p3, "listener" # Landroid/location/ILocationListener; */
/* .line 60 */
/* if-nez p3, :cond_0 */
return;
/* .line 61 */
} // :cond_0
v0 = this.mGpsRecordMap;
/* monitor-enter v0 */
/* .line 62 */
try { // :try_start_0
v1 = this.mGpsRecordMap;
java.lang.Integer .valueOf ( p2 );
(( android.util.ArrayMap ) v1 ).get ( v2 ); // invoke-virtual {v1, v2}, Landroid/util/ArrayMap;->get(Ljava/lang/Object;)Ljava/lang/Object;
/* check-cast v1, Lcom/miui/server/smartpower/AppGPSResource$GpsRecord; */
/* .line 63 */
/* .local v1, "gpsRecord":Lcom/miui/server/smartpower/AppGPSResource$GpsRecord; */
/* if-nez v1, :cond_1 */
/* .line 64 */
/* new-instance v2, Lcom/miui/server/smartpower/AppGPSResource$GpsRecord; */
/* invoke-direct {v2, p0, p1, p2}, Lcom/miui/server/smartpower/AppGPSResource$GpsRecord;-><init>(Lcom/miui/server/smartpower/AppGPSResource;II)V */
/* move-object v1, v2 */
/* .line 65 */
v2 = this.mGpsRecordMap;
java.lang.Integer .valueOf ( p2 );
(( android.util.ArrayMap ) v2 ).put ( v3, v1 ); // invoke-virtual {v2, v3, v1}, Landroid/util/ArrayMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;
/* .line 66 */
/* new-instance v2, Ljava/lang/StringBuilder; */
/* invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V */
final String v3 = "gps u:"; // const-string v3, "gps u:"
(( java.lang.StringBuilder ) v2 ).append ( v3 ); // invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v2 ).append ( p1 ); // invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;
final String v3 = " p:"; // const-string v3, " p:"
(( java.lang.StringBuilder ) v2 ).append ( v3 ); // invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v2 ).append ( p2 ); // invoke-virtual {v2, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;
final String v3 = " s:true"; // const-string v3, " s:true"
(( java.lang.StringBuilder ) v2 ).append ( v3 ); // invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v2 ).toString ( ); // invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
/* const v3, 0x15ff2 */
android.util.EventLog .writeEvent ( v3,v2 );
/* .line 68 */
int v2 = 1; // const/4 v2, 0x1
/* const/16 v3, 0x40 */
(( com.miui.server.smartpower.AppGPSResource ) p0 ).reportResourceStatus ( p1, p2, v2, v3 ); // invoke-virtual {p0, p1, p2, v2, v3}, Lcom/miui/server/smartpower/AppGPSResource;->reportResourceStatus(IIZI)V
/* .line 71 */
} // :cond_1
(( com.miui.server.smartpower.AppGPSResource$GpsRecord ) v1 ).onAquireLocation ( p3 ); // invoke-virtual {v1, p3}, Lcom/miui/server/smartpower/AppGPSResource$GpsRecord;->onAquireLocation(Landroid/location/ILocationListener;)V
/* .line 72 */
} // .end local v1 # "gpsRecord":Lcom/miui/server/smartpower/AppGPSResource$GpsRecord;
/* monitor-exit v0 */
/* .line 73 */
return;
/* .line 72 */
/* :catchall_0 */
/* move-exception v1 */
/* monitor-exit v0 */
/* :try_end_0 */
/* .catchall {:try_start_0 ..:try_end_0} :catchall_0 */
/* throw v1 */
} // .end method
public void onReleaseLocation ( Integer p0, Integer p1, android.location.ILocationListener p2 ) {
/* .locals 4 */
/* .param p1, "uid" # I */
/* .param p2, "pid" # I */
/* .param p3, "listener" # Landroid/location/ILocationListener; */
/* .line 76 */
/* if-nez p3, :cond_0 */
return;
/* .line 77 */
} // :cond_0
v0 = this.mGpsRecordMap;
/* monitor-enter v0 */
/* .line 78 */
try { // :try_start_0
v1 = this.mGpsRecordMap;
java.lang.Integer .valueOf ( p2 );
(( android.util.ArrayMap ) v1 ).get ( v2 ); // invoke-virtual {v1, v2}, Landroid/util/ArrayMap;->get(Ljava/lang/Object;)Ljava/lang/Object;
/* check-cast v1, Lcom/miui/server/smartpower/AppGPSResource$GpsRecord; */
/* .line 79 */
/* .local v1, "gpsRecord":Lcom/miui/server/smartpower/AppGPSResource$GpsRecord; */
/* if-nez v1, :cond_1 */
/* monitor-exit v0 */
return;
/* .line 80 */
} // :cond_1
(( com.miui.server.smartpower.AppGPSResource$GpsRecord ) v1 ).onReleaseLocation ( p3 ); // invoke-virtual {v1, p3}, Lcom/miui/server/smartpower/AppGPSResource$GpsRecord;->onReleaseLocation(Landroid/location/ILocationListener;)V
/* .line 81 */
v2 = (( com.miui.server.smartpower.AppGPSResource$GpsRecord ) v1 ).isActive ( ); // invoke-virtual {v1}, Lcom/miui/server/smartpower/AppGPSResource$GpsRecord;->isActive()Z
/* if-nez v2, :cond_2 */
/* .line 82 */
v2 = this.mGpsRecordMap;
java.lang.Integer .valueOf ( p2 );
(( android.util.ArrayMap ) v2 ).remove ( v3 ); // invoke-virtual {v2, v3}, Landroid/util/ArrayMap;->remove(Ljava/lang/Object;)Ljava/lang/Object;
/* .line 83 */
/* new-instance v2, Ljava/lang/StringBuilder; */
/* invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V */
final String v3 = "gps u:"; // const-string v3, "gps u:"
(( java.lang.StringBuilder ) v2 ).append ( v3 ); // invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v2 ).append ( p1 ); // invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;
final String v3 = " p:"; // const-string v3, " p:"
(( java.lang.StringBuilder ) v2 ).append ( v3 ); // invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v2 ).append ( p2 ); // invoke-virtual {v2, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;
final String v3 = " s:false"; // const-string v3, " s:false"
(( java.lang.StringBuilder ) v2 ).append ( v3 ); // invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v2 ).toString ( ); // invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
/* const v3, 0x15ff2 */
android.util.EventLog .writeEvent ( v3,v2 );
/* .line 85 */
int v2 = 0; // const/4 v2, 0x0
/* const/16 v3, 0x40 */
(( com.miui.server.smartpower.AppGPSResource ) p0 ).reportResourceStatus ( p1, p2, v2, v3 ); // invoke-virtual {p0, p1, p2, v2, v3}, Lcom/miui/server/smartpower/AppGPSResource;->reportResourceStatus(IIZI)V
/* .line 88 */
} // .end local v1 # "gpsRecord":Lcom/miui/server/smartpower/AppGPSResource$GpsRecord;
} // :cond_2
/* monitor-exit v0 */
/* .line 89 */
return;
/* .line 88 */
/* :catchall_0 */
/* move-exception v1 */
/* monitor-exit v0 */
/* :try_end_0 */
/* .catchall {:try_start_0 ..:try_end_0} :catchall_0 */
/* throw v1 */
} // .end method
public void releaseAppPowerResource ( Integer p0 ) {
/* .locals 0 */
/* .param p1, "pid" # I */
/* .line 54 */
return;
} // .end method
public void resumeAppPowerResource ( Integer p0 ) {
/* .locals 0 */
/* .param p1, "pid" # I */
/* .line 57 */
return;
} // .end method
