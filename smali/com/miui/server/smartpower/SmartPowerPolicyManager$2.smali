.class Lcom/miui/server/smartpower/SmartPowerPolicyManager$2;
.super Ljava/lang/Object;
.source "SmartPowerPolicyManager.java"

# interfaces
.implements Ljava/lang/Runnable;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/miui/server/smartpower/SmartPowerPolicyManager;->hibernationProcess(Lcom/android/server/am/AppStateManager$AppState$RunningProcess;Ljava/lang/String;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/miui/server/smartpower/SmartPowerPolicyManager;

.field final synthetic val$proc:Lcom/android/server/am/AppStateManager$AppState$RunningProcess;

.field final synthetic val$reason:Ljava/lang/String;


# direct methods
.method constructor <init>(Lcom/miui/server/smartpower/SmartPowerPolicyManager;Lcom/android/server/am/AppStateManager$AppState$RunningProcess;Ljava/lang/String;)V
    .locals 0
    .param p1, "this$0"    # Lcom/miui/server/smartpower/SmartPowerPolicyManager;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .line 846
    iput-object p1, p0, Lcom/miui/server/smartpower/SmartPowerPolicyManager$2;->this$0:Lcom/miui/server/smartpower/SmartPowerPolicyManager;

    iput-object p2, p0, Lcom/miui/server/smartpower/SmartPowerPolicyManager$2;->val$proc:Lcom/android/server/am/AppStateManager$AppState$RunningProcess;

    iput-object p3, p0, Lcom/miui/server/smartpower/SmartPowerPolicyManager$2;->val$reason:Ljava/lang/String;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public run()V
    .locals 7

    .line 849
    const-string v0, "hibernationProcess"

    const-wide/32 v1, 0x20000

    invoke-static {v1, v2, v0}, Landroid/os/Trace;->traceBegin(JLjava/lang/String;)V

    .line 850
    iget-object v0, p0, Lcom/miui/server/smartpower/SmartPowerPolicyManager$2;->val$proc:Lcom/android/server/am/AppStateManager$AppState$RunningProcess;

    invoke-virtual {v0}, Lcom/android/server/am/AppStateManager$AppState$RunningProcess;->updatePss()V

    .line 851
    iget-object v0, p0, Lcom/miui/server/smartpower/SmartPowerPolicyManager$2;->this$0:Lcom/miui/server/smartpower/SmartPowerPolicyManager;

    iget-object v3, p0, Lcom/miui/server/smartpower/SmartPowerPolicyManager$2;->val$proc:Lcom/android/server/am/AppStateManager$AppState$RunningProcess;

    invoke-virtual {v3}, Lcom/android/server/am/AppStateManager$AppState$RunningProcess;->getUid()I

    move-result v3

    iget-object v4, p0, Lcom/miui/server/smartpower/SmartPowerPolicyManager$2;->val$proc:Lcom/android/server/am/AppStateManager$AppState$RunningProcess;

    invoke-virtual {v4}, Lcom/android/server/am/AppStateManager$AppState$RunningProcess;->getPid()I

    move-result v4

    iget-object v5, p0, Lcom/miui/server/smartpower/SmartPowerPolicyManager$2;->val$proc:Lcom/android/server/am/AppStateManager$AppState$RunningProcess;

    invoke-virtual {v5}, Lcom/android/server/am/AppStateManager$AppState$RunningProcess;->getProcessName()Ljava/lang/String;

    move-result-object v5

    iget-object v6, p0, Lcom/miui/server/smartpower/SmartPowerPolicyManager$2;->val$reason:Ljava/lang/String;

    invoke-static {v0, v3, v4, v5, v6}, Lcom/miui/server/smartpower/SmartPowerPolicyManager;->-$$Nest$mforzenProcess(Lcom/miui/server/smartpower/SmartPowerPolicyManager;IILjava/lang/String;Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 852
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "power hibernation ("

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v3, p0, Lcom/miui/server/smartpower/SmartPowerPolicyManager$2;->val$proc:Lcom/android/server/am/AppStateManager$AppState$RunningProcess;

    .line 853
    invoke-virtual {v3}, Lcom/android/server/am/AppStateManager$AppState$RunningProcess;->getPid()I

    move-result v3

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v3, ")"

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v3, p0, Lcom/miui/server/smartpower/SmartPowerPolicyManager$2;->val$proc:Lcom/android/server/am/AppStateManager$AppState$RunningProcess;

    .line 854
    invoke-virtual {v3}, Lcom/android/server/am/AppStateManager$AppState$RunningProcess;->getProcessName()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v3, "/"

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v3, p0, Lcom/miui/server/smartpower/SmartPowerPolicyManager$2;->val$proc:Lcom/android/server/am/AppStateManager$AppState$RunningProcess;

    invoke-virtual {v3}, Lcom/android/server/am/AppStateManager$AppState$RunningProcess;->getUid()I

    move-result v3

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v3, " adj "

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v3, p0, Lcom/miui/server/smartpower/SmartPowerPolicyManager$2;->val$proc:Lcom/android/server/am/AppStateManager$AppState$RunningProcess;

    .line 855
    invoke-virtual {v3}, Lcom/android/server/am/AppStateManager$AppState$RunningProcess;->getAdj()I

    move-result v3

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    .line 852
    const v3, 0x15ff2

    invoke-static {v3, v0}, Landroid/util/EventLog;->writeEvent(ILjava/lang/String;)I

    .line 857
    :cond_0
    iget-object v0, p0, Lcom/miui/server/smartpower/SmartPowerPolicyManager$2;->this$0:Lcom/miui/server/smartpower/SmartPowerPolicyManager;

    iget-object v3, p0, Lcom/miui/server/smartpower/SmartPowerPolicyManager$2;->val$proc:Lcom/android/server/am/AppStateManager$AppState$RunningProcess;

    invoke-virtual {v3}, Lcom/android/server/am/AppStateManager$AppState$RunningProcess;->getPackageName()Ljava/lang/String;

    move-result-object v3

    iget-object v4, p0, Lcom/miui/server/smartpower/SmartPowerPolicyManager$2;->val$proc:Lcom/android/server/am/AppStateManager$AppState$RunningProcess;

    invoke-static {v0, v3, v4}, Lcom/miui/server/smartpower/SmartPowerPolicyManager;->-$$Nest$mcompactProcess(Lcom/miui/server/smartpower/SmartPowerPolicyManager;Ljava/lang/String;Lcom/android/server/am/AppStateManager$AppState$RunningProcess;)V

    .line 858
    invoke-static {v1, v2}, Landroid/os/Trace;->traceEnd(J)V

    .line 859
    return-void
.end method
