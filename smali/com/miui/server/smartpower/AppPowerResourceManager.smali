.class public Lcom/miui/server/smartpower/AppPowerResourceManager;
.super Ljava/lang/Object;
.source "AppPowerResourceManager.java"


# static fields
.field public static final DEBUG:Z

.field public static final RESOURCE_BEHAVIOR_CAMERA:I = 0x20

.field public static final RESOURCE_BEHAVIOR_EXTERNAL_CASTING:I = 0x200

.field public static final RESOURCE_BEHAVIOR_GPS:I = 0x40

.field public static final RESOURCE_BEHAVIOR_HOLD_SCREEN:I = 0x400

.field public static final RESOURCE_BEHAVIOR_INCALL:I = 0x10

.field public static final RESOURCE_BEHAVIOR_NETWORK:I = 0x80

.field public static final RESOURCE_BEHAVIOR_PLAYER:I = 0x4

.field public static final RESOURCE_BEHAVIOR_PLAYER_PLAYBACK:I = 0x2

.field public static final RESOURCE_BEHAVIOR_RECORDER:I = 0x8

.field public static final RESOURCE_BEHAVIOR_WIFI_CASTING:I = 0x100

.field public static final RES_TYPE_AUDIO:I = 0x1

.field public static final RES_TYPE_BLE:I = 0x5

.field public static final RES_TYPE_CAMERA:I = 0x6

.field public static final RES_TYPE_DISPLAY:I = 0x7

.field public static final RES_TYPE_GPS:I = 0x3

.field public static final RES_TYPE_NET:I = 0x2

.field public static final RES_TYPE_WAKELOCK:I = 0x4

.field public static final TAG:Ljava/lang/String; = "SmartPower.AppResource"


# instance fields
.field private mAudioResource:Lcom/miui/server/smartpower/AppAudioResource;

.field private mBluetoothResource:Lcom/miui/server/smartpower/AppBluetoothResource;

.field private mCameraResource:Lcom/miui/server/smartpower/AppCameraResource;

.field private mDeviceResouceMap:Ljava/util/HashMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/HashMap<",
            "Ljava/lang/Integer;",
            "Lcom/miui/server/smartpower/AppPowerResource;",
            ">;"
        }
    .end annotation
.end field

.field private mDisplayResource:Lcom/miui/server/smartpower/AppDisplayResource;

.field private mGPSResource:Lcom/miui/server/smartpower/AppGPSResource;

.field private mNetWorkResource:Lcom/miui/server/smartpower/AppNetworkResource;

.field private mWakelockResource:Lcom/miui/server/smartpower/AppWakelockResource;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .line 16
    sget-boolean v0, Lcom/android/server/am/SmartPowerService;->DEBUG:Z

    sput-boolean v0, Lcom/miui/server/smartpower/AppPowerResourceManager;->DEBUG:Z

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/os/Looper;)V
    .locals 2
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "looper"    # Landroid/os/Looper;

    .line 51
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 41
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    iput-object v0, p0, Lcom/miui/server/smartpower/AppPowerResourceManager;->mDeviceResouceMap:Ljava/util/HashMap;

    .line 52
    new-instance v0, Lcom/miui/server/smartpower/AppAudioResource;

    invoke-direct {v0, p1, p2}, Lcom/miui/server/smartpower/AppAudioResource;-><init>(Landroid/content/Context;Landroid/os/Looper;)V

    iput-object v0, p0, Lcom/miui/server/smartpower/AppPowerResourceManager;->mAudioResource:Lcom/miui/server/smartpower/AppAudioResource;

    .line 53
    new-instance v0, Lcom/miui/server/smartpower/AppNetworkResource;

    invoke-direct {v0, p1, p2}, Lcom/miui/server/smartpower/AppNetworkResource;-><init>(Landroid/content/Context;Landroid/os/Looper;)V

    iput-object v0, p0, Lcom/miui/server/smartpower/AppPowerResourceManager;->mNetWorkResource:Lcom/miui/server/smartpower/AppNetworkResource;

    .line 54
    new-instance v0, Lcom/miui/server/smartpower/AppGPSResource;

    invoke-direct {v0, p1, p2}, Lcom/miui/server/smartpower/AppGPSResource;-><init>(Landroid/content/Context;Landroid/os/Looper;)V

    iput-object v0, p0, Lcom/miui/server/smartpower/AppPowerResourceManager;->mGPSResource:Lcom/miui/server/smartpower/AppGPSResource;

    .line 55
    new-instance v0, Lcom/miui/server/smartpower/AppWakelockResource;

    invoke-direct {v0, p1, p2}, Lcom/miui/server/smartpower/AppWakelockResource;-><init>(Landroid/content/Context;Landroid/os/Looper;)V

    iput-object v0, p0, Lcom/miui/server/smartpower/AppPowerResourceManager;->mWakelockResource:Lcom/miui/server/smartpower/AppWakelockResource;

    .line 56
    new-instance v0, Lcom/miui/server/smartpower/AppBluetoothResource;

    invoke-direct {v0, p1, p2}, Lcom/miui/server/smartpower/AppBluetoothResource;-><init>(Landroid/content/Context;Landroid/os/Looper;)V

    iput-object v0, p0, Lcom/miui/server/smartpower/AppPowerResourceManager;->mBluetoothResource:Lcom/miui/server/smartpower/AppBluetoothResource;

    .line 57
    new-instance v0, Lcom/miui/server/smartpower/AppCameraResource;

    invoke-direct {v0, p1, p2}, Lcom/miui/server/smartpower/AppCameraResource;-><init>(Landroid/content/Context;Landroid/os/Looper;)V

    iput-object v0, p0, Lcom/miui/server/smartpower/AppPowerResourceManager;->mCameraResource:Lcom/miui/server/smartpower/AppCameraResource;

    .line 58
    new-instance v0, Lcom/miui/server/smartpower/AppDisplayResource;

    invoke-direct {v0, p1, p2}, Lcom/miui/server/smartpower/AppDisplayResource;-><init>(Landroid/content/Context;Landroid/os/Looper;)V

    iput-object v0, p0, Lcom/miui/server/smartpower/AppPowerResourceManager;->mDisplayResource:Lcom/miui/server/smartpower/AppDisplayResource;

    .line 60
    iget-object v0, p0, Lcom/miui/server/smartpower/AppPowerResourceManager;->mAudioResource:Lcom/miui/server/smartpower/AppAudioResource;

    iget v0, v0, Lcom/miui/server/smartpower/AppAudioResource;->mType:I

    iget-object v1, p0, Lcom/miui/server/smartpower/AppPowerResourceManager;->mAudioResource:Lcom/miui/server/smartpower/AppAudioResource;

    invoke-virtual {p0, v0, v1}, Lcom/miui/server/smartpower/AppPowerResourceManager;->addDeviceResource(ILcom/miui/server/smartpower/AppPowerResource;)V

    .line 61
    iget-object v0, p0, Lcom/miui/server/smartpower/AppPowerResourceManager;->mNetWorkResource:Lcom/miui/server/smartpower/AppNetworkResource;

    iget v0, v0, Lcom/miui/server/smartpower/AppNetworkResource;->mType:I

    iget-object v1, p0, Lcom/miui/server/smartpower/AppPowerResourceManager;->mNetWorkResource:Lcom/miui/server/smartpower/AppNetworkResource;

    invoke-virtual {p0, v0, v1}, Lcom/miui/server/smartpower/AppPowerResourceManager;->addDeviceResource(ILcom/miui/server/smartpower/AppPowerResource;)V

    .line 62
    iget-object v0, p0, Lcom/miui/server/smartpower/AppPowerResourceManager;->mGPSResource:Lcom/miui/server/smartpower/AppGPSResource;

    iget v0, v0, Lcom/miui/server/smartpower/AppGPSResource;->mType:I

    iget-object v1, p0, Lcom/miui/server/smartpower/AppPowerResourceManager;->mGPSResource:Lcom/miui/server/smartpower/AppGPSResource;

    invoke-virtual {p0, v0, v1}, Lcom/miui/server/smartpower/AppPowerResourceManager;->addDeviceResource(ILcom/miui/server/smartpower/AppPowerResource;)V

    .line 63
    iget-object v0, p0, Lcom/miui/server/smartpower/AppPowerResourceManager;->mWakelockResource:Lcom/miui/server/smartpower/AppWakelockResource;

    iget v0, v0, Lcom/miui/server/smartpower/AppWakelockResource;->mType:I

    iget-object v1, p0, Lcom/miui/server/smartpower/AppPowerResourceManager;->mWakelockResource:Lcom/miui/server/smartpower/AppWakelockResource;

    invoke-virtual {p0, v0, v1}, Lcom/miui/server/smartpower/AppPowerResourceManager;->addDeviceResource(ILcom/miui/server/smartpower/AppPowerResource;)V

    .line 64
    iget-object v0, p0, Lcom/miui/server/smartpower/AppPowerResourceManager;->mBluetoothResource:Lcom/miui/server/smartpower/AppBluetoothResource;

    iget v0, v0, Lcom/miui/server/smartpower/AppBluetoothResource;->mType:I

    iget-object v1, p0, Lcom/miui/server/smartpower/AppPowerResourceManager;->mBluetoothResource:Lcom/miui/server/smartpower/AppBluetoothResource;

    invoke-virtual {p0, v0, v1}, Lcom/miui/server/smartpower/AppPowerResourceManager;->addDeviceResource(ILcom/miui/server/smartpower/AppPowerResource;)V

    .line 65
    iget-object v0, p0, Lcom/miui/server/smartpower/AppPowerResourceManager;->mCameraResource:Lcom/miui/server/smartpower/AppCameraResource;

    iget v0, v0, Lcom/miui/server/smartpower/AppCameraResource;->mType:I

    iget-object v1, p0, Lcom/miui/server/smartpower/AppPowerResourceManager;->mCameraResource:Lcom/miui/server/smartpower/AppCameraResource;

    invoke-virtual {p0, v0, v1}, Lcom/miui/server/smartpower/AppPowerResourceManager;->addDeviceResource(ILcom/miui/server/smartpower/AppPowerResource;)V

    .line 66
    iget-object v0, p0, Lcom/miui/server/smartpower/AppPowerResourceManager;->mDisplayResource:Lcom/miui/server/smartpower/AppDisplayResource;

    iget v0, v0, Lcom/miui/server/smartpower/AppDisplayResource;->mType:I

    iget-object v1, p0, Lcom/miui/server/smartpower/AppPowerResourceManager;->mDisplayResource:Lcom/miui/server/smartpower/AppDisplayResource;

    invoke-virtual {p0, v0, v1}, Lcom/miui/server/smartpower/AppPowerResourceManager;->addDeviceResource(ILcom/miui/server/smartpower/AppPowerResource;)V

    .line 67
    return-void
.end method

.method public static typeToString(I)Ljava/lang/String;
    .locals 1
    .param p0, "type"    # I

    .line 263
    packed-switch p0, :pswitch_data_0

    .line 279
    invoke-static {p0}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v0

    return-object v0

    .line 277
    :pswitch_0
    const-string v0, "display"

    return-object v0

    .line 275
    :pswitch_1
    const-string v0, "camera"

    return-object v0

    .line 273
    :pswitch_2
    const-string v0, "bluetooth"

    return-object v0

    .line 271
    :pswitch_3
    const-string/jumbo v0, "wakelock"

    return-object v0

    .line 269
    :pswitch_4
    const-string v0, "gps"

    return-object v0

    .line 267
    :pswitch_5
    const-string v0, "net"

    return-object v0

    .line 265
    :pswitch_6
    const-string v0, "audio"

    return-object v0

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_6
        :pswitch_5
        :pswitch_4
        :pswitch_3
        :pswitch_2
        :pswitch_1
        :pswitch_0
    .end packed-switch
.end method


# virtual methods
.method protected addDeviceResource(ILcom/miui/server/smartpower/AppPowerResource;)V
    .locals 2
    .param p1, "type"    # I
    .param p2, "resource"    # Lcom/miui/server/smartpower/AppPowerResource;

    .line 78
    iget-object v0, p0, Lcom/miui/server/smartpower/AppPowerResourceManager;->mDeviceResouceMap:Ljava/util/HashMap;

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-virtual {v0, v1, p2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 79
    return-void
.end method

.method public getActiveUids(I)Ljava/util/ArrayList;
    .locals 2
    .param p1, "resourceType"    # I

    .line 222
    iget-object v0, p0, Lcom/miui/server/smartpower/AppPowerResourceManager;->mDeviceResouceMap:Ljava/util/HashMap;

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/miui/server/smartpower/AppPowerResource;

    .line 223
    .local v0, "res":Lcom/miui/server/smartpower/AppPowerResource;
    if-eqz v0, :cond_0

    .line 224
    invoke-virtual {v0}, Lcom/miui/server/smartpower/AppPowerResource;->getActiveUids()Ljava/util/ArrayList;

    move-result-object v1

    return-object v1

    .line 226
    :cond_0
    const/4 v1, 0x0

    return-object v1
.end method

.method public getLastMusicPlayTimeStamp(I)J
    .locals 2
    .param p1, "pid"    # I

    .line 231
    iget-object v0, p0, Lcom/miui/server/smartpower/AppPowerResourceManager;->mAudioResource:Lcom/miui/server/smartpower/AppAudioResource;

    invoke-virtual {v0, p1}, Lcom/miui/server/smartpower/AppAudioResource;->getLastMusicPlayTimeStamp(I)J

    move-result-wide v0

    return-wide v0
.end method

.method public init()V
    .locals 2

    .line 70
    iget-object v0, p0, Lcom/miui/server/smartpower/AppPowerResourceManager;->mDeviceResouceMap:Ljava/util/HashMap;

    invoke-virtual {v0}, Ljava/util/HashMap;->values()Ljava/util/Collection;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v0

    :goto_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_1

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/miui/server/smartpower/AppPowerResource;

    .line 71
    .local v1, "res":Lcom/miui/server/smartpower/AppPowerResource;
    if-eqz v1, :cond_0

    .line 72
    invoke-virtual {v1}, Lcom/miui/server/smartpower/AppPowerResource;->init()V

    .line 74
    .end local v1    # "res":Lcom/miui/server/smartpower/AppPowerResource;
    :cond_0
    goto :goto_0

    .line 75
    :cond_1
    return-void
.end method

.method public isAppResActive(II)Z
    .locals 2
    .param p1, "uid"    # I
    .param p2, "resourceType"    # I

    .line 204
    iget-object v0, p0, Lcom/miui/server/smartpower/AppPowerResourceManager;->mDeviceResouceMap:Ljava/util/HashMap;

    invoke-static {p2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/miui/server/smartpower/AppPowerResource;

    .line 205
    .local v0, "res":Lcom/miui/server/smartpower/AppPowerResource;
    if-eqz v0, :cond_0

    .line 206
    invoke-virtual {v0, p1}, Lcom/miui/server/smartpower/AppPowerResource;->isAppResourceActive(I)Z

    move-result v1

    return v1

    .line 208
    :cond_0
    const/4 v1, 0x0

    return v1
.end method

.method public isAppResActive(III)Z
    .locals 2
    .param p1, "uid"    # I
    .param p2, "pid"    # I
    .param p3, "resourceType"    # I

    .line 213
    iget-object v0, p0, Lcom/miui/server/smartpower/AppPowerResourceManager;->mDeviceResouceMap:Ljava/util/HashMap;

    invoke-static {p3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/miui/server/smartpower/AppPowerResource;

    .line 214
    .local v0, "res":Lcom/miui/server/smartpower/AppPowerResource;
    if-eqz v0, :cond_0

    .line 215
    invoke-virtual {v0, p1, p2}, Lcom/miui/server/smartpower/AppPowerResource;->isAppResourceActive(II)Z

    move-result v1

    return v1

    .line 217
    :cond_0
    const/4 v1, 0x0

    return v1
.end method

.method public notifyCameraForegroundState(Ljava/lang/String;ZLjava/lang/String;II)V
    .locals 6
    .param p1, "cameraId"    # Ljava/lang/String;
    .param p2, "isForeground"    # Z
    .param p3, "caller"    # Ljava/lang/String;
    .param p4, "callerUid"    # I
    .param p5, "callerPid"    # I

    .line 153
    iget-object v0, p0, Lcom/miui/server/smartpower/AppPowerResourceManager;->mCameraResource:Lcom/miui/server/smartpower/AppCameraResource;

    move-object v1, p1

    move v2, p2

    move-object v3, p3

    move v4, p4

    move v5, p5

    invoke-virtual/range {v0 .. v5}, Lcom/miui/server/smartpower/AppCameraResource;->notifyCameraForegroundState(Ljava/lang/String;ZLjava/lang/String;II)V

    .line 155
    return-void
.end method

.method public onAcquireWakelock(Landroid/os/IBinder;ILjava/lang/String;II)V
    .locals 6
    .param p1, "lock"    # Landroid/os/IBinder;
    .param p2, "flags"    # I
    .param p3, "tag"    # Ljava/lang/String;
    .param p4, "ownerUid"    # I
    .param p5, "ownerPid"    # I

    .line 135
    iget-object v0, p0, Lcom/miui/server/smartpower/AppPowerResourceManager;->mWakelockResource:Lcom/miui/server/smartpower/AppWakelockResource;

    move-object v1, p1

    move v2, p2

    move-object v3, p3

    move v4, p4

    move v5, p5

    invoke-virtual/range {v0 .. v5}, Lcom/miui/server/smartpower/AppWakelockResource;->acquireWakelock(Landroid/os/IBinder;ILjava/lang/String;II)V

    .line 136
    return-void
.end method

.method public onAquireLocation(IILandroid/location/ILocationListener;)V
    .locals 1
    .param p1, "uid"    # I
    .param p2, "pid"    # I
    .param p3, "listener"    # Landroid/location/ILocationListener;

    .line 126
    iget-object v0, p0, Lcom/miui/server/smartpower/AppPowerResourceManager;->mGPSResource:Lcom/miui/server/smartpower/AppGPSResource;

    invoke-virtual {v0, p1, p2, p3}, Lcom/miui/server/smartpower/AppGPSResource;->onAquireLocation(IILandroid/location/ILocationListener;)V

    .line 127
    return-void
.end method

.method public onBluetoothEvent(ZIIII)V
    .locals 6
    .param p1, "isConnect"    # Z
    .param p2, "bleType"    # I
    .param p3, "uid"    # I
    .param p4, "pid"    # I
    .param p5, "data"    # I

    .line 144
    iget-object v0, p0, Lcom/miui/server/smartpower/AppPowerResourceManager;->mBluetoothResource:Lcom/miui/server/smartpower/AppBluetoothResource;

    move v1, p1

    move v2, p2

    move v3, p3

    move v4, p4

    move v5, p5

    invoke-virtual/range {v0 .. v5}, Lcom/miui/server/smartpower/AppBluetoothResource;->onBluetoothEvent(ZIIII)V

    .line 145
    return-void
.end method

.method public onPlayerEvent(IIII)V
    .locals 1
    .param p1, "uid"    # I
    .param p2, "pid"    # I
    .param p3, "piid"    # I
    .param p4, "event"    # I

    .line 102
    iget-object v0, p0, Lcom/miui/server/smartpower/AppPowerResourceManager;->mAudioResource:Lcom/miui/server/smartpower/AppAudioResource;

    invoke-virtual {v0, p1, p2, p3, p4}, Lcom/miui/server/smartpower/AppAudioResource;->onPlayerEvent(IIII)V

    .line 103
    return-void
.end method

.method public onPlayerRlease(III)V
    .locals 1
    .param p1, "uid"    # I
    .param p2, "pid"    # I
    .param p3, "piid"    # I

    .line 98
    iget-object v0, p0, Lcom/miui/server/smartpower/AppPowerResourceManager;->mAudioResource:Lcom/miui/server/smartpower/AppAudioResource;

    invoke-virtual {v0, p1, p2, p3}, Lcom/miui/server/smartpower/AppAudioResource;->onPlayerRlease(III)V

    .line 99
    return-void
.end method

.method public onPlayerTrack(IIII)V
    .locals 1
    .param p1, "uid"    # I
    .param p2, "pid"    # I
    .param p3, "piid"    # I
    .param p4, "sessionId"    # I

    .line 94
    iget-object v0, p0, Lcom/miui/server/smartpower/AppPowerResourceManager;->mAudioResource:Lcom/miui/server/smartpower/AppAudioResource;

    invoke-virtual {v0, p1, p2, p3, p4}, Lcom/miui/server/smartpower/AppAudioResource;->onPlayerTrack(IIII)V

    .line 95
    return-void
.end method

.method public onRecorderEvent(III)V
    .locals 1
    .param p1, "uid"    # I
    .param p2, "riid"    # I
    .param p3, "event"    # I

    .line 114
    iget-object v0, p0, Lcom/miui/server/smartpower/AppPowerResourceManager;->mAudioResource:Lcom/miui/server/smartpower/AppAudioResource;

    invoke-virtual {v0, p1, p2, p3}, Lcom/miui/server/smartpower/AppAudioResource;->onRecorderEvent(III)V

    .line 115
    return-void
.end method

.method public onRecorderRlease(II)V
    .locals 1
    .param p1, "uid"    # I
    .param p2, "riid"    # I

    .line 110
    iget-object v0, p0, Lcom/miui/server/smartpower/AppPowerResourceManager;->mAudioResource:Lcom/miui/server/smartpower/AppAudioResource;

    invoke-virtual {v0, p1, p2}, Lcom/miui/server/smartpower/AppAudioResource;->onRecorderRlease(II)V

    .line 111
    return-void
.end method

.method public onRecorderTrack(III)V
    .locals 1
    .param p1, "uid"    # I
    .param p2, "pid"    # I
    .param p3, "riid"    # I

    .line 106
    iget-object v0, p0, Lcom/miui/server/smartpower/AppPowerResourceManager;->mAudioResource:Lcom/miui/server/smartpower/AppAudioResource;

    invoke-virtual {v0, p1, p2, p3}, Lcom/miui/server/smartpower/AppAudioResource;->onRecorderTrack(III)V

    .line 107
    return-void
.end method

.method public onReleaseLocation(IILandroid/location/ILocationListener;)V
    .locals 1
    .param p1, "uid"    # I
    .param p2, "pid"    # I
    .param p3, "listener"    # Landroid/location/ILocationListener;

    .line 130
    iget-object v0, p0, Lcom/miui/server/smartpower/AppPowerResourceManager;->mGPSResource:Lcom/miui/server/smartpower/AppGPSResource;

    invoke-virtual {v0, p1, p2, p3}, Lcom/miui/server/smartpower/AppGPSResource;->onReleaseLocation(IILandroid/location/ILocationListener;)V

    .line 131
    return-void
.end method

.method public onReleaseWakelock(Landroid/os/IBinder;I)V
    .locals 1
    .param p1, "lock"    # Landroid/os/IBinder;
    .param p2, "flags"    # I

    .line 139
    iget-object v0, p0, Lcom/miui/server/smartpower/AppPowerResourceManager;->mWakelockResource:Lcom/miui/server/smartpower/AppWakelockResource;

    invoke-virtual {v0, p1, p2}, Lcom/miui/server/smartpower/AppWakelockResource;->releaseWakelock(Landroid/os/IBinder;I)V

    .line 140
    return-void
.end method

.method public playbackStateChanged(IIII)V
    .locals 1
    .param p1, "uid"    # I
    .param p2, "pid"    # I
    .param p3, "oldState"    # I
    .param p4, "newState"    # I

    .line 82
    iget-object v0, p0, Lcom/miui/server/smartpower/AppPowerResourceManager;->mAudioResource:Lcom/miui/server/smartpower/AppAudioResource;

    invoke-virtual {v0, p1, p2, p3, p4}, Lcom/miui/server/smartpower/AppAudioResource;->playbackStateChanged(IIII)V

    .line 83
    return-void
.end method

.method public recordAudioFocus(IILjava/lang/String;Z)V
    .locals 1
    .param p1, "uid"    # I
    .param p2, "pid"    # I
    .param p3, "clientId"    # Ljava/lang/String;
    .param p4, "request"    # Z

    .line 86
    iget-object v0, p0, Lcom/miui/server/smartpower/AppPowerResourceManager;->mAudioResource:Lcom/miui/server/smartpower/AppAudioResource;

    invoke-virtual {v0, p1, p2, p3, p4}, Lcom/miui/server/smartpower/AppAudioResource;->recordAudioFocus(IILjava/lang/String;Z)V

    .line 87
    return-void
.end method

.method public recordAudioFocusLoss(ILjava/lang/String;I)V
    .locals 1
    .param p1, "uid"    # I
    .param p2, "clientId"    # Ljava/lang/String;
    .param p3, "focusLoss"    # I

    .line 90
    iget-object v0, p0, Lcom/miui/server/smartpower/AppPowerResourceManager;->mAudioResource:Lcom/miui/server/smartpower/AppAudioResource;

    invoke-virtual {v0, p1, p2, p3}, Lcom/miui/server/smartpower/AppAudioResource;->recordAudioFocusLoss(ILjava/lang/String;I)V

    .line 91
    return-void
.end method

.method public registerCallback(ILcom/miui/server/smartpower/AppPowerResource$IAppPowerResourceCallback;I)V
    .locals 2
    .param p1, "type"    # I
    .param p2, "callback"    # Lcom/miui/server/smartpower/AppPowerResource$IAppPowerResourceCallback;
    .param p3, "uid"    # I

    .line 235
    iget-object v0, p0, Lcom/miui/server/smartpower/AppPowerResourceManager;->mDeviceResouceMap:Ljava/util/HashMap;

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/miui/server/smartpower/AppPowerResource;

    .line 236
    .local v0, "res":Lcom/miui/server/smartpower/AppPowerResource;
    if-eqz v0, :cond_0

    .line 237
    invoke-virtual {v0, p2, p3}, Lcom/miui/server/smartpower/AppPowerResource;->registerCallback(Lcom/miui/server/smartpower/AppPowerResource$IAppPowerResourceCallback;I)V

    .line 239
    :cond_0
    return-void
.end method

.method public registerCallback(ILcom/miui/server/smartpower/AppPowerResource$IAppPowerResourceCallback;II)V
    .locals 2
    .param p1, "type"    # I
    .param p2, "callback"    # Lcom/miui/server/smartpower/AppPowerResource$IAppPowerResourceCallback;
    .param p3, "uid"    # I
    .param p4, "pid"    # I

    .line 249
    iget-object v0, p0, Lcom/miui/server/smartpower/AppPowerResourceManager;->mDeviceResouceMap:Ljava/util/HashMap;

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/miui/server/smartpower/AppPowerResource;

    .line 250
    .local v0, "res":Lcom/miui/server/smartpower/AppPowerResource;
    if-eqz v0, :cond_0

    .line 251
    invoke-virtual {v0, p2, p3, p4}, Lcom/miui/server/smartpower/AppPowerResource;->registerCallback(Lcom/miui/server/smartpower/AppPowerResource$IAppPowerResourceCallback;II)V

    .line 253
    :cond_0
    return-void
.end method

.method public releaseAppAllPowerResources(I)V
    .locals 2
    .param p1, "uid"    # I

    .line 185
    iget-object v0, p0, Lcom/miui/server/smartpower/AppPowerResourceManager;->mDeviceResouceMap:Ljava/util/HashMap;

    invoke-virtual {v0}, Ljava/util/HashMap;->values()Ljava/util/Collection;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v0

    :goto_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_1

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/miui/server/smartpower/AppPowerResource;

    .line 186
    .local v1, "res":Lcom/miui/server/smartpower/AppPowerResource;
    if-eqz v1, :cond_0

    .line 187
    invoke-virtual {v1, p1}, Lcom/miui/server/smartpower/AppPowerResource;->releaseAppPowerResource(I)V

    .line 189
    .end local v1    # "res":Lcom/miui/server/smartpower/AppPowerResource;
    :cond_0
    goto :goto_0

    .line 190
    :cond_1
    return-void
.end method

.method public releaseAppPowerResources(ILjava/util/ArrayList;)V
    .locals 4
    .param p1, "uid"    # I
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(I",
            "Ljava/util/ArrayList<",
            "Ljava/lang/Integer;",
            ">;)V"
        }
    .end annotation

    .line 161
    .local p2, "resourceTypes":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/Integer;>;"
    invoke-virtual {p2}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v0

    :goto_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_1

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/Integer;

    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v1

    .line 162
    .local v1, "type":I
    iget-object v2, p0, Lcom/miui/server/smartpower/AppPowerResourceManager;->mDeviceResouceMap:Ljava/util/HashMap;

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/miui/server/smartpower/AppPowerResource;

    .line 163
    .local v2, "res":Lcom/miui/server/smartpower/AppPowerResource;
    if-eqz v2, :cond_0

    .line 164
    invoke-virtual {v2, p1}, Lcom/miui/server/smartpower/AppPowerResource;->releaseAppPowerResource(I)V

    .line 166
    .end local v1    # "type":I
    .end local v2    # "res":Lcom/miui/server/smartpower/AppPowerResource;
    :cond_0
    goto :goto_0

    .line 167
    :cond_1
    return-void
.end method

.method public reportTrackStatus(IIIZ)V
    .locals 1
    .param p1, "uid"    # I
    .param p2, "pid"    # I
    .param p3, "sessionId"    # I
    .param p4, "isMuted"    # Z

    .line 148
    iget-object v0, p0, Lcom/miui/server/smartpower/AppPowerResourceManager;->mAudioResource:Lcom/miui/server/smartpower/AppAudioResource;

    invoke-virtual {v0, p1, p2, p3, p4}, Lcom/miui/server/smartpower/AppAudioResource;->reportTrackStatus(IIIZ)V

    .line 149
    return-void
.end method

.method public resumeAppAllPowerResources(I)V
    .locals 2
    .param p1, "uid"    # I

    .line 196
    iget-object v0, p0, Lcom/miui/server/smartpower/AppPowerResourceManager;->mDeviceResouceMap:Ljava/util/HashMap;

    invoke-virtual {v0}, Ljava/util/HashMap;->values()Ljava/util/Collection;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v0

    :goto_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_1

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/miui/server/smartpower/AppPowerResource;

    .line 197
    .local v1, "res":Lcom/miui/server/smartpower/AppPowerResource;
    if-eqz v1, :cond_0

    .line 198
    invoke-virtual {v1, p1}, Lcom/miui/server/smartpower/AppPowerResource;->resumeAppPowerResource(I)V

    .line 200
    .end local v1    # "res":Lcom/miui/server/smartpower/AppPowerResource;
    :cond_0
    goto :goto_0

    .line 201
    :cond_1
    return-void
.end method

.method public resumeAppPowerResources(ILjava/util/ArrayList;)V
    .locals 4
    .param p1, "uid"    # I
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(I",
            "Ljava/util/ArrayList<",
            "Ljava/lang/Integer;",
            ">;)V"
        }
    .end annotation

    .line 173
    .local p2, "resourceTypes":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/Integer;>;"
    invoke-virtual {p2}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v0

    :goto_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_1

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/Integer;

    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v1

    .line 174
    .local v1, "type":I
    iget-object v2, p0, Lcom/miui/server/smartpower/AppPowerResourceManager;->mDeviceResouceMap:Ljava/util/HashMap;

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/miui/server/smartpower/AppPowerResource;

    .line 175
    .local v2, "res":Lcom/miui/server/smartpower/AppPowerResource;
    if-eqz v2, :cond_0

    .line 176
    invoke-virtual {v2, p1}, Lcom/miui/server/smartpower/AppPowerResource;->resumeAppPowerResource(I)V

    .line 178
    .end local v1    # "type":I
    .end local v2    # "res":Lcom/miui/server/smartpower/AppPowerResource;
    :cond_0
    goto :goto_0

    .line 179
    :cond_1
    return-void
.end method

.method public uidAudioStatusChanged(IZ)V
    .locals 1
    .param p1, "uid"    # I
    .param p2, "active"    # Z

    .line 118
    iget-object v0, p0, Lcom/miui/server/smartpower/AppPowerResourceManager;->mAudioResource:Lcom/miui/server/smartpower/AppAudioResource;

    invoke-virtual {v0, p1, p2}, Lcom/miui/server/smartpower/AppAudioResource;->uidAudioStatusChanged(IZ)V

    .line 119
    return-void
.end method

.method public uidVideoStatusChanged(IZ)V
    .locals 1
    .param p1, "uid"    # I
    .param p2, "active"    # Z

    .line 122
    iget-object v0, p0, Lcom/miui/server/smartpower/AppPowerResourceManager;->mAudioResource:Lcom/miui/server/smartpower/AppAudioResource;

    invoke-virtual {v0, p1, p2}, Lcom/miui/server/smartpower/AppAudioResource;->uidVideoStatusChanged(IZ)V

    .line 123
    return-void
.end method

.method public unRegisterCallback(ILcom/miui/server/smartpower/AppPowerResource$IAppPowerResourceCallback;I)V
    .locals 2
    .param p1, "type"    # I
    .param p2, "callback"    # Lcom/miui/server/smartpower/AppPowerResource$IAppPowerResourceCallback;
    .param p3, "uid"    # I

    .line 242
    iget-object v0, p0, Lcom/miui/server/smartpower/AppPowerResourceManager;->mDeviceResouceMap:Ljava/util/HashMap;

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/miui/server/smartpower/AppPowerResource;

    .line 243
    .local v0, "res":Lcom/miui/server/smartpower/AppPowerResource;
    if-eqz v0, :cond_0

    .line 244
    invoke-virtual {v0, p2, p3}, Lcom/miui/server/smartpower/AppPowerResource;->unRegisterCallback(Lcom/miui/server/smartpower/AppPowerResource$IAppPowerResourceCallback;I)V

    .line 246
    :cond_0
    return-void
.end method

.method public unRegisterCallback(ILcom/miui/server/smartpower/AppPowerResource$IAppPowerResourceCallback;II)V
    .locals 2
    .param p1, "type"    # I
    .param p2, "callback"    # Lcom/miui/server/smartpower/AppPowerResource$IAppPowerResourceCallback;
    .param p3, "uid"    # I
    .param p4, "pid"    # I

    .line 256
    iget-object v0, p0, Lcom/miui/server/smartpower/AppPowerResourceManager;->mDeviceResouceMap:Ljava/util/HashMap;

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/miui/server/smartpower/AppPowerResource;

    .line 257
    .local v0, "res":Lcom/miui/server/smartpower/AppPowerResource;
    if-eqz v0, :cond_0

    .line 258
    invoke-virtual {v0, p2, p3, p4}, Lcom/miui/server/smartpower/AppPowerResource;->unRegisterCallback(Lcom/miui/server/smartpower/AppPowerResource$IAppPowerResourceCallback;II)V

    .line 260
    :cond_0
    return-void
.end method
