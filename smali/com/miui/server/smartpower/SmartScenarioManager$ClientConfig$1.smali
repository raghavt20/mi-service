.class Lcom/miui/server/smartpower/SmartScenarioManager$ClientConfig$1;
.super Ljava/lang/Object;
.source "SmartScenarioManager.java"

# interfaces
.implements Ljava/lang/Runnable;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/miui/server/smartpower/SmartScenarioManager$ClientConfig;->reportSenarioChanged(JJ)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$1:Lcom/miui/server/smartpower/SmartScenarioManager$ClientConfig;

.field final synthetic val$additionalScenario:J

.field final synthetic val$mainScenario:J


# direct methods
.method constructor <init>(Lcom/miui/server/smartpower/SmartScenarioManager$ClientConfig;JJ)V
    .locals 0
    .param p1, "this$1"    # Lcom/miui/server/smartpower/SmartScenarioManager$ClientConfig;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .line 394
    iput-object p1, p0, Lcom/miui/server/smartpower/SmartScenarioManager$ClientConfig$1;->this$1:Lcom/miui/server/smartpower/SmartScenarioManager$ClientConfig;

    iput-wide p2, p0, Lcom/miui/server/smartpower/SmartScenarioManager$ClientConfig$1;->val$mainScenario:J

    iput-wide p4, p0, Lcom/miui/server/smartpower/SmartScenarioManager$ClientConfig$1;->val$additionalScenario:J

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public run()V
    .locals 5

    .line 397
    iget-object v0, p0, Lcom/miui/server/smartpower/SmartScenarioManager$ClientConfig$1;->this$1:Lcom/miui/server/smartpower/SmartScenarioManager$ClientConfig;

    invoke-static {v0}, Lcom/miui/server/smartpower/SmartScenarioManager$ClientConfig;->-$$Nest$fgetmMainSenarioDescription(Lcom/miui/server/smartpower/SmartScenarioManager$ClientConfig;)J

    move-result-wide v0

    iget-wide v2, p0, Lcom/miui/server/smartpower/SmartScenarioManager$ClientConfig$1;->val$mainScenario:J

    cmp-long v0, v0, v2

    if-nez v0, :cond_1

    iget-object v0, p0, Lcom/miui/server/smartpower/SmartScenarioManager$ClientConfig$1;->this$1:Lcom/miui/server/smartpower/SmartScenarioManager$ClientConfig;

    invoke-static {v0}, Lcom/miui/server/smartpower/SmartScenarioManager$ClientConfig;->-$$Nest$fgetmAdditionalScenarioDescription(Lcom/miui/server/smartpower/SmartScenarioManager$ClientConfig;)J

    move-result-wide v0

    iget-wide v2, p0, Lcom/miui/server/smartpower/SmartScenarioManager$ClientConfig$1;->val$additionalScenario:J

    cmp-long v0, v0, v2

    if-eqz v0, :cond_0

    goto :goto_0

    .line 401
    :cond_0
    iget-object v0, p0, Lcom/miui/server/smartpower/SmartScenarioManager$ClientConfig$1;->this$1:Lcom/miui/server/smartpower/SmartScenarioManager$ClientConfig;

    invoke-static {v0}, Lcom/miui/server/smartpower/SmartScenarioManager$ClientConfig;->-$$Nest$fgetmCallback(Lcom/miui/server/smartpower/SmartScenarioManager$ClientConfig;)Lcom/miui/server/smartpower/SmartScenarioManager$ISmartScenarioCallback;

    move-result-object v0

    iget-object v1, p0, Lcom/miui/server/smartpower/SmartScenarioManager$ClientConfig$1;->this$1:Lcom/miui/server/smartpower/SmartScenarioManager$ClientConfig;

    invoke-static {v1}, Lcom/miui/server/smartpower/SmartScenarioManager$ClientConfig;->-$$Nest$fgetmMainSenarioDescription(Lcom/miui/server/smartpower/SmartScenarioManager$ClientConfig;)J

    move-result-wide v1

    iget-object v3, p0, Lcom/miui/server/smartpower/SmartScenarioManager$ClientConfig$1;->this$1:Lcom/miui/server/smartpower/SmartScenarioManager$ClientConfig;

    invoke-static {v3}, Lcom/miui/server/smartpower/SmartScenarioManager$ClientConfig;->-$$Nest$fgetmAdditionalScenarioDescription(Lcom/miui/server/smartpower/SmartScenarioManager$ClientConfig;)J

    move-result-wide v3

    invoke-interface {v0, v1, v2, v3, v4}, Lcom/miui/server/smartpower/SmartScenarioManager$ISmartScenarioCallback;->onCurrentScenarioChanged(JJ)V

    .line 403
    return-void

    .line 399
    :cond_1
    :goto_0
    return-void
.end method
