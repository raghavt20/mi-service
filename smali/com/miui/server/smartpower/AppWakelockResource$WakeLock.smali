.class final Lcom/miui/server/smartpower/AppWakelockResource$WakeLock;
.super Ljava/lang/Object;
.source "AppWakelockResource.java"

# interfaces
.implements Landroid/os/IBinder$DeathRecipient;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/miui/server/smartpower/AppWakelockResource;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x10
    name = "WakeLock"
.end annotation


# instance fields
.field public mFlags:I

.field public final mLock:Landroid/os/IBinder;

.field public final mOwnerPid:I

.field public final mOwnerUid:I

.field public mTag:Ljava/lang/String;

.field final synthetic this$0:Lcom/miui/server/smartpower/AppWakelockResource;


# direct methods
.method public constructor <init>(Lcom/miui/server/smartpower/AppWakelockResource;Landroid/os/IBinder;ILjava/lang/String;II)V
    .locals 0
    .param p1, "this$0"    # Lcom/miui/server/smartpower/AppWakelockResource;
    .param p2, "lock"    # Landroid/os/IBinder;
    .param p3, "flags"    # I
    .param p4, "tag"    # Ljava/lang/String;
    .param p5, "ownerUid"    # I
    .param p6, "ownerPid"    # I

    .line 201
    iput-object p1, p0, Lcom/miui/server/smartpower/AppWakelockResource$WakeLock;->this$0:Lcom/miui/server/smartpower/AppWakelockResource;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 202
    iput-object p2, p0, Lcom/miui/server/smartpower/AppWakelockResource$WakeLock;->mLock:Landroid/os/IBinder;

    .line 203
    iput p3, p0, Lcom/miui/server/smartpower/AppWakelockResource$WakeLock;->mFlags:I

    .line 204
    iput-object p4, p0, Lcom/miui/server/smartpower/AppWakelockResource$WakeLock;->mTag:Ljava/lang/String;

    .line 205
    iput p5, p0, Lcom/miui/server/smartpower/AppWakelockResource$WakeLock;->mOwnerUid:I

    .line 206
    iput p6, p0, Lcom/miui/server/smartpower/AppWakelockResource$WakeLock;->mOwnerPid:I

    .line 207
    return-void
.end method

.method private getLockFlagsString()Ljava/lang/String;
    .locals 3

    .line 241
    const-string v0, ""

    .line 242
    .local v0, "result":Ljava/lang/String;
    iget v1, p0, Lcom/miui/server/smartpower/AppWakelockResource$WakeLock;->mFlags:I

    const/high16 v2, 0x10000000

    and-int/2addr v1, v2

    if-eqz v1, :cond_0

    .line 243
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " ACQUIRE_CAUSES_WAKEUP"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    .line 245
    :cond_0
    iget v1, p0, Lcom/miui/server/smartpower/AppWakelockResource$WakeLock;->mFlags:I

    const/high16 v2, 0x20000000

    and-int/2addr v1, v2

    if-eqz v1, :cond_1

    .line 246
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " ON_AFTER_RELEASE"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    .line 248
    :cond_1
    return-object v0
.end method

.method private getLockLevelString()Ljava/lang/String;
    .locals 2

    .line 220
    iget v0, p0, Lcom/miui/server/smartpower/AppWakelockResource$WakeLock;->mFlags:I

    const v1, 0xffff

    and-int/2addr v0, v1

    sparse-switch v0, :sswitch_data_0

    .line 236
    const-string v0, "???                           "

    return-object v0

    .line 234
    :sswitch_0
    const-string v0, "DRAW_WAKE_LOCK                "

    return-object v0

    .line 232
    :sswitch_1
    const-string v0, "DOZE_WAKE_LOCK                "

    return-object v0

    .line 230
    :sswitch_2
    const-string v0, "PROXIMITY_SCREEN_OFF_WAKE_LOCK"

    return-object v0

    .line 222
    :sswitch_3
    const-string v0, "FULL_WAKE_LOCK                "

    return-object v0

    .line 224
    :sswitch_4
    const-string v0, "SCREEN_BRIGHT_WAKE_LOCK       "

    return-object v0

    .line 226
    :sswitch_5
    const-string v0, "SCREEN_DIM_WAKE_LOCK          "

    return-object v0

    .line 228
    :sswitch_6
    const-string v0, "PARTIAL_WAKE_LOCK             "

    return-object v0

    nop

    :sswitch_data_0
    .sparse-switch
        0x1 -> :sswitch_6
        0x6 -> :sswitch_5
        0xa -> :sswitch_4
        0x1a -> :sswitch_3
        0x20 -> :sswitch_2
        0x40 -> :sswitch_1
        0x80 -> :sswitch_0
    .end sparse-switch
.end method


# virtual methods
.method public binderDied()V
    .locals 1

    .line 216
    iget-object v0, p0, Lcom/miui/server/smartpower/AppWakelockResource$WakeLock;->this$0:Lcom/miui/server/smartpower/AppWakelockResource;

    invoke-static {v0, p0}, Lcom/miui/server/smartpower/AppWakelockResource;->-$$Nest$mhandleWakeLockDeath(Lcom/miui/server/smartpower/AppWakelockResource;Lcom/miui/server/smartpower/AppWakelockResource$WakeLock;)V

    .line 217
    return-void
.end method

.method public toString()Ljava/lang/String;
    .locals 2

    .line 253
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    .line 254
    .local v0, "sb":Ljava/lang/StringBuilder;
    invoke-direct {p0}, Lcom/miui/server/smartpower/AppWakelockResource$WakeLock;->getLockLevelString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 255
    const-string v1, " \'"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 256
    iget-object v1, p0, Lcom/miui/server/smartpower/AppWakelockResource$WakeLock;->mTag:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 257
    const-string v1, "\'"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 258
    invoke-direct {p0}, Lcom/miui/server/smartpower/AppWakelockResource$WakeLock;->getLockFlagsString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 259
    const-string v1, " (uid="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 260
    iget v1, p0, Lcom/miui/server/smartpower/AppWakelockResource$WakeLock;->mOwnerUid:I

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    .line 261
    iget v1, p0, Lcom/miui/server/smartpower/AppWakelockResource$WakeLock;->mOwnerPid:I

    if-eqz v1, :cond_0

    .line 262
    const-string v1, " pid="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 263
    iget v1, p0, Lcom/miui/server/smartpower/AppWakelockResource$WakeLock;->mOwnerPid:I

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    .line 265
    :cond_0
    const-string v1, ")"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 266
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    return-object v1
.end method

.method public updateProperties(ILjava/lang/String;)V
    .locals 0
    .param p1, "flags"    # I
    .param p2, "tag"    # Ljava/lang/String;

    .line 210
    iput p1, p0, Lcom/miui/server/smartpower/AppWakelockResource$WakeLock;->mFlags:I

    .line 211
    iput-object p2, p0, Lcom/miui/server/smartpower/AppWakelockResource$WakeLock;->mTag:Ljava/lang/String;

    .line 212
    return-void
.end method
