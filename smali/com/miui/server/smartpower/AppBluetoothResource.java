class com.miui.server.smartpower.AppBluetoothResource extends com.miui.server.smartpower.AppPowerResource {
	 /* .source "AppBluetoothResource.java" */
	 /* # annotations */
	 /* .annotation system Ldalvik/annotation/MemberClasses; */
	 /* value = { */
	 /* Lcom/miui/server/smartpower/AppBluetoothResource$BleRecord; */
	 /* } */
} // .end annotation
/* # instance fields */
private final android.util.SparseArray mActivePidsMap;
/* .annotation system Ldalvik/annotation/Signature; */
/* value = { */
/* "Landroid/util/SparseArray<", */
/* "Lcom/miui/server/smartpower/AppBluetoothResource$BleRecord;", */
/* ">;" */
/* } */
} // .end annotation
} // .end field
/* # direct methods */
public com.miui.server.smartpower.AppBluetoothResource ( ) {
/* .locals 1 */
/* .param p1, "context" # Landroid/content/Context; */
/* .param p2, "looper" # Landroid/os/Looper; */
/* .line 21 */
/* invoke-direct {p0}, Lcom/miui/server/smartpower/AppPowerResource;-><init>()V */
/* .line 19 */
/* new-instance v0, Landroid/util/SparseArray; */
/* invoke-direct {v0}, Landroid/util/SparseArray;-><init>()V */
this.mActivePidsMap = v0;
/* .line 22 */
int v0 = 5; // const/4 v0, 0x5
/* iput v0, p0, Lcom/miui/server/smartpower/AppBluetoothResource;->mType:I */
/* .line 23 */
return;
} // .end method
private com.miui.server.smartpower.AppBluetoothResource$BleRecord getBleRecord ( Integer p0 ) {
/* .locals 2 */
/* .param p1, "pid" # I */
/* .line 26 */
v0 = this.mActivePidsMap;
/* monitor-enter v0 */
/* .line 27 */
try { // :try_start_0
v1 = this.mActivePidsMap;
(( android.util.SparseArray ) v1 ).get ( p1 ); // invoke-virtual {v1, p1}, Landroid/util/SparseArray;->get(I)Ljava/lang/Object;
/* check-cast v1, Lcom/miui/server/smartpower/AppBluetoothResource$BleRecord; */
/* monitor-exit v0 */
/* .line 28 */
/* :catchall_0 */
/* move-exception v1 */
/* monitor-exit v0 */
/* :try_end_0 */
/* .catchall {:try_start_0 ..:try_end_0} :catchall_0 */
/* throw v1 */
} // .end method
private com.miui.server.smartpower.AppBluetoothResource$BleRecord getOrCreateBleRecord ( Integer p0, Integer p1 ) {
/* .locals 3 */
/* .param p1, "uid" # I */
/* .param p2, "pid" # I */
/* .line 32 */
v0 = this.mActivePidsMap;
/* monitor-enter v0 */
/* .line 33 */
try { // :try_start_0
	 v1 = this.mActivePidsMap;
	 (( android.util.SparseArray ) v1 ).get ( p2 ); // invoke-virtual {v1, p2}, Landroid/util/SparseArray;->get(I)Ljava/lang/Object;
	 /* check-cast v1, Lcom/miui/server/smartpower/AppBluetoothResource$BleRecord; */
	 /* .line 34 */
	 /* .local v1, "record":Lcom/miui/server/smartpower/AppBluetoothResource$BleRecord; */
	 /* if-nez v1, :cond_0 */
	 /* .line 35 */
	 /* new-instance v2, Lcom/miui/server/smartpower/AppBluetoothResource$BleRecord; */
	 /* invoke-direct {v2, p0, p1, p2}, Lcom/miui/server/smartpower/AppBluetoothResource$BleRecord;-><init>(Lcom/miui/server/smartpower/AppBluetoothResource;II)V */
	 /* move-object v1, v2 */
	 /* .line 36 */
	 v2 = this.mActivePidsMap;
	 (( android.util.SparseArray ) v2 ).put ( p2, v1 ); // invoke-virtual {v2, p2, v1}, Landroid/util/SparseArray;->put(ILjava/lang/Object;)V
	 /* .line 38 */
} // :cond_0
/* monitor-exit v0 */
/* .line 39 */
} // .end local v1 # "record":Lcom/miui/server/smartpower/AppBluetoothResource$BleRecord;
/* :catchall_0 */
/* move-exception v1 */
/* monitor-exit v0 */
/* :try_end_0 */
/* .catchall {:try_start_0 ..:try_end_0} :catchall_0 */
/* throw v1 */
} // .end method
/* # virtual methods */
public java.util.ArrayList getActiveUids ( ) {
/* .locals 1 */
/* .annotation system Ldalvik/annotation/Signature; */
/* value = { */
/* "()", */
/* "Ljava/util/ArrayList<", */
/* "Ljava/lang/Integer;", */
/* ">;" */
/* } */
} // .end annotation
/* .line 44 */
int v0 = 0; // const/4 v0, 0x0
} // .end method
public Boolean isAppResourceActive ( Integer p0 ) {
/* .locals 5 */
/* .param p1, "uid" # I */
/* .line 49 */
int v0 = 0; // const/4 v0, 0x0
/* .line 50 */
/* .local v0, "active":Z */
v1 = this.mActivePidsMap;
/* monitor-enter v1 */
/* .line 51 */
int v2 = 0; // const/4 v2, 0x0
/* .local v2, "i":I */
} // :goto_0
try { // :try_start_0
v3 = this.mActivePidsMap;
v3 = (( android.util.SparseArray ) v3 ).size ( ); // invoke-virtual {v3}, Landroid/util/SparseArray;->size()I
/* if-ge v2, v3, :cond_1 */
/* .line 52 */
v3 = this.mActivePidsMap;
(( android.util.SparseArray ) v3 ).valueAt ( v2 ); // invoke-virtual {v3, v2}, Landroid/util/SparseArray;->valueAt(I)Ljava/lang/Object;
/* check-cast v3, Lcom/miui/server/smartpower/AppBluetoothResource$BleRecord; */
/* .line 53 */
/* .local v3, "record":Lcom/miui/server/smartpower/AppBluetoothResource$BleRecord; */
if ( v3 != null) { // if-eqz v3, :cond_0
/* iget v4, v3, Lcom/miui/server/smartpower/AppBluetoothResource$BleRecord;->mOwnerUid:I */
/* if-ne v4, p1, :cond_0 */
v4 = (( com.miui.server.smartpower.AppBluetoothResource$BleRecord ) v3 ).isActive ( ); // invoke-virtual {v3}, Lcom/miui/server/smartpower/AppBluetoothResource$BleRecord;->isActive()Z
if ( v4 != null) { // if-eqz v4, :cond_0
/* .line 54 */
/* monitor-exit v1 */
int v1 = 1; // const/4 v1, 0x1
/* .line 51 */
} // .end local v3 # "record":Lcom/miui/server/smartpower/AppBluetoothResource$BleRecord;
} // :cond_0
/* add-int/lit8 v2, v2, 0x1 */
/* .line 57 */
} // .end local v2 # "i":I
} // :cond_1
/* monitor-exit v1 */
/* .line 58 */
/* .line 57 */
/* :catchall_0 */
/* move-exception v2 */
/* monitor-exit v1 */
/* :try_end_0 */
/* .catchall {:try_start_0 ..:try_end_0} :catchall_0 */
/* throw v2 */
} // .end method
public Boolean isAppResourceActive ( Integer p0, Integer p1 ) {
/* .locals 2 */
/* .param p1, "uid" # I */
/* .param p2, "pid" # I */
/* .line 63 */
/* invoke-direct {p0, p2}, Lcom/miui/server/smartpower/AppBluetoothResource;->getBleRecord(I)Lcom/miui/server/smartpower/AppBluetoothResource$BleRecord; */
/* .line 64 */
/* .local v0, "record":Lcom/miui/server/smartpower/AppBluetoothResource$BleRecord; */
if ( v0 != null) { // if-eqz v0, :cond_0
/* .line 65 */
v1 = (( com.miui.server.smartpower.AppBluetoothResource$BleRecord ) v0 ).isActive ( ); // invoke-virtual {v0}, Lcom/miui/server/smartpower/AppBluetoothResource$BleRecord;->isActive()Z
/* .line 67 */
} // :cond_0
int v1 = 0; // const/4 v1, 0x0
} // .end method
public void onBluetoothEvent ( Boolean p0, Integer p1, Integer p2, Integer p3, Integer p4 ) {
/* .locals 3 */
/* .param p1, "isConnect" # Z */
/* .param p2, "bleType" # I */
/* .param p3, "uid" # I */
/* .param p4, "pid" # I */
/* .param p5, "flag" # I */
/* .line 106 */
/* sget-boolean v0, Lcom/miui/server/smartpower/AppBluetoothResource;->DEBUG:Z */
if ( v0 != null) { // if-eqz v0, :cond_0
/* .line 107 */
final String v0 = "SmartPower.AppResource"; // const-string v0, "SmartPower.AppResource"
/* new-instance v1, Ljava/lang/StringBuilder; */
/* invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V */
final String v2 = "bluttooth: connect="; // const-string v2, "bluttooth: connect="
(( java.lang.StringBuilder ) v1 ).append ( v2 ); // invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v1 ).append ( p1 ); // invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;
final String v2 = " bleType="; // const-string v2, " bleType="
(( java.lang.StringBuilder ) v1 ).append ( v2 ); // invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v1 ).append ( p2 ); // invoke-virtual {v1, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;
final String v2 = " uid="; // const-string v2, " uid="
(( java.lang.StringBuilder ) v1 ).append ( v2 ); // invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v1 ).append ( p3 ); // invoke-virtual {v1, p3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;
final String v2 = " pid="; // const-string v2, " pid="
(( java.lang.StringBuilder ) v1 ).append ( v2 ); // invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v1 ).append ( p4 ); // invoke-virtual {v1, p4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;
final String v2 = " bleType="; // const-string v2, " bleType="
(( java.lang.StringBuilder ) v1 ).append ( v2 ); // invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v1 ).append ( p2 ); // invoke-virtual {v1, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;
final String v2 = " flag="; // const-string v2, " flag="
(( java.lang.StringBuilder ) v1 ).append ( v2 ); // invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v1 ).append ( p5 ); // invoke-virtual {v1, p5}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v1 ).toString ( ); // invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
android.util.Slog .d ( v0,v1 );
/* .line 114 */
} // :cond_0
if ( p1 != null) { // if-eqz p1, :cond_1
/* .line 115 */
/* invoke-direct {p0, p3, p4}, Lcom/miui/server/smartpower/AppBluetoothResource;->getOrCreateBleRecord(II)Lcom/miui/server/smartpower/AppBluetoothResource$BleRecord; */
/* .line 116 */
/* .local v0, "record":Lcom/miui/server/smartpower/AppBluetoothResource$BleRecord; */
(( com.miui.server.smartpower.AppBluetoothResource$BleRecord ) v0 ).onBluetoothConnect ( p2, p5 ); // invoke-virtual {v0, p2, p5}, Lcom/miui/server/smartpower/AppBluetoothResource$BleRecord;->onBluetoothConnect(II)V
/* .line 117 */
} // .end local v0 # "record":Lcom/miui/server/smartpower/AppBluetoothResource$BleRecord;
/* .line 118 */
} // :cond_1
/* invoke-direct {p0, p4}, Lcom/miui/server/smartpower/AppBluetoothResource;->getBleRecord(I)Lcom/miui/server/smartpower/AppBluetoothResource$BleRecord; */
/* .line 119 */
/* .restart local v0 # "record":Lcom/miui/server/smartpower/AppBluetoothResource$BleRecord; */
if ( v0 != null) { // if-eqz v0, :cond_2
/* .line 120 */
(( com.miui.server.smartpower.AppBluetoothResource$BleRecord ) v0 ).onBluetoothDisconnect ( p2, p5 ); // invoke-virtual {v0, p2, p5}, Lcom/miui/server/smartpower/AppBluetoothResource$BleRecord;->onBluetoothDisconnect(II)V
/* .line 121 */
v1 = (( com.miui.server.smartpower.AppBluetoothResource$BleRecord ) v0 ).isActive ( ); // invoke-virtual {v0}, Lcom/miui/server/smartpower/AppBluetoothResource$BleRecord;->isActive()Z
/* if-nez v1, :cond_2 */
/* .line 122 */
v1 = this.mActivePidsMap;
/* monitor-enter v1 */
/* .line 123 */
try { // :try_start_0
v2 = this.mActivePidsMap;
(( android.util.SparseArray ) v2 ).remove ( p4 ); // invoke-virtual {v2, p4}, Landroid/util/SparseArray;->remove(I)V
/* .line 124 */
/* monitor-exit v1 */
/* :catchall_0 */
/* move-exception v2 */
/* monitor-exit v1 */
/* :try_end_0 */
/* .catchall {:try_start_0 ..:try_end_0} :catchall_0 */
/* throw v2 */
/* .line 128 */
} // .end local v0 # "record":Lcom/miui/server/smartpower/AppBluetoothResource$BleRecord;
} // :cond_2
} // :goto_0
return;
} // .end method
public void registerCallback ( com.miui.server.smartpower.AppPowerResource$IAppPowerResourceCallback p0, Integer p1 ) {
/* .locals 0 */
/* .param p1, "callback" # Lcom/miui/server/smartpower/AppPowerResource$IAppPowerResourceCallback; */
/* .param p2, "uid" # I */
/* .line 82 */
/* invoke-super {p0, p1, p2}, Lcom/miui/server/smartpower/AppPowerResource;->registerCallback(Lcom/miui/server/smartpower/AppPowerResource$IAppPowerResourceCallback;I)V */
/* .line 83 */
return;
} // .end method
public void registerCallback ( com.miui.server.smartpower.AppPowerResource$IAppPowerResourceCallback p0, Integer p1, Integer p2 ) {
/* .locals 0 */
/* .param p1, "callback" # Lcom/miui/server/smartpower/AppPowerResource$IAppPowerResourceCallback; */
/* .param p2, "uid" # I */
/* .param p3, "pid" # I */
/* .line 92 */
/* invoke-super {p0, p1, p2, p3}, Lcom/miui/server/smartpower/AppPowerResource;->registerCallback(Lcom/miui/server/smartpower/AppPowerResource$IAppPowerResourceCallback;II)V */
/* .line 93 */
return;
} // .end method
public void releaseAppPowerResource ( Integer p0 ) {
/* .locals 0 */
/* .param p1, "uid" # I */
/* .line 73 */
return;
} // .end method
public void resumeAppPowerResource ( Integer p0 ) {
/* .locals 0 */
/* .param p1, "uid" # I */
/* .line 78 */
return;
} // .end method
public void unRegisterCallback ( com.miui.server.smartpower.AppPowerResource$IAppPowerResourceCallback p0, Integer p1 ) {
/* .locals 0 */
/* .param p1, "callback" # Lcom/miui/server/smartpower/AppPowerResource$IAppPowerResourceCallback; */
/* .param p2, "uid" # I */
/* .line 87 */
/* invoke-super {p0, p1, p2}, Lcom/miui/server/smartpower/AppPowerResource;->unRegisterCallback(Lcom/miui/server/smartpower/AppPowerResource$IAppPowerResourceCallback;I)V */
/* .line 88 */
return;
} // .end method
public void unRegisterCallback ( com.miui.server.smartpower.AppPowerResource$IAppPowerResourceCallback p0, Integer p1, Integer p2 ) {
/* .locals 2 */
/* .param p1, "callback" # Lcom/miui/server/smartpower/AppPowerResource$IAppPowerResourceCallback; */
/* .param p2, "uid" # I */
/* .param p3, "pid" # I */
/* .line 97 */
v0 = this.mActivePidsMap;
/* monitor-enter v0 */
/* .line 98 */
try { // :try_start_0
v1 = this.mActivePidsMap;
(( android.util.SparseArray ) v1 ).get ( p3 ); // invoke-virtual {v1, p3}, Landroid/util/SparseArray;->get(I)Ljava/lang/Object;
if ( v1 != null) { // if-eqz v1, :cond_0
/* .line 99 */
v1 = this.mActivePidsMap;
(( android.util.SparseArray ) v1 ).remove ( p3 ); // invoke-virtual {v1, p3}, Landroid/util/SparseArray;->remove(I)V
/* .line 101 */
} // :cond_0
/* monitor-exit v0 */
/* :try_end_0 */
/* .catchall {:try_start_0 ..:try_end_0} :catchall_0 */
/* .line 102 */
/* invoke-super {p0, p1, p2, p3}, Lcom/miui/server/smartpower/AppPowerResource;->unRegisterCallback(Lcom/miui/server/smartpower/AppPowerResource$IAppPowerResourceCallback;II)V */
/* .line 103 */
return;
/* .line 101 */
/* :catchall_0 */
/* move-exception v1 */
try { // :try_start_1
/* monitor-exit v0 */
/* :try_end_1 */
/* .catchall {:try_start_1 ..:try_end_1} :catchall_0 */
/* throw v1 */
} // .end method
