public class com.miui.server.smartpower.SmartScenarioManager$ClientConfig {
	 /* .source "SmartScenarioManager.java" */
	 /* # annotations */
	 /* .annotation system Ldalvik/annotation/EnclosingClass; */
	 /* value = Lcom/miui/server/smartpower/SmartScenarioManager; */
} // .end annotation
/* .annotation system Ldalvik/annotation/InnerClass; */
/* accessFlags = 0x1 */
/* name = "ClientConfig" */
} // .end annotation
/* # instance fields */
private Long mAdditionalScenarioDescription;
private final android.util.LongSparseArray mAdditionalScenarioIdMap;
/* .annotation system Ldalvik/annotation/Signature; */
/* value = { */
/* "Landroid/util/LongSparseArray<", */
/* "Lcom/miui/server/smartpower/SmartScenarioManager$ScenarioIdInfo;", */
/* ">;" */
/* } */
} // .end annotation
} // .end field
private com.miui.server.smartpower.SmartScenarioManager$ISmartScenarioCallback mCallback;
java.lang.String mClientName;
private final android.util.LongSparseArray mMainScenarioIdMap;
/* .annotation system Ldalvik/annotation/Signature; */
/* value = { */
/* "Landroid/util/LongSparseArray<", */
/* "Lcom/miui/server/smartpower/SmartScenarioManager$ScenarioIdInfo;", */
/* ">;" */
/* } */
} // .end annotation
} // .end field
private Long mMainSenarioDescription;
final com.miui.server.smartpower.SmartScenarioManager this$0; //synthetic
/* # direct methods */
static Long -$$Nest$fgetmAdditionalScenarioDescription ( com.miui.server.smartpower.SmartScenarioManager$ClientConfig p0 ) { //bridge//synthethic
/* .locals 2 */
/* iget-wide v0, p0, Lcom/miui/server/smartpower/SmartScenarioManager$ClientConfig;->mAdditionalScenarioDescription:J */
/* return-wide v0 */
} // .end method
static com.miui.server.smartpower.SmartScenarioManager$ISmartScenarioCallback -$$Nest$fgetmCallback ( com.miui.server.smartpower.SmartScenarioManager$ClientConfig p0 ) { //bridge//synthethic
/* .locals 0 */
p0 = this.mCallback;
} // .end method
static Long -$$Nest$fgetmMainSenarioDescription ( com.miui.server.smartpower.SmartScenarioManager$ClientConfig p0 ) { //bridge//synthethic
/* .locals 2 */
/* iget-wide v0, p0, Lcom/miui/server/smartpower/SmartScenarioManager$ClientConfig;->mMainSenarioDescription:J */
/* return-wide v0 */
} // .end method
public com.miui.server.smartpower.SmartScenarioManager$ClientConfig ( ) {
/* .locals 1 */
/* .param p1, "this$0" # Lcom/miui/server/smartpower/SmartScenarioManager; */
/* .param p2, "clientName" # Ljava/lang/String; */
/* .param p3, "callback" # Lcom/miui/server/smartpower/SmartScenarioManager$ISmartScenarioCallback; */
/* .line 341 */
this.this$0 = p1;
/* invoke-direct {p0}, Ljava/lang/Object;-><init>()V */
/* .line 337 */
/* new-instance v0, Landroid/util/LongSparseArray; */
/* invoke-direct {v0}, Landroid/util/LongSparseArray;-><init>()V */
this.mMainScenarioIdMap = v0;
/* .line 338 */
/* new-instance v0, Landroid/util/LongSparseArray; */
/* invoke-direct {v0}, Landroid/util/LongSparseArray;-><init>()V */
this.mAdditionalScenarioIdMap = v0;
/* .line 342 */
this.mClientName = p2;
/* .line 343 */
this.mCallback = p3;
/* .line 344 */
return;
} // .end method
private void onAdditionalScenarioChanged ( Integer p0, java.lang.String p1, Boolean p2 ) {
/* .locals 7 */
/* .param p1, "action" # I */
/* .param p2, "packageName" # Ljava/lang/String; */
/* .param p3, "add" # Z */
/* .line 409 */
/* iget-wide v0, p0, Lcom/miui/server/smartpower/SmartScenarioManager$ClientConfig;->mAdditionalScenarioDescription:J */
/* .line 410 */
/* .local v0, "currentScenarioDescription":J */
v2 = this.mAdditionalScenarioIdMap;
/* monitor-enter v2 */
/* .line 411 */
int v3 = 0; // const/4 v3, 0x0
/* .local v3, "i":I */
} // :goto_0
try { // :try_start_0
v4 = this.mAdditionalScenarioIdMap;
v4 = (( android.util.LongSparseArray ) v4 ).size ( ); // invoke-virtual {v4}, Landroid/util/LongSparseArray;->size()I
/* if-ge v3, v4, :cond_4 */
/* .line 412 */
v4 = this.mAdditionalScenarioIdMap;
(( android.util.LongSparseArray ) v4 ).valueAt ( v3 ); // invoke-virtual {v4, v3}, Landroid/util/LongSparseArray;->valueAt(I)Ljava/lang/Object;
/* check-cast v4, Lcom/miui/server/smartpower/SmartScenarioManager$ScenarioIdInfo; */
/* .line 413 */
/* .local v4, "info":Lcom/miui/server/smartpower/SmartScenarioManager$ScenarioIdInfo; */
v5 = com.miui.server.smartpower.SmartScenarioManager$ScenarioIdInfo .-$$Nest$fgetmAction ( v4 );
/* and-int/2addr v5, p1 */
if ( v5 != null) { // if-eqz v5, :cond_3
if ( p2 != null) { // if-eqz p2, :cond_0
com.miui.server.smartpower.SmartScenarioManager$ScenarioIdInfo .-$$Nest$fgetmPackageName ( v4 );
/* .line 414 */
v5 = (( java.lang.String ) p2 ).equals ( v5 ); // invoke-virtual {p2, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z
/* if-nez v5, :cond_1 */
} // :cond_0
/* if-nez p2, :cond_3 */
com.miui.server.smartpower.SmartScenarioManager$ScenarioIdInfo .-$$Nest$fgetmPackageName ( v4 );
/* if-nez v5, :cond_3 */
/* .line 416 */
} // :cond_1
if ( p3 != null) { // if-eqz p3, :cond_2
/* .line 417 */
com.miui.server.smartpower.SmartScenarioManager$ScenarioIdInfo .-$$Nest$fgetmId ( v4 );
/* move-result-wide v5 */
/* or-long/2addr v0, v5 */
/* .line 419 */
} // :cond_2
com.miui.server.smartpower.SmartScenarioManager$ScenarioIdInfo .-$$Nest$fgetmId ( v4 );
/* move-result-wide v5 */
/* not-long v5, v5 */
/* and-long/2addr v0, v5 */
/* .line 411 */
} // .end local v4 # "info":Lcom/miui/server/smartpower/SmartScenarioManager$ScenarioIdInfo;
} // :cond_3
} // :goto_1
/* add-int/lit8 v3, v3, 0x1 */
/* .line 423 */
} // .end local v3 # "i":I
} // :cond_4
/* monitor-exit v2 */
/* :try_end_0 */
/* .catchall {:try_start_0 ..:try_end_0} :catchall_0 */
/* .line 424 */
/* iget-wide v2, p0, Lcom/miui/server/smartpower/SmartScenarioManager$ClientConfig;->mAdditionalScenarioDescription:J */
/* cmp-long v2, v0, v2 */
if ( v2 != null) { // if-eqz v2, :cond_5
/* .line 425 */
/* iput-wide v0, p0, Lcom/miui/server/smartpower/SmartScenarioManager$ClientConfig;->mAdditionalScenarioDescription:J */
/* .line 426 */
/* iget-wide v2, p0, Lcom/miui/server/smartpower/SmartScenarioManager$ClientConfig;->mMainSenarioDescription:J */
/* invoke-direct {p0, v2, v3, v0, v1}, Lcom/miui/server/smartpower/SmartScenarioManager$ClientConfig;->reportSenarioChanged(JJ)V */
/* .line 428 */
} // :cond_5
return;
/* .line 423 */
/* :catchall_0 */
/* move-exception v3 */
try { // :try_start_1
/* monitor-exit v2 */
/* :try_end_1 */
/* .catchall {:try_start_1 ..:try_end_1} :catchall_0 */
/* throw v3 */
} // .end method
private void onMainScenarioChanged ( Integer p0, java.lang.String p1, Boolean p2 ) {
/* .locals 7 */
/* .param p1, "action" # I */
/* .param p2, "packageName" # Ljava/lang/String; */
/* .param p3, "add" # Z */
/* .line 372 */
/* iget-wide v0, p0, Lcom/miui/server/smartpower/SmartScenarioManager$ClientConfig;->mMainSenarioDescription:J */
/* .line 373 */
/* .local v0, "currentScenarioDescription":J */
v2 = this.mMainScenarioIdMap;
/* monitor-enter v2 */
/* .line 374 */
int v3 = 0; // const/4 v3, 0x0
/* .local v3, "i":I */
} // :goto_0
try { // :try_start_0
v4 = this.mMainScenarioIdMap;
v4 = (( android.util.LongSparseArray ) v4 ).size ( ); // invoke-virtual {v4}, Landroid/util/LongSparseArray;->size()I
/* if-ge v3, v4, :cond_4 */
/* .line 375 */
v4 = this.mMainScenarioIdMap;
(( android.util.LongSparseArray ) v4 ).valueAt ( v3 ); // invoke-virtual {v4, v3}, Landroid/util/LongSparseArray;->valueAt(I)Ljava/lang/Object;
/* check-cast v4, Lcom/miui/server/smartpower/SmartScenarioManager$ScenarioIdInfo; */
/* .line 376 */
/* .local v4, "info":Lcom/miui/server/smartpower/SmartScenarioManager$ScenarioIdInfo; */
v5 = com.miui.server.smartpower.SmartScenarioManager$ScenarioIdInfo .-$$Nest$fgetmAction ( v4 );
/* and-int/2addr v5, p1 */
if ( v5 != null) { // if-eqz v5, :cond_3
if ( p2 != null) { // if-eqz p2, :cond_0
com.miui.server.smartpower.SmartScenarioManager$ScenarioIdInfo .-$$Nest$fgetmPackageName ( v4 );
/* .line 377 */
v5 = (( java.lang.String ) p2 ).equals ( v5 ); // invoke-virtual {p2, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z
/* if-nez v5, :cond_1 */
} // :cond_0
/* if-nez p2, :cond_3 */
com.miui.server.smartpower.SmartScenarioManager$ScenarioIdInfo .-$$Nest$fgetmPackageName ( v4 );
/* if-nez v5, :cond_3 */
/* .line 379 */
} // :cond_1
if ( p3 != null) { // if-eqz p3, :cond_2
/* .line 380 */
com.miui.server.smartpower.SmartScenarioManager$ScenarioIdInfo .-$$Nest$fgetmId ( v4 );
/* move-result-wide v5 */
/* or-long/2addr v0, v5 */
/* .line 382 */
} // :cond_2
com.miui.server.smartpower.SmartScenarioManager$ScenarioIdInfo .-$$Nest$fgetmId ( v4 );
/* move-result-wide v5 */
/* not-long v5, v5 */
/* and-long/2addr v0, v5 */
/* .line 374 */
} // .end local v4 # "info":Lcom/miui/server/smartpower/SmartScenarioManager$ScenarioIdInfo;
} // :cond_3
} // :goto_1
/* add-int/lit8 v3, v3, 0x1 */
/* .line 386 */
} // .end local v3 # "i":I
} // :cond_4
/* monitor-exit v2 */
/* :try_end_0 */
/* .catchall {:try_start_0 ..:try_end_0} :catchall_0 */
/* .line 387 */
/* iget-wide v2, p0, Lcom/miui/server/smartpower/SmartScenarioManager$ClientConfig;->mMainSenarioDescription:J */
/* cmp-long v2, v0, v2 */
if ( v2 != null) { // if-eqz v2, :cond_5
/* .line 388 */
/* iput-wide v0, p0, Lcom/miui/server/smartpower/SmartScenarioManager$ClientConfig;->mMainSenarioDescription:J */
/* .line 389 */
/* iget-wide v2, p0, Lcom/miui/server/smartpower/SmartScenarioManager$ClientConfig;->mAdditionalScenarioDescription:J */
/* invoke-direct {p0, v0, v1, v2, v3}, Lcom/miui/server/smartpower/SmartScenarioManager$ClientConfig;->reportSenarioChanged(JJ)V */
/* .line 391 */
} // :cond_5
return;
/* .line 386 */
/* :catchall_0 */
/* move-exception v3 */
try { // :try_start_1
/* monitor-exit v2 */
/* :try_end_1 */
/* .catchall {:try_start_1 ..:try_end_1} :catchall_0 */
/* throw v3 */
} // .end method
private void reportSenarioChanged ( Long p0, Long p1 ) {
/* .locals 8 */
/* .param p1, "mainScenario" # J */
/* .param p3, "additionalScenario" # J */
/* .line 394 */
v0 = this.this$0;
com.miui.server.smartpower.SmartScenarioManager .-$$Nest$fgetmHandler ( v0 );
/* new-instance v7, Lcom/miui/server/smartpower/SmartScenarioManager$ClientConfig$1; */
/* move-object v1, v7 */
/* move-object v2, p0 */
/* move-wide v3, p1 */
/* move-wide v5, p3 */
/* invoke-direct/range {v1 ..v6}, Lcom/miui/server/smartpower/SmartScenarioManager$ClientConfig$1;-><init>(Lcom/miui/server/smartpower/SmartScenarioManager$ClientConfig;JJ)V */
(( android.os.Handler ) v0 ).post ( v7 ); // invoke-virtual {v0, v7}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z
/* .line 406 */
return;
} // .end method
/* # virtual methods */
public void addAdditionalScenarioIdConfig ( Long p0, java.lang.String p1, Integer p2 ) {
/* .locals 9 */
/* .param p1, "id" # J */
/* .param p3, "packageName" # Ljava/lang/String; */
/* .param p4, "action" # I */
/* .line 353 */
/* const/16 v0, 0x8 */
/* if-ne p4, v0, :cond_0 */
/* .line 354 */
v0 = this.this$0;
com.miui.server.smartpower.SmartScenarioManager .-$$Nest$fgetmInterestBgPkgs ( v0 );
/* monitor-enter v0 */
/* .line 355 */
try { // :try_start_0
v1 = this.this$0;
com.miui.server.smartpower.SmartScenarioManager .-$$Nest$fgetmInterestBgPkgs ( v1 );
(( android.util.ArraySet ) v1 ).add ( p3 ); // invoke-virtual {v1, p3}, Landroid/util/ArraySet;->add(Ljava/lang/Object;)Z
/* .line 356 */
/* monitor-exit v0 */
/* :catchall_0 */
/* move-exception v1 */
/* monitor-exit v0 */
/* :try_end_0 */
/* .catchall {:try_start_0 ..:try_end_0} :catchall_0 */
/* throw v1 */
/* .line 358 */
} // :cond_0
} // :goto_0
v0 = this.mAdditionalScenarioIdMap;
/* monitor-enter v0 */
/* .line 359 */
try { // :try_start_1
v1 = this.mAdditionalScenarioIdMap;
/* new-instance v8, Lcom/miui/server/smartpower/SmartScenarioManager$ScenarioIdInfo; */
v3 = this.this$0;
/* move-object v2, v8 */
/* move-object v4, p3 */
/* move v5, p4 */
/* move-wide v6, p1 */
/* invoke-direct/range {v2 ..v7}, Lcom/miui/server/smartpower/SmartScenarioManager$ScenarioIdInfo;-><init>(Lcom/miui/server/smartpower/SmartScenarioManager;Ljava/lang/String;IJ)V */
(( android.util.LongSparseArray ) v1 ).put ( p1, p2, v8 ); // invoke-virtual {v1, p1, p2, v8}, Landroid/util/LongSparseArray;->put(JLjava/lang/Object;)V
/* .line 360 */
/* monitor-exit v0 */
/* .line 361 */
return;
/* .line 360 */
/* :catchall_1 */
/* move-exception v1 */
/* monitor-exit v0 */
/* :try_end_1 */
/* .catchall {:try_start_1 ..:try_end_1} :catchall_1 */
/* throw v1 */
} // .end method
public void addMainScenarioIdConfig ( Long p0, java.lang.String p1, Integer p2 ) {
/* .locals 9 */
/* .param p1, "id" # J */
/* .param p3, "packageName" # Ljava/lang/String; */
/* .param p4, "action" # I */
/* .line 347 */
v0 = this.mMainScenarioIdMap;
/* monitor-enter v0 */
/* .line 348 */
try { // :try_start_0
v1 = this.mMainScenarioIdMap;
/* new-instance v8, Lcom/miui/server/smartpower/SmartScenarioManager$ScenarioIdInfo; */
v3 = this.this$0;
/* move-object v2, v8 */
/* move-object v4, p3 */
/* move v5, p4 */
/* move-wide v6, p1 */
/* invoke-direct/range {v2 ..v7}, Lcom/miui/server/smartpower/SmartScenarioManager$ScenarioIdInfo;-><init>(Lcom/miui/server/smartpower/SmartScenarioManager;Ljava/lang/String;IJ)V */
(( android.util.LongSparseArray ) v1 ).put ( p1, p2, v8 ); // invoke-virtual {v1, p1, p2, v8}, Landroid/util/LongSparseArray;->put(JLjava/lang/Object;)V
/* .line 349 */
/* monitor-exit v0 */
/* .line 350 */
return;
/* .line 349 */
/* :catchall_0 */
/* move-exception v1 */
/* monitor-exit v0 */
/* :try_end_0 */
/* .catchall {:try_start_0 ..:try_end_0} :catchall_0 */
/* throw v1 */
} // .end method
public void dump ( java.io.PrintWriter p0, java.lang.String[] p1, Integer p2 ) {
/* .locals 9 */
/* .param p1, "pw" # Ljava/io/PrintWriter; */
/* .param p2, "args" # [Ljava/lang/String; */
/* .param p3, "opti" # I */
/* .line 431 */
/* new-instance v0, Ljava/lang/StringBuilder; */
/* invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V */
final String v1 = "client "; // const-string v1, "client "
(( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
v1 = this.mClientName;
(( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
final String v1 = ":"; // const-string v1, ":"
(( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v0 ).toString ( ); // invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
(( java.io.PrintWriter ) p1 ).println ( v0 ); // invoke-virtual {p1, v0}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V
/* .line 432 */
final String v0 = " main scenario:"; // const-string v0, " main scenario:"
(( java.io.PrintWriter ) p1 ).println ( v0 ); // invoke-virtual {p1, v0}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V
/* .line 433 */
v0 = this.mMainScenarioIdMap;
/* monitor-enter v0 */
/* .line 434 */
int v1 = 0; // const/4 v1, 0x0
/* .local v1, "i":I */
} // :goto_0
try { // :try_start_0
v2 = this.mMainScenarioIdMap;
v2 = (( android.util.LongSparseArray ) v2 ).size ( ); // invoke-virtual {v2}, Landroid/util/LongSparseArray;->size()I
/* const-wide/16 v3, 0x0 */
/* if-ge v1, v2, :cond_1 */
/* .line 435 */
v2 = this.mMainScenarioIdMap;
(( android.util.LongSparseArray ) v2 ).valueAt ( v1 ); // invoke-virtual {v2, v1}, Landroid/util/LongSparseArray;->valueAt(I)Ljava/lang/Object;
/* check-cast v2, Lcom/miui/server/smartpower/SmartScenarioManager$ScenarioIdInfo; */
/* .line 436 */
/* .local v2, "info":Lcom/miui/server/smartpower/SmartScenarioManager$ScenarioIdInfo; */
com.miui.server.smartpower.SmartScenarioManager$ScenarioIdInfo .-$$Nest$fgetmId ( v2 );
/* move-result-wide v5 */
/* iget-wide v7, p0, Lcom/miui/server/smartpower/SmartScenarioManager$ClientConfig;->mMainSenarioDescription:J */
/* and-long/2addr v5, v7 */
/* cmp-long v3, v5, v3 */
if ( v3 != null) { // if-eqz v3, :cond_0
/* .line 437 */
/* new-instance v3, Ljava/lang/StringBuilder; */
/* invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V */
final String v4 = " "; // const-string v4, " "
(( java.lang.StringBuilder ) v3 ).append ( v4 ); // invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v3 ).append ( v2 ); // invoke-virtual {v3, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v3 ).toString ( ); // invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
(( java.io.PrintWriter ) p1 ).println ( v3 ); // invoke-virtual {p1, v3}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V
/* .line 434 */
} // .end local v2 # "info":Lcom/miui/server/smartpower/SmartScenarioManager$ScenarioIdInfo;
} // :cond_0
/* add-int/lit8 v1, v1, 0x1 */
/* .line 440 */
} // .end local v1 # "i":I
} // :cond_1
/* monitor-exit v0 */
/* :try_end_0 */
/* .catchall {:try_start_0 ..:try_end_0} :catchall_1 */
/* .line 442 */
final String v0 = " addtional scenario:"; // const-string v0, " addtional scenario:"
(( java.io.PrintWriter ) p1 ).println ( v0 ); // invoke-virtual {p1, v0}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V
/* .line 443 */
v1 = this.mAdditionalScenarioIdMap;
/* monitor-enter v1 */
/* .line 444 */
int v0 = 0; // const/4 v0, 0x0
/* .local v0, "i":I */
} // :goto_1
try { // :try_start_1
v2 = this.mAdditionalScenarioIdMap;
v2 = (( android.util.LongSparseArray ) v2 ).size ( ); // invoke-virtual {v2}, Landroid/util/LongSparseArray;->size()I
/* if-ge v0, v2, :cond_3 */
/* .line 445 */
v2 = this.mAdditionalScenarioIdMap;
(( android.util.LongSparseArray ) v2 ).valueAt ( v0 ); // invoke-virtual {v2, v0}, Landroid/util/LongSparseArray;->valueAt(I)Ljava/lang/Object;
/* check-cast v2, Lcom/miui/server/smartpower/SmartScenarioManager$ScenarioIdInfo; */
/* .line 446 */
/* .restart local v2 # "info":Lcom/miui/server/smartpower/SmartScenarioManager$ScenarioIdInfo; */
com.miui.server.smartpower.SmartScenarioManager$ScenarioIdInfo .-$$Nest$fgetmId ( v2 );
/* move-result-wide v5 */
/* iget-wide v7, p0, Lcom/miui/server/smartpower/SmartScenarioManager$ClientConfig;->mAdditionalScenarioDescription:J */
/* and-long/2addr v5, v7 */
/* cmp-long v5, v5, v3 */
if ( v5 != null) { // if-eqz v5, :cond_2
/* .line 447 */
/* new-instance v5, Ljava/lang/StringBuilder; */
/* invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V */
final String v6 = " "; // const-string v6, " "
(( java.lang.StringBuilder ) v5 ).append ( v6 ); // invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v5 ).append ( v2 ); // invoke-virtual {v5, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v5 ).toString ( ); // invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
(( java.io.PrintWriter ) p1 ).println ( v5 ); // invoke-virtual {p1, v5}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V
/* .line 444 */
} // .end local v2 # "info":Lcom/miui/server/smartpower/SmartScenarioManager$ScenarioIdInfo;
} // :cond_2
/* add-int/lit8 v0, v0, 0x1 */
/* .line 450 */
} // .end local v0 # "i":I
} // :cond_3
/* monitor-exit v1 */
/* .line 451 */
return;
/* .line 450 */
/* :catchall_0 */
/* move-exception v0 */
/* monitor-exit v1 */
/* :try_end_1 */
/* .catchall {:try_start_1 ..:try_end_1} :catchall_0 */
/* throw v0 */
/* .line 440 */
/* :catchall_1 */
/* move-exception v1 */
try { // :try_start_2
/* monitor-exit v0 */
/* :try_end_2 */
/* .catchall {:try_start_2 ..:try_end_2} :catchall_1 */
/* throw v1 */
} // .end method
public void onAppActionChanged ( Integer p0, java.lang.String p1, Boolean p2 ) {
/* .locals 1 */
/* .param p1, "action" # I */
/* .param p2, "packageName" # Ljava/lang/String; */
/* .param p3, "add" # Z */
/* .line 364 */
/* and-int/lit8 v0, p1, 0x6 */
if ( v0 != null) { // if-eqz v0, :cond_0
/* .line 365 */
/* invoke-direct {p0, p1, p2, p3}, Lcom/miui/server/smartpower/SmartScenarioManager$ClientConfig;->onMainScenarioChanged(ILjava/lang/String;Z)V */
/* .line 366 */
} // :cond_0
/* const v0, 0x1fff8 */
/* and-int/2addr v0, p1 */
if ( v0 != null) { // if-eqz v0, :cond_1
/* .line 367 */
/* invoke-direct {p0, p1, p2, p3}, Lcom/miui/server/smartpower/SmartScenarioManager$ClientConfig;->onAdditionalScenarioChanged(ILjava/lang/String;Z)V */
/* .line 369 */
} // :cond_1
} // :goto_0
return;
} // .end method
