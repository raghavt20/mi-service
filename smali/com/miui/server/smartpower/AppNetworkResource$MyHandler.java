class com.miui.server.smartpower.AppNetworkResource$MyHandler extends android.os.Handler {
	 /* .source "AppNetworkResource.java" */
	 /* # annotations */
	 /* .annotation system Ldalvik/annotation/EnclosingClass; */
	 /* value = Lcom/miui/server/smartpower/AppNetworkResource; */
} // .end annotation
/* .annotation system Ldalvik/annotation/InnerClass; */
/* accessFlags = 0x2 */
/* name = "MyHandler" */
} // .end annotation
/* # instance fields */
final com.miui.server.smartpower.AppNetworkResource this$0; //synthetic
/* # direct methods */
 com.miui.server.smartpower.AppNetworkResource$MyHandler ( ) {
/* .locals 0 */
/* .param p2, "looper" # Landroid/os/Looper; */
/* .line 149 */
this.this$0 = p1;
/* .line 150 */
/* invoke-direct {p0, p2}, Landroid/os/Handler;-><init>(Landroid/os/Looper;)V */
/* .line 151 */
return;
} // .end method
/* # virtual methods */
public void handleMessage ( android.os.Message p0 ) {
/* .locals 6 */
/* .param p1, "msg" # Landroid/os/Message; */
/* .line 155 */
/* invoke-super {p0, p1}, Landroid/os/Handler;->handleMessage(Landroid/os/Message;)V */
/* .line 156 */
/* iget v0, p1, Landroid/os/Message;->what:I */
int v1 = 1; // const/4 v1, 0x1
/* if-ne v0, v1, :cond_1 */
/* .line 157 */
v0 = this.obj;
/* check-cast v0, Lcom/miui/server/smartpower/AppNetworkResource$NetworkMonitor; */
/* .line 158 */
/* .local v0, "monitor":Lcom/miui/server/smartpower/AppNetworkResource$NetworkMonitor; */
v2 = this.this$0;
com.miui.server.smartpower.AppNetworkResource .-$$Nest$fgetmNetworkMonitorMap ( v2 );
/* monitor-enter v2 */
/* .line 159 */
try { // :try_start_0
	 v3 = this.this$0;
	 com.miui.server.smartpower.AppNetworkResource .-$$Nest$fgetmNetworkMonitorMap ( v3 );
	 v3 = 	 (( java.util.HashMap ) v3 ).containsValue ( v0 ); // invoke-virtual {v3, v0}, Ljava/util/HashMap;->containsValue(Ljava/lang/Object;)Z
	 /* if-nez v3, :cond_0 */
	 /* .line 160 */
	 /* monitor-exit v2 */
	 return;
	 /* .line 162 */
} // :cond_0
com.miui.server.smartpower.AppNetworkResource$NetworkMonitor .-$$Nest$mupdateNetworkStatus ( v0 );
/* .line 163 */
v3 = this.this$0;
com.miui.server.smartpower.AppNetworkResource .-$$Nest$fgetmHandler ( v3 );
(( android.os.Handler ) v3 ).obtainMessage ( v1, v0 ); // invoke-virtual {v3, v1, v0}, Landroid/os/Handler;->obtainMessage(ILjava/lang/Object;)Landroid/os/Message;
/* .line 164 */
/* .local v1, "message":Landroid/os/Message; */
v3 = this.this$0;
com.miui.server.smartpower.AppNetworkResource .-$$Nest$fgetmHandler ( v3 );
/* const-wide/16 v4, 0x3e8 */
(( android.os.Handler ) v3 ).sendMessageDelayed ( v1, v4, v5 ); // invoke-virtual {v3, v1, v4, v5}, Landroid/os/Handler;->sendMessageDelayed(Landroid/os/Message;J)Z
/* .line 165 */
/* nop */
} // .end local v1 # "message":Landroid/os/Message;
/* monitor-exit v2 */
/* :catchall_0 */
/* move-exception v1 */
/* monitor-exit v2 */
/* :try_end_0 */
/* .catchall {:try_start_0 ..:try_end_0} :catchall_0 */
/* throw v1 */
/* .line 167 */
} // .end local v0 # "monitor":Lcom/miui/server/smartpower/AppNetworkResource$NetworkMonitor;
} // :cond_1
} // :goto_0
return;
} // .end method
