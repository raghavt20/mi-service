class com.miui.server.smartpower.AppDisplayResource$DisplayRecord {
	 /* .source "AppDisplayResource.java" */
	 /* # annotations */
	 /* .annotation system Ldalvik/annotation/EnclosingClass; */
	 /* value = Lcom/miui/server/smartpower/AppDisplayResource; */
} // .end annotation
/* .annotation system Ldalvik/annotation/InnerClass; */
/* accessFlags = 0x0 */
/* name = "DisplayRecord" */
} // .end annotation
/* # instance fields */
private Boolean mActive;
private Integer mBehavier;
private final android.util.ArrayMap mDisplays;
/* .annotation system Ldalvik/annotation/Signature; */
/* value = { */
/* "Landroid/util/ArrayMap<", */
/* "Ljava/lang/Integer;", */
/* "Landroid/view/Display;", */
/* ">;" */
/* } */
} // .end annotation
} // .end field
private final Integer mUid;
final com.miui.server.smartpower.AppDisplayResource this$0; //synthetic
/* # direct methods */
static Integer -$$Nest$fgetmBehavier ( com.miui.server.smartpower.AppDisplayResource$DisplayRecord p0 ) { //bridge//synthethic
/* .locals 0 */
/* iget p0, p0, Lcom/miui/server/smartpower/AppDisplayResource$DisplayRecord;->mBehavier:I */
} // .end method
static Integer -$$Nest$fgetmUid ( com.miui.server.smartpower.AppDisplayResource$DisplayRecord p0 ) { //bridge//synthethic
/* .locals 0 */
/* iget p0, p0, Lcom/miui/server/smartpower/AppDisplayResource$DisplayRecord;->mUid:I */
} // .end method
static void -$$Nest$mupdateCurrentStatus ( com.miui.server.smartpower.AppDisplayResource$DisplayRecord p0 ) { //bridge//synthethic
/* .locals 0 */
/* invoke-direct {p0}, Lcom/miui/server/smartpower/AppDisplayResource$DisplayRecord;->updateCurrentStatus()V */
return;
} // .end method
 com.miui.server.smartpower.AppDisplayResource$DisplayRecord ( ) {
/* .locals 1 */
/* .param p1, "this$0" # Lcom/miui/server/smartpower/AppDisplayResource; */
/* .param p2, "uid" # I */
/* .line 190 */
this.this$0 = p1;
/* invoke-direct {p0}, Ljava/lang/Object;-><init>()V */
/* .line 188 */
/* new-instance v0, Landroid/util/ArrayMap; */
/* invoke-direct {v0}, Landroid/util/ArrayMap;-><init>()V */
this.mDisplays = v0;
/* .line 191 */
/* iput p2, p0, Lcom/miui/server/smartpower/AppDisplayResource$DisplayRecord;->mUid:I */
/* .line 192 */
return;
} // .end method
private void updateCurrentStatus ( ) {
/* .locals 7 */
/* .line 219 */
int v0 = 0; // const/4 v0, 0x0
/* .line 220 */
/* .local v0, "behavier":I */
int v1 = 0; // const/4 v1, 0x0
/* .line 221 */
/* .local v1, "active":Z */
v2 = this.mDisplays;
/* monitor-enter v2 */
/* .line 222 */
try { // :try_start_0
v3 = this.mDisplays;
v3 = (( android.util.ArrayMap ) v3 ).size ( ); // invoke-virtual {v3}, Landroid/util/ArrayMap;->size()I
/* if-lez v3, :cond_0 */
int v3 = 1; // const/4 v3, 0x1
} // :cond_0
int v3 = 0; // const/4 v3, 0x0
} // :goto_0
/* move v1, v3 */
/* .line 223 */
v3 = this.mDisplays;
(( android.util.ArrayMap ) v3 ).values ( ); // invoke-virtual {v3}, Landroid/util/ArrayMap;->values()Ljava/util/Collection;
v4 = } // :goto_1
if ( v4 != null) { // if-eqz v4, :cond_2
/* check-cast v4, Landroid/view/Display; */
/* .line 224 */
/* .local v4, "display":Landroid/view/Display; */
v5 = (( android.view.Display ) v4 ).getType ( ); // invoke-virtual {v4}, Landroid/view/Display;->getType()I
/* packed-switch v5, :pswitch_data_0 */
/* :pswitch_0 */
/* .line 232 */
/* :pswitch_1 */
v5 = this.this$0;
v5 = com.miui.server.smartpower.AppDisplayResource .-$$Nest$fgetmWifiP2pConnected ( v5 );
if ( v5 != null) { // if-eqz v5, :cond_1
v5 = this.this$0;
com.miui.server.smartpower.AppDisplayResource .-$$Nest$fgetmCastingWhitelists ( v5 );
/* .line 233 */
(( android.view.Display ) v4 ).getOwnerPackageName ( ); // invoke-virtual {v4}, Landroid/view/Display;->getOwnerPackageName()Ljava/lang/String;
v5 = (( android.util.ArraySet ) v5 ).contains ( v6 ); // invoke-virtual {v5, v6}, Landroid/util/ArraySet;->contains(Ljava/lang/Object;)Z
if ( v5 != null) { // if-eqz v5, :cond_1
/* .line 234 */
/* or-int/lit16 v0, v0, 0x100 */
/* .line 226 */
/* :pswitch_2 */
/* or-int/lit16 v0, v0, 0x100 */
/* .line 227 */
/* .line 229 */
/* :pswitch_3 */
/* or-int/lit16 v0, v0, 0x200 */
/* .line 230 */
/* nop */
/* .line 238 */
} // .end local v4 # "display":Landroid/view/Display;
} // :cond_1
} // :goto_2
/* .line 239 */
} // :cond_2
/* monitor-exit v2 */
/* :try_end_0 */
/* .catchall {:try_start_0 ..:try_end_0} :catchall_0 */
/* .line 240 */
/* iget-boolean v2, p0, Lcom/miui/server/smartpower/AppDisplayResource$DisplayRecord;->mActive:Z */
/* if-ne v1, v2, :cond_3 */
/* iget v2, p0, Lcom/miui/server/smartpower/AppDisplayResource$DisplayRecord;->mBehavier:I */
/* if-eq v0, v2, :cond_5 */
/* .line 241 */
} // :cond_3
/* sget-boolean v2, Lcom/miui/server/smartpower/AppPowerResource;->DEBUG:Z */
if ( v2 != null) { // if-eqz v2, :cond_4
/* .line 242 */
final String v2 = "SmartPower.AppResource"; // const-string v2, "SmartPower.AppResource"
/* new-instance v3, Ljava/lang/StringBuilder; */
/* invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V */
/* iget v4, p0, Lcom/miui/server/smartpower/AppDisplayResource$DisplayRecord;->mUid:I */
(( java.lang.StringBuilder ) v3 ).append ( v4 ); // invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;
final String v4 = " display active "; // const-string v4, " display active "
(( java.lang.StringBuilder ) v3 ).append ( v4 ); // invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
/* iget-boolean v4, p0, Lcom/miui/server/smartpower/AppDisplayResource$DisplayRecord;->mActive:Z */
(( java.lang.StringBuilder ) v3 ).append ( v4 ); // invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;
final String v4 = " "; // const-string v4, " "
(( java.lang.StringBuilder ) v3 ).append ( v4 ); // invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
/* iget v4, p0, Lcom/miui/server/smartpower/AppDisplayResource$DisplayRecord;->mBehavier:I */
(( java.lang.StringBuilder ) v3 ).append ( v4 ); // invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v3 ).toString ( ); // invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
android.util.Slog .d ( v2,v3 );
/* .line 244 */
} // :cond_4
/* new-instance v2, Ljava/lang/StringBuilder; */
/* invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V */
final String v3 = "display u:"; // const-string v3, "display u:"
(( java.lang.StringBuilder ) v2 ).append ( v3 ); // invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
/* iget v3, p0, Lcom/miui/server/smartpower/AppDisplayResource$DisplayRecord;->mUid:I */
(( java.lang.StringBuilder ) v2 ).append ( v3 ); // invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;
final String v3 = " s:"; // const-string v3, " s:"
(( java.lang.StringBuilder ) v2 ).append ( v3 ); // invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v2 ).append ( v1 ); // invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v2 ).toString ( ); // invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
/* const v3, 0x15ff2 */
android.util.EventLog .writeEvent ( v3,v2 );
/* .line 246 */
v2 = this.this$0;
/* iget v3, p0, Lcom/miui/server/smartpower/AppDisplayResource$DisplayRecord;->mUid:I */
(( com.miui.server.smartpower.AppDisplayResource ) v2 ).reportResourceStatus ( v3, v1, v0 ); // invoke-virtual {v2, v3, v1, v0}, Lcom/miui/server/smartpower/AppDisplayResource;->reportResourceStatus(IZI)V
/* .line 247 */
/* iput-boolean v1, p0, Lcom/miui/server/smartpower/AppDisplayResource$DisplayRecord;->mActive:Z */
/* .line 248 */
/* iput v0, p0, Lcom/miui/server/smartpower/AppDisplayResource$DisplayRecord;->mBehavier:I */
/* .line 250 */
} // :cond_5
return;
/* .line 239 */
/* :catchall_0 */
/* move-exception v3 */
try { // :try_start_1
/* monitor-exit v2 */
/* :try_end_1 */
/* .catchall {:try_start_1 ..:try_end_1} :catchall_0 */
/* throw v3 */
/* :pswitch_data_0 */
/* .packed-switch 0x2 */
/* :pswitch_3 */
/* :pswitch_2 */
/* :pswitch_0 */
/* :pswitch_1 */
} // .end packed-switch
} // .end method
/* # virtual methods */
Boolean isActive ( ) {
/* .locals 1 */
/* .line 195 */
/* iget-boolean v0, p0, Lcom/miui/server/smartpower/AppDisplayResource$DisplayRecord;->mActive:Z */
} // .end method
public void onDisplayAdded ( Integer p0, android.view.Display p1 ) {
/* .locals 4 */
/* .param p1, "displayId" # I */
/* .param p2, "display" # Landroid/view/Display; */
/* .line 199 */
v0 = this.mDisplays;
/* monitor-enter v0 */
/* .line 200 */
try { // :try_start_0
v1 = this.mDisplays;
java.lang.Integer .valueOf ( p1 );
(( android.util.ArrayMap ) v1 ).get ( v2 ); // invoke-virtual {v1, v2}, Landroid/util/ArrayMap;->get(Ljava/lang/Object;)Ljava/lang/Object;
/* check-cast v1, Landroid/view/Display; */
/* .line 201 */
/* .local v1, "displayRecord":Landroid/view/Display; */
/* if-nez v1, :cond_0 */
/* .line 202 */
v2 = this.mDisplays;
java.lang.Integer .valueOf ( p1 );
(( android.util.ArrayMap ) v2 ).put ( v3, p2 ); // invoke-virtual {v2, v3, p2}, Landroid/util/ArrayMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;
/* .line 204 */
} // .end local v1 # "displayRecord":Landroid/view/Display;
} // :cond_0
/* monitor-exit v0 */
/* :try_end_0 */
/* .catchall {:try_start_0 ..:try_end_0} :catchall_0 */
/* .line 205 */
/* invoke-direct {p0}, Lcom/miui/server/smartpower/AppDisplayResource$DisplayRecord;->updateCurrentStatus()V */
/* .line 206 */
return;
/* .line 204 */
/* :catchall_0 */
/* move-exception v1 */
try { // :try_start_1
/* monitor-exit v0 */
/* :try_end_1 */
/* .catchall {:try_start_1 ..:try_end_1} :catchall_0 */
/* throw v1 */
} // .end method
public Boolean onDisplayRemoved ( Integer p0 ) {
/* .locals 3 */
/* .param p1, "displayId" # I */
/* .line 209 */
v0 = this.mDisplays;
/* monitor-enter v0 */
/* .line 210 */
try { // :try_start_0
v1 = this.mDisplays;
java.lang.Integer .valueOf ( p1 );
(( android.util.ArrayMap ) v1 ).remove ( v2 ); // invoke-virtual {v1, v2}, Landroid/util/ArrayMap;->remove(Ljava/lang/Object;)Ljava/lang/Object;
/* if-nez v1, :cond_0 */
/* .line 211 */
/* monitor-exit v0 */
int v0 = 0; // const/4 v0, 0x0
/* .line 213 */
} // :cond_0
/* monitor-exit v0 */
/* :try_end_0 */
/* .catchall {:try_start_0 ..:try_end_0} :catchall_0 */
/* .line 214 */
/* invoke-direct {p0}, Lcom/miui/server/smartpower/AppDisplayResource$DisplayRecord;->updateCurrentStatus()V */
/* .line 215 */
int v0 = 1; // const/4 v0, 0x1
/* .line 213 */
/* :catchall_0 */
/* move-exception v1 */
try { // :try_start_1
/* monitor-exit v0 */
/* :try_end_1 */
/* .catchall {:try_start_1 ..:try_end_1} :catchall_0 */
/* throw v1 */
} // .end method
