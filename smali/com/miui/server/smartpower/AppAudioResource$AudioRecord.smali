.class Lcom/miui/server/smartpower/AppAudioResource$AudioRecord;
.super Ljava/lang/Object;
.source "AppAudioResource.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/miui/server/smartpower/AppAudioResource;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "AudioRecord"
.end annotation

.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/miui/server/smartpower/AppAudioResource$AudioRecord$RecorderRecord;,
        Lcom/miui/server/smartpower/AppAudioResource$AudioRecord$PlayerRecord;
    }
.end annotation


# instance fields
.field mActive:Z

.field mBehavier:I

.field mCurrentActive:Z

.field mCurrentBehavier:I

.field mCurrentPlaying:Z

.field mFocusLoss:I

.field final mFocusedClientIds:Landroid/util/ArrayMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/util/ArrayMap<",
            "Ljava/lang/String;",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation
.end field

.field mInactiveDelayTask:Ljava/lang/Runnable;

.field mLastPlayingTimeStamp:J

.field mOwnerPid:I

.field mOwnerUid:I

.field mPlaybackState:I

.field final mPlayerRecords:Landroid/util/ArrayMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/util/ArrayMap<",
            "Ljava/lang/Integer;",
            "Lcom/miui/server/smartpower/AppAudioResource$AudioRecord$PlayerRecord;",
            ">;"
        }
    .end annotation
.end field

.field mPlaying:Z

.field final mRecorderRecords:Landroid/util/ArrayMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/util/ArrayMap<",
            "Ljava/lang/Integer;",
            "Lcom/miui/server/smartpower/AppAudioResource$AudioRecord$RecorderRecord;",
            ">;"
        }
    .end annotation
.end field

.field final synthetic this$0:Lcom/miui/server/smartpower/AppAudioResource;


# direct methods
.method static bridge synthetic -$$Nest$mpauseZeroAudioTrack(Lcom/miui/server/smartpower/AppAudioResource$AudioRecord;)V
    .locals 0

    invoke-direct {p0}, Lcom/miui/server/smartpower/AppAudioResource$AudioRecord;->pauseZeroAudioTrack()V

    return-void
.end method

.method static bridge synthetic -$$Nest$mrealUpdateAudioStatus(Lcom/miui/server/smartpower/AppAudioResource$AudioRecord;)V
    .locals 0

    invoke-direct {p0}, Lcom/miui/server/smartpower/AppAudioResource$AudioRecord;->realUpdateAudioStatus()V

    return-void
.end method

.method static bridge synthetic -$$Nest$mupdateAudioStatus(Lcom/miui/server/smartpower/AppAudioResource$AudioRecord;)V
    .locals 0

    invoke-direct {p0}, Lcom/miui/server/smartpower/AppAudioResource$AudioRecord;->updateAudioStatus()V

    return-void
.end method

.method constructor <init>(Lcom/miui/server/smartpower/AppAudioResource;II)V
    .locals 2
    .param p2, "uid"    # I
    .param p3, "pid"    # I

    .line 233
    iput-object p1, p0, Lcom/miui/server/smartpower/AppAudioResource$AudioRecord;->this$0:Lcom/miui/server/smartpower/AppAudioResource;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 218
    const/4 p1, 0x0

    iput-boolean p1, p0, Lcom/miui/server/smartpower/AppAudioResource$AudioRecord;->mActive:Z

    .line 219
    iput-boolean p1, p0, Lcom/miui/server/smartpower/AppAudioResource$AudioRecord;->mCurrentActive:Z

    .line 220
    iput p1, p0, Lcom/miui/server/smartpower/AppAudioResource$AudioRecord;->mBehavier:I

    .line 221
    iput p1, p0, Lcom/miui/server/smartpower/AppAudioResource$AudioRecord;->mCurrentBehavier:I

    .line 222
    iput-boolean p1, p0, Lcom/miui/server/smartpower/AppAudioResource$AudioRecord;->mPlaying:Z

    .line 223
    iput-boolean p1, p0, Lcom/miui/server/smartpower/AppAudioResource$AudioRecord;->mCurrentPlaying:Z

    .line 224
    const/4 v0, -0x1

    iput v0, p0, Lcom/miui/server/smartpower/AppAudioResource$AudioRecord;->mPlaybackState:I

    .line 225
    new-instance v0, Landroid/util/ArrayMap;

    invoke-direct {v0}, Landroid/util/ArrayMap;-><init>()V

    iput-object v0, p0, Lcom/miui/server/smartpower/AppAudioResource$AudioRecord;->mFocusedClientIds:Landroid/util/ArrayMap;

    .line 226
    iput p1, p0, Lcom/miui/server/smartpower/AppAudioResource$AudioRecord;->mFocusLoss:I

    .line 227
    const-wide/16 v0, 0x0

    iput-wide v0, p0, Lcom/miui/server/smartpower/AppAudioResource$AudioRecord;->mLastPlayingTimeStamp:J

    .line 229
    new-instance p1, Landroid/util/ArrayMap;

    invoke-direct {p1}, Landroid/util/ArrayMap;-><init>()V

    iput-object p1, p0, Lcom/miui/server/smartpower/AppAudioResource$AudioRecord;->mPlayerRecords:Landroid/util/ArrayMap;

    .line 230
    new-instance p1, Landroid/util/ArrayMap;

    invoke-direct {p1}, Landroid/util/ArrayMap;-><init>()V

    iput-object p1, p0, Lcom/miui/server/smartpower/AppAudioResource$AudioRecord;->mRecorderRecords:Landroid/util/ArrayMap;

    .line 234
    iput p2, p0, Lcom/miui/server/smartpower/AppAudioResource$AudioRecord;->mOwnerUid:I

    .line 235
    iput p3, p0, Lcom/miui/server/smartpower/AppAudioResource$AudioRecord;->mOwnerPid:I

    .line 236
    return-void
.end method

.method private getPlayerRecord(I)Lcom/miui/server/smartpower/AppAudioResource$AudioRecord$PlayerRecord;
    .locals 3
    .param p1, "piid"    # I

    .line 478
    iget-object v0, p0, Lcom/miui/server/smartpower/AppAudioResource$AudioRecord;->mPlayerRecords:Landroid/util/ArrayMap;

    monitor-enter v0

    .line 479
    :try_start_0
    iget-object v1, p0, Lcom/miui/server/smartpower/AppAudioResource$AudioRecord;->mPlayerRecords:Landroid/util/ArrayMap;

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v1, v2}, Landroid/util/ArrayMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/miui/server/smartpower/AppAudioResource$AudioRecord$PlayerRecord;

    monitor-exit v0

    return-object v1

    .line 480
    :catchall_0
    move-exception v1

    monitor-exit v0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v1
.end method

.method private getRecorderRecord(I)Lcom/miui/server/smartpower/AppAudioResource$AudioRecord$RecorderRecord;
    .locals 3
    .param p1, "riid"    # I

    .line 484
    iget-object v0, p0, Lcom/miui/server/smartpower/AppAudioResource$AudioRecord;->mRecorderRecords:Landroid/util/ArrayMap;

    monitor-enter v0

    .line 485
    :try_start_0
    iget-object v1, p0, Lcom/miui/server/smartpower/AppAudioResource$AudioRecord;->mRecorderRecords:Landroid/util/ArrayMap;

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v1, v2}, Landroid/util/ArrayMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/miui/server/smartpower/AppAudioResource$AudioRecord$RecorderRecord;

    monitor-exit v0

    return-object v1

    .line 486
    :catchall_0
    move-exception v1

    monitor-exit v0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v1
.end method

.method private isZeroAudioRecord()Z
    .locals 5

    .line 490
    iget-object v0, p0, Lcom/miui/server/smartpower/AppAudioResource$AudioRecord;->mPlayerRecords:Landroid/util/ArrayMap;

    monitor-enter v0

    .line 491
    :try_start_0
    iget-object v1, p0, Lcom/miui/server/smartpower/AppAudioResource$AudioRecord;->mPlayerRecords:Landroid/util/ArrayMap;

    invoke-virtual {v1}, Landroid/util/ArrayMap;->values()Ljava/util/Collection;

    move-result-object v1

    invoke-interface {v1}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_1

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/miui/server/smartpower/AppAudioResource$AudioRecord$PlayerRecord;

    .line 492
    .local v2, "r":Lcom/miui/server/smartpower/AppAudioResource$AudioRecord$PlayerRecord;
    iget v3, v2, Lcom/miui/server/smartpower/AppAudioResource$AudioRecord$PlayerRecord;->mEvent:I

    const/16 v4, 0x64

    if-ne v3, v4, :cond_0

    .line 493
    monitor-exit v0

    const/4 v0, 0x1

    return v0

    .line 495
    .end local v2    # "r":Lcom/miui/server/smartpower/AppAudioResource$AudioRecord$PlayerRecord;
    :cond_0
    goto :goto_0

    .line 496
    :cond_1
    monitor-exit v0

    .line 497
    const/4 v0, 0x0

    return v0

    .line 496
    :catchall_0
    move-exception v1

    monitor-exit v0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v1
.end method

.method private pauseZeroAudioTrack()V
    .locals 6

    .line 501
    iget-object v0, p0, Lcom/miui/server/smartpower/AppAudioResource$AudioRecord;->mPlayerRecords:Landroid/util/ArrayMap;

    monitor-enter v0

    .line 502
    :try_start_0
    iget-object v1, p0, Lcom/miui/server/smartpower/AppAudioResource$AudioRecord;->mPlayerRecords:Landroid/util/ArrayMap;

    invoke-virtual {v1}, Landroid/util/ArrayMap;->values()Ljava/util/Collection;

    move-result-object v1

    invoke-interface {v1}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_1

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/miui/server/smartpower/AppAudioResource$AudioRecord$PlayerRecord;

    .line 503
    .local v2, "r":Lcom/miui/server/smartpower/AppAudioResource$AudioRecord$PlayerRecord;
    iget v3, v2, Lcom/miui/server/smartpower/AppAudioResource$AudioRecord$PlayerRecord;->mEvent:I

    const/16 v4, 0x64

    if-ne v3, v4, :cond_0

    .line 504
    iget v3, p0, Lcom/miui/server/smartpower/AppAudioResource$AudioRecord;->mOwnerUid:I

    iget v4, p0, Lcom/miui/server/smartpower/AppAudioResource$AudioRecord;->mOwnerPid:I

    iget v5, v2, Lcom/miui/server/smartpower/AppAudioResource$AudioRecord$PlayerRecord;->mSessionId:I

    invoke-static {v3, v4, v5}, Landroid/media/AudioSystem;->pauseAudioTracks(III)I

    .line 505
    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v4, "zero audio u:"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    iget v4, p0, Lcom/miui/server/smartpower/AppAudioResource$AudioRecord;->mOwnerUid:I

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, " session:"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    iget v4, v2, Lcom/miui/server/smartpower/AppAudioResource$AudioRecord$PlayerRecord;->mSessionId:I

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, " s:release"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    .line 507
    .local v3, "event":Ljava/lang/String;
    const v4, 0x15ff2

    invoke-static {v4, v3}, Landroid/util/EventLog;->writeEvent(ILjava/lang/String;)I

    .line 509
    .end local v2    # "r":Lcom/miui/server/smartpower/AppAudioResource$AudioRecord$PlayerRecord;
    .end local v3    # "event":Ljava/lang/String;
    :cond_0
    goto :goto_0

    .line 510
    :cond_1
    monitor-exit v0

    .line 511
    return-void

    .line 510
    :catchall_0
    move-exception v1

    monitor-exit v0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v1
.end method

.method private realUpdateAudioStatus()V
    .locals 5

    .line 463
    iget-boolean v0, p0, Lcom/miui/server/smartpower/AppAudioResource$AudioRecord;->mActive:Z

    iget-boolean v1, p0, Lcom/miui/server/smartpower/AppAudioResource$AudioRecord;->mCurrentActive:Z

    if-ne v0, v1, :cond_0

    iget v0, p0, Lcom/miui/server/smartpower/AppAudioResource$AudioRecord;->mBehavier:I

    iget v2, p0, Lcom/miui/server/smartpower/AppAudioResource$AudioRecord;->mCurrentBehavier:I

    if-eq v0, v2, :cond_2

    .line 464
    :cond_0
    iput-boolean v1, p0, Lcom/miui/server/smartpower/AppAudioResource$AudioRecord;->mActive:Z

    .line 465
    iget v0, p0, Lcom/miui/server/smartpower/AppAudioResource$AudioRecord;->mCurrentBehavier:I

    iput v0, p0, Lcom/miui/server/smartpower/AppAudioResource$AudioRecord;->mBehavier:I

    .line 466
    iget-object v1, p0, Lcom/miui/server/smartpower/AppAudioResource$AudioRecord;->this$0:Lcom/miui/server/smartpower/AppAudioResource;

    iget v2, p0, Lcom/miui/server/smartpower/AppAudioResource$AudioRecord;->mOwnerUid:I

    invoke-static {v1, v2, v0}, Lcom/miui/server/smartpower/AppAudioResource;->-$$Nest$mupdateUidStatus(Lcom/miui/server/smartpower/AppAudioResource;II)V

    .line 467
    iget-object v0, p0, Lcom/miui/server/smartpower/AppAudioResource$AudioRecord;->this$0:Lcom/miui/server/smartpower/AppAudioResource;

    iget v1, p0, Lcom/miui/server/smartpower/AppAudioResource$AudioRecord;->mOwnerUid:I

    iget v2, p0, Lcom/miui/server/smartpower/AppAudioResource$AudioRecord;->mOwnerPid:I

    iget-boolean v3, p0, Lcom/miui/server/smartpower/AppAudioResource$AudioRecord;->mActive:Z

    iget v4, p0, Lcom/miui/server/smartpower/AppAudioResource$AudioRecord;->mBehavier:I

    invoke-virtual {v0, v1, v2, v3, v4}, Lcom/miui/server/smartpower/AppAudioResource;->reportResourceStatus(IIZI)V

    .line 468
    sget-boolean v0, Lcom/miui/server/smartpower/AppPowerResource;->DEBUG:Z

    if-eqz v0, :cond_1

    .line 469
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    iget v1, p0, Lcom/miui/server/smartpower/AppAudioResource$AudioRecord;->mOwnerUid:I

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, " "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget v2, p0, Lcom/miui/server/smartpower/AppAudioResource$AudioRecord;->mOwnerPid:I

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v2, " audio active "

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-boolean v2, p0, Lcom/miui/server/smartpower/AppAudioResource$AudioRecord;->mActive:Z

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget v1, p0, Lcom/miui/server/smartpower/AppAudioResource$AudioRecord;->mBehavier:I

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    const-string v1, "SmartPower.AppResource"

    invoke-static {v1, v0}, Landroid/util/Slog;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 472
    :cond_1
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "audio u:"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget v1, p0, Lcom/miui/server/smartpower/AppAudioResource$AudioRecord;->mOwnerUid:I

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, " p:"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget v1, p0, Lcom/miui/server/smartpower/AppAudioResource$AudioRecord;->mOwnerPid:I

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, " s:"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-boolean v1, p0, Lcom/miui/server/smartpower/AppAudioResource$AudioRecord;->mActive:Z

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    const v1, 0x15ff2

    invoke-static {v1, v0}, Landroid/util/EventLog;->writeEvent(ILjava/lang/String;)I

    .line 475
    :cond_2
    return-void
.end method

.method private sendUpdateAudioStatusMsg()V
    .locals 2

    .line 362
    iget-object v0, p0, Lcom/miui/server/smartpower/AppAudioResource$AudioRecord;->this$0:Lcom/miui/server/smartpower/AppAudioResource;

    invoke-static {v0}, Lcom/miui/server/smartpower/AppAudioResource;->-$$Nest$fgetmHandler(Lcom/miui/server/smartpower/AppAudioResource;)Landroid/os/Handler;

    move-result-object v0

    new-instance v1, Lcom/miui/server/smartpower/AppAudioResource$AudioRecord$1;

    invoke-direct {v1, p0}, Lcom/miui/server/smartpower/AppAudioResource$AudioRecord$1;-><init>(Lcom/miui/server/smartpower/AppAudioResource$AudioRecord;)V

    invoke-virtual {v0, v1}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    .line 368
    return-void
.end method

.method private updateAudioStatus()V
    .locals 13

    .line 371
    const/4 v0, 0x0

    .line 372
    .local v0, "active":Z
    const/4 v1, 0x0

    .line 373
    .local v1, "behavier":I
    const/4 v2, 0x0

    .line 374
    .local v2, "playing":Z
    const/4 v3, 0x0

    .line 375
    .local v3, "inactiveDelay":Z
    invoke-static {}, Landroid/os/SystemClock;->uptimeMillis()J

    move-result-wide v4

    .line 376
    .local v4, "now":J
    const/4 v6, 0x0

    .line 378
    .local v6, "recentActive":Z
    iget-object v7, p0, Lcom/miui/server/smartpower/AppAudioResource$AudioRecord;->mPlayerRecords:Landroid/util/ArrayMap;

    monitor-enter v7

    .line 379
    :try_start_0
    iget-object v8, p0, Lcom/miui/server/smartpower/AppAudioResource$AudioRecord;->mPlayerRecords:Landroid/util/ArrayMap;

    invoke-virtual {v8}, Landroid/util/ArrayMap;->size()I

    move-result v8

    if-lez v8, :cond_0

    .line 380
    or-int/lit8 v1, v1, 0x4

    .line 382
    :cond_0
    iget-object v8, p0, Lcom/miui/server/smartpower/AppAudioResource$AudioRecord;->mPlayerRecords:Landroid/util/ArrayMap;

    invoke-virtual {v8}, Landroid/util/ArrayMap;->values()Ljava/util/Collection;

    move-result-object v8

    invoke-interface {v8}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v8

    :goto_0
    invoke-interface {v8}, Ljava/util/Iterator;->hasNext()Z

    move-result v9

    const/4 v10, 0x2

    if-eqz v9, :cond_3

    invoke-interface {v8}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v9

    check-cast v9, Lcom/miui/server/smartpower/AppAudioResource$AudioRecord$PlayerRecord;

    .line 383
    .local v9, "player":Lcom/miui/server/smartpower/AppAudioResource$AudioRecord$PlayerRecord;
    iget v11, v9, Lcom/miui/server/smartpower/AppAudioResource$AudioRecord$PlayerRecord;->mEvent:I

    const/4 v12, 0x5

    if-eq v11, v12, :cond_2

    iget v11, v9, Lcom/miui/server/smartpower/AppAudioResource$AudioRecord$PlayerRecord;->mEvent:I

    if-ne v11, v10, :cond_1

    goto :goto_1

    .line 389
    .end local v9    # "player":Lcom/miui/server/smartpower/AppAudioResource$AudioRecord$PlayerRecord;
    :cond_1
    goto :goto_0

    .line 385
    .restart local v9    # "player":Lcom/miui/server/smartpower/AppAudioResource$AudioRecord$PlayerRecord;
    :cond_2
    :goto_1
    const/4 v0, 0x1

    .line 386
    const/4 v2, 0x1

    .line 387
    nop

    .line 390
    .end local v9    # "player":Lcom/miui/server/smartpower/AppAudioResource$AudioRecord$PlayerRecord;
    :cond_3
    if-nez v2, :cond_4

    iget-boolean v8, p0, Lcom/miui/server/smartpower/AppAudioResource$AudioRecord;->mCurrentPlaying:Z

    if-eqz v8, :cond_4

    .line 391
    invoke-static {}, Landroid/os/SystemClock;->uptimeMillis()J

    move-result-wide v8

    iput-wide v8, p0, Lcom/miui/server/smartpower/AppAudioResource$AudioRecord;->mLastPlayingTimeStamp:J

    .line 393
    :cond_4
    iput-boolean v2, p0, Lcom/miui/server/smartpower/AppAudioResource$AudioRecord;->mCurrentPlaying:Z

    .line 394
    iget-wide v8, p0, Lcom/miui/server/smartpower/AppAudioResource$AudioRecord;->mLastPlayingTimeStamp:J

    sub-long v8, v4, v8

    const-wide/16 v11, 0x3e8

    cmp-long v8, v8, v11

    if-gez v8, :cond_5

    const/4 v8, 0x1

    goto :goto_2

    :cond_5
    const/4 v8, 0x0

    :goto_2
    move v6, v8

    .line 395
    monitor-exit v7
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_2

    .line 396
    iget v7, p0, Lcom/miui/server/smartpower/AppAudioResource$AudioRecord;->mPlaybackState:I

    if-ltz v7, :cond_8

    .line 397
    or-int/lit8 v1, v1, 0x4

    .line 398
    or-int/2addr v1, v10

    .line 399
    if-eq v7, v10, :cond_7

    if-nez v2, :cond_6

    if-eqz v6, :cond_7

    .line 400
    :cond_6
    const/4 v0, 0x1

    move v8, v3

    goto :goto_3

    .line 405
    :cond_7
    move v8, v3

    goto :goto_3

    .line 403
    :cond_8
    xor-int/lit8 v7, v0, 0x1

    move v3, v7

    move v8, v3

    .line 405
    .end local v3    # "inactiveDelay":Z
    .local v8, "inactiveDelay":Z
    :goto_3
    if-nez v0, :cond_b

    .line 406
    iget-object v3, p0, Lcom/miui/server/smartpower/AppAudioResource$AudioRecord;->mFocusedClientIds:Landroid/util/ArrayMap;

    monitor-enter v3

    .line 407
    :try_start_1
    iget-object v7, p0, Lcom/miui/server/smartpower/AppAudioResource$AudioRecord;->mFocusedClientIds:Landroid/util/ArrayMap;

    invoke-virtual {v7}, Landroid/util/ArrayMap;->size()I

    move-result v7

    if-lez v7, :cond_a

    .line 408
    iget-object v7, p0, Lcom/miui/server/smartpower/AppAudioResource$AudioRecord;->mFocusedClientIds:Landroid/util/ArrayMap;

    invoke-virtual {v7}, Landroid/util/ArrayMap;->values()Ljava/util/Collection;

    move-result-object v7

    invoke-interface {v7}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v7

    :goto_4
    invoke-interface {v7}, Ljava/util/Iterator;->hasNext()Z

    move-result v9

    if-eqz v9, :cond_a

    invoke-interface {v7}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v9

    check-cast v9, Ljava/lang/Integer;

    invoke-virtual {v9}, Ljava/lang/Integer;->intValue()I

    move-result v9

    .line 409
    .local v9, "focusLoss":I
    const/4 v11, -0x2

    if-ne v9, v11, :cond_9

    .line 410
    const/4 v0, 0x1

    .line 411
    goto :goto_5

    .line 413
    .end local v9    # "focusLoss":I
    :cond_9
    goto :goto_4

    .line 415
    :cond_a
    :goto_5
    monitor-exit v3

    goto :goto_6

    :catchall_0
    move-exception v7

    monitor-exit v3
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    throw v7

    .line 418
    :cond_b
    :goto_6
    iget-object v9, p0, Lcom/miui/server/smartpower/AppAudioResource$AudioRecord;->mRecorderRecords:Landroid/util/ArrayMap;

    monitor-enter v9

    .line 419
    :try_start_2
    iget-object v3, p0, Lcom/miui/server/smartpower/AppAudioResource$AudioRecord;->mRecorderRecords:Landroid/util/ArrayMap;

    invoke-virtual {v3}, Landroid/util/ArrayMap;->size()I

    move-result v3

    if-lez v3, :cond_c

    .line 420
    or-int/lit8 v1, v1, 0x8

    .line 422
    :cond_c
    iget-object v3, p0, Lcom/miui/server/smartpower/AppAudioResource$AudioRecord;->mRecorderRecords:Landroid/util/ArrayMap;

    invoke-virtual {v3}, Landroid/util/ArrayMap;->values()Ljava/util/Collection;

    move-result-object v3

    invoke-interface {v3}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :goto_7
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v7

    if-eqz v7, :cond_e

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v7

    check-cast v7, Lcom/miui/server/smartpower/AppAudioResource$AudioRecord$RecorderRecord;

    .line 423
    .local v7, "recorder":Lcom/miui/server/smartpower/AppAudioResource$AudioRecord$RecorderRecord;
    iget v11, v7, Lcom/miui/server/smartpower/AppAudioResource$AudioRecord$RecorderRecord;->mEvent:I

    if-nez v11, :cond_d

    .line 424
    const/4 v0, 0x1

    .line 426
    .end local v7    # "recorder":Lcom/miui/server/smartpower/AppAudioResource$AudioRecord$RecorderRecord;
    :cond_d
    goto :goto_7

    .line 427
    :cond_e
    monitor-exit v9
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 429
    and-int/lit8 v3, v1, 0x4

    if-eqz v3, :cond_10

    and-int/lit8 v3, v1, 0x8

    if-eqz v3, :cond_10

    .line 431
    iget-object v3, p0, Lcom/miui/server/smartpower/AppAudioResource$AudioRecord;->this$0:Lcom/miui/server/smartpower/AppAudioResource;

    invoke-static {v3}, Lcom/miui/server/smartpower/AppAudioResource;->-$$Nest$fgetmAudioManager(Lcom/miui/server/smartpower/AppAudioResource;)Landroid/media/AudioManager;

    move-result-object v3

    invoke-virtual {v3}, Landroid/media/AudioManager;->getMode()I

    move-result v3

    .line 432
    .local v3, "mode":I
    if-eq v3, v10, :cond_f

    const/4 v7, 0x3

    if-ne v3, v7, :cond_10

    .line 434
    :cond_f
    or-int/lit8 v1, v1, 0x10

    .line 438
    .end local v3    # "mode":I
    :cond_10
    iget-boolean v3, p0, Lcom/miui/server/smartpower/AppAudioResource$AudioRecord;->mCurrentActive:Z

    if-ne v0, v3, :cond_11

    iget v3, p0, Lcom/miui/server/smartpower/AppAudioResource$AudioRecord;->mCurrentBehavier:I

    if-eq v1, v3, :cond_14

    .line 439
    :cond_11
    iput-boolean v0, p0, Lcom/miui/server/smartpower/AppAudioResource$AudioRecord;->mCurrentActive:Z

    .line 440
    iput v1, p0, Lcom/miui/server/smartpower/AppAudioResource$AudioRecord;->mCurrentBehavier:I

    .line 441
    if-eqz v8, :cond_12

    .line 442
    iget-object v3, p0, Lcom/miui/server/smartpower/AppAudioResource$AudioRecord;->mInactiveDelayTask:Ljava/lang/Runnable;

    if-nez v3, :cond_14

    .line 443
    new-instance v3, Lcom/miui/server/smartpower/AppAudioResource$AudioRecord$2;

    invoke-direct {v3, p0}, Lcom/miui/server/smartpower/AppAudioResource$AudioRecord$2;-><init>(Lcom/miui/server/smartpower/AppAudioResource$AudioRecord;)V

    iput-object v3, p0, Lcom/miui/server/smartpower/AppAudioResource$AudioRecord;->mInactiveDelayTask:Ljava/lang/Runnable;

    .line 450
    iget-object v3, p0, Lcom/miui/server/smartpower/AppAudioResource$AudioRecord;->this$0:Lcom/miui/server/smartpower/AppAudioResource;

    invoke-static {v3}, Lcom/miui/server/smartpower/AppAudioResource;->-$$Nest$fgetmHandler(Lcom/miui/server/smartpower/AppAudioResource;)Landroid/os/Handler;

    move-result-object v3

    iget-object v7, p0, Lcom/miui/server/smartpower/AppAudioResource$AudioRecord;->mInactiveDelayTask:Ljava/lang/Runnable;

    const-wide/16 v9, 0x7d0

    invoke-virtual {v3, v7, v9, v10}, Landroid/os/Handler;->postDelayed(Ljava/lang/Runnable;J)Z

    goto :goto_8

    .line 453
    :cond_12
    iget-object v3, p0, Lcom/miui/server/smartpower/AppAudioResource$AudioRecord;->mInactiveDelayTask:Ljava/lang/Runnable;

    if-eqz v3, :cond_13

    .line 454
    iget-object v3, p0, Lcom/miui/server/smartpower/AppAudioResource$AudioRecord;->this$0:Lcom/miui/server/smartpower/AppAudioResource;

    invoke-static {v3}, Lcom/miui/server/smartpower/AppAudioResource;->-$$Nest$fgetmHandler(Lcom/miui/server/smartpower/AppAudioResource;)Landroid/os/Handler;

    move-result-object v3

    iget-object v7, p0, Lcom/miui/server/smartpower/AppAudioResource$AudioRecord;->mInactiveDelayTask:Ljava/lang/Runnable;

    invoke-virtual {v3, v7}, Landroid/os/Handler;->removeCallbacks(Ljava/lang/Runnable;)V

    .line 455
    const/4 v3, 0x0

    iput-object v3, p0, Lcom/miui/server/smartpower/AppAudioResource$AudioRecord;->mInactiveDelayTask:Ljava/lang/Runnable;

    .line 457
    :cond_13
    invoke-direct {p0}, Lcom/miui/server/smartpower/AppAudioResource$AudioRecord;->realUpdateAudioStatus()V

    .line 460
    :cond_14
    :goto_8
    return-void

    .line 427
    :catchall_1
    move-exception v3

    :try_start_3
    monitor-exit v9
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    throw v3

    .line 395
    .end local v8    # "inactiveDelay":Z
    .local v3, "inactiveDelay":Z
    :catchall_2
    move-exception v8

    :try_start_4
    monitor-exit v7
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_2

    throw v8
.end method


# virtual methods
.method containsClientId(Ljava/lang/String;)Z
    .locals 2
    .param p1, "clientId"    # Ljava/lang/String;

    .line 250
    iget-object v0, p0, Lcom/miui/server/smartpower/AppAudioResource$AudioRecord;->mFocusedClientIds:Landroid/util/ArrayMap;

    monitor-enter v0

    .line 251
    :try_start_0
    iget-object v1, p0, Lcom/miui/server/smartpower/AppAudioResource$AudioRecord;->mFocusedClientIds:Landroid/util/ArrayMap;

    invoke-virtual {v1, p1}, Landroid/util/ArrayMap;->containsKey(Ljava/lang/Object;)Z

    move-result v1

    monitor-exit v0

    return v1

    .line 252
    :catchall_0
    move-exception v1

    monitor-exit v0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v1
.end method

.method containsRecorder(I)Z
    .locals 4
    .param p1, "riid"    # I

    .line 239
    iget-object v0, p0, Lcom/miui/server/smartpower/AppAudioResource$AudioRecord;->mRecorderRecords:Landroid/util/ArrayMap;

    monitor-enter v0

    .line 240
    :try_start_0
    iget-object v1, p0, Lcom/miui/server/smartpower/AppAudioResource$AudioRecord;->mRecorderRecords:Landroid/util/ArrayMap;

    invoke-virtual {v1}, Landroid/util/ArrayMap;->values()Ljava/util/Collection;

    move-result-object v1

    invoke-interface {v1}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_1

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/miui/server/smartpower/AppAudioResource$AudioRecord$RecorderRecord;

    .line 241
    .local v2, "recorder":Lcom/miui/server/smartpower/AppAudioResource$AudioRecord$RecorderRecord;
    iget v3, v2, Lcom/miui/server/smartpower/AppAudioResource$AudioRecord$RecorderRecord;->mRiid:I

    if-ne v3, p1, :cond_0

    .line 242
    monitor-exit v0

    const/4 v0, 0x1

    return v0

    .line 244
    .end local v2    # "recorder":Lcom/miui/server/smartpower/AppAudioResource$AudioRecord$RecorderRecord;
    :cond_0
    goto :goto_0

    .line 245
    :cond_1
    monitor-exit v0

    .line 246
    const/4 v0, 0x0

    return v0

    .line 245
    :catchall_0
    move-exception v1

    monitor-exit v0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v1
.end method

.method onPlayerEvent(II)V
    .locals 1
    .param p1, "piid"    # I
    .param p2, "event"    # I

    .line 304
    invoke-direct {p0, p1}, Lcom/miui/server/smartpower/AppAudioResource$AudioRecord;->getPlayerRecord(I)Lcom/miui/server/smartpower/AppAudioResource$AudioRecord$PlayerRecord;

    move-result-object v0

    .line 305
    .local v0, "record":Lcom/miui/server/smartpower/AppAudioResource$AudioRecord$PlayerRecord;
    if-eqz v0, :cond_0

    .line 306
    iput p2, v0, Lcom/miui/server/smartpower/AppAudioResource$AudioRecord$PlayerRecord;->mEvent:I

    .line 307
    invoke-direct {p0}, Lcom/miui/server/smartpower/AppAudioResource$AudioRecord;->sendUpdateAudioStatusMsg()V

    .line 309
    :cond_0
    return-void
.end method

.method onPlayerRlease(I)V
    .locals 3
    .param p1, "piid"    # I

    .line 297
    iget-object v0, p0, Lcom/miui/server/smartpower/AppAudioResource$AudioRecord;->mPlayerRecords:Landroid/util/ArrayMap;

    monitor-enter v0

    .line 298
    :try_start_0
    iget-object v1, p0, Lcom/miui/server/smartpower/AppAudioResource$AudioRecord;->mPlayerRecords:Landroid/util/ArrayMap;

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v1, v2}, Landroid/util/ArrayMap;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    .line 299
    monitor-exit v0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 300
    invoke-direct {p0}, Lcom/miui/server/smartpower/AppAudioResource$AudioRecord;->sendUpdateAudioStatusMsg()V

    .line 301
    return-void

    .line 299
    :catchall_0
    move-exception v1

    :try_start_1
    monitor-exit v0
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    throw v1
.end method

.method onPlayerTrack(II)V
    .locals 4
    .param p1, "piid"    # I
    .param p2, "sessionId"    # I

    .line 287
    iget-object v0, p0, Lcom/miui/server/smartpower/AppAudioResource$AudioRecord;->mPlayerRecords:Landroid/util/ArrayMap;

    monitor-enter v0

    .line 288
    :try_start_0
    iget-object v1, p0, Lcom/miui/server/smartpower/AppAudioResource$AudioRecord;->mPlayerRecords:Landroid/util/ArrayMap;

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v1, v2}, Landroid/util/ArrayMap;->containsKey(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_0

    .line 289
    new-instance v1, Lcom/miui/server/smartpower/AppAudioResource$AudioRecord$PlayerRecord;

    invoke-direct {v1, p0, p1, p2}, Lcom/miui/server/smartpower/AppAudioResource$AudioRecord$PlayerRecord;-><init>(Lcom/miui/server/smartpower/AppAudioResource$AudioRecord;II)V

    .line 290
    .local v1, "record":Lcom/miui/server/smartpower/AppAudioResource$AudioRecord$PlayerRecord;
    iget-object v2, p0, Lcom/miui/server/smartpower/AppAudioResource$AudioRecord;->mPlayerRecords:Landroid/util/ArrayMap;

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    invoke-virtual {v2, v3, v1}, Landroid/util/ArrayMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 291
    invoke-direct {p0}, Lcom/miui/server/smartpower/AppAudioResource$AudioRecord;->sendUpdateAudioStatusMsg()V

    .line 293
    .end local v1    # "record":Lcom/miui/server/smartpower/AppAudioResource$AudioRecord$PlayerRecord;
    :cond_0
    monitor-exit v0

    .line 294
    return-void

    .line 293
    :catchall_0
    move-exception v1

    monitor-exit v0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v1
.end method

.method onRecorderEvent(II)V
    .locals 1
    .param p1, "riid"    # I
    .param p2, "event"    # I

    .line 340
    invoke-direct {p0, p1}, Lcom/miui/server/smartpower/AppAudioResource$AudioRecord;->getRecorderRecord(I)Lcom/miui/server/smartpower/AppAudioResource$AudioRecord$RecorderRecord;

    move-result-object v0

    .line 341
    .local v0, "record":Lcom/miui/server/smartpower/AppAudioResource$AudioRecord$RecorderRecord;
    if-eqz v0, :cond_0

    .line 342
    iput p2, v0, Lcom/miui/server/smartpower/AppAudioResource$AudioRecord$RecorderRecord;->mEvent:I

    .line 343
    invoke-direct {p0}, Lcom/miui/server/smartpower/AppAudioResource$AudioRecord;->sendUpdateAudioStatusMsg()V

    .line 345
    :cond_0
    return-void
.end method

.method onRecorderRlease(I)V
    .locals 3
    .param p1, "riid"    # I

    .line 334
    iget-object v0, p0, Lcom/miui/server/smartpower/AppAudioResource$AudioRecord;->mRecorderRecords:Landroid/util/ArrayMap;

    monitor-enter v0

    .line 335
    :try_start_0
    iget-object v1, p0, Lcom/miui/server/smartpower/AppAudioResource$AudioRecord;->mRecorderRecords:Landroid/util/ArrayMap;

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v1, v2}, Landroid/util/ArrayMap;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    .line 336
    monitor-exit v0

    .line 337
    return-void

    .line 336
    :catchall_0
    move-exception v1

    monitor-exit v0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v1
.end method

.method onRecorderTrack(I)V
    .locals 4
    .param p1, "riid"    # I

    .line 325
    iget-object v0, p0, Lcom/miui/server/smartpower/AppAudioResource$AudioRecord;->mRecorderRecords:Landroid/util/ArrayMap;

    monitor-enter v0

    .line 326
    :try_start_0
    iget-object v1, p0, Lcom/miui/server/smartpower/AppAudioResource$AudioRecord;->mRecorderRecords:Landroid/util/ArrayMap;

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v1, v2}, Landroid/util/ArrayMap;->containsKey(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_0

    .line 327
    new-instance v1, Lcom/miui/server/smartpower/AppAudioResource$AudioRecord$RecorderRecord;

    invoke-direct {v1, p0, p1}, Lcom/miui/server/smartpower/AppAudioResource$AudioRecord$RecorderRecord;-><init>(Lcom/miui/server/smartpower/AppAudioResource$AudioRecord;I)V

    .line 328
    .local v1, "record":Lcom/miui/server/smartpower/AppAudioResource$AudioRecord$RecorderRecord;
    iget-object v2, p0, Lcom/miui/server/smartpower/AppAudioResource$AudioRecord;->mRecorderRecords:Landroid/util/ArrayMap;

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    invoke-virtual {v2, v3, v1}, Landroid/util/ArrayMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 330
    .end local v1    # "record":Lcom/miui/server/smartpower/AppAudioResource$AudioRecord$RecorderRecord;
    :cond_0
    monitor-exit v0

    .line 331
    return-void

    .line 330
    :catchall_0
    move-exception v1

    monitor-exit v0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v1
.end method

.method playbackStateChanged(II)V
    .locals 3
    .param p1, "oldState"    # I
    .param p2, "newState"    # I

    .line 256
    iget v0, p0, Lcom/miui/server/smartpower/AppAudioResource$AudioRecord;->mPlaybackState:I

    if-eq v0, p2, :cond_0

    .line 257
    iput p2, p0, Lcom/miui/server/smartpower/AppAudioResource$AudioRecord;->mPlaybackState:I

    .line 258
    invoke-direct {p0}, Lcom/miui/server/smartpower/AppAudioResource$AudioRecord;->sendUpdateAudioStatusMsg()V

    .line 259
    const/4 v0, 0x2

    if-ne p2, v0, :cond_0

    .line 260
    iget-object v0, p0, Lcom/miui/server/smartpower/AppAudioResource$AudioRecord;->this$0:Lcom/miui/server/smartpower/AppAudioResource;

    iget v1, p0, Lcom/miui/server/smartpower/AppAudioResource$AudioRecord;->mOwnerPid:I

    int-to-long v1, v1

    invoke-static {v0, v1, v2}, Lcom/miui/server/smartpower/AppAudioResource;->-$$Nest$fputmLastMusicPlayPid(Lcom/miui/server/smartpower/AppAudioResource;J)V

    .line 261
    iget-object v0, p0, Lcom/miui/server/smartpower/AppAudioResource$AudioRecord;->this$0:Lcom/miui/server/smartpower/AppAudioResource;

    invoke-static {}, Landroid/os/SystemClock;->uptimeMillis()J

    move-result-wide v1

    invoke-static {v0, v1, v2}, Lcom/miui/server/smartpower/AppAudioResource;->-$$Nest$fputmLastMusicPlayTimeStamp(Lcom/miui/server/smartpower/AppAudioResource;J)V

    .line 264
    :cond_0
    return-void
.end method

.method recordAudioFocus(ZLjava/lang/String;)V
    .locals 3
    .param p1, "request"    # Z
    .param p2, "clientId"    # Ljava/lang/String;

    .line 267
    iget-object v0, p0, Lcom/miui/server/smartpower/AppAudioResource$AudioRecord;->mFocusedClientIds:Landroid/util/ArrayMap;

    monitor-enter v0

    .line 268
    if-eqz p1, :cond_0

    .line 269
    :try_start_0
    iget-object v1, p0, Lcom/miui/server/smartpower/AppAudioResource$AudioRecord;->mFocusedClientIds:Landroid/util/ArrayMap;

    const/4 v2, 0x0

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v1, p2, v2}, Landroid/util/ArrayMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    goto :goto_0

    .line 271
    :cond_0
    iget-object v1, p0, Lcom/miui/server/smartpower/AppAudioResource$AudioRecord;->mFocusedClientIds:Landroid/util/ArrayMap;

    invoke-virtual {v1, p2}, Landroid/util/ArrayMap;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    .line 273
    :goto_0
    iget-object v1, p0, Lcom/miui/server/smartpower/AppAudioResource$AudioRecord;->mFocusedClientIds:Landroid/util/ArrayMap;

    invoke-virtual {v1}, Landroid/util/ArrayMap;->size()I

    move-result v1

    if-lez v1, :cond_1

    .line 274
    monitor-exit v0

    return-void

    .line 276
    :cond_1
    monitor-exit v0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 277
    invoke-direct {p0}, Lcom/miui/server/smartpower/AppAudioResource$AudioRecord;->sendUpdateAudioStatusMsg()V

    .line 278
    return-void

    .line 276
    :catchall_0
    move-exception v1

    :try_start_1
    monitor-exit v0
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    throw v1
.end method

.method recordAudioFocusLoss(Ljava/lang/String;I)V
    .locals 3
    .param p1, "clientId"    # Ljava/lang/String;
    .param p2, "focusLoss"    # I

    .line 281
    iget-object v0, p0, Lcom/miui/server/smartpower/AppAudioResource$AudioRecord;->mFocusedClientIds:Landroid/util/ArrayMap;

    monitor-enter v0

    .line 282
    :try_start_0
    iget-object v1, p0, Lcom/miui/server/smartpower/AppAudioResource$AudioRecord;->mFocusedClientIds:Landroid/util/ArrayMap;

    invoke-static {p2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v1, p1, v2}, Landroid/util/ArrayMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 283
    monitor-exit v0

    .line 284
    return-void

    .line 283
    :catchall_0
    move-exception v1

    monitor-exit v0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v1
.end method

.method reportTrackStatus(IZ)V
    .locals 4
    .param p1, "sessionId"    # I
    .param p2, "isMuted"    # Z

    .line 312
    iget-object v0, p0, Lcom/miui/server/smartpower/AppAudioResource$AudioRecord;->mPlayerRecords:Landroid/util/ArrayMap;

    monitor-enter v0

    .line 313
    :try_start_0
    iget-object v1, p0, Lcom/miui/server/smartpower/AppAudioResource$AudioRecord;->mPlayerRecords:Landroid/util/ArrayMap;

    invoke-virtual {v1}, Landroid/util/ArrayMap;->values()Ljava/util/Collection;

    move-result-object v1

    invoke-interface {v1}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_2

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/miui/server/smartpower/AppAudioResource$AudioRecord$PlayerRecord;

    .line 314
    .local v2, "r":Lcom/miui/server/smartpower/AppAudioResource$AudioRecord$PlayerRecord;
    iget v3, v2, Lcom/miui/server/smartpower/AppAudioResource$AudioRecord$PlayerRecord;->mSessionId:I

    if-ne p1, v3, :cond_1

    .line 315
    if-eqz p2, :cond_0

    const/16 v1, 0x64

    goto :goto_1

    .line 316
    :cond_0
    const/4 v1, 0x5

    :goto_1
    iput v1, v2, Lcom/miui/server/smartpower/AppAudioResource$AudioRecord$PlayerRecord;->mEvent:I

    .line 317
    invoke-direct {p0}, Lcom/miui/server/smartpower/AppAudioResource$AudioRecord;->sendUpdateAudioStatusMsg()V

    .line 318
    monitor-exit v0

    return-void

    .line 320
    .end local v2    # "r":Lcom/miui/server/smartpower/AppAudioResource$AudioRecord$PlayerRecord;
    :cond_1
    goto :goto_0

    .line 321
    :cond_2
    monitor-exit v0

    .line 322
    return-void

    .line 321
    :catchall_0
    move-exception v1

    monitor-exit v0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v1
.end method

.method uidAudioStatusChanged(Z)V
    .locals 5
    .param p1, "active"    # Z

    .line 348
    if-nez p1, :cond_3

    .line 349
    iget-object v0, p0, Lcom/miui/server/smartpower/AppAudioResource$AudioRecord;->mPlayerRecords:Landroid/util/ArrayMap;

    monitor-enter v0

    .line 350
    :try_start_0
    iget-object v1, p0, Lcom/miui/server/smartpower/AppAudioResource$AudioRecord;->mPlayerRecords:Landroid/util/ArrayMap;

    invoke-virtual {v1}, Landroid/util/ArrayMap;->values()Ljava/util/Collection;

    move-result-object v1

    invoke-interface {v1}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_2

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/miui/server/smartpower/AppAudioResource$AudioRecord$PlayerRecord;

    .line 351
    .local v2, "player":Lcom/miui/server/smartpower/AppAudioResource$AudioRecord$PlayerRecord;
    iget v3, v2, Lcom/miui/server/smartpower/AppAudioResource$AudioRecord$PlayerRecord;->mEvent:I

    const/4 v4, 0x5

    if-eq v3, v4, :cond_0

    iget v3, v2, Lcom/miui/server/smartpower/AppAudioResource$AudioRecord$PlayerRecord;->mEvent:I

    const/4 v4, 0x2

    if-ne v3, v4, :cond_1

    .line 353
    :cond_0
    const/4 v3, 0x3

    iput v3, v2, Lcom/miui/server/smartpower/AppAudioResource$AudioRecord$PlayerRecord;->mEvent:I

    .line 355
    .end local v2    # "player":Lcom/miui/server/smartpower/AppAudioResource$AudioRecord$PlayerRecord;
    :cond_1
    goto :goto_0

    .line 356
    :cond_2
    monitor-exit v0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 357
    invoke-direct {p0}, Lcom/miui/server/smartpower/AppAudioResource$AudioRecord;->sendUpdateAudioStatusMsg()V

    goto :goto_1

    .line 356
    :catchall_0
    move-exception v1

    :try_start_1
    monitor-exit v0
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    throw v1

    .line 359
    :cond_3
    :goto_1
    return-void
.end method
