.class Lcom/miui/server/smartpower/AppNetworkResource;
.super Lcom/miui/server/smartpower/AppPowerResource;
.source "AppNetworkResource.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/miui/server/smartpower/AppNetworkResource$MyHandler;,
        Lcom/miui/server/smartpower/AppNetworkResource$NetworkMonitor;
    }
.end annotation


# static fields
.field private static final MSG_NETWORK_CHECK:I = 0x1

.field private static final NET_DOWNLOAD_SCENE_THRESHOLD:I = 0x4

.field private static final NET_KB:I = 0x400


# instance fields
.field private mHandler:Landroid/os/Handler;

.field private final mNetworkMonitorMap:Ljava/util/HashMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/HashMap<",
            "Ljava/lang/Integer;",
            "Lcom/miui/server/smartpower/AppNetworkResource$NetworkMonitor;",
            ">;"
        }
    .end annotation
.end field

.field private mNetworkStatsManager:Landroid/app/usage/NetworkStatsManager;


# direct methods
.method static bridge synthetic -$$Nest$fgetmHandler(Lcom/miui/server/smartpower/AppNetworkResource;)Landroid/os/Handler;
    .locals 0

    iget-object p0, p0, Lcom/miui/server/smartpower/AppNetworkResource;->mHandler:Landroid/os/Handler;

    return-object p0
.end method

.method static bridge synthetic -$$Nest$fgetmNetworkMonitorMap(Lcom/miui/server/smartpower/AppNetworkResource;)Ljava/util/HashMap;
    .locals 0

    iget-object p0, p0, Lcom/miui/server/smartpower/AppNetworkResource;->mNetworkMonitorMap:Ljava/util/HashMap;

    return-object p0
.end method

.method constructor <init>(Landroid/content/Context;Landroid/os/Looper;)V
    .locals 1
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "looper"    # Landroid/os/Looper;

    .line 32
    invoke-direct {p0}, Lcom/miui/server/smartpower/AppPowerResource;-><init>()V

    .line 30
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    iput-object v0, p0, Lcom/miui/server/smartpower/AppNetworkResource;->mNetworkMonitorMap:Ljava/util/HashMap;

    .line 33
    new-instance v0, Lcom/miui/server/smartpower/AppNetworkResource$MyHandler;

    invoke-direct {v0, p0, p2}, Lcom/miui/server/smartpower/AppNetworkResource$MyHandler;-><init>(Lcom/miui/server/smartpower/AppNetworkResource;Landroid/os/Looper;)V

    iput-object v0, p0, Lcom/miui/server/smartpower/AppNetworkResource;->mHandler:Landroid/os/Handler;

    .line 34
    const/4 v0, 0x2

    iput v0, p0, Lcom/miui/server/smartpower/AppNetworkResource;->mType:I

    .line 35
    return-void
.end method

.method private updateNetworkRule(IZ)V
    .locals 4
    .param p1, "uid"    # I
    .param p2, "allow"    # Z

    .line 71
    const-string v0, "SmartPower.AppResource"

    :try_start_0
    invoke-static {}, Lcom/android/server/net/NetworkManagementServiceStub;->getInstance()Lcom/android/server/net/NetworkManagementServiceStub;

    move-result-object v1

    invoke-interface {v1, p1, p2}, Lcom/android/server/net/NetworkManagementServiceStub;->updateAurogonUidRule(IZ)V

    .line 72
    sget-boolean v1, Lcom/miui/server/smartpower/AppNetworkResource;->DEBUG:Z

    if-eqz v1, :cond_0

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v2, "setFirewall received: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Slog;->d(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 75
    :cond_0
    goto :goto_0

    .line 73
    :catch_0
    move-exception v1

    .line 74
    .local v1, "e":Ljava/lang/Exception;
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v3, "setFirewall failed "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v0, v2}, Landroid/util/Slog;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 76
    .end local v1    # "e":Ljava/lang/Exception;
    :goto_0
    return-void
.end method


# virtual methods
.method public getActiveUids()Ljava/util/ArrayList;
    .locals 1

    .line 39
    const/4 v0, 0x0

    return-object v0
.end method

.method public isAppResourceActive(I)Z
    .locals 1
    .param p1, "uid"    # I

    .line 44
    const/4 v0, 0x0

    return v0
.end method

.method public isAppResourceActive(II)Z
    .locals 1
    .param p1, "uid"    # I
    .param p2, "pid"    # I

    .line 49
    const/4 v0, 0x0

    return v0
.end method

.method public registerCallback(Lcom/miui/server/smartpower/AppPowerResource$IAppPowerResourceCallback;I)V
    .locals 4
    .param p1, "callback"    # Lcom/miui/server/smartpower/AppPowerResource$IAppPowerResourceCallback;
    .param p2, "uid"    # I

    .line 80
    invoke-super {p0, p1, p2}, Lcom/miui/server/smartpower/AppPowerResource;->registerCallback(Lcom/miui/server/smartpower/AppPowerResource$IAppPowerResourceCallback;I)V

    .line 81
    iget-object v0, p0, Lcom/miui/server/smartpower/AppNetworkResource;->mNetworkMonitorMap:Ljava/util/HashMap;

    monitor-enter v0

    .line 82
    :try_start_0
    iget-object v1, p0, Lcom/miui/server/smartpower/AppNetworkResource;->mNetworkMonitorMap:Ljava/util/HashMap;

    invoke-static {p2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    if-nez v1, :cond_0

    .line 83
    iget-object v1, p0, Lcom/miui/server/smartpower/AppNetworkResource;->mNetworkMonitorMap:Ljava/util/HashMap;

    invoke-static {p2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    new-instance v3, Lcom/miui/server/smartpower/AppNetworkResource$NetworkMonitor;

    invoke-direct {v3, p0, p2}, Lcom/miui/server/smartpower/AppNetworkResource$NetworkMonitor;-><init>(Lcom/miui/server/smartpower/AppNetworkResource;I)V

    invoke-virtual {v1, v2, v3}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 85
    :cond_0
    monitor-exit v0

    .line 86
    return-void

    .line 85
    :catchall_0
    move-exception v1

    monitor-exit v0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v1
.end method

.method public releaseAppPowerResource(I)V
    .locals 4
    .param p1, "uid"    # I

    .line 54
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "network u:"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, " s:release"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    .line 55
    .local v0, "event":Ljava/lang/String;
    const-wide/32 v1, 0x20000

    invoke-static {v1, v2, v0}, Landroid/os/Trace;->traceBegin(JLjava/lang/String;)V

    .line 56
    const/4 v3, 0x1

    invoke-direct {p0, p1, v3}, Lcom/miui/server/smartpower/AppNetworkResource;->updateNetworkRule(IZ)V

    .line 57
    const v3, 0x15ff2

    invoke-static {v3, v0}, Landroid/util/EventLog;->writeEvent(ILjava/lang/String;)I

    .line 58
    invoke-static {v1, v2}, Landroid/os/Trace;->traceEnd(J)V

    .line 59
    return-void
.end method

.method public resumeAppPowerResource(I)V
    .locals 3
    .param p1, "uid"    # I

    .line 63
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "network u:"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, " s:resume"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    .line 64
    .local v0, "event":Ljava/lang/String;
    const/4 v1, 0x0

    invoke-direct {p0, p1, v1}, Lcom/miui/server/smartpower/AppNetworkResource;->updateNetworkRule(IZ)V

    .line 65
    const v1, 0x15ff2

    invoke-static {v1, v0}, Landroid/util/EventLog;->writeEvent(ILjava/lang/String;)I

    .line 66
    const-wide/32 v1, 0x20000

    invoke-static {v1, v2}, Landroid/os/Trace;->traceEnd(J)V

    .line 67
    return-void
.end method

.method public unRegisterCallback(Lcom/miui/server/smartpower/AppPowerResource$IAppPowerResourceCallback;I)V
    .locals 3
    .param p1, "callback"    # Lcom/miui/server/smartpower/AppPowerResource$IAppPowerResourceCallback;
    .param p2, "uid"    # I

    .line 90
    invoke-super {p0, p1, p2}, Lcom/miui/server/smartpower/AppPowerResource;->unRegisterCallback(Lcom/miui/server/smartpower/AppPowerResource$IAppPowerResourceCallback;I)V

    .line 91
    iget-object v0, p0, Lcom/miui/server/smartpower/AppNetworkResource;->mNetworkMonitorMap:Ljava/util/HashMap;

    monitor-enter v0

    .line 92
    :try_start_0
    iget-object v1, p0, Lcom/miui/server/smartpower/AppNetworkResource;->mResourceCallbacksByUid:Landroid/util/SparseArray;

    invoke-virtual {v1, p2}, Landroid/util/SparseArray;->get(I)Ljava/lang/Object;

    move-result-object v1

    if-nez v1, :cond_0

    .line 93
    iget-object v1, p0, Lcom/miui/server/smartpower/AppNetworkResource;->mNetworkMonitorMap:Ljava/util/HashMap;

    invoke-static {p2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/util/HashMap;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    .line 95
    :cond_0
    monitor-exit v0

    .line 96
    return-void

    .line 95
    :catchall_0
    move-exception v1

    monitor-exit v0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v1
.end method
