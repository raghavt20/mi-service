public class com.miui.server.smartpower.SmartScenarioManager$ScenarioIdInfo {
	 /* .source "SmartScenarioManager.java" */
	 /* # annotations */
	 /* .annotation system Ldalvik/annotation/EnclosingClass; */
	 /* value = Lcom/miui/server/smartpower/SmartScenarioManager; */
} // .end annotation
/* .annotation system Ldalvik/annotation/InnerClass; */
/* accessFlags = 0x1 */
/* name = "ScenarioIdInfo" */
} // .end annotation
/* # instance fields */
private Integer mAction;
private Long mId;
private java.lang.String mPackageName;
final com.miui.server.smartpower.SmartScenarioManager this$0; //synthetic
/* # direct methods */
static Integer -$$Nest$fgetmAction ( com.miui.server.smartpower.SmartScenarioManager$ScenarioIdInfo p0 ) { //bridge//synthethic
/* .locals 0 */
/* iget p0, p0, Lcom/miui/server/smartpower/SmartScenarioManager$ScenarioIdInfo;->mAction:I */
} // .end method
static Long -$$Nest$fgetmId ( com.miui.server.smartpower.SmartScenarioManager$ScenarioIdInfo p0 ) { //bridge//synthethic
/* .locals 2 */
/* iget-wide v0, p0, Lcom/miui/server/smartpower/SmartScenarioManager$ScenarioIdInfo;->mId:J */
/* return-wide v0 */
} // .end method
static java.lang.String -$$Nest$fgetmPackageName ( com.miui.server.smartpower.SmartScenarioManager$ScenarioIdInfo p0 ) { //bridge//synthethic
/* .locals 0 */
p0 = this.mPackageName;
} // .end method
public com.miui.server.smartpower.SmartScenarioManager$ScenarioIdInfo ( ) {
/* .locals 0 */
/* .param p1, "this$0" # Lcom/miui/server/smartpower/SmartScenarioManager; */
/* .param p2, "packageName" # Ljava/lang/String; */
/* .param p3, "action" # I */
/* .param p4, "id" # J */
/* .line 463 */
this.this$0 = p1;
/* invoke-direct {p0}, Ljava/lang/Object;-><init>()V */
/* .line 464 */
this.mPackageName = p2;
/* .line 465 */
/* iput p3, p0, Lcom/miui/server/smartpower/SmartScenarioManager$ScenarioIdInfo;->mAction:I */
/* .line 466 */
/* iput-wide p4, p0, Lcom/miui/server/smartpower/SmartScenarioManager$ScenarioIdInfo;->mId:J */
/* .line 467 */
return;
} // .end method
/* # virtual methods */
public java.lang.String toString ( ) {
/* .locals 2 */
/* .line 471 */
/* new-instance v0, Ljava/lang/StringBuilder; */
/* invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V */
/* iget v1, p0, Lcom/miui/server/smartpower/SmartScenarioManager$ScenarioIdInfo;->mAction:I */
com.miui.server.smartpower.SmartScenarioManager .actionTypeToString ( v1 );
(( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
final String v1 = " "; // const-string v1, " "
(( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
v1 = this.mPackageName;
(( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v0 ).toString ( ); // invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
} // .end method
