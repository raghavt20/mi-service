class com.miui.server.smartpower.AppBluetoothResource$BleRecord {
	 /* .source "AppBluetoothResource.java" */
	 /* # annotations */
	 /* .annotation system Ldalvik/annotation/EnclosingClass; */
	 /* value = Lcom/miui/server/smartpower/AppBluetoothResource; */
} // .end annotation
/* .annotation system Ldalvik/annotation/InnerClass; */
/* accessFlags = 0x2 */
/* name = "BleRecord" */
} // .end annotation
/* .annotation system Ldalvik/annotation/MemberClasses; */
/* value = { */
/* Lcom/miui/server/smartpower/AppBluetoothResource$BleRecord$ProfileRecord; */
/* } */
} // .end annotation
/* # instance fields */
Integer mOwnerPid;
Integer mOwnerUid;
private final android.util.SparseArray mProfileRecords;
/* .annotation system Ldalvik/annotation/Signature; */
/* value = { */
/* "Landroid/util/SparseArray<", */
/* "Lcom/miui/server/smartpower/AppBluetoothResource$BleRecord$ProfileRecord;", */
/* ">;" */
/* } */
} // .end annotation
} // .end field
final com.miui.server.smartpower.AppBluetoothResource this$0; //synthetic
/* # direct methods */
 com.miui.server.smartpower.AppBluetoothResource$BleRecord ( ) {
/* .locals 0 */
/* .param p2, "uid" # I */
/* .param p3, "pid" # I */
/* .line 139 */
this.this$0 = p1;
/* invoke-direct {p0}, Ljava/lang/Object;-><init>()V */
/* .line 137 */
/* new-instance p1, Landroid/util/SparseArray; */
/* invoke-direct {p1}, Landroid/util/SparseArray;-><init>()V */
this.mProfileRecords = p1;
/* .line 140 */
/* iput p2, p0, Lcom/miui/server/smartpower/AppBluetoothResource$BleRecord;->mOwnerUid:I */
/* .line 141 */
/* iput p3, p0, Lcom/miui/server/smartpower/AppBluetoothResource$BleRecord;->mOwnerPid:I */
/* .line 142 */
return;
} // .end method
/* # virtual methods */
Boolean isActive ( ) {
/* .locals 1 */
/* .line 175 */
v0 = this.mProfileRecords;
v0 = (( android.util.SparseArray ) v0 ).size ( ); // invoke-virtual {v0}, Landroid/util/SparseArray;->size()I
/* if-lez v0, :cond_0 */
int v0 = 1; // const/4 v0, 0x1
} // :cond_0
int v0 = 0; // const/4 v0, 0x0
} // :goto_0
} // .end method
void onBluetoothConnect ( Integer p0, Integer p1 ) {
/* .locals 7 */
/* .param p1, "bleType" # I */
/* .param p2, "flag" # I */
/* .line 146 */
v0 = this.mProfileRecords;
/* monitor-enter v0 */
/* .line 147 */
try { // :try_start_0
v1 = this.mProfileRecords;
(( android.util.SparseArray ) v1 ).get ( p1 ); // invoke-virtual {v1, p1}, Landroid/util/SparseArray;->get(I)Ljava/lang/Object;
/* check-cast v1, Lcom/miui/server/smartpower/AppBluetoothResource$BleRecord$ProfileRecord; */
/* .line 148 */
/* .local v1, "profileRecord":Lcom/miui/server/smartpower/AppBluetoothResource$BleRecord$ProfileRecord; */
/* if-nez v1, :cond_0 */
/* .line 149 */
/* new-instance v2, Lcom/miui/server/smartpower/AppBluetoothResource$BleRecord$ProfileRecord; */
/* invoke-direct {v2, p0, p1}, Lcom/miui/server/smartpower/AppBluetoothResource$BleRecord$ProfileRecord;-><init>(Lcom/miui/server/smartpower/AppBluetoothResource$BleRecord;I)V */
/* move-object v1, v2 */
/* .line 150 */
v2 = this.mProfileRecords;
(( android.util.SparseArray ) v2 ).put ( p1, v1 ); // invoke-virtual {v2, p1, v1}, Landroid/util/SparseArray;->put(ILjava/lang/Object;)V
/* .line 152 */
} // :cond_0
/* new-instance v2, Ljava/lang/StringBuilder; */
/* invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V */
final String v3 = "bluetooth u:"; // const-string v3, "bluetooth u:"
(( java.lang.StringBuilder ) v2 ).append ( v3 ); // invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
/* iget v3, p0, Lcom/miui/server/smartpower/AppBluetoothResource$BleRecord;->mOwnerUid:I */
(( java.lang.StringBuilder ) v2 ).append ( v3 ); // invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;
final String v3 = " p:"; // const-string v3, " p:"
(( java.lang.StringBuilder ) v2 ).append ( v3 ); // invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
/* iget v3, p0, Lcom/miui/server/smartpower/AppBluetoothResource$BleRecord;->mOwnerPid:I */
(( java.lang.StringBuilder ) v2 ).append ( v3 ); // invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;
final String v3 = " s:true"; // const-string v3, " s:true"
(( java.lang.StringBuilder ) v2 ).append ( v3 ); // invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v2 ).toString ( ); // invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
/* const v3, 0x15ff2 */
android.util.EventLog .writeEvent ( v3,v2 );
/* .line 154 */
v2 = this.this$0;
/* iget v3, p0, Lcom/miui/server/smartpower/AppBluetoothResource$BleRecord;->mOwnerUid:I */
/* iget v4, p0, Lcom/miui/server/smartpower/AppBluetoothResource$BleRecord;->mOwnerPid:I */
int v5 = 1; // const/4 v5, 0x1
int v6 = 0; // const/4 v6, 0x0
(( com.miui.server.smartpower.AppBluetoothResource ) v2 ).reportResourceStatus ( v3, v4, v5, v6 ); // invoke-virtual {v2, v3, v4, v5, v6}, Lcom/miui/server/smartpower/AppBluetoothResource;->reportResourceStatus(IIZI)V
/* .line 155 */
/* monitor-exit v0 */
/* :try_end_0 */
/* .catchall {:try_start_0 ..:try_end_0} :catchall_0 */
/* .line 156 */
(( com.miui.server.smartpower.AppBluetoothResource$BleRecord$ProfileRecord ) v1 ).bluetoothConnect ( p2 ); // invoke-virtual {v1, p2}, Lcom/miui/server/smartpower/AppBluetoothResource$BleRecord$ProfileRecord;->bluetoothConnect(I)V
/* .line 157 */
return;
/* .line 155 */
} // .end local v1 # "profileRecord":Lcom/miui/server/smartpower/AppBluetoothResource$BleRecord$ProfileRecord;
/* :catchall_0 */
/* move-exception v1 */
try { // :try_start_1
/* monitor-exit v0 */
/* :try_end_1 */
/* .catchall {:try_start_1 ..:try_end_1} :catchall_0 */
/* throw v1 */
} // .end method
void onBluetoothDisconnect ( Integer p0, Integer p1 ) {
/* .locals 6 */
/* .param p1, "bleType" # I */
/* .param p2, "flag" # I */
/* .line 160 */
v0 = this.mProfileRecords;
/* monitor-enter v0 */
/* .line 161 */
try { // :try_start_0
v1 = this.mProfileRecords;
(( android.util.SparseArray ) v1 ).get ( p1 ); // invoke-virtual {v1, p1}, Landroid/util/SparseArray;->get(I)Ljava/lang/Object;
/* check-cast v1, Lcom/miui/server/smartpower/AppBluetoothResource$BleRecord$ProfileRecord; */
/* .line 162 */
/* .local v1, "profileRecord":Lcom/miui/server/smartpower/AppBluetoothResource$BleRecord$ProfileRecord; */
if ( v1 != null) { // if-eqz v1, :cond_0
/* .line 163 */
(( com.miui.server.smartpower.AppBluetoothResource$BleRecord$ProfileRecord ) v1 ).bluetoothDisconnect ( p2 ); // invoke-virtual {v1, p2}, Lcom/miui/server/smartpower/AppBluetoothResource$BleRecord$ProfileRecord;->bluetoothDisconnect(I)V
/* .line 164 */
v2 = (( com.miui.server.smartpower.AppBluetoothResource$BleRecord$ProfileRecord ) v1 ).isActive ( ); // invoke-virtual {v1}, Lcom/miui/server/smartpower/AppBluetoothResource$BleRecord$ProfileRecord;->isActive()Z
/* if-nez v2, :cond_0 */
/* .line 165 */
v2 = this.mProfileRecords;
(( android.util.SparseArray ) v2 ).remove ( p1 ); // invoke-virtual {v2, p1}, Landroid/util/SparseArray;->remove(I)V
/* .line 166 */
/* new-instance v2, Ljava/lang/StringBuilder; */
/* invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V */
final String v3 = "bluetooth u:"; // const-string v3, "bluetooth u:"
(( java.lang.StringBuilder ) v2 ).append ( v3 ); // invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
/* iget v3, p0, Lcom/miui/server/smartpower/AppBluetoothResource$BleRecord;->mOwnerUid:I */
(( java.lang.StringBuilder ) v2 ).append ( v3 ); // invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;
final String v3 = " p:"; // const-string v3, " p:"
(( java.lang.StringBuilder ) v2 ).append ( v3 ); // invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
/* iget v3, p0, Lcom/miui/server/smartpower/AppBluetoothResource$BleRecord;->mOwnerPid:I */
(( java.lang.StringBuilder ) v2 ).append ( v3 ); // invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;
final String v3 = " s:false"; // const-string v3, " s:false"
(( java.lang.StringBuilder ) v2 ).append ( v3 ); // invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v2 ).toString ( ); // invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
/* const v3, 0x15ff2 */
android.util.EventLog .writeEvent ( v3,v2 );
/* .line 168 */
v2 = this.this$0;
/* iget v3, p0, Lcom/miui/server/smartpower/AppBluetoothResource$BleRecord;->mOwnerUid:I */
/* iget v4, p0, Lcom/miui/server/smartpower/AppBluetoothResource$BleRecord;->mOwnerPid:I */
int v5 = 0; // const/4 v5, 0x0
(( com.miui.server.smartpower.AppBluetoothResource ) v2 ).reportResourceStatus ( v3, v4, v5, v5 ); // invoke-virtual {v2, v3, v4, v5, v5}, Lcom/miui/server/smartpower/AppBluetoothResource;->reportResourceStatus(IIZI)V
/* .line 171 */
} // .end local v1 # "profileRecord":Lcom/miui/server/smartpower/AppBluetoothResource$BleRecord$ProfileRecord;
} // :cond_0
/* monitor-exit v0 */
/* .line 172 */
return;
/* .line 171 */
/* :catchall_0 */
/* move-exception v1 */
/* monitor-exit v0 */
/* :try_end_0 */
/* .catchall {:try_start_0 ..:try_end_0} :catchall_0 */
/* throw v1 */
} // .end method
