class com.miui.server.smartpower.AppWakelockResource extends com.miui.server.smartpower.AppPowerResource {
	 /* .source "AppWakelockResource.java" */
	 /* # annotations */
	 /* .annotation system Ldalvik/annotation/MemberClasses; */
	 /* value = { */
	 /* Lcom/miui/server/smartpower/AppWakelockResource$MyHandler;, */
	 /* Lcom/miui/server/smartpower/AppWakelockResource$WakeLock; */
	 /* } */
} // .end annotation
/* # static fields */
private static final Integer BLOOEAN_DISABLE;
private static final Integer BLOOEAN_ENABLE;
private static final Integer MSG_SET_WAKELOCK_STATE;
/* # instance fields */
private android.os.Handler mHandler;
private final java.lang.Object mLock;
private android.content.pm.PackageManagerInternal mPackageManagerInt;
private miui.security.SecurityManagerInternal mSecurityManagerInt;
private final java.util.ArrayList mWakeLocks;
/* .annotation system Ldalvik/annotation/Signature; */
/* value = { */
/* "Ljava/util/ArrayList<", */
/* "Lcom/miui/server/smartpower/AppWakelockResource$WakeLock;", */
/* ">;" */
/* } */
} // .end annotation
} // .end field
/* # direct methods */
public static void $r8$lambda$475vpAqpCkbCJYlMumLxJqPqZLo ( com.miui.server.smartpower.AppWakelockResource p0, android.os.IBinder p1, Integer p2 ) { //synthethic
/* .locals 0 */
/* invoke-direct {p0, p1, p2}, Lcom/miui/server/smartpower/AppWakelockResource;->lambda$acquireWakelock$0(Landroid/os/IBinder;I)V */
return;
} // .end method
public static void $r8$lambda$QFyfXSZD-mFgmShxIG9ApR412g8 ( com.miui.server.smartpower.AppWakelockResource p0, com.miui.server.smartpower.AppWakelockResource$WakeLock p1 ) { //synthethic
/* .locals 0 */
/* invoke-direct {p0, p1}, Lcom/miui/server/smartpower/AppWakelockResource;->lambda$removeWakeLockLocked$1(Lcom/miui/server/smartpower/AppWakelockResource$WakeLock;)V */
return;
} // .end method
static void -$$Nest$mhandleWakeLockDeath ( com.miui.server.smartpower.AppWakelockResource p0, com.miui.server.smartpower.AppWakelockResource$WakeLock p1 ) { //bridge//synthethic
/* .locals 0 */
/* invoke-direct {p0, p1}, Lcom/miui/server/smartpower/AppWakelockResource;->handleWakeLockDeath(Lcom/miui/server/smartpower/AppWakelockResource$WakeLock;)V */
return;
} // .end method
static void -$$Nest$msetWakeLockState ( com.miui.server.smartpower.AppWakelockResource p0, Integer p1, Boolean p2 ) { //bridge//synthethic
/* .locals 0 */
/* invoke-direct {p0, p1, p2}, Lcom/miui/server/smartpower/AppWakelockResource;->setWakeLockState(IZ)V */
return;
} // .end method
public com.miui.server.smartpower.AppWakelockResource ( ) {
/* .locals 1 */
/* .param p1, "context" # Landroid/content/Context; */
/* .param p2, "looper" # Landroid/os/Looper; */
/* .line 41 */
/* invoke-direct {p0}, Lcom/miui/server/smartpower/AppPowerResource;-><init>()V */
/* .line 33 */
/* new-instance v0, Ljava/lang/Object; */
/* invoke-direct {v0}, Ljava/lang/Object;-><init>()V */
this.mLock = v0;
/* .line 36 */
/* new-instance v0, Ljava/util/ArrayList; */
/* invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V */
this.mWakeLocks = v0;
/* .line 42 */
/* new-instance v0, Lcom/miui/server/smartpower/AppWakelockResource$MyHandler; */
/* invoke-direct {v0, p0, p2}, Lcom/miui/server/smartpower/AppWakelockResource$MyHandler;-><init>(Lcom/miui/server/smartpower/AppWakelockResource;Landroid/os/Looper;)V */
this.mHandler = v0;
/* .line 43 */
int v0 = 4; // const/4 v0, 0x4
/* iput v0, p0, Lcom/miui/server/smartpower/AppWakelockResource;->mType:I */
/* .line 44 */
/* const-class v0, Lmiui/security/SecurityManagerInternal; */
com.android.server.LocalServices .getService ( v0 );
/* check-cast v0, Lmiui/security/SecurityManagerInternal; */
this.mSecurityManagerInt = v0;
/* .line 45 */
/* const-class v0, Landroid/content/pm/PackageManagerInternal; */
com.android.server.LocalServices .getService ( v0 );
/* check-cast v0, Landroid/content/pm/PackageManagerInternal; */
this.mPackageManagerInt = v0;
/* .line 46 */
return;
} // .end method
private Integer findWakeLockIndexLocked ( android.os.IBinder p0 ) {
/* .locals 3 */
/* .param p1, "lock" # Landroid/os/IBinder; */
/* .line 170 */
v0 = this.mWakeLocks;
v0 = (( java.util.ArrayList ) v0 ).size ( ); // invoke-virtual {v0}, Ljava/util/ArrayList;->size()I
/* .line 171 */
/* .local v0, "count":I */
int v1 = 0; // const/4 v1, 0x0
/* .local v1, "i":I */
} // :goto_0
/* if-ge v1, v0, :cond_1 */
/* .line 172 */
v2 = this.mWakeLocks;
(( java.util.ArrayList ) v2 ).get ( v1 ); // invoke-virtual {v2, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;
/* check-cast v2, Lcom/miui/server/smartpower/AppWakelockResource$WakeLock; */
v2 = this.mLock;
/* if-ne v2, p1, :cond_0 */
/* .line 173 */
/* .line 171 */
} // :cond_0
/* add-int/lit8 v1, v1, 0x1 */
/* .line 176 */
} // .end local v1 # "i":I
} // :cond_1
int v1 = -1; // const/4 v1, -0x1
} // .end method
private void handleWakeLockDeath ( com.miui.server.smartpower.AppWakelockResource$WakeLock p0 ) {
/* .locals 2 */
/* .param p1, "wakeLock" # Lcom/miui/server/smartpower/AppWakelockResource$WakeLock; */
/* .line 180 */
v0 = this.mLock;
/* monitor-enter v0 */
/* .line 181 */
try { // :try_start_0
v1 = this.mWakeLocks;
v1 = (( java.util.ArrayList ) v1 ).indexOf ( p1 ); // invoke-virtual {v1, p1}, Ljava/util/ArrayList;->indexOf(Ljava/lang/Object;)I
/* .line 182 */
/* .local v1, "index":I */
/* if-gez v1, :cond_0 */
/* .line 183 */
/* monitor-exit v0 */
return;
/* .line 185 */
} // :cond_0
/* invoke-direct {p0, p1, v1}, Lcom/miui/server/smartpower/AppWakelockResource;->removeWakeLockLocked(Lcom/miui/server/smartpower/AppWakelockResource$WakeLock;I)V */
/* .line 186 */
} // .end local v1 # "index":I
/* monitor-exit v0 */
/* .line 187 */
return;
/* .line 186 */
/* :catchall_0 */
/* move-exception v1 */
/* monitor-exit v0 */
/* :try_end_0 */
/* .catchall {:try_start_0 ..:try_end_0} :catchall_0 */
/* throw v1 */
} // .end method
private void lambda$acquireWakelock$0 ( android.os.IBinder p0, Integer p1 ) { //synthethic
/* .locals 1 */
/* .param p1, "lock" # Landroid/os/IBinder; */
/* .param p2, "ownerUid" # I */
/* .line 145 */
int v0 = 1; // const/4 v0, 0x1
(( com.miui.server.smartpower.AppWakelockResource ) p0 ).onAppBehaviorEvent ( p1, p2, v0 ); // invoke-virtual {p0, p1, p2, v0}, Lcom/miui/server/smartpower/AppWakelockResource;->onAppBehaviorEvent(Landroid/os/IBinder;IZ)V
return;
} // .end method
private void lambda$removeWakeLockLocked$1 ( com.miui.server.smartpower.AppWakelockResource$WakeLock p0 ) { //synthethic
/* .locals 3 */
/* .param p1, "wakeLock" # Lcom/miui/server/smartpower/AppWakelockResource$WakeLock; */
/* .line 191 */
v0 = this.mLock;
/* iget v1, p1, Lcom/miui/server/smartpower/AppWakelockResource$WakeLock;->mOwnerUid:I */
int v2 = 0; // const/4 v2, 0x0
(( com.miui.server.smartpower.AppWakelockResource ) p0 ).onAppBehaviorEvent ( v0, v1, v2 ); // invoke-virtual {p0, v0, v1, v2}, Lcom/miui/server/smartpower/AppWakelockResource;->onAppBehaviorEvent(Landroid/os/IBinder;IZ)V
return;
} // .end method
private void removeWakeLockLocked ( com.miui.server.smartpower.AppWakelockResource$WakeLock p0, Integer p1 ) {
/* .locals 2 */
/* .param p1, "wakeLock" # Lcom/miui/server/smartpower/AppWakelockResource$WakeLock; */
/* .param p2, "index" # I */
/* .line 190 */
v0 = this.mWakeLocks;
(( java.util.ArrayList ) v0 ).remove ( p2 ); // invoke-virtual {v0, p2}, Ljava/util/ArrayList;->remove(I)Ljava/lang/Object;
/* .line 191 */
v0 = this.mHandler;
/* new-instance v1, Lcom/miui/server/smartpower/AppWakelockResource$$ExternalSyntheticLambda1; */
/* invoke-direct {v1, p0, p1}, Lcom/miui/server/smartpower/AppWakelockResource$$ExternalSyntheticLambda1;-><init>(Lcom/miui/server/smartpower/AppWakelockResource;Lcom/miui/server/smartpower/AppWakelockResource$WakeLock;)V */
(( android.os.Handler ) v0 ).post ( v1 ); // invoke-virtual {v0, v1}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z
/* .line 192 */
return;
} // .end method
private void setWakeLockState ( Integer p0, Boolean p1 ) {
/* .locals 5 */
/* .param p1, "uid" # I */
/* .param p2, "enable" # Z */
/* .line 105 */
try { // :try_start_0
/* new-instance v0, Landroid/util/ArraySet; */
/* invoke-direct {v0}, Landroid/util/ArraySet;-><init>()V */
/* .line 106 */
/* .local v0, "targetTags":Landroid/util/ArraySet;, "Landroid/util/ArraySet<Ljava/lang/String;>;" */
v1 = this.mLock;
/* monitor-enter v1 */
/* :try_end_0 */
/* .catch Ljava/lang/Exception; {:try_start_0 ..:try_end_0} :catch_0 */
/* .line 107 */
try { // :try_start_1
v2 = this.mWakeLocks;
(( java.util.ArrayList ) v2 ).iterator ( ); // invoke-virtual {v2}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;
v3 = } // :goto_0
if ( v3 != null) { // if-eqz v3, :cond_1
/* check-cast v3, Lcom/miui/server/smartpower/AppWakelockResource$WakeLock; */
/* .line 108 */
/* .local v3, "wakelock":Lcom/miui/server/smartpower/AppWakelockResource$WakeLock; */
/* iget v4, v3, Lcom/miui/server/smartpower/AppWakelockResource$WakeLock;->mOwnerUid:I */
/* if-ne v4, p1, :cond_0 */
/* .line 109 */
v4 = this.mTag;
(( android.util.ArraySet ) v0 ).add ( v4 ); // invoke-virtual {v0, v4}, Landroid/util/ArraySet;->add(Ljava/lang/Object;)Z
/* .line 111 */
} // .end local v3 # "wakelock":Lcom/miui/server/smartpower/AppWakelockResource$WakeLock;
} // :cond_0
/* .line 112 */
} // :cond_1
/* monitor-exit v1 */
/* :try_end_1 */
/* .catchall {:try_start_1 ..:try_end_1} :catchall_0 */
/* .line 113 */
try { // :try_start_2
(( android.util.ArraySet ) v0 ).iterator ( ); // invoke-virtual {v0}, Landroid/util/ArraySet;->iterator()Ljava/util/Iterator;
v2 = } // :goto_1
if ( v2 != null) { // if-eqz v2, :cond_3
/* check-cast v2, Ljava/lang/String; */
/* .line 114 */
/* .local v2, "tag":Ljava/lang/String; */
com.android.server.power.PowerManagerServiceStub .get ( );
/* if-nez p2, :cond_2 */
int v4 = 1; // const/4 v4, 0x1
} // :cond_2
int v4 = 0; // const/4 v4, 0x0
} // :goto_2
(( com.android.server.power.PowerManagerServiceStub ) v3 ).setUidPartialWakeLockDisabledState ( p1, v2, v4 ); // invoke-virtual {v3, p1, v2, v4}, Lcom/android/server/power/PowerManagerServiceStub;->setUidPartialWakeLockDisabledState(ILjava/lang/String;Z)V
/* .line 115 */
} // .end local v2 # "tag":Ljava/lang/String;
/* .line 116 */
} // :cond_3
if ( p2 != null) { // if-eqz p2, :cond_4
final String v1 = "resume"; // const-string v1, "resume"
} // :cond_4
final String v1 = "release"; // const-string v1, "release"
/* .line 117 */
/* .local v1, "reason":Ljava/lang/String; */
} // :goto_3
/* new-instance v2, Ljava/lang/StringBuilder; */
/* invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V */
/* const-string/jumbo v3, "wakelock u:" */
(( java.lang.StringBuilder ) v2 ).append ( v3 ); // invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v2 ).append ( p1 ); // invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;
final String v3 = " s:"; // const-string v3, " s:"
(( java.lang.StringBuilder ) v2 ).append ( v3 ); // invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v2 ).append ( v1 ); // invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v2 ).toString ( ); // invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
/* const v3, 0x15ff2 */
android.util.EventLog .writeEvent ( v3,v2 );
/* .line 119 */
/* sget-boolean v2, Lcom/miui/server/smartpower/AppWakelockResource;->DEBUG:Z */
if ( v2 != null) { // if-eqz v2, :cond_5
final String v2 = "SmartPower.AppResource"; // const-string v2, "SmartPower.AppResource"
/* new-instance v3, Ljava/lang/StringBuilder; */
/* invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V */
/* const-string/jumbo v4, "setWakeLockState " */
(( java.lang.StringBuilder ) v3 ).append ( v4 ); // invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v3 ).append ( p1 ); // invoke-virtual {v3, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;
final String v4 = " s:"; // const-string v4, " s:"
(( java.lang.StringBuilder ) v3 ).append ( v4 ); // invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v3 ).append ( v1 ); // invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v3 ).toString ( ); // invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
android.util.Slog .d ( v2,v3 );
/* :try_end_2 */
/* .catch Ljava/lang/Exception; {:try_start_2 ..:try_end_2} :catch_0 */
/* .line 122 */
} // .end local v0 # "targetTags":Landroid/util/ArraySet;, "Landroid/util/ArraySet<Ljava/lang/String;>;"
} // .end local v1 # "reason":Ljava/lang/String;
} // :cond_5
/* .line 112 */
/* .restart local v0 # "targetTags":Landroid/util/ArraySet;, "Landroid/util/ArraySet<Ljava/lang/String;>;" */
/* :catchall_0 */
/* move-exception v2 */
try { // :try_start_3
/* monitor-exit v1 */
/* :try_end_3 */
/* .catchall {:try_start_3 ..:try_end_3} :catchall_0 */
} // .end local p0 # "this":Lcom/miui/server/smartpower/AppWakelockResource;
} // .end local p1 # "uid":I
} // .end local p2 # "enable":Z
try { // :try_start_4
/* throw v2 */
/* :try_end_4 */
/* .catch Ljava/lang/Exception; {:try_start_4 ..:try_end_4} :catch_0 */
/* .line 120 */
} // .end local v0 # "targetTags":Landroid/util/ArraySet;, "Landroid/util/ArraySet<Ljava/lang/String;>;"
/* .restart local p0 # "this":Lcom/miui/server/smartpower/AppWakelockResource; */
/* .restart local p1 # "uid":I */
/* .restart local p2 # "enable":Z */
/* :catch_0 */
/* move-exception v0 */
/* .line 121 */
/* .local v0, "e":Ljava/lang/Exception; */
final String v1 = "SmartPower.AppResource"; // const-string v1, "SmartPower.AppResource"
final String v2 = "releaseAppPowerResource"; // const-string v2, "releaseAppPowerResource"
android.util.Slog .e ( v1,v2,v0 );
/* .line 123 */
} // .end local v0 # "e":Ljava/lang/Exception;
} // :goto_4
return;
} // .end method
/* # virtual methods */
public void acquireWakelock ( android.os.IBinder p0, Integer p1, java.lang.String p2, Integer p3, Integer p4 ) {
/* .locals 10 */
/* .param p1, "lock" # Landroid/os/IBinder; */
/* .param p2, "flags" # I */
/* .param p3, "tag" # Ljava/lang/String; */
/* .param p4, "ownerUid" # I */
/* .param p5, "ownerPid" # I */
/* .line 126 */
v0 = this.mLock;
/* monitor-enter v0 */
/* .line 128 */
try { // :try_start_0
v1 = /* invoke-direct {p0, p1}, Lcom/miui/server/smartpower/AppWakelockResource;->findWakeLockIndexLocked(Landroid/os/IBinder;)I */
/* .line 129 */
/* .local v1, "index":I */
/* if-ltz v1, :cond_0 */
/* .line 130 */
v2 = this.mWakeLocks;
(( java.util.ArrayList ) v2 ).get ( v1 ); // invoke-virtual {v2, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;
/* check-cast v2, Lcom/miui/server/smartpower/AppWakelockResource$WakeLock; */
/* .line 131 */
/* .local v2, "wakeLock":Lcom/miui/server/smartpower/AppWakelockResource$WakeLock; */
(( com.miui.server.smartpower.AppWakelockResource$WakeLock ) v2 ).updateProperties ( p2, p3 ); // invoke-virtual {v2, p2, p3}, Lcom/miui/server/smartpower/AppWakelockResource$WakeLock;->updateProperties(ILjava/lang/String;)V
/* .line 133 */
} // .end local v2 # "wakeLock":Lcom/miui/server/smartpower/AppWakelockResource$WakeLock;
} // :cond_0
/* new-instance v9, Lcom/miui/server/smartpower/AppWakelockResource$WakeLock; */
/* move-object v2, v9 */
/* move-object v3, p0 */
/* move-object v4, p1 */
/* move v5, p2 */
/* move-object v6, p3 */
/* move v7, p4 */
/* move v8, p5 */
/* invoke-direct/range {v2 ..v8}, Lcom/miui/server/smartpower/AppWakelockResource$WakeLock;-><init>(Lcom/miui/server/smartpower/AppWakelockResource;Landroid/os/IBinder;ILjava/lang/String;II)V */
/* :try_end_0 */
/* .catchall {:try_start_0 ..:try_end_0} :catchall_0 */
/* move-object v2, v9 */
/* .line 135 */
/* .restart local v2 # "wakeLock":Lcom/miui/server/smartpower/AppWakelockResource$WakeLock; */
int v3 = 0; // const/4 v3, 0x0
try { // :try_start_1
/* :try_end_1 */
/* .catch Landroid/os/RemoteException; {:try_start_1 ..:try_end_1} :catch_0 */
/* .catchall {:try_start_1 ..:try_end_1} :catchall_0 */
/* .line 138 */
/* nop */
/* .line 139 */
try { // :try_start_2
v3 = this.mWakeLocks;
(( java.util.ArrayList ) v3 ).add ( v2 ); // invoke-virtual {v3, v2}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z
/* .line 141 */
} // :goto_0
/* sget-boolean v3, Lcom/miui/server/smartpower/AppWakelockResource;->DEBUG:Z */
if ( v3 != null) { // if-eqz v3, :cond_1
/* .line 142 */
final String v3 = "SmartPower.AppResource"; // const-string v3, "SmartPower.AppResource"
/* new-instance v4, Ljava/lang/StringBuilder; */
/* invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V */
final String v5 = "onAcquireWakelock: "; // const-string v5, "onAcquireWakelock: "
(( java.lang.StringBuilder ) v4 ).append ( v5 ); // invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( com.miui.server.smartpower.AppWakelockResource$WakeLock ) v2 ).toString ( ); // invoke-virtual {v2}, Lcom/miui/server/smartpower/AppWakelockResource$WakeLock;->toString()Ljava/lang/String;
(( java.lang.StringBuilder ) v4 ).append ( v5 ); // invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v4 ).toString ( ); // invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
android.util.Slog .d ( v3,v4 );
/* .line 144 */
} // .end local v1 # "index":I
} // .end local v2 # "wakeLock":Lcom/miui/server/smartpower/AppWakelockResource$WakeLock;
} // :cond_1
/* monitor-exit v0 */
/* :try_end_2 */
/* .catchall {:try_start_2 ..:try_end_2} :catchall_0 */
/* .line 145 */
v0 = this.mHandler;
/* new-instance v1, Lcom/miui/server/smartpower/AppWakelockResource$$ExternalSyntheticLambda0; */
/* invoke-direct {v1, p0, p1, p4}, Lcom/miui/server/smartpower/AppWakelockResource$$ExternalSyntheticLambda0;-><init>(Lcom/miui/server/smartpower/AppWakelockResource;Landroid/os/IBinder;I)V */
(( android.os.Handler ) v0 ).post ( v1 ); // invoke-virtual {v0, v1}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z
/* .line 146 */
return;
/* .line 136 */
/* .restart local v1 # "index":I */
/* .restart local v2 # "wakeLock":Lcom/miui/server/smartpower/AppWakelockResource$WakeLock; */
/* :catch_0 */
/* move-exception v3 */
/* .line 137 */
/* .local v3, "ex":Landroid/os/RemoteException; */
try { // :try_start_3
/* new-instance v4, Ljava/lang/IllegalArgumentException; */
final String v5 = "Wake lock is already dead."; // const-string v5, "Wake lock is already dead."
/* invoke-direct {v4, v5}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V */
} // .end local p0 # "this":Lcom/miui/server/smartpower/AppWakelockResource;
} // .end local p1 # "lock":Landroid/os/IBinder;
} // .end local p2 # "flags":I
} // .end local p3 # "tag":Ljava/lang/String;
} // .end local p4 # "ownerUid":I
} // .end local p5 # "ownerPid":I
/* throw v4 */
/* .line 144 */
} // .end local v1 # "index":I
} // .end local v2 # "wakeLock":Lcom/miui/server/smartpower/AppWakelockResource$WakeLock;
} // .end local v3 # "ex":Landroid/os/RemoteException;
/* .restart local p0 # "this":Lcom/miui/server/smartpower/AppWakelockResource; */
/* .restart local p1 # "lock":Landroid/os/IBinder; */
/* .restart local p2 # "flags":I */
/* .restart local p3 # "tag":Ljava/lang/String; */
/* .restart local p4 # "ownerUid":I */
/* .restart local p5 # "ownerPid":I */
/* :catchall_0 */
/* move-exception v1 */
/* monitor-exit v0 */
/* :try_end_3 */
/* .catchall {:try_start_3 ..:try_end_3} :catchall_0 */
/* throw v1 */
} // .end method
public java.util.ArrayList getActiveUids ( ) {
/* .locals 5 */
/* .line 50 */
/* new-instance v0, Ljava/util/ArrayList; */
/* invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V */
/* .line 51 */
/* .local v0, "uids":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/Integer;>;" */
v1 = this.mLock;
/* monitor-enter v1 */
/* .line 52 */
try { // :try_start_0
v2 = this.mWakeLocks;
(( java.util.ArrayList ) v2 ).iterator ( ); // invoke-virtual {v2}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;
v3 = } // :goto_0
if ( v3 != null) { // if-eqz v3, :cond_0
/* check-cast v3, Lcom/miui/server/smartpower/AppWakelockResource$WakeLock; */
/* .line 53 */
/* .local v3, "wakeLock":Lcom/miui/server/smartpower/AppWakelockResource$WakeLock; */
/* iget v4, v3, Lcom/miui/server/smartpower/AppWakelockResource$WakeLock;->mOwnerUid:I */
java.lang.Integer .valueOf ( v4 );
(( java.util.ArrayList ) v0 ).add ( v4 ); // invoke-virtual {v0, v4}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z
/* .line 54 */
/* nop */
} // .end local v3 # "wakeLock":Lcom/miui/server/smartpower/AppWakelockResource$WakeLock;
/* .line 55 */
} // :cond_0
/* monitor-exit v1 */
/* .line 56 */
/* .line 55 */
/* :catchall_0 */
/* move-exception v2 */
/* monitor-exit v1 */
/* :try_end_0 */
/* .catchall {:try_start_0 ..:try_end_0} :catchall_0 */
/* throw v2 */
} // .end method
public Boolean isAppResourceActive ( Integer p0 ) {
/* .locals 4 */
/* .param p1, "uid" # I */
/* .line 61 */
v0 = this.mLock;
/* monitor-enter v0 */
/* .line 62 */
try { // :try_start_0
v1 = this.mWakeLocks;
(( java.util.ArrayList ) v1 ).iterator ( ); // invoke-virtual {v1}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;
v2 = } // :goto_0
if ( v2 != null) { // if-eqz v2, :cond_1
/* check-cast v2, Lcom/miui/server/smartpower/AppWakelockResource$WakeLock; */
/* .line 63 */
/* .local v2, "wakeLock":Lcom/miui/server/smartpower/AppWakelockResource$WakeLock; */
/* iget v3, v2, Lcom/miui/server/smartpower/AppWakelockResource$WakeLock;->mOwnerUid:I */
/* if-ne v3, p1, :cond_0 */
/* .line 64 */
/* monitor-exit v0 */
int v0 = 1; // const/4 v0, 0x1
/* .line 66 */
} // .end local v2 # "wakeLock":Lcom/miui/server/smartpower/AppWakelockResource$WakeLock;
} // :cond_0
/* .line 67 */
} // :cond_1
/* monitor-exit v0 */
/* .line 68 */
int v0 = 0; // const/4 v0, 0x0
/* .line 67 */
/* :catchall_0 */
/* move-exception v1 */
/* monitor-exit v0 */
/* :try_end_0 */
/* .catchall {:try_start_0 ..:try_end_0} :catchall_0 */
/* throw v1 */
} // .end method
public Boolean isAppResourceActive ( Integer p0, Integer p1 ) {
/* .locals 4 */
/* .param p1, "uid" # I */
/* .param p2, "pid" # I */
/* .line 73 */
v0 = this.mLock;
/* monitor-enter v0 */
/* .line 74 */
try { // :try_start_0
v1 = this.mWakeLocks;
(( java.util.ArrayList ) v1 ).iterator ( ); // invoke-virtual {v1}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;
v2 = } // :goto_0
if ( v2 != null) { // if-eqz v2, :cond_1
/* check-cast v2, Lcom/miui/server/smartpower/AppWakelockResource$WakeLock; */
/* .line 75 */
/* .local v2, "wakeLock":Lcom/miui/server/smartpower/AppWakelockResource$WakeLock; */
/* iget v3, v2, Lcom/miui/server/smartpower/AppWakelockResource$WakeLock;->mOwnerPid:I */
/* if-ne v3, p2, :cond_0 */
/* .line 76 */
/* monitor-exit v0 */
int v0 = 1; // const/4 v0, 0x1
/* .line 78 */
} // .end local v2 # "wakeLock":Lcom/miui/server/smartpower/AppWakelockResource$WakeLock;
} // :cond_0
/* .line 79 */
} // :cond_1
/* monitor-exit v0 */
/* .line 80 */
int v0 = 0; // const/4 v0, 0x0
/* .line 79 */
/* :catchall_0 */
/* move-exception v1 */
/* monitor-exit v0 */
/* :try_end_0 */
/* .catchall {:try_start_0 ..:try_end_0} :catchall_0 */
/* throw v1 */
} // .end method
public void onAppBehaviorEvent ( android.os.IBinder p0, Integer p1, Boolean p2 ) {
/* .locals 9 */
/* .param p1, "binder" # Landroid/os/IBinder; */
/* .param p2, "uid" # I */
/* .param p3, "state" # Z */
/* .line 287 */
v0 = android.os.UserHandle .getAppId ( p2 );
/* const/16 v1, 0x2710 */
/* if-ge v0, v1, :cond_0 */
/* .line 288 */
return;
/* .line 290 */
} // :cond_0
v0 = this.mPackageManagerInt;
(( android.content.pm.PackageManagerInternal ) v0 ).getNameForUid ( p2 ); // invoke-virtual {v0, p2}, Landroid/content/pm/PackageManagerInternal;->getNameForUid(I)Ljava/lang/String;
/* .line 291 */
/* .local v0, "packageName":Ljava/lang/String; */
v1 = this.mPackageManagerInt;
/* const-wide/16 v3, 0x0 */
/* const/16 v5, 0x3e8 */
/* .line 292 */
v6 = android.os.UserHandle .getUserId ( p2 );
/* .line 291 */
/* move-object v2, v0 */
/* invoke-virtual/range {v1 ..v6}, Landroid/content/pm/PackageManagerInternal;->getApplicationInfo(Ljava/lang/String;JII)Landroid/content/pm/ApplicationInfo; */
/* .line 293 */
/* .local v1, "applicationInfo":Landroid/content/pm/ApplicationInfo; */
if ( v1 != null) { // if-eqz v1, :cond_2
v2 = com.android.server.am.ProcessUtils .isSystem ( v1 );
if ( v2 != null) { // if-eqz v2, :cond_1
/* .line 296 */
} // :cond_1
v3 = this.mSecurityManagerInt;
/* const/16 v4, 0x1a */
v7 = this.packageName;
/* move-object v5, p1 */
/* move v6, p2 */
/* move v8, p3 */
/* invoke-virtual/range {v3 ..v8}, Lmiui/security/SecurityManagerInternal;->recordDurationInfo(ILandroid/os/IBinder;ILjava/lang/String;Z)V */
/* .line 298 */
return;
/* .line 294 */
} // :cond_2
} // :goto_0
return;
} // .end method
public void releaseAppPowerResource ( Integer p0 ) {
/* .locals 2 */
/* .param p1, "uid" # I */
/* .line 85 */
v0 = android.os.UserHandle .isApp ( p1 );
if ( v0 != null) { // if-eqz v0, :cond_0
/* .line 86 */
v0 = this.mHandler;
int v1 = 1; // const/4 v1, 0x1
(( android.os.Handler ) v0 ).obtainMessage ( v1 ); // invoke-virtual {v0, v1}, Landroid/os/Handler;->obtainMessage(I)Landroid/os/Message;
/* .line 87 */
/* .local v0, "msg":Landroid/os/Message; */
/* iput p1, v0, Landroid/os/Message;->arg1:I */
/* .line 88 */
int v1 = 0; // const/4 v1, 0x0
/* iput v1, v0, Landroid/os/Message;->arg2:I */
/* .line 89 */
v1 = this.mHandler;
(( android.os.Handler ) v1 ).sendMessage ( v0 ); // invoke-virtual {v1, v0}, Landroid/os/Handler;->sendMessage(Landroid/os/Message;)Z
/* .line 91 */
} // .end local v0 # "msg":Landroid/os/Message;
} // :cond_0
return;
} // .end method
public void releaseWakelock ( android.os.IBinder p0, Integer p1 ) {
/* .locals 7 */
/* .param p1, "lock" # Landroid/os/IBinder; */
/* .param p2, "flags" # I */
/* .line 149 */
v0 = this.mLock;
/* monitor-enter v0 */
/* .line 150 */
try { // :try_start_0
v1 = /* invoke-direct {p0, p1}, Lcom/miui/server/smartpower/AppWakelockResource;->findWakeLockIndexLocked(Landroid/os/IBinder;)I */
/* .line 151 */
/* .local v1, "index":I */
/* if-gez v1, :cond_0 */
/* .line 152 */
/* monitor-exit v0 */
return;
/* .line 155 */
} // :cond_0
v2 = this.mWakeLocks;
(( java.util.ArrayList ) v2 ).get ( v1 ); // invoke-virtual {v2, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;
/* check-cast v2, Lcom/miui/server/smartpower/AppWakelockResource$WakeLock; */
/* .line 156 */
/* .local v2, "wakeLock":Lcom/miui/server/smartpower/AppWakelockResource$WakeLock; */
/* sget-boolean v3, Lcom/miui/server/smartpower/AppWakelockResource;->DEBUG:Z */
if ( v3 != null) { // if-eqz v3, :cond_1
/* .line 157 */
final String v3 = "SmartPower.AppResource"; // const-string v3, "SmartPower.AppResource"
/* new-instance v4, Ljava/lang/StringBuilder; */
/* invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V */
final String v5 = "onReleaseWakelock: "; // const-string v5, "onReleaseWakelock: "
(( java.lang.StringBuilder ) v4 ).append ( v5 ); // invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( com.miui.server.smartpower.AppWakelockResource$WakeLock ) v2 ).toString ( ); // invoke-virtual {v2}, Lcom/miui/server/smartpower/AppWakelockResource$WakeLock;->toString()Ljava/lang/String;
(( java.lang.StringBuilder ) v4 ).append ( v5 ); // invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v4 ).toString ( ); // invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
android.util.Slog .d ( v3,v4 );
/* :try_end_0 */
/* .catchall {:try_start_0 ..:try_end_0} :catchall_0 */
/* .line 160 */
} // :cond_1
try { // :try_start_1
v3 = this.mLock;
int v4 = 0; // const/4 v4, 0x0
/* :try_end_1 */
/* .catch Ljava/lang/Exception; {:try_start_1 ..:try_end_1} :catch_0 */
/* .catchall {:try_start_1 ..:try_end_1} :catchall_0 */
/* .line 164 */
/* .line 161 */
/* :catch_0 */
/* move-exception v3 */
/* .line 162 */
/* .local v3, "e":Ljava/lang/Exception; */
try { // :try_start_2
final String v4 = "SmartPower.AppResource"; // const-string v4, "SmartPower.AppResource"
/* new-instance v5, Ljava/lang/StringBuilder; */
/* invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V */
/* const-string/jumbo v6, "unlinkToDeath failed wakelock=" */
(( java.lang.StringBuilder ) v5 ).append ( v6 ); // invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
/* .line 163 */
(( com.miui.server.smartpower.AppWakelockResource$WakeLock ) v2 ).toString ( ); // invoke-virtual {v2}, Lcom/miui/server/smartpower/AppWakelockResource$WakeLock;->toString()Ljava/lang/String;
(( java.lang.StringBuilder ) v5 ).append ( v6 ); // invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
final String v6 = " lock="; // const-string v6, " lock="
(( java.lang.StringBuilder ) v5 ).append ( v6 ); // invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v5 ).append ( p1 ); // invoke-virtual {v5, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v5 ).toString ( ); // invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
/* .line 162 */
android.util.Slog .w ( v4,v5,v3 );
/* .line 165 */
} // .end local v3 # "e":Ljava/lang/Exception;
} // :goto_0
/* invoke-direct {p0, v2, v1}, Lcom/miui/server/smartpower/AppWakelockResource;->removeWakeLockLocked(Lcom/miui/server/smartpower/AppWakelockResource$WakeLock;I)V */
/* .line 166 */
} // .end local v1 # "index":I
} // .end local v2 # "wakeLock":Lcom/miui/server/smartpower/AppWakelockResource$WakeLock;
/* monitor-exit v0 */
/* .line 167 */
return;
/* .line 166 */
/* :catchall_0 */
/* move-exception v1 */
/* monitor-exit v0 */
/* :try_end_2 */
/* .catchall {:try_start_2 ..:try_end_2} :catchall_0 */
/* throw v1 */
} // .end method
public void resumeAppPowerResource ( Integer p0 ) {
/* .locals 2 */
/* .param p1, "uid" # I */
/* .line 95 */
v0 = android.os.UserHandle .isApp ( p1 );
if ( v0 != null) { // if-eqz v0, :cond_0
/* .line 96 */
v0 = this.mHandler;
int v1 = 1; // const/4 v1, 0x1
(( android.os.Handler ) v0 ).obtainMessage ( v1 ); // invoke-virtual {v0, v1}, Landroid/os/Handler;->obtainMessage(I)Landroid/os/Message;
/* .line 97 */
/* .local v0, "msg":Landroid/os/Message; */
/* iput p1, v0, Landroid/os/Message;->arg1:I */
/* .line 98 */
/* iput v1, v0, Landroid/os/Message;->arg2:I */
/* .line 99 */
v1 = this.mHandler;
(( android.os.Handler ) v1 ).sendMessage ( v0 ); // invoke-virtual {v1, v0}, Landroid/os/Handler;->sendMessage(Landroid/os/Message;)Z
/* .line 101 */
} // .end local v0 # "msg":Landroid/os/Message;
} // :cond_0
return;
} // .end method
