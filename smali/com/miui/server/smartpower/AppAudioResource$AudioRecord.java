class com.miui.server.smartpower.AppAudioResource$AudioRecord {
	 /* .source "AppAudioResource.java" */
	 /* # annotations */
	 /* .annotation system Ldalvik/annotation/EnclosingClass; */
	 /* value = Lcom/miui/server/smartpower/AppAudioResource; */
} // .end annotation
/* .annotation system Ldalvik/annotation/InnerClass; */
/* accessFlags = 0x2 */
/* name = "AudioRecord" */
} // .end annotation
/* .annotation system Ldalvik/annotation/MemberClasses; */
/* value = { */
/* Lcom/miui/server/smartpower/AppAudioResource$AudioRecord$RecorderRecord;, */
/* Lcom/miui/server/smartpower/AppAudioResource$AudioRecord$PlayerRecord; */
/* } */
} // .end annotation
/* # instance fields */
Boolean mActive;
Integer mBehavier;
Boolean mCurrentActive;
Integer mCurrentBehavier;
Boolean mCurrentPlaying;
Integer mFocusLoss;
final android.util.ArrayMap mFocusedClientIds;
/* .annotation system Ldalvik/annotation/Signature; */
/* value = { */
/* "Landroid/util/ArrayMap<", */
/* "Ljava/lang/String;", */
/* "Ljava/lang/Integer;", */
/* ">;" */
/* } */
} // .end annotation
} // .end field
java.lang.Runnable mInactiveDelayTask;
Long mLastPlayingTimeStamp;
Integer mOwnerPid;
Integer mOwnerUid;
Integer mPlaybackState;
final android.util.ArrayMap mPlayerRecords;
/* .annotation system Ldalvik/annotation/Signature; */
/* value = { */
/* "Landroid/util/ArrayMap<", */
/* "Ljava/lang/Integer;", */
/* "Lcom/miui/server/smartpower/AppAudioResource$AudioRecord$PlayerRecord;", */
/* ">;" */
/* } */
} // .end annotation
} // .end field
Boolean mPlaying;
final android.util.ArrayMap mRecorderRecords;
/* .annotation system Ldalvik/annotation/Signature; */
/* value = { */
/* "Landroid/util/ArrayMap<", */
/* "Ljava/lang/Integer;", */
/* "Lcom/miui/server/smartpower/AppAudioResource$AudioRecord$RecorderRecord;", */
/* ">;" */
/* } */
} // .end annotation
} // .end field
final com.miui.server.smartpower.AppAudioResource this$0; //synthetic
/* # direct methods */
static void -$$Nest$mpauseZeroAudioTrack ( com.miui.server.smartpower.AppAudioResource$AudioRecord p0 ) { //bridge//synthethic
/* .locals 0 */
/* invoke-direct {p0}, Lcom/miui/server/smartpower/AppAudioResource$AudioRecord;->pauseZeroAudioTrack()V */
return;
} // .end method
static void -$$Nest$mrealUpdateAudioStatus ( com.miui.server.smartpower.AppAudioResource$AudioRecord p0 ) { //bridge//synthethic
/* .locals 0 */
/* invoke-direct {p0}, Lcom/miui/server/smartpower/AppAudioResource$AudioRecord;->realUpdateAudioStatus()V */
return;
} // .end method
static void -$$Nest$mupdateAudioStatus ( com.miui.server.smartpower.AppAudioResource$AudioRecord p0 ) { //bridge//synthethic
/* .locals 0 */
/* invoke-direct {p0}, Lcom/miui/server/smartpower/AppAudioResource$AudioRecord;->updateAudioStatus()V */
return;
} // .end method
 com.miui.server.smartpower.AppAudioResource$AudioRecord ( ) {
/* .locals 2 */
/* .param p2, "uid" # I */
/* .param p3, "pid" # I */
/* .line 233 */
this.this$0 = p1;
/* invoke-direct {p0}, Ljava/lang/Object;-><init>()V */
/* .line 218 */
int p1 = 0; // const/4 p1, 0x0
/* iput-boolean p1, p0, Lcom/miui/server/smartpower/AppAudioResource$AudioRecord;->mActive:Z */
/* .line 219 */
/* iput-boolean p1, p0, Lcom/miui/server/smartpower/AppAudioResource$AudioRecord;->mCurrentActive:Z */
/* .line 220 */
/* iput p1, p0, Lcom/miui/server/smartpower/AppAudioResource$AudioRecord;->mBehavier:I */
/* .line 221 */
/* iput p1, p0, Lcom/miui/server/smartpower/AppAudioResource$AudioRecord;->mCurrentBehavier:I */
/* .line 222 */
/* iput-boolean p1, p0, Lcom/miui/server/smartpower/AppAudioResource$AudioRecord;->mPlaying:Z */
/* .line 223 */
/* iput-boolean p1, p0, Lcom/miui/server/smartpower/AppAudioResource$AudioRecord;->mCurrentPlaying:Z */
/* .line 224 */
int v0 = -1; // const/4 v0, -0x1
/* iput v0, p0, Lcom/miui/server/smartpower/AppAudioResource$AudioRecord;->mPlaybackState:I */
/* .line 225 */
/* new-instance v0, Landroid/util/ArrayMap; */
/* invoke-direct {v0}, Landroid/util/ArrayMap;-><init>()V */
this.mFocusedClientIds = v0;
/* .line 226 */
/* iput p1, p0, Lcom/miui/server/smartpower/AppAudioResource$AudioRecord;->mFocusLoss:I */
/* .line 227 */
/* const-wide/16 v0, 0x0 */
/* iput-wide v0, p0, Lcom/miui/server/smartpower/AppAudioResource$AudioRecord;->mLastPlayingTimeStamp:J */
/* .line 229 */
/* new-instance p1, Landroid/util/ArrayMap; */
/* invoke-direct {p1}, Landroid/util/ArrayMap;-><init>()V */
this.mPlayerRecords = p1;
/* .line 230 */
/* new-instance p1, Landroid/util/ArrayMap; */
/* invoke-direct {p1}, Landroid/util/ArrayMap;-><init>()V */
this.mRecorderRecords = p1;
/* .line 234 */
/* iput p2, p0, Lcom/miui/server/smartpower/AppAudioResource$AudioRecord;->mOwnerUid:I */
/* .line 235 */
/* iput p3, p0, Lcom/miui/server/smartpower/AppAudioResource$AudioRecord;->mOwnerPid:I */
/* .line 236 */
return;
} // .end method
private com.miui.server.smartpower.AppAudioResource$AudioRecord$PlayerRecord getPlayerRecord ( Integer p0 ) {
/* .locals 3 */
/* .param p1, "piid" # I */
/* .line 478 */
v0 = this.mPlayerRecords;
/* monitor-enter v0 */
/* .line 479 */
try { // :try_start_0
v1 = this.mPlayerRecords;
java.lang.Integer .valueOf ( p1 );
(( android.util.ArrayMap ) v1 ).get ( v2 ); // invoke-virtual {v1, v2}, Landroid/util/ArrayMap;->get(Ljava/lang/Object;)Ljava/lang/Object;
/* check-cast v1, Lcom/miui/server/smartpower/AppAudioResource$AudioRecord$PlayerRecord; */
/* monitor-exit v0 */
/* .line 480 */
/* :catchall_0 */
/* move-exception v1 */
/* monitor-exit v0 */
/* :try_end_0 */
/* .catchall {:try_start_0 ..:try_end_0} :catchall_0 */
/* throw v1 */
} // .end method
private com.miui.server.smartpower.AppAudioResource$AudioRecord$RecorderRecord getRecorderRecord ( Integer p0 ) {
/* .locals 3 */
/* .param p1, "riid" # I */
/* .line 484 */
v0 = this.mRecorderRecords;
/* monitor-enter v0 */
/* .line 485 */
try { // :try_start_0
v1 = this.mRecorderRecords;
java.lang.Integer .valueOf ( p1 );
(( android.util.ArrayMap ) v1 ).get ( v2 ); // invoke-virtual {v1, v2}, Landroid/util/ArrayMap;->get(Ljava/lang/Object;)Ljava/lang/Object;
/* check-cast v1, Lcom/miui/server/smartpower/AppAudioResource$AudioRecord$RecorderRecord; */
/* monitor-exit v0 */
/* .line 486 */
/* :catchall_0 */
/* move-exception v1 */
/* monitor-exit v0 */
/* :try_end_0 */
/* .catchall {:try_start_0 ..:try_end_0} :catchall_0 */
/* throw v1 */
} // .end method
private Boolean isZeroAudioRecord ( ) {
/* .locals 5 */
/* .line 490 */
v0 = this.mPlayerRecords;
/* monitor-enter v0 */
/* .line 491 */
try { // :try_start_0
v1 = this.mPlayerRecords;
(( android.util.ArrayMap ) v1 ).values ( ); // invoke-virtual {v1}, Landroid/util/ArrayMap;->values()Ljava/util/Collection;
v2 = } // :goto_0
if ( v2 != null) { // if-eqz v2, :cond_1
/* check-cast v2, Lcom/miui/server/smartpower/AppAudioResource$AudioRecord$PlayerRecord; */
/* .line 492 */
/* .local v2, "r":Lcom/miui/server/smartpower/AppAudioResource$AudioRecord$PlayerRecord; */
/* iget v3, v2, Lcom/miui/server/smartpower/AppAudioResource$AudioRecord$PlayerRecord;->mEvent:I */
/* const/16 v4, 0x64 */
/* if-ne v3, v4, :cond_0 */
/* .line 493 */
/* monitor-exit v0 */
int v0 = 1; // const/4 v0, 0x1
/* .line 495 */
} // .end local v2 # "r":Lcom/miui/server/smartpower/AppAudioResource$AudioRecord$PlayerRecord;
} // :cond_0
/* .line 496 */
} // :cond_1
/* monitor-exit v0 */
/* .line 497 */
int v0 = 0; // const/4 v0, 0x0
/* .line 496 */
/* :catchall_0 */
/* move-exception v1 */
/* monitor-exit v0 */
/* :try_end_0 */
/* .catchall {:try_start_0 ..:try_end_0} :catchall_0 */
/* throw v1 */
} // .end method
private void pauseZeroAudioTrack ( ) {
/* .locals 6 */
/* .line 501 */
v0 = this.mPlayerRecords;
/* monitor-enter v0 */
/* .line 502 */
try { // :try_start_0
v1 = this.mPlayerRecords;
(( android.util.ArrayMap ) v1 ).values ( ); // invoke-virtual {v1}, Landroid/util/ArrayMap;->values()Ljava/util/Collection;
v2 = } // :goto_0
if ( v2 != null) { // if-eqz v2, :cond_1
/* check-cast v2, Lcom/miui/server/smartpower/AppAudioResource$AudioRecord$PlayerRecord; */
/* .line 503 */
/* .local v2, "r":Lcom/miui/server/smartpower/AppAudioResource$AudioRecord$PlayerRecord; */
/* iget v3, v2, Lcom/miui/server/smartpower/AppAudioResource$AudioRecord$PlayerRecord;->mEvent:I */
/* const/16 v4, 0x64 */
/* if-ne v3, v4, :cond_0 */
/* .line 504 */
/* iget v3, p0, Lcom/miui/server/smartpower/AppAudioResource$AudioRecord;->mOwnerUid:I */
/* iget v4, p0, Lcom/miui/server/smartpower/AppAudioResource$AudioRecord;->mOwnerPid:I */
/* iget v5, v2, Lcom/miui/server/smartpower/AppAudioResource$AudioRecord$PlayerRecord;->mSessionId:I */
android.media.AudioSystem .pauseAudioTracks ( v3,v4,v5 );
/* .line 505 */
/* new-instance v3, Ljava/lang/StringBuilder; */
/* invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V */
/* const-string/jumbo v4, "zero audio u:" */
(( java.lang.StringBuilder ) v3 ).append ( v4 ); // invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
/* iget v4, p0, Lcom/miui/server/smartpower/AppAudioResource$AudioRecord;->mOwnerUid:I */
(( java.lang.StringBuilder ) v3 ).append ( v4 ); // invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;
final String v4 = " session:"; // const-string v4, " session:"
(( java.lang.StringBuilder ) v3 ).append ( v4 ); // invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
/* iget v4, v2, Lcom/miui/server/smartpower/AppAudioResource$AudioRecord$PlayerRecord;->mSessionId:I */
(( java.lang.StringBuilder ) v3 ).append ( v4 ); // invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;
final String v4 = " s:release"; // const-string v4, " s:release"
(( java.lang.StringBuilder ) v3 ).append ( v4 ); // invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v3 ).toString ( ); // invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
/* .line 507 */
/* .local v3, "event":Ljava/lang/String; */
/* const v4, 0x15ff2 */
android.util.EventLog .writeEvent ( v4,v3 );
/* .line 509 */
} // .end local v2 # "r":Lcom/miui/server/smartpower/AppAudioResource$AudioRecord$PlayerRecord;
} // .end local v3 # "event":Ljava/lang/String;
} // :cond_0
/* .line 510 */
} // :cond_1
/* monitor-exit v0 */
/* .line 511 */
return;
/* .line 510 */
/* :catchall_0 */
/* move-exception v1 */
/* monitor-exit v0 */
/* :try_end_0 */
/* .catchall {:try_start_0 ..:try_end_0} :catchall_0 */
/* throw v1 */
} // .end method
private void realUpdateAudioStatus ( ) {
/* .locals 5 */
/* .line 463 */
/* iget-boolean v0, p0, Lcom/miui/server/smartpower/AppAudioResource$AudioRecord;->mActive:Z */
/* iget-boolean v1, p0, Lcom/miui/server/smartpower/AppAudioResource$AudioRecord;->mCurrentActive:Z */
/* if-ne v0, v1, :cond_0 */
/* iget v0, p0, Lcom/miui/server/smartpower/AppAudioResource$AudioRecord;->mBehavier:I */
/* iget v2, p0, Lcom/miui/server/smartpower/AppAudioResource$AudioRecord;->mCurrentBehavier:I */
/* if-eq v0, v2, :cond_2 */
/* .line 464 */
} // :cond_0
/* iput-boolean v1, p0, Lcom/miui/server/smartpower/AppAudioResource$AudioRecord;->mActive:Z */
/* .line 465 */
/* iget v0, p0, Lcom/miui/server/smartpower/AppAudioResource$AudioRecord;->mCurrentBehavier:I */
/* iput v0, p0, Lcom/miui/server/smartpower/AppAudioResource$AudioRecord;->mBehavier:I */
/* .line 466 */
v1 = this.this$0;
/* iget v2, p0, Lcom/miui/server/smartpower/AppAudioResource$AudioRecord;->mOwnerUid:I */
com.miui.server.smartpower.AppAudioResource .-$$Nest$mupdateUidStatus ( v1,v2,v0 );
/* .line 467 */
v0 = this.this$0;
/* iget v1, p0, Lcom/miui/server/smartpower/AppAudioResource$AudioRecord;->mOwnerUid:I */
/* iget v2, p0, Lcom/miui/server/smartpower/AppAudioResource$AudioRecord;->mOwnerPid:I */
/* iget-boolean v3, p0, Lcom/miui/server/smartpower/AppAudioResource$AudioRecord;->mActive:Z */
/* iget v4, p0, Lcom/miui/server/smartpower/AppAudioResource$AudioRecord;->mBehavier:I */
(( com.miui.server.smartpower.AppAudioResource ) v0 ).reportResourceStatus ( v1, v2, v3, v4 ); // invoke-virtual {v0, v1, v2, v3, v4}, Lcom/miui/server/smartpower/AppAudioResource;->reportResourceStatus(IIZI)V
/* .line 468 */
/* sget-boolean v0, Lcom/miui/server/smartpower/AppPowerResource;->DEBUG:Z */
if ( v0 != null) { // if-eqz v0, :cond_1
/* .line 469 */
/* new-instance v0, Ljava/lang/StringBuilder; */
/* invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V */
/* iget v1, p0, Lcom/miui/server/smartpower/AppAudioResource$AudioRecord;->mOwnerUid:I */
(( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;
final String v1 = " "; // const-string v1, " "
(( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
/* iget v2, p0, Lcom/miui/server/smartpower/AppAudioResource$AudioRecord;->mOwnerPid:I */
(( java.lang.StringBuilder ) v0 ).append ( v2 ); // invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;
final String v2 = " audio active "; // const-string v2, " audio active "
(( java.lang.StringBuilder ) v0 ).append ( v2 ); // invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
/* iget-boolean v2, p0, Lcom/miui/server/smartpower/AppAudioResource$AudioRecord;->mActive:Z */
(( java.lang.StringBuilder ) v0 ).append ( v2 ); // invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
/* iget v1, p0, Lcom/miui/server/smartpower/AppAudioResource$AudioRecord;->mBehavier:I */
(( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v0 ).toString ( ); // invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
final String v1 = "SmartPower.AppResource"; // const-string v1, "SmartPower.AppResource"
android.util.Slog .d ( v1,v0 );
/* .line 472 */
} // :cond_1
/* new-instance v0, Ljava/lang/StringBuilder; */
/* invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V */
final String v1 = "audio u:"; // const-string v1, "audio u:"
(( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
/* iget v1, p0, Lcom/miui/server/smartpower/AppAudioResource$AudioRecord;->mOwnerUid:I */
(( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;
final String v1 = " p:"; // const-string v1, " p:"
(( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
/* iget v1, p0, Lcom/miui/server/smartpower/AppAudioResource$AudioRecord;->mOwnerPid:I */
(( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;
final String v1 = " s:"; // const-string v1, " s:"
(( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
/* iget-boolean v1, p0, Lcom/miui/server/smartpower/AppAudioResource$AudioRecord;->mActive:Z */
(( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v0 ).toString ( ); // invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
/* const v1, 0x15ff2 */
android.util.EventLog .writeEvent ( v1,v0 );
/* .line 475 */
} // :cond_2
return;
} // .end method
private void sendUpdateAudioStatusMsg ( ) {
/* .locals 2 */
/* .line 362 */
v0 = this.this$0;
com.miui.server.smartpower.AppAudioResource .-$$Nest$fgetmHandler ( v0 );
/* new-instance v1, Lcom/miui/server/smartpower/AppAudioResource$AudioRecord$1; */
/* invoke-direct {v1, p0}, Lcom/miui/server/smartpower/AppAudioResource$AudioRecord$1;-><init>(Lcom/miui/server/smartpower/AppAudioResource$AudioRecord;)V */
(( android.os.Handler ) v0 ).post ( v1 ); // invoke-virtual {v0, v1}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z
/* .line 368 */
return;
} // .end method
private void updateAudioStatus ( ) {
/* .locals 13 */
/* .line 371 */
int v0 = 0; // const/4 v0, 0x0
/* .line 372 */
/* .local v0, "active":Z */
int v1 = 0; // const/4 v1, 0x0
/* .line 373 */
/* .local v1, "behavier":I */
int v2 = 0; // const/4 v2, 0x0
/* .line 374 */
/* .local v2, "playing":Z */
int v3 = 0; // const/4 v3, 0x0
/* .line 375 */
/* .local v3, "inactiveDelay":Z */
android.os.SystemClock .uptimeMillis ( );
/* move-result-wide v4 */
/* .line 376 */
/* .local v4, "now":J */
int v6 = 0; // const/4 v6, 0x0
/* .line 378 */
/* .local v6, "recentActive":Z */
v7 = this.mPlayerRecords;
/* monitor-enter v7 */
/* .line 379 */
try { // :try_start_0
v8 = this.mPlayerRecords;
v8 = (( android.util.ArrayMap ) v8 ).size ( ); // invoke-virtual {v8}, Landroid/util/ArrayMap;->size()I
/* if-lez v8, :cond_0 */
/* .line 380 */
/* or-int/lit8 v1, v1, 0x4 */
/* .line 382 */
} // :cond_0
v8 = this.mPlayerRecords;
(( android.util.ArrayMap ) v8 ).values ( ); // invoke-virtual {v8}, Landroid/util/ArrayMap;->values()Ljava/util/Collection;
v9 = } // :goto_0
int v10 = 2; // const/4 v10, 0x2
if ( v9 != null) { // if-eqz v9, :cond_3
/* check-cast v9, Lcom/miui/server/smartpower/AppAudioResource$AudioRecord$PlayerRecord; */
/* .line 383 */
/* .local v9, "player":Lcom/miui/server/smartpower/AppAudioResource$AudioRecord$PlayerRecord; */
/* iget v11, v9, Lcom/miui/server/smartpower/AppAudioResource$AudioRecord$PlayerRecord;->mEvent:I */
int v12 = 5; // const/4 v12, 0x5
/* if-eq v11, v12, :cond_2 */
/* iget v11, v9, Lcom/miui/server/smartpower/AppAudioResource$AudioRecord$PlayerRecord;->mEvent:I */
/* if-ne v11, v10, :cond_1 */
/* .line 389 */
} // .end local v9 # "player":Lcom/miui/server/smartpower/AppAudioResource$AudioRecord$PlayerRecord;
} // :cond_1
/* .line 385 */
/* .restart local v9 # "player":Lcom/miui/server/smartpower/AppAudioResource$AudioRecord$PlayerRecord; */
} // :cond_2
} // :goto_1
int v0 = 1; // const/4 v0, 0x1
/* .line 386 */
int v2 = 1; // const/4 v2, 0x1
/* .line 387 */
/* nop */
/* .line 390 */
} // .end local v9 # "player":Lcom/miui/server/smartpower/AppAudioResource$AudioRecord$PlayerRecord;
} // :cond_3
/* if-nez v2, :cond_4 */
/* iget-boolean v8, p0, Lcom/miui/server/smartpower/AppAudioResource$AudioRecord;->mCurrentPlaying:Z */
if ( v8 != null) { // if-eqz v8, :cond_4
/* .line 391 */
android.os.SystemClock .uptimeMillis ( );
/* move-result-wide v8 */
/* iput-wide v8, p0, Lcom/miui/server/smartpower/AppAudioResource$AudioRecord;->mLastPlayingTimeStamp:J */
/* .line 393 */
} // :cond_4
/* iput-boolean v2, p0, Lcom/miui/server/smartpower/AppAudioResource$AudioRecord;->mCurrentPlaying:Z */
/* .line 394 */
/* iget-wide v8, p0, Lcom/miui/server/smartpower/AppAudioResource$AudioRecord;->mLastPlayingTimeStamp:J */
/* sub-long v8, v4, v8 */
/* const-wide/16 v11, 0x3e8 */
/* cmp-long v8, v8, v11 */
/* if-gez v8, :cond_5 */
int v8 = 1; // const/4 v8, 0x1
} // :cond_5
int v8 = 0; // const/4 v8, 0x0
} // :goto_2
/* move v6, v8 */
/* .line 395 */
/* monitor-exit v7 */
/* :try_end_0 */
/* .catchall {:try_start_0 ..:try_end_0} :catchall_2 */
/* .line 396 */
/* iget v7, p0, Lcom/miui/server/smartpower/AppAudioResource$AudioRecord;->mPlaybackState:I */
/* if-ltz v7, :cond_8 */
/* .line 397 */
/* or-int/lit8 v1, v1, 0x4 */
/* .line 398 */
/* or-int/2addr v1, v10 */
/* .line 399 */
/* if-eq v7, v10, :cond_7 */
/* if-nez v2, :cond_6 */
if ( v6 != null) { // if-eqz v6, :cond_7
/* .line 400 */
} // :cond_6
int v0 = 1; // const/4 v0, 0x1
/* move v8, v3 */
/* .line 405 */
} // :cond_7
/* move v8, v3 */
/* .line 403 */
} // :cond_8
/* xor-int/lit8 v7, v0, 0x1 */
/* move v3, v7 */
/* move v8, v3 */
/* .line 405 */
} // .end local v3 # "inactiveDelay":Z
/* .local v8, "inactiveDelay":Z */
} // :goto_3
/* if-nez v0, :cond_b */
/* .line 406 */
v3 = this.mFocusedClientIds;
/* monitor-enter v3 */
/* .line 407 */
try { // :try_start_1
v7 = this.mFocusedClientIds;
v7 = (( android.util.ArrayMap ) v7 ).size ( ); // invoke-virtual {v7}, Landroid/util/ArrayMap;->size()I
/* if-lez v7, :cond_a */
/* .line 408 */
v7 = this.mFocusedClientIds;
(( android.util.ArrayMap ) v7 ).values ( ); // invoke-virtual {v7}, Landroid/util/ArrayMap;->values()Ljava/util/Collection;
v9 = } // :goto_4
if ( v9 != null) { // if-eqz v9, :cond_a
/* check-cast v9, Ljava/lang/Integer; */
v9 = (( java.lang.Integer ) v9 ).intValue ( ); // invoke-virtual {v9}, Ljava/lang/Integer;->intValue()I
/* .line 409 */
/* .local v9, "focusLoss":I */
int v11 = -2; // const/4 v11, -0x2
/* if-ne v9, v11, :cond_9 */
/* .line 410 */
int v0 = 1; // const/4 v0, 0x1
/* .line 411 */
/* .line 413 */
} // .end local v9 # "focusLoss":I
} // :cond_9
/* .line 415 */
} // :cond_a
} // :goto_5
/* monitor-exit v3 */
/* :catchall_0 */
/* move-exception v7 */
/* monitor-exit v3 */
/* :try_end_1 */
/* .catchall {:try_start_1 ..:try_end_1} :catchall_0 */
/* throw v7 */
/* .line 418 */
} // :cond_b
} // :goto_6
v9 = this.mRecorderRecords;
/* monitor-enter v9 */
/* .line 419 */
try { // :try_start_2
v3 = this.mRecorderRecords;
v3 = (( android.util.ArrayMap ) v3 ).size ( ); // invoke-virtual {v3}, Landroid/util/ArrayMap;->size()I
/* if-lez v3, :cond_c */
/* .line 420 */
/* or-int/lit8 v1, v1, 0x8 */
/* .line 422 */
} // :cond_c
v3 = this.mRecorderRecords;
(( android.util.ArrayMap ) v3 ).values ( ); // invoke-virtual {v3}, Landroid/util/ArrayMap;->values()Ljava/util/Collection;
v7 = } // :goto_7
if ( v7 != null) { // if-eqz v7, :cond_e
/* check-cast v7, Lcom/miui/server/smartpower/AppAudioResource$AudioRecord$RecorderRecord; */
/* .line 423 */
/* .local v7, "recorder":Lcom/miui/server/smartpower/AppAudioResource$AudioRecord$RecorderRecord; */
/* iget v11, v7, Lcom/miui/server/smartpower/AppAudioResource$AudioRecord$RecorderRecord;->mEvent:I */
/* if-nez v11, :cond_d */
/* .line 424 */
int v0 = 1; // const/4 v0, 0x1
/* .line 426 */
} // .end local v7 # "recorder":Lcom/miui/server/smartpower/AppAudioResource$AudioRecord$RecorderRecord;
} // :cond_d
/* .line 427 */
} // :cond_e
/* monitor-exit v9 */
/* :try_end_2 */
/* .catchall {:try_start_2 ..:try_end_2} :catchall_1 */
/* .line 429 */
/* and-int/lit8 v3, v1, 0x4 */
if ( v3 != null) { // if-eqz v3, :cond_10
/* and-int/lit8 v3, v1, 0x8 */
if ( v3 != null) { // if-eqz v3, :cond_10
/* .line 431 */
v3 = this.this$0;
com.miui.server.smartpower.AppAudioResource .-$$Nest$fgetmAudioManager ( v3 );
v3 = (( android.media.AudioManager ) v3 ).getMode ( ); // invoke-virtual {v3}, Landroid/media/AudioManager;->getMode()I
/* .line 432 */
/* .local v3, "mode":I */
/* if-eq v3, v10, :cond_f */
int v7 = 3; // const/4 v7, 0x3
/* if-ne v3, v7, :cond_10 */
/* .line 434 */
} // :cond_f
/* or-int/lit8 v1, v1, 0x10 */
/* .line 438 */
} // .end local v3 # "mode":I
} // :cond_10
/* iget-boolean v3, p0, Lcom/miui/server/smartpower/AppAudioResource$AudioRecord;->mCurrentActive:Z */
/* if-ne v0, v3, :cond_11 */
/* iget v3, p0, Lcom/miui/server/smartpower/AppAudioResource$AudioRecord;->mCurrentBehavier:I */
/* if-eq v1, v3, :cond_14 */
/* .line 439 */
} // :cond_11
/* iput-boolean v0, p0, Lcom/miui/server/smartpower/AppAudioResource$AudioRecord;->mCurrentActive:Z */
/* .line 440 */
/* iput v1, p0, Lcom/miui/server/smartpower/AppAudioResource$AudioRecord;->mCurrentBehavier:I */
/* .line 441 */
if ( v8 != null) { // if-eqz v8, :cond_12
/* .line 442 */
v3 = this.mInactiveDelayTask;
/* if-nez v3, :cond_14 */
/* .line 443 */
/* new-instance v3, Lcom/miui/server/smartpower/AppAudioResource$AudioRecord$2; */
/* invoke-direct {v3, p0}, Lcom/miui/server/smartpower/AppAudioResource$AudioRecord$2;-><init>(Lcom/miui/server/smartpower/AppAudioResource$AudioRecord;)V */
this.mInactiveDelayTask = v3;
/* .line 450 */
v3 = this.this$0;
com.miui.server.smartpower.AppAudioResource .-$$Nest$fgetmHandler ( v3 );
v7 = this.mInactiveDelayTask;
/* const-wide/16 v9, 0x7d0 */
(( android.os.Handler ) v3 ).postDelayed ( v7, v9, v10 ); // invoke-virtual {v3, v7, v9, v10}, Landroid/os/Handler;->postDelayed(Ljava/lang/Runnable;J)Z
/* .line 453 */
} // :cond_12
v3 = this.mInactiveDelayTask;
if ( v3 != null) { // if-eqz v3, :cond_13
/* .line 454 */
v3 = this.this$0;
com.miui.server.smartpower.AppAudioResource .-$$Nest$fgetmHandler ( v3 );
v7 = this.mInactiveDelayTask;
(( android.os.Handler ) v3 ).removeCallbacks ( v7 ); // invoke-virtual {v3, v7}, Landroid/os/Handler;->removeCallbacks(Ljava/lang/Runnable;)V
/* .line 455 */
int v3 = 0; // const/4 v3, 0x0
this.mInactiveDelayTask = v3;
/* .line 457 */
} // :cond_13
/* invoke-direct {p0}, Lcom/miui/server/smartpower/AppAudioResource$AudioRecord;->realUpdateAudioStatus()V */
/* .line 460 */
} // :cond_14
} // :goto_8
return;
/* .line 427 */
/* :catchall_1 */
/* move-exception v3 */
try { // :try_start_3
/* monitor-exit v9 */
/* :try_end_3 */
/* .catchall {:try_start_3 ..:try_end_3} :catchall_1 */
/* throw v3 */
/* .line 395 */
} // .end local v8 # "inactiveDelay":Z
/* .local v3, "inactiveDelay":Z */
/* :catchall_2 */
/* move-exception v8 */
try { // :try_start_4
/* monitor-exit v7 */
/* :try_end_4 */
/* .catchall {:try_start_4 ..:try_end_4} :catchall_2 */
/* throw v8 */
} // .end method
/* # virtual methods */
Boolean containsClientId ( java.lang.String p0 ) {
/* .locals 2 */
/* .param p1, "clientId" # Ljava/lang/String; */
/* .line 250 */
v0 = this.mFocusedClientIds;
/* monitor-enter v0 */
/* .line 251 */
try { // :try_start_0
v1 = this.mFocusedClientIds;
v1 = (( android.util.ArrayMap ) v1 ).containsKey ( p1 ); // invoke-virtual {v1, p1}, Landroid/util/ArrayMap;->containsKey(Ljava/lang/Object;)Z
/* monitor-exit v0 */
/* .line 252 */
/* :catchall_0 */
/* move-exception v1 */
/* monitor-exit v0 */
/* :try_end_0 */
/* .catchall {:try_start_0 ..:try_end_0} :catchall_0 */
/* throw v1 */
} // .end method
Boolean containsRecorder ( Integer p0 ) {
/* .locals 4 */
/* .param p1, "riid" # I */
/* .line 239 */
v0 = this.mRecorderRecords;
/* monitor-enter v0 */
/* .line 240 */
try { // :try_start_0
v1 = this.mRecorderRecords;
(( android.util.ArrayMap ) v1 ).values ( ); // invoke-virtual {v1}, Landroid/util/ArrayMap;->values()Ljava/util/Collection;
v2 = } // :goto_0
if ( v2 != null) { // if-eqz v2, :cond_1
/* check-cast v2, Lcom/miui/server/smartpower/AppAudioResource$AudioRecord$RecorderRecord; */
/* .line 241 */
/* .local v2, "recorder":Lcom/miui/server/smartpower/AppAudioResource$AudioRecord$RecorderRecord; */
/* iget v3, v2, Lcom/miui/server/smartpower/AppAudioResource$AudioRecord$RecorderRecord;->mRiid:I */
/* if-ne v3, p1, :cond_0 */
/* .line 242 */
/* monitor-exit v0 */
int v0 = 1; // const/4 v0, 0x1
/* .line 244 */
} // .end local v2 # "recorder":Lcom/miui/server/smartpower/AppAudioResource$AudioRecord$RecorderRecord;
} // :cond_0
/* .line 245 */
} // :cond_1
/* monitor-exit v0 */
/* .line 246 */
int v0 = 0; // const/4 v0, 0x0
/* .line 245 */
/* :catchall_0 */
/* move-exception v1 */
/* monitor-exit v0 */
/* :try_end_0 */
/* .catchall {:try_start_0 ..:try_end_0} :catchall_0 */
/* throw v1 */
} // .end method
void onPlayerEvent ( Integer p0, Integer p1 ) {
/* .locals 1 */
/* .param p1, "piid" # I */
/* .param p2, "event" # I */
/* .line 304 */
/* invoke-direct {p0, p1}, Lcom/miui/server/smartpower/AppAudioResource$AudioRecord;->getPlayerRecord(I)Lcom/miui/server/smartpower/AppAudioResource$AudioRecord$PlayerRecord; */
/* .line 305 */
/* .local v0, "record":Lcom/miui/server/smartpower/AppAudioResource$AudioRecord$PlayerRecord; */
if ( v0 != null) { // if-eqz v0, :cond_0
/* .line 306 */
/* iput p2, v0, Lcom/miui/server/smartpower/AppAudioResource$AudioRecord$PlayerRecord;->mEvent:I */
/* .line 307 */
/* invoke-direct {p0}, Lcom/miui/server/smartpower/AppAudioResource$AudioRecord;->sendUpdateAudioStatusMsg()V */
/* .line 309 */
} // :cond_0
return;
} // .end method
void onPlayerRlease ( Integer p0 ) {
/* .locals 3 */
/* .param p1, "piid" # I */
/* .line 297 */
v0 = this.mPlayerRecords;
/* monitor-enter v0 */
/* .line 298 */
try { // :try_start_0
v1 = this.mPlayerRecords;
java.lang.Integer .valueOf ( p1 );
(( android.util.ArrayMap ) v1 ).remove ( v2 ); // invoke-virtual {v1, v2}, Landroid/util/ArrayMap;->remove(Ljava/lang/Object;)Ljava/lang/Object;
/* .line 299 */
/* monitor-exit v0 */
/* :try_end_0 */
/* .catchall {:try_start_0 ..:try_end_0} :catchall_0 */
/* .line 300 */
/* invoke-direct {p0}, Lcom/miui/server/smartpower/AppAudioResource$AudioRecord;->sendUpdateAudioStatusMsg()V */
/* .line 301 */
return;
/* .line 299 */
/* :catchall_0 */
/* move-exception v1 */
try { // :try_start_1
/* monitor-exit v0 */
/* :try_end_1 */
/* .catchall {:try_start_1 ..:try_end_1} :catchall_0 */
/* throw v1 */
} // .end method
void onPlayerTrack ( Integer p0, Integer p1 ) {
/* .locals 4 */
/* .param p1, "piid" # I */
/* .param p2, "sessionId" # I */
/* .line 287 */
v0 = this.mPlayerRecords;
/* monitor-enter v0 */
/* .line 288 */
try { // :try_start_0
v1 = this.mPlayerRecords;
java.lang.Integer .valueOf ( p1 );
v1 = (( android.util.ArrayMap ) v1 ).containsKey ( v2 ); // invoke-virtual {v1, v2}, Landroid/util/ArrayMap;->containsKey(Ljava/lang/Object;)Z
/* if-nez v1, :cond_0 */
/* .line 289 */
/* new-instance v1, Lcom/miui/server/smartpower/AppAudioResource$AudioRecord$PlayerRecord; */
/* invoke-direct {v1, p0, p1, p2}, Lcom/miui/server/smartpower/AppAudioResource$AudioRecord$PlayerRecord;-><init>(Lcom/miui/server/smartpower/AppAudioResource$AudioRecord;II)V */
/* .line 290 */
/* .local v1, "record":Lcom/miui/server/smartpower/AppAudioResource$AudioRecord$PlayerRecord; */
v2 = this.mPlayerRecords;
java.lang.Integer .valueOf ( p1 );
(( android.util.ArrayMap ) v2 ).put ( v3, v1 ); // invoke-virtual {v2, v3, v1}, Landroid/util/ArrayMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;
/* .line 291 */
/* invoke-direct {p0}, Lcom/miui/server/smartpower/AppAudioResource$AudioRecord;->sendUpdateAudioStatusMsg()V */
/* .line 293 */
} // .end local v1 # "record":Lcom/miui/server/smartpower/AppAudioResource$AudioRecord$PlayerRecord;
} // :cond_0
/* monitor-exit v0 */
/* .line 294 */
return;
/* .line 293 */
/* :catchall_0 */
/* move-exception v1 */
/* monitor-exit v0 */
/* :try_end_0 */
/* .catchall {:try_start_0 ..:try_end_0} :catchall_0 */
/* throw v1 */
} // .end method
void onRecorderEvent ( Integer p0, Integer p1 ) {
/* .locals 1 */
/* .param p1, "riid" # I */
/* .param p2, "event" # I */
/* .line 340 */
/* invoke-direct {p0, p1}, Lcom/miui/server/smartpower/AppAudioResource$AudioRecord;->getRecorderRecord(I)Lcom/miui/server/smartpower/AppAudioResource$AudioRecord$RecorderRecord; */
/* .line 341 */
/* .local v0, "record":Lcom/miui/server/smartpower/AppAudioResource$AudioRecord$RecorderRecord; */
if ( v0 != null) { // if-eqz v0, :cond_0
/* .line 342 */
/* iput p2, v0, Lcom/miui/server/smartpower/AppAudioResource$AudioRecord$RecorderRecord;->mEvent:I */
/* .line 343 */
/* invoke-direct {p0}, Lcom/miui/server/smartpower/AppAudioResource$AudioRecord;->sendUpdateAudioStatusMsg()V */
/* .line 345 */
} // :cond_0
return;
} // .end method
void onRecorderRlease ( Integer p0 ) {
/* .locals 3 */
/* .param p1, "riid" # I */
/* .line 334 */
v0 = this.mRecorderRecords;
/* monitor-enter v0 */
/* .line 335 */
try { // :try_start_0
v1 = this.mRecorderRecords;
java.lang.Integer .valueOf ( p1 );
(( android.util.ArrayMap ) v1 ).remove ( v2 ); // invoke-virtual {v1, v2}, Landroid/util/ArrayMap;->remove(Ljava/lang/Object;)Ljava/lang/Object;
/* .line 336 */
/* monitor-exit v0 */
/* .line 337 */
return;
/* .line 336 */
/* :catchall_0 */
/* move-exception v1 */
/* monitor-exit v0 */
/* :try_end_0 */
/* .catchall {:try_start_0 ..:try_end_0} :catchall_0 */
/* throw v1 */
} // .end method
void onRecorderTrack ( Integer p0 ) {
/* .locals 4 */
/* .param p1, "riid" # I */
/* .line 325 */
v0 = this.mRecorderRecords;
/* monitor-enter v0 */
/* .line 326 */
try { // :try_start_0
v1 = this.mRecorderRecords;
java.lang.Integer .valueOf ( p1 );
v1 = (( android.util.ArrayMap ) v1 ).containsKey ( v2 ); // invoke-virtual {v1, v2}, Landroid/util/ArrayMap;->containsKey(Ljava/lang/Object;)Z
/* if-nez v1, :cond_0 */
/* .line 327 */
/* new-instance v1, Lcom/miui/server/smartpower/AppAudioResource$AudioRecord$RecorderRecord; */
/* invoke-direct {v1, p0, p1}, Lcom/miui/server/smartpower/AppAudioResource$AudioRecord$RecorderRecord;-><init>(Lcom/miui/server/smartpower/AppAudioResource$AudioRecord;I)V */
/* .line 328 */
/* .local v1, "record":Lcom/miui/server/smartpower/AppAudioResource$AudioRecord$RecorderRecord; */
v2 = this.mRecorderRecords;
java.lang.Integer .valueOf ( p1 );
(( android.util.ArrayMap ) v2 ).put ( v3, v1 ); // invoke-virtual {v2, v3, v1}, Landroid/util/ArrayMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;
/* .line 330 */
} // .end local v1 # "record":Lcom/miui/server/smartpower/AppAudioResource$AudioRecord$RecorderRecord;
} // :cond_0
/* monitor-exit v0 */
/* .line 331 */
return;
/* .line 330 */
/* :catchall_0 */
/* move-exception v1 */
/* monitor-exit v0 */
/* :try_end_0 */
/* .catchall {:try_start_0 ..:try_end_0} :catchall_0 */
/* throw v1 */
} // .end method
void playbackStateChanged ( Integer p0, Integer p1 ) {
/* .locals 3 */
/* .param p1, "oldState" # I */
/* .param p2, "newState" # I */
/* .line 256 */
/* iget v0, p0, Lcom/miui/server/smartpower/AppAudioResource$AudioRecord;->mPlaybackState:I */
/* if-eq v0, p2, :cond_0 */
/* .line 257 */
/* iput p2, p0, Lcom/miui/server/smartpower/AppAudioResource$AudioRecord;->mPlaybackState:I */
/* .line 258 */
/* invoke-direct {p0}, Lcom/miui/server/smartpower/AppAudioResource$AudioRecord;->sendUpdateAudioStatusMsg()V */
/* .line 259 */
int v0 = 2; // const/4 v0, 0x2
/* if-ne p2, v0, :cond_0 */
/* .line 260 */
v0 = this.this$0;
/* iget v1, p0, Lcom/miui/server/smartpower/AppAudioResource$AudioRecord;->mOwnerPid:I */
/* int-to-long v1, v1 */
com.miui.server.smartpower.AppAudioResource .-$$Nest$fputmLastMusicPlayPid ( v0,v1,v2 );
/* .line 261 */
v0 = this.this$0;
android.os.SystemClock .uptimeMillis ( );
/* move-result-wide v1 */
com.miui.server.smartpower.AppAudioResource .-$$Nest$fputmLastMusicPlayTimeStamp ( v0,v1,v2 );
/* .line 264 */
} // :cond_0
return;
} // .end method
void recordAudioFocus ( Boolean p0, java.lang.String p1 ) {
/* .locals 3 */
/* .param p1, "request" # Z */
/* .param p2, "clientId" # Ljava/lang/String; */
/* .line 267 */
v0 = this.mFocusedClientIds;
/* monitor-enter v0 */
/* .line 268 */
if ( p1 != null) { // if-eqz p1, :cond_0
/* .line 269 */
try { // :try_start_0
v1 = this.mFocusedClientIds;
int v2 = 0; // const/4 v2, 0x0
java.lang.Integer .valueOf ( v2 );
(( android.util.ArrayMap ) v1 ).put ( p2, v2 ); // invoke-virtual {v1, p2, v2}, Landroid/util/ArrayMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;
/* .line 271 */
} // :cond_0
v1 = this.mFocusedClientIds;
(( android.util.ArrayMap ) v1 ).remove ( p2 ); // invoke-virtual {v1, p2}, Landroid/util/ArrayMap;->remove(Ljava/lang/Object;)Ljava/lang/Object;
/* .line 273 */
} // :goto_0
v1 = this.mFocusedClientIds;
v1 = (( android.util.ArrayMap ) v1 ).size ( ); // invoke-virtual {v1}, Landroid/util/ArrayMap;->size()I
/* if-lez v1, :cond_1 */
/* .line 274 */
/* monitor-exit v0 */
return;
/* .line 276 */
} // :cond_1
/* monitor-exit v0 */
/* :try_end_0 */
/* .catchall {:try_start_0 ..:try_end_0} :catchall_0 */
/* .line 277 */
/* invoke-direct {p0}, Lcom/miui/server/smartpower/AppAudioResource$AudioRecord;->sendUpdateAudioStatusMsg()V */
/* .line 278 */
return;
/* .line 276 */
/* :catchall_0 */
/* move-exception v1 */
try { // :try_start_1
/* monitor-exit v0 */
/* :try_end_1 */
/* .catchall {:try_start_1 ..:try_end_1} :catchall_0 */
/* throw v1 */
} // .end method
void recordAudioFocusLoss ( java.lang.String p0, Integer p1 ) {
/* .locals 3 */
/* .param p1, "clientId" # Ljava/lang/String; */
/* .param p2, "focusLoss" # I */
/* .line 281 */
v0 = this.mFocusedClientIds;
/* monitor-enter v0 */
/* .line 282 */
try { // :try_start_0
v1 = this.mFocusedClientIds;
java.lang.Integer .valueOf ( p2 );
(( android.util.ArrayMap ) v1 ).put ( p1, v2 ); // invoke-virtual {v1, p1, v2}, Landroid/util/ArrayMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;
/* .line 283 */
/* monitor-exit v0 */
/* .line 284 */
return;
/* .line 283 */
/* :catchall_0 */
/* move-exception v1 */
/* monitor-exit v0 */
/* :try_end_0 */
/* .catchall {:try_start_0 ..:try_end_0} :catchall_0 */
/* throw v1 */
} // .end method
void reportTrackStatus ( Integer p0, Boolean p1 ) {
/* .locals 4 */
/* .param p1, "sessionId" # I */
/* .param p2, "isMuted" # Z */
/* .line 312 */
v0 = this.mPlayerRecords;
/* monitor-enter v0 */
/* .line 313 */
try { // :try_start_0
v1 = this.mPlayerRecords;
(( android.util.ArrayMap ) v1 ).values ( ); // invoke-virtual {v1}, Landroid/util/ArrayMap;->values()Ljava/util/Collection;
v2 = } // :goto_0
if ( v2 != null) { // if-eqz v2, :cond_2
/* check-cast v2, Lcom/miui/server/smartpower/AppAudioResource$AudioRecord$PlayerRecord; */
/* .line 314 */
/* .local v2, "r":Lcom/miui/server/smartpower/AppAudioResource$AudioRecord$PlayerRecord; */
/* iget v3, v2, Lcom/miui/server/smartpower/AppAudioResource$AudioRecord$PlayerRecord;->mSessionId:I */
/* if-ne p1, v3, :cond_1 */
/* .line 315 */
if ( p2 != null) { // if-eqz p2, :cond_0
/* const/16 v1, 0x64 */
/* .line 316 */
} // :cond_0
int v1 = 5; // const/4 v1, 0x5
} // :goto_1
/* iput v1, v2, Lcom/miui/server/smartpower/AppAudioResource$AudioRecord$PlayerRecord;->mEvent:I */
/* .line 317 */
/* invoke-direct {p0}, Lcom/miui/server/smartpower/AppAudioResource$AudioRecord;->sendUpdateAudioStatusMsg()V */
/* .line 318 */
/* monitor-exit v0 */
return;
/* .line 320 */
} // .end local v2 # "r":Lcom/miui/server/smartpower/AppAudioResource$AudioRecord$PlayerRecord;
} // :cond_1
/* .line 321 */
} // :cond_2
/* monitor-exit v0 */
/* .line 322 */
return;
/* .line 321 */
/* :catchall_0 */
/* move-exception v1 */
/* monitor-exit v0 */
/* :try_end_0 */
/* .catchall {:try_start_0 ..:try_end_0} :catchall_0 */
/* throw v1 */
} // .end method
void uidAudioStatusChanged ( Boolean p0 ) {
/* .locals 5 */
/* .param p1, "active" # Z */
/* .line 348 */
/* if-nez p1, :cond_3 */
/* .line 349 */
v0 = this.mPlayerRecords;
/* monitor-enter v0 */
/* .line 350 */
try { // :try_start_0
v1 = this.mPlayerRecords;
(( android.util.ArrayMap ) v1 ).values ( ); // invoke-virtual {v1}, Landroid/util/ArrayMap;->values()Ljava/util/Collection;
v2 = } // :goto_0
if ( v2 != null) { // if-eqz v2, :cond_2
/* check-cast v2, Lcom/miui/server/smartpower/AppAudioResource$AudioRecord$PlayerRecord; */
/* .line 351 */
/* .local v2, "player":Lcom/miui/server/smartpower/AppAudioResource$AudioRecord$PlayerRecord; */
/* iget v3, v2, Lcom/miui/server/smartpower/AppAudioResource$AudioRecord$PlayerRecord;->mEvent:I */
int v4 = 5; // const/4 v4, 0x5
/* if-eq v3, v4, :cond_0 */
/* iget v3, v2, Lcom/miui/server/smartpower/AppAudioResource$AudioRecord$PlayerRecord;->mEvent:I */
int v4 = 2; // const/4 v4, 0x2
/* if-ne v3, v4, :cond_1 */
/* .line 353 */
} // :cond_0
int v3 = 3; // const/4 v3, 0x3
/* iput v3, v2, Lcom/miui/server/smartpower/AppAudioResource$AudioRecord$PlayerRecord;->mEvent:I */
/* .line 355 */
} // .end local v2 # "player":Lcom/miui/server/smartpower/AppAudioResource$AudioRecord$PlayerRecord;
} // :cond_1
/* .line 356 */
} // :cond_2
/* monitor-exit v0 */
/* :try_end_0 */
/* .catchall {:try_start_0 ..:try_end_0} :catchall_0 */
/* .line 357 */
/* invoke-direct {p0}, Lcom/miui/server/smartpower/AppAudioResource$AudioRecord;->sendUpdateAudioStatusMsg()V */
/* .line 356 */
/* :catchall_0 */
/* move-exception v1 */
try { // :try_start_1
/* monitor-exit v0 */
/* :try_end_1 */
/* .catchall {:try_start_1 ..:try_end_1} :catchall_0 */
/* throw v1 */
/* .line 359 */
} // :cond_3
} // :goto_1
return;
} // .end method
