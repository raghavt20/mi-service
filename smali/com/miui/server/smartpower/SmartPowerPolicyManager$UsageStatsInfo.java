public class com.miui.server.smartpower.SmartPowerPolicyManager$UsageStatsInfo {
	 /* .source "SmartPowerPolicyManager.java" */
	 /* # annotations */
	 /* .annotation system Ldalvik/annotation/EnclosingClass; */
	 /* value = Lcom/miui/server/smartpower/SmartPowerPolicyManager; */
} // .end annotation
/* .annotation system Ldalvik/annotation/InnerClass; */
/* accessFlags = 0x1 */
/* name = "UsageStatsInfo" */
} // .end annotation
/* # instance fields */
private Integer mAppLaunchCount;
private Long mBeginTimeStamp;
private Long mDurationHours;
private Long mEndTimeStamp;
private Long mLastTimeVisible;
private java.lang.String mPackageName;
private Long mTotalTimeInForeground;
private Long mTotalTimeVisible;
final com.miui.server.smartpower.SmartPowerPolicyManager this$0; //synthetic
/* # direct methods */
static Integer -$$Nest$fgetmAppLaunchCount ( com.miui.server.smartpower.SmartPowerPolicyManager$UsageStatsInfo p0 ) { //bridge//synthethic
/* .locals 0 */
/* iget p0, p0, Lcom/miui/server/smartpower/SmartPowerPolicyManager$UsageStatsInfo;->mAppLaunchCount:I */
} // .end method
 com.miui.server.smartpower.SmartPowerPolicyManager$UsageStatsInfo ( ) {
/* .locals 3 */
/* .param p1, "this$0" # Lcom/miui/server/smartpower/SmartPowerPolicyManager; */
/* .param p2, "stats" # Landroid/app/usage/UsageStats; */
/* .line 1599 */
this.this$0 = p1;
/* invoke-direct {p0}, Ljava/lang/Object;-><init>()V */
/* .line 1587 */
/* const-wide/16 v0, 0x0 */
/* iput-wide v0, p0, Lcom/miui/server/smartpower/SmartPowerPolicyManager$UsageStatsInfo;->mTotalTimeInForeground:J */
/* .line 1588 */
/* iput-wide v0, p0, Lcom/miui/server/smartpower/SmartPowerPolicyManager$UsageStatsInfo;->mTotalTimeVisible:J */
/* .line 1589 */
/* iput-wide v0, p0, Lcom/miui/server/smartpower/SmartPowerPolicyManager$UsageStatsInfo;->mLastTimeVisible:J */
/* .line 1590 */
int v2 = 0; // const/4 v2, 0x0
/* iput v2, p0, Lcom/miui/server/smartpower/SmartPowerPolicyManager$UsageStatsInfo;->mAppLaunchCount:I */
/* .line 1591 */
/* iput-wide v0, p0, Lcom/miui/server/smartpower/SmartPowerPolicyManager$UsageStatsInfo;->mBeginTimeStamp:J */
/* .line 1592 */
/* iput-wide v0, p0, Lcom/miui/server/smartpower/SmartPowerPolicyManager$UsageStatsInfo;->mEndTimeStamp:J */
/* .line 1593 */
/* iput-wide v0, p0, Lcom/miui/server/smartpower/SmartPowerPolicyManager$UsageStatsInfo;->mDurationHours:J */
/* .line 1600 */
(( com.miui.server.smartpower.SmartPowerPolicyManager$UsageStatsInfo ) p0 ).updateStatsInfo ( p2 ); // invoke-virtual {p0, p2}, Lcom/miui/server/smartpower/SmartPowerPolicyManager$UsageStatsInfo;->updateStatsInfo(Landroid/app/usage/UsageStats;)V
/* .line 1601 */
return;
} // .end method
 com.miui.server.smartpower.SmartPowerPolicyManager$UsageStatsInfo ( ) {
/* .locals 3 */
/* .param p1, "this$0" # Lcom/miui/server/smartpower/SmartPowerPolicyManager; */
/* .param p2, "pakageName" # Ljava/lang/String; */
/* .line 1595 */
this.this$0 = p1;
/* invoke-direct {p0}, Ljava/lang/Object;-><init>()V */
/* .line 1587 */
/* const-wide/16 v0, 0x0 */
/* iput-wide v0, p0, Lcom/miui/server/smartpower/SmartPowerPolicyManager$UsageStatsInfo;->mTotalTimeInForeground:J */
/* .line 1588 */
/* iput-wide v0, p0, Lcom/miui/server/smartpower/SmartPowerPolicyManager$UsageStatsInfo;->mTotalTimeVisible:J */
/* .line 1589 */
/* iput-wide v0, p0, Lcom/miui/server/smartpower/SmartPowerPolicyManager$UsageStatsInfo;->mLastTimeVisible:J */
/* .line 1590 */
int v2 = 0; // const/4 v2, 0x0
/* iput v2, p0, Lcom/miui/server/smartpower/SmartPowerPolicyManager$UsageStatsInfo;->mAppLaunchCount:I */
/* .line 1591 */
/* iput-wide v0, p0, Lcom/miui/server/smartpower/SmartPowerPolicyManager$UsageStatsInfo;->mBeginTimeStamp:J */
/* .line 1592 */
/* iput-wide v0, p0, Lcom/miui/server/smartpower/SmartPowerPolicyManager$UsageStatsInfo;->mEndTimeStamp:J */
/* .line 1593 */
/* iput-wide v0, p0, Lcom/miui/server/smartpower/SmartPowerPolicyManager$UsageStatsInfo;->mDurationHours:J */
/* .line 1596 */
this.mPackageName = p2;
/* .line 1597 */
return;
} // .end method
/* # virtual methods */
public void dump ( java.io.PrintWriter p0, java.lang.String[] p1, Integer p2 ) {
/* .locals 3 */
/* .param p1, "pw" # Ljava/io/PrintWriter; */
/* .param p2, "args" # [Ljava/lang/String; */
/* .param p3, "opti" # I */
/* .line 1639 */
/* iget v0, p0, Lcom/miui/server/smartpower/SmartPowerPolicyManager$UsageStatsInfo;->mAppLaunchCount:I */
/* if-lez v0, :cond_0 */
/* .line 1640 */
/* new-instance v0, Ljava/lang/StringBuilder; */
/* invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V */
final String v1 = "#"; // const-string v1, "#"
(( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
v1 = this.mPackageName;
(( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
final String v1 = " dur:"; // const-string v1, " dur:"
(( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
/* iget-wide v1, p0, Lcom/miui/server/smartpower/SmartPowerPolicyManager$UsageStatsInfo;->mDurationHours:J */
(( java.lang.StringBuilder ) v0 ).append ( v1, v2 ); // invoke-virtual {v0, v1, v2}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;
final String v1 = "H vis="; // const-string v1, "H vis="
(( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
/* iget-wide v1, p0, Lcom/miui/server/smartpower/SmartPowerPolicyManager$UsageStatsInfo;->mTotalTimeVisible:J */
(( java.lang.StringBuilder ) v0 ).append ( v1, v2 ); // invoke-virtual {v0, v1, v2}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;
final String v1 = "ms top="; // const-string v1, "ms top="
(( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
/* iget-wide v1, p0, Lcom/miui/server/smartpower/SmartPowerPolicyManager$UsageStatsInfo;->mTotalTimeInForeground:J */
(( java.lang.StringBuilder ) v0 ).append ( v1, v2 ); // invoke-virtual {v0, v1, v2}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;
final String v1 = "ms launch="; // const-string v1, "ms launch="
(( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
/* iget v1, p0, Lcom/miui/server/smartpower/SmartPowerPolicyManager$UsageStatsInfo;->mAppLaunchCount:I */
(( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v0 ).toString ( ); // invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
(( java.io.PrintWriter ) p1 ).println ( v0 ); // invoke-virtual {p1, v0}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V
/* .line 1646 */
} // :cond_0
return;
} // .end method
public Integer getAppLaunchCount ( ) {
/* .locals 1 */
/* .line 1627 */
/* iget v0, p0, Lcom/miui/server/smartpower/SmartPowerPolicyManager$UsageStatsInfo;->mAppLaunchCount:I */
} // .end method
public Long getLastTimeVisible ( ) {
/* .locals 2 */
/* .line 1631 */
/* iget-wide v0, p0, Lcom/miui/server/smartpower/SmartPowerPolicyManager$UsageStatsInfo;->mLastTimeVisible:J */
/* return-wide v0 */
} // .end method
public Long getTotalTimeInForeground ( ) {
/* .locals 2 */
/* .line 1635 */
/* iget-wide v0, p0, Lcom/miui/server/smartpower/SmartPowerPolicyManager$UsageStatsInfo;->mTotalTimeInForeground:J */
/* return-wide v0 */
} // .end method
public void reset ( ) {
/* .locals 3 */
/* .line 1658 */
/* const-wide/16 v0, 0x0 */
/* iput-wide v0, p0, Lcom/miui/server/smartpower/SmartPowerPolicyManager$UsageStatsInfo;->mTotalTimeInForeground:J */
/* .line 1659 */
/* iput-wide v0, p0, Lcom/miui/server/smartpower/SmartPowerPolicyManager$UsageStatsInfo;->mTotalTimeVisible:J */
/* .line 1660 */
int v2 = 0; // const/4 v2, 0x0
/* iput v2, p0, Lcom/miui/server/smartpower/SmartPowerPolicyManager$UsageStatsInfo;->mAppLaunchCount:I */
/* .line 1661 */
/* iput-wide v0, p0, Lcom/miui/server/smartpower/SmartPowerPolicyManager$UsageStatsInfo;->mBeginTimeStamp:J */
/* .line 1662 */
/* iput-wide v0, p0, Lcom/miui/server/smartpower/SmartPowerPolicyManager$UsageStatsInfo;->mEndTimeStamp:J */
/* .line 1663 */
/* iput-wide v0, p0, Lcom/miui/server/smartpower/SmartPowerPolicyManager$UsageStatsInfo;->mDurationHours:J */
/* .line 1664 */
return;
} // .end method
public java.lang.String toString ( ) {
/* .locals 3 */
/* .line 1650 */
/* new-instance v0, Ljava/lang/StringBuilder; */
/* invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V */
v1 = this.mPackageName;
(( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
final String v1 = " dur:"; // const-string v1, " dur:"
(( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
/* iget-wide v1, p0, Lcom/miui/server/smartpower/SmartPowerPolicyManager$UsageStatsInfo;->mDurationHours:J */
(( java.lang.StringBuilder ) v0 ).append ( v1, v2 ); // invoke-virtual {v0, v1, v2}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;
final String v1 = "H vis="; // const-string v1, "H vis="
(( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
/* iget-wide v1, p0, Lcom/miui/server/smartpower/SmartPowerPolicyManager$UsageStatsInfo;->mTotalTimeVisible:J */
(( java.lang.StringBuilder ) v0 ).append ( v1, v2 ); // invoke-virtual {v0, v1, v2}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;
final String v1 = "ms top="; // const-string v1, "ms top="
(( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
/* iget-wide v1, p0, Lcom/miui/server/smartpower/SmartPowerPolicyManager$UsageStatsInfo;->mTotalTimeInForeground:J */
(( java.lang.StringBuilder ) v0 ).append ( v1, v2 ); // invoke-virtual {v0, v1, v2}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;
final String v1 = "ms launch="; // const-string v1, "ms launch="
(( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
/* iget v1, p0, Lcom/miui/server/smartpower/SmartPowerPolicyManager$UsageStatsInfo;->mAppLaunchCount:I */
(( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v0 ).toString ( ); // invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
} // .end method
void updateStatsInfo ( android.app.usage.UsageStats p0 ) {
/* .locals 6 */
/* .param p1, "stats" # Landroid/app/usage/UsageStats; */
/* .line 1604 */
/* const-wide/16 v0, 0x0 */
/* .line 1605 */
/* .local v0, "firstInstallTime":J */
v2 = this.mPackageName;
this.mPackageName = v2;
/* .line 1607 */
try { // :try_start_0
v2 = this.this$0;
com.miui.server.smartpower.SmartPowerPolicyManager .-$$Nest$fgetmContext ( v2 );
(( android.content.Context ) v2 ).getPackageManager ( ); // invoke-virtual {v2}, Landroid/content/Context;->getPackageManager()Landroid/content/pm/PackageManager;
v3 = this.mPackageName;
/* .line 1608 */
int v4 = 0; // const/4 v4, 0x0
(( android.content.pm.PackageManager ) v2 ).getPackageInfo ( v3, v4 ); // invoke-virtual {v2, v3, v4}, Landroid/content/pm/PackageManager;->getPackageInfo(Ljava/lang/String;I)Landroid/content/pm/PackageInfo;
/* iget-wide v2, v2, Landroid/content/pm/PackageInfo;->firstInstallTime:J */
/* :try_end_0 */
/* .catch Landroid/content/pm/PackageManager$NameNotFoundException; {:try_start_0 ..:try_end_0} :catch_0 */
/* move-wide v0, v2 */
/* .line 1609 */
/* :catch_0 */
/* move-exception v2 */
} // :goto_0
/* nop */
/* .line 1611 */
/* iget-wide v2, p0, Lcom/miui/server/smartpower/SmartPowerPolicyManager$UsageStatsInfo;->mTotalTimeInForeground:J */
/* iget-wide v4, p1, Landroid/app/usage/UsageStats;->mTotalTimeInForeground:J */
/* add-long/2addr v2, v4 */
/* iput-wide v2, p0, Lcom/miui/server/smartpower/SmartPowerPolicyManager$UsageStatsInfo;->mTotalTimeInForeground:J */
/* .line 1612 */
/* iget-wide v2, p0, Lcom/miui/server/smartpower/SmartPowerPolicyManager$UsageStatsInfo;->mTotalTimeVisible:J */
/* iget-wide v4, p1, Landroid/app/usage/UsageStats;->mTotalTimeVisible:J */
/* add-long/2addr v2, v4 */
/* iput-wide v2, p0, Lcom/miui/server/smartpower/SmartPowerPolicyManager$UsageStatsInfo;->mTotalTimeVisible:J */
/* .line 1613 */
/* iget v2, p0, Lcom/miui/server/smartpower/SmartPowerPolicyManager$UsageStatsInfo;->mAppLaunchCount:I */
/* iget v3, p1, Landroid/app/usage/UsageStats;->mAppLaunchCount:I */
/* add-int/2addr v2, v3 */
/* iput v2, p0, Lcom/miui/server/smartpower/SmartPowerPolicyManager$UsageStatsInfo;->mAppLaunchCount:I */
/* .line 1614 */
/* iget-wide v2, p0, Lcom/miui/server/smartpower/SmartPowerPolicyManager$UsageStatsInfo;->mBeginTimeStamp:J */
/* const-wide/16 v4, 0x0 */
/* cmp-long v2, v2, v4 */
/* if-nez v2, :cond_0 */
/* .line 1615 */
/* iget-wide v2, p1, Landroid/app/usage/UsageStats;->mBeginTimeStamp:J */
java.lang.Math .max ( v2,v3,v0,v1 );
/* move-result-wide v2 */
/* iput-wide v2, p0, Lcom/miui/server/smartpower/SmartPowerPolicyManager$UsageStatsInfo;->mBeginTimeStamp:J */
/* .line 1617 */
} // :cond_0
/* iget-wide v2, p1, Landroid/app/usage/UsageStats;->mBeginTimeStamp:J */
java.lang.Math .max ( v2,v3,v0,v1 );
/* move-result-wide v2 */
/* .line 1618 */
/* .local v2, "beginTimeStamp":J */
/* iget-wide v4, p0, Lcom/miui/server/smartpower/SmartPowerPolicyManager$UsageStatsInfo;->mBeginTimeStamp:J */
java.lang.Math .min ( v2,v3,v4,v5 );
/* move-result-wide v4 */
/* iput-wide v4, p0, Lcom/miui/server/smartpower/SmartPowerPolicyManager$UsageStatsInfo;->mBeginTimeStamp:J */
/* .line 1621 */
} // .end local v2 # "beginTimeStamp":J
} // :goto_1
/* iget-wide v2, p1, Landroid/app/usage/UsageStats;->mEndTimeStamp:J */
/* iget-wide v4, p0, Lcom/miui/server/smartpower/SmartPowerPolicyManager$UsageStatsInfo;->mEndTimeStamp:J */
java.lang.Math .max ( v2,v3,v4,v5 );
/* move-result-wide v2 */
/* iput-wide v2, p0, Lcom/miui/server/smartpower/SmartPowerPolicyManager$UsageStatsInfo;->mEndTimeStamp:J */
/* .line 1622 */
/* iget-wide v4, p0, Lcom/miui/server/smartpower/SmartPowerPolicyManager$UsageStatsInfo;->mBeginTimeStamp:J */
/* sub-long/2addr v2, v4 */
/* const-wide/32 v4, 0x36ee80 */
/* div-long/2addr v2, v4 */
/* iput-wide v2, p0, Lcom/miui/server/smartpower/SmartPowerPolicyManager$UsageStatsInfo;->mDurationHours:J */
/* .line 1623 */
/* iget-wide v2, p1, Landroid/app/usage/UsageStats;->mLastTimeVisible:J */
/* iput-wide v2, p0, Lcom/miui/server/smartpower/SmartPowerPolicyManager$UsageStatsInfo;->mLastTimeVisible:J */
/* .line 1624 */
return;
} // .end method
