.class public Lcom/miui/server/smartpower/SmartPowerPolicyManager$UsageStatsInfo;
.super Ljava/lang/Object;
.source "SmartPowerPolicyManager.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/miui/server/smartpower/SmartPowerPolicyManager;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x1
    name = "UsageStatsInfo"
.end annotation


# instance fields
.field private mAppLaunchCount:I

.field private mBeginTimeStamp:J

.field private mDurationHours:J

.field private mEndTimeStamp:J

.field private mLastTimeVisible:J

.field private mPackageName:Ljava/lang/String;

.field private mTotalTimeInForeground:J

.field private mTotalTimeVisible:J

.field final synthetic this$0:Lcom/miui/server/smartpower/SmartPowerPolicyManager;


# direct methods
.method static bridge synthetic -$$Nest$fgetmAppLaunchCount(Lcom/miui/server/smartpower/SmartPowerPolicyManager$UsageStatsInfo;)I
    .locals 0

    iget p0, p0, Lcom/miui/server/smartpower/SmartPowerPolicyManager$UsageStatsInfo;->mAppLaunchCount:I

    return p0
.end method

.method constructor <init>(Lcom/miui/server/smartpower/SmartPowerPolicyManager;Landroid/app/usage/UsageStats;)V
    .locals 3
    .param p1, "this$0"    # Lcom/miui/server/smartpower/SmartPowerPolicyManager;
    .param p2, "stats"    # Landroid/app/usage/UsageStats;

    .line 1599
    iput-object p1, p0, Lcom/miui/server/smartpower/SmartPowerPolicyManager$UsageStatsInfo;->this$0:Lcom/miui/server/smartpower/SmartPowerPolicyManager;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1587
    const-wide/16 v0, 0x0

    iput-wide v0, p0, Lcom/miui/server/smartpower/SmartPowerPolicyManager$UsageStatsInfo;->mTotalTimeInForeground:J

    .line 1588
    iput-wide v0, p0, Lcom/miui/server/smartpower/SmartPowerPolicyManager$UsageStatsInfo;->mTotalTimeVisible:J

    .line 1589
    iput-wide v0, p0, Lcom/miui/server/smartpower/SmartPowerPolicyManager$UsageStatsInfo;->mLastTimeVisible:J

    .line 1590
    const/4 v2, 0x0

    iput v2, p0, Lcom/miui/server/smartpower/SmartPowerPolicyManager$UsageStatsInfo;->mAppLaunchCount:I

    .line 1591
    iput-wide v0, p0, Lcom/miui/server/smartpower/SmartPowerPolicyManager$UsageStatsInfo;->mBeginTimeStamp:J

    .line 1592
    iput-wide v0, p0, Lcom/miui/server/smartpower/SmartPowerPolicyManager$UsageStatsInfo;->mEndTimeStamp:J

    .line 1593
    iput-wide v0, p0, Lcom/miui/server/smartpower/SmartPowerPolicyManager$UsageStatsInfo;->mDurationHours:J

    .line 1600
    invoke-virtual {p0, p2}, Lcom/miui/server/smartpower/SmartPowerPolicyManager$UsageStatsInfo;->updateStatsInfo(Landroid/app/usage/UsageStats;)V

    .line 1601
    return-void
.end method

.method constructor <init>(Lcom/miui/server/smartpower/SmartPowerPolicyManager;Ljava/lang/String;)V
    .locals 3
    .param p1, "this$0"    # Lcom/miui/server/smartpower/SmartPowerPolicyManager;
    .param p2, "pakageName"    # Ljava/lang/String;

    .line 1595
    iput-object p1, p0, Lcom/miui/server/smartpower/SmartPowerPolicyManager$UsageStatsInfo;->this$0:Lcom/miui/server/smartpower/SmartPowerPolicyManager;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1587
    const-wide/16 v0, 0x0

    iput-wide v0, p0, Lcom/miui/server/smartpower/SmartPowerPolicyManager$UsageStatsInfo;->mTotalTimeInForeground:J

    .line 1588
    iput-wide v0, p0, Lcom/miui/server/smartpower/SmartPowerPolicyManager$UsageStatsInfo;->mTotalTimeVisible:J

    .line 1589
    iput-wide v0, p0, Lcom/miui/server/smartpower/SmartPowerPolicyManager$UsageStatsInfo;->mLastTimeVisible:J

    .line 1590
    const/4 v2, 0x0

    iput v2, p0, Lcom/miui/server/smartpower/SmartPowerPolicyManager$UsageStatsInfo;->mAppLaunchCount:I

    .line 1591
    iput-wide v0, p0, Lcom/miui/server/smartpower/SmartPowerPolicyManager$UsageStatsInfo;->mBeginTimeStamp:J

    .line 1592
    iput-wide v0, p0, Lcom/miui/server/smartpower/SmartPowerPolicyManager$UsageStatsInfo;->mEndTimeStamp:J

    .line 1593
    iput-wide v0, p0, Lcom/miui/server/smartpower/SmartPowerPolicyManager$UsageStatsInfo;->mDurationHours:J

    .line 1596
    iput-object p2, p0, Lcom/miui/server/smartpower/SmartPowerPolicyManager$UsageStatsInfo;->mPackageName:Ljava/lang/String;

    .line 1597
    return-void
.end method


# virtual methods
.method public dump(Ljava/io/PrintWriter;[Ljava/lang/String;I)V
    .locals 3
    .param p1, "pw"    # Ljava/io/PrintWriter;
    .param p2, "args"    # [Ljava/lang/String;
    .param p3, "opti"    # I

    .line 1639
    iget v0, p0, Lcom/miui/server/smartpower/SmartPowerPolicyManager$UsageStatsInfo;->mAppLaunchCount:I

    if-lez v0, :cond_0

    .line 1640
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "#"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lcom/miui/server/smartpower/SmartPowerPolicyManager$UsageStatsInfo;->mPackageName:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, " dur:"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-wide v1, p0, Lcom/miui/server/smartpower/SmartPowerPolicyManager$UsageStatsInfo;->mDurationHours:J

    invoke-virtual {v0, v1, v2}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "H vis="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-wide v1, p0, Lcom/miui/server/smartpower/SmartPowerPolicyManager$UsageStatsInfo;->mTotalTimeVisible:J

    invoke-virtual {v0, v1, v2}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "ms top="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-wide v1, p0, Lcom/miui/server/smartpower/SmartPowerPolicyManager$UsageStatsInfo;->mTotalTimeInForeground:J

    invoke-virtual {v0, v1, v2}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "ms launch="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget v1, p0, Lcom/miui/server/smartpower/SmartPowerPolicyManager$UsageStatsInfo;->mAppLaunchCount:I

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v0}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V

    .line 1646
    :cond_0
    return-void
.end method

.method public getAppLaunchCount()I
    .locals 1

    .line 1627
    iget v0, p0, Lcom/miui/server/smartpower/SmartPowerPolicyManager$UsageStatsInfo;->mAppLaunchCount:I

    return v0
.end method

.method public getLastTimeVisible()J
    .locals 2

    .line 1631
    iget-wide v0, p0, Lcom/miui/server/smartpower/SmartPowerPolicyManager$UsageStatsInfo;->mLastTimeVisible:J

    return-wide v0
.end method

.method public getTotalTimeInForeground()J
    .locals 2

    .line 1635
    iget-wide v0, p0, Lcom/miui/server/smartpower/SmartPowerPolicyManager$UsageStatsInfo;->mTotalTimeInForeground:J

    return-wide v0
.end method

.method public reset()V
    .locals 3

    .line 1658
    const-wide/16 v0, 0x0

    iput-wide v0, p0, Lcom/miui/server/smartpower/SmartPowerPolicyManager$UsageStatsInfo;->mTotalTimeInForeground:J

    .line 1659
    iput-wide v0, p0, Lcom/miui/server/smartpower/SmartPowerPolicyManager$UsageStatsInfo;->mTotalTimeVisible:J

    .line 1660
    const/4 v2, 0x0

    iput v2, p0, Lcom/miui/server/smartpower/SmartPowerPolicyManager$UsageStatsInfo;->mAppLaunchCount:I

    .line 1661
    iput-wide v0, p0, Lcom/miui/server/smartpower/SmartPowerPolicyManager$UsageStatsInfo;->mBeginTimeStamp:J

    .line 1662
    iput-wide v0, p0, Lcom/miui/server/smartpower/SmartPowerPolicyManager$UsageStatsInfo;->mEndTimeStamp:J

    .line 1663
    iput-wide v0, p0, Lcom/miui/server/smartpower/SmartPowerPolicyManager$UsageStatsInfo;->mDurationHours:J

    .line 1664
    return-void
.end method

.method public toString()Ljava/lang/String;
    .locals 3

    .line 1650
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v1, p0, Lcom/miui/server/smartpower/SmartPowerPolicyManager$UsageStatsInfo;->mPackageName:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, " dur:"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-wide v1, p0, Lcom/miui/server/smartpower/SmartPowerPolicyManager$UsageStatsInfo;->mDurationHours:J

    invoke-virtual {v0, v1, v2}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "H vis="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-wide v1, p0, Lcom/miui/server/smartpower/SmartPowerPolicyManager$UsageStatsInfo;->mTotalTimeVisible:J

    invoke-virtual {v0, v1, v2}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "ms top="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-wide v1, p0, Lcom/miui/server/smartpower/SmartPowerPolicyManager$UsageStatsInfo;->mTotalTimeInForeground:J

    invoke-virtual {v0, v1, v2}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "ms launch="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget v1, p0, Lcom/miui/server/smartpower/SmartPowerPolicyManager$UsageStatsInfo;->mAppLaunchCount:I

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method updateStatsInfo(Landroid/app/usage/UsageStats;)V
    .locals 6
    .param p1, "stats"    # Landroid/app/usage/UsageStats;

    .line 1604
    const-wide/16 v0, 0x0

    .line 1605
    .local v0, "firstInstallTime":J
    iget-object v2, p1, Landroid/app/usage/UsageStats;->mPackageName:Ljava/lang/String;

    iput-object v2, p0, Lcom/miui/server/smartpower/SmartPowerPolicyManager$UsageStatsInfo;->mPackageName:Ljava/lang/String;

    .line 1607
    :try_start_0
    iget-object v2, p0, Lcom/miui/server/smartpower/SmartPowerPolicyManager$UsageStatsInfo;->this$0:Lcom/miui/server/smartpower/SmartPowerPolicyManager;

    invoke-static {v2}, Lcom/miui/server/smartpower/SmartPowerPolicyManager;->-$$Nest$fgetmContext(Lcom/miui/server/smartpower/SmartPowerPolicyManager;)Landroid/content/Context;

    move-result-object v2

    invoke-virtual {v2}, Landroid/content/Context;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v2

    iget-object v3, p0, Lcom/miui/server/smartpower/SmartPowerPolicyManager$UsageStatsInfo;->mPackageName:Ljava/lang/String;

    .line 1608
    const/4 v4, 0x0

    invoke-virtual {v2, v3, v4}, Landroid/content/pm/PackageManager;->getPackageInfo(Ljava/lang/String;I)Landroid/content/pm/PackageInfo;

    move-result-object v2

    iget-wide v2, v2, Landroid/content/pm/PackageInfo;->firstInstallTime:J
    :try_end_0
    .catch Landroid/content/pm/PackageManager$NameNotFoundException; {:try_start_0 .. :try_end_0} :catch_0

    move-wide v0, v2

    goto :goto_0

    .line 1609
    :catch_0
    move-exception v2

    :goto_0
    nop

    .line 1611
    iget-wide v2, p0, Lcom/miui/server/smartpower/SmartPowerPolicyManager$UsageStatsInfo;->mTotalTimeInForeground:J

    iget-wide v4, p1, Landroid/app/usage/UsageStats;->mTotalTimeInForeground:J

    add-long/2addr v2, v4

    iput-wide v2, p0, Lcom/miui/server/smartpower/SmartPowerPolicyManager$UsageStatsInfo;->mTotalTimeInForeground:J

    .line 1612
    iget-wide v2, p0, Lcom/miui/server/smartpower/SmartPowerPolicyManager$UsageStatsInfo;->mTotalTimeVisible:J

    iget-wide v4, p1, Landroid/app/usage/UsageStats;->mTotalTimeVisible:J

    add-long/2addr v2, v4

    iput-wide v2, p0, Lcom/miui/server/smartpower/SmartPowerPolicyManager$UsageStatsInfo;->mTotalTimeVisible:J

    .line 1613
    iget v2, p0, Lcom/miui/server/smartpower/SmartPowerPolicyManager$UsageStatsInfo;->mAppLaunchCount:I

    iget v3, p1, Landroid/app/usage/UsageStats;->mAppLaunchCount:I

    add-int/2addr v2, v3

    iput v2, p0, Lcom/miui/server/smartpower/SmartPowerPolicyManager$UsageStatsInfo;->mAppLaunchCount:I

    .line 1614
    iget-wide v2, p0, Lcom/miui/server/smartpower/SmartPowerPolicyManager$UsageStatsInfo;->mBeginTimeStamp:J

    const-wide/16 v4, 0x0

    cmp-long v2, v2, v4

    if-nez v2, :cond_0

    .line 1615
    iget-wide v2, p1, Landroid/app/usage/UsageStats;->mBeginTimeStamp:J

    invoke-static {v2, v3, v0, v1}, Ljava/lang/Math;->max(JJ)J

    move-result-wide v2

    iput-wide v2, p0, Lcom/miui/server/smartpower/SmartPowerPolicyManager$UsageStatsInfo;->mBeginTimeStamp:J

    goto :goto_1

    .line 1617
    :cond_0
    iget-wide v2, p1, Landroid/app/usage/UsageStats;->mBeginTimeStamp:J

    invoke-static {v2, v3, v0, v1}, Ljava/lang/Math;->max(JJ)J

    move-result-wide v2

    .line 1618
    .local v2, "beginTimeStamp":J
    iget-wide v4, p0, Lcom/miui/server/smartpower/SmartPowerPolicyManager$UsageStatsInfo;->mBeginTimeStamp:J

    invoke-static {v2, v3, v4, v5}, Ljava/lang/Math;->min(JJ)J

    move-result-wide v4

    iput-wide v4, p0, Lcom/miui/server/smartpower/SmartPowerPolicyManager$UsageStatsInfo;->mBeginTimeStamp:J

    .line 1621
    .end local v2    # "beginTimeStamp":J
    :goto_1
    iget-wide v2, p1, Landroid/app/usage/UsageStats;->mEndTimeStamp:J

    iget-wide v4, p0, Lcom/miui/server/smartpower/SmartPowerPolicyManager$UsageStatsInfo;->mEndTimeStamp:J

    invoke-static {v2, v3, v4, v5}, Ljava/lang/Math;->max(JJ)J

    move-result-wide v2

    iput-wide v2, p0, Lcom/miui/server/smartpower/SmartPowerPolicyManager$UsageStatsInfo;->mEndTimeStamp:J

    .line 1622
    iget-wide v4, p0, Lcom/miui/server/smartpower/SmartPowerPolicyManager$UsageStatsInfo;->mBeginTimeStamp:J

    sub-long/2addr v2, v4

    const-wide/32 v4, 0x36ee80

    div-long/2addr v2, v4

    iput-wide v2, p0, Lcom/miui/server/smartpower/SmartPowerPolicyManager$UsageStatsInfo;->mDurationHours:J

    .line 1623
    iget-wide v2, p1, Landroid/app/usage/UsageStats;->mLastTimeVisible:J

    iput-wide v2, p0, Lcom/miui/server/smartpower/SmartPowerPolicyManager$UsageStatsInfo;->mLastTimeVisible:J

    .line 1624
    return-void
.end method
