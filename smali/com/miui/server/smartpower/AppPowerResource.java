public abstract class com.miui.server.smartpower.AppPowerResource {
	 /* .source "AppPowerResource.java" */
	 /* # annotations */
	 /* .annotation system Ldalvik/annotation/MemberClasses; */
	 /* value = { */
	 /* Lcom/miui/server/smartpower/AppPowerResource$IAppPowerResourceCallback; */
	 /* } */
} // .end annotation
/* # static fields */
protected static final Boolean DEBUG;
protected static final java.lang.String TAG;
/* # instance fields */
final android.util.SparseArray mResourceCallbacksByPid;
/* .annotation system Ldalvik/annotation/Signature; */
/* value = { */
/* "Landroid/util/SparseArray<", */
/* "Ljava/util/List<", */
/* "Lcom/miui/server/smartpower/AppPowerResource$IAppPowerResourceCallback;", */
/* ">;>;" */
/* } */
} // .end annotation
} // .end field
final android.util.SparseArray mResourceCallbacksByUid;
/* .annotation system Ldalvik/annotation/Signature; */
/* value = { */
/* "Landroid/util/SparseArray<", */
/* "Ljava/util/List<", */
/* "Lcom/miui/server/smartpower/AppPowerResource$IAppPowerResourceCallback;", */
/* ">;>;" */
/* } */
} // .end annotation
} // .end field
public Integer mType;
/* # direct methods */
static com.miui.server.smartpower.AppPowerResource ( ) {
/* .locals 1 */
/* .line 11 */
/* sget-boolean v0, Lcom/miui/server/smartpower/AppPowerResourceManager;->DEBUG:Z */
com.miui.server.smartpower.AppPowerResource.DEBUG = (v0!= 0);
return;
} // .end method
public com.miui.server.smartpower.AppPowerResource ( ) {
/* .locals 1 */
/* .line 9 */
/* invoke-direct {p0}, Ljava/lang/Object;-><init>()V */
/* .line 14 */
/* new-instance v0, Landroid/util/SparseArray; */
/* invoke-direct {v0}, Landroid/util/SparseArray;-><init>()V */
this.mResourceCallbacksByUid = v0;
/* .line 16 */
/* new-instance v0, Landroid/util/SparseArray; */
/* invoke-direct {v0}, Landroid/util/SparseArray;-><init>()V */
this.mResourceCallbacksByPid = v0;
return;
} // .end method
/* # virtual methods */
public abstract java.util.ArrayList getActiveUids ( ) {
} // .end method
public void init ( ) {
/* .locals 0 */
/* .line 29 */
return;
} // .end method
public abstract Boolean isAppResourceActive ( Integer p0 ) {
} // .end method
public abstract Boolean isAppResourceActive ( Integer p0, Integer p1 ) {
} // .end method
public void registerCallback ( com.miui.server.smartpower.AppPowerResource$IAppPowerResourceCallback p0, Integer p1 ) {
/* .locals 3 */
/* .param p1, "callback" # Lcom/miui/server/smartpower/AppPowerResource$IAppPowerResourceCallback; */
/* .param p2, "uid" # I */
/* .line 32 */
v0 = this.mResourceCallbacksByUid;
/* monitor-enter v0 */
/* .line 33 */
try { // :try_start_0
v1 = this.mResourceCallbacksByUid;
(( android.util.SparseArray ) v1 ).get ( p2 ); // invoke-virtual {v1, p2}, Landroid/util/SparseArray;->get(I)Ljava/lang/Object;
/* check-cast v1, Ljava/util/List; */
/* .line 34 */
/* .local v1, "callbacks":Ljava/util/List;, "Ljava/util/List<Lcom/miui/server/smartpower/AppPowerResource$IAppPowerResourceCallback;>;" */
/* if-nez v1, :cond_0 */
/* .line 35 */
/* new-instance v2, Ljava/util/ArrayList; */
/* invoke-direct {v2}, Ljava/util/ArrayList;-><init>()V */
/* move-object v1, v2 */
/* .line 36 */
v2 = this.mResourceCallbacksByUid;
(( android.util.SparseArray ) v2 ).put ( p2, v1 ); // invoke-virtual {v2, p2, v1}, Landroid/util/SparseArray;->put(ILjava/lang/Object;)V
/* .line 38 */
v2 = } // :cond_0
/* if-nez v2, :cond_1 */
/* .line 39 */
/* .line 41 */
} // .end local v1 # "callbacks":Ljava/util/List;, "Ljava/util/List<Lcom/miui/server/smartpower/AppPowerResource$IAppPowerResourceCallback;>;"
} // :cond_1
/* monitor-exit v0 */
/* .line 42 */
return;
/* .line 41 */
/* :catchall_0 */
/* move-exception v1 */
/* monitor-exit v0 */
/* :try_end_0 */
/* .catchall {:try_start_0 ..:try_end_0} :catchall_0 */
/* throw v1 */
} // .end method
public void registerCallback ( com.miui.server.smartpower.AppPowerResource$IAppPowerResourceCallback p0, Integer p1, Integer p2 ) {
/* .locals 3 */
/* .param p1, "callback" # Lcom/miui/server/smartpower/AppPowerResource$IAppPowerResourceCallback; */
/* .param p2, "uid" # I */
/* .param p3, "pid" # I */
/* .line 72 */
v0 = this.mResourceCallbacksByPid;
/* monitor-enter v0 */
/* .line 73 */
try { // :try_start_0
v1 = this.mResourceCallbacksByPid;
(( android.util.SparseArray ) v1 ).get ( p3 ); // invoke-virtual {v1, p3}, Landroid/util/SparseArray;->get(I)Ljava/lang/Object;
/* check-cast v1, Ljava/util/List; */
/* .line 74 */
/* .local v1, "callbacks":Ljava/util/List;, "Ljava/util/List<Lcom/miui/server/smartpower/AppPowerResource$IAppPowerResourceCallback;>;" */
/* if-nez v1, :cond_0 */
/* .line 75 */
/* new-instance v2, Ljava/util/ArrayList; */
/* invoke-direct {v2}, Ljava/util/ArrayList;-><init>()V */
/* move-object v1, v2 */
/* .line 76 */
v2 = this.mResourceCallbacksByPid;
(( android.util.SparseArray ) v2 ).put ( p3, v1 ); // invoke-virtual {v2, p3, v1}, Landroid/util/SparseArray;->put(ILjava/lang/Object;)V
/* .line 78 */
v2 = } // :cond_0
/* if-nez v2, :cond_1 */
/* .line 79 */
/* .line 81 */
} // .end local v1 # "callbacks":Ljava/util/List;, "Ljava/util/List<Lcom/miui/server/smartpower/AppPowerResource$IAppPowerResourceCallback;>;"
} // :cond_1
/* monitor-exit v0 */
/* .line 82 */
return;
/* .line 81 */
/* :catchall_0 */
/* move-exception v1 */
/* monitor-exit v0 */
/* :try_end_0 */
/* .catchall {:try_start_0 ..:try_end_0} :catchall_0 */
/* throw v1 */
} // .end method
public abstract void releaseAppPowerResource ( Integer p0 ) {
} // .end method
void reportResourceStatus ( Integer p0, Integer p1, Boolean p2, Integer p3 ) {
/* .locals 5 */
/* .param p1, "uid" # I */
/* .param p2, "pid" # I */
/* .param p3, "active" # Z */
/* .param p4, "behavier" # I */
/* .line 97 */
v0 = this.mResourceCallbacksByPid;
/* monitor-enter v0 */
/* .line 98 */
try { // :try_start_0
v1 = this.mResourceCallbacksByPid;
(( android.util.SparseArray ) v1 ).get ( p2 ); // invoke-virtual {v1, p2}, Landroid/util/SparseArray;->get(I)Ljava/lang/Object;
/* check-cast v1, Ljava/util/List; */
/* .line 99 */
/* .local v1, "callbacks":Ljava/util/List;, "Ljava/util/List<Lcom/miui/server/smartpower/AppPowerResource$IAppPowerResourceCallback;>;" */
if ( v1 != null) { // if-eqz v1, :cond_1
/* .line 100 */
v3 = } // :goto_0
if ( v3 != null) { // if-eqz v3, :cond_1
/* check-cast v3, Lcom/miui/server/smartpower/AppPowerResource$IAppPowerResourceCallback; */
/* .line 101 */
/* .local v3, "callback":Lcom/miui/server/smartpower/AppPowerResource$IAppPowerResourceCallback; */
if ( p3 != null) { // if-eqz p3, :cond_0
/* .line 102 */
/* iget v4, p0, Lcom/miui/server/smartpower/AppPowerResource;->mType:I */
/* .line 104 */
} // :cond_0
/* iget v4, p0, Lcom/miui/server/smartpower/AppPowerResource;->mType:I */
/* .line 106 */
} // .end local v3 # "callback":Lcom/miui/server/smartpower/AppPowerResource$IAppPowerResourceCallback;
} // :goto_1
/* .line 108 */
} // .end local v1 # "callbacks":Ljava/util/List;, "Ljava/util/List<Lcom/miui/server/smartpower/AppPowerResource$IAppPowerResourceCallback;>;"
} // :cond_1
/* monitor-exit v0 */
/* .line 109 */
return;
/* .line 108 */
/* :catchall_0 */
/* move-exception v1 */
/* monitor-exit v0 */
/* :try_end_0 */
/* .catchall {:try_start_0 ..:try_end_0} :catchall_0 */
/* throw v1 */
} // .end method
void reportResourceStatus ( Integer p0, Boolean p1, Integer p2 ) {
/* .locals 5 */
/* .param p1, "uid" # I */
/* .param p2, "active" # Z */
/* .param p3, "behavier" # I */
/* .line 57 */
v0 = this.mResourceCallbacksByUid;
/* monitor-enter v0 */
/* .line 58 */
try { // :try_start_0
v1 = this.mResourceCallbacksByUid;
(( android.util.SparseArray ) v1 ).get ( p1 ); // invoke-virtual {v1, p1}, Landroid/util/SparseArray;->get(I)Ljava/lang/Object;
/* check-cast v1, Ljava/util/List; */
/* .line 59 */
/* .local v1, "callbacks":Ljava/util/List;, "Ljava/util/List<Lcom/miui/server/smartpower/AppPowerResource$IAppPowerResourceCallback;>;" */
if ( v1 != null) { // if-eqz v1, :cond_1
/* .line 60 */
v3 = } // :goto_0
if ( v3 != null) { // if-eqz v3, :cond_1
/* check-cast v3, Lcom/miui/server/smartpower/AppPowerResource$IAppPowerResourceCallback; */
/* .line 61 */
/* .local v3, "callback":Lcom/miui/server/smartpower/AppPowerResource$IAppPowerResourceCallback; */
if ( p2 != null) { // if-eqz p2, :cond_0
/* .line 62 */
/* iget v4, p0, Lcom/miui/server/smartpower/AppPowerResource;->mType:I */
/* .line 64 */
} // :cond_0
/* iget v4, p0, Lcom/miui/server/smartpower/AppPowerResource;->mType:I */
/* .line 66 */
} // .end local v3 # "callback":Lcom/miui/server/smartpower/AppPowerResource$IAppPowerResourceCallback;
} // :goto_1
/* .line 68 */
} // .end local v1 # "callbacks":Ljava/util/List;, "Ljava/util/List<Lcom/miui/server/smartpower/AppPowerResource$IAppPowerResourceCallback;>;"
} // :cond_1
/* monitor-exit v0 */
/* .line 69 */
return;
/* .line 68 */
/* :catchall_0 */
/* move-exception v1 */
/* monitor-exit v0 */
/* :try_end_0 */
/* .catchall {:try_start_0 ..:try_end_0} :catchall_0 */
/* throw v1 */
} // .end method
public abstract void resumeAppPowerResource ( Integer p0 ) {
} // .end method
public void unRegisterCallback ( com.miui.server.smartpower.AppPowerResource$IAppPowerResourceCallback p0, Integer p1 ) {
/* .locals 3 */
/* .param p1, "callback" # Lcom/miui/server/smartpower/AppPowerResource$IAppPowerResourceCallback; */
/* .param p2, "uid" # I */
/* .line 45 */
v0 = this.mResourceCallbacksByUid;
/* monitor-enter v0 */
/* .line 46 */
try { // :try_start_0
v1 = this.mResourceCallbacksByUid;
(( android.util.SparseArray ) v1 ).get ( p2 ); // invoke-virtual {v1, p2}, Landroid/util/SparseArray;->get(I)Ljava/lang/Object;
/* check-cast v1, Ljava/util/List; */
/* .line 47 */
/* .local v1, "callbacks":Ljava/util/List;, "Ljava/util/List<Lcom/miui/server/smartpower/AppPowerResource$IAppPowerResourceCallback;>;" */
v2 = if ( v1 != null) { // if-eqz v1, :cond_0
if ( v2 != null) { // if-eqz v2, :cond_0
/* .line 48 */
v2 = /* .line 49 */
/* if-nez v2, :cond_0 */
/* .line 50 */
v2 = this.mResourceCallbacksByUid;
(( android.util.SparseArray ) v2 ).remove ( p2 ); // invoke-virtual {v2, p2}, Landroid/util/SparseArray;->remove(I)V
/* .line 53 */
} // .end local v1 # "callbacks":Ljava/util/List;, "Ljava/util/List<Lcom/miui/server/smartpower/AppPowerResource$IAppPowerResourceCallback;>;"
} // :cond_0
/* monitor-exit v0 */
/* .line 54 */
return;
/* .line 53 */
/* :catchall_0 */
/* move-exception v1 */
/* monitor-exit v0 */
/* :try_end_0 */
/* .catchall {:try_start_0 ..:try_end_0} :catchall_0 */
/* throw v1 */
} // .end method
public void unRegisterCallback ( com.miui.server.smartpower.AppPowerResource$IAppPowerResourceCallback p0, Integer p1, Integer p2 ) {
/* .locals 3 */
/* .param p1, "callback" # Lcom/miui/server/smartpower/AppPowerResource$IAppPowerResourceCallback; */
/* .param p2, "uid" # I */
/* .param p3, "pid" # I */
/* .line 85 */
v0 = this.mResourceCallbacksByPid;
/* monitor-enter v0 */
/* .line 86 */
try { // :try_start_0
v1 = this.mResourceCallbacksByPid;
(( android.util.SparseArray ) v1 ).get ( p3 ); // invoke-virtual {v1, p3}, Landroid/util/SparseArray;->get(I)Ljava/lang/Object;
/* check-cast v1, Ljava/util/List; */
/* .line 87 */
/* .local v1, "callbacks":Ljava/util/List;, "Ljava/util/List<Lcom/miui/server/smartpower/AppPowerResource$IAppPowerResourceCallback;>;" */
v2 = if ( v1 != null) { // if-eqz v1, :cond_0
if ( v2 != null) { // if-eqz v2, :cond_0
/* .line 88 */
v2 = /* .line 89 */
/* if-nez v2, :cond_0 */
/* .line 90 */
v2 = this.mResourceCallbacksByPid;
(( android.util.SparseArray ) v2 ).remove ( p3 ); // invoke-virtual {v2, p3}, Landroid/util/SparseArray;->remove(I)V
/* .line 93 */
} // .end local v1 # "callbacks":Ljava/util/List;, "Ljava/util/List<Lcom/miui/server/smartpower/AppPowerResource$IAppPowerResourceCallback;>;"
} // :cond_0
/* monitor-exit v0 */
/* .line 94 */
return;
/* .line 93 */
/* :catchall_0 */
/* move-exception v1 */
/* monitor-exit v0 */
/* :try_end_0 */
/* .catchall {:try_start_0 ..:try_end_0} :catchall_0 */
/* throw v1 */
} // .end method
