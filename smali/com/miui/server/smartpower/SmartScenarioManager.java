public class com.miui.server.smartpower.SmartScenarioManager {
	 /* .source "SmartScenarioManager.java" */
	 /* # annotations */
	 /* .annotation system Ldalvik/annotation/MemberClasses; */
	 /* value = { */
	 /* Lcom/miui/server/smartpower/SmartScenarioManager$ScenarioItem;, */
	 /* Lcom/miui/server/smartpower/SmartScenarioManager$ClientConfig;, */
	 /* Lcom/miui/server/smartpower/SmartScenarioManager$ISmartScenarioCallback;, */
	 /* Lcom/miui/server/smartpower/SmartScenarioManager$ScenarioIdInfo;, */
	 /* Lcom/miui/server/smartpower/SmartScenarioManager$ScenarioAppChild; */
	 /* } */
} // .end annotation
/* # static fields */
public static final Boolean DEBUG;
public static final Integer INTERACTIVE_ACTION_TYPE_BACKGROUND;
public static final Integer INTERACTIVE_ACTION_TYPE_CAMERA;
public static final Integer INTERACTIVE_ACTION_TYPE_CHARGING;
public static final Integer INTERACTIVE_ACTION_TYPE_DOWNLOAD;
public static final Integer INTERACTIVE_ACTION_TYPE_EXTERNAL_CASTING;
public static final Integer INTERACTIVE_ACTION_TYPE_FREEFORM;
public static final Integer INTERACTIVE_ACTION_TYPE_MUSICPLAY;
public static final Integer INTERACTIVE_ACTION_TYPE_NAVIGATION;
public static final Integer INTERACTIVE_ACTION_TYPE_OVERLAY;
public static final Integer INTERACTIVE_ACTION_TYPE_SPLIT;
public static final Integer INTERACTIVE_ACTION_TYPE_TOPAPP;
public static final Integer INTERACTIVE_ACTION_TYPE_TOPGAME;
public static final Integer INTERACTIVE_ACTION_TYPE_VIDEOCALL;
public static final Integer INTERACTIVE_ACTION_TYPE_VIDEOPLAY;
public static final Integer INTERACTIVE_ACTION_TYPE_VOICECALL;
public static final Integer INTERACTIVE_ACTION_TYPE_WIFI_CASTING;
public static final Integer INTERACTIVE_ADDITIONAL_SCENARIO_ACTION_MASK;
public static final Integer INTERACTIVE_MAIN_SCENARIO_ACTION_MASK;
private static final Integer RESOURCE_SCENARIO_CAMERA;
private static final Integer RESOURCE_SCENARIO_DOWNLOAD;
private static final Integer RESOURCE_SCENARIO_EXTERNAL_CASTING;
private static final Integer RESOURCE_SCENARIO_MUSIC;
private static final Integer RESOURCE_SCENARIO_NAVIGATION;
private static final Integer RESOURCE_SCENARIO_VIDEO;
private static final Integer RESOURCE_SCENARIO_VIDEOCALL;
private static final Integer RESOURCE_SCENARIO_VOICECALL;
private static final Integer RESOURCE_SCENARIO_WIFI_CASTING;
public static final java.lang.String TAG;
/* # instance fields */
private Boolean mCharging;
private final android.util.ArraySet mClients;
/* .annotation system Ldalvik/annotation/Signature; */
/* value = { */
/* "Landroid/util/ArraySet<", */
/* "Lcom/miui/server/smartpower/SmartScenarioManager$ClientConfig;", */
/* ">;" */
/* } */
} // .end annotation
} // .end field
private android.content.Context mContext;
private final android.util.SparseArray mCurrentScenarios;
/* .annotation system Ldalvik/annotation/Signature; */
/* value = { */
/* "Landroid/util/SparseArray<", */
/* "Lcom/miui/server/smartpower/SmartScenarioManager$ScenarioItem;", */
/* ">;" */
/* } */
} // .end annotation
} // .end field
private android.os.Handler mHandler;
private final android.util.ArraySet mInterestBgPkgs;
/* .annotation system Ldalvik/annotation/Signature; */
/* value = { */
/* "Landroid/util/ArraySet<", */
/* "Ljava/lang/String;", */
/* ">;" */
/* } */
} // .end annotation
} // .end field
private final android.content.BroadcastReceiver mReceiver;
/* # direct methods */
static Boolean -$$Nest$fgetmCharging ( com.miui.server.smartpower.SmartScenarioManager p0 ) { //bridge//synthethic
/* .locals 0 */
/* iget-boolean p0, p0, Lcom/miui/server/smartpower/SmartScenarioManager;->mCharging:Z */
} // .end method
static android.util.ArraySet -$$Nest$fgetmClients ( com.miui.server.smartpower.SmartScenarioManager p0 ) { //bridge//synthethic
/* .locals 0 */
p0 = this.mClients;
} // .end method
static android.os.Handler -$$Nest$fgetmHandler ( com.miui.server.smartpower.SmartScenarioManager p0 ) { //bridge//synthethic
/* .locals 0 */
p0 = this.mHandler;
} // .end method
static android.util.ArraySet -$$Nest$fgetmInterestBgPkgs ( com.miui.server.smartpower.SmartScenarioManager p0 ) { //bridge//synthethic
/* .locals 0 */
p0 = this.mInterestBgPkgs;
} // .end method
static void -$$Nest$fputmCharging ( com.miui.server.smartpower.SmartScenarioManager p0, Boolean p1 ) { //bridge//synthethic
/* .locals 0 */
/* iput-boolean p1, p0, Lcom/miui/server/smartpower/SmartScenarioManager;->mCharging:Z */
return;
} // .end method
static com.miui.server.smartpower.SmartScenarioManager ( ) {
/* .locals 1 */
/* .line 19 */
/* sget-boolean v0, Lcom/android/server/am/SmartPowerService;->DEBUG:Z */
com.miui.server.smartpower.SmartScenarioManager.DEBUG = (v0!= 0);
return;
} // .end method
public com.miui.server.smartpower.SmartScenarioManager ( ) {
/* .locals 3 */
/* .param p1, "context" # Landroid/content/Context; */
/* .param p2, "looper" # Landroid/os/Looper; */
/* .line 176 */
/* invoke-direct {p0}, Ljava/lang/Object;-><init>()V */
/* .line 154 */
/* new-instance v0, Landroid/util/SparseArray; */
/* invoke-direct {v0}, Landroid/util/SparseArray;-><init>()V */
this.mCurrentScenarios = v0;
/* .line 155 */
/* new-instance v0, Landroid/util/ArraySet; */
/* invoke-direct {v0}, Landroid/util/ArraySet;-><init>()V */
this.mClients = v0;
/* .line 156 */
/* new-instance v0, Landroid/util/ArraySet; */
/* invoke-direct {v0}, Landroid/util/ArraySet;-><init>()V */
this.mInterestBgPkgs = v0;
/* .line 159 */
int v0 = 0; // const/4 v0, 0x0
/* iput-boolean v0, p0, Lcom/miui/server/smartpower/SmartScenarioManager;->mCharging:Z */
/* .line 161 */
/* new-instance v0, Lcom/miui/server/smartpower/SmartScenarioManager$1; */
/* invoke-direct {v0, p0}, Lcom/miui/server/smartpower/SmartScenarioManager$1;-><init>(Lcom/miui/server/smartpower/SmartScenarioManager;)V */
this.mReceiver = v0;
/* .line 177 */
this.mContext = p1;
/* .line 178 */
/* new-instance v1, Landroid/os/Handler; */
/* invoke-direct {v1, p2}, Landroid/os/Handler;-><init>(Landroid/os/Looper;)V */
this.mHandler = v1;
/* .line 179 */
/* new-instance v1, Landroid/content/IntentFilter; */
/* invoke-direct {v1}, Landroid/content/IntentFilter;-><init>()V */
/* .line 180 */
/* .local v1, "filter":Landroid/content/IntentFilter; */
final String v2 = "android.intent.action.BATTERY_CHANGED"; // const-string v2, "android.intent.action.BATTERY_CHANGED"
(( android.content.IntentFilter ) v1 ).addAction ( v2 ); // invoke-virtual {v1, v2}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V
/* .line 181 */
v2 = this.mContext;
(( android.content.Context ) v2 ).registerReceiver ( v0, v1 ); // invoke-virtual {v2, v0, v1}, Landroid/content/Context;->registerReceiver(Landroid/content/BroadcastReceiver;Landroid/content/IntentFilter;)Landroid/content/Intent;
/* .line 182 */
return;
} // .end method
public static java.lang.String actionTypeToString ( Integer p0 ) {
/* .locals 3 */
/* .param p0, "action" # I */
/* .line 47 */
final String v0 = ""; // const-string v0, ""
/* .line 48 */
/* .local v0, "typeStr":Ljava/lang/String; */
/* and-int/lit8 v1, p0, 0x2 */
if ( v1 != null) { // if-eqz v1, :cond_0
/* .line 49 */
/* new-instance v1, Ljava/lang/StringBuilder; */
/* invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V */
(( java.lang.StringBuilder ) v1 ).append ( v0 ); // invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
/* const-string/jumbo v2, "top app, " */
(( java.lang.StringBuilder ) v1 ).append ( v2 ); // invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v1 ).toString ( ); // invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
/* .line 51 */
} // :cond_0
/* and-int/lit8 v1, p0, 0x4 */
if ( v1 != null) { // if-eqz v1, :cond_1
/* .line 52 */
/* new-instance v1, Ljava/lang/StringBuilder; */
/* invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V */
(( java.lang.StringBuilder ) v1 ).append ( v0 ); // invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
/* const-string/jumbo v2, "top game, " */
(( java.lang.StringBuilder ) v1 ).append ( v2 ); // invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v1 ).toString ( ); // invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
/* .line 54 */
} // :cond_1
/* and-int/lit8 v1, p0, 0x8 */
if ( v1 != null) { // if-eqz v1, :cond_2
/* .line 55 */
/* new-instance v1, Ljava/lang/StringBuilder; */
/* invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V */
(( java.lang.StringBuilder ) v1 ).append ( v0 ); // invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
final String v2 = "background, "; // const-string v2, "background, "
(( java.lang.StringBuilder ) v1 ).append ( v2 ); // invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v1 ).toString ( ); // invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
/* .line 57 */
} // :cond_2
/* and-int/lit8 v1, p0, 0x10 */
if ( v1 != null) { // if-eqz v1, :cond_3
/* .line 58 */
/* new-instance v1, Ljava/lang/StringBuilder; */
/* invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V */
(( java.lang.StringBuilder ) v1 ).append ( v0 ); // invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
final String v2 = "freeform, "; // const-string v2, "freeform, "
(( java.lang.StringBuilder ) v1 ).append ( v2 ); // invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v1 ).toString ( ); // invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
/* .line 60 */
} // :cond_3
/* and-int/lit8 v1, p0, 0x20 */
if ( v1 != null) { // if-eqz v1, :cond_4
/* .line 61 */
/* new-instance v1, Ljava/lang/StringBuilder; */
/* invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V */
(( java.lang.StringBuilder ) v1 ).append ( v0 ); // invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
final String v2 = "overlay, "; // const-string v2, "overlay, "
(( java.lang.StringBuilder ) v1 ).append ( v2 ); // invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v1 ).toString ( ); // invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
/* .line 63 */
} // :cond_4
/* and-int/lit8 v1, p0, 0x40 */
if ( v1 != null) { // if-eqz v1, :cond_5
/* .line 64 */
/* new-instance v1, Ljava/lang/StringBuilder; */
/* invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V */
(( java.lang.StringBuilder ) v1 ).append ( v0 ); // invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
/* const-string/jumbo v2, "voice call, " */
(( java.lang.StringBuilder ) v1 ).append ( v2 ); // invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v1 ).toString ( ); // invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
/* .line 66 */
} // :cond_5
/* and-int/lit16 v1, p0, 0x80 */
if ( v1 != null) { // if-eqz v1, :cond_6
/* .line 67 */
/* new-instance v1, Ljava/lang/StringBuilder; */
/* invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V */
(( java.lang.StringBuilder ) v1 ).append ( v0 ); // invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
/* const-string/jumbo v2, "video call, " */
(( java.lang.StringBuilder ) v1 ).append ( v2 ); // invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v1 ).toString ( ); // invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
/* .line 69 */
} // :cond_6
/* and-int/lit16 v1, p0, 0x100 */
if ( v1 != null) { // if-eqz v1, :cond_7
/* .line 70 */
/* new-instance v1, Ljava/lang/StringBuilder; */
/* invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V */
(( java.lang.StringBuilder ) v1 ).append ( v0 ); // invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
final String v2 = "download, "; // const-string v2, "download, "
(( java.lang.StringBuilder ) v1 ).append ( v2 ); // invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v1 ).toString ( ); // invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
/* .line 72 */
} // :cond_7
/* and-int/lit16 v1, p0, 0x200 */
if ( v1 != null) { // if-eqz v1, :cond_8
/* .line 73 */
/* new-instance v1, Ljava/lang/StringBuilder; */
/* invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V */
(( java.lang.StringBuilder ) v1 ).append ( v0 ); // invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
final String v2 = "music play, "; // const-string v2, "music play, "
(( java.lang.StringBuilder ) v1 ).append ( v2 ); // invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v1 ).toString ( ); // invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
/* .line 75 */
} // :cond_8
/* and-int/lit16 v1, p0, 0x400 */
if ( v1 != null) { // if-eqz v1, :cond_9
/* .line 76 */
/* new-instance v1, Ljava/lang/StringBuilder; */
/* invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V */
(( java.lang.StringBuilder ) v1 ).append ( v0 ); // invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
/* const-string/jumbo v2, "video play, " */
(( java.lang.StringBuilder ) v1 ).append ( v2 ); // invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v1 ).toString ( ); // invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
/* .line 78 */
} // :cond_9
/* and-int/lit16 v1, p0, 0x800 */
if ( v1 != null) { // if-eqz v1, :cond_a
/* .line 79 */
/* new-instance v1, Ljava/lang/StringBuilder; */
/* invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V */
(( java.lang.StringBuilder ) v1 ).append ( v0 ); // invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
final String v2 = "charging, "; // const-string v2, "charging, "
(( java.lang.StringBuilder ) v1 ).append ( v2 ); // invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v1 ).toString ( ); // invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
/* .line 81 */
} // :cond_a
/* and-int/lit16 v1, p0, 0x1000 */
if ( v1 != null) { // if-eqz v1, :cond_b
/* .line 82 */
/* new-instance v1, Ljava/lang/StringBuilder; */
/* invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V */
(( java.lang.StringBuilder ) v1 ).append ( v0 ); // invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
final String v2 = "navigation, "; // const-string v2, "navigation, "
(( java.lang.StringBuilder ) v1 ).append ( v2 ); // invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v1 ).toString ( ); // invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
/* .line 84 */
} // :cond_b
/* and-int/lit16 v1, p0, 0x2000 */
if ( v1 != null) { // if-eqz v1, :cond_c
/* .line 85 */
/* new-instance v1, Ljava/lang/StringBuilder; */
/* invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V */
(( java.lang.StringBuilder ) v1 ).append ( v0 ); // invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
/* const-string/jumbo v2, "wifi casting, " */
(( java.lang.StringBuilder ) v1 ).append ( v2 ); // invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v1 ).toString ( ); // invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
/* .line 87 */
} // :cond_c
/* and-int/lit16 v1, p0, 0x4000 */
if ( v1 != null) { // if-eqz v1, :cond_d
/* .line 88 */
/* new-instance v1, Ljava/lang/StringBuilder; */
/* invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V */
(( java.lang.StringBuilder ) v1 ).append ( v0 ); // invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
final String v2 = "external casting, "; // const-string v2, "external casting, "
(( java.lang.StringBuilder ) v1 ).append ( v2 ); // invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v1 ).toString ( ); // invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
/* .line 90 */
} // :cond_d
/* const v1, 0x8000 */
/* and-int/2addr v1, p0 */
if ( v1 != null) { // if-eqz v1, :cond_e
/* .line 91 */
/* new-instance v1, Ljava/lang/StringBuilder; */
/* invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V */
(( java.lang.StringBuilder ) v1 ).append ( v0 ); // invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
/* const-string/jumbo v2, "split screen, " */
(( java.lang.StringBuilder ) v1 ).append ( v2 ); // invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v1 ).toString ( ); // invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
/* .line 93 */
} // :cond_e
/* const/high16 v1, 0x10000 */
/* and-int/2addr v1, p0 */
if ( v1 != null) { // if-eqz v1, :cond_f
/* .line 94 */
/* new-instance v1, Ljava/lang/StringBuilder; */
/* invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V */
(( java.lang.StringBuilder ) v1 ).append ( v0 ); // invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
final String v2 = "camera, "; // const-string v2, "camera, "
(( java.lang.StringBuilder ) v1 ).append ( v2 ); // invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v1 ).toString ( ); // invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
/* .line 96 */
} // :cond_f
} // .end method
private static Boolean behavierContainsScenario ( Integer p0, Integer p1 ) {
/* .locals 1 */
/* .param p0, "behavier" # I */
/* .param p1, "scenario" # I */
/* .line 147 */
/* and-int v0, p0, p1 */
/* if-ne v0, p1, :cond_0 */
int v0 = 1; // const/4 v0, 0x1
} // :cond_0
int v0 = 0; // const/4 v0, 0x0
} // :goto_0
} // .end method
public static Integer calculateScenarioAction ( Integer p0 ) {
/* .locals 2 */
/* .param p0, "behavier" # I */
/* .line 121 */
int v0 = 0; // const/4 v0, 0x0
/* .line 122 */
/* .local v0, "actions":I */
/* const/16 v1, 0x100 */
v1 = com.miui.server.smartpower.SmartScenarioManager .behavierContainsScenario ( p0,v1 );
if ( v1 != null) { // if-eqz v1, :cond_0
/* .line 123 */
/* or-int/lit16 v0, v0, 0x2000 */
/* .line 124 */
} // :cond_0
/* const/16 v1, 0x200 */
v1 = com.miui.server.smartpower.SmartScenarioManager .behavierContainsScenario ( p0,v1 );
if ( v1 != null) { // if-eqz v1, :cond_1
/* .line 125 */
/* or-int/lit16 v0, v0, 0x4000 */
/* .line 126 */
} // :cond_1
/* const/16 v1, 0x30 */
v1 = com.miui.server.smartpower.SmartScenarioManager .behavierContainsScenario ( p0,v1 );
if ( v1 != null) { // if-eqz v1, :cond_2
/* .line 127 */
/* or-int/lit16 v0, v0, 0x80 */
/* .line 128 */
} // :cond_2
/* const/16 v1, 0x10 */
v1 = com.miui.server.smartpower.SmartScenarioManager .behavierContainsScenario ( p0,v1 );
if ( v1 != null) { // if-eqz v1, :cond_3
/* .line 129 */
/* or-int/lit8 v0, v0, 0x40 */
/* .line 130 */
} // :cond_3
/* const/16 v1, 0x404 */
v1 = com.miui.server.smartpower.SmartScenarioManager .behavierContainsScenario ( p0,v1 );
if ( v1 != null) { // if-eqz v1, :cond_4
/* .line 131 */
/* or-int/lit16 v0, v0, 0x400 */
/* .line 132 */
} // :cond_4
int v1 = 2; // const/4 v1, 0x2
v1 = com.miui.server.smartpower.SmartScenarioManager .behavierContainsScenario ( p0,v1 );
if ( v1 != null) { // if-eqz v1, :cond_5
/* .line 133 */
/* or-int/lit16 v0, v0, 0x200 */
/* .line 134 */
} // :cond_5
/* const/16 v1, 0x80 */
v1 = com.miui.server.smartpower.SmartScenarioManager .behavierContainsScenario ( p0,v1 );
if ( v1 != null) { // if-eqz v1, :cond_6
/* .line 135 */
/* or-int/lit16 v0, v0, 0x100 */
/* .line 137 */
} // :cond_6
} // :goto_0
/* const/16 v1, 0x20 */
v1 = com.miui.server.smartpower.SmartScenarioManager .behavierContainsScenario ( p0,v1 );
if ( v1 != null) { // if-eqz v1, :cond_7
/* .line 138 */
/* const/high16 v1, 0x10000 */
/* or-int/2addr v0, v1 */
/* .line 140 */
} // :cond_7
/* const/16 v1, 0x40 */
v1 = com.miui.server.smartpower.SmartScenarioManager .behavierContainsScenario ( p0,v1 );
if ( v1 != null) { // if-eqz v1, :cond_8
/* .line 141 */
/* or-int/lit16 v0, v0, 0x1000 */
/* .line 143 */
} // :cond_8
} // .end method
static void lambda$onAppActionChanged$0 ( Integer p0, Boolean p1, com.miui.server.smartpower.SmartScenarioManager$ClientConfig p2 ) { //synthethic
/* .locals 1 */
/* .param p0, "action" # I */
/* .param p1, "add" # Z */
/* .param p2, "v" # Lcom/miui/server/smartpower/SmartScenarioManager$ClientConfig; */
/* .line 212 */
int v0 = 0; // const/4 v0, 0x0
(( com.miui.server.smartpower.SmartScenarioManager$ClientConfig ) p2 ).onAppActionChanged ( p0, v0, p1 ); // invoke-virtual {p2, p0, v0, p1}, Lcom/miui/server/smartpower/SmartScenarioManager$ClientConfig;->onAppActionChanged(ILjava/lang/String;Z)V
/* .line 213 */
return;
} // .end method
/* # virtual methods */
public com.miui.server.smartpower.SmartScenarioManager$ClientConfig createClientConfig ( java.lang.String p0, com.miui.server.smartpower.SmartScenarioManager$ISmartScenarioCallback p1 ) {
/* .locals 1 */
/* .param p1, "clientName" # Ljava/lang/String; */
/* .param p2, "callback" # Lcom/miui/server/smartpower/SmartScenarioManager$ISmartScenarioCallback; */
/* .line 219 */
/* new-instance v0, Lcom/miui/server/smartpower/SmartScenarioManager$ClientConfig; */
/* invoke-direct {v0, p0, p1, p2}, Lcom/miui/server/smartpower/SmartScenarioManager$ClientConfig;-><init>(Lcom/miui/server/smartpower/SmartScenarioManager;Ljava/lang/String;Lcom/miui/server/smartpower/SmartScenarioManager$ISmartScenarioCallback;)V */
} // .end method
public void dump ( java.io.PrintWriter p0, java.lang.String[] p1, Integer p2 ) {
/* .locals 4 */
/* .param p1, "pw" # Ljava/io/PrintWriter; */
/* .param p2, "args" # [Ljava/lang/String; */
/* .param p3, "opti" # I */
/* .line 239 */
final String v0 = "interactive apps:"; // const-string v0, "interactive apps:"
(( java.io.PrintWriter ) p1 ).println ( v0 ); // invoke-virtual {p1, v0}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V
/* .line 240 */
v0 = this.mCurrentScenarios;
/* monitor-enter v0 */
/* .line 241 */
int v1 = 0; // const/4 v1, 0x0
/* .local v1, "i":I */
} // :goto_0
try { // :try_start_0
v2 = this.mCurrentScenarios;
v2 = (( android.util.SparseArray ) v2 ).size ( ); // invoke-virtual {v2}, Landroid/util/SparseArray;->size()I
/* if-ge v1, v2, :cond_0 */
/* .line 242 */
/* new-instance v2, Ljava/lang/StringBuilder; */
/* invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V */
final String v3 = " "; // const-string v3, " "
(( java.lang.StringBuilder ) v2 ).append ( v3 ); // invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
v3 = this.mCurrentScenarios;
(( android.util.SparseArray ) v3 ).valueAt ( v1 ); // invoke-virtual {v3, v1}, Landroid/util/SparseArray;->valueAt(I)Ljava/lang/Object;
(( java.lang.StringBuilder ) v2 ).append ( v3 ); // invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v2 ).toString ( ); // invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
(( java.io.PrintWriter ) p1 ).println ( v2 ); // invoke-virtual {p1, v2}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V
/* .line 241 */
/* add-int/lit8 v1, v1, 0x1 */
/* .line 244 */
} // .end local v1 # "i":I
} // :cond_0
/* monitor-exit v0 */
/* :try_end_0 */
/* .catchall {:try_start_0 ..:try_end_0} :catchall_1 */
/* .line 246 */
final String v0 = ""; // const-string v0, ""
(( java.io.PrintWriter ) p1 ).println ( v0 ); // invoke-virtual {p1, v0}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V
/* .line 247 */
v1 = this.mClients;
/* monitor-enter v1 */
/* .line 248 */
int v0 = 0; // const/4 v0, 0x0
/* .local v0, "i":I */
} // :goto_1
try { // :try_start_1
v2 = this.mClients;
v2 = (( android.util.ArraySet ) v2 ).size ( ); // invoke-virtual {v2}, Landroid/util/ArraySet;->size()I
/* if-ge v0, v2, :cond_1 */
/* .line 249 */
v2 = this.mClients;
(( android.util.ArraySet ) v2 ).valueAt ( v0 ); // invoke-virtual {v2, v0}, Landroid/util/ArraySet;->valueAt(I)Ljava/lang/Object;
/* check-cast v2, Lcom/miui/server/smartpower/SmartScenarioManager$ClientConfig; */
(( com.miui.server.smartpower.SmartScenarioManager$ClientConfig ) v2 ).dump ( p1, p2, p3 ); // invoke-virtual {v2, p1, p2, p3}, Lcom/miui/server/smartpower/SmartScenarioManager$ClientConfig;->dump(Ljava/io/PrintWriter;[Ljava/lang/String;I)V
/* .line 248 */
/* add-int/lit8 v0, v0, 0x1 */
/* .line 251 */
} // .end local v0 # "i":I
} // :cond_1
/* monitor-exit v1 */
/* .line 252 */
return;
/* .line 251 */
/* :catchall_0 */
/* move-exception v0 */
/* monitor-exit v1 */
/* :try_end_1 */
/* .catchall {:try_start_1 ..:try_end_1} :catchall_0 */
/* throw v0 */
/* .line 244 */
/* :catchall_1 */
/* move-exception v1 */
try { // :try_start_2
/* monitor-exit v0 */
/* :try_end_2 */
/* .catchall {:try_start_2 ..:try_end_2} :catchall_1 */
/* throw v1 */
} // .end method
public void onAppActionChanged ( Integer p0, com.android.server.am.AppStateManager$AppState p1, Boolean p2 ) {
/* .locals 5 */
/* .param p1, "action" # I */
/* .param p2, "app" # Lcom/android/server/am/AppStateManager$AppState; */
/* .param p3, "add" # Z */
/* .line 185 */
v0 = this.mInterestBgPkgs;
/* monitor-enter v0 */
/* .line 186 */
if ( p2 != null) { // if-eqz p2, :cond_0
/* const/16 v1, 0x8 */
/* if-ne p1, v1, :cond_0 */
try { // :try_start_0
v1 = this.mInterestBgPkgs;
/* .line 187 */
(( com.android.server.am.AppStateManager$AppState ) p2 ).getPackageName ( ); // invoke-virtual {p2}, Lcom/android/server/am/AppStateManager$AppState;->getPackageName()Ljava/lang/String;
v1 = (( android.util.ArraySet ) v1 ).contains ( v2 ); // invoke-virtual {v1, v2}, Landroid/util/ArraySet;->contains(Ljava/lang/Object;)Z
/* if-nez v1, :cond_0 */
/* .line 188 */
/* monitor-exit v0 */
return;
/* .line 190 */
} // :cond_0
/* monitor-exit v0 */
/* :try_end_0 */
/* .catchall {:try_start_0 ..:try_end_0} :catchall_2 */
/* .line 192 */
int v0 = 0; // const/4 v0, 0x0
/* .line 193 */
/* .local v0, "changed":Z */
v1 = this.mCurrentScenarios;
/* monitor-enter v1 */
/* .line 194 */
try { // :try_start_1
v2 = this.mCurrentScenarios;
(( android.util.SparseArray ) v2 ).get ( p1 ); // invoke-virtual {v2, p1}, Landroid/util/SparseArray;->get(I)Ljava/lang/Object;
/* check-cast v2, Lcom/miui/server/smartpower/SmartScenarioManager$ScenarioItem; */
/* .line 195 */
/* .local v2, "scenarioItem":Lcom/miui/server/smartpower/SmartScenarioManager$ScenarioItem; */
if ( p3 != null) { // if-eqz p3, :cond_1
/* if-nez v2, :cond_1 */
/* .line 196 */
/* new-instance v3, Lcom/miui/server/smartpower/SmartScenarioManager$ScenarioItem; */
int v4 = 0; // const/4 v4, 0x0
/* invoke-direct {v3, p0, p1, v4}, Lcom/miui/server/smartpower/SmartScenarioManager$ScenarioItem;-><init>(Lcom/miui/server/smartpower/SmartScenarioManager;ILcom/miui/server/smartpower/SmartScenarioManager$ScenarioItem-IA;)V */
/* move-object v2, v3 */
/* .line 197 */
v3 = this.mCurrentScenarios;
(( android.util.SparseArray ) v3 ).append ( p1, v2 ); // invoke-virtual {v3, p1, v2}, Landroid/util/SparseArray;->append(ILjava/lang/Object;)V
/* .line 198 */
int v0 = 1; // const/4 v0, 0x1
/* .line 201 */
} // :cond_1
if ( v2 != null) { // if-eqz v2, :cond_2
/* .line 202 */
(( com.miui.server.smartpower.SmartScenarioManager$ScenarioItem ) v2 ).onAppActionChanged ( p2, p3 ); // invoke-virtual {v2, p2, p3}, Lcom/miui/server/smartpower/SmartScenarioManager$ScenarioItem;->onAppActionChanged(Lcom/android/server/am/AppStateManager$AppState;Z)V
/* .line 203 */
/* if-nez p3, :cond_2 */
com.miui.server.smartpower.SmartScenarioManager$ScenarioItem .-$$Nest$fgetmInteractiveApps ( v2 );
v3 = (( java.util.ArrayList ) v3 ).size ( ); // invoke-virtual {v3}, Ljava/util/ArrayList;->size()I
/* if-nez v3, :cond_2 */
/* .line 204 */
v3 = this.mCurrentScenarios;
(( android.util.SparseArray ) v3 ).remove ( p1 ); // invoke-virtual {v3, p1}, Landroid/util/SparseArray;->remove(I)V
/* .line 205 */
int v0 = 1; // const/4 v0, 0x1
/* .line 208 */
} // .end local v2 # "scenarioItem":Lcom/miui/server/smartpower/SmartScenarioManager$ScenarioItem;
} // :cond_2
/* monitor-exit v1 */
/* :try_end_1 */
/* .catchall {:try_start_1 ..:try_end_1} :catchall_1 */
/* .line 209 */
if ( v0 != null) { // if-eqz v0, :cond_3
/* .line 210 */
v1 = this.mClients;
/* monitor-enter v1 */
/* .line 211 */
try { // :try_start_2
v2 = this.mClients;
/* new-instance v3, Lcom/miui/server/smartpower/SmartScenarioManager$$ExternalSyntheticLambda0; */
/* invoke-direct {v3, p1, p3}, Lcom/miui/server/smartpower/SmartScenarioManager$$ExternalSyntheticLambda0;-><init>(IZ)V */
(( android.util.ArraySet ) v2 ).forEach ( v3 ); // invoke-virtual {v2, v3}, Landroid/util/ArraySet;->forEach(Ljava/util/function/Consumer;)V
/* .line 214 */
/* monitor-exit v1 */
/* :catchall_0 */
/* move-exception v2 */
/* monitor-exit v1 */
/* :try_end_2 */
/* .catchall {:try_start_2 ..:try_end_2} :catchall_0 */
/* throw v2 */
/* .line 216 */
} // :cond_3
} // :goto_0
return;
/* .line 208 */
/* :catchall_1 */
/* move-exception v2 */
try { // :try_start_3
/* monitor-exit v1 */
/* :try_end_3 */
/* .catchall {:try_start_3 ..:try_end_3} :catchall_1 */
/* throw v2 */
/* .line 190 */
} // .end local v0 # "changed":Z
/* :catchall_2 */
/* move-exception v1 */
try { // :try_start_4
/* monitor-exit v0 */
/* :try_end_4 */
/* .catchall {:try_start_4 ..:try_end_4} :catchall_2 */
/* throw v1 */
} // .end method
public void parseCurrentScenarioId ( com.miui.server.smartpower.SmartScenarioManager$ClientConfig p0 ) {
/* .locals 3 */
/* .param p1, "client" # Lcom/miui/server/smartpower/SmartScenarioManager$ClientConfig; */
/* .line 230 */
v0 = this.mCurrentScenarios;
/* monitor-enter v0 */
/* .line 231 */
int v1 = 0; // const/4 v1, 0x0
/* .local v1, "i":I */
} // :goto_0
try { // :try_start_0
v2 = this.mCurrentScenarios;
v2 = (( android.util.SparseArray ) v2 ).size ( ); // invoke-virtual {v2}, Landroid/util/SparseArray;->size()I
/* if-ge v1, v2, :cond_0 */
/* .line 232 */
v2 = this.mCurrentScenarios;
(( android.util.SparseArray ) v2 ).valueAt ( v1 ); // invoke-virtual {v2, v1}, Landroid/util/SparseArray;->valueAt(I)Ljava/lang/Object;
/* check-cast v2, Lcom/miui/server/smartpower/SmartScenarioManager$ScenarioItem; */
/* .line 233 */
/* .local v2, "scenarioItem":Lcom/miui/server/smartpower/SmartScenarioManager$ScenarioItem; */
(( com.miui.server.smartpower.SmartScenarioManager$ScenarioItem ) v2 ).parseCurrentScenarioId ( p1 ); // invoke-virtual {v2, p1}, Lcom/miui/server/smartpower/SmartScenarioManager$ScenarioItem;->parseCurrentScenarioId(Lcom/miui/server/smartpower/SmartScenarioManager$ClientConfig;)V
/* .line 231 */
} // .end local v2 # "scenarioItem":Lcom/miui/server/smartpower/SmartScenarioManager$ScenarioItem;
/* add-int/lit8 v1, v1, 0x1 */
/* .line 235 */
} // .end local v1 # "i":I
} // :cond_0
/* monitor-exit v0 */
/* .line 236 */
return;
/* .line 235 */
/* :catchall_0 */
/* move-exception v1 */
/* monitor-exit v0 */
/* :try_end_0 */
/* .catchall {:try_start_0 ..:try_end_0} :catchall_0 */
/* throw v1 */
} // .end method
public void registClientConfig ( com.miui.server.smartpower.SmartScenarioManager$ClientConfig p0 ) {
/* .locals 2 */
/* .param p1, "client" # Lcom/miui/server/smartpower/SmartScenarioManager$ClientConfig; */
/* .line 223 */
v0 = this.mClients;
/* monitor-enter v0 */
/* .line 224 */
try { // :try_start_0
v1 = this.mClients;
(( android.util.ArraySet ) v1 ).add ( p1 ); // invoke-virtual {v1, p1}, Landroid/util/ArraySet;->add(Ljava/lang/Object;)Z
/* .line 225 */
/* monitor-exit v0 */
/* :try_end_0 */
/* .catchall {:try_start_0 ..:try_end_0} :catchall_0 */
/* .line 226 */
(( com.miui.server.smartpower.SmartScenarioManager ) p0 ).parseCurrentScenarioId ( p1 ); // invoke-virtual {p0, p1}, Lcom/miui/server/smartpower/SmartScenarioManager;->parseCurrentScenarioId(Lcom/miui/server/smartpower/SmartScenarioManager$ClientConfig;)V
/* .line 227 */
return;
/* .line 225 */
/* :catchall_0 */
/* move-exception v1 */
try { // :try_start_1
/* monitor-exit v0 */
/* :try_end_1 */
/* .catchall {:try_start_1 ..:try_end_1} :catchall_0 */
/* throw v1 */
} // .end method
