class com.miui.server.smartpower.AppNetworkResource extends com.miui.server.smartpower.AppPowerResource {
	 /* .source "AppNetworkResource.java" */
	 /* # annotations */
	 /* .annotation system Ldalvik/annotation/MemberClasses; */
	 /* value = { */
	 /* Lcom/miui/server/smartpower/AppNetworkResource$MyHandler;, */
	 /* Lcom/miui/server/smartpower/AppNetworkResource$NetworkMonitor; */
	 /* } */
} // .end annotation
/* # static fields */
private static final Integer MSG_NETWORK_CHECK;
private static final Integer NET_DOWNLOAD_SCENE_THRESHOLD;
private static final Integer NET_KB;
/* # instance fields */
private android.os.Handler mHandler;
private final java.util.HashMap mNetworkMonitorMap;
/* .annotation system Ldalvik/annotation/Signature; */
/* value = { */
/* "Ljava/util/HashMap<", */
/* "Ljava/lang/Integer;", */
/* "Lcom/miui/server/smartpower/AppNetworkResource$NetworkMonitor;", */
/* ">;" */
/* } */
} // .end annotation
} // .end field
private android.app.usage.NetworkStatsManager mNetworkStatsManager;
/* # direct methods */
static android.os.Handler -$$Nest$fgetmHandler ( com.miui.server.smartpower.AppNetworkResource p0 ) { //bridge//synthethic
/* .locals 0 */
p0 = this.mHandler;
} // .end method
static java.util.HashMap -$$Nest$fgetmNetworkMonitorMap ( com.miui.server.smartpower.AppNetworkResource p0 ) { //bridge//synthethic
/* .locals 0 */
p0 = this.mNetworkMonitorMap;
} // .end method
 com.miui.server.smartpower.AppNetworkResource ( ) {
/* .locals 1 */
/* .param p1, "context" # Landroid/content/Context; */
/* .param p2, "looper" # Landroid/os/Looper; */
/* .line 32 */
/* invoke-direct {p0}, Lcom/miui/server/smartpower/AppPowerResource;-><init>()V */
/* .line 30 */
/* new-instance v0, Ljava/util/HashMap; */
/* invoke-direct {v0}, Ljava/util/HashMap;-><init>()V */
this.mNetworkMonitorMap = v0;
/* .line 33 */
/* new-instance v0, Lcom/miui/server/smartpower/AppNetworkResource$MyHandler; */
/* invoke-direct {v0, p0, p2}, Lcom/miui/server/smartpower/AppNetworkResource$MyHandler;-><init>(Lcom/miui/server/smartpower/AppNetworkResource;Landroid/os/Looper;)V */
this.mHandler = v0;
/* .line 34 */
int v0 = 2; // const/4 v0, 0x2
/* iput v0, p0, Lcom/miui/server/smartpower/AppNetworkResource;->mType:I */
/* .line 35 */
return;
} // .end method
private void updateNetworkRule ( Integer p0, Boolean p1 ) {
/* .locals 4 */
/* .param p1, "uid" # I */
/* .param p2, "allow" # Z */
/* .line 71 */
final String v0 = "SmartPower.AppResource"; // const-string v0, "SmartPower.AppResource"
try { // :try_start_0
com.android.server.net.NetworkManagementServiceStub .getInstance ( );
/* .line 72 */
/* sget-boolean v1, Lcom/miui/server/smartpower/AppNetworkResource;->DEBUG:Z */
if ( v1 != null) { // if-eqz v1, :cond_0
	 /* new-instance v1, Ljava/lang/StringBuilder; */
	 /* invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V */
	 /* const-string/jumbo v2, "setFirewall received: " */
	 (( java.lang.StringBuilder ) v1 ).append ( v2 ); // invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
	 (( java.lang.StringBuilder ) v1 ).append ( p1 ); // invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;
	 (( java.lang.StringBuilder ) v1 ).toString ( ); // invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
	 android.util.Slog .d ( v0,v1 );
	 /* :try_end_0 */
	 /* .catch Ljava/lang/Exception; {:try_start_0 ..:try_end_0} :catch_0 */
	 /* .line 75 */
} // :cond_0
/* .line 73 */
/* :catch_0 */
/* move-exception v1 */
/* .line 74 */
/* .local v1, "e":Ljava/lang/Exception; */
/* new-instance v2, Ljava/lang/StringBuilder; */
/* invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V */
/* const-string/jumbo v3, "setFirewall failed " */
(( java.lang.StringBuilder ) v2 ).append ( v3 ); // invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v2 ).append ( p1 ); // invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v2 ).toString ( ); // invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
android.util.Slog .i ( v0,v2 );
/* .line 76 */
} // .end local v1 # "e":Ljava/lang/Exception;
} // :goto_0
return;
} // .end method
/* # virtual methods */
public java.util.ArrayList getActiveUids ( ) {
/* .locals 1 */
/* .line 39 */
int v0 = 0; // const/4 v0, 0x0
} // .end method
public Boolean isAppResourceActive ( Integer p0 ) {
/* .locals 1 */
/* .param p1, "uid" # I */
/* .line 44 */
int v0 = 0; // const/4 v0, 0x0
} // .end method
public Boolean isAppResourceActive ( Integer p0, Integer p1 ) {
/* .locals 1 */
/* .param p1, "uid" # I */
/* .param p2, "pid" # I */
/* .line 49 */
int v0 = 0; // const/4 v0, 0x0
} // .end method
public void registerCallback ( com.miui.server.smartpower.AppPowerResource$IAppPowerResourceCallback p0, Integer p1 ) {
/* .locals 4 */
/* .param p1, "callback" # Lcom/miui/server/smartpower/AppPowerResource$IAppPowerResourceCallback; */
/* .param p2, "uid" # I */
/* .line 80 */
/* invoke-super {p0, p1, p2}, Lcom/miui/server/smartpower/AppPowerResource;->registerCallback(Lcom/miui/server/smartpower/AppPowerResource$IAppPowerResourceCallback;I)V */
/* .line 81 */
v0 = this.mNetworkMonitorMap;
/* monitor-enter v0 */
/* .line 82 */
try { // :try_start_0
v1 = this.mNetworkMonitorMap;
java.lang.Integer .valueOf ( p2 );
(( java.util.HashMap ) v1 ).get ( v2 ); // invoke-virtual {v1, v2}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;
/* if-nez v1, :cond_0 */
/* .line 83 */
v1 = this.mNetworkMonitorMap;
java.lang.Integer .valueOf ( p2 );
/* new-instance v3, Lcom/miui/server/smartpower/AppNetworkResource$NetworkMonitor; */
/* invoke-direct {v3, p0, p2}, Lcom/miui/server/smartpower/AppNetworkResource$NetworkMonitor;-><init>(Lcom/miui/server/smartpower/AppNetworkResource;I)V */
(( java.util.HashMap ) v1 ).put ( v2, v3 ); // invoke-virtual {v1, v2, v3}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;
/* .line 85 */
} // :cond_0
/* monitor-exit v0 */
/* .line 86 */
return;
/* .line 85 */
/* :catchall_0 */
/* move-exception v1 */
/* monitor-exit v0 */
/* :try_end_0 */
/* .catchall {:try_start_0 ..:try_end_0} :catchall_0 */
/* throw v1 */
} // .end method
public void releaseAppPowerResource ( Integer p0 ) {
/* .locals 4 */
/* .param p1, "uid" # I */
/* .line 54 */
/* new-instance v0, Ljava/lang/StringBuilder; */
/* invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V */
final String v1 = "network u:"; // const-string v1, "network u:"
(( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v0 ).append ( p1 ); // invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;
final String v1 = " s:release"; // const-string v1, " s:release"
(( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v0 ).toString ( ); // invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
/* .line 55 */
/* .local v0, "event":Ljava/lang/String; */
/* const-wide/32 v1, 0x20000 */
android.os.Trace .traceBegin ( v1,v2,v0 );
/* .line 56 */
int v3 = 1; // const/4 v3, 0x1
/* invoke-direct {p0, p1, v3}, Lcom/miui/server/smartpower/AppNetworkResource;->updateNetworkRule(IZ)V */
/* .line 57 */
/* const v3, 0x15ff2 */
android.util.EventLog .writeEvent ( v3,v0 );
/* .line 58 */
android.os.Trace .traceEnd ( v1,v2 );
/* .line 59 */
return;
} // .end method
public void resumeAppPowerResource ( Integer p0 ) {
/* .locals 3 */
/* .param p1, "uid" # I */
/* .line 63 */
/* new-instance v0, Ljava/lang/StringBuilder; */
/* invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V */
final String v1 = "network u:"; // const-string v1, "network u:"
(( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v0 ).append ( p1 ); // invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;
final String v1 = " s:resume"; // const-string v1, " s:resume"
(( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v0 ).toString ( ); // invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
/* .line 64 */
/* .local v0, "event":Ljava/lang/String; */
int v1 = 0; // const/4 v1, 0x0
/* invoke-direct {p0, p1, v1}, Lcom/miui/server/smartpower/AppNetworkResource;->updateNetworkRule(IZ)V */
/* .line 65 */
/* const v1, 0x15ff2 */
android.util.EventLog .writeEvent ( v1,v0 );
/* .line 66 */
/* const-wide/32 v1, 0x20000 */
android.os.Trace .traceEnd ( v1,v2 );
/* .line 67 */
return;
} // .end method
public void unRegisterCallback ( com.miui.server.smartpower.AppPowerResource$IAppPowerResourceCallback p0, Integer p1 ) {
/* .locals 3 */
/* .param p1, "callback" # Lcom/miui/server/smartpower/AppPowerResource$IAppPowerResourceCallback; */
/* .param p2, "uid" # I */
/* .line 90 */
/* invoke-super {p0, p1, p2}, Lcom/miui/server/smartpower/AppPowerResource;->unRegisterCallback(Lcom/miui/server/smartpower/AppPowerResource$IAppPowerResourceCallback;I)V */
/* .line 91 */
v0 = this.mNetworkMonitorMap;
/* monitor-enter v0 */
/* .line 92 */
try { // :try_start_0
v1 = this.mResourceCallbacksByUid;
(( android.util.SparseArray ) v1 ).get ( p2 ); // invoke-virtual {v1, p2}, Landroid/util/SparseArray;->get(I)Ljava/lang/Object;
/* if-nez v1, :cond_0 */
/* .line 93 */
v1 = this.mNetworkMonitorMap;
java.lang.Integer .valueOf ( p2 );
(( java.util.HashMap ) v1 ).remove ( v2 ); // invoke-virtual {v1, v2}, Ljava/util/HashMap;->remove(Ljava/lang/Object;)Ljava/lang/Object;
/* .line 95 */
} // :cond_0
/* monitor-exit v0 */
/* .line 96 */
return;
/* .line 95 */
/* :catchall_0 */
/* move-exception v1 */
/* monitor-exit v0 */
/* :try_end_0 */
/* .catchall {:try_start_0 ..:try_end_0} :catchall_0 */
/* throw v1 */
} // .end method
