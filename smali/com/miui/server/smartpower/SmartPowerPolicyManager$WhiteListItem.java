public class com.miui.server.smartpower.SmartPowerPolicyManager$WhiteListItem {
	 /* .source "SmartPowerPolicyManager.java" */
	 /* # annotations */
	 /* .annotation system Ldalvik/annotation/EnclosingClass; */
	 /* value = Lcom/miui/server/smartpower/SmartPowerPolicyManager; */
} // .end annotation
/* .annotation system Ldalvik/annotation/InnerClass; */
/* accessFlags = 0x1 */
/* name = "WhiteListItem" */
} // .end annotation
/* # instance fields */
private Integer mActions;
private java.lang.String mName;
private Integer mPid;
private Integer mTypes;
private Integer mUid;
final com.miui.server.smartpower.SmartPowerPolicyManager this$0; //synthetic
/* # direct methods */
static Integer -$$Nest$fgetmActions ( com.miui.server.smartpower.SmartPowerPolicyManager$WhiteListItem p0 ) { //bridge//synthethic
/* .locals 0 */
/* iget p0, p0, Lcom/miui/server/smartpower/SmartPowerPolicyManager$WhiteListItem;->mActions:I */
} // .end method
static java.lang.String -$$Nest$fgetmName ( com.miui.server.smartpower.SmartPowerPolicyManager$WhiteListItem p0 ) { //bridge//synthethic
/* .locals 0 */
p0 = this.mName;
} // .end method
static Integer -$$Nest$fgetmPid ( com.miui.server.smartpower.SmartPowerPolicyManager$WhiteListItem p0 ) { //bridge//synthethic
/* .locals 0 */
/* iget p0, p0, Lcom/miui/server/smartpower/SmartPowerPolicyManager$WhiteListItem;->mPid:I */
} // .end method
static Integer -$$Nest$fgetmTypes ( com.miui.server.smartpower.SmartPowerPolicyManager$WhiteListItem p0 ) { //bridge//synthethic
/* .locals 0 */
/* iget p0, p0, Lcom/miui/server/smartpower/SmartPowerPolicyManager$WhiteListItem;->mTypes:I */
} // .end method
static Integer -$$Nest$fgetmUid ( com.miui.server.smartpower.SmartPowerPolicyManager$WhiteListItem p0 ) { //bridge//synthethic
/* .locals 0 */
/* iget p0, p0, Lcom/miui/server/smartpower/SmartPowerPolicyManager$WhiteListItem;->mUid:I */
} // .end method
static void -$$Nest$fputmActions ( com.miui.server.smartpower.SmartPowerPolicyManager$WhiteListItem p0, Integer p1 ) { //bridge//synthethic
/* .locals 0 */
/* iput p1, p0, Lcom/miui/server/smartpower/SmartPowerPolicyManager$WhiteListItem;->mActions:I */
return;
} // .end method
static void -$$Nest$fputmPid ( com.miui.server.smartpower.SmartPowerPolicyManager$WhiteListItem p0, Integer p1 ) { //bridge//synthethic
/* .locals 0 */
/* iput p1, p0, Lcom/miui/server/smartpower/SmartPowerPolicyManager$WhiteListItem;->mPid:I */
return;
} // .end method
static void -$$Nest$fputmTypes ( com.miui.server.smartpower.SmartPowerPolicyManager$WhiteListItem p0, Integer p1 ) { //bridge//synthethic
/* .locals 0 */
/* iput p1, p0, Lcom/miui/server/smartpower/SmartPowerPolicyManager$WhiteListItem;->mTypes:I */
return;
} // .end method
static void -$$Nest$fputmUid ( com.miui.server.smartpower.SmartPowerPolicyManager$WhiteListItem p0, Integer p1 ) { //bridge//synthethic
/* .locals 0 */
/* iput p1, p0, Lcom/miui/server/smartpower/SmartPowerPolicyManager$WhiteListItem;->mUid:I */
return;
} // .end method
 com.miui.server.smartpower.SmartPowerPolicyManager$WhiteListItem ( ) {
/* .locals 1 */
/* .param p1, "this$0" # Lcom/miui/server/smartpower/SmartPowerPolicyManager; */
/* .param p2, "name" # Ljava/lang/String; */
/* .param p3, "type" # I */
/* .param p4, "action" # I */
/* .line 1548 */
this.this$0 = p1;
/* invoke-direct {p0}, Ljava/lang/Object;-><init>()V */
/* .line 1542 */
int v0 = -1; // const/4 v0, -0x1
/* iput v0, p0, Lcom/miui/server/smartpower/SmartPowerPolicyManager$WhiteListItem;->mUid:I */
/* .line 1543 */
/* iput v0, p0, Lcom/miui/server/smartpower/SmartPowerPolicyManager$WhiteListItem;->mPid:I */
/* .line 1549 */
this.mName = p2;
/* .line 1550 */
/* iput p3, p0, Lcom/miui/server/smartpower/SmartPowerPolicyManager$WhiteListItem;->mTypes:I */
/* .line 1551 */
/* iput p4, p0, Lcom/miui/server/smartpower/SmartPowerPolicyManager$WhiteListItem;->mActions:I */
/* .line 1552 */
return;
} // .end method
/* # virtual methods */
public Boolean hasAction ( Integer p0 ) {
/* .locals 1 */
/* .param p1, "action" # I */
/* .line 1559 */
/* iget v0, p0, Lcom/miui/server/smartpower/SmartPowerPolicyManager$WhiteListItem;->mActions:I */
/* and-int/2addr v0, p1 */
if ( v0 != null) { // if-eqz v0, :cond_0
	 int v0 = 1; // const/4 v0, 0x1
} // :cond_0
int v0 = 0; // const/4 v0, 0x0
} // :goto_0
} // .end method
public Boolean hasWhiteListType ( Integer p0 ) {
/* .locals 1 */
/* .param p1, "type" # I */
/* .line 1555 */
/* iget v0, p0, Lcom/miui/server/smartpower/SmartPowerPolicyManager$WhiteListItem;->mTypes:I */
/* and-int/2addr v0, p1 */
if ( v0 != null) { // if-eqz v0, :cond_0
int v0 = 1; // const/4 v0, 0x1
} // :cond_0
int v0 = 0; // const/4 v0, 0x0
} // :goto_0
} // .end method
public java.lang.String toString ( ) {
/* .locals 3 */
/* .line 1564 */
/* new-instance v0, Ljava/lang/StringBuilder; */
/* const/16 v1, 0x80 */
/* invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(I)V */
/* .line 1565 */
/* .local v0, "sb":Ljava/lang/StringBuilder; */
v1 = this.mName;
(( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
/* .line 1566 */
/* iget v1, p0, Lcom/miui/server/smartpower/SmartPowerPolicyManager$WhiteListItem;->mUid:I */
int v2 = -1; // const/4 v2, -0x1
/* if-eq v1, v2, :cond_1 */
/* .line 1567 */
final String v1 = "(u:"; // const-string v1, "(u:"
(( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
/* .line 1568 */
/* iget v1, p0, Lcom/miui/server/smartpower/SmartPowerPolicyManager$WhiteListItem;->mUid:I */
(( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;
/* .line 1569 */
/* iget v1, p0, Lcom/miui/server/smartpower/SmartPowerPolicyManager$WhiteListItem;->mPid:I */
/* if-eq v1, v2, :cond_0 */
/* .line 1570 */
final String v1 = ", p:"; // const-string v1, ", p:"
(( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
/* .line 1571 */
/* iget v1, p0, Lcom/miui/server/smartpower/SmartPowerPolicyManager$WhiteListItem;->mPid:I */
(( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;
/* .line 1573 */
} // :cond_0
final String v1 = ")"; // const-string v1, ")"
(( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
/* .line 1575 */
} // :cond_1
final String v1 = " {"; // const-string v1, " {"
(( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
/* .line 1576 */
/* iget v1, p0, Lcom/miui/server/smartpower/SmartPowerPolicyManager$WhiteListItem;->mTypes:I */
com.miui.server.smartpower.SmartPowerPolicyManager .-$$Nest$smwhiteListTypeToString ( v1 );
(( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
/* .line 1577 */
/* const-string/jumbo v1, "} {" */
(( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
/* .line 1578 */
/* iget v1, p0, Lcom/miui/server/smartpower/SmartPowerPolicyManager$WhiteListItem;->mActions:I */
com.miui.server.smartpower.SmartPowerPolicyManager .-$$Nest$smwhiteListActionToString ( v1 );
(( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
/* .line 1579 */
/* const-string/jumbo v1, "}" */
(( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
/* .line 1580 */
(( java.lang.StringBuilder ) v0 ).toString ( ); // invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
} // .end method
