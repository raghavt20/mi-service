public class com.miui.server.smartpower.SmartCpuPolicyManager {
	 /* .source "SmartCpuPolicyManager.java" */
	 /* # annotations */
	 /* .annotation system Ldalvik/annotation/MemberClasses; */
	 /* value = { */
	 /* Lcom/miui/server/smartpower/SmartCpuPolicyManager$H; */
	 /* } */
} // .end annotation
/* # static fields */
private static final Integer CPU_EXCEPTION_HANDLE_THRESHOLD;
private static final java.lang.String CPU_EXCEPTION_KILL_REASON;
public static final Boolean DEBUG;
public static final Integer DEFAULT_BACKGROUND_CPU_CORE_NUM;
private static final Integer MONITOR_CPU_MIN_TIME;
static final Boolean MONITOR_THREAD_CPU_USAGE;
private static final Integer MSG_CPU_EXCEPTION;
public static final java.lang.String TAG;
/* # instance fields */
private com.android.server.am.ActivityManagerService mAMS;
private com.android.server.am.AppStateManager mAppStateManager;
private Integer mBackgroundCpuCoreNum;
private com.miui.server.smartpower.SmartCpuPolicyManager$H mHandler;
private android.os.HandlerThread mHandlerTh;
private Long mLashHandleCpuException;
private final java.util.concurrent.atomic.AtomicLong mLastUpdateCpuTime;
private final com.android.internal.os.ProcessCpuTracker mProcessCpuTracker;
private com.miui.server.smartpower.SmartPowerPolicyManager mSmartPowerPolicyManager;
/* # direct methods */
public static void $r8$lambda$hrw7akw63cq1a8LnnBAW_4RwzFw ( com.miui.server.smartpower.SmartCpuPolicyManager p0, Boolean p1, com.android.internal.os.ProcessCpuTracker$Stats p2 ) { //synthethic
	 /* .locals 0 */
	 /* invoke-direct {p0, p1, p2}, Lcom/miui/server/smartpower/SmartCpuPolicyManager;->lambda$updateCpuStatsNow$0(ZLcom/android/internal/os/ProcessCpuTracker$Stats;)V */
	 return;
} // .end method
static void -$$Nest$mhandleLimitCpuException ( com.miui.server.smartpower.SmartCpuPolicyManager p0, Integer p1 ) { //bridge//synthethic
	 /* .locals 0 */
	 /* invoke-direct {p0, p1}, Lcom/miui/server/smartpower/SmartCpuPolicyManager;->handleLimitCpuException(I)V */
	 return;
} // .end method
static com.miui.server.smartpower.SmartCpuPolicyManager ( ) {
	 /* .locals 1 */
	 /* .line 31 */
	 /* sget-boolean v0, Landroid/os/spc/PressureStateSettings;->DEBUG_ALL:Z */
	 com.miui.server.smartpower.SmartCpuPolicyManager.DEBUG = (v0!= 0);
	 return;
} // .end method
public com.miui.server.smartpower.SmartCpuPolicyManager ( ) {
	 /* .locals 3 */
	 /* .param p1, "context" # Landroid/content/Context; */
	 /* .param p2, "ams" # Lcom/android/server/am/ActivityManagerService; */
	 /* .line 53 */
	 /* invoke-direct {p0}, Ljava/lang/Object;-><init>()V */
	 /* .line 40 */
	 int v0 = 0; // const/4 v0, 0x0
	 this.mAppStateManager = v0;
	 /* .line 43 */
	 /* new-instance v0, Landroid/os/HandlerThread; */
	 final String v1 = "CpuPolicyTh"; // const-string v1, "CpuPolicyTh"
	 int v2 = -2; // const/4 v2, -0x2
	 /* invoke-direct {v0, v1, v2}, Landroid/os/HandlerThread;-><init>(Ljava/lang/String;I)V */
	 this.mHandlerTh = v0;
	 /* .line 45 */
	 int v0 = 4; // const/4 v0, 0x4
	 /* iput v0, p0, Lcom/miui/server/smartpower/SmartCpuPolicyManager;->mBackgroundCpuCoreNum:I */
	 /* .line 46 */
	 /* const-wide/16 v0, 0x0 */
	 /* iput-wide v0, p0, Lcom/miui/server/smartpower/SmartCpuPolicyManager;->mLashHandleCpuException:J */
	 /* .line 47 */
	 /* new-instance v2, Ljava/util/concurrent/atomic/AtomicLong; */
	 /* invoke-direct {v2, v0, v1}, Ljava/util/concurrent/atomic/AtomicLong;-><init>(J)V */
	 this.mLastUpdateCpuTime = v2;
	 /* .line 50 */
	 /* new-instance v0, Lcom/android/internal/os/ProcessCpuTracker; */
	 int v1 = 1; // const/4 v1, 0x1
	 /* invoke-direct {v0, v1}, Lcom/android/internal/os/ProcessCpuTracker;-><init>(Z)V */
	 this.mProcessCpuTracker = v0;
	 /* .line 54 */
	 this.mAMS = p2;
	 /* .line 56 */
	 return;
} // .end method
private Boolean checkProcessRecord ( com.miui.server.smartpower.IAppState$IRunningProcess p0 ) {
	 /* .locals 2 */
	 /* .param p1, "proc" # Lcom/miui/server/smartpower/IAppState$IRunningProcess; */
	 /* .line 223 */
	 if ( p1 != null) { // if-eqz p1, :cond_0
		 v0 = 		 (( com.miui.server.smartpower.IAppState$IRunningProcess ) p1 ).getAdj ( ); // invoke-virtual {p1}, Lcom/miui/server/smartpower/IAppState$IRunningProcess;->getAdj()I
		 /* const/16 v1, 0xc8 */
		 /* if-le v0, v1, :cond_0 */
		 /* .line 224 */
		 v0 = 		 (( com.miui.server.smartpower.IAppState$IRunningProcess ) p1 ).getPid ( ); // invoke-virtual {p1}, Lcom/miui/server/smartpower/IAppState$IRunningProcess;->getPid()I
		 /* if-lez v0, :cond_0 */
		 v0 = 		 (( com.miui.server.smartpower.IAppState$IRunningProcess ) p1 ).getUid ( ); // invoke-virtual {p1}, Lcom/miui/server/smartpower/IAppState$IRunningProcess;->getUid()I
		 v0 = 		 android.os.UserHandle .isApp ( v0 );
		 if ( v0 != null) { // if-eqz v0, :cond_0
			 /* .line 225 */
			 v0 = 			 (( com.miui.server.smartpower.IAppState$IRunningProcess ) p1 ).isKilled ( ); // invoke-virtual {p1}, Lcom/miui/server/smartpower/IAppState$IRunningProcess;->isKilled()Z
			 /* if-nez v0, :cond_0 */
			 int v0 = 1; // const/4 v0, 0x1
		 } // :cond_0
		 int v0 = 0; // const/4 v0, 0x0
		 /* .line 223 */
	 } // :goto_0
} // .end method
private void forAllCpuStats ( java.util.function.Consumer p0 ) {
	 /* .locals 4 */
	 /* .annotation system Ldalvik/annotation/Signature; */
	 /* value = { */
	 /* "(", */
	 /* "Ljava/util/function/Consumer<", */
	 /* "Lcom/android/internal/os/ProcessCpuTracker$Stats;", */
	 /* ">;)V" */
	 /* } */
} // .end annotation
/* .line 175 */
/* .local p1, "consumer":Ljava/util/function/Consumer;, "Ljava/util/function/Consumer<Lcom/android/internal/os/ProcessCpuTracker$Stats;>;" */
v0 = this.mProcessCpuTracker;
/* monitor-enter v0 */
/* .line 176 */
try { // :try_start_0
	 v1 = this.mProcessCpuTracker;
	 v1 = 	 (( com.android.internal.os.ProcessCpuTracker ) v1 ).countStats ( ); // invoke-virtual {v1}, Lcom/android/internal/os/ProcessCpuTracker;->countStats()I
	 /* .line 177 */
	 /* .local v1, "numOfStats":I */
	 int v2 = 0; // const/4 v2, 0x0
	 /* .local v2, "i":I */
} // :goto_0
/* if-ge v2, v1, :cond_0 */
/* .line 178 */
v3 = this.mProcessCpuTracker;
(( com.android.internal.os.ProcessCpuTracker ) v3 ).getStats ( v2 ); // invoke-virtual {v3, v2}, Lcom/android/internal/os/ProcessCpuTracker;->getStats(I)Lcom/android/internal/os/ProcessCpuTracker$Stats;
/* .line 177 */
/* add-int/lit8 v2, v2, 0x1 */
/* .line 180 */
} // .end local v1 # "numOfStats":I
} // .end local v2 # "i":I
} // :cond_0
/* monitor-exit v0 */
/* .line 181 */
return;
/* .line 180 */
/* :catchall_0 */
/* move-exception v1 */
/* monitor-exit v0 */
/* :try_end_0 */
/* .catchall {:try_start_0 ..:try_end_0} :catchall_0 */
/* throw v1 */
} // .end method
private void handleLimitCpuException ( Integer p0 ) {
/* .locals 23 */
/* .param p1, "type" # I */
/* .line 127 */
/* move-object/from16 v0, p0 */
android.os.SystemClock .uptimeMillis ( );
/* move-result-wide v1 */
/* iput-wide v1, v0, Lcom/miui/server/smartpower/SmartCpuPolicyManager;->mLashHandleCpuException:J */
/* .line 128 */
int v1 = 0; // const/4 v1, 0x0
(( com.miui.server.smartpower.SmartCpuPolicyManager ) v0 ).updateCpuStatsNow ( v1 ); // invoke-virtual {v0, v1}, Lcom/miui/server/smartpower/SmartCpuPolicyManager;->updateCpuStatsNow(Z)J
/* move-result-wide v2 */
/* .line 129 */
/* .local v2, "uptimeSince":J */
/* const-wide/16 v4, 0x0 */
/* cmp-long v6, v2, v4 */
/* if-gtz v6, :cond_0 */
/* .line 130 */
return;
/* .line 132 */
} // :cond_0
/* new-instance v6, Ljava/lang/StringBuilder; */
/* invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V */
final String v7 = "HandleLimitCpuException: type="; // const-string v7, "HandleLimitCpuException: type="
(( java.lang.StringBuilder ) v6 ).append ( v7 ); // invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
/* move/from16 v7, p1 */
(( java.lang.StringBuilder ) v6 ).append ( v7 ); // invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;
final String v8 = " bgcpu="; // const-string v8, " bgcpu="
(( java.lang.StringBuilder ) v6 ).append ( v8 ); // invoke-virtual {v6, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
/* iget v8, v0, Lcom/miui/server/smartpower/SmartCpuPolicyManager;->mBackgroundCpuCoreNum:I */
(( java.lang.StringBuilder ) v6 ).append ( v8 ); // invoke-virtual {v6, v8}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;
final String v8 = " over:"; // const-string v8, " over:"
(( java.lang.StringBuilder ) v6 ).append ( v8 ); // invoke-virtual {v6, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
/* .line 134 */
android.util.TimeUtils .formatDuration ( v2,v3 );
(( java.lang.StringBuilder ) v6 ).append ( v8 ); // invoke-virtual {v6, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v6 ).toString ( ); // invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
/* .line 132 */
final String v8 = "SmartPower.CpuPolicy"; // const-string v8, "SmartPower.CpuPolicy"
android.util.Slog .d ( v8,v6 );
/* .line 135 */
/* const/16 v6, 0x1d8 */
/* .line 140 */
/* .local v6, "flag":I */
v8 = this.mAppStateManager;
(( com.android.server.am.AppStateManager ) v8 ).getAllAppState ( ); // invoke-virtual {v8}, Lcom/android/server/am/AppStateManager;->getAllAppState()Ljava/util/ArrayList;
/* .line 141 */
/* .local v8, "appStateList":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/miui/server/smartpower/IAppState;>;" */
(( java.util.ArrayList ) v8 ).iterator ( ); // invoke-virtual {v8}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;
v10 = } // :goto_0
if ( v10 != null) { // if-eqz v10, :cond_9
/* check-cast v10, Lcom/miui/server/smartpower/IAppState; */
/* .line 142 */
/* .local v10, "appState":Lcom/miui/server/smartpower/IAppState; */
/* iget-wide v11, v0, Lcom/miui/server/smartpower/SmartCpuPolicyManager;->mLashHandleCpuException:J */
(( com.miui.server.smartpower.IAppState ) v10 ).getLastTopTime ( ); // invoke-virtual {v10}, Lcom/miui/server/smartpower/IAppState;->getLastTopTime()J
/* move-result-wide v13 */
/* sub-long/2addr v11, v13 */
/* .line 143 */
/* .local v11, "backgroundUpdateTime":J */
v13 = (( com.miui.server.smartpower.IAppState ) v10 ).isVsible ( ); // invoke-virtual {v10}, Lcom/miui/server/smartpower/IAppState;->isVsible()Z
/* if-nez v13, :cond_8 */
/* const-wide/16 v13, 0x753a */
/* cmp-long v13, v11, v13 */
/* if-gez v13, :cond_1 */
/* .line 145 */
/* .line 147 */
} // :cond_1
(( com.miui.server.smartpower.IAppState ) v10 ).getRunningProcessList ( ); // invoke-virtual {v10}, Lcom/miui/server/smartpower/IAppState;->getRunningProcessList()Ljava/util/ArrayList;
(( java.util.ArrayList ) v13 ).iterator ( ); // invoke-virtual {v13}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;
v14 = } // :goto_1
if ( v14 != null) { // if-eqz v14, :cond_7
/* check-cast v14, Lcom/miui/server/smartpower/IAppState$IRunningProcess; */
/* .line 148 */
/* .local v14, "runningProc":Lcom/miui/server/smartpower/IAppState$IRunningProcess; */
/* move-wide/from16 v16, v2 */
} // .end local v2 # "uptimeSince":J
/* .local v16, "uptimeSince":J */
(( com.miui.server.smartpower.IAppState$IRunningProcess ) v14 ).getCurCpuTime ( ); // invoke-virtual {v14}, Lcom/miui/server/smartpower/IAppState$IRunningProcess;->getCurCpuTime()J
/* move-result-wide v1 */
/* .line 149 */
/* .local v1, "curCpuTime":J */
(( com.miui.server.smartpower.IAppState$IRunningProcess ) v14 ).getLastCpuTime ( ); // invoke-virtual {v14}, Lcom/miui/server/smartpower/IAppState$IRunningProcess;->getLastCpuTime()J
/* move-result-wide v18 */
/* .line 150 */
/* .local v18, "lastCpuTime":J */
(( com.miui.server.smartpower.IAppState$IRunningProcess ) v14 ).setLastCpuTime ( v1, v2 ); // invoke-virtual {v14, v1, v2}, Lcom/miui/server/smartpower/IAppState$IRunningProcess;->setLastCpuTime(J)V
/* .line 152 */
/* cmp-long v3, v18, v4 */
/* if-lez v3, :cond_6 */
/* .line 153 */
v3 = /* invoke-direct {v0, v14}, Lcom/miui/server/smartpower/SmartCpuPolicyManager;->checkProcessRecord(Lcom/miui/server/smartpower/IAppState$IRunningProcess;)Z */
if ( v3 != null) { // if-eqz v3, :cond_5
/* .line 154 */
v3 = (( com.miui.server.smartpower.IAppState$IRunningProcess ) v14 ).isProcessPerceptible ( ); // invoke-virtual {v14}, Lcom/miui/server/smartpower/IAppState$IRunningProcess;->isProcessPerceptible()Z
/* if-nez v3, :cond_4 */
v3 = this.mSmartPowerPolicyManager;
/* .line 156 */
(( com.miui.server.smartpower.IAppState$IRunningProcess ) v14 ).getPackageName ( ); // invoke-virtual {v14}, Lcom/miui/server/smartpower/IAppState$IRunningProcess;->getPackageName()Ljava/lang/String;
(( com.miui.server.smartpower.IAppState$IRunningProcess ) v14 ).getProcessName ( ); // invoke-virtual {v14}, Lcom/miui/server/smartpower/IAppState$IRunningProcess;->getProcessName()Ljava/lang/String;
/* .line 155 */
v3 = (( com.miui.server.smartpower.SmartPowerPolicyManager ) v3 ).isProcessWhiteList ( v6, v4, v5 ); // invoke-virtual {v3, v6, v4, v5}, Lcom/miui/server/smartpower/SmartPowerPolicyManager;->isProcessWhiteList(ILjava/lang/String;Ljava/lang/String;)Z
if ( v3 != null) { // if-eqz v3, :cond_2
/* .line 157 */
/* move-wide/from16 v2, v16 */
int v1 = 0; // const/4 v1, 0x0
/* const-wide/16 v4, 0x0 */
/* .line 159 */
} // :cond_2
/* sub-long v3, v1, v18 */
/* iget v5, v0, Lcom/miui/server/smartpower/SmartCpuPolicyManager;->mBackgroundCpuCoreNum:I */
/* move/from16 v20, v6 */
} // .end local v6 # "flag":I
/* .local v20, "flag":I */
/* int-to-long v5, v5 */
/* div-long/2addr v3, v5 */
/* .line 160 */
/* .local v3, "cpuTimeUsed":J */
/* long-to-float v5, v3 */
/* const/high16 v6, 0x42c80000 # 100.0f */
/* mul-float/2addr v5, v6 */
/* move-wide/from16 v6, v16 */
} // .end local v16 # "uptimeSince":J
/* .local v6, "uptimeSince":J */
/* long-to-float v15, v6 */
/* div-float/2addr v5, v15 */
/* int-to-float v15, v15 */
/* cmpl-float v5, v5, v15 */
/* if-ltz v5, :cond_3 */
/* .line 162 */
/* new-instance v5, Ljava/lang/StringBuilder; */
/* invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V */
final String v15 = "cpu exception over "; // const-string v15, "cpu exception over "
(( java.lang.StringBuilder ) v5 ).append ( v15 ); // invoke-virtual {v5, v15}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
/* .line 163 */
android.util.TimeUtils .formatDuration ( v6,v7 );
(( java.lang.StringBuilder ) v5 ).append ( v15 ); // invoke-virtual {v5, v15}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
final String v15 = " used "; // const-string v15, " used "
(( java.lang.StringBuilder ) v5 ).append ( v15 ); // invoke-virtual {v5, v15}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
/* .line 164 */
android.util.TimeUtils .formatDuration ( v3,v4 );
(( java.lang.StringBuilder ) v5 ).append ( v15 ); // invoke-virtual {v5, v15}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
final String v15 = " ("; // const-string v15, " ("
(( java.lang.StringBuilder ) v5 ).append ( v15 ); // invoke-virtual {v5, v15}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
/* long-to-float v15, v3 */
/* const/high16 v17, 0x42c80000 # 100.0f */
/* mul-float v15, v15, v17 */
/* long-to-float v0, v6 */
/* div-float/2addr v15, v0 */
(( java.lang.StringBuilder ) v5 ).append ( v15 ); // invoke-virtual {v5, v15}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;
final String v5 = "%)"; // const-string v5, "%)"
(( java.lang.StringBuilder ) v0 ).append ( v5 ); // invoke-virtual {v0, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v0 ).toString ( ); // invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
/* .line 166 */
/* .local v0, "reason":Ljava/lang/String; */
com.android.server.am.SystemPressureControllerStub .getInstance ( );
/* .line 167 */
(( com.miui.server.smartpower.IAppState$IRunningProcess ) v14 ).getProcessRecord ( ); // invoke-virtual {v14}, Lcom/miui/server/smartpower/IAppState$IRunningProcess;->getProcessRecord()Lcom/android/server/am/ProcessRecord;
/* .line 166 */
/* move-wide/from16 v21, v3 */
int v3 = 0; // const/4 v3, 0x0
} // .end local v3 # "cpuTimeUsed":J
/* .local v21, "cpuTimeUsed":J */
(( com.android.server.am.SystemPressureControllerStub ) v5 ).killProcess ( v15, v3, v0 ); // invoke-virtual {v5, v15, v3, v0}, Lcom/android/server/am/SystemPressureControllerStub;->killProcess(Lcom/android/server/am/ProcessRecord;ILjava/lang/String;)J
/* .line 160 */
} // .end local v0 # "reason":Ljava/lang/String;
} // .end local v21 # "cpuTimeUsed":J
/* .restart local v3 # "cpuTimeUsed":J */
} // :cond_3
/* move-wide/from16 v21, v3 */
int v3 = 0; // const/4 v3, 0x0
/* .line 169 */
} // .end local v3 # "cpuTimeUsed":J
/* .restart local v21 # "cpuTimeUsed":J */
} // :goto_2
(( com.miui.server.smartpower.IAppState$IRunningProcess ) v14 ).setLastCpuTime ( v1, v2 ); // invoke-virtual {v14, v1, v2}, Lcom/miui/server/smartpower/IAppState$IRunningProcess;->setLastCpuTime(J)V
/* .line 170 */
} // .end local v1 # "curCpuTime":J
} // .end local v14 # "runningProc":Lcom/miui/server/smartpower/IAppState$IRunningProcess;
} // .end local v18 # "lastCpuTime":J
} // .end local v21 # "cpuTimeUsed":J
/* move-object/from16 v0, p0 */
/* move v1, v3 */
/* move-wide v2, v6 */
/* move/from16 v6, v20 */
/* const-wide/16 v4, 0x0 */
/* move/from16 v7, p1 */
/* goto/16 :goto_1 */
/* .line 154 */
} // .end local v20 # "flag":I
/* .restart local v1 # "curCpuTime":J */
/* .local v6, "flag":I */
/* .restart local v14 # "runningProc":Lcom/miui/server/smartpower/IAppState$IRunningProcess; */
/* .restart local v16 # "uptimeSince":J */
/* .restart local v18 # "lastCpuTime":J */
} // :cond_4
/* move/from16 v20, v6 */
/* move-wide/from16 v6, v16 */
int v3 = 0; // const/4 v3, 0x0
} // .end local v16 # "uptimeSince":J
/* .local v6, "uptimeSince":J */
/* .restart local v20 # "flag":I */
/* move-object/from16 v0, p0 */
/* move v1, v3 */
/* move-wide v2, v6 */
/* move/from16 v6, v20 */
/* const-wide/16 v4, 0x0 */
/* move/from16 v7, p1 */
/* goto/16 :goto_1 */
/* .line 153 */
} // .end local v20 # "flag":I
/* .local v6, "flag":I */
/* .restart local v16 # "uptimeSince":J */
} // :cond_5
/* move/from16 v20, v6 */
/* move-wide/from16 v6, v16 */
int v3 = 0; // const/4 v3, 0x0
} // .end local v16 # "uptimeSince":J
/* .local v6, "uptimeSince":J */
/* .restart local v20 # "flag":I */
/* move-object/from16 v0, p0 */
/* move v1, v3 */
/* move-wide v2, v6 */
/* move/from16 v6, v20 */
/* const-wide/16 v4, 0x0 */
/* move/from16 v7, p1 */
/* goto/16 :goto_1 */
/* .line 152 */
} // .end local v20 # "flag":I
/* .local v6, "flag":I */
/* .restart local v16 # "uptimeSince":J */
} // :cond_6
/* move/from16 v20, v6 */
/* move-wide/from16 v6, v16 */
int v3 = 0; // const/4 v3, 0x0
} // .end local v16 # "uptimeSince":J
/* .local v6, "uptimeSince":J */
/* .restart local v20 # "flag":I */
/* move-object/from16 v0, p0 */
/* move v1, v3 */
/* move-wide v2, v6 */
/* move/from16 v6, v20 */
/* const-wide/16 v4, 0x0 */
/* move/from16 v7, p1 */
/* goto/16 :goto_1 */
/* .line 147 */
} // .end local v1 # "curCpuTime":J
} // .end local v14 # "runningProc":Lcom/miui/server/smartpower/IAppState$IRunningProcess;
} // .end local v18 # "lastCpuTime":J
} // .end local v20 # "flag":I
/* .restart local v2 # "uptimeSince":J */
/* .local v6, "flag":I */
} // :cond_7
/* move/from16 v20, v6 */
/* move-wide v6, v2 */
/* move v3, v1 */
/* .line 171 */
} // .end local v2 # "uptimeSince":J
} // .end local v10 # "appState":Lcom/miui/server/smartpower/IAppState;
} // .end local v11 # "backgroundUpdateTime":J
/* .local v6, "uptimeSince":J */
/* .restart local v20 # "flag":I */
/* move-object/from16 v0, p0 */
/* move-wide v2, v6 */
/* move/from16 v6, v20 */
/* const-wide/16 v4, 0x0 */
/* move/from16 v7, p1 */
/* goto/16 :goto_0 */
/* .line 143 */
} // .end local v20 # "flag":I
/* .restart local v2 # "uptimeSince":J */
/* .local v6, "flag":I */
/* .restart local v10 # "appState":Lcom/miui/server/smartpower/IAppState; */
/* .restart local v11 # "backgroundUpdateTime":J */
} // :cond_8
/* move/from16 v20, v6 */
/* move-wide v6, v2 */
/* move v3, v1 */
} // .end local v2 # "uptimeSince":J
/* .local v6, "uptimeSince":J */
/* .restart local v20 # "flag":I */
/* move-object/from16 v0, p0 */
/* move-wide v2, v6 */
/* move/from16 v6, v20 */
/* const-wide/16 v4, 0x0 */
/* move/from16 v7, p1 */
/* goto/16 :goto_0 */
/* .line 172 */
} // .end local v10 # "appState":Lcom/miui/server/smartpower/IAppState;
} // .end local v11 # "backgroundUpdateTime":J
} // .end local v20 # "flag":I
/* .restart local v2 # "uptimeSince":J */
/* .local v6, "flag":I */
} // :cond_9
return;
} // .end method
public static Boolean isEnableCpuLimit ( ) {
/* .locals 1 */
/* .line 67 */
int v0 = 0; // const/4 v0, 0x0
} // .end method
private void lambda$updateCpuStatsNow$0 ( Boolean p0, com.android.internal.os.ProcessCpuTracker$Stats p1 ) { //synthethic
/* .locals 5 */
/* .param p1, "isReset" # Z */
/* .param p2, "st" # Lcom/android/internal/os/ProcessCpuTracker$Stats; */
/* .line 199 */
/* iget-boolean v0, p2, Lcom/android/internal/os/ProcessCpuTracker$Stats;->working:Z */
if ( v0 != null) { // if-eqz v0, :cond_4
/* iget v0, p2, Lcom/android/internal/os/ProcessCpuTracker$Stats;->uid:I */
/* const/16 v1, 0x3e8 */
/* if-le v0, v1, :cond_4 */
/* iget v0, p2, Lcom/android/internal/os/ProcessCpuTracker$Stats;->pid:I */
/* if-gtz v0, :cond_0 */
/* .line 202 */
} // :cond_0
v0 = this.mAppStateManager;
/* iget v1, p2, Lcom/android/internal/os/ProcessCpuTracker$Stats;->uid:I */
/* iget v2, p2, Lcom/android/internal/os/ProcessCpuTracker$Stats;->pid:I */
(( com.android.server.am.AppStateManager ) v0 ).getRunningProcess ( v1, v2 ); // invoke-virtual {v0, v1, v2}, Lcom/android/server/am/AppStateManager;->getRunningProcess(II)Lcom/android/server/am/AppStateManager$AppState$RunningProcess;
/* .line 203 */
/* .local v0, "proc":Lcom/android/server/am/AppStateManager$AppState$RunningProcess; */
if ( v0 != null) { // if-eqz v0, :cond_3
v1 = (( com.android.server.am.AppStateManager$AppState$RunningProcess ) v0 ).isKilled ( ); // invoke-virtual {v0}, Lcom/android/server/am/AppStateManager$AppState$RunningProcess;->isKilled()Z
if ( v1 != null) { // if-eqz v1, :cond_1
/* .line 206 */
} // :cond_1
/* iget v1, p2, Lcom/android/internal/os/ProcessCpuTracker$Stats;->rel_utime:I */
/* iget v2, p2, Lcom/android/internal/os/ProcessCpuTracker$Stats;->rel_stime:I */
/* add-int/2addr v1, v2 */
/* int-to-long v1, v1 */
(( com.android.server.am.AppStateManager$AppState$RunningProcess ) v0 ).addAndGetCurCpuTime ( v1, v2 ); // invoke-virtual {v0, v1, v2}, Lcom/android/server/am/AppStateManager$AppState$RunningProcess;->addAndGetCurCpuTime(J)J
/* move-result-wide v1 */
/* .line 207 */
/* .local v1, "curCpuTime":J */
if ( p1 != null) { // if-eqz p1, :cond_2
/* .line 208 */
(( com.android.server.am.AppStateManager$AppState$RunningProcess ) v0 ).setLastCpuTime ( v1, v2 ); // invoke-virtual {v0, v1, v2}, Lcom/android/server/am/AppStateManager$AppState$RunningProcess;->setLastCpuTime(J)V
/* .line 210 */
} // :cond_2
/* const-wide/16 v3, 0x0 */
(( com.android.server.am.AppStateManager$AppState$RunningProcess ) v0 ).compareAndSetLastCpuTime ( v3, v4, v1, v2 ); // invoke-virtual {v0, v3, v4, v1, v2}, Lcom/android/server/am/AppStateManager$AppState$RunningProcess;->compareAndSetLastCpuTime(JJ)Z
/* .line 212 */
} // :goto_0
return;
/* .line 204 */
} // .end local v1 # "curCpuTime":J
} // :cond_3
} // :goto_1
return;
/* .line 200 */
} // .end local v0 # "proc":Lcom/android/server/am/AppStateManager$AppState$RunningProcess;
} // :cond_4
} // :goto_2
return;
} // .end method
public static android.util.IntArray parseCpuCores ( java.lang.String p0 ) {
/* .locals 9 */
/* .param p0, "cpuContent" # Ljava/lang/String; */
/* .line 86 */
final String v0 = "SmartPower.CpuPolicy"; // const-string v0, "SmartPower.CpuPolicy"
final String v1 = ","; // const-string v1, ","
/* new-instance v2, Landroid/util/IntArray; */
int v3 = 0; // const/4 v3, 0x0
/* invoke-direct {v2, v3}, Landroid/util/IntArray;-><init>(I)V */
/* .line 88 */
/* .local v2, "cpuCores":Landroid/util/IntArray; */
try { // :try_start_0
v4 = (( java.lang.String ) p0 ).contains ( v1 ); // invoke-virtual {p0, v1}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z
if ( v4 != null) { // if-eqz v4, :cond_0
(( java.lang.String ) p0 ).trim ( ); // invoke-virtual {p0}, Ljava/lang/String;->trim()Ljava/lang/String;
(( java.lang.String ) v4 ).split ( v1 ); // invoke-virtual {v4, v1}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;
/* .line 89 */
} // :cond_0
(( java.lang.String ) p0 ).trim ( ); // invoke-virtual {p0}, Ljava/lang/String;->trim()Ljava/lang/String;
final String v4 = " "; // const-string v4, " "
(( java.lang.String ) v1 ).split ( v4 ); // invoke-virtual {v1, v4}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;
} // :goto_0
/* nop */
/* .line 90 */
/* .local v1, "pairs":[Ljava/lang/String; */
int v4 = 0; // const/4 v4, 0x0
/* .local v4, "j":I */
} // :goto_1
/* array-length v5, v1 */
/* if-ge v4, v5, :cond_5 */
/* .line 91 */
/* aget-object v5, v1, v4 */
final String v6 = "-"; // const-string v6, "-"
(( java.lang.String ) v5 ).split ( v6 ); // invoke-virtual {v5, v6}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;
/* .line 92 */
/* .local v5, "minMaxPairs":[Ljava/lang/String; */
/* array-length v6, v5 */
int v7 = 2; // const/4 v7, 0x2
int v8 = 1; // const/4 v8, 0x1
/* if-lt v6, v7, :cond_3 */
/* .line 93 */
/* aget-object v6, v5, v3 */
v6 = java.lang.Integer .parseInt ( v6 );
/* .line 94 */
/* .local v6, "min":I */
/* aget-object v7, v5, v8 */
v7 = java.lang.Integer .parseInt ( v7 );
/* .line 95 */
/* .local v7, "max":I */
/* if-le v6, v7, :cond_1 */
/* .line 96 */
/* .line 98 */
} // :cond_1
/* move v8, v6 */
/* .local v8, "id":I */
} // :goto_2
/* if-gt v8, v7, :cond_2 */
/* .line 99 */
(( android.util.IntArray ) v2 ).add ( v8 ); // invoke-virtual {v2, v8}, Landroid/util/IntArray;->add(I)V
/* .line 98 */
/* add-int/lit8 v8, v8, 0x1 */
/* .line 101 */
} // .end local v6 # "min":I
} // .end local v7 # "max":I
} // .end local v8 # "id":I
} // :cond_2
} // :cond_3
/* array-length v6, v5 */
/* if-ne v6, v8, :cond_4 */
/* .line 102 */
/* aget-object v6, v5, v3 */
v6 = java.lang.Integer .parseInt ( v6 );
(( android.util.IntArray ) v2 ).add ( v6 ); // invoke-virtual {v2, v6}, Landroid/util/IntArray;->add(I)V
/* .line 104 */
} // :cond_4
/* new-instance v6, Ljava/lang/StringBuilder; */
/* invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V */
final String v7 = "Invalid CPU core range format "; // const-string v7, "Invalid CPU core range format "
(( java.lang.StringBuilder ) v6 ).append ( v7 ); // invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
/* aget-object v7, v1, v4 */
(( java.lang.StringBuilder ) v6 ).append ( v7 ); // invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v6 ).toString ( ); // invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
android.util.Slog .i ( v0,v6 );
/* :try_end_0 */
/* .catch Ljava/lang/Exception; {:try_start_0 ..:try_end_0} :catch_0 */
/* .line 90 */
} // .end local v5 # "minMaxPairs":[Ljava/lang/String;
} // :goto_3
/* add-int/lit8 v4, v4, 0x1 */
/* .line 109 */
} // .end local v1 # "pairs":[Ljava/lang/String;
} // .end local v4 # "j":I
} // :cond_5
/* .line 107 */
/* :catch_0 */
/* move-exception v1 */
/* .line 108 */
/* .local v1, "e":Ljava/lang/Exception; */
/* new-instance v3, Ljava/lang/StringBuilder; */
/* invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V */
final String v4 = "Failed to parse CPU cores from "; // const-string v4, "Failed to parse CPU cores from "
(( java.lang.StringBuilder ) v3 ).append ( v4 ); // invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v3 ).append ( p0 ); // invoke-virtual {v3, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v3 ).toString ( ); // invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
android.util.Slog .i ( v0,v3 );
/* .line 110 */
} // .end local v1 # "e":Ljava/lang/Exception;
} // :goto_4
} // .end method
/* # virtual methods */
public void cpuExceptionEvents ( Integer p0 ) {
/* .locals 4 */
/* .param p1, "type" # I */
/* .line 117 */
/* sget-boolean v0, Landroid/os/spc/PressureStateSettings;->PROC_CPU_EXCEPTION_ENABLE:Z */
if ( v0 != null) { // if-eqz v0, :cond_0
/* .line 118 */
android.os.SystemClock .uptimeMillis ( );
/* move-result-wide v0 */
/* iget-wide v2, p0, Lcom/miui/server/smartpower/SmartCpuPolicyManager;->mLashHandleCpuException:J */
/* sub-long/2addr v0, v2 */
/* const-wide/16 v2, 0x7530 */
/* cmp-long v0, v0, v2 */
/* if-lez v0, :cond_0 */
v0 = this.mHandler;
if ( v0 != null) { // if-eqz v0, :cond_0
/* .line 119 */
int v1 = 7; // const/4 v1, 0x7
v0 = (( com.miui.server.smartpower.SmartCpuPolicyManager$H ) v0 ).hasMessages ( v1 ); // invoke-virtual {v0, v1}, Lcom/miui/server/smartpower/SmartCpuPolicyManager$H;->hasMessages(I)Z
/* if-nez v0, :cond_0 */
/* .line 120 */
v0 = this.mHandler;
(( com.miui.server.smartpower.SmartCpuPolicyManager$H ) v0 ).obtainMessage ( v1 ); // invoke-virtual {v0, v1}, Lcom/miui/server/smartpower/SmartCpuPolicyManager$H;->obtainMessage(I)Landroid/os/Message;
/* .line 121 */
/* .local v0, "msg":Landroid/os/Message; */
/* iput p1, v0, Landroid/os/Message;->arg1:I */
/* .line 122 */
v1 = this.mHandler;
(( com.miui.server.smartpower.SmartCpuPolicyManager$H ) v1 ).sendMessage ( v0 ); // invoke-virtual {v1, v0}, Lcom/miui/server/smartpower/SmartCpuPolicyManager$H;->sendMessage(Landroid/os/Message;)Z
/* .line 124 */
} // .end local v0 # "msg":Landroid/os/Message;
} // :cond_0
return;
} // .end method
public void cpuPressureEvents ( Integer p0 ) {
/* .locals 0 */
/* .param p1, "level" # I */
/* .line 114 */
return;
} // .end method
public Integer getBackgroundCpuCoreNum ( ) {
/* .locals 1 */
/* .line 72 */
/* iget v0, p0, Lcom/miui/server/smartpower/SmartCpuPolicyManager;->mBackgroundCpuCoreNum:I */
} // .end method
public void init ( com.android.server.am.AppStateManager p0, com.miui.server.smartpower.SmartPowerPolicyManager p1 ) {
/* .locals 2 */
/* .param p1, "appStateManager" # Lcom/android/server/am/AppStateManager; */
/* .param p2, "smartPowerPolicyManager" # Lcom/miui/server/smartpower/SmartPowerPolicyManager; */
/* .line 60 */
this.mAppStateManager = p1;
/* .line 61 */
this.mSmartPowerPolicyManager = p2;
/* .line 62 */
v0 = this.mHandlerTh;
(( android.os.HandlerThread ) v0 ).start ( ); // invoke-virtual {v0}, Landroid/os/HandlerThread;->start()V
/* .line 63 */
/* new-instance v0, Lcom/miui/server/smartpower/SmartCpuPolicyManager$H; */
v1 = this.mHandlerTh;
(( android.os.HandlerThread ) v1 ).getLooper ( ); // invoke-virtual {v1}, Landroid/os/HandlerThread;->getLooper()Landroid/os/Looper;
/* invoke-direct {v0, p0, v1}, Lcom/miui/server/smartpower/SmartCpuPolicyManager$H;-><init>(Lcom/miui/server/smartpower/SmartCpuPolicyManager;Landroid/os/Looper;)V */
this.mHandler = v0;
/* .line 64 */
return;
} // .end method
public void updateBackgroundCpuCoreNum ( ) {
/* .locals 4 */
/* .line 76 */
com.android.server.am.SystemPressureControllerStub .getInstance ( );
(( com.android.server.am.SystemPressureControllerStub ) v0 ).getBackgroundCpuPolicy ( ); // invoke-virtual {v0}, Lcom/android/server/am/SystemPressureControllerStub;->getBackgroundCpuPolicy()Ljava/lang/String;
(( java.lang.String ) v0 ).trim ( ); // invoke-virtual {v0}, Ljava/lang/String;->trim()Ljava/lang/String;
/* .line 77 */
/* .local v0, "bgCpu":Ljava/lang/String; */
com.miui.server.smartpower.SmartCpuPolicyManager .parseCpuCores ( v0 );
/* .line 78 */
/* .local v1, "cpuCores":Landroid/util/IntArray; */
if ( v1 != null) { // if-eqz v1, :cond_1
v2 = (( android.util.IntArray ) v1 ).size ( ); // invoke-virtual {v1}, Landroid/util/IntArray;->size()I
/* if-nez v2, :cond_0 */
/* .line 82 */
} // :cond_0
v2 = (( android.util.IntArray ) v1 ).size ( ); // invoke-virtual {v1}, Landroid/util/IntArray;->size()I
/* iput v2, p0, Lcom/miui/server/smartpower/SmartCpuPolicyManager;->mBackgroundCpuCoreNum:I */
/* .line 83 */
return;
/* .line 79 */
} // :cond_1
} // :goto_0
/* new-instance v2, Ljava/lang/StringBuilder; */
/* invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V */
final String v3 = "Failed to parse CPU cores from "; // const-string v3, "Failed to parse CPU cores from "
(( java.lang.StringBuilder ) v2 ).append ( v3 ); // invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v2 ).append ( v0 ); // invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v2 ).toString ( ); // invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
final String v3 = "SmartPower.CpuPolicy"; // const-string v3, "SmartPower.CpuPolicy"
android.util.Slog .i ( v3,v2 );
/* .line 80 */
return;
} // .end method
public Long updateCpuStatsNow ( Boolean p0 ) {
/* .locals 9 */
/* .param p1, "isReset" # Z */
/* .line 184 */
/* const-wide/16 v0, 0x0 */
/* .line 185 */
/* .local v0, "uptimeSince":J */
v2 = this.mProcessCpuTracker;
/* monitor-enter v2 */
/* .line 186 */
try { // :try_start_0
android.os.SystemClock .uptimeMillis ( );
/* move-result-wide v3 */
/* .line 187 */
/* .local v3, "nowTime":J */
v5 = this.mLastUpdateCpuTime;
(( java.util.concurrent.atomic.AtomicLong ) v5 ).get ( ); // invoke-virtual {v5}, Ljava/util/concurrent/atomic/AtomicLong;->get()J
/* move-result-wide v5 */
/* sub-long v5, v3, v5 */
/* const-wide/16 v7, 0x2710 */
/* cmp-long v5, v5, v7 */
/* if-gez v5, :cond_0 */
/* .line 188 */
/* monitor-exit v2 */
/* return-wide v0 */
/* .line 190 */
} // :cond_0
final String v5 = "SmartPower.CpuPolicy"; // const-string v5, "SmartPower.CpuPolicy"
/* const-string/jumbo v6, "update process cpu time" */
android.util.Slog .d ( v5,v6 );
/* .line 191 */
(( com.miui.server.smartpower.SmartCpuPolicyManager ) p0 ).updateBackgroundCpuCoreNum ( ); // invoke-virtual {p0}, Lcom/miui/server/smartpower/SmartCpuPolicyManager;->updateBackgroundCpuCoreNum()V
/* .line 192 */
v5 = this.mProcessCpuTracker;
(( com.android.internal.os.ProcessCpuTracker ) v5 ).update ( ); // invoke-virtual {v5}, Lcom/android/internal/os/ProcessCpuTracker;->update()V
/* .line 193 */
v5 = this.mProcessCpuTracker;
v5 = (( com.android.internal.os.ProcessCpuTracker ) v5 ).hasGoodLastStats ( ); // invoke-virtual {v5}, Lcom/android/internal/os/ProcessCpuTracker;->hasGoodLastStats()Z
/* if-nez v5, :cond_1 */
/* .line 194 */
/* monitor-exit v2 */
/* return-wide v0 */
/* .line 196 */
} // :cond_1
v5 = this.mLastUpdateCpuTime;
(( java.util.concurrent.atomic.AtomicLong ) v5 ).get ( ); // invoke-virtual {v5}, Ljava/util/concurrent/atomic/AtomicLong;->get()J
/* move-result-wide v5 */
/* sub-long v0, v3, v5 */
/* .line 197 */
v5 = this.mLastUpdateCpuTime;
(( java.util.concurrent.atomic.AtomicLong ) v5 ).set ( v3, v4 ); // invoke-virtual {v5, v3, v4}, Ljava/util/concurrent/atomic/AtomicLong;->set(J)V
/* .line 198 */
/* new-instance v5, Lcom/miui/server/smartpower/SmartCpuPolicyManager$$ExternalSyntheticLambda0; */
/* invoke-direct {v5, p0, p1}, Lcom/miui/server/smartpower/SmartCpuPolicyManager$$ExternalSyntheticLambda0;-><init>(Lcom/miui/server/smartpower/SmartCpuPolicyManager;Z)V */
/* invoke-direct {p0, v5}, Lcom/miui/server/smartpower/SmartCpuPolicyManager;->forAllCpuStats(Ljava/util/function/Consumer;)V */
/* .line 213 */
} // .end local v3 # "nowTime":J
/* monitor-exit v2 */
/* .line 214 */
/* return-wide v0 */
/* .line 213 */
/* :catchall_0 */
/* move-exception v3 */
/* monitor-exit v2 */
/* :try_end_0 */
/* .catchall {:try_start_0 ..:try_end_0} :catchall_0 */
/* throw v3 */
} // .end method
