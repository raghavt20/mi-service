class com.miui.server.smartpower.SmartDisplayPolicyManager$FrameInsertionObserver extends android.database.ContentObserver {
	 /* .source "SmartDisplayPolicyManager.java" */
	 /* # annotations */
	 /* .annotation system Ldalvik/annotation/EnclosingClass; */
	 /* value = Lcom/miui/server/smartpower/SmartDisplayPolicyManager; */
} // .end annotation
/* .annotation system Ldalvik/annotation/InnerClass; */
/* accessFlags = 0x2 */
/* name = "FrameInsertionObserver" */
} // .end annotation
/* # static fields */
private static final Integer IRIS_GAME_OFF;
private static final Integer IRIS_GAME_ON_EXTERNAL_FRAME;
private static final Integer IRIS_GAME_ON_SUPER_RESOLUTION;
private static final Integer IRIS_GAME_ON_WITHIN_FRAME;
private static final Integer IRIS_GAME_ON_WITHIN_FRAME_SUPER_RESOLUTION;
public static final java.lang.String IRIS_GAME_STATUS;
private static final Integer IRIS_VIDEO_OFF;
private static final Integer IRIS_VIDEO_ON;
public static final java.lang.String IRIS_VIDEO_STATUS;
/* # instance fields */
private final android.content.ContentResolver mContentResolver;
private final android.content.Context mContext;
private final android.net.Uri mIrisGameStatusUri;
private final android.net.Uri mIrisVideoStatusUri;
final com.miui.server.smartpower.SmartDisplayPolicyManager this$0; //synthetic
/* # direct methods */
public com.miui.server.smartpower.SmartDisplayPolicyManager$FrameInsertionObserver ( ) {
/* .locals 3 */
/* .param p2, "context" # Landroid/content/Context; */
/* .param p3, "handler" # Landroid/os/Handler; */
/* .line 941 */
this.this$0 = p1;
/* .line 942 */
/* invoke-direct {p0, p3}, Landroid/database/ContentObserver;-><init>(Landroid/os/Handler;)V */
/* .line 917 */
final String p1 = "game_iris_status"; // const-string p1, "game_iris_status"
android.provider.Settings$System .getUriFor ( p1 );
this.mIrisGameStatusUri = p1;
/* .line 918 */
/* const-string/jumbo v0, "video_iris_status" */
android.provider.Settings$System .getUriFor ( v0 );
this.mIrisVideoStatusUri = v0;
/* .line 943 */
this.mContext = p2;
/* .line 944 */
(( android.content.Context ) p2 ).getContentResolver ( ); // invoke-virtual {p2}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;
this.mContentResolver = v1;
/* .line 946 */
int v2 = 0; // const/4 v2, 0x0
(( android.content.ContentResolver ) v1 ).registerContentObserver ( p1, v2, p0, v2 ); // invoke-virtual {v1, p1, v2, p0, v2}, Landroid/content/ContentResolver;->registerContentObserver(Landroid/net/Uri;ZLandroid/database/ContentObserver;I)V
/* .line 948 */
(( android.content.ContentResolver ) v1 ).registerContentObserver ( v0, v2, p0, v2 ); // invoke-virtual {v1, v0, v2, p0, v2}, Landroid/content/ContentResolver;->registerContentObserver(Landroid/net/Uri;ZLandroid/database/ContentObserver;I)V
/* .line 950 */
return;
} // .end method
private void updateFrameInsertScene ( ) {
/* .locals 2 */
/* .line 978 */
v0 = this.this$0;
v0 = com.miui.server.smartpower.SmartDisplayPolicyManager .-$$Nest$fgetmFrameInsertingForGame ( v0 );
if ( v0 != null) { // if-eqz v0, :cond_0
	 /* .line 979 */
	 v0 = this.this$0;
	 final String v1 = "game"; // const-string v1, "game"
	 com.miui.server.smartpower.SmartDisplayPolicyManager .-$$Nest$fputmFrameInsertScene ( v0,v1 );
	 /* .line 980 */
} // :cond_0
v0 = this.this$0;
v0 = com.miui.server.smartpower.SmartDisplayPolicyManager .-$$Nest$fgetmFrameInsertingForVideo ( v0 );
if ( v0 != null) { // if-eqz v0, :cond_1
	 /* .line 981 */
	 v0 = this.this$0;
	 /* const-string/jumbo v1, "video" */
	 com.miui.server.smartpower.SmartDisplayPolicyManager .-$$Nest$fputmFrameInsertScene ( v0,v1 );
	 /* .line 983 */
} // :cond_1
} // :goto_0
return;
} // .end method
private void updateIrisGameStatus ( ) {
/* .locals 5 */
/* .line 962 */
v0 = this.mContext;
(( android.content.Context ) v0 ).getContentResolver ( ); // invoke-virtual {v0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;
final String v1 = "game_iris_status"; // const-string v1, "game_iris_status"
int v2 = 0; // const/4 v2, 0x0
v0 = android.provider.Settings$System .getInt ( v0,v1,v2 );
/* .line 964 */
/* .local v0, "gameStatus":I */
v1 = this.this$0;
int v3 = 1; // const/4 v3, 0x1
/* if-eq v0, v3, :cond_0 */
int v4 = 3; // const/4 v4, 0x3
/* if-eq v0, v4, :cond_0 */
int v4 = 4; // const/4 v4, 0x4
/* if-ne v0, v4, :cond_1 */
} // :cond_0
/* move v2, v3 */
} // :cond_1
com.miui.server.smartpower.SmartDisplayPolicyManager .-$$Nest$fputmFrameInsertingForGame ( v1,v2 );
/* .line 967 */
/* invoke-direct {p0}, Lcom/miui/server/smartpower/SmartDisplayPolicyManager$FrameInsertionObserver;->updateFrameInsertScene()V */
/* .line 968 */
return;
} // .end method
private void updateIrisVideoStatus ( ) {
/* .locals 4 */
/* .line 971 */
v0 = this.mContext;
(( android.content.Context ) v0 ).getContentResolver ( ); // invoke-virtual {v0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;
/* const-string/jumbo v1, "video_iris_status" */
int v2 = 0; // const/4 v2, 0x0
v0 = android.provider.Settings$System .getInt ( v0,v1,v2 );
/* .line 973 */
/* .local v0, "videoStatus":I */
v1 = this.this$0;
int v3 = 1; // const/4 v3, 0x1
/* if-ne v0, v3, :cond_0 */
/* move v2, v3 */
} // :cond_0
com.miui.server.smartpower.SmartDisplayPolicyManager .-$$Nest$fputmFrameInsertingForVideo ( v1,v2 );
/* .line 974 */
/* invoke-direct {p0}, Lcom/miui/server/smartpower/SmartDisplayPolicyManager$FrameInsertionObserver;->updateFrameInsertScene()V */
/* .line 975 */
return;
} // .end method
/* # virtual methods */
public void onChange ( Boolean p0, android.net.Uri p1, Integer p2 ) {
/* .locals 1 */
/* .param p1, "selfChange" # Z */
/* .param p2, "uri" # Landroid/net/Uri; */
/* .param p3, "flags" # I */
/* .line 954 */
v0 = this.mIrisGameStatusUri;
v0 = (( android.net.Uri ) v0 ).equals ( p2 ); // invoke-virtual {v0, p2}, Landroid/net/Uri;->equals(Ljava/lang/Object;)Z
if ( v0 != null) { // if-eqz v0, :cond_0
/* .line 955 */
/* invoke-direct {p0}, Lcom/miui/server/smartpower/SmartDisplayPolicyManager$FrameInsertionObserver;->updateIrisGameStatus()V */
/* .line 956 */
} // :cond_0
v0 = this.mIrisVideoStatusUri;
v0 = (( android.net.Uri ) v0 ).equals ( p2 ); // invoke-virtual {v0, p2}, Landroid/net/Uri;->equals(Ljava/lang/Object;)Z
if ( v0 != null) { // if-eqz v0, :cond_1
/* .line 957 */
/* invoke-direct {p0}, Lcom/miui/server/smartpower/SmartDisplayPolicyManager$FrameInsertionObserver;->updateIrisVideoStatus()V */
/* .line 959 */
} // :cond_1
} // :goto_0
return;
} // .end method
