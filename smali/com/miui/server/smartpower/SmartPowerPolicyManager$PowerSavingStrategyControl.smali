.class Lcom/miui/server/smartpower/SmartPowerPolicyManager$PowerSavingStrategyControl;
.super Ljava/lang/Object;
.source "SmartPowerPolicyManager.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/miui/server/smartpower/SmartPowerPolicyManager;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "PowerSavingStrategyControl"
.end annotation

.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/miui/server/smartpower/SmartPowerPolicyManager$PowerSavingStrategyControl$PolicyChangeObserver;
    }
.end annotation


# static fields
.field private static final DB_AUTHORITY:Ljava/lang/String; = "com.miui.powerkeeper.configure"

.field private static final DB_COL_BG_CONTROL:Ljava/lang/String; = "bgControl"

.field private static final DB_COL_PACKAGE_NAME:Ljava/lang/String; = "pkgName"

.field private static final DB_COL_USER_ID:Ljava/lang/String; = "userId"

.field public static final DB_TABLE:Ljava/lang/String; = "userTable"

.field private static final POLICY_NO_RESTRICT:Ljava/lang/String; = "noRestrict"


# instance fields
.field private final CONTENT_URI:Landroid/net/Uri;

.field private mIsInit:Z

.field private mNoRestrictApps:Landroid/util/ArraySet;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/util/ArraySet<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field final synthetic this$0:Lcom/miui/server/smartpower/SmartPowerPolicyManager;


# direct methods
.method static bridge synthetic -$$Nest$fgetmIsInit(Lcom/miui/server/smartpower/SmartPowerPolicyManager$PowerSavingStrategyControl;)Z
    .locals 0

    iget-boolean p0, p0, Lcom/miui/server/smartpower/SmartPowerPolicyManager$PowerSavingStrategyControl;->mIsInit:Z

    return p0
.end method

.method static bridge synthetic -$$Nest$fgetmNoRestrictApps(Lcom/miui/server/smartpower/SmartPowerPolicyManager$PowerSavingStrategyControl;)Landroid/util/ArraySet;
    .locals 0

    iget-object p0, p0, Lcom/miui/server/smartpower/SmartPowerPolicyManager$PowerSavingStrategyControl;->mNoRestrictApps:Landroid/util/ArraySet;

    return-object p0
.end method

.method static bridge synthetic -$$Nest$misNoRestrictApp(Lcom/miui/server/smartpower/SmartPowerPolicyManager$PowerSavingStrategyControl;Ljava/lang/String;)Z
    .locals 0

    invoke-direct {p0, p1}, Lcom/miui/server/smartpower/SmartPowerPolicyManager$PowerSavingStrategyControl;->isNoRestrictApp(Ljava/lang/String;)Z

    move-result p0

    return p0
.end method

.method static bridge synthetic -$$Nest$mupdateAllNoRestrictApps(Lcom/miui/server/smartpower/SmartPowerPolicyManager$PowerSavingStrategyControl;)V
    .locals 0

    invoke-direct {p0}, Lcom/miui/server/smartpower/SmartPowerPolicyManager$PowerSavingStrategyControl;->updateAllNoRestrictApps()V

    return-void
.end method

.method constructor <init>(Lcom/miui/server/smartpower/SmartPowerPolicyManager;)V
    .locals 3

    .line 1680
    iput-object p1, p0, Lcom/miui/server/smartpower/SmartPowerPolicyManager$PowerSavingStrategyControl;->this$0:Lcom/miui/server/smartpower/SmartPowerPolicyManager;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1675
    nop

    .line 1676
    const-string v0, "content://com.miui.powerkeeper.configure"

    invoke-static {v0}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v0

    const-string/jumbo v1, "userTable"

    invoke-static {v0, v1}, Landroid/net/Uri;->withAppendedPath(Landroid/net/Uri;Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v0

    iput-object v0, p0, Lcom/miui/server/smartpower/SmartPowerPolicyManager$PowerSavingStrategyControl;->CONTENT_URI:Landroid/net/Uri;

    .line 1678
    new-instance v1, Landroid/util/ArraySet;

    invoke-direct {v1}, Landroid/util/ArraySet;-><init>()V

    iput-object v1, p0, Lcom/miui/server/smartpower/SmartPowerPolicyManager$PowerSavingStrategyControl;->mNoRestrictApps:Landroid/util/ArraySet;

    .line 1679
    const/4 v1, 0x0

    iput-boolean v1, p0, Lcom/miui/server/smartpower/SmartPowerPolicyManager$PowerSavingStrategyControl;->mIsInit:Z

    .line 1682
    :try_start_0
    invoke-static {p1}, Lcom/miui/server/smartpower/SmartPowerPolicyManager;->-$$Nest$fgetmContext(Lcom/miui/server/smartpower/SmartPowerPolicyManager;)Landroid/content/Context;

    move-result-object v1

    sget-object v2, Landroid/os/UserHandle;->SYSTEM:Landroid/os/UserHandle;

    invoke-virtual {v1, v2}, Landroid/content/Context;->getContentResolverForUser(Landroid/os/UserHandle;)Landroid/content/ContentResolver;

    move-result-object v1

    .line 1683
    .local v1, "cr":Landroid/content/ContentResolver;
    new-instance v2, Lcom/miui/server/smartpower/SmartPowerPolicyManager$PowerSavingStrategyControl$PolicyChangeObserver;

    invoke-static {p1}, Lcom/miui/server/smartpower/SmartPowerPolicyManager;->-$$Nest$fgetmHandler(Lcom/miui/server/smartpower/SmartPowerPolicyManager;)Landroid/os/Handler;

    move-result-object p1

    invoke-direct {v2, p0, p1}, Lcom/miui/server/smartpower/SmartPowerPolicyManager$PowerSavingStrategyControl$PolicyChangeObserver;-><init>(Lcom/miui/server/smartpower/SmartPowerPolicyManager$PowerSavingStrategyControl;Landroid/os/Handler;)V

    const/4 p1, 0x1

    invoke-virtual {v1, v0, p1, v2}, Landroid/content/ContentResolver;->registerContentObserver(Landroid/net/Uri;ZLandroid/database/ContentObserver;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 1686
    .end local v1    # "cr":Landroid/content/ContentResolver;
    goto :goto_0

    .line 1684
    :catch_0
    move-exception p1

    .line 1685
    .local p1, "e":Ljava/lang/Exception;
    const-string v0, "SmartPower.PowerPolicy"

    invoke-virtual {p1}, Ljava/lang/Exception;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Slog;->w(Ljava/lang/String;Ljava/lang/String;)I

    .line 1687
    .end local p1    # "e":Ljava/lang/Exception;
    :goto_0
    return-void
.end method

.method private isNoRestrictApp(Ljava/lang/String;)Z
    .locals 1
    .param p1, "packageName"    # Ljava/lang/String;

    .line 1729
    iget-object v0, p0, Lcom/miui/server/smartpower/SmartPowerPolicyManager$PowerSavingStrategyControl;->mNoRestrictApps:Landroid/util/ArraySet;

    invoke-virtual {v0, p1}, Landroid/util/ArraySet;->contains(Ljava/lang/Object;)Z

    move-result v0

    return v0
.end method

.method private updateAllNoRestrictApps()V
    .locals 10

    .line 1733
    const-string v0, "pkgName"

    filled-new-array {v0}, [Ljava/lang/String;

    move-result-object v3

    .line 1734
    .local v3, "projection":[Ljava/lang/String;
    const-string/jumbo v7, "userId = ? AND bgControl = ?"

    .line 1735
    .local v7, "selection":Ljava/lang/String;
    nop

    .line 1736
    const/4 v8, 0x0

    invoke-static {v8}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v1

    const-string v2, "noRestrict"

    filled-new-array {v1, v2}, [Ljava/lang/String;

    move-result-object v5

    .line 1737
    .local v5, "selectionArgs":[Ljava/lang/String;
    iget-object v1, p0, Lcom/miui/server/smartpower/SmartPowerPolicyManager$PowerSavingStrategyControl;->this$0:Lcom/miui/server/smartpower/SmartPowerPolicyManager;

    invoke-static {v1}, Lcom/miui/server/smartpower/SmartPowerPolicyManager;->-$$Nest$fgetmContext(Lcom/miui/server/smartpower/SmartPowerPolicyManager;)Landroid/content/Context;

    move-result-object v1

    sget-object v2, Landroid/os/UserHandle;->SYSTEM:Landroid/os/UserHandle;

    invoke-virtual {v1, v2}, Landroid/content/Context;->getContentResolverForUser(Landroid/os/UserHandle;)Landroid/content/ContentResolver;

    move-result-object v9

    .line 1738
    .local v9, "cr":Landroid/content/ContentResolver;
    iget-object v2, p0, Lcom/miui/server/smartpower/SmartPowerPolicyManager$PowerSavingStrategyControl;->CONTENT_URI:Landroid/net/Uri;

    const/4 v6, 0x0

    move-object v1, v9

    move-object v4, v7

    invoke-virtual/range {v1 .. v6}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v1

    .line 1739
    .local v1, "cursor":Landroid/database/Cursor;
    if-nez v1, :cond_0

    .line 1740
    return-void

    .line 1742
    :cond_0
    const/4 v2, 0x1

    iput-boolean v2, p0, Lcom/miui/server/smartpower/SmartPowerPolicyManager$PowerSavingStrategyControl;->mIsInit:Z

    .line 1743
    iget-object v2, p0, Lcom/miui/server/smartpower/SmartPowerPolicyManager$PowerSavingStrategyControl;->mNoRestrictApps:Landroid/util/ArraySet;

    invoke-virtual {v2}, Landroid/util/ArraySet;->clear()V

    .line 1745
    :try_start_0
    invoke-interface {v1, v0}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v0

    .line 1746
    .local v0, "bgPkgIndex":I
    :goto_0
    invoke-interface {v1}, Landroid/database/Cursor;->moveToNext()Z

    move-result v2

    if-eqz v2, :cond_1

    .line 1747
    invoke-interface {v1, v0}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v2

    .line 1748
    .local v2, "app":Ljava/lang/String;
    iget-object v4, p0, Lcom/miui/server/smartpower/SmartPowerPolicyManager$PowerSavingStrategyControl;->mNoRestrictApps:Landroid/util/ArraySet;

    invoke-virtual {v4, v2}, Landroid/util/ArraySet;->add(Ljava/lang/Object;)Z
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 1749
    nop

    .end local v2    # "app":Ljava/lang/String;
    goto :goto_0

    .line 1746
    .end local v0    # "bgPkgIndex":I
    :cond_1
    goto :goto_1

    .line 1753
    :catchall_0
    move-exception v0

    goto :goto_2

    .line 1750
    :catch_0
    move-exception v0

    .line 1751
    .local v0, "e":Ljava/lang/Exception;
    :try_start_1
    iput-boolean v8, p0, Lcom/miui/server/smartpower/SmartPowerPolicyManager$PowerSavingStrategyControl;->mIsInit:Z
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 1753
    .end local v0    # "e":Ljava/lang/Exception;
    :goto_1
    invoke-interface {v1}, Landroid/database/Cursor;->close()V

    .line 1754
    nop

    .line 1755
    return-void

    .line 1753
    :goto_2
    invoke-interface {v1}, Landroid/database/Cursor;->close()V

    .line 1754
    throw v0
.end method
