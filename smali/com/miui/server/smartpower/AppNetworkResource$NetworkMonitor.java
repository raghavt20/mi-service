class com.miui.server.smartpower.AppNetworkResource$NetworkMonitor {
	 /* .source "AppNetworkResource.java" */
	 /* # annotations */
	 /* .annotation system Ldalvik/annotation/EnclosingClass; */
	 /* value = Lcom/miui/server/smartpower/AppNetworkResource; */
} // .end annotation
/* .annotation system Ldalvik/annotation/InnerClass; */
/* accessFlags = 0x0 */
/* name = "NetworkMonitor" */
} // .end annotation
/* # instance fields */
Integer mActiveSeconds;
Integer mInactiveSeconds;
Boolean mIsActive;
Long mLastTimeStamp;
Integer mUid;
Long mlastTotalKiloBytes;
final com.miui.server.smartpower.AppNetworkResource this$0; //synthetic
/* # direct methods */
static void -$$Nest$mupdateNetworkStatus ( com.miui.server.smartpower.AppNetworkResource$NetworkMonitor p0 ) { //bridge//synthethic
/* .locals 0 */
/* invoke-direct {p0}, Lcom/miui/server/smartpower/AppNetworkResource$NetworkMonitor;->updateNetworkStatus()V */
return;
} // .end method
 com.miui.server.smartpower.AppNetworkResource$NetworkMonitor ( ) {
/* .locals 4 */
/* .param p1, "this$0" # Lcom/miui/server/smartpower/AppNetworkResource; */
/* .param p2, "uid" # I */
/* .line 106 */
this.this$0 = p1;
/* invoke-direct {p0}, Ljava/lang/Object;-><init>()V */
/* .line 102 */
int v0 = 0; // const/4 v0, 0x0
/* iput v0, p0, Lcom/miui/server/smartpower/AppNetworkResource$NetworkMonitor;->mActiveSeconds:I */
/* .line 103 */
/* iput v0, p0, Lcom/miui/server/smartpower/AppNetworkResource$NetworkMonitor;->mInactiveSeconds:I */
/* .line 104 */
/* iput-boolean v0, p0, Lcom/miui/server/smartpower/AppNetworkResource$NetworkMonitor;->mIsActive:Z */
/* .line 107 */
/* iput p2, p0, Lcom/miui/server/smartpower/AppNetworkResource$NetworkMonitor;->mUid:I */
/* .line 108 */
java.lang.System .currentTimeMillis ( );
/* move-result-wide v0 */
/* iput-wide v0, p0, Lcom/miui/server/smartpower/AppNetworkResource$NetworkMonitor;->mLastTimeStamp:J */
/* .line 109 */
/* invoke-direct {p0}, Lcom/miui/server/smartpower/AppNetworkResource$NetworkMonitor;->getUidTxBytes()J */
/* move-result-wide v0 */
/* const-wide/16 v2, 0x400 */
/* div-long/2addr v0, v2 */
/* iput-wide v0, p0, Lcom/miui/server/smartpower/AppNetworkResource$NetworkMonitor;->mlastTotalKiloBytes:J */
/* .line 110 */
com.miui.server.smartpower.AppNetworkResource .-$$Nest$fgetmHandler ( p1 );
int v1 = 1; // const/4 v1, 0x1
(( android.os.Handler ) v0 ).obtainMessage ( v1, p0 ); // invoke-virtual {v0, v1, p0}, Landroid/os/Handler;->obtainMessage(ILjava/lang/Object;)Landroid/os/Message;
/* .line 111 */
/* .local v0, "nextMsg":Landroid/os/Message; */
com.miui.server.smartpower.AppNetworkResource .-$$Nest$fgetmHandler ( p1 );
/* sget-wide v2, Lcom/miui/app/smartpower/SmartPowerSettings;->DEF_RES_NET_MONITOR_PERIOD:J */
(( android.os.Handler ) v1 ).sendMessageDelayed ( v0, v2, v3 ); // invoke-virtual {v1, v0, v2, v3}, Landroid/os/Handler;->sendMessageDelayed(Landroid/os/Message;J)Z
/* .line 112 */
return;
} // .end method
private Long getUidTxBytes ( ) {
/* .locals 4 */
/* .line 144 */
/* iget v0, p0, Lcom/miui/server/smartpower/AppNetworkResource$NetworkMonitor;->mUid:I */
android.net.TrafficStats .getUidTxBytes ( v0 );
/* move-result-wide v0 */
/* iget v2, p0, Lcom/miui/server/smartpower/AppNetworkResource$NetworkMonitor;->mUid:I */
android.net.TrafficStats .getUidRxBytes ( v2 );
/* move-result-wide v2 */
/* add-long/2addr v0, v2 */
/* return-wide v0 */
} // .end method
private void updateNetworkStatus ( ) {
/* .locals 13 */
/* .line 115 */
/* invoke-direct {p0}, Lcom/miui/server/smartpower/AppNetworkResource$NetworkMonitor;->getUidTxBytes()J */
/* move-result-wide v0 */
/* const-wide/16 v2, 0x400 */
/* div-long/2addr v0, v2 */
/* .line 116 */
/* .local v0, "currentTotalKiloBytes":J */
java.lang.System .currentTimeMillis ( );
/* move-result-wide v2 */
/* .line 117 */
/* .local v2, "now":J */
/* iget-wide v4, p0, Lcom/miui/server/smartpower/AppNetworkResource$NetworkMonitor;->mLastTimeStamp:J */
/* sub-long v4, v2, v4 */
/* .line 118 */
/* .local v4, "duration":J */
/* const-wide/16 v6, 0x0 */
/* cmp-long v6, v4, v6 */
/* if-gtz v6, :cond_0 */
return;
/* .line 119 */
} // :cond_0
/* iget-wide v6, p0, Lcom/miui/server/smartpower/AppNetworkResource$NetworkMonitor;->mlastTotalKiloBytes:J */
/* sub-long v6, v0, v6 */
/* const-wide/16 v8, 0x3e8 */
/* mul-long/2addr v6, v8 */
/* div-long/2addr v6, v4 */
/* .line 121 */
/* .local v6, "speed":J */
/* sget-boolean v8, Lcom/miui/server/smartpower/AppPowerResource;->DEBUG:Z */
if ( v8 != null) { // if-eqz v8, :cond_1
/* new-instance v8, Ljava/lang/StringBuilder; */
/* invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V */
/* iget v9, p0, Lcom/miui/server/smartpower/AppNetworkResource$NetworkMonitor;->mUid:I */
(( java.lang.StringBuilder ) v8 ).append ( v9 ); // invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;
final String v9 = " speed "; // const-string v9, " speed "
(( java.lang.StringBuilder ) v8 ).append ( v9 ); // invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v8 ).append ( v6, v7 ); // invoke-virtual {v8, v6, v7}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;
final String v9 = "kb/s"; // const-string v9, "kb/s"
(( java.lang.StringBuilder ) v8 ).append ( v9 ); // invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v8 ).toString ( ); // invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
final String v9 = "SmartPower.AppResource"; // const-string v9, "SmartPower.AppResource"
android.util.Slog .d ( v9,v8 );
/* .line 122 */
} // :cond_1
/* sget-wide v8, Lcom/miui/app/smartpower/SmartPowerSettings;->DEF_RES_NET_ACTIVE_SPEED:J */
/* cmp-long v8, v6, v8 */
/* const/16 v9, 0x80 */
int v10 = 4; // const/4 v10, 0x4
int v11 = 0; // const/4 v11, 0x0
int v12 = 1; // const/4 v12, 0x1
/* if-lez v8, :cond_2 */
/* .line 123 */
/* iget v8, p0, Lcom/miui/server/smartpower/AppNetworkResource$NetworkMonitor;->mActiveSeconds:I */
/* add-int/2addr v8, v12 */
/* iput v8, p0, Lcom/miui/server/smartpower/AppNetworkResource$NetworkMonitor;->mActiveSeconds:I */
/* .line 124 */
/* iput v11, p0, Lcom/miui/server/smartpower/AppNetworkResource$NetworkMonitor;->mInactiveSeconds:I */
/* .line 125 */
/* add-int/lit8 v11, v8, 0x1 */
/* iput v11, p0, Lcom/miui/server/smartpower/AppNetworkResource$NetworkMonitor;->mActiveSeconds:I */
/* if-le v8, v10, :cond_3 */
/* .line 126 */
/* iput-boolean v12, p0, Lcom/miui/server/smartpower/AppNetworkResource$NetworkMonitor;->mIsActive:Z */
/* .line 127 */
v8 = this.this$0;
/* iget v10, p0, Lcom/miui/server/smartpower/AppNetworkResource$NetworkMonitor;->mUid:I */
(( com.miui.server.smartpower.AppNetworkResource ) v8 ).reportResourceStatus ( v10, v12, v9 ); // invoke-virtual {v8, v10, v12, v9}, Lcom/miui/server/smartpower/AppNetworkResource;->reportResourceStatus(IZI)V
/* .line 131 */
} // :cond_2
/* iget v8, p0, Lcom/miui/server/smartpower/AppNetworkResource$NetworkMonitor;->mInactiveSeconds:I */
/* add-int/2addr v8, v12 */
/* iput v8, p0, Lcom/miui/server/smartpower/AppNetworkResource$NetworkMonitor;->mInactiveSeconds:I */
/* .line 132 */
/* iput v11, p0, Lcom/miui/server/smartpower/AppNetworkResource$NetworkMonitor;->mActiveSeconds:I */
/* .line 133 */
/* add-int/lit8 v12, v8, 0x1 */
/* iput v12, p0, Lcom/miui/server/smartpower/AppNetworkResource$NetworkMonitor;->mInactiveSeconds:I */
/* if-le v8, v10, :cond_3 */
/* .line 134 */
/* iput-boolean v11, p0, Lcom/miui/server/smartpower/AppNetworkResource$NetworkMonitor;->mIsActive:Z */
/* .line 135 */
v8 = this.this$0;
/* iget v10, p0, Lcom/miui/server/smartpower/AppNetworkResource$NetworkMonitor;->mUid:I */
(( com.miui.server.smartpower.AppNetworkResource ) v8 ).reportResourceStatus ( v10, v11, v9 ); // invoke-virtual {v8, v10, v11, v9}, Lcom/miui/server/smartpower/AppNetworkResource;->reportResourceStatus(IZI)V
/* .line 139 */
} // :cond_3
} // :goto_0
/* iput-wide v0, p0, Lcom/miui/server/smartpower/AppNetworkResource$NetworkMonitor;->mlastTotalKiloBytes:J */
/* .line 140 */
/* iput-wide v2, p0, Lcom/miui/server/smartpower/AppNetworkResource$NetworkMonitor;->mLastTimeStamp:J */
/* .line 141 */
return;
} // .end method
