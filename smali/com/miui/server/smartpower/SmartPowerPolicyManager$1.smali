.class Lcom/miui/server/smartpower/SmartPowerPolicyManager$1;
.super Landroid/content/BroadcastReceiver;
.source "SmartPowerPolicyManager.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/miui/server/smartpower/SmartPowerPolicyManager;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/miui/server/smartpower/SmartPowerPolicyManager;


# direct methods
.method constructor <init>(Lcom/miui/server/smartpower/SmartPowerPolicyManager;)V
    .locals 0
    .param p1, "this$0"    # Lcom/miui/server/smartpower/SmartPowerPolicyManager;

    .line 215
    iput-object p1, p0, Lcom/miui/server/smartpower/SmartPowerPolicyManager$1;->this$0:Lcom/miui/server/smartpower/SmartPowerPolicyManager;

    invoke-direct {p0}, Landroid/content/BroadcastReceiver;-><init>()V

    return-void
.end method


# virtual methods
.method public onReceive(Landroid/content/Context;Landroid/content/Intent;)V
    .locals 2
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "intent"    # Landroid/content/Intent;

    .line 218
    const-string v0, "android.intent.action.SCREEN_OFF"

    invoke-virtual {p2}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 219
    iget-object v0, p0, Lcom/miui/server/smartpower/SmartPowerPolicyManager$1;->this$0:Lcom/miui/server/smartpower/SmartPowerPolicyManager;

    const/4 v1, 0x1

    invoke-static {v0, v1}, Lcom/miui/server/smartpower/SmartPowerPolicyManager;->-$$Nest$fputmScreenOff(Lcom/miui/server/smartpower/SmartPowerPolicyManager;Z)V

    goto :goto_0

    .line 220
    :cond_0
    const-string v0, "android.intent.action.SCREEN_ON"

    invoke-virtual {p2}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 221
    iget-object v0, p0, Lcom/miui/server/smartpower/SmartPowerPolicyManager$1;->this$0:Lcom/miui/server/smartpower/SmartPowerPolicyManager;

    const/4 v1, 0x0

    invoke-static {v0, v1}, Lcom/miui/server/smartpower/SmartPowerPolicyManager;->-$$Nest$fputmScreenOff(Lcom/miui/server/smartpower/SmartPowerPolicyManager;Z)V

    goto :goto_0

    .line 222
    :cond_1
    const-string v0, "android.intent.action.BOOT_COMPLETED"

    invoke-virtual {p2}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 223
    iget-object v0, p0, Lcom/miui/server/smartpower/SmartPowerPolicyManager$1;->this$0:Lcom/miui/server/smartpower/SmartPowerPolicyManager;

    invoke-static {v0}, Lcom/miui/server/smartpower/SmartPowerPolicyManager;->-$$Nest$fgetmPowerSavingStrategyControl(Lcom/miui/server/smartpower/SmartPowerPolicyManager;)Lcom/miui/server/smartpower/SmartPowerPolicyManager$PowerSavingStrategyControl;

    move-result-object v0

    invoke-static {v0}, Lcom/miui/server/smartpower/SmartPowerPolicyManager$PowerSavingStrategyControl;->-$$Nest$fgetmIsInit(Lcom/miui/server/smartpower/SmartPowerPolicyManager$PowerSavingStrategyControl;)Z

    move-result v0

    if-nez v0, :cond_2

    .line 224
    iget-object v0, p0, Lcom/miui/server/smartpower/SmartPowerPolicyManager$1;->this$0:Lcom/miui/server/smartpower/SmartPowerPolicyManager;

    invoke-static {v0}, Lcom/miui/server/smartpower/SmartPowerPolicyManager;->-$$Nest$fgetmPowerSavingStrategyControl(Lcom/miui/server/smartpower/SmartPowerPolicyManager;)Lcom/miui/server/smartpower/SmartPowerPolicyManager$PowerSavingStrategyControl;

    move-result-object v0

    invoke-static {v0}, Lcom/miui/server/smartpower/SmartPowerPolicyManager$PowerSavingStrategyControl;->-$$Nest$mupdateAllNoRestrictApps(Lcom/miui/server/smartpower/SmartPowerPolicyManager$PowerSavingStrategyControl;)V

    .line 227
    :cond_2
    :goto_0
    return-void
.end method
