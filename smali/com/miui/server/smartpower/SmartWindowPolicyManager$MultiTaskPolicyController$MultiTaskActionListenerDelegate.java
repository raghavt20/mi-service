class com.miui.server.smartpower.SmartWindowPolicyManager$MultiTaskPolicyController$MultiTaskActionListenerDelegate extends android.os.Handler {
	 /* .source "SmartWindowPolicyManager.java" */
	 /* # annotations */
	 /* .annotation system Ldalvik/annotation/EnclosingClass; */
	 /* value = Lcom/miui/server/smartpower/SmartWindowPolicyManager$MultiTaskPolicyController; */
} // .end annotation
/* .annotation system Ldalvik/annotation/InnerClass; */
/* accessFlags = 0xa */
/* name = "MultiTaskActionListenerDelegate" */
} // .end annotation
/* # instance fields */
private com.miui.app.smartpower.SmartPowerServiceInternal$MultiTaskActionListener mListener;
private java.lang.String mSourceName;
/* # direct methods */
public com.miui.server.smartpower.SmartWindowPolicyManager$MultiTaskPolicyController$MultiTaskActionListenerDelegate ( ) {
/* .locals 1 */
/* .param p1, "sourceName" # Ljava/lang/String; */
/* .param p2, "looper" # Landroid/os/Looper; */
/* .param p3, "listener" # Lcom/miui/app/smartpower/SmartPowerServiceInternal$MultiTaskActionListener; */
/* .line 289 */
/* invoke-direct {p0, p2}, Landroid/os/Handler;-><init>(Landroid/os/Looper;)V */
/* .line 284 */
int v0 = 0; // const/4 v0, 0x0
this.mSourceName = v0;
/* .line 285 */
this.mListener = v0;
/* .line 290 */
this.mSourceName = p1;
/* .line 291 */
this.mListener = p3;
/* .line 292 */
return;
} // .end method
/* # virtual methods */
public void clearEvents ( ) {
/* .locals 1 */
/* .line 300 */
int v0 = 0; // const/4 v0, 0x0
(( com.miui.server.smartpower.SmartWindowPolicyManager$MultiTaskPolicyController$MultiTaskActionListenerDelegate ) p0 ).removeCallbacksAndMessages ( v0 ); // invoke-virtual {p0, v0}, Lcom/miui/server/smartpower/SmartWindowPolicyManager$MultiTaskPolicyController$MultiTaskActionListenerDelegate;->removeCallbacksAndMessages(Ljava/lang/Object;)V
/* .line 301 */
return;
} // .end method
public com.miui.app.smartpower.SmartPowerServiceInternal$MultiTaskActionListener getListener ( ) {
/* .locals 1 */
/* .line 326 */
v0 = this.mListener;
} // .end method
public java.lang.String getSourceName ( ) {
/* .locals 1 */
/* .line 322 */
v0 = this.mSourceName;
} // .end method
public void handleMessage ( android.os.Message p0 ) {
/* .locals 2 */
/* .param p1, "msg" # Landroid/os/Message; */
/* .line 305 */
/* iget v0, p1, Landroid/os/Message;->what:I */
/* packed-switch v0, :pswitch_data_0 */
/* .line 312 */
/* :pswitch_0 */
v0 = this.obj;
/* check-cast v0, Lmiui/smartpower/MultiTaskActionManager$ActionInfo; */
/* .line 313 */
/* .local v0, "actionInfo":Lmiui/smartpower/MultiTaskActionManager$ActionInfo; */
v1 = this.mListener;
/* .line 314 */
/* .line 307 */
} // .end local v0 # "actionInfo":Lmiui/smartpower/MultiTaskActionManager$ActionInfo;
/* :pswitch_1 */
v0 = this.obj;
/* check-cast v0, Lmiui/smartpower/MultiTaskActionManager$ActionInfo; */
/* .line 308 */
/* .restart local v0 # "actionInfo":Lmiui/smartpower/MultiTaskActionManager$ActionInfo; */
v1 = this.mListener;
/* .line 309 */
/* nop */
/* .line 319 */
} // .end local v0 # "actionInfo":Lmiui/smartpower/MultiTaskActionManager$ActionInfo;
} // :goto_0
return;
/* nop */
/* :pswitch_data_0 */
/* .packed-switch 0x1 */
/* :pswitch_1 */
/* :pswitch_0 */
} // .end packed-switch
} // .end method
public void sendMultiTaskActionEvent ( Integer p0, miui.smartpower.MultiTaskActionManager$ActionInfo p1 ) {
/* .locals 1 */
/* .param p1, "event" # I */
/* .param p2, "actionInfo" # Lmiui/smartpower/MultiTaskActionManager$ActionInfo; */
/* .line 295 */
(( com.miui.server.smartpower.SmartWindowPolicyManager$MultiTaskPolicyController$MultiTaskActionListenerDelegate ) p0 ).obtainMessage ( p1, p2 ); // invoke-virtual {p0, p1, p2}, Lcom/miui/server/smartpower/SmartWindowPolicyManager$MultiTaskPolicyController$MultiTaskActionListenerDelegate;->obtainMessage(ILjava/lang/Object;)Landroid/os/Message;
/* .line 296 */
/* .local v0, "msg":Landroid/os/Message; */
(( com.miui.server.smartpower.SmartWindowPolicyManager$MultiTaskPolicyController$MultiTaskActionListenerDelegate ) p0 ).sendMessage ( v0 ); // invoke-virtual {p0, v0}, Lcom/miui/server/smartpower/SmartWindowPolicyManager$MultiTaskPolicyController$MultiTaskActionListenerDelegate;->sendMessage(Landroid/os/Message;)Z
/* .line 297 */
return;
} // .end method
