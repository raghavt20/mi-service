class com.miui.server.smartpower.AppCameraResource$CameraRecord {
	 /* .source "AppCameraResource.java" */
	 /* # annotations */
	 /* .annotation system Ldalvik/annotation/EnclosingClass; */
	 /* value = Lcom/miui/server/smartpower/AppCameraResource; */
} // .end annotation
/* .annotation system Ldalvik/annotation/InnerClass; */
/* accessFlags = 0x2 */
/* name = "CameraRecord" */
} // .end annotation
/* # instance fields */
private java.lang.String mCallerPackage;
private Integer mCallerPid;
private Integer mCallerUid;
final com.miui.server.smartpower.AppCameraResource this$0; //synthetic
/* # direct methods */
static Integer -$$Nest$fgetmCallerUid ( com.miui.server.smartpower.AppCameraResource$CameraRecord p0 ) { //bridge//synthethic
/* .locals 0 */
/* iget p0, p0, Lcom/miui/server/smartpower/AppCameraResource$CameraRecord;->mCallerUid:I */
} // .end method
public com.miui.server.smartpower.AppCameraResource$CameraRecord ( ) {
/* .locals 0 */
/* .param p2, "caller" # Ljava/lang/String; */
/* .param p3, "callerUid" # I */
/* .param p4, "callerPid" # I */
/* .line 85 */
this.this$0 = p1;
/* invoke-direct {p0}, Ljava/lang/Object;-><init>()V */
/* .line 86 */
/* iput p3, p0, Lcom/miui/server/smartpower/AppCameraResource$CameraRecord;->mCallerUid:I */
/* .line 87 */
/* iput p4, p0, Lcom/miui/server/smartpower/AppCameraResource$CameraRecord;->mCallerPid:I */
/* .line 88 */
this.mCallerPackage = p2;
/* .line 89 */
return;
} // .end method
/* # virtual methods */
public void notifyCameraForegroundState ( java.lang.String p0, Boolean p1 ) {
/* .locals 4 */
/* .param p1, "cameraId" # Ljava/lang/String; */
/* .param p2, "isForeground" # Z */
/* .line 92 */
/* sget-boolean v0, Lcom/miui/server/smartpower/AppPowerResource;->DEBUG:Z */
if ( v0 != null) { // if-eqz v0, :cond_0
	 /* .line 93 */
	 /* new-instance v0, Ljava/lang/StringBuilder; */
	 /* invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V */
	 final String v1 = "camera: active="; // const-string v1, "camera: active="
	 (( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
	 (( java.lang.StringBuilder ) v0 ).append ( p2 ); // invoke-virtual {v0, p2}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;
	 final String v1 = " cameraId="; // const-string v1, " cameraId="
	 (( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
	 (( java.lang.StringBuilder ) v0 ).append ( p1 ); // invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
	 final String v1 = " pkg="; // const-string v1, " pkg="
	 (( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
	 v1 = this.mCallerPackage;
	 (( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
	 final String v1 = " uid="; // const-string v1, " uid="
	 (( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
	 /* iget v1, p0, Lcom/miui/server/smartpower/AppCameraResource$CameraRecord;->mCallerUid:I */
	 (( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;
	 final String v1 = " pid="; // const-string v1, " pid="
	 (( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
	 /* iget v1, p0, Lcom/miui/server/smartpower/AppCameraResource$CameraRecord;->mCallerPid:I */
	 (( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;
	 (( java.lang.StringBuilder ) v0 ).toString ( ); // invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
	 final String v1 = "SmartPower.AppResource"; // const-string v1, "SmartPower.AppResource"
	 android.util.Slog .d ( v1,v0 );
	 /* .line 99 */
} // :cond_0
v0 = this.this$0;
/* iget v1, p0, Lcom/miui/server/smartpower/AppCameraResource$CameraRecord;->mCallerUid:I */
/* iget v2, p0, Lcom/miui/server/smartpower/AppCameraResource$CameraRecord;->mCallerPid:I */
/* const/16 v3, 0x20 */
(( com.miui.server.smartpower.AppCameraResource ) v0 ).reportResourceStatus ( v1, v2, p2, v3 ); // invoke-virtual {v0, v1, v2, p2, v3}, Lcom/miui/server/smartpower/AppCameraResource;->reportResourceStatus(IIZI)V
/* .line 101 */
v0 = this.this$0;
/* iget v1, p0, Lcom/miui/server/smartpower/AppCameraResource$CameraRecord;->mCallerUid:I */
(( com.miui.server.smartpower.AppCameraResource ) v0 ).reportResourceStatus ( v1, p2, v3 ); // invoke-virtual {v0, v1, p2, v3}, Lcom/miui/server/smartpower/AppCameraResource;->reportResourceStatus(IZI)V
/* .line 103 */
/* new-instance v0, Ljava/lang/StringBuilder; */
/* invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V */
final String v1 = "camera u:"; // const-string v1, "camera u:"
(( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
/* iget v1, p0, Lcom/miui/server/smartpower/AppCameraResource$CameraRecord;->mCallerUid:I */
(( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;
final String v1 = " p:"; // const-string v1, " p:"
(( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
/* iget v1, p0, Lcom/miui/server/smartpower/AppCameraResource$CameraRecord;->mCallerPid:I */
(( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;
final String v1 = " s:"; // const-string v1, " s:"
(( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v0 ).append ( p2 ); // invoke-virtual {v0, p2}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v0 ).toString ( ); // invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
/* const v1, 0x15ff2 */
android.util.EventLog .writeEvent ( v1,v0 );
/* .line 105 */
return;
} // .end method
