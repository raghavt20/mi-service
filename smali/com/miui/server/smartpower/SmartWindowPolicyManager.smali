.class public Lcom/miui/server/smartpower/SmartWindowPolicyManager;
.super Ljava/lang/Object;
.source "SmartWindowPolicyManager.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/miui/server/smartpower/SmartWindowPolicyManager$MultiTaskPolicyController;,
        Lcom/miui/server/smartpower/SmartWindowPolicyManager$H;
    }
.end annotation


# static fields
.field public static final DEBUG:Z

.field private static final EVENT_MULTI_TASK_ACTION_END:I = 0x2

.field private static final EVENT_MULTI_TASK_ACTION_START:I = 0x1

.field private static final MSG_MULTI_TASK_ACTION_CHANGED:I = 0x1

.field public static final TAG:Ljava/lang/String; = "SmartPower.WindowPolicy"

.field public static sEnable:Z


# instance fields
.field private final mContext:Landroid/content/Context;

.field private final mHandler:Lcom/miui/server/smartpower/SmartWindowPolicyManager$H;

.field private mMultiTaskPolicyController:Lcom/miui/server/smartpower/SmartWindowPolicyManager$MultiTaskPolicyController;

.field private mSmartBoostPolicyManager:Lcom/miui/server/smartpower/SmartBoostPolicyManager;

.field private mSmartDisplayPolicyManager:Lcom/miui/server/smartpower/SmartDisplayPolicyManager;

.field private final mWMS:Lcom/android/server/wm/WindowManagerService;


# direct methods
.method static bridge synthetic -$$Nest$fgetmSmartBoostPolicyManager(Lcom/miui/server/smartpower/SmartWindowPolicyManager;)Lcom/miui/server/smartpower/SmartBoostPolicyManager;
    .locals 0

    iget-object p0, p0, Lcom/miui/server/smartpower/SmartWindowPolicyManager;->mSmartBoostPolicyManager:Lcom/miui/server/smartpower/SmartBoostPolicyManager;

    return-object p0
.end method

.method static bridge synthetic -$$Nest$fgetmSmartDisplayPolicyManager(Lcom/miui/server/smartpower/SmartWindowPolicyManager;)Lcom/miui/server/smartpower/SmartDisplayPolicyManager;
    .locals 0

    iget-object p0, p0, Lcom/miui/server/smartpower/SmartWindowPolicyManager;->mSmartDisplayPolicyManager:Lcom/miui/server/smartpower/SmartDisplayPolicyManager;

    return-object p0
.end method

.method static bridge synthetic -$$Nest$mnotifyMultiTaskActionChanged(Lcom/miui/server/smartpower/SmartWindowPolicyManager;ILmiui/smartpower/MultiTaskActionManager$ActionInfo;)V
    .locals 0

    invoke-direct {p0, p1, p2}, Lcom/miui/server/smartpower/SmartWindowPolicyManager;->notifyMultiTaskActionChanged(ILmiui/smartpower/MultiTaskActionManager$ActionInfo;)V

    return-void
.end method

.method static constructor <clinit>()V
    .locals 1

    .line 24
    sget-boolean v0, Lcom/android/server/am/SmartPowerService;->DEBUG:Z

    sput-boolean v0, Lcom/miui/server/smartpower/SmartWindowPolicyManager;->DEBUG:Z

    .line 26
    sget-boolean v0, Lcom/miui/app/smartpower/SmartPowerSettings;->WINDOW_POLICY_ENABLE:Z

    sput-boolean v0, Lcom/miui/server/smartpower/SmartWindowPolicyManager;->sEnable:Z

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/os/Looper;Lcom/android/server/wm/WindowManagerService;)V
    .locals 2
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "looper"    # Landroid/os/Looper;
    .param p3, "wms"    # Lcom/android/server/wm/WindowManagerService;

    .line 41
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 37
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/miui/server/smartpower/SmartWindowPolicyManager;->mMultiTaskPolicyController:Lcom/miui/server/smartpower/SmartWindowPolicyManager$MultiTaskPolicyController;

    .line 38
    iput-object v0, p0, Lcom/miui/server/smartpower/SmartWindowPolicyManager;->mSmartDisplayPolicyManager:Lcom/miui/server/smartpower/SmartDisplayPolicyManager;

    .line 39
    iput-object v0, p0, Lcom/miui/server/smartpower/SmartWindowPolicyManager;->mSmartBoostPolicyManager:Lcom/miui/server/smartpower/SmartBoostPolicyManager;

    .line 42
    iput-object p1, p0, Lcom/miui/server/smartpower/SmartWindowPolicyManager;->mContext:Landroid/content/Context;

    .line 43
    new-instance v1, Lcom/miui/server/smartpower/SmartWindowPolicyManager$H;

    invoke-direct {v1, p0, p2}, Lcom/miui/server/smartpower/SmartWindowPolicyManager$H;-><init>(Lcom/miui/server/smartpower/SmartWindowPolicyManager;Landroid/os/Looper;)V

    iput-object v1, p0, Lcom/miui/server/smartpower/SmartWindowPolicyManager;->mHandler:Lcom/miui/server/smartpower/SmartWindowPolicyManager$H;

    .line 44
    iput-object p3, p0, Lcom/miui/server/smartpower/SmartWindowPolicyManager;->mWMS:Lcom/android/server/wm/WindowManagerService;

    .line 46
    new-instance v1, Lcom/miui/server/smartpower/SmartWindowPolicyManager$MultiTaskPolicyController;

    invoke-direct {v1, p0, v0}, Lcom/miui/server/smartpower/SmartWindowPolicyManager$MultiTaskPolicyController;-><init>(Lcom/miui/server/smartpower/SmartWindowPolicyManager;Lcom/miui/server/smartpower/SmartWindowPolicyManager$MultiTaskPolicyController-IA;)V

    iput-object v1, p0, Lcom/miui/server/smartpower/SmartWindowPolicyManager;->mMultiTaskPolicyController:Lcom/miui/server/smartpower/SmartWindowPolicyManager$MultiTaskPolicyController;

    .line 47
    return-void
.end method

.method private isEnableWindowPolicy()Z
    .locals 1

    .line 138
    sget-boolean v0, Lcom/miui/server/smartpower/SmartWindowPolicyManager;->sEnable:Z

    return v0
.end method

.method private notifyMultiTaskActionChanged(ILmiui/smartpower/MultiTaskActionManager$ActionInfo;)V
    .locals 1
    .param p1, "event"    # I
    .param p2, "actionInfo"    # Lmiui/smartpower/MultiTaskActionManager$ActionInfo;

    .line 134
    iget-object v0, p0, Lcom/miui/server/smartpower/SmartWindowPolicyManager;->mMultiTaskPolicyController:Lcom/miui/server/smartpower/SmartWindowPolicyManager$MultiTaskPolicyController;

    invoke-virtual {v0, p1, p2}, Lcom/miui/server/smartpower/SmartWindowPolicyManager$MultiTaskPolicyController;->notifyMultiTaskActionChanged(ILmiui/smartpower/MultiTaskActionManager$ActionInfo;)V

    .line 135
    return-void
.end method

.method private onMultiTaskActionChanged(ILmiui/smartpower/MultiTaskActionManager$ActionInfo;)V
    .locals 2
    .param p1, "event"    # I
    .param p2, "actionInfo"    # Lmiui/smartpower/MultiTaskActionManager$ActionInfo;

    .line 127
    iget-object v0, p0, Lcom/miui/server/smartpower/SmartWindowPolicyManager;->mHandler:Lcom/miui/server/smartpower/SmartWindowPolicyManager$H;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Lcom/miui/server/smartpower/SmartWindowPolicyManager$H;->obtainMessage(I)Landroid/os/Message;

    move-result-object v0

    .line 128
    .local v0, "message":Landroid/os/Message;
    iput p1, v0, Landroid/os/Message;->arg1:I

    .line 129
    iput-object p2, v0, Landroid/os/Message;->obj:Ljava/lang/Object;

    .line 130
    iget-object v1, p0, Lcom/miui/server/smartpower/SmartWindowPolicyManager;->mHandler:Lcom/miui/server/smartpower/SmartWindowPolicyManager$H;

    invoke-virtual {v1, v0}, Lcom/miui/server/smartpower/SmartWindowPolicyManager$H;->sendMessage(Landroid/os/Message;)Z

    .line 131
    return-void
.end method


# virtual methods
.method public dump(Ljava/io/PrintWriter;[Ljava/lang/String;I)V
    .locals 2
    .param p1, "pw"    # Ljava/io/PrintWriter;
    .param p2, "args"    # [Ljava/lang/String;
    .param p3, "opti"    # I

    .line 161
    const-string v0, "SmartWindowPolicyManager Dump:"

    invoke-virtual {p1, v0}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V

    .line 162
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "Enable: "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-direct {p0}, Lcom/miui/server/smartpower/SmartWindowPolicyManager;->isEnableWindowPolicy()Z

    move-result v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v0}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V

    .line 164
    iget-object v0, p0, Lcom/miui/server/smartpower/SmartWindowPolicyManager;->mMultiTaskPolicyController:Lcom/miui/server/smartpower/SmartWindowPolicyManager$MultiTaskPolicyController;

    if-eqz v0, :cond_0

    .line 165
    invoke-virtual {v0, p1}, Lcom/miui/server/smartpower/SmartWindowPolicyManager$MultiTaskPolicyController;->dump(Ljava/io/PrintWriter;)V

    .line 167
    :cond_0
    return-void
.end method

.method public init(Lcom/miui/server/smartpower/SmartDisplayPolicyManager;Lcom/miui/server/smartpower/SmartBoostPolicyManager;)V
    .locals 0
    .param p1, "smartDisplayPolicyManager"    # Lcom/miui/server/smartpower/SmartDisplayPolicyManager;
    .param p2, "smartBoostPolicyManager"    # Lcom/miui/server/smartpower/SmartBoostPolicyManager;

    .line 51
    iput-object p1, p0, Lcom/miui/server/smartpower/SmartWindowPolicyManager;->mSmartDisplayPolicyManager:Lcom/miui/server/smartpower/SmartDisplayPolicyManager;

    .line 52
    iput-object p2, p0, Lcom/miui/server/smartpower/SmartWindowPolicyManager;->mSmartBoostPolicyManager:Lcom/miui/server/smartpower/SmartBoostPolicyManager;

    .line 53
    return-void
.end method

.method public onMultiTaskActionEnd(Lmiui/smartpower/MultiTaskActionManager$ActionInfo;I)V
    .locals 5
    .param p1, "actionInfo"    # Lmiui/smartpower/MultiTaskActionManager$ActionInfo;
    .param p2, "callingPid"    # I

    .line 105
    invoke-direct {p0}, Lcom/miui/server/smartpower/SmartWindowPolicyManager;->isEnableWindowPolicy()Z

    move-result v0

    if-nez v0, :cond_0

    .line 106
    return-void

    .line 109
    :cond_0
    const-string v0, "SmartPower.WindowPolicy"

    if-nez p1, :cond_1

    .line 110
    const-string v1, "action info must not be null"

    invoke-static {v0, v1}, Landroid/util/Slog;->w(Ljava/lang/String;Ljava/lang/String;)I

    .line 111
    return-void

    .line 114
    :cond_1
    sget-boolean v1, Lcom/miui/server/smartpower/SmartWindowPolicyManager;->DEBUG:Z

    if-eqz v1, :cond_2

    .line 115
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "onMultiTaskActionEnd: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Slog;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 118
    :cond_2
    invoke-virtual {p1}, Lmiui/smartpower/MultiTaskActionManager$ActionInfo;->getType()I

    move-result v0

    invoke-static {v0}, Lmiui/smartpower/MultiTaskActionManager;->actionTypeToString(I)Ljava/lang/String;

    move-result-object v0

    .line 119
    .local v0, "typeString":Ljava/lang/String;
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Multi task "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    const/4 v2, 0x0

    const-wide/16 v3, 0x40

    invoke-static {v3, v4, v1, v2}, Landroid/os/Trace;->asyncTraceEnd(JLjava/lang/String;I)V

    .line 122
    invoke-virtual {p1, p2}, Lmiui/smartpower/MultiTaskActionManager$ActionInfo;->setCallingPid(I)V

    .line 123
    const/4 v1, 0x2

    invoke-direct {p0, v1, p1}, Lcom/miui/server/smartpower/SmartWindowPolicyManager;->onMultiTaskActionChanged(ILmiui/smartpower/MultiTaskActionManager$ActionInfo;)V

    .line 124
    return-void
.end method

.method public onMultiTaskActionStart(Lmiui/smartpower/MultiTaskActionManager$ActionInfo;I)V
    .locals 5
    .param p1, "actionInfo"    # Lmiui/smartpower/MultiTaskActionManager$ActionInfo;
    .param p2, "callingPid"    # I

    .line 83
    invoke-direct {p0}, Lcom/miui/server/smartpower/SmartWindowPolicyManager;->isEnableWindowPolicy()Z

    move-result v0

    if-nez v0, :cond_0

    .line 84
    return-void

    .line 87
    :cond_0
    const-string v0, "SmartPower.WindowPolicy"

    if-nez p1, :cond_1

    .line 88
    const-string v1, "action info must not be null"

    invoke-static {v0, v1}, Landroid/util/Slog;->w(Ljava/lang/String;Ljava/lang/String;)I

    .line 89
    return-void

    .line 92
    :cond_1
    sget-boolean v1, Lcom/miui/server/smartpower/SmartWindowPolicyManager;->DEBUG:Z

    if-eqz v1, :cond_2

    .line 93
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "onMultiTaskActionStart: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Slog;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 96
    :cond_2
    invoke-virtual {p1}, Lmiui/smartpower/MultiTaskActionManager$ActionInfo;->getType()I

    move-result v0

    invoke-static {v0}, Lmiui/smartpower/MultiTaskActionManager;->actionTypeToString(I)Ljava/lang/String;

    move-result-object v0

    .line 97
    .local v0, "typeString":Ljava/lang/String;
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Multi task "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    const/4 v2, 0x0

    const-wide/16 v3, 0x20

    invoke-static {v3, v4, v1, v2}, Landroid/os/Trace;->asyncTraceBegin(JLjava/lang/String;I)V

    .line 100
    invoke-virtual {p1, p2}, Lmiui/smartpower/MultiTaskActionManager$ActionInfo;->setCallingPid(I)V

    .line 101
    const/4 v1, 0x1

    invoke-direct {p0, v1, p1}, Lcom/miui/server/smartpower/SmartWindowPolicyManager;->onMultiTaskActionChanged(ILmiui/smartpower/MultiTaskActionManager$ActionInfo;)V

    .line 102
    return-void
.end method

.method public registerMultiTaskActionListener(ILandroid/os/Handler;Lcom/miui/app/smartpower/SmartPowerServiceInternal$MultiTaskActionListener;)Z
    .locals 1
    .param p1, "listenerFlag"    # I
    .param p2, "handler"    # Landroid/os/Handler;
    .param p3, "listener"    # Lcom/miui/app/smartpower/SmartPowerServiceInternal$MultiTaskActionListener;

    .line 57
    invoke-direct {p0}, Lcom/miui/server/smartpower/SmartWindowPolicyManager;->isEnableWindowPolicy()Z

    move-result v0

    if-nez v0, :cond_0

    .line 58
    const/4 v0, 0x0

    return v0

    .line 61
    :cond_0
    iget-object v0, p0, Lcom/miui/server/smartpower/SmartWindowPolicyManager;->mMultiTaskPolicyController:Lcom/miui/server/smartpower/SmartWindowPolicyManager$MultiTaskPolicyController;

    invoke-virtual {v0, p1, p2, p3}, Lcom/miui/server/smartpower/SmartWindowPolicyManager$MultiTaskPolicyController;->registerMultiTaskActionListener(ILandroid/os/Handler;Lcom/miui/app/smartpower/SmartPowerServiceInternal$MultiTaskActionListener;)Z

    move-result v0

    return v0
.end method

.method public unregisterMultiTaskActionListener(ILcom/miui/app/smartpower/SmartPowerServiceInternal$MultiTaskActionListener;)Z
    .locals 1
    .param p1, "listenerFlag"    # I
    .param p2, "listener"    # Lcom/miui/app/smartpower/SmartPowerServiceInternal$MultiTaskActionListener;

    .line 67
    invoke-direct {p0}, Lcom/miui/server/smartpower/SmartWindowPolicyManager;->isEnableWindowPolicy()Z

    move-result v0

    if-nez v0, :cond_0

    .line 68
    const/4 v0, 0x0

    return v0

    .line 71
    :cond_0
    iget-object v0, p0, Lcom/miui/server/smartpower/SmartWindowPolicyManager;->mMultiTaskPolicyController:Lcom/miui/server/smartpower/SmartWindowPolicyManager$MultiTaskPolicyController;

    invoke-virtual {v0, p1, p2}, Lcom/miui/server/smartpower/SmartWindowPolicyManager$MultiTaskPolicyController;->unregisterMultiTaskActionListener(ILcom/miui/app/smartpower/SmartPowerServiceInternal$MultiTaskActionListener;)Z

    move-result v0

    return v0
.end method
