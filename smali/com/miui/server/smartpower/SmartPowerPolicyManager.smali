.class public Lcom/miui/server/smartpower/SmartPowerPolicyManager;
.super Ljava/lang/Object;
.source "SmartPowerPolicyManager.java"

# interfaces
.implements Lcom/android/server/am/AppStateManager$IProcessStateCallback;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/miui/server/smartpower/SmartPowerPolicyManager$MyHandler;,
        Lcom/miui/server/smartpower/SmartPowerPolicyManager$PowerSavingStrategyControl;,
        Lcom/miui/server/smartpower/SmartPowerPolicyManager$WhiteListItem;,
        Lcom/miui/server/smartpower/SmartPowerPolicyManager$UsageStatsInfo;
    }
.end annotation


# static fields
.field public static final DEBUG:Z

.field public static final INVALID_PID:I = -0x1

.field public static final MSG_UPDATE_USAGESTATS:I = 0x1

.field public static final NEVER_PERIODIC_ACTIVE:J = 0x0L

.field public static final PERIODIC_ACTIVE_THRESHOLD_HIGH:I = 0x5

.field public static final REASON:Ljava/lang/String; = "SmartPowerService"

.field public static final TAG:Ljava/lang/String; = "SmartPower.PowerPolicy"

.field public static final UPDATE_USAGESTATS_DURATION:J = 0x1b7740L

.field public static final WHITE_LIST_ACTION_HIBERNATION:I = 0x2

.field public static final WHITE_LIST_ACTION_INTERCEPT_ALARM:I = 0x8

.field public static final WHITE_LIST_ACTION_INTERCEPT_PROVIDER:I = 0x20

.field public static final WHITE_LIST_ACTION_INTERCEPT_SERVICE:I = 0x10

.field public static final WHITE_LIST_ACTION_SCREENON_HIBERNATION:I = 0x4

.field public static final WHITE_LIST_TYPE_ALARM_CLOUDCONTROL:I = 0x10000

.field public static final WHITE_LIST_TYPE_ALARM_DEFAULT:I = 0x8000

.field private static final WHITE_LIST_TYPE_ALARM_MASK:I = 0x1c000

.field private static final WHITE_LIST_TYPE_ALARM_MAX:I = 0x20000

.field private static final WHITE_LIST_TYPE_ALARM_MIN:I = 0x4000

.field public static final WHITE_LIST_TYPE_BACKUP:I = 0x40

.field public static final WHITE_LIST_TYPE_CLOUDCONTROL:I = 0x4

.field public static final WHITE_LIST_TYPE_CTS:I = 0x400

.field public static final WHITE_LIST_TYPE_DEFAULT:I = 0x2

.field public static final WHITE_LIST_TYPE_DEPEND:I = 0x100

.field public static final WHITE_LIST_TYPE_EXTAUDIO:I = 0x200

.field public static final WHITE_LIST_TYPE_FREQUENTTHAW:I = 0x20

.field private static final WHITE_LIST_TYPE_HIBERNATION_MASK:I = 0x7fe

.field public static final WHITE_LIST_TYPE_PROVIDER_CLOUDCONTROL:I = 0x400000

.field public static final WHITE_LIST_TYPE_PROVIDER_DEFAULT:I = 0x200000

.field private static final WHITE_LIST_TYPE_PROVIDER_MASK:I = 0x700000

.field private static final WHITE_LIST_TYPE_PROVIDER_MAX:I = 0x800000

.field private static final WHITE_LIST_TYPE_PROVIDER_MIN:I = 0x100000

.field public static final WHITE_LIST_TYPE_SCREENON_CLOUDCONTROL:I = 0x2000

.field public static final WHITE_LIST_TYPE_SCREENON_DEFAULT:I = 0x1000

.field private static final WHITE_LIST_TYPE_SCREENON_MASK:I = 0x3800

.field private static final WHITE_LIST_TYPE_SCREENON_MAX:I = 0x4000

.field public static final WHITE_LIST_TYPE_SERVICE_CLOUDCONTROL:I = 0x80000

.field public static final WHITE_LIST_TYPE_SERVICE_DEFAULT:I = 0x40000

.field private static final WHITE_LIST_TYPE_SERVICE_MASK:I = 0xe0000

.field private static final WHITE_LIST_TYPE_SERVICE_MAX:I = 0x100000

.field private static final WHITE_LIST_TYPE_SERVICE_MIN:I = 0x20000

.field public static final WHITE_LIST_TYPE_USB:I = 0x80

.field public static final WHITE_LIST_TYPE_VPN:I = 0x8

.field public static final WHITE_LIST_TYPE_WALLPAPER:I = 0x10

.field private static final sInterceptPackageList:Landroid/util/ArraySet;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/util/ArraySet<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field private mAMS:Lcom/android/server/am/ActivityManagerService;

.field private mAppPowerResourceManager:Lcom/miui/server/smartpower/AppPowerResourceManager;

.field private mAppStateManager:Lcom/android/server/am/AppStateManager;

.field private final mBroadcastBlackList:Landroid/util/ArraySet;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/util/ArraySet<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private final mBroadcastProcessActionWhiteList:Landroid/util/ArraySet;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/util/ArraySet<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private final mBroadcastWhiteList:Landroid/util/ArraySet;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/util/ArraySet<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private mContext:Landroid/content/Context;

.field private final mDependencyMap:Landroid/util/ArrayMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/util/ArrayMap<",
            "Ljava/lang/Integer;",
            "Landroid/util/ArraySet<",
            "Ljava/lang/String;",
            ">;>;"
        }
    .end annotation
.end field

.field private mHandler:Landroid/os/Handler;

.field private mLocalDeviceIdleController:Lcom/android/server/DeviceIdleInternal;

.field private final mPackageWhiteList:Landroid/util/ArrayMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/util/ArrayMap<",
            "Ljava/lang/String;",
            "Lcom/miui/server/smartpower/SmartPowerPolicyManager$WhiteListItem;",
            ">;"
        }
    .end annotation
.end field

.field private final mPidWhiteList:Landroid/util/ArrayMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/util/ArrayMap<",
            "Ljava/lang/Integer;",
            "Lcom/miui/server/smartpower/SmartPowerPolicyManager$WhiteListItem;",
            ">;"
        }
    .end annotation
.end field

.field private mPowerFrozenManager:Lcom/miui/server/smartpower/PowerFrozenManager;

.field private mPowerSavingStrategyControl:Lcom/miui/server/smartpower/SmartPowerPolicyManager$PowerSavingStrategyControl;

.field private final mProcessWhiteList:Landroid/util/ArrayMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/util/ArrayMap<",
            "Ljava/lang/String;",
            "Lcom/miui/server/smartpower/SmartPowerPolicyManager$WhiteListItem;",
            ">;"
        }
    .end annotation
.end field

.field private final mProviderPkgBlackList:Landroid/util/ArraySet;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/util/ArraySet<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private mRunning32ProcCount:I

.field private mScreenOff:Z

.field private final mServicePkgBlackList:Landroid/util/ArraySet;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/util/ArraySet<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private final mSysStatusReceiver:Landroid/content/BroadcastReceiver;

.field private final mUidWhiteList:Landroid/util/ArrayMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/util/ArrayMap<",
            "Ljava/lang/Integer;",
            "Lcom/miui/server/smartpower/SmartPowerPolicyManager$WhiteListItem;",
            ">;"
        }
    .end annotation
.end field

.field private mUsageStats:Landroid/app/usage/UsageStatsManagerInternal;

.field private final mUsageStatsMap:Ljava/util/HashMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/HashMap<",
            "Ljava/lang/String;",
            "Lcom/miui/server/smartpower/SmartPowerPolicyManager$UsageStatsInfo;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public static synthetic $r8$lambda$Qz5QQMFII7qrSmYkw_CQ3bKW_mQ(Lcom/miui/server/smartpower/SmartPowerPolicyManager;Lcom/android/server/am/AppStateManager$AppState;)V
    .locals 0

    invoke-direct {p0, p1}, Lcom/miui/server/smartpower/SmartPowerPolicyManager;->lambda$idleApp$0(Lcom/android/server/am/AppStateManager$AppState;)V

    return-void
.end method

.method static bridge synthetic -$$Nest$fgetmAppStateManager(Lcom/miui/server/smartpower/SmartPowerPolicyManager;)Lcom/android/server/am/AppStateManager;
    .locals 0

    iget-object p0, p0, Lcom/miui/server/smartpower/SmartPowerPolicyManager;->mAppStateManager:Lcom/android/server/am/AppStateManager;

    return-object p0
.end method

.method static bridge synthetic -$$Nest$fgetmContext(Lcom/miui/server/smartpower/SmartPowerPolicyManager;)Landroid/content/Context;
    .locals 0

    iget-object p0, p0, Lcom/miui/server/smartpower/SmartPowerPolicyManager;->mContext:Landroid/content/Context;

    return-object p0
.end method

.method static bridge synthetic -$$Nest$fgetmHandler(Lcom/miui/server/smartpower/SmartPowerPolicyManager;)Landroid/os/Handler;
    .locals 0

    iget-object p0, p0, Lcom/miui/server/smartpower/SmartPowerPolicyManager;->mHandler:Landroid/os/Handler;

    return-object p0
.end method

.method static bridge synthetic -$$Nest$fgetmPowerSavingStrategyControl(Lcom/miui/server/smartpower/SmartPowerPolicyManager;)Lcom/miui/server/smartpower/SmartPowerPolicyManager$PowerSavingStrategyControl;
    .locals 0

    iget-object p0, p0, Lcom/miui/server/smartpower/SmartPowerPolicyManager;->mPowerSavingStrategyControl:Lcom/miui/server/smartpower/SmartPowerPolicyManager$PowerSavingStrategyControl;

    return-object p0
.end method

.method static bridge synthetic -$$Nest$fputmScreenOff(Lcom/miui/server/smartpower/SmartPowerPolicyManager;Z)V
    .locals 0

    iput-boolean p1, p0, Lcom/miui/server/smartpower/SmartPowerPolicyManager;->mScreenOff:Z

    return-void
.end method

.method static bridge synthetic -$$Nest$mcompactProcess(Lcom/miui/server/smartpower/SmartPowerPolicyManager;Ljava/lang/String;Lcom/android/server/am/AppStateManager$AppState$RunningProcess;)V
    .locals 0

    invoke-direct {p0, p1, p2}, Lcom/miui/server/smartpower/SmartPowerPolicyManager;->compactProcess(Ljava/lang/String;Lcom/android/server/am/AppStateManager$AppState$RunningProcess;)V

    return-void
.end method

.method static bridge synthetic -$$Nest$mforzenProcess(Lcom/miui/server/smartpower/SmartPowerPolicyManager;IILjava/lang/String;Ljava/lang/String;)Z
    .locals 0

    invoke-direct {p0, p1, p2, p3, p4}, Lcom/miui/server/smartpower/SmartPowerPolicyManager;->forzenProcess(IILjava/lang/String;Ljava/lang/String;)Z

    move-result p0

    return p0
.end method

.method static bridge synthetic -$$Nest$mupdateUsageStats(Lcom/miui/server/smartpower/SmartPowerPolicyManager;)V
    .locals 0

    invoke-direct {p0}, Lcom/miui/server/smartpower/SmartPowerPolicyManager;->updateUsageStats()V

    return-void
.end method

.method static bridge synthetic -$$Nest$smwhiteListActionToString(I)Ljava/lang/String;
    .locals 0

    invoke-static {p0}, Lcom/miui/server/smartpower/SmartPowerPolicyManager;->whiteListActionToString(I)Ljava/lang/String;

    move-result-object p0

    return-object p0
.end method

.method static bridge synthetic -$$Nest$smwhiteListTypeToString(I)Ljava/lang/String;
    .locals 0

    invoke-static {p0}, Lcom/miui/server/smartpower/SmartPowerPolicyManager;->whiteListTypeToString(I)Ljava/lang/String;

    move-result-object p0

    return-object p0
.end method

.method static constructor <clinit>()V
    .locals 1

    .line 63
    sget-boolean v0, Lcom/android/server/am/SmartPowerService;->DEBUG:Z

    sput-boolean v0, Lcom/miui/server/smartpower/SmartPowerPolicyManager;->DEBUG:Z

    .line 199
    new-instance v0, Landroid/util/ArraySet;

    invoke-direct {v0}, Landroid/util/ArraySet;-><init>()V

    sput-object v0, Lcom/miui/server/smartpower/SmartPowerPolicyManager;->sInterceptPackageList:Landroid/util/ArraySet;

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/os/Looper;Lcom/android/server/am/ActivityManagerService;Lcom/miui/server/smartpower/PowerFrozenManager;)V
    .locals 5
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "looper"    # Landroid/os/Looper;
    .param p3, "ams"    # Lcom/android/server/am/ActivityManagerService;
    .param p4, "powerFrozenManager"    # Lcom/miui/server/smartpower/PowerFrozenManager;

    .line 231
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 159
    new-instance v0, Landroid/util/ArrayMap;

    invoke-direct {v0}, Landroid/util/ArrayMap;-><init>()V

    iput-object v0, p0, Lcom/miui/server/smartpower/SmartPowerPolicyManager;->mPackageWhiteList:Landroid/util/ArrayMap;

    .line 160
    new-instance v0, Landroid/util/ArrayMap;

    invoke-direct {v0}, Landroid/util/ArrayMap;-><init>()V

    iput-object v0, p0, Lcom/miui/server/smartpower/SmartPowerPolicyManager;->mProcessWhiteList:Landroid/util/ArrayMap;

    .line 165
    new-instance v0, Landroid/util/ArrayMap;

    invoke-direct {v0}, Landroid/util/ArrayMap;-><init>()V

    iput-object v0, p0, Lcom/miui/server/smartpower/SmartPowerPolicyManager;->mUidWhiteList:Landroid/util/ArrayMap;

    .line 166
    new-instance v0, Landroid/util/ArrayMap;

    invoke-direct {v0}, Landroid/util/ArrayMap;-><init>()V

    iput-object v0, p0, Lcom/miui/server/smartpower/SmartPowerPolicyManager;->mPidWhiteList:Landroid/util/ArrayMap;

    .line 172
    new-instance v0, Landroid/util/ArrayMap;

    invoke-direct {v0}, Landroid/util/ArrayMap;-><init>()V

    iput-object v0, p0, Lcom/miui/server/smartpower/SmartPowerPolicyManager;->mDependencyMap:Landroid/util/ArrayMap;

    .line 174
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/miui/server/smartpower/SmartPowerPolicyManager;->mAppStateManager:Lcom/android/server/am/AppStateManager;

    .line 175
    iput-object v0, p0, Lcom/miui/server/smartpower/SmartPowerPolicyManager;->mAppPowerResourceManager:Lcom/miui/server/smartpower/AppPowerResourceManager;

    .line 179
    new-instance v0, Landroid/util/ArraySet;

    invoke-direct {v0}, Landroid/util/ArraySet;-><init>()V

    iput-object v0, p0, Lcom/miui/server/smartpower/SmartPowerPolicyManager;->mBroadcastBlackList:Landroid/util/ArraySet;

    .line 184
    new-instance v0, Landroid/util/ArraySet;

    invoke-direct {v0}, Landroid/util/ArraySet;-><init>()V

    iput-object v0, p0, Lcom/miui/server/smartpower/SmartPowerPolicyManager;->mProviderPkgBlackList:Landroid/util/ArraySet;

    .line 189
    new-instance v0, Landroid/util/ArraySet;

    invoke-direct {v0}, Landroid/util/ArraySet;-><init>()V

    iput-object v0, p0, Lcom/miui/server/smartpower/SmartPowerPolicyManager;->mServicePkgBlackList:Landroid/util/ArraySet;

    .line 193
    new-instance v0, Landroid/util/ArraySet;

    invoke-direct {v0}, Landroid/util/ArraySet;-><init>()V

    iput-object v0, p0, Lcom/miui/server/smartpower/SmartPowerPolicyManager;->mBroadcastWhiteList:Landroid/util/ArraySet;

    .line 194
    new-instance v0, Landroid/util/ArraySet;

    invoke-direct {v0}, Landroid/util/ArraySet;-><init>()V

    iput-object v0, p0, Lcom/miui/server/smartpower/SmartPowerPolicyManager;->mBroadcastProcessActionWhiteList:Landroid/util/ArraySet;

    .line 208
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    iput-object v0, p0, Lcom/miui/server/smartpower/SmartPowerPolicyManager;->mUsageStatsMap:Ljava/util/HashMap;

    .line 211
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/miui/server/smartpower/SmartPowerPolicyManager;->mScreenOff:Z

    .line 213
    iput v0, p0, Lcom/miui/server/smartpower/SmartPowerPolicyManager;->mRunning32ProcCount:I

    .line 215
    new-instance v0, Lcom/miui/server/smartpower/SmartPowerPolicyManager$1;

    invoke-direct {v0, p0}, Lcom/miui/server/smartpower/SmartPowerPolicyManager$1;-><init>(Lcom/miui/server/smartpower/SmartPowerPolicyManager;)V

    iput-object v0, p0, Lcom/miui/server/smartpower/SmartPowerPolicyManager;->mSysStatusReceiver:Landroid/content/BroadcastReceiver;

    .line 232
    new-instance v0, Lcom/miui/server/smartpower/SmartPowerPolicyManager$MyHandler;

    invoke-direct {v0, p0, p2}, Lcom/miui/server/smartpower/SmartPowerPolicyManager$MyHandler;-><init>(Lcom/miui/server/smartpower/SmartPowerPolicyManager;Landroid/os/Looper;)V

    iput-object v0, p0, Lcom/miui/server/smartpower/SmartPowerPolicyManager;->mHandler:Landroid/os/Handler;

    .line 233
    iput-object p3, p0, Lcom/miui/server/smartpower/SmartPowerPolicyManager;->mAMS:Lcom/android/server/am/ActivityManagerService;

    .line 234
    iput-object p1, p0, Lcom/miui/server/smartpower/SmartPowerPolicyManager;->mContext:Landroid/content/Context;

    .line 235
    iput-object p4, p0, Lcom/miui/server/smartpower/SmartPowerPolicyManager;->mPowerFrozenManager:Lcom/miui/server/smartpower/PowerFrozenManager;

    .line 236
    const-class v0, Landroid/app/usage/UsageStatsManagerInternal;

    invoke-static {v0}, Lcom/android/server/LocalServices;->getService(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/app/usage/UsageStatsManagerInternal;

    iput-object v0, p0, Lcom/miui/server/smartpower/SmartPowerPolicyManager;->mUsageStats:Landroid/app/usage/UsageStatsManagerInternal;

    .line 237
    const-class v0, Lcom/android/server/DeviceIdleInternal;

    invoke-static {v0}, Lcom/android/server/LocalServices;->getService(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/android/server/DeviceIdleInternal;

    iput-object v0, p0, Lcom/miui/server/smartpower/SmartPowerPolicyManager;->mLocalDeviceIdleController:Lcom/android/server/DeviceIdleInternal;

    .line 238
    invoke-virtual {p1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x110300cd

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getStringArray(I)[Ljava/lang/String;

    move-result-object v0

    .line 240
    .local v0, "packges":[Ljava/lang/String;
    const/4 v1, 0x0

    .local v1, "i":I
    :goto_0
    array-length v2, v0

    const/4 v3, 0x2

    if-ge v1, v2, :cond_0

    .line 241
    aget-object v2, v0, v1

    invoke-direct {p0, v2, v3}, Lcom/miui/server/smartpower/SmartPowerPolicyManager;->addPackageWhiteList(Ljava/lang/String;I)V

    .line 240
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 244
    .end local v1    # "i":I
    :cond_0
    invoke-virtual {p1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x110300ce

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getStringArray(I)[Ljava/lang/String;

    move-result-object v1

    .line 246
    .local v1, "processes":[Ljava/lang/String;
    const/4 v2, 0x0

    .local v2, "i":I
    :goto_1
    array-length v4, v1

    if-ge v2, v4, :cond_1

    .line 247
    aget-object v4, v1, v2

    invoke-direct {p0, v4, v3}, Lcom/miui/server/smartpower/SmartPowerPolicyManager;->addProcessWhiteList(Ljava/lang/String;I)V

    .line 246
    add-int/lit8 v2, v2, 0x1

    goto :goto_1

    .line 250
    .end local v2    # "i":I
    :cond_1
    invoke-virtual {p1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x110300c7

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getStringArray(I)[Ljava/lang/String;

    move-result-object v0

    .line 252
    const/4 v2, 0x0

    .restart local v2    # "i":I
    :goto_2
    array-length v3, v0

    if-ge v2, v3, :cond_2

    .line 253
    aget-object v3, v0, v2

    invoke-direct {p0, v3}, Lcom/miui/server/smartpower/SmartPowerPolicyManager;->addAlarmPackageWhiteList(Ljava/lang/String;)V

    .line 252
    add-int/lit8 v2, v2, 0x1

    goto :goto_2

    .line 256
    .end local v2    # "i":I
    :cond_2
    invoke-virtual {p1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x110300d0

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getStringArray(I)[Ljava/lang/String;

    move-result-object v0

    .line 258
    const/4 v2, 0x0

    .restart local v2    # "i":I
    :goto_3
    array-length v3, v0

    if-ge v2, v3, :cond_3

    .line 259
    aget-object v3, v0, v2

    invoke-direct {p0, v3}, Lcom/miui/server/smartpower/SmartPowerPolicyManager;->addServicePackageWhiteList(Ljava/lang/String;)V

    .line 258
    add-int/lit8 v2, v2, 0x1

    goto :goto_3

    .line 262
    .end local v2    # "i":I
    :cond_3
    invoke-virtual {p1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x110300cf

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getStringArray(I)[Ljava/lang/String;

    move-result-object v0

    .line 264
    const/4 v2, 0x0

    .restart local v2    # "i":I
    :goto_4
    array-length v3, v0

    if-ge v2, v3, :cond_4

    .line 265
    aget-object v3, v0, v2

    invoke-direct {p0, v3}, Lcom/miui/server/smartpower/SmartPowerPolicyManager;->addProviderPackageWhiteList(Ljava/lang/String;)V

    .line 264
    add-int/lit8 v2, v2, 0x1

    goto :goto_4

    .line 268
    .end local v2    # "i":I
    :cond_4
    invoke-virtual {p1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x110300c9

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getStringArray(I)[Ljava/lang/String;

    move-result-object v0

    .line 270
    const/4 v2, 0x0

    .restart local v2    # "i":I
    :goto_5
    array-length v3, v0

    if-ge v2, v3, :cond_5

    .line 271
    aget-object v3, v0, v2

    invoke-direct {p0, v3}, Lcom/miui/server/smartpower/SmartPowerPolicyManager;->addBroadcastProcessActionWhiteList(Ljava/lang/String;)V

    .line 270
    add-int/lit8 v2, v2, 0x1

    goto :goto_5

    .line 274
    .end local v2    # "i":I
    :cond_5
    iget-object v2, p0, Lcom/miui/server/smartpower/SmartPowerPolicyManager;->mBroadcastBlackList:Landroid/util/ArraySet;

    invoke-virtual {p1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    const v4, 0x110300c8

    invoke-virtual {v3, v4}, Landroid/content/res/Resources;->getStringArray(I)[Ljava/lang/String;

    move-result-object v3

    invoke-static {v3}, Ljava/util/Arrays;->asList([Ljava/lang/Object;)Ljava/util/List;

    move-result-object v3

    invoke-virtual {v2, v3}, Landroid/util/ArraySet;->addAll(Ljava/util/Collection;)Z

    .line 277
    iget-object v2, p0, Lcom/miui/server/smartpower/SmartPowerPolicyManager;->mBroadcastWhiteList:Landroid/util/ArraySet;

    invoke-virtual {p1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    const v4, 0x110300ca

    invoke-virtual {v3, v4}, Landroid/content/res/Resources;->getStringArray(I)[Ljava/lang/String;

    move-result-object v3

    invoke-static {v3}, Ljava/util/Arrays;->asList([Ljava/lang/Object;)Ljava/util/List;

    move-result-object v3

    invoke-virtual {v2, v3}, Landroid/util/ArraySet;->addAll(Ljava/util/Collection;)Z

    .line 280
    sget-object v2, Lcom/miui/server/smartpower/SmartPowerPolicyManager;->sInterceptPackageList:Landroid/util/ArraySet;

    monitor-enter v2

    .line 281
    :try_start_0
    invoke-virtual {p1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    const v4, 0x110300cc

    invoke-virtual {v3, v4}, Landroid/content/res/Resources;->getStringArray(I)[Ljava/lang/String;

    move-result-object v3

    invoke-static {v3}, Ljava/util/Arrays;->asList([Ljava/lang/Object;)Ljava/util/List;

    move-result-object v3

    invoke-virtual {v2, v3}, Landroid/util/ArraySet;->addAll(Ljava/util/Collection;)Z

    .line 283
    monitor-exit v2
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 285
    new-instance v2, Landroid/content/IntentFilter;

    invoke-direct {v2}, Landroid/content/IntentFilter;-><init>()V

    .line 286
    .local v2, "filter":Landroid/content/IntentFilter;
    const-string v3, "android.intent.action.SCREEN_OFF"

    invoke-virtual {v2, v3}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    .line 287
    const-string v3, "android.intent.action.SCREEN_ON"

    invoke-virtual {v2, v3}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    .line 288
    const-string v3, "android.intent.action.BOOT_COMPLETED"

    invoke-virtual {v2, v3}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    .line 289
    iget-object v3, p0, Lcom/miui/server/smartpower/SmartPowerPolicyManager;->mSysStatusReceiver:Landroid/content/BroadcastReceiver;

    invoke-virtual {p1, v3, v2}, Landroid/content/Context;->registerReceiver(Landroid/content/BroadcastReceiver;Landroid/content/IntentFilter;)Landroid/content/Intent;

    .line 290
    new-instance v3, Lcom/miui/server/smartpower/SmartPowerPolicyManager$PowerSavingStrategyControl;

    invoke-direct {v3, p0}, Lcom/miui/server/smartpower/SmartPowerPolicyManager$PowerSavingStrategyControl;-><init>(Lcom/miui/server/smartpower/SmartPowerPolicyManager;)V

    iput-object v3, p0, Lcom/miui/server/smartpower/SmartPowerPolicyManager;->mPowerSavingStrategyControl:Lcom/miui/server/smartpower/SmartPowerPolicyManager$PowerSavingStrategyControl;

    .line 291
    invoke-direct {p0}, Lcom/miui/server/smartpower/SmartPowerPolicyManager;->registerTestSuitSpecificObserver()V

    .line 292
    return-void

    .line 283
    .end local v2    # "filter":Landroid/content/IntentFilter;
    :catchall_0
    move-exception v3

    :try_start_1
    monitor-exit v2
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    throw v3
.end method

.method private activeApp(Lcom/android/server/am/AppStateManager$AppState;Ljava/lang/String;)V
    .locals 2
    .param p1, "app"    # Lcom/android/server/am/AppStateManager$AppState;
    .param p2, "reason"    # Ljava/lang/String;

    .line 864
    invoke-direct {p0}, Lcom/miui/server/smartpower/SmartPowerPolicyManager;->isEnableFrozen()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 865
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "power active "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    .line 866
    invoke-virtual {p1}, Lcom/android/server/am/AppStateManager$AppState;->getPackageName()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "/"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {p1}, Lcom/android/server/am/AppStateManager$AppState;->getUid()I

    move-result v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    .line 865
    const v1, 0x15ff2

    invoke-static {v1, v0}, Landroid/util/EventLog;->writeEvent(ILjava/lang/String;)I

    .line 867
    iget-object v0, p0, Lcom/miui/server/smartpower/SmartPowerPolicyManager;->mAppPowerResourceManager:Lcom/miui/server/smartpower/AppPowerResourceManager;

    invoke-virtual {p1}, Lcom/android/server/am/AppStateManager$AppState;->getUid()I

    move-result v1

    invoke-virtual {v0, v1}, Lcom/miui/server/smartpower/AppPowerResourceManager;->resumeAppAllPowerResources(I)V

    .line 869
    :cond_0
    return-void
.end method

.method private activeProcess(Lcom/android/server/am/AppStateManager$AppState$RunningProcess;Ljava/lang/String;)V
    .locals 4
    .param p1, "proc"    # Lcom/android/server/am/AppStateManager$AppState$RunningProcess;
    .param p2, "reason"    # Ljava/lang/String;

    .line 872
    const-string v0, "activeProcess"

    const-wide/32 v1, 0x20000

    invoke-static {v1, v2, v0}, Landroid/os/Trace;->traceBegin(JLjava/lang/String;)V

    .line 873
    invoke-virtual {p1}, Lcom/android/server/am/AppStateManager$AppState$RunningProcess;->getUid()I

    move-result v0

    invoke-virtual {p1}, Lcom/android/server/am/AppStateManager$AppState$RunningProcess;->getPid()I

    move-result v3

    invoke-direct {p0, v0, v3, p2}, Lcom/miui/server/smartpower/SmartPowerPolicyManager;->thawProcess(IILjava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 874
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "power active  ("

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    .line 875
    invoke-virtual {p1}, Lcom/android/server/am/AppStateManager$AppState$RunningProcess;->getPid()I

    move-result v3

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v3, ")"

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    .line 876
    invoke-virtual {p1}, Lcom/android/server/am/AppStateManager$AppState$RunningProcess;->getProcessName()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v3, "/"

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {p1}, Lcom/android/server/am/AppStateManager$AppState$RunningProcess;->getUid()I

    move-result v3

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    .line 874
    const v3, 0x15ff2

    invoke-static {v3, v0}, Landroid/util/EventLog;->writeEvent(ILjava/lang/String;)I

    .line 878
    :cond_0
    invoke-static {v1, v2}, Landroid/os/Trace;->traceEnd(J)V

    .line 879
    return-void
.end method

.method private addAlarmPackageWhiteList(Ljava/lang/String;)V
    .locals 4
    .param p1, "pkgname"    # Ljava/lang/String;

    .line 1231
    iget-object v0, p0, Lcom/miui/server/smartpower/SmartPowerPolicyManager;->mPackageWhiteList:Landroid/util/ArrayMap;

    monitor-enter v0

    .line 1232
    :try_start_0
    iget-object v1, p0, Lcom/miui/server/smartpower/SmartPowerPolicyManager;->mPackageWhiteList:Landroid/util/ArrayMap;

    const v2, 0x8000

    const/16 v3, 0x8

    invoke-direct {p0, v1, p1, v2, v3}, Lcom/miui/server/smartpower/SmartPowerPolicyManager;->addStaticWhiteListLocked(Landroid/util/ArrayMap;Ljava/lang/String;II)V

    .line 1234
    monitor-exit v0

    .line 1235
    return-void

    .line 1234
    :catchall_0
    move-exception v1

    monitor-exit v0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v1
.end method

.method private addBroadcastBlackList(Ljava/lang/String;)V
    .locals 2
    .param p1, "intent"    # Ljava/lang/String;

    .line 1300
    iget-object v0, p0, Lcom/miui/server/smartpower/SmartPowerPolicyManager;->mBroadcastBlackList:Landroid/util/ArraySet;

    monitor-enter v0

    .line 1301
    :try_start_0
    iget-object v1, p0, Lcom/miui/server/smartpower/SmartPowerPolicyManager;->mBroadcastBlackList:Landroid/util/ArraySet;

    invoke-virtual {v1, p1}, Landroid/util/ArraySet;->add(Ljava/lang/Object;)Z

    .line 1302
    monitor-exit v0

    .line 1303
    return-void

    .line 1302
    :catchall_0
    move-exception v1

    monitor-exit v0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v1
.end method

.method private addBroadcastProcessActionWhiteList(Ljava/lang/String;)V
    .locals 2
    .param p1, "processAction"    # Ljava/lang/String;

    .line 1294
    iget-object v0, p0, Lcom/miui/server/smartpower/SmartPowerPolicyManager;->mBroadcastProcessActionWhiteList:Landroid/util/ArraySet;

    monitor-enter v0

    .line 1295
    :try_start_0
    iget-object v1, p0, Lcom/miui/server/smartpower/SmartPowerPolicyManager;->mBroadcastProcessActionWhiteList:Landroid/util/ArraySet;

    invoke-virtual {v1, p1}, Landroid/util/ArraySet;->add(Ljava/lang/Object;)Z

    .line 1296
    monitor-exit v0

    .line 1297
    return-void

    .line 1296
    :catchall_0
    move-exception v1

    monitor-exit v0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v1
.end method

.method private addBroadcastProcessActionWhiteList(Ljava/lang/String;Ljava/lang/String;)V
    .locals 1
    .param p1, "processName"    # Ljava/lang/String;
    .param p2, "action"    # Ljava/lang/String;

    .line 1290
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-direct {p0, v0}, Lcom/miui/server/smartpower/SmartPowerPolicyManager;->addBroadcastProcessActionWhiteList(Ljava/lang/String;)V

    .line 1291
    return-void
.end method

.method private addBroadcastWhiteList(Ljava/lang/String;)V
    .locals 2
    .param p1, "intent"    # Ljava/lang/String;

    .line 1336
    iget-object v0, p0, Lcom/miui/server/smartpower/SmartPowerPolicyManager;->mBroadcastWhiteList:Landroid/util/ArraySet;

    monitor-enter v0

    .line 1337
    :try_start_0
    iget-object v1, p0, Lcom/miui/server/smartpower/SmartPowerPolicyManager;->mBroadcastWhiteList:Landroid/util/ArraySet;

    invoke-virtual {v1, p1}, Landroid/util/ArraySet;->add(Ljava/lang/Object;)Z

    .line 1338
    monitor-exit v0

    .line 1339
    return-void

    .line 1338
    :catchall_0
    move-exception v1

    monitor-exit v0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v1
.end method

.method private addDynamicWhiteListLocked(Landroid/util/ArrayMap;Ljava/lang/Integer;Ljava/lang/String;II)Lcom/miui/server/smartpower/SmartPowerPolicyManager$WhiteListItem;
    .locals 2
    .param p2, "key"    # Ljava/lang/Integer;
    .param p3, "name"    # Ljava/lang/String;
    .param p4, "type"    # I
    .param p5, "action"    # I
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/util/ArrayMap<",
            "Ljava/lang/Integer;",
            "Lcom/miui/server/smartpower/SmartPowerPolicyManager$WhiteListItem;",
            ">;",
            "Ljava/lang/Integer;",
            "Ljava/lang/String;",
            "II)",
            "Lcom/miui/server/smartpower/SmartPowerPolicyManager$WhiteListItem;"
        }
    .end annotation

    .line 1261
    .local p1, "whiteList":Landroid/util/ArrayMap;, "Landroid/util/ArrayMap<Ljava/lang/Integer;Lcom/miui/server/smartpower/SmartPowerPolicyManager$WhiteListItem;>;"
    invoke-virtual {p1, p2}, Landroid/util/ArrayMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/miui/server/smartpower/SmartPowerPolicyManager$WhiteListItem;

    .line 1262
    .local v0, "item":Lcom/miui/server/smartpower/SmartPowerPolicyManager$WhiteListItem;
    if-nez v0, :cond_0

    .line 1263
    new-instance v1, Lcom/miui/server/smartpower/SmartPowerPolicyManager$WhiteListItem;

    invoke-direct {v1, p0, p3, p4, p5}, Lcom/miui/server/smartpower/SmartPowerPolicyManager$WhiteListItem;-><init>(Lcom/miui/server/smartpower/SmartPowerPolicyManager;Ljava/lang/String;II)V

    move-object v0, v1

    .line 1264
    invoke-virtual {p1, p2, v0}, Landroid/util/ArrayMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 1265
    return-object v0

    .line 1267
    :cond_0
    invoke-static {v0}, Lcom/miui/server/smartpower/SmartPowerPolicyManager$WhiteListItem;->-$$Nest$fgetmTypes(Lcom/miui/server/smartpower/SmartPowerPolicyManager$WhiteListItem;)I

    move-result v1

    or-int/2addr v1, p4

    invoke-static {v0, v1}, Lcom/miui/server/smartpower/SmartPowerPolicyManager$WhiteListItem;->-$$Nest$fputmTypes(Lcom/miui/server/smartpower/SmartPowerPolicyManager$WhiteListItem;I)V

    .line 1268
    invoke-static {v0}, Lcom/miui/server/smartpower/SmartPowerPolicyManager$WhiteListItem;->-$$Nest$fgetmActions(Lcom/miui/server/smartpower/SmartPowerPolicyManager$WhiteListItem;)I

    move-result v1

    or-int/2addr v1, p5

    invoke-static {v0, v1}, Lcom/miui/server/smartpower/SmartPowerPolicyManager$WhiteListItem;->-$$Nest$fputmActions(Lcom/miui/server/smartpower/SmartPowerPolicyManager$WhiteListItem;I)V

    .line 1270
    const/4 v1, 0x0

    return-object v1
.end method

.method private addPackageWhiteList(Ljava/lang/String;I)V
    .locals 3
    .param p1, "pkgName"    # Ljava/lang/String;
    .param p2, "type"    # I

    .line 1142
    iget-object v0, p0, Lcom/miui/server/smartpower/SmartPowerPolicyManager;->mPackageWhiteList:Landroid/util/ArrayMap;

    monitor-enter v0

    .line 1143
    :try_start_0
    iget-object v1, p0, Lcom/miui/server/smartpower/SmartPowerPolicyManager;->mPackageWhiteList:Landroid/util/ArrayMap;

    const/4 v2, 0x2

    invoke-direct {p0, v1, p1, p2, v2}, Lcom/miui/server/smartpower/SmartPowerPolicyManager;->addStaticWhiteListLocked(Landroid/util/ArrayMap;Ljava/lang/String;II)V

    .line 1145
    monitor-exit v0

    .line 1146
    return-void

    .line 1145
    :catchall_0
    move-exception v1

    monitor-exit v0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v1
.end method

.method private addPidWhiteList(IILjava/lang/String;I)V
    .locals 7
    .param p1, "uid"    # I
    .param p2, "pid"    # I
    .param p3, "procName"    # Ljava/lang/String;
    .param p4, "type"    # I

    .line 1150
    iget-object v0, p0, Lcom/miui/server/smartpower/SmartPowerPolicyManager;->mPidWhiteList:Landroid/util/ArrayMap;

    monitor-enter v0

    .line 1151
    :try_start_0
    iget-object v2, p0, Lcom/miui/server/smartpower/SmartPowerPolicyManager;->mPidWhiteList:Landroid/util/ArrayMap;

    .line 1152
    invoke-static {p2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    const/4 v6, 0x2

    .line 1151
    move-object v1, p0

    move-object v4, p3

    move v5, p4

    invoke-direct/range {v1 .. v6}, Lcom/miui/server/smartpower/SmartPowerPolicyManager;->addDynamicWhiteListLocked(Landroid/util/ArrayMap;Ljava/lang/Integer;Ljava/lang/String;II)Lcom/miui/server/smartpower/SmartPowerPolicyManager$WhiteListItem;

    move-result-object v1

    .line 1153
    .local v1, "item":Lcom/miui/server/smartpower/SmartPowerPolicyManager$WhiteListItem;
    monitor-exit v0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 1154
    if-eqz v1, :cond_0

    .line 1155
    invoke-static {v1, p1}, Lcom/miui/server/smartpower/SmartPowerPolicyManager$WhiteListItem;->-$$Nest$fputmUid(Lcom/miui/server/smartpower/SmartPowerPolicyManager$WhiteListItem;I)V

    .line 1156
    invoke-static {v1, p2}, Lcom/miui/server/smartpower/SmartPowerPolicyManager$WhiteListItem;->-$$Nest$fputmPid(Lcom/miui/server/smartpower/SmartPowerPolicyManager$WhiteListItem;I)V

    .line 1157
    iget-object v0, p0, Lcom/miui/server/smartpower/SmartPowerPolicyManager;->mHandler:Landroid/os/Handler;

    new-instance v2, Lcom/miui/server/smartpower/SmartPowerPolicyManager$5;

    invoke-direct {v2, p0, p1, p2}, Lcom/miui/server/smartpower/SmartPowerPolicyManager$5;-><init>(Lcom/miui/server/smartpower/SmartPowerPolicyManager;II)V

    invoke-virtual {v0, v2}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    .line 1164
    :cond_0
    return-void

    .line 1153
    .end local v1    # "item":Lcom/miui/server/smartpower/SmartPowerPolicyManager$WhiteListItem;
    :catchall_0
    move-exception v1

    :try_start_1
    monitor-exit v0
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    throw v1
.end method

.method private addProcessWhiteList(Ljava/lang/String;I)V
    .locals 3
    .param p1, "procName"    # Ljava/lang/String;
    .param p2, "type"    # I

    .line 1183
    iget-object v0, p0, Lcom/miui/server/smartpower/SmartPowerPolicyManager;->mProcessWhiteList:Landroid/util/ArrayMap;

    monitor-enter v0

    .line 1184
    :try_start_0
    iget-object v1, p0, Lcom/miui/server/smartpower/SmartPowerPolicyManager;->mProcessWhiteList:Landroid/util/ArrayMap;

    const/4 v2, 0x2

    invoke-direct {p0, v1, p1, p2, v2}, Lcom/miui/server/smartpower/SmartPowerPolicyManager;->addStaticWhiteListLocked(Landroid/util/ArrayMap;Ljava/lang/String;II)V

    .line 1186
    monitor-exit v0

    .line 1187
    return-void

    .line 1186
    :catchall_0
    move-exception v1

    monitor-exit v0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v1
.end method

.method private addProviderPackageWhiteList(Ljava/lang/String;)V
    .locals 4
    .param p1, "pkgName"    # Ljava/lang/String;

    .line 1214
    iget-object v0, p0, Lcom/miui/server/smartpower/SmartPowerPolicyManager;->mPackageWhiteList:Landroid/util/ArrayMap;

    monitor-enter v0

    .line 1215
    :try_start_0
    iget-object v1, p0, Lcom/miui/server/smartpower/SmartPowerPolicyManager;->mPackageWhiteList:Landroid/util/ArrayMap;

    const/high16 v2, 0x200000

    const/16 v3, 0x20

    invoke-direct {p0, v1, p1, v2, v3}, Lcom/miui/server/smartpower/SmartPowerPolicyManager;->addStaticWhiteListLocked(Landroid/util/ArrayMap;Ljava/lang/String;II)V

    .line 1217
    monitor-exit v0

    .line 1218
    return-void

    .line 1217
    :catchall_0
    move-exception v1

    monitor-exit v0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v1
.end method

.method private addProviderPkgBlackList(Ljava/lang/String;)V
    .locals 2
    .param p1, "pkgName"    # Ljava/lang/String;

    .line 1312
    iget-object v0, p0, Lcom/miui/server/smartpower/SmartPowerPolicyManager;->mProviderPkgBlackList:Landroid/util/ArraySet;

    monitor-enter v0

    .line 1313
    :try_start_0
    iget-object v1, p0, Lcom/miui/server/smartpower/SmartPowerPolicyManager;->mProviderPkgBlackList:Landroid/util/ArraySet;

    invoke-virtual {v1, p1}, Landroid/util/ArraySet;->add(Ljava/lang/Object;)Z

    .line 1314
    monitor-exit v0

    .line 1315
    return-void

    .line 1314
    :catchall_0
    move-exception v1

    monitor-exit v0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v1
.end method

.method private addServicePackageWhiteList(Ljava/lang/String;)V
    .locals 4
    .param p1, "pkgName"    # Ljava/lang/String;

    .line 1197
    iget-object v0, p0, Lcom/miui/server/smartpower/SmartPowerPolicyManager;->mPackageWhiteList:Landroid/util/ArrayMap;

    monitor-enter v0

    .line 1198
    :try_start_0
    iget-object v1, p0, Lcom/miui/server/smartpower/SmartPowerPolicyManager;->mPackageWhiteList:Landroid/util/ArrayMap;

    const/high16 v2, 0x40000

    const/16 v3, 0x10

    invoke-direct {p0, v1, p1, v2, v3}, Lcom/miui/server/smartpower/SmartPowerPolicyManager;->addStaticWhiteListLocked(Landroid/util/ArrayMap;Ljava/lang/String;II)V

    .line 1200
    monitor-exit v0

    .line 1201
    return-void

    .line 1200
    :catchall_0
    move-exception v1

    monitor-exit v0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v1
.end method

.method private addServicePkgBlackList(Ljava/lang/String;)V
    .locals 2
    .param p1, "pkgName"    # Ljava/lang/String;

    .line 1324
    iget-object v0, p0, Lcom/miui/server/smartpower/SmartPowerPolicyManager;->mServicePkgBlackList:Landroid/util/ArraySet;

    monitor-enter v0

    .line 1325
    :try_start_0
    iget-object v1, p0, Lcom/miui/server/smartpower/SmartPowerPolicyManager;->mServicePkgBlackList:Landroid/util/ArraySet;

    invoke-virtual {v1, p1}, Landroid/util/ArraySet;->add(Ljava/lang/Object;)Z

    .line 1326
    monitor-exit v0

    .line 1327
    return-void

    .line 1326
    :catchall_0
    move-exception v1

    monitor-exit v0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v1
.end method

.method private addSreenonWhiteList(Ljava/lang/String;I)V
    .locals 3
    .param p1, "pkgName"    # Ljava/lang/String;
    .param p2, "type"    # I

    .line 1190
    iget-object v0, p0, Lcom/miui/server/smartpower/SmartPowerPolicyManager;->mPackageWhiteList:Landroid/util/ArrayMap;

    monitor-enter v0

    .line 1191
    :try_start_0
    iget-object v1, p0, Lcom/miui/server/smartpower/SmartPowerPolicyManager;->mPackageWhiteList:Landroid/util/ArrayMap;

    const/4 v2, 0x4

    invoke-direct {p0, v1, p1, p2, v2}, Lcom/miui/server/smartpower/SmartPowerPolicyManager;->addStaticWhiteListLocked(Landroid/util/ArrayMap;Ljava/lang/String;II)V

    .line 1193
    monitor-exit v0

    .line 1194
    return-void

    .line 1193
    :catchall_0
    move-exception v1

    monitor-exit v0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v1
.end method

.method private addStaticWhiteListLocked(Landroid/util/ArrayMap;Ljava/lang/String;II)V
    .locals 2
    .param p2, "name"    # Ljava/lang/String;
    .param p3, "type"    # I
    .param p4, "action"    # I
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/util/ArrayMap<",
            "Ljava/lang/String;",
            "Lcom/miui/server/smartpower/SmartPowerPolicyManager$WhiteListItem;",
            ">;",
            "Ljava/lang/String;",
            "II)V"
        }
    .end annotation

    .line 1249
    .local p1, "whiteList":Landroid/util/ArrayMap;, "Landroid/util/ArrayMap<Ljava/lang/String;Lcom/miui/server/smartpower/SmartPowerPolicyManager$WhiteListItem;>;"
    invoke-virtual {p1, p2}, Landroid/util/ArrayMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/miui/server/smartpower/SmartPowerPolicyManager$WhiteListItem;

    .line 1250
    .local v0, "item":Lcom/miui/server/smartpower/SmartPowerPolicyManager$WhiteListItem;
    if-nez v0, :cond_0

    .line 1251
    new-instance v1, Lcom/miui/server/smartpower/SmartPowerPolicyManager$WhiteListItem;

    invoke-direct {v1, p0, p2, p3, p4}, Lcom/miui/server/smartpower/SmartPowerPolicyManager$WhiteListItem;-><init>(Lcom/miui/server/smartpower/SmartPowerPolicyManager;Ljava/lang/String;II)V

    move-object v0, v1

    .line 1252
    invoke-virtual {p1, p2, v0}, Landroid/util/ArrayMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    goto :goto_0

    .line 1254
    :cond_0
    invoke-static {v0}, Lcom/miui/server/smartpower/SmartPowerPolicyManager$WhiteListItem;->-$$Nest$fgetmTypes(Lcom/miui/server/smartpower/SmartPowerPolicyManager$WhiteListItem;)I

    move-result v1

    or-int/2addr v1, p3

    invoke-static {v0, v1}, Lcom/miui/server/smartpower/SmartPowerPolicyManager$WhiteListItem;->-$$Nest$fputmTypes(Lcom/miui/server/smartpower/SmartPowerPolicyManager$WhiteListItem;I)V

    .line 1255
    invoke-static {v0}, Lcom/miui/server/smartpower/SmartPowerPolicyManager$WhiteListItem;->-$$Nest$fgetmActions(Lcom/miui/server/smartpower/SmartPowerPolicyManager$WhiteListItem;)I

    move-result v1

    or-int/2addr v1, p4

    invoke-static {v0, v1}, Lcom/miui/server/smartpower/SmartPowerPolicyManager$WhiteListItem;->-$$Nest$fputmActions(Lcom/miui/server/smartpower/SmartPowerPolicyManager$WhiteListItem;I)V

    .line 1257
    :goto_0
    return-void
.end method

.method private addUidWhiteList(ILjava/lang/String;I)V
    .locals 7
    .param p1, "uid"    # I
    .param p2, "pkgName"    # Ljava/lang/String;
    .param p3, "type"    # I

    .line 1110
    iget-object v0, p0, Lcom/miui/server/smartpower/SmartPowerPolicyManager;->mUidWhiteList:Landroid/util/ArrayMap;

    monitor-enter v0

    .line 1111
    :try_start_0
    iget-object v2, p0, Lcom/miui/server/smartpower/SmartPowerPolicyManager;->mUidWhiteList:Landroid/util/ArrayMap;

    .line 1112
    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    const/4 v6, 0x2

    .line 1111
    move-object v1, p0

    move-object v4, p2

    move v5, p3

    invoke-direct/range {v1 .. v6}, Lcom/miui/server/smartpower/SmartPowerPolicyManager;->addDynamicWhiteListLocked(Landroid/util/ArrayMap;Ljava/lang/Integer;Ljava/lang/String;II)Lcom/miui/server/smartpower/SmartPowerPolicyManager$WhiteListItem;

    move-result-object v1

    .line 1113
    .local v1, "item":Lcom/miui/server/smartpower/SmartPowerPolicyManager$WhiteListItem;
    monitor-exit v0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 1114
    if-eqz v1, :cond_0

    .line 1115
    invoke-static {v1, p1}, Lcom/miui/server/smartpower/SmartPowerPolicyManager$WhiteListItem;->-$$Nest$fputmUid(Lcom/miui/server/smartpower/SmartPowerPolicyManager$WhiteListItem;I)V

    .line 1116
    iget-object v0, p0, Lcom/miui/server/smartpower/SmartPowerPolicyManager;->mHandler:Landroid/os/Handler;

    new-instance v2, Lcom/miui/server/smartpower/SmartPowerPolicyManager$3;

    invoke-direct {v2, p0, p1}, Lcom/miui/server/smartpower/SmartPowerPolicyManager$3;-><init>(Lcom/miui/server/smartpower/SmartPowerPolicyManager;I)V

    invoke-virtual {v0, v2}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    .line 1123
    :cond_0
    return-void

    .line 1113
    .end local v1    # "item":Lcom/miui/server/smartpower/SmartPowerPolicyManager$WhiteListItem;
    :catchall_0
    move-exception v1

    :try_start_1
    monitor-exit v0
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    throw v1
.end method

.method private compactProcess(Ljava/lang/String;Lcom/android/server/am/AppStateManager$AppState$RunningProcess;)V
    .locals 2
    .param p1, "packageName"    # Ljava/lang/String;
    .param p2, "proc"    # Lcom/android/server/am/AppStateManager$AppState$RunningProcess;

    .line 882
    invoke-virtual {p2}, Lcom/android/server/am/AppStateManager$AppState$RunningProcess;->getProcessRecord()Lcom/android/server/am/ProcessRecord;

    move-result-object v0

    invoke-static {v0}, Lcom/android/server/am/AppStateManager;->isSystemApp(Lcom/android/server/am/ProcessRecord;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 883
    return-void

    .line 885
    :cond_0
    invoke-virtual {p2}, Lcom/android/server/am/AppStateManager$AppState$RunningProcess;->getAdj()I

    move-result v0

    const/16 v1, 0x2bc

    if-gt v0, v1, :cond_1

    .line 886
    invoke-static {}, Lcom/android/server/am/SystemPressureControllerStub;->getInstance()Lcom/android/server/am/SystemPressureControllerStub;

    move-result-object v0

    const/4 v1, 0x1

    invoke-virtual {v0, p2, v1}, Lcom/android/server/am/SystemPressureControllerStub;->compactBackgroundProcess(Lcom/miui/server/smartpower/IAppState$IRunningProcess;Z)V

    goto :goto_0

    .line 888
    :cond_1
    invoke-static {}, Lcom/android/server/am/SystemPressureControllerStub;->getInstance()Lcom/android/server/am/SystemPressureControllerStub;

    move-result-object v0

    const/4 v1, 0x0

    invoke-virtual {v0, p2, v1}, Lcom/android/server/am/SystemPressureControllerStub;->compactBackgroundProcess(Lcom/miui/server/smartpower/IAppState$IRunningProcess;Z)V

    .line 890
    :goto_0
    return-void
.end method

.method private diedProcess(Lcom/android/server/am/AppStateManager$AppState$RunningProcess;)V
    .locals 2
    .param p1, "proc"    # Lcom/android/server/am/AppStateManager$AppState$RunningProcess;

    .line 763
    invoke-virtual {p1}, Lcom/android/server/am/AppStateManager$AppState$RunningProcess;->is64BitProcess()Z

    move-result v0

    if-nez v0, :cond_1

    .line 764
    iget v0, p0, Lcom/miui/server/smartpower/SmartPowerPolicyManager;->mRunning32ProcCount:I

    if-gtz v0, :cond_0

    const/4 v0, 0x0

    goto :goto_0

    :cond_0
    add-int/lit8 v0, v0, -0x1

    :goto_0
    iput v0, p0, Lcom/miui/server/smartpower/SmartPowerPolicyManager;->mRunning32ProcCount:I

    .line 765
    if-gtz v0, :cond_1

    .line 766
    const-string/jumbo v0, "sys.mi_zygote32.start"

    const-string v1, "false"

    invoke-static {v0, v1}, Landroid/os/SystemProperties;->set(Ljava/lang/String;Ljava/lang/String;)V

    .line 769
    :cond_1
    return-void
.end method

.method private forzenProcess(IILjava/lang/String;Ljava/lang/String;)Z
    .locals 2
    .param p1, "uid"    # I
    .param p2, "pid"    # I
    .param p3, "processName"    # Ljava/lang/String;
    .param p4, "reason"    # Ljava/lang/String;

    .line 901
    const/4 v0, 0x0

    .line 902
    .local v0, "done":Z
    if-lez p2, :cond_0

    iget-object v1, p0, Lcom/miui/server/smartpower/SmartPowerPolicyManager;->mPowerFrozenManager:Lcom/miui/server/smartpower/PowerFrozenManager;

    invoke-virtual {v1, p2}, Lcom/miui/server/smartpower/PowerFrozenManager;->isAllFrozenPid(I)Z

    move-result v1

    if-nez v1, :cond_0

    .line 903
    iget-object v1, p0, Lcom/miui/server/smartpower/SmartPowerPolicyManager;->mPowerFrozenManager:Lcom/miui/server/smartpower/PowerFrozenManager;

    invoke-virtual {v1, p1, p2, p3, p4}, Lcom/miui/server/smartpower/PowerFrozenManager;->frozenProcess(IILjava/lang/String;Ljava/lang/String;)Z

    move-result v0

    .line 905
    :cond_0
    return v0
.end method

.method private static getWhiteListTypeMask(I)I
    .locals 1
    .param p0, "action"    # I

    .line 1391
    sparse-switch p0, :sswitch_data_0

    .line 1398
    const/4 v0, 0x0

    return v0

    .line 1396
    :sswitch_0
    const/high16 v0, 0x700000

    return v0

    .line 1395
    :sswitch_1
    const/high16 v0, 0xe0000

    return v0

    .line 1394
    :sswitch_2
    const v0, 0x1c000

    return v0

    .line 1393
    :sswitch_3
    const/16 v0, 0x3800

    return v0

    .line 1392
    :sswitch_4
    const/16 v0, 0x7fe

    return v0

    nop

    :sswitch_data_0
    .sparse-switch
        0x2 -> :sswitch_4
        0x4 -> :sswitch_3
        0x8 -> :sswitch_2
        0x10 -> :sswitch_1
        0x20 -> :sswitch_0
    .end sparse-switch
.end method

.method private hasPidWhiteList(I)Z
    .locals 4
    .param p1, "uid"    # I

    .line 1010
    iget-object v0, p0, Lcom/miui/server/smartpower/SmartPowerPolicyManager;->mPidWhiteList:Landroid/util/ArrayMap;

    monitor-enter v0

    .line 1011
    :try_start_0
    iget-object v1, p0, Lcom/miui/server/smartpower/SmartPowerPolicyManager;->mPidWhiteList:Landroid/util/ArrayMap;

    invoke-virtual {v1}, Landroid/util/ArrayMap;->values()Ljava/util/Collection;

    move-result-object v1

    invoke-interface {v1}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_1

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/miui/server/smartpower/SmartPowerPolicyManager$WhiteListItem;

    .line 1012
    .local v2, "item":Lcom/miui/server/smartpower/SmartPowerPolicyManager$WhiteListItem;
    invoke-static {v2}, Lcom/miui/server/smartpower/SmartPowerPolicyManager$WhiteListItem;->-$$Nest$fgetmUid(Lcom/miui/server/smartpower/SmartPowerPolicyManager$WhiteListItem;)I

    move-result v3

    if-ne v3, p1, :cond_0

    const/4 v3, 0x2

    invoke-virtual {v2, v3}, Lcom/miui/server/smartpower/SmartPowerPolicyManager$WhiteListItem;->hasAction(I)Z

    move-result v3

    if-eqz v3, :cond_0

    .line 1013
    monitor-exit v0

    const/4 v0, 0x1

    return v0

    .line 1015
    .end local v2    # "item":Lcom/miui/server/smartpower/SmartPowerPolicyManager$WhiteListItem;
    :cond_0
    goto :goto_0

    .line 1016
    :cond_1
    monitor-exit v0

    .line 1017
    const/4 v0, 0x0

    return v0

    .line 1016
    :catchall_0
    move-exception v1

    monitor-exit v0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v1
.end method

.method private hibernationApp(Lcom/android/server/am/AppStateManager$AppState;Ljava/lang/String;)V
    .locals 4
    .param p1, "app"    # Lcom/android/server/am/AppStateManager$AppState;
    .param p2, "reason"    # Ljava/lang/String;

    .line 833
    invoke-direct {p0}, Lcom/miui/server/smartpower/SmartPowerPolicyManager;->isEnableFrozen()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 834
    const-string v0, "hibernationApp"

    const-wide/32 v1, 0x20000

    invoke-static {v1, v2, v0}, Landroid/os/Trace;->traceBegin(JLjava/lang/String;)V

    .line 835
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "power hibernation "

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    .line 836
    invoke-virtual {p1}, Lcom/android/server/am/AppStateManager$AppState;->getPackageName()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v3, "/"

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {p1}, Lcom/android/server/am/AppStateManager$AppState;->getUid()I

    move-result v3

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    .line 835
    const v3, 0x15ff2

    invoke-static {v3, v0}, Landroid/util/EventLog;->writeEvent(ILjava/lang/String;)I

    .line 837
    iget-object v0, p0, Lcom/miui/server/smartpower/SmartPowerPolicyManager;->mAppPowerResourceManager:Lcom/miui/server/smartpower/AppPowerResourceManager;

    invoke-virtual {p1}, Lcom/android/server/am/AppStateManager$AppState;->getUid()I

    move-result v3

    invoke-virtual {v0, v3}, Lcom/miui/server/smartpower/AppPowerResourceManager;->releaseAppAllPowerResources(I)V

    .line 838
    invoke-static {v1, v2}, Landroid/os/Trace;->traceEnd(J)V

    .line 840
    :cond_0
    return-void
.end method

.method private hibernationProcess(Lcom/android/server/am/AppStateManager$AppState$RunningProcess;Ljava/lang/String;)V
    .locals 2
    .param p1, "proc"    # Lcom/android/server/am/AppStateManager$AppState$RunningProcess;
    .param p2, "reason"    # Ljava/lang/String;

    .line 843
    invoke-virtual {p1}, Lcom/android/server/am/AppStateManager$AppState$RunningProcess;->getProcessRecord()Lcom/android/server/am/ProcessRecord;

    move-result-object v0

    if-nez v0, :cond_0

    .line 844
    return-void

    .line 846
    :cond_0
    iget-object v0, p0, Lcom/miui/server/smartpower/SmartPowerPolicyManager;->mHandler:Landroid/os/Handler;

    new-instance v1, Lcom/miui/server/smartpower/SmartPowerPolicyManager$2;

    invoke-direct {v1, p0, p1, p2}, Lcom/miui/server/smartpower/SmartPowerPolicyManager$2;-><init>(Lcom/miui/server/smartpower/SmartPowerPolicyManager;Lcom/android/server/am/AppStateManager$AppState$RunningProcess;Ljava/lang/String;)V

    invoke-virtual {v0, v1}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    .line 861
    return-void
.end method

.method private idleApp(Lcom/android/server/am/AppStateManager$AppState;)V
    .locals 4
    .param p1, "app"    # Lcom/android/server/am/AppStateManager$AppState;

    .line 806
    invoke-direct {p0}, Lcom/miui/server/smartpower/SmartPowerPolicyManager;->isEnableIntercept()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 807
    invoke-virtual {p1}, Lcom/android/server/am/AppStateManager$AppState;->getPackageName()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/miui/server/process/ProcessManagerInternal;->checkCtsProcess(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_1

    .line 808
    new-instance v0, Lcom/miui/server/smartpower/SmartPowerPolicyManager$$ExternalSyntheticLambda0;

    invoke-direct {v0, p0, p1}, Lcom/miui/server/smartpower/SmartPowerPolicyManager$$ExternalSyntheticLambda0;-><init>(Lcom/miui/server/smartpower/SmartPowerPolicyManager;Lcom/android/server/am/AppStateManager$AppState;)V

    .line 819
    .local v0, "idleRunnable":Ljava/lang/Runnable;
    invoke-virtual {p1}, Lcom/android/server/am/AppStateManager$AppState;->getMinAmsProcState()I

    move-result v1

    const/4 v2, 0x6

    if-le v1, v2, :cond_0

    .line 820
    iget-object v1, p0, Lcom/miui/server/smartpower/SmartPowerPolicyManager;->mHandler:Landroid/os/Handler;

    invoke-virtual {v1, v0}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    goto :goto_0

    .line 822
    :cond_0
    iget-object v1, p0, Lcom/miui/server/smartpower/SmartPowerPolicyManager;->mHandler:Landroid/os/Handler;

    const-wide/16 v2, 0x4e20

    invoke-virtual {v1, v0, v2, v3}, Landroid/os/Handler;->postDelayed(Ljava/lang/Runnable;J)Z

    .line 825
    .end local v0    # "idleRunnable":Ljava/lang/Runnable;
    :cond_1
    :goto_0
    return-void
.end method

.method private idleProcess(Lcom/android/server/am/AppStateManager$AppState$RunningProcess;)V
    .locals 0
    .param p1, "proc"    # Lcom/android/server/am/AppStateManager$AppState$RunningProcess;

    .line 803
    return-void
.end method

.method private isBroadcastProcessWhiteList(Ljava/lang/String;Ljava/lang/String;)Z
    .locals 3
    .param p1, "processName"    # Ljava/lang/String;
    .param p2, "action"    # Ljava/lang/String;

    .line 1054
    iget-object v0, p0, Lcom/miui/server/smartpower/SmartPowerPolicyManager;->mBroadcastProcessActionWhiteList:Landroid/util/ArraySet;

    monitor-enter v0

    .line 1055
    :try_start_0
    iget-object v1, p0, Lcom/miui/server/smartpower/SmartPowerPolicyManager;->mBroadcastProcessActionWhiteList:Landroid/util/ArraySet;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Landroid/util/ArraySet;->contains(Ljava/lang/Object;)Z

    move-result v1

    monitor-exit v0

    return v1

    .line 1056
    :catchall_0
    move-exception v1

    monitor-exit v0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v1
.end method

.method private isDeviceIdleWhiteList(I)Z
    .locals 2
    .param p1, "uid"    # I

    .line 828
    iget-object v0, p0, Lcom/miui/server/smartpower/SmartPowerPolicyManager;->mLocalDeviceIdleController:Lcom/android/server/DeviceIdleInternal;

    if-eqz v0, :cond_0

    .line 829
    invoke-static {p1}, Landroid/os/UserHandle;->getAppId(I)I

    move-result v1

    invoke-interface {v0, v1}, Lcom/android/server/DeviceIdleInternal;->isAppOnWhitelist(I)Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    .line 828
    :goto_0
    return v0
.end method

.method private isEnableFrozen()Z
    .locals 1

    .line 939
    invoke-static {}, Lcom/miui/server/smartpower/PowerFrozenManager;->isEnable()Z

    move-result v0

    return v0
.end method

.method private isEnableIntercept()Z
    .locals 1

    .line 943
    sget-boolean v0, Lcom/miui/app/smartpower/SmartPowerPolicyConstants;->TESTSUITSPECIFIC:Z

    if-nez v0, :cond_1

    sget-boolean v0, Lcom/miui/app/smartpower/SmartPowerSettings;->PROP_INTERCEPT_ENABLE:Z

    if-nez v0, :cond_0

    .line 944
    invoke-static {}, Lcom/miui/server/smartpower/SmartPowerPolicyManager;->isSpeedTestMode()Z

    move-result v0

    if-nez v0, :cond_0

    invoke-direct {p0}, Lcom/miui/server/smartpower/SmartPowerPolicyManager;->isEnableFrozen()Z

    move-result v0

    if-eqz v0, :cond_1

    :cond_0
    const/4 v0, 0x1

    goto :goto_0

    :cond_1
    const/4 v0, 0x0

    .line 943
    :goto_0
    return v0
.end method

.method public static isPackageInterceptList(Ljava/lang/String;)Z
    .locals 2
    .param p0, "pkgName"    # Ljava/lang/String;

    .line 1021
    sget-object v0, Lcom/miui/server/smartpower/SmartPowerPolicyManager;->sInterceptPackageList:Landroid/util/ArraySet;

    monitor-enter v0

    .line 1022
    :try_start_0
    invoke-virtual {v0, p0}, Landroid/util/ArraySet;->contains(Ljava/lang/Object;)Z

    move-result v1

    monitor-exit v0

    return v1

    .line 1023
    :catchall_0
    move-exception v1

    monitor-exit v0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v1
.end method

.method private isPackageWhiteList(Ljava/lang/String;)Z
    .locals 3
    .param p1, "packageName"    # Ljava/lang/String;

    .line 948
    iget-object v0, p0, Lcom/miui/server/smartpower/SmartPowerPolicyManager;->mPackageWhiteList:Landroid/util/ArrayMap;

    monitor-enter v0

    .line 949
    :try_start_0
    iget-object v1, p0, Lcom/miui/server/smartpower/SmartPowerPolicyManager;->mPackageWhiteList:Landroid/util/ArrayMap;

    invoke-virtual {v1, p1}, Landroid/util/ArrayMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/miui/server/smartpower/SmartPowerPolicyManager$WhiteListItem;

    .line 950
    .local v1, "item":Lcom/miui/server/smartpower/SmartPowerPolicyManager$WhiteListItem;
    if-eqz v1, :cond_1

    .line 951
    const/4 v2, 0x2

    invoke-virtual {v1, v2}, Lcom/miui/server/smartpower/SmartPowerPolicyManager$WhiteListItem;->hasAction(I)Z

    move-result v2

    if-nez v2, :cond_0

    iget-boolean v2, p0, Lcom/miui/server/smartpower/SmartPowerPolicyManager;->mScreenOff:Z

    if-nez v2, :cond_1

    .line 952
    const/4 v2, 0x4

    invoke-virtual {v1, v2}, Lcom/miui/server/smartpower/SmartPowerPolicyManager$WhiteListItem;->hasAction(I)Z

    move-result v2

    if-eqz v2, :cond_1

    .line 953
    :cond_0
    monitor-exit v0

    const/4 v0, 0x1

    return v0

    .line 955
    .end local v1    # "item":Lcom/miui/server/smartpower/SmartPowerPolicyManager$WhiteListItem;
    :cond_1
    monitor-exit v0

    .line 956
    const/4 v0, 0x0

    return v0

    .line 955
    :catchall_0
    move-exception v1

    monitor-exit v0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v1
.end method

.method private isPidWhiteList(I)Z
    .locals 3
    .param p1, "pid"    # I

    .line 1000
    iget-object v0, p0, Lcom/miui/server/smartpower/SmartPowerPolicyManager;->mPidWhiteList:Landroid/util/ArrayMap;

    monitor-enter v0

    .line 1001
    :try_start_0
    iget-object v1, p0, Lcom/miui/server/smartpower/SmartPowerPolicyManager;->mPidWhiteList:Landroid/util/ArrayMap;

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v1, v2}, Landroid/util/ArrayMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/miui/server/smartpower/SmartPowerPolicyManager$WhiteListItem;

    .line 1002
    .local v1, "item":Lcom/miui/server/smartpower/SmartPowerPolicyManager$WhiteListItem;
    if-eqz v1, :cond_0

    const/4 v2, 0x2

    invoke-virtual {v1, v2}, Lcom/miui/server/smartpower/SmartPowerPolicyManager$WhiteListItem;->hasAction(I)Z

    move-result v2

    if-eqz v2, :cond_0

    .line 1003
    monitor-exit v0

    const/4 v0, 0x1

    return v0

    .line 1005
    .end local v1    # "item":Lcom/miui/server/smartpower/SmartPowerPolicyManager$WhiteListItem;
    :cond_0
    monitor-exit v0

    .line 1006
    const/4 v0, 0x0

    return v0

    .line 1005
    :catchall_0
    move-exception v1

    monitor-exit v0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v1
.end method

.method private isProcessWhiteList(Ljava/lang/String;)Z
    .locals 3
    .param p1, "procName"    # Ljava/lang/String;

    .line 990
    iget-object v0, p0, Lcom/miui/server/smartpower/SmartPowerPolicyManager;->mProcessWhiteList:Landroid/util/ArrayMap;

    monitor-enter v0

    .line 991
    :try_start_0
    iget-object v1, p0, Lcom/miui/server/smartpower/SmartPowerPolicyManager;->mProcessWhiteList:Landroid/util/ArrayMap;

    invoke-virtual {v1, p1}, Landroid/util/ArrayMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/miui/server/smartpower/SmartPowerPolicyManager$WhiteListItem;

    .line 992
    .local v1, "item":Lcom/miui/server/smartpower/SmartPowerPolicyManager$WhiteListItem;
    if-eqz v1, :cond_0

    const/4 v2, 0x2

    invoke-virtual {v1, v2}, Lcom/miui/server/smartpower/SmartPowerPolicyManager$WhiteListItem;->hasAction(I)Z

    move-result v2

    if-eqz v2, :cond_0

    .line 993
    monitor-exit v0

    const/4 v0, 0x1

    return v0

    .line 995
    .end local v1    # "item":Lcom/miui/server/smartpower/SmartPowerPolicyManager$WhiteListItem;
    :cond_0
    monitor-exit v0

    .line 996
    const/4 v0, 0x0

    return v0

    .line 995
    :catchall_0
    move-exception v1

    monitor-exit v0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v1
.end method

.method public static isSpeedTestMode()Z
    .locals 1

    .line 679
    const-class v0, Lcom/miui/app/SpeedTestModeServiceInternal;

    invoke-static {v0}, Lcom/android/server/LocalServices;->getService(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/miui/app/SpeedTestModeServiceInternal;

    invoke-interface {v0}, Lcom/miui/app/SpeedTestModeServiceInternal;->isSpeedTestMode()Z

    move-result v0

    return v0
.end method

.method private isUidWhiteList(I)Z
    .locals 3
    .param p1, "uid"    # I

    .line 960
    iget-object v0, p0, Lcom/miui/server/smartpower/SmartPowerPolicyManager;->mUidWhiteList:Landroid/util/ArrayMap;

    monitor-enter v0

    .line 961
    :try_start_0
    iget-object v1, p0, Lcom/miui/server/smartpower/SmartPowerPolicyManager;->mUidWhiteList:Landroid/util/ArrayMap;

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v1, v2}, Landroid/util/ArrayMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/miui/server/smartpower/SmartPowerPolicyManager$WhiteListItem;

    .line 962
    .local v1, "item":Lcom/miui/server/smartpower/SmartPowerPolicyManager$WhiteListItem;
    if-eqz v1, :cond_0

    const/4 v2, 0x2

    invoke-virtual {v1, v2}, Lcom/miui/server/smartpower/SmartPowerPolicyManager$WhiteListItem;->hasAction(I)Z

    move-result v2

    if-eqz v2, :cond_0

    .line 963
    monitor-exit v0

    const/4 v0, 0x1

    return v0

    .line 965
    .end local v1    # "item":Lcom/miui/server/smartpower/SmartPowerPolicyManager$WhiteListItem;
    :cond_0
    monitor-exit v0

    .line 966
    const/4 v0, 0x0

    return v0

    .line 965
    :catchall_0
    move-exception v1

    monitor-exit v0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v1
.end method

.method private synthetic lambda$idleApp$0(Lcom/android/server/am/AppStateManager$AppState;)V
    .locals 6
    .param p1, "app"    # Lcom/android/server/am/AppStateManager$AppState;

    .line 809
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "power idle "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {p1}, Lcom/android/server/am/AppStateManager$AppState;->getPackageName()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "/"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {p1}, Lcom/android/server/am/AppStateManager$AppState;->getUid()I

    move-result v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    .line 810
    .local v0, "event":Ljava/lang/String;
    const-wide/32 v1, 0x20000

    invoke-static {v1, v2, v0}, Landroid/os/Trace;->traceBegin(JLjava/lang/String;)V

    .line 811
    invoke-virtual {p1}, Lcom/android/server/am/AppStateManager$AppState;->isAlive()Z

    move-result v3

    if-eqz v3, :cond_0

    invoke-virtual {p1}, Lcom/android/server/am/AppStateManager$AppState;->isIdle()Z

    move-result v3

    if-eqz v3, :cond_0

    invoke-virtual {p1}, Lcom/android/server/am/AppStateManager$AppState;->getUid()I

    move-result v3

    invoke-direct {p0, v3}, Lcom/miui/server/smartpower/SmartPowerPolicyManager;->isDeviceIdleWhiteList(I)Z

    move-result v3

    if-nez v3, :cond_0

    .line 813
    :try_start_0
    iget-object v3, p0, Lcom/miui/server/smartpower/SmartPowerPolicyManager;->mAMS:Lcom/android/server/am/ActivityManagerService;

    invoke-virtual {p1}, Lcom/android/server/am/AppStateManager$AppState;->getPackageName()Ljava/lang/String;

    move-result-object v4

    const/4 v5, -0x2

    invoke-virtual {v3, v4, v5}, Lcom/android/server/am/ActivityManagerService;->makePackageIdle(Ljava/lang/String;I)V

    .line 814
    const v3, 0x15ff2

    invoke-static {v3, v0}, Landroid/util/EventLog;->writeEvent(ILjava/lang/String;)I
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    .line 815
    :catch_0
    move-exception v3

    :goto_0
    nop

    .line 817
    :cond_0
    invoke-static {v1, v2}, Landroid/os/Trace;->traceEnd(J)V

    .line 818
    return-void
.end method

.method private lunchProcess(Lcom/android/server/am/AppStateManager$AppState$RunningProcess;)V
    .locals 1
    .param p1, "proc"    # Lcom/android/server/am/AppStateManager$AppState$RunningProcess;

    .line 757
    invoke-virtual {p1}, Lcom/android/server/am/AppStateManager$AppState$RunningProcess;->is64BitProcess()Z

    move-result v0

    if-nez v0, :cond_0

    .line 758
    iget v0, p0, Lcom/miui/server/smartpower/SmartPowerPolicyManager;->mRunning32ProcCount:I

    add-int/lit8 v0, v0, 0x1

    iput v0, p0, Lcom/miui/server/smartpower/SmartPowerPolicyManager;->mRunning32ProcCount:I

    .line 760
    :cond_0
    return-void
.end method

.method private queryUsageStats()Ljava/util/List;
    .locals 12
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List<",
            "Landroid/app/usage/UsageStats;",
            ">;"
        }
    .end annotation

    .line 1102
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v8

    .line 1103
    .local v8, "endTime":J
    const-wide/32 v0, 0x240c8400

    sub-long v10, v8, v0

    .line 1104
    .local v10, "beginTime":J
    iget-object v0, p0, Lcom/miui/server/smartpower/SmartPowerPolicyManager;->mUsageStats:Landroid/app/usage/UsageStatsManagerInternal;

    invoke-static {}, Landroid/os/UserHandle;->getCallingUserId()I

    move-result v1

    const/4 v2, 0x1

    const/4 v7, 0x0

    move-wide v3, v10

    move-wide v5, v8

    invoke-virtual/range {v0 .. v7}, Landroid/app/usage/UsageStatsManagerInternal;->queryUsageStatsForUser(IIJJZ)Ljava/util/List;

    move-result-object v0

    return-object v0
.end method

.method private registerTestSuitSpecificObserver()V
    .locals 5

    .line 1513
    new-instance v0, Lcom/miui/server/smartpower/SmartPowerPolicyManager$7;

    const/4 v1, 0x0

    invoke-direct {v0, p0, v1}, Lcom/miui/server/smartpower/SmartPowerPolicyManager$7;-><init>(Lcom/miui/server/smartpower/SmartPowerPolicyManager;Landroid/os/Handler;)V

    .line 1519
    .local v0, "observer":Landroid/database/ContentObserver;
    iget-object v1, p0, Lcom/miui/server/smartpower/SmartPowerPolicyManager;->mContext:Landroid/content/Context;

    invoke-virtual {v1}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v1

    sget-object v2, Landroid/provider/MiuiSettings$Secure;->MIUI_OPTIMIZATION:Ljava/lang/String;

    .line 1520
    invoke-static {v2}, Landroid/provider/Settings$Secure;->getUriFor(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v2

    .line 1519
    const/4 v3, 0x0

    const/4 v4, -0x2

    invoke-virtual {v1, v2, v3, v0, v4}, Landroid/content/ContentResolver;->registerContentObserver(Landroid/net/Uri;ZLandroid/database/ContentObserver;I)V

    .line 1522
    invoke-virtual {v0, v3}, Landroid/database/ContentObserver;->onChange(Z)V

    .line 1523
    return-void
.end method

.method private removeDynamicWhiteListLocked(Landroid/util/ArrayMap;Ljava/lang/Integer;Ljava/lang/String;II)Lcom/miui/server/smartpower/SmartPowerPolicyManager$WhiteListItem;
    .locals 3
    .param p2, "key"    # Ljava/lang/Integer;
    .param p3, "name"    # Ljava/lang/String;
    .param p4, "type"    # I
    .param p5, "action"    # I
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/util/ArrayMap<",
            "Ljava/lang/Integer;",
            "Lcom/miui/server/smartpower/SmartPowerPolicyManager$WhiteListItem;",
            ">;",
            "Ljava/lang/Integer;",
            "Ljava/lang/String;",
            "II)",
            "Lcom/miui/server/smartpower/SmartPowerPolicyManager$WhiteListItem;"
        }
    .end annotation

    .line 1275
    .local p1, "whiteList":Landroid/util/ArrayMap;, "Landroid/util/ArrayMap<Ljava/lang/Integer;Lcom/miui/server/smartpower/SmartPowerPolicyManager$WhiteListItem;>;"
    invoke-virtual {p1, p2}, Landroid/util/ArrayMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/miui/server/smartpower/SmartPowerPolicyManager$WhiteListItem;

    .line 1276
    .local v0, "item":Lcom/miui/server/smartpower/SmartPowerPolicyManager$WhiteListItem;
    if-eqz v0, :cond_1

    .line 1277
    invoke-static {v0}, Lcom/miui/server/smartpower/SmartPowerPolicyManager$WhiteListItem;->-$$Nest$fgetmTypes(Lcom/miui/server/smartpower/SmartPowerPolicyManager$WhiteListItem;)I

    move-result v1

    not-int v2, p4

    and-int/2addr v1, v2

    invoke-static {v0, v1}, Lcom/miui/server/smartpower/SmartPowerPolicyManager$WhiteListItem;->-$$Nest$fputmTypes(Lcom/miui/server/smartpower/SmartPowerPolicyManager$WhiteListItem;I)V

    .line 1278
    invoke-static {v0}, Lcom/miui/server/smartpower/SmartPowerPolicyManager$WhiteListItem;->-$$Nest$fgetmTypes(Lcom/miui/server/smartpower/SmartPowerPolicyManager$WhiteListItem;)I

    move-result v1

    invoke-static {p5}, Lcom/miui/server/smartpower/SmartPowerPolicyManager;->getWhiteListTypeMask(I)I

    move-result v2

    and-int/2addr v1, v2

    if-nez v1, :cond_0

    .line 1279
    invoke-static {v0}, Lcom/miui/server/smartpower/SmartPowerPolicyManager$WhiteListItem;->-$$Nest$fgetmActions(Lcom/miui/server/smartpower/SmartPowerPolicyManager$WhiteListItem;)I

    move-result v1

    not-int v2, p5

    and-int/2addr v1, v2

    invoke-static {v0, v1}, Lcom/miui/server/smartpower/SmartPowerPolicyManager$WhiteListItem;->-$$Nest$fputmActions(Lcom/miui/server/smartpower/SmartPowerPolicyManager$WhiteListItem;I)V

    .line 1281
    :cond_0
    invoke-static {v0}, Lcom/miui/server/smartpower/SmartPowerPolicyManager$WhiteListItem;->-$$Nest$fgetmTypes(Lcom/miui/server/smartpower/SmartPowerPolicyManager$WhiteListItem;)I

    move-result v1

    if-nez v1, :cond_1

    invoke-static {v0}, Lcom/miui/server/smartpower/SmartPowerPolicyManager$WhiteListItem;->-$$Nest$fgetmActions(Lcom/miui/server/smartpower/SmartPowerPolicyManager$WhiteListItem;)I

    move-result v1

    if-nez v1, :cond_1

    .line 1282
    invoke-virtual {p1, p2}, Landroid/util/ArrayMap;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    .line 1283
    return-object v0

    .line 1286
    :cond_1
    const/4 v1, 0x0

    return-object v1
.end method

.method private removePidWhiteList(IILjava/lang/String;I)V
    .locals 7
    .param p1, "uid"    # I
    .param p2, "pid"    # I
    .param p3, "procName"    # Ljava/lang/String;
    .param p4, "type"    # I

    .line 1168
    iget-object v0, p0, Lcom/miui/server/smartpower/SmartPowerPolicyManager;->mPidWhiteList:Landroid/util/ArrayMap;

    monitor-enter v0

    .line 1169
    :try_start_0
    iget-object v2, p0, Lcom/miui/server/smartpower/SmartPowerPolicyManager;->mPidWhiteList:Landroid/util/ArrayMap;

    invoke-static {p2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    const/4 v6, 0x2

    move-object v1, p0

    move-object v4, p3

    move v5, p4

    invoke-direct/range {v1 .. v6}, Lcom/miui/server/smartpower/SmartPowerPolicyManager;->removeDynamicWhiteListLocked(Landroid/util/ArrayMap;Ljava/lang/Integer;Ljava/lang/String;II)Lcom/miui/server/smartpower/SmartPowerPolicyManager$WhiteListItem;

    move-result-object v1

    .line 1171
    .local v1, "item":Lcom/miui/server/smartpower/SmartPowerPolicyManager$WhiteListItem;
    monitor-exit v0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 1172
    if-eqz v1, :cond_0

    .line 1173
    iget-object v0, p0, Lcom/miui/server/smartpower/SmartPowerPolicyManager;->mHandler:Landroid/os/Handler;

    new-instance v2, Lcom/miui/server/smartpower/SmartPowerPolicyManager$6;

    invoke-direct {v2, p0, p1, p2}, Lcom/miui/server/smartpower/SmartPowerPolicyManager$6;-><init>(Lcom/miui/server/smartpower/SmartPowerPolicyManager;II)V

    invoke-virtual {v0, v2}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    .line 1180
    :cond_0
    return-void

    .line 1171
    .end local v1    # "item":Lcom/miui/server/smartpower/SmartPowerPolicyManager$WhiteListItem;
    :catchall_0
    move-exception v1

    :try_start_1
    monitor-exit v0
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    throw v1
.end method

.method private removeUidWhiteList(ILjava/lang/String;I)V
    .locals 7
    .param p1, "uid"    # I
    .param p2, "pkgName"    # Ljava/lang/String;
    .param p3, "type"    # I

    .line 1127
    iget-object v0, p0, Lcom/miui/server/smartpower/SmartPowerPolicyManager;->mUidWhiteList:Landroid/util/ArrayMap;

    monitor-enter v0

    .line 1128
    :try_start_0
    iget-object v2, p0, Lcom/miui/server/smartpower/SmartPowerPolicyManager;->mUidWhiteList:Landroid/util/ArrayMap;

    .line 1129
    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    const/4 v6, 0x2

    .line 1128
    move-object v1, p0

    move-object v4, p2

    move v5, p3

    invoke-direct/range {v1 .. v6}, Lcom/miui/server/smartpower/SmartPowerPolicyManager;->removeDynamicWhiteListLocked(Landroid/util/ArrayMap;Ljava/lang/Integer;Ljava/lang/String;II)Lcom/miui/server/smartpower/SmartPowerPolicyManager$WhiteListItem;

    move-result-object v1

    .line 1130
    .local v1, "item":Lcom/miui/server/smartpower/SmartPowerPolicyManager$WhiteListItem;
    monitor-exit v0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 1131
    if-eqz v1, :cond_0

    .line 1132
    iget-object v0, p0, Lcom/miui/server/smartpower/SmartPowerPolicyManager;->mHandler:Landroid/os/Handler;

    new-instance v2, Lcom/miui/server/smartpower/SmartPowerPolicyManager$4;

    invoke-direct {v2, p0, p1}, Lcom/miui/server/smartpower/SmartPowerPolicyManager$4;-><init>(Lcom/miui/server/smartpower/SmartPowerPolicyManager;I)V

    invoke-virtual {v0, v2}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    .line 1139
    :cond_0
    return-void

    .line 1130
    .end local v1    # "item":Lcom/miui/server/smartpower/SmartPowerPolicyManager$WhiteListItem;
    :catchall_0
    move-exception v1

    :try_start_1
    monitor-exit v0
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    throw v1
.end method

.method private thawProcess(IILjava/lang/String;)Z
    .locals 2
    .param p1, "uid"    # I
    .param p2, "pid"    # I
    .param p3, "reason"    # Ljava/lang/String;

    .line 893
    const/4 v0, 0x0

    .line 894
    .local v0, "done":Z
    if-lez p2, :cond_0

    iget-object v1, p0, Lcom/miui/server/smartpower/SmartPowerPolicyManager;->mPowerFrozenManager:Lcom/miui/server/smartpower/PowerFrozenManager;

    invoke-virtual {v1, p2}, Lcom/miui/server/smartpower/PowerFrozenManager;->isAllFrozenPid(I)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 895
    iget-object v1, p0, Lcom/miui/server/smartpower/SmartPowerPolicyManager;->mPowerFrozenManager:Lcom/miui/server/smartpower/PowerFrozenManager;

    invoke-virtual {v1, p1, p2, p3}, Lcom/miui/server/smartpower/PowerFrozenManager;->thawProcess(IILjava/lang/String;)Z

    move-result v0

    .line 897
    :cond_0
    return v0
.end method

.method private updateUsageStats()V
    .locals 8

    .line 1075
    const-string/jumbo v0, "updateUsageStats"

    const-wide/32 v1, 0x20000

    invoke-static {v1, v2, v0}, Landroid/os/Trace;->traceBegin(JLjava/lang/String;)V

    .line 1076
    invoke-direct {p0}, Lcom/miui/server/smartpower/SmartPowerPolicyManager;->queryUsageStats()Ljava/util/List;

    move-result-object v0

    .line 1077
    .local v0, "usageStats":Ljava/util/List;, "Ljava/util/List<Landroid/app/usage/UsageStats;>;"
    if-eqz v0, :cond_3

    invoke-interface {v0}, Ljava/util/List;->isEmpty()Z

    move-result v3

    if-eqz v3, :cond_0

    goto :goto_2

    .line 1081
    :cond_0
    new-instance v3, Ljava/util/HashMap;

    invoke-direct {v3}, Ljava/util/HashMap;-><init>()V

    .line 1082
    .local v3, "usageStatsMap":Ljava/util/HashMap;, "Ljava/util/HashMap<Ljava/lang/String;Lcom/miui/server/smartpower/SmartPowerPolicyManager$UsageStatsInfo;>;"
    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v4

    :goto_0
    invoke-interface {v4}, Ljava/util/Iterator;->hasNext()Z

    move-result v5

    if-eqz v5, :cond_2

    invoke-interface {v4}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Landroid/app/usage/UsageStats;

    .line 1083
    .local v5, "st":Landroid/app/usage/UsageStats;
    iget-object v6, v5, Landroid/app/usage/UsageStats;->mPackageName:Ljava/lang/String;

    invoke-virtual {v3, v6}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v6

    check-cast v6, Lcom/miui/server/smartpower/SmartPowerPolicyManager$UsageStatsInfo;

    .line 1084
    .local v6, "info":Lcom/miui/server/smartpower/SmartPowerPolicyManager$UsageStatsInfo;
    if-nez v6, :cond_1

    .line 1085
    new-instance v7, Lcom/miui/server/smartpower/SmartPowerPolicyManager$UsageStatsInfo;

    invoke-direct {v7, p0, v5}, Lcom/miui/server/smartpower/SmartPowerPolicyManager$UsageStatsInfo;-><init>(Lcom/miui/server/smartpower/SmartPowerPolicyManager;Landroid/app/usage/UsageStats;)V

    move-object v6, v7

    .line 1086
    iget-object v7, v5, Landroid/app/usage/UsageStats;->mPackageName:Ljava/lang/String;

    invoke-virtual {v3, v7, v6}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    goto :goto_1

    .line 1088
    :cond_1
    invoke-virtual {v6, v5}, Lcom/miui/server/smartpower/SmartPowerPolicyManager$UsageStatsInfo;->updateStatsInfo(Landroid/app/usage/UsageStats;)V

    .line 1090
    .end local v5    # "st":Landroid/app/usage/UsageStats;
    .end local v6    # "info":Lcom/miui/server/smartpower/SmartPowerPolicyManager$UsageStatsInfo;
    :goto_1
    goto :goto_0

    .line 1091
    :cond_2
    iget-object v4, p0, Lcom/miui/server/smartpower/SmartPowerPolicyManager;->mUsageStatsMap:Ljava/util/HashMap;

    monitor-enter v4

    .line 1092
    :try_start_0
    iget-object v5, p0, Lcom/miui/server/smartpower/SmartPowerPolicyManager;->mUsageStatsMap:Ljava/util/HashMap;

    invoke-virtual {v5}, Ljava/util/HashMap;->clear()V

    .line 1093
    iget-object v5, p0, Lcom/miui/server/smartpower/SmartPowerPolicyManager;->mUsageStatsMap:Ljava/util/HashMap;

    invoke-virtual {v5, v3}, Ljava/util/HashMap;->putAll(Ljava/util/Map;)V

    .line 1094
    monitor-exit v4
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 1095
    iget-object v4, p0, Lcom/miui/server/smartpower/SmartPowerPolicyManager;->mAppStateManager:Lcom/android/server/am/AppStateManager;

    invoke-virtual {v4}, Lcom/android/server/am/AppStateManager;->updateAllAppUsageStats()Ljava/util/ArrayList;

    .line 1096
    iget-object v4, p0, Lcom/miui/server/smartpower/SmartPowerPolicyManager;->mHandler:Landroid/os/Handler;

    const/4 v5, 0x1

    invoke-virtual {v4, v5}, Landroid/os/Handler;->removeMessages(I)V

    .line 1097
    iget-object v4, p0, Lcom/miui/server/smartpower/SmartPowerPolicyManager;->mHandler:Landroid/os/Handler;

    const-wide/32 v6, 0x1b7740

    invoke-virtual {v4, v5, v6, v7}, Landroid/os/Handler;->sendEmptyMessageDelayed(IJ)Z

    .line 1098
    invoke-static {v1, v2}, Landroid/os/Trace;->traceEnd(J)V

    .line 1099
    return-void

    .line 1094
    :catchall_0
    move-exception v1

    :try_start_1
    monitor-exit v4
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    throw v1

    .line 1078
    .end local v3    # "usageStatsMap":Ljava/util/HashMap;, "Ljava/util/HashMap<Ljava/lang/String;Lcom/miui/server/smartpower/SmartPowerPolicyManager$UsageStatsInfo;>;"
    :cond_3
    :goto_2
    invoke-static {v1, v2}, Landroid/os/Trace;->traceEnd(J)V

    .line 1079
    return-void
.end method

.method private static whiteListActionToString(I)Ljava/lang/String;
    .locals 3
    .param p0, "action"    # I

    .line 1402
    const-string v0, ""

    .line 1403
    .local v0, "actionStr":Ljava/lang/String;
    and-int/lit8 v1, p0, 0x2

    if-eqz v1, :cond_0

    .line 1404
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "hibernation, "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    .line 1406
    :cond_0
    and-int/lit8 v1, p0, 0x4

    if-eqz v1, :cond_1

    .line 1407
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string/jumbo v2, "screenon, "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    .line 1409
    :cond_1
    and-int/lit8 v1, p0, 0x8

    if-eqz v1, :cond_2

    .line 1410
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "alarm, "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    .line 1412
    :cond_2
    and-int/lit8 v1, p0, 0x10

    if-eqz v1, :cond_3

    .line 1413
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string/jumbo v2, "service, "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    .line 1415
    :cond_3
    and-int/lit8 v1, p0, 0x20

    if-eqz v1, :cond_4

    .line 1416
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "provider, "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    .line 1418
    :cond_4
    return-object v0
.end method

.method private static whiteListTypeToString(I)Ljava/lang/String;
    .locals 3
    .param p0, "type"    # I

    .line 1348
    const-string v0, ""

    .line 1349
    .local v0, "typeStr":Ljava/lang/String;
    and-int/lit8 v1, p0, 0x2

    if-nez v1, :cond_0

    and-int/lit16 v1, p0, 0x1000

    if-nez v1, :cond_0

    const v1, 0x8000

    and-int/2addr v1, p0

    if-nez v1, :cond_0

    const/high16 v1, 0x40000

    and-int/2addr v1, p0

    if-nez v1, :cond_0

    const/high16 v1, 0x200000

    and-int/2addr v1, p0

    if-eqz v1, :cond_1

    .line 1354
    :cond_0
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "default, "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    .line 1356
    :cond_1
    and-int/lit8 v1, p0, 0x4

    if-nez v1, :cond_2

    and-int/lit16 v1, p0, 0x2000

    if-nez v1, :cond_2

    const/high16 v1, 0x10000

    and-int/2addr v1, p0

    if-nez v1, :cond_2

    const/high16 v1, 0x80000

    and-int/2addr v1, p0

    if-nez v1, :cond_2

    const/high16 v1, 0x400000

    and-int/2addr v1, p0

    if-eqz v1, :cond_3

    .line 1361
    :cond_2
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "cloud control, "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    .line 1363
    :cond_3
    and-int/lit8 v1, p0, 0x8

    if-eqz v1, :cond_4

    .line 1364
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string/jumbo v2, "vpn, "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    .line 1366
    :cond_4
    and-int/lit8 v1, p0, 0x10

    if-eqz v1, :cond_5

    .line 1367
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string/jumbo v2, "wallpaper, "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    .line 1369
    :cond_5
    and-int/lit8 v1, p0, 0x20

    if-eqz v1, :cond_6

    .line 1370
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "frequent thaw, "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    .line 1372
    :cond_6
    and-int/lit8 v1, p0, 0x40

    if-eqz v1, :cond_7

    .line 1373
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "backup, "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    .line 1375
    :cond_7
    and-int/lit16 v1, p0, 0x80

    if-eqz v1, :cond_8

    .line 1376
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string/jumbo v2, "usb, "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    .line 1378
    :cond_8
    and-int/lit16 v1, p0, 0x100

    if-eqz v1, :cond_9

    .line 1379
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "depend, "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    .line 1381
    :cond_9
    and-int/lit16 v1, p0, 0x200

    if-eqz v1, :cond_a

    .line 1382
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "external audio focus policy, "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    .line 1384
    :cond_a
    and-int/lit16 v1, p0, 0x400

    if-eqz v1, :cond_b

    .line 1385
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "cts, "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    .line 1387
    :cond_b
    return-object v0
.end method


# virtual methods
.method public dump(Ljava/io/PrintWriter;[Ljava/lang/String;I)V
    .locals 5
    .param p1, "pw"    # Ljava/io/PrintWriter;
    .param p2, "args"    # [Ljava/lang/String;
    .param p3, "opti"    # I

    .line 1422
    add-int/lit8 v0, p3, 0x1

    array-length v1, p2

    if-ge v0, v1, :cond_8

    .line 1423
    const-string v0, "add white list:"

    invoke-virtual {p1, v0}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V

    .line 1424
    add-int/lit8 v0, p3, 0x1

    .end local p3    # "opti":I
    .local v0, "opti":I
    aget-object p3, p2, p3

    .line 1425
    .local p3, "parm":Ljava/lang/String;
    add-int/lit8 v1, v0, 0x1

    .end local v0    # "opti":I
    .local v1, "opti":I
    aget-object v0, p2, v0

    .line 1426
    .local v0, "value":Ljava/lang/String;
    const-string v2, "addProviderPkg"

    invoke-virtual {p3, v2}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v2

    if-eqz v2, :cond_0

    .line 1427
    invoke-direct {p0, v0}, Lcom/miui/server/smartpower/SmartPowerPolicyManager;->addProviderPackageWhiteList(Ljava/lang/String;)V

    goto :goto_0

    .line 1428
    :cond_0
    const-string v2, "addServicePkg"

    invoke-virtual {p3, v2}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v2

    if-eqz v2, :cond_1

    .line 1429
    invoke-direct {p0, v0}, Lcom/miui/server/smartpower/SmartPowerPolicyManager;->addServicePackageWhiteList(Ljava/lang/String;)V

    goto :goto_0

    .line 1430
    :cond_1
    const-string v2, "addAlarmPkg"

    invoke-virtual {p3, v2}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v2

    if-eqz v2, :cond_2

    .line 1431
    invoke-direct {p0, v0}, Lcom/miui/server/smartpower/SmartPowerPolicyManager;->addAlarmPackageWhiteList(Ljava/lang/String;)V

    goto :goto_0

    .line 1432
    :cond_2
    const-string v2, "addBroadcastProc"

    invoke-virtual {p3, v2}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v2

    if-eqz v2, :cond_3

    .line 1433
    invoke-direct {p0, v0}, Lcom/miui/server/smartpower/SmartPowerPolicyManager;->addBroadcastProcessActionWhiteList(Ljava/lang/String;)V

    goto :goto_0

    .line 1434
    :cond_3
    const-string v2, "addFrozenPkg"

    invoke-virtual {p3, v2}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v2

    if-eqz v2, :cond_4

    .line 1435
    const/4 v2, 0x4

    invoke-direct {p0, v0, v2}, Lcom/miui/server/smartpower/SmartPowerPolicyManager;->addPackageWhiteList(Ljava/lang/String;I)V

    goto :goto_0

    .line 1436
    :cond_4
    const-string v2, "addBroadcastBlackAction"

    invoke-virtual {p3, v2}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v2

    if-eqz v2, :cond_5

    .line 1437
    invoke-direct {p0, v0}, Lcom/miui/server/smartpower/SmartPowerPolicyManager;->addBroadcastBlackList(Ljava/lang/String;)V

    goto :goto_0

    .line 1438
    :cond_5
    const-string v2, "addServiceBlackPkg"

    invoke-virtual {p3, v2}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v2

    if-eqz v2, :cond_6

    .line 1439
    invoke-direct {p0, v0}, Lcom/miui/server/smartpower/SmartPowerPolicyManager;->addServicePkgBlackList(Ljava/lang/String;)V

    goto :goto_0

    .line 1440
    :cond_6
    const-string v2, "addProviderBlackPkg"

    invoke-virtual {p3, v2}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v2

    if-eqz v2, :cond_7

    .line 1441
    invoke-direct {p0, v0}, Lcom/miui/server/smartpower/SmartPowerPolicyManager;->addProviderPkgBlackList(Ljava/lang/String;)V

    .line 1447
    .end local v0    # "value":Ljava/lang/String;
    .end local p3    # "parm":Ljava/lang/String;
    :goto_0
    move p3, v1

    goto :goto_1

    .line 1443
    .restart local v0    # "value":Ljava/lang/String;
    .restart local p3    # "parm":Ljava/lang/String;
    :cond_7
    const-string v2, "The parameter is invalid"

    invoke-virtual {p1, v2}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V

    .line 1444
    return-void

    .line 1447
    .end local v0    # "value":Ljava/lang/String;
    .end local v1    # "opti":I
    .local p3, "opti":I
    :cond_8
    :goto_1
    const-string v0, "PackageWhiteList:"

    invoke-virtual {p1, v0}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V

    .line 1448
    iget-object v0, p0, Lcom/miui/server/smartpower/SmartPowerPolicyManager;->mPackageWhiteList:Landroid/util/ArrayMap;

    monitor-enter v0

    .line 1449
    :try_start_0
    iget-object v1, p0, Lcom/miui/server/smartpower/SmartPowerPolicyManager;->mPackageWhiteList:Landroid/util/ArrayMap;

    invoke-virtual {v1}, Landroid/util/ArrayMap;->entrySet()Ljava/util/Set;

    move-result-object v1

    invoke-interface {v1}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_2
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_9

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/util/Map$Entry;

    .line 1450
    .local v2, "entry":Ljava/util/Map$Entry;, "Ljava/util/Map$Entry<Ljava/lang/String;Lcom/miui/server/smartpower/SmartPowerPolicyManager$WhiteListItem;>;"
    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "    "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-interface {v2}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {p1, v3}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V

    .line 1451
    .end local v2    # "entry":Ljava/util/Map$Entry;, "Ljava/util/Map$Entry<Ljava/lang/String;Lcom/miui/server/smartpower/SmartPowerPolicyManager$WhiteListItem;>;"
    goto :goto_2

    .line 1452
    :cond_9
    monitor-exit v0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_8

    .line 1453
    const-string v0, "ProcessWhiteList:"

    invoke-virtual {p1, v0}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V

    .line 1454
    iget-object v1, p0, Lcom/miui/server/smartpower/SmartPowerPolicyManager;->mProcessWhiteList:Landroid/util/ArrayMap;

    monitor-enter v1

    .line 1455
    :try_start_1
    iget-object v0, p0, Lcom/miui/server/smartpower/SmartPowerPolicyManager;->mProcessWhiteList:Landroid/util/ArrayMap;

    invoke-virtual {v0}, Landroid/util/ArrayMap;->entrySet()Ljava/util/Set;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v0

    :goto_3
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_a

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/util/Map$Entry;

    .line 1456
    .restart local v2    # "entry":Ljava/util/Map$Entry;, "Ljava/util/Map$Entry<Ljava/lang/String;Lcom/miui/server/smartpower/SmartPowerPolicyManager$WhiteListItem;>;"
    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "    "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-interface {v2}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {p1, v3}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V

    .line 1457
    .end local v2    # "entry":Ljava/util/Map$Entry;, "Ljava/util/Map$Entry<Ljava/lang/String;Lcom/miui/server/smartpower/SmartPowerPolicyManager$WhiteListItem;>;"
    goto :goto_3

    .line 1458
    :cond_a
    monitor-exit v1
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_7

    .line 1459
    const-string v0, "UidWhiteList:"

    invoke-virtual {p1, v0}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V

    .line 1460
    iget-object v0, p0, Lcom/miui/server/smartpower/SmartPowerPolicyManager;->mUidWhiteList:Landroid/util/ArrayMap;

    monitor-enter v0

    .line 1461
    :try_start_2
    iget-object v1, p0, Lcom/miui/server/smartpower/SmartPowerPolicyManager;->mUidWhiteList:Landroid/util/ArrayMap;

    invoke-virtual {v1}, Landroid/util/ArrayMap;->entrySet()Ljava/util/Set;

    move-result-object v1

    invoke-interface {v1}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_4
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_b

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/util/Map$Entry;

    .line 1462
    .local v2, "entry":Ljava/util/Map$Entry;, "Ljava/util/Map$Entry<Ljava/lang/Integer;Lcom/miui/server/smartpower/SmartPowerPolicyManager$WhiteListItem;>;"
    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "    "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-interface {v2}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {p1, v3}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V

    .line 1463
    .end local v2    # "entry":Ljava/util/Map$Entry;, "Ljava/util/Map$Entry<Ljava/lang/Integer;Lcom/miui/server/smartpower/SmartPowerPolicyManager$WhiteListItem;>;"
    goto :goto_4

    .line 1464
    :cond_b
    monitor-exit v0
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_6

    .line 1465
    const-string v0, "PidWhiteList:"

    invoke-virtual {p1, v0}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V

    .line 1466
    iget-object v1, p0, Lcom/miui/server/smartpower/SmartPowerPolicyManager;->mPidWhiteList:Landroid/util/ArrayMap;

    monitor-enter v1

    .line 1467
    :try_start_3
    iget-object v0, p0, Lcom/miui/server/smartpower/SmartPowerPolicyManager;->mPidWhiteList:Landroid/util/ArrayMap;

    invoke-virtual {v0}, Landroid/util/ArrayMap;->entrySet()Ljava/util/Set;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v0

    :goto_5
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_c

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/util/Map$Entry;

    .line 1468
    .restart local v2    # "entry":Ljava/util/Map$Entry;, "Ljava/util/Map$Entry<Ljava/lang/Integer;Lcom/miui/server/smartpower/SmartPowerPolicyManager$WhiteListItem;>;"
    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "    "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-interface {v2}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {p1, v3}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V

    .line 1469
    .end local v2    # "entry":Ljava/util/Map$Entry;, "Ljava/util/Map$Entry<Ljava/lang/Integer;Lcom/miui/server/smartpower/SmartPowerPolicyManager$WhiteListItem;>;"
    goto :goto_5

    .line 1470
    :cond_c
    monitor-exit v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_5

    .line 1472
    const-string v0, "BroadcastWhiteList:"

    invoke-virtual {p1, v0}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V

    .line 1473
    iget-object v0, p0, Lcom/miui/server/smartpower/SmartPowerPolicyManager;->mBroadcastWhiteList:Landroid/util/ArraySet;

    monitor-enter v0

    .line 1474
    const/4 v1, 0x0

    .local v1, "i":I
    :goto_6
    :try_start_4
    iget-object v2, p0, Lcom/miui/server/smartpower/SmartPowerPolicyManager;->mBroadcastWhiteList:Landroid/util/ArraySet;

    invoke-virtual {v2}, Landroid/util/ArraySet;->size()I

    move-result v2

    if-ge v1, v2, :cond_d

    .line 1475
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "    "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget-object v3, p0, Lcom/miui/server/smartpower/SmartPowerPolicyManager;->mBroadcastWhiteList:Landroid/util/ArraySet;

    invoke-virtual {v3, v1}, Landroid/util/ArraySet;->valueAt(I)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {p1, v2}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V

    .line 1474
    add-int/lit8 v1, v1, 0x1

    goto :goto_6

    .line 1477
    .end local v1    # "i":I
    :cond_d
    monitor-exit v0
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_4

    .line 1479
    const-string v0, "BroadcastProcessWhiteList:"

    invoke-virtual {p1, v0}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V

    .line 1480
    iget-object v1, p0, Lcom/miui/server/smartpower/SmartPowerPolicyManager;->mBroadcastProcessActionWhiteList:Landroid/util/ArraySet;

    monitor-enter v1

    .line 1481
    const/4 v0, 0x0

    .local v0, "i":I
    :goto_7
    :try_start_5
    iget-object v2, p0, Lcom/miui/server/smartpower/SmartPowerPolicyManager;->mBroadcastProcessActionWhiteList:Landroid/util/ArraySet;

    invoke-virtual {v2}, Landroid/util/ArraySet;->size()I

    move-result v2

    if-ge v0, v2, :cond_e

    .line 1482
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "    "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget-object v3, p0, Lcom/miui/server/smartpower/SmartPowerPolicyManager;->mBroadcastProcessActionWhiteList:Landroid/util/ArraySet;

    invoke-virtual {v3, v0}, Landroid/util/ArraySet;->valueAt(I)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {p1, v2}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V

    .line 1481
    add-int/lit8 v0, v0, 0x1

    goto :goto_7

    .line 1484
    .end local v0    # "i":I
    :cond_e
    monitor-exit v1
    :try_end_5
    .catchall {:try_start_5 .. :try_end_5} :catchall_3

    .line 1486
    const-string v0, "InterceptPackageList:"

    invoke-virtual {p1, v0}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V

    .line 1487
    sget-object v0, Lcom/miui/server/smartpower/SmartPowerPolicyManager;->sInterceptPackageList:Landroid/util/ArraySet;

    monitor-enter v0

    .line 1488
    const/4 v1, 0x0

    .restart local v1    # "i":I
    :goto_8
    :try_start_6
    sget-object v2, Lcom/miui/server/smartpower/SmartPowerPolicyManager;->sInterceptPackageList:Landroid/util/ArraySet;

    invoke-virtual {v2}, Landroid/util/ArraySet;->size()I

    move-result v3

    if-ge v1, v3, :cond_f

    .line 1489
    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "    "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v2, v1}, Landroid/util/ArraySet;->valueAt(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/lang/String;

    invoke-virtual {v3, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {p1, v2}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V

    .line 1488
    add-int/lit8 v1, v1, 0x1

    goto :goto_8

    .line 1491
    .end local v1    # "i":I
    :cond_f
    monitor-exit v0
    :try_end_6
    .catchall {:try_start_6 .. :try_end_6} :catchall_2

    .line 1493
    const-string v0, "Dependency:"

    invoke-virtual {p1, v0}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V

    .line 1494
    iget-object v1, p0, Lcom/miui/server/smartpower/SmartPowerPolicyManager;->mDependencyMap:Landroid/util/ArrayMap;

    monitor-enter v1

    .line 1495
    :try_start_7
    iget-object v0, p0, Lcom/miui/server/smartpower/SmartPowerPolicyManager;->mDependencyMap:Landroid/util/ArrayMap;

    invoke-virtual {v0}, Landroid/util/ArrayMap;->entrySet()Ljava/util/Set;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v0

    :goto_9
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_10

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/util/Map$Entry;

    .line 1496
    .local v2, "entry":Ljava/util/Map$Entry;, "Ljava/util/Map$Entry<Ljava/lang/Integer;Landroid/util/ArraySet<Ljava/lang/String;>;>;"
    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "    "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-interface {v2}, Ljava/util/Map$Entry;->getKey()Ljava/lang/Object;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, " "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-interface {v2}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {p1, v3}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V

    .line 1497
    .end local v2    # "entry":Ljava/util/Map$Entry;, "Ljava/util/Map$Entry<Ljava/lang/Integer;Landroid/util/ArraySet<Ljava/lang/String;>;>;"
    goto :goto_9

    .line 1498
    :cond_10
    monitor-exit v1
    :try_end_7
    .catchall {:try_start_7 .. :try_end_7} :catchall_1

    .line 1500
    const-string v0, "UsageStats:"

    invoke-virtual {p1, v0}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V

    .line 1501
    iget-object v0, p0, Lcom/miui/server/smartpower/SmartPowerPolicyManager;->mUsageStatsMap:Ljava/util/HashMap;

    monitor-enter v0

    .line 1502
    :try_start_8
    iget-object v1, p0, Lcom/miui/server/smartpower/SmartPowerPolicyManager;->mUsageStatsMap:Ljava/util/HashMap;

    invoke-virtual {v1}, Ljava/util/HashMap;->values()Ljava/util/Collection;

    move-result-object v1

    invoke-interface {v1}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_a
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_11

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/miui/server/smartpower/SmartPowerPolicyManager$UsageStatsInfo;

    .line 1503
    .local v2, "info":Lcom/miui/server/smartpower/SmartPowerPolicyManager$UsageStatsInfo;
    invoke-virtual {v2, p1, p2, p3}, Lcom/miui/server/smartpower/SmartPowerPolicyManager$UsageStatsInfo;->dump(Ljava/io/PrintWriter;[Ljava/lang/String;I)V

    .line 1504
    .end local v2    # "info":Lcom/miui/server/smartpower/SmartPowerPolicyManager$UsageStatsInfo;
    goto :goto_a

    .line 1505
    :cond_11
    monitor-exit v0

    .line 1506
    return-void

    .line 1505
    :catchall_0
    move-exception v1

    monitor-exit v0
    :try_end_8
    .catchall {:try_start_8 .. :try_end_8} :catchall_0

    throw v1

    .line 1498
    :catchall_1
    move-exception v0

    :try_start_9
    monitor-exit v1
    :try_end_9
    .catchall {:try_start_9 .. :try_end_9} :catchall_1

    throw v0

    .line 1491
    :catchall_2
    move-exception v1

    :try_start_a
    monitor-exit v0
    :try_end_a
    .catchall {:try_start_a .. :try_end_a} :catchall_2

    throw v1

    .line 1484
    :catchall_3
    move-exception v0

    :try_start_b
    monitor-exit v1
    :try_end_b
    .catchall {:try_start_b .. :try_end_b} :catchall_3

    throw v0

    .line 1477
    :catchall_4
    move-exception v1

    :try_start_c
    monitor-exit v0
    :try_end_c
    .catchall {:try_start_c .. :try_end_c} :catchall_4

    throw v1

    .line 1470
    :catchall_5
    move-exception v0

    :try_start_d
    monitor-exit v1
    :try_end_d
    .catchall {:try_start_d .. :try_end_d} :catchall_5

    throw v0

    .line 1464
    :catchall_6
    move-exception v1

    :try_start_e
    monitor-exit v0
    :try_end_e
    .catchall {:try_start_e .. :try_end_e} :catchall_6

    throw v1

    .line 1458
    :catchall_7
    move-exception v0

    :try_start_f
    monitor-exit v1
    :try_end_f
    .catchall {:try_start_f .. :try_end_f} :catchall_7

    throw v0

    .line 1452
    :catchall_8
    move-exception v1

    :try_start_10
    monitor-exit v0
    :try_end_10
    .catchall {:try_start_10 .. :try_end_10} :catchall_8

    throw v1
.end method

.method public getHibernateDuration(Lcom/android/server/am/AppStateManager$AppState;)J
    .locals 3
    .param p1, "app"    # Lcom/android/server/am/AppStateManager$AppState;

    .line 776
    invoke-virtual {p1}, Lcom/android/server/am/AppStateManager$AppState;->getUsageLevel()I

    move-result v0

    .line 777
    .local v0, "usageLevel":I
    const/4 v1, 0x5

    if-ne v0, v1, :cond_0

    .line 778
    invoke-virtual {p1}, Lcom/android/server/am/AppStateManager$AppState;->isAutoStartApp()Z

    move-result v1

    if-eqz v1, :cond_0

    invoke-virtual {p1}, Lcom/android/server/am/AppStateManager$AppState;->hasProtectProcess()Z

    move-result v1

    if-nez v1, :cond_0

    .line 779
    sget-wide v1, Lcom/miui/app/smartpower/SmartPowerSettings;->HIBERNATE_DURATION:J

    return-wide v1

    .line 781
    :cond_0
    const-wide/16 v1, 0x0

    return-wide v1
.end method

.method public getIdleDur(Ljava/lang/String;)J
    .locals 2
    .param p1, "packageName"    # Ljava/lang/String;

    .line 772
    sget-wide v0, Lcom/miui/app/smartpower/SmartPowerSettings;->IDLE_DURATION:J

    return-wide v0
.end method

.method public getInactiveDuration(Ljava/lang/String;I)J
    .locals 2
    .param p1, "packageName"    # Ljava/lang/String;
    .param p2, "uid"    # I

    .line 789
    invoke-static {}, Lcom/miui/server/smartpower/SmartPowerPolicyManager;->isSpeedTestMode()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 790
    sget-wide v0, Lcom/miui/app/smartpower/SmartPowerSettings;->INACTIVE_SPTM_DURATION:J

    return-wide v0

    .line 792
    :cond_0
    sget-wide v0, Lcom/miui/app/smartpower/SmartPowerSettings;->INACTIVE_DURATION:J

    return-wide v0
.end method

.method public getMaintenanceDur(Ljava/lang/String;)J
    .locals 2
    .param p1, "packageName"    # Ljava/lang/String;

    .line 785
    sget-wide v0, Lcom/miui/app/smartpower/SmartPowerSettings;->MAINTENANCE_DURATION:J

    return-wide v0
.end method

.method public getUsageStatsInfo(Ljava/lang/String;)Lcom/miui/server/smartpower/SmartPowerPolicyManager$UsageStatsInfo;
    .locals 3
    .param p1, "packageName"    # Ljava/lang/String;

    .line 1061
    iget-object v0, p0, Lcom/miui/server/smartpower/SmartPowerPolicyManager;->mUsageStatsMap:Ljava/util/HashMap;

    monitor-enter v0

    .line 1062
    :try_start_0
    iget-object v1, p0, Lcom/miui/server/smartpower/SmartPowerPolicyManager;->mUsageStatsMap:Ljava/util/HashMap;

    invoke-virtual {v1, p1}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/miui/server/smartpower/SmartPowerPolicyManager$UsageStatsInfo;

    .line 1063
    .local v1, "info":Lcom/miui/server/smartpower/SmartPowerPolicyManager$UsageStatsInfo;
    monitor-exit v0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 1064
    if-nez v1, :cond_1

    .line 1065
    iget-object v0, p0, Lcom/miui/server/smartpower/SmartPowerPolicyManager;->mUsageStatsMap:Ljava/util/HashMap;

    invoke-virtual {v0}, Ljava/util/HashMap;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1066
    iget-object v0, p0, Lcom/miui/server/smartpower/SmartPowerPolicyManager;->mHandler:Landroid/os/Handler;

    const/4 v2, 0x1

    invoke-virtual {v0, v2}, Landroid/os/Handler;->removeMessages(I)V

    .line 1067
    iget-object v0, p0, Lcom/miui/server/smartpower/SmartPowerPolicyManager;->mHandler:Landroid/os/Handler;

    invoke-virtual {v0, v2}, Landroid/os/Handler;->sendEmptyMessage(I)Z

    .line 1069
    :cond_0
    new-instance v0, Lcom/miui/server/smartpower/SmartPowerPolicyManager$UsageStatsInfo;

    invoke-direct {v0, p0, p1}, Lcom/miui/server/smartpower/SmartPowerPolicyManager$UsageStatsInfo;-><init>(Lcom/miui/server/smartpower/SmartPowerPolicyManager;Ljava/lang/String;)V

    move-object v1, v0

    .line 1071
    :cond_1
    return-object v1

    .line 1063
    .end local v1    # "info":Lcom/miui/server/smartpower/SmartPowerPolicyManager$UsageStatsInfo;
    :catchall_0
    move-exception v1

    :try_start_1
    monitor-exit v0
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    throw v1
.end method

.method public init(Lcom/android/server/am/AppStateManager;Lcom/miui/server/smartpower/AppPowerResourceManager;)V
    .locals 0
    .param p1, "appStateManager"    # Lcom/android/server/am/AppStateManager;
    .param p2, "appPowerResourceManager"    # Lcom/miui/server/smartpower/AppPowerResourceManager;

    .line 296
    iput-object p1, p0, Lcom/miui/server/smartpower/SmartPowerPolicyManager;->mAppStateManager:Lcom/android/server/am/AppStateManager;

    .line 297
    invoke-virtual {p1, p0}, Lcom/android/server/am/AppStateManager;->registerAppStateListener(Lcom/android/server/am/AppStateManager$IProcessStateCallback;)Z

    .line 298
    iput-object p2, p0, Lcom/miui/server/smartpower/SmartPowerPolicyManager;->mAppPowerResourceManager:Lcom/miui/server/smartpower/AppPowerResourceManager;

    .line 299
    return-void
.end method

.method public isAlarmPackageWhiteList(Ljava/lang/String;)Z
    .locals 3
    .param p1, "pkgname"    # Ljava/lang/String;

    .line 1238
    iget-object v0, p0, Lcom/miui/server/smartpower/SmartPowerPolicyManager;->mPackageWhiteList:Landroid/util/ArrayMap;

    monitor-enter v0

    .line 1239
    :try_start_0
    iget-object v1, p0, Lcom/miui/server/smartpower/SmartPowerPolicyManager;->mPackageWhiteList:Landroid/util/ArrayMap;

    invoke-virtual {v1, p1}, Landroid/util/ArrayMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/miui/server/smartpower/SmartPowerPolicyManager$WhiteListItem;

    .line 1240
    .local v1, "item":Lcom/miui/server/smartpower/SmartPowerPolicyManager$WhiteListItem;
    if-eqz v1, :cond_0

    const/16 v2, 0x8

    invoke-virtual {v1, v2}, Lcom/miui/server/smartpower/SmartPowerPolicyManager$WhiteListItem;->hasAction(I)Z

    move-result v2

    if-eqz v2, :cond_0

    .line 1241
    monitor-exit v0

    const/4 v0, 0x1

    return v0

    .line 1243
    .end local v1    # "item":Lcom/miui/server/smartpower/SmartPowerPolicyManager$WhiteListItem;
    :cond_0
    monitor-exit v0

    .line 1244
    const/4 v0, 0x0

    return v0

    .line 1243
    :catchall_0
    move-exception v1

    monitor-exit v0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v1
.end method

.method public isAppHibernationWhiteList(ILjava/lang/String;)Z
    .locals 1
    .param p1, "uid"    # I
    .param p2, "packageName"    # Ljava/lang/String;

    .line 929
    invoke-static {p1}, Landroid/os/Process;->isCoreUid(I)Z

    move-result v0

    if-nez v0, :cond_1

    .line 930
    invoke-direct {p0, p2}, Lcom/miui/server/smartpower/SmartPowerPolicyManager;->isPackageWhiteList(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_1

    .line 931
    invoke-direct {p0, p1}, Lcom/miui/server/smartpower/SmartPowerPolicyManager;->isUidWhiteList(I)Z

    move-result v0

    if-nez v0, :cond_1

    iget-object v0, p0, Lcom/miui/server/smartpower/SmartPowerPolicyManager;->mPowerSavingStrategyControl:Lcom/miui/server/smartpower/SmartPowerPolicyManager$PowerSavingStrategyControl;

    .line 932
    invoke-static {v0, p2}, Lcom/miui/server/smartpower/SmartPowerPolicyManager$PowerSavingStrategyControl;->-$$Nest$misNoRestrictApp(Lcom/miui/server/smartpower/SmartPowerPolicyManager$PowerSavingStrategyControl;Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_0

    goto :goto_0

    .line 935
    :cond_0
    const/4 v0, 0x0

    return v0

    .line 933
    :cond_1
    :goto_0
    const/4 v0, 0x1

    return v0
.end method

.method public isAppHibernationWhiteList(Lcom/android/server/am/AppStateManager$AppState;)Z
    .locals 2
    .param p1, "appState"    # Lcom/android/server/am/AppStateManager$AppState;

    .line 914
    invoke-virtual {p1}, Lcom/android/server/am/AppStateManager$AppState;->getUid()I

    move-result v0

    invoke-virtual {p1}, Lcom/android/server/am/AppStateManager$AppState;->getPackageName()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p0, v0, v1}, Lcom/miui/server/smartpower/SmartPowerPolicyManager;->isAppHibernationWhiteList(ILjava/lang/String;)Z

    move-result v0

    return v0
.end method

.method public isBroadcastBlackList(Ljava/lang/String;)Z
    .locals 2
    .param p1, "intent"    # Ljava/lang/String;

    .line 1306
    iget-object v0, p0, Lcom/miui/server/smartpower/SmartPowerPolicyManager;->mBroadcastBlackList:Landroid/util/ArraySet;

    monitor-enter v0

    .line 1307
    :try_start_0
    iget-object v1, p0, Lcom/miui/server/smartpower/SmartPowerPolicyManager;->mBroadcastBlackList:Landroid/util/ArraySet;

    invoke-virtual {v1, p1}, Landroid/util/ArraySet;->contains(Ljava/lang/Object;)Z

    move-result v1

    monitor-exit v0

    return v1

    .line 1308
    :catchall_0
    move-exception v1

    monitor-exit v0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v1
.end method

.method public isBroadcastWhiteList(Ljava/lang/String;)Z
    .locals 2
    .param p1, "intent"    # Ljava/lang/String;

    .line 1342
    iget-object v0, p0, Lcom/miui/server/smartpower/SmartPowerPolicyManager;->mBroadcastWhiteList:Landroid/util/ArraySet;

    monitor-enter v0

    .line 1343
    :try_start_0
    iget-object v1, p0, Lcom/miui/server/smartpower/SmartPowerPolicyManager;->mBroadcastWhiteList:Landroid/util/ArraySet;

    invoke-virtual {v1, p1}, Landroid/util/ArraySet;->contains(Ljava/lang/Object;)Z

    move-result v1

    monitor-exit v0

    return v1

    .line 1344
    :catchall_0
    move-exception v1

    monitor-exit v0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v1
.end method

.method public isDependedProcess(I)Z
    .locals 3
    .param p1, "pid"    # I

    .line 359
    iget-object v0, p0, Lcom/miui/server/smartpower/SmartPowerPolicyManager;->mDependencyMap:Landroid/util/ArrayMap;

    monitor-enter v0

    .line 360
    :try_start_0
    iget-object v1, p0, Lcom/miui/server/smartpower/SmartPowerPolicyManager;->mDependencyMap:Landroid/util/ArrayMap;

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v1, v2}, Landroid/util/ArrayMap;->containsKey(Ljava/lang/Object;)Z

    move-result v1

    monitor-exit v0

    return v1

    .line 361
    :catchall_0
    move-exception v1

    monitor-exit v0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v1
.end method

.method public isNoRestrictApp(Ljava/lang/String;)Z
    .locals 1
    .param p1, "packageName"    # Ljava/lang/String;

    .line 1509
    iget-object v0, p0, Lcom/miui/server/smartpower/SmartPowerPolicyManager;->mPowerSavingStrategyControl:Lcom/miui/server/smartpower/SmartPowerPolicyManager$PowerSavingStrategyControl;

    invoke-static {v0, p1}, Lcom/miui/server/smartpower/SmartPowerPolicyManager$PowerSavingStrategyControl;->-$$Nest$misNoRestrictApp(Lcom/miui/server/smartpower/SmartPowerPolicyManager$PowerSavingStrategyControl;Ljava/lang/String;)Z

    move-result v0

    return v0
.end method

.method public isPackageWhiteList(ILjava/lang/String;)Z
    .locals 5
    .param p1, "flag"    # I
    .param p2, "packageName"    # Ljava/lang/String;

    .line 970
    iget-object v0, p0, Lcom/miui/server/smartpower/SmartPowerPolicyManager;->mPackageWhiteList:Landroid/util/ArrayMap;

    monitor-enter v0

    .line 971
    :try_start_0
    iget-object v1, p0, Lcom/miui/server/smartpower/SmartPowerPolicyManager;->mPackageWhiteList:Landroid/util/ArrayMap;

    invoke-virtual {v1, p2}, Landroid/util/ArrayMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/miui/server/smartpower/SmartPowerPolicyManager$WhiteListItem;

    .line 972
    .local v1, "item":Lcom/miui/server/smartpower/SmartPowerPolicyManager$WhiteListItem;
    const/4 v2, 0x1

    if-eqz v1, :cond_0

    invoke-static {v1}, Lcom/miui/server/smartpower/SmartPowerPolicyManager$WhiteListItem;->-$$Nest$fgetmTypes(Lcom/miui/server/smartpower/SmartPowerPolicyManager$WhiteListItem;)I

    move-result v3

    and-int/2addr v3, p1

    if-eqz v3, :cond_0

    .line 973
    monitor-exit v0

    return v2

    .line 975
    .end local v1    # "item":Lcom/miui/server/smartpower/SmartPowerPolicyManager$WhiteListItem;
    :cond_0
    monitor-exit v0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 976
    iget-object v1, p0, Lcom/miui/server/smartpower/SmartPowerPolicyManager;->mUidWhiteList:Landroid/util/ArrayMap;

    monitor-enter v1

    .line 977
    :try_start_1
    iget-object v0, p0, Lcom/miui/server/smartpower/SmartPowerPolicyManager;->mUidWhiteList:Landroid/util/ArrayMap;

    invoke-virtual {v0}, Landroid/util/ArrayMap;->values()Ljava/util/Collection;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v0

    :goto_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_2

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/miui/server/smartpower/SmartPowerPolicyManager$WhiteListItem;

    .line 978
    .local v3, "item":Lcom/miui/server/smartpower/SmartPowerPolicyManager$WhiteListItem;
    invoke-static {v3}, Lcom/miui/server/smartpower/SmartPowerPolicyManager$WhiteListItem;->-$$Nest$fgetmName(Lcom/miui/server/smartpower/SmartPowerPolicyManager$WhiteListItem;)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v4, p2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_1

    .line 979
    invoke-virtual {v3, p1}, Lcom/miui/server/smartpower/SmartPowerPolicyManager$WhiteListItem;->hasWhiteListType(I)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 980
    monitor-exit v1

    return v2

    .line 984
    .end local v3    # "item":Lcom/miui/server/smartpower/SmartPowerPolicyManager$WhiteListItem;
    :cond_1
    goto :goto_0

    .line 985
    :cond_2
    monitor-exit v1

    .line 986
    const/4 v0, 0x0

    return v0

    .line 985
    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    throw v0

    .line 975
    :catchall_1
    move-exception v1

    :try_start_2
    monitor-exit v0
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    throw v1
.end method

.method public isProcessHibernationWhiteList(IILjava/lang/String;Ljava/lang/String;)Z
    .locals 1
    .param p1, "uid"    # I
    .param p2, "pid"    # I
    .param p3, "packageName"    # Ljava/lang/String;
    .param p4, "processName"    # Ljava/lang/String;

    .line 919
    invoke-virtual {p0, p1, p3}, Lcom/miui/server/smartpower/SmartPowerPolicyManager;->isAppHibernationWhiteList(ILjava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_1

    .line 920
    invoke-direct {p0, p4}, Lcom/miui/server/smartpower/SmartPowerPolicyManager;->isProcessWhiteList(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_1

    .line 921
    invoke-static {p4}, Lcom/miui/server/process/ProcessManagerInternal;->checkCtsProcess(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_1

    .line 922
    invoke-direct {p0, p2}, Lcom/miui/server/smartpower/SmartPowerPolicyManager;->isPidWhiteList(I)Z

    move-result v0

    if-eqz v0, :cond_0

    goto :goto_0

    .line 925
    :cond_0
    const/4 v0, 0x0

    return v0

    .line 923
    :cond_1
    :goto_0
    const/4 v0, 0x1

    return v0
.end method

.method public isProcessHibernationWhiteList(Lcom/android/server/am/AppStateManager$AppState$RunningProcess;)Z
    .locals 4
    .param p1, "proc"    # Lcom/android/server/am/AppStateManager$AppState$RunningProcess;

    .line 909
    invoke-virtual {p1}, Lcom/android/server/am/AppStateManager$AppState$RunningProcess;->getUid()I

    move-result v0

    invoke-virtual {p1}, Lcom/android/server/am/AppStateManager$AppState$RunningProcess;->getPid()I

    move-result v1

    .line 910
    invoke-virtual {p1}, Lcom/android/server/am/AppStateManager$AppState$RunningProcess;->getPackageName()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {p1}, Lcom/android/server/am/AppStateManager$AppState$RunningProcess;->getProcessName()Ljava/lang/String;

    move-result-object v3

    .line 909
    invoke-virtual {p0, v0, v1, v2, v3}, Lcom/miui/server/smartpower/SmartPowerPolicyManager;->isProcessHibernationWhiteList(IILjava/lang/String;Ljava/lang/String;)Z

    move-result v0

    return v0
.end method

.method public isProcessWhiteList(ILjava/lang/String;)Z
    .locals 5
    .param p1, "flag"    # I
    .param p2, "procName"    # Ljava/lang/String;

    .line 1027
    iget-object v0, p0, Lcom/miui/server/smartpower/SmartPowerPolicyManager;->mProcessWhiteList:Landroid/util/ArrayMap;

    monitor-enter v0

    .line 1028
    :try_start_0
    iget-object v1, p0, Lcom/miui/server/smartpower/SmartPowerPolicyManager;->mProcessWhiteList:Landroid/util/ArrayMap;

    invoke-virtual {v1, p2}, Landroid/util/ArrayMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/miui/server/smartpower/SmartPowerPolicyManager$WhiteListItem;

    .line 1029
    .local v1, "item":Lcom/miui/server/smartpower/SmartPowerPolicyManager$WhiteListItem;
    const/4 v2, 0x1

    if-eqz v1, :cond_0

    invoke-static {v1}, Lcom/miui/server/smartpower/SmartPowerPolicyManager$WhiteListItem;->-$$Nest$fgetmTypes(Lcom/miui/server/smartpower/SmartPowerPolicyManager$WhiteListItem;)I

    move-result v3

    and-int/2addr v3, p1

    if-eqz v3, :cond_0

    .line 1030
    monitor-exit v0

    return v2

    .line 1032
    .end local v1    # "item":Lcom/miui/server/smartpower/SmartPowerPolicyManager$WhiteListItem;
    :cond_0
    monitor-exit v0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 1033
    iget-object v1, p0, Lcom/miui/server/smartpower/SmartPowerPolicyManager;->mPidWhiteList:Landroid/util/ArrayMap;

    monitor-enter v1

    .line 1034
    :try_start_1
    iget-object v0, p0, Lcom/miui/server/smartpower/SmartPowerPolicyManager;->mPidWhiteList:Landroid/util/ArrayMap;

    invoke-virtual {v0}, Landroid/util/ArrayMap;->values()Ljava/util/Collection;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v0

    :goto_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_2

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/miui/server/smartpower/SmartPowerPolicyManager$WhiteListItem;

    .line 1035
    .local v3, "item":Lcom/miui/server/smartpower/SmartPowerPolicyManager$WhiteListItem;
    invoke-static {v3}, Lcom/miui/server/smartpower/SmartPowerPolicyManager$WhiteListItem;->-$$Nest$fgetmName(Lcom/miui/server/smartpower/SmartPowerPolicyManager$WhiteListItem;)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v4, p2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_1

    .line 1036
    invoke-virtual {v3, p1}, Lcom/miui/server/smartpower/SmartPowerPolicyManager$WhiteListItem;->hasWhiteListType(I)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 1037
    monitor-exit v1

    return v2

    .line 1041
    .end local v3    # "item":Lcom/miui/server/smartpower/SmartPowerPolicyManager$WhiteListItem;
    :cond_1
    goto :goto_0

    .line 1042
    :cond_2
    monitor-exit v1

    .line 1043
    const/4 v0, 0x0

    return v0

    .line 1042
    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    throw v0

    .line 1032
    :catchall_1
    move-exception v1

    :try_start_2
    monitor-exit v0
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    throw v1
.end method

.method public isProcessWhiteList(ILjava/lang/String;Ljava/lang/String;)Z
    .locals 1
    .param p1, "flag"    # I
    .param p2, "packageName"    # Ljava/lang/String;
    .param p3, "procName"    # Ljava/lang/String;

    .line 1047
    invoke-virtual {p0, p1, p2}, Lcom/miui/server/smartpower/SmartPowerPolicyManager;->isPackageWhiteList(ILjava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1048
    const/4 v0, 0x1

    return v0

    .line 1050
    :cond_0
    invoke-virtual {p0, p1, p3}, Lcom/miui/server/smartpower/SmartPowerPolicyManager;->isProcessWhiteList(ILjava/lang/String;)Z

    move-result v0

    return v0
.end method

.method public isProviderPackageWhiteList(Ljava/lang/String;)Z
    .locals 3
    .param p1, "pkgName"    # Ljava/lang/String;

    .line 1221
    iget-object v0, p0, Lcom/miui/server/smartpower/SmartPowerPolicyManager;->mPackageWhiteList:Landroid/util/ArrayMap;

    monitor-enter v0

    .line 1222
    :try_start_0
    iget-object v1, p0, Lcom/miui/server/smartpower/SmartPowerPolicyManager;->mPackageWhiteList:Landroid/util/ArrayMap;

    invoke-virtual {v1, p1}, Landroid/util/ArrayMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/miui/server/smartpower/SmartPowerPolicyManager$WhiteListItem;

    .line 1223
    .local v1, "item":Lcom/miui/server/smartpower/SmartPowerPolicyManager$WhiteListItem;
    if-eqz v1, :cond_0

    const/16 v2, 0x20

    invoke-virtual {v1, v2}, Lcom/miui/server/smartpower/SmartPowerPolicyManager$WhiteListItem;->hasAction(I)Z

    move-result v2

    if-eqz v2, :cond_0

    .line 1224
    monitor-exit v0

    const/4 v0, 0x1

    return v0

    .line 1226
    .end local v1    # "item":Lcom/miui/server/smartpower/SmartPowerPolicyManager$WhiteListItem;
    :cond_0
    monitor-exit v0

    .line 1227
    const/4 v0, 0x0

    return v0

    .line 1226
    :catchall_0
    move-exception v1

    monitor-exit v0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v1
.end method

.method public isProviderPkgBlackList(Ljava/lang/String;)Z
    .locals 2
    .param p1, "pkgName"    # Ljava/lang/String;

    .line 1318
    iget-object v0, p0, Lcom/miui/server/smartpower/SmartPowerPolicyManager;->mProviderPkgBlackList:Landroid/util/ArraySet;

    monitor-enter v0

    .line 1319
    :try_start_0
    iget-object v1, p0, Lcom/miui/server/smartpower/SmartPowerPolicyManager;->mProviderPkgBlackList:Landroid/util/ArraySet;

    invoke-virtual {v1, p1}, Landroid/util/ArraySet;->contains(Ljava/lang/Object;)Z

    move-result v1

    monitor-exit v0

    return v1

    .line 1320
    :catchall_0
    move-exception v1

    monitor-exit v0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v1
.end method

.method public isScreenOff()Z
    .locals 1

    .line 683
    iget-boolean v0, p0, Lcom/miui/server/smartpower/SmartPowerPolicyManager;->mScreenOff:Z

    return v0
.end method

.method public isServicePackageWhiteList(Ljava/lang/String;)Z
    .locals 3
    .param p1, "pkgName"    # Ljava/lang/String;

    .line 1204
    iget-object v0, p0, Lcom/miui/server/smartpower/SmartPowerPolicyManager;->mPackageWhiteList:Landroid/util/ArrayMap;

    monitor-enter v0

    .line 1205
    :try_start_0
    iget-object v1, p0, Lcom/miui/server/smartpower/SmartPowerPolicyManager;->mPackageWhiteList:Landroid/util/ArrayMap;

    invoke-virtual {v1, p1}, Landroid/util/ArrayMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/miui/server/smartpower/SmartPowerPolicyManager$WhiteListItem;

    .line 1206
    .local v1, "item":Lcom/miui/server/smartpower/SmartPowerPolicyManager$WhiteListItem;
    if-eqz v1, :cond_0

    const/16 v2, 0x10

    invoke-virtual {v1, v2}, Lcom/miui/server/smartpower/SmartPowerPolicyManager$WhiteListItem;->hasAction(I)Z

    move-result v2

    if-eqz v2, :cond_0

    .line 1207
    monitor-exit v0

    const/4 v0, 0x1

    return v0

    .line 1209
    .end local v1    # "item":Lcom/miui/server/smartpower/SmartPowerPolicyManager$WhiteListItem;
    :cond_0
    monitor-exit v0

    .line 1210
    const/4 v0, 0x0

    return v0

    .line 1209
    :catchall_0
    move-exception v1

    monitor-exit v0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v1
.end method

.method public isServicePkgBlackList(Ljava/lang/String;)Z
    .locals 2
    .param p1, "pkgName"    # Ljava/lang/String;

    .line 1330
    iget-object v0, p0, Lcom/miui/server/smartpower/SmartPowerPolicyManager;->mServicePkgBlackList:Landroid/util/ArraySet;

    monitor-enter v0

    .line 1331
    :try_start_0
    iget-object v1, p0, Lcom/miui/server/smartpower/SmartPowerPolicyManager;->mServicePkgBlackList:Landroid/util/ArraySet;

    invoke-virtual {v1, p1}, Landroid/util/ArraySet;->contains(Ljava/lang/Object;)Z

    move-result v1

    monitor-exit v0

    return v1

    .line 1332
    :catchall_0
    move-exception v1

    monitor-exit v0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v1
.end method

.method public needCheckNet(Ljava/lang/String;I)Z
    .locals 3
    .param p1, "packageName"    # Ljava/lang/String;
    .param p2, "uid"    # I

    .line 796
    invoke-virtual {p0, p1}, Lcom/miui/server/smartpower/SmartPowerPolicyManager;->getUsageStatsInfo(Ljava/lang/String;)Lcom/miui/server/smartpower/SmartPowerPolicyManager$UsageStatsInfo;

    move-result-object v0

    .line 797
    .local v0, "info":Lcom/miui/server/smartpower/SmartPowerPolicyManager$UsageStatsInfo;
    const/4 v1, 0x0

    if-eqz v0, :cond_1

    .line 798
    invoke-static {v0}, Lcom/miui/server/smartpower/SmartPowerPolicyManager$UsageStatsInfo;->-$$Nest$fgetmAppLaunchCount(Lcom/miui/server/smartpower/SmartPowerPolicyManager$UsageStatsInfo;)I

    move-result v2

    if-lez v2, :cond_0

    const/4 v1, 0x1

    :cond_0
    return v1

    .line 800
    :cond_1
    return v1
.end method

.method public onAppStateChanged(Lcom/android/server/am/AppStateManager$AppState;IIZZLjava/lang/String;)V
    .locals 5
    .param p1, "app"    # Lcom/android/server/am/AppStateManager$AppState;
    .param p2, "oldState"    # I
    .param p3, "newState"    # I
    .param p4, "oldProtectState"    # Z
    .param p5, "newProtectState"    # Z
    .param p6, "reason"    # Ljava/lang/String;

    .line 732
    const-string v0, "onAppStateChanged"

    const-wide/32 v1, 0x20000

    invoke-static {v1, v2, v0}, Landroid/os/Trace;->traceBegin(JLjava/lang/String;)V

    .line 733
    if-nez p5, :cond_0

    const/4 v0, 0x5

    if-lt p3, v0, :cond_0

    if-ge p2, v0, :cond_0

    .line 735
    invoke-direct {p0, p1}, Lcom/miui/server/smartpower/SmartPowerPolicyManager;->idleApp(Lcom/android/server/am/AppStateManager$AppState;)V

    .line 738
    :cond_0
    const/4 v0, 0x6

    if-nez p5, :cond_1

    if-lt p3, v0, :cond_1

    if-ge p2, v0, :cond_1

    .line 740
    invoke-direct {p0, p1, p6}, Lcom/miui/server/smartpower/SmartPowerPolicyManager;->hibernationApp(Lcom/android/server/am/AppStateManager$AppState;Ljava/lang/String;)V

    .line 743
    :cond_1
    if-lt p3, v0, :cond_2

    if-eqz p5, :cond_3

    :cond_2
    if-ne p2, v0, :cond_3

    if-nez p4, :cond_3

    .line 745
    invoke-direct {p0, p1, p6}, Lcom/miui/server/smartpower/SmartPowerPolicyManager;->activeApp(Lcom/android/server/am/AppStateManager$AppState;Ljava/lang/String;)V

    .line 748
    :cond_3
    if-nez p3, :cond_4

    .line 749
    iget-object v0, p0, Lcom/miui/server/smartpower/SmartPowerPolicyManager;->mUidWhiteList:Landroid/util/ArrayMap;

    monitor-enter v0

    .line 750
    :try_start_0
    iget-object v3, p0, Lcom/miui/server/smartpower/SmartPowerPolicyManager;->mUidWhiteList:Landroid/util/ArrayMap;

    invoke-virtual {p1}, Lcom/android/server/am/AppStateManager$AppState;->getUid()I

    move-result v4

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    invoke-virtual {v3, v4}, Landroid/util/ArrayMap;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    .line 751
    monitor-exit v0

    goto :goto_0

    :catchall_0
    move-exception v1

    monitor-exit v0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v1

    .line 753
    :cond_4
    :goto_0
    invoke-static {v1, v2}, Landroid/os/Trace;->traceEnd(J)V

    .line 754
    return-void
.end method

.method public onBackupChanged(ZILjava/lang/String;)V
    .locals 1
    .param p1, "active"    # Z
    .param p2, "uid"    # I
    .param p3, "packageName"    # Ljava/lang/String;

    .line 325
    const/16 v0, 0x40

    if-eqz p1, :cond_0

    .line 326
    invoke-direct {p0, p2, p3, v0}, Lcom/miui/server/smartpower/SmartPowerPolicyManager;->addUidWhiteList(ILjava/lang/String;I)V

    goto :goto_0

    .line 328
    :cond_0
    invoke-direct {p0, p2, p3, v0}, Lcom/miui/server/smartpower/SmartPowerPolicyManager;->removeUidWhiteList(ILjava/lang/String;I)V

    .line 330
    :goto_0
    return-void
.end method

.method public onDependChanged(ZLjava/lang/String;Ljava/lang/String;II)V
    .locals 5
    .param p1, "isConnected"    # Z
    .param p2, "processName"    # Ljava/lang/String;
    .param p3, "dependProcName"    # Ljava/lang/String;
    .param p4, "uid"    # I
    .param p5, "pid"    # I

    .line 336
    iget-object v0, p0, Lcom/miui/server/smartpower/SmartPowerPolicyManager;->mDependencyMap:Landroid/util/ArrayMap;

    monitor-enter v0

    .line 337
    :try_start_0
    iget-object v1, p0, Lcom/miui/server/smartpower/SmartPowerPolicyManager;->mDependencyMap:Landroid/util/ArrayMap;

    invoke-static {p5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v1, v2}, Landroid/util/ArrayMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/util/ArraySet;

    .line 338
    .local v1, "dependList":Landroid/util/ArraySet;, "Landroid/util/ArraySet<Ljava/lang/String;>;"
    const/16 v2, 0x100

    if-eqz p1, :cond_1

    .line 339
    if-nez v1, :cond_0

    .line 340
    new-instance v3, Landroid/util/ArraySet;

    invoke-direct {v3}, Landroid/util/ArraySet;-><init>()V

    move-object v1, v3

    .line 341
    iget-object v3, p0, Lcom/miui/server/smartpower/SmartPowerPolicyManager;->mDependencyMap:Landroid/util/ArrayMap;

    invoke-static {p5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    invoke-virtual {v3, v4, v1}, Landroid/util/ArrayMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 342
    invoke-direct {p0, p4, p5, p3, v2}, Lcom/miui/server/smartpower/SmartPowerPolicyManager;->addPidWhiteList(IILjava/lang/String;I)V

    .line 344
    :cond_0
    invoke-virtual {v1, p2}, Landroid/util/ArraySet;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 346
    :cond_1
    if-eqz v1, :cond_2

    .line 347
    invoke-virtual {v1, p2}, Landroid/util/ArraySet;->remove(Ljava/lang/Object;)Z

    .line 348
    invoke-virtual {v1}, Landroid/util/ArraySet;->size()I

    move-result v3

    if-nez v3, :cond_2

    .line 349
    iget-object v3, p0, Lcom/miui/server/smartpower/SmartPowerPolicyManager;->mDependencyMap:Landroid/util/ArrayMap;

    invoke-static {p5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    invoke-virtual {v3, v4}, Landroid/util/ArrayMap;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    .line 350
    invoke-direct {p0, p4, p5, p3, v2}, Lcom/miui/server/smartpower/SmartPowerPolicyManager;->removePidWhiteList(IILjava/lang/String;I)V

    .line 355
    .end local v1    # "dependList":Landroid/util/ArraySet;, "Landroid/util/ArraySet<Ljava/lang/String;>;"
    :cond_2
    :goto_0
    monitor-exit v0

    .line 356
    return-void

    .line 355
    :catchall_0
    move-exception v1

    monitor-exit v0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v1
.end method

.method public onExternalAudioRegister(II)V
    .locals 3
    .param p1, "uid"    # I
    .param p2, "pid"    # I

    .line 302
    iget-object v0, p0, Lcom/miui/server/smartpower/SmartPowerPolicyManager;->mAppStateManager:Lcom/android/server/am/AppStateManager;

    invoke-virtual {v0, p1, p2}, Lcom/android/server/am/AppStateManager;->getRunningProcess(II)Lcom/android/server/am/AppStateManager$AppState$RunningProcess;

    move-result-object v0

    .line 303
    .local v0, "proc":Lcom/android/server/am/AppStateManager$AppState$RunningProcess;
    if-eqz v0, :cond_0

    .line 304
    invoke-virtual {v0}, Lcom/android/server/am/AppStateManager$AppState$RunningProcess;->getProcessName()Ljava/lang/String;

    move-result-object v1

    const/16 v2, 0x200

    invoke-direct {p0, p1, p2, v1, v2}, Lcom/miui/server/smartpower/SmartPowerPolicyManager;->addPidWhiteList(IILjava/lang/String;I)V

    .line 306
    :cond_0
    return-void
.end method

.method public onProcessStateChanged(Lcom/android/server/am/AppStateManager$AppState$RunningProcess;IILjava/lang/String;)V
    .locals 6
    .param p1, "proc"    # Lcom/android/server/am/AppStateManager$AppState$RunningProcess;
    .param p2, "oldState"    # I
    .param p3, "newState"    # I
    .param p4, "reason"    # Ljava/lang/String;

    .line 689
    const-string v0, "onProcessStateChanged"

    const-wide/32 v1, 0x20000

    invoke-static {v1, v2, v0}, Landroid/os/Trace;->traceBegin(JLjava/lang/String;)V

    .line 690
    const/4 v0, 0x6

    if-lt p3, v0, :cond_0

    if-ge p2, v0, :cond_0

    .line 692
    invoke-direct {p0, p1}, Lcom/miui/server/smartpower/SmartPowerPolicyManager;->idleProcess(Lcom/android/server/am/AppStateManager$AppState$RunningProcess;)V

    .line 695
    :cond_0
    const/4 v0, 0x7

    if-lt p3, v0, :cond_1

    if-ge p2, v0, :cond_1

    .line 697
    invoke-direct {p0, p1, p4}, Lcom/miui/server/smartpower/SmartPowerPolicyManager;->hibernationProcess(Lcom/android/server/am/AppStateManager$AppState$RunningProcess;Ljava/lang/String;)V

    .line 700
    :cond_1
    if-ge p3, v0, :cond_2

    if-ne p2, v0, :cond_2

    .line 702
    invoke-direct {p0, p1, p4}, Lcom/miui/server/smartpower/SmartPowerPolicyManager;->activeProcess(Lcom/android/server/am/AppStateManager$AppState$RunningProcess;Ljava/lang/String;)V

    .line 705
    :cond_2
    if-gtz p2, :cond_3

    if-lez p3, :cond_3

    .line 707
    invoke-direct {p0, p1}, Lcom/miui/server/smartpower/SmartPowerPolicyManager;->lunchProcess(Lcom/android/server/am/AppStateManager$AppState$RunningProcess;)V

    .line 710
    :cond_3
    if-nez p3, :cond_4

    .line 711
    invoke-direct {p0, p1}, Lcom/miui/server/smartpower/SmartPowerPolicyManager;->diedProcess(Lcom/android/server/am/AppStateManager$AppState$RunningProcess;)V

    .line 714
    :cond_4
    if-nez p3, :cond_5

    .line 715
    const/4 v0, 0x0

    .line 716
    .local v0, "item":Lcom/miui/server/smartpower/SmartPowerPolicyManager$WhiteListItem;
    iget-object v3, p0, Lcom/miui/server/smartpower/SmartPowerPolicyManager;->mPidWhiteList:Landroid/util/ArrayMap;

    monitor-enter v3

    .line 717
    :try_start_0
    iget-object v4, p0, Lcom/miui/server/smartpower/SmartPowerPolicyManager;->mPidWhiteList:Landroid/util/ArrayMap;

    invoke-virtual {p1}, Lcom/android/server/am/AppStateManager$AppState$RunningProcess;->getPid()I

    move-result v5

    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    invoke-virtual {v4, v5}, Landroid/util/ArrayMap;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lcom/miui/server/smartpower/SmartPowerPolicyManager$WhiteListItem;

    move-object v0, v4

    .line 718
    monitor-exit v3
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 719
    if-eqz v0, :cond_5

    .line 720
    iget-object v3, p0, Lcom/miui/server/smartpower/SmartPowerPolicyManager;->mDependencyMap:Landroid/util/ArrayMap;

    monitor-enter v3

    .line 721
    :try_start_1
    iget-object v4, p0, Lcom/miui/server/smartpower/SmartPowerPolicyManager;->mDependencyMap:Landroid/util/ArrayMap;

    invoke-static {v0}, Lcom/miui/server/smartpower/SmartPowerPolicyManager$WhiteListItem;->-$$Nest$fgetmPid(Lcom/miui/server/smartpower/SmartPowerPolicyManager$WhiteListItem;)I

    move-result v5

    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    invoke-virtual {v4, v5}, Landroid/util/ArrayMap;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    .line 722
    monitor-exit v3

    goto :goto_0

    :catchall_0
    move-exception v1

    monitor-exit v3
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    throw v1

    .line 718
    :catchall_1
    move-exception v1

    :try_start_2
    monitor-exit v3
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    throw v1

    .line 726
    .end local v0    # "item":Lcom/miui/server/smartpower/SmartPowerPolicyManager$WhiteListItem;
    :cond_5
    :goto_0
    invoke-static {v1, v2}, Landroid/os/Trace;->traceEnd(J)V

    .line 727
    return-void
.end method

.method public onUsbStateChanged(ZZ)V
    .locals 0
    .param p1, "isConnected"    # Z
    .param p2, "isDataTransfer"    # Z

    .line 332
    return-void
.end method

.method public onVpnChanged(ZILjava/lang/String;)V
    .locals 1
    .param p1, "active"    # Z
    .param p2, "uid"    # I
    .param p3, "packageName"    # Ljava/lang/String;

    .line 317
    const/16 v0, 0x8

    if-eqz p1, :cond_0

    .line 318
    invoke-direct {p0, p2, p3, v0}, Lcom/miui/server/smartpower/SmartPowerPolicyManager;->addUidWhiteList(ILjava/lang/String;I)V

    goto :goto_0

    .line 320
    :cond_0
    invoke-direct {p0, p2, p3, v0}, Lcom/miui/server/smartpower/SmartPowerPolicyManager;->removeUidWhiteList(ILjava/lang/String;I)V

    .line 322
    :goto_0
    return-void
.end method

.method public onWallpaperComponentChangedLocked(ZILjava/lang/String;)V
    .locals 1
    .param p1, "active"    # Z
    .param p2, "uid"    # I
    .param p3, "packageName"    # Ljava/lang/String;

    .line 309
    const/16 v0, 0x10

    if-eqz p1, :cond_0

    .line 310
    invoke-direct {p0, p2, p3, v0}, Lcom/miui/server/smartpower/SmartPowerPolicyManager;->addUidWhiteList(ILjava/lang/String;I)V

    goto :goto_0

    .line 312
    :cond_0
    invoke-direct {p0, p2, p3, v0}, Lcom/miui/server/smartpower/SmartPowerPolicyManager;->removeUidWhiteList(ILjava/lang/String;I)V

    .line 314
    :goto_0
    return-void
.end method

.method public shouldInterceptAlarmLocked(II)Z
    .locals 4
    .param p1, "uid"    # I
    .param p2, "type"    # I

    .line 565
    invoke-direct {p0}, Lcom/miui/server/smartpower/SmartPowerPolicyManager;->isEnableIntercept()Z

    move-result v0

    const/4 v1, 0x0

    if-nez v0, :cond_0

    .line 566
    return v1

    .line 568
    :cond_0
    iget-object v0, p0, Lcom/miui/server/smartpower/SmartPowerPolicyManager;->mAppStateManager:Lcom/android/server/am/AppStateManager;

    invoke-virtual {v0, p1}, Lcom/android/server/am/AppStateManager;->getAppState(I)Lcom/android/server/am/AppStateManager$AppState;

    move-result-object v0

    .line 569
    .local v0, "appState":Lcom/android/server/am/AppStateManager$AppState;
    if-eqz v0, :cond_6

    .line 570
    invoke-virtual {v0}, Lcom/android/server/am/AppStateManager$AppState;->isProcessPerceptible()Z

    move-result v2

    if-nez v2, :cond_5

    .line 571
    invoke-virtual {v0}, Lcom/android/server/am/AppStateManager$AppState;->getPackageName()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {p0, p1, v2}, Lcom/miui/server/smartpower/SmartPowerPolicyManager;->isAppHibernationWhiteList(ILjava/lang/String;)Z

    move-result v2

    if-nez v2, :cond_5

    .line 572
    invoke-virtual {v0}, Lcom/android/server/am/AppStateManager$AppState;->isSystemApp()Z

    move-result v2

    if-eqz v2, :cond_1

    goto :goto_1

    .line 575
    :cond_1
    invoke-static {}, Lcom/miui/server/smartpower/SmartPowerPolicyManager;->isSpeedTestMode()Z

    move-result v2

    const/4 v3, 0x1

    if-eqz v2, :cond_2

    .line 576
    return v3

    .line 578
    :cond_2
    invoke-virtual {v0}, Lcom/android/server/am/AppStateManager$AppState;->getPackageName()Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Lcom/miui/server/smartpower/SmartPowerPolicyManager;->isPackageInterceptList(Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_4

    if-eqz p2, :cond_4

    const/4 v2, 0x2

    if-eq p2, v2, :cond_4

    .line 581
    invoke-virtual {v0}, Lcom/android/server/am/AppStateManager$AppState;->getPackageName()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {p0, v2}, Lcom/miui/server/smartpower/SmartPowerPolicyManager;->isAlarmPackageWhiteList(Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_3

    goto :goto_0

    .line 584
    :cond_3
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "power alarm h:{ u:"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " t:"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string/jumbo v2, "}"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    .line 585
    invoke-virtual {v0}, Lcom/android/server/am/AppStateManager$AppState;->getPackageName()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    .line 584
    const v2, 0x15ff2

    invoke-static {v2, v1}, Landroid/util/EventLog;->writeEvent(ILjava/lang/String;)I

    .line 586
    return v3

    .line 582
    :cond_4
    :goto_0
    return v1

    .line 573
    :cond_5
    :goto_1
    return v1

    .line 588
    :cond_6
    return v1
.end method

.method public shouldInterceptBroadcastLocked(IILjava/lang/String;Lcom/android/server/am/ProcessRecord;Landroid/content/Intent;ZZ)Z
    .locals 14
    .param p1, "callingUid"    # I
    .param p2, "callingPid"    # I
    .param p3, "callingPkg"    # Ljava/lang/String;
    .param p4, "recProc"    # Lcom/android/server/am/ProcessRecord;
    .param p5, "intent"    # Landroid/content/Intent;
    .param p6, "ordered"    # Z
    .param p7, "sticky"    # Z

    .line 498
    move-object v0, p0

    move v1, p1

    move-object/from16 v2, p4

    invoke-direct {p0}, Lcom/miui/server/smartpower/SmartPowerPolicyManager;->isEnableIntercept()Z

    move-result v3

    const/4 v4, 0x0

    if-eqz v3, :cond_17

    if-eqz v2, :cond_17

    .line 499
    invoke-virtual/range {p5 .. p5}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {p0, v3}, Lcom/miui/server/smartpower/SmartPowerPolicyManager;->isBroadcastWhiteList(Ljava/lang/String;)Z

    move-result v3

    if-nez v3, :cond_16

    iget-object v3, v2, Lcom/android/server/am/ProcessRecord;->processName:Ljava/lang/String;

    .line 500
    invoke-virtual/range {p5 .. p5}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v5

    invoke-direct {p0, v3, v5}, Lcom/miui/server/smartpower/SmartPowerPolicyManager;->isBroadcastProcessWhiteList(Ljava/lang/String;Ljava/lang/String;)Z

    move-result v3

    if-eqz v3, :cond_0

    move/from16 v10, p2

    move-object/from16 v13, p3

    goto/16 :goto_4

    .line 504
    :cond_0
    iget-object v3, v0, Lcom/miui/server/smartpower/SmartPowerPolicyManager;->mAppStateManager:Lcom/android/server/am/AppStateManager;

    iget v5, v2, Lcom/android/server/am/ProcessRecord;->uid:I

    iget v6, v2, Lcom/android/server/am/ProcessRecord;->mPid:I

    .line 505
    invoke-virtual {v3, v5, v6}, Lcom/android/server/am/AppStateManager;->getRunningProcess(II)Lcom/android/server/am/AppStateManager$AppState$RunningProcess;

    move-result-object v3

    .line 507
    .local v3, "recProcessState":Lcom/android/server/am/AppStateManager$AppState$RunningProcess;
    iget v5, v2, Lcom/android/server/am/ProcessRecord;->uid:I

    invoke-static {v5}, Landroid/os/Process;->isCoreUid(I)Z

    move-result v5

    if-nez v5, :cond_15

    if-eqz v3, :cond_15

    .line 508
    invoke-virtual {v3}, Lcom/android/server/am/AppStateManager$AppState$RunningProcess;->isForeground()Z

    move-result v5

    if-nez v5, :cond_14

    invoke-virtual {v3}, Lcom/android/server/am/AppStateManager$AppState$RunningProcess;->canHibernate()Z

    move-result v5

    if-nez v5, :cond_1

    move/from16 v10, p2

    move-object/from16 v13, p3

    goto/16 :goto_3

    .line 513
    :cond_1
    iget-object v5, v0, Lcom/miui/server/smartpower/SmartPowerPolicyManager;->mAppStateManager:Lcom/android/server/am/AppStateManager;

    iget v6, v2, Lcom/android/server/am/ProcessRecord;->uid:I

    invoke-virtual {v5, v6}, Lcom/android/server/am/AppStateManager;->getAppState(I)Lcom/android/server/am/AppStateManager$AppState;

    move-result-object v5

    .line 514
    .local v5, "appState":Lcom/android/server/am/AppStateManager$AppState;
    if-eqz v5, :cond_2

    invoke-virtual {v5}, Lcom/android/server/am/AppStateManager$AppState;->isProcessPerceptible()Z

    move-result v6

    if-eqz v6, :cond_2

    .line 515
    return v4

    .line 518
    :cond_2
    invoke-static {}, Lcom/miui/server/smartpower/SmartPowerPolicyManager;->isSpeedTestMode()Z

    move-result v6

    const/4 v7, 0x1

    if-eqz v6, :cond_3

    .line 519
    return v7

    .line 522
    :cond_3
    invoke-virtual {v3}, Lcom/android/server/am/AppStateManager$AppState$RunningProcess;->getPackageName()Ljava/lang/String;

    move-result-object v6

    invoke-static {v6}, Lcom/miui/server/smartpower/SmartPowerPolicyManager;->isPackageInterceptList(Ljava/lang/String;)Z

    move-result v6

    if-nez v6, :cond_4

    .line 523
    return v4

    .line 526
    :cond_4
    invoke-virtual/range {p5 .. p5}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {p0, v6}, Lcom/miui/server/smartpower/SmartPowerPolicyManager;->isBroadcastBlackList(Ljava/lang/String;)Z

    move-result v6

    .line 528
    .local v6, "isBlackList":Z
    invoke-virtual {v3}, Lcom/android/server/am/AppStateManager$AppState$RunningProcess;->isSystemApp()Z

    move-result v8

    if-nez v8, :cond_12

    invoke-virtual {v3}, Lcom/android/server/am/AppStateManager$AppState$RunningProcess;->isSystemSignature()Z

    move-result v8

    if-eqz v8, :cond_5

    move/from16 v10, p2

    move-object/from16 v13, p3

    goto/16 :goto_2

    .line 531
    :cond_5
    if-eqz v6, :cond_6

    .line 532
    return v7

    .line 536
    :cond_6
    nop

    .line 537
    invoke-virtual/range {p5 .. p5}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v8

    if-eqz v8, :cond_7

    invoke-virtual/range {p5 .. p5}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v8

    const-string v9, "APPWIDGET"

    invoke-virtual {v8, v9}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v8

    if-eqz v8, :cond_7

    move v8, v7

    goto :goto_0

    :cond_7
    move v8, v4

    .line 538
    .local v8, "isWidgetAction":Z
    :goto_0
    iget v9, v2, Lcom/android/server/am/ProcessRecord;->uid:I

    if-eq v1, v9, :cond_8

    if-eqz v8, :cond_9

    :cond_8
    iget-object v9, v2, Lcom/android/server/am/ProcessRecord;->processName:Ljava/lang/String;

    .line 539
    const-string v10, ":widgetProvider"

    invoke-virtual {v9, v10}, Ljava/lang/String;->endsWith(Ljava/lang/String;)Z

    move-result v9

    if-eqz v9, :cond_9

    .line 540
    return v4

    .line 543
    :cond_9
    iget-object v9, v0, Lcom/miui/server/smartpower/SmartPowerPolicyManager;->mAppStateManager:Lcom/android/server/am/AppStateManager;

    .line 544
    move/from16 v10, p2

    invoke-virtual {v9, p1, v10}, Lcom/android/server/am/AppStateManager;->isProcessPerceptible(II)Z

    move-result v9

    .line 545
    .local v9, "isPerceptibleCalling":Z
    invoke-virtual/range {p5 .. p5}, Landroid/content/Intent;->getComponent()Landroid/content/ComponentName;

    move-result-object v11

    if-eqz v11, :cond_a

    move v11, v7

    goto :goto_1

    :cond_a
    move v11, v4

    .line 547
    .local v11, "isExplicitBroadcast":Z
    :goto_1
    invoke-virtual {v3}, Lcom/android/server/am/AppStateManager$AppState$RunningProcess;->isKilled()Z

    move-result v12

    if-eqz v12, :cond_c

    .line 548
    invoke-virtual {v3}, Lcom/android/server/am/AppStateManager$AppState$RunningProcess;->isAutoStartApp()Z

    move-result v12

    if-nez v12, :cond_b

    if-nez v11, :cond_b

    if-nez v9, :cond_b

    move v4, v7

    :cond_b
    return v4

    .line 552
    :cond_c
    invoke-virtual {v3}, Lcom/android/server/am/AppStateManager$AppState$RunningProcess;->isIdle()Z

    move-result v12

    if-nez v12, :cond_f

    .line 553
    if-nez p6, :cond_d

    if-eqz p7, :cond_e

    :cond_d
    move v4, v7

    :cond_e
    return v4

    .line 556
    :cond_f
    invoke-virtual {v3}, Lcom/android/server/am/AppStateManager$AppState$RunningProcess;->getPackageName()Ljava/lang/String;

    move-result-object v12

    move-object/from16 v13, p3

    invoke-virtual {v12, v13}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v12

    if-eqz v12, :cond_10

    if-nez v11, :cond_11

    if-nez v9, :cond_11

    :cond_10
    move v4, v7

    :cond_11
    return v4

    .line 528
    .end local v8    # "isWidgetAction":Z
    .end local v9    # "isPerceptibleCalling":Z
    .end local v11    # "isExplicitBroadcast":Z
    :cond_12
    move/from16 v10, p2

    move-object/from16 v13, p3

    .line 529
    :goto_2
    if-eqz v6, :cond_13

    invoke-virtual {v3}, Lcom/android/server/am/AppStateManager$AppState$RunningProcess;->getAdj()I

    move-result v8

    const/16 v9, 0xc8

    if-le v8, v9, :cond_13

    move v4, v7

    :cond_13
    return v4

    .line 508
    .end local v5    # "appState":Lcom/android/server/am/AppStateManager$AppState;
    .end local v6    # "isBlackList":Z
    :cond_14
    move/from16 v10, p2

    move-object/from16 v13, p3

    goto :goto_3

    .line 507
    :cond_15
    move/from16 v10, p2

    move-object/from16 v13, p3

    .line 509
    :goto_3
    return v4

    .line 499
    .end local v3    # "recProcessState":Lcom/android/server/am/AppStateManager$AppState$RunningProcess;
    :cond_16
    move/from16 v10, p2

    move-object/from16 v13, p3

    goto :goto_4

    .line 498
    :cond_17
    move/from16 v10, p2

    move-object/from16 v13, p3

    .line 501
    :goto_4
    return v4
.end method

.method public shouldInterceptProviderLocked(ILcom/android/server/am/ProcessRecord;Lcom/android/server/am/ProcessRecord;Z)Z
    .locals 5
    .param p1, "callingUid"    # I
    .param p2, "callingProc"    # Lcom/android/server/am/ProcessRecord;
    .param p3, "hostingProc"    # Lcom/android/server/am/ProcessRecord;
    .param p4, "isRunning"    # Z

    .line 439
    const/4 v0, 0x0

    if-eqz p4, :cond_9

    invoke-direct {p0}, Lcom/miui/server/smartpower/SmartPowerPolicyManager;->isEnableIntercept()Z

    move-result v1

    if-eqz v1, :cond_9

    .line 440
    iget-object v1, p0, Lcom/miui/server/smartpower/SmartPowerPolicyManager;->mAppStateManager:Lcom/android/server/am/AppStateManager;

    iget v2, p3, Lcom/android/server/am/ProcessRecord;->uid:I

    iget v3, p3, Lcom/android/server/am/ProcessRecord;->mPid:I

    .line 441
    invoke-virtual {v1, v2, v3}, Lcom/android/server/am/AppStateManager;->getRunningProcess(II)Lcom/android/server/am/AppStateManager$AppState$RunningProcess;

    move-result-object v1

    .line 443
    .local v1, "hostingProcessState":Lcom/android/server/am/AppStateManager$AppState$RunningProcess;
    if-eqz v1, :cond_8

    .line 444
    invoke-virtual {v1}, Lcom/android/server/am/AppStateManager$AppState$RunningProcess;->getPackageName()Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Lcom/miui/server/smartpower/SmartPowerPolicyManager;->isPackageInterceptList(Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_8

    .line 445
    invoke-virtual {v1}, Lcom/android/server/am/AppStateManager$AppState$RunningProcess;->getPackageName()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {p0, v2}, Lcom/miui/server/smartpower/SmartPowerPolicyManager;->isProviderPkgBlackList(Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_0

    .line 446
    invoke-virtual {v1}, Lcom/android/server/am/AppStateManager$AppState$RunningProcess;->canHibernate()Z

    move-result v2

    if-nez v2, :cond_1

    goto/16 :goto_3

    :cond_0
    invoke-virtual {v1}, Lcom/android/server/am/AppStateManager$AppState$RunningProcess;->isIdle()Z

    move-result v2

    if-eqz v2, :cond_8

    .line 447
    :cond_1
    invoke-virtual {v1}, Lcom/android/server/am/AppStateManager$AppState$RunningProcess;->isSystemApp()Z

    move-result v2

    if-nez v2, :cond_8

    .line 448
    invoke-virtual {v1}, Lcom/android/server/am/AppStateManager$AppState$RunningProcess;->isSystemSignature()Z

    move-result v2

    if-nez v2, :cond_8

    .line 449
    invoke-virtual {v1}, Lcom/android/server/am/AppStateManager$AppState$RunningProcess;->getPackageName()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {p0, v2}, Lcom/miui/server/smartpower/SmartPowerPolicyManager;->isProviderPackageWhiteList(Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_2

    goto/16 :goto_3

    .line 453
    :cond_2
    if-eqz p2, :cond_5

    .line 454
    iget-object v2, p0, Lcom/miui/server/smartpower/SmartPowerPolicyManager;->mAppStateManager:Lcom/android/server/am/AppStateManager;

    iget v3, p2, Lcom/android/server/am/ProcessRecord;->uid:I

    iget v4, p2, Lcom/android/server/am/ProcessRecord;->mPid:I

    .line 455
    invoke-virtual {v2, v3, v4}, Lcom/android/server/am/AppStateManager;->getRunningProcess(II)Lcom/android/server/am/AppStateManager$AppState$RunningProcess;

    move-result-object v2

    .line 456
    .local v2, "callingProcessState":Lcom/android/server/am/AppStateManager$AppState$RunningProcess;
    if-eqz v2, :cond_4

    .line 457
    invoke-virtual {v2}, Lcom/android/server/am/AppStateManager$AppState$RunningProcess;->isSystemApp()Z

    move-result v3

    if-nez v3, :cond_4

    iget v3, p2, Lcom/android/server/am/ProcessRecord;->mPid:I

    iget v4, p3, Lcom/android/server/am/ProcessRecord;->mPid:I

    if-eq v3, v4, :cond_4

    .line 459
    invoke-virtual {v2}, Lcom/android/server/am/AppStateManager$AppState$RunningProcess;->canHibernate()Z

    move-result v3

    if-nez v3, :cond_3

    goto :goto_0

    .line 462
    .end local v2    # "callingProcessState":Lcom/android/server/am/AppStateManager$AppState$RunningProcess;
    :cond_3
    goto :goto_1

    .line 460
    .restart local v2    # "callingProcessState":Lcom/android/server/am/AppStateManager$AppState$RunningProcess;
    :cond_4
    :goto_0
    return v0

    .line 463
    .end local v2    # "callingProcessState":Lcom/android/server/am/AppStateManager$AppState$RunningProcess;
    :cond_5
    iget-object v2, p0, Lcom/miui/server/smartpower/SmartPowerPolicyManager;->mAppStateManager:Lcom/android/server/am/AppStateManager;

    invoke-virtual {v2, p1}, Lcom/android/server/am/AppStateManager;->getAppState(I)Lcom/android/server/am/AppStateManager$AppState;

    move-result-object v2

    .line 464
    .local v2, "callingAppState":Lcom/android/server/am/AppStateManager$AppState;
    if-eqz v2, :cond_7

    invoke-virtual {v2}, Lcom/android/server/am/AppStateManager$AppState;->isVsible()Z

    move-result v3

    if-nez v3, :cond_7

    .line 465
    invoke-virtual {p0, v2}, Lcom/miui/server/smartpower/SmartPowerPolicyManager;->isAppHibernationWhiteList(Lcom/android/server/am/AppStateManager$AppState;)Z

    move-result v3

    if-eqz v3, :cond_6

    goto :goto_2

    .line 469
    .end local v2    # "callingAppState":Lcom/android/server/am/AppStateManager$AppState;
    :cond_6
    :goto_1
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "power prv h:{ u:"

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget v2, p3, Lcom/android/server/am/ProcessRecord;->uid:I

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v2, " p:"

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v2, p3, Lcom/android/server/am/ProcessRecord;->processName:Ljava/lang/String;

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v2, " s:"

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    .line 471
    invoke-virtual {v1}, Lcom/android/server/am/AppStateManager$AppState$RunningProcess;->getCurrentState()I

    move-result v2

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string/jumbo v2, "} c:"

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    .line 469
    const v2, 0x15ff2

    invoke-static {v2, v0}, Landroid/util/EventLog;->writeEvent(ILjava/lang/String;)I

    .line 473
    const/4 v0, 0x1

    return v0

    .line 466
    .restart local v2    # "callingAppState":Lcom/android/server/am/AppStateManager$AppState;
    :cond_7
    :goto_2
    return v0

    .line 450
    .end local v2    # "callingAppState":Lcom/android/server/am/AppStateManager$AppState;
    :cond_8
    :goto_3
    return v0

    .line 475
    .end local v1    # "hostingProcessState":Lcom/android/server/am/AppStateManager$AppState$RunningProcess;
    :cond_9
    return v0
.end method

.method public shouldInterceptService(Landroid/content/Intent;Lmiui/security/CallerInfo;Landroid/content/pm/ServiceInfo;)Z
    .locals 12
    .param p1, "service"    # Landroid/content/Intent;
    .param p2, "callingInfo"    # Lmiui/security/CallerInfo;
    .param p3, "serviceInfo"    # Landroid/content/pm/ServiceInfo;

    .line 629
    invoke-direct {p0}, Lcom/miui/server/smartpower/SmartPowerPolicyManager;->isEnableIntercept()Z

    move-result v0

    const/4 v1, 0x0

    if-eqz v0, :cond_7

    iget-object v0, p3, Landroid/content/pm/ServiceInfo;->applicationInfo:Landroid/content/pm/ApplicationInfo;

    iget-object v0, v0, Landroid/content/pm/ApplicationInfo;->packageName:Ljava/lang/String;

    .line 630
    invoke-static {v0}, Lcom/miui/server/smartpower/SmartPowerPolicyManager;->isPackageInterceptList(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_0

    goto/16 :goto_3

    .line 633
    :cond_0
    iget-object v0, p0, Lcom/miui/server/smartpower/SmartPowerPolicyManager;->mAppStateManager:Lcom/android/server/am/AppStateManager;

    iget v2, p2, Lmiui/security/CallerInfo;->callerUid:I

    iget v3, p2, Lmiui/security/CallerInfo;->callerPid:I

    .line 634
    invoke-virtual {v0, v2, v3}, Lcom/android/server/am/AppStateManager;->getRunningProcess(II)Lcom/android/server/am/AppStateManager$AppState$RunningProcess;

    move-result-object v0

    .line 635
    .local v0, "callingProcessState":Lcom/android/server/am/AppStateManager$AppState$RunningProcess;
    iget-object v2, p3, Landroid/content/pm/ServiceInfo;->applicationInfo:Landroid/content/pm/ApplicationInfo;

    iget v2, v2, Landroid/content/pm/ApplicationInfo;->uid:I

    .line 636
    .local v2, "serviceUid":I
    iget-object v3, p3, Landroid/content/pm/ServiceInfo;->applicationInfo:Landroid/content/pm/ApplicationInfo;

    iget-object v3, v3, Landroid/content/pm/ApplicationInfo;->packageName:Ljava/lang/String;

    .line 637
    .local v3, "servicePckName":Ljava/lang/String;
    iget-object v4, p3, Landroid/content/pm/ServiceInfo;->processName:Ljava/lang/String;

    .line 638
    .local v4, "serviceProcName":Ljava/lang/String;
    iget-object v5, p2, Lmiui/security/CallerInfo;->callerProcessName:Ljava/lang/String;

    invoke-virtual {v4, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    .line 639
    .local v5, "isSelfProc":Z
    const/4 v6, 0x1

    if-eqz v5, :cond_1

    const-string v7, ":widgetProvider"

    invoke-virtual {v4, v7}, Ljava/lang/String;->endsWith(Ljava/lang/String;)Z

    move-result v7

    if-eqz v7, :cond_1

    move v7, v6

    goto :goto_0

    :cond_1
    move v7, v1

    .line 640
    .local v7, "isWidgetSelf":Z
    :goto_0
    if-eqz v0, :cond_6

    .line 641
    if-eqz v5, :cond_2

    invoke-virtual {v0}, Lcom/android/server/am/AppStateManager$AppState$RunningProcess;->isSystemSignature()Z

    move-result v8

    if-eqz v8, :cond_2

    move v8, v6

    goto :goto_1

    :cond_2
    move v8, v1

    .line 642
    .local v8, "isSystemSignSelf":Z
    :goto_1
    iget v9, p2, Lmiui/security/CallerInfo;->callerUid:I

    if-ne v9, v2, :cond_6

    .line 643
    invoke-virtual {v0}, Lcom/android/server/am/AppStateManager$AppState$RunningProcess;->isSystemApp()Z

    move-result v9

    if-nez v9, :cond_6

    .line 644
    invoke-virtual {v0}, Lcom/android/server/am/AppStateManager$AppState$RunningProcess;->isAutoStartApp()Z

    move-result v9

    if-nez v9, :cond_6

    if-nez v7, :cond_6

    if-nez v8, :cond_6

    .line 647
    invoke-virtual {v0}, Lcom/android/server/am/AppStateManager$AppState$RunningProcess;->canHibernate()Z

    move-result v9

    if-eqz v9, :cond_6

    .line 648
    invoke-virtual {p0, v3}, Lcom/miui/server/smartpower/SmartPowerPolicyManager;->isServicePackageWhiteList(Ljava/lang/String;)Z

    move-result v9

    if-nez v9, :cond_6

    .line 649
    iget-object v9, p0, Lcom/miui/server/smartpower/SmartPowerPolicyManager;->mAppStateManager:Lcom/android/server/am/AppStateManager;

    .line 650
    invoke-virtual {v9, v2, v4}, Lcom/android/server/am/AppStateManager;->getRunningProcess(ILjava/lang/String;)Lcom/android/server/am/AppStateManager$AppState$RunningProcess;

    move-result-object v9

    .line 651
    .local v9, "serverProcessState":Lcom/android/server/am/AppStateManager$AppState$RunningProcess;
    if-eqz v9, :cond_6

    .line 652
    invoke-virtual {p0, v3}, Lcom/miui/server/smartpower/SmartPowerPolicyManager;->isServicePkgBlackList(Ljava/lang/String;)Z

    move-result v10

    if-eqz v10, :cond_3

    .line 653
    invoke-virtual {v9}, Lcom/android/server/am/AppStateManager$AppState$RunningProcess;->canHibernate()Z

    move-result v10

    if-eqz v10, :cond_6

    goto :goto_2

    :cond_3
    invoke-virtual {v9}, Lcom/android/server/am/AppStateManager$AppState$RunningProcess;->isIdle()Z

    move-result v10

    if-eqz v10, :cond_6

    .line 654
    :goto_2
    invoke-virtual {v9}, Lcom/android/server/am/AppStateManager$AppState$RunningProcess;->getPid()I

    move-result v10

    iget v11, p2, Lmiui/security/CallerInfo;->callerPid:I

    if-ne v10, v11, :cond_4

    iget v10, p2, Lmiui/security/CallerInfo;->callerUid:I

    .line 655
    invoke-direct {p0, v10}, Lcom/miui/server/smartpower/SmartPowerPolicyManager;->hasPidWhiteList(I)Z

    move-result v10

    if-nez v10, :cond_6

    .line 656
    :cond_4
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v10, "power ser h:{ u:"

    invoke-virtual {v1, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v10, " p:"

    invoke-virtual {v1, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v10, "/"

    invoke-virtual {v1, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    .line 658
    invoke-virtual {v9}, Lcom/android/server/am/AppStateManager$AppState$RunningProcess;->getPid()I

    move-result v11

    invoke-virtual {v1, v11}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v11, " s:"

    invoke-virtual {v1, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    .line 659
    invoke-virtual {v9}, Lcom/android/server/am/AppStateManager$AppState$RunningProcess;->getCurrentState()I

    move-result v11

    invoke-virtual {v1, v11}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string/jumbo v11, "} c:"

    invoke-virtual {v1, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget v11, p2, Lmiui/security/CallerInfo;->callerUid:I

    invoke-virtual {v1, v11}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v11, ":"

    invoke-virtual {v1, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget v11, p2, Lmiui/security/CallerInfo;->callerPid:I

    invoke-virtual {v1, v11}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    .line 656
    const v11, 0x15ff2

    invoke-static {v11, v1}, Landroid/util/EventLog;->writeEvent(ILjava/lang/String;)I

    .line 662
    sget-boolean v1, Lcom/miui/server/smartpower/SmartPowerPolicyManager;->DEBUG:Z

    if-eqz v1, :cond_5

    .line 663
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v11, "Intercept: service calling uid "

    invoke-virtual {v1, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget v11, p2, Lmiui/security/CallerInfo;->callerUid:I

    invoke-virtual {v1, v11}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v10, p2, Lmiui/security/CallerInfo;->callerProcessName:Ljava/lang/String;

    invoke-virtual {v1, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v10, " serviceState:"

    invoke-virtual {v1, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    .line 665
    invoke-virtual {v0}, Lcom/android/server/am/AppStateManager$AppState$RunningProcess;->getCurrentState()I

    move-result v10

    invoke-virtual {v1, v10}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v10, " serviceInfo( processName="

    invoke-virtual {v1, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v10, " getComponentName="

    invoke-virtual {v1, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    .line 667
    invoke-virtual {p3}, Landroid/content/pm/ServiceInfo;->getComponentName()Landroid/content/ComponentName;

    move-result-object v10

    invoke-virtual {v1, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v10, " uid="

    invoke-virtual {v1, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v10, ") Intent="

    invoke-virtual {v1, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    .line 663
    const-string v10, "SmartPower.PowerPolicy"

    invoke-static {v10, v1}, Landroid/util/Slog;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 671
    :cond_5
    return v6

    .line 675
    .end local v8    # "isSystemSignSelf":Z
    .end local v9    # "serverProcessState":Lcom/android/server/am/AppStateManager$AppState$RunningProcess;
    :cond_6
    return v1

    .line 631
    .end local v0    # "callingProcessState":Lcom/android/server/am/AppStateManager$AppState$RunningProcess;
    .end local v2    # "serviceUid":I
    .end local v3    # "servicePckName":Ljava/lang/String;
    .end local v4    # "serviceProcName":Ljava/lang/String;
    .end local v5    # "isSelfProc":Z
    .end local v7    # "isWidgetSelf":Z
    :cond_7
    :goto_3
    return v1
.end method

.method public skipFrozenAppAnr(Landroid/content/pm/ApplicationInfo;ILjava/lang/String;)Z
    .locals 6
    .param p1, "info"    # Landroid/content/pm/ApplicationInfo;
    .param p2, "uid"    # I
    .param p3, "report"    # Ljava/lang/String;

    .line 592
    invoke-direct {p0}, Lcom/miui/server/smartpower/SmartPowerPolicyManager;->isEnableFrozen()Z

    move-result v0

    const/4 v1, 0x0

    if-nez v0, :cond_0

    .line 593
    return v1

    .line 595
    :cond_0
    iget-object v0, p0, Lcom/miui/server/smartpower/SmartPowerPolicyManager;->mPowerFrozenManager:Lcom/miui/server/smartpower/PowerFrozenManager;

    invoke-virtual {v0, p2}, Lcom/miui/server/smartpower/PowerFrozenManager;->isFrozenForUid(I)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 596
    const/4 v0, 0x1

    return v0

    .line 598
    :cond_1
    invoke-static {p3}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 599
    return v1

    .line 601
    :cond_2
    const/4 v0, 0x0

    .line 602
    .local v0, "timeout":I
    const-string v2, "Broadcast of"

    invoke-virtual {p3, v2}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_3

    .line 603
    const/16 v0, 0x4e20

    goto :goto_0

    .line 604
    :cond_3
    const-string v2, "executing service"

    invoke-virtual {p3, v2}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_4

    .line 605
    const/16 v0, 0x4e20

    goto :goto_0

    .line 606
    :cond_4
    const-string v2, "ContentProvider not"

    invoke-virtual {p3, v2}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_5

    .line 607
    const/16 v0, 0x2710

    goto :goto_0

    .line 608
    :cond_5
    const-string v2, "Input dispatching"

    invoke-virtual {p3, v2}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_6

    .line 609
    const/16 v0, 0x1388

    .line 613
    :goto_0
    iget-object v1, p0, Lcom/miui/server/smartpower/SmartPowerPolicyManager;->mAppStateManager:Lcom/android/server/am/AppStateManager;

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v2

    int-to-long v4, v0

    sub-long/2addr v2, v4

    invoke-virtual {v1, p2, v2, v3}, Lcom/android/server/am/AppStateManager;->isRecentThawed(IJ)Z

    move-result v1

    return v1

    .line 611
    :cond_6
    return v1
.end method

.method public updateCloudAlarmWhiteLit(Ljava/lang/String;)V
    .locals 4
    .param p1, "procList"    # Ljava/lang/String;

    .line 404
    invoke-static {p1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 405
    const-string v0, ","

    invoke-virtual {p1, v0}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v0

    .line 406
    .local v0, "procArray":[Ljava/lang/String;
    array-length v1, v0

    const/4 v2, 0x0

    :goto_0
    if-ge v2, v1, :cond_0

    aget-object v3, v0, v2

    .line 407
    .local v3, "proc":Ljava/lang/String;
    invoke-direct {p0, v3}, Lcom/miui/server/smartpower/SmartPowerPolicyManager;->addAlarmPackageWhiteList(Ljava/lang/String;)V

    .line 406
    .end local v3    # "proc":Ljava/lang/String;
    add-int/lit8 v2, v2, 0x1

    goto :goto_0

    .line 410
    .end local v0    # "procArray":[Ljava/lang/String;
    :cond_0
    return-void
.end method

.method public updateCloudBroadcastWhiteLit(Ljava/lang/String;)V
    .locals 4
    .param p1, "procList"    # Ljava/lang/String;

    .line 395
    invoke-static {p1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 396
    const-string v0, ","

    invoke-virtual {p1, v0}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v0

    .line 397
    .local v0, "procArray":[Ljava/lang/String;
    array-length v1, v0

    const/4 v2, 0x0

    :goto_0
    if-ge v2, v1, :cond_0

    aget-object v3, v0, v2

    .line 398
    .local v3, "procAction":Ljava/lang/String;
    invoke-direct {p0, v3}, Lcom/miui/server/smartpower/SmartPowerPolicyManager;->addBroadcastProcessActionWhiteList(Ljava/lang/String;)V

    .line 397
    .end local v3    # "procAction":Ljava/lang/String;
    add-int/lit8 v2, v2, 0x1

    goto :goto_0

    .line 401
    .end local v0    # "procArray":[Ljava/lang/String;
    :cond_0
    return-void
.end method

.method public updateCloudPackageWhiteList(Ljava/lang/String;)V
    .locals 5
    .param p1, "pkgList"    # Ljava/lang/String;

    .line 365
    invoke-static {p1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 366
    const-string v0, ","

    invoke-virtual {p1, v0}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v0

    .line 367
    .local v0, "pkgArray":[Ljava/lang/String;
    array-length v1, v0

    const/4 v2, 0x0

    :goto_0
    if-ge v2, v1, :cond_0

    aget-object v3, v0, v2

    .line 368
    .local v3, "packageName":Ljava/lang/String;
    const/4 v4, 0x4

    invoke-direct {p0, v3, v4}, Lcom/miui/server/smartpower/SmartPowerPolicyManager;->addPackageWhiteList(Ljava/lang/String;I)V

    .line 367
    .end local v3    # "packageName":Ljava/lang/String;
    add-int/lit8 v2, v2, 0x1

    goto :goto_0

    .line 371
    .end local v0    # "pkgArray":[Ljava/lang/String;
    :cond_0
    return-void
.end method

.method public updateCloudProcessWhiteList(Ljava/lang/String;)V
    .locals 5
    .param p1, "procList"    # Ljava/lang/String;

    .line 374
    invoke-static {p1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 375
    const-string v0, ","

    invoke-virtual {p1, v0}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v0

    .line 376
    .local v0, "procArray":[Ljava/lang/String;
    array-length v1, v0

    const/4 v2, 0x0

    :goto_0
    if-ge v2, v1, :cond_0

    aget-object v3, v0, v2

    .line 377
    .local v3, "procName":Ljava/lang/String;
    const/4 v4, 0x4

    invoke-direct {p0, v3, v4}, Lcom/miui/server/smartpower/SmartPowerPolicyManager;->addProcessWhiteList(Ljava/lang/String;I)V

    .line 376
    .end local v3    # "procName":Ljava/lang/String;
    add-int/lit8 v2, v2, 0x1

    goto :goto_0

    .line 380
    .end local v0    # "procArray":[Ljava/lang/String;
    :cond_0
    return-void
.end method

.method public updateCloudProviderWhiteLit(Ljava/lang/String;)V
    .locals 4
    .param p1, "procList"    # Ljava/lang/String;

    .line 413
    invoke-static {p1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 414
    const-string v0, ","

    invoke-virtual {p1, v0}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v0

    .line 415
    .local v0, "procArray":[Ljava/lang/String;
    array-length v1, v0

    const/4 v2, 0x0

    :goto_0
    if-ge v2, v1, :cond_0

    aget-object v3, v0, v2

    .line 416
    .local v3, "proc":Ljava/lang/String;
    invoke-direct {p0, v3}, Lcom/miui/server/smartpower/SmartPowerPolicyManager;->addProviderPackageWhiteList(Ljava/lang/String;)V

    .line 415
    .end local v3    # "proc":Ljava/lang/String;
    add-int/lit8 v2, v2, 0x1

    goto :goto_0

    .line 419
    .end local v0    # "procArray":[Ljava/lang/String;
    :cond_0
    return-void
.end method

.method public updateCloudServiceWhiteLit(Ljava/lang/String;)V
    .locals 4
    .param p1, "procList"    # Ljava/lang/String;

    .line 422
    invoke-static {p1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 423
    const-string v0, ","

    invoke-virtual {p1, v0}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v0

    .line 424
    .local v0, "procArray":[Ljava/lang/String;
    array-length v1, v0

    const/4 v2, 0x0

    :goto_0
    if-ge v2, v1, :cond_0

    aget-object v3, v0, v2

    .line 425
    .local v3, "proc":Ljava/lang/String;
    invoke-direct {p0, v3}, Lcom/miui/server/smartpower/SmartPowerPolicyManager;->addServicePackageWhiteList(Ljava/lang/String;)V

    .line 424
    .end local v3    # "proc":Ljava/lang/String;
    add-int/lit8 v2, v2, 0x1

    goto :goto_0

    .line 428
    .end local v0    # "procArray":[Ljava/lang/String;
    :cond_0
    return-void
.end method

.method public updateCloudSreenonWhiteList(Ljava/lang/String;)V
    .locals 5
    .param p1, "sreenonList"    # Ljava/lang/String;

    .line 383
    invoke-static {p1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 384
    const-string v0, ","

    invoke-virtual {p1, v0}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v0

    .line 385
    .local v0, "screenAppArray":[Ljava/lang/String;
    array-length v1, v0

    const/4 v2, 0x0

    :goto_0
    if-ge v2, v1, :cond_0

    aget-object v3, v0, v2

    .line 386
    .local v3, "packageName":Ljava/lang/String;
    const/16 v4, 0x2000

    invoke-direct {p0, v3, v4}, Lcom/miui/server/smartpower/SmartPowerPolicyManager;->addSreenonWhiteList(Ljava/lang/String;I)V

    .line 385
    .end local v3    # "packageName":Ljava/lang/String;
    add-int/lit8 v2, v2, 0x1

    goto :goto_0

    .line 389
    .end local v0    # "screenAppArray":[Ljava/lang/String;
    :cond_0
    return-void
.end method
