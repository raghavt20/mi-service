public class com.miui.server.smartpower.AppCameraResource extends com.miui.server.smartpower.AppPowerResource {
	 /* .source "AppCameraResource.java" */
	 /* # annotations */
	 /* .annotation system Ldalvik/annotation/MemberClasses; */
	 /* value = { */
	 /* Lcom/miui/server/smartpower/AppCameraResource$CameraRecord; */
	 /* } */
} // .end annotation
/* # instance fields */
private final android.util.SparseArray mActivePidsMap;
/* .annotation system Ldalvik/annotation/Signature; */
/* value = { */
/* "Landroid/util/SparseArray<", */
/* "Lcom/miui/server/smartpower/AppCameraResource$CameraRecord;", */
/* ">;" */
/* } */
} // .end annotation
} // .end field
/* # direct methods */
public com.miui.server.smartpower.AppCameraResource ( ) {
/* .locals 1 */
/* .param p1, "context" # Landroid/content/Context; */
/* .param p2, "looper" # Landroid/os/Looper; */
/* .line 20 */
/* invoke-direct {p0}, Lcom/miui/server/smartpower/AppPowerResource;-><init>()V */
/* .line 18 */
/* new-instance v0, Landroid/util/SparseArray; */
/* invoke-direct {v0}, Landroid/util/SparseArray;-><init>()V */
this.mActivePidsMap = v0;
/* .line 21 */
int v0 = 6; // const/4 v0, 0x6
/* iput v0, p0, Lcom/miui/server/smartpower/AppCameraResource;->mType:I */
/* .line 22 */
return;
} // .end method
/* # virtual methods */
public java.util.ArrayList getActiveUids ( ) {
/* .locals 1 */
/* .annotation system Ldalvik/annotation/Signature; */
/* value = { */
/* "()", */
/* "Ljava/util/ArrayList<", */
/* "Ljava/lang/Integer;", */
/* ">;" */
/* } */
} // .end annotation
/* .line 26 */
int v0 = 0; // const/4 v0, 0x0
} // .end method
public Boolean isAppResourceActive ( Integer p0 ) {
/* .locals 5 */
/* .param p1, "uid" # I */
/* .line 31 */
int v0 = 0; // const/4 v0, 0x0
/* .line 32 */
/* .local v0, "active":Z */
v1 = this.mActivePidsMap;
/* monitor-enter v1 */
/* .line 33 */
int v2 = 0; // const/4 v2, 0x0
/* .local v2, "i":I */
} // :goto_0
try { // :try_start_0
v3 = this.mActivePidsMap;
v3 = (( android.util.SparseArray ) v3 ).size ( ); // invoke-virtual {v3}, Landroid/util/SparseArray;->size()I
/* if-ge v2, v3, :cond_1 */
/* .line 34 */
v3 = this.mActivePidsMap;
(( android.util.SparseArray ) v3 ).valueAt ( v2 ); // invoke-virtual {v3, v2}, Landroid/util/SparseArray;->valueAt(I)Ljava/lang/Object;
/* check-cast v3, Lcom/miui/server/smartpower/AppCameraResource$CameraRecord; */
/* .line 35 */
/* .local v3, "record":Lcom/miui/server/smartpower/AppCameraResource$CameraRecord; */
if ( v3 != null) { // if-eqz v3, :cond_0
v4 = com.miui.server.smartpower.AppCameraResource$CameraRecord .-$$Nest$fgetmCallerUid ( v3 );
/* if-ne v4, p1, :cond_0 */
/* .line 36 */
/* monitor-exit v1 */
int v1 = 1; // const/4 v1, 0x1
/* .line 33 */
} // .end local v3 # "record":Lcom/miui/server/smartpower/AppCameraResource$CameraRecord;
} // :cond_0
/* add-int/lit8 v2, v2, 0x1 */
/* .line 39 */
} // .end local v2 # "i":I
} // :cond_1
/* monitor-exit v1 */
/* .line 40 */
/* .line 39 */
/* :catchall_0 */
/* move-exception v2 */
/* monitor-exit v1 */
/* :try_end_0 */
/* .catchall {:try_start_0 ..:try_end_0} :catchall_0 */
/* throw v2 */
} // .end method
public Boolean isAppResourceActive ( Integer p0, Integer p1 ) {
/* .locals 2 */
/* .param p1, "uid" # I */
/* .param p2, "pid" # I */
/* .line 45 */
v0 = this.mActivePidsMap;
/* monitor-enter v0 */
/* .line 46 */
try { // :try_start_0
v1 = this.mActivePidsMap;
v1 = (( android.util.SparseArray ) v1 ).contains ( p2 ); // invoke-virtual {v1, p2}, Landroid/util/SparseArray;->contains(I)Z
/* monitor-exit v0 */
/* .line 47 */
/* :catchall_0 */
/* move-exception v1 */
/* monitor-exit v0 */
/* :try_end_0 */
/* .catchall {:try_start_0 ..:try_end_0} :catchall_0 */
/* throw v1 */
} // .end method
public void notifyCameraForegroundState ( java.lang.String p0, Boolean p1, java.lang.String p2, Integer p3, Integer p4 ) {
/* .locals 4 */
/* .param p1, "cameraId" # Ljava/lang/String; */
/* .param p2, "isForeground" # Z */
/* .param p3, "caller" # Ljava/lang/String; */
/* .param p4, "callerUid" # I */
/* .param p5, "callerPid" # I */
/* .line 60 */
v0 = this.mActivePidsMap;
/* monitor-enter v0 */
/* .line 61 */
try { // :try_start_0
v1 = this.mActivePidsMap;
(( android.util.SparseArray ) v1 ).get ( p5 ); // invoke-virtual {v1, p5}, Landroid/util/SparseArray;->get(I)Ljava/lang/Object;
/* check-cast v1, Lcom/miui/server/smartpower/AppCameraResource$CameraRecord; */
/* .line 62 */
/* .local v1, "record":Lcom/miui/server/smartpower/AppCameraResource$CameraRecord; */
if ( p2 != null) { // if-eqz p2, :cond_0
/* .line 63 */
/* if-nez v1, :cond_1 */
/* .line 64 */
/* new-instance v2, Lcom/miui/server/smartpower/AppCameraResource$CameraRecord; */
/* invoke-direct {v2, p0, p3, p4, p5}, Lcom/miui/server/smartpower/AppCameraResource$CameraRecord;-><init>(Lcom/miui/server/smartpower/AppCameraResource;Ljava/lang/String;II)V */
/* move-object v1, v2 */
/* .line 65 */
v2 = this.mActivePidsMap;
(( android.util.SparseArray ) v2 ).put ( p5, v1 ); // invoke-virtual {v2, p5, v1}, Landroid/util/SparseArray;->put(ILjava/lang/Object;)V
/* .line 68 */
} // :cond_0
if ( v1 != null) { // if-eqz v1, :cond_1
/* .line 69 */
v2 = this.mActivePidsMap;
/* monitor-enter v2 */
/* :try_end_0 */
/* .catchall {:try_start_0 ..:try_end_0} :catchall_1 */
/* .line 70 */
try { // :try_start_1
v3 = this.mActivePidsMap;
(( android.util.SparseArray ) v3 ).remove ( p5 ); // invoke-virtual {v3, p5}, Landroid/util/SparseArray;->remove(I)V
/* .line 71 */
/* monitor-exit v2 */
/* :catchall_0 */
/* move-exception v3 */
/* monitor-exit v2 */
/* :try_end_1 */
/* .catchall {:try_start_1 ..:try_end_1} :catchall_0 */
} // .end local p0 # "this":Lcom/miui/server/smartpower/AppCameraResource;
} // .end local p1 # "cameraId":Ljava/lang/String;
} // .end local p2 # "isForeground":Z
} // .end local p3 # "caller":Ljava/lang/String;
} // .end local p4 # "callerUid":I
} // .end local p5 # "callerPid":I
try { // :try_start_2
/* throw v3 */
/* .line 74 */
/* .restart local p0 # "this":Lcom/miui/server/smartpower/AppCameraResource; */
/* .restart local p1 # "cameraId":Ljava/lang/String; */
/* .restart local p2 # "isForeground":Z */
/* .restart local p3 # "caller":Ljava/lang/String; */
/* .restart local p4 # "callerUid":I */
/* .restart local p5 # "callerPid":I */
} // :cond_1
} // :goto_0
/* monitor-exit v0 */
/* :try_end_2 */
/* .catchall {:try_start_2 ..:try_end_2} :catchall_1 */
/* .line 75 */
if ( v1 != null) { // if-eqz v1, :cond_2
/* .line 76 */
(( com.miui.server.smartpower.AppCameraResource$CameraRecord ) v1 ).notifyCameraForegroundState ( p1, p2 ); // invoke-virtual {v1, p1, p2}, Lcom/miui/server/smartpower/AppCameraResource$CameraRecord;->notifyCameraForegroundState(Ljava/lang/String;Z)V
/* .line 78 */
} // :cond_2
return;
/* .line 74 */
} // .end local v1 # "record":Lcom/miui/server/smartpower/AppCameraResource$CameraRecord;
/* :catchall_1 */
/* move-exception v1 */
try { // :try_start_3
/* monitor-exit v0 */
/* :try_end_3 */
/* .catchall {:try_start_3 ..:try_end_3} :catchall_1 */
/* throw v1 */
} // .end method
public void releaseAppPowerResource ( Integer p0 ) {
/* .locals 0 */
/* .param p1, "uid" # I */
/* .line 51 */
return;
} // .end method
public void resumeAppPowerResource ( Integer p0 ) {
/* .locals 0 */
/* .param p1, "uid" # I */
/* .line 54 */
return;
} // .end method
