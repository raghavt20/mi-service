.class Lcom/miui/server/smartpower/SmartCpuPolicyManager$H;
.super Landroid/os/Handler;
.source "SmartCpuPolicyManager.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/miui/server/smartpower/SmartCpuPolicyManager;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "H"
.end annotation


# instance fields
.field final synthetic this$0:Lcom/miui/server/smartpower/SmartCpuPolicyManager;


# direct methods
.method public constructor <init>(Lcom/miui/server/smartpower/SmartCpuPolicyManager;Landroid/os/Looper;)V
    .locals 0
    .param p2, "looper"    # Landroid/os/Looper;

    .line 230
    iput-object p1, p0, Lcom/miui/server/smartpower/SmartCpuPolicyManager$H;->this$0:Lcom/miui/server/smartpower/SmartCpuPolicyManager;

    .line 231
    invoke-direct {p0, p2}, Landroid/os/Handler;-><init>(Landroid/os/Looper;)V

    .line 232
    return-void
.end method


# virtual methods
.method public handleMessage(Landroid/os/Message;)V
    .locals 2
    .param p1, "msg"    # Landroid/os/Message;

    .line 236
    invoke-super {p0, p1}, Landroid/os/Handler;->handleMessage(Landroid/os/Message;)V

    .line 237
    iget v0, p1, Landroid/os/Message;->what:I

    packed-switch v0, :pswitch_data_0

    goto :goto_0

    .line 240
    :pswitch_0
    :try_start_0
    iget-object v0, p0, Lcom/miui/server/smartpower/SmartCpuPolicyManager$H;->this$0:Lcom/miui/server/smartpower/SmartCpuPolicyManager;

    iget v1, p1, Landroid/os/Message;->arg1:I

    invoke-static {v0, v1}, Lcom/miui/server/smartpower/SmartCpuPolicyManager;->-$$Nest$mhandleLimitCpuException(Lcom/miui/server/smartpower/SmartCpuPolicyManager;I)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 243
    goto :goto_0

    .line 241
    :catch_0
    move-exception v0

    .line 242
    .local v0, "e":Ljava/lang/Exception;
    invoke-virtual {v0}, Ljava/lang/Exception;->printStackTrace()V

    .line 244
    .end local v0    # "e":Ljava/lang/Exception;
    nop

    .line 248
    :goto_0
    return-void

    nop

    :pswitch_data_0
    .packed-switch 0x7
        :pswitch_0
    .end packed-switch
.end method
