class com.miui.server.smartpower.SmartDisplayPolicyManager$GesturesEventListenerCallback implements com.android.server.wm.SchedBoostGesturesEvent$GesturesEventListener {
	 /* .source "SmartDisplayPolicyManager.java" */
	 /* # interfaces */
	 /* # annotations */
	 /* .annotation system Ldalvik/annotation/EnclosingClass; */
	 /* value = Lcom/miui/server/smartpower/SmartDisplayPolicyManager; */
} // .end annotation
/* .annotation system Ldalvik/annotation/InnerClass; */
/* accessFlags = 0x2 */
/* name = "GesturesEventListenerCallback" */
} // .end annotation
/* # instance fields */
final com.miui.server.smartpower.SmartDisplayPolicyManager this$0; //synthetic
/* # direct methods */
private com.miui.server.smartpower.SmartDisplayPolicyManager$GesturesEventListenerCallback ( ) {
/* .locals 0 */
/* .line 293 */
this.this$0 = p1;
/* invoke-direct {p0}, Ljava/lang/Object;-><init>()V */
return;
} // .end method
 com.miui.server.smartpower.SmartDisplayPolicyManager$GesturesEventListenerCallback ( ) { //synthethic
/* .locals 0 */
/* invoke-direct {p0, p1}, Lcom/miui/server/smartpower/SmartDisplayPolicyManager$GesturesEventListenerCallback;-><init>(Lcom/miui/server/smartpower/SmartDisplayPolicyManager;)V */
return;
} // .end method
private Boolean isVerticalFling ( Integer p0, Integer p1 ) {
/* .locals 2 */
/* .param p1, "velocityX" # I */
/* .param p2, "velocityY" # I */
/* .line 319 */
v0 = java.lang.Math .abs ( p2 );
v1 = java.lang.Math .abs ( p1 );
/* sub-int/2addr v0, v1 */
/* if-lez v0, :cond_0 */
int v0 = 1; // const/4 v0, 0x1
} // :cond_0
int v0 = 0; // const/4 v0, 0x0
} // :goto_0
} // .end method
/* # virtual methods */
public void onDown ( ) {
/* .locals 2 */
/* .line 310 */
v0 = this.this$0;
/* const/16 v1, 0x8 */
com.miui.server.smartpower.SmartDisplayPolicyManager .-$$Nest$mnotifyInputEventReceived ( v0,v1 );
/* .line 311 */
return;
} // .end method
public void onFling ( Float p0, Float p1, Integer p2 ) {
/* .locals 4 */
/* .param p1, "velocityX" # F */
/* .param p2, "velocityY" # F */
/* .param p3, "durationMs" # I */
/* .line 298 */
/* float-to-int v0, p1 */
/* float-to-int v1, p2 */
v0 = /* invoke-direct {p0, v0, v1}, Lcom/miui/server/smartpower/SmartDisplayPolicyManager$GesturesEventListenerCallback;->isVerticalFling(II)Z */
if ( v0 != null) { // if-eqz v0, :cond_0
/* .line 299 */
v0 = this.this$0;
/* .line 300 */
/* const/16 v1, 0xbb8 */
v1 = java.lang.Math .min ( p3,v1 );
/* int-to-long v1, v1 */
/* .line 299 */
/* const/16 v3, 0xa */
com.miui.server.smartpower.SmartDisplayPolicyManager .-$$Nest$mnotifyInputEventReceived ( v0,v3,v1,v2 );
/* .line 302 */
} // :cond_0
return;
} // .end method
public void onMove ( ) {
/* .locals 2 */
/* .line 315 */
v0 = this.this$0;
/* const/16 v1, 0x9 */
com.miui.server.smartpower.SmartDisplayPolicyManager .-$$Nest$mnotifyInputEventReceived ( v0,v1 );
/* .line 316 */
return;
} // .end method
public void onScroll ( Boolean p0 ) {
/* .locals 0 */
/* .param p1, "started" # Z */
/* .line 306 */
return;
} // .end method
