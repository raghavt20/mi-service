.class Lcom/miui/server/smartpower/SmartWindowPolicyManager$MultiTaskPolicyController$MultiTaskActionListenerDelegate;
.super Landroid/os/Handler;
.source "SmartWindowPolicyManager.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/miui/server/smartpower/SmartWindowPolicyManager$MultiTaskPolicyController;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0xa
    name = "MultiTaskActionListenerDelegate"
.end annotation


# instance fields
.field private mListener:Lcom/miui/app/smartpower/SmartPowerServiceInternal$MultiTaskActionListener;

.field private mSourceName:Ljava/lang/String;


# direct methods
.method public constructor <init>(Ljava/lang/String;Landroid/os/Looper;Lcom/miui/app/smartpower/SmartPowerServiceInternal$MultiTaskActionListener;)V
    .locals 1
    .param p1, "sourceName"    # Ljava/lang/String;
    .param p2, "looper"    # Landroid/os/Looper;
    .param p3, "listener"    # Lcom/miui/app/smartpower/SmartPowerServiceInternal$MultiTaskActionListener;

    .line 289
    invoke-direct {p0, p2}, Landroid/os/Handler;-><init>(Landroid/os/Looper;)V

    .line 284
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/miui/server/smartpower/SmartWindowPolicyManager$MultiTaskPolicyController$MultiTaskActionListenerDelegate;->mSourceName:Ljava/lang/String;

    .line 285
    iput-object v0, p0, Lcom/miui/server/smartpower/SmartWindowPolicyManager$MultiTaskPolicyController$MultiTaskActionListenerDelegate;->mListener:Lcom/miui/app/smartpower/SmartPowerServiceInternal$MultiTaskActionListener;

    .line 290
    iput-object p1, p0, Lcom/miui/server/smartpower/SmartWindowPolicyManager$MultiTaskPolicyController$MultiTaskActionListenerDelegate;->mSourceName:Ljava/lang/String;

    .line 291
    iput-object p3, p0, Lcom/miui/server/smartpower/SmartWindowPolicyManager$MultiTaskPolicyController$MultiTaskActionListenerDelegate;->mListener:Lcom/miui/app/smartpower/SmartPowerServiceInternal$MultiTaskActionListener;

    .line 292
    return-void
.end method


# virtual methods
.method public clearEvents()V
    .locals 1

    .line 300
    const/4 v0, 0x0

    invoke-virtual {p0, v0}, Lcom/miui/server/smartpower/SmartWindowPolicyManager$MultiTaskPolicyController$MultiTaskActionListenerDelegate;->removeCallbacksAndMessages(Ljava/lang/Object;)V

    .line 301
    return-void
.end method

.method public getListener()Lcom/miui/app/smartpower/SmartPowerServiceInternal$MultiTaskActionListener;
    .locals 1

    .line 326
    iget-object v0, p0, Lcom/miui/server/smartpower/SmartWindowPolicyManager$MultiTaskPolicyController$MultiTaskActionListenerDelegate;->mListener:Lcom/miui/app/smartpower/SmartPowerServiceInternal$MultiTaskActionListener;

    return-object v0
.end method

.method public getSourceName()Ljava/lang/String;
    .locals 1

    .line 322
    iget-object v0, p0, Lcom/miui/server/smartpower/SmartWindowPolicyManager$MultiTaskPolicyController$MultiTaskActionListenerDelegate;->mSourceName:Ljava/lang/String;

    return-object v0
.end method

.method public handleMessage(Landroid/os/Message;)V
    .locals 2
    .param p1, "msg"    # Landroid/os/Message;

    .line 305
    iget v0, p1, Landroid/os/Message;->what:I

    packed-switch v0, :pswitch_data_0

    goto :goto_0

    .line 312
    :pswitch_0
    iget-object v0, p1, Landroid/os/Message;->obj:Ljava/lang/Object;

    check-cast v0, Lmiui/smartpower/MultiTaskActionManager$ActionInfo;

    .line 313
    .local v0, "actionInfo":Lmiui/smartpower/MultiTaskActionManager$ActionInfo;
    iget-object v1, p0, Lcom/miui/server/smartpower/SmartWindowPolicyManager$MultiTaskPolicyController$MultiTaskActionListenerDelegate;->mListener:Lcom/miui/app/smartpower/SmartPowerServiceInternal$MultiTaskActionListener;

    invoke-interface {v1, v0}, Lcom/miui/app/smartpower/SmartPowerServiceInternal$MultiTaskActionListener;->onMultiTaskActionEnd(Lmiui/smartpower/MultiTaskActionManager$ActionInfo;)V

    .line 314
    goto :goto_0

    .line 307
    .end local v0    # "actionInfo":Lmiui/smartpower/MultiTaskActionManager$ActionInfo;
    :pswitch_1
    iget-object v0, p1, Landroid/os/Message;->obj:Ljava/lang/Object;

    check-cast v0, Lmiui/smartpower/MultiTaskActionManager$ActionInfo;

    .line 308
    .restart local v0    # "actionInfo":Lmiui/smartpower/MultiTaskActionManager$ActionInfo;
    iget-object v1, p0, Lcom/miui/server/smartpower/SmartWindowPolicyManager$MultiTaskPolicyController$MultiTaskActionListenerDelegate;->mListener:Lcom/miui/app/smartpower/SmartPowerServiceInternal$MultiTaskActionListener;

    invoke-interface {v1, v0}, Lcom/miui/app/smartpower/SmartPowerServiceInternal$MultiTaskActionListener;->onMultiTaskActionStart(Lmiui/smartpower/MultiTaskActionManager$ActionInfo;)V

    .line 309
    nop

    .line 319
    .end local v0    # "actionInfo":Lmiui/smartpower/MultiTaskActionManager$ActionInfo;
    :goto_0
    return-void

    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_1
        :pswitch_0
    .end packed-switch
.end method

.method public sendMultiTaskActionEvent(ILmiui/smartpower/MultiTaskActionManager$ActionInfo;)V
    .locals 1
    .param p1, "event"    # I
    .param p2, "actionInfo"    # Lmiui/smartpower/MultiTaskActionManager$ActionInfo;

    .line 295
    invoke-virtual {p0, p1, p2}, Lcom/miui/server/smartpower/SmartWindowPolicyManager$MultiTaskPolicyController$MultiTaskActionListenerDelegate;->obtainMessage(ILjava/lang/Object;)Landroid/os/Message;

    move-result-object v0

    .line 296
    .local v0, "msg":Landroid/os/Message;
    invoke-virtual {p0, v0}, Lcom/miui/server/smartpower/SmartWindowPolicyManager$MultiTaskPolicyController$MultiTaskActionListenerDelegate;->sendMessage(Landroid/os/Message;)Z

    .line 297
    return-void
.end method
