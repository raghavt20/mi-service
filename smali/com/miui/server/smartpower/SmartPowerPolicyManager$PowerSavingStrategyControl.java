class com.miui.server.smartpower.SmartPowerPolicyManager$PowerSavingStrategyControl {
	 /* .source "SmartPowerPolicyManager.java" */
	 /* # annotations */
	 /* .annotation system Ldalvik/annotation/EnclosingClass; */
	 /* value = Lcom/miui/server/smartpower/SmartPowerPolicyManager; */
} // .end annotation
/* .annotation system Ldalvik/annotation/InnerClass; */
/* accessFlags = 0x2 */
/* name = "PowerSavingStrategyControl" */
} // .end annotation
/* .annotation system Ldalvik/annotation/MemberClasses; */
/* value = { */
/* Lcom/miui/server/smartpower/SmartPowerPolicyManager$PowerSavingStrategyControl$PolicyChangeObserver; */
/* } */
} // .end annotation
/* # static fields */
private static final java.lang.String DB_AUTHORITY;
private static final java.lang.String DB_COL_BG_CONTROL;
private static final java.lang.String DB_COL_PACKAGE_NAME;
private static final java.lang.String DB_COL_USER_ID;
public static final java.lang.String DB_TABLE;
private static final java.lang.String POLICY_NO_RESTRICT;
/* # instance fields */
private final android.net.Uri CONTENT_URI;
private Boolean mIsInit;
private android.util.ArraySet mNoRestrictApps;
/* .annotation system Ldalvik/annotation/Signature; */
/* value = { */
/* "Landroid/util/ArraySet<", */
/* "Ljava/lang/String;", */
/* ">;" */
/* } */
} // .end annotation
} // .end field
final com.miui.server.smartpower.SmartPowerPolicyManager this$0; //synthetic
/* # direct methods */
static Boolean -$$Nest$fgetmIsInit ( com.miui.server.smartpower.SmartPowerPolicyManager$PowerSavingStrategyControl p0 ) { //bridge//synthethic
/* .locals 0 */
/* iget-boolean p0, p0, Lcom/miui/server/smartpower/SmartPowerPolicyManager$PowerSavingStrategyControl;->mIsInit:Z */
} // .end method
static android.util.ArraySet -$$Nest$fgetmNoRestrictApps ( com.miui.server.smartpower.SmartPowerPolicyManager$PowerSavingStrategyControl p0 ) { //bridge//synthethic
/* .locals 0 */
p0 = this.mNoRestrictApps;
} // .end method
static Boolean -$$Nest$misNoRestrictApp ( com.miui.server.smartpower.SmartPowerPolicyManager$PowerSavingStrategyControl p0, java.lang.String p1 ) { //bridge//synthethic
/* .locals 0 */
p0 = /* invoke-direct {p0, p1}, Lcom/miui/server/smartpower/SmartPowerPolicyManager$PowerSavingStrategyControl;->isNoRestrictApp(Ljava/lang/String;)Z */
} // .end method
static void -$$Nest$mupdateAllNoRestrictApps ( com.miui.server.smartpower.SmartPowerPolicyManager$PowerSavingStrategyControl p0 ) { //bridge//synthethic
/* .locals 0 */
/* invoke-direct {p0}, Lcom/miui/server/smartpower/SmartPowerPolicyManager$PowerSavingStrategyControl;->updateAllNoRestrictApps()V */
return;
} // .end method
 com.miui.server.smartpower.SmartPowerPolicyManager$PowerSavingStrategyControl ( ) {
/* .locals 3 */
/* .line 1680 */
this.this$0 = p1;
/* invoke-direct {p0}, Ljava/lang/Object;-><init>()V */
/* .line 1675 */
/* nop */
/* .line 1676 */
final String v0 = "content://com.miui.powerkeeper.configure"; // const-string v0, "content://com.miui.powerkeeper.configure"
android.net.Uri .parse ( v0 );
/* const-string/jumbo v1, "userTable" */
android.net.Uri .withAppendedPath ( v0,v1 );
this.CONTENT_URI = v0;
/* .line 1678 */
/* new-instance v1, Landroid/util/ArraySet; */
/* invoke-direct {v1}, Landroid/util/ArraySet;-><init>()V */
this.mNoRestrictApps = v1;
/* .line 1679 */
int v1 = 0; // const/4 v1, 0x0
/* iput-boolean v1, p0, Lcom/miui/server/smartpower/SmartPowerPolicyManager$PowerSavingStrategyControl;->mIsInit:Z */
/* .line 1682 */
try { // :try_start_0
com.miui.server.smartpower.SmartPowerPolicyManager .-$$Nest$fgetmContext ( p1 );
v2 = android.os.UserHandle.SYSTEM;
(( android.content.Context ) v1 ).getContentResolverForUser ( v2 ); // invoke-virtual {v1, v2}, Landroid/content/Context;->getContentResolverForUser(Landroid/os/UserHandle;)Landroid/content/ContentResolver;
/* .line 1683 */
/* .local v1, "cr":Landroid/content/ContentResolver; */
/* new-instance v2, Lcom/miui/server/smartpower/SmartPowerPolicyManager$PowerSavingStrategyControl$PolicyChangeObserver; */
com.miui.server.smartpower.SmartPowerPolicyManager .-$$Nest$fgetmHandler ( p1 );
/* invoke-direct {v2, p0, p1}, Lcom/miui/server/smartpower/SmartPowerPolicyManager$PowerSavingStrategyControl$PolicyChangeObserver;-><init>(Lcom/miui/server/smartpower/SmartPowerPolicyManager$PowerSavingStrategyControl;Landroid/os/Handler;)V */
int p1 = 1; // const/4 p1, 0x1
(( android.content.ContentResolver ) v1 ).registerContentObserver ( v0, p1, v2 ); // invoke-virtual {v1, v0, p1, v2}, Landroid/content/ContentResolver;->registerContentObserver(Landroid/net/Uri;ZLandroid/database/ContentObserver;)V
/* :try_end_0 */
/* .catch Ljava/lang/Exception; {:try_start_0 ..:try_end_0} :catch_0 */
/* .line 1686 */
} // .end local v1 # "cr":Landroid/content/ContentResolver;
/* .line 1684 */
/* :catch_0 */
/* move-exception p1 */
/* .line 1685 */
/* .local p1, "e":Ljava/lang/Exception; */
final String v0 = "SmartPower.PowerPolicy"; // const-string v0, "SmartPower.PowerPolicy"
(( java.lang.Exception ) p1 ).toString ( ); // invoke-virtual {p1}, Ljava/lang/Exception;->toString()Ljava/lang/String;
android.util.Slog .w ( v0,v1 );
/* .line 1687 */
} // .end local p1 # "e":Ljava/lang/Exception;
} // :goto_0
return;
} // .end method
private Boolean isNoRestrictApp ( java.lang.String p0 ) {
/* .locals 1 */
/* .param p1, "packageName" # Ljava/lang/String; */
/* .line 1729 */
v0 = this.mNoRestrictApps;
v0 = (( android.util.ArraySet ) v0 ).contains ( p1 ); // invoke-virtual {v0, p1}, Landroid/util/ArraySet;->contains(Ljava/lang/Object;)Z
} // .end method
private void updateAllNoRestrictApps ( ) {
/* .locals 10 */
/* .line 1733 */
final String v0 = "pkgName"; // const-string v0, "pkgName"
/* filled-new-array {v0}, [Ljava/lang/String; */
/* .line 1734 */
/* .local v3, "projection":[Ljava/lang/String; */
/* const-string/jumbo v7, "userId = ? AND bgControl = ?" */
/* .line 1735 */
/* .local v7, "selection":Ljava/lang/String; */
/* nop */
/* .line 1736 */
int v8 = 0; // const/4 v8, 0x0
java.lang.Integer .toString ( v8 );
final String v2 = "noRestrict"; // const-string v2, "noRestrict"
/* filled-new-array {v1, v2}, [Ljava/lang/String; */
/* .line 1737 */
/* .local v5, "selectionArgs":[Ljava/lang/String; */
v1 = this.this$0;
com.miui.server.smartpower.SmartPowerPolicyManager .-$$Nest$fgetmContext ( v1 );
v2 = android.os.UserHandle.SYSTEM;
(( android.content.Context ) v1 ).getContentResolverForUser ( v2 ); // invoke-virtual {v1, v2}, Landroid/content/Context;->getContentResolverForUser(Landroid/os/UserHandle;)Landroid/content/ContentResolver;
/* .line 1738 */
/* .local v9, "cr":Landroid/content/ContentResolver; */
v2 = this.CONTENT_URI;
int v6 = 0; // const/4 v6, 0x0
/* move-object v1, v9 */
/* move-object v4, v7 */
/* invoke-virtual/range {v1 ..v6}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor; */
/* .line 1739 */
/* .local v1, "cursor":Landroid/database/Cursor; */
/* if-nez v1, :cond_0 */
/* .line 1740 */
return;
/* .line 1742 */
} // :cond_0
int v2 = 1; // const/4 v2, 0x1
/* iput-boolean v2, p0, Lcom/miui/server/smartpower/SmartPowerPolicyManager$PowerSavingStrategyControl;->mIsInit:Z */
/* .line 1743 */
v2 = this.mNoRestrictApps;
(( android.util.ArraySet ) v2 ).clear ( ); // invoke-virtual {v2}, Landroid/util/ArraySet;->clear()V
/* .line 1745 */
v0 = try { // :try_start_0
/* .line 1746 */
/* .local v0, "bgPkgIndex":I */
v2 = } // :goto_0
if ( v2 != null) { // if-eqz v2, :cond_1
/* .line 1747 */
/* .line 1748 */
/* .local v2, "app":Ljava/lang/String; */
v4 = this.mNoRestrictApps;
(( android.util.ArraySet ) v4 ).add ( v2 ); // invoke-virtual {v4, v2}, Landroid/util/ArraySet;->add(Ljava/lang/Object;)Z
/* :try_end_0 */
/* .catch Ljava/lang/Exception; {:try_start_0 ..:try_end_0} :catch_0 */
/* .catchall {:try_start_0 ..:try_end_0} :catchall_0 */
/* .line 1749 */
/* nop */
} // .end local v2 # "app":Ljava/lang/String;
/* .line 1746 */
} // .end local v0 # "bgPkgIndex":I
} // :cond_1
/* .line 1753 */
/* :catchall_0 */
/* move-exception v0 */
/* .line 1750 */
/* :catch_0 */
/* move-exception v0 */
/* .line 1751 */
/* .local v0, "e":Ljava/lang/Exception; */
try { // :try_start_1
/* iput-boolean v8, p0, Lcom/miui/server/smartpower/SmartPowerPolicyManager$PowerSavingStrategyControl;->mIsInit:Z */
/* :try_end_1 */
/* .catchall {:try_start_1 ..:try_end_1} :catchall_0 */
/* .line 1753 */
} // .end local v0 # "e":Ljava/lang/Exception;
} // :goto_1
/* .line 1754 */
/* nop */
/* .line 1755 */
return;
/* .line 1753 */
} // :goto_2
/* .line 1754 */
/* throw v0 */
} // .end method
