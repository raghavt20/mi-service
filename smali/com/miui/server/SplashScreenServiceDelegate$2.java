class com.miui.server.SplashScreenServiceDelegate$2 implements android.content.ServiceConnection {
	 /* .source "SplashScreenServiceDelegate.java" */
	 /* # interfaces */
	 /* # annotations */
	 /* .annotation system Ldalvik/annotation/EnclosingClass; */
	 /* value = Lcom/miui/server/SplashScreenServiceDelegate; */
} // .end annotation
/* .annotation system Ldalvik/annotation/InnerClass; */
/* accessFlags = 0x0 */
/* name = null */
} // .end annotation
/* # instance fields */
final com.miui.server.SplashScreenServiceDelegate this$0; //synthetic
/* # direct methods */
 com.miui.server.SplashScreenServiceDelegate$2 ( ) {
/* .locals 0 */
/* .param p1, "this$0" # Lcom/miui/server/SplashScreenServiceDelegate; */
/* .line 178 */
this.this$0 = p1;
/* invoke-direct {p0}, Ljava/lang/Object;-><init>()V */
return;
} // .end method
private void asyncSetSplashPackageCheckListener ( ) {
/* .locals 2 */
/* .line 205 */
com.android.internal.os.BackgroundThread .getHandler ( );
/* new-instance v1, Lcom/miui/server/SplashScreenServiceDelegate$2$1; */
/* invoke-direct {v1, p0}, Lcom/miui/server/SplashScreenServiceDelegate$2$1;-><init>(Lcom/miui/server/SplashScreenServiceDelegate$2;)V */
(( android.os.Handler ) v0 ).post ( v1 ); // invoke-virtual {v0, v1}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z
/* .line 219 */
return;
} // .end method
/* # virtual methods */
public void onServiceConnected ( android.content.ComponentName p0, android.os.IBinder p1 ) {
/* .locals 3 */
/* .param p1, "name" # Landroid/content/ComponentName; */
/* .param p2, "service" # Landroid/os/IBinder; */
/* .line 181 */
v0 = this.this$0;
final String v1 = "SplashScreenService connected!"; // const-string v1, "SplashScreenService connected!"
com.miui.server.SplashScreenServiceDelegate .-$$Nest$mlogI ( v0,v1 );
/* .line 182 */
v0 = this.this$0;
com.miui.server.ISplashScreenService$Stub .asInterface ( p2 );
com.miui.server.SplashScreenServiceDelegate .-$$Nest$fputmSplashScreenService ( v0,v1 );
/* .line 183 */
v0 = this.this$0;
java.lang.System .currentTimeMillis ( );
/* move-result-wide v1 */
com.miui.server.SplashScreenServiceDelegate .-$$Nest$fputmStartTime ( v0,v1,v2 );
/* .line 184 */
v0 = this.this$0;
int v1 = 0; // const/4 v1, 0x0
com.miui.server.SplashScreenServiceDelegate .-$$Nest$fputmRebindCount ( v0,v1 );
/* .line 185 */
v0 = this.this$0;
com.miui.server.SplashScreenServiceDelegate .-$$Nest$fgetmHandler ( v0 );
int v2 = 1; // const/4 v2, 0x1
(( android.os.Handler ) v0 ).removeMessages ( v2 ); // invoke-virtual {v0, v2}, Landroid/os/Handler;->removeMessages(I)V
/* .line 187 */
try { // :try_start_0
	 v0 = this.this$0;
	 com.miui.server.SplashScreenServiceDelegate .-$$Nest$fgetmSplashScreenService ( v0 );
	 v2 = this.this$0;
	 com.miui.server.SplashScreenServiceDelegate .-$$Nest$fgetmDeathHandler ( v2 );
	 /* :try_end_0 */
	 /* .catch Ljava/lang/Exception; {:try_start_0 ..:try_end_0} :catch_0 */
	 /* .line 190 */
	 /* .line 188 */
	 /* :catch_0 */
	 /* move-exception v0 */
	 /* .line 189 */
	 /* .local v0, "e":Ljava/lang/Exception; */
	 v1 = this.this$0;
	 final String v2 = "linkToDeath exception"; // const-string v2, "linkToDeath exception"
	 com.miui.server.SplashScreenServiceDelegate .-$$Nest$mlogE ( v1,v2,v0 );
	 /* .line 192 */
} // .end local v0 # "e":Ljava/lang/Exception;
} // :goto_0
/* invoke-direct {p0}, Lcom/miui/server/SplashScreenServiceDelegate$2;->asyncSetSplashPackageCheckListener()V */
/* .line 193 */
return;
} // .end method
public void onServiceDisconnected ( android.content.ComponentName p0 ) {
/* .locals 2 */
/* .param p1, "name" # Landroid/content/ComponentName; */
/* .line 197 */
v0 = this.this$0;
final String v1 = "SplashScreenService disconnected!"; // const-string v1, "SplashScreenService disconnected!"
com.miui.server.SplashScreenServiceDelegate .-$$Nest$mlogI ( v0,v1 );
/* .line 198 */
v0 = this.this$0;
int v1 = 0; // const/4 v1, 0x0
com.miui.server.SplashScreenServiceDelegate .-$$Nest$fputmSplashScreenService ( v0,v1 );
/* .line 199 */
v0 = this.this$0;
com.miui.server.SplashScreenServiceDelegate .-$$Nest$fgetmContext ( v0 );
if ( v0 != null) { // if-eqz v0, :cond_0
/* .line 200 */
v0 = this.this$0;
com.miui.server.SplashScreenServiceDelegate .-$$Nest$fgetmContext ( v0 );
v1 = this.this$0;
com.miui.server.SplashScreenServiceDelegate .-$$Nest$fgetmSplashScreenConnection ( v1 );
(( android.content.Context ) v0 ).unbindService ( v1 ); // invoke-virtual {v0, v1}, Landroid/content/Context;->unbindService(Landroid/content/ServiceConnection;)V
/* .line 202 */
} // :cond_0
return;
} // .end method
