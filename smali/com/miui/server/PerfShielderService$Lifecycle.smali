.class public final Lcom/miui/server/PerfShielderService$Lifecycle;
.super Lcom/android/server/SystemService;
.source "PerfShielderService.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/miui/server/PerfShielderService;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "Lifecycle"
.end annotation


# instance fields
.field private final mService:Lcom/miui/server/PerfShielderService;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 1
    .param p1, "context"    # Landroid/content/Context;

    .line 161
    invoke-direct {p0, p1}, Lcom/android/server/SystemService;-><init>(Landroid/content/Context;)V

    .line 162
    new-instance v0, Lcom/miui/server/PerfShielderService;

    invoke-direct {v0, p1}, Lcom/miui/server/PerfShielderService;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Lcom/miui/server/PerfShielderService$Lifecycle;->mService:Lcom/miui/server/PerfShielderService;

    .line 163
    return-void
.end method


# virtual methods
.method public onBootPhase(I)V
    .locals 1
    .param p1, "phase"    # I

    .line 172
    const/16 v0, 0x3e8

    if-ne p1, v0, :cond_0

    .line 173
    const-class v0, Lcom/miui/app/SpeedTestModeServiceInternal;

    .line 174
    invoke-static {v0}, Lcom/android/server/LocalServices;->getService(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/miui/app/SpeedTestModeServiceInternal;

    .line 175
    .local v0, "speedTestModeService":Lcom/miui/app/SpeedTestModeServiceInternal;
    if-eqz v0, :cond_0

    .line 176
    invoke-interface {v0}, Lcom/miui/app/SpeedTestModeServiceInternal;->onBootPhase()V

    .line 179
    .end local v0    # "speedTestModeService":Lcom/miui/app/SpeedTestModeServiceInternal;
    :cond_0
    return-void
.end method

.method public onStart()V
    .locals 2

    .line 167
    const-string v0, "perfshielder"

    iget-object v1, p0, Lcom/miui/server/PerfShielderService$Lifecycle;->mService:Lcom/miui/server/PerfShielderService;

    invoke-virtual {p0, v0, v1}, Lcom/miui/server/PerfShielderService$Lifecycle;->publishBinderService(Ljava/lang/String;Landroid/os/IBinder;)V

    .line 168
    return-void
.end method
