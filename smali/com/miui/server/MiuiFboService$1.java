class com.miui.server.MiuiFboService$1 implements java.lang.Runnable {
	 /* .source "MiuiFboService.java" */
	 /* # interfaces */
	 /* # annotations */
	 /* .annotation system Ldalvik/annotation/EnclosingMethod; */
	 /* value = Lcom/miui/server/MiuiFboService;->initFboSocket()V */
} // .end annotation
/* .annotation system Ldalvik/annotation/InnerClass; */
/* accessFlags = 0x0 */
/* name = null */
} // .end annotation
/* # direct methods */
 com.miui.server.MiuiFboService$1 ( ) {
/* .locals 0 */
/* .line 387 */
/* invoke-direct {p0}, Ljava/lang/Object;-><init>()V */
return;
} // .end method
/* # virtual methods */
public void run ( ) {
/* .locals 13 */
/* .line 391 */
int v0 = 0; // const/4 v0, 0x0
try { // :try_start_0
	 /* new-instance v1, Landroid/net/LocalServerSocket; */
	 final String v2 = "fbs_native_socket"; // const-string v2, "fbs_native_socket"
	 /* invoke-direct {v1, v2}, Landroid/net/LocalServerSocket;-><init>(Ljava/lang/String;)V */
	 com.miui.server.MiuiFboService .-$$Nest$sfputmServerSocket ( v1 );
	 /* :try_end_0 */
	 /* .catch Ljava/io/IOException; {:try_start_0 ..:try_end_0} :catch_0 */
	 /* .line 395 */
	 /* .line 392 */
	 /* :catch_0 */
	 /* move-exception v1 */
	 /* .line 393 */
	 /* .local v1, "e":Ljava/io/IOException; */
	 (( java.io.IOException ) v1 ).printStackTrace ( ); // invoke-virtual {v1}, Ljava/io/IOException;->printStackTrace()V
	 /* .line 394 */
	 com.miui.server.MiuiFboService .-$$Nest$sfputmKeepRunning ( v0 );
	 /* .line 396 */
} // .end local v1 # "e":Ljava/io/IOException;
} // :goto_0
v1 = com.miui.server.MiuiFboService .-$$Nest$sfgetmKeepRunning ( );
if ( v1 != null) { // if-eqz v1, :cond_4
/* .line 398 */
try { // :try_start_1
	 com.miui.server.MiuiFboService .-$$Nest$sfgetmServerSocket ( );
	 (( android.net.LocalServerSocket ) v1 ).accept ( ); // invoke-virtual {v1}, Landroid/net/LocalServerSocket;->accept()Landroid/net/LocalSocket;
	 com.miui.server.MiuiFboService .-$$Nest$sfputinteractClientSocket ( v1 );
	 /* .line 399 */
	 com.miui.server.MiuiFboService .-$$Nest$sfgetinteractClientSocket ( );
	 (( android.net.LocalSocket ) v1 ).getInputStream ( ); // invoke-virtual {v1}, Landroid/net/LocalSocket;->getInputStream()Ljava/io/InputStream;
	 com.miui.server.MiuiFboService .-$$Nest$sfputinputStream ( v1 );
	 /* .line 400 */
	 com.miui.server.MiuiFboService .-$$Nest$sfgetinteractClientSocket ( );
	 (( android.net.LocalSocket ) v1 ).getOutputStream ( ); // invoke-virtual {v1}, Landroid/net/LocalSocket;->getOutputStream()Ljava/io/OutputStream;
	 com.miui.server.MiuiFboService .-$$Nest$sfputoutputStream ( v1 );
	 /* .line 401 */
	 /* const v1, 0x19000 */
	 /* new-array v1, v1, [B */
	 /* .line 402 */
	 /* .local v1, "bytes":[B */
	 com.miui.server.MiuiFboService .-$$Nest$sfgetinputStream ( );
	 (( java.io.InputStream ) v2 ).read ( v1 ); // invoke-virtual {v2, v1}, Ljava/io/InputStream;->read([B)I
	 /* .line 403 */
	 /* new-instance v2, Ljava/lang/String; */
	 /* invoke-direct {v2, v1}, Ljava/lang/String;-><init>([B)V */
	 /* .line 404 */
	 /* .local v2, "dataReceived":Ljava/lang/String; */
	 /* const/16 v3, 0x7d */
	 v3 = 	 (( java.lang.String ) v2 ).indexOf ( v3 ); // invoke-virtual {v2, v3}, Ljava/lang/String;->indexOf(I)I
	 (( java.lang.String ) v2 ).substring ( v0, v3 ); // invoke-virtual {v2, v0, v3}, Ljava/lang/String;->substring(II)Ljava/lang/String;
	 /* move-object v2, v3 */
	 /* .line 405 */
	 com.miui.server.MiuiFboService .-$$Nest$sfgetTAG ( );
	 /* new-instance v4, Ljava/lang/StringBuilder; */
	 /* invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V */
	 final String v5 = "Receive data from native:"; // const-string v5, "Receive data from native:"
	 (( java.lang.StringBuilder ) v4 ).append ( v5 ); // invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
	 (( java.lang.StringBuilder ) v4 ).append ( v2 ); // invoke-virtual {v4, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
	 (( java.lang.StringBuilder ) v4 ).toString ( ); // invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
	 android.util.Slog .d ( v3,v4 );
	 /* .line 406 */
	 final String v3 = "pkg:"; // const-string v3, "pkg:"
	 v3 = 	 (( java.lang.String ) v2 ).contains ( v3 ); // invoke-virtual {v2, v3}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z
	 int v4 = 1; // const/4 v4, 0x1
	 if ( v3 != null) { // if-eqz v3, :cond_1
		 /* .line 407 */
		 com.miui.server.MiuiFboService .-$$Nest$sfputmFinishedApp ( v4 );
		 /* .line 408 */
		 com.miui.server.MiuiFboService .-$$Nest$sfgetsInstance ( );
		 com.miui.server.MiuiFboService .-$$Nest$fputcldStrategyStatus ( v3,v4 );
		 /* .line 409 */
		 com.miui.server.MiuiFboService .-$$Nest$smuseCldStrategy ( v4 );
		 /* .line 410 */
		 com.miui.server.MiuiFboService .-$$Nest$sfgetsInstance ( );
		 com.miui.server.MiuiFboService .-$$Nest$fputcldStrategyStatus ( v3,v0 );
		 /* .line 411 */
		 com.miui.server.MiuiFboService .-$$Nest$smaggregateBroadcastData ( v2 );
		 /* .line 412 */
		 com.miui.server.MiuiFboService .-$$Nest$sfgetsInstance ( );
		 com.miui.server.MiuiFboService .-$$Nest$sfgetpackageNameList ( );
		 (( java.util.ArrayList ) v5 ).toString ( ); // invoke-virtual {v5}, Ljava/util/ArrayList;->toString()Ljava/lang/String;
		 /* const-wide/16 v6, 0x0 */
		 (( com.miui.server.MiuiFboService ) v3 ).deliverMessage ( v5, v4, v6, v7 ); // invoke-virtual {v3, v5, v4, v6, v7}, Lcom/miui/server/MiuiFboService;->deliverMessage(Ljava/lang/String;IJ)V
		 /* .line 413 */
		 v3 = 		 com.miui.server.MiuiFboService .-$$Nest$sfgetlistSize ( );
		 /* if-gez v3, :cond_0 */
		 /* .line 414 */
		 com.miui.server.MiuiFboService .-$$Nest$sfgetsInstance ( );
		 /* const-string/jumbo v4, "stop" */
		 int v5 = 3; // const/4 v5, 0x3
		 (( com.miui.server.MiuiFboService ) v3 ).deliverMessage ( v4, v5, v6, v7 ); // invoke-virtual {v3, v4, v5, v6, v7}, Lcom/miui/server/MiuiFboService;->deliverMessage(Ljava/lang/String;IJ)V
		 /* :try_end_1 */
		 /* .catch Ljava/lang/Exception; {:try_start_1 ..:try_end_1} :catch_2 */
		 /* .catchall {:try_start_1 ..:try_end_1} :catchall_0 */
		 /* .line 434 */
	 } // :cond_0
	 try { // :try_start_2
		 com.miui.server.MiuiFboService .-$$Nest$sfgetinputStream ( );
		 (( java.io.InputStream ) v3 ).close ( ); // invoke-virtual {v3}, Ljava/io/InputStream;->close()V
		 /* .line 435 */
		 com.miui.server.MiuiFboService .-$$Nest$sfgetoutputStream ( );
		 (( java.io.OutputStream ) v3 ).close ( ); // invoke-virtual {v3}, Ljava/io/OutputStream;->close()V
		 /* :try_end_2 */
		 /* .catch Ljava/io/IOException; {:try_start_2 ..:try_end_2} :catch_1 */
		 /* .line 438 */
		 /* goto/16 :goto_0 */
		 /* .line 436 */
		 /* :catch_1 */
		 /* move-exception v3 */
		 /* .line 437 */
		 /* .local v3, "e":Ljava/io/IOException; */
		 (( java.io.IOException ) v3 ).printStackTrace ( ); // invoke-virtual {v3}, Ljava/io/IOException;->printStackTrace()V
		 /* .line 416 */
	 } // .end local v3 # "e":Ljava/io/IOException;
	 /* goto/16 :goto_0 */
	 /* .line 418 */
} // :cond_1
try { // :try_start_3
	 com.miui.server.MiuiFboService .-$$Nest$sfgetsInstance ( );
	 v3 = 	 com.miui.server.MiuiFboService .-$$Nest$fgetmDriverSupport ( v3 );
	 if ( v3 != null) { // if-eqz v3, :cond_3
		 /* .line 419 */
		 int v3 = 2; // const/4 v3, 0x2
		 com.miui.server.MiuiFboService .-$$Nest$smcallHalFunction ( v2,v3 );
		 /* check-cast v5, Ljava/lang/String; */
		 /* .line 420 */
		 /* .local v5, "halReturnData":Ljava/lang/String; */
		 int v6 = 0; // const/4 v6, 0x0
		 /* .line 421 */
		 /* .local v6, "split":[Ljava/lang/String; */
		 if ( v5 != null) { // if-eqz v5, :cond_2
			 /* .line 422 */
			 final String v7 = ":"; // const-string v7, ":"
			 (( java.lang.String ) v5 ).split ( v7 ); // invoke-virtual {v5, v7}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;
			 /* move-object v6, v7 */
			 /* .line 424 */
		 } // :cond_2
		 /* array-length v7, v6 */
		 /* sub-int/2addr v7, v3 */
		 /* aget-object v3, v6, v7 */
		 java.lang.Long .parseLong ( v3 );
		 /* move-result-wide v7 */
		 java.lang.Long .valueOf ( v7,v8 );
		 /* .line 425 */
		 /* .local v3, "beforeCleanup":Ljava/lang/Long; */
		 /* array-length v7, v6 */
		 /* sub-int/2addr v7, v4 */
		 /* aget-object v4, v6, v7 */
		 java.lang.Long .parseLong ( v4 );
		 /* move-result-wide v7 */
		 java.lang.Long .valueOf ( v7,v8 );
		 /* .line 426 */
		 /* .local v4, "afterCleanup":Ljava/lang/Long; */
		 com.miui.server.MiuiFboService .-$$Nest$sfgetmFragmentCount ( );
		 (( java.lang.Long ) v7 ).longValue ( ); // invoke-virtual {v7}, Ljava/lang/Long;->longValue()J
		 /* move-result-wide v7 */
		 (( java.lang.Long ) v3 ).longValue ( ); // invoke-virtual {v3}, Ljava/lang/Long;->longValue()J
		 /* move-result-wide v9 */
		 (( java.lang.Long ) v4 ).longValue ( ); // invoke-virtual {v4}, Ljava/lang/Long;->longValue()J
		 /* move-result-wide v11 */
		 /* sub-long/2addr v9, v11 */
		 /* add-long/2addr v7, v9 */
		 java.lang.Long .valueOf ( v7,v8 );
		 com.miui.server.MiuiFboService .-$$Nest$sfputmFragmentCount ( v7 );
		 /* .line 428 */
	 } // .end local v3 # "beforeCleanup":Ljava/lang/Long;
} // .end local v4 # "afterCleanup":Ljava/lang/Long;
} // .end local v5 # "halReturnData":Ljava/lang/String;
} // .end local v6 # "split":[Ljava/lang/String;
} // :cond_3
com.miui.server.MiuiFboService .-$$Nest$sfgetoutputStream ( );
/* const-string/jumbo v4, "{send message to native}" */
(( java.lang.String ) v4 ).getBytes ( ); // invoke-virtual {v4}, Ljava/lang/String;->getBytes()[B
(( java.io.OutputStream ) v3 ).write ( v4 ); // invoke-virtual {v3, v4}, Ljava/io/OutputStream;->write([B)V
/* :try_end_3 */
/* .catch Ljava/lang/Exception; {:try_start_3 ..:try_end_3} :catch_2 */
/* .catchall {:try_start_3 ..:try_end_3} :catchall_0 */
/* .line 434 */
} // .end local v1 # "bytes":[B
} // .end local v2 # "dataReceived":Ljava/lang/String;
try { // :try_start_4
com.miui.server.MiuiFboService .-$$Nest$sfgetinputStream ( );
(( java.io.InputStream ) v1 ).close ( ); // invoke-virtual {v1}, Ljava/io/InputStream;->close()V
/* .line 435 */
com.miui.server.MiuiFboService .-$$Nest$sfgetoutputStream ( );
(( java.io.OutputStream ) v1 ).close ( ); // invoke-virtual {v1}, Ljava/io/OutputStream;->close()V
/* :try_end_4 */
/* .catch Ljava/io/IOException; {:try_start_4 ..:try_end_4} :catch_3 */
/* .line 433 */
/* :catchall_0 */
/* move-exception v0 */
/* .line 429 */
/* :catch_2 */
/* move-exception v1 */
/* .line 430 */
/* .local v1, "e":Ljava/lang/Exception; */
try { // :try_start_5
(( java.lang.Exception ) v1 ).printStackTrace ( ); // invoke-virtual {v1}, Ljava/lang/Exception;->printStackTrace()V
/* .line 431 */
com.miui.server.MiuiFboService .-$$Nest$smwriteFailToHal ( );
/* :try_end_5 */
/* .catchall {:try_start_5 ..:try_end_5} :catchall_0 */
/* .line 434 */
} // .end local v1 # "e":Ljava/lang/Exception;
try { // :try_start_6
com.miui.server.MiuiFboService .-$$Nest$sfgetinputStream ( );
(( java.io.InputStream ) v1 ).close ( ); // invoke-virtual {v1}, Ljava/io/InputStream;->close()V
/* .line 435 */
com.miui.server.MiuiFboService .-$$Nest$sfgetoutputStream ( );
(( java.io.OutputStream ) v1 ).close ( ); // invoke-virtual {v1}, Ljava/io/OutputStream;->close()V
/* :try_end_6 */
/* .catch Ljava/io/IOException; {:try_start_6 ..:try_end_6} :catch_3 */
/* .line 438 */
} // :goto_1
/* .line 436 */
/* :catch_3 */
/* move-exception v1 */
/* .line 437 */
/* .local v1, "e":Ljava/io/IOException; */
(( java.io.IOException ) v1 ).printStackTrace ( ); // invoke-virtual {v1}, Ljava/io/IOException;->printStackTrace()V
/* .line 439 */
} // .end local v1 # "e":Ljava/io/IOException;
/* nop */
} // :goto_2
/* goto/16 :goto_0 */
/* .line 434 */
} // :goto_3
try { // :try_start_7
com.miui.server.MiuiFboService .-$$Nest$sfgetinputStream ( );
(( java.io.InputStream ) v1 ).close ( ); // invoke-virtual {v1}, Ljava/io/InputStream;->close()V
/* .line 435 */
com.miui.server.MiuiFboService .-$$Nest$sfgetoutputStream ( );
(( java.io.OutputStream ) v1 ).close ( ); // invoke-virtual {v1}, Ljava/io/OutputStream;->close()V
/* :try_end_7 */
/* .catch Ljava/io/IOException; {:try_start_7 ..:try_end_7} :catch_4 */
/* .line 438 */
/* .line 436 */
/* :catch_4 */
/* move-exception v1 */
/* .line 437 */
/* .restart local v1 # "e":Ljava/io/IOException; */
(( java.io.IOException ) v1 ).printStackTrace ( ); // invoke-virtual {v1}, Ljava/io/IOException;->printStackTrace()V
/* .line 439 */
} // .end local v1 # "e":Ljava/io/IOException;
} // :goto_4
/* throw v0 */
/* .line 441 */
} // :cond_4
return;
} // .end method
