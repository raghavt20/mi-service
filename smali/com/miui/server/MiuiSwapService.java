public class com.miui.server.MiuiSwapService extends miui.swap.ISwapManager$Stub {
	 /* .source "MiuiSwapService.java" */
	 /* # annotations */
	 /* .annotation system Ldalvik/annotation/MemberClasses; */
	 /* value = { */
	 /* Lcom/miui/server/MiuiSwapService$SwapServiceHandler;, */
	 /* Lcom/miui/server/MiuiSwapService$UsbStatusReceiver;, */
	 /* Lcom/miui/server/MiuiSwapService$Lifecycle; */
	 /* } */
} // .end annotation
/* # static fields */
public static final java.lang.String ACTION_TEST_USB_STATE;
private static final Integer EXECUTE_SWAP_DISABLE;
private static final Integer EXECUTE_SWAP_trigger;
private static final java.lang.String HAL_DEFAULT;
private static final java.lang.String HAL_INTERFACE_DESCRIPTOR;
private static final java.lang.String HAL_SERVICE_NAME;
public static final Boolean IS_SWAP_ENABLE;
private static final Integer IS_SWAP_SUPPORTED;
private static final java.lang.String NATIVE_SERVICE_KEY;
private static final java.lang.String NATIVE_SERVICE_NAME;
public static final java.lang.String SERVICE_NAME;
private static final Integer START_SWAP;
public static final Boolean SWAP_DEBUG;
private static final java.lang.String TAG;
private static final Integer WHETHER_START;
private static volatile com.miui.server.MiuiSwapService$SwapServiceHandler mSwapServiceHandler;
/* # instance fields */
private android.content.Context mContext;
private volatile miui.swap.ISwap mSwapNativeService;
private android.os.HandlerThread mSwapServiceThread;
private Boolean mUsbConnect;
/* # direct methods */
static Boolean -$$Nest$fgetmUsbConnect ( com.miui.server.MiuiSwapService p0 ) { //bridge//synthethic
	 /* .locals 0 */
	 /* iget-boolean p0, p0, Lcom/miui/server/MiuiSwapService;->mUsbConnect:Z */
} // .end method
static void -$$Nest$fputmSwapNativeService ( com.miui.server.MiuiSwapService p0, miui.swap.ISwap p1 ) { //bridge//synthethic
	 /* .locals 0 */
	 this.mSwapNativeService = p1;
	 return;
} // .end method
static void -$$Nest$fputmUsbConnect ( com.miui.server.MiuiSwapService p0, Boolean p1 ) { //bridge//synthethic
	 /* .locals 0 */
	 /* iput-boolean p1, p0, Lcom/miui/server/MiuiSwapService;->mUsbConnect:Z */
	 return;
} // .end method
static void -$$Nest$mstartSwapLocked ( com.miui.server.MiuiSwapService p0 ) { //bridge//synthethic
	 /* .locals 0 */
	 /* invoke-direct {p0}, Lcom/miui/server/MiuiSwapService;->startSwapLocked()V */
	 return;
} // .end method
static java.lang.String -$$Nest$sfgetTAG ( ) { //bridge//synthethic
	 /* .locals 1 */
	 v0 = com.miui.server.MiuiSwapService.TAG;
} // .end method
static com.miui.server.MiuiSwapService ( ) {
	 /* .locals 2 */
	 /* .line 29 */
	 /* const-class v0, Lcom/miui/server/MiuiSwapService; */
	 (( java.lang.Class ) v0 ).getSimpleName ( ); // invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;
	 /* .line 39 */
	 final String v0 = "persist.sys.miui_swap_debug"; // const-string v0, "persist.sys.miui_swap_debug"
	 int v1 = 0; // const/4 v1, 0x0
	 v0 = 	 android.os.SystemProperties .getBoolean ( v0,v1 );
	 com.miui.server.MiuiSwapService.SWAP_DEBUG = (v0!= 0);
	 /* .line 40 */
	 final String v0 = "persist.sys.stability.swapEnable"; // const-string v0, "persist.sys.stability.swapEnable"
	 v0 = 	 android.os.SystemProperties .getBoolean ( v0,v1 );
	 com.miui.server.MiuiSwapService.IS_SWAP_ENABLE = (v0!= 0);
	 return;
} // .end method
public com.miui.server.MiuiSwapService ( ) {
	 /* .locals 4 */
	 /* .param p1, "context" # Landroid/content/Context; */
	 /* .line 97 */
	 /* invoke-direct {p0}, Lmiui/swap/ISwapManager$Stub;-><init>()V */
	 /* .line 48 */
	 int v0 = 0; // const/4 v0, 0x0
	 /* iput-boolean v0, p0, Lcom/miui/server/MiuiSwapService;->mUsbConnect:Z */
	 /* .line 98 */
	 this.mContext = p1;
	 /* .line 99 */
	 (( com.miui.server.MiuiSwapService ) p0 ).getUsbStatus ( ); // invoke-virtual {p0}, Lcom/miui/server/MiuiSwapService;->getUsbStatus()V
	 /* .line 100 */
	 /* new-instance v0, Landroid/os/HandlerThread; */
	 /* const-string/jumbo v1, "swapServiceWork" */
	 /* invoke-direct {v0, v1}, Landroid/os/HandlerThread;-><init>(Ljava/lang/String;)V */
	 this.mSwapServiceThread = v0;
	 /* .line 101 */
	 (( android.os.HandlerThread ) v0 ).start ( ); // invoke-virtual {v0}, Landroid/os/HandlerThread;->start()V
	 /* .line 102 */
	 /* new-instance v0, Lcom/miui/server/MiuiSwapService$SwapServiceHandler; */
	 v1 = this.mSwapServiceThread;
	 (( android.os.HandlerThread ) v1 ).getLooper ( ); // invoke-virtual {v1}, Landroid/os/HandlerThread;->getLooper()Landroid/os/Looper;
	 /* invoke-direct {v0, p0, v1}, Lcom/miui/server/MiuiSwapService$SwapServiceHandler;-><init>(Lcom/miui/server/MiuiSwapService;Landroid/os/Looper;)V */
	 /* .line 104 */
	 v0 = com.miui.server.MiuiSwapService.mSwapServiceHandler;
	 int v1 = 2; // const/4 v1, 0x2
	 /* const-wide/16 v2, 0x1388 */
	 (( com.miui.server.MiuiSwapService$SwapServiceHandler ) v0 ).sendEmptyMessageDelayed ( v1, v2, v3 ); // invoke-virtual {v0, v1, v2, v3}, Lcom/miui/server/MiuiSwapService$SwapServiceHandler;->sendEmptyMessageDelayed(IJ)Z
	 /* .line 106 */
	 v0 = com.miui.server.MiuiSwapService.mSwapServiceHandler;
	 int v1 = 1; // const/4 v1, 0x1
	 /* const-wide/16 v2, 0x2710 */
	 (( com.miui.server.MiuiSwapService$SwapServiceHandler ) v0 ).sendEmptyMessageDelayed ( v1, v2, v3 ); // invoke-virtual {v0, v1, v2, v3}, Lcom/miui/server/MiuiSwapService$SwapServiceHandler;->sendEmptyMessageDelayed(IJ)Z
	 /* .line 107 */
	 return;
} // .end method
private void callHalSwap ( java.lang.String p0 ) {
	 /* .locals 7 */
	 /* .param p1, "nativeResult" # Ljava/lang/String; */
	 /* .line 188 */
	 /* const-string/jumbo v0, "vendor.xiaomi.hardware.swap@1.0::ISwap" */
	 /* new-instance v1, Landroid/os/HwParcel; */
	 /* invoke-direct {v1}, Landroid/os/HwParcel;-><init>()V */
	 /* .line 190 */
	 /* .local v1, "hidl_reply":Landroid/os/HwParcel; */
	 try { // :try_start_0
		 final String v2 = "default"; // const-string v2, "default"
		 android.os.HwBinder .getService ( v0,v2 );
		 /* .line 191 */
		 /* .local v2, "hwService":Landroid/os/IHwBinder; */
		 if ( v2 != null) { // if-eqz v2, :cond_1
			 /* .line 192 */
			 v3 = 			 /* invoke-direct {p0}, Lcom/miui/server/MiuiSwapService;->isSWAPSupport()Z */
			 if ( v3 != null) { // if-eqz v3, :cond_0
				 /* .line 193 */
				 /* new-instance v3, Landroid/os/HwParcel; */
				 /* invoke-direct {v3}, Landroid/os/HwParcel;-><init>()V */
				 /* .line 194 */
				 /* .local v3, "hidl_request":Landroid/os/HwParcel; */
				 (( android.os.HwParcel ) v3 ).writeInterfaceToken ( v0 ); // invoke-virtual {v3, v0}, Landroid/os/HwParcel;->writeInterfaceToken(Ljava/lang/String;)V
				 /* .line 195 */
				 (( android.os.HwParcel ) v3 ).writeString ( p1 ); // invoke-virtual {v3, p1}, Landroid/os/HwParcel;->writeString(Ljava/lang/String;)V
				 /* .line 196 */
				 int v0 = 2; // const/4 v0, 0x2
				 int v4 = 0; // const/4 v4, 0x0
				 /* .line 197 */
				 (( android.os.HwParcel ) v1 ).verifySuccess ( ); // invoke-virtual {v1}, Landroid/os/HwParcel;->verifySuccess()V
				 /* .line 198 */
				 (( android.os.HwParcel ) v3 ).releaseTemporaryStorage ( ); // invoke-virtual {v3}, Landroid/os/HwParcel;->releaseTemporaryStorage()V
				 /* .line 199 */
				 (( android.os.HwParcel ) v1 ).readString ( ); // invoke-virtual {v1}, Landroid/os/HwParcel;->readString()Ljava/lang/String;
				 /* .line 200 */
				 /* .local v0, "HalReturnResult":Ljava/lang/String; */
				 v4 = com.miui.server.MiuiSwapService.TAG;
				 /* new-instance v5, Ljava/lang/StringBuilder; */
				 /* invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V */
				 final String v6 = "callHalSwap return result is :"; // const-string v6, "callHalSwap return result is :"
				 (( java.lang.StringBuilder ) v5 ).append ( v6 ); // invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
				 (( java.lang.StringBuilder ) v5 ).append ( v0 ); // invoke-virtual {v5, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
				 (( java.lang.StringBuilder ) v5 ).toString ( ); // invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
				 android.util.Slog .d ( v4,v5 );
				 /* .line 201 */
				 /* nop */
			 } // .end local v0 # "HalReturnResult":Ljava/lang/String;
		 } // .end local v3 # "hidl_request":Landroid/os/HwParcel;
		 /* .line 202 */
	 } // :cond_0
	 v0 = com.miui.server.MiuiSwapService.TAG;
	 final String v3 = "This device does not support swap on hal"; // const-string v3, "This device does not support swap on hal"
	 android.util.Slog .d ( v0,v3 );
	 /* :try_end_0 */
	 /* .catch Ljava/lang/Exception; {:try_start_0 ..:try_end_0} :catch_0 */
	 /* .catchall {:try_start_0 ..:try_end_0} :catchall_0 */
	 /* .line 208 */
} // .end local v2 # "hwService":Landroid/os/IHwBinder;
} // :cond_1
} // :goto_0
/* nop */
} // :goto_1
(( android.os.HwParcel ) v1 ).release ( ); // invoke-virtual {v1}, Landroid/os/HwParcel;->release()V
/* .line 209 */
/* .line 208 */
/* :catchall_0 */
/* move-exception v0 */
/* .line 205 */
/* :catch_0 */
/* move-exception v0 */
/* .line 206 */
/* .local v0, "e":Ljava/lang/Exception; */
try { // :try_start_1
v2 = com.miui.server.MiuiSwapService.TAG;
/* new-instance v3, Ljava/lang/StringBuilder; */
/* invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V */
final String v4 = "fail to call SwapHalService"; // const-string v4, "fail to call SwapHalService"
(( java.lang.StringBuilder ) v3 ).append ( v4 ); // invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v3 ).append ( v0 ); // invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v3 ).toString ( ); // invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
android.util.Slog .e ( v2,v3 );
/* :try_end_1 */
/* .catchall {:try_start_1 ..:try_end_1} :catchall_0 */
/* .line 208 */
/* nop */
} // .end local v0 # "e":Ljava/lang/Exception;
/* .line 210 */
} // :goto_2
return;
/* .line 208 */
} // :goto_3
(( android.os.HwParcel ) v1 ).release ( ); // invoke-virtual {v1}, Landroid/os/HwParcel;->release()V
/* .line 209 */
/* throw v0 */
} // .end method
private miui.swap.ISwap getNativeService ( ) {
/* .locals 5 */
/* .line 153 */
int v0 = 0; // const/4 v0, 0x0
/* .line 155 */
/* .local v0, "Ibinder":Landroid/os/IBinder; */
try { // :try_start_0
final String v1 = "persist.sys.swapservice.ctrl"; // const-string v1, "persist.sys.swapservice.ctrl"
int v2 = 0; // const/4 v2, 0x0
v1 = android.os.SystemProperties .getBoolean ( v1,v2 );
java.lang.Boolean .valueOf ( v1 );
/* .line 156 */
/* .local v1, "isStartService":Ljava/lang/Boolean; */
v3 = (( java.lang.Boolean ) v1 ).booleanValue ( ); // invoke-virtual {v1}, Ljava/lang/Boolean;->booleanValue()Z
if ( v3 != null) { // if-eqz v3, :cond_0
/* .line 157 */
final String v3 = "SwapNativeService"; // const-string v3, "SwapNativeService"
android.os.ServiceManager .getService ( v3 );
/* move-object v0, v3 */
/* .line 158 */
if ( v0 != null) { // if-eqz v0, :cond_0
/* .line 159 */
/* new-instance v3, Lcom/miui/server/MiuiSwapService$1; */
/* invoke-direct {v3, p0}, Lcom/miui/server/MiuiSwapService$1;-><init>(Lcom/miui/server/MiuiSwapService;)V */
/* :try_end_0 */
/* .catch Ljava/lang/Exception; {:try_start_0 ..:try_end_0} :catch_0 */
/* .line 170 */
} // .end local v1 # "isStartService":Ljava/lang/Boolean;
} // :cond_0
/* .line 168 */
/* :catch_0 */
/* move-exception v1 */
/* .line 169 */
/* .local v1, "e":Ljava/lang/Exception; */
v2 = com.miui.server.MiuiSwapService.TAG;
/* new-instance v3, Ljava/lang/StringBuilder; */
/* invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V */
final String v4 = "fail to get SwapNativeService:"; // const-string v4, "fail to get SwapNativeService:"
(( java.lang.StringBuilder ) v3 ).append ( v4 ); // invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v3 ).append ( v1 ); // invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v3 ).toString ( ); // invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
android.util.Slog .e ( v2,v3 );
/* .line 171 */
} // .end local v1 # "e":Ljava/lang/Exception;
} // :goto_0
if ( v0 != null) { // if-eqz v0, :cond_1
/* .line 172 */
miui.swap.ISwap$Stub .asInterface ( v0 );
this.mSwapNativeService = v1;
/* .line 173 */
v1 = com.miui.server.MiuiSwapService.TAG;
final String v2 = "get ISwap"; // const-string v2, "get ISwap"
android.util.Slog .w ( v1,v2 );
/* .line 175 */
} // :cond_1
int v1 = 0; // const/4 v1, 0x0
this.mSwapNativeService = v1;
/* .line 176 */
v1 = com.miui.server.MiuiSwapService.TAG;
final String v2 = "ISwap get failed, please try again"; // const-string v2, "ISwap get failed, please try again"
android.util.Slog .w ( v1,v2 );
/* .line 178 */
} // :goto_1
v1 = this.mSwapNativeService;
} // .end method
private Boolean isSWAPSupport ( ) {
/* .locals 6 */
/* .line 240 */
/* const-string/jumbo v0, "vendor.xiaomi.hardware.swap@1.0::ISwap" */
/* new-instance v1, Landroid/os/HwParcel; */
/* invoke-direct {v1}, Landroid/os/HwParcel;-><init>()V */
/* .line 242 */
/* .local v1, "hidl_reply":Landroid/os/HwParcel; */
int v2 = 0; // const/4 v2, 0x0
try { // :try_start_0
final String v3 = "default"; // const-string v3, "default"
android.os.HwBinder .getService ( v0,v3 );
/* .line 243 */
/* .local v3, "hwService":Landroid/os/IHwBinder; */
if ( v3 != null) { // if-eqz v3, :cond_0
/* .line 244 */
/* new-instance v4, Landroid/os/HwParcel; */
/* invoke-direct {v4}, Landroid/os/HwParcel;-><init>()V */
/* .line 245 */
/* .local v4, "hidl_request":Landroid/os/HwParcel; */
(( android.os.HwParcel ) v4 ).writeInterfaceToken ( v0 ); // invoke-virtual {v4, v0}, Landroid/os/HwParcel;->writeInterfaceToken(Ljava/lang/String;)V
/* .line 246 */
int v0 = 1; // const/4 v0, 0x1
/* .line 247 */
(( android.os.HwParcel ) v1 ).verifySuccess ( ); // invoke-virtual {v1}, Landroid/os/HwParcel;->verifySuccess()V
/* .line 248 */
(( android.os.HwParcel ) v4 ).releaseTemporaryStorage ( ); // invoke-virtual {v4}, Landroid/os/HwParcel;->releaseTemporaryStorage()V
/* .line 249 */
v0 = (( android.os.HwParcel ) v1 ).readBool ( ); // invoke-virtual {v1}, Landroid/os/HwParcel;->readBool()Z
/* :try_end_0 */
/* .catch Ljava/lang/Exception; {:try_start_0 ..:try_end_0} :catch_0 */
/* .catchall {:try_start_0 ..:try_end_0} :catchall_0 */
/* .line 250 */
/* .local v0, "flag":Z */
/* nop */
/* .line 257 */
(( android.os.HwParcel ) v1 ).release ( ); // invoke-virtual {v1}, Landroid/os/HwParcel;->release()V
/* .line 250 */
/* .line 252 */
} // .end local v0 # "flag":Z
} // .end local v4 # "hidl_request":Landroid/os/HwParcel;
} // :cond_0
try { // :try_start_1
v0 = com.miui.server.MiuiSwapService.TAG;
final String v4 = "hwService get failed, please try again"; // const-string v4, "hwService get failed, please try again"
android.util.Slog .d ( v0,v4 );
/* :try_end_1 */
/* .catch Ljava/lang/Exception; {:try_start_1 ..:try_end_1} :catch_0 */
/* .catchall {:try_start_1 ..:try_end_1} :catchall_0 */
/* .line 257 */
/* nop */
} // .end local v3 # "hwService":Landroid/os/IHwBinder;
} // :goto_0
(( android.os.HwParcel ) v1 ).release ( ); // invoke-virtual {v1}, Landroid/os/HwParcel;->release()V
/* .line 258 */
/* .line 257 */
/* :catchall_0 */
/* move-exception v0 */
/* .line 254 */
/* :catch_0 */
/* move-exception v0 */
/* .line 255 */
/* .local v0, "e":Ljava/lang/Exception; */
try { // :try_start_2
v3 = com.miui.server.MiuiSwapService.TAG;
/* new-instance v4, Ljava/lang/StringBuilder; */
/* invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V */
final String v5 = "fail to get SwapHalService"; // const-string v5, "fail to get SwapHalService"
(( java.lang.StringBuilder ) v4 ).append ( v5 ); // invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v4 ).append ( v0 ); // invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v4 ).toString ( ); // invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
android.util.Slog .e ( v3,v4 );
/* :try_end_2 */
/* .catchall {:try_start_2 ..:try_end_2} :catchall_0 */
/* .line 257 */
/* nop */
} // .end local v0 # "e":Ljava/lang/Exception;
/* .line 259 */
} // :goto_1
/* .line 257 */
} // :goto_2
(( android.os.HwParcel ) v1 ).release ( ); // invoke-virtual {v1}, Landroid/os/HwParcel;->release()V
/* .line 258 */
/* throw v0 */
} // .end method
private void startSwapLocked ( ) {
/* .locals 4 */
/* .line 111 */
(( com.miui.server.MiuiSwapService ) p0 ).callNativeSwap ( ); // invoke-virtual {p0}, Lcom/miui/server/MiuiSwapService;->callNativeSwap()Ljava/lang/String;
/* .line 112 */
/* .local v0, "nativeReturnReslut":Ljava/lang/String; */
v1 = com.miui.server.MiuiSwapService.TAG;
/* new-instance v2, Ljava/lang/StringBuilder; */
/* invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V */
final String v3 = "native return result :"; // const-string v3, "native return result :"
(( java.lang.StringBuilder ) v2 ).append ( v3 ); // invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v2 ).append ( v0 ); // invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v2 ).toString ( ); // invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
android.util.Slog .d ( v1,v2 );
/* .line 113 */
final String v2 = ""; // const-string v2, ""
/* if-eq v0, v2, :cond_0 */
/* .line 114 */
/* invoke-direct {p0, v0}, Lcom/miui/server/MiuiSwapService;->callHalSwap(Ljava/lang/String;)V */
/* .line 116 */
} // :cond_0
final String v2 = "callNativeSwap return result is null"; // const-string v2, "callNativeSwap return result is null"
android.util.Slog .w ( v1,v2 );
/* .line 118 */
} // :goto_0
return;
} // .end method
private Boolean swapHalDisable ( ) {
/* .locals 6 */
/* .line 217 */
/* const-string/jumbo v0, "vendor.xiaomi.hardware.swap@1.0::ISwap" */
/* new-instance v1, Landroid/os/HwParcel; */
/* invoke-direct {v1}, Landroid/os/HwParcel;-><init>()V */
/* .line 219 */
/* .local v1, "hidl_reply":Landroid/os/HwParcel; */
int v2 = 0; // const/4 v2, 0x0
try { // :try_start_0
final String v3 = "default"; // const-string v3, "default"
android.os.HwBinder .getService ( v0,v3 );
/* .line 220 */
/* .local v3, "hwService":Landroid/os/IHwBinder; */
if ( v3 != null) { // if-eqz v3, :cond_0
/* .line 221 */
/* new-instance v4, Landroid/os/HwParcel; */
/* invoke-direct {v4}, Landroid/os/HwParcel;-><init>()V */
/* .line 222 */
/* .local v4, "hidl_request":Landroid/os/HwParcel; */
(( android.os.HwParcel ) v4 ).writeInterfaceToken ( v0 ); // invoke-virtual {v4, v0}, Landroid/os/HwParcel;->writeInterfaceToken(Ljava/lang/String;)V
/* .line 223 */
int v0 = 4; // const/4 v0, 0x4
/* .line 224 */
(( android.os.HwParcel ) v1 ).verifySuccess ( ); // invoke-virtual {v1}, Landroid/os/HwParcel;->verifySuccess()V
/* .line 225 */
(( android.os.HwParcel ) v4 ).releaseTemporaryStorage ( ); // invoke-virtual {v4}, Landroid/os/HwParcel;->releaseTemporaryStorage()V
/* .line 226 */
v0 = (( android.os.HwParcel ) v1 ).readBool ( ); // invoke-virtual {v1}, Landroid/os/HwParcel;->readBool()Z
/* :try_end_0 */
/* .catch Ljava/lang/Exception; {:try_start_0 ..:try_end_0} :catch_0 */
/* .catchall {:try_start_0 ..:try_end_0} :catchall_0 */
/* .line 227 */
/* .local v0, "flag":Z */
/* nop */
/* .line 234 */
(( android.os.HwParcel ) v1 ).release ( ); // invoke-virtual {v1}, Landroid/os/HwParcel;->release()V
/* .line 227 */
/* .line 229 */
} // .end local v0 # "flag":Z
} // .end local v4 # "hidl_request":Landroid/os/HwParcel;
} // :cond_0
try { // :try_start_1
v0 = com.miui.server.MiuiSwapService.TAG;
final String v4 = "hwService get failed, please try again"; // const-string v4, "hwService get failed, please try again"
android.util.Slog .d ( v0,v4 );
/* :try_end_1 */
/* .catch Ljava/lang/Exception; {:try_start_1 ..:try_end_1} :catch_0 */
/* .catchall {:try_start_1 ..:try_end_1} :catchall_0 */
/* .line 234 */
/* nop */
} // .end local v3 # "hwService":Landroid/os/IHwBinder;
} // :goto_0
(( android.os.HwParcel ) v1 ).release ( ); // invoke-virtual {v1}, Landroid/os/HwParcel;->release()V
/* .line 235 */
/* .line 234 */
/* :catchall_0 */
/* move-exception v0 */
/* .line 231 */
/* :catch_0 */
/* move-exception v0 */
/* .line 232 */
/* .local v0, "e":Ljava/lang/Exception; */
try { // :try_start_2
v3 = com.miui.server.MiuiSwapService.TAG;
/* new-instance v4, Ljava/lang/StringBuilder; */
/* invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V */
final String v5 = "fail to get SwapHalService"; // const-string v5, "fail to get SwapHalService"
(( java.lang.StringBuilder ) v4 ).append ( v5 ); // invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v4 ).append ( v0 ); // invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v4 ).toString ( ); // invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
android.util.Slog .e ( v3,v4 );
/* :try_end_2 */
/* .catchall {:try_start_2 ..:try_end_2} :catchall_0 */
/* .line 234 */
/* nop */
} // .end local v0 # "e":Ljava/lang/Exception;
/* .line 236 */
} // :goto_1
/* .line 234 */
} // :goto_2
(( android.os.HwParcel ) v1 ).release ( ); // invoke-virtual {v1}, Landroid/os/HwParcel;->release()V
/* .line 235 */
/* throw v0 */
} // .end method
/* # virtual methods */
public java.lang.String callNativeSwap ( ) {
/* .locals 4 */
/* .line 141 */
/* invoke-direct {p0}, Lcom/miui/server/MiuiSwapService;->getNativeService()Lmiui/swap/ISwap; */
/* .line 142 */
final String v0 = ""; // const-string v0, ""
/* .line 144 */
/* .local v0, "getLba":Ljava/lang/String; */
try { // :try_start_0
v1 = this.mSwapNativeService;
if ( v1 != null) { // if-eqz v1, :cond_0
v1 = v1 = this.mSwapNativeService;
if ( v1 != null) { // if-eqz v1, :cond_0
/* .line 145 */
v1 = this.mSwapNativeService;
/* :try_end_0 */
/* .catch Ljava/lang/Exception; {:try_start_0 ..:try_end_0} :catch_0 */
} // :cond_0
int v1 = 0; // const/4 v1, 0x0
} // :goto_0
/* move-object v0, v1 */
/* .line 148 */
/* .line 146 */
/* :catch_0 */
/* move-exception v1 */
/* .line 147 */
/* .local v1, "e":Ljava/lang/Exception; */
v2 = com.miui.server.MiuiSwapService.TAG;
final String v3 = "Failed to access native swap methods"; // const-string v3, "Failed to access native swap methods"
android.util.Slog .e ( v2,v3,v1 );
/* .line 149 */
} // .end local v1 # "e":Ljava/lang/Exception;
} // :goto_1
} // .end method
public void getUsbStatus ( ) {
/* .locals 3 */
/* .line 263 */
v0 = this.mContext;
if ( v0 != null) { // if-eqz v0, :cond_0
/* .line 264 */
/* new-instance v0, Landroid/content/IntentFilter; */
/* invoke-direct {v0}, Landroid/content/IntentFilter;-><init>()V */
/* .line 265 */
/* .local v0, "filter":Landroid/content/IntentFilter; */
final String v1 = "android.hardware.usb.action.USB_STATE"; // const-string v1, "android.hardware.usb.action.USB_STATE"
(( android.content.IntentFilter ) v0 ).addAction ( v1 ); // invoke-virtual {v0, v1}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V
/* .line 266 */
v1 = this.mContext;
/* new-instance v2, Lcom/miui/server/MiuiSwapService$UsbStatusReceiver; */
/* invoke-direct {v2, p0}, Lcom/miui/server/MiuiSwapService$UsbStatusReceiver;-><init>(Lcom/miui/server/MiuiSwapService;)V */
(( android.content.Context ) v1 ).registerReceiver ( v2, v0 ); // invoke-virtual {v1, v2, v0}, Landroid/content/Context;->registerReceiver(Landroid/content/BroadcastReceiver;Landroid/content/IntentFilter;)Landroid/content/Intent;
/* .line 268 */
} // .end local v0 # "filter":Landroid/content/IntentFilter;
} // :cond_0
return;
} // .end method
public void swapControl ( ) {
/* .locals 4 */
/* .line 123 */
final String v0 = "persist.sys.stability.swapEnable"; // const-string v0, "persist.sys.stability.swapEnable"
int v1 = 0; // const/4 v1, 0x0
v0 = android.os.SystemProperties .getBoolean ( v0,v1 );
java.lang.Boolean .valueOf ( v0 );
/* .line 124 */
/* .local v0, "swapEnable":Ljava/lang/Boolean; */
v2 = (( java.lang.Boolean ) v0 ).booleanValue ( ); // invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z
if ( v2 != null) { // if-eqz v2, :cond_0
/* .line 125 */
/* iget-boolean v2, p0, Lcom/miui/server/MiuiSwapService;->mUsbConnect:Z */
/* if-nez v2, :cond_1 */
/* .line 126 */
/* invoke-direct {p0}, Lcom/miui/server/MiuiSwapService;->startSwapLocked()V */
/* .line 129 */
} // :cond_0
/* invoke-direct {p0}, Lcom/miui/server/MiuiSwapService;->swapHalDisable()Z */
/* .line 130 */
v2 = com.miui.server.MiuiSwapService.TAG;
final String v3 = "The cloud control version does not support this function"; // const-string v3, "The cloud control version does not support this function"
android.util.Slog .w ( v2,v3 );
/* .line 132 */
} // :cond_1
} // :goto_0
v2 = (( java.lang.Boolean ) v0 ).booleanValue ( ); // invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z
java.lang.Boolean .toString ( v2 );
final String v3 = "persist.sys.stability.preswapEnable"; // const-string v3, "persist.sys.stability.preswapEnable"
android.os.SystemProperties .set ( v3,v2 );
/* .line 133 */
final String v2 = "persist.sys.swapservice.ctrl"; // const-string v2, "persist.sys.swapservice.ctrl"
java.lang.Boolean .toString ( v1 );
android.os.SystemProperties .set ( v2,v1 );
/* .line 134 */
return;
} // .end method
