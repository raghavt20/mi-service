class com.miui.server.MiuiWebViewManagerService$2 extends android.os.Handler {
	 /* .source "MiuiWebViewManagerService.java" */
	 /* # annotations */
	 /* .annotation system Ldalvik/annotation/EnclosingMethod; */
	 /* value = Lcom/miui/server/MiuiWebViewManagerService;-><init>(Landroid/content/Context;)V */
} // .end annotation
/* .annotation system Ldalvik/annotation/InnerClass; */
/* accessFlags = 0x0 */
/* name = null */
} // .end annotation
/* # instance fields */
final com.miui.server.MiuiWebViewManagerService this$0; //synthetic
/* # direct methods */
 com.miui.server.MiuiWebViewManagerService$2 ( ) {
/* .locals 0 */
/* .param p1, "this$0" # Lcom/miui/server/MiuiWebViewManagerService; */
/* .param p2, "looper" # Landroid/os/Looper; */
/* .line 44 */
this.this$0 = p1;
/* invoke-direct {p0, p2}, Landroid/os/Handler;-><init>(Landroid/os/Looper;)V */
return;
} // .end method
/* # virtual methods */
public void handleMessage ( android.os.Message p0 ) {
/* .locals 13 */
/* .param p1, "msg" # Landroid/os/Message; */
/* .line 47 */
/* iget v0, p1, Landroid/os/Message;->what:I */
v1 = this.this$0;
v1 = com.miui.server.MiuiWebViewManagerService .-$$Nest$fgetMSG_RESTART_WEBVIEW ( v1 );
/* if-ne v0, v1, :cond_3 */
/* .line 49 */
(( android.os.Message ) p1 ).getData ( ); // invoke-virtual {p1}, Landroid/os/Message;->getData()Landroid/os/Bundle;
/* const-string/jumbo v1, "sendTime" */
(( android.os.Bundle ) v0 ).getLong ( v1 ); // invoke-virtual {v0, v1}, Landroid/os/Bundle;->getLong(Ljava/lang/String;)J
/* move-result-wide v0 */
/* .line 50 */
/* .local v0, "send":J */
final String v2 = "activity"; // const-string v2, "activity"
android.os.ServiceManager .getService ( v2 );
/* check-cast v2, Lcom/android/server/am/ActivityManagerService; */
/* .line 51 */
/* .local v2, "am":Lcom/android/server/am/ActivityManagerService; */
v3 = this.this$0;
com.miui.server.MiuiWebViewManagerService .-$$Nest$mcollectWebViewProcesses ( v3,v2 );
/* .line 52 */
/* .local v3, "pkgs":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;" */
int v4 = 0; // const/4 v4, 0x0
/* .line 53 */
/* .local v4, "killed":I */
if ( v3 != null) { // if-eqz v3, :cond_3
	 /* .line 54 */
	 int v5 = 0; // const/4 v5, 0x0
	 /* .local v5, "i":I */
v6 = } // :goto_0
final String v7 = "MiuiWebViewManagerService"; // const-string v7, "MiuiWebViewManagerService"
/* if-ge v5, v6, :cond_2 */
/* .line 55 */
/* check-cast v6, Ljava/lang/String; */
/* .line 56 */
/* .local v6, "pkgName":Ljava/lang/String; */
/* if-nez v6, :cond_0 */
/* .line 57 */
/* .line 59 */
} // :cond_0
final String v8 = "#"; // const-string v8, "#"
(( java.lang.String ) v6 ).split ( v8 ); // invoke-virtual {v6, v8}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;
/* .line 60 */
/* .local v8, "splitInfo":[Ljava/lang/String; */
int v9 = 0; // const/4 v9, 0x0
/* aget-object v9, v8, v9 */
/* .line 61 */
/* .local v9, "barePkgName":Ljava/lang/String; */
int v10 = 1; // const/4 v10, 0x1
/* aget-object v10, v8, v10 */
java.lang.Integer .valueOf ( v10 );
v10 = (( java.lang.Integer ) v10 ).intValue ( ); // invoke-virtual {v10}, Ljava/lang/Integer;->intValue()I
/* .line 62 */
/* .local v10, "pid":I */
v11 = com.miui.server.MiuiWebViewManagerService .-$$Nest$sfgetEXEMPT_APPS ( );
if ( v11 != null) { // if-eqz v11, :cond_1
/* .line 63 */
/* .line 65 */
} // :cond_1
v11 = this.this$0;
com.miui.server.MiuiWebViewManagerService .-$$Nest$fgetmContext ( v11 );
v11 = (( android.content.Context ) v11 ).getUserId ( ); // invoke-virtual {v11}, Landroid/content/Context;->getUserId()I
(( com.android.server.am.ActivityManagerService ) v2 ).forceStopPackage ( v9, v11 ); // invoke-virtual {v2, v9, v11}, Lcom/android/server/am/ActivityManagerService;->forceStopPackage(Ljava/lang/String;I)V
/* .line 66 */
/* new-instance v11, Ljava/lang/StringBuilder; */
/* invoke-direct {v11}, Ljava/lang/StringBuilder;-><init>()V */
final String v12 = "kill pkgName: "; // const-string v12, "kill pkgName: "
(( java.lang.StringBuilder ) v11 ).append ( v12 ); // invoke-virtual {v11, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v11 ).append ( v9 ); // invoke-virtual {v11, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
final String v12 = " pid: "; // const-string v12, " pid: "
(( java.lang.StringBuilder ) v11 ).append ( v12 ); // invoke-virtual {v11, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v11 ).append ( v10 ); // invoke-virtual {v11, v10}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v11 ).toString ( ); // invoke-virtual {v11}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
android.util.Log .d ( v7,v11 );
/* .line 67 */
/* add-int/lit8 v4, v4, 0x1 */
/* .line 54 */
} // .end local v6 # "pkgName":Ljava/lang/String;
} // .end local v8 # "splitInfo":[Ljava/lang/String;
} // .end local v9 # "barePkgName":Ljava/lang/String;
} // .end local v10 # "pid":I
} // :goto_1
/* add-int/lit8 v5, v5, 0x1 */
/* .line 70 */
} // .end local v5 # "i":I
} // :cond_2
/* new-instance v5, Ljava/lang/StringBuilder; */
/* invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V */
final String v6 = "restart webview procs: "; // const-string v6, "restart webview procs: "
v6 = (( java.lang.StringBuilder ) v5 ).append ( v6 ); // invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v5 ).append ( v6 ); // invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;
final String v6 = " with timeUsage: "; // const-string v6, " with timeUsage: "
(( java.lang.StringBuilder ) v5 ).append ( v6 ); // invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
/* .line 71 */
java.lang.System .currentTimeMillis ( );
/* move-result-wide v8 */
/* sub-long/2addr v8, v0 */
(( java.lang.StringBuilder ) v5 ).append ( v8, v9 ); // invoke-virtual {v5, v8, v9}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;
final String v6 = "ms killed: "; // const-string v6, "ms killed: "
(( java.lang.StringBuilder ) v5 ).append ( v6 ); // invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v5 ).append ( v4 ); // invoke-virtual {v5, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v5 ).toString ( ); // invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
/* .line 70 */
android.util.Log .d ( v7,v5 );
/* .line 74 */
} // .end local v0 # "send":J
} // .end local v2 # "am":Lcom/android/server/am/ActivityManagerService;
} // .end local v3 # "pkgs":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;"
} // .end local v4 # "killed":I
} // :cond_3
return;
} // .end method
