.class public final Lcom/miui/server/MiuiWebViewManagerService$Lifecycle;
.super Lcom/android/server/SystemService;
.source "MiuiWebViewManagerService.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/miui/server/MiuiWebViewManagerService;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "Lifecycle"
.end annotation


# instance fields
.field private final mService:Lcom/miui/server/MiuiWebViewManagerService;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 1
    .param p1, "context"    # Landroid/content/Context;

    .line 124
    invoke-direct {p0, p1}, Lcom/android/server/SystemService;-><init>(Landroid/content/Context;)V

    .line 125
    new-instance v0, Lcom/miui/server/MiuiWebViewManagerService;

    invoke-direct {v0, p1}, Lcom/miui/server/MiuiWebViewManagerService;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Lcom/miui/server/MiuiWebViewManagerService$Lifecycle;->mService:Lcom/miui/server/MiuiWebViewManagerService;

    .line 126
    return-void
.end method


# virtual methods
.method public onStart()V
    .locals 2

    .line 130
    const-string v0, "miuiwebview"

    iget-object v1, p0, Lcom/miui/server/MiuiWebViewManagerService$Lifecycle;->mService:Lcom/miui/server/MiuiWebViewManagerService;

    invoke-virtual {p0, v0, v1}, Lcom/miui/server/MiuiWebViewManagerService$Lifecycle;->publishBinderService(Ljava/lang/String;Landroid/os/IBinder;)V

    .line 131
    return-void
.end method
