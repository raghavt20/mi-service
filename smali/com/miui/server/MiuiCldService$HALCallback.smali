.class Lcom/miui/server/MiuiCldService$HALCallback;
.super Lmiui/hardware/ICldCallback$Stub;
.source "MiuiCldService.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/miui/server/MiuiCldService;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0xa
    name = "HALCallback"
.end annotation


# instance fields
.field public cldService:Lcom/miui/server/MiuiCldService;


# direct methods
.method constructor <init>(Lcom/miui/server/MiuiCldService;)V
    .locals 0
    .param p1, "cldService"    # Lcom/miui/server/MiuiCldService;

    .line 217
    invoke-direct {p0}, Lmiui/hardware/ICldCallback$Stub;-><init>()V

    .line 218
    iput-object p1, p0, Lcom/miui/server/MiuiCldService$HALCallback;->cldService:Lcom/miui/server/MiuiCldService;

    .line 219
    return-void
.end method


# virtual methods
.method public notifyStatusChange(I)V
    .locals 1
    .param p1, "level"    # I

    .line 222
    iget-object v0, p0, Lcom/miui/server/MiuiCldService$HALCallback;->cldService:Lcom/miui/server/MiuiCldService;

    invoke-static {v0, p1}, Lcom/miui/server/MiuiCldService;->-$$Nest$mreportCldProcessedBroadcast(Lcom/miui/server/MiuiCldService;I)V

    .line 223
    return-void
.end method
