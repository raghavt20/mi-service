.class Lcom/miui/server/SplashScreenServiceDelegate$2$1;
.super Ljava/lang/Object;
.source "SplashScreenServiceDelegate.java"

# interfaces
.implements Ljava/lang/Runnable;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/miui/server/SplashScreenServiceDelegate$2;->asyncSetSplashPackageCheckListener()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$1:Lcom/miui/server/SplashScreenServiceDelegate$2;


# direct methods
.method constructor <init>(Lcom/miui/server/SplashScreenServiceDelegate$2;)V
    .locals 0
    .param p1, "this$1"    # Lcom/miui/server/SplashScreenServiceDelegate$2;

    .line 205
    iput-object p1, p0, Lcom/miui/server/SplashScreenServiceDelegate$2$1;->this$1:Lcom/miui/server/SplashScreenServiceDelegate$2;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public run()V
    .locals 4

    .line 208
    iget-object v0, p0, Lcom/miui/server/SplashScreenServiceDelegate$2$1;->this$1:Lcom/miui/server/SplashScreenServiceDelegate$2;

    iget-object v0, v0, Lcom/miui/server/SplashScreenServiceDelegate$2;->this$0:Lcom/miui/server/SplashScreenServiceDelegate;

    invoke-static {v0}, Lcom/miui/server/SplashScreenServiceDelegate;->-$$Nest$fgetmSplashScreenService(Lcom/miui/server/SplashScreenServiceDelegate;)Lcom/miui/server/ISplashScreenService;

    move-result-object v0

    .line 209
    .local v0, "sss":Lcom/miui/server/ISplashScreenService;
    if-eqz v0, :cond_0

    .line 211
    :try_start_0
    iget-object v1, p0, Lcom/miui/server/SplashScreenServiceDelegate$2$1;->this$1:Lcom/miui/server/SplashScreenServiceDelegate$2;

    iget-object v1, v1, Lcom/miui/server/SplashScreenServiceDelegate$2;->this$0:Lcom/miui/server/SplashScreenServiceDelegate;

    const-string v2, "Set splash package check listener"

    invoke-static {v1, v2}, Lcom/miui/server/SplashScreenServiceDelegate;->-$$Nest$mlogI(Lcom/miui/server/SplashScreenServiceDelegate;Ljava/lang/String;)V

    .line 212
    iget-object v1, p0, Lcom/miui/server/SplashScreenServiceDelegate$2$1;->this$1:Lcom/miui/server/SplashScreenServiceDelegate$2;

    iget-object v1, v1, Lcom/miui/server/SplashScreenServiceDelegate$2;->this$0:Lcom/miui/server/SplashScreenServiceDelegate;

    invoke-static {v1}, Lcom/miui/server/SplashScreenServiceDelegate;->-$$Nest$fgetmSplashPackageCheckListener(Lcom/miui/server/SplashScreenServiceDelegate;)Lcom/miui/server/ISplashPackageCheckListener;

    move-result-object v1

    invoke-interface {v0, v1}, Lcom/miui/server/ISplashScreenService;->setSplashPackageListener(Lcom/miui/server/ISplashPackageCheckListener;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 215
    goto :goto_0

    .line 213
    :catch_0
    move-exception v1

    .line 214
    .local v1, "e":Ljava/lang/Exception;
    iget-object v2, p0, Lcom/miui/server/SplashScreenServiceDelegate$2$1;->this$1:Lcom/miui/server/SplashScreenServiceDelegate$2;

    iget-object v2, v2, Lcom/miui/server/SplashScreenServiceDelegate$2;->this$0:Lcom/miui/server/SplashScreenServiceDelegate;

    const-string v3, "asyncSetSplashPackageCheckListener exception"

    invoke-static {v2, v3, v1}, Lcom/miui/server/SplashScreenServiceDelegate;->-$$Nest$mlogE(Lcom/miui/server/SplashScreenServiceDelegate;Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 217
    .end local v1    # "e":Ljava/lang/Exception;
    :cond_0
    :goto_0
    return-void
.end method
