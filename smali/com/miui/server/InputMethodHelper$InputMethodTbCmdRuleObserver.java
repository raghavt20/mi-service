class com.miui.server.InputMethodHelper$InputMethodTbCmdRuleObserver extends android.database.ContentObserver {
	 /* .source "InputMethodHelper.java" */
	 /* # annotations */
	 /* .annotation system Ldalvik/annotation/EnclosingClass; */
	 /* value = Lcom/miui/server/InputMethodHelper; */
} // .end annotation
/* .annotation system Ldalvik/annotation/InnerClass; */
/* accessFlags = 0xa */
/* name = "InputMethodTbCmdRuleObserver" */
} // .end annotation
/* # instance fields */
private android.content.Context mContext;
/* # direct methods */
public com.miui.server.InputMethodHelper$InputMethodTbCmdRuleObserver ( ) {
/* .locals 0 */
/* .param p1, "handler" # Landroid/os/Handler; */
/* .param p2, "context" # Landroid/content/Context; */
/* .line 120 */
/* invoke-direct {p0, p1}, Landroid/database/ContentObserver;-><init>(Landroid/os/Handler;)V */
/* .line 121 */
this.mContext = p2;
/* .line 122 */
return;
} // .end method
/* # virtual methods */
public void onChange ( Boolean p0 ) {
/* .locals 1 */
/* .param p1, "selfChange" # Z */
/* .line 126 */
/* invoke-super {p0, p1}, Landroid/database/ContentObserver;->onChange(Z)V */
/* .line 127 */
v0 = this.mContext;
com.miui.server.InputMethodHelper .-$$Nest$sminitInputMethodTbCmdRule ( v0 );
/* .line 128 */
return;
} // .end method
