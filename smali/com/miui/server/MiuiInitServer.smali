.class public Lcom/miui/server/MiuiInitServer;
.super Lmiui/os/IMiuiInit$Stub;
.source "MiuiInitServer.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/miui/server/MiuiInitServer$InitCustEnvironmentTask;,
        Lcom/miui/server/MiuiInitServer$Lifecycle;
    }
.end annotation


# static fields
.field private static final CARRIER_REGION_PROP_FILE_NAME:Ljava/lang/String; = "region_specified.prop"

.field private static final CUST_PROPERTIES_FILE_NAME:Ljava/lang/String; = "cust.prop"

.field private static final PREINSTALL_APP_HISTORY_FILE:Ljava/lang/String; = "/data/app/preinstall_history"

.field private static final PREINSTALL_PACKAGE_LIST:Ljava/lang/String; = "/data/system/preinstall.list"

.field private static final TAG:Ljava/lang/String; = "MiuiInitServer"


# instance fields
.field mCompatModePackages:Lcom/miui/server/MiuiCompatModePackages;

.field private final mContext:Landroid/content/Context;

.field private mDoing:Z

.field private mMiuiPreinstallHelper:Lcom/android/server/pm/MiuiPreinstallHelper;

.field mNeedAspectSettings:Z

.field private mPreinstallHistoryMap:Ljava/util/HashMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/HashMap<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private mPreinstalledChannels:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method static bridge synthetic -$$Nest$fgetmContext(Lcom/miui/server/MiuiInitServer;)Landroid/content/Context;
    .locals 0

    iget-object p0, p0, Lcom/miui/server/MiuiInitServer;->mContext:Landroid/content/Context;

    return-object p0
.end method

.method static bridge synthetic -$$Nest$fgetmMiuiPreinstallHelper(Lcom/miui/server/MiuiInitServer;)Lcom/android/server/pm/MiuiPreinstallHelper;
    .locals 0

    iget-object p0, p0, Lcom/miui/server/MiuiInitServer;->mMiuiPreinstallHelper:Lcom/android/server/pm/MiuiPreinstallHelper;

    return-object p0
.end method

.method static bridge synthetic -$$Nest$fputmDoing(Lcom/miui/server/MiuiInitServer;Z)V
    .locals 0

    iput-boolean p1, p0, Lcom/miui/server/MiuiInitServer;->mDoing:Z

    return-void
.end method

.method static bridge synthetic -$$Nest$mdeletePackagesByRegion(Lcom/miui/server/MiuiInitServer;)V
    .locals 0

    invoke-direct {p0}, Lcom/miui/server/MiuiInitServer;->deletePackagesByRegion()V

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;)V
    .locals 3
    .param p1, "context"    # Landroid/content/Context;

    .line 82
    invoke-direct {p0}, Lmiui/os/IMiuiInit$Stub;-><init>()V

    .line 83
    iput-object p1, p0, Lcom/miui/server/MiuiInitServer;->mContext:Landroid/content/Context;

    .line 84
    invoke-static {p1}, Lcom/miui/server/EnableStateManager;->updateApplicationEnableState(Landroid/content/Context;)V

    .line 85
    invoke-direct {p0}, Lcom/miui/server/MiuiInitServer;->deletePackagesByRegion()V

    .line 86
    invoke-static {}, Lcom/android/server/pm/MiuiPreinstallHelper;->getInstance()Lcom/android/server/pm/MiuiPreinstallHelper;

    move-result-object v0

    iput-object v0, p0, Lcom/miui/server/MiuiInitServer;->mMiuiPreinstallHelper:Lcom/android/server/pm/MiuiPreinstallHelper;

    .line 89
    invoke-static {}, Lmiui/util/MiuiFeatureUtils;->setMiuisdkProperties()V

    .line 90
    const-string/jumbo v0, "window"

    invoke-static {v0}, Landroid/os/ServiceManager;->getService(Ljava/lang/String;)Landroid/os/IBinder;

    move-result-object v0

    check-cast v0, Lcom/android/server/wm/WindowManagerService;

    .line 91
    .local v0, "windowManager":Lcom/android/server/wm/WindowManagerService;
    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/android/server/wm/WindowManagerService;->hasNavigationBar(I)Z

    move-result v2

    iput-boolean v2, p0, Lcom/miui/server/MiuiInitServer;->mNeedAspectSettings:Z

    .line 92
    if-eqz v2, :cond_0

    sget-boolean v2, Lmiui/os/Build;->IS_TABLET:Z

    if-nez v2, :cond_0

    const/4 v1, 0x1

    :cond_0
    iput-boolean v1, p0, Lcom/miui/server/MiuiInitServer;->mNeedAspectSettings:Z

    .line 93
    if-nez v1, :cond_1

    sget-boolean v1, Lmiui/util/CustomizeUtil;->HAS_NOTCH:Z

    if-eqz v1, :cond_2

    .line 94
    :cond_1
    new-instance v1, Lcom/miui/server/MiuiCompatModePackages;

    invoke-direct {v1, p1}, Lcom/miui/server/MiuiCompatModePackages;-><init>(Landroid/content/Context;)V

    iput-object v1, p0, Lcom/miui/server/MiuiInitServer;->mCompatModePackages:Lcom/miui/server/MiuiCompatModePackages;

    .line 96
    :cond_2
    return-void
.end method

.method private checkPermission(Ljava/lang/String;)V
    .locals 4
    .param p1, "reason"    # Ljava/lang/String;

    .line 581
    iget-object v0, p0, Lcom/miui/server/MiuiInitServer;->mContext:Landroid/content/Context;

    const-string v1, "android.permission.SET_SCREEN_COMPATIBILITY"

    invoke-virtual {v0, v1}, Landroid/content/Context;->checkCallingPermission(Ljava/lang/String;)I

    move-result v0

    .line 583
    .local v0, "permission":I
    if-nez v0, :cond_0

    .line 584
    return-void

    .line 585
    :cond_0
    new-instance v1, Ljava/lang/SecurityException;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "Permission Denial: "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, " pid="

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    .line 586
    invoke-static {}, Landroid/os/Binder;->getCallingPid()I

    move-result v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, ", uid="

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-static {}, Landroid/os/Binder;->getCallingUid()I

    move-result v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-direct {v1, v2}, Ljava/lang/SecurityException;-><init>(Ljava/lang/String;)V

    throw v1
.end method

.method private deletePackagesByRegion()V
    .locals 4

    .line 591
    const-string v0, "ro.miui.region"

    const-string v1, ""

    invoke-static {v0, v1}, Landroid/os/SystemProperties;->get(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    const-string v2, "IT"

    invoke-virtual {v2, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    const-string v0, "ro.miui.build.region"

    invoke-static {v0, v1}, Landroid/os/SystemProperties;->get(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    const-string v1, "eea"

    invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 593
    :try_start_0
    iget-object v0, p0, Lcom/miui/server/MiuiInitServer;->mContext:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v0

    .line 594
    .local v0, "pm":Landroid/content/pm/PackageManager;
    const-string v1, "com.miui.fm"

    const/4 v2, 0x0

    const/4 v3, 0x0

    invoke-virtual {v0, v1, v3, v2}, Landroid/content/pm/PackageManager;->deletePackage(Ljava/lang/String;Landroid/content/pm/IPackageDeleteObserver;I)V

    .line 595
    const-string v1, "com.miui.fmservice"

    invoke-virtual {v0, v1, v3, v2}, Landroid/content/pm/PackageManager;->deletePackage(Ljava/lang/String;Landroid/content/pm/IPackageDeleteObserver;I)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 597
    .end local v0    # "pm":Landroid/content/pm/PackageManager;
    goto :goto_0

    .line 596
    :catch_0
    move-exception v0

    .line 599
    :cond_0
    :goto_0
    return-void
.end method


# virtual methods
.method public copyPreinstallPAITrackingFile(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V
    .locals 3
    .param p1, "type"    # Ljava/lang/String;
    .param p2, "fileName"    # Ljava/lang/String;
    .param p3, "content"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    .line 463
    iget-object v0, p0, Lcom/miui/server/MiuiInitServer;->mContext:Landroid/content/Context;

    const-string v1, "miui.os.permisson.INIT_MIUI_ENVIRONMENT"

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Landroid/content/Context;->enforceCallingOrSelfPermission(Ljava/lang/String;Ljava/lang/String;)V

    .line 466
    invoke-static {p1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_2

    invoke-static {p3}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_0

    goto :goto_1

    .line 470
    :cond_0
    iget-object v0, p0, Lcom/miui/server/MiuiInitServer;->mMiuiPreinstallHelper:Lcom/android/server/pm/MiuiPreinstallHelper;

    invoke-virtual {v0}, Lcom/android/server/pm/MiuiPreinstallHelper;->isSupportNewFrame()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 471
    invoke-static {p1, p2, p3}, Lcom/android/server/pm/MiuiPAIPreinstallConfig;->copyPreinstallPAITrackingFile(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    .line 473
    :cond_1
    invoke-static {p1, p2, p3}, Lcom/android/server/pm/PreinstallApp;->copyPreinstallPAITrackingFile(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 475
    :goto_0
    return-void

    .line 467
    :cond_2
    :goto_1
    return-void
.end method

.method public deleteAllEventRecords(I)Z
    .locals 1
    .param p1, "eventType"    # I

    .line 612
    const-class v0, Lcom/android/server/pm/PackageEventRecorderInternal;

    invoke-static {v0}, Lcom/android/server/LocalServices;->getService(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/android/server/pm/PackageEventRecorderInternal;

    .line 613
    invoke-interface {v0, p1}, Lcom/android/server/pm/PackageEventRecorderInternal;->deleteAllEventRecords(I)Z

    move-result v0

    .line 612
    return v0
.end method

.method public deleteEventRecords(ILjava/util/List;)V
    .locals 1
    .param p1, "eventType"    # I
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(I",
            "Ljava/util/List<",
            "Ljava/lang/String;",
            ">;)V"
        }
    .end annotation

    .line 607
    .local p2, "eventIds":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;"
    const-class v0, Lcom/android/server/pm/PackageEventRecorderInternal;

    invoke-static {v0}, Lcom/android/server/LocalServices;->getService(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/android/server/pm/PackageEventRecorderInternal;

    .line 608
    invoke-interface {v0, p1, p2}, Lcom/android/server/pm/PackageEventRecorderInternal;->commitDeletedEvents(ILjava/util/List;)V

    .line 609
    return-void
.end method

.method public doFactoryReset(Z)V
    .locals 2
    .param p1, "keepUserApps"    # Z
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    .line 351
    sget-boolean v0, Lmiui/os/Build;->IS_GLOBAL_BUILD:Z

    if-eqz v0, :cond_0

    .line 352
    const-string v0, ""

    invoke-static {v0}, Lmiui/util/CustomizeUtil;->setMiuiCustVariatDir(Ljava/lang/String;)V

    .line 353
    invoke-static {}, Lmiui/util/CustomizeUtil;->getMiuiCustVariantFile()Ljava/io/File;

    move-result-object v0

    .line 354
    .local v0, "file":Ljava/io/File;
    invoke-virtual {v0}, Ljava/io/File;->exists()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 355
    invoke-virtual {v0}, Ljava/io/File;->delete()Z

    .line 358
    .end local v0    # "file":Ljava/io/File;
    :cond_0
    if-nez p1, :cond_1

    .line 359
    new-instance v0, Ljava/io/File;

    const-string v1, "/data/app/preinstall_history"

    invoke-direct {v0, v1}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 360
    .restart local v0    # "file":Ljava/io/File;
    invoke-virtual {v0}, Ljava/io/File;->exists()Z

    move-result v1

    if-eqz v1, :cond_1

    .line 361
    invoke-virtual {v0}, Ljava/io/File;->delete()Z

    .line 364
    .end local v0    # "file":Ljava/io/File;
    :cond_1
    return-void
.end method

.method public getAspectRatio(Ljava/lang/String;)F
    .locals 1
    .param p1, "pkg"    # Ljava/lang/String;

    .line 527
    iget-boolean v0, p0, Lcom/miui/server/MiuiInitServer;->mNeedAspectSettings:Z

    if-nez v0, :cond_0

    .line 528
    const/high16 v0, 0x40400000    # 3.0f

    return v0

    .line 530
    :cond_0
    iget-object v0, p0, Lcom/miui/server/MiuiInitServer;->mCompatModePackages:Lcom/miui/server/MiuiCompatModePackages;

    invoke-virtual {v0, p1}, Lcom/miui/server/MiuiCompatModePackages;->getAspectRatio(Ljava/lang/String;)F

    move-result v0

    return v0
.end method

.method public getCustVariants()[Ljava/lang/String;
    .locals 13
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    .line 327
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    .line 328
    .local v0, "regionList":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/String;>;"
    invoke-static {}, Lmiui/util/CustomizeUtil;->getMiuiCustPropDir()Ljava/io/File;

    move-result-object v1

    .line 329
    .local v1, "cust":Ljava/io/File;
    invoke-static {}, Ljava/util/Locale;->getISOCountries()[Ljava/lang/String;

    move-result-object v2

    .line 330
    .local v2, "cs":[Ljava/lang/String;
    invoke-virtual {v1}, Ljava/io/File;->listFiles()[Ljava/io/File;

    move-result-object v3

    .line 331
    .local v3, "resgions":[Ljava/io/File;
    const/4 v4, 0x0

    if-eqz v3, :cond_3

    .line 332
    array-length v5, v3

    move v6, v4

    :goto_0
    if-ge v6, v5, :cond_3

    aget-object v7, v3, v6

    .line 333
    .local v7, "region":Ljava/io/File;
    invoke-virtual {v7}, Ljava/io/File;->isDirectory()Z

    move-result v8

    if-nez v8, :cond_0

    .line 334
    goto :goto_2

    .line 337
    :cond_0
    invoke-virtual {v7}, Ljava/io/File;->getName()Ljava/lang/String;

    move-result-object v8

    .line 338
    .local v8, "r":Ljava/lang/String;
    array-length v9, v2

    move v10, v4

    :goto_1
    if-ge v10, v9, :cond_2

    aget-object v11, v2, v10

    .line 339
    .local v11, "c":Ljava/lang/String;
    invoke-virtual {v11, v8}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v12

    if-eqz v12, :cond_1

    .line 340
    invoke-virtual {v0, v8}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 341
    nop

    .line 338
    .end local v11    # "c":Ljava/lang/String;
    :cond_1
    add-int/lit8 v10, v10, 0x1

    goto :goto_1

    .line 332
    .end local v7    # "region":Ljava/io/File;
    .end local v8    # "r":Ljava/lang/String;
    :cond_2
    :goto_2
    add-int/lit8 v6, v6, 0x1

    goto :goto_0

    .line 346
    :cond_3
    new-array v4, v4, [Ljava/lang/String;

    invoke-virtual {v0, v4}, Ljava/util/ArrayList;->toArray([Ljava/lang/Object;)[Ljava/lang/Object;

    move-result-object v4

    check-cast v4, [Ljava/lang/String;

    return-object v4
.end method

.method public getCutoutMode(Ljava/lang/String;)I
    .locals 3
    .param p1, "pkg"    # Ljava/lang/String;

    .line 569
    sget-boolean v0, Lmiui/util/CustomizeUtil;->HAS_NOTCH:Z

    if-nez v0, :cond_0

    .line 570
    const/4 v0, 0x0

    return v0

    .line 572
    :cond_0
    invoke-static {}, Landroid/os/Binder;->clearCallingIdentity()J

    move-result-wide v0

    .line 574
    .local v0, "identity":J
    :try_start_0
    iget-object v2, p0, Lcom/miui/server/MiuiInitServer;->mCompatModePackages:Lcom/miui/server/MiuiCompatModePackages;

    invoke-virtual {v2, p1}, Lcom/miui/server/MiuiCompatModePackages;->getCutoutMode(Ljava/lang/String;)I

    move-result v2
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 576
    invoke-static {v0, v1}, Landroid/os/Binder;->restoreCallingIdentity(J)V

    .line 574
    return v2

    .line 576
    :catchall_0
    move-exception v2

    invoke-static {v0, v1}, Landroid/os/Binder;->restoreCallingIdentity(J)V

    .line 577
    throw v2
.end method

.method public getDefaultAspectType(Ljava/lang/String;)I
    .locals 1
    .param p1, "pkg"    # Ljava/lang/String;

    .line 534
    iget-boolean v0, p0, Lcom/miui/server/MiuiInitServer;->mNeedAspectSettings:Z

    if-nez v0, :cond_0

    .line 535
    const/4 v0, 0x0

    return v0

    .line 537
    :cond_0
    iget-object v0, p0, Lcom/miui/server/MiuiInitServer;->mCompatModePackages:Lcom/miui/server/MiuiCompatModePackages;

    invoke-virtual {v0, p1}, Lcom/miui/server/MiuiCompatModePackages;->getDefaultAspectType(Ljava/lang/String;)I

    move-result v0

    return v0
.end method

.method public getMiuiChannelPath(Ljava/lang/String;)Ljava/lang/String;
    .locals 4
    .param p1, "pkg"    # Ljava/lang/String;

    .line 391
    invoke-static {p1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    const-string v1, ""

    if-eqz v0, :cond_0

    .line 392
    return-object v1

    .line 394
    :cond_0
    invoke-virtual {p0, p1}, Lcom/miui/server/MiuiInitServer;->isPreinstalledPackage(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_1

    .line 395
    return-object v1

    .line 397
    :cond_1
    iget-object v0, p0, Lcom/miui/server/MiuiInitServer;->mPreinstalledChannels:Ljava/util/ArrayList;

    if-nez v0, :cond_3

    .line 398
    iget-object v0, p0, Lcom/miui/server/MiuiInitServer;->mMiuiPreinstallHelper:Lcom/android/server/pm/MiuiPreinstallHelper;

    invoke-virtual {v0}, Lcom/android/server/pm/MiuiPreinstallHelper;->isSupportNewFrame()Z

    move-result v0

    if-eqz v0, :cond_2

    .line 399
    iget-object v0, p0, Lcom/miui/server/MiuiInitServer;->mMiuiPreinstallHelper:Lcom/android/server/pm/MiuiPreinstallHelper;

    .line 400
    invoke-virtual {v0}, Lcom/android/server/pm/MiuiPreinstallHelper;->getBusinessPreinstallConfig()Lcom/android/server/pm/MiuiBusinessPreinstallConfig;

    move-result-object v0

    invoke-virtual {v0}, Lcom/android/server/pm/MiuiBusinessPreinstallConfig;->getPeinstalledChannelList()Ljava/util/ArrayList;

    move-result-object v0

    iput-object v0, p0, Lcom/miui/server/MiuiInitServer;->mPreinstalledChannels:Ljava/util/ArrayList;

    goto :goto_0

    .line 402
    :cond_2
    invoke-static {}, Lcom/android/server/pm/PreinstallApp;->getPeinstalledChannelList()Ljava/util/ArrayList;

    move-result-object v0

    iput-object v0, p0, Lcom/miui/server/MiuiInitServer;->mPreinstalledChannels:Ljava/util/ArrayList;

    .line 405
    :cond_3
    :goto_0
    iget-object v0, p0, Lcom/miui/server/MiuiInitServer;->mPreinstalledChannels:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v0

    :goto_1
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_5

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/lang/String;

    .line 406
    .local v2, "channel":Ljava/lang/String;
    invoke-virtual {v2, p1}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v3

    if-eqz v3, :cond_4

    new-instance v3, Ljava/io/File;

    invoke-direct {v3, v2}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    invoke-virtual {v3}, Ljava/io/File;->exists()Z

    move-result v3

    if-eqz v3, :cond_4

    .line 407
    return-object v2

    .line 409
    .end local v2    # "channel":Ljava/lang/String;
    :cond_4
    goto :goto_1

    .line 410
    :cond_5
    return-object v1
.end method

.method public getMiuiPreinstallAppPath(Ljava/lang/String;)Ljava/lang/String;
    .locals 7
    .param p1, "pkg"    # Ljava/lang/String;

    .line 491
    invoke-static {p1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    const-string v1, ""

    if-eqz v0, :cond_0

    .line 492
    return-object v1

    .line 494
    :cond_0
    iget-object v0, p0, Lcom/miui/server/MiuiInitServer;->mPreinstallHistoryMap:Ljava/util/HashMap;

    if-nez v0, :cond_2

    .line 495
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    iput-object v0, p0, Lcom/miui/server/MiuiInitServer;->mPreinstallHistoryMap:Ljava/util/HashMap;

    .line 497
    :try_start_0
    const-string v0, "removable_apk_list"

    invoke-static {v0}, Lmiui/util/FeatureParser;->getStringArray(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v0

    .line 498
    .local v0, "pkgNameList":[Ljava/lang/String;
    const-string v2, "removable_apk_path_list"

    invoke-static {v2}, Lmiui/util/FeatureParser;->getStringArray(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v2

    .line 499
    .local v2, "appPathList":[Ljava/lang/String;
    if-eqz v0, :cond_1

    if-eqz v2, :cond_1

    array-length v3, v0

    array-length v4, v2

    if-ne v3, v4, :cond_1

    .line 500
    const/4 v3, 0x0

    .local v3, "i":I
    :goto_0
    array-length v4, v0

    if-ge v3, v4, :cond_1

    .line 501
    iget-object v4, p0, Lcom/miui/server/MiuiInitServer;->mPreinstallHistoryMap:Ljava/util/HashMap;

    aget-object v5, v0, v3

    aget-object v6, v2, v3

    invoke-virtual {v4, v5, v6}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 500
    add-int/lit8 v3, v3, 0x1

    goto :goto_0

    .line 506
    .end local v0    # "pkgNameList":[Ljava/lang/String;
    .end local v2    # "appPathList":[Ljava/lang/String;
    .end local v3    # "i":I
    :cond_1
    goto :goto_1

    .line 504
    :catch_0
    move-exception v0

    .line 505
    .local v0, "e":Ljava/lang/Exception;
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "Error occurs while get miui preinstall app path + "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    const-string v3, "MiuiInitServer"

    invoke-static {v3, v2}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 508
    .end local v0    # "e":Ljava/lang/Exception;
    :cond_2
    :goto_1
    iget-object v0, p0, Lcom/miui/server/MiuiInitServer;->mPreinstallHistoryMap:Ljava/util/HashMap;

    invoke-virtual {v0, p1}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    if-nez v0, :cond_3

    goto :goto_2

    :cond_3
    iget-object v0, p0, Lcom/miui/server/MiuiInitServer;->mPreinstallHistoryMap:Ljava/util/HashMap;

    invoke-virtual {v0, p1}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    move-object v1, v0

    check-cast v1, Ljava/lang/String;

    :goto_2
    return-object v1
.end method

.method public getNotchConfig(Ljava/lang/String;)I
    .locals 3
    .param p1, "pkg"    # Ljava/lang/String;

    .line 541
    sget-boolean v0, Lmiui/util/CustomizeUtil;->HAS_NOTCH:Z

    if-nez v0, :cond_0

    .line 542
    const/4 v0, 0x0

    return v0

    .line 544
    :cond_0
    invoke-static {}, Landroid/os/Binder;->clearCallingIdentity()J

    move-result-wide v0

    .line 546
    .local v0, "identity":J
    :try_start_0
    iget-object v2, p0, Lcom/miui/server/MiuiInitServer;->mCompatModePackages:Lcom/miui/server/MiuiCompatModePackages;

    invoke-virtual {v2, p1}, Lcom/miui/server/MiuiCompatModePackages;->getNotchConfig(Ljava/lang/String;)I

    move-result v2
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 548
    invoke-static {v0, v1}, Landroid/os/Binder;->restoreCallingIdentity(J)V

    .line 546
    return v2

    .line 548
    :catchall_0
    move-exception v2

    invoke-static {v0, v1}, Landroid/os/Binder;->restoreCallingIdentity(J)V

    .line 549
    throw v2
.end method

.method public getPackageEventRecords(I)Landroid/os/Bundle;
    .locals 1
    .param p1, "eventType"    # I

    .line 602
    const-class v0, Lcom/android/server/pm/PackageEventRecorderInternal;

    invoke-static {v0}, Lcom/android/server/LocalServices;->getService(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/android/server/pm/PackageEventRecorderInternal;

    .line 603
    invoke-interface {v0, p1}, Lcom/android/server/pm/PackageEventRecorderInternal;->getPackageEventRecords(I)Landroid/os/Bundle;

    move-result-object v0

    .line 602
    return-object v0
.end method

.method public getPreinstalledAppVersion(Ljava/lang/String;)I
    .locals 1
    .param p1, "pkg"    # Ljava/lang/String;

    .line 480
    invoke-static {p1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 481
    const/4 v0, -0x1

    return v0

    .line 483
    :cond_0
    iget-object v0, p0, Lcom/miui/server/MiuiInitServer;->mMiuiPreinstallHelper:Lcom/android/server/pm/MiuiPreinstallHelper;

    invoke-virtual {v0}, Lcom/android/server/pm/MiuiPreinstallHelper;->isSupportNewFrame()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 484
    iget-object v0, p0, Lcom/miui/server/MiuiInitServer;->mMiuiPreinstallHelper:Lcom/android/server/pm/MiuiPreinstallHelper;

    invoke-virtual {v0, p1}, Lcom/android/server/pm/MiuiPreinstallHelper;->getPreinstallAppVersion(Ljava/lang/String;)I

    move-result v0

    return v0

    .line 486
    :cond_1
    invoke-static {p1}, Lcom/android/server/pm/PreinstallApp;->getPreinstalledAppVersion(Ljava/lang/String;)I

    move-result v0

    return v0
.end method

.method public initCustEnvironment(Ljava/lang/String;Lmiui/os/IMiuiInitObserver;)Z
    .locals 4
    .param p1, "custVariant"    # Ljava/lang/String;
    .param p2, "obs"    # Lmiui/os/IMiuiInitObserver;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    .line 294
    iget-object v0, p0, Lcom/miui/server/MiuiInitServer;->mContext:Landroid/content/Context;

    const-string v1, "miui.os.permisson.INIT_MIUI_ENVIRONMENT"

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Landroid/content/Context;->enforceCallingOrSelfPermission(Ljava/lang/String;Ljava/lang/String;)V

    .line 296
    const-string v0, "MiuiInitServer"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "check status, cust variant["

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "]"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Slog;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 297
    monitor-enter p0

    .line 298
    :try_start_0
    iget-boolean v0, p0, Lcom/miui/server/MiuiInitServer;->mDoing:Z

    const/4 v1, 0x0

    if-eqz v0, :cond_0

    .line 299
    const-string v0, "MiuiInitServer"

    const-string/jumbo v2, "skip, initializing cust environment"

    invoke-static {v0, v2}, Landroid/util/Slog;->w(Ljava/lang/String;Ljava/lang/String;)I

    .line 300
    monitor-exit p0

    return v1

    .line 302
    :cond_0
    invoke-static {p1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 303
    const-string v0, "MiuiInitServer"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v3, "skip, cust variant["

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, "] is empty"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v0, v2}, Landroid/util/Slog;->w(Ljava/lang/String;Ljava/lang/String;)I

    .line 304
    monitor-exit p0

    return v1

    .line 306
    :cond_1
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/miui/server/MiuiInitServer;->mDoing:Z

    .line 307
    const-string v1, "MiuiInitServer"

    const-string v2, "initializing cust environment"

    invoke-static {v1, v2}, Landroid/util/Slog;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 308
    new-instance v1, Lcom/miui/server/MiuiInitServer$InitCustEnvironmentTask;

    invoke-direct {v1, p0, p1, p2}, Lcom/miui/server/MiuiInitServer$InitCustEnvironmentTask;-><init>(Lcom/miui/server/MiuiInitServer;Ljava/lang/String;Lmiui/os/IMiuiInitObserver;)V

    invoke-virtual {v1}, Lcom/miui/server/MiuiInitServer$InitCustEnvironmentTask;->start()V

    .line 309
    monitor-exit p0

    return v0

    .line 310
    :catchall_0
    move-exception v0

    monitor-exit p0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method

.method public installPreinstallApp()V
    .locals 3

    .line 316
    iget-object v0, p0, Lcom/miui/server/MiuiInitServer;->mContext:Landroid/content/Context;

    const-string v1, "miui.os.permisson.INIT_MIUI_ENVIRONMENT"

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Landroid/content/Context;->enforceCallingOrSelfPermission(Ljava/lang/String;Ljava/lang/String;)V

    .line 318
    iget-object v0, p0, Lcom/miui/server/MiuiInitServer;->mMiuiPreinstallHelper:Lcom/android/server/pm/MiuiPreinstallHelper;

    invoke-virtual {v0}, Lcom/android/server/pm/MiuiPreinstallHelper;->isSupportNewFrame()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 319
    iget-object v0, p0, Lcom/miui/server/MiuiInitServer;->mMiuiPreinstallHelper:Lcom/android/server/pm/MiuiPreinstallHelper;

    invoke-virtual {v0}, Lcom/android/server/pm/MiuiPreinstallHelper;->installCustApps()V

    goto :goto_0

    .line 321
    :cond_0
    iget-object v0, p0, Lcom/miui/server/MiuiInitServer;->mContext:Landroid/content/Context;

    invoke-static {v0}, Lcom/android/server/pm/PreinstallApp;->installCustApps(Landroid/content/Context;)V

    .line 323
    :goto_0
    return-void
.end method

.method public isPreinstalledPAIPackage(Ljava/lang/String;)Z
    .locals 1
    .param p1, "pkg"    # Ljava/lang/String;

    .line 380
    invoke-static {p1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 381
    const/4 v0, 0x0

    return v0

    .line 383
    :cond_0
    iget-object v0, p0, Lcom/miui/server/MiuiInitServer;->mMiuiPreinstallHelper:Lcom/android/server/pm/MiuiPreinstallHelper;

    invoke-virtual {v0}, Lcom/android/server/pm/MiuiPreinstallHelper;->isSupportNewFrame()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 384
    invoke-static {p1}, Lcom/android/server/pm/MiuiPAIPreinstallConfig;->isPreinstalledPAIPackage(Ljava/lang/String;)Z

    move-result v0

    return v0

    .line 386
    :cond_1
    invoke-static {p1}, Lcom/android/server/pm/PreinstallApp;->isPreinstalledPAIPackage(Ljava/lang/String;)Z

    move-result v0

    return v0
.end method

.method public isPreinstalledPackage(Ljava/lang/String;)Z
    .locals 1
    .param p1, "pkg"    # Ljava/lang/String;

    .line 368
    invoke-static {p1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 369
    const/4 v0, 0x0

    return v0

    .line 371
    :cond_0
    iget-object v0, p0, Lcom/miui/server/MiuiInitServer;->mMiuiPreinstallHelper:Lcom/android/server/pm/MiuiPreinstallHelper;

    invoke-virtual {v0}, Lcom/android/server/pm/MiuiPreinstallHelper;->isSupportNewFrame()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 372
    iget-object v0, p0, Lcom/miui/server/MiuiInitServer;->mMiuiPreinstallHelper:Lcom/android/server/pm/MiuiPreinstallHelper;

    invoke-virtual {v0, p1}, Lcom/android/server/pm/MiuiPreinstallHelper;->isPreinstalledPackage(Ljava/lang/String;)Z

    move-result v0

    return v0

    .line 374
    :cond_1
    invoke-static {p1}, Lcom/android/server/pm/PreinstallApp;->isPreinstalledPackage(Ljava/lang/String;)Z

    move-result v0

    return v0
.end method

.method public isRestrictAspect(Ljava/lang/String;)Z
    .locals 1
    .param p1, "packageName"    # Ljava/lang/String;

    .line 520
    iget-boolean v0, p0, Lcom/miui/server/MiuiInitServer;->mNeedAspectSettings:Z

    if-nez v0, :cond_0

    .line 521
    const/4 v0, 0x1

    return v0

    .line 523
    :cond_0
    iget-object v0, p0, Lcom/miui/server/MiuiInitServer;->mCompatModePackages:Lcom/miui/server/MiuiCompatModePackages;

    invoke-virtual {v0, p1}, Lcom/miui/server/MiuiCompatModePackages;->isRestrictAspect(Ljava/lang/String;)Z

    move-result v0

    return v0
.end method

.method public removeFromPreinstallList(Ljava/lang/String;)V
    .locals 3
    .param p1, "pkg"    # Ljava/lang/String;

    .line 416
    iget-object v0, p0, Lcom/miui/server/MiuiInitServer;->mContext:Landroid/content/Context;

    const-string v1, "miui.os.permisson.INIT_MIUI_ENVIRONMENT"

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Landroid/content/Context;->enforceCallingOrSelfPermission(Ljava/lang/String;Ljava/lang/String;)V

    .line 419
    invoke-static {p1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 420
    return-void

    .line 423
    :cond_0
    invoke-static {p1}, Lcom/android/server/pm/PreinstallApp;->removeFromPreinstallList(Ljava/lang/String;)V

    .line 424
    return-void
.end method

.method public removeFromPreinstallPAIList(Ljava/lang/String;)V
    .locals 3
    .param p1, "pkg"    # Ljava/lang/String;

    .line 429
    iget-object v0, p0, Lcom/miui/server/MiuiInitServer;->mContext:Landroid/content/Context;

    const-string v1, "miui.os.permisson.INIT_MIUI_ENVIRONMENT"

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Landroid/content/Context;->enforceCallingOrSelfPermission(Ljava/lang/String;Ljava/lang/String;)V

    .line 432
    invoke-static {p1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 433
    return-void

    .line 436
    :cond_0
    iget-object v0, p0, Lcom/miui/server/MiuiInitServer;->mMiuiPreinstallHelper:Lcom/android/server/pm/MiuiPreinstallHelper;

    invoke-virtual {v0}, Lcom/android/server/pm/MiuiPreinstallHelper;->isSupportNewFrame()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 437
    invoke-static {p1}, Lcom/android/server/pm/MiuiPAIPreinstallConfig;->removeFromPreinstallPAIList(Ljava/lang/String;)V

    goto :goto_0

    .line 439
    :cond_1
    invoke-static {p1}, Lcom/android/server/pm/PreinstallApp;->removeFromPreinstallPAIList(Ljava/lang/String;)V

    .line 441
    :goto_0
    return-void
.end method

.method public setCutoutMode(Ljava/lang/String;I)V
    .locals 1
    .param p1, "pkg"    # Ljava/lang/String;
    .param p2, "mode"    # I

    .line 561
    sget-boolean v0, Lmiui/util/CustomizeUtil;->HAS_NOTCH:Z

    if-nez v0, :cond_0

    .line 562
    return-void

    .line 564
    :cond_0
    const-string/jumbo v0, "setCutoutMode"

    invoke-direct {p0, v0}, Lcom/miui/server/MiuiInitServer;->checkPermission(Ljava/lang/String;)V

    .line 565
    iget-object v0, p0, Lcom/miui/server/MiuiInitServer;->mCompatModePackages:Lcom/miui/server/MiuiCompatModePackages;

    invoke-virtual {v0, p1, p2}, Lcom/miui/server/MiuiCompatModePackages;->setCutoutMode(Ljava/lang/String;I)V

    .line 566
    return-void
.end method

.method public setNotchSpecialMode(Ljava/lang/String;Z)V
    .locals 1
    .param p1, "pkg"    # Ljava/lang/String;
    .param p2, "special"    # Z

    .line 553
    sget-boolean v0, Lmiui/util/CustomizeUtil;->HAS_NOTCH:Z

    if-nez v0, :cond_0

    .line 554
    return-void

    .line 556
    :cond_0
    const-string/jumbo v0, "setNotchSpecialMode"

    invoke-direct {p0, v0}, Lcom/miui/server/MiuiInitServer;->checkPermission(Ljava/lang/String;)V

    .line 557
    iget-object v0, p0, Lcom/miui/server/MiuiInitServer;->mCompatModePackages:Lcom/miui/server/MiuiCompatModePackages;

    invoke-virtual {v0, p1, p2}, Lcom/miui/server/MiuiCompatModePackages;->setNotchSpecialMode(Ljava/lang/String;Z)V

    .line 558
    return-void
.end method

.method public setRestrictAspect(Ljava/lang/String;Z)V
    .locals 1
    .param p1, "pkg"    # Ljava/lang/String;
    .param p2, "restrict"    # Z

    .line 512
    iget-boolean v0, p0, Lcom/miui/server/MiuiInitServer;->mNeedAspectSettings:Z

    if-nez v0, :cond_0

    .line 513
    return-void

    .line 515
    :cond_0
    const-string/jumbo v0, "setRestrictAspect"

    invoke-direct {p0, v0}, Lcom/miui/server/MiuiInitServer;->checkPermission(Ljava/lang/String;)V

    .line 516
    iget-object v0, p0, Lcom/miui/server/MiuiInitServer;->mCompatModePackages:Lcom/miui/server/MiuiCompatModePackages;

    invoke-virtual {v0, p1, p2}, Lcom/miui/server/MiuiCompatModePackages;->setRestrictAspect(Ljava/lang/String;Z)V

    .line 517
    return-void
.end method

.method public writePreinstallPAIPackage(Ljava/lang/String;)V
    .locals 3
    .param p1, "pkg"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    .line 446
    iget-object v0, p0, Lcom/miui/server/MiuiInitServer;->mContext:Landroid/content/Context;

    const-string v1, "miui.os.permisson.INIT_MIUI_ENVIRONMENT"

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Landroid/content/Context;->enforceCallingOrSelfPermission(Ljava/lang/String;Ljava/lang/String;)V

    .line 449
    invoke-static {p1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 450
    return-void

    .line 453
    :cond_0
    iget-object v0, p0, Lcom/miui/server/MiuiInitServer;->mMiuiPreinstallHelper:Lcom/android/server/pm/MiuiPreinstallHelper;

    invoke-virtual {v0}, Lcom/android/server/pm/MiuiPreinstallHelper;->isSupportNewFrame()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 454
    invoke-static {p1}, Lcom/android/server/pm/MiuiPAIPreinstallConfig;->writePreinstallPAIPackage(Ljava/lang/String;)V

    goto :goto_0

    .line 456
    :cond_1
    invoke-static {p1}, Lcom/android/server/pm/PreinstallApp;->writePreinstallPAIPackage(Ljava/lang/String;)V

    .line 458
    :goto_0
    return-void
.end method
