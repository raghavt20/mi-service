public class com.miui.server.MiuiSwapService$UsbStatusReceiver extends android.content.BroadcastReceiver {
	 /* .source "MiuiSwapService.java" */
	 /* # annotations */
	 /* .annotation system Ldalvik/annotation/EnclosingClass; */
	 /* value = Lcom/miui/server/MiuiSwapService; */
} // .end annotation
/* .annotation system Ldalvik/annotation/InnerClass; */
/* accessFlags = 0x1 */
/* name = "UsbStatusReceiver" */
} // .end annotation
/* # instance fields */
final com.miui.server.MiuiSwapService this$0; //synthetic
/* # direct methods */
public com.miui.server.MiuiSwapService$UsbStatusReceiver ( ) {
/* .locals 0 */
/* .param p1, "this$0" # Lcom/miui/server/MiuiSwapService; */
/* .line 270 */
this.this$0 = p1;
/* invoke-direct {p0}, Landroid/content/BroadcastReceiver;-><init>()V */
return;
} // .end method
/* # virtual methods */
public void onReceive ( android.content.Context p0, android.content.Intent p1 ) {
/* .locals 4 */
/* .param p1, "context" # Landroid/content/Context; */
/* .param p2, "intent" # Landroid/content/Intent; */
/* .line 274 */
if ( p2 != null) { // if-eqz p2, :cond_3
	 /* sget-boolean v0, Lcom/miui/server/MiuiSwapService;->SWAP_DEBUG:Z */
	 if ( v0 != null) { // if-eqz v0, :cond_0
		 /* .line 278 */
	 } // :cond_0
	 (( android.content.Intent ) p2 ).getAction ( ); // invoke-virtual {p2}, Landroid/content/Intent;->getAction()Ljava/lang/String;
	 /* .line 279 */
	 /* .local v0, "action":Ljava/lang/String; */
	 final String v1 = "android.hardware.usb.action.USB_STATE"; // const-string v1, "android.hardware.usb.action.USB_STATE"
	 v1 = 	 (( java.lang.String ) v1 ).equals ( v0 ); // invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z
	 if ( v1 != null) { // if-eqz v1, :cond_2
		 /* .line 280 */
		 (( android.content.Intent ) p2 ).getExtras ( ); // invoke-virtual {p2}, Landroid/content/Intent;->getExtras()Landroid/os/Bundle;
		 final String v2 = "connected"; // const-string v2, "connected"
		 v1 = 		 (( android.os.Bundle ) v1 ).getBoolean ( v2 ); // invoke-virtual {v1, v2}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;)Z
		 /* .line 281 */
		 /* .local v1, "connected":Z */
		 if ( v1 != null) { // if-eqz v1, :cond_1
			 /* .line 282 */
			 v2 = this.this$0;
			 int v3 = 1; // const/4 v3, 0x1
			 com.miui.server.MiuiSwapService .-$$Nest$fputmUsbConnect ( v2,v3 );
			 /* .line 283 */
			 com.miui.server.MiuiSwapService .-$$Nest$sfgetTAG ( );
			 final String v3 = "Usb connect"; // const-string v3, "Usb connect"
			 android.util.Slog .d ( v2,v3 );
			 /* .line 285 */
		 } // :cond_1
		 v2 = this.this$0;
		 int v3 = 0; // const/4 v3, 0x0
		 com.miui.server.MiuiSwapService .-$$Nest$fputmUsbConnect ( v2,v3 );
		 /* .line 286 */
		 com.miui.server.MiuiSwapService .-$$Nest$sfgetTAG ( );
		 final String v3 = "Usb disconnect"; // const-string v3, "Usb disconnect"
		 android.util.Slog .d ( v2,v3 );
		 /* .line 289 */
	 } // .end local v1 # "connected":Z
} // :cond_2
} // :goto_0
return;
/* .line 275 */
} // .end local v0 # "action":Ljava/lang/String;
} // :cond_3
} // :goto_1
com.miui.server.MiuiSwapService .-$$Nest$sfgetTAG ( );
final String v1 = "intent is null or debug"; // const-string v1, "intent is null or debug"
android.util.Slog .w ( v0,v1 );
/* .line 276 */
return;
} // .end method
