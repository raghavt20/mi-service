.class final Lcom/miui/server/MiuiFboService$FboServiceHandler;
.super Landroid/os/Handler;
.source "MiuiFboService.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/miui/server/MiuiFboService;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x12
    name = "FboServiceHandler"
.end annotation


# instance fields
.field final synthetic this$0:Lcom/miui/server/MiuiFboService;


# direct methods
.method public constructor <init>(Lcom/miui/server/MiuiFboService;Landroid/os/Looper;)V
    .locals 0
    .param p2, "looper"    # Landroid/os/Looper;

    .line 145
    iput-object p1, p0, Lcom/miui/server/MiuiFboService$FboServiceHandler;->this$0:Lcom/miui/server/MiuiFboService;

    .line 146
    const/4 p1, 0x0

    invoke-direct {p0, p2, p1}, Landroid/os/Handler;-><init>(Landroid/os/Looper;Landroid/os/Handler$Callback;)V

    .line 147
    return-void
.end method


# virtual methods
.method public handleMessage(Landroid/os/Message;)V
    .locals 17
    .param p1, "msg"    # Landroid/os/Message;

    .line 151
    move-object/from16 v1, p0

    move-object/from16 v2, p1

    iget v0, v2, Landroid/os/Message;->what:I

    const-string v3, "/"

    const-string v8, "miui.intent.action.startAgain"

    const-string v9, "do not meet the conditions exit"

    const-string v10, ",screenOnTimes:"

    const-string v11, ",batteryStatus:"

    const-string v12, "screenStatus:"

    const-string v14, "screenOnTimes"

    const-string v15, "current batteryTemperature"

    const-string v13, "current batteryLevel"

    const-string v5, "current batteryStatus"

    const-string v4, "current screenOn"

    const-string v6, ",batteryTemperature:"

    const-string v7, ",batteryLevel:"

    const-string/jumbo v2, "stop"

    move-object/from16 v16, v10

    const/4 v10, 0x0

    packed-switch v0, :pswitch_data_0

    .line 293
    invoke-static {}, Lcom/miui/server/MiuiFboService;->-$$Nest$sfgetTAG()Ljava/lang/String;

    move-result-object v0

    const-string v2, "Unrecognized message command"

    invoke-static {v0, v2}, Landroid/util/Slog;->d(Ljava/lang/String;Ljava/lang/String;)I

    goto/16 :goto_2

    .line 283
    :pswitch_0
    :try_start_0
    new-instance v0, Ljava/io/File;

    const-string v2, "/sys/class/android_usb/android0/state"

    invoke-direct {v0, v2}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    const-string v2, ""

    const/16 v3, 0x80

    invoke-static {v0, v3, v2}, Landroid/os/FileUtils;->readTextFile(Ljava/io/File;ILjava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 284
    .local v0, "state":Ljava/lang/String;
    iget-object v2, v1, Lcom/miui/server/MiuiFboService$FboServiceHandler;->this$0:Lcom/miui/server/MiuiFboService;

    const-string v3, "CONFIGURED"

    invoke-virtual {v0}, Ljava/lang/String;->trim()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    invoke-virtual {v2, v3}, Lcom/miui/server/MiuiFboService;->setUsbState(Z)V

    .line 285
    invoke-static {}, Lcom/miui/server/MiuiFboService;->-$$Nest$sfgetTAG()Ljava/lang/String;

    move-result-object v2

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "USBState:"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    iget-object v4, v1, Lcom/miui/server/MiuiFboService$FboServiceHandler;->this$0:Lcom/miui/server/MiuiFboService;

    invoke-virtual {v4}, Lcom/miui/server/MiuiFboService;->getUsbState()Z

    move-result v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Landroid/util/Slog;->d(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 289
    nop

    .end local v0    # "state":Ljava/lang/String;
    goto/16 :goto_2

    .line 286
    :catch_0
    move-exception v0

    .line 287
    .local v0, "e":Ljava/lang/Exception;
    invoke-static {}, Lcom/miui/server/MiuiFboService;->-$$Nest$sfgetTAG()Ljava/lang/String;

    move-result-object v2

    const-string v3, "Failed to determine if device was on USB"

    invoke-static {v2, v3, v0}, Landroid/util/Slog;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    .line 288
    iget-object v2, v1, Lcom/miui/server/MiuiFboService$FboServiceHandler;->this$0:Lcom/miui/server/MiuiFboService;

    const/4 v3, 0x1

    invoke-virtual {v2, v3}, Lcom/miui/server/MiuiFboService;->setUsbState(Z)V

    .line 290
    .end local v0    # "e":Ljava/lang/Exception;
    goto/16 :goto_2

    .line 268
    :pswitch_1
    :try_start_1
    invoke-static {v10}, Lcom/miui/server/MiuiFboService;->-$$Nest$smuseCldStrategy(I)V

    .line 269
    iget-object v0, v1, Lcom/miui/server/MiuiFboService$FboServiceHandler;->this$0:Lcom/miui/server/MiuiFboService;

    invoke-static {v0}, Lcom/miui/server/MiuiFboService;->-$$Nest$fgetmFboNativeService(Lcom/miui/server/MiuiFboService;)Lmiui/fbo/IFbo;

    move-result-object v3

    invoke-interface {v3, v2}, Lmiui/fbo/IFbo;->FBO_stateCtl(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    invoke-static {v0, v3}, Lcom/miui/server/MiuiFboService;->-$$Nest$fputstagingData(Lcom/miui/server/MiuiFboService;Ljava/lang/String;)V

    .line 270
    invoke-static {}, Lcom/miui/server/MiuiFboService;->-$$Nest$sfgetTAG()Ljava/lang/String;

    move-result-object v0

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v4, "stopDueTobatteryTemperature && batteryTemperatureStagingData:"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    iget-object v4, v1, Lcom/miui/server/MiuiFboService$FboServiceHandler;->this$0:Lcom/miui/server/MiuiFboService;

    invoke-static {v4}, Lcom/miui/server/MiuiFboService;->-$$Nest$fgetstagingData(Lcom/miui/server/MiuiFboService;)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v0, v3}, Landroid/util/Slog;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 271
    iget-object v0, v1, Lcom/miui/server/MiuiFboService$FboServiceHandler;->this$0:Lcom/miui/server/MiuiFboService;

    invoke-static {v0, v2}, Lcom/miui/server/MiuiFboService;->-$$Nest$msendStopOrContinueToHal(Lcom/miui/server/MiuiFboService;Ljava/lang/String;)V

    .line 272
    iget-object v0, v1, Lcom/miui/server/MiuiFboService$FboServiceHandler;->this$0:Lcom/miui/server/MiuiFboService;

    invoke-virtual {v0, v10}, Lcom/miui/server/MiuiFboService;->setNativeIsRunning(Z)V

    .line 273
    invoke-static {}, Lcom/miui/server/MiuiFboService;->-$$Nest$sfgetmStopReason()Ljava/util/ArrayList;

    move-result-object v0

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v3, "stop because battery,batteryStatus:"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget-object v3, v1, Lcom/miui/server/MiuiFboService$FboServiceHandler;->this$0:Lcom/miui/server/MiuiFboService;

    invoke-static {v3}, Lcom/miui/server/MiuiFboService;->-$$Nest$fgetbatteryStatus(Lcom/miui/server/MiuiFboService;)I

    move-result v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget-object v3, v1, Lcom/miui/server/MiuiFboService$FboServiceHandler;->this$0:Lcom/miui/server/MiuiFboService;

    invoke-static {v3}, Lcom/miui/server/MiuiFboService;->-$$Nest$fgetbatteryLevel(Lcom/miui/server/MiuiFboService;)I

    move-result v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget-object v3, v1, Lcom/miui/server/MiuiFboService$FboServiceHandler;->this$0:Lcom/miui/server/MiuiFboService;

    invoke-static {v3}, Lcom/miui/server/MiuiFboService;->-$$Nest$fgetbatteryTemperature(Lcom/miui/server/MiuiFboService;)I

    move-result v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v2}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_1

    .line 278
    goto/16 :goto_2

    .line 276
    :catch_1
    move-exception v0

    .line 277
    .restart local v0    # "e":Ljava/lang/Exception;
    invoke-static {}, Lcom/miui/server/MiuiFboService;->-$$Nest$sfgetTAG()Ljava/lang/String;

    move-result-object v2

    const-string v3, "fail to execute STOP_DUETO_BATTERYTEMPERATURE"

    invoke-static {v2, v3}, Landroid/util/Slog;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 279
    .end local v0    # "e":Ljava/lang/Exception;
    goto/16 :goto_2

    .line 252
    :pswitch_2
    :try_start_2
    invoke-static {v10}, Lcom/miui/server/MiuiFboService;->-$$Nest$smuseCldStrategy(I)V

    .line 253
    iget-object v0, v1, Lcom/miui/server/MiuiFboService$FboServiceHandler;->this$0:Lcom/miui/server/MiuiFboService;

    invoke-static {v0}, Lcom/miui/server/MiuiFboService;->-$$Nest$fgetmFboNativeService(Lcom/miui/server/MiuiFboService;)Lmiui/fbo/IFbo;

    move-result-object v3

    invoke-interface {v3, v2}, Lmiui/fbo/IFbo;->FBO_stateCtl(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    invoke-static {v0, v3}, Lcom/miui/server/MiuiFboService;->-$$Nest$fputstagingData(Lcom/miui/server/MiuiFboService;Ljava/lang/String;)V

    .line 254
    invoke-static {}, Lcom/miui/server/MiuiFboService;->-$$Nest$sfgetTAG()Ljava/lang/String;

    move-result-object v0

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v4, "stopDueToScreen && sreenStagingData:"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    iget-object v4, v1, Lcom/miui/server/MiuiFboService$FboServiceHandler;->this$0:Lcom/miui/server/MiuiFboService;

    invoke-static {v4}, Lcom/miui/server/MiuiFboService;->-$$Nest$fgetstagingData(Lcom/miui/server/MiuiFboService;)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v0, v3}, Landroid/util/Slog;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 255
    iget-object v0, v1, Lcom/miui/server/MiuiFboService$FboServiceHandler;->this$0:Lcom/miui/server/MiuiFboService;

    invoke-static {v0, v2}, Lcom/miui/server/MiuiFboService;->-$$Nest$msendStopOrContinueToHal(Lcom/miui/server/MiuiFboService;Ljava/lang/String;)V

    .line 256
    iget-object v0, v1, Lcom/miui/server/MiuiFboService$FboServiceHandler;->this$0:Lcom/miui/server/MiuiFboService;

    invoke-virtual {v0, v10}, Lcom/miui/server/MiuiFboService;->setNativeIsRunning(Z)V

    .line 257
    iget-object v0, v1, Lcom/miui/server/MiuiFboService$FboServiceHandler;->this$0:Lcom/miui/server/MiuiFboService;

    const/4 v2, 0x1

    invoke-virtual {v0, v2}, Lcom/miui/server/MiuiFboService;->setDueToScreenWait(Z)V

    .line 258
    invoke-static {}, Lcom/miui/server/MiuiFboService;->-$$Nest$sfgetsInstance()Lcom/miui/server/MiuiFboService;

    const/4 v0, 0x0

    const-wide/32 v2, 0x927c0

    invoke-static {v8, v0, v2, v3, v10}, Lcom/miui/server/MiuiFboService;->-$$Nest$smsetAlarm(Ljava/lang/String;Ljava/lang/String;JZ)V

    .line 259
    iget-object v0, v1, Lcom/miui/server/MiuiFboService$FboServiceHandler;->this$0:Lcom/miui/server/MiuiFboService;

    invoke-static {v0, v10}, Lcom/miui/server/MiuiFboService;->-$$Nest$fputscreenOnTimes(Lcom/miui/server/MiuiFboService;I)V

    .line 260
    invoke-static {}, Lcom/miui/server/MiuiFboService;->-$$Nest$sfgetmStopReason()Ljava/util/ArrayList;

    move-result-object v0

    const-string/jumbo v2, "stop because screen on"

    invoke-virtual {v0, v2}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z
    :try_end_2
    .catch Ljava/lang/Exception; {:try_start_2 .. :try_end_2} :catch_2

    .line 263
    goto/16 :goto_2

    .line 261
    :catch_2
    move-exception v0

    .line 262
    .restart local v0    # "e":Ljava/lang/Exception;
    invoke-static {}, Lcom/miui/server/MiuiFboService;->-$$Nest$sfgetTAG()Ljava/lang/String;

    move-result-object v2

    const-string v3, "fail to execute STOP_DUETO_SCREEN"

    invoke-static {v2, v3}, Landroid/util/Slog;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 264
    .end local v0    # "e":Ljava/lang/Exception;
    goto/16 :goto_2

    .line 229
    :pswitch_3
    :try_start_3
    invoke-static {}, Lcom/miui/server/MiuiFboService;->-$$Nest$sfgetmFinishedApp()Z

    move-result v0

    if-nez v0, :cond_0

    .line 230
    const/4 v3, 0x1

    invoke-static {v3}, Lcom/miui/server/MiuiFboService;->-$$Nest$smuseCldStrategy(I)V

    .line 232
    :cond_0
    iget-object v0, v1, Lcom/miui/server/MiuiFboService$FboServiceHandler;->this$0:Lcom/miui/server/MiuiFboService;

    invoke-static {v0}, Lcom/miui/server/MiuiFboService;->-$$Nest$mreportFboEvent(Lcom/miui/server/MiuiFboService;)V

    .line 233
    invoke-static {v10}, Lcom/miui/server/MiuiFboService;->-$$Nest$sfputmFinishedApp(Z)V

    .line 234
    iget-object v0, v1, Lcom/miui/server/MiuiFboService$FboServiceHandler;->this$0:Lcom/miui/server/MiuiFboService;

    invoke-static {v0}, Lcom/miui/server/MiuiFboService;->-$$Nest$fgetmFboNativeService(Lcom/miui/server/MiuiFboService;)Lmiui/fbo/IFbo;

    move-result-object v0

    invoke-interface {v0, v2}, Lmiui/fbo/IFbo;->FBO_stateCtl(Ljava/lang/String;)Ljava/lang/String;

    .line 235
    iget-object v0, v1, Lcom/miui/server/MiuiFboService$FboServiceHandler;->this$0:Lcom/miui/server/MiuiFboService;

    invoke-static {v0, v2}, Lcom/miui/server/MiuiFboService;->-$$Nest$msendStopOrContinueToHal(Lcom/miui/server/MiuiFboService;Ljava/lang/String;)V

    .line 236
    iget-object v0, v1, Lcom/miui/server/MiuiFboService$FboServiceHandler;->this$0:Lcom/miui/server/MiuiFboService;

    invoke-virtual {v0, v10}, Lcom/miui/server/MiuiFboService;->setNativeIsRunning(Z)V

    .line 237
    iget-object v0, v1, Lcom/miui/server/MiuiFboService$FboServiceHandler;->this$0:Lcom/miui/server/MiuiFboService;

    invoke-virtual {v0, v10}, Lcom/miui/server/MiuiFboService;->setGlobalSwitch(Z)V

    .line 238
    invoke-static {}, Lcom/miui/server/MiuiFboService;->-$$Nest$smclearBroadcastData()V

    .line 239
    invoke-static {}, Lcom/miui/server/MiuiFboService;->-$$Nest$sfgetsInstance()Lcom/miui/server/MiuiFboService;

    move-result-object v0

    invoke-static {v0}, Lcom/miui/server/MiuiFboService;->-$$Nest$muserAreaExtend(Lcom/miui/server/MiuiFboService;)V

    .line 240
    const-string v0, "persist.sys.fboservice.ctrl"

    const-string v2, "false"

    invoke-static {v0, v2}, Landroid/os/SystemProperties;->set(Ljava/lang/String;Ljava/lang/String;)V

    .line 241
    iget-object v0, v1, Lcom/miui/server/MiuiFboService$FboServiceHandler;->this$0:Lcom/miui/server/MiuiFboService;

    invoke-static {v0}, Lcom/miui/server/MiuiFboService;->-$$Nest$fgetmFboServiceHandler(Lcom/miui/server/MiuiFboService;)Lcom/miui/server/MiuiFboService$FboServiceHandler;

    move-result-object v0

    const/4 v2, 0x0

    invoke-virtual {v0, v2}, Lcom/miui/server/MiuiFboService$FboServiceHandler;->removeCallbacksAndMessages(Ljava/lang/Object;)V

    .line 242
    iget-object v0, v1, Lcom/miui/server/MiuiFboService$FboServiceHandler;->this$0:Lcom/miui/server/MiuiFboService;

    invoke-virtual {v0, v10}, Lcom/miui/server/MiuiFboService;->setEnableNightJudgment(Z)V

    .line 243
    iget-object v0, v1, Lcom/miui/server/MiuiFboService$FboServiceHandler;->this$0:Lcom/miui/server/MiuiFboService;

    invoke-static {v0, v10}, Lcom/miui/server/MiuiFboService;->-$$Nest$fputmessageHasbeenSent(Lcom/miui/server/MiuiFboService;Z)V

    .line 244
    invoke-static {}, Lcom/miui/server/MiuiFboService;->-$$Nest$sfgetTAG()Ljava/lang/String;

    move-result-object v0

    const-string v2, "All stop,exit"

    invoke-static {v0, v2}, Landroid/util/Slog;->d(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_3
    .catch Ljava/lang/Exception; {:try_start_3 .. :try_end_3} :catch_3

    .line 247
    goto/16 :goto_2

    .line 245
    :catch_3
    move-exception v0

    .line 246
    .restart local v0    # "e":Ljava/lang/Exception;
    invoke-static {}, Lcom/miui/server/MiuiFboService;->-$$Nest$sfgetTAG()Ljava/lang/String;

    move-result-object v2

    const-string v3, "fail to execute STOP_FBO"

    invoke-static {v2, v3}, Landroid/util/Slog;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 248
    .end local v0    # "e":Ljava/lang/Exception;
    goto/16 :goto_2

    .line 194
    :pswitch_4
    invoke-static {}, Lcom/miui/server/MiuiFboService;->-$$Nest$sfgetTAG()Ljava/lang/String;

    move-result-object v0

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v2, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget-object v4, v1, Lcom/miui/server/MiuiFboService$FboServiceHandler;->this$0:Lcom/miui/server/MiuiFboService;

    invoke-static {v4}, Lcom/miui/server/MiuiFboService;->-$$Nest$fgetscreenOn(Lcom/miui/server/MiuiFboService;)Z

    move-result v4

    invoke-virtual {v2, v4}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget-object v4, v1, Lcom/miui/server/MiuiFboService$FboServiceHandler;->this$0:Lcom/miui/server/MiuiFboService;

    invoke-static {v4}, Lcom/miui/server/MiuiFboService;->-$$Nest$fgetbatteryStatus(Lcom/miui/server/MiuiFboService;)I

    move-result v4

    invoke-virtual {v2, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v13}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget-object v4, v1, Lcom/miui/server/MiuiFboService$FboServiceHandler;->this$0:Lcom/miui/server/MiuiFboService;

    invoke-static {v4}, Lcom/miui/server/MiuiFboService;->-$$Nest$fgetbatteryLevel(Lcom/miui/server/MiuiFboService;)I

    move-result v4

    invoke-virtual {v2, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v15}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget-object v4, v1, Lcom/miui/server/MiuiFboService$FboServiceHandler;->this$0:Lcom/miui/server/MiuiFboService;

    invoke-static {v4}, Lcom/miui/server/MiuiFboService;->-$$Nest$fgetbatteryTemperature(Lcom/miui/server/MiuiFboService;)I

    move-result v4

    invoke-virtual {v2, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v14}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget-object v4, v1, Lcom/miui/server/MiuiFboService$FboServiceHandler;->this$0:Lcom/miui/server/MiuiFboService;

    invoke-static {v4}, Lcom/miui/server/MiuiFboService;->-$$Nest$fgetscreenOnTimes(Lcom/miui/server/MiuiFboService;)I

    move-result v4

    invoke-virtual {v2, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v0, v2}, Landroid/util/Slog;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 199
    iget-object v0, v1, Lcom/miui/server/MiuiFboService$FboServiceHandler;->this$0:Lcom/miui/server/MiuiFboService;

    invoke-static {v0}, Lcom/miui/server/MiuiFboService;->-$$Nest$fgetscreenOnTimes(Lcom/miui/server/MiuiFboService;)I

    move-result v0

    if-nez v0, :cond_2

    iget-object v0, v1, Lcom/miui/server/MiuiFboService$FboServiceHandler;->this$0:Lcom/miui/server/MiuiFboService;

    invoke-static {v0}, Lcom/miui/server/MiuiFboService;->-$$Nest$fgetscreenOn(Lcom/miui/server/MiuiFboService;)Z

    move-result v0

    if-nez v0, :cond_2

    iget-object v0, v1, Lcom/miui/server/MiuiFboService$FboServiceHandler;->this$0:Lcom/miui/server/MiuiFboService;

    invoke-static {v0}, Lcom/miui/server/MiuiFboService;->-$$Nest$fgetbatteryStatus(Lcom/miui/server/MiuiFboService;)I

    move-result v0

    if-gtz v0, :cond_1

    iget-object v0, v1, Lcom/miui/server/MiuiFboService$FboServiceHandler;->this$0:Lcom/miui/server/MiuiFboService;

    invoke-static {v0}, Lcom/miui/server/MiuiFboService;->-$$Nest$fgetbatteryLevel(Lcom/miui/server/MiuiFboService;)I

    move-result v0

    const/16 v2, 0x4b

    if-le v0, v2, :cond_2

    :cond_1
    iget-object v0, v1, Lcom/miui/server/MiuiFboService$FboServiceHandler;->this$0:Lcom/miui/server/MiuiFboService;

    invoke-static {v0}, Lcom/miui/server/MiuiFboService;->-$$Nest$fgetbatteryTemperature(Lcom/miui/server/MiuiFboService;)I

    move-result v0

    const/16 v2, 0x190

    if-ge v0, v2, :cond_2

    .line 201
    iget-object v0, v1, Lcom/miui/server/MiuiFboService$FboServiceHandler;->this$0:Lcom/miui/server/MiuiFboService;

    invoke-static {v0}, Lcom/miui/server/MiuiFboService;->-$$Nest$fgetbatteryLevel(Lcom/miui/server/MiuiFboService;)I

    move-result v2

    invoke-static {v0, v2}, Lcom/miui/server/MiuiFboService;->-$$Nest$fputmCurrentBatteryLevel(Lcom/miui/server/MiuiFboService;I)V

    .line 203
    const/4 v2, 0x1

    :try_start_4
    invoke-static {v2}, Lcom/miui/server/MiuiFboService;->-$$Nest$smuseCldStrategy(I)V

    .line 204
    iget-object v0, v1, Lcom/miui/server/MiuiFboService$FboServiceHandler;->this$0:Lcom/miui/server/MiuiFboService;

    invoke-static {v0}, Lcom/miui/server/MiuiFboService;->-$$Nest$fgetmFboNativeService(Lcom/miui/server/MiuiFboService;)Lmiui/fbo/IFbo;

    move-result-object v0

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "continue,"

    invoke-virtual {v2, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget-object v4, v1, Lcom/miui/server/MiuiFboService$FboServiceHandler;->this$0:Lcom/miui/server/MiuiFboService;

    invoke-static {v4}, Lcom/miui/server/MiuiFboService;->-$$Nest$fgetstagingData(Lcom/miui/server/MiuiFboService;)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v2, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-interface {v0, v2}, Lmiui/fbo/IFbo;->FBO_stateCtl(Ljava/lang/String;)Ljava/lang/String;

    .line 205
    iget-object v0, v1, Lcom/miui/server/MiuiFboService$FboServiceHandler;->this$0:Lcom/miui/server/MiuiFboService;

    invoke-static {v0}, Lcom/miui/server/MiuiFboService;->-$$Nest$fgetmFboNativeService(Lcom/miui/server/MiuiFboService;)Lmiui/fbo/IFbo;

    move-result-object v0

    invoke-static {}, Lcom/miui/server/MiuiFboService;->-$$Nest$sfgetmCurrentPackageName()Ljava/lang/String;

    move-result-object v2

    iget-object v4, v1, Lcom/miui/server/MiuiFboService$FboServiceHandler;->this$0:Lcom/miui/server/MiuiFboService;

    invoke-static {v4}, Lcom/miui/server/MiuiFboService;->-$$Nest$fgetmDriverSupport(Lcom/miui/server/MiuiFboService;)Z

    move-result v4

    iget-object v5, v1, Lcom/miui/server/MiuiFboService$FboServiceHandler;->this$0:Lcom/miui/server/MiuiFboService;

    invoke-static {v5}, Lcom/miui/server/MiuiFboService;->-$$Nest$fgetmVdexPath(Lcom/miui/server/MiuiFboService;)Ljava/lang/String;

    move-result-object v5

    iget-object v6, v1, Lcom/miui/server/MiuiFboService$FboServiceHandler;->this$0:Lcom/miui/server/MiuiFboService;

    invoke-static {v6}, Lcom/miui/server/MiuiFboService;->-$$Nest$fgetmVdexPath(Lcom/miui/server/MiuiFboService;)Ljava/lang/String;

    move-result-object v6

    .line 206
    invoke-virtual {v6, v3}, Ljava/lang/String;->lastIndexOf(Ljava/lang/String;)I

    move-result v3

    invoke-virtual {v5, v10, v3}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v3

    .line 205
    invoke-interface {v0, v2, v4, v3}, Lmiui/fbo/IFbo;->FBO_trigger(Ljava/lang/String;ZLjava/lang/String;)V

    .line 207
    iget-object v0, v1, Lcom/miui/server/MiuiFboService$FboServiceHandler;->this$0:Lcom/miui/server/MiuiFboService;

    const-string v2, "continue"

    invoke-static {v0, v2}, Lcom/miui/server/MiuiFboService;->-$$Nest$msendStopOrContinueToHal(Lcom/miui/server/MiuiFboService;Ljava/lang/String;)V

    .line 208
    iget-object v0, v1, Lcom/miui/server/MiuiFboService$FboServiceHandler;->this$0:Lcom/miui/server/MiuiFboService;

    const/4 v2, 0x1

    invoke-virtual {v0, v2}, Lcom/miui/server/MiuiFboService;->setNativeIsRunning(Z)V

    .line 209
    iget-object v0, v1, Lcom/miui/server/MiuiFboService$FboServiceHandler;->this$0:Lcom/miui/server/MiuiFboService;

    invoke-virtual {v0, v10}, Lcom/miui/server/MiuiFboService;->setDueToScreenWait(Z)V
    :try_end_4
    .catch Ljava/lang/Exception; {:try_start_4 .. :try_end_4} :catch_4

    goto :goto_0

    .line 210
    :catch_4
    move-exception v0

    .line 211
    .restart local v0    # "e":Ljava/lang/Exception;
    invoke-static {}, Lcom/miui/server/MiuiFboService;->-$$Nest$sfgetTAG()Ljava/lang/String;

    move-result-object v2

    const-string v3, "fail to execute START_FBO_AGAIN"

    invoke-static {v2, v3}, Landroid/util/Slog;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 212
    .end local v0    # "e":Ljava/lang/Exception;
    :goto_0
    goto/16 :goto_2

    .line 213
    :cond_2
    iget-object v0, v1, Lcom/miui/server/MiuiFboService$FboServiceHandler;->this$0:Lcom/miui/server/MiuiFboService;

    invoke-static {v0}, Lcom/miui/server/MiuiFboService;->-$$Nest$fgetbatteryTemperature(Lcom/miui/server/MiuiFboService;)I

    move-result v0

    const/16 v2, 0x1f4

    if-lt v0, v2, :cond_3

    .line 214
    invoke-static {}, Lcom/miui/server/MiuiFboService;->-$$Nest$sfgetmStopReason()Ljava/util/ArrayList;

    move-result-object v0

    const-string/jumbo v2, "stop because batteryTemperature >= 500"

    invoke-virtual {v0, v2}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 215
    invoke-static {}, Lcom/miui/server/MiuiFboService;->-$$Nest$sfgetTAG()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0, v9}, Landroid/util/Slog;->d(Ljava/lang/String;Ljava/lang/String;)I

    goto/16 :goto_2

    .line 217
    :cond_3
    invoke-static {}, Lcom/miui/server/MiuiFboService;->-$$Nest$sfgetsInstance()Lcom/miui/server/MiuiFboService;

    const/4 v0, 0x0

    const-wide/32 v2, 0x927c0

    invoke-static {v8, v0, v2, v3, v10}, Lcom/miui/server/MiuiFboService;->-$$Nest$smsetAlarm(Ljava/lang/String;Ljava/lang/String;JZ)V

    .line 218
    iget-object v0, v1, Lcom/miui/server/MiuiFboService$FboServiceHandler;->this$0:Lcom/miui/server/MiuiFboService;

    invoke-static {v0, v10}, Lcom/miui/server/MiuiFboService;->-$$Nest$fputscreenOnTimes(Lcom/miui/server/MiuiFboService;I)V

    .line 219
    invoke-static {}, Lcom/miui/server/MiuiFboService;->-$$Nest$sfgetmStopReason()Ljava/util/ArrayList;

    move-result-object v0

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v2, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget-object v3, v1, Lcom/miui/server/MiuiFboService$FboServiceHandler;->this$0:Lcom/miui/server/MiuiFboService;

    invoke-static {v3}, Lcom/miui/server/MiuiFboService;->-$$Nest$fgetscreenOn(Lcom/miui/server/MiuiFboService;)Z

    move-result v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget-object v3, v1, Lcom/miui/server/MiuiFboService$FboServiceHandler;->this$0:Lcom/miui/server/MiuiFboService;

    invoke-static {v3}, Lcom/miui/server/MiuiFboService;->-$$Nest$fgetbatteryStatus(Lcom/miui/server/MiuiFboService;)I

    move-result v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget-object v3, v1, Lcom/miui/server/MiuiFboService$FboServiceHandler;->this$0:Lcom/miui/server/MiuiFboService;

    invoke-static {v3}, Lcom/miui/server/MiuiFboService;->-$$Nest$fgetbatteryLevel(Lcom/miui/server/MiuiFboService;)I

    move-result v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget-object v3, v1, Lcom/miui/server/MiuiFboService$FboServiceHandler;->this$0:Lcom/miui/server/MiuiFboService;

    invoke-static {v3}, Lcom/miui/server/MiuiFboService;->-$$Nest$fgetbatteryTemperature(Lcom/miui/server/MiuiFboService;)I

    move-result v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    move-object/from16 v8, v16

    invoke-virtual {v2, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget-object v3, v1, Lcom/miui/server/MiuiFboService$FboServiceHandler;->this$0:Lcom/miui/server/MiuiFboService;

    invoke-static {v3}, Lcom/miui/server/MiuiFboService;->-$$Nest$fgetscreenOnTimes(Lcom/miui/server/MiuiFboService;)I

    move-result v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v2}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 225
    goto/16 :goto_2

    .line 153
    :pswitch_5
    move-object/from16 v8, v16

    iget-object v0, v1, Lcom/miui/server/MiuiFboService$FboServiceHandler;->this$0:Lcom/miui/server/MiuiFboService;

    invoke-static {v0, v10}, Lcom/miui/server/MiuiFboService;->-$$Nest$fputmessageHasbeenSent(Lcom/miui/server/MiuiFboService;Z)V

    .line 154
    invoke-static {}, Lcom/miui/server/MiuiFboService;->-$$Nest$sfgetTAG()Ljava/lang/String;

    move-result-object v0

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v2, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget-object v4, v1, Lcom/miui/server/MiuiFboService$FboServiceHandler;->this$0:Lcom/miui/server/MiuiFboService;

    invoke-static {v4}, Lcom/miui/server/MiuiFboService;->-$$Nest$fgetscreenOn(Lcom/miui/server/MiuiFboService;)Z

    move-result v4

    invoke-virtual {v2, v4}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget-object v4, v1, Lcom/miui/server/MiuiFboService$FboServiceHandler;->this$0:Lcom/miui/server/MiuiFboService;

    invoke-static {v4}, Lcom/miui/server/MiuiFboService;->-$$Nest$fgetbatteryStatus(Lcom/miui/server/MiuiFboService;)I

    move-result v4

    invoke-virtual {v2, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v13}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget-object v4, v1, Lcom/miui/server/MiuiFboService$FboServiceHandler;->this$0:Lcom/miui/server/MiuiFboService;

    invoke-static {v4}, Lcom/miui/server/MiuiFboService;->-$$Nest$fgetbatteryLevel(Lcom/miui/server/MiuiFboService;)I

    move-result v4

    invoke-virtual {v2, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v15}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget-object v4, v1, Lcom/miui/server/MiuiFboService$FboServiceHandler;->this$0:Lcom/miui/server/MiuiFboService;

    invoke-static {v4}, Lcom/miui/server/MiuiFboService;->-$$Nest$fgetbatteryTemperature(Lcom/miui/server/MiuiFboService;)I

    move-result v4

    invoke-virtual {v2, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v14}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget-object v4, v1, Lcom/miui/server/MiuiFboService$FboServiceHandler;->this$0:Lcom/miui/server/MiuiFboService;

    invoke-static {v4}, Lcom/miui/server/MiuiFboService;->-$$Nest$fgetscreenOnTimes(Lcom/miui/server/MiuiFboService;)I

    move-result v4

    invoke-virtual {v2, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v0, v2}, Landroid/util/Slog;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 159
    iget-object v0, v1, Lcom/miui/server/MiuiFboService$FboServiceHandler;->this$0:Lcom/miui/server/MiuiFboService;

    invoke-static {v0}, Lcom/miui/server/MiuiFboService;->-$$Nest$fgetscreenOnTimes(Lcom/miui/server/MiuiFboService;)I

    move-result v0

    if-nez v0, :cond_6

    iget-object v0, v1, Lcom/miui/server/MiuiFboService$FboServiceHandler;->this$0:Lcom/miui/server/MiuiFboService;

    invoke-static {v0}, Lcom/miui/server/MiuiFboService;->-$$Nest$fgetscreenOn(Lcom/miui/server/MiuiFboService;)Z

    move-result v0

    if-nez v0, :cond_6

    iget-object v0, v1, Lcom/miui/server/MiuiFboService$FboServiceHandler;->this$0:Lcom/miui/server/MiuiFboService;

    invoke-static {v0}, Lcom/miui/server/MiuiFboService;->-$$Nest$fgetbatteryStatus(Lcom/miui/server/MiuiFboService;)I

    move-result v0

    if-gtz v0, :cond_4

    iget-object v0, v1, Lcom/miui/server/MiuiFboService$FboServiceHandler;->this$0:Lcom/miui/server/MiuiFboService;

    invoke-static {v0}, Lcom/miui/server/MiuiFboService;->-$$Nest$fgetbatteryLevel(Lcom/miui/server/MiuiFboService;)I

    move-result v0

    const/16 v2, 0x4b

    if-le v0, v2, :cond_6

    :cond_4
    iget-object v0, v1, Lcom/miui/server/MiuiFboService$FboServiceHandler;->this$0:Lcom/miui/server/MiuiFboService;

    invoke-static {v0}, Lcom/miui/server/MiuiFboService;->-$$Nest$fgetbatteryTemperature(Lcom/miui/server/MiuiFboService;)I

    move-result v0

    const/16 v2, 0x190

    if-ge v0, v2, :cond_6

    .line 161
    iget-object v0, v1, Lcom/miui/server/MiuiFboService$FboServiceHandler;->this$0:Lcom/miui/server/MiuiFboService;

    invoke-static {v0}, Lcom/miui/server/MiuiFboService;->-$$Nest$fgetbatteryLevel(Lcom/miui/server/MiuiFboService;)I

    move-result v2

    invoke-static {v0, v2}, Lcom/miui/server/MiuiFboService;->-$$Nest$fputmCurrentBatteryLevel(Lcom/miui/server/MiuiFboService;I)V

    .line 163
    :try_start_5
    invoke-static {}, Lcom/miui/server/MiuiFboService;->-$$Nest$sfgetlistSize()I

    move-result v0

    if-ltz v0, :cond_5

    .line 164
    invoke-static {}, Lcom/miui/server/MiuiFboService;->-$$Nest$sfgetpackageNameList()Ljava/util/ArrayList;

    move-result-object v0

    invoke-static {}, Lcom/miui/server/MiuiFboService;->-$$Nest$sfgetlistSize()I

    move-result v2

    invoke-virtual {v0, v2}, Ljava/util/ArrayList;->remove(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    invoke-static {v0}, Lcom/miui/server/MiuiFboService;->-$$Nest$sfputmCurrentPackageName(Ljava/lang/String;)V

    .line 165
    iget-object v0, v1, Lcom/miui/server/MiuiFboService$FboServiceHandler;->this$0:Lcom/miui/server/MiuiFboService;

    invoke-static {v0}, Lcom/miui/server/MiuiFboService;->-$$Nest$fgetmFboNativeService(Lcom/miui/server/MiuiFboService;)Lmiui/fbo/IFbo;

    move-result-object v0

    invoke-interface {v0}, Lmiui/fbo/IFbo;->startF2fsGC()V

    .line 166
    iget-object v0, v1, Lcom/miui/server/MiuiFboService$FboServiceHandler;->this$0:Lcom/miui/server/MiuiFboService;

    invoke-static {v0}, Lcom/miui/server/MiuiFboService;->-$$Nest$fgetmContext(Lcom/miui/server/MiuiFboService;)Landroid/content/Context;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/Context;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v0

    invoke-static {}, Lcom/miui/server/MiuiFboService;->-$$Nest$sfgetmCurrentPackageName()Ljava/lang/String;

    move-result-object v2

    .line 167
    const/16 v4, 0x400

    invoke-virtual {v0, v2, v4}, Landroid/content/pm/PackageManager;->getApplicationInfo(Ljava/lang/String;I)Landroid/content/pm/ApplicationInfo;

    move-result-object v0

    .line 168
    .local v0, "applicationInfo":Landroid/content/pm/ApplicationInfo;
    invoke-static {}, Lcom/miui/server/MiuiFboService;->-$$Nest$sfgetTAG()Ljava/lang/String;

    move-result-object v2

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v5, "vdex path:"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    iget-object v5, v0, Landroid/content/pm/ApplicationInfo;->sourceDir:Ljava/lang/String;

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v2, v4}, Landroid/util/Slog;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 169
    iget-object v2, v1, Lcom/miui/server/MiuiFboService$FboServiceHandler;->this$0:Lcom/miui/server/MiuiFboService;

    iget-object v4, v0, Landroid/content/pm/ApplicationInfo;->sourceDir:Ljava/lang/String;

    invoke-static {v2, v4}, Lcom/miui/server/MiuiFboService;->-$$Nest$fputmVdexPath(Lcom/miui/server/MiuiFboService;Ljava/lang/String;)V

    .line 170
    iget-object v2, v1, Lcom/miui/server/MiuiFboService$FboServiceHandler;->this$0:Lcom/miui/server/MiuiFboService;

    invoke-static {v2}, Lcom/miui/server/MiuiFboService;->-$$Nest$fgetmFboNativeService(Lcom/miui/server/MiuiFboService;)Lmiui/fbo/IFbo;

    move-result-object v2

    invoke-static {}, Lcom/miui/server/MiuiFboService;->-$$Nest$sfgetmCurrentPackageName()Ljava/lang/String;

    move-result-object v4

    iget-object v5, v1, Lcom/miui/server/MiuiFboService$FboServiceHandler;->this$0:Lcom/miui/server/MiuiFboService;

    invoke-static {v5}, Lcom/miui/server/MiuiFboService;->-$$Nest$fgetmDriverSupport(Lcom/miui/server/MiuiFboService;)Z

    move-result v5

    iget-object v6, v1, Lcom/miui/server/MiuiFboService$FboServiceHandler;->this$0:Lcom/miui/server/MiuiFboService;

    invoke-static {v6}, Lcom/miui/server/MiuiFboService;->-$$Nest$fgetmVdexPath(Lcom/miui/server/MiuiFboService;)Ljava/lang/String;

    move-result-object v6

    iget-object v7, v1, Lcom/miui/server/MiuiFboService$FboServiceHandler;->this$0:Lcom/miui/server/MiuiFboService;

    invoke-static {v7}, Lcom/miui/server/MiuiFboService;->-$$Nest$fgetmVdexPath(Lcom/miui/server/MiuiFboService;)Ljava/lang/String;

    move-result-object v7

    .line 171
    invoke-virtual {v7, v3}, Ljava/lang/String;->lastIndexOf(Ljava/lang/String;)I

    move-result v3

    invoke-virtual {v6, v10, v3}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v3

    .line 170
    invoke-interface {v2, v4, v5, v3}, Lmiui/fbo/IFbo;->FBO_trigger(Ljava/lang/String;ZLjava/lang/String;)V

    .line 172
    iget-object v2, v1, Lcom/miui/server/MiuiFboService$FboServiceHandler;->this$0:Lcom/miui/server/MiuiFboService;

    const/4 v3, 0x1

    invoke-virtual {v2, v3}, Lcom/miui/server/MiuiFboService;->setNativeIsRunning(Z)V

    .line 173
    iget-object v2, v1, Lcom/miui/server/MiuiFboService$FboServiceHandler;->this$0:Lcom/miui/server/MiuiFboService;

    invoke-virtual {v2, v3}, Lcom/miui/server/MiuiFboService;->setGlobalSwitch(Z)V

    .line 175
    .end local v0    # "applicationInfo":Landroid/content/pm/ApplicationInfo;
    :cond_5
    invoke-static {}, Lcom/miui/server/MiuiFboService;->-$$Nest$sfgetlistSize()I

    move-result v0

    const/4 v2, 0x1

    sub-int/2addr v0, v2

    invoke-static {v0}, Lcom/miui/server/MiuiFboService;->-$$Nest$sfputlistSize(I)V
    :try_end_5
    .catch Ljava/lang/Exception; {:try_start_5 .. :try_end_5} :catch_5

    goto :goto_1

    .line 176
    :catch_5
    move-exception v0

    .line 177
    .local v0, "e":Ljava/lang/Exception;
    invoke-virtual {v0}, Ljava/lang/Exception;->printStackTrace()V

    .line 178
    .end local v0    # "e":Ljava/lang/Exception;
    :goto_1
    goto/16 :goto_2

    .line 179
    :cond_6
    iget-object v0, v1, Lcom/miui/server/MiuiFboService$FboServiceHandler;->this$0:Lcom/miui/server/MiuiFboService;

    invoke-static {v0}, Lcom/miui/server/MiuiFboService;->-$$Nest$fgetbatteryTemperature(Lcom/miui/server/MiuiFboService;)I

    move-result v0

    const/16 v2, 0x1f4

    if-lt v0, v2, :cond_7

    .line 180
    invoke-static {}, Lcom/miui/server/MiuiFboService;->-$$Nest$sfgetmStopReason()Ljava/util/ArrayList;

    move-result-object v0

    const-string v2, "batteryTemperature >= 500"

    invoke-virtual {v0, v2}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 181
    invoke-static {}, Lcom/miui/server/MiuiFboService;->-$$Nest$sfgetTAG()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0, v9}, Landroid/util/Slog;->d(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_2

    .line 183
    :cond_7
    invoke-static {}, Lcom/miui/server/MiuiFboService;->-$$Nest$sfgetsInstance()Lcom/miui/server/MiuiFboService;

    const-string v0, "miui.intent.action.start"

    const-wide/32 v2, 0x1b7740

    const/4 v4, 0x0

    invoke-static {v0, v4, v2, v3, v10}, Lcom/miui/server/MiuiFboService;->-$$Nest$smsetAlarm(Ljava/lang/String;Ljava/lang/String;JZ)V

    .line 184
    iget-object v0, v1, Lcom/miui/server/MiuiFboService$FboServiceHandler;->this$0:Lcom/miui/server/MiuiFboService;

    invoke-static {v0, v10}, Lcom/miui/server/MiuiFboService;->-$$Nest$fputscreenOnTimes(Lcom/miui/server/MiuiFboService;I)V

    .line 185
    invoke-static {}, Lcom/miui/server/MiuiFboService;->-$$Nest$sfgetmStopReason()Ljava/util/ArrayList;

    move-result-object v0

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v2, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget-object v3, v1, Lcom/miui/server/MiuiFboService$FboServiceHandler;->this$0:Lcom/miui/server/MiuiFboService;

    invoke-static {v3}, Lcom/miui/server/MiuiFboService;->-$$Nest$fgetscreenOn(Lcom/miui/server/MiuiFboService;)Z

    move-result v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget-object v3, v1, Lcom/miui/server/MiuiFboService$FboServiceHandler;->this$0:Lcom/miui/server/MiuiFboService;

    invoke-static {v3}, Lcom/miui/server/MiuiFboService;->-$$Nest$fgetbatteryStatus(Lcom/miui/server/MiuiFboService;)I

    move-result v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget-object v3, v1, Lcom/miui/server/MiuiFboService$FboServiceHandler;->this$0:Lcom/miui/server/MiuiFboService;

    invoke-static {v3}, Lcom/miui/server/MiuiFboService;->-$$Nest$fgetbatteryLevel(Lcom/miui/server/MiuiFboService;)I

    move-result v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget-object v3, v1, Lcom/miui/server/MiuiFboService$FboServiceHandler;->this$0:Lcom/miui/server/MiuiFboService;

    invoke-static {v3}, Lcom/miui/server/MiuiFboService;->-$$Nest$fgetbatteryTemperature(Lcom/miui/server/MiuiFboService;)I

    move-result v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget-object v3, v1, Lcom/miui/server/MiuiFboService$FboServiceHandler;->this$0:Lcom/miui/server/MiuiFboService;

    invoke-static {v3}, Lcom/miui/server/MiuiFboService;->-$$Nest$fgetscreenOnTimes(Lcom/miui/server/MiuiFboService;)I

    move-result v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v2}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 191
    nop

    .line 295
    :goto_2
    return-void

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_5
        :pswitch_4
        :pswitch_3
        :pswitch_2
        :pswitch_1
        :pswitch_0
    .end packed-switch
.end method
