.class public Lcom/miui/server/GreenGuardManagerService;
.super Ljava/lang/Object;
.source "GreenGuardManagerService.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/miui/server/GreenGuardManagerService$GreenGuardServiceConn;
    }
.end annotation


# static fields
.field public static final GREEN_KID_AGENT_PKG_NAME:Ljava/lang/String; = "com.miui.greenguard"

.field public static final GREEN_KID_SERVICE:Ljava/lang/String; = "com.miui.greenguard.service.GreenKidService"

.field private static final TAG:Ljava/lang/String; = "GreenKidManagerService"

.field private static mGreenGuardServiceConnection:Landroid/content/ServiceConnection;


# direct methods
.method static bridge synthetic -$$Nest$smstartWatchGreenGuardProcessInner(Landroid/content/Context;)V
    .locals 0

    invoke-static {p0}, Lcom/miui/server/GreenGuardManagerService;->startWatchGreenGuardProcessInner(Landroid/content/Context;)V

    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .line 22
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method private static disableAgentProcess(Landroid/content/Context;)V
    .locals 5
    .param p0, "context"    # Landroid/content/Context;

    .line 37
    const-string v0, "GreenKidManagerService"

    const-string v1, "com.miui.greenguard"

    invoke-virtual {p0}, Landroid/content/Context;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v2

    .line 39
    .local v2, "packageManager":Landroid/content/pm/PackageManager;
    const/16 v3, 0x2000

    :try_start_0
    invoke-virtual {v2, v1, v3}, Landroid/content/pm/PackageManager;->getApplicationInfo(Ljava/lang/String;I)Landroid/content/pm/ApplicationInfo;

    move-result-object v3

    if-eqz v3, :cond_0

    .line 40
    const/4 v3, 0x2

    const/4 v4, 0x0

    invoke-virtual {v2, v1, v3, v4}, Landroid/content/pm/PackageManager;->setApplicationEnabledSetting(Ljava/lang/String;II)V

    .line 41
    const-string v1, "Disable GreenGuard agent : [ com.miui.greenguard] ."

    invoke-static {v0, v1}, Landroid/util/Slog;->i(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 45
    :cond_0
    goto :goto_0

    .line 43
    :catch_0
    move-exception v1

    .line 44
    .local v1, "e":Ljava/lang/Exception;
    const-string v3, "Disable greenGuard agent : [ com.miui.greenguard] failed , package not install"

    invoke-static {v0, v3, v1}, Landroid/util/Slog;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    .line 46
    .end local v1    # "e":Ljava/lang/Exception;
    :goto_0
    return-void
.end method

.method public static init(Landroid/content/Context;)V
    .locals 1
    .param p0, "context"    # Landroid/content/Context;

    .line 29
    invoke-static {p0}, Lcom/miui/server/GreenGuardManagerService;->isGreenKidActive(Landroid/content/Context;)Z

    move-result v0

    if-nez v0, :cond_0

    invoke-static {p0}, Lcom/miui/server/GreenGuardManagerService;->isGreenKidNeedWipe(Landroid/content/Context;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 30
    invoke-static {p0}, Lcom/miui/server/GreenGuardManagerService;->disableAgentProcess(Landroid/content/Context;)V

    goto :goto_0

    .line 32
    :cond_0
    new-instance v0, Lcom/miui/server/GreenGuardManagerService$GreenGuardServiceConn;

    invoke-direct {v0, p0}, Lcom/miui/server/GreenGuardManagerService$GreenGuardServiceConn;-><init>(Landroid/content/Context;)V

    sput-object v0, Lcom/miui/server/GreenGuardManagerService;->mGreenGuardServiceConnection:Landroid/content/ServiceConnection;

    .line 34
    :goto_0
    return-void
.end method

.method private static isGreenKidActive(Landroid/content/Context;)Z
    .locals 1
    .param p0, "context"    # Landroid/content/Context;

    .line 49
    invoke-virtual {p0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    invoke-static {v0}, Landroid/provider/MiuiSettings$Secure;->isGreenKidActive(Landroid/content/ContentResolver;)Z

    move-result v0

    return v0
.end method

.method private static isGreenKidNeedWipe(Landroid/content/Context;)Z
    .locals 3
    .param p0, "context"    # Landroid/content/Context;

    .line 53
    invoke-virtual {p0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    const-string v1, "key_url_green_guard_sdk_need_clear_data"

    const/4 v2, 0x0

    invoke-static {v0, v1, v2}, Landroid/provider/Settings$Secure;->getInt(Landroid/content/ContentResolver;Ljava/lang/String;I)I

    move-result v0

    const/4 v1, 0x1

    if-ne v0, v1, :cond_0

    move v2, v1

    :cond_0
    return v2
.end method

.method public static startWatchGreenGuardProcess(Landroid/content/Context;)V
    .locals 5
    .param p0, "context"    # Landroid/content/Context;

    .line 58
    invoke-static {}, Landroid/os/Binder;->getCallingUid()I

    move-result v0

    .line 59
    .local v0, "callingUid":I
    invoke-static {}, Landroid/os/Binder;->getCallingPid()I

    move-result v1

    .line 60
    .local v1, "callingPid":I
    invoke-static {v1}, Lcom/android/server/am/ProcessUtils;->getPackageNameByPid(I)Ljava/lang/String;

    move-result-object v2

    .line 61
    .local v2, "callingPackageName":Ljava/lang/String;
    invoke-static {v0}, Landroid/os/UserHandle;->getAppId(I)I

    move-result v3

    const/16 v4, 0x3e8

    if-eq v3, v4, :cond_1

    .line 62
    const-string v3, "com.miui.greenguard"

    invoke-virtual {v3, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-nez v3, :cond_1

    .line 63
    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "Permission Denial from pid="

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, ", uid="

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, "callingPkg:"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    .line 64
    if-nez v2, :cond_0

    const-string v4, ""

    goto :goto_0

    :cond_0
    move-object v4, v2

    :goto_0
    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    .line 65
    .local v3, "msg":Ljava/lang/String;
    const-string v4, "GreenKidManagerService"

    invoke-static {v4, v3}, Landroid/util/Slog;->w(Ljava/lang/String;Ljava/lang/String;)I

    .line 66
    new-instance v4, Ljava/lang/SecurityException;

    invoke-direct {v4, v3}, Ljava/lang/SecurityException;-><init>(Ljava/lang/String;)V

    throw v4

    .line 68
    .end local v3    # "msg":Ljava/lang/String;
    :cond_1
    invoke-static {p0}, Lcom/miui/server/GreenGuardManagerService;->startWatchGreenGuardProcessInner(Landroid/content/Context;)V

    .line 69
    return-void
.end method

.method private static declared-synchronized startWatchGreenGuardProcessInner(Landroid/content/Context;)V
    .locals 4
    .param p0, "context"    # Landroid/content/Context;

    const-class v0, Lcom/miui/server/GreenGuardManagerService;

    monitor-enter v0

    .line 72
    :try_start_0
    const-string v1, "GreenKidManagerService"

    const-string/jumbo v2, "startWatchGreenGuardProcess"

    invoke-static {v1, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 73
    new-instance v1, Landroid/content/Intent;

    invoke-direct {v1}, Landroid/content/Intent;-><init>()V

    .line 74
    .local v1, "intent":Landroid/content/Intent;
    const-string v2, "com.miui.greenguard"

    const-string v3, "com.miui.greenguard.service.GreenKidService"

    invoke-virtual {v1, v2, v3}, Landroid/content/Intent;->setClassName(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 75
    sget-object v2, Lcom/miui/server/GreenGuardManagerService;->mGreenGuardServiceConnection:Landroid/content/ServiceConnection;

    const/4 v3, 0x1

    invoke-virtual {p0, v1, v2, v3}, Landroid/content/Context;->bindService(Landroid/content/Intent;Landroid/content/ServiceConnection;I)Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 76
    monitor-exit v0

    return-void

    .line 71
    .end local v1    # "intent":Landroid/content/Intent;
    .end local p0    # "context":Landroid/content/Context;
    :catchall_0
    move-exception p0

    monitor-exit v0

    throw p0
.end method
