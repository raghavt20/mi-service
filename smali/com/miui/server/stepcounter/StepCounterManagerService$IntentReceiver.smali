.class Lcom/miui/server/stepcounter/StepCounterManagerService$IntentReceiver;
.super Landroid/content/BroadcastReceiver;
.source "StepCounterManagerService.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/miui/server/stepcounter/StepCounterManagerService;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "IntentReceiver"
.end annotation


# instance fields
.field final synthetic this$0:Lcom/miui/server/stepcounter/StepCounterManagerService;


# direct methods
.method private constructor <init>(Lcom/miui/server/stepcounter/StepCounterManagerService;)V
    .locals 0

    .line 315
    iput-object p1, p0, Lcom/miui/server/stepcounter/StepCounterManagerService$IntentReceiver;->this$0:Lcom/miui/server/stepcounter/StepCounterManagerService;

    invoke-direct {p0}, Landroid/content/BroadcastReceiver;-><init>()V

    return-void
.end method

.method synthetic constructor <init>(Lcom/miui/server/stepcounter/StepCounterManagerService;Lcom/miui/server/stepcounter/StepCounterManagerService$IntentReceiver-IA;)V
    .locals 0

    invoke-direct {p0, p1}, Lcom/miui/server/stepcounter/StepCounterManagerService$IntentReceiver;-><init>(Lcom/miui/server/stepcounter/StepCounterManagerService;)V

    return-void
.end method

.method private dealSleepModeChanged(I)V
    .locals 2
    .param p1, "state"    # I

    .line 328
    const/4 v0, 0x2

    if-ne p1, v0, :cond_0

    iget-object v0, p0, Lcom/miui/server/stepcounter/StepCounterManagerService$IntentReceiver;->this$0:Lcom/miui/server/stepcounter/StepCounterManagerService;

    invoke-static {v0}, Lcom/miui/server/stepcounter/StepCounterManagerService;->-$$Nest$fgetisRegisterDetectorListener(Lcom/miui/server/stepcounter/StepCounterManagerService;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 329
    iget-object v0, p0, Lcom/miui/server/stepcounter/StepCounterManagerService$IntentReceiver;->this$0:Lcom/miui/server/stepcounter/StepCounterManagerService;

    invoke-static {v0}, Lcom/miui/server/stepcounter/StepCounterManagerService;->-$$Nest$mregisterDetectorListener(Lcom/miui/server/stepcounter/StepCounterManagerService;)Z

    move-result v1

    invoke-static {v0, v1}, Lcom/miui/server/stepcounter/StepCounterManagerService;->-$$Nest$fputisRegisterDetectorListener(Lcom/miui/server/stepcounter/StepCounterManagerService;Z)V

    goto :goto_0

    .line 330
    :cond_0
    const/4 v0, 0x1

    if-ne p1, v0, :cond_1

    iget-object v0, p0, Lcom/miui/server/stepcounter/StepCounterManagerService$IntentReceiver;->this$0:Lcom/miui/server/stepcounter/StepCounterManagerService;

    invoke-static {v0}, Lcom/miui/server/stepcounter/StepCounterManagerService;->-$$Nest$fgetisRegisterDetectorListener(Lcom/miui/server/stepcounter/StepCounterManagerService;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 331
    iget-object v0, p0, Lcom/miui/server/stepcounter/StepCounterManagerService$IntentReceiver;->this$0:Lcom/miui/server/stepcounter/StepCounterManagerService;

    invoke-static {v0}, Lcom/miui/server/stepcounter/StepCounterManagerService;->-$$Nest$munregisterDetectorListener(Lcom/miui/server/stepcounter/StepCounterManagerService;)V

    .line 332
    iget-object v0, p0, Lcom/miui/server/stepcounter/StepCounterManagerService$IntentReceiver;->this$0:Lcom/miui/server/stepcounter/StepCounterManagerService;

    const/4 v1, 0x0

    invoke-static {v0, v1}, Lcom/miui/server/stepcounter/StepCounterManagerService;->-$$Nest$fputisRegisterDetectorListener(Lcom/miui/server/stepcounter/StepCounterManagerService;Z)V

    .line 334
    :cond_1
    :goto_0
    return-void
.end method


# virtual methods
.method public onReceive(Landroid/content/Context;Landroid/content/Intent;)V
    .locals 3
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "intent"    # Landroid/content/Intent;

    .line 318
    if-eqz p2, :cond_0

    invoke-virtual {p2}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    .line 319
    .local v0, "action":Ljava/lang/String;
    :goto_0
    const-string v1, "android.intent.action.TIME_SET"

    invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_1

    .line 320
    invoke-static {}, Lcom/miui/server/stepcounter/StepCounterManagerService;->resetSystemBootTime()V

    goto :goto_1

    .line 321
    :cond_1
    const-string v1, "com.miui.powerkeeper_sleep_changed"

    invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    .line 322
    const-string/jumbo v1, "state"

    const/4 v2, -0x1

    invoke-virtual {p2, v1, v2}, Landroid/content/Intent;->getIntExtra(Ljava/lang/String;I)I

    move-result v1

    .line 323
    .local v1, "state":I
    invoke-direct {p0, v1}, Lcom/miui/server/stepcounter/StepCounterManagerService$IntentReceiver;->dealSleepModeChanged(I)V

    .line 325
    .end local v1    # "state":I
    :cond_2
    :goto_1
    return-void
.end method
