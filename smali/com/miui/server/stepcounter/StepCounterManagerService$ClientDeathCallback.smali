.class Lcom/miui/server/stepcounter/StepCounterManagerService$ClientDeathCallback;
.super Ljava/lang/Object;
.source "StepCounterManagerService.java"

# interfaces
.implements Landroid/os/IBinder$DeathRecipient;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/miui/server/stepcounter/StepCounterManagerService;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = "ClientDeathCallback"
.end annotation


# instance fields
.field private mToken:Landroid/os/IBinder;

.field final synthetic this$0:Lcom/miui/server/stepcounter/StepCounterManagerService;


# direct methods
.method public constructor <init>(Lcom/miui/server/stepcounter/StepCounterManagerService;Landroid/os/IBinder;)V
    .locals 1
    .param p1, "this$0"    # Lcom/miui/server/stepcounter/StepCounterManagerService;
    .param p2, "token"    # Landroid/os/IBinder;

    .line 490
    iput-object p1, p0, Lcom/miui/server/stepcounter/StepCounterManagerService$ClientDeathCallback;->this$0:Lcom/miui/server/stepcounter/StepCounterManagerService;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 491
    iput-object p2, p0, Lcom/miui/server/stepcounter/StepCounterManagerService$ClientDeathCallback;->mToken:Landroid/os/IBinder;

    .line 493
    const/4 v0, 0x0

    :try_start_0
    invoke-interface {p2, p0, v0}, Landroid/os/IBinder;->linkToDeath(Landroid/os/IBinder$DeathRecipient;I)V
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    .line 496
    goto :goto_0

    .line 494
    :catch_0
    move-exception v0

    .line 495
    .local v0, "e":Landroid/os/RemoteException;
    invoke-virtual {v0}, Landroid/os/RemoteException;->printStackTrace()V

    .line 497
    .end local v0    # "e":Landroid/os/RemoteException;
    :goto_0
    return-void
.end method


# virtual methods
.method public binderDied()V
    .locals 3

    .line 501
    iget-object v0, p0, Lcom/miui/server/stepcounter/StepCounterManagerService$ClientDeathCallback;->this$0:Lcom/miui/server/stepcounter/StepCounterManagerService;

    invoke-static {v0}, Lcom/miui/server/stepcounter/StepCounterManagerService;->-$$Nest$fgetmLocked(Lcom/miui/server/stepcounter/StepCounterManagerService;)Ljava/lang/Object;

    move-result-object v0

    monitor-enter v0

    .line 502
    :try_start_0
    iget-object v1, p0, Lcom/miui/server/stepcounter/StepCounterManagerService$ClientDeathCallback;->this$0:Lcom/miui/server/stepcounter/StepCounterManagerService;

    invoke-static {v1}, Lcom/miui/server/stepcounter/StepCounterManagerService;->-$$Nest$fgetmClientDeathCallbacks(Lcom/miui/server/stepcounter/StepCounterManagerService;)Ljava/util/HashMap;

    move-result-object v1

    iget-object v2, p0, Lcom/miui/server/stepcounter/StepCounterManagerService$ClientDeathCallback;->mToken:Landroid/os/IBinder;

    invoke-virtual {v1, v2}, Ljava/util/HashMap;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    .line 503
    iget-object v1, p0, Lcom/miui/server/stepcounter/StepCounterManagerService$ClientDeathCallback;->this$0:Lcom/miui/server/stepcounter/StepCounterManagerService;

    invoke-static {v1}, Lcom/miui/server/stepcounter/StepCounterManagerService;->-$$Nest$fgetmClientDeathCallbacks(Lcom/miui/server/stepcounter/StepCounterManagerService;)Ljava/util/HashMap;

    move-result-object v1

    invoke-virtual {v1}, Ljava/util/HashMap;->size()I

    move-result v1

    if-nez v1, :cond_0

    iget-object v1, p0, Lcom/miui/server/stepcounter/StepCounterManagerService$ClientDeathCallback;->this$0:Lcom/miui/server/stepcounter/StepCounterManagerService;

    invoke-static {v1}, Lcom/miui/server/stepcounter/StepCounterManagerService;->-$$Nest$fgetmTreadmillSensor(Lcom/miui/server/stepcounter/StepCounterManagerService;)Landroid/hardware/Sensor;

    move-result-object v1

    if-eqz v1, :cond_0

    iget-object v1, p0, Lcom/miui/server/stepcounter/StepCounterManagerService$ClientDeathCallback;->this$0:Lcom/miui/server/stepcounter/StepCounterManagerService;

    invoke-static {v1}, Lcom/miui/server/stepcounter/StepCounterManagerService;->-$$Nest$fgetmTreadmillSensor(Lcom/miui/server/stepcounter/StepCounterManagerService;)Landroid/hardware/Sensor;

    move-result-object v1

    .line 504
    invoke-virtual {v1}, Landroid/hardware/Sensor;->getName()Ljava/lang/String;

    move-result-object v1

    const-string v2, "oem_treadmill  Wakeup"

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 505
    iget-object v1, p0, Lcom/miui/server/stepcounter/StepCounterManagerService$ClientDeathCallback;->this$0:Lcom/miui/server/stepcounter/StepCounterManagerService;

    const/4 v2, 0x0

    invoke-static {v1, v2}, Lcom/miui/server/stepcounter/StepCounterManagerService;->-$$Nest$fputmTreadmillEnabled(Lcom/miui/server/stepcounter/StepCounterManagerService;Z)V

    .line 506
    iget-object v1, p0, Lcom/miui/server/stepcounter/StepCounterManagerService$ClientDeathCallback;->this$0:Lcom/miui/server/stepcounter/StepCounterManagerService;

    invoke-static {v1}, Lcom/miui/server/stepcounter/StepCounterManagerService;->-$$Nest$fgetmSensorManager(Lcom/miui/server/stepcounter/StepCounterManagerService;)Landroid/hardware/SensorManager;

    move-result-object v1

    iget-object v2, p0, Lcom/miui/server/stepcounter/StepCounterManagerService$ClientDeathCallback;->this$0:Lcom/miui/server/stepcounter/StepCounterManagerService;

    invoke-static {v2}, Lcom/miui/server/stepcounter/StepCounterManagerService;->-$$Nest$fgetmTreadmillListener(Lcom/miui/server/stepcounter/StepCounterManagerService;)Landroid/hardware/SensorEventListener;

    move-result-object v2

    invoke-virtual {v1, v2}, Landroid/hardware/SensorManager;->unregisterListener(Landroid/hardware/SensorEventListener;)V

    .line 507
    const-string v1, "StepCounterManagerService"

    const-string v2, "binderDied:unregisterListener Treadmill Listener"

    invoke-static {v1, v2}, Landroid/util/Slog;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 509
    :cond_0
    monitor-exit v0

    .line 510
    return-void

    .line 509
    :catchall_0
    move-exception v1

    monitor-exit v0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v1
.end method
