.class Lcom/miui/server/stepcounter/StepCounterManagerService$StepDetectorListener;
.super Ljava/lang/Object;
.source "StepCounterManagerService.java"

# interfaces
.implements Landroid/hardware/SensorEventListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/miui/server/stepcounter/StepCounterManagerService;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "StepDetectorListener"
.end annotation


# instance fields
.field final synthetic this$0:Lcom/miui/server/stepcounter/StepCounterManagerService;


# direct methods
.method private constructor <init>(Lcom/miui/server/stepcounter/StepCounterManagerService;)V
    .locals 0

    .line 252
    iput-object p1, p0, Lcom/miui/server/stepcounter/StepCounterManagerService$StepDetectorListener;->this$0:Lcom/miui/server/stepcounter/StepCounterManagerService;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method synthetic constructor <init>(Lcom/miui/server/stepcounter/StepCounterManagerService;Lcom/miui/server/stepcounter/StepCounterManagerService$StepDetectorListener-IA;)V
    .locals 0

    invoke-direct {p0, p1}, Lcom/miui/server/stepcounter/StepCounterManagerService$StepDetectorListener;-><init>(Lcom/miui/server/stepcounter/StepCounterManagerService;)V

    return-void
.end method


# virtual methods
.method public onAccuracyChanged(Landroid/hardware/Sensor;I)V
    .locals 0
    .param p1, "sensor"    # Landroid/hardware/Sensor;
    .param p2, "accuracy"    # I

    .line 273
    return-void
.end method

.method public onSensorChanged(Landroid/hardware/SensorEvent;)V
    .locals 8
    .param p1, "event"    # Landroid/hardware/SensorEvent;

    .line 255
    iget-object v0, p1, Landroid/hardware/SensorEvent;->values:[F

    const/4 v1, 0x0

    aget v0, v0, v1

    float-to-int v0, v0

    .line 257
    .local v0, "counter":I
    iget-object v1, p1, Landroid/hardware/SensorEvent;->values:[F

    array-length v1, v1

    const/4 v2, 0x1

    if-le v1, v2, :cond_0

    .line 258
    iget-object v1, p1, Landroid/hardware/SensorEvent;->values:[F

    aget v1, v1, v2

    float-to-int v1, v1

    .local v1, "sensorMode":I
    goto :goto_0

    .line 260
    .end local v1    # "sensorMode":I
    :cond_0
    const/4 v1, 0x1

    .line 262
    .restart local v1    # "sensorMode":I
    :goto_0
    invoke-static {}, Lcom/miui/server/stepcounter/StepCounterManagerService;->-$$Nest$sfgetSYSTEM_BOOT_TIME()J

    move-result-wide v2

    iget-wide v4, p1, Landroid/hardware/SensorEvent;->timestamp:J

    const-wide/32 v6, 0xf4240

    div-long/2addr v4, v6

    add-long/2addr v2, v4

    .line 263
    .local v2, "timestamp":J
    iget-object v4, p0, Lcom/miui/server/stepcounter/StepCounterManagerService$StepDetectorListener;->this$0:Lcom/miui/server/stepcounter/StepCounterManagerService;

    invoke-static {v4}, Lcom/miui/server/stepcounter/StepCounterManagerService;->-$$Nest$fgetmStepList(Lcom/miui/server/stepcounter/StepCounterManagerService;)Ljava/util/List;

    move-result-object v4

    new-instance v5, Lmiui/stepcounter/StepDetector;

    invoke-direct {v5, v0, v2, v3, v1}, Lmiui/stepcounter/StepDetector;-><init>(IJI)V

    invoke-interface {v4, v5}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 264
    invoke-static {}, Lcom/miui/server/stepcounter/StepCounterManagerService;->-$$Nest$sfgetsDEBUG()Z

    move-result v4

    if-eqz v4, :cond_1

    .line 265
    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "onSensorChanged: count: "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, "\tsensorMode: "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, "\ttimestamp: "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v2, v3}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    const-string v5, "StepCounterManagerService"

    invoke-static {v5, v4}, Landroid/util/Slog;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 268
    :cond_1
    return-void
.end method
