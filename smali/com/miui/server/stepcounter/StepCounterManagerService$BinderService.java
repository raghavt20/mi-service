class com.miui.server.stepcounter.StepCounterManagerService$BinderService extends android.os.Binder {
	 /* .source "StepCounterManagerService.java" */
	 /* # annotations */
	 /* .annotation system Ldalvik/annotation/EnclosingClass; */
	 /* value = Lcom/miui/server/stepcounter/StepCounterManagerService; */
} // .end annotation
/* .annotation system Ldalvik/annotation/InnerClass; */
/* accessFlags = 0x2 */
/* name = "BinderService" */
} // .end annotation
/* # static fields */
private static final Integer GET_LATEST_DATA;
private static final Integer HAVE_STEP_SENSOR;
private static final Integer RECEIVE_TREADMILL;
public static final java.lang.String SERVICE_NAME;
/* # instance fields */
final com.miui.server.stepcounter.StepCounterManagerService this$0; //synthetic
/* # direct methods */
private com.miui.server.stepcounter.StepCounterManagerService$BinderService ( ) {
/* .locals 0 */
/* .line 353 */
this.this$0 = p1;
/* invoke-direct {p0}, Landroid/os/Binder;-><init>()V */
return;
} // .end method
 com.miui.server.stepcounter.StepCounterManagerService$BinderService ( ) { //synthethic
/* .locals 0 */
/* invoke-direct {p0, p1}, Lcom/miui/server/stepcounter/StepCounterManagerService$BinderService;-><init>(Lcom/miui/server/stepcounter/StepCounterManagerService;)V */
return;
} // .end method
/* # virtual methods */
protected void dump ( java.io.FileDescriptor p0, java.io.PrintWriter p1, java.lang.String[] p2 ) {
/* .locals 3 */
/* .param p1, "fd" # Ljava/io/FileDescriptor; */
/* .param p2, "pw" # Ljava/io/PrintWriter; */
/* .param p3, "args" # [Ljava/lang/String; */
/* .line 368 */
v0 = this.this$0;
com.miui.server.stepcounter.StepCounterManagerService .-$$Nest$fgetmContext ( v0 );
final String v1 = "StepCounterManagerService"; // const-string v1, "StepCounterManagerService"
v0 = com.android.internal.util.DumpUtils .checkDumpPermission ( v0,v1,p2 );
/* if-nez v0, :cond_0 */
return;
/* .line 369 */
} // :cond_0
android.os.Binder .clearCallingIdentity ( );
/* move-result-wide v0 */
/* .line 371 */
/* .local v0, "ident":J */
try { // :try_start_0
v2 = this.this$0;
com.miui.server.stepcounter.StepCounterManagerService .-$$Nest$mdumpInternal ( v2,p2 );
/* :try_end_0 */
/* .catchall {:try_start_0 ..:try_end_0} :catchall_0 */
/* .line 373 */
android.os.Binder .restoreCallingIdentity ( v0,v1 );
/* .line 374 */
/* nop */
/* .line 375 */
return;
/* .line 373 */
/* :catchall_0 */
/* move-exception v2 */
android.os.Binder .restoreCallingIdentity ( v0,v1 );
/* .line 374 */
/* throw v2 */
} // .end method
public Boolean getLatestData ( Boolean p0 ) {
/* .locals 5 */
/* .param p1, "isForce" # Z */
/* .line 447 */
android.os.Binder .clearCallingIdentity ( );
/* move-result-wide v0 */
/* .line 449 */
/* .local v0, "ident":J */
try { // :try_start_0
	 v2 = 	 com.miui.server.stepcounter.StepCounterManagerService .-$$Nest$sfgetsDEBUG ( );
	 if ( v2 != null) { // if-eqz v2, :cond_0
		 /* .line 450 */
		 final String v2 = "StepCounterManagerService"; // const-string v2, "StepCounterManagerService"
		 /* new-instance v3, Ljava/lang/StringBuilder; */
		 /* invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V */
		 final String v4 = "getLatestDataInternal: isForce? "; // const-string v4, "getLatestDataInternal: isForce? "
		 (( java.lang.StringBuilder ) v3 ).append ( v4 ); // invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
		 (( java.lang.StringBuilder ) v3 ).append ( p1 ); // invoke-virtual {v3, p1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;
		 (( java.lang.StringBuilder ) v3 ).toString ( ); // invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
		 android.util.Slog .i ( v2,v3 );
		 /* .line 452 */
	 } // :cond_0
	 v2 = 	 (( com.miui.server.stepcounter.StepCounterManagerService$BinderService ) p0 ).getLatestDataInternal ( p1 ); // invoke-virtual {p0, p1}, Lcom/miui/server/stepcounter/StepCounterManagerService$BinderService;->getLatestDataInternal(Z)Z
	 /* :try_end_0 */
	 /* .catchall {:try_start_0 ..:try_end_0} :catchall_0 */
	 /* .line 454 */
	 android.os.Binder .restoreCallingIdentity ( v0,v1 );
	 /* .line 452 */
	 /* .line 454 */
	 /* :catchall_0 */
	 /* move-exception v2 */
	 android.os.Binder .restoreCallingIdentity ( v0,v1 );
	 /* .line 455 */
	 /* throw v2 */
} // .end method
public Boolean getLatestDataInternal ( Boolean p0 ) {
	 /* .locals 3 */
	 /* .param p1, "isForce" # Z */
	 /* .line 459 */
	 v0 = this.this$0;
	 com.miui.server.stepcounter.StepCounterManagerService .-$$Nest$fgetmLocked ( v0 );
	 /* monitor-enter v0 */
	 /* .line 460 */
	 try { // :try_start_0
		 v1 = this.this$0;
		 v1 = 		 com.miui.server.stepcounter.StepCounterManagerService .-$$Nest$fgetmHaveStepSensor ( v1 );
		 if ( v1 != null) { // if-eqz v1, :cond_1
			 /* .line 461 */
			 if ( p1 != null) { // if-eqz p1, :cond_0
				 /* .line 462 */
				 v1 = this.this$0;
				 com.miui.server.stepcounter.StepCounterManagerService .-$$Nest$fgetmHandler ( v1 );
				 /* const/16 v2, 0x8 */
				 (( com.miui.server.stepcounter.StepCounterManagerService$StepCountHandler ) v1 ).sendEmptyMessage ( v2 ); // invoke-virtual {v1, v2}, Lcom/miui/server/stepcounter/StepCounterManagerService$StepCountHandler;->sendEmptyMessage(I)Z
				 /* .line 464 */
			 } // :cond_0
			 v1 = this.this$0;
			 com.miui.server.stepcounter.StepCounterManagerService .-$$Nest$fgetmHandler ( v1 );
			 int v2 = 4; // const/4 v2, 0x4
			 (( com.miui.server.stepcounter.StepCounterManagerService$StepCountHandler ) v1 ).sendEmptyMessage ( v2 ); // invoke-virtual {v1, v2}, Lcom/miui/server/stepcounter/StepCounterManagerService$StepCountHandler;->sendEmptyMessage(I)Z
			 /* .line 467 */
		 } // :cond_1
	 } // :goto_0
	 /* monitor-exit v0 */
	 int v0 = 1; // const/4 v0, 0x1
	 /* .line 468 */
	 /* :catchall_0 */
	 /* move-exception v1 */
	 /* monitor-exit v0 */
	 /* :try_end_0 */
	 /* .catchall {:try_start_0 ..:try_end_0} :catchall_0 */
	 /* throw v1 */
} // .end method
public Boolean haveStepSensor ( android.os.Parcel p0 ) {
	 /* .locals 3 */
	 /* .param p1, "reply" # Landroid/os/Parcel; */
	 /* .line 431 */
	 android.os.Binder .clearCallingIdentity ( );
	 /* move-result-wide v0 */
	 /* .line 433 */
	 /* .local v0, "ident":J */
	 try { // :try_start_0
		 v2 = 		 (( com.miui.server.stepcounter.StepCounterManagerService$BinderService ) p0 ).haveStepSensorInternal ( p1 ); // invoke-virtual {p0, p1}, Lcom/miui/server/stepcounter/StepCounterManagerService$BinderService;->haveStepSensorInternal(Landroid/os/Parcel;)Z
		 /* :try_end_0 */
		 /* .catchall {:try_start_0 ..:try_end_0} :catchall_0 */
		 /* .line 435 */
		 android.os.Binder .restoreCallingIdentity ( v0,v1 );
		 /* .line 433 */
		 /* .line 435 */
		 /* :catchall_0 */
		 /* move-exception v2 */
		 android.os.Binder .restoreCallingIdentity ( v0,v1 );
		 /* .line 436 */
		 /* throw v2 */
	 } // .end method
	 public Boolean haveStepSensorInternal ( android.os.Parcel p0 ) {
		 /* .locals 3 */
		 /* .param p1, "reply" # Landroid/os/Parcel; */
		 /* .line 440 */
		 v0 = this.this$0;
		 com.miui.server.stepcounter.StepCounterManagerService .-$$Nest$fgetmLocked ( v0 );
		 /* monitor-enter v0 */
		 /* .line 441 */
		 try { // :try_start_0
			 v1 = this.this$0;
			 v1 = 			 com.miui.server.stepcounter.StepCounterManagerService .-$$Nest$fgetmHaveStepSensor ( v1 );
			 int v2 = 1; // const/4 v2, 0x1
			 if ( v1 != null) { // if-eqz v1, :cond_0
				 /* move v1, v2 */
			 } // :cond_0
			 int v1 = 0; // const/4 v1, 0x0
		 } // :goto_0
		 (( android.os.Parcel ) p1 ).writeInt ( v1 ); // invoke-virtual {p1, v1}, Landroid/os/Parcel;->writeInt(I)V
		 /* .line 442 */
		 /* monitor-exit v0 */
		 /* .line 443 */
		 /* :catchall_0 */
		 /* move-exception v1 */
		 /* monitor-exit v0 */
		 /* :try_end_0 */
		 /* .catchall {:try_start_0 ..:try_end_0} :catchall_0 */
		 /* throw v1 */
	 } // .end method
	 public void onShellCommand ( java.io.FileDescriptor p0, java.io.FileDescriptor p1, java.io.FileDescriptor p2, java.lang.String[] p3, android.os.ShellCallback p4, android.os.ResultReceiver p5 ) {
		 /* .locals 8 */
		 /* .param p1, "in" # Ljava/io/FileDescriptor; */
		 /* .param p2, "out" # Ljava/io/FileDescriptor; */
		 /* .param p3, "err" # Ljava/io/FileDescriptor; */
		 /* .param p4, "args" # [Ljava/lang/String; */
		 /* .param p5, "callback" # Landroid/os/ShellCallback; */
		 /* .param p6, "resultReceiver" # Landroid/os/ResultReceiver; */
		 /* .annotation system Ldalvik/annotation/Throws; */
		 /* value = { */
		 /* Landroid/os/RemoteException; */
		 /* } */
	 } // .end annotation
	 /* .line 363 */
	 /* new-instance v0, Lcom/miui/server/stepcounter/StepCounterManagerService$Shell; */
	 v1 = this.this$0;
	 int v2 = 0; // const/4 v2, 0x0
	 /* invoke-direct {v0, v1, v2}, Lcom/miui/server/stepcounter/StepCounterManagerService$Shell;-><init>(Lcom/miui/server/stepcounter/StepCounterManagerService;Lcom/miui/server/stepcounter/StepCounterManagerService$Shell-IA;)V */
	 /* move-object v1, p0 */
	 /* move-object v2, p1 */
	 /* move-object v3, p2 */
	 /* move-object v4, p3 */
	 /* move-object v5, p4 */
	 /* move-object v6, p5 */
	 /* move-object v7, p6 */
	 /* invoke-virtual/range {v0 ..v7}, Lcom/miui/server/stepcounter/StepCounterManagerService$Shell;->exec(Landroid/os/Binder;Ljava/io/FileDescriptor;Ljava/io/FileDescriptor;Ljava/io/FileDescriptor;[Ljava/lang/String;Landroid/os/ShellCallback;Landroid/os/ResultReceiver;)I */
	 /* .line 364 */
	 return;
} // .end method
protected Boolean onTransact ( Integer p0, android.os.Parcel p1, android.os.Parcel p2, Integer p3 ) {
	 /* .locals 3 */
	 /* .param p1, "code" # I */
	 /* .param p2, "data" # Landroid/os/Parcel; */
	 /* .param p3, "reply" # Landroid/os/Parcel; */
	 /* .param p4, "flags" # I */
	 /* .annotation system Ldalvik/annotation/Throws; */
	 /* value = { */
	 /* Landroid/os/RemoteException; */
	 /* } */
} // .end annotation
/* .line 380 */
final String v0 = "miui_step_counter_service"; // const-string v0, "miui_step_counter_service"
/* packed-switch p1, :pswitch_data_0 */
/* .line 394 */
v0 = /* invoke-super {p0, p1, p2, p3, p4}, Landroid/os/Binder;->onTransact(ILandroid/os/Parcel;Landroid/os/Parcel;I)Z */
/* .line 390 */
/* :pswitch_0 */
(( android.os.Parcel ) p2 ).enforceInterface ( v0 ); // invoke-virtual {p2, v0}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V
/* .line 391 */
v0 = (( android.os.Parcel ) p2 ).readBoolean ( ); // invoke-virtual {p2}, Landroid/os/Parcel;->readBoolean()Z
/* .line 392 */
/* .local v0, "isForce":Z */
v1 = (( com.miui.server.stepcounter.StepCounterManagerService$BinderService ) p0 ).getLatestData ( v0 ); // invoke-virtual {p0, v0}, Lcom/miui/server/stepcounter/StepCounterManagerService$BinderService;->getLatestData(Z)Z
/* .line 387 */
} // .end local v0 # "isForce":Z
/* :pswitch_1 */
(( android.os.Parcel ) p2 ).enforceInterface ( v0 ); // invoke-virtual {p2, v0}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V
/* .line 388 */
v0 = (( com.miui.server.stepcounter.StepCounterManagerService$BinderService ) p0 ).haveStepSensor ( p3 ); // invoke-virtual {p0, p3}, Lcom/miui/server/stepcounter/StepCounterManagerService$BinderService;->haveStepSensor(Landroid/os/Parcel;)Z
/* .line 382 */
/* :pswitch_2 */
(( android.os.Parcel ) p2 ).enforceInterface ( v0 ); // invoke-virtual {p2, v0}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V
/* .line 383 */
v0 = (( android.os.Parcel ) p2 ).readBoolean ( ); // invoke-virtual {p2}, Landroid/os/Parcel;->readBoolean()Z
/* .line 384 */
/* .local v0, "enabled":Z */
(( android.os.Parcel ) p2 ).readStrongBinder ( ); // invoke-virtual {p2}, Landroid/os/Parcel;->readStrongBinder()Landroid/os/IBinder;
/* .line 385 */
/* .local v1, "mBinder":Landroid/os/IBinder; */
v2 = (( com.miui.server.stepcounter.StepCounterManagerService$BinderService ) p0 ).registerTreadmillSensor ( v0, v1 ); // invoke-virtual {p0, v0, v1}, Lcom/miui/server/stepcounter/StepCounterManagerService$BinderService;->registerTreadmillSensor(ZLandroid/os/IBinder;)Z
/* :pswitch_data_0 */
/* .packed-switch 0x0 */
/* :pswitch_2 */
/* :pswitch_1 */
/* :pswitch_0 */
} // .end packed-switch
} // .end method
protected void registerDeathCallbackLocked ( android.os.IBinder p0 ) {
/* .locals 3 */
/* .param p1, "token" # Landroid/os/IBinder; */
/* .line 472 */
v0 = this.this$0;
com.miui.server.stepcounter.StepCounterManagerService .-$$Nest$fgetmClientDeathCallbacks ( v0 );
v0 = (( java.util.HashMap ) v0 ).containsKey ( p1 ); // invoke-virtual {v0, p1}, Ljava/util/HashMap;->containsKey(Ljava/lang/Object;)Z
if ( v0 != null) { // if-eqz v0, :cond_0
/* .line 473 */
return;
/* .line 475 */
} // :cond_0
v0 = this.this$0;
com.miui.server.stepcounter.StepCounterManagerService .-$$Nest$fgetmClientDeathCallbacks ( v0 );
/* new-instance v1, Lcom/miui/server/stepcounter/StepCounterManagerService$ClientDeathCallback; */
v2 = this.this$0;
/* invoke-direct {v1, v2, p1}, Lcom/miui/server/stepcounter/StepCounterManagerService$ClientDeathCallback;-><init>(Lcom/miui/server/stepcounter/StepCounterManagerService;Landroid/os/IBinder;)V */
(( java.util.HashMap ) v0 ).put ( p1, v1 ); // invoke-virtual {v0, p1, v1}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;
/* .line 476 */
return;
} // .end method
public Boolean registerTreadmillSensor ( Boolean p0, android.os.IBinder p1 ) {
/* .locals 3 */
/* .param p1, "enabled" # Z */
/* .param p2, "token" # Landroid/os/IBinder; */
/* .line 399 */
android.os.Binder .clearCallingIdentity ( );
/* move-result-wide v0 */
/* .line 401 */
/* .local v0, "ident":J */
try { // :try_start_0
v2 = (( com.miui.server.stepcounter.StepCounterManagerService$BinderService ) p0 ).registerTreadmillSensorInternal ( p1, p2 ); // invoke-virtual {p0, p1, p2}, Lcom/miui/server/stepcounter/StepCounterManagerService$BinderService;->registerTreadmillSensorInternal(ZLandroid/os/IBinder;)Z
/* :try_end_0 */
/* .catchall {:try_start_0 ..:try_end_0} :catchall_0 */
/* .line 403 */
android.os.Binder .restoreCallingIdentity ( v0,v1 );
/* .line 401 */
/* .line 403 */
/* :catchall_0 */
/* move-exception v2 */
android.os.Binder .restoreCallingIdentity ( v0,v1 );
/* .line 404 */
/* throw v2 */
} // .end method
public Boolean registerTreadmillSensorInternal ( Boolean p0, android.os.IBinder p1 ) {
/* .locals 9 */
/* .param p1, "enabled" # Z */
/* .param p2, "token" # Landroid/os/IBinder; */
/* .line 408 */
v0 = this.this$0;
com.miui.server.stepcounter.StepCounterManagerService .-$$Nest$fgetmLocked ( v0 );
/* monitor-enter v0 */
/* .line 409 */
int v1 = 1; // const/4 v1, 0x1
if ( p1 != null) { // if-eqz p1, :cond_0
/* .line 410 */
try { // :try_start_0
	 (( com.miui.server.stepcounter.StepCounterManagerService$BinderService ) p0 ).registerDeathCallbackLocked ( p2 ); // invoke-virtual {p0, p2}, Lcom/miui/server/stepcounter/StepCounterManagerService$BinderService;->registerDeathCallbackLocked(Landroid/os/IBinder;)V
	 /* .line 411 */
	 v2 = this.this$0;
	 v2 = 	 com.miui.server.stepcounter.StepCounterManagerService .-$$Nest$fgetmTreadmillEnabled ( v2 );
	 /* if-nez v2, :cond_1 */
	 v2 = this.this$0;
	 com.miui.server.stepcounter.StepCounterManagerService .-$$Nest$fgetmClientDeathCallbacks ( v2 );
	 v2 = 	 (( java.util.HashMap ) v2 ).size ( ); // invoke-virtual {v2}, Ljava/util/HashMap;->size()I
	 /* if-ne v2, v1, :cond_1 */
	 v2 = this.this$0;
	 com.miui.server.stepcounter.StepCounterManagerService .-$$Nest$fgetmTreadmillSensor ( v2 );
	 if ( v2 != null) { // if-eqz v2, :cond_1
		 v2 = this.this$0;
		 com.miui.server.stepcounter.StepCounterManagerService .-$$Nest$fgetmTreadmillSensor ( v2 );
		 /* .line 412 */
		 (( android.hardware.Sensor ) v2 ).getName ( ); // invoke-virtual {v2}, Landroid/hardware/Sensor;->getName()Ljava/lang/String;
		 final String v3 = "oem_treadmill Wakeup"; // const-string v3, "oem_treadmill Wakeup"
		 v2 = 		 (( java.lang.String ) v2 ).equals ( v3 ); // invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z
		 if ( v2 != null) { // if-eqz v2, :cond_1
			 /* .line 413 */
			 v2 = this.this$0;
			 com.miui.server.stepcounter.StepCounterManagerService .-$$Nest$fputmTreadmillEnabled ( v2,v1 );
			 /* .line 414 */
			 v2 = this.this$0;
			 com.miui.server.stepcounter.StepCounterManagerService .-$$Nest$fgetmSensorManager ( v2 );
			 v2 = this.this$0;
			 com.miui.server.stepcounter.StepCounterManagerService .-$$Nest$fgetmTreadmillListener ( v2 );
			 v2 = this.this$0;
			 com.miui.server.stepcounter.StepCounterManagerService .-$$Nest$fgetmTreadmillSensor ( v2 );
			 int v6 = 3; // const/4 v6, 0x3
			 /* const v7, 0x3938700 */
			 v2 = this.this$0;
			 com.miui.server.stepcounter.StepCounterManagerService .-$$Nest$fgetmHandler ( v2 );
			 /* invoke-virtual/range {v3 ..v8}, Landroid/hardware/SensorManager;->registerListener(Landroid/hardware/SensorEventListener;Landroid/hardware/Sensor;IILandroid/os/Handler;)Z */
			 /* .line 416 */
			 final String v2 = "StepCounterManagerService"; // const-string v2, "StepCounterManagerService"
			 final String v3 = "TreadmillSensor register success!"; // const-string v3, "TreadmillSensor register success!"
			 android.util.Slog .d ( v2,v3 );
			 /* .line 419 */
		 } // :cond_0
		 (( com.miui.server.stepcounter.StepCounterManagerService$BinderService ) p0 ).unregisterDeathCallbackLocked ( p2 ); // invoke-virtual {p0, p2}, Lcom/miui/server/stepcounter/StepCounterManagerService$BinderService;->unregisterDeathCallbackLocked(Landroid/os/IBinder;)V
		 /* .line 420 */
		 v2 = this.this$0;
		 v2 = 		 com.miui.server.stepcounter.StepCounterManagerService .-$$Nest$fgetmTreadmillEnabled ( v2 );
		 if ( v2 != null) { // if-eqz v2, :cond_1
			 v2 = this.this$0;
			 com.miui.server.stepcounter.StepCounterManagerService .-$$Nest$fgetmClientDeathCallbacks ( v2 );
			 v2 = 			 (( java.util.HashMap ) v2 ).size ( ); // invoke-virtual {v2}, Ljava/util/HashMap;->size()I
			 /* if-nez v2, :cond_1 */
			 /* .line 421 */
			 v2 = this.this$0;
			 int v3 = 0; // const/4 v3, 0x0
			 com.miui.server.stepcounter.StepCounterManagerService .-$$Nest$fputmTreadmillEnabled ( v2,v3 );
			 /* .line 422 */
			 v2 = this.this$0;
			 com.miui.server.stepcounter.StepCounterManagerService .-$$Nest$fgetmSensorManager ( v2 );
			 v3 = this.this$0;
			 com.miui.server.stepcounter.StepCounterManagerService .-$$Nest$fgetmTreadmillListener ( v3 );
			 (( android.hardware.SensorManager ) v2 ).unregisterListener ( v3 ); // invoke-virtual {v2, v3}, Landroid/hardware/SensorManager;->unregisterListener(Landroid/hardware/SensorEventListener;)V
			 /* .line 423 */
			 final String v2 = "StepCounterManagerService"; // const-string v2, "StepCounterManagerService"
			 final String v3 = "TreadmillSensor unregister success!"; // const-string v3, "TreadmillSensor unregister success!"
			 android.util.Slog .d ( v2,v3 );
			 /* .line 426 */
		 } // :cond_1
	 } // :goto_0
	 /* monitor-exit v0 */
	 /* .line 427 */
	 /* :catchall_0 */
	 /* move-exception v1 */
	 /* monitor-exit v0 */
	 /* :try_end_0 */
	 /* .catchall {:try_start_0 ..:try_end_0} :catchall_0 */
	 /* throw v1 */
} // .end method
protected void unregisterDeathCallbackLocked ( android.os.IBinder p0 ) {
	 /* .locals 2 */
	 /* .param p1, "token" # Landroid/os/IBinder; */
	 /* .line 479 */
	 if ( p1 != null) { // if-eqz p1, :cond_0
		 v0 = this.this$0;
		 com.miui.server.stepcounter.StepCounterManagerService .-$$Nest$fgetmClientDeathCallbacks ( v0 );
		 v0 = 		 (( java.util.HashMap ) v0 ).containsKey ( p1 ); // invoke-virtual {v0, p1}, Ljava/util/HashMap;->containsKey(Ljava/lang/Object;)Z
		 if ( v0 != null) { // if-eqz v0, :cond_0
			 /* .line 480 */
			 v0 = this.this$0;
			 com.miui.server.stepcounter.StepCounterManagerService .-$$Nest$fgetmClientDeathCallbacks ( v0 );
			 (( java.util.HashMap ) v0 ).get ( p1 ); // invoke-virtual {v0, p1}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;
			 /* check-cast v0, Landroid/os/IBinder$DeathRecipient; */
			 int v1 = 0; // const/4 v1, 0x0
			 /* .line 481 */
			 v0 = this.this$0;
			 com.miui.server.stepcounter.StepCounterManagerService .-$$Nest$fgetmClientDeathCallbacks ( v0 );
			 (( java.util.HashMap ) v0 ).remove ( p1 ); // invoke-virtual {v0, p1}, Ljava/util/HashMap;->remove(Ljava/lang/Object;)Ljava/lang/Object;
			 /* .line 483 */
		 } // :cond_0
		 return;
	 } // .end method
