class com.miui.server.stepcounter.StepCounterManagerService$IntentReceiver extends android.content.BroadcastReceiver {
	 /* .source "StepCounterManagerService.java" */
	 /* # annotations */
	 /* .annotation system Ldalvik/annotation/EnclosingClass; */
	 /* value = Lcom/miui/server/stepcounter/StepCounterManagerService; */
} // .end annotation
/* .annotation system Ldalvik/annotation/InnerClass; */
/* accessFlags = 0x2 */
/* name = "IntentReceiver" */
} // .end annotation
/* # instance fields */
final com.miui.server.stepcounter.StepCounterManagerService this$0; //synthetic
/* # direct methods */
private com.miui.server.stepcounter.StepCounterManagerService$IntentReceiver ( ) {
/* .locals 0 */
/* .line 315 */
this.this$0 = p1;
/* invoke-direct {p0}, Landroid/content/BroadcastReceiver;-><init>()V */
return;
} // .end method
 com.miui.server.stepcounter.StepCounterManagerService$IntentReceiver ( ) { //synthethic
/* .locals 0 */
/* invoke-direct {p0, p1}, Lcom/miui/server/stepcounter/StepCounterManagerService$IntentReceiver;-><init>(Lcom/miui/server/stepcounter/StepCounterManagerService;)V */
return;
} // .end method
private void dealSleepModeChanged ( Integer p0 ) {
/* .locals 2 */
/* .param p1, "state" # I */
/* .line 328 */
int v0 = 2; // const/4 v0, 0x2
/* if-ne p1, v0, :cond_0 */
v0 = this.this$0;
v0 = com.miui.server.stepcounter.StepCounterManagerService .-$$Nest$fgetisRegisterDetectorListener ( v0 );
/* if-nez v0, :cond_0 */
/* .line 329 */
v0 = this.this$0;
v1 = com.miui.server.stepcounter.StepCounterManagerService .-$$Nest$mregisterDetectorListener ( v0 );
com.miui.server.stepcounter.StepCounterManagerService .-$$Nest$fputisRegisterDetectorListener ( v0,v1 );
/* .line 330 */
} // :cond_0
int v0 = 1; // const/4 v0, 0x1
/* if-ne p1, v0, :cond_1 */
v0 = this.this$0;
v0 = com.miui.server.stepcounter.StepCounterManagerService .-$$Nest$fgetisRegisterDetectorListener ( v0 );
if ( v0 != null) { // if-eqz v0, :cond_1
/* .line 331 */
v0 = this.this$0;
com.miui.server.stepcounter.StepCounterManagerService .-$$Nest$munregisterDetectorListener ( v0 );
/* .line 332 */
v0 = this.this$0;
int v1 = 0; // const/4 v1, 0x0
com.miui.server.stepcounter.StepCounterManagerService .-$$Nest$fputisRegisterDetectorListener ( v0,v1 );
/* .line 334 */
} // :cond_1
} // :goto_0
return;
} // .end method
/* # virtual methods */
public void onReceive ( android.content.Context p0, android.content.Intent p1 ) {
/* .locals 3 */
/* .param p1, "context" # Landroid/content/Context; */
/* .param p2, "intent" # Landroid/content/Intent; */
/* .line 318 */
if ( p2 != null) { // if-eqz p2, :cond_0
(( android.content.Intent ) p2 ).getAction ( ); // invoke-virtual {p2}, Landroid/content/Intent;->getAction()Ljava/lang/String;
} // :cond_0
int v0 = 0; // const/4 v0, 0x0
/* .line 319 */
/* .local v0, "action":Ljava/lang/String; */
} // :goto_0
final String v1 = "android.intent.action.TIME_SET"; // const-string v1, "android.intent.action.TIME_SET"
v1 = (( java.lang.String ) v1 ).equals ( v0 ); // invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z
if ( v1 != null) { // if-eqz v1, :cond_1
/* .line 320 */
com.miui.server.stepcounter.StepCounterManagerService .resetSystemBootTime ( );
/* .line 321 */
} // :cond_1
final String v1 = "com.miui.powerkeeper_sleep_changed"; // const-string v1, "com.miui.powerkeeper_sleep_changed"
v1 = (( java.lang.String ) v1 ).equals ( v0 ); // invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z
if ( v1 != null) { // if-eqz v1, :cond_2
/* .line 322 */
/* const-string/jumbo v1, "state" */
int v2 = -1; // const/4 v2, -0x1
v1 = (( android.content.Intent ) p2 ).getIntExtra ( v1, v2 ); // invoke-virtual {p2, v1, v2}, Landroid/content/Intent;->getIntExtra(Ljava/lang/String;I)I
/* .line 323 */
/* .local v1, "state":I */
/* invoke-direct {p0, v1}, Lcom/miui/server/stepcounter/StepCounterManagerService$IntentReceiver;->dealSleepModeChanged(I)V */
/* .line 325 */
} // .end local v1 # "state":I
} // :cond_2
} // :goto_1
return;
} // .end method
