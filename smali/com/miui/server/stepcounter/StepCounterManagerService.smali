.class public Lcom/miui/server/stepcounter/StepCounterManagerService;
.super Lcom/android/server/SystemService;
.source "StepCounterManagerService.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/miui/server/stepcounter/StepCounterManagerService$IntentReceiver;,
        Lcom/miui/server/stepcounter/StepCounterManagerService$StepCountHandler;,
        Lcom/miui/server/stepcounter/StepCounterManagerService$StepDetectorListener;,
        Lcom/miui/server/stepcounter/StepCounterManagerService$TreadmillListener;,
        Lcom/miui/server/stepcounter/StepCounterManagerService$LocalService;,
        Lcom/miui/server/stepcounter/StepCounterManagerService$BinderService;,
        Lcom/miui/server/stepcounter/StepCounterManagerService$Shell;,
        Lcom/miui/server/stepcounter/StepCounterManagerService$ClientDeathCallback;
    }
.end annotation


# static fields
.field private static final ACTION_SLEEP_CHANGED:Ljava/lang/String; = "com.miui.powerkeeper_sleep_changed"

.field private static final DEFAULT_DELAY:I = 0x493e0

.field private static final DEFAULT_RECORD_DELAY:I = 0x7530

.field private static final DUMP_NUMBER:I = 0x3e8

.field private static final EXTRA_STATE:Ljava/lang/String; = "state"

.field private static final FIRST_DELAY:I = 0x2710

.field private static final MSG_ADD_STEP:I = 0x1

.field private static final MSG_TRIM:I = 0x2

.field private static final MSG_TRIM_ALL:I = 0x4

.field private static final MSG_TRIM_ALL_FORCE:I = 0x8

.field private static final PROP_STEPS_SUPPORT:Ljava/lang/String; = "persist.sys.steps_provider"

.field private static final SENSOR_NAME:Ljava/lang/String; = "oem_treadmill  Wakeup"

.field private static final STATE_ENTER_SLEEP:I = 0x1

.field private static final STATE_EXIT_SLEEP:I = 0x2

.field private static SYSTEM_BOOT_TIME:J = 0x0L

.field private static final TAG:Ljava/lang/String; = "StepCounterManagerService"

.field private static final TYPE_TREADMILL:I = 0x1fa2661

.field private static sDEBUG:Z


# instance fields
.field private isRegisterDetectorListener:Z

.field private mClientDeathCallbacks:Ljava/util/HashMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/HashMap<",
            "Landroid/os/IBinder;",
            "Lcom/miui/server/stepcounter/StepCounterManagerService$ClientDeathCallback;",
            ">;"
        }
    .end annotation
.end field

.field private mContext:Landroid/content/Context;

.field private mDelay:I

.field private mHandler:Lcom/miui/server/stepcounter/StepCounterManagerService$StepCountHandler;

.field private mHaveStepSensor:Z

.field private mIntentReceiver:Lcom/miui/server/stepcounter/StepCounterManagerService$IntentReceiver;

.field private mLastTrimAllTimeInMills:J

.field private final mLocked:Ljava/lang/Object;

.field private mRecordDelay:I

.field private mResolver:Landroid/content/ContentResolver;

.field private mRunnable:Ljava/lang/Runnable;

.field private mSensor:Landroid/hardware/Sensor;

.field private mSensorManager:Landroid/hardware/SensorManager;

.field private mStepDetectorListener:Landroid/hardware/SensorEventListener;

.field private mStepList:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List<",
            "Lmiui/stepcounter/StepDetector;",
            ">;"
        }
    .end annotation
.end field

.field private mTreadmillEnabled:Z

.field private mTreadmillListener:Landroid/hardware/SensorEventListener;

.field private mTreadmillSensor:Landroid/hardware/Sensor;

.field private sCurrentPos:I


# direct methods
.method public static synthetic $r8$lambda$ZqqONIaXiqXVqU1sqdLpwr2VmmY(Lcom/miui/server/stepcounter/StepCounterManagerService;JLjava/lang/String;)V
    .locals 0

    invoke-direct {p0, p1, p2, p3}, Lcom/miui/server/stepcounter/StepCounterManagerService;->lambda$reportStepToOneTrack$0(JLjava/lang/String;)V

    return-void
.end method

.method static bridge synthetic -$$Nest$fgetisRegisterDetectorListener(Lcom/miui/server/stepcounter/StepCounterManagerService;)Z
    .locals 0

    iget-boolean p0, p0, Lcom/miui/server/stepcounter/StepCounterManagerService;->isRegisterDetectorListener:Z

    return p0
.end method

.method static bridge synthetic -$$Nest$fgetmClientDeathCallbacks(Lcom/miui/server/stepcounter/StepCounterManagerService;)Ljava/util/HashMap;
    .locals 0

    iget-object p0, p0, Lcom/miui/server/stepcounter/StepCounterManagerService;->mClientDeathCallbacks:Ljava/util/HashMap;

    return-object p0
.end method

.method static bridge synthetic -$$Nest$fgetmContext(Lcom/miui/server/stepcounter/StepCounterManagerService;)Landroid/content/Context;
    .locals 0

    iget-object p0, p0, Lcom/miui/server/stepcounter/StepCounterManagerService;->mContext:Landroid/content/Context;

    return-object p0
.end method

.method static bridge synthetic -$$Nest$fgetmDelay(Lcom/miui/server/stepcounter/StepCounterManagerService;)I
    .locals 0

    iget p0, p0, Lcom/miui/server/stepcounter/StepCounterManagerService;->mDelay:I

    return p0
.end method

.method static bridge synthetic -$$Nest$fgetmHandler(Lcom/miui/server/stepcounter/StepCounterManagerService;)Lcom/miui/server/stepcounter/StepCounterManagerService$StepCountHandler;
    .locals 0

    iget-object p0, p0, Lcom/miui/server/stepcounter/StepCounterManagerService;->mHandler:Lcom/miui/server/stepcounter/StepCounterManagerService$StepCountHandler;

    return-object p0
.end method

.method static bridge synthetic -$$Nest$fgetmHaveStepSensor(Lcom/miui/server/stepcounter/StepCounterManagerService;)Z
    .locals 0

    iget-boolean p0, p0, Lcom/miui/server/stepcounter/StepCounterManagerService;->mHaveStepSensor:Z

    return p0
.end method

.method static bridge synthetic -$$Nest$fgetmLocked(Lcom/miui/server/stepcounter/StepCounterManagerService;)Ljava/lang/Object;
    .locals 0

    iget-object p0, p0, Lcom/miui/server/stepcounter/StepCounterManagerService;->mLocked:Ljava/lang/Object;

    return-object p0
.end method

.method static bridge synthetic -$$Nest$fgetmSensorManager(Lcom/miui/server/stepcounter/StepCounterManagerService;)Landroid/hardware/SensorManager;
    .locals 0

    iget-object p0, p0, Lcom/miui/server/stepcounter/StepCounterManagerService;->mSensorManager:Landroid/hardware/SensorManager;

    return-object p0
.end method

.method static bridge synthetic -$$Nest$fgetmStepList(Lcom/miui/server/stepcounter/StepCounterManagerService;)Ljava/util/List;
    .locals 0

    iget-object p0, p0, Lcom/miui/server/stepcounter/StepCounterManagerService;->mStepList:Ljava/util/List;

    return-object p0
.end method

.method static bridge synthetic -$$Nest$fgetmTreadmillEnabled(Lcom/miui/server/stepcounter/StepCounterManagerService;)Z
    .locals 0

    iget-boolean p0, p0, Lcom/miui/server/stepcounter/StepCounterManagerService;->mTreadmillEnabled:Z

    return p0
.end method

.method static bridge synthetic -$$Nest$fgetmTreadmillListener(Lcom/miui/server/stepcounter/StepCounterManagerService;)Landroid/hardware/SensorEventListener;
    .locals 0

    iget-object p0, p0, Lcom/miui/server/stepcounter/StepCounterManagerService;->mTreadmillListener:Landroid/hardware/SensorEventListener;

    return-object p0
.end method

.method static bridge synthetic -$$Nest$fgetmTreadmillSensor(Lcom/miui/server/stepcounter/StepCounterManagerService;)Landroid/hardware/Sensor;
    .locals 0

    iget-object p0, p0, Lcom/miui/server/stepcounter/StepCounterManagerService;->mTreadmillSensor:Landroid/hardware/Sensor;

    return-object p0
.end method

.method static bridge synthetic -$$Nest$fputisRegisterDetectorListener(Lcom/miui/server/stepcounter/StepCounterManagerService;Z)V
    .locals 0

    iput-boolean p1, p0, Lcom/miui/server/stepcounter/StepCounterManagerService;->isRegisterDetectorListener:Z

    return-void
.end method

.method static bridge synthetic -$$Nest$fputmTreadmillEnabled(Lcom/miui/server/stepcounter/StepCounterManagerService;Z)V
    .locals 0

    iput-boolean p1, p0, Lcom/miui/server/stepcounter/StepCounterManagerService;->mTreadmillEnabled:Z

    return-void
.end method

.method static bridge synthetic -$$Nest$mdumpInternal(Lcom/miui/server/stepcounter/StepCounterManagerService;Ljava/io/PrintWriter;)V
    .locals 0

    invoke-direct {p0, p1}, Lcom/miui/server/stepcounter/StepCounterManagerService;->dumpInternal(Ljava/io/PrintWriter;)V

    return-void
.end method

.method static bridge synthetic -$$Nest$mregisterDetectorListener(Lcom/miui/server/stepcounter/StepCounterManagerService;)Z
    .locals 0

    invoke-direct {p0}, Lcom/miui/server/stepcounter/StepCounterManagerService;->registerDetectorListener()Z

    move-result p0

    return p0
.end method

.method static bridge synthetic -$$Nest$mreportStepToOneTrack(Lcom/miui/server/stepcounter/StepCounterManagerService;)V
    .locals 0

    invoke-direct {p0}, Lcom/miui/server/stepcounter/StepCounterManagerService;->reportStepToOneTrack()V

    return-void
.end method

.method static bridge synthetic -$$Nest$msetDebugEnabled(Lcom/miui/server/stepcounter/StepCounterManagerService;Z)V
    .locals 0

    invoke-direct {p0, p1}, Lcom/miui/server/stepcounter/StepCounterManagerService;->setDebugEnabled(Z)V

    return-void
.end method

.method static bridge synthetic -$$Nest$msetDelayValue(Lcom/miui/server/stepcounter/StepCounterManagerService;I)V
    .locals 0

    invoke-direct {p0, p1}, Lcom/miui/server/stepcounter/StepCounterManagerService;->setDelayValue(I)V

    return-void
.end method

.method static bridge synthetic -$$Nest$msetRecordDelayValue(Lcom/miui/server/stepcounter/StepCounterManagerService;I)V
    .locals 0

    invoke-direct {p0, p1}, Lcom/miui/server/stepcounter/StepCounterManagerService;->setRecordDelayValue(I)V

    return-void
.end method

.method static bridge synthetic -$$Nest$munregisterDetectorListener(Lcom/miui/server/stepcounter/StepCounterManagerService;)V
    .locals 0

    invoke-direct {p0}, Lcom/miui/server/stepcounter/StepCounterManagerService;->unregisterDetectorListener()V

    return-void
.end method

.method static bridge synthetic -$$Nest$sfgetSYSTEM_BOOT_TIME()J
    .locals 2

    sget-wide v0, Lcom/miui/server/stepcounter/StepCounterManagerService;->SYSTEM_BOOT_TIME:J

    return-wide v0
.end method

.method static bridge synthetic -$$Nest$sfgetsDEBUG()Z
    .locals 1

    sget-boolean v0, Lcom/miui/server/stepcounter/StepCounterManagerService;->sDEBUG:Z

    return v0
.end method

.method static constructor <clinit>()V
    .locals 4

    .line 59
    const/4 v0, 0x0

    sput-boolean v0, Lcom/miui/server/stepcounter/StepCounterManagerService;->sDEBUG:Z

    .line 83
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v0

    invoke-static {}, Landroid/os/SystemClock;->elapsedRealtime()J

    move-result-wide v2

    sub-long/2addr v0, v2

    sput-wide v0, Lcom/miui/server/stepcounter/StepCounterManagerService;->SYSTEM_BOOT_TIME:J

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;)V
    .locals 5
    .param p1, "context"    # Landroid/content/Context;

    .line 104
    invoke-direct {p0, p1}, Lcom/android/server/SystemService;-><init>(Landroid/content/Context;)V

    .line 60
    new-instance v0, Ljava/lang/Object;

    invoke-direct {v0}, Ljava/lang/Object;-><init>()V

    iput-object v0, p0, Lcom/miui/server/stepcounter/StepCounterManagerService;->mLocked:Ljava/lang/Object;

    .line 84
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/miui/server/stepcounter/StepCounterManagerService;->mHaveStepSensor:Z

    .line 85
    iput-boolean v0, p0, Lcom/miui/server/stepcounter/StepCounterManagerService;->isRegisterDetectorListener:Z

    .line 90
    const/4 v1, 0x0

    iput-object v1, p0, Lcom/miui/server/stepcounter/StepCounterManagerService;->mIntentReceiver:Lcom/miui/server/stepcounter/StepCounterManagerService$IntentReceiver;

    .line 101
    new-instance v2, Ljava/util/HashMap;

    invoke-direct {v2}, Ljava/util/HashMap;-><init>()V

    iput-object v2, p0, Lcom/miui/server/stepcounter/StepCounterManagerService;->mClientDeathCallbacks:Ljava/util/HashMap;

    .line 152
    new-instance v2, Lcom/miui/server/stepcounter/StepCounterManagerService$1;

    invoke-direct {v2, p0}, Lcom/miui/server/stepcounter/StepCounterManagerService$1;-><init>(Lcom/miui/server/stepcounter/StepCounterManagerService;)V

    iput-object v2, p0, Lcom/miui/server/stepcounter/StepCounterManagerService;->mRunnable:Ljava/lang/Runnable;

    .line 105
    iput-object p1, p0, Lcom/miui/server/stepcounter/StepCounterManagerService;->mContext:Landroid/content/Context;

    .line 106
    new-instance v2, Lcom/miui/server/stepcounter/StepCounterManagerService$StepCountHandler;

    invoke-static {}, Lcom/android/internal/os/BackgroundThread;->get()Lcom/android/internal/os/BackgroundThread;

    move-result-object v3

    invoke-virtual {v3}, Lcom/android/internal/os/BackgroundThread;->getLooper()Landroid/os/Looper;

    move-result-object v3

    invoke-direct {v2, p0, v3}, Lcom/miui/server/stepcounter/StepCounterManagerService$StepCountHandler;-><init>(Lcom/miui/server/stepcounter/StepCounterManagerService;Landroid/os/Looper;)V

    iput-object v2, p0, Lcom/miui/server/stepcounter/StepCounterManagerService;->mHandler:Lcom/miui/server/stepcounter/StepCounterManagerService$StepCountHandler;

    .line 107
    iget-object v2, p0, Lcom/miui/server/stepcounter/StepCounterManagerService;->mContext:Landroid/content/Context;

    invoke-virtual {v2}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v2

    iput-object v2, p0, Lcom/miui/server/stepcounter/StepCounterManagerService;->mResolver:Landroid/content/ContentResolver;

    .line 108
    iget-object v2, p0, Lcom/miui/server/stepcounter/StepCounterManagerService;->mContext:Landroid/content/Context;

    const-string/jumbo v3, "sensor"

    invoke-virtual {v2, v3}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Landroid/hardware/SensorManager;

    iput-object v2, p0, Lcom/miui/server/stepcounter/StepCounterManagerService;->mSensorManager:Landroid/hardware/SensorManager;

    .line 109
    const/16 v3, 0x12

    const/4 v4, 0x1

    invoke-virtual {v2, v3, v4}, Landroid/hardware/SensorManager;->getDefaultSensor(IZ)Landroid/hardware/Sensor;

    move-result-object v2

    iput-object v2, p0, Lcom/miui/server/stepcounter/StepCounterManagerService;->mSensor:Landroid/hardware/Sensor;

    .line 110
    iget-object v2, p0, Lcom/miui/server/stepcounter/StepCounterManagerService;->mSensorManager:Landroid/hardware/SensorManager;

    const v3, 0x1fa2661

    invoke-virtual {v2, v3, v4}, Landroid/hardware/SensorManager;->getDefaultSensor(IZ)Landroid/hardware/Sensor;

    move-result-object v2

    iput-object v2, p0, Lcom/miui/server/stepcounter/StepCounterManagerService;->mTreadmillSensor:Landroid/hardware/Sensor;

    .line 111
    new-instance v2, Lcom/miui/server/stepcounter/StepCounterManagerService$StepDetectorListener;

    invoke-direct {v2, p0, v1}, Lcom/miui/server/stepcounter/StepCounterManagerService$StepDetectorListener;-><init>(Lcom/miui/server/stepcounter/StepCounterManagerService;Lcom/miui/server/stepcounter/StepCounterManagerService$StepDetectorListener-IA;)V

    iput-object v2, p0, Lcom/miui/server/stepcounter/StepCounterManagerService;->mStepDetectorListener:Landroid/hardware/SensorEventListener;

    .line 112
    new-instance v2, Lcom/miui/server/stepcounter/StepCounterManagerService$TreadmillListener;

    invoke-direct {v2, p0, v1}, Lcom/miui/server/stepcounter/StepCounterManagerService$TreadmillListener;-><init>(Lcom/miui/server/stepcounter/StepCounterManagerService;Lcom/miui/server/stepcounter/StepCounterManagerService$TreadmillListener-IA;)V

    iput-object v2, p0, Lcom/miui/server/stepcounter/StepCounterManagerService;->mTreadmillListener:Landroid/hardware/SensorEventListener;

    .line 113
    new-instance v2, Lcom/miui/server/stepcounter/StepCounterManagerService$IntentReceiver;

    invoke-direct {v2, p0, v1}, Lcom/miui/server/stepcounter/StepCounterManagerService$IntentReceiver;-><init>(Lcom/miui/server/stepcounter/StepCounterManagerService;Lcom/miui/server/stepcounter/StepCounterManagerService$IntentReceiver-IA;)V

    iput-object v2, p0, Lcom/miui/server/stepcounter/StepCounterManagerService;->mIntentReceiver:Lcom/miui/server/stepcounter/StepCounterManagerService$IntentReceiver;

    .line 114
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    iput-object v1, p0, Lcom/miui/server/stepcounter/StepCounterManagerService;->mStepList:Ljava/util/List;

    .line 115
    const v1, 0x493e0

    iput v1, p0, Lcom/miui/server/stepcounter/StepCounterManagerService;->mDelay:I

    .line 116
    const/16 v1, 0x7530

    iput v1, p0, Lcom/miui/server/stepcounter/StepCounterManagerService;->mRecordDelay:I

    .line 117
    iput v0, p0, Lcom/miui/server/stepcounter/StepCounterManagerService;->sCurrentPos:I

    .line 118
    const-wide/16 v0, 0x0

    iput-wide v0, p0, Lcom/miui/server/stepcounter/StepCounterManagerService;->mLastTrimAllTimeInMills:J

    .line 120
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "Create StepCounterManagerService success "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-static {}, Landroid/os/UserHandle;->myUserId()I

    move-result v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    const-string v1, "StepCounterManagerService"

    invoke-static {v1, v0}, Landroid/util/Slog;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 121
    return-void
.end method

.method private dumpInternal(Ljava/io/PrintWriter;)V
    .locals 24
    .param p1, "pw"    # Ljava/io/PrintWriter;

    .line 514
    move-object/from16 v1, p0

    move-object/from16 v2, p1

    const-string v0, "Step Counter Manager Service(dumpsys miui_step_counter_service)\n"

    invoke-virtual {v2, v0}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V

    .line 515
    iget-object v3, v1, Lcom/miui/server/stepcounter/StepCounterManagerService;->mLocked:Ljava/lang/Object;

    monitor-enter v3

    .line 516
    :try_start_0
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "Delay: "

    invoke-virtual {v0, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget v4, v1, Lcom/miui/server/stepcounter/StepCounterManagerService;->mDelay:I

    div-int/lit16 v4, v4, 0x3e8

    invoke-virtual {v0, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v4, "s, default: "

    invoke-virtual {v0, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const/16 v4, 0x12c

    invoke-virtual {v0, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v4, "s"

    invoke-virtual {v0, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v2, v0}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V

    .line 517
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "Record Delay: "

    invoke-virtual {v0, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget v4, v1, Lcom/miui/server/stepcounter/StepCounterManagerService;->mRecordDelay:I

    div-int/lit16 v4, v4, 0x3e8

    invoke-virtual {v0, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v4, "s, default: "

    invoke-virtual {v0, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const/16 v4, 0x1e

    invoke-virtual {v0, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v4, "s"

    invoke-virtual {v0, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v2, v0}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V

    .line 518
    invoke-virtual/range {p1 .. p1}, Ljava/io/PrintWriter;->println()V

    .line 520
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "current step list cache size: "

    invoke-virtual {v0, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v4, v1, Lcom/miui/server/stepcounter/StepCounterManagerService;->mStepList:Ljava/util/List;

    invoke-interface {v4}, Ljava/util/List;->size()I

    move-result v4

    invoke-virtual {v0, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v2, v0}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V

    .line 521
    const/4 v0, 0x0

    .local v0, "i":I
    :goto_0
    iget-object v4, v1, Lcom/miui/server/stepcounter/StepCounterManagerService;->mStepList:Ljava/util/List;

    invoke-interface {v4}, Ljava/util/List;->size()I

    move-result v4

    if-ge v0, v4, :cond_0

    .line 522
    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    add-int/lit8 v5, v0, 0x1

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, " "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v2, v4}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    .line 523
    iget-object v4, v1, Lcom/miui/server/stepcounter/StepCounterManagerService;->mStepList:Ljava/util/List;

    invoke-interface {v4, v0}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lmiui/stepcounter/StepDetector;

    invoke-virtual {v4, v2}, Lmiui/stepcounter/StepDetector;->dump(Ljava/io/PrintWriter;)V

    .line 521
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 525
    .end local v0    # "i":I
    :cond_0
    invoke-virtual/range {p1 .. p1}, Ljava/io/PrintWriter;->println()V

    .line 528
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    .line 529
    .local v0, "mStepCellList":Ljava/util/List;, "Ljava/util/List<Lmiui/stepcounter/StepCell;>;"
    iget-object v4, v1, Lcom/miui/server/stepcounter/StepCounterManagerService;->mResolver:Landroid/content/ContentResolver;

    sget-object v5, Lmiui/stepcounter/StepSqlite;->CONTENT_URI:Landroid/net/Uri;

    sget-object v6, Lmiui/stepcounter/StepSqlite;->DEFAULT_PROJECTION:[Ljava/lang/String;

    const/4 v7, 0x0

    const/4 v8, 0x0

    const-string v9, "_id DESC"

    invoke-virtual/range {v4 .. v9}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v4

    .line 530
    .local v4, "cursor":Landroid/database/Cursor;
    const-wide/16 v5, 0x0

    .line 531
    .local v5, "mDumpRunStep":J
    const-wide/16 v7, 0x0

    .line 532
    .local v7, "mDumpWalkStep":J
    const-wide/16 v9, 0x0

    .line 533
    .local v9, "mDumpTreadmillStep":J
    invoke-direct/range {p0 .. p0}, Lcom/miui/server/stepcounter/StepCounterManagerService;->getTodayBeginTime()J

    move-result-wide v11

    .line 534
    .local v11, "mTodayBeginTime":J
    const/4 v13, 0x1

    if-eqz v4, :cond_5

    .line 535
    :goto_1
    invoke-interface {v4}, Landroid/database/Cursor;->moveToNext()Z

    move-result v14

    if-eqz v14, :cond_4

    .line 536
    const/4 v14, 0x2

    invoke-interface {v4, v14}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v15

    cmp-long v15, v15, v11

    if-lez v15, :cond_3

    .line 537
    new-instance v15, Lmiui/stepcounter/StepCell;

    const/4 v14, 0x0

    invoke-interface {v4, v14}, Landroid/database/Cursor;->getInt(I)I

    move-result v17

    .line 538
    invoke-interface {v4, v13}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v18

    const/4 v14, 0x2

    invoke-interface {v4, v14}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v20

    .line 539
    const/4 v14, 0x3

    invoke-interface {v4, v14}, Landroid/database/Cursor;->getInt(I)I

    move-result v22

    const/4 v13, 0x4

    invoke-interface {v4, v13}, Landroid/database/Cursor;->getInt(I)I

    move-result v23

    move-object/from16 v16, v15

    invoke-direct/range {v16 .. v23}, Lmiui/stepcounter/StepCell;-><init>(IJJII)V

    .line 537
    invoke-interface {v0, v15}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 541
    invoke-interface {v4, v14}, Landroid/database/Cursor;->getInt(I)I

    move-result v15

    const/4 v14, 0x2

    if-ne v15, v14, :cond_1

    .line 542
    invoke-interface {v4, v13}, Landroid/database/Cursor;->getInt(I)I

    move-result v13

    int-to-long v13, v13

    add-long/2addr v7, v13

    const/4 v13, 0x1

    goto :goto_1

    .line 543
    :cond_1
    const/4 v14, 0x3

    invoke-interface {v4, v14}, Landroid/database/Cursor;->getInt(I)I

    move-result v15

    if-ne v15, v14, :cond_2

    .line 544
    invoke-interface {v4, v13}, Landroid/database/Cursor;->getInt(I)I

    move-result v13

    int-to-long v13, v13

    add-long/2addr v5, v13

    const/4 v13, 0x1

    goto :goto_1

    .line 545
    :cond_2
    invoke-interface {v4, v14}, Landroid/database/Cursor;->getInt(I)I

    move-result v14

    if-ne v14, v13, :cond_3

    .line 546
    invoke-interface {v4, v13}, Landroid/database/Cursor;->getInt(I)I

    move-result v13

    int-to-long v13, v13

    add-long/2addr v9, v13

    const/4 v13, 0x1

    goto :goto_1

    .line 535
    :cond_3
    const/4 v13, 0x1

    goto :goto_1

    .line 550
    :cond_4
    invoke-interface {v4}, Landroid/database/Cursor;->close()V

    .line 552
    :cond_5
    const-string/jumbo v13, "step counter database info"

    invoke-virtual {v2, v13}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V

    .line 553
    new-instance v13, Ljava/lang/StringBuilder;

    invoke-direct {v13}, Ljava/lang/StringBuilder;-><init>()V

    const-string v14, "Today Total steps: "

    invoke-virtual {v13, v14}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v13

    add-long v14, v7, v5

    add-long/2addr v14, v9

    invoke-virtual {v13, v14, v15}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v13

    const-string v14, ", walk: "

    invoke-virtual {v13, v14}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v13

    invoke-virtual {v13, v7, v8}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v13

    const-string v14, ", run: "

    invoke-virtual {v13, v14}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v13

    invoke-virtual {v13, v5, v6}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v13

    const-string v14, ", treadmill: "

    invoke-virtual {v13, v14}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v13

    invoke-virtual {v13, v9, v10}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v13

    invoke-virtual {v13}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v13

    invoke-virtual {v2, v13}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V

    .line 557
    const-string v13, "Today step history info:"

    invoke-virtual {v2, v13}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V

    .line 558
    const/4 v13, 0x0

    .local v13, "i":I
    :goto_2
    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v14

    if-ge v13, v14, :cond_6

    .line 559
    new-instance v14, Ljava/lang/StringBuilder;

    invoke-direct {v14}, Ljava/lang/StringBuilder;-><init>()V

    add-int/lit8 v15, v13, 0x1

    invoke-virtual {v14, v15}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v14

    const-string v15, " "

    invoke-virtual {v14, v15}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v14

    invoke-virtual {v14}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v14

    invoke-virtual {v2, v14}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    .line 560
    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v14

    sub-int/2addr v14, v13

    const/4 v15, 0x1

    sub-int/2addr v14, v15

    invoke-interface {v0, v14}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v14

    check-cast v14, Lmiui/stepcounter/StepCell;

    invoke-virtual {v14, v2}, Lmiui/stepcounter/StepCell;->dump(Ljava/io/PrintWriter;)V

    .line 558
    add-int/lit8 v13, v13, 0x1

    goto :goto_2

    .line 562
    .end local v13    # "i":I
    :cond_6
    invoke-interface {v0}, Ljava/util/List;->clear()V

    .line 563
    .end local v0    # "mStepCellList":Ljava/util/List;, "Ljava/util/List<Lmiui/stepcounter/StepCell;>;"
    .end local v4    # "cursor":Landroid/database/Cursor;
    .end local v5    # "mDumpRunStep":J
    .end local v7    # "mDumpWalkStep":J
    .end local v9    # "mDumpTreadmillStep":J
    .end local v11    # "mTodayBeginTime":J
    monitor-exit v3

    .line 564
    return-void

    .line 563
    :catchall_0
    move-exception v0

    monitor-exit v3
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method

.method private getTodayBeginTime()J
    .locals 3

    .line 701
    invoke-static {}, Ljava/util/Calendar;->getInstance()Ljava/util/Calendar;

    move-result-object v0

    .line 702
    .local v0, "calendar":Ljava/util/Calendar;
    const/16 v1, 0xb

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Ljava/util/Calendar;->set(II)V

    .line 703
    const/16 v1, 0xc

    invoke-virtual {v0, v1, v2}, Ljava/util/Calendar;->set(II)V

    .line 704
    const/16 v1, 0xd

    invoke-virtual {v0, v1, v2}, Ljava/util/Calendar;->set(II)V

    .line 705
    const/16 v1, 0xe

    invoke-virtual {v0, v1, v2}, Ljava/util/Calendar;->set(II)V

    .line 706
    invoke-virtual {v0}, Ljava/util/Calendar;->getTimeInMillis()J

    move-result-wide v1

    return-wide v1
.end method

.method private getTodayEndTime()J
    .locals 3

    .line 710
    invoke-static {}, Ljava/util/Calendar;->getInstance()Ljava/util/Calendar;

    move-result-object v0

    .line 711
    .local v0, "calendar":Ljava/util/Calendar;
    const/16 v1, 0xb

    const/16 v2, 0x17

    invoke-virtual {v0, v1, v2}, Ljava/util/Calendar;->set(II)V

    .line 712
    const/16 v1, 0xc

    const/16 v2, 0x3b

    invoke-virtual {v0, v1, v2}, Ljava/util/Calendar;->set(II)V

    .line 713
    const/16 v1, 0xd

    invoke-virtual {v0, v1, v2}, Ljava/util/Calendar;->set(II)V

    .line 714
    const/16 v1, 0xe

    const/16 v2, 0x3e7

    invoke-virtual {v0, v1, v2}, Ljava/util/Calendar;->set(II)V

    .line 715
    invoke-virtual {v0}, Ljava/util/Calendar;->getTimeInMillis()J

    move-result-wide v1

    return-wide v1
.end method

.method private isEmpty()Z
    .locals 3

    .line 237
    iget-object v0, p0, Lcom/miui/server/stepcounter/StepCounterManagerService;->mStepList:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->isEmpty()Z

    move-result v0

    const/4 v1, 0x1

    if-nez v0, :cond_1

    iget v0, p0, Lcom/miui/server/stepcounter/StepCounterManagerService;->sCurrentPos:I

    iget-object v2, p0, Lcom/miui/server/stepcounter/StepCounterManagerService;->mStepList:Ljava/util/List;

    invoke-interface {v2}, Ljava/util/List;->size()I

    move-result v2

    sub-int/2addr v2, v1

    if-ne v0, v2, :cond_0

    goto :goto_0

    :cond_0
    const/4 v1, 0x0

    :cond_1
    :goto_0
    return v1
.end method

.method private synthetic lambda$reportStepToOneTrack$0(JLjava/lang/String;)V
    .locals 4
    .param p1, "mStep"    # J
    .param p3, "DEVICE_REGION"    # Ljava/lang/String;

    .line 749
    const-string v0, "StepCounterManagerService"

    :try_start_0
    new-instance v1, Landroid/content/Intent;

    const-string v2, "onetrack.action.TRACK_EVENT"

    invoke-direct {v1, v2}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 750
    .local v1, "intent":Landroid/content/Intent;
    const-string v2, "com.miui.analytics"

    invoke-virtual {v1, v2}, Landroid/content/Intent;->setPackage(Ljava/lang/String;)Landroid/content/Intent;

    .line 751
    const-string v2, "APP_ID"

    const-string v3, "31000000621"

    invoke-virtual {v1, v2, v3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 752
    const-string v2, "EVENT_NAME"

    const-string v3, "StepCount"

    invoke-virtual {v1, v2, v3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 753
    const-string v2, "PACKAGE"

    iget-object v3, p0, Lcom/miui/server/stepcounter/StepCounterManagerService;->mContext:Landroid/content/Context;

    invoke-virtual {v3}, Landroid/content/Context;->getPackageName()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v1, v2, v3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 754
    new-instance v2, Landroid/os/Bundle;

    invoke-direct {v2}, Landroid/os/Bundle;-><init>()V

    .line 755
    .local v2, "params":Landroid/os/Bundle;
    const-string/jumbo v3, "step_count"

    invoke-virtual {v2, v3, p1, p2}, Landroid/os/Bundle;->putLong(Ljava/lang/String;J)V

    .line 756
    const-string v3, "CN"

    invoke-virtual {p3, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_0

    .line 757
    const/4 v3, 0x2

    invoke-virtual {v1, v3}, Landroid/content/Intent;->setFlags(I)Landroid/content/Intent;

    .line 759
    :cond_0
    invoke-virtual {v1, v2}, Landroid/content/Intent;->putExtras(Landroid/os/Bundle;)Landroid/content/Intent;

    .line 760
    iget-object v3, p0, Lcom/miui/server/stepcounter/StepCounterManagerService;->mContext:Landroid/content/Context;

    invoke-virtual {v3, v1}, Landroid/content/Context;->startService(Landroid/content/Intent;)Landroid/content/ComponentName;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 763
    nop

    .end local v1    # "intent":Landroid/content/Intent;
    .end local v2    # "params":Landroid/os/Bundle;
    goto :goto_0

    .line 761
    :catch_0
    move-exception v1

    .line 762
    .local v1, "e":Ljava/lang/Exception;
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "error reportStepToOneTrack:"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v1}, Ljava/lang/Exception;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v0, v2}, Landroid/util/Slog;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 764
    .end local v1    # "e":Ljava/lang/Exception;
    :goto_0
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "reportStepToOneTrack step="

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1, p2}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Slog;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 765
    return-void
.end method

.method private registerDetectorListener()Z
    .locals 6

    .line 300
    iget-object v0, p0, Lcom/miui/server/stepcounter/StepCounterManagerService;->mSensorManager:Landroid/hardware/SensorManager;

    if-eqz v0, :cond_0

    .line 301
    iget-object v1, p0, Lcom/miui/server/stepcounter/StepCounterManagerService;->mStepDetectorListener:Landroid/hardware/SensorEventListener;

    iget-object v2, p0, Lcom/miui/server/stepcounter/StepCounterManagerService;->mSensor:Landroid/hardware/Sensor;

    const/4 v3, 0x3

    iget v4, p0, Lcom/miui/server/stepcounter/StepCounterManagerService;->mDelay:I

    mul-int/lit16 v4, v4, 0x3e8

    iget-object v5, p0, Lcom/miui/server/stepcounter/StepCounterManagerService;->mHandler:Lcom/miui/server/stepcounter/StepCounterManagerService$StepCountHandler;

    invoke-virtual/range {v0 .. v5}, Landroid/hardware/SensorManager;->registerListener(Landroid/hardware/SensorEventListener;Landroid/hardware/Sensor;IILandroid/os/Handler;)Z

    move-result v0

    return v0

    .line 304
    :cond_0
    const-string v0, "StepCounterManagerService"

    const-string v1, "StepDetector Sensor not available!"

    invoke-static {v0, v1}, Landroid/util/Slog;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 305
    const/4 v0, 0x0

    return v0
.end method

.method private registerReceiver()V
    .locals 4

    .line 147
    new-instance v0, Landroid/content/IntentFilter;

    const-string v1, "android.intent.action.TIME_SET"

    invoke-direct {v0, v1}, Landroid/content/IntentFilter;-><init>(Ljava/lang/String;)V

    .line 148
    .local v0, "filter":Landroid/content/IntentFilter;
    const-string v1, "com.miui.powerkeeper_sleep_changed"

    invoke-virtual {v0, v1}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    .line 149
    iget-object v1, p0, Lcom/miui/server/stepcounter/StepCounterManagerService;->mContext:Landroid/content/Context;

    iget-object v2, p0, Lcom/miui/server/stepcounter/StepCounterManagerService;->mIntentReceiver:Lcom/miui/server/stepcounter/StepCounterManagerService$IntentReceiver;

    const/4 v3, 0x2

    invoke-virtual {v1, v2, v0, v3}, Landroid/content/Context;->registerReceiver(Landroid/content/BroadcastReceiver;Landroid/content/IntentFilter;I)Landroid/content/Intent;

    .line 150
    return-void
.end method

.method private reportStepToOneTrack()V
    .locals 18

    .line 719
    move-object/from16 v0, p0

    const-string v1, "31000000621"

    .line 720
    .local v1, "EVENT_APP_ID":Ljava/lang/String;
    const-string v2, "StepCount"

    .line 721
    .local v2, "EVENT_NAME":Ljava/lang/String;
    const/4 v3, 0x2

    .line 722
    .local v3, "FLAG_NON_ANONYMOUS":I
    const-string v4, "onetrack.action.TRACK_EVENT"

    .line 723
    .local v4, "ONE_TRACK_ACTION":Ljava/lang/String;
    const-string v5, "ro.miui.region"

    const-string v6, "CN"

    invoke-static {v5, v6}, Landroid/os/SystemProperties;->get(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v5

    .line 725
    .local v5, "DEVICE_REGION":Ljava/lang/String;
    invoke-direct/range {p0 .. p0}, Lcom/miui/server/stepcounter/StepCounterManagerService;->getTodayEndTime()J

    move-result-wide v6

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v8

    sub-long/2addr v6, v8

    iget v8, v0, Lcom/miui/server/stepcounter/StepCounterManagerService;->mDelay:I

    int-to-long v8, v8

    cmp-long v6, v6, v8

    if-gtz v6, :cond_6

    .line 726
    invoke-direct/range {p0 .. p0}, Lcom/miui/server/stepcounter/StepCounterManagerService;->getTodayEndTime()J

    move-result-wide v6

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v8

    sub-long/2addr v6, v8

    const-wide/16 v8, 0x0

    cmp-long v6, v6, v8

    if-lez v6, :cond_5

    .line 728
    const-wide/16 v6, 0x0

    .line 729
    .local v6, "mDumpRunStep":J
    const-wide/16 v8, 0x0

    .line 730
    .local v8, "mDumpWalkStep":J
    const-wide/16 v10, 0x0

    .line 731
    .local v10, "mDumpTreadmillStep":J
    iget-object v12, v0, Lcom/miui/server/stepcounter/StepCounterManagerService;->mResolver:Landroid/content/ContentResolver;

    sget-object v13, Lmiui/stepcounter/StepSqlite;->CONTENT_URI:Landroid/net/Uri;

    sget-object v14, Lmiui/stepcounter/StepSqlite;->DEFAULT_PROJECTION:[Ljava/lang/String;

    const/4 v15, 0x0

    const/16 v16, 0x0

    const-string v17, "_id DESC"

    invoke-virtual/range {v12 .. v17}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v12

    .line 732
    .local v12, "cursor":Landroid/database/Cursor;
    if-eqz v12, :cond_4

    .line 733
    :cond_0
    :goto_0
    invoke-interface {v12}, Landroid/database/Cursor;->moveToNext()Z

    move-result v13

    if-eqz v13, :cond_3

    .line 734
    const/4 v13, 0x2

    invoke-interface {v12, v13}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v14

    invoke-direct/range {p0 .. p0}, Lcom/miui/server/stepcounter/StepCounterManagerService;->getTodayBeginTime()J

    move-result-wide v16

    cmp-long v14, v14, v16

    if-lez v14, :cond_0

    .line 735
    const/4 v14, 0x3

    invoke-interface {v12, v14}, Landroid/database/Cursor;->getInt(I)I

    move-result v15

    const/4 v14, 0x4

    if-ne v15, v13, :cond_1

    .line 736
    invoke-interface {v12, v14}, Landroid/database/Cursor;->getInt(I)I

    move-result v13

    int-to-long v13, v13

    add-long/2addr v8, v13

    goto :goto_0

    .line 737
    :cond_1
    const/4 v13, 0x3

    invoke-interface {v12, v13}, Landroid/database/Cursor;->getInt(I)I

    move-result v15

    if-ne v15, v13, :cond_2

    .line 738
    invoke-interface {v12, v14}, Landroid/database/Cursor;->getInt(I)I

    move-result v13

    int-to-long v13, v13

    add-long/2addr v6, v13

    goto :goto_0

    .line 739
    :cond_2
    invoke-interface {v12, v13}, Landroid/database/Cursor;->getInt(I)I

    move-result v13

    if-ne v13, v14, :cond_0

    .line 740
    invoke-interface {v12, v14}, Landroid/database/Cursor;->getInt(I)I

    move-result v13

    int-to-long v13, v13

    add-long/2addr v10, v13

    goto :goto_0

    .line 744
    :cond_3
    invoke-interface {v12}, Landroid/database/Cursor;->close()V

    .line 746
    :cond_4
    add-long v13, v8, v6

    add-long/2addr v13, v10

    .line 747
    .local v13, "mStep":J
    invoke-static {}, Lcom/android/internal/os/BackgroundThread;->getHandler()Landroid/os/Handler;

    move-result-object v15

    move-object/from16 v16, v1

    .end local v1    # "EVENT_APP_ID":Ljava/lang/String;
    .local v16, "EVENT_APP_ID":Ljava/lang/String;
    new-instance v1, Lcom/miui/server/stepcounter/StepCounterManagerService$$ExternalSyntheticLambda0;

    invoke-direct {v1, v0, v13, v14, v5}, Lcom/miui/server/stepcounter/StepCounterManagerService$$ExternalSyntheticLambda0;-><init>(Lcom/miui/server/stepcounter/StepCounterManagerService;JLjava/lang/String;)V

    invoke-virtual {v15, v1}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    goto :goto_1

    .line 726
    .end local v6    # "mDumpRunStep":J
    .end local v8    # "mDumpWalkStep":J
    .end local v10    # "mDumpTreadmillStep":J
    .end local v12    # "cursor":Landroid/database/Cursor;
    .end local v13    # "mStep":J
    .end local v16    # "EVENT_APP_ID":Ljava/lang/String;
    .restart local v1    # "EVENT_APP_ID":Ljava/lang/String;
    :cond_5
    move-object/from16 v16, v1

    .end local v1    # "EVENT_APP_ID":Ljava/lang/String;
    .restart local v16    # "EVENT_APP_ID":Ljava/lang/String;
    goto :goto_1

    .line 725
    .end local v16    # "EVENT_APP_ID":Ljava/lang/String;
    .restart local v1    # "EVENT_APP_ID":Ljava/lang/String;
    :cond_6
    move-object/from16 v16, v1

    .line 767
    .end local v1    # "EVENT_APP_ID":Ljava/lang/String;
    .restart local v16    # "EVENT_APP_ID":Ljava/lang/String;
    :goto_1
    return-void
.end method

.method public static resetSystemBootTime()V
    .locals 4

    .line 296
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v0

    invoke-static {}, Landroid/os/SystemClock;->elapsedRealtime()J

    move-result-wide v2

    sub-long/2addr v0, v2

    sput-wide v0, Lcom/miui/server/stepcounter/StepCounterManagerService;->SYSTEM_BOOT_TIME:J

    .line 297
    return-void
.end method

.method private setDebugEnabled(Z)V
    .locals 4
    .param p1, "enabled"    # Z

    .line 688
    iget-object v0, p0, Lcom/miui/server/stepcounter/StepCounterManagerService;->mLocked:Ljava/lang/Object;

    monitor-enter v0

    .line 689
    :try_start_0
    invoke-static {}, Landroid/os/Binder;->clearCallingIdentity()J

    move-result-wide v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 691
    .local v1, "ident":J
    :try_start_1
    sput-boolean p1, Lcom/miui/server/stepcounter/StepCounterManagerService;->sDEBUG:Z

    .line 692
    invoke-static {p1}, Lmiui/stepcounter/StepProvider;->updateDebug(Z)V

    .line 693
    invoke-static {p1}, Lmiui/stepcounter/StepMode;->updateDebug(Z)V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 695
    :try_start_2
    invoke-static {v1, v2}, Landroid/os/Binder;->restoreCallingIdentity(J)V

    .line 696
    nop

    .line 697
    .end local v1    # "ident":J
    monitor-exit v0

    .line 698
    return-void

    .line 695
    .restart local v1    # "ident":J
    :catchall_0
    move-exception v3

    invoke-static {v1, v2}, Landroid/os/Binder;->restoreCallingIdentity(J)V

    .line 696
    nop

    .end local p0    # "this":Lcom/miui/server/stepcounter/StepCounterManagerService;
    .end local p1    # "enabled":Z
    throw v3

    .line 697
    .end local v1    # "ident":J
    .restart local p0    # "this":Lcom/miui/server/stepcounter/StepCounterManagerService;
    .restart local p1    # "enabled":Z
    :catchall_1
    move-exception v1

    monitor-exit v0
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    throw v1
.end method

.method private setDelayValue(I)V
    .locals 1
    .param p1, "delay"    # I

    .line 680
    mul-int/lit16 v0, p1, 0x3e8

    iput v0, p0, Lcom/miui/server/stepcounter/StepCounterManagerService;->mDelay:I

    .line 681
    return-void
.end method

.method private setRecordDelayValue(I)V
    .locals 1
    .param p1, "recordDelay"    # I

    .line 684
    mul-int/lit16 v0, p1, 0x3e8

    iput v0, p0, Lcom/miui/server/stepcounter/StepCounterManagerService;->mRecordDelay:I

    .line 685
    return-void
.end method

.method private trim()V
    .locals 9

    .line 203
    iget v0, p0, Lcom/miui/server/stepcounter/StepCounterManagerService;->sCurrentPos:I

    .line 204
    .local v0, "begin":I
    iget-object v1, p0, Lcom/miui/server/stepcounter/StepCounterManagerService;->mStepList:Ljava/util/List;

    iget v2, p0, Lcom/miui/server/stepcounter/StepCounterManagerService;->sCurrentPos:I

    invoke-interface {v1, v2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lmiui/stepcounter/StepDetector;

    .line 206
    .local v1, "head":Lmiui/stepcounter/StepDetector;
    :goto_0
    iget-object v2, p0, Lcom/miui/server/stepcounter/StepCounterManagerService;->mStepList:Ljava/util/List;

    invoke-interface {v2}, Ljava/util/List;->isEmpty()Z

    move-result v2

    const-string v3, "StepCounterManagerService"

    if-nez v2, :cond_4

    .line 207
    iget-object v2, p0, Lcom/miui/server/stepcounter/StepCounterManagerService;->mStepList:Ljava/util/List;

    iget v4, p0, Lcom/miui/server/stepcounter/StepCounterManagerService;->sCurrentPos:I

    invoke-interface {v2, v4}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lmiui/stepcounter/StepDetector;

    .line 208
    .local v2, "tail":Lmiui/stepcounter/StepDetector;
    if-eqz v2, :cond_3

    if-eqz v1, :cond_3

    .line 209
    invoke-virtual {v2}, Lmiui/stepcounter/StepDetector;->getTimestamp()J

    move-result-wide v4

    invoke-virtual {v1}, Lmiui/stepcounter/StepDetector;->getTimestamp()J

    move-result-wide v6

    sub-long/2addr v4, v6

    .line 211
    .local v4, "deltaTime":J
    iget v6, p0, Lcom/miui/server/stepcounter/StepCounterManagerService;->mRecordDelay:I

    int-to-long v7, v6

    cmp-long v7, v4, v7

    if-lez v7, :cond_0

    iget v7, p0, Lcom/miui/server/stepcounter/StepCounterManagerService;->sCurrentPos:I

    if-lez v7, :cond_0

    .line 212
    add-int/lit8 v7, v7, -0x1

    iput v7, p0, Lcom/miui/server/stepcounter/StepCounterManagerService;->sCurrentPos:I

    .line 213
    :cond_0
    int-to-long v6, v6

    cmp-long v6, v4, v6

    if-gez v6, :cond_2

    invoke-direct {p0}, Lcom/miui/server/stepcounter/StepCounterManagerService;->isEmpty()Z

    move-result v6

    if-eqz v6, :cond_1

    goto :goto_1

    .line 226
    :cond_1
    iget v3, p0, Lcom/miui/server/stepcounter/StepCounterManagerService;->sCurrentPos:I

    add-int/lit8 v3, v3, 0x1

    iput v3, p0, Lcom/miui/server/stepcounter/StepCounterManagerService;->sCurrentPos:I

    .line 227
    .end local v4    # "deltaTime":J
    nop

    .line 230
    .end local v2    # "tail":Lmiui/stepcounter/StepDetector;
    goto :goto_0

    .line 215
    .restart local v2    # "tail":Lmiui/stepcounter/StepDetector;
    .restart local v4    # "deltaTime":J
    :cond_2
    :goto_1
    :try_start_0
    new-instance v6, Lmiui/stepcounter/StepMode;

    iget-object v7, p0, Lcom/miui/server/stepcounter/StepCounterManagerService;->mStepList:Ljava/util/List;

    iget v8, p0, Lcom/miui/server/stepcounter/StepCounterManagerService;->sCurrentPos:I

    invoke-direct {v6, v7, v0, v8}, Lmiui/stepcounter/StepMode;-><init>(Ljava/util/List;II)V

    iget-object v7, p0, Lcom/miui/server/stepcounter/StepCounterManagerService;->mResolver:Landroid/content/ContentResolver;

    invoke-virtual {v6, v7}, Lmiui/stepcounter/StepMode;->run(Landroid/content/ContentResolver;)V
    :try_end_0
    .catch Ljava/lang/IllegalArgumentException; {:try_start_0 .. :try_end_0} :catch_0

    .line 219
    nop

    .line 221
    invoke-direct {p0}, Lcom/miui/server/stepcounter/StepCounterManagerService;->isEmpty()Z

    move-result v6

    if-nez v6, :cond_4

    .line 222
    iget v6, p0, Lcom/miui/server/stepcounter/StepCounterManagerService;->sCurrentPos:I

    add-int/lit8 v6, v6, 0x1

    iput v6, p0, Lcom/miui/server/stepcounter/StepCounterManagerService;->sCurrentPos:I

    goto :goto_2

    .line 216
    :catch_0
    move-exception v6

    .line 217
    .local v6, "e":Ljava/lang/IllegalArgumentException;
    const-string v7, "Illegal argument"

    invoke-static {v3, v7}, Landroid/util/Slog;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 218
    return-void

    .line 228
    .end local v4    # "deltaTime":J
    .end local v6    # "e":Ljava/lang/IllegalArgumentException;
    :cond_3
    return-void

    .line 231
    .end local v2    # "tail":Lmiui/stepcounter/StepDetector;
    :cond_4
    :goto_2
    sget-boolean v2, Lcom/miui/server/stepcounter/StepCounterManagerService;->sDEBUG:Z

    if-eqz v2, :cond_5

    .line 232
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v4, "step current position start at "

    invoke-virtual {v2, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v4, " end at "

    invoke-virtual {v2, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget v4, p0, Lcom/miui/server/stepcounter/StepCounterManagerService;->sCurrentPos:I

    invoke-virtual {v2, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v3, v2}, Landroid/util/Slog;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 234
    :cond_5
    return-void
.end method

.method private unregisterDetectorListener()V
    .locals 2

    .line 310
    iget-object v0, p0, Lcom/miui/server/stepcounter/StepCounterManagerService;->mSensorManager:Landroid/hardware/SensorManager;

    if-eqz v0, :cond_0

    .line 311
    iget-object v1, p0, Lcom/miui/server/stepcounter/StepCounterManagerService;->mStepDetectorListener:Landroid/hardware/SensorEventListener;

    invoke-virtual {v0, v1}, Landroid/hardware/SensorManager;->unregisterListener(Landroid/hardware/SensorEventListener;)V

    .line 313
    :cond_0
    return-void
.end method


# virtual methods
.method public getLatestDataInternal(Z)V
    .locals 2
    .param p1, "isForce"    # Z

    .line 342
    iget-boolean v0, p0, Lcom/miui/server/stepcounter/StepCounterManagerService;->mHaveStepSensor:Z

    if-eqz v0, :cond_2

    .line 343
    sget-boolean v0, Lcom/miui/server/stepcounter/StepCounterManagerService;->sDEBUG:Z

    if-eqz v0, :cond_0

    .line 344
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "getLatestDataInternal: isForce? "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    const-string v1, "StepCounterManagerService"

    invoke-static {v1, v0}, Landroid/util/Slog;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 346
    :cond_0
    if-eqz p1, :cond_1

    .line 347
    iget-object v0, p0, Lcom/miui/server/stepcounter/StepCounterManagerService;->mHandler:Lcom/miui/server/stepcounter/StepCounterManagerService$StepCountHandler;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Lcom/miui/server/stepcounter/StepCounterManagerService$StepCountHandler;->sendEmptyMessage(I)Z

    goto :goto_0

    .line 349
    :cond_1
    iget-object v0, p0, Lcom/miui/server/stepcounter/StepCounterManagerService;->mHandler:Lcom/miui/server/stepcounter/StepCounterManagerService$StepCountHandler;

    const/4 v1, 0x4

    invoke-virtual {v0, v1}, Lcom/miui/server/stepcounter/StepCounterManagerService$StepCountHandler;->sendEmptyMessage(I)Z

    .line 351
    :cond_2
    :goto_0
    return-void
.end method

.method public haveStepSensorInternal()Z
    .locals 1

    .line 338
    iget-boolean v0, p0, Lcom/miui/server/stepcounter/StepCounterManagerService;->mHaveStepSensor:Z

    return v0
.end method

.method public onBootPhase(I)V
    .locals 0
    .param p1, "phase"    # I

    .line 143
    invoke-super {p0, p1}, Lcom/android/server/SystemService;->onBootPhase(I)V

    .line 144
    return-void
.end method

.method public onStart()V
    .locals 5

    .line 125
    const-string/jumbo v0, "support_steps_provider"

    const/4 v1, 0x0

    invoke-static {v0, v1}, Lmiui/util/FeatureParser;->getBoolean(Ljava/lang/String;Z)Z

    move-result v0

    iput-boolean v0, p0, Lcom/miui/server/stepcounter/StepCounterManagerService;->mHaveStepSensor:Z

    .line 126
    if-eqz v0, :cond_0

    .line 127
    const-string v0, "persist.sys.steps_provider"

    const/4 v1, 0x1

    invoke-static {v0, v1}, Landroid/os/SystemProperties;->getBoolean(Ljava/lang/String;Z)Z

    move-result v0

    iput-boolean v0, p0, Lcom/miui/server/stepcounter/StepCounterManagerService;->mHaveStepSensor:Z

    .line 129
    :cond_0
    iget-boolean v0, p0, Lcom/miui/server/stepcounter/StepCounterManagerService;->mHaveStepSensor:Z

    const-string v1, "StepCounterManagerService"

    if-nez v0, :cond_1

    .line 130
    const-string v0, "StepDetector Sensor not support"

    invoke-static {v1, v0}, Landroid/util/Slog;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 131
    return-void

    .line 133
    :cond_1
    const-class v0, Lmiui/stepcounter/StepCounterManagerInternal;

    new-instance v2, Lcom/miui/server/stepcounter/StepCounterManagerService$LocalService;

    invoke-direct {v2, p0}, Lcom/miui/server/stepcounter/StepCounterManagerService$LocalService;-><init>(Lcom/miui/server/stepcounter/StepCounterManagerService;)V

    invoke-virtual {p0, v0, v2}, Lcom/miui/server/stepcounter/StepCounterManagerService;->publishLocalService(Ljava/lang/Class;Ljava/lang/Object;)V

    .line 134
    new-instance v0, Lcom/miui/server/stepcounter/StepCounterManagerService$BinderService;

    const/4 v2, 0x0

    invoke-direct {v0, p0, v2}, Lcom/miui/server/stepcounter/StepCounterManagerService$BinderService;-><init>(Lcom/miui/server/stepcounter/StepCounterManagerService;Lcom/miui/server/stepcounter/StepCounterManagerService$BinderService-IA;)V

    const-string v2, "miui_step_counter_service"

    invoke-virtual {p0, v2, v0}, Lcom/miui/server/stepcounter/StepCounterManagerService;->publishBinderService(Ljava/lang/String;Landroid/os/IBinder;)V

    .line 135
    invoke-direct {p0}, Lcom/miui/server/stepcounter/StepCounterManagerService;->registerDetectorListener()Z

    move-result v0

    iput-boolean v0, p0, Lcom/miui/server/stepcounter/StepCounterManagerService;->isRegisterDetectorListener:Z

    .line 136
    invoke-direct {p0}, Lcom/miui/server/stepcounter/StepCounterManagerService;->registerReceiver()V

    .line 137
    iget-object v0, p0, Lcom/miui/server/stepcounter/StepCounterManagerService;->mHandler:Lcom/miui/server/stepcounter/StepCounterManagerService$StepCountHandler;

    iget-object v2, p0, Lcom/miui/server/stepcounter/StepCounterManagerService;->mRunnable:Ljava/lang/Runnable;

    const-wide/16 v3, 0x2710

    invoke-virtual {v0, v2, v3, v4}, Lcom/miui/server/stepcounter/StepCounterManagerService$StepCountHandler;->postDelayed(Ljava/lang/Runnable;J)Z

    .line 138
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v2, "step_debug onStart success isRegisterDetectorListener="

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-boolean v2, p0, Lcom/miui/server/stepcounter/StepCounterManagerService;->isRegisterDetectorListener:Z

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v1, v0}, Landroid/util/Slog;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 139
    return-void
.end method

.method public trimAll(Z)V
    .locals 4
    .param p1, "force"    # Z

    .line 189
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v0

    iget-wide v2, p0, Lcom/miui/server/stepcounter/StepCounterManagerService;->mLastTrimAllTimeInMills:J

    sub-long/2addr v0, v2

    iget v2, p0, Lcom/miui/server/stepcounter/StepCounterManagerService;->mRecordDelay:I

    int-to-long v2, v2

    cmp-long v0, v0, v2

    const/4 v1, 0x0

    if-ltz v0, :cond_0

    const/4 v0, 0x1

    goto :goto_0

    :cond_0
    move v0, v1

    .line 190
    .local v0, "pass":Z
    :goto_0
    if-eqz v0, :cond_1

    .line 191
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v2

    iput-wide v2, p0, Lcom/miui/server/stepcounter/StepCounterManagerService;->mLastTrimAllTimeInMills:J

    .line 193
    :cond_1
    if-nez p1, :cond_2

    if-eqz v0, :cond_4

    :cond_2
    iget-object v2, p0, Lcom/miui/server/stepcounter/StepCounterManagerService;->mContext:Landroid/content/Context;

    invoke-static {v2}, Landroid/os/UserManager;->get(Landroid/content/Context;)Landroid/os/UserManager;

    move-result-object v2

    invoke-virtual {v2}, Landroid/os/UserManager;->isUserUnlocked()Z

    move-result v2

    if-eqz v2, :cond_4

    .line 194
    :goto_1
    invoke-direct {p0}, Lcom/miui/server/stepcounter/StepCounterManagerService;->isEmpty()Z

    move-result v2

    if-nez v2, :cond_3

    .line 195
    invoke-direct {p0}, Lcom/miui/server/stepcounter/StepCounterManagerService;->trim()V

    goto :goto_1

    .line 197
    :cond_3
    iget-object v2, p0, Lcom/miui/server/stepcounter/StepCounterManagerService;->mStepList:Ljava/util/List;

    invoke-interface {v2}, Ljava/util/List;->clear()V

    .line 198
    iput v1, p0, Lcom/miui/server/stepcounter/StepCounterManagerService;->sCurrentPos:I

    .line 200
    :cond_4
    return-void
.end method
