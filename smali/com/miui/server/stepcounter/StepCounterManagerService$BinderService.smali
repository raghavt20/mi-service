.class Lcom/miui/server/stepcounter/StepCounterManagerService$BinderService;
.super Landroid/os/Binder;
.source "StepCounterManagerService.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/miui/server/stepcounter/StepCounterManagerService;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "BinderService"
.end annotation


# static fields
.field private static final GET_LATEST_DATA:I = 0x2

.field private static final HAVE_STEP_SENSOR:I = 0x1

.field private static final RECEIVE_TREADMILL:I = 0x0

.field public static final SERVICE_NAME:Ljava/lang/String; = "miui_step_counter_service"


# instance fields
.field final synthetic this$0:Lcom/miui/server/stepcounter/StepCounterManagerService;


# direct methods
.method private constructor <init>(Lcom/miui/server/stepcounter/StepCounterManagerService;)V
    .locals 0

    .line 353
    iput-object p1, p0, Lcom/miui/server/stepcounter/StepCounterManagerService$BinderService;->this$0:Lcom/miui/server/stepcounter/StepCounterManagerService;

    invoke-direct {p0}, Landroid/os/Binder;-><init>()V

    return-void
.end method

.method synthetic constructor <init>(Lcom/miui/server/stepcounter/StepCounterManagerService;Lcom/miui/server/stepcounter/StepCounterManagerService$BinderService-IA;)V
    .locals 0

    invoke-direct {p0, p1}, Lcom/miui/server/stepcounter/StepCounterManagerService$BinderService;-><init>(Lcom/miui/server/stepcounter/StepCounterManagerService;)V

    return-void
.end method


# virtual methods
.method protected dump(Ljava/io/FileDescriptor;Ljava/io/PrintWriter;[Ljava/lang/String;)V
    .locals 3
    .param p1, "fd"    # Ljava/io/FileDescriptor;
    .param p2, "pw"    # Ljava/io/PrintWriter;
    .param p3, "args"    # [Ljava/lang/String;

    .line 368
    iget-object v0, p0, Lcom/miui/server/stepcounter/StepCounterManagerService$BinderService;->this$0:Lcom/miui/server/stepcounter/StepCounterManagerService;

    invoke-static {v0}, Lcom/miui/server/stepcounter/StepCounterManagerService;->-$$Nest$fgetmContext(Lcom/miui/server/stepcounter/StepCounterManagerService;)Landroid/content/Context;

    move-result-object v0

    const-string v1, "StepCounterManagerService"

    invoke-static {v0, v1, p2}, Lcom/android/internal/util/DumpUtils;->checkDumpPermission(Landroid/content/Context;Ljava/lang/String;Ljava/io/PrintWriter;)Z

    move-result v0

    if-nez v0, :cond_0

    return-void

    .line 369
    :cond_0
    invoke-static {}, Landroid/os/Binder;->clearCallingIdentity()J

    move-result-wide v0

    .line 371
    .local v0, "ident":J
    :try_start_0
    iget-object v2, p0, Lcom/miui/server/stepcounter/StepCounterManagerService$BinderService;->this$0:Lcom/miui/server/stepcounter/StepCounterManagerService;

    invoke-static {v2, p2}, Lcom/miui/server/stepcounter/StepCounterManagerService;->-$$Nest$mdumpInternal(Lcom/miui/server/stepcounter/StepCounterManagerService;Ljava/io/PrintWriter;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 373
    invoke-static {v0, v1}, Landroid/os/Binder;->restoreCallingIdentity(J)V

    .line 374
    nop

    .line 375
    return-void

    .line 373
    :catchall_0
    move-exception v2

    invoke-static {v0, v1}, Landroid/os/Binder;->restoreCallingIdentity(J)V

    .line 374
    throw v2
.end method

.method public getLatestData(Z)Z
    .locals 5
    .param p1, "isForce"    # Z

    .line 447
    invoke-static {}, Landroid/os/Binder;->clearCallingIdentity()J

    move-result-wide v0

    .line 449
    .local v0, "ident":J
    :try_start_0
    invoke-static {}, Lcom/miui/server/stepcounter/StepCounterManagerService;->-$$Nest$sfgetsDEBUG()Z

    move-result v2

    if-eqz v2, :cond_0

    .line 450
    const-string v2, "StepCounterManagerService"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "getLatestDataInternal: isForce? "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, p1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Landroid/util/Slog;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 452
    :cond_0
    invoke-virtual {p0, p1}, Lcom/miui/server/stepcounter/StepCounterManagerService$BinderService;->getLatestDataInternal(Z)Z

    move-result v2
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 454
    invoke-static {v0, v1}, Landroid/os/Binder;->restoreCallingIdentity(J)V

    .line 452
    return v2

    .line 454
    :catchall_0
    move-exception v2

    invoke-static {v0, v1}, Landroid/os/Binder;->restoreCallingIdentity(J)V

    .line 455
    throw v2
.end method

.method public getLatestDataInternal(Z)Z
    .locals 3
    .param p1, "isForce"    # Z

    .line 459
    iget-object v0, p0, Lcom/miui/server/stepcounter/StepCounterManagerService$BinderService;->this$0:Lcom/miui/server/stepcounter/StepCounterManagerService;

    invoke-static {v0}, Lcom/miui/server/stepcounter/StepCounterManagerService;->-$$Nest$fgetmLocked(Lcom/miui/server/stepcounter/StepCounterManagerService;)Ljava/lang/Object;

    move-result-object v0

    monitor-enter v0

    .line 460
    :try_start_0
    iget-object v1, p0, Lcom/miui/server/stepcounter/StepCounterManagerService$BinderService;->this$0:Lcom/miui/server/stepcounter/StepCounterManagerService;

    invoke-static {v1}, Lcom/miui/server/stepcounter/StepCounterManagerService;->-$$Nest$fgetmHaveStepSensor(Lcom/miui/server/stepcounter/StepCounterManagerService;)Z

    move-result v1

    if-eqz v1, :cond_1

    .line 461
    if-eqz p1, :cond_0

    .line 462
    iget-object v1, p0, Lcom/miui/server/stepcounter/StepCounterManagerService$BinderService;->this$0:Lcom/miui/server/stepcounter/StepCounterManagerService;

    invoke-static {v1}, Lcom/miui/server/stepcounter/StepCounterManagerService;->-$$Nest$fgetmHandler(Lcom/miui/server/stepcounter/StepCounterManagerService;)Lcom/miui/server/stepcounter/StepCounterManagerService$StepCountHandler;

    move-result-object v1

    const/16 v2, 0x8

    invoke-virtual {v1, v2}, Lcom/miui/server/stepcounter/StepCounterManagerService$StepCountHandler;->sendEmptyMessage(I)Z

    goto :goto_0

    .line 464
    :cond_0
    iget-object v1, p0, Lcom/miui/server/stepcounter/StepCounterManagerService$BinderService;->this$0:Lcom/miui/server/stepcounter/StepCounterManagerService;

    invoke-static {v1}, Lcom/miui/server/stepcounter/StepCounterManagerService;->-$$Nest$fgetmHandler(Lcom/miui/server/stepcounter/StepCounterManagerService;)Lcom/miui/server/stepcounter/StepCounterManagerService$StepCountHandler;

    move-result-object v1

    const/4 v2, 0x4

    invoke-virtual {v1, v2}, Lcom/miui/server/stepcounter/StepCounterManagerService$StepCountHandler;->sendEmptyMessage(I)Z

    .line 467
    :cond_1
    :goto_0
    monitor-exit v0

    const/4 v0, 0x1

    return v0

    .line 468
    :catchall_0
    move-exception v1

    monitor-exit v0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v1
.end method

.method public haveStepSensor(Landroid/os/Parcel;)Z
    .locals 3
    .param p1, "reply"    # Landroid/os/Parcel;

    .line 431
    invoke-static {}, Landroid/os/Binder;->clearCallingIdentity()J

    move-result-wide v0

    .line 433
    .local v0, "ident":J
    :try_start_0
    invoke-virtual {p0, p1}, Lcom/miui/server/stepcounter/StepCounterManagerService$BinderService;->haveStepSensorInternal(Landroid/os/Parcel;)Z

    move-result v2
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 435
    invoke-static {v0, v1}, Landroid/os/Binder;->restoreCallingIdentity(J)V

    .line 433
    return v2

    .line 435
    :catchall_0
    move-exception v2

    invoke-static {v0, v1}, Landroid/os/Binder;->restoreCallingIdentity(J)V

    .line 436
    throw v2
.end method

.method public haveStepSensorInternal(Landroid/os/Parcel;)Z
    .locals 3
    .param p1, "reply"    # Landroid/os/Parcel;

    .line 440
    iget-object v0, p0, Lcom/miui/server/stepcounter/StepCounterManagerService$BinderService;->this$0:Lcom/miui/server/stepcounter/StepCounterManagerService;

    invoke-static {v0}, Lcom/miui/server/stepcounter/StepCounterManagerService;->-$$Nest$fgetmLocked(Lcom/miui/server/stepcounter/StepCounterManagerService;)Ljava/lang/Object;

    move-result-object v0

    monitor-enter v0

    .line 441
    :try_start_0
    iget-object v1, p0, Lcom/miui/server/stepcounter/StepCounterManagerService$BinderService;->this$0:Lcom/miui/server/stepcounter/StepCounterManagerService;

    invoke-static {v1}, Lcom/miui/server/stepcounter/StepCounterManagerService;->-$$Nest$fgetmHaveStepSensor(Lcom/miui/server/stepcounter/StepCounterManagerService;)Z

    move-result v1

    const/4 v2, 0x1

    if-eqz v1, :cond_0

    move v1, v2

    goto :goto_0

    :cond_0
    const/4 v1, 0x0

    :goto_0
    invoke-virtual {p1, v1}, Landroid/os/Parcel;->writeInt(I)V

    .line 442
    monitor-exit v0

    return v2

    .line 443
    :catchall_0
    move-exception v1

    monitor-exit v0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v1
.end method

.method public onShellCommand(Ljava/io/FileDescriptor;Ljava/io/FileDescriptor;Ljava/io/FileDescriptor;[Ljava/lang/String;Landroid/os/ShellCallback;Landroid/os/ResultReceiver;)V
    .locals 8
    .param p1, "in"    # Ljava/io/FileDescriptor;
    .param p2, "out"    # Ljava/io/FileDescriptor;
    .param p3, "err"    # Ljava/io/FileDescriptor;
    .param p4, "args"    # [Ljava/lang/String;
    .param p5, "callback"    # Landroid/os/ShellCallback;
    .param p6, "resultReceiver"    # Landroid/os/ResultReceiver;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    .line 363
    new-instance v0, Lcom/miui/server/stepcounter/StepCounterManagerService$Shell;

    iget-object v1, p0, Lcom/miui/server/stepcounter/StepCounterManagerService$BinderService;->this$0:Lcom/miui/server/stepcounter/StepCounterManagerService;

    const/4 v2, 0x0

    invoke-direct {v0, v1, v2}, Lcom/miui/server/stepcounter/StepCounterManagerService$Shell;-><init>(Lcom/miui/server/stepcounter/StepCounterManagerService;Lcom/miui/server/stepcounter/StepCounterManagerService$Shell-IA;)V

    move-object v1, p0

    move-object v2, p1

    move-object v3, p2

    move-object v4, p3

    move-object v5, p4

    move-object v6, p5

    move-object v7, p6

    invoke-virtual/range {v0 .. v7}, Lcom/miui/server/stepcounter/StepCounterManagerService$Shell;->exec(Landroid/os/Binder;Ljava/io/FileDescriptor;Ljava/io/FileDescriptor;Ljava/io/FileDescriptor;[Ljava/lang/String;Landroid/os/ShellCallback;Landroid/os/ResultReceiver;)I

    .line 364
    return-void
.end method

.method protected onTransact(ILandroid/os/Parcel;Landroid/os/Parcel;I)Z
    .locals 3
    .param p1, "code"    # I
    .param p2, "data"    # Landroid/os/Parcel;
    .param p3, "reply"    # Landroid/os/Parcel;
    .param p4, "flags"    # I
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    .line 380
    const-string v0, "miui_step_counter_service"

    packed-switch p1, :pswitch_data_0

    .line 394
    invoke-super {p0, p1, p2, p3, p4}, Landroid/os/Binder;->onTransact(ILandroid/os/Parcel;Landroid/os/Parcel;I)Z

    move-result v0

    return v0

    .line 390
    :pswitch_0
    invoke-virtual {p2, v0}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    .line 391
    invoke-virtual {p2}, Landroid/os/Parcel;->readBoolean()Z

    move-result v0

    .line 392
    .local v0, "isForce":Z
    invoke-virtual {p0, v0}, Lcom/miui/server/stepcounter/StepCounterManagerService$BinderService;->getLatestData(Z)Z

    move-result v1

    return v1

    .line 387
    .end local v0    # "isForce":Z
    :pswitch_1
    invoke-virtual {p2, v0}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    .line 388
    invoke-virtual {p0, p3}, Lcom/miui/server/stepcounter/StepCounterManagerService$BinderService;->haveStepSensor(Landroid/os/Parcel;)Z

    move-result v0

    return v0

    .line 382
    :pswitch_2
    invoke-virtual {p2, v0}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    .line 383
    invoke-virtual {p2}, Landroid/os/Parcel;->readBoolean()Z

    move-result v0

    .line 384
    .local v0, "enabled":Z
    invoke-virtual {p2}, Landroid/os/Parcel;->readStrongBinder()Landroid/os/IBinder;

    move-result-object v1

    .line 385
    .local v1, "mBinder":Landroid/os/IBinder;
    invoke-virtual {p0, v0, v1}, Lcom/miui/server/stepcounter/StepCounterManagerService$BinderService;->registerTreadmillSensor(ZLandroid/os/IBinder;)Z

    move-result v2

    return v2

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_2
        :pswitch_1
        :pswitch_0
    .end packed-switch
.end method

.method protected registerDeathCallbackLocked(Landroid/os/IBinder;)V
    .locals 3
    .param p1, "token"    # Landroid/os/IBinder;

    .line 472
    iget-object v0, p0, Lcom/miui/server/stepcounter/StepCounterManagerService$BinderService;->this$0:Lcom/miui/server/stepcounter/StepCounterManagerService;

    invoke-static {v0}, Lcom/miui/server/stepcounter/StepCounterManagerService;->-$$Nest$fgetmClientDeathCallbacks(Lcom/miui/server/stepcounter/StepCounterManagerService;)Ljava/util/HashMap;

    move-result-object v0

    invoke-virtual {v0, p1}, Ljava/util/HashMap;->containsKey(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 473
    return-void

    .line 475
    :cond_0
    iget-object v0, p0, Lcom/miui/server/stepcounter/StepCounterManagerService$BinderService;->this$0:Lcom/miui/server/stepcounter/StepCounterManagerService;

    invoke-static {v0}, Lcom/miui/server/stepcounter/StepCounterManagerService;->-$$Nest$fgetmClientDeathCallbacks(Lcom/miui/server/stepcounter/StepCounterManagerService;)Ljava/util/HashMap;

    move-result-object v0

    new-instance v1, Lcom/miui/server/stepcounter/StepCounterManagerService$ClientDeathCallback;

    iget-object v2, p0, Lcom/miui/server/stepcounter/StepCounterManagerService$BinderService;->this$0:Lcom/miui/server/stepcounter/StepCounterManagerService;

    invoke-direct {v1, v2, p1}, Lcom/miui/server/stepcounter/StepCounterManagerService$ClientDeathCallback;-><init>(Lcom/miui/server/stepcounter/StepCounterManagerService;Landroid/os/IBinder;)V

    invoke-virtual {v0, p1, v1}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 476
    return-void
.end method

.method public registerTreadmillSensor(ZLandroid/os/IBinder;)Z
    .locals 3
    .param p1, "enabled"    # Z
    .param p2, "token"    # Landroid/os/IBinder;

    .line 399
    invoke-static {}, Landroid/os/Binder;->clearCallingIdentity()J

    move-result-wide v0

    .line 401
    .local v0, "ident":J
    :try_start_0
    invoke-virtual {p0, p1, p2}, Lcom/miui/server/stepcounter/StepCounterManagerService$BinderService;->registerTreadmillSensorInternal(ZLandroid/os/IBinder;)Z

    move-result v2
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 403
    invoke-static {v0, v1}, Landroid/os/Binder;->restoreCallingIdentity(J)V

    .line 401
    return v2

    .line 403
    :catchall_0
    move-exception v2

    invoke-static {v0, v1}, Landroid/os/Binder;->restoreCallingIdentity(J)V

    .line 404
    throw v2
.end method

.method public registerTreadmillSensorInternal(ZLandroid/os/IBinder;)Z
    .locals 9
    .param p1, "enabled"    # Z
    .param p2, "token"    # Landroid/os/IBinder;

    .line 408
    iget-object v0, p0, Lcom/miui/server/stepcounter/StepCounterManagerService$BinderService;->this$0:Lcom/miui/server/stepcounter/StepCounterManagerService;

    invoke-static {v0}, Lcom/miui/server/stepcounter/StepCounterManagerService;->-$$Nest$fgetmLocked(Lcom/miui/server/stepcounter/StepCounterManagerService;)Ljava/lang/Object;

    move-result-object v0

    monitor-enter v0

    .line 409
    const/4 v1, 0x1

    if-eqz p1, :cond_0

    .line 410
    :try_start_0
    invoke-virtual {p0, p2}, Lcom/miui/server/stepcounter/StepCounterManagerService$BinderService;->registerDeathCallbackLocked(Landroid/os/IBinder;)V

    .line 411
    iget-object v2, p0, Lcom/miui/server/stepcounter/StepCounterManagerService$BinderService;->this$0:Lcom/miui/server/stepcounter/StepCounterManagerService;

    invoke-static {v2}, Lcom/miui/server/stepcounter/StepCounterManagerService;->-$$Nest$fgetmTreadmillEnabled(Lcom/miui/server/stepcounter/StepCounterManagerService;)Z

    move-result v2

    if-nez v2, :cond_1

    iget-object v2, p0, Lcom/miui/server/stepcounter/StepCounterManagerService$BinderService;->this$0:Lcom/miui/server/stepcounter/StepCounterManagerService;

    invoke-static {v2}, Lcom/miui/server/stepcounter/StepCounterManagerService;->-$$Nest$fgetmClientDeathCallbacks(Lcom/miui/server/stepcounter/StepCounterManagerService;)Ljava/util/HashMap;

    move-result-object v2

    invoke-virtual {v2}, Ljava/util/HashMap;->size()I

    move-result v2

    if-ne v2, v1, :cond_1

    iget-object v2, p0, Lcom/miui/server/stepcounter/StepCounterManagerService$BinderService;->this$0:Lcom/miui/server/stepcounter/StepCounterManagerService;

    invoke-static {v2}, Lcom/miui/server/stepcounter/StepCounterManagerService;->-$$Nest$fgetmTreadmillSensor(Lcom/miui/server/stepcounter/StepCounterManagerService;)Landroid/hardware/Sensor;

    move-result-object v2

    if-eqz v2, :cond_1

    iget-object v2, p0, Lcom/miui/server/stepcounter/StepCounterManagerService$BinderService;->this$0:Lcom/miui/server/stepcounter/StepCounterManagerService;

    invoke-static {v2}, Lcom/miui/server/stepcounter/StepCounterManagerService;->-$$Nest$fgetmTreadmillSensor(Lcom/miui/server/stepcounter/StepCounterManagerService;)Landroid/hardware/Sensor;

    move-result-object v2

    .line 412
    invoke-virtual {v2}, Landroid/hardware/Sensor;->getName()Ljava/lang/String;

    move-result-object v2

    const-string v3, "oem_treadmill  Wakeup"

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_1

    .line 413
    iget-object v2, p0, Lcom/miui/server/stepcounter/StepCounterManagerService$BinderService;->this$0:Lcom/miui/server/stepcounter/StepCounterManagerService;

    invoke-static {v2, v1}, Lcom/miui/server/stepcounter/StepCounterManagerService;->-$$Nest$fputmTreadmillEnabled(Lcom/miui/server/stepcounter/StepCounterManagerService;Z)V

    .line 414
    iget-object v2, p0, Lcom/miui/server/stepcounter/StepCounterManagerService$BinderService;->this$0:Lcom/miui/server/stepcounter/StepCounterManagerService;

    invoke-static {v2}, Lcom/miui/server/stepcounter/StepCounterManagerService;->-$$Nest$fgetmSensorManager(Lcom/miui/server/stepcounter/StepCounterManagerService;)Landroid/hardware/SensorManager;

    move-result-object v3

    iget-object v2, p0, Lcom/miui/server/stepcounter/StepCounterManagerService$BinderService;->this$0:Lcom/miui/server/stepcounter/StepCounterManagerService;

    invoke-static {v2}, Lcom/miui/server/stepcounter/StepCounterManagerService;->-$$Nest$fgetmTreadmillListener(Lcom/miui/server/stepcounter/StepCounterManagerService;)Landroid/hardware/SensorEventListener;

    move-result-object v4

    iget-object v2, p0, Lcom/miui/server/stepcounter/StepCounterManagerService$BinderService;->this$0:Lcom/miui/server/stepcounter/StepCounterManagerService;

    invoke-static {v2}, Lcom/miui/server/stepcounter/StepCounterManagerService;->-$$Nest$fgetmTreadmillSensor(Lcom/miui/server/stepcounter/StepCounterManagerService;)Landroid/hardware/Sensor;

    move-result-object v5

    const/4 v6, 0x3

    const v7, 0x3938700

    iget-object v2, p0, Lcom/miui/server/stepcounter/StepCounterManagerService$BinderService;->this$0:Lcom/miui/server/stepcounter/StepCounterManagerService;

    invoke-static {v2}, Lcom/miui/server/stepcounter/StepCounterManagerService;->-$$Nest$fgetmHandler(Lcom/miui/server/stepcounter/StepCounterManagerService;)Lcom/miui/server/stepcounter/StepCounterManagerService$StepCountHandler;

    move-result-object v8

    invoke-virtual/range {v3 .. v8}, Landroid/hardware/SensorManager;->registerListener(Landroid/hardware/SensorEventListener;Landroid/hardware/Sensor;IILandroid/os/Handler;)Z

    .line 416
    const-string v2, "StepCounterManagerService"

    const-string v3, "TreadmillSensor register success!"

    invoke-static {v2, v3}, Landroid/util/Slog;->d(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    .line 419
    :cond_0
    invoke-virtual {p0, p2}, Lcom/miui/server/stepcounter/StepCounterManagerService$BinderService;->unregisterDeathCallbackLocked(Landroid/os/IBinder;)V

    .line 420
    iget-object v2, p0, Lcom/miui/server/stepcounter/StepCounterManagerService$BinderService;->this$0:Lcom/miui/server/stepcounter/StepCounterManagerService;

    invoke-static {v2}, Lcom/miui/server/stepcounter/StepCounterManagerService;->-$$Nest$fgetmTreadmillEnabled(Lcom/miui/server/stepcounter/StepCounterManagerService;)Z

    move-result v2

    if-eqz v2, :cond_1

    iget-object v2, p0, Lcom/miui/server/stepcounter/StepCounterManagerService$BinderService;->this$0:Lcom/miui/server/stepcounter/StepCounterManagerService;

    invoke-static {v2}, Lcom/miui/server/stepcounter/StepCounterManagerService;->-$$Nest$fgetmClientDeathCallbacks(Lcom/miui/server/stepcounter/StepCounterManagerService;)Ljava/util/HashMap;

    move-result-object v2

    invoke-virtual {v2}, Ljava/util/HashMap;->size()I

    move-result v2

    if-nez v2, :cond_1

    .line 421
    iget-object v2, p0, Lcom/miui/server/stepcounter/StepCounterManagerService$BinderService;->this$0:Lcom/miui/server/stepcounter/StepCounterManagerService;

    const/4 v3, 0x0

    invoke-static {v2, v3}, Lcom/miui/server/stepcounter/StepCounterManagerService;->-$$Nest$fputmTreadmillEnabled(Lcom/miui/server/stepcounter/StepCounterManagerService;Z)V

    .line 422
    iget-object v2, p0, Lcom/miui/server/stepcounter/StepCounterManagerService$BinderService;->this$0:Lcom/miui/server/stepcounter/StepCounterManagerService;

    invoke-static {v2}, Lcom/miui/server/stepcounter/StepCounterManagerService;->-$$Nest$fgetmSensorManager(Lcom/miui/server/stepcounter/StepCounterManagerService;)Landroid/hardware/SensorManager;

    move-result-object v2

    iget-object v3, p0, Lcom/miui/server/stepcounter/StepCounterManagerService$BinderService;->this$0:Lcom/miui/server/stepcounter/StepCounterManagerService;

    invoke-static {v3}, Lcom/miui/server/stepcounter/StepCounterManagerService;->-$$Nest$fgetmTreadmillListener(Lcom/miui/server/stepcounter/StepCounterManagerService;)Landroid/hardware/SensorEventListener;

    move-result-object v3

    invoke-virtual {v2, v3}, Landroid/hardware/SensorManager;->unregisterListener(Landroid/hardware/SensorEventListener;)V

    .line 423
    const-string v2, "StepCounterManagerService"

    const-string v3, "TreadmillSensor unregister success!"

    invoke-static {v2, v3}, Landroid/util/Slog;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 426
    :cond_1
    :goto_0
    monitor-exit v0

    return v1

    .line 427
    :catchall_0
    move-exception v1

    monitor-exit v0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v1
.end method

.method protected unregisterDeathCallbackLocked(Landroid/os/IBinder;)V
    .locals 2
    .param p1, "token"    # Landroid/os/IBinder;

    .line 479
    if-eqz p1, :cond_0

    iget-object v0, p0, Lcom/miui/server/stepcounter/StepCounterManagerService$BinderService;->this$0:Lcom/miui/server/stepcounter/StepCounterManagerService;

    invoke-static {v0}, Lcom/miui/server/stepcounter/StepCounterManagerService;->-$$Nest$fgetmClientDeathCallbacks(Lcom/miui/server/stepcounter/StepCounterManagerService;)Ljava/util/HashMap;

    move-result-object v0

    invoke-virtual {v0, p1}, Ljava/util/HashMap;->containsKey(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 480
    iget-object v0, p0, Lcom/miui/server/stepcounter/StepCounterManagerService$BinderService;->this$0:Lcom/miui/server/stepcounter/StepCounterManagerService;

    invoke-static {v0}, Lcom/miui/server/stepcounter/StepCounterManagerService;->-$$Nest$fgetmClientDeathCallbacks(Lcom/miui/server/stepcounter/StepCounterManagerService;)Ljava/util/HashMap;

    move-result-object v0

    invoke-virtual {v0, p1}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/os/IBinder$DeathRecipient;

    const/4 v1, 0x0

    invoke-interface {p1, v0, v1}, Landroid/os/IBinder;->unlinkToDeath(Landroid/os/IBinder$DeathRecipient;I)Z

    .line 481
    iget-object v0, p0, Lcom/miui/server/stepcounter/StepCounterManagerService$BinderService;->this$0:Lcom/miui/server/stepcounter/StepCounterManagerService;

    invoke-static {v0}, Lcom/miui/server/stepcounter/StepCounterManagerService;->-$$Nest$fgetmClientDeathCallbacks(Lcom/miui/server/stepcounter/StepCounterManagerService;)Ljava/util/HashMap;

    move-result-object v0

    invoke-virtual {v0, p1}, Ljava/util/HashMap;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    .line 483
    :cond_0
    return-void
.end method
