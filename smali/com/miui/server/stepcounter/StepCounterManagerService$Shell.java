class com.miui.server.stepcounter.StepCounterManagerService$Shell extends android.os.ShellCommand {
	 /* .source "StepCounterManagerService.java" */
	 /* # annotations */
	 /* .annotation system Ldalvik/annotation/EnclosingClass; */
	 /* value = Lcom/miui/server/stepcounter/StepCounterManagerService; */
} // .end annotation
/* .annotation system Ldalvik/annotation/InnerClass; */
/* accessFlags = 0x2 */
/* name = "Shell" */
} // .end annotation
/* # instance fields */
final com.miui.server.stepcounter.StepCounterManagerService this$0; //synthetic
/* # direct methods */
private com.miui.server.stepcounter.StepCounterManagerService$Shell ( ) {
/* .locals 0 */
/* .line 566 */
this.this$0 = p1;
/* invoke-direct {p0}, Landroid/os/ShellCommand;-><init>()V */
return;
} // .end method
 com.miui.server.stepcounter.StepCounterManagerService$Shell ( ) { //synthethic
/* .locals 0 */
/* invoke-direct {p0, p1}, Lcom/miui/server/stepcounter/StepCounterManagerService$Shell;-><init>(Lcom/miui/server/stepcounter/StepCounterManagerService;)V */
return;
} // .end method
private void dumpHelp ( ) {
/* .locals 2 */
/* .line 657 */
(( com.miui.server.stepcounter.StepCounterManagerService$Shell ) p0 ).getOutPrintWriter ( ); // invoke-virtual {p0}, Lcom/miui/server/stepcounter/StepCounterManagerService$Shell;->getOutPrintWriter()Ljava/io/PrintWriter;
/* .line 658 */
/* .local v0, "pw":Ljava/io/PrintWriter; */
final String v1 = "Miui Step Counter manager commands:"; // const-string v1, "Miui Step Counter manager commands:"
(( java.io.PrintWriter ) v0 ).println ( v1 ); // invoke-virtual {v0, v1}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V
/* .line 659 */
final String v1 = " --help|-h"; // const-string v1, " --help|-h"
(( java.io.PrintWriter ) v0 ).println ( v1 ); // invoke-virtual {v0, v1}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V
/* .line 660 */
final String v1 = " Print this help text."; // const-string v1, " Print this help text."
(( java.io.PrintWriter ) v0 ).println ( v1 ); // invoke-virtual {v0, v1}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V
/* .line 661 */
(( java.io.PrintWriter ) v0 ).println ( ); // invoke-virtual {v0}, Ljava/io/PrintWriter;->println()V
/* .line 662 */
final String v1 = " logging-enable"; // const-string v1, " logging-enable"
(( java.io.PrintWriter ) v0 ).println ( v1 ); // invoke-virtual {v0, v1}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V
/* .line 663 */
final String v1 = " Enable logging."; // const-string v1, " Enable logging."
(( java.io.PrintWriter ) v0 ).println ( v1 ); // invoke-virtual {v0, v1}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V
/* .line 664 */
final String v1 = " logging-disable"; // const-string v1, " logging-disable"
(( java.io.PrintWriter ) v0 ).println ( v1 ); // invoke-virtual {v0, v1}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V
/* .line 665 */
final String v1 = " Disable logging."; // const-string v1, " Disable logging."
(( java.io.PrintWriter ) v0 ).println ( v1 ); // invoke-virtual {v0, v1}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V
/* .line 666 */
final String v1 = " trim-all"; // const-string v1, " trim-all"
(( java.io.PrintWriter ) v0 ).println ( v1 ); // invoke-virtual {v0, v1}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V
/* .line 667 */
final String v1 = " store cache steps to database"; // const-string v1, " store cache steps to database"
(( java.io.PrintWriter ) v0 ).println ( v1 ); // invoke-virtual {v0, v1}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V
/* .line 668 */
final String v1 = " set-delay DELAY"; // const-string v1, " set-delay DELAY"
(( java.io.PrintWriter ) v0 ).println ( v1 ); // invoke-virtual {v0, v1}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V
/* .line 669 */
final String v1 = " disable"; // const-string v1, " disable"
(( java.io.PrintWriter ) v0 ).println ( v1 ); // invoke-virtual {v0, v1}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V
/* .line 670 */
final String v1 = " set system auto delay value(s)."; // const-string v1, " set system auto delay value(s)."
(( java.io.PrintWriter ) v0 ).println ( v1 ); // invoke-virtual {v0, v1}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V
/* .line 671 */
final String v1 = " set-record-delay DELAY"; // const-string v1, " set-record-delay DELAY"
(( java.io.PrintWriter ) v0 ).println ( v1 ); // invoke-virtual {v0, v1}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V
/* .line 672 */
final String v1 = " set system record delay value(s)."; // const-string v1, " set system record delay value(s)."
(( java.io.PrintWriter ) v0 ).println ( v1 ); // invoke-virtual {v0, v1}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V
/* .line 673 */
final String v1 = " note: this will affect system the minimum interval for"; // const-string v1, " note: this will affect system the minimum interval for"
(( java.io.PrintWriter ) v0 ).println ( v1 ); // invoke-virtual {v0, v1}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V
/* .line 674 */
final String v1 = " recording the number of steps."; // const-string v1, " recording the number of steps."
(( java.io.PrintWriter ) v0 ).println ( v1 ); // invoke-virtual {v0, v1}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V
/* .line 675 */
return;
} // .end method
private Integer onShellCommand ( java.lang.String p0 ) {
/* .locals 4 */
/* .param p1, "cmd" # Ljava/lang/String; */
/* .line 578 */
/* if-nez p1, :cond_0 */
/* .line 579 */
v0 = (( com.miui.server.stepcounter.StepCounterManagerService$Shell ) p0 ).handleDefaultCommands ( p1 ); // invoke-virtual {p0, p1}, Lcom/miui/server/stepcounter/StepCounterManagerService$Shell;->handleDefaultCommands(Ljava/lang/String;)I
/* .line 581 */
} // :cond_0
(( com.miui.server.stepcounter.StepCounterManagerService$Shell ) p0 ).getOutPrintWriter ( ); // invoke-virtual {p0}, Lcom/miui/server/stepcounter/StepCounterManagerService$Shell;->getOutPrintWriter()Ljava/io/PrintWriter;
/* .line 582 */
/* .local v0, "pw":Ljava/io/PrintWriter; */
v1 = (( java.lang.String ) p1 ).hashCode ( ); // invoke-virtual {p1}, Ljava/lang/String;->hashCode()I
int v2 = 1; // const/4 v2, 0x1
int v3 = 0; // const/4 v3, 0x0
/* sparse-switch v1, :sswitch_data_0 */
} // :cond_1
/* :sswitch_0 */
final String v1 = "logging-enable"; // const-string v1, "logging-enable"
v1 = (( java.lang.String ) p1 ).equals ( v1 ); // invoke-virtual {p1, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z
if ( v1 != null) { // if-eqz v1, :cond_1
/* move v1, v3 */
/* :sswitch_1 */
final String v1 = "disable"; // const-string v1, "disable"
v1 = (( java.lang.String ) p1 ).equals ( v1 ); // invoke-virtual {p1, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z
if ( v1 != null) { // if-eqz v1, :cond_1
int v1 = 5; // const/4 v1, 0x5
/* :sswitch_2 */
/* const-string/jumbo v1, "trim-all" */
v1 = (( java.lang.String ) p1 ).equals ( v1 ); // invoke-virtual {p1, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z
if ( v1 != null) { // if-eqz v1, :cond_1
	 int v1 = 4; // const/4 v1, 0x4
	 /* :sswitch_3 */
	 final String v1 = "--help"; // const-string v1, "--help"
	 v1 = 	 (( java.lang.String ) p1 ).equals ( v1 ); // invoke-virtual {p1, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z
	 if ( v1 != null) { // if-eqz v1, :cond_1
		 int v1 = 7; // const/4 v1, 0x7
		 /* :sswitch_4 */
		 final String v1 = "logging-disable"; // const-string v1, "logging-disable"
		 v1 = 		 (( java.lang.String ) p1 ).equals ( v1 ); // invoke-virtual {p1, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z
		 if ( v1 != null) { // if-eqz v1, :cond_1
			 /* move v1, v2 */
			 /* :sswitch_5 */
			 /* const-string/jumbo v1, "set-record-delay" */
			 v1 = 			 (( java.lang.String ) p1 ).equals ( v1 ); // invoke-virtual {p1, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z
			 if ( v1 != null) { // if-eqz v1, :cond_1
				 int v1 = 3; // const/4 v1, 0x3
				 /* :sswitch_6 */
				 final String v1 = "-h"; // const-string v1, "-h"
				 v1 = 				 (( java.lang.String ) p1 ).equals ( v1 ); // invoke-virtual {p1, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z
				 if ( v1 != null) { // if-eqz v1, :cond_1
					 int v1 = 6; // const/4 v1, 0x6
					 /* :sswitch_7 */
					 /* const-string/jumbo v1, "set-delay" */
					 v1 = 					 (( java.lang.String ) p1 ).equals ( v1 ); // invoke-virtual {p1, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z
					 if ( v1 != null) { // if-eqz v1, :cond_1
						 int v1 = 2; // const/4 v1, 0x2
					 } // :goto_0
					 int v1 = -1; // const/4 v1, -0x1
				 } // :goto_1
				 /* packed-switch v1, :pswitch_data_0 */
				 /* .line 608 */
				 (( com.miui.server.stepcounter.StepCounterManagerService$Shell ) p0 ).onHelp ( ); // invoke-virtual {p0}, Lcom/miui/server/stepcounter/StepCounterManagerService$Shell;->onHelp()V
				 /* .line 601 */
				 /* :pswitch_0 */
				 v1 = this.this$0;
				 com.miui.server.stepcounter.StepCounterManagerService .-$$Nest$munregisterDetectorListener ( v1 );
				 /* .line 602 */
				 v1 = this.this$0;
				 com.miui.server.stepcounter.StepCounterManagerService .-$$Nest$fputisRegisterDetectorListener ( v1,v3 );
				 /* .line 603 */
				 final String v1 = "Disable Step Counter"; // const-string v1, "Disable Step Counter"
				 (( java.io.PrintWriter ) v0 ).println ( v1 ); // invoke-virtual {v0, v1}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V
				 /* .line 604 */
				 /* .line 598 */
				 /* :pswitch_1 */
				 /* invoke-direct {p0}, Lcom/miui/server/stepcounter/StepCounterManagerService$Shell;->setTrimAll()V */
				 /* .line 599 */
				 /* .line 595 */
				 /* :pswitch_2 */
				 /* invoke-direct {p0}, Lcom/miui/server/stepcounter/StepCounterManagerService$Shell;->setRecordDelay()V */
				 /* .line 596 */
				 /* .line 592 */
				 /* :pswitch_3 */
				 /* invoke-direct {p0}, Lcom/miui/server/stepcounter/StepCounterManagerService$Shell;->setDelay()V */
				 /* .line 593 */
				 /* .line 588 */
				 /* :pswitch_4 */
				 v1 = this.this$0;
				 com.miui.server.stepcounter.StepCounterManagerService .-$$Nest$msetDebugEnabled ( v1,v3 );
				 /* .line 589 */
				 final String v1 = "Set Step Counter Log Disable"; // const-string v1, "Set Step Counter Log Disable"
				 (( java.io.PrintWriter ) v0 ).println ( v1 ); // invoke-virtual {v0, v1}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V
				 /* .line 590 */
				 /* .line 584 */
				 /* :pswitch_5 */
				 v1 = this.this$0;
				 com.miui.server.stepcounter.StepCounterManagerService .-$$Nest$msetDebugEnabled ( v1,v2 );
				 /* .line 585 */
				 final String v1 = "Set Step Counter Log Enable"; // const-string v1, "Set Step Counter Log Enable"
				 (( java.io.PrintWriter ) v0 ).println ( v1 ); // invoke-virtual {v0, v1}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V
				 /* .line 586 */
				 /* nop */
				 /* .line 611 */
			 } // :goto_2
			 /* nop */
			 /* :sswitch_data_0 */
			 /* .sparse-switch */
			 /* -0x1e9dc7a8 -> :sswitch_7 */
			 /* 0x5db -> :sswitch_6 */
			 /* 0x39b81092 -> :sswitch_5 */
			 /* 0x3e04027a -> :sswitch_4 */
			 /* 0x4f7504e1 -> :sswitch_3 */
			 /* 0x59d18176 -> :sswitch_2 */
			 /* 0x639e22e8 -> :sswitch_1 */
			 /* 0x670c0bb1 -> :sswitch_0 */
		 } // .end sparse-switch
		 /* :pswitch_data_0 */
		 /* .packed-switch 0x0 */
		 /* :pswitch_5 */
		 /* :pswitch_4 */
		 /* :pswitch_3 */
		 /* :pswitch_2 */
		 /* :pswitch_1 */
		 /* :pswitch_0 */
	 } // .end packed-switch
} // .end method
private void setDelay ( ) {
	 /* .locals 5 */
	 /* .line 615 */
	 (( com.miui.server.stepcounter.StepCounterManagerService$Shell ) p0 ).getOutPrintWriter ( ); // invoke-virtual {p0}, Lcom/miui/server/stepcounter/StepCounterManagerService$Shell;->getOutPrintWriter()Ljava/io/PrintWriter;
	 /* .line 616 */
	 /* .local v0, "pw":Ljava/io/PrintWriter; */
	 int v1 = -1; // const/4 v1, -0x1
	 /* .line 618 */
	 /* .local v1, "delay":I */
	 try { // :try_start_0
		 (( com.miui.server.stepcounter.StepCounterManagerService$Shell ) p0 ).getNextArgRequired ( ); // invoke-virtual {p0}, Lcom/miui/server/stepcounter/StepCounterManagerService$Shell;->getNextArgRequired()Ljava/lang/String;
		 v2 = 		 java.lang.Integer .parseInt ( v2 );
		 /* :try_end_0 */
		 /* .catch Ljava/lang/RuntimeException; {:try_start_0 ..:try_end_0} :catch_0 */
		 /* move v1, v2 */
		 /* .line 621 */
		 /* .line 619 */
		 /* :catch_0 */
		 /* move-exception v2 */
		 /* .line 620 */
		 /* .local v2, "ex":Ljava/lang/RuntimeException; */
		 /* new-instance v3, Ljava/lang/StringBuilder; */
		 /* invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V */
		 final String v4 = "Error: "; // const-string v4, "Error: "
		 (( java.lang.StringBuilder ) v3 ).append ( v4 ); // invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
		 (( java.lang.RuntimeException ) v2 ).toString ( ); // invoke-virtual {v2}, Ljava/lang/RuntimeException;->toString()Ljava/lang/String;
		 (( java.lang.StringBuilder ) v3 ).append ( v4 ); // invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
		 (( java.lang.StringBuilder ) v3 ).toString ( ); // invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
		 (( java.io.PrintWriter ) v0 ).println ( v3 ); // invoke-virtual {v0, v3}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V
		 /* .line 622 */
	 } // .end local v2 # "ex":Ljava/lang/RuntimeException;
} // :goto_0
final String v2 = "s"; // const-string v2, "s"
/* if-lez v1, :cond_0 */
/* .line 623 */
v3 = this.this$0;
com.miui.server.stepcounter.StepCounterManagerService .-$$Nest$msetDelayValue ( v3,v1 );
/* .line 624 */
/* new-instance v3, Ljava/lang/StringBuilder; */
/* invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V */
/* const-string/jumbo v4, "set delay success! current delay is " */
(( java.lang.StringBuilder ) v3 ).append ( v4 ); // invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v3 ).append ( v1 ); // invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v3 ).append ( v2 ); // invoke-virtual {v3, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v2 ).toString ( ); // invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
(( java.io.PrintWriter ) v0 ).println ( v2 ); // invoke-virtual {v0, v2}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V
/* .line 626 */
} // :cond_0
/* new-instance v3, Ljava/lang/StringBuilder; */
/* invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V */
/* const-string/jumbo v4, "set delay fail! current delay is " */
(( java.lang.StringBuilder ) v3 ).append ( v4 ); // invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v3 ).append ( v1 ); // invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v3 ).append ( v2 ); // invoke-virtual {v3, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v2 ).toString ( ); // invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
(( java.io.PrintWriter ) v0 ).println ( v2 ); // invoke-virtual {v0, v2}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V
/* .line 628 */
} // :goto_1
return;
} // .end method
private void setRecordDelay ( ) {
/* .locals 5 */
/* .line 631 */
(( com.miui.server.stepcounter.StepCounterManagerService$Shell ) p0 ).getOutPrintWriter ( ); // invoke-virtual {p0}, Lcom/miui/server/stepcounter/StepCounterManagerService$Shell;->getOutPrintWriter()Ljava/io/PrintWriter;
/* .line 632 */
/* .local v0, "pw":Ljava/io/PrintWriter; */
int v1 = -1; // const/4 v1, -0x1
/* .line 634 */
/* .local v1, "recordDelay":I */
try { // :try_start_0
(( com.miui.server.stepcounter.StepCounterManagerService$Shell ) p0 ).getNextArgRequired ( ); // invoke-virtual {p0}, Lcom/miui/server/stepcounter/StepCounterManagerService$Shell;->getNextArgRequired()Ljava/lang/String;
v2 = java.lang.Integer .parseInt ( v2 );
/* :try_end_0 */
/* .catch Ljava/lang/RuntimeException; {:try_start_0 ..:try_end_0} :catch_0 */
/* move v1, v2 */
/* .line 637 */
/* .line 635 */
/* :catch_0 */
/* move-exception v2 */
/* .line 636 */
/* .local v2, "ex":Ljava/lang/RuntimeException; */
/* new-instance v3, Ljava/lang/StringBuilder; */
/* invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V */
final String v4 = "Error: "; // const-string v4, "Error: "
(( java.lang.StringBuilder ) v3 ).append ( v4 ); // invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.RuntimeException ) v2 ).toString ( ); // invoke-virtual {v2}, Ljava/lang/RuntimeException;->toString()Ljava/lang/String;
(( java.lang.StringBuilder ) v3 ).append ( v4 ); // invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v3 ).toString ( ); // invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
(( java.io.PrintWriter ) v0 ).println ( v3 ); // invoke-virtual {v0, v3}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V
/* .line 638 */
} // .end local v2 # "ex":Ljava/lang/RuntimeException;
} // :goto_0
final String v2 = "s"; // const-string v2, "s"
/* if-lez v1, :cond_0 */
/* .line 639 */
v3 = this.this$0;
com.miui.server.stepcounter.StepCounterManagerService .-$$Nest$msetRecordDelayValue ( v3,v1 );
/* .line 640 */
/* new-instance v3, Ljava/lang/StringBuilder; */
/* invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V */
/* const-string/jumbo v4, "set record-delay success! current record-delay is " */
(( java.lang.StringBuilder ) v3 ).append ( v4 ); // invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v3 ).append ( v1 ); // invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v3 ).append ( v2 ); // invoke-virtual {v3, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v2 ).toString ( ); // invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
(( java.io.PrintWriter ) v0 ).println ( v2 ); // invoke-virtual {v0, v2}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V
/* .line 642 */
} // :cond_0
/* new-instance v3, Ljava/lang/StringBuilder; */
/* invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V */
/* const-string/jumbo v4, "set record-delay fail! current record-delay is " */
(( java.lang.StringBuilder ) v3 ).append ( v4 ); // invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v3 ).append ( v1 ); // invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v3 ).append ( v2 ); // invoke-virtual {v3, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v2 ).toString ( ); // invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
(( java.io.PrintWriter ) v0 ).println ( v2 ); // invoke-virtual {v0, v2}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V
/* .line 644 */
} // :goto_1
return;
} // .end method
private void setTrimAll ( ) {
/* .locals 3 */
/* .line 647 */
(( com.miui.server.stepcounter.StepCounterManagerService$Shell ) p0 ).getOutPrintWriter ( ); // invoke-virtual {p0}, Lcom/miui/server/stepcounter/StepCounterManagerService$Shell;->getOutPrintWriter()Ljava/io/PrintWriter;
/* .line 648 */
/* .local v0, "pw":Ljava/io/PrintWriter; */
v1 = this.this$0;
v1 = com.miui.server.stepcounter.StepCounterManagerService .-$$Nest$fgetmHaveStepSensor ( v1 );
if ( v1 != null) { // if-eqz v1, :cond_0
/* .line 649 */
v1 = this.this$0;
com.miui.server.stepcounter.StepCounterManagerService .-$$Nest$fgetmHandler ( v1 );
/* const/16 v2, 0x8 */
(( com.miui.server.stepcounter.StepCounterManagerService$StepCountHandler ) v1 ).sendEmptyMessage ( v2 ); // invoke-virtual {v1, v2}, Lcom/miui/server/stepcounter/StepCounterManagerService$StepCountHandler;->sendEmptyMessage(I)Z
/* .line 650 */
/* const-string/jumbo v1, "trimAll success!" */
(( java.io.PrintWriter ) v0 ).println ( v1 ); // invoke-virtual {v0, v1}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V
/* .line 652 */
} // :cond_0
/* const-string/jumbo v1, "trimAll fail!" */
(( java.io.PrintWriter ) v0 ).println ( v1 ); // invoke-virtual {v0, v1}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V
/* .line 654 */
} // :goto_0
return;
} // .end method
/* # virtual methods */
public Integer onCommand ( java.lang.String p0 ) {
/* .locals 1 */
/* .param p1, "cmd" # Ljava/lang/String; */
/* .line 569 */
v0 = /* invoke-direct {p0, p1}, Lcom/miui/server/stepcounter/StepCounterManagerService$Shell;->onShellCommand(Ljava/lang/String;)I */
} // .end method
public void onHelp ( ) {
/* .locals 0 */
/* .line 574 */
/* invoke-direct {p0}, Lcom/miui/server/stepcounter/StepCounterManagerService$Shell;->dumpHelp()V */
/* .line 575 */
return;
} // .end method
