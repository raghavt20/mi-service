class com.miui.server.stepcounter.StepCounterManagerService$ClientDeathCallback implements android.os.IBinder$DeathRecipient {
	 /* .source "StepCounterManagerService.java" */
	 /* # interfaces */
	 /* # annotations */
	 /* .annotation system Ldalvik/annotation/EnclosingClass; */
	 /* value = Lcom/miui/server/stepcounter/StepCounterManagerService; */
} // .end annotation
/* .annotation system Ldalvik/annotation/InnerClass; */
/* accessFlags = 0x0 */
/* name = "ClientDeathCallback" */
} // .end annotation
/* # instance fields */
private android.os.IBinder mToken;
final com.miui.server.stepcounter.StepCounterManagerService this$0; //synthetic
/* # direct methods */
public com.miui.server.stepcounter.StepCounterManagerService$ClientDeathCallback ( ) {
/* .locals 1 */
/* .param p1, "this$0" # Lcom/miui/server/stepcounter/StepCounterManagerService; */
/* .param p2, "token" # Landroid/os/IBinder; */
/* .line 490 */
this.this$0 = p1;
/* invoke-direct {p0}, Ljava/lang/Object;-><init>()V */
/* .line 491 */
this.mToken = p2;
/* .line 493 */
int v0 = 0; // const/4 v0, 0x0
try { // :try_start_0
	 /* :try_end_0 */
	 /* .catch Landroid/os/RemoteException; {:try_start_0 ..:try_end_0} :catch_0 */
	 /* .line 496 */
	 /* .line 494 */
	 /* :catch_0 */
	 /* move-exception v0 */
	 /* .line 495 */
	 /* .local v0, "e":Landroid/os/RemoteException; */
	 (( android.os.RemoteException ) v0 ).printStackTrace ( ); // invoke-virtual {v0}, Landroid/os/RemoteException;->printStackTrace()V
	 /* .line 497 */
} // .end local v0 # "e":Landroid/os/RemoteException;
} // :goto_0
return;
} // .end method
/* # virtual methods */
public void binderDied ( ) {
/* .locals 3 */
/* .line 501 */
v0 = this.this$0;
com.miui.server.stepcounter.StepCounterManagerService .-$$Nest$fgetmLocked ( v0 );
/* monitor-enter v0 */
/* .line 502 */
try { // :try_start_0
v1 = this.this$0;
com.miui.server.stepcounter.StepCounterManagerService .-$$Nest$fgetmClientDeathCallbacks ( v1 );
v2 = this.mToken;
(( java.util.HashMap ) v1 ).remove ( v2 ); // invoke-virtual {v1, v2}, Ljava/util/HashMap;->remove(Ljava/lang/Object;)Ljava/lang/Object;
/* .line 503 */
v1 = this.this$0;
com.miui.server.stepcounter.StepCounterManagerService .-$$Nest$fgetmClientDeathCallbacks ( v1 );
v1 = (( java.util.HashMap ) v1 ).size ( ); // invoke-virtual {v1}, Ljava/util/HashMap;->size()I
/* if-nez v1, :cond_0 */
v1 = this.this$0;
com.miui.server.stepcounter.StepCounterManagerService .-$$Nest$fgetmTreadmillSensor ( v1 );
if ( v1 != null) { // if-eqz v1, :cond_0
	 v1 = this.this$0;
	 com.miui.server.stepcounter.StepCounterManagerService .-$$Nest$fgetmTreadmillSensor ( v1 );
	 /* .line 504 */
	 (( android.hardware.Sensor ) v1 ).getName ( ); // invoke-virtual {v1}, Landroid/hardware/Sensor;->getName()Ljava/lang/String;
	 final String v2 = "oem_treadmill Wakeup"; // const-string v2, "oem_treadmill Wakeup"
	 v1 = 	 (( java.lang.String ) v1 ).equals ( v2 ); // invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z
	 if ( v1 != null) { // if-eqz v1, :cond_0
		 /* .line 505 */
		 v1 = this.this$0;
		 int v2 = 0; // const/4 v2, 0x0
		 com.miui.server.stepcounter.StepCounterManagerService .-$$Nest$fputmTreadmillEnabled ( v1,v2 );
		 /* .line 506 */
		 v1 = this.this$0;
		 com.miui.server.stepcounter.StepCounterManagerService .-$$Nest$fgetmSensorManager ( v1 );
		 v2 = this.this$0;
		 com.miui.server.stepcounter.StepCounterManagerService .-$$Nest$fgetmTreadmillListener ( v2 );
		 (( android.hardware.SensorManager ) v1 ).unregisterListener ( v2 ); // invoke-virtual {v1, v2}, Landroid/hardware/SensorManager;->unregisterListener(Landroid/hardware/SensorEventListener;)V
		 /* .line 507 */
		 final String v1 = "StepCounterManagerService"; // const-string v1, "StepCounterManagerService"
		 final String v2 = "binderDied:unregisterListener Treadmill Listener"; // const-string v2, "binderDied:unregisterListener Treadmill Listener"
		 android.util.Slog .d ( v1,v2 );
		 /* .line 509 */
	 } // :cond_0
	 /* monitor-exit v0 */
	 /* .line 510 */
	 return;
	 /* .line 509 */
	 /* :catchall_0 */
	 /* move-exception v1 */
	 /* monitor-exit v0 */
	 /* :try_end_0 */
	 /* .catchall {:try_start_0 ..:try_end_0} :catchall_0 */
	 /* throw v1 */
} // .end method
