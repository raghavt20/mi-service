class com.miui.server.stepcounter.StepCounterManagerService$StepDetectorListener implements android.hardware.SensorEventListener {
	 /* .source "StepCounterManagerService.java" */
	 /* # interfaces */
	 /* # annotations */
	 /* .annotation system Ldalvik/annotation/EnclosingClass; */
	 /* value = Lcom/miui/server/stepcounter/StepCounterManagerService; */
} // .end annotation
/* .annotation system Ldalvik/annotation/InnerClass; */
/* accessFlags = 0x2 */
/* name = "StepDetectorListener" */
} // .end annotation
/* # instance fields */
final com.miui.server.stepcounter.StepCounterManagerService this$0; //synthetic
/* # direct methods */
private com.miui.server.stepcounter.StepCounterManagerService$StepDetectorListener ( ) {
/* .locals 0 */
/* .line 252 */
this.this$0 = p1;
/* invoke-direct {p0}, Ljava/lang/Object;-><init>()V */
return;
} // .end method
 com.miui.server.stepcounter.StepCounterManagerService$StepDetectorListener ( ) { //synthethic
/* .locals 0 */
/* invoke-direct {p0, p1}, Lcom/miui/server/stepcounter/StepCounterManagerService$StepDetectorListener;-><init>(Lcom/miui/server/stepcounter/StepCounterManagerService;)V */
return;
} // .end method
/* # virtual methods */
public void onAccuracyChanged ( android.hardware.Sensor p0, Integer p1 ) {
/* .locals 0 */
/* .param p1, "sensor" # Landroid/hardware/Sensor; */
/* .param p2, "accuracy" # I */
/* .line 273 */
return;
} // .end method
public void onSensorChanged ( android.hardware.SensorEvent p0 ) {
/* .locals 8 */
/* .param p1, "event" # Landroid/hardware/SensorEvent; */
/* .line 255 */
v0 = this.values;
int v1 = 0; // const/4 v1, 0x0
/* aget v0, v0, v1 */
/* float-to-int v0, v0 */
/* .line 257 */
/* .local v0, "counter":I */
v1 = this.values;
/* array-length v1, v1 */
int v2 = 1; // const/4 v2, 0x1
/* if-le v1, v2, :cond_0 */
/* .line 258 */
v1 = this.values;
/* aget v1, v1, v2 */
/* float-to-int v1, v1 */
/* .local v1, "sensorMode":I */
/* .line 260 */
} // .end local v1 # "sensorMode":I
} // :cond_0
int v1 = 1; // const/4 v1, 0x1
/* .line 262 */
/* .restart local v1 # "sensorMode":I */
} // :goto_0
com.miui.server.stepcounter.StepCounterManagerService .-$$Nest$sfgetSYSTEM_BOOT_TIME ( );
/* move-result-wide v2 */
/* iget-wide v4, p1, Landroid/hardware/SensorEvent;->timestamp:J */
/* const-wide/32 v6, 0xf4240 */
/* div-long/2addr v4, v6 */
/* add-long/2addr v2, v4 */
/* .line 263 */
/* .local v2, "timestamp":J */
v4 = this.this$0;
com.miui.server.stepcounter.StepCounterManagerService .-$$Nest$fgetmStepList ( v4 );
/* new-instance v5, Lmiui/stepcounter/StepDetector; */
/* invoke-direct {v5, v0, v2, v3, v1}, Lmiui/stepcounter/StepDetector;-><init>(IJI)V */
/* .line 264 */
v4 = com.miui.server.stepcounter.StepCounterManagerService .-$$Nest$sfgetsDEBUG ( );
if ( v4 != null) { // if-eqz v4, :cond_1
/* .line 265 */
/* new-instance v4, Ljava/lang/StringBuilder; */
/* invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V */
final String v5 = "onSensorChanged: count: "; // const-string v5, "onSensorChanged: count: "
(( java.lang.StringBuilder ) v4 ).append ( v5 ); // invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v4 ).append ( v0 ); // invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;
final String v5 = "\tsensorMode: "; // const-string v5, "\tsensorMode: "
(( java.lang.StringBuilder ) v4 ).append ( v5 ); // invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v4 ).append ( v1 ); // invoke-virtual {v4, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;
final String v5 = "\ttimestamp: "; // const-string v5, "\ttimestamp: "
(( java.lang.StringBuilder ) v4 ).append ( v5 ); // invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v4 ).append ( v2, v3 ); // invoke-virtual {v4, v2, v3}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v4 ).toString ( ); // invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
final String v5 = "StepCounterManagerService"; // const-string v5, "StepCounterManagerService"
android.util.Slog .i ( v5,v4 );
/* .line 268 */
} // :cond_1
return;
} // .end method
