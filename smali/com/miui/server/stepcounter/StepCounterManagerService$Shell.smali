.class Lcom/miui/server/stepcounter/StepCounterManagerService$Shell;
.super Landroid/os/ShellCommand;
.source "StepCounterManagerService.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/miui/server/stepcounter/StepCounterManagerService;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "Shell"
.end annotation


# instance fields
.field final synthetic this$0:Lcom/miui/server/stepcounter/StepCounterManagerService;


# direct methods
.method private constructor <init>(Lcom/miui/server/stepcounter/StepCounterManagerService;)V
    .locals 0

    .line 566
    iput-object p1, p0, Lcom/miui/server/stepcounter/StepCounterManagerService$Shell;->this$0:Lcom/miui/server/stepcounter/StepCounterManagerService;

    invoke-direct {p0}, Landroid/os/ShellCommand;-><init>()V

    return-void
.end method

.method synthetic constructor <init>(Lcom/miui/server/stepcounter/StepCounterManagerService;Lcom/miui/server/stepcounter/StepCounterManagerService$Shell-IA;)V
    .locals 0

    invoke-direct {p0, p1}, Lcom/miui/server/stepcounter/StepCounterManagerService$Shell;-><init>(Lcom/miui/server/stepcounter/StepCounterManagerService;)V

    return-void
.end method

.method private dumpHelp()V
    .locals 2

    .line 657
    invoke-virtual {p0}, Lcom/miui/server/stepcounter/StepCounterManagerService$Shell;->getOutPrintWriter()Ljava/io/PrintWriter;

    move-result-object v0

    .line 658
    .local v0, "pw":Ljava/io/PrintWriter;
    const-string v1, "Miui Step Counter manager commands:"

    invoke-virtual {v0, v1}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V

    .line 659
    const-string v1, "  --help|-h"

    invoke-virtual {v0, v1}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V

    .line 660
    const-string v1, "    Print this help text."

    invoke-virtual {v0, v1}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V

    .line 661
    invoke-virtual {v0}, Ljava/io/PrintWriter;->println()V

    .line 662
    const-string v1, "  logging-enable"

    invoke-virtual {v0, v1}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V

    .line 663
    const-string v1, "    Enable logging."

    invoke-virtual {v0, v1}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V

    .line 664
    const-string v1, "  logging-disable"

    invoke-virtual {v0, v1}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V

    .line 665
    const-string v1, "    Disable logging."

    invoke-virtual {v0, v1}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V

    .line 666
    const-string v1, "  trim-all"

    invoke-virtual {v0, v1}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V

    .line 667
    const-string v1, "    store cache steps to database"

    invoke-virtual {v0, v1}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V

    .line 668
    const-string v1, "  set-delay DELAY"

    invoke-virtual {v0, v1}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V

    .line 669
    const-string v1, "  disable"

    invoke-virtual {v0, v1}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V

    .line 670
    const-string v1, "    set system auto delay value(s)."

    invoke-virtual {v0, v1}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V

    .line 671
    const-string v1, "  set-record-delay DELAY"

    invoke-virtual {v0, v1}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V

    .line 672
    const-string v1, "    set system record delay value(s)."

    invoke-virtual {v0, v1}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V

    .line 673
    const-string v1, "    note: this will affect system the minimum interval for"

    invoke-virtual {v0, v1}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V

    .line 674
    const-string v1, "          recording the number of steps."

    invoke-virtual {v0, v1}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V

    .line 675
    return-void
.end method

.method private onShellCommand(Ljava/lang/String;)I
    .locals 4
    .param p1, "cmd"    # Ljava/lang/String;

    .line 578
    if-nez p1, :cond_0

    .line 579
    invoke-virtual {p0, p1}, Lcom/miui/server/stepcounter/StepCounterManagerService$Shell;->handleDefaultCommands(Ljava/lang/String;)I

    move-result v0

    return v0

    .line 581
    :cond_0
    invoke-virtual {p0}, Lcom/miui/server/stepcounter/StepCounterManagerService$Shell;->getOutPrintWriter()Ljava/io/PrintWriter;

    move-result-object v0

    .line 582
    .local v0, "pw":Ljava/io/PrintWriter;
    invoke-virtual {p1}, Ljava/lang/String;->hashCode()I

    move-result v1

    const/4 v2, 0x1

    const/4 v3, 0x0

    sparse-switch v1, :sswitch_data_0

    :cond_1
    goto :goto_0

    :sswitch_0
    const-string v1, "logging-enable"

    invoke-virtual {p1, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_1

    move v1, v3

    goto :goto_1

    :sswitch_1
    const-string v1, "disable"

    invoke-virtual {p1, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_1

    const/4 v1, 0x5

    goto :goto_1

    :sswitch_2
    const-string/jumbo v1, "trim-all"

    invoke-virtual {p1, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_1

    const/4 v1, 0x4

    goto :goto_1

    :sswitch_3
    const-string v1, "--help"

    invoke-virtual {p1, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_1

    const/4 v1, 0x7

    goto :goto_1

    :sswitch_4
    const-string v1, "logging-disable"

    invoke-virtual {p1, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_1

    move v1, v2

    goto :goto_1

    :sswitch_5
    const-string/jumbo v1, "set-record-delay"

    invoke-virtual {p1, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_1

    const/4 v1, 0x3

    goto :goto_1

    :sswitch_6
    const-string v1, "-h"

    invoke-virtual {p1, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_1

    const/4 v1, 0x6

    goto :goto_1

    :sswitch_7
    const-string/jumbo v1, "set-delay"

    invoke-virtual {p1, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_1

    const/4 v1, 0x2

    goto :goto_1

    :goto_0
    const/4 v1, -0x1

    :goto_1
    packed-switch v1, :pswitch_data_0

    .line 608
    invoke-virtual {p0}, Lcom/miui/server/stepcounter/StepCounterManagerService$Shell;->onHelp()V

    goto :goto_2

    .line 601
    :pswitch_0
    iget-object v1, p0, Lcom/miui/server/stepcounter/StepCounterManagerService$Shell;->this$0:Lcom/miui/server/stepcounter/StepCounterManagerService;

    invoke-static {v1}, Lcom/miui/server/stepcounter/StepCounterManagerService;->-$$Nest$munregisterDetectorListener(Lcom/miui/server/stepcounter/StepCounterManagerService;)V

    .line 602
    iget-object v1, p0, Lcom/miui/server/stepcounter/StepCounterManagerService$Shell;->this$0:Lcom/miui/server/stepcounter/StepCounterManagerService;

    invoke-static {v1, v3}, Lcom/miui/server/stepcounter/StepCounterManagerService;->-$$Nest$fputisRegisterDetectorListener(Lcom/miui/server/stepcounter/StepCounterManagerService;Z)V

    .line 603
    const-string v1, "Disable Step Counter"

    invoke-virtual {v0, v1}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V

    .line 604
    goto :goto_2

    .line 598
    :pswitch_1
    invoke-direct {p0}, Lcom/miui/server/stepcounter/StepCounterManagerService$Shell;->setTrimAll()V

    .line 599
    goto :goto_2

    .line 595
    :pswitch_2
    invoke-direct {p0}, Lcom/miui/server/stepcounter/StepCounterManagerService$Shell;->setRecordDelay()V

    .line 596
    goto :goto_2

    .line 592
    :pswitch_3
    invoke-direct {p0}, Lcom/miui/server/stepcounter/StepCounterManagerService$Shell;->setDelay()V

    .line 593
    goto :goto_2

    .line 588
    :pswitch_4
    iget-object v1, p0, Lcom/miui/server/stepcounter/StepCounterManagerService$Shell;->this$0:Lcom/miui/server/stepcounter/StepCounterManagerService;

    invoke-static {v1, v3}, Lcom/miui/server/stepcounter/StepCounterManagerService;->-$$Nest$msetDebugEnabled(Lcom/miui/server/stepcounter/StepCounterManagerService;Z)V

    .line 589
    const-string v1, "Set Step Counter Log Disable"

    invoke-virtual {v0, v1}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V

    .line 590
    goto :goto_2

    .line 584
    :pswitch_5
    iget-object v1, p0, Lcom/miui/server/stepcounter/StepCounterManagerService$Shell;->this$0:Lcom/miui/server/stepcounter/StepCounterManagerService;

    invoke-static {v1, v2}, Lcom/miui/server/stepcounter/StepCounterManagerService;->-$$Nest$msetDebugEnabled(Lcom/miui/server/stepcounter/StepCounterManagerService;Z)V

    .line 585
    const-string v1, "Set Step Counter Log Enable"

    invoke-virtual {v0, v1}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V

    .line 586
    nop

    .line 611
    :goto_2
    return v3

    nop

    :sswitch_data_0
    .sparse-switch
        -0x1e9dc7a8 -> :sswitch_7
        0x5db -> :sswitch_6
        0x39b81092 -> :sswitch_5
        0x3e04027a -> :sswitch_4
        0x4f7504e1 -> :sswitch_3
        0x59d18176 -> :sswitch_2
        0x639e22e8 -> :sswitch_1
        0x670c0bb1 -> :sswitch_0
    .end sparse-switch

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_5
        :pswitch_4
        :pswitch_3
        :pswitch_2
        :pswitch_1
        :pswitch_0
    .end packed-switch
.end method

.method private setDelay()V
    .locals 5

    .line 615
    invoke-virtual {p0}, Lcom/miui/server/stepcounter/StepCounterManagerService$Shell;->getOutPrintWriter()Ljava/io/PrintWriter;

    move-result-object v0

    .line 616
    .local v0, "pw":Ljava/io/PrintWriter;
    const/4 v1, -0x1

    .line 618
    .local v1, "delay":I
    :try_start_0
    invoke-virtual {p0}, Lcom/miui/server/stepcounter/StepCounterManagerService$Shell;->getNextArgRequired()Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v2
    :try_end_0
    .catch Ljava/lang/RuntimeException; {:try_start_0 .. :try_end_0} :catch_0

    move v1, v2

    .line 621
    goto :goto_0

    .line 619
    :catch_0
    move-exception v2

    .line 620
    .local v2, "ex":Ljava/lang/RuntimeException;
    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "Error: "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v2}, Ljava/lang/RuntimeException;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v0, v3}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V

    .line 622
    .end local v2    # "ex":Ljava/lang/RuntimeException;
    :goto_0
    const-string v2, "s"

    if-lez v1, :cond_0

    .line 623
    iget-object v3, p0, Lcom/miui/server/stepcounter/StepCounterManagerService$Shell;->this$0:Lcom/miui/server/stepcounter/StepCounterManagerService;

    invoke-static {v3, v1}, Lcom/miui/server/stepcounter/StepCounterManagerService;->-$$Nest$msetDelayValue(Lcom/miui/server/stepcounter/StepCounterManagerService;I)V

    .line 624
    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v4, "set delay success! current delay is "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v2}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V

    goto :goto_1

    .line 626
    :cond_0
    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v4, "set delay fail! current delay is "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v2}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V

    .line 628
    :goto_1
    return-void
.end method

.method private setRecordDelay()V
    .locals 5

    .line 631
    invoke-virtual {p0}, Lcom/miui/server/stepcounter/StepCounterManagerService$Shell;->getOutPrintWriter()Ljava/io/PrintWriter;

    move-result-object v0

    .line 632
    .local v0, "pw":Ljava/io/PrintWriter;
    const/4 v1, -0x1

    .line 634
    .local v1, "recordDelay":I
    :try_start_0
    invoke-virtual {p0}, Lcom/miui/server/stepcounter/StepCounterManagerService$Shell;->getNextArgRequired()Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v2
    :try_end_0
    .catch Ljava/lang/RuntimeException; {:try_start_0 .. :try_end_0} :catch_0

    move v1, v2

    .line 637
    goto :goto_0

    .line 635
    :catch_0
    move-exception v2

    .line 636
    .local v2, "ex":Ljava/lang/RuntimeException;
    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "Error: "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v2}, Ljava/lang/RuntimeException;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v0, v3}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V

    .line 638
    .end local v2    # "ex":Ljava/lang/RuntimeException;
    :goto_0
    const-string v2, "s"

    if-lez v1, :cond_0

    .line 639
    iget-object v3, p0, Lcom/miui/server/stepcounter/StepCounterManagerService$Shell;->this$0:Lcom/miui/server/stepcounter/StepCounterManagerService;

    invoke-static {v3, v1}, Lcom/miui/server/stepcounter/StepCounterManagerService;->-$$Nest$msetRecordDelayValue(Lcom/miui/server/stepcounter/StepCounterManagerService;I)V

    .line 640
    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v4, "set record-delay success! current record-delay is "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v2}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V

    goto :goto_1

    .line 642
    :cond_0
    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v4, "set record-delay fail! current record-delay is "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v2}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V

    .line 644
    :goto_1
    return-void
.end method

.method private setTrimAll()V
    .locals 3

    .line 647
    invoke-virtual {p0}, Lcom/miui/server/stepcounter/StepCounterManagerService$Shell;->getOutPrintWriter()Ljava/io/PrintWriter;

    move-result-object v0

    .line 648
    .local v0, "pw":Ljava/io/PrintWriter;
    iget-object v1, p0, Lcom/miui/server/stepcounter/StepCounterManagerService$Shell;->this$0:Lcom/miui/server/stepcounter/StepCounterManagerService;

    invoke-static {v1}, Lcom/miui/server/stepcounter/StepCounterManagerService;->-$$Nest$fgetmHaveStepSensor(Lcom/miui/server/stepcounter/StepCounterManagerService;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 649
    iget-object v1, p0, Lcom/miui/server/stepcounter/StepCounterManagerService$Shell;->this$0:Lcom/miui/server/stepcounter/StepCounterManagerService;

    invoke-static {v1}, Lcom/miui/server/stepcounter/StepCounterManagerService;->-$$Nest$fgetmHandler(Lcom/miui/server/stepcounter/StepCounterManagerService;)Lcom/miui/server/stepcounter/StepCounterManagerService$StepCountHandler;

    move-result-object v1

    const/16 v2, 0x8

    invoke-virtual {v1, v2}, Lcom/miui/server/stepcounter/StepCounterManagerService$StepCountHandler;->sendEmptyMessage(I)Z

    .line 650
    const-string/jumbo v1, "trimAll success!"

    invoke-virtual {v0, v1}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V

    goto :goto_0

    .line 652
    :cond_0
    const-string/jumbo v1, "trimAll fail!"

    invoke-virtual {v0, v1}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V

    .line 654
    :goto_0
    return-void
.end method


# virtual methods
.method public onCommand(Ljava/lang/String;)I
    .locals 1
    .param p1, "cmd"    # Ljava/lang/String;

    .line 569
    invoke-direct {p0, p1}, Lcom/miui/server/stepcounter/StepCounterManagerService$Shell;->onShellCommand(Ljava/lang/String;)I

    move-result v0

    return v0
.end method

.method public onHelp()V
    .locals 0

    .line 574
    invoke-direct {p0}, Lcom/miui/server/stepcounter/StepCounterManagerService$Shell;->dumpHelp()V

    .line 575
    return-void
.end method
