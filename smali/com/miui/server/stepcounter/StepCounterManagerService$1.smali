.class Lcom/miui/server/stepcounter/StepCounterManagerService$1;
.super Ljava/lang/Object;
.source "StepCounterManagerService.java"

# interfaces
.implements Ljava/lang/Runnable;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/miui/server/stepcounter/StepCounterManagerService;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/miui/server/stepcounter/StepCounterManagerService;


# direct methods
.method constructor <init>(Lcom/miui/server/stepcounter/StepCounterManagerService;)V
    .locals 0
    .param p1, "this$0"    # Lcom/miui/server/stepcounter/StepCounterManagerService;

    .line 152
    iput-object p1, p0, Lcom/miui/server/stepcounter/StepCounterManagerService$1;->this$0:Lcom/miui/server/stepcounter/StepCounterManagerService;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public run()V
    .locals 3

    .line 155
    iget-object v0, p0, Lcom/miui/server/stepcounter/StepCounterManagerService$1;->this$0:Lcom/miui/server/stepcounter/StepCounterManagerService;

    invoke-static {v0}, Lcom/miui/server/stepcounter/StepCounterManagerService;->-$$Nest$fgetmHandler(Lcom/miui/server/stepcounter/StepCounterManagerService;)Lcom/miui/server/stepcounter/StepCounterManagerService$StepCountHandler;

    move-result-object v0

    iget-object v1, p0, Lcom/miui/server/stepcounter/StepCounterManagerService$1;->this$0:Lcom/miui/server/stepcounter/StepCounterManagerService;

    invoke-static {v1}, Lcom/miui/server/stepcounter/StepCounterManagerService;->-$$Nest$fgetmDelay(Lcom/miui/server/stepcounter/StepCounterManagerService;)I

    move-result v1

    int-to-long v1, v1

    invoke-virtual {v0, p0, v1, v2}, Lcom/miui/server/stepcounter/StepCounterManagerService$StepCountHandler;->postDelayed(Ljava/lang/Runnable;J)Z

    .line 156
    iget-object v0, p0, Lcom/miui/server/stepcounter/StepCounterManagerService$1;->this$0:Lcom/miui/server/stepcounter/StepCounterManagerService;

    invoke-static {v0}, Lcom/miui/server/stepcounter/StepCounterManagerService;->-$$Nest$fgetmHandler(Lcom/miui/server/stepcounter/StepCounterManagerService;)Lcom/miui/server/stepcounter/StepCounterManagerService$StepCountHandler;

    move-result-object v0

    const/4 v1, 0x2

    invoke-virtual {v0, v1}, Lcom/miui/server/stepcounter/StepCounterManagerService$StepCountHandler;->sendEmptyMessage(I)Z

    .line 157
    invoke-static {}, Lcom/miui/server/stepcounter/StepCounterManagerService;->-$$Nest$sfgetsDEBUG()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 158
    const-string v0, "StepCounterManagerService"

    const-string/jumbo v1, "sendEmptyMessage(MSG_TRIM): trigger trim\uff01"

    invoke-static {v0, v1}, Landroid/util/Slog;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 160
    :cond_0
    const-string v0, "is_pad"

    const/4 v1, 0x0

    invoke-static {v0, v1}, Lmiui/util/FeatureParser;->getBoolean(Ljava/lang/String;Z)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 161
    iget-object v0, p0, Lcom/miui/server/stepcounter/StepCounterManagerService$1;->this$0:Lcom/miui/server/stepcounter/StepCounterManagerService;

    invoke-static {v0}, Lcom/miui/server/stepcounter/StepCounterManagerService;->-$$Nest$mreportStepToOneTrack(Lcom/miui/server/stepcounter/StepCounterManagerService;)V

    .line 163
    :cond_1
    return-void
.end method
