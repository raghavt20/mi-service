public class com.miui.server.stepcounter.StepCounterManagerService extends com.android.server.SystemService {
	 /* .source "StepCounterManagerService.java" */
	 /* # annotations */
	 /* .annotation system Ldalvik/annotation/MemberClasses; */
	 /* value = { */
	 /* Lcom/miui/server/stepcounter/StepCounterManagerService$IntentReceiver;, */
	 /* Lcom/miui/server/stepcounter/StepCounterManagerService$StepCountHandler;, */
	 /* Lcom/miui/server/stepcounter/StepCounterManagerService$StepDetectorListener;, */
	 /* Lcom/miui/server/stepcounter/StepCounterManagerService$TreadmillListener;, */
	 /* Lcom/miui/server/stepcounter/StepCounterManagerService$LocalService;, */
	 /* Lcom/miui/server/stepcounter/StepCounterManagerService$BinderService;, */
	 /* Lcom/miui/server/stepcounter/StepCounterManagerService$Shell;, */
	 /* Lcom/miui/server/stepcounter/StepCounterManagerService$ClientDeathCallback; */
	 /* } */
} // .end annotation
/* # static fields */
private static final java.lang.String ACTION_SLEEP_CHANGED;
private static final Integer DEFAULT_DELAY;
private static final Integer DEFAULT_RECORD_DELAY;
private static final Integer DUMP_NUMBER;
private static final java.lang.String EXTRA_STATE;
private static final Integer FIRST_DELAY;
private static final Integer MSG_ADD_STEP;
private static final Integer MSG_TRIM;
private static final Integer MSG_TRIM_ALL;
private static final Integer MSG_TRIM_ALL_FORCE;
private static final java.lang.String PROP_STEPS_SUPPORT;
private static final java.lang.String SENSOR_NAME;
private static final Integer STATE_ENTER_SLEEP;
private static final Integer STATE_EXIT_SLEEP;
private static Long SYSTEM_BOOT_TIME;
private static final java.lang.String TAG;
private static final Integer TYPE_TREADMILL;
private static Boolean sDEBUG;
/* # instance fields */
private Boolean isRegisterDetectorListener;
private java.util.HashMap mClientDeathCallbacks;
/* .annotation system Ldalvik/annotation/Signature; */
/* value = { */
/* "Ljava/util/HashMap<", */
/* "Landroid/os/IBinder;", */
/* "Lcom/miui/server/stepcounter/StepCounterManagerService$ClientDeathCallback;", */
/* ">;" */
/* } */
} // .end annotation
} // .end field
private android.content.Context mContext;
private Integer mDelay;
private com.miui.server.stepcounter.StepCounterManagerService$StepCountHandler mHandler;
private Boolean mHaveStepSensor;
private com.miui.server.stepcounter.StepCounterManagerService$IntentReceiver mIntentReceiver;
private Long mLastTrimAllTimeInMills;
private final java.lang.Object mLocked;
private Integer mRecordDelay;
private android.content.ContentResolver mResolver;
private java.lang.Runnable mRunnable;
private android.hardware.Sensor mSensor;
private android.hardware.SensorManager mSensorManager;
private android.hardware.SensorEventListener mStepDetectorListener;
private java.util.List mStepList;
/* .annotation system Ldalvik/annotation/Signature; */
/* value = { */
/* "Ljava/util/List<", */
/* "Lmiui/stepcounter/StepDetector;", */
/* ">;" */
/* } */
} // .end annotation
} // .end field
private Boolean mTreadmillEnabled;
private android.hardware.SensorEventListener mTreadmillListener;
private android.hardware.Sensor mTreadmillSensor;
private Integer sCurrentPos;
/* # direct methods */
public static void $r8$lambda$ZqqONIaXiqXVqU1sqdLpwr2VmmY ( com.miui.server.stepcounter.StepCounterManagerService p0, Long p1, java.lang.String p2 ) { //synthethic
/* .locals 0 */
/* invoke-direct {p0, p1, p2, p3}, Lcom/miui/server/stepcounter/StepCounterManagerService;->lambda$reportStepToOneTrack$0(JLjava/lang/String;)V */
return;
} // .end method
static Boolean -$$Nest$fgetisRegisterDetectorListener ( com.miui.server.stepcounter.StepCounterManagerService p0 ) { //bridge//synthethic
/* .locals 0 */
/* iget-boolean p0, p0, Lcom/miui/server/stepcounter/StepCounterManagerService;->isRegisterDetectorListener:Z */
} // .end method
static java.util.HashMap -$$Nest$fgetmClientDeathCallbacks ( com.miui.server.stepcounter.StepCounterManagerService p0 ) { //bridge//synthethic
/* .locals 0 */
p0 = this.mClientDeathCallbacks;
} // .end method
static android.content.Context -$$Nest$fgetmContext ( com.miui.server.stepcounter.StepCounterManagerService p0 ) { //bridge//synthethic
/* .locals 0 */
p0 = this.mContext;
} // .end method
static Integer -$$Nest$fgetmDelay ( com.miui.server.stepcounter.StepCounterManagerService p0 ) { //bridge//synthethic
/* .locals 0 */
/* iget p0, p0, Lcom/miui/server/stepcounter/StepCounterManagerService;->mDelay:I */
} // .end method
static com.miui.server.stepcounter.StepCounterManagerService$StepCountHandler -$$Nest$fgetmHandler ( com.miui.server.stepcounter.StepCounterManagerService p0 ) { //bridge//synthethic
/* .locals 0 */
p0 = this.mHandler;
} // .end method
static Boolean -$$Nest$fgetmHaveStepSensor ( com.miui.server.stepcounter.StepCounterManagerService p0 ) { //bridge//synthethic
/* .locals 0 */
/* iget-boolean p0, p0, Lcom/miui/server/stepcounter/StepCounterManagerService;->mHaveStepSensor:Z */
} // .end method
static java.lang.Object -$$Nest$fgetmLocked ( com.miui.server.stepcounter.StepCounterManagerService p0 ) { //bridge//synthethic
/* .locals 0 */
p0 = this.mLocked;
} // .end method
static android.hardware.SensorManager -$$Nest$fgetmSensorManager ( com.miui.server.stepcounter.StepCounterManagerService p0 ) { //bridge//synthethic
/* .locals 0 */
p0 = this.mSensorManager;
} // .end method
static java.util.List -$$Nest$fgetmStepList ( com.miui.server.stepcounter.StepCounterManagerService p0 ) { //bridge//synthethic
/* .locals 0 */
p0 = this.mStepList;
} // .end method
static Boolean -$$Nest$fgetmTreadmillEnabled ( com.miui.server.stepcounter.StepCounterManagerService p0 ) { //bridge//synthethic
/* .locals 0 */
/* iget-boolean p0, p0, Lcom/miui/server/stepcounter/StepCounterManagerService;->mTreadmillEnabled:Z */
} // .end method
static android.hardware.SensorEventListener -$$Nest$fgetmTreadmillListener ( com.miui.server.stepcounter.StepCounterManagerService p0 ) { //bridge//synthethic
/* .locals 0 */
p0 = this.mTreadmillListener;
} // .end method
static android.hardware.Sensor -$$Nest$fgetmTreadmillSensor ( com.miui.server.stepcounter.StepCounterManagerService p0 ) { //bridge//synthethic
/* .locals 0 */
p0 = this.mTreadmillSensor;
} // .end method
static void -$$Nest$fputisRegisterDetectorListener ( com.miui.server.stepcounter.StepCounterManagerService p0, Boolean p1 ) { //bridge//synthethic
/* .locals 0 */
/* iput-boolean p1, p0, Lcom/miui/server/stepcounter/StepCounterManagerService;->isRegisterDetectorListener:Z */
return;
} // .end method
static void -$$Nest$fputmTreadmillEnabled ( com.miui.server.stepcounter.StepCounterManagerService p0, Boolean p1 ) { //bridge//synthethic
/* .locals 0 */
/* iput-boolean p1, p0, Lcom/miui/server/stepcounter/StepCounterManagerService;->mTreadmillEnabled:Z */
return;
} // .end method
static void -$$Nest$mdumpInternal ( com.miui.server.stepcounter.StepCounterManagerService p0, java.io.PrintWriter p1 ) { //bridge//synthethic
/* .locals 0 */
/* invoke-direct {p0, p1}, Lcom/miui/server/stepcounter/StepCounterManagerService;->dumpInternal(Ljava/io/PrintWriter;)V */
return;
} // .end method
static Boolean -$$Nest$mregisterDetectorListener ( com.miui.server.stepcounter.StepCounterManagerService p0 ) { //bridge//synthethic
/* .locals 0 */
p0 = /* invoke-direct {p0}, Lcom/miui/server/stepcounter/StepCounterManagerService;->registerDetectorListener()Z */
} // .end method
static void -$$Nest$mreportStepToOneTrack ( com.miui.server.stepcounter.StepCounterManagerService p0 ) { //bridge//synthethic
/* .locals 0 */
/* invoke-direct {p0}, Lcom/miui/server/stepcounter/StepCounterManagerService;->reportStepToOneTrack()V */
return;
} // .end method
static void -$$Nest$msetDebugEnabled ( com.miui.server.stepcounter.StepCounterManagerService p0, Boolean p1 ) { //bridge//synthethic
/* .locals 0 */
/* invoke-direct {p0, p1}, Lcom/miui/server/stepcounter/StepCounterManagerService;->setDebugEnabled(Z)V */
return;
} // .end method
static void -$$Nest$msetDelayValue ( com.miui.server.stepcounter.StepCounterManagerService p0, Integer p1 ) { //bridge//synthethic
/* .locals 0 */
/* invoke-direct {p0, p1}, Lcom/miui/server/stepcounter/StepCounterManagerService;->setDelayValue(I)V */
return;
} // .end method
static void -$$Nest$msetRecordDelayValue ( com.miui.server.stepcounter.StepCounterManagerService p0, Integer p1 ) { //bridge//synthethic
/* .locals 0 */
/* invoke-direct {p0, p1}, Lcom/miui/server/stepcounter/StepCounterManagerService;->setRecordDelayValue(I)V */
return;
} // .end method
static void -$$Nest$munregisterDetectorListener ( com.miui.server.stepcounter.StepCounterManagerService p0 ) { //bridge//synthethic
/* .locals 0 */
/* invoke-direct {p0}, Lcom/miui/server/stepcounter/StepCounterManagerService;->unregisterDetectorListener()V */
return;
} // .end method
static Long -$$Nest$sfgetSYSTEM_BOOT_TIME ( ) { //bridge//synthethic
/* .locals 2 */
/* sget-wide v0, Lcom/miui/server/stepcounter/StepCounterManagerService;->SYSTEM_BOOT_TIME:J */
/* return-wide v0 */
} // .end method
static Boolean -$$Nest$sfgetsDEBUG ( ) { //bridge//synthethic
/* .locals 1 */
/* sget-boolean v0, Lcom/miui/server/stepcounter/StepCounterManagerService;->sDEBUG:Z */
} // .end method
static com.miui.server.stepcounter.StepCounterManagerService ( ) {
/* .locals 4 */
/* .line 59 */
int v0 = 0; // const/4 v0, 0x0
com.miui.server.stepcounter.StepCounterManagerService.sDEBUG = (v0!= 0);
/* .line 83 */
java.lang.System .currentTimeMillis ( );
/* move-result-wide v0 */
android.os.SystemClock .elapsedRealtime ( );
/* move-result-wide v2 */
/* sub-long/2addr v0, v2 */
/* sput-wide v0, Lcom/miui/server/stepcounter/StepCounterManagerService;->SYSTEM_BOOT_TIME:J */
return;
} // .end method
public com.miui.server.stepcounter.StepCounterManagerService ( ) {
/* .locals 5 */
/* .param p1, "context" # Landroid/content/Context; */
/* .line 104 */
/* invoke-direct {p0, p1}, Lcom/android/server/SystemService;-><init>(Landroid/content/Context;)V */
/* .line 60 */
/* new-instance v0, Ljava/lang/Object; */
/* invoke-direct {v0}, Ljava/lang/Object;-><init>()V */
this.mLocked = v0;
/* .line 84 */
int v0 = 0; // const/4 v0, 0x0
/* iput-boolean v0, p0, Lcom/miui/server/stepcounter/StepCounterManagerService;->mHaveStepSensor:Z */
/* .line 85 */
/* iput-boolean v0, p0, Lcom/miui/server/stepcounter/StepCounterManagerService;->isRegisterDetectorListener:Z */
/* .line 90 */
int v1 = 0; // const/4 v1, 0x0
this.mIntentReceiver = v1;
/* .line 101 */
/* new-instance v2, Ljava/util/HashMap; */
/* invoke-direct {v2}, Ljava/util/HashMap;-><init>()V */
this.mClientDeathCallbacks = v2;
/* .line 152 */
/* new-instance v2, Lcom/miui/server/stepcounter/StepCounterManagerService$1; */
/* invoke-direct {v2, p0}, Lcom/miui/server/stepcounter/StepCounterManagerService$1;-><init>(Lcom/miui/server/stepcounter/StepCounterManagerService;)V */
this.mRunnable = v2;
/* .line 105 */
this.mContext = p1;
/* .line 106 */
/* new-instance v2, Lcom/miui/server/stepcounter/StepCounterManagerService$StepCountHandler; */
com.android.internal.os.BackgroundThread .get ( );
(( com.android.internal.os.BackgroundThread ) v3 ).getLooper ( ); // invoke-virtual {v3}, Lcom/android/internal/os/BackgroundThread;->getLooper()Landroid/os/Looper;
/* invoke-direct {v2, p0, v3}, Lcom/miui/server/stepcounter/StepCounterManagerService$StepCountHandler;-><init>(Lcom/miui/server/stepcounter/StepCounterManagerService;Landroid/os/Looper;)V */
this.mHandler = v2;
/* .line 107 */
v2 = this.mContext;
(( android.content.Context ) v2 ).getContentResolver ( ); // invoke-virtual {v2}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;
this.mResolver = v2;
/* .line 108 */
v2 = this.mContext;
/* const-string/jumbo v3, "sensor" */
(( android.content.Context ) v2 ).getSystemService ( v3 ); // invoke-virtual {v2, v3}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;
/* check-cast v2, Landroid/hardware/SensorManager; */
this.mSensorManager = v2;
/* .line 109 */
/* const/16 v3, 0x12 */
int v4 = 1; // const/4 v4, 0x1
(( android.hardware.SensorManager ) v2 ).getDefaultSensor ( v3, v4 ); // invoke-virtual {v2, v3, v4}, Landroid/hardware/SensorManager;->getDefaultSensor(IZ)Landroid/hardware/Sensor;
this.mSensor = v2;
/* .line 110 */
v2 = this.mSensorManager;
/* const v3, 0x1fa2661 */
(( android.hardware.SensorManager ) v2 ).getDefaultSensor ( v3, v4 ); // invoke-virtual {v2, v3, v4}, Landroid/hardware/SensorManager;->getDefaultSensor(IZ)Landroid/hardware/Sensor;
this.mTreadmillSensor = v2;
/* .line 111 */
/* new-instance v2, Lcom/miui/server/stepcounter/StepCounterManagerService$StepDetectorListener; */
/* invoke-direct {v2, p0, v1}, Lcom/miui/server/stepcounter/StepCounterManagerService$StepDetectorListener;-><init>(Lcom/miui/server/stepcounter/StepCounterManagerService;Lcom/miui/server/stepcounter/StepCounterManagerService$StepDetectorListener-IA;)V */
this.mStepDetectorListener = v2;
/* .line 112 */
/* new-instance v2, Lcom/miui/server/stepcounter/StepCounterManagerService$TreadmillListener; */
/* invoke-direct {v2, p0, v1}, Lcom/miui/server/stepcounter/StepCounterManagerService$TreadmillListener;-><init>(Lcom/miui/server/stepcounter/StepCounterManagerService;Lcom/miui/server/stepcounter/StepCounterManagerService$TreadmillListener-IA;)V */
this.mTreadmillListener = v2;
/* .line 113 */
/* new-instance v2, Lcom/miui/server/stepcounter/StepCounterManagerService$IntentReceiver; */
/* invoke-direct {v2, p0, v1}, Lcom/miui/server/stepcounter/StepCounterManagerService$IntentReceiver;-><init>(Lcom/miui/server/stepcounter/StepCounterManagerService;Lcom/miui/server/stepcounter/StepCounterManagerService$IntentReceiver-IA;)V */
this.mIntentReceiver = v2;
/* .line 114 */
/* new-instance v1, Ljava/util/ArrayList; */
/* invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V */
this.mStepList = v1;
/* .line 115 */
/* const v1, 0x493e0 */
/* iput v1, p0, Lcom/miui/server/stepcounter/StepCounterManagerService;->mDelay:I */
/* .line 116 */
/* const/16 v1, 0x7530 */
/* iput v1, p0, Lcom/miui/server/stepcounter/StepCounterManagerService;->mRecordDelay:I */
/* .line 117 */
/* iput v0, p0, Lcom/miui/server/stepcounter/StepCounterManagerService;->sCurrentPos:I */
/* .line 118 */
/* const-wide/16 v0, 0x0 */
/* iput-wide v0, p0, Lcom/miui/server/stepcounter/StepCounterManagerService;->mLastTrimAllTimeInMills:J */
/* .line 120 */
/* new-instance v0, Ljava/lang/StringBuilder; */
/* invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V */
final String v1 = "Create StepCounterManagerService success "; // const-string v1, "Create StepCounterManagerService success "
(( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
v1 = android.os.UserHandle .myUserId ( );
(( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v0 ).toString ( ); // invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
final String v1 = "StepCounterManagerService"; // const-string v1, "StepCounterManagerService"
android.util.Slog .i ( v1,v0 );
/* .line 121 */
return;
} // .end method
private void dumpInternal ( java.io.PrintWriter p0 ) {
/* .locals 24 */
/* .param p1, "pw" # Ljava/io/PrintWriter; */
/* .line 514 */
/* move-object/from16 v1, p0 */
/* move-object/from16 v2, p1 */
final String v0 = "Step Counter Manager Service(dumpsys miui_step_counter_service)\n"; // const-string v0, "Step Counter Manager Service(dumpsys miui_step_counter_service)\n"
(( java.io.PrintWriter ) v2 ).println ( v0 ); // invoke-virtual {v2, v0}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V
/* .line 515 */
v3 = this.mLocked;
/* monitor-enter v3 */
/* .line 516 */
try { // :try_start_0
/* new-instance v0, Ljava/lang/StringBuilder; */
/* invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V */
final String v4 = "Delay: "; // const-string v4, "Delay: "
(( java.lang.StringBuilder ) v0 ).append ( v4 ); // invoke-virtual {v0, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
/* iget v4, v1, Lcom/miui/server/stepcounter/StepCounterManagerService;->mDelay:I */
/* div-int/lit16 v4, v4, 0x3e8 */
(( java.lang.StringBuilder ) v0 ).append ( v4 ); // invoke-virtual {v0, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;
final String v4 = "s, default: "; // const-string v4, "s, default: "
(( java.lang.StringBuilder ) v0 ).append ( v4 ); // invoke-virtual {v0, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
/* const/16 v4, 0x12c */
(( java.lang.StringBuilder ) v0 ).append ( v4 ); // invoke-virtual {v0, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;
final String v4 = "s"; // const-string v4, "s"
(( java.lang.StringBuilder ) v0 ).append ( v4 ); // invoke-virtual {v0, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v0 ).toString ( ); // invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
(( java.io.PrintWriter ) v2 ).println ( v0 ); // invoke-virtual {v2, v0}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V
/* .line 517 */
/* new-instance v0, Ljava/lang/StringBuilder; */
/* invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V */
final String v4 = "Record Delay: "; // const-string v4, "Record Delay: "
(( java.lang.StringBuilder ) v0 ).append ( v4 ); // invoke-virtual {v0, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
/* iget v4, v1, Lcom/miui/server/stepcounter/StepCounterManagerService;->mRecordDelay:I */
/* div-int/lit16 v4, v4, 0x3e8 */
(( java.lang.StringBuilder ) v0 ).append ( v4 ); // invoke-virtual {v0, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;
final String v4 = "s, default: "; // const-string v4, "s, default: "
(( java.lang.StringBuilder ) v0 ).append ( v4 ); // invoke-virtual {v0, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
/* const/16 v4, 0x1e */
(( java.lang.StringBuilder ) v0 ).append ( v4 ); // invoke-virtual {v0, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;
final String v4 = "s"; // const-string v4, "s"
(( java.lang.StringBuilder ) v0 ).append ( v4 ); // invoke-virtual {v0, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v0 ).toString ( ); // invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
(( java.io.PrintWriter ) v2 ).println ( v0 ); // invoke-virtual {v2, v0}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V
/* .line 518 */
/* invoke-virtual/range {p1 ..p1}, Ljava/io/PrintWriter;->println()V */
/* .line 520 */
/* new-instance v0, Ljava/lang/StringBuilder; */
/* invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V */
final String v4 = "current step list cache size: "; // const-string v4, "current step list cache size: "
(( java.lang.StringBuilder ) v0 ).append ( v4 ); // invoke-virtual {v0, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
v4 = v4 = this.mStepList;
(( java.lang.StringBuilder ) v0 ).append ( v4 ); // invoke-virtual {v0, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v0 ).toString ( ); // invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
(( java.io.PrintWriter ) v2 ).println ( v0 ); // invoke-virtual {v2, v0}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V
/* .line 521 */
int v0 = 0; // const/4 v0, 0x0
/* .local v0, "i":I */
} // :goto_0
v4 = v4 = this.mStepList;
/* if-ge v0, v4, :cond_0 */
/* .line 522 */
/* new-instance v4, Ljava/lang/StringBuilder; */
/* invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V */
/* add-int/lit8 v5, v0, 0x1 */
(( java.lang.StringBuilder ) v4 ).append ( v5 ); // invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;
final String v5 = " "; // const-string v5, " "
(( java.lang.StringBuilder ) v4 ).append ( v5 ); // invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v4 ).toString ( ); // invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
(( java.io.PrintWriter ) v2 ).print ( v4 ); // invoke-virtual {v2, v4}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V
/* .line 523 */
v4 = this.mStepList;
/* check-cast v4, Lmiui/stepcounter/StepDetector; */
(( miui.stepcounter.StepDetector ) v4 ).dump ( v2 ); // invoke-virtual {v4, v2}, Lmiui/stepcounter/StepDetector;->dump(Ljava/io/PrintWriter;)V
/* .line 521 */
/* add-int/lit8 v0, v0, 0x1 */
/* .line 525 */
} // .end local v0 # "i":I
} // :cond_0
/* invoke-virtual/range {p1 ..p1}, Ljava/io/PrintWriter;->println()V */
/* .line 528 */
/* new-instance v0, Ljava/util/ArrayList; */
/* invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V */
/* .line 529 */
/* .local v0, "mStepCellList":Ljava/util/List;, "Ljava/util/List<Lmiui/stepcounter/StepCell;>;" */
v4 = this.mResolver;
v5 = miui.stepcounter.StepSqlite.CONTENT_URI;
v6 = miui.stepcounter.StepSqlite.DEFAULT_PROJECTION;
int v7 = 0; // const/4 v7, 0x0
int v8 = 0; // const/4 v8, 0x0
final String v9 = "_id DESC"; // const-string v9, "_id DESC"
/* invoke-virtual/range {v4 ..v9}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor; */
/* .line 530 */
/* .local v4, "cursor":Landroid/database/Cursor; */
/* const-wide/16 v5, 0x0 */
/* .line 531 */
/* .local v5, "mDumpRunStep":J */
/* const-wide/16 v7, 0x0 */
/* .line 532 */
/* .local v7, "mDumpWalkStep":J */
/* const-wide/16 v9, 0x0 */
/* .line 533 */
/* .local v9, "mDumpTreadmillStep":J */
/* invoke-direct/range {p0 ..p0}, Lcom/miui/server/stepcounter/StepCounterManagerService;->getTodayBeginTime()J */
/* move-result-wide v11 */
/* .line 534 */
/* .local v11, "mTodayBeginTime":J */
int v13 = 1; // const/4 v13, 0x1
if ( v4 != null) { // if-eqz v4, :cond_5
/* .line 535 */
v14 = } // :goto_1
if ( v14 != null) { // if-eqz v14, :cond_4
/* .line 536 */
int v14 = 2; // const/4 v14, 0x2
/* move-result-wide v15 */
/* cmp-long v15, v15, v11 */
/* if-lez v15, :cond_3 */
/* .line 537 */
/* new-instance v15, Lmiui/stepcounter/StepCell; */
v17 = int v14 = 0; // const/4 v14, 0x0
/* .line 538 */
/* move-result-wide v18 */
int v14 = 2; // const/4 v14, 0x2
/* move-result-wide v20 */
/* .line 539 */
v22 = int v14 = 3; // const/4 v14, 0x3
v23 = int v13 = 4; // const/4 v13, 0x4
/* move-object/from16 v16, v15 */
/* invoke-direct/range {v16 ..v23}, Lmiui/stepcounter/StepCell;-><init>(IJJII)V */
/* .line 537 */
v15 = /* .line 541 */
int v14 = 2; // const/4 v14, 0x2
/* if-ne v15, v14, :cond_1 */
v13 = /* .line 542 */
/* int-to-long v13, v13 */
/* add-long/2addr v7, v13 */
int v13 = 1; // const/4 v13, 0x1
/* .line 543 */
} // :cond_1
v15 = int v14 = 3; // const/4 v14, 0x3
/* if-ne v15, v14, :cond_2 */
v13 = /* .line 544 */
/* int-to-long v13, v13 */
/* add-long/2addr v5, v13 */
int v13 = 1; // const/4 v13, 0x1
/* .line 545 */
v14 = } // :cond_2
/* if-ne v14, v13, :cond_3 */
v13 = /* .line 546 */
/* int-to-long v13, v13 */
/* add-long/2addr v9, v13 */
int v13 = 1; // const/4 v13, 0x1
/* .line 535 */
} // :cond_3
int v13 = 1; // const/4 v13, 0x1
/* .line 550 */
} // :cond_4
/* .line 552 */
} // :cond_5
/* const-string/jumbo v13, "step counter database info" */
(( java.io.PrintWriter ) v2 ).println ( v13 ); // invoke-virtual {v2, v13}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V
/* .line 553 */
/* new-instance v13, Ljava/lang/StringBuilder; */
/* invoke-direct {v13}, Ljava/lang/StringBuilder;-><init>()V */
final String v14 = "Today Total steps: "; // const-string v14, "Today Total steps: "
(( java.lang.StringBuilder ) v13 ).append ( v14 ); // invoke-virtual {v13, v14}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
/* add-long v14, v7, v5 */
/* add-long/2addr v14, v9 */
(( java.lang.StringBuilder ) v13 ).append ( v14, v15 ); // invoke-virtual {v13, v14, v15}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;
final String v14 = ", walk: "; // const-string v14, ", walk: "
(( java.lang.StringBuilder ) v13 ).append ( v14 ); // invoke-virtual {v13, v14}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v13 ).append ( v7, v8 ); // invoke-virtual {v13, v7, v8}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;
final String v14 = ", run: "; // const-string v14, ", run: "
(( java.lang.StringBuilder ) v13 ).append ( v14 ); // invoke-virtual {v13, v14}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v13 ).append ( v5, v6 ); // invoke-virtual {v13, v5, v6}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;
final String v14 = ", treadmill: "; // const-string v14, ", treadmill: "
(( java.lang.StringBuilder ) v13 ).append ( v14 ); // invoke-virtual {v13, v14}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v13 ).append ( v9, v10 ); // invoke-virtual {v13, v9, v10}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v13 ).toString ( ); // invoke-virtual {v13}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
(( java.io.PrintWriter ) v2 ).println ( v13 ); // invoke-virtual {v2, v13}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V
/* .line 557 */
final String v13 = "Today step history info:"; // const-string v13, "Today step history info:"
(( java.io.PrintWriter ) v2 ).println ( v13 ); // invoke-virtual {v2, v13}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V
/* .line 558 */
int v13 = 0; // const/4 v13, 0x0
/* .local v13, "i":I */
v14 = } // :goto_2
/* if-ge v13, v14, :cond_6 */
/* .line 559 */
/* new-instance v14, Ljava/lang/StringBuilder; */
/* invoke-direct {v14}, Ljava/lang/StringBuilder;-><init>()V */
/* add-int/lit8 v15, v13, 0x1 */
(( java.lang.StringBuilder ) v14 ).append ( v15 ); // invoke-virtual {v14, v15}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;
final String v15 = " "; // const-string v15, " "
(( java.lang.StringBuilder ) v14 ).append ( v15 ); // invoke-virtual {v14, v15}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v14 ).toString ( ); // invoke-virtual {v14}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
(( java.io.PrintWriter ) v2 ).print ( v14 ); // invoke-virtual {v2, v14}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V
v14 = /* .line 560 */
/* sub-int/2addr v14, v13 */
int v15 = 1; // const/4 v15, 0x1
/* sub-int/2addr v14, v15 */
/* check-cast v14, Lmiui/stepcounter/StepCell; */
(( miui.stepcounter.StepCell ) v14 ).dump ( v2 ); // invoke-virtual {v14, v2}, Lmiui/stepcounter/StepCell;->dump(Ljava/io/PrintWriter;)V
/* .line 558 */
/* add-int/lit8 v13, v13, 0x1 */
/* .line 562 */
} // .end local v13 # "i":I
} // :cond_6
/* .line 563 */
} // .end local v0 # "mStepCellList":Ljava/util/List;, "Ljava/util/List<Lmiui/stepcounter/StepCell;>;"
} // .end local v4 # "cursor":Landroid/database/Cursor;
} // .end local v5 # "mDumpRunStep":J
} // .end local v7 # "mDumpWalkStep":J
} // .end local v9 # "mDumpTreadmillStep":J
} // .end local v11 # "mTodayBeginTime":J
/* monitor-exit v3 */
/* .line 564 */
return;
/* .line 563 */
/* :catchall_0 */
/* move-exception v0 */
/* monitor-exit v3 */
/* :try_end_0 */
/* .catchall {:try_start_0 ..:try_end_0} :catchall_0 */
/* throw v0 */
} // .end method
private Long getTodayBeginTime ( ) {
/* .locals 3 */
/* .line 701 */
java.util.Calendar .getInstance ( );
/* .line 702 */
/* .local v0, "calendar":Ljava/util/Calendar; */
/* const/16 v1, 0xb */
int v2 = 0; // const/4 v2, 0x0
(( java.util.Calendar ) v0 ).set ( v1, v2 ); // invoke-virtual {v0, v1, v2}, Ljava/util/Calendar;->set(II)V
/* .line 703 */
/* const/16 v1, 0xc */
(( java.util.Calendar ) v0 ).set ( v1, v2 ); // invoke-virtual {v0, v1, v2}, Ljava/util/Calendar;->set(II)V
/* .line 704 */
/* const/16 v1, 0xd */
(( java.util.Calendar ) v0 ).set ( v1, v2 ); // invoke-virtual {v0, v1, v2}, Ljava/util/Calendar;->set(II)V
/* .line 705 */
/* const/16 v1, 0xe */
(( java.util.Calendar ) v0 ).set ( v1, v2 ); // invoke-virtual {v0, v1, v2}, Ljava/util/Calendar;->set(II)V
/* .line 706 */
(( java.util.Calendar ) v0 ).getTimeInMillis ( ); // invoke-virtual {v0}, Ljava/util/Calendar;->getTimeInMillis()J
/* move-result-wide v1 */
/* return-wide v1 */
} // .end method
private Long getTodayEndTime ( ) {
/* .locals 3 */
/* .line 710 */
java.util.Calendar .getInstance ( );
/* .line 711 */
/* .local v0, "calendar":Ljava/util/Calendar; */
/* const/16 v1, 0xb */
/* const/16 v2, 0x17 */
(( java.util.Calendar ) v0 ).set ( v1, v2 ); // invoke-virtual {v0, v1, v2}, Ljava/util/Calendar;->set(II)V
/* .line 712 */
/* const/16 v1, 0xc */
/* const/16 v2, 0x3b */
(( java.util.Calendar ) v0 ).set ( v1, v2 ); // invoke-virtual {v0, v1, v2}, Ljava/util/Calendar;->set(II)V
/* .line 713 */
/* const/16 v1, 0xd */
(( java.util.Calendar ) v0 ).set ( v1, v2 ); // invoke-virtual {v0, v1, v2}, Ljava/util/Calendar;->set(II)V
/* .line 714 */
/* const/16 v1, 0xe */
/* const/16 v2, 0x3e7 */
(( java.util.Calendar ) v0 ).set ( v1, v2 ); // invoke-virtual {v0, v1, v2}, Ljava/util/Calendar;->set(II)V
/* .line 715 */
(( java.util.Calendar ) v0 ).getTimeInMillis ( ); // invoke-virtual {v0}, Ljava/util/Calendar;->getTimeInMillis()J
/* move-result-wide v1 */
/* return-wide v1 */
} // .end method
private Boolean isEmpty ( ) {
/* .locals 3 */
/* .line 237 */
v0 = v0 = this.mStepList;
int v1 = 1; // const/4 v1, 0x1
/* if-nez v0, :cond_1 */
/* iget v0, p0, Lcom/miui/server/stepcounter/StepCounterManagerService;->sCurrentPos:I */
v2 = v2 = this.mStepList;
/* sub-int/2addr v2, v1 */
/* if-ne v0, v2, :cond_0 */
} // :cond_0
int v1 = 0; // const/4 v1, 0x0
} // :cond_1
} // :goto_0
} // .end method
private void lambda$reportStepToOneTrack$0 ( Long p0, java.lang.String p1 ) { //synthethic
/* .locals 4 */
/* .param p1, "mStep" # J */
/* .param p3, "DEVICE_REGION" # Ljava/lang/String; */
/* .line 749 */
final String v0 = "StepCounterManagerService"; // const-string v0, "StepCounterManagerService"
try { // :try_start_0
/* new-instance v1, Landroid/content/Intent; */
final String v2 = "onetrack.action.TRACK_EVENT"; // const-string v2, "onetrack.action.TRACK_EVENT"
/* invoke-direct {v1, v2}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V */
/* .line 750 */
/* .local v1, "intent":Landroid/content/Intent; */
final String v2 = "com.miui.analytics"; // const-string v2, "com.miui.analytics"
(( android.content.Intent ) v1 ).setPackage ( v2 ); // invoke-virtual {v1, v2}, Landroid/content/Intent;->setPackage(Ljava/lang/String;)Landroid/content/Intent;
/* .line 751 */
final String v2 = "APP_ID"; // const-string v2, "APP_ID"
final String v3 = "31000000621"; // const-string v3, "31000000621"
(( android.content.Intent ) v1 ).putExtra ( v2, v3 ); // invoke-virtual {v1, v2, v3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;
/* .line 752 */
final String v2 = "EVENT_NAME"; // const-string v2, "EVENT_NAME"
final String v3 = "StepCount"; // const-string v3, "StepCount"
(( android.content.Intent ) v1 ).putExtra ( v2, v3 ); // invoke-virtual {v1, v2, v3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;
/* .line 753 */
final String v2 = "PACKAGE"; // const-string v2, "PACKAGE"
v3 = this.mContext;
(( android.content.Context ) v3 ).getPackageName ( ); // invoke-virtual {v3}, Landroid/content/Context;->getPackageName()Ljava/lang/String;
(( android.content.Intent ) v1 ).putExtra ( v2, v3 ); // invoke-virtual {v1, v2, v3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;
/* .line 754 */
/* new-instance v2, Landroid/os/Bundle; */
/* invoke-direct {v2}, Landroid/os/Bundle;-><init>()V */
/* .line 755 */
/* .local v2, "params":Landroid/os/Bundle; */
/* const-string/jumbo v3, "step_count" */
(( android.os.Bundle ) v2 ).putLong ( v3, p1, p2 ); // invoke-virtual {v2, v3, p1, p2}, Landroid/os/Bundle;->putLong(Ljava/lang/String;J)V
/* .line 756 */
final String v3 = "CN"; // const-string v3, "CN"
v3 = (( java.lang.String ) p3 ).equals ( v3 ); // invoke-virtual {p3, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z
if ( v3 != null) { // if-eqz v3, :cond_0
/* .line 757 */
int v3 = 2; // const/4 v3, 0x2
(( android.content.Intent ) v1 ).setFlags ( v3 ); // invoke-virtual {v1, v3}, Landroid/content/Intent;->setFlags(I)Landroid/content/Intent;
/* .line 759 */
} // :cond_0
(( android.content.Intent ) v1 ).putExtras ( v2 ); // invoke-virtual {v1, v2}, Landroid/content/Intent;->putExtras(Landroid/os/Bundle;)Landroid/content/Intent;
/* .line 760 */
v3 = this.mContext;
(( android.content.Context ) v3 ).startService ( v1 ); // invoke-virtual {v3, v1}, Landroid/content/Context;->startService(Landroid/content/Intent;)Landroid/content/ComponentName;
/* :try_end_0 */
/* .catch Ljava/lang/Exception; {:try_start_0 ..:try_end_0} :catch_0 */
/* .line 763 */
/* nop */
} // .end local v1 # "intent":Landroid/content/Intent;
} // .end local v2 # "params":Landroid/os/Bundle;
/* .line 761 */
/* :catch_0 */
/* move-exception v1 */
/* .line 762 */
/* .local v1, "e":Ljava/lang/Exception; */
/* new-instance v2, Ljava/lang/StringBuilder; */
/* invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V */
final String v3 = "error reportStepToOneTrack:"; // const-string v3, "error reportStepToOneTrack:"
(( java.lang.StringBuilder ) v2 ).append ( v3 ); // invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.Exception ) v1 ).toString ( ); // invoke-virtual {v1}, Ljava/lang/Exception;->toString()Ljava/lang/String;
(( java.lang.StringBuilder ) v2 ).append ( v3 ); // invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v2 ).toString ( ); // invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
android.util.Slog .e ( v0,v2 );
/* .line 764 */
} // .end local v1 # "e":Ljava/lang/Exception;
} // :goto_0
/* new-instance v1, Ljava/lang/StringBuilder; */
/* invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V */
final String v2 = "reportStepToOneTrack step="; // const-string v2, "reportStepToOneTrack step="
(( java.lang.StringBuilder ) v1 ).append ( v2 ); // invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v1 ).append ( p1, p2 ); // invoke-virtual {v1, p1, p2}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v1 ).toString ( ); // invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
android.util.Slog .d ( v0,v1 );
/* .line 765 */
return;
} // .end method
private Boolean registerDetectorListener ( ) {
/* .locals 6 */
/* .line 300 */
v0 = this.mSensorManager;
if ( v0 != null) { // if-eqz v0, :cond_0
/* .line 301 */
v1 = this.mStepDetectorListener;
v2 = this.mSensor;
int v3 = 3; // const/4 v3, 0x3
/* iget v4, p0, Lcom/miui/server/stepcounter/StepCounterManagerService;->mDelay:I */
/* mul-int/lit16 v4, v4, 0x3e8 */
v5 = this.mHandler;
v0 = /* invoke-virtual/range {v0 ..v5}, Landroid/hardware/SensorManager;->registerListener(Landroid/hardware/SensorEventListener;Landroid/hardware/Sensor;IILandroid/os/Handler;)Z */
/* .line 304 */
} // :cond_0
final String v0 = "StepCounterManagerService"; // const-string v0, "StepCounterManagerService"
final String v1 = "StepDetector Sensor not available!"; // const-string v1, "StepDetector Sensor not available!"
android.util.Slog .e ( v0,v1 );
/* .line 305 */
int v0 = 0; // const/4 v0, 0x0
} // .end method
private void registerReceiver ( ) {
/* .locals 4 */
/* .line 147 */
/* new-instance v0, Landroid/content/IntentFilter; */
final String v1 = "android.intent.action.TIME_SET"; // const-string v1, "android.intent.action.TIME_SET"
/* invoke-direct {v0, v1}, Landroid/content/IntentFilter;-><init>(Ljava/lang/String;)V */
/* .line 148 */
/* .local v0, "filter":Landroid/content/IntentFilter; */
final String v1 = "com.miui.powerkeeper_sleep_changed"; // const-string v1, "com.miui.powerkeeper_sleep_changed"
(( android.content.IntentFilter ) v0 ).addAction ( v1 ); // invoke-virtual {v0, v1}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V
/* .line 149 */
v1 = this.mContext;
v2 = this.mIntentReceiver;
int v3 = 2; // const/4 v3, 0x2
(( android.content.Context ) v1 ).registerReceiver ( v2, v0, v3 ); // invoke-virtual {v1, v2, v0, v3}, Landroid/content/Context;->registerReceiver(Landroid/content/BroadcastReceiver;Landroid/content/IntentFilter;I)Landroid/content/Intent;
/* .line 150 */
return;
} // .end method
private void reportStepToOneTrack ( ) {
/* .locals 18 */
/* .line 719 */
/* move-object/from16 v0, p0 */
final String v1 = "31000000621"; // const-string v1, "31000000621"
/* .line 720 */
/* .local v1, "EVENT_APP_ID":Ljava/lang/String; */
final String v2 = "StepCount"; // const-string v2, "StepCount"
/* .line 721 */
/* .local v2, "EVENT_NAME":Ljava/lang/String; */
int v3 = 2; // const/4 v3, 0x2
/* .line 722 */
/* .local v3, "FLAG_NON_ANONYMOUS":I */
final String v4 = "onetrack.action.TRACK_EVENT"; // const-string v4, "onetrack.action.TRACK_EVENT"
/* .line 723 */
/* .local v4, "ONE_TRACK_ACTION":Ljava/lang/String; */
final String v5 = "ro.miui.region"; // const-string v5, "ro.miui.region"
final String v6 = "CN"; // const-string v6, "CN"
android.os.SystemProperties .get ( v5,v6 );
/* .line 725 */
/* .local v5, "DEVICE_REGION":Ljava/lang/String; */
/* invoke-direct/range {p0 ..p0}, Lcom/miui/server/stepcounter/StepCounterManagerService;->getTodayEndTime()J */
/* move-result-wide v6 */
java.lang.System .currentTimeMillis ( );
/* move-result-wide v8 */
/* sub-long/2addr v6, v8 */
/* iget v8, v0, Lcom/miui/server/stepcounter/StepCounterManagerService;->mDelay:I */
/* int-to-long v8, v8 */
/* cmp-long v6, v6, v8 */
/* if-gtz v6, :cond_6 */
/* .line 726 */
/* invoke-direct/range {p0 ..p0}, Lcom/miui/server/stepcounter/StepCounterManagerService;->getTodayEndTime()J */
/* move-result-wide v6 */
java.lang.System .currentTimeMillis ( );
/* move-result-wide v8 */
/* sub-long/2addr v6, v8 */
/* const-wide/16 v8, 0x0 */
/* cmp-long v6, v6, v8 */
/* if-lez v6, :cond_5 */
/* .line 728 */
/* const-wide/16 v6, 0x0 */
/* .line 729 */
/* .local v6, "mDumpRunStep":J */
/* const-wide/16 v8, 0x0 */
/* .line 730 */
/* .local v8, "mDumpWalkStep":J */
/* const-wide/16 v10, 0x0 */
/* .line 731 */
/* .local v10, "mDumpTreadmillStep":J */
v12 = this.mResolver;
v13 = miui.stepcounter.StepSqlite.CONTENT_URI;
v14 = miui.stepcounter.StepSqlite.DEFAULT_PROJECTION;
int v15 = 0; // const/4 v15, 0x0
/* const/16 v16, 0x0 */
final String v17 = "_id DESC"; // const-string v17, "_id DESC"
/* invoke-virtual/range {v12 ..v17}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor; */
/* .line 732 */
/* .local v12, "cursor":Landroid/database/Cursor; */
if ( v12 != null) { // if-eqz v12, :cond_4
/* .line 733 */
} // :cond_0
v13 = } // :goto_0
if ( v13 != null) { // if-eqz v13, :cond_3
/* .line 734 */
int v13 = 2; // const/4 v13, 0x2
/* move-result-wide v14 */
/* invoke-direct/range {p0 ..p0}, Lcom/miui/server/stepcounter/StepCounterManagerService;->getTodayBeginTime()J */
/* move-result-wide v16 */
/* cmp-long v14, v14, v16 */
/* if-lez v14, :cond_0 */
/* .line 735 */
v15 = int v14 = 3; // const/4 v14, 0x3
int v14 = 4; // const/4 v14, 0x4
/* if-ne v15, v13, :cond_1 */
v13 = /* .line 736 */
/* int-to-long v13, v13 */
/* add-long/2addr v8, v13 */
/* .line 737 */
} // :cond_1
v15 = int v13 = 3; // const/4 v13, 0x3
/* if-ne v15, v13, :cond_2 */
v13 = /* .line 738 */
/* int-to-long v13, v13 */
/* add-long/2addr v6, v13 */
/* .line 739 */
v13 = } // :cond_2
/* if-ne v13, v14, :cond_0 */
v13 = /* .line 740 */
/* int-to-long v13, v13 */
/* add-long/2addr v10, v13 */
/* .line 744 */
} // :cond_3
/* .line 746 */
} // :cond_4
/* add-long v13, v8, v6 */
/* add-long/2addr v13, v10 */
/* .line 747 */
/* .local v13, "mStep":J */
com.android.internal.os.BackgroundThread .getHandler ( );
/* move-object/from16 v16, v1 */
} // .end local v1 # "EVENT_APP_ID":Ljava/lang/String;
/* .local v16, "EVENT_APP_ID":Ljava/lang/String; */
/* new-instance v1, Lcom/miui/server/stepcounter/StepCounterManagerService$$ExternalSyntheticLambda0; */
/* invoke-direct {v1, v0, v13, v14, v5}, Lcom/miui/server/stepcounter/StepCounterManagerService$$ExternalSyntheticLambda0;-><init>(Lcom/miui/server/stepcounter/StepCounterManagerService;JLjava/lang/String;)V */
(( android.os.Handler ) v15 ).post ( v1 ); // invoke-virtual {v15, v1}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z
/* .line 726 */
} // .end local v6 # "mDumpRunStep":J
} // .end local v8 # "mDumpWalkStep":J
} // .end local v10 # "mDumpTreadmillStep":J
} // .end local v12 # "cursor":Landroid/database/Cursor;
} // .end local v13 # "mStep":J
} // .end local v16 # "EVENT_APP_ID":Ljava/lang/String;
/* .restart local v1 # "EVENT_APP_ID":Ljava/lang/String; */
} // :cond_5
/* move-object/from16 v16, v1 */
} // .end local v1 # "EVENT_APP_ID":Ljava/lang/String;
/* .restart local v16 # "EVENT_APP_ID":Ljava/lang/String; */
/* .line 725 */
} // .end local v16 # "EVENT_APP_ID":Ljava/lang/String;
/* .restart local v1 # "EVENT_APP_ID":Ljava/lang/String; */
} // :cond_6
/* move-object/from16 v16, v1 */
/* .line 767 */
} // .end local v1 # "EVENT_APP_ID":Ljava/lang/String;
/* .restart local v16 # "EVENT_APP_ID":Ljava/lang/String; */
} // :goto_1
return;
} // .end method
public static void resetSystemBootTime ( ) {
/* .locals 4 */
/* .line 296 */
java.lang.System .currentTimeMillis ( );
/* move-result-wide v0 */
android.os.SystemClock .elapsedRealtime ( );
/* move-result-wide v2 */
/* sub-long/2addr v0, v2 */
/* sput-wide v0, Lcom/miui/server/stepcounter/StepCounterManagerService;->SYSTEM_BOOT_TIME:J */
/* .line 297 */
return;
} // .end method
private void setDebugEnabled ( Boolean p0 ) {
/* .locals 4 */
/* .param p1, "enabled" # Z */
/* .line 688 */
v0 = this.mLocked;
/* monitor-enter v0 */
/* .line 689 */
try { // :try_start_0
android.os.Binder .clearCallingIdentity ( );
/* move-result-wide v1 */
/* :try_end_0 */
/* .catchall {:try_start_0 ..:try_end_0} :catchall_1 */
/* .line 691 */
/* .local v1, "ident":J */
try { // :try_start_1
com.miui.server.stepcounter.StepCounterManagerService.sDEBUG = (p1!= 0);
/* .line 692 */
miui.stepcounter.StepProvider .updateDebug ( p1 );
/* .line 693 */
miui.stepcounter.StepMode .updateDebug ( p1 );
/* :try_end_1 */
/* .catchall {:try_start_1 ..:try_end_1} :catchall_0 */
/* .line 695 */
try { // :try_start_2
android.os.Binder .restoreCallingIdentity ( v1,v2 );
/* .line 696 */
/* nop */
/* .line 697 */
} // .end local v1 # "ident":J
/* monitor-exit v0 */
/* .line 698 */
return;
/* .line 695 */
/* .restart local v1 # "ident":J */
/* :catchall_0 */
/* move-exception v3 */
android.os.Binder .restoreCallingIdentity ( v1,v2 );
/* .line 696 */
/* nop */
} // .end local p0 # "this":Lcom/miui/server/stepcounter/StepCounterManagerService;
} // .end local p1 # "enabled":Z
/* throw v3 */
/* .line 697 */
} // .end local v1 # "ident":J
/* .restart local p0 # "this":Lcom/miui/server/stepcounter/StepCounterManagerService; */
/* .restart local p1 # "enabled":Z */
/* :catchall_1 */
/* move-exception v1 */
/* monitor-exit v0 */
/* :try_end_2 */
/* .catchall {:try_start_2 ..:try_end_2} :catchall_1 */
/* throw v1 */
} // .end method
private void setDelayValue ( Integer p0 ) {
/* .locals 1 */
/* .param p1, "delay" # I */
/* .line 680 */
/* mul-int/lit16 v0, p1, 0x3e8 */
/* iput v0, p0, Lcom/miui/server/stepcounter/StepCounterManagerService;->mDelay:I */
/* .line 681 */
return;
} // .end method
private void setRecordDelayValue ( Integer p0 ) {
/* .locals 1 */
/* .param p1, "recordDelay" # I */
/* .line 684 */
/* mul-int/lit16 v0, p1, 0x3e8 */
/* iput v0, p0, Lcom/miui/server/stepcounter/StepCounterManagerService;->mRecordDelay:I */
/* .line 685 */
return;
} // .end method
private void trim ( ) {
/* .locals 9 */
/* .line 203 */
/* iget v0, p0, Lcom/miui/server/stepcounter/StepCounterManagerService;->sCurrentPos:I */
/* .line 204 */
/* .local v0, "begin":I */
v1 = this.mStepList;
/* iget v2, p0, Lcom/miui/server/stepcounter/StepCounterManagerService;->sCurrentPos:I */
/* check-cast v1, Lmiui/stepcounter/StepDetector; */
/* .line 206 */
/* .local v1, "head":Lmiui/stepcounter/StepDetector; */
} // :goto_0
v2 = v2 = this.mStepList;
final String v3 = "StepCounterManagerService"; // const-string v3, "StepCounterManagerService"
/* if-nez v2, :cond_4 */
/* .line 207 */
v2 = this.mStepList;
/* iget v4, p0, Lcom/miui/server/stepcounter/StepCounterManagerService;->sCurrentPos:I */
/* check-cast v2, Lmiui/stepcounter/StepDetector; */
/* .line 208 */
/* .local v2, "tail":Lmiui/stepcounter/StepDetector; */
if ( v2 != null) { // if-eqz v2, :cond_3
if ( v1 != null) { // if-eqz v1, :cond_3
/* .line 209 */
(( miui.stepcounter.StepDetector ) v2 ).getTimestamp ( ); // invoke-virtual {v2}, Lmiui/stepcounter/StepDetector;->getTimestamp()J
/* move-result-wide v4 */
(( miui.stepcounter.StepDetector ) v1 ).getTimestamp ( ); // invoke-virtual {v1}, Lmiui/stepcounter/StepDetector;->getTimestamp()J
/* move-result-wide v6 */
/* sub-long/2addr v4, v6 */
/* .line 211 */
/* .local v4, "deltaTime":J */
/* iget v6, p0, Lcom/miui/server/stepcounter/StepCounterManagerService;->mRecordDelay:I */
/* int-to-long v7, v6 */
/* cmp-long v7, v4, v7 */
/* if-lez v7, :cond_0 */
/* iget v7, p0, Lcom/miui/server/stepcounter/StepCounterManagerService;->sCurrentPos:I */
/* if-lez v7, :cond_0 */
/* .line 212 */
/* add-int/lit8 v7, v7, -0x1 */
/* iput v7, p0, Lcom/miui/server/stepcounter/StepCounterManagerService;->sCurrentPos:I */
/* .line 213 */
} // :cond_0
/* int-to-long v6, v6 */
/* cmp-long v6, v4, v6 */
/* if-gez v6, :cond_2 */
v6 = /* invoke-direct {p0}, Lcom/miui/server/stepcounter/StepCounterManagerService;->isEmpty()Z */
if ( v6 != null) { // if-eqz v6, :cond_1
/* .line 226 */
} // :cond_1
/* iget v3, p0, Lcom/miui/server/stepcounter/StepCounterManagerService;->sCurrentPos:I */
/* add-int/lit8 v3, v3, 0x1 */
/* iput v3, p0, Lcom/miui/server/stepcounter/StepCounterManagerService;->sCurrentPos:I */
/* .line 227 */
} // .end local v4 # "deltaTime":J
/* nop */
/* .line 230 */
} // .end local v2 # "tail":Lmiui/stepcounter/StepDetector;
/* .line 215 */
/* .restart local v2 # "tail":Lmiui/stepcounter/StepDetector; */
/* .restart local v4 # "deltaTime":J */
} // :cond_2
} // :goto_1
try { // :try_start_0
/* new-instance v6, Lmiui/stepcounter/StepMode; */
v7 = this.mStepList;
/* iget v8, p0, Lcom/miui/server/stepcounter/StepCounterManagerService;->sCurrentPos:I */
/* invoke-direct {v6, v7, v0, v8}, Lmiui/stepcounter/StepMode;-><init>(Ljava/util/List;II)V */
v7 = this.mResolver;
(( miui.stepcounter.StepMode ) v6 ).run ( v7 ); // invoke-virtual {v6, v7}, Lmiui/stepcounter/StepMode;->run(Landroid/content/ContentResolver;)V
/* :try_end_0 */
/* .catch Ljava/lang/IllegalArgumentException; {:try_start_0 ..:try_end_0} :catch_0 */
/* .line 219 */
/* nop */
/* .line 221 */
v6 = /* invoke-direct {p0}, Lcom/miui/server/stepcounter/StepCounterManagerService;->isEmpty()Z */
/* if-nez v6, :cond_4 */
/* .line 222 */
/* iget v6, p0, Lcom/miui/server/stepcounter/StepCounterManagerService;->sCurrentPos:I */
/* add-int/lit8 v6, v6, 0x1 */
/* iput v6, p0, Lcom/miui/server/stepcounter/StepCounterManagerService;->sCurrentPos:I */
/* .line 216 */
/* :catch_0 */
/* move-exception v6 */
/* .line 217 */
/* .local v6, "e":Ljava/lang/IllegalArgumentException; */
final String v7 = "Illegal argument"; // const-string v7, "Illegal argument"
android.util.Slog .i ( v3,v7 );
/* .line 218 */
return;
/* .line 228 */
} // .end local v4 # "deltaTime":J
} // .end local v6 # "e":Ljava/lang/IllegalArgumentException;
} // :cond_3
return;
/* .line 231 */
} // .end local v2 # "tail":Lmiui/stepcounter/StepDetector;
} // :cond_4
} // :goto_2
/* sget-boolean v2, Lcom/miui/server/stepcounter/StepCounterManagerService;->sDEBUG:Z */
if ( v2 != null) { // if-eqz v2, :cond_5
/* .line 232 */
/* new-instance v2, Ljava/lang/StringBuilder; */
/* invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V */
/* const-string/jumbo v4, "step current position start at " */
(( java.lang.StringBuilder ) v2 ).append ( v4 ); // invoke-virtual {v2, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v2 ).append ( v0 ); // invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;
final String v4 = " end at "; // const-string v4, " end at "
(( java.lang.StringBuilder ) v2 ).append ( v4 ); // invoke-virtual {v2, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
/* iget v4, p0, Lcom/miui/server/stepcounter/StepCounterManagerService;->sCurrentPos:I */
(( java.lang.StringBuilder ) v2 ).append ( v4 ); // invoke-virtual {v2, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v2 ).toString ( ); // invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
android.util.Slog .i ( v3,v2 );
/* .line 234 */
} // :cond_5
return;
} // .end method
private void unregisterDetectorListener ( ) {
/* .locals 2 */
/* .line 310 */
v0 = this.mSensorManager;
if ( v0 != null) { // if-eqz v0, :cond_0
/* .line 311 */
v1 = this.mStepDetectorListener;
(( android.hardware.SensorManager ) v0 ).unregisterListener ( v1 ); // invoke-virtual {v0, v1}, Landroid/hardware/SensorManager;->unregisterListener(Landroid/hardware/SensorEventListener;)V
/* .line 313 */
} // :cond_0
return;
} // .end method
/* # virtual methods */
public void getLatestDataInternal ( Boolean p0 ) {
/* .locals 2 */
/* .param p1, "isForce" # Z */
/* .line 342 */
/* iget-boolean v0, p0, Lcom/miui/server/stepcounter/StepCounterManagerService;->mHaveStepSensor:Z */
if ( v0 != null) { // if-eqz v0, :cond_2
/* .line 343 */
/* sget-boolean v0, Lcom/miui/server/stepcounter/StepCounterManagerService;->sDEBUG:Z */
if ( v0 != null) { // if-eqz v0, :cond_0
/* .line 344 */
/* new-instance v0, Ljava/lang/StringBuilder; */
/* invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V */
final String v1 = "getLatestDataInternal: isForce? "; // const-string v1, "getLatestDataInternal: isForce? "
(( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v0 ).append ( p1 ); // invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v0 ).toString ( ); // invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
final String v1 = "StepCounterManagerService"; // const-string v1, "StepCounterManagerService"
android.util.Slog .i ( v1,v0 );
/* .line 346 */
} // :cond_0
if ( p1 != null) { // if-eqz p1, :cond_1
/* .line 347 */
v0 = this.mHandler;
/* const/16 v1, 0x8 */
(( com.miui.server.stepcounter.StepCounterManagerService$StepCountHandler ) v0 ).sendEmptyMessage ( v1 ); // invoke-virtual {v0, v1}, Lcom/miui/server/stepcounter/StepCounterManagerService$StepCountHandler;->sendEmptyMessage(I)Z
/* .line 349 */
} // :cond_1
v0 = this.mHandler;
int v1 = 4; // const/4 v1, 0x4
(( com.miui.server.stepcounter.StepCounterManagerService$StepCountHandler ) v0 ).sendEmptyMessage ( v1 ); // invoke-virtual {v0, v1}, Lcom/miui/server/stepcounter/StepCounterManagerService$StepCountHandler;->sendEmptyMessage(I)Z
/* .line 351 */
} // :cond_2
} // :goto_0
return;
} // .end method
public Boolean haveStepSensorInternal ( ) {
/* .locals 1 */
/* .line 338 */
/* iget-boolean v0, p0, Lcom/miui/server/stepcounter/StepCounterManagerService;->mHaveStepSensor:Z */
} // .end method
public void onBootPhase ( Integer p0 ) {
/* .locals 0 */
/* .param p1, "phase" # I */
/* .line 143 */
/* invoke-super {p0, p1}, Lcom/android/server/SystemService;->onBootPhase(I)V */
/* .line 144 */
return;
} // .end method
public void onStart ( ) {
/* .locals 5 */
/* .line 125 */
/* const-string/jumbo v0, "support_steps_provider" */
int v1 = 0; // const/4 v1, 0x0
v0 = miui.util.FeatureParser .getBoolean ( v0,v1 );
/* iput-boolean v0, p0, Lcom/miui/server/stepcounter/StepCounterManagerService;->mHaveStepSensor:Z */
/* .line 126 */
if ( v0 != null) { // if-eqz v0, :cond_0
/* .line 127 */
final String v0 = "persist.sys.steps_provider"; // const-string v0, "persist.sys.steps_provider"
int v1 = 1; // const/4 v1, 0x1
v0 = android.os.SystemProperties .getBoolean ( v0,v1 );
/* iput-boolean v0, p0, Lcom/miui/server/stepcounter/StepCounterManagerService;->mHaveStepSensor:Z */
/* .line 129 */
} // :cond_0
/* iget-boolean v0, p0, Lcom/miui/server/stepcounter/StepCounterManagerService;->mHaveStepSensor:Z */
final String v1 = "StepCounterManagerService"; // const-string v1, "StepCounterManagerService"
/* if-nez v0, :cond_1 */
/* .line 130 */
final String v0 = "StepDetector Sensor not support"; // const-string v0, "StepDetector Sensor not support"
android.util.Slog .d ( v1,v0 );
/* .line 131 */
return;
/* .line 133 */
} // :cond_1
/* const-class v0, Lmiui/stepcounter/StepCounterManagerInternal; */
/* new-instance v2, Lcom/miui/server/stepcounter/StepCounterManagerService$LocalService; */
/* invoke-direct {v2, p0}, Lcom/miui/server/stepcounter/StepCounterManagerService$LocalService;-><init>(Lcom/miui/server/stepcounter/StepCounterManagerService;)V */
(( com.miui.server.stepcounter.StepCounterManagerService ) p0 ).publishLocalService ( v0, v2 ); // invoke-virtual {p0, v0, v2}, Lcom/miui/server/stepcounter/StepCounterManagerService;->publishLocalService(Ljava/lang/Class;Ljava/lang/Object;)V
/* .line 134 */
/* new-instance v0, Lcom/miui/server/stepcounter/StepCounterManagerService$BinderService; */
int v2 = 0; // const/4 v2, 0x0
/* invoke-direct {v0, p0, v2}, Lcom/miui/server/stepcounter/StepCounterManagerService$BinderService;-><init>(Lcom/miui/server/stepcounter/StepCounterManagerService;Lcom/miui/server/stepcounter/StepCounterManagerService$BinderService-IA;)V */
final String v2 = "miui_step_counter_service"; // const-string v2, "miui_step_counter_service"
(( com.miui.server.stepcounter.StepCounterManagerService ) p0 ).publishBinderService ( v2, v0 ); // invoke-virtual {p0, v2, v0}, Lcom/miui/server/stepcounter/StepCounterManagerService;->publishBinderService(Ljava/lang/String;Landroid/os/IBinder;)V
/* .line 135 */
v0 = /* invoke-direct {p0}, Lcom/miui/server/stepcounter/StepCounterManagerService;->registerDetectorListener()Z */
/* iput-boolean v0, p0, Lcom/miui/server/stepcounter/StepCounterManagerService;->isRegisterDetectorListener:Z */
/* .line 136 */
/* invoke-direct {p0}, Lcom/miui/server/stepcounter/StepCounterManagerService;->registerReceiver()V */
/* .line 137 */
v0 = this.mHandler;
v2 = this.mRunnable;
/* const-wide/16 v3, 0x2710 */
(( com.miui.server.stepcounter.StepCounterManagerService$StepCountHandler ) v0 ).postDelayed ( v2, v3, v4 ); // invoke-virtual {v0, v2, v3, v4}, Lcom/miui/server/stepcounter/StepCounterManagerService$StepCountHandler;->postDelayed(Ljava/lang/Runnable;J)Z
/* .line 138 */
/* new-instance v0, Ljava/lang/StringBuilder; */
/* invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V */
/* const-string/jumbo v2, "step_debug onStart success isRegisterDetectorListener=" */
(( java.lang.StringBuilder ) v0 ).append ( v2 ); // invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
/* iget-boolean v2, p0, Lcom/miui/server/stepcounter/StepCounterManagerService;->isRegisterDetectorListener:Z */
(( java.lang.StringBuilder ) v0 ).append ( v2 ); // invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v0 ).toString ( ); // invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
android.util.Slog .i ( v1,v0 );
/* .line 139 */
return;
} // .end method
public void trimAll ( Boolean p0 ) {
/* .locals 4 */
/* .param p1, "force" # Z */
/* .line 189 */
java.lang.System .currentTimeMillis ( );
/* move-result-wide v0 */
/* iget-wide v2, p0, Lcom/miui/server/stepcounter/StepCounterManagerService;->mLastTrimAllTimeInMills:J */
/* sub-long/2addr v0, v2 */
/* iget v2, p0, Lcom/miui/server/stepcounter/StepCounterManagerService;->mRecordDelay:I */
/* int-to-long v2, v2 */
/* cmp-long v0, v0, v2 */
int v1 = 0; // const/4 v1, 0x0
/* if-ltz v0, :cond_0 */
int v0 = 1; // const/4 v0, 0x1
} // :cond_0
/* move v0, v1 */
/* .line 190 */
/* .local v0, "pass":Z */
} // :goto_0
if ( v0 != null) { // if-eqz v0, :cond_1
/* .line 191 */
java.lang.System .currentTimeMillis ( );
/* move-result-wide v2 */
/* iput-wide v2, p0, Lcom/miui/server/stepcounter/StepCounterManagerService;->mLastTrimAllTimeInMills:J */
/* .line 193 */
} // :cond_1
/* if-nez p1, :cond_2 */
if ( v0 != null) { // if-eqz v0, :cond_4
} // :cond_2
v2 = this.mContext;
android.os.UserManager .get ( v2 );
v2 = (( android.os.UserManager ) v2 ).isUserUnlocked ( ); // invoke-virtual {v2}, Landroid/os/UserManager;->isUserUnlocked()Z
if ( v2 != null) { // if-eqz v2, :cond_4
/* .line 194 */
} // :goto_1
v2 = /* invoke-direct {p0}, Lcom/miui/server/stepcounter/StepCounterManagerService;->isEmpty()Z */
/* if-nez v2, :cond_3 */
/* .line 195 */
/* invoke-direct {p0}, Lcom/miui/server/stepcounter/StepCounterManagerService;->trim()V */
/* .line 197 */
} // :cond_3
v2 = this.mStepList;
/* .line 198 */
/* iput v1, p0, Lcom/miui/server/stepcounter/StepCounterManagerService;->sCurrentPos:I */
/* .line 200 */
} // :cond_4
return;
} // .end method
