.class final Lcom/miui/server/stepcounter/StepCounterManagerService$StepCountHandler;
.super Landroid/os/Handler;
.source "StepCounterManagerService.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/miui/server/stepcounter/StepCounterManagerService;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x12
    name = "StepCountHandler"
.end annotation


# instance fields
.field final synthetic this$0:Lcom/miui/server/stepcounter/StepCounterManagerService;


# direct methods
.method public constructor <init>(Lcom/miui/server/stepcounter/StepCounterManagerService;Landroid/os/Looper;)V
    .locals 0
    .param p2, "looper"    # Landroid/os/Looper;

    .line 167
    iput-object p1, p0, Lcom/miui/server/stepcounter/StepCounterManagerService$StepCountHandler;->this$0:Lcom/miui/server/stepcounter/StepCounterManagerService;

    .line 168
    invoke-direct {p0, p2}, Landroid/os/Handler;-><init>(Landroid/os/Looper;)V

    .line 169
    return-void
.end method


# virtual methods
.method public handleMessage(Landroid/os/Message;)V
    .locals 2
    .param p1, "msg"    # Landroid/os/Message;

    .line 173
    iget v0, p1, Landroid/os/Message;->what:I

    sparse-switch v0, :sswitch_data_0

    goto :goto_0

    .line 179
    :sswitch_0
    iget-object v0, p0, Lcom/miui/server/stepcounter/StepCounterManagerService$StepCountHandler;->this$0:Lcom/miui/server/stepcounter/StepCounterManagerService;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/miui/server/stepcounter/StepCounterManagerService;->trimAll(Z)V

    .line 180
    goto :goto_0

    .line 176
    :sswitch_1
    iget-object v0, p0, Lcom/miui/server/stepcounter/StepCounterManagerService$StepCountHandler;->this$0:Lcom/miui/server/stepcounter/StepCounterManagerService;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Lcom/miui/server/stepcounter/StepCounterManagerService;->trimAll(Z)V

    .line 177
    nop

    .line 184
    :goto_0
    return-void

    nop

    :sswitch_data_0
    .sparse-switch
        0x2 -> :sswitch_1
        0x4 -> :sswitch_0
        0x8 -> :sswitch_1
    .end sparse-switch
.end method
