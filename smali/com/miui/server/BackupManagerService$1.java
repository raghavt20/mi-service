class com.miui.server.BackupManagerService$1 extends com.android.internal.content.PackageMonitor {
	 /* .source "BackupManagerService.java" */
	 /* # annotations */
	 /* .annotation system Ldalvik/annotation/EnclosingClass; */
	 /* value = Lcom/miui/server/BackupManagerService; */
} // .end annotation
/* .annotation system Ldalvik/annotation/InnerClass; */
/* accessFlags = 0x0 */
/* name = null */
} // .end annotation
/* # instance fields */
final com.miui.server.BackupManagerService this$0; //synthetic
/* # direct methods */
 com.miui.server.BackupManagerService$1 ( ) {
/* .locals 0 */
/* .param p1, "this$0" # Lcom/miui/server/BackupManagerService; */
/* .line 113 */
this.this$0 = p1;
/* invoke-direct {p0}, Lcom/android/internal/content/PackageMonitor;-><init>()V */
return;
} // .end method
/* # virtual methods */
public Boolean onPackageChanged ( java.lang.String p0, Integer p1, java.lang.String[] p2 ) {
/* .locals 3 */
/* .param p1, "packageName" # Ljava/lang/String; */
/* .param p2, "uid" # I */
/* .param p3, "components" # [Ljava/lang/String; */
/* .line 116 */
if ( p1 != null) { // if-eqz p1, :cond_1
	 v0 = this.this$0;
	 com.miui.server.BackupManagerService .-$$Nest$fgetmCurrentWorkingPkg ( v0 );
	 v0 = 	 (( java.lang.String ) p1 ).equals ( v0 ); // invoke-virtual {p1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z
	 if ( v0 != null) { // if-eqz v0, :cond_1
		 /* .line 117 */
		 v0 = this.this$0;
		 com.miui.server.BackupManagerService .-$$Nest$fgetmPkgChangingLock ( v0 );
		 /* monitor-enter v0 */
		 /* .line 118 */
		 try { // :try_start_0
			 v1 = this.this$0;
			 com.miui.server.BackupManagerService .-$$Nest$fgetmPkgChangingLock ( v1 );
			 v1 = 			 (( java.util.concurrent.atomic.AtomicBoolean ) v1 ).get ( ); // invoke-virtual {v1}, Ljava/util/concurrent/atomic/AtomicBoolean;->get()Z
			 if ( v1 != null) { // if-eqz v1, :cond_0
				 /* .line 119 */
				 v1 = this.this$0;
				 com.miui.server.BackupManagerService .-$$Nest$fgetmPkgChangingLock ( v1 );
				 int v2 = 0; // const/4 v2, 0x0
				 (( java.util.concurrent.atomic.AtomicBoolean ) v1 ).set ( v2 ); // invoke-virtual {v1, v2}, Ljava/util/concurrent/atomic/AtomicBoolean;->set(Z)V
				 /* .line 120 */
				 v1 = this.this$0;
				 com.miui.server.BackupManagerService .-$$Nest$fgetmPkgChangingLock ( v1 );
				 (( java.lang.Object ) v1 ).notify ( ); // invoke-virtual {v1}, Ljava/lang/Object;->notify()V
				 /* .line 122 */
			 } // :cond_0
			 /* monitor-exit v0 */
			 /* :catchall_0 */
			 /* move-exception v1 */
			 /* monitor-exit v0 */
			 /* :try_end_0 */
			 /* .catchall {:try_start_0 ..:try_end_0} :catchall_0 */
			 /* throw v1 */
			 /* .line 124 */
		 } // :cond_1
	 } // :goto_0
	 int v0 = 1; // const/4 v0, 0x1
} // .end method
public void onPackagesSuspended ( java.lang.String[] p0 ) {
	 /* .locals 6 */
	 /* .param p1, "packages" # [Ljava/lang/String; */
	 /* .line 129 */
	 /* invoke-super {p0, p1}, Lcom/android/internal/content/PackageMonitor;->onPackagesSuspended([Ljava/lang/String;)V */
	 /* .line 130 */
	 if ( p1 != null) { // if-eqz p1, :cond_2
		 /* .line 131 */
		 /* array-length v0, p1 */
		 int v1 = 0; // const/4 v1, 0x0
		 /* move v2, v1 */
	 } // :goto_0
	 /* if-ge v2, v0, :cond_2 */
	 /* aget-object v3, p1, v2 */
	 /* .line 132 */
	 /* .local v3, "name":Ljava/lang/String; */
	 v4 = this.this$0;
	 com.miui.server.BackupManagerService .-$$Nest$fgetmCurrentWorkingPkg ( v4 );
	 v4 = 	 (( java.lang.String ) v3 ).equals ( v4 ); // invoke-virtual {v3, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z
	 if ( v4 != null) { // if-eqz v4, :cond_1
		 /* .line 133 */
		 v4 = this.this$0;
		 com.miui.server.BackupManagerService .-$$Nest$fgetmPkgChangingLock ( v4 );
		 /* monitor-enter v4 */
		 /* .line 134 */
		 try { // :try_start_0
			 v5 = this.this$0;
			 com.miui.server.BackupManagerService .-$$Nest$fgetmPkgChangingLock ( v5 );
			 v5 = 			 (( java.util.concurrent.atomic.AtomicBoolean ) v5 ).get ( ); // invoke-virtual {v5}, Ljava/util/concurrent/atomic/AtomicBoolean;->get()Z
			 if ( v5 != null) { // if-eqz v5, :cond_0
				 /* .line 135 */
				 v5 = this.this$0;
				 com.miui.server.BackupManagerService .-$$Nest$fgetmPkgChangingLock ( v5 );
				 (( java.util.concurrent.atomic.AtomicBoolean ) v5 ).set ( v1 ); // invoke-virtual {v5, v1}, Ljava/util/concurrent/atomic/AtomicBoolean;->set(Z)V
				 /* .line 136 */
				 v5 = this.this$0;
				 com.miui.server.BackupManagerService .-$$Nest$fgetmPkgChangingLock ( v5 );
				 (( java.lang.Object ) v5 ).notify ( ); // invoke-virtual {v5}, Ljava/lang/Object;->notify()V
				 /* .line 138 */
			 } // :cond_0
			 /* monitor-exit v4 */
			 /* :catchall_0 */
			 /* move-exception v0 */
			 /* monitor-exit v4 */
			 /* :try_end_0 */
			 /* .catchall {:try_start_0 ..:try_end_0} :catchall_0 */
			 /* throw v0 */
			 /* .line 131 */
		 } // .end local v3 # "name":Ljava/lang/String;
	 } // :cond_1
} // :goto_1
/* add-int/lit8 v2, v2, 0x1 */
/* .line 142 */
} // :cond_2
return;
} // .end method
