.class public Lcom/miui/server/turbosched/FrameTurboAction;
.super Ljava/lang/Object;
.source "FrameTurboAction.java"


# static fields
.field private static final TAG:Ljava/lang/String; = "TurboSched_FrameTurbo"


# instance fields
.field private mActiveSceneWaitListMap:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map<",
            "Ljava/lang/String;",
            "Ljava/util/LinkedList<",
            "Ljava/lang/String;",
            ">;>;"
        }
    .end annotation
.end field

.field private mCurrentTurboScene:Ljava/lang/String;

.field protected mDebug:Z

.field protected mFrameTurboAppMap:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map<",
            "Ljava/lang/String;",
            "Ljava/util/List<",
            "Ljava/lang/String;",
            ">;>;"
        }
    .end annotation
.end field

.field protected mFrameTurboSceneCallbacks:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List<",
            "Lmiui/turbosched/ITurboSchedManager$IFrameTurboSceneCallback;",
            ">;"
        }
    .end annotation
.end field

.field protected mFrameTurboSceneWhiteListMap:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map<",
            "Ljava/lang/String;",
            "Ljava/util/List<",
            "Ljava/lang/String;",
            ">;>;"
        }
    .end annotation
.end field

.field private mHistoryLog:Landroid/util/LocalLog;


# direct methods
.method protected constructor <init>()V
    .locals 2

    .line 35
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 26
    new-instance v0, Landroid/util/LocalLog;

    const/16 v1, 0x200

    invoke-direct {v0, v1}, Landroid/util/LocalLog;-><init>(I)V

    iput-object v0, p0, Lcom/miui/server/turbosched/FrameTurboAction;->mHistoryLog:Landroid/util/LocalLog;

    .line 27
    const-string v0, ""

    iput-object v0, p0, Lcom/miui/server/turbosched/FrameTurboAction;->mCurrentTurboScene:Ljava/lang/String;

    .line 28
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    iput-object v0, p0, Lcom/miui/server/turbosched/FrameTurboAction;->mActiveSceneWaitListMap:Ljava/util/Map;

    .line 32
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/miui/server/turbosched/FrameTurboAction;->mDebug:Z

    .line 33
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/miui/server/turbosched/FrameTurboAction;->mFrameTurboSceneCallbacks:Ljava/util/List;

    .line 36
    invoke-direct {p0}, Lcom/miui/server/turbosched/FrameTurboAction;->initFrameTurboAppMap()Ljava/util/Map;

    move-result-object v0

    iput-object v0, p0, Lcom/miui/server/turbosched/FrameTurboAction;->mFrameTurboAppMap:Ljava/util/Map;

    .line 37
    invoke-direct {p0}, Lcom/miui/server/turbosched/FrameTurboAction;->initFrameTurboSceneWhiteListMap()Ljava/util/Map;

    move-result-object v0

    iput-object v0, p0, Lcom/miui/server/turbosched/FrameTurboAction;->mFrameTurboSceneWhiteListMap:Ljava/util/Map;

    .line 38
    return-void
.end method

.method private checkPermission(Ljava/lang/String;Ljava/lang/String;)I
    .locals 5
    .param p1, "packageName"    # Ljava/lang/String;
    .param p2, "sceneId"    # Ljava/lang/String;

    .line 308
    const-string v0, "TurboSched_FrameTurbo"

    const/4 v1, -0x4

    const-string v2, ",sceneId: "

    const-string v3, "TS [MARK] : failed, R: not in white list, packageName: "

    if-eqz p1, :cond_4

    invoke-virtual {p1}, Ljava/lang/String;->length()I

    move-result v4

    if-eqz v4, :cond_4

    iget-object v4, p0, Lcom/miui/server/turbosched/FrameTurboAction;->mFrameTurboSceneWhiteListMap:Ljava/util/Map;

    invoke-interface {v4, p1}, Ljava/util/Map;->containsKey(Ljava/lang/Object;)Z

    move-result v4

    if-nez v4, :cond_0

    goto :goto_1

    .line 316
    :cond_0
    if-eqz p2, :cond_2

    invoke-virtual {p2}, Ljava/lang/String;->length()I

    move-result v4

    if-eqz v4, :cond_2

    iget-object v4, p0, Lcom/miui/server/turbosched/FrameTurboAction;->mFrameTurboSceneWhiteListMap:Ljava/util/Map;

    invoke-interface {v4, p1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v4

    if-eqz v4, :cond_2

    iget-object v4, p0, Lcom/miui/server/turbosched/FrameTurboAction;->mFrameTurboSceneWhiteListMap:Ljava/util/Map;

    invoke-interface {v4, p1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Ljava/util/List;

    invoke-interface {v4, p2}, Ljava/util/List;->contains(Ljava/lang/Object;)Z

    move-result v4

    if-nez v4, :cond_1

    goto :goto_0

    .line 323
    :cond_1
    const/4 v0, 0x0

    return v0

    .line 317
    :cond_2
    :goto_0
    iget-boolean v4, p0, Lcom/miui/server/turbosched/FrameTurboAction;->mDebug:Z

    if-eqz v4, :cond_3

    .line 318
    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v4, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v0, v4}, Landroid/util/Slog;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 320
    :cond_3
    iget-object v0, p0, Lcom/miui/server/turbosched/FrameTurboAction;->mHistoryLog:Landroid/util/LocalLog;

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v4, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v2}, Landroid/util/LocalLog;->log(Ljava/lang/String;)V

    .line 321
    return v1

    .line 309
    :cond_4
    :goto_1
    iget-boolean v4, p0, Lcom/miui/server/turbosched/FrameTurboAction;->mDebug:Z

    if-eqz v4, :cond_5

    .line 310
    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v4, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v0, v4}, Landroid/util/Slog;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 312
    :cond_5
    iget-object v0, p0, Lcom/miui/server/turbosched/FrameTurboAction;->mHistoryLog:Landroid/util/LocalLog;

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v4, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v2}, Landroid/util/LocalLog;->log(Ljava/lang/String;)V

    .line 313
    return v1
.end method

.method private checkSceneFormat(Ljava/lang/String;)Z
    .locals 9
    .param p1, "scene"    # Ljava/lang/String;

    .line 205
    const-string v0, ","

    invoke-virtual {p1, v0}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v0

    .line 206
    .local v0, "appScenes":[Ljava/lang/String;
    array-length v1, v0

    const/4 v2, 0x0

    move v3, v2

    :goto_0
    if-ge v3, v1, :cond_2

    aget-object v4, v0, v3

    .line 207
    .local v4, "appScene":Ljava/lang/String;
    const-string v5, ":"

    invoke-virtual {v4, v5}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v5

    .line 208
    .local v5, "appSceneConfig":[Ljava/lang/String;
    array-length v6, v5

    const/4 v7, 0x2

    if-ge v6, v7, :cond_0

    .line 209
    return v2

    .line 211
    :cond_0
    aget-object v6, v5, v2

    const-string v8, "#"

    invoke-virtual {v6, v8}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v6

    .line 212
    .local v6, "scenes":[Ljava/lang/String;
    array-length v8, v6

    if-ge v8, v7, :cond_1

    .line 213
    return v2

    .line 206
    .end local v4    # "appScene":Ljava/lang/String;
    .end local v5    # "appSceneConfig":[Ljava/lang/String;
    .end local v6    # "scenes":[Ljava/lang/String;
    :cond_1
    add-int/lit8 v3, v3, 0x1

    goto :goto_0

    .line 216
    :cond_2
    const/4 v1, 0x1

    return v1
.end method

.method private initFrameTurboAppMap()Ljava/util/Map;
    .locals 14
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/Map<",
            "Ljava/lang/String;",
            "Ljava/util/List<",
            "Ljava/lang/String;",
            ">;>;"
        }
    .end annotation

    .line 41
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    .line 42
    .local v0, "appMap":Ljava/util/Map;, "Ljava/util/Map<Ljava/lang/String;Ljava/util/List<Ljava/lang/String;>;>;"
    const-string v1, "persist.sys.turbosched.frame_turbo.apps"

    const-string v2, ""

    invoke-static {v1, v2}, Landroid/os/SystemProperties;->get(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 43
    .local v1, "appListStr":Ljava/lang/String;
    const-string v2, "TurboSched_FrameTurbo"

    if-eqz v1, :cond_1

    invoke-virtual {v1}, Ljava/lang/String;->isEmpty()Z

    move-result v3

    if-nez v3, :cond_1

    .line 44
    const-string v3, ","

    invoke-virtual {v1, v3}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v3

    .line 45
    .local v3, "appList":[Ljava/lang/String;
    array-length v4, v3

    const/4 v5, 0x0

    move v6, v5

    :goto_0
    if-ge v6, v4, :cond_1

    aget-object v7, v3, v6

    .line 46
    .local v7, "app":Ljava/lang/String;
    const-string v8, ":"

    invoke-virtual {v7, v8}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v8

    .line 47
    .local v8, "appConfig":[Ljava/lang/String;
    new-instance v9, Ljava/util/ArrayList;

    invoke-direct {v9}, Ljava/util/ArrayList;-><init>()V

    .line 48
    .local v9, "sceneList":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;"
    array-length v10, v8

    const/4 v11, 0x1

    if-le v10, v11, :cond_0

    .line 49
    aget-object v10, v8, v11

    const-string v11, "\\|"

    invoke-virtual {v10, v11}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v10

    .line 50
    .local v10, "scenes":[Ljava/lang/String;
    new-instance v11, Ljava/lang/StringBuilder;

    invoke-direct {v11}, Ljava/lang/StringBuilder;-><init>()V

    const-string v12, "scenes: "

    invoke-virtual {v11, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v11

    aget-object v12, v10, v5

    invoke-virtual {v11, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v11

    const-string v12, ", size = "

    invoke-virtual {v11, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v11

    array-length v12, v10

    invoke-virtual {v11, v12}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v11

    invoke-virtual {v11}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v11

    invoke-static {v2, v11}, Landroid/util/Slog;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 51
    array-length v11, v10

    move v12, v5

    :goto_1
    if-ge v12, v11, :cond_0

    aget-object v13, v10, v12

    .line 52
    .local v13, "scene":Ljava/lang/String;
    invoke-interface {v9, v13}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 51
    .end local v13    # "scene":Ljava/lang/String;
    add-int/lit8 v12, v12, 0x1

    goto :goto_1

    .line 55
    .end local v10    # "scenes":[Ljava/lang/String;
    :cond_0
    aget-object v10, v8, v5

    invoke-interface {v0, v10, v9}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 45
    .end local v7    # "app":Ljava/lang/String;
    .end local v8    # "appConfig":[Ljava/lang/String;
    .end local v9    # "sceneList":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;"
    add-int/lit8 v6, v6, 0x1

    goto :goto_0

    .line 58
    .end local v3    # "appList":[Ljava/lang/String;
    :cond_1
    iget-boolean v3, p0, Lcom/miui/server/turbosched/FrameTurboAction;->mDebug:Z

    if-eqz v3, :cond_2

    .line 59
    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "frame turbo init success, appMap: "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v0}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Landroid/util/Slog;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 61
    :cond_2
    return-object v0
.end method

.method private initFrameTurboSceneWhiteListMap()Ljava/util/Map;
    .locals 13
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/Map<",
            "Ljava/lang/String;",
            "Ljava/util/List<",
            "Ljava/lang/String;",
            ">;>;"
        }
    .end annotation

    .line 65
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    .line 66
    .local v0, "sceneMap":Ljava/util/Map;, "Ljava/util/Map<Ljava/lang/String;Ljava/util/List<Ljava/lang/String;>;>;"
    const-string v1, "persist.sys.turbosched.frame_turbo.scene_white_list"

    const-string v2, ""

    invoke-static {v1, v2}, Landroid/os/SystemProperties;->get(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 67
    .local v1, "sceneWhiteListStr":Ljava/lang/String;
    if-eqz v1, :cond_2

    invoke-virtual {v1}, Ljava/lang/String;->isEmpty()Z

    move-result v2

    if-nez v2, :cond_2

    .line 68
    const-string v2, ","

    invoke-virtual {v1, v2}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v2

    .line 69
    .local v2, "sceneWhiteList":[Ljava/lang/String;
    array-length v3, v2

    const/4 v4, 0x0

    move v5, v4

    :goto_0
    if-ge v5, v3, :cond_2

    aget-object v6, v2, v5

    .line 70
    .local v6, "scene":Ljava/lang/String;
    const-string v7, ":"

    invoke-virtual {v6, v7}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v7

    .line 71
    .local v7, "sceneConfig":[Ljava/lang/String;
    new-instance v8, Ljava/util/ArrayList;

    invoke-direct {v8}, Ljava/util/ArrayList;-><init>()V

    .line 72
    .local v8, "sceneList":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;"
    array-length v9, v7

    const/4 v10, 0x1

    if-le v9, v10, :cond_1

    .line 73
    aget-object v9, v7, v10

    const-string v10, "\\|"

    invoke-virtual {v9, v10}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v9

    .line 74
    .local v9, "scenes":[Ljava/lang/String;
    array-length v10, v9

    move v11, v4

    :goto_1
    if-ge v11, v10, :cond_0

    aget-object v12, v9, v11

    .line 75
    .local v12, "s":Ljava/lang/String;
    invoke-interface {v8, v12}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 74
    .end local v12    # "s":Ljava/lang/String;
    add-int/lit8 v11, v11, 0x1

    goto :goto_1

    .line 77
    :cond_0
    aget-object v10, v7, v4

    invoke-interface {v0, v10, v8}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 69
    .end local v6    # "scene":Ljava/lang/String;
    .end local v7    # "sceneConfig":[Ljava/lang/String;
    .end local v8    # "sceneList":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;"
    .end local v9    # "scenes":[Ljava/lang/String;
    :cond_1
    add-int/lit8 v5, v5, 0x1

    goto :goto_0

    .line 81
    .end local v2    # "sceneWhiteList":[Ljava/lang/String;
    :cond_2
    return-object v0
.end method

.method private parseCommandArgs(Ljava/io/FileDescriptor;Ljava/io/PrintWriter;[Ljava/lang/String;)Z
    .locals 4
    .param p1, "fd"    # Ljava/io/FileDescriptor;
    .param p2, "pw"    # Ljava/io/PrintWriter;
    .param p3, "args"    # [Ljava/lang/String;

    .line 85
    const/4 v0, 0x0

    .line 86
    .local v0, "result":Z
    array-length v1, p3

    const/4 v2, 0x2

    if-ge v1, v2, :cond_0

    .line 87
    return v0

    .line 89
    :cond_0
    const/4 v1, 0x1

    aget-object v1, p3, v1

    .line 90
    .local v1, "command":Ljava/lang/String;
    array-length v3, p3

    if-ne v3, v2, :cond_1

    .line 91
    invoke-direct {p0, p1, p2, v1}, Lcom/miui/server/turbosched/FrameTurboAction;->parseCommandWithoutArgs(Ljava/io/FileDescriptor;Ljava/io/PrintWriter;Ljava/lang/String;)Z

    move-result v0

    goto :goto_0

    .line 93
    :cond_1
    invoke-direct {p0, p1, p2, v1, p3}, Lcom/miui/server/turbosched/FrameTurboAction;->parseCommandWithArgs(Ljava/io/FileDescriptor;Ljava/io/PrintWriter;Ljava/lang/String;[Ljava/lang/String;)Z

    move-result v0

    .line 95
    :goto_0
    return v0
.end method

.method private parseCommandWithArgs(Ljava/io/FileDescriptor;Ljava/io/PrintWriter;Ljava/lang/String;[Ljava/lang/String;)Z
    .locals 2
    .param p1, "fd"    # Ljava/io/FileDescriptor;
    .param p2, "pw"    # Ljava/io/PrintWriter;
    .param p3, "command"    # Ljava/lang/String;
    .param p4, "args"    # [Ljava/lang/String;

    .line 113
    const/4 v0, 0x0

    .line 115
    .local v0, "result":Z
    invoke-virtual {p3}, Ljava/lang/String;->hashCode()I

    move-result v1

    sparse-switch v1, :sswitch_data_0

    :cond_0
    goto :goto_0

    :sswitch_0
    const-string v1, "scene"

    invoke-virtual {p3, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    const/4 v1, 0x2

    goto :goto_1

    :sswitch_1
    const-string v1, "debug"

    invoke-virtual {p3, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    const/4 v1, 0x1

    goto :goto_1

    :sswitch_2
    const-string v1, "enable"

    invoke-virtual {p3, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    const/4 v1, 0x0

    goto :goto_1

    :goto_0
    const/4 v1, -0x1

    :goto_1
    packed-switch v1, :pswitch_data_0

    goto :goto_2

    .line 123
    :pswitch_0
    invoke-direct {p0, p4, p2}, Lcom/miui/server/turbosched/FrameTurboAction;->parseSceneCommand([Ljava/lang/String;Ljava/io/PrintWriter;)Z

    move-result v0

    goto :goto_2

    .line 120
    :pswitch_1
    invoke-direct {p0, p4, p2}, Lcom/miui/server/turbosched/FrameTurboAction;->parseDebugCommand([Ljava/lang/String;Ljava/io/PrintWriter;)Z

    move-result v0

    .line 121
    goto :goto_2

    .line 117
    :pswitch_2
    invoke-direct {p0, p4, p2}, Lcom/miui/server/turbosched/FrameTurboAction;->parseEnableCommand([Ljava/lang/String;Ljava/io/PrintWriter;)Z

    move-result v0

    .line 118
    nop

    .line 126
    :goto_2
    return v0

    :sswitch_data_0
    .sparse-switch
        -0x4d6ada7d -> :sswitch_2
        0x5b09653 -> :sswitch_1
        0x683188c -> :sswitch_0
    .end sparse-switch

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_2
        :pswitch_1
        :pswitch_0
    .end packed-switch
.end method

.method private parseCommandWithoutArgs(Ljava/io/FileDescriptor;Ljava/io/PrintWriter;Ljava/lang/String;)Z
    .locals 3
    .param p1, "fd"    # Ljava/io/FileDescriptor;
    .param p2, "pw"    # Ljava/io/PrintWriter;
    .param p3, "command"    # Ljava/lang/String;

    .line 99
    const/4 v0, 0x0

    .line 100
    .local v0, "result":Z
    invoke-virtual {p3}, Ljava/lang/String;->hashCode()I

    move-result v1

    const/4 v2, 0x0

    packed-switch v1, :pswitch_data_0

    :cond_0
    goto :goto_0

    :pswitch_0
    const-string v1, "history"

    invoke-virtual {p3, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    move v1, v2

    goto :goto_1

    :goto_0
    const/4 v1, -0x1

    :goto_1
    packed-switch v1, :pswitch_data_1

    .line 106
    const/4 v0, 0x0

    goto :goto_2

    .line 102
    :pswitch_1
    iget-object v1, p0, Lcom/miui/server/turbosched/FrameTurboAction;->mHistoryLog:Landroid/util/LocalLog;

    new-array v2, v2, [Ljava/lang/String;

    invoke-virtual {v1, p1, p2, v2}, Landroid/util/LocalLog;->dump(Ljava/io/FileDescriptor;Ljava/io/PrintWriter;[Ljava/lang/String;)V

    .line 103
    const/4 v0, 0x1

    .line 104
    nop

    .line 109
    :goto_2
    return v0

    :pswitch_data_0
    .packed-switch 0x373fe494
        :pswitch_0
    .end packed-switch

    :pswitch_data_1
    .packed-switch 0x0
        :pswitch_1
    .end packed-switch
.end method

.method private parseDebugCommand([Ljava/lang/String;Ljava/io/PrintWriter;)Z
    .locals 3
    .param p1, "args"    # [Ljava/lang/String;
    .param p2, "pw"    # Ljava/io/PrintWriter;

    .line 151
    const/4 v0, 0x2

    aget-object v0, p1, v0

    const-string v1, "0"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    const/4 v1, 0x1

    xor-int/2addr v0, v1

    iput-boolean v0, p0, Lcom/miui/server/turbosched/FrameTurboAction;->mDebug:Z

    .line 152
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v2, "set debug: "

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-boolean v2, p0, Lcom/miui/server/turbosched/FrameTurboAction;->mDebug:Z

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p2, v0}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V

    .line 153
    return v1
.end method

.method private parseEnableCommand([Ljava/lang/String;Ljava/io/PrintWriter;)Z
    .locals 5
    .param p1, "args"    # [Ljava/lang/String;
    .param p2, "pw"    # Ljava/io/PrintWriter;

    .line 131
    const/4 v0, 0x2

    aget-object v0, p1, v0

    const-string v1, "0"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    const/4 v1, 0x1

    xor-int/2addr v0, v1

    .line 132
    .local v0, "enable":Z
    invoke-static {}, Lcom/miui/server/turbosched/TurboSchedConfig;->getPolicyList()Ljava/util/List;

    move-result-object v2

    .line 133
    .local v2, "policyList":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;"
    const-string v3, "frame_turbo"

    if-eqz v0, :cond_1

    .line 134
    invoke-interface {v2, v3}, Ljava/util/List;->contains(Ljava/lang/Object;)Z

    move-result v4

    if-nez v4, :cond_0

    .line 135
    invoke-interface {v2, v3}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 136
    invoke-static {v2}, Lcom/miui/server/turbosched/TurboSchedConfig;->setPolicyList(Ljava/util/List;)V

    .line 138
    :cond_0
    const-string v3, "enable frame turbo success"

    invoke-virtual {p2, v3}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V

    goto :goto_0

    .line 140
    :cond_1
    invoke-interface {v2, v3}, Ljava/util/List;->contains(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_2

    .line 141
    invoke-interface {v2, v3}, Ljava/util/List;->remove(Ljava/lang/Object;)Z

    .line 142
    invoke-static {v2}, Lcom/miui/server/turbosched/TurboSchedConfig;->setPolicyList(Ljava/util/List;)V

    .line 144
    :cond_2
    const-string v3, "disable frame turbo success"

    invoke-virtual {p2, v3}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V

    .line 146
    :goto_0
    return v1
.end method

.method private parseSceneCommand([Ljava/lang/String;Ljava/io/PrintWriter;)Z
    .locals 11
    .param p1, "args"    # [Ljava/lang/String;
    .param p2, "pw"    # Ljava/io/PrintWriter;

    .line 157
    array-length v0, p1

    const/4 v1, 0x0

    const/4 v2, 0x4

    if-gt v0, v2, :cond_0

    .line 158
    return v1

    .line 160
    :cond_0
    const/4 v0, 0x0

    .line 161
    .local v0, "result":Z
    const/4 v3, 0x3

    aget-object v4, p1, v3

    .line 162
    .local v4, "scene":Ljava/lang/String;
    invoke-direct {p0, v4}, Lcom/miui/server/turbosched/FrameTurboAction;->checkSceneFormat(Ljava/lang/String;)Z

    move-result v5

    const/4 v6, 0x1

    if-nez v5, :cond_1

    .line 163
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "scene format has error, scene: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p2, v1}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V

    .line 164
    return v6

    .line 166
    :cond_1
    new-instance v5, Ljava/util/ArrayList;

    invoke-direct {v5}, Ljava/util/ArrayList;-><init>()V

    .line 167
    .local v5, "sceneList":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;"
    const-string v7, ","

    invoke-virtual {v4, v7}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v7

    .line 168
    .local v7, "appScenes":[Ljava/lang/String;
    array-length v8, v7

    move v9, v1

    :goto_0
    if-ge v9, v8, :cond_2

    aget-object v10, v7, v9

    .line 169
    .local v10, "appScene":Ljava/lang/String;
    invoke-interface {v5, v10}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 168
    .end local v10    # "appScene":Ljava/lang/String;
    add-int/lit8 v9, v9, 0x1

    goto :goto_0

    .line 171
    :cond_2
    const/4 v8, 0x2

    aget-object v9, p1, v8

    .line 172
    .local v9, "childCommand":Ljava/lang/String;
    aget-object v2, p1, v2

    const-string v10, "0"

    invoke-virtual {v2, v10}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    xor-int/2addr v2, v6

    .line 173
    .local v2, "enable":Z
    invoke-virtual {v9}, Ljava/lang/String;->hashCode()I

    move-result v10

    sparse-switch v10, :sswitch_data_0

    :cond_3
    goto :goto_1

    :sswitch_0
    const-string/jumbo v1, "test"

    invoke-virtual {v9, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_3

    move v1, v3

    goto :goto_2

    :sswitch_1
    const-string v1, "mark"

    invoke-virtual {v9, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_3

    move v1, v8

    goto :goto_2

    :sswitch_2
    const-string/jumbo v1, "trigger"

    invoke-virtual {v9, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_3

    move v1, v6

    goto :goto_2

    :sswitch_3
    const-string/jumbo v3, "whitelist"

    invoke-virtual {v9, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_3

    goto :goto_2

    :goto_1
    const/4 v1, -0x1

    :goto_2
    const-string v3, ": "

    packed-switch v1, :pswitch_data_0

    .line 197
    const/4 v0, 0x0

    goto/16 :goto_5

    .line 194
    :pswitch_0
    invoke-direct {p0, v2, v5}, Lcom/miui/server/turbosched/FrameTurboAction;->testScene(ZLjava/util/List;)Z

    move-result v0

    .line 195
    goto/16 :goto_5

    .line 191
    :pswitch_1
    invoke-direct {p0, v2, v5}, Lcom/miui/server/turbosched/FrameTurboAction;->updateMarkScene(ZLjava/util/List;)Z

    move-result v0

    .line 192
    goto/16 :goto_5

    .line 183
    :pswitch_2
    invoke-direct {p0, v2, v5}, Lcom/miui/server/turbosched/FrameTurboAction;->updateTriggerList(ZLjava/util/List;)V

    .line 184
    const/4 v0, 0x1

    .line 185
    const-string v1, "frame turbo list: "

    invoke-virtual {p2, v1}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V

    .line 186
    iget-object v1, p0, Lcom/miui/server/turbosched/FrameTurboAction;->mFrameTurboAppMap:Ljava/util/Map;

    invoke-interface {v1}, Ljava/util/Map;->keySet()Ljava/util/Set;

    move-result-object v1

    invoke-interface {v1}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_3
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v6

    if-eqz v6, :cond_4

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v6

    check-cast v6, Ljava/lang/String;

    .line 187
    .local v6, "key":Ljava/lang/String;
    new-instance v8, Ljava/lang/StringBuilder;

    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v8, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    iget-object v10, p0, Lcom/miui/server/turbosched/FrameTurboAction;->mFrameTurboAppMap:Ljava/util/Map;

    invoke-interface {v10, v6}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v10

    invoke-virtual {v8, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    invoke-virtual {p2, v8}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V

    .line 188
    .end local v6    # "key":Ljava/lang/String;
    goto :goto_3

    .line 189
    :cond_4
    goto :goto_5

    .line 175
    :pswitch_3
    invoke-direct {p0, v2, v5}, Lcom/miui/server/turbosched/FrameTurboAction;->updateSceneWhiteList(ZLjava/util/List;)V

    .line 176
    const/4 v0, 0x1

    .line 177
    const-string v1, "scene white list now is: "

    invoke-virtual {p2, v1}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V

    .line 178
    iget-object v1, p0, Lcom/miui/server/turbosched/FrameTurboAction;->mFrameTurboSceneWhiteListMap:Ljava/util/Map;

    invoke-interface {v1}, Ljava/util/Map;->entrySet()Ljava/util/Set;

    move-result-object v1

    invoke-interface {v1}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_4
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v6

    if-eqz v6, :cond_5

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v6

    check-cast v6, Ljava/util/Map$Entry;

    .line 179
    .local v6, "entry":Ljava/util/Map$Entry;, "Ljava/util/Map$Entry<Ljava/lang/String;Ljava/util/List<Ljava/lang/String;>;>;"
    new-instance v8, Ljava/lang/StringBuilder;

    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    invoke-interface {v6}, Ljava/util/Map$Entry;->getKey()Ljava/lang/Object;

    move-result-object v10

    check-cast v10, Ljava/lang/String;

    invoke-virtual {v8, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-interface {v6}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v10

    check-cast v10, Ljava/util/List;

    invoke-virtual {v10}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v10

    invoke-virtual {v8, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    invoke-virtual {p2, v8}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V

    .line 180
    .end local v6    # "entry":Ljava/util/Map$Entry;, "Ljava/util/Map$Entry<Ljava/lang/String;Ljava/util/List<Ljava/lang/String;>;>;"
    goto :goto_4

    .line 181
    :cond_5
    nop

    .line 200
    :goto_5
    return v0

    nop

    :sswitch_data_0
    .sparse-switch
        -0x6293bfb9 -> :sswitch_3
        -0x3f2caa48 -> :sswitch_2
        0x3306cd -> :sswitch_1
        0x364492 -> :sswitch_0
    .end sparse-switch

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_3
        :pswitch_2
        :pswitch_1
        :pswitch_0
    .end packed-switch
.end method

.method private printCommandResult(Ljava/io/PrintWriter;Ljava/lang/String;)V
    .locals 1
    .param p1, "pw"    # Ljava/io/PrintWriter;
    .param p2, "result"    # Ljava/lang/String;

    .line 301
    const-string v0, "--------------------command result-----------------------"

    invoke-virtual {p1, v0}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V

    .line 302
    invoke-virtual {p1, p2}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V

    .line 303
    return-void
.end method

.method private testScene(ZLjava/util/List;)Z
    .locals 2
    .param p1, "start"    # Z
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(Z",
            "Ljava/util/List<",
            "Ljava/lang/String;",
            ">;)Z"
        }
    .end annotation

    .line 220
    .local p2, "sceneList":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;"
    invoke-interface {p2}, Ljava/util/List;->size()I

    move-result v0

    const/4 v1, 0x1

    if-le v0, v1, :cond_0

    .line 221
    const/4 v0, 0x0

    return v0

    .line 224
    :cond_0
    invoke-direct {p0, p1, p2}, Lcom/miui/server/turbosched/FrameTurboAction;->updateSceneWhiteList(ZLjava/util/List;)V

    .line 227
    invoke-direct {p0, p1, p2}, Lcom/miui/server/turbosched/FrameTurboAction;->updateTriggerList(ZLjava/util/List;)V

    .line 230
    invoke-direct {p0, p1, p2}, Lcom/miui/server/turbosched/FrameTurboAction;->updateMarkScene(ZLjava/util/List;)Z

    .line 231
    return v1
.end method

.method private updateMarkScene(ZLjava/util/List;)Z
    .locals 5
    .param p1, "enable"    # Z
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(Z",
            "Ljava/util/List<",
            "Ljava/lang/String;",
            ">;)Z"
        }
    .end annotation

    .line 248
    .local p2, "frameConfigs":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;"
    invoke-interface {p2}, Ljava/util/List;->size()I

    move-result v0

    const/4 v1, 0x0

    const/4 v2, 0x1

    if-le v0, v2, :cond_0

    .line 249
    return v1

    .line 251
    :cond_0
    invoke-interface {p2, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    const-string v3, ":"

    invoke-virtual {v0, v3}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v0

    .line 252
    .local v0, "appConfig":[Ljava/lang/String;
    array-length v3, v0

    const/4 v4, 0x2

    if-ge v3, v4, :cond_1

    .line 253
    return v1

    .line 255
    :cond_1
    aget-object v3, v0, v1

    aget-object v4, v0, v2

    invoke-virtual {p0, v3, v4, p1}, Lcom/miui/server/turbosched/FrameTurboAction;->markScene(Ljava/lang/String;Ljava/lang/String;Z)I

    move-result v3

    .line 256
    .local v3, "code":I
    if-nez v3, :cond_2

    move v1, v2

    :cond_2
    return v1
.end method

.method private updateSceneWhiteList(ZLjava/util/List;)V
    .locals 10
    .param p1, "add"    # Z
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(Z",
            "Ljava/util/List<",
            "Ljava/lang/String;",
            ">;)V"
        }
    .end annotation

    .line 260
    .local p2, "sceneList":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;"
    invoke-interface {p2}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v0

    :goto_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    const-string v2, ":"

    const/4 v3, 0x0

    if-eqz v1, :cond_6

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/String;

    .line 261
    .local v1, "scene":Ljava/lang/String;
    invoke-virtual {v1, v2}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v2

    .line 262
    .local v2, "sceneConfig":[Ljava/lang/String;
    array-length v4, v2

    const/4 v5, 0x1

    if-le v4, v5, :cond_5

    .line 263
    aget-object v4, v2, v5

    const-string v5, "\\|"

    invoke-virtual {v4, v5}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v4

    .line 264
    .local v4, "scenes":[Ljava/lang/String;
    iget-object v5, p0, Lcom/miui/server/turbosched/FrameTurboAction;->mFrameTurboSceneWhiteListMap:Ljava/util/Map;

    aget-object v6, v2, v3

    invoke-interface {v5, v6}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Ljava/util/List;

    .line 265
    .local v5, "sceneWhiteList":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;"
    array-length v6, v4

    move v7, v3

    :goto_1
    if-ge v7, v6, :cond_3

    aget-object v8, v4, v7

    .line 266
    .local v8, "s":Ljava/lang/String;
    if-eqz p1, :cond_1

    .line 267
    if-nez v5, :cond_0

    .line 268
    new-instance v9, Ljava/util/ArrayList;

    invoke-direct {v9}, Ljava/util/ArrayList;-><init>()V

    move-object v5, v9

    .line 270
    :cond_0
    invoke-interface {v5, v8}, Ljava/util/List;->contains(Ljava/lang/Object;)Z

    move-result v9

    if-nez v9, :cond_2

    .line 271
    invoke-interface {v5, v8}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_2

    .line 274
    :cond_1
    if-eqz v5, :cond_2

    invoke-interface {v5, v8}, Ljava/util/List;->contains(Ljava/lang/Object;)Z

    move-result v9

    if-eqz v9, :cond_2

    .line 275
    invoke-interface {v5, v8}, Ljava/util/List;->remove(Ljava/lang/Object;)Z

    .line 265
    .end local v8    # "s":Ljava/lang/String;
    :cond_2
    :goto_2
    add-int/lit8 v7, v7, 0x1

    goto :goto_1

    .line 279
    :cond_3
    if-eqz v5, :cond_4

    invoke-interface {v5}, Ljava/util/List;->isEmpty()Z

    move-result v6

    if-nez v6, :cond_4

    .line 280
    iget-object v6, p0, Lcom/miui/server/turbosched/FrameTurboAction;->mFrameTurboSceneWhiteListMap:Ljava/util/Map;

    aget-object v3, v2, v3

    invoke-interface {v6, v3, v5}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    goto :goto_3

    .line 282
    :cond_4
    iget-object v6, p0, Lcom/miui/server/turbosched/FrameTurboAction;->mFrameTurboSceneWhiteListMap:Ljava/util/Map;

    aget-object v3, v2, v3

    invoke-interface {v6, v3}, Ljava/util/Map;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    .line 285
    .end local v1    # "scene":Ljava/lang/String;
    .end local v2    # "sceneConfig":[Ljava/lang/String;
    .end local v4    # "scenes":[Ljava/lang/String;
    .end local v5    # "sceneWhiteList":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;"
    :cond_5
    :goto_3
    goto :goto_0

    .line 286
    :cond_6
    const-string v0, ""

    .line 287
    .local v0, "whiteListStr":Ljava/lang/String;
    iget-object v1, p0, Lcom/miui/server/turbosched/FrameTurboAction;->mFrameTurboSceneWhiteListMap:Ljava/util/Map;

    invoke-interface {v1}, Ljava/util/Map;->keySet()Ljava/util/Set;

    move-result-object v1

    invoke-interface {v1}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :cond_7
    :goto_4
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v4

    if-eqz v4, :cond_9

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Ljava/lang/String;

    .line 288
    .local v4, "key":Ljava/lang/String;
    iget-object v5, p0, Lcom/miui/server/turbosched/FrameTurboAction;->mFrameTurboSceneWhiteListMap:Ljava/util/Map;

    invoke-interface {v5, v4}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Ljava/util/List;

    .line 289
    .restart local v5    # "sceneWhiteList":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;"
    if-eqz v5, :cond_7

    invoke-interface {v5}, Ljava/util/List;->isEmpty()Z

    move-result v6

    if-eqz v6, :cond_8

    .line 290
    goto :goto_4

    .line 292
    :cond_8
    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v6, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    const-string/jumbo v7, "|"

    invoke-static {v7, v5}, Landroid/text/TextUtils;->join(Ljava/lang/CharSequence;Ljava/lang/Iterable;)Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    const-string v7, ", "

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    .line 293
    .end local v4    # "key":Ljava/lang/String;
    .end local v5    # "sceneWhiteList":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;"
    goto :goto_4

    .line 294
    :cond_9
    invoke-virtual {v0}, Ljava/lang/String;->length()I

    move-result v1

    if-lez v1, :cond_a

    .line 295
    invoke-virtual {v0}, Ljava/lang/String;->length()I

    move-result v1

    add-int/lit8 v1, v1, -0x2

    invoke-virtual {v0, v3, v1}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v0

    .line 297
    :cond_a
    const-string v1, "persist.sys.turbosched.frame_turbo.scene_white_list"

    invoke-static {v1, v0}, Landroid/os/SystemProperties;->set(Ljava/lang/String;Ljava/lang/String;)V

    .line 298
    return-void
.end method

.method private updateTriggerList(ZLjava/util/List;)V
    .locals 5
    .param p1, "add"    # Z
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(Z",
            "Ljava/util/List<",
            "Ljava/lang/String;",
            ">;)V"
        }
    .end annotation

    .line 235
    .local p2, "frameCongfigs":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;"
    invoke-interface {p2}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v0

    :cond_0
    :goto_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_2

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/String;

    .line 236
    .local v1, "config":Ljava/lang/String;
    const-string v2, ":"

    if-eqz v1, :cond_1

    invoke-virtual {v1, v2}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v3

    if-nez v3, :cond_1

    .line 237
    iget-boolean v2, p0, Lcom/miui/server/turbosched/FrameTurboAction;->mDebug:Z

    if-eqz v2, :cond_0

    .line 238
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "invalid config: "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    const-string v3, "TurboSched_FrameTurbo"

    invoke-static {v3, v2}, Landroid/util/Slog;->d(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    .line 242
    :cond_1
    invoke-virtual {v1, v2}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v2

    .line 243
    .local v2, "appConfig":[Ljava/lang/String;
    const/4 v3, 0x0

    aget-object v3, v2, v3

    const/4 v4, 0x1

    aget-object v4, v2, v4

    invoke-virtual {p0, p1, v3, v4}, Lcom/miui/server/turbosched/FrameTurboAction;->enableFrameTurboInternal(ZLjava/lang/String;Ljava/lang/String;)V

    .line 244
    .end local v1    # "config":Ljava/lang/String;
    .end local v2    # "appConfig":[Ljava/lang/String;
    goto :goto_0

    .line 245
    :cond_2
    return-void
.end method


# virtual methods
.method protected enableFrameTurboInternal(ZLjava/lang/String;Ljava/lang/String;)V
    .locals 9
    .param p1, "enable"    # Z
    .param p2, "processThread"    # Ljava/lang/String;
    .param p3, "sceneListStr"    # Ljava/lang/String;

    .line 384
    const-string v0, ","

    if-eqz p1, :cond_5

    .line 385
    iget-object v1, p0, Lcom/miui/server/turbosched/FrameTurboAction;->mFrameTurboAppMap:Ljava/util/Map;

    invoke-interface {v1, p2}, Ljava/util/Map;->containsKey(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    .line 386
    iget-object v1, p0, Lcom/miui/server/turbosched/FrameTurboAction;->mFrameTurboAppMap:Ljava/util/Map;

    invoke-interface {v1, p2}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/util/List;

    .line 387
    .local v1, "sceneList":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;"
    if-eqz p3, :cond_1

    .line 388
    invoke-virtual {p3, v0}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v2

    .line 389
    .local v2, "sceneArray":[Ljava/lang/String;
    const/4 v3, 0x0

    .local v3, "i":I
    :goto_0
    array-length v4, v2

    if-ge v3, v4, :cond_1

    .line 390
    aget-object v4, v2, v3

    invoke-interface {v1, v4}, Ljava/util/List;->contains(Ljava/lang/Object;)Z

    move-result v4

    if-nez v4, :cond_0

    .line 391
    aget-object v4, v2, v3

    invoke-interface {v1, v4}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 389
    :cond_0
    add-int/lit8 v3, v3, 0x1

    goto :goto_0

    .line 395
    .end local v1    # "sceneList":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;"
    .end local v2    # "sceneArray":[Ljava/lang/String;
    .end local v3    # "i":I
    :cond_1
    goto :goto_3

    .line 396
    :cond_2
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    .line 397
    .restart local v1    # "sceneList":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;"
    if-eqz p3, :cond_4

    .line 398
    invoke-virtual {p3, v0}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v2

    .line 399
    .restart local v2    # "sceneArray":[Ljava/lang/String;
    const/4 v3, 0x0

    .restart local v3    # "i":I
    :goto_1
    array-length v4, v2

    if-ge v3, v4, :cond_3

    .line 400
    aget-object v4, v2, v3

    invoke-interface {v1, v4}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 399
    add-int/lit8 v3, v3, 0x1

    goto :goto_1

    .line 402
    .end local v3    # "i":I
    :cond_3
    iget-object v3, p0, Lcom/miui/server/turbosched/FrameTurboAction;->mFrameTurboAppMap:Ljava/util/Map;

    invoke-interface {v3, p2, v1}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 404
    .end local v1    # "sceneList":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;"
    .end local v2    # "sceneArray":[Ljava/lang/String;
    :cond_4
    goto :goto_3

    .line 406
    :cond_5
    iget-object v1, p0, Lcom/miui/server/turbosched/FrameTurboAction;->mFrameTurboAppMap:Ljava/util/Map;

    invoke-interface {v1, p2}, Ljava/util/Map;->containsKey(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_8

    .line 407
    iget-object v1, p0, Lcom/miui/server/turbosched/FrameTurboAction;->mFrameTurboAppMap:Ljava/util/Map;

    invoke-interface {v1, p2}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/util/List;

    .line 408
    .restart local v1    # "sceneList":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;"
    if-eqz p3, :cond_8

    .line 409
    invoke-virtual {p3, v0}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v2

    .line 410
    .restart local v2    # "sceneArray":[Ljava/lang/String;
    const/4 v3, 0x0

    .restart local v3    # "i":I
    :goto_2
    array-length v4, v2

    if-ge v3, v4, :cond_8

    .line 411
    aget-object v4, v2, v3

    invoke-interface {v1, v4}, Ljava/util/List;->contains(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_6

    .line 412
    aget-object v4, v2, v3

    invoke-interface {v1, v4}, Ljava/util/List;->remove(Ljava/lang/Object;)Z

    .line 414
    :cond_6
    invoke-interface {v1}, Ljava/util/List;->size()I

    move-result v4

    if-nez v4, :cond_7

    .line 415
    iget-object v4, p0, Lcom/miui/server/turbosched/FrameTurboAction;->mFrameTurboAppMap:Ljava/util/Map;

    invoke-interface {v4, p2}, Ljava/util/Map;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    .line 410
    :cond_7
    add-int/lit8 v3, v3, 0x1

    goto :goto_2

    .line 421
    .end local v1    # "sceneList":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;"
    .end local v2    # "sceneArray":[Ljava/lang/String;
    .end local v3    # "i":I
    :cond_8
    :goto_3
    const-string v1, ""

    .line 422
    .local v1, "frameTurboApps":Ljava/lang/String;
    iget-object v2, p0, Lcom/miui/server/turbosched/FrameTurboAction;->mFrameTurboAppMap:Ljava/util/Map;

    invoke-interface {v2}, Ljava/util/Map;->entrySet()Ljava/util/Set;

    move-result-object v2

    invoke-interface {v2}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_4
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_9

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/util/Map$Entry;

    .line 423
    .local v3, "entry":Ljava/util/Map$Entry;, "Ljava/util/Map$Entry<Ljava/lang/String;Ljava/util/List<Ljava/lang/String;>;>;"
    invoke-interface {v3}, Ljava/util/Map$Entry;->getKey()Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Ljava/lang/String;

    .line 424
    .local v4, "key":Ljava/lang/String;
    invoke-interface {v3}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Ljava/util/List;

    .line 425
    .local v5, "value":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;"
    const-string/jumbo v6, "|"

    invoke-static {v6, v5}, Landroid/text/TextUtils;->join(Ljava/lang/CharSequence;Ljava/lang/Iterable;)Ljava/lang/String;

    move-result-object v6

    .line 426
    .local v6, "sceneList":Ljava/lang/String;
    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v7, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    const-string v8, ":"

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    .line 427
    .end local v3    # "entry":Ljava/util/Map$Entry;, "Ljava/util/Map$Entry<Ljava/lang/String;Ljava/util/List<Ljava/lang/String;>;>;"
    .end local v4    # "key":Ljava/lang/String;
    .end local v5    # "value":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;"
    .end local v6    # "sceneList":Ljava/lang/String;
    goto :goto_4

    .line 428
    :cond_9
    invoke-virtual {v1}, Ljava/lang/String;->length()I

    move-result v0

    if-lez v0, :cond_a

    .line 429
    invoke-virtual {v1}, Ljava/lang/String;->length()I

    move-result v0

    add-int/lit8 v0, v0, -0x1

    const/4 v2, 0x0

    invoke-virtual {v1, v2, v0}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v1

    .line 431
    :cond_a
    const-string v0, "persist.sys.turbosched.frame_turbo.apps"

    invoke-static {v0, v1}, Landroid/os/SystemProperties;->set(Ljava/lang/String;Ljava/lang/String;)V

    .line 432
    return-void
.end method

.method protected markScene(Ljava/lang/String;Ljava/lang/String;Z)I
    .locals 4
    .param p1, "packageName"    # Ljava/lang/String;
    .param p2, "sceneId"    # Ljava/lang/String;
    .param p3, "start"    # Z

    .line 485
    if-eqz p3, :cond_0

    .line 486
    invoke-direct {p0, p1, p2}, Lcom/miui/server/turbosched/FrameTurboAction;->checkPermission(Ljava/lang/String;Ljava/lang/String;)I

    move-result v0

    .line 487
    .local v0, "code":I
    if-eqz v0, :cond_0

    .line 488
    return v0

    .line 493
    .end local v0    # "code":I
    :cond_0
    const/4 v0, 0x0

    if-eqz p3, :cond_4

    .line 494
    const-string v1, ""

    iget-object v2, p0, Lcom/miui/server/turbosched/FrameTurboAction;->mCurrentTurboScene:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_3

    .line 495
    iget-object v0, p0, Lcom/miui/server/turbosched/FrameTurboAction;->mActiveSceneWaitListMap:Ljava/util/Map;

    invoke-interface {v0, p1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/LinkedList;

    .line 496
    .local v0, "wList":Ljava/util/LinkedList;, "Ljava/util/LinkedList<Ljava/lang/String;>;"
    if-nez v0, :cond_1

    .line 497
    new-instance v1, Ljava/util/LinkedList;

    invoke-direct {v1}, Ljava/util/LinkedList;-><init>()V

    move-object v0, v1

    .line 499
    :cond_1
    invoke-virtual {v0, p2}, Ljava/util/LinkedList;->contains(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_2

    .line 500
    invoke-virtual {v0, p2}, Ljava/util/LinkedList;->add(Ljava/lang/Object;)Z

    .line 502
    :cond_2
    iget-object v1, p0, Lcom/miui/server/turbosched/FrameTurboAction;->mActiveSceneWaitListMap:Ljava/util/Map;

    invoke-interface {v1, p1, v0}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 503
    const/4 v1, -0x5

    return v1

    .line 505
    .end local v0    # "wList":Ljava/util/LinkedList;, "Ljava/util/LinkedList<Ljava/lang/String;>;"
    :cond_3
    invoke-virtual {p0, p1, p2, p3}, Lcom/miui/server/turbosched/FrameTurboAction;->notifySceneChanged(Ljava/lang/String;Ljava/lang/String;Z)V

    .line 506
    return v0

    .line 511
    :cond_4
    if-nez p3, :cond_8

    .line 512
    invoke-virtual {p0, p1, p2, p3}, Lcom/miui/server/turbosched/FrameTurboAction;->notifySceneChanged(Ljava/lang/String;Ljava/lang/String;Z)V

    .line 513
    iget-object v1, p0, Lcom/miui/server/turbosched/FrameTurboAction;->mActiveSceneWaitListMap:Ljava/util/Map;

    invoke-interface {v1, p1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/util/LinkedList;

    .line 514
    .local v1, "wList":Ljava/util/LinkedList;, "Ljava/util/LinkedList<Ljava/lang/String;>;"
    if-eqz v1, :cond_8

    .line 515
    invoke-virtual {v1, p2}, Ljava/util/LinkedList;->contains(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_5

    .line 516
    invoke-virtual {v1, p2}, Ljava/util/LinkedList;->remove(Ljava/lang/Object;)Z

    .line 518
    :cond_5
    invoke-virtual {v1}, Ljava/util/LinkedList;->size()I

    move-result v2

    if-lez v2, :cond_6

    .line 519
    invoke-virtual {v1}, Ljava/util/LinkedList;->getFirst()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/lang/String;

    .line 520
    .local v2, "nextScene":Ljava/lang/String;
    const/4 v3, 0x1

    invoke-virtual {p0, p1, v2, v3}, Lcom/miui/server/turbosched/FrameTurboAction;->notifySceneChanged(Ljava/lang/String;Ljava/lang/String;Z)V

    .line 521
    invoke-virtual {v1}, Ljava/util/LinkedList;->removeFirst()Ljava/lang/Object;

    .line 524
    .end local v2    # "nextScene":Ljava/lang/String;
    :cond_6
    invoke-virtual {v1}, Ljava/util/LinkedList;->size()I

    move-result v2

    if-nez v2, :cond_7

    .line 525
    iget-object v2, p0, Lcom/miui/server/turbosched/FrameTurboAction;->mActiveSceneWaitListMap:Ljava/util/Map;

    invoke-interface {v2, p1}, Ljava/util/Map;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    goto :goto_0

    .line 527
    :cond_7
    iget-object v2, p0, Lcom/miui/server/turbosched/FrameTurboAction;->mActiveSceneWaitListMap:Ljava/util/Map;

    invoke-interface {v2, p1, v1}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 531
    .end local v1    # "wList":Ljava/util/LinkedList;, "Ljava/util/LinkedList<Ljava/lang/String;>;"
    :cond_8
    :goto_0
    return v0
.end method

.method protected notifySceneChanged(Ljava/lang/String;Ljava/lang/String;Z)V
    .locals 6
    .param p1, "packageName"    # Ljava/lang/String;
    .param p2, "sceneId"    # Ljava/lang/String;
    .param p3, "start"    # Z

    .line 457
    iget-boolean v0, p0, Lcom/miui/server/turbosched/FrameTurboAction;->mDebug:Z

    if-eqz v0, :cond_0

    .line 458
    const-string v0, "TurboSched_FrameTurbo"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "notifySceneChanged, packageName: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ",sceneId: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ",start: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p3}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ",mFrameTurboSceneCallbacks: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lcom/miui/server/turbosched/FrameTurboAction;->mFrameTurboSceneCallbacks:Ljava/util/List;

    invoke-interface {v2}, Ljava/util/List;->size()I

    move-result v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Slog;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 460
    :cond_0
    if-eqz p3, :cond_1

    .line 461
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ":"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/miui/server/turbosched/FrameTurboAction;->mCurrentTurboScene:Ljava/lang/String;

    goto :goto_0

    .line 463
    :cond_1
    const-string v0, ""

    iput-object v0, p0, Lcom/miui/server/turbosched/FrameTurboAction;->mCurrentTurboScene:Ljava/lang/String;

    .line 465
    :goto_0
    const-string v0, "persist.sys.turbosched.frame_turbo.current_turbo_scene"

    iget-object v1, p0, Lcom/miui/server/turbosched/FrameTurboAction;->mCurrentTurboScene:Ljava/lang/String;

    invoke-static {v0, v1}, Landroid/os/SystemProperties;->set(Ljava/lang/String;Ljava/lang/String;)V

    .line 468
    iget-object v0, p0, Lcom/miui/server/turbosched/FrameTurboAction;->mFrameTurboSceneCallbacks:Ljava/util/List;

    monitor-enter v0

    .line 469
    :try_start_0
    iget-object v1, p0, Lcom/miui/server/turbosched/FrameTurboAction;->mFrameTurboSceneCallbacks:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->size()I

    move-result v1

    if-nez v1, :cond_2

    .line 470
    monitor-exit v0

    return-void

    .line 472
    :cond_2
    iget-object v1, p0, Lcom/miui/server/turbosched/FrameTurboAction;->mFrameTurboSceneCallbacks:Ljava/util/List;

    const/4 v2, 0x0

    new-array v2, v2, [Lmiui/turbosched/ITurboSchedManager$IFrameTurboSceneCallback;

    invoke-interface {v1, v2}, Ljava/util/List;->toArray([Ljava/lang/Object;)[Ljava/lang/Object;

    move-result-object v1

    check-cast v1, [Lmiui/turbosched/ITurboSchedManager$IFrameTurboSceneCallback;

    .line 473
    .local v1, "copyCallbacks":[Lmiui/turbosched/ITurboSchedManager$IFrameTurboSceneCallback;
    monitor-exit v0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 474
    const/4 v0, 0x0

    .local v0, "i":I
    :goto_1
    array-length v2, v1

    if-ge v0, v2, :cond_3

    .line 476
    :try_start_1
    aget-object v2, v1, v0

    invoke-interface {v2, p1, p2, p3}, Lmiui/turbosched/ITurboSchedManager$IFrameTurboSceneCallback;->onSceneChanged(Ljava/lang/String;Ljava/lang/String;Z)V
    :try_end_1
    .catch Landroid/os/RemoteException; {:try_start_1 .. :try_end_1} :catch_0

    .line 479
    goto :goto_2

    .line 477
    :catch_0
    move-exception v2

    .line 478
    .local v2, "e":Landroid/os/RemoteException;
    const-string v3, "TurboSched_FrameTurbo"

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "notifySceneChanged, RemoteException: "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v2}, Landroid/os/RemoteException;->getMessage()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Landroid/util/Slog;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 474
    .end local v2    # "e":Landroid/os/RemoteException;
    :goto_2
    add-int/lit8 v0, v0, 0x1

    goto :goto_1

    .line 481
    .end local v0    # "i":I
    :cond_3
    return-void

    .line 473
    .end local v1    # "copyCallbacks":[Lmiui/turbosched/ITurboSchedManager$IFrameTurboSceneCallback;
    :catchall_0
    move-exception v1

    :try_start_2
    monitor-exit v0
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    throw v1
.end method

.method protected onForegroundActivitiesChanged(Ljava/lang/String;Z)V
    .locals 5
    .param p1, "processName"    # Ljava/lang/String;
    .param p2, "foregroundActivities"    # Z

    .line 586
    const/4 v0, 0x1

    if-nez p2, :cond_4

    .line 587
    iget-object v1, p0, Lcom/miui/server/turbosched/FrameTurboAction;->mCurrentTurboScene:Ljava/lang/String;

    const-string v2, ""

    invoke-virtual {v2, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 588
    return-void

    .line 590
    :cond_0
    iget-object v1, p0, Lcom/miui/server/turbosched/FrameTurboAction;->mCurrentTurboScene:Ljava/lang/String;

    const-string v3, ":"

    invoke-virtual {v1, v3}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v1

    .line 591
    .local v1, "sceneInfo":[Ljava/lang/String;
    array-length v3, v1

    const/4 v4, 0x2

    if-eq v3, v4, :cond_1

    .line 592
    return-void

    .line 594
    :cond_1
    const/4 v3, 0x0

    aget-object v4, v1, v3

    invoke-virtual {v4, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-nez v4, :cond_2

    .line 595
    return-void

    .line 597
    :cond_2
    aget-object v0, v1, v0

    .line 598
    .local v0, "sceneId":Ljava/lang/String;
    invoke-virtual {p0, p1, v0, v3}, Lcom/miui/server/turbosched/FrameTurboAction;->notifySceneChanged(Ljava/lang/String;Ljava/lang/String;Z)V

    .line 600
    iget-object v3, p0, Lcom/miui/server/turbosched/FrameTurboAction;->mActiveSceneWaitListMap:Ljava/util/Map;

    invoke-interface {v3, p1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/util/LinkedList;

    .line 601
    .local v3, "wList":Ljava/util/LinkedList;, "Ljava/util/LinkedList<Ljava/lang/String;>;"
    if-nez v3, :cond_3

    .line 602
    new-instance v4, Ljava/util/LinkedList;

    invoke-direct {v4}, Ljava/util/LinkedList;-><init>()V

    move-object v3, v4

    .line 604
    :cond_3
    invoke-virtual {v3, v0}, Ljava/util/LinkedList;->addFirst(Ljava/lang/Object;)V

    .line 605
    iget-object v4, p0, Lcom/miui/server/turbosched/FrameTurboAction;->mActiveSceneWaitListMap:Ljava/util/Map;

    invoke-interface {v4, p1, v3}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 606
    iput-object v2, p0, Lcom/miui/server/turbosched/FrameTurboAction;->mCurrentTurboScene:Ljava/lang/String;

    .line 607
    .end local v0    # "sceneId":Ljava/lang/String;
    .end local v1    # "sceneInfo":[Ljava/lang/String;
    .end local v3    # "wList":Ljava/util/LinkedList;, "Ljava/util/LinkedList<Ljava/lang/String;>;"
    goto :goto_0

    .line 609
    :cond_4
    iget-object v1, p0, Lcom/miui/server/turbosched/FrameTurboAction;->mActiveSceneWaitListMap:Ljava/util/Map;

    invoke-interface {v1, p1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/util/LinkedList;

    .line 610
    .local v1, "wList":Ljava/util/LinkedList;, "Ljava/util/LinkedList<Ljava/lang/String;>;"
    if-eqz v1, :cond_7

    .line 611
    invoke-virtual {v1}, Ljava/util/LinkedList;->size()I

    move-result v2

    if-lez v2, :cond_5

    .line 612
    invoke-virtual {v1}, Ljava/util/LinkedList;->getFirst()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/lang/String;

    .line 613
    .local v2, "nextScene":Ljava/lang/String;
    invoke-virtual {p0, p1, v2, v0}, Lcom/miui/server/turbosched/FrameTurboAction;->notifySceneChanged(Ljava/lang/String;Ljava/lang/String;Z)V

    .line 614
    invoke-virtual {v1}, Ljava/util/LinkedList;->removeFirst()Ljava/lang/Object;

    .line 617
    .end local v2    # "nextScene":Ljava/lang/String;
    :cond_5
    invoke-virtual {v1}, Ljava/util/LinkedList;->size()I

    move-result v0

    if-nez v0, :cond_6

    .line 618
    iget-object v0, p0, Lcom/miui/server/turbosched/FrameTurboAction;->mActiveSceneWaitListMap:Ljava/util/Map;

    invoke-interface {v0, p1}, Ljava/util/Map;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    goto :goto_0

    .line 620
    :cond_6
    iget-object v0, p0, Lcom/miui/server/turbosched/FrameTurboAction;->mActiveSceneWaitListMap:Ljava/util/Map;

    invoke-interface {v0, p1, v1}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 624
    .end local v1    # "wList":Ljava/util/LinkedList;, "Ljava/util/LinkedList<Ljava/lang/String;>;"
    :cond_7
    :goto_0
    return-void
.end method

.method protected parseFrameTurboCommand(Ljava/io/FileDescriptor;Ljava/io/PrintWriter;[Ljava/lang/String;)Z
    .locals 11
    .param p1, "fd"    # Ljava/io/FileDescriptor;
    .param p2, "pw"    # Ljava/io/PrintWriter;
    .param p3, "args"    # [Ljava/lang/String;

    .line 328
    array-length v0, p3

    const/4 v1, 0x2

    const/4 v2, 0x1

    if-lt v0, v1, :cond_1

    .line 329
    invoke-direct {p0, p1, p2, p3}, Lcom/miui/server/turbosched/FrameTurboAction;->parseCommandArgs(Ljava/io/FileDescriptor;Ljava/io/PrintWriter;[Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 330
    return v2

    .line 332
    :cond_0
    const-string/jumbo v0, "wrong command"

    invoke-virtual {p2, v0}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V

    .line 333
    return v2

    .line 336
    :cond_1
    const-string v0, "--------------------config-------------------------------"

    invoke-virtual {p2, v0}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V

    .line 337
    invoke-static {}, Lcom/miui/server/turbosched/TurboSchedConfig;->getPolicyList()Ljava/util/List;

    move-result-object v0

    const-string v1, "frame_turbo"

    invoke-interface {v0, v1}, Ljava/util/List;->contains(Ljava/lang/Object;)Z

    move-result v0

    .line 338
    .local v0, "isEnable":Z
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "enable: "

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p2, v1}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V

    .line 339
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "debug: "

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-boolean v3, p0, Lcom/miui/server/turbosched/FrameTurboAction;->mDebug:Z

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p2, v1}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V

    .line 340
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "current scene: "

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v3, p0, Lcom/miui/server/turbosched/FrameTurboAction;->mCurrentTurboScene:Ljava/lang/String;

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p2, v1}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V

    .line 341
    iget-object v1, p0, Lcom/miui/server/turbosched/FrameTurboAction;->mFrameTurboAppMap:Ljava/util/Map;

    const/4 v3, 0x0

    const-string v4, ":"

    const-string/jumbo v5, "|"

    if-eqz v1, :cond_4

    invoke-interface {v1}, Ljava/util/Map;->size()I

    move-result v1

    if-lez v1, :cond_4

    .line 342
    const-string v1, "-----------------turbo trigger list------------------------"

    invoke-virtual {p2, v1}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V

    .line 343
    iget-object v1, p0, Lcom/miui/server/turbosched/FrameTurboAction;->mFrameTurboAppMap:Ljava/util/Map;

    invoke-interface {v1}, Ljava/util/Map;->entrySet()Ljava/util/Set;

    move-result-object v1

    invoke-interface {v1}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v6

    if-eqz v6, :cond_4

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v6

    check-cast v6, Ljava/util/Map$Entry;

    .line 344
    .local v6, "entry":Ljava/util/Map$Entry;, "Ljava/util/Map$Entry<Ljava/lang/String;Ljava/util/List<Ljava/lang/String;>;>;"
    const-string v7, ""

    .line 345
    .local v7, "valueStr":Ljava/lang/String;
    invoke-interface {v6}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v8

    check-cast v8, Ljava/util/List;

    invoke-interface {v8}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v8

    :goto_1
    invoke-interface {v8}, Ljava/util/Iterator;->hasNext()Z

    move-result v9

    if-eqz v9, :cond_2

    invoke-interface {v8}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v9

    check-cast v9, Ljava/lang/String;

    .line 346
    .local v9, "value":Ljava/lang/String;
    new-instance v10, Ljava/lang/StringBuilder;

    invoke-direct {v10}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v10, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-virtual {v10, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-virtual {v10, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-virtual {v10}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    .line 347
    .end local v9    # "value":Ljava/lang/String;
    goto :goto_1

    .line 348
    :cond_2
    invoke-virtual {v7, v5}, Ljava/lang/String;->endsWith(Ljava/lang/String;)Z

    move-result v8

    if-eqz v8, :cond_3

    .line 349
    invoke-virtual {v7}, Ljava/lang/String;->length()I

    move-result v8

    sub-int/2addr v8, v2

    invoke-virtual {v7, v3, v8}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v7

    .line 351
    :cond_3
    new-instance v8, Ljava/lang/StringBuilder;

    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    invoke-interface {v6}, Ljava/util/Map$Entry;->getKey()Ljava/lang/Object;

    move-result-object v9

    check-cast v9, Ljava/lang/String;

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    invoke-virtual {p2, v8}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V

    .line 352
    .end local v6    # "entry":Ljava/util/Map$Entry;, "Ljava/util/Map$Entry<Ljava/lang/String;Ljava/util/List<Ljava/lang/String;>;>;"
    .end local v7    # "valueStr":Ljava/lang/String;
    goto :goto_0

    .line 354
    :cond_4
    iget-object v1, p0, Lcom/miui/server/turbosched/FrameTurboAction;->mFrameTurboSceneWhiteListMap:Ljava/util/Map;

    if-eqz v1, :cond_7

    invoke-interface {v1}, Ljava/util/Map;->size()I

    move-result v1

    if-lez v1, :cond_7

    .line 355
    const-string v1, "--------------------scene white list---------------------"

    invoke-virtual {p2, v1}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V

    .line 356
    iget-object v1, p0, Lcom/miui/server/turbosched/FrameTurboAction;->mFrameTurboSceneWhiteListMap:Ljava/util/Map;

    invoke-interface {v1}, Ljava/util/Map;->entrySet()Ljava/util/Set;

    move-result-object v1

    invoke-interface {v1}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_2
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v6

    if-eqz v6, :cond_7

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v6

    check-cast v6, Ljava/util/Map$Entry;

    .line 357
    .restart local v6    # "entry":Ljava/util/Map$Entry;, "Ljava/util/Map$Entry<Ljava/lang/String;Ljava/util/List<Ljava/lang/String;>;>;"
    const-string v7, ""

    .line 358
    .restart local v7    # "valueStr":Ljava/lang/String;
    invoke-interface {v6}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v8

    check-cast v8, Ljava/util/List;

    invoke-interface {v8}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v8

    :goto_3
    invoke-interface {v8}, Ljava/util/Iterator;->hasNext()Z

    move-result v9

    if-eqz v9, :cond_5

    invoke-interface {v8}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v9

    check-cast v9, Ljava/lang/String;

    .line 359
    .restart local v9    # "value":Ljava/lang/String;
    new-instance v10, Ljava/lang/StringBuilder;

    invoke-direct {v10}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v10, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-virtual {v10, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-virtual {v10, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-virtual {v10}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    .line 360
    .end local v9    # "value":Ljava/lang/String;
    goto :goto_3

    .line 361
    :cond_5
    invoke-virtual {v7, v5}, Ljava/lang/String;->endsWith(Ljava/lang/String;)Z

    move-result v8

    if-eqz v8, :cond_6

    .line 362
    invoke-virtual {v7}, Ljava/lang/String;->length()I

    move-result v8

    sub-int/2addr v8, v2

    invoke-virtual {v7, v3, v8}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v7

    .line 364
    :cond_6
    new-instance v8, Ljava/lang/StringBuilder;

    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    invoke-interface {v6}, Ljava/util/Map$Entry;->getKey()Ljava/lang/Object;

    move-result-object v9

    check-cast v9, Ljava/lang/String;

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    invoke-virtual {p2, v8}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V

    .line 365
    .end local v6    # "entry":Ljava/util/Map$Entry;, "Ljava/util/Map$Entry<Ljava/lang/String;Ljava/util/List<Ljava/lang/String;>;>;"
    .end local v7    # "valueStr":Ljava/lang/String;
    goto :goto_2

    .line 367
    :cond_7
    iget-object v1, p0, Lcom/miui/server/turbosched/FrameTurboAction;->mActiveSceneWaitListMap:Ljava/util/Map;

    if-eqz v1, :cond_a

    invoke-interface {v1}, Ljava/util/Map;->size()I

    move-result v1

    if-lez v1, :cond_a

    .line 368
    const-string v1, "--------------------active scene wait set---------------------"

    invoke-virtual {p2, v1}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V

    .line 369
    iget-object v1, p0, Lcom/miui/server/turbosched/FrameTurboAction;->mActiveSceneWaitListMap:Ljava/util/Map;

    invoke-interface {v1}, Ljava/util/Map;->entrySet()Ljava/util/Set;

    move-result-object v1

    invoke-interface {v1}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_4
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v6

    if-eqz v6, :cond_a

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v6

    check-cast v6, Ljava/util/Map$Entry;

    .line 370
    .local v6, "entry":Ljava/util/Map$Entry;, "Ljava/util/Map$Entry<Ljava/lang/String;Ljava/util/LinkedList<Ljava/lang/String;>;>;"
    const-string v7, ""

    .line 371
    .restart local v7    # "valueStr":Ljava/lang/String;
    invoke-interface {v6}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v8

    check-cast v8, Ljava/util/LinkedList;

    invoke-virtual {v8}, Ljava/util/LinkedList;->iterator()Ljava/util/Iterator;

    move-result-object v8

    :goto_5
    invoke-interface {v8}, Ljava/util/Iterator;->hasNext()Z

    move-result v9

    if-eqz v9, :cond_8

    invoke-interface {v8}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v9

    check-cast v9, Ljava/lang/String;

    .line 372
    .restart local v9    # "value":Ljava/lang/String;
    new-instance v10, Ljava/lang/StringBuilder;

    invoke-direct {v10}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v10, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-virtual {v10, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-virtual {v10, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-virtual {v10}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    .line 373
    .end local v9    # "value":Ljava/lang/String;
    goto :goto_5

    .line 374
    :cond_8
    invoke-virtual {v7, v5}, Ljava/lang/String;->endsWith(Ljava/lang/String;)Z

    move-result v8

    if-eqz v8, :cond_9

    .line 375
    invoke-virtual {v7}, Ljava/lang/String;->length()I

    move-result v8

    sub-int/2addr v8, v2

    invoke-virtual {v7, v3, v8}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v7

    .line 377
    :cond_9
    new-instance v8, Ljava/lang/StringBuilder;

    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    invoke-interface {v6}, Ljava/util/Map$Entry;->getKey()Ljava/lang/Object;

    move-result-object v9

    check-cast v9, Ljava/lang/String;

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    invoke-virtual {p2, v8}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V

    .line 378
    .end local v6    # "entry":Ljava/util/Map$Entry;, "Ljava/util/Map$Entry<Ljava/lang/String;Ljava/util/LinkedList<Ljava/lang/String;>;>;"
    .end local v7    # "valueStr":Ljava/lang/String;
    goto :goto_4

    .line 380
    :cond_a
    return v2
.end method

.method protected registerSceneCallback(Lmiui/turbosched/ITurboSchedManager$IFrameTurboSceneCallback;)V
    .locals 2
    .param p1, "callback"    # Lmiui/turbosched/ITurboSchedManager$IFrameTurboSceneCallback;

    .line 435
    iget-boolean v0, p0, Lcom/miui/server/turbosched/FrameTurboAction;->mDebug:Z

    if-eqz v0, :cond_0

    .line 436
    const-string v0, "TurboSched_FrameTurbo"

    const-string v1, "registerSceneCallback"

    invoke-static {v0, v1}, Landroid/util/Slog;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 438
    :cond_0
    iget-object v0, p0, Lcom/miui/server/turbosched/FrameTurboAction;->mFrameTurboSceneCallbacks:Ljava/util/List;

    monitor-enter v0

    .line 439
    :try_start_0
    iget-object v1, p0, Lcom/miui/server/turbosched/FrameTurboAction;->mFrameTurboSceneCallbacks:Ljava/util/List;

    invoke-interface {v1, p1}, Ljava/util/List;->contains(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_1

    .line 440
    iget-object v1, p0, Lcom/miui/server/turbosched/FrameTurboAction;->mFrameTurboSceneCallbacks:Ljava/util/List;

    invoke-interface {v1, p1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 442
    :cond_1
    monitor-exit v0

    .line 443
    return-void

    .line 442
    :catchall_0
    move-exception v1

    monitor-exit v0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v1
.end method

.method protected triggerFrameTurbo(Ljava/lang/String;ZLjava/util/List;Ljava/util/List;)I
    .locals 4
    .param p1, "packageName"    # Ljava/lang/String;
    .param p2, "turbo"    # Z
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "Z",
            "Ljava/util/List<",
            "Ljava/lang/String;",
            ">;",
            "Ljava/util/List<",
            "Ljava/lang/String;",
            ">;)I"
        }
    .end annotation

    .line 535
    .local p3, "uiThreads":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;"
    .local p4, "scenes":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;"
    invoke-interface {p3}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v0

    :goto_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_2

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/String;

    .line 536
    .local v1, "uiThread":Ljava/lang/String;
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, "#"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    .line 537
    .local v2, "processThread":Ljava/lang/String;
    if-eqz p4, :cond_1

    invoke-interface {p4}, Ljava/util/List;->size()I

    move-result v3

    if-nez v3, :cond_0

    goto :goto_1

    .line 540
    :cond_0
    const-string/jumbo v3, "|"

    invoke-static {v3, p4}, Landroid/text/TextUtils;->join(Ljava/lang/CharSequence;Ljava/lang/Iterable;)Ljava/lang/String;

    move-result-object v3

    .line 541
    .local v3, "sceneList":Ljava/lang/String;
    invoke-virtual {p0, p2, v2, v3}, Lcom/miui/server/turbosched/FrameTurboAction;->enableFrameTurboInternal(ZLjava/lang/String;Ljava/lang/String;)V

    .line 542
    .end local v1    # "uiThread":Ljava/lang/String;
    .end local v2    # "processThread":Ljava/lang/String;
    .end local v3    # "sceneList":Ljava/lang/String;
    goto :goto_0

    .line 538
    .restart local v1    # "uiThread":Ljava/lang/String;
    .restart local v2    # "processThread":Ljava/lang/String;
    :cond_1
    :goto_1
    const/4 v0, -0x3

    return v0

    .line 543
    .end local v1    # "uiThread":Ljava/lang/String;
    .end local v2    # "processThread":Ljava/lang/String;
    :cond_2
    const/4 v0, 0x0

    return v0
.end method

.method protected unregisterSceneCallback(Lmiui/turbosched/ITurboSchedManager$IFrameTurboSceneCallback;)V
    .locals 2
    .param p1, "callback"    # Lmiui/turbosched/ITurboSchedManager$IFrameTurboSceneCallback;

    .line 446
    iget-boolean v0, p0, Lcom/miui/server/turbosched/FrameTurboAction;->mDebug:Z

    if-eqz v0, :cond_0

    .line 447
    const-string v0, "TurboSched_FrameTurbo"

    const-string/jumbo v1, "unregisterSceneCallback"

    invoke-static {v0, v1}, Landroid/util/Slog;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 449
    :cond_0
    iget-object v0, p0, Lcom/miui/server/turbosched/FrameTurboAction;->mFrameTurboSceneCallbacks:Ljava/util/List;

    monitor-enter v0

    .line 450
    :try_start_0
    iget-object v1, p0, Lcom/miui/server/turbosched/FrameTurboAction;->mFrameTurboSceneCallbacks:Ljava/util/List;

    invoke-interface {v1, p1}, Ljava/util/List;->contains(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_1

    .line 451
    iget-object v1, p0, Lcom/miui/server/turbosched/FrameTurboAction;->mFrameTurboSceneCallbacks:Ljava/util/List;

    invoke-interface {v1, p1}, Ljava/util/List;->remove(Ljava/lang/Object;)Z

    .line 453
    :cond_1
    monitor-exit v0

    .line 454
    return-void

    .line 453
    :catchall_0
    move-exception v1

    monitor-exit v0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v1
.end method

.method protected updateWhiteList([Ljava/lang/String;)V
    .locals 11
    .param p1, "pkgScenes"    # [Ljava/lang/String;

    .line 548
    iget-object v0, p0, Lcom/miui/server/turbosched/FrameTurboAction;->mFrameTurboSceneWhiteListMap:Ljava/util/Map;

    invoke-interface {v0}, Ljava/util/Map;->clear()V

    .line 549
    array-length v0, p1

    if-lez v0, :cond_9

    .line 550
    array-length v0, p1

    const/4 v1, 0x0

    move v2, v1

    :goto_0
    const/4 v3, 0x2

    const-string v4, ":"

    if-ge v2, v0, :cond_4

    aget-object v5, p1, v2

    .line 551
    .local v5, "pkgScene":Ljava/lang/String;
    invoke-virtual {v5, v4}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v4

    .line 552
    .local v4, "pkgSceneInfo":[Ljava/lang/String;
    array-length v6, v4

    if-ne v6, v3, :cond_3

    .line 553
    aget-object v3, v4, v1

    .line 554
    .local v3, "pkgName":Ljava/lang/String;
    iget-object v6, p0, Lcom/miui/server/turbosched/FrameTurboAction;->mFrameTurboSceneWhiteListMap:Ljava/util/Map;

    invoke-interface {v6, v3}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v6

    check-cast v6, Ljava/util/List;

    .line 555
    .local v6, "sceneList":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;"
    if-nez v6, :cond_0

    .line 556
    new-instance v7, Ljava/util/ArrayList;

    invoke-direct {v7}, Ljava/util/ArrayList;-><init>()V

    move-object v6, v7

    .line 558
    :cond_0
    const/4 v7, 0x1

    aget-object v7, v4, v7

    const-string v8, "\\|"

    invoke-virtual {v7, v8}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v7

    .line 559
    .local v7, "scenes":[Ljava/lang/String;
    array-length v8, v7

    move v9, v1

    :goto_1
    if-ge v9, v8, :cond_1

    aget-object v10, v7, v9

    .line 560
    .local v10, "scene":Ljava/lang/String;
    invoke-interface {v6, v10}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 559
    .end local v10    # "scene":Ljava/lang/String;
    add-int/lit8 v9, v9, 0x1

    goto :goto_1

    .line 562
    :cond_1
    invoke-interface {v6}, Ljava/util/List;->size()I

    move-result v8

    if-nez v8, :cond_2

    .line 563
    iget-object v8, p0, Lcom/miui/server/turbosched/FrameTurboAction;->mFrameTurboSceneWhiteListMap:Ljava/util/Map;

    invoke-interface {v8, v3}, Ljava/util/Map;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    goto :goto_2

    .line 565
    :cond_2
    iget-object v8, p0, Lcom/miui/server/turbosched/FrameTurboAction;->mFrameTurboSceneWhiteListMap:Ljava/util/Map;

    invoke-interface {v8, v3, v6}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 550
    .end local v3    # "pkgName":Ljava/lang/String;
    .end local v4    # "pkgSceneInfo":[Ljava/lang/String;
    .end local v5    # "pkgScene":Ljava/lang/String;
    .end local v6    # "sceneList":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;"
    .end local v7    # "scenes":[Ljava/lang/String;
    :cond_3
    :goto_2
    add-int/lit8 v2, v2, 0x1

    goto :goto_0

    .line 569
    :cond_4
    const-string v0, ""

    .line 570
    .local v0, "whiteListStr":Ljava/lang/String;
    iget-object v2, p0, Lcom/miui/server/turbosched/FrameTurboAction;->mFrameTurboSceneWhiteListMap:Ljava/util/Map;

    invoke-interface {v2}, Ljava/util/Map;->keySet()Ljava/util/Set;

    move-result-object v2

    invoke-interface {v2}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :cond_5
    :goto_3
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v5

    if-eqz v5, :cond_7

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Ljava/lang/String;

    .line 571
    .local v5, "key":Ljava/lang/String;
    iget-object v6, p0, Lcom/miui/server/turbosched/FrameTurboAction;->mFrameTurboSceneWhiteListMap:Ljava/util/Map;

    invoke-interface {v6, v5}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v6

    check-cast v6, Ljava/util/List;

    .line 572
    .local v6, "sceneWhiteList":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;"
    if-eqz v6, :cond_5

    invoke-interface {v6}, Ljava/util/List;->isEmpty()Z

    move-result v7

    if-eqz v7, :cond_6

    .line 573
    goto :goto_3

    .line 575
    :cond_6
    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v7, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    const-string/jumbo v8, "|"

    invoke-static {v8, v6}, Landroid/text/TextUtils;->join(Ljava/lang/CharSequence;Ljava/lang/Iterable;)Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    const-string v8, ", "

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    .line 576
    .end local v5    # "key":Ljava/lang/String;
    .end local v6    # "sceneWhiteList":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;"
    goto :goto_3

    .line 577
    :cond_7
    invoke-virtual {v0}, Ljava/lang/String;->length()I

    move-result v2

    if-lez v2, :cond_8

    .line 578
    invoke-virtual {v0}, Ljava/lang/String;->length()I

    move-result v2

    sub-int/2addr v2, v3

    invoke-virtual {v0, v1, v2}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v0

    .line 580
    :cond_8
    const-string v1, "persist.sys.turbosched.frame_turbo.scene_white_list"

    invoke-static {v1, v0}, Landroid/os/SystemProperties;->set(Ljava/lang/String;Ljava/lang/String;)V

    .line 582
    .end local v0    # "whiteListStr":Ljava/lang/String;
    :cond_9
    return-void
.end method
