public class com.miui.server.turbosched.GameTurboAction {
	 /* .source "GameTurboAction.java" */
	 /* # static fields */
	 private static final java.lang.String FLW_CHOOSE_LOAD_POLICY_PATH;
	 private static final java.lang.String FLW_CHOOSE_WAKE_STATE_PATH;
	 private static final java.lang.String FLW_DEBUG_PATH;
	 private static final java.lang.String FLW_DOWN_THRESH_MARGIN_PATH;
	 private static final java.lang.String FLW_ENABLE_PATH;
	 private static final java.lang.String FLW_FREQ_ENABLE_PATH;
	 private static final java.lang.String FLW_TARGET_DELTA_PATH;
	 private static final java.lang.String FLW_UP_THRESH_MARGIN_PATH;
	 private static final java.lang.String FLW_VERSION_PATH;
	 private static final java.lang.String SOC_DFLT_BW_ENABLE_PATH;
	 private static final java.lang.String SOC_DFLT_DEBUG_PATH;
	 private static final java.lang.String SOC_DFLT_LAT_ENABLE_PATH;
	 private static final java.lang.String SOC_DFLT_VERSION_PATH;
	 private static final java.lang.String SOC_GFLT_DEBUG_PATH;
	 private static final java.lang.String SOC_GFLT_ENABLE_PATH;
	 private static final java.lang.String TAG;
	 private static final java.lang.String VERSION;
	 /* # instance fields */
	 private Boolean isFLWAccess;
	 private Boolean isFLWEnable;
	 private Boolean isFLWFreqAccess;
	 private Boolean isFLWFreqEnable;
	 private Boolean isSoCGfltAccess;
	 private Boolean isSoCGfltEnable;
	 private Boolean isSocDfltBwAccess;
	 private Boolean isSocDfltBwEnable;
	 private Boolean isSocDfltLatAccess;
	 private Boolean isSocDfltLatEnable;
	 /* # direct methods */
	 protected com.miui.server.turbosched.GameTurboAction ( ) {
		 /* .locals 1 */
		 /* .line 41 */
		 /* invoke-direct {p0}, Ljava/lang/Object;-><init>()V */
		 /* .line 29 */
		 final String v0 = "/sys/module/migt/parameters/flw_enable"; // const-string v0, "/sys/module/migt/parameters/flw_enable"
		 v0 = 		 com.miui.server.turbosched.TurboSchedUtil .checkFileAccess ( v0 );
		 /* iput-boolean v0, p0, Lcom/miui/server/turbosched/GameTurboAction;->isFLWAccess:Z */
		 /* .line 30 */
		 final String v0 = "/sys/module/migt/parameters/flw_freq_enable"; // const-string v0, "/sys/module/migt/parameters/flw_freq_enable"
		 v0 = 		 com.miui.server.turbosched.TurboSchedUtil .checkFileAccess ( v0 );
		 /* iput-boolean v0, p0, Lcom/miui/server/turbosched/GameTurboAction;->isFLWFreqAccess:Z */
		 /* .line 31 */
		 final String v0 = "/sys/module/mist/parameters/gflt_enable"; // const-string v0, "/sys/module/mist/parameters/gflt_enable"
		 v0 = 		 com.miui.server.turbosched.TurboSchedUtil .checkFileAccess ( v0 );
		 /* iput-boolean v0, p0, Lcom/miui/server/turbosched/GameTurboAction;->isSoCGfltAccess:Z */
		 /* .line 32 */
		 final String v0 = "/sys/module/mist/parameters/dflt_bw_enable"; // const-string v0, "/sys/module/mist/parameters/dflt_bw_enable"
		 v0 = 		 com.miui.server.turbosched.TurboSchedUtil .checkFileAccess ( v0 );
		 /* iput-boolean v0, p0, Lcom/miui/server/turbosched/GameTurboAction;->isSocDfltBwAccess:Z */
		 /* .line 33 */
		 final String v0 = "/sys/module/mist/parameters/dflt_lat_enable"; // const-string v0, "/sys/module/mist/parameters/dflt_lat_enable"
		 v0 = 		 com.miui.server.turbosched.TurboSchedUtil .checkFileAccess ( v0 );
		 /* iput-boolean v0, p0, Lcom/miui/server/turbosched/GameTurboAction;->isSocDfltLatAccess:Z */
		 /* .line 35 */
		 int v0 = 0; // const/4 v0, 0x0
		 /* iput-boolean v0, p0, Lcom/miui/server/turbosched/GameTurboAction;->isFLWEnable:Z */
		 /* .line 36 */
		 /* iput-boolean v0, p0, Lcom/miui/server/turbosched/GameTurboAction;->isFLWFreqEnable:Z */
		 /* .line 37 */
		 /* iput-boolean v0, p0, Lcom/miui/server/turbosched/GameTurboAction;->isSoCGfltEnable:Z */
		 /* .line 38 */
		 /* iput-boolean v0, p0, Lcom/miui/server/turbosched/GameTurboAction;->isSocDfltBwEnable:Z */
		 /* .line 39 */
		 /* iput-boolean v0, p0, Lcom/miui/server/turbosched/GameTurboAction;->isSocDfltLatEnable:Z */
		 /* .line 42 */
		 /* invoke-direct {p0}, Lcom/miui/server/turbosched/GameTurboAction;->readConfigValue()V */
		 /* .line 43 */
		 return;
	 } // .end method
	 private Boolean parseDFLTDumpCommand ( java.io.FileDescriptor p0, java.io.PrintWriter p1, java.lang.String[] p2 ) {
		 /* .locals 11 */
		 /* .param p1, "fd" # Ljava/io/FileDescriptor; */
		 /* .param p2, "pw" # Ljava/io/PrintWriter; */
		 /* .param p3, "args" # [Ljava/lang/String; */
		 /* .line 232 */
		 /* array-length v0, p3 */
		 final String v1 = "invalid dflt command"; // const-string v1, "invalid dflt command"
		 int v2 = 1; // const/4 v2, 0x1
		 int v3 = 3; // const/4 v3, 0x3
		 /* if-ge v0, v3, :cond_0 */
		 /* .line 233 */
		 (( java.io.PrintWriter ) p2 ).println ( v1 ); // invoke-virtual {p2, v1}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V
		 /* .line 234 */
		 /* .line 237 */
	 } // :cond_0
	 /* aget-object v0, p3, v2 */
	 /* .line 238 */
	 /* .local v0, "cmd":Ljava/lang/String; */
	 final String v4 = "enable"; // const-string v4, "enable"
	 int v5 = 2; // const/4 v5, 0x2
	 /* aget-object v6, p3, v5 */
	 v4 = 	 (( java.lang.String ) v4 ).equals ( v6 ); // invoke-virtual {v4, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z
	 final String v6 = "1"; // const-string v6, "1"
	 final String v7 = "0"; // const-string v7, "0"
	 if ( v4 != null) { // if-eqz v4, :cond_1
		 /* move-object v4, v6 */
	 } // :cond_1
	 /* move-object v4, v7 */
	 /* .line 239 */
	 /* .local v4, "enable":Ljava/lang/String; */
} // :goto_0
int v8 = 0; // const/4 v8, 0x0
/* .line 240 */
/* .local v8, "errorMsg":Ljava/lang/String; */
v9 = (( java.lang.String ) v0 ).hashCode ( ); // invoke-virtual {v0}, Ljava/lang/String;->hashCode()I
/* sparse-switch v9, :sswitch_data_0 */
} // :cond_2
/* :sswitch_0 */
final String v9 = "debug"; // const-string v9, "debug"
v9 = (( java.lang.String ) v0 ).equals ( v9 ); // invoke-virtual {v0, v9}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z
if ( v9 != null) { // if-eqz v9, :cond_2
/* move v9, v5 */
/* :sswitch_1 */
final String v9 = "lat"; // const-string v9, "lat"
v9 = (( java.lang.String ) v0 ).equals ( v9 ); // invoke-virtual {v0, v9}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z
if ( v9 != null) { // if-eqz v9, :cond_2
	 /* move v9, v2 */
	 /* :sswitch_2 */
	 final String v9 = "bw"; // const-string v9, "bw"
	 v9 = 	 (( java.lang.String ) v0 ).equals ( v9 ); // invoke-virtual {v0, v9}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z
	 if ( v9 != null) { // if-eqz v9, :cond_2
		 int v9 = 0; // const/4 v9, 0x0
	 } // :goto_1
	 int v9 = -1; // const/4 v9, -0x1
} // :goto_2
/* packed-switch v9, :pswitch_data_0 */
/* goto/16 :goto_4 */
/* .line 255 */
/* :pswitch_0 */
final String v9 = "/sys/module/mist/parameters/dflt_debug"; // const-string v9, "/sys/module/mist/parameters/dflt_debug"
v10 = com.miui.server.turbosched.TurboSchedUtil .checkFileAccess ( v9 );
if ( v10 != null) { // if-eqz v10, :cond_7
	 /* .line 256 */
	 /* array-length v10, p3 */
	 /* if-ge v10, v3, :cond_3 */
	 /* .line 257 */
	 (( java.io.PrintWriter ) p2 ).println ( v1 ); // invoke-virtual {p2, v1}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V
	 /* .line 258 */
	 /* .line 261 */
} // :cond_3
/* aget-object v3, p3, v5 */
v3 = (( java.lang.String ) v7 ).equals ( v3 ); // invoke-virtual {v7, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z
/* if-nez v3, :cond_4 */
/* aget-object v3, p3, v5 */
v3 = (( java.lang.String ) v6 ).equals ( v3 ); // invoke-virtual {v6, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z
/* if-nez v3, :cond_4 */
/* .line 262 */
final String v1 = "invalid dflt debug command"; // const-string v1, "invalid dflt debug command"
(( java.io.PrintWriter ) p2 ).println ( v1 ); // invoke-virtual {p2, v1}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V
/* .line 263 */
/* .line 265 */
} // :cond_4
/* aget-object v3, p3, v5 */
com.miui.server.turbosched.TurboSchedUtil .writeValueToFile ( v9,v3 );
/* goto/16 :goto_4 */
/* .line 249 */
/* :pswitch_1 */
/* iget-boolean v1, p0, Lcom/miui/server/turbosched/GameTurboAction;->isSocDfltLatAccess:Z */
if ( v1 != null) { // if-eqz v1, :cond_5
/* .line 250 */
final String v1 = "/sys/module/mist/parameters/dflt_lat_enable"; // const-string v1, "/sys/module/mist/parameters/dflt_lat_enable"
com.miui.server.turbosched.TurboSchedUtil .writeValueToFile ( v1,v4 );
/* .line 251 */
v1 = com.miui.server.turbosched.TurboSchedUtil .readBooleanValueFromFile ( v1 );
/* iput-boolean v1, p0, Lcom/miui/server/turbosched/GameTurboAction;->isSocDfltLatEnable:Z */
/* .line 242 */
/* :pswitch_2 */
/* iget-boolean v1, p0, Lcom/miui/server/turbosched/GameTurboAction;->isSocDfltBwAccess:Z */
if ( v1 != null) { // if-eqz v1, :cond_5
	 /* .line 243 */
	 final String v1 = "/sys/module/mist/parameters/dflt_bw_enable"; // const-string v1, "/sys/module/mist/parameters/dflt_bw_enable"
	 com.miui.server.turbosched.TurboSchedUtil .writeValueToFile ( v1,v4 );
	 /* .line 244 */
	 v1 = 	 com.miui.server.turbosched.TurboSchedUtil .readBooleanValueFromFile ( v1 );
	 /* iput-boolean v1, p0, Lcom/miui/server/turbosched/GameTurboAction;->isSocDfltBwEnable:Z */
	 /* .line 245 */
	 /* new-instance v1, Ljava/lang/StringBuilder; */
	 /* invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V */
	 final String v3 = "isSocDfltBwEnable: "; // const-string v3, "isSocDfltBwEnable: "
	 (( java.lang.StringBuilder ) v1 ).append ( v3 ); // invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
	 /* iget-boolean v3, p0, Lcom/miui/server/turbosched/GameTurboAction;->isSocDfltBwEnable:Z */
	 (( java.lang.StringBuilder ) v1 ).append ( v3 ); // invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;
	 (( java.lang.StringBuilder ) v1 ).toString ( ); // invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
	 final String v3 = "TurboSched_GameTurboAction"; // const-string v3, "TurboSched_GameTurboAction"
	 android.util.Slog .d ( v3,v1 );
	 /* .line 272 */
} // :cond_5
} // :goto_3
final String v1 = " "; // const-string v1, " "
final String v3 = "dflt "; // const-string v3, "dflt "
if ( v8 != null) { // if-eqz v8, :cond_6
/* .line 273 */
/* new-instance v6, Ljava/lang/StringBuilder; */
/* invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V */
(( java.lang.StringBuilder ) v6 ).append ( v3 ); // invoke-virtual {v6, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v3 ).append ( v0 ); // invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v3 ).append ( v1 ); // invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
/* aget-object v3, p3, v5 */
(( java.lang.StringBuilder ) v1 ).append ( v3 ); // invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
final String v3 = " failed: "; // const-string v3, " failed: "
(( java.lang.StringBuilder ) v1 ).append ( v3 ); // invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v1 ).append ( v8 ); // invoke-virtual {v1, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v1 ).toString ( ); // invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
(( java.io.PrintWriter ) p2 ).println ( v1 ); // invoke-virtual {p2, v1}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V
/* .line 274 */
/* .line 277 */
} // :cond_6
/* new-instance v6, Ljava/lang/StringBuilder; */
/* invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V */
(( java.lang.StringBuilder ) v6 ).append ( v3 ); // invoke-virtual {v6, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v3 ).append ( v0 ); // invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v3 ).append ( v1 ); // invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
/* aget-object v3, p3, v5 */
(( java.lang.StringBuilder ) v1 ).append ( v3 ); // invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
final String v3 = " success"; // const-string v3, " success"
(( java.lang.StringBuilder ) v1 ).append ( v3 ); // invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v1 ).toString ( ); // invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
(( java.io.PrintWriter ) p2 ).println ( v1 ); // invoke-virtual {p2, v1}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V
/* .line 278 */
final String v1 = "--------------------current status-----------------------"; // const-string v1, "--------------------current status-----------------------"
(( java.io.PrintWriter ) p2 ).println ( v1 ); // invoke-virtual {p2, v1}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V
/* .line 279 */
/* invoke-direct {p0, p2}, Lcom/miui/server/turbosched/GameTurboAction;->printSocDfltConfig(Ljava/io/PrintWriter;)V */
/* .line 280 */
/* .line 268 */
} // :cond_7
} // :goto_4
(( java.io.PrintWriter ) p2 ).println ( v1 ); // invoke-virtual {p2, v1}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V
/* .line 269 */
/* :sswitch_data_0 */
/* .sparse-switch */
/* 0xc55 -> :sswitch_2 */
/* 0x1a19f -> :sswitch_1 */
/* 0x5b09653 -> :sswitch_0 */
} // .end sparse-switch
/* :pswitch_data_0 */
/* .packed-switch 0x0 */
/* :pswitch_2 */
/* :pswitch_1 */
/* :pswitch_0 */
} // .end packed-switch
} // .end method
private Boolean parseDumpCommand ( java.io.FileDescriptor p0, java.io.PrintWriter p1, java.lang.String[] p2 ) {
/* .locals 4 */
/* .param p1, "fd" # Ljava/io/FileDescriptor; */
/* .param p2, "pw" # Ljava/io/PrintWriter; */
/* .param p3, "args" # [Ljava/lang/String; */
/* .line 102 */
/* array-length v0, p3 */
int v1 = 0; // const/4 v1, 0x0
int v2 = 1; // const/4 v2, 0x1
/* if-ge v0, v2, :cond_0 */
/* .line 103 */
/* .line 106 */
} // :cond_0
/* aget-object v0, p3, v1 */
/* .line 107 */
/* .local v0, "cmd":Ljava/lang/String; */
v3 = (( java.lang.String ) v0 ).hashCode ( ); // invoke-virtual {v0}, Ljava/lang/String;->hashCode()I
/* sparse-switch v3, :sswitch_data_0 */
} // :cond_1
/* :sswitch_0 */
final String v3 = "gflt"; // const-string v3, "gflt"
v3 = (( java.lang.String ) v0 ).equals ( v3 ); // invoke-virtual {v0, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z
if ( v3 != null) { // if-eqz v3, :cond_1
/* :sswitch_1 */
final String v2 = "dflt"; // const-string v2, "dflt"
v2 = (( java.lang.String ) v0 ).equals ( v2 ); // invoke-virtual {v0, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z
if ( v2 != null) { // if-eqz v2, :cond_1
int v2 = 2; // const/4 v2, 0x2
/* :sswitch_2 */
final String v2 = "flw"; // const-string v2, "flw"
v2 = (( java.lang.String ) v0 ).equals ( v2 ); // invoke-virtual {v0, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z
if ( v2 != null) { // if-eqz v2, :cond_1
/* move v2, v1 */
} // :goto_0
int v2 = -1; // const/4 v2, -0x1
} // :goto_1
/* packed-switch v2, :pswitch_data_0 */
/* .line 115 */
/* .line 113 */
/* :pswitch_0 */
v1 = /* invoke-direct {p0, p1, p2, p3}, Lcom/miui/server/turbosched/GameTurboAction;->parseDFLTDumpCommand(Ljava/io/FileDescriptor;Ljava/io/PrintWriter;[Ljava/lang/String;)Z */
/* .line 111 */
/* :pswitch_1 */
v1 = /* invoke-direct {p0, p1, p2, p3}, Lcom/miui/server/turbosched/GameTurboAction;->parseGFLTDumpCommand(Ljava/io/FileDescriptor;Ljava/io/PrintWriter;[Ljava/lang/String;)Z */
/* .line 109 */
/* :pswitch_2 */
v1 = /* invoke-direct {p0, p1, p2, p3}, Lcom/miui/server/turbosched/GameTurboAction;->parseFLWDumpCommand(Ljava/io/FileDescriptor;Ljava/io/PrintWriter;[Ljava/lang/String;)Z */
/* nop */
/* :sswitch_data_0 */
/* .sparse-switch */
/* 0x18c71 -> :sswitch_2 */
/* 0x2f018a -> :sswitch_1 */
/* 0x305ea7 -> :sswitch_0 */
} // .end sparse-switch
/* :pswitch_data_0 */
/* .packed-switch 0x0 */
/* :pswitch_2 */
/* :pswitch_1 */
/* :pswitch_0 */
} // .end packed-switch
} // .end method
private Boolean parseFLWDumpCommand ( java.io.FileDescriptor p0, java.io.PrintWriter p1, java.lang.String[] p2 ) {
/* .locals 9 */
/* .param p1, "fd" # Ljava/io/FileDescriptor; */
/* .param p2, "pw" # Ljava/io/PrintWriter; */
/* .param p3, "args" # [Ljava/lang/String; */
/* .line 121 */
/* array-length v0, p3 */
final String v1 = "invalid flw command"; // const-string v1, "invalid flw command"
int v2 = 1; // const/4 v2, 0x1
int v3 = 2; // const/4 v3, 0x2
/* if-ge v0, v3, :cond_0 */
/* .line 122 */
(( java.io.PrintWriter ) p2 ).println ( v1 ); // invoke-virtual {p2, v1}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V
/* .line 123 */
/* .line 126 */
} // :cond_0
/* aget-object v0, p3, v2 */
/* .line 127 */
/* .local v0, "cmd":Ljava/lang/String; */
int v4 = 0; // const/4 v4, 0x0
/* .line 128 */
/* .local v4, "errorMsg":Ljava/lang/String; */
v5 = (( java.lang.String ) v0 ).hashCode ( ); // invoke-virtual {v0}, Ljava/lang/String;->hashCode()I
int v6 = 3; // const/4 v6, 0x3
/* sparse-switch v5, :sswitch_data_0 */
} // :cond_1
/* :sswitch_0 */
final String v5 = "disable"; // const-string v5, "disable"
v5 = (( java.lang.String ) v0 ).equals ( v5 ); // invoke-virtual {v0, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z
if ( v5 != null) { // if-eqz v5, :cond_1
/* move v5, v2 */
/* :sswitch_1 */
final String v5 = "delta"; // const-string v5, "delta"
v5 = (( java.lang.String ) v0 ).equals ( v5 ); // invoke-virtual {v0, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z
if ( v5 != null) { // if-eqz v5, :cond_1
/* move v5, v3 */
/* :sswitch_2 */
final String v5 = "debug"; // const-string v5, "debug"
v5 = (( java.lang.String ) v0 ).equals ( v5 ); // invoke-virtual {v0, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z
if ( v5 != null) { // if-eqz v5, :cond_1
/* move v5, v6 */
/* :sswitch_3 */
final String v5 = "enable"; // const-string v5, "enable"
v5 = (( java.lang.String ) v0 ).equals ( v5 ); // invoke-virtual {v0, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z
if ( v5 != null) { // if-eqz v5, :cond_1
int v5 = 0; // const/4 v5, 0x0
} // :goto_0
int v5 = -1; // const/4 v5, -0x1
} // :goto_1
final String v7 = "1"; // const-string v7, "1"
final String v8 = "/sys/module/migt/parameters/flw_enable"; // const-string v8, "/sys/module/migt/parameters/flw_enable"
/* packed-switch v5, :pswitch_data_0 */
/* goto/16 :goto_3 */
/* .line 151 */
/* :pswitch_0 */
final String v5 = "/sys/module/migt/parameters/flw_debug"; // const-string v5, "/sys/module/migt/parameters/flw_debug"
v8 = com.miui.server.turbosched.TurboSchedUtil .checkFileAccess ( v5 );
if ( v8 != null) { // if-eqz v8, :cond_7
/* .line 152 */
/* array-length v8, p3 */
/* if-ge v8, v6, :cond_2 */
/* .line 153 */
(( java.io.PrintWriter ) p2 ).println ( v1 ); // invoke-virtual {p2, v1}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V
/* .line 154 */
/* .line 157 */
} // :cond_2
final String v6 = "0"; // const-string v6, "0"
/* aget-object v8, p3, v3 */
v6 = (( java.lang.String ) v6 ).equals ( v8 ); // invoke-virtual {v6, v8}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z
/* if-nez v6, :cond_3 */
/* aget-object v6, p3, v3 */
v6 = (( java.lang.String ) v7 ).equals ( v6 ); // invoke-virtual {v7, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z
/* if-nez v6, :cond_3 */
/* .line 158 */
final String v1 = "invalid flw debug command"; // const-string v1, "invalid flw debug command"
(( java.io.PrintWriter ) p2 ).println ( v1 ); // invoke-virtual {p2, v1}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V
/* .line 159 */
/* .line 161 */
} // :cond_3
/* aget-object v3, p3, v3 */
com.miui.server.turbosched.TurboSchedUtil .writeValueToFile ( v5,v3 );
/* goto/16 :goto_3 */
/* .line 142 */
/* :pswitch_1 */
final String v5 = "/sys/module/migt/parameters/target_delta"; // const-string v5, "/sys/module/migt/parameters/target_delta"
v7 = com.miui.server.turbosched.TurboSchedUtil .checkFileAccess ( v5 );
if ( v7 != null) { // if-eqz v7, :cond_5
/* .line 143 */
/* array-length v7, p3 */
/* if-ge v7, v6, :cond_4 */
/* .line 144 */
(( java.io.PrintWriter ) p2 ).println ( v1 ); // invoke-virtual {p2, v1}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V
/* .line 145 */
/* .line 147 */
} // :cond_4
/* aget-object v1, p3, v3 */
com.miui.server.turbosched.TurboSchedUtil .writeValueToFile ( v5,v1 );
/* .line 136 */
/* :pswitch_2 */
/* iget-boolean v1, p0, Lcom/miui/server/turbosched/GameTurboAction;->isFLWFreqAccess:Z */
if ( v1 != null) { // if-eqz v1, :cond_5
/* .line 137 */
final String v1 = "/sys/module/migt/parameters/flw_freq_enable"; // const-string v1, "/sys/module/migt/parameters/flw_freq_enable"
com.miui.server.turbosched.TurboSchedUtil .writeValueToFile ( v1,v7 );
/* .line 138 */
v1 = com.miui.server.turbosched.TurboSchedUtil .readBooleanValueFromFile ( v8 );
/* iput-boolean v1, p0, Lcom/miui/server/turbosched/GameTurboAction;->isFLWEnable:Z */
/* .line 130 */
/* :pswitch_3 */
/* iget-boolean v1, p0, Lcom/miui/server/turbosched/GameTurboAction;->isFLWAccess:Z */
if ( v1 != null) { // if-eqz v1, :cond_5
/* .line 131 */
com.miui.server.turbosched.TurboSchedUtil .writeValueToFile ( v8,v7 );
/* .line 132 */
v1 = com.miui.server.turbosched.TurboSchedUtil .readBooleanValueFromFile ( v8 );
/* iput-boolean v1, p0, Lcom/miui/server/turbosched/GameTurboAction;->isFLWEnable:Z */
/* .line 168 */
} // :cond_5
} // :goto_2
final String v1 = "flw "; // const-string v1, "flw "
if ( v4 != null) { // if-eqz v4, :cond_6
/* .line 169 */
/* new-instance v3, Ljava/lang/StringBuilder; */
/* invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V */
(( java.lang.StringBuilder ) v3 ).append ( v1 ); // invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v1 ).append ( v0 ); // invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
final String v3 = " failed: "; // const-string v3, " failed: "
(( java.lang.StringBuilder ) v1 ).append ( v3 ); // invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v1 ).append ( v4 ); // invoke-virtual {v1, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v1 ).toString ( ); // invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
(( java.io.PrintWriter ) p2 ).println ( v1 ); // invoke-virtual {p2, v1}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V
/* .line 170 */
/* .line 173 */
} // :cond_6
/* new-instance v3, Ljava/lang/StringBuilder; */
/* invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V */
(( java.lang.StringBuilder ) v3 ).append ( v1 ); // invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v1 ).append ( v0 ); // invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
final String v3 = " success"; // const-string v3, " success"
(( java.lang.StringBuilder ) v1 ).append ( v3 ); // invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v1 ).toString ( ); // invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
(( java.io.PrintWriter ) p2 ).println ( v1 ); // invoke-virtual {p2, v1}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V
/* .line 174 */
final String v1 = "--------------------current status-----------------------"; // const-string v1, "--------------------current status-----------------------"
(( java.io.PrintWriter ) p2 ).println ( v1 ); // invoke-virtual {p2, v1}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V
/* .line 175 */
/* invoke-direct {p0, p2}, Lcom/miui/server/turbosched/GameTurboAction;->printFLWConfig(Ljava/io/PrintWriter;)V */
/* .line 176 */
/* .line 164 */
} // :cond_7
} // :goto_3
(( java.io.PrintWriter ) p2 ).println ( v1 ); // invoke-virtual {p2, v1}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V
/* .line 165 */
/* nop */
/* :sswitch_data_0 */
/* .sparse-switch */
/* -0x4d6ada7d -> :sswitch_3 */
/* 0x5b09653 -> :sswitch_2 */
/* 0x5b0bbb8 -> :sswitch_1 */
/* 0x639e22e8 -> :sswitch_0 */
} // .end sparse-switch
/* :pswitch_data_0 */
/* .packed-switch 0x0 */
/* :pswitch_3 */
/* :pswitch_2 */
/* :pswitch_1 */
/* :pswitch_0 */
} // .end packed-switch
} // .end method
private Boolean parseGFLTDumpCommand ( java.io.FileDescriptor p0, java.io.PrintWriter p1, java.lang.String[] p2 ) {
/* .locals 10 */
/* .param p1, "fd" # Ljava/io/FileDescriptor; */
/* .param p2, "pw" # Ljava/io/PrintWriter; */
/* .param p3, "args" # [Ljava/lang/String; */
/* .line 181 */
/* array-length v0, p3 */
final String v1 = "invalid gflt command"; // const-string v1, "invalid gflt command"
int v2 = 1; // const/4 v2, 0x1
int v3 = 2; // const/4 v3, 0x2
/* if-ge v0, v3, :cond_0 */
/* .line 182 */
(( java.io.PrintWriter ) p2 ).println ( v1 ); // invoke-virtual {p2, v1}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V
/* .line 183 */
/* .line 186 */
} // :cond_0
/* aget-object v0, p3, v2 */
/* .line 187 */
/* .local v0, "cmd":Ljava/lang/String; */
int v4 = 0; // const/4 v4, 0x0
/* .line 188 */
/* .local v4, "errorMsg":Ljava/lang/String; */
v5 = (( java.lang.String ) v0 ).hashCode ( ); // invoke-virtual {v0}, Ljava/lang/String;->hashCode()I
/* sparse-switch v5, :sswitch_data_0 */
} // :cond_1
/* :sswitch_0 */
final String v5 = "disable"; // const-string v5, "disable"
v5 = (( java.lang.String ) v0 ).equals ( v5 ); // invoke-virtual {v0, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z
if ( v5 != null) { // if-eqz v5, :cond_1
/* move v5, v2 */
/* :sswitch_1 */
final String v5 = "debug"; // const-string v5, "debug"
v5 = (( java.lang.String ) v0 ).equals ( v5 ); // invoke-virtual {v0, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z
if ( v5 != null) { // if-eqz v5, :cond_1
/* move v5, v3 */
/* :sswitch_2 */
final String v5 = "enable"; // const-string v5, "enable"
v5 = (( java.lang.String ) v0 ).equals ( v5 ); // invoke-virtual {v0, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z
if ( v5 != null) { // if-eqz v5, :cond_1
int v5 = 0; // const/4 v5, 0x0
} // :goto_0
int v5 = -1; // const/4 v5, -0x1
} // :goto_1
final String v6 = "0"; // const-string v6, "0"
final String v7 = "1"; // const-string v7, "1"
final String v8 = "/sys/module/mist/parameters/gflt_enable"; // const-string v8, "/sys/module/mist/parameters/gflt_enable"
/* packed-switch v5, :pswitch_data_0 */
/* goto/16 :goto_3 */
/* .line 202 */
/* :pswitch_0 */
final String v5 = "/sys/module/mist/parameters/gflt_debug"; // const-string v5, "/sys/module/mist/parameters/gflt_debug"
v8 = com.miui.server.turbosched.TurboSchedUtil .checkFileAccess ( v5 );
if ( v8 != null) { // if-eqz v8, :cond_6
/* .line 203 */
/* array-length v8, p3 */
int v9 = 3; // const/4 v9, 0x3
/* if-ge v8, v9, :cond_2 */
/* .line 204 */
(( java.io.PrintWriter ) p2 ).println ( v1 ); // invoke-virtual {p2, v1}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V
/* .line 205 */
/* .line 208 */
} // :cond_2
/* aget-object v8, p3, v3 */
v6 = (( java.lang.String ) v6 ).equals ( v8 ); // invoke-virtual {v6, v8}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z
/* if-nez v6, :cond_3 */
/* aget-object v6, p3, v3 */
v6 = (( java.lang.String ) v7 ).equals ( v6 ); // invoke-virtual {v7, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z
/* if-nez v6, :cond_3 */
/* .line 209 */
final String v1 = "invalid gflt debug command"; // const-string v1, "invalid gflt debug command"
(( java.io.PrintWriter ) p2 ).println ( v1 ); // invoke-virtual {p2, v1}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V
/* .line 210 */
/* .line 212 */
} // :cond_3
/* aget-object v3, p3, v3 */
com.miui.server.turbosched.TurboSchedUtil .writeValueToFile ( v5,v3 );
/* .line 196 */
/* :pswitch_1 */
/* iget-boolean v1, p0, Lcom/miui/server/turbosched/GameTurboAction;->isSoCGfltAccess:Z */
if ( v1 != null) { // if-eqz v1, :cond_4
/* .line 197 */
com.miui.server.turbosched.TurboSchedUtil .writeValueToFile ( v8,v6 );
/* .line 198 */
v1 = com.miui.server.turbosched.TurboSchedUtil .readBooleanValueFromFile ( v8 );
/* iput-boolean v1, p0, Lcom/miui/server/turbosched/GameTurboAction;->isSoCGfltEnable:Z */
/* .line 190 */
/* :pswitch_2 */
/* iget-boolean v1, p0, Lcom/miui/server/turbosched/GameTurboAction;->isSoCGfltAccess:Z */
if ( v1 != null) { // if-eqz v1, :cond_4
/* .line 191 */
com.miui.server.turbosched.TurboSchedUtil .writeValueToFile ( v8,v7 );
/* .line 192 */
v1 = com.miui.server.turbosched.TurboSchedUtil .readBooleanValueFromFile ( v8 );
/* iput-boolean v1, p0, Lcom/miui/server/turbosched/GameTurboAction;->isSoCGfltEnable:Z */
/* .line 219 */
} // :cond_4
} // :goto_2
final String v1 = "gflt "; // const-string v1, "gflt "
if ( v4 != null) { // if-eqz v4, :cond_5
/* .line 220 */
/* new-instance v3, Ljava/lang/StringBuilder; */
/* invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V */
(( java.lang.StringBuilder ) v3 ).append ( v1 ); // invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v1 ).append ( v0 ); // invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
final String v3 = " failed: "; // const-string v3, " failed: "
(( java.lang.StringBuilder ) v1 ).append ( v3 ); // invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v1 ).append ( v4 ); // invoke-virtual {v1, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v1 ).toString ( ); // invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
(( java.io.PrintWriter ) p2 ).println ( v1 ); // invoke-virtual {p2, v1}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V
/* .line 221 */
/* .line 224 */
} // :cond_5
/* new-instance v3, Ljava/lang/StringBuilder; */
/* invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V */
(( java.lang.StringBuilder ) v3 ).append ( v1 ); // invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v1 ).append ( v0 ); // invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
final String v3 = " success"; // const-string v3, " success"
(( java.lang.StringBuilder ) v1 ).append ( v3 ); // invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v1 ).toString ( ); // invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
(( java.io.PrintWriter ) p2 ).println ( v1 ); // invoke-virtual {p2, v1}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V
/* .line 225 */
final String v1 = "--------------------current status-----------------------"; // const-string v1, "--------------------current status-----------------------"
(( java.io.PrintWriter ) p2 ).println ( v1 ); // invoke-virtual {p2, v1}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V
/* .line 226 */
/* invoke-direct {p0, p2}, Lcom/miui/server/turbosched/GameTurboAction;->printSoCGfltConfig(Ljava/io/PrintWriter;)V */
/* .line 227 */
/* .line 215 */
} // :cond_6
} // :goto_3
(( java.io.PrintWriter ) p2 ).println ( v1 ); // invoke-virtual {p2, v1}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V
/* .line 216 */
/* :sswitch_data_0 */
/* .sparse-switch */
/* -0x4d6ada7d -> :sswitch_2 */
/* 0x5b09653 -> :sswitch_1 */
/* 0x639e22e8 -> :sswitch_0 */
} // .end sparse-switch
/* :pswitch_data_0 */
/* .packed-switch 0x0 */
/* :pswitch_2 */
/* :pswitch_1 */
/* :pswitch_0 */
} // .end packed-switch
} // .end method
private void printFLWConfig ( java.io.PrintWriter p0 ) {
/* .locals 2 */
/* .param p1, "pw" # Ljava/io/PrintWriter; */
/* .line 79 */
final String v0 = "flw :"; // const-string v0, "flw :"
(( java.io.PrintWriter ) p1 ).println ( v0 ); // invoke-virtual {p1, v0}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V
/* .line 80 */
/* new-instance v0, Ljava/lang/StringBuilder; */
/* invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V */
final String v1 = " enable: "; // const-string v1, " enable: "
(( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
/* iget-boolean v1, p0, Lcom/miui/server/turbosched/GameTurboAction;->isFLWEnable:Z */
(( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;
final String v1 = ", version: "; // const-string v1, ", version: "
(( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
final String v1 = "/sys/module/migt/parameters/version"; // const-string v1, "/sys/module/migt/parameters/version"
com.miui.server.turbosched.TurboSchedUtil .readValueFromFile ( v1 );
(( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v0 ).toString ( ); // invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
(( java.io.PrintWriter ) p1 ).println ( v0 ); // invoke-virtual {p1, v0}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V
/* .line 81 */
/* new-instance v0, Ljava/lang/StringBuilder; */
/* invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V */
final String v1 = " freq enable: "; // const-string v1, " freq enable: "
(( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
/* iget-boolean v1, p0, Lcom/miui/server/turbosched/GameTurboAction;->isFLWFreqEnable:Z */
(( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v0 ).toString ( ); // invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
(( java.io.PrintWriter ) p1 ).println ( v0 ); // invoke-virtual {p1, v0}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V
/* .line 82 */
/* new-instance v0, Ljava/lang/StringBuilder; */
/* invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V */
final String v1 = " target delta: "; // const-string v1, " target delta: "
(( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
final String v1 = "/sys/module/migt/parameters/target_delta"; // const-string v1, "/sys/module/migt/parameters/target_delta"
v1 = com.miui.server.turbosched.TurboSchedUtil .readBooleanValueFromFile ( v1 );
(( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v0 ).toString ( ); // invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
(( java.io.PrintWriter ) p1 ).println ( v0 ); // invoke-virtual {p1, v0}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V
/* .line 83 */
/* new-instance v0, Ljava/lang/StringBuilder; */
/* invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V */
final String v1 = " up thresh margin: "; // const-string v1, " up thresh margin: "
(( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
final String v1 = "/sys/module/migt/parameters/up_thresh_margin"; // const-string v1, "/sys/module/migt/parameters/up_thresh_margin"
com.miui.server.turbosched.TurboSchedUtil .readValueFromFile ( v1 );
(( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v0 ).toString ( ); // invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
(( java.io.PrintWriter ) p1 ).println ( v0 ); // invoke-virtual {p1, v0}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V
/* .line 84 */
/* new-instance v0, Ljava/lang/StringBuilder; */
/* invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V */
final String v1 = " down thresh margin: "; // const-string v1, " down thresh margin: "
(( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
final String v1 = "/sys/module/migt/parameters/down_thresh_margin"; // const-string v1, "/sys/module/migt/parameters/down_thresh_margin"
com.miui.server.turbosched.TurboSchedUtil .readValueFromFile ( v1 );
(( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v0 ).toString ( ); // invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
(( java.io.PrintWriter ) p1 ).println ( v0 ); // invoke-virtual {p1, v0}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V
/* .line 85 */
/* new-instance v0, Ljava/lang/StringBuilder; */
/* invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V */
final String v1 = " choose wake state: "; // const-string v1, " choose wake state: "
(( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
final String v1 = "/sys/module/migt/parameters/choose_wake_state"; // const-string v1, "/sys/module/migt/parameters/choose_wake_state"
com.miui.server.turbosched.TurboSchedUtil .readValueFromFile ( v1 );
(( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v0 ).toString ( ); // invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
(( java.io.PrintWriter ) p1 ).println ( v0 ); // invoke-virtual {p1, v0}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V
/* .line 86 */
/* new-instance v0, Ljava/lang/StringBuilder; */
/* invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V */
final String v1 = " choose load policy: "; // const-string v1, " choose load policy: "
(( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
final String v1 = "/sys/module/migt/parameters/choose_load_policy"; // const-string v1, "/sys/module/migt/parameters/choose_load_policy"
com.miui.server.turbosched.TurboSchedUtil .readValueFromFile ( v1 );
(( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v0 ).toString ( ); // invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
(( java.io.PrintWriter ) p1 ).println ( v0 ); // invoke-virtual {p1, v0}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V
/* .line 87 */
return;
} // .end method
private void printSoCGfltConfig ( java.io.PrintWriter p0 ) {
/* .locals 2 */
/* .param p1, "pw" # Ljava/io/PrintWriter; */
/* .line 90 */
final String v0 = "gflt :"; // const-string v0, "gflt :"
(( java.io.PrintWriter ) p1 ).println ( v0 ); // invoke-virtual {p1, v0}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V
/* .line 91 */
/* new-instance v0, Ljava/lang/StringBuilder; */
/* invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V */
final String v1 = " enable: "; // const-string v1, " enable: "
(( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
/* iget-boolean v1, p0, Lcom/miui/server/turbosched/GameTurboAction;->isSoCGfltEnable:Z */
(( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v0 ).toString ( ); // invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
(( java.io.PrintWriter ) p1 ).println ( v0 ); // invoke-virtual {p1, v0}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V
/* .line 92 */
return;
} // .end method
private void printSocDfltConfig ( java.io.PrintWriter p0 ) {
/* .locals 2 */
/* .param p1, "pw" # Ljava/io/PrintWriter; */
/* .line 95 */
final String v0 = "dflt :"; // const-string v0, "dflt :"
(( java.io.PrintWriter ) p1 ).println ( v0 ); // invoke-virtual {p1, v0}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V
/* .line 96 */
/* new-instance v0, Ljava/lang/StringBuilder; */
/* invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V */
final String v1 = " version: "; // const-string v1, " version: "
(( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
final String v1 = "/sys/module/mist/parameters/mist_version"; // const-string v1, "/sys/module/mist/parameters/mist_version"
com.miui.server.turbosched.TurboSchedUtil .readValueFromFile ( v1 );
(( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v0 ).toString ( ); // invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
(( java.io.PrintWriter ) p1 ).println ( v0 ); // invoke-virtual {p1, v0}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V
/* .line 97 */
/* new-instance v0, Ljava/lang/StringBuilder; */
/* invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V */
final String v1 = " bw enable: "; // const-string v1, " bw enable: "
(( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
/* iget-boolean v1, p0, Lcom/miui/server/turbosched/GameTurboAction;->isSocDfltBwEnable:Z */
(( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v0 ).toString ( ); // invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
(( java.io.PrintWriter ) p1 ).println ( v0 ); // invoke-virtual {p1, v0}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V
/* .line 98 */
/* new-instance v0, Ljava/lang/StringBuilder; */
/* invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V */
final String v1 = " lat enable: "; // const-string v1, " lat enable: "
(( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
/* iget-boolean v1, p0, Lcom/miui/server/turbosched/GameTurboAction;->isSocDfltLatEnable:Z */
(( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v0 ).toString ( ); // invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
(( java.io.PrintWriter ) p1 ).println ( v0 ); // invoke-virtual {p1, v0}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V
/* .line 99 */
return;
} // .end method
private void readConfigValue ( ) {
/* .locals 1 */
/* .line 284 */
/* iget-boolean v0, p0, Lcom/miui/server/turbosched/GameTurboAction;->isFLWAccess:Z */
if ( v0 != null) { // if-eqz v0, :cond_0
/* .line 285 */
final String v0 = "/sys/module/migt/parameters/flw_enable"; // const-string v0, "/sys/module/migt/parameters/flw_enable"
v0 = com.miui.server.turbosched.TurboSchedUtil .readBooleanValueFromFile ( v0 );
/* iput-boolean v0, p0, Lcom/miui/server/turbosched/GameTurboAction;->isFLWEnable:Z */
/* .line 287 */
} // :cond_0
/* iget-boolean v0, p0, Lcom/miui/server/turbosched/GameTurboAction;->isFLWFreqAccess:Z */
if ( v0 != null) { // if-eqz v0, :cond_1
/* .line 288 */
final String v0 = "/sys/module/migt/parameters/flw_freq_enable"; // const-string v0, "/sys/module/migt/parameters/flw_freq_enable"
v0 = com.miui.server.turbosched.TurboSchedUtil .readBooleanValueFromFile ( v0 );
/* iput-boolean v0, p0, Lcom/miui/server/turbosched/GameTurboAction;->isFLWFreqEnable:Z */
/* .line 290 */
} // :cond_1
/* iget-boolean v0, p0, Lcom/miui/server/turbosched/GameTurboAction;->isSoCGfltAccess:Z */
if ( v0 != null) { // if-eqz v0, :cond_2
/* .line 291 */
final String v0 = "/sys/module/mist/parameters/gflt_enable"; // const-string v0, "/sys/module/mist/parameters/gflt_enable"
v0 = com.miui.server.turbosched.TurboSchedUtil .readBooleanValueFromFile ( v0 );
/* iput-boolean v0, p0, Lcom/miui/server/turbosched/GameTurboAction;->isSoCGfltEnable:Z */
/* .line 293 */
} // :cond_2
/* iget-boolean v0, p0, Lcom/miui/server/turbosched/GameTurboAction;->isSocDfltBwAccess:Z */
if ( v0 != null) { // if-eqz v0, :cond_3
/* .line 294 */
final String v0 = "/sys/module/mist/parameters/dflt_bw_enable"; // const-string v0, "/sys/module/mist/parameters/dflt_bw_enable"
v0 = com.miui.server.turbosched.TurboSchedUtil .readBooleanValueFromFile ( v0 );
/* iput-boolean v0, p0, Lcom/miui/server/turbosched/GameTurboAction;->isSocDfltBwEnable:Z */
/* .line 296 */
} // :cond_3
/* iget-boolean v0, p0, Lcom/miui/server/turbosched/GameTurboAction;->isSocDfltLatAccess:Z */
if ( v0 != null) { // if-eqz v0, :cond_4
/* .line 297 */
final String v0 = "/sys/module/mist/parameters/dflt_lat_enable"; // const-string v0, "/sys/module/mist/parameters/dflt_lat_enable"
v0 = com.miui.server.turbosched.TurboSchedUtil .readBooleanValueFromFile ( v0 );
/* iput-boolean v0, p0, Lcom/miui/server/turbosched/GameTurboAction;->isSocDfltLatEnable:Z */
/* .line 299 */
} // :cond_4
return;
} // .end method
/* # virtual methods */
protected Boolean dump ( java.io.FileDescriptor p0, java.io.PrintWriter p1, java.lang.String[] p2 ) {
/* .locals 4 */
/* .param p1, "fd" # Ljava/io/FileDescriptor; */
/* .param p2, "pw" # Ljava/io/PrintWriter; */
/* .param p3, "args" # [Ljava/lang/String; */
/* .line 46 */
v0 = /* invoke-direct {p0, p1, p2, p3}, Lcom/miui/server/turbosched/GameTurboAction;->parseDumpCommand(Ljava/io/FileDescriptor;Ljava/io/PrintWriter;[Ljava/lang/String;)Z */
/* .line 47 */
/* .local v0, "handled":Z */
int v1 = 1; // const/4 v1, 0x1
if ( v0 != null) { // if-eqz v0, :cond_0
/* .line 48 */
/* .line 51 */
} // :cond_0
/* const-string/jumbo v2, "version: 1.0.0" */
(( java.io.PrintWriter ) p2 ).println ( v2 ); // invoke-virtual {p2, v2}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V
/* .line 53 */
final String v2 = "--------------------current status-----------------------"; // const-string v2, "--------------------current status-----------------------"
(( java.io.PrintWriter ) p2 ).println ( v2 ); // invoke-virtual {p2, v2}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V
/* .line 55 */
/* invoke-direct {p0, p2}, Lcom/miui/server/turbosched/GameTurboAction;->printFLWConfig(Ljava/io/PrintWriter;)V */
/* .line 58 */
/* invoke-direct {p0, p2}, Lcom/miui/server/turbosched/GameTurboAction;->printSoCGfltConfig(Ljava/io/PrintWriter;)V */
/* .line 61 */
/* invoke-direct {p0, p2}, Lcom/miui/server/turbosched/GameTurboAction;->printSocDfltConfig(Ljava/io/PrintWriter;)V */
/* .line 63 */
final String v2 = "--------------------kernel config-----------------------"; // const-string v2, "--------------------kernel config-----------------------"
(( java.io.PrintWriter ) p2 ).println ( v2 ); // invoke-virtual {p2, v2}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V
/* .line 65 */
/* new-instance v2, Ljava/lang/StringBuilder; */
/* invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V */
final String v3 = "flw access: "; // const-string v3, "flw access: "
(( java.lang.StringBuilder ) v2 ).append ( v3 ); // invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
/* iget-boolean v3, p0, Lcom/miui/server/turbosched/GameTurboAction;->isFLWAccess:Z */
(( java.lang.StringBuilder ) v2 ).append ( v3 ); // invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v2 ).toString ( ); // invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
(( java.io.PrintWriter ) p2 ).println ( v2 ); // invoke-virtual {p2, v2}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V
/* .line 66 */
/* new-instance v2, Ljava/lang/StringBuilder; */
/* invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V */
final String v3 = "flw freq access: "; // const-string v3, "flw freq access: "
(( java.lang.StringBuilder ) v2 ).append ( v3 ); // invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
/* iget-boolean v3, p0, Lcom/miui/server/turbosched/GameTurboAction;->isFLWFreqAccess:Z */
(( java.lang.StringBuilder ) v2 ).append ( v3 ); // invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v2 ).toString ( ); // invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
(( java.io.PrintWriter ) p2 ).println ( v2 ); // invoke-virtual {p2, v2}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V
/* .line 69 */
/* new-instance v2, Ljava/lang/StringBuilder; */
/* invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V */
final String v3 = "gflt access: "; // const-string v3, "gflt access: "
(( java.lang.StringBuilder ) v2 ).append ( v3 ); // invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
/* iget-boolean v3, p0, Lcom/miui/server/turbosched/GameTurboAction;->isSoCGfltAccess:Z */
(( java.lang.StringBuilder ) v2 ).append ( v3 ); // invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v2 ).toString ( ); // invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
(( java.io.PrintWriter ) p2 ).println ( v2 ); // invoke-virtual {p2, v2}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V
/* .line 72 */
/* new-instance v2, Ljava/lang/StringBuilder; */
/* invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V */
final String v3 = "dflt bw access: "; // const-string v3, "dflt bw access: "
(( java.lang.StringBuilder ) v2 ).append ( v3 ); // invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
/* iget-boolean v3, p0, Lcom/miui/server/turbosched/GameTurboAction;->isSocDfltBwAccess:Z */
(( java.lang.StringBuilder ) v2 ).append ( v3 ); // invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v2 ).toString ( ); // invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
(( java.io.PrintWriter ) p2 ).println ( v2 ); // invoke-virtual {p2, v2}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V
/* .line 73 */
/* new-instance v2, Ljava/lang/StringBuilder; */
/* invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V */
final String v3 = "dflt lat access: "; // const-string v3, "dflt lat access: "
(( java.lang.StringBuilder ) v2 ).append ( v3 ); // invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
/* iget-boolean v3, p0, Lcom/miui/server/turbosched/GameTurboAction;->isSocDfltLatAccess:Z */
(( java.lang.StringBuilder ) v2 ).append ( v3 ); // invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v2 ).toString ( ); // invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
(( java.io.PrintWriter ) p2 ).println ( v2 ); // invoke-virtual {p2, v2}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V
/* .line 75 */
} // .end method
