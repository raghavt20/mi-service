class com.miui.server.turbosched.TurboSchedManagerService$12 extends android.app.IProcessObserver$Stub {
	 /* .source "TurboSchedManagerService.java" */
	 /* # annotations */
	 /* .annotation system Ldalvik/annotation/EnclosingClass; */
	 /* value = Lcom/miui/server/turbosched/TurboSchedManagerService; */
} // .end annotation
/* .annotation system Ldalvik/annotation/InnerClass; */
/* accessFlags = 0x0 */
/* name = null */
} // .end annotation
/* # instance fields */
final com.miui.server.turbosched.TurboSchedManagerService this$0; //synthetic
/* # direct methods */
 com.miui.server.turbosched.TurboSchedManagerService$12 ( ) {
/* .locals 0 */
/* .param p1, "this$0" # Lcom/miui/server/turbosched/TurboSchedManagerService; */
/* .line 1883 */
this.this$0 = p1;
/* invoke-direct {p0}, Landroid/app/IProcessObserver$Stub;-><init>()V */
return;
} // .end method
/* # virtual methods */
public void onForegroundActivitiesChanged ( Integer p0, Integer p1, Boolean p2 ) {
/* .locals 7 */
/* .param p1, "pid" # I */
/* .param p2, "uid" # I */
/* .param p3, "foregroundActivities" # Z */
/* .line 1887 */
v0 = this.this$0;
com.miui.server.turbosched.TurboSchedManagerService .-$$Nest$mgetProcessNameByPid ( v0,p1 );
/* .line 1889 */
/* .local v0, "curProcessName":Ljava/lang/String; */
v1 = this.this$0;
v1 = com.miui.server.turbosched.TurboSchedManagerService .-$$Nest$fgetmStartUpModeInitFinish ( v1 );
/* if-nez v1, :cond_0 */
if ( p3 != null) { // if-eqz p3, :cond_0
	 final String v1 = "com.miui.home"; // const-string v1, "com.miui.home"
	 v1 = 	 (( java.lang.String ) v1 ).equals ( v0 ); // invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z
	 if ( v1 != null) { // if-eqz v1, :cond_0
		 /* .line 1890 */
		 v1 = this.this$0;
		 com.miui.server.turbosched.TurboSchedManagerService .-$$Nest$mexitStartUpMode ( v1 );
		 /* .line 1892 */
	 } // :cond_0
	 int v1 = 0; // const/4 v1, 0x0
	 /* .line 1893 */
	 /* .local v1, "isSuccess":Z */
	 int v2 = 0; // const/4 v2, 0x0
	 /* .line 1895 */
	 /* .local v2, "is_enable":Z */
	 android.os.TurboSchedMonitor .getInstance ( );
	 v2 = 	 (( android.os.TurboSchedMonitor ) v3 ).isCoreAppOptimizerEnabled ( ); // invoke-virtual {v3}, Landroid/os/TurboSchedMonitor;->isCoreAppOptimizerEnabled()Z
	 /* .line 1897 */
	 /* if-nez v2, :cond_1 */
	 /* .line 1898 */
	 return;
	 /* .line 1900 */
} // :cond_1
com.miui.server.turbosched.TurboSchedConfig .getPolicyList ( );
v3 = final String v4 = "frame_turbo"; // const-string v4, "frame_turbo"
if ( v3 != null) { // if-eqz v3, :cond_2
	 /* .line 1901 */
	 v3 = this.this$0;
	 com.miui.server.turbosched.TurboSchedManagerService .-$$Nest$fgetmFrameTurboAction ( v3 );
	 (( com.miui.server.turbosched.FrameTurboAction ) v3 ).onForegroundActivitiesChanged ( v0, p3 ); // invoke-virtual {v3, v0, p3}, Lcom/miui/server/turbosched/FrameTurboAction;->onForegroundActivitiesChanged(Ljava/lang/String;Z)V
	 /* .line 1904 */
} // :cond_2
if ( p3 != null) { // if-eqz p3, :cond_3
	 v3 = this.this$0;
	 v3 = 	 com.miui.server.turbosched.TurboSchedManagerService .-$$Nest$fgetmScreenOn ( v3 );
	 if ( v3 != null) { // if-eqz v3, :cond_3
		 /* .line 1905 */
		 v3 = this.this$0;
		 com.miui.server.turbosched.TurboSchedManagerService .-$$Nest$fputmProcessName ( v3,v0 );
		 /* .line 1906 */
		 v3 = this.this$0;
		 com.miui.server.turbosched.TurboSchedManagerService .-$$Nest$fgetmProcessName ( v3 );
		 if ( v3 != null) { // if-eqz v3, :cond_3
			 android.os.TurboSchedMonitor .getInstance ( );
			 v4 = this.this$0;
			 com.miui.server.turbosched.TurboSchedManagerService .-$$Nest$fgetmProcessName ( v4 );
			 v3 = 			 (( android.os.TurboSchedMonitor ) v3 ).isCoreApp ( v4 ); // invoke-virtual {v3, v4}, Landroid/os/TurboSchedMonitor;->isCoreApp(Ljava/lang/String;)Z
			 if ( v3 != null) { // if-eqz v3, :cond_3
				 /* .line 1907 */
				 v3 = this.this$0;
				 /* int-to-long v4, p1 */
				 com.miui.server.turbosched.TurboSchedManagerService .-$$Nest$fputmFgCorePid ( v3,v4,v5 );
				 /* .line 1908 */
				 v3 = this.this$0;
				 /* int-to-long v4, p2 */
				 com.miui.server.turbosched.TurboSchedManagerService .-$$Nest$fputmFgCoreUid ( v3,v4,v5 );
				 /* .line 1912 */
			 } // :cond_3
			 if ( v0 != null) { // if-eqz v0, :cond_5
				 v3 = this.this$0;
				 v3 = 				 com.miui.server.turbosched.TurboSchedManagerService .-$$Nest$fgetmAppsLockContentionList ( v3 );
				 if ( v3 != null) { // if-eqz v3, :cond_5
					 if ( v2 != null) { // if-eqz v2, :cond_5
						 /* .line 1913 */
						 if ( p3 != null) { // if-eqz p3, :cond_5
							 v3 = this.this$0;
							 v3 = 							 com.miui.server.turbosched.TurboSchedManagerService .-$$Nest$fgetmScreenOn ( v3 );
							 if ( v3 != null) { // if-eqz v3, :cond_5
								 /* if-lez p1, :cond_5 */
								 v3 = this.this$0;
								 v3 = 								 com.miui.server.turbosched.TurboSchedManagerService .-$$Nest$fgetmAppsLockContentionPid ( v3 );
								 /* if-eq p1, v3, :cond_5 */
								 /* .line 1914 */
								 v3 = this.this$0;
								 final String v4 = "/sys/module/metis/parameters/mi_lock_blocked_pid"; // const-string v4, "/sys/module/metis/parameters/mi_lock_blocked_pid"
								 java.lang.String .valueOf ( p1 );
								 final String v6 = "LINK"; // const-string v6, "LINK"
								 v1 = 								 com.miui.server.turbosched.TurboSchedManagerService .-$$Nest$mwriteTurboSchedNode ( v3,v6,v4,v5 );
								 /* .line 1915 */
								 if ( v1 != null) { // if-eqz v1, :cond_4
									 /* .line 1916 */
									 v3 = this.this$0;
									 com.miui.server.turbosched.TurboSchedManagerService .-$$Nest$fputmAppsLockContentionPid ( v3,p1 );
									 /* .line 1919 */
								 } // :cond_4
								 android.os.TurboSchedMonitor .getInstance ( );
								 v3 = 								 (( android.os.TurboSchedMonitor ) v3 ).isDebugMode ( ); // invoke-virtual {v3}, Landroid/os/TurboSchedMonitor;->isDebugMode()Z
								 if ( v3 != null) { // if-eqz v3, :cond_5
									 /* .line 1920 */
									 /* new-instance v3, Ljava/lang/StringBuilder; */
									 /* invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V */
									 final String v4 = "Lock Contention, isSuccess:"; // const-string v4, "Lock Contention, isSuccess:"
									 (( java.lang.StringBuilder ) v3 ).append ( v4 ); // invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
									 (( java.lang.StringBuilder ) v3 ).append ( v1 ); // invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;
									 final String v4 = " Blocked Task Pid "; // const-string v4, " Blocked Task Pid "
									 (( java.lang.StringBuilder ) v3 ).append ( v4 ); // invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
									 (( java.lang.StringBuilder ) v3 ).append ( p1 ); // invoke-virtual {v3, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;
									 (( java.lang.StringBuilder ) v3 ).toString ( ); // invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
									 final String v4 = "TurboSchedManagerService"; // const-string v4, "TurboSchedManagerService"
									 android.util.Slog .i ( v4,v3 );
									 /* .line 1924 */
								 } // :cond_5
								 return;
							 } // .end method
							 public void onForegroundServicesChanged ( Integer p0, Integer p1, Integer p2 ) {
								 /* .locals 0 */
								 /* .param p1, "pid" # I */
								 /* .param p2, "uid" # I */
								 /* .param p3, "serviceTypes" # I */
								 /* .line 1929 */
								 return;
							 } // .end method
							 public void onProcessDied ( Integer p0, Integer p1 ) {
								 /* .locals 0 */
								 /* .param p1, "pid" # I */
								 /* .param p2, "uid" # I */
								 /* .line 1934 */
								 return;
							 } // .end method
