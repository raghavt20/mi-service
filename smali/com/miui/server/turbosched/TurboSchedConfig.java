public class com.miui.server.turbosched.TurboSchedConfig {
	 /* .source "TurboSchedConfig.java" */
	 /* # static fields */
	 protected static final Integer ERROR_CODE_EXIST_TURBO_SCENE;
	 protected static final Integer ERROR_CODE_INTERNAL_EXCEPTION;
	 protected static final Integer ERROR_CODE_INVALID_PARAM;
	 protected static final Integer ERROR_CODE_NOT_ENABLE;
	 protected static final Integer ERROR_CODE_NOT_FOREGROUND;
	 protected static final Integer ERROR_CODE_NOT_IN_WHITE_LIST;
	 protected static final Integer ERROR_CODE_SUCCESS;
	 protected static final java.lang.String PERSIST_KEY_FRAME_TURBO_APPS;
	 protected static final java.lang.String PERSIST_KEY_FRAME_TURBO_CURRENT_TURBO_SCENE;
	 protected static final java.lang.String PERSIST_KEY_FRAME_TURBO_SCENE_WHITE_LIST;
	 protected static final java.lang.String PERSIST_KEY_POLICY_LIST;
	 protected static java.util.List POLICY_ALL;
	 /* .annotation system Ldalvik/annotation/Signature; */
	 /* value = { */
	 /* "Ljava/util/List<", */
	 /* "Ljava/lang/String;", */
	 /* ">;" */
	 /* } */
} // .end annotation
} // .end field
protected static final java.lang.String POLICY_NAME_BOOST_WITH_FREQUENCY;
protected static final java.lang.String POLICY_NAME_FRAME_TURBO;
protected static final java.lang.String POLICY_NAME_LINK;
protected static final java.lang.String POLICY_NAME_LITTLE_CORE;
protected static final java.lang.String POLICY_NAME_PRIORITY;
private static java.util.List mPolicyList;
/* .annotation system Ldalvik/annotation/Signature; */
/* value = { */
/* "Ljava/util/List<", */
/* "Ljava/lang/String;", */
/* ">;" */
/* } */
} // .end annotation
} // .end field
/* # direct methods */
static com.miui.server.turbosched.TurboSchedConfig ( ) {
/* .locals 5 */
/* .line 24 */
/* new-instance v0, Ljava/util/ArrayList; */
final String v1 = "priority"; // const-string v1, "priority"
final String v2 = "frame_turbo"; // const-string v2, "frame_turbo"
final String v3 = "link"; // const-string v3, "link"
final String v4 = "bwf"; // const-string v4, "bwf"
/* filled-new-array {v3, v4, v1, v2, v3}, [Ljava/lang/String; */
/* .line 25 */
java.util.Arrays .asList ( v1 );
/* invoke-direct {v0, v1}, Ljava/util/ArrayList;-><init>(Ljava/util/Collection;)V */
/* .line 33 */
/* new-instance v0, Ljava/util/ArrayList; */
/* filled-new-array {v3}, [Ljava/lang/String; */
/* .line 34 */
java.util.Arrays .asList ( v1 );
/* invoke-direct {v0, v1}, Ljava/util/ArrayList;-><init>(Ljava/util/Collection;)V */
/* .line 33 */
return;
} // .end method
public com.miui.server.turbosched.TurboSchedConfig ( ) {
/* .locals 0 */
/* .line 9 */
/* invoke-direct {p0}, Ljava/lang/Object;-><init>()V */
return;
} // .end method
protected static java.util.List getPolicyList ( ) {
/* .locals 1 */
/* .annotation system Ldalvik/annotation/Signature; */
/* value = { */
/* "()", */
/* "Ljava/util/List<", */
/* "Ljava/lang/String;", */
/* ">;" */
/* } */
} // .end annotation
/* .line 43 */
v0 = com.miui.server.turbosched.TurboSchedConfig.mPolicyList;
} // .end method
protected static void setPolicyList ( java.util.List p0 ) {
/* .locals 2 */
/* .annotation system Ldalvik/annotation/Signature; */
/* value = { */
/* "(", */
/* "Ljava/util/List<", */
/* "Ljava/lang/String;", */
/* ">;)V" */
/* } */
} // .end annotation
/* .line 38 */
/* .local p0, "policyList":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;" */
/* .line 39 */
final String v0 = ","; // const-string v0, ","
java.lang.String .join ( v0,p0 );
final String v1 = "persist.sys.turbosched.policy_list"; // const-string v1, "persist.sys.turbosched.policy_list"
android.os.SystemProperties .set ( v1,v0 );
/* .line 40 */
return;
} // .end method
