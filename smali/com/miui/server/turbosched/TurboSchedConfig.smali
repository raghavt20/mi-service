.class public Lcom/miui/server/turbosched/TurboSchedConfig;
.super Ljava/lang/Object;
.source "TurboSchedConfig.java"


# static fields
.field protected static final ERROR_CODE_EXIST_TURBO_SCENE:I = -0x5

.field protected static final ERROR_CODE_INTERNAL_EXCEPTION:I = -0x2

.field protected static final ERROR_CODE_INVALID_PARAM:I = -0x3

.field protected static final ERROR_CODE_NOT_ENABLE:I = -0x1

.field protected static final ERROR_CODE_NOT_FOREGROUND:I = -0x6

.field protected static final ERROR_CODE_NOT_IN_WHITE_LIST:I = -0x4

.field protected static final ERROR_CODE_SUCCESS:I = 0x0

.field protected static final PERSIST_KEY_FRAME_TURBO_APPS:Ljava/lang/String; = "persist.sys.turbosched.frame_turbo.apps"

.field protected static final PERSIST_KEY_FRAME_TURBO_CURRENT_TURBO_SCENE:Ljava/lang/String; = "persist.sys.turbosched.frame_turbo.current_turbo_scene"

.field protected static final PERSIST_KEY_FRAME_TURBO_SCENE_WHITE_LIST:Ljava/lang/String; = "persist.sys.turbosched.frame_turbo.scene_white_list"

.field protected static final PERSIST_KEY_POLICY_LIST:Ljava/lang/String; = "persist.sys.turbosched.policy_list"

.field protected static POLICY_ALL:Ljava/util/List; = null
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field protected static final POLICY_NAME_BOOST_WITH_FREQUENCY:Ljava/lang/String; = "bwf"

.field protected static final POLICY_NAME_FRAME_TURBO:Ljava/lang/String; = "frame_turbo"

.field protected static final POLICY_NAME_LINK:Ljava/lang/String; = "link"

.field protected static final POLICY_NAME_LITTLE_CORE:Ljava/lang/String; = "lc"

.field protected static final POLICY_NAME_PRIORITY:Ljava/lang/String; = "priority"

.field private static mPolicyList:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .locals 5

    .line 24
    new-instance v0, Ljava/util/ArrayList;

    const-string v1, "priority"

    const-string v2, "frame_turbo"

    const-string v3, "link"

    const-string v4, "bwf"

    filled-new-array {v3, v4, v1, v2, v3}, [Ljava/lang/String;

    move-result-object v1

    .line 25
    invoke-static {v1}, Ljava/util/Arrays;->asList([Ljava/lang/Object;)Ljava/util/List;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/util/ArrayList;-><init>(Ljava/util/Collection;)V

    sput-object v0, Lcom/miui/server/turbosched/TurboSchedConfig;->POLICY_ALL:Ljava/util/List;

    .line 33
    new-instance v0, Ljava/util/ArrayList;

    filled-new-array {v3}, [Ljava/lang/String;

    move-result-object v1

    .line 34
    invoke-static {v1}, Ljava/util/Arrays;->asList([Ljava/lang/Object;)Ljava/util/List;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/util/ArrayList;-><init>(Ljava/util/Collection;)V

    sput-object v0, Lcom/miui/server/turbosched/TurboSchedConfig;->mPolicyList:Ljava/util/List;

    .line 33
    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .line 9
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method protected static getPolicyList()Ljava/util/List;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .line 43
    sget-object v0, Lcom/miui/server/turbosched/TurboSchedConfig;->mPolicyList:Ljava/util/List;

    return-object v0
.end method

.method protected static setPolicyList(Ljava/util/List;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List<",
            "Ljava/lang/String;",
            ">;)V"
        }
    .end annotation

    .line 38
    .local p0, "policyList":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;"
    sput-object p0, Lcom/miui/server/turbosched/TurboSchedConfig;->mPolicyList:Ljava/util/List;

    .line 39
    const-string v0, ","

    invoke-static {v0, p0}, Ljava/lang/String;->join(Ljava/lang/CharSequence;Ljava/lang/Iterable;)Ljava/lang/String;

    move-result-object v0

    const-string v1, "persist.sys.turbosched.policy_list"

    invoke-static {v1, v0}, Landroid/os/SystemProperties;->set(Ljava/lang/String;Ljava/lang/String;)V

    .line 40
    return-void
.end method
