.class Lcom/miui/server/turbosched/TurboSchedManagerService$12;
.super Landroid/app/IProcessObserver$Stub;
.source "TurboSchedManagerService.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/miui/server/turbosched/TurboSchedManagerService;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/miui/server/turbosched/TurboSchedManagerService;


# direct methods
.method constructor <init>(Lcom/miui/server/turbosched/TurboSchedManagerService;)V
    .locals 0
    .param p1, "this$0"    # Lcom/miui/server/turbosched/TurboSchedManagerService;

    .line 1883
    iput-object p1, p0, Lcom/miui/server/turbosched/TurboSchedManagerService$12;->this$0:Lcom/miui/server/turbosched/TurboSchedManagerService;

    invoke-direct {p0}, Landroid/app/IProcessObserver$Stub;-><init>()V

    return-void
.end method


# virtual methods
.method public onForegroundActivitiesChanged(IIZ)V
    .locals 7
    .param p1, "pid"    # I
    .param p2, "uid"    # I
    .param p3, "foregroundActivities"    # Z

    .line 1887
    iget-object v0, p0, Lcom/miui/server/turbosched/TurboSchedManagerService$12;->this$0:Lcom/miui/server/turbosched/TurboSchedManagerService;

    invoke-static {v0, p1}, Lcom/miui/server/turbosched/TurboSchedManagerService;->-$$Nest$mgetProcessNameByPid(Lcom/miui/server/turbosched/TurboSchedManagerService;I)Ljava/lang/String;

    move-result-object v0

    .line 1889
    .local v0, "curProcessName":Ljava/lang/String;
    iget-object v1, p0, Lcom/miui/server/turbosched/TurboSchedManagerService$12;->this$0:Lcom/miui/server/turbosched/TurboSchedManagerService;

    invoke-static {v1}, Lcom/miui/server/turbosched/TurboSchedManagerService;->-$$Nest$fgetmStartUpModeInitFinish(Lcom/miui/server/turbosched/TurboSchedManagerService;)Z

    move-result v1

    if-nez v1, :cond_0

    if-eqz p3, :cond_0

    const-string v1, "com.miui.home"

    invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 1890
    iget-object v1, p0, Lcom/miui/server/turbosched/TurboSchedManagerService$12;->this$0:Lcom/miui/server/turbosched/TurboSchedManagerService;

    invoke-static {v1}, Lcom/miui/server/turbosched/TurboSchedManagerService;->-$$Nest$mexitStartUpMode(Lcom/miui/server/turbosched/TurboSchedManagerService;)V

    .line 1892
    :cond_0
    const/4 v1, 0x0

    .line 1893
    .local v1, "isSuccess":Z
    const/4 v2, 0x0

    .line 1895
    .local v2, "is_enable":Z
    invoke-static {}, Landroid/os/TurboSchedMonitor;->getInstance()Landroid/os/TurboSchedMonitor;

    move-result-object v3

    invoke-virtual {v3}, Landroid/os/TurboSchedMonitor;->isCoreAppOptimizerEnabled()Z

    move-result v2

    .line 1897
    if-nez v2, :cond_1

    .line 1898
    return-void

    .line 1900
    :cond_1
    invoke-static {}, Lcom/miui/server/turbosched/TurboSchedConfig;->getPolicyList()Ljava/util/List;

    move-result-object v3

    const-string v4, "frame_turbo"

    invoke-interface {v3, v4}, Ljava/util/List;->contains(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_2

    .line 1901
    iget-object v3, p0, Lcom/miui/server/turbosched/TurboSchedManagerService$12;->this$0:Lcom/miui/server/turbosched/TurboSchedManagerService;

    invoke-static {v3}, Lcom/miui/server/turbosched/TurboSchedManagerService;->-$$Nest$fgetmFrameTurboAction(Lcom/miui/server/turbosched/TurboSchedManagerService;)Lcom/miui/server/turbosched/FrameTurboAction;

    move-result-object v3

    invoke-virtual {v3, v0, p3}, Lcom/miui/server/turbosched/FrameTurboAction;->onForegroundActivitiesChanged(Ljava/lang/String;Z)V

    .line 1904
    :cond_2
    if-eqz p3, :cond_3

    iget-object v3, p0, Lcom/miui/server/turbosched/TurboSchedManagerService$12;->this$0:Lcom/miui/server/turbosched/TurboSchedManagerService;

    invoke-static {v3}, Lcom/miui/server/turbosched/TurboSchedManagerService;->-$$Nest$fgetmScreenOn(Lcom/miui/server/turbosched/TurboSchedManagerService;)Z

    move-result v3

    if-eqz v3, :cond_3

    .line 1905
    iget-object v3, p0, Lcom/miui/server/turbosched/TurboSchedManagerService$12;->this$0:Lcom/miui/server/turbosched/TurboSchedManagerService;

    invoke-static {v3, v0}, Lcom/miui/server/turbosched/TurboSchedManagerService;->-$$Nest$fputmProcessName(Lcom/miui/server/turbosched/TurboSchedManagerService;Ljava/lang/String;)V

    .line 1906
    iget-object v3, p0, Lcom/miui/server/turbosched/TurboSchedManagerService$12;->this$0:Lcom/miui/server/turbosched/TurboSchedManagerService;

    invoke-static {v3}, Lcom/miui/server/turbosched/TurboSchedManagerService;->-$$Nest$fgetmProcessName(Lcom/miui/server/turbosched/TurboSchedManagerService;)Ljava/lang/String;

    move-result-object v3

    if-eqz v3, :cond_3

    invoke-static {}, Landroid/os/TurboSchedMonitor;->getInstance()Landroid/os/TurboSchedMonitor;

    move-result-object v3

    iget-object v4, p0, Lcom/miui/server/turbosched/TurboSchedManagerService$12;->this$0:Lcom/miui/server/turbosched/TurboSchedManagerService;

    invoke-static {v4}, Lcom/miui/server/turbosched/TurboSchedManagerService;->-$$Nest$fgetmProcessName(Lcom/miui/server/turbosched/TurboSchedManagerService;)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Landroid/os/TurboSchedMonitor;->isCoreApp(Ljava/lang/String;)Z

    move-result v3

    if-eqz v3, :cond_3

    .line 1907
    iget-object v3, p0, Lcom/miui/server/turbosched/TurboSchedManagerService$12;->this$0:Lcom/miui/server/turbosched/TurboSchedManagerService;

    int-to-long v4, p1

    invoke-static {v3, v4, v5}, Lcom/miui/server/turbosched/TurboSchedManagerService;->-$$Nest$fputmFgCorePid(Lcom/miui/server/turbosched/TurboSchedManagerService;J)V

    .line 1908
    iget-object v3, p0, Lcom/miui/server/turbosched/TurboSchedManagerService$12;->this$0:Lcom/miui/server/turbosched/TurboSchedManagerService;

    int-to-long v4, p2

    invoke-static {v3, v4, v5}, Lcom/miui/server/turbosched/TurboSchedManagerService;->-$$Nest$fputmFgCoreUid(Lcom/miui/server/turbosched/TurboSchedManagerService;J)V

    .line 1912
    :cond_3
    if-eqz v0, :cond_5

    iget-object v3, p0, Lcom/miui/server/turbosched/TurboSchedManagerService$12;->this$0:Lcom/miui/server/turbosched/TurboSchedManagerService;

    invoke-static {v3}, Lcom/miui/server/turbosched/TurboSchedManagerService;->-$$Nest$fgetmAppsLockContentionList(Lcom/miui/server/turbosched/TurboSchedManagerService;)Ljava/util/List;

    move-result-object v3

    invoke-interface {v3, v0}, Ljava/util/List;->contains(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_5

    if-eqz v2, :cond_5

    .line 1913
    if-eqz p3, :cond_5

    iget-object v3, p0, Lcom/miui/server/turbosched/TurboSchedManagerService$12;->this$0:Lcom/miui/server/turbosched/TurboSchedManagerService;

    invoke-static {v3}, Lcom/miui/server/turbosched/TurboSchedManagerService;->-$$Nest$fgetmScreenOn(Lcom/miui/server/turbosched/TurboSchedManagerService;)Z

    move-result v3

    if-eqz v3, :cond_5

    if-lez p1, :cond_5

    iget-object v3, p0, Lcom/miui/server/turbosched/TurboSchedManagerService$12;->this$0:Lcom/miui/server/turbosched/TurboSchedManagerService;

    invoke-static {v3}, Lcom/miui/server/turbosched/TurboSchedManagerService;->-$$Nest$fgetmAppsLockContentionPid(Lcom/miui/server/turbosched/TurboSchedManagerService;)I

    move-result v3

    if-eq p1, v3, :cond_5

    .line 1914
    iget-object v3, p0, Lcom/miui/server/turbosched/TurboSchedManagerService$12;->this$0:Lcom/miui/server/turbosched/TurboSchedManagerService;

    const-string v4, "/sys/module/metis/parameters/mi_lock_blocked_pid"

    invoke-static {p1}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v5

    const-string v6, "LINK"

    invoke-static {v3, v6, v4, v5}, Lcom/miui/server/turbosched/TurboSchedManagerService;->-$$Nest$mwriteTurboSchedNode(Lcom/miui/server/turbosched/TurboSchedManagerService;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Z

    move-result v1

    .line 1915
    if-eqz v1, :cond_4

    .line 1916
    iget-object v3, p0, Lcom/miui/server/turbosched/TurboSchedManagerService$12;->this$0:Lcom/miui/server/turbosched/TurboSchedManagerService;

    invoke-static {v3, p1}, Lcom/miui/server/turbosched/TurboSchedManagerService;->-$$Nest$fputmAppsLockContentionPid(Lcom/miui/server/turbosched/TurboSchedManagerService;I)V

    .line 1919
    :cond_4
    invoke-static {}, Landroid/os/TurboSchedMonitor;->getInstance()Landroid/os/TurboSchedMonitor;

    move-result-object v3

    invoke-virtual {v3}, Landroid/os/TurboSchedMonitor;->isDebugMode()Z

    move-result v3

    if-eqz v3, :cond_5

    .line 1920
    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "Lock Contention, isSuccess:"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, " Blocked Task Pid "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    const-string v4, "TurboSchedManagerService"

    invoke-static {v4, v3}, Landroid/util/Slog;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 1924
    :cond_5
    return-void
.end method

.method public onForegroundServicesChanged(III)V
    .locals 0
    .param p1, "pid"    # I
    .param p2, "uid"    # I
    .param p3, "serviceTypes"    # I

    .line 1929
    return-void
.end method

.method public onProcessDied(II)V
    .locals 0
    .param p1, "pid"    # I
    .param p2, "uid"    # I

    .line 1934
    return-void
.end method
