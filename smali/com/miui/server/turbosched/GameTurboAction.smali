.class public Lcom/miui/server/turbosched/GameTurboAction;
.super Ljava/lang/Object;
.source "GameTurboAction.java"


# static fields
.field private static final FLW_CHOOSE_LOAD_POLICY_PATH:Ljava/lang/String; = "/sys/module/migt/parameters/choose_load_policy"

.field private static final FLW_CHOOSE_WAKE_STATE_PATH:Ljava/lang/String; = "/sys/module/migt/parameters/choose_wake_state"

.field private static final FLW_DEBUG_PATH:Ljava/lang/String; = "/sys/module/migt/parameters/flw_debug"

.field private static final FLW_DOWN_THRESH_MARGIN_PATH:Ljava/lang/String; = "/sys/module/migt/parameters/down_thresh_margin"

.field private static final FLW_ENABLE_PATH:Ljava/lang/String; = "/sys/module/migt/parameters/flw_enable"

.field private static final FLW_FREQ_ENABLE_PATH:Ljava/lang/String; = "/sys/module/migt/parameters/flw_freq_enable"

.field private static final FLW_TARGET_DELTA_PATH:Ljava/lang/String; = "/sys/module/migt/parameters/target_delta"

.field private static final FLW_UP_THRESH_MARGIN_PATH:Ljava/lang/String; = "/sys/module/migt/parameters/up_thresh_margin"

.field private static final FLW_VERSION_PATH:Ljava/lang/String; = "/sys/module/migt/parameters/version"

.field private static final SOC_DFLT_BW_ENABLE_PATH:Ljava/lang/String; = "/sys/module/mist/parameters/dflt_bw_enable"

.field private static final SOC_DFLT_DEBUG_PATH:Ljava/lang/String; = "/sys/module/mist/parameters/dflt_debug"

.field private static final SOC_DFLT_LAT_ENABLE_PATH:Ljava/lang/String; = "/sys/module/mist/parameters/dflt_lat_enable"

.field private static final SOC_DFLT_VERSION_PATH:Ljava/lang/String; = "/sys/module/mist/parameters/mist_version"

.field private static final SOC_GFLT_DEBUG_PATH:Ljava/lang/String; = "/sys/module/mist/parameters/gflt_debug"

.field private static final SOC_GFLT_ENABLE_PATH:Ljava/lang/String; = "/sys/module/mist/parameters/gflt_enable"

.field private static final TAG:Ljava/lang/String; = "TurboSched_GameTurboAction"

.field private static final VERSION:Ljava/lang/String; = "1.0.0"


# instance fields
.field private isFLWAccess:Z

.field private isFLWEnable:Z

.field private isFLWFreqAccess:Z

.field private isFLWFreqEnable:Z

.field private isSoCGfltAccess:Z

.field private isSoCGfltEnable:Z

.field private isSocDfltBwAccess:Z

.field private isSocDfltBwEnable:Z

.field private isSocDfltLatAccess:Z

.field private isSocDfltLatEnable:Z


# direct methods
.method protected constructor <init>()V
    .locals 1

    .line 41
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 29
    const-string v0, "/sys/module/migt/parameters/flw_enable"

    invoke-static {v0}, Lcom/miui/server/turbosched/TurboSchedUtil;->checkFileAccess(Ljava/lang/String;)Z

    move-result v0

    iput-boolean v0, p0, Lcom/miui/server/turbosched/GameTurboAction;->isFLWAccess:Z

    .line 30
    const-string v0, "/sys/module/migt/parameters/flw_freq_enable"

    invoke-static {v0}, Lcom/miui/server/turbosched/TurboSchedUtil;->checkFileAccess(Ljava/lang/String;)Z

    move-result v0

    iput-boolean v0, p0, Lcom/miui/server/turbosched/GameTurboAction;->isFLWFreqAccess:Z

    .line 31
    const-string v0, "/sys/module/mist/parameters/gflt_enable"

    invoke-static {v0}, Lcom/miui/server/turbosched/TurboSchedUtil;->checkFileAccess(Ljava/lang/String;)Z

    move-result v0

    iput-boolean v0, p0, Lcom/miui/server/turbosched/GameTurboAction;->isSoCGfltAccess:Z

    .line 32
    const-string v0, "/sys/module/mist/parameters/dflt_bw_enable"

    invoke-static {v0}, Lcom/miui/server/turbosched/TurboSchedUtil;->checkFileAccess(Ljava/lang/String;)Z

    move-result v0

    iput-boolean v0, p0, Lcom/miui/server/turbosched/GameTurboAction;->isSocDfltBwAccess:Z

    .line 33
    const-string v0, "/sys/module/mist/parameters/dflt_lat_enable"

    invoke-static {v0}, Lcom/miui/server/turbosched/TurboSchedUtil;->checkFileAccess(Ljava/lang/String;)Z

    move-result v0

    iput-boolean v0, p0, Lcom/miui/server/turbosched/GameTurboAction;->isSocDfltLatAccess:Z

    .line 35
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/miui/server/turbosched/GameTurboAction;->isFLWEnable:Z

    .line 36
    iput-boolean v0, p0, Lcom/miui/server/turbosched/GameTurboAction;->isFLWFreqEnable:Z

    .line 37
    iput-boolean v0, p0, Lcom/miui/server/turbosched/GameTurboAction;->isSoCGfltEnable:Z

    .line 38
    iput-boolean v0, p0, Lcom/miui/server/turbosched/GameTurboAction;->isSocDfltBwEnable:Z

    .line 39
    iput-boolean v0, p0, Lcom/miui/server/turbosched/GameTurboAction;->isSocDfltLatEnable:Z

    .line 42
    invoke-direct {p0}, Lcom/miui/server/turbosched/GameTurboAction;->readConfigValue()V

    .line 43
    return-void
.end method

.method private parseDFLTDumpCommand(Ljava/io/FileDescriptor;Ljava/io/PrintWriter;[Ljava/lang/String;)Z
    .locals 11
    .param p1, "fd"    # Ljava/io/FileDescriptor;
    .param p2, "pw"    # Ljava/io/PrintWriter;
    .param p3, "args"    # [Ljava/lang/String;

    .line 232
    array-length v0, p3

    const-string v1, "invalid dflt command"

    const/4 v2, 0x1

    const/4 v3, 0x3

    if-ge v0, v3, :cond_0

    .line 233
    invoke-virtual {p2, v1}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V

    .line 234
    return v2

    .line 237
    :cond_0
    aget-object v0, p3, v2

    .line 238
    .local v0, "cmd":Ljava/lang/String;
    const-string v4, "enable"

    const/4 v5, 0x2

    aget-object v6, p3, v5

    invoke-virtual {v4, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    const-string v6, "1"

    const-string v7, "0"

    if-eqz v4, :cond_1

    move-object v4, v6

    goto :goto_0

    :cond_1
    move-object v4, v7

    .line 239
    .local v4, "enable":Ljava/lang/String;
    :goto_0
    const/4 v8, 0x0

    .line 240
    .local v8, "errorMsg":Ljava/lang/String;
    invoke-virtual {v0}, Ljava/lang/String;->hashCode()I

    move-result v9

    sparse-switch v9, :sswitch_data_0

    :cond_2
    goto :goto_1

    :sswitch_0
    const-string v9, "debug"

    invoke-virtual {v0, v9}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v9

    if-eqz v9, :cond_2

    move v9, v5

    goto :goto_2

    :sswitch_1
    const-string v9, "lat"

    invoke-virtual {v0, v9}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v9

    if-eqz v9, :cond_2

    move v9, v2

    goto :goto_2

    :sswitch_2
    const-string v9, "bw"

    invoke-virtual {v0, v9}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v9

    if-eqz v9, :cond_2

    const/4 v9, 0x0

    goto :goto_2

    :goto_1
    const/4 v9, -0x1

    :goto_2
    packed-switch v9, :pswitch_data_0

    goto/16 :goto_4

    .line 255
    :pswitch_0
    const-string v9, "/sys/module/mist/parameters/dflt_debug"

    invoke-static {v9}, Lcom/miui/server/turbosched/TurboSchedUtil;->checkFileAccess(Ljava/lang/String;)Z

    move-result v10

    if-eqz v10, :cond_7

    .line 256
    array-length v10, p3

    if-ge v10, v3, :cond_3

    .line 257
    invoke-virtual {p2, v1}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V

    .line 258
    return v2

    .line 261
    :cond_3
    aget-object v3, p3, v5

    invoke-virtual {v7, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-nez v3, :cond_4

    aget-object v3, p3, v5

    invoke-virtual {v6, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-nez v3, :cond_4

    .line 262
    const-string v1, "invalid dflt debug command"

    invoke-virtual {p2, v1}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V

    .line 263
    return v2

    .line 265
    :cond_4
    aget-object v3, p3, v5

    invoke-static {v9, v3}, Lcom/miui/server/turbosched/TurboSchedUtil;->writeValueToFile(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v8

    goto/16 :goto_4

    .line 249
    :pswitch_1
    iget-boolean v1, p0, Lcom/miui/server/turbosched/GameTurboAction;->isSocDfltLatAccess:Z

    if-eqz v1, :cond_5

    .line 250
    const-string v1, "/sys/module/mist/parameters/dflt_lat_enable"

    invoke-static {v1, v4}, Lcom/miui/server/turbosched/TurboSchedUtil;->writeValueToFile(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v8

    .line 251
    invoke-static {v1}, Lcom/miui/server/turbosched/TurboSchedUtil;->readBooleanValueFromFile(Ljava/lang/String;)Z

    move-result v1

    iput-boolean v1, p0, Lcom/miui/server/turbosched/GameTurboAction;->isSocDfltLatEnable:Z

    goto :goto_3

    .line 242
    :pswitch_2
    iget-boolean v1, p0, Lcom/miui/server/turbosched/GameTurboAction;->isSocDfltBwAccess:Z

    if-eqz v1, :cond_5

    .line 243
    const-string v1, "/sys/module/mist/parameters/dflt_bw_enable"

    invoke-static {v1, v4}, Lcom/miui/server/turbosched/TurboSchedUtil;->writeValueToFile(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v8

    .line 244
    invoke-static {v1}, Lcom/miui/server/turbosched/TurboSchedUtil;->readBooleanValueFromFile(Ljava/lang/String;)Z

    move-result v1

    iput-boolean v1, p0, Lcom/miui/server/turbosched/GameTurboAction;->isSocDfltBwEnable:Z

    .line 245
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "isSocDfltBwEnable: "

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-boolean v3, p0, Lcom/miui/server/turbosched/GameTurboAction;->isSocDfltBwEnable:Z

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    const-string v3, "TurboSched_GameTurboAction"

    invoke-static {v3, v1}, Landroid/util/Slog;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 272
    :cond_5
    :goto_3
    const-string v1, " "

    const-string v3, "dflt "

    if-eqz v8, :cond_6

    .line 273
    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v6, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    aget-object v3, p3, v5

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v3, " failed: "

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p2, v1}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V

    .line 274
    return v2

    .line 277
    :cond_6
    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v6, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    aget-object v3, p3, v5

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v3, " success"

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p2, v1}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V

    .line 278
    const-string v1, "--------------------current status-----------------------"

    invoke-virtual {p2, v1}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V

    .line 279
    invoke-direct {p0, p2}, Lcom/miui/server/turbosched/GameTurboAction;->printSocDfltConfig(Ljava/io/PrintWriter;)V

    .line 280
    return v2

    .line 268
    :cond_7
    :goto_4
    invoke-virtual {p2, v1}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V

    .line 269
    return v2

    :sswitch_data_0
    .sparse-switch
        0xc55 -> :sswitch_2
        0x1a19f -> :sswitch_1
        0x5b09653 -> :sswitch_0
    .end sparse-switch

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_2
        :pswitch_1
        :pswitch_0
    .end packed-switch
.end method

.method private parseDumpCommand(Ljava/io/FileDescriptor;Ljava/io/PrintWriter;[Ljava/lang/String;)Z
    .locals 4
    .param p1, "fd"    # Ljava/io/FileDescriptor;
    .param p2, "pw"    # Ljava/io/PrintWriter;
    .param p3, "args"    # [Ljava/lang/String;

    .line 102
    array-length v0, p3

    const/4 v1, 0x0

    const/4 v2, 0x1

    if-ge v0, v2, :cond_0

    .line 103
    return v1

    .line 106
    :cond_0
    aget-object v0, p3, v1

    .line 107
    .local v0, "cmd":Ljava/lang/String;
    invoke-virtual {v0}, Ljava/lang/String;->hashCode()I

    move-result v3

    sparse-switch v3, :sswitch_data_0

    :cond_1
    goto :goto_0

    :sswitch_0
    const-string v3, "gflt"

    invoke-virtual {v0, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_1

    goto :goto_1

    :sswitch_1
    const-string v2, "dflt"

    invoke-virtual {v0, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_1

    const/4 v2, 0x2

    goto :goto_1

    :sswitch_2
    const-string v2, "flw"

    invoke-virtual {v0, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_1

    move v2, v1

    goto :goto_1

    :goto_0
    const/4 v2, -0x1

    :goto_1
    packed-switch v2, :pswitch_data_0

    .line 115
    return v1

    .line 113
    :pswitch_0
    invoke-direct {p0, p1, p2, p3}, Lcom/miui/server/turbosched/GameTurboAction;->parseDFLTDumpCommand(Ljava/io/FileDescriptor;Ljava/io/PrintWriter;[Ljava/lang/String;)Z

    move-result v1

    return v1

    .line 111
    :pswitch_1
    invoke-direct {p0, p1, p2, p3}, Lcom/miui/server/turbosched/GameTurboAction;->parseGFLTDumpCommand(Ljava/io/FileDescriptor;Ljava/io/PrintWriter;[Ljava/lang/String;)Z

    move-result v1

    return v1

    .line 109
    :pswitch_2
    invoke-direct {p0, p1, p2, p3}, Lcom/miui/server/turbosched/GameTurboAction;->parseFLWDumpCommand(Ljava/io/FileDescriptor;Ljava/io/PrintWriter;[Ljava/lang/String;)Z

    move-result v1

    return v1

    nop

    :sswitch_data_0
    .sparse-switch
        0x18c71 -> :sswitch_2
        0x2f018a -> :sswitch_1
        0x305ea7 -> :sswitch_0
    .end sparse-switch

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_2
        :pswitch_1
        :pswitch_0
    .end packed-switch
.end method

.method private parseFLWDumpCommand(Ljava/io/FileDescriptor;Ljava/io/PrintWriter;[Ljava/lang/String;)Z
    .locals 9
    .param p1, "fd"    # Ljava/io/FileDescriptor;
    .param p2, "pw"    # Ljava/io/PrintWriter;
    .param p3, "args"    # [Ljava/lang/String;

    .line 121
    array-length v0, p3

    const-string v1, "invalid flw command"

    const/4 v2, 0x1

    const/4 v3, 0x2

    if-ge v0, v3, :cond_0

    .line 122
    invoke-virtual {p2, v1}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V

    .line 123
    return v2

    .line 126
    :cond_0
    aget-object v0, p3, v2

    .line 127
    .local v0, "cmd":Ljava/lang/String;
    const/4 v4, 0x0

    .line 128
    .local v4, "errorMsg":Ljava/lang/String;
    invoke-virtual {v0}, Ljava/lang/String;->hashCode()I

    move-result v5

    const/4 v6, 0x3

    sparse-switch v5, :sswitch_data_0

    :cond_1
    goto :goto_0

    :sswitch_0
    const-string v5, "disable"

    invoke-virtual {v0, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-eqz v5, :cond_1

    move v5, v2

    goto :goto_1

    :sswitch_1
    const-string v5, "delta"

    invoke-virtual {v0, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-eqz v5, :cond_1

    move v5, v3

    goto :goto_1

    :sswitch_2
    const-string v5, "debug"

    invoke-virtual {v0, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-eqz v5, :cond_1

    move v5, v6

    goto :goto_1

    :sswitch_3
    const-string v5, "enable"

    invoke-virtual {v0, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-eqz v5, :cond_1

    const/4 v5, 0x0

    goto :goto_1

    :goto_0
    const/4 v5, -0x1

    :goto_1
    const-string v7, "1"

    const-string v8, "/sys/module/migt/parameters/flw_enable"

    packed-switch v5, :pswitch_data_0

    goto/16 :goto_3

    .line 151
    :pswitch_0
    const-string v5, "/sys/module/migt/parameters/flw_debug"

    invoke-static {v5}, Lcom/miui/server/turbosched/TurboSchedUtil;->checkFileAccess(Ljava/lang/String;)Z

    move-result v8

    if-eqz v8, :cond_7

    .line 152
    array-length v8, p3

    if-ge v8, v6, :cond_2

    .line 153
    invoke-virtual {p2, v1}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V

    .line 154
    return v2

    .line 157
    :cond_2
    const-string v6, "0"

    aget-object v8, p3, v3

    invoke-virtual {v6, v8}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-nez v6, :cond_3

    aget-object v6, p3, v3

    invoke-virtual {v7, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-nez v6, :cond_3

    .line 158
    const-string v1, "invalid flw debug command"

    invoke-virtual {p2, v1}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V

    .line 159
    return v2

    .line 161
    :cond_3
    aget-object v3, p3, v3

    invoke-static {v5, v3}, Lcom/miui/server/turbosched/TurboSchedUtil;->writeValueToFile(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    goto/16 :goto_3

    .line 142
    :pswitch_1
    const-string v5, "/sys/module/migt/parameters/target_delta"

    invoke-static {v5}, Lcom/miui/server/turbosched/TurboSchedUtil;->checkFileAccess(Ljava/lang/String;)Z

    move-result v7

    if-eqz v7, :cond_5

    .line 143
    array-length v7, p3

    if-ge v7, v6, :cond_4

    .line 144
    invoke-virtual {p2, v1}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V

    .line 145
    return v2

    .line 147
    :cond_4
    aget-object v1, p3, v3

    invoke-static {v5, v1}, Lcom/miui/server/turbosched/TurboSchedUtil;->writeValueToFile(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    goto :goto_2

    .line 136
    :pswitch_2
    iget-boolean v1, p0, Lcom/miui/server/turbosched/GameTurboAction;->isFLWFreqAccess:Z

    if-eqz v1, :cond_5

    .line 137
    const-string v1, "/sys/module/migt/parameters/flw_freq_enable"

    invoke-static {v1, v7}, Lcom/miui/server/turbosched/TurboSchedUtil;->writeValueToFile(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    .line 138
    invoke-static {v8}, Lcom/miui/server/turbosched/TurboSchedUtil;->readBooleanValueFromFile(Ljava/lang/String;)Z

    move-result v1

    iput-boolean v1, p0, Lcom/miui/server/turbosched/GameTurboAction;->isFLWEnable:Z

    goto :goto_2

    .line 130
    :pswitch_3
    iget-boolean v1, p0, Lcom/miui/server/turbosched/GameTurboAction;->isFLWAccess:Z

    if-eqz v1, :cond_5

    .line 131
    invoke-static {v8, v7}, Lcom/miui/server/turbosched/TurboSchedUtil;->writeValueToFile(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    .line 132
    invoke-static {v8}, Lcom/miui/server/turbosched/TurboSchedUtil;->readBooleanValueFromFile(Ljava/lang/String;)Z

    move-result v1

    iput-boolean v1, p0, Lcom/miui/server/turbosched/GameTurboAction;->isFLWEnable:Z

    .line 168
    :cond_5
    :goto_2
    const-string v1, "flw "

    if-eqz v4, :cond_6

    .line 169
    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v3, " failed: "

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p2, v1}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V

    .line 170
    return v2

    .line 173
    :cond_6
    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v3, " success"

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p2, v1}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V

    .line 174
    const-string v1, "--------------------current status-----------------------"

    invoke-virtual {p2, v1}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V

    .line 175
    invoke-direct {p0, p2}, Lcom/miui/server/turbosched/GameTurboAction;->printFLWConfig(Ljava/io/PrintWriter;)V

    .line 176
    return v2

    .line 164
    :cond_7
    :goto_3
    invoke-virtual {p2, v1}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V

    .line 165
    return v2

    nop

    :sswitch_data_0
    .sparse-switch
        -0x4d6ada7d -> :sswitch_3
        0x5b09653 -> :sswitch_2
        0x5b0bbb8 -> :sswitch_1
        0x639e22e8 -> :sswitch_0
    .end sparse-switch

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_3
        :pswitch_2
        :pswitch_1
        :pswitch_0
    .end packed-switch
.end method

.method private parseGFLTDumpCommand(Ljava/io/FileDescriptor;Ljava/io/PrintWriter;[Ljava/lang/String;)Z
    .locals 10
    .param p1, "fd"    # Ljava/io/FileDescriptor;
    .param p2, "pw"    # Ljava/io/PrintWriter;
    .param p3, "args"    # [Ljava/lang/String;

    .line 181
    array-length v0, p3

    const-string v1, "invalid gflt command"

    const/4 v2, 0x1

    const/4 v3, 0x2

    if-ge v0, v3, :cond_0

    .line 182
    invoke-virtual {p2, v1}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V

    .line 183
    return v2

    .line 186
    :cond_0
    aget-object v0, p3, v2

    .line 187
    .local v0, "cmd":Ljava/lang/String;
    const/4 v4, 0x0

    .line 188
    .local v4, "errorMsg":Ljava/lang/String;
    invoke-virtual {v0}, Ljava/lang/String;->hashCode()I

    move-result v5

    sparse-switch v5, :sswitch_data_0

    :cond_1
    goto :goto_0

    :sswitch_0
    const-string v5, "disable"

    invoke-virtual {v0, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-eqz v5, :cond_1

    move v5, v2

    goto :goto_1

    :sswitch_1
    const-string v5, "debug"

    invoke-virtual {v0, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-eqz v5, :cond_1

    move v5, v3

    goto :goto_1

    :sswitch_2
    const-string v5, "enable"

    invoke-virtual {v0, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-eqz v5, :cond_1

    const/4 v5, 0x0

    goto :goto_1

    :goto_0
    const/4 v5, -0x1

    :goto_1
    const-string v6, "0"

    const-string v7, "1"

    const-string v8, "/sys/module/mist/parameters/gflt_enable"

    packed-switch v5, :pswitch_data_0

    goto/16 :goto_3

    .line 202
    :pswitch_0
    const-string v5, "/sys/module/mist/parameters/gflt_debug"

    invoke-static {v5}, Lcom/miui/server/turbosched/TurboSchedUtil;->checkFileAccess(Ljava/lang/String;)Z

    move-result v8

    if-eqz v8, :cond_6

    .line 203
    array-length v8, p3

    const/4 v9, 0x3

    if-ge v8, v9, :cond_2

    .line 204
    invoke-virtual {p2, v1}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V

    .line 205
    return v2

    .line 208
    :cond_2
    aget-object v8, p3, v3

    invoke-virtual {v6, v8}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-nez v6, :cond_3

    aget-object v6, p3, v3

    invoke-virtual {v7, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-nez v6, :cond_3

    .line 209
    const-string v1, "invalid gflt debug command"

    invoke-virtual {p2, v1}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V

    .line 210
    return v2

    .line 212
    :cond_3
    aget-object v3, p3, v3

    invoke-static {v5, v3}, Lcom/miui/server/turbosched/TurboSchedUtil;->writeValueToFile(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    goto :goto_3

    .line 196
    :pswitch_1
    iget-boolean v1, p0, Lcom/miui/server/turbosched/GameTurboAction;->isSoCGfltAccess:Z

    if-eqz v1, :cond_4

    .line 197
    invoke-static {v8, v6}, Lcom/miui/server/turbosched/TurboSchedUtil;->writeValueToFile(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    .line 198
    invoke-static {v8}, Lcom/miui/server/turbosched/TurboSchedUtil;->readBooleanValueFromFile(Ljava/lang/String;)Z

    move-result v1

    iput-boolean v1, p0, Lcom/miui/server/turbosched/GameTurboAction;->isSoCGfltEnable:Z

    goto :goto_2

    .line 190
    :pswitch_2
    iget-boolean v1, p0, Lcom/miui/server/turbosched/GameTurboAction;->isSoCGfltAccess:Z

    if-eqz v1, :cond_4

    .line 191
    invoke-static {v8, v7}, Lcom/miui/server/turbosched/TurboSchedUtil;->writeValueToFile(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    .line 192
    invoke-static {v8}, Lcom/miui/server/turbosched/TurboSchedUtil;->readBooleanValueFromFile(Ljava/lang/String;)Z

    move-result v1

    iput-boolean v1, p0, Lcom/miui/server/turbosched/GameTurboAction;->isSoCGfltEnable:Z

    .line 219
    :cond_4
    :goto_2
    const-string v1, "gflt "

    if-eqz v4, :cond_5

    .line 220
    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v3, " failed: "

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p2, v1}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V

    .line 221
    return v2

    .line 224
    :cond_5
    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v3, " success"

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p2, v1}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V

    .line 225
    const-string v1, "--------------------current status-----------------------"

    invoke-virtual {p2, v1}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V

    .line 226
    invoke-direct {p0, p2}, Lcom/miui/server/turbosched/GameTurboAction;->printSoCGfltConfig(Ljava/io/PrintWriter;)V

    .line 227
    return v2

    .line 215
    :cond_6
    :goto_3
    invoke-virtual {p2, v1}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V

    .line 216
    return v2

    :sswitch_data_0
    .sparse-switch
        -0x4d6ada7d -> :sswitch_2
        0x5b09653 -> :sswitch_1
        0x639e22e8 -> :sswitch_0
    .end sparse-switch

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_2
        :pswitch_1
        :pswitch_0
    .end packed-switch
.end method

.method private printFLWConfig(Ljava/io/PrintWriter;)V
    .locals 2
    .param p1, "pw"    # Ljava/io/PrintWriter;

    .line 79
    const-string v0, "flw :"

    invoke-virtual {p1, v0}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V

    .line 80
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, " enable: "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-boolean v1, p0, Lcom/miui/server/turbosched/GameTurboAction;->isFLWEnable:Z

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", version: "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "/sys/module/migt/parameters/version"

    invoke-static {v1}, Lcom/miui/server/turbosched/TurboSchedUtil;->readValueFromFile(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v0}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V

    .line 81
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, " freq enable: "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-boolean v1, p0, Lcom/miui/server/turbosched/GameTurboAction;->isFLWFreqEnable:Z

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v0}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V

    .line 82
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, " target delta: "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "/sys/module/migt/parameters/target_delta"

    invoke-static {v1}, Lcom/miui/server/turbosched/TurboSchedUtil;->readBooleanValueFromFile(Ljava/lang/String;)Z

    move-result v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v0}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V

    .line 83
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, " up thresh margin: "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "/sys/module/migt/parameters/up_thresh_margin"

    invoke-static {v1}, Lcom/miui/server/turbosched/TurboSchedUtil;->readValueFromFile(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v0}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V

    .line 84
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, " down thresh margin: "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "/sys/module/migt/parameters/down_thresh_margin"

    invoke-static {v1}, Lcom/miui/server/turbosched/TurboSchedUtil;->readValueFromFile(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v0}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V

    .line 85
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, " choose wake state: "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "/sys/module/migt/parameters/choose_wake_state"

    invoke-static {v1}, Lcom/miui/server/turbosched/TurboSchedUtil;->readValueFromFile(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v0}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V

    .line 86
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, " choose load policy: "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "/sys/module/migt/parameters/choose_load_policy"

    invoke-static {v1}, Lcom/miui/server/turbosched/TurboSchedUtil;->readValueFromFile(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v0}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V

    .line 87
    return-void
.end method

.method private printSoCGfltConfig(Ljava/io/PrintWriter;)V
    .locals 2
    .param p1, "pw"    # Ljava/io/PrintWriter;

    .line 90
    const-string v0, "gflt :"

    invoke-virtual {p1, v0}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V

    .line 91
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, " enable: "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-boolean v1, p0, Lcom/miui/server/turbosched/GameTurboAction;->isSoCGfltEnable:Z

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v0}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V

    .line 92
    return-void
.end method

.method private printSocDfltConfig(Ljava/io/PrintWriter;)V
    .locals 2
    .param p1, "pw"    # Ljava/io/PrintWriter;

    .line 95
    const-string v0, "dflt :"

    invoke-virtual {p1, v0}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V

    .line 96
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, " version: "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "/sys/module/mist/parameters/mist_version"

    invoke-static {v1}, Lcom/miui/server/turbosched/TurboSchedUtil;->readValueFromFile(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v0}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V

    .line 97
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, " bw enable: "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-boolean v1, p0, Lcom/miui/server/turbosched/GameTurboAction;->isSocDfltBwEnable:Z

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v0}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V

    .line 98
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, " lat enable: "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-boolean v1, p0, Lcom/miui/server/turbosched/GameTurboAction;->isSocDfltLatEnable:Z

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v0}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V

    .line 99
    return-void
.end method

.method private readConfigValue()V
    .locals 1

    .line 284
    iget-boolean v0, p0, Lcom/miui/server/turbosched/GameTurboAction;->isFLWAccess:Z

    if-eqz v0, :cond_0

    .line 285
    const-string v0, "/sys/module/migt/parameters/flw_enable"

    invoke-static {v0}, Lcom/miui/server/turbosched/TurboSchedUtil;->readBooleanValueFromFile(Ljava/lang/String;)Z

    move-result v0

    iput-boolean v0, p0, Lcom/miui/server/turbosched/GameTurboAction;->isFLWEnable:Z

    .line 287
    :cond_0
    iget-boolean v0, p0, Lcom/miui/server/turbosched/GameTurboAction;->isFLWFreqAccess:Z

    if-eqz v0, :cond_1

    .line 288
    const-string v0, "/sys/module/migt/parameters/flw_freq_enable"

    invoke-static {v0}, Lcom/miui/server/turbosched/TurboSchedUtil;->readBooleanValueFromFile(Ljava/lang/String;)Z

    move-result v0

    iput-boolean v0, p0, Lcom/miui/server/turbosched/GameTurboAction;->isFLWFreqEnable:Z

    .line 290
    :cond_1
    iget-boolean v0, p0, Lcom/miui/server/turbosched/GameTurboAction;->isSoCGfltAccess:Z

    if-eqz v0, :cond_2

    .line 291
    const-string v0, "/sys/module/mist/parameters/gflt_enable"

    invoke-static {v0}, Lcom/miui/server/turbosched/TurboSchedUtil;->readBooleanValueFromFile(Ljava/lang/String;)Z

    move-result v0

    iput-boolean v0, p0, Lcom/miui/server/turbosched/GameTurboAction;->isSoCGfltEnable:Z

    .line 293
    :cond_2
    iget-boolean v0, p0, Lcom/miui/server/turbosched/GameTurboAction;->isSocDfltBwAccess:Z

    if-eqz v0, :cond_3

    .line 294
    const-string v0, "/sys/module/mist/parameters/dflt_bw_enable"

    invoke-static {v0}, Lcom/miui/server/turbosched/TurboSchedUtil;->readBooleanValueFromFile(Ljava/lang/String;)Z

    move-result v0

    iput-boolean v0, p0, Lcom/miui/server/turbosched/GameTurboAction;->isSocDfltBwEnable:Z

    .line 296
    :cond_3
    iget-boolean v0, p0, Lcom/miui/server/turbosched/GameTurboAction;->isSocDfltLatAccess:Z

    if-eqz v0, :cond_4

    .line 297
    const-string v0, "/sys/module/mist/parameters/dflt_lat_enable"

    invoke-static {v0}, Lcom/miui/server/turbosched/TurboSchedUtil;->readBooleanValueFromFile(Ljava/lang/String;)Z

    move-result v0

    iput-boolean v0, p0, Lcom/miui/server/turbosched/GameTurboAction;->isSocDfltLatEnable:Z

    .line 299
    :cond_4
    return-void
.end method


# virtual methods
.method protected dump(Ljava/io/FileDescriptor;Ljava/io/PrintWriter;[Ljava/lang/String;)Z
    .locals 4
    .param p1, "fd"    # Ljava/io/FileDescriptor;
    .param p2, "pw"    # Ljava/io/PrintWriter;
    .param p3, "args"    # [Ljava/lang/String;

    .line 46
    invoke-direct {p0, p1, p2, p3}, Lcom/miui/server/turbosched/GameTurboAction;->parseDumpCommand(Ljava/io/FileDescriptor;Ljava/io/PrintWriter;[Ljava/lang/String;)Z

    move-result v0

    .line 47
    .local v0, "handled":Z
    const/4 v1, 0x1

    if-eqz v0, :cond_0

    .line 48
    return v1

    .line 51
    :cond_0
    const-string/jumbo v2, "version: 1.0.0"

    invoke-virtual {p2, v2}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V

    .line 53
    const-string v2, "--------------------current status-----------------------"

    invoke-virtual {p2, v2}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V

    .line 55
    invoke-direct {p0, p2}, Lcom/miui/server/turbosched/GameTurboAction;->printFLWConfig(Ljava/io/PrintWriter;)V

    .line 58
    invoke-direct {p0, p2}, Lcom/miui/server/turbosched/GameTurboAction;->printSoCGfltConfig(Ljava/io/PrintWriter;)V

    .line 61
    invoke-direct {p0, p2}, Lcom/miui/server/turbosched/GameTurboAction;->printSocDfltConfig(Ljava/io/PrintWriter;)V

    .line 63
    const-string v2, "--------------------kernel config-----------------------"

    invoke-virtual {p2, v2}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V

    .line 65
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "flw access: "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget-boolean v3, p0, Lcom/miui/server/turbosched/GameTurboAction;->isFLWAccess:Z

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {p2, v2}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V

    .line 66
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "flw freq access: "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget-boolean v3, p0, Lcom/miui/server/turbosched/GameTurboAction;->isFLWFreqAccess:Z

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {p2, v2}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V

    .line 69
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "gflt access: "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget-boolean v3, p0, Lcom/miui/server/turbosched/GameTurboAction;->isSoCGfltAccess:Z

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {p2, v2}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V

    .line 72
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "dflt bw access: "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget-boolean v3, p0, Lcom/miui/server/turbosched/GameTurboAction;->isSocDfltBwAccess:Z

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {p2, v2}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V

    .line 73
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "dflt lat access: "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget-boolean v3, p0, Lcom/miui/server/turbosched/GameTurboAction;->isSocDfltLatAccess:Z

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {p2, v2}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V

    .line 75
    return v1
.end method
