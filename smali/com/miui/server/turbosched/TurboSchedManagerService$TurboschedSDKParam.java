class com.miui.server.turbosched.TurboSchedManagerService$TurboschedSDKParam {
	 /* .source "TurboSchedManagerService.java" */
	 /* # annotations */
	 /* .annotation system Ldalvik/annotation/EnclosingClass; */
	 /* value = Lcom/miui/server/turbosched/TurboSchedManagerService; */
} // .end annotation
/* .annotation system Ldalvik/annotation/InnerClass; */
/* accessFlags = 0x2 */
/* name = "TurboschedSDKParam" */
} // .end annotation
/* # instance fields */
private Long mSetTime;
private Long mStartTime;
private Integer mTid;
private java.lang.String mType;
final com.miui.server.turbosched.TurboSchedManagerService this$0; //synthetic
/* # direct methods */
static Long -$$Nest$fgetmSetTime ( com.miui.server.turbosched.TurboSchedManagerService$TurboschedSDKParam p0 ) { //bridge//synthethic
/* .locals 2 */
/* iget-wide v0, p0, Lcom/miui/server/turbosched/TurboSchedManagerService$TurboschedSDKParam;->mSetTime:J */
/* return-wide v0 */
} // .end method
static Long -$$Nest$fgetmStartTime ( com.miui.server.turbosched.TurboSchedManagerService$TurboschedSDKParam p0 ) { //bridge//synthethic
/* .locals 2 */
/* iget-wide v0, p0, Lcom/miui/server/turbosched/TurboSchedManagerService$TurboschedSDKParam;->mStartTime:J */
/* return-wide v0 */
} // .end method
static Integer -$$Nest$fgetmTid ( com.miui.server.turbosched.TurboSchedManagerService$TurboschedSDKParam p0 ) { //bridge//synthethic
/* .locals 0 */
/* iget p0, p0, Lcom/miui/server/turbosched/TurboSchedManagerService$TurboschedSDKParam;->mTid:I */
} // .end method
public com.miui.server.turbosched.TurboSchedManagerService$TurboschedSDKParam ( ) {
/* .locals 0 */
/* .param p2, "startTime" # J */
/* .param p4, "setTime" # J */
/* .param p6, "tid" # I */
/* .param p7, "type" # Ljava/lang/String; */
/* .line 412 */
this.this$0 = p1;
/* invoke-direct {p0}, Ljava/lang/Object;-><init>()V */
/* .line 413 */
/* iput-wide p2, p0, Lcom/miui/server/turbosched/TurboSchedManagerService$TurboschedSDKParam;->mStartTime:J */
/* .line 414 */
/* iput-wide p4, p0, Lcom/miui/server/turbosched/TurboSchedManagerService$TurboschedSDKParam;->mSetTime:J */
/* .line 415 */
/* iput p6, p0, Lcom/miui/server/turbosched/TurboSchedManagerService$TurboschedSDKParam;->mTid:I */
/* .line 416 */
this.mType = p7;
/* .line 417 */
return;
} // .end method
