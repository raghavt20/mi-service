class com.miui.server.turbosched.TurboSchedManagerService$8 extends android.database.ContentObserver {
	 /* .source "TurboSchedManagerService.java" */
	 /* # annotations */
	 /* .annotation system Ldalvik/annotation/EnclosingClass; */
	 /* value = Lcom/miui/server/turbosched/TurboSchedManagerService; */
} // .end annotation
/* .annotation system Ldalvik/annotation/InnerClass; */
/* accessFlags = 0x0 */
/* name = null */
} // .end annotation
/* # instance fields */
final com.miui.server.turbosched.TurboSchedManagerService this$0; //synthetic
/* # direct methods */
 com.miui.server.turbosched.TurboSchedManagerService$8 ( ) {
/* .locals 0 */
/* .param p1, "this$0" # Lcom/miui/server/turbosched/TurboSchedManagerService; */
/* .param p2, "handler" # Landroid/os/Handler; */
/* .line 744 */
this.this$0 = p1;
/* invoke-direct {p0, p2}, Landroid/database/ContentObserver;-><init>(Landroid/os/Handler;)V */
return;
} // .end method
/* # virtual methods */
public void onChange ( Boolean p0, android.net.Uri p1 ) {
/* .locals 1 */
/* .param p1, "selfChange" # Z */
/* .param p2, "uri" # Landroid/net/Uri; */
/* .line 747 */
if ( p2 != null) { // if-eqz p2, :cond_0
	 final String v0 = "cloud_turbo_sched_thermal_break_threshold"; // const-string v0, "cloud_turbo_sched_thermal_break_threshold"
	 android.provider.Settings$System .getUriFor ( v0 );
	 v0 = 	 (( android.net.Uri ) p2 ).equals ( v0 ); // invoke-virtual {p2, v0}, Landroid/net/Uri;->equals(Ljava/lang/Object;)Z
	 if ( v0 != null) { // if-eqz v0, :cond_0
		 /* .line 748 */
		 v0 = this.this$0;
		 com.miui.server.turbosched.TurboSchedManagerService .-$$Nest$mupdateCloudThermalBreakThresholdProp ( v0 );
		 /* .line 750 */
	 } // :cond_0
	 return;
} // .end method
