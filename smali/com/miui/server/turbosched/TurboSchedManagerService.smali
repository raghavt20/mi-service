.class public Lcom/miui/server/turbosched/TurboSchedManagerService;
.super Lmiui/turbosched/ITurboSchedManager$Stub;
.source "TurboSchedManagerService.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/miui/server/turbosched/TurboSchedManagerService$TurboSchedHandler;,
        Lcom/miui/server/turbosched/TurboSchedManagerService$TurboSchedBroadcastReceiver;,
        Lcom/miui/server/turbosched/TurboSchedManagerService$TraceInfo;,
        Lcom/miui/server/turbosched/TurboSchedManagerService$TurboschedSDKParam;,
        Lcom/miui/server/turbosched/TurboSchedManagerService$Lifecycle;
    }
.end annotation


# static fields
.field private static final BOARD_TEMP_FILE:Ljava/lang/String; = "/sys/class/thermal/thermal_message/board_sensor_temp"

.field public static final BOOST_ENABLE_PATH:Ljava/lang/String; = "/sys/module/metis/parameters/mi_fboost_enable"

.field public static final BOOST_SCHED_PATH:Ljava/lang/String; = "/sys/module/metis/parameters/boost_task"

.field public static final BOOST_THERMAL_PATH:Ljava/lang/String; = "/sys/class/thermal/thermal_message/boost"

.field private static final CLOUD_TURBO_SCHED_ALLOW_LIST:Ljava/lang/String; = "cloud_turbo_sched_allow_list"

.field private static final CLOUD_TURBO_SCHED_DPS_ENABLE:Ljava/lang/String; = "cloud_turbo_sched_dps_enable"

.field private static final CLOUD_TURBO_SCHED_ENABLE_ALL:Ljava/lang/String; = "cloud_turbo_sched_enable"

.field private static final CLOUD_TURBO_SCHED_ENABLE_CORE_APP_OPTIMIZER:Ljava/lang/String; = "cloud_turbo_sched_enable_core_app_optimizer"

.field private static final CLOUD_TURBO_SCHED_ENABLE_CORE_TOP20_APP_OPTIMIZER:Ljava/lang/String; = "cloud_turbo_sched_enable_core_top20_app_optimizer"

.field private static final CLOUD_TURBO_SCHED_ENABLE_V2:Ljava/lang/String; = "cloud_turbo_sched_enable_v2"

.field private static final CLOUD_TURBO_SCHED_FRAME_TURBO_WHITE_LIST:Ljava/lang/String; = "cloud_turbo_sched_frame_turbo_white_list"

.field private static final CLOUD_TURBO_SCHED_LINK_APP_LIST:Ljava/lang/String; = "cloud_turbo_sched_link_app_list"

.field private static final CLOUD_TURBO_SCHED_POLICY_LIST:Ljava/lang/String; = "cloud_turbo_sched_policy_list"

.field private static final CLOUD_TURBO_SCHED_THERMAL_BREAK_ENABLE:Ljava/lang/String; = "cloud_turbo_sched_thermal_break_enable"

.field private static final COMMAND_DRY_RUN:Ljava/lang/String; = "dry_run"

.field private static final ClOUD_TURBO_SCHED_THERMAL_BREAK_THRESHOLD:Ljava/lang/String; = "cloud_turbo_sched_thermal_break_threshold"

.field public static final DEBUG:Z

.field private static DEFAULT_APP_LIST:Ljava/util/List; = null
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private static DEFAULT_GL_APP_LIST:Ljava/util/List; = null
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private static DEFAULT_TOP20_APP_LIST:Ljava/util/List; = null
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field public static final DPS_FORCE_CLUSTER_SCHED_ENABLE_PATH:Ljava/lang/String; = "/sys/module/metis/parameters/force_cluster_sched_enable"

.field public static final DPS_FORCE_FORBIDDEN_WALT_LB_PATH:Ljava/lang/String; = "/sys/module/metis/parameters/force_forbidden_walt_lb"

.field public static final DPS_FORCE_VIPTASK_SELECT_RQ_PATH:Ljava/lang/String; = "/sys/module/metis/parameters/force_viptask_select_rq"

.field public static final DPS_IP_PREFER_CLUSTER_PATH:Ljava/lang/String; = "/sys/module/metis/parameters/ip_prefer_cluster"

.field public static final DPS_RENDER_PREFER_CLUSTER_PATH:Ljava/lang/String; = "/sys/module/metis/parameters/render_prefer_cluster"

.field public static final DPS_STASK_PREFER_CLUSTER_PATH:Ljava/lang/String; = "/sys/module/metis/parameters/stask_prefer_cluster"

.field public static final DPS_VIP_PREFER_FREQ_PATH:Ljava/lang/String; = "/sys/module/metis/parameters/vip_prefer_freq"

.field public static final GAEA_PATH:Ljava/lang/String; = "/sys/module/gaea/parameters/mi_gaea_enable"

.field private static final GET_BUFFER_TX_COUNT_TRANSACTION:I = 0x3f3

.field protected static final MAX_HISTORY_ITEMS:I

.field private static final METIS_BOOST_DURATION:I = 0x28

.field private static final METIS_BOOST_DURATION_PATH:Ljava/lang/String; = "sys/module/metis/parameters/mi_boost_duration"

.field private static final METIS_DEV_PATH:Ljava/lang/String; = "/dev/metis"

.field private static final METIS_IOCTL_BOOST_CMD:I = 0x6

.field public static final METIS_VERSION_PATH:Ljava/lang/String; = "/sys/module/metis/parameters/version"

.field private static final NO_THERMAL_BREAK_STRING:Ljava/lang/String; = " cpu4:2419200 cpu7:2841600"

.field public static final ORI_TURBO_SCHED_PATH:Ljava/lang/String; = "/sys/module/migt/parameters/mi_viptask"

.field private static final PROCESS_ALIEXPRESS:Ljava/lang/String; = "com.alibaba.aliexpresshd"

.field private static final PROCESS_ALIPAY:Ljava/lang/String; = "com.eg.android.AlipayGphone"

.field private static final PROCESS_AMAZON_SHOPPING:Ljava/lang/String; = "com.amazon.mShop.android.shopping"

.field private static final PROCESS_ARTICLE_LITE:Ljava/lang/String; = "com.ss.android.article.lite"

.field private static final PROCESS_ARTICLE_NEWS:Ljava/lang/String; = "com.ss.android.article.news"

.field private static final PROCESS_ARTICLE_VIDEO:Ljava/lang/String; = "com.ss.android.article.video"

.field private static final PROCESS_AWEME:Ljava/lang/String; = "com.ss.android.ugc.aweme"

.field private static final PROCESS_AWEME_LITE:Ljava/lang/String; = "com.ss.android.ugc.aweme.lite"

.field private static final PROCESS_BAIDU:Ljava/lang/String; = "com.baidu.searchbox"

.field private static final PROCESS_BAIDU_DISK:Ljava/lang/String; = "com.baidu.netdisk"

.field private static final PROCESS_BILIBILI:Ljava/lang/String; = "tv.danmaku.bili"

.field private static final PROCESS_BILIBILI_HD:Ljava/lang/String; = "tv.danmaku.bilibilihd"

.field private static final PROCESS_DISCORD:Ljava/lang/String; = "com.discord"

.field private static final PROCESS_DOUYU:Ljava/lang/String; = "air.tv.douyu.android"

.field private static final PROCESS_DRAGON_READ:Ljava/lang/String; = "com.dragon.read"

.field private static final PROCESS_FACEBOOK:Ljava/lang/String; = "com.facebook.katana"

.field private static final PROCESS_GIFMAKER:Ljava/lang/String; = "com.smile.gifmaker"

.field private static final PROCESS_HUNANTV_IMGO:Ljava/lang/String; = "com.hunantv.imgo.activity"

.field private static final PROCESS_HUYA:Ljava/lang/String; = "com.duowan.kiwi"

.field private static final PROCESS_INSHOT:Ljava/lang/String; = "com.camerasideas.instashot"

.field private static final PROCESS_INSTAGRAM:Ljava/lang/String; = "com.instagram.android"

.field private static final PROCESS_KMXS_READER:Ljava/lang/String; = "com.kmxs.reader"

.field private static final PROCESS_KUAISHOU_NEBULA:Ljava/lang/String; = "com.kuaishou.nebula"

.field private static final PROCESS_KUGOU:Ljava/lang/String; = "com.kugou.android"

.field private static final PROCESS_LAZADA:Ljava/lang/String; = "com.lazada.android"

.field private static final PROCESS_LINKEDIN:Ljava/lang/String; = "com.linkedin.android"

.field private static final PROCESS_MINIMAP:Ljava/lang/String; = "com.autonavi.minimap"

.field private static final PROCESS_MOBILEQQ:Ljava/lang/String; = "com.tencent.mobileqq"

.field private static final PROCESS_MOBILEQQ_QZONE:Ljava/lang/String; = "com.tencent.mobileqq:qzone"

.field private static final PROCESS_MOJ:Ljava/lang/String; = "in.mohalla.video"

.field private static final PROCESS_MX_PLAYER:Ljava/lang/String; = "com.mxtech.videoplayer.ad"

.field private static final PROCESS_OUTLOOK:Ljava/lang/String; = "com.microsoft.office.outlook"

.field private static final PROCESS_PINDUODUO:Ljava/lang/String; = "com.xunmeng.pinduoduo"

.field private static final PROCESS_PINTEREST:Ljava/lang/String; = "com.pinterest"

.field private static final PROCESS_QIYI:Ljava/lang/String; = "com.qiyi.video"

.field private static final PROCESS_QQMUSIC:Ljava/lang/String; = "com.tencent.qqmusic"

.field private static final PROCESS_QUARK_BROWSER:Ljava/lang/String; = "com.quark.browser"

.field private static final PROCESS_RIMET:Ljava/lang/String; = "com.alibaba.android.rimet"

.field private static final PROCESS_SHARECHAT:Ljava/lang/String; = "in.mohalla.sharechat"

.field private static final PROCESS_SHAZAM:Ljava/lang/String; = "com.shazam.android"

.field private static final PROCESS_SHEIN:Ljava/lang/String; = "com.zzkko"

.field private static final PROCESS_SNAPCHAT:Ljava/lang/String; = "com.snapchat.android"

.field private static final PROCESS_SPOTIFY:Ljava/lang/String; = "com.spotify.music"

.field private static final PROCESS_TAOBAO:Ljava/lang/String; = "com.taobao.taobao"

.field private static final PROCESS_TELEGRAM:Ljava/lang/String; = "org.telegram.messenger"

.field private static final PROCESS_TENCENT_MTT:Ljava/lang/String; = "com.tencent.mtt"

.field private static final PROCESS_TENCENT_NEWS:Ljava/lang/String; = "com.tencent.news"

.field private static final PROCESS_TENCENT_QQLIVE:Ljava/lang/String; = "com.tencent.qqlive"

.field private static final PROCESS_TRUECALLER:Ljava/lang/String; = "com.truecaller"

.field private static final PROCESS_UCMOBILE:Ljava/lang/String; = "com.UCMobile"

.field private static final PROCESS_WA_BUSINESS:Ljava/lang/String; = "com.whatsapp.w4b"

.field private static final PROCESS_WECHAT:Ljava/lang/String; = "com.tencent.mm"

.field private static final PROCESS_WEIBO:Ljava/lang/String; = "com.sina.weibo"

.field private static final PROCESS_WHATSAPP:Ljava/lang/String; = "com.whatsapp"

.field private static final PROCESS_WPS_OFFICE:Ljava/lang/String; = "cn.wps.moffice_eng"

.field private static final PROCESS_X:Ljava/lang/String; = "com.twitter.android"

.field private static final PROCESS_XHS:Ljava/lang/String; = "com.xingin.xhs"

.field private static final PROCESS_XUEXI:Ljava/lang/String; = "cn.xuexi.android"

.field private static final PROCESS_ZHIHU:Ljava/lang/String; = "com.zhihu.android"

.field private static PR_NAME:Ljava/lang/String; = null

.field public static final RECORD_RT_PATH:Ljava/lang/String; = "/sys/module/metis/parameters/rt_binder_client"

.field public static final SERVICE_NAME:Ljava/lang/String; = "turbosched"

.field private static TABLET_TOP20_APP_LIST:Ljava/util/List; = null
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private static TABLET_TOP8_APP_LIST:Ljava/util/List; = null
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field public static final TAG:Ljava/lang/String; = "TurboSchedManagerService"

.field private static TEMPREATURE_THROSHOLD_BOOST_STRING_MAP:Ljava/util/Map; = null
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map<",
            "Ljava/lang/Integer;",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private static final TURBO_SCHED_BOOST_WITH_FREQUENCY_PATH:Ljava/lang/String; = "/sys/module/metis/parameters/add_mi_viptask_enqueue_boost"

.field private static final TURBO_SCHED_CANCEL_BOOST_WITH_FREQUENCY_PATH:Ljava/lang/String; = "/sys/module/metis/parameters/del_mi_viptask_enqueue_boost"

.field private static final TURBO_SCHED_CANCEL_LITTLE_CORE_PATH:Ljava/lang/String; = "/sys/module/metis/parameters/del_mi_viptask_sched_lit_core"

.field private static final TURBO_SCHED_CANCEL_PRIORITY_PATH:Ljava/lang/String; = "/sys/module/metis/parameters/del_mi_viptask_sched_priority"

.field private static final TURBO_SCHED_LITTLE_CORE_PATH:Ljava/lang/String; = "/sys/module/metis/parameters/add_mi_viptask_sched_lit_core"

.field private static final TURBO_SCHED_LOCK_BLOCKED_PID_PATH:Ljava/lang/String; = "/sys/module/metis/parameters/mi_lock_blocked_pid"

.field public static final TURBO_SCHED_PATH:Ljava/lang/String; = "/sys/module/metis/parameters/mi_viptask"

.field private static final TURBO_SCHED_PRIORITY_PATH:Ljava/lang/String; = "/sys/module/metis/parameters/add_mi_viptask_sched_priority"

.field private static final TURBO_SCHED_THERMAL_BREAK_PATH:Ljava/lang/String; = "/sys/module/metis/parameters/is_break_enable"

.field private static final VERSION:Ljava/lang/String; = "v4.0.0"

.field public static final VIP_LINK_PATH:Ljava/lang/String; = "/sys/module/metis/parameters/vip_link_enable"

.field private static mTurboLock:Ljava/lang/Object;


# instance fields
.field private final BufferTxCountPath:Ljava/lang/String;

.field TurboSchedSDKTimer:Ljava/util/Timer;

.field TurboSchedSDKTimerTask:Ljava/util/TimerTask;

.field private enableGaeaAppProcessName:Ljava/lang/String;

.field private enableLinkVipAppProcessName:Ljava/lang/String;

.field private m8450Devices:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private m8475Devices:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private m8550Devices:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private final mActivityManagerService:Landroid/app/IActivityManager;

.field private mAppsLinkVipList:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private mAppsLockContentionList:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private mAppsLockContentionPid:I

.field private mBWFTidStartTimeMap:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map<",
            "Ljava/lang/Integer;",
            "Ljava/lang/Long;",
            ">;"
        }
    .end annotation
.end field

.field private mBoostDuration:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List<",
            "Ljava/lang/Long;",
            ">;"
        }
    .end annotation
.end field

.field private mCallerAllowList:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field final mCloudAllowListObserver:Landroid/database/ContentObserver;

.field private mCloudControlEnabled:Z

.field final mCloudCoreAppOptimizerEnableObserver:Landroid/database/ContentObserver;

.field final mCloudCoreTop20AppOptimizerEnableObserver:Landroid/database/ContentObserver;

.field final mCloudDpsEnableObserver:Landroid/database/ContentObserver;

.field final mCloudFrameTurboWhiteListObserver:Landroid/database/ContentObserver;

.field final mCloudLinkWhiteListObserver:Landroid/database/ContentObserver;

.field final mCloudPolicyListObserver:Landroid/database/ContentObserver;

.field final mCloudSwichObserver:Landroid/database/ContentObserver;

.field final mCloudThermalBreakEnableObserver:Landroid/database/ContentObserver;

.field final mCloudThermalBreakThresholdObserver:Landroid/database/ContentObserver;

.field final mCloudTurboschedMiuiSdkObserver:Landroid/database/ContentObserver;

.field private mContext:Landroid/content/Context;

.field private mCoreAppsPackageNameList:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private mDPSEnable:Z

.field private mDPSSupport:Z

.field private mEnableTop20:Z

.field private mFgCorePid:J

.field private mFgCoreUid:J

.field private mFrameTurboAction:Lcom/miui/server/turbosched/FrameTurboAction;

.field private mFrameTurboAppMap:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map<",
            "Ljava/lang/String;",
            "Ljava/util/List<",
            "Ljava/lang/String;",
            ">;>;"
        }
    .end annotation
.end field

.field private mGaeaEnable:Z

.field private mGameTurboAction:Lcom/miui/server/turbosched/GameTurboAction;

.field public mHandlerThread:Landroid/os/HandlerThread;

.field private mHistoryLog:Landroid/util/LocalLog;

.field private mInStartUpMode:Z

.field private mIsGlobal:Z

.field private mIsOnScroll:Z

.field private mKernelEnabelUpdate:Z

.field private mLastVsyncId:J

.field private mLinkEnable:Z

.field private mMetisVersion:Ljava/lang/String;

.field private mPm:Landroid/content/pm/PackageManager;

.field private mPolicyListBackup:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private mPriorityTidStartTimeMap:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map<",
            "Ljava/lang/Integer;",
            "Ljava/lang/Long;",
            ">;"
        }
    .end annotation
.end field

.field private mProcessName:Ljava/lang/String;

.field private final mProcessObserver:Landroid/app/IProcessObserver$Stub;

.field private mReceiveCloudData:Z

.field private mRegistedForegroundReceiver:Z

.field private mScreenOn:Z

.field private mStartUpAllowList:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private mStartUpModeInitFinish:Z

.field private mSupportGaea:Z

.field private mSurfaceFlinger:Landroid/os/IBinder;

.field private mThermalBreakEnabled:Z

.field private mTurboEnabled:Z

.field private mTurboEnabledV2:Z

.field private mTurboNodeExist:Z

.field private mTurboSchedHandler:Lcom/miui/server/turbosched/TurboSchedManagerService$TurboSchedHandler;

.field private mTurboschedSDKParamMap:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map<",
            "Ljava/lang/String;",
            "Lcom/miui/server/turbosched/TurboSchedManagerService$TurboschedSDKParam;",
            ">;"
        }
    .end annotation
.end field

.field private mTurboschedSdkTidsList:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List<",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation
.end field

.field private mTurboschedSdkTidsLock:Ljava/lang/Object;


# direct methods
.method static bridge synthetic -$$Nest$fgetmAppsLockContentionList(Lcom/miui/server/turbosched/TurboSchedManagerService;)Ljava/util/List;
    .locals 0

    iget-object p0, p0, Lcom/miui/server/turbosched/TurboSchedManagerService;->mAppsLockContentionList:Ljava/util/List;

    return-object p0
.end method

.method static bridge synthetic -$$Nest$fgetmAppsLockContentionPid(Lcom/miui/server/turbosched/TurboSchedManagerService;)I
    .locals 0

    iget p0, p0, Lcom/miui/server/turbosched/TurboSchedManagerService;->mAppsLockContentionPid:I

    return p0
.end method

.method static bridge synthetic -$$Nest$fgetmContext(Lcom/miui/server/turbosched/TurboSchedManagerService;)Landroid/content/Context;
    .locals 0

    iget-object p0, p0, Lcom/miui/server/turbosched/TurboSchedManagerService;->mContext:Landroid/content/Context;

    return-object p0
.end method

.method static bridge synthetic -$$Nest$fgetmFrameTurboAction(Lcom/miui/server/turbosched/TurboSchedManagerService;)Lcom/miui/server/turbosched/FrameTurboAction;
    .locals 0

    iget-object p0, p0, Lcom/miui/server/turbosched/TurboSchedManagerService;->mFrameTurboAction:Lcom/miui/server/turbosched/FrameTurboAction;

    return-object p0
.end method

.method static bridge synthetic -$$Nest$fgetmHistoryLog(Lcom/miui/server/turbosched/TurboSchedManagerService;)Landroid/util/LocalLog;
    .locals 0

    iget-object p0, p0, Lcom/miui/server/turbosched/TurboSchedManagerService;->mHistoryLog:Landroid/util/LocalLog;

    return-object p0
.end method

.method static bridge synthetic -$$Nest$fgetmProcessName(Lcom/miui/server/turbosched/TurboSchedManagerService;)Ljava/lang/String;
    .locals 0

    iget-object p0, p0, Lcom/miui/server/turbosched/TurboSchedManagerService;->mProcessName:Ljava/lang/String;

    return-object p0
.end method

.method static bridge synthetic -$$Nest$fgetmScreenOn(Lcom/miui/server/turbosched/TurboSchedManagerService;)Z
    .locals 0

    iget-boolean p0, p0, Lcom/miui/server/turbosched/TurboSchedManagerService;->mScreenOn:Z

    return p0
.end method

.method static bridge synthetic -$$Nest$fgetmStartUpModeInitFinish(Lcom/miui/server/turbosched/TurboSchedManagerService;)Z
    .locals 0

    iget-boolean p0, p0, Lcom/miui/server/turbosched/TurboSchedManagerService;->mStartUpModeInitFinish:Z

    return p0
.end method

.method static bridge synthetic -$$Nest$fgetmTurboschedSDKParamMap(Lcom/miui/server/turbosched/TurboSchedManagerService;)Ljava/util/Map;
    .locals 0

    iget-object p0, p0, Lcom/miui/server/turbosched/TurboSchedManagerService;->mTurboschedSDKParamMap:Ljava/util/Map;

    return-object p0
.end method

.method static bridge synthetic -$$Nest$fgetmTurboschedSdkTidsLock(Lcom/miui/server/turbosched/TurboSchedManagerService;)Ljava/lang/Object;
    .locals 0

    iget-object p0, p0, Lcom/miui/server/turbosched/TurboSchedManagerService;->mTurboschedSdkTidsLock:Ljava/lang/Object;

    return-object p0
.end method

.method static bridge synthetic -$$Nest$fputmAppsLockContentionPid(Lcom/miui/server/turbosched/TurboSchedManagerService;I)V
    .locals 0

    iput p1, p0, Lcom/miui/server/turbosched/TurboSchedManagerService;->mAppsLockContentionPid:I

    return-void
.end method

.method static bridge synthetic -$$Nest$fputmFgCorePid(Lcom/miui/server/turbosched/TurboSchedManagerService;J)V
    .locals 0

    iput-wide p1, p0, Lcom/miui/server/turbosched/TurboSchedManagerService;->mFgCorePid:J

    return-void
.end method

.method static bridge synthetic -$$Nest$fputmFgCoreUid(Lcom/miui/server/turbosched/TurboSchedManagerService;J)V
    .locals 0

    iput-wide p1, p0, Lcom/miui/server/turbosched/TurboSchedManagerService;->mFgCoreUid:J

    return-void
.end method

.method static bridge synthetic -$$Nest$fputmProcessName(Lcom/miui/server/turbosched/TurboSchedManagerService;Ljava/lang/String;)V
    .locals 0

    iput-object p1, p0, Lcom/miui/server/turbosched/TurboSchedManagerService;->mProcessName:Ljava/lang/String;

    return-void
.end method

.method static bridge synthetic -$$Nest$fputmScreenOn(Lcom/miui/server/turbosched/TurboSchedManagerService;Z)V
    .locals 0

    iput-boolean p1, p0, Lcom/miui/server/turbosched/TurboSchedManagerService;->mScreenOn:Z

    return-void
.end method

.method static bridge synthetic -$$Nest$mexitStartUpMode(Lcom/miui/server/turbosched/TurboSchedManagerService;)V
    .locals 0

    invoke-direct {p0}, Lcom/miui/server/turbosched/TurboSchedManagerService;->exitStartUpMode()V

    return-void
.end method

.method static bridge synthetic -$$Nest$mgetProcessNameByPid(Lcom/miui/server/turbosched/TurboSchedManagerService;I)Ljava/lang/String;
    .locals 0

    invoke-direct {p0, p1}, Lcom/miui/server/turbosched/TurboSchedManagerService;->getProcessNameByPid(I)Ljava/lang/String;

    move-result-object p0

    return-object p0
.end method

.method static bridge synthetic -$$Nest$mupdateCloudAllowListProp(Lcom/miui/server/turbosched/TurboSchedManagerService;)V
    .locals 0

    invoke-direct {p0}, Lcom/miui/server/turbosched/TurboSchedManagerService;->updateCloudAllowListProp()V

    return-void
.end method

.method static bridge synthetic -$$Nest$mupdateCloudDpsEnableProp(Lcom/miui/server/turbosched/TurboSchedManagerService;)V
    .locals 0

    invoke-direct {p0}, Lcom/miui/server/turbosched/TurboSchedManagerService;->updateCloudDpsEnableProp()V

    return-void
.end method

.method static bridge synthetic -$$Nest$mupdateCloudFrameTurboWhiteListProp(Lcom/miui/server/turbosched/TurboSchedManagerService;)V
    .locals 0

    invoke-direct {p0}, Lcom/miui/server/turbosched/TurboSchedManagerService;->updateCloudFrameTurboWhiteListProp()V

    return-void
.end method

.method static bridge synthetic -$$Nest$mupdateCloudLinkWhiteListProp(Lcom/miui/server/turbosched/TurboSchedManagerService;)V
    .locals 0

    invoke-direct {p0}, Lcom/miui/server/turbosched/TurboSchedManagerService;->updateCloudLinkWhiteListProp()V

    return-void
.end method

.method static bridge synthetic -$$Nest$mupdateCloudPolicyListProp(Lcom/miui/server/turbosched/TurboSchedManagerService;)V
    .locals 0

    invoke-direct {p0}, Lcom/miui/server/turbosched/TurboSchedManagerService;->updateCloudPolicyListProp()V

    return-void
.end method

.method static bridge synthetic -$$Nest$mupdateCloudThermalBreakEnableProp(Lcom/miui/server/turbosched/TurboSchedManagerService;)V
    .locals 0

    invoke-direct {p0}, Lcom/miui/server/turbosched/TurboSchedManagerService;->updateCloudThermalBreakEnableProp()V

    return-void
.end method

.method static bridge synthetic -$$Nest$mupdateCloudThermalBreakThresholdProp(Lcom/miui/server/turbosched/TurboSchedManagerService;)V
    .locals 0

    invoke-direct {p0}, Lcom/miui/server/turbosched/TurboSchedManagerService;->updateCloudThermalBreakThresholdProp()V

    return-void
.end method

.method static bridge synthetic -$$Nest$mupdateCoreAppOptimizerEnableProp(Lcom/miui/server/turbosched/TurboSchedManagerService;)V
    .locals 0

    invoke-direct {p0}, Lcom/miui/server/turbosched/TurboSchedManagerService;->updateCoreAppOptimizerEnableProp()V

    return-void
.end method

.method static bridge synthetic -$$Nest$mupdateCoreTop20AppOptimizerEnableProp(Lcom/miui/server/turbosched/TurboSchedManagerService;)V
    .locals 0

    invoke-direct {p0}, Lcom/miui/server/turbosched/TurboSchedManagerService;->updateCoreTop20AppOptimizerEnableProp()V

    return-void
.end method

.method static bridge synthetic -$$Nest$mupdateEnableMiuiSdkProp(Lcom/miui/server/turbosched/TurboSchedManagerService;Z)V
    .locals 0

    invoke-direct {p0, p1}, Lcom/miui/server/turbosched/TurboSchedManagerService;->updateEnableMiuiSdkProp(Z)V

    return-void
.end method

.method static bridge synthetic -$$Nest$mupdateEnableProp(Lcom/miui/server/turbosched/TurboSchedManagerService;Z)V
    .locals 0

    invoke-direct {p0, p1}, Lcom/miui/server/turbosched/TurboSchedManagerService;->updateEnableProp(Z)V

    return-void
.end method

.method static bridge synthetic -$$Nest$mwriteTidsToPath(Lcom/miui/server/turbosched/TurboSchedManagerService;Ljava/lang/String;[I)Z
    .locals 0

    invoke-direct {p0, p1, p2}, Lcom/miui/server/turbosched/TurboSchedManagerService;->writeTidsToPath(Ljava/lang/String;[I)Z

    move-result p0

    return p0
.end method

.method static bridge synthetic -$$Nest$mwriteTurboSchedNode(Lcom/miui/server/turbosched/TurboSchedManagerService;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Z
    .locals 0

    invoke-direct {p0, p1, p2, p3}, Lcom/miui/server/turbosched/TurboSchedManagerService;->writeTurboSchedNode(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Z

    move-result p0

    return p0
.end method

.method static constructor <clinit>()V
    .locals 24

    .line 71
    sget-boolean v0, Landroid/os/Build;->IS_DEBUGGABLE:Z

    sput-boolean v0, Lcom/miui/server/turbosched/TurboSchedManagerService;->DEBUG:Z

    .line 187
    new-instance v1, Ljava/util/LinkedHashMap;

    invoke-direct {v1}, Ljava/util/LinkedHashMap;-><init>()V

    sput-object v1, Lcom/miui/server/turbosched/TurboSchedManagerService;->TEMPREATURE_THROSHOLD_BOOST_STRING_MAP:Ljava/util/Map;

    .line 189
    const/16 v2, 0x32

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    const-string v3, " cpu4:2112000 cpu7:2054400"

    invoke-interface {v1, v2, v3}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 190
    sget-object v1, Lcom/miui/server/turbosched/TurboSchedManagerService;->TEMPREATURE_THROSHOLD_BOOST_STRING_MAP:Ljava/util/Map;

    const/16 v2, 0x2d

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    const-string v3, " cpu4:2227200 cpu7:2169600"

    invoke-interface {v1, v2, v3}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 191
    sget-object v1, Lcom/miui/server/turbosched/TurboSchedManagerService;->TEMPREATURE_THROSHOLD_BOOST_STRING_MAP:Ljava/util/Map;

    const/16 v2, 0x28

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    const-string v3, " cpu4:2342400 cpu7:2284800"

    invoke-interface {v1, v2, v3}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 192
    sget-object v1, Lcom/miui/server/turbosched/TurboSchedManagerService;->TEMPREATURE_THROSHOLD_BOOST_STRING_MAP:Ljava/util/Map;

    const/16 v2, 0x23

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    const-string v3, " cpu4:2419200 cpu7:2400000"

    invoke-interface {v1, v2, v3}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 198
    if-eqz v0, :cond_0

    const/16 v0, 0x4000

    goto :goto_0

    :cond_0
    const/16 v0, 0x1000

    :goto_0
    sput v0, Lcom/miui/server/turbosched/TurboSchedManagerService;->MAX_HISTORY_ITEMS:I

    .line 202
    new-instance v0, Ljava/lang/Object;

    invoke-direct {v0}, Ljava/lang/Object;-><init>()V

    sput-object v0, Lcom/miui/server/turbosched/TurboSchedManagerService;->mTurboLock:Ljava/lang/Object;

    .line 242
    const-string v0, "ro.product.name"

    const-string v1, ""

    invoke-static {v0, v1}, Landroid/os/SystemProperties;->get(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/miui/server/turbosched/TurboSchedManagerService;->PR_NAME:Ljava/lang/String;

    .line 253
    new-instance v0, Ljava/util/ArrayList;

    const-string v1, "com.smile.gifmaker"

    const-string v2, "com.sina.weibo"

    const-string v3, "com.ss.android.article.news"

    const-string v4, "com.taobao.taobao"

    const-string v5, "com.tencent.mm"

    const-string v6, "com.ss.android.ugc.aweme"

    const-string/jumbo v7, "tv.danmaku.bili"

    filled-new-array/range {v1 .. v7}, [Ljava/lang/String;

    move-result-object v1

    .line 254
    invoke-static {v1}, Ljava/util/Arrays;->asList([Ljava/lang/Object;)Ljava/util/List;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/util/ArrayList;-><init>(Ljava/util/Collection;)V

    sput-object v0, Lcom/miui/server/turbosched/TurboSchedManagerService;->DEFAULT_APP_LIST:Ljava/util/List;

    .line 268
    new-instance v0, Ljava/util/ArrayList;

    const-string v1, "com.smile.gifmaker"

    const-string v2, "com.sina.weibo"

    const-string v3, "com.ss.android.article.news"

    const-string v4, "com.taobao.taobao"

    const-string v5, "com.tencent.mm"

    const-string v6, "com.ss.android.ugc.aweme"

    const-string/jumbo v7, "tv.danmaku.bili"

    const-string v8, "com.kuaishou.nebula"

    const-string v9, "com.tencent.mobileqq"

    const-string v10, "com.tencent.mobileqq:qzone"

    const-string v11, "com.ss.android.ugc.aweme.lite"

    const-string v12, "com.tencent.qqlive"

    const-string v13, "com.ss.android.article.lite"

    const-string v14, "com.baidu.searchbox"

    const-string v15, "com.xunmeng.pinduoduo"

    const-string v16, "com.UCMobile"

    const-string v17, "com.dragon.read"

    const-string v18, "com.qiyi.video"

    const-string v19, "com.ss.android.article.video"

    const-string v20, "com.eg.android.AlipayGphone"

    const-string v21, "com.tencent.mtt"

    const-string v22, "com.kmxs.reader"

    filled-new-array/range {v1 .. v22}, [Ljava/lang/String;

    move-result-object v1

    .line 269
    invoke-static {v1}, Ljava/util/Arrays;->asList([Ljava/lang/Object;)Ljava/util/List;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/util/ArrayList;-><init>(Ljava/util/Collection;)V

    sput-object v0, Lcom/miui/server/turbosched/TurboSchedManagerService;->DEFAULT_TOP20_APP_LIST:Ljava/util/List;

    .line 295
    new-instance v0, Ljava/util/ArrayList;

    const-string v1, "com.ss.android.ugc.aweme"

    const-string v2, "com.tencent.mm"

    const-string v3, "com.alibaba.android.rimet"

    const-string v4, "com.smile.gifmaker"

    const-string v5, "com.kuaishou.nebula"

    const-string/jumbo v6, "tv.danmaku.bilibilihd"

    const-string v7, "com.ss.android.ugc.aweme.lite"

    const-string v8, "com.tencent.mobileqq"

    filled-new-array/range {v1 .. v8}, [Ljava/lang/String;

    move-result-object v1

    .line 296
    invoke-static {v1}, Ljava/util/Arrays;->asList([Ljava/lang/Object;)Ljava/util/List;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/util/ArrayList;-><init>(Ljava/util/Collection;)V

    sput-object v0, Lcom/miui/server/turbosched/TurboSchedManagerService;->TABLET_TOP8_APP_LIST:Ljava/util/List;

    .line 309
    new-instance v0, Ljava/util/ArrayList;

    const-string v1, "com.ss.android.ugc.aweme"

    const-string v2, "com.tencent.mm"

    const-string v3, "com.alibaba.android.rimet"

    const-string v4, "com.smile.gifmaker"

    const-string v5, "com.kuaishou.nebula"

    const-string/jumbo v6, "tv.danmaku.bilibilihd"

    const-string v7, "com.ss.android.ugc.aweme.lite"

    const-string v8, "com.tencent.mobileqq"

    const-string v9, "com.baidu.netdisk"

    const-string/jumbo v10, "tv.danmaku.bili"

    const-string v11, "com.ss.android.article.news"

    const-string v12, "com.baidu.searchbox"

    const-string v13, "com.xingin.xhs"

    const-string v14, "com.dragon.read"

    const-string v15, "com.tencent.qqlive"

    const-string v16, "com.ss.android.article.video"

    const-string v17, "com.ss.android.article.lite"

    const-string v18, "com.duowan.kiwi"

    const-string v19, "com.quark.browser"

    const-string v20, "air.tv.douyu.android"

    filled-new-array/range {v1 .. v20}, [Ljava/lang/String;

    move-result-object v1

    .line 310
    invoke-static {v1}, Ljava/util/Arrays;->asList([Ljava/lang/Object;)Ljava/util/List;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/util/ArrayList;-><init>(Ljava/util/Collection;)V

    sput-object v0, Lcom/miui/server/turbosched/TurboSchedManagerService;->TABLET_TOP20_APP_LIST:Ljava/util/List;

    .line 334
    new-instance v0, Ljava/util/ArrayList;

    const-string v1, "com.whatsapp"

    const-string v2, "com.facebook.katana"

    const-string v3, "com.instagram.android"

    const-string v4, "org.telegram.messenger"

    const-string v5, "com.snapchat.android"

    const-string v6, "cn.wps.moffice_eng"

    const-string v7, "com.truecaller"

    const-string v8, "com.spotify.music"

    const-string v9, "com.mxtech.videoplayer.ad"

    const-string v10, "com.twitter.android"

    const-string v11, "com.pinterest"

    const-string v12, "in.mohalla.sharechat"

    const-string v13, "com.whatsapp.w4b"

    const-string v14, "com.amazon.mShop.android.shopping"

    const-string v15, "com.linkedin.android"

    const-string v16, "in.mohalla.video"

    const-string v17, "com.alibaba.aliexpresshd"

    const-string v18, "com.camerasideas.instashot"

    const-string v19, "com.microsoft.office.outlook"

    const-string v20, "com.shazam.android"

    const-string v21, "com.lazada.android"

    const-string v22, "com.zzkko"

    const-string v23, "com.discord"

    filled-new-array/range {v1 .. v23}, [Ljava/lang/String;

    move-result-object v1

    .line 335
    invoke-static {v1}, Ljava/util/Arrays;->asList([Ljava/lang/Object;)Ljava/util/List;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/util/ArrayList;-><init>(Ljava/util/Collection;)V

    sput-object v0, Lcom/miui/server/turbosched/TurboSchedManagerService;->DEFAULT_GL_APP_LIST:Ljava/util/List;

    .line 334
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;)V
    .locals 27
    .param p1, "context"    # Landroid/content/Context;

    .line 421
    move-object/from16 v0, p0

    move-object/from16 v1, p1

    invoke-direct/range {p0 .. p0}, Lmiui/turbosched/ITurboSchedManager$Stub;-><init>()V

    .line 103
    const-string v2, "/sys/module/metis/parameters/buffer_tx_count"

    iput-object v2, v0, Lcom/miui/server/turbosched/TurboSchedManagerService;->BufferTxCountPath:Ljava/lang/String;

    .line 119
    const/4 v2, 0x0

    iput-boolean v2, v0, Lcom/miui/server/turbosched/TurboSchedManagerService;->mDPSSupport:Z

    .line 120
    const-string v3, "persist.sys.turbosched.dps.enable"

    invoke-static {v3, v2}, Landroid/os/SystemProperties;->getBoolean(Ljava/lang/String;Z)Z

    move-result v3

    iput-boolean v3, v0, Lcom/miui/server/turbosched/TurboSchedManagerService;->mDPSEnable:Z

    .line 195
    const-string v3, "persist.sys.turbosched.thermal_break.enable"

    invoke-static {v3, v2}, Landroid/os/SystemProperties;->getBoolean(Ljava/lang/String;Z)Z

    move-result v3

    iput-boolean v3, v0, Lcom/miui/server/turbosched/TurboSchedManagerService;->mThermalBreakEnabled:Z

    .line 204
    new-instance v3, Landroid/util/LocalLog;

    sget v4, Lcom/miui/server/turbosched/TurboSchedManagerService;->MAX_HISTORY_ITEMS:I

    invoke-direct {v3, v4}, Landroid/util/LocalLog;-><init>(I)V

    iput-object v3, v0, Lcom/miui/server/turbosched/TurboSchedManagerService;->mHistoryLog:Landroid/util/LocalLog;

    .line 206
    const-string v3, "persist.sys.turbosched.enable"

    invoke-static {v3, v2}, Landroid/os/SystemProperties;->getBoolean(Ljava/lang/String;Z)Z

    move-result v3

    iput-boolean v3, v0, Lcom/miui/server/turbosched/TurboSchedManagerService;->mTurboEnabled:Z

    .line 207
    const-string v3, "persist.sys.turbosched.enable_v2"

    const/4 v4, 0x1

    invoke-static {v3, v4}, Landroid/os/SystemProperties;->getBoolean(Ljava/lang/String;Z)Z

    move-result v3

    iput-boolean v3, v0, Lcom/miui/server/turbosched/TurboSchedManagerService;->mTurboEnabledV2:Z

    .line 208
    iput-boolean v2, v0, Lcom/miui/server/turbosched/TurboSchedManagerService;->mCloudControlEnabled:Z

    .line 209
    iput-boolean v2, v0, Lcom/miui/server/turbosched/TurboSchedManagerService;->mTurboNodeExist:Z

    .line 210
    const/4 v3, 0x0

    iput-object v3, v0, Lcom/miui/server/turbosched/TurboSchedManagerService;->mHandlerThread:Landroid/os/HandlerThread;

    .line 211
    iput-object v3, v0, Lcom/miui/server/turbosched/TurboSchedManagerService;->mTurboSchedHandler:Lcom/miui/server/turbosched/TurboSchedManagerService$TurboSchedHandler;

    .line 212
    iput-object v3, v0, Lcom/miui/server/turbosched/TurboSchedManagerService;->mPm:Landroid/content/pm/PackageManager;

    .line 213
    iput-object v3, v0, Lcom/miui/server/turbosched/TurboSchedManagerService;->mSurfaceFlinger:Landroid/os/IBinder;

    .line 215
    new-instance v5, Ljava/util/ArrayList;

    const-string v6, "com.miui.personalassistant"

    const-string v7, "com.miui.home"

    filled-new-array {v7, v6}, [Ljava/lang/String;

    move-result-object v6

    .line 216
    invoke-static {v6}, Ljava/util/Arrays;->asList([Ljava/lang/Object;)Ljava/util/List;

    move-result-object v6

    invoke-direct {v5, v6}, Ljava/util/ArrayList;-><init>(Ljava/util/Collection;)V

    iput-object v5, v0, Lcom/miui/server/turbosched/TurboSchedManagerService;->mCallerAllowList:Ljava/util/List;

    .line 217
    new-instance v5, Ljava/util/ArrayList;

    const-string v6, "com.android.provision"

    filled-new-array {v6}, [Ljava/lang/String;

    move-result-object v6

    .line 218
    invoke-static {v6}, Ljava/util/Arrays;->asList([Ljava/lang/Object;)Ljava/util/List;

    move-result-object v6

    invoke-direct {v5, v6}, Ljava/util/ArrayList;-><init>(Ljava/util/Collection;)V

    iput-object v5, v0, Lcom/miui/server/turbosched/TurboSchedManagerService;->mStartUpAllowList:Ljava/util/List;

    .line 219
    iput-boolean v4, v0, Lcom/miui/server/turbosched/TurboSchedManagerService;->mInStartUpMode:Z

    .line 220
    iput-boolean v2, v0, Lcom/miui/server/turbosched/TurboSchedManagerService;->mStartUpModeInitFinish:Z

    .line 221
    iput-boolean v2, v0, Lcom/miui/server/turbosched/TurboSchedManagerService;->mKernelEnabelUpdate:Z

    .line 222
    invoke-static {}, Lcom/miui/server/turbosched/TurboSchedConfig;->getPolicyList()Ljava/util/List;

    move-result-object v5

    iput-object v5, v0, Lcom/miui/server/turbosched/TurboSchedManagerService;->mPolicyListBackup:Ljava/util/List;

    .line 224
    iput-boolean v2, v0, Lcom/miui/server/turbosched/TurboSchedManagerService;->mIsOnScroll:Z

    .line 225
    const-string v5, ""

    iput-object v5, v0, Lcom/miui/server/turbosched/TurboSchedManagerService;->mProcessName:Ljava/lang/String;

    .line 226
    const-wide/16 v8, -0x1

    iput-wide v8, v0, Lcom/miui/server/turbosched/TurboSchedManagerService;->mFgCorePid:J

    .line 227
    iput-wide v8, v0, Lcom/miui/server/turbosched/TurboSchedManagerService;->mFgCoreUid:J

    .line 229
    new-instance v6, Ljava/util/ArrayList;

    invoke-direct {v6}, Ljava/util/ArrayList;-><init>()V

    iput-object v6, v0, Lcom/miui/server/turbosched/TurboSchedManagerService;->mTurboschedSdkTidsList:Ljava/util/List;

    .line 230
    new-instance v6, Ljava/lang/Object;

    invoke-direct {v6}, Ljava/lang/Object;-><init>()V

    iput-object v6, v0, Lcom/miui/server/turbosched/TurboSchedManagerService;->mTurboschedSdkTidsLock:Ljava/lang/Object;

    .line 231
    iput-boolean v2, v0, Lcom/miui/server/turbosched/TurboSchedManagerService;->mRegistedForegroundReceiver:Z

    .line 232
    const-string v6, "persist.sys.turbosched.receive.cloud.data"

    invoke-static {v6, v4}, Landroid/os/SystemProperties;->getBoolean(Ljava/lang/String;Z)Z

    move-result v6

    iput-boolean v6, v0, Lcom/miui/server/turbosched/TurboSchedManagerService;->mReceiveCloudData:Z

    .line 233
    new-instance v6, Ljava/util/ArrayList;

    const-string v8, "ishtar"

    const-string v9, "babylon"

    const-string v10, "nuwa"

    const-string v11, "fuxi"

    const-string/jumbo v12, "socrates"

    filled-new-array {v10, v11, v12, v8, v9}, [Ljava/lang/String;

    move-result-object v8

    .line 234
    invoke-static {v8}, Ljava/util/Arrays;->asList([Ljava/lang/Object;)Ljava/util/List;

    move-result-object v8

    invoke-direct {v6, v8}, Ljava/util/ArrayList;-><init>(Ljava/util/Collection;)V

    iput-object v6, v0, Lcom/miui/server/turbosched/TurboSchedManagerService;->m8550Devices:Ljava/util/List;

    .line 236
    new-instance v6, Ljava/util/ArrayList;

    const-string/jumbo v8, "yudi"

    const-string v9, "marble"

    const-string/jumbo v10, "thor"

    const-string/jumbo v11, "unicorn"

    const-string v12, "mayfly"

    const-string/jumbo v13, "zizhan"

    filled-new-array/range {v8 .. v13}, [Ljava/lang/String;

    move-result-object v8

    .line 237
    invoke-static {v8}, Ljava/util/Arrays;->asList([Ljava/lang/Object;)Ljava/util/List;

    move-result-object v8

    invoke-direct {v6, v8}, Ljava/util/ArrayList;-><init>(Ljava/util/Collection;)V

    iput-object v6, v0, Lcom/miui/server/turbosched/TurboSchedManagerService;->m8475Devices:Ljava/util/List;

    .line 239
    new-instance v6, Ljava/util/ArrayList;

    const-string/jumbo v8, "zeus"

    const-string v9, "cupid"

    filled-new-array {v8, v9}, [Ljava/lang/String;

    move-result-object v8

    .line 240
    invoke-static {v8}, Ljava/util/Arrays;->asList([Ljava/lang/Object;)Ljava/util/List;

    move-result-object v8

    invoke-direct {v6, v8}, Ljava/util/ArrayList;-><init>(Ljava/util/Collection;)V

    iput-object v6, v0, Lcom/miui/server/turbosched/TurboSchedManagerService;->m8450Devices:Ljava/util/List;

    .line 243
    sget-boolean v6, Lmiui/os/Build;->IS_INTERNATIONAL_BUILD:Z

    if-nez v6, :cond_1

    .line 244
    const-string v6, "ro.miui.region"

    const-string/jumbo v8, "unknown"

    invoke-static {v6, v8}, Landroid/os/SystemProperties;->get(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v6

    const-string v8, "CN"

    invoke-virtual {v8, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-eqz v6, :cond_1

    sget-object v6, Lcom/miui/server/turbosched/TurboSchedManagerService;->PR_NAME:Ljava/lang/String;

    .line 245
    const-string v8, "_"

    invoke-virtual {v6, v8}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v6

    if-eqz v6, :cond_0

    sget-object v6, Lcom/miui/server/turbosched/TurboSchedManagerService;->PR_NAME:Ljava/lang/String;

    const-string v8, "_cn"

    invoke-virtual {v6, v8}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v6

    if-nez v6, :cond_0

    sget-object v6, Lcom/miui/server/turbosched/TurboSchedManagerService;->PR_NAME:Ljava/lang/String;

    const-string v8, "_CN"

    invoke-virtual {v6, v8}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v6

    if-nez v6, :cond_0

    goto :goto_0

    :cond_0
    move v6, v2

    goto :goto_1

    :cond_1
    :goto_0
    move v6, v4

    :goto_1
    iput-boolean v6, v0, Lcom/miui/server/turbosched/TurboSchedManagerService;->mIsGlobal:Z

    .line 247
    const-string v6, "persist.sys.turbosched.link.enable"

    invoke-static {v6, v4}, Landroid/os/SystemProperties;->getBoolean(Ljava/lang/String;Z)Z

    move-result v6

    iput-boolean v6, v0, Lcom/miui/server/turbosched/TurboSchedManagerService;->mLinkEnable:Z

    .line 248
    const-string v6, "persist.sys.turbosched.support.gaea"

    invoke-static {v6, v2}, Landroid/os/SystemProperties;->getBoolean(Ljava/lang/String;Z)Z

    move-result v6

    iput-boolean v6, v0, Lcom/miui/server/turbosched/TurboSchedManagerService;->mSupportGaea:Z

    .line 249
    const-string v6, "persist.sys.turbosched.gaea.enable"

    invoke-static {v6, v2}, Landroid/os/SystemProperties;->getBoolean(Ljava/lang/String;Z)Z

    move-result v6

    iput-boolean v6, v0, Lcom/miui/server/turbosched/TurboSchedManagerService;->mGaeaEnable:Z

    .line 250
    const-string v6, "persist.sys.turbosched.enabletop20app"

    invoke-static {v6, v2}, Landroid/os/SystemProperties;->getBoolean(Ljava/lang/String;Z)Z

    move-result v2

    iput-boolean v2, v0, Lcom/miui/server/turbosched/TurboSchedManagerService;->mEnableTop20:Z

    .line 265
    sget-object v2, Lcom/miui/server/turbosched/TurboSchedManagerService;->DEFAULT_APP_LIST:Ljava/util/List;

    iput-object v2, v0, Lcom/miui/server/turbosched/TurboSchedManagerService;->mCoreAppsPackageNameList:Ljava/util/List;

    .line 362
    new-instance v2, Ljava/util/ArrayList;

    const-string v8, "com.smile.gifmaker"

    const-string v9, "com.kuaishou.nebula"

    const-string v10, "com.ss.android.ugc.aweme"

    const-string v11, "com.tencent.mobileqq"

    const-string v12, "com.ss.android.article.news"

    const-string v13, "com.ss.android.article.lite"

    const-string v14, "com.tencent.news"

    const-string v15, "com.alibaba.android.rimet"

    const-string v16, "cn.xuexi.android"

    const-string v17, "com.zhihu.android"

    const-string v18, "com.kugou.android"

    const-string v19, "com.hunantv.imgo.activity"

    const-string v20, "com.tencent.qqmusic"

    const-string/jumbo v21, "tv.danmaku.bili"

    const-string v22, "com.tencent.qqlive"

    const-string v23, "com.UCMobile"

    const-string v24, "com.sina.weibo"

    const-string v25, "com.tencent.mobileqq:qzone"

    const-string v26, "com.autonavi.minimap"

    filled-new-array/range {v8 .. v26}, [Ljava/lang/String;

    move-result-object v6

    .line 363
    invoke-static {v6}, Ljava/util/Arrays;->asList([Ljava/lang/Object;)Ljava/util/List;

    move-result-object v6

    invoke-direct {v2, v6}, Ljava/util/ArrayList;-><init>(Ljava/util/Collection;)V

    iput-object v2, v0, Lcom/miui/server/turbosched/TurboSchedManagerService;->mAppsLinkVipList:Ljava/util/List;

    .line 373
    const/4 v2, -0x1

    iput v2, v0, Lcom/miui/server/turbosched/TurboSchedManagerService;->mAppsLockContentionPid:I

    .line 374
    new-instance v2, Ljava/util/ArrayList;

    filled-new-array {v7}, [Ljava/lang/String;

    move-result-object v6

    .line 375
    invoke-static {v6}, Ljava/util/Arrays;->asList([Ljava/lang/Object;)Ljava/util/List;

    move-result-object v6

    invoke-direct {v2, v6}, Ljava/util/ArrayList;-><init>(Ljava/util/Collection;)V

    iput-object v2, v0, Lcom/miui/server/turbosched/TurboSchedManagerService;->mAppsLockContentionList:Ljava/util/List;

    .line 381
    iput-boolean v4, v0, Lcom/miui/server/turbosched/TurboSchedManagerService;->mScreenOn:Z

    .line 382
    new-instance v2, Ljava/util/HashMap;

    invoke-direct {v2}, Ljava/util/HashMap;-><init>()V

    iput-object v2, v0, Lcom/miui/server/turbosched/TurboSchedManagerService;->mBWFTidStartTimeMap:Ljava/util/Map;

    .line 383
    new-instance v2, Ljava/util/HashMap;

    invoke-direct {v2}, Ljava/util/HashMap;-><init>()V

    iput-object v2, v0, Lcom/miui/server/turbosched/TurboSchedManagerService;->mPriorityTidStartTimeMap:Ljava/util/Map;

    .line 384
    iput-object v5, v0, Lcom/miui/server/turbosched/TurboSchedManagerService;->enableGaeaAppProcessName:Ljava/lang/String;

    .line 385
    iput-object v5, v0, Lcom/miui/server/turbosched/TurboSchedManagerService;->enableLinkVipAppProcessName:Ljava/lang/String;

    .line 405
    iput-object v3, v0, Lcom/miui/server/turbosched/TurboSchedManagerService;->TurboSchedSDKTimer:Ljava/util/Timer;

    .line 406
    iput-object v3, v0, Lcom/miui/server/turbosched/TurboSchedManagerService;->TurboSchedSDKTimerTask:Ljava/util/TimerTask;

    .line 419
    new-instance v2, Ljava/util/HashMap;

    invoke-direct {v2}, Ljava/util/HashMap;-><init>()V

    iput-object v2, v0, Lcom/miui/server/turbosched/TurboSchedManagerService;->mTurboschedSDKParamMap:Ljava/util/Map;

    .line 673
    new-instance v2, Lcom/miui/server/turbosched/TurboSchedManagerService$1;

    iget-object v4, v0, Lcom/miui/server/turbosched/TurboSchedManagerService;->mTurboSchedHandler:Lcom/miui/server/turbosched/TurboSchedManagerService$TurboSchedHandler;

    invoke-direct {v2, v0, v4}, Lcom/miui/server/turbosched/TurboSchedManagerService$1;-><init>(Lcom/miui/server/turbosched/TurboSchedManagerService;Landroid/os/Handler;)V

    iput-object v2, v0, Lcom/miui/server/turbosched/TurboSchedManagerService;->mCloudSwichObserver:Landroid/database/ContentObserver;

    .line 684
    new-instance v2, Lcom/miui/server/turbosched/TurboSchedManagerService$2;

    iget-object v4, v0, Lcom/miui/server/turbosched/TurboSchedManagerService;->mTurboSchedHandler:Lcom/miui/server/turbosched/TurboSchedManagerService$TurboSchedHandler;

    invoke-direct {v2, v0, v4}, Lcom/miui/server/turbosched/TurboSchedManagerService$2;-><init>(Lcom/miui/server/turbosched/TurboSchedManagerService;Landroid/os/Handler;)V

    iput-object v2, v0, Lcom/miui/server/turbosched/TurboSchedManagerService;->mCloudTurboschedMiuiSdkObserver:Landroid/database/ContentObserver;

    .line 695
    new-instance v2, Lcom/miui/server/turbosched/TurboSchedManagerService$3;

    iget-object v4, v0, Lcom/miui/server/turbosched/TurboSchedManagerService;->mTurboSchedHandler:Lcom/miui/server/turbosched/TurboSchedManagerService$TurboSchedHandler;

    invoke-direct {v2, v0, v4}, Lcom/miui/server/turbosched/TurboSchedManagerService$3;-><init>(Lcom/miui/server/turbosched/TurboSchedManagerService;Landroid/os/Handler;)V

    iput-object v2, v0, Lcom/miui/server/turbosched/TurboSchedManagerService;->mCloudCoreAppOptimizerEnableObserver:Landroid/database/ContentObserver;

    .line 706
    new-instance v2, Lcom/miui/server/turbosched/TurboSchedManagerService$4;

    iget-object v4, v0, Lcom/miui/server/turbosched/TurboSchedManagerService;->mTurboSchedHandler:Lcom/miui/server/turbosched/TurboSchedManagerService$TurboSchedHandler;

    invoke-direct {v2, v0, v4}, Lcom/miui/server/turbosched/TurboSchedManagerService$4;-><init>(Lcom/miui/server/turbosched/TurboSchedManagerService;Landroid/os/Handler;)V

    iput-object v2, v0, Lcom/miui/server/turbosched/TurboSchedManagerService;->mCloudCoreTop20AppOptimizerEnableObserver:Landroid/database/ContentObserver;

    .line 715
    new-instance v2, Lcom/miui/server/turbosched/TurboSchedManagerService$5;

    iget-object v4, v0, Lcom/miui/server/turbosched/TurboSchedManagerService;->mTurboSchedHandler:Lcom/miui/server/turbosched/TurboSchedManagerService$TurboSchedHandler;

    invoke-direct {v2, v0, v4}, Lcom/miui/server/turbosched/TurboSchedManagerService$5;-><init>(Lcom/miui/server/turbosched/TurboSchedManagerService;Landroid/os/Handler;)V

    iput-object v2, v0, Lcom/miui/server/turbosched/TurboSchedManagerService;->mCloudAllowListObserver:Landroid/database/ContentObserver;

    .line 724
    new-instance v2, Lcom/miui/server/turbosched/TurboSchedManagerService$6;

    iget-object v4, v0, Lcom/miui/server/turbosched/TurboSchedManagerService;->mTurboSchedHandler:Lcom/miui/server/turbosched/TurboSchedManagerService$TurboSchedHandler;

    invoke-direct {v2, v0, v4}, Lcom/miui/server/turbosched/TurboSchedManagerService$6;-><init>(Lcom/miui/server/turbosched/TurboSchedManagerService;Landroid/os/Handler;)V

    iput-object v2, v0, Lcom/miui/server/turbosched/TurboSchedManagerService;->mCloudPolicyListObserver:Landroid/database/ContentObserver;

    .line 735
    new-instance v2, Lcom/miui/server/turbosched/TurboSchedManagerService$7;

    iget-object v4, v0, Lcom/miui/server/turbosched/TurboSchedManagerService;->mTurboSchedHandler:Lcom/miui/server/turbosched/TurboSchedManagerService$TurboSchedHandler;

    invoke-direct {v2, v0, v4}, Lcom/miui/server/turbosched/TurboSchedManagerService$7;-><init>(Lcom/miui/server/turbosched/TurboSchedManagerService;Landroid/os/Handler;)V

    iput-object v2, v0, Lcom/miui/server/turbosched/TurboSchedManagerService;->mCloudThermalBreakEnableObserver:Landroid/database/ContentObserver;

    .line 744
    new-instance v2, Lcom/miui/server/turbosched/TurboSchedManagerService$8;

    iget-object v4, v0, Lcom/miui/server/turbosched/TurboSchedManagerService;->mTurboSchedHandler:Lcom/miui/server/turbosched/TurboSchedManagerService$TurboSchedHandler;

    invoke-direct {v2, v0, v4}, Lcom/miui/server/turbosched/TurboSchedManagerService$8;-><init>(Lcom/miui/server/turbosched/TurboSchedManagerService;Landroid/os/Handler;)V

    iput-object v2, v0, Lcom/miui/server/turbosched/TurboSchedManagerService;->mCloudThermalBreakThresholdObserver:Landroid/database/ContentObserver;

    .line 753
    new-instance v2, Lcom/miui/server/turbosched/TurboSchedManagerService$9;

    iget-object v4, v0, Lcom/miui/server/turbosched/TurboSchedManagerService;->mTurboSchedHandler:Lcom/miui/server/turbosched/TurboSchedManagerService$TurboSchedHandler;

    invoke-direct {v2, v0, v4}, Lcom/miui/server/turbosched/TurboSchedManagerService$9;-><init>(Lcom/miui/server/turbosched/TurboSchedManagerService;Landroid/os/Handler;)V

    iput-object v2, v0, Lcom/miui/server/turbosched/TurboSchedManagerService;->mCloudLinkWhiteListObserver:Landroid/database/ContentObserver;

    .line 762
    new-instance v2, Lcom/miui/server/turbosched/TurboSchedManagerService$10;

    iget-object v4, v0, Lcom/miui/server/turbosched/TurboSchedManagerService;->mTurboSchedHandler:Lcom/miui/server/turbosched/TurboSchedManagerService$TurboSchedHandler;

    invoke-direct {v2, v0, v4}, Lcom/miui/server/turbosched/TurboSchedManagerService$10;-><init>(Lcom/miui/server/turbosched/TurboSchedManagerService;Landroid/os/Handler;)V

    iput-object v2, v0, Lcom/miui/server/turbosched/TurboSchedManagerService;->mCloudFrameTurboWhiteListObserver:Landroid/database/ContentObserver;

    .line 771
    new-instance v2, Lcom/miui/server/turbosched/TurboSchedManagerService$11;

    iget-object v4, v0, Lcom/miui/server/turbosched/TurboSchedManagerService;->mTurboSchedHandler:Lcom/miui/server/turbosched/TurboSchedManagerService$TurboSchedHandler;

    invoke-direct {v2, v0, v4}, Lcom/miui/server/turbosched/TurboSchedManagerService$11;-><init>(Lcom/miui/server/turbosched/TurboSchedManagerService;Landroid/os/Handler;)V

    iput-object v2, v0, Lcom/miui/server/turbosched/TurboSchedManagerService;->mCloudDpsEnableObserver:Landroid/database/ContentObserver;

    .line 1883
    new-instance v2, Lcom/miui/server/turbosched/TurboSchedManagerService$12;

    invoke-direct {v2, v0}, Lcom/miui/server/turbosched/TurboSchedManagerService$12;-><init>(Lcom/miui/server/turbosched/TurboSchedManagerService;)V

    iput-object v2, v0, Lcom/miui/server/turbosched/TurboSchedManagerService;->mProcessObserver:Landroid/app/IProcessObserver$Stub;

    .line 422
    iput-object v1, v0, Lcom/miui/server/turbosched/TurboSchedManagerService;->mContext:Landroid/content/Context;

    .line 423
    new-instance v2, Landroid/os/HandlerThread;

    const-string/jumbo v4, "turbosched"

    invoke-direct {v2, v4}, Landroid/os/HandlerThread;-><init>(Ljava/lang/String;)V

    iput-object v2, v0, Lcom/miui/server/turbosched/TurboSchedManagerService;->mHandlerThread:Landroid/os/HandlerThread;

    .line 424
    invoke-virtual {v2}, Landroid/os/HandlerThread;->start()V

    .line 425
    new-instance v2, Lcom/miui/server/turbosched/TurboSchedManagerService$TurboSchedHandler;

    iget-object v4, v0, Lcom/miui/server/turbosched/TurboSchedManagerService;->mHandlerThread:Landroid/os/HandlerThread;

    invoke-virtual {v4}, Landroid/os/HandlerThread;->getLooper()Landroid/os/Looper;

    move-result-object v4

    invoke-direct {v2, v0, v4, v3}, Lcom/miui/server/turbosched/TurboSchedManagerService$TurboSchedHandler;-><init>(Lcom/miui/server/turbosched/TurboSchedManagerService;Landroid/os/Looper;Lcom/miui/server/turbosched/TurboSchedManagerService$TurboSchedHandler-IA;)V

    iput-object v2, v0, Lcom/miui/server/turbosched/TurboSchedManagerService;->mTurboSchedHandler:Lcom/miui/server/turbosched/TurboSchedManagerService$TurboSchedHandler;

    .line 426
    invoke-static {}, Lcom/miui/server/turbosched/TurboSchedSceneManager;->getInstance()Lcom/miui/server/turbosched/TurboSchedSceneManager;

    move-result-object v2

    iget-object v3, v0, Lcom/miui/server/turbosched/TurboSchedManagerService;->mHandlerThread:Landroid/os/HandlerThread;

    invoke-virtual {v3}, Landroid/os/HandlerThread;->getLooper()Landroid/os/Looper;

    move-result-object v3

    invoke-virtual {v2, v1, v3}, Lcom/miui/server/turbosched/TurboSchedSceneManager;->initialize(Landroid/content/Context;Landroid/os/Looper;)V

    .line 427
    iget-object v2, v0, Lcom/miui/server/turbosched/TurboSchedManagerService;->mContext:Landroid/content/Context;

    invoke-virtual {v2}, Landroid/content/Context;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v2

    iput-object v2, v0, Lcom/miui/server/turbosched/TurboSchedManagerService;->mPm:Landroid/content/pm/PackageManager;

    .line 428
    invoke-static {}, Landroid/app/ActivityManager;->getService()Landroid/app/IActivityManager;

    move-result-object v2

    iput-object v2, v0, Lcom/miui/server/turbosched/TurboSchedManagerService;->mActivityManagerService:Landroid/app/IActivityManager;

    .line 429
    invoke-direct/range {p0 .. p0}, Lcom/miui/server/turbosched/TurboSchedManagerService;->onServiceStart()V

    .line 430
    invoke-direct/range {p0 .. p0}, Lcom/miui/server/turbosched/TurboSchedManagerService;->registeForegroundReceiver()V

    .line 431
    iget-boolean v2, v0, Lcom/miui/server/turbosched/TurboSchedManagerService;->mReceiveCloudData:Z

    if-eqz v2, :cond_2

    .line 432
    invoke-direct/range {p0 .. p0}, Lcom/miui/server/turbosched/TurboSchedManagerService;->registerObserver()V

    .line 433
    invoke-direct/range {p0 .. p0}, Lcom/miui/server/turbosched/TurboSchedManagerService;->updateCloudControlProp()V

    goto :goto_2

    .line 435
    :cond_2
    invoke-direct/range {p0 .. p0}, Lcom/miui/server/turbosched/TurboSchedManagerService;->updateLocalProp()V

    .line 438
    :goto_2
    new-instance v2, Lcom/miui/server/turbosched/TurboSchedManagerService$TurboSchedBroadcastReceiver;

    invoke-direct {v2, v0}, Lcom/miui/server/turbosched/TurboSchedManagerService$TurboSchedBroadcastReceiver;-><init>(Lcom/miui/server/turbosched/TurboSchedManagerService;)V

    .line 440
    const/16 v2, 0x28

    invoke-static {v2}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v2

    .line 441
    .local v2, "str":Ljava/lang/String;
    const-string v3, "B-Duration"

    const-string/jumbo v4, "sys/module/metis/parameters/mi_boost_duration"

    invoke-direct {v0, v3, v4, v2}, Lcom/miui/server/turbosched/TurboSchedManagerService;->writeTurboSchedNode(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Z

    .line 442
    invoke-direct/range {p0 .. p0}, Lcom/miui/server/turbosched/TurboSchedManagerService;->initTopAppList()V

    .line 444
    new-instance v3, Lcom/miui/server/turbosched/FrameTurboAction;

    invoke-direct {v3}, Lcom/miui/server/turbosched/FrameTurboAction;-><init>()V

    iput-object v3, v0, Lcom/miui/server/turbosched/TurboSchedManagerService;->mFrameTurboAction:Lcom/miui/server/turbosched/FrameTurboAction;

    .line 445
    iget-object v3, v3, Lcom/miui/server/turbosched/FrameTurboAction;->mFrameTurboAppMap:Ljava/util/Map;

    iput-object v3, v0, Lcom/miui/server/turbosched/TurboSchedManagerService;->mFrameTurboAppMap:Ljava/util/Map;

    .line 447
    new-instance v3, Lcom/miui/server/turbosched/GameTurboAction;

    invoke-direct {v3}, Lcom/miui/server/turbosched/GameTurboAction;-><init>()V

    iput-object v3, v0, Lcom/miui/server/turbosched/TurboSchedManagerService;->mGameTurboAction:Lcom/miui/server/turbosched/GameTurboAction;

    .line 450
    invoke-direct/range {p0 .. p0}, Lcom/miui/server/turbosched/TurboSchedManagerService;->initStartUpMode()V

    .line 451
    return-void
.end method

.method private addCoreApp(Ljava/util/List;)V
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List<",
            "Ljava/lang/String;",
            ">;)V"
        }
    .end annotation

    .line 2099
    .local p1, "coreAppList":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;"
    if-nez p1, :cond_0

    .line 2100
    return-void

    .line 2102
    :cond_0
    new-instance v0, Ljava/util/ArrayList;

    invoke-static {}, Landroid/os/TurboSchedMonitor;->getInstance()Landroid/os/TurboSchedMonitor;

    move-result-object v1

    invoke-virtual {v1}, Landroid/os/TurboSchedMonitor;->getCoreAppList()Ljava/util/List;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/util/ArrayList;-><init>(Ljava/util/Collection;)V

    .line 2103
    .local v0, "applist":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;"
    invoke-interface {p1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_2

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/lang/String;

    .line 2104
    .local v2, "app":Ljava/lang/String;
    invoke-interface {v0, v2}, Ljava/util/List;->contains(Ljava/lang/Object;)Z

    move-result v3

    if-nez v3, :cond_1

    .line 2105
    invoke-interface {v0, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 2107
    .end local v2    # "app":Ljava/lang/String;
    :cond_1
    goto :goto_0

    .line 2108
    :cond_2
    invoke-static {}, Landroid/os/TurboSchedMonitor;->getInstance()Landroid/os/TurboSchedMonitor;

    move-result-object v1

    iget-object v2, p0, Lcom/miui/server/turbosched/TurboSchedManagerService;->mContext:Landroid/content/Context;

    invoke-virtual {v1, v2, v0}, Landroid/os/TurboSchedMonitor;->setCoreAppList(Landroid/content/Context;Ljava/util/List;)V

    .line 2109
    return-void
.end method

.method private checkCallerPermmsion(II)Z
    .locals 4
    .param p1, "pid"    # I
    .param p2, "uid"    # I

    .line 1487
    invoke-static {p1}, Lcom/android/server/am/ProcessUtils;->getPackageNameByPid(I)Ljava/lang/String;

    move-result-object v0

    .line 1488
    .local v0, "str":Ljava/lang/String;
    if-eqz p2, :cond_2

    if-eqz v0, :cond_0

    iget-object v1, p0, Lcom/miui/server/turbosched/TurboSchedManagerService;->mCallerAllowList:Ljava/util/List;

    invoke-interface {v1, v0}, Ljava/util/List;->contains(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_2

    :cond_0
    invoke-direct {p0, v0, p2}, Lcom/miui/server/turbosched/TurboSchedManagerService;->isStartUpModeAllowed(Ljava/lang/String;I)Z

    move-result v1

    if-eqz v1, :cond_1

    goto :goto_0

    .line 1491
    :cond_1
    iget-object v1, p0, Lcom/miui/server/turbosched/TurboSchedManagerService;->mHistoryLog:Landroid/util/LocalLog;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "TS [V] : caller is not allow: uid: "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, ", pkg: "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, ", pid: "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Landroid/util/LocalLog;->log(Ljava/lang/String;)V

    .line 1492
    const/4 v1, 0x0

    return v1

    .line 1489
    :cond_2
    :goto_0
    const/4 v1, 0x1

    return v1
.end method

.method private delCoreApp(Ljava/util/List;)V
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List<",
            "Ljava/lang/String;",
            ">;)V"
        }
    .end annotation

    .line 2112
    .local p1, "coreAppList":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;"
    if-nez p1, :cond_0

    .line 2113
    return-void

    .line 2115
    :cond_0
    new-instance v0, Ljava/util/ArrayList;

    invoke-static {}, Landroid/os/TurboSchedMonitor;->getInstance()Landroid/os/TurboSchedMonitor;

    move-result-object v1

    invoke-virtual {v1}, Landroid/os/TurboSchedMonitor;->getCoreAppList()Ljava/util/List;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/util/ArrayList;-><init>(Ljava/util/Collection;)V

    .line 2116
    .local v0, "applist":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;"
    invoke-interface {p1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_2

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/lang/String;

    .line 2117
    .local v2, "app":Ljava/lang/String;
    invoke-interface {v0, v2}, Ljava/util/List;->contains(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_1

    .line 2118
    invoke-interface {v0, v2}, Ljava/util/List;->remove(Ljava/lang/Object;)Z

    .line 2120
    .end local v2    # "app":Ljava/lang/String;
    :cond_1
    goto :goto_0

    .line 2121
    :cond_2
    invoke-static {}, Landroid/os/TurboSchedMonitor;->getInstance()Landroid/os/TurboSchedMonitor;

    move-result-object v1

    iget-object v2, p0, Lcom/miui/server/turbosched/TurboSchedManagerService;->mContext:Landroid/content/Context;

    invoke-virtual {v1, v2, v0}, Landroid/os/TurboSchedMonitor;->setCoreAppList(Landroid/content/Context;Ljava/util/List;)V

    .line 2122
    return-void
.end method

.method private dumpConfig(Ljava/io/PrintWriter;)V
    .locals 7
    .param p1, "pw"    # Ljava/io/PrintWriter;

    .line 1567
    const-string v0, "--------------------current config-----------------------"

    invoke-virtual {p1, v0}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V

    .line 1569
    const-string v0, ""

    .line 1570
    .local v0, "metisVersionSuffix":Ljava/lang/String;
    iget-object v1, p0, Lcom/miui/server/turbosched/TurboSchedManagerService;->mMetisVersion:Ljava/lang/String;

    if-eqz v1, :cond_0

    invoke-virtual {v1}, Ljava/lang/String;->isEmpty()Z

    move-result v1

    if-nez v1, :cond_0

    .line 1571
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "-"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lcom/miui/server/turbosched/TurboSchedManagerService;->mMetisVersion:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    .line 1573
    :cond_0
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "TurboSchedManagerService: v4.0.0"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p1, v1}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V

    .line 1574
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, " disable cloud control : "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-boolean v2, p0, Lcom/miui/server/turbosched/TurboSchedManagerService;->mReceiveCloudData:Z

    const/4 v3, 0x1

    xor-int/2addr v2, v3

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p1, v1}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V

    .line 1575
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, " mTurboEnabled : "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-boolean v2, p0, Lcom/miui/server/turbosched/TurboSchedManagerService;->mTurboEnabled:Z

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ", mTurboEnabledV2: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-boolean v2, p0, Lcom/miui/server/turbosched/TurboSchedManagerService;->mTurboEnabledV2:Z

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ", mCloudControlEnabled : "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-boolean v2, p0, Lcom/miui/server/turbosched/TurboSchedManagerService;->mCloudControlEnabled:Z

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p1, v1}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V

    .line 1576
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, " CoreAppOptimizerEnabled: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-static {}, Landroid/os/TurboSchedMonitor;->getInstance()Landroid/os/TurboSchedMonitor;

    move-result-object v2

    invoke-virtual {v2}, Landroid/os/TurboSchedMonitor;->isCoreAppOptimizerEnabled()Z

    move-result v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ", DebugMode: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    .line 1577
    invoke-static {}, Landroid/os/TurboSchedMonitor;->getInstance()Landroid/os/TurboSchedMonitor;

    move-result-object v2

    invoke-virtual {v2}, Landroid/os/TurboSchedMonitor;->isDebugMode()Z

    move-result v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    .line 1576
    invoke-virtual {p1, v1}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V

    .line 1578
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, " CoreTop20AppOptimizerEnabled: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-boolean v2, p0, Lcom/miui/server/turbosched/TurboSchedManagerService;->mEnableTop20:Z

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p1, v1}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V

    .line 1579
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, " mThermalBreakEnabled: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-boolean v2, p0, Lcom/miui/server/turbosched/TurboSchedManagerService;->mThermalBreakEnabled:Z

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ", mLinkEnable: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-boolean v2, p0, Lcom/miui/server/turbosched/TurboSchedManagerService;->mLinkEnable:Z

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ", mSupportGaea: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-boolean v2, p0, Lcom/miui/server/turbosched/TurboSchedManagerService;->mSupportGaea:Z

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ", mGaeaEnable: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-boolean v2, p0, Lcom/miui/server/turbosched/TurboSchedManagerService;->mGaeaEnable:Z

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p1, v1}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V

    .line 1580
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, " TurboSchedConfig.mPolicyList : "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ","

    invoke-static {}, Lcom/miui/server/turbosched/TurboSchedConfig;->getPolicyList()Ljava/util/List;

    move-result-object v4

    invoke-static {v2, v4}, Ljava/lang/String;->join(Ljava/lang/CharSequence;Ljava/lang/Iterable;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p1, v1}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V

    .line 1581
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, " Caller allow list : "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lcom/miui/server/turbosched/TurboSchedManagerService;->mCallerAllowList:Ljava/util/List;

    invoke-interface {v2}, Ljava/util/List;->toArray()[Ljava/lang/Object;

    move-result-object v2

    invoke-static {v2}, Ljava/util/Arrays;->toString([Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p1, v1}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V

    .line 1582
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, " Lock Contention Enabled : "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-static {}, Landroid/os/TurboSchedMonitor;->getInstance()Landroid/os/TurboSchedMonitor;

    move-result-object v2

    invoke-virtual {v2}, Landroid/os/TurboSchedMonitor;->isCoreAppOptimizerEnabled()Z

    move-result v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p1, v1}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V

    .line 1583
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, " mDPSEnable : "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-boolean v2, p0, Lcom/miui/server/turbosched/TurboSchedManagerService;->mDPSEnable:Z

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p1, v1}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V

    .line 1585
    const-string v1, "--------------------environment config-----------------------"

    invoke-virtual {p1, v1}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V

    .line 1586
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, " cores sched: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "persist.sys.miui_animator_sched.big_prime_cores"

    const-string v4, "not set"

    invoke-static {v2, v4}, Landroid/os/SystemProperties;->get(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p1, v1}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V

    .line 1587
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, " [PRendering] enable:"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "ro.vendor.perf.scroll_opt"

    invoke-static {v2, v4}, Landroid/os/SystemProperties;->get(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ", h-a value: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "ro.vendor.perf.scroll_opt.heavy_app"

    invoke-static {v2, v4}, Landroid/os/SystemProperties;->get(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p1, v1}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V

    .line 1589
    const-string v1, "--------------------kernel config-----------------------"

    invoke-virtual {p1, v1}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V

    .line 1590
    iget-boolean v1, p0, Lcom/miui/server/turbosched/TurboSchedManagerService;->mTurboNodeExist:Z

    const-string v2, " isFileAccess = "

    if-eqz v1, :cond_1

    .line 1591
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "/sys/module/metis/parameters/mi_viptask"

    invoke-static {v2}, Lcom/miui/server/turbosched/TurboSchedUtil;->checkFileAccess(Ljava/lang/String;)Z

    move-result v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p1, v1}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V

    goto :goto_0

    .line 1593
    :cond_1
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "/sys/module/migt/parameters/mi_viptask"

    invoke-static {v2}, Lcom/miui/server/turbosched/TurboSchedUtil;->checkFileAccess(Ljava/lang/String;)Z

    move-result v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p1, v1}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V

    .line 1596
    :goto_0
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, " metis dev access = "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "/dev/metis"

    invoke-static {v2}, Lcom/miui/server/turbosched/TurboSchedUtil;->checkFileAccess(Ljava/lang/String;)Z

    move-result v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p1, v1}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V

    .line 1597
    const-string v1, "/sys/module/metis/parameters/add_mi_viptask_enqueue_boost"

    invoke-static {v1}, Lcom/miui/server/turbosched/TurboSchedUtil;->checkFileAccess(Ljava/lang/String;)Z

    move-result v1

    const/4 v2, 0x0

    if-eqz v1, :cond_2

    const-string v1, "/sys/module/metis/parameters/del_mi_viptask_enqueue_boost"

    invoke-static {v1}, Lcom/miui/server/turbosched/TurboSchedUtil;->checkFileAccess(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_2

    move v1, v3

    goto :goto_1

    :cond_2
    move v1, v2

    .line 1598
    .local v1, "isBWFPolicyAccess":Z
    :goto_1
    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, " bwf policy access = "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {p1, v4}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V

    .line 1599
    const-string v4, "/sys/module/metis/parameters/add_mi_viptask_sched_priority"

    invoke-static {v4}, Lcom/miui/server/turbosched/TurboSchedUtil;->checkFileAccess(Ljava/lang/String;)Z

    move-result v4

    if-eqz v4, :cond_3

    const-string v4, "/sys/module/metis/parameters/del_mi_viptask_sched_priority"

    invoke-static {v4}, Lcom/miui/server/turbosched/TurboSchedUtil;->checkFileAccess(Ljava/lang/String;)Z

    move-result v4

    if-eqz v4, :cond_3

    move v4, v3

    goto :goto_2

    :cond_3
    move v4, v2

    .line 1600
    .local v4, "isPriorityPolicyAccess":Z
    :goto_2
    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, " priority policy access = "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, v4}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {p1, v5}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V

    .line 1601
    const-string v5, "/sys/module/metis/parameters/add_mi_viptask_sched_lit_core"

    invoke-static {v5}, Lcom/miui/server/turbosched/TurboSchedUtil;->checkFileAccess(Ljava/lang/String;)Z

    move-result v5

    if-eqz v5, :cond_4

    const-string v5, "/sys/module/metis/parameters/del_mi_viptask_sched_lit_core"

    invoke-static {v5}, Lcom/miui/server/turbosched/TurboSchedUtil;->checkFileAccess(Ljava/lang/String;)Z

    move-result v5

    if-eqz v5, :cond_4

    goto :goto_3

    :cond_4
    move v3, v2

    :goto_3
    move v2, v3

    .line 1602
    .local v2, "isLittleCorePolicyAccess":Z
    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, " little core policy access = "

    invoke-virtual {v3, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v2}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {p1, v3}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V

    .line 1604
    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, " thermal break enable access = "

    invoke-virtual {v3, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v5, "/sys/module/metis/parameters/is_break_enable"

    invoke-static {v5}, Lcom/miui/server/turbosched/TurboSchedUtil;->checkFileAccess(Ljava/lang/String;)Z

    move-result v5

    invoke-virtual {v3, v5}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {p1, v3}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V

    .line 1605
    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, " thermal break access = "

    invoke-virtual {v3, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v5, "/sys/class/thermal/thermal_message/boost"

    invoke-static {v5}, Lcom/miui/server/turbosched/TurboSchedUtil;->checkFileAccess(Ljava/lang/String;)Z

    move-result v5

    invoke-virtual {v3, v5}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {p1, v3}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V

    .line 1606
    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, " link access ="

    invoke-virtual {v3, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v5, "/sys/module/metis/parameters/vip_link_enable"

    invoke-static {v5}, Lcom/miui/server/turbosched/TurboSchedUtil;->checkFileAccess(Ljava/lang/String;)Z

    move-result v5

    invoke-virtual {v3, v5}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {p1, v3}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V

    .line 1607
    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, " gaea access ="

    invoke-virtual {v3, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v5, "/sys/module/gaea/parameters/mi_gaea_enable"

    invoke-static {v5}, Lcom/miui/server/turbosched/TurboSchedUtil;->checkFileAccess(Ljava/lang/String;)Z

    move-result v5

    invoke-virtual {v3, v5}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {p1, v3}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V

    .line 1608
    return-void
.end method

.method private enableFrameBoostInKernel(Z)V
    .locals 3
    .param p1, "enable"    # Z

    .line 1958
    const-string v0, "/sys/module/metis/parameters/mi_fboost_enable"

    const-string v1, "TB-Enabled"

    if-eqz p1, :cond_0

    .line 1959
    const-string v2, "1"

    invoke-direct {p0, v1, v0, v2}, Lcom/miui/server/turbosched/TurboSchedManagerService;->writeTurboSchedNode(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Z

    move-result v0

    .local v0, "isSuccess":Z
    goto :goto_0

    .line 1961
    .end local v0    # "isSuccess":Z
    :cond_0
    const-string v2, "0"

    invoke-direct {p0, v1, v0, v2}, Lcom/miui/server/turbosched/TurboSchedManagerService;->writeTurboSchedNode(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Z

    move-result v0

    .line 1963
    .restart local v0    # "isSuccess":Z
    :goto_0
    invoke-static {}, Landroid/os/TurboSchedMonitor;->getInstance()Landroid/os/TurboSchedMonitor;

    move-result-object v1

    invoke-virtual {v1}, Landroid/os/TurboSchedMonitor;->isDebugMode()Z

    move-result v1

    if-eqz v1, :cond_1

    .line 1964
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "enableFrameBoostInKernel, isSuccess:"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    const-string v2, "TurboSchedManagerService"

    invoke-static {v2, v1}, Landroid/util/Slog;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 1965
    :cond_1
    return-void
.end method

.method private enableGaea(Z)V
    .locals 4
    .param p1, "bEnable"    # Z

    .line 1539
    const/4 v0, 0x0

    .line 1541
    .local v0, "isSuccess":Z
    iput-boolean p1, p0, Lcom/miui/server/turbosched/TurboSchedManagerService;->mGaeaEnable:Z

    .line 1542
    const-string v1, "persist.sys.turbosched.gaea.enable"

    invoke-static {p1}, Ljava/lang/Boolean;->toString(Z)Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/os/SystemProperties;->set(Ljava/lang/String;Ljava/lang/String;)V

    .line 1543
    if-nez p1, :cond_0

    .line 1544
    const-string v1, "/sys/module/gaea/parameters/mi_gaea_enable"

    const-string v2, "0"

    const-string v3, "GAEA"

    invoke-direct {p0, v3, v1, v2}, Lcom/miui/server/turbosched/TurboSchedManagerService;->writeTurboSchedNode(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Z

    move-result v0

    .line 1545
    invoke-static {}, Landroid/os/TurboSchedMonitor;->getInstance()Landroid/os/TurboSchedMonitor;

    move-result-object v1

    invoke-virtual {v1}, Landroid/os/TurboSchedMonitor;->isDebugMode()Z

    move-result v1

    if-eqz v1, :cond_0

    if-nez v0, :cond_0

    .line 1546
    const-string v1, "TurboSchedManagerService"

    const-string v2, "failed to write gaea path node!"

    invoke-static {v1, v2}, Landroid/util/Slog;->w(Ljava/lang/String;Ljava/lang/String;)I

    .line 1548
    :cond_0
    return-void
.end method

.method private enableTurboSched(Z)V
    .locals 2
    .param p1, "enable"    # Z

    .line 1988
    iput-boolean p1, p0, Lcom/miui/server/turbosched/TurboSchedManagerService;->mTurboEnabled:Z

    .line 1989
    const-string v0, "persist.sys.turbosched.enable"

    invoke-static {p1}, Ljava/lang/Boolean;->toString(Z)Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/os/SystemProperties;->set(Ljava/lang/String;Ljava/lang/String;)V

    .line 1990
    return-void
.end method

.method private exitStartUpMode()V
    .locals 2

    .line 475
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/miui/server/turbosched/TurboSchedManagerService;->mStartUpModeInitFinish:Z

    .line 476
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/miui/server/turbosched/TurboSchedManagerService;->mInStartUpMode:Z

    .line 477
    iget-boolean v1, p0, Lcom/miui/server/turbosched/TurboSchedManagerService;->mKernelEnabelUpdate:Z

    if-nez v1, :cond_0

    .line 478
    invoke-direct {p0, v0}, Lcom/miui/server/turbosched/TurboSchedManagerService;->enableFrameBoostInKernel(Z)V

    .line 480
    :cond_0
    iget-object v0, p0, Lcom/miui/server/turbosched/TurboSchedManagerService;->mPolicyListBackup:Ljava/util/List;

    invoke-static {v0}, Lcom/miui/server/turbosched/TurboSchedConfig;->setPolicyList(Ljava/util/List;)V

    .line 481
    return-void
.end method

.method private getBoardSensorTemperature()I
    .locals 8

    .line 2067
    const-string v0, "getBoardSensorTemperature failed : "

    const-string v1, "TurboSchedManagerService"

    const/4 v2, 0x0

    .line 2068
    .local v2, "sensorTemp":I
    const/4 v3, 0x0

    .line 2070
    .local v3, "fis":Ljava/io/FileInputStream;
    :try_start_0
    new-instance v4, Ljava/io/FileInputStream;

    const-string v5, "/sys/class/thermal/thermal_message/board_sensor_temp"

    invoke-direct {v4, v5}, Ljava/io/FileInputStream;-><init>(Ljava/lang/String;)V

    move-object v3, v4

    .line 2071
    const/16 v4, 0xa

    new-array v4, v4, [B

    .line 2072
    .local v4, "buffer":[B
    invoke-virtual {v3, v4}, Ljava/io/FileInputStream;->read([B)I

    move-result v5

    .line 2073
    .local v5, "read":I
    if-lez v5, :cond_0

    .line 2074
    new-instance v6, Ljava/lang/String;

    const/4 v7, 0x0

    invoke-direct {v6, v4, v7, v5}, Ljava/lang/String;-><init>([BII)V

    .line 2075
    .local v6, "str":Ljava/lang/String;
    invoke-virtual {v6}, Ljava/lang/String;->trim()Ljava/lang/String;

    move-result-object v7

    invoke-static {v7}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v7

    div-int/lit16 v7, v7, 0x3e8
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_1
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move v2, v7

    .line 2080
    .end local v4    # "buffer":[B
    .end local v5    # "read":I
    .end local v6    # "str":Ljava/lang/String;
    :cond_0
    nop

    .line 2082
    :try_start_1
    invoke-virtual {v3}, Ljava/io/FileInputStream;->close()V
    :try_end_1
    .catch Ljava/io/IOException; {:try_start_1 .. :try_end_1} :catch_0

    .line 2085
    :goto_0
    goto :goto_2

    .line 2083
    :catch_0
    move-exception v4

    .line 2084
    .local v4, "e":Ljava/io/IOException;
    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    :goto_1
    invoke-virtual {v5, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v4}, Ljava/io/IOException;->getMessage()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v0, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v1, v0}, Landroid/util/Slog;->e(Ljava/lang/String;Ljava/lang/String;)I

    .end local v4    # "e":Ljava/io/IOException;
    goto :goto_0

    .line 2080
    :catchall_0
    move-exception v4

    goto :goto_3

    .line 2077
    :catch_1
    move-exception v4

    .line 2078
    .local v4, "e":Ljava/lang/Exception;
    :try_start_2
    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v5, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v4}, Ljava/lang/Exception;->getMessage()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v1, v5}, Landroid/util/Slog;->e(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    .line 2080
    nop

    .end local v4    # "e":Ljava/lang/Exception;
    if-eqz v3, :cond_1

    .line 2082
    :try_start_3
    invoke-virtual {v3}, Ljava/io/FileInputStream;->close()V
    :try_end_3
    .catch Ljava/io/IOException; {:try_start_3 .. :try_end_3} :catch_2

    goto :goto_0

    .line 2083
    :catch_2
    move-exception v4

    .line 2084
    .local v4, "e":Ljava/io/IOException;
    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    goto :goto_1

    .line 2088
    .end local v4    # "e":Ljava/io/IOException;
    :cond_1
    :goto_2
    return v2

    .line 2080
    :goto_3
    if-eqz v3, :cond_2

    .line 2082
    :try_start_4
    invoke-virtual {v3}, Ljava/io/FileInputStream;->close()V
    :try_end_4
    .catch Ljava/io/IOException; {:try_start_4 .. :try_end_4} :catch_3

    .line 2085
    goto :goto_4

    .line 2083
    :catch_3
    move-exception v5

    .line 2084
    .local v5, "e":Ljava/io/IOException;
    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v6, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v5}, Ljava/io/IOException;->getMessage()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v0, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v1, v0}, Landroid/util/Slog;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 2087
    .end local v5    # "e":Ljava/io/IOException;
    :cond_2
    :goto_4
    throw v4
.end method

.method private getBufferTxCount()I
    .locals 5

    .line 1439
    const/4 v0, 0x0

    .line 1440
    .local v0, "data":Landroid/os/Parcel;
    const/4 v1, 0x0

    .line 1442
    .local v1, "reply":Landroid/os/Parcel;
    :try_start_0
    iget-object v2, p0, Lcom/miui/server/turbosched/TurboSchedManagerService;->mSurfaceFlinger:Landroid/os/IBinder;

    if-nez v2, :cond_0

    .line 1443
    const-string v2, "SurfaceFlinger"

    invoke-static {v2}, Landroid/os/ServiceManager;->getService(Ljava/lang/String;)Landroid/os/IBinder;

    move-result-object v2

    iput-object v2, p0, Lcom/miui/server/turbosched/TurboSchedManagerService;->mSurfaceFlinger:Landroid/os/IBinder;

    .line 1445
    :cond_0
    iget-object v2, p0, Lcom/miui/server/turbosched/TurboSchedManagerService;->mSurfaceFlinger:Landroid/os/IBinder;

    if-eqz v2, :cond_3

    .line 1446
    invoke-static {}, Landroid/os/Parcel;->obtain()Landroid/os/Parcel;

    move-result-object v2

    move-object v1, v2

    .line 1447
    invoke-static {}, Landroid/os/Parcel;->obtain()Landroid/os/Parcel;

    move-result-object v2

    move-object v0, v2

    .line 1448
    const-string v2, "android.ui.ISurfaceComposer"

    invoke-virtual {v0, v2}, Landroid/os/Parcel;->writeInterfaceToken(Ljava/lang/String;)V

    .line 1449
    iget-object v2, p0, Lcom/miui/server/turbosched/TurboSchedManagerService;->mSurfaceFlinger:Landroid/os/IBinder;

    const/16 v3, 0x3f3

    const/4 v4, 0x0

    invoke-interface {v2, v3, v0, v1, v4}, Landroid/os/IBinder;->transact(ILandroid/os/Parcel;Landroid/os/Parcel;I)Z

    .line 1450
    invoke-virtual {v1}, Landroid/os/Parcel;->readInt()I

    move-result v2
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 1455
    if-eqz v0, :cond_1

    .line 1456
    invoke-virtual {v0}, Landroid/os/Parcel;->recycle()V

    .line 1458
    :cond_1
    if-eqz v1, :cond_2

    .line 1459
    invoke-virtual {v1}, Landroid/os/Parcel;->recycle()V

    .line 1450
    :cond_2
    return v2

    .line 1455
    :cond_3
    if-eqz v0, :cond_4

    .line 1456
    invoke-virtual {v0}, Landroid/os/Parcel;->recycle()V

    .line 1458
    :cond_4
    if-eqz v1, :cond_6

    .line 1459
    :goto_0
    invoke-virtual {v1}, Landroid/os/Parcel;->recycle()V

    goto :goto_1

    .line 1455
    :catchall_0
    move-exception v2

    goto :goto_2

    .line 1452
    :catch_0
    move-exception v2

    .line 1453
    .local v2, "e":Landroid/os/RemoteException;
    :try_start_1
    const-string v3, "TurboSchedManagerService"

    const-string v4, "Failed to get Buffer-Tx count"

    invoke-static {v3, v4}, Landroid/util/Slog;->e(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 1455
    nop

    .end local v2    # "e":Landroid/os/RemoteException;
    if-eqz v0, :cond_5

    .line 1456
    invoke-virtual {v0}, Landroid/os/Parcel;->recycle()V

    .line 1458
    :cond_5
    if-eqz v1, :cond_6

    .line 1459
    goto :goto_0

    .line 1463
    :cond_6
    :goto_1
    const/4 v2, -0x1

    return v2

    .line 1455
    :goto_2
    if-eqz v0, :cond_7

    .line 1456
    invoke-virtual {v0}, Landroid/os/Parcel;->recycle()V

    .line 1458
    :cond_7
    if-eqz v1, :cond_8

    .line 1459
    invoke-virtual {v1}, Landroid/os/Parcel;->recycle()V

    .line 1461
    :cond_8
    throw v2
.end method

.method private getPackageNameFromUid(I)Ljava/lang/String;
    .locals 3
    .param p1, "uid"    # I

    .line 1497
    const/4 v0, 0x0

    .line 1499
    .local v0, "packName":Ljava/lang/String;
    iget-object v1, p0, Lcom/miui/server/turbosched/TurboSchedManagerService;->mPm:Landroid/content/pm/PackageManager;

    if-eqz v1, :cond_0

    .line 1500
    invoke-virtual {v1, p1}, Landroid/content/pm/PackageManager;->getNameForUid(I)Ljava/lang/String;

    move-result-object v0

    .line 1503
    :cond_0
    if-nez v0, :cond_1

    .line 1504
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "get caller pkgname failed uid = "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    const-string v2, "TurboSchedManagerService"

    invoke-static {v2, v1}, Landroid/util/Slog;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 1506
    :cond_1
    return-object v0
.end method

.method private getProcessNameByPid(I)Ljava/lang/String;
    .locals 1
    .param p1, "pid"    # I

    .line 1510
    const/4 v0, 0x0

    .line 1511
    .local v0, "processName":Ljava/lang/String;
    invoke-static {p1}, Lcom/android/server/am/ProcessUtils;->getProcessNameByPid(I)Ljava/lang/String;

    move-result-object v0

    .line 1512
    return-object v0
.end method

.method private getThermalBreakString()Ljava/lang/String;
    .locals 4

    .line 2057
    invoke-direct {p0}, Lcom/miui/server/turbosched/TurboSchedManagerService;->getBoardSensorTemperature()I

    move-result v0

    .line 2058
    .local v0, "temperatue":I
    sget-object v1, Lcom/miui/server/turbosched/TurboSchedManagerService;->TEMPREATURE_THROSHOLD_BOOST_STRING_MAP:Ljava/util/Map;

    invoke-interface {v1}, Ljava/util/Map;->entrySet()Ljava/util/Set;

    move-result-object v1

    invoke-interface {v1}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_1

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/util/Map$Entry;

    .line 2059
    .local v2, "entry":Ljava/util/Map$Entry;, "Ljava/util/Map$Entry<Ljava/lang/Integer;Ljava/lang/String;>;"
    invoke-interface {v2}, Ljava/util/Map$Entry;->getKey()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/lang/Integer;

    invoke-virtual {v3}, Ljava/lang/Integer;->intValue()I

    move-result v3

    if-lt v0, v3, :cond_0

    .line 2060
    invoke-interface {v2}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/String;

    return-object v1

    .line 2062
    .end local v2    # "entry":Ljava/util/Map$Entry;, "Ljava/util/Map$Entry<Ljava/lang/Integer;Ljava/lang/String;>;"
    :cond_0
    goto :goto_0

    .line 2063
    :cond_1
    const-string v1, " cpu4:2419200 cpu7:2841600"

    return-object v1
.end method

.method private initFrameTurboAppList()Ljava/util/List;
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .line 2133
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    .line 2134
    .local v0, "appList":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;"
    const-string v1, "persist.sys.turbosched.frame_turbo.apps"

    const-string v2, ""

    invoke-static {v1, v2}, Landroid/os/SystemProperties;->get(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 2135
    .local v1, "appListStr":Ljava/lang/String;
    if-eqz v1, :cond_0

    invoke-virtual {v1}, Ljava/lang/String;->isEmpty()Z

    move-result v2

    if-nez v2, :cond_0

    .line 2136
    new-instance v2, Ljava/util/ArrayList;

    const-string v3, ","

    invoke-virtual {v1, v3}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v3

    invoke-static {v3}, Ljava/util/Arrays;->asList([Ljava/lang/Object;)Ljava/util/List;

    move-result-object v3

    invoke-direct {v2, v3}, Ljava/util/ArrayList;-><init>(Ljava/util/Collection;)V

    move-object v0, v2

    .line 2138
    :cond_0
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "initFrameTurboAppList: "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    const-string v3, "TurboSchedManagerService"

    invoke-static {v3, v2}, Landroid/util/Slog;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 2139
    return-object v0
.end method

.method private initStartUpMode()V
    .locals 1

    .line 464
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/miui/server/turbosched/TurboSchedManagerService;->mTurboEnabledV2:Z

    .line 466
    invoke-direct {p0, v0}, Lcom/miui/server/turbosched/TurboSchedManagerService;->enableFrameBoostInKernel(Z)V

    .line 468
    iget-boolean v0, p0, Lcom/miui/server/turbosched/TurboSchedManagerService;->mRegistedForegroundReceiver:Z

    if-nez v0, :cond_0

    .line 469
    invoke-direct {p0}, Lcom/miui/server/turbosched/TurboSchedManagerService;->registeForegroundReceiver()V

    .line 471
    :cond_0
    sget-object v0, Lcom/miui/server/turbosched/TurboSchedConfig;->POLICY_ALL:Ljava/util/List;

    invoke-static {v0}, Lcom/miui/server/turbosched/TurboSchedConfig;->setPolicyList(Ljava/util/List;)V

    .line 472
    return-void
.end method

.method private initTopAppList()V
    .locals 4

    .line 2125
    iget-object v0, p0, Lcom/miui/server/turbosched/TurboSchedManagerService;->mContext:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    const-string/jumbo v1, "turbo_sched_core_app_list"

    invoke-static {v0, v1}, Landroid/provider/Settings$System;->getString(Landroid/content/ContentResolver;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 2126
    .local v0, "appListStr":Ljava/lang/String;
    if-eqz v0, :cond_0

    .line 2127
    const-string v1, ","

    invoke-virtual {v0, v1}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Ljava/util/Arrays;->asList([Ljava/lang/Object;)Ljava/util/List;

    move-result-object v1

    iput-object v1, p0, Lcom/miui/server/turbosched/TurboSchedManagerService;->mCoreAppsPackageNameList:Ljava/util/List;

    .line 2129
    :cond_0
    invoke-static {}, Landroid/os/TurboSchedMonitor;->getInstance()Landroid/os/TurboSchedMonitor;

    move-result-object v1

    iget-object v2, p0, Lcom/miui/server/turbosched/TurboSchedManagerService;->mContext:Landroid/content/Context;

    iget-object v3, p0, Lcom/miui/server/turbosched/TurboSchedManagerService;->mCoreAppsPackageNameList:Ljava/util/List;

    invoke-virtual {v1, v2, v3}, Landroid/os/TurboSchedMonitor;->setCoreAppList(Landroid/content/Context;Ljava/util/List;)V

    .line 2130
    return-void
.end method

.method private isFgDrawingFrame([I)Z
    .locals 5
    .param p1, "tids"    # [I

    .line 1292
    const/4 v0, 0x0

    .local v0, "i":I
    :goto_0
    array-length v1, p1

    if-ge v0, v1, :cond_1

    .line 1293
    aget v1, p1, v0

    int-to-long v1, v1

    iget-wide v3, p0, Lcom/miui/server/turbosched/TurboSchedManagerService;->mFgCorePid:J

    cmp-long v1, v1, v3

    if-nez v1, :cond_0

    .line 1294
    const/4 v1, 0x1

    return v1

    .line 1292
    :cond_0
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 1297
    .end local v0    # "i":I
    :cond_1
    const/4 v0, 0x0

    return v0
.end method

.method private isForeground(Ljava/lang/String;)Z
    .locals 1
    .param p1, "packageName"    # Ljava/lang/String;

    .line 2183
    iget-object v0, p0, Lcom/miui/server/turbosched/TurboSchedManagerService;->mProcessName:Ljava/lang/String;

    invoke-virtual {p1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    return v0
.end method

.method private isStartUpModeAllowed(Ljava/lang/String;I)Z
    .locals 3
    .param p1, "packageName"    # Ljava/lang/String;
    .param p2, "uid"    # I

    .line 1467
    iget-boolean v0, p0, Lcom/miui/server/turbosched/TurboSchedManagerService;->mInStartUpMode:Z

    const/4 v1, 0x0

    if-nez v0, :cond_0

    .line 1468
    return v1

    .line 1471
    :cond_0
    const/4 v0, 0x1

    if-eqz p1, :cond_1

    iget-object v2, p0, Lcom/miui/server/turbosched/TurboSchedManagerService;->mStartUpAllowList:Ljava/util/List;

    invoke-interface {v2, p1}, Ljava/util/List;->contains(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_1

    .line 1472
    return v0

    .line 1475
    :cond_1
    if-nez p2, :cond_2

    .line 1476
    return v0

    .line 1479
    :cond_2
    const/16 v2, 0x3e8

    if-ne p2, v2, :cond_3

    .line 1480
    return v0

    .line 1483
    :cond_3
    return v1
.end method

.method private isTurboEnabled()Z
    .locals 1

    .line 484
    iget-boolean v0, p0, Lcom/miui/server/turbosched/TurboSchedManagerService;->mTurboEnabled:Z

    if-eqz v0, :cond_0

    .line 485
    const/4 v0, 0x1

    return v0

    .line 487
    :cond_0
    iget-boolean v0, p0, Lcom/miui/server/turbosched/TurboSchedManagerService;->mTurboEnabledV2:Z

    return v0
.end method

.method private metisFrameBoost(I)V
    .locals 5
    .param p1, "boostMs"    # I

    .line 1521
    invoke-static {}, Landroid/os/TurboSchedMonitor;->getInstance()Landroid/os/TurboSchedMonitor;

    move-result-object v0

    invoke-virtual {v0}, Landroid/os/TurboSchedMonitor;->isCoreAppOptimizerEnabled()Z

    move-result v0

    if-eqz v0, :cond_2

    const-string v0, "/dev/metis"

    invoke-static {v0}, Lcom/miui/server/turbosched/TurboSchedUtil;->checkFileAccess(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 1522
    sget-object v0, Lcom/miui/server/turbosched/TurboSchedManagerService;->mTurboLock:Ljava/lang/Object;

    monitor-enter v0

    .line 1523
    :try_start_0
    const-string v1, "/dev/metis"

    invoke-static {v1}, Landroid/os/NativeTurboSchedManager;->nativeOpenDevice(Ljava/lang/String;)I

    move-result v1

    .line 1524
    .local v1, "handle":I
    if-ltz v1, :cond_1

    .line 1525
    invoke-static {}, Landroid/os/TurboSchedMonitor;->getInstance()Landroid/os/TurboSchedMonitor;

    move-result-object v2

    invoke-virtual {v2}, Landroid/os/TurboSchedMonitor;->isDebugMode()Z

    move-result v2

    const-wide/16 v3, 0x40

    if-eqz v2, :cond_0

    .line 1526
    const-string v2, "metisFrameBoost"

    invoke-static {v3, v4, v2}, Landroid/os/Trace;->traceBegin(JLjava/lang/String;)V

    .line 1528
    :cond_0
    const/4 v2, 0x6

    invoke-static {v1, v2}, Landroid/os/NativeTurboSchedManager;->nativeIoctlDevice(II)V

    .line 1529
    invoke-static {v1}, Landroid/os/NativeTurboSchedManager;->nativeCloseDevice(I)V

    .line 1531
    invoke-static {}, Landroid/os/TurboSchedMonitor;->getInstance()Landroid/os/TurboSchedMonitor;

    move-result-object v2

    invoke-virtual {v2}, Landroid/os/TurboSchedMonitor;->isDebugMode()Z

    move-result v2

    if-eqz v2, :cond_1

    .line 1532
    invoke-static {v3, v4}, Landroid/os/Trace;->traceEnd(J)V

    .line 1534
    .end local v1    # "handle":I
    :cond_1
    monitor-exit v0

    goto :goto_0

    :catchall_0
    move-exception v1

    monitor-exit v0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v1

    .line 1536
    :cond_2
    :goto_0
    return-void
.end method

.method private onServiceStart()V
    .locals 1

    .line 454
    const-string v0, "/sys/module/metis/parameters/mi_viptask"

    invoke-static {v0}, Lcom/miui/server/turbosched/TurboSchedUtil;->checkFileAccess(Ljava/lang/String;)Z

    move-result v0

    iput-boolean v0, p0, Lcom/miui/server/turbosched/TurboSchedManagerService;->mTurboNodeExist:Z

    .line 455
    invoke-static {}, Landroid/os/TurboSchedMonitor;->getInstance()Landroid/os/TurboSchedMonitor;

    move-result-object v0

    invoke-virtual {v0}, Landroid/os/TurboSchedMonitor;->getBoostDuration()Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lcom/miui/server/turbosched/TurboSchedManagerService;->mBoostDuration:Ljava/util/List;

    .line 456
    const-string v0, "/sys/module/metis/parameters/version"

    invoke-static {v0}, Lcom/miui/server/turbosched/TurboSchedUtil;->readValueFromFile(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/miui/server/turbosched/TurboSchedManagerService;->mMetisVersion:Ljava/lang/String;

    .line 457
    if-eqz v0, :cond_0

    .line 458
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/miui/server/turbosched/TurboSchedManagerService;->mDPSSupport:Z

    .line 460
    :cond_0
    return-void
.end method

.method private parseDumpCommand(Ljava/io/FileDescriptor;Ljava/io/PrintWriter;[Ljava/lang/String;)Z
    .locals 11
    .param p1, "fd"    # Ljava/io/FileDescriptor;
    .param p2, "pw"    # Ljava/io/PrintWriter;
    .param p3, "args"    # [Ljava/lang/String;

    .line 1611
    array-length v0, p3

    const/4 v1, 0x2

    const/4 v2, 0x0

    const/4 v3, 0x1

    const/4 v4, 0x3

    if-lt v0, v4, :cond_0

    const-string/jumbo v0, "setAppFrameDelay"

    aget-object v5, p3, v2

    invoke-virtual {v0, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1612
    aget-object v0, p3, v3

    invoke-static {v0}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v0

    .line 1613
    .local v0, "appType":I
    aget-object v2, p3, v1

    invoke-static {v2}, Ljava/lang/Float;->parseFloat(Ljava/lang/String;)F

    move-result v2

    .line 1614
    .local v2, "frameDelayTH":F
    invoke-static {}, Landroid/os/TurboSchedMonitor;->getInstance()Landroid/os/TurboSchedMonitor;

    move-result-object v4

    invoke-virtual {v4, v0, v2}, Landroid/os/TurboSchedMonitor;->setCoreAppFrameDealyThreshold(IF)V

    .line 1615
    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "  AppType: "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    aget-object v5, p3, v3

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, " frame delay TH: "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    aget-object v1, p3, v1

    invoke-virtual {v4, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {p0, p2, v1}, Lcom/miui/server/turbosched/TurboSchedManagerService;->printCommandResult(Ljava/io/PrintWriter;Ljava/lang/String;)V

    .line 1616
    return v3

    .line 1619
    .end local v0    # "appType":I
    .end local v2    # "frameDelayTH":F
    :cond_0
    array-length v0, p3

    const-string v5, " Duration: "

    if-lt v0, v4, :cond_1

    const-string/jumbo v0, "setBoostDuration"

    aget-object v6, p3, v2

    invoke-virtual {v0, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 1620
    aget-object v0, p3, v3

    invoke-static {v0}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v0

    .line 1621
    .local v0, "boostType":I
    aget-object v2, p3, v1

    invoke-static {v2}, Ljava/lang/Long;->parseLong(Ljava/lang/String;)J

    move-result-wide v6

    .line 1622
    .local v6, "time":J
    invoke-static {}, Landroid/os/TurboSchedMonitor;->getInstance()Landroid/os/TurboSchedMonitor;

    move-result-object v2

    invoke-virtual {v2, v0, v6, v7}, Landroid/os/TurboSchedMonitor;->setBoostDuration(IJ)V

    .line 1623
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "  BoostType: "

    invoke-virtual {v2, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    aget-object v4, p3, v3

    invoke-virtual {v2, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    aget-object v1, p3, v1

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {p0, p2, v1}, Lcom/miui/server/turbosched/TurboSchedManagerService;->printCommandResult(Ljava/io/PrintWriter;Ljava/lang/String;)V

    .line 1624
    return v3

    .line 1627
    .end local v0    # "boostType":I
    .end local v6    # "time":J
    :cond_1
    array-length v0, p3

    if-lt v0, v1, :cond_2

    const-string/jumbo v0, "setDebugMode"

    aget-object v6, p3, v2

    invoke-virtual {v0, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 1628
    aget-object v0, p3, v3

    invoke-static {v0}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v0

    .line 1629
    .local v0, "debugMode":I
    invoke-static {}, Landroid/os/TurboSchedMonitor;->getInstance()Landroid/os/TurboSchedMonitor;

    move-result-object v1

    invoke-virtual {v1, v0}, Landroid/os/TurboSchedMonitor;->setDebugMode(I)V

    .line 1630
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, " setDebugMode: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    aget-object v2, p3, v3

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {p0, p2, v1}, Lcom/miui/server/turbosched/TurboSchedManagerService;->printCommandResult(Ljava/io/PrintWriter;Ljava/lang/String;)V

    .line 1631
    return v3

    .line 1634
    .end local v0    # "debugMode":I
    :cond_2
    array-length v0, p3

    if-lt v0, v1, :cond_4

    const-string v0, "disableCloudControl"

    aget-object v6, p3, v2

    invoke-virtual {v0, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_4

    .line 1635
    aget-object v0, p3, v3

    invoke-static {v0}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v0

    .line 1636
    .local v0, "disableCloudControl":I
    if-lez v0, :cond_3

    goto :goto_0

    :cond_3
    move v2, v3

    :goto_0
    iput-boolean v2, p0, Lcom/miui/server/turbosched/TurboSchedManagerService;->mReceiveCloudData:Z

    .line 1637
    const-string v1, "persist.sys.turbosched.receive.cloud.data"

    invoke-static {v2}, Ljava/lang/Boolean;->toString(Z)Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/os/SystemProperties;->set(Ljava/lang/String;Ljava/lang/String;)V

    .line 1638
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, " disableCloudControl: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    aget-object v2, p3, v3

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {p0, p2, v1}, Lcom/miui/server/turbosched/TurboSchedManagerService;->printCommandResult(Ljava/io/PrintWriter;Ljava/lang/String;)V

    .line 1639
    return v3

    .line 1642
    .end local v0    # "disableCloudControl":I
    :cond_4
    array-length v0, p3

    if-lt v0, v1, :cond_6

    const-string v0, "enableTop20App"

    aget-object v6, p3, v2

    invoke-virtual {v0, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_6

    .line 1643
    aget-object v0, p3, v3

    invoke-static {v0}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v0

    .line 1644
    .local v0, "enableInt":I
    if-lez v0, :cond_5

    move v2, v3

    :cond_5
    move v1, v2

    .line 1645
    .local v1, "enableBool":Z
    invoke-direct {p0, v1}, Lcom/miui/server/turbosched/TurboSchedManagerService;->syncTop20AppList(Z)V

    .line 1646
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, " enableTop20App: "

    invoke-virtual {v2, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    aget-object v4, p3, v3

    invoke-virtual {v2, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-direct {p0, p2, v2}, Lcom/miui/server/turbosched/TurboSchedManagerService;->printCommandResult(Ljava/io/PrintWriter;Ljava/lang/String;)V

    .line 1647
    return v3

    .line 1650
    .end local v0    # "enableInt":I
    .end local v1    # "enableBool":Z
    :cond_6
    array-length v0, p3

    if-lt v0, v1, :cond_10

    const-string v0, "enableCoreAppOptimizer"

    aget-object v6, p3, v2

    invoke-virtual {v0, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_10

    .line 1651
    aget-object v0, p3, v3

    invoke-static {v0}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v0

    .line 1652
    .local v0, "enable":I
    invoke-static {}, Landroid/os/TurboSchedMonitor;->getInstance()Landroid/os/TurboSchedMonitor;

    move-result-object v1

    invoke-virtual {v1, v0}, Landroid/os/TurboSchedMonitor;->enableCoreAppOptimizer(I)V

    .line 1653
    if-lez v0, :cond_7

    move v2, v3

    :cond_7
    move v1, v2

    .line 1654
    .local v1, "enableBoolean":Z
    invoke-direct {p0, v1}, Lcom/miui/server/turbosched/TurboSchedManagerService;->enableFrameBoostInKernel(Z)V

    .line 1655
    if-eqz v1, :cond_8

    .line 1656
    iput-boolean v3, p0, Lcom/miui/server/turbosched/TurboSchedManagerService;->mKernelEnabelUpdate:Z

    .line 1658
    :cond_8
    iget-boolean v2, p0, Lcom/miui/server/turbosched/TurboSchedManagerService;->mRegistedForegroundReceiver:Z

    if-nez v2, :cond_9

    .line 1659
    invoke-direct {p0}, Lcom/miui/server/turbosched/TurboSchedManagerService;->registeForegroundReceiver()V

    .line 1661
    :cond_9
    invoke-direct {p0, v1}, Lcom/miui/server/turbosched/TurboSchedManagerService;->enableGaea(Z)V

    .line 1664
    const-string v2, "persist.sys.miui_animator_sched.big_prime_cores"

    if-eqz v1, :cond_c

    .line 1665
    iget-object v4, p0, Lcom/miui/server/turbosched/TurboSchedManagerService;->m8550Devices:Ljava/util/List;

    sget-object v5, Lmiui/os/Build;->DEVICE:Ljava/lang/String;

    invoke-interface {v4, v5}, Ljava/util/List;->contains(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_a

    .line 1666
    const-string v4, "3-7"

    invoke-static {v2, v4}, Landroid/os/SystemProperties;->set(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_1

    .line 1667
    :cond_a
    iget-object v4, p0, Lcom/miui/server/turbosched/TurboSchedManagerService;->m8475Devices:Ljava/util/List;

    sget-object v5, Lmiui/os/Build;->DEVICE:Ljava/lang/String;

    invoke-interface {v4, v5}, Ljava/util/List;->contains(Ljava/lang/Object;)Z

    move-result v4

    if-nez v4, :cond_b

    iget-object v4, p0, Lcom/miui/server/turbosched/TurboSchedManagerService;->m8450Devices:Ljava/util/List;

    sget-object v5, Lmiui/os/Build;->DEVICE:Ljava/lang/String;

    invoke-interface {v4, v5}, Ljava/util/List;->contains(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_f

    .line 1668
    :cond_b
    const-string v4, "4-7"

    invoke-static {v2, v4}, Landroid/os/SystemProperties;->set(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_1

    .line 1671
    :cond_c
    iget-object v4, p0, Lcom/miui/server/turbosched/TurboSchedManagerService;->m8550Devices:Ljava/util/List;

    sget-object v5, Lmiui/os/Build;->DEVICE:Ljava/lang/String;

    invoke-interface {v4, v5}, Ljava/util/List;->contains(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_d

    .line 1672
    const-string v4, "3-6"

    invoke-static {v2, v4}, Landroid/os/SystemProperties;->set(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_1

    .line 1673
    :cond_d
    iget-object v4, p0, Lcom/miui/server/turbosched/TurboSchedManagerService;->m8475Devices:Ljava/util/List;

    sget-object v5, Lmiui/os/Build;->DEVICE:Ljava/lang/String;

    invoke-interface {v4, v5}, Ljava/util/List;->contains(Ljava/lang/Object;)Z

    move-result v4

    if-nez v4, :cond_e

    iget-object v4, p0, Lcom/miui/server/turbosched/TurboSchedManagerService;->m8450Devices:Ljava/util/List;

    sget-object v5, Lmiui/os/Build;->DEVICE:Ljava/lang/String;

    invoke-interface {v4, v5}, Ljava/util/List;->contains(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_f

    .line 1674
    :cond_e
    const-string v4, "4-6"

    invoke-static {v2, v4}, Landroid/os/SystemProperties;->set(Ljava/lang/String;Ljava/lang/String;)V

    .line 1678
    :cond_f
    :goto_1
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, " enableCoreAppOptimizer: "

    invoke-virtual {v2, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    aget-object v4, p3, v3

    invoke-virtual {v2, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-direct {p0, p2, v2}, Lcom/miui/server/turbosched/TurboSchedManagerService;->printCommandResult(Ljava/io/PrintWriter;Ljava/lang/String;)V

    .line 1679
    return v3

    .line 1682
    .end local v0    # "enable":I
    .end local v1    # "enableBoolean":Z
    :cond_10
    array-length v0, p3

    if-ne v0, v1, :cond_12

    const-string/jumbo v0, "setVipTask"

    aget-object v6, p3, v2

    invoke-virtual {v0, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_12

    .line 1683
    aget-object v0, p3, v3

    .line 1684
    .local v0, "str":Ljava/lang/String;
    const/4 v1, 0x0

    .line 1685
    .local v1, "result":Z
    iget-boolean v2, p0, Lcom/miui/server/turbosched/TurboSchedManagerService;->mTurboNodeExist:Z

    const-string v4, "dump-setVipTask"

    if-eqz v2, :cond_11

    .line 1686
    const-string v2, "/sys/module/metis/parameters/mi_viptask"

    invoke-direct {p0, v4, v2, v0}, Lcom/miui/server/turbosched/TurboSchedManagerService;->writeTurboSchedNode(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Z

    move-result v1

    goto :goto_2

    .line 1688
    :cond_11
    const-string v2, "/sys/module/migt/parameters/mi_viptask"

    invoke-direct {p0, v4, v2, v0}, Lcom/miui/server/turbosched/TurboSchedManagerService;->writeTurboSchedNode(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Z

    move-result v1

    .line 1690
    :goto_2
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v4, "setVipTask result: "

    invoke-virtual {v2, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-direct {p0, p2, v2}, Lcom/miui/server/turbosched/TurboSchedManagerService;->printCommandResult(Ljava/io/PrintWriter;Ljava/lang/String;)V

    .line 1691
    return v3

    .line 1694
    .end local v0    # "str":Ljava/lang/String;
    .end local v1    # "result":Z
    :cond_12
    array-length v0, p3

    const/4 v6, 0x4

    if-lt v0, v6, :cond_18

    const-string v0, "policy"

    aget-object v7, p3, v2

    invoke-virtual {v0, v7}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_18

    .line 1695
    aget-object v0, p3, v3

    .line 1696
    .local v0, "policy":Ljava/lang/String;
    aget-object v1, p3, v1

    invoke-static {v1}, Ljava/lang/Long;->parseLong(Ljava/lang/String;)J

    move-result-wide v1

    .line 1697
    .local v1, "time":J
    array-length v5, p3

    sub-int/2addr v5, v4

    new-array v4, v5, [I

    .line 1698
    .local v4, "tids":[I
    const/4 v5, 0x3

    .local v5, "i":I
    :goto_3
    array-length v6, p3

    if-ge v5, v6, :cond_13

    .line 1699
    add-int/lit8 v6, v5, -0x3

    aget-object v7, p3, v5

    invoke-static {v7}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v7

    aput v7, v4, v6

    .line 1698
    add-int/lit8 v5, v5, 0x1

    goto :goto_3

    .line 1701
    .end local v5    # "i":I
    :cond_13
    const-string v5, "bwf"

    invoke-virtual {v0, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-eqz v5, :cond_14

    .line 1702
    invoke-virtual {p0, v4, v1, v2}, Lcom/miui/server/turbosched/TurboSchedManagerService;->setTurboSchedActionWithBoostFrequency([IJ)V

    goto :goto_4

    .line 1703
    :cond_14
    const-string v5, "priority"

    invoke-virtual {v0, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-eqz v5, :cond_15

    .line 1704
    invoke-virtual {p0, v4, v1, v2}, Lcom/miui/server/turbosched/TurboSchedManagerService;->setTurboSchedActionWithPriority([IJ)V

    goto :goto_4

    .line 1705
    :cond_15
    const-string/jumbo v5, "vip"

    invoke-virtual {v0, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-eqz v5, :cond_16

    .line 1706
    invoke-virtual {p0, v4, v1, v2}, Lcom/miui/server/turbosched/TurboSchedManagerService;->setTurboSchedAction([IJ)Z

    goto :goto_4

    .line 1707
    :cond_16
    const-string v5, "lc"

    invoke-virtual {v0, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-eqz v5, :cond_17

    .line 1708
    invoke-virtual {p0, v4, v1, v2}, Lcom/miui/server/turbosched/TurboSchedManagerService;->setTurboSchedActionToLittleCore([IJ)V

    .line 1710
    :cond_17
    :goto_4
    return v3

    .line 1713
    .end local v0    # "policy":Ljava/lang/String;
    .end local v1    # "time":J
    .end local v4    # "tids":[I
    :cond_18
    array-length v0, p3

    const-string v7, ","

    if-ne v0, v1, :cond_1a

    const-string/jumbo v0, "setPolicy"

    aget-object v8, p3, v2

    invoke-virtual {v0, v8}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1a

    .line 1714
    aget-object v0, p3, v3

    .line 1715
    .local v0, "str":Ljava/lang/String;
    invoke-virtual {v0, v7}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v1

    .line 1716
    .local v1, "policies":[Ljava/lang/String;
    new-instance v4, Ljava/util/ArrayList;

    invoke-direct {v4}, Ljava/util/ArrayList;-><init>()V

    .line 1717
    .local v4, "policyList":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;"
    array-length v5, v1

    :goto_5
    if-ge v2, v5, :cond_19

    aget-object v6, v1, v2

    .line 1718
    .local v6, "policy":Ljava/lang/String;
    invoke-interface {v4, v6}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 1717
    .end local v6    # "policy":Ljava/lang/String;
    add-int/lit8 v2, v2, 0x1

    goto :goto_5

    .line 1720
    :cond_19
    invoke-static {v4}, Lcom/miui/server/turbosched/TurboSchedConfig;->setPolicyList(Ljava/util/List;)V

    .line 1721
    iput-object v4, p0, Lcom/miui/server/turbosched/TurboSchedManagerService;->mPolicyListBackup:Ljava/util/List;

    .line 1722
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "TurboSchedConfig.mPolicyList: "

    invoke-virtual {v2, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-static {}, Lcom/miui/server/turbosched/TurboSchedConfig;->getPolicyList()Ljava/util/List;

    move-result-object v5

    invoke-virtual {v2, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-direct {p0, p2, v2}, Lcom/miui/server/turbosched/TurboSchedManagerService;->printCommandResult(Ljava/io/PrintWriter;Ljava/lang/String;)V

    .line 1723
    return v3

    .line 1726
    .end local v0    # "str":Ljava/lang/String;
    .end local v1    # "policies":[Ljava/lang/String;
    .end local v4    # "policyList":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;"
    :cond_1a
    array-length v0, p3

    if-ne v0, v1, :cond_1c

    const-string/jumbo v0, "setCallerAllowList"

    aget-object v8, p3, v2

    invoke-virtual {v0, v8}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1c

    .line 1727
    aget-object v0, p3, v3

    .line 1728
    .restart local v0    # "str":Ljava/lang/String;
    invoke-virtual {v0, v7}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v1

    .line 1729
    .restart local v1    # "policies":[Ljava/lang/String;
    iget-object v4, p0, Lcom/miui/server/turbosched/TurboSchedManagerService;->mCallerAllowList:Ljava/util/List;

    invoke-interface {v4}, Ljava/util/List;->clear()V

    .line 1730
    array-length v4, v1

    :goto_6
    if-ge v2, v4, :cond_1b

    aget-object v5, v1, v2

    .line 1731
    .local v5, "policy":Ljava/lang/String;
    iget-object v6, p0, Lcom/miui/server/turbosched/TurboSchedManagerService;->mCallerAllowList:Ljava/util/List;

    invoke-interface {v6, v5}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 1730
    .end local v5    # "policy":Ljava/lang/String;
    add-int/lit8 v2, v2, 0x1

    goto :goto_6

    .line 1733
    :cond_1b
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "mCallerAllowList: "

    invoke-virtual {v2, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget-object v4, p0, Lcom/miui/server/turbosched/TurboSchedManagerService;->mCallerAllowList:Ljava/util/List;

    invoke-virtual {v2, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-direct {p0, p2, v2}, Lcom/miui/server/turbosched/TurboSchedManagerService;->printCommandResult(Ljava/io/PrintWriter;Ljava/lang/String;)V

    .line 1734
    return v3

    .line 1737
    .end local v0    # "str":Ljava/lang/String;
    .end local v1    # "policies":[Ljava/lang/String;
    :cond_1c
    array-length v0, p3

    if-lt v0, v4, :cond_1d

    const-string/jumbo v0, "thermalBreak"

    aget-object v8, p3, v2

    invoke-virtual {v0, v8}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1d

    .line 1738
    aget-object v0, p3, v3

    invoke-static {v0}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v0

    .line 1739
    .local v0, "boost":I
    aget-object v1, p3, v1

    invoke-static {v1}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v1

    .line 1740
    .local v1, "time":I
    int-to-long v4, v1

    invoke-virtual {p0, v0, v4, v5}, Lcom/miui/server/turbosched/TurboSchedManagerService;->breakThermlimit(IJ)V

    .line 1741
    return v3

    .line 1744
    .end local v0    # "boost":I
    .end local v1    # "time":I
    :cond_1d
    array-length v0, p3

    if-ne v0, v1, :cond_1f

    const-string/jumbo v0, "setThermalBreakEnable"

    aget-object v8, p3, v2

    invoke-virtual {v0, v8}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1f

    .line 1745
    aget-object v0, p3, v3

    invoke-static {v0}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v0

    .line 1746
    .local v0, "enable":I
    if-lez v0, :cond_1e

    move v2, v3

    :cond_1e
    move v1, v2

    .line 1747
    .local v1, "enableBoolean":Z
    invoke-direct {p0, v1}, Lcom/miui/server/turbosched/TurboSchedManagerService;->setThermalBreakEnable(Z)V

    .line 1748
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v4, "setThermalBreakEnable: "

    invoke-virtual {v2, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-direct {p0, p2, v2}, Lcom/miui/server/turbosched/TurboSchedManagerService;->printCommandResult(Ljava/io/PrintWriter;Ljava/lang/String;)V

    .line 1749
    return v3

    .line 1752
    .end local v0    # "enable":I
    .end local v1    # "enableBoolean":Z
    :cond_1f
    array-length v0, p3

    if-ne v0, v1, :cond_22

    const-string/jumbo v0, "setThermalBreakThreshold"

    aget-object v8, p3, v2

    invoke-virtual {v0, v8}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_22

    .line 1753
    iget-boolean v0, p0, Lcom/miui/server/turbosched/TurboSchedManagerService;->mThermalBreakEnabled:Z

    if-nez v0, :cond_20

    .line 1754
    const-string/jumbo v0, "thermal break is not enabled"

    invoke-direct {p0, p2, v0}, Lcom/miui/server/turbosched/TurboSchedManagerService;->printCommandResult(Ljava/io/PrintWriter;Ljava/lang/String;)V

    .line 1755
    return v3

    .line 1757
    :cond_20
    aget-object v0, p3, v3

    .line 1758
    .local v0, "thresholdStr":Ljava/lang/String;
    const-string v1, "dry_run"

    invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_21

    .line 1759
    invoke-direct {p0, v0}, Lcom/miui/server/turbosched/TurboSchedManagerService;->setThermalBreakThresholdMap(Ljava/lang/String;)V

    .line 1761
    :cond_21
    sget-object v1, Lcom/miui/server/turbosched/TurboSchedManagerService;->TEMPREATURE_THROSHOLD_BOOST_STRING_MAP:Ljava/util/Map;

    invoke-virtual {v1}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {p0, p2, v1}, Lcom/miui/server/turbosched/TurboSchedManagerService;->printCommandResult(Ljava/io/PrintWriter;Ljava/lang/String;)V

    .line 1762
    return v3

    .line 1765
    .end local v0    # "thresholdStr":Ljava/lang/String;
    :cond_22
    array-length v0, p3

    if-lt v0, v3, :cond_31

    const-string v0, "CoreApp"

    aget-object v8, p3, v2

    invoke-virtual {v0, v8}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_31

    .line 1766
    array-length v0, p3

    if-ne v0, v4, :cond_24

    .line 1767
    aget-object v0, p3, v1

    invoke-virtual {v0, v7}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Ljava/util/Arrays;->asList([Ljava/lang/Object;)Ljava/util/List;

    move-result-object v0

    .line 1768
    .local v0, "apps":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;"
    aget-object v1, p3, v3

    invoke-virtual {v1}, Ljava/lang/String;->hashCode()I

    move-result v4

    sparse-switch v4, :sswitch_data_0

    :cond_23
    goto :goto_7

    :sswitch_0
    const-string v2, "del"

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_23

    move v2, v3

    goto :goto_8

    :sswitch_1
    const-string v4, "add"

    invoke-virtual {v1, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_23

    goto :goto_8

    :goto_7
    const/4 v2, -0x1

    :goto_8
    packed-switch v2, :pswitch_data_0

    goto :goto_9

    .line 1773
    :pswitch_0
    invoke-direct {p0, v0}, Lcom/miui/server/turbosched/TurboSchedManagerService;->delCoreApp(Ljava/util/List;)V

    .line 1774
    goto :goto_9

    .line 1770
    :pswitch_1
    invoke-direct {p0, v0}, Lcom/miui/server/turbosched/TurboSchedManagerService;->addCoreApp(Ljava/util/List;)V

    .line 1771
    nop

    .line 1778
    :goto_9
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "CoreApp: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-static {}, Landroid/os/TurboSchedMonitor;->getInstance()Landroid/os/TurboSchedMonitor;

    move-result-object v2

    invoke-virtual {v2}, Landroid/os/TurboSchedMonitor;->getCoreAppList()Ljava/util/List;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {p0, p2, v1}, Lcom/miui/server/turbosched/TurboSchedManagerService;->printCommandResult(Ljava/io/PrintWriter;Ljava/lang/String;)V

    .line 1779
    return v3

    .line 1781
    .end local v0    # "apps":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;"
    :cond_24
    const-string v0, " appFrameDelay: "

    invoke-virtual {p2, v0}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V

    .line 1782
    const/4 v0, 0x0

    .line 1783
    .local v0, "index":I
    invoke-static {}, Landroid/os/TurboSchedMonitor;->getInstance()Landroid/os/TurboSchedMonitor;

    move-result-object v7

    invoke-virtual {v7}, Landroid/os/TurboSchedMonitor;->getCoreAppFrameDealyThreshold()Ljava/util/List;

    move-result-object v7

    invoke-interface {v7}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v7

    :goto_a
    invoke-interface {v7}, Ljava/util/Iterator;->hasNext()Z

    move-result v8

    if-eqz v8, :cond_2f

    invoke-interface {v7}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v8

    check-cast v8, Ljava/lang/Float;

    invoke-virtual {v8}, Ljava/lang/Float;->floatValue()F

    move-result v8

    .line 1784
    .local v8, "ths":F
    if-nez v0, :cond_25

    .line 1785
    new-instance v9, Ljava/lang/StringBuilder;

    invoke-direct {v9}, Ljava/lang/StringBuilder;-><init>()V

    const-string v10, "  0 AWEME: "

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9, v8}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v9

    invoke-virtual {p2, v9}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V

    goto/16 :goto_b

    .line 1786
    :cond_25
    if-ne v0, v3, :cond_26

    .line 1787
    new-instance v9, Ljava/lang/StringBuilder;

    invoke-direct {v9}, Ljava/lang/StringBuilder;-><init>()V

    const-string v10, "  1 GIFMAKER: "

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9, v8}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v9

    invoke-virtual {p2, v9}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V

    goto/16 :goto_b

    .line 1788
    :cond_26
    if-ne v0, v1, :cond_27

    .line 1789
    new-instance v9, Ljava/lang/StringBuilder;

    invoke-direct {v9}, Ljava/lang/StringBuilder;-><init>()V

    const-string v10, "  2 WEIBO: "

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9, v8}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v9

    invoke-virtual {p2, v9}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V

    goto/16 :goto_b

    .line 1790
    :cond_27
    if-ne v0, v4, :cond_28

    .line 1791
    new-instance v9, Ljava/lang/StringBuilder;

    invoke-direct {v9}, Ljava/lang/StringBuilder;-><init>()V

    const-string v10, "  3 ARTICLE_NEWS: "

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9, v8}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v9

    invoke-virtual {p2, v9}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V

    goto/16 :goto_b

    .line 1792
    :cond_28
    if-ne v0, v6, :cond_29

    .line 1793
    new-instance v9, Ljava/lang/StringBuilder;

    invoke-direct {v9}, Ljava/lang/StringBuilder;-><init>()V

    const-string v10, "  4 TAOBAO: "

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9, v8}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v9

    invoke-virtual {p2, v9}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V

    goto/16 :goto_b

    .line 1794
    :cond_29
    const/4 v9, 0x5

    if-ne v0, v9, :cond_2a

    .line 1795
    new-instance v9, Ljava/lang/StringBuilder;

    invoke-direct {v9}, Ljava/lang/StringBuilder;-><init>()V

    const-string v10, "  5 WECHAT: "

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9, v8}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v9

    invoke-virtual {p2, v9}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V

    goto :goto_b

    .line 1796
    :cond_2a
    const/4 v9, 0x6

    if-ne v0, v9, :cond_2b

    .line 1797
    new-instance v9, Ljava/lang/StringBuilder;

    invoke-direct {v9}, Ljava/lang/StringBuilder;-><init>()V

    const-string v10, "  6 BILIBILI: "

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9, v8}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v9

    invoke-virtual {p2, v9}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V

    goto :goto_b

    .line 1798
    :cond_2b
    const/4 v9, 0x7

    if-ne v0, v9, :cond_2c

    .line 1799
    new-instance v9, Ljava/lang/StringBuilder;

    invoke-direct {v9}, Ljava/lang/StringBuilder;-><init>()V

    const-string v10, "  7 AWEME_LITE "

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9, v8}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v9

    invoke-virtual {p2, v9}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V

    goto :goto_b

    .line 1800
    :cond_2c
    const/16 v9, 0x8

    if-ne v0, v9, :cond_2d

    .line 1801
    new-instance v9, Ljava/lang/StringBuilder;

    invoke-direct {v9}, Ljava/lang/StringBuilder;-><init>()V

    const-string v10, "  8 KUAISHOU_NEBULA: "

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9, v8}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v9

    invoke-virtual {p2, v9}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V

    goto :goto_b

    .line 1802
    :cond_2d
    const/16 v9, 0x9

    if-ne v0, v9, :cond_2e

    .line 1803
    new-instance v9, Ljava/lang/StringBuilder;

    invoke-direct {v9}, Ljava/lang/StringBuilder;-><init>()V

    const-string v10, "  9 QQ: "

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9, v8}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v9

    invoke-virtual {p2, v9}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V

    .line 1805
    :cond_2e
    :goto_b
    nop

    .end local v8    # "ths":F
    add-int/lit8 v0, v0, 0x1

    .line 1806
    goto/16 :goto_a

    .line 1808
    :cond_2f
    invoke-virtual {p2, v5}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V

    .line 1809
    invoke-static {}, Landroid/os/TurboSchedMonitor;->getInstance()Landroid/os/TurboSchedMonitor;

    move-result-object v1

    invoke-virtual {v1}, Landroid/os/TurboSchedMonitor;->getBoostDuration()Ljava/util/List;

    move-result-object v1

    .line 1810
    .local v1, "boostDuration":Ljava/util/List;, "Ljava/util/List<Ljava/lang/Long;>;"
    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "  0 Boost Duration: "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-interface {v1, v2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v2

    invoke-virtual {v4, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {p2, v2}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V

    .line 1811
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "  1 Thermal Break Duration: "

    invoke-virtual {v2, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-interface {v1, v3}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v4

    invoke-virtual {v2, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {p2, v2}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V

    .line 1813
    const-string v2, "Boost allow list : "

    invoke-virtual {p2, v2}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V

    .line 1814
    invoke-static {}, Landroid/os/TurboSchedMonitor;->getInstance()Landroid/os/TurboSchedMonitor;

    move-result-object v2

    invoke-virtual {v2}, Landroid/os/TurboSchedMonitor;->getCoreAppList()Ljava/util/List;

    move-result-object v2

    invoke-interface {v2}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_c
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v4

    if-eqz v4, :cond_30

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Ljava/lang/String;

    .line 1815
    .local v4, "name":Ljava/lang/String;
    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "CoreAppName : "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {p2, v5}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V

    .line 1816
    .end local v4    # "name":Ljava/lang/String;
    goto :goto_c

    .line 1817
    :cond_30
    return v3

    .line 1820
    .end local v0    # "index":I
    .end local v1    # "boostDuration":Ljava/util/List;, "Ljava/util/List<Ljava/lang/Long;>;"
    :cond_31
    array-length v0, p3

    if-lt v0, v3, :cond_34

    const-string v0, "history"

    aget-object v4, p3, v2

    invoke-virtual {v0, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_34

    .line 1821
    array-length v0, p3

    if-le v0, v3, :cond_33

    .line 1822
    const-string v0, "-c"

    aget-object v1, p3, v3

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_32

    const-string v0, "--clear"

    aget-object v1, p3, v3

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_33

    .line 1823
    :cond_32
    new-instance v0, Landroid/util/LocalLog;

    sget v1, Lcom/miui/server/turbosched/TurboSchedManagerService;->MAX_HISTORY_ITEMS:I

    invoke-direct {v0, v1}, Landroid/util/LocalLog;-><init>(I)V

    iput-object v0, p0, Lcom/miui/server/turbosched/TurboSchedManagerService;->mHistoryLog:Landroid/util/LocalLog;

    .line 1824
    const-string v0, "History log cleared"

    invoke-virtual {p2, v0}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V

    .line 1825
    return v3

    .line 1828
    :cond_33
    iget-object v0, p0, Lcom/miui/server/turbosched/TurboSchedManagerService;->mHistoryLog:Landroid/util/LocalLog;

    invoke-virtual {v0, p1, p2, p3}, Landroid/util/LocalLog;->dump(Ljava/io/FileDescriptor;Ljava/io/PrintWriter;[Ljava/lang/String;)V

    .line 1829
    return v3

    .line 1832
    :cond_34
    array-length v0, p3

    if-ne v0, v1, :cond_36

    const-string v0, "enableLink"

    aget-object v4, p3, v2

    invoke-virtual {v0, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_36

    .line 1833
    aget-object v0, p3, v3

    invoke-static {v0}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v0

    .line 1834
    .local v0, "enable":I
    if-lez v0, :cond_35

    move v2, v3

    :cond_35
    iput-boolean v2, p0, Lcom/miui/server/turbosched/TurboSchedManagerService;->mLinkEnable:Z

    .line 1835
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "enableLink: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {p0, p2, v1}, Lcom/miui/server/turbosched/TurboSchedManagerService;->printCommandResult(Ljava/io/PrintWriter;Ljava/lang/String;)V

    .line 1836
    return v3

    .line 1839
    .end local v0    # "enable":I
    :cond_36
    array-length v0, p3

    if-ne v0, v1, :cond_39

    const-string v0, "enableGaea"

    aget-object v4, p3, v2

    invoke-virtual {v0, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_39

    .line 1840
    iget-boolean v0, p0, Lcom/miui/server/turbosched/TurboSchedManagerService;->mSupportGaea:Z

    if-eqz v0, :cond_38

    .line 1841
    aget-object v0, p3, v3

    invoke-static {v0}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v0

    .line 1842
    .restart local v0    # "enable":I
    if-lez v0, :cond_37

    move v4, v3

    goto :goto_d

    :cond_37
    move v4, v2

    :goto_d
    iput-boolean v4, p0, Lcom/miui/server/turbosched/TurboSchedManagerService;->mGaeaEnable:Z

    .line 1844
    invoke-direct {p0, v4}, Lcom/miui/server/turbosched/TurboSchedManagerService;->enableGaea(Z)V

    .line 1845
    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "enableGaea: "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-direct {p0, p2, v4}, Lcom/miui/server/turbosched/TurboSchedManagerService;->printCommandResult(Ljava/io/PrintWriter;Ljava/lang/String;)V

    .line 1846
    .end local v0    # "enable":I
    goto :goto_e

    .line 1847
    :cond_38
    const-string v0, "current devices don\'t support gaea ..."

    invoke-direct {p0, p2, v0}, Lcom/miui/server/turbosched/TurboSchedManagerService;->printCommandResult(Ljava/io/PrintWriter;Ljava/lang/String;)V

    .line 1851
    :cond_39
    :goto_e
    array-length v0, p3

    if-lt v0, v3, :cond_3a

    const-string v0, "frameTurbo"

    aget-object v4, p3, v2

    invoke-virtual {v0, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_3a

    .line 1852
    iget-object v0, p0, Lcom/miui/server/turbosched/TurboSchedManagerService;->mFrameTurboAction:Lcom/miui/server/turbosched/FrameTurboAction;

    invoke-virtual {v0, p1, p2, p3}, Lcom/miui/server/turbosched/FrameTurboAction;->parseFrameTurboCommand(Ljava/io/FileDescriptor;Ljava/io/PrintWriter;[Ljava/lang/String;)Z

    move-result v0

    return v0

    .line 1855
    :cond_3a
    array-length v0, p3

    if-lt v0, v1, :cond_3b

    const-string v0, "api"

    aget-object v1, p3, v2

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_3b

    .line 1856
    const-string v0, "enable"

    aget-object v1, p3, v3

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    .line 1857
    .local v0, "enable":Z
    iput-boolean v0, p0, Lcom/miui/server/turbosched/TurboSchedManagerService;->mTurboEnabledV2:Z

    .line 1858
    const-string v1, "persist.sys.turbosched.enable_v2"

    invoke-static {v0}, Ljava/lang/Boolean;->toString(Z)Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/os/SystemProperties;->set(Ljava/lang/String;Ljava/lang/String;)V

    .line 1859
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "mTurboEnabledV2 change to: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-boolean v2, p0, Lcom/miui/server/turbosched/TurboSchedManagerService;->mTurboEnabledV2:Z

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {p0, p2, v1}, Lcom/miui/server/turbosched/TurboSchedManagerService;->printCommandResult(Ljava/io/PrintWriter;Ljava/lang/String;)V

    .line 1860
    return v3

    .line 1863
    .end local v0    # "enable":Z
    :cond_3b
    array-length v0, p3

    if-lt v0, v3, :cond_3c

    const-string v0, "game"

    aget-object v1, p3, v2

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_3c

    .line 1865
    array-length v0, p3

    sub-int/2addr v0, v3

    new-array v0, v0, [Ljava/lang/String;

    .line 1866
    .local v0, "newArgs":[Ljava/lang/String;
    array-length v1, p3

    sub-int/2addr v1, v3

    invoke-static {p3, v3, v0, v2, v1}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 1867
    iget-object v1, p0, Lcom/miui/server/turbosched/TurboSchedManagerService;->mGameTurboAction:Lcom/miui/server/turbosched/GameTurboAction;

    invoke-virtual {v1, p1, p2, v0}, Lcom/miui/server/turbosched/GameTurboAction;->dump(Ljava/io/FileDescriptor;Ljava/io/PrintWriter;[Ljava/lang/String;)Z

    move-result v1

    return v1

    .line 1870
    .end local v0    # "newArgs":[Ljava/lang/String;
    :cond_3c
    array-length v0, p3

    if-lt v0, v3, :cond_3d

    const-string v0, "config"

    aget-object v1, p3, v2

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_3d

    .line 1871
    invoke-direct {p0, p2}, Lcom/miui/server/turbosched/TurboSchedManagerService;->dumpConfig(Ljava/io/PrintWriter;)V

    .line 1872
    return v3

    .line 1874
    :cond_3d
    return v2

    :sswitch_data_0
    .sparse-switch
        0x178a1 -> :sswitch_1
        0x1840b -> :sswitch_0
    .end sparse-switch

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_1
        :pswitch_0
    .end packed-switch
.end method

.method private printCommandResult(Ljava/io/PrintWriter;Ljava/lang/String;)V
    .locals 1
    .param p1, "pw"    # Ljava/io/PrintWriter;
    .param p2, "result"    # Ljava/lang/String;

    .line 1878
    const-string v0, "--------------------command result-----------------------"

    invoke-virtual {p1, v0}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V

    .line 1879
    invoke-virtual {p1, p2}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V

    .line 1880
    return-void
.end method

.method private recordRtPid(I)V
    .locals 7
    .param p1, "id"    # I

    .line 1938
    const/4 v0, 0x0

    .line 1939
    .local v0, "isSuccess":Z
    invoke-static {}, Landroid/os/TurboSchedMonitor;->getInstance()Landroid/os/TurboSchedMonitor;

    move-result-object v1

    invoke-virtual {v1}, Landroid/os/TurboSchedMonitor;->isCoreAppOptimizerEnabled()Z

    move-result v1

    if-nez v1, :cond_0

    .line 1940
    return-void

    .line 1942
    :cond_0
    const-string v1, "TurboSchedManagerService"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "recordrtpid,pid "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, ", MITEST"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Slog;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 1944
    sget-object v1, Lcom/miui/server/turbosched/TurboSchedManagerService;->mTurboLock:Ljava/lang/Object;

    monitor-enter v1

    .line 1945
    :try_start_0
    const-string v2, ""

    .line 1946
    .local v2, "str":Ljava/lang/String;
    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    .line 1947
    .local v3, "sb":Ljava/lang/StringBuilder;
    invoke-virtual {v3, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    .line 1948
    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    move-object v2, v4

    .line 1949
    const-string v4, "RR-PID"

    const-string v5, "/sys/module/metis/parameters/rt_binder_client"

    invoke-direct {p0, v4, v5, v2}, Lcom/miui/server/turbosched/TurboSchedManagerService;->writeTurboSchedNode(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Z

    move-result v4

    move v0, v4

    .line 1951
    invoke-static {}, Landroid/os/TurboSchedMonitor;->getInstance()Landroid/os/TurboSchedMonitor;

    move-result-object v4

    invoke-virtual {v4}, Landroid/os/TurboSchedMonitor;->isDebugMode()Z

    move-result v4

    if-eqz v4, :cond_1

    .line 1952
    const-string v4, "TurboSchedManagerService"

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "recordrtpid, isSuccess:"

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, v0}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, "pid "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, ", MITEST"

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5}, Landroid/util/Slog;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 1953
    .end local v2    # "str":Ljava/lang/String;
    .end local v3    # "sb":Ljava/lang/StringBuilder;
    :cond_1
    monitor-exit v1

    .line 1954
    return-void

    .line 1953
    :catchall_0
    move-exception v2

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v2
.end method

.method private registeForegroundReceiver()V
    .locals 3

    .line 900
    const-string v0, "TurboSchedManagerService"

    invoke-static {}, Landroid/os/TurboSchedMonitor;->getInstance()Landroid/os/TurboSchedMonitor;

    move-result-object v1

    invoke-virtual {v1}, Landroid/os/TurboSchedMonitor;->isCoreAppOptimizerEnabled()Z

    move-result v1

    if-nez v1, :cond_0

    iget-boolean v1, p0, Lcom/miui/server/turbosched/TurboSchedManagerService;->mInStartUpMode:Z

    if-eqz v1, :cond_1

    .line 902
    :cond_0
    :try_start_0
    const-string v1, "registerProcessObserver!"

    invoke-static {v0, v1}, Landroid/util/Slog;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 903
    iget-object v1, p0, Lcom/miui/server/turbosched/TurboSchedManagerService;->mActivityManagerService:Landroid/app/IActivityManager;

    iget-object v2, p0, Lcom/miui/server/turbosched/TurboSchedManagerService;->mProcessObserver:Landroid/app/IProcessObserver$Stub;

    invoke-interface {v1, v2}, Landroid/app/IActivityManager;->registerProcessObserver(Landroid/app/IProcessObserver;)V

    .line 904
    const/4 v1, 0x1

    iput-boolean v1, p0, Lcom/miui/server/turbosched/TurboSchedManagerService;->mRegistedForegroundReceiver:Z
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    .line 907
    goto :goto_0

    .line 905
    :catch_0
    move-exception v1

    .line 906
    .local v1, "e":Landroid/os/RemoteException;
    const-string v2, "registerProcessObserver failed"

    invoke-static {v0, v2}, Landroid/util/Slog;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 909
    .end local v1    # "e":Landroid/os/RemoteException;
    :cond_1
    :goto_0
    return-void
.end method

.method private registerObserver()V
    .locals 5

    .line 623
    iget-object v0, p0, Lcom/miui/server/turbosched/TurboSchedManagerService;->mContext:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    .line 624
    const-string v1, "cloud_turbo_sched_enable"

    invoke-static {v1}, Landroid/provider/Settings$System;->getUriFor(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v1

    iget-object v2, p0, Lcom/miui/server/turbosched/TurboSchedManagerService;->mCloudSwichObserver:Landroid/database/ContentObserver;

    .line 623
    const/4 v3, 0x0

    const/4 v4, -0x2

    invoke-virtual {v0, v1, v3, v2, v4}, Landroid/content/ContentResolver;->registerContentObserver(Landroid/net/Uri;ZLandroid/database/ContentObserver;I)V

    .line 628
    iget-object v0, p0, Lcom/miui/server/turbosched/TurboSchedManagerService;->mContext:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    .line 629
    const-string v1, "cloud_turbo_sched_enable_v2"

    invoke-static {v1}, Landroid/provider/Settings$System;->getUriFor(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v1

    iget-object v2, p0, Lcom/miui/server/turbosched/TurboSchedManagerService;->mCloudTurboschedMiuiSdkObserver:Landroid/database/ContentObserver;

    .line 628
    invoke-virtual {v0, v1, v3, v2, v4}, Landroid/content/ContentResolver;->registerContentObserver(Landroid/net/Uri;ZLandroid/database/ContentObserver;I)V

    .line 633
    iget-object v0, p0, Lcom/miui/server/turbosched/TurboSchedManagerService;->mContext:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    .line 634
    const-string v1, "cloud_turbo_sched_enable_core_app_optimizer"

    invoke-static {v1}, Landroid/provider/Settings$System;->getUriFor(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v1

    iget-object v2, p0, Lcom/miui/server/turbosched/TurboSchedManagerService;->mCloudCoreAppOptimizerEnableObserver:Landroid/database/ContentObserver;

    .line 633
    invoke-virtual {v0, v1, v3, v2, v4}, Landroid/content/ContentResolver;->registerContentObserver(Landroid/net/Uri;ZLandroid/database/ContentObserver;I)V

    .line 638
    iget-object v0, p0, Lcom/miui/server/turbosched/TurboSchedManagerService;->mContext:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    .line 639
    const-string v1, "cloud_turbo_sched_enable_core_top20_app_optimizer"

    invoke-static {v1}, Landroid/provider/Settings$System;->getUriFor(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v1

    iget-object v2, p0, Lcom/miui/server/turbosched/TurboSchedManagerService;->mCloudCoreTop20AppOptimizerEnableObserver:Landroid/database/ContentObserver;

    .line 638
    invoke-virtual {v0, v1, v3, v2, v4}, Landroid/content/ContentResolver;->registerContentObserver(Landroid/net/Uri;ZLandroid/database/ContentObserver;I)V

    .line 643
    iget-object v0, p0, Lcom/miui/server/turbosched/TurboSchedManagerService;->mContext:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    .line 644
    const-string v1, "cloud_turbo_sched_allow_list"

    invoke-static {v1}, Landroid/provider/Settings$System;->getUriFor(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v1

    iget-object v2, p0, Lcom/miui/server/turbosched/TurboSchedManagerService;->mCloudAllowListObserver:Landroid/database/ContentObserver;

    .line 643
    invoke-virtual {v0, v1, v3, v2, v4}, Landroid/content/ContentResolver;->registerContentObserver(Landroid/net/Uri;ZLandroid/database/ContentObserver;I)V

    .line 648
    iget-object v0, p0, Lcom/miui/server/turbosched/TurboSchedManagerService;->mContext:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    .line 649
    const-string v1, "cloud_turbo_sched_policy_list"

    invoke-static {v1}, Landroid/provider/Settings$System;->getUriFor(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v1

    iget-object v2, p0, Lcom/miui/server/turbosched/TurboSchedManagerService;->mCloudPolicyListObserver:Landroid/database/ContentObserver;

    .line 648
    invoke-virtual {v0, v1, v3, v2, v4}, Landroid/content/ContentResolver;->registerContentObserver(Landroid/net/Uri;ZLandroid/database/ContentObserver;I)V

    .line 653
    iget-object v0, p0, Lcom/miui/server/turbosched/TurboSchedManagerService;->mContext:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    .line 654
    const-string v1, "cloud_turbo_sched_thermal_break_enable"

    invoke-static {v1}, Landroid/provider/Settings$System;->getUriFor(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v1

    iget-object v2, p0, Lcom/miui/server/turbosched/TurboSchedManagerService;->mCloudThermalBreakEnableObserver:Landroid/database/ContentObserver;

    .line 653
    invoke-virtual {v0, v1, v3, v2, v4}, Landroid/content/ContentResolver;->registerContentObserver(Landroid/net/Uri;ZLandroid/database/ContentObserver;I)V

    .line 658
    iget-object v0, p0, Lcom/miui/server/turbosched/TurboSchedManagerService;->mContext:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    .line 659
    const-string v1, "cloud_turbo_sched_link_app_list"

    invoke-static {v1}, Landroid/provider/Settings$System;->getUriFor(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v1

    iget-object v2, p0, Lcom/miui/server/turbosched/TurboSchedManagerService;->mCloudLinkWhiteListObserver:Landroid/database/ContentObserver;

    .line 658
    invoke-virtual {v0, v1, v3, v2, v4}, Landroid/content/ContentResolver;->registerContentObserver(Landroid/net/Uri;ZLandroid/database/ContentObserver;I)V

    .line 663
    iget-object v0, p0, Lcom/miui/server/turbosched/TurboSchedManagerService;->mContext:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    .line 664
    const-string v1, "cloud_turbo_sched_frame_turbo_white_list"

    invoke-static {v1}, Landroid/provider/Settings$System;->getUriFor(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v1

    iget-object v2, p0, Lcom/miui/server/turbosched/TurboSchedManagerService;->mCloudFrameTurboWhiteListObserver:Landroid/database/ContentObserver;

    .line 663
    invoke-virtual {v0, v1, v3, v2, v4}, Landroid/content/ContentResolver;->registerContentObserver(Landroid/net/Uri;ZLandroid/database/ContentObserver;I)V

    .line 668
    iget-object v0, p0, Lcom/miui/server/turbosched/TurboSchedManagerService;->mContext:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    .line 669
    const-string v1, "cloud_turbo_sched_dps_enable"

    invoke-static {v1}, Landroid/provider/Settings$System;->getUriFor(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v1

    iget-object v2, p0, Lcom/miui/server/turbosched/TurboSchedManagerService;->mCloudDpsEnableObserver:Landroid/database/ContentObserver;

    .line 668
    invoke-virtual {v0, v1, v3, v2, v4}, Landroid/content/ContentResolver;->registerContentObserver(Landroid/net/Uri;ZLandroid/database/ContentObserver;I)V

    .line 671
    return-void
.end method

.method private sendTraceBeginMsg(Ljava/lang/String;)V
    .locals 1
    .param p1, "name"    # Ljava/lang/String;

    .line 594
    const-string v0, ""

    invoke-direct {p0, p1, v0}, Lcom/miui/server/turbosched/TurboSchedManagerService;->sendTraceBeginMsg(Ljava/lang/String;Ljava/lang/String;)V

    .line 595
    return-void
.end method

.method private sendTraceBeginMsg(Ljava/lang/String;Ljava/lang/String;)V
    .locals 4
    .param p1, "name"    # Ljava/lang/String;
    .param p2, "desc"    # Ljava/lang/String;

    .line 598
    iget-object v0, p0, Lcom/miui/server/turbosched/TurboSchedManagerService;->mTurboSchedHandler:Lcom/miui/server/turbosched/TurboSchedManagerService$TurboSchedHandler;

    if-nez v0, :cond_0

    .line 599
    const-string v0, "TurboSchedManagerService"

    const-string v1, "handler is null while sending trace begin message"

    invoke-static {v0, v1}, Landroid/util/Slog;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 600
    return-void

    .line 602
    :cond_0
    new-instance v0, Lcom/miui/server/turbosched/TurboSchedManagerService$TraceInfo;

    invoke-direct {v0}, Lcom/miui/server/turbosched/TurboSchedManagerService$TraceInfo;-><init>()V

    .line 603
    .local v0, "info":Lcom/miui/server/turbosched/TurboSchedManagerService$TraceInfo;
    iput-object p1, v0, Lcom/miui/server/turbosched/TurboSchedManagerService$TraceInfo;->name:Ljava/lang/String;

    .line 604
    iput-object p2, v0, Lcom/miui/server/turbosched/TurboSchedManagerService$TraceInfo;->desc:Ljava/lang/String;

    .line 605
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v1

    iput-wide v1, v0, Lcom/miui/server/turbosched/TurboSchedManagerService$TraceInfo;->startTime:J

    .line 606
    iget-object v1, p0, Lcom/miui/server/turbosched/TurboSchedManagerService;->mTurboSchedHandler:Lcom/miui/server/turbosched/TurboSchedManagerService$TurboSchedHandler;

    const/4 v2, 0x0

    invoke-virtual {v1, v2, v0}, Lcom/miui/server/turbosched/TurboSchedManagerService$TurboSchedHandler;->obtainMessage(ILjava/lang/Object;)Landroid/os/Message;

    move-result-object v1

    .line 607
    .local v1, "msg":Landroid/os/Message;
    invoke-static {}, Landroid/os/Process;->myTid()I

    move-result v2

    .line 608
    .local v2, "tid":I
    iput v2, v1, Landroid/os/Message;->arg1:I

    .line 609
    iget-object v3, p0, Lcom/miui/server/turbosched/TurboSchedManagerService;->mTurboSchedHandler:Lcom/miui/server/turbosched/TurboSchedManagerService$TurboSchedHandler;

    invoke-virtual {v3, v1}, Lcom/miui/server/turbosched/TurboSchedManagerService$TurboSchedHandler;->sendMessage(Landroid/os/Message;)Z

    .line 610
    return-void
.end method

.method private sendTraceEndMsg()V
    .locals 2

    .line 613
    iget-object v0, p0, Lcom/miui/server/turbosched/TurboSchedManagerService;->mTurboSchedHandler:Lcom/miui/server/turbosched/TurboSchedManagerService$TurboSchedHandler;

    if-nez v0, :cond_0

    .line 614
    const-string v0, "TurboSchedManagerService"

    const-string v1, "handler is null while sending trace end message"

    invoke-static {v0, v1}, Landroid/util/Slog;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 615
    return-void

    .line 617
    :cond_0
    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Lcom/miui/server/turbosched/TurboSchedManagerService$TurboSchedHandler;->obtainMessage(I)Landroid/os/Message;

    move-result-object v0

    .line 618
    .local v0, "msg":Landroid/os/Message;
    iget-object v1, p0, Lcom/miui/server/turbosched/TurboSchedManagerService;->mTurboSchedHandler:Lcom/miui/server/turbosched/TurboSchedManagerService$TurboSchedHandler;

    invoke-virtual {v1, v0}, Lcom/miui/server/turbosched/TurboSchedManagerService$TurboSchedHandler;->sendMessage(Landroid/os/Message;)Z

    .line 619
    return-void
.end method

.method private setDpsEnable(Z)V
    .locals 2
    .param p1, "enable"    # Z

    .line 969
    iget-boolean v0, p0, Lcom/miui/server/turbosched/TurboSchedManagerService;->mDPSSupport:Z

    if-nez v0, :cond_0

    .line 970
    const-string v0, "TurboSchedManagerService"

    const-string v1, "DPS not support"

    invoke-static {v0, v1}, Landroid/util/Slog;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 971
    return-void

    .line 973
    :cond_0
    iput-boolean p1, p0, Lcom/miui/server/turbosched/TurboSchedManagerService;->mDPSEnable:Z

    .line 974
    const-string v0, "persist.sys.turbosched.dps.enable"

    invoke-static {p1}, Ljava/lang/Boolean;->toString(Z)Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/os/SystemProperties;->set(Ljava/lang/String;Ljava/lang/String;)V

    .line 975
    return-void
.end method

.method private setPolicyTimeoutChecker(Ljava/lang/String;[IJLjava/lang/String;Ljava/util/Map;)V
    .locals 7
    .param p1, "logTag"    # Ljava/lang/String;
    .param p2, "tids"    # [I
    .param p3, "time"    # J
    .param p5, "cancelPath"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "[IJ",
            "Ljava/lang/String;",
            "Ljava/util/Map<",
            "Ljava/lang/Integer;",
            "Ljava/lang/Long;",
            ">;)V"
        }
    .end annotation

    .line 2010
    .local p6, "startTimeMap":Ljava/util/Map;, "Ljava/util/Map<Ljava/lang/Integer;Ljava/lang/Long;>;"
    iget-object v0, p0, Lcom/miui/server/turbosched/TurboSchedManagerService;->TurboSchedSDKTimer:Ljava/util/Timer;

    if-eqz v0, :cond_0

    .line 2011
    return-void

    .line 2013
    :cond_0
    array-length v0, p2

    const/4 v1, 0x0

    :goto_0
    if-ge v1, v0, :cond_1

    aget v2, p2, v1

    .line 2014
    .local v2, "tid":I
    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v4

    invoke-static {v4, v5}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v4

    invoke-interface {p6, v3, v4}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 2013
    .end local v2    # "tid":I
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 2016
    :cond_1
    new-instance v0, Ljava/util/Timer;

    invoke-direct {v0}, Ljava/util/Timer;-><init>()V

    iput-object v0, p0, Lcom/miui/server/turbosched/TurboSchedManagerService;->TurboSchedSDKTimer:Ljava/util/Timer;

    .line 2017
    new-instance v2, Lcom/miui/server/turbosched/TurboSchedManagerService$13;

    invoke-direct {v2, p0, p5, p1}, Lcom/miui/server/turbosched/TurboSchedManagerService$13;-><init>(Lcom/miui/server/turbosched/TurboSchedManagerService;Ljava/lang/String;Ljava/lang/String;)V

    iput-object v2, p0, Lcom/miui/server/turbosched/TurboSchedManagerService;->TurboSchedSDKTimerTask:Ljava/util/TimerTask;

    .line 2053
    iget-object v1, p0, Lcom/miui/server/turbosched/TurboSchedManagerService;->TurboSchedSDKTimer:Ljava/util/Timer;

    const-wide/16 v3, 0x3e8

    const-wide/16 v5, 0x3e8

    invoke-virtual/range {v1 .. v6}, Ljava/util/Timer;->schedule(Ljava/util/TimerTask;JJ)V

    .line 2054
    return-void
.end method

.method private setThermalBreakEnable(Z)V
    .locals 4
    .param p1, "enable"    # Z

    .line 1005
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "cloud thermal break set received :"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    const-string v1, "TurboSchedManagerService"

    invoke-static {v1, v0}, Landroid/util/Slog;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 1006
    iput-boolean p1, p0, Lcom/miui/server/turbosched/TurboSchedManagerService;->mThermalBreakEnabled:Z

    .line 1007
    const-string v0, "persist.sys.turbosched.thermal_break.enable"

    invoke-static {p1}, Ljava/lang/Boolean;->toString(Z)Ljava/lang/String;

    move-result-object v2

    invoke-static {v0, v2}, Landroid/os/SystemProperties;->set(Ljava/lang/String;Ljava/lang/String;)V

    .line 1008
    iget-boolean v0, p0, Lcom/miui/server/turbosched/TurboSchedManagerService;->mThermalBreakEnabled:Z

    if-eqz v0, :cond_0

    const-string v0, "1"

    goto :goto_0

    :cond_0
    const-string v0, "0"

    .line 1009
    .local v0, "enableStr":Ljava/lang/String;
    :goto_0
    const-string v2, "TB-Enabled"

    const-string v3, "/sys/module/metis/parameters/is_break_enable"

    invoke-direct {p0, v2, v3, v0}, Lcom/miui/server/turbosched/TurboSchedManagerService;->writeTurboSchedNode(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Z

    move-result v2

    .line 1010
    .local v2, "isSuccess":Z
    if-nez v2, :cond_1

    .line 1011
    const-string/jumbo v3, "write turbo sched thermal break failed"

    invoke-static {v1, v3}, Landroid/util/Slog;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 1013
    :cond_1
    return-void
.end method

.method private setThermalBreakThresholdMap(Ljava/lang/String;)V
    .locals 10
    .param p1, "thresholdStr"    # Ljava/lang/String;

    .line 1060
    if-eqz p1, :cond_1

    invoke-virtual {p1}, Ljava/lang/String;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_1

    .line 1061
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v1, "thermal break threshold set received :"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    const-string v1, "TurboSchedManagerService"

    invoke-static {v1, v0}, Landroid/util/Slog;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 1062
    const-string v0, ","

    invoke-virtual {p1, v0}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v0

    .line 1063
    .local v0, "thresholdList":[Ljava/lang/String;
    array-length v1, v0

    const/4 v2, 0x0

    move v3, v2

    :goto_0
    if-ge v3, v1, :cond_1

    aget-object v4, v0, v3

    .line 1064
    .local v4, "t":Ljava/lang/String;
    const-string v5, "-"

    invoke-virtual {v4, v5}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v5

    .line 1065
    .local v5, "thresholdValues":[Ljava/lang/String;
    array-length v6, v5

    const/4 v7, 0x2

    if-ne v6, v7, :cond_0

    .line 1066
    aget-object v6, v5, v2

    invoke-static {v6}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v6

    .line 1067
    .local v6, "temp":I
    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    const-string v8, " "

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    const/4 v9, 0x1

    aget-object v9, v5, v9

    invoke-virtual {v9}, Ljava/lang/String;->trim()Ljava/lang/String;

    move-result-object v9

    invoke-virtual {v7, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    .line 1068
    .local v7, "threshold":Ljava/lang/String;
    const-string v9, "_"

    invoke-virtual {v7, v9, v8}, Ljava/lang/String;->replace(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Ljava/lang/String;

    move-result-object v7

    .line 1069
    sget-object v8, Lcom/miui/server/turbosched/TurboSchedManagerService;->TEMPREATURE_THROSHOLD_BOOST_STRING_MAP:Ljava/util/Map;

    invoke-static {v6}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v9

    invoke-interface {v8, v9, v7}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 1063
    .end local v4    # "t":Ljava/lang/String;
    .end local v5    # "thresholdValues":[Ljava/lang/String;
    .end local v6    # "temp":I
    .end local v7    # "threshold":Ljava/lang/String;
    :cond_0
    add-int/lit8 v3, v3, 0x1

    goto :goto_0

    .line 1073
    .end local v0    # "thresholdList":[Ljava/lang/String;
    :cond_1
    return-void
.end method

.method private setTurboSchedActionInternal([IJLjava/lang/String;)Z
    .locals 9
    .param p1, "tids"    # [I
    .param p2, "time"    # J
    .param p4, "path"    # Ljava/lang/String;

    .line 492
    invoke-direct {p0}, Lcom/miui/server/turbosched/TurboSchedManagerService;->isTurboEnabled()Z

    move-result v0

    const/4 v1, 0x0

    if-nez v0, :cond_0

    .line 493
    iget-object v0, p0, Lcom/miui/server/turbosched/TurboSchedManagerService;->mHistoryLog:Landroid/util/LocalLog;

    const-string v2, "TS [V]: not enable"

    invoke-virtual {v0, v2}, Landroid/util/LocalLog;->log(Ljava/lang/String;)V

    .line 494
    return v1

    .line 496
    :cond_0
    array-length v0, p1

    const/4 v2, 0x3

    if-le v0, v2, :cond_1

    .line 497
    iget-object v0, p0, Lcom/miui/server/turbosched/TurboSchedManagerService;->mHistoryLog:Landroid/util/LocalLog;

    const-string v2, "TS [V]: tids length over limit"

    invoke-virtual {v0, v2}, Landroid/util/LocalLog;->log(Ljava/lang/String;)V

    .line 498
    return v1

    .line 500
    :cond_1
    const/4 v0, 0x0

    .line 502
    .local v0, "isSuccess":Z
    const-string v2, "V"

    invoke-direct {p0, v2}, Lcom/miui/server/turbosched/TurboSchedManagerService;->sendTraceBeginMsg(Ljava/lang/String;)V

    .line 504
    invoke-static {}, Landroid/os/TurboSchedMonitor;->getInstance()Landroid/os/TurboSchedMonitor;

    move-result-object v2

    invoke-virtual {v2}, Landroid/os/TurboSchedMonitor;->isDebugMode()Z

    move-result v2

    const-wide/16 v3, 0x40

    if-eqz v2, :cond_2

    .line 505
    const-string/jumbo v2, "setTurboSchedActionInternal"

    invoke-static {v3, v4, v2}, Landroid/os/Trace;->traceBegin(JLjava/lang/String;)V

    .line 507
    :cond_2
    sget-object v2, Lcom/miui/server/turbosched/TurboSchedManagerService;->mTurboLock:Ljava/lang/Object;

    monitor-enter v2

    .line 508
    :try_start_0
    const-string v5, ""

    .line 509
    .local v5, "str":Ljava/lang/String;
    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    .line 510
    .local v6, "sb":Ljava/lang/StringBuilder;
    aget v1, p1, v1

    invoke-virtual {v6, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    .line 511
    const/4 v1, 0x1

    .local v1, "i":I
    :goto_0
    array-length v7, p1

    if-ge v1, v7, :cond_4

    .line 512
    aget v7, p1, v1

    if-nez v7, :cond_3

    .line 513
    goto :goto_1

    .line 514
    :cond_3
    const-string v7, ":"

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 515
    aget v7, p1, v1

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    .line 511
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 517
    .end local v1    # "i":I
    :cond_4
    :goto_1
    const-string v1, "-"

    invoke-virtual {v6, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 518
    invoke-virtual {v6, p2, p3}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    .line 519
    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    .line 521
    .end local v5    # "str":Ljava/lang/String;
    .local v1, "str":Ljava/lang/String;
    const-string v5, "V"

    invoke-direct {p0, v5, p4, v1}, Lcom/miui/server/turbosched/TurboSchedManagerService;->writeTurboSchedNode(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Z

    move-result v5

    move v0, v5

    .line 523
    if-nez v0, :cond_5

    invoke-static {}, Landroid/os/TurboSchedMonitor;->getInstance()Landroid/os/TurboSchedMonitor;

    move-result-object v5

    invoke-virtual {v5}, Landroid/os/TurboSchedMonitor;->isDebugMode()Z

    move-result v5

    if-eqz v5, :cond_5

    .line 524
    const-string v5, "TurboSchedManagerService"

    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v8, "setTurboSchedActionInternal, not success, check file:"

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7, p4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-static {v5, v7}, Landroid/util/Slog;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 525
    .end local v1    # "str":Ljava/lang/String;
    .end local v6    # "sb":Ljava/lang/StringBuilder;
    :cond_5
    monitor-exit v2
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 526
    invoke-static {}, Landroid/os/TurboSchedMonitor;->getInstance()Landroid/os/TurboSchedMonitor;

    move-result-object v1

    invoke-virtual {v1}, Landroid/os/TurboSchedMonitor;->isDebugMode()Z

    move-result v1

    if-eqz v1, :cond_6

    .line 527
    invoke-static {v3, v4}, Landroid/os/Trace;->traceEnd(J)V

    .line 529
    :cond_6
    invoke-direct {p0}, Lcom/miui/server/turbosched/TurboSchedManagerService;->sendTraceEndMsg()V

    .line 531
    iget-object v1, p0, Lcom/miui/server/turbosched/TurboSchedManagerService;->mHistoryLog:Landroid/util/LocalLog;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "TS [V]: success: "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Landroid/util/LocalLog;->log(Ljava/lang/String;)V

    .line 532
    return v0

    .line 525
    :catchall_0
    move-exception v1

    :try_start_1
    monitor-exit v2
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    throw v1
.end method

.method private setTurboSchedActionParam(Ljava/lang/String;[IJLjava/lang/String;)Ljava/util/List;
    .locals 17
    .param p1, "logTag"    # Ljava/lang/String;
    .param p2, "tids"    # [I
    .param p3, "time"    # J
    .param p5, "type"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "[IJ",
            "Ljava/lang/String;",
            ")",
            "Ljava/util/List<",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation

    .line 1078
    move-object/from16 v9, p0

    move-object/from16 v10, p2

    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    move-object v11, v0

    .line 1079
    .local v11, "tidsList":Ljava/util/List;, "Ljava/util/List<Ljava/lang/Integer;>;"
    iget-object v12, v9, Lcom/miui/server/turbosched/TurboSchedManagerService;->mTurboschedSdkTidsLock:Ljava/lang/Object;

    monitor-enter v12

    .line 1080
    :try_start_0
    array-length v0, v10

    const/4 v1, 0x0

    move v13, v1

    :goto_0
    if-ge v13, v0, :cond_1

    aget v1, v10, v13

    move v14, v1

    .line 1081
    .local v14, "tid":I
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    invoke-static {v14}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    move-object/from16 v15, p5

    invoke-virtual {v1, v15}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    move-object v8, v1

    .line 1082
    .local v8, "paramId":Ljava/lang/String;
    iget-object v1, v9, Lcom/miui/server/turbosched/TurboSchedManagerService;->mTurboschedSDKParamMap:Ljava/util/Map;

    invoke-interface {v1, v8}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_2

    if-eqz v1, :cond_0

    .line 1083
    :try_start_1
    iget-object v7, v9, Lcom/miui/server/turbosched/TurboSchedManagerService;->mTurboschedSDKParamMap:Ljava/util/Map;

    new-instance v5, Lcom/miui/server/turbosched/TurboSchedManagerService$TurboschedSDKParam;

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v3

    move-object v1, v5

    move-object/from16 v2, p0

    move/from16 v16, v0

    move-object v0, v5

    move-wide/from16 v5, p3

    move-object v10, v7

    move v7, v14

    move-object v15, v8

    .end local v8    # "paramId":Ljava/lang/String;
    .local v15, "paramId":Ljava/lang/String;
    move-object/from16 v8, p5

    invoke-direct/range {v1 .. v8}, Lcom/miui/server/turbosched/TurboSchedManagerService$TurboschedSDKParam;-><init>(Lcom/miui/server/turbosched/TurboSchedManagerService;JJILjava/lang/String;)V

    invoke-interface {v10, v15, v0}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 1084
    iget-object v0, v9, Lcom/miui/server/turbosched/TurboSchedManagerService;->mHistoryLog:Landroid/util/LocalLog;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "TS ["

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_1

    move-object/from16 v10, p1

    :try_start_2
    invoke-virtual {v1, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "] : tid duplication, tids: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v14}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ",time: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    move-wide/from16 v7, p3

    :try_start_3
    invoke-virtual {v1, v7, v8}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/util/LocalLog;->log(Ljava/lang/String;)V

    goto :goto_2

    .line 1090
    .end local v14    # "tid":I
    .end local v15    # "paramId":Ljava/lang/String;
    :catchall_0
    move-exception v0

    goto :goto_1

    :catchall_1
    move-exception v0

    move-object/from16 v10, p1

    :goto_1
    move-wide/from16 v7, p3

    goto :goto_3

    .line 1086
    .restart local v8    # "paramId":Ljava/lang/String;
    .restart local v14    # "tid":I
    :cond_0
    move-object/from16 v10, p1

    move/from16 v16, v0

    move-object v15, v8

    move-wide/from16 v7, p3

    .end local v8    # "paramId":Ljava/lang/String;
    .restart local v15    # "paramId":Ljava/lang/String;
    invoke-static {v14}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    invoke-interface {v11, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 1087
    iget-object v0, v9, Lcom/miui/server/turbosched/TurboSchedManagerService;->mTurboschedSDKParamMap:Ljava/util/Map;

    new-instance v5, Lcom/miui/server/turbosched/TurboSchedManagerService$TurboschedSDKParam;

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v3

    move-object v1, v5

    move-object/from16 v2, p0

    move-object v9, v5

    move-wide/from16 v5, p3

    move v7, v14

    move-object/from16 v8, p5

    invoke-direct/range {v1 .. v8}, Lcom/miui/server/turbosched/TurboSchedManagerService$TurboschedSDKParam;-><init>(Lcom/miui/server/turbosched/TurboSchedManagerService;JJILjava/lang/String;)V

    invoke-interface {v0, v15, v9}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 1080
    .end local v14    # "tid":I
    .end local v15    # "paramId":Ljava/lang/String;
    :goto_2
    add-int/lit8 v13, v13, 0x1

    move-object/from16 v9, p0

    move-object/from16 v10, p2

    move/from16 v0, v16

    goto/16 :goto_0

    .line 1090
    :cond_1
    move-object/from16 v10, p1

    monitor-exit v12

    .line 1091
    return-object v11

    .line 1090
    :catchall_2
    move-exception v0

    move-object/from16 v10, p1

    :goto_3
    monitor-exit v12
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_3

    throw v0

    :catchall_3
    move-exception v0

    goto :goto_3
.end method

.method private syncTop20AppList(Z)V
    .locals 2
    .param p1, "optimizerEnabled"    # Z

    .line 2170
    iput-boolean p1, p0, Lcom/miui/server/turbosched/TurboSchedManagerService;->mEnableTop20:Z

    .line 2171
    const-string v0, "persist.sys.turbosched.enabletop20app"

    invoke-static {p1}, Ljava/lang/Boolean;->toString(Z)Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/os/SystemProperties;->set(Ljava/lang/String;Ljava/lang/String;)V

    .line 2172
    sget-boolean v0, Lmiui/os/Build;->IS_TABLET:Z

    if-nez v0, :cond_0

    .line 2173
    invoke-virtual {p0}, Lcom/miui/server/turbosched/TurboSchedManagerService;->getCoreAppList()Ljava/util/List;

    move-result-object v0

    .line 2174
    .local v0, "apps":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;"
    invoke-direct {p0, v0}, Lcom/miui/server/turbosched/TurboSchedManagerService;->syncTopAppList(Ljava/util/List;)V

    .line 2175
    .end local v0    # "apps":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;"
    goto :goto_0

    .line 2177
    :cond_0
    sget-object v0, Lcom/miui/server/turbosched/TurboSchedManagerService;->TABLET_TOP8_APP_LIST:Ljava/util/List;

    .line 2178
    .restart local v0    # "apps":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;"
    invoke-direct {p0, v0}, Lcom/miui/server/turbosched/TurboSchedManagerService;->syncTopAppList(Ljava/util/List;)V

    .line 2180
    .end local v0    # "apps":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;"
    :goto_0
    return-void
.end method

.method private syncTopAppList(Ljava/util/List;)V
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List<",
            "Ljava/lang/String;",
            ">;)V"
        }
    .end annotation

    .line 2143
    .local p1, "coreAppList":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;"
    iget-boolean v0, p0, Lcom/miui/server/turbosched/TurboSchedManagerService;->mIsGlobal:Z

    if-eqz v0, :cond_0

    .line 2144
    sget-object v0, Lcom/miui/server/turbosched/TurboSchedManagerService;->DEFAULT_GL_APP_LIST:Ljava/util/List;

    iput-object v0, p0, Lcom/miui/server/turbosched/TurboSchedManagerService;->mCoreAppsPackageNameList:Ljava/util/List;

    goto :goto_2

    .line 2146
    :cond_0
    sget-boolean v0, Lmiui/os/Build;->IS_TABLET:Z

    if-nez v0, :cond_2

    .line 2147
    iget-boolean v0, p0, Lcom/miui/server/turbosched/TurboSchedManagerService;->mEnableTop20:Z

    if-eqz v0, :cond_1

    .line 2148
    sget-object v0, Lcom/miui/server/turbosched/TurboSchedManagerService;->DEFAULT_TOP20_APP_LIST:Ljava/util/List;

    iput-object v0, p0, Lcom/miui/server/turbosched/TurboSchedManagerService;->mCoreAppsPackageNameList:Ljava/util/List;

    goto :goto_0

    .line 2151
    :cond_1
    sget-object v0, Lcom/miui/server/turbosched/TurboSchedManagerService;->DEFAULT_APP_LIST:Ljava/util/List;

    iput-object v0, p0, Lcom/miui/server/turbosched/TurboSchedManagerService;->mCoreAppsPackageNameList:Ljava/util/List;

    goto :goto_0

    .line 2154
    :cond_2
    iget-boolean v0, p0, Lcom/miui/server/turbosched/TurboSchedManagerService;->mEnableTop20:Z

    if-eqz v0, :cond_3

    .line 2155
    sget-object v0, Lcom/miui/server/turbosched/TurboSchedManagerService;->TABLET_TOP20_APP_LIST:Ljava/util/List;

    iput-object v0, p0, Lcom/miui/server/turbosched/TurboSchedManagerService;->mCoreAppsPackageNameList:Ljava/util/List;

    goto :goto_0

    .line 2157
    :cond_3
    sget-object v0, Lcom/miui/server/turbosched/TurboSchedManagerService;->TABLET_TOP8_APP_LIST:Ljava/util/List;

    iput-object v0, p0, Lcom/miui/server/turbosched/TurboSchedManagerService;->mCoreAppsPackageNameList:Ljava/util/List;

    .line 2160
    :goto_0
    invoke-interface {p1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v0

    :goto_1
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_5

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/String;

    .line 2161
    .local v1, "app":Ljava/lang/String;
    iget-object v2, p0, Lcom/miui/server/turbosched/TurboSchedManagerService;->mCoreAppsPackageNameList:Ljava/util/List;

    invoke-interface {v2, v1}, Ljava/util/List;->contains(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_4

    .line 2162
    iget-object v2, p0, Lcom/miui/server/turbosched/TurboSchedManagerService;->mCoreAppsPackageNameList:Ljava/util/List;

    invoke-interface {v2, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 2164
    .end local v1    # "app":Ljava/lang/String;
    :cond_4
    goto :goto_1

    .line 2166
    :cond_5
    :goto_2
    invoke-static {}, Landroid/os/TurboSchedMonitor;->getInstance()Landroid/os/TurboSchedMonitor;

    move-result-object v0

    iget-object v1, p0, Lcom/miui/server/turbosched/TurboSchedManagerService;->mContext:Landroid/content/Context;

    iget-object v2, p0, Lcom/miui/server/turbosched/TurboSchedManagerService;->mCoreAppsPackageNameList:Ljava/util/List;

    invoke-virtual {v0, v1, v2}, Landroid/os/TurboSchedMonitor;->setCoreAppList(Landroid/content/Context;Ljava/util/List;)V

    .line 2167
    return-void
.end method

.method private updateCloudAllowListProp()V
    .locals 6

    .line 912
    iget-object v0, p0, Lcom/miui/server/turbosched/TurboSchedManagerService;->mContext:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    const-string v1, "cloud_turbo_sched_allow_list"

    const/4 v2, -0x2

    invoke-static {v0, v1, v2}, Landroid/provider/Settings$System;->getStringForUser(Landroid/content/ContentResolver;Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v0

    .line 915
    .local v0, "str":Ljava/lang/String;
    iget-boolean v1, p0, Lcom/miui/server/turbosched/TurboSchedManagerService;->mReceiveCloudData:Z

    if-nez v1, :cond_0

    .line 916
    return-void

    .line 918
    :cond_0
    if-eqz v0, :cond_1

    invoke-virtual {v0}, Ljava/lang/String;->isEmpty()Z

    move-result v1

    if-nez v1, :cond_1

    .line 919
    const-string v1, ","

    invoke-virtual {v0, v1}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v1

    .line 920
    .local v1, "pkgName":[Ljava/lang/String;
    iget-object v2, p0, Lcom/miui/server/turbosched/TurboSchedManagerService;->mCallerAllowList:Ljava/util/List;

    invoke-interface {v2}, Ljava/util/List;->clear()V

    .line 921
    array-length v2, v1

    if-lez v2, :cond_1

    .line 922
    array-length v2, v1

    const/4 v3, 0x0

    :goto_0
    if-ge v3, v2, :cond_1

    aget-object v4, v1, v3

    .line 923
    .local v4, "name":Ljava/lang/String;
    iget-object v5, p0, Lcom/miui/server/turbosched/TurboSchedManagerService;->mCallerAllowList:Ljava/util/List;

    invoke-interface {v5, v4}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 922
    .end local v4    # "name":Ljava/lang/String;
    add-int/lit8 v3, v3, 0x1

    goto :goto_0

    .line 927
    .end local v1    # "pkgName":[Ljava/lang/String;
    :cond_1
    return-void
.end method

.method private updateCloudControlProp()V
    .locals 1

    .line 781
    invoke-direct {p0}, Lcom/miui/server/turbosched/TurboSchedManagerService;->updateCoreAppOptimizerEnableProp()V

    .line 782
    invoke-static {}, Landroid/os/TurboSchedMonitor;->getInstance()Landroid/os/TurboSchedMonitor;

    move-result-object v0

    invoke-virtual {v0}, Landroid/os/TurboSchedMonitor;->isCoreAppOptimizerEnabled()Z

    move-result v0

    invoke-direct {p0, v0}, Lcom/miui/server/turbosched/TurboSchedManagerService;->updateEnableProp(Z)V

    .line 783
    invoke-direct {p0}, Lcom/miui/server/turbosched/TurboSchedManagerService;->updateCoreTop20AppOptimizerEnableProp()V

    .line 784
    invoke-direct {p0}, Lcom/miui/server/turbosched/TurboSchedManagerService;->updateCloudAllowListProp()V

    .line 785
    invoke-direct {p0}, Lcom/miui/server/turbosched/TurboSchedManagerService;->updateCloudPolicyListProp()V

    .line 786
    invoke-direct {p0}, Lcom/miui/server/turbosched/TurboSchedManagerService;->updateCloudThermalBreakEnableProp()V

    .line 787
    invoke-direct {p0}, Lcom/miui/server/turbosched/TurboSchedManagerService;->updateCloudThermalBreakThresholdProp()V

    .line 788
    invoke-direct {p0}, Lcom/miui/server/turbosched/TurboSchedManagerService;->updateCloudLinkWhiteListProp()V

    .line 789
    invoke-direct {p0}, Lcom/miui/server/turbosched/TurboSchedManagerService;->updateCloudFrameTurboWhiteListProp()V

    .line 790
    invoke-direct {p0}, Lcom/miui/server/turbosched/TurboSchedManagerService;->updateCloudDpsEnableProp()V

    .line 791
    return-void
.end method

.method private updateCloudDpsEnableProp()V
    .locals 3

    .line 957
    iget-object v0, p0, Lcom/miui/server/turbosched/TurboSchedManagerService;->mContext:Landroid/content/Context;

    .line 958
    invoke-virtual {v0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    .line 957
    const-string v1, "cloud_turbo_sched_dps_enable"

    const/4 v2, -0x2

    invoke-static {v0, v1, v2}, Landroid/provider/Settings$System;->getStringForUser(Landroid/content/ContentResolver;Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v0

    .line 960
    .local v0, "enableStr":Ljava/lang/String;
    iget-boolean v1, p0, Lcom/miui/server/turbosched/TurboSchedManagerService;->mReceiveCloudData:Z

    if-nez v1, :cond_0

    .line 961
    return-void

    .line 963
    :cond_0
    if-eqz v0, :cond_1

    invoke-virtual {v0}, Ljava/lang/String;->isEmpty()Z

    move-result v1

    if-nez v1, :cond_1

    .line 964
    invoke-static {v0}, Ljava/lang/Boolean;->parseBoolean(Ljava/lang/String;)Z

    move-result v1

    invoke-direct {p0, v1}, Lcom/miui/server/turbosched/TurboSchedManagerService;->setDpsEnable(Z)V

    .line 966
    :cond_1
    return-void
.end method

.method private updateCloudFrameTurboWhiteListProp()V
    .locals 3

    .line 1045
    iget-object v0, p0, Lcom/miui/server/turbosched/TurboSchedManagerService;->mContext:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    const-string v1, "cloud_turbo_sched_frame_turbo_white_list"

    const/4 v2, -0x2

    invoke-static {v0, v1, v2}, Landroid/provider/Settings$System;->getStringForUser(Landroid/content/ContentResolver;Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v0

    .line 1048
    .local v0, "str":Ljava/lang/String;
    iget-boolean v1, p0, Lcom/miui/server/turbosched/TurboSchedManagerService;->mReceiveCloudData:Z

    if-nez v1, :cond_0

    .line 1049
    return-void

    .line 1051
    :cond_0
    if-eqz v0, :cond_1

    invoke-virtual {v0}, Ljava/lang/String;->isEmpty()Z

    move-result v1

    if-nez v1, :cond_1

    .line 1052
    const-string v1, ","

    invoke-virtual {v0, v1}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v1

    .line 1053
    .local v1, "pkgScenes":[Ljava/lang/String;
    iget-object v2, p0, Lcom/miui/server/turbosched/TurboSchedManagerService;->mFrameTurboAction:Lcom/miui/server/turbosched/FrameTurboAction;

    invoke-virtual {v2, v1}, Lcom/miui/server/turbosched/FrameTurboAction;->updateWhiteList([Ljava/lang/String;)V

    .line 1055
    .end local v1    # "pkgScenes":[Ljava/lang/String;
    :cond_1
    return-void
.end method

.method private updateCloudLinkWhiteListProp()V
    .locals 6

    .line 1026
    iget-object v0, p0, Lcom/miui/server/turbosched/TurboSchedManagerService;->mContext:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    const-string v1, "cloud_turbo_sched_link_app_list"

    const/4 v2, -0x2

    invoke-static {v0, v1, v2}, Landroid/provider/Settings$System;->getStringForUser(Landroid/content/ContentResolver;Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v0

    .line 1029
    .local v0, "str":Ljava/lang/String;
    iget-boolean v1, p0, Lcom/miui/server/turbosched/TurboSchedManagerService;->mReceiveCloudData:Z

    if-nez v1, :cond_0

    .line 1030
    return-void

    .line 1032
    :cond_0
    if-eqz v0, :cond_1

    invoke-virtual {v0}, Ljava/lang/String;->isEmpty()Z

    move-result v1

    if-nez v1, :cond_1

    .line 1033
    const-string v1, ","

    invoke-virtual {v0, v1}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v1

    .line 1034
    .local v1, "pkgName":[Ljava/lang/String;
    iget-object v2, p0, Lcom/miui/server/turbosched/TurboSchedManagerService;->mAppsLinkVipList:Ljava/util/List;

    invoke-interface {v2}, Ljava/util/List;->clear()V

    .line 1035
    array-length v2, v1

    if-lez v2, :cond_1

    .line 1036
    array-length v2, v1

    const/4 v3, 0x0

    :goto_0
    if-ge v3, v2, :cond_1

    aget-object v4, v1, v3

    .line 1037
    .local v4, "name":Ljava/lang/String;
    iget-object v5, p0, Lcom/miui/server/turbosched/TurboSchedManagerService;->mAppsLinkVipList:Ljava/util/List;

    invoke-interface {v5, v4}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 1036
    .end local v4    # "name":Ljava/lang/String;
    add-int/lit8 v3, v3, 0x1

    goto :goto_0

    .line 1041
    .end local v1    # "pkgName":[Ljava/lang/String;
    :cond_1
    return-void
.end method

.method private updateCloudPolicyListProp()V
    .locals 7

    .line 930
    iget-object v0, p0, Lcom/miui/server/turbosched/TurboSchedManagerService;->mContext:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    const-string v1, "cloud_turbo_sched_policy_list"

    const/4 v2, -0x2

    invoke-static {v0, v1, v2}, Landroid/provider/Settings$System;->getStringForUser(Landroid/content/ContentResolver;Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v0

    .line 933
    .local v0, "str":Ljava/lang/String;
    iget-boolean v1, p0, Lcom/miui/server/turbosched/TurboSchedManagerService;->mReceiveCloudData:Z

    if-nez v1, :cond_0

    .line 934
    return-void

    .line 936
    :cond_0
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    .line 937
    .local v1, "policyList":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;"
    const-string v2, "TurboSchedManagerService"

    if-eqz v0, :cond_3

    invoke-virtual {v0}, Ljava/lang/String;->isEmpty()Z

    move-result v3

    if-nez v3, :cond_3

    .line 938
    invoke-interface {v1}, Ljava/util/List;->clear()V

    .line 939
    const-string v3, ","

    invoke-virtual {v0, v3}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v3

    .line 940
    .local v3, "policyListName":[Ljava/lang/String;
    array-length v4, v3

    if-lez v4, :cond_2

    .line 941
    array-length v4, v3

    const/4 v5, 0x0

    :goto_0
    if-ge v5, v4, :cond_1

    aget-object v6, v3, v5

    .line 942
    .local v6, "name":Ljava/lang/String;
    invoke-interface {v1, v6}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 941
    .end local v6    # "name":Ljava/lang/String;
    add-int/lit8 v5, v5, 0x1

    goto :goto_0

    .line 944
    :cond_1
    invoke-static {v1}, Lcom/miui/server/turbosched/TurboSchedConfig;->setPolicyList(Ljava/util/List;)V

    .line 945
    iput-object v1, p0, Lcom/miui/server/turbosched/TurboSchedManagerService;->mPolicyListBackup:Ljava/util/List;

    .line 946
    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "Update Cloud PolicyList Prop - mPolicyList: "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-interface {v1}, Ljava/util/List;->toArray()[Ljava/lang/Object;

    move-result-object v5

    invoke-static {v5}, Ljava/util/Arrays;->toString([Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v2, v4}, Landroid/util/Slog;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 948
    .end local v3    # "policyListName":[Ljava/lang/String;
    :cond_2
    goto :goto_1

    .line 949
    :cond_3
    invoke-interface {v1}, Ljava/util/List;->clear()V

    .line 950
    invoke-static {v1}, Lcom/miui/server/turbosched/TurboSchedConfig;->setPolicyList(Ljava/util/List;)V

    .line 951
    iput-object v1, p0, Lcom/miui/server/turbosched/TurboSchedManagerService;->mPolicyListBackup:Ljava/util/List;

    .line 952
    const-string v3, "Update Cloud PolicyList Prop - mPolicyList: null"

    invoke-static {v2, v3}, Landroid/util/Slog;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 954
    :goto_1
    return-void
.end method

.method private updateCloudThermalBreakEnableProp()V
    .locals 3

    .line 993
    iget-object v0, p0, Lcom/miui/server/turbosched/TurboSchedManagerService;->mContext:Landroid/content/Context;

    .line 994
    invoke-virtual {v0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    .line 993
    const-string v1, "cloud_turbo_sched_thermal_break_enable"

    const/4 v2, -0x2

    invoke-static {v0, v1, v2}, Landroid/provider/Settings$System;->getStringForUser(Landroid/content/ContentResolver;Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v0

    .line 996
    .local v0, "enableStr":Ljava/lang/String;
    iget-boolean v1, p0, Lcom/miui/server/turbosched/TurboSchedManagerService;->mReceiveCloudData:Z

    if-nez v1, :cond_0

    .line 997
    return-void

    .line 999
    :cond_0
    if-eqz v0, :cond_1

    invoke-virtual {v0}, Ljava/lang/String;->isEmpty()Z

    move-result v1

    if-nez v1, :cond_1

    .line 1000
    invoke-static {v0}, Ljava/lang/Boolean;->parseBoolean(Ljava/lang/String;)Z

    move-result v1

    invoke-direct {p0, v1}, Lcom/miui/server/turbosched/TurboSchedManagerService;->setThermalBreakEnable(Z)V

    .line 1002
    :cond_1
    return-void
.end method

.method private updateCloudThermalBreakThresholdProp()V
    .locals 3

    .line 1016
    iget-object v0, p0, Lcom/miui/server/turbosched/TurboSchedManagerService;->mContext:Landroid/content/Context;

    .line 1017
    invoke-virtual {v0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    .line 1016
    const-string v1, "cloud_turbo_sched_thermal_break_threshold"

    const/4 v2, -0x2

    invoke-static {v0, v1, v2}, Landroid/provider/Settings$System;->getStringForUser(Landroid/content/ContentResolver;Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v0

    .line 1019
    .local v0, "thresholdStr":Ljava/lang/String;
    iget-boolean v1, p0, Lcom/miui/server/turbosched/TurboSchedManagerService;->mReceiveCloudData:Z

    if-nez v1, :cond_0

    .line 1020
    return-void

    .line 1022
    :cond_0
    invoke-direct {p0, v0}, Lcom/miui/server/turbosched/TurboSchedManagerService;->setThermalBreakThresholdMap(Ljava/lang/String;)V

    .line 1023
    return-void
.end method

.method private updateCoreAppOptimizerEnableProp()V
    .locals 6

    .line 858
    const/4 v0, 0x0

    .line 859
    .local v0, "coreAppOptimizerEnabled":Z
    iget-object v1, p0, Lcom/miui/server/turbosched/TurboSchedManagerService;->mContext:Landroid/content/Context;

    .line 860
    invoke-virtual {v1}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v1

    .line 859
    const-string v2, "cloud_turbo_sched_enable_core_app_optimizer"

    const/4 v3, -0x2

    invoke-static {v1, v2, v3}, Landroid/provider/Settings$System;->getStringForUser(Landroid/content/ContentResolver;Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v1

    .line 862
    .local v1, "enableStr":Ljava/lang/String;
    iget-boolean v2, p0, Lcom/miui/server/turbosched/TurboSchedManagerService;->mReceiveCloudData:Z

    if-nez v2, :cond_0

    .line 863
    return-void

    .line 865
    :cond_0
    const-string v2, "TurboSchedManagerService"

    if-eqz v1, :cond_1

    invoke-virtual {v1}, Ljava/lang/String;->isEmpty()Z

    move-result v3

    if-nez v3, :cond_1

    .line 866
    invoke-static {v1}, Ljava/lang/Boolean;->parseBoolean(Ljava/lang/String;)Z

    move-result v0

    .line 867
    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "cloud core app optimizer set received: "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Landroid/util/Slog;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 870
    :cond_1
    move v3, v0

    .line 871
    .local v3, "enable":I
    invoke-static {}, Landroid/os/TurboSchedMonitor;->getInstance()Landroid/os/TurboSchedMonitor;

    move-result-object v4

    invoke-virtual {v4, v3}, Landroid/os/TurboSchedMonitor;->enableCoreAppOptimizer(I)V

    .line 872
    invoke-direct {p0, v0}, Lcom/miui/server/turbosched/TurboSchedManagerService;->enableGaea(Z)V

    .line 873
    if-eqz v0, :cond_2

    iget-boolean v4, p0, Lcom/miui/server/turbosched/TurboSchedManagerService;->mRegistedForegroundReceiver:Z

    if-nez v4, :cond_2

    .line 874
    invoke-direct {p0}, Lcom/miui/server/turbosched/TurboSchedManagerService;->registeForegroundReceiver()V

    .line 876
    :cond_2
    invoke-direct {p0, v0}, Lcom/miui/server/turbosched/TurboSchedManagerService;->enableFrameBoostInKernel(Z)V

    .line 877
    if-eqz v0, :cond_3

    .line 878
    const/4 v4, 0x1

    iput-boolean v4, p0, Lcom/miui/server/turbosched/TurboSchedManagerService;->mKernelEnabelUpdate:Z

    .line 880
    :cond_3
    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "cloud data update done - coreapp enable: "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v2, v4}, Landroid/util/Slog;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 881
    return-void
.end method

.method private updateCoreTop20AppOptimizerEnableProp()V
    .locals 5

    .line 884
    const/4 v0, 0x0

    .line 885
    .local v0, "optimizerEnabled":Z
    iget-object v1, p0, Lcom/miui/server/turbosched/TurboSchedManagerService;->mContext:Landroid/content/Context;

    .line 886
    invoke-virtual {v1}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v1

    .line 885
    const-string v2, "cloud_turbo_sched_enable_core_top20_app_optimizer"

    const/4 v3, -0x2

    invoke-static {v1, v2, v3}, Landroid/provider/Settings$System;->getStringForUser(Landroid/content/ContentResolver;Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v1

    .line 888
    .local v1, "enableStr":Ljava/lang/String;
    iget-boolean v2, p0, Lcom/miui/server/turbosched/TurboSchedManagerService;->mReceiveCloudData:Z

    if-nez v2, :cond_0

    .line 889
    return-void

    .line 891
    :cond_0
    const-string v2, "TurboSchedManagerService"

    if-eqz v1, :cond_1

    invoke-virtual {v1}, Ljava/lang/String;->isEmpty()Z

    move-result v3

    if-nez v3, :cond_1

    .line 892
    invoke-static {v1}, Ljava/lang/Boolean;->parseBoolean(Ljava/lang/String;)Z

    move-result v0

    .line 893
    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "cloud core top20 app optimizer set received: "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Landroid/util/Slog;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 895
    :cond_1
    invoke-direct {p0, v0}, Lcom/miui/server/turbosched/TurboSchedManagerService;->syncTop20AppList(Z)V

    .line 896
    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "cloud data update done - core top20 app enable: "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Landroid/util/Slog;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 897
    return-void
.end method

.method private updateEnableMiuiSdkProp(Z)V
    .locals 5
    .param p1, "optimizerInitEnabled"    # Z

    .line 836
    const/4 v0, 0x0

    .line 837
    .local v0, "optimizerEnabled":Z
    iget-object v1, p0, Lcom/miui/server/turbosched/TurboSchedManagerService;->mContext:Landroid/content/Context;

    .line 838
    invoke-virtual {v1}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v1

    .line 837
    const-string v2, "cloud_turbo_sched_enable_v2"

    const/4 v3, -0x2

    invoke-static {v1, v2, v3}, Landroid/provider/Settings$System;->getStringForUser(Landroid/content/ContentResolver;Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v1

    .line 840
    .local v1, "enableStr":Ljava/lang/String;
    iget-boolean v2, p0, Lcom/miui/server/turbosched/TurboSchedManagerService;->mReceiveCloudData:Z

    if-nez v2, :cond_0

    .line 841
    return-void

    .line 843
    :cond_0
    const-string v2, "TurboSchedManagerService"

    if-eqz v1, :cond_1

    invoke-virtual {v1}, Ljava/lang/String;->isEmpty()Z

    move-result v3

    if-nez v3, :cond_1

    .line 844
    invoke-static {v1}, Ljava/lang/Boolean;->parseBoolean(Ljava/lang/String;)Z

    move-result v0

    .line 845
    iput-boolean v0, p0, Lcom/miui/server/turbosched/TurboSchedManagerService;->mCloudControlEnabled:Z

    .line 846
    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "cloud turbosched v2 set received: "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    iget-boolean v4, p0, Lcom/miui/server/turbosched/TurboSchedManagerService;->mCloudControlEnabled:Z

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Landroid/util/Slog;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 847
    iget-boolean v3, p0, Lcom/miui/server/turbosched/TurboSchedManagerService;->mRegistedForegroundReceiver:Z

    if-nez v3, :cond_1

    .line 848
    invoke-direct {p0}, Lcom/miui/server/turbosched/TurboSchedManagerService;->registeForegroundReceiver()V

    .line 852
    :cond_1
    or-int/2addr v0, p1

    .line 853
    invoke-virtual {p0, v0}, Lcom/miui/server/turbosched/TurboSchedManagerService;->enableTurboSchedV2(Z)V

    .line 854
    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "cloud data update done - turbosched v2: "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Landroid/util/Slog;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 855
    return-void
.end method

.method private updateEnableProp(Z)V
    .locals 5
    .param p1, "optimizerInitEnabled"    # Z

    .line 803
    const/4 v0, 0x0

    .line 804
    .local v0, "optimizerEnabled":Z
    iget-object v1, p0, Lcom/miui/server/turbosched/TurboSchedManagerService;->mContext:Landroid/content/Context;

    .line 805
    invoke-virtual {v1}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v1

    .line 804
    const-string v2, "cloud_turbo_sched_enable"

    const/4 v3, -0x2

    invoke-static {v1, v2, v3}, Landroid/provider/Settings$System;->getStringForUser(Landroid/content/ContentResolver;Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v1

    .line 807
    .local v1, "enableStr":Ljava/lang/String;
    iget-boolean v2, p0, Lcom/miui/server/turbosched/TurboSchedManagerService;->mReceiveCloudData:Z

    if-nez v2, :cond_0

    .line 808
    return-void

    .line 810
    :cond_0
    const-string v2, "TurboSchedManagerService"

    if-eqz v1, :cond_1

    invoke-virtual {v1}, Ljava/lang/String;->isEmpty()Z

    move-result v3

    if-nez v3, :cond_1

    .line 811
    invoke-static {v1}, Ljava/lang/Boolean;->parseBoolean(Ljava/lang/String;)Z

    move-result v0

    .line 812
    iput-boolean v0, p0, Lcom/miui/server/turbosched/TurboSchedManagerService;->mCloudControlEnabled:Z

    .line 813
    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "cloud turbosched v1 set received: "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    iget-boolean v4, p0, Lcom/miui/server/turbosched/TurboSchedManagerService;->mCloudControlEnabled:Z

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Landroid/util/Slog;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 816
    :cond_1
    invoke-direct {p0, v0}, Lcom/miui/server/turbosched/TurboSchedManagerService;->enableTurboSched(Z)V

    .line 817
    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "cloud data update done - turbosched v1: "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Landroid/util/Slog;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 819
    invoke-direct {p0, v0}, Lcom/miui/server/turbosched/TurboSchedManagerService;->updateEnableMiuiSdkProp(Z)V

    .line 821
    if-nez p1, :cond_2

    .line 822
    move v2, v0

    .line 823
    .local v2, "enable":I
    invoke-static {}, Landroid/os/TurboSchedMonitor;->getInstance()Landroid/os/TurboSchedMonitor;

    move-result-object v3

    invoke-virtual {v3, v2}, Landroid/os/TurboSchedMonitor;->enableCoreAppOptimizer(I)V

    .line 824
    invoke-direct {p0, v0}, Lcom/miui/server/turbosched/TurboSchedManagerService;->enableFrameBoostInKernel(Z)V

    .line 825
    if-eqz v0, :cond_2

    .line 826
    const/4 v3, 0x1

    iput-boolean v3, p0, Lcom/miui/server/turbosched/TurboSchedManagerService;->mKernelEnabelUpdate:Z

    .line 830
    .end local v2    # "enable":I
    :cond_2
    if-eqz v0, :cond_3

    iget-boolean v2, p0, Lcom/miui/server/turbosched/TurboSchedManagerService;->mRegistedForegroundReceiver:Z

    if-nez v2, :cond_3

    .line 831
    invoke-direct {p0}, Lcom/miui/server/turbosched/TurboSchedManagerService;->registeForegroundReceiver()V

    .line 833
    :cond_3
    return-void
.end method

.method private updateLocalPolicyListProp()V
    .locals 6

    .line 978
    const-string v0, "persist.sys.turbosched.policy_list"

    const-string v1, ""

    invoke-static {v0, v1}, Landroid/os/SystemProperties;->get(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 979
    .local v0, "str":Ljava/lang/String;
    if-eqz v0, :cond_1

    invoke-virtual {v0}, Ljava/lang/String;->isEmpty()Z

    move-result v1

    if-nez v1, :cond_1

    .line 980
    const-string v1, ","

    invoke-virtual {v0, v1}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v1

    .line 981
    .local v1, "pkgName":[Ljava/lang/String;
    new-instance v2, Ljava/util/ArrayList;

    invoke-direct {v2}, Ljava/util/ArrayList;-><init>()V

    .line 982
    .local v2, "policyList":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;"
    array-length v3, v1

    if-lez v3, :cond_1

    .line 983
    array-length v3, v1

    const/4 v4, 0x0

    :goto_0
    if-ge v4, v3, :cond_0

    aget-object v5, v1, v4

    .line 984
    .local v5, "name":Ljava/lang/String;
    invoke-interface {v2, v5}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 983
    .end local v5    # "name":Ljava/lang/String;
    add-int/lit8 v4, v4, 0x1

    goto :goto_0

    .line 986
    :cond_0
    invoke-static {v2}, Lcom/miui/server/turbosched/TurboSchedConfig;->setPolicyList(Ljava/util/List;)V

    .line 987
    iput-object v2, p0, Lcom/miui/server/turbosched/TurboSchedManagerService;->mPolicyListBackup:Ljava/util/List;

    .line 990
    .end local v1    # "pkgName":[Ljava/lang/String;
    .end local v2    # "policyList":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;"
    :cond_1
    return-void
.end method

.method private updateLocalProp()V
    .locals 1

    .line 794
    invoke-static {}, Landroid/os/TurboSchedMonitor;->getInstance()Landroid/os/TurboSchedMonitor;

    move-result-object v0

    invoke-virtual {v0}, Landroid/os/TurboSchedMonitor;->isCoreAppOptimizerEnabled()Z

    move-result v0

    invoke-direct {p0, v0}, Lcom/miui/server/turbosched/TurboSchedManagerService;->enableFrameBoostInKernel(Z)V

    .line 795
    invoke-static {}, Landroid/os/TurboSchedMonitor;->getInstance()Landroid/os/TurboSchedMonitor;

    move-result-object v0

    invoke-virtual {v0}, Landroid/os/TurboSchedMonitor;->isCoreAppOptimizerEnabled()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 796
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/miui/server/turbosched/TurboSchedManagerService;->mKernelEnabelUpdate:Z

    .line 798
    :cond_0
    iget-boolean v0, p0, Lcom/miui/server/turbosched/TurboSchedManagerService;->mThermalBreakEnabled:Z

    invoke-direct {p0, v0}, Lcom/miui/server/turbosched/TurboSchedManagerService;->setThermalBreakEnable(Z)V

    .line 799
    invoke-direct {p0}, Lcom/miui/server/turbosched/TurboSchedManagerService;->updateLocalPolicyListProp()V

    .line 800
    return-void
.end method

.method private writeTidsToPath(Ljava/lang/String;[I)Z
    .locals 5
    .param p1, "path"    # Ljava/lang/String;
    .param p2, "tids"    # [I

    .line 1998
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    .line 1999
    .local v0, "sb":Ljava/lang/StringBuilder;
    array-length v1, p2

    const/4 v2, 0x0

    :goto_0
    if-ge v2, v1, :cond_0

    aget v3, p2, v2

    .line 2000
    .local v3, "i":I
    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    .line 2001
    const-string v4, ":"

    invoke-virtual {v0, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1999
    .end local v3    # "i":I
    add-int/lit8 v2, v2, 0x1

    goto :goto_0

    .line 2003
    :cond_0
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->length()I

    move-result v1

    if-lez v1, :cond_1

    .line 2004
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->length()I

    move-result v1

    add-int/lit8 v1, v1, -0x1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->deleteCharAt(I)Ljava/lang/StringBuilder;

    .line 2006
    :cond_1
    const-string v1, "M-Tids"

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-direct {p0, v1, p1, v2}, Lcom/miui/server/turbosched/TurboSchedManagerService;->writeTurboSchedNode(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Z

    move-result v1

    return v1
.end method

.method private writeTurboSchedNode(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Z
    .locals 6
    .param p1, "logTag"    # Ljava/lang/String;
    .param p2, "path"    # Ljava/lang/String;
    .param p3, "value"    # Ljava/lang/String;

    .line 536
    new-instance v0, Ljava/io/File;

    invoke-direct {v0, p2}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 537
    .local v0, "file":Ljava/io/File;
    invoke-virtual {v0}, Ljava/io/File;->exists()Z

    move-result v1

    const/4 v2, 0x0

    if-nez v1, :cond_0

    .line 538
    return v2

    .line 540
    :cond_0
    :try_start_0
    new-instance v1, Ljava/io/PrintWriter;

    invoke-direct {v1, p2}, Ljava/io/PrintWriter;-><init>(Ljava/lang/String;)V
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    .line 541
    .local v1, "writer":Ljava/io/PrintWriter;
    :try_start_1
    invoke-virtual {v1, p3}, Ljava/io/PrintWriter;->write(Ljava/lang/String;)V

    .line 542
    iget-object v3, p0, Lcom/miui/server/turbosched/TurboSchedManagerService;->mHistoryLog:Landroid/util/LocalLog;

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "[ "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, " ] write message : "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, p3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Landroid/util/LocalLog;->log(Ljava/lang/String;)V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 543
    nop

    .line 544
    :try_start_2
    invoke-virtual {v1}, Ljava/io/PrintWriter;->close()V
    :try_end_2
    .catch Ljava/io/IOException; {:try_start_2 .. :try_end_2} :catch_0

    .line 543
    const/4 v2, 0x1

    return v2

    .line 540
    :catchall_0
    move-exception v3

    :try_start_3
    invoke-virtual {v1}, Ljava/io/PrintWriter;->close()V
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    goto :goto_0

    :catchall_1
    move-exception v4

    :try_start_4
    invoke-virtual {v3, v4}, Ljava/lang/Throwable;->addSuppressed(Ljava/lang/Throwable;)V

    .end local v0    # "file":Ljava/io/File;
    .end local p0    # "this":Lcom/miui/server/turbosched/TurboSchedManagerService;
    .end local p1    # "logTag":Ljava/lang/String;
    .end local p2    # "path":Ljava/lang/String;
    .end local p3    # "value":Ljava/lang/String;
    :goto_0
    throw v3
    :try_end_4
    .catch Ljava/io/IOException; {:try_start_4 .. :try_end_4} :catch_0

    .line 544
    .end local v1    # "writer":Ljava/io/PrintWriter;
    .restart local v0    # "file":Ljava/io/File;
    .restart local p0    # "this":Lcom/miui/server/turbosched/TurboSchedManagerService;
    .restart local p1    # "logTag":Ljava/lang/String;
    .restart local p2    # "path":Ljava/lang/String;
    .restart local p3    # "value":Ljava/lang/String;
    :catch_0
    move-exception v1

    .line 545
    .local v1, "e":Ljava/io/IOException;
    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "Failed to write path : "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, "  value : "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, p3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    const-string v4, "TurboSchedManagerService"

    invoke-static {v4, v3, v1}, Landroid/util/Slog;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    .line 546
    return v2
.end method


# virtual methods
.method public breakThermlimit(IJ)V
    .locals 7
    .param p1, "boost"    # I
    .param p2, "time"    # J

    .line 1395
    invoke-static {}, Landroid/os/TurboSchedMonitor;->getInstance()Landroid/os/TurboSchedMonitor;

    move-result-object v0

    invoke-virtual {v0}, Landroid/os/TurboSchedMonitor;->isCoreAppOptimizerEnabled()Z

    move-result v0

    if-eqz v0, :cond_5

    iget-boolean v0, p0, Lcom/miui/server/turbosched/TurboSchedManagerService;->mThermalBreakEnabled:Z

    if-nez v0, :cond_0

    goto/16 :goto_1

    .line 1398
    :cond_0
    const-string v0, "breakThermlimit"

    invoke-static {p1}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v1

    invoke-direct {p0, v0, v1}, Lcom/miui/server/turbosched/TurboSchedManagerService;->sendTraceBeginMsg(Ljava/lang/String;Ljava/lang/String;)V

    .line 1399
    const/4 v0, 0x0

    .line 1400
    .local v0, "isSuccess":Z
    const-string v1, ""

    .line 1402
    .local v1, "str":Ljava/lang/String;
    invoke-static {}, Landroid/os/TurboSchedMonitor;->getInstance()Landroid/os/TurboSchedMonitor;

    move-result-object v2

    invoke-virtual {v2}, Landroid/os/TurboSchedMonitor;->isDebugMode()Z

    move-result v2

    const-string v3, "TurboSchedManagerService"

    if-eqz v2, :cond_1

    .line 1403
    const-string v2, "breakThermlimit, begin"

    invoke-static {v3, v2}, Landroid/util/Slog;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 1405
    :cond_1
    if-nez p1, :cond_2

    .line 1406
    const-string v1, "boost:0"

    goto :goto_0

    .line 1408
    :cond_2
    const-string v2, "boost:1"

    .line 1409
    .local v2, "strBoost":Ljava/lang/String;
    invoke-direct {p0}, Lcom/miui/server/turbosched/TurboSchedManagerService;->getThermalBreakString()Ljava/lang/String;

    move-result-object v4

    .line 1411
    .local v4, "strThermalBreak":Ljava/lang/String;
    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    .line 1412
    .local v5, "sb":Ljava/lang/StringBuilder;
    invoke-virtual {v5, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1413
    invoke-virtual {v5, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1414
    const-string v6, " time:"

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1415
    invoke-virtual {v5, p2, p3}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    .line 1416
    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    .line 1418
    .end local v2    # "strBoost":Ljava/lang/String;
    .end local v4    # "strThermalBreak":Ljava/lang/String;
    .end local v5    # "sb":Ljava/lang/StringBuilder;
    :goto_0
    invoke-static {}, Landroid/os/TurboSchedMonitor;->getInstance()Landroid/os/TurboSchedMonitor;

    move-result-object v2

    invoke-virtual {v2}, Landroid/os/TurboSchedMonitor;->isDebugMode()Z

    move-result v2

    if-eqz v2, :cond_3

    .line 1419
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "breakThermlimit, str:"

    invoke-virtual {v2, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v3, v2}, Landroid/util/Slog;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 1420
    :cond_3
    const-string v2, "BT-Limit"

    const-string v4, "/sys/class/thermal/thermal_message/boost"

    invoke-direct {p0, v2, v4, v1}, Lcom/miui/server/turbosched/TurboSchedManagerService;->writeTurboSchedNode(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Z

    move-result v0

    .line 1421
    invoke-direct {p0}, Lcom/miui/server/turbosched/TurboSchedManagerService;->sendTraceEndMsg()V

    .line 1422
    invoke-static {}, Landroid/os/TurboSchedMonitor;->getInstance()Landroid/os/TurboSchedMonitor;

    move-result-object v2

    invoke-virtual {v2}, Landroid/os/TurboSchedMonitor;->isDebugMode()Z

    move-result v2

    if-eqz v2, :cond_4

    .line 1423
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "breakThermlimit, isSuccess:"

    invoke-virtual {v2, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v4, ", FluencyOptimizer"

    invoke-virtual {v2, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v3, v2}, Landroid/util/Slog;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 1424
    :cond_4
    return-void

    .line 1396
    .end local v0    # "isSuccess":Z
    .end local v1    # "str":Ljava/lang/String;
    :cond_5
    :goto_1
    return-void
.end method

.method public checkBoostPermission()I
    .locals 8

    .line 1329
    invoke-static {}, Landroid/os/Binder;->getCallingUid()I

    move-result v0

    .line 1330
    .local v0, "uid":I
    invoke-static {}, Landroid/os/Binder;->getCallingPid()I

    move-result v1

    .line 1331
    .local v1, "pid":I
    invoke-direct {p0, v1}, Lcom/miui/server/turbosched/TurboSchedManagerService;->getProcessNameByPid(I)Ljava/lang/String;

    move-result-object v2

    .line 1332
    .local v2, "str":Ljava/lang/String;
    const/4 v3, -0x1

    .line 1334
    .local v3, "ret":I
    iget-wide v4, p0, Lcom/miui/server/turbosched/TurboSchedManagerService;->mFgCoreUid:J

    const-wide/16 v6, -0x1

    cmp-long v4, v4, v6

    if-eqz v4, :cond_1

    .line 1335
    if-eqz v2, :cond_0

    invoke-static {}, Landroid/os/TurboSchedMonitor;->getInstance()Landroid/os/TurboSchedMonitor;

    move-result-object v4

    invoke-virtual {v4, v2}, Landroid/os/TurboSchedMonitor;->isCoreApp(Ljava/lang/String;)Z

    move-result v4

    if-eqz v4, :cond_0

    int-to-long v4, v0

    iget-wide v6, p0, Lcom/miui/server/turbosched/TurboSchedManagerService;->mFgCoreUid:J

    cmp-long v4, v4, v6

    if-nez v4, :cond_0

    .line 1336
    const/4 v3, 0x1

    goto :goto_0

    .line 1338
    :cond_0
    const/4 v3, 0x0

    .line 1342
    :cond_1
    :goto_0
    invoke-static {}, Landroid/os/TurboSchedMonitor;->getInstance()Landroid/os/TurboSchedMonitor;

    move-result-object v4

    invoke-virtual {v4}, Landroid/os/TurboSchedMonitor;->isDebugMode()Z

    move-result v4

    if-eqz v4, :cond_2

    .line 1343
    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "checkBoostPermission, uid:"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, ", pid:"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, ", str:"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, ", mFgCoreUid:"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    iget-wide v5, p0, Lcom/miui/server/turbosched/TurboSchedManagerService;->mFgCoreUid:J

    invoke-virtual {v4, v5, v6}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, ", ret:"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    const-string v5, "TurboSchedManagerService"

    invoke-static {v5, v4}, Landroid/util/Slog;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 1345
    :cond_2
    return v3
.end method

.method public checkPackagePermission(Ljava/lang/String;)Z
    .locals 1
    .param p1, "packageName"    # Ljava/lang/String;

    .line 1350
    if-eqz p1, :cond_0

    invoke-static {}, Landroid/os/TurboSchedMonitor;->getInstance()Landroid/os/TurboSchedMonitor;

    move-result-object v0

    invoke-virtual {v0, p1}, Landroid/os/TurboSchedMonitor;->isCoreApp(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1351
    const/4 v0, 0x1

    return v0

    .line 1353
    :cond_0
    const/4 v0, 0x0

    return v0
.end method

.method protected dump(Ljava/io/FileDescriptor;Ljava/io/PrintWriter;[Ljava/lang/String;)V
    .locals 2
    .param p1, "fd"    # Ljava/io/FileDescriptor;
    .param p2, "pw"    # Ljava/io/PrintWriter;
    .param p3, "args"    # [Ljava/lang/String;

    .line 1552
    iget-object v0, p0, Lcom/miui/server/turbosched/TurboSchedManagerService;->mContext:Landroid/content/Context;

    const-string v1, "TurboSchedManagerService"

    invoke-static {v0, v1, p2}, Lcom/android/internal/util/DumpUtils;->checkDumpPermission(Landroid/content/Context;Ljava/lang/String;Ljava/io/PrintWriter;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 1553
    return-void

    .line 1555
    :cond_0
    invoke-direct {p0, p1, p2, p3}, Lcom/miui/server/turbosched/TurboSchedManagerService;->parseDumpCommand(Ljava/io/FileDescriptor;Ljava/io/PrintWriter;[Ljava/lang/String;)Z

    move-result v0

    .line 1556
    .local v0, "handled":Z
    if-eqz v0, :cond_1

    .line 1557
    return-void

    .line 1560
    :cond_1
    invoke-direct {p0, p2}, Lcom/miui/server/turbosched/TurboSchedManagerService;->dumpConfig(Ljava/io/PrintWriter;)V

    .line 1562
    const-string v1, "-----------------------history----------------------------"

    invoke-virtual {p2, v1}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V

    .line 1563
    iget-object v1, p0, Lcom/miui/server/turbosched/TurboSchedManagerService;->mHistoryLog:Landroid/util/LocalLog;

    invoke-virtual {v1, p2}, Landroid/util/LocalLog;->dump(Ljava/io/PrintWriter;)V

    .line 1564
    return-void
.end method

.method protected enableTurboSchedV2(Z)V
    .locals 2
    .param p1, "enable"    # Z

    .line 1993
    iput-boolean p1, p0, Lcom/miui/server/turbosched/TurboSchedManagerService;->mTurboEnabledV2:Z

    .line 1994
    const-string v0, "persist.sys.turbosched.enable_v2"

    invoke-static {p1}, Ljava/lang/Boolean;->toString(Z)Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/os/SystemProperties;->set(Ljava/lang/String;Ljava/lang/String;)V

    .line 1995
    return-void
.end method

.method public frameTurboMarkScene(Ljava/lang/String;Z)I
    .locals 6
    .param p1, "sceneId"    # Ljava/lang/String;
    .param p2, "start"    # Z

    .line 1169
    invoke-direct {p0}, Lcom/miui/server/turbosched/TurboSchedManagerService;->isTurboEnabled()Z

    move-result v0

    const-string v1, ",start: "

    if-eqz v0, :cond_2

    invoke-static {}, Lcom/miui/server/turbosched/TurboSchedConfig;->getPolicyList()Ljava/util/List;

    move-result-object v0

    const-string v2, "frame_turbo"

    invoke-interface {v0, v2}, Ljava/util/List;->contains(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    goto :goto_0

    .line 1174
    :cond_0
    invoke-static {}, Landroid/os/Binder;->getCallingUid()I

    move-result v0

    .line 1175
    .local v0, "uid":I
    iget-object v2, p0, Lcom/miui/server/turbosched/TurboSchedManagerService;->mContext:Landroid/content/Context;

    invoke-virtual {v2}, Landroid/content/Context;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v2

    invoke-virtual {v2, v0}, Landroid/content/pm/PackageManager;->getNameForUid(I)Ljava/lang/String;

    move-result-object v2

    .line 1177
    .local v2, "packageName":Ljava/lang/String;
    invoke-direct {p0, v2}, Lcom/miui/server/turbosched/TurboSchedManagerService;->isForeground(Ljava/lang/String;)Z

    move-result v3

    if-nez v3, :cond_1

    .line 1178
    iget-object v3, p0, Lcom/miui/server/turbosched/TurboSchedManagerService;->mHistoryLog:Landroid/util/LocalLog;

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "TS [MARK] : failed, R: not foreground, sceneId: "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p2}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v3, v1}, Landroid/util/LocalLog;->log(Ljava/lang/String;)V

    .line 1179
    const/4 v1, -0x6

    return v1

    .line 1181
    :cond_1
    iget-object v1, p0, Lcom/miui/server/turbosched/TurboSchedManagerService;->mFrameTurboAction:Lcom/miui/server/turbosched/FrameTurboAction;

    invoke-virtual {v1, v2, p1, p2}, Lcom/miui/server/turbosched/FrameTurboAction;->markScene(Ljava/lang/String;Ljava/lang/String;Z)I

    move-result v1

    return v1

    .line 1170
    .end local v0    # "uid":I
    .end local v2    # "packageName":Ljava/lang/String;
    :cond_2
    :goto_0
    iget-object v0, p0, Lcom/miui/server/turbosched/TurboSchedManagerService;->mHistoryLog:Landroid/util/LocalLog;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "TS [MARK] : failed, R: not enable, sceneId: "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p2}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/util/LocalLog;->log(Ljava/lang/String;)V

    .line 1171
    const/4 v0, -0x1

    return v0
.end method

.method public frameTurboRegisterSceneCallback(Lmiui/turbosched/ITurboSchedManager$IFrameTurboSceneCallback;)V
    .locals 2
    .param p1, "callback"    # Lmiui/turbosched/ITurboSchedManager$IFrameTurboSceneCallback;

    .line 1206
    invoke-direct {p0}, Lcom/miui/server/turbosched/TurboSchedManagerService;->isTurboEnabled()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-static {}, Lcom/miui/server/turbosched/TurboSchedConfig;->getPolicyList()Ljava/util/List;

    move-result-object v0

    const-string v1, "frame_turbo"

    invoke-interface {v0, v1}, Ljava/util/List;->contains(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    goto :goto_0

    .line 1210
    :cond_0
    iget-object v0, p0, Lcom/miui/server/turbosched/TurboSchedManagerService;->mFrameTurboAction:Lcom/miui/server/turbosched/FrameTurboAction;

    invoke-virtual {v0, p1}, Lcom/miui/server/turbosched/FrameTurboAction;->registerSceneCallback(Lmiui/turbosched/ITurboSchedManager$IFrameTurboSceneCallback;)V

    .line 1211
    return-void

    .line 1207
    :cond_1
    :goto_0
    iget-object v0, p0, Lcom/miui/server/turbosched/TurboSchedManagerService;->mHistoryLog:Landroid/util/LocalLog;

    const-string v1, "TS [FRAME] : failed, R: not enable"

    invoke-virtual {v0, v1}, Landroid/util/LocalLog;->log(Ljava/lang/String;)V

    .line 1208
    return-void
.end method

.method public frameTurboTrigger(ZLjava/util/List;Ljava/util/List;)I
    .locals 5
    .param p1, "turbo"    # Z
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(Z",
            "Ljava/util/List<",
            "Ljava/lang/String;",
            ">;",
            "Ljava/util/List<",
            "Ljava/lang/String;",
            ">;)I"
        }
    .end annotation

    .line 1186
    .local p2, "uiThreads":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;"
    .local p3, "scenes":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;"
    invoke-direct {p0}, Lcom/miui/server/turbosched/TurboSchedManagerService;->isTurboEnabled()Z

    move-result v0

    if-eqz v0, :cond_4

    invoke-static {}, Lcom/miui/server/turbosched/TurboSchedConfig;->getPolicyList()Ljava/util/List;

    move-result-object v0

    const-string v1, "frame_turbo"

    invoke-interface {v0, v1}, Ljava/util/List;->contains(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    goto :goto_0

    .line 1190
    :cond_0
    invoke-static {}, Landroid/os/Binder;->getCallingPid()I

    move-result v0

    .line 1191
    .local v0, "pid":I
    if-eqz p2, :cond_1

    invoke-interface {p2}, Ljava/util/List;->size()I

    move-result v1

    if-nez v1, :cond_2

    .line 1192
    :cond_1
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    move-object p2, v1

    .line 1193
    invoke-static {v0}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v1

    invoke-interface {p2, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 1195
    :cond_2
    invoke-static {}, Landroid/os/Binder;->getCallingUid()I

    move-result v1

    .line 1196
    .local v1, "uid":I
    iget-object v2, p0, Lcom/miui/server/turbosched/TurboSchedManagerService;->mContext:Landroid/content/Context;

    invoke-virtual {v2}, Landroid/content/Context;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v2

    invoke-virtual {v2, v1}, Landroid/content/pm/PackageManager;->getNameForUid(I)Ljava/lang/String;

    move-result-object v2

    .line 1197
    .local v2, "packageName":Ljava/lang/String;
    invoke-direct {p0, v2}, Lcom/miui/server/turbosched/TurboSchedManagerService;->isForeground(Ljava/lang/String;)Z

    move-result v3

    if-nez v3, :cond_3

    .line 1198
    iget-object v3, p0, Lcom/miui/server/turbosched/TurboSchedManagerService;->mHistoryLog:Landroid/util/LocalLog;

    const-string v4, "TS [FRAME] : failed, R: not foreground"

    invoke-virtual {v3, v4}, Landroid/util/LocalLog;->log(Ljava/lang/String;)V

    .line 1199
    const/4 v3, -0x6

    return v3

    .line 1201
    :cond_3
    iget-object v3, p0, Lcom/miui/server/turbosched/TurboSchedManagerService;->mFrameTurboAction:Lcom/miui/server/turbosched/FrameTurboAction;

    invoke-virtual {v3, v2, p1, p2, p3}, Lcom/miui/server/turbosched/FrameTurboAction;->triggerFrameTurbo(Ljava/lang/String;ZLjava/util/List;Ljava/util/List;)I

    move-result v3

    return v3

    .line 1187
    .end local v0    # "pid":I
    .end local v1    # "uid":I
    .end local v2    # "packageName":Ljava/lang/String;
    :cond_4
    :goto_0
    iget-object v0, p0, Lcom/miui/server/turbosched/TurboSchedManagerService;->mHistoryLog:Landroid/util/LocalLog;

    const-string v1, "TS [FRAME] : failed, R: not enable"

    invoke-virtual {v0, v1}, Landroid/util/LocalLog;->log(Ljava/lang/String;)V

    .line 1188
    const/4 v0, -0x1

    return v0
.end method

.method public frameTurboUnregisterSceneCallback(Lmiui/turbosched/ITurboSchedManager$IFrameTurboSceneCallback;)V
    .locals 2
    .param p1, "callback"    # Lmiui/turbosched/ITurboSchedManager$IFrameTurboSceneCallback;

    .line 1215
    invoke-direct {p0}, Lcom/miui/server/turbosched/TurboSchedManagerService;->isTurboEnabled()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-static {}, Lcom/miui/server/turbosched/TurboSchedConfig;->getPolicyList()Ljava/util/List;

    move-result-object v0

    const-string v1, "frame_turbo"

    invoke-interface {v0, v1}, Ljava/util/List;->contains(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    goto :goto_0

    .line 1219
    :cond_0
    iget-object v0, p0, Lcom/miui/server/turbosched/TurboSchedManagerService;->mFrameTurboAction:Lcom/miui/server/turbosched/FrameTurboAction;

    invoke-virtual {v0, p1}, Lcom/miui/server/turbosched/FrameTurboAction;->unregisterSceneCallback(Lmiui/turbosched/ITurboSchedManager$IFrameTurboSceneCallback;)V

    .line 1220
    return-void

    .line 1216
    :cond_1
    :goto_0
    iget-object v0, p0, Lcom/miui/server/turbosched/TurboSchedManagerService;->mHistoryLog:Landroid/util/LocalLog;

    const-string v1, "TS [FRAME] : failed, R: not enable"

    invoke-virtual {v0, v1}, Landroid/util/LocalLog;->log(Ljava/lang/String;)V

    .line 1217
    return-void
.end method

.method public getApiVersion()Ljava/lang/String;
    .locals 1

    .line 1102
    const-string/jumbo v0, "v4.0.0"

    return-object v0
.end method

.method public getCallerAllowList()Ljava/util/List;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .line 1372
    iget-object v0, p0, Lcom/miui/server/turbosched/TurboSchedManagerService;->mCallerAllowList:Ljava/util/List;

    return-object v0
.end method

.method public getCoreAppList()Ljava/util/List;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .line 1366
    iget-object v0, p0, Lcom/miui/server/turbosched/TurboSchedManagerService;->mCoreAppsPackageNameList:Ljava/util/List;

    return-object v0
.end method

.method public getPolicyList()Ljava/util/List;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .line 1107
    invoke-static {}, Lcom/miui/server/turbosched/TurboSchedConfig;->getPolicyList()Ljava/util/List;

    move-result-object v0

    return-object v0
.end method

.method public isApiEnable()Z
    .locals 1

    .line 1097
    invoke-direct {p0}, Lcom/miui/server/turbosched/TurboSchedManagerService;->isTurboEnabled()Z

    move-result v0

    return v0
.end method

.method public isCoreApp()Z
    .locals 3

    .line 1359
    invoke-static {}, Landroid/os/Binder;->getCallingPid()I

    move-result v0

    .line 1360
    .local v0, "pid":I
    invoke-direct {p0, v0}, Lcom/miui/server/turbosched/TurboSchedManagerService;->getProcessNameByPid(I)Ljava/lang/String;

    move-result-object v1

    .line 1361
    .local v1, "processName":Ljava/lang/String;
    invoke-static {}, Landroid/os/TurboSchedMonitor;->getInstance()Landroid/os/TurboSchedMonitor;

    move-result-object v2

    invoke-virtual {v2, v1}, Landroid/os/TurboSchedMonitor;->isCoreApp(Ljava/lang/String;)Z

    move-result v2

    return v2
.end method

.method public isFileAccess()Z
    .locals 2

    .line 1516
    new-instance v0, Ljava/io/File;

    const-string v1, "/sys/module/metis/parameters/mi_viptask"

    invoke-direct {v0, v1}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 1517
    .local v0, "file":Ljava/io/File;
    invoke-virtual {v0}, Ljava/io/File;->exists()Z

    move-result v1

    return v1
.end method

.method public notifyOnScroll(Z)V
    .locals 0
    .param p1, "isOnScroll"    # Z

    .line 1302
    iput-boolean p1, p0, Lcom/miui/server/turbosched/TurboSchedManagerService;->mIsOnScroll:Z

    .line 1303
    return-void
.end method

.method public onCoreAppFirstActivityStart(Ljava/lang/String;)V
    .locals 5
    .param p1, "processName"    # Ljava/lang/String;

    .line 2188
    iget-boolean v0, p0, Lcom/miui/server/turbosched/TurboSchedManagerService;->mSupportGaea:Z

    const-string v1, "TurboSchedManagerService"

    const-string v2, "1"

    if-eqz v0, :cond_0

    iget-boolean v0, p0, Lcom/miui/server/turbosched/TurboSchedManagerService;->mGaeaEnable:Z

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/miui/server/turbosched/TurboSchedManagerService;->mCoreAppsPackageNameList:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->contains(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2189
    iput-object p1, p0, Lcom/miui/server/turbosched/TurboSchedManagerService;->enableGaeaAppProcessName:Ljava/lang/String;

    .line 2190
    const-string v0, "GAEA"

    const-string v3, "/sys/module/gaea/parameters/mi_gaea_enable"

    invoke-direct {p0, v0, v3, v2}, Lcom/miui/server/turbosched/TurboSchedManagerService;->writeTurboSchedNode(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Z

    move-result v0

    .line 2191
    .local v0, "isSuccess":Z
    invoke-static {}, Landroid/os/TurboSchedMonitor;->getInstance()Landroid/os/TurboSchedMonitor;

    move-result-object v3

    invoke-virtual {v3}, Landroid/os/TurboSchedMonitor;->isDebugMode()Z

    move-result v3

    if-eqz v3, :cond_0

    .line 2192
    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v4, "write gaea path node 1: "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v1, v3}, Landroid/util/Slog;->w(Ljava/lang/String;Ljava/lang/String;)I

    .line 2195
    .end local v0    # "isSuccess":Z
    :cond_0
    iget-boolean v0, p0, Lcom/miui/server/turbosched/TurboSchedManagerService;->mLinkEnable:Z

    if-eqz v0, :cond_1

    if-eqz p1, :cond_1

    iget-object v0, p0, Lcom/miui/server/turbosched/TurboSchedManagerService;->mAppsLinkVipList:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->contains(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 2196
    iput-object p1, p0, Lcom/miui/server/turbosched/TurboSchedManagerService;->enableLinkVipAppProcessName:Ljava/lang/String;

    .line 2197
    const-string v0, "LINK"

    const-string v3, "/sys/module/metis/parameters/vip_link_enable"

    invoke-direct {p0, v0, v3, v2}, Lcom/miui/server/turbosched/TurboSchedManagerService;->writeTurboSchedNode(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Z

    move-result v0

    .line 2198
    .restart local v0    # "isSuccess":Z
    invoke-static {}, Landroid/os/TurboSchedMonitor;->getInstance()Landroid/os/TurboSchedMonitor;

    move-result-object v2

    invoke-virtual {v2}, Landroid/os/TurboSchedMonitor;->isDebugMode()Z

    move-result v2

    if-eqz v2, :cond_1

    if-nez v0, :cond_1

    .line 2199
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "LinkVip, write linkvip path node 1:"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Slog;->w(Ljava/lang/String;Ljava/lang/String;)I

    .line 2201
    .end local v0    # "isSuccess":Z
    :cond_1
    return-void
.end method

.method public onCoreAppLastActivityStop(Ljava/lang/String;)V
    .locals 5
    .param p1, "processName"    # Ljava/lang/String;

    .line 2205
    iget-boolean v0, p0, Lcom/miui/server/turbosched/TurboSchedManagerService;->mSupportGaea:Z

    const-string v1, "TurboSchedManagerService"

    const-string v2, "0"

    if-eqz v0, :cond_0

    iget-boolean v0, p0, Lcom/miui/server/turbosched/TurboSchedManagerService;->mGaeaEnable:Z

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/miui/server/turbosched/TurboSchedManagerService;->mCoreAppsPackageNameList:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->contains(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2206
    iget-object v0, p0, Lcom/miui/server/turbosched/TurboSchedManagerService;->enableGaeaAppProcessName:Ljava/lang/String;

    if-eqz v0, :cond_0

    invoke-virtual {v0, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2207
    const-string v0, "GAEA"

    const-string v3, "/sys/module/gaea/parameters/mi_gaea_enable"

    invoke-direct {p0, v0, v3, v2}, Lcom/miui/server/turbosched/TurboSchedManagerService;->writeTurboSchedNode(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Z

    move-result v0

    .line 2208
    .local v0, "isSuccess":Z
    invoke-static {}, Landroid/os/TurboSchedMonitor;->getInstance()Landroid/os/TurboSchedMonitor;

    move-result-object v3

    invoke-virtual {v3}, Landroid/os/TurboSchedMonitor;->isDebugMode()Z

    move-result v3

    if-eqz v3, :cond_0

    .line 2209
    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v4, "write gaea path node 0: "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v1, v3}, Landroid/util/Slog;->w(Ljava/lang/String;Ljava/lang/String;)I

    .line 2213
    .end local v0    # "isSuccess":Z
    :cond_0
    iget-boolean v0, p0, Lcom/miui/server/turbosched/TurboSchedManagerService;->mLinkEnable:Z

    if-eqz v0, :cond_1

    if-eqz p1, :cond_1

    iget-object v0, p0, Lcom/miui/server/turbosched/TurboSchedManagerService;->mAppsLinkVipList:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->contains(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 2214
    iget-object v0, p0, Lcom/miui/server/turbosched/TurboSchedManagerService;->enableLinkVipAppProcessName:Ljava/lang/String;

    if-eqz v0, :cond_1

    invoke-virtual {v0, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 2215
    const-string v0, "LINK"

    const-string v3, "/sys/module/metis/parameters/vip_link_enable"

    invoke-direct {p0, v0, v3, v2}, Lcom/miui/server/turbosched/TurboSchedManagerService;->writeTurboSchedNode(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Z

    move-result v0

    .line 2216
    .restart local v0    # "isSuccess":Z
    invoke-static {}, Landroid/os/TurboSchedMonitor;->getInstance()Landroid/os/TurboSchedMonitor;

    move-result-object v2

    invoke-virtual {v2}, Landroid/os/TurboSchedMonitor;->isDebugMode()Z

    move-result v2

    if-eqz v2, :cond_1

    if-nez v0, :cond_1

    .line 2217
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "LinkVip, write linkvip path node 0:"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Slog;->w(Ljava/lang/String;Ljava/lang/String;)I

    .line 2221
    .end local v0    # "isSuccess":Z
    :cond_1
    return-void
.end method

.method public onFocusedWindowChangeLocked(Ljava/lang/String;I)V
    .locals 1
    .param p1, "focus"    # Ljava/lang/String;
    .param p2, "type"    # I

    .line 1288
    invoke-static {}, Lcom/miui/server/turbosched/TurboSchedSceneManager;->getInstance()Lcom/miui/server/turbosched/TurboSchedSceneManager;

    move-result-object v0

    invoke-virtual {v0, p1, p2}, Lcom/miui/server/turbosched/TurboSchedSceneManager;->onFocusedWindowChangeLocked(Ljava/lang/String;I)V

    .line 1289
    return-void
.end method

.method public registerStateChangeCallback(Ljava/lang/String;Lmiui/turbosched/ITurboSchedManager$ITurboSchedStateChangeCallback;)V
    .locals 1
    .param p1, "pkgName"    # Ljava/lang/String;
    .param p2, "cb"    # Lmiui/turbosched/ITurboSchedManager$ITurboSchedStateChangeCallback;

    .line 1278
    invoke-static {}, Lcom/miui/server/turbosched/TurboSchedSceneManager;->getInstance()Lcom/miui/server/turbosched/TurboSchedSceneManager;

    move-result-object v0

    invoke-virtual {v0, p1, p2}, Lcom/miui/server/turbosched/TurboSchedSceneManager;->registerStateChangeCallback(Ljava/lang/String;Lmiui/turbosched/ITurboSchedManager$ITurboSchedStateChangeCallback;)V

    .line 1279
    return-void
.end method

.method public setForegroundProcessName(Ljava/lang/String;)V
    .locals 0
    .param p1, "processName"    # Ljava/lang/String;

    .line 2226
    iput-object p1, p0, Lcom/miui/server/turbosched/TurboSchedManagerService;->mProcessName:Ljava/lang/String;

    .line 2227
    return-void
.end method

.method public setSceneAction(Ljava/lang/String;Ljava/lang/String;I)V
    .locals 1
    .param p1, "scene"    # Ljava/lang/String;
    .param p2, "action"    # Ljava/lang/String;
    .param p3, "timeout"    # I

    .line 1428
    invoke-virtual {p2}, Ljava/lang/String;->hashCode()I

    move-result v0

    packed-switch v0, :pswitch_data_0

    :cond_0
    goto :goto_0

    :pswitch_0
    const-string v0, "TS_ACTION_FLING"

    invoke-virtual {p2, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x0

    goto :goto_1

    :goto_0
    const/4 v0, -0x1

    :goto_1
    packed-switch v0, :pswitch_data_1

    goto :goto_2

    .line 1430
    :pswitch_1
    nop

    .line 1435
    :goto_2
    return-void

    nop

    :pswitch_data_0
    .packed-switch 0x67d68fb3
        :pswitch_0
    .end packed-switch

    :pswitch_data_1
    .packed-switch 0x0
        :pswitch_1
    .end packed-switch
.end method

.method public setTurboSchedAction([IJ)Z
    .locals 3
    .param p1, "tid"    # [I
    .param p2, "time"    # J

    .line 1113
    invoke-static {}, Landroid/os/Binder;->getCallingPid()I

    move-result v0

    .line 1114
    .local v0, "pid":I
    invoke-static {}, Landroid/os/Binder;->getCallingUid()I

    move-result v1

    .line 1115
    .local v1, "uid":I
    invoke-direct {p0, v0, v1}, Lcom/miui/server/turbosched/TurboSchedManagerService;->checkCallerPermmsion(II)Z

    move-result v2

    if-nez v2, :cond_0

    const/4 v2, 0x0

    return v2

    .line 1118
    :cond_0
    iget-boolean v2, p0, Lcom/miui/server/turbosched/TurboSchedManagerService;->mTurboNodeExist:Z

    if-eqz v2, :cond_1

    .line 1119
    const-string v2, "/sys/module/metis/parameters/mi_viptask"

    invoke-direct {p0, p1, p2, p3, v2}, Lcom/miui/server/turbosched/TurboSchedManagerService;->setTurboSchedActionInternal([IJLjava/lang/String;)Z

    move-result v2

    .local v2, "ret":Z
    goto :goto_0

    .line 1121
    .end local v2    # "ret":Z
    :cond_1
    const-string v2, "/sys/module/migt/parameters/mi_viptask"

    invoke-direct {p0, p1, p2, p3, v2}, Lcom/miui/server/turbosched/TurboSchedManagerService;->setTurboSchedActionInternal([IJLjava/lang/String;)Z

    move-result v2

    .line 1123
    .restart local v2    # "ret":Z
    :goto_0
    return v2
.end method

.method public setTurboSchedActionToLittleCore([IJ)V
    .locals 11
    .param p1, "tids"    # [I
    .param p2, "time"    # J

    .line 1252
    invoke-direct {p0}, Lcom/miui/server/turbosched/TurboSchedManagerService;->isTurboEnabled()Z

    move-result v0

    const-string v1, ",time: "

    if-eqz v0, :cond_4

    invoke-static {}, Lcom/miui/server/turbosched/TurboSchedConfig;->getPolicyList()Ljava/util/List;

    move-result-object v0

    const-string v2, "lc"

    invoke-interface {v0, v2}, Ljava/util/List;->contains(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    goto/16 :goto_2

    .line 1256
    :cond_0
    invoke-direct {p0, v2}, Lcom/miui/server/turbosched/TurboSchedManagerService;->sendTraceBeginMsg(Ljava/lang/String;)V

    .line 1257
    const/4 v0, 0x0

    .line 1259
    .local v0, "success":Z
    const-string v3, "LC"

    const-string v7, "lc"

    move-object v2, p0

    move-object v4, p1

    move-wide v5, p2

    invoke-direct/range {v2 .. v7}, Lcom/miui/server/turbosched/TurboSchedManagerService;->setTurboSchedActionParam(Ljava/lang/String;[IJLjava/lang/String;)Ljava/util/List;

    move-result-object v2

    .line 1260
    .local v2, "tidsList":Ljava/util/List;, "Ljava/util/List<Ljava/lang/Integer;>;"
    if-eqz v2, :cond_3

    invoke-interface {v2}, Ljava/util/List;->isEmpty()Z

    move-result v3

    if-eqz v3, :cond_1

    goto :goto_1

    .line 1264
    :cond_1
    invoke-interface {v2}, Ljava/util/List;->stream()Ljava/util/stream/Stream;

    move-result-object v3

    new-instance v4, Lcom/android/server/audio/AudioServiceStubImpl$$ExternalSyntheticLambda1;

    invoke-direct {v4}, Lcom/android/server/audio/AudioServiceStubImpl$$ExternalSyntheticLambda1;-><init>()V

    invoke-interface {v3, v4}, Ljava/util/stream/Stream;->mapToInt(Ljava/util/function/ToIntFunction;)Ljava/util/stream/IntStream;

    move-result-object v3

    invoke-interface {v3}, Ljava/util/stream/IntStream;->toArray()[I

    move-result-object v3

    .line 1265
    .local v3, "newTids":[I
    const-string v4, "/sys/module/metis/parameters/add_mi_viptask_sched_lit_core"

    invoke-direct {p0, v4, v3}, Lcom/miui/server/turbosched/TurboSchedManagerService;->writeTidsToPath(Ljava/lang/String;[I)Z

    move-result v0

    .line 1267
    if-eqz v0, :cond_2

    .line 1268
    const-string v5, "-LC"

    const-string v9, "/sys/module/metis/parameters/del_mi_viptask_sched_lit_core"

    iget-object v10, p0, Lcom/miui/server/turbosched/TurboSchedManagerService;->mPriorityTidStartTimeMap:Ljava/util/Map;

    move-object v4, p0

    move-object v6, v3

    move-wide v7, p2

    invoke-direct/range {v4 .. v10}, Lcom/miui/server/turbosched/TurboSchedManagerService;->setPolicyTimeoutChecker(Ljava/lang/String;[IJLjava/lang/String;Ljava/util/Map;)V

    .line 1269
    iget-object v4, p0, Lcom/miui/server/turbosched/TurboSchedManagerService;->mHistoryLog:Landroid/util/LocalLog;

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "TS [LC] : success, tids: "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-static {v3}, Ljava/util/Arrays;->toString([I)Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p2, p3}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v4, v1}, Landroid/util/LocalLog;->log(Ljava/lang/String;)V

    goto :goto_0

    .line 1271
    :cond_2
    iget-object v4, p0, Lcom/miui/server/turbosched/TurboSchedManagerService;->mHistoryLog:Landroid/util/LocalLog;

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "TS [LC] : failed, R: write failed, newTids: "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-static {v3}, Ljava/util/Arrays;->toString([I)Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p2, p3}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v4, v1}, Landroid/util/LocalLog;->log(Ljava/lang/String;)V

    .line 1273
    :goto_0
    invoke-direct {p0}, Lcom/miui/server/turbosched/TurboSchedManagerService;->sendTraceEndMsg()V

    .line 1274
    return-void

    .line 1261
    .end local v3    # "newTids":[I
    :cond_3
    :goto_1
    return-void

    .line 1253
    .end local v0    # "success":Z
    .end local v2    # "tidsList":Ljava/util/List;, "Ljava/util/List<Ljava/lang/Integer;>;"
    :cond_4
    :goto_2
    iget-object v0, p0, Lcom/miui/server/turbosched/TurboSchedManagerService;->mHistoryLog:Landroid/util/LocalLog;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "TS [LC] : failed, R: not enable, tids: "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-static {p1}, Ljava/util/Arrays;->toString([I)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p2, p3}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/util/LocalLog;->log(Ljava/lang/String;)V

    .line 1254
    return-void
.end method

.method public setTurboSchedActionWithBoostFrequency([IJ)V
    .locals 11
    .param p1, "tids"    # [I
    .param p2, "time"    # J

    .line 1135
    invoke-direct {p0}, Lcom/miui/server/turbosched/TurboSchedManagerService;->isTurboEnabled()Z

    move-result v0

    const-string v1, ",time: "

    if-eqz v0, :cond_6

    invoke-static {}, Lcom/miui/server/turbosched/TurboSchedConfig;->getPolicyList()Ljava/util/List;

    move-result-object v0

    const-string v2, "bwf"

    invoke-interface {v0, v2}, Ljava/util/List;->contains(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    goto/16 :goto_2

    .line 1140
    :cond_0
    array-length v0, p1

    const/4 v3, 0x1

    if-le v0, v3, :cond_1

    .line 1141
    iget-object v0, p0, Lcom/miui/server/turbosched/TurboSchedManagerService;->mHistoryLog:Landroid/util/LocalLog;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "TS [BWF] : failed, R: tids length must be 1, tids: "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-static {p1}, Ljava/util/Arrays;->toString([I)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p2, p3}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/util/LocalLog;->log(Ljava/lang/String;)V

    .line 1142
    return-void

    .line 1144
    :cond_1
    invoke-direct {p0, v2}, Lcom/miui/server/turbosched/TurboSchedManagerService;->sendTraceBeginMsg(Ljava/lang/String;)V

    .line 1145
    const/4 v0, 0x0

    .line 1146
    .local v0, "success":Z
    const-string v3, "BWF"

    const-string v7, "bwf"

    move-object v2, p0

    move-object v4, p1

    move-wide v5, p2

    invoke-direct/range {v2 .. v7}, Lcom/miui/server/turbosched/TurboSchedManagerService;->setTurboSchedActionParam(Ljava/lang/String;[IJLjava/lang/String;)Ljava/util/List;

    move-result-object v2

    .line 1147
    .local v2, "tidsList":Ljava/util/List;, "Ljava/util/List<Ljava/lang/Integer;>;"
    if-eqz v2, :cond_5

    invoke-interface {v2}, Ljava/util/List;->isEmpty()Z

    move-result v3

    if-eqz v3, :cond_2

    goto/16 :goto_1

    .line 1150
    :cond_2
    invoke-interface {v2}, Ljava/util/List;->stream()Ljava/util/stream/Stream;

    move-result-object v3

    new-instance v4, Lcom/android/server/audio/AudioServiceStubImpl$$ExternalSyntheticLambda1;

    invoke-direct {v4}, Lcom/android/server/audio/AudioServiceStubImpl$$ExternalSyntheticLambda1;-><init>()V

    invoke-interface {v3, v4}, Ljava/util/stream/Stream;->mapToInt(Ljava/util/function/ToIntFunction;)Ljava/util/stream/IntStream;

    move-result-object v3

    invoke-interface {v3}, Ljava/util/stream/IntStream;->toArray()[I

    move-result-object v3

    .line 1151
    .local v3, "newTids":[I
    invoke-virtual {p0, v3, p2, p3}, Lcom/miui/server/turbosched/TurboSchedManagerService;->setTurboSchedAction([IJ)Z

    move-result v0

    .line 1152
    if-eqz v0, :cond_4

    .line 1153
    const-string v4, "/sys/module/metis/parameters/add_mi_viptask_enqueue_boost"

    invoke-direct {p0, v4, v3}, Lcom/miui/server/turbosched/TurboSchedManagerService;->writeTidsToPath(Ljava/lang/String;[I)Z

    move-result v0

    .line 1154
    if-eqz v0, :cond_3

    .line 1155
    const-string v5, "-BWF"

    const-string v9, "/sys/module/metis/parameters/del_mi_viptask_enqueue_boost"

    iget-object v10, p0, Lcom/miui/server/turbosched/TurboSchedManagerService;->mBWFTidStartTimeMap:Ljava/util/Map;

    move-object v4, p0

    move-object v6, v3

    move-wide v7, p2

    invoke-direct/range {v4 .. v10}, Lcom/miui/server/turbosched/TurboSchedManagerService;->setPolicyTimeoutChecker(Ljava/lang/String;[IJLjava/lang/String;Ljava/util/Map;)V

    .line 1156
    iget-object v4, p0, Lcom/miui/server/turbosched/TurboSchedManagerService;->mHistoryLog:Landroid/util/LocalLog;

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "TS [BWF] : success, tids: "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-static {v3}, Ljava/util/Arrays;->toString([I)Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p2, p3}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v4, v1}, Landroid/util/LocalLog;->log(Ljava/lang/String;)V

    goto :goto_0

    .line 1158
    :cond_3
    iget-object v4, p0, Lcom/miui/server/turbosched/TurboSchedManagerService;->mHistoryLog:Landroid/util/LocalLog;

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "TS [BWF] : failed, R: write failed 1, tids: "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-static {v3}, Ljava/util/Arrays;->toString([I)Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p2, p3}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v4, v1}, Landroid/util/LocalLog;->log(Ljava/lang/String;)V

    goto :goto_0

    .line 1161
    :cond_4
    iget-object v4, p0, Lcom/miui/server/turbosched/TurboSchedManagerService;->mHistoryLog:Landroid/util/LocalLog;

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "TS [BWF] : failed, R: write failed 2, tids: "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-static {v3}, Ljava/util/Arrays;->toString([I)Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p2, p3}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v4, v1}, Landroid/util/LocalLog;->log(Ljava/lang/String;)V

    .line 1163
    :goto_0
    invoke-direct {p0}, Lcom/miui/server/turbosched/TurboSchedManagerService;->sendTraceEndMsg()V

    .line 1164
    return-void

    .line 1148
    .end local v3    # "newTids":[I
    :cond_5
    :goto_1
    return-void

    .line 1136
    .end local v0    # "success":Z
    .end local v2    # "tidsList":Ljava/util/List;, "Ljava/util/List<Ljava/lang/Integer;>;"
    :cond_6
    :goto_2
    iget-object v0, p0, Lcom/miui/server/turbosched/TurboSchedManagerService;->mHistoryLog:Landroid/util/LocalLog;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "TS [BWF] : failed, R: not enable, tids: "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-static {p1}, Ljava/util/Arrays;->toString([I)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p2, p3}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/util/LocalLog;->log(Ljava/lang/String;)V

    .line 1137
    return-void
.end method

.method public setTurboSchedActionWithId([IJJI)V
    .locals 5
    .param p1, "tids"    # [I
    .param p2, "time"    # J
    .param p4, "id"    # J
    .param p6, "mode"    # I

    .line 1307
    invoke-direct {p0}, Lcom/miui/server/turbosched/TurboSchedManagerService;->isTurboEnabled()Z

    move-result v0

    if-eqz v0, :cond_3

    invoke-direct {p0, p1}, Lcom/miui/server/turbosched/TurboSchedManagerService;->isFgDrawingFrame([I)Z

    move-result v0

    if-eqz v0, :cond_3

    iget-boolean v0, p0, Lcom/miui/server/turbosched/TurboSchedManagerService;->mIsOnScroll:Z

    if-eqz v0, :cond_3

    invoke-static {}, Landroid/os/TurboSchedMonitor;->getInstance()Landroid/os/TurboSchedMonitor;

    move-result-object v0

    iget-object v1, p0, Lcom/miui/server/turbosched/TurboSchedManagerService;->mProcessName:Ljava/lang/String;

    invoke-virtual {v0, v1}, Landroid/os/TurboSchedMonitor;->isCoreApp(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_3

    .line 1308
    iget-wide v0, p0, Lcom/miui/server/turbosched/TurboSchedManagerService;->mLastVsyncId:J

    cmp-long v0, p4, v0

    if-eqz v0, :cond_3

    .line 1309
    invoke-direct {p0}, Lcom/miui/server/turbosched/TurboSchedManagerService;->getBufferTxCount()I

    move-result v0

    .line 1310
    .local v0, "bufferTx":I
    const-string v1, "TurboSchedManagerService"

    const/4 v2, 0x1

    if-lt v0, v2, :cond_1

    .line 1311
    invoke-static {}, Landroid/os/TurboSchedMonitor;->getInstance()Landroid/os/TurboSchedMonitor;

    move-result-object v2

    invoke-virtual {v2}, Landroid/os/TurboSchedMonitor;->isDebugMode()Z

    move-result v2

    if-eqz v2, :cond_0

    .line 1312
    const-string v2, "bufferTx larger than 2, dont\'t need boost, FluencyOptimizer"

    invoke-static {v1, v2}, Landroid/util/Slog;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 1313
    :cond_0
    return-void

    .line 1316
    :cond_1
    invoke-static {}, Landroid/os/TurboSchedMonitor;->getInstance()Landroid/os/TurboSchedMonitor;

    move-result-object v3

    invoke-virtual {v3}, Landroid/os/TurboSchedMonitor;->isDebugMode()Z

    move-result v3

    if-eqz v3, :cond_2

    .line 1317
    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v4, "setTurboSchedActionWithId, id:"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, p4, p5}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, ", bufferTx:"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, ", FluencyOptimizer"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v1, v3}, Landroid/util/Slog;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 1319
    :cond_2
    iput-wide p4, p0, Lcom/miui/server/turbosched/TurboSchedManagerService;->mLastVsyncId:J

    .line 1321
    invoke-virtual {p0, v2, p2, p3}, Lcom/miui/server/turbosched/TurboSchedManagerService;->breakThermlimit(IJ)V

    .line 1322
    iget-object v1, p0, Lcom/miui/server/turbosched/TurboSchedManagerService;->mBoostDuration:Ljava/util/List;

    const/4 v2, 0x0

    invoke-interface {v1, v2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/Long;

    invoke-virtual {v1}, Ljava/lang/Long;->intValue()I

    move-result v1

    invoke-direct {p0, v1}, Lcom/miui/server/turbosched/TurboSchedManagerService;->metisFrameBoost(I)V

    .line 1325
    .end local v0    # "bufferTx":I
    :cond_3
    return-void
.end method

.method public setTurboSchedActionWithPriority([IJ)V
    .locals 11
    .param p1, "tids"    # [I
    .param p2, "time"    # J

    .line 1225
    invoke-direct {p0}, Lcom/miui/server/turbosched/TurboSchedManagerService;->isTurboEnabled()Z

    move-result v0

    const-string v1, ",time: "

    if-eqz v0, :cond_4

    invoke-static {}, Lcom/miui/server/turbosched/TurboSchedConfig;->getPolicyList()Ljava/util/List;

    move-result-object v0

    const-string v2, "priority"

    invoke-interface {v0, v2}, Ljava/util/List;->contains(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    goto/16 :goto_2

    .line 1229
    :cond_0
    invoke-direct {p0, v2}, Lcom/miui/server/turbosched/TurboSchedManagerService;->sendTraceBeginMsg(Ljava/lang/String;)V

    .line 1230
    const/4 v0, 0x0

    .line 1232
    .local v0, "success":Z
    const-string v3, "PRIORITY"

    const-string v7, "priority"

    move-object v2, p0

    move-object v4, p1

    move-wide v5, p2

    invoke-direct/range {v2 .. v7}, Lcom/miui/server/turbosched/TurboSchedManagerService;->setTurboSchedActionParam(Ljava/lang/String;[IJLjava/lang/String;)Ljava/util/List;

    move-result-object v2

    .line 1233
    .local v2, "tidsList":Ljava/util/List;, "Ljava/util/List<Ljava/lang/Integer;>;"
    if-eqz v2, :cond_3

    invoke-interface {v2}, Ljava/util/List;->isEmpty()Z

    move-result v3

    if-eqz v3, :cond_1

    goto :goto_1

    .line 1237
    :cond_1
    invoke-interface {v2}, Ljava/util/List;->stream()Ljava/util/stream/Stream;

    move-result-object v3

    new-instance v4, Lcom/android/server/audio/AudioServiceStubImpl$$ExternalSyntheticLambda1;

    invoke-direct {v4}, Lcom/android/server/audio/AudioServiceStubImpl$$ExternalSyntheticLambda1;-><init>()V

    invoke-interface {v3, v4}, Ljava/util/stream/Stream;->mapToInt(Ljava/util/function/ToIntFunction;)Ljava/util/stream/IntStream;

    move-result-object v3

    invoke-interface {v3}, Ljava/util/stream/IntStream;->toArray()[I

    move-result-object v3

    .line 1238
    .local v3, "newTids":[I
    const-string v4, "/sys/module/metis/parameters/add_mi_viptask_sched_priority"

    invoke-direct {p0, v4, v3}, Lcom/miui/server/turbosched/TurboSchedManagerService;->writeTidsToPath(Ljava/lang/String;[I)Z

    move-result v0

    .line 1240
    if-eqz v0, :cond_2

    .line 1241
    const-string v5, "-PRIORITY"

    const-string v9, "/sys/module/metis/parameters/del_mi_viptask_sched_priority"

    iget-object v10, p0, Lcom/miui/server/turbosched/TurboSchedManagerService;->mPriorityTidStartTimeMap:Ljava/util/Map;

    move-object v4, p0

    move-object v6, v3

    move-wide v7, p2

    invoke-direct/range {v4 .. v10}, Lcom/miui/server/turbosched/TurboSchedManagerService;->setPolicyTimeoutChecker(Ljava/lang/String;[IJLjava/lang/String;Ljava/util/Map;)V

    .line 1242
    iget-object v4, p0, Lcom/miui/server/turbosched/TurboSchedManagerService;->mHistoryLog:Landroid/util/LocalLog;

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "TS [PRIORITY] : success, tids: "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-static {v3}, Ljava/util/Arrays;->toString([I)Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p2, p3}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v4, v1}, Landroid/util/LocalLog;->log(Ljava/lang/String;)V

    goto :goto_0

    .line 1244
    :cond_2
    iget-object v4, p0, Lcom/miui/server/turbosched/TurboSchedManagerService;->mHistoryLog:Landroid/util/LocalLog;

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "TS [PRIORITY] : failed, R: write failed, tids: "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-static {v3}, Ljava/util/Arrays;->toString([I)Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p2, p3}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v4, v1}, Landroid/util/LocalLog;->log(Ljava/lang/String;)V

    .line 1246
    :goto_0
    invoke-direct {p0}, Lcom/miui/server/turbosched/TurboSchedManagerService;->sendTraceEndMsg()V

    .line 1247
    return-void

    .line 1234
    .end local v3    # "newTids":[I
    :cond_3
    :goto_1
    return-void

    .line 1226
    .end local v0    # "success":Z
    .end local v2    # "tidsList":Ljava/util/List;, "Ljava/util/List<Ljava/lang/Integer;>;"
    :cond_4
    :goto_2
    iget-object v0, p0, Lcom/miui/server/turbosched/TurboSchedManagerService;->mHistoryLog:Landroid/util/LocalLog;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "TS [PRIORITY] : failed, R: not enable, tids: "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-static {p1}, Ljava/util/Arrays;->toString([I)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p2, p3}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/util/LocalLog;->log(Ljava/lang/String;)V

    .line 1227
    return-void
.end method

.method public setTurboSchedActionWithoutBlock([IJ)V
    .locals 0
    .param p1, "tids"    # [I
    .param p2, "time"    # J

    .line 1129
    invoke-virtual {p0, p1, p2, p3}, Lcom/miui/server/turbosched/TurboSchedManagerService;->setTurboSchedAction([IJ)Z

    .line 1130
    return-void
.end method

.method public triggerBoostAction(I)V
    .locals 0
    .param p1, "boostMs"    # I

    .line 1377
    invoke-direct {p0, p1}, Lcom/miui/server/turbosched/TurboSchedManagerService;->metisFrameBoost(I)V

    .line 1378
    return-void
.end method

.method public triggerBoostTask(II)V
    .locals 2
    .param p1, "tid"    # I
    .param p2, "boostSec"    # I

    .line 1382
    invoke-static {}, Landroid/os/TurboSchedMonitor;->getInstance()Landroid/os/TurboSchedMonitor;

    move-result-object v0

    invoke-virtual {v0}, Landroid/os/TurboSchedMonitor;->isCoreAppOptimizerEnabled()Z

    move-result v0

    if-eqz v0, :cond_1

    const-string v0, "/dev/metis"

    invoke-static {v0}, Lcom/miui/server/turbosched/TurboSchedUtil;->checkFileAccess(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 1383
    sget-object v0, Lcom/miui/server/turbosched/TurboSchedManagerService;->mTurboLock:Ljava/lang/Object;

    monitor-enter v0

    .line 1384
    :try_start_0
    const-string v1, "/dev/metis"

    invoke-static {v1}, Landroid/os/NativeTurboSchedManager;->nativeOpenDevice(Ljava/lang/String;)I

    move-result v1

    .line 1385
    .local v1, "handle":I
    if-ltz v1, :cond_0

    .line 1386
    invoke-static {v1, p1, p2}, Landroid/os/NativeTurboSchedManager;->nativeTaskBoost(III)V

    .line 1387
    invoke-static {v1}, Landroid/os/NativeTurboSchedManager;->nativeCloseDevice(I)V

    .line 1389
    .end local v1    # "handle":I
    :cond_0
    monitor-exit v0

    goto :goto_0

    :catchall_0
    move-exception v1

    monitor-exit v0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v1

    .line 1391
    :cond_1
    :goto_0
    return-void
.end method

.method public unregisterStateChangeCallback(Ljava/lang/String;Lmiui/turbosched/ITurboSchedManager$ITurboSchedStateChangeCallback;)V
    .locals 1
    .param p1, "pkgName"    # Ljava/lang/String;
    .param p2, "cb"    # Lmiui/turbosched/ITurboSchedManager$ITurboSchedStateChangeCallback;

    .line 1283
    invoke-static {}, Lcom/miui/server/turbosched/TurboSchedSceneManager;->getInstance()Lcom/miui/server/turbosched/TurboSchedSceneManager;

    move-result-object v0

    invoke-virtual {v0, p1, p2}, Lcom/miui/server/turbosched/TurboSchedSceneManager;->unregisterStateChangeCallback(Ljava/lang/String;Lmiui/turbosched/ITurboSchedManager$ITurboSchedStateChangeCallback;)V

    .line 1284
    return-void
.end method
