class com.miui.server.turbosched.TurboSchedManagerService$13 extends java.util.TimerTask {
	 /* .source "TurboSchedManagerService.java" */
	 /* # annotations */
	 /* .annotation system Ldalvik/annotation/EnclosingMethod; */
	 /* value = Lcom/miui/server/turbosched/TurboSchedManagerService;->setPolicyTimeoutChecker(Ljava/lang/String;[IJLjava/lang/String;Ljava/util/Map;)V */
} // .end annotation
/* .annotation system Ldalvik/annotation/InnerClass; */
/* accessFlags = 0x0 */
/* name = null */
} // .end annotation
/* # instance fields */
final com.miui.server.turbosched.TurboSchedManagerService this$0; //synthetic
final java.lang.String val$cancelPath; //synthetic
final java.lang.String val$logTag; //synthetic
/* # direct methods */
 com.miui.server.turbosched.TurboSchedManagerService$13 ( ) {
/* .locals 0 */
/* .param p1, "this$0" # Lcom/miui/server/turbosched/TurboSchedManagerService; */
/* .line 2017 */
this.this$0 = p1;
this.val$cancelPath = p2;
this.val$logTag = p3;
/* invoke-direct {p0}, Ljava/util/TimerTask;-><init>()V */
return;
} // .end method
/* # virtual methods */
public void run ( ) {
/* .locals 10 */
/* .line 2020 */
/* new-instance v0, Ljava/util/ArrayList; */
/* invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V */
/* .line 2021 */
/* .local v0, "tidsList":Ljava/util/List;, "Ljava/util/List<Ljava/lang/Integer;>;" */
v1 = this.this$0;
com.miui.server.turbosched.TurboSchedManagerService .-$$Nest$fgetmTurboschedSdkTidsLock ( v1 );
/* monitor-enter v1 */
/* .line 2022 */
try { // :try_start_0
	 v2 = this.this$0;
	 com.miui.server.turbosched.TurboSchedManagerService .-$$Nest$fgetmTurboschedSDKParamMap ( v2 );
	 /* .line 2023 */
	 /* .local v2, "iterator":Ljava/util/Iterator;, "Ljava/util/Iterator<Ljava/util/Map$Entry<Ljava/lang/String;Lcom/miui/server/turbosched/TurboSchedManagerService$TurboschedSDKParam;>;>;" */
v3 = } // :goto_0
if ( v3 != null) { // if-eqz v3, :cond_1
	 /* .line 2024 */
	 /* check-cast v3, Ljava/util/Map$Entry; */
	 /* .line 2025 */
	 /* .local v3, "entry":Ljava/util/Map$Entry;, "Ljava/util/Map$Entry<Ljava/lang/String;Lcom/miui/server/turbosched/TurboSchedManagerService$TurboschedSDKParam;>;" */
	 /* check-cast v4, Ljava/lang/String; */
	 /* .line 2026 */
	 /* .local v4, "paramId":Ljava/lang/String; */
	 v5 = this.this$0;
	 com.miui.server.turbosched.TurboSchedManagerService .-$$Nest$fgetmTurboschedSDKParamMap ( v5 );
	 /* check-cast v5, Lcom/miui/server/turbosched/TurboSchedManagerService$TurboschedSDKParam; */
	 /* .line 2027 */
	 /* .local v5, "param":Lcom/miui/server/turbosched/TurboSchedManagerService$TurboschedSDKParam; */
	 if ( v5 != null) { // if-eqz v5, :cond_0
		 com.miui.server.turbosched.TurboSchedManagerService$TurboschedSDKParam .-$$Nest$fgetmStartTime ( v5 );
		 /* move-result-wide v6 */
		 com.miui.server.turbosched.TurboSchedManagerService$TurboschedSDKParam .-$$Nest$fgetmSetTime ( v5 );
		 /* move-result-wide v8 */
		 /* add-long/2addr v6, v8 */
		 java.lang.System .currentTimeMillis ( );
		 /* move-result-wide v8 */
		 /* cmp-long v6, v6, v8 */
		 /* if-gtz v6, :cond_0 */
		 /* .line 2028 */
		 v6 = 		 com.miui.server.turbosched.TurboSchedManagerService$TurboschedSDKParam .-$$Nest$fgetmTid ( v5 );
		 java.lang.Integer .valueOf ( v6 );
		 /* .line 2029 */
		 /* .line 2031 */
	 } // .end local v3 # "entry":Ljava/util/Map$Entry;, "Ljava/util/Map$Entry<Ljava/lang/String;Lcom/miui/server/turbosched/TurboSchedManagerService$TurboschedSDKParam;>;"
} // .end local v4 # "paramId":Ljava/lang/String;
} // .end local v5 # "param":Lcom/miui/server/turbosched/TurboSchedManagerService$TurboschedSDKParam;
} // :cond_0
/* .line 2033 */
v3 = } // :cond_1
/* if-nez v3, :cond_2 */
/* .line 2034 */
/* new-instance v4, Lcom/android/server/audio/AudioServiceStubImpl$$ExternalSyntheticLambda1; */
/* invoke-direct {v4}, Lcom/android/server/audio/AudioServiceStubImpl$$ExternalSyntheticLambda1;-><init>()V */
/* .line 2035 */
/* .local v3, "newTids":[I */
v4 = this.this$0;
v5 = this.val$cancelPath;
v4 = com.miui.server.turbosched.TurboSchedManagerService .-$$Nest$mwriteTidsToPath ( v4,v5,v3 );
/* .line 2036 */
/* .local v4, "ret":Z */
v5 = this.this$0;
com.miui.server.turbosched.TurboSchedManagerService .-$$Nest$fgetmHistoryLog ( v5 );
/* new-instance v6, Ljava/lang/StringBuilder; */
/* invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V */
final String v7 = "TS ["; // const-string v7, "TS ["
(( java.lang.StringBuilder ) v6 ).append ( v7 ); // invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
v7 = this.val$logTag;
(( java.lang.StringBuilder ) v6 ).append ( v7 ); // invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
final String v7 = "] : success: "; // const-string v7, "] : success: "
(( java.lang.StringBuilder ) v6 ).append ( v7 ); // invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v6 ).append ( v4 ); // invoke-virtual {v6, v4}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;
final String v7 = ", tids: "; // const-string v7, ", tids: "
(( java.lang.StringBuilder ) v6 ).append ( v7 ); // invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
java.util.Arrays .toString ( v3 );
(( java.lang.StringBuilder ) v6 ).append ( v7 ); // invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v6 ).toString ( ); // invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
(( android.util.LocalLog ) v5 ).log ( v6 ); // invoke-virtual {v5, v6}, Landroid/util/LocalLog;->log(Ljava/lang/String;)V
/* .line 2039 */
} // .end local v3 # "newTids":[I
} // .end local v4 # "ret":Z
} // :cond_2
v3 = this.this$0;
com.miui.server.turbosched.TurboSchedManagerService .-$$Nest$fgetmTurboschedSDKParamMap ( v3 );
if ( v3 != null) { // if-eqz v3, :cond_3
v3 = this.this$0;
v3 = com.miui.server.turbosched.TurboSchedManagerService .-$$Nest$fgetmTurboschedSDKParamMap ( v3 );
if ( v3 != null) { // if-eqz v3, :cond_5
/* .line 2040 */
} // :cond_3
v3 = this.this$0;
v3 = this.TurboSchedSDKTimerTask;
int v4 = 0; // const/4 v4, 0x0
if ( v3 != null) { // if-eqz v3, :cond_4
/* .line 2041 */
v3 = this.this$0;
v3 = this.TurboSchedSDKTimerTask;
(( java.util.TimerTask ) v3 ).cancel ( ); // invoke-virtual {v3}, Ljava/util/TimerTask;->cancel()Z
/* .line 2042 */
v3 = this.this$0;
this.TurboSchedSDKTimerTask = v4;
/* .line 2044 */
} // :cond_4
v3 = this.this$0;
v3 = this.TurboSchedSDKTimer;
if ( v3 != null) { // if-eqz v3, :cond_5
/* .line 2045 */
v3 = this.this$0;
v3 = this.TurboSchedSDKTimer;
(( java.util.Timer ) v3 ).cancel ( ); // invoke-virtual {v3}, Ljava/util/Timer;->cancel()V
/* .line 2046 */
v3 = this.this$0;
this.TurboSchedSDKTimer = v4;
/* .line 2049 */
} // .end local v2 # "iterator":Ljava/util/Iterator;, "Ljava/util/Iterator<Ljava/util/Map$Entry<Ljava/lang/String;Lcom/miui/server/turbosched/TurboSchedManagerService$TurboschedSDKParam;>;>;"
} // :cond_5
/* monitor-exit v1 */
/* .line 2050 */
return;
/* .line 2049 */
/* :catchall_0 */
/* move-exception v2 */
/* monitor-exit v1 */
/* :try_end_0 */
/* .catchall {:try_start_0 ..:try_end_0} :catchall_0 */
/* throw v2 */
} // .end method
