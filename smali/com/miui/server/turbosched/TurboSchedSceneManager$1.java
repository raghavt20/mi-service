class com.miui.server.turbosched.TurboSchedSceneManager$1 implements android.os.IBinder$DeathRecipient {
	 /* .source "TurboSchedSceneManager.java" */
	 /* # interfaces */
	 /* # annotations */
	 /* .annotation system Ldalvik/annotation/EnclosingMethod; */
	 /* value = Lcom/miui/server/turbosched/TurboSchedSceneManager;->registerStateChangeCallback(Ljava/lang/String;Lmiui/turbosched/ITurboSchedManager$ITurboSchedStateChangeCallback;)V */
} // .end annotation
/* .annotation system Ldalvik/annotation/InnerClass; */
/* accessFlags = 0x0 */
/* name = null */
} // .end annotation
/* # instance fields */
final com.miui.server.turbosched.TurboSchedSceneManager this$0; //synthetic
final miui.turbosched.ITurboSchedManager$ITurboSchedStateChangeCallback val$cb; //synthetic
final java.lang.String val$pkgName; //synthetic
/* # direct methods */
 com.miui.server.turbosched.TurboSchedSceneManager$1 ( ) {
/* .locals 0 */
/* .param p1, "this$0" # Lcom/miui/server/turbosched/TurboSchedSceneManager; */
/* .annotation system Ldalvik/annotation/Signature; */
/* value = { */
/* "()V" */
/* } */
} // .end annotation
/* .line 145 */
this.this$0 = p1;
this.val$pkgName = p2;
this.val$cb = p3;
/* invoke-direct {p0}, Ljava/lang/Object;-><init>()V */
return;
} // .end method
/* # virtual methods */
public void binderDied ( ) {
/* .locals 3 */
/* .line 148 */
/* new-instance v0, Ljava/lang/StringBuilder; */
/* invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V */
final String v1 = "binder died pkg="; // const-string v1, "binder died pkg="
(( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
v1 = this.val$pkgName;
(( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
final String v1 = ", cb="; // const-string v1, ", cb="
(( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
v1 = this.val$cb;
(( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v0 ).toString ( ); // invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
/* const-string/jumbo v1, "tsched_scene" */
android.util.Slog .i ( v1,v0 );
/* .line 149 */
v0 = this.val$cb;
int v1 = 0; // const/4 v1, 0x0
/* .line 150 */
v0 = this.this$0;
v1 = this.val$pkgName;
v2 = this.val$cb;
(( com.miui.server.turbosched.TurboSchedSceneManager ) v0 ).unregisterStateChangeCallback ( v1, v2 ); // invoke-virtual {v0, v1, v2}, Lcom/miui/server/turbosched/TurboSchedSceneManager;->unregisterStateChangeCallback(Ljava/lang/String;Lmiui/turbosched/ITurboSchedManager$ITurboSchedStateChangeCallback;)V
/* .line 151 */
return;
} // .end method
