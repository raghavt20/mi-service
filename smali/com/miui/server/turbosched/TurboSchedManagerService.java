public class com.miui.server.turbosched.TurboSchedManagerService extends miui.turbosched.ITurboSchedManager$Stub {
	 /* .source "TurboSchedManagerService.java" */
	 /* # annotations */
	 /* .annotation system Ldalvik/annotation/MemberClasses; */
	 /* value = { */
	 /* Lcom/miui/server/turbosched/TurboSchedManagerService$TurboSchedHandler;, */
	 /* Lcom/miui/server/turbosched/TurboSchedManagerService$TurboSchedBroadcastReceiver;, */
	 /* Lcom/miui/server/turbosched/TurboSchedManagerService$TraceInfo;, */
	 /* Lcom/miui/server/turbosched/TurboSchedManagerService$TurboschedSDKParam;, */
	 /* Lcom/miui/server/turbosched/TurboSchedManagerService$Lifecycle; */
	 /* } */
} // .end annotation
/* # static fields */
private static final java.lang.String BOARD_TEMP_FILE;
public static final java.lang.String BOOST_ENABLE_PATH;
public static final java.lang.String BOOST_SCHED_PATH;
public static final java.lang.String BOOST_THERMAL_PATH;
private static final java.lang.String CLOUD_TURBO_SCHED_ALLOW_LIST;
private static final java.lang.String CLOUD_TURBO_SCHED_DPS_ENABLE;
private static final java.lang.String CLOUD_TURBO_SCHED_ENABLE_ALL;
private static final java.lang.String CLOUD_TURBO_SCHED_ENABLE_CORE_APP_OPTIMIZER;
private static final java.lang.String CLOUD_TURBO_SCHED_ENABLE_CORE_TOP20_APP_OPTIMIZER;
private static final java.lang.String CLOUD_TURBO_SCHED_ENABLE_V2;
private static final java.lang.String CLOUD_TURBO_SCHED_FRAME_TURBO_WHITE_LIST;
private static final java.lang.String CLOUD_TURBO_SCHED_LINK_APP_LIST;
private static final java.lang.String CLOUD_TURBO_SCHED_POLICY_LIST;
private static final java.lang.String CLOUD_TURBO_SCHED_THERMAL_BREAK_ENABLE;
private static final java.lang.String COMMAND_DRY_RUN;
private static final java.lang.String ClOUD_TURBO_SCHED_THERMAL_BREAK_THRESHOLD;
public static final Boolean DEBUG;
private static java.util.List DEFAULT_APP_LIST;
/* .annotation system Ldalvik/annotation/Signature; */
/* value = { */
/* "Ljava/util/List<", */
/* "Ljava/lang/String;", */
/* ">;" */
/* } */
} // .end annotation
} // .end field
private static java.util.List DEFAULT_GL_APP_LIST;
/* .annotation system Ldalvik/annotation/Signature; */
/* value = { */
/* "Ljava/util/List<", */
/* "Ljava/lang/String;", */
/* ">;" */
/* } */
} // .end annotation
} // .end field
private static java.util.List DEFAULT_TOP20_APP_LIST;
/* .annotation system Ldalvik/annotation/Signature; */
/* value = { */
/* "Ljava/util/List<", */
/* "Ljava/lang/String;", */
/* ">;" */
/* } */
} // .end annotation
} // .end field
public static final java.lang.String DPS_FORCE_CLUSTER_SCHED_ENABLE_PATH;
public static final java.lang.String DPS_FORCE_FORBIDDEN_WALT_LB_PATH;
public static final java.lang.String DPS_FORCE_VIPTASK_SELECT_RQ_PATH;
public static final java.lang.String DPS_IP_PREFER_CLUSTER_PATH;
public static final java.lang.String DPS_RENDER_PREFER_CLUSTER_PATH;
public static final java.lang.String DPS_STASK_PREFER_CLUSTER_PATH;
public static final java.lang.String DPS_VIP_PREFER_FREQ_PATH;
public static final java.lang.String GAEA_PATH;
private static final Integer GET_BUFFER_TX_COUNT_TRANSACTION;
protected static final Integer MAX_HISTORY_ITEMS;
private static final Integer METIS_BOOST_DURATION;
private static final java.lang.String METIS_BOOST_DURATION_PATH;
private static final java.lang.String METIS_DEV_PATH;
private static final Integer METIS_IOCTL_BOOST_CMD;
public static final java.lang.String METIS_VERSION_PATH;
private static final java.lang.String NO_THERMAL_BREAK_STRING;
public static final java.lang.String ORI_TURBO_SCHED_PATH;
private static final java.lang.String PROCESS_ALIEXPRESS;
private static final java.lang.String PROCESS_ALIPAY;
private static final java.lang.String PROCESS_AMAZON_SHOPPING;
private static final java.lang.String PROCESS_ARTICLE_LITE;
private static final java.lang.String PROCESS_ARTICLE_NEWS;
private static final java.lang.String PROCESS_ARTICLE_VIDEO;
private static final java.lang.String PROCESS_AWEME;
private static final java.lang.String PROCESS_AWEME_LITE;
private static final java.lang.String PROCESS_BAIDU;
private static final java.lang.String PROCESS_BAIDU_DISK;
private static final java.lang.String PROCESS_BILIBILI;
private static final java.lang.String PROCESS_BILIBILI_HD;
private static final java.lang.String PROCESS_DISCORD;
private static final java.lang.String PROCESS_DOUYU;
private static final java.lang.String PROCESS_DRAGON_READ;
private static final java.lang.String PROCESS_FACEBOOK;
private static final java.lang.String PROCESS_GIFMAKER;
private static final java.lang.String PROCESS_HUNANTV_IMGO;
private static final java.lang.String PROCESS_HUYA;
private static final java.lang.String PROCESS_INSHOT;
private static final java.lang.String PROCESS_INSTAGRAM;
private static final java.lang.String PROCESS_KMXS_READER;
private static final java.lang.String PROCESS_KUAISHOU_NEBULA;
private static final java.lang.String PROCESS_KUGOU;
private static final java.lang.String PROCESS_LAZADA;
private static final java.lang.String PROCESS_LINKEDIN;
private static final java.lang.String PROCESS_MINIMAP;
private static final java.lang.String PROCESS_MOBILEQQ;
private static final java.lang.String PROCESS_MOBILEQQ_QZONE;
private static final java.lang.String PROCESS_MOJ;
private static final java.lang.String PROCESS_MX_PLAYER;
private static final java.lang.String PROCESS_OUTLOOK;
private static final java.lang.String PROCESS_PINDUODUO;
private static final java.lang.String PROCESS_PINTEREST;
private static final java.lang.String PROCESS_QIYI;
private static final java.lang.String PROCESS_QQMUSIC;
private static final java.lang.String PROCESS_QUARK_BROWSER;
private static final java.lang.String PROCESS_RIMET;
private static final java.lang.String PROCESS_SHARECHAT;
private static final java.lang.String PROCESS_SHAZAM;
private static final java.lang.String PROCESS_SHEIN;
private static final java.lang.String PROCESS_SNAPCHAT;
private static final java.lang.String PROCESS_SPOTIFY;
private static final java.lang.String PROCESS_TAOBAO;
private static final java.lang.String PROCESS_TELEGRAM;
private static final java.lang.String PROCESS_TENCENT_MTT;
private static final java.lang.String PROCESS_TENCENT_NEWS;
private static final java.lang.String PROCESS_TENCENT_QQLIVE;
private static final java.lang.String PROCESS_TRUECALLER;
private static final java.lang.String PROCESS_UCMOBILE;
private static final java.lang.String PROCESS_WA_BUSINESS;
private static final java.lang.String PROCESS_WECHAT;
private static final java.lang.String PROCESS_WEIBO;
private static final java.lang.String PROCESS_WHATSAPP;
private static final java.lang.String PROCESS_WPS_OFFICE;
private static final java.lang.String PROCESS_X;
private static final java.lang.String PROCESS_XHS;
private static final java.lang.String PROCESS_XUEXI;
private static final java.lang.String PROCESS_ZHIHU;
private static java.lang.String PR_NAME;
public static final java.lang.String RECORD_RT_PATH;
public static final java.lang.String SERVICE_NAME;
private static java.util.List TABLET_TOP20_APP_LIST;
/* .annotation system Ldalvik/annotation/Signature; */
/* value = { */
/* "Ljava/util/List<", */
/* "Ljava/lang/String;", */
/* ">;" */
/* } */
} // .end annotation
} // .end field
private static java.util.List TABLET_TOP8_APP_LIST;
/* .annotation system Ldalvik/annotation/Signature; */
/* value = { */
/* "Ljava/util/List<", */
/* "Ljava/lang/String;", */
/* ">;" */
/* } */
} // .end annotation
} // .end field
public static final java.lang.String TAG;
private static java.util.Map TEMPREATURE_THROSHOLD_BOOST_STRING_MAP;
/* .annotation system Ldalvik/annotation/Signature; */
/* value = { */
/* "Ljava/util/Map<", */
/* "Ljava/lang/Integer;", */
/* "Ljava/lang/String;", */
/* ">;" */
/* } */
} // .end annotation
} // .end field
private static final java.lang.String TURBO_SCHED_BOOST_WITH_FREQUENCY_PATH;
private static final java.lang.String TURBO_SCHED_CANCEL_BOOST_WITH_FREQUENCY_PATH;
private static final java.lang.String TURBO_SCHED_CANCEL_LITTLE_CORE_PATH;
private static final java.lang.String TURBO_SCHED_CANCEL_PRIORITY_PATH;
private static final java.lang.String TURBO_SCHED_LITTLE_CORE_PATH;
private static final java.lang.String TURBO_SCHED_LOCK_BLOCKED_PID_PATH;
public static final java.lang.String TURBO_SCHED_PATH;
private static final java.lang.String TURBO_SCHED_PRIORITY_PATH;
private static final java.lang.String TURBO_SCHED_THERMAL_BREAK_PATH;
private static final java.lang.String VERSION;
public static final java.lang.String VIP_LINK_PATH;
private static java.lang.Object mTurboLock;
/* # instance fields */
private final java.lang.String BufferTxCountPath;
java.util.Timer TurboSchedSDKTimer;
java.util.TimerTask TurboSchedSDKTimerTask;
private java.lang.String enableGaeaAppProcessName;
private java.lang.String enableLinkVipAppProcessName;
private java.util.List m8450Devices;
/* .annotation system Ldalvik/annotation/Signature; */
/* value = { */
/* "Ljava/util/List<", */
/* "Ljava/lang/String;", */
/* ">;" */
/* } */
} // .end annotation
} // .end field
private java.util.List m8475Devices;
/* .annotation system Ldalvik/annotation/Signature; */
/* value = { */
/* "Ljava/util/List<", */
/* "Ljava/lang/String;", */
/* ">;" */
/* } */
} // .end annotation
} // .end field
private java.util.List m8550Devices;
/* .annotation system Ldalvik/annotation/Signature; */
/* value = { */
/* "Ljava/util/List<", */
/* "Ljava/lang/String;", */
/* ">;" */
/* } */
} // .end annotation
} // .end field
private final android.app.IActivityManager mActivityManagerService;
private java.util.List mAppsLinkVipList;
/* .annotation system Ldalvik/annotation/Signature; */
/* value = { */
/* "Ljava/util/List<", */
/* "Ljava/lang/String;", */
/* ">;" */
/* } */
} // .end annotation
} // .end field
private java.util.List mAppsLockContentionList;
/* .annotation system Ldalvik/annotation/Signature; */
/* value = { */
/* "Ljava/util/List<", */
/* "Ljava/lang/String;", */
/* ">;" */
/* } */
} // .end annotation
} // .end field
private Integer mAppsLockContentionPid;
private java.util.Map mBWFTidStartTimeMap;
/* .annotation system Ldalvik/annotation/Signature; */
/* value = { */
/* "Ljava/util/Map<", */
/* "Ljava/lang/Integer;", */
/* "Ljava/lang/Long;", */
/* ">;" */
/* } */
} // .end annotation
} // .end field
private java.util.List mBoostDuration;
/* .annotation system Ldalvik/annotation/Signature; */
/* value = { */
/* "Ljava/util/List<", */
/* "Ljava/lang/Long;", */
/* ">;" */
/* } */
} // .end annotation
} // .end field
private java.util.List mCallerAllowList;
/* .annotation system Ldalvik/annotation/Signature; */
/* value = { */
/* "Ljava/util/List<", */
/* "Ljava/lang/String;", */
/* ">;" */
/* } */
} // .end annotation
} // .end field
final android.database.ContentObserver mCloudAllowListObserver;
private Boolean mCloudControlEnabled;
final android.database.ContentObserver mCloudCoreAppOptimizerEnableObserver;
final android.database.ContentObserver mCloudCoreTop20AppOptimizerEnableObserver;
final android.database.ContentObserver mCloudDpsEnableObserver;
final android.database.ContentObserver mCloudFrameTurboWhiteListObserver;
final android.database.ContentObserver mCloudLinkWhiteListObserver;
final android.database.ContentObserver mCloudPolicyListObserver;
final android.database.ContentObserver mCloudSwichObserver;
final android.database.ContentObserver mCloudThermalBreakEnableObserver;
final android.database.ContentObserver mCloudThermalBreakThresholdObserver;
final android.database.ContentObserver mCloudTurboschedMiuiSdkObserver;
private android.content.Context mContext;
private java.util.List mCoreAppsPackageNameList;
/* .annotation system Ldalvik/annotation/Signature; */
/* value = { */
/* "Ljava/util/List<", */
/* "Ljava/lang/String;", */
/* ">;" */
/* } */
} // .end annotation
} // .end field
private Boolean mDPSEnable;
private Boolean mDPSSupport;
private Boolean mEnableTop20;
private Long mFgCorePid;
private Long mFgCoreUid;
private com.miui.server.turbosched.FrameTurboAction mFrameTurboAction;
private java.util.Map mFrameTurboAppMap;
/* .annotation system Ldalvik/annotation/Signature; */
/* value = { */
/* "Ljava/util/Map<", */
/* "Ljava/lang/String;", */
/* "Ljava/util/List<", */
/* "Ljava/lang/String;", */
/* ">;>;" */
/* } */
} // .end annotation
} // .end field
private Boolean mGaeaEnable;
private com.miui.server.turbosched.GameTurboAction mGameTurboAction;
public android.os.HandlerThread mHandlerThread;
private android.util.LocalLog mHistoryLog;
private Boolean mInStartUpMode;
private Boolean mIsGlobal;
private Boolean mIsOnScroll;
private Boolean mKernelEnabelUpdate;
private Long mLastVsyncId;
private Boolean mLinkEnable;
private java.lang.String mMetisVersion;
private android.content.pm.PackageManager mPm;
private java.util.List mPolicyListBackup;
/* .annotation system Ldalvik/annotation/Signature; */
/* value = { */
/* "Ljava/util/List<", */
/* "Ljava/lang/String;", */
/* ">;" */
/* } */
} // .end annotation
} // .end field
private java.util.Map mPriorityTidStartTimeMap;
/* .annotation system Ldalvik/annotation/Signature; */
/* value = { */
/* "Ljava/util/Map<", */
/* "Ljava/lang/Integer;", */
/* "Ljava/lang/Long;", */
/* ">;" */
/* } */
} // .end annotation
} // .end field
private java.lang.String mProcessName;
private final android.app.IProcessObserver$Stub mProcessObserver;
private Boolean mReceiveCloudData;
private Boolean mRegistedForegroundReceiver;
private Boolean mScreenOn;
private java.util.List mStartUpAllowList;
/* .annotation system Ldalvik/annotation/Signature; */
/* value = { */
/* "Ljava/util/List<", */
/* "Ljava/lang/String;", */
/* ">;" */
/* } */
} // .end annotation
} // .end field
private Boolean mStartUpModeInitFinish;
private Boolean mSupportGaea;
private android.os.IBinder mSurfaceFlinger;
private Boolean mThermalBreakEnabled;
private Boolean mTurboEnabled;
private Boolean mTurboEnabledV2;
private Boolean mTurboNodeExist;
private com.miui.server.turbosched.TurboSchedManagerService$TurboSchedHandler mTurboSchedHandler;
private java.util.Map mTurboschedSDKParamMap;
/* .annotation system Ldalvik/annotation/Signature; */
/* value = { */
/* "Ljava/util/Map<", */
/* "Ljava/lang/String;", */
/* "Lcom/miui/server/turbosched/TurboSchedManagerService$TurboschedSDKParam;", */
/* ">;" */
/* } */
} // .end annotation
} // .end field
private java.util.List mTurboschedSdkTidsList;
/* .annotation system Ldalvik/annotation/Signature; */
/* value = { */
/* "Ljava/util/List<", */
/* "Ljava/lang/Integer;", */
/* ">;" */
/* } */
} // .end annotation
} // .end field
private java.lang.Object mTurboschedSdkTidsLock;
/* # direct methods */
static java.util.List -$$Nest$fgetmAppsLockContentionList ( com.miui.server.turbosched.TurboSchedManagerService p0 ) { //bridge//synthethic
/* .locals 0 */
p0 = this.mAppsLockContentionList;
} // .end method
static Integer -$$Nest$fgetmAppsLockContentionPid ( com.miui.server.turbosched.TurboSchedManagerService p0 ) { //bridge//synthethic
/* .locals 0 */
/* iget p0, p0, Lcom/miui/server/turbosched/TurboSchedManagerService;->mAppsLockContentionPid:I */
} // .end method
static android.content.Context -$$Nest$fgetmContext ( com.miui.server.turbosched.TurboSchedManagerService p0 ) { //bridge//synthethic
/* .locals 0 */
p0 = this.mContext;
} // .end method
static com.miui.server.turbosched.FrameTurboAction -$$Nest$fgetmFrameTurboAction ( com.miui.server.turbosched.TurboSchedManagerService p0 ) { //bridge//synthethic
/* .locals 0 */
p0 = this.mFrameTurboAction;
} // .end method
static android.util.LocalLog -$$Nest$fgetmHistoryLog ( com.miui.server.turbosched.TurboSchedManagerService p0 ) { //bridge//synthethic
/* .locals 0 */
p0 = this.mHistoryLog;
} // .end method
static java.lang.String -$$Nest$fgetmProcessName ( com.miui.server.turbosched.TurboSchedManagerService p0 ) { //bridge//synthethic
/* .locals 0 */
p0 = this.mProcessName;
} // .end method
static Boolean -$$Nest$fgetmScreenOn ( com.miui.server.turbosched.TurboSchedManagerService p0 ) { //bridge//synthethic
/* .locals 0 */
/* iget-boolean p0, p0, Lcom/miui/server/turbosched/TurboSchedManagerService;->mScreenOn:Z */
} // .end method
static Boolean -$$Nest$fgetmStartUpModeInitFinish ( com.miui.server.turbosched.TurboSchedManagerService p0 ) { //bridge//synthethic
/* .locals 0 */
/* iget-boolean p0, p0, Lcom/miui/server/turbosched/TurboSchedManagerService;->mStartUpModeInitFinish:Z */
} // .end method
static java.util.Map -$$Nest$fgetmTurboschedSDKParamMap ( com.miui.server.turbosched.TurboSchedManagerService p0 ) { //bridge//synthethic
/* .locals 0 */
p0 = this.mTurboschedSDKParamMap;
} // .end method
static java.lang.Object -$$Nest$fgetmTurboschedSdkTidsLock ( com.miui.server.turbosched.TurboSchedManagerService p0 ) { //bridge//synthethic
/* .locals 0 */
p0 = this.mTurboschedSdkTidsLock;
} // .end method
static void -$$Nest$fputmAppsLockContentionPid ( com.miui.server.turbosched.TurboSchedManagerService p0, Integer p1 ) { //bridge//synthethic
/* .locals 0 */
/* iput p1, p0, Lcom/miui/server/turbosched/TurboSchedManagerService;->mAppsLockContentionPid:I */
return;
} // .end method
static void -$$Nest$fputmFgCorePid ( com.miui.server.turbosched.TurboSchedManagerService p0, Long p1 ) { //bridge//synthethic
/* .locals 0 */
/* iput-wide p1, p0, Lcom/miui/server/turbosched/TurboSchedManagerService;->mFgCorePid:J */
return;
} // .end method
static void -$$Nest$fputmFgCoreUid ( com.miui.server.turbosched.TurboSchedManagerService p0, Long p1 ) { //bridge//synthethic
/* .locals 0 */
/* iput-wide p1, p0, Lcom/miui/server/turbosched/TurboSchedManagerService;->mFgCoreUid:J */
return;
} // .end method
static void -$$Nest$fputmProcessName ( com.miui.server.turbosched.TurboSchedManagerService p0, java.lang.String p1 ) { //bridge//synthethic
/* .locals 0 */
this.mProcessName = p1;
return;
} // .end method
static void -$$Nest$fputmScreenOn ( com.miui.server.turbosched.TurboSchedManagerService p0, Boolean p1 ) { //bridge//synthethic
/* .locals 0 */
/* iput-boolean p1, p0, Lcom/miui/server/turbosched/TurboSchedManagerService;->mScreenOn:Z */
return;
} // .end method
static void -$$Nest$mexitStartUpMode ( com.miui.server.turbosched.TurboSchedManagerService p0 ) { //bridge//synthethic
/* .locals 0 */
/* invoke-direct {p0}, Lcom/miui/server/turbosched/TurboSchedManagerService;->exitStartUpMode()V */
return;
} // .end method
static java.lang.String -$$Nest$mgetProcessNameByPid ( com.miui.server.turbosched.TurboSchedManagerService p0, Integer p1 ) { //bridge//synthethic
/* .locals 0 */
/* invoke-direct {p0, p1}, Lcom/miui/server/turbosched/TurboSchedManagerService;->getProcessNameByPid(I)Ljava/lang/String; */
} // .end method
static void -$$Nest$mupdateCloudAllowListProp ( com.miui.server.turbosched.TurboSchedManagerService p0 ) { //bridge//synthethic
/* .locals 0 */
/* invoke-direct {p0}, Lcom/miui/server/turbosched/TurboSchedManagerService;->updateCloudAllowListProp()V */
return;
} // .end method
static void -$$Nest$mupdateCloudDpsEnableProp ( com.miui.server.turbosched.TurboSchedManagerService p0 ) { //bridge//synthethic
/* .locals 0 */
/* invoke-direct {p0}, Lcom/miui/server/turbosched/TurboSchedManagerService;->updateCloudDpsEnableProp()V */
return;
} // .end method
static void -$$Nest$mupdateCloudFrameTurboWhiteListProp ( com.miui.server.turbosched.TurboSchedManagerService p0 ) { //bridge//synthethic
/* .locals 0 */
/* invoke-direct {p0}, Lcom/miui/server/turbosched/TurboSchedManagerService;->updateCloudFrameTurboWhiteListProp()V */
return;
} // .end method
static void -$$Nest$mupdateCloudLinkWhiteListProp ( com.miui.server.turbosched.TurboSchedManagerService p0 ) { //bridge//synthethic
/* .locals 0 */
/* invoke-direct {p0}, Lcom/miui/server/turbosched/TurboSchedManagerService;->updateCloudLinkWhiteListProp()V */
return;
} // .end method
static void -$$Nest$mupdateCloudPolicyListProp ( com.miui.server.turbosched.TurboSchedManagerService p0 ) { //bridge//synthethic
/* .locals 0 */
/* invoke-direct {p0}, Lcom/miui/server/turbosched/TurboSchedManagerService;->updateCloudPolicyListProp()V */
return;
} // .end method
static void -$$Nest$mupdateCloudThermalBreakEnableProp ( com.miui.server.turbosched.TurboSchedManagerService p0 ) { //bridge//synthethic
/* .locals 0 */
/* invoke-direct {p0}, Lcom/miui/server/turbosched/TurboSchedManagerService;->updateCloudThermalBreakEnableProp()V */
return;
} // .end method
static void -$$Nest$mupdateCloudThermalBreakThresholdProp ( com.miui.server.turbosched.TurboSchedManagerService p0 ) { //bridge//synthethic
/* .locals 0 */
/* invoke-direct {p0}, Lcom/miui/server/turbosched/TurboSchedManagerService;->updateCloudThermalBreakThresholdProp()V */
return;
} // .end method
static void -$$Nest$mupdateCoreAppOptimizerEnableProp ( com.miui.server.turbosched.TurboSchedManagerService p0 ) { //bridge//synthethic
/* .locals 0 */
/* invoke-direct {p0}, Lcom/miui/server/turbosched/TurboSchedManagerService;->updateCoreAppOptimizerEnableProp()V */
return;
} // .end method
static void -$$Nest$mupdateCoreTop20AppOptimizerEnableProp ( com.miui.server.turbosched.TurboSchedManagerService p0 ) { //bridge//synthethic
/* .locals 0 */
/* invoke-direct {p0}, Lcom/miui/server/turbosched/TurboSchedManagerService;->updateCoreTop20AppOptimizerEnableProp()V */
return;
} // .end method
static void -$$Nest$mupdateEnableMiuiSdkProp ( com.miui.server.turbosched.TurboSchedManagerService p0, Boolean p1 ) { //bridge//synthethic
/* .locals 0 */
/* invoke-direct {p0, p1}, Lcom/miui/server/turbosched/TurboSchedManagerService;->updateEnableMiuiSdkProp(Z)V */
return;
} // .end method
static void -$$Nest$mupdateEnableProp ( com.miui.server.turbosched.TurboSchedManagerService p0, Boolean p1 ) { //bridge//synthethic
/* .locals 0 */
/* invoke-direct {p0, p1}, Lcom/miui/server/turbosched/TurboSchedManagerService;->updateEnableProp(Z)V */
return;
} // .end method
static Boolean -$$Nest$mwriteTidsToPath ( com.miui.server.turbosched.TurboSchedManagerService p0, java.lang.String p1, Integer[] p2 ) { //bridge//synthethic
/* .locals 0 */
p0 = /* invoke-direct {p0, p1, p2}, Lcom/miui/server/turbosched/TurboSchedManagerService;->writeTidsToPath(Ljava/lang/String;[I)Z */
} // .end method
static Boolean -$$Nest$mwriteTurboSchedNode ( com.miui.server.turbosched.TurboSchedManagerService p0, java.lang.String p1, java.lang.String p2, java.lang.String p3 ) { //bridge//synthethic
/* .locals 0 */
p0 = /* invoke-direct {p0, p1, p2, p3}, Lcom/miui/server/turbosched/TurboSchedManagerService;->writeTurboSchedNode(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Z */
} // .end method
static com.miui.server.turbosched.TurboSchedManagerService ( ) {
/* .locals 24 */
/* .line 71 */
/* sget-boolean v0, Landroid/os/Build;->IS_DEBUGGABLE:Z */
com.miui.server.turbosched.TurboSchedManagerService.DEBUG = (v0!= 0);
/* .line 187 */
/* new-instance v1, Ljava/util/LinkedHashMap; */
/* invoke-direct {v1}, Ljava/util/LinkedHashMap;-><init>()V */
/* .line 189 */
/* const/16 v2, 0x32 */
java.lang.Integer .valueOf ( v2 );
final String v3 = " cpu4:2112000 cpu7:2054400"; // const-string v3, " cpu4:2112000 cpu7:2054400"
/* .line 190 */
v1 = com.miui.server.turbosched.TurboSchedManagerService.TEMPREATURE_THROSHOLD_BOOST_STRING_MAP;
/* const/16 v2, 0x2d */
java.lang.Integer .valueOf ( v2 );
final String v3 = " cpu4:2227200 cpu7:2169600"; // const-string v3, " cpu4:2227200 cpu7:2169600"
/* .line 191 */
v1 = com.miui.server.turbosched.TurboSchedManagerService.TEMPREATURE_THROSHOLD_BOOST_STRING_MAP;
/* const/16 v2, 0x28 */
java.lang.Integer .valueOf ( v2 );
final String v3 = " cpu4:2342400 cpu7:2284800"; // const-string v3, " cpu4:2342400 cpu7:2284800"
/* .line 192 */
v1 = com.miui.server.turbosched.TurboSchedManagerService.TEMPREATURE_THROSHOLD_BOOST_STRING_MAP;
/* const/16 v2, 0x23 */
java.lang.Integer .valueOf ( v2 );
final String v3 = " cpu4:2419200 cpu7:2400000"; // const-string v3, " cpu4:2419200 cpu7:2400000"
/* .line 198 */
if ( v0 != null) { // if-eqz v0, :cond_0
/* const/16 v0, 0x4000 */
} // :cond_0
/* const/16 v0, 0x1000 */
} // :goto_0
/* .line 202 */
/* new-instance v0, Ljava/lang/Object; */
/* invoke-direct {v0}, Ljava/lang/Object;-><init>()V */
/* .line 242 */
final String v0 = "ro.product.name"; // const-string v0, "ro.product.name"
final String v1 = ""; // const-string v1, ""
android.os.SystemProperties .get ( v0,v1 );
/* .line 253 */
/* new-instance v0, Ljava/util/ArrayList; */
final String v1 = "com.smile.gifmaker"; // const-string v1, "com.smile.gifmaker"
final String v2 = "com.sina.weibo"; // const-string v2, "com.sina.weibo"
final String v3 = "com.ss.android.article.news"; // const-string v3, "com.ss.android.article.news"
final String v4 = "com.taobao.taobao"; // const-string v4, "com.taobao.taobao"
final String v5 = "com.tencent.mm"; // const-string v5, "com.tencent.mm"
final String v6 = "com.ss.android.ugc.aweme"; // const-string v6, "com.ss.android.ugc.aweme"
/* const-string/jumbo v7, "tv.danmaku.bili" */
/* filled-new-array/range {v1 ..v7}, [Ljava/lang/String; */
/* .line 254 */
java.util.Arrays .asList ( v1 );
/* invoke-direct {v0, v1}, Ljava/util/ArrayList;-><init>(Ljava/util/Collection;)V */
/* .line 268 */
/* new-instance v0, Ljava/util/ArrayList; */
final String v1 = "com.smile.gifmaker"; // const-string v1, "com.smile.gifmaker"
final String v2 = "com.sina.weibo"; // const-string v2, "com.sina.weibo"
final String v3 = "com.ss.android.article.news"; // const-string v3, "com.ss.android.article.news"
final String v4 = "com.taobao.taobao"; // const-string v4, "com.taobao.taobao"
final String v5 = "com.tencent.mm"; // const-string v5, "com.tencent.mm"
final String v6 = "com.ss.android.ugc.aweme"; // const-string v6, "com.ss.android.ugc.aweme"
/* const-string/jumbo v7, "tv.danmaku.bili" */
final String v8 = "com.kuaishou.nebula"; // const-string v8, "com.kuaishou.nebula"
final String v9 = "com.tencent.mobileqq"; // const-string v9, "com.tencent.mobileqq"
final String v10 = "com.tencent.mobileqq:qzone"; // const-string v10, "com.tencent.mobileqq:qzone"
final String v11 = "com.ss.android.ugc.aweme.lite"; // const-string v11, "com.ss.android.ugc.aweme.lite"
final String v12 = "com.tencent.qqlive"; // const-string v12, "com.tencent.qqlive"
final String v13 = "com.ss.android.article.lite"; // const-string v13, "com.ss.android.article.lite"
final String v14 = "com.baidu.searchbox"; // const-string v14, "com.baidu.searchbox"
final String v15 = "com.xunmeng.pinduoduo"; // const-string v15, "com.xunmeng.pinduoduo"
final String v16 = "com.UCMobile"; // const-string v16, "com.UCMobile"
final String v17 = "com.dragon.read"; // const-string v17, "com.dragon.read"
final String v18 = "com.qiyi.video"; // const-string v18, "com.qiyi.video"
final String v19 = "com.ss.android.article.video"; // const-string v19, "com.ss.android.article.video"
final String v20 = "com.eg.android.AlipayGphone"; // const-string v20, "com.eg.android.AlipayGphone"
final String v21 = "com.tencent.mtt"; // const-string v21, "com.tencent.mtt"
final String v22 = "com.kmxs.reader"; // const-string v22, "com.kmxs.reader"
/* filled-new-array/range {v1 ..v22}, [Ljava/lang/String; */
/* .line 269 */
java.util.Arrays .asList ( v1 );
/* invoke-direct {v0, v1}, Ljava/util/ArrayList;-><init>(Ljava/util/Collection;)V */
/* .line 295 */
/* new-instance v0, Ljava/util/ArrayList; */
final String v1 = "com.ss.android.ugc.aweme"; // const-string v1, "com.ss.android.ugc.aweme"
final String v2 = "com.tencent.mm"; // const-string v2, "com.tencent.mm"
final String v3 = "com.alibaba.android.rimet"; // const-string v3, "com.alibaba.android.rimet"
final String v4 = "com.smile.gifmaker"; // const-string v4, "com.smile.gifmaker"
final String v5 = "com.kuaishou.nebula"; // const-string v5, "com.kuaishou.nebula"
/* const-string/jumbo v6, "tv.danmaku.bilibilihd" */
final String v7 = "com.ss.android.ugc.aweme.lite"; // const-string v7, "com.ss.android.ugc.aweme.lite"
final String v8 = "com.tencent.mobileqq"; // const-string v8, "com.tencent.mobileqq"
/* filled-new-array/range {v1 ..v8}, [Ljava/lang/String; */
/* .line 296 */
java.util.Arrays .asList ( v1 );
/* invoke-direct {v0, v1}, Ljava/util/ArrayList;-><init>(Ljava/util/Collection;)V */
/* .line 309 */
/* new-instance v0, Ljava/util/ArrayList; */
final String v1 = "com.ss.android.ugc.aweme"; // const-string v1, "com.ss.android.ugc.aweme"
final String v2 = "com.tencent.mm"; // const-string v2, "com.tencent.mm"
final String v3 = "com.alibaba.android.rimet"; // const-string v3, "com.alibaba.android.rimet"
final String v4 = "com.smile.gifmaker"; // const-string v4, "com.smile.gifmaker"
final String v5 = "com.kuaishou.nebula"; // const-string v5, "com.kuaishou.nebula"
/* const-string/jumbo v6, "tv.danmaku.bilibilihd" */
final String v7 = "com.ss.android.ugc.aweme.lite"; // const-string v7, "com.ss.android.ugc.aweme.lite"
final String v8 = "com.tencent.mobileqq"; // const-string v8, "com.tencent.mobileqq"
final String v9 = "com.baidu.netdisk"; // const-string v9, "com.baidu.netdisk"
/* const-string/jumbo v10, "tv.danmaku.bili" */
final String v11 = "com.ss.android.article.news"; // const-string v11, "com.ss.android.article.news"
final String v12 = "com.baidu.searchbox"; // const-string v12, "com.baidu.searchbox"
final String v13 = "com.xingin.xhs"; // const-string v13, "com.xingin.xhs"
final String v14 = "com.dragon.read"; // const-string v14, "com.dragon.read"
final String v15 = "com.tencent.qqlive"; // const-string v15, "com.tencent.qqlive"
final String v16 = "com.ss.android.article.video"; // const-string v16, "com.ss.android.article.video"
final String v17 = "com.ss.android.article.lite"; // const-string v17, "com.ss.android.article.lite"
final String v18 = "com.duowan.kiwi"; // const-string v18, "com.duowan.kiwi"
final String v19 = "com.quark.browser"; // const-string v19, "com.quark.browser"
final String v20 = "air.tv.douyu.android"; // const-string v20, "air.tv.douyu.android"
/* filled-new-array/range {v1 ..v20}, [Ljava/lang/String; */
/* .line 310 */
java.util.Arrays .asList ( v1 );
/* invoke-direct {v0, v1}, Ljava/util/ArrayList;-><init>(Ljava/util/Collection;)V */
/* .line 334 */
/* new-instance v0, Ljava/util/ArrayList; */
final String v1 = "com.whatsapp"; // const-string v1, "com.whatsapp"
final String v2 = "com.facebook.katana"; // const-string v2, "com.facebook.katana"
final String v3 = "com.instagram.android"; // const-string v3, "com.instagram.android"
final String v4 = "org.telegram.messenger"; // const-string v4, "org.telegram.messenger"
final String v5 = "com.snapchat.android"; // const-string v5, "com.snapchat.android"
final String v6 = "cn.wps.moffice_eng"; // const-string v6, "cn.wps.moffice_eng"
final String v7 = "com.truecaller"; // const-string v7, "com.truecaller"
final String v8 = "com.spotify.music"; // const-string v8, "com.spotify.music"
final String v9 = "com.mxtech.videoplayer.ad"; // const-string v9, "com.mxtech.videoplayer.ad"
final String v10 = "com.twitter.android"; // const-string v10, "com.twitter.android"
final String v11 = "com.pinterest"; // const-string v11, "com.pinterest"
final String v12 = "in.mohalla.sharechat"; // const-string v12, "in.mohalla.sharechat"
final String v13 = "com.whatsapp.w4b"; // const-string v13, "com.whatsapp.w4b"
final String v14 = "com.amazon.mShop.android.shopping"; // const-string v14, "com.amazon.mShop.android.shopping"
final String v15 = "com.linkedin.android"; // const-string v15, "com.linkedin.android"
final String v16 = "in.mohalla.video"; // const-string v16, "in.mohalla.video"
final String v17 = "com.alibaba.aliexpresshd"; // const-string v17, "com.alibaba.aliexpresshd"
final String v18 = "com.camerasideas.instashot"; // const-string v18, "com.camerasideas.instashot"
final String v19 = "com.microsoft.office.outlook"; // const-string v19, "com.microsoft.office.outlook"
final String v20 = "com.shazam.android"; // const-string v20, "com.shazam.android"
final String v21 = "com.lazada.android"; // const-string v21, "com.lazada.android"
final String v22 = "com.zzkko"; // const-string v22, "com.zzkko"
final String v23 = "com.discord"; // const-string v23, "com.discord"
/* filled-new-array/range {v1 ..v23}, [Ljava/lang/String; */
/* .line 335 */
java.util.Arrays .asList ( v1 );
/* invoke-direct {v0, v1}, Ljava/util/ArrayList;-><init>(Ljava/util/Collection;)V */
/* .line 334 */
return;
} // .end method
public com.miui.server.turbosched.TurboSchedManagerService ( ) {
/* .locals 27 */
/* .param p1, "context" # Landroid/content/Context; */
/* .line 421 */
/* move-object/from16 v0, p0 */
/* move-object/from16 v1, p1 */
/* invoke-direct/range {p0 ..p0}, Lmiui/turbosched/ITurboSchedManager$Stub;-><init>()V */
/* .line 103 */
final String v2 = "/sys/module/metis/parameters/buffer_tx_count"; // const-string v2, "/sys/module/metis/parameters/buffer_tx_count"
this.BufferTxCountPath = v2;
/* .line 119 */
int v2 = 0; // const/4 v2, 0x0
/* iput-boolean v2, v0, Lcom/miui/server/turbosched/TurboSchedManagerService;->mDPSSupport:Z */
/* .line 120 */
final String v3 = "persist.sys.turbosched.dps.enable"; // const-string v3, "persist.sys.turbosched.dps.enable"
v3 = android.os.SystemProperties .getBoolean ( v3,v2 );
/* iput-boolean v3, v0, Lcom/miui/server/turbosched/TurboSchedManagerService;->mDPSEnable:Z */
/* .line 195 */
final String v3 = "persist.sys.turbosched.thermal_break.enable"; // const-string v3, "persist.sys.turbosched.thermal_break.enable"
v3 = android.os.SystemProperties .getBoolean ( v3,v2 );
/* iput-boolean v3, v0, Lcom/miui/server/turbosched/TurboSchedManagerService;->mThermalBreakEnabled:Z */
/* .line 204 */
/* new-instance v3, Landroid/util/LocalLog; */
/* invoke-direct {v3, v4}, Landroid/util/LocalLog;-><init>(I)V */
this.mHistoryLog = v3;
/* .line 206 */
final String v3 = "persist.sys.turbosched.enable"; // const-string v3, "persist.sys.turbosched.enable"
v3 = android.os.SystemProperties .getBoolean ( v3,v2 );
/* iput-boolean v3, v0, Lcom/miui/server/turbosched/TurboSchedManagerService;->mTurboEnabled:Z */
/* .line 207 */
final String v3 = "persist.sys.turbosched.enable_v2"; // const-string v3, "persist.sys.turbosched.enable_v2"
int v4 = 1; // const/4 v4, 0x1
v3 = android.os.SystemProperties .getBoolean ( v3,v4 );
/* iput-boolean v3, v0, Lcom/miui/server/turbosched/TurboSchedManagerService;->mTurboEnabledV2:Z */
/* .line 208 */
/* iput-boolean v2, v0, Lcom/miui/server/turbosched/TurboSchedManagerService;->mCloudControlEnabled:Z */
/* .line 209 */
/* iput-boolean v2, v0, Lcom/miui/server/turbosched/TurboSchedManagerService;->mTurboNodeExist:Z */
/* .line 210 */
int v3 = 0; // const/4 v3, 0x0
this.mHandlerThread = v3;
/* .line 211 */
this.mTurboSchedHandler = v3;
/* .line 212 */
this.mPm = v3;
/* .line 213 */
this.mSurfaceFlinger = v3;
/* .line 215 */
/* new-instance v5, Ljava/util/ArrayList; */
final String v6 = "com.miui.personalassistant"; // const-string v6, "com.miui.personalassistant"
final String v7 = "com.miui.home"; // const-string v7, "com.miui.home"
/* filled-new-array {v7, v6}, [Ljava/lang/String; */
/* .line 216 */
java.util.Arrays .asList ( v6 );
/* invoke-direct {v5, v6}, Ljava/util/ArrayList;-><init>(Ljava/util/Collection;)V */
this.mCallerAllowList = v5;
/* .line 217 */
/* new-instance v5, Ljava/util/ArrayList; */
final String v6 = "com.android.provision"; // const-string v6, "com.android.provision"
/* filled-new-array {v6}, [Ljava/lang/String; */
/* .line 218 */
java.util.Arrays .asList ( v6 );
/* invoke-direct {v5, v6}, Ljava/util/ArrayList;-><init>(Ljava/util/Collection;)V */
this.mStartUpAllowList = v5;
/* .line 219 */
/* iput-boolean v4, v0, Lcom/miui/server/turbosched/TurboSchedManagerService;->mInStartUpMode:Z */
/* .line 220 */
/* iput-boolean v2, v0, Lcom/miui/server/turbosched/TurboSchedManagerService;->mStartUpModeInitFinish:Z */
/* .line 221 */
/* iput-boolean v2, v0, Lcom/miui/server/turbosched/TurboSchedManagerService;->mKernelEnabelUpdate:Z */
/* .line 222 */
com.miui.server.turbosched.TurboSchedConfig .getPolicyList ( );
this.mPolicyListBackup = v5;
/* .line 224 */
/* iput-boolean v2, v0, Lcom/miui/server/turbosched/TurboSchedManagerService;->mIsOnScroll:Z */
/* .line 225 */
final String v5 = ""; // const-string v5, ""
this.mProcessName = v5;
/* .line 226 */
/* const-wide/16 v8, -0x1 */
/* iput-wide v8, v0, Lcom/miui/server/turbosched/TurboSchedManagerService;->mFgCorePid:J */
/* .line 227 */
/* iput-wide v8, v0, Lcom/miui/server/turbosched/TurboSchedManagerService;->mFgCoreUid:J */
/* .line 229 */
/* new-instance v6, Ljava/util/ArrayList; */
/* invoke-direct {v6}, Ljava/util/ArrayList;-><init>()V */
this.mTurboschedSdkTidsList = v6;
/* .line 230 */
/* new-instance v6, Ljava/lang/Object; */
/* invoke-direct {v6}, Ljava/lang/Object;-><init>()V */
this.mTurboschedSdkTidsLock = v6;
/* .line 231 */
/* iput-boolean v2, v0, Lcom/miui/server/turbosched/TurboSchedManagerService;->mRegistedForegroundReceiver:Z */
/* .line 232 */
final String v6 = "persist.sys.turbosched.receive.cloud.data"; // const-string v6, "persist.sys.turbosched.receive.cloud.data"
v6 = android.os.SystemProperties .getBoolean ( v6,v4 );
/* iput-boolean v6, v0, Lcom/miui/server/turbosched/TurboSchedManagerService;->mReceiveCloudData:Z */
/* .line 233 */
/* new-instance v6, Ljava/util/ArrayList; */
final String v8 = "ishtar"; // const-string v8, "ishtar"
final String v9 = "babylon"; // const-string v9, "babylon"
final String v10 = "nuwa"; // const-string v10, "nuwa"
final String v11 = "fuxi"; // const-string v11, "fuxi"
/* const-string/jumbo v12, "socrates" */
/* filled-new-array {v10, v11, v12, v8, v9}, [Ljava/lang/String; */
/* .line 234 */
java.util.Arrays .asList ( v8 );
/* invoke-direct {v6, v8}, Ljava/util/ArrayList;-><init>(Ljava/util/Collection;)V */
this.m8550Devices = v6;
/* .line 236 */
/* new-instance v6, Ljava/util/ArrayList; */
/* const-string/jumbo v8, "yudi" */
final String v9 = "marble"; // const-string v9, "marble"
/* const-string/jumbo v10, "thor" */
/* const-string/jumbo v11, "unicorn" */
final String v12 = "mayfly"; // const-string v12, "mayfly"
/* const-string/jumbo v13, "zizhan" */
/* filled-new-array/range {v8 ..v13}, [Ljava/lang/String; */
/* .line 237 */
java.util.Arrays .asList ( v8 );
/* invoke-direct {v6, v8}, Ljava/util/ArrayList;-><init>(Ljava/util/Collection;)V */
this.m8475Devices = v6;
/* .line 239 */
/* new-instance v6, Ljava/util/ArrayList; */
/* const-string/jumbo v8, "zeus" */
final String v9 = "cupid"; // const-string v9, "cupid"
/* filled-new-array {v8, v9}, [Ljava/lang/String; */
/* .line 240 */
java.util.Arrays .asList ( v8 );
/* invoke-direct {v6, v8}, Ljava/util/ArrayList;-><init>(Ljava/util/Collection;)V */
this.m8450Devices = v6;
/* .line 243 */
/* sget-boolean v6, Lmiui/os/Build;->IS_INTERNATIONAL_BUILD:Z */
/* if-nez v6, :cond_1 */
/* .line 244 */
final String v6 = "ro.miui.region"; // const-string v6, "ro.miui.region"
/* const-string/jumbo v8, "unknown" */
android.os.SystemProperties .get ( v6,v8 );
final String v8 = "CN"; // const-string v8, "CN"
v6 = (( java.lang.String ) v8 ).equals ( v6 ); // invoke-virtual {v8, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z
if ( v6 != null) { // if-eqz v6, :cond_1
v6 = com.miui.server.turbosched.TurboSchedManagerService.PR_NAME;
/* .line 245 */
final String v8 = "_"; // const-string v8, "_"
v6 = (( java.lang.String ) v6 ).contains ( v8 ); // invoke-virtual {v6, v8}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z
if ( v6 != null) { // if-eqz v6, :cond_0
v6 = com.miui.server.turbosched.TurboSchedManagerService.PR_NAME;
final String v8 = "_cn"; // const-string v8, "_cn"
v6 = (( java.lang.String ) v6 ).contains ( v8 ); // invoke-virtual {v6, v8}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z
/* if-nez v6, :cond_0 */
v6 = com.miui.server.turbosched.TurboSchedManagerService.PR_NAME;
final String v8 = "_CN"; // const-string v8, "_CN"
v6 = (( java.lang.String ) v6 ).contains ( v8 ); // invoke-virtual {v6, v8}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z
/* if-nez v6, :cond_0 */
} // :cond_0
/* move v6, v2 */
} // :cond_1
} // :goto_0
/* move v6, v4 */
} // :goto_1
/* iput-boolean v6, v0, Lcom/miui/server/turbosched/TurboSchedManagerService;->mIsGlobal:Z */
/* .line 247 */
final String v6 = "persist.sys.turbosched.link.enable"; // const-string v6, "persist.sys.turbosched.link.enable"
v6 = android.os.SystemProperties .getBoolean ( v6,v4 );
/* iput-boolean v6, v0, Lcom/miui/server/turbosched/TurboSchedManagerService;->mLinkEnable:Z */
/* .line 248 */
final String v6 = "persist.sys.turbosched.support.gaea"; // const-string v6, "persist.sys.turbosched.support.gaea"
v6 = android.os.SystemProperties .getBoolean ( v6,v2 );
/* iput-boolean v6, v0, Lcom/miui/server/turbosched/TurboSchedManagerService;->mSupportGaea:Z */
/* .line 249 */
final String v6 = "persist.sys.turbosched.gaea.enable"; // const-string v6, "persist.sys.turbosched.gaea.enable"
v6 = android.os.SystemProperties .getBoolean ( v6,v2 );
/* iput-boolean v6, v0, Lcom/miui/server/turbosched/TurboSchedManagerService;->mGaeaEnable:Z */
/* .line 250 */
final String v6 = "persist.sys.turbosched.enabletop20app"; // const-string v6, "persist.sys.turbosched.enabletop20app"
v2 = android.os.SystemProperties .getBoolean ( v6,v2 );
/* iput-boolean v2, v0, Lcom/miui/server/turbosched/TurboSchedManagerService;->mEnableTop20:Z */
/* .line 265 */
v2 = com.miui.server.turbosched.TurboSchedManagerService.DEFAULT_APP_LIST;
this.mCoreAppsPackageNameList = v2;
/* .line 362 */
/* new-instance v2, Ljava/util/ArrayList; */
final String v8 = "com.smile.gifmaker"; // const-string v8, "com.smile.gifmaker"
final String v9 = "com.kuaishou.nebula"; // const-string v9, "com.kuaishou.nebula"
final String v10 = "com.ss.android.ugc.aweme"; // const-string v10, "com.ss.android.ugc.aweme"
final String v11 = "com.tencent.mobileqq"; // const-string v11, "com.tencent.mobileqq"
final String v12 = "com.ss.android.article.news"; // const-string v12, "com.ss.android.article.news"
final String v13 = "com.ss.android.article.lite"; // const-string v13, "com.ss.android.article.lite"
final String v14 = "com.tencent.news"; // const-string v14, "com.tencent.news"
final String v15 = "com.alibaba.android.rimet"; // const-string v15, "com.alibaba.android.rimet"
final String v16 = "cn.xuexi.android"; // const-string v16, "cn.xuexi.android"
final String v17 = "com.zhihu.android"; // const-string v17, "com.zhihu.android"
final String v18 = "com.kugou.android"; // const-string v18, "com.kugou.android"
final String v19 = "com.hunantv.imgo.activity"; // const-string v19, "com.hunantv.imgo.activity"
final String v20 = "com.tencent.qqmusic"; // const-string v20, "com.tencent.qqmusic"
/* const-string/jumbo v21, "tv.danmaku.bili" */
final String v22 = "com.tencent.qqlive"; // const-string v22, "com.tencent.qqlive"
final String v23 = "com.UCMobile"; // const-string v23, "com.UCMobile"
final String v24 = "com.sina.weibo"; // const-string v24, "com.sina.weibo"
final String v25 = "com.tencent.mobileqq:qzone"; // const-string v25, "com.tencent.mobileqq:qzone"
final String v26 = "com.autonavi.minimap"; // const-string v26, "com.autonavi.minimap"
/* filled-new-array/range {v8 ..v26}, [Ljava/lang/String; */
/* .line 363 */
java.util.Arrays .asList ( v6 );
/* invoke-direct {v2, v6}, Ljava/util/ArrayList;-><init>(Ljava/util/Collection;)V */
this.mAppsLinkVipList = v2;
/* .line 373 */
int v2 = -1; // const/4 v2, -0x1
/* iput v2, v0, Lcom/miui/server/turbosched/TurboSchedManagerService;->mAppsLockContentionPid:I */
/* .line 374 */
/* new-instance v2, Ljava/util/ArrayList; */
/* filled-new-array {v7}, [Ljava/lang/String; */
/* .line 375 */
java.util.Arrays .asList ( v6 );
/* invoke-direct {v2, v6}, Ljava/util/ArrayList;-><init>(Ljava/util/Collection;)V */
this.mAppsLockContentionList = v2;
/* .line 381 */
/* iput-boolean v4, v0, Lcom/miui/server/turbosched/TurboSchedManagerService;->mScreenOn:Z */
/* .line 382 */
/* new-instance v2, Ljava/util/HashMap; */
/* invoke-direct {v2}, Ljava/util/HashMap;-><init>()V */
this.mBWFTidStartTimeMap = v2;
/* .line 383 */
/* new-instance v2, Ljava/util/HashMap; */
/* invoke-direct {v2}, Ljava/util/HashMap;-><init>()V */
this.mPriorityTidStartTimeMap = v2;
/* .line 384 */
this.enableGaeaAppProcessName = v5;
/* .line 385 */
this.enableLinkVipAppProcessName = v5;
/* .line 405 */
this.TurboSchedSDKTimer = v3;
/* .line 406 */
this.TurboSchedSDKTimerTask = v3;
/* .line 419 */
/* new-instance v2, Ljava/util/HashMap; */
/* invoke-direct {v2}, Ljava/util/HashMap;-><init>()V */
this.mTurboschedSDKParamMap = v2;
/* .line 673 */
/* new-instance v2, Lcom/miui/server/turbosched/TurboSchedManagerService$1; */
v4 = this.mTurboSchedHandler;
/* invoke-direct {v2, v0, v4}, Lcom/miui/server/turbosched/TurboSchedManagerService$1;-><init>(Lcom/miui/server/turbosched/TurboSchedManagerService;Landroid/os/Handler;)V */
this.mCloudSwichObserver = v2;
/* .line 684 */
/* new-instance v2, Lcom/miui/server/turbosched/TurboSchedManagerService$2; */
v4 = this.mTurboSchedHandler;
/* invoke-direct {v2, v0, v4}, Lcom/miui/server/turbosched/TurboSchedManagerService$2;-><init>(Lcom/miui/server/turbosched/TurboSchedManagerService;Landroid/os/Handler;)V */
this.mCloudTurboschedMiuiSdkObserver = v2;
/* .line 695 */
/* new-instance v2, Lcom/miui/server/turbosched/TurboSchedManagerService$3; */
v4 = this.mTurboSchedHandler;
/* invoke-direct {v2, v0, v4}, Lcom/miui/server/turbosched/TurboSchedManagerService$3;-><init>(Lcom/miui/server/turbosched/TurboSchedManagerService;Landroid/os/Handler;)V */
this.mCloudCoreAppOptimizerEnableObserver = v2;
/* .line 706 */
/* new-instance v2, Lcom/miui/server/turbosched/TurboSchedManagerService$4; */
v4 = this.mTurboSchedHandler;
/* invoke-direct {v2, v0, v4}, Lcom/miui/server/turbosched/TurboSchedManagerService$4;-><init>(Lcom/miui/server/turbosched/TurboSchedManagerService;Landroid/os/Handler;)V */
this.mCloudCoreTop20AppOptimizerEnableObserver = v2;
/* .line 715 */
/* new-instance v2, Lcom/miui/server/turbosched/TurboSchedManagerService$5; */
v4 = this.mTurboSchedHandler;
/* invoke-direct {v2, v0, v4}, Lcom/miui/server/turbosched/TurboSchedManagerService$5;-><init>(Lcom/miui/server/turbosched/TurboSchedManagerService;Landroid/os/Handler;)V */
this.mCloudAllowListObserver = v2;
/* .line 724 */
/* new-instance v2, Lcom/miui/server/turbosched/TurboSchedManagerService$6; */
v4 = this.mTurboSchedHandler;
/* invoke-direct {v2, v0, v4}, Lcom/miui/server/turbosched/TurboSchedManagerService$6;-><init>(Lcom/miui/server/turbosched/TurboSchedManagerService;Landroid/os/Handler;)V */
this.mCloudPolicyListObserver = v2;
/* .line 735 */
/* new-instance v2, Lcom/miui/server/turbosched/TurboSchedManagerService$7; */
v4 = this.mTurboSchedHandler;
/* invoke-direct {v2, v0, v4}, Lcom/miui/server/turbosched/TurboSchedManagerService$7;-><init>(Lcom/miui/server/turbosched/TurboSchedManagerService;Landroid/os/Handler;)V */
this.mCloudThermalBreakEnableObserver = v2;
/* .line 744 */
/* new-instance v2, Lcom/miui/server/turbosched/TurboSchedManagerService$8; */
v4 = this.mTurboSchedHandler;
/* invoke-direct {v2, v0, v4}, Lcom/miui/server/turbosched/TurboSchedManagerService$8;-><init>(Lcom/miui/server/turbosched/TurboSchedManagerService;Landroid/os/Handler;)V */
this.mCloudThermalBreakThresholdObserver = v2;
/* .line 753 */
/* new-instance v2, Lcom/miui/server/turbosched/TurboSchedManagerService$9; */
v4 = this.mTurboSchedHandler;
/* invoke-direct {v2, v0, v4}, Lcom/miui/server/turbosched/TurboSchedManagerService$9;-><init>(Lcom/miui/server/turbosched/TurboSchedManagerService;Landroid/os/Handler;)V */
this.mCloudLinkWhiteListObserver = v2;
/* .line 762 */
/* new-instance v2, Lcom/miui/server/turbosched/TurboSchedManagerService$10; */
v4 = this.mTurboSchedHandler;
/* invoke-direct {v2, v0, v4}, Lcom/miui/server/turbosched/TurboSchedManagerService$10;-><init>(Lcom/miui/server/turbosched/TurboSchedManagerService;Landroid/os/Handler;)V */
this.mCloudFrameTurboWhiteListObserver = v2;
/* .line 771 */
/* new-instance v2, Lcom/miui/server/turbosched/TurboSchedManagerService$11; */
v4 = this.mTurboSchedHandler;
/* invoke-direct {v2, v0, v4}, Lcom/miui/server/turbosched/TurboSchedManagerService$11;-><init>(Lcom/miui/server/turbosched/TurboSchedManagerService;Landroid/os/Handler;)V */
this.mCloudDpsEnableObserver = v2;
/* .line 1883 */
/* new-instance v2, Lcom/miui/server/turbosched/TurboSchedManagerService$12; */
/* invoke-direct {v2, v0}, Lcom/miui/server/turbosched/TurboSchedManagerService$12;-><init>(Lcom/miui/server/turbosched/TurboSchedManagerService;)V */
this.mProcessObserver = v2;
/* .line 422 */
this.mContext = v1;
/* .line 423 */
/* new-instance v2, Landroid/os/HandlerThread; */
/* const-string/jumbo v4, "turbosched" */
/* invoke-direct {v2, v4}, Landroid/os/HandlerThread;-><init>(Ljava/lang/String;)V */
this.mHandlerThread = v2;
/* .line 424 */
(( android.os.HandlerThread ) v2 ).start ( ); // invoke-virtual {v2}, Landroid/os/HandlerThread;->start()V
/* .line 425 */
/* new-instance v2, Lcom/miui/server/turbosched/TurboSchedManagerService$TurboSchedHandler; */
v4 = this.mHandlerThread;
(( android.os.HandlerThread ) v4 ).getLooper ( ); // invoke-virtual {v4}, Landroid/os/HandlerThread;->getLooper()Landroid/os/Looper;
/* invoke-direct {v2, v0, v4, v3}, Lcom/miui/server/turbosched/TurboSchedManagerService$TurboSchedHandler;-><init>(Lcom/miui/server/turbosched/TurboSchedManagerService;Landroid/os/Looper;Lcom/miui/server/turbosched/TurboSchedManagerService$TurboSchedHandler-IA;)V */
this.mTurboSchedHandler = v2;
/* .line 426 */
com.miui.server.turbosched.TurboSchedSceneManager .getInstance ( );
v3 = this.mHandlerThread;
(( android.os.HandlerThread ) v3 ).getLooper ( ); // invoke-virtual {v3}, Landroid/os/HandlerThread;->getLooper()Landroid/os/Looper;
(( com.miui.server.turbosched.TurboSchedSceneManager ) v2 ).initialize ( v1, v3 ); // invoke-virtual {v2, v1, v3}, Lcom/miui/server/turbosched/TurboSchedSceneManager;->initialize(Landroid/content/Context;Landroid/os/Looper;)V
/* .line 427 */
v2 = this.mContext;
(( android.content.Context ) v2 ).getPackageManager ( ); // invoke-virtual {v2}, Landroid/content/Context;->getPackageManager()Landroid/content/pm/PackageManager;
this.mPm = v2;
/* .line 428 */
android.app.ActivityManager .getService ( );
this.mActivityManagerService = v2;
/* .line 429 */
/* invoke-direct/range {p0 ..p0}, Lcom/miui/server/turbosched/TurboSchedManagerService;->onServiceStart()V */
/* .line 430 */
/* invoke-direct/range {p0 ..p0}, Lcom/miui/server/turbosched/TurboSchedManagerService;->registeForegroundReceiver()V */
/* .line 431 */
/* iget-boolean v2, v0, Lcom/miui/server/turbosched/TurboSchedManagerService;->mReceiveCloudData:Z */
if ( v2 != null) { // if-eqz v2, :cond_2
/* .line 432 */
/* invoke-direct/range {p0 ..p0}, Lcom/miui/server/turbosched/TurboSchedManagerService;->registerObserver()V */
/* .line 433 */
/* invoke-direct/range {p0 ..p0}, Lcom/miui/server/turbosched/TurboSchedManagerService;->updateCloudControlProp()V */
/* .line 435 */
} // :cond_2
/* invoke-direct/range {p0 ..p0}, Lcom/miui/server/turbosched/TurboSchedManagerService;->updateLocalProp()V */
/* .line 438 */
} // :goto_2
/* new-instance v2, Lcom/miui/server/turbosched/TurboSchedManagerService$TurboSchedBroadcastReceiver; */
/* invoke-direct {v2, v0}, Lcom/miui/server/turbosched/TurboSchedManagerService$TurboSchedBroadcastReceiver;-><init>(Lcom/miui/server/turbosched/TurboSchedManagerService;)V */
/* .line 440 */
/* const/16 v2, 0x28 */
java.lang.String .valueOf ( v2 );
/* .line 441 */
/* .local v2, "str":Ljava/lang/String; */
final String v3 = "B-Duration"; // const-string v3, "B-Duration"
/* const-string/jumbo v4, "sys/module/metis/parameters/mi_boost_duration" */
/* invoke-direct {v0, v3, v4, v2}, Lcom/miui/server/turbosched/TurboSchedManagerService;->writeTurboSchedNode(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Z */
/* .line 442 */
/* invoke-direct/range {p0 ..p0}, Lcom/miui/server/turbosched/TurboSchedManagerService;->initTopAppList()V */
/* .line 444 */
/* new-instance v3, Lcom/miui/server/turbosched/FrameTurboAction; */
/* invoke-direct {v3}, Lcom/miui/server/turbosched/FrameTurboAction;-><init>()V */
this.mFrameTurboAction = v3;
/* .line 445 */
v3 = this.mFrameTurboAppMap;
this.mFrameTurboAppMap = v3;
/* .line 447 */
/* new-instance v3, Lcom/miui/server/turbosched/GameTurboAction; */
/* invoke-direct {v3}, Lcom/miui/server/turbosched/GameTurboAction;-><init>()V */
this.mGameTurboAction = v3;
/* .line 450 */
/* invoke-direct/range {p0 ..p0}, Lcom/miui/server/turbosched/TurboSchedManagerService;->initStartUpMode()V */
/* .line 451 */
return;
} // .end method
private void addCoreApp ( java.util.List p0 ) {
/* .locals 4 */
/* .annotation system Ldalvik/annotation/Signature; */
/* value = { */
/* "(", */
/* "Ljava/util/List<", */
/* "Ljava/lang/String;", */
/* ">;)V" */
/* } */
} // .end annotation
/* .line 2099 */
/* .local p1, "coreAppList":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;" */
/* if-nez p1, :cond_0 */
/* .line 2100 */
return;
/* .line 2102 */
} // :cond_0
/* new-instance v0, Ljava/util/ArrayList; */
android.os.TurboSchedMonitor .getInstance ( );
(( android.os.TurboSchedMonitor ) v1 ).getCoreAppList ( ); // invoke-virtual {v1}, Landroid/os/TurboSchedMonitor;->getCoreAppList()Ljava/util/List;
/* invoke-direct {v0, v1}, Ljava/util/ArrayList;-><init>(Ljava/util/Collection;)V */
/* .line 2103 */
/* .local v0, "applist":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;" */
v2 = } // :goto_0
if ( v2 != null) { // if-eqz v2, :cond_2
/* check-cast v2, Ljava/lang/String; */
/* .line 2104 */
v3 = /* .local v2, "app":Ljava/lang/String; */
/* if-nez v3, :cond_1 */
/* .line 2105 */
/* .line 2107 */
} // .end local v2 # "app":Ljava/lang/String;
} // :cond_1
/* .line 2108 */
} // :cond_2
android.os.TurboSchedMonitor .getInstance ( );
v2 = this.mContext;
(( android.os.TurboSchedMonitor ) v1 ).setCoreAppList ( v2, v0 ); // invoke-virtual {v1, v2, v0}, Landroid/os/TurboSchedMonitor;->setCoreAppList(Landroid/content/Context;Ljava/util/List;)V
/* .line 2109 */
return;
} // .end method
private Boolean checkCallerPermmsion ( Integer p0, Integer p1 ) {
/* .locals 4 */
/* .param p1, "pid" # I */
/* .param p2, "uid" # I */
/* .line 1487 */
com.android.server.am.ProcessUtils .getPackageNameByPid ( p1 );
/* .line 1488 */
/* .local v0, "str":Ljava/lang/String; */
if ( p2 != null) { // if-eqz p2, :cond_2
if ( v0 != null) { // if-eqz v0, :cond_0
v1 = v1 = this.mCallerAllowList;
/* if-nez v1, :cond_2 */
} // :cond_0
v1 = /* invoke-direct {p0, v0, p2}, Lcom/miui/server/turbosched/TurboSchedManagerService;->isStartUpModeAllowed(Ljava/lang/String;I)Z */
if ( v1 != null) { // if-eqz v1, :cond_1
/* .line 1491 */
} // :cond_1
v1 = this.mHistoryLog;
/* new-instance v2, Ljava/lang/StringBuilder; */
/* invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V */
final String v3 = "TS [V] : caller is not allow: uid: "; // const-string v3, "TS [V] : caller is not allow: uid: "
(( java.lang.StringBuilder ) v2 ).append ( v3 ); // invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v2 ).append ( p2 ); // invoke-virtual {v2, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;
final String v3 = ", pkg: "; // const-string v3, ", pkg: "
(( java.lang.StringBuilder ) v2 ).append ( v3 ); // invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v2 ).append ( v0 ); // invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
final String v3 = ", pid: "; // const-string v3, ", pid: "
(( java.lang.StringBuilder ) v2 ).append ( v3 ); // invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v2 ).append ( p1 ); // invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v2 ).toString ( ); // invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
(( android.util.LocalLog ) v1 ).log ( v2 ); // invoke-virtual {v1, v2}, Landroid/util/LocalLog;->log(Ljava/lang/String;)V
/* .line 1492 */
int v1 = 0; // const/4 v1, 0x0
/* .line 1489 */
} // :cond_2
} // :goto_0
int v1 = 1; // const/4 v1, 0x1
} // .end method
private void delCoreApp ( java.util.List p0 ) {
/* .locals 4 */
/* .annotation system Ldalvik/annotation/Signature; */
/* value = { */
/* "(", */
/* "Ljava/util/List<", */
/* "Ljava/lang/String;", */
/* ">;)V" */
/* } */
} // .end annotation
/* .line 2112 */
/* .local p1, "coreAppList":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;" */
/* if-nez p1, :cond_0 */
/* .line 2113 */
return;
/* .line 2115 */
} // :cond_0
/* new-instance v0, Ljava/util/ArrayList; */
android.os.TurboSchedMonitor .getInstance ( );
(( android.os.TurboSchedMonitor ) v1 ).getCoreAppList ( ); // invoke-virtual {v1}, Landroid/os/TurboSchedMonitor;->getCoreAppList()Ljava/util/List;
/* invoke-direct {v0, v1}, Ljava/util/ArrayList;-><init>(Ljava/util/Collection;)V */
/* .line 2116 */
/* .local v0, "applist":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;" */
v2 = } // :goto_0
if ( v2 != null) { // if-eqz v2, :cond_2
/* check-cast v2, Ljava/lang/String; */
/* .line 2117 */
v3 = /* .local v2, "app":Ljava/lang/String; */
if ( v3 != null) { // if-eqz v3, :cond_1
/* .line 2118 */
/* .line 2120 */
} // .end local v2 # "app":Ljava/lang/String;
} // :cond_1
/* .line 2121 */
} // :cond_2
android.os.TurboSchedMonitor .getInstance ( );
v2 = this.mContext;
(( android.os.TurboSchedMonitor ) v1 ).setCoreAppList ( v2, v0 ); // invoke-virtual {v1, v2, v0}, Landroid/os/TurboSchedMonitor;->setCoreAppList(Landroid/content/Context;Ljava/util/List;)V
/* .line 2122 */
return;
} // .end method
private void dumpConfig ( java.io.PrintWriter p0 ) {
/* .locals 7 */
/* .param p1, "pw" # Ljava/io/PrintWriter; */
/* .line 1567 */
final String v0 = "--------------------current config-----------------------"; // const-string v0, "--------------------current config-----------------------"
(( java.io.PrintWriter ) p1 ).println ( v0 ); // invoke-virtual {p1, v0}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V
/* .line 1569 */
final String v0 = ""; // const-string v0, ""
/* .line 1570 */
/* .local v0, "metisVersionSuffix":Ljava/lang/String; */
v1 = this.mMetisVersion;
if ( v1 != null) { // if-eqz v1, :cond_0
v1 = (( java.lang.String ) v1 ).isEmpty ( ); // invoke-virtual {v1}, Ljava/lang/String;->isEmpty()Z
/* if-nez v1, :cond_0 */
/* .line 1571 */
/* new-instance v1, Ljava/lang/StringBuilder; */
/* invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V */
final String v2 = "-"; // const-string v2, "-"
(( java.lang.StringBuilder ) v1 ).append ( v2 ); // invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
v2 = this.mMetisVersion;
(( java.lang.StringBuilder ) v1 ).append ( v2 ); // invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v1 ).toString ( ); // invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
/* .line 1573 */
} // :cond_0
/* new-instance v1, Ljava/lang/StringBuilder; */
/* invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V */
final String v2 = "TurboSchedManagerService: v4.0.0"; // const-string v2, "TurboSchedManagerService: v4.0.0"
(( java.lang.StringBuilder ) v1 ).append ( v2 ); // invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v1 ).append ( v0 ); // invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v1 ).toString ( ); // invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
(( java.io.PrintWriter ) p1 ).println ( v1 ); // invoke-virtual {p1, v1}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V
/* .line 1574 */
/* new-instance v1, Ljava/lang/StringBuilder; */
/* invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V */
final String v2 = " disable cloud control : "; // const-string v2, " disable cloud control : "
(( java.lang.StringBuilder ) v1 ).append ( v2 ); // invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
/* iget-boolean v2, p0, Lcom/miui/server/turbosched/TurboSchedManagerService;->mReceiveCloudData:Z */
int v3 = 1; // const/4 v3, 0x1
/* xor-int/2addr v2, v3 */
(( java.lang.StringBuilder ) v1 ).append ( v2 ); // invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v1 ).toString ( ); // invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
(( java.io.PrintWriter ) p1 ).println ( v1 ); // invoke-virtual {p1, v1}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V
/* .line 1575 */
/* new-instance v1, Ljava/lang/StringBuilder; */
/* invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V */
final String v2 = " mTurboEnabled : "; // const-string v2, " mTurboEnabled : "
(( java.lang.StringBuilder ) v1 ).append ( v2 ); // invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
/* iget-boolean v2, p0, Lcom/miui/server/turbosched/TurboSchedManagerService;->mTurboEnabled:Z */
(( java.lang.StringBuilder ) v1 ).append ( v2 ); // invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;
final String v2 = ", mTurboEnabledV2: "; // const-string v2, ", mTurboEnabledV2: "
(( java.lang.StringBuilder ) v1 ).append ( v2 ); // invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
/* iget-boolean v2, p0, Lcom/miui/server/turbosched/TurboSchedManagerService;->mTurboEnabledV2:Z */
(( java.lang.StringBuilder ) v1 ).append ( v2 ); // invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;
final String v2 = ", mCloudControlEnabled : "; // const-string v2, ", mCloudControlEnabled : "
(( java.lang.StringBuilder ) v1 ).append ( v2 ); // invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
/* iget-boolean v2, p0, Lcom/miui/server/turbosched/TurboSchedManagerService;->mCloudControlEnabled:Z */
(( java.lang.StringBuilder ) v1 ).append ( v2 ); // invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v1 ).toString ( ); // invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
(( java.io.PrintWriter ) p1 ).println ( v1 ); // invoke-virtual {p1, v1}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V
/* .line 1576 */
/* new-instance v1, Ljava/lang/StringBuilder; */
/* invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V */
final String v2 = " CoreAppOptimizerEnabled: "; // const-string v2, " CoreAppOptimizerEnabled: "
(( java.lang.StringBuilder ) v1 ).append ( v2 ); // invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
android.os.TurboSchedMonitor .getInstance ( );
v2 = (( android.os.TurboSchedMonitor ) v2 ).isCoreAppOptimizerEnabled ( ); // invoke-virtual {v2}, Landroid/os/TurboSchedMonitor;->isCoreAppOptimizerEnabled()Z
(( java.lang.StringBuilder ) v1 ).append ( v2 ); // invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;
final String v2 = ", DebugMode: "; // const-string v2, ", DebugMode: "
(( java.lang.StringBuilder ) v1 ).append ( v2 ); // invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
/* .line 1577 */
android.os.TurboSchedMonitor .getInstance ( );
v2 = (( android.os.TurboSchedMonitor ) v2 ).isDebugMode ( ); // invoke-virtual {v2}, Landroid/os/TurboSchedMonitor;->isDebugMode()Z
(( java.lang.StringBuilder ) v1 ).append ( v2 ); // invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v1 ).toString ( ); // invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
/* .line 1576 */
(( java.io.PrintWriter ) p1 ).println ( v1 ); // invoke-virtual {p1, v1}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V
/* .line 1578 */
/* new-instance v1, Ljava/lang/StringBuilder; */
/* invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V */
final String v2 = " CoreTop20AppOptimizerEnabled: "; // const-string v2, " CoreTop20AppOptimizerEnabled: "
(( java.lang.StringBuilder ) v1 ).append ( v2 ); // invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
/* iget-boolean v2, p0, Lcom/miui/server/turbosched/TurboSchedManagerService;->mEnableTop20:Z */
(( java.lang.StringBuilder ) v1 ).append ( v2 ); // invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v1 ).toString ( ); // invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
(( java.io.PrintWriter ) p1 ).println ( v1 ); // invoke-virtual {p1, v1}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V
/* .line 1579 */
/* new-instance v1, Ljava/lang/StringBuilder; */
/* invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V */
final String v2 = " mThermalBreakEnabled: "; // const-string v2, " mThermalBreakEnabled: "
(( java.lang.StringBuilder ) v1 ).append ( v2 ); // invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
/* iget-boolean v2, p0, Lcom/miui/server/turbosched/TurboSchedManagerService;->mThermalBreakEnabled:Z */
(( java.lang.StringBuilder ) v1 ).append ( v2 ); // invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;
final String v2 = ", mLinkEnable: "; // const-string v2, ", mLinkEnable: "
(( java.lang.StringBuilder ) v1 ).append ( v2 ); // invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
/* iget-boolean v2, p0, Lcom/miui/server/turbosched/TurboSchedManagerService;->mLinkEnable:Z */
(( java.lang.StringBuilder ) v1 ).append ( v2 ); // invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;
final String v2 = ", mSupportGaea: "; // const-string v2, ", mSupportGaea: "
(( java.lang.StringBuilder ) v1 ).append ( v2 ); // invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
/* iget-boolean v2, p0, Lcom/miui/server/turbosched/TurboSchedManagerService;->mSupportGaea:Z */
(( java.lang.StringBuilder ) v1 ).append ( v2 ); // invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;
final String v2 = ", mGaeaEnable: "; // const-string v2, ", mGaeaEnable: "
(( java.lang.StringBuilder ) v1 ).append ( v2 ); // invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
/* iget-boolean v2, p0, Lcom/miui/server/turbosched/TurboSchedManagerService;->mGaeaEnable:Z */
(( java.lang.StringBuilder ) v1 ).append ( v2 ); // invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v1 ).toString ( ); // invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
(( java.io.PrintWriter ) p1 ).println ( v1 ); // invoke-virtual {p1, v1}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V
/* .line 1580 */
/* new-instance v1, Ljava/lang/StringBuilder; */
/* invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V */
final String v2 = " TurboSchedConfig.mPolicyList : "; // const-string v2, " TurboSchedConfig.mPolicyList : "
(( java.lang.StringBuilder ) v1 ).append ( v2 ); // invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
final String v2 = ","; // const-string v2, ","
com.miui.server.turbosched.TurboSchedConfig .getPolicyList ( );
java.lang.String .join ( v2,v4 );
(( java.lang.StringBuilder ) v1 ).append ( v2 ); // invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v1 ).toString ( ); // invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
(( java.io.PrintWriter ) p1 ).println ( v1 ); // invoke-virtual {p1, v1}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V
/* .line 1581 */
/* new-instance v1, Ljava/lang/StringBuilder; */
/* invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V */
final String v2 = " Caller allow list : "; // const-string v2, " Caller allow list : "
(( java.lang.StringBuilder ) v1 ).append ( v2 ); // invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
v2 = this.mCallerAllowList;
java.util.Arrays .toString ( v2 );
(( java.lang.StringBuilder ) v1 ).append ( v2 ); // invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v1 ).toString ( ); // invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
(( java.io.PrintWriter ) p1 ).println ( v1 ); // invoke-virtual {p1, v1}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V
/* .line 1582 */
/* new-instance v1, Ljava/lang/StringBuilder; */
/* invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V */
final String v2 = " Lock Contention Enabled : "; // const-string v2, " Lock Contention Enabled : "
(( java.lang.StringBuilder ) v1 ).append ( v2 ); // invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
android.os.TurboSchedMonitor .getInstance ( );
v2 = (( android.os.TurboSchedMonitor ) v2 ).isCoreAppOptimizerEnabled ( ); // invoke-virtual {v2}, Landroid/os/TurboSchedMonitor;->isCoreAppOptimizerEnabled()Z
(( java.lang.StringBuilder ) v1 ).append ( v2 ); // invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v1 ).toString ( ); // invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
(( java.io.PrintWriter ) p1 ).println ( v1 ); // invoke-virtual {p1, v1}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V
/* .line 1583 */
/* new-instance v1, Ljava/lang/StringBuilder; */
/* invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V */
final String v2 = " mDPSEnable : "; // const-string v2, " mDPSEnable : "
(( java.lang.StringBuilder ) v1 ).append ( v2 ); // invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
/* iget-boolean v2, p0, Lcom/miui/server/turbosched/TurboSchedManagerService;->mDPSEnable:Z */
(( java.lang.StringBuilder ) v1 ).append ( v2 ); // invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v1 ).toString ( ); // invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
(( java.io.PrintWriter ) p1 ).println ( v1 ); // invoke-virtual {p1, v1}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V
/* .line 1585 */
final String v1 = "--------------------environment config-----------------------"; // const-string v1, "--------------------environment config-----------------------"
(( java.io.PrintWriter ) p1 ).println ( v1 ); // invoke-virtual {p1, v1}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V
/* .line 1586 */
/* new-instance v1, Ljava/lang/StringBuilder; */
/* invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V */
final String v2 = " cores sched: "; // const-string v2, " cores sched: "
(( java.lang.StringBuilder ) v1 ).append ( v2 ); // invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
final String v2 = "persist.sys.miui_animator_sched.big_prime_cores"; // const-string v2, "persist.sys.miui_animator_sched.big_prime_cores"
final String v4 = "not set"; // const-string v4, "not set"
android.os.SystemProperties .get ( v2,v4 );
(( java.lang.StringBuilder ) v1 ).append ( v2 ); // invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v1 ).toString ( ); // invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
(( java.io.PrintWriter ) p1 ).println ( v1 ); // invoke-virtual {p1, v1}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V
/* .line 1587 */
/* new-instance v1, Ljava/lang/StringBuilder; */
/* invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V */
final String v2 = " [PRendering] enable:"; // const-string v2, " [PRendering] enable:"
(( java.lang.StringBuilder ) v1 ).append ( v2 ); // invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
final String v2 = "ro.vendor.perf.scroll_opt"; // const-string v2, "ro.vendor.perf.scroll_opt"
android.os.SystemProperties .get ( v2,v4 );
(( java.lang.StringBuilder ) v1 ).append ( v2 ); // invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
final String v2 = ", h-a value: "; // const-string v2, ", h-a value: "
(( java.lang.StringBuilder ) v1 ).append ( v2 ); // invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
final String v2 = "ro.vendor.perf.scroll_opt.heavy_app"; // const-string v2, "ro.vendor.perf.scroll_opt.heavy_app"
android.os.SystemProperties .get ( v2,v4 );
(( java.lang.StringBuilder ) v1 ).append ( v2 ); // invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v1 ).toString ( ); // invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
(( java.io.PrintWriter ) p1 ).println ( v1 ); // invoke-virtual {p1, v1}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V
/* .line 1589 */
final String v1 = "--------------------kernel config-----------------------"; // const-string v1, "--------------------kernel config-----------------------"
(( java.io.PrintWriter ) p1 ).println ( v1 ); // invoke-virtual {p1, v1}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V
/* .line 1590 */
/* iget-boolean v1, p0, Lcom/miui/server/turbosched/TurboSchedManagerService;->mTurboNodeExist:Z */
final String v2 = " isFileAccess = "; // const-string v2, " isFileAccess = "
if ( v1 != null) { // if-eqz v1, :cond_1
/* .line 1591 */
/* new-instance v1, Ljava/lang/StringBuilder; */
/* invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V */
(( java.lang.StringBuilder ) v1 ).append ( v2 ); // invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
final String v2 = "/sys/module/metis/parameters/mi_viptask"; // const-string v2, "/sys/module/metis/parameters/mi_viptask"
v2 = com.miui.server.turbosched.TurboSchedUtil .checkFileAccess ( v2 );
(( java.lang.StringBuilder ) v1 ).append ( v2 ); // invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v1 ).toString ( ); // invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
(( java.io.PrintWriter ) p1 ).println ( v1 ); // invoke-virtual {p1, v1}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V
/* .line 1593 */
} // :cond_1
/* new-instance v1, Ljava/lang/StringBuilder; */
/* invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V */
(( java.lang.StringBuilder ) v1 ).append ( v2 ); // invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
final String v2 = "/sys/module/migt/parameters/mi_viptask"; // const-string v2, "/sys/module/migt/parameters/mi_viptask"
v2 = com.miui.server.turbosched.TurboSchedUtil .checkFileAccess ( v2 );
(( java.lang.StringBuilder ) v1 ).append ( v2 ); // invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v1 ).toString ( ); // invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
(( java.io.PrintWriter ) p1 ).println ( v1 ); // invoke-virtual {p1, v1}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V
/* .line 1596 */
} // :goto_0
/* new-instance v1, Ljava/lang/StringBuilder; */
/* invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V */
final String v2 = " metis dev access = "; // const-string v2, " metis dev access = "
(( java.lang.StringBuilder ) v1 ).append ( v2 ); // invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
final String v2 = "/dev/metis"; // const-string v2, "/dev/metis"
v2 = com.miui.server.turbosched.TurboSchedUtil .checkFileAccess ( v2 );
(( java.lang.StringBuilder ) v1 ).append ( v2 ); // invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v1 ).toString ( ); // invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
(( java.io.PrintWriter ) p1 ).println ( v1 ); // invoke-virtual {p1, v1}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V
/* .line 1597 */
final String v1 = "/sys/module/metis/parameters/add_mi_viptask_enqueue_boost"; // const-string v1, "/sys/module/metis/parameters/add_mi_viptask_enqueue_boost"
v1 = com.miui.server.turbosched.TurboSchedUtil .checkFileAccess ( v1 );
int v2 = 0; // const/4 v2, 0x0
if ( v1 != null) { // if-eqz v1, :cond_2
final String v1 = "/sys/module/metis/parameters/del_mi_viptask_enqueue_boost"; // const-string v1, "/sys/module/metis/parameters/del_mi_viptask_enqueue_boost"
v1 = com.miui.server.turbosched.TurboSchedUtil .checkFileAccess ( v1 );
if ( v1 != null) { // if-eqz v1, :cond_2
/* move v1, v3 */
} // :cond_2
/* move v1, v2 */
/* .line 1598 */
/* .local v1, "isBWFPolicyAccess":Z */
} // :goto_1
/* new-instance v4, Ljava/lang/StringBuilder; */
/* invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V */
final String v5 = " bwf policy access = "; // const-string v5, " bwf policy access = "
(( java.lang.StringBuilder ) v4 ).append ( v5 ); // invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v4 ).append ( v1 ); // invoke-virtual {v4, v1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v4 ).toString ( ); // invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
(( java.io.PrintWriter ) p1 ).println ( v4 ); // invoke-virtual {p1, v4}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V
/* .line 1599 */
final String v4 = "/sys/module/metis/parameters/add_mi_viptask_sched_priority"; // const-string v4, "/sys/module/metis/parameters/add_mi_viptask_sched_priority"
v4 = com.miui.server.turbosched.TurboSchedUtil .checkFileAccess ( v4 );
if ( v4 != null) { // if-eqz v4, :cond_3
final String v4 = "/sys/module/metis/parameters/del_mi_viptask_sched_priority"; // const-string v4, "/sys/module/metis/parameters/del_mi_viptask_sched_priority"
v4 = com.miui.server.turbosched.TurboSchedUtil .checkFileAccess ( v4 );
if ( v4 != null) { // if-eqz v4, :cond_3
/* move v4, v3 */
} // :cond_3
/* move v4, v2 */
/* .line 1600 */
/* .local v4, "isPriorityPolicyAccess":Z */
} // :goto_2
/* new-instance v5, Ljava/lang/StringBuilder; */
/* invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V */
final String v6 = " priority policy access = "; // const-string v6, " priority policy access = "
(( java.lang.StringBuilder ) v5 ).append ( v6 ); // invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v5 ).append ( v4 ); // invoke-virtual {v5, v4}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v5 ).toString ( ); // invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
(( java.io.PrintWriter ) p1 ).println ( v5 ); // invoke-virtual {p1, v5}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V
/* .line 1601 */
final String v5 = "/sys/module/metis/parameters/add_mi_viptask_sched_lit_core"; // const-string v5, "/sys/module/metis/parameters/add_mi_viptask_sched_lit_core"
v5 = com.miui.server.turbosched.TurboSchedUtil .checkFileAccess ( v5 );
if ( v5 != null) { // if-eqz v5, :cond_4
final String v5 = "/sys/module/metis/parameters/del_mi_viptask_sched_lit_core"; // const-string v5, "/sys/module/metis/parameters/del_mi_viptask_sched_lit_core"
v5 = com.miui.server.turbosched.TurboSchedUtil .checkFileAccess ( v5 );
if ( v5 != null) { // if-eqz v5, :cond_4
} // :cond_4
/* move v3, v2 */
} // :goto_3
/* move v2, v3 */
/* .line 1602 */
/* .local v2, "isLittleCorePolicyAccess":Z */
/* new-instance v3, Ljava/lang/StringBuilder; */
/* invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V */
final String v5 = " little core policy access = "; // const-string v5, " little core policy access = "
(( java.lang.StringBuilder ) v3 ).append ( v5 ); // invoke-virtual {v3, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v3 ).append ( v2 ); // invoke-virtual {v3, v2}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v3 ).toString ( ); // invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
(( java.io.PrintWriter ) p1 ).println ( v3 ); // invoke-virtual {p1, v3}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V
/* .line 1604 */
/* new-instance v3, Ljava/lang/StringBuilder; */
/* invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V */
final String v5 = " thermal break enable access = "; // const-string v5, " thermal break enable access = "
(( java.lang.StringBuilder ) v3 ).append ( v5 ); // invoke-virtual {v3, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
final String v5 = "/sys/module/metis/parameters/is_break_enable"; // const-string v5, "/sys/module/metis/parameters/is_break_enable"
v5 = com.miui.server.turbosched.TurboSchedUtil .checkFileAccess ( v5 );
(( java.lang.StringBuilder ) v3 ).append ( v5 ); // invoke-virtual {v3, v5}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v3 ).toString ( ); // invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
(( java.io.PrintWriter ) p1 ).println ( v3 ); // invoke-virtual {p1, v3}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V
/* .line 1605 */
/* new-instance v3, Ljava/lang/StringBuilder; */
/* invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V */
final String v5 = " thermal break access = "; // const-string v5, " thermal break access = "
(( java.lang.StringBuilder ) v3 ).append ( v5 ); // invoke-virtual {v3, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
final String v5 = "/sys/class/thermal/thermal_message/boost"; // const-string v5, "/sys/class/thermal/thermal_message/boost"
v5 = com.miui.server.turbosched.TurboSchedUtil .checkFileAccess ( v5 );
(( java.lang.StringBuilder ) v3 ).append ( v5 ); // invoke-virtual {v3, v5}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v3 ).toString ( ); // invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
(( java.io.PrintWriter ) p1 ).println ( v3 ); // invoke-virtual {p1, v3}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V
/* .line 1606 */
/* new-instance v3, Ljava/lang/StringBuilder; */
/* invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V */
final String v5 = " link access ="; // const-string v5, " link access ="
(( java.lang.StringBuilder ) v3 ).append ( v5 ); // invoke-virtual {v3, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
final String v5 = "/sys/module/metis/parameters/vip_link_enable"; // const-string v5, "/sys/module/metis/parameters/vip_link_enable"
v5 = com.miui.server.turbosched.TurboSchedUtil .checkFileAccess ( v5 );
(( java.lang.StringBuilder ) v3 ).append ( v5 ); // invoke-virtual {v3, v5}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v3 ).toString ( ); // invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
(( java.io.PrintWriter ) p1 ).println ( v3 ); // invoke-virtual {p1, v3}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V
/* .line 1607 */
/* new-instance v3, Ljava/lang/StringBuilder; */
/* invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V */
final String v5 = " gaea access ="; // const-string v5, " gaea access ="
(( java.lang.StringBuilder ) v3 ).append ( v5 ); // invoke-virtual {v3, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
final String v5 = "/sys/module/gaea/parameters/mi_gaea_enable"; // const-string v5, "/sys/module/gaea/parameters/mi_gaea_enable"
v5 = com.miui.server.turbosched.TurboSchedUtil .checkFileAccess ( v5 );
(( java.lang.StringBuilder ) v3 ).append ( v5 ); // invoke-virtual {v3, v5}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v3 ).toString ( ); // invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
(( java.io.PrintWriter ) p1 ).println ( v3 ); // invoke-virtual {p1, v3}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V
/* .line 1608 */
return;
} // .end method
private void enableFrameBoostInKernel ( Boolean p0 ) {
/* .locals 3 */
/* .param p1, "enable" # Z */
/* .line 1958 */
final String v0 = "/sys/module/metis/parameters/mi_fboost_enable"; // const-string v0, "/sys/module/metis/parameters/mi_fboost_enable"
final String v1 = "TB-Enabled"; // const-string v1, "TB-Enabled"
if ( p1 != null) { // if-eqz p1, :cond_0
/* .line 1959 */
final String v2 = "1"; // const-string v2, "1"
v0 = /* invoke-direct {p0, v1, v0, v2}, Lcom/miui/server/turbosched/TurboSchedManagerService;->writeTurboSchedNode(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Z */
/* .local v0, "isSuccess":Z */
/* .line 1961 */
} // .end local v0 # "isSuccess":Z
} // :cond_0
final String v2 = "0"; // const-string v2, "0"
v0 = /* invoke-direct {p0, v1, v0, v2}, Lcom/miui/server/turbosched/TurboSchedManagerService;->writeTurboSchedNode(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Z */
/* .line 1963 */
/* .restart local v0 # "isSuccess":Z */
} // :goto_0
android.os.TurboSchedMonitor .getInstance ( );
v1 = (( android.os.TurboSchedMonitor ) v1 ).isDebugMode ( ); // invoke-virtual {v1}, Landroid/os/TurboSchedMonitor;->isDebugMode()Z
if ( v1 != null) { // if-eqz v1, :cond_1
/* .line 1964 */
/* new-instance v1, Ljava/lang/StringBuilder; */
/* invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V */
final String v2 = "enableFrameBoostInKernel, isSuccess:"; // const-string v2, "enableFrameBoostInKernel, isSuccess:"
(( java.lang.StringBuilder ) v1 ).append ( v2 ); // invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v1 ).append ( v0 ); // invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v1 ).toString ( ); // invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
final String v2 = "TurboSchedManagerService"; // const-string v2, "TurboSchedManagerService"
android.util.Slog .i ( v2,v1 );
/* .line 1965 */
} // :cond_1
return;
} // .end method
private void enableGaea ( Boolean p0 ) {
/* .locals 4 */
/* .param p1, "bEnable" # Z */
/* .line 1539 */
int v0 = 0; // const/4 v0, 0x0
/* .line 1541 */
/* .local v0, "isSuccess":Z */
/* iput-boolean p1, p0, Lcom/miui/server/turbosched/TurboSchedManagerService;->mGaeaEnable:Z */
/* .line 1542 */
final String v1 = "persist.sys.turbosched.gaea.enable"; // const-string v1, "persist.sys.turbosched.gaea.enable"
java.lang.Boolean .toString ( p1 );
android.os.SystemProperties .set ( v1,v2 );
/* .line 1543 */
/* if-nez p1, :cond_0 */
/* .line 1544 */
final String v1 = "/sys/module/gaea/parameters/mi_gaea_enable"; // const-string v1, "/sys/module/gaea/parameters/mi_gaea_enable"
final String v2 = "0"; // const-string v2, "0"
final String v3 = "GAEA"; // const-string v3, "GAEA"
v0 = /* invoke-direct {p0, v3, v1, v2}, Lcom/miui/server/turbosched/TurboSchedManagerService;->writeTurboSchedNode(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Z */
/* .line 1545 */
android.os.TurboSchedMonitor .getInstance ( );
v1 = (( android.os.TurboSchedMonitor ) v1 ).isDebugMode ( ); // invoke-virtual {v1}, Landroid/os/TurboSchedMonitor;->isDebugMode()Z
if ( v1 != null) { // if-eqz v1, :cond_0
/* if-nez v0, :cond_0 */
/* .line 1546 */
final String v1 = "TurboSchedManagerService"; // const-string v1, "TurboSchedManagerService"
final String v2 = "failed to write gaea path node!"; // const-string v2, "failed to write gaea path node!"
android.util.Slog .w ( v1,v2 );
/* .line 1548 */
} // :cond_0
return;
} // .end method
private void enableTurboSched ( Boolean p0 ) {
/* .locals 2 */
/* .param p1, "enable" # Z */
/* .line 1988 */
/* iput-boolean p1, p0, Lcom/miui/server/turbosched/TurboSchedManagerService;->mTurboEnabled:Z */
/* .line 1989 */
final String v0 = "persist.sys.turbosched.enable"; // const-string v0, "persist.sys.turbosched.enable"
java.lang.Boolean .toString ( p1 );
android.os.SystemProperties .set ( v0,v1 );
/* .line 1990 */
return;
} // .end method
private void exitStartUpMode ( ) {
/* .locals 2 */
/* .line 475 */
int v0 = 1; // const/4 v0, 0x1
/* iput-boolean v0, p0, Lcom/miui/server/turbosched/TurboSchedManagerService;->mStartUpModeInitFinish:Z */
/* .line 476 */
int v0 = 0; // const/4 v0, 0x0
/* iput-boolean v0, p0, Lcom/miui/server/turbosched/TurboSchedManagerService;->mInStartUpMode:Z */
/* .line 477 */
/* iget-boolean v1, p0, Lcom/miui/server/turbosched/TurboSchedManagerService;->mKernelEnabelUpdate:Z */
/* if-nez v1, :cond_0 */
/* .line 478 */
/* invoke-direct {p0, v0}, Lcom/miui/server/turbosched/TurboSchedManagerService;->enableFrameBoostInKernel(Z)V */
/* .line 480 */
} // :cond_0
v0 = this.mPolicyListBackup;
com.miui.server.turbosched.TurboSchedConfig .setPolicyList ( v0 );
/* .line 481 */
return;
} // .end method
private Integer getBoardSensorTemperature ( ) {
/* .locals 8 */
/* .line 2067 */
final String v0 = "getBoardSensorTemperature failed : "; // const-string v0, "getBoardSensorTemperature failed : "
final String v1 = "TurboSchedManagerService"; // const-string v1, "TurboSchedManagerService"
int v2 = 0; // const/4 v2, 0x0
/* .line 2068 */
/* .local v2, "sensorTemp":I */
int v3 = 0; // const/4 v3, 0x0
/* .line 2070 */
/* .local v3, "fis":Ljava/io/FileInputStream; */
try { // :try_start_0
/* new-instance v4, Ljava/io/FileInputStream; */
final String v5 = "/sys/class/thermal/thermal_message/board_sensor_temp"; // const-string v5, "/sys/class/thermal/thermal_message/board_sensor_temp"
/* invoke-direct {v4, v5}, Ljava/io/FileInputStream;-><init>(Ljava/lang/String;)V */
/* move-object v3, v4 */
/* .line 2071 */
/* const/16 v4, 0xa */
/* new-array v4, v4, [B */
/* .line 2072 */
/* .local v4, "buffer":[B */
v5 = (( java.io.FileInputStream ) v3 ).read ( v4 ); // invoke-virtual {v3, v4}, Ljava/io/FileInputStream;->read([B)I
/* .line 2073 */
/* .local v5, "read":I */
/* if-lez v5, :cond_0 */
/* .line 2074 */
/* new-instance v6, Ljava/lang/String; */
int v7 = 0; // const/4 v7, 0x0
/* invoke-direct {v6, v4, v7, v5}, Ljava/lang/String;-><init>([BII)V */
/* .line 2075 */
/* .local v6, "str":Ljava/lang/String; */
(( java.lang.String ) v6 ).trim ( ); // invoke-virtual {v6}, Ljava/lang/String;->trim()Ljava/lang/String;
v7 = java.lang.Integer .parseInt ( v7 );
/* div-int/lit16 v7, v7, 0x3e8 */
/* :try_end_0 */
/* .catch Ljava/lang/Exception; {:try_start_0 ..:try_end_0} :catch_1 */
/* .catchall {:try_start_0 ..:try_end_0} :catchall_0 */
/* move v2, v7 */
/* .line 2080 */
} // .end local v4 # "buffer":[B
} // .end local v5 # "read":I
} // .end local v6 # "str":Ljava/lang/String;
} // :cond_0
/* nop */
/* .line 2082 */
try { // :try_start_1
(( java.io.FileInputStream ) v3 ).close ( ); // invoke-virtual {v3}, Ljava/io/FileInputStream;->close()V
/* :try_end_1 */
/* .catch Ljava/io/IOException; {:try_start_1 ..:try_end_1} :catch_0 */
/* .line 2085 */
} // :goto_0
/* .line 2083 */
/* :catch_0 */
/* move-exception v4 */
/* .line 2084 */
/* .local v4, "e":Ljava/io/IOException; */
/* new-instance v5, Ljava/lang/StringBuilder; */
/* invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V */
} // :goto_1
(( java.lang.StringBuilder ) v5 ).append ( v0 ); // invoke-virtual {v5, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.io.IOException ) v4 ).getMessage ( ); // invoke-virtual {v4}, Ljava/io/IOException;->getMessage()Ljava/lang/String;
(( java.lang.StringBuilder ) v0 ).append ( v5 ); // invoke-virtual {v0, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v0 ).toString ( ); // invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
android.util.Slog .e ( v1,v0 );
} // .end local v4 # "e":Ljava/io/IOException;
/* .line 2080 */
/* :catchall_0 */
/* move-exception v4 */
/* .line 2077 */
/* :catch_1 */
/* move-exception v4 */
/* .line 2078 */
/* .local v4, "e":Ljava/lang/Exception; */
try { // :try_start_2
/* new-instance v5, Ljava/lang/StringBuilder; */
/* invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V */
(( java.lang.StringBuilder ) v5 ).append ( v0 ); // invoke-virtual {v5, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.Exception ) v4 ).getMessage ( ); // invoke-virtual {v4}, Ljava/lang/Exception;->getMessage()Ljava/lang/String;
(( java.lang.StringBuilder ) v5 ).append ( v6 ); // invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v5 ).toString ( ); // invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
android.util.Slog .e ( v1,v5 );
/* :try_end_2 */
/* .catchall {:try_start_2 ..:try_end_2} :catchall_0 */
/* .line 2080 */
/* nop */
} // .end local v4 # "e":Ljava/lang/Exception;
if ( v3 != null) { // if-eqz v3, :cond_1
/* .line 2082 */
try { // :try_start_3
(( java.io.FileInputStream ) v3 ).close ( ); // invoke-virtual {v3}, Ljava/io/FileInputStream;->close()V
/* :try_end_3 */
/* .catch Ljava/io/IOException; {:try_start_3 ..:try_end_3} :catch_2 */
/* .line 2083 */
/* :catch_2 */
/* move-exception v4 */
/* .line 2084 */
/* .local v4, "e":Ljava/io/IOException; */
/* new-instance v5, Ljava/lang/StringBuilder; */
/* invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V */
/* .line 2088 */
} // .end local v4 # "e":Ljava/io/IOException;
} // :cond_1
} // :goto_2
/* .line 2080 */
} // :goto_3
if ( v3 != null) { // if-eqz v3, :cond_2
/* .line 2082 */
try { // :try_start_4
(( java.io.FileInputStream ) v3 ).close ( ); // invoke-virtual {v3}, Ljava/io/FileInputStream;->close()V
/* :try_end_4 */
/* .catch Ljava/io/IOException; {:try_start_4 ..:try_end_4} :catch_3 */
/* .line 2085 */
/* .line 2083 */
/* :catch_3 */
/* move-exception v5 */
/* .line 2084 */
/* .local v5, "e":Ljava/io/IOException; */
/* new-instance v6, Ljava/lang/StringBuilder; */
/* invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V */
(( java.lang.StringBuilder ) v6 ).append ( v0 ); // invoke-virtual {v6, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.io.IOException ) v5 ).getMessage ( ); // invoke-virtual {v5}, Ljava/io/IOException;->getMessage()Ljava/lang/String;
(( java.lang.StringBuilder ) v0 ).append ( v6 ); // invoke-virtual {v0, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v0 ).toString ( ); // invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
android.util.Slog .e ( v1,v0 );
/* .line 2087 */
} // .end local v5 # "e":Ljava/io/IOException;
} // :cond_2
} // :goto_4
/* throw v4 */
} // .end method
private Integer getBufferTxCount ( ) {
/* .locals 5 */
/* .line 1439 */
int v0 = 0; // const/4 v0, 0x0
/* .line 1440 */
/* .local v0, "data":Landroid/os/Parcel; */
int v1 = 0; // const/4 v1, 0x0
/* .line 1442 */
/* .local v1, "reply":Landroid/os/Parcel; */
try { // :try_start_0
v2 = this.mSurfaceFlinger;
/* if-nez v2, :cond_0 */
/* .line 1443 */
final String v2 = "SurfaceFlinger"; // const-string v2, "SurfaceFlinger"
android.os.ServiceManager .getService ( v2 );
this.mSurfaceFlinger = v2;
/* .line 1445 */
} // :cond_0
v2 = this.mSurfaceFlinger;
if ( v2 != null) { // if-eqz v2, :cond_3
/* .line 1446 */
android.os.Parcel .obtain ( );
/* move-object v1, v2 */
/* .line 1447 */
android.os.Parcel .obtain ( );
/* move-object v0, v2 */
/* .line 1448 */
final String v2 = "android.ui.ISurfaceComposer"; // const-string v2, "android.ui.ISurfaceComposer"
(( android.os.Parcel ) v0 ).writeInterfaceToken ( v2 ); // invoke-virtual {v0, v2}, Landroid/os/Parcel;->writeInterfaceToken(Ljava/lang/String;)V
/* .line 1449 */
v2 = this.mSurfaceFlinger;
/* const/16 v3, 0x3f3 */
int v4 = 0; // const/4 v4, 0x0
/* .line 1450 */
v2 = (( android.os.Parcel ) v1 ).readInt ( ); // invoke-virtual {v1}, Landroid/os/Parcel;->readInt()I
/* :try_end_0 */
/* .catch Landroid/os/RemoteException; {:try_start_0 ..:try_end_0} :catch_0 */
/* .catchall {:try_start_0 ..:try_end_0} :catchall_0 */
/* .line 1455 */
if ( v0 != null) { // if-eqz v0, :cond_1
/* .line 1456 */
(( android.os.Parcel ) v0 ).recycle ( ); // invoke-virtual {v0}, Landroid/os/Parcel;->recycle()V
/* .line 1458 */
} // :cond_1
if ( v1 != null) { // if-eqz v1, :cond_2
/* .line 1459 */
(( android.os.Parcel ) v1 ).recycle ( ); // invoke-virtual {v1}, Landroid/os/Parcel;->recycle()V
/* .line 1450 */
} // :cond_2
/* .line 1455 */
} // :cond_3
if ( v0 != null) { // if-eqz v0, :cond_4
/* .line 1456 */
(( android.os.Parcel ) v0 ).recycle ( ); // invoke-virtual {v0}, Landroid/os/Parcel;->recycle()V
/* .line 1458 */
} // :cond_4
if ( v1 != null) { // if-eqz v1, :cond_6
/* .line 1459 */
} // :goto_0
(( android.os.Parcel ) v1 ).recycle ( ); // invoke-virtual {v1}, Landroid/os/Parcel;->recycle()V
/* .line 1455 */
/* :catchall_0 */
/* move-exception v2 */
/* .line 1452 */
/* :catch_0 */
/* move-exception v2 */
/* .line 1453 */
/* .local v2, "e":Landroid/os/RemoteException; */
try { // :try_start_1
final String v3 = "TurboSchedManagerService"; // const-string v3, "TurboSchedManagerService"
final String v4 = "Failed to get Buffer-Tx count"; // const-string v4, "Failed to get Buffer-Tx count"
android.util.Slog .e ( v3,v4 );
/* :try_end_1 */
/* .catchall {:try_start_1 ..:try_end_1} :catchall_0 */
/* .line 1455 */
/* nop */
} // .end local v2 # "e":Landroid/os/RemoteException;
if ( v0 != null) { // if-eqz v0, :cond_5
/* .line 1456 */
(( android.os.Parcel ) v0 ).recycle ( ); // invoke-virtual {v0}, Landroid/os/Parcel;->recycle()V
/* .line 1458 */
} // :cond_5
if ( v1 != null) { // if-eqz v1, :cond_6
/* .line 1459 */
/* .line 1463 */
} // :cond_6
} // :goto_1
int v2 = -1; // const/4 v2, -0x1
/* .line 1455 */
} // :goto_2
if ( v0 != null) { // if-eqz v0, :cond_7
/* .line 1456 */
(( android.os.Parcel ) v0 ).recycle ( ); // invoke-virtual {v0}, Landroid/os/Parcel;->recycle()V
/* .line 1458 */
} // :cond_7
if ( v1 != null) { // if-eqz v1, :cond_8
/* .line 1459 */
(( android.os.Parcel ) v1 ).recycle ( ); // invoke-virtual {v1}, Landroid/os/Parcel;->recycle()V
/* .line 1461 */
} // :cond_8
/* throw v2 */
} // .end method
private java.lang.String getPackageNameFromUid ( Integer p0 ) {
/* .locals 3 */
/* .param p1, "uid" # I */
/* .line 1497 */
int v0 = 0; // const/4 v0, 0x0
/* .line 1499 */
/* .local v0, "packName":Ljava/lang/String; */
v1 = this.mPm;
if ( v1 != null) { // if-eqz v1, :cond_0
/* .line 1500 */
(( android.content.pm.PackageManager ) v1 ).getNameForUid ( p1 ); // invoke-virtual {v1, p1}, Landroid/content/pm/PackageManager;->getNameForUid(I)Ljava/lang/String;
/* .line 1503 */
} // :cond_0
/* if-nez v0, :cond_1 */
/* .line 1504 */
/* new-instance v1, Ljava/lang/StringBuilder; */
/* invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V */
final String v2 = "get caller pkgname failed uid = "; // const-string v2, "get caller pkgname failed uid = "
(( java.lang.StringBuilder ) v1 ).append ( v2 ); // invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v1 ).append ( p1 ); // invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v1 ).toString ( ); // invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
final String v2 = "TurboSchedManagerService"; // const-string v2, "TurboSchedManagerService"
android.util.Slog .d ( v2,v1 );
/* .line 1506 */
} // :cond_1
} // .end method
private java.lang.String getProcessNameByPid ( Integer p0 ) {
/* .locals 1 */
/* .param p1, "pid" # I */
/* .line 1510 */
int v0 = 0; // const/4 v0, 0x0
/* .line 1511 */
/* .local v0, "processName":Ljava/lang/String; */
com.android.server.am.ProcessUtils .getProcessNameByPid ( p1 );
/* .line 1512 */
} // .end method
private java.lang.String getThermalBreakString ( ) {
/* .locals 4 */
/* .line 2057 */
v0 = /* invoke-direct {p0}, Lcom/miui/server/turbosched/TurboSchedManagerService;->getBoardSensorTemperature()I */
/* .line 2058 */
/* .local v0, "temperatue":I */
v1 = com.miui.server.turbosched.TurboSchedManagerService.TEMPREATURE_THROSHOLD_BOOST_STRING_MAP;
v2 = } // :goto_0
if ( v2 != null) { // if-eqz v2, :cond_1
/* check-cast v2, Ljava/util/Map$Entry; */
/* .line 2059 */
/* .local v2, "entry":Ljava/util/Map$Entry;, "Ljava/util/Map$Entry<Ljava/lang/Integer;Ljava/lang/String;>;" */
/* check-cast v3, Ljava/lang/Integer; */
v3 = (( java.lang.Integer ) v3 ).intValue ( ); // invoke-virtual {v3}, Ljava/lang/Integer;->intValue()I
/* if-lt v0, v3, :cond_0 */
/* .line 2060 */
/* check-cast v1, Ljava/lang/String; */
/* .line 2062 */
} // .end local v2 # "entry":Ljava/util/Map$Entry;, "Ljava/util/Map$Entry<Ljava/lang/Integer;Ljava/lang/String;>;"
} // :cond_0
/* .line 2063 */
} // :cond_1
final String v1 = " cpu4:2419200 cpu7:2841600"; // const-string v1, " cpu4:2419200 cpu7:2841600"
} // .end method
private java.util.List initFrameTurboAppList ( ) {
/* .locals 4 */
/* .annotation system Ldalvik/annotation/Signature; */
/* value = { */
/* "()", */
/* "Ljava/util/List<", */
/* "Ljava/lang/String;", */
/* ">;" */
/* } */
} // .end annotation
/* .line 2133 */
/* new-instance v0, Ljava/util/ArrayList; */
/* invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V */
/* .line 2134 */
/* .local v0, "appList":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;" */
final String v1 = "persist.sys.turbosched.frame_turbo.apps"; // const-string v1, "persist.sys.turbosched.frame_turbo.apps"
final String v2 = ""; // const-string v2, ""
android.os.SystemProperties .get ( v1,v2 );
/* .line 2135 */
/* .local v1, "appListStr":Ljava/lang/String; */
if ( v1 != null) { // if-eqz v1, :cond_0
v2 = (( java.lang.String ) v1 ).isEmpty ( ); // invoke-virtual {v1}, Ljava/lang/String;->isEmpty()Z
/* if-nez v2, :cond_0 */
/* .line 2136 */
/* new-instance v2, Ljava/util/ArrayList; */
final String v3 = ","; // const-string v3, ","
(( java.lang.String ) v1 ).split ( v3 ); // invoke-virtual {v1, v3}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;
java.util.Arrays .asList ( v3 );
/* invoke-direct {v2, v3}, Ljava/util/ArrayList;-><init>(Ljava/util/Collection;)V */
/* move-object v0, v2 */
/* .line 2138 */
} // :cond_0
/* new-instance v2, Ljava/lang/StringBuilder; */
/* invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V */
final String v3 = "initFrameTurboAppList: "; // const-string v3, "initFrameTurboAppList: "
(( java.lang.StringBuilder ) v2 ).append ( v3 ); // invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v2 ).append ( v0 ); // invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v2 ).toString ( ); // invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
final String v3 = "TurboSchedManagerService"; // const-string v3, "TurboSchedManagerService"
android.util.Slog .d ( v3,v2 );
/* .line 2139 */
} // .end method
private void initStartUpMode ( ) {
/* .locals 1 */
/* .line 464 */
int v0 = 1; // const/4 v0, 0x1
/* iput-boolean v0, p0, Lcom/miui/server/turbosched/TurboSchedManagerService;->mTurboEnabledV2:Z */
/* .line 466 */
/* invoke-direct {p0, v0}, Lcom/miui/server/turbosched/TurboSchedManagerService;->enableFrameBoostInKernel(Z)V */
/* .line 468 */
/* iget-boolean v0, p0, Lcom/miui/server/turbosched/TurboSchedManagerService;->mRegistedForegroundReceiver:Z */
/* if-nez v0, :cond_0 */
/* .line 469 */
/* invoke-direct {p0}, Lcom/miui/server/turbosched/TurboSchedManagerService;->registeForegroundReceiver()V */
/* .line 471 */
} // :cond_0
v0 = com.miui.server.turbosched.TurboSchedConfig.POLICY_ALL;
com.miui.server.turbosched.TurboSchedConfig .setPolicyList ( v0 );
/* .line 472 */
return;
} // .end method
private void initTopAppList ( ) {
/* .locals 4 */
/* .line 2125 */
v0 = this.mContext;
(( android.content.Context ) v0 ).getContentResolver ( ); // invoke-virtual {v0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;
/* const-string/jumbo v1, "turbo_sched_core_app_list" */
android.provider.Settings$System .getString ( v0,v1 );
/* .line 2126 */
/* .local v0, "appListStr":Ljava/lang/String; */
if ( v0 != null) { // if-eqz v0, :cond_0
/* .line 2127 */
final String v1 = ","; // const-string v1, ","
(( java.lang.String ) v0 ).split ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;
java.util.Arrays .asList ( v1 );
this.mCoreAppsPackageNameList = v1;
/* .line 2129 */
} // :cond_0
android.os.TurboSchedMonitor .getInstance ( );
v2 = this.mContext;
v3 = this.mCoreAppsPackageNameList;
(( android.os.TurboSchedMonitor ) v1 ).setCoreAppList ( v2, v3 ); // invoke-virtual {v1, v2, v3}, Landroid/os/TurboSchedMonitor;->setCoreAppList(Landroid/content/Context;Ljava/util/List;)V
/* .line 2130 */
return;
} // .end method
private Boolean isFgDrawingFrame ( Integer[] p0 ) {
/* .locals 5 */
/* .param p1, "tids" # [I */
/* .line 1292 */
int v0 = 0; // const/4 v0, 0x0
/* .local v0, "i":I */
} // :goto_0
/* array-length v1, p1 */
/* if-ge v0, v1, :cond_1 */
/* .line 1293 */
/* aget v1, p1, v0 */
/* int-to-long v1, v1 */
/* iget-wide v3, p0, Lcom/miui/server/turbosched/TurboSchedManagerService;->mFgCorePid:J */
/* cmp-long v1, v1, v3 */
/* if-nez v1, :cond_0 */
/* .line 1294 */
int v1 = 1; // const/4 v1, 0x1
/* .line 1292 */
} // :cond_0
/* add-int/lit8 v0, v0, 0x1 */
/* .line 1297 */
} // .end local v0 # "i":I
} // :cond_1
int v0 = 0; // const/4 v0, 0x0
} // .end method
private Boolean isForeground ( java.lang.String p0 ) {
/* .locals 1 */
/* .param p1, "packageName" # Ljava/lang/String; */
/* .line 2183 */
v0 = this.mProcessName;
v0 = (( java.lang.String ) p1 ).equals ( v0 ); // invoke-virtual {p1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z
} // .end method
private Boolean isStartUpModeAllowed ( java.lang.String p0, Integer p1 ) {
/* .locals 3 */
/* .param p1, "packageName" # Ljava/lang/String; */
/* .param p2, "uid" # I */
/* .line 1467 */
/* iget-boolean v0, p0, Lcom/miui/server/turbosched/TurboSchedManagerService;->mInStartUpMode:Z */
int v1 = 0; // const/4 v1, 0x0
/* if-nez v0, :cond_0 */
/* .line 1468 */
/* .line 1471 */
} // :cond_0
int v0 = 1; // const/4 v0, 0x1
if ( p1 != null) { // if-eqz p1, :cond_1
v2 = v2 = this.mStartUpAllowList;
if ( v2 != null) { // if-eqz v2, :cond_1
/* .line 1472 */
/* .line 1475 */
} // :cond_1
/* if-nez p2, :cond_2 */
/* .line 1476 */
/* .line 1479 */
} // :cond_2
/* const/16 v2, 0x3e8 */
/* if-ne p2, v2, :cond_3 */
/* .line 1480 */
/* .line 1483 */
} // :cond_3
} // .end method
private Boolean isTurboEnabled ( ) {
/* .locals 1 */
/* .line 484 */
/* iget-boolean v0, p0, Lcom/miui/server/turbosched/TurboSchedManagerService;->mTurboEnabled:Z */
if ( v0 != null) { // if-eqz v0, :cond_0
/* .line 485 */
int v0 = 1; // const/4 v0, 0x1
/* .line 487 */
} // :cond_0
/* iget-boolean v0, p0, Lcom/miui/server/turbosched/TurboSchedManagerService;->mTurboEnabledV2:Z */
} // .end method
private void metisFrameBoost ( Integer p0 ) {
/* .locals 5 */
/* .param p1, "boostMs" # I */
/* .line 1521 */
android.os.TurboSchedMonitor .getInstance ( );
v0 = (( android.os.TurboSchedMonitor ) v0 ).isCoreAppOptimizerEnabled ( ); // invoke-virtual {v0}, Landroid/os/TurboSchedMonitor;->isCoreAppOptimizerEnabled()Z
if ( v0 != null) { // if-eqz v0, :cond_2
final String v0 = "/dev/metis"; // const-string v0, "/dev/metis"
v0 = com.miui.server.turbosched.TurboSchedUtil .checkFileAccess ( v0 );
if ( v0 != null) { // if-eqz v0, :cond_2
/* .line 1522 */
v0 = com.miui.server.turbosched.TurboSchedManagerService.mTurboLock;
/* monitor-enter v0 */
/* .line 1523 */
try { // :try_start_0
final String v1 = "/dev/metis"; // const-string v1, "/dev/metis"
v1 = android.os.NativeTurboSchedManager .nativeOpenDevice ( v1 );
/* .line 1524 */
/* .local v1, "handle":I */
/* if-ltz v1, :cond_1 */
/* .line 1525 */
android.os.TurboSchedMonitor .getInstance ( );
v2 = (( android.os.TurboSchedMonitor ) v2 ).isDebugMode ( ); // invoke-virtual {v2}, Landroid/os/TurboSchedMonitor;->isDebugMode()Z
/* const-wide/16 v3, 0x40 */
if ( v2 != null) { // if-eqz v2, :cond_0
/* .line 1526 */
final String v2 = "metisFrameBoost"; // const-string v2, "metisFrameBoost"
android.os.Trace .traceBegin ( v3,v4,v2 );
/* .line 1528 */
} // :cond_0
int v2 = 6; // const/4 v2, 0x6
android.os.NativeTurboSchedManager .nativeIoctlDevice ( v1,v2 );
/* .line 1529 */
android.os.NativeTurboSchedManager .nativeCloseDevice ( v1 );
/* .line 1531 */
android.os.TurboSchedMonitor .getInstance ( );
v2 = (( android.os.TurboSchedMonitor ) v2 ).isDebugMode ( ); // invoke-virtual {v2}, Landroid/os/TurboSchedMonitor;->isDebugMode()Z
if ( v2 != null) { // if-eqz v2, :cond_1
/* .line 1532 */
android.os.Trace .traceEnd ( v3,v4 );
/* .line 1534 */
} // .end local v1 # "handle":I
} // :cond_1
/* monitor-exit v0 */
/* :catchall_0 */
/* move-exception v1 */
/* monitor-exit v0 */
/* :try_end_0 */
/* .catchall {:try_start_0 ..:try_end_0} :catchall_0 */
/* throw v1 */
/* .line 1536 */
} // :cond_2
} // :goto_0
return;
} // .end method
private void onServiceStart ( ) {
/* .locals 1 */
/* .line 454 */
final String v0 = "/sys/module/metis/parameters/mi_viptask"; // const-string v0, "/sys/module/metis/parameters/mi_viptask"
v0 = com.miui.server.turbosched.TurboSchedUtil .checkFileAccess ( v0 );
/* iput-boolean v0, p0, Lcom/miui/server/turbosched/TurboSchedManagerService;->mTurboNodeExist:Z */
/* .line 455 */
android.os.TurboSchedMonitor .getInstance ( );
(( android.os.TurboSchedMonitor ) v0 ).getBoostDuration ( ); // invoke-virtual {v0}, Landroid/os/TurboSchedMonitor;->getBoostDuration()Ljava/util/List;
this.mBoostDuration = v0;
/* .line 456 */
final String v0 = "/sys/module/metis/parameters/version"; // const-string v0, "/sys/module/metis/parameters/version"
com.miui.server.turbosched.TurboSchedUtil .readValueFromFile ( v0 );
this.mMetisVersion = v0;
/* .line 457 */
if ( v0 != null) { // if-eqz v0, :cond_0
/* .line 458 */
int v0 = 1; // const/4 v0, 0x1
/* iput-boolean v0, p0, Lcom/miui/server/turbosched/TurboSchedManagerService;->mDPSSupport:Z */
/* .line 460 */
} // :cond_0
return;
} // .end method
private Boolean parseDumpCommand ( java.io.FileDescriptor p0, java.io.PrintWriter p1, java.lang.String[] p2 ) {
/* .locals 11 */
/* .param p1, "fd" # Ljava/io/FileDescriptor; */
/* .param p2, "pw" # Ljava/io/PrintWriter; */
/* .param p3, "args" # [Ljava/lang/String; */
/* .line 1611 */
/* array-length v0, p3 */
int v1 = 2; // const/4 v1, 0x2
int v2 = 0; // const/4 v2, 0x0
int v3 = 1; // const/4 v3, 0x1
int v4 = 3; // const/4 v4, 0x3
/* if-lt v0, v4, :cond_0 */
/* const-string/jumbo v0, "setAppFrameDelay" */
/* aget-object v5, p3, v2 */
v0 = (( java.lang.String ) v0 ).equals ( v5 ); // invoke-virtual {v0, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z
if ( v0 != null) { // if-eqz v0, :cond_0
/* .line 1612 */
/* aget-object v0, p3, v3 */
v0 = java.lang.Integer .parseInt ( v0 );
/* .line 1613 */
/* .local v0, "appType":I */
/* aget-object v2, p3, v1 */
v2 = java.lang.Float .parseFloat ( v2 );
/* .line 1614 */
/* .local v2, "frameDelayTH":F */
android.os.TurboSchedMonitor .getInstance ( );
(( android.os.TurboSchedMonitor ) v4 ).setCoreAppFrameDealyThreshold ( v0, v2 ); // invoke-virtual {v4, v0, v2}, Landroid/os/TurboSchedMonitor;->setCoreAppFrameDealyThreshold(IF)V
/* .line 1615 */
/* new-instance v4, Ljava/lang/StringBuilder; */
/* invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V */
final String v5 = " AppType: "; // const-string v5, " AppType: "
(( java.lang.StringBuilder ) v4 ).append ( v5 ); // invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
/* aget-object v5, p3, v3 */
(( java.lang.StringBuilder ) v4 ).append ( v5 ); // invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
final String v5 = " frame delay TH: "; // const-string v5, " frame delay TH: "
(( java.lang.StringBuilder ) v4 ).append ( v5 ); // invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
/* aget-object v1, p3, v1 */
(( java.lang.StringBuilder ) v4 ).append ( v1 ); // invoke-virtual {v4, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v1 ).toString ( ); // invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
/* invoke-direct {p0, p2, v1}, Lcom/miui/server/turbosched/TurboSchedManagerService;->printCommandResult(Ljava/io/PrintWriter;Ljava/lang/String;)V */
/* .line 1616 */
/* .line 1619 */
} // .end local v0 # "appType":I
} // .end local v2 # "frameDelayTH":F
} // :cond_0
/* array-length v0, p3 */
final String v5 = " Duration: "; // const-string v5, " Duration: "
/* if-lt v0, v4, :cond_1 */
/* const-string/jumbo v0, "setBoostDuration" */
/* aget-object v6, p3, v2 */
v0 = (( java.lang.String ) v0 ).equals ( v6 ); // invoke-virtual {v0, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z
if ( v0 != null) { // if-eqz v0, :cond_1
/* .line 1620 */
/* aget-object v0, p3, v3 */
v0 = java.lang.Integer .parseInt ( v0 );
/* .line 1621 */
/* .local v0, "boostType":I */
/* aget-object v2, p3, v1 */
java.lang.Long .parseLong ( v2 );
/* move-result-wide v6 */
/* .line 1622 */
/* .local v6, "time":J */
android.os.TurboSchedMonitor .getInstance ( );
(( android.os.TurboSchedMonitor ) v2 ).setBoostDuration ( v0, v6, v7 ); // invoke-virtual {v2, v0, v6, v7}, Landroid/os/TurboSchedMonitor;->setBoostDuration(IJ)V
/* .line 1623 */
/* new-instance v2, Ljava/lang/StringBuilder; */
/* invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V */
final String v4 = " BoostType: "; // const-string v4, " BoostType: "
(( java.lang.StringBuilder ) v2 ).append ( v4 ); // invoke-virtual {v2, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
/* aget-object v4, p3, v3 */
(( java.lang.StringBuilder ) v2 ).append ( v4 ); // invoke-virtual {v2, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v2 ).append ( v5 ); // invoke-virtual {v2, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
/* aget-object v1, p3, v1 */
(( java.lang.StringBuilder ) v2 ).append ( v1 ); // invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v1 ).toString ( ); // invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
/* invoke-direct {p0, p2, v1}, Lcom/miui/server/turbosched/TurboSchedManagerService;->printCommandResult(Ljava/io/PrintWriter;Ljava/lang/String;)V */
/* .line 1624 */
/* .line 1627 */
} // .end local v0 # "boostType":I
} // .end local v6 # "time":J
} // :cond_1
/* array-length v0, p3 */
/* if-lt v0, v1, :cond_2 */
/* const-string/jumbo v0, "setDebugMode" */
/* aget-object v6, p3, v2 */
v0 = (( java.lang.String ) v0 ).equals ( v6 ); // invoke-virtual {v0, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z
if ( v0 != null) { // if-eqz v0, :cond_2
/* .line 1628 */
/* aget-object v0, p3, v3 */
v0 = java.lang.Integer .parseInt ( v0 );
/* .line 1629 */
/* .local v0, "debugMode":I */
android.os.TurboSchedMonitor .getInstance ( );
(( android.os.TurboSchedMonitor ) v1 ).setDebugMode ( v0 ); // invoke-virtual {v1, v0}, Landroid/os/TurboSchedMonitor;->setDebugMode(I)V
/* .line 1630 */
/* new-instance v1, Ljava/lang/StringBuilder; */
/* invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V */
final String v2 = " setDebugMode: "; // const-string v2, " setDebugMode: "
(( java.lang.StringBuilder ) v1 ).append ( v2 ); // invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
/* aget-object v2, p3, v3 */
(( java.lang.StringBuilder ) v1 ).append ( v2 ); // invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v1 ).toString ( ); // invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
/* invoke-direct {p0, p2, v1}, Lcom/miui/server/turbosched/TurboSchedManagerService;->printCommandResult(Ljava/io/PrintWriter;Ljava/lang/String;)V */
/* .line 1631 */
/* .line 1634 */
} // .end local v0 # "debugMode":I
} // :cond_2
/* array-length v0, p3 */
/* if-lt v0, v1, :cond_4 */
final String v0 = "disableCloudControl"; // const-string v0, "disableCloudControl"
/* aget-object v6, p3, v2 */
v0 = (( java.lang.String ) v0 ).equals ( v6 ); // invoke-virtual {v0, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z
if ( v0 != null) { // if-eqz v0, :cond_4
/* .line 1635 */
/* aget-object v0, p3, v3 */
v0 = java.lang.Integer .parseInt ( v0 );
/* .line 1636 */
/* .local v0, "disableCloudControl":I */
/* if-lez v0, :cond_3 */
} // :cond_3
/* move v2, v3 */
} // :goto_0
/* iput-boolean v2, p0, Lcom/miui/server/turbosched/TurboSchedManagerService;->mReceiveCloudData:Z */
/* .line 1637 */
final String v1 = "persist.sys.turbosched.receive.cloud.data"; // const-string v1, "persist.sys.turbosched.receive.cloud.data"
java.lang.Boolean .toString ( v2 );
android.os.SystemProperties .set ( v1,v2 );
/* .line 1638 */
/* new-instance v1, Ljava/lang/StringBuilder; */
/* invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V */
final String v2 = " disableCloudControl: "; // const-string v2, " disableCloudControl: "
(( java.lang.StringBuilder ) v1 ).append ( v2 ); // invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
/* aget-object v2, p3, v3 */
(( java.lang.StringBuilder ) v1 ).append ( v2 ); // invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v1 ).toString ( ); // invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
/* invoke-direct {p0, p2, v1}, Lcom/miui/server/turbosched/TurboSchedManagerService;->printCommandResult(Ljava/io/PrintWriter;Ljava/lang/String;)V */
/* .line 1639 */
/* .line 1642 */
} // .end local v0 # "disableCloudControl":I
} // :cond_4
/* array-length v0, p3 */
/* if-lt v0, v1, :cond_6 */
final String v0 = "enableTop20App"; // const-string v0, "enableTop20App"
/* aget-object v6, p3, v2 */
v0 = (( java.lang.String ) v0 ).equals ( v6 ); // invoke-virtual {v0, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z
if ( v0 != null) { // if-eqz v0, :cond_6
/* .line 1643 */
/* aget-object v0, p3, v3 */
v0 = java.lang.Integer .parseInt ( v0 );
/* .line 1644 */
/* .local v0, "enableInt":I */
/* if-lez v0, :cond_5 */
/* move v2, v3 */
} // :cond_5
/* move v1, v2 */
/* .line 1645 */
/* .local v1, "enableBool":Z */
/* invoke-direct {p0, v1}, Lcom/miui/server/turbosched/TurboSchedManagerService;->syncTop20AppList(Z)V */
/* .line 1646 */
/* new-instance v2, Ljava/lang/StringBuilder; */
/* invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V */
final String v4 = " enableTop20App: "; // const-string v4, " enableTop20App: "
(( java.lang.StringBuilder ) v2 ).append ( v4 ); // invoke-virtual {v2, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
/* aget-object v4, p3, v3 */
(( java.lang.StringBuilder ) v2 ).append ( v4 ); // invoke-virtual {v2, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v2 ).toString ( ); // invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
/* invoke-direct {p0, p2, v2}, Lcom/miui/server/turbosched/TurboSchedManagerService;->printCommandResult(Ljava/io/PrintWriter;Ljava/lang/String;)V */
/* .line 1647 */
/* .line 1650 */
} // .end local v0 # "enableInt":I
} // .end local v1 # "enableBool":Z
} // :cond_6
/* array-length v0, p3 */
/* if-lt v0, v1, :cond_10 */
final String v0 = "enableCoreAppOptimizer"; // const-string v0, "enableCoreAppOptimizer"
/* aget-object v6, p3, v2 */
v0 = (( java.lang.String ) v0 ).equals ( v6 ); // invoke-virtual {v0, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z
if ( v0 != null) { // if-eqz v0, :cond_10
/* .line 1651 */
/* aget-object v0, p3, v3 */
v0 = java.lang.Integer .parseInt ( v0 );
/* .line 1652 */
/* .local v0, "enable":I */
android.os.TurboSchedMonitor .getInstance ( );
(( android.os.TurboSchedMonitor ) v1 ).enableCoreAppOptimizer ( v0 ); // invoke-virtual {v1, v0}, Landroid/os/TurboSchedMonitor;->enableCoreAppOptimizer(I)V
/* .line 1653 */
/* if-lez v0, :cond_7 */
/* move v2, v3 */
} // :cond_7
/* move v1, v2 */
/* .line 1654 */
/* .local v1, "enableBoolean":Z */
/* invoke-direct {p0, v1}, Lcom/miui/server/turbosched/TurboSchedManagerService;->enableFrameBoostInKernel(Z)V */
/* .line 1655 */
if ( v1 != null) { // if-eqz v1, :cond_8
/* .line 1656 */
/* iput-boolean v3, p0, Lcom/miui/server/turbosched/TurboSchedManagerService;->mKernelEnabelUpdate:Z */
/* .line 1658 */
} // :cond_8
/* iget-boolean v2, p0, Lcom/miui/server/turbosched/TurboSchedManagerService;->mRegistedForegroundReceiver:Z */
/* if-nez v2, :cond_9 */
/* .line 1659 */
/* invoke-direct {p0}, Lcom/miui/server/turbosched/TurboSchedManagerService;->registeForegroundReceiver()V */
/* .line 1661 */
} // :cond_9
/* invoke-direct {p0, v1}, Lcom/miui/server/turbosched/TurboSchedManagerService;->enableGaea(Z)V */
/* .line 1664 */
final String v2 = "persist.sys.miui_animator_sched.big_prime_cores"; // const-string v2, "persist.sys.miui_animator_sched.big_prime_cores"
if ( v1 != null) { // if-eqz v1, :cond_c
/* .line 1665 */
v4 = this.m8550Devices;
v4 = v5 = miui.os.Build.DEVICE;
if ( v4 != null) { // if-eqz v4, :cond_a
/* .line 1666 */
final String v4 = "3-7"; // const-string v4, "3-7"
android.os.SystemProperties .set ( v2,v4 );
/* .line 1667 */
} // :cond_a
v4 = this.m8475Devices;
v4 = v5 = miui.os.Build.DEVICE;
/* if-nez v4, :cond_b */
v4 = this.m8450Devices;
v4 = v5 = miui.os.Build.DEVICE;
if ( v4 != null) { // if-eqz v4, :cond_f
/* .line 1668 */
} // :cond_b
final String v4 = "4-7"; // const-string v4, "4-7"
android.os.SystemProperties .set ( v2,v4 );
/* .line 1671 */
} // :cond_c
v4 = this.m8550Devices;
v4 = v5 = miui.os.Build.DEVICE;
if ( v4 != null) { // if-eqz v4, :cond_d
/* .line 1672 */
final String v4 = "3-6"; // const-string v4, "3-6"
android.os.SystemProperties .set ( v2,v4 );
/* .line 1673 */
} // :cond_d
v4 = this.m8475Devices;
v4 = v5 = miui.os.Build.DEVICE;
/* if-nez v4, :cond_e */
v4 = this.m8450Devices;
v4 = v5 = miui.os.Build.DEVICE;
if ( v4 != null) { // if-eqz v4, :cond_f
/* .line 1674 */
} // :cond_e
final String v4 = "4-6"; // const-string v4, "4-6"
android.os.SystemProperties .set ( v2,v4 );
/* .line 1678 */
} // :cond_f
} // :goto_1
/* new-instance v2, Ljava/lang/StringBuilder; */
/* invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V */
final String v4 = " enableCoreAppOptimizer: "; // const-string v4, " enableCoreAppOptimizer: "
(( java.lang.StringBuilder ) v2 ).append ( v4 ); // invoke-virtual {v2, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
/* aget-object v4, p3, v3 */
(( java.lang.StringBuilder ) v2 ).append ( v4 ); // invoke-virtual {v2, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v2 ).toString ( ); // invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
/* invoke-direct {p0, p2, v2}, Lcom/miui/server/turbosched/TurboSchedManagerService;->printCommandResult(Ljava/io/PrintWriter;Ljava/lang/String;)V */
/* .line 1679 */
/* .line 1682 */
} // .end local v0 # "enable":I
} // .end local v1 # "enableBoolean":Z
} // :cond_10
/* array-length v0, p3 */
/* if-ne v0, v1, :cond_12 */
/* const-string/jumbo v0, "setVipTask" */
/* aget-object v6, p3, v2 */
v0 = (( java.lang.String ) v0 ).equals ( v6 ); // invoke-virtual {v0, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z
if ( v0 != null) { // if-eqz v0, :cond_12
/* .line 1683 */
/* aget-object v0, p3, v3 */
/* .line 1684 */
/* .local v0, "str":Ljava/lang/String; */
int v1 = 0; // const/4 v1, 0x0
/* .line 1685 */
/* .local v1, "result":Z */
/* iget-boolean v2, p0, Lcom/miui/server/turbosched/TurboSchedManagerService;->mTurboNodeExist:Z */
final String v4 = "dump-setVipTask"; // const-string v4, "dump-setVipTask"
if ( v2 != null) { // if-eqz v2, :cond_11
/* .line 1686 */
final String v2 = "/sys/module/metis/parameters/mi_viptask"; // const-string v2, "/sys/module/metis/parameters/mi_viptask"
v1 = /* invoke-direct {p0, v4, v2, v0}, Lcom/miui/server/turbosched/TurboSchedManagerService;->writeTurboSchedNode(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Z */
/* .line 1688 */
} // :cond_11
final String v2 = "/sys/module/migt/parameters/mi_viptask"; // const-string v2, "/sys/module/migt/parameters/mi_viptask"
v1 = /* invoke-direct {p0, v4, v2, v0}, Lcom/miui/server/turbosched/TurboSchedManagerService;->writeTurboSchedNode(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Z */
/* .line 1690 */
} // :goto_2
/* new-instance v2, Ljava/lang/StringBuilder; */
/* invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V */
/* const-string/jumbo v4, "setVipTask result: " */
(( java.lang.StringBuilder ) v2 ).append ( v4 ); // invoke-virtual {v2, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v2 ).append ( v1 ); // invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v2 ).toString ( ); // invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
/* invoke-direct {p0, p2, v2}, Lcom/miui/server/turbosched/TurboSchedManagerService;->printCommandResult(Ljava/io/PrintWriter;Ljava/lang/String;)V */
/* .line 1691 */
/* .line 1694 */
} // .end local v0 # "str":Ljava/lang/String;
} // .end local v1 # "result":Z
} // :cond_12
/* array-length v0, p3 */
int v6 = 4; // const/4 v6, 0x4
/* if-lt v0, v6, :cond_18 */
final String v0 = "policy"; // const-string v0, "policy"
/* aget-object v7, p3, v2 */
v0 = (( java.lang.String ) v0 ).equals ( v7 ); // invoke-virtual {v0, v7}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z
if ( v0 != null) { // if-eqz v0, :cond_18
/* .line 1695 */
/* aget-object v0, p3, v3 */
/* .line 1696 */
/* .local v0, "policy":Ljava/lang/String; */
/* aget-object v1, p3, v1 */
java.lang.Long .parseLong ( v1 );
/* move-result-wide v1 */
/* .line 1697 */
/* .local v1, "time":J */
/* array-length v5, p3 */
/* sub-int/2addr v5, v4 */
/* new-array v4, v5, [I */
/* .line 1698 */
/* .local v4, "tids":[I */
int v5 = 3; // const/4 v5, 0x3
/* .local v5, "i":I */
} // :goto_3
/* array-length v6, p3 */
/* if-ge v5, v6, :cond_13 */
/* .line 1699 */
/* add-int/lit8 v6, v5, -0x3 */
/* aget-object v7, p3, v5 */
v7 = java.lang.Integer .parseInt ( v7 );
/* aput v7, v4, v6 */
/* .line 1698 */
/* add-int/lit8 v5, v5, 0x1 */
/* .line 1701 */
} // .end local v5 # "i":I
} // :cond_13
final String v5 = "bwf"; // const-string v5, "bwf"
v5 = (( java.lang.String ) v0 ).equals ( v5 ); // invoke-virtual {v0, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z
if ( v5 != null) { // if-eqz v5, :cond_14
/* .line 1702 */
(( com.miui.server.turbosched.TurboSchedManagerService ) p0 ).setTurboSchedActionWithBoostFrequency ( v4, v1, v2 ); // invoke-virtual {p0, v4, v1, v2}, Lcom/miui/server/turbosched/TurboSchedManagerService;->setTurboSchedActionWithBoostFrequency([IJ)V
/* .line 1703 */
} // :cond_14
final String v5 = "priority"; // const-string v5, "priority"
v5 = (( java.lang.String ) v0 ).equals ( v5 ); // invoke-virtual {v0, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z
if ( v5 != null) { // if-eqz v5, :cond_15
/* .line 1704 */
(( com.miui.server.turbosched.TurboSchedManagerService ) p0 ).setTurboSchedActionWithPriority ( v4, v1, v2 ); // invoke-virtual {p0, v4, v1, v2}, Lcom/miui/server/turbosched/TurboSchedManagerService;->setTurboSchedActionWithPriority([IJ)V
/* .line 1705 */
} // :cond_15
/* const-string/jumbo v5, "vip" */
v5 = (( java.lang.String ) v0 ).equals ( v5 ); // invoke-virtual {v0, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z
if ( v5 != null) { // if-eqz v5, :cond_16
/* .line 1706 */
(( com.miui.server.turbosched.TurboSchedManagerService ) p0 ).setTurboSchedAction ( v4, v1, v2 ); // invoke-virtual {p0, v4, v1, v2}, Lcom/miui/server/turbosched/TurboSchedManagerService;->setTurboSchedAction([IJ)Z
/* .line 1707 */
} // :cond_16
final String v5 = "lc"; // const-string v5, "lc"
v5 = (( java.lang.String ) v0 ).equals ( v5 ); // invoke-virtual {v0, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z
if ( v5 != null) { // if-eqz v5, :cond_17
/* .line 1708 */
(( com.miui.server.turbosched.TurboSchedManagerService ) p0 ).setTurboSchedActionToLittleCore ( v4, v1, v2 ); // invoke-virtual {p0, v4, v1, v2}, Lcom/miui/server/turbosched/TurboSchedManagerService;->setTurboSchedActionToLittleCore([IJ)V
/* .line 1710 */
} // :cond_17
} // :goto_4
/* .line 1713 */
} // .end local v0 # "policy":Ljava/lang/String;
} // .end local v1 # "time":J
} // .end local v4 # "tids":[I
} // :cond_18
/* array-length v0, p3 */
final String v7 = ","; // const-string v7, ","
/* if-ne v0, v1, :cond_1a */
/* const-string/jumbo v0, "setPolicy" */
/* aget-object v8, p3, v2 */
v0 = (( java.lang.String ) v0 ).equals ( v8 ); // invoke-virtual {v0, v8}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z
if ( v0 != null) { // if-eqz v0, :cond_1a
/* .line 1714 */
/* aget-object v0, p3, v3 */
/* .line 1715 */
/* .local v0, "str":Ljava/lang/String; */
(( java.lang.String ) v0 ).split ( v7 ); // invoke-virtual {v0, v7}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;
/* .line 1716 */
/* .local v1, "policies":[Ljava/lang/String; */
/* new-instance v4, Ljava/util/ArrayList; */
/* invoke-direct {v4}, Ljava/util/ArrayList;-><init>()V */
/* .line 1717 */
/* .local v4, "policyList":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;" */
/* array-length v5, v1 */
} // :goto_5
/* if-ge v2, v5, :cond_19 */
/* aget-object v6, v1, v2 */
/* .line 1718 */
/* .local v6, "policy":Ljava/lang/String; */
/* .line 1717 */
} // .end local v6 # "policy":Ljava/lang/String;
/* add-int/lit8 v2, v2, 0x1 */
/* .line 1720 */
} // :cond_19
com.miui.server.turbosched.TurboSchedConfig .setPolicyList ( v4 );
/* .line 1721 */
this.mPolicyListBackup = v4;
/* .line 1722 */
/* new-instance v2, Ljava/lang/StringBuilder; */
/* invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V */
final String v5 = "TurboSchedConfig.mPolicyList: "; // const-string v5, "TurboSchedConfig.mPolicyList: "
(( java.lang.StringBuilder ) v2 ).append ( v5 ); // invoke-virtual {v2, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
com.miui.server.turbosched.TurboSchedConfig .getPolicyList ( );
(( java.lang.StringBuilder ) v2 ).append ( v5 ); // invoke-virtual {v2, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v2 ).toString ( ); // invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
/* invoke-direct {p0, p2, v2}, Lcom/miui/server/turbosched/TurboSchedManagerService;->printCommandResult(Ljava/io/PrintWriter;Ljava/lang/String;)V */
/* .line 1723 */
/* .line 1726 */
} // .end local v0 # "str":Ljava/lang/String;
} // .end local v1 # "policies":[Ljava/lang/String;
} // .end local v4 # "policyList":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;"
} // :cond_1a
/* array-length v0, p3 */
/* if-ne v0, v1, :cond_1c */
/* const-string/jumbo v0, "setCallerAllowList" */
/* aget-object v8, p3, v2 */
v0 = (( java.lang.String ) v0 ).equals ( v8 ); // invoke-virtual {v0, v8}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z
if ( v0 != null) { // if-eqz v0, :cond_1c
/* .line 1727 */
/* aget-object v0, p3, v3 */
/* .line 1728 */
/* .restart local v0 # "str":Ljava/lang/String; */
(( java.lang.String ) v0 ).split ( v7 ); // invoke-virtual {v0, v7}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;
/* .line 1729 */
/* .restart local v1 # "policies":[Ljava/lang/String; */
v4 = this.mCallerAllowList;
/* .line 1730 */
/* array-length v4, v1 */
} // :goto_6
/* if-ge v2, v4, :cond_1b */
/* aget-object v5, v1, v2 */
/* .line 1731 */
/* .local v5, "policy":Ljava/lang/String; */
v6 = this.mCallerAllowList;
/* .line 1730 */
} // .end local v5 # "policy":Ljava/lang/String;
/* add-int/lit8 v2, v2, 0x1 */
/* .line 1733 */
} // :cond_1b
/* new-instance v2, Ljava/lang/StringBuilder; */
/* invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V */
final String v4 = "mCallerAllowList: "; // const-string v4, "mCallerAllowList: "
(( java.lang.StringBuilder ) v2 ).append ( v4 ); // invoke-virtual {v2, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
v4 = this.mCallerAllowList;
(( java.lang.StringBuilder ) v2 ).append ( v4 ); // invoke-virtual {v2, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v2 ).toString ( ); // invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
/* invoke-direct {p0, p2, v2}, Lcom/miui/server/turbosched/TurboSchedManagerService;->printCommandResult(Ljava/io/PrintWriter;Ljava/lang/String;)V */
/* .line 1734 */
/* .line 1737 */
} // .end local v0 # "str":Ljava/lang/String;
} // .end local v1 # "policies":[Ljava/lang/String;
} // :cond_1c
/* array-length v0, p3 */
/* if-lt v0, v4, :cond_1d */
/* const-string/jumbo v0, "thermalBreak" */
/* aget-object v8, p3, v2 */
v0 = (( java.lang.String ) v0 ).equals ( v8 ); // invoke-virtual {v0, v8}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z
if ( v0 != null) { // if-eqz v0, :cond_1d
/* .line 1738 */
/* aget-object v0, p3, v3 */
v0 = java.lang.Integer .parseInt ( v0 );
/* .line 1739 */
/* .local v0, "boost":I */
/* aget-object v1, p3, v1 */
v1 = java.lang.Integer .parseInt ( v1 );
/* .line 1740 */
/* .local v1, "time":I */
/* int-to-long v4, v1 */
(( com.miui.server.turbosched.TurboSchedManagerService ) p0 ).breakThermlimit ( v0, v4, v5 ); // invoke-virtual {p0, v0, v4, v5}, Lcom/miui/server/turbosched/TurboSchedManagerService;->breakThermlimit(IJ)V
/* .line 1741 */
/* .line 1744 */
} // .end local v0 # "boost":I
} // .end local v1 # "time":I
} // :cond_1d
/* array-length v0, p3 */
/* if-ne v0, v1, :cond_1f */
/* const-string/jumbo v0, "setThermalBreakEnable" */
/* aget-object v8, p3, v2 */
v0 = (( java.lang.String ) v0 ).equals ( v8 ); // invoke-virtual {v0, v8}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z
if ( v0 != null) { // if-eqz v0, :cond_1f
/* .line 1745 */
/* aget-object v0, p3, v3 */
v0 = java.lang.Integer .parseInt ( v0 );
/* .line 1746 */
/* .local v0, "enable":I */
/* if-lez v0, :cond_1e */
/* move v2, v3 */
} // :cond_1e
/* move v1, v2 */
/* .line 1747 */
/* .local v1, "enableBoolean":Z */
/* invoke-direct {p0, v1}, Lcom/miui/server/turbosched/TurboSchedManagerService;->setThermalBreakEnable(Z)V */
/* .line 1748 */
/* new-instance v2, Ljava/lang/StringBuilder; */
/* invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V */
/* const-string/jumbo v4, "setThermalBreakEnable: " */
(( java.lang.StringBuilder ) v2 ).append ( v4 ); // invoke-virtual {v2, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v2 ).append ( v0 ); // invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v2 ).toString ( ); // invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
/* invoke-direct {p0, p2, v2}, Lcom/miui/server/turbosched/TurboSchedManagerService;->printCommandResult(Ljava/io/PrintWriter;Ljava/lang/String;)V */
/* .line 1749 */
/* .line 1752 */
} // .end local v0 # "enable":I
} // .end local v1 # "enableBoolean":Z
} // :cond_1f
/* array-length v0, p3 */
/* if-ne v0, v1, :cond_22 */
/* const-string/jumbo v0, "setThermalBreakThreshold" */
/* aget-object v8, p3, v2 */
v0 = (( java.lang.String ) v0 ).equals ( v8 ); // invoke-virtual {v0, v8}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z
if ( v0 != null) { // if-eqz v0, :cond_22
/* .line 1753 */
/* iget-boolean v0, p0, Lcom/miui/server/turbosched/TurboSchedManagerService;->mThermalBreakEnabled:Z */
/* if-nez v0, :cond_20 */
/* .line 1754 */
/* const-string/jumbo v0, "thermal break is not enabled" */
/* invoke-direct {p0, p2, v0}, Lcom/miui/server/turbosched/TurboSchedManagerService;->printCommandResult(Ljava/io/PrintWriter;Ljava/lang/String;)V */
/* .line 1755 */
/* .line 1757 */
} // :cond_20
/* aget-object v0, p3, v3 */
/* .line 1758 */
/* .local v0, "thresholdStr":Ljava/lang/String; */
final String v1 = "dry_run"; // const-string v1, "dry_run"
v1 = (( java.lang.String ) v1 ).equals ( v0 ); // invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z
/* if-nez v1, :cond_21 */
/* .line 1759 */
/* invoke-direct {p0, v0}, Lcom/miui/server/turbosched/TurboSchedManagerService;->setThermalBreakThresholdMap(Ljava/lang/String;)V */
/* .line 1761 */
} // :cond_21
v1 = com.miui.server.turbosched.TurboSchedManagerService.TEMPREATURE_THROSHOLD_BOOST_STRING_MAP;
(( java.lang.Object ) v1 ).toString ( ); // invoke-virtual {v1}, Ljava/lang/Object;->toString()Ljava/lang/String;
/* invoke-direct {p0, p2, v1}, Lcom/miui/server/turbosched/TurboSchedManagerService;->printCommandResult(Ljava/io/PrintWriter;Ljava/lang/String;)V */
/* .line 1762 */
/* .line 1765 */
} // .end local v0 # "thresholdStr":Ljava/lang/String;
} // :cond_22
/* array-length v0, p3 */
/* if-lt v0, v3, :cond_31 */
final String v0 = "CoreApp"; // const-string v0, "CoreApp"
/* aget-object v8, p3, v2 */
v0 = (( java.lang.String ) v0 ).equals ( v8 ); // invoke-virtual {v0, v8}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z
if ( v0 != null) { // if-eqz v0, :cond_31
/* .line 1766 */
/* array-length v0, p3 */
/* if-ne v0, v4, :cond_24 */
/* .line 1767 */
/* aget-object v0, p3, v1 */
(( java.lang.String ) v0 ).split ( v7 ); // invoke-virtual {v0, v7}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;
java.util.Arrays .asList ( v0 );
/* .line 1768 */
/* .local v0, "apps":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;" */
/* aget-object v1, p3, v3 */
v4 = (( java.lang.String ) v1 ).hashCode ( ); // invoke-virtual {v1}, Ljava/lang/String;->hashCode()I
/* sparse-switch v4, :sswitch_data_0 */
} // :cond_23
/* :sswitch_0 */
final String v2 = "del"; // const-string v2, "del"
v1 = (( java.lang.String ) v1 ).equals ( v2 ); // invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z
if ( v1 != null) { // if-eqz v1, :cond_23
/* move v2, v3 */
/* :sswitch_1 */
final String v4 = "add"; // const-string v4, "add"
v1 = (( java.lang.String ) v1 ).equals ( v4 ); // invoke-virtual {v1, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z
if ( v1 != null) { // if-eqz v1, :cond_23
} // :goto_7
int v2 = -1; // const/4 v2, -0x1
} // :goto_8
/* packed-switch v2, :pswitch_data_0 */
/* .line 1773 */
/* :pswitch_0 */
/* invoke-direct {p0, v0}, Lcom/miui/server/turbosched/TurboSchedManagerService;->delCoreApp(Ljava/util/List;)V */
/* .line 1774 */
/* .line 1770 */
/* :pswitch_1 */
/* invoke-direct {p0, v0}, Lcom/miui/server/turbosched/TurboSchedManagerService;->addCoreApp(Ljava/util/List;)V */
/* .line 1771 */
/* nop */
/* .line 1778 */
} // :goto_9
/* new-instance v1, Ljava/lang/StringBuilder; */
/* invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V */
final String v2 = "CoreApp: "; // const-string v2, "CoreApp: "
(( java.lang.StringBuilder ) v1 ).append ( v2 ); // invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
android.os.TurboSchedMonitor .getInstance ( );
(( android.os.TurboSchedMonitor ) v2 ).getCoreAppList ( ); // invoke-virtual {v2}, Landroid/os/TurboSchedMonitor;->getCoreAppList()Ljava/util/List;
(( java.lang.StringBuilder ) v1 ).append ( v2 ); // invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v1 ).toString ( ); // invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
/* invoke-direct {p0, p2, v1}, Lcom/miui/server/turbosched/TurboSchedManagerService;->printCommandResult(Ljava/io/PrintWriter;Ljava/lang/String;)V */
/* .line 1779 */
/* .line 1781 */
} // .end local v0 # "apps":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;"
} // :cond_24
final String v0 = " appFrameDelay: "; // const-string v0, " appFrameDelay: "
(( java.io.PrintWriter ) p2 ).println ( v0 ); // invoke-virtual {p2, v0}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V
/* .line 1782 */
int v0 = 0; // const/4 v0, 0x0
/* .line 1783 */
/* .local v0, "index":I */
android.os.TurboSchedMonitor .getInstance ( );
(( android.os.TurboSchedMonitor ) v7 ).getCoreAppFrameDealyThreshold ( ); // invoke-virtual {v7}, Landroid/os/TurboSchedMonitor;->getCoreAppFrameDealyThreshold()Ljava/util/List;
v8 = } // :goto_a
if ( v8 != null) { // if-eqz v8, :cond_2f
/* check-cast v8, Ljava/lang/Float; */
v8 = (( java.lang.Float ) v8 ).floatValue ( ); // invoke-virtual {v8}, Ljava/lang/Float;->floatValue()F
/* .line 1784 */
/* .local v8, "ths":F */
/* if-nez v0, :cond_25 */
/* .line 1785 */
/* new-instance v9, Ljava/lang/StringBuilder; */
/* invoke-direct {v9}, Ljava/lang/StringBuilder;-><init>()V */
final String v10 = " 0 AWEME: "; // const-string v10, " 0 AWEME: "
(( java.lang.StringBuilder ) v9 ).append ( v10 ); // invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v9 ).append ( v8 ); // invoke-virtual {v9, v8}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v9 ).toString ( ); // invoke-virtual {v9}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
(( java.io.PrintWriter ) p2 ).println ( v9 ); // invoke-virtual {p2, v9}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V
/* goto/16 :goto_b */
/* .line 1786 */
} // :cond_25
/* if-ne v0, v3, :cond_26 */
/* .line 1787 */
/* new-instance v9, Ljava/lang/StringBuilder; */
/* invoke-direct {v9}, Ljava/lang/StringBuilder;-><init>()V */
final String v10 = " 1 GIFMAKER: "; // const-string v10, " 1 GIFMAKER: "
(( java.lang.StringBuilder ) v9 ).append ( v10 ); // invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v9 ).append ( v8 ); // invoke-virtual {v9, v8}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v9 ).toString ( ); // invoke-virtual {v9}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
(( java.io.PrintWriter ) p2 ).println ( v9 ); // invoke-virtual {p2, v9}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V
/* goto/16 :goto_b */
/* .line 1788 */
} // :cond_26
/* if-ne v0, v1, :cond_27 */
/* .line 1789 */
/* new-instance v9, Ljava/lang/StringBuilder; */
/* invoke-direct {v9}, Ljava/lang/StringBuilder;-><init>()V */
final String v10 = " 2 WEIBO: "; // const-string v10, " 2 WEIBO: "
(( java.lang.StringBuilder ) v9 ).append ( v10 ); // invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v9 ).append ( v8 ); // invoke-virtual {v9, v8}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v9 ).toString ( ); // invoke-virtual {v9}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
(( java.io.PrintWriter ) p2 ).println ( v9 ); // invoke-virtual {p2, v9}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V
/* goto/16 :goto_b */
/* .line 1790 */
} // :cond_27
/* if-ne v0, v4, :cond_28 */
/* .line 1791 */
/* new-instance v9, Ljava/lang/StringBuilder; */
/* invoke-direct {v9}, Ljava/lang/StringBuilder;-><init>()V */
final String v10 = " 3 ARTICLE_NEWS: "; // const-string v10, " 3 ARTICLE_NEWS: "
(( java.lang.StringBuilder ) v9 ).append ( v10 ); // invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v9 ).append ( v8 ); // invoke-virtual {v9, v8}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v9 ).toString ( ); // invoke-virtual {v9}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
(( java.io.PrintWriter ) p2 ).println ( v9 ); // invoke-virtual {p2, v9}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V
/* goto/16 :goto_b */
/* .line 1792 */
} // :cond_28
/* if-ne v0, v6, :cond_29 */
/* .line 1793 */
/* new-instance v9, Ljava/lang/StringBuilder; */
/* invoke-direct {v9}, Ljava/lang/StringBuilder;-><init>()V */
final String v10 = " 4 TAOBAO: "; // const-string v10, " 4 TAOBAO: "
(( java.lang.StringBuilder ) v9 ).append ( v10 ); // invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v9 ).append ( v8 ); // invoke-virtual {v9, v8}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v9 ).toString ( ); // invoke-virtual {v9}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
(( java.io.PrintWriter ) p2 ).println ( v9 ); // invoke-virtual {p2, v9}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V
/* goto/16 :goto_b */
/* .line 1794 */
} // :cond_29
int v9 = 5; // const/4 v9, 0x5
/* if-ne v0, v9, :cond_2a */
/* .line 1795 */
/* new-instance v9, Ljava/lang/StringBuilder; */
/* invoke-direct {v9}, Ljava/lang/StringBuilder;-><init>()V */
final String v10 = " 5 WECHAT: "; // const-string v10, " 5 WECHAT: "
(( java.lang.StringBuilder ) v9 ).append ( v10 ); // invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v9 ).append ( v8 ); // invoke-virtual {v9, v8}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v9 ).toString ( ); // invoke-virtual {v9}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
(( java.io.PrintWriter ) p2 ).println ( v9 ); // invoke-virtual {p2, v9}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V
/* .line 1796 */
} // :cond_2a
int v9 = 6; // const/4 v9, 0x6
/* if-ne v0, v9, :cond_2b */
/* .line 1797 */
/* new-instance v9, Ljava/lang/StringBuilder; */
/* invoke-direct {v9}, Ljava/lang/StringBuilder;-><init>()V */
final String v10 = " 6 BILIBILI: "; // const-string v10, " 6 BILIBILI: "
(( java.lang.StringBuilder ) v9 ).append ( v10 ); // invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v9 ).append ( v8 ); // invoke-virtual {v9, v8}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v9 ).toString ( ); // invoke-virtual {v9}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
(( java.io.PrintWriter ) p2 ).println ( v9 ); // invoke-virtual {p2, v9}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V
/* .line 1798 */
} // :cond_2b
int v9 = 7; // const/4 v9, 0x7
/* if-ne v0, v9, :cond_2c */
/* .line 1799 */
/* new-instance v9, Ljava/lang/StringBuilder; */
/* invoke-direct {v9}, Ljava/lang/StringBuilder;-><init>()V */
final String v10 = " 7 AWEME_LITE "; // const-string v10, " 7 AWEME_LITE "
(( java.lang.StringBuilder ) v9 ).append ( v10 ); // invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v9 ).append ( v8 ); // invoke-virtual {v9, v8}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v9 ).toString ( ); // invoke-virtual {v9}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
(( java.io.PrintWriter ) p2 ).println ( v9 ); // invoke-virtual {p2, v9}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V
/* .line 1800 */
} // :cond_2c
/* const/16 v9, 0x8 */
/* if-ne v0, v9, :cond_2d */
/* .line 1801 */
/* new-instance v9, Ljava/lang/StringBuilder; */
/* invoke-direct {v9}, Ljava/lang/StringBuilder;-><init>()V */
final String v10 = " 8 KUAISHOU_NEBULA: "; // const-string v10, " 8 KUAISHOU_NEBULA: "
(( java.lang.StringBuilder ) v9 ).append ( v10 ); // invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v9 ).append ( v8 ); // invoke-virtual {v9, v8}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v9 ).toString ( ); // invoke-virtual {v9}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
(( java.io.PrintWriter ) p2 ).println ( v9 ); // invoke-virtual {p2, v9}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V
/* .line 1802 */
} // :cond_2d
/* const/16 v9, 0x9 */
/* if-ne v0, v9, :cond_2e */
/* .line 1803 */
/* new-instance v9, Ljava/lang/StringBuilder; */
/* invoke-direct {v9}, Ljava/lang/StringBuilder;-><init>()V */
final String v10 = " 9 QQ: "; // const-string v10, " 9 QQ: "
(( java.lang.StringBuilder ) v9 ).append ( v10 ); // invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v9 ).append ( v8 ); // invoke-virtual {v9, v8}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v9 ).toString ( ); // invoke-virtual {v9}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
(( java.io.PrintWriter ) p2 ).println ( v9 ); // invoke-virtual {p2, v9}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V
/* .line 1805 */
} // :cond_2e
} // :goto_b
/* nop */
} // .end local v8 # "ths":F
/* add-int/lit8 v0, v0, 0x1 */
/* .line 1806 */
/* goto/16 :goto_a */
/* .line 1808 */
} // :cond_2f
(( java.io.PrintWriter ) p2 ).println ( v5 ); // invoke-virtual {p2, v5}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V
/* .line 1809 */
android.os.TurboSchedMonitor .getInstance ( );
(( android.os.TurboSchedMonitor ) v1 ).getBoostDuration ( ); // invoke-virtual {v1}, Landroid/os/TurboSchedMonitor;->getBoostDuration()Ljava/util/List;
/* .line 1810 */
/* .local v1, "boostDuration":Ljava/util/List;, "Ljava/util/List<Ljava/lang/Long;>;" */
/* new-instance v4, Ljava/lang/StringBuilder; */
/* invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V */
final String v5 = " 0 Boost Duration: "; // const-string v5, " 0 Boost Duration: "
(( java.lang.StringBuilder ) v4 ).append ( v5 ); // invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v4 ).append ( v2 ); // invoke-virtual {v4, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v2 ).toString ( ); // invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
(( java.io.PrintWriter ) p2 ).println ( v2 ); // invoke-virtual {p2, v2}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V
/* .line 1811 */
/* new-instance v2, Ljava/lang/StringBuilder; */
/* invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V */
final String v4 = " 1 Thermal Break Duration: "; // const-string v4, " 1 Thermal Break Duration: "
(( java.lang.StringBuilder ) v2 ).append ( v4 ); // invoke-virtual {v2, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v2 ).append ( v4 ); // invoke-virtual {v2, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v2 ).toString ( ); // invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
(( java.io.PrintWriter ) p2 ).println ( v2 ); // invoke-virtual {p2, v2}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V
/* .line 1813 */
final String v2 = "Boost allow list : "; // const-string v2, "Boost allow list : "
(( java.io.PrintWriter ) p2 ).println ( v2 ); // invoke-virtual {p2, v2}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V
/* .line 1814 */
android.os.TurboSchedMonitor .getInstance ( );
(( android.os.TurboSchedMonitor ) v2 ).getCoreAppList ( ); // invoke-virtual {v2}, Landroid/os/TurboSchedMonitor;->getCoreAppList()Ljava/util/List;
v4 = } // :goto_c
if ( v4 != null) { // if-eqz v4, :cond_30
/* check-cast v4, Ljava/lang/String; */
/* .line 1815 */
/* .local v4, "name":Ljava/lang/String; */
/* new-instance v5, Ljava/lang/StringBuilder; */
/* invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V */
final String v6 = "CoreAppName : "; // const-string v6, "CoreAppName : "
(( java.lang.StringBuilder ) v5 ).append ( v6 ); // invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v5 ).append ( v4 ); // invoke-virtual {v5, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v5 ).toString ( ); // invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
(( java.io.PrintWriter ) p2 ).println ( v5 ); // invoke-virtual {p2, v5}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V
/* .line 1816 */
} // .end local v4 # "name":Ljava/lang/String;
/* .line 1817 */
} // :cond_30
/* .line 1820 */
} // .end local v0 # "index":I
} // .end local v1 # "boostDuration":Ljava/util/List;, "Ljava/util/List<Ljava/lang/Long;>;"
} // :cond_31
/* array-length v0, p3 */
/* if-lt v0, v3, :cond_34 */
final String v0 = "history"; // const-string v0, "history"
/* aget-object v4, p3, v2 */
v0 = (( java.lang.String ) v0 ).equals ( v4 ); // invoke-virtual {v0, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z
if ( v0 != null) { // if-eqz v0, :cond_34
/* .line 1821 */
/* array-length v0, p3 */
/* if-le v0, v3, :cond_33 */
/* .line 1822 */
final String v0 = "-c"; // const-string v0, "-c"
/* aget-object v1, p3, v3 */
v0 = (( java.lang.String ) v0 ).equals ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z
/* if-nez v0, :cond_32 */
final String v0 = "--clear"; // const-string v0, "--clear"
/* aget-object v1, p3, v3 */
v0 = (( java.lang.String ) v0 ).equals ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z
if ( v0 != null) { // if-eqz v0, :cond_33
/* .line 1823 */
} // :cond_32
/* new-instance v0, Landroid/util/LocalLog; */
/* invoke-direct {v0, v1}, Landroid/util/LocalLog;-><init>(I)V */
this.mHistoryLog = v0;
/* .line 1824 */
final String v0 = "History log cleared"; // const-string v0, "History log cleared"
(( java.io.PrintWriter ) p2 ).println ( v0 ); // invoke-virtual {p2, v0}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V
/* .line 1825 */
/* .line 1828 */
} // :cond_33
v0 = this.mHistoryLog;
(( android.util.LocalLog ) v0 ).dump ( p1, p2, p3 ); // invoke-virtual {v0, p1, p2, p3}, Landroid/util/LocalLog;->dump(Ljava/io/FileDescriptor;Ljava/io/PrintWriter;[Ljava/lang/String;)V
/* .line 1829 */
/* .line 1832 */
} // :cond_34
/* array-length v0, p3 */
/* if-ne v0, v1, :cond_36 */
final String v0 = "enableLink"; // const-string v0, "enableLink"
/* aget-object v4, p3, v2 */
v0 = (( java.lang.String ) v0 ).equals ( v4 ); // invoke-virtual {v0, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z
if ( v0 != null) { // if-eqz v0, :cond_36
/* .line 1833 */
/* aget-object v0, p3, v3 */
v0 = java.lang.Integer .parseInt ( v0 );
/* .line 1834 */
/* .local v0, "enable":I */
/* if-lez v0, :cond_35 */
/* move v2, v3 */
} // :cond_35
/* iput-boolean v2, p0, Lcom/miui/server/turbosched/TurboSchedManagerService;->mLinkEnable:Z */
/* .line 1835 */
/* new-instance v1, Ljava/lang/StringBuilder; */
/* invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V */
final String v2 = "enableLink: "; // const-string v2, "enableLink: "
(( java.lang.StringBuilder ) v1 ).append ( v2 ); // invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v1 ).append ( v0 ); // invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v1 ).toString ( ); // invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
/* invoke-direct {p0, p2, v1}, Lcom/miui/server/turbosched/TurboSchedManagerService;->printCommandResult(Ljava/io/PrintWriter;Ljava/lang/String;)V */
/* .line 1836 */
/* .line 1839 */
} // .end local v0 # "enable":I
} // :cond_36
/* array-length v0, p3 */
/* if-ne v0, v1, :cond_39 */
final String v0 = "enableGaea"; // const-string v0, "enableGaea"
/* aget-object v4, p3, v2 */
v0 = (( java.lang.String ) v0 ).equals ( v4 ); // invoke-virtual {v0, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z
if ( v0 != null) { // if-eqz v0, :cond_39
/* .line 1840 */
/* iget-boolean v0, p0, Lcom/miui/server/turbosched/TurboSchedManagerService;->mSupportGaea:Z */
if ( v0 != null) { // if-eqz v0, :cond_38
/* .line 1841 */
/* aget-object v0, p3, v3 */
v0 = java.lang.Integer .parseInt ( v0 );
/* .line 1842 */
/* .restart local v0 # "enable":I */
/* if-lez v0, :cond_37 */
/* move v4, v3 */
} // :cond_37
/* move v4, v2 */
} // :goto_d
/* iput-boolean v4, p0, Lcom/miui/server/turbosched/TurboSchedManagerService;->mGaeaEnable:Z */
/* .line 1844 */
/* invoke-direct {p0, v4}, Lcom/miui/server/turbosched/TurboSchedManagerService;->enableGaea(Z)V */
/* .line 1845 */
/* new-instance v4, Ljava/lang/StringBuilder; */
/* invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V */
final String v5 = "enableGaea: "; // const-string v5, "enableGaea: "
(( java.lang.StringBuilder ) v4 ).append ( v5 ); // invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v4 ).append ( v0 ); // invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v4 ).toString ( ); // invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
/* invoke-direct {p0, p2, v4}, Lcom/miui/server/turbosched/TurboSchedManagerService;->printCommandResult(Ljava/io/PrintWriter;Ljava/lang/String;)V */
/* .line 1846 */
} // .end local v0 # "enable":I
/* .line 1847 */
} // :cond_38
final String v0 = "current devices don\'t support gaea ..."; // const-string v0, "current devices don\'t support gaea ..."
/* invoke-direct {p0, p2, v0}, Lcom/miui/server/turbosched/TurboSchedManagerService;->printCommandResult(Ljava/io/PrintWriter;Ljava/lang/String;)V */
/* .line 1851 */
} // :cond_39
} // :goto_e
/* array-length v0, p3 */
/* if-lt v0, v3, :cond_3a */
final String v0 = "frameTurbo"; // const-string v0, "frameTurbo"
/* aget-object v4, p3, v2 */
v0 = (( java.lang.String ) v0 ).equals ( v4 ); // invoke-virtual {v0, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z
if ( v0 != null) { // if-eqz v0, :cond_3a
/* .line 1852 */
v0 = this.mFrameTurboAction;
v0 = (( com.miui.server.turbosched.FrameTurboAction ) v0 ).parseFrameTurboCommand ( p1, p2, p3 ); // invoke-virtual {v0, p1, p2, p3}, Lcom/miui/server/turbosched/FrameTurboAction;->parseFrameTurboCommand(Ljava/io/FileDescriptor;Ljava/io/PrintWriter;[Ljava/lang/String;)Z
/* .line 1855 */
} // :cond_3a
/* array-length v0, p3 */
/* if-lt v0, v1, :cond_3b */
final String v0 = "api"; // const-string v0, "api"
/* aget-object v1, p3, v2 */
v0 = (( java.lang.String ) v0 ).equals ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z
if ( v0 != null) { // if-eqz v0, :cond_3b
/* .line 1856 */
final String v0 = "enable"; // const-string v0, "enable"
/* aget-object v1, p3, v3 */
v0 = (( java.lang.String ) v0 ).equals ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z
/* .line 1857 */
/* .local v0, "enable":Z */
/* iput-boolean v0, p0, Lcom/miui/server/turbosched/TurboSchedManagerService;->mTurboEnabledV2:Z */
/* .line 1858 */
final String v1 = "persist.sys.turbosched.enable_v2"; // const-string v1, "persist.sys.turbosched.enable_v2"
java.lang.Boolean .toString ( v0 );
android.os.SystemProperties .set ( v1,v2 );
/* .line 1859 */
/* new-instance v1, Ljava/lang/StringBuilder; */
/* invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V */
final String v2 = "mTurboEnabledV2 change to: "; // const-string v2, "mTurboEnabledV2 change to: "
(( java.lang.StringBuilder ) v1 ).append ( v2 ); // invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
/* iget-boolean v2, p0, Lcom/miui/server/turbosched/TurboSchedManagerService;->mTurboEnabledV2:Z */
(( java.lang.StringBuilder ) v1 ).append ( v2 ); // invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v1 ).toString ( ); // invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
/* invoke-direct {p0, p2, v1}, Lcom/miui/server/turbosched/TurboSchedManagerService;->printCommandResult(Ljava/io/PrintWriter;Ljava/lang/String;)V */
/* .line 1860 */
/* .line 1863 */
} // .end local v0 # "enable":Z
} // :cond_3b
/* array-length v0, p3 */
/* if-lt v0, v3, :cond_3c */
final String v0 = "game"; // const-string v0, "game"
/* aget-object v1, p3, v2 */
v0 = (( java.lang.String ) v0 ).equals ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z
if ( v0 != null) { // if-eqz v0, :cond_3c
/* .line 1865 */
/* array-length v0, p3 */
/* sub-int/2addr v0, v3 */
/* new-array v0, v0, [Ljava/lang/String; */
/* .line 1866 */
/* .local v0, "newArgs":[Ljava/lang/String; */
/* array-length v1, p3 */
/* sub-int/2addr v1, v3 */
java.lang.System .arraycopy ( p3,v3,v0,v2,v1 );
/* .line 1867 */
v1 = this.mGameTurboAction;
v1 = (( com.miui.server.turbosched.GameTurboAction ) v1 ).dump ( p1, p2, v0 ); // invoke-virtual {v1, p1, p2, v0}, Lcom/miui/server/turbosched/GameTurboAction;->dump(Ljava/io/FileDescriptor;Ljava/io/PrintWriter;[Ljava/lang/String;)Z
/* .line 1870 */
} // .end local v0 # "newArgs":[Ljava/lang/String;
} // :cond_3c
/* array-length v0, p3 */
/* if-lt v0, v3, :cond_3d */
final String v0 = "config"; // const-string v0, "config"
/* aget-object v1, p3, v2 */
v0 = (( java.lang.String ) v0 ).equals ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z
if ( v0 != null) { // if-eqz v0, :cond_3d
/* .line 1871 */
/* invoke-direct {p0, p2}, Lcom/miui/server/turbosched/TurboSchedManagerService;->dumpConfig(Ljava/io/PrintWriter;)V */
/* .line 1872 */
/* .line 1874 */
} // :cond_3d
/* :sswitch_data_0 */
/* .sparse-switch */
/* 0x178a1 -> :sswitch_1 */
/* 0x1840b -> :sswitch_0 */
} // .end sparse-switch
/* :pswitch_data_0 */
/* .packed-switch 0x0 */
/* :pswitch_1 */
/* :pswitch_0 */
} // .end packed-switch
} // .end method
private void printCommandResult ( java.io.PrintWriter p0, java.lang.String p1 ) {
/* .locals 1 */
/* .param p1, "pw" # Ljava/io/PrintWriter; */
/* .param p2, "result" # Ljava/lang/String; */
/* .line 1878 */
final String v0 = "--------------------command result-----------------------"; // const-string v0, "--------------------command result-----------------------"
(( java.io.PrintWriter ) p1 ).println ( v0 ); // invoke-virtual {p1, v0}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V
/* .line 1879 */
(( java.io.PrintWriter ) p1 ).println ( p2 ); // invoke-virtual {p1, p2}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V
/* .line 1880 */
return;
} // .end method
private void recordRtPid ( Integer p0 ) {
/* .locals 7 */
/* .param p1, "id" # I */
/* .line 1938 */
int v0 = 0; // const/4 v0, 0x0
/* .line 1939 */
/* .local v0, "isSuccess":Z */
android.os.TurboSchedMonitor .getInstance ( );
v1 = (( android.os.TurboSchedMonitor ) v1 ).isCoreAppOptimizerEnabled ( ); // invoke-virtual {v1}, Landroid/os/TurboSchedMonitor;->isCoreAppOptimizerEnabled()Z
/* if-nez v1, :cond_0 */
/* .line 1940 */
return;
/* .line 1942 */
} // :cond_0
final String v1 = "TurboSchedManagerService"; // const-string v1, "TurboSchedManagerService"
/* new-instance v2, Ljava/lang/StringBuilder; */
/* invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V */
final String v3 = "recordrtpid,pid "; // const-string v3, "recordrtpid,pid "
(( java.lang.StringBuilder ) v2 ).append ( v3 ); // invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v2 ).append ( p1 ); // invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;
final String v3 = ", MITEST"; // const-string v3, ", MITEST"
(( java.lang.StringBuilder ) v2 ).append ( v3 ); // invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v2 ).toString ( ); // invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
android.util.Slog .i ( v1,v2 );
/* .line 1944 */
v1 = com.miui.server.turbosched.TurboSchedManagerService.mTurboLock;
/* monitor-enter v1 */
/* .line 1945 */
try { // :try_start_0
final String v2 = ""; // const-string v2, ""
/* .line 1946 */
/* .local v2, "str":Ljava/lang/String; */
/* new-instance v3, Ljava/lang/StringBuilder; */
/* invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V */
/* .line 1947 */
/* .local v3, "sb":Ljava/lang/StringBuilder; */
(( java.lang.StringBuilder ) v3 ).append ( p1 ); // invoke-virtual {v3, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;
/* .line 1948 */
(( java.lang.StringBuilder ) v3 ).toString ( ); // invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
/* move-object v2, v4 */
/* .line 1949 */
final String v4 = "RR-PID"; // const-string v4, "RR-PID"
final String v5 = "/sys/module/metis/parameters/rt_binder_client"; // const-string v5, "/sys/module/metis/parameters/rt_binder_client"
v4 = /* invoke-direct {p0, v4, v5, v2}, Lcom/miui/server/turbosched/TurboSchedManagerService;->writeTurboSchedNode(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Z */
/* move v0, v4 */
/* .line 1951 */
android.os.TurboSchedMonitor .getInstance ( );
v4 = (( android.os.TurboSchedMonitor ) v4 ).isDebugMode ( ); // invoke-virtual {v4}, Landroid/os/TurboSchedMonitor;->isDebugMode()Z
if ( v4 != null) { // if-eqz v4, :cond_1
/* .line 1952 */
final String v4 = "TurboSchedManagerService"; // const-string v4, "TurboSchedManagerService"
/* new-instance v5, Ljava/lang/StringBuilder; */
/* invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V */
final String v6 = "recordrtpid, isSuccess:"; // const-string v6, "recordrtpid, isSuccess:"
(( java.lang.StringBuilder ) v5 ).append ( v6 ); // invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v5 ).append ( v0 ); // invoke-virtual {v5, v0}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;
final String v6 = "pid "; // const-string v6, "pid "
(( java.lang.StringBuilder ) v5 ).append ( v6 ); // invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v5 ).append ( p1 ); // invoke-virtual {v5, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;
final String v6 = ", MITEST"; // const-string v6, ", MITEST"
(( java.lang.StringBuilder ) v5 ).append ( v6 ); // invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v5 ).toString ( ); // invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
android.util.Slog .i ( v4,v5 );
/* .line 1953 */
} // .end local v2 # "str":Ljava/lang/String;
} // .end local v3 # "sb":Ljava/lang/StringBuilder;
} // :cond_1
/* monitor-exit v1 */
/* .line 1954 */
return;
/* .line 1953 */
/* :catchall_0 */
/* move-exception v2 */
/* monitor-exit v1 */
/* :try_end_0 */
/* .catchall {:try_start_0 ..:try_end_0} :catchall_0 */
/* throw v2 */
} // .end method
private void registeForegroundReceiver ( ) {
/* .locals 3 */
/* .line 900 */
final String v0 = "TurboSchedManagerService"; // const-string v0, "TurboSchedManagerService"
android.os.TurboSchedMonitor .getInstance ( );
v1 = (( android.os.TurboSchedMonitor ) v1 ).isCoreAppOptimizerEnabled ( ); // invoke-virtual {v1}, Landroid/os/TurboSchedMonitor;->isCoreAppOptimizerEnabled()Z
/* if-nez v1, :cond_0 */
/* iget-boolean v1, p0, Lcom/miui/server/turbosched/TurboSchedManagerService;->mInStartUpMode:Z */
if ( v1 != null) { // if-eqz v1, :cond_1
/* .line 902 */
} // :cond_0
try { // :try_start_0
final String v1 = "registerProcessObserver!"; // const-string v1, "registerProcessObserver!"
android.util.Slog .i ( v0,v1 );
/* .line 903 */
v1 = this.mActivityManagerService;
v2 = this.mProcessObserver;
/* .line 904 */
int v1 = 1; // const/4 v1, 0x1
/* iput-boolean v1, p0, Lcom/miui/server/turbosched/TurboSchedManagerService;->mRegistedForegroundReceiver:Z */
/* :try_end_0 */
/* .catch Landroid/os/RemoteException; {:try_start_0 ..:try_end_0} :catch_0 */
/* .line 907 */
/* .line 905 */
/* :catch_0 */
/* move-exception v1 */
/* .line 906 */
/* .local v1, "e":Landroid/os/RemoteException; */
final String v2 = "registerProcessObserver failed"; // const-string v2, "registerProcessObserver failed"
android.util.Slog .e ( v0,v2 );
/* .line 909 */
} // .end local v1 # "e":Landroid/os/RemoteException;
} // :cond_1
} // :goto_0
return;
} // .end method
private void registerObserver ( ) {
/* .locals 5 */
/* .line 623 */
v0 = this.mContext;
(( android.content.Context ) v0 ).getContentResolver ( ); // invoke-virtual {v0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;
/* .line 624 */
final String v1 = "cloud_turbo_sched_enable"; // const-string v1, "cloud_turbo_sched_enable"
android.provider.Settings$System .getUriFor ( v1 );
v2 = this.mCloudSwichObserver;
/* .line 623 */
int v3 = 0; // const/4 v3, 0x0
int v4 = -2; // const/4 v4, -0x2
(( android.content.ContentResolver ) v0 ).registerContentObserver ( v1, v3, v2, v4 ); // invoke-virtual {v0, v1, v3, v2, v4}, Landroid/content/ContentResolver;->registerContentObserver(Landroid/net/Uri;ZLandroid/database/ContentObserver;I)V
/* .line 628 */
v0 = this.mContext;
(( android.content.Context ) v0 ).getContentResolver ( ); // invoke-virtual {v0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;
/* .line 629 */
final String v1 = "cloud_turbo_sched_enable_v2"; // const-string v1, "cloud_turbo_sched_enable_v2"
android.provider.Settings$System .getUriFor ( v1 );
v2 = this.mCloudTurboschedMiuiSdkObserver;
/* .line 628 */
(( android.content.ContentResolver ) v0 ).registerContentObserver ( v1, v3, v2, v4 ); // invoke-virtual {v0, v1, v3, v2, v4}, Landroid/content/ContentResolver;->registerContentObserver(Landroid/net/Uri;ZLandroid/database/ContentObserver;I)V
/* .line 633 */
v0 = this.mContext;
(( android.content.Context ) v0 ).getContentResolver ( ); // invoke-virtual {v0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;
/* .line 634 */
final String v1 = "cloud_turbo_sched_enable_core_app_optimizer"; // const-string v1, "cloud_turbo_sched_enable_core_app_optimizer"
android.provider.Settings$System .getUriFor ( v1 );
v2 = this.mCloudCoreAppOptimizerEnableObserver;
/* .line 633 */
(( android.content.ContentResolver ) v0 ).registerContentObserver ( v1, v3, v2, v4 ); // invoke-virtual {v0, v1, v3, v2, v4}, Landroid/content/ContentResolver;->registerContentObserver(Landroid/net/Uri;ZLandroid/database/ContentObserver;I)V
/* .line 638 */
v0 = this.mContext;
(( android.content.Context ) v0 ).getContentResolver ( ); // invoke-virtual {v0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;
/* .line 639 */
final String v1 = "cloud_turbo_sched_enable_core_top20_app_optimizer"; // const-string v1, "cloud_turbo_sched_enable_core_top20_app_optimizer"
android.provider.Settings$System .getUriFor ( v1 );
v2 = this.mCloudCoreTop20AppOptimizerEnableObserver;
/* .line 638 */
(( android.content.ContentResolver ) v0 ).registerContentObserver ( v1, v3, v2, v4 ); // invoke-virtual {v0, v1, v3, v2, v4}, Landroid/content/ContentResolver;->registerContentObserver(Landroid/net/Uri;ZLandroid/database/ContentObserver;I)V
/* .line 643 */
v0 = this.mContext;
(( android.content.Context ) v0 ).getContentResolver ( ); // invoke-virtual {v0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;
/* .line 644 */
final String v1 = "cloud_turbo_sched_allow_list"; // const-string v1, "cloud_turbo_sched_allow_list"
android.provider.Settings$System .getUriFor ( v1 );
v2 = this.mCloudAllowListObserver;
/* .line 643 */
(( android.content.ContentResolver ) v0 ).registerContentObserver ( v1, v3, v2, v4 ); // invoke-virtual {v0, v1, v3, v2, v4}, Landroid/content/ContentResolver;->registerContentObserver(Landroid/net/Uri;ZLandroid/database/ContentObserver;I)V
/* .line 648 */
v0 = this.mContext;
(( android.content.Context ) v0 ).getContentResolver ( ); // invoke-virtual {v0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;
/* .line 649 */
final String v1 = "cloud_turbo_sched_policy_list"; // const-string v1, "cloud_turbo_sched_policy_list"
android.provider.Settings$System .getUriFor ( v1 );
v2 = this.mCloudPolicyListObserver;
/* .line 648 */
(( android.content.ContentResolver ) v0 ).registerContentObserver ( v1, v3, v2, v4 ); // invoke-virtual {v0, v1, v3, v2, v4}, Landroid/content/ContentResolver;->registerContentObserver(Landroid/net/Uri;ZLandroid/database/ContentObserver;I)V
/* .line 653 */
v0 = this.mContext;
(( android.content.Context ) v0 ).getContentResolver ( ); // invoke-virtual {v0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;
/* .line 654 */
final String v1 = "cloud_turbo_sched_thermal_break_enable"; // const-string v1, "cloud_turbo_sched_thermal_break_enable"
android.provider.Settings$System .getUriFor ( v1 );
v2 = this.mCloudThermalBreakEnableObserver;
/* .line 653 */
(( android.content.ContentResolver ) v0 ).registerContentObserver ( v1, v3, v2, v4 ); // invoke-virtual {v0, v1, v3, v2, v4}, Landroid/content/ContentResolver;->registerContentObserver(Landroid/net/Uri;ZLandroid/database/ContentObserver;I)V
/* .line 658 */
v0 = this.mContext;
(( android.content.Context ) v0 ).getContentResolver ( ); // invoke-virtual {v0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;
/* .line 659 */
final String v1 = "cloud_turbo_sched_link_app_list"; // const-string v1, "cloud_turbo_sched_link_app_list"
android.provider.Settings$System .getUriFor ( v1 );
v2 = this.mCloudLinkWhiteListObserver;
/* .line 658 */
(( android.content.ContentResolver ) v0 ).registerContentObserver ( v1, v3, v2, v4 ); // invoke-virtual {v0, v1, v3, v2, v4}, Landroid/content/ContentResolver;->registerContentObserver(Landroid/net/Uri;ZLandroid/database/ContentObserver;I)V
/* .line 663 */
v0 = this.mContext;
(( android.content.Context ) v0 ).getContentResolver ( ); // invoke-virtual {v0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;
/* .line 664 */
final String v1 = "cloud_turbo_sched_frame_turbo_white_list"; // const-string v1, "cloud_turbo_sched_frame_turbo_white_list"
android.provider.Settings$System .getUriFor ( v1 );
v2 = this.mCloudFrameTurboWhiteListObserver;
/* .line 663 */
(( android.content.ContentResolver ) v0 ).registerContentObserver ( v1, v3, v2, v4 ); // invoke-virtual {v0, v1, v3, v2, v4}, Landroid/content/ContentResolver;->registerContentObserver(Landroid/net/Uri;ZLandroid/database/ContentObserver;I)V
/* .line 668 */
v0 = this.mContext;
(( android.content.Context ) v0 ).getContentResolver ( ); // invoke-virtual {v0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;
/* .line 669 */
final String v1 = "cloud_turbo_sched_dps_enable"; // const-string v1, "cloud_turbo_sched_dps_enable"
android.provider.Settings$System .getUriFor ( v1 );
v2 = this.mCloudDpsEnableObserver;
/* .line 668 */
(( android.content.ContentResolver ) v0 ).registerContentObserver ( v1, v3, v2, v4 ); // invoke-virtual {v0, v1, v3, v2, v4}, Landroid/content/ContentResolver;->registerContentObserver(Landroid/net/Uri;ZLandroid/database/ContentObserver;I)V
/* .line 671 */
return;
} // .end method
private void sendTraceBeginMsg ( java.lang.String p0 ) {
/* .locals 1 */
/* .param p1, "name" # Ljava/lang/String; */
/* .line 594 */
final String v0 = ""; // const-string v0, ""
/* invoke-direct {p0, p1, v0}, Lcom/miui/server/turbosched/TurboSchedManagerService;->sendTraceBeginMsg(Ljava/lang/String;Ljava/lang/String;)V */
/* .line 595 */
return;
} // .end method
private void sendTraceBeginMsg ( java.lang.String p0, java.lang.String p1 ) {
/* .locals 4 */
/* .param p1, "name" # Ljava/lang/String; */
/* .param p2, "desc" # Ljava/lang/String; */
/* .line 598 */
v0 = this.mTurboSchedHandler;
/* if-nez v0, :cond_0 */
/* .line 599 */
final String v0 = "TurboSchedManagerService"; // const-string v0, "TurboSchedManagerService"
final String v1 = "handler is null while sending trace begin message"; // const-string v1, "handler is null while sending trace begin message"
android.util.Slog .d ( v0,v1 );
/* .line 600 */
return;
/* .line 602 */
} // :cond_0
/* new-instance v0, Lcom/miui/server/turbosched/TurboSchedManagerService$TraceInfo; */
/* invoke-direct {v0}, Lcom/miui/server/turbosched/TurboSchedManagerService$TraceInfo;-><init>()V */
/* .line 603 */
/* .local v0, "info":Lcom/miui/server/turbosched/TurboSchedManagerService$TraceInfo; */
this.name = p1;
/* .line 604 */
this.desc = p2;
/* .line 605 */
java.lang.System .currentTimeMillis ( );
/* move-result-wide v1 */
/* iput-wide v1, v0, Lcom/miui/server/turbosched/TurboSchedManagerService$TraceInfo;->startTime:J */
/* .line 606 */
v1 = this.mTurboSchedHandler;
int v2 = 0; // const/4 v2, 0x0
(( com.miui.server.turbosched.TurboSchedManagerService$TurboSchedHandler ) v1 ).obtainMessage ( v2, v0 ); // invoke-virtual {v1, v2, v0}, Lcom/miui/server/turbosched/TurboSchedManagerService$TurboSchedHandler;->obtainMessage(ILjava/lang/Object;)Landroid/os/Message;
/* .line 607 */
/* .local v1, "msg":Landroid/os/Message; */
v2 = android.os.Process .myTid ( );
/* .line 608 */
/* .local v2, "tid":I */
/* iput v2, v1, Landroid/os/Message;->arg1:I */
/* .line 609 */
v3 = this.mTurboSchedHandler;
(( com.miui.server.turbosched.TurboSchedManagerService$TurboSchedHandler ) v3 ).sendMessage ( v1 ); // invoke-virtual {v3, v1}, Lcom/miui/server/turbosched/TurboSchedManagerService$TurboSchedHandler;->sendMessage(Landroid/os/Message;)Z
/* .line 610 */
return;
} // .end method
private void sendTraceEndMsg ( ) {
/* .locals 2 */
/* .line 613 */
v0 = this.mTurboSchedHandler;
/* if-nez v0, :cond_0 */
/* .line 614 */
final String v0 = "TurboSchedManagerService"; // const-string v0, "TurboSchedManagerService"
final String v1 = "handler is null while sending trace end message"; // const-string v1, "handler is null while sending trace end message"
android.util.Slog .d ( v0,v1 );
/* .line 615 */
return;
/* .line 617 */
} // :cond_0
int v1 = 1; // const/4 v1, 0x1
(( com.miui.server.turbosched.TurboSchedManagerService$TurboSchedHandler ) v0 ).obtainMessage ( v1 ); // invoke-virtual {v0, v1}, Lcom/miui/server/turbosched/TurboSchedManagerService$TurboSchedHandler;->obtainMessage(I)Landroid/os/Message;
/* .line 618 */
/* .local v0, "msg":Landroid/os/Message; */
v1 = this.mTurboSchedHandler;
(( com.miui.server.turbosched.TurboSchedManagerService$TurboSchedHandler ) v1 ).sendMessage ( v0 ); // invoke-virtual {v1, v0}, Lcom/miui/server/turbosched/TurboSchedManagerService$TurboSchedHandler;->sendMessage(Landroid/os/Message;)Z
/* .line 619 */
return;
} // .end method
private void setDpsEnable ( Boolean p0 ) {
/* .locals 2 */
/* .param p1, "enable" # Z */
/* .line 969 */
/* iget-boolean v0, p0, Lcom/miui/server/turbosched/TurboSchedManagerService;->mDPSSupport:Z */
/* if-nez v0, :cond_0 */
/* .line 970 */
final String v0 = "TurboSchedManagerService"; // const-string v0, "TurboSchedManagerService"
final String v1 = "DPS not support"; // const-string v1, "DPS not support"
android.util.Slog .d ( v0,v1 );
/* .line 971 */
return;
/* .line 973 */
} // :cond_0
/* iput-boolean p1, p0, Lcom/miui/server/turbosched/TurboSchedManagerService;->mDPSEnable:Z */
/* .line 974 */
final String v0 = "persist.sys.turbosched.dps.enable"; // const-string v0, "persist.sys.turbosched.dps.enable"
java.lang.Boolean .toString ( p1 );
android.os.SystemProperties .set ( v0,v1 );
/* .line 975 */
return;
} // .end method
private void setPolicyTimeoutChecker ( java.lang.String p0, Integer[] p1, Long p2, java.lang.String p3, java.util.Map p4 ) {
/* .locals 7 */
/* .param p1, "logTag" # Ljava/lang/String; */
/* .param p2, "tids" # [I */
/* .param p3, "time" # J */
/* .param p5, "cancelPath" # Ljava/lang/String; */
/* .annotation system Ldalvik/annotation/Signature; */
/* value = { */
/* "(", */
/* "Ljava/lang/String;", */
/* "[IJ", */
/* "Ljava/lang/String;", */
/* "Ljava/util/Map<", */
/* "Ljava/lang/Integer;", */
/* "Ljava/lang/Long;", */
/* ">;)V" */
/* } */
} // .end annotation
/* .line 2010 */
/* .local p6, "startTimeMap":Ljava/util/Map;, "Ljava/util/Map<Ljava/lang/Integer;Ljava/lang/Long;>;" */
v0 = this.TurboSchedSDKTimer;
if ( v0 != null) { // if-eqz v0, :cond_0
/* .line 2011 */
return;
/* .line 2013 */
} // :cond_0
/* array-length v0, p2 */
int v1 = 0; // const/4 v1, 0x0
} // :goto_0
/* if-ge v1, v0, :cond_1 */
/* aget v2, p2, v1 */
/* .line 2014 */
/* .local v2, "tid":I */
java.lang.Integer .valueOf ( v2 );
java.lang.System .currentTimeMillis ( );
/* move-result-wide v4 */
java.lang.Long .valueOf ( v4,v5 );
/* .line 2013 */
} // .end local v2 # "tid":I
/* add-int/lit8 v1, v1, 0x1 */
/* .line 2016 */
} // :cond_1
/* new-instance v0, Ljava/util/Timer; */
/* invoke-direct {v0}, Ljava/util/Timer;-><init>()V */
this.TurboSchedSDKTimer = v0;
/* .line 2017 */
/* new-instance v2, Lcom/miui/server/turbosched/TurboSchedManagerService$13; */
/* invoke-direct {v2, p0, p5, p1}, Lcom/miui/server/turbosched/TurboSchedManagerService$13;-><init>(Lcom/miui/server/turbosched/TurboSchedManagerService;Ljava/lang/String;Ljava/lang/String;)V */
this.TurboSchedSDKTimerTask = v2;
/* .line 2053 */
v1 = this.TurboSchedSDKTimer;
/* const-wide/16 v3, 0x3e8 */
/* const-wide/16 v5, 0x3e8 */
/* invoke-virtual/range {v1 ..v6}, Ljava/util/Timer;->schedule(Ljava/util/TimerTask;JJ)V */
/* .line 2054 */
return;
} // .end method
private void setThermalBreakEnable ( Boolean p0 ) {
/* .locals 4 */
/* .param p1, "enable" # Z */
/* .line 1005 */
/* new-instance v0, Ljava/lang/StringBuilder; */
/* invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V */
final String v1 = "cloud thermal break set received :"; // const-string v1, "cloud thermal break set received :"
(( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v0 ).append ( p1 ); // invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v0 ).toString ( ); // invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
final String v1 = "TurboSchedManagerService"; // const-string v1, "TurboSchedManagerService"
android.util.Slog .d ( v1,v0 );
/* .line 1006 */
/* iput-boolean p1, p0, Lcom/miui/server/turbosched/TurboSchedManagerService;->mThermalBreakEnabled:Z */
/* .line 1007 */
final String v0 = "persist.sys.turbosched.thermal_break.enable"; // const-string v0, "persist.sys.turbosched.thermal_break.enable"
java.lang.Boolean .toString ( p1 );
android.os.SystemProperties .set ( v0,v2 );
/* .line 1008 */
/* iget-boolean v0, p0, Lcom/miui/server/turbosched/TurboSchedManagerService;->mThermalBreakEnabled:Z */
if ( v0 != null) { // if-eqz v0, :cond_0
final String v0 = "1"; // const-string v0, "1"
} // :cond_0
final String v0 = "0"; // const-string v0, "0"
/* .line 1009 */
/* .local v0, "enableStr":Ljava/lang/String; */
} // :goto_0
final String v2 = "TB-Enabled"; // const-string v2, "TB-Enabled"
final String v3 = "/sys/module/metis/parameters/is_break_enable"; // const-string v3, "/sys/module/metis/parameters/is_break_enable"
v2 = /* invoke-direct {p0, v2, v3, v0}, Lcom/miui/server/turbosched/TurboSchedManagerService;->writeTurboSchedNode(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Z */
/* .line 1010 */
/* .local v2, "isSuccess":Z */
/* if-nez v2, :cond_1 */
/* .line 1011 */
/* const-string/jumbo v3, "write turbo sched thermal break failed" */
android.util.Slog .e ( v1,v3 );
/* .line 1013 */
} // :cond_1
return;
} // .end method
private void setThermalBreakThresholdMap ( java.lang.String p0 ) {
/* .locals 10 */
/* .param p1, "thresholdStr" # Ljava/lang/String; */
/* .line 1060 */
if ( p1 != null) { // if-eqz p1, :cond_1
v0 = (( java.lang.String ) p1 ).isEmpty ( ); // invoke-virtual {p1}, Ljava/lang/String;->isEmpty()Z
/* if-nez v0, :cond_1 */
/* .line 1061 */
/* new-instance v0, Ljava/lang/StringBuilder; */
/* invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V */
/* const-string/jumbo v1, "thermal break threshold set received :" */
(( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v0 ).append ( p1 ); // invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v0 ).toString ( ); // invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
final String v1 = "TurboSchedManagerService"; // const-string v1, "TurboSchedManagerService"
android.util.Slog .d ( v1,v0 );
/* .line 1062 */
final String v0 = ","; // const-string v0, ","
(( java.lang.String ) p1 ).split ( v0 ); // invoke-virtual {p1, v0}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;
/* .line 1063 */
/* .local v0, "thresholdList":[Ljava/lang/String; */
/* array-length v1, v0 */
int v2 = 0; // const/4 v2, 0x0
/* move v3, v2 */
} // :goto_0
/* if-ge v3, v1, :cond_1 */
/* aget-object v4, v0, v3 */
/* .line 1064 */
/* .local v4, "t":Ljava/lang/String; */
final String v5 = "-"; // const-string v5, "-"
(( java.lang.String ) v4 ).split ( v5 ); // invoke-virtual {v4, v5}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;
/* .line 1065 */
/* .local v5, "thresholdValues":[Ljava/lang/String; */
/* array-length v6, v5 */
int v7 = 2; // const/4 v7, 0x2
/* if-ne v6, v7, :cond_0 */
/* .line 1066 */
/* aget-object v6, v5, v2 */
v6 = java.lang.Integer .parseInt ( v6 );
/* .line 1067 */
/* .local v6, "temp":I */
/* new-instance v7, Ljava/lang/StringBuilder; */
/* invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V */
final String v8 = " "; // const-string v8, " "
(( java.lang.StringBuilder ) v7 ).append ( v8 ); // invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
int v9 = 1; // const/4 v9, 0x1
/* aget-object v9, v5, v9 */
(( java.lang.String ) v9 ).trim ( ); // invoke-virtual {v9}, Ljava/lang/String;->trim()Ljava/lang/String;
(( java.lang.StringBuilder ) v7 ).append ( v9 ); // invoke-virtual {v7, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v7 ).toString ( ); // invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
/* .line 1068 */
/* .local v7, "threshold":Ljava/lang/String; */
final String v9 = "_"; // const-string v9, "_"
(( java.lang.String ) v7 ).replace ( v9, v8 ); // invoke-virtual {v7, v9, v8}, Ljava/lang/String;->replace(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Ljava/lang/String;
/* .line 1069 */
v8 = com.miui.server.turbosched.TurboSchedManagerService.TEMPREATURE_THROSHOLD_BOOST_STRING_MAP;
java.lang.Integer .valueOf ( v6 );
/* .line 1063 */
} // .end local v4 # "t":Ljava/lang/String;
} // .end local v5 # "thresholdValues":[Ljava/lang/String;
} // .end local v6 # "temp":I
} // .end local v7 # "threshold":Ljava/lang/String;
} // :cond_0
/* add-int/lit8 v3, v3, 0x1 */
/* .line 1073 */
} // .end local v0 # "thresholdList":[Ljava/lang/String;
} // :cond_1
return;
} // .end method
private Boolean setTurboSchedActionInternal ( Integer[] p0, Long p1, java.lang.String p2 ) {
/* .locals 9 */
/* .param p1, "tids" # [I */
/* .param p2, "time" # J */
/* .param p4, "path" # Ljava/lang/String; */
/* .line 492 */
v0 = /* invoke-direct {p0}, Lcom/miui/server/turbosched/TurboSchedManagerService;->isTurboEnabled()Z */
int v1 = 0; // const/4 v1, 0x0
/* if-nez v0, :cond_0 */
/* .line 493 */
v0 = this.mHistoryLog;
final String v2 = "TS [V]: not enable"; // const-string v2, "TS [V]: not enable"
(( android.util.LocalLog ) v0 ).log ( v2 ); // invoke-virtual {v0, v2}, Landroid/util/LocalLog;->log(Ljava/lang/String;)V
/* .line 494 */
/* .line 496 */
} // :cond_0
/* array-length v0, p1 */
int v2 = 3; // const/4 v2, 0x3
/* if-le v0, v2, :cond_1 */
/* .line 497 */
v0 = this.mHistoryLog;
final String v2 = "TS [V]: tids length over limit"; // const-string v2, "TS [V]: tids length over limit"
(( android.util.LocalLog ) v0 ).log ( v2 ); // invoke-virtual {v0, v2}, Landroid/util/LocalLog;->log(Ljava/lang/String;)V
/* .line 498 */
/* .line 500 */
} // :cond_1
int v0 = 0; // const/4 v0, 0x0
/* .line 502 */
/* .local v0, "isSuccess":Z */
final String v2 = "V"; // const-string v2, "V"
/* invoke-direct {p0, v2}, Lcom/miui/server/turbosched/TurboSchedManagerService;->sendTraceBeginMsg(Ljava/lang/String;)V */
/* .line 504 */
android.os.TurboSchedMonitor .getInstance ( );
v2 = (( android.os.TurboSchedMonitor ) v2 ).isDebugMode ( ); // invoke-virtual {v2}, Landroid/os/TurboSchedMonitor;->isDebugMode()Z
/* const-wide/16 v3, 0x40 */
if ( v2 != null) { // if-eqz v2, :cond_2
/* .line 505 */
/* const-string/jumbo v2, "setTurboSchedActionInternal" */
android.os.Trace .traceBegin ( v3,v4,v2 );
/* .line 507 */
} // :cond_2
v2 = com.miui.server.turbosched.TurboSchedManagerService.mTurboLock;
/* monitor-enter v2 */
/* .line 508 */
try { // :try_start_0
final String v5 = ""; // const-string v5, ""
/* .line 509 */
/* .local v5, "str":Ljava/lang/String; */
/* new-instance v6, Ljava/lang/StringBuilder; */
/* invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V */
/* .line 510 */
/* .local v6, "sb":Ljava/lang/StringBuilder; */
/* aget v1, p1, v1 */
(( java.lang.StringBuilder ) v6 ).append ( v1 ); // invoke-virtual {v6, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;
/* .line 511 */
int v1 = 1; // const/4 v1, 0x1
/* .local v1, "i":I */
} // :goto_0
/* array-length v7, p1 */
/* if-ge v1, v7, :cond_4 */
/* .line 512 */
/* aget v7, p1, v1 */
/* if-nez v7, :cond_3 */
/* .line 513 */
/* .line 514 */
} // :cond_3
final String v7 = ":"; // const-string v7, ":"
(( java.lang.StringBuilder ) v6 ).append ( v7 ); // invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
/* .line 515 */
/* aget v7, p1, v1 */
(( java.lang.StringBuilder ) v6 ).append ( v7 ); // invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;
/* .line 511 */
/* add-int/lit8 v1, v1, 0x1 */
/* .line 517 */
} // .end local v1 # "i":I
} // :cond_4
} // :goto_1
final String v1 = "-"; // const-string v1, "-"
(( java.lang.StringBuilder ) v6 ).append ( v1 ); // invoke-virtual {v6, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
/* .line 518 */
(( java.lang.StringBuilder ) v6 ).append ( p2, p3 ); // invoke-virtual {v6, p2, p3}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;
/* .line 519 */
(( java.lang.StringBuilder ) v6 ).toString ( ); // invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
/* .line 521 */
} // .end local v5 # "str":Ljava/lang/String;
/* .local v1, "str":Ljava/lang/String; */
final String v5 = "V"; // const-string v5, "V"
v5 = /* invoke-direct {p0, v5, p4, v1}, Lcom/miui/server/turbosched/TurboSchedManagerService;->writeTurboSchedNode(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Z */
/* move v0, v5 */
/* .line 523 */
/* if-nez v0, :cond_5 */
android.os.TurboSchedMonitor .getInstance ( );
v5 = (( android.os.TurboSchedMonitor ) v5 ).isDebugMode ( ); // invoke-virtual {v5}, Landroid/os/TurboSchedMonitor;->isDebugMode()Z
if ( v5 != null) { // if-eqz v5, :cond_5
/* .line 524 */
final String v5 = "TurboSchedManagerService"; // const-string v5, "TurboSchedManagerService"
/* new-instance v7, Ljava/lang/StringBuilder; */
/* invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V */
/* const-string/jumbo v8, "setTurboSchedActionInternal, not success, check file:" */
(( java.lang.StringBuilder ) v7 ).append ( v8 ); // invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v7 ).append ( p4 ); // invoke-virtual {v7, p4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v7 ).toString ( ); // invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
android.util.Slog .d ( v5,v7 );
/* .line 525 */
} // .end local v1 # "str":Ljava/lang/String;
} // .end local v6 # "sb":Ljava/lang/StringBuilder;
} // :cond_5
/* monitor-exit v2 */
/* :try_end_0 */
/* .catchall {:try_start_0 ..:try_end_0} :catchall_0 */
/* .line 526 */
android.os.TurboSchedMonitor .getInstance ( );
v1 = (( android.os.TurboSchedMonitor ) v1 ).isDebugMode ( ); // invoke-virtual {v1}, Landroid/os/TurboSchedMonitor;->isDebugMode()Z
if ( v1 != null) { // if-eqz v1, :cond_6
/* .line 527 */
android.os.Trace .traceEnd ( v3,v4 );
/* .line 529 */
} // :cond_6
/* invoke-direct {p0}, Lcom/miui/server/turbosched/TurboSchedManagerService;->sendTraceEndMsg()V */
/* .line 531 */
v1 = this.mHistoryLog;
/* new-instance v2, Ljava/lang/StringBuilder; */
/* invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V */
final String v3 = "TS [V]: success: "; // const-string v3, "TS [V]: success: "
(( java.lang.StringBuilder ) v2 ).append ( v3 ); // invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v2 ).append ( v0 ); // invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v2 ).toString ( ); // invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
(( android.util.LocalLog ) v1 ).log ( v2 ); // invoke-virtual {v1, v2}, Landroid/util/LocalLog;->log(Ljava/lang/String;)V
/* .line 532 */
/* .line 525 */
/* :catchall_0 */
/* move-exception v1 */
try { // :try_start_1
/* monitor-exit v2 */
/* :try_end_1 */
/* .catchall {:try_start_1 ..:try_end_1} :catchall_0 */
/* throw v1 */
} // .end method
private java.util.List setTurboSchedActionParam ( java.lang.String p0, Integer[] p1, Long p2, java.lang.String p3 ) {
/* .locals 17 */
/* .param p1, "logTag" # Ljava/lang/String; */
/* .param p2, "tids" # [I */
/* .param p3, "time" # J */
/* .param p5, "type" # Ljava/lang/String; */
/* .annotation system Ldalvik/annotation/Signature; */
/* value = { */
/* "(", */
/* "Ljava/lang/String;", */
/* "[IJ", */
/* "Ljava/lang/String;", */
/* ")", */
/* "Ljava/util/List<", */
/* "Ljava/lang/Integer;", */
/* ">;" */
/* } */
} // .end annotation
/* .line 1078 */
/* move-object/from16 v9, p0 */
/* move-object/from16 v10, p2 */
/* new-instance v0, Ljava/util/ArrayList; */
/* invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V */
/* move-object v11, v0 */
/* .line 1079 */
/* .local v11, "tidsList":Ljava/util/List;, "Ljava/util/List<Ljava/lang/Integer;>;" */
v12 = this.mTurboschedSdkTidsLock;
/* monitor-enter v12 */
/* .line 1080 */
try { // :try_start_0
/* array-length v0, v10 */
int v1 = 0; // const/4 v1, 0x0
/* move v13, v1 */
} // :goto_0
/* if-ge v13, v0, :cond_1 */
/* aget v1, v10, v13 */
/* move v14, v1 */
/* .line 1081 */
/* .local v14, "tid":I */
/* new-instance v1, Ljava/lang/StringBuilder; */
/* invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V */
java.lang.Integer .toString ( v14 );
(( java.lang.StringBuilder ) v1 ).append ( v2 ); // invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
/* move-object/from16 v15, p5 */
(( java.lang.StringBuilder ) v1 ).append ( v15 ); // invoke-virtual {v1, v15}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v1 ).toString ( ); // invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
/* move-object v8, v1 */
/* .line 1082 */
/* .local v8, "paramId":Ljava/lang/String; */
v1 = this.mTurboschedSDKParamMap;
/* :try_end_0 */
/* .catchall {:try_start_0 ..:try_end_0} :catchall_2 */
if ( v1 != null) { // if-eqz v1, :cond_0
/* .line 1083 */
try { // :try_start_1
v7 = this.mTurboschedSDKParamMap;
/* new-instance v5, Lcom/miui/server/turbosched/TurboSchedManagerService$TurboschedSDKParam; */
java.lang.System .currentTimeMillis ( );
/* move-result-wide v3 */
/* move-object v1, v5 */
/* move-object/from16 v2, p0 */
/* move/from16 v16, v0 */
/* move-object v0, v5 */
/* move-wide/from16 v5, p3 */
/* move-object v10, v7 */
/* move v7, v14 */
/* move-object v15, v8 */
} // .end local v8 # "paramId":Ljava/lang/String;
/* .local v15, "paramId":Ljava/lang/String; */
/* move-object/from16 v8, p5 */
/* invoke-direct/range {v1 ..v8}, Lcom/miui/server/turbosched/TurboSchedManagerService$TurboschedSDKParam;-><init>(Lcom/miui/server/turbosched/TurboSchedManagerService;JJILjava/lang/String;)V */
/* .line 1084 */
v0 = this.mHistoryLog;
/* new-instance v1, Ljava/lang/StringBuilder; */
/* invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V */
final String v2 = "TS ["; // const-string v2, "TS ["
(( java.lang.StringBuilder ) v1 ).append ( v2 ); // invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
/* :try_end_1 */
/* .catchall {:try_start_1 ..:try_end_1} :catchall_1 */
/* move-object/from16 v10, p1 */
try { // :try_start_2
(( java.lang.StringBuilder ) v1 ).append ( v10 ); // invoke-virtual {v1, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
final String v2 = "] : tid duplication, tids: "; // const-string v2, "] : tid duplication, tids: "
(( java.lang.StringBuilder ) v1 ).append ( v2 ); // invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v1 ).append ( v14 ); // invoke-virtual {v1, v14}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;
final String v2 = ",time: "; // const-string v2, ",time: "
(( java.lang.StringBuilder ) v1 ).append ( v2 ); // invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
/* :try_end_2 */
/* .catchall {:try_start_2 ..:try_end_2} :catchall_0 */
/* move-wide/from16 v7, p3 */
try { // :try_start_3
(( java.lang.StringBuilder ) v1 ).append ( v7, v8 ); // invoke-virtual {v1, v7, v8}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v1 ).toString ( ); // invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
(( android.util.LocalLog ) v0 ).log ( v1 ); // invoke-virtual {v0, v1}, Landroid/util/LocalLog;->log(Ljava/lang/String;)V
/* .line 1090 */
} // .end local v14 # "tid":I
} // .end local v15 # "paramId":Ljava/lang/String;
/* :catchall_0 */
/* move-exception v0 */
/* :catchall_1 */
/* move-exception v0 */
/* move-object/from16 v10, p1 */
} // :goto_1
/* move-wide/from16 v7, p3 */
/* .line 1086 */
/* .restart local v8 # "paramId":Ljava/lang/String; */
/* .restart local v14 # "tid":I */
} // :cond_0
/* move-object/from16 v10, p1 */
/* move/from16 v16, v0 */
/* move-object v15, v8 */
/* move-wide/from16 v7, p3 */
} // .end local v8 # "paramId":Ljava/lang/String;
/* .restart local v15 # "paramId":Ljava/lang/String; */
java.lang.Integer .valueOf ( v14 );
/* .line 1087 */
v0 = this.mTurboschedSDKParamMap;
/* new-instance v5, Lcom/miui/server/turbosched/TurboSchedManagerService$TurboschedSDKParam; */
java.lang.System .currentTimeMillis ( );
/* move-result-wide v3 */
/* move-object v1, v5 */
/* move-object/from16 v2, p0 */
/* move-object v9, v5 */
/* move-wide/from16 v5, p3 */
/* move v7, v14 */
/* move-object/from16 v8, p5 */
/* invoke-direct/range {v1 ..v8}, Lcom/miui/server/turbosched/TurboSchedManagerService$TurboschedSDKParam;-><init>(Lcom/miui/server/turbosched/TurboSchedManagerService;JJILjava/lang/String;)V */
/* .line 1080 */
} // .end local v14 # "tid":I
} // .end local v15 # "paramId":Ljava/lang/String;
} // :goto_2
/* add-int/lit8 v13, v13, 0x1 */
/* move-object/from16 v9, p0 */
/* move-object/from16 v10, p2 */
/* move/from16 v0, v16 */
/* goto/16 :goto_0 */
/* .line 1090 */
} // :cond_1
/* move-object/from16 v10, p1 */
/* monitor-exit v12 */
/* .line 1091 */
/* .line 1090 */
/* :catchall_2 */
/* move-exception v0 */
/* move-object/from16 v10, p1 */
} // :goto_3
/* monitor-exit v12 */
/* :try_end_3 */
/* .catchall {:try_start_3 ..:try_end_3} :catchall_3 */
/* throw v0 */
/* :catchall_3 */
/* move-exception v0 */
} // .end method
private void syncTop20AppList ( Boolean p0 ) {
/* .locals 2 */
/* .param p1, "optimizerEnabled" # Z */
/* .line 2170 */
/* iput-boolean p1, p0, Lcom/miui/server/turbosched/TurboSchedManagerService;->mEnableTop20:Z */
/* .line 2171 */
final String v0 = "persist.sys.turbosched.enabletop20app"; // const-string v0, "persist.sys.turbosched.enabletop20app"
java.lang.Boolean .toString ( p1 );
android.os.SystemProperties .set ( v0,v1 );
/* .line 2172 */
/* sget-boolean v0, Lmiui/os/Build;->IS_TABLET:Z */
/* if-nez v0, :cond_0 */
/* .line 2173 */
(( com.miui.server.turbosched.TurboSchedManagerService ) p0 ).getCoreAppList ( ); // invoke-virtual {p0}, Lcom/miui/server/turbosched/TurboSchedManagerService;->getCoreAppList()Ljava/util/List;
/* .line 2174 */
/* .local v0, "apps":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;" */
/* invoke-direct {p0, v0}, Lcom/miui/server/turbosched/TurboSchedManagerService;->syncTopAppList(Ljava/util/List;)V */
/* .line 2175 */
} // .end local v0 # "apps":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;"
/* .line 2177 */
} // :cond_0
v0 = com.miui.server.turbosched.TurboSchedManagerService.TABLET_TOP8_APP_LIST;
/* .line 2178 */
/* .restart local v0 # "apps":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;" */
/* invoke-direct {p0, v0}, Lcom/miui/server/turbosched/TurboSchedManagerService;->syncTopAppList(Ljava/util/List;)V */
/* .line 2180 */
} // .end local v0 # "apps":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;"
} // :goto_0
return;
} // .end method
private void syncTopAppList ( java.util.List p0 ) {
/* .locals 3 */
/* .annotation system Ldalvik/annotation/Signature; */
/* value = { */
/* "(", */
/* "Ljava/util/List<", */
/* "Ljava/lang/String;", */
/* ">;)V" */
/* } */
} // .end annotation
/* .line 2143 */
/* .local p1, "coreAppList":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;" */
/* iget-boolean v0, p0, Lcom/miui/server/turbosched/TurboSchedManagerService;->mIsGlobal:Z */
if ( v0 != null) { // if-eqz v0, :cond_0
/* .line 2144 */
v0 = com.miui.server.turbosched.TurboSchedManagerService.DEFAULT_GL_APP_LIST;
this.mCoreAppsPackageNameList = v0;
/* .line 2146 */
} // :cond_0
/* sget-boolean v0, Lmiui/os/Build;->IS_TABLET:Z */
/* if-nez v0, :cond_2 */
/* .line 2147 */
/* iget-boolean v0, p0, Lcom/miui/server/turbosched/TurboSchedManagerService;->mEnableTop20:Z */
if ( v0 != null) { // if-eqz v0, :cond_1
/* .line 2148 */
v0 = com.miui.server.turbosched.TurboSchedManagerService.DEFAULT_TOP20_APP_LIST;
this.mCoreAppsPackageNameList = v0;
/* .line 2151 */
} // :cond_1
v0 = com.miui.server.turbosched.TurboSchedManagerService.DEFAULT_APP_LIST;
this.mCoreAppsPackageNameList = v0;
/* .line 2154 */
} // :cond_2
/* iget-boolean v0, p0, Lcom/miui/server/turbosched/TurboSchedManagerService;->mEnableTop20:Z */
if ( v0 != null) { // if-eqz v0, :cond_3
/* .line 2155 */
v0 = com.miui.server.turbosched.TurboSchedManagerService.TABLET_TOP20_APP_LIST;
this.mCoreAppsPackageNameList = v0;
/* .line 2157 */
} // :cond_3
v0 = com.miui.server.turbosched.TurboSchedManagerService.TABLET_TOP8_APP_LIST;
this.mCoreAppsPackageNameList = v0;
/* .line 2160 */
} // :goto_0
v1 = } // :goto_1
if ( v1 != null) { // if-eqz v1, :cond_5
/* check-cast v1, Ljava/lang/String; */
/* .line 2161 */
/* .local v1, "app":Ljava/lang/String; */
v2 = v2 = this.mCoreAppsPackageNameList;
/* if-nez v2, :cond_4 */
/* .line 2162 */
v2 = this.mCoreAppsPackageNameList;
/* .line 2164 */
} // .end local v1 # "app":Ljava/lang/String;
} // :cond_4
/* .line 2166 */
} // :cond_5
} // :goto_2
android.os.TurboSchedMonitor .getInstance ( );
v1 = this.mContext;
v2 = this.mCoreAppsPackageNameList;
(( android.os.TurboSchedMonitor ) v0 ).setCoreAppList ( v1, v2 ); // invoke-virtual {v0, v1, v2}, Landroid/os/TurboSchedMonitor;->setCoreAppList(Landroid/content/Context;Ljava/util/List;)V
/* .line 2167 */
return;
} // .end method
private void updateCloudAllowListProp ( ) {
/* .locals 6 */
/* .line 912 */
v0 = this.mContext;
(( android.content.Context ) v0 ).getContentResolver ( ); // invoke-virtual {v0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;
final String v1 = "cloud_turbo_sched_allow_list"; // const-string v1, "cloud_turbo_sched_allow_list"
int v2 = -2; // const/4 v2, -0x2
android.provider.Settings$System .getStringForUser ( v0,v1,v2 );
/* .line 915 */
/* .local v0, "str":Ljava/lang/String; */
/* iget-boolean v1, p0, Lcom/miui/server/turbosched/TurboSchedManagerService;->mReceiveCloudData:Z */
/* if-nez v1, :cond_0 */
/* .line 916 */
return;
/* .line 918 */
} // :cond_0
if ( v0 != null) { // if-eqz v0, :cond_1
v1 = (( java.lang.String ) v0 ).isEmpty ( ); // invoke-virtual {v0}, Ljava/lang/String;->isEmpty()Z
/* if-nez v1, :cond_1 */
/* .line 919 */
final String v1 = ","; // const-string v1, ","
(( java.lang.String ) v0 ).split ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;
/* .line 920 */
/* .local v1, "pkgName":[Ljava/lang/String; */
v2 = this.mCallerAllowList;
/* .line 921 */
/* array-length v2, v1 */
/* if-lez v2, :cond_1 */
/* .line 922 */
/* array-length v2, v1 */
int v3 = 0; // const/4 v3, 0x0
} // :goto_0
/* if-ge v3, v2, :cond_1 */
/* aget-object v4, v1, v3 */
/* .line 923 */
/* .local v4, "name":Ljava/lang/String; */
v5 = this.mCallerAllowList;
/* .line 922 */
} // .end local v4 # "name":Ljava/lang/String;
/* add-int/lit8 v3, v3, 0x1 */
/* .line 927 */
} // .end local v1 # "pkgName":[Ljava/lang/String;
} // :cond_1
return;
} // .end method
private void updateCloudControlProp ( ) {
/* .locals 1 */
/* .line 781 */
/* invoke-direct {p0}, Lcom/miui/server/turbosched/TurboSchedManagerService;->updateCoreAppOptimizerEnableProp()V */
/* .line 782 */
android.os.TurboSchedMonitor .getInstance ( );
v0 = (( android.os.TurboSchedMonitor ) v0 ).isCoreAppOptimizerEnabled ( ); // invoke-virtual {v0}, Landroid/os/TurboSchedMonitor;->isCoreAppOptimizerEnabled()Z
/* invoke-direct {p0, v0}, Lcom/miui/server/turbosched/TurboSchedManagerService;->updateEnableProp(Z)V */
/* .line 783 */
/* invoke-direct {p0}, Lcom/miui/server/turbosched/TurboSchedManagerService;->updateCoreTop20AppOptimizerEnableProp()V */
/* .line 784 */
/* invoke-direct {p0}, Lcom/miui/server/turbosched/TurboSchedManagerService;->updateCloudAllowListProp()V */
/* .line 785 */
/* invoke-direct {p0}, Lcom/miui/server/turbosched/TurboSchedManagerService;->updateCloudPolicyListProp()V */
/* .line 786 */
/* invoke-direct {p0}, Lcom/miui/server/turbosched/TurboSchedManagerService;->updateCloudThermalBreakEnableProp()V */
/* .line 787 */
/* invoke-direct {p0}, Lcom/miui/server/turbosched/TurboSchedManagerService;->updateCloudThermalBreakThresholdProp()V */
/* .line 788 */
/* invoke-direct {p0}, Lcom/miui/server/turbosched/TurboSchedManagerService;->updateCloudLinkWhiteListProp()V */
/* .line 789 */
/* invoke-direct {p0}, Lcom/miui/server/turbosched/TurboSchedManagerService;->updateCloudFrameTurboWhiteListProp()V */
/* .line 790 */
/* invoke-direct {p0}, Lcom/miui/server/turbosched/TurboSchedManagerService;->updateCloudDpsEnableProp()V */
/* .line 791 */
return;
} // .end method
private void updateCloudDpsEnableProp ( ) {
/* .locals 3 */
/* .line 957 */
v0 = this.mContext;
/* .line 958 */
(( android.content.Context ) v0 ).getContentResolver ( ); // invoke-virtual {v0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;
/* .line 957 */
final String v1 = "cloud_turbo_sched_dps_enable"; // const-string v1, "cloud_turbo_sched_dps_enable"
int v2 = -2; // const/4 v2, -0x2
android.provider.Settings$System .getStringForUser ( v0,v1,v2 );
/* .line 960 */
/* .local v0, "enableStr":Ljava/lang/String; */
/* iget-boolean v1, p0, Lcom/miui/server/turbosched/TurboSchedManagerService;->mReceiveCloudData:Z */
/* if-nez v1, :cond_0 */
/* .line 961 */
return;
/* .line 963 */
} // :cond_0
if ( v0 != null) { // if-eqz v0, :cond_1
v1 = (( java.lang.String ) v0 ).isEmpty ( ); // invoke-virtual {v0}, Ljava/lang/String;->isEmpty()Z
/* if-nez v1, :cond_1 */
/* .line 964 */
v1 = java.lang.Boolean .parseBoolean ( v0 );
/* invoke-direct {p0, v1}, Lcom/miui/server/turbosched/TurboSchedManagerService;->setDpsEnable(Z)V */
/* .line 966 */
} // :cond_1
return;
} // .end method
private void updateCloudFrameTurboWhiteListProp ( ) {
/* .locals 3 */
/* .line 1045 */
v0 = this.mContext;
(( android.content.Context ) v0 ).getContentResolver ( ); // invoke-virtual {v0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;
final String v1 = "cloud_turbo_sched_frame_turbo_white_list"; // const-string v1, "cloud_turbo_sched_frame_turbo_white_list"
int v2 = -2; // const/4 v2, -0x2
android.provider.Settings$System .getStringForUser ( v0,v1,v2 );
/* .line 1048 */
/* .local v0, "str":Ljava/lang/String; */
/* iget-boolean v1, p0, Lcom/miui/server/turbosched/TurboSchedManagerService;->mReceiveCloudData:Z */
/* if-nez v1, :cond_0 */
/* .line 1049 */
return;
/* .line 1051 */
} // :cond_0
if ( v0 != null) { // if-eqz v0, :cond_1
v1 = (( java.lang.String ) v0 ).isEmpty ( ); // invoke-virtual {v0}, Ljava/lang/String;->isEmpty()Z
/* if-nez v1, :cond_1 */
/* .line 1052 */
final String v1 = ","; // const-string v1, ","
(( java.lang.String ) v0 ).split ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;
/* .line 1053 */
/* .local v1, "pkgScenes":[Ljava/lang/String; */
v2 = this.mFrameTurboAction;
(( com.miui.server.turbosched.FrameTurboAction ) v2 ).updateWhiteList ( v1 ); // invoke-virtual {v2, v1}, Lcom/miui/server/turbosched/FrameTurboAction;->updateWhiteList([Ljava/lang/String;)V
/* .line 1055 */
} // .end local v1 # "pkgScenes":[Ljava/lang/String;
} // :cond_1
return;
} // .end method
private void updateCloudLinkWhiteListProp ( ) {
/* .locals 6 */
/* .line 1026 */
v0 = this.mContext;
(( android.content.Context ) v0 ).getContentResolver ( ); // invoke-virtual {v0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;
final String v1 = "cloud_turbo_sched_link_app_list"; // const-string v1, "cloud_turbo_sched_link_app_list"
int v2 = -2; // const/4 v2, -0x2
android.provider.Settings$System .getStringForUser ( v0,v1,v2 );
/* .line 1029 */
/* .local v0, "str":Ljava/lang/String; */
/* iget-boolean v1, p0, Lcom/miui/server/turbosched/TurboSchedManagerService;->mReceiveCloudData:Z */
/* if-nez v1, :cond_0 */
/* .line 1030 */
return;
/* .line 1032 */
} // :cond_0
if ( v0 != null) { // if-eqz v0, :cond_1
v1 = (( java.lang.String ) v0 ).isEmpty ( ); // invoke-virtual {v0}, Ljava/lang/String;->isEmpty()Z
/* if-nez v1, :cond_1 */
/* .line 1033 */
final String v1 = ","; // const-string v1, ","
(( java.lang.String ) v0 ).split ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;
/* .line 1034 */
/* .local v1, "pkgName":[Ljava/lang/String; */
v2 = this.mAppsLinkVipList;
/* .line 1035 */
/* array-length v2, v1 */
/* if-lez v2, :cond_1 */
/* .line 1036 */
/* array-length v2, v1 */
int v3 = 0; // const/4 v3, 0x0
} // :goto_0
/* if-ge v3, v2, :cond_1 */
/* aget-object v4, v1, v3 */
/* .line 1037 */
/* .local v4, "name":Ljava/lang/String; */
v5 = this.mAppsLinkVipList;
/* .line 1036 */
} // .end local v4 # "name":Ljava/lang/String;
/* add-int/lit8 v3, v3, 0x1 */
/* .line 1041 */
} // .end local v1 # "pkgName":[Ljava/lang/String;
} // :cond_1
return;
} // .end method
private void updateCloudPolicyListProp ( ) {
/* .locals 7 */
/* .line 930 */
v0 = this.mContext;
(( android.content.Context ) v0 ).getContentResolver ( ); // invoke-virtual {v0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;
final String v1 = "cloud_turbo_sched_policy_list"; // const-string v1, "cloud_turbo_sched_policy_list"
int v2 = -2; // const/4 v2, -0x2
android.provider.Settings$System .getStringForUser ( v0,v1,v2 );
/* .line 933 */
/* .local v0, "str":Ljava/lang/String; */
/* iget-boolean v1, p0, Lcom/miui/server/turbosched/TurboSchedManagerService;->mReceiveCloudData:Z */
/* if-nez v1, :cond_0 */
/* .line 934 */
return;
/* .line 936 */
} // :cond_0
/* new-instance v1, Ljava/util/ArrayList; */
/* invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V */
/* .line 937 */
/* .local v1, "policyList":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;" */
final String v2 = "TurboSchedManagerService"; // const-string v2, "TurboSchedManagerService"
if ( v0 != null) { // if-eqz v0, :cond_3
v3 = (( java.lang.String ) v0 ).isEmpty ( ); // invoke-virtual {v0}, Ljava/lang/String;->isEmpty()Z
/* if-nez v3, :cond_3 */
/* .line 938 */
/* .line 939 */
final String v3 = ","; // const-string v3, ","
(( java.lang.String ) v0 ).split ( v3 ); // invoke-virtual {v0, v3}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;
/* .line 940 */
/* .local v3, "policyListName":[Ljava/lang/String; */
/* array-length v4, v3 */
/* if-lez v4, :cond_2 */
/* .line 941 */
/* array-length v4, v3 */
int v5 = 0; // const/4 v5, 0x0
} // :goto_0
/* if-ge v5, v4, :cond_1 */
/* aget-object v6, v3, v5 */
/* .line 942 */
/* .local v6, "name":Ljava/lang/String; */
/* .line 941 */
} // .end local v6 # "name":Ljava/lang/String;
/* add-int/lit8 v5, v5, 0x1 */
/* .line 944 */
} // :cond_1
com.miui.server.turbosched.TurboSchedConfig .setPolicyList ( v1 );
/* .line 945 */
this.mPolicyListBackup = v1;
/* .line 946 */
/* new-instance v4, Ljava/lang/StringBuilder; */
/* invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V */
final String v5 = "Update Cloud PolicyList Prop - mPolicyList: "; // const-string v5, "Update Cloud PolicyList Prop - mPolicyList: "
(( java.lang.StringBuilder ) v4 ).append ( v5 ); // invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
java.util.Arrays .toString ( v5 );
(( java.lang.StringBuilder ) v4 ).append ( v5 ); // invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v4 ).toString ( ); // invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
android.util.Slog .d ( v2,v4 );
/* .line 948 */
} // .end local v3 # "policyListName":[Ljava/lang/String;
} // :cond_2
/* .line 949 */
} // :cond_3
/* .line 950 */
com.miui.server.turbosched.TurboSchedConfig .setPolicyList ( v1 );
/* .line 951 */
this.mPolicyListBackup = v1;
/* .line 952 */
final String v3 = "Update Cloud PolicyList Prop - mPolicyList: null"; // const-string v3, "Update Cloud PolicyList Prop - mPolicyList: null"
android.util.Slog .d ( v2,v3 );
/* .line 954 */
} // :goto_1
return;
} // .end method
private void updateCloudThermalBreakEnableProp ( ) {
/* .locals 3 */
/* .line 993 */
v0 = this.mContext;
/* .line 994 */
(( android.content.Context ) v0 ).getContentResolver ( ); // invoke-virtual {v0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;
/* .line 993 */
final String v1 = "cloud_turbo_sched_thermal_break_enable"; // const-string v1, "cloud_turbo_sched_thermal_break_enable"
int v2 = -2; // const/4 v2, -0x2
android.provider.Settings$System .getStringForUser ( v0,v1,v2 );
/* .line 996 */
/* .local v0, "enableStr":Ljava/lang/String; */
/* iget-boolean v1, p0, Lcom/miui/server/turbosched/TurboSchedManagerService;->mReceiveCloudData:Z */
/* if-nez v1, :cond_0 */
/* .line 997 */
return;
/* .line 999 */
} // :cond_0
if ( v0 != null) { // if-eqz v0, :cond_1
v1 = (( java.lang.String ) v0 ).isEmpty ( ); // invoke-virtual {v0}, Ljava/lang/String;->isEmpty()Z
/* if-nez v1, :cond_1 */
/* .line 1000 */
v1 = java.lang.Boolean .parseBoolean ( v0 );
/* invoke-direct {p0, v1}, Lcom/miui/server/turbosched/TurboSchedManagerService;->setThermalBreakEnable(Z)V */
/* .line 1002 */
} // :cond_1
return;
} // .end method
private void updateCloudThermalBreakThresholdProp ( ) {
/* .locals 3 */
/* .line 1016 */
v0 = this.mContext;
/* .line 1017 */
(( android.content.Context ) v0 ).getContentResolver ( ); // invoke-virtual {v0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;
/* .line 1016 */
final String v1 = "cloud_turbo_sched_thermal_break_threshold"; // const-string v1, "cloud_turbo_sched_thermal_break_threshold"
int v2 = -2; // const/4 v2, -0x2
android.provider.Settings$System .getStringForUser ( v0,v1,v2 );
/* .line 1019 */
/* .local v0, "thresholdStr":Ljava/lang/String; */
/* iget-boolean v1, p0, Lcom/miui/server/turbosched/TurboSchedManagerService;->mReceiveCloudData:Z */
/* if-nez v1, :cond_0 */
/* .line 1020 */
return;
/* .line 1022 */
} // :cond_0
/* invoke-direct {p0, v0}, Lcom/miui/server/turbosched/TurboSchedManagerService;->setThermalBreakThresholdMap(Ljava/lang/String;)V */
/* .line 1023 */
return;
} // .end method
private void updateCoreAppOptimizerEnableProp ( ) {
/* .locals 6 */
/* .line 858 */
int v0 = 0; // const/4 v0, 0x0
/* .line 859 */
/* .local v0, "coreAppOptimizerEnabled":Z */
v1 = this.mContext;
/* .line 860 */
(( android.content.Context ) v1 ).getContentResolver ( ); // invoke-virtual {v1}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;
/* .line 859 */
final String v2 = "cloud_turbo_sched_enable_core_app_optimizer"; // const-string v2, "cloud_turbo_sched_enable_core_app_optimizer"
int v3 = -2; // const/4 v3, -0x2
android.provider.Settings$System .getStringForUser ( v1,v2,v3 );
/* .line 862 */
/* .local v1, "enableStr":Ljava/lang/String; */
/* iget-boolean v2, p0, Lcom/miui/server/turbosched/TurboSchedManagerService;->mReceiveCloudData:Z */
/* if-nez v2, :cond_0 */
/* .line 863 */
return;
/* .line 865 */
} // :cond_0
final String v2 = "TurboSchedManagerService"; // const-string v2, "TurboSchedManagerService"
if ( v1 != null) { // if-eqz v1, :cond_1
v3 = (( java.lang.String ) v1 ).isEmpty ( ); // invoke-virtual {v1}, Ljava/lang/String;->isEmpty()Z
/* if-nez v3, :cond_1 */
/* .line 866 */
v0 = java.lang.Boolean .parseBoolean ( v1 );
/* .line 867 */
/* new-instance v3, Ljava/lang/StringBuilder; */
/* invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V */
final String v4 = "cloud core app optimizer set received: "; // const-string v4, "cloud core app optimizer set received: "
(( java.lang.StringBuilder ) v3 ).append ( v4 ); // invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v3 ).append ( v0 ); // invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v3 ).toString ( ); // invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
android.util.Slog .d ( v2,v3 );
/* .line 870 */
} // :cond_1
/* move v3, v0 */
/* .line 871 */
/* .local v3, "enable":I */
android.os.TurboSchedMonitor .getInstance ( );
(( android.os.TurboSchedMonitor ) v4 ).enableCoreAppOptimizer ( v3 ); // invoke-virtual {v4, v3}, Landroid/os/TurboSchedMonitor;->enableCoreAppOptimizer(I)V
/* .line 872 */
/* invoke-direct {p0, v0}, Lcom/miui/server/turbosched/TurboSchedManagerService;->enableGaea(Z)V */
/* .line 873 */
if ( v0 != null) { // if-eqz v0, :cond_2
/* iget-boolean v4, p0, Lcom/miui/server/turbosched/TurboSchedManagerService;->mRegistedForegroundReceiver:Z */
/* if-nez v4, :cond_2 */
/* .line 874 */
/* invoke-direct {p0}, Lcom/miui/server/turbosched/TurboSchedManagerService;->registeForegroundReceiver()V */
/* .line 876 */
} // :cond_2
/* invoke-direct {p0, v0}, Lcom/miui/server/turbosched/TurboSchedManagerService;->enableFrameBoostInKernel(Z)V */
/* .line 877 */
if ( v0 != null) { // if-eqz v0, :cond_3
/* .line 878 */
int v4 = 1; // const/4 v4, 0x1
/* iput-boolean v4, p0, Lcom/miui/server/turbosched/TurboSchedManagerService;->mKernelEnabelUpdate:Z */
/* .line 880 */
} // :cond_3
/* new-instance v4, Ljava/lang/StringBuilder; */
/* invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V */
final String v5 = "cloud data update done - coreapp enable: "; // const-string v5, "cloud data update done - coreapp enable: "
(( java.lang.StringBuilder ) v4 ).append ( v5 ); // invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v4 ).append ( v0 ); // invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v4 ).toString ( ); // invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
android.util.Slog .d ( v2,v4 );
/* .line 881 */
return;
} // .end method
private void updateCoreTop20AppOptimizerEnableProp ( ) {
/* .locals 5 */
/* .line 884 */
int v0 = 0; // const/4 v0, 0x0
/* .line 885 */
/* .local v0, "optimizerEnabled":Z */
v1 = this.mContext;
/* .line 886 */
(( android.content.Context ) v1 ).getContentResolver ( ); // invoke-virtual {v1}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;
/* .line 885 */
final String v2 = "cloud_turbo_sched_enable_core_top20_app_optimizer"; // const-string v2, "cloud_turbo_sched_enable_core_top20_app_optimizer"
int v3 = -2; // const/4 v3, -0x2
android.provider.Settings$System .getStringForUser ( v1,v2,v3 );
/* .line 888 */
/* .local v1, "enableStr":Ljava/lang/String; */
/* iget-boolean v2, p0, Lcom/miui/server/turbosched/TurboSchedManagerService;->mReceiveCloudData:Z */
/* if-nez v2, :cond_0 */
/* .line 889 */
return;
/* .line 891 */
} // :cond_0
final String v2 = "TurboSchedManagerService"; // const-string v2, "TurboSchedManagerService"
if ( v1 != null) { // if-eqz v1, :cond_1
v3 = (( java.lang.String ) v1 ).isEmpty ( ); // invoke-virtual {v1}, Ljava/lang/String;->isEmpty()Z
/* if-nez v3, :cond_1 */
/* .line 892 */
v0 = java.lang.Boolean .parseBoolean ( v1 );
/* .line 893 */
/* new-instance v3, Ljava/lang/StringBuilder; */
/* invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V */
final String v4 = "cloud core top20 app optimizer set received: "; // const-string v4, "cloud core top20 app optimizer set received: "
(( java.lang.StringBuilder ) v3 ).append ( v4 ); // invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v3 ).append ( v0 ); // invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v3 ).toString ( ); // invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
android.util.Slog .d ( v2,v3 );
/* .line 895 */
} // :cond_1
/* invoke-direct {p0, v0}, Lcom/miui/server/turbosched/TurboSchedManagerService;->syncTop20AppList(Z)V */
/* .line 896 */
/* new-instance v3, Ljava/lang/StringBuilder; */
/* invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V */
final String v4 = "cloud data update done - core top20 app enable: "; // const-string v4, "cloud data update done - core top20 app enable: "
(( java.lang.StringBuilder ) v3 ).append ( v4 ); // invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v3 ).append ( v0 ); // invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v3 ).toString ( ); // invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
android.util.Slog .d ( v2,v3 );
/* .line 897 */
return;
} // .end method
private void updateEnableMiuiSdkProp ( Boolean p0 ) {
/* .locals 5 */
/* .param p1, "optimizerInitEnabled" # Z */
/* .line 836 */
int v0 = 0; // const/4 v0, 0x0
/* .line 837 */
/* .local v0, "optimizerEnabled":Z */
v1 = this.mContext;
/* .line 838 */
(( android.content.Context ) v1 ).getContentResolver ( ); // invoke-virtual {v1}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;
/* .line 837 */
final String v2 = "cloud_turbo_sched_enable_v2"; // const-string v2, "cloud_turbo_sched_enable_v2"
int v3 = -2; // const/4 v3, -0x2
android.provider.Settings$System .getStringForUser ( v1,v2,v3 );
/* .line 840 */
/* .local v1, "enableStr":Ljava/lang/String; */
/* iget-boolean v2, p0, Lcom/miui/server/turbosched/TurboSchedManagerService;->mReceiveCloudData:Z */
/* if-nez v2, :cond_0 */
/* .line 841 */
return;
/* .line 843 */
} // :cond_0
final String v2 = "TurboSchedManagerService"; // const-string v2, "TurboSchedManagerService"
if ( v1 != null) { // if-eqz v1, :cond_1
v3 = (( java.lang.String ) v1 ).isEmpty ( ); // invoke-virtual {v1}, Ljava/lang/String;->isEmpty()Z
/* if-nez v3, :cond_1 */
/* .line 844 */
v0 = java.lang.Boolean .parseBoolean ( v1 );
/* .line 845 */
/* iput-boolean v0, p0, Lcom/miui/server/turbosched/TurboSchedManagerService;->mCloudControlEnabled:Z */
/* .line 846 */
/* new-instance v3, Ljava/lang/StringBuilder; */
/* invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V */
final String v4 = "cloud turbosched v2 set received: "; // const-string v4, "cloud turbosched v2 set received: "
(( java.lang.StringBuilder ) v3 ).append ( v4 ); // invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
/* iget-boolean v4, p0, Lcom/miui/server/turbosched/TurboSchedManagerService;->mCloudControlEnabled:Z */
(( java.lang.StringBuilder ) v3 ).append ( v4 ); // invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v3 ).toString ( ); // invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
android.util.Slog .d ( v2,v3 );
/* .line 847 */
/* iget-boolean v3, p0, Lcom/miui/server/turbosched/TurboSchedManagerService;->mRegistedForegroundReceiver:Z */
/* if-nez v3, :cond_1 */
/* .line 848 */
/* invoke-direct {p0}, Lcom/miui/server/turbosched/TurboSchedManagerService;->registeForegroundReceiver()V */
/* .line 852 */
} // :cond_1
/* or-int/2addr v0, p1 */
/* .line 853 */
(( com.miui.server.turbosched.TurboSchedManagerService ) p0 ).enableTurboSchedV2 ( v0 ); // invoke-virtual {p0, v0}, Lcom/miui/server/turbosched/TurboSchedManagerService;->enableTurboSchedV2(Z)V
/* .line 854 */
/* new-instance v3, Ljava/lang/StringBuilder; */
/* invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V */
final String v4 = "cloud data update done - turbosched v2: "; // const-string v4, "cloud data update done - turbosched v2: "
(( java.lang.StringBuilder ) v3 ).append ( v4 ); // invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v3 ).append ( v0 ); // invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v3 ).toString ( ); // invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
android.util.Slog .d ( v2,v3 );
/* .line 855 */
return;
} // .end method
private void updateEnableProp ( Boolean p0 ) {
/* .locals 5 */
/* .param p1, "optimizerInitEnabled" # Z */
/* .line 803 */
int v0 = 0; // const/4 v0, 0x0
/* .line 804 */
/* .local v0, "optimizerEnabled":Z */
v1 = this.mContext;
/* .line 805 */
(( android.content.Context ) v1 ).getContentResolver ( ); // invoke-virtual {v1}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;
/* .line 804 */
final String v2 = "cloud_turbo_sched_enable"; // const-string v2, "cloud_turbo_sched_enable"
int v3 = -2; // const/4 v3, -0x2
android.provider.Settings$System .getStringForUser ( v1,v2,v3 );
/* .line 807 */
/* .local v1, "enableStr":Ljava/lang/String; */
/* iget-boolean v2, p0, Lcom/miui/server/turbosched/TurboSchedManagerService;->mReceiveCloudData:Z */
/* if-nez v2, :cond_0 */
/* .line 808 */
return;
/* .line 810 */
} // :cond_0
final String v2 = "TurboSchedManagerService"; // const-string v2, "TurboSchedManagerService"
if ( v1 != null) { // if-eqz v1, :cond_1
v3 = (( java.lang.String ) v1 ).isEmpty ( ); // invoke-virtual {v1}, Ljava/lang/String;->isEmpty()Z
/* if-nez v3, :cond_1 */
/* .line 811 */
v0 = java.lang.Boolean .parseBoolean ( v1 );
/* .line 812 */
/* iput-boolean v0, p0, Lcom/miui/server/turbosched/TurboSchedManagerService;->mCloudControlEnabled:Z */
/* .line 813 */
/* new-instance v3, Ljava/lang/StringBuilder; */
/* invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V */
final String v4 = "cloud turbosched v1 set received: "; // const-string v4, "cloud turbosched v1 set received: "
(( java.lang.StringBuilder ) v3 ).append ( v4 ); // invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
/* iget-boolean v4, p0, Lcom/miui/server/turbosched/TurboSchedManagerService;->mCloudControlEnabled:Z */
(( java.lang.StringBuilder ) v3 ).append ( v4 ); // invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v3 ).toString ( ); // invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
android.util.Slog .d ( v2,v3 );
/* .line 816 */
} // :cond_1
/* invoke-direct {p0, v0}, Lcom/miui/server/turbosched/TurboSchedManagerService;->enableTurboSched(Z)V */
/* .line 817 */
/* new-instance v3, Ljava/lang/StringBuilder; */
/* invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V */
final String v4 = "cloud data update done - turbosched v1: "; // const-string v4, "cloud data update done - turbosched v1: "
(( java.lang.StringBuilder ) v3 ).append ( v4 ); // invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v3 ).append ( v0 ); // invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v3 ).toString ( ); // invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
android.util.Slog .d ( v2,v3 );
/* .line 819 */
/* invoke-direct {p0, v0}, Lcom/miui/server/turbosched/TurboSchedManagerService;->updateEnableMiuiSdkProp(Z)V */
/* .line 821 */
/* if-nez p1, :cond_2 */
/* .line 822 */
/* move v2, v0 */
/* .line 823 */
/* .local v2, "enable":I */
android.os.TurboSchedMonitor .getInstance ( );
(( android.os.TurboSchedMonitor ) v3 ).enableCoreAppOptimizer ( v2 ); // invoke-virtual {v3, v2}, Landroid/os/TurboSchedMonitor;->enableCoreAppOptimizer(I)V
/* .line 824 */
/* invoke-direct {p0, v0}, Lcom/miui/server/turbosched/TurboSchedManagerService;->enableFrameBoostInKernel(Z)V */
/* .line 825 */
if ( v0 != null) { // if-eqz v0, :cond_2
/* .line 826 */
int v3 = 1; // const/4 v3, 0x1
/* iput-boolean v3, p0, Lcom/miui/server/turbosched/TurboSchedManagerService;->mKernelEnabelUpdate:Z */
/* .line 830 */
} // .end local v2 # "enable":I
} // :cond_2
if ( v0 != null) { // if-eqz v0, :cond_3
/* iget-boolean v2, p0, Lcom/miui/server/turbosched/TurboSchedManagerService;->mRegistedForegroundReceiver:Z */
/* if-nez v2, :cond_3 */
/* .line 831 */
/* invoke-direct {p0}, Lcom/miui/server/turbosched/TurboSchedManagerService;->registeForegroundReceiver()V */
/* .line 833 */
} // :cond_3
return;
} // .end method
private void updateLocalPolicyListProp ( ) {
/* .locals 6 */
/* .line 978 */
final String v0 = "persist.sys.turbosched.policy_list"; // const-string v0, "persist.sys.turbosched.policy_list"
final String v1 = ""; // const-string v1, ""
android.os.SystemProperties .get ( v0,v1 );
/* .line 979 */
/* .local v0, "str":Ljava/lang/String; */
if ( v0 != null) { // if-eqz v0, :cond_1
v1 = (( java.lang.String ) v0 ).isEmpty ( ); // invoke-virtual {v0}, Ljava/lang/String;->isEmpty()Z
/* if-nez v1, :cond_1 */
/* .line 980 */
final String v1 = ","; // const-string v1, ","
(( java.lang.String ) v0 ).split ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;
/* .line 981 */
/* .local v1, "pkgName":[Ljava/lang/String; */
/* new-instance v2, Ljava/util/ArrayList; */
/* invoke-direct {v2}, Ljava/util/ArrayList;-><init>()V */
/* .line 982 */
/* .local v2, "policyList":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;" */
/* array-length v3, v1 */
/* if-lez v3, :cond_1 */
/* .line 983 */
/* array-length v3, v1 */
int v4 = 0; // const/4 v4, 0x0
} // :goto_0
/* if-ge v4, v3, :cond_0 */
/* aget-object v5, v1, v4 */
/* .line 984 */
/* .local v5, "name":Ljava/lang/String; */
/* .line 983 */
} // .end local v5 # "name":Ljava/lang/String;
/* add-int/lit8 v4, v4, 0x1 */
/* .line 986 */
} // :cond_0
com.miui.server.turbosched.TurboSchedConfig .setPolicyList ( v2 );
/* .line 987 */
this.mPolicyListBackup = v2;
/* .line 990 */
} // .end local v1 # "pkgName":[Ljava/lang/String;
} // .end local v2 # "policyList":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;"
} // :cond_1
return;
} // .end method
private void updateLocalProp ( ) {
/* .locals 1 */
/* .line 794 */
android.os.TurboSchedMonitor .getInstance ( );
v0 = (( android.os.TurboSchedMonitor ) v0 ).isCoreAppOptimizerEnabled ( ); // invoke-virtual {v0}, Landroid/os/TurboSchedMonitor;->isCoreAppOptimizerEnabled()Z
/* invoke-direct {p0, v0}, Lcom/miui/server/turbosched/TurboSchedManagerService;->enableFrameBoostInKernel(Z)V */
/* .line 795 */
android.os.TurboSchedMonitor .getInstance ( );
v0 = (( android.os.TurboSchedMonitor ) v0 ).isCoreAppOptimizerEnabled ( ); // invoke-virtual {v0}, Landroid/os/TurboSchedMonitor;->isCoreAppOptimizerEnabled()Z
if ( v0 != null) { // if-eqz v0, :cond_0
/* .line 796 */
int v0 = 1; // const/4 v0, 0x1
/* iput-boolean v0, p0, Lcom/miui/server/turbosched/TurboSchedManagerService;->mKernelEnabelUpdate:Z */
/* .line 798 */
} // :cond_0
/* iget-boolean v0, p0, Lcom/miui/server/turbosched/TurboSchedManagerService;->mThermalBreakEnabled:Z */
/* invoke-direct {p0, v0}, Lcom/miui/server/turbosched/TurboSchedManagerService;->setThermalBreakEnable(Z)V */
/* .line 799 */
/* invoke-direct {p0}, Lcom/miui/server/turbosched/TurboSchedManagerService;->updateLocalPolicyListProp()V */
/* .line 800 */
return;
} // .end method
private Boolean writeTidsToPath ( java.lang.String p0, Integer[] p1 ) {
/* .locals 5 */
/* .param p1, "path" # Ljava/lang/String; */
/* .param p2, "tids" # [I */
/* .line 1998 */
/* new-instance v0, Ljava/lang/StringBuilder; */
/* invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V */
/* .line 1999 */
/* .local v0, "sb":Ljava/lang/StringBuilder; */
/* array-length v1, p2 */
int v2 = 0; // const/4 v2, 0x0
} // :goto_0
/* if-ge v2, v1, :cond_0 */
/* aget v3, p2, v2 */
/* .line 2000 */
/* .local v3, "i":I */
(( java.lang.StringBuilder ) v0 ).append ( v3 ); // invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;
/* .line 2001 */
final String v4 = ":"; // const-string v4, ":"
(( java.lang.StringBuilder ) v0 ).append ( v4 ); // invoke-virtual {v0, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
/* .line 1999 */
} // .end local v3 # "i":I
/* add-int/lit8 v2, v2, 0x1 */
/* .line 2003 */
} // :cond_0
v1 = (( java.lang.StringBuilder ) v0 ).length ( ); // invoke-virtual {v0}, Ljava/lang/StringBuilder;->length()I
/* if-lez v1, :cond_1 */
/* .line 2004 */
v1 = (( java.lang.StringBuilder ) v0 ).length ( ); // invoke-virtual {v0}, Ljava/lang/StringBuilder;->length()I
/* add-int/lit8 v1, v1, -0x1 */
(( java.lang.StringBuilder ) v0 ).deleteCharAt ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->deleteCharAt(I)Ljava/lang/StringBuilder;
/* .line 2006 */
} // :cond_1
final String v1 = "M-Tids"; // const-string v1, "M-Tids"
(( java.lang.StringBuilder ) v0 ).toString ( ); // invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
v1 = /* invoke-direct {p0, v1, p1, v2}, Lcom/miui/server/turbosched/TurboSchedManagerService;->writeTurboSchedNode(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Z */
} // .end method
private Boolean writeTurboSchedNode ( java.lang.String p0, java.lang.String p1, java.lang.String p2 ) {
/* .locals 6 */
/* .param p1, "logTag" # Ljava/lang/String; */
/* .param p2, "path" # Ljava/lang/String; */
/* .param p3, "value" # Ljava/lang/String; */
/* .line 536 */
/* new-instance v0, Ljava/io/File; */
/* invoke-direct {v0, p2}, Ljava/io/File;-><init>(Ljava/lang/String;)V */
/* .line 537 */
/* .local v0, "file":Ljava/io/File; */
v1 = (( java.io.File ) v0 ).exists ( ); // invoke-virtual {v0}, Ljava/io/File;->exists()Z
int v2 = 0; // const/4 v2, 0x0
/* if-nez v1, :cond_0 */
/* .line 538 */
/* .line 540 */
} // :cond_0
try { // :try_start_0
/* new-instance v1, Ljava/io/PrintWriter; */
/* invoke-direct {v1, p2}, Ljava/io/PrintWriter;-><init>(Ljava/lang/String;)V */
/* :try_end_0 */
/* .catch Ljava/io/IOException; {:try_start_0 ..:try_end_0} :catch_0 */
/* .line 541 */
/* .local v1, "writer":Ljava/io/PrintWriter; */
try { // :try_start_1
(( java.io.PrintWriter ) v1 ).write ( p3 ); // invoke-virtual {v1, p3}, Ljava/io/PrintWriter;->write(Ljava/lang/String;)V
/* .line 542 */
v3 = this.mHistoryLog;
/* new-instance v4, Ljava/lang/StringBuilder; */
/* invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V */
final String v5 = "[ "; // const-string v5, "[ "
(( java.lang.StringBuilder ) v4 ).append ( v5 ); // invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v4 ).append ( p1 ); // invoke-virtual {v4, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
final String v5 = " ] write message : "; // const-string v5, " ] write message : "
(( java.lang.StringBuilder ) v4 ).append ( v5 ); // invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v4 ).append ( p3 ); // invoke-virtual {v4, p3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v4 ).toString ( ); // invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
(( android.util.LocalLog ) v3 ).log ( v4 ); // invoke-virtual {v3, v4}, Landroid/util/LocalLog;->log(Ljava/lang/String;)V
/* :try_end_1 */
/* .catchall {:try_start_1 ..:try_end_1} :catchall_0 */
/* .line 543 */
/* nop */
/* .line 544 */
try { // :try_start_2
(( java.io.PrintWriter ) v1 ).close ( ); // invoke-virtual {v1}, Ljava/io/PrintWriter;->close()V
/* :try_end_2 */
/* .catch Ljava/io/IOException; {:try_start_2 ..:try_end_2} :catch_0 */
/* .line 543 */
int v2 = 1; // const/4 v2, 0x1
/* .line 540 */
/* :catchall_0 */
/* move-exception v3 */
try { // :try_start_3
(( java.io.PrintWriter ) v1 ).close ( ); // invoke-virtual {v1}, Ljava/io/PrintWriter;->close()V
/* :try_end_3 */
/* .catchall {:try_start_3 ..:try_end_3} :catchall_1 */
/* :catchall_1 */
/* move-exception v4 */
try { // :try_start_4
(( java.lang.Throwable ) v3 ).addSuppressed ( v4 ); // invoke-virtual {v3, v4}, Ljava/lang/Throwable;->addSuppressed(Ljava/lang/Throwable;)V
} // .end local v0 # "file":Ljava/io/File;
} // .end local p0 # "this":Lcom/miui/server/turbosched/TurboSchedManagerService;
} // .end local p1 # "logTag":Ljava/lang/String;
} // .end local p2 # "path":Ljava/lang/String;
} // .end local p3 # "value":Ljava/lang/String;
} // :goto_0
/* throw v3 */
/* :try_end_4 */
/* .catch Ljava/io/IOException; {:try_start_4 ..:try_end_4} :catch_0 */
/* .line 544 */
} // .end local v1 # "writer":Ljava/io/PrintWriter;
/* .restart local v0 # "file":Ljava/io/File; */
/* .restart local p0 # "this":Lcom/miui/server/turbosched/TurboSchedManagerService; */
/* .restart local p1 # "logTag":Ljava/lang/String; */
/* .restart local p2 # "path":Ljava/lang/String; */
/* .restart local p3 # "value":Ljava/lang/String; */
/* :catch_0 */
/* move-exception v1 */
/* .line 545 */
/* .local v1, "e":Ljava/io/IOException; */
/* new-instance v3, Ljava/lang/StringBuilder; */
/* invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V */
final String v4 = "Failed to write path : "; // const-string v4, "Failed to write path : "
(( java.lang.StringBuilder ) v3 ).append ( v4 ); // invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v3 ).append ( p2 ); // invoke-virtual {v3, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
final String v4 = " value : "; // const-string v4, " value : "
(( java.lang.StringBuilder ) v3 ).append ( v4 ); // invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v3 ).append ( p3 ); // invoke-virtual {v3, p3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v3 ).toString ( ); // invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
final String v4 = "TurboSchedManagerService"; // const-string v4, "TurboSchedManagerService"
android.util.Slog .w ( v4,v3,v1 );
/* .line 546 */
} // .end method
/* # virtual methods */
public void breakThermlimit ( Integer p0, Long p1 ) {
/* .locals 7 */
/* .param p1, "boost" # I */
/* .param p2, "time" # J */
/* .line 1395 */
android.os.TurboSchedMonitor .getInstance ( );
v0 = (( android.os.TurboSchedMonitor ) v0 ).isCoreAppOptimizerEnabled ( ); // invoke-virtual {v0}, Landroid/os/TurboSchedMonitor;->isCoreAppOptimizerEnabled()Z
if ( v0 != null) { // if-eqz v0, :cond_5
/* iget-boolean v0, p0, Lcom/miui/server/turbosched/TurboSchedManagerService;->mThermalBreakEnabled:Z */
/* if-nez v0, :cond_0 */
/* goto/16 :goto_1 */
/* .line 1398 */
} // :cond_0
final String v0 = "breakThermlimit"; // const-string v0, "breakThermlimit"
java.lang.String .valueOf ( p1 );
/* invoke-direct {p0, v0, v1}, Lcom/miui/server/turbosched/TurboSchedManagerService;->sendTraceBeginMsg(Ljava/lang/String;Ljava/lang/String;)V */
/* .line 1399 */
int v0 = 0; // const/4 v0, 0x0
/* .line 1400 */
/* .local v0, "isSuccess":Z */
final String v1 = ""; // const-string v1, ""
/* .line 1402 */
/* .local v1, "str":Ljava/lang/String; */
android.os.TurboSchedMonitor .getInstance ( );
v2 = (( android.os.TurboSchedMonitor ) v2 ).isDebugMode ( ); // invoke-virtual {v2}, Landroid/os/TurboSchedMonitor;->isDebugMode()Z
final String v3 = "TurboSchedManagerService"; // const-string v3, "TurboSchedManagerService"
if ( v2 != null) { // if-eqz v2, :cond_1
/* .line 1403 */
final String v2 = "breakThermlimit, begin"; // const-string v2, "breakThermlimit, begin"
android.util.Slog .d ( v3,v2 );
/* .line 1405 */
} // :cond_1
/* if-nez p1, :cond_2 */
/* .line 1406 */
final String v1 = "boost:0"; // const-string v1, "boost:0"
/* .line 1408 */
} // :cond_2
final String v2 = "boost:1"; // const-string v2, "boost:1"
/* .line 1409 */
/* .local v2, "strBoost":Ljava/lang/String; */
/* invoke-direct {p0}, Lcom/miui/server/turbosched/TurboSchedManagerService;->getThermalBreakString()Ljava/lang/String; */
/* .line 1411 */
/* .local v4, "strThermalBreak":Ljava/lang/String; */
/* new-instance v5, Ljava/lang/StringBuilder; */
/* invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V */
/* .line 1412 */
/* .local v5, "sb":Ljava/lang/StringBuilder; */
(( java.lang.StringBuilder ) v5 ).append ( v2 ); // invoke-virtual {v5, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
/* .line 1413 */
(( java.lang.StringBuilder ) v5 ).append ( v4 ); // invoke-virtual {v5, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
/* .line 1414 */
final String v6 = " time:"; // const-string v6, " time:"
(( java.lang.StringBuilder ) v5 ).append ( v6 ); // invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
/* .line 1415 */
(( java.lang.StringBuilder ) v5 ).append ( p2, p3 ); // invoke-virtual {v5, p2, p3}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;
/* .line 1416 */
(( java.lang.StringBuilder ) v5 ).toString ( ); // invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
/* .line 1418 */
} // .end local v2 # "strBoost":Ljava/lang/String;
} // .end local v4 # "strThermalBreak":Ljava/lang/String;
} // .end local v5 # "sb":Ljava/lang/StringBuilder;
} // :goto_0
android.os.TurboSchedMonitor .getInstance ( );
v2 = (( android.os.TurboSchedMonitor ) v2 ).isDebugMode ( ); // invoke-virtual {v2}, Landroid/os/TurboSchedMonitor;->isDebugMode()Z
if ( v2 != null) { // if-eqz v2, :cond_3
/* .line 1419 */
/* new-instance v2, Ljava/lang/StringBuilder; */
/* invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V */
final String v4 = "breakThermlimit, str:"; // const-string v4, "breakThermlimit, str:"
(( java.lang.StringBuilder ) v2 ).append ( v4 ); // invoke-virtual {v2, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v2 ).append ( v1 ); // invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v2 ).toString ( ); // invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
android.util.Slog .d ( v3,v2 );
/* .line 1420 */
} // :cond_3
final String v2 = "BT-Limit"; // const-string v2, "BT-Limit"
final String v4 = "/sys/class/thermal/thermal_message/boost"; // const-string v4, "/sys/class/thermal/thermal_message/boost"
v0 = /* invoke-direct {p0, v2, v4, v1}, Lcom/miui/server/turbosched/TurboSchedManagerService;->writeTurboSchedNode(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Z */
/* .line 1421 */
/* invoke-direct {p0}, Lcom/miui/server/turbosched/TurboSchedManagerService;->sendTraceEndMsg()V */
/* .line 1422 */
android.os.TurboSchedMonitor .getInstance ( );
v2 = (( android.os.TurboSchedMonitor ) v2 ).isDebugMode ( ); // invoke-virtual {v2}, Landroid/os/TurboSchedMonitor;->isDebugMode()Z
if ( v2 != null) { // if-eqz v2, :cond_4
/* .line 1423 */
/* new-instance v2, Ljava/lang/StringBuilder; */
/* invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V */
final String v4 = "breakThermlimit, isSuccess:"; // const-string v4, "breakThermlimit, isSuccess:"
(( java.lang.StringBuilder ) v2 ).append ( v4 ); // invoke-virtual {v2, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v2 ).append ( v0 ); // invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;
final String v4 = ", FluencyOptimizer"; // const-string v4, ", FluencyOptimizer"
(( java.lang.StringBuilder ) v2 ).append ( v4 ); // invoke-virtual {v2, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v2 ).toString ( ); // invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
android.util.Slog .d ( v3,v2 );
/* .line 1424 */
} // :cond_4
return;
/* .line 1396 */
} // .end local v0 # "isSuccess":Z
} // .end local v1 # "str":Ljava/lang/String;
} // :cond_5
} // :goto_1
return;
} // .end method
public Integer checkBoostPermission ( ) {
/* .locals 8 */
/* .line 1329 */
v0 = android.os.Binder .getCallingUid ( );
/* .line 1330 */
/* .local v0, "uid":I */
v1 = android.os.Binder .getCallingPid ( );
/* .line 1331 */
/* .local v1, "pid":I */
/* invoke-direct {p0, v1}, Lcom/miui/server/turbosched/TurboSchedManagerService;->getProcessNameByPid(I)Ljava/lang/String; */
/* .line 1332 */
/* .local v2, "str":Ljava/lang/String; */
int v3 = -1; // const/4 v3, -0x1
/* .line 1334 */
/* .local v3, "ret":I */
/* iget-wide v4, p0, Lcom/miui/server/turbosched/TurboSchedManagerService;->mFgCoreUid:J */
/* const-wide/16 v6, -0x1 */
/* cmp-long v4, v4, v6 */
if ( v4 != null) { // if-eqz v4, :cond_1
/* .line 1335 */
if ( v2 != null) { // if-eqz v2, :cond_0
android.os.TurboSchedMonitor .getInstance ( );
v4 = (( android.os.TurboSchedMonitor ) v4 ).isCoreApp ( v2 ); // invoke-virtual {v4, v2}, Landroid/os/TurboSchedMonitor;->isCoreApp(Ljava/lang/String;)Z
if ( v4 != null) { // if-eqz v4, :cond_0
/* int-to-long v4, v0 */
/* iget-wide v6, p0, Lcom/miui/server/turbosched/TurboSchedManagerService;->mFgCoreUid:J */
/* cmp-long v4, v4, v6 */
/* if-nez v4, :cond_0 */
/* .line 1336 */
int v3 = 1; // const/4 v3, 0x1
/* .line 1338 */
} // :cond_0
int v3 = 0; // const/4 v3, 0x0
/* .line 1342 */
} // :cond_1
} // :goto_0
android.os.TurboSchedMonitor .getInstance ( );
v4 = (( android.os.TurboSchedMonitor ) v4 ).isDebugMode ( ); // invoke-virtual {v4}, Landroid/os/TurboSchedMonitor;->isDebugMode()Z
if ( v4 != null) { // if-eqz v4, :cond_2
/* .line 1343 */
/* new-instance v4, Ljava/lang/StringBuilder; */
/* invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V */
final String v5 = "checkBoostPermission, uid:"; // const-string v5, "checkBoostPermission, uid:"
(( java.lang.StringBuilder ) v4 ).append ( v5 ); // invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v4 ).append ( v0 ); // invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;
final String v5 = ", pid:"; // const-string v5, ", pid:"
(( java.lang.StringBuilder ) v4 ).append ( v5 ); // invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v4 ).append ( v1 ); // invoke-virtual {v4, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;
final String v5 = ", str:"; // const-string v5, ", str:"
(( java.lang.StringBuilder ) v4 ).append ( v5 ); // invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v4 ).append ( v2 ); // invoke-virtual {v4, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
final String v5 = ", mFgCoreUid:"; // const-string v5, ", mFgCoreUid:"
(( java.lang.StringBuilder ) v4 ).append ( v5 ); // invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
/* iget-wide v5, p0, Lcom/miui/server/turbosched/TurboSchedManagerService;->mFgCoreUid:J */
(( java.lang.StringBuilder ) v4 ).append ( v5, v6 ); // invoke-virtual {v4, v5, v6}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;
final String v5 = ", ret:"; // const-string v5, ", ret:"
(( java.lang.StringBuilder ) v4 ).append ( v5 ); // invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v4 ).append ( v3 ); // invoke-virtual {v4, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v4 ).toString ( ); // invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
final String v5 = "TurboSchedManagerService"; // const-string v5, "TurboSchedManagerService"
android.util.Slog .d ( v5,v4 );
/* .line 1345 */
} // :cond_2
} // .end method
public Boolean checkPackagePermission ( java.lang.String p0 ) {
/* .locals 1 */
/* .param p1, "packageName" # Ljava/lang/String; */
/* .line 1350 */
if ( p1 != null) { // if-eqz p1, :cond_0
android.os.TurboSchedMonitor .getInstance ( );
v0 = (( android.os.TurboSchedMonitor ) v0 ).isCoreApp ( p1 ); // invoke-virtual {v0, p1}, Landroid/os/TurboSchedMonitor;->isCoreApp(Ljava/lang/String;)Z
if ( v0 != null) { // if-eqz v0, :cond_0
/* .line 1351 */
int v0 = 1; // const/4 v0, 0x1
/* .line 1353 */
} // :cond_0
int v0 = 0; // const/4 v0, 0x0
} // .end method
protected void dump ( java.io.FileDescriptor p0, java.io.PrintWriter p1, java.lang.String[] p2 ) {
/* .locals 2 */
/* .param p1, "fd" # Ljava/io/FileDescriptor; */
/* .param p2, "pw" # Ljava/io/PrintWriter; */
/* .param p3, "args" # [Ljava/lang/String; */
/* .line 1552 */
v0 = this.mContext;
final String v1 = "TurboSchedManagerService"; // const-string v1, "TurboSchedManagerService"
v0 = com.android.internal.util.DumpUtils .checkDumpPermission ( v0,v1,p2 );
/* if-nez v0, :cond_0 */
/* .line 1553 */
return;
/* .line 1555 */
} // :cond_0
v0 = /* invoke-direct {p0, p1, p2, p3}, Lcom/miui/server/turbosched/TurboSchedManagerService;->parseDumpCommand(Ljava/io/FileDescriptor;Ljava/io/PrintWriter;[Ljava/lang/String;)Z */
/* .line 1556 */
/* .local v0, "handled":Z */
if ( v0 != null) { // if-eqz v0, :cond_1
/* .line 1557 */
return;
/* .line 1560 */
} // :cond_1
/* invoke-direct {p0, p2}, Lcom/miui/server/turbosched/TurboSchedManagerService;->dumpConfig(Ljava/io/PrintWriter;)V */
/* .line 1562 */
final String v1 = "-----------------------history----------------------------"; // const-string v1, "-----------------------history----------------------------"
(( java.io.PrintWriter ) p2 ).println ( v1 ); // invoke-virtual {p2, v1}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V
/* .line 1563 */
v1 = this.mHistoryLog;
(( android.util.LocalLog ) v1 ).dump ( p2 ); // invoke-virtual {v1, p2}, Landroid/util/LocalLog;->dump(Ljava/io/PrintWriter;)V
/* .line 1564 */
return;
} // .end method
protected void enableTurboSchedV2 ( Boolean p0 ) {
/* .locals 2 */
/* .param p1, "enable" # Z */
/* .line 1993 */
/* iput-boolean p1, p0, Lcom/miui/server/turbosched/TurboSchedManagerService;->mTurboEnabledV2:Z */
/* .line 1994 */
final String v0 = "persist.sys.turbosched.enable_v2"; // const-string v0, "persist.sys.turbosched.enable_v2"
java.lang.Boolean .toString ( p1 );
android.os.SystemProperties .set ( v0,v1 );
/* .line 1995 */
return;
} // .end method
public Integer frameTurboMarkScene ( java.lang.String p0, Boolean p1 ) {
/* .locals 6 */
/* .param p1, "sceneId" # Ljava/lang/String; */
/* .param p2, "start" # Z */
/* .line 1169 */
v0 = /* invoke-direct {p0}, Lcom/miui/server/turbosched/TurboSchedManagerService;->isTurboEnabled()Z */
final String v1 = ",start: "; // const-string v1, ",start: "
if ( v0 != null) { // if-eqz v0, :cond_2
com.miui.server.turbosched.TurboSchedConfig .getPolicyList ( );
v0 = final String v2 = "frame_turbo"; // const-string v2, "frame_turbo"
/* if-nez v0, :cond_0 */
/* .line 1174 */
} // :cond_0
v0 = android.os.Binder .getCallingUid ( );
/* .line 1175 */
/* .local v0, "uid":I */
v2 = this.mContext;
(( android.content.Context ) v2 ).getPackageManager ( ); // invoke-virtual {v2}, Landroid/content/Context;->getPackageManager()Landroid/content/pm/PackageManager;
(( android.content.pm.PackageManager ) v2 ).getNameForUid ( v0 ); // invoke-virtual {v2, v0}, Landroid/content/pm/PackageManager;->getNameForUid(I)Ljava/lang/String;
/* .line 1177 */
/* .local v2, "packageName":Ljava/lang/String; */
v3 = /* invoke-direct {p0, v2}, Lcom/miui/server/turbosched/TurboSchedManagerService;->isForeground(Ljava/lang/String;)Z */
/* if-nez v3, :cond_1 */
/* .line 1178 */
v3 = this.mHistoryLog;
/* new-instance v4, Ljava/lang/StringBuilder; */
/* invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V */
final String v5 = "TS [MARK] : failed, R: not foreground, sceneId: "; // const-string v5, "TS [MARK] : failed, R: not foreground, sceneId: "
(( java.lang.StringBuilder ) v4 ).append ( v5 ); // invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v4 ).append ( p1 ); // invoke-virtual {v4, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v4 ).append ( v1 ); // invoke-virtual {v4, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v1 ).append ( p2 ); // invoke-virtual {v1, p2}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v1 ).toString ( ); // invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
(( android.util.LocalLog ) v3 ).log ( v1 ); // invoke-virtual {v3, v1}, Landroid/util/LocalLog;->log(Ljava/lang/String;)V
/* .line 1179 */
int v1 = -6; // const/4 v1, -0x6
/* .line 1181 */
} // :cond_1
v1 = this.mFrameTurboAction;
v1 = (( com.miui.server.turbosched.FrameTurboAction ) v1 ).markScene ( v2, p1, p2 ); // invoke-virtual {v1, v2, p1, p2}, Lcom/miui/server/turbosched/FrameTurboAction;->markScene(Ljava/lang/String;Ljava/lang/String;Z)I
/* .line 1170 */
} // .end local v0 # "uid":I
} // .end local v2 # "packageName":Ljava/lang/String;
} // :cond_2
} // :goto_0
v0 = this.mHistoryLog;
/* new-instance v2, Ljava/lang/StringBuilder; */
/* invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V */
final String v3 = "TS [MARK] : failed, R: not enable, sceneId: "; // const-string v3, "TS [MARK] : failed, R: not enable, sceneId: "
(( java.lang.StringBuilder ) v2 ).append ( v3 ); // invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v2 ).append ( p1 ); // invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v2 ).append ( v1 ); // invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v1 ).append ( p2 ); // invoke-virtual {v1, p2}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v1 ).toString ( ); // invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
(( android.util.LocalLog ) v0 ).log ( v1 ); // invoke-virtual {v0, v1}, Landroid/util/LocalLog;->log(Ljava/lang/String;)V
/* .line 1171 */
int v0 = -1; // const/4 v0, -0x1
} // .end method
public void frameTurboRegisterSceneCallback ( miui.turbosched.ITurboSchedManager$IFrameTurboSceneCallback p0 ) {
/* .locals 2 */
/* .param p1, "callback" # Lmiui/turbosched/ITurboSchedManager$IFrameTurboSceneCallback; */
/* .line 1206 */
v0 = /* invoke-direct {p0}, Lcom/miui/server/turbosched/TurboSchedManagerService;->isTurboEnabled()Z */
if ( v0 != null) { // if-eqz v0, :cond_1
com.miui.server.turbosched.TurboSchedConfig .getPolicyList ( );
v0 = final String v1 = "frame_turbo"; // const-string v1, "frame_turbo"
/* if-nez v0, :cond_0 */
/* .line 1210 */
} // :cond_0
v0 = this.mFrameTurboAction;
(( com.miui.server.turbosched.FrameTurboAction ) v0 ).registerSceneCallback ( p1 ); // invoke-virtual {v0, p1}, Lcom/miui/server/turbosched/FrameTurboAction;->registerSceneCallback(Lmiui/turbosched/ITurboSchedManager$IFrameTurboSceneCallback;)V
/* .line 1211 */
return;
/* .line 1207 */
} // :cond_1
} // :goto_0
v0 = this.mHistoryLog;
final String v1 = "TS [FRAME] : failed, R: not enable"; // const-string v1, "TS [FRAME] : failed, R: not enable"
(( android.util.LocalLog ) v0 ).log ( v1 ); // invoke-virtual {v0, v1}, Landroid/util/LocalLog;->log(Ljava/lang/String;)V
/* .line 1208 */
return;
} // .end method
public Integer frameTurboTrigger ( Boolean p0, java.util.List p1, java.util.List p2 ) {
/* .locals 5 */
/* .param p1, "turbo" # Z */
/* .annotation system Ldalvik/annotation/Signature; */
/* value = { */
/* "(Z", */
/* "Ljava/util/List<", */
/* "Ljava/lang/String;", */
/* ">;", */
/* "Ljava/util/List<", */
/* "Ljava/lang/String;", */
/* ">;)I" */
/* } */
} // .end annotation
/* .line 1186 */
/* .local p2, "uiThreads":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;" */
/* .local p3, "scenes":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;" */
v0 = /* invoke-direct {p0}, Lcom/miui/server/turbosched/TurboSchedManagerService;->isTurboEnabled()Z */
if ( v0 != null) { // if-eqz v0, :cond_4
com.miui.server.turbosched.TurboSchedConfig .getPolicyList ( );
v0 = final String v1 = "frame_turbo"; // const-string v1, "frame_turbo"
/* if-nez v0, :cond_0 */
/* .line 1190 */
} // :cond_0
v0 = android.os.Binder .getCallingPid ( );
/* .line 1191 */
/* .local v0, "pid":I */
v1 = if ( p2 != null) { // if-eqz p2, :cond_1
/* if-nez v1, :cond_2 */
/* .line 1192 */
} // :cond_1
/* new-instance v1, Ljava/util/ArrayList; */
/* invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V */
/* move-object p2, v1 */
/* .line 1193 */
java.lang.String .valueOf ( v0 );
/* .line 1195 */
} // :cond_2
v1 = android.os.Binder .getCallingUid ( );
/* .line 1196 */
/* .local v1, "uid":I */
v2 = this.mContext;
(( android.content.Context ) v2 ).getPackageManager ( ); // invoke-virtual {v2}, Landroid/content/Context;->getPackageManager()Landroid/content/pm/PackageManager;
(( android.content.pm.PackageManager ) v2 ).getNameForUid ( v1 ); // invoke-virtual {v2, v1}, Landroid/content/pm/PackageManager;->getNameForUid(I)Ljava/lang/String;
/* .line 1197 */
/* .local v2, "packageName":Ljava/lang/String; */
v3 = /* invoke-direct {p0, v2}, Lcom/miui/server/turbosched/TurboSchedManagerService;->isForeground(Ljava/lang/String;)Z */
/* if-nez v3, :cond_3 */
/* .line 1198 */
v3 = this.mHistoryLog;
final String v4 = "TS [FRAME] : failed, R: not foreground"; // const-string v4, "TS [FRAME] : failed, R: not foreground"
(( android.util.LocalLog ) v3 ).log ( v4 ); // invoke-virtual {v3, v4}, Landroid/util/LocalLog;->log(Ljava/lang/String;)V
/* .line 1199 */
int v3 = -6; // const/4 v3, -0x6
/* .line 1201 */
} // :cond_3
v3 = this.mFrameTurboAction;
v3 = (( com.miui.server.turbosched.FrameTurboAction ) v3 ).triggerFrameTurbo ( v2, p1, p2, p3 ); // invoke-virtual {v3, v2, p1, p2, p3}, Lcom/miui/server/turbosched/FrameTurboAction;->triggerFrameTurbo(Ljava/lang/String;ZLjava/util/List;Ljava/util/List;)I
/* .line 1187 */
} // .end local v0 # "pid":I
} // .end local v1 # "uid":I
} // .end local v2 # "packageName":Ljava/lang/String;
} // :cond_4
} // :goto_0
v0 = this.mHistoryLog;
final String v1 = "TS [FRAME] : failed, R: not enable"; // const-string v1, "TS [FRAME] : failed, R: not enable"
(( android.util.LocalLog ) v0 ).log ( v1 ); // invoke-virtual {v0, v1}, Landroid/util/LocalLog;->log(Ljava/lang/String;)V
/* .line 1188 */
int v0 = -1; // const/4 v0, -0x1
} // .end method
public void frameTurboUnregisterSceneCallback ( miui.turbosched.ITurboSchedManager$IFrameTurboSceneCallback p0 ) {
/* .locals 2 */
/* .param p1, "callback" # Lmiui/turbosched/ITurboSchedManager$IFrameTurboSceneCallback; */
/* .line 1215 */
v0 = /* invoke-direct {p0}, Lcom/miui/server/turbosched/TurboSchedManagerService;->isTurboEnabled()Z */
if ( v0 != null) { // if-eqz v0, :cond_1
com.miui.server.turbosched.TurboSchedConfig .getPolicyList ( );
v0 = final String v1 = "frame_turbo"; // const-string v1, "frame_turbo"
/* if-nez v0, :cond_0 */
/* .line 1219 */
} // :cond_0
v0 = this.mFrameTurboAction;
(( com.miui.server.turbosched.FrameTurboAction ) v0 ).unregisterSceneCallback ( p1 ); // invoke-virtual {v0, p1}, Lcom/miui/server/turbosched/FrameTurboAction;->unregisterSceneCallback(Lmiui/turbosched/ITurboSchedManager$IFrameTurboSceneCallback;)V
/* .line 1220 */
return;
/* .line 1216 */
} // :cond_1
} // :goto_0
v0 = this.mHistoryLog;
final String v1 = "TS [FRAME] : failed, R: not enable"; // const-string v1, "TS [FRAME] : failed, R: not enable"
(( android.util.LocalLog ) v0 ).log ( v1 ); // invoke-virtual {v0, v1}, Landroid/util/LocalLog;->log(Ljava/lang/String;)V
/* .line 1217 */
return;
} // .end method
public java.lang.String getApiVersion ( ) {
/* .locals 1 */
/* .line 1102 */
/* const-string/jumbo v0, "v4.0.0" */
} // .end method
public java.util.List getCallerAllowList ( ) {
/* .locals 1 */
/* .annotation system Ldalvik/annotation/Signature; */
/* value = { */
/* "()", */
/* "Ljava/util/List<", */
/* "Ljava/lang/String;", */
/* ">;" */
/* } */
} // .end annotation
/* .line 1372 */
v0 = this.mCallerAllowList;
} // .end method
public java.util.List getCoreAppList ( ) {
/* .locals 1 */
/* .annotation system Ldalvik/annotation/Signature; */
/* value = { */
/* "()", */
/* "Ljava/util/List<", */
/* "Ljava/lang/String;", */
/* ">;" */
/* } */
} // .end annotation
/* .line 1366 */
v0 = this.mCoreAppsPackageNameList;
} // .end method
public java.util.List getPolicyList ( ) {
/* .locals 1 */
/* .annotation system Ldalvik/annotation/Signature; */
/* value = { */
/* "()", */
/* "Ljava/util/List<", */
/* "Ljava/lang/String;", */
/* ">;" */
/* } */
} // .end annotation
/* .line 1107 */
com.miui.server.turbosched.TurboSchedConfig .getPolicyList ( );
} // .end method
public Boolean isApiEnable ( ) {
/* .locals 1 */
/* .line 1097 */
v0 = /* invoke-direct {p0}, Lcom/miui/server/turbosched/TurboSchedManagerService;->isTurboEnabled()Z */
} // .end method
public Boolean isCoreApp ( ) {
/* .locals 3 */
/* .line 1359 */
v0 = android.os.Binder .getCallingPid ( );
/* .line 1360 */
/* .local v0, "pid":I */
/* invoke-direct {p0, v0}, Lcom/miui/server/turbosched/TurboSchedManagerService;->getProcessNameByPid(I)Ljava/lang/String; */
/* .line 1361 */
/* .local v1, "processName":Ljava/lang/String; */
android.os.TurboSchedMonitor .getInstance ( );
v2 = (( android.os.TurboSchedMonitor ) v2 ).isCoreApp ( v1 ); // invoke-virtual {v2, v1}, Landroid/os/TurboSchedMonitor;->isCoreApp(Ljava/lang/String;)Z
} // .end method
public Boolean isFileAccess ( ) {
/* .locals 2 */
/* .line 1516 */
/* new-instance v0, Ljava/io/File; */
final String v1 = "/sys/module/metis/parameters/mi_viptask"; // const-string v1, "/sys/module/metis/parameters/mi_viptask"
/* invoke-direct {v0, v1}, Ljava/io/File;-><init>(Ljava/lang/String;)V */
/* .line 1517 */
/* .local v0, "file":Ljava/io/File; */
v1 = (( java.io.File ) v0 ).exists ( ); // invoke-virtual {v0}, Ljava/io/File;->exists()Z
} // .end method
public void notifyOnScroll ( Boolean p0 ) {
/* .locals 0 */
/* .param p1, "isOnScroll" # Z */
/* .line 1302 */
/* iput-boolean p1, p0, Lcom/miui/server/turbosched/TurboSchedManagerService;->mIsOnScroll:Z */
/* .line 1303 */
return;
} // .end method
public void onCoreAppFirstActivityStart ( java.lang.String p0 ) {
/* .locals 5 */
/* .param p1, "processName" # Ljava/lang/String; */
/* .line 2188 */
/* iget-boolean v0, p0, Lcom/miui/server/turbosched/TurboSchedManagerService;->mSupportGaea:Z */
final String v1 = "TurboSchedManagerService"; // const-string v1, "TurboSchedManagerService"
final String v2 = "1"; // const-string v2, "1"
if ( v0 != null) { // if-eqz v0, :cond_0
/* iget-boolean v0, p0, Lcom/miui/server/turbosched/TurboSchedManagerService;->mGaeaEnable:Z */
if ( v0 != null) { // if-eqz v0, :cond_0
v0 = v0 = this.mCoreAppsPackageNameList;
if ( v0 != null) { // if-eqz v0, :cond_0
/* .line 2189 */
this.enableGaeaAppProcessName = p1;
/* .line 2190 */
final String v0 = "GAEA"; // const-string v0, "GAEA"
final String v3 = "/sys/module/gaea/parameters/mi_gaea_enable"; // const-string v3, "/sys/module/gaea/parameters/mi_gaea_enable"
v0 = /* invoke-direct {p0, v0, v3, v2}, Lcom/miui/server/turbosched/TurboSchedManagerService;->writeTurboSchedNode(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Z */
/* .line 2191 */
/* .local v0, "isSuccess":Z */
android.os.TurboSchedMonitor .getInstance ( );
v3 = (( android.os.TurboSchedMonitor ) v3 ).isDebugMode ( ); // invoke-virtual {v3}, Landroid/os/TurboSchedMonitor;->isDebugMode()Z
if ( v3 != null) { // if-eqz v3, :cond_0
/* .line 2192 */
/* new-instance v3, Ljava/lang/StringBuilder; */
/* invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V */
/* const-string/jumbo v4, "write gaea path node 1: " */
(( java.lang.StringBuilder ) v3 ).append ( v4 ); // invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v3 ).append ( v0 ); // invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v3 ).toString ( ); // invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
android.util.Slog .w ( v1,v3 );
/* .line 2195 */
} // .end local v0 # "isSuccess":Z
} // :cond_0
/* iget-boolean v0, p0, Lcom/miui/server/turbosched/TurboSchedManagerService;->mLinkEnable:Z */
if ( v0 != null) { // if-eqz v0, :cond_1
if ( p1 != null) { // if-eqz p1, :cond_1
v0 = v0 = this.mAppsLinkVipList;
if ( v0 != null) { // if-eqz v0, :cond_1
/* .line 2196 */
this.enableLinkVipAppProcessName = p1;
/* .line 2197 */
final String v0 = "LINK"; // const-string v0, "LINK"
final String v3 = "/sys/module/metis/parameters/vip_link_enable"; // const-string v3, "/sys/module/metis/parameters/vip_link_enable"
v0 = /* invoke-direct {p0, v0, v3, v2}, Lcom/miui/server/turbosched/TurboSchedManagerService;->writeTurboSchedNode(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Z */
/* .line 2198 */
/* .restart local v0 # "isSuccess":Z */
android.os.TurboSchedMonitor .getInstance ( );
v2 = (( android.os.TurboSchedMonitor ) v2 ).isDebugMode ( ); // invoke-virtual {v2}, Landroid/os/TurboSchedMonitor;->isDebugMode()Z
if ( v2 != null) { // if-eqz v2, :cond_1
/* if-nez v0, :cond_1 */
/* .line 2199 */
/* new-instance v2, Ljava/lang/StringBuilder; */
/* invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V */
final String v3 = "LinkVip, write linkvip path node 1:"; // const-string v3, "LinkVip, write linkvip path node 1:"
(( java.lang.StringBuilder ) v2 ).append ( v3 ); // invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v2 ).append ( v0 ); // invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v2 ).toString ( ); // invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
android.util.Slog .w ( v1,v2 );
/* .line 2201 */
} // .end local v0 # "isSuccess":Z
} // :cond_1
return;
} // .end method
public void onCoreAppLastActivityStop ( java.lang.String p0 ) {
/* .locals 5 */
/* .param p1, "processName" # Ljava/lang/String; */
/* .line 2205 */
/* iget-boolean v0, p0, Lcom/miui/server/turbosched/TurboSchedManagerService;->mSupportGaea:Z */
final String v1 = "TurboSchedManagerService"; // const-string v1, "TurboSchedManagerService"
final String v2 = "0"; // const-string v2, "0"
if ( v0 != null) { // if-eqz v0, :cond_0
/* iget-boolean v0, p0, Lcom/miui/server/turbosched/TurboSchedManagerService;->mGaeaEnable:Z */
if ( v0 != null) { // if-eqz v0, :cond_0
v0 = v0 = this.mCoreAppsPackageNameList;
if ( v0 != null) { // if-eqz v0, :cond_0
/* .line 2206 */
v0 = this.enableGaeaAppProcessName;
if ( v0 != null) { // if-eqz v0, :cond_0
v0 = (( java.lang.String ) v0 ).equals ( p1 ); // invoke-virtual {v0, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z
if ( v0 != null) { // if-eqz v0, :cond_0
/* .line 2207 */
final String v0 = "GAEA"; // const-string v0, "GAEA"
final String v3 = "/sys/module/gaea/parameters/mi_gaea_enable"; // const-string v3, "/sys/module/gaea/parameters/mi_gaea_enable"
v0 = /* invoke-direct {p0, v0, v3, v2}, Lcom/miui/server/turbosched/TurboSchedManagerService;->writeTurboSchedNode(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Z */
/* .line 2208 */
/* .local v0, "isSuccess":Z */
android.os.TurboSchedMonitor .getInstance ( );
v3 = (( android.os.TurboSchedMonitor ) v3 ).isDebugMode ( ); // invoke-virtual {v3}, Landroid/os/TurboSchedMonitor;->isDebugMode()Z
if ( v3 != null) { // if-eqz v3, :cond_0
/* .line 2209 */
/* new-instance v3, Ljava/lang/StringBuilder; */
/* invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V */
/* const-string/jumbo v4, "write gaea path node 0: " */
(( java.lang.StringBuilder ) v3 ).append ( v4 ); // invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v3 ).append ( v0 ); // invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v3 ).toString ( ); // invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
android.util.Slog .w ( v1,v3 );
/* .line 2213 */
} // .end local v0 # "isSuccess":Z
} // :cond_0
/* iget-boolean v0, p0, Lcom/miui/server/turbosched/TurboSchedManagerService;->mLinkEnable:Z */
if ( v0 != null) { // if-eqz v0, :cond_1
if ( p1 != null) { // if-eqz p1, :cond_1
v0 = v0 = this.mAppsLinkVipList;
if ( v0 != null) { // if-eqz v0, :cond_1
/* .line 2214 */
v0 = this.enableLinkVipAppProcessName;
if ( v0 != null) { // if-eqz v0, :cond_1
v0 = (( java.lang.String ) v0 ).equals ( p1 ); // invoke-virtual {v0, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z
if ( v0 != null) { // if-eqz v0, :cond_1
/* .line 2215 */
final String v0 = "LINK"; // const-string v0, "LINK"
final String v3 = "/sys/module/metis/parameters/vip_link_enable"; // const-string v3, "/sys/module/metis/parameters/vip_link_enable"
v0 = /* invoke-direct {p0, v0, v3, v2}, Lcom/miui/server/turbosched/TurboSchedManagerService;->writeTurboSchedNode(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Z */
/* .line 2216 */
/* .restart local v0 # "isSuccess":Z */
android.os.TurboSchedMonitor .getInstance ( );
v2 = (( android.os.TurboSchedMonitor ) v2 ).isDebugMode ( ); // invoke-virtual {v2}, Landroid/os/TurboSchedMonitor;->isDebugMode()Z
if ( v2 != null) { // if-eqz v2, :cond_1
/* if-nez v0, :cond_1 */
/* .line 2217 */
/* new-instance v2, Ljava/lang/StringBuilder; */
/* invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V */
final String v3 = "LinkVip, write linkvip path node 0:"; // const-string v3, "LinkVip, write linkvip path node 0:"
(( java.lang.StringBuilder ) v2 ).append ( v3 ); // invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v2 ).append ( v0 ); // invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v2 ).toString ( ); // invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
android.util.Slog .w ( v1,v2 );
/* .line 2221 */
} // .end local v0 # "isSuccess":Z
} // :cond_1
return;
} // .end method
public void onFocusedWindowChangeLocked ( java.lang.String p0, Integer p1 ) {
/* .locals 1 */
/* .param p1, "focus" # Ljava/lang/String; */
/* .param p2, "type" # I */
/* .line 1288 */
com.miui.server.turbosched.TurboSchedSceneManager .getInstance ( );
(( com.miui.server.turbosched.TurboSchedSceneManager ) v0 ).onFocusedWindowChangeLocked ( p1, p2 ); // invoke-virtual {v0, p1, p2}, Lcom/miui/server/turbosched/TurboSchedSceneManager;->onFocusedWindowChangeLocked(Ljava/lang/String;I)V
/* .line 1289 */
return;
} // .end method
public void registerStateChangeCallback ( java.lang.String p0, miui.turbosched.ITurboSchedManager$ITurboSchedStateChangeCallback p1 ) {
/* .locals 1 */
/* .param p1, "pkgName" # Ljava/lang/String; */
/* .param p2, "cb" # Lmiui/turbosched/ITurboSchedManager$ITurboSchedStateChangeCallback; */
/* .line 1278 */
com.miui.server.turbosched.TurboSchedSceneManager .getInstance ( );
(( com.miui.server.turbosched.TurboSchedSceneManager ) v0 ).registerStateChangeCallback ( p1, p2 ); // invoke-virtual {v0, p1, p2}, Lcom/miui/server/turbosched/TurboSchedSceneManager;->registerStateChangeCallback(Ljava/lang/String;Lmiui/turbosched/ITurboSchedManager$ITurboSchedStateChangeCallback;)V
/* .line 1279 */
return;
} // .end method
public void setForegroundProcessName ( java.lang.String p0 ) {
/* .locals 0 */
/* .param p1, "processName" # Ljava/lang/String; */
/* .line 2226 */
this.mProcessName = p1;
/* .line 2227 */
return;
} // .end method
public void setSceneAction ( java.lang.String p0, java.lang.String p1, Integer p2 ) {
/* .locals 1 */
/* .param p1, "scene" # Ljava/lang/String; */
/* .param p2, "action" # Ljava/lang/String; */
/* .param p3, "timeout" # I */
/* .line 1428 */
v0 = (( java.lang.String ) p2 ).hashCode ( ); // invoke-virtual {p2}, Ljava/lang/String;->hashCode()I
/* packed-switch v0, :pswitch_data_0 */
} // :cond_0
/* :pswitch_0 */
final String v0 = "TS_ACTION_FLING"; // const-string v0, "TS_ACTION_FLING"
v0 = (( java.lang.String ) p2 ).equals ( v0 ); // invoke-virtual {p2, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z
if ( v0 != null) { // if-eqz v0, :cond_0
int v0 = 0; // const/4 v0, 0x0
} // :goto_0
int v0 = -1; // const/4 v0, -0x1
} // :goto_1
/* packed-switch v0, :pswitch_data_1 */
/* .line 1430 */
/* :pswitch_1 */
/* nop */
/* .line 1435 */
} // :goto_2
return;
/* nop */
/* :pswitch_data_0 */
/* .packed-switch 0x67d68fb3 */
/* :pswitch_0 */
} // .end packed-switch
/* :pswitch_data_1 */
/* .packed-switch 0x0 */
/* :pswitch_1 */
} // .end packed-switch
} // .end method
public Boolean setTurboSchedAction ( Integer[] p0, Long p1 ) {
/* .locals 3 */
/* .param p1, "tid" # [I */
/* .param p2, "time" # J */
/* .line 1113 */
v0 = android.os.Binder .getCallingPid ( );
/* .line 1114 */
/* .local v0, "pid":I */
v1 = android.os.Binder .getCallingUid ( );
/* .line 1115 */
/* .local v1, "uid":I */
v2 = /* invoke-direct {p0, v0, v1}, Lcom/miui/server/turbosched/TurboSchedManagerService;->checkCallerPermmsion(II)Z */
/* if-nez v2, :cond_0 */
int v2 = 0; // const/4 v2, 0x0
/* .line 1118 */
} // :cond_0
/* iget-boolean v2, p0, Lcom/miui/server/turbosched/TurboSchedManagerService;->mTurboNodeExist:Z */
if ( v2 != null) { // if-eqz v2, :cond_1
/* .line 1119 */
final String v2 = "/sys/module/metis/parameters/mi_viptask"; // const-string v2, "/sys/module/metis/parameters/mi_viptask"
v2 = /* invoke-direct {p0, p1, p2, p3, v2}, Lcom/miui/server/turbosched/TurboSchedManagerService;->setTurboSchedActionInternal([IJLjava/lang/String;)Z */
/* .local v2, "ret":Z */
/* .line 1121 */
} // .end local v2 # "ret":Z
} // :cond_1
final String v2 = "/sys/module/migt/parameters/mi_viptask"; // const-string v2, "/sys/module/migt/parameters/mi_viptask"
v2 = /* invoke-direct {p0, p1, p2, p3, v2}, Lcom/miui/server/turbosched/TurboSchedManagerService;->setTurboSchedActionInternal([IJLjava/lang/String;)Z */
/* .line 1123 */
/* .restart local v2 # "ret":Z */
} // :goto_0
} // .end method
public void setTurboSchedActionToLittleCore ( Integer[] p0, Long p1 ) {
/* .locals 11 */
/* .param p1, "tids" # [I */
/* .param p2, "time" # J */
/* .line 1252 */
v0 = /* invoke-direct {p0}, Lcom/miui/server/turbosched/TurboSchedManagerService;->isTurboEnabled()Z */
final String v1 = ",time: "; // const-string v1, ",time: "
if ( v0 != null) { // if-eqz v0, :cond_4
com.miui.server.turbosched.TurboSchedConfig .getPolicyList ( );
v0 = final String v2 = "lc"; // const-string v2, "lc"
/* if-nez v0, :cond_0 */
/* goto/16 :goto_2 */
/* .line 1256 */
} // :cond_0
/* invoke-direct {p0, v2}, Lcom/miui/server/turbosched/TurboSchedManagerService;->sendTraceBeginMsg(Ljava/lang/String;)V */
/* .line 1257 */
int v0 = 0; // const/4 v0, 0x0
/* .line 1259 */
/* .local v0, "success":Z */
final String v3 = "LC"; // const-string v3, "LC"
final String v7 = "lc"; // const-string v7, "lc"
/* move-object v2, p0 */
/* move-object v4, p1 */
/* move-wide v5, p2 */
/* invoke-direct/range {v2 ..v7}, Lcom/miui/server/turbosched/TurboSchedManagerService;->setTurboSchedActionParam(Ljava/lang/String;[IJLjava/lang/String;)Ljava/util/List; */
/* .line 1260 */
/* .local v2, "tidsList":Ljava/util/List;, "Ljava/util/List<Ljava/lang/Integer;>;" */
v3 = if ( v2 != null) { // if-eqz v2, :cond_3
if ( v3 != null) { // if-eqz v3, :cond_1
/* .line 1264 */
} // :cond_1
/* new-instance v4, Lcom/android/server/audio/AudioServiceStubImpl$$ExternalSyntheticLambda1; */
/* invoke-direct {v4}, Lcom/android/server/audio/AudioServiceStubImpl$$ExternalSyntheticLambda1;-><init>()V */
/* .line 1265 */
/* .local v3, "newTids":[I */
final String v4 = "/sys/module/metis/parameters/add_mi_viptask_sched_lit_core"; // const-string v4, "/sys/module/metis/parameters/add_mi_viptask_sched_lit_core"
v0 = /* invoke-direct {p0, v4, v3}, Lcom/miui/server/turbosched/TurboSchedManagerService;->writeTidsToPath(Ljava/lang/String;[I)Z */
/* .line 1267 */
if ( v0 != null) { // if-eqz v0, :cond_2
/* .line 1268 */
final String v5 = "-LC"; // const-string v5, "-LC"
final String v9 = "/sys/module/metis/parameters/del_mi_viptask_sched_lit_core"; // const-string v9, "/sys/module/metis/parameters/del_mi_viptask_sched_lit_core"
v10 = this.mPriorityTidStartTimeMap;
/* move-object v4, p0 */
/* move-object v6, v3 */
/* move-wide v7, p2 */
/* invoke-direct/range {v4 ..v10}, Lcom/miui/server/turbosched/TurboSchedManagerService;->setPolicyTimeoutChecker(Ljava/lang/String;[IJLjava/lang/String;Ljava/util/Map;)V */
/* .line 1269 */
v4 = this.mHistoryLog;
/* new-instance v5, Ljava/lang/StringBuilder; */
/* invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V */
final String v6 = "TS [LC] : success, tids: "; // const-string v6, "TS [LC] : success, tids: "
(( java.lang.StringBuilder ) v5 ).append ( v6 ); // invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
java.util.Arrays .toString ( v3 );
(( java.lang.StringBuilder ) v5 ).append ( v6 ); // invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v5 ).append ( v1 ); // invoke-virtual {v5, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v1 ).append ( p2, p3 ); // invoke-virtual {v1, p2, p3}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v1 ).toString ( ); // invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
(( android.util.LocalLog ) v4 ).log ( v1 ); // invoke-virtual {v4, v1}, Landroid/util/LocalLog;->log(Ljava/lang/String;)V
/* .line 1271 */
} // :cond_2
v4 = this.mHistoryLog;
/* new-instance v5, Ljava/lang/StringBuilder; */
/* invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V */
final String v6 = "TS [LC] : failed, R: write failed, newTids: "; // const-string v6, "TS [LC] : failed, R: write failed, newTids: "
(( java.lang.StringBuilder ) v5 ).append ( v6 ); // invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
java.util.Arrays .toString ( v3 );
(( java.lang.StringBuilder ) v5 ).append ( v6 ); // invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v5 ).append ( v1 ); // invoke-virtual {v5, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v1 ).append ( p2, p3 ); // invoke-virtual {v1, p2, p3}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v1 ).toString ( ); // invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
(( android.util.LocalLog ) v4 ).log ( v1 ); // invoke-virtual {v4, v1}, Landroid/util/LocalLog;->log(Ljava/lang/String;)V
/* .line 1273 */
} // :goto_0
/* invoke-direct {p0}, Lcom/miui/server/turbosched/TurboSchedManagerService;->sendTraceEndMsg()V */
/* .line 1274 */
return;
/* .line 1261 */
} // .end local v3 # "newTids":[I
} // :cond_3
} // :goto_1
return;
/* .line 1253 */
} // .end local v0 # "success":Z
} // .end local v2 # "tidsList":Ljava/util/List;, "Ljava/util/List<Ljava/lang/Integer;>;"
} // :cond_4
} // :goto_2
v0 = this.mHistoryLog;
/* new-instance v2, Ljava/lang/StringBuilder; */
/* invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V */
final String v3 = "TS [LC] : failed, R: not enable, tids: "; // const-string v3, "TS [LC] : failed, R: not enable, tids: "
(( java.lang.StringBuilder ) v2 ).append ( v3 ); // invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
java.util.Arrays .toString ( p1 );
(( java.lang.StringBuilder ) v2 ).append ( v3 ); // invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v2 ).append ( v1 ); // invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v1 ).append ( p2, p3 ); // invoke-virtual {v1, p2, p3}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v1 ).toString ( ); // invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
(( android.util.LocalLog ) v0 ).log ( v1 ); // invoke-virtual {v0, v1}, Landroid/util/LocalLog;->log(Ljava/lang/String;)V
/* .line 1254 */
return;
} // .end method
public void setTurboSchedActionWithBoostFrequency ( Integer[] p0, Long p1 ) {
/* .locals 11 */
/* .param p1, "tids" # [I */
/* .param p2, "time" # J */
/* .line 1135 */
v0 = /* invoke-direct {p0}, Lcom/miui/server/turbosched/TurboSchedManagerService;->isTurboEnabled()Z */
final String v1 = ",time: "; // const-string v1, ",time: "
if ( v0 != null) { // if-eqz v0, :cond_6
com.miui.server.turbosched.TurboSchedConfig .getPolicyList ( );
v0 = final String v2 = "bwf"; // const-string v2, "bwf"
/* if-nez v0, :cond_0 */
/* goto/16 :goto_2 */
/* .line 1140 */
} // :cond_0
/* array-length v0, p1 */
int v3 = 1; // const/4 v3, 0x1
/* if-le v0, v3, :cond_1 */
/* .line 1141 */
v0 = this.mHistoryLog;
/* new-instance v2, Ljava/lang/StringBuilder; */
/* invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V */
final String v3 = "TS [BWF] : failed, R: tids length must be 1, tids: "; // const-string v3, "TS [BWF] : failed, R: tids length must be 1, tids: "
(( java.lang.StringBuilder ) v2 ).append ( v3 ); // invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
java.util.Arrays .toString ( p1 );
(( java.lang.StringBuilder ) v2 ).append ( v3 ); // invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v2 ).append ( v1 ); // invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v1 ).append ( p2, p3 ); // invoke-virtual {v1, p2, p3}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v1 ).toString ( ); // invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
(( android.util.LocalLog ) v0 ).log ( v1 ); // invoke-virtual {v0, v1}, Landroid/util/LocalLog;->log(Ljava/lang/String;)V
/* .line 1142 */
return;
/* .line 1144 */
} // :cond_1
/* invoke-direct {p0, v2}, Lcom/miui/server/turbosched/TurboSchedManagerService;->sendTraceBeginMsg(Ljava/lang/String;)V */
/* .line 1145 */
int v0 = 0; // const/4 v0, 0x0
/* .line 1146 */
/* .local v0, "success":Z */
final String v3 = "BWF"; // const-string v3, "BWF"
final String v7 = "bwf"; // const-string v7, "bwf"
/* move-object v2, p0 */
/* move-object v4, p1 */
/* move-wide v5, p2 */
/* invoke-direct/range {v2 ..v7}, Lcom/miui/server/turbosched/TurboSchedManagerService;->setTurboSchedActionParam(Ljava/lang/String;[IJLjava/lang/String;)Ljava/util/List; */
/* .line 1147 */
/* .local v2, "tidsList":Ljava/util/List;, "Ljava/util/List<Ljava/lang/Integer;>;" */
v3 = if ( v2 != null) { // if-eqz v2, :cond_5
if ( v3 != null) { // if-eqz v3, :cond_2
/* goto/16 :goto_1 */
/* .line 1150 */
} // :cond_2
/* new-instance v4, Lcom/android/server/audio/AudioServiceStubImpl$$ExternalSyntheticLambda1; */
/* invoke-direct {v4}, Lcom/android/server/audio/AudioServiceStubImpl$$ExternalSyntheticLambda1;-><init>()V */
/* .line 1151 */
/* .local v3, "newTids":[I */
v0 = (( com.miui.server.turbosched.TurboSchedManagerService ) p0 ).setTurboSchedAction ( v3, p2, p3 ); // invoke-virtual {p0, v3, p2, p3}, Lcom/miui/server/turbosched/TurboSchedManagerService;->setTurboSchedAction([IJ)Z
/* .line 1152 */
if ( v0 != null) { // if-eqz v0, :cond_4
/* .line 1153 */
final String v4 = "/sys/module/metis/parameters/add_mi_viptask_enqueue_boost"; // const-string v4, "/sys/module/metis/parameters/add_mi_viptask_enqueue_boost"
v0 = /* invoke-direct {p0, v4, v3}, Lcom/miui/server/turbosched/TurboSchedManagerService;->writeTidsToPath(Ljava/lang/String;[I)Z */
/* .line 1154 */
if ( v0 != null) { // if-eqz v0, :cond_3
/* .line 1155 */
final String v5 = "-BWF"; // const-string v5, "-BWF"
final String v9 = "/sys/module/metis/parameters/del_mi_viptask_enqueue_boost"; // const-string v9, "/sys/module/metis/parameters/del_mi_viptask_enqueue_boost"
v10 = this.mBWFTidStartTimeMap;
/* move-object v4, p0 */
/* move-object v6, v3 */
/* move-wide v7, p2 */
/* invoke-direct/range {v4 ..v10}, Lcom/miui/server/turbosched/TurboSchedManagerService;->setPolicyTimeoutChecker(Ljava/lang/String;[IJLjava/lang/String;Ljava/util/Map;)V */
/* .line 1156 */
v4 = this.mHistoryLog;
/* new-instance v5, Ljava/lang/StringBuilder; */
/* invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V */
final String v6 = "TS [BWF] : success, tids: "; // const-string v6, "TS [BWF] : success, tids: "
(( java.lang.StringBuilder ) v5 ).append ( v6 ); // invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
java.util.Arrays .toString ( v3 );
(( java.lang.StringBuilder ) v5 ).append ( v6 ); // invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v5 ).append ( v1 ); // invoke-virtual {v5, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v1 ).append ( p2, p3 ); // invoke-virtual {v1, p2, p3}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v1 ).toString ( ); // invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
(( android.util.LocalLog ) v4 ).log ( v1 ); // invoke-virtual {v4, v1}, Landroid/util/LocalLog;->log(Ljava/lang/String;)V
/* .line 1158 */
} // :cond_3
v4 = this.mHistoryLog;
/* new-instance v5, Ljava/lang/StringBuilder; */
/* invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V */
final String v6 = "TS [BWF] : failed, R: write failed 1, tids: "; // const-string v6, "TS [BWF] : failed, R: write failed 1, tids: "
(( java.lang.StringBuilder ) v5 ).append ( v6 ); // invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
java.util.Arrays .toString ( v3 );
(( java.lang.StringBuilder ) v5 ).append ( v6 ); // invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v5 ).append ( v1 ); // invoke-virtual {v5, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v1 ).append ( p2, p3 ); // invoke-virtual {v1, p2, p3}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v1 ).toString ( ); // invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
(( android.util.LocalLog ) v4 ).log ( v1 ); // invoke-virtual {v4, v1}, Landroid/util/LocalLog;->log(Ljava/lang/String;)V
/* .line 1161 */
} // :cond_4
v4 = this.mHistoryLog;
/* new-instance v5, Ljava/lang/StringBuilder; */
/* invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V */
final String v6 = "TS [BWF] : failed, R: write failed 2, tids: "; // const-string v6, "TS [BWF] : failed, R: write failed 2, tids: "
(( java.lang.StringBuilder ) v5 ).append ( v6 ); // invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
java.util.Arrays .toString ( v3 );
(( java.lang.StringBuilder ) v5 ).append ( v6 ); // invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v5 ).append ( v1 ); // invoke-virtual {v5, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v1 ).append ( p2, p3 ); // invoke-virtual {v1, p2, p3}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v1 ).toString ( ); // invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
(( android.util.LocalLog ) v4 ).log ( v1 ); // invoke-virtual {v4, v1}, Landroid/util/LocalLog;->log(Ljava/lang/String;)V
/* .line 1163 */
} // :goto_0
/* invoke-direct {p0}, Lcom/miui/server/turbosched/TurboSchedManagerService;->sendTraceEndMsg()V */
/* .line 1164 */
return;
/* .line 1148 */
} // .end local v3 # "newTids":[I
} // :cond_5
} // :goto_1
return;
/* .line 1136 */
} // .end local v0 # "success":Z
} // .end local v2 # "tidsList":Ljava/util/List;, "Ljava/util/List<Ljava/lang/Integer;>;"
} // :cond_6
} // :goto_2
v0 = this.mHistoryLog;
/* new-instance v2, Ljava/lang/StringBuilder; */
/* invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V */
final String v3 = "TS [BWF] : failed, R: not enable, tids: "; // const-string v3, "TS [BWF] : failed, R: not enable, tids: "
(( java.lang.StringBuilder ) v2 ).append ( v3 ); // invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
java.util.Arrays .toString ( p1 );
(( java.lang.StringBuilder ) v2 ).append ( v3 ); // invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v2 ).append ( v1 ); // invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v1 ).append ( p2, p3 ); // invoke-virtual {v1, p2, p3}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v1 ).toString ( ); // invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
(( android.util.LocalLog ) v0 ).log ( v1 ); // invoke-virtual {v0, v1}, Landroid/util/LocalLog;->log(Ljava/lang/String;)V
/* .line 1137 */
return;
} // .end method
public void setTurboSchedActionWithId ( Integer[] p0, Long p1, Long p2, Integer p3 ) {
/* .locals 5 */
/* .param p1, "tids" # [I */
/* .param p2, "time" # J */
/* .param p4, "id" # J */
/* .param p6, "mode" # I */
/* .line 1307 */
v0 = /* invoke-direct {p0}, Lcom/miui/server/turbosched/TurboSchedManagerService;->isTurboEnabled()Z */
if ( v0 != null) { // if-eqz v0, :cond_3
v0 = /* invoke-direct {p0, p1}, Lcom/miui/server/turbosched/TurboSchedManagerService;->isFgDrawingFrame([I)Z */
if ( v0 != null) { // if-eqz v0, :cond_3
/* iget-boolean v0, p0, Lcom/miui/server/turbosched/TurboSchedManagerService;->mIsOnScroll:Z */
if ( v0 != null) { // if-eqz v0, :cond_3
android.os.TurboSchedMonitor .getInstance ( );
v1 = this.mProcessName;
v0 = (( android.os.TurboSchedMonitor ) v0 ).isCoreApp ( v1 ); // invoke-virtual {v0, v1}, Landroid/os/TurboSchedMonitor;->isCoreApp(Ljava/lang/String;)Z
if ( v0 != null) { // if-eqz v0, :cond_3
/* .line 1308 */
/* iget-wide v0, p0, Lcom/miui/server/turbosched/TurboSchedManagerService;->mLastVsyncId:J */
/* cmp-long v0, p4, v0 */
if ( v0 != null) { // if-eqz v0, :cond_3
/* .line 1309 */
v0 = /* invoke-direct {p0}, Lcom/miui/server/turbosched/TurboSchedManagerService;->getBufferTxCount()I */
/* .line 1310 */
/* .local v0, "bufferTx":I */
final String v1 = "TurboSchedManagerService"; // const-string v1, "TurboSchedManagerService"
int v2 = 1; // const/4 v2, 0x1
/* if-lt v0, v2, :cond_1 */
/* .line 1311 */
android.os.TurboSchedMonitor .getInstance ( );
v2 = (( android.os.TurboSchedMonitor ) v2 ).isDebugMode ( ); // invoke-virtual {v2}, Landroid/os/TurboSchedMonitor;->isDebugMode()Z
if ( v2 != null) { // if-eqz v2, :cond_0
/* .line 1312 */
final String v2 = "bufferTx larger than 2, dont\'t need boost, FluencyOptimizer"; // const-string v2, "bufferTx larger than 2, dont\'t need boost, FluencyOptimizer"
android.util.Slog .d ( v1,v2 );
/* .line 1313 */
} // :cond_0
return;
/* .line 1316 */
} // :cond_1
android.os.TurboSchedMonitor .getInstance ( );
v3 = (( android.os.TurboSchedMonitor ) v3 ).isDebugMode ( ); // invoke-virtual {v3}, Landroid/os/TurboSchedMonitor;->isDebugMode()Z
if ( v3 != null) { // if-eqz v3, :cond_2
/* .line 1317 */
/* new-instance v3, Ljava/lang/StringBuilder; */
/* invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V */
/* const-string/jumbo v4, "setTurboSchedActionWithId, id:" */
(( java.lang.StringBuilder ) v3 ).append ( v4 ); // invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v3 ).append ( p4, p5 ); // invoke-virtual {v3, p4, p5}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;
final String v4 = ", bufferTx:"; // const-string v4, ", bufferTx:"
(( java.lang.StringBuilder ) v3 ).append ( v4 ); // invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v3 ).append ( v0 ); // invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;
final String v4 = ", FluencyOptimizer"; // const-string v4, ", FluencyOptimizer"
(( java.lang.StringBuilder ) v3 ).append ( v4 ); // invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v3 ).toString ( ); // invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
android.util.Slog .d ( v1,v3 );
/* .line 1319 */
} // :cond_2
/* iput-wide p4, p0, Lcom/miui/server/turbosched/TurboSchedManagerService;->mLastVsyncId:J */
/* .line 1321 */
(( com.miui.server.turbosched.TurboSchedManagerService ) p0 ).breakThermlimit ( v2, p2, p3 ); // invoke-virtual {p0, v2, p2, p3}, Lcom/miui/server/turbosched/TurboSchedManagerService;->breakThermlimit(IJ)V
/* .line 1322 */
v1 = this.mBoostDuration;
int v2 = 0; // const/4 v2, 0x0
/* check-cast v1, Ljava/lang/Long; */
v1 = (( java.lang.Long ) v1 ).intValue ( ); // invoke-virtual {v1}, Ljava/lang/Long;->intValue()I
/* invoke-direct {p0, v1}, Lcom/miui/server/turbosched/TurboSchedManagerService;->metisFrameBoost(I)V */
/* .line 1325 */
} // .end local v0 # "bufferTx":I
} // :cond_3
return;
} // .end method
public void setTurboSchedActionWithPriority ( Integer[] p0, Long p1 ) {
/* .locals 11 */
/* .param p1, "tids" # [I */
/* .param p2, "time" # J */
/* .line 1225 */
v0 = /* invoke-direct {p0}, Lcom/miui/server/turbosched/TurboSchedManagerService;->isTurboEnabled()Z */
final String v1 = ",time: "; // const-string v1, ",time: "
if ( v0 != null) { // if-eqz v0, :cond_4
com.miui.server.turbosched.TurboSchedConfig .getPolicyList ( );
v0 = final String v2 = "priority"; // const-string v2, "priority"
/* if-nez v0, :cond_0 */
/* goto/16 :goto_2 */
/* .line 1229 */
} // :cond_0
/* invoke-direct {p0, v2}, Lcom/miui/server/turbosched/TurboSchedManagerService;->sendTraceBeginMsg(Ljava/lang/String;)V */
/* .line 1230 */
int v0 = 0; // const/4 v0, 0x0
/* .line 1232 */
/* .local v0, "success":Z */
final String v3 = "PRIORITY"; // const-string v3, "PRIORITY"
final String v7 = "priority"; // const-string v7, "priority"
/* move-object v2, p0 */
/* move-object v4, p1 */
/* move-wide v5, p2 */
/* invoke-direct/range {v2 ..v7}, Lcom/miui/server/turbosched/TurboSchedManagerService;->setTurboSchedActionParam(Ljava/lang/String;[IJLjava/lang/String;)Ljava/util/List; */
/* .line 1233 */
/* .local v2, "tidsList":Ljava/util/List;, "Ljava/util/List<Ljava/lang/Integer;>;" */
v3 = if ( v2 != null) { // if-eqz v2, :cond_3
if ( v3 != null) { // if-eqz v3, :cond_1
/* .line 1237 */
} // :cond_1
/* new-instance v4, Lcom/android/server/audio/AudioServiceStubImpl$$ExternalSyntheticLambda1; */
/* invoke-direct {v4}, Lcom/android/server/audio/AudioServiceStubImpl$$ExternalSyntheticLambda1;-><init>()V */
/* .line 1238 */
/* .local v3, "newTids":[I */
final String v4 = "/sys/module/metis/parameters/add_mi_viptask_sched_priority"; // const-string v4, "/sys/module/metis/parameters/add_mi_viptask_sched_priority"
v0 = /* invoke-direct {p0, v4, v3}, Lcom/miui/server/turbosched/TurboSchedManagerService;->writeTidsToPath(Ljava/lang/String;[I)Z */
/* .line 1240 */
if ( v0 != null) { // if-eqz v0, :cond_2
/* .line 1241 */
final String v5 = "-PRIORITY"; // const-string v5, "-PRIORITY"
final String v9 = "/sys/module/metis/parameters/del_mi_viptask_sched_priority"; // const-string v9, "/sys/module/metis/parameters/del_mi_viptask_sched_priority"
v10 = this.mPriorityTidStartTimeMap;
/* move-object v4, p0 */
/* move-object v6, v3 */
/* move-wide v7, p2 */
/* invoke-direct/range {v4 ..v10}, Lcom/miui/server/turbosched/TurboSchedManagerService;->setPolicyTimeoutChecker(Ljava/lang/String;[IJLjava/lang/String;Ljava/util/Map;)V */
/* .line 1242 */
v4 = this.mHistoryLog;
/* new-instance v5, Ljava/lang/StringBuilder; */
/* invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V */
final String v6 = "TS [PRIORITY] : success, tids: "; // const-string v6, "TS [PRIORITY] : success, tids: "
(( java.lang.StringBuilder ) v5 ).append ( v6 ); // invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
java.util.Arrays .toString ( v3 );
(( java.lang.StringBuilder ) v5 ).append ( v6 ); // invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v5 ).append ( v1 ); // invoke-virtual {v5, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v1 ).append ( p2, p3 ); // invoke-virtual {v1, p2, p3}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v1 ).toString ( ); // invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
(( android.util.LocalLog ) v4 ).log ( v1 ); // invoke-virtual {v4, v1}, Landroid/util/LocalLog;->log(Ljava/lang/String;)V
/* .line 1244 */
} // :cond_2
v4 = this.mHistoryLog;
/* new-instance v5, Ljava/lang/StringBuilder; */
/* invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V */
final String v6 = "TS [PRIORITY] : failed, R: write failed, tids: "; // const-string v6, "TS [PRIORITY] : failed, R: write failed, tids: "
(( java.lang.StringBuilder ) v5 ).append ( v6 ); // invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
java.util.Arrays .toString ( v3 );
(( java.lang.StringBuilder ) v5 ).append ( v6 ); // invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v5 ).append ( v1 ); // invoke-virtual {v5, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v1 ).append ( p2, p3 ); // invoke-virtual {v1, p2, p3}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v1 ).toString ( ); // invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
(( android.util.LocalLog ) v4 ).log ( v1 ); // invoke-virtual {v4, v1}, Landroid/util/LocalLog;->log(Ljava/lang/String;)V
/* .line 1246 */
} // :goto_0
/* invoke-direct {p0}, Lcom/miui/server/turbosched/TurboSchedManagerService;->sendTraceEndMsg()V */
/* .line 1247 */
return;
/* .line 1234 */
} // .end local v3 # "newTids":[I
} // :cond_3
} // :goto_1
return;
/* .line 1226 */
} // .end local v0 # "success":Z
} // .end local v2 # "tidsList":Ljava/util/List;, "Ljava/util/List<Ljava/lang/Integer;>;"
} // :cond_4
} // :goto_2
v0 = this.mHistoryLog;
/* new-instance v2, Ljava/lang/StringBuilder; */
/* invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V */
final String v3 = "TS [PRIORITY] : failed, R: not enable, tids: "; // const-string v3, "TS [PRIORITY] : failed, R: not enable, tids: "
(( java.lang.StringBuilder ) v2 ).append ( v3 ); // invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
java.util.Arrays .toString ( p1 );
(( java.lang.StringBuilder ) v2 ).append ( v3 ); // invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v2 ).append ( v1 ); // invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v1 ).append ( p2, p3 ); // invoke-virtual {v1, p2, p3}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v1 ).toString ( ); // invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
(( android.util.LocalLog ) v0 ).log ( v1 ); // invoke-virtual {v0, v1}, Landroid/util/LocalLog;->log(Ljava/lang/String;)V
/* .line 1227 */
return;
} // .end method
public void setTurboSchedActionWithoutBlock ( Integer[] p0, Long p1 ) {
/* .locals 0 */
/* .param p1, "tids" # [I */
/* .param p2, "time" # J */
/* .line 1129 */
(( com.miui.server.turbosched.TurboSchedManagerService ) p0 ).setTurboSchedAction ( p1, p2, p3 ); // invoke-virtual {p0, p1, p2, p3}, Lcom/miui/server/turbosched/TurboSchedManagerService;->setTurboSchedAction([IJ)Z
/* .line 1130 */
return;
} // .end method
public void triggerBoostAction ( Integer p0 ) {
/* .locals 0 */
/* .param p1, "boostMs" # I */
/* .line 1377 */
/* invoke-direct {p0, p1}, Lcom/miui/server/turbosched/TurboSchedManagerService;->metisFrameBoost(I)V */
/* .line 1378 */
return;
} // .end method
public void triggerBoostTask ( Integer p0, Integer p1 ) {
/* .locals 2 */
/* .param p1, "tid" # I */
/* .param p2, "boostSec" # I */
/* .line 1382 */
android.os.TurboSchedMonitor .getInstance ( );
v0 = (( android.os.TurboSchedMonitor ) v0 ).isCoreAppOptimizerEnabled ( ); // invoke-virtual {v0}, Landroid/os/TurboSchedMonitor;->isCoreAppOptimizerEnabled()Z
if ( v0 != null) { // if-eqz v0, :cond_1
final String v0 = "/dev/metis"; // const-string v0, "/dev/metis"
v0 = com.miui.server.turbosched.TurboSchedUtil .checkFileAccess ( v0 );
if ( v0 != null) { // if-eqz v0, :cond_1
/* .line 1383 */
v0 = com.miui.server.turbosched.TurboSchedManagerService.mTurboLock;
/* monitor-enter v0 */
/* .line 1384 */
try { // :try_start_0
final String v1 = "/dev/metis"; // const-string v1, "/dev/metis"
v1 = android.os.NativeTurboSchedManager .nativeOpenDevice ( v1 );
/* .line 1385 */
/* .local v1, "handle":I */
/* if-ltz v1, :cond_0 */
/* .line 1386 */
android.os.NativeTurboSchedManager .nativeTaskBoost ( v1,p1,p2 );
/* .line 1387 */
android.os.NativeTurboSchedManager .nativeCloseDevice ( v1 );
/* .line 1389 */
} // .end local v1 # "handle":I
} // :cond_0
/* monitor-exit v0 */
/* :catchall_0 */
/* move-exception v1 */
/* monitor-exit v0 */
/* :try_end_0 */
/* .catchall {:try_start_0 ..:try_end_0} :catchall_0 */
/* throw v1 */
/* .line 1391 */
} // :cond_1
} // :goto_0
return;
} // .end method
public void unregisterStateChangeCallback ( java.lang.String p0, miui.turbosched.ITurboSchedManager$ITurboSchedStateChangeCallback p1 ) {
/* .locals 1 */
/* .param p1, "pkgName" # Ljava/lang/String; */
/* .param p2, "cb" # Lmiui/turbosched/ITurboSchedManager$ITurboSchedStateChangeCallback; */
/* .line 1283 */
com.miui.server.turbosched.TurboSchedSceneManager .getInstance ( );
(( com.miui.server.turbosched.TurboSchedSceneManager ) v0 ).unregisterStateChangeCallback ( p1, p2 ); // invoke-virtual {v0, p1, p2}, Lcom/miui/server/turbosched/TurboSchedSceneManager;->unregisterStateChangeCallback(Ljava/lang/String;Lmiui/turbosched/ITurboSchedManager$ITurboSchedStateChangeCallback;)V
/* .line 1284 */
return;
} // .end method
