.class Lcom/miui/server/turbosched/TurboSchedSceneManager$5;
.super Lmiui/app/IFreeformCallback$Stub;
.source "TurboSchedSceneManager.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/miui/server/turbosched/TurboSchedSceneManager;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/miui/server/turbosched/TurboSchedSceneManager;


# direct methods
.method constructor <init>(Lcom/miui/server/turbosched/TurboSchedSceneManager;)V
    .locals 0
    .param p1, "this$0"    # Lcom/miui/server/turbosched/TurboSchedSceneManager;

    .line 340
    iput-object p1, p0, Lcom/miui/server/turbosched/TurboSchedSceneManager$5;->this$0:Lcom/miui/server/turbosched/TurboSchedSceneManager;

    invoke-direct {p0}, Lmiui/app/IFreeformCallback$Stub;-><init>()V

    return-void
.end method


# virtual methods
.method public dispatchFreeFormStackModeChanged(ILmiui/app/MiuiFreeFormManager$MiuiFreeFormStackInfo;)V
    .locals 3
    .param p1, "action"    # I
    .param p2, "stackInfo"    # Lmiui/app/MiuiFreeFormManager$MiuiFreeFormStackInfo;

    .line 342
    new-instance v0, Landroid/os/Bundle;

    invoke-direct {v0}, Landroid/os/Bundle;-><init>()V

    .line 343
    .local v0, "bundle":Landroid/os/Bundle;
    const-string v1, "action"

    invoke-virtual {v0, v1, p1}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    .line 344
    const-string v1, "pkgName"

    iget-object v2, p2, Lmiui/app/MiuiFreeFormManager$MiuiFreeFormStackInfo;->packageName:Ljava/lang/String;

    invoke-virtual {v0, v1, v2}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 345
    const-string/jumbo v1, "windowState"

    iget v2, p2, Lmiui/app/MiuiFreeFormManager$MiuiFreeFormStackInfo;->windowState:I

    invoke-virtual {v0, v1, v2}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    .line 346
    iget-object v1, p0, Lcom/miui/server/turbosched/TurboSchedSceneManager$5;->this$0:Lcom/miui/server/turbosched/TurboSchedSceneManager;

    invoke-static {v1}, Lcom/miui/server/turbosched/TurboSchedSceneManager;->-$$Nest$fgetmTbSceneHandler(Lcom/miui/server/turbosched/TurboSchedSceneManager;)Lcom/miui/server/turbosched/TurboSchedSceneManager$TurboSchedSceneHandler;

    move-result-object v1

    invoke-virtual {v1}, Lcom/miui/server/turbosched/TurboSchedSceneManager$TurboSchedSceneHandler;->obtainMessage()Landroid/os/Message;

    move-result-object v1

    .line 347
    .local v1, "msg":Landroid/os/Message;
    invoke-virtual {v1, v0}, Landroid/os/Message;->setData(Landroid/os/Bundle;)V

    .line 348
    const/16 v2, 0xb

    iput v2, v1, Landroid/os/Message;->what:I

    .line 349
    iget-object v2, p0, Lcom/miui/server/turbosched/TurboSchedSceneManager$5;->this$0:Lcom/miui/server/turbosched/TurboSchedSceneManager;

    invoke-static {v2}, Lcom/miui/server/turbosched/TurboSchedSceneManager;->-$$Nest$fgetmTbSceneHandler(Lcom/miui/server/turbosched/TurboSchedSceneManager;)Lcom/miui/server/turbosched/TurboSchedSceneManager$TurboSchedSceneHandler;

    move-result-object v2

    invoke-virtual {v2, v1}, Lcom/miui/server/turbosched/TurboSchedSceneManager$TurboSchedSceneHandler;->sendMessage(Landroid/os/Message;)Z

    .line 350
    return-void
.end method
