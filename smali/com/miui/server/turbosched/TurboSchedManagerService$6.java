class com.miui.server.turbosched.TurboSchedManagerService$6 extends android.database.ContentObserver {
	 /* .source "TurboSchedManagerService.java" */
	 /* # annotations */
	 /* .annotation system Ldalvik/annotation/EnclosingClass; */
	 /* value = Lcom/miui/server/turbosched/TurboSchedManagerService; */
} // .end annotation
/* .annotation system Ldalvik/annotation/InnerClass; */
/* accessFlags = 0x0 */
/* name = null */
} // .end annotation
/* # instance fields */
final com.miui.server.turbosched.TurboSchedManagerService this$0; //synthetic
/* # direct methods */
 com.miui.server.turbosched.TurboSchedManagerService$6 ( ) {
/* .locals 0 */
/* .param p1, "this$0" # Lcom/miui/server/turbosched/TurboSchedManagerService; */
/* .param p2, "handler" # Landroid/os/Handler; */
/* .line 724 */
this.this$0 = p1;
/* invoke-direct {p0, p2}, Landroid/database/ContentObserver;-><init>(Landroid/os/Handler;)V */
return;
} // .end method
/* # virtual methods */
public void onChange ( Boolean p0, android.net.Uri p1 ) {
/* .locals 2 */
/* .param p1, "selfChange" # Z */
/* .param p2, "uri" # Landroid/net/Uri; */
/* .line 727 */
final String v0 = "TurboSchedManagerService"; // const-string v0, "TurboSchedManagerService"
if ( p2 != null) { // if-eqz p2, :cond_0
	 final String v1 = "cloud_turbo_sched_policy_list"; // const-string v1, "cloud_turbo_sched_policy_list"
	 android.provider.Settings$System .getUriFor ( v1 );
	 v1 = 	 (( android.net.Uri ) p2 ).equals ( v1 ); // invoke-virtual {p2, v1}, Landroid/net/Uri;->equals(Ljava/lang/Object;)Z
	 if ( v1 != null) { // if-eqz v1, :cond_0
		 /* .line 728 */
		 final String v1 = "Cloud PolicyList Observer onChange!"; // const-string v1, "Cloud PolicyList Observer onChange!"
		 android.util.Slog .i ( v0,v1 );
		 /* .line 729 */
		 v1 = this.this$0;
		 com.miui.server.turbosched.TurboSchedManagerService .-$$Nest$mupdateCloudPolicyListProp ( v1 );
		 /* .line 731 */
	 } // :cond_0
	 final String v1 = "Cloud PolicyList Observer onChange done!"; // const-string v1, "Cloud PolicyList Observer onChange done!"
	 android.util.Slog .i ( v0,v1 );
	 /* .line 732 */
	 return;
} // .end method
