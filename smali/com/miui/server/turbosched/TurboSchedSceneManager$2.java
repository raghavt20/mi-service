class com.miui.server.turbosched.TurboSchedSceneManager$2 extends android.content.BroadcastReceiver {
	 /* .source "TurboSchedSceneManager.java" */
	 /* # annotations */
	 /* .annotation system Ldalvik/annotation/EnclosingClass; */
	 /* value = Lcom/miui/server/turbosched/TurboSchedSceneManager; */
} // .end annotation
/* .annotation system Ldalvik/annotation/InnerClass; */
/* accessFlags = 0x0 */
/* name = null */
} // .end annotation
/* # instance fields */
final com.miui.server.turbosched.TurboSchedSceneManager this$0; //synthetic
/* # direct methods */
 com.miui.server.turbosched.TurboSchedSceneManager$2 ( ) {
/* .locals 0 */
/* .param p1, "this$0" # Lcom/miui/server/turbosched/TurboSchedSceneManager; */
/* .line 234 */
this.this$0 = p1;
/* invoke-direct {p0}, Landroid/content/BroadcastReceiver;-><init>()V */
return;
} // .end method
/* # virtual methods */
public void onReceive ( android.content.Context p0, android.content.Intent p1 ) {
/* .locals 2 */
/* .param p1, "context" # Landroid/content/Context; */
/* .param p2, "intent" # Landroid/content/Intent; */
/* .line 237 */
(( android.content.Intent ) p2 ).getAction ( ); // invoke-virtual {p2}, Landroid/content/Intent;->getAction()Ljava/lang/String;
/* if-nez v0, :cond_0 */
/* .line 238 */
return;
/* .line 240 */
} // :cond_0
(( android.content.Intent ) p2 ).getAction ( ); // invoke-virtual {p2}, Landroid/content/Intent;->getAction()Ljava/lang/String;
v1 = (( java.lang.String ) v0 ).hashCode ( ); // invoke-virtual {v0}, Ljava/lang/String;->hashCode()I
/* packed-switch v1, :pswitch_data_0 */
} // :cond_1
/* :pswitch_0 */
final String v1 = "android.intent.action.BOOT_COMPLETED"; // const-string v1, "android.intent.action.BOOT_COMPLETED"
v0 = (( java.lang.String ) v0 ).equals ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z
if ( v0 != null) { // if-eqz v0, :cond_1
int v0 = 0; // const/4 v0, 0x0
} // :goto_0
int v0 = -1; // const/4 v0, -0x1
} // :goto_1
/* packed-switch v0, :pswitch_data_1 */
/* .line 242 */
/* :pswitch_1 */
android.os.TurboSchedMonitor .getInstance ( );
v0 = (( android.os.TurboSchedMonitor ) v0 ).isDebugMode ( ); // invoke-virtual {v0}, Landroid/os/TurboSchedMonitor;->isDebugMode()Z
if ( v0 != null) { // if-eqz v0, :cond_2
/* .line 243 */
/* const-string/jumbo v0, "tsched_scene" */
/* const-string/jumbo v1, "turbosched scene manager receive boot complete message" */
android.util.Slog .i ( v0,v1 );
/* .line 245 */
} // :cond_2
v0 = this.this$0;
com.miui.server.turbosched.TurboSchedSceneManager .-$$Nest$monBootComplete ( v0 );
/* .line 246 */
/* nop */
/* .line 250 */
} // :goto_2
return;
/* :pswitch_data_0 */
/* .packed-switch 0x2f94f923 */
/* :pswitch_0 */
} // .end packed-switch
/* :pswitch_data_1 */
/* .packed-switch 0x0 */
/* :pswitch_1 */
} // .end packed-switch
} // .end method
