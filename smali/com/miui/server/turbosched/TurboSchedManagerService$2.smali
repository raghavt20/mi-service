.class Lcom/miui/server/turbosched/TurboSchedManagerService$2;
.super Landroid/database/ContentObserver;
.source "TurboSchedManagerService.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/miui/server/turbosched/TurboSchedManagerService;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/miui/server/turbosched/TurboSchedManagerService;


# direct methods
.method constructor <init>(Lcom/miui/server/turbosched/TurboSchedManagerService;Landroid/os/Handler;)V
    .locals 0
    .param p1, "this$0"    # Lcom/miui/server/turbosched/TurboSchedManagerService;
    .param p2, "handler"    # Landroid/os/Handler;

    .line 684
    iput-object p1, p0, Lcom/miui/server/turbosched/TurboSchedManagerService$2;->this$0:Lcom/miui/server/turbosched/TurboSchedManagerService;

    invoke-direct {p0, p2}, Landroid/database/ContentObserver;-><init>(Landroid/os/Handler;)V

    return-void
.end method


# virtual methods
.method public onChange(ZLandroid/net/Uri;)V
    .locals 3
    .param p1, "selfChange"    # Z
    .param p2, "uri"    # Landroid/net/Uri;

    .line 687
    const-string v0, "TurboSchedManagerService"

    if-eqz p2, :cond_0

    const-string v1, "cloud_turbo_sched_enable_v2"

    invoke-static {v1}, Landroid/provider/Settings$System;->getUriFor(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v1

    invoke-virtual {p2, v1}, Landroid/net/Uri;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 688
    const-string v1, "mCloudTurboschedMiuiSdkObserver onChange!"

    invoke-static {v0, v1}, Landroid/util/Slog;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 689
    iget-object v1, p0, Lcom/miui/server/turbosched/TurboSchedManagerService$2;->this$0:Lcom/miui/server/turbosched/TurboSchedManagerService;

    const/4 v2, 0x0

    invoke-static {v1, v2}, Lcom/miui/server/turbosched/TurboSchedManagerService;->-$$Nest$mupdateEnableMiuiSdkProp(Lcom/miui/server/turbosched/TurboSchedManagerService;Z)V

    .line 691
    :cond_0
    const-string v1, "mCloudTurboschedMiuiSdkObserver onChange done!"

    invoke-static {v0, v1}, Landroid/util/Slog;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 692
    return-void
.end method
