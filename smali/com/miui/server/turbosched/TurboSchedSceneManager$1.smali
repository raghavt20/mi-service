.class Lcom/miui/server/turbosched/TurboSchedSceneManager$1;
.super Ljava/lang/Object;
.source "TurboSchedSceneManager.java"

# interfaces
.implements Landroid/os/IBinder$DeathRecipient;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/miui/server/turbosched/TurboSchedSceneManager;->registerStateChangeCallback(Ljava/lang/String;Lmiui/turbosched/ITurboSchedManager$ITurboSchedStateChangeCallback;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/miui/server/turbosched/TurboSchedSceneManager;

.field final synthetic val$cb:Lmiui/turbosched/ITurboSchedManager$ITurboSchedStateChangeCallback;

.field final synthetic val$pkgName:Ljava/lang/String;


# direct methods
.method constructor <init>(Lcom/miui/server/turbosched/TurboSchedSceneManager;Ljava/lang/String;Lmiui/turbosched/ITurboSchedManager$ITurboSchedStateChangeCallback;)V
    .locals 0
    .param p1, "this$0"    # Lcom/miui/server/turbosched/TurboSchedSceneManager;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .line 145
    iput-object p1, p0, Lcom/miui/server/turbosched/TurboSchedSceneManager$1;->this$0:Lcom/miui/server/turbosched/TurboSchedSceneManager;

    iput-object p2, p0, Lcom/miui/server/turbosched/TurboSchedSceneManager$1;->val$pkgName:Ljava/lang/String;

    iput-object p3, p0, Lcom/miui/server/turbosched/TurboSchedSceneManager$1;->val$cb:Lmiui/turbosched/ITurboSchedManager$ITurboSchedStateChangeCallback;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public binderDied()V
    .locals 3

    .line 148
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "binder died pkg="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lcom/miui/server/turbosched/TurboSchedSceneManager$1;->val$pkgName:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", cb="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lcom/miui/server/turbosched/TurboSchedSceneManager$1;->val$cb:Lmiui/turbosched/ITurboSchedManager$ITurboSchedStateChangeCallback;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    const-string/jumbo v1, "tsched_scene"

    invoke-static {v1, v0}, Landroid/util/Slog;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 149
    iget-object v0, p0, Lcom/miui/server/turbosched/TurboSchedSceneManager$1;->val$cb:Lmiui/turbosched/ITurboSchedManager$ITurboSchedStateChangeCallback;

    invoke-interface {v0}, Lmiui/turbosched/ITurboSchedManager$ITurboSchedStateChangeCallback;->asBinder()Landroid/os/IBinder;

    move-result-object v0

    const/4 v1, 0x0

    invoke-interface {v0, p0, v1}, Landroid/os/IBinder;->unlinkToDeath(Landroid/os/IBinder$DeathRecipient;I)Z

    .line 150
    iget-object v0, p0, Lcom/miui/server/turbosched/TurboSchedSceneManager$1;->this$0:Lcom/miui/server/turbosched/TurboSchedSceneManager;

    iget-object v1, p0, Lcom/miui/server/turbosched/TurboSchedSceneManager$1;->val$pkgName:Ljava/lang/String;

    iget-object v2, p0, Lcom/miui/server/turbosched/TurboSchedSceneManager$1;->val$cb:Lmiui/turbosched/ITurboSchedManager$ITurboSchedStateChangeCallback;

    invoke-virtual {v0, v1, v2}, Lcom/miui/server/turbosched/TurboSchedSceneManager;->unregisterStateChangeCallback(Ljava/lang/String;Lmiui/turbosched/ITurboSchedManager$ITurboSchedStateChangeCallback;)V

    .line 151
    return-void
.end method
