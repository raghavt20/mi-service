public class com.miui.server.turbosched.TurboSchedUtil {
	 /* .source "TurboSchedUtil.java" */
	 /* # static fields */
	 private static final java.lang.String TAG;
	 /* # direct methods */
	 public com.miui.server.turbosched.TurboSchedUtil ( ) {
		 /* .locals 0 */
		 /* .line 12 */
		 /* invoke-direct {p0}, Ljava/lang/Object;-><init>()V */
		 return;
	 } // .end method
	 protected static Boolean checkFileAccess ( java.lang.String p0 ) {
		 /* .locals 2 */
		 /* .param p0, "path" # Ljava/lang/String; */
		 /* .line 16 */
		 /* new-instance v0, Ljava/io/File; */
		 /* invoke-direct {v0, p0}, Ljava/io/File;-><init>(Ljava/lang/String;)V */
		 /* .line 17 */
		 /* .local v0, "file":Ljava/io/File; */
		 v1 = 		 (( java.io.File ) v0 ).exists ( ); // invoke-virtual {v0}, Ljava/io/File;->exists()Z
	 } // .end method
	 protected static Boolean readBooleanValueFromFile ( java.lang.String p0 ) {
		 /* .locals 2 */
		 /* .param p0, "path" # Ljava/lang/String; */
		 /* .line 22 */
		 com.miui.server.turbosched.TurboSchedUtil .readValueFromFile ( p0 );
		 /* .line 23 */
		 /* .local v0, "value":Ljava/lang/String; */
		 final String v1 = "1"; // const-string v1, "1"
		 v1 = 		 (( java.lang.String ) v1 ).equals ( v0 ); // invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z
	 } // .end method
	 protected static java.lang.String readValueFromFile ( java.lang.String p0 ) {
		 /* .locals 3 */
		 /* .param p0, "path" # Ljava/lang/String; */
		 /* .line 29 */
		 try { // :try_start_0
			 /* new-instance v0, Ljava/io/BufferedReader; */
			 /* new-instance v1, Ljava/io/FileReader; */
			 /* invoke-direct {v1, p0}, Ljava/io/FileReader;-><init>(Ljava/lang/String;)V */
			 /* invoke-direct {v0, v1}, Ljava/io/BufferedReader;-><init>(Ljava/io/Reader;)V */
			 /* .line 30 */
			 /* .local v0, "reader":Ljava/io/BufferedReader; */
			 (( java.io.BufferedReader ) v0 ).readLine ( ); // invoke-virtual {v0}, Ljava/io/BufferedReader;->readLine()Ljava/lang/String;
			 /* .line 31 */
			 /* .local v1, "value":Ljava/lang/String; */
			 (( java.io.BufferedReader ) v0 ).close ( ); // invoke-virtual {v0}, Ljava/io/BufferedReader;->close()V
			 /* :try_end_0 */
			 /* .catch Ljava/io/IOException; {:try_start_0 ..:try_end_0} :catch_0 */
			 /* .line 35 */
		 } // .end local v0 # "reader":Ljava/io/BufferedReader;
		 /* nop */
		 /* .line 36 */
		 /* .line 32 */
	 } // .end local v1 # "value":Ljava/lang/String;
	 /* :catch_0 */
	 /* move-exception v0 */
	 /* .line 33 */
	 /* .local v0, "e":Ljava/io/IOException; */
	 /* new-instance v1, Ljava/lang/StringBuilder; */
	 /* invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V */
	 final String v2 = "read file "; // const-string v2, "read file "
	 (( java.lang.StringBuilder ) v1 ).append ( v2 ); // invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
	 (( java.lang.StringBuilder ) v1 ).append ( p0 ); // invoke-virtual {v1, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
	 final String v2 = " failed"; // const-string v2, " failed"
	 (( java.lang.StringBuilder ) v1 ).append ( v2 ); // invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
	 (( java.lang.StringBuilder ) v1 ).toString ( ); // invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
	 final String v2 = "TurboSched"; // const-string v2, "TurboSched"
	 android.util.Slog .e ( v2,v1 );
	 /* .line 34 */
	 int v1 = 0; // const/4 v1, 0x0
} // .end method
protected static java.lang.String writeValueToFile ( java.lang.String p0, java.lang.String p1 ) {
	 /* .locals 2 */
	 /* .param p0, "path" # Ljava/lang/String; */
	 /* .param p1, "value" # Ljava/lang/String; */
	 /* .line 41 */
	 try { // :try_start_0
		 /* new-instance v0, Ljava/io/BufferedWriter; */
		 /* new-instance v1, Ljava/io/FileWriter; */
		 /* invoke-direct {v1, p0}, Ljava/io/FileWriter;-><init>(Ljava/lang/String;)V */
		 /* invoke-direct {v0, v1}, Ljava/io/BufferedWriter;-><init>(Ljava/io/Writer;)V */
		 /* .line 42 */
		 /* .local v0, "writer":Ljava/io/BufferedWriter; */
		 (( java.io.BufferedWriter ) v0 ).write ( p1 ); // invoke-virtual {v0, p1}, Ljava/io/BufferedWriter;->write(Ljava/lang/String;)V
		 /* .line 43 */
		 (( java.io.BufferedWriter ) v0 ).close ( ); // invoke-virtual {v0}, Ljava/io/BufferedWriter;->close()V
		 /* :try_end_0 */
		 /* .catch Ljava/io/IOException; {:try_start_0 ..:try_end_0} :catch_0 */
		 /* .line 46 */
	 } // .end local v0 # "writer":Ljava/io/BufferedWriter;
	 /* nop */
	 /* .line 47 */
	 int v0 = 0; // const/4 v0, 0x0
	 /* .line 44 */
	 /* :catch_0 */
	 /* move-exception v0 */
	 /* .line 45 */
	 /* .local v0, "e":Ljava/io/IOException; */
	 (( java.io.IOException ) v0 ).getMessage ( ); // invoke-virtual {v0}, Ljava/io/IOException;->getMessage()Ljava/lang/String;
} // .end method
