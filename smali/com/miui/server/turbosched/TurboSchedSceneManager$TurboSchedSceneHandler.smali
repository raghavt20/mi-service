.class Lcom/miui/server/turbosched/TurboSchedSceneManager$TurboSchedSceneHandler;
.super Landroid/os/Handler;
.source "TurboSchedSceneManager.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/miui/server/turbosched/TurboSchedSceneManager;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = "TurboSchedSceneHandler"
.end annotation


# instance fields
.field final synthetic this$0:Lcom/miui/server/turbosched/TurboSchedSceneManager;


# direct methods
.method public constructor <init>(Lcom/miui/server/turbosched/TurboSchedSceneManager;Landroid/os/Looper;)V
    .locals 0
    .param p1, "this$0"    # Lcom/miui/server/turbosched/TurboSchedSceneManager;
    .param p2, "looper"    # Landroid/os/Looper;

    .line 465
    iput-object p1, p0, Lcom/miui/server/turbosched/TurboSchedSceneManager$TurboSchedSceneHandler;->this$0:Lcom/miui/server/turbosched/TurboSchedSceneManager;

    .line 466
    invoke-direct {p0, p2}, Landroid/os/Handler;-><init>(Landroid/os/Looper;)V

    .line 467
    return-void
.end method


# virtual methods
.method public handleMessage(Landroid/os/Message;)V
    .locals 3
    .param p1, "msg"    # Landroid/os/Message;

    .line 471
    iget v0, p1, Landroid/os/Message;->what:I

    .line 472
    .local v0, "what":I
    sparse-switch v0, :sswitch_data_0

    goto :goto_0

    .line 487
    :sswitch_0
    iget-object v1, p0, Lcom/miui/server/turbosched/TurboSchedSceneManager$TurboSchedSceneHandler;->this$0:Lcom/miui/server/turbosched/TurboSchedSceneManager;

    invoke-static {v1, p1}, Lcom/miui/server/turbosched/TurboSchedSceneManager;->-$$Nest$mhandleFreeformModeChanged(Lcom/miui/server/turbosched/TurboSchedSceneManager;Landroid/os/Message;)V

    .line 488
    goto :goto_0

    .line 480
    :sswitch_1
    iget-object v1, p0, Lcom/miui/server/turbosched/TurboSchedSceneManager$TurboSchedSceneHandler;->this$0:Lcom/miui/server/turbosched/TurboSchedSceneManager;

    invoke-static {v1, p1}, Lcom/miui/server/turbosched/TurboSchedSceneManager;->-$$Nest$mhandleFocusAppChange(Lcom/miui/server/turbosched/TurboSchedSceneManager;Landroid/os/Message;)V

    .line 481
    goto :goto_0

    .line 477
    :sswitch_2
    iget-object v1, p0, Lcom/miui/server/turbosched/TurboSchedSceneManager$TurboSchedSceneHandler;->this$0:Lcom/miui/server/turbosched/TurboSchedSceneManager;

    iget-object v2, p1, Landroid/os/Message;->obj:Ljava/lang/Object;

    check-cast v2, Lmiui/process/ForegroundInfo;

    invoke-static {v1, v2}, Lcom/miui/server/turbosched/TurboSchedSceneManager;->-$$Nest$mhandleWindowChanged(Lcom/miui/server/turbosched/TurboSchedSceneManager;Lmiui/process/ForegroundInfo;)V

    .line 478
    goto :goto_0

    .line 475
    :sswitch_3
    nop

    .line 492
    :goto_0
    iget-object v1, p0, Lcom/miui/server/turbosched/TurboSchedSceneManager$TurboSchedSceneHandler;->this$0:Lcom/miui/server/turbosched/TurboSchedSceneManager;

    invoke-static {v1}, Lcom/miui/server/turbosched/TurboSchedSceneManager;->-$$Nest$mmakeTurboSchedDesion(Lcom/miui/server/turbosched/TurboSchedSceneManager;)V

    .line 493
    return-void

    nop

    :sswitch_data_0
    .sparse-switch
        0x1 -> :sswitch_3
        0x2 -> :sswitch_2
        0x3 -> :sswitch_1
        0xb -> :sswitch_0
    .end sparse-switch
.end method
