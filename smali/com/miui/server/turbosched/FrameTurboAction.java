public class com.miui.server.turbosched.FrameTurboAction {
	 /* .source "FrameTurboAction.java" */
	 /* # static fields */
	 private static final java.lang.String TAG;
	 /* # instance fields */
	 private java.util.Map mActiveSceneWaitListMap;
	 /* .annotation system Ldalvik/annotation/Signature; */
	 /* value = { */
	 /* "Ljava/util/Map<", */
	 /* "Ljava/lang/String;", */
	 /* "Ljava/util/LinkedList<", */
	 /* "Ljava/lang/String;", */
	 /* ">;>;" */
	 /* } */
} // .end annotation
} // .end field
private java.lang.String mCurrentTurboScene;
protected Boolean mDebug;
protected java.util.Map mFrameTurboAppMap;
/* .annotation system Ldalvik/annotation/Signature; */
/* value = { */
/* "Ljava/util/Map<", */
/* "Ljava/lang/String;", */
/* "Ljava/util/List<", */
/* "Ljava/lang/String;", */
/* ">;>;" */
/* } */
} // .end annotation
} // .end field
protected java.util.List mFrameTurboSceneCallbacks;
/* .annotation system Ldalvik/annotation/Signature; */
/* value = { */
/* "Ljava/util/List<", */
/* "Lmiui/turbosched/ITurboSchedManager$IFrameTurboSceneCallback;", */
/* ">;" */
/* } */
} // .end annotation
} // .end field
protected java.util.Map mFrameTurboSceneWhiteListMap;
/* .annotation system Ldalvik/annotation/Signature; */
/* value = { */
/* "Ljava/util/Map<", */
/* "Ljava/lang/String;", */
/* "Ljava/util/List<", */
/* "Ljava/lang/String;", */
/* ">;>;" */
/* } */
} // .end annotation
} // .end field
private android.util.LocalLog mHistoryLog;
/* # direct methods */
protected com.miui.server.turbosched.FrameTurboAction ( ) {
/* .locals 2 */
/* .line 35 */
/* invoke-direct {p0}, Ljava/lang/Object;-><init>()V */
/* .line 26 */
/* new-instance v0, Landroid/util/LocalLog; */
/* const/16 v1, 0x200 */
/* invoke-direct {v0, v1}, Landroid/util/LocalLog;-><init>(I)V */
this.mHistoryLog = v0;
/* .line 27 */
final String v0 = ""; // const-string v0, ""
this.mCurrentTurboScene = v0;
/* .line 28 */
/* new-instance v0, Ljava/util/HashMap; */
/* invoke-direct {v0}, Ljava/util/HashMap;-><init>()V */
this.mActiveSceneWaitListMap = v0;
/* .line 32 */
int v0 = 0; // const/4 v0, 0x0
/* iput-boolean v0, p0, Lcom/miui/server/turbosched/FrameTurboAction;->mDebug:Z */
/* .line 33 */
/* new-instance v0, Ljava/util/ArrayList; */
/* invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V */
this.mFrameTurboSceneCallbacks = v0;
/* .line 36 */
/* invoke-direct {p0}, Lcom/miui/server/turbosched/FrameTurboAction;->initFrameTurboAppMap()Ljava/util/Map; */
this.mFrameTurboAppMap = v0;
/* .line 37 */
/* invoke-direct {p0}, Lcom/miui/server/turbosched/FrameTurboAction;->initFrameTurboSceneWhiteListMap()Ljava/util/Map; */
this.mFrameTurboSceneWhiteListMap = v0;
/* .line 38 */
return;
} // .end method
private Integer checkPermission ( java.lang.String p0, java.lang.String p1 ) {
/* .locals 5 */
/* .param p1, "packageName" # Ljava/lang/String; */
/* .param p2, "sceneId" # Ljava/lang/String; */
/* .line 308 */
final String v0 = "TurboSched_FrameTurbo"; // const-string v0, "TurboSched_FrameTurbo"
int v1 = -4; // const/4 v1, -0x4
final String v2 = ",sceneId: "; // const-string v2, ",sceneId: "
final String v3 = "TS [MARK] : failed, R: not in white list, packageName: "; // const-string v3, "TS [MARK] : failed, R: not in white list, packageName: "
if ( p1 != null) { // if-eqz p1, :cond_4
v4 = (( java.lang.String ) p1 ).length ( ); // invoke-virtual {p1}, Ljava/lang/String;->length()I
if ( v4 != null) { // if-eqz v4, :cond_4
v4 = v4 = this.mFrameTurboSceneWhiteListMap;
/* if-nez v4, :cond_0 */
/* .line 316 */
} // :cond_0
if ( p2 != null) { // if-eqz p2, :cond_2
v4 = (( java.lang.String ) p2 ).length ( ); // invoke-virtual {p2}, Ljava/lang/String;->length()I
if ( v4 != null) { // if-eqz v4, :cond_2
v4 = this.mFrameTurboSceneWhiteListMap;
if ( v4 != null) { // if-eqz v4, :cond_2
v4 = this.mFrameTurboSceneWhiteListMap;
v4 = /* check-cast v4, Ljava/util/List; */
/* if-nez v4, :cond_1 */
/* .line 323 */
} // :cond_1
int v0 = 0; // const/4 v0, 0x0
/* .line 317 */
} // :cond_2
} // :goto_0
/* iget-boolean v4, p0, Lcom/miui/server/turbosched/FrameTurboAction;->mDebug:Z */
if ( v4 != null) { // if-eqz v4, :cond_3
/* .line 318 */
/* new-instance v4, Ljava/lang/StringBuilder; */
/* invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V */
(( java.lang.StringBuilder ) v4 ).append ( v3 ); // invoke-virtual {v4, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v4 ).append ( p1 ); // invoke-virtual {v4, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v4 ).append ( v2 ); // invoke-virtual {v4, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v4 ).append ( p2 ); // invoke-virtual {v4, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v4 ).toString ( ); // invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
android.util.Slog .d ( v0,v4 );
/* .line 320 */
} // :cond_3
v0 = this.mHistoryLog;
/* new-instance v4, Ljava/lang/StringBuilder; */
/* invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V */
(( java.lang.StringBuilder ) v4 ).append ( v3 ); // invoke-virtual {v4, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v3 ).append ( p1 ); // invoke-virtual {v3, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v3 ).append ( v2 ); // invoke-virtual {v3, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v2 ).append ( p2 ); // invoke-virtual {v2, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v2 ).toString ( ); // invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
(( android.util.LocalLog ) v0 ).log ( v2 ); // invoke-virtual {v0, v2}, Landroid/util/LocalLog;->log(Ljava/lang/String;)V
/* .line 321 */
/* .line 309 */
} // :cond_4
} // :goto_1
/* iget-boolean v4, p0, Lcom/miui/server/turbosched/FrameTurboAction;->mDebug:Z */
if ( v4 != null) { // if-eqz v4, :cond_5
/* .line 310 */
/* new-instance v4, Ljava/lang/StringBuilder; */
/* invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V */
(( java.lang.StringBuilder ) v4 ).append ( v3 ); // invoke-virtual {v4, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v4 ).append ( p1 ); // invoke-virtual {v4, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v4 ).append ( v2 ); // invoke-virtual {v4, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v4 ).append ( p2 ); // invoke-virtual {v4, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v4 ).toString ( ); // invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
android.util.Slog .d ( v0,v4 );
/* .line 312 */
} // :cond_5
v0 = this.mHistoryLog;
/* new-instance v4, Ljava/lang/StringBuilder; */
/* invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V */
(( java.lang.StringBuilder ) v4 ).append ( v3 ); // invoke-virtual {v4, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v3 ).append ( p1 ); // invoke-virtual {v3, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v3 ).append ( v2 ); // invoke-virtual {v3, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v2 ).append ( p2 ); // invoke-virtual {v2, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v2 ).toString ( ); // invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
(( android.util.LocalLog ) v0 ).log ( v2 ); // invoke-virtual {v0, v2}, Landroid/util/LocalLog;->log(Ljava/lang/String;)V
/* .line 313 */
} // .end method
private Boolean checkSceneFormat ( java.lang.String p0 ) {
/* .locals 9 */
/* .param p1, "scene" # Ljava/lang/String; */
/* .line 205 */
final String v0 = ","; // const-string v0, ","
(( java.lang.String ) p1 ).split ( v0 ); // invoke-virtual {p1, v0}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;
/* .line 206 */
/* .local v0, "appScenes":[Ljava/lang/String; */
/* array-length v1, v0 */
int v2 = 0; // const/4 v2, 0x0
/* move v3, v2 */
} // :goto_0
/* if-ge v3, v1, :cond_2 */
/* aget-object v4, v0, v3 */
/* .line 207 */
/* .local v4, "appScene":Ljava/lang/String; */
final String v5 = ":"; // const-string v5, ":"
(( java.lang.String ) v4 ).split ( v5 ); // invoke-virtual {v4, v5}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;
/* .line 208 */
/* .local v5, "appSceneConfig":[Ljava/lang/String; */
/* array-length v6, v5 */
int v7 = 2; // const/4 v7, 0x2
/* if-ge v6, v7, :cond_0 */
/* .line 209 */
/* .line 211 */
} // :cond_0
/* aget-object v6, v5, v2 */
final String v8 = "#"; // const-string v8, "#"
(( java.lang.String ) v6 ).split ( v8 ); // invoke-virtual {v6, v8}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;
/* .line 212 */
/* .local v6, "scenes":[Ljava/lang/String; */
/* array-length v8, v6 */
/* if-ge v8, v7, :cond_1 */
/* .line 213 */
/* .line 206 */
} // .end local v4 # "appScene":Ljava/lang/String;
} // .end local v5 # "appSceneConfig":[Ljava/lang/String;
} // .end local v6 # "scenes":[Ljava/lang/String;
} // :cond_1
/* add-int/lit8 v3, v3, 0x1 */
/* .line 216 */
} // :cond_2
int v1 = 1; // const/4 v1, 0x1
} // .end method
private java.util.Map initFrameTurboAppMap ( ) {
/* .locals 14 */
/* .annotation system Ldalvik/annotation/Signature; */
/* value = { */
/* "()", */
/* "Ljava/util/Map<", */
/* "Ljava/lang/String;", */
/* "Ljava/util/List<", */
/* "Ljava/lang/String;", */
/* ">;>;" */
/* } */
} // .end annotation
/* .line 41 */
/* new-instance v0, Ljava/util/HashMap; */
/* invoke-direct {v0}, Ljava/util/HashMap;-><init>()V */
/* .line 42 */
/* .local v0, "appMap":Ljava/util/Map;, "Ljava/util/Map<Ljava/lang/String;Ljava/util/List<Ljava/lang/String;>;>;" */
final String v1 = "persist.sys.turbosched.frame_turbo.apps"; // const-string v1, "persist.sys.turbosched.frame_turbo.apps"
final String v2 = ""; // const-string v2, ""
android.os.SystemProperties .get ( v1,v2 );
/* .line 43 */
/* .local v1, "appListStr":Ljava/lang/String; */
final String v2 = "TurboSched_FrameTurbo"; // const-string v2, "TurboSched_FrameTurbo"
if ( v1 != null) { // if-eqz v1, :cond_1
v3 = (( java.lang.String ) v1 ).isEmpty ( ); // invoke-virtual {v1}, Ljava/lang/String;->isEmpty()Z
/* if-nez v3, :cond_1 */
/* .line 44 */
final String v3 = ","; // const-string v3, ","
(( java.lang.String ) v1 ).split ( v3 ); // invoke-virtual {v1, v3}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;
/* .line 45 */
/* .local v3, "appList":[Ljava/lang/String; */
/* array-length v4, v3 */
int v5 = 0; // const/4 v5, 0x0
/* move v6, v5 */
} // :goto_0
/* if-ge v6, v4, :cond_1 */
/* aget-object v7, v3, v6 */
/* .line 46 */
/* .local v7, "app":Ljava/lang/String; */
final String v8 = ":"; // const-string v8, ":"
(( java.lang.String ) v7 ).split ( v8 ); // invoke-virtual {v7, v8}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;
/* .line 47 */
/* .local v8, "appConfig":[Ljava/lang/String; */
/* new-instance v9, Ljava/util/ArrayList; */
/* invoke-direct {v9}, Ljava/util/ArrayList;-><init>()V */
/* .line 48 */
/* .local v9, "sceneList":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;" */
/* array-length v10, v8 */
int v11 = 1; // const/4 v11, 0x1
/* if-le v10, v11, :cond_0 */
/* .line 49 */
/* aget-object v10, v8, v11 */
final String v11 = "\\|"; // const-string v11, "\\|"
(( java.lang.String ) v10 ).split ( v11 ); // invoke-virtual {v10, v11}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;
/* .line 50 */
/* .local v10, "scenes":[Ljava/lang/String; */
/* new-instance v11, Ljava/lang/StringBuilder; */
/* invoke-direct {v11}, Ljava/lang/StringBuilder;-><init>()V */
final String v12 = "scenes: "; // const-string v12, "scenes: "
(( java.lang.StringBuilder ) v11 ).append ( v12 ); // invoke-virtual {v11, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
/* aget-object v12, v10, v5 */
(( java.lang.StringBuilder ) v11 ).append ( v12 ); // invoke-virtual {v11, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
final String v12 = ", size = "; // const-string v12, ", size = "
(( java.lang.StringBuilder ) v11 ).append ( v12 ); // invoke-virtual {v11, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
/* array-length v12, v10 */
(( java.lang.StringBuilder ) v11 ).append ( v12 ); // invoke-virtual {v11, v12}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v11 ).toString ( ); // invoke-virtual {v11}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
android.util.Slog .d ( v2,v11 );
/* .line 51 */
/* array-length v11, v10 */
/* move v12, v5 */
} // :goto_1
/* if-ge v12, v11, :cond_0 */
/* aget-object v13, v10, v12 */
/* .line 52 */
/* .local v13, "scene":Ljava/lang/String; */
/* .line 51 */
} // .end local v13 # "scene":Ljava/lang/String;
/* add-int/lit8 v12, v12, 0x1 */
/* .line 55 */
} // .end local v10 # "scenes":[Ljava/lang/String;
} // :cond_0
/* aget-object v10, v8, v5 */
/* .line 45 */
} // .end local v7 # "app":Ljava/lang/String;
} // .end local v8 # "appConfig":[Ljava/lang/String;
} // .end local v9 # "sceneList":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;"
/* add-int/lit8 v6, v6, 0x1 */
/* .line 58 */
} // .end local v3 # "appList":[Ljava/lang/String;
} // :cond_1
/* iget-boolean v3, p0, Lcom/miui/server/turbosched/FrameTurboAction;->mDebug:Z */
if ( v3 != null) { // if-eqz v3, :cond_2
/* .line 59 */
/* new-instance v3, Ljava/lang/StringBuilder; */
/* invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V */
final String v4 = "frame turbo init success, appMap: "; // const-string v4, "frame turbo init success, appMap: "
(( java.lang.StringBuilder ) v3 ).append ( v4 ); // invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.Object ) v0 ).toString ( ); // invoke-virtual {v0}, Ljava/lang/Object;->toString()Ljava/lang/String;
(( java.lang.StringBuilder ) v3 ).append ( v4 ); // invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v3 ).toString ( ); // invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
android.util.Slog .d ( v2,v3 );
/* .line 61 */
} // :cond_2
} // .end method
private java.util.Map initFrameTurboSceneWhiteListMap ( ) {
/* .locals 13 */
/* .annotation system Ldalvik/annotation/Signature; */
/* value = { */
/* "()", */
/* "Ljava/util/Map<", */
/* "Ljava/lang/String;", */
/* "Ljava/util/List<", */
/* "Ljava/lang/String;", */
/* ">;>;" */
/* } */
} // .end annotation
/* .line 65 */
/* new-instance v0, Ljava/util/HashMap; */
/* invoke-direct {v0}, Ljava/util/HashMap;-><init>()V */
/* .line 66 */
/* .local v0, "sceneMap":Ljava/util/Map;, "Ljava/util/Map<Ljava/lang/String;Ljava/util/List<Ljava/lang/String;>;>;" */
final String v1 = "persist.sys.turbosched.frame_turbo.scene_white_list"; // const-string v1, "persist.sys.turbosched.frame_turbo.scene_white_list"
final String v2 = ""; // const-string v2, ""
android.os.SystemProperties .get ( v1,v2 );
/* .line 67 */
/* .local v1, "sceneWhiteListStr":Ljava/lang/String; */
if ( v1 != null) { // if-eqz v1, :cond_2
v2 = (( java.lang.String ) v1 ).isEmpty ( ); // invoke-virtual {v1}, Ljava/lang/String;->isEmpty()Z
/* if-nez v2, :cond_2 */
/* .line 68 */
final String v2 = ","; // const-string v2, ","
(( java.lang.String ) v1 ).split ( v2 ); // invoke-virtual {v1, v2}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;
/* .line 69 */
/* .local v2, "sceneWhiteList":[Ljava/lang/String; */
/* array-length v3, v2 */
int v4 = 0; // const/4 v4, 0x0
/* move v5, v4 */
} // :goto_0
/* if-ge v5, v3, :cond_2 */
/* aget-object v6, v2, v5 */
/* .line 70 */
/* .local v6, "scene":Ljava/lang/String; */
final String v7 = ":"; // const-string v7, ":"
(( java.lang.String ) v6 ).split ( v7 ); // invoke-virtual {v6, v7}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;
/* .line 71 */
/* .local v7, "sceneConfig":[Ljava/lang/String; */
/* new-instance v8, Ljava/util/ArrayList; */
/* invoke-direct {v8}, Ljava/util/ArrayList;-><init>()V */
/* .line 72 */
/* .local v8, "sceneList":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;" */
/* array-length v9, v7 */
int v10 = 1; // const/4 v10, 0x1
/* if-le v9, v10, :cond_1 */
/* .line 73 */
/* aget-object v9, v7, v10 */
final String v10 = "\\|"; // const-string v10, "\\|"
(( java.lang.String ) v9 ).split ( v10 ); // invoke-virtual {v9, v10}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;
/* .line 74 */
/* .local v9, "scenes":[Ljava/lang/String; */
/* array-length v10, v9 */
/* move v11, v4 */
} // :goto_1
/* if-ge v11, v10, :cond_0 */
/* aget-object v12, v9, v11 */
/* .line 75 */
/* .local v12, "s":Ljava/lang/String; */
/* .line 74 */
} // .end local v12 # "s":Ljava/lang/String;
/* add-int/lit8 v11, v11, 0x1 */
/* .line 77 */
} // :cond_0
/* aget-object v10, v7, v4 */
/* .line 69 */
} // .end local v6 # "scene":Ljava/lang/String;
} // .end local v7 # "sceneConfig":[Ljava/lang/String;
} // .end local v8 # "sceneList":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;"
} // .end local v9 # "scenes":[Ljava/lang/String;
} // :cond_1
/* add-int/lit8 v5, v5, 0x1 */
/* .line 81 */
} // .end local v2 # "sceneWhiteList":[Ljava/lang/String;
} // :cond_2
} // .end method
private Boolean parseCommandArgs ( java.io.FileDescriptor p0, java.io.PrintWriter p1, java.lang.String[] p2 ) {
/* .locals 4 */
/* .param p1, "fd" # Ljava/io/FileDescriptor; */
/* .param p2, "pw" # Ljava/io/PrintWriter; */
/* .param p3, "args" # [Ljava/lang/String; */
/* .line 85 */
int v0 = 0; // const/4 v0, 0x0
/* .line 86 */
/* .local v0, "result":Z */
/* array-length v1, p3 */
int v2 = 2; // const/4 v2, 0x2
/* if-ge v1, v2, :cond_0 */
/* .line 87 */
/* .line 89 */
} // :cond_0
int v1 = 1; // const/4 v1, 0x1
/* aget-object v1, p3, v1 */
/* .line 90 */
/* .local v1, "command":Ljava/lang/String; */
/* array-length v3, p3 */
/* if-ne v3, v2, :cond_1 */
/* .line 91 */
v0 = /* invoke-direct {p0, p1, p2, v1}, Lcom/miui/server/turbosched/FrameTurboAction;->parseCommandWithoutArgs(Ljava/io/FileDescriptor;Ljava/io/PrintWriter;Ljava/lang/String;)Z */
/* .line 93 */
} // :cond_1
v0 = /* invoke-direct {p0, p1, p2, v1, p3}, Lcom/miui/server/turbosched/FrameTurboAction;->parseCommandWithArgs(Ljava/io/FileDescriptor;Ljava/io/PrintWriter;Ljava/lang/String;[Ljava/lang/String;)Z */
/* .line 95 */
} // :goto_0
} // .end method
private Boolean parseCommandWithArgs ( java.io.FileDescriptor p0, java.io.PrintWriter p1, java.lang.String p2, java.lang.String[] p3 ) {
/* .locals 2 */
/* .param p1, "fd" # Ljava/io/FileDescriptor; */
/* .param p2, "pw" # Ljava/io/PrintWriter; */
/* .param p3, "command" # Ljava/lang/String; */
/* .param p4, "args" # [Ljava/lang/String; */
/* .line 113 */
int v0 = 0; // const/4 v0, 0x0
/* .line 115 */
/* .local v0, "result":Z */
v1 = (( java.lang.String ) p3 ).hashCode ( ); // invoke-virtual {p3}, Ljava/lang/String;->hashCode()I
/* sparse-switch v1, :sswitch_data_0 */
} // :cond_0
/* :sswitch_0 */
final String v1 = "scene"; // const-string v1, "scene"
v1 = (( java.lang.String ) p3 ).equals ( v1 ); // invoke-virtual {p3, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z
if ( v1 != null) { // if-eqz v1, :cond_0
int v1 = 2; // const/4 v1, 0x2
/* :sswitch_1 */
final String v1 = "debug"; // const-string v1, "debug"
v1 = (( java.lang.String ) p3 ).equals ( v1 ); // invoke-virtual {p3, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z
if ( v1 != null) { // if-eqz v1, :cond_0
int v1 = 1; // const/4 v1, 0x1
/* :sswitch_2 */
final String v1 = "enable"; // const-string v1, "enable"
v1 = (( java.lang.String ) p3 ).equals ( v1 ); // invoke-virtual {p3, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z
if ( v1 != null) { // if-eqz v1, :cond_0
int v1 = 0; // const/4 v1, 0x0
} // :goto_0
int v1 = -1; // const/4 v1, -0x1
} // :goto_1
/* packed-switch v1, :pswitch_data_0 */
/* .line 123 */
/* :pswitch_0 */
v0 = /* invoke-direct {p0, p4, p2}, Lcom/miui/server/turbosched/FrameTurboAction;->parseSceneCommand([Ljava/lang/String;Ljava/io/PrintWriter;)Z */
/* .line 120 */
/* :pswitch_1 */
v0 = /* invoke-direct {p0, p4, p2}, Lcom/miui/server/turbosched/FrameTurboAction;->parseDebugCommand([Ljava/lang/String;Ljava/io/PrintWriter;)Z */
/* .line 121 */
/* .line 117 */
/* :pswitch_2 */
v0 = /* invoke-direct {p0, p4, p2}, Lcom/miui/server/turbosched/FrameTurboAction;->parseEnableCommand([Ljava/lang/String;Ljava/io/PrintWriter;)Z */
/* .line 118 */
/* nop */
/* .line 126 */
} // :goto_2
/* :sswitch_data_0 */
/* .sparse-switch */
/* -0x4d6ada7d -> :sswitch_2 */
/* 0x5b09653 -> :sswitch_1 */
/* 0x683188c -> :sswitch_0 */
} // .end sparse-switch
/* :pswitch_data_0 */
/* .packed-switch 0x0 */
/* :pswitch_2 */
/* :pswitch_1 */
/* :pswitch_0 */
} // .end packed-switch
} // .end method
private Boolean parseCommandWithoutArgs ( java.io.FileDescriptor p0, java.io.PrintWriter p1, java.lang.String p2 ) {
/* .locals 3 */
/* .param p1, "fd" # Ljava/io/FileDescriptor; */
/* .param p2, "pw" # Ljava/io/PrintWriter; */
/* .param p3, "command" # Ljava/lang/String; */
/* .line 99 */
int v0 = 0; // const/4 v0, 0x0
/* .line 100 */
/* .local v0, "result":Z */
v1 = (( java.lang.String ) p3 ).hashCode ( ); // invoke-virtual {p3}, Ljava/lang/String;->hashCode()I
int v2 = 0; // const/4 v2, 0x0
/* packed-switch v1, :pswitch_data_0 */
} // :cond_0
/* :pswitch_0 */
final String v1 = "history"; // const-string v1, "history"
v1 = (( java.lang.String ) p3 ).equals ( v1 ); // invoke-virtual {p3, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z
if ( v1 != null) { // if-eqz v1, :cond_0
/* move v1, v2 */
} // :goto_0
int v1 = -1; // const/4 v1, -0x1
} // :goto_1
/* packed-switch v1, :pswitch_data_1 */
/* .line 106 */
int v0 = 0; // const/4 v0, 0x0
/* .line 102 */
/* :pswitch_1 */
v1 = this.mHistoryLog;
/* new-array v2, v2, [Ljava/lang/String; */
(( android.util.LocalLog ) v1 ).dump ( p1, p2, v2 ); // invoke-virtual {v1, p1, p2, v2}, Landroid/util/LocalLog;->dump(Ljava/io/FileDescriptor;Ljava/io/PrintWriter;[Ljava/lang/String;)V
/* .line 103 */
int v0 = 1; // const/4 v0, 0x1
/* .line 104 */
/* nop */
/* .line 109 */
} // :goto_2
/* :pswitch_data_0 */
/* .packed-switch 0x373fe494 */
/* :pswitch_0 */
} // .end packed-switch
/* :pswitch_data_1 */
/* .packed-switch 0x0 */
/* :pswitch_1 */
} // .end packed-switch
} // .end method
private Boolean parseDebugCommand ( java.lang.String[] p0, java.io.PrintWriter p1 ) {
/* .locals 3 */
/* .param p1, "args" # [Ljava/lang/String; */
/* .param p2, "pw" # Ljava/io/PrintWriter; */
/* .line 151 */
int v0 = 2; // const/4 v0, 0x2
/* aget-object v0, p1, v0 */
final String v1 = "0"; // const-string v1, "0"
v0 = (( java.lang.String ) v0 ).equals ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z
int v1 = 1; // const/4 v1, 0x1
/* xor-int/2addr v0, v1 */
/* iput-boolean v0, p0, Lcom/miui/server/turbosched/FrameTurboAction;->mDebug:Z */
/* .line 152 */
/* new-instance v0, Ljava/lang/StringBuilder; */
/* invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V */
/* const-string/jumbo v2, "set debug: " */
(( java.lang.StringBuilder ) v0 ).append ( v2 ); // invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
/* iget-boolean v2, p0, Lcom/miui/server/turbosched/FrameTurboAction;->mDebug:Z */
(( java.lang.StringBuilder ) v0 ).append ( v2 ); // invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v0 ).toString ( ); // invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
(( java.io.PrintWriter ) p2 ).println ( v0 ); // invoke-virtual {p2, v0}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V
/* .line 153 */
} // .end method
private Boolean parseEnableCommand ( java.lang.String[] p0, java.io.PrintWriter p1 ) {
/* .locals 5 */
/* .param p1, "args" # [Ljava/lang/String; */
/* .param p2, "pw" # Ljava/io/PrintWriter; */
/* .line 131 */
int v0 = 2; // const/4 v0, 0x2
/* aget-object v0, p1, v0 */
final String v1 = "0"; // const-string v1, "0"
v0 = (( java.lang.String ) v0 ).equals ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z
int v1 = 1; // const/4 v1, 0x1
/* xor-int/2addr v0, v1 */
/* .line 132 */
/* .local v0, "enable":Z */
com.miui.server.turbosched.TurboSchedConfig .getPolicyList ( );
/* .line 133 */
/* .local v2, "policyList":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;" */
final String v3 = "frame_turbo"; // const-string v3, "frame_turbo"
if ( v0 != null) { // if-eqz v0, :cond_1
v4 = /* .line 134 */
/* if-nez v4, :cond_0 */
/* .line 135 */
/* .line 136 */
com.miui.server.turbosched.TurboSchedConfig .setPolicyList ( v2 );
/* .line 138 */
} // :cond_0
final String v3 = "enable frame turbo success"; // const-string v3, "enable frame turbo success"
(( java.io.PrintWriter ) p2 ).println ( v3 ); // invoke-virtual {p2, v3}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V
/* .line 140 */
v4 = } // :cond_1
if ( v4 != null) { // if-eqz v4, :cond_2
/* .line 141 */
/* .line 142 */
com.miui.server.turbosched.TurboSchedConfig .setPolicyList ( v2 );
/* .line 144 */
} // :cond_2
final String v3 = "disable frame turbo success"; // const-string v3, "disable frame turbo success"
(( java.io.PrintWriter ) p2 ).println ( v3 ); // invoke-virtual {p2, v3}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V
/* .line 146 */
} // :goto_0
} // .end method
private Boolean parseSceneCommand ( java.lang.String[] p0, java.io.PrintWriter p1 ) {
/* .locals 11 */
/* .param p1, "args" # [Ljava/lang/String; */
/* .param p2, "pw" # Ljava/io/PrintWriter; */
/* .line 157 */
/* array-length v0, p1 */
int v1 = 0; // const/4 v1, 0x0
int v2 = 4; // const/4 v2, 0x4
/* if-gt v0, v2, :cond_0 */
/* .line 158 */
/* .line 160 */
} // :cond_0
int v0 = 0; // const/4 v0, 0x0
/* .line 161 */
/* .local v0, "result":Z */
int v3 = 3; // const/4 v3, 0x3
/* aget-object v4, p1, v3 */
/* .line 162 */
/* .local v4, "scene":Ljava/lang/String; */
v5 = /* invoke-direct {p0, v4}, Lcom/miui/server/turbosched/FrameTurboAction;->checkSceneFormat(Ljava/lang/String;)Z */
int v6 = 1; // const/4 v6, 0x1
/* if-nez v5, :cond_1 */
/* .line 163 */
/* new-instance v1, Ljava/lang/StringBuilder; */
/* invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V */
final String v2 = "scene format has error, scene: "; // const-string v2, "scene format has error, scene: "
(( java.lang.StringBuilder ) v1 ).append ( v2 ); // invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v1 ).append ( v4 ); // invoke-virtual {v1, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v1 ).toString ( ); // invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
(( java.io.PrintWriter ) p2 ).println ( v1 ); // invoke-virtual {p2, v1}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V
/* .line 164 */
/* .line 166 */
} // :cond_1
/* new-instance v5, Ljava/util/ArrayList; */
/* invoke-direct {v5}, Ljava/util/ArrayList;-><init>()V */
/* .line 167 */
/* .local v5, "sceneList":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;" */
final String v7 = ","; // const-string v7, ","
(( java.lang.String ) v4 ).split ( v7 ); // invoke-virtual {v4, v7}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;
/* .line 168 */
/* .local v7, "appScenes":[Ljava/lang/String; */
/* array-length v8, v7 */
/* move v9, v1 */
} // :goto_0
/* if-ge v9, v8, :cond_2 */
/* aget-object v10, v7, v9 */
/* .line 169 */
/* .local v10, "appScene":Ljava/lang/String; */
/* .line 168 */
} // .end local v10 # "appScene":Ljava/lang/String;
/* add-int/lit8 v9, v9, 0x1 */
/* .line 171 */
} // :cond_2
int v8 = 2; // const/4 v8, 0x2
/* aget-object v9, p1, v8 */
/* .line 172 */
/* .local v9, "childCommand":Ljava/lang/String; */
/* aget-object v2, p1, v2 */
final String v10 = "0"; // const-string v10, "0"
v2 = (( java.lang.String ) v2 ).equals ( v10 ); // invoke-virtual {v2, v10}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z
/* xor-int/2addr v2, v6 */
/* .line 173 */
/* .local v2, "enable":Z */
v10 = (( java.lang.String ) v9 ).hashCode ( ); // invoke-virtual {v9}, Ljava/lang/String;->hashCode()I
/* sparse-switch v10, :sswitch_data_0 */
} // :cond_3
/* :sswitch_0 */
/* const-string/jumbo v1, "test" */
v1 = (( java.lang.String ) v9 ).equals ( v1 ); // invoke-virtual {v9, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z
if ( v1 != null) { // if-eqz v1, :cond_3
/* move v1, v3 */
/* :sswitch_1 */
final String v1 = "mark"; // const-string v1, "mark"
v1 = (( java.lang.String ) v9 ).equals ( v1 ); // invoke-virtual {v9, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z
if ( v1 != null) { // if-eqz v1, :cond_3
/* move v1, v8 */
/* :sswitch_2 */
/* const-string/jumbo v1, "trigger" */
v1 = (( java.lang.String ) v9 ).equals ( v1 ); // invoke-virtual {v9, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z
if ( v1 != null) { // if-eqz v1, :cond_3
/* move v1, v6 */
/* :sswitch_3 */
/* const-string/jumbo v3, "whitelist" */
v3 = (( java.lang.String ) v9 ).equals ( v3 ); // invoke-virtual {v9, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z
if ( v3 != null) { // if-eqz v3, :cond_3
} // :goto_1
int v1 = -1; // const/4 v1, -0x1
} // :goto_2
final String v3 = ": "; // const-string v3, ": "
/* packed-switch v1, :pswitch_data_0 */
/* .line 197 */
int v0 = 0; // const/4 v0, 0x0
/* goto/16 :goto_5 */
/* .line 194 */
/* :pswitch_0 */
v0 = /* invoke-direct {p0, v2, v5}, Lcom/miui/server/turbosched/FrameTurboAction;->testScene(ZLjava/util/List;)Z */
/* .line 195 */
/* goto/16 :goto_5 */
/* .line 191 */
/* :pswitch_1 */
v0 = /* invoke-direct {p0, v2, v5}, Lcom/miui/server/turbosched/FrameTurboAction;->updateMarkScene(ZLjava/util/List;)Z */
/* .line 192 */
/* goto/16 :goto_5 */
/* .line 183 */
/* :pswitch_2 */
/* invoke-direct {p0, v2, v5}, Lcom/miui/server/turbosched/FrameTurboAction;->updateTriggerList(ZLjava/util/List;)V */
/* .line 184 */
int v0 = 1; // const/4 v0, 0x1
/* .line 185 */
final String v1 = "frame turbo list: "; // const-string v1, "frame turbo list: "
(( java.io.PrintWriter ) p2 ).println ( v1 ); // invoke-virtual {p2, v1}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V
/* .line 186 */
v1 = this.mFrameTurboAppMap;
v6 = } // :goto_3
if ( v6 != null) { // if-eqz v6, :cond_4
/* check-cast v6, Ljava/lang/String; */
/* .line 187 */
/* .local v6, "key":Ljava/lang/String; */
/* new-instance v8, Ljava/lang/StringBuilder; */
/* invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V */
(( java.lang.StringBuilder ) v8 ).append ( v6 ); // invoke-virtual {v8, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v8 ).append ( v3 ); // invoke-virtual {v8, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
v10 = this.mFrameTurboAppMap;
(( java.lang.StringBuilder ) v8 ).append ( v10 ); // invoke-virtual {v8, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v8 ).toString ( ); // invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
(( java.io.PrintWriter ) p2 ).println ( v8 ); // invoke-virtual {p2, v8}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V
/* .line 188 */
} // .end local v6 # "key":Ljava/lang/String;
/* .line 189 */
} // :cond_4
/* .line 175 */
/* :pswitch_3 */
/* invoke-direct {p0, v2, v5}, Lcom/miui/server/turbosched/FrameTurboAction;->updateSceneWhiteList(ZLjava/util/List;)V */
/* .line 176 */
int v0 = 1; // const/4 v0, 0x1
/* .line 177 */
final String v1 = "scene white list now is: "; // const-string v1, "scene white list now is: "
(( java.io.PrintWriter ) p2 ).println ( v1 ); // invoke-virtual {p2, v1}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V
/* .line 178 */
v1 = this.mFrameTurboSceneWhiteListMap;
v6 = } // :goto_4
if ( v6 != null) { // if-eqz v6, :cond_5
/* check-cast v6, Ljava/util/Map$Entry; */
/* .line 179 */
/* .local v6, "entry":Ljava/util/Map$Entry;, "Ljava/util/Map$Entry<Ljava/lang/String;Ljava/util/List<Ljava/lang/String;>;>;" */
/* new-instance v8, Ljava/lang/StringBuilder; */
/* invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V */
/* check-cast v10, Ljava/lang/String; */
(( java.lang.StringBuilder ) v8 ).append ( v10 ); // invoke-virtual {v8, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v8 ).append ( v3 ); // invoke-virtual {v8, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
/* check-cast v10, Ljava/util/List; */
(( java.lang.Object ) v10 ).toString ( ); // invoke-virtual {v10}, Ljava/lang/Object;->toString()Ljava/lang/String;
(( java.lang.StringBuilder ) v8 ).append ( v10 ); // invoke-virtual {v8, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v8 ).toString ( ); // invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
(( java.io.PrintWriter ) p2 ).println ( v8 ); // invoke-virtual {p2, v8}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V
/* .line 180 */
} // .end local v6 # "entry":Ljava/util/Map$Entry;, "Ljava/util/Map$Entry<Ljava/lang/String;Ljava/util/List<Ljava/lang/String;>;>;"
/* .line 181 */
} // :cond_5
/* nop */
/* .line 200 */
} // :goto_5
/* nop */
/* :sswitch_data_0 */
/* .sparse-switch */
/* -0x6293bfb9 -> :sswitch_3 */
/* -0x3f2caa48 -> :sswitch_2 */
/* 0x3306cd -> :sswitch_1 */
/* 0x364492 -> :sswitch_0 */
} // .end sparse-switch
/* :pswitch_data_0 */
/* .packed-switch 0x0 */
/* :pswitch_3 */
/* :pswitch_2 */
/* :pswitch_1 */
/* :pswitch_0 */
} // .end packed-switch
} // .end method
private void printCommandResult ( java.io.PrintWriter p0, java.lang.String p1 ) {
/* .locals 1 */
/* .param p1, "pw" # Ljava/io/PrintWriter; */
/* .param p2, "result" # Ljava/lang/String; */
/* .line 301 */
final String v0 = "--------------------command result-----------------------"; // const-string v0, "--------------------command result-----------------------"
(( java.io.PrintWriter ) p1 ).println ( v0 ); // invoke-virtual {p1, v0}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V
/* .line 302 */
(( java.io.PrintWriter ) p1 ).println ( p2 ); // invoke-virtual {p1, p2}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V
/* .line 303 */
return;
} // .end method
private Boolean testScene ( Boolean p0, java.util.List p1 ) {
/* .locals 2 */
/* .param p1, "start" # Z */
/* .annotation system Ldalvik/annotation/Signature; */
/* value = { */
/* "(Z", */
/* "Ljava/util/List<", */
/* "Ljava/lang/String;", */
/* ">;)Z" */
/* } */
} // .end annotation
/* .line 220 */
v0 = /* .local p2, "sceneList":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;" */
int v1 = 1; // const/4 v1, 0x1
/* if-le v0, v1, :cond_0 */
/* .line 221 */
int v0 = 0; // const/4 v0, 0x0
/* .line 224 */
} // :cond_0
/* invoke-direct {p0, p1, p2}, Lcom/miui/server/turbosched/FrameTurboAction;->updateSceneWhiteList(ZLjava/util/List;)V */
/* .line 227 */
/* invoke-direct {p0, p1, p2}, Lcom/miui/server/turbosched/FrameTurboAction;->updateTriggerList(ZLjava/util/List;)V */
/* .line 230 */
/* invoke-direct {p0, p1, p2}, Lcom/miui/server/turbosched/FrameTurboAction;->updateMarkScene(ZLjava/util/List;)Z */
/* .line 231 */
} // .end method
private Boolean updateMarkScene ( Boolean p0, java.util.List p1 ) {
/* .locals 5 */
/* .param p1, "enable" # Z */
/* .annotation system Ldalvik/annotation/Signature; */
/* value = { */
/* "(Z", */
/* "Ljava/util/List<", */
/* "Ljava/lang/String;", */
/* ">;)Z" */
/* } */
} // .end annotation
/* .line 248 */
v0 = /* .local p2, "frameConfigs":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;" */
int v1 = 0; // const/4 v1, 0x0
int v2 = 1; // const/4 v2, 0x1
/* if-le v0, v2, :cond_0 */
/* .line 249 */
/* .line 251 */
} // :cond_0
/* check-cast v0, Ljava/lang/String; */
final String v3 = ":"; // const-string v3, ":"
(( java.lang.String ) v0 ).split ( v3 ); // invoke-virtual {v0, v3}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;
/* .line 252 */
/* .local v0, "appConfig":[Ljava/lang/String; */
/* array-length v3, v0 */
int v4 = 2; // const/4 v4, 0x2
/* if-ge v3, v4, :cond_1 */
/* .line 253 */
/* .line 255 */
} // :cond_1
/* aget-object v3, v0, v1 */
/* aget-object v4, v0, v2 */
v3 = (( com.miui.server.turbosched.FrameTurboAction ) p0 ).markScene ( v3, v4, p1 ); // invoke-virtual {p0, v3, v4, p1}, Lcom/miui/server/turbosched/FrameTurboAction;->markScene(Ljava/lang/String;Ljava/lang/String;Z)I
/* .line 256 */
/* .local v3, "code":I */
/* if-nez v3, :cond_2 */
/* move v1, v2 */
} // :cond_2
} // .end method
private void updateSceneWhiteList ( Boolean p0, java.util.List p1 ) {
/* .locals 10 */
/* .param p1, "add" # Z */
/* .annotation system Ldalvik/annotation/Signature; */
/* value = { */
/* "(Z", */
/* "Ljava/util/List<", */
/* "Ljava/lang/String;", */
/* ">;)V" */
/* } */
} // .end annotation
/* .line 260 */
/* .local p2, "sceneList":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;" */
v1 = } // :goto_0
final String v2 = ":"; // const-string v2, ":"
int v3 = 0; // const/4 v3, 0x0
if ( v1 != null) { // if-eqz v1, :cond_6
/* check-cast v1, Ljava/lang/String; */
/* .line 261 */
/* .local v1, "scene":Ljava/lang/String; */
(( java.lang.String ) v1 ).split ( v2 ); // invoke-virtual {v1, v2}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;
/* .line 262 */
/* .local v2, "sceneConfig":[Ljava/lang/String; */
/* array-length v4, v2 */
int v5 = 1; // const/4 v5, 0x1
/* if-le v4, v5, :cond_5 */
/* .line 263 */
/* aget-object v4, v2, v5 */
final String v5 = "\\|"; // const-string v5, "\\|"
(( java.lang.String ) v4 ).split ( v5 ); // invoke-virtual {v4, v5}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;
/* .line 264 */
/* .local v4, "scenes":[Ljava/lang/String; */
v5 = this.mFrameTurboSceneWhiteListMap;
/* aget-object v6, v2, v3 */
/* check-cast v5, Ljava/util/List; */
/* .line 265 */
/* .local v5, "sceneWhiteList":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;" */
/* array-length v6, v4 */
/* move v7, v3 */
} // :goto_1
/* if-ge v7, v6, :cond_3 */
/* aget-object v8, v4, v7 */
/* .line 266 */
/* .local v8, "s":Ljava/lang/String; */
if ( p1 != null) { // if-eqz p1, :cond_1
/* .line 267 */
/* if-nez v5, :cond_0 */
/* .line 268 */
/* new-instance v9, Ljava/util/ArrayList; */
/* invoke-direct {v9}, Ljava/util/ArrayList;-><init>()V */
/* move-object v5, v9 */
/* .line 270 */
v9 = } // :cond_0
/* if-nez v9, :cond_2 */
/* .line 271 */
/* .line 274 */
} // :cond_1
v9 = if ( v5 != null) { // if-eqz v5, :cond_2
if ( v9 != null) { // if-eqz v9, :cond_2
/* .line 275 */
/* .line 265 */
} // .end local v8 # "s":Ljava/lang/String;
} // :cond_2
} // :goto_2
/* add-int/lit8 v7, v7, 0x1 */
/* .line 279 */
} // :cond_3
v6 = if ( v5 != null) { // if-eqz v5, :cond_4
/* if-nez v6, :cond_4 */
/* .line 280 */
v6 = this.mFrameTurboSceneWhiteListMap;
/* aget-object v3, v2, v3 */
/* .line 282 */
} // :cond_4
v6 = this.mFrameTurboSceneWhiteListMap;
/* aget-object v3, v2, v3 */
/* .line 285 */
} // .end local v1 # "scene":Ljava/lang/String;
} // .end local v2 # "sceneConfig":[Ljava/lang/String;
} // .end local v4 # "scenes":[Ljava/lang/String;
} // .end local v5 # "sceneWhiteList":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;"
} // :cond_5
} // :goto_3
/* .line 286 */
} // :cond_6
final String v0 = ""; // const-string v0, ""
/* .line 287 */
/* .local v0, "whiteListStr":Ljava/lang/String; */
v1 = this.mFrameTurboSceneWhiteListMap;
} // :cond_7
v4 = } // :goto_4
if ( v4 != null) { // if-eqz v4, :cond_9
/* check-cast v4, Ljava/lang/String; */
/* .line 288 */
/* .local v4, "key":Ljava/lang/String; */
v5 = this.mFrameTurboSceneWhiteListMap;
/* check-cast v5, Ljava/util/List; */
/* .line 289 */
/* .restart local v5 # "sceneWhiteList":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;" */
v6 = if ( v5 != null) { // if-eqz v5, :cond_7
if ( v6 != null) { // if-eqz v6, :cond_8
/* .line 290 */
/* .line 292 */
} // :cond_8
/* new-instance v6, Ljava/lang/StringBuilder; */
/* invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V */
(( java.lang.StringBuilder ) v6 ).append ( v0 ); // invoke-virtual {v6, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v6 ).append ( v4 ); // invoke-virtual {v6, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v6 ).append ( v2 ); // invoke-virtual {v6, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
/* const-string/jumbo v7, "|" */
android.text.TextUtils .join ( v7,v5 );
(( java.lang.StringBuilder ) v6 ).append ( v7 ); // invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
final String v7 = ", "; // const-string v7, ", "
(( java.lang.StringBuilder ) v6 ).append ( v7 ); // invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v6 ).toString ( ); // invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
/* .line 293 */
} // .end local v4 # "key":Ljava/lang/String;
} // .end local v5 # "sceneWhiteList":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;"
/* .line 294 */
} // :cond_9
v1 = (( java.lang.String ) v0 ).length ( ); // invoke-virtual {v0}, Ljava/lang/String;->length()I
/* if-lez v1, :cond_a */
/* .line 295 */
v1 = (( java.lang.String ) v0 ).length ( ); // invoke-virtual {v0}, Ljava/lang/String;->length()I
/* add-int/lit8 v1, v1, -0x2 */
(( java.lang.String ) v0 ).substring ( v3, v1 ); // invoke-virtual {v0, v3, v1}, Ljava/lang/String;->substring(II)Ljava/lang/String;
/* .line 297 */
} // :cond_a
final String v1 = "persist.sys.turbosched.frame_turbo.scene_white_list"; // const-string v1, "persist.sys.turbosched.frame_turbo.scene_white_list"
android.os.SystemProperties .set ( v1,v0 );
/* .line 298 */
return;
} // .end method
private void updateTriggerList ( Boolean p0, java.util.List p1 ) {
/* .locals 5 */
/* .param p1, "add" # Z */
/* .annotation system Ldalvik/annotation/Signature; */
/* value = { */
/* "(Z", */
/* "Ljava/util/List<", */
/* "Ljava/lang/String;", */
/* ">;)V" */
/* } */
} // .end annotation
/* .line 235 */
/* .local p2, "frameCongfigs":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;" */
} // :cond_0
v1 = } // :goto_0
if ( v1 != null) { // if-eqz v1, :cond_2
/* check-cast v1, Ljava/lang/String; */
/* .line 236 */
/* .local v1, "config":Ljava/lang/String; */
final String v2 = ":"; // const-string v2, ":"
if ( v1 != null) { // if-eqz v1, :cond_1
v3 = (( java.lang.String ) v1 ).contains ( v2 ); // invoke-virtual {v1, v2}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z
/* if-nez v3, :cond_1 */
/* .line 237 */
/* iget-boolean v2, p0, Lcom/miui/server/turbosched/FrameTurboAction;->mDebug:Z */
if ( v2 != null) { // if-eqz v2, :cond_0
/* .line 238 */
/* new-instance v2, Ljava/lang/StringBuilder; */
/* invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V */
final String v3 = "invalid config: "; // const-string v3, "invalid config: "
(( java.lang.StringBuilder ) v2 ).append ( v3 ); // invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v2 ).append ( v1 ); // invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v2 ).toString ( ); // invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
final String v3 = "TurboSched_FrameTurbo"; // const-string v3, "TurboSched_FrameTurbo"
android.util.Slog .d ( v3,v2 );
/* .line 242 */
} // :cond_1
(( java.lang.String ) v1 ).split ( v2 ); // invoke-virtual {v1, v2}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;
/* .line 243 */
/* .local v2, "appConfig":[Ljava/lang/String; */
int v3 = 0; // const/4 v3, 0x0
/* aget-object v3, v2, v3 */
int v4 = 1; // const/4 v4, 0x1
/* aget-object v4, v2, v4 */
(( com.miui.server.turbosched.FrameTurboAction ) p0 ).enableFrameTurboInternal ( p1, v3, v4 ); // invoke-virtual {p0, p1, v3, v4}, Lcom/miui/server/turbosched/FrameTurboAction;->enableFrameTurboInternal(ZLjava/lang/String;Ljava/lang/String;)V
/* .line 244 */
} // .end local v1 # "config":Ljava/lang/String;
} // .end local v2 # "appConfig":[Ljava/lang/String;
/* .line 245 */
} // :cond_2
return;
} // .end method
/* # virtual methods */
protected void enableFrameTurboInternal ( Boolean p0, java.lang.String p1, java.lang.String p2 ) {
/* .locals 9 */
/* .param p1, "enable" # Z */
/* .param p2, "processThread" # Ljava/lang/String; */
/* .param p3, "sceneListStr" # Ljava/lang/String; */
/* .line 384 */
final String v0 = ","; // const-string v0, ","
if ( p1 != null) { // if-eqz p1, :cond_5
/* .line 385 */
v1 = v1 = this.mFrameTurboAppMap;
if ( v1 != null) { // if-eqz v1, :cond_2
/* .line 386 */
v1 = this.mFrameTurboAppMap;
/* check-cast v1, Ljava/util/List; */
/* .line 387 */
/* .local v1, "sceneList":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;" */
if ( p3 != null) { // if-eqz p3, :cond_1
/* .line 388 */
(( java.lang.String ) p3 ).split ( v0 ); // invoke-virtual {p3, v0}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;
/* .line 389 */
/* .local v2, "sceneArray":[Ljava/lang/String; */
int v3 = 0; // const/4 v3, 0x0
/* .local v3, "i":I */
} // :goto_0
/* array-length v4, v2 */
/* if-ge v3, v4, :cond_1 */
/* .line 390 */
v4 = /* aget-object v4, v2, v3 */
/* if-nez v4, :cond_0 */
/* .line 391 */
/* aget-object v4, v2, v3 */
/* .line 389 */
} // :cond_0
/* add-int/lit8 v3, v3, 0x1 */
/* .line 395 */
} // .end local v1 # "sceneList":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;"
} // .end local v2 # "sceneArray":[Ljava/lang/String;
} // .end local v3 # "i":I
} // :cond_1
/* .line 396 */
} // :cond_2
/* new-instance v1, Ljava/util/ArrayList; */
/* invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V */
/* .line 397 */
/* .restart local v1 # "sceneList":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;" */
if ( p3 != null) { // if-eqz p3, :cond_4
/* .line 398 */
(( java.lang.String ) p3 ).split ( v0 ); // invoke-virtual {p3, v0}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;
/* .line 399 */
/* .restart local v2 # "sceneArray":[Ljava/lang/String; */
int v3 = 0; // const/4 v3, 0x0
/* .restart local v3 # "i":I */
} // :goto_1
/* array-length v4, v2 */
/* if-ge v3, v4, :cond_3 */
/* .line 400 */
/* aget-object v4, v2, v3 */
/* .line 399 */
/* add-int/lit8 v3, v3, 0x1 */
/* .line 402 */
} // .end local v3 # "i":I
} // :cond_3
v3 = this.mFrameTurboAppMap;
/* .line 404 */
} // .end local v1 # "sceneList":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;"
} // .end local v2 # "sceneArray":[Ljava/lang/String;
} // :cond_4
/* .line 406 */
} // :cond_5
v1 = v1 = this.mFrameTurboAppMap;
if ( v1 != null) { // if-eqz v1, :cond_8
/* .line 407 */
v1 = this.mFrameTurboAppMap;
/* check-cast v1, Ljava/util/List; */
/* .line 408 */
/* .restart local v1 # "sceneList":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;" */
if ( p3 != null) { // if-eqz p3, :cond_8
/* .line 409 */
(( java.lang.String ) p3 ).split ( v0 ); // invoke-virtual {p3, v0}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;
/* .line 410 */
/* .restart local v2 # "sceneArray":[Ljava/lang/String; */
int v3 = 0; // const/4 v3, 0x0
/* .restart local v3 # "i":I */
} // :goto_2
/* array-length v4, v2 */
/* if-ge v3, v4, :cond_8 */
/* .line 411 */
v4 = /* aget-object v4, v2, v3 */
if ( v4 != null) { // if-eqz v4, :cond_6
/* .line 412 */
/* aget-object v4, v2, v3 */
/* .line 414 */
v4 = } // :cond_6
/* if-nez v4, :cond_7 */
/* .line 415 */
v4 = this.mFrameTurboAppMap;
/* .line 410 */
} // :cond_7
/* add-int/lit8 v3, v3, 0x1 */
/* .line 421 */
} // .end local v1 # "sceneList":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;"
} // .end local v2 # "sceneArray":[Ljava/lang/String;
} // .end local v3 # "i":I
} // :cond_8
} // :goto_3
final String v1 = ""; // const-string v1, ""
/* .line 422 */
/* .local v1, "frameTurboApps":Ljava/lang/String; */
v2 = this.mFrameTurboAppMap;
v3 = } // :goto_4
if ( v3 != null) { // if-eqz v3, :cond_9
/* check-cast v3, Ljava/util/Map$Entry; */
/* .line 423 */
/* .local v3, "entry":Ljava/util/Map$Entry;, "Ljava/util/Map$Entry<Ljava/lang/String;Ljava/util/List<Ljava/lang/String;>;>;" */
/* check-cast v4, Ljava/lang/String; */
/* .line 424 */
/* .local v4, "key":Ljava/lang/String; */
/* check-cast v5, Ljava/util/List; */
/* .line 425 */
/* .local v5, "value":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;" */
/* const-string/jumbo v6, "|" */
android.text.TextUtils .join ( v6,v5 );
/* .line 426 */
/* .local v6, "sceneList":Ljava/lang/String; */
/* new-instance v7, Ljava/lang/StringBuilder; */
/* invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V */
(( java.lang.StringBuilder ) v7 ).append ( v1 ); // invoke-virtual {v7, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v7 ).append ( v4 ); // invoke-virtual {v7, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
final String v8 = ":"; // const-string v8, ":"
(( java.lang.StringBuilder ) v7 ).append ( v8 ); // invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v7 ).append ( v6 ); // invoke-virtual {v7, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v7 ).append ( v0 ); // invoke-virtual {v7, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v7 ).toString ( ); // invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
/* .line 427 */
} // .end local v3 # "entry":Ljava/util/Map$Entry;, "Ljava/util/Map$Entry<Ljava/lang/String;Ljava/util/List<Ljava/lang/String;>;>;"
} // .end local v4 # "key":Ljava/lang/String;
} // .end local v5 # "value":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;"
} // .end local v6 # "sceneList":Ljava/lang/String;
/* .line 428 */
} // :cond_9
v0 = (( java.lang.String ) v1 ).length ( ); // invoke-virtual {v1}, Ljava/lang/String;->length()I
/* if-lez v0, :cond_a */
/* .line 429 */
v0 = (( java.lang.String ) v1 ).length ( ); // invoke-virtual {v1}, Ljava/lang/String;->length()I
/* add-int/lit8 v0, v0, -0x1 */
int v2 = 0; // const/4 v2, 0x0
(( java.lang.String ) v1 ).substring ( v2, v0 ); // invoke-virtual {v1, v2, v0}, Ljava/lang/String;->substring(II)Ljava/lang/String;
/* .line 431 */
} // :cond_a
final String v0 = "persist.sys.turbosched.frame_turbo.apps"; // const-string v0, "persist.sys.turbosched.frame_turbo.apps"
android.os.SystemProperties .set ( v0,v1 );
/* .line 432 */
return;
} // .end method
protected Integer markScene ( java.lang.String p0, java.lang.String p1, Boolean p2 ) {
/* .locals 4 */
/* .param p1, "packageName" # Ljava/lang/String; */
/* .param p2, "sceneId" # Ljava/lang/String; */
/* .param p3, "start" # Z */
/* .line 485 */
if ( p3 != null) { // if-eqz p3, :cond_0
/* .line 486 */
v0 = /* invoke-direct {p0, p1, p2}, Lcom/miui/server/turbosched/FrameTurboAction;->checkPermission(Ljava/lang/String;Ljava/lang/String;)I */
/* .line 487 */
/* .local v0, "code":I */
if ( v0 != null) { // if-eqz v0, :cond_0
/* .line 488 */
/* .line 493 */
} // .end local v0 # "code":I
} // :cond_0
int v0 = 0; // const/4 v0, 0x0
if ( p3 != null) { // if-eqz p3, :cond_4
/* .line 494 */
final String v1 = ""; // const-string v1, ""
v2 = this.mCurrentTurboScene;
v1 = (( java.lang.String ) v1 ).equals ( v2 ); // invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z
/* if-nez v1, :cond_3 */
/* .line 495 */
v0 = this.mActiveSceneWaitListMap;
/* check-cast v0, Ljava/util/LinkedList; */
/* .line 496 */
/* .local v0, "wList":Ljava/util/LinkedList;, "Ljava/util/LinkedList<Ljava/lang/String;>;" */
/* if-nez v0, :cond_1 */
/* .line 497 */
/* new-instance v1, Ljava/util/LinkedList; */
/* invoke-direct {v1}, Ljava/util/LinkedList;-><init>()V */
/* move-object v0, v1 */
/* .line 499 */
} // :cond_1
v1 = (( java.util.LinkedList ) v0 ).contains ( p2 ); // invoke-virtual {v0, p2}, Ljava/util/LinkedList;->contains(Ljava/lang/Object;)Z
/* if-nez v1, :cond_2 */
/* .line 500 */
(( java.util.LinkedList ) v0 ).add ( p2 ); // invoke-virtual {v0, p2}, Ljava/util/LinkedList;->add(Ljava/lang/Object;)Z
/* .line 502 */
} // :cond_2
v1 = this.mActiveSceneWaitListMap;
/* .line 503 */
int v1 = -5; // const/4 v1, -0x5
/* .line 505 */
} // .end local v0 # "wList":Ljava/util/LinkedList;, "Ljava/util/LinkedList<Ljava/lang/String;>;"
} // :cond_3
(( com.miui.server.turbosched.FrameTurboAction ) p0 ).notifySceneChanged ( p1, p2, p3 ); // invoke-virtual {p0, p1, p2, p3}, Lcom/miui/server/turbosched/FrameTurboAction;->notifySceneChanged(Ljava/lang/String;Ljava/lang/String;Z)V
/* .line 506 */
/* .line 511 */
} // :cond_4
/* if-nez p3, :cond_8 */
/* .line 512 */
(( com.miui.server.turbosched.FrameTurboAction ) p0 ).notifySceneChanged ( p1, p2, p3 ); // invoke-virtual {p0, p1, p2, p3}, Lcom/miui/server/turbosched/FrameTurboAction;->notifySceneChanged(Ljava/lang/String;Ljava/lang/String;Z)V
/* .line 513 */
v1 = this.mActiveSceneWaitListMap;
/* check-cast v1, Ljava/util/LinkedList; */
/* .line 514 */
/* .local v1, "wList":Ljava/util/LinkedList;, "Ljava/util/LinkedList<Ljava/lang/String;>;" */
if ( v1 != null) { // if-eqz v1, :cond_8
/* .line 515 */
v2 = (( java.util.LinkedList ) v1 ).contains ( p2 ); // invoke-virtual {v1, p2}, Ljava/util/LinkedList;->contains(Ljava/lang/Object;)Z
if ( v2 != null) { // if-eqz v2, :cond_5
/* .line 516 */
(( java.util.LinkedList ) v1 ).remove ( p2 ); // invoke-virtual {v1, p2}, Ljava/util/LinkedList;->remove(Ljava/lang/Object;)Z
/* .line 518 */
} // :cond_5
v2 = (( java.util.LinkedList ) v1 ).size ( ); // invoke-virtual {v1}, Ljava/util/LinkedList;->size()I
/* if-lez v2, :cond_6 */
/* .line 519 */
(( java.util.LinkedList ) v1 ).getFirst ( ); // invoke-virtual {v1}, Ljava/util/LinkedList;->getFirst()Ljava/lang/Object;
/* check-cast v2, Ljava/lang/String; */
/* .line 520 */
/* .local v2, "nextScene":Ljava/lang/String; */
int v3 = 1; // const/4 v3, 0x1
(( com.miui.server.turbosched.FrameTurboAction ) p0 ).notifySceneChanged ( p1, v2, v3 ); // invoke-virtual {p0, p1, v2, v3}, Lcom/miui/server/turbosched/FrameTurboAction;->notifySceneChanged(Ljava/lang/String;Ljava/lang/String;Z)V
/* .line 521 */
(( java.util.LinkedList ) v1 ).removeFirst ( ); // invoke-virtual {v1}, Ljava/util/LinkedList;->removeFirst()Ljava/lang/Object;
/* .line 524 */
} // .end local v2 # "nextScene":Ljava/lang/String;
} // :cond_6
v2 = (( java.util.LinkedList ) v1 ).size ( ); // invoke-virtual {v1}, Ljava/util/LinkedList;->size()I
/* if-nez v2, :cond_7 */
/* .line 525 */
v2 = this.mActiveSceneWaitListMap;
/* .line 527 */
} // :cond_7
v2 = this.mActiveSceneWaitListMap;
/* .line 531 */
} // .end local v1 # "wList":Ljava/util/LinkedList;, "Ljava/util/LinkedList<Ljava/lang/String;>;"
} // :cond_8
} // :goto_0
} // .end method
protected void notifySceneChanged ( java.lang.String p0, java.lang.String p1, Boolean p2 ) {
/* .locals 6 */
/* .param p1, "packageName" # Ljava/lang/String; */
/* .param p2, "sceneId" # Ljava/lang/String; */
/* .param p3, "start" # Z */
/* .line 457 */
/* iget-boolean v0, p0, Lcom/miui/server/turbosched/FrameTurboAction;->mDebug:Z */
if ( v0 != null) { // if-eqz v0, :cond_0
/* .line 458 */
final String v0 = "TurboSched_FrameTurbo"; // const-string v0, "TurboSched_FrameTurbo"
/* new-instance v1, Ljava/lang/StringBuilder; */
/* invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V */
final String v2 = "notifySceneChanged, packageName: "; // const-string v2, "notifySceneChanged, packageName: "
(( java.lang.StringBuilder ) v1 ).append ( v2 ); // invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v1 ).append ( p1 ); // invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
final String v2 = ",sceneId: "; // const-string v2, ",sceneId: "
(( java.lang.StringBuilder ) v1 ).append ( v2 ); // invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v1 ).append ( p2 ); // invoke-virtual {v1, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
final String v2 = ",start: "; // const-string v2, ",start: "
(( java.lang.StringBuilder ) v1 ).append ( v2 ); // invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v1 ).append ( p3 ); // invoke-virtual {v1, p3}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;
final String v2 = ",mFrameTurboSceneCallbacks: "; // const-string v2, ",mFrameTurboSceneCallbacks: "
(( java.lang.StringBuilder ) v1 ).append ( v2 ); // invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
v2 = v2 = this.mFrameTurboSceneCallbacks;
(( java.lang.StringBuilder ) v1 ).append ( v2 ); // invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v1 ).toString ( ); // invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
android.util.Slog .d ( v0,v1 );
/* .line 460 */
} // :cond_0
if ( p3 != null) { // if-eqz p3, :cond_1
/* .line 461 */
/* new-instance v0, Ljava/lang/StringBuilder; */
/* invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V */
(( java.lang.StringBuilder ) v0 ).append ( p1 ); // invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
final String v1 = ":"; // const-string v1, ":"
(( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v0 ).append ( p2 ); // invoke-virtual {v0, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v0 ).toString ( ); // invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
this.mCurrentTurboScene = v0;
/* .line 463 */
} // :cond_1
final String v0 = ""; // const-string v0, ""
this.mCurrentTurboScene = v0;
/* .line 465 */
} // :goto_0
final String v0 = "persist.sys.turbosched.frame_turbo.current_turbo_scene"; // const-string v0, "persist.sys.turbosched.frame_turbo.current_turbo_scene"
v1 = this.mCurrentTurboScene;
android.os.SystemProperties .set ( v0,v1 );
/* .line 468 */
v0 = this.mFrameTurboSceneCallbacks;
/* monitor-enter v0 */
/* .line 469 */
try { // :try_start_0
v1 = v1 = this.mFrameTurboSceneCallbacks;
/* if-nez v1, :cond_2 */
/* .line 470 */
/* monitor-exit v0 */
return;
/* .line 472 */
} // :cond_2
v1 = this.mFrameTurboSceneCallbacks;
int v2 = 0; // const/4 v2, 0x0
/* new-array v2, v2, [Lmiui/turbosched/ITurboSchedManager$IFrameTurboSceneCallback; */
/* check-cast v1, [Lmiui/turbosched/ITurboSchedManager$IFrameTurboSceneCallback; */
/* .line 473 */
/* .local v1, "copyCallbacks":[Lmiui/turbosched/ITurboSchedManager$IFrameTurboSceneCallback; */
/* monitor-exit v0 */
/* :try_end_0 */
/* .catchall {:try_start_0 ..:try_end_0} :catchall_0 */
/* .line 474 */
int v0 = 0; // const/4 v0, 0x0
/* .local v0, "i":I */
} // :goto_1
/* array-length v2, v1 */
/* if-ge v0, v2, :cond_3 */
/* .line 476 */
try { // :try_start_1
/* aget-object v2, v1, v0 */
/* :try_end_1 */
/* .catch Landroid/os/RemoteException; {:try_start_1 ..:try_end_1} :catch_0 */
/* .line 479 */
/* .line 477 */
/* :catch_0 */
/* move-exception v2 */
/* .line 478 */
/* .local v2, "e":Landroid/os/RemoteException; */
final String v3 = "TurboSched_FrameTurbo"; // const-string v3, "TurboSched_FrameTurbo"
/* new-instance v4, Ljava/lang/StringBuilder; */
/* invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V */
final String v5 = "notifySceneChanged, RemoteException: "; // const-string v5, "notifySceneChanged, RemoteException: "
(( java.lang.StringBuilder ) v4 ).append ( v5 ); // invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( android.os.RemoteException ) v2 ).getMessage ( ); // invoke-virtual {v2}, Landroid/os/RemoteException;->getMessage()Ljava/lang/String;
(( java.lang.StringBuilder ) v4 ).append ( v5 ); // invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v4 ).toString ( ); // invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
android.util.Slog .e ( v3,v4 );
/* .line 474 */
} // .end local v2 # "e":Landroid/os/RemoteException;
} // :goto_2
/* add-int/lit8 v0, v0, 0x1 */
/* .line 481 */
} // .end local v0 # "i":I
} // :cond_3
return;
/* .line 473 */
} // .end local v1 # "copyCallbacks":[Lmiui/turbosched/ITurboSchedManager$IFrameTurboSceneCallback;
/* :catchall_0 */
/* move-exception v1 */
try { // :try_start_2
/* monitor-exit v0 */
/* :try_end_2 */
/* .catchall {:try_start_2 ..:try_end_2} :catchall_0 */
/* throw v1 */
} // .end method
protected void onForegroundActivitiesChanged ( java.lang.String p0, Boolean p1 ) {
/* .locals 5 */
/* .param p1, "processName" # Ljava/lang/String; */
/* .param p2, "foregroundActivities" # Z */
/* .line 586 */
int v0 = 1; // const/4 v0, 0x1
/* if-nez p2, :cond_4 */
/* .line 587 */
v1 = this.mCurrentTurboScene;
final String v2 = ""; // const-string v2, ""
v1 = (( java.lang.String ) v2 ).equals ( v1 ); // invoke-virtual {v2, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z
if ( v1 != null) { // if-eqz v1, :cond_0
/* .line 588 */
return;
/* .line 590 */
} // :cond_0
v1 = this.mCurrentTurboScene;
final String v3 = ":"; // const-string v3, ":"
(( java.lang.String ) v1 ).split ( v3 ); // invoke-virtual {v1, v3}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;
/* .line 591 */
/* .local v1, "sceneInfo":[Ljava/lang/String; */
/* array-length v3, v1 */
int v4 = 2; // const/4 v4, 0x2
/* if-eq v3, v4, :cond_1 */
/* .line 592 */
return;
/* .line 594 */
} // :cond_1
int v3 = 0; // const/4 v3, 0x0
/* aget-object v4, v1, v3 */
v4 = (( java.lang.String ) v4 ).equals ( p1 ); // invoke-virtual {v4, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z
/* if-nez v4, :cond_2 */
/* .line 595 */
return;
/* .line 597 */
} // :cond_2
/* aget-object v0, v1, v0 */
/* .line 598 */
/* .local v0, "sceneId":Ljava/lang/String; */
(( com.miui.server.turbosched.FrameTurboAction ) p0 ).notifySceneChanged ( p1, v0, v3 ); // invoke-virtual {p0, p1, v0, v3}, Lcom/miui/server/turbosched/FrameTurboAction;->notifySceneChanged(Ljava/lang/String;Ljava/lang/String;Z)V
/* .line 600 */
v3 = this.mActiveSceneWaitListMap;
/* check-cast v3, Ljava/util/LinkedList; */
/* .line 601 */
/* .local v3, "wList":Ljava/util/LinkedList;, "Ljava/util/LinkedList<Ljava/lang/String;>;" */
/* if-nez v3, :cond_3 */
/* .line 602 */
/* new-instance v4, Ljava/util/LinkedList; */
/* invoke-direct {v4}, Ljava/util/LinkedList;-><init>()V */
/* move-object v3, v4 */
/* .line 604 */
} // :cond_3
(( java.util.LinkedList ) v3 ).addFirst ( v0 ); // invoke-virtual {v3, v0}, Ljava/util/LinkedList;->addFirst(Ljava/lang/Object;)V
/* .line 605 */
v4 = this.mActiveSceneWaitListMap;
/* .line 606 */
this.mCurrentTurboScene = v2;
/* .line 607 */
} // .end local v0 # "sceneId":Ljava/lang/String;
} // .end local v1 # "sceneInfo":[Ljava/lang/String;
} // .end local v3 # "wList":Ljava/util/LinkedList;, "Ljava/util/LinkedList<Ljava/lang/String;>;"
/* .line 609 */
} // :cond_4
v1 = this.mActiveSceneWaitListMap;
/* check-cast v1, Ljava/util/LinkedList; */
/* .line 610 */
/* .local v1, "wList":Ljava/util/LinkedList;, "Ljava/util/LinkedList<Ljava/lang/String;>;" */
if ( v1 != null) { // if-eqz v1, :cond_7
/* .line 611 */
v2 = (( java.util.LinkedList ) v1 ).size ( ); // invoke-virtual {v1}, Ljava/util/LinkedList;->size()I
/* if-lez v2, :cond_5 */
/* .line 612 */
(( java.util.LinkedList ) v1 ).getFirst ( ); // invoke-virtual {v1}, Ljava/util/LinkedList;->getFirst()Ljava/lang/Object;
/* check-cast v2, Ljava/lang/String; */
/* .line 613 */
/* .local v2, "nextScene":Ljava/lang/String; */
(( com.miui.server.turbosched.FrameTurboAction ) p0 ).notifySceneChanged ( p1, v2, v0 ); // invoke-virtual {p0, p1, v2, v0}, Lcom/miui/server/turbosched/FrameTurboAction;->notifySceneChanged(Ljava/lang/String;Ljava/lang/String;Z)V
/* .line 614 */
(( java.util.LinkedList ) v1 ).removeFirst ( ); // invoke-virtual {v1}, Ljava/util/LinkedList;->removeFirst()Ljava/lang/Object;
/* .line 617 */
} // .end local v2 # "nextScene":Ljava/lang/String;
} // :cond_5
v0 = (( java.util.LinkedList ) v1 ).size ( ); // invoke-virtual {v1}, Ljava/util/LinkedList;->size()I
/* if-nez v0, :cond_6 */
/* .line 618 */
v0 = this.mActiveSceneWaitListMap;
/* .line 620 */
} // :cond_6
v0 = this.mActiveSceneWaitListMap;
/* .line 624 */
} // .end local v1 # "wList":Ljava/util/LinkedList;, "Ljava/util/LinkedList<Ljava/lang/String;>;"
} // :cond_7
} // :goto_0
return;
} // .end method
protected Boolean parseFrameTurboCommand ( java.io.FileDescriptor p0, java.io.PrintWriter p1, java.lang.String[] p2 ) {
/* .locals 11 */
/* .param p1, "fd" # Ljava/io/FileDescriptor; */
/* .param p2, "pw" # Ljava/io/PrintWriter; */
/* .param p3, "args" # [Ljava/lang/String; */
/* .line 328 */
/* array-length v0, p3 */
int v1 = 2; // const/4 v1, 0x2
int v2 = 1; // const/4 v2, 0x1
/* if-lt v0, v1, :cond_1 */
/* .line 329 */
v0 = /* invoke-direct {p0, p1, p2, p3}, Lcom/miui/server/turbosched/FrameTurboAction;->parseCommandArgs(Ljava/io/FileDescriptor;Ljava/io/PrintWriter;[Ljava/lang/String;)Z */
if ( v0 != null) { // if-eqz v0, :cond_0
/* .line 330 */
/* .line 332 */
} // :cond_0
/* const-string/jumbo v0, "wrong command" */
(( java.io.PrintWriter ) p2 ).println ( v0 ); // invoke-virtual {p2, v0}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V
/* .line 333 */
/* .line 336 */
} // :cond_1
final String v0 = "--------------------config-------------------------------"; // const-string v0, "--------------------config-------------------------------"
(( java.io.PrintWriter ) p2 ).println ( v0 ); // invoke-virtual {p2, v0}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V
/* .line 337 */
com.miui.server.turbosched.TurboSchedConfig .getPolicyList ( );
v0 = final String v1 = "frame_turbo"; // const-string v1, "frame_turbo"
/* .line 338 */
/* .local v0, "isEnable":Z */
/* new-instance v1, Ljava/lang/StringBuilder; */
/* invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V */
final String v3 = "enable: "; // const-string v3, "enable: "
(( java.lang.StringBuilder ) v1 ).append ( v3 ); // invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v1 ).append ( v0 ); // invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v1 ).toString ( ); // invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
(( java.io.PrintWriter ) p2 ).println ( v1 ); // invoke-virtual {p2, v1}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V
/* .line 339 */
/* new-instance v1, Ljava/lang/StringBuilder; */
/* invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V */
final String v3 = "debug: "; // const-string v3, "debug: "
(( java.lang.StringBuilder ) v1 ).append ( v3 ); // invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
/* iget-boolean v3, p0, Lcom/miui/server/turbosched/FrameTurboAction;->mDebug:Z */
(( java.lang.StringBuilder ) v1 ).append ( v3 ); // invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v1 ).toString ( ); // invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
(( java.io.PrintWriter ) p2 ).println ( v1 ); // invoke-virtual {p2, v1}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V
/* .line 340 */
/* new-instance v1, Ljava/lang/StringBuilder; */
/* invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V */
final String v3 = "current scene: "; // const-string v3, "current scene: "
(( java.lang.StringBuilder ) v1 ).append ( v3 ); // invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
v3 = this.mCurrentTurboScene;
(( java.lang.StringBuilder ) v1 ).append ( v3 ); // invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v1 ).toString ( ); // invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
(( java.io.PrintWriter ) p2 ).println ( v1 ); // invoke-virtual {p2, v1}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V
/* .line 341 */
v1 = this.mFrameTurboAppMap;
int v3 = 0; // const/4 v3, 0x0
final String v4 = ":"; // const-string v4, ":"
/* const-string/jumbo v5, "|" */
v1 = if ( v1 != null) { // if-eqz v1, :cond_4
/* if-lez v1, :cond_4 */
/* .line 342 */
final String v1 = "-----------------turbo trigger list------------------------"; // const-string v1, "-----------------turbo trigger list------------------------"
(( java.io.PrintWriter ) p2 ).println ( v1 ); // invoke-virtual {p2, v1}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V
/* .line 343 */
v1 = this.mFrameTurboAppMap;
v6 = } // :goto_0
if ( v6 != null) { // if-eqz v6, :cond_4
/* check-cast v6, Ljava/util/Map$Entry; */
/* .line 344 */
/* .local v6, "entry":Ljava/util/Map$Entry;, "Ljava/util/Map$Entry<Ljava/lang/String;Ljava/util/List<Ljava/lang/String;>;>;" */
final String v7 = ""; // const-string v7, ""
/* .line 345 */
/* .local v7, "valueStr":Ljava/lang/String; */
/* check-cast v8, Ljava/util/List; */
v9 = } // :goto_1
if ( v9 != null) { // if-eqz v9, :cond_2
/* check-cast v9, Ljava/lang/String; */
/* .line 346 */
/* .local v9, "value":Ljava/lang/String; */
/* new-instance v10, Ljava/lang/StringBuilder; */
/* invoke-direct {v10}, Ljava/lang/StringBuilder;-><init>()V */
(( java.lang.StringBuilder ) v10 ).append ( v7 ); // invoke-virtual {v10, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v10 ).append ( v9 ); // invoke-virtual {v10, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v10 ).append ( v5 ); // invoke-virtual {v10, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v10 ).toString ( ); // invoke-virtual {v10}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
/* .line 347 */
} // .end local v9 # "value":Ljava/lang/String;
/* .line 348 */
} // :cond_2
v8 = (( java.lang.String ) v7 ).endsWith ( v5 ); // invoke-virtual {v7, v5}, Ljava/lang/String;->endsWith(Ljava/lang/String;)Z
if ( v8 != null) { // if-eqz v8, :cond_3
/* .line 349 */
v8 = (( java.lang.String ) v7 ).length ( ); // invoke-virtual {v7}, Ljava/lang/String;->length()I
/* sub-int/2addr v8, v2 */
(( java.lang.String ) v7 ).substring ( v3, v8 ); // invoke-virtual {v7, v3, v8}, Ljava/lang/String;->substring(II)Ljava/lang/String;
/* .line 351 */
} // :cond_3
/* new-instance v8, Ljava/lang/StringBuilder; */
/* invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V */
/* check-cast v9, Ljava/lang/String; */
(( java.lang.StringBuilder ) v8 ).append ( v9 ); // invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v8 ).append ( v4 ); // invoke-virtual {v8, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v8 ).append ( v7 ); // invoke-virtual {v8, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v8 ).toString ( ); // invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
(( java.io.PrintWriter ) p2 ).println ( v8 ); // invoke-virtual {p2, v8}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V
/* .line 352 */
} // .end local v6 # "entry":Ljava/util/Map$Entry;, "Ljava/util/Map$Entry<Ljava/lang/String;Ljava/util/List<Ljava/lang/String;>;>;"
} // .end local v7 # "valueStr":Ljava/lang/String;
/* .line 354 */
} // :cond_4
v1 = this.mFrameTurboSceneWhiteListMap;
v1 = if ( v1 != null) { // if-eqz v1, :cond_7
/* if-lez v1, :cond_7 */
/* .line 355 */
final String v1 = "--------------------scene white list---------------------"; // const-string v1, "--------------------scene white list---------------------"
(( java.io.PrintWriter ) p2 ).println ( v1 ); // invoke-virtual {p2, v1}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V
/* .line 356 */
v1 = this.mFrameTurboSceneWhiteListMap;
v6 = } // :goto_2
if ( v6 != null) { // if-eqz v6, :cond_7
/* check-cast v6, Ljava/util/Map$Entry; */
/* .line 357 */
/* .restart local v6 # "entry":Ljava/util/Map$Entry;, "Ljava/util/Map$Entry<Ljava/lang/String;Ljava/util/List<Ljava/lang/String;>;>;" */
final String v7 = ""; // const-string v7, ""
/* .line 358 */
/* .restart local v7 # "valueStr":Ljava/lang/String; */
/* check-cast v8, Ljava/util/List; */
v9 = } // :goto_3
if ( v9 != null) { // if-eqz v9, :cond_5
/* check-cast v9, Ljava/lang/String; */
/* .line 359 */
/* .restart local v9 # "value":Ljava/lang/String; */
/* new-instance v10, Ljava/lang/StringBuilder; */
/* invoke-direct {v10}, Ljava/lang/StringBuilder;-><init>()V */
(( java.lang.StringBuilder ) v10 ).append ( v7 ); // invoke-virtual {v10, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v10 ).append ( v9 ); // invoke-virtual {v10, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v10 ).append ( v5 ); // invoke-virtual {v10, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v10 ).toString ( ); // invoke-virtual {v10}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
/* .line 360 */
} // .end local v9 # "value":Ljava/lang/String;
/* .line 361 */
} // :cond_5
v8 = (( java.lang.String ) v7 ).endsWith ( v5 ); // invoke-virtual {v7, v5}, Ljava/lang/String;->endsWith(Ljava/lang/String;)Z
if ( v8 != null) { // if-eqz v8, :cond_6
/* .line 362 */
v8 = (( java.lang.String ) v7 ).length ( ); // invoke-virtual {v7}, Ljava/lang/String;->length()I
/* sub-int/2addr v8, v2 */
(( java.lang.String ) v7 ).substring ( v3, v8 ); // invoke-virtual {v7, v3, v8}, Ljava/lang/String;->substring(II)Ljava/lang/String;
/* .line 364 */
} // :cond_6
/* new-instance v8, Ljava/lang/StringBuilder; */
/* invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V */
/* check-cast v9, Ljava/lang/String; */
(( java.lang.StringBuilder ) v8 ).append ( v9 ); // invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v8 ).append ( v4 ); // invoke-virtual {v8, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v8 ).append ( v7 ); // invoke-virtual {v8, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v8 ).toString ( ); // invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
(( java.io.PrintWriter ) p2 ).println ( v8 ); // invoke-virtual {p2, v8}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V
/* .line 365 */
} // .end local v6 # "entry":Ljava/util/Map$Entry;, "Ljava/util/Map$Entry<Ljava/lang/String;Ljava/util/List<Ljava/lang/String;>;>;"
} // .end local v7 # "valueStr":Ljava/lang/String;
/* .line 367 */
} // :cond_7
v1 = this.mActiveSceneWaitListMap;
v1 = if ( v1 != null) { // if-eqz v1, :cond_a
/* if-lez v1, :cond_a */
/* .line 368 */
final String v1 = "--------------------active scene wait set---------------------"; // const-string v1, "--------------------active scene wait set---------------------"
(( java.io.PrintWriter ) p2 ).println ( v1 ); // invoke-virtual {p2, v1}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V
/* .line 369 */
v1 = this.mActiveSceneWaitListMap;
v6 = } // :goto_4
if ( v6 != null) { // if-eqz v6, :cond_a
/* check-cast v6, Ljava/util/Map$Entry; */
/* .line 370 */
/* .local v6, "entry":Ljava/util/Map$Entry;, "Ljava/util/Map$Entry<Ljava/lang/String;Ljava/util/LinkedList<Ljava/lang/String;>;>;" */
final String v7 = ""; // const-string v7, ""
/* .line 371 */
/* .restart local v7 # "valueStr":Ljava/lang/String; */
/* check-cast v8, Ljava/util/LinkedList; */
(( java.util.LinkedList ) v8 ).iterator ( ); // invoke-virtual {v8}, Ljava/util/LinkedList;->iterator()Ljava/util/Iterator;
v9 = } // :goto_5
if ( v9 != null) { // if-eqz v9, :cond_8
/* check-cast v9, Ljava/lang/String; */
/* .line 372 */
/* .restart local v9 # "value":Ljava/lang/String; */
/* new-instance v10, Ljava/lang/StringBuilder; */
/* invoke-direct {v10}, Ljava/lang/StringBuilder;-><init>()V */
(( java.lang.StringBuilder ) v10 ).append ( v7 ); // invoke-virtual {v10, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v10 ).append ( v9 ); // invoke-virtual {v10, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v10 ).append ( v5 ); // invoke-virtual {v10, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v10 ).toString ( ); // invoke-virtual {v10}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
/* .line 373 */
} // .end local v9 # "value":Ljava/lang/String;
/* .line 374 */
} // :cond_8
v8 = (( java.lang.String ) v7 ).endsWith ( v5 ); // invoke-virtual {v7, v5}, Ljava/lang/String;->endsWith(Ljava/lang/String;)Z
if ( v8 != null) { // if-eqz v8, :cond_9
/* .line 375 */
v8 = (( java.lang.String ) v7 ).length ( ); // invoke-virtual {v7}, Ljava/lang/String;->length()I
/* sub-int/2addr v8, v2 */
(( java.lang.String ) v7 ).substring ( v3, v8 ); // invoke-virtual {v7, v3, v8}, Ljava/lang/String;->substring(II)Ljava/lang/String;
/* .line 377 */
} // :cond_9
/* new-instance v8, Ljava/lang/StringBuilder; */
/* invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V */
/* check-cast v9, Ljava/lang/String; */
(( java.lang.StringBuilder ) v8 ).append ( v9 ); // invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v8 ).append ( v4 ); // invoke-virtual {v8, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v8 ).append ( v7 ); // invoke-virtual {v8, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v8 ).toString ( ); // invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
(( java.io.PrintWriter ) p2 ).println ( v8 ); // invoke-virtual {p2, v8}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V
/* .line 378 */
} // .end local v6 # "entry":Ljava/util/Map$Entry;, "Ljava/util/Map$Entry<Ljava/lang/String;Ljava/util/LinkedList<Ljava/lang/String;>;>;"
} // .end local v7 # "valueStr":Ljava/lang/String;
/* .line 380 */
} // :cond_a
} // .end method
protected void registerSceneCallback ( miui.turbosched.ITurboSchedManager$IFrameTurboSceneCallback p0 ) {
/* .locals 2 */
/* .param p1, "callback" # Lmiui/turbosched/ITurboSchedManager$IFrameTurboSceneCallback; */
/* .line 435 */
/* iget-boolean v0, p0, Lcom/miui/server/turbosched/FrameTurboAction;->mDebug:Z */
if ( v0 != null) { // if-eqz v0, :cond_0
/* .line 436 */
final String v0 = "TurboSched_FrameTurbo"; // const-string v0, "TurboSched_FrameTurbo"
final String v1 = "registerSceneCallback"; // const-string v1, "registerSceneCallback"
android.util.Slog .d ( v0,v1 );
/* .line 438 */
} // :cond_0
v0 = this.mFrameTurboSceneCallbacks;
/* monitor-enter v0 */
/* .line 439 */
try { // :try_start_0
v1 = v1 = this.mFrameTurboSceneCallbacks;
/* if-nez v1, :cond_1 */
/* .line 440 */
v1 = this.mFrameTurboSceneCallbacks;
/* .line 442 */
} // :cond_1
/* monitor-exit v0 */
/* .line 443 */
return;
/* .line 442 */
/* :catchall_0 */
/* move-exception v1 */
/* monitor-exit v0 */
/* :try_end_0 */
/* .catchall {:try_start_0 ..:try_end_0} :catchall_0 */
/* throw v1 */
} // .end method
protected Integer triggerFrameTurbo ( java.lang.String p0, Boolean p1, java.util.List p2, java.util.List p3 ) {
/* .locals 4 */
/* .param p1, "packageName" # Ljava/lang/String; */
/* .param p2, "turbo" # Z */
/* .annotation system Ldalvik/annotation/Signature; */
/* value = { */
/* "(", */
/* "Ljava/lang/String;", */
/* "Z", */
/* "Ljava/util/List<", */
/* "Ljava/lang/String;", */
/* ">;", */
/* "Ljava/util/List<", */
/* "Ljava/lang/String;", */
/* ">;)I" */
/* } */
} // .end annotation
/* .line 535 */
/* .local p3, "uiThreads":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;" */
/* .local p4, "scenes":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;" */
v1 = } // :goto_0
if ( v1 != null) { // if-eqz v1, :cond_2
/* check-cast v1, Ljava/lang/String; */
/* .line 536 */
/* .local v1, "uiThread":Ljava/lang/String; */
/* new-instance v2, Ljava/lang/StringBuilder; */
/* invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V */
(( java.lang.StringBuilder ) v2 ).append ( p1 ); // invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
final String v3 = "#"; // const-string v3, "#"
(( java.lang.StringBuilder ) v2 ).append ( v3 ); // invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v2 ).append ( v1 ); // invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v2 ).toString ( ); // invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
/* .line 537 */
/* .local v2, "processThread":Ljava/lang/String; */
v3 = if ( p4 != null) { // if-eqz p4, :cond_1
/* if-nez v3, :cond_0 */
/* .line 540 */
} // :cond_0
/* const-string/jumbo v3, "|" */
android.text.TextUtils .join ( v3,p4 );
/* .line 541 */
/* .local v3, "sceneList":Ljava/lang/String; */
(( com.miui.server.turbosched.FrameTurboAction ) p0 ).enableFrameTurboInternal ( p2, v2, v3 ); // invoke-virtual {p0, p2, v2, v3}, Lcom/miui/server/turbosched/FrameTurboAction;->enableFrameTurboInternal(ZLjava/lang/String;Ljava/lang/String;)V
/* .line 542 */
} // .end local v1 # "uiThread":Ljava/lang/String;
} // .end local v2 # "processThread":Ljava/lang/String;
} // .end local v3 # "sceneList":Ljava/lang/String;
/* .line 538 */
/* .restart local v1 # "uiThread":Ljava/lang/String; */
/* .restart local v2 # "processThread":Ljava/lang/String; */
} // :cond_1
} // :goto_1
int v0 = -3; // const/4 v0, -0x3
/* .line 543 */
} // .end local v1 # "uiThread":Ljava/lang/String;
} // .end local v2 # "processThread":Ljava/lang/String;
} // :cond_2
int v0 = 0; // const/4 v0, 0x0
} // .end method
protected void unregisterSceneCallback ( miui.turbosched.ITurboSchedManager$IFrameTurboSceneCallback p0 ) {
/* .locals 2 */
/* .param p1, "callback" # Lmiui/turbosched/ITurboSchedManager$IFrameTurboSceneCallback; */
/* .line 446 */
/* iget-boolean v0, p0, Lcom/miui/server/turbosched/FrameTurboAction;->mDebug:Z */
if ( v0 != null) { // if-eqz v0, :cond_0
/* .line 447 */
final String v0 = "TurboSched_FrameTurbo"; // const-string v0, "TurboSched_FrameTurbo"
/* const-string/jumbo v1, "unregisterSceneCallback" */
android.util.Slog .d ( v0,v1 );
/* .line 449 */
} // :cond_0
v0 = this.mFrameTurboSceneCallbacks;
/* monitor-enter v0 */
/* .line 450 */
try { // :try_start_0
v1 = v1 = this.mFrameTurboSceneCallbacks;
if ( v1 != null) { // if-eqz v1, :cond_1
/* .line 451 */
v1 = this.mFrameTurboSceneCallbacks;
/* .line 453 */
} // :cond_1
/* monitor-exit v0 */
/* .line 454 */
return;
/* .line 453 */
/* :catchall_0 */
/* move-exception v1 */
/* monitor-exit v0 */
/* :try_end_0 */
/* .catchall {:try_start_0 ..:try_end_0} :catchall_0 */
/* throw v1 */
} // .end method
protected void updateWhiteList ( java.lang.String[] p0 ) {
/* .locals 11 */
/* .param p1, "pkgScenes" # [Ljava/lang/String; */
/* .line 548 */
v0 = this.mFrameTurboSceneWhiteListMap;
/* .line 549 */
/* array-length v0, p1 */
/* if-lez v0, :cond_9 */
/* .line 550 */
/* array-length v0, p1 */
int v1 = 0; // const/4 v1, 0x0
/* move v2, v1 */
} // :goto_0
int v3 = 2; // const/4 v3, 0x2
final String v4 = ":"; // const-string v4, ":"
/* if-ge v2, v0, :cond_4 */
/* aget-object v5, p1, v2 */
/* .line 551 */
/* .local v5, "pkgScene":Ljava/lang/String; */
(( java.lang.String ) v5 ).split ( v4 ); // invoke-virtual {v5, v4}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;
/* .line 552 */
/* .local v4, "pkgSceneInfo":[Ljava/lang/String; */
/* array-length v6, v4 */
/* if-ne v6, v3, :cond_3 */
/* .line 553 */
/* aget-object v3, v4, v1 */
/* .line 554 */
/* .local v3, "pkgName":Ljava/lang/String; */
v6 = this.mFrameTurboSceneWhiteListMap;
/* check-cast v6, Ljava/util/List; */
/* .line 555 */
/* .local v6, "sceneList":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;" */
/* if-nez v6, :cond_0 */
/* .line 556 */
/* new-instance v7, Ljava/util/ArrayList; */
/* invoke-direct {v7}, Ljava/util/ArrayList;-><init>()V */
/* move-object v6, v7 */
/* .line 558 */
} // :cond_0
int v7 = 1; // const/4 v7, 0x1
/* aget-object v7, v4, v7 */
final String v8 = "\\|"; // const-string v8, "\\|"
(( java.lang.String ) v7 ).split ( v8 ); // invoke-virtual {v7, v8}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;
/* .line 559 */
/* .local v7, "scenes":[Ljava/lang/String; */
/* array-length v8, v7 */
/* move v9, v1 */
} // :goto_1
/* if-ge v9, v8, :cond_1 */
/* aget-object v10, v7, v9 */
/* .line 560 */
/* .local v10, "scene":Ljava/lang/String; */
/* .line 559 */
} // .end local v10 # "scene":Ljava/lang/String;
/* add-int/lit8 v9, v9, 0x1 */
/* .line 562 */
v8 = } // :cond_1
/* if-nez v8, :cond_2 */
/* .line 563 */
v8 = this.mFrameTurboSceneWhiteListMap;
/* .line 565 */
} // :cond_2
v8 = this.mFrameTurboSceneWhiteListMap;
/* .line 550 */
} // .end local v3 # "pkgName":Ljava/lang/String;
} // .end local v4 # "pkgSceneInfo":[Ljava/lang/String;
} // .end local v5 # "pkgScene":Ljava/lang/String;
} // .end local v6 # "sceneList":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;"
} // .end local v7 # "scenes":[Ljava/lang/String;
} // :cond_3
} // :goto_2
/* add-int/lit8 v2, v2, 0x1 */
/* .line 569 */
} // :cond_4
final String v0 = ""; // const-string v0, ""
/* .line 570 */
/* .local v0, "whiteListStr":Ljava/lang/String; */
v2 = this.mFrameTurboSceneWhiteListMap;
} // :cond_5
v5 = } // :goto_3
if ( v5 != null) { // if-eqz v5, :cond_7
/* check-cast v5, Ljava/lang/String; */
/* .line 571 */
/* .local v5, "key":Ljava/lang/String; */
v6 = this.mFrameTurboSceneWhiteListMap;
/* check-cast v6, Ljava/util/List; */
/* .line 572 */
/* .local v6, "sceneWhiteList":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;" */
v7 = if ( v6 != null) { // if-eqz v6, :cond_5
if ( v7 != null) { // if-eqz v7, :cond_6
/* .line 573 */
/* .line 575 */
} // :cond_6
/* new-instance v7, Ljava/lang/StringBuilder; */
/* invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V */
(( java.lang.StringBuilder ) v7 ).append ( v0 ); // invoke-virtual {v7, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v7 ).append ( v5 ); // invoke-virtual {v7, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v7 ).append ( v4 ); // invoke-virtual {v7, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
/* const-string/jumbo v8, "|" */
android.text.TextUtils .join ( v8,v6 );
(( java.lang.StringBuilder ) v7 ).append ( v8 ); // invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
final String v8 = ", "; // const-string v8, ", "
(( java.lang.StringBuilder ) v7 ).append ( v8 ); // invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v7 ).toString ( ); // invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
/* .line 576 */
} // .end local v5 # "key":Ljava/lang/String;
} // .end local v6 # "sceneWhiteList":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;"
/* .line 577 */
} // :cond_7
v2 = (( java.lang.String ) v0 ).length ( ); // invoke-virtual {v0}, Ljava/lang/String;->length()I
/* if-lez v2, :cond_8 */
/* .line 578 */
v2 = (( java.lang.String ) v0 ).length ( ); // invoke-virtual {v0}, Ljava/lang/String;->length()I
/* sub-int/2addr v2, v3 */
(( java.lang.String ) v0 ).substring ( v1, v2 ); // invoke-virtual {v0, v1, v2}, Ljava/lang/String;->substring(II)Ljava/lang/String;
/* .line 580 */
} // :cond_8
final String v1 = "persist.sys.turbosched.frame_turbo.scene_white_list"; // const-string v1, "persist.sys.turbosched.frame_turbo.scene_white_list"
android.os.SystemProperties .set ( v1,v0 );
/* .line 582 */
} // .end local v0 # "whiteListStr":Ljava/lang/String;
} // :cond_9
return;
} // .end method
