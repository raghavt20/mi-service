class com.miui.server.turbosched.TurboSchedSceneManager$5 extends miui.app.IFreeformCallback$Stub {
	 /* .source "TurboSchedSceneManager.java" */
	 /* # annotations */
	 /* .annotation system Ldalvik/annotation/EnclosingClass; */
	 /* value = Lcom/miui/server/turbosched/TurboSchedSceneManager; */
} // .end annotation
/* .annotation system Ldalvik/annotation/InnerClass; */
/* accessFlags = 0x0 */
/* name = null */
} // .end annotation
/* # instance fields */
final com.miui.server.turbosched.TurboSchedSceneManager this$0; //synthetic
/* # direct methods */
 com.miui.server.turbosched.TurboSchedSceneManager$5 ( ) {
/* .locals 0 */
/* .param p1, "this$0" # Lcom/miui/server/turbosched/TurboSchedSceneManager; */
/* .line 340 */
this.this$0 = p1;
/* invoke-direct {p0}, Lmiui/app/IFreeformCallback$Stub;-><init>()V */
return;
} // .end method
/* # virtual methods */
public void dispatchFreeFormStackModeChanged ( Integer p0, miui.app.MiuiFreeFormManager$MiuiFreeFormStackInfo p1 ) {
/* .locals 3 */
/* .param p1, "action" # I */
/* .param p2, "stackInfo" # Lmiui/app/MiuiFreeFormManager$MiuiFreeFormStackInfo; */
/* .line 342 */
/* new-instance v0, Landroid/os/Bundle; */
/* invoke-direct {v0}, Landroid/os/Bundle;-><init>()V */
/* .line 343 */
/* .local v0, "bundle":Landroid/os/Bundle; */
final String v1 = "action"; // const-string v1, "action"
(( android.os.Bundle ) v0 ).putInt ( v1, p1 ); // invoke-virtual {v0, v1, p1}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V
/* .line 344 */
final String v1 = "pkgName"; // const-string v1, "pkgName"
v2 = this.packageName;
(( android.os.Bundle ) v0 ).putString ( v1, v2 ); // invoke-virtual {v0, v1, v2}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V
/* .line 345 */
/* const-string/jumbo v1, "windowState" */
/* iget v2, p2, Lmiui/app/MiuiFreeFormManager$MiuiFreeFormStackInfo;->windowState:I */
(( android.os.Bundle ) v0 ).putInt ( v1, v2 ); // invoke-virtual {v0, v1, v2}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V
/* .line 346 */
v1 = this.this$0;
com.miui.server.turbosched.TurboSchedSceneManager .-$$Nest$fgetmTbSceneHandler ( v1 );
(( com.miui.server.turbosched.TurboSchedSceneManager$TurboSchedSceneHandler ) v1 ).obtainMessage ( ); // invoke-virtual {v1}, Lcom/miui/server/turbosched/TurboSchedSceneManager$TurboSchedSceneHandler;->obtainMessage()Landroid/os/Message;
/* .line 347 */
/* .local v1, "msg":Landroid/os/Message; */
(( android.os.Message ) v1 ).setData ( v0 ); // invoke-virtual {v1, v0}, Landroid/os/Message;->setData(Landroid/os/Bundle;)V
/* .line 348 */
/* const/16 v2, 0xb */
/* iput v2, v1, Landroid/os/Message;->what:I */
/* .line 349 */
v2 = this.this$0;
com.miui.server.turbosched.TurboSchedSceneManager .-$$Nest$fgetmTbSceneHandler ( v2 );
(( com.miui.server.turbosched.TurboSchedSceneManager$TurboSchedSceneHandler ) v2 ).sendMessage ( v1 ); // invoke-virtual {v2, v1}, Lcom/miui/server/turbosched/TurboSchedSceneManager$TurboSchedSceneHandler;->sendMessage(Landroid/os/Message;)Z
/* .line 350 */
return;
} // .end method
