.class Lcom/miui/server/turbosched/TurboSchedSceneManager$2;
.super Landroid/content/BroadcastReceiver;
.source "TurboSchedSceneManager.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/miui/server/turbosched/TurboSchedSceneManager;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/miui/server/turbosched/TurboSchedSceneManager;


# direct methods
.method constructor <init>(Lcom/miui/server/turbosched/TurboSchedSceneManager;)V
    .locals 0
    .param p1, "this$0"    # Lcom/miui/server/turbosched/TurboSchedSceneManager;

    .line 234
    iput-object p1, p0, Lcom/miui/server/turbosched/TurboSchedSceneManager$2;->this$0:Lcom/miui/server/turbosched/TurboSchedSceneManager;

    invoke-direct {p0}, Landroid/content/BroadcastReceiver;-><init>()V

    return-void
.end method


# virtual methods
.method public onReceive(Landroid/content/Context;Landroid/content/Intent;)V
    .locals 2
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "intent"    # Landroid/content/Intent;

    .line 237
    invoke-virtual {p2}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v0

    if-nez v0, :cond_0

    .line 238
    return-void

    .line 240
    :cond_0
    invoke-virtual {p2}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/String;->hashCode()I

    move-result v1

    packed-switch v1, :pswitch_data_0

    :cond_1
    goto :goto_0

    :pswitch_0
    const-string v1, "android.intent.action.BOOT_COMPLETED"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    const/4 v0, 0x0

    goto :goto_1

    :goto_0
    const/4 v0, -0x1

    :goto_1
    packed-switch v0, :pswitch_data_1

    goto :goto_2

    .line 242
    :pswitch_1
    invoke-static {}, Landroid/os/TurboSchedMonitor;->getInstance()Landroid/os/TurboSchedMonitor;

    move-result-object v0

    invoke-virtual {v0}, Landroid/os/TurboSchedMonitor;->isDebugMode()Z

    move-result v0

    if-eqz v0, :cond_2

    .line 243
    const-string/jumbo v0, "tsched_scene"

    const-string/jumbo v1, "turbosched scene manager receive boot complete message"

    invoke-static {v0, v1}, Landroid/util/Slog;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 245
    :cond_2
    iget-object v0, p0, Lcom/miui/server/turbosched/TurboSchedSceneManager$2;->this$0:Lcom/miui/server/turbosched/TurboSchedSceneManager;

    invoke-static {v0}, Lcom/miui/server/turbosched/TurboSchedSceneManager;->-$$Nest$monBootComplete(Lcom/miui/server/turbosched/TurboSchedSceneManager;)V

    .line 246
    nop

    .line 250
    :goto_2
    return-void

    :pswitch_data_0
    .packed-switch 0x2f94f923
        :pswitch_0
    .end packed-switch

    :pswitch_data_1
    .packed-switch 0x0
        :pswitch_1
    .end packed-switch
.end method
