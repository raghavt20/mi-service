.class Lcom/miui/server/turbosched/TurboSchedManagerService$4;
.super Landroid/database/ContentObserver;
.source "TurboSchedManagerService.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/miui/server/turbosched/TurboSchedManagerService;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/miui/server/turbosched/TurboSchedManagerService;


# direct methods
.method constructor <init>(Lcom/miui/server/turbosched/TurboSchedManagerService;Landroid/os/Handler;)V
    .locals 0
    .param p1, "this$0"    # Lcom/miui/server/turbosched/TurboSchedManagerService;
    .param p2, "handler"    # Landroid/os/Handler;

    .line 706
    iput-object p1, p0, Lcom/miui/server/turbosched/TurboSchedManagerService$4;->this$0:Lcom/miui/server/turbosched/TurboSchedManagerService;

    invoke-direct {p0, p2}, Landroid/database/ContentObserver;-><init>(Landroid/os/Handler;)V

    return-void
.end method


# virtual methods
.method public onChange(ZLandroid/net/Uri;)V
    .locals 1
    .param p1, "selfChange"    # Z
    .param p2, "uri"    # Landroid/net/Uri;

    .line 709
    if-eqz p2, :cond_0

    const-string v0, "cloud_turbo_sched_enable_core_top20_app_optimizer"

    invoke-static {v0}, Landroid/provider/Settings$System;->getUriFor(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v0

    invoke-virtual {p2, v0}, Landroid/net/Uri;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 710
    iget-object v0, p0, Lcom/miui/server/turbosched/TurboSchedManagerService$4;->this$0:Lcom/miui/server/turbosched/TurboSchedManagerService;

    invoke-static {v0}, Lcom/miui/server/turbosched/TurboSchedManagerService;->-$$Nest$mupdateCoreTop20AppOptimizerEnableProp(Lcom/miui/server/turbosched/TurboSchedManagerService;)V

    .line 712
    :cond_0
    return-void
.end method
