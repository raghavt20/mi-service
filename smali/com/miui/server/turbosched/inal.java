class inal extends android.os.Handler {
	 /* .source "TurboSchedManagerService.java" */
	 /* # annotations */
	 /* .annotation system Ldalvik/annotation/EnclosingClass; */
	 /* value = Lcom/miui/server/turbosched/TurboSchedManagerService; */
} // .end annotation
/* .annotation system Ldalvik/annotation/InnerClass; */
/* accessFlags = 0x12 */
/* name = "TurboSchedHandler" */
} // .end annotation
/* # static fields */
static final Integer MSG_TRACE_BEGIN;
static final Integer MSG_TRACE_END;
/* # instance fields */
final com.miui.server.turbosched.TurboSchedManagerService this$0; //synthetic
/* # direct methods */
private inal ( ) {
/* .locals 0 */
/* .param p2, "looper" # Landroid/os/Looper; */
/* .line 554 */
this.this$0 = p1;
/* .line 555 */
/* invoke-direct {p0, p2}, Landroid/os/Handler;-><init>(Landroid/os/Looper;)V */
/* .line 556 */
return;
} // .end method
 inal ( ) { //synthethic
/* .locals 0 */
/* invoke-direct {p0, p1, p2}, Lcom/miui/server/turbosched/TurboSchedManagerService$TurboSchedHandler;-><init>(Lcom/miui/server/turbosched/TurboSchedManagerService;Landroid/os/Looper;)V */
return;
} // .end method
private void traceBegin ( java.lang.String p0, java.lang.String p1, Long p2 ) {
/* .locals 5 */
/* .param p1, "name" # Ljava/lang/String; */
/* .param p2, "desc" # Ljava/lang/String; */
/* .param p3, "stratTime" # J */
/* .line 580 */
java.lang.System .currentTimeMillis ( );
/* move-result-wide v0 */
/* sub-long/2addr v0, p3 */
/* .line 581 */
/* .local v0, "delay":J */
if ( p2 != null) { // if-eqz p2, :cond_0
	 v2 = 	 (( java.lang.String ) p2 ).isEmpty ( ); // invoke-virtual {p2}, Ljava/lang/String;->isEmpty()Z
	 /* if-nez v2, :cond_0 */
	 /* .line 582 */
	 /* new-instance v2, Ljava/lang/StringBuilder; */
	 /* invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V */
	 final String v3 = "("; // const-string v3, "("
	 (( java.lang.StringBuilder ) v2 ).append ( v3 ); // invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
	 (( java.lang.StringBuilder ) v2 ).append ( p2 ); // invoke-virtual {v2, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
	 final String v3 = ")"; // const-string v3, ")"
	 (( java.lang.StringBuilder ) v2 ).append ( v3 ); // invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
	 (( java.lang.StringBuilder ) v2 ).toString ( ); // invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
	 /* .line 584 */
} // :cond_0
/* new-instance v2, Ljava/lang/StringBuilder; */
/* invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V */
(( java.lang.StringBuilder ) v2 ).append ( p1 ); // invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v2 ).append ( p2 ); // invoke-virtual {v2, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
final String v3 = " delay="; // const-string v3, " delay="
(( java.lang.StringBuilder ) v2 ).append ( v3 ); // invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v2 ).append ( v0, v1 ); // invoke-virtual {v2, v0, v1}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v2 ).toString ( ); // invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
/* .line 585 */
/* .local v2, "tag":Ljava/lang/String; */
/* const-wide/16 v3, 0x40 */
android.os.Trace .traceBegin ( v3,v4,v2 );
/* .line 586 */
return;
} // .end method
private void traceEnd ( ) {
/* .locals 2 */
/* .line 589 */
/* const-wide/16 v0, 0x40 */
android.os.Trace .traceEnd ( v0,v1 );
/* .line 590 */
return;
} // .end method
/* # virtual methods */
public void handleMessage ( android.os.Message p0 ) {
/* .locals 8 */
/* .param p1, "msg" # Landroid/os/Message; */
/* .line 560 */
/* iget v0, p1, Landroid/os/Message;->what:I */
/* .line 561 */
/* .local v0, "what":I */
/* const-wide/16 v1, 0x40 */
/* packed-switch v0, :pswitch_data_0 */
/* .line 570 */
/* :pswitch_0 */
android.os.Trace .traceEnd ( v1,v2 );
/* .line 571 */
/* invoke-direct {p0}, Lcom/miui/server/turbosched/TurboSchedManagerService$TurboSchedHandler;->traceEnd()V */
/* .line 572 */
/* .line 563 */
/* :pswitch_1 */
v3 = this.obj;
/* check-cast v3, Lcom/miui/server/turbosched/TurboSchedManagerService$TraceInfo; */
/* .line 564 */
/* .local v3, "info":Lcom/miui/server/turbosched/TurboSchedManagerService$TraceInfo; */
v4 = this.name;
v5 = this.desc;
/* iget-wide v6, v3, Lcom/miui/server/turbosched/TurboSchedManagerService$TraceInfo;->startTime:J */
/* invoke-direct {p0, v4, v5, v6, v7}, Lcom/miui/server/turbosched/TurboSchedManagerService$TurboSchedHandler;->traceBegin(Ljava/lang/String;Ljava/lang/String;J)V */
/* .line 565 */
/* iget v4, p1, Landroid/os/Message;->arg1:I */
/* .line 566 */
/* .local v4, "tid":I */
/* new-instance v5, Ljava/lang/StringBuilder; */
/* invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V */
final String v6 = "P-Tid:"; // const-string v6, "P-Tid:"
(( java.lang.StringBuilder ) v5 ).append ( v6 ); // invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v5 ).append ( v4 ); // invoke-virtual {v5, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v5 ).toString ( ); // invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
android.os.Trace .traceBegin ( v1,v2,v5 );
/* .line 567 */
/* nop */
/* .line 577 */
} // .end local v3 # "info":Lcom/miui/server/turbosched/TurboSchedManagerService$TraceInfo;
} // .end local v4 # "tid":I
} // :goto_0
return;
/* :pswitch_data_0 */
/* .packed-switch 0x0 */
/* :pswitch_1 */
/* :pswitch_0 */
} // .end packed-switch
} // .end method
