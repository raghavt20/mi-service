.class Lcom/miui/server/turbosched/TurboSchedManagerService$TurboSchedBroadcastReceiver;
.super Landroid/content/BroadcastReceiver;
.source "TurboSchedManagerService.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/miui/server/turbosched/TurboSchedManagerService;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "TurboSchedBroadcastReceiver"
.end annotation


# instance fields
.field final synthetic this$0:Lcom/miui/server/turbosched/TurboSchedManagerService;


# direct methods
.method public constructor <init>(Lcom/miui/server/turbosched/TurboSchedManagerService;)V
    .locals 2

    .line 1969
    iput-object p1, p0, Lcom/miui/server/turbosched/TurboSchedManagerService$TurboSchedBroadcastReceiver;->this$0:Lcom/miui/server/turbosched/TurboSchedManagerService;

    invoke-direct {p0}, Landroid/content/BroadcastReceiver;-><init>()V

    .line 1970
    new-instance v0, Landroid/content/IntentFilter;

    invoke-direct {v0}, Landroid/content/IntentFilter;-><init>()V

    .line 1971
    .local v0, "filter":Landroid/content/IntentFilter;
    const-string v1, "android.intent.action.SCREEN_ON"

    invoke-virtual {v0, v1}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    .line 1972
    const-string v1, "android.intent.action.SCREEN_OFF"

    invoke-virtual {v0, v1}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    .line 1973
    invoke-static {p1}, Lcom/miui/server/turbosched/TurboSchedManagerService;->-$$Nest$fgetmContext(Lcom/miui/server/turbosched/TurboSchedManagerService;)Landroid/content/Context;

    move-result-object p1

    invoke-virtual {p1, p0, v0}, Landroid/content/Context;->registerReceiver(Landroid/content/BroadcastReceiver;Landroid/content/IntentFilter;)Landroid/content/Intent;

    .line 1974
    return-void
.end method


# virtual methods
.method public onReceive(Landroid/content/Context;Landroid/content/Intent;)V
    .locals 3
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "intent"    # Landroid/content/Intent;

    .line 1978
    invoke-virtual {p2}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v0

    .line 1979
    .local v0, "action":Ljava/lang/String;
    const-string v1, "android.intent.action.SCREEN_ON"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 1980
    iget-object v1, p0, Lcom/miui/server/turbosched/TurboSchedManagerService$TurboSchedBroadcastReceiver;->this$0:Lcom/miui/server/turbosched/TurboSchedManagerService;

    const/4 v2, 0x1

    invoke-static {v1, v2}, Lcom/miui/server/turbosched/TurboSchedManagerService;->-$$Nest$fputmScreenOn(Lcom/miui/server/turbosched/TurboSchedManagerService;Z)V

    goto :goto_0

    .line 1981
    :cond_0
    const-string v1, "android.intent.action.SCREEN_OFF"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_1

    .line 1982
    iget-object v1, p0, Lcom/miui/server/turbosched/TurboSchedManagerService$TurboSchedBroadcastReceiver;->this$0:Lcom/miui/server/turbosched/TurboSchedManagerService;

    const/4 v2, 0x0

    invoke-static {v1, v2}, Lcom/miui/server/turbosched/TurboSchedManagerService;->-$$Nest$fputmScreenOn(Lcom/miui/server/turbosched/TurboSchedManagerService;Z)V

    .line 1984
    :cond_1
    :goto_0
    return-void
.end method
