.class public Lcom/miui/server/turbosched/TurboSchedSceneManager;
.super Ljava/lang/Object;
.source "TurboSchedSceneManager.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/miui/server/turbosched/TurboSchedSceneManager$TurboSchedSceneManagerHolder;,
        Lcom/miui/server/turbosched/TurboSchedSceneManager$TurboSchedSceneHandler;
    }
.end annotation


# static fields
.field private static final ACTIVITYTASKMANAGER:Ljava/lang/String; = "android.app.ActivityTaskManager"

.field private static final CURR_FOCUSED:I = 0x1

.field private static final FULL_SCREEN:I = -0x1

.field private static final KEY_GAME_BOOSTER:Ljava/lang/String; = "gb_boosting"

.field private static final LAST_FOCUSED:I = 0x0

.field private static final METHOD_GETSERVICE:Ljava/lang/String; = "getService"

.field private static final MINI_SCREEN:I = 0x1

.field private static final MSG_FREEFORM_MODE_CHANGED:I = 0xb

.field private static final MSG_ON_FOCUSED_APP_CHANGED:I = 0x3

.field private static final MSG_ON_FOREGROUND_APP_CHANGED:I = 0x2

.field private static final MSG_ON_TASK_STACK_CHANGE:I = 0x1

.field private static final PERMISSION_PKG_NAME:Ljava/lang/String; = "com.lbe.security.miui"

.field private static final SMALL_SCREEN:I = 0x0

.field private static final TAG:Ljava/lang/String; = "tsched_scene"


# instance fields
.field private mActivityTaskManager:Landroid/app/IActivityTaskManager;

.field private mAppStateListeners:Landroid/util/ArrayMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/util/ArrayMap<",
            "Ljava/lang/String;",
            "Lmiui/turbosched/ITurboSchedManager$ITurboSchedStateChangeCallback;",
            ">;"
        }
    .end annotation
.end field

.field private mContext:Landroid/content/Context;

.field private mCurFocusedPkgName:Ljava/lang/String;

.field private mForegroundAppName:Ljava/lang/String;

.field private mFreeformCallback:Lmiui/app/IFreeformCallback;

.field private mIsGameMode:Z

.field private mIsMiniScreen:Z

.field private mIsSmallScreen:Z

.field private mIsSplitScreen:Z

.field private mLastFocusedPkgName:Ljava/lang/String;

.field private final mLifecycleReceiver:Landroid/content/BroadcastReceiver;

.field private final mLock:Ljava/lang/Object;

.field private final mTaskStackListener:Landroid/app/TaskStackListener;

.field private mTbSceneHandler:Lcom/miui/server/turbosched/TurboSchedSceneManager$TurboSchedSceneHandler;

.field private final mWindowListener:Lmiui/process/IForegroundWindowListener;


# direct methods
.method static bridge synthetic -$$Nest$fgetmTbSceneHandler(Lcom/miui/server/turbosched/TurboSchedSceneManager;)Lcom/miui/server/turbosched/TurboSchedSceneManager$TurboSchedSceneHandler;
    .locals 0

    iget-object p0, p0, Lcom/miui/server/turbosched/TurboSchedSceneManager;->mTbSceneHandler:Lcom/miui/server/turbosched/TurboSchedSceneManager$TurboSchedSceneHandler;

    return-object p0
.end method

.method static bridge synthetic -$$Nest$mhandleFocusAppChange(Lcom/miui/server/turbosched/TurboSchedSceneManager;Landroid/os/Message;)V
    .locals 0

    invoke-direct {p0, p1}, Lcom/miui/server/turbosched/TurboSchedSceneManager;->handleFocusAppChange(Landroid/os/Message;)V

    return-void
.end method

.method static bridge synthetic -$$Nest$mhandleFreeformModeChanged(Lcom/miui/server/turbosched/TurboSchedSceneManager;Landroid/os/Message;)V
    .locals 0

    invoke-direct {p0, p1}, Lcom/miui/server/turbosched/TurboSchedSceneManager;->handleFreeformModeChanged(Landroid/os/Message;)V

    return-void
.end method

.method static bridge synthetic -$$Nest$mhandleWindowChanged(Lcom/miui/server/turbosched/TurboSchedSceneManager;Lmiui/process/ForegroundInfo;)V
    .locals 0

    invoke-direct {p0, p1}, Lcom/miui/server/turbosched/TurboSchedSceneManager;->handleWindowChanged(Lmiui/process/ForegroundInfo;)V

    return-void
.end method

.method static bridge synthetic -$$Nest$mmakeTurboSchedDesion(Lcom/miui/server/turbosched/TurboSchedSceneManager;)V
    .locals 0

    invoke-direct {p0}, Lcom/miui/server/turbosched/TurboSchedSceneManager;->makeTurboSchedDesion()V

    return-void
.end method

.method static bridge synthetic -$$Nest$monBootComplete(Lcom/miui/server/turbosched/TurboSchedSceneManager;)V
    .locals 0

    invoke-direct {p0}, Lcom/miui/server/turbosched/TurboSchedSceneManager;->onBootComplete()V

    return-void
.end method

.method private constructor <init>()V
    .locals 1

    .line 100
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 64
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/miui/server/turbosched/TurboSchedSceneManager;->mIsSmallScreen:Z

    .line 65
    iput-boolean v0, p0, Lcom/miui/server/turbosched/TurboSchedSceneManager;->mIsMiniScreen:Z

    .line 66
    iput-boolean v0, p0, Lcom/miui/server/turbosched/TurboSchedSceneManager;->mIsSplitScreen:Z

    .line 67
    iput-boolean v0, p0, Lcom/miui/server/turbosched/TurboSchedSceneManager;->mIsGameMode:Z

    .line 68
    const-string v0, ""

    iput-object v0, p0, Lcom/miui/server/turbosched/TurboSchedSceneManager;->mLastFocusedPkgName:Ljava/lang/String;

    .line 69
    iput-object v0, p0, Lcom/miui/server/turbosched/TurboSchedSceneManager;->mCurFocusedPkgName:Ljava/lang/String;

    .line 78
    new-instance v0, Ljava/lang/Object;

    invoke-direct {v0}, Ljava/lang/Object;-><init>()V

    iput-object v0, p0, Lcom/miui/server/turbosched/TurboSchedSceneManager;->mLock:Ljava/lang/Object;

    .line 79
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/miui/server/turbosched/TurboSchedSceneManager;->mAppStateListeners:Landroid/util/ArrayMap;

    .line 234
    new-instance v0, Lcom/miui/server/turbosched/TurboSchedSceneManager$2;

    invoke-direct {v0, p0}, Lcom/miui/server/turbosched/TurboSchedSceneManager$2;-><init>(Lcom/miui/server/turbosched/TurboSchedSceneManager;)V

    iput-object v0, p0, Lcom/miui/server/turbosched/TurboSchedSceneManager;->mLifecycleReceiver:Landroid/content/BroadcastReceiver;

    .line 314
    new-instance v0, Lcom/miui/server/turbosched/TurboSchedSceneManager$3;

    invoke-direct {v0, p0}, Lcom/miui/server/turbosched/TurboSchedSceneManager$3;-><init>(Lcom/miui/server/turbosched/TurboSchedSceneManager;)V

    iput-object v0, p0, Lcom/miui/server/turbosched/TurboSchedSceneManager;->mTaskStackListener:Landroid/app/TaskStackListener;

    .line 327
    new-instance v0, Lcom/miui/server/turbosched/TurboSchedSceneManager$4;

    invoke-direct {v0, p0}, Lcom/miui/server/turbosched/TurboSchedSceneManager$4;-><init>(Lcom/miui/server/turbosched/TurboSchedSceneManager;)V

    iput-object v0, p0, Lcom/miui/server/turbosched/TurboSchedSceneManager;->mWindowListener:Lmiui/process/IForegroundWindowListener;

    .line 340
    new-instance v0, Lcom/miui/server/turbosched/TurboSchedSceneManager$5;

    invoke-direct {v0, p0}, Lcom/miui/server/turbosched/TurboSchedSceneManager$5;-><init>(Lcom/miui/server/turbosched/TurboSchedSceneManager;)V

    iput-object v0, p0, Lcom/miui/server/turbosched/TurboSchedSceneManager;->mFreeformCallback:Lmiui/app/IFreeformCallback;

    .line 101
    new-instance v0, Landroid/util/ArrayMap;

    invoke-direct {v0}, Landroid/util/ArrayMap;-><init>()V

    iput-object v0, p0, Lcom/miui/server/turbosched/TurboSchedSceneManager;->mAppStateListeners:Landroid/util/ArrayMap;

    .line 102
    invoke-static {}, Landroid/app/ActivityTaskManager;->getService()Landroid/app/IActivityTaskManager;

    move-result-object v0

    iput-object v0, p0, Lcom/miui/server/turbosched/TurboSchedSceneManager;->mActivityTaskManager:Landroid/app/IActivityTaskManager;

    .line 103
    return-void
.end method

.method synthetic constructor <init>(Lcom/miui/server/turbosched/TurboSchedSceneManager-IA;)V
    .locals 0

    invoke-direct {p0}, Lcom/miui/server/turbosched/TurboSchedSceneManager;-><init>()V

    return-void
.end method

.method public static getInstance()Lcom/miui/server/turbosched/TurboSchedSceneManager;
    .locals 1

    .line 97
    sget-object v0, Lcom/miui/server/turbosched/TurboSchedSceneManager$TurboSchedSceneManagerHolder;->sInstance:Lcom/miui/server/turbosched/TurboSchedSceneManager;

    return-object v0
.end method

.method private handleFocusAppChange(Landroid/os/Message;)V
    .locals 4
    .param p1, "msg"    # Landroid/os/Message;

    .line 378
    invoke-virtual {p1}, Landroid/os/Message;->getData()Landroid/os/Bundle;

    move-result-object v0

    .line 379
    .local v0, "data":Landroid/os/Bundle;
    const-string/jumbo v1, "type"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getInt(Ljava/lang/String;)I

    move-result v1

    .line 380
    .local v1, "type":I
    const-string v2, "focus"

    invoke-virtual {v0, v2}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    .line 381
    .local v2, "focus":Ljava/lang/String;
    packed-switch v1, :pswitch_data_0

    goto :goto_0

    .line 387
    :pswitch_0
    iput-object v2, p0, Lcom/miui/server/turbosched/TurboSchedSceneManager;->mCurFocusedPkgName:Ljava/lang/String;

    .line 388
    const/4 v3, 0x1

    invoke-direct {p0, v2, v3}, Lcom/miui/server/turbosched/TurboSchedSceneManager;->setFocusedPkgState(Ljava/lang/String;Z)V

    .line 389
    goto :goto_0

    .line 383
    :pswitch_1
    iput-object v2, p0, Lcom/miui/server/turbosched/TurboSchedSceneManager;->mLastFocusedPkgName:Ljava/lang/String;

    .line 384
    const/4 v3, 0x0

    invoke-direct {p0, v2, v3}, Lcom/miui/server/turbosched/TurboSchedSceneManager;->setFocusedPkgState(Ljava/lang/String;Z)V

    .line 385
    nop

    .line 393
    :goto_0
    return-void

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_1
        :pswitch_0
    .end packed-switch
.end method

.method private handleFreeformModeChanged(Landroid/os/Message;)V
    .locals 8
    .param p1, "msg"    # Landroid/os/Message;

    .line 396
    invoke-virtual {p1}, Landroid/os/Message;->getData()Landroid/os/Bundle;

    move-result-object v0

    .line 397
    .local v0, "data":Landroid/os/Bundle;
    const-string v1, "action"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getInt(Ljava/lang/String;)I

    move-result v1

    .line 398
    .local v1, "action":I
    const-string v2, "pkgName"

    invoke-virtual {v0, v2}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    .line 399
    .local v2, "pkgName":Ljava/lang/String;
    const-string/jumbo v3, "windowState"

    invoke-virtual {v0, v3}, Landroid/os/Bundle;->getInt(Ljava/lang/String;)I

    move-result v3

    .line 400
    .local v3, "windowState":I
    const-string v4, ", state:"

    const/4 v5, 0x0

    const/4 v6, 0x1

    const-string/jumbo v7, "tsched_scene"

    packed-switch v1, :pswitch_data_0

    .line 457
    :pswitch_0
    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v5, "warning for access here action="

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, ", windowstate="

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v7, v4}, Landroid/util/Slog;->w(Ljava/lang/String;Ljava/lang/String;)I

    .line 458
    return-void

    .line 455
    :pswitch_1
    goto/16 :goto_0

    .line 445
    :pswitch_2
    iput-boolean v5, p0, Lcom/miui/server/turbosched/TurboSchedSceneManager;->mIsSmallScreen:Z

    .line 446
    iput-boolean v5, p0, Lcom/miui/server/turbosched/TurboSchedSceneManager;->mIsMiniScreen:Z

    .line 447
    invoke-static {}, Landroid/os/TurboSchedMonitor;->getInstance()Landroid/os/TurboSchedMonitor;

    move-result-object v5

    invoke-virtual {v5}, Landroid/os/TurboSchedMonitor;->isDebugMode()Z

    move-result v5

    if-eqz v5, :cond_0

    .line 448
    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "MINIFREEFORM_TO_FULLSCREEN:"

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v7, v4}, Landroid/util/Slog;->d(Ljava/lang/String;Ljava/lang/String;)I

    goto/16 :goto_0

    .line 429
    :pswitch_3
    iput-boolean v6, p0, Lcom/miui/server/turbosched/TurboSchedSceneManager;->mIsSmallScreen:Z

    .line 430
    iput-boolean v5, p0, Lcom/miui/server/turbosched/TurboSchedSceneManager;->mIsMiniScreen:Z

    .line 431
    invoke-static {}, Landroid/os/TurboSchedMonitor;->getInstance()Landroid/os/TurboSchedMonitor;

    move-result-object v5

    invoke-virtual {v5}, Landroid/os/TurboSchedMonitor;->isDebugMode()Z

    move-result v5

    if-eqz v5, :cond_0

    .line 432
    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "MINIFREEFORM_TO_FREEFORM:"

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v7, v4}, Landroid/util/Slog;->d(Ljava/lang/String;Ljava/lang/String;)I

    goto/16 :goto_0

    .line 437
    :pswitch_4
    iput-boolean v5, p0, Lcom/miui/server/turbosched/TurboSchedSceneManager;->mIsSmallScreen:Z

    .line 438
    iput-boolean v5, p0, Lcom/miui/server/turbosched/TurboSchedSceneManager;->mIsMiniScreen:Z

    .line 439
    invoke-static {}, Landroid/os/TurboSchedMonitor;->getInstance()Landroid/os/TurboSchedMonitor;

    move-result-object v5

    invoke-virtual {v5}, Landroid/os/TurboSchedMonitor;->isDebugMode()Z

    move-result v5

    if-eqz v5, :cond_0

    .line 440
    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "FREEFORM_TO_FULLSCREEN:"

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v7, v4}, Landroid/util/Slog;->d(Ljava/lang/String;Ljava/lang/String;)I

    goto/16 :goto_0

    .line 421
    :pswitch_5
    iput-boolean v6, p0, Lcom/miui/server/turbosched/TurboSchedSceneManager;->mIsSmallScreen:Z

    .line 422
    iput-boolean v6, p0, Lcom/miui/server/turbosched/TurboSchedSceneManager;->mIsMiniScreen:Z

    .line 423
    invoke-static {}, Landroid/os/TurboSchedMonitor;->getInstance()Landroid/os/TurboSchedMonitor;

    move-result-object v5

    invoke-virtual {v5}, Landroid/os/TurboSchedMonitor;->isDebugMode()Z

    move-result v5

    if-eqz v5, :cond_0

    .line 424
    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "FREEFORM_TO_MINIFREEFORM:"

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v7, v4}, Landroid/util/Slog;->d(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    .line 413
    :pswitch_6
    iput-boolean v6, p0, Lcom/miui/server/turbosched/TurboSchedSceneManager;->mIsSmallScreen:Z

    .line 414
    iput-boolean v6, p0, Lcom/miui/server/turbosched/TurboSchedSceneManager;->mIsMiniScreen:Z

    .line 415
    invoke-static {}, Landroid/os/TurboSchedMonitor;->getInstance()Landroid/os/TurboSchedMonitor;

    move-result-object v5

    invoke-virtual {v5}, Landroid/os/TurboSchedMonitor;->isDebugMode()Z

    move-result v5

    if-eqz v5, :cond_0

    .line 416
    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "FULLSCREEN_TO_MINIFREEFORM:"

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v7, v4}, Landroid/util/Slog;->d(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    .line 405
    :pswitch_7
    iput-boolean v6, p0, Lcom/miui/server/turbosched/TurboSchedSceneManager;->mIsSmallScreen:Z

    .line 406
    iput-boolean v5, p0, Lcom/miui/server/turbosched/TurboSchedSceneManager;->mIsMiniScreen:Z

    .line 407
    invoke-static {}, Landroid/os/TurboSchedMonitor;->getInstance()Landroid/os/TurboSchedMonitor;

    move-result-object v5

    invoke-virtual {v5}, Landroid/os/TurboSchedMonitor;->isDebugMode()Z

    move-result v5

    if-eqz v5, :cond_0

    .line 408
    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "FULLSCREEN_TO_FREEFORM:"

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v7, v4}, Landroid/util/Slog;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 460
    :cond_0
    :goto_0
    invoke-direct {p0, v2, v3}, Lcom/miui/server/turbosched/TurboSchedSceneManager;->setPkgWindowState(Ljava/lang/String;I)V

    .line 461
    return-void

    nop

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_7
        :pswitch_6
        :pswitch_5
        :pswitch_4
        :pswitch_3
        :pswitch_2
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_1
        :pswitch_1
        :pswitch_1
        :pswitch_1
    .end packed-switch
.end method

.method private handleTaskStackChange()V
    .locals 2

    .line 354
    invoke-direct {p0}, Lcom/miui/server/turbosched/TurboSchedSceneManager;->isInSplitScreenMode()Z

    move-result v0

    iput-boolean v0, p0, Lcom/miui/server/turbosched/TurboSchedSceneManager;->mIsSplitScreen:Z

    .line 355
    invoke-direct {p0}, Lcom/miui/server/turbosched/TurboSchedSceneManager;->isGameMode()Z

    move-result v0

    iput-boolean v0, p0, Lcom/miui/server/turbosched/TurboSchedSceneManager;->mIsGameMode:Z

    .line 356
    invoke-static {}, Landroid/os/TurboSchedMonitor;->getInstance()Landroid/os/TurboSchedMonitor;

    move-result-object v0

    invoke-virtual {v0}, Landroid/os/TurboSchedMonitor;->isDebugMode()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 357
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "onTaskStackChanged, splitScreenMode: "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-boolean v1, p0, Lcom/miui/server/turbosched/TurboSchedSceneManager;->mIsSplitScreen:Z

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, " SmallScreen: "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-boolean v1, p0, Lcom/miui/server/turbosched/TurboSchedSceneManager;->mIsSmallScreen:Z

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, " GameMode:"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-boolean v1, p0, Lcom/miui/server/turbosched/TurboSchedSceneManager;->mIsGameMode:Z

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    const-string/jumbo v1, "tsched_scene"

    invoke-static {v1, v0}, Landroid/util/Slog;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 360
    :cond_0
    return-void
.end method

.method private handleWindowChanged(Lmiui/process/ForegroundInfo;)V
    .locals 3
    .param p1, "foregroundInfo"    # Lmiui/process/ForegroundInfo;

    .line 363
    iget-object v0, p1, Lmiui/process/ForegroundInfo;->mForegroundPackageName:Ljava/lang/String;

    .line 365
    .local v0, "packageName":Ljava/lang/String;
    const-string v1, "com.lbe.security.miui"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 366
    return-void

    .line 369
    :cond_0
    iget-object v1, p0, Lcom/miui/server/turbosched/TurboSchedSceneManager;->mForegroundAppName:Ljava/lang/String;

    invoke-static {v1, v0}, Landroid/text/TextUtils;->equals(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Z

    move-result v1

    if-nez v1, :cond_2

    .line 370
    invoke-static {}, Landroid/os/TurboSchedMonitor;->getInstance()Landroid/os/TurboSchedMonitor;

    move-result-object v1

    invoke-virtual {v1}, Landroid/os/TurboSchedMonitor;->isDebugMode()Z

    move-result v1

    if-eqz v1, :cond_1

    .line 371
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v2, "windowChanged: fg pkg from "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lcom/miui/server/turbosched/TurboSchedSceneManager;->mForegroundAppName:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " to "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    const-string/jumbo v2, "tsched_scene"

    invoke-static {v2, v1}, Landroid/util/Slog;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 373
    :cond_1
    iput-object v0, p0, Lcom/miui/server/turbosched/TurboSchedSceneManager;->mForegroundAppName:Ljava/lang/String;

    .line 375
    :cond_2
    return-void
.end method

.method private isGameMode()Z
    .locals 4

    .line 275
    iget-object v0, p0, Lcom/miui/server/turbosched/TurboSchedSceneManager;->mContext:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    const/4 v1, -0x2

    const-string v2, "gb_boosting"

    const/4 v3, 0x0

    invoke-static {v0, v2, v3, v1}, Landroid/provider/Settings$Secure;->getIntForUser(Landroid/content/ContentResolver;Ljava/lang/String;II)I

    move-result v0

    const/4 v1, 0x1

    if-ne v0, v1, :cond_0

    move v3, v1

    :cond_0
    move v0, v3

    .line 277
    .local v0, "ret":Z
    return v0
.end method

.method private isInSplitScreenMode()Z
    .locals 6

    .line 259
    const/4 v0, 0x0

    :try_start_0
    const-string v1, "android.app.ActivityTaskManager"

    invoke-static {v1}, Ljava/lang/Class;->forName(Ljava/lang/String;)Ljava/lang/Class;

    move-result-object v1

    .line 260
    .local v1, "clazz":Ljava/lang/Class;, "Ljava/lang/Class<*>;"
    const-string v2, "getService"

    new-array v3, v0, [Ljava/lang/Class;

    invoke-virtual {v1, v2, v3}, Ljava/lang/Class;->getMethod(Ljava/lang/String;[Ljava/lang/Class;)Ljava/lang/reflect/Method;

    move-result-object v2

    new-array v3, v0, [Ljava/lang/Object;

    const/4 v4, 0x0

    invoke-virtual {v2, v4, v3}, Ljava/lang/reflect/Method;->invoke(Ljava/lang/Object;[Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v2

    .line 261
    .local v2, "obj":Ljava/lang/Object;
    invoke-virtual {v2}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v3

    const-string v4, "isInSplitScreenWindowingMode"

    new-array v5, v0, [Ljava/lang/Class;

    invoke-virtual {v3, v4, v5}, Ljava/lang/Class;->getMethod(Ljava/lang/String;[Ljava/lang/Class;)Ljava/lang/reflect/Method;

    move-result-object v3

    .line 262
    .local v3, "method":Ljava/lang/reflect/Method;
    new-array v4, v0, [Ljava/lang/Object;

    invoke-virtual {v3, v2, v4}, Ljava/lang/reflect/Method;->invoke(Ljava/lang/Object;[Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Ljava/lang/Boolean;

    invoke-virtual {v4}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    return v0

    .line 263
    .end local v1    # "clazz":Ljava/lang/Class;, "Ljava/lang/Class<*>;"
    .end local v2    # "obj":Ljava/lang/Object;
    .end local v3    # "method":Ljava/lang/reflect/Method;
    :catch_0
    move-exception v1

    .line 264
    .local v1, "e":Ljava/lang/Exception;
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "getStackInfo exception : "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    const-string/jumbo v3, "tsched_scene"

    invoke-static {v3, v2}, Landroid/util/Slog;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 266
    .end local v1    # "e":Ljava/lang/Exception;
    return v0
.end method

.method private makeTurboSchedDesion()V
    .locals 3

    .line 284
    invoke-direct {p0}, Lcom/miui/server/turbosched/TurboSchedSceneManager;->isGameMode()Z

    move-result v0

    .line 285
    .local v0, "gameMode":Z
    iget-boolean v1, p0, Lcom/miui/server/turbosched/TurboSchedSceneManager;->mIsGameMode:Z

    if-eq v0, v1, :cond_1

    .line 286
    invoke-static {}, Landroid/os/TurboSchedMonitor;->getInstance()Landroid/os/TurboSchedMonitor;

    move-result-object v1

    invoke-virtual {v1}, Landroid/os/TurboSchedMonitor;->isDebugMode()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 287
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "make turbo sched desion, GameMode: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-boolean v2, p0, Lcom/miui/server/turbosched/TurboSchedSceneManager;->mIsGameMode:Z

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    const-string/jumbo v2, "tsched_scene"

    invoke-static {v2, v1}, Landroid/util/Slog;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 289
    :cond_0
    iput-boolean v0, p0, Lcom/miui/server/turbosched/TurboSchedSceneManager;->mIsGameMode:Z

    .line 290
    invoke-direct {p0, v0}, Lcom/miui/server/turbosched/TurboSchedSceneManager;->notifyInGameMode(Z)V

    .line 292
    :cond_1
    return-void
.end method

.method private notifyInGameMode(Z)V
    .locals 5
    .param p1, "state"    # Z

    .line 215
    :try_start_0
    iget-object v0, p0, Lcom/miui/server/turbosched/TurboSchedSceneManager;->mLock:Ljava/lang/Object;

    monitor-enter v0
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    .line 216
    :try_start_1
    iget-object v1, p0, Lcom/miui/server/turbosched/TurboSchedSceneManager;->mAppStateListeners:Landroid/util/ArrayMap;

    invoke-virtual {v1}, Landroid/util/ArrayMap;->entrySet()Ljava/util/Set;

    move-result-object v1

    invoke-interface {v1}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v1

    .line 217
    .local v1, "iterator":Ljava/util/Iterator;, "Ljava/util/Iterator<Ljava/util/Map$Entry<Ljava/lang/String;Lmiui/turbosched/ITurboSchedManager$ITurboSchedStateChangeCallback;>;>;"
    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_1

    .line 218
    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/util/Map$Entry;

    .line 219
    .local v2, "entry":Ljava/util/Map$Entry;, "Ljava/util/Map$Entry<Ljava/lang/String;Lmiui/turbosched/ITurboSchedManager$ITurboSchedStateChangeCallback;>;"
    invoke-interface {v2}, Ljava/util/Map$Entry;->getKey()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/lang/String;

    .line 220
    .local v3, "pkgName":Ljava/lang/String;
    invoke-interface {v2}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lmiui/turbosched/ITurboSchedManager$ITurboSchedStateChangeCallback;

    .line 221
    .local v4, "listener":Lmiui/turbosched/ITurboSchedManager$ITurboSchedStateChangeCallback;
    if-eqz v4, :cond_0

    .line 222
    invoke-interface {v4, p1}, Lmiui/turbosched/ITurboSchedManager$ITurboSchedStateChangeCallback;->onGameModeChange(Z)V

    .line 224
    .end local v2    # "entry":Ljava/util/Map$Entry;, "Ljava/util/Map$Entry<Ljava/lang/String;Lmiui/turbosched/ITurboSchedManager$ITurboSchedStateChangeCallback;>;"
    .end local v3    # "pkgName":Ljava/lang/String;
    .end local v4    # "listener":Lmiui/turbosched/ITurboSchedManager$ITurboSchedStateChangeCallback;
    :cond_0
    goto :goto_0

    .line 225
    .end local v1    # "iterator":Ljava/util/Iterator;, "Ljava/util/Iterator<Ljava/util/Map$Entry<Ljava/lang/String;Lmiui/turbosched/ITurboSchedManager$ITurboSchedStateChangeCallback;>;>;"
    :cond_1
    monitor-exit v0

    .line 228
    goto :goto_1

    .line 225
    :catchall_0
    move-exception v1

    monitor-exit v0
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .end local p0    # "this":Lcom/miui/server/turbosched/TurboSchedSceneManager;
    .end local p1    # "state":Z
    :try_start_2
    throw v1
    :try_end_2
    .catch Landroid/os/RemoteException; {:try_start_2 .. :try_end_2} :catch_0

    .line 226
    .restart local p0    # "this":Lcom/miui/server/turbosched/TurboSchedSceneManager;
    .restart local p1    # "state":Z
    :catch_0
    move-exception v0

    .line 227
    .local v0, "e":Landroid/os/RemoteException;
    const-string/jumbo v1, "tsched_scene"

    const-string/jumbo v2, "setFocusedPkgState catch remoteException"

    invoke-static {v1, v2, v0}, Landroid/util/Slog;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    .line 229
    .end local v0    # "e":Landroid/os/RemoteException;
    :goto_1
    return-void
.end method

.method private onBootComplete()V
    .locals 0

    .line 123
    invoke-direct {p0}, Lcom/miui/server/turbosched/TurboSchedSceneManager;->registerCommonListener()V

    .line 124
    return-void
.end method

.method private registerCommonListener()V
    .locals 3

    .line 131
    :try_start_0
    iget-object v0, p0, Lcom/miui/server/turbosched/TurboSchedSceneManager;->mActivityTaskManager:Landroid/app/IActivityTaskManager;

    iget-object v1, p0, Lcom/miui/server/turbosched/TurboSchedSceneManager;->mTaskStackListener:Landroid/app/TaskStackListener;

    invoke-interface {v0, v1}, Landroid/app/IActivityTaskManager;->registerTaskStackListener(Landroid/app/ITaskStackListener;)V

    .line 132
    iget-object v0, p0, Lcom/miui/server/turbosched/TurboSchedSceneManager;->mWindowListener:Lmiui/process/IForegroundWindowListener;

    invoke-static {v0}, Lmiui/process/ProcessManager;->registerForegroundWindowListener(Lmiui/process/IForegroundWindowListener;)V

    .line 133
    iget-object v0, p0, Lcom/miui/server/turbosched/TurboSchedSceneManager;->mFreeformCallback:Lmiui/app/IFreeformCallback;

    invoke-static {v0}, Lmiui/app/MiuiFreeFormManager;->registerFreeformCallback(Lmiui/app/IFreeformCallback;)V
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    .line 136
    goto :goto_0

    .line 134
    :catch_0
    move-exception v0

    .line 135
    .local v0, "e":Landroid/os/RemoteException;
    const-string/jumbo v1, "tsched_scene"

    const-string v2, "register common listener failed"

    invoke-static {v1, v2, v0}, Landroid/util/Slog;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    .line 137
    .end local v0    # "e":Landroid/os/RemoteException;
    :goto_0
    return-void
.end method

.method private setFocusedPkgState(Ljava/lang/String;Z)V
    .locals 3
    .param p1, "pkgName"    # Ljava/lang/String;
    .param p2, "isFocused"    # Z

    .line 202
    :try_start_0
    iget-object v0, p0, Lcom/miui/server/turbosched/TurboSchedSceneManager;->mLock:Ljava/lang/Object;

    monitor-enter v0
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    .line 203
    :try_start_1
    iget-object v1, p0, Lcom/miui/server/turbosched/TurboSchedSceneManager;->mAppStateListeners:Landroid/util/ArrayMap;

    invoke-virtual {v1, p1}, Landroid/util/ArrayMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lmiui/turbosched/ITurboSchedManager$ITurboSchedStateChangeCallback;

    .line 204
    .local v1, "listener":Lmiui/turbosched/ITurboSchedManager$ITurboSchedStateChangeCallback;
    if-eqz v1, :cond_0

    .line 205
    invoke-interface {v1, p1, p2}, Lmiui/turbosched/ITurboSchedManager$ITurboSchedStateChangeCallback;->onFocusedWindowChange(Ljava/lang/String;Z)V

    .line 207
    .end local v1    # "listener":Lmiui/turbosched/ITurboSchedManager$ITurboSchedStateChangeCallback;
    :cond_0
    monitor-exit v0

    .line 210
    goto :goto_0

    .line 207
    :catchall_0
    move-exception v1

    monitor-exit v0
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .end local p0    # "this":Lcom/miui/server/turbosched/TurboSchedSceneManager;
    .end local p1    # "pkgName":Ljava/lang/String;
    .end local p2    # "isFocused":Z
    :try_start_2
    throw v1
    :try_end_2
    .catch Landroid/os/RemoteException; {:try_start_2 .. :try_end_2} :catch_0

    .line 208
    .restart local p0    # "this":Lcom/miui/server/turbosched/TurboSchedSceneManager;
    .restart local p1    # "pkgName":Ljava/lang/String;
    .restart local p2    # "isFocused":Z
    :catch_0
    move-exception v0

    .line 209
    .local v0, "e":Landroid/os/RemoteException;
    const-string/jumbo v1, "tsched_scene"

    const-string/jumbo v2, "setFocusedPkgState catch remoteException"

    invoke-static {v1, v2, v0}, Landroid/util/Slog;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    .line 211
    .end local v0    # "e":Landroid/os/RemoteException;
    :goto_0
    return-void
.end method

.method private setPkgWindowState(Ljava/lang/String;I)V
    .locals 3
    .param p1, "pkgName"    # Ljava/lang/String;
    .param p2, "windowState"    # I

    .line 189
    :try_start_0
    iget-object v0, p0, Lcom/miui/server/turbosched/TurboSchedSceneManager;->mLock:Ljava/lang/Object;

    monitor-enter v0
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    .line 190
    :try_start_1
    iget-object v1, p0, Lcom/miui/server/turbosched/TurboSchedSceneManager;->mAppStateListeners:Landroid/util/ArrayMap;

    invoke-virtual {v1, p1}, Landroid/util/ArrayMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lmiui/turbosched/ITurboSchedManager$ITurboSchedStateChangeCallback;

    .line 191
    .local v1, "listener":Lmiui/turbosched/ITurboSchedManager$ITurboSchedStateChangeCallback;
    if-eqz v1, :cond_0

    .line 192
    invoke-interface {v1, p1, p2}, Lmiui/turbosched/ITurboSchedManager$ITurboSchedStateChangeCallback;->onScreenStateChanged(Ljava/lang/String;I)V

    .line 194
    .end local v1    # "listener":Lmiui/turbosched/ITurboSchedManager$ITurboSchedStateChangeCallback;
    :cond_0
    monitor-exit v0

    .line 197
    goto :goto_0

    .line 194
    :catchall_0
    move-exception v1

    monitor-exit v0
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .end local p0    # "this":Lcom/miui/server/turbosched/TurboSchedSceneManager;
    .end local p1    # "pkgName":Ljava/lang/String;
    .end local p2    # "windowState":I
    :try_start_2
    throw v1
    :try_end_2
    .catch Landroid/os/RemoteException; {:try_start_2 .. :try_end_2} :catch_0

    .line 195
    .restart local p0    # "this":Lcom/miui/server/turbosched/TurboSchedSceneManager;
    .restart local p1    # "pkgName":Ljava/lang/String;
    .restart local p2    # "windowState":I
    :catch_0
    move-exception v0

    .line 196
    .local v0, "e":Landroid/os/RemoteException;
    const-string/jumbo v1, "tsched_scene"

    const-string/jumbo v2, "setPkgWindowState catch remoteException"

    invoke-static {v1, v2, v0}, Landroid/util/Slog;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    .line 198
    .end local v0    # "e":Landroid/os/RemoteException;
    :goto_0
    return-void
.end method


# virtual methods
.method public initialize(Landroid/content/Context;Landroid/os/Looper;)V
    .locals 2
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "looper"    # Landroid/os/Looper;

    .line 111
    iput-object p1, p0, Lcom/miui/server/turbosched/TurboSchedSceneManager;->mContext:Landroid/content/Context;

    .line 112
    new-instance v0, Landroid/content/IntentFilter;

    invoke-direct {v0}, Landroid/content/IntentFilter;-><init>()V

    .line 113
    .local v0, "intentFilter":Landroid/content/IntentFilter;
    const-string v1, "android.intent.action.BOOT_COMPLETED"

    invoke-virtual {v0, v1}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    .line 114
    iget-object v1, p0, Lcom/miui/server/turbosched/TurboSchedSceneManager;->mLifecycleReceiver:Landroid/content/BroadcastReceiver;

    invoke-virtual {p1, v1, v0}, Landroid/content/Context;->registerReceiver(Landroid/content/BroadcastReceiver;Landroid/content/IntentFilter;)Landroid/content/Intent;

    .line 115
    new-instance v1, Lcom/miui/server/turbosched/TurboSchedSceneManager$TurboSchedSceneHandler;

    invoke-direct {v1, p0, p2}, Lcom/miui/server/turbosched/TurboSchedSceneManager$TurboSchedSceneHandler;-><init>(Lcom/miui/server/turbosched/TurboSchedSceneManager;Landroid/os/Looper;)V

    iput-object v1, p0, Lcom/miui/server/turbosched/TurboSchedSceneManager;->mTbSceneHandler:Lcom/miui/server/turbosched/TurboSchedSceneManager$TurboSchedSceneHandler;

    .line 116
    return-void
.end method

.method public onFocusedWindowChangeLocked(Ljava/lang/String;I)V
    .locals 3
    .param p1, "focus"    # Ljava/lang/String;
    .param p2, "type"    # I

    .line 301
    new-instance v0, Landroid/os/Bundle;

    invoke-direct {v0}, Landroid/os/Bundle;-><init>()V

    .line 302
    .local v0, "bundle":Landroid/os/Bundle;
    const-string/jumbo v1, "type"

    invoke-virtual {v0, v1, p2}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    .line 303
    const-string v1, "focus"

    invoke-virtual {v0, v1, p1}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 304
    iget-object v1, p0, Lcom/miui/server/turbosched/TurboSchedSceneManager;->mTbSceneHandler:Lcom/miui/server/turbosched/TurboSchedSceneManager$TurboSchedSceneHandler;

    invoke-virtual {v1}, Lcom/miui/server/turbosched/TurboSchedSceneManager$TurboSchedSceneHandler;->obtainMessage()Landroid/os/Message;

    move-result-object v1

    .line 305
    .local v1, "msg":Landroid/os/Message;
    invoke-virtual {v1, v0}, Landroid/os/Message;->setData(Landroid/os/Bundle;)V

    .line 306
    const/4 v2, 0x3

    iput v2, v1, Landroid/os/Message;->what:I

    .line 307
    iget-object v2, p0, Lcom/miui/server/turbosched/TurboSchedSceneManager;->mTbSceneHandler:Lcom/miui/server/turbosched/TurboSchedSceneManager$TurboSchedSceneHandler;

    invoke-virtual {v2, v1}, Lcom/miui/server/turbosched/TurboSchedSceneManager$TurboSchedSceneHandler;->sendMessage(Landroid/os/Message;)Z

    .line 308
    return-void
.end method

.method public registerStateChangeCallback(Ljava/lang/String;Lmiui/turbosched/ITurboSchedManager$ITurboSchedStateChangeCallback;)V
    .locals 4
    .param p1, "pkgName"    # Ljava/lang/String;
    .param p2, "cb"    # Lmiui/turbosched/ITurboSchedManager$ITurboSchedStateChangeCallback;

    .line 145
    new-instance v0, Lcom/miui/server/turbosched/TurboSchedSceneManager$1;

    invoke-direct {v0, p0, p1, p2}, Lcom/miui/server/turbosched/TurboSchedSceneManager$1;-><init>(Lcom/miui/server/turbosched/TurboSchedSceneManager;Ljava/lang/String;Lmiui/turbosched/ITurboSchedManager$ITurboSchedStateChangeCallback;)V

    .line 155
    .local v0, "dr":Landroid/os/IBinder$DeathRecipient;
    :try_start_0
    invoke-interface {p2}, Lmiui/turbosched/ITurboSchedManager$ITurboSchedStateChangeCallback;->asBinder()Landroid/os/IBinder;

    move-result-object v1

    const/4 v2, 0x0

    invoke-interface {v1, v0, v2}, Landroid/os/IBinder;->linkToDeath(Landroid/os/IBinder$DeathRecipient;I)V
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    .line 159
    nop

    .line 161
    iget-object v1, p0, Lcom/miui/server/turbosched/TurboSchedSceneManager;->mLock:Ljava/lang/Object;

    monitor-enter v1

    .line 162
    :try_start_1
    iget-object v2, p0, Lcom/miui/server/turbosched/TurboSchedSceneManager;->mAppStateListeners:Landroid/util/ArrayMap;

    invoke-virtual {v2, p1, p2}, Landroid/util/ArrayMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 163
    monitor-exit v1
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 164
    invoke-static {}, Landroid/os/TurboSchedMonitor;->getInstance()Landroid/os/TurboSchedMonitor;

    move-result-object v1

    invoke-virtual {v1}, Landroid/os/TurboSchedMonitor;->isDebugMode()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 165
    const-string/jumbo v1, "tsched_scene"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "register window state callback pkg="

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, " listener size="

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget-object v3, p0, Lcom/miui/server/turbosched/TurboSchedSceneManager;->mAppStateListeners:Landroid/util/ArrayMap;

    invoke-virtual {v3}, Landroid/util/ArrayMap;->size()I

    move-result v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, ", "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget-object v3, p0, Lcom/miui/server/turbosched/TurboSchedSceneManager;->mAppStateListeners:Landroid/util/ArrayMap;

    invoke-virtual {v3, p1}, Landroid/util/ArrayMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Slog;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 167
    :cond_0
    return-void

    .line 163
    :catchall_0
    move-exception v2

    :try_start_2
    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    throw v2

    .line 156
    :catch_0
    move-exception v1

    .line 157
    .local v1, "e":Landroid/os/RemoteException;
    const-string/jumbo v2, "tsched_scene"

    const-string v3, "Error on linkToDeath - "

    invoke-static {v2, v3, v1}, Landroid/util/Slog;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    .line 158
    return-void
.end method

.method public unregisterStateChangeCallback(Ljava/lang/String;Lmiui/turbosched/ITurboSchedManager$ITurboSchedStateChangeCallback;)V
    .locals 3
    .param p1, "pkgName"    # Ljava/lang/String;
    .param p2, "cb"    # Lmiui/turbosched/ITurboSchedManager$ITurboSchedStateChangeCallback;

    .line 175
    iget-object v0, p0, Lcom/miui/server/turbosched/TurboSchedSceneManager;->mLock:Ljava/lang/Object;

    monitor-enter v0

    .line 176
    :try_start_0
    iget-object v1, p0, Lcom/miui/server/turbosched/TurboSchedSceneManager;->mAppStateListeners:Landroid/util/ArrayMap;

    invoke-virtual {v1, p1}, Landroid/util/ArrayMap;->containsKey(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 177
    iget-object v1, p0, Lcom/miui/server/turbosched/TurboSchedSceneManager;->mAppStateListeners:Landroid/util/ArrayMap;

    invoke-virtual {v1, p1}, Landroid/util/ArrayMap;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    .line 179
    :cond_0
    monitor-exit v0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 180
    invoke-static {}, Landroid/os/TurboSchedMonitor;->getInstance()Landroid/os/TurboSchedMonitor;

    move-result-object v0

    invoke-virtual {v0}, Landroid/os/TurboSchedMonitor;->isDebugMode()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 181
    const-string/jumbo v0, "tsched_scene"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v2, "unregister window state callback pkg="

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " listener size="

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lcom/miui/server/turbosched/TurboSchedSceneManager;->mAppStateListeners:Landroid/util/ArrayMap;

    invoke-virtual {v2}, Landroid/util/ArrayMap;->size()I

    move-result v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Slog;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 183
    :cond_1
    return-void

    .line 179
    :catchall_0
    move-exception v1

    :try_start_1
    monitor-exit v0
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    throw v1
.end method
