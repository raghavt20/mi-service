.class Lcom/miui/server/turbosched/TurboSchedManagerService$TurboschedSDKParam;
.super Ljava/lang/Object;
.source "TurboSchedManagerService.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/miui/server/turbosched/TurboSchedManagerService;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "TurboschedSDKParam"
.end annotation


# instance fields
.field private mSetTime:J

.field private mStartTime:J

.field private mTid:I

.field private mType:Ljava/lang/String;

.field final synthetic this$0:Lcom/miui/server/turbosched/TurboSchedManagerService;


# direct methods
.method static bridge synthetic -$$Nest$fgetmSetTime(Lcom/miui/server/turbosched/TurboSchedManagerService$TurboschedSDKParam;)J
    .locals 2

    iget-wide v0, p0, Lcom/miui/server/turbosched/TurboSchedManagerService$TurboschedSDKParam;->mSetTime:J

    return-wide v0
.end method

.method static bridge synthetic -$$Nest$fgetmStartTime(Lcom/miui/server/turbosched/TurboSchedManagerService$TurboschedSDKParam;)J
    .locals 2

    iget-wide v0, p0, Lcom/miui/server/turbosched/TurboSchedManagerService$TurboschedSDKParam;->mStartTime:J

    return-wide v0
.end method

.method static bridge synthetic -$$Nest$fgetmTid(Lcom/miui/server/turbosched/TurboSchedManagerService$TurboschedSDKParam;)I
    .locals 0

    iget p0, p0, Lcom/miui/server/turbosched/TurboSchedManagerService$TurboschedSDKParam;->mTid:I

    return p0
.end method

.method public constructor <init>(Lcom/miui/server/turbosched/TurboSchedManagerService;JJILjava/lang/String;)V
    .locals 0
    .param p2, "startTime"    # J
    .param p4, "setTime"    # J
    .param p6, "tid"    # I
    .param p7, "type"    # Ljava/lang/String;

    .line 412
    iput-object p1, p0, Lcom/miui/server/turbosched/TurboSchedManagerService$TurboschedSDKParam;->this$0:Lcom/miui/server/turbosched/TurboSchedManagerService;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 413
    iput-wide p2, p0, Lcom/miui/server/turbosched/TurboSchedManagerService$TurboschedSDKParam;->mStartTime:J

    .line 414
    iput-wide p4, p0, Lcom/miui/server/turbosched/TurboSchedManagerService$TurboschedSDKParam;->mSetTime:J

    .line 415
    iput p6, p0, Lcom/miui/server/turbosched/TurboSchedManagerService$TurboschedSDKParam;->mTid:I

    .line 416
    iput-object p7, p0, Lcom/miui/server/turbosched/TurboSchedManagerService$TurboschedSDKParam;->mType:Ljava/lang/String;

    .line 417
    return-void
.end method
