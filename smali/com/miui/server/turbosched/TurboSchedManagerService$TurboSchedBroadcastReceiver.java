class com.miui.server.turbosched.TurboSchedManagerService$TurboSchedBroadcastReceiver extends android.content.BroadcastReceiver {
	 /* .source "TurboSchedManagerService.java" */
	 /* # annotations */
	 /* .annotation system Ldalvik/annotation/EnclosingClass; */
	 /* value = Lcom/miui/server/turbosched/TurboSchedManagerService; */
} // .end annotation
/* .annotation system Ldalvik/annotation/InnerClass; */
/* accessFlags = 0x2 */
/* name = "TurboSchedBroadcastReceiver" */
} // .end annotation
/* # instance fields */
final com.miui.server.turbosched.TurboSchedManagerService this$0; //synthetic
/* # direct methods */
public com.miui.server.turbosched.TurboSchedManagerService$TurboSchedBroadcastReceiver ( ) {
/* .locals 2 */
/* .line 1969 */
this.this$0 = p1;
/* invoke-direct {p0}, Landroid/content/BroadcastReceiver;-><init>()V */
/* .line 1970 */
/* new-instance v0, Landroid/content/IntentFilter; */
/* invoke-direct {v0}, Landroid/content/IntentFilter;-><init>()V */
/* .line 1971 */
/* .local v0, "filter":Landroid/content/IntentFilter; */
final String v1 = "android.intent.action.SCREEN_ON"; // const-string v1, "android.intent.action.SCREEN_ON"
(( android.content.IntentFilter ) v0 ).addAction ( v1 ); // invoke-virtual {v0, v1}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V
/* .line 1972 */
final String v1 = "android.intent.action.SCREEN_OFF"; // const-string v1, "android.intent.action.SCREEN_OFF"
(( android.content.IntentFilter ) v0 ).addAction ( v1 ); // invoke-virtual {v0, v1}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V
/* .line 1973 */
com.miui.server.turbosched.TurboSchedManagerService .-$$Nest$fgetmContext ( p1 );
(( android.content.Context ) p1 ).registerReceiver ( p0, v0 ); // invoke-virtual {p1, p0, v0}, Landroid/content/Context;->registerReceiver(Landroid/content/BroadcastReceiver;Landroid/content/IntentFilter;)Landroid/content/Intent;
/* .line 1974 */
return;
} // .end method
/* # virtual methods */
public void onReceive ( android.content.Context p0, android.content.Intent p1 ) {
/* .locals 3 */
/* .param p1, "context" # Landroid/content/Context; */
/* .param p2, "intent" # Landroid/content/Intent; */
/* .line 1978 */
(( android.content.Intent ) p2 ).getAction ( ); // invoke-virtual {p2}, Landroid/content/Intent;->getAction()Ljava/lang/String;
/* .line 1979 */
/* .local v0, "action":Ljava/lang/String; */
final String v1 = "android.intent.action.SCREEN_ON"; // const-string v1, "android.intent.action.SCREEN_ON"
v1 = (( java.lang.String ) v0 ).equals ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z
if ( v1 != null) { // if-eqz v1, :cond_0
	 /* .line 1980 */
	 v1 = this.this$0;
	 int v2 = 1; // const/4 v2, 0x1
	 com.miui.server.turbosched.TurboSchedManagerService .-$$Nest$fputmScreenOn ( v1,v2 );
	 /* .line 1981 */
} // :cond_0
final String v1 = "android.intent.action.SCREEN_OFF"; // const-string v1, "android.intent.action.SCREEN_OFF"
v1 = (( java.lang.String ) v0 ).equals ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z
if ( v1 != null) { // if-eqz v1, :cond_1
	 /* .line 1982 */
	 v1 = this.this$0;
	 int v2 = 0; // const/4 v2, 0x0
	 com.miui.server.turbosched.TurboSchedManagerService .-$$Nest$fputmScreenOn ( v1,v2 );
	 /* .line 1984 */
} // :cond_1
} // :goto_0
return;
} // .end method
