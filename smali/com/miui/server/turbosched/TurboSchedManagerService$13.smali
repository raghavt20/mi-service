.class Lcom/miui/server/turbosched/TurboSchedManagerService$13;
.super Ljava/util/TimerTask;
.source "TurboSchedManagerService.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/miui/server/turbosched/TurboSchedManagerService;->setPolicyTimeoutChecker(Ljava/lang/String;[IJLjava/lang/String;Ljava/util/Map;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/miui/server/turbosched/TurboSchedManagerService;

.field final synthetic val$cancelPath:Ljava/lang/String;

.field final synthetic val$logTag:Ljava/lang/String;


# direct methods
.method constructor <init>(Lcom/miui/server/turbosched/TurboSchedManagerService;Ljava/lang/String;Ljava/lang/String;)V
    .locals 0
    .param p1, "this$0"    # Lcom/miui/server/turbosched/TurboSchedManagerService;

    .line 2017
    iput-object p1, p0, Lcom/miui/server/turbosched/TurboSchedManagerService$13;->this$0:Lcom/miui/server/turbosched/TurboSchedManagerService;

    iput-object p2, p0, Lcom/miui/server/turbosched/TurboSchedManagerService$13;->val$cancelPath:Ljava/lang/String;

    iput-object p3, p0, Lcom/miui/server/turbosched/TurboSchedManagerService$13;->val$logTag:Ljava/lang/String;

    invoke-direct {p0}, Ljava/util/TimerTask;-><init>()V

    return-void
.end method


# virtual methods
.method public run()V
    .locals 10

    .line 2020
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    .line 2021
    .local v0, "tidsList":Ljava/util/List;, "Ljava/util/List<Ljava/lang/Integer;>;"
    iget-object v1, p0, Lcom/miui/server/turbosched/TurboSchedManagerService$13;->this$0:Lcom/miui/server/turbosched/TurboSchedManagerService;

    invoke-static {v1}, Lcom/miui/server/turbosched/TurboSchedManagerService;->-$$Nest$fgetmTurboschedSdkTidsLock(Lcom/miui/server/turbosched/TurboSchedManagerService;)Ljava/lang/Object;

    move-result-object v1

    monitor-enter v1

    .line 2022
    :try_start_0
    iget-object v2, p0, Lcom/miui/server/turbosched/TurboSchedManagerService$13;->this$0:Lcom/miui/server/turbosched/TurboSchedManagerService;

    invoke-static {v2}, Lcom/miui/server/turbosched/TurboSchedManagerService;->-$$Nest$fgetmTurboschedSDKParamMap(Lcom/miui/server/turbosched/TurboSchedManagerService;)Ljava/util/Map;

    move-result-object v2

    invoke-interface {v2}, Ljava/util/Map;->entrySet()Ljava/util/Set;

    move-result-object v2

    invoke-interface {v2}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v2

    .line 2023
    .local v2, "iterator":Ljava/util/Iterator;, "Ljava/util/Iterator<Ljava/util/Map$Entry<Ljava/lang/String;Lcom/miui/server/turbosched/TurboSchedManagerService$TurboschedSDKParam;>;>;"
    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_1

    .line 2024
    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/util/Map$Entry;

    .line 2025
    .local v3, "entry":Ljava/util/Map$Entry;, "Ljava/util/Map$Entry<Ljava/lang/String;Lcom/miui/server/turbosched/TurboSchedManagerService$TurboschedSDKParam;>;"
    invoke-interface {v3}, Ljava/util/Map$Entry;->getKey()Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Ljava/lang/String;

    .line 2026
    .local v4, "paramId":Ljava/lang/String;
    iget-object v5, p0, Lcom/miui/server/turbosched/TurboSchedManagerService$13;->this$0:Lcom/miui/server/turbosched/TurboSchedManagerService;

    invoke-static {v5}, Lcom/miui/server/turbosched/TurboSchedManagerService;->-$$Nest$fgetmTurboschedSDKParamMap(Lcom/miui/server/turbosched/TurboSchedManagerService;)Ljava/util/Map;

    move-result-object v5

    invoke-interface {v5, v4}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Lcom/miui/server/turbosched/TurboSchedManagerService$TurboschedSDKParam;

    .line 2027
    .local v5, "param":Lcom/miui/server/turbosched/TurboSchedManagerService$TurboschedSDKParam;
    if-eqz v5, :cond_0

    invoke-static {v5}, Lcom/miui/server/turbosched/TurboSchedManagerService$TurboschedSDKParam;->-$$Nest$fgetmStartTime(Lcom/miui/server/turbosched/TurboSchedManagerService$TurboschedSDKParam;)J

    move-result-wide v6

    invoke-static {v5}, Lcom/miui/server/turbosched/TurboSchedManagerService$TurboschedSDKParam;->-$$Nest$fgetmSetTime(Lcom/miui/server/turbosched/TurboSchedManagerService$TurboschedSDKParam;)J

    move-result-wide v8

    add-long/2addr v6, v8

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v8

    cmp-long v6, v6, v8

    if-gtz v6, :cond_0

    .line 2028
    invoke-static {v5}, Lcom/miui/server/turbosched/TurboSchedManagerService$TurboschedSDKParam;->-$$Nest$fgetmTid(Lcom/miui/server/turbosched/TurboSchedManagerService$TurboschedSDKParam;)I

    move-result v6

    invoke-static {v6}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v6

    invoke-interface {v0, v6}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 2029
    invoke-interface {v2}, Ljava/util/Iterator;->remove()V

    .line 2031
    .end local v3    # "entry":Ljava/util/Map$Entry;, "Ljava/util/Map$Entry<Ljava/lang/String;Lcom/miui/server/turbosched/TurboSchedManagerService$TurboschedSDKParam;>;"
    .end local v4    # "paramId":Ljava/lang/String;
    .end local v5    # "param":Lcom/miui/server/turbosched/TurboSchedManagerService$TurboschedSDKParam;
    :cond_0
    goto :goto_0

    .line 2033
    :cond_1
    invoke-interface {v0}, Ljava/util/List;->isEmpty()Z

    move-result v3

    if-nez v3, :cond_2

    .line 2034
    invoke-interface {v0}, Ljava/util/List;->stream()Ljava/util/stream/Stream;

    move-result-object v3

    new-instance v4, Lcom/android/server/audio/AudioServiceStubImpl$$ExternalSyntheticLambda1;

    invoke-direct {v4}, Lcom/android/server/audio/AudioServiceStubImpl$$ExternalSyntheticLambda1;-><init>()V

    invoke-interface {v3, v4}, Ljava/util/stream/Stream;->mapToInt(Ljava/util/function/ToIntFunction;)Ljava/util/stream/IntStream;

    move-result-object v3

    invoke-interface {v3}, Ljava/util/stream/IntStream;->toArray()[I

    move-result-object v3

    .line 2035
    .local v3, "newTids":[I
    iget-object v4, p0, Lcom/miui/server/turbosched/TurboSchedManagerService$13;->this$0:Lcom/miui/server/turbosched/TurboSchedManagerService;

    iget-object v5, p0, Lcom/miui/server/turbosched/TurboSchedManagerService$13;->val$cancelPath:Ljava/lang/String;

    invoke-static {v4, v5, v3}, Lcom/miui/server/turbosched/TurboSchedManagerService;->-$$Nest$mwriteTidsToPath(Lcom/miui/server/turbosched/TurboSchedManagerService;Ljava/lang/String;[I)Z

    move-result v4

    .line 2036
    .local v4, "ret":Z
    iget-object v5, p0, Lcom/miui/server/turbosched/TurboSchedManagerService$13;->this$0:Lcom/miui/server/turbosched/TurboSchedManagerService;

    invoke-static {v5}, Lcom/miui/server/turbosched/TurboSchedManagerService;->-$$Nest$fgetmHistoryLog(Lcom/miui/server/turbosched/TurboSchedManagerService;)Landroid/util/LocalLog;

    move-result-object v5

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string v7, "TS ["

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    iget-object v7, p0, Lcom/miui/server/turbosched/TurboSchedManagerService$13;->val$logTag:Ljava/lang/String;

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    const-string v7, "] : success: "

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6, v4}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v6

    const-string v7, ", tids: "

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-static {v3}, Ljava/util/Arrays;->toString([I)Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6}, Landroid/util/LocalLog;->log(Ljava/lang/String;)V

    .line 2039
    .end local v3    # "newTids":[I
    .end local v4    # "ret":Z
    :cond_2
    iget-object v3, p0, Lcom/miui/server/turbosched/TurboSchedManagerService$13;->this$0:Lcom/miui/server/turbosched/TurboSchedManagerService;

    invoke-static {v3}, Lcom/miui/server/turbosched/TurboSchedManagerService;->-$$Nest$fgetmTurboschedSDKParamMap(Lcom/miui/server/turbosched/TurboSchedManagerService;)Ljava/util/Map;

    move-result-object v3

    if-eqz v3, :cond_3

    iget-object v3, p0, Lcom/miui/server/turbosched/TurboSchedManagerService$13;->this$0:Lcom/miui/server/turbosched/TurboSchedManagerService;

    invoke-static {v3}, Lcom/miui/server/turbosched/TurboSchedManagerService;->-$$Nest$fgetmTurboschedSDKParamMap(Lcom/miui/server/turbosched/TurboSchedManagerService;)Ljava/util/Map;

    move-result-object v3

    invoke-interface {v3}, Ljava/util/Map;->isEmpty()Z

    move-result v3

    if-eqz v3, :cond_5

    .line 2040
    :cond_3
    iget-object v3, p0, Lcom/miui/server/turbosched/TurboSchedManagerService$13;->this$0:Lcom/miui/server/turbosched/TurboSchedManagerService;

    iget-object v3, v3, Lcom/miui/server/turbosched/TurboSchedManagerService;->TurboSchedSDKTimerTask:Ljava/util/TimerTask;

    const/4 v4, 0x0

    if-eqz v3, :cond_4

    .line 2041
    iget-object v3, p0, Lcom/miui/server/turbosched/TurboSchedManagerService$13;->this$0:Lcom/miui/server/turbosched/TurboSchedManagerService;

    iget-object v3, v3, Lcom/miui/server/turbosched/TurboSchedManagerService;->TurboSchedSDKTimerTask:Ljava/util/TimerTask;

    invoke-virtual {v3}, Ljava/util/TimerTask;->cancel()Z

    .line 2042
    iget-object v3, p0, Lcom/miui/server/turbosched/TurboSchedManagerService$13;->this$0:Lcom/miui/server/turbosched/TurboSchedManagerService;

    iput-object v4, v3, Lcom/miui/server/turbosched/TurboSchedManagerService;->TurboSchedSDKTimerTask:Ljava/util/TimerTask;

    .line 2044
    :cond_4
    iget-object v3, p0, Lcom/miui/server/turbosched/TurboSchedManagerService$13;->this$0:Lcom/miui/server/turbosched/TurboSchedManagerService;

    iget-object v3, v3, Lcom/miui/server/turbosched/TurboSchedManagerService;->TurboSchedSDKTimer:Ljava/util/Timer;

    if-eqz v3, :cond_5

    .line 2045
    iget-object v3, p0, Lcom/miui/server/turbosched/TurboSchedManagerService$13;->this$0:Lcom/miui/server/turbosched/TurboSchedManagerService;

    iget-object v3, v3, Lcom/miui/server/turbosched/TurboSchedManagerService;->TurboSchedSDKTimer:Ljava/util/Timer;

    invoke-virtual {v3}, Ljava/util/Timer;->cancel()V

    .line 2046
    iget-object v3, p0, Lcom/miui/server/turbosched/TurboSchedManagerService$13;->this$0:Lcom/miui/server/turbosched/TurboSchedManagerService;

    iput-object v4, v3, Lcom/miui/server/turbosched/TurboSchedManagerService;->TurboSchedSDKTimer:Ljava/util/Timer;

    .line 2049
    .end local v2    # "iterator":Ljava/util/Iterator;, "Ljava/util/Iterator<Ljava/util/Map$Entry<Ljava/lang/String;Lcom/miui/server/turbosched/TurboSchedManagerService$TurboschedSDKParam;>;>;"
    :cond_5
    monitor-exit v1

    .line 2050
    return-void

    .line 2049
    :catchall_0
    move-exception v2

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v2
.end method
