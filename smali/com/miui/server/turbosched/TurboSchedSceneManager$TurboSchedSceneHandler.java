class com.miui.server.turbosched.TurboSchedSceneManager$TurboSchedSceneHandler extends android.os.Handler {
	 /* .source "TurboSchedSceneManager.java" */
	 /* # annotations */
	 /* .annotation system Ldalvik/annotation/EnclosingClass; */
	 /* value = Lcom/miui/server/turbosched/TurboSchedSceneManager; */
} // .end annotation
/* .annotation system Ldalvik/annotation/InnerClass; */
/* accessFlags = 0x0 */
/* name = "TurboSchedSceneHandler" */
} // .end annotation
/* # instance fields */
final com.miui.server.turbosched.TurboSchedSceneManager this$0; //synthetic
/* # direct methods */
public com.miui.server.turbosched.TurboSchedSceneManager$TurboSchedSceneHandler ( ) {
/* .locals 0 */
/* .param p1, "this$0" # Lcom/miui/server/turbosched/TurboSchedSceneManager; */
/* .param p2, "looper" # Landroid/os/Looper; */
/* .line 465 */
this.this$0 = p1;
/* .line 466 */
/* invoke-direct {p0, p2}, Landroid/os/Handler;-><init>(Landroid/os/Looper;)V */
/* .line 467 */
return;
} // .end method
/* # virtual methods */
public void handleMessage ( android.os.Message p0 ) {
/* .locals 3 */
/* .param p1, "msg" # Landroid/os/Message; */
/* .line 471 */
/* iget v0, p1, Landroid/os/Message;->what:I */
/* .line 472 */
/* .local v0, "what":I */
/* sparse-switch v0, :sswitch_data_0 */
/* .line 487 */
/* :sswitch_0 */
v1 = this.this$0;
com.miui.server.turbosched.TurboSchedSceneManager .-$$Nest$mhandleFreeformModeChanged ( v1,p1 );
/* .line 488 */
/* .line 480 */
/* :sswitch_1 */
v1 = this.this$0;
com.miui.server.turbosched.TurboSchedSceneManager .-$$Nest$mhandleFocusAppChange ( v1,p1 );
/* .line 481 */
/* .line 477 */
/* :sswitch_2 */
v1 = this.this$0;
v2 = this.obj;
/* check-cast v2, Lmiui/process/ForegroundInfo; */
com.miui.server.turbosched.TurboSchedSceneManager .-$$Nest$mhandleWindowChanged ( v1,v2 );
/* .line 478 */
/* .line 475 */
/* :sswitch_3 */
/* nop */
/* .line 492 */
} // :goto_0
v1 = this.this$0;
com.miui.server.turbosched.TurboSchedSceneManager .-$$Nest$mmakeTurboSchedDesion ( v1 );
/* .line 493 */
return;
/* nop */
/* :sswitch_data_0 */
/* .sparse-switch */
/* 0x1 -> :sswitch_3 */
/* 0x2 -> :sswitch_2 */
/* 0x3 -> :sswitch_1 */
/* 0xb -> :sswitch_0 */
} // .end sparse-switch
} // .end method
