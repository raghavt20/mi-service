.class final Lcom/miui/server/turbosched/TurboSchedManagerService$TurboSchedHandler;
.super Landroid/os/Handler;
.source "TurboSchedManagerService.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/miui/server/turbosched/TurboSchedManagerService;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x12
    name = "TurboSchedHandler"
.end annotation


# static fields
.field static final MSG_TRACE_BEGIN:I = 0x0

.field static final MSG_TRACE_END:I = 0x1


# instance fields
.field final synthetic this$0:Lcom/miui/server/turbosched/TurboSchedManagerService;


# direct methods
.method private constructor <init>(Lcom/miui/server/turbosched/TurboSchedManagerService;Landroid/os/Looper;)V
    .locals 0
    .param p2, "looper"    # Landroid/os/Looper;

    .line 554
    iput-object p1, p0, Lcom/miui/server/turbosched/TurboSchedManagerService$TurboSchedHandler;->this$0:Lcom/miui/server/turbosched/TurboSchedManagerService;

    .line 555
    invoke-direct {p0, p2}, Landroid/os/Handler;-><init>(Landroid/os/Looper;)V

    .line 556
    return-void
.end method

.method synthetic constructor <init>(Lcom/miui/server/turbosched/TurboSchedManagerService;Landroid/os/Looper;Lcom/miui/server/turbosched/TurboSchedManagerService$TurboSchedHandler-IA;)V
    .locals 0

    invoke-direct {p0, p1, p2}, Lcom/miui/server/turbosched/TurboSchedManagerService$TurboSchedHandler;-><init>(Lcom/miui/server/turbosched/TurboSchedManagerService;Landroid/os/Looper;)V

    return-void
.end method

.method private traceBegin(Ljava/lang/String;Ljava/lang/String;J)V
    .locals 5
    .param p1, "name"    # Ljava/lang/String;
    .param p2, "desc"    # Ljava/lang/String;
    .param p3, "stratTime"    # J

    .line 580
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v0

    sub-long/2addr v0, p3

    .line 581
    .local v0, "delay":J
    if-eqz p2, :cond_0

    invoke-virtual {p2}, Ljava/lang/String;->isEmpty()Z

    move-result v2

    if-nez v2, :cond_0

    .line 582
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "("

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, ")"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p2

    .line 584
    :cond_0
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, " delay="

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v0, v1}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    .line 585
    .local v2, "tag":Ljava/lang/String;
    const-wide/16 v3, 0x40

    invoke-static {v3, v4, v2}, Landroid/os/Trace;->traceBegin(JLjava/lang/String;)V

    .line 586
    return-void
.end method

.method private traceEnd()V
    .locals 2

    .line 589
    const-wide/16 v0, 0x40

    invoke-static {v0, v1}, Landroid/os/Trace;->traceEnd(J)V

    .line 590
    return-void
.end method


# virtual methods
.method public handleMessage(Landroid/os/Message;)V
    .locals 8
    .param p1, "msg"    # Landroid/os/Message;

    .line 560
    iget v0, p1, Landroid/os/Message;->what:I

    .line 561
    .local v0, "what":I
    const-wide/16 v1, 0x40

    packed-switch v0, :pswitch_data_0

    goto :goto_0

    .line 570
    :pswitch_0
    invoke-static {v1, v2}, Landroid/os/Trace;->traceEnd(J)V

    .line 571
    invoke-direct {p0}, Lcom/miui/server/turbosched/TurboSchedManagerService$TurboSchedHandler;->traceEnd()V

    .line 572
    goto :goto_0

    .line 563
    :pswitch_1
    iget-object v3, p1, Landroid/os/Message;->obj:Ljava/lang/Object;

    check-cast v3, Lcom/miui/server/turbosched/TurboSchedManagerService$TraceInfo;

    .line 564
    .local v3, "info":Lcom/miui/server/turbosched/TurboSchedManagerService$TraceInfo;
    iget-object v4, v3, Lcom/miui/server/turbosched/TurboSchedManagerService$TraceInfo;->name:Ljava/lang/String;

    iget-object v5, v3, Lcom/miui/server/turbosched/TurboSchedManagerService$TraceInfo;->desc:Ljava/lang/String;

    iget-wide v6, v3, Lcom/miui/server/turbosched/TurboSchedManagerService$TraceInfo;->startTime:J

    invoke-direct {p0, v4, v5, v6, v7}, Lcom/miui/server/turbosched/TurboSchedManagerService$TurboSchedHandler;->traceBegin(Ljava/lang/String;Ljava/lang/String;J)V

    .line 565
    iget v4, p1, Landroid/os/Message;->arg1:I

    .line 566
    .local v4, "tid":I
    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "P-Tid:"

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v1, v2, v5}, Landroid/os/Trace;->traceBegin(JLjava/lang/String;)V

    .line 567
    nop

    .line 577
    .end local v3    # "info":Lcom/miui/server/turbosched/TurboSchedManagerService$TraceInfo;
    .end local v4    # "tid":I
    :goto_0
    return-void

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_1
        :pswitch_0
    .end packed-switch
.end method
