.class public final Lcom/miui/server/turbosched/TurboSchedManagerService$Lifecycle;
.super Lcom/android/server/SystemService;
.source "TurboSchedManagerService.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/miui/server/turbosched/TurboSchedManagerService;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "Lifecycle"
.end annotation


# instance fields
.field private final mService:Lcom/miui/server/turbosched/TurboSchedManagerService;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 1
    .param p1, "context"    # Landroid/content/Context;

    .line 395
    invoke-direct {p0, p1}, Lcom/android/server/SystemService;-><init>(Landroid/content/Context;)V

    .line 396
    new-instance v0, Lcom/miui/server/turbosched/TurboSchedManagerService;

    invoke-direct {v0, p1}, Lcom/miui/server/turbosched/TurboSchedManagerService;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Lcom/miui/server/turbosched/TurboSchedManagerService$Lifecycle;->mService:Lcom/miui/server/turbosched/TurboSchedManagerService;

    .line 397
    return-void
.end method


# virtual methods
.method public onStart()V
    .locals 2

    .line 401
    const-string/jumbo v0, "turbosched"

    iget-object v1, p0, Lcom/miui/server/turbosched/TurboSchedManagerService$Lifecycle;->mService:Lcom/miui/server/turbosched/TurboSchedManagerService;

    invoke-virtual {p0, v0, v1}, Lcom/miui/server/turbosched/TurboSchedManagerService$Lifecycle;->publishBinderService(Ljava/lang/String;Landroid/os/IBinder;)V

    .line 402
    return-void
.end method
