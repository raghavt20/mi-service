public class com.miui.server.turbosched.TurboSchedSceneManager {
	 /* .source "TurboSchedSceneManager.java" */
	 /* # annotations */
	 /* .annotation system Ldalvik/annotation/MemberClasses; */
	 /* value = { */
	 /* Lcom/miui/server/turbosched/TurboSchedSceneManager$TurboSchedSceneManagerHolder;, */
	 /* Lcom/miui/server/turbosched/TurboSchedSceneManager$TurboSchedSceneHandler; */
	 /* } */
} // .end annotation
/* # static fields */
private static final java.lang.String ACTIVITYTASKMANAGER;
private static final Integer CURR_FOCUSED;
private static final Integer FULL_SCREEN;
private static final java.lang.String KEY_GAME_BOOSTER;
private static final Integer LAST_FOCUSED;
private static final java.lang.String METHOD_GETSERVICE;
private static final Integer MINI_SCREEN;
private static final Integer MSG_FREEFORM_MODE_CHANGED;
private static final Integer MSG_ON_FOCUSED_APP_CHANGED;
private static final Integer MSG_ON_FOREGROUND_APP_CHANGED;
private static final Integer MSG_ON_TASK_STACK_CHANGE;
private static final java.lang.String PERMISSION_PKG_NAME;
private static final Integer SMALL_SCREEN;
private static final java.lang.String TAG;
/* # instance fields */
private android.app.IActivityTaskManager mActivityTaskManager;
private android.util.ArrayMap mAppStateListeners;
/* .annotation system Ldalvik/annotation/Signature; */
/* value = { */
/* "Landroid/util/ArrayMap<", */
/* "Ljava/lang/String;", */
/* "Lmiui/turbosched/ITurboSchedManager$ITurboSchedStateChangeCallback;", */
/* ">;" */
/* } */
} // .end annotation
} // .end field
private android.content.Context mContext;
private java.lang.String mCurFocusedPkgName;
private java.lang.String mForegroundAppName;
private miui.app.IFreeformCallback mFreeformCallback;
private Boolean mIsGameMode;
private Boolean mIsMiniScreen;
private Boolean mIsSmallScreen;
private Boolean mIsSplitScreen;
private java.lang.String mLastFocusedPkgName;
private final android.content.BroadcastReceiver mLifecycleReceiver;
private final java.lang.Object mLock;
private final android.app.TaskStackListener mTaskStackListener;
private com.miui.server.turbosched.TurboSchedSceneManager$TurboSchedSceneHandler mTbSceneHandler;
private final miui.process.IForegroundWindowListener mWindowListener;
/* # direct methods */
static com.miui.server.turbosched.TurboSchedSceneManager$TurboSchedSceneHandler -$$Nest$fgetmTbSceneHandler ( com.miui.server.turbosched.TurboSchedSceneManager p0 ) { //bridge//synthethic
/* .locals 0 */
p0 = this.mTbSceneHandler;
} // .end method
static void -$$Nest$mhandleFocusAppChange ( com.miui.server.turbosched.TurboSchedSceneManager p0, android.os.Message p1 ) { //bridge//synthethic
/* .locals 0 */
/* invoke-direct {p0, p1}, Lcom/miui/server/turbosched/TurboSchedSceneManager;->handleFocusAppChange(Landroid/os/Message;)V */
return;
} // .end method
static void -$$Nest$mhandleFreeformModeChanged ( com.miui.server.turbosched.TurboSchedSceneManager p0, android.os.Message p1 ) { //bridge//synthethic
/* .locals 0 */
/* invoke-direct {p0, p1}, Lcom/miui/server/turbosched/TurboSchedSceneManager;->handleFreeformModeChanged(Landroid/os/Message;)V */
return;
} // .end method
static void -$$Nest$mhandleWindowChanged ( com.miui.server.turbosched.TurboSchedSceneManager p0, miui.process.ForegroundInfo p1 ) { //bridge//synthethic
/* .locals 0 */
/* invoke-direct {p0, p1}, Lcom/miui/server/turbosched/TurboSchedSceneManager;->handleWindowChanged(Lmiui/process/ForegroundInfo;)V */
return;
} // .end method
static void -$$Nest$mmakeTurboSchedDesion ( com.miui.server.turbosched.TurboSchedSceneManager p0 ) { //bridge//synthethic
/* .locals 0 */
/* invoke-direct {p0}, Lcom/miui/server/turbosched/TurboSchedSceneManager;->makeTurboSchedDesion()V */
return;
} // .end method
static void -$$Nest$monBootComplete ( com.miui.server.turbosched.TurboSchedSceneManager p0 ) { //bridge//synthethic
/* .locals 0 */
/* invoke-direct {p0}, Lcom/miui/server/turbosched/TurboSchedSceneManager;->onBootComplete()V */
return;
} // .end method
private com.miui.server.turbosched.TurboSchedSceneManager ( ) {
/* .locals 1 */
/* .line 100 */
/* invoke-direct {p0}, Ljava/lang/Object;-><init>()V */
/* .line 64 */
int v0 = 0; // const/4 v0, 0x0
/* iput-boolean v0, p0, Lcom/miui/server/turbosched/TurboSchedSceneManager;->mIsSmallScreen:Z */
/* .line 65 */
/* iput-boolean v0, p0, Lcom/miui/server/turbosched/TurboSchedSceneManager;->mIsMiniScreen:Z */
/* .line 66 */
/* iput-boolean v0, p0, Lcom/miui/server/turbosched/TurboSchedSceneManager;->mIsSplitScreen:Z */
/* .line 67 */
/* iput-boolean v0, p0, Lcom/miui/server/turbosched/TurboSchedSceneManager;->mIsGameMode:Z */
/* .line 68 */
final String v0 = ""; // const-string v0, ""
this.mLastFocusedPkgName = v0;
/* .line 69 */
this.mCurFocusedPkgName = v0;
/* .line 78 */
/* new-instance v0, Ljava/lang/Object; */
/* invoke-direct {v0}, Ljava/lang/Object;-><init>()V */
this.mLock = v0;
/* .line 79 */
int v0 = 0; // const/4 v0, 0x0
this.mAppStateListeners = v0;
/* .line 234 */
/* new-instance v0, Lcom/miui/server/turbosched/TurboSchedSceneManager$2; */
/* invoke-direct {v0, p0}, Lcom/miui/server/turbosched/TurboSchedSceneManager$2;-><init>(Lcom/miui/server/turbosched/TurboSchedSceneManager;)V */
this.mLifecycleReceiver = v0;
/* .line 314 */
/* new-instance v0, Lcom/miui/server/turbosched/TurboSchedSceneManager$3; */
/* invoke-direct {v0, p0}, Lcom/miui/server/turbosched/TurboSchedSceneManager$3;-><init>(Lcom/miui/server/turbosched/TurboSchedSceneManager;)V */
this.mTaskStackListener = v0;
/* .line 327 */
/* new-instance v0, Lcom/miui/server/turbosched/TurboSchedSceneManager$4; */
/* invoke-direct {v0, p0}, Lcom/miui/server/turbosched/TurboSchedSceneManager$4;-><init>(Lcom/miui/server/turbosched/TurboSchedSceneManager;)V */
this.mWindowListener = v0;
/* .line 340 */
/* new-instance v0, Lcom/miui/server/turbosched/TurboSchedSceneManager$5; */
/* invoke-direct {v0, p0}, Lcom/miui/server/turbosched/TurboSchedSceneManager$5;-><init>(Lcom/miui/server/turbosched/TurboSchedSceneManager;)V */
this.mFreeformCallback = v0;
/* .line 101 */
/* new-instance v0, Landroid/util/ArrayMap; */
/* invoke-direct {v0}, Landroid/util/ArrayMap;-><init>()V */
this.mAppStateListeners = v0;
/* .line 102 */
android.app.ActivityTaskManager .getService ( );
this.mActivityTaskManager = v0;
/* .line 103 */
return;
} // .end method
 com.miui.server.turbosched.TurboSchedSceneManager ( ) { //synthethic
/* .locals 0 */
/* invoke-direct {p0}, Lcom/miui/server/turbosched/TurboSchedSceneManager;-><init>()V */
return;
} // .end method
public static com.miui.server.turbosched.TurboSchedSceneManager getInstance ( ) {
/* .locals 1 */
/* .line 97 */
v0 = com.miui.server.turbosched.TurboSchedSceneManager$TurboSchedSceneManagerHolder.sInstance;
} // .end method
private void handleFocusAppChange ( android.os.Message p0 ) {
/* .locals 4 */
/* .param p1, "msg" # Landroid/os/Message; */
/* .line 378 */
(( android.os.Message ) p1 ).getData ( ); // invoke-virtual {p1}, Landroid/os/Message;->getData()Landroid/os/Bundle;
/* .line 379 */
/* .local v0, "data":Landroid/os/Bundle; */
/* const-string/jumbo v1, "type" */
v1 = (( android.os.Bundle ) v0 ).getInt ( v1 ); // invoke-virtual {v0, v1}, Landroid/os/Bundle;->getInt(Ljava/lang/String;)I
/* .line 380 */
/* .local v1, "type":I */
final String v2 = "focus"; // const-string v2, "focus"
(( android.os.Bundle ) v0 ).getString ( v2 ); // invoke-virtual {v0, v2}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;
/* .line 381 */
/* .local v2, "focus":Ljava/lang/String; */
/* packed-switch v1, :pswitch_data_0 */
/* .line 387 */
/* :pswitch_0 */
this.mCurFocusedPkgName = v2;
/* .line 388 */
int v3 = 1; // const/4 v3, 0x1
/* invoke-direct {p0, v2, v3}, Lcom/miui/server/turbosched/TurboSchedSceneManager;->setFocusedPkgState(Ljava/lang/String;Z)V */
/* .line 389 */
/* .line 383 */
/* :pswitch_1 */
this.mLastFocusedPkgName = v2;
/* .line 384 */
int v3 = 0; // const/4 v3, 0x0
/* invoke-direct {p0, v2, v3}, Lcom/miui/server/turbosched/TurboSchedSceneManager;->setFocusedPkgState(Ljava/lang/String;Z)V */
/* .line 385 */
/* nop */
/* .line 393 */
} // :goto_0
return;
/* :pswitch_data_0 */
/* .packed-switch 0x0 */
/* :pswitch_1 */
/* :pswitch_0 */
} // .end packed-switch
} // .end method
private void handleFreeformModeChanged ( android.os.Message p0 ) {
/* .locals 8 */
/* .param p1, "msg" # Landroid/os/Message; */
/* .line 396 */
(( android.os.Message ) p1 ).getData ( ); // invoke-virtual {p1}, Landroid/os/Message;->getData()Landroid/os/Bundle;
/* .line 397 */
/* .local v0, "data":Landroid/os/Bundle; */
final String v1 = "action"; // const-string v1, "action"
v1 = (( android.os.Bundle ) v0 ).getInt ( v1 ); // invoke-virtual {v0, v1}, Landroid/os/Bundle;->getInt(Ljava/lang/String;)I
/* .line 398 */
/* .local v1, "action":I */
final String v2 = "pkgName"; // const-string v2, "pkgName"
(( android.os.Bundle ) v0 ).getString ( v2 ); // invoke-virtual {v0, v2}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;
/* .line 399 */
/* .local v2, "pkgName":Ljava/lang/String; */
/* const-string/jumbo v3, "windowState" */
v3 = (( android.os.Bundle ) v0 ).getInt ( v3 ); // invoke-virtual {v0, v3}, Landroid/os/Bundle;->getInt(Ljava/lang/String;)I
/* .line 400 */
/* .local v3, "windowState":I */
final String v4 = ", state:"; // const-string v4, ", state:"
int v5 = 0; // const/4 v5, 0x0
int v6 = 1; // const/4 v6, 0x1
/* const-string/jumbo v7, "tsched_scene" */
/* packed-switch v1, :pswitch_data_0 */
/* .line 457 */
/* :pswitch_0 */
/* new-instance v4, Ljava/lang/StringBuilder; */
/* invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V */
/* const-string/jumbo v5, "warning for access here action=" */
(( java.lang.StringBuilder ) v4 ).append ( v5 ); // invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v4 ).append ( v1 ); // invoke-virtual {v4, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;
final String v5 = ", windowstate="; // const-string v5, ", windowstate="
(( java.lang.StringBuilder ) v4 ).append ( v5 ); // invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v4 ).append ( v3 ); // invoke-virtual {v4, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v4 ).toString ( ); // invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
android.util.Slog .w ( v7,v4 );
/* .line 458 */
return;
/* .line 455 */
/* :pswitch_1 */
/* goto/16 :goto_0 */
/* .line 445 */
/* :pswitch_2 */
/* iput-boolean v5, p0, Lcom/miui/server/turbosched/TurboSchedSceneManager;->mIsSmallScreen:Z */
/* .line 446 */
/* iput-boolean v5, p0, Lcom/miui/server/turbosched/TurboSchedSceneManager;->mIsMiniScreen:Z */
/* .line 447 */
android.os.TurboSchedMonitor .getInstance ( );
v5 = (( android.os.TurboSchedMonitor ) v5 ).isDebugMode ( ); // invoke-virtual {v5}, Landroid/os/TurboSchedMonitor;->isDebugMode()Z
if ( v5 != null) { // if-eqz v5, :cond_0
/* .line 448 */
/* new-instance v5, Ljava/lang/StringBuilder; */
/* invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V */
final String v6 = "MINIFREEFORM_TO_FULLSCREEN:"; // const-string v6, "MINIFREEFORM_TO_FULLSCREEN:"
(( java.lang.StringBuilder ) v5 ).append ( v6 ); // invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v5 ).append ( v2 ); // invoke-virtual {v5, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v5 ).append ( v4 ); // invoke-virtual {v5, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v4 ).append ( v3 ); // invoke-virtual {v4, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v4 ).toString ( ); // invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
android.util.Slog .d ( v7,v4 );
/* goto/16 :goto_0 */
/* .line 429 */
/* :pswitch_3 */
/* iput-boolean v6, p0, Lcom/miui/server/turbosched/TurboSchedSceneManager;->mIsSmallScreen:Z */
/* .line 430 */
/* iput-boolean v5, p0, Lcom/miui/server/turbosched/TurboSchedSceneManager;->mIsMiniScreen:Z */
/* .line 431 */
android.os.TurboSchedMonitor .getInstance ( );
v5 = (( android.os.TurboSchedMonitor ) v5 ).isDebugMode ( ); // invoke-virtual {v5}, Landroid/os/TurboSchedMonitor;->isDebugMode()Z
if ( v5 != null) { // if-eqz v5, :cond_0
/* .line 432 */
/* new-instance v5, Ljava/lang/StringBuilder; */
/* invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V */
final String v6 = "MINIFREEFORM_TO_FREEFORM:"; // const-string v6, "MINIFREEFORM_TO_FREEFORM:"
(( java.lang.StringBuilder ) v5 ).append ( v6 ); // invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v5 ).append ( v2 ); // invoke-virtual {v5, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v5 ).append ( v4 ); // invoke-virtual {v5, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v4 ).append ( v3 ); // invoke-virtual {v4, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v4 ).toString ( ); // invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
android.util.Slog .d ( v7,v4 );
/* goto/16 :goto_0 */
/* .line 437 */
/* :pswitch_4 */
/* iput-boolean v5, p0, Lcom/miui/server/turbosched/TurboSchedSceneManager;->mIsSmallScreen:Z */
/* .line 438 */
/* iput-boolean v5, p0, Lcom/miui/server/turbosched/TurboSchedSceneManager;->mIsMiniScreen:Z */
/* .line 439 */
android.os.TurboSchedMonitor .getInstance ( );
v5 = (( android.os.TurboSchedMonitor ) v5 ).isDebugMode ( ); // invoke-virtual {v5}, Landroid/os/TurboSchedMonitor;->isDebugMode()Z
if ( v5 != null) { // if-eqz v5, :cond_0
/* .line 440 */
/* new-instance v5, Ljava/lang/StringBuilder; */
/* invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V */
final String v6 = "FREEFORM_TO_FULLSCREEN:"; // const-string v6, "FREEFORM_TO_FULLSCREEN:"
(( java.lang.StringBuilder ) v5 ).append ( v6 ); // invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v5 ).append ( v2 ); // invoke-virtual {v5, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v5 ).append ( v4 ); // invoke-virtual {v5, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v4 ).append ( v3 ); // invoke-virtual {v4, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v4 ).toString ( ); // invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
android.util.Slog .d ( v7,v4 );
/* goto/16 :goto_0 */
/* .line 421 */
/* :pswitch_5 */
/* iput-boolean v6, p0, Lcom/miui/server/turbosched/TurboSchedSceneManager;->mIsSmallScreen:Z */
/* .line 422 */
/* iput-boolean v6, p0, Lcom/miui/server/turbosched/TurboSchedSceneManager;->mIsMiniScreen:Z */
/* .line 423 */
android.os.TurboSchedMonitor .getInstance ( );
v5 = (( android.os.TurboSchedMonitor ) v5 ).isDebugMode ( ); // invoke-virtual {v5}, Landroid/os/TurboSchedMonitor;->isDebugMode()Z
if ( v5 != null) { // if-eqz v5, :cond_0
	 /* .line 424 */
	 /* new-instance v5, Ljava/lang/StringBuilder; */
	 /* invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V */
	 final String v6 = "FREEFORM_TO_MINIFREEFORM:"; // const-string v6, "FREEFORM_TO_MINIFREEFORM:"
	 (( java.lang.StringBuilder ) v5 ).append ( v6 ); // invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
	 (( java.lang.StringBuilder ) v5 ).append ( v2 ); // invoke-virtual {v5, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
	 (( java.lang.StringBuilder ) v5 ).append ( v4 ); // invoke-virtual {v5, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
	 (( java.lang.StringBuilder ) v4 ).append ( v3 ); // invoke-virtual {v4, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;
	 (( java.lang.StringBuilder ) v4 ).toString ( ); // invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
	 android.util.Slog .d ( v7,v4 );
	 /* .line 413 */
	 /* :pswitch_6 */
	 /* iput-boolean v6, p0, Lcom/miui/server/turbosched/TurboSchedSceneManager;->mIsSmallScreen:Z */
	 /* .line 414 */
	 /* iput-boolean v6, p0, Lcom/miui/server/turbosched/TurboSchedSceneManager;->mIsMiniScreen:Z */
	 /* .line 415 */
	 android.os.TurboSchedMonitor .getInstance ( );
	 v5 = 	 (( android.os.TurboSchedMonitor ) v5 ).isDebugMode ( ); // invoke-virtual {v5}, Landroid/os/TurboSchedMonitor;->isDebugMode()Z
	 if ( v5 != null) { // if-eqz v5, :cond_0
		 /* .line 416 */
		 /* new-instance v5, Ljava/lang/StringBuilder; */
		 /* invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V */
		 final String v6 = "FULLSCREEN_TO_MINIFREEFORM:"; // const-string v6, "FULLSCREEN_TO_MINIFREEFORM:"
		 (( java.lang.StringBuilder ) v5 ).append ( v6 ); // invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
		 (( java.lang.StringBuilder ) v5 ).append ( v2 ); // invoke-virtual {v5, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
		 (( java.lang.StringBuilder ) v5 ).append ( v4 ); // invoke-virtual {v5, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
		 (( java.lang.StringBuilder ) v4 ).append ( v3 ); // invoke-virtual {v4, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;
		 (( java.lang.StringBuilder ) v4 ).toString ( ); // invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
		 android.util.Slog .d ( v7,v4 );
		 /* .line 405 */
		 /* :pswitch_7 */
		 /* iput-boolean v6, p0, Lcom/miui/server/turbosched/TurboSchedSceneManager;->mIsSmallScreen:Z */
		 /* .line 406 */
		 /* iput-boolean v5, p0, Lcom/miui/server/turbosched/TurboSchedSceneManager;->mIsMiniScreen:Z */
		 /* .line 407 */
		 android.os.TurboSchedMonitor .getInstance ( );
		 v5 = 		 (( android.os.TurboSchedMonitor ) v5 ).isDebugMode ( ); // invoke-virtual {v5}, Landroid/os/TurboSchedMonitor;->isDebugMode()Z
		 if ( v5 != null) { // if-eqz v5, :cond_0
			 /* .line 408 */
			 /* new-instance v5, Ljava/lang/StringBuilder; */
			 /* invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V */
			 final String v6 = "FULLSCREEN_TO_FREEFORM:"; // const-string v6, "FULLSCREEN_TO_FREEFORM:"
			 (( java.lang.StringBuilder ) v5 ).append ( v6 ); // invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
			 (( java.lang.StringBuilder ) v5 ).append ( v2 ); // invoke-virtual {v5, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
			 (( java.lang.StringBuilder ) v5 ).append ( v4 ); // invoke-virtual {v5, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
			 (( java.lang.StringBuilder ) v4 ).append ( v3 ); // invoke-virtual {v4, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;
			 (( java.lang.StringBuilder ) v4 ).toString ( ); // invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
			 android.util.Slog .d ( v7,v4 );
			 /* .line 460 */
		 } // :cond_0
	 } // :goto_0
	 /* invoke-direct {p0, v2, v3}, Lcom/miui/server/turbosched/TurboSchedSceneManager;->setPkgWindowState(Ljava/lang/String;I)V */
	 /* .line 461 */
	 return;
	 /* nop */
	 /* :pswitch_data_0 */
	 /* .packed-switch 0x0 */
	 /* :pswitch_7 */
	 /* :pswitch_6 */
	 /* :pswitch_5 */
	 /* :pswitch_4 */
	 /* :pswitch_3 */
	 /* :pswitch_2 */
	 /* :pswitch_0 */
	 /* :pswitch_0 */
	 /* :pswitch_0 */
	 /* :pswitch_1 */
	 /* :pswitch_1 */
	 /* :pswitch_1 */
	 /* :pswitch_1 */
} // .end packed-switch
} // .end method
private void handleTaskStackChange ( ) {
/* .locals 2 */
/* .line 354 */
v0 = /* invoke-direct {p0}, Lcom/miui/server/turbosched/TurboSchedSceneManager;->isInSplitScreenMode()Z */
/* iput-boolean v0, p0, Lcom/miui/server/turbosched/TurboSchedSceneManager;->mIsSplitScreen:Z */
/* .line 355 */
v0 = /* invoke-direct {p0}, Lcom/miui/server/turbosched/TurboSchedSceneManager;->isGameMode()Z */
/* iput-boolean v0, p0, Lcom/miui/server/turbosched/TurboSchedSceneManager;->mIsGameMode:Z */
/* .line 356 */
android.os.TurboSchedMonitor .getInstance ( );
v0 = (( android.os.TurboSchedMonitor ) v0 ).isDebugMode ( ); // invoke-virtual {v0}, Landroid/os/TurboSchedMonitor;->isDebugMode()Z
if ( v0 != null) { // if-eqz v0, :cond_0
	 /* .line 357 */
	 /* new-instance v0, Ljava/lang/StringBuilder; */
	 /* invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V */
	 final String v1 = "onTaskStackChanged, splitScreenMode: "; // const-string v1, "onTaskStackChanged, splitScreenMode: "
	 (( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
	 /* iget-boolean v1, p0, Lcom/miui/server/turbosched/TurboSchedSceneManager;->mIsSplitScreen:Z */
	 (( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;
	 final String v1 = " SmallScreen: "; // const-string v1, " SmallScreen: "
	 (( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
	 /* iget-boolean v1, p0, Lcom/miui/server/turbosched/TurboSchedSceneManager;->mIsSmallScreen:Z */
	 (( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;
	 final String v1 = " GameMode:"; // const-string v1, " GameMode:"
	 (( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
	 /* iget-boolean v1, p0, Lcom/miui/server/turbosched/TurboSchedSceneManager;->mIsGameMode:Z */
	 (( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;
	 (( java.lang.StringBuilder ) v0 ).toString ( ); // invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
	 /* const-string/jumbo v1, "tsched_scene" */
	 android.util.Slog .i ( v1,v0 );
	 /* .line 360 */
} // :cond_0
return;
} // .end method
private void handleWindowChanged ( miui.process.ForegroundInfo p0 ) {
/* .locals 3 */
/* .param p1, "foregroundInfo" # Lmiui/process/ForegroundInfo; */
/* .line 363 */
v0 = this.mForegroundPackageName;
/* .line 365 */
/* .local v0, "packageName":Ljava/lang/String; */
final String v1 = "com.lbe.security.miui"; // const-string v1, "com.lbe.security.miui"
v1 = (( java.lang.String ) v0 ).equals ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z
if ( v1 != null) { // if-eqz v1, :cond_0
	 /* .line 366 */
	 return;
	 /* .line 369 */
} // :cond_0
v1 = this.mForegroundAppName;
v1 = android.text.TextUtils .equals ( v1,v0 );
/* if-nez v1, :cond_2 */
/* .line 370 */
android.os.TurboSchedMonitor .getInstance ( );
v1 = (( android.os.TurboSchedMonitor ) v1 ).isDebugMode ( ); // invoke-virtual {v1}, Landroid/os/TurboSchedMonitor;->isDebugMode()Z
if ( v1 != null) { // if-eqz v1, :cond_1
	 /* .line 371 */
	 /* new-instance v1, Ljava/lang/StringBuilder; */
	 /* invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V */
	 /* const-string/jumbo v2, "windowChanged: fg pkg from " */
	 (( java.lang.StringBuilder ) v1 ).append ( v2 ); // invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
	 v2 = this.mForegroundAppName;
	 (( java.lang.StringBuilder ) v1 ).append ( v2 ); // invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
	 final String v2 = " to "; // const-string v2, " to "
	 (( java.lang.StringBuilder ) v1 ).append ( v2 ); // invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
	 (( java.lang.StringBuilder ) v1 ).append ( v0 ); // invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
	 (( java.lang.StringBuilder ) v1 ).toString ( ); // invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
	 /* const-string/jumbo v2, "tsched_scene" */
	 android.util.Slog .d ( v2,v1 );
	 /* .line 373 */
} // :cond_1
this.mForegroundAppName = v0;
/* .line 375 */
} // :cond_2
return;
} // .end method
private Boolean isGameMode ( ) {
/* .locals 4 */
/* .line 275 */
v0 = this.mContext;
(( android.content.Context ) v0 ).getContentResolver ( ); // invoke-virtual {v0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;
int v1 = -2; // const/4 v1, -0x2
final String v2 = "gb_boosting"; // const-string v2, "gb_boosting"
int v3 = 0; // const/4 v3, 0x0
v0 = android.provider.Settings$Secure .getIntForUser ( v0,v2,v3,v1 );
int v1 = 1; // const/4 v1, 0x1
/* if-ne v0, v1, :cond_0 */
/* move v3, v1 */
} // :cond_0
/* move v0, v3 */
/* .line 277 */
/* .local v0, "ret":Z */
} // .end method
private Boolean isInSplitScreenMode ( ) {
/* .locals 6 */
/* .line 259 */
int v0 = 0; // const/4 v0, 0x0
try { // :try_start_0
final String v1 = "android.app.ActivityTaskManager"; // const-string v1, "android.app.ActivityTaskManager"
java.lang.Class .forName ( v1 );
/* .line 260 */
/* .local v1, "clazz":Ljava/lang/Class;, "Ljava/lang/Class<*>;" */
final String v2 = "getService"; // const-string v2, "getService"
/* new-array v3, v0, [Ljava/lang/Class; */
(( java.lang.Class ) v1 ).getMethod ( v2, v3 ); // invoke-virtual {v1, v2, v3}, Ljava/lang/Class;->getMethod(Ljava/lang/String;[Ljava/lang/Class;)Ljava/lang/reflect/Method;
/* new-array v3, v0, [Ljava/lang/Object; */
int v4 = 0; // const/4 v4, 0x0
(( java.lang.reflect.Method ) v2 ).invoke ( v4, v3 ); // invoke-virtual {v2, v4, v3}, Ljava/lang/reflect/Method;->invoke(Ljava/lang/Object;[Ljava/lang/Object;)Ljava/lang/Object;
/* .line 261 */
/* .local v2, "obj":Ljava/lang/Object; */
(( java.lang.Object ) v2 ).getClass ( ); // invoke-virtual {v2}, Ljava/lang/Object;->getClass()Ljava/lang/Class;
final String v4 = "isInSplitScreenWindowingMode"; // const-string v4, "isInSplitScreenWindowingMode"
/* new-array v5, v0, [Ljava/lang/Class; */
(( java.lang.Class ) v3 ).getMethod ( v4, v5 ); // invoke-virtual {v3, v4, v5}, Ljava/lang/Class;->getMethod(Ljava/lang/String;[Ljava/lang/Class;)Ljava/lang/reflect/Method;
/* .line 262 */
/* .local v3, "method":Ljava/lang/reflect/Method; */
/* new-array v4, v0, [Ljava/lang/Object; */
(( java.lang.reflect.Method ) v3 ).invoke ( v2, v4 ); // invoke-virtual {v3, v2, v4}, Ljava/lang/reflect/Method;->invoke(Ljava/lang/Object;[Ljava/lang/Object;)Ljava/lang/Object;
/* check-cast v4, Ljava/lang/Boolean; */
v0 = (( java.lang.Boolean ) v4 ).booleanValue ( ); // invoke-virtual {v4}, Ljava/lang/Boolean;->booleanValue()Z
/* :try_end_0 */
/* .catch Ljava/lang/Exception; {:try_start_0 ..:try_end_0} :catch_0 */
/* .line 263 */
} // .end local v1 # "clazz":Ljava/lang/Class;, "Ljava/lang/Class<*>;"
} // .end local v2 # "obj":Ljava/lang/Object;
} // .end local v3 # "method":Ljava/lang/reflect/Method;
/* :catch_0 */
/* move-exception v1 */
/* .line 264 */
/* .local v1, "e":Ljava/lang/Exception; */
/* new-instance v2, Ljava/lang/StringBuilder; */
/* invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V */
final String v3 = "getStackInfo exception : "; // const-string v3, "getStackInfo exception : "
(( java.lang.StringBuilder ) v2 ).append ( v3 ); // invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v2 ).append ( v1 ); // invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v2 ).toString ( ); // invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
/* const-string/jumbo v3, "tsched_scene" */
android.util.Slog .e ( v3,v2 );
/* .line 266 */
} // .end local v1 # "e":Ljava/lang/Exception;
} // .end method
private void makeTurboSchedDesion ( ) {
/* .locals 3 */
/* .line 284 */
v0 = /* invoke-direct {p0}, Lcom/miui/server/turbosched/TurboSchedSceneManager;->isGameMode()Z */
/* .line 285 */
/* .local v0, "gameMode":Z */
/* iget-boolean v1, p0, Lcom/miui/server/turbosched/TurboSchedSceneManager;->mIsGameMode:Z */
/* if-eq v0, v1, :cond_1 */
/* .line 286 */
android.os.TurboSchedMonitor .getInstance ( );
v1 = (( android.os.TurboSchedMonitor ) v1 ).isDebugMode ( ); // invoke-virtual {v1}, Landroid/os/TurboSchedMonitor;->isDebugMode()Z
if ( v1 != null) { // if-eqz v1, :cond_0
/* .line 287 */
/* new-instance v1, Ljava/lang/StringBuilder; */
/* invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V */
final String v2 = "make turbo sched desion, GameMode: "; // const-string v2, "make turbo sched desion, GameMode: "
(( java.lang.StringBuilder ) v1 ).append ( v2 ); // invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
/* iget-boolean v2, p0, Lcom/miui/server/turbosched/TurboSchedSceneManager;->mIsGameMode:Z */
(( java.lang.StringBuilder ) v1 ).append ( v2 ); // invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v1 ).toString ( ); // invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
/* const-string/jumbo v2, "tsched_scene" */
android.util.Slog .i ( v2,v1 );
/* .line 289 */
} // :cond_0
/* iput-boolean v0, p0, Lcom/miui/server/turbosched/TurboSchedSceneManager;->mIsGameMode:Z */
/* .line 290 */
/* invoke-direct {p0, v0}, Lcom/miui/server/turbosched/TurboSchedSceneManager;->notifyInGameMode(Z)V */
/* .line 292 */
} // :cond_1
return;
} // .end method
private void notifyInGameMode ( Boolean p0 ) {
/* .locals 5 */
/* .param p1, "state" # Z */
/* .line 215 */
try { // :try_start_0
v0 = this.mLock;
/* monitor-enter v0 */
/* :try_end_0 */
/* .catch Landroid/os/RemoteException; {:try_start_0 ..:try_end_0} :catch_0 */
/* .line 216 */
try { // :try_start_1
v1 = this.mAppStateListeners;
(( android.util.ArrayMap ) v1 ).entrySet ( ); // invoke-virtual {v1}, Landroid/util/ArrayMap;->entrySet()Ljava/util/Set;
/* .line 217 */
/* .local v1, "iterator":Ljava/util/Iterator;, "Ljava/util/Iterator<Ljava/util/Map$Entry<Ljava/lang/String;Lmiui/turbosched/ITurboSchedManager$ITurboSchedStateChangeCallback;>;>;" */
v2 = } // :goto_0
if ( v2 != null) { // if-eqz v2, :cond_1
/* .line 218 */
/* check-cast v2, Ljava/util/Map$Entry; */
/* .line 219 */
/* .local v2, "entry":Ljava/util/Map$Entry;, "Ljava/util/Map$Entry<Ljava/lang/String;Lmiui/turbosched/ITurboSchedManager$ITurboSchedStateChangeCallback;>;" */
/* check-cast v3, Ljava/lang/String; */
/* .line 220 */
/* .local v3, "pkgName":Ljava/lang/String; */
/* check-cast v4, Lmiui/turbosched/ITurboSchedManager$ITurboSchedStateChangeCallback; */
/* .line 221 */
/* .local v4, "listener":Lmiui/turbosched/ITurboSchedManager$ITurboSchedStateChangeCallback; */
if ( v4 != null) { // if-eqz v4, :cond_0
/* .line 222 */
/* .line 224 */
} // .end local v2 # "entry":Ljava/util/Map$Entry;, "Ljava/util/Map$Entry<Ljava/lang/String;Lmiui/turbosched/ITurboSchedManager$ITurboSchedStateChangeCallback;>;"
} // .end local v3 # "pkgName":Ljava/lang/String;
} // .end local v4 # "listener":Lmiui/turbosched/ITurboSchedManager$ITurboSchedStateChangeCallback;
} // :cond_0
/* .line 225 */
} // .end local v1 # "iterator":Ljava/util/Iterator;, "Ljava/util/Iterator<Ljava/util/Map$Entry<Ljava/lang/String;Lmiui/turbosched/ITurboSchedManager$ITurboSchedStateChangeCallback;>;>;"
} // :cond_1
/* monitor-exit v0 */
/* .line 228 */
/* .line 225 */
/* :catchall_0 */
/* move-exception v1 */
/* monitor-exit v0 */
/* :try_end_1 */
/* .catchall {:try_start_1 ..:try_end_1} :catchall_0 */
} // .end local p0 # "this":Lcom/miui/server/turbosched/TurboSchedSceneManager;
} // .end local p1 # "state":Z
try { // :try_start_2
/* throw v1 */
/* :try_end_2 */
/* .catch Landroid/os/RemoteException; {:try_start_2 ..:try_end_2} :catch_0 */
/* .line 226 */
/* .restart local p0 # "this":Lcom/miui/server/turbosched/TurboSchedSceneManager; */
/* .restart local p1 # "state":Z */
/* :catch_0 */
/* move-exception v0 */
/* .line 227 */
/* .local v0, "e":Landroid/os/RemoteException; */
/* const-string/jumbo v1, "tsched_scene" */
/* const-string/jumbo v2, "setFocusedPkgState catch remoteException" */
android.util.Slog .e ( v1,v2,v0 );
/* .line 229 */
} // .end local v0 # "e":Landroid/os/RemoteException;
} // :goto_1
return;
} // .end method
private void onBootComplete ( ) {
/* .locals 0 */
/* .line 123 */
/* invoke-direct {p0}, Lcom/miui/server/turbosched/TurboSchedSceneManager;->registerCommonListener()V */
/* .line 124 */
return;
} // .end method
private void registerCommonListener ( ) {
/* .locals 3 */
/* .line 131 */
try { // :try_start_0
v0 = this.mActivityTaskManager;
v1 = this.mTaskStackListener;
/* .line 132 */
v0 = this.mWindowListener;
miui.process.ProcessManager .registerForegroundWindowListener ( v0 );
/* .line 133 */
v0 = this.mFreeformCallback;
miui.app.MiuiFreeFormManager .registerFreeformCallback ( v0 );
/* :try_end_0 */
/* .catch Landroid/os/RemoteException; {:try_start_0 ..:try_end_0} :catch_0 */
/* .line 136 */
/* .line 134 */
/* :catch_0 */
/* move-exception v0 */
/* .line 135 */
/* .local v0, "e":Landroid/os/RemoteException; */
/* const-string/jumbo v1, "tsched_scene" */
final String v2 = "register common listener failed"; // const-string v2, "register common listener failed"
android.util.Slog .e ( v1,v2,v0 );
/* .line 137 */
} // .end local v0 # "e":Landroid/os/RemoteException;
} // :goto_0
return;
} // .end method
private void setFocusedPkgState ( java.lang.String p0, Boolean p1 ) {
/* .locals 3 */
/* .param p1, "pkgName" # Ljava/lang/String; */
/* .param p2, "isFocused" # Z */
/* .line 202 */
try { // :try_start_0
v0 = this.mLock;
/* monitor-enter v0 */
/* :try_end_0 */
/* .catch Landroid/os/RemoteException; {:try_start_0 ..:try_end_0} :catch_0 */
/* .line 203 */
try { // :try_start_1
v1 = this.mAppStateListeners;
(( android.util.ArrayMap ) v1 ).get ( p1 ); // invoke-virtual {v1, p1}, Landroid/util/ArrayMap;->get(Ljava/lang/Object;)Ljava/lang/Object;
/* check-cast v1, Lmiui/turbosched/ITurboSchedManager$ITurboSchedStateChangeCallback; */
/* .line 204 */
/* .local v1, "listener":Lmiui/turbosched/ITurboSchedManager$ITurboSchedStateChangeCallback; */
if ( v1 != null) { // if-eqz v1, :cond_0
/* .line 205 */
/* .line 207 */
} // .end local v1 # "listener":Lmiui/turbosched/ITurboSchedManager$ITurboSchedStateChangeCallback;
} // :cond_0
/* monitor-exit v0 */
/* .line 210 */
/* .line 207 */
/* :catchall_0 */
/* move-exception v1 */
/* monitor-exit v0 */
/* :try_end_1 */
/* .catchall {:try_start_1 ..:try_end_1} :catchall_0 */
} // .end local p0 # "this":Lcom/miui/server/turbosched/TurboSchedSceneManager;
} // .end local p1 # "pkgName":Ljava/lang/String;
} // .end local p2 # "isFocused":Z
try { // :try_start_2
/* throw v1 */
/* :try_end_2 */
/* .catch Landroid/os/RemoteException; {:try_start_2 ..:try_end_2} :catch_0 */
/* .line 208 */
/* .restart local p0 # "this":Lcom/miui/server/turbosched/TurboSchedSceneManager; */
/* .restart local p1 # "pkgName":Ljava/lang/String; */
/* .restart local p2 # "isFocused":Z */
/* :catch_0 */
/* move-exception v0 */
/* .line 209 */
/* .local v0, "e":Landroid/os/RemoteException; */
/* const-string/jumbo v1, "tsched_scene" */
/* const-string/jumbo v2, "setFocusedPkgState catch remoteException" */
android.util.Slog .e ( v1,v2,v0 );
/* .line 211 */
} // .end local v0 # "e":Landroid/os/RemoteException;
} // :goto_0
return;
} // .end method
private void setPkgWindowState ( java.lang.String p0, Integer p1 ) {
/* .locals 3 */
/* .param p1, "pkgName" # Ljava/lang/String; */
/* .param p2, "windowState" # I */
/* .line 189 */
try { // :try_start_0
v0 = this.mLock;
/* monitor-enter v0 */
/* :try_end_0 */
/* .catch Landroid/os/RemoteException; {:try_start_0 ..:try_end_0} :catch_0 */
/* .line 190 */
try { // :try_start_1
v1 = this.mAppStateListeners;
(( android.util.ArrayMap ) v1 ).get ( p1 ); // invoke-virtual {v1, p1}, Landroid/util/ArrayMap;->get(Ljava/lang/Object;)Ljava/lang/Object;
/* check-cast v1, Lmiui/turbosched/ITurboSchedManager$ITurboSchedStateChangeCallback; */
/* .line 191 */
/* .local v1, "listener":Lmiui/turbosched/ITurboSchedManager$ITurboSchedStateChangeCallback; */
if ( v1 != null) { // if-eqz v1, :cond_0
/* .line 192 */
/* .line 194 */
} // .end local v1 # "listener":Lmiui/turbosched/ITurboSchedManager$ITurboSchedStateChangeCallback;
} // :cond_0
/* monitor-exit v0 */
/* .line 197 */
/* .line 194 */
/* :catchall_0 */
/* move-exception v1 */
/* monitor-exit v0 */
/* :try_end_1 */
/* .catchall {:try_start_1 ..:try_end_1} :catchall_0 */
} // .end local p0 # "this":Lcom/miui/server/turbosched/TurboSchedSceneManager;
} // .end local p1 # "pkgName":Ljava/lang/String;
} // .end local p2 # "windowState":I
try { // :try_start_2
/* throw v1 */
/* :try_end_2 */
/* .catch Landroid/os/RemoteException; {:try_start_2 ..:try_end_2} :catch_0 */
/* .line 195 */
/* .restart local p0 # "this":Lcom/miui/server/turbosched/TurboSchedSceneManager; */
/* .restart local p1 # "pkgName":Ljava/lang/String; */
/* .restart local p2 # "windowState":I */
/* :catch_0 */
/* move-exception v0 */
/* .line 196 */
/* .local v0, "e":Landroid/os/RemoteException; */
/* const-string/jumbo v1, "tsched_scene" */
/* const-string/jumbo v2, "setPkgWindowState catch remoteException" */
android.util.Slog .e ( v1,v2,v0 );
/* .line 198 */
} // .end local v0 # "e":Landroid/os/RemoteException;
} // :goto_0
return;
} // .end method
/* # virtual methods */
public void initialize ( android.content.Context p0, android.os.Looper p1 ) {
/* .locals 2 */
/* .param p1, "context" # Landroid/content/Context; */
/* .param p2, "looper" # Landroid/os/Looper; */
/* .line 111 */
this.mContext = p1;
/* .line 112 */
/* new-instance v0, Landroid/content/IntentFilter; */
/* invoke-direct {v0}, Landroid/content/IntentFilter;-><init>()V */
/* .line 113 */
/* .local v0, "intentFilter":Landroid/content/IntentFilter; */
final String v1 = "android.intent.action.BOOT_COMPLETED"; // const-string v1, "android.intent.action.BOOT_COMPLETED"
(( android.content.IntentFilter ) v0 ).addAction ( v1 ); // invoke-virtual {v0, v1}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V
/* .line 114 */
v1 = this.mLifecycleReceiver;
(( android.content.Context ) p1 ).registerReceiver ( v1, v0 ); // invoke-virtual {p1, v1, v0}, Landroid/content/Context;->registerReceiver(Landroid/content/BroadcastReceiver;Landroid/content/IntentFilter;)Landroid/content/Intent;
/* .line 115 */
/* new-instance v1, Lcom/miui/server/turbosched/TurboSchedSceneManager$TurboSchedSceneHandler; */
/* invoke-direct {v1, p0, p2}, Lcom/miui/server/turbosched/TurboSchedSceneManager$TurboSchedSceneHandler;-><init>(Lcom/miui/server/turbosched/TurboSchedSceneManager;Landroid/os/Looper;)V */
this.mTbSceneHandler = v1;
/* .line 116 */
return;
} // .end method
public void onFocusedWindowChangeLocked ( java.lang.String p0, Integer p1 ) {
/* .locals 3 */
/* .param p1, "focus" # Ljava/lang/String; */
/* .param p2, "type" # I */
/* .line 301 */
/* new-instance v0, Landroid/os/Bundle; */
/* invoke-direct {v0}, Landroid/os/Bundle;-><init>()V */
/* .line 302 */
/* .local v0, "bundle":Landroid/os/Bundle; */
/* const-string/jumbo v1, "type" */
(( android.os.Bundle ) v0 ).putInt ( v1, p2 ); // invoke-virtual {v0, v1, p2}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V
/* .line 303 */
final String v1 = "focus"; // const-string v1, "focus"
(( android.os.Bundle ) v0 ).putString ( v1, p1 ); // invoke-virtual {v0, v1, p1}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V
/* .line 304 */
v1 = this.mTbSceneHandler;
(( com.miui.server.turbosched.TurboSchedSceneManager$TurboSchedSceneHandler ) v1 ).obtainMessage ( ); // invoke-virtual {v1}, Lcom/miui/server/turbosched/TurboSchedSceneManager$TurboSchedSceneHandler;->obtainMessage()Landroid/os/Message;
/* .line 305 */
/* .local v1, "msg":Landroid/os/Message; */
(( android.os.Message ) v1 ).setData ( v0 ); // invoke-virtual {v1, v0}, Landroid/os/Message;->setData(Landroid/os/Bundle;)V
/* .line 306 */
int v2 = 3; // const/4 v2, 0x3
/* iput v2, v1, Landroid/os/Message;->what:I */
/* .line 307 */
v2 = this.mTbSceneHandler;
(( com.miui.server.turbosched.TurboSchedSceneManager$TurboSchedSceneHandler ) v2 ).sendMessage ( v1 ); // invoke-virtual {v2, v1}, Lcom/miui/server/turbosched/TurboSchedSceneManager$TurboSchedSceneHandler;->sendMessage(Landroid/os/Message;)Z
/* .line 308 */
return;
} // .end method
public void registerStateChangeCallback ( java.lang.String p0, miui.turbosched.ITurboSchedManager$ITurboSchedStateChangeCallback p1 ) {
/* .locals 4 */
/* .param p1, "pkgName" # Ljava/lang/String; */
/* .param p2, "cb" # Lmiui/turbosched/ITurboSchedManager$ITurboSchedStateChangeCallback; */
/* .line 145 */
/* new-instance v0, Lcom/miui/server/turbosched/TurboSchedSceneManager$1; */
/* invoke-direct {v0, p0, p1, p2}, Lcom/miui/server/turbosched/TurboSchedSceneManager$1;-><init>(Lcom/miui/server/turbosched/TurboSchedSceneManager;Ljava/lang/String;Lmiui/turbosched/ITurboSchedManager$ITurboSchedStateChangeCallback;)V */
/* .line 155 */
/* .local v0, "dr":Landroid/os/IBinder$DeathRecipient; */
try { // :try_start_0
int v2 = 0; // const/4 v2, 0x0
/* :try_end_0 */
/* .catch Landroid/os/RemoteException; {:try_start_0 ..:try_end_0} :catch_0 */
/* .line 159 */
/* nop */
/* .line 161 */
v1 = this.mLock;
/* monitor-enter v1 */
/* .line 162 */
try { // :try_start_1
v2 = this.mAppStateListeners;
(( android.util.ArrayMap ) v2 ).put ( p1, p2 ); // invoke-virtual {v2, p1, p2}, Landroid/util/ArrayMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;
/* .line 163 */
/* monitor-exit v1 */
/* :try_end_1 */
/* .catchall {:try_start_1 ..:try_end_1} :catchall_0 */
/* .line 164 */
android.os.TurboSchedMonitor .getInstance ( );
v1 = (( android.os.TurboSchedMonitor ) v1 ).isDebugMode ( ); // invoke-virtual {v1}, Landroid/os/TurboSchedMonitor;->isDebugMode()Z
if ( v1 != null) { // if-eqz v1, :cond_0
/* .line 165 */
/* const-string/jumbo v1, "tsched_scene" */
/* new-instance v2, Ljava/lang/StringBuilder; */
/* invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V */
final String v3 = "register window state callback pkg="; // const-string v3, "register window state callback pkg="
(( java.lang.StringBuilder ) v2 ).append ( v3 ); // invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v2 ).append ( p1 ); // invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
final String v3 = " listener size="; // const-string v3, " listener size="
(( java.lang.StringBuilder ) v2 ).append ( v3 ); // invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
v3 = this.mAppStateListeners;
v3 = (( android.util.ArrayMap ) v3 ).size ( ); // invoke-virtual {v3}, Landroid/util/ArrayMap;->size()I
(( java.lang.StringBuilder ) v2 ).append ( v3 ); // invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;
final String v3 = ", "; // const-string v3, ", "
(( java.lang.StringBuilder ) v2 ).append ( v3 ); // invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
v3 = this.mAppStateListeners;
(( android.util.ArrayMap ) v3 ).get ( p1 ); // invoke-virtual {v3, p1}, Landroid/util/ArrayMap;->get(Ljava/lang/Object;)Ljava/lang/Object;
(( java.lang.StringBuilder ) v2 ).append ( v3 ); // invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v2 ).toString ( ); // invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
android.util.Slog .i ( v1,v2 );
/* .line 167 */
} // :cond_0
return;
/* .line 163 */
/* :catchall_0 */
/* move-exception v2 */
try { // :try_start_2
/* monitor-exit v1 */
/* :try_end_2 */
/* .catchall {:try_start_2 ..:try_end_2} :catchall_0 */
/* throw v2 */
/* .line 156 */
/* :catch_0 */
/* move-exception v1 */
/* .line 157 */
/* .local v1, "e":Landroid/os/RemoteException; */
/* const-string/jumbo v2, "tsched_scene" */
final String v3 = "Error on linkToDeath - "; // const-string v3, "Error on linkToDeath - "
android.util.Slog .e ( v2,v3,v1 );
/* .line 158 */
return;
} // .end method
public void unregisterStateChangeCallback ( java.lang.String p0, miui.turbosched.ITurboSchedManager$ITurboSchedStateChangeCallback p1 ) {
/* .locals 3 */
/* .param p1, "pkgName" # Ljava/lang/String; */
/* .param p2, "cb" # Lmiui/turbosched/ITurboSchedManager$ITurboSchedStateChangeCallback; */
/* .line 175 */
v0 = this.mLock;
/* monitor-enter v0 */
/* .line 176 */
try { // :try_start_0
v1 = this.mAppStateListeners;
v1 = (( android.util.ArrayMap ) v1 ).containsKey ( p1 ); // invoke-virtual {v1, p1}, Landroid/util/ArrayMap;->containsKey(Ljava/lang/Object;)Z
if ( v1 != null) { // if-eqz v1, :cond_0
/* .line 177 */
v1 = this.mAppStateListeners;
(( android.util.ArrayMap ) v1 ).remove ( p1 ); // invoke-virtual {v1, p1}, Landroid/util/ArrayMap;->remove(Ljava/lang/Object;)Ljava/lang/Object;
/* .line 179 */
} // :cond_0
/* monitor-exit v0 */
/* :try_end_0 */
/* .catchall {:try_start_0 ..:try_end_0} :catchall_0 */
/* .line 180 */
android.os.TurboSchedMonitor .getInstance ( );
v0 = (( android.os.TurboSchedMonitor ) v0 ).isDebugMode ( ); // invoke-virtual {v0}, Landroid/os/TurboSchedMonitor;->isDebugMode()Z
if ( v0 != null) { // if-eqz v0, :cond_1
/* .line 181 */
/* const-string/jumbo v0, "tsched_scene" */
/* new-instance v1, Ljava/lang/StringBuilder; */
/* invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V */
/* const-string/jumbo v2, "unregister window state callback pkg=" */
(( java.lang.StringBuilder ) v1 ).append ( v2 ); // invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v1 ).append ( p1 ); // invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
final String v2 = " listener size="; // const-string v2, " listener size="
(( java.lang.StringBuilder ) v1 ).append ( v2 ); // invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
v2 = this.mAppStateListeners;
v2 = (( android.util.ArrayMap ) v2 ).size ( ); // invoke-virtual {v2}, Landroid/util/ArrayMap;->size()I
(( java.lang.StringBuilder ) v1 ).append ( v2 ); // invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v1 ).toString ( ); // invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
android.util.Slog .i ( v0,v1 );
/* .line 183 */
} // :cond_1
return;
/* .line 179 */
/* :catchall_0 */
/* move-exception v1 */
try { // :try_start_1
/* monitor-exit v0 */
/* :try_end_1 */
/* .catchall {:try_start_1 ..:try_end_1} :catchall_0 */
/* throw v1 */
} // .end method
