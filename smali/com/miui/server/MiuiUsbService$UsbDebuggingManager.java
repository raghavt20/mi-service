class com.miui.server.MiuiUsbService$UsbDebuggingManager extends com.miui.server.UsbManagerConnect {
	 /* .source "MiuiUsbService.java" */
	 /* # annotations */
	 /* .annotation system Ldalvik/annotation/EnclosingClass; */
	 /* value = Lcom/miui/server/MiuiUsbService; */
} // .end annotation
/* .annotation system Ldalvik/annotation/InnerClass; */
/* accessFlags = 0x0 */
/* name = "UsbDebuggingManager" */
} // .end annotation
/* .annotation system Ldalvik/annotation/MemberClasses; */
/* value = { */
/* Lcom/miui/server/MiuiUsbService$UsbDebuggingManager$UsbDebuggingHandler;, */
/* Lcom/miui/server/MiuiUsbService$UsbDebuggingManager$AdbSettingsObserver; */
/* } */
} // .end annotation
/* # instance fields */
private final java.lang.String ADBD_SOCKET;
private final java.lang.String ADB_DIRECTORY;
private final java.lang.String ADB_KEYS_FILE;
private Boolean mAdbEnabled;
private android.content.ContentResolver mContentResolver;
private android.content.Context mContext;
private java.lang.String mFingerprints;
private final android.os.Handler mHandler;
private final android.os.HandlerThread mHandlerThread;
private java.lang.Thread mThread;
final com.miui.server.MiuiUsbService this$0; //synthetic
/* # direct methods */
static Boolean -$$Nest$fgetmAdbEnabled ( com.miui.server.MiuiUsbService$UsbDebuggingManager p0 ) { //bridge//synthethic
/* .locals 0 */
/* iget-boolean p0, p0, Lcom/miui/server/MiuiUsbService$UsbDebuggingManager;->mAdbEnabled:Z */
} // .end method
static android.content.ContentResolver -$$Nest$fgetmContentResolver ( com.miui.server.MiuiUsbService$UsbDebuggingManager p0 ) { //bridge//synthethic
/* .locals 0 */
p0 = this.mContentResolver;
} // .end method
static java.lang.String -$$Nest$fgetmFingerprints ( com.miui.server.MiuiUsbService$UsbDebuggingManager p0 ) { //bridge//synthethic
/* .locals 0 */
p0 = this.mFingerprints;
} // .end method
static java.lang.Thread -$$Nest$fgetmThread ( com.miui.server.MiuiUsbService$UsbDebuggingManager p0 ) { //bridge//synthethic
/* .locals 0 */
p0 = this.mThread;
} // .end method
static void -$$Nest$fputmAdbEnabled ( com.miui.server.MiuiUsbService$UsbDebuggingManager p0, Boolean p1 ) { //bridge//synthethic
/* .locals 0 */
/* iput-boolean p1, p0, Lcom/miui/server/MiuiUsbService$UsbDebuggingManager;->mAdbEnabled:Z */
return;
} // .end method
static void -$$Nest$fputmFingerprints ( com.miui.server.MiuiUsbService$UsbDebuggingManager p0, java.lang.String p1 ) { //bridge//synthethic
/* .locals 0 */
this.mFingerprints = p1;
return;
} // .end method
static void -$$Nest$fputmThread ( com.miui.server.MiuiUsbService$UsbDebuggingManager p0, java.lang.Thread p1 ) { //bridge//synthethic
/* .locals 0 */
this.mThread = p1;
return;
} // .end method
static java.lang.String -$$Nest$mgetFingerprints ( com.miui.server.MiuiUsbService$UsbDebuggingManager p0, java.lang.String p1 ) { //bridge//synthethic
/* .locals 0 */
/* invoke-direct {p0, p1}, Lcom/miui/server/MiuiUsbService$UsbDebuggingManager;->getFingerprints(Ljava/lang/String;)Ljava/lang/String; */
} // .end method
static void -$$Nest$mshowConfirmationDialog ( com.miui.server.MiuiUsbService$UsbDebuggingManager p0, java.lang.String p1, java.lang.String p2 ) { //bridge//synthethic
/* .locals 0 */
/* invoke-direct {p0, p1, p2}, Lcom/miui/server/MiuiUsbService$UsbDebuggingManager;->showConfirmationDialog(Ljava/lang/String;Ljava/lang/String;)V */
return;
} // .end method
static void -$$Nest$mwriteKey ( com.miui.server.MiuiUsbService$UsbDebuggingManager p0, java.lang.String p1 ) { //bridge//synthethic
/* .locals 0 */
/* invoke-direct {p0, p1}, Lcom/miui/server/MiuiUsbService$UsbDebuggingManager;->writeKey(Ljava/lang/String;)V */
return;
} // .end method
public com.miui.server.MiuiUsbService$UsbDebuggingManager ( ) {
/* .locals 4 */
/* .param p1, "this$0" # Lcom/miui/server/MiuiUsbService; */
/* .param p2, "context" # Landroid/content/Context; */
/* .line 120 */
this.this$0 = p1;
/* invoke-direct {p0}, Lcom/miui/server/UsbManagerConnect;-><init>()V */
/* .line 108 */
final String v0 = "adbd"; // const-string v0, "adbd"
this.ADBD_SOCKET = v0;
/* .line 109 */
final String v0 = "misc/adb"; // const-string v0, "misc/adb"
this.ADB_DIRECTORY = v0;
/* .line 110 */
final String v0 = "adb_keys"; // const-string v0, "adb_keys"
this.ADB_KEYS_FILE = v0;
/* .line 115 */
int v0 = 0; // const/4 v0, 0x0
/* iput-boolean v0, p0, Lcom/miui/server/MiuiUsbService$UsbDebuggingManager;->mAdbEnabled:Z */
/* .line 121 */
/* new-instance v1, Landroid/os/HandlerThread; */
final String v2 = "UsbDebuggingHandler"; // const-string v2, "UsbDebuggingHandler"
/* invoke-direct {v1, v2}, Landroid/os/HandlerThread;-><init>(Ljava/lang/String;)V */
this.mHandlerThread = v1;
/* .line 122 */
(( android.os.HandlerThread ) v1 ).start ( ); // invoke-virtual {v1}, Landroid/os/HandlerThread;->start()V
/* .line 123 */
/* new-instance v2, Lcom/miui/server/MiuiUsbService$UsbDebuggingManager$UsbDebuggingHandler; */
(( android.os.HandlerThread ) v1 ).getLooper ( ); // invoke-virtual {v1}, Landroid/os/HandlerThread;->getLooper()Landroid/os/Looper;
/* invoke-direct {v2, p0, v1}, Lcom/miui/server/MiuiUsbService$UsbDebuggingManager$UsbDebuggingHandler;-><init>(Lcom/miui/server/MiuiUsbService$UsbDebuggingManager;Landroid/os/Looper;)V */
this.mHandler = v2;
/* .line 124 */
this.mContext = p2;
/* .line 125 */
(( android.content.Context ) p2 ).getContentResolver ( ); // invoke-virtual {p2}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;
this.mContentResolver = v1;
/* .line 127 */
/* nop */
/* .line 128 */
final String v2 = "adb_enabled"; // const-string v2, "adb_enabled"
android.provider.Settings$Secure .getUriFor ( v2 );
/* new-instance v3, Lcom/miui/server/MiuiUsbService$UsbDebuggingManager$AdbSettingsObserver; */
/* invoke-direct {v3, p0}, Lcom/miui/server/MiuiUsbService$UsbDebuggingManager$AdbSettingsObserver;-><init>(Lcom/miui/server/MiuiUsbService$UsbDebuggingManager;)V */
/* .line 127 */
(( android.content.ContentResolver ) v1 ).registerContentObserver ( v2, v0, v3 ); // invoke-virtual {v1, v2, v0, v3}, Landroid/content/ContentResolver;->registerContentObserver(Landroid/net/Uri;ZLandroid/database/ContentObserver;)V
/* .line 130 */
return;
} // .end method
private java.lang.String getFingerprints ( java.lang.String p0 ) {
/* .locals 7 */
/* .param p1, "key" # Ljava/lang/String; */
/* .line 273 */
final String v0 = "0123456789ABCDEF"; // const-string v0, "0123456789ABCDEF"
/* .line 274 */
/* .local v0, "hex":Ljava/lang/String; */
/* new-instance v1, Ljava/lang/StringBuilder; */
/* invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V */
/* .line 278 */
/* .local v1, "sb":Ljava/lang/StringBuilder; */
try { // :try_start_0
final String v2 = "MD5"; // const-string v2, "MD5"
java.security.MessageDigest .getInstance ( v2 );
/* :try_end_0 */
/* .catch Ljava/lang/Exception; {:try_start_0 ..:try_end_0} :catch_0 */
/* .line 282 */
/* .local v2, "digester":Ljava/security/MessageDigest; */
/* nop */
/* .line 284 */
final String v3 = "\\s+"; // const-string v3, "\\s+"
(( java.lang.String ) p1 ).split ( v3 ); // invoke-virtual {p1, v3}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;
int v4 = 0; // const/4 v4, 0x0
/* aget-object v3, v3, v4 */
(( java.lang.String ) v3 ).getBytes ( ); // invoke-virtual {v3}, Ljava/lang/String;->getBytes()[B
/* .line 285 */
/* .local v3, "base64_data":[B */
android.util.Base64 .decode ( v3,v4 );
(( java.security.MessageDigest ) v2 ).digest ( v4 ); // invoke-virtual {v2, v4}, Ljava/security/MessageDigest;->digest([B)[B
/* .line 287 */
/* .local v4, "digest":[B */
int v5 = 0; // const/4 v5, 0x0
/* .local v5, "i":I */
} // :goto_0
/* array-length v6, v4 */
/* if-ge v5, v6, :cond_1 */
/* .line 288 */
/* aget-byte v6, v4, v5 */
/* shr-int/lit8 v6, v6, 0x4 */
/* and-int/lit8 v6, v6, 0xf */
v6 = (( java.lang.String ) v0 ).charAt ( v6 ); // invoke-virtual {v0, v6}, Ljava/lang/String;->charAt(I)C
(( java.lang.StringBuilder ) v1 ).append ( v6 ); // invoke-virtual {v1, v6}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;
/* .line 289 */
/* aget-byte v6, v4, v5 */
/* and-int/lit8 v6, v6, 0xf */
v6 = (( java.lang.String ) v0 ).charAt ( v6 ); // invoke-virtual {v0, v6}, Ljava/lang/String;->charAt(I)C
(( java.lang.StringBuilder ) v1 ).append ( v6 ); // invoke-virtual {v1, v6}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;
/* .line 290 */
/* array-length v6, v4 */
/* add-int/lit8 v6, v6, -0x1 */
/* if-ge v5, v6, :cond_0 */
/* .line 291 */
final String v6 = ":"; // const-string v6, ":"
(( java.lang.StringBuilder ) v1 ).append ( v6 ); // invoke-virtual {v1, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
/* .line 287 */
} // :cond_0
/* add-int/lit8 v5, v5, 0x1 */
/* .line 293 */
} // .end local v5 # "i":I
} // :cond_1
(( java.lang.StringBuilder ) v1 ).toString ( ); // invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
/* .line 279 */
} // .end local v2 # "digester":Ljava/security/MessageDigest;
} // .end local v3 # "base64_data":[B
} // .end local v4 # "digest":[B
/* :catch_0 */
/* move-exception v2 */
/* .line 280 */
/* .local v2, "ex":Ljava/lang/Exception; */
/* new-instance v3, Ljava/lang/StringBuilder; */
/* invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V */
final String v4 = "Error getting digester: "; // const-string v4, "Error getting digester: "
(( java.lang.StringBuilder ) v3 ).append ( v4 ); // invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v3 ).append ( v2 ); // invoke-virtual {v3, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v3 ).toString ( ); // invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
final String v4 = "MiuiUsbService"; // const-string v4, "MiuiUsbService"
android.util.Slog .e ( v4,v3 );
/* .line 281 */
final String v3 = ""; // const-string v3, ""
} // .end method
private void showConfirmationDialog ( java.lang.String p0, java.lang.String p1 ) {
/* .locals 4 */
/* .param p1, "key" # Ljava/lang/String; */
/* .param p2, "fingerprints" # Ljava/lang/String; */
/* .line 297 */
/* new-instance v0, Landroid/content/Intent; */
/* invoke-direct {v0}, Landroid/content/Intent;-><init>()V */
/* .line 299 */
/* .local v0, "dialogIntent":Landroid/content/Intent; */
final String v1 = "com.android.systemui"; // const-string v1, "com.android.systemui"
final String v2 = "com.android.systemui.usb.UsbDebuggingActivity"; // const-string v2, "com.android.systemui.usb.UsbDebuggingActivity"
(( android.content.Intent ) v0 ).setClassName ( v1, v2 ); // invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->setClassName(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;
/* .line 301 */
/* const/high16 v1, 0x10000000 */
(( android.content.Intent ) v0 ).addFlags ( v1 ); // invoke-virtual {v0, v1}, Landroid/content/Intent;->addFlags(I)Landroid/content/Intent;
/* .line 302 */
final String v1 = "key"; // const-string v1, "key"
(( android.content.Intent ) v0 ).putExtra ( v1, p1 ); // invoke-virtual {v0, v1, p1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;
/* .line 303 */
final String v1 = "fingerprints"; // const-string v1, "fingerprints"
(( android.content.Intent ) v0 ).putExtra ( v1, p2 ); // invoke-virtual {v0, v1, p2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;
/* .line 305 */
try { // :try_start_0
v1 = this.mContext;
(( android.content.Context ) v1 ).startActivity ( v0 ); // invoke-virtual {v1, v0}, Landroid/content/Context;->startActivity(Landroid/content/Intent;)V
/* :try_end_0 */
/* .catch Landroid/content/ActivityNotFoundException; {:try_start_0 ..:try_end_0} :catch_0 */
/* .line 308 */
/* .line 306 */
/* :catch_0 */
/* move-exception v1 */
/* .line 307 */
/* .local v1, "e":Landroid/content/ActivityNotFoundException; */
final String v2 = "MiuiUsbService"; // const-string v2, "MiuiUsbService"
/* const-string/jumbo v3, "unable to start UsbDebuggingActivity" */
android.util.Slog .e ( v2,v3 );
/* .line 309 */
} // .end local v1 # "e":Landroid/content/ActivityNotFoundException;
} // :goto_0
return;
} // .end method
private void writeKey ( java.lang.String p0 ) {
/* .locals 7 */
/* .param p1, "key" # Ljava/lang/String; */
/* .line 312 */
android.os.Environment .getDataDirectory ( );
/* .line 313 */
/* .local v0, "dataDir":Ljava/io/File; */
/* new-instance v1, Ljava/io/File; */
final String v2 = "misc/adb"; // const-string v2, "misc/adb"
/* invoke-direct {v1, v0, v2}, Ljava/io/File;-><init>(Ljava/io/File;Ljava/lang/String;)V */
/* .line 315 */
/* .local v1, "adbDir":Ljava/io/File; */
v2 = (( java.io.File ) v1 ).exists ( ); // invoke-virtual {v1}, Ljava/io/File;->exists()Z
final String v3 = "MiuiUsbService"; // const-string v3, "MiuiUsbService"
/* if-nez v2, :cond_0 */
/* .line 316 */
final String v2 = "ADB data directory does not exist"; // const-string v2, "ADB data directory does not exist"
android.util.Slog .e ( v3,v2 );
/* .line 317 */
return;
/* .line 321 */
} // :cond_0
try { // :try_start_0
/* new-instance v2, Ljava/io/File; */
final String v4 = "adb_keys"; // const-string v4, "adb_keys"
/* invoke-direct {v2, v1, v4}, Ljava/io/File;-><init>(Ljava/io/File;Ljava/lang/String;)V */
/* .line 323 */
/* .local v2, "keyFile":Ljava/io/File; */
v4 = (( java.io.File ) v2 ).exists ( ); // invoke-virtual {v2}, Ljava/io/File;->exists()Z
/* if-nez v4, :cond_1 */
/* .line 324 */
(( java.io.File ) v2 ).createNewFile ( ); // invoke-virtual {v2}, Ljava/io/File;->createNewFile()Z
/* .line 325 */
(( java.io.File ) v2 ).toString ( ); // invoke-virtual {v2}, Ljava/io/File;->toString()Ljava/lang/String;
/* const/16 v5, 0x1a0 */
int v6 = -1; // const/4 v6, -0x1
android.os.FileUtils .setPermissions ( v4,v5,v6,v6 );
/* .line 330 */
} // :cond_1
/* new-instance v4, Ljava/io/FileOutputStream; */
int v5 = 1; // const/4 v5, 0x1
/* invoke-direct {v4, v2, v5}, Ljava/io/FileOutputStream;-><init>(Ljava/io/File;Z)V */
/* .line 331 */
/* .local v4, "fo":Ljava/io/FileOutputStream; */
(( java.lang.String ) p1 ).getBytes ( ); // invoke-virtual {p1}, Ljava/lang/String;->getBytes()[B
(( java.io.FileOutputStream ) v4 ).write ( v5 ); // invoke-virtual {v4, v5}, Ljava/io/FileOutputStream;->write([B)V
/* .line 332 */
/* const/16 v5, 0xa */
(( java.io.FileOutputStream ) v4 ).write ( v5 ); // invoke-virtual {v4, v5}, Ljava/io/FileOutputStream;->write(I)V
/* .line 333 */
(( java.io.FileOutputStream ) v4 ).close ( ); // invoke-virtual {v4}, Ljava/io/FileOutputStream;->close()V
/* :try_end_0 */
/* .catch Ljava/io/IOException; {:try_start_0 ..:try_end_0} :catch_0 */
/* .line 336 */
} // .end local v2 # "keyFile":Ljava/io/File;
} // .end local v4 # "fo":Ljava/io/FileOutputStream;
/* .line 334 */
/* :catch_0 */
/* move-exception v2 */
/* .line 335 */
/* .local v2, "ex":Ljava/io/IOException; */
/* new-instance v4, Ljava/lang/StringBuilder; */
/* invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V */
final String v5 = "Error writing key:"; // const-string v5, "Error writing key:"
(( java.lang.StringBuilder ) v4 ).append ( v5 ); // invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v4 ).append ( v2 ); // invoke-virtual {v4, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v4 ).toString ( ); // invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
android.util.Slog .e ( v3,v4 );
/* .line 337 */
} // .end local v2 # "ex":Ljava/io/IOException;
} // :goto_0
return;
} // .end method
/* # virtual methods */
public void allowUsbDebugging ( Boolean p0, java.lang.String p1 ) {
/* .locals 2 */
/* .param p1, "alwaysAllow" # Z */
/* .param p2, "publicKey" # Ljava/lang/String; */
/* .line 345 */
v0 = this.mHandler;
int v1 = 3; // const/4 v1, 0x3
(( android.os.Handler ) v0 ).obtainMessage ( v1 ); // invoke-virtual {v0, v1}, Landroid/os/Handler;->obtainMessage(I)Landroid/os/Message;
/* .line 346 */
/* .local v0, "msg":Landroid/os/Message; */
/* iput p1, v0, Landroid/os/Message;->arg1:I */
/* .line 347 */
this.obj = p2;
/* .line 348 */
v1 = this.mHandler;
(( android.os.Handler ) v1 ).sendMessage ( v0 ); // invoke-virtual {v1, v0}, Landroid/os/Handler;->sendMessage(Landroid/os/Message;)Z
/* .line 349 */
return;
} // .end method
public void denyUsbDebugging ( ) {
/* .locals 2 */
/* .line 352 */
v0 = this.mHandler;
int v1 = 4; // const/4 v1, 0x4
(( android.os.Handler ) v0 ).sendEmptyMessage ( v1 ); // invoke-virtual {v0, v1}, Landroid/os/Handler;->sendEmptyMessage(I)Z
/* .line 353 */
return;
} // .end method
public void dump ( java.io.FileDescriptor p0, java.io.PrintWriter p1 ) {
/* .locals 5 */
/* .param p1, "fd" # Ljava/io/FileDescriptor; */
/* .param p2, "pw" # Ljava/io/PrintWriter; */
/* .line 356 */
final String v0 = "IOException: "; // const-string v0, "IOException: "
final String v1 = " USB Debugging State:"; // const-string v1, " USB Debugging State:"
(( java.io.PrintWriter ) p2 ).println ( v1 ); // invoke-virtual {p2, v1}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V
/* .line 357 */
/* new-instance v1, Ljava/lang/StringBuilder; */
/* invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V */
final String v2 = " Connected to adbd: "; // const-string v2, " Connected to adbd: "
(( java.lang.StringBuilder ) v1 ).append ( v2 ); // invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
v2 = this.mOutputStream;
int v3 = 0; // const/4 v3, 0x0
if ( v2 != null) { // if-eqz v2, :cond_0
int v2 = 1; // const/4 v2, 0x1
} // :cond_0
/* move v2, v3 */
} // :goto_0
(( java.lang.StringBuilder ) v1 ).append ( v2 ); // invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v1 ).toString ( ); // invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
(( java.io.PrintWriter ) p2 ).println ( v1 ); // invoke-virtual {p2, v1}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V
/* .line 358 */
/* new-instance v1, Ljava/lang/StringBuilder; */
/* invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V */
final String v2 = " Last key received: "; // const-string v2, " Last key received: "
(( java.lang.StringBuilder ) v1 ).append ( v2 ); // invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
v2 = this.mFingerprints;
(( java.lang.StringBuilder ) v1 ).append ( v2 ); // invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v1 ).toString ( ); // invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
(( java.io.PrintWriter ) p2 ).println ( v1 ); // invoke-virtual {p2, v1}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V
/* .line 359 */
final String v1 = " User keys:"; // const-string v1, " User keys:"
(( java.io.PrintWriter ) p2 ).println ( v1 ); // invoke-virtual {p2, v1}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V
/* .line 361 */
int v1 = 0; // const/4 v1, 0x0
try { // :try_start_0
/* new-instance v2, Ljava/io/File; */
final String v4 = "/data/misc/adb/adb_keys"; // const-string v4, "/data/misc/adb/adb_keys"
/* invoke-direct {v2, v4}, Ljava/io/File;-><init>(Ljava/lang/String;)V */
android.os.FileUtils .readTextFile ( v2,v3,v1 );
(( java.io.PrintWriter ) p2 ).println ( v2 ); // invoke-virtual {p2, v2}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V
/* :try_end_0 */
/* .catch Ljava/io/IOException; {:try_start_0 ..:try_end_0} :catch_0 */
/* .line 364 */
/* .line 362 */
/* :catch_0 */
/* move-exception v2 */
/* .line 363 */
/* .local v2, "e":Ljava/io/IOException; */
/* new-instance v4, Ljava/lang/StringBuilder; */
/* invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V */
(( java.lang.StringBuilder ) v4 ).append ( v0 ); // invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v4 ).append ( v2 ); // invoke-virtual {v4, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v4 ).toString ( ); // invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
(( java.io.PrintWriter ) p2 ).println ( v4 ); // invoke-virtual {p2, v4}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V
/* .line 365 */
} // .end local v2 # "e":Ljava/io/IOException;
} // :goto_1
final String v2 = " System keys:"; // const-string v2, " System keys:"
(( java.io.PrintWriter ) p2 ).println ( v2 ); // invoke-virtual {p2, v2}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V
/* .line 367 */
try { // :try_start_1
/* new-instance v2, Ljava/io/File; */
final String v4 = "/adb_keys"; // const-string v4, "/adb_keys"
/* invoke-direct {v2, v4}, Ljava/io/File;-><init>(Ljava/lang/String;)V */
android.os.FileUtils .readTextFile ( v2,v3,v1 );
(( java.io.PrintWriter ) p2 ).println ( v1 ); // invoke-virtual {p2, v1}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V
/* :try_end_1 */
/* .catch Ljava/io/IOException; {:try_start_1 ..:try_end_1} :catch_1 */
/* .line 370 */
/* .line 368 */
/* :catch_1 */
/* move-exception v1 */
/* .line 369 */
/* .local v1, "e":Ljava/io/IOException; */
/* new-instance v2, Ljava/lang/StringBuilder; */
/* invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V */
(( java.lang.StringBuilder ) v2 ).append ( v0 ); // invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v0 ).toString ( ); // invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
(( java.io.PrintWriter ) p2 ).println ( v0 ); // invoke-virtual {p2, v0}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V
/* .line 371 */
} // .end local v1 # "e":Ljava/io/IOException;
} // :goto_2
return;
} // .end method
void listenToSocket ( ) {
/* .locals 9 */
/* .annotation system Ldalvik/annotation/Throws; */
/* value = { */
/* Ljava/io/IOException; */
/* } */
} // .end annotation
/* .line 134 */
final String v0 = "MiuiUsbService"; // const-string v0, "MiuiUsbService"
/* const/16 v1, 0x1000 */
try { // :try_start_0
/* new-array v1, v1, [B */
/* .line 135 */
/* .local v1, "buffer":[B */
/* new-instance v2, Landroid/net/LocalSocketAddress; */
final String v3 = "adbd"; // const-string v3, "adbd"
v4 = android.net.LocalSocketAddress$Namespace.RESERVED;
/* invoke-direct {v2, v3, v4}, Landroid/net/LocalSocketAddress;-><init>(Ljava/lang/String;Landroid/net/LocalSocketAddress$Namespace;)V */
/* .line 137 */
/* .local v2, "address":Landroid/net/LocalSocketAddress; */
int v3 = 0; // const/4 v3, 0x0
/* .line 139 */
/* .local v3, "inputStream":Ljava/io/InputStream; */
/* new-instance v4, Landroid/net/LocalSocket; */
/* invoke-direct {v4}, Landroid/net/LocalSocket;-><init>()V */
this.mSocket = v4;
/* .line 140 */
v4 = this.mSocket;
(( android.net.LocalSocket ) v4 ).connect ( v2 ); // invoke-virtual {v4, v2}, Landroid/net/LocalSocket;->connect(Landroid/net/LocalSocketAddress;)V
/* .line 141 */
final String v4 = "connected to adbd"; // const-string v4, "connected to adbd"
android.util.Log .i ( v0,v4 );
/* .line 143 */
v4 = this.mSocket;
(( android.net.LocalSocket ) v4 ).getOutputStream ( ); // invoke-virtual {v4}, Landroid/net/LocalSocket;->getOutputStream()Ljava/io/OutputStream;
this.mOutputStream = v4;
/* .line 144 */
v4 = this.mSocket;
(( android.net.LocalSocket ) v4 ).getInputStream ( ); // invoke-virtual {v4}, Landroid/net/LocalSocket;->getInputStream()Ljava/io/InputStream;
/* move-object v3, v4 */
/* .line 147 */
} // :goto_0
v4 = (( java.io.InputStream ) v3 ).read ( v1 ); // invoke-virtual {v3, v1}, Ljava/io/InputStream;->read([B)I
/* .line 148 */
/* .local v4, "count":I */
/* if-gez v4, :cond_0 */
/* .line 149 */
/* new-instance v5, Ljava/lang/StringBuilder; */
/* invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V */
final String v6 = "got "; // const-string v6, "got "
(( java.lang.StringBuilder ) v5 ).append ( v6 ); // invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v5 ).append ( v4 ); // invoke-virtual {v5, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;
final String v6 = " reading"; // const-string v6, " reading"
(( java.lang.StringBuilder ) v5 ).append ( v6 ); // invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v5 ).toString ( ); // invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
android.util.Slog .e ( v0,v5 );
/* .line 150 */
/* .line 153 */
} // :cond_0
int v5 = 0; // const/4 v5, 0x0
/* aget-byte v6, v1, v5 */
/* const/16 v7, 0x50 */
int v8 = 2; // const/4 v8, 0x2
/* if-ne v6, v7, :cond_1 */
int v6 = 1; // const/4 v6, 0x1
/* aget-byte v6, v1, v6 */
/* const/16 v7, 0x4b */
/* if-ne v6, v7, :cond_1 */
/* .line 154 */
/* new-instance v5, Ljava/lang/String; */
java.util.Arrays .copyOfRange ( v1,v8,v4 );
/* invoke-direct {v5, v6}, Ljava/lang/String;-><init>([B)V */
/* .line 155 */
/* .local v5, "key":Ljava/lang/String; */
/* new-instance v6, Ljava/lang/StringBuilder; */
/* invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V */
final String v7 = "Received public key: "; // const-string v7, "Received public key: "
(( java.lang.StringBuilder ) v6 ).append ( v7 ); // invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v6 ).append ( v5 ); // invoke-virtual {v6, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v6 ).toString ( ); // invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
android.util.Slog .d ( v0,v6 );
/* .line 156 */
v6 = this.mHandler;
/* .line 157 */
int v7 = 5; // const/4 v7, 0x5
(( android.os.Handler ) v6 ).obtainMessage ( v7 ); // invoke-virtual {v6, v7}, Landroid/os/Handler;->obtainMessage(I)Landroid/os/Message;
/* .line 158 */
/* .local v6, "msg":Landroid/os/Message; */
this.obj = v5;
/* .line 159 */
v7 = this.mHandler;
(( android.os.Handler ) v7 ).sendMessage ( v6 ); // invoke-virtual {v7, v6}, Landroid/os/Handler;->sendMessage(Landroid/os/Message;)Z
/* .line 160 */
/* nop */
/* .line 165 */
} // .end local v4 # "count":I
} // .end local v5 # "key":Ljava/lang/String;
} // .end local v6 # "msg":Landroid/os/Message;
/* .line 161 */
/* .restart local v4 # "count":I */
} // :cond_1
/* new-instance v6, Ljava/lang/StringBuilder; */
/* invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V */
final String v7 = "Wrong message: "; // const-string v7, "Wrong message: "
(( java.lang.StringBuilder ) v6 ).append ( v7 ); // invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
/* new-instance v7, Ljava/lang/String; */
/* .line 162 */
java.util.Arrays .copyOfRange ( v1,v5,v8 );
/* invoke-direct {v7, v5}, Ljava/lang/String;-><init>([B)V */
(( java.lang.StringBuilder ) v6 ).append ( v7 ); // invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v5 ).toString ( ); // invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
/* .line 161 */
android.util.Slog .e ( v0,v5 );
/* :try_end_0 */
/* .catch Ljava/io/IOException; {:try_start_0 ..:try_end_0} :catch_0 */
/* .catchall {:try_start_0 ..:try_end_0} :catchall_0 */
/* .line 163 */
/* nop */
/* .line 170 */
} // .end local v1 # "buffer":[B
} // .end local v2 # "address":Landroid/net/LocalSocketAddress;
} // .end local v3 # "inputStream":Ljava/io/InputStream;
} // .end local v4 # "count":I
} // :goto_1
(( com.miui.server.MiuiUsbService$UsbDebuggingManager ) p0 ).closeSocket ( ); // invoke-virtual {p0}, Lcom/miui/server/MiuiUsbService$UsbDebuggingManager;->closeSocket()V
/* .line 171 */
/* nop */
/* .line 172 */
return;
/* .line 170 */
/* :catchall_0 */
/* move-exception v0 */
/* .line 166 */
/* :catch_0 */
/* move-exception v1 */
/* .line 167 */
/* .local v1, "ex":Ljava/io/IOException; */
try { // :try_start_1
final String v2 = "Communication error: "; // const-string v2, "Communication error: "
android.util.Slog .e ( v0,v2,v1 );
/* .line 168 */
/* nop */
} // .end local p0 # "this":Lcom/miui/server/MiuiUsbService$UsbDebuggingManager;
/* throw v1 */
/* :try_end_1 */
/* .catchall {:try_start_1 ..:try_end_1} :catchall_0 */
/* .line 170 */
} // .end local v1 # "ex":Ljava/io/IOException;
/* .restart local p0 # "this":Lcom/miui/server/MiuiUsbService$UsbDebuggingManager; */
} // :goto_2
(( com.miui.server.MiuiUsbService$UsbDebuggingManager ) p0 ).closeSocket ( ); // invoke-virtual {p0}, Lcom/miui/server/MiuiUsbService$UsbDebuggingManager;->closeSocket()V
/* .line 171 */
/* throw v0 */
} // .end method
public void run ( ) {
/* .locals 3 */
/* .line 176 */
/* nop */
} // :goto_0
/* iget-boolean v0, p0, Lcom/miui/server/MiuiUsbService$UsbDebuggingManager;->mAdbEnabled:Z */
if ( v0 != null) { // if-eqz v0, :cond_0
/* .line 178 */
try { // :try_start_0
(( com.miui.server.MiuiUsbService$UsbDebuggingManager ) p0 ).listenToSocket ( ); // invoke-virtual {p0}, Lcom/miui/server/MiuiUsbService$UsbDebuggingManager;->listenToSocket()V
/* :try_end_0 */
/* .catch Ljava/lang/Exception; {:try_start_0 ..:try_end_0} :catch_0 */
/* .line 182 */
} // :goto_1
/* .line 179 */
/* :catch_0 */
/* move-exception v0 */
/* .line 181 */
/* .local v0, "e":Ljava/lang/Exception; */
/* const-wide/16 v1, 0x3e8 */
android.os.SystemClock .sleep ( v1,v2 );
} // .end local v0 # "e":Ljava/lang/Exception;
/* .line 184 */
} // :cond_0
return;
} // .end method
public void setAdbEnabled ( Boolean p0 ) {
/* .locals 2 */
/* .param p1, "enabled" # Z */
/* .line 340 */
v0 = this.mHandler;
if ( p1 != null) { // if-eqz p1, :cond_0
int v1 = 1; // const/4 v1, 0x1
/* .line 341 */
} // :cond_0
int v1 = 2; // const/4 v1, 0x2
/* .line 340 */
} // :goto_0
(( android.os.Handler ) v0 ).sendEmptyMessage ( v1 ); // invoke-virtual {v0, v1}, Landroid/os/Handler;->sendEmptyMessage(I)Z
/* .line 342 */
return;
} // .end method
