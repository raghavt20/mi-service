.class public final Lcom/miui/server/MiuiCldService$Lifecycle;
.super Lcom/android/server/SystemService;
.source "MiuiCldService.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/miui/server/MiuiCldService;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "Lifecycle"
.end annotation


# instance fields
.field private final mService:Lcom/miui/server/MiuiCldService;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 1
    .param p1, "context"    # Landroid/content/Context;

    .line 230
    invoke-direct {p0, p1}, Lcom/android/server/SystemService;-><init>(Landroid/content/Context;)V

    .line 231
    new-instance v0, Lcom/miui/server/MiuiCldService;

    invoke-direct {v0, p1}, Lcom/miui/server/MiuiCldService;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Lcom/miui/server/MiuiCldService$Lifecycle;->mService:Lcom/miui/server/MiuiCldService;

    .line 232
    return-void
.end method


# virtual methods
.method public onStart()V
    .locals 2

    .line 236
    const-string v0, "miui.cld.service"

    iget-object v1, p0, Lcom/miui/server/MiuiCldService$Lifecycle;->mService:Lcom/miui/server/MiuiCldService;

    invoke-virtual {p0, v0, v1}, Lcom/miui/server/MiuiCldService$Lifecycle;->publishBinderService(Ljava/lang/String;Landroid/os/IBinder;)V

    .line 237
    return-void
.end method
