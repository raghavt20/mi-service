.class public Lcom/miui/server/InputMethodHelper;
.super Ljava/lang/Object;
.source "InputMethodHelper.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/miui/server/InputMethodHelper$InputMethodTbCmdRuleObserver;
    }
.end annotation


# static fields
.field private static final DEFAULT_TAOBAO_CMD_RULE:Ljava/lang/String; = "[\uffe5\u20ac$\u00a2\u20b3\u20a4\u20b4][a-zA-Z0-9]+[\uffe5\u20ac$\u00a2\u20b3\u20a4\u20b4]"

.field private static final INPUT_METHOD_TAOBAO_CMD_ENABLE:Ljava/lang/String; = "input_method_taobao_cmd_enable"

.field private static final INPUT_METHOD_TAOBAO_CMD_MODULE_NAME:Ljava/lang/String; = "InputMethodTaobaoCmdModule"

.field private static final INPUT_METHOD_TAOBAO_CMD_RULE:Ljava/lang/String; = "input_method_taobao_cmd_rule"

.field private static final TAG:Ljava/lang/String; = "InputMethodHelper"

.field private static final TAOBAO_RULE_ENABLE_KEY:Ljava/lang/String; = "EnableTbCmd"

.field private static final TAOBAO_RULE_TEXT_KEY:Ljava/lang/String; = "TbCmdRule"

.field private static final URI_CLOUD_ALL_DATA_NOTIFY:Landroid/net/Uri;


# direct methods
.method static bridge synthetic -$$Nest$sminitInputMethodTbCmdRule(Landroid/content/Context;)V
    .locals 0

    invoke-static {p0}, Lcom/miui/server/InputMethodHelper;->initInputMethodTbCmdRule(Landroid/content/Context;)V

    return-void
.end method

.method static constructor <clinit>()V
    .locals 1

    .line 36
    nop

    .line 37
    const-string v0, "content://com.android.settings.cloud.CloudSettings/cloud_all_data/notify"

    invoke-static {v0}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v0

    sput-object v0, Lcom/miui/server/InputMethodHelper;->URI_CLOUD_ALL_DATA_NOTIFY:Landroid/net/Uri;

    .line 36
    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .line 28
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static init(Landroid/content/Context;)V
    .locals 2
    .param p0, "context"    # Landroid/content/Context;

    .line 40
    invoke-static {}, Lcom/miui/server/InputMethodHelper;->isSupportMiuiBottom()Z

    move-result v0

    if-nez v0, :cond_0

    .line 41
    const-string v0, "InputMethodHelper"

    const-string v1, "Not support miui bottom."

    invoke-static {v0, v1}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 42
    return-void

    .line 44
    :cond_0
    invoke-static {p0}, Lcom/miui/server/InputMethodHelper;->registerContentObserver(Landroid/content/Context;)V

    .line 45
    invoke-static {p0}, Lcom/miui/server/InputMethodHelper;->initInputMethodTbCmdRule(Landroid/content/Context;)V

    .line 46
    const/4 v0, 0x0

    invoke-static {p0, v0}, Lcom/miui/server/InputMethodHelper;->initForUser(Landroid/content/Context;I)V

    .line 77
    return-void
.end method

.method private static initForUser(Landroid/content/Context;I)V
    .locals 4
    .param p0, "context"    # Landroid/content/Context;
    .param p1, "userId"    # I

    .line 93
    invoke-static {p0, p1}, Lcom/miui/server/InputMethodHelper;->isMiuiBottomNeedSet(Landroid/content/Context;I)Z

    move-result v0

    if-nez v0, :cond_0

    .line 94
    return-void

    .line 96
    :cond_0
    invoke-static {}, Lcom/miui/server/InputMethodHelper;->isFullScreenDevice()Z

    move-result v0

    const-string v1, "enable_miui_ime_bottom_view"

    const-string v2, "InputMethodHelper"

    if-eqz v0, :cond_2

    sget-object v0, Landroid/inputmethodservice/MiuiBottomConfig;->sBigChinDevices:Ljava/util/HashSet;

    sget-object v3, Landroid/os/Build;->DEVICE:Ljava/lang/String;

    invoke-virtual {v0, v3}, Ljava/util/HashSet;->contains(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_2

    sget-boolean v0, Lmiui/os/Build;->IS_INTERNATIONAL_BUILD:Z

    if-eqz v0, :cond_1

    goto :goto_0

    .line 101
    :cond_1
    invoke-virtual {p0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    const/4 v3, 0x1

    invoke-static {v0, v1, v3, p1}, Landroid/provider/Settings$Secure;->putIntForUser(Landroid/content/ContentResolver;Ljava/lang/String;II)Z

    .line 102
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "enable miui bottom "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v2, v0}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_1

    .line 98
    :cond_2
    :goto_0
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "disable miui bottom for user "

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v2, v0}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 99
    invoke-virtual {p0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    const/4 v2, 0x0

    invoke-static {v0, v1, v2, p1}, Landroid/provider/Settings$Secure;->putIntForUser(Landroid/content/ContentResolver;Ljava/lang/String;II)Z

    .line 104
    :goto_1
    return-void
.end method

.method private static initInputMethodTbCmdRule(Landroid/content/Context;)V
    .locals 5
    .param p0, "context"    # Landroid/content/Context;

    .line 133
    invoke-virtual {p0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    const-string v1, "EnableTbCmd"

    const/4 v2, 0x1

    const-string v3, "InputMethodTaobaoCmdModule"

    invoke-static {v0, v3, v1, v2}, Landroid/provider/MiuiSettings$SettingsCloudData;->getCloudDataBoolean(Landroid/content/ContentResolver;Ljava/lang/String;Ljava/lang/String;Z)Z

    move-result v0

    .line 135
    .local v0, "isEnable":Z
    invoke-virtual {p0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v1

    const-string v2, "input_method_taobao_cmd_enable"

    invoke-static {v1, v2, v0}, Landroid/provider/Settings$Global;->putInt(Landroid/content/ContentResolver;Ljava/lang/String;I)Z

    .line 137
    invoke-virtual {p0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v1

    const-string v2, "TbCmdRule"

    const-string v4, "[\uffe5\u20ac$\u00a2\u20b3\u20a4\u20b4][a-zA-Z0-9]+[\uffe5\u20ac$\u00a2\u20b3\u20a4\u20b4]"

    invoke-static {v1, v3, v2, v4}, Landroid/provider/MiuiSettings$SettingsCloudData;->getCloudDataString(Landroid/content/ContentResolver;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 139
    .local v1, "tbCmdRule":Ljava/lang/String;
    invoke-virtual {p0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v2

    const-string v3, "input_method_taobao_cmd_rule"

    invoke-static {v2, v3, v1}, Landroid/provider/Settings$Global;->putString(Landroid/content/ContentResolver;Ljava/lang/String;Ljava/lang/String;)Z

    .line 140
    return-void
.end method

.method private static isFullScreenDevice()Z
    .locals 4

    .line 81
    const/4 v0, 0x0

    :try_start_0
    invoke-static {v0}, Lmiui/util/CompatibilityHelper;->hasNavigationBar(I)Z

    move-result v0
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    return v0

    .line 82
    :catch_0
    move-exception v1

    .line 83
    .local v1, "e":Landroid/os/RemoteException;
    const-string v2, "InputMethodHelper"

    const-string v3, "get isFullScreenDevice error"

    invoke-static {v2, v3, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    .line 85
    .end local v1    # "e":Landroid/os/RemoteException;
    return v0
.end method

.method private static isMiuiBottomNeedSet(Landroid/content/Context;I)Z
    .locals 3
    .param p0, "context"    # Landroid/content/Context;
    .param p1, "userId"    # I

    .line 89
    invoke-virtual {p0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    const-string v1, "enable_miui_ime_bottom_view"

    const/4 v2, -0x1

    invoke-static {v0, v1, v2, p1}, Landroid/provider/Settings$Secure;->getIntForUser(Landroid/content/ContentResolver;Ljava/lang/String;II)I

    move-result v0

    if-ne v0, v2, :cond_0

    const/4 v0, 0x1

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    :goto_0
    return v0
.end method

.method private static isSupportMiuiBottom()Z
    .locals 3

    .line 107
    const-string v0, "ro.miui.support_miui_ime_bottom"

    const/4 v1, 0x0

    invoke-static {v0, v1}, Landroid/os/SystemProperties;->getInt(Ljava/lang/String;I)I

    move-result v0

    const/4 v2, 0x1

    if-ne v0, v2, :cond_0

    move v1, v2

    :cond_0
    return v1
.end method

.method private static registerContentObserver(Landroid/content/Context;)V
    .locals 4
    .param p0, "context"    # Landroid/content/Context;

    .line 111
    new-instance v0, Lcom/miui/server/InputMethodHelper$InputMethodTbCmdRuleObserver;

    invoke-static {}, Lcom/android/internal/os/BackgroundThread;->getHandler()Landroid/os/Handler;

    move-result-object v1

    invoke-direct {v0, v1, p0}, Lcom/miui/server/InputMethodHelper$InputMethodTbCmdRuleObserver;-><init>(Landroid/os/Handler;Landroid/content/Context;)V

    .line 112
    .local v0, "tbCmdRuleObserver":Lcom/miui/server/InputMethodHelper$InputMethodTbCmdRuleObserver;
    invoke-virtual {p0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v1

    sget-object v2, Lcom/miui/server/InputMethodHelper;->URI_CLOUD_ALL_DATA_NOTIFY:Landroid/net/Uri;

    const/4 v3, 0x1

    invoke-virtual {v1, v2, v3, v0}, Landroid/content/ContentResolver;->registerContentObserver(Landroid/net/Uri;ZLandroid/database/ContentObserver;)V

    .line 114
    return-void
.end method
