abstract class com.miui.server.MiuiCompatModePackages$AppLaunchObserver extends android.app.IMiuiProcessObserver {
	 /* .source "MiuiCompatModePackages.java" */
	 /* # annotations */
	 /* .annotation system Ldalvik/annotation/EnclosingClass; */
	 /* value = Lcom/miui/server/MiuiCompatModePackages; */
} // .end annotation
/* .annotation system Ldalvik/annotation/InnerClass; */
/* accessFlags = 0x40a */
/* name = "AppLaunchObserver" */
} // .end annotation
/* # instance fields */
private java.util.HashSet mRunningFgActivityProcesses;
/* .annotation system Ldalvik/annotation/Signature; */
/* value = { */
/* "Ljava/util/HashSet<", */
/* "Ljava/lang/Integer;", */
/* ">;" */
/* } */
} // .end annotation
} // .end field
/* # direct methods */
private com.miui.server.MiuiCompatModePackages$AppLaunchObserver ( ) {
/* .locals 1 */
/* .line 949 */
/* invoke-direct {p0}, Landroid/app/IMiuiProcessObserver;-><init>()V */
/* .line 951 */
/* new-instance v0, Ljava/util/HashSet; */
/* invoke-direct {v0}, Ljava/util/HashSet;-><init>()V */
this.mRunningFgActivityProcesses = v0;
return;
} // .end method
 com.miui.server.MiuiCompatModePackages$AppLaunchObserver ( ) { //synthethic
/* .locals 0 */
/* invoke-direct {p0}, Lcom/miui/server/MiuiCompatModePackages$AppLaunchObserver;-><init>()V */
return;
} // .end method
/* # virtual methods */
protected abstract void onFirstLaunch ( java.lang.String p0 ) {
} // .end method
public void onForegroundActivitiesChanged ( Integer p0, Integer p1, Boolean p2 ) {
/* .locals 2 */
/* .param p1, "pid" # I */
/* .param p2, "uid" # I */
/* .param p3, "foregroundActivities" # Z */
/* .line 955 */
if ( p3 != null) { // if-eqz p3, :cond_0
/* .line 956 */
v0 = this.mRunningFgActivityProcesses;
java.lang.Integer .valueOf ( p1 );
v0 = (( java.util.HashSet ) v0 ).contains ( v1 ); // invoke-virtual {v0, v1}, Ljava/util/HashSet;->contains(Ljava/lang/Object;)Z
/* if-nez v0, :cond_0 */
/* .line 957 */
v0 = this.mRunningFgActivityProcesses;
java.lang.Integer .valueOf ( p1 );
(( java.util.HashSet ) v0 ).add ( v1 ); // invoke-virtual {v0, v1}, Ljava/util/HashSet;->add(Ljava/lang/Object;)Z
/* .line 959 */
com.android.server.am.ProcessUtils .getPackageNameByPid ( p1 );
/* .line 960 */
/* .local v0, "packageName":Ljava/lang/String; */
(( com.miui.server.MiuiCompatModePackages$AppLaunchObserver ) p0 ).onFirstLaunch ( v0 ); // invoke-virtual {p0, v0}, Lcom/miui/server/MiuiCompatModePackages$AppLaunchObserver;->onFirstLaunch(Ljava/lang/String;)V
/* .line 963 */
} // .end local v0 # "packageName":Ljava/lang/String;
} // :cond_0
return;
} // .end method
public void onForegroundServicesChanged ( Integer p0, Integer p1, Integer p2 ) {
/* .locals 0 */
/* .param p1, "pid" # I */
/* .param p2, "uid" # I */
/* .param p3, "serviceTypes" # I */
/* .line 968 */
return;
} // .end method
public void onImportanceChanged ( Integer p0, Integer p1, Integer p2 ) {
/* .locals 0 */
/* .param p1, "pid" # I */
/* .param p2, "uid" # I */
/* .param p3, "importance" # I */
/* .line 972 */
return;
} // .end method
public void onProcessDied ( Integer p0, Integer p1 ) {
/* .locals 2 */
/* .param p1, "pid" # I */
/* .param p2, "uid" # I */
/* .line 976 */
v0 = this.mRunningFgActivityProcesses;
java.lang.Integer .valueOf ( p1 );
(( java.util.HashSet ) v0 ).remove ( v1 ); // invoke-virtual {v0, v1}, Ljava/util/HashSet;->remove(Ljava/lang/Object;)Z
/* .line 977 */
return;
} // .end method
public void onProcessStateChanged ( Integer p0, Integer p1, Integer p2 ) {
/* .locals 0 */
/* .param p1, "pid" # I */
/* .param p2, "uid" # I */
/* .param p3, "procState" # I */
/* .line 981 */
return;
} // .end method
