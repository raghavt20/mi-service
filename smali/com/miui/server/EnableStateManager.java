public class com.miui.server.EnableStateManager {
	 /* .source "EnableStateManager.java" */
	 /* # annotations */
	 /* .annotation system Ldalvik/annotation/MemberClasses; */
	 /* value = { */
	 /* Lcom/miui/server/EnableStateManager$OtherChangedReceiver;, */
	 /* Lcom/miui/server/EnableStateManager$PackageAddedReceiver; */
	 /* } */
} // .end annotation
/* # static fields */
public static final java.lang.String ACTION_COTA_CARRIER;
public static final java.lang.String HAS_EVER_ENABLED;
public static final java.lang.String HAS_UPDATE_APPLICATION_STATE;
public static final java.lang.String LAST_REGION;
private static final java.lang.String PKG_NAME_MIPICKS;
private static final java.lang.String PROPERTIE_KEY_COTA;
private static final java.lang.String PROPERTIE_KEY_CUSTOM;
private static final java.lang.String PROPERTIE_KEY_DT;
private static final java.lang.String PROVISION_COMPLETE_BROADCAST;
private static final java.lang.String SETTINGS_KEY_FIRST_FINGER_PRINT;
private static final java.lang.String TAG;
private static java.util.Map mCloudEnableSettings;
/* .annotation system Ldalvik/annotation/Signature; */
/* value = { */
/* "Ljava/util/Map<", */
/* "Ljava/lang/String;", */
/* "Ljava/util/List<", */
/* "Ljava/lang/String;", */
/* ">;>;" */
/* } */
} // .end annotation
} // .end field
private static android.content.Context mContext;
private static Boolean receiverRegistered;
private static java.util.List sEnableStateControlledPkgList;
/* .annotation system Ldalvik/annotation/Signature; */
/* value = { */
/* "Ljava/util/List<", */
/* "Ljava/lang/String;", */
/* ">;" */
/* } */
} // .end annotation
} // .end field
private static java.util.List sShouldKeepStatePackages;
/* .annotation system Ldalvik/annotation/Signature; */
/* value = { */
/* "Ljava/util/List<", */
/* "Ljava/lang/String;", */
/* ">;" */
/* } */
} // .end annotation
} // .end field
/* # direct methods */
static java.lang.String -$$Nest$sfgetTAG ( ) { //bridge//synthethic
/* .locals 1 */
v0 = com.miui.server.EnableStateManager.TAG;
} // .end method
static android.content.Context -$$Nest$sfgetmContext ( ) { //bridge//synthethic
/* .locals 1 */
v0 = com.miui.server.EnableStateManager.mContext;
} // .end method
static java.util.List -$$Nest$sfgetsEnableStateControlledPkgList ( ) { //bridge//synthethic
/* .locals 1 */
v0 = com.miui.server.EnableStateManager.sEnableStateControlledPkgList;
} // .end method
static java.lang.String -$$Nest$smgetPackageName ( android.content.Intent p0 ) { //bridge//synthethic
/* .locals 0 */
com.miui.server.EnableStateManager .getPackageName ( p0 );
} // .end method
static com.miui.server.EnableStateManager ( ) {
/* .locals 1 */
/* .line 40 */
/* const-class v0, Lcom/miui/server/EnableStateManager; */
(( java.lang.Class ) v0 ).getSimpleName ( ); // invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;
/* .line 58 */
/* new-instance v0, Ljava/util/HashMap; */
/* invoke-direct {v0}, Ljava/util/HashMap;-><init>()V */
/* .line 59 */
/* new-instance v0, Ljava/util/ArrayList; */
/* invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V */
/* .line 61 */
/* new-instance v0, Ljava/util/ArrayList; */
/* invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V */
/* .line 63 */
int v0 = 0; // const/4 v0, 0x0
com.miui.server.EnableStateManager.receiverRegistered = (v0!= 0);
return;
} // .end method
private com.miui.server.EnableStateManager ( ) {
/* .locals 0 */
/* .line 65 */
/* invoke-direct {p0}, Ljava/lang/Object;-><init>()V */
/* .line 67 */
return;
} // .end method
private static void getEnableRegionListFromRSA ( org.json.JSONObject p0, java.lang.String p1, java.util.List p2 ) {
/* .locals 6 */
/* .param p0, "rsaEnableObject" # Lorg/json/JSONObject; */
/* .param p1, "systemTier" # Ljava/lang/String; */
/* .annotation system Ldalvik/annotation/Signature; */
/* value = { */
/* "(", */
/* "Lorg/json/JSONObject;", */
/* "Ljava/lang/String;", */
/* "Ljava/util/List<", */
/* "Ljava/lang/String;", */
/* ">;)V" */
/* } */
} // .end annotation
/* .line 373 */
/* .local p2, "enableRegionList":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;" */
if ( p0 != null) { // if-eqz p0, :cond_6
v0 = android.text.TextUtils .isEmpty ( p1 );
if ( v0 != null) { // if-eqz v0, :cond_0
/* .line 376 */
} // :cond_0
(( org.json.JSONObject ) p0 ).keys ( ); // invoke-virtual {p0}, Lorg/json/JSONObject;->keys()Ljava/util/Iterator;
/* .line 377 */
/* .local v0, "rsaList":Ljava/util/Iterator;, "Ljava/util/Iterator<Ljava/lang/String;>;" */
v1 = } // :goto_0
if ( v1 != null) { // if-eqz v1, :cond_5
/* .line 378 */
/* check-cast v1, Ljava/lang/String; */
/* .line 379 */
/* .local v1, "rsa":Ljava/lang/String; */
v2 = android.text.TextUtils .equals ( v1,p1 );
/* if-nez v2, :cond_1 */
/* .line 380 */
v2 = android.text.TextUtils .isEmpty ( p1 );
/* if-nez v2, :cond_4 */
/* const-string/jumbo v2, "tier_all" */
v2 = android.text.TextUtils .equals ( v1,v2 );
if ( v2 != null) { // if-eqz v2, :cond_4
/* .line 381 */
} // :cond_1
(( org.json.JSONObject ) p0 ).optJSONArray ( v1 ); // invoke-virtual {p0, v1}, Lorg/json/JSONObject;->optJSONArray(Ljava/lang/String;)Lorg/json/JSONArray;
/* .line 382 */
/* .local v2, "rsaRegionArray":Lorg/json/JSONArray; */
/* if-nez v2, :cond_2 */
/* .line 383 */
return;
/* .line 385 */
} // :cond_2
int v3 = 0; // const/4 v3, 0x0
/* .local v3, "index":I */
} // :goto_1
v4 = (( org.json.JSONArray ) v2 ).length ( ); // invoke-virtual {v2}, Lorg/json/JSONArray;->length()I
/* if-ge v3, v4, :cond_4 */
/* .line 386 */
(( org.json.JSONArray ) v2 ).optString ( v3 ); // invoke-virtual {v2, v3}, Lorg/json/JSONArray;->optString(I)Ljava/lang/String;
/* .line 387 */
v5 = /* .local v4, "region":Ljava/lang/String; */
/* if-nez v5, :cond_3 */
/* .line 388 */
/* .line 385 */
} // .end local v4 # "region":Ljava/lang/String;
} // :cond_3
/* add-int/lit8 v3, v3, 0x1 */
/* .line 392 */
} // .end local v1 # "rsa":Ljava/lang/String;
} // .end local v2 # "rsaRegionArray":Lorg/json/JSONArray;
} // .end local v3 # "index":I
} // :cond_4
/* .line 393 */
} // :cond_5
return;
/* .line 374 */
} // .end local v0 # "rsaList":Ljava/util/Iterator;, "Ljava/util/Iterator<Ljava/lang/String;>;"
} // :cond_6
} // :goto_2
return;
} // .end method
private static java.util.Set getEnableSettings ( java.lang.String p0, Boolean p1 ) {
/* .locals 12 */
/* .param p0, "pkgName" # Ljava/lang/String; */
/* .param p1, "shouldKeep" # Z */
/* .annotation system Ldalvik/annotation/Signature; */
/* value = { */
/* "(", */
/* "Ljava/lang/String;", */
/* "Z)", */
/* "Ljava/util/Set<", */
/* "Ljava/lang/String;", */
/* ">;" */
/* } */
} // .end annotation
/* .line 252 */
/* new-instance v0, Ljava/util/HashSet; */
/* invoke-direct {v0}, Ljava/util/HashSet;-><init>()V */
/* .line 254 */
/* .local v0, "result":Ljava/util/Set;, "Ljava/util/Set<Ljava/lang/String;>;" */
final String v1 = "persist.sys.carrier.name"; // const-string v1, "persist.sys.carrier.name"
final String v2 = ""; // const-string v2, ""
android.os.SystemProperties .get ( v1,v2 );
/* .line 255 */
/* .local v1, "carrierTagV1":Ljava/lang/String; */
final String v3 = "ro.miui.customized.region"; // const-string v3, "ro.miui.customized.region"
android.os.SystemProperties .get ( v3,v2 );
/* .line 256 */
/* .local v3, "carrierTagV2":Ljava/lang/String; */
final String v4 = "persist.sys.cota.carrier"; // const-string v4, "persist.sys.cota.carrier"
android.os.SystemProperties .get ( v4,v2 );
/* .line 257 */
/* .local v2, "cotaTag":Ljava/lang/String; */
v4 = android.text.TextUtils .isEmpty ( v1 );
if ( v4 != null) { // if-eqz v4, :cond_0
v4 = android.text.TextUtils .isEmpty ( v3 );
if ( v4 != null) { // if-eqz v4, :cond_0
/* .line 258 */
v4 = android.text.TextUtils .isEmpty ( v2 );
/* if-nez v4, :cond_1 */
/* .line 259 */
} // :cond_0
v4 = com.miui.server.EnableStateManager.mContext;
final String v5 = "disable_carriers"; // const-string v5, "disable_carriers"
com.miui.server.EnableStateManager .getStringArray ( v4,p0,v5 );
/* .line 260 */
/* .local v4, "disableCarriers":[Ljava/lang/String; */
if ( v4 != null) { // if-eqz v4, :cond_1
/* array-length v5, v4 */
/* if-lez v5, :cond_1 */
/* .line 261 */
java.util.Arrays .stream ( v4 );
/* new-instance v6, Lcom/miui/server/EnableStateManager$$ExternalSyntheticLambda0; */
v5 = /* invoke-direct {v6, v1, v3, v2}, Lcom/miui/server/EnableStateManager$$ExternalSyntheticLambda0;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V */
if ( v5 != null) { // if-eqz v5, :cond_1
/* .line 264 */
/* .line 267 */
} // .end local v4 # "disableCarriers":[Ljava/lang/String;
} // :cond_1
miui.os.Build .getRegion ( );
/* .line 268 */
/* .local v4, "region":Ljava/lang/String; */
v5 = com.miui.server.EnableStateManager.mCloudEnableSettings;
/* check-cast v5, Ljava/util/List; */
/* .line 269 */
/* .local v5, "regions":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;" */
if ( v5 != null) { // if-eqz v5, :cond_2
/* .line 270 */
/* .line 273 */
} // :cond_2
v6 = com.miui.server.EnableStateManager.mContext;
final String v7 = "enable_regions"; // const-string v7, "enable_regions"
com.miui.server.EnableStateManager .getStringArray ( v6,p0,v7 );
/* .line 274 */
/* .local v6, "apkPresetRegions":[Ljava/lang/String; */
if ( v6 != null) { // if-eqz v6, :cond_3
/* array-length v7, v6 */
/* if-lez v7, :cond_3 */
/* .line 275 */
java.util.Arrays .asList ( v6 );
/* .line 279 */
} // :cond_3
v7 = v7 = com.miui.server.EnableStateManager.sShouldKeepStatePackages;
if ( v7 != null) { // if-eqz v7, :cond_6
/* .line 280 */
/* new-instance v7, Ljava/lang/StringBuilder; */
/* invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V */
final String v8 = "com.xiaomi.market.hasEverEnabled_"; // const-string v8, "com.xiaomi.market.hasEverEnabled_"
(( java.lang.StringBuilder ) v7 ).append ( v8 ); // invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v7 ).append ( p0 ); // invoke-virtual {v7, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v7 ).toString ( ); // invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
com.miui.server.EnableStateManager .getRegionSetByPkgName ( v7 );
/* .line 281 */
v9 = /* .local v7, "hasEnableRegions":Ljava/util/Set;, "Ljava/util/Set<Ljava/lang/String;>;" */
/* if-lez v9, :cond_4 */
/* .line 282 */
/* .line 285 */
} // :cond_4
v9 = com.miui.server.EnableStateManager.TAG;
/* new-instance v10, Ljava/lang/StringBuilder; */
/* invoke-direct {v10}, Ljava/lang/StringBuilder;-><init>()V */
/* const-string/jumbo v11, "shouldKeep: " */
(( java.lang.StringBuilder ) v10 ).append ( v11 ); // invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v10 ).append ( p1 ); // invoke-virtual {v10, p1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;
final String v11 = "\n is "; // const-string v11, "\n is "
(( java.lang.StringBuilder ) v10 ).append ( v11 ); // invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v10 ).append ( p0 ); // invoke-virtual {v10, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
final String v11 = " enable "; // const-string v11, " enable "
(( java.lang.StringBuilder ) v10 ).append ( v11 ); // invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
v11 = com.miui.server.EnableStateManager.mContext;
/* .line 286 */
v11 = com.miui.server.EnableStateManager .isPackageEnabled ( v11,p0 );
(( java.lang.StringBuilder ) v10 ).append ( v11 ); // invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v10 ).toString ( ); // invoke-virtual {v10}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
/* .line 285 */
android.util.Log .d ( v9,v10 );
/* .line 288 */
if ( p1 != null) { // if-eqz p1, :cond_5
v10 = com.miui.server.EnableStateManager.mContext;
v10 = com.miui.server.EnableStateManager .isPackageEnabled ( v10,p0 );
if ( v10 != null) { // if-eqz v10, :cond_5
/* .line 289 */
/* new-instance v10, Ljava/lang/StringBuilder; */
/* invoke-direct {v10}, Ljava/lang/StringBuilder;-><init>()V */
final String v11 = "add "; // const-string v11, "add "
(( java.lang.StringBuilder ) v10 ).append ( v11 ); // invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v10 ).append ( p0 ); // invoke-virtual {v10, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
final String v11 = " at "; // const-string v11, " at "
(( java.lang.StringBuilder ) v10 ).append ( v11 ); // invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v10 ).append ( v4 ); // invoke-virtual {v10, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v10 ).toString ( ); // invoke-virtual {v10}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
android.util.Log .d ( v9,v10 );
/* .line 290 */
/* .line 293 */
} // :cond_5
/* new-instance v9, Ljava/lang/StringBuilder; */
/* invoke-direct {v9}, Ljava/lang/StringBuilder;-><init>()V */
(( java.lang.StringBuilder ) v9 ).append ( v8 ); // invoke-virtual {v9, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v8 ).append ( p0 ); // invoke-virtual {v8, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v8 ).toString ( ); // invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
com.miui.server.EnableStateManager .setRegionSetByPkgName ( v8,v0 );
/* .line 296 */
} // .end local v7 # "hasEnableRegions":Ljava/util/Set;, "Ljava/util/Set<Ljava/lang/String;>;"
} // :cond_6
} // .end method
private static java.lang.String getPackageName ( android.content.Intent p0 ) {
/* .locals 2 */
/* .param p0, "intent" # Landroid/content/Intent; */
/* .line 169 */
int v0 = 0; // const/4 v0, 0x0
/* if-nez p0, :cond_0 */
/* .line 170 */
/* .line 172 */
} // :cond_0
(( android.content.Intent ) p0 ).getData ( ); // invoke-virtual {p0}, Landroid/content/Intent;->getData()Landroid/net/Uri;
/* .line 173 */
/* .local v1, "uri":Landroid/net/Uri; */
if ( v1 != null) { // if-eqz v1, :cond_1
(( android.net.Uri ) v1 ).getSchemeSpecificPart ( ); // invoke-virtual {v1}, Landroid/net/Uri;->getSchemeSpecificPart()Ljava/lang/String;
} // :cond_1
} // .end method
public static java.util.Set getRegionSetByPkgName ( java.lang.String p0 ) {
/* .locals 6 */
/* .param p0, "packageName" # Ljava/lang/String; */
/* .annotation system Ldalvik/annotation/Signature; */
/* value = { */
/* "(", */
/* "Ljava/lang/String;", */
/* ")", */
/* "Ljava/util/Set<", */
/* "Ljava/lang/String;", */
/* ">;" */
/* } */
} // .end annotation
/* .line 443 */
v0 = com.miui.server.EnableStateManager.mContext;
(( android.content.Context ) v0 ).getContentResolver ( ); // invoke-virtual {v0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;
android.provider.Settings$System .getString ( v0,p0 );
/* .line 444 */
/* .local v0, "hasEnableRegions":Ljava/lang/String; */
v1 = com.miui.server.EnableStateManager.TAG;
/* new-instance v2, Ljava/lang/StringBuilder; */
/* invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V */
final String v3 = "getRegionSetByPkgName: "; // const-string v3, "getRegionSetByPkgName: "
(( java.lang.StringBuilder ) v2 ).append ( v3 ); // invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v2 ).append ( v0 ); // invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v2 ).toString ( ); // invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
android.util.Log .d ( v1,v2 );
/* .line 445 */
v1 = android.text.TextUtils .isEmpty ( v0 );
if ( v1 != null) { // if-eqz v1, :cond_0
/* .line 446 */
/* new-instance v1, Ljava/util/HashSet; */
/* invoke-direct {v1}, Ljava/util/HashSet;-><init>()V */
/* .line 448 */
} // :cond_0
final String v1 = ";"; // const-string v1, ";"
(( java.lang.String ) v0 ).split ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;
/* .line 449 */
/* .local v1, "hasEnableRegionList":[Ljava/lang/String; */
/* new-instance v2, Ljava/util/HashSet; */
/* invoke-direct {v2}, Ljava/util/HashSet;-><init>()V */
/* .line 450 */
/* .local v2, "hasEnableRegionSet":Ljava/util/Set;, "Ljava/util/Set<Ljava/lang/String;>;" */
/* array-length v3, v1 */
int v4 = 0; // const/4 v4, 0x0
} // :goto_0
/* if-ge v4, v3, :cond_1 */
/* aget-object v5, v1, v4 */
/* .line 451 */
/* .local v5, "hasEnableRegion":Ljava/lang/String; */
/* .line 450 */
} // .end local v5 # "hasEnableRegion":Ljava/lang/String;
/* add-int/lit8 v4, v4, 0x1 */
/* .line 453 */
} // :cond_1
} // .end method
private static java.lang.String getRomFingerPrint ( ) {
/* .locals 2 */
/* .line 512 */
final String v0 = "ro.build.fingerprint"; // const-string v0, "ro.build.fingerprint"
final String v1 = ""; // const-string v1, ""
android.os.SystemProperties .get ( v0,v1 );
} // .end method
private static java.lang.String getRomRSA ( ) {
/* .locals 2 */
/* .line 504 */
final String v0 = "ro.com.miui.rsa"; // const-string v0, "ro.com.miui.rsa"
final String v1 = ""; // const-string v1, ""
android.os.SystemProperties .get ( v0,v1 );
} // .end method
private static java.lang.String getString ( java.lang.String p0 ) {
/* .locals 1 */
/* .param p0, "key" # Ljava/lang/String; */
/* .line 422 */
v0 = com.miui.server.EnableStateManager.mContext;
(( android.content.Context ) v0 ).getContentResolver ( ); // invoke-virtual {v0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;
android.provider.Settings$System .getString ( v0,p0 );
} // .end method
private static java.lang.String getStringArray ( android.content.Context p0, java.lang.String p1, java.lang.String p2 ) {
/* .locals 4 */
/* .param p0, "context" # Landroid/content/Context; */
/* .param p1, "pkgName" # Ljava/lang/String; */
/* .param p2, "resName" # Ljava/lang/String; */
/* .line 302 */
int v0 = 0; // const/4 v0, 0x0
try { // :try_start_0
(( android.content.Context ) p0 ).createPackageContext ( p1, v0 ); // invoke-virtual {p0, p1, v0}, Landroid/content/Context;->createPackageContext(Ljava/lang/String;I)Landroid/content/Context;
/* .line 303 */
/* .local v1, "fContext":Landroid/content/Context; */
(( android.content.Context ) v1 ).getResources ( ); // invoke-virtual {v1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;
final String v3 = "array"; // const-string v3, "array"
v2 = (( android.content.res.Resources ) v2 ).getIdentifier ( p2, v3, p1 ); // invoke-virtual {v2, p2, v3, p1}, Landroid/content/res/Resources;->getIdentifier(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)I
/* .line 305 */
/* .local v2, "id":I */
(( android.content.Context ) v1 ).getResources ( ); // invoke-virtual {v1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;
(( android.content.res.Resources ) v3 ).getStringArray ( v2 ); // invoke-virtual {v3, v2}, Landroid/content/res/Resources;->getStringArray(I)[Ljava/lang/String;
/* :try_end_0 */
/* .catch Ljava/lang/Exception; {:try_start_0 ..:try_end_0} :catch_0 */
/* .line 306 */
} // .end local v1 # "fContext":Landroid/content/Context;
} // .end local v2 # "id":I
/* :catch_0 */
/* move-exception v1 */
/* .line 307 */
/* .local v1, "e":Ljava/lang/Exception; */
v2 = com.miui.server.EnableStateManager.TAG;
(( java.lang.Exception ) v1 ).toString ( ); // invoke-virtual {v1}, Ljava/lang/Exception;->toString()Ljava/lang/String;
android.util.Log .e ( v2,v3,v1 );
/* .line 309 */
} // .end local v1 # "e":Ljava/lang/Exception;
/* new-array v0, v0, [Ljava/lang/String; */
} // .end method
private static Boolean isAppInstalled ( java.lang.String p0 ) {
/* .locals 4 */
/* .param p0, "pkgName" # Ljava/lang/String; */
/* .line 178 */
int v0 = 0; // const/4 v0, 0x0
try { // :try_start_0
v1 = com.miui.server.EnableStateManager.mContext;
(( android.content.Context ) v1 ).getPackageManager ( ); // invoke-virtual {v1}, Landroid/content/Context;->getPackageManager()Landroid/content/pm/PackageManager;
/* .line 179 */
/* .local v1, "pm":Landroid/content/pm/PackageManager; */
(( android.content.pm.PackageManager ) v1 ).getApplicationInfo ( p0, v0 ); // invoke-virtual {v1, p0, v0}, Landroid/content/pm/PackageManager;->getApplicationInfo(Ljava/lang/String;I)Landroid/content/pm/ApplicationInfo;
/* :try_end_0 */
/* .catch Ljava/lang/Exception; {:try_start_0 ..:try_end_0} :catch_0 */
if ( v2 != null) { // if-eqz v2, :cond_0
/* .line 180 */
int v0 = 1; // const/4 v0, 0x1
/* .line 184 */
} // .end local v1 # "pm":Landroid/content/pm/PackageManager;
} // :cond_0
/* .line 182 */
/* :catch_0 */
/* move-exception v1 */
/* .line 183 */
/* .local v1, "e":Ljava/lang/Exception; */
v2 = com.miui.server.EnableStateManager.TAG;
(( java.lang.Exception ) v1 ).toString ( ); // invoke-virtual {v1}, Ljava/lang/Exception;->toString()Ljava/lang/String;
android.util.Log .e ( v2,v3,v1 );
/* .line 185 */
} // .end local v1 # "e":Ljava/lang/Exception;
} // :goto_0
} // .end method
private static Boolean isFirstBoot ( ) {
/* .locals 2 */
/* .line 562 */
com.android.server.pm.PackageManagerServiceStub .get ( );
(( com.android.server.pm.PackageManagerServiceStub ) v0 ).getService ( ); // invoke-virtual {v0}, Lcom/android/server/pm/PackageManagerServiceStub;->getService()Lcom/android/server/pm/PackageManagerService;
/* .line 563 */
/* .local v0, "pms":Lcom/android/server/pm/PackageManagerService; */
v1 = (( com.android.server.pm.PackageManagerService ) v0 ).isFirstBoot ( ); // invoke-virtual {v0}, Lcom/android/server/pm/PackageManagerService;->isFirstBoot()Z
} // .end method
private static Boolean isMexicanOperators ( java.lang.String p0 ) {
/* .locals 1 */
/* .param p0, "customizedRegion" # Ljava/lang/String; */
/* .line 111 */
final String v0 = "lm_cr"; // const-string v0, "lm_cr"
v0 = (( java.lang.String ) p0 ).equalsIgnoreCase ( v0 ); // invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z
/* if-nez v0, :cond_1 */
/* .line 112 */
final String v0 = "mx_telcel"; // const-string v0, "mx_telcel"
v0 = (( java.lang.String ) p0 ).equalsIgnoreCase ( v0 ); // invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z
/* if-nez v0, :cond_1 */
/* .line 113 */
final String v0 = "mx_at"; // const-string v0, "mx_at"
v0 = (( java.lang.String ) p0 ).equalsIgnoreCase ( v0 ); // invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z
if ( v0 != null) { // if-eqz v0, :cond_0
/* .line 116 */
} // :cond_0
int v0 = 0; // const/4 v0, 0x0
/* .line 114 */
} // :cond_1
} // :goto_0
int v0 = 1; // const/4 v0, 0x1
} // .end method
private static Boolean isPackageEnabled ( android.content.Context p0, java.lang.String p1 ) {
/* .locals 6 */
/* .param p0, "context" # Landroid/content/Context; */
/* .param p1, "packageName" # Ljava/lang/String; */
/* .line 397 */
int v0 = 1; // const/4 v0, 0x1
try { // :try_start_0
(( android.content.Context ) p0 ).getPackageManager ( ); // invoke-virtual {p0}, Landroid/content/Context;->getPackageManager()Landroid/content/pm/PackageManager;
/* .line 398 */
/* .local v1, "pm":Landroid/content/pm/PackageManager; */
v2 = (( android.content.pm.PackageManager ) v1 ).getApplicationEnabledSetting ( p1 ); // invoke-virtual {v1, p1}, Landroid/content/pm/PackageManager;->getApplicationEnabledSetting(Ljava/lang/String;)I
/* .line 399 */
/* .local v2, "state":I */
v3 = com.miui.server.EnableStateManager.TAG;
/* new-instance v4, Ljava/lang/StringBuilder; */
/* invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V */
(( java.lang.StringBuilder ) v4 ).append ( p1 ); // invoke-virtual {v4, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
final String v5 = " state is "; // const-string v5, " state is "
(( java.lang.StringBuilder ) v4 ).append ( v5 ); // invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v4 ).append ( v2 ); // invoke-virtual {v4, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v4 ).toString ( ); // invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
android.util.Log .d ( v3,v4 );
/* .line 400 */
int v3 = 0; // const/4 v3, 0x0
/* packed-switch v2, :pswitch_data_0 */
/* .line 409 */
/* .line 402 */
/* :pswitch_0 */
/* .line 407 */
/* :pswitch_1 */
(( android.content.pm.PackageManager ) v1 ).getApplicationInfo ( p1, v3 ); // invoke-virtual {v1, p1, v3}, Landroid/content/pm/PackageManager;->getApplicationInfo(Ljava/lang/String;I)Landroid/content/pm/ApplicationInfo;
/* iget-boolean v0, v3, Landroid/content/pm/ApplicationInfo;->enabled:Z */
/* :try_end_0 */
/* .catch Ljava/lang/Exception; {:try_start_0 ..:try_end_0} :catch_0 */
/* .line 411 */
} // .end local v1 # "pm":Landroid/content/pm/PackageManager;
} // .end local v2 # "state":I
/* :catch_0 */
/* move-exception v1 */
/* .line 412 */
/* .local v1, "e":Ljava/lang/Exception; */
v2 = com.miui.server.EnableStateManager.TAG;
(( java.lang.Exception ) v1 ).getMessage ( ); // invoke-virtual {v1}, Ljava/lang/Exception;->getMessage()Ljava/lang/String;
android.util.Log .e ( v2,v3,v1 );
/* .line 414 */
} // .end local v1 # "e":Ljava/lang/Exception;
/* nop */
/* :pswitch_data_0 */
/* .packed-switch 0x0 */
/* :pswitch_1 */
/* :pswitch_0 */
/* :pswitch_1 */
/* :pswitch_1 */
/* :pswitch_1 */
} // .end packed-switch
} // .end method
public static Boolean isProvisioned ( android.content.Context p0 ) {
/* .locals 3 */
/* .param p0, "context" # Landroid/content/Context; */
/* .line 492 */
int v0 = 0; // const/4 v0, 0x0
/* if-nez p0, :cond_0 */
/* .line 493 */
/* .line 495 */
} // :cond_0
(( android.content.Context ) p0 ).getContentResolver ( ); // invoke-virtual {p0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;
final String v2 = "device_provisioned"; // const-string v2, "device_provisioned"
v1 = android.provider.Settings$Global .getInt ( v1,v2,v0 );
int v2 = 1; // const/4 v2, 0x1
/* if-ne v1, v2, :cond_1 */
/* move v0, v2 */
} // :cond_1
} // .end method
private static Boolean isRomOTAd ( ) {
/* .locals 4 */
/* .line 540 */
int v0 = 0; // const/4 v0, 0x0
/* .line 541 */
/* .local v0, "isOTAd":Z */
v1 = com.miui.server.EnableStateManager .isFirstBoot ( );
final String v2 = "enable_state_manager_first_finger_print"; // const-string v2, "enable_state_manager_first_finger_print"
if ( v1 != null) { // if-eqz v1, :cond_0
/* .line 542 */
v1 = com.miui.server.EnableStateManager.TAG;
/* const-string/jumbo v3, "update mipicks firstBoot" */
android.util.Log .i ( v1,v3 );
/* .line 543 */
v1 = com.miui.server.EnableStateManager.mContext;
(( android.content.Context ) v1 ).getContentResolver ( ); // invoke-virtual {v1}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;
com.miui.server.EnableStateManager .getRomFingerPrint ( );
android.provider.Settings$Secure .putString ( v1,v2,v3 );
/* .line 544 */
int v0 = 0; // const/4 v0, 0x0
/* .line 546 */
} // :cond_0
v1 = com.miui.server.EnableStateManager.mContext;
(( android.content.Context ) v1 ).getContentResolver ( ); // invoke-virtual {v1}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;
android.provider.Settings$Secure .getString ( v1,v2 );
/* .line 547 */
/* .local v1, "firstFP":Ljava/lang/String; */
if ( v1 != null) { // if-eqz v1, :cond_1
com.miui.server.EnableStateManager .getRomFingerPrint ( );
v2 = (( java.lang.String ) v1 ).equals ( v2 ); // invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z
if ( v2 != null) { // if-eqz v2, :cond_1
/* .line 548 */
v2 = com.miui.server.EnableStateManager.TAG;
/* const-string/jumbo v3, "update mipicks normal" */
android.util.Log .i ( v2,v3 );
/* .line 549 */
int v0 = 0; // const/4 v0, 0x0
/* .line 551 */
} // :cond_1
int v0 = 1; // const/4 v0, 0x1
/* .line 554 */
} // .end local v1 # "firstFP":Ljava/lang/String;
} // :goto_0
} // .end method
static Boolean lambda$getEnableSettings$0 ( java.lang.String p0, java.lang.String p1, java.lang.String p2, java.lang.String p3 ) { //synthethic
/* .locals 1 */
/* .param p0, "carrierTagV1" # Ljava/lang/String; */
/* .param p1, "carrierTagV2" # Ljava/lang/String; */
/* .param p2, "cotaTag" # Ljava/lang/String; */
/* .param p3, "carrierTag" # Ljava/lang/String; */
/* .line 262 */
v0 = (( java.lang.String ) p3 ).equals ( p0 ); // invoke-virtual {p3, p0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z
/* if-nez v0, :cond_1 */
v0 = (( java.lang.String ) p3 ).equals ( p1 ); // invoke-virtual {p3, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z
/* if-nez v0, :cond_1 */
/* .line 263 */
v0 = (( java.lang.String ) p3 ).equals ( p2 ); // invoke-virtual {p3, p2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z
if ( v0 != null) { // if-eqz v0, :cond_0
} // :cond_0
int v0 = 0; // const/4 v0, 0x0
} // :cond_1
} // :goto_0
int v0 = 1; // const/4 v0, 0x1
/* .line 262 */
} // :goto_1
} // .end method
private static void putCustomizedJSONObject ( org.json.JSONObject p0 ) {
/* .locals 10 */
/* .param p0, "jsonObject" # Lorg/json/JSONObject; */
/* .line 460 */
final String v0 = "ro.miui.customized.region"; // const-string v0, "ro.miui.customized.region"
final String v1 = ""; // const-string v1, ""
android.os.SystemProperties .get ( v0,v1 );
/* .line 461 */
/* .local v0, "region":Ljava/lang/String; */
v1 = android.text.TextUtils .isEmpty ( v0 );
/* if-nez v1, :cond_2 */
/* .line 462 */
int v1 = 0; // const/4 v1, 0x0
/* .line 464 */
/* .local v1, "reader":Ljava/io/BufferedReader; */
try { // :try_start_0
/* new-instance v2, Ljava/io/BufferedReader; */
/* new-instance v3, Ljava/io/InputStreamReader; */
v4 = com.miui.server.EnableStateManager.mContext;
/* .line 465 */
(( android.content.Context ) v4 ).getResources ( ); // invoke-virtual {v4}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;
/* const v5, 0x110e0013 */
(( android.content.res.Resources ) v4 ).openRawResource ( v5 ); // invoke-virtual {v4, v5}, Landroid/content/res/Resources;->openRawResource(I)Ljava/io/InputStream;
/* invoke-direct {v3, v4}, Ljava/io/InputStreamReader;-><init>(Ljava/io/InputStream;)V */
/* invoke-direct {v2, v3}, Ljava/io/BufferedReader;-><init>(Ljava/io/Reader;)V */
/* move-object v1, v2 */
/* .line 466 */
/* new-instance v2, Ljava/lang/StringBuilder; */
/* invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V */
/* .line 468 */
/* .local v2, "sb":Ljava/lang/StringBuilder; */
} // :goto_0
(( java.io.BufferedReader ) v1 ).readLine ( ); // invoke-virtual {v1}, Ljava/io/BufferedReader;->readLine()Ljava/lang/String;
/* move-object v4, v3 */
/* .local v4, "temp":Ljava/lang/String; */
if ( v3 != null) { // if-eqz v3, :cond_0
/* .line 469 */
(( java.lang.StringBuilder ) v2 ).append ( v4 ); // invoke-virtual {v2, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
/* .line 471 */
} // :cond_0
/* new-instance v3, Lorg/json/JSONObject; */
(( java.lang.StringBuilder ) v2 ).toString ( ); // invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
/* invoke-direct {v3, v5}, Lorg/json/JSONObject;-><init>(Ljava/lang/String;)V */
/* .line 472 */
/* .local v3, "json":Lorg/json/JSONObject; */
(( org.json.JSONObject ) v3 ).keys ( ); // invoke-virtual {v3}, Lorg/json/JSONObject;->keys()Ljava/util/Iterator;
/* .line 473 */
/* .local v5, "pkgNameList":Ljava/util/Iterator;, "Ljava/util/Iterator<Ljava/lang/String;>;" */
v6 = } // :goto_1
if ( v6 != null) { // if-eqz v6, :cond_1
/* .line 474 */
/* check-cast v6, Ljava/lang/String; */
/* .line 475 */
/* .local v6, "pkgName":Ljava/lang/String; */
v7 = com.miui.server.EnableStateManager.TAG;
/* new-instance v8, Ljava/lang/StringBuilder; */
/* invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V */
final String v9 = "op_enable_list pkgName: "; // const-string v9, "op_enable_list pkgName: "
(( java.lang.StringBuilder ) v8 ).append ( v9 ); // invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v8 ).append ( v6 ); // invoke-virtual {v8, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v8 ).toString ( ); // invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
android.util.Log .d ( v7,v8 );
/* .line 476 */
(( org.json.JSONObject ) v3 ).getJSONObject ( v6 ); // invoke-virtual {v3, v6}, Lorg/json/JSONObject;->getJSONObject(Ljava/lang/String;)Lorg/json/JSONObject;
/* .line 477 */
/* .local v7, "settingJson":Lorg/json/JSONObject; */
(( org.json.JSONObject ) p0 ).putOpt ( v6, v7 ); // invoke-virtual {p0, v6, v7}, Lorg/json/JSONObject;->putOpt(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;
/* :try_end_0 */
/* .catch Ljava/lang/Exception; {:try_start_0 ..:try_end_0} :catch_0 */
/* .catchall {:try_start_0 ..:try_end_0} :catchall_0 */
/* .line 478 */
/* nop */
} // .end local v6 # "pkgName":Ljava/lang/String;
} // .end local v7 # "settingJson":Lorg/json/JSONObject;
/* .line 483 */
} // .end local v2 # "sb":Ljava/lang/StringBuilder;
} // .end local v3 # "json":Lorg/json/JSONObject;
} // .end local v4 # "temp":Ljava/lang/String;
} // .end local v5 # "pkgNameList":Ljava/util/Iterator;, "Ljava/util/Iterator<Ljava/lang/String;>;"
} // :cond_1
try { // :try_start_1
(( java.io.BufferedReader ) v1 ).close ( ); // invoke-virtual {v1}, Ljava/io/BufferedReader;->close()V
/* :try_end_1 */
/* .catch Ljava/lang/Exception; {:try_start_1 ..:try_end_1} :catch_1 */
/* .line 482 */
/* :catchall_0 */
/* move-exception v2 */
/* .line 479 */
/* :catch_0 */
/* move-exception v2 */
/* .line 480 */
/* .local v2, "e":Ljava/lang/Exception; */
try { // :try_start_2
v3 = com.miui.server.EnableStateManager.TAG;
(( java.lang.Exception ) v2 ).toString ( ); // invoke-virtual {v2}, Ljava/lang/Exception;->toString()Ljava/lang/String;
android.util.Log .e ( v3,v4,v2 );
/* :try_end_2 */
/* .catchall {:try_start_2 ..:try_end_2} :catchall_0 */
/* .line 483 */
} // .end local v2 # "e":Ljava/lang/Exception;
try { // :try_start_3
(( java.io.BufferedReader ) v1 ).close ( ); // invoke-virtual {v1}, Ljava/io/BufferedReader;->close()V
/* :try_end_3 */
/* .catch Ljava/lang/Exception; {:try_start_3 ..:try_end_3} :catch_1 */
/* .line 486 */
} // :goto_2
/* .line 484 */
/* :catch_1 */
/* move-exception v2 */
/* .line 485 */
/* .restart local v2 # "e":Ljava/lang/Exception; */
v3 = com.miui.server.EnableStateManager.TAG;
(( java.lang.Exception ) v2 ).toString ( ); // invoke-virtual {v2}, Ljava/lang/Exception;->toString()Ljava/lang/String;
android.util.Log .e ( v3,v4,v2 );
/* .line 487 */
} // .end local v2 # "e":Ljava/lang/Exception;
/* .line 483 */
} // :goto_3
try { // :try_start_4
(( java.io.BufferedReader ) v1 ).close ( ); // invoke-virtual {v1}, Ljava/io/BufferedReader;->close()V
/* :try_end_4 */
/* .catch Ljava/lang/Exception; {:try_start_4 ..:try_end_4} :catch_2 */
/* .line 486 */
/* .line 484 */
/* :catch_2 */
/* move-exception v3 */
/* .line 485 */
/* .local v3, "e":Ljava/lang/Exception; */
v4 = com.miui.server.EnableStateManager.TAG;
(( java.lang.Exception ) v3 ).toString ( ); // invoke-virtual {v3}, Ljava/lang/Exception;->toString()Ljava/lang/String;
android.util.Log .e ( v4,v5,v3 );
/* .line 487 */
} // .end local v3 # "e":Ljava/lang/Exception;
} // :goto_4
/* throw v2 */
/* .line 489 */
} // .end local v1 # "reader":Ljava/io/BufferedReader;
} // :cond_2
} // :goto_5
return;
} // .end method
private static void registerReceiverIfNeed ( ) {
/* .locals 1 */
/* .line 120 */
/* sget-boolean v0, Lcom/miui/server/EnableStateManager;->receiverRegistered:Z */
if ( v0 != null) { // if-eqz v0, :cond_0
/* .line 121 */
return;
/* .line 123 */
} // :cond_0
int v0 = 1; // const/4 v0, 0x1
com.miui.server.EnableStateManager.receiverRegistered = (v0!= 0);
/* .line 124 */
/* new-instance v0, Lcom/miui/server/EnableStateManager$1; */
/* invoke-direct {v0}, Lcom/miui/server/EnableStateManager$1;-><init>()V */
/* .line 137 */
(( com.miui.server.EnableStateManager$1 ) v0 ).start ( ); // invoke-virtual {v0}, Lcom/miui/server/EnableStateManager$1;->start()V
/* .line 138 */
return;
} // .end method
public static void setRegionSetByPkgName ( java.lang.String p0, java.util.Set p1 ) {
/* .locals 5 */
/* .param p0, "packageName" # Ljava/lang/String; */
/* .annotation system Ldalvik/annotation/Signature; */
/* value = { */
/* "(", */
/* "Ljava/lang/String;", */
/* "Ljava/util/Set<", */
/* "Ljava/lang/String;", */
/* ">;)V" */
/* } */
} // .end annotation
/* .line 426 */
/* .local p1, "regions":Ljava/util/Set;, "Ljava/util/Set<Ljava/lang/String;>;" */
final String v0 = ""; // const-string v0, ""
/* .line 427 */
/* .local v0, "tempResult":Ljava/lang/String; */
/* .line 428 */
/* .local v1, "iter":Ljava/util/Iterator;, "Ljava/util/Iterator<Ljava/lang/String;>;" */
/* if-nez v1, :cond_0 */
/* .line 429 */
return;
/* .line 431 */
} // :cond_0
v2 = } // :goto_0
if ( v2 != null) { // if-eqz v2, :cond_2
/* .line 432 */
v2 = android.text.TextUtils .isEmpty ( v0 );
if ( v2 != null) { // if-eqz v2, :cond_1
/* .line 433 */
/* new-instance v2, Ljava/lang/StringBuilder; */
/* invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V */
(( java.lang.StringBuilder ) v2 ).append ( v0 ); // invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
/* check-cast v3, Ljava/lang/String; */
(( java.lang.StringBuilder ) v2 ).append ( v3 ); // invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v2 ).toString ( ); // invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
/* .line 435 */
} // :cond_1
/* new-instance v2, Ljava/lang/StringBuilder; */
/* invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V */
(( java.lang.StringBuilder ) v2 ).append ( v0 ); // invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
final String v3 = ";"; // const-string v3, ";"
(( java.lang.StringBuilder ) v2 ).append ( v3 ); // invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
/* check-cast v3, Ljava/lang/String; */
(( java.lang.StringBuilder ) v2 ).append ( v3 ); // invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v2 ).toString ( ); // invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
/* .line 438 */
} // :cond_2
v2 = com.miui.server.EnableStateManager.TAG;
/* new-instance v3, Ljava/lang/StringBuilder; */
/* invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V */
/* const-string/jumbo v4, "setRegionSetByPkgName: " */
(( java.lang.StringBuilder ) v3 ).append ( v4 ); // invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v3 ).append ( v0 ); // invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v3 ).toString ( ); // invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
android.util.Log .d ( v2,v3 );
/* .line 439 */
v2 = com.miui.server.EnableStateManager.mContext;
(( android.content.Context ) v2 ).getContentResolver ( ); // invoke-virtual {v2}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;
android.provider.Settings$System .putString ( v2,p0,v0 );
/* .line 440 */
return;
} // .end method
private static void setString ( java.lang.String p0, java.lang.String p1 ) {
/* .locals 1 */
/* .param p0, "key" # Ljava/lang/String; */
/* .param p1, "value" # Ljava/lang/String; */
/* .line 418 */
v0 = com.miui.server.EnableStateManager.mContext;
(( android.content.Context ) v0 ).getContentResolver ( ); // invoke-virtual {v0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;
android.provider.Settings$System .putString ( v0,p0,p1 );
/* .line 419 */
return;
} // .end method
private static void tryDisablePkg ( java.lang.String p0 ) {
/* .locals 4 */
/* .param p0, "pkgName" # Ljava/lang/String; */
/* .line 224 */
try { // :try_start_0
v0 = com.miui.server.EnableStateManager.mContext;
(( android.content.Context ) v0 ).getPackageManager ( ); // invoke-virtual {v0}, Landroid/content/Context;->getPackageManager()Landroid/content/pm/PackageManager;
/* .line 225 */
/* .local v0, "pm":Landroid/content/pm/PackageManager; */
v1 = com.miui.server.EnableStateManager.mContext;
(( android.content.Context ) v1 ).getPackageManager ( ); // invoke-virtual {v1}, Landroid/content/Context;->getPackageManager()Landroid/content/pm/PackageManager;
v1 = (( android.content.pm.PackageManager ) v1 ).getApplicationEnabledSetting ( p0 ); // invoke-virtual {v1, p0}, Landroid/content/pm/PackageManager;->getApplicationEnabledSetting(Ljava/lang/String;)I
/* .line 226 */
/* .local v1, "state":I */
if ( v1 != null) { // if-eqz v1, :cond_0
int v2 = 1; // const/4 v2, 0x1
/* if-ne v1, v2, :cond_1 */
/* .line 228 */
} // :cond_0
int v2 = 2; // const/4 v2, 0x2
int v3 = 0; // const/4 v3, 0x0
(( android.content.pm.PackageManager ) v0 ).setApplicationEnabledSetting ( p0, v2, v3 ); // invoke-virtual {v0, p0, v2, v3}, Landroid/content/pm/PackageManager;->setApplicationEnabledSetting(Ljava/lang/String;II)V
/* :try_end_0 */
/* .catch Ljava/lang/Exception; {:try_start_0 ..:try_end_0} :catch_0 */
/* .line 234 */
} // .end local v0 # "pm":Landroid/content/pm/PackageManager;
} // .end local v1 # "state":I
} // :cond_1
/* .line 231 */
/* :catch_0 */
/* move-exception v0 */
/* .line 232 */
/* .local v0, "e":Ljava/lang/Exception; */
v1 = com.miui.server.EnableStateManager.TAG;
(( java.lang.Exception ) v0 ).toString ( ); // invoke-virtual {v0}, Ljava/lang/Exception;->toString()Ljava/lang/String;
android.util.Log .e ( v1,v2,v0 );
/* .line 235 */
} // .end local v0 # "e":Ljava/lang/Exception;
} // :goto_0
return;
} // .end method
private static void tryEnableMiPicks ( ) {
/* .locals 5 */
/* .line 519 */
com.miui.server.EnableStateManager .getRomRSA ( );
/* .line 520 */
/* .local v0, "romRsa":Ljava/lang/String; */
final String v1 = "ro.com.google.clientidbase.ms"; // const-string v1, "ro.com.google.clientidbase.ms"
final String v2 = ""; // const-string v2, ""
android.os.SystemProperties .get ( v1,v2 );
/* .line 522 */
/* .local v1, "romClientId":Ljava/lang/String; */
/* const-string/jumbo v2, "tier1" */
v2 = (( java.lang.String ) v2 ).equals ( v0 ); // invoke-virtual {v2, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z
final String v3 = "com.xiaomi.mipicks"; // const-string v3, "com.xiaomi.mipicks"
/* if-nez v2, :cond_0 */
final String v2 = "android-xiaomi-rvo3"; // const-string v2, "android-xiaomi-rvo3"
v2 = (( java.lang.String ) v2 ).equals ( v1 ); // invoke-virtual {v2, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z
/* if-nez v2, :cond_0 */
/* .line 523 */
com.miui.server.EnableStateManager .tryEnablePkg ( v3 );
/* .line 524 */
v2 = com.miui.server.EnableStateManager.TAG;
final String v3 = "enable mipicks"; // const-string v3, "enable mipicks"
android.util.Log .i ( v2,v3 );
/* .line 525 */
return;
/* .line 527 */
} // :cond_0
v2 = com.miui.server.EnableStateManager .isRomOTAd ( );
if ( v2 != null) { // if-eqz v2, :cond_1
/* .line 528 */
v2 = com.miui.server.EnableStateManager.TAG;
final String v3 = "keep origin because ota"; // const-string v3, "keep origin because ota"
android.util.Log .i ( v2,v3 );
/* .line 529 */
return;
/* .line 531 */
} // :cond_1
v2 = com.miui.server.EnableStateManager.TAG;
final String v4 = "disable mipicks"; // const-string v4, "disable mipicks"
android.util.Log .i ( v2,v4 );
/* .line 532 */
com.miui.server.EnableStateManager .tryDisablePkg ( v3 );
/* .line 533 */
return;
} // .end method
private static void tryEnablePkg ( java.lang.String p0 ) {
/* .locals 4 */
/* .param p0, "pkgName" # Ljava/lang/String; */
/* .line 239 */
try { // :try_start_0
v0 = com.miui.server.EnableStateManager.mContext;
(( android.content.Context ) v0 ).getPackageManager ( ); // invoke-virtual {v0}, Landroid/content/Context;->getPackageManager()Landroid/content/pm/PackageManager;
/* .line 240 */
/* .local v0, "pm":Landroid/content/pm/PackageManager; */
v1 = com.miui.server.EnableStateManager.mContext;
(( android.content.Context ) v1 ).getPackageManager ( ); // invoke-virtual {v1}, Landroid/content/Context;->getPackageManager()Landroid/content/pm/PackageManager;
v1 = (( android.content.pm.PackageManager ) v1 ).getApplicationEnabledSetting ( p0 ); // invoke-virtual {v1, p0}, Landroid/content/pm/PackageManager;->getApplicationEnabledSetting(Ljava/lang/String;)I
/* .line 241 */
/* .local v1, "state":I */
int v2 = 2; // const/4 v2, 0x2
/* if-ne v1, v2, :cond_0 */
/* .line 242 */
int v2 = 1; // const/4 v2, 0x1
int v3 = 0; // const/4 v3, 0x0
(( android.content.pm.PackageManager ) v0 ).setApplicationEnabledSetting ( p0, v2, v3 ); // invoke-virtual {v0, p0, v2, v3}, Landroid/content/pm/PackageManager;->setApplicationEnabledSetting(Ljava/lang/String;II)V
/* :try_end_0 */
/* .catch Ljava/lang/Exception; {:try_start_0 ..:try_end_0} :catch_0 */
/* .line 248 */
} // .end local v0 # "pm":Landroid/content/pm/PackageManager;
} // .end local v1 # "state":I
} // :cond_0
/* .line 245 */
/* :catch_0 */
/* move-exception v0 */
/* .line 246 */
/* .local v0, "e":Ljava/lang/Exception; */
v1 = com.miui.server.EnableStateManager.TAG;
(( java.lang.Exception ) v0 ).toString ( ); // invoke-virtual {v0}, Ljava/lang/Exception;->toString()Ljava/lang/String;
android.util.Log .e ( v1,v2,v0 );
/* .line 249 */
} // .end local v0 # "e":Ljava/lang/Exception;
} // :goto_0
return;
} // .end method
public static synchronized void updateApplicationEnableState ( android.content.Context p0 ) {
/* .locals 9 */
/* .param p0, "context" # Landroid/content/Context; */
/* const-class v0, Lcom/miui/server/EnableStateManager; */
/* monitor-enter v0 */
/* .line 69 */
try { // :try_start_0
/* sget-boolean v1, Lmiui/os/Build;->IS_INTERNATIONAL_BUILD:Z */
/* :try_end_0 */
/* .catchall {:try_start_0 ..:try_end_0} :catchall_0 */
/* if-nez v1, :cond_0 */
/* .line 70 */
/* monitor-exit v0 */
return;
/* .line 72 */
} // :cond_0
try { // :try_start_1
v1 = com.miui.server.EnableStateManager.mContext;
/* if-nez v1, :cond_2 */
/* .line 73 */
if ( p0 != null) { // if-eqz p0, :cond_1
/* .line 74 */
/* .line 76 */
} // :cond_1
v1 = com.miui.server.EnableStateManager.TAG;
final String v2 = "no context"; // const-string v2, "no context"
android.util.Log .i ( v1,v2 );
/* :try_end_1 */
/* .catchall {:try_start_1 ..:try_end_1} :catchall_0 */
/* .line 77 */
/* monitor-exit v0 */
return;
/* .line 80 */
} // :cond_2
} // :goto_0
try { // :try_start_2
v1 = com.miui.server.EnableStateManager.mContext;
(( android.content.Context ) v1 ).getContentResolver ( ); // invoke-virtual {v1}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;
final String v2 = "has_update_application_state"; // const-string v2, "has_update_application_state"
/* const-string/jumbo v3, "true" */
android.provider.Settings$System .putString ( v1,v2,v3 );
/* .line 82 */
int v1 = 0; // const/4 v1, 0x0
/* .line 83 */
/* .local v1, "shouldKeep":Z */
com.miui.server.EnableStateManager .updateApplicationEnableStateInner ( v1 );
/* .line 85 */
v2 = com.miui.server.EnableStateManager.mContext;
(( android.content.Context ) v2 ).getPackageManager ( ); // invoke-virtual {v2}, Landroid/content/Context;->getPackageManager()Landroid/content/pm/PackageManager;
/* .line 86 */
/* .local v2, "pm":Landroid/content/pm/PackageManager; */
v3 = (( android.content.pm.PackageManager ) v2 ).isDeviceUpgrading ( ); // invoke-virtual {v2}, Landroid/content/pm/PackageManager;->isDeviceUpgrading()Z
/* :try_end_2 */
/* .catchall {:try_start_2 ..:try_end_2} :catchall_0 */
if ( v3 != null) { // if-eqz v3, :cond_4
/* .line 88 */
try { // :try_start_3
final String v3 = "com.mi.global.shop"; // const-string v3, "com.mi.global.shop"
int v4 = 0; // const/4 v4, 0x0
(( android.content.pm.PackageManager ) v2 ).getApplicationInfo ( v3, v4 ); // invoke-virtual {v2, v3, v4}, Landroid/content/pm/PackageManager;->getApplicationInfo(Ljava/lang/String;I)Landroid/content/pm/ApplicationInfo;
/* .line 89 */
/* .local v3, "applicationInfo":Landroid/content/pm/ApplicationInfo; */
if ( v3 != null) { // if-eqz v3, :cond_3
/* .line 90 */
v5 = this.sourceDir;
/* .line 91 */
/* .local v5, "gbShopPath":Ljava/lang/String; */
v6 = android.text.TextUtils .isEmpty ( v5 );
/* if-nez v6, :cond_3 */
final String v6 = "/data/app/MISTORE_OVERSEA/base.apk"; // const-string v6, "/data/app/MISTORE_OVERSEA/base.apk"
/* .line 92 */
v6 = (( java.lang.String ) v5 ).equals ( v6 ); // invoke-virtual {v5, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z
if ( v6 != null) { // if-eqz v6, :cond_3
/* .line 93 */
final String v6 = "ro.miui.customized.region"; // const-string v6, "ro.miui.customized.region"
final String v7 = ""; // const-string v7, ""
/* .line 94 */
android.os.SystemProperties .get ( v6,v7 );
/* .line 95 */
/* .local v6, "customizedRegion":Ljava/lang/String; */
v7 = android.text.TextUtils .isEmpty ( v6 );
/* if-nez v7, :cond_3 */
/* .line 96 */
v7 = com.miui.server.EnableStateManager .isMexicanOperators ( v6 );
if ( v7 != null) { // if-eqz v7, :cond_3
/* .line 97 */
final String v7 = "com.mi.global.shop"; // const-string v7, "com.mi.global.shop"
int v8 = 0; // const/4 v8, 0x0
(( android.content.pm.PackageManager ) v2 ).deletePackage ( v7, v8, v4 ); // invoke-virtual {v2, v7, v8, v4}, Landroid/content/pm/PackageManager;->deletePackage(Ljava/lang/String;Landroid/content/pm/IPackageDeleteObserver;I)V
/* :try_end_3 */
/* .catch Landroid/content/pm/PackageManager$NameNotFoundException; {:try_start_3 ..:try_end_3} :catch_0 */
/* .catchall {:try_start_3 ..:try_end_3} :catchall_0 */
/* .line 103 */
} // .end local v3 # "applicationInfo":Landroid/content/pm/ApplicationInfo;
} // .end local v5 # "gbShopPath":Ljava/lang/String;
} // .end local v6 # "customizedRegion":Ljava/lang/String;
} // :cond_3
/* .line 101 */
/* :catch_0 */
/* move-exception v3 */
/* .line 102 */
/* .local v3, "e":Landroid/content/pm/PackageManager$NameNotFoundException; */
try { // :try_start_4
(( android.content.pm.PackageManager$NameNotFoundException ) v3 ).printStackTrace ( ); // invoke-virtual {v3}, Landroid/content/pm/PackageManager$NameNotFoundException;->printStackTrace()V
/* .line 106 */
} // .end local v3 # "e":Landroid/content/pm/PackageManager$NameNotFoundException;
} // :cond_4
} // :goto_1
com.miui.server.EnableStateManager .registerReceiverIfNeed ( );
/* :try_end_4 */
/* .catchall {:try_start_4 ..:try_end_4} :catchall_0 */
/* .line 107 */
/* monitor-exit v0 */
return;
/* .line 68 */
} // .end local v1 # "shouldKeep":Z
} // .end local v2 # "pm":Landroid/content/pm/PackageManager;
} // .end local p0 # "context":Landroid/content/Context;
/* :catchall_0 */
/* move-exception p0 */
/* monitor-exit v0 */
/* throw p0 */
} // .end method
private static void updateApplicationEnableStateInner ( Boolean p0 ) {
/* .locals 3 */
/* .param p0, "shouldKeep" # Z */
/* .line 140 */
v0 = com.miui.server.EnableStateManager.TAG;
/* const-string/jumbo v1, "updateConfigFromFile" */
android.util.Log .i ( v0,v1 );
/* .line 141 */
com.miui.server.EnableStateManager .updateConfigFromFile ( );
/* .line 142 */
v0 = com.miui.server.EnableStateManager.sEnableStateControlledPkgList;
v1 = } // :goto_0
if ( v1 != null) { // if-eqz v1, :cond_1
/* check-cast v1, Ljava/lang/String; */
/* .line 143 */
/* .local v1, "pkgName":Ljava/lang/String; */
v2 = com.miui.server.EnableStateManager .isAppInstalled ( v1 );
if ( v2 != null) { // if-eqz v2, :cond_0
/* .line 144 */
com.miui.server.EnableStateManager .updateEnableState ( v1,p0 );
/* .line 146 */
} // .end local v1 # "pkgName":Ljava/lang/String;
} // :cond_0
/* .line 147 */
} // :cond_1
return;
} // .end method
private static void updateConfigFromFile ( ) {
/* .locals 14 */
/* .line 313 */
final String v0 = ""; // const-string v0, ""
v1 = com.miui.server.EnableStateManager.mCloudEnableSettings;
/* .line 314 */
v1 = com.miui.server.EnableStateManager.sEnableStateControlledPkgList;
/* .line 315 */
v1 = com.miui.server.EnableStateManager.sShouldKeepStatePackages;
/* .line 316 */
int v1 = 0; // const/4 v1, 0x0
/* .line 318 */
/* .local v1, "reader":Ljava/io/BufferedReader; */
try { // :try_start_0
/* new-instance v2, Ljava/io/BufferedReader; */
/* new-instance v3, Ljava/io/InputStreamReader; */
v4 = com.miui.server.EnableStateManager.mContext;
/* .line 319 */
(( android.content.Context ) v4 ).getResources ( ); // invoke-virtual {v4}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;
/* const v5, 0x110e0002 */
(( android.content.res.Resources ) v4 ).openRawResource ( v5 ); // invoke-virtual {v4, v5}, Landroid/content/res/Resources;->openRawResource(I)Ljava/io/InputStream;
/* invoke-direct {v3, v4}, Ljava/io/InputStreamReader;-><init>(Ljava/io/InputStream;)V */
/* invoke-direct {v2, v3}, Ljava/io/BufferedReader;-><init>(Ljava/io/Reader;)V */
/* move-object v1, v2 */
/* .line 320 */
/* new-instance v2, Ljava/lang/StringBuilder; */
/* invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V */
/* .line 322 */
/* .local v2, "sb":Ljava/lang/StringBuilder; */
} // :goto_0
(( java.io.BufferedReader ) v1 ).readLine ( ); // invoke-virtual {v1}, Ljava/io/BufferedReader;->readLine()Ljava/lang/String;
/* move-object v4, v3 */
/* .local v4, "temp":Ljava/lang/String; */
if ( v3 != null) { // if-eqz v3, :cond_0
/* .line 323 */
(( java.lang.StringBuilder ) v2 ).append ( v4 ); // invoke-virtual {v2, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
/* .line 325 */
} // :cond_0
/* new-instance v3, Lorg/json/JSONObject; */
(( java.lang.StringBuilder ) v2 ).toString ( ); // invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
/* invoke-direct {v3, v5}, Lorg/json/JSONObject;-><init>(Ljava/lang/String;)V */
/* .line 327 */
/* .local v3, "json":Lorg/json/JSONObject; */
com.miui.server.EnableStateManager .putCustomizedJSONObject ( v3 );
/* .line 328 */
(( org.json.JSONObject ) v3 ).keys ( ); // invoke-virtual {v3}, Lorg/json/JSONObject;->keys()Ljava/util/Iterator;
/* .line 329 */
/* .local v5, "pkgNameList":Ljava/util/Iterator;, "Ljava/util/Iterator<Ljava/lang/String;>;" */
v6 = } // :goto_1
if ( v6 != null) { // if-eqz v6, :cond_5
/* .line 330 */
/* check-cast v6, Ljava/lang/String; */
/* .line 331 */
/* .local v6, "pkgName":Ljava/lang/String; */
(( org.json.JSONObject ) v3 ).optJSONObject ( v6 ); // invoke-virtual {v3, v6}, Lorg/json/JSONObject;->optJSONObject(Ljava/lang/String;)Lorg/json/JSONObject;
/* .line 332 */
/* .local v7, "settingJson":Lorg/json/JSONObject; */
if ( v7 != null) { // if-eqz v7, :cond_4
/* .line 333 */
/* new-instance v8, Ljava/util/ArrayList; */
/* invoke-direct {v8}, Ljava/util/ArrayList;-><init>()V */
/* .line 334 */
/* .local v8, "enableRegionList":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;" */
final String v9 = "ro.com.miui.rsa.feature"; // const-string v9, "ro.com.miui.rsa.feature"
android.os.SystemProperties .get ( v9,v0 );
/* .line 335 */
/* .local v9, "rsa4":Ljava/lang/String; */
v10 = android.text.TextUtils .isEmpty ( v9 );
/* if-nez v10, :cond_1 */
/* .line 336 */
final String v10 = "rsa4_enable_list"; // const-string v10, "rsa4_enable_list"
(( org.json.JSONObject ) v7 ).optJSONObject ( v10 ); // invoke-virtual {v7, v10}, Lorg/json/JSONObject;->optJSONObject(Ljava/lang/String;)Lorg/json/JSONObject;
/* .line 337 */
/* .local v10, "rsa\uff14EnableObject":Lorg/json/JSONObject; */
com.miui.server.EnableStateManager .getEnableRegionListFromRSA ( v10,v9,v8 );
/* .line 338 */
} // .end local v10 # "rsa\uff14EnableObject":Lorg/json/JSONObject;
/* .line 339 */
} // :cond_1
final String v10 = "ro.com.miui.rsa"; // const-string v10, "ro.com.miui.rsa"
android.os.SystemProperties .get ( v10,v0 );
/* .line 340 */
/* .local v10, "rsa":Ljava/lang/String; */
final String v11 = "rsa_enable_list"; // const-string v11, "rsa_enable_list"
(( org.json.JSONObject ) v7 ).optJSONObject ( v11 ); // invoke-virtual {v7, v11}, Lorg/json/JSONObject;->optJSONObject(Ljava/lang/String;)Lorg/json/JSONObject;
/* .line 341 */
/* .local v11, "rsaEnableObject":Lorg/json/JSONObject; */
com.miui.server.EnableStateManager .getEnableRegionListFromRSA ( v11,v10,v8 );
/* .line 343 */
} // .end local v10 # "rsa":Ljava/lang/String;
} // .end local v11 # "rsaEnableObject":Lorg/json/JSONObject;
} // :goto_2
final String v10 = "enable_list"; // const-string v10, "enable_list"
(( org.json.JSONObject ) v7 ).optJSONArray ( v10 ); // invoke-virtual {v7, v10}, Lorg/json/JSONObject;->optJSONArray(Ljava/lang/String;)Lorg/json/JSONArray;
/* .line 344 */
/* .local v10, "enableRegionArray":Lorg/json/JSONArray; */
if ( v10 != null) { // if-eqz v10, :cond_3
/* .line 345 */
int v11 = 0; // const/4 v11, 0x0
/* .local v11, "index":I */
} // :goto_3
v12 = (( org.json.JSONArray ) v10 ).length ( ); // invoke-virtual {v10}, Lorg/json/JSONArray;->length()I
/* if-ge v11, v12, :cond_3 */
/* .line 346 */
(( org.json.JSONArray ) v10 ).optString ( v11 ); // invoke-virtual {v10, v11}, Lorg/json/JSONArray;->optString(I)Ljava/lang/String;
/* .line 347 */
v13 = /* .local v12, "region":Ljava/lang/String; */
/* if-nez v13, :cond_2 */
/* .line 348 */
/* .line 345 */
} // .end local v12 # "region":Ljava/lang/String;
} // :cond_2
/* add-int/lit8 v11, v11, 0x1 */
/* .line 352 */
} // .end local v11 # "index":I
} // :cond_3
v11 = com.miui.server.EnableStateManager.mCloudEnableSettings;
/* .line 353 */
/* const-string/jumbo v11, "shouldKeep" */
int v12 = 0; // const/4 v12, 0x0
v11 = (( org.json.JSONObject ) v7 ).optBoolean ( v11, v12 ); // invoke-virtual {v7, v11, v12}, Lorg/json/JSONObject;->optBoolean(Ljava/lang/String;Z)Z
/* .line 354 */
/* .local v11, "shouldKeep":Z */
if ( v11 != null) { // if-eqz v11, :cond_4
/* .line 355 */
v12 = com.miui.server.EnableStateManager.sShouldKeepStatePackages;
/* .line 358 */
} // .end local v8 # "enableRegionList":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;"
} // .end local v9 # "rsa4":Ljava/lang/String;
} // .end local v10 # "enableRegionArray":Lorg/json/JSONArray;
} // .end local v11 # "shouldKeep":Z
} // :cond_4
v8 = com.miui.server.EnableStateManager.sEnableStateControlledPkgList;
/* :try_end_0 */
/* .catch Ljava/lang/Exception; {:try_start_0 ..:try_end_0} :catch_1 */
/* .catchall {:try_start_0 ..:try_end_0} :catchall_0 */
/* .line 359 */
/* nop */
} // .end local v6 # "pkgName":Ljava/lang/String;
} // .end local v7 # "settingJson":Lorg/json/JSONObject;
/* .line 365 */
} // .end local v2 # "sb":Ljava/lang/StringBuilder;
} // .end local v3 # "json":Lorg/json/JSONObject;
} // .end local v4 # "temp":Ljava/lang/String;
} // .end local v5 # "pkgNameList":Ljava/util/Iterator;, "Ljava/util/Iterator<Ljava/lang/String;>;"
} // :cond_5
try { // :try_start_1
(( java.io.BufferedReader ) v1 ).close ( ); // invoke-virtual {v1}, Ljava/io/BufferedReader;->close()V
/* :try_end_1 */
/* .catch Ljava/lang/Exception; {:try_start_1 ..:try_end_1} :catch_0 */
/* .line 368 */
} // :goto_4
/* .line 366 */
/* :catch_0 */
/* move-exception v0 */
/* .line 367 */
/* .local v0, "e":Ljava/lang/Exception; */
v2 = com.miui.server.EnableStateManager.TAG;
(( java.lang.Exception ) v0 ).toString ( ); // invoke-virtual {v0}, Ljava/lang/Exception;->toString()Ljava/lang/String;
android.util.Log .e ( v2,v3,v0 );
/* .line 369 */
} // .end local v0 # "e":Ljava/lang/Exception;
/* .line 364 */
/* :catchall_0 */
/* move-exception v0 */
/* .line 360 */
/* :catch_1 */
/* move-exception v0 */
/* .line 361 */
/* .restart local v0 # "e":Ljava/lang/Exception; */
try { // :try_start_2
v2 = com.miui.server.EnableStateManager.TAG;
(( java.lang.Exception ) v0 ).toString ( ); // invoke-virtual {v0}, Ljava/lang/Exception;->toString()Ljava/lang/String;
android.util.Log .e ( v2,v3,v0 );
/* :try_end_2 */
/* .catchall {:try_start_2 ..:try_end_2} :catchall_0 */
/* .line 365 */
} // .end local v0 # "e":Ljava/lang/Exception;
try { // :try_start_3
(( java.io.BufferedReader ) v1 ).close ( ); // invoke-virtual {v1}, Ljava/io/BufferedReader;->close()V
/* :try_end_3 */
/* .catch Ljava/lang/Exception; {:try_start_3 ..:try_end_3} :catch_0 */
/* .line 370 */
} // :goto_5
return;
/* .line 365 */
} // :goto_6
try { // :try_start_4
(( java.io.BufferedReader ) v1 ).close ( ); // invoke-virtual {v1}, Ljava/io/BufferedReader;->close()V
/* :try_end_4 */
/* .catch Ljava/lang/Exception; {:try_start_4 ..:try_end_4} :catch_2 */
/* .line 368 */
/* .line 366 */
/* :catch_2 */
/* move-exception v2 */
/* .line 367 */
/* .local v2, "e":Ljava/lang/Exception; */
v3 = com.miui.server.EnableStateManager.TAG;
(( java.lang.Exception ) v2 ).toString ( ); // invoke-virtual {v2}, Ljava/lang/Exception;->toString()Ljava/lang/String;
android.util.Log .e ( v3,v4,v2 );
/* .line 369 */
} // .end local v2 # "e":Ljava/lang/Exception;
} // :goto_7
/* throw v0 */
} // .end method
private static void updateEnableState ( java.lang.String p0, Boolean p1 ) {
/* .locals 8 */
/* .param p0, "pkgName" # Ljava/lang/String; */
/* .param p1, "shouldKeep" # Z */
/* .line 190 */
final String v0 = "com.xiaomi.market.lastRegion"; // const-string v0, "com.xiaomi.market.lastRegion"
try { // :try_start_0
miui.os.Build .getRegion ( );
/* .line 191 */
/* .local v1, "region":Ljava/lang/String; */
v2 = com.miui.server.EnableStateManager.TAG;
/* new-instance v3, Ljava/lang/StringBuilder; */
/* invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V */
final String v4 = "region: "; // const-string v4, "region: "
(( java.lang.StringBuilder ) v3 ).append ( v4 ); // invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v3 ).append ( v1 ); // invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v3 ).toString ( ); // invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
android.util.Log .d ( v2,v3 );
/* .line 192 */
v3 = android.text.TextUtils .isEmpty ( v1 );
if ( v3 != null) { // if-eqz v3, :cond_0
/* .line 193 */
return;
/* .line 196 */
} // :cond_0
v3 = com.miui.server.EnableStateManager.mContext;
v3 = com.miui.server.EnableStateManager .isProvisioned ( v3 );
if ( v3 != null) { // if-eqz v3, :cond_1
final String v3 = "com.boundax.koreapreloadappinstaller"; // const-string v3, "com.boundax.koreapreloadappinstaller"
/* .line 197 */
v3 = android.text.TextUtils .equals ( p0,v3 );
if ( v3 != null) { // if-eqz v3, :cond_1
/* .line 198 */
return;
/* .line 200 */
} // :cond_1
final String v3 = "all"; // const-string v3, "all"
/* .line 201 */
/* .local v3, "regionAll":Ljava/lang/String; */
com.miui.server.EnableStateManager .getString ( v0 );
/* .line 202 */
/* .local v4, "lastRegion":Ljava/lang/String; */
v5 = android.text.TextUtils .isEmpty ( v4 );
/* if-nez v5, :cond_2 */
v5 = android.text.TextUtils .equals ( v4,v1 );
/* if-nez v5, :cond_2 */
/* .line 203 */
int p1 = 0; // const/4 p1, 0x0
/* .line 205 */
} // :cond_2
com.miui.server.EnableStateManager .getEnableSettings ( p0,p1 );
/* .line 206 */
/* .local v5, "regionList":Ljava/util/Set;, "Ljava/util/Set<Ljava/lang/String;>;" */
/* new-instance v6, Ljava/lang/StringBuilder; */
/* invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V */
final String v7 = "enable "; // const-string v7, "enable "
(( java.lang.StringBuilder ) v6 ).append ( v7 ); // invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v6 ).append ( p0 ); // invoke-virtual {v6, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
final String v7 = " in "; // const-string v7, " in "
(( java.lang.StringBuilder ) v6 ).append ( v7 ); // invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.Object ) v5 ).toString ( ); // invoke-virtual {v5}, Ljava/lang/Object;->toString()Ljava/lang/String;
(( java.lang.StringBuilder ) v6 ).append ( v7 ); // invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v6 ).toString ( ); // invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
android.util.Log .d ( v2,v6 );
v2 = /* .line 207 */
v2 = /* if-nez v2, :cond_4 */
if ( v2 != null) { // if-eqz v2, :cond_3
/* .line 214 */
} // :cond_3
com.miui.server.EnableStateManager .tryDisablePkg ( p0 );
/* .line 208 */
} // :cond_4
} // :goto_0
final String v2 = "com.xiaomi.mipicks"; // const-string v2, "com.xiaomi.mipicks"
v2 = (( java.lang.String ) v2 ).equals ( p0 ); // invoke-virtual {v2, p0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z
if ( v2 != null) { // if-eqz v2, :cond_5
/* .line 209 */
com.miui.server.EnableStateManager .tryEnableMiPicks ( );
/* .line 211 */
} // :cond_5
com.miui.server.EnableStateManager .tryEnablePkg ( p0 );
/* .line 216 */
} // :goto_1
com.miui.server.EnableStateManager .setString ( v0,v1 );
/* :try_end_0 */
/* .catch Ljava/lang/Exception; {:try_start_0 ..:try_end_0} :catch_0 */
/* .line 219 */
} // .end local v1 # "region":Ljava/lang/String;
} // .end local v3 # "regionAll":Ljava/lang/String;
} // .end local v4 # "lastRegion":Ljava/lang/String;
} // .end local v5 # "regionList":Ljava/util/Set;, "Ljava/util/Set<Ljava/lang/String;>;"
/* .line 217 */
/* :catch_0 */
/* move-exception v0 */
/* .line 218 */
/* .local v0, "e":Ljava/lang/Exception; */
v1 = com.miui.server.EnableStateManager.TAG;
(( java.lang.Exception ) v0 ).toString ( ); // invoke-virtual {v0}, Ljava/lang/Exception;->toString()Ljava/lang/String;
android.util.Log .e ( v1,v2,v0 );
/* .line 220 */
} // .end local v0 # "e":Ljava/lang/Exception;
} // :goto_2
return;
} // .end method
