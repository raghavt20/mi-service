public class com.miui.server.MiuiDfcService$UsbStatusReceiver extends android.content.BroadcastReceiver {
	 /* .source "MiuiDfcService.java" */
	 /* # annotations */
	 /* .annotation system Ldalvik/annotation/EnclosingClass; */
	 /* value = Lcom/miui/server/MiuiDfcService; */
} // .end annotation
/* .annotation system Ldalvik/annotation/InnerClass; */
/* accessFlags = 0x1 */
/* name = "UsbStatusReceiver" */
} // .end annotation
/* # instance fields */
final com.miui.server.MiuiDfcService this$0; //synthetic
/* # direct methods */
public com.miui.server.MiuiDfcService$UsbStatusReceiver ( ) {
/* .locals 0 */
/* .param p1, "this$0" # Lcom/miui/server/MiuiDfcService; */
/* .line 281 */
this.this$0 = p1;
/* invoke-direct {p0}, Landroid/content/BroadcastReceiver;-><init>()V */
return;
} // .end method
/* # virtual methods */
public void onReceive ( android.content.Context p0, android.content.Intent p1 ) {
/* .locals 8 */
/* .param p1, "context" # Landroid/content/Context; */
/* .param p2, "intent" # Landroid/content/Intent; */
/* .line 285 */
/* const-string/jumbo v0, "sys.dfcservice.ctrl" */
/* const-string/jumbo v1, "sys.dfcservice.is_front_scene" */
int v2 = 0; // const/4 v2, 0x0
v1 = android.os.SystemProperties .getInt ( v1,v2 );
/* .line 287 */
/* .local v1, "isScene":I */
final String v3 = "MiuiDfcService"; // const-string v3, "MiuiDfcService"
if ( p2 != null) { // if-eqz p2, :cond_4
	 /* sget-boolean v4, Lcom/miui/server/MiuiDfcService;->DFC_DEBUG:Z */
	 /* if-nez v4, :cond_4 */
	 int v4 = 1; // const/4 v4, 0x1
	 /* if-ne v1, v4, :cond_0 */
	 /* .line 291 */
} // :cond_0
(( android.content.Intent ) p2 ).getAction ( ); // invoke-virtual {p2}, Landroid/content/Intent;->getAction()Ljava/lang/String;
/* .line 292 */
/* .local v5, "action":Ljava/lang/String; */
final String v6 = "android.hardware.usb.action.USB_STATE"; // const-string v6, "android.hardware.usb.action.USB_STATE"
v6 = (( java.lang.String ) v6 ).equals ( v5 ); // invoke-virtual {v6, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z
if ( v6 != null) { // if-eqz v6, :cond_3
	 /* .line 293 */
	 (( android.content.Intent ) p2 ).getExtras ( ); // invoke-virtual {p2}, Landroid/content/Intent;->getExtras()Landroid/os/Bundle;
	 final String v7 = "connected"; // const-string v7, "connected"
	 v6 = 	 (( android.os.Bundle ) v6 ).getBoolean ( v7 ); // invoke-virtual {v6, v7}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;)Z
	 /* .line 294 */
	 /* .local v6, "connected":Z */
	 if ( v6 != null) { // if-eqz v6, :cond_2
		 /* .line 297 */
		 try { // :try_start_0
			 v2 = 			 android.os.SystemProperties .getBoolean ( v0,v2 );
			 if ( v2 != null) { // if-eqz v2, :cond_1
				 /* .line 298 */
				 final String v2 = "false"; // const-string v2, "false"
				 android.os.SystemProperties .set ( v0,v2 );
				 /* :try_end_0 */
				 /* .catch Ljava/lang/RuntimeException; {:try_start_0 ..:try_end_0} :catch_0 */
				 /* .line 302 */
			 } // :cond_1
			 /* .line 300 */
			 /* :catch_0 */
			 /* move-exception v0 */
			 /* .line 301 */
			 /* .local v0, "e":Ljava/lang/RuntimeException; */
			 final String v2 = "Failed to set sys.dfcservice.ctrl property"; // const-string v2, "Failed to set sys.dfcservice.ctrl property"
			 android.util.Slog .e ( v3,v2,v0 );
			 /* .line 304 */
		 } // .end local v0 # "e":Ljava/lang/RuntimeException;
	 } // :goto_0
	 v0 = this.this$0;
	 com.miui.server.MiuiDfcService .-$$Nest$fputmUsbConnect ( v0,v4 );
	 /* .line 305 */
	 final String v0 = "Usb connect, shutdown DfcNativeService"; // const-string v0, "Usb connect, shutdown DfcNativeService"
	 android.util.Slog .d ( v3,v0 );
	 /* .line 307 */
} // :cond_2
v0 = this.this$0;
com.miui.server.MiuiDfcService .-$$Nest$fputmUsbConnect ( v0,v2 );
/* .line 308 */
final String v0 = "Usb disconnect"; // const-string v0, "Usb disconnect"
android.util.Slog .d ( v3,v0 );
/* .line 311 */
} // .end local v6 # "connected":Z
} // :cond_3
} // :goto_1
return;
/* .line 288 */
} // .end local v5 # "action":Ljava/lang/String;
} // :cond_4
} // :goto_2
/* new-instance v0, Ljava/lang/StringBuilder; */
/* invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V */
final String v2 = "cleanmaster isScene: "; // const-string v2, "cleanmaster isScene: "
(( java.lang.StringBuilder ) v0 ).append ( v2 ); // invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v0 ).toString ( ); // invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
android.util.Slog .w ( v3,v0 );
/* .line 289 */
return;
} // .end method
