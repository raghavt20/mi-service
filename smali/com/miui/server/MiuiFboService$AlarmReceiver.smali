.class public Lcom/miui/server/MiuiFboService$AlarmReceiver;
.super Landroid/content/BroadcastReceiver;
.source "MiuiFboService.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/miui/server/MiuiFboService;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x9
    name = "AlarmReceiver"
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 0

    .line 299
    invoke-direct {p0}, Landroid/content/BroadcastReceiver;-><init>()V

    return-void
.end method


# virtual methods
.method public onReceive(Landroid/content/Context;Landroid/content/Intent;)V
    .locals 8
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "intent"    # Landroid/content/Intent;

    .line 303
    invoke-static {}, Lcom/miui/server/MiuiFboService;->-$$Nest$sfgetTAG()Ljava/lang/String;

    move-result-object v0

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "received the broadcast and intent.action : "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {p2}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ",intent.getStringExtra:"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    .line 304
    const-string v2, "appList"

    invoke-virtual {p2, v2}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    .line 303
    invoke-static {v0, v1}, Landroid/util/Slog;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 305
    invoke-virtual {p2}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/String;->hashCode()I

    move-result v1

    const/4 v3, 0x3

    const/4 v4, 0x1

    const/4 v5, 0x2

    sparse-switch v1, :sswitch_data_0

    :cond_0
    goto :goto_0

    :sswitch_0
    const-string v1, "miui.intent.action.stop"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    move v0, v3

    goto :goto_1

    :sswitch_1
    const-string v1, "miui.intent.action.start"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x0

    goto :goto_1

    :sswitch_2
    const-string v1, "miui.intent.action.transferFboTrigger"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    move v0, v5

    goto :goto_1

    :sswitch_3
    const-string v1, "miui.intent.action.startAgain"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    move v0, v4

    goto :goto_1

    :goto_0
    const/4 v0, -0x1

    :goto_1
    const-wide/16 v6, 0x3e8

    packed-switch v0, :pswitch_data_0

    goto/16 :goto_2

    .line 327
    :pswitch_0
    invoke-static {}, Lcom/miui/server/MiuiFboService;->-$$Nest$sfgetsInstance()Lcom/miui/server/MiuiFboService;

    move-result-object v0

    invoke-virtual {v0}, Lcom/miui/server/MiuiFboService;->getGlobalSwitch()Z

    move-result v0

    if-nez v0, :cond_1

    invoke-static {}, Lcom/miui/server/MiuiFboService;->-$$Nest$sfgetsInstance()Lcom/miui/server/MiuiFboService;

    move-result-object v0

    invoke-static {v0}, Lcom/miui/server/MiuiFboService;->-$$Nest$fgetmFboNativeService(Lcom/miui/server/MiuiFboService;)Lmiui/fbo/IFbo;

    move-result-object v0

    if-eqz v0, :cond_2

    .line 328
    :cond_1
    invoke-static {}, Lcom/miui/server/MiuiFboService;->-$$Nest$sfgetsInstance()Lcom/miui/server/MiuiFboService;

    move-result-object v0

    const-string/jumbo v1, "stop"

    invoke-virtual {v0, v1, v3, v6, v7}, Lcom/miui/server/MiuiFboService;->deliverMessage(Ljava/lang/String;IJ)V

    .line 329
    invoke-static {}, Lcom/miui/server/MiuiFboService;->-$$Nest$sfgetTAG()Ljava/lang/String;

    move-result-object v0

    const-string v1, "execute STOP"

    invoke-static {v0, v1}, Landroid/util/Slog;->d(Ljava/lang/String;Ljava/lang/String;)I

    goto/16 :goto_2

    .line 320
    :pswitch_1
    invoke-virtual {p2, v2}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    if-eqz v0, :cond_2

    invoke-virtual {p2, v2}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/String;->length()I

    move-result v0

    if-lez v0, :cond_2

    .line 321
    invoke-static {}, Lcom/miui/server/MiuiFboService;->-$$Nest$sfgetsInstance()Lcom/miui/server/MiuiFboService;

    move-result-object v0

    invoke-virtual {p2, v2}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/miui/server/MiuiFboService;->FBO_trigger(Ljava/lang/String;)V

    .line 322
    invoke-static {}, Lcom/miui/server/MiuiFboService;->-$$Nest$sfgetTAG()Ljava/lang/String;

    move-result-object v0

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "execute TRANSFERFBOTRIGGER and appList:"

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {p2, v2}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Slog;->d(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_2

    .line 315
    :pswitch_2
    invoke-static {}, Lcom/miui/server/MiuiFboService;->-$$Nest$sfgetsInstance()Lcom/miui/server/MiuiFboService;

    move-result-object v0

    invoke-static {}, Lcom/miui/server/MiuiFboService;->-$$Nest$sfgetpackageNameList()Ljava/util/ArrayList;

    move-result-object v1

    invoke-virtual {v1}, Ljava/util/ArrayList;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1, v5, v6, v7}, Lcom/miui/server/MiuiFboService;->deliverMessage(Ljava/lang/String;IJ)V

    .line 316
    invoke-static {}, Lcom/miui/server/MiuiFboService;->-$$Nest$sfgetTAG()Ljava/lang/String;

    move-result-object v0

    const-string v1, "execute START_FBO_AGAIN"

    invoke-static {v0, v1}, Landroid/util/Slog;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 317
    goto :goto_2

    .line 308
    :pswitch_3
    invoke-static {}, Lcom/miui/server/MiuiFboService;->-$$Nest$sfgetsInstance()Lcom/miui/server/MiuiFboService;

    move-result-object v0

    invoke-static {v0}, Lcom/miui/server/MiuiFboService;->-$$Nest$misWithinTheTimeInterval(Lcom/miui/server/MiuiFboService;)Z

    move-result v0

    if-eqz v0, :cond_2

    invoke-static {}, Lcom/miui/server/MiuiFboService;->-$$Nest$sfgetsInstance()Lcom/miui/server/MiuiFboService;

    move-result-object v0

    invoke-virtual {v0}, Lcom/miui/server/MiuiFboService;->getGlobalSwitch()Z

    move-result v0

    if-nez v0, :cond_2

    .line 309
    invoke-static {}, Lcom/miui/server/MiuiFboService;->-$$Nest$sfgetsInstance()Lcom/miui/server/MiuiFboService;

    move-result-object v0

    invoke-static {}, Lcom/miui/server/MiuiFboService;->-$$Nest$sfgetpackageNameList()Ljava/util/ArrayList;

    move-result-object v1

    invoke-virtual {v1}, Ljava/util/ArrayList;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1, v4, v6, v7}, Lcom/miui/server/MiuiFboService;->deliverMessage(Ljava/lang/String;IJ)V

    .line 310
    invoke-static {}, Lcom/miui/server/MiuiFboService;->-$$Nest$sfgetTAG()Ljava/lang/String;

    move-result-object v0

    const-string v1, "execute START_FBO"

    invoke-static {v0, v1}, Landroid/util/Slog;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 334
    :cond_2
    :goto_2
    return-void

    :sswitch_data_0
    .sparse-switch
        -0x769280fe -> :sswitch_3
        -0x1bdab3cc -> :sswitch_2
        0x480cf2fe -> :sswitch_1
        0x4ca595e6 -> :sswitch_0
    .end sparse-switch

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_3
        :pswitch_2
        :pswitch_1
        :pswitch_0
    .end packed-switch
.end method
