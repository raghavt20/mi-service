.class Lcom/miui/server/MiuiInitServer$InitCustEnvironmentTask;
.super Ljava/lang/Thread;
.source "MiuiInitServer.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/miui/server/MiuiInitServer;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "InitCustEnvironmentTask"
.end annotation


# instance fields
.field private mCustVarinat:Ljava/lang/String;

.field private mObs:Lmiui/os/IMiuiInitObserver;

.field final synthetic this$0:Lcom/miui/server/MiuiInitServer;


# direct methods
.method constructor <init>(Lcom/miui/server/MiuiInitServer;Ljava/lang/String;Lmiui/os/IMiuiInitObserver;)V
    .locals 0
    .param p2, "custVariant"    # Ljava/lang/String;
    .param p3, "obs"    # Lmiui/os/IMiuiInitObserver;

    .line 102
    iput-object p1, p0, Lcom/miui/server/MiuiInitServer$InitCustEnvironmentTask;->this$0:Lcom/miui/server/MiuiInitServer;

    invoke-direct {p0}, Ljava/lang/Thread;-><init>()V

    .line 103
    iput-object p2, p0, Lcom/miui/server/MiuiInitServer$InitCustEnvironmentTask;->mCustVarinat:Ljava/lang/String;

    .line 104
    iput-object p3, p0, Lcom/miui/server/MiuiInitServer$InitCustEnvironmentTask;->mObs:Lmiui/os/IMiuiInitObserver;

    .line 105
    return-void
.end method

.method private importCustProperties(Ljava/io/File;Z)V
    .locals 7
    .param p1, "custProp"    # Ljava/io/File;
    .param p2, "isTimezoneAuto"    # Z

    .line 217
    invoke-virtual {p1}, Ljava/io/File;->exists()Z

    move-result v0

    if-eqz v0, :cond_6

    .line 218
    :try_start_0
    new-instance v0, Ljava/io/BufferedReader;

    new-instance v1, Ljava/io/FileReader;

    invoke-direct {v1, p1}, Ljava/io/FileReader;-><init>(Ljava/io/File;)V

    invoke-direct {v0, v1}, Ljava/io/BufferedReader;-><init>(Ljava/io/Reader;)V
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    .line 220
    .local v0, "bufferReader":Ljava/io/BufferedReader;
    :cond_0
    :goto_0
    :try_start_1
    invoke-virtual {v0}, Ljava/io/BufferedReader;->readLine()Ljava/lang/String;

    move-result-object v1
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    move-object v2, v1

    .local v2, "line":Ljava/lang/String;
    const-string v3, "MiuiInitServer"

    const-string v4, "persist.sys.timezone"

    if-eqz v1, :cond_4

    .line 221
    :try_start_2
    invoke-virtual {v2}, Ljava/lang/String;->trim()Ljava/lang/String;

    move-result-object v1

    .line 222
    .end local v2    # "line":Ljava/lang/String;
    .local v1, "line":Ljava/lang/String;
    const-string v2, "#"

    invoke-virtual {v1, v2}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_1

    .line 223
    goto :goto_0

    .line 225
    :cond_1
    const-string v2, "="

    invoke-virtual {v1, v2}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v2

    .line 226
    .local v2, "ss":[Ljava/lang/String;
    if-eqz v2, :cond_0

    array-length v5, v2

    const/4 v6, 0x2

    if-eq v5, v6, :cond_2

    .line 227
    goto :goto_0

    .line 230
    :cond_2
    const/4 v5, 0x0

    aget-object v6, v2, v5

    invoke-virtual {v4, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_3

    if-eqz p2, :cond_3

    invoke-direct {p0}, Lcom/miui/server/MiuiInitServer$InitCustEnvironmentTask;->isDeviceNotInProvision()Z

    move-result v4

    if-eqz v4, :cond_3

    .line 231
    const-string v4, "persist.sys.timezone will not be changed when AUTO_TIME_ZONE is open!"

    invoke-static {v3, v4}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_1

    .line 233
    :cond_3
    aget-object v3, v2, v5

    const/4 v4, 0x1

    aget-object v4, v2, v4

    invoke-static {v3, v4}, Landroid/os/SystemProperties;->set(Ljava/lang/String;Ljava/lang/String;)V

    .line 235
    .end local v2    # "ss":[Ljava/lang/String;
    :goto_1
    goto :goto_0

    .line 236
    .end local v1    # "line":Ljava/lang/String;
    .local v2, "line":Ljava/lang/String;
    :cond_4
    sget-boolean v1, Lmiui/os/Build;->IS_GLOBAL_BUILD:Z

    if-eqz v1, :cond_5

    .line 237
    const-string v1, ""

    invoke-static {v4, v1}, Landroid/os/SystemProperties;->get(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 238
    .local v1, "zoneid":Ljava/lang/String;
    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "init MiuiSettings RESIDENT_TIMEZONE: "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 239
    iget-object v3, p0, Lcom/miui/server/MiuiInitServer$InitCustEnvironmentTask;->this$0:Lcom/miui/server/MiuiInitServer;

    invoke-static {v3}, Lcom/miui/server/MiuiInitServer;->-$$Nest$fgetmContext(Lcom/miui/server/MiuiInitServer;)Landroid/content/Context;

    move-result-object v3

    invoke-virtual {v3}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v3

    const-string v4, "resident_timezone"

    invoke-static {v3, v4, v1}, Landroid/provider/Settings$System;->putString(Landroid/content/ContentResolver;Ljava/lang/String;Ljava/lang/String;)Z

    .line 242
    .end local v1    # "zoneid":Ljava/lang/String;
    :cond_5
    invoke-direct {p0}, Lcom/miui/server/MiuiInitServer$InitCustEnvironmentTask;->pokeSystemProperties()V
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    .line 243
    .end local v2    # "line":Ljava/lang/String;
    :try_start_3
    invoke-virtual {v0}, Ljava/io/BufferedReader;->close()V
    :try_end_3
    .catch Ljava/io/IOException; {:try_start_3 .. :try_end_3} :catch_0

    .line 244
    .end local v0    # "bufferReader":Ljava/io/BufferedReader;
    goto :goto_3

    .line 218
    .restart local v0    # "bufferReader":Ljava/io/BufferedReader;
    :catchall_0
    move-exception v1

    :try_start_4
    invoke-virtual {v0}, Ljava/io/BufferedReader;->close()V
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_1

    goto :goto_2

    :catchall_1
    move-exception v2

    :try_start_5
    invoke-virtual {v1, v2}, Ljava/lang/Throwable;->addSuppressed(Ljava/lang/Throwable;)V

    .end local p0    # "this":Lcom/miui/server/MiuiInitServer$InitCustEnvironmentTask;
    .end local p1    # "custProp":Ljava/io/File;
    .end local p2    # "isTimezoneAuto":Z
    :goto_2
    throw v1
    :try_end_5
    .catch Ljava/io/IOException; {:try_start_5 .. :try_end_5} :catch_0

    .line 243
    .end local v0    # "bufferReader":Ljava/io/BufferedReader;
    .restart local p0    # "this":Lcom/miui/server/MiuiInitServer$InitCustEnvironmentTask;
    .restart local p1    # "custProp":Ljava/io/File;
    .restart local p2    # "isTimezoneAuto":Z
    :catch_0
    move-exception v0

    .line 246
    :cond_6
    :goto_3
    return-void
.end method

.method private initCustEnvironment(Ljava/lang/String;)Z
    .locals 5
    .param p1, "custVariant"    # Ljava/lang/String;

    .line 169
    invoke-static {p1}, Lmiui/util/CustomizeUtil;->setMiuiCustVariatDir(Ljava/lang/String;)V

    .line 170
    const/4 v0, 0x1

    invoke-static {v0}, Lmiui/util/CustomizeUtil;->getMiuiCustVariantDir(Z)Ljava/io/File;

    move-result-object v0

    .line 171
    .local v0, "custVariantDir":Ljava/io/File;
    invoke-static {}, Lmiui/util/CustomizeUtil;->geCarrierRegionPropDir()Ljava/io/File;

    move-result-object v1

    .line 172
    .local v1, "carrierPropDir":Ljava/io/File;
    const/4 v2, 0x0

    if-nez v0, :cond_0

    .line 173
    return v2

    .line 182
    :cond_0
    new-instance v3, Ljava/io/File;

    const-string v4, "cust.prop"

    invoke-direct {v3, v0, v4}, Ljava/io/File;-><init>(Ljava/io/File;Ljava/lang/String;)V

    invoke-direct {p0}, Lcom/miui/server/MiuiInitServer$InitCustEnvironmentTask;->isTimeZoneAuto()Z

    move-result v4

    invoke-direct {p0, v3, v4}, Lcom/miui/server/MiuiInitServer$InitCustEnvironmentTask;->importCustProperties(Ljava/io/File;Z)V

    .line 184
    new-instance v3, Ljava/io/File;

    const-string v4, "region_specified.prop"

    invoke-direct {v3, v1, v4}, Ljava/io/File;-><init>(Ljava/io/File;Ljava/lang/String;)V

    invoke-direct {p0, v3, v2}, Lcom/miui/server/MiuiInitServer$InitCustEnvironmentTask;->importCustProperties(Ljava/io/File;Z)V

    .line 187
    invoke-direct {p0, p1}, Lcom/miui/server/MiuiInitServer$InitCustEnvironmentTask;->saveCustVariantToFile(Ljava/lang/String;)V

    .line 190
    iget-object v2, p0, Lcom/miui/server/MiuiInitServer$InitCustEnvironmentTask;->this$0:Lcom/miui/server/MiuiInitServer;

    invoke-static {v2}, Lcom/miui/server/MiuiInitServer;->-$$Nest$fgetmContext(Lcom/miui/server/MiuiInitServer;)Landroid/content/Context;

    move-result-object v2

    invoke-virtual {v2}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v2

    const-string/jumbo v3, "wifi_country_code"

    invoke-static {v2, v3}, Landroid/provider/Settings$Global;->getString(Landroid/content/ContentResolver;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    .line 192
    .local v2, "countryCode":Ljava/lang/String;
    invoke-static {v2}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v3

    if-eqz v3, :cond_1

    .line 193
    invoke-static {}, Lmiui/os/Build;->getRegion()Ljava/lang/String;

    move-result-object v2

    .line 194
    invoke-static {v2}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v3

    if-nez v3, :cond_1

    .line 195
    iget-object v3, p0, Lcom/miui/server/MiuiInitServer$InitCustEnvironmentTask;->this$0:Lcom/miui/server/MiuiInitServer;

    invoke-static {v3}, Lcom/miui/server/MiuiInitServer;->-$$Nest$fgetmContext(Lcom/miui/server/MiuiInitServer;)Landroid/content/Context;

    move-result-object v3

    .line 196
    const-string/jumbo v4, "wifi"

    invoke-virtual {v3, v4}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Landroid/net/wifi/WifiManager;

    .line 203
    :cond_1
    invoke-direct {p0}, Lcom/miui/server/MiuiInitServer$InitCustEnvironmentTask;->installVanwardCustApps()V

    .line 205
    invoke-virtual {v0}, Ljava/io/File;->exists()Z

    move-result v3

    return v3
.end method

.method private installVanwardCustApps()V
    .locals 1

    .line 209
    iget-object v0, p0, Lcom/miui/server/MiuiInitServer$InitCustEnvironmentTask;->this$0:Lcom/miui/server/MiuiInitServer;

    invoke-static {v0}, Lcom/miui/server/MiuiInitServer;->-$$Nest$fgetmMiuiPreinstallHelper(Lcom/miui/server/MiuiInitServer;)Lcom/android/server/pm/MiuiPreinstallHelper;

    move-result-object v0

    invoke-virtual {v0}, Lcom/android/server/pm/MiuiPreinstallHelper;->isSupportNewFrame()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 210
    iget-object v0, p0, Lcom/miui/server/MiuiInitServer$InitCustEnvironmentTask;->this$0:Lcom/miui/server/MiuiInitServer;

    invoke-static {v0}, Lcom/miui/server/MiuiInitServer;->-$$Nest$fgetmMiuiPreinstallHelper(Lcom/miui/server/MiuiInitServer;)Lcom/android/server/pm/MiuiPreinstallHelper;

    move-result-object v0

    invoke-virtual {v0}, Lcom/android/server/pm/MiuiPreinstallHelper;->installVanwardApps()V

    goto :goto_0

    .line 212
    :cond_0
    iget-object v0, p0, Lcom/miui/server/MiuiInitServer$InitCustEnvironmentTask;->this$0:Lcom/miui/server/MiuiInitServer;

    invoke-static {v0}, Lcom/miui/server/MiuiInitServer;->-$$Nest$fgetmContext(Lcom/miui/server/MiuiInitServer;)Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Lcom/android/server/pm/PreinstallApp;->installVanwardCustApps(Landroid/content/Context;)V

    .line 214
    :goto_0
    return-void
.end method

.method private isDeviceNotInProvision()Z
    .locals 3

    .line 165
    iget-object v0, p0, Lcom/miui/server/MiuiInitServer$InitCustEnvironmentTask;->this$0:Lcom/miui/server/MiuiInitServer;

    invoke-static {v0}, Lcom/miui/server/MiuiInitServer;->-$$Nest$fgetmContext(Lcom/miui/server/MiuiInitServer;)Landroid/content/Context;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    const-string v1, "device_provisioned"

    const/4 v2, 0x0

    invoke-static {v0, v1, v2}, Landroid/provider/Settings$Secure;->getInt(Landroid/content/ContentResolver;Ljava/lang/String;I)I

    move-result v0

    if-eqz v0, :cond_0

    const/4 v2, 0x1

    :cond_0
    return v2
.end method

.method private isTimeZoneAuto()Z
    .locals 4

    .line 156
    const/4 v0, 0x0

    :try_start_0
    iget-object v1, p0, Lcom/miui/server/MiuiInitServer$InitCustEnvironmentTask;->this$0:Lcom/miui/server/MiuiInitServer;

    invoke-static {v1}, Lcom/miui/server/MiuiInitServer;->-$$Nest$fgetmContext(Lcom/miui/server/MiuiInitServer;)Landroid/content/Context;

    move-result-object v1

    invoke-virtual {v1}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v1

    const-string v2, "auto_time_zone"

    invoke-static {v1, v2}, Landroid/provider/Settings$Global;->getInt(Landroid/content/ContentResolver;Ljava/lang/String;)I

    move-result v1
    :try_end_0
    .catch Landroid/provider/Settings$SettingNotFoundException; {:try_start_0 .. :try_end_0} :catch_0

    if-lez v1, :cond_0

    const/4 v0, 0x1

    :cond_0
    return v0

    .line 158
    :catch_0
    move-exception v1

    .line 159
    .local v1, "e":Landroid/provider/Settings$SettingNotFoundException;
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "AUTO_TIME_ZONE can\'t found : "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    const-string v3, "MiuiInitServer"

    invoke-static {v3, v2}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 160
    return v0
.end method

.method private pokeSystemProperties()V
    .locals 10

    .line 251
    :try_start_0
    invoke-static {}, Landroid/os/ServiceManager;->listServices()[Ljava/lang/String;

    move-result-object v0
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_2

    .line 254
    .local v0, "services":[Ljava/lang/String;
    nop

    .line 255
    array-length v1, v0

    const/4 v2, 0x0

    move v3, v2

    :goto_0
    if-ge v3, v1, :cond_1

    aget-object v4, v0, v3

    .line 256
    .local v4, "service":Ljava/lang/String;
    invoke-static {v4}, Landroid/os/ServiceManager;->checkService(Ljava/lang/String;)Landroid/os/IBinder;

    move-result-object v5

    .line 257
    .local v5, "obj":Landroid/os/IBinder;
    if-eqz v5, :cond_0

    .line 258
    invoke-static {}, Landroid/os/Parcel;->obtain()Landroid/os/Parcel;

    move-result-object v6

    .line 260
    .local v6, "data":Landroid/os/Parcel;
    const v7, 0x5f535052

    const/4 v8, 0x0

    :try_start_1
    invoke-interface {v5, v7, v6, v8, v2}, Landroid/os/IBinder;->transact(ILandroid/os/Parcel;Landroid/os/Parcel;I)Z
    :try_end_1
    .catch Landroid/os/RemoteException; {:try_start_1 .. :try_end_1} :catch_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_0

    .line 265
    :goto_1
    goto :goto_2

    .line 262
    :catch_0
    move-exception v7

    .line 263
    .local v7, "e":Ljava/lang/Exception;
    new-instance v8, Ljava/lang/StringBuilder;

    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    const-string v9, "Someone wrote a bad service \'"

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    const-string v9, "\' that doesn\'t like to be poked: "

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    const-string v9, "MiuiInitServer"

    invoke-static {v9, v8}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_2

    .line 261
    .end local v7    # "e":Ljava/lang/Exception;
    :catch_1
    move-exception v7

    goto :goto_1

    .line 266
    :goto_2
    invoke-virtual {v6}, Landroid/os/Parcel;->recycle()V

    .line 255
    .end local v4    # "service":Ljava/lang/String;
    .end local v5    # "obj":Landroid/os/IBinder;
    .end local v6    # "data":Landroid/os/Parcel;
    :cond_0
    add-int/lit8 v3, v3, 0x1

    goto :goto_0

    .line 269
    :cond_1
    return-void

    .line 252
    .end local v0    # "services":[Ljava/lang/String;
    :catch_2
    move-exception v0

    .line 253
    .local v0, "e":Ljava/lang/Exception;
    return-void
.end method

.method private saveCustVariantToFile(Ljava/lang/String;)V
    .locals 5
    .param p1, "custVariant"    # Ljava/lang/String;

    .line 272
    invoke-static {}, Lmiui/util/CustomizeUtil;->getMiuiCustVariantFile()Ljava/io/File;

    move-result-object v0

    .line 274
    .local v0, "custVariantFile":Ljava/io/File;
    :try_start_0
    invoke-virtual {v0}, Ljava/io/File;->exists()Z

    move-result v1

    if-nez v1, :cond_0

    .line 275
    invoke-virtual {v0}, Ljava/io/File;->getParentFile()Ljava/io/File;

    move-result-object v1

    invoke-virtual {v1}, Ljava/io/File;->mkdirs()Z

    .line 276
    invoke-virtual {v0}, Ljava/io/File;->createNewFile()Z

    .line 278
    :cond_0
    new-instance v1, Ljava/io/FileWriter;

    const/4 v2, 0x0

    invoke-direct {v1, v0, v2}, Ljava/io/FileWriter;-><init>(Ljava/io/File;Z)V

    .line 279
    .local v1, "fileWriter":Ljava/io/FileWriter;
    new-instance v3, Ljava/io/BufferedWriter;

    invoke-direct {v3, v1}, Ljava/io/BufferedWriter;-><init>(Ljava/io/Writer;)V

    .line 280
    .local v3, "bufferWriter":Ljava/io/BufferedWriter;
    invoke-virtual {v3, p1}, Ljava/io/BufferedWriter;->write(Ljava/lang/String;)V

    .line 281
    invoke-virtual {v3}, Ljava/io/BufferedWriter;->close()V

    .line 282
    invoke-virtual {v1}, Ljava/io/FileWriter;->close()V

    .line 283
    const/4 v4, 0x1

    invoke-virtual {v0, v4, v2}, Ljava/io/File;->setReadable(ZZ)Z
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    .line 286
    nop

    .end local v1    # "fileWriter":Ljava/io/FileWriter;
    .end local v3    # "bufferWriter":Ljava/io/BufferedWriter;
    goto :goto_0

    .line 284
    :catch_0
    move-exception v1

    .line 285
    .local v1, "e":Ljava/io/IOException;
    invoke-virtual {v1}, Ljava/io/IOException;->printStackTrace()V

    .line 287
    .end local v1    # "e":Ljava/io/IOException;
    :goto_0
    return-void
.end method


# virtual methods
.method public run()V
    .locals 6

    .line 109
    iget-object v0, p0, Lcom/miui/server/MiuiInitServer$InitCustEnvironmentTask;->mCustVarinat:Ljava/lang/String;

    invoke-direct {p0, v0}, Lcom/miui/server/MiuiInitServer$InitCustEnvironmentTask;->initCustEnvironment(Ljava/lang/String;)Z

    move-result v0

    .line 110
    .local v0, "ret":Z
    iget-object v1, p0, Lcom/miui/server/MiuiInitServer$InitCustEnvironmentTask;->mObs:Lmiui/os/IMiuiInitObserver;

    if-eqz v1, :cond_0

    .line 112
    :try_start_0
    invoke-interface {v1, v0}, Lmiui/os/IMiuiInitObserver;->initDone(Z)V
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    .line 114
    goto :goto_0

    .line 113
    :catch_0
    move-exception v1

    .line 116
    :cond_0
    :goto_0
    iget-object v1, p0, Lcom/miui/server/MiuiInitServer$InitCustEnvironmentTask;->this$0:Lcom/miui/server/MiuiInitServer;

    const/4 v2, 0x0

    invoke-static {v1, v2}, Lcom/miui/server/MiuiInitServer;->-$$Nest$fputmDoing(Lcom/miui/server/MiuiInitServer;Z)V

    .line 128
    :try_start_1
    invoke-static {}, Lmiui/content/res/GlobalConfiguration;->get()Landroid/content/res/Configuration;

    move-result-object v1

    .line 129
    .local v1, "curConfig":Landroid/content/res/Configuration;
    invoke-virtual {v1}, Landroid/content/res/Configuration;->getExtraConfig()Landroid/content/res/IMiuiConfiguration;

    move-result-object v2

    .line 130
    .local v2, "iExtraConfig":Landroid/content/res/IMiuiConfiguration;
    instance-of v3, v2, Landroid/content/res/MiuiConfiguration;

    if-eqz v3, :cond_1

    .line 131
    move-object v3, v2

    check-cast v3, Landroid/content/res/MiuiConfiguration;

    goto :goto_1

    :cond_1
    const/4 v3, 0x0

    .line 132
    .local v3, "extraConfig":Landroid/content/res/MiuiConfiguration;
    :goto_1
    if-eqz v3, :cond_2

    .line 133
    const-wide/16 v4, 0x0

    invoke-virtual {v3, v4, v5}, Landroid/content/res/MiuiConfiguration;->updateTheme(J)V

    .line 135
    :cond_2
    invoke-static {v1}, Lmiui/content/res/GlobalConfiguration;->update(Landroid/content/res/Configuration;)V
    :try_end_1
    .catch Landroid/os/RemoteException; {:try_start_1 .. :try_end_1} :catch_1

    .line 138
    .end local v1    # "curConfig":Landroid/content/res/Configuration;
    .end local v2    # "iExtraConfig":Landroid/content/res/IMiuiConfiguration;
    .end local v3    # "extraConfig":Landroid/content/res/MiuiConfiguration;
    goto :goto_2

    .line 136
    :catch_1
    move-exception v1

    .line 137
    .local v1, "e":Landroid/os/RemoteException;
    invoke-virtual {v1}, Landroid/os/RemoteException;->printStackTrace()V

    .line 140
    .end local v1    # "e":Landroid/os/RemoteException;
    :goto_2
    iget-object v1, p0, Lcom/miui/server/MiuiInitServer$InitCustEnvironmentTask;->this$0:Lcom/miui/server/MiuiInitServer;

    invoke-static {v1}, Lcom/miui/server/MiuiInitServer;->-$$Nest$fgetmContext(Lcom/miui/server/MiuiInitServer;)Landroid/content/Context;

    move-result-object v1

    invoke-static {v1}, Lcom/miui/server/EnableStateManager;->updateApplicationEnableState(Landroid/content/Context;)V

    .line 141
    iget-object v1, p0, Lcom/miui/server/MiuiInitServer$InitCustEnvironmentTask;->this$0:Lcom/miui/server/MiuiInitServer;

    invoke-static {v1}, Lcom/miui/server/MiuiInitServer;->-$$Nest$mdeletePackagesByRegion(Lcom/miui/server/MiuiInitServer;)V

    .line 144
    iget-object v1, p0, Lcom/miui/server/MiuiInitServer$InitCustEnvironmentTask;->this$0:Lcom/miui/server/MiuiInitServer;

    invoke-static {v1}, Lcom/miui/server/MiuiInitServer;->-$$Nest$fgetmContext(Lcom/miui/server/MiuiInitServer;)Landroid/content/Context;

    move-result-object v1

    new-instance v2, Landroid/content/Intent;

    const-string v3, "miui.intent.action.MIUI_INIT_COMPLETED"

    invoke-direct {v2, v3}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    const-string v3, "miui.os.permisson.INIT_MIUI_ENVIRONMENT"

    invoke-virtual {v1, v2, v3}, Landroid/content/Context;->sendBroadcast(Landroid/content/Intent;Ljava/lang/String;)V

    .line 148
    new-instance v1, Landroid/content/Intent;

    const-string v2, "miui.intent.action.MIUI_REGION_CHANGED"

    invoke-direct {v1, v2}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 149
    .local v1, "intent":Landroid/content/Intent;
    const/high16 v2, 0x1000000

    invoke-virtual {v1, v2}, Landroid/content/Intent;->addFlags(I)Landroid/content/Intent;

    .line 150
    const-string v2, "region"

    invoke-static {}, Lmiui/os/Build;->getRegion()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v1, v2, v3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 151
    iget-object v2, p0, Lcom/miui/server/MiuiInitServer$InitCustEnvironmentTask;->this$0:Lcom/miui/server/MiuiInitServer;

    invoke-static {v2}, Lcom/miui/server/MiuiInitServer;->-$$Nest$fgetmContext(Lcom/miui/server/MiuiInitServer;)Landroid/content/Context;

    move-result-object v2

    invoke-virtual {v2, v1}, Landroid/content/Context;->sendBroadcast(Landroid/content/Intent;)V

    .line 152
    return-void
.end method
