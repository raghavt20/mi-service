class com.miui.server.PerfShielderService$2 implements android.content.ServiceConnection {
	 /* .source "PerfShielderService.java" */
	 /* # interfaces */
	 /* # annotations */
	 /* .annotation system Ldalvik/annotation/EnclosingClass; */
	 /* value = Lcom/miui/server/PerfShielderService; */
} // .end annotation
/* .annotation system Ldalvik/annotation/InnerClass; */
/* accessFlags = 0x0 */
/* name = null */
} // .end annotation
/* # instance fields */
final com.miui.server.PerfShielderService this$0; //synthetic
/* # direct methods */
 com.miui.server.PerfShielderService$2 ( ) {
/* .locals 0 */
/* .param p1, "this$0" # Lcom/miui/server/PerfShielderService; */
/* .line 599 */
this.this$0 = p1;
/* invoke-direct {p0}, Ljava/lang/Object;-><init>()V */
return;
} // .end method
/* # virtual methods */
public void onServiceConnected ( android.content.ComponentName p0, android.os.IBinder p1 ) {
/* .locals 3 */
/* .param p1, "arg0" # Landroid/content/ComponentName; */
/* .param p2, "arg1" # Landroid/os/IBinder; */
/* .line 611 */
v0 = this.this$0;
com.miui.daemon.performance.server.IMiuiPerfService$Stub .asInterface ( p2 );
this.mPerfService = v1;
/* .line 612 */
v0 = this.this$0;
com.miui.server.PerfShielderService .-$$Nest$fgetmHandler ( v0 );
int v1 = 1; // const/4 v1, 0x1
(( com.miui.server.PerfShielderService$BindServiceHandler ) v0 ).removeMessages ( v1 ); // invoke-virtual {v0, v1}, Lcom/miui/server/PerfShielderService$BindServiceHandler;->removeMessages(I)V
/* .line 614 */
try { // :try_start_0
	 final String v0 = "PerfShielderService"; // const-string v0, "PerfShielderService"
	 final String v1 = "Miui performance service connected!"; // const-string v1, "Miui performance service connected!"
	 android.util.Slog .v ( v0,v1 );
	 /* .line 615 */
	 v0 = this.this$0;
	 v0 = this.mPerfService;
	 v1 = this.this$0;
	 v1 = this.mDeathHandler;
	 int v2 = 0; // const/4 v2, 0x0
	 /* :try_end_0 */
	 /* .catch Ljava/lang/Exception; {:try_start_0 ..:try_end_0} :catch_0 */
	 /* .line 618 */
	 /* .line 616 */
	 /* :catch_0 */
	 /* move-exception v0 */
	 /* .line 617 */
	 /* .local v0, "e":Ljava/lang/Exception; */
	 (( java.lang.Exception ) v0 ).printStackTrace ( ); // invoke-virtual {v0}, Ljava/lang/Exception;->printStackTrace()V
	 /* .line 619 */
} // .end local v0 # "e":Ljava/lang/Exception;
} // :goto_0
return;
} // .end method
public void onServiceDisconnected ( android.content.ComponentName p0 ) {
/* .locals 2 */
/* .param p1, "arg0" # Landroid/content/ComponentName; */
/* .line 602 */
final String v0 = "PerfShielderService"; // const-string v0, "PerfShielderService"
final String v1 = "Miui performance service disconnected!"; // const-string v1, "Miui performance service disconnected!"
android.util.Slog .v ( v0,v1 );
/* .line 603 */
v0 = this.this$0;
int v1 = 0; // const/4 v1, 0x0
this.mPerfService = v1;
/* .line 604 */
v0 = this.this$0;
com.miui.server.PerfShielderService .-$$Nest$fgetmContext ( v0 );
if ( v0 != null) { // if-eqz v0, :cond_0
/* .line 605 */
v0 = this.this$0;
com.miui.server.PerfShielderService .-$$Nest$fgetmContext ( v0 );
v1 = this.this$0;
com.miui.server.PerfShielderService .-$$Nest$fgetmPerformanceConnection ( v1 );
(( android.content.Context ) v0 ).unbindService ( v1 ); // invoke-virtual {v0, v1}, Landroid/content/Context;->unbindService(Landroid/content/ServiceConnection;)V
/* .line 607 */
} // :cond_0
return;
} // .end method
