.class Lcom/miui/server/EnableStateManager$1;
.super Ljava/lang/Thread;
.source "EnableStateManager.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/miui/server/EnableStateManager;->registerReceiverIfNeed()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# direct methods
.method constructor <init>()V
    .locals 0

    .line 124
    invoke-direct {p0}, Ljava/lang/Thread;-><init>()V

    return-void
.end method


# virtual methods
.method public run()V
    .locals 5

    .line 127
    new-instance v0, Landroid/content/IntentFilter;

    invoke-direct {v0}, Landroid/content/IntentFilter;-><init>()V

    .line 128
    .local v0, "filter":Landroid/content/IntentFilter;
    const-string v1, "android.intent.action.PACKAGE_ADDED"

    invoke-virtual {v0, v1}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    .line 129
    const-string v1, "package"

    invoke-virtual {v0, v1}, Landroid/content/IntentFilter;->addDataScheme(Ljava/lang/String;)V

    .line 130
    invoke-static {}, Lcom/miui/server/EnableStateManager;->-$$Nest$sfgetmContext()Landroid/content/Context;

    move-result-object v1

    new-instance v2, Lcom/miui/server/EnableStateManager$PackageAddedReceiver;

    const/4 v3, 0x0

    invoke-direct {v2, v3}, Lcom/miui/server/EnableStateManager$PackageAddedReceiver;-><init>(Lcom/miui/server/EnableStateManager$PackageAddedReceiver-IA;)V

    invoke-virtual {v1, v2, v0}, Landroid/content/Context;->registerReceiver(Landroid/content/BroadcastReceiver;Landroid/content/IntentFilter;)Landroid/content/Intent;

    .line 132
    new-instance v1, Landroid/content/IntentFilter;

    invoke-direct {v1}, Landroid/content/IntentFilter;-><init>()V

    .line 133
    .local v1, "otherFilter":Landroid/content/IntentFilter;
    const-string v2, "com.android.updater.action.COTA_CARRIER"

    invoke-virtual {v1, v2}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    .line 134
    const-string v2, "android.provision.action.PROVISION_COMPLETE"

    invoke-virtual {v1, v2}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    .line 135
    invoke-static {}, Lcom/miui/server/EnableStateManager;->-$$Nest$sfgetmContext()Landroid/content/Context;

    move-result-object v2

    new-instance v4, Lcom/miui/server/EnableStateManager$OtherChangedReceiver;

    invoke-direct {v4, v3}, Lcom/miui/server/EnableStateManager$OtherChangedReceiver;-><init>(Lcom/miui/server/EnableStateManager$OtherChangedReceiver-IA;)V

    invoke-virtual {v2, v4, v1}, Landroid/content/Context;->registerReceiver(Landroid/content/BroadcastReceiver;Landroid/content/IntentFilter;)Landroid/content/Intent;

    .line 136
    return-void
.end method
