public class com.miui.server.stability.GpuMemoryProcUsageInfo {
	 /* .source "GpuMemoryProcUsageInfo.java" */
	 /* # instance fields */
	 private Long gfxDev;
	 private Long glMtrack;
	 private Integer oomadj;
	 private Integer pid;
	 private java.lang.String procName;
	 private Long rss;
	 private Integer type;
	 /* # direct methods */
	 public com.miui.server.stability.GpuMemoryProcUsageInfo ( ) {
		 /* .locals 0 */
		 /* .param p1, "type" # I */
		 /* .line 18 */
		 /* invoke-direct {p0}, Ljava/lang/Object;-><init>()V */
		 /* .line 19 */
		 /* iput p1, p0, Lcom/miui/server/stability/GpuMemoryProcUsageInfo;->type:I */
		 /* .line 20 */
		 return;
	 } // .end method
	 /* # virtual methods */
	 public Long getGfxDev ( ) {
		 /* .locals 2 */
		 /* .line 59 */
		 /* iget-wide v0, p0, Lcom/miui/server/stability/GpuMemoryProcUsageInfo;->gfxDev:J */
		 /* return-wide v0 */
	 } // .end method
	 public Long getGlMtrack ( ) {
		 /* .locals 2 */
		 /* .line 67 */
		 /* iget-wide v0, p0, Lcom/miui/server/stability/GpuMemoryProcUsageInfo;->glMtrack:J */
		 /* return-wide v0 */
	 } // .end method
	 public java.lang.String getName ( ) {
		 /* .locals 1 */
		 /* .line 27 */
		 v0 = this.procName;
	 } // .end method
	 public Integer getOomadj ( ) {
		 /* .locals 1 */
		 /* .line 43 */
		 /* iget v0, p0, Lcom/miui/server/stability/GpuMemoryProcUsageInfo;->oomadj:I */
	 } // .end method
	 public Integer getPid ( ) {
		 /* .locals 1 */
		 /* .line 35 */
		 /* iget v0, p0, Lcom/miui/server/stability/GpuMemoryProcUsageInfo;->pid:I */
	 } // .end method
	 public Long getRss ( ) {
		 /* .locals 2 */
		 /* .line 51 */
		 /* iget-wide v0, p0, Lcom/miui/server/stability/GpuMemoryProcUsageInfo;->rss:J */
		 /* return-wide v0 */
	 } // .end method
	 public void setGfxDev ( Long p0 ) {
		 /* .locals 0 */
		 /* .param p1, "gfxDev" # J */
		 /* .line 55 */
		 /* iput-wide p1, p0, Lcom/miui/server/stability/GpuMemoryProcUsageInfo;->gfxDev:J */
		 /* .line 56 */
		 return;
	 } // .end method
	 public void setGlMtrack ( Long p0 ) {
		 /* .locals 0 */
		 /* .param p1, "glMtrack" # J */
		 /* .line 63 */
		 /* iput-wide p1, p0, Lcom/miui/server/stability/GpuMemoryProcUsageInfo;->glMtrack:J */
		 /* .line 64 */
		 return;
	 } // .end method
	 public void setName ( java.lang.String p0 ) {
		 /* .locals 0 */
		 /* .param p1, "procName" # Ljava/lang/String; */
		 /* .line 23 */
		 this.procName = p1;
		 /* .line 24 */
		 return;
	 } // .end method
	 public void setOomadj ( Integer p0 ) {
		 /* .locals 0 */
		 /* .param p1, "oomadj" # I */
		 /* .line 39 */
		 /* iput p1, p0, Lcom/miui/server/stability/GpuMemoryProcUsageInfo;->oomadj:I */
		 /* .line 40 */
		 return;
	 } // .end method
	 public void setPid ( Integer p0 ) {
		 /* .locals 0 */
		 /* .param p1, "pid" # I */
		 /* .line 31 */
		 /* iput p1, p0, Lcom/miui/server/stability/GpuMemoryProcUsageInfo;->pid:I */
		 /* .line 32 */
		 return;
	 } // .end method
	 public void setRss ( Long p0 ) {
		 /* .locals 0 */
		 /* .param p1, "rss" # J */
		 /* .line 47 */
		 /* iput-wide p1, p0, Lcom/miui/server/stability/GpuMemoryProcUsageInfo;->rss:J */
		 /* .line 48 */
		 return;
	 } // .end method
	 public java.lang.String toString ( Integer p0 ) {
		 /* .locals 7 */
		 /* .param p1, "gpuType" # I */
		 /* .line 71 */
		 /* new-instance v0, Ljava/lang/StringBuilder; */
		 /* invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V */
		 /* .line 72 */
		 /* .local v0, "sb":Ljava/lang/StringBuilder; */
		 int v1 = 1; // const/4 v1, 0x1
		 final String v2 = "kB"; // const-string v2, "kB"
		 final String v3 = " Rss="; // const-string v3, " Rss="
		 final String v4 = " Oomadj="; // const-string v4, " Oomadj="
		 final String v5 = " Pid="; // const-string v5, " Pid="
		 /* if-ne p1, v1, :cond_0 */
		 /* .line 73 */
		 /* new-instance v1, Ljava/lang/StringBuilder; */
		 /* invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V */
		 final String v6 = "KgslMemory proc info Name: "; // const-string v6, "KgslMemory proc info Name: "
		 (( java.lang.StringBuilder ) v1 ).append ( v6 ); // invoke-virtual {v1, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
		 v6 = this.procName;
		 (( java.lang.StringBuilder ) v1 ).append ( v6 ); // invoke-virtual {v1, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
		 (( java.lang.StringBuilder ) v1 ).append ( v5 ); // invoke-virtual {v1, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
		 /* iget v5, p0, Lcom/miui/server/stability/GpuMemoryProcUsageInfo;->pid:I */
		 (( java.lang.StringBuilder ) v1 ).append ( v5 ); // invoke-virtual {v1, v5}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;
		 (( java.lang.StringBuilder ) v1 ).append ( v4 ); // invoke-virtual {v1, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
		 /* iget v4, p0, Lcom/miui/server/stability/GpuMemoryProcUsageInfo;->oomadj:I */
		 (( java.lang.StringBuilder ) v1 ).append ( v4 ); // invoke-virtual {v1, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;
		 (( java.lang.StringBuilder ) v1 ).append ( v3 ); // invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
		 /* iget-wide v3, p0, Lcom/miui/server/stability/GpuMemoryProcUsageInfo;->rss:J */
		 (( java.lang.StringBuilder ) v1 ).append ( v3, v4 ); // invoke-virtual {v1, v3, v4}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;
		 final String v3 = "kB GfxDev="; // const-string v3, "kB GfxDev="
		 (( java.lang.StringBuilder ) v1 ).append ( v3 ); // invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
		 /* iget-wide v3, p0, Lcom/miui/server/stability/GpuMemoryProcUsageInfo;->gfxDev:J */
		 (( java.lang.StringBuilder ) v1 ).append ( v3, v4 ); // invoke-virtual {v1, v3, v4}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;
		 final String v3 = "kB GlMtrack="; // const-string v3, "kB GlMtrack="
		 (( java.lang.StringBuilder ) v1 ).append ( v3 ); // invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
		 /* iget-wide v3, p0, Lcom/miui/server/stability/GpuMemoryProcUsageInfo;->glMtrack:J */
		 (( java.lang.StringBuilder ) v1 ).append ( v3, v4 ); // invoke-virtual {v1, v3, v4}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;
		 (( java.lang.StringBuilder ) v1 ).append ( v2 ); // invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
		 (( java.lang.StringBuilder ) v1 ).toString ( ); // invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
		 (( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
		 /* .line 75 */
	 } // :cond_0
	 int v1 = 2; // const/4 v1, 0x2
	 /* if-ne p1, v1, :cond_1 */
	 /* .line 76 */
	 /* new-instance v1, Ljava/lang/StringBuilder; */
	 /* invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V */
	 final String v6 = "MaliMemory proc info Name: "; // const-string v6, "MaliMemory proc info Name: "
	 (( java.lang.StringBuilder ) v1 ).append ( v6 ); // invoke-virtual {v1, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
	 v6 = this.procName;
	 (( java.lang.StringBuilder ) v1 ).append ( v6 ); // invoke-virtual {v1, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
	 (( java.lang.StringBuilder ) v1 ).append ( v5 ); // invoke-virtual {v1, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
	 /* iget v5, p0, Lcom/miui/server/stability/GpuMemoryProcUsageInfo;->pid:I */
	 (( java.lang.StringBuilder ) v1 ).append ( v5 ); // invoke-virtual {v1, v5}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;
	 (( java.lang.StringBuilder ) v1 ).append ( v4 ); // invoke-virtual {v1, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
	 /* iget v4, p0, Lcom/miui/server/stability/GpuMemoryProcUsageInfo;->oomadj:I */
	 (( java.lang.StringBuilder ) v1 ).append ( v4 ); // invoke-virtual {v1, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;
	 (( java.lang.StringBuilder ) v1 ).append ( v3 ); // invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
	 /* iget-wide v3, p0, Lcom/miui/server/stability/GpuMemoryProcUsageInfo;->rss:J */
	 (( java.lang.StringBuilder ) v1 ).append ( v3, v4 ); // invoke-virtual {v1, v3, v4}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;
	 (( java.lang.StringBuilder ) v1 ).append ( v2 ); // invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
	 (( java.lang.StringBuilder ) v1 ).toString ( ); // invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
	 (( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
	 /* .line 81 */
} // :goto_0
(( java.lang.StringBuilder ) v0 ).toString ( ); // invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
/* .line 79 */
} // :cond_1
final String v1 = "Error Info"; // const-string v1, "Error Info"
} // .end method
