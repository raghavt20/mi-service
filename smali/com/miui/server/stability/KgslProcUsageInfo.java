public class com.miui.server.stability.KgslProcUsageInfo {
	 /* .source "KgslProcUsageInfo.java" */
	 /* # instance fields */
	 private Long gfxDev;
	 private Long glMtrack;
	 private Integer oomadj;
	 private Integer pid;
	 private java.lang.String procName;
	 private Long rss;
	 /* # direct methods */
	 public com.miui.server.stability.KgslProcUsageInfo ( ) {
		 /* .locals 0 */
		 /* .line 15 */
		 /* invoke-direct {p0}, Ljava/lang/Object;-><init>()V */
		 /* .line 16 */
		 return;
	 } // .end method
	 /* # virtual methods */
	 public Long getGfxDev ( ) {
		 /* .locals 2 */
		 /* .line 55 */
		 /* iget-wide v0, p0, Lcom/miui/server/stability/KgslProcUsageInfo;->gfxDev:J */
		 /* return-wide v0 */
	 } // .end method
	 public Long getGlMtrack ( ) {
		 /* .locals 2 */
		 /* .line 63 */
		 /* iget-wide v0, p0, Lcom/miui/server/stability/KgslProcUsageInfo;->glMtrack:J */
		 /* return-wide v0 */
	 } // .end method
	 public java.lang.String getName ( ) {
		 /* .locals 1 */
		 /* .line 23 */
		 v0 = this.procName;
	 } // .end method
	 public Integer getOomadj ( ) {
		 /* .locals 1 */
		 /* .line 39 */
		 /* iget v0, p0, Lcom/miui/server/stability/KgslProcUsageInfo;->oomadj:I */
	 } // .end method
	 public Integer getPid ( ) {
		 /* .locals 1 */
		 /* .line 31 */
		 /* iget v0, p0, Lcom/miui/server/stability/KgslProcUsageInfo;->pid:I */
	 } // .end method
	 public Long getRss ( ) {
		 /* .locals 2 */
		 /* .line 47 */
		 /* iget-wide v0, p0, Lcom/miui/server/stability/KgslProcUsageInfo;->rss:J */
		 /* return-wide v0 */
	 } // .end method
	 public void setGfxDev ( Long p0 ) {
		 /* .locals 0 */
		 /* .param p1, "gfxDev" # J */
		 /* .line 51 */
		 /* iput-wide p1, p0, Lcom/miui/server/stability/KgslProcUsageInfo;->gfxDev:J */
		 /* .line 52 */
		 return;
	 } // .end method
	 public void setGlMtrack ( Long p0 ) {
		 /* .locals 0 */
		 /* .param p1, "glMtrack" # J */
		 /* .line 59 */
		 /* iput-wide p1, p0, Lcom/miui/server/stability/KgslProcUsageInfo;->glMtrack:J */
		 /* .line 60 */
		 return;
	 } // .end method
	 public void setName ( java.lang.String p0 ) {
		 /* .locals 0 */
		 /* .param p1, "procName" # Ljava/lang/String; */
		 /* .line 19 */
		 this.procName = p1;
		 /* .line 20 */
		 return;
	 } // .end method
	 public void setOomadj ( Integer p0 ) {
		 /* .locals 0 */
		 /* .param p1, "oomadj" # I */
		 /* .line 35 */
		 /* iput p1, p0, Lcom/miui/server/stability/KgslProcUsageInfo;->oomadj:I */
		 /* .line 36 */
		 return;
	 } // .end method
	 public void setPid ( Integer p0 ) {
		 /* .locals 0 */
		 /* .param p1, "pid" # I */
		 /* .line 27 */
		 /* iput p1, p0, Lcom/miui/server/stability/KgslProcUsageInfo;->pid:I */
		 /* .line 28 */
		 return;
	 } // .end method
	 public void setRss ( Long p0 ) {
		 /* .locals 0 */
		 /* .param p1, "rss" # J */
		 /* .line 43 */
		 /* iput-wide p1, p0, Lcom/miui/server/stability/KgslProcUsageInfo;->rss:J */
		 /* .line 44 */
		 return;
	 } // .end method
	 public java.lang.String toString ( ) {
		 /* .locals 4 */
		 /* .line 67 */
		 /* new-instance v0, Ljava/lang/StringBuilder; */
		 /* invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V */
		 /* .line 68 */
		 /* .local v0, "sb":Ljava/lang/StringBuilder; */
		 /* new-instance v1, Ljava/lang/StringBuilder; */
		 /* invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V */
		 final String v2 = "Kgsl proc info Name: "; // const-string v2, "Kgsl proc info Name: "
		 (( java.lang.StringBuilder ) v1 ).append ( v2 ); // invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
		 v2 = this.procName;
		 (( java.lang.StringBuilder ) v1 ).append ( v2 ); // invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
		 final String v2 = " Pid="; // const-string v2, " Pid="
		 (( java.lang.StringBuilder ) v1 ).append ( v2 ); // invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
		 /* iget v2, p0, Lcom/miui/server/stability/KgslProcUsageInfo;->pid:I */
		 (( java.lang.StringBuilder ) v1 ).append ( v2 ); // invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;
		 final String v2 = " Oomadj="; // const-string v2, " Oomadj="
		 (( java.lang.StringBuilder ) v1 ).append ( v2 ); // invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
		 /* iget v2, p0, Lcom/miui/server/stability/KgslProcUsageInfo;->oomadj:I */
		 (( java.lang.StringBuilder ) v1 ).append ( v2 ); // invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;
		 final String v2 = " Rss="; // const-string v2, " Rss="
		 (( java.lang.StringBuilder ) v1 ).append ( v2 ); // invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
		 /* iget-wide v2, p0, Lcom/miui/server/stability/KgslProcUsageInfo;->rss:J */
		 (( java.lang.StringBuilder ) v1 ).append ( v2, v3 ); // invoke-virtual {v1, v2, v3}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;
		 final String v2 = "kB GfxDev="; // const-string v2, "kB GfxDev="
		 (( java.lang.StringBuilder ) v1 ).append ( v2 ); // invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
		 /* iget-wide v2, p0, Lcom/miui/server/stability/KgslProcUsageInfo;->gfxDev:J */
		 (( java.lang.StringBuilder ) v1 ).append ( v2, v3 ); // invoke-virtual {v1, v2, v3}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;
		 final String v2 = "kB GlMtrack="; // const-string v2, "kB GlMtrack="
		 (( java.lang.StringBuilder ) v1 ).append ( v2 ); // invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
		 /* iget-wide v2, p0, Lcom/miui/server/stability/KgslProcUsageInfo;->glMtrack:J */
		 (( java.lang.StringBuilder ) v1 ).append ( v2, v3 ); // invoke-virtual {v1, v2, v3}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;
		 final String v2 = "kB"; // const-string v2, "kB"
		 (( java.lang.StringBuilder ) v1 ).append ( v2 ); // invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
		 (( java.lang.StringBuilder ) v1 ).toString ( ); // invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
		 (( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
		 /* .line 70 */
		 (( java.lang.StringBuilder ) v0 ).toString ( ); // invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
	 } // .end method
