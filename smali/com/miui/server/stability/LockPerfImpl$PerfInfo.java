class com.miui.server.stability.LockPerfImpl$PerfInfo {
	 /* .source "LockPerfImpl.java" */
	 /* # annotations */
	 /* .annotation system Ldalvik/annotation/EnclosingClass; */
	 /* value = Lcom/miui/server/stability/LockPerfImpl; */
} // .end annotation
/* .annotation system Ldalvik/annotation/InnerClass; */
/* accessFlags = 0xa */
/* name = "PerfInfo" */
} // .end annotation
/* # instance fields */
private java.lang.String className;
private Long duration;
private final java.lang.String fullMethod;
private java.lang.String methodDesc;
private java.lang.String methodName;
private final Long start;
/* # direct methods */
static Long -$$Nest$fgetstart ( com.miui.server.stability.LockPerfImpl$PerfInfo p0 ) { //bridge//synthethic
/* .locals 2 */
/* iget-wide v0, p0, Lcom/miui/server/stability/LockPerfImpl$PerfInfo;->start:J */
/* return-wide v0 */
} // .end method
static java.lang.String -$$Nest$mgetDurationString ( com.miui.server.stability.LockPerfImpl$PerfInfo p0 ) { //bridge//synthethic
/* .locals 0 */
/* invoke-direct {p0}, Lcom/miui/server/stability/LockPerfImpl$PerfInfo;->getDurationString()Ljava/lang/String; */
} // .end method
static java.lang.String -$$Nest$mgetMethodString ( com.miui.server.stability.LockPerfImpl$PerfInfo p0 ) { //bridge//synthethic
/* .locals 0 */
/* invoke-direct {p0}, Lcom/miui/server/stability/LockPerfImpl$PerfInfo;->getMethodString()Ljava/lang/String; */
} // .end method
static Boolean -$$Nest$mmatchMethod ( com.miui.server.stability.LockPerfImpl$PerfInfo p0, java.lang.String p1 ) { //bridge//synthethic
/* .locals 0 */
p0 = /* invoke-direct {p0, p1}, Lcom/miui/server/stability/LockPerfImpl$PerfInfo;->matchMethod(Ljava/lang/String;)Z */
} // .end method
static Boolean -$$Nest$mmatchStackTraceElement ( com.miui.server.stability.LockPerfImpl$PerfInfo p0, java.lang.StackTraceElement p1 ) { //bridge//synthethic
/* .locals 0 */
p0 = /* invoke-direct {p0, p1}, Lcom/miui/server/stability/LockPerfImpl$PerfInfo;->matchStackTraceElement(Ljava/lang/StackTraceElement;)Z */
} // .end method
static void -$$Nest$msetDuration ( com.miui.server.stability.LockPerfImpl$PerfInfo p0, Long p1 ) { //bridge//synthethic
/* .locals 0 */
/* invoke-direct {p0, p1, p2}, Lcom/miui/server/stability/LockPerfImpl$PerfInfo;->setDuration(J)V */
return;
} // .end method
 com.miui.server.stability.LockPerfImpl$PerfInfo ( ) {
/* .locals 2 */
/* .param p1, "fullMethod" # Ljava/lang/String; */
/* .param p2, "start" # J */
/* .line 175 */
/* invoke-direct {p0}, Ljava/lang/Object;-><init>()V */
/* .line 173 */
/* const-wide/16 v0, -0x1 */
/* iput-wide v0, p0, Lcom/miui/server/stability/LockPerfImpl$PerfInfo;->duration:J */
/* .line 176 */
this.fullMethod = p1;
/* .line 177 */
/* iput-wide p2, p0, Lcom/miui/server/stability/LockPerfImpl$PerfInfo;->start:J */
/* .line 178 */
return;
} // .end method
private java.lang.String getClassName ( ) {
/* .locals 1 */
/* .line 197 */
v0 = this.className;
/* if-nez v0, :cond_0 */
/* .line 198 */
/* invoke-direct {p0}, Lcom/miui/server/stability/LockPerfImpl$PerfInfo;->initMethodDetail()V */
/* .line 200 */
} // :cond_0
v0 = this.className;
} // .end method
private java.lang.String getDurationString ( ) {
/* .locals 4 */
/* .line 225 */
/* iget-wide v0, p0, Lcom/miui/server/stability/LockPerfImpl$PerfInfo;->duration:J */
/* const-wide/16 v2, 0x0 */
/* cmp-long v0, v0, v2 */
/* if-ltz v0, :cond_0 */
/* new-instance v0, Ljava/lang/StringBuilder; */
/* invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V */
final String v1 = "duration="; // const-string v1, "duration="
(( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
/* iget-wide v1, p0, Lcom/miui/server/stability/LockPerfImpl$PerfInfo;->duration:J */
(( java.lang.StringBuilder ) v0 ).append ( v1, v2 ); // invoke-virtual {v0, v1, v2}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;
final String v1 = " ms"; // const-string v1, " ms"
(( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v0 ).toString ( ); // invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
} // :cond_0
/* const-string/jumbo v0, "un-finished" */
} // :goto_0
} // .end method
private java.lang.String getMethodDesc ( ) {
/* .locals 1 */
/* .line 211 */
v0 = this.methodDesc;
/* if-nez v0, :cond_0 */
/* .line 212 */
/* invoke-direct {p0}, Lcom/miui/server/stability/LockPerfImpl$PerfInfo;->initMethodDetail()V */
/* .line 214 */
} // :cond_0
v0 = this.methodDesc;
} // .end method
private java.lang.String getMethodName ( ) {
/* .locals 1 */
/* .line 204 */
v0 = this.methodName;
/* if-nez v0, :cond_0 */
/* .line 205 */
/* invoke-direct {p0}, Lcom/miui/server/stability/LockPerfImpl$PerfInfo;->initMethodDetail()V */
/* .line 207 */
} // :cond_0
v0 = this.methodName;
} // .end method
private java.lang.String getMethodString ( ) {
/* .locals 1 */
/* .line 193 */
v0 = this.fullMethod;
} // .end method
private void initMethodDetail ( ) {
/* .locals 2 */
/* .line 218 */
v0 = this.fullMethod;
com.miui.server.stability.LockPerfImpl .-$$Nest$smparseFullMethod ( v0 );
/* .line 219 */
/* .local v0, "strs":[Ljava/lang/String; */
int v1 = 0; // const/4 v1, 0x0
/* aget-object v1, v0, v1 */
this.className = v1;
/* .line 220 */
int v1 = 1; // const/4 v1, 0x1
/* aget-object v1, v0, v1 */
this.methodName = v1;
/* .line 221 */
int v1 = 2; // const/4 v1, 0x2
/* aget-object v1, v0, v1 */
this.methodDesc = v1;
/* .line 222 */
return;
} // .end method
private Boolean matchMethod ( java.lang.String p0 ) {
/* .locals 1 */
/* .param p1, "fullMethod" # Ljava/lang/String; */
/* .line 189 */
v0 = this.fullMethod;
v0 = (( java.lang.String ) v0 ).equals ( p1 ); // invoke-virtual {v0, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z
} // .end method
private Boolean matchStackTraceElement ( java.lang.StackTraceElement p0 ) {
/* .locals 2 */
/* .param p1, "element" # Ljava/lang/StackTraceElement; */
/* .line 185 */
/* invoke-direct {p0}, Lcom/miui/server/stability/LockPerfImpl$PerfInfo;->getClassName()Ljava/lang/String; */
(( java.lang.StackTraceElement ) p1 ).getClassName ( ); // invoke-virtual {p1}, Ljava/lang/StackTraceElement;->getClassName()Ljava/lang/String;
v0 = (( java.lang.String ) v0 ).equals ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z
if ( v0 != null) { // if-eqz v0, :cond_0
/* invoke-direct {p0}, Lcom/miui/server/stability/LockPerfImpl$PerfInfo;->getMethodName()Ljava/lang/String; */
(( java.lang.StackTraceElement ) p1 ).getMethodName ( ); // invoke-virtual {p1}, Ljava/lang/StackTraceElement;->getMethodName()Ljava/lang/String;
v0 = (( java.lang.String ) v0 ).equals ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z
if ( v0 != null) { // if-eqz v0, :cond_0
int v0 = 1; // const/4 v0, 0x1
} // :cond_0
int v0 = 0; // const/4 v0, 0x0
} // :goto_0
} // .end method
private void setDuration ( Long p0 ) {
/* .locals 0 */
/* .param p1, "duration" # J */
/* .line 181 */
/* iput-wide p1, p0, Lcom/miui/server/stability/LockPerfImpl$PerfInfo;->duration:J */
/* .line 182 */
return;
} // .end method
