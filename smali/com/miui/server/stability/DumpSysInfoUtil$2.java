class com.miui.server.stability.DumpSysInfoUtil$2 implements java.lang.Runnable {
	 /* .source "DumpSysInfoUtil.java" */
	 /* # interfaces */
	 /* # annotations */
	 /* .annotation system Ldalvik/annotation/EnclosingMethod; */
	 /* value = Lcom/miui/server/stability/DumpSysInfoUtil;->crawlLogcat()V */
} // .end annotation
/* .annotation system Ldalvik/annotation/InnerClass; */
/* accessFlags = 0x0 */
/* name = null */
} // .end annotation
/* # direct methods */
 com.miui.server.stability.DumpSysInfoUtil$2 ( ) {
/* .locals 0 */
/* .line 122 */
/* invoke-direct {p0}, Ljava/lang/Object;-><init>()V */
return;
} // .end method
/* # virtual methods */
public void run ( ) {
/* .locals 7 */
/* .line 126 */
final String v0 = "logcat"; // const-string v0, "logcat"
try { // :try_start_0
	 final String v1 = "logcat.txt"; // const-string v1, "logcat.txt"
	 com.miui.server.stability.DumpSysInfoUtil .-$$Nest$smcreateFile ( v1 );
	 /* .line 127 */
	 /* .local v1, "path":Ljava/lang/String; */
	 com.miui.server.stability.DumpSysInfoUtil .-$$Nest$smgetmDaemon ( );
	 /* .line 128 */
	 /* .local v2, "mClient":Lmiui/mqsas/IMQSNative; */
	 if ( v2 != null) { // if-eqz v2, :cond_0
		 if ( v1 != null) { // if-eqz v1, :cond_0
			 /* .line 129 */
			 int v3 = 2; // const/4 v3, 0x2
			 /* new-array v4, v3, [Ljava/lang/String; */
			 int v5 = 0; // const/4 v5, 0x0
			 /* aput-object v0, v4, v5 */
			 int v6 = 1; // const/4 v6, 0x1
			 /* aput-object v0, v4, v6 */
			 java.util.Arrays .asList ( v4 );
			 /* .line 130 */
			 /* .local v0, "actions":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;" */
			 /* new-array v3, v3, [Ljava/lang/String; */
			 final String v4 = "-v threadtime -d *:v"; // const-string v4, "-v threadtime -d *:v"
			 /* aput-object v4, v3, v5 */
			 final String v4 = "-v threadtime -b events -d *:v"; // const-string v4, "-v threadtime -b events -d *:v"
			 /* aput-object v4, v3, v6 */
			 java.util.Arrays .asList ( v3 );
			 /* .line 132 */
			 /* .local v3, "params":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;" */
			 int v4 = 3; // const/4 v4, 0x3
			 /* :try_end_0 */
			 /* .catch Ljava/lang/Exception; {:try_start_0 ..:try_end_0} :catch_0 */
			 /* .line 136 */
		 } // .end local v0 # "actions":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;"
	 } // .end local v1 # "path":Ljava/lang/String;
} // .end local v2 # "mClient":Lmiui/mqsas/IMQSNative;
} // .end local v3 # "params":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;"
} // :cond_0
/* .line 134 */
/* :catch_0 */
/* move-exception v0 */
/* .line 135 */
/* .local v0, "e":Ljava/lang/Exception; */
final String v1 = "DumpSysInfoUtil"; // const-string v1, "DumpSysInfoUtil"
final String v2 = "crash in the crawlLogcat()"; // const-string v2, "crash in the crawlLogcat()"
android.util.Slog .e ( v1,v2,v0 );
/* .line 137 */
} // .end local v0 # "e":Ljava/lang/Exception;
} // :goto_0
return;
} // .end method
