public class com.miui.server.stability.StabilityMemoryMonitor {
	 /* .source "StabilityMemoryMonitor.java" */
	 /* # annotations */
	 /* .annotation system Ldalvik/annotation/MemberClasses; */
	 /* value = { */
	 /* Lcom/miui/server/stability/StabilityMemoryMonitor$AlarmReceiver; */
	 /* } */
} // .end annotation
/* # static fields */
private static final java.lang.String ACTION_SMM_ALARM;
private static final Integer ALARM_HOUR;
private static final Integer ALARM_MINUTE;
private static final Integer ALARM_SECOND;
private static final java.lang.String CLOUD_CONFIG_FILE;
private static final java.util.HashMap DEFAULT_CONFIG_DATA;
/* .annotation system Ldalvik/annotation/Signature; */
/* value = { */
/* "Ljava/util/HashMap<", */
/* "Ljava/lang/String;", */
/* "Ljava/lang/Long;", */
/* ">;" */
/* } */
} // .end annotation
} // .end field
private static final java.lang.String SMM_CONTROL_PROPERTY;
private static final java.lang.String TAG;
private static volatile com.miui.server.stability.StabilityMemoryMonitor sInstance;
/* # instance fields */
private android.content.Context mContext;
private Boolean mEnabled;
private volatile Boolean mPendingWork;
private volatile Boolean mScreenState;
/* # direct methods */
public static void $r8$lambda$6uMJGpZ24PDQiQz19BGYy8VVh7w ( com.miui.server.stability.StabilityMemoryMonitor p0 ) { //synthethic
/* .locals 0 */
/* invoke-direct {p0}, Lcom/miui/server/stability/StabilityMemoryMonitor;->lambda$sendTask$0()V */
return;
} // .end method
static Boolean -$$Nest$fgetmEnabled ( com.miui.server.stability.StabilityMemoryMonitor p0 ) { //bridge//synthethic
/* .locals 0 */
/* iget-boolean p0, p0, Lcom/miui/server/stability/StabilityMemoryMonitor;->mEnabled:Z */
} // .end method
static Boolean -$$Nest$fgetmScreenState ( com.miui.server.stability.StabilityMemoryMonitor p0 ) { //bridge//synthethic
/* .locals 0 */
/* iget-boolean p0, p0, Lcom/miui/server/stability/StabilityMemoryMonitor;->mScreenState:Z */
} // .end method
static void -$$Nest$fputmEnabled ( com.miui.server.stability.StabilityMemoryMonitor p0, Boolean p1 ) { //bridge//synthethic
/* .locals 0 */
/* iput-boolean p1, p0, Lcom/miui/server/stability/StabilityMemoryMonitor;->mEnabled:Z */
return;
} // .end method
static void -$$Nest$fputmPendingWork ( com.miui.server.stability.StabilityMemoryMonitor p0, Boolean p1 ) { //bridge//synthethic
/* .locals 0 */
/* iput-boolean p1, p0, Lcom/miui/server/stability/StabilityMemoryMonitor;->mPendingWork:Z */
return;
} // .end method
static void -$$Nest$msendTask ( com.miui.server.stability.StabilityMemoryMonitor p0 ) { //bridge//synthethic
/* .locals 0 */
/* invoke-direct {p0}, Lcom/miui/server/stability/StabilityMemoryMonitor;->sendTask()V */
return;
} // .end method
static com.miui.server.stability.StabilityMemoryMonitor ( ) {
/* .locals 1 */
/* .line 40 */
/* new-instance v0, Lcom/miui/server/stability/StabilityMemoryMonitor$1; */
/* invoke-direct {v0}, Lcom/miui/server/stability/StabilityMemoryMonitor$1;-><init>()V */
return;
} // .end method
private com.miui.server.stability.StabilityMemoryMonitor ( ) {
/* .locals 2 */
/* .line 81 */
/* invoke-direct {p0}, Ljava/lang/Object;-><init>()V */
/* .line 55 */
int v0 = 0; // const/4 v0, 0x0
/* iput-boolean v0, p0, Lcom/miui/server/stability/StabilityMemoryMonitor;->mPendingWork:Z */
/* .line 57 */
/* iput-boolean v0, p0, Lcom/miui/server/stability/StabilityMemoryMonitor;->mScreenState:Z */
/* .line 82 */
final String v1 = "persist.sys.stability_memory_monitor.enable"; // const-string v1, "persist.sys.stability_memory_monitor.enable"
v0 = android.os.SystemProperties .getBoolean ( v1,v0 );
/* iput-boolean v0, p0, Lcom/miui/server/stability/StabilityMemoryMonitor;->mEnabled:Z */
/* .line 83 */
return;
} // .end method
private java.util.HashMap getCloudConfig ( ) {
/* .locals 11 */
/* .annotation system Ldalvik/annotation/Signature; */
/* value = { */
/* "()", */
/* "Ljava/util/HashMap<", */
/* "Ljava/lang/String;", */
/* "Ljava/lang/Long;", */
/* ">;" */
/* } */
} // .end annotation
/* .line 183 */
/* new-instance v0, Ljava/io/File; */
final String v1 = "/data/mqsas/cloud/stability_memory_monitor_config.json"; // const-string v1, "/data/mqsas/cloud/stability_memory_monitor_config.json"
/* invoke-direct {v0, v1}, Ljava/io/File;-><init>(Ljava/lang/String;)V */
/* .line 184 */
/* .local v0, "cloudConfigFile":Ljava/io/File; */
/* new-instance v1, Ljava/util/HashMap; */
v2 = com.miui.server.stability.StabilityMemoryMonitor.DEFAULT_CONFIG_DATA;
/* invoke-direct {v1, v2}, Ljava/util/HashMap;-><init>(Ljava/util/Map;)V */
/* .line 186 */
/* .local v1, "configData":Ljava/util/HashMap;, "Ljava/util/HashMap<Ljava/lang/String;Ljava/lang/Long;>;" */
v2 = (( java.io.File ) v0 ).exists ( ); // invoke-virtual {v0}, Ljava/io/File;->exists()Z
final String v3 = "StabilityMemoryMonitor"; // const-string v3, "StabilityMemoryMonitor"
if ( v2 != null) { // if-eqz v2, :cond_3
/* .line 188 */
int v2 = 0; // const/4 v2, 0x0
int v4 = 0; // const/4 v4, 0x0
try { // :try_start_0
android.os.FileUtils .readTextFile ( v0,v2,v4 );
/* .line 189 */
/* .local v2, "originData":Ljava/lang/String; */
v4 = (( java.lang.String ) v2 ).isEmpty ( ); // invoke-virtual {v2}, Ljava/lang/String;->isEmpty()Z
/* if-nez v4, :cond_2 */
/* .line 190 */
/* new-instance v4, Lorg/json/JSONObject; */
/* invoke-direct {v4, v2}, Lorg/json/JSONObject;-><init>(Ljava/lang/String;)V */
/* .line 191 */
/* .local v4, "jsonConfig":Lorg/json/JSONObject; */
final String v5 = "config"; // const-string v5, "config"
(( org.json.JSONObject ) v4 ).getJSONArray ( v5 ); // invoke-virtual {v4, v5}, Lorg/json/JSONObject;->getJSONArray(Ljava/lang/String;)Lorg/json/JSONArray;
/* .line 192 */
/* .local v5, "configItems":Lorg/json/JSONArray; */
int v6 = 0; // const/4 v6, 0x0
/* .local v6, "i":I */
} // :goto_0
v7 = (( org.json.JSONArray ) v5 ).length ( ); // invoke-virtual {v5}, Lorg/json/JSONArray;->length()I
/* if-ge v6, v7, :cond_1 */
/* .line 193 */
(( org.json.JSONArray ) v5 ).get ( v6 ); // invoke-virtual {v5, v6}, Lorg/json/JSONArray;->get(I)Ljava/lang/Object;
/* check-cast v7, Lorg/json/JSONObject; */
/* .line 194 */
/* .local v7, "item":Lorg/json/JSONObject; */
final String v8 = "name"; // const-string v8, "name"
(( org.json.JSONObject ) v7 ).getString ( v8 ); // invoke-virtual {v7, v8}, Lorg/json/JSONObject;->getString(Ljava/lang/String;)Ljava/lang/String;
/* .line 195 */
/* .local v8, "appPackage":Ljava/lang/String; */
final String v9 = "pss"; // const-string v9, "pss"
v9 = (( org.json.JSONObject ) v7 ).getInt ( v9 ); // invoke-virtual {v7, v9}, Lorg/json/JSONObject;->getInt(Ljava/lang/String;)I
/* int-to-long v9, v9 */
java.lang.Long .valueOf ( v9,v10 );
/* .line 196 */
/* .local v9, "appPssLimit":Ljava/lang/Long; */
v10 = (( java.util.HashMap ) v1 ).containsKey ( v8 ); // invoke-virtual {v1, v8}, Ljava/util/HashMap;->containsKey(Ljava/lang/Object;)Z
if ( v10 != null) { // if-eqz v10, :cond_0
/* .line 197 */
(( java.util.HashMap ) v1 ).replace ( v8, v9 ); // invoke-virtual {v1, v8, v9}, Ljava/util/HashMap;->replace(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;
/* .line 199 */
} // :cond_0
(( java.util.HashMap ) v1 ).put ( v8, v9 ); // invoke-virtual {v1, v8, v9}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;
/* .line 192 */
} // .end local v7 # "item":Lorg/json/JSONObject;
} // .end local v8 # "appPackage":Ljava/lang/String;
} // .end local v9 # "appPssLimit":Ljava/lang/Long;
} // :goto_1
/* add-int/lit8 v6, v6, 0x1 */
/* .line 202 */
} // .end local v4 # "jsonConfig":Lorg/json/JSONObject;
} // .end local v5 # "configItems":Lorg/json/JSONArray;
} // .end local v6 # "i":I
} // :cond_1
/* .line 203 */
} // :cond_2
final String v4 = "cloud config file contains noting"; // const-string v4, "cloud config file contains noting"
android.util.Slog .e ( v3,v4 );
/* :try_end_0 */
/* .catch Ljava/io/IOException; {:try_start_0 ..:try_end_0} :catch_1 */
/* .catch Lorg/json/JSONException; {:try_start_0 ..:try_end_0} :catch_0 */
/* .line 208 */
} // .end local v2 # "originData":Ljava/lang/String;
/* :catch_0 */
/* move-exception v2 */
/* .line 209 */
/* .local v2, "e":Lorg/json/JSONException; */
final String v4 = "retrive config data failed: JSONException"; // const-string v4, "retrive config data failed: JSONException"
android.util.Slog .e ( v3,v4 );
/* .line 210 */
(( java.util.HashMap ) v1 ).clear ( ); // invoke-virtual {v1}, Ljava/util/HashMap;->clear()V
/* .line 211 */
v3 = com.miui.server.stability.StabilityMemoryMonitor.DEFAULT_CONFIG_DATA;
(( java.util.HashMap ) v1 ).putAll ( v3 ); // invoke-virtual {v1, v3}, Ljava/util/HashMap;->putAll(Ljava/util/Map;)V
/* .line 212 */
(( org.json.JSONException ) v2 ).printStackTrace ( ); // invoke-virtual {v2}, Lorg/json/JSONException;->printStackTrace()V
} // .end local v2 # "e":Lorg/json/JSONException;
/* .line 205 */
/* :catch_1 */
/* move-exception v2 */
/* .line 206 */
/* .local v2, "e":Ljava/io/IOException; */
final String v4 = "read cloud config file failed: IOException"; // const-string v4, "read cloud config file failed: IOException"
android.util.Slog .e ( v3,v4 );
/* .line 207 */
(( java.io.IOException ) v2 ).printStackTrace ( ); // invoke-virtual {v2}, Ljava/io/IOException;->printStackTrace()V
/* .line 213 */
} // .end local v2 # "e":Ljava/io/IOException;
} // :goto_2
/* .line 215 */
} // :cond_3
final String v2 = "can\'t find cloud config file"; // const-string v2, "can\'t find cloud config file"
android.util.Slog .e ( v3,v2 );
/* .line 218 */
} // :goto_3
} // .end method
public static com.miui.server.stability.StabilityMemoryMonitor getInstance ( ) {
/* .locals 1 */
/* .line 86 */
v0 = com.miui.server.stability.StabilityMemoryMonitor.sInstance;
/* if-nez v0, :cond_0 */
/* .line 87 */
/* new-instance v0, Lcom/miui/server/stability/StabilityMemoryMonitor; */
/* invoke-direct {v0}, Lcom/miui/server/stability/StabilityMemoryMonitor;-><init>()V */
/* .line 89 */
} // :cond_0
v0 = com.miui.server.stability.StabilityMemoryMonitor.sInstance;
} // .end method
private Integer getPidByPackage ( java.lang.String p0 ) {
/* .locals 6 */
/* .param p1, "packageName" # Ljava/lang/String; */
/* .line 169 */
v0 = this.mContext;
final String v1 = "activity"; // const-string v1, "activity"
(( android.content.Context ) v0 ).getSystemService ( v1 ); // invoke-virtual {v0, v1}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;
/* check-cast v0, Landroid/app/ActivityManager; */
/* .line 170 */
/* .local v0, "am":Landroid/app/ActivityManager; */
(( android.app.ActivityManager ) v0 ).getRunningAppProcesses ( ); // invoke-virtual {v0}, Landroid/app/ActivityManager;->getRunningAppProcesses()Ljava/util/List;
/* .line 171 */
/* .local v1, "infos":Ljava/util/List;, "Ljava/util/List<Landroid/app/ActivityManager$RunningAppProcessInfo;>;" */
int v2 = -1; // const/4 v2, -0x1
/* .line 173 */
/* .local v2, "pid":I */
v4 = } // :goto_0
if ( v4 != null) { // if-eqz v4, :cond_1
/* check-cast v4, Landroid/app/ActivityManager$RunningAppProcessInfo; */
/* .line 174 */
/* .local v4, "info":Landroid/app/ActivityManager$RunningAppProcessInfo; */
v5 = this.processName;
v5 = (( java.lang.String ) v5 ).equals ( p1 ); // invoke-virtual {v5, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z
if ( v5 != null) { // if-eqz v5, :cond_0
/* .line 175 */
/* iget v2, v4, Landroid/app/ActivityManager$RunningAppProcessInfo;->pid:I */
/* .line 176 */
/* .line 178 */
} // .end local v4 # "info":Landroid/app/ActivityManager$RunningAppProcessInfo;
} // :cond_0
/* .line 179 */
} // :cond_1
} // :goto_1
} // .end method
private Long getPssByPackage ( java.lang.String p0 ) {
/* .locals 7 */
/* .param p1, "packageName" # Ljava/lang/String; */
/* .line 155 */
v0 = this.mContext;
final String v1 = "activity"; // const-string v1, "activity"
(( android.content.Context ) v0 ).getSystemService ( v1 ); // invoke-virtual {v0, v1}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;
/* check-cast v0, Landroid/app/ActivityManager; */
/* .line 156 */
/* .local v0, "am":Landroid/app/ActivityManager; */
(( android.app.ActivityManager ) v0 ).getRunningAppProcesses ( ); // invoke-virtual {v0}, Landroid/app/ActivityManager;->getRunningAppProcesses()Ljava/util/List;
/* .line 157 */
/* .local v1, "infos":Ljava/util/List;, "Ljava/util/List<Landroid/app/ActivityManager$RunningAppProcessInfo;>;" */
/* const-wide/16 v2, 0x0 */
/* .line 159 */
/* .local v2, "pss":J */
v5 = } // :goto_0
if ( v5 != null) { // if-eqz v5, :cond_1
/* check-cast v5, Landroid/app/ActivityManager$RunningAppProcessInfo; */
/* .line 160 */
/* .local v5, "info":Landroid/app/ActivityManager$RunningAppProcessInfo; */
v6 = this.processName;
v6 = (( java.lang.String ) v6 ).equals ( p1 ); // invoke-virtual {v6, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z
if ( v6 != null) { // if-eqz v6, :cond_0
/* .line 161 */
/* iget v4, v5, Landroid/app/ActivityManager$RunningAppProcessInfo;->pid:I */
int v6 = 0; // const/4 v6, 0x0
android.os.Debug .getPss ( v4,v6,v6 );
/* move-result-wide v2 */
/* .line 162 */
/* .line 164 */
} // .end local v5 # "info":Landroid/app/ActivityManager$RunningAppProcessInfo;
} // :cond_0
/* .line 165 */
} // :cond_1
} // :goto_1
/* return-wide v2 */
} // .end method
private void lambda$sendTask$0 ( ) { //synthethic
/* .locals 9 */
/* .line 114 */
/* invoke-direct {p0}, Lcom/miui/server/stability/StabilityMemoryMonitor;->getCloudConfig()Ljava/util/HashMap; */
(( java.util.HashMap ) v0 ).entrySet ( ); // invoke-virtual {v0}, Ljava/util/HashMap;->entrySet()Ljava/util/Set;
v1 = } // :goto_0
if ( v1 != null) { // if-eqz v1, :cond_1
/* check-cast v1, Ljava/util/Map$Entry; */
/* .line 115 */
/* .local v1, "entry":Ljava/util/Map$Entry;, "Ljava/util/Map$Entry<Ljava/lang/String;Ljava/lang/Long;>;" */
/* check-cast v2, Ljava/lang/String; */
/* .line 116 */
/* .local v2, "procName":Ljava/lang/String; */
/* check-cast v3, Ljava/lang/Long; */
(( java.lang.Long ) v3 ).longValue ( ); // invoke-virtual {v3}, Ljava/lang/Long;->longValue()J
/* move-result-wide v3 */
/* .line 117 */
/* .local v3, "pssLimit":J */
/* invoke-direct {p0, v2}, Lcom/miui/server/stability/StabilityMemoryMonitor;->getPssByPackage(Ljava/lang/String;)J */
/* move-result-wide v5 */
/* .line 118 */
/* .local v5, "pssNow":J */
/* cmp-long v7, v5, v3 */
/* if-lez v7, :cond_0 */
/* .line 119 */
v7 = /* invoke-direct {p0, v2}, Lcom/miui/server/stability/StabilityMemoryMonitor;->getPidByPackage(Ljava/lang/String;)I */
android.os.Process .killProcess ( v7 );
/* .line 120 */
/* new-instance v7, Ljava/lang/StringBuilder; */
/* invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V */
final String v8 = "kill process: "; // const-string v8, "kill process: "
(( java.lang.StringBuilder ) v7 ).append ( v8 ); // invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v7 ).append ( v2 ); // invoke-virtual {v7, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
final String v8 = " due to abnormal PSS occupied: "; // const-string v8, " due to abnormal PSS occupied: "
(( java.lang.StringBuilder ) v7 ).append ( v8 ); // invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v7 ).append ( v5, v6 ); // invoke-virtual {v7, v5, v6}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v7 ).toString ( ); // invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
final String v8 = "StabilityMemoryMonitor"; // const-string v8, "StabilityMemoryMonitor"
android.util.Slog .w ( v8,v7 );
/* .line 123 */
} // .end local v1 # "entry":Ljava/util/Map$Entry;, "Ljava/util/Map$Entry<Ljava/lang/String;Ljava/lang/Long;>;"
} // .end local v2 # "procName":Ljava/lang/String;
} // .end local v3 # "pssLimit":J
} // .end local v5 # "pssNow":J
} // :cond_0
/* .line 124 */
} // :cond_1
return;
} // .end method
private void registerAlarmReceiver ( ) {
/* .locals 3 */
/* .line 129 */
/* new-instance v0, Landroid/content/IntentFilter; */
/* invoke-direct {v0}, Landroid/content/IntentFilter;-><init>()V */
/* .line 130 */
/* .local v0, "filter":Landroid/content/IntentFilter; */
final String v1 = "miui.intent.action.SET_STABILITY_MEMORY_MONITOR_ALARM"; // const-string v1, "miui.intent.action.SET_STABILITY_MEMORY_MONITOR_ALARM"
(( android.content.IntentFilter ) v0 ).addAction ( v1 ); // invoke-virtual {v0, v1}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V
/* .line 131 */
v1 = this.mContext;
/* new-instance v2, Lcom/miui/server/stability/StabilityMemoryMonitor$AlarmReceiver; */
/* invoke-direct {v2, p0}, Lcom/miui/server/stability/StabilityMemoryMonitor$AlarmReceiver;-><init>(Lcom/miui/server/stability/StabilityMemoryMonitor;)V */
(( android.content.Context ) v1 ).registerReceiver ( v2, v0 ); // invoke-virtual {v1, v2, v0}, Landroid/content/Context;->registerReceiver(Landroid/content/BroadcastReceiver;Landroid/content/IntentFilter;)Landroid/content/Intent;
/* .line 132 */
return;
} // .end method
private void sendTask ( ) {
/* .locals 2 */
/* .line 112 */
com.android.internal.os.BackgroundThread .getHandler ( );
/* .line 113 */
/* .local v0, "bgHandler":Landroid/os/Handler; */
/* new-instance v1, Lcom/miui/server/stability/StabilityMemoryMonitor$$ExternalSyntheticLambda0; */
/* invoke-direct {v1, p0}, Lcom/miui/server/stability/StabilityMemoryMonitor$$ExternalSyntheticLambda0;-><init>(Lcom/miui/server/stability/StabilityMemoryMonitor;)V */
(( android.os.Handler ) v0 ).post ( v1 ); // invoke-virtual {v0, v1}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z
/* .line 125 */
int v1 = 0; // const/4 v1, 0x0
/* iput-boolean v1, p0, Lcom/miui/server/stability/StabilityMemoryMonitor;->mPendingWork:Z */
/* .line 126 */
return;
} // .end method
private void setAlarm ( ) {
/* .locals 12 */
/* .line 135 */
v0 = this.mContext;
final String v1 = "alarm"; // const-string v1, "alarm"
(( android.content.Context ) v0 ).getSystemService ( v1 ); // invoke-virtual {v0, v1}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;
/* check-cast v0, Landroid/app/AlarmManager; */
/* .line 136 */
/* .local v0, "alarm":Landroid/app/AlarmManager; */
/* new-instance v1, Landroid/content/Intent; */
/* invoke-direct {v1}, Landroid/content/Intent;-><init>()V */
/* move-object v8, v1 */
/* .line 137 */
/* .local v8, "intent":Landroid/content/Intent; */
final String v1 = "miui.intent.action.SET_STABILITY_MEMORY_MONITOR_ALARM"; // const-string v1, "miui.intent.action.SET_STABILITY_MEMORY_MONITOR_ALARM"
(( android.content.Intent ) v8 ).setAction ( v1 ); // invoke-virtual {v8, v1}, Landroid/content/Intent;->setAction(Ljava/lang/String;)Landroid/content/Intent;
/* .line 138 */
v1 = this.mContext;
/* const/high16 v2, 0x4000000 */
int v3 = 0; // const/4 v3, 0x0
android.app.PendingIntent .getBroadcast ( v1,v3,v8,v2 );
/* .line 141 */
/* .local v9, "pi":Landroid/app/PendingIntent; */
java.util.Calendar .getInstance ( );
/* .line 142 */
/* .local v10, "calendar":Ljava/util/Calendar; */
/* const/16 v1, 0xb */
int v2 = 3; // const/4 v2, 0x3
(( java.util.Calendar ) v10 ).set ( v1, v2 ); // invoke-virtual {v10, v1, v2}, Ljava/util/Calendar;->set(II)V
/* .line 143 */
/* const/16 v1, 0xc */
(( java.util.Calendar ) v10 ).set ( v1, v3 ); // invoke-virtual {v10, v1, v3}, Ljava/util/Calendar;->set(II)V
/* .line 144 */
/* const/16 v1, 0xd */
(( java.util.Calendar ) v10 ).set ( v1, v3 ); // invoke-virtual {v10, v1, v3}, Ljava/util/Calendar;->set(II)V
/* .line 145 */
java.util.Calendar .getInstance ( );
/* .line 146 */
/* .local v11, "now":Ljava/util/Calendar; */
v1 = (( java.util.Calendar ) v11 ).compareTo ( v10 ); // invoke-virtual {v11, v10}, Ljava/util/Calendar;->compareTo(Ljava/util/Calendar;)I
/* if-lez v1, :cond_0 */
/* .line 147 */
int v1 = 5; // const/4 v1, 0x5
int v2 = 1; // const/4 v2, 0x1
(( java.util.Calendar ) v10 ).add ( v1, v2 ); // invoke-virtual {v10, v1, v2}, Ljava/util/Calendar;->add(II)V
/* .line 150 */
} // :cond_0
int v2 = 0; // const/4 v2, 0x0
(( java.util.Calendar ) v10 ).getTimeInMillis ( ); // invoke-virtual {v10}, Ljava/util/Calendar;->getTimeInMillis()J
/* move-result-wide v3 */
/* const-wide/32 v5, 0x5265c00 */
/* move-object v1, v0 */
/* move-object v7, v9 */
/* invoke-virtual/range {v1 ..v7}, Landroid/app/AlarmManager;->setRepeating(IJJLandroid/app/PendingIntent;)V */
/* .line 152 */
return;
} // .end method
/* # virtual methods */
public void initContext ( android.content.Context p0 ) {
/* .locals 0 */
/* .param p1, "context" # Landroid/content/Context; */
/* .line 93 */
this.mContext = p1;
/* .line 94 */
return;
} // .end method
public void startMemoryMonitor ( ) {
/* .locals 0 */
/* .line 97 */
/* invoke-direct {p0}, Lcom/miui/server/stability/StabilityMemoryMonitor;->registerAlarmReceiver()V */
/* .line 98 */
/* invoke-direct {p0}, Lcom/miui/server/stability/StabilityMemoryMonitor;->setAlarm()V */
/* .line 99 */
return;
} // .end method
public void updateScreenState ( Boolean p0 ) {
/* .locals 1 */
/* .param p1, "screenOn" # Z */
/* .line 102 */
/* iput-boolean p1, p0, Lcom/miui/server/stability/StabilityMemoryMonitor;->mScreenState:Z */
/* .line 103 */
/* iget-boolean v0, p0, Lcom/miui/server/stability/StabilityMemoryMonitor;->mEnabled:Z */
/* if-nez v0, :cond_0 */
/* .line 104 */
return;
/* .line 106 */
} // :cond_0
/* iget-boolean v0, p0, Lcom/miui/server/stability/StabilityMemoryMonitor;->mScreenState:Z */
/* if-nez v0, :cond_1 */
/* iget-boolean v0, p0, Lcom/miui/server/stability/StabilityMemoryMonitor;->mPendingWork:Z */
if ( v0 != null) { // if-eqz v0, :cond_1
/* .line 107 */
/* invoke-direct {p0}, Lcom/miui/server/stability/StabilityMemoryMonitor;->sendTask()V */
/* .line 109 */
} // :cond_1
return;
} // .end method
