.class Lcom/miui/server/stability/ScoutDisplayMemoryManager$4;
.super Ljava/lang/Object;
.source "ScoutDisplayMemoryManager.java"

# interfaces
.implements Ljava/lang/Runnable;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/miui/server/stability/ScoutDisplayMemoryManager;->reportGpuMemoryLeakException(Ljava/lang/String;J)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/miui/server/stability/ScoutDisplayMemoryManager;

.field final synthetic val$reason:Ljava/lang/String;

.field final synthetic val$totalSizeKB:J


# direct methods
.method constructor <init>(Lcom/miui/server/stability/ScoutDisplayMemoryManager;JLjava/lang/String;)V
    .locals 0
    .param p1, "this$0"    # Lcom/miui/server/stability/ScoutDisplayMemoryManager;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .line 535
    iput-object p1, p0, Lcom/miui/server/stability/ScoutDisplayMemoryManager$4;->this$0:Lcom/miui/server/stability/ScoutDisplayMemoryManager;

    iput-wide p2, p0, Lcom/miui/server/stability/ScoutDisplayMemoryManager$4;->val$totalSizeKB:J

    iput-object p4, p0, Lcom/miui/server/stability/ScoutDisplayMemoryManager$4;->val$reason:Ljava/lang/String;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public run()V
    .locals 20

    .line 538
    move-object/from16 v0, p0

    const-string v1, ""

    .line 539
    .local v1, "gpuMemoryLog":Ljava/lang/String;
    iget-object v2, v0, Lcom/miui/server/stability/ScoutDisplayMemoryManager$4;->this$0:Lcom/miui/server/stability/ScoutDisplayMemoryManager;

    invoke-static {v2}, Lcom/miui/server/stability/ScoutDisplayMemoryManager;->-$$Nest$fgetgpuType(Lcom/miui/server/stability/ScoutDisplayMemoryManager;)I

    move-result v3

    invoke-static {v2, v3}, Lcom/miui/server/stability/ScoutDisplayMemoryManager;->-$$Nest$mreadGpuMemoryInfo(Lcom/miui/server/stability/ScoutDisplayMemoryManager;I)Lcom/miui/server/stability/GpuMemoryUsageInfo;

    move-result-object v2

    .line 540
    .local v2, "gpuMemoryInfo":Lcom/miui/server/stability/GpuMemoryUsageInfo;
    const/4 v3, 0x0

    .line 541
    .local v3, "infoList":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/miui/server/stability/GpuMemoryProcUsageInfo;>;"
    const-wide/16 v4, 0x400

    const/4 v6, 0x0

    const-string v7, "MIUIScout Memory"

    if-eqz v2, :cond_0

    .line 542
    invoke-virtual {v2}, Lcom/miui/server/stability/GpuMemoryUsageInfo;->getList()Ljava/util/ArrayList;

    move-result-object v3

    .line 543
    new-instance v8, Lcom/miui/server/stability/ScoutDisplayMemoryManager$4$1;

    invoke-direct {v8, v0}, Lcom/miui/server/stability/ScoutDisplayMemoryManager$4$1;-><init>(Lcom/miui/server/stability/ScoutDisplayMemoryManager$4;)V

    invoke-static {v3, v8}, Ljava/util/Collections;->sort(Ljava/util/List;Ljava/util/Comparator;)V

    .line 555
    iget-object v8, v0, Lcom/miui/server/stability/ScoutDisplayMemoryManager$4;->this$0:Lcom/miui/server/stability/ScoutDisplayMemoryManager;

    invoke-static {v8}, Lcom/miui/server/stability/ScoutDisplayMemoryManager;->-$$Nest$fgetgpuType(Lcom/miui/server/stability/ScoutDisplayMemoryManager;)I

    move-result v8

    invoke-virtual {v2, v8}, Lcom/miui/server/stability/GpuMemoryUsageInfo;->toString(I)Ljava/lang/String;

    move-result-object v1

    .line 556
    invoke-static {v7, v1}, Landroid/util/Slog;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 557
    invoke-virtual {v2}, Lcom/miui/server/stability/GpuMemoryUsageInfo;->getTotalSize()J

    move-result-wide v8

    invoke-static {}, Lcom/miui/server/stability/ScoutDisplayMemoryManager;->-$$Nest$sfgetGPU_MEMORY_LEAK_THRESHOLD()I

    move-result v10

    int-to-long v10, v10

    mul-long/2addr v10, v4

    cmp-long v8, v8, v10

    if-gez v8, :cond_0

    .line 558
    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "TotalSize "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    iget-wide v8, v0, Lcom/miui/server/stability/ScoutDisplayMemoryManager$4;->val$totalSizeKB:J

    invoke-static {v8, v9}, Lcom/miui/server/stability/ScoutDisplayMemoryManager;->stringifyKBSize(J)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, " gpu memory, GpuMemoryInfo totalSize "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    .line 559
    invoke-virtual {v2}, Lcom/miui/server/stability/GpuMemoryUsageInfo;->getTotalSize()J

    move-result-wide v8

    invoke-virtual {v4, v8, v9}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, "kB less than threshold, Skip"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    .line 560
    .local v4, "failMsg":Ljava/lang/String;
    invoke-static {v7, v4}, Landroid/util/Slog;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 561
    iget-object v5, v0, Lcom/miui/server/stability/ScoutDisplayMemoryManager$4;->this$0:Lcom/miui/server/stability/ScoutDisplayMemoryManager;

    invoke-static {v5}, Lcom/miui/server/stability/ScoutDisplayMemoryManager;->-$$Nest$fgetisBusy(Lcom/miui/server/stability/ScoutDisplayMemoryManager;)Ljava/util/concurrent/atomic/AtomicBoolean;

    move-result-object v5

    invoke-virtual {v5, v6}, Ljava/util/concurrent/atomic/AtomicBoolean;->set(Z)V

    .line 562
    return-void

    .line 565
    .end local v4    # "failMsg":Ljava/lang/String;
    :cond_0
    const/4 v8, 0x0

    .line 566
    .local v8, "topProc":Lcom/miui/server/stability/GpuMemoryProcUsageInfo;
    const/4 v9, 0x0

    .line 567
    .local v9, "sfProc":Lcom/miui/server/stability/GpuMemoryProcUsageInfo;
    if-eqz v1, :cond_6

    if-eqz v3, :cond_6

    invoke-virtual {v3}, Ljava/util/ArrayList;->size()I

    move-result v10

    if-lez v10, :cond_6

    .line 568
    invoke-virtual {v3, v6}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v10

    move-object v8, v10

    check-cast v8, Lcom/miui/server/stability/GpuMemoryProcUsageInfo;

    .line 569
    new-instance v10, Ljava/lang/StringBuilder;

    invoke-direct {v10}, Ljava/lang/StringBuilder;-><init>()V

    const-string v11, "Most used process name="

    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-virtual {v8}, Lcom/miui/server/stability/GpuMemoryProcUsageInfo;->getName()Ljava/lang/String;

    move-result-object v11

    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    const-string v11, " pid="

    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    .line 570
    invoke-virtual {v8}, Lcom/miui/server/stability/GpuMemoryProcUsageInfo;->getPid()I

    move-result v11

    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v10

    const-string v11, " adj="

    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-virtual {v8}, Lcom/miui/server/stability/GpuMemoryProcUsageInfo;->getOomadj()I

    move-result v11

    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v10

    const-string v11, " size="

    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    .line 571
    invoke-virtual {v8}, Lcom/miui/server/stability/GpuMemoryProcUsageInfo;->getRss()J

    move-result-wide v11

    invoke-virtual {v10, v11, v12}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v10

    const-string v11, "kB"

    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-virtual {v10}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v10

    .line 569
    invoke-static {v7, v10}, Landroid/util/Slog;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 572
    sget-object v10, Lcom/miui/server/stability/ScoutMemoryUtils;->gpuMemoryWhiteList:Ljava/util/Set;

    invoke-virtual {v8}, Lcom/miui/server/stability/GpuMemoryProcUsageInfo;->getName()Ljava/lang/String;

    move-result-object v11

    invoke-interface {v10, v11}, Ljava/util/Set;->contains(Ljava/lang/Object;)Z

    move-result v10

    if-eqz v10, :cond_1

    .line 573
    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v8}, Lcom/miui/server/stability/GpuMemoryProcUsageInfo;->getName()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, " is white app, skip"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v7, v4}, Landroid/util/Slog;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 574
    iget-object v4, v0, Lcom/miui/server/stability/ScoutDisplayMemoryManager$4;->this$0:Lcom/miui/server/stability/ScoutDisplayMemoryManager;

    invoke-static {v4}, Lcom/miui/server/stability/ScoutDisplayMemoryManager;->-$$Nest$fgetisBusy(Lcom/miui/server/stability/ScoutDisplayMemoryManager;)Ljava/util/concurrent/atomic/AtomicBoolean;

    move-result-object v4

    invoke-virtual {v4, v6}, Ljava/util/concurrent/atomic/AtomicBoolean;->set(Z)V

    .line 575
    return-void

    .line 577
    :cond_1
    const-string v10, "/system/bin/surfaceflinger"

    invoke-virtual {v8}, Lcom/miui/server/stability/GpuMemoryProcUsageInfo;->getName()Ljava/lang/String;

    move-result-object v11

    invoke-virtual {v10, v11}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v10

    if-eqz v10, :cond_2

    .line 578
    move-object v9, v8

    .line 580
    :cond_2
    iget-object v10, v0, Lcom/miui/server/stability/ScoutDisplayMemoryManager$4;->val$reason:Ljava/lang/String;

    iget-object v11, v0, Lcom/miui/server/stability/ScoutDisplayMemoryManager$4;->this$0:Lcom/miui/server/stability/ScoutDisplayMemoryManager;

    invoke-static {v11}, Lcom/miui/server/stability/ScoutDisplayMemoryManager;->-$$Nest$fgetgpuType(Lcom/miui/server/stability/ScoutDisplayMemoryManager;)I

    move-result v11

    invoke-static {v1, v10, v11}, Lcom/miui/server/stability/ScoutMemoryUtils;->captureGpuMemoryLeakLog(Ljava/lang/String;Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v10

    .line 581
    .local v10, "filePath":Ljava/lang/String;
    invoke-static {}, Lcom/miui/server/stability/ScoutDisplayMemoryManager;->-$$Nest$sfgetGPU_MEMORY_LEAK_THRESHOLD()I

    move-result v11

    int-to-long v11, v11

    mul-long/2addr v11, v4

    long-to-double v11, v11

    const-wide v13, 0x3fe3333333333333L    # 0.6

    mul-double/2addr v11, v13

    double-to-long v13, v11

    .line 582
    .local v13, "thresholdSize":J
    new-instance v17, Lcom/miui/server/stability/ScoutDisplayMemoryManager$DiaplayMemoryErrorInfo;

    iget-wide v11, v0, Lcom/miui/server/stability/ScoutDisplayMemoryManager$4;->val$totalSizeKB:J

    move-wide v15, v11

    move-object/from16 v11, v17

    move-object v12, v8

    move-wide/from16 v18, v13

    .end local v13    # "thresholdSize":J
    .local v18, "thresholdSize":J
    invoke-direct/range {v11 .. v16}, Lcom/miui/server/stability/ScoutDisplayMemoryManager$DiaplayMemoryErrorInfo;-><init>(Lcom/miui/server/stability/GpuMemoryProcUsageInfo;JJ)V

    .line 583
    .local v11, "errorInfo":Lcom/miui/server/stability/ScoutDisplayMemoryManager$DiaplayMemoryErrorInfo;
    invoke-virtual {v8}, Lcom/miui/server/stability/GpuMemoryProcUsageInfo;->getRss()J

    move-result-wide v12

    cmp-long v12, v12, v18

    if-lez v12, :cond_4

    .line 584
    invoke-static {v11}, Lcom/miui/server/stability/ScoutDisplayMemoryManager;->preserveCrimeSceneIfNeed(Lcom/miui/server/stability/ScoutDisplayMemoryManager$DiaplayMemoryErrorInfo;)Z

    move-result v12

    if-nez v12, :cond_4

    .line 585
    iget-object v4, v0, Lcom/miui/server/stability/ScoutDisplayMemoryManager$4;->this$0:Lcom/miui/server/stability/ScoutDisplayMemoryManager;

    invoke-virtual {v4}, Lcom/miui/server/stability/ScoutDisplayMemoryManager;->isEnableResumeFeature()Z

    move-result v4

    if-eqz v4, :cond_3

    .line 586
    iget-object v4, v0, Lcom/miui/server/stability/ScoutDisplayMemoryManager$4;->this$0:Lcom/miui/server/stability/ScoutDisplayMemoryManager;

    invoke-static {v4, v11}, Lcom/miui/server/stability/ScoutDisplayMemoryManager;->-$$Nest$mresumeMemLeak(Lcom/miui/server/stability/ScoutDisplayMemoryManager;Lcom/miui/server/stability/ScoutDisplayMemoryManager$DiaplayMemoryErrorInfo;)V

    goto :goto_0

    .line 588
    :cond_3
    iget-object v4, v0, Lcom/miui/server/stability/ScoutDisplayMemoryManager$4;->this$0:Lcom/miui/server/stability/ScoutDisplayMemoryManager;

    invoke-virtual {v4, v11}, Lcom/miui/server/stability/ScoutDisplayMemoryManager;->reportDisplayMemoryLeakEvent(Lcom/miui/server/stability/ScoutDisplayMemoryManager$DiaplayMemoryErrorInfo;)V

    .line 590
    :goto_0
    iget-object v4, v0, Lcom/miui/server/stability/ScoutDisplayMemoryManager$4;->this$0:Lcom/miui/server/stability/ScoutDisplayMemoryManager;

    invoke-virtual {v4, v11, v10}, Lcom/miui/server/stability/ScoutDisplayMemoryManager;->handleReportMqs(Lcom/miui/server/stability/ScoutDisplayMemoryManager$DiaplayMemoryErrorInfo;Ljava/lang/String;)V

    goto :goto_1

    .line 591
    :cond_4
    invoke-static {}, Lmiui/mqsas/scout/ScoutUtils;->isLibraryTest()Z

    move-result v12

    if-eqz v12, :cond_5

    if-eqz v9, :cond_5

    .line 592
    invoke-virtual {v9}, Lcom/miui/server/stability/GpuMemoryProcUsageInfo;->getRss()J

    move-result-wide v12

    invoke-static {}, Lcom/miui/server/stability/ScoutDisplayMemoryManager;->-$$Nest$sfgetGPU_MEMORY_LEAK_THRESHOLD()I

    move-result v14

    int-to-long v14, v14

    mul-long/2addr v14, v4

    cmp-long v4, v12, v14

    if-lez v4, :cond_5

    iget-wide v4, v0, Lcom/miui/server/stability/ScoutDisplayMemoryManager$4;->val$totalSizeKB:J

    .line 593
    const-string/jumbo v12, "surfaceflinger"

    const/4 v13, -0x1

    const-string v14, "GpuMemory"

    invoke-static {v12, v13, v4, v5, v14}, Lcom/miui/server/stability/ScoutDisplayMemoryManager;->-$$Nest$smdoPreserveCrimeSceneIfNeed(Ljava/lang/String;IJLjava/lang/String;)Z

    move-result v4

    if-nez v4, :cond_5

    .line 595
    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "System consumed "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    iget-wide v12, v0, Lcom/miui/server/stability/ScoutDisplayMemoryManager$4;->val$totalSizeKB:J

    invoke-virtual {v4, v12, v13}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, "KB GpuMemory totally"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v7, v4}, Landroid/util/Slog;->w(Ljava/lang/String;Ljava/lang/String;)I

    .line 598
    :cond_5
    :goto_1
    iget-object v4, v0, Lcom/miui/server/stability/ScoutDisplayMemoryManager$4;->this$0:Lcom/miui/server/stability/ScoutDisplayMemoryManager;

    invoke-virtual {v4}, Lcom/miui/server/stability/ScoutDisplayMemoryManager;->updateLastReportTime()V

    .line 600
    .end local v10    # "filePath":Ljava/lang/String;
    .end local v11    # "errorInfo":Lcom/miui/server/stability/ScoutDisplayMemoryManager$DiaplayMemoryErrorInfo;
    .end local v18    # "thresholdSize":J
    :cond_6
    iget-object v4, v0, Lcom/miui/server/stability/ScoutDisplayMemoryManager$4;->this$0:Lcom/miui/server/stability/ScoutDisplayMemoryManager;

    invoke-static {v4}, Lcom/miui/server/stability/ScoutDisplayMemoryManager;->-$$Nest$fgetisBusy(Lcom/miui/server/stability/ScoutDisplayMemoryManager;)Ljava/util/concurrent/atomic/AtomicBoolean;

    move-result-object v4

    invoke-virtual {v4, v6}, Ljava/util/concurrent/atomic/AtomicBoolean;->set(Z)V

    .line 601
    return-void
.end method
