class com.miui.server.stability.LockPerfImpl$ThreadLocalData {
	 /* .source "LockPerfImpl.java" */
	 /* # annotations */
	 /* .annotation system Ldalvik/annotation/EnclosingClass; */
	 /* value = Lcom/miui/server/stability/LockPerfImpl; */
} // .end annotation
/* .annotation system Ldalvik/annotation/InnerClass; */
/* accessFlags = 0xa */
/* name = "ThreadLocalData" */
} // .end annotation
/* # instance fields */
private final com.android.internal.util.RingBuffer debugEventBuffer;
/* .annotation system Ldalvik/annotation/Signature; */
/* value = { */
/* "Lcom/android/internal/util/RingBuffer<", */
/* "Ljava/lang/String;", */
/* ">;" */
/* } */
} // .end annotation
} // .end field
private final com.miui.server.stability.LockPerfImpl$PerfStack perfStack;
/* .annotation system Ldalvik/annotation/Signature; */
/* value = { */
/* "Lcom/miui/server/stability/LockPerfImpl$PerfStack<", */
/* "Lcom/miui/server/stability/LockPerfImpl$PerfInfo;", */
/* ">;" */
/* } */
} // .end annotation
} // .end field
private java.lang.StackTraceElement unwindingStack;
/* # direct methods */
static com.android.internal.util.RingBuffer -$$Nest$fgetdebugEventBuffer ( com.miui.server.stability.LockPerfImpl$ThreadLocalData p0 ) { //bridge//synthethic
/* .locals 0 */
p0 = this.debugEventBuffer;
} // .end method
static com.miui.server.stability.LockPerfImpl$PerfStack -$$Nest$fgetperfStack ( com.miui.server.stability.LockPerfImpl$ThreadLocalData p0 ) { //bridge//synthethic
/* .locals 0 */
p0 = this.perfStack;
} // .end method
static java.lang.StackTraceElement -$$Nest$fgetunwindingStack ( com.miui.server.stability.LockPerfImpl$ThreadLocalData p0 ) { //bridge//synthethic
/* .locals 0 */
p0 = this.unwindingStack;
} // .end method
static void -$$Nest$mrecordEvent ( com.miui.server.stability.LockPerfImpl$ThreadLocalData p0 ) { //bridge//synthethic
/* .locals 0 */
/* invoke-direct {p0}, Lcom/miui/server/stability/LockPerfImpl$ThreadLocalData;->recordEvent()V */
return;
} // .end method
static void -$$Nest$msetUnwindingStack ( com.miui.server.stability.LockPerfImpl$ThreadLocalData p0, java.lang.StackTraceElement[] p1 ) { //bridge//synthethic
/* .locals 0 */
/* invoke-direct {p0, p1}, Lcom/miui/server/stability/LockPerfImpl$ThreadLocalData;->setUnwindingStack([Ljava/lang/StackTraceElement;)V */
return;
} // .end method
private com.miui.server.stability.LockPerfImpl$ThreadLocalData ( ) {
/* .locals 3 */
/* .line 324 */
/* invoke-direct {p0}, Ljava/lang/Object;-><init>()V */
/* .line 320 */
/* new-instance v0, Lcom/miui/server/stability/LockPerfImpl$PerfStack; */
int v1 = 0; // const/4 v1, 0x0
/* invoke-direct {v0, v1}, Lcom/miui/server/stability/LockPerfImpl$PerfStack;-><init>(Lcom/miui/server/stability/LockPerfImpl$PerfStack-IA;)V */
this.perfStack = v0;
/* .line 322 */
/* new-instance v0, Lcom/android/internal/util/RingBuffer; */
/* const-class v1, Ljava/lang/String; */
/* const/16 v2, 0x14 */
/* invoke-direct {v0, v1, v2}, Lcom/android/internal/util/RingBuffer;-><init>(Ljava/lang/Class;I)V */
this.debugEventBuffer = v0;
/* .line 325 */
return;
} // .end method
 com.miui.server.stability.LockPerfImpl$ThreadLocalData ( ) { //synthethic
/* .locals 0 */
/* invoke-direct {p0}, Lcom/miui/server/stability/LockPerfImpl$ThreadLocalData;-><init>()V */
return;
} // .end method
private void recordEvent ( ) {
/* .locals 10 */
/* .line 333 */
java.lang.Thread .currentThread ( );
/* .line 334 */
/* .local v0, "currentThread":Ljava/lang/Thread; */
/* new-instance v9, Lcom/miui/server/stability/LockPerfImpl$SlowLockedMethodEvent; */
v2 = this.perfStack;
v3 = this.unwindingStack;
(( java.lang.Thread ) v0 ).getId ( ); // invoke-virtual {v0}, Ljava/lang/Thread;->getId()J
/* move-result-wide v4 */
/* .line 335 */
(( java.lang.Thread ) v0 ).getName ( ); // invoke-virtual {v0}, Ljava/lang/Thread;->getName()Ljava/lang/String;
java.time.LocalDateTime .now ( );
int v8 = 0; // const/4 v8, 0x0
/* move-object v1, v9 */
/* invoke-direct/range {v1 ..v8}, Lcom/miui/server/stability/LockPerfImpl$SlowLockedMethodEvent;-><init>(Lcom/miui/server/stability/LockPerfImpl$PerfStack;[Ljava/lang/StackTraceElement;JLjava/lang/String;Ljava/time/LocalDateTime;Lcom/miui/server/stability/LockPerfImpl$SlowLockedMethodEvent-IA;)V */
/* .line 337 */
/* .local v1, "event":Lcom/miui/server/stability/LockPerfImpl$SlowLockedMethodEvent; */
int v2 = 0; // const/4 v2, 0x0
/* invoke-direct {p0, v2}, Lcom/miui/server/stability/LockPerfImpl$ThreadLocalData;->setUnwindingStack([Ljava/lang/StackTraceElement;)V */
/* .line 338 */
v2 = this.perfStack;
com.miui.server.stability.LockPerfImpl$PerfStack .-$$Nest$mclearBuffer ( v2 );
/* .line 339 */
com.miui.server.stability.LockPerfImpl$SlowLockedMethodEvent .-$$Nest$mprintToLogcat ( v1 );
/* .line 340 */
com.miui.server.stability.LockPerfImpl .-$$Nest$sfgetsEventRingBuffer ( );
(( com.miui.server.stability.LockPerfImpl$ThreadSafeRingBuffer ) v2 ).append ( v1 ); // invoke-virtual {v2, v1}, Lcom/miui/server/stability/LockPerfImpl$ThreadSafeRingBuffer;->append(Ljava/lang/Object;)V
/* .line 341 */
return;
} // .end method
private void setUnwindingStack ( java.lang.StackTraceElement[] p0 ) {
/* .locals 0 */
/* .param p1, "stack" # [Ljava/lang/StackTraceElement; */
/* .line 328 */
this.unwindingStack = p1;
/* .line 329 */
return;
} // .end method
/* # virtual methods */
public void outputRecentEvents ( ) {
/* .locals 5 */
/* .line 344 */
v0 = this.debugEventBuffer;
(( com.android.internal.util.RingBuffer ) v0 ).toArray ( ); // invoke-virtual {v0}, Lcom/android/internal/util/RingBuffer;->toArray()[Ljava/lang/Object;
/* check-cast v0, [Ljava/lang/String; */
/* .line 345 */
/* .local v0, "events":[Ljava/lang/String; */
int v1 = 0; // const/4 v1, 0x0
/* .local v1, "i":I */
} // :goto_0
/* array-length v2, v0 */
/* if-ge v1, v2, :cond_0 */
/* .line 346 */
com.miui.server.stability.LockPerfImpl .-$$Nest$sfgetTAG ( );
/* new-instance v3, Ljava/lang/StringBuilder; */
/* invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V */
final String v4 = "Event["; // const-string v4, "Event["
(( java.lang.StringBuilder ) v3 ).append ( v4 ); // invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v3 ).append ( v1 ); // invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;
final String v4 = "]: "; // const-string v4, "]: "
(( java.lang.StringBuilder ) v3 ).append ( v4 ); // invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
/* aget-object v4, v0, v1 */
(( java.lang.StringBuilder ) v3 ).append ( v4 ); // invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v3 ).toString ( ); // invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
android.util.Slog .e ( v2,v3 );
/* .line 345 */
/* add-int/lit8 v1, v1, 0x1 */
/* .line 348 */
} // .end local v1 # "i":I
} // :cond_0
return;
} // .end method
