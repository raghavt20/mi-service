class com.miui.server.stability.ScoutDisplayMemoryManager$4 implements java.lang.Runnable {
	 /* .source "ScoutDisplayMemoryManager.java" */
	 /* # interfaces */
	 /* # annotations */
	 /* .annotation system Ldalvik/annotation/EnclosingMethod; */
	 /* value = Lcom/miui/server/stability/ScoutDisplayMemoryManager;->reportGpuMemoryLeakException(Ljava/lang/String;J)V */
} // .end annotation
/* .annotation system Ldalvik/annotation/InnerClass; */
/* accessFlags = 0x0 */
/* name = null */
} // .end annotation
/* # instance fields */
final com.miui.server.stability.ScoutDisplayMemoryManager this$0; //synthetic
final java.lang.String val$reason; //synthetic
final Long val$totalSizeKB; //synthetic
/* # direct methods */
 com.miui.server.stability.ScoutDisplayMemoryManager$4 ( ) {
/* .locals 0 */
/* .param p1, "this$0" # Lcom/miui/server/stability/ScoutDisplayMemoryManager; */
/* .annotation system Ldalvik/annotation/Signature; */
/* value = { */
/* "()V" */
/* } */
} // .end annotation
/* .line 535 */
this.this$0 = p1;
/* iput-wide p2, p0, Lcom/miui/server/stability/ScoutDisplayMemoryManager$4;->val$totalSizeKB:J */
this.val$reason = p4;
/* invoke-direct {p0}, Ljava/lang/Object;-><init>()V */
return;
} // .end method
/* # virtual methods */
public void run ( ) {
/* .locals 20 */
/* .line 538 */
/* move-object/from16 v0, p0 */
final String v1 = ""; // const-string v1, ""
/* .line 539 */
/* .local v1, "gpuMemoryLog":Ljava/lang/String; */
v2 = this.this$0;
v3 = com.miui.server.stability.ScoutDisplayMemoryManager .-$$Nest$fgetgpuType ( v2 );
com.miui.server.stability.ScoutDisplayMemoryManager .-$$Nest$mreadGpuMemoryInfo ( v2,v3 );
/* .line 540 */
/* .local v2, "gpuMemoryInfo":Lcom/miui/server/stability/GpuMemoryUsageInfo; */
int v3 = 0; // const/4 v3, 0x0
/* .line 541 */
/* .local v3, "infoList":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/miui/server/stability/GpuMemoryProcUsageInfo;>;" */
/* const-wide/16 v4, 0x400 */
int v6 = 0; // const/4 v6, 0x0
final String v7 = "MIUIScout Memory"; // const-string v7, "MIUIScout Memory"
if ( v2 != null) { // if-eqz v2, :cond_0
/* .line 542 */
(( com.miui.server.stability.GpuMemoryUsageInfo ) v2 ).getList ( ); // invoke-virtual {v2}, Lcom/miui/server/stability/GpuMemoryUsageInfo;->getList()Ljava/util/ArrayList;
/* .line 543 */
/* new-instance v8, Lcom/miui/server/stability/ScoutDisplayMemoryManager$4$1; */
/* invoke-direct {v8, v0}, Lcom/miui/server/stability/ScoutDisplayMemoryManager$4$1;-><init>(Lcom/miui/server/stability/ScoutDisplayMemoryManager$4;)V */
java.util.Collections .sort ( v3,v8 );
/* .line 555 */
v8 = this.this$0;
v8 = com.miui.server.stability.ScoutDisplayMemoryManager .-$$Nest$fgetgpuType ( v8 );
(( com.miui.server.stability.GpuMemoryUsageInfo ) v2 ).toString ( v8 ); // invoke-virtual {v2, v8}, Lcom/miui/server/stability/GpuMemoryUsageInfo;->toString(I)Ljava/lang/String;
/* .line 556 */
android.util.Slog .e ( v7,v1 );
/* .line 557 */
(( com.miui.server.stability.GpuMemoryUsageInfo ) v2 ).getTotalSize ( ); // invoke-virtual {v2}, Lcom/miui/server/stability/GpuMemoryUsageInfo;->getTotalSize()J
/* move-result-wide v8 */
v10 = com.miui.server.stability.ScoutDisplayMemoryManager .-$$Nest$sfgetGPU_MEMORY_LEAK_THRESHOLD ( );
/* int-to-long v10, v10 */
/* mul-long/2addr v10, v4 */
/* cmp-long v8, v8, v10 */
/* if-gez v8, :cond_0 */
/* .line 558 */
/* new-instance v4, Ljava/lang/StringBuilder; */
/* invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V */
final String v5 = "TotalSize "; // const-string v5, "TotalSize "
(( java.lang.StringBuilder ) v4 ).append ( v5 ); // invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
/* iget-wide v8, v0, Lcom/miui/server/stability/ScoutDisplayMemoryManager$4;->val$totalSizeKB:J */
com.miui.server.stability.ScoutDisplayMemoryManager .stringifyKBSize ( v8,v9 );
(( java.lang.StringBuilder ) v4 ).append ( v5 ); // invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
final String v5 = " gpu memory, GpuMemoryInfo totalSize "; // const-string v5, " gpu memory, GpuMemoryInfo totalSize "
(( java.lang.StringBuilder ) v4 ).append ( v5 ); // invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
/* .line 559 */
(( com.miui.server.stability.GpuMemoryUsageInfo ) v2 ).getTotalSize ( ); // invoke-virtual {v2}, Lcom/miui/server/stability/GpuMemoryUsageInfo;->getTotalSize()J
/* move-result-wide v8 */
(( java.lang.StringBuilder ) v4 ).append ( v8, v9 ); // invoke-virtual {v4, v8, v9}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;
final String v5 = "kB less than threshold, Skip"; // const-string v5, "kB less than threshold, Skip"
(( java.lang.StringBuilder ) v4 ).append ( v5 ); // invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v4 ).toString ( ); // invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
/* .line 560 */
/* .local v4, "failMsg":Ljava/lang/String; */
android.util.Slog .e ( v7,v4 );
/* .line 561 */
v5 = this.this$0;
com.miui.server.stability.ScoutDisplayMemoryManager .-$$Nest$fgetisBusy ( v5 );
(( java.util.concurrent.atomic.AtomicBoolean ) v5 ).set ( v6 ); // invoke-virtual {v5, v6}, Ljava/util/concurrent/atomic/AtomicBoolean;->set(Z)V
/* .line 562 */
return;
/* .line 565 */
} // .end local v4 # "failMsg":Ljava/lang/String;
} // :cond_0
int v8 = 0; // const/4 v8, 0x0
/* .line 566 */
/* .local v8, "topProc":Lcom/miui/server/stability/GpuMemoryProcUsageInfo; */
int v9 = 0; // const/4 v9, 0x0
/* .line 567 */
/* .local v9, "sfProc":Lcom/miui/server/stability/GpuMemoryProcUsageInfo; */
if ( v1 != null) { // if-eqz v1, :cond_6
if ( v3 != null) { // if-eqz v3, :cond_6
v10 = (( java.util.ArrayList ) v3 ).size ( ); // invoke-virtual {v3}, Ljava/util/ArrayList;->size()I
/* if-lez v10, :cond_6 */
/* .line 568 */
(( java.util.ArrayList ) v3 ).get ( v6 ); // invoke-virtual {v3, v6}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;
/* move-object v8, v10 */
/* check-cast v8, Lcom/miui/server/stability/GpuMemoryProcUsageInfo; */
/* .line 569 */
/* new-instance v10, Ljava/lang/StringBuilder; */
/* invoke-direct {v10}, Ljava/lang/StringBuilder;-><init>()V */
final String v11 = "Most used process name="; // const-string v11, "Most used process name="
(( java.lang.StringBuilder ) v10 ).append ( v11 ); // invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( com.miui.server.stability.GpuMemoryProcUsageInfo ) v8 ).getName ( ); // invoke-virtual {v8}, Lcom/miui/server/stability/GpuMemoryProcUsageInfo;->getName()Ljava/lang/String;
(( java.lang.StringBuilder ) v10 ).append ( v11 ); // invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
final String v11 = " pid="; // const-string v11, " pid="
(( java.lang.StringBuilder ) v10 ).append ( v11 ); // invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
/* .line 570 */
v11 = (( com.miui.server.stability.GpuMemoryProcUsageInfo ) v8 ).getPid ( ); // invoke-virtual {v8}, Lcom/miui/server/stability/GpuMemoryProcUsageInfo;->getPid()I
(( java.lang.StringBuilder ) v10 ).append ( v11 ); // invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;
final String v11 = " adj="; // const-string v11, " adj="
(( java.lang.StringBuilder ) v10 ).append ( v11 ); // invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
v11 = (( com.miui.server.stability.GpuMemoryProcUsageInfo ) v8 ).getOomadj ( ); // invoke-virtual {v8}, Lcom/miui/server/stability/GpuMemoryProcUsageInfo;->getOomadj()I
(( java.lang.StringBuilder ) v10 ).append ( v11 ); // invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;
final String v11 = " size="; // const-string v11, " size="
(( java.lang.StringBuilder ) v10 ).append ( v11 ); // invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
/* .line 571 */
(( com.miui.server.stability.GpuMemoryProcUsageInfo ) v8 ).getRss ( ); // invoke-virtual {v8}, Lcom/miui/server/stability/GpuMemoryProcUsageInfo;->getRss()J
/* move-result-wide v11 */
(( java.lang.StringBuilder ) v10 ).append ( v11, v12 ); // invoke-virtual {v10, v11, v12}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;
final String v11 = "kB"; // const-string v11, "kB"
(( java.lang.StringBuilder ) v10 ).append ( v11 ); // invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v10 ).toString ( ); // invoke-virtual {v10}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
/* .line 569 */
android.util.Slog .e ( v7,v10 );
/* .line 572 */
v10 = com.miui.server.stability.ScoutMemoryUtils.gpuMemoryWhiteList;
v10 = (( com.miui.server.stability.GpuMemoryProcUsageInfo ) v8 ).getName ( ); // invoke-virtual {v8}, Lcom/miui/server/stability/GpuMemoryProcUsageInfo;->getName()Ljava/lang/String;
if ( v10 != null) { // if-eqz v10, :cond_1
	 /* .line 573 */
	 /* new-instance v4, Ljava/lang/StringBuilder; */
	 /* invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V */
	 (( com.miui.server.stability.GpuMemoryProcUsageInfo ) v8 ).getName ( ); // invoke-virtual {v8}, Lcom/miui/server/stability/GpuMemoryProcUsageInfo;->getName()Ljava/lang/String;
	 (( java.lang.StringBuilder ) v4 ).append ( v5 ); // invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
	 final String v5 = " is white app, skip"; // const-string v5, " is white app, skip"
	 (( java.lang.StringBuilder ) v4 ).append ( v5 ); // invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
	 (( java.lang.StringBuilder ) v4 ).toString ( ); // invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
	 android.util.Slog .e ( v7,v4 );
	 /* .line 574 */
	 v4 = this.this$0;
	 com.miui.server.stability.ScoutDisplayMemoryManager .-$$Nest$fgetisBusy ( v4 );
	 (( java.util.concurrent.atomic.AtomicBoolean ) v4 ).set ( v6 ); // invoke-virtual {v4, v6}, Ljava/util/concurrent/atomic/AtomicBoolean;->set(Z)V
	 /* .line 575 */
	 return;
	 /* .line 577 */
} // :cond_1
final String v10 = "/system/bin/surfaceflinger"; // const-string v10, "/system/bin/surfaceflinger"
(( com.miui.server.stability.GpuMemoryProcUsageInfo ) v8 ).getName ( ); // invoke-virtual {v8}, Lcom/miui/server/stability/GpuMemoryProcUsageInfo;->getName()Ljava/lang/String;
v10 = (( java.lang.String ) v10 ).equals ( v11 ); // invoke-virtual {v10, v11}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z
if ( v10 != null) { // if-eqz v10, :cond_2
	 /* .line 578 */
	 /* move-object v9, v8 */
	 /* .line 580 */
} // :cond_2
v10 = this.val$reason;
v11 = this.this$0;
v11 = com.miui.server.stability.ScoutDisplayMemoryManager .-$$Nest$fgetgpuType ( v11 );
com.miui.server.stability.ScoutMemoryUtils .captureGpuMemoryLeakLog ( v1,v10,v11 );
/* .line 581 */
/* .local v10, "filePath":Ljava/lang/String; */
v11 = com.miui.server.stability.ScoutDisplayMemoryManager .-$$Nest$sfgetGPU_MEMORY_LEAK_THRESHOLD ( );
/* int-to-long v11, v11 */
/* mul-long/2addr v11, v4 */
/* long-to-double v11, v11 */
/* const-wide v13, 0x3fe3333333333333L # 0.6 */
/* mul-double/2addr v11, v13 */
/* double-to-long v13, v11 */
/* .line 582 */
/* .local v13, "thresholdSize":J */
/* new-instance v17, Lcom/miui/server/stability/ScoutDisplayMemoryManager$DiaplayMemoryErrorInfo; */
/* iget-wide v11, v0, Lcom/miui/server/stability/ScoutDisplayMemoryManager$4;->val$totalSizeKB:J */
/* move-wide v15, v11 */
/* move-object/from16 v11, v17 */
/* move-object v12, v8 */
/* move-wide/from16 v18, v13 */
} // .end local v13 # "thresholdSize":J
/* .local v18, "thresholdSize":J */
/* invoke-direct/range {v11 ..v16}, Lcom/miui/server/stability/ScoutDisplayMemoryManager$DiaplayMemoryErrorInfo;-><init>(Lcom/miui/server/stability/GpuMemoryProcUsageInfo;JJ)V */
/* .line 583 */
/* .local v11, "errorInfo":Lcom/miui/server/stability/ScoutDisplayMemoryManager$DiaplayMemoryErrorInfo; */
(( com.miui.server.stability.GpuMemoryProcUsageInfo ) v8 ).getRss ( ); // invoke-virtual {v8}, Lcom/miui/server/stability/GpuMemoryProcUsageInfo;->getRss()J
/* move-result-wide v12 */
/* cmp-long v12, v12, v18 */
/* if-lez v12, :cond_4 */
/* .line 584 */
v12 = com.miui.server.stability.ScoutDisplayMemoryManager .preserveCrimeSceneIfNeed ( v11 );
/* if-nez v12, :cond_4 */
/* .line 585 */
v4 = this.this$0;
v4 = (( com.miui.server.stability.ScoutDisplayMemoryManager ) v4 ).isEnableResumeFeature ( ); // invoke-virtual {v4}, Lcom/miui/server/stability/ScoutDisplayMemoryManager;->isEnableResumeFeature()Z
if ( v4 != null) { // if-eqz v4, :cond_3
/* .line 586 */
v4 = this.this$0;
com.miui.server.stability.ScoutDisplayMemoryManager .-$$Nest$mresumeMemLeak ( v4,v11 );
/* .line 588 */
} // :cond_3
v4 = this.this$0;
(( com.miui.server.stability.ScoutDisplayMemoryManager ) v4 ).reportDisplayMemoryLeakEvent ( v11 ); // invoke-virtual {v4, v11}, Lcom/miui/server/stability/ScoutDisplayMemoryManager;->reportDisplayMemoryLeakEvent(Lcom/miui/server/stability/ScoutDisplayMemoryManager$DiaplayMemoryErrorInfo;)V
/* .line 590 */
} // :goto_0
v4 = this.this$0;
(( com.miui.server.stability.ScoutDisplayMemoryManager ) v4 ).handleReportMqs ( v11, v10 ); // invoke-virtual {v4, v11, v10}, Lcom/miui/server/stability/ScoutDisplayMemoryManager;->handleReportMqs(Lcom/miui/server/stability/ScoutDisplayMemoryManager$DiaplayMemoryErrorInfo;Ljava/lang/String;)V
/* .line 591 */
} // :cond_4
v12 = miui.mqsas.scout.ScoutUtils .isLibraryTest ( );
if ( v12 != null) { // if-eqz v12, :cond_5
if ( v9 != null) { // if-eqz v9, :cond_5
/* .line 592 */
(( com.miui.server.stability.GpuMemoryProcUsageInfo ) v9 ).getRss ( ); // invoke-virtual {v9}, Lcom/miui/server/stability/GpuMemoryProcUsageInfo;->getRss()J
/* move-result-wide v12 */
v14 = com.miui.server.stability.ScoutDisplayMemoryManager .-$$Nest$sfgetGPU_MEMORY_LEAK_THRESHOLD ( );
/* int-to-long v14, v14 */
/* mul-long/2addr v14, v4 */
/* cmp-long v4, v12, v14 */
/* if-lez v4, :cond_5 */
/* iget-wide v4, v0, Lcom/miui/server/stability/ScoutDisplayMemoryManager$4;->val$totalSizeKB:J */
/* .line 593 */
/* const-string/jumbo v12, "surfaceflinger" */
int v13 = -1; // const/4 v13, -0x1
final String v14 = "GpuMemory"; // const-string v14, "GpuMemory"
v4 = com.miui.server.stability.ScoutDisplayMemoryManager .-$$Nest$smdoPreserveCrimeSceneIfNeed ( v12,v13,v4,v5,v14 );
/* if-nez v4, :cond_5 */
/* .line 595 */
/* new-instance v4, Ljava/lang/StringBuilder; */
/* invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V */
final String v5 = "System consumed "; // const-string v5, "System consumed "
(( java.lang.StringBuilder ) v4 ).append ( v5 ); // invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
/* iget-wide v12, v0, Lcom/miui/server/stability/ScoutDisplayMemoryManager$4;->val$totalSizeKB:J */
(( java.lang.StringBuilder ) v4 ).append ( v12, v13 ); // invoke-virtual {v4, v12, v13}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;
final String v5 = "KB GpuMemory totally"; // const-string v5, "KB GpuMemory totally"
(( java.lang.StringBuilder ) v4 ).append ( v5 ); // invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v4 ).toString ( ); // invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
android.util.Slog .w ( v7,v4 );
/* .line 598 */
} // :cond_5
} // :goto_1
v4 = this.this$0;
(( com.miui.server.stability.ScoutDisplayMemoryManager ) v4 ).updateLastReportTime ( ); // invoke-virtual {v4}, Lcom/miui/server/stability/ScoutDisplayMemoryManager;->updateLastReportTime()V
/* .line 600 */
} // .end local v10 # "filePath":Ljava/lang/String;
} // .end local v11 # "errorInfo":Lcom/miui/server/stability/ScoutDisplayMemoryManager$DiaplayMemoryErrorInfo;
} // .end local v18 # "thresholdSize":J
} // :cond_6
v4 = this.this$0;
com.miui.server.stability.ScoutDisplayMemoryManager .-$$Nest$fgetisBusy ( v4 );
(( java.util.concurrent.atomic.AtomicBoolean ) v4 ).set ( v6 ); // invoke-virtual {v4, v6}, Ljava/util/concurrent/atomic/AtomicBoolean;->set(Z)V
/* .line 601 */
return;
} // .end method
