.class public Lcom/miui/server/stability/DmaBufProcUsageInfo;
.super Ljava/lang/Object;
.source "DmaBufProcUsageInfo.java"


# instance fields
.field private oomadj:I

.field private pid:I

.field private procName:Ljava/lang/String;

.field private pss:J

.field private rss:J


# direct methods
.method public constructor <init>()V
    .locals 0

    .line 13
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 14
    return-void
.end method


# virtual methods
.method public getName()Ljava/lang/String;
    .locals 1

    .line 21
    iget-object v0, p0, Lcom/miui/server/stability/DmaBufProcUsageInfo;->procName:Ljava/lang/String;

    return-object v0
.end method

.method public getOomadj()I
    .locals 1

    .line 37
    iget v0, p0, Lcom/miui/server/stability/DmaBufProcUsageInfo;->oomadj:I

    return v0
.end method

.method public getPid()I
    .locals 1

    .line 29
    iget v0, p0, Lcom/miui/server/stability/DmaBufProcUsageInfo;->pid:I

    return v0
.end method

.method public getPss()J
    .locals 2

    .line 53
    iget-wide v0, p0, Lcom/miui/server/stability/DmaBufProcUsageInfo;->pss:J

    return-wide v0
.end method

.method public getRss()J
    .locals 2

    .line 45
    iget-wide v0, p0, Lcom/miui/server/stability/DmaBufProcUsageInfo;->rss:J

    return-wide v0
.end method

.method public setName(Ljava/lang/String;)V
    .locals 0
    .param p1, "procName"    # Ljava/lang/String;

    .line 17
    iput-object p1, p0, Lcom/miui/server/stability/DmaBufProcUsageInfo;->procName:Ljava/lang/String;

    .line 18
    return-void
.end method

.method public setOomadj(I)V
    .locals 0
    .param p1, "oomadj"    # I

    .line 33
    iput p1, p0, Lcom/miui/server/stability/DmaBufProcUsageInfo;->oomadj:I

    .line 34
    return-void
.end method

.method public setPid(I)V
    .locals 0
    .param p1, "pid"    # I

    .line 25
    iput p1, p0, Lcom/miui/server/stability/DmaBufProcUsageInfo;->pid:I

    .line 26
    return-void
.end method

.method public setPss(J)V
    .locals 0
    .param p1, "pss"    # J

    .line 49
    iput-wide p1, p0, Lcom/miui/server/stability/DmaBufProcUsageInfo;->pss:J

    .line 50
    return-void
.end method

.method public setRss(J)V
    .locals 0
    .param p1, "rss"    # J

    .line 41
    iput-wide p1, p0, Lcom/miui/server/stability/DmaBufProcUsageInfo;->rss:J

    .line 42
    return-void
.end method

.method public toString()Ljava/lang/String;
    .locals 4

    .line 57
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    .line 58
    .local v0, "sb":Ljava/lang/StringBuilder;
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "DMA-BUF proc info Name: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lcom/miui/server/stability/DmaBufProcUsageInfo;->procName:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " Pid="

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget v2, p0, Lcom/miui/server/stability/DmaBufProcUsageInfo;->pid:I

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " Oomadj="

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget v2, p0, Lcom/miui/server/stability/DmaBufProcUsageInfo;->oomadj:I

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " Rss="

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-wide v2, p0, Lcom/miui/server/stability/DmaBufProcUsageInfo;->rss:J

    invoke-virtual {v1, v2, v3}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "kB Pss="

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-wide v2, p0, Lcom/miui/server/stability/DmaBufProcUsageInfo;->pss:J

    invoke-virtual {v1, v2, v3}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "kB"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 60
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    return-object v1
.end method
