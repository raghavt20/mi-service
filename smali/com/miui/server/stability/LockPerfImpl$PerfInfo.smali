.class Lcom/miui/server/stability/LockPerfImpl$PerfInfo;
.super Ljava/lang/Object;
.source "LockPerfImpl.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/miui/server/stability/LockPerfImpl;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0xa
    name = "PerfInfo"
.end annotation


# instance fields
.field private className:Ljava/lang/String;

.field private duration:J

.field private final fullMethod:Ljava/lang/String;

.field private methodDesc:Ljava/lang/String;

.field private methodName:Ljava/lang/String;

.field private final start:J


# direct methods
.method static bridge synthetic -$$Nest$fgetstart(Lcom/miui/server/stability/LockPerfImpl$PerfInfo;)J
    .locals 2

    iget-wide v0, p0, Lcom/miui/server/stability/LockPerfImpl$PerfInfo;->start:J

    return-wide v0
.end method

.method static bridge synthetic -$$Nest$mgetDurationString(Lcom/miui/server/stability/LockPerfImpl$PerfInfo;)Ljava/lang/String;
    .locals 0

    invoke-direct {p0}, Lcom/miui/server/stability/LockPerfImpl$PerfInfo;->getDurationString()Ljava/lang/String;

    move-result-object p0

    return-object p0
.end method

.method static bridge synthetic -$$Nest$mgetMethodString(Lcom/miui/server/stability/LockPerfImpl$PerfInfo;)Ljava/lang/String;
    .locals 0

    invoke-direct {p0}, Lcom/miui/server/stability/LockPerfImpl$PerfInfo;->getMethodString()Ljava/lang/String;

    move-result-object p0

    return-object p0
.end method

.method static bridge synthetic -$$Nest$mmatchMethod(Lcom/miui/server/stability/LockPerfImpl$PerfInfo;Ljava/lang/String;)Z
    .locals 0

    invoke-direct {p0, p1}, Lcom/miui/server/stability/LockPerfImpl$PerfInfo;->matchMethod(Ljava/lang/String;)Z

    move-result p0

    return p0
.end method

.method static bridge synthetic -$$Nest$mmatchStackTraceElement(Lcom/miui/server/stability/LockPerfImpl$PerfInfo;Ljava/lang/StackTraceElement;)Z
    .locals 0

    invoke-direct {p0, p1}, Lcom/miui/server/stability/LockPerfImpl$PerfInfo;->matchStackTraceElement(Ljava/lang/StackTraceElement;)Z

    move-result p0

    return p0
.end method

.method static bridge synthetic -$$Nest$msetDuration(Lcom/miui/server/stability/LockPerfImpl$PerfInfo;J)V
    .locals 0

    invoke-direct {p0, p1, p2}, Lcom/miui/server/stability/LockPerfImpl$PerfInfo;->setDuration(J)V

    return-void
.end method

.method constructor <init>(Ljava/lang/String;J)V
    .locals 2
    .param p1, "fullMethod"    # Ljava/lang/String;
    .param p2, "start"    # J

    .line 175
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 173
    const-wide/16 v0, -0x1

    iput-wide v0, p0, Lcom/miui/server/stability/LockPerfImpl$PerfInfo;->duration:J

    .line 176
    iput-object p1, p0, Lcom/miui/server/stability/LockPerfImpl$PerfInfo;->fullMethod:Ljava/lang/String;

    .line 177
    iput-wide p2, p0, Lcom/miui/server/stability/LockPerfImpl$PerfInfo;->start:J

    .line 178
    return-void
.end method

.method private getClassName()Ljava/lang/String;
    .locals 1

    .line 197
    iget-object v0, p0, Lcom/miui/server/stability/LockPerfImpl$PerfInfo;->className:Ljava/lang/String;

    if-nez v0, :cond_0

    .line 198
    invoke-direct {p0}, Lcom/miui/server/stability/LockPerfImpl$PerfInfo;->initMethodDetail()V

    .line 200
    :cond_0
    iget-object v0, p0, Lcom/miui/server/stability/LockPerfImpl$PerfInfo;->className:Ljava/lang/String;

    return-object v0
.end method

.method private getDurationString()Ljava/lang/String;
    .locals 4

    .line 225
    iget-wide v0, p0, Lcom/miui/server/stability/LockPerfImpl$PerfInfo;->duration:J

    const-wide/16 v2, 0x0

    cmp-long v0, v0, v2

    if-ltz v0, :cond_0

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "duration="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-wide v1, p0, Lcom/miui/server/stability/LockPerfImpl$PerfInfo;->duration:J

    invoke-virtual {v0, v1, v2}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, " ms"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    :cond_0
    const-string/jumbo v0, "un-finished"

    :goto_0
    return-object v0
.end method

.method private getMethodDesc()Ljava/lang/String;
    .locals 1

    .line 211
    iget-object v0, p0, Lcom/miui/server/stability/LockPerfImpl$PerfInfo;->methodDesc:Ljava/lang/String;

    if-nez v0, :cond_0

    .line 212
    invoke-direct {p0}, Lcom/miui/server/stability/LockPerfImpl$PerfInfo;->initMethodDetail()V

    .line 214
    :cond_0
    iget-object v0, p0, Lcom/miui/server/stability/LockPerfImpl$PerfInfo;->methodDesc:Ljava/lang/String;

    return-object v0
.end method

.method private getMethodName()Ljava/lang/String;
    .locals 1

    .line 204
    iget-object v0, p0, Lcom/miui/server/stability/LockPerfImpl$PerfInfo;->methodName:Ljava/lang/String;

    if-nez v0, :cond_0

    .line 205
    invoke-direct {p0}, Lcom/miui/server/stability/LockPerfImpl$PerfInfo;->initMethodDetail()V

    .line 207
    :cond_0
    iget-object v0, p0, Lcom/miui/server/stability/LockPerfImpl$PerfInfo;->methodName:Ljava/lang/String;

    return-object v0
.end method

.method private getMethodString()Ljava/lang/String;
    .locals 1

    .line 193
    iget-object v0, p0, Lcom/miui/server/stability/LockPerfImpl$PerfInfo;->fullMethod:Ljava/lang/String;

    return-object v0
.end method

.method private initMethodDetail()V
    .locals 2

    .line 218
    iget-object v0, p0, Lcom/miui/server/stability/LockPerfImpl$PerfInfo;->fullMethod:Ljava/lang/String;

    invoke-static {v0}, Lcom/miui/server/stability/LockPerfImpl;->-$$Nest$smparseFullMethod(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v0

    .line 219
    .local v0, "strs":[Ljava/lang/String;
    const/4 v1, 0x0

    aget-object v1, v0, v1

    iput-object v1, p0, Lcom/miui/server/stability/LockPerfImpl$PerfInfo;->className:Ljava/lang/String;

    .line 220
    const/4 v1, 0x1

    aget-object v1, v0, v1

    iput-object v1, p0, Lcom/miui/server/stability/LockPerfImpl$PerfInfo;->methodName:Ljava/lang/String;

    .line 221
    const/4 v1, 0x2

    aget-object v1, v0, v1

    iput-object v1, p0, Lcom/miui/server/stability/LockPerfImpl$PerfInfo;->methodDesc:Ljava/lang/String;

    .line 222
    return-void
.end method

.method private matchMethod(Ljava/lang/String;)Z
    .locals 1
    .param p1, "fullMethod"    # Ljava/lang/String;

    .line 189
    iget-object v0, p0, Lcom/miui/server/stability/LockPerfImpl$PerfInfo;->fullMethod:Ljava/lang/String;

    invoke-virtual {v0, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    return v0
.end method

.method private matchStackTraceElement(Ljava/lang/StackTraceElement;)Z
    .locals 2
    .param p1, "element"    # Ljava/lang/StackTraceElement;

    .line 185
    invoke-direct {p0}, Lcom/miui/server/stability/LockPerfImpl$PerfInfo;->getClassName()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1}, Ljava/lang/StackTraceElement;->getClassName()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-direct {p0}, Lcom/miui/server/stability/LockPerfImpl$PerfInfo;->getMethodName()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1}, Ljava/lang/StackTraceElement;->getMethodName()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    :goto_0
    return v0
.end method

.method private setDuration(J)V
    .locals 0
    .param p1, "duration"    # J

    .line 181
    iput-wide p1, p0, Lcom/miui/server/stability/LockPerfImpl$PerfInfo;->duration:J

    .line 182
    return-void
.end method
