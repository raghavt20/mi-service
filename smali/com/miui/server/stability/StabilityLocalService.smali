.class public Lcom/miui/server/stability/StabilityLocalService;
.super Ljava/lang/Object;
.source "StabilityLocalService.java"

# interfaces
.implements Lcom/miui/server/stability/StabilityLocalServiceInternal;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/miui/server/stability/StabilityLocalService$Lifecycle;
    }
.end annotation


# static fields
.field private static final TAG:Ljava/lang/String;

.field private static mStabilityMemoryMonitor:Lcom/miui/server/stability/StabilityMemoryMonitor;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .line 9
    const-class v0, Lcom/miui/server/stability/StabilityLocalService;

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/miui/server/stability/StabilityLocalService;->TAG:Ljava/lang/String;

    .line 11
    invoke-static {}, Lcom/miui/server/stability/StabilityMemoryMonitor;->getInstance()Lcom/miui/server/stability/StabilityMemoryMonitor;

    move-result-object v0

    sput-object v0, Lcom/miui/server/stability/StabilityLocalService;->mStabilityMemoryMonitor:Lcom/miui/server/stability/StabilityMemoryMonitor;

    .line 10
    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .line 7
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public captureDumpLog()V
    .locals 0

    .line 27
    invoke-static {}, Lcom/miui/server/stability/DumpSysInfoUtil;->captureDumpLog()V

    .line 28
    return-void
.end method

.method public crawlLogsByPower()V
    .locals 0

    .line 32
    invoke-static {}, Lcom/miui/server/stability/DumpSysInfoUtil;->crawlLogsByPower()V

    .line 33
    return-void
.end method

.method public initContext(Landroid/content/Context;)V
    .locals 1
    .param p1, "context"    # Landroid/content/Context;

    .line 37
    sget-object v0, Lcom/miui/server/stability/StabilityLocalService;->mStabilityMemoryMonitor:Lcom/miui/server/stability/StabilityMemoryMonitor;

    invoke-virtual {v0, p1}, Lcom/miui/server/stability/StabilityMemoryMonitor;->initContext(Landroid/content/Context;)V

    .line 38
    return-void
.end method

.method public startMemoryMonitor()V
    .locals 1

    .line 42
    sget-object v0, Lcom/miui/server/stability/StabilityLocalService;->mStabilityMemoryMonitor:Lcom/miui/server/stability/StabilityMemoryMonitor;

    invoke-virtual {v0}, Lcom/miui/server/stability/StabilityMemoryMonitor;->startMemoryMonitor()V

    .line 43
    return-void
.end method
