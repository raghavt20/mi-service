.class Lcom/miui/server/stability/ScoutDisplayMemoryManager$3;
.super Ljava/lang/Object;
.source "ScoutDisplayMemoryManager.java"

# interfaces
.implements Ljava/lang/Runnable;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/miui/server/stability/ScoutDisplayMemoryManager;->reportDmabufLeakException(Ljava/lang/String;J)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/miui/server/stability/ScoutDisplayMemoryManager;

.field final synthetic val$reason:Ljava/lang/String;

.field final synthetic val$totalSizeKB:J


# direct methods
.method constructor <init>(Lcom/miui/server/stability/ScoutDisplayMemoryManager;JLjava/lang/String;)V
    .locals 0
    .param p1, "this$0"    # Lcom/miui/server/stability/ScoutDisplayMemoryManager;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .line 439
    iput-object p1, p0, Lcom/miui/server/stability/ScoutDisplayMemoryManager$3;->this$0:Lcom/miui/server/stability/ScoutDisplayMemoryManager;

    iput-wide p2, p0, Lcom/miui/server/stability/ScoutDisplayMemoryManager$3;->val$totalSizeKB:J

    iput-object p4, p0, Lcom/miui/server/stability/ScoutDisplayMemoryManager$3;->val$reason:Ljava/lang/String;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public run()V
    .locals 22

    .line 442
    move-object/from16 v0, p0

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Warring!!! dma-buf leak:"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-wide v2, v0, Lcom/miui/server/stability/ScoutDisplayMemoryManager$3;->val$totalSizeKB:J

    .line 443
    invoke-static {v2, v3}, Lcom/miui/server/stability/ScoutDisplayMemoryManager;->stringifyKBSize(J)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    .line 444
    iget-object v2, v0, Lcom/miui/server/stability/ScoutDisplayMemoryManager$3;->this$0:Lcom/miui/server/stability/ScoutDisplayMemoryManager;

    invoke-static {v2}, Lcom/miui/server/stability/ScoutDisplayMemoryManager;->-$$Nest$fgetisCameraForeground(Lcom/miui/server/stability/ScoutDisplayMemoryManager;)Z

    move-result v2

    if-eqz v2, :cond_0

    const-string v2, " , camera in the foreground"

    goto :goto_0

    :cond_0
    const-string v2, ""

    :goto_0
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    .line 442
    const-string v2, "MIUIScout Memory"

    invoke-static {v2, v1}, Landroid/util/Slog;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 445
    const-string v1, ""

    .line 446
    .local v1, "dambufLog":Ljava/lang/String;
    iget-object v3, v0, Lcom/miui/server/stability/ScoutDisplayMemoryManager$3;->this$0:Lcom/miui/server/stability/ScoutDisplayMemoryManager;

    invoke-static {v3}, Lcom/miui/server/stability/ScoutDisplayMemoryManager;->-$$Nest$mreadDmabufInfo(Lcom/miui/server/stability/ScoutDisplayMemoryManager;)Lcom/miui/server/stability/DmaBufUsageInfo;

    move-result-object v3

    .line 447
    .local v3, "dmabufInfo":Lcom/miui/server/stability/DmaBufUsageInfo;
    const/4 v4, 0x0

    .line 448
    .local v4, "infoList":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/miui/server/stability/DmaBufProcUsageInfo;>;"
    const/4 v5, 0x0

    const-wide/16 v6, 0x400

    if-eqz v3, :cond_1

    .line 449
    invoke-virtual {v3}, Lcom/miui/server/stability/DmaBufUsageInfo;->getList()Ljava/util/ArrayList;

    move-result-object v4

    .line 450
    new-instance v8, Lcom/miui/server/stability/ScoutDisplayMemoryManager$3$1;

    invoke-direct {v8, v0}, Lcom/miui/server/stability/ScoutDisplayMemoryManager$3$1;-><init>(Lcom/miui/server/stability/ScoutDisplayMemoryManager$3;)V

    invoke-static {v4, v8}, Ljava/util/Collections;->sort(Ljava/util/List;Ljava/util/Comparator;)V

    .line 462
    invoke-virtual {v3}, Lcom/miui/server/stability/DmaBufUsageInfo;->toString()Ljava/lang/String;

    move-result-object v1

    .line 463
    invoke-static {v2, v1}, Landroid/util/Slog;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 464
    invoke-virtual {v3}, Lcom/miui/server/stability/DmaBufUsageInfo;->getTotalSize()J

    move-result-wide v8

    invoke-static {}, Lcom/miui/server/stability/ScoutDisplayMemoryManager;->-$$Nest$sfgetDMABUF_LEAK_THRESHOLD()I

    move-result v10

    int-to-long v10, v10

    mul-long/2addr v10, v6

    cmp-long v8, v8, v10

    if-gez v8, :cond_1

    .line 465
    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string v7, "TotalSize "

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    iget-wide v7, v0, Lcom/miui/server/stability/ScoutDisplayMemoryManager$3;->val$totalSizeKB:J

    invoke-static {v7, v8}, Lcom/miui/server/stability/ScoutDisplayMemoryManager;->stringifyKBSize(J)Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    const-string v7, " dma-buf, DmabufInfo totalSize "

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    .line 466
    invoke-virtual {v3}, Lcom/miui/server/stability/DmaBufUsageInfo;->getTotalSize()J

    move-result-wide v7

    invoke-virtual {v6, v7, v8}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v6

    const-string v7, "kB less than threshold, Skip"

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    .line 467
    .local v6, "failMsg":Ljava/lang/String;
    invoke-static {v2, v6}, Landroid/util/Slog;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 468
    iget-object v2, v0, Lcom/miui/server/stability/ScoutDisplayMemoryManager$3;->this$0:Lcom/miui/server/stability/ScoutDisplayMemoryManager;

    invoke-static {v2}, Lcom/miui/server/stability/ScoutDisplayMemoryManager;->-$$Nest$fgetisBusy(Lcom/miui/server/stability/ScoutDisplayMemoryManager;)Ljava/util/concurrent/atomic/AtomicBoolean;

    move-result-object v2

    invoke-virtual {v2, v5}, Ljava/util/concurrent/atomic/AtomicBoolean;->set(Z)V

    .line 469
    return-void

    .line 472
    .end local v6    # "failMsg":Ljava/lang/String;
    :cond_1
    const/4 v8, 0x0

    .line 473
    .local v8, "topProc":Lcom/miui/server/stability/DmaBufProcUsageInfo;
    const/4 v9, 0x0

    .line 474
    .local v9, "sfProc":Lcom/miui/server/stability/DmaBufProcUsageInfo;
    if-eqz v4, :cond_a

    .line 475
    new-instance v10, Ljava/util/HashSet;

    const/4 v11, 0x5

    invoke-direct {v10, v11}, Ljava/util/HashSet;-><init>(I)V

    .line 476
    .local v10, "fdPids":Ljava/util/HashSet;, "Ljava/util/HashSet<Ljava/lang/Integer;>;"
    invoke-virtual {v4}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v11

    :goto_1
    invoke-interface {v11}, Ljava/util/Iterator;->hasNext()Z

    move-result v12

    if-eqz v12, :cond_4

    invoke-interface {v11}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v12

    check-cast v12, Lcom/miui/server/stability/DmaBufProcUsageInfo;

    .line 477
    .local v12, "procInfo":Lcom/miui/server/stability/DmaBufProcUsageInfo;
    invoke-virtual {v12}, Lcom/miui/server/stability/DmaBufProcUsageInfo;->getName()Ljava/lang/String;

    move-result-object v13

    .line 478
    .local v13, "procName":Ljava/lang/String;
    const-string v14, "/system/bin/surfaceflinger"

    invoke-virtual {v14, v13}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v14

    if-eqz v14, :cond_2

    .line 479
    move-object v9, v12

    .line 481
    :cond_2
    sget-object v14, Lcom/miui/server/stability/ScoutMemoryUtils;->skipIonProcList:Ljava/util/Set;

    invoke-interface {v14, v13}, Ljava/util/Set;->contains(Ljava/lang/Object;)Z

    move-result v14

    const-string v15, " adj="

    if-eqz v14, :cond_3

    .line 482
    invoke-virtual {v12}, Lcom/miui/server/stability/DmaBufProcUsageInfo;->getPid()I

    move-result v14

    invoke-static {v14}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v14

    invoke-virtual {v10, v14}, Ljava/util/HashSet;->add(Ljava/lang/Object;)Z

    .line 483
    new-instance v14, Ljava/lang/StringBuilder;

    invoke-direct {v14}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "Skip "

    invoke-virtual {v14, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, v13}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v14, "(pid="

    invoke-virtual {v5, v14}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v12}, Lcom/miui/server/stability/DmaBufProcUsageInfo;->getPid()I

    move-result v14

    invoke-virtual {v5, v14}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, v15}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    .line 484
    invoke-virtual {v12}, Lcom/miui/server/stability/DmaBufProcUsageInfo;->getOomadj()I

    move-result v14

    invoke-virtual {v5, v14}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v14, ")"

    invoke-virtual {v5, v14}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    .line 483
    invoke-static {v2, v5}, Landroid/util/Slog;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 485
    const/4 v5, 0x0

    goto :goto_1

    .line 487
    :cond_3
    move-object v8, v12

    .line 488
    invoke-virtual {v8}, Lcom/miui/server/stability/DmaBufProcUsageInfo;->getPid()I

    move-result v5

    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    invoke-virtual {v10, v5}, Ljava/util/HashSet;->add(Ljava/lang/Object;)Z

    .line 489
    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v11, "Most used process name="

    invoke-virtual {v5, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, v13}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v11, " pid="

    invoke-virtual {v5, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    .line 490
    invoke-virtual {v12}, Lcom/miui/server/stability/DmaBufProcUsageInfo;->getPid()I

    move-result v11

    invoke-virtual {v5, v11}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, v15}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v12}, Lcom/miui/server/stability/DmaBufProcUsageInfo;->getOomadj()I

    move-result v11

    invoke-virtual {v5, v11}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v11, " rss="

    invoke-virtual {v5, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    .line 491
    invoke-virtual {v12}, Lcom/miui/server/stability/DmaBufProcUsageInfo;->getRss()J

    move-result-wide v14

    invoke-virtual {v5, v14, v15}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    .line 489
    invoke-static {v2, v5}, Landroid/util/Slog;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 492
    nop

    .line 495
    .end local v12    # "procInfo":Lcom/miui/server/stability/DmaBufProcUsageInfo;
    .end local v13    # "procName":Ljava/lang/String;
    :cond_4
    if-eqz v8, :cond_9

    .line 496
    invoke-static {}, Landroid/os/Process;->myPid()I

    move-result v5

    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    invoke-virtual {v10, v5}, Ljava/util/HashSet;->add(Ljava/lang/Object;)Z

    .line 497
    iget-object v5, v0, Lcom/miui/server/stability/ScoutDisplayMemoryManager$3;->val$reason:Ljava/lang/String;

    invoke-static {v1, v5, v10}, Lcom/miui/server/stability/ScoutMemoryUtils;->captureIonLeakLog(Ljava/lang/String;Ljava/lang/String;Ljava/util/HashSet;)Ljava/lang/String;

    move-result-object v5

    .line 498
    .local v5, "zipPath":Ljava/lang/String;
    invoke-static {}, Lcom/miui/server/stability/ScoutDisplayMemoryManager;->-$$Nest$sfgetDMABUF_LEAK_THRESHOLD()I

    move-result v11

    int-to-long v11, v11

    mul-long/2addr v11, v6

    long-to-double v11, v11

    const-wide v13, 0x3fe3333333333333L    # 0.6

    mul-double/2addr v11, v13

    double-to-long v11, v11

    .line 499
    .local v11, "thresholdSize":J
    new-instance v13, Lcom/miui/server/stability/ScoutDisplayMemoryManager$DiaplayMemoryErrorInfo;

    iget-wide v14, v0, Lcom/miui/server/stability/ScoutDisplayMemoryManager$3;->val$totalSizeKB:J

    move-object/from16 v16, v13

    move-object/from16 v17, v8

    move-wide/from16 v18, v11

    move-wide/from16 v20, v14

    invoke-direct/range {v16 .. v21}, Lcom/miui/server/stability/ScoutDisplayMemoryManager$DiaplayMemoryErrorInfo;-><init>(Lcom/miui/server/stability/DmaBufProcUsageInfo;JJ)V

    .line 500
    .local v13, "errorInfo":Lcom/miui/server/stability/ScoutDisplayMemoryManager$DiaplayMemoryErrorInfo;
    invoke-virtual {v8}, Lcom/miui/server/stability/DmaBufProcUsageInfo;->getRss()J

    move-result-wide v14

    cmp-long v14, v14, v11

    if-lez v14, :cond_6

    .line 501
    invoke-static {v13}, Lcom/miui/server/stability/ScoutDisplayMemoryManager;->preserveCrimeSceneIfNeed(Lcom/miui/server/stability/ScoutDisplayMemoryManager$DiaplayMemoryErrorInfo;)Z

    move-result v14

    if-nez v14, :cond_6

    .line 502
    iget-object v2, v0, Lcom/miui/server/stability/ScoutDisplayMemoryManager$3;->this$0:Lcom/miui/server/stability/ScoutDisplayMemoryManager;

    invoke-virtual {v2}, Lcom/miui/server/stability/ScoutDisplayMemoryManager;->isEnableResumeFeature()Z

    move-result v2

    if-eqz v2, :cond_5

    .line 503
    iget-object v2, v0, Lcom/miui/server/stability/ScoutDisplayMemoryManager$3;->this$0:Lcom/miui/server/stability/ScoutDisplayMemoryManager;

    invoke-static {v2, v13}, Lcom/miui/server/stability/ScoutDisplayMemoryManager;->-$$Nest$mresumeMemLeak(Lcom/miui/server/stability/ScoutDisplayMemoryManager;Lcom/miui/server/stability/ScoutDisplayMemoryManager$DiaplayMemoryErrorInfo;)V

    goto :goto_2

    .line 505
    :cond_5
    iget-object v2, v0, Lcom/miui/server/stability/ScoutDisplayMemoryManager$3;->this$0:Lcom/miui/server/stability/ScoutDisplayMemoryManager;

    invoke-virtual {v2, v13}, Lcom/miui/server/stability/ScoutDisplayMemoryManager;->reportDisplayMemoryLeakEvent(Lcom/miui/server/stability/ScoutDisplayMemoryManager$DiaplayMemoryErrorInfo;)V

    .line 507
    :goto_2
    iget-object v2, v0, Lcom/miui/server/stability/ScoutDisplayMemoryManager$3;->this$0:Lcom/miui/server/stability/ScoutDisplayMemoryManager;

    invoke-virtual {v2, v13, v5}, Lcom/miui/server/stability/ScoutDisplayMemoryManager;->handleReportMqs(Lcom/miui/server/stability/ScoutDisplayMemoryManager$DiaplayMemoryErrorInfo;Ljava/lang/String;)V

    move-object/from16 v16, v1

    move-object/from16 v17, v3

    move-object/from16 v18, v4

    goto :goto_3

    .line 508
    :cond_6
    invoke-static {}, Lmiui/mqsas/scout/ScoutUtils;->isLibraryTest()Z

    move-result v14

    if-eqz v14, :cond_7

    if-eqz v9, :cond_7

    .line 509
    invoke-virtual {v9}, Lcom/miui/server/stability/DmaBufProcUsageInfo;->getRss()J

    move-result-wide v14

    move-object/from16 v16, v1

    .end local v1    # "dambufLog":Ljava/lang/String;
    .local v16, "dambufLog":Ljava/lang/String;
    invoke-static {}, Lcom/miui/server/stability/ScoutDisplayMemoryManager;->-$$Nest$sfgetDMABUF_LEAK_SF_THRESHOLD()I

    move-result v1

    move-object/from16 v17, v3

    move-object/from16 v18, v4

    .end local v3    # "dmabufInfo":Lcom/miui/server/stability/DmaBufUsageInfo;
    .end local v4    # "infoList":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/miui/server/stability/DmaBufProcUsageInfo;>;"
    .local v17, "dmabufInfo":Lcom/miui/server/stability/DmaBufUsageInfo;
    .local v18, "infoList":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/miui/server/stability/DmaBufProcUsageInfo;>;"
    int-to-long v3, v1

    mul-long/2addr v3, v6

    cmp-long v1, v14, v3

    if-lez v1, :cond_8

    iget-wide v3, v0, Lcom/miui/server/stability/ScoutDisplayMemoryManager$3;->val$totalSizeKB:J

    .line 510
    const-string/jumbo v1, "surfaceflinger"

    const/4 v6, -0x1

    const-string v7, "dmabuf"

    invoke-static {v1, v6, v3, v4, v7}, Lcom/miui/server/stability/ScoutDisplayMemoryManager;->-$$Nest$smdoPreserveCrimeSceneIfNeed(Ljava/lang/String;IJLjava/lang/String;)Z

    move-result v1

    if-nez v1, :cond_8

    .line 513
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v3, "surfaceflinger consumed "

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-wide v3, v0, Lcom/miui/server/stability/ScoutDisplayMemoryManager$3;->val$totalSizeKB:J

    invoke-virtual {v1, v3, v4}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v3, "KB dmabuf memory totally, killing systemui and miui home"

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v2, v1}, Landroid/util/Slog;->w(Ljava/lang/String;Ljava/lang/String;)I

    .line 515
    invoke-static {}, Landroid/view/SurfaceControlStub;->getInstance()Landroid/view/SurfaceControlStub;

    move-result-object v1

    check-cast v1, Landroid/view/SurfaceControlImpl;

    .line 516
    .local v1, "scImpl":Landroid/view/SurfaceControlImpl;
    const/4 v2, 0x0

    invoke-virtual {v1, v2}, Landroid/view/SurfaceControlImpl;->startAsyncDumpIfNeed(Z)V

    .line 517
    invoke-virtual {v1}, Landroid/view/SurfaceControlImpl;->applyGenialRenovation()V

    .line 520
    const/4 v2, 0x1

    invoke-virtual {v1, v2}, Landroid/view/SurfaceControlImpl;->increaseLeakLevel(Z)Ljava/lang/String;

    goto :goto_3

    .line 508
    .end local v16    # "dambufLog":Ljava/lang/String;
    .end local v17    # "dmabufInfo":Lcom/miui/server/stability/DmaBufUsageInfo;
    .end local v18    # "infoList":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/miui/server/stability/DmaBufProcUsageInfo;>;"
    .local v1, "dambufLog":Ljava/lang/String;
    .restart local v3    # "dmabufInfo":Lcom/miui/server/stability/DmaBufUsageInfo;
    .restart local v4    # "infoList":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/miui/server/stability/DmaBufProcUsageInfo;>;"
    :cond_7
    move-object/from16 v16, v1

    move-object/from16 v17, v3

    move-object/from16 v18, v4

    .line 522
    .end local v1    # "dambufLog":Ljava/lang/String;
    .end local v3    # "dmabufInfo":Lcom/miui/server/stability/DmaBufUsageInfo;
    .end local v4    # "infoList":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/miui/server/stability/DmaBufProcUsageInfo;>;"
    .restart local v16    # "dambufLog":Ljava/lang/String;
    .restart local v17    # "dmabufInfo":Lcom/miui/server/stability/DmaBufUsageInfo;
    .restart local v18    # "infoList":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/miui/server/stability/DmaBufProcUsageInfo;>;"
    :cond_8
    :goto_3
    iget-object v1, v0, Lcom/miui/server/stability/ScoutDisplayMemoryManager$3;->this$0:Lcom/miui/server/stability/ScoutDisplayMemoryManager;

    invoke-virtual {v1}, Lcom/miui/server/stability/ScoutDisplayMemoryManager;->updateLastReportTime()V

    goto :goto_4

    .line 495
    .end local v5    # "zipPath":Ljava/lang/String;
    .end local v11    # "thresholdSize":J
    .end local v13    # "errorInfo":Lcom/miui/server/stability/ScoutDisplayMemoryManager$DiaplayMemoryErrorInfo;
    .end local v16    # "dambufLog":Ljava/lang/String;
    .end local v17    # "dmabufInfo":Lcom/miui/server/stability/DmaBufUsageInfo;
    .end local v18    # "infoList":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/miui/server/stability/DmaBufProcUsageInfo;>;"
    .restart local v1    # "dambufLog":Ljava/lang/String;
    .restart local v3    # "dmabufInfo":Lcom/miui/server/stability/DmaBufUsageInfo;
    .restart local v4    # "infoList":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/miui/server/stability/DmaBufProcUsageInfo;>;"
    :cond_9
    move-object/from16 v16, v1

    move-object/from16 v17, v3

    move-object/from16 v18, v4

    .end local v1    # "dambufLog":Ljava/lang/String;
    .end local v3    # "dmabufInfo":Lcom/miui/server/stability/DmaBufUsageInfo;
    .end local v4    # "infoList":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/miui/server/stability/DmaBufProcUsageInfo;>;"
    .restart local v16    # "dambufLog":Ljava/lang/String;
    .restart local v17    # "dmabufInfo":Lcom/miui/server/stability/DmaBufUsageInfo;
    .restart local v18    # "infoList":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/miui/server/stability/DmaBufProcUsageInfo;>;"
    goto :goto_4

    .line 474
    .end local v10    # "fdPids":Ljava/util/HashSet;, "Ljava/util/HashSet<Ljava/lang/Integer;>;"
    .end local v16    # "dambufLog":Ljava/lang/String;
    .end local v17    # "dmabufInfo":Lcom/miui/server/stability/DmaBufUsageInfo;
    .end local v18    # "infoList":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/miui/server/stability/DmaBufProcUsageInfo;>;"
    .restart local v1    # "dambufLog":Ljava/lang/String;
    .restart local v3    # "dmabufInfo":Lcom/miui/server/stability/DmaBufUsageInfo;
    .restart local v4    # "infoList":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/miui/server/stability/DmaBufProcUsageInfo;>;"
    :cond_a
    move-object/from16 v16, v1

    move-object/from16 v17, v3

    move-object/from16 v18, v4

    .line 525
    .end local v1    # "dambufLog":Ljava/lang/String;
    .end local v3    # "dmabufInfo":Lcom/miui/server/stability/DmaBufUsageInfo;
    .end local v4    # "infoList":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/miui/server/stability/DmaBufProcUsageInfo;>;"
    .restart local v16    # "dambufLog":Ljava/lang/String;
    .restart local v17    # "dmabufInfo":Lcom/miui/server/stability/DmaBufUsageInfo;
    .restart local v18    # "infoList":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/miui/server/stability/DmaBufProcUsageInfo;>;"
    :goto_4
    iget-object v1, v0, Lcom/miui/server/stability/ScoutDisplayMemoryManager$3;->this$0:Lcom/miui/server/stability/ScoutDisplayMemoryManager;

    invoke-static {v1}, Lcom/miui/server/stability/ScoutDisplayMemoryManager;->-$$Nest$fgetisBusy(Lcom/miui/server/stability/ScoutDisplayMemoryManager;)Ljava/util/concurrent/atomic/AtomicBoolean;

    move-result-object v1

    const/4 v2, 0x0

    invoke-virtual {v1, v2}, Ljava/util/concurrent/atomic/AtomicBoolean;->set(Z)V

    .line 526
    return-void
.end method
