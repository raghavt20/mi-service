.class public final Lcom/miui/server/stability/ScoutDisplayMemoryManager$CrimeScenePreservedDialog;
.super Ljava/lang/Object;
.source "ScoutDisplayMemoryManager.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/miui/server/stability/ScoutDisplayMemoryManager;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "CrimeScenePreservedDialog"
.end annotation


# static fields
.field private static sDialog:Lcom/android/server/am/BaseErrorDialog;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .line 746
    const/4 v0, 0x0

    sput-object v0, Lcom/miui/server/stability/ScoutDisplayMemoryManager$CrimeScenePreservedDialog;->sDialog:Lcom/android/server/am/BaseErrorDialog;

    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .line 744
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method static synthetic lambda$show$0(Ljava/lang/String;Ljava/lang/String;)V
    .locals 5
    .param p0, "title"    # Ljava/lang/String;
    .param p1, "msg"    # Ljava/lang/String;

    .line 752
    sget-object v0, Lcom/miui/server/stability/ScoutDisplayMemoryManager$CrimeScenePreservedDialog;->sDialog:Lcom/android/server/am/BaseErrorDialog;

    if-eqz v0, :cond_0

    .line 753
    invoke-virtual {v0}, Lcom/android/server/am/BaseErrorDialog;->dismiss()V

    .line 754
    const/4 v0, 0x0

    sput-object v0, Lcom/miui/server/stability/ScoutDisplayMemoryManager$CrimeScenePreservedDialog;->sDialog:Lcom/android/server/am/BaseErrorDialog;

    .line 756
    :cond_0
    invoke-static {}, Landroid/app/ActivityThread;->currentActivityThread()Landroid/app/ActivityThread;

    move-result-object v0

    invoke-virtual {v0}, Landroid/app/ActivityThread;->getSystemUiContext()Landroid/app/ContextImpl;

    move-result-object v0

    .line 757
    .local v0, "uiContext":Landroid/app/ContextImpl;
    new-instance v1, Lcom/miui/server/stability/ScoutDisplayMemoryManager$CrimeScenePreservedDialog$1;

    invoke-direct {v1, v0}, Lcom/miui/server/stability/ScoutDisplayMemoryManager$CrimeScenePreservedDialog$1;-><init>(Landroid/content/Context;)V

    .line 763
    .local v1, "dialog":Lcom/android/server/am/BaseErrorDialog;
    invoke-virtual {v1, p0}, Lcom/android/server/am/BaseErrorDialog;->setTitle(Ljava/lang/CharSequence;)V

    .line 764
    invoke-virtual {v1, p1}, Lcom/android/server/am/BaseErrorDialog;->setMessage(Ljava/lang/CharSequence;)V

    .line 765
    const/4 v2, 0x0

    invoke-virtual {v1, v2}, Lcom/android/server/am/BaseErrorDialog;->setCancelable(Z)V

    .line 766
    invoke-virtual {v1, v2}, Lcom/android/server/am/BaseErrorDialog;->setCanceledOnTouchOutside(Z)V

    .line 767
    invoke-virtual {v1}, Lcom/android/server/am/BaseErrorDialog;->getWindow()Landroid/view/Window;

    move-result-object v2

    .line 768
    .local v2, "window":Landroid/view/Window;
    if-nez v2, :cond_1

    .line 769
    const-string v3, "MIUIScout Memory"

    const-string v4, "Cannot show dialog: no window"

    invoke-static {v3, v4}, Landroid/util/Slog;->w(Ljava/lang/String;Ljava/lang/String;)I

    .line 770
    return-void

    .line 772
    :cond_1
    invoke-virtual {v2}, Landroid/view/Window;->getAttributes()Landroid/view/WindowManager$LayoutParams;

    move-result-object v3

    .line 773
    .local v3, "attrs":Landroid/view/WindowManager$LayoutParams;
    invoke-virtual {v3, p0}, Landroid/view/WindowManager$LayoutParams;->setTitle(Ljava/lang/CharSequence;)V

    .line 774
    invoke-virtual {v1}, Lcom/android/server/am/BaseErrorDialog;->show()V

    .line 775
    sput-object v1, Lcom/miui/server/stability/ScoutDisplayMemoryManager$CrimeScenePreservedDialog;->sDialog:Lcom/android/server/am/BaseErrorDialog;

    .line 776
    return-void
.end method

.method public static show(Ljava/lang/String;Ljava/lang/String;)V
    .locals 2
    .param p0, "title"    # Ljava/lang/String;
    .param p1, "msg"    # Ljava/lang/String;

    .line 749
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v0, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ": "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    const-string v1, "MIUIScout Memory"

    invoke-static {v1, v0}, Landroid/util/Slog;->w(Ljava/lang/String;Ljava/lang/String;)I

    .line 750
    new-instance v0, Lcom/miui/server/stability/ScoutDisplayMemoryManager$CrimeScenePreservedDialog$$ExternalSyntheticLambda0;

    invoke-direct {v0, p0, p1}, Lcom/miui/server/stability/ScoutDisplayMemoryManager$CrimeScenePreservedDialog$$ExternalSyntheticLambda0;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    .line 777
    .local v0, "task":Ljava/lang/Runnable;
    invoke-static {}, Lcom/android/server/UiThread;->getHandler()Landroid/os/Handler;

    move-result-object v1

    invoke-virtual {v1, v0}, Landroid/os/Handler;->postAtFrontOfQueue(Ljava/lang/Runnable;)Z

    .line 778
    return-void
.end method
