public class com.miui.server.stability.GpuMemoryUsageInfo {
	 /* .source "GpuMemoryUsageInfo.java" */
	 /* # instance fields */
	 private Long nativeTotalSize;
	 private Long runtimeTotalSize;
	 private Long totalSize;
	 private java.util.ArrayList usageList;
	 /* .annotation system Ldalvik/annotation/Signature; */
	 /* value = { */
	 /* "Ljava/util/ArrayList<", */
	 /* "Lcom/miui/server/stability/GpuMemoryProcUsageInfo;", */
	 /* ">;" */
	 /* } */
} // .end annotation
} // .end field
/* # direct methods */
public com.miui.server.stability.GpuMemoryUsageInfo ( ) {
/* .locals 1 */
/* .line 14 */
/* invoke-direct {p0}, Ljava/lang/Object;-><init>()V */
/* .line 15 */
/* new-instance v0, Ljava/util/ArrayList; */
/* invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V */
this.usageList = v0;
/* .line 16 */
return;
} // .end method
/* # virtual methods */
public void add ( com.miui.server.stability.GpuMemoryProcUsageInfo p0 ) {
/* .locals 1 */
/* .param p1, "procInfo" # Lcom/miui/server/stability/GpuMemoryProcUsageInfo; */
/* .line 19 */
v0 = this.usageList;
(( java.util.ArrayList ) v0 ).add ( p1 ); // invoke-virtual {v0, p1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z
/* .line 20 */
return;
} // .end method
public java.util.ArrayList getList ( ) {
/* .locals 1 */
/* .annotation system Ldalvik/annotation/Signature; */
/* value = { */
/* "()", */
/* "Ljava/util/ArrayList<", */
/* "Lcom/miui/server/stability/GpuMemoryProcUsageInfo;", */
/* ">;" */
/* } */
} // .end annotation
/* .line 23 */
v0 = this.usageList;
} // .end method
public Long getNativeTotalSize ( ) {
/* .locals 2 */
/* .line 47 */
/* iget-wide v0, p0, Lcom/miui/server/stability/GpuMemoryUsageInfo;->nativeTotalSize:J */
/* return-wide v0 */
} // .end method
public Long getRuntimeTotalSize ( ) {
/* .locals 2 */
/* .line 39 */
/* iget-wide v0, p0, Lcom/miui/server/stability/GpuMemoryUsageInfo;->runtimeTotalSize:J */
/* return-wide v0 */
} // .end method
public Long getTotalSize ( ) {
/* .locals 2 */
/* .line 31 */
/* iget-wide v0, p0, Lcom/miui/server/stability/GpuMemoryUsageInfo;->totalSize:J */
/* return-wide v0 */
} // .end method
public void setNativeTotalSize ( Long p0 ) {
/* .locals 0 */
/* .param p1, "nativeTotalSize" # J */
/* .line 43 */
/* iput-wide p1, p0, Lcom/miui/server/stability/GpuMemoryUsageInfo;->nativeTotalSize:J */
/* .line 44 */
return;
} // .end method
public void setRuntimeTotalSize ( Long p0 ) {
/* .locals 0 */
/* .param p1, "runtimeTotalSize" # J */
/* .line 35 */
/* iput-wide p1, p0, Lcom/miui/server/stability/GpuMemoryUsageInfo;->runtimeTotalSize:J */
/* .line 36 */
return;
} // .end method
public void setTotalSize ( Long p0 ) {
/* .locals 0 */
/* .param p1, "totalSize" # J */
/* .line 27 */
/* iput-wide p1, p0, Lcom/miui/server/stability/GpuMemoryUsageInfo;->totalSize:J */
/* .line 28 */
return;
} // .end method
public java.lang.String toString ( Integer p0 ) {
/* .locals 5 */
/* .param p1, "gpuType" # I */
/* .line 51 */
/* new-instance v0, Ljava/lang/StringBuilder; */
/* invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V */
/* .line 52 */
/* .local v0, "sb":Ljava/lang/StringBuilder; */
int v1 = 1; // const/4 v1, 0x1
/* if-ne p1, v1, :cond_0 */
/* .line 53 */
final String v1 = "KgslMemory"; // const-string v1, "KgslMemory"
(( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
/* .line 54 */
} // :cond_0
int v1 = 2; // const/4 v1, 0x2
/* if-ne p1, v1, :cond_2 */
/* .line 55 */
final String v1 = "MaliMemory"; // const-string v1, "MaliMemory"
(( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
/* .line 59 */
} // :goto_0
/* new-instance v1, Ljava/lang/StringBuilder; */
/* invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V */
final String v2 = " info total: "; // const-string v2, " info total: "
(( java.lang.StringBuilder ) v1 ).append ( v2 ); // invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
/* iget-wide v2, p0, Lcom/miui/server/stability/GpuMemoryUsageInfo;->totalSize:J */
(( java.lang.StringBuilder ) v1 ).append ( v2, v3 ); // invoke-virtual {v1, v2, v3}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;
final String v2 = "kB RuntimeTotalSize="; // const-string v2, "kB RuntimeTotalSize="
(( java.lang.StringBuilder ) v1 ).append ( v2 ); // invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
/* iget-wide v2, p0, Lcom/miui/server/stability/GpuMemoryUsageInfo;->runtimeTotalSize:J */
(( java.lang.StringBuilder ) v1 ).append ( v2, v3 ); // invoke-virtual {v1, v2, v3}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;
final String v2 = "kB NativeTotalSize="; // const-string v2, "kB NativeTotalSize="
(( java.lang.StringBuilder ) v1 ).append ( v2 ); // invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
/* iget-wide v2, p0, Lcom/miui/server/stability/GpuMemoryUsageInfo;->nativeTotalSize:J */
(( java.lang.StringBuilder ) v1 ).append ( v2, v3 ); // invoke-virtual {v1, v2, v3}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;
final String v2 = "kB\n"; // const-string v2, "kB\n"
(( java.lang.StringBuilder ) v1 ).append ( v2 ); // invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v1 ).toString ( ); // invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
(( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
/* .line 61 */
v1 = this.usageList;
(( java.util.ArrayList ) v1 ).iterator ( ); // invoke-virtual {v1}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;
v2 = } // :goto_1
if ( v2 != null) { // if-eqz v2, :cond_1
/* check-cast v2, Lcom/miui/server/stability/GpuMemoryProcUsageInfo; */
/* .line 62 */
/* .local v2, "info":Lcom/miui/server/stability/GpuMemoryProcUsageInfo; */
/* new-instance v3, Ljava/lang/StringBuilder; */
/* invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V */
(( com.miui.server.stability.GpuMemoryProcUsageInfo ) v2 ).toString ( p1 ); // invoke-virtual {v2, p1}, Lcom/miui/server/stability/GpuMemoryProcUsageInfo;->toString(I)Ljava/lang/String;
(( java.lang.StringBuilder ) v3 ).append ( v4 ); // invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
final String v4 = "\n"; // const-string v4, "\n"
(( java.lang.StringBuilder ) v3 ).append ( v4 ); // invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v3 ).toString ( ); // invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
(( java.lang.StringBuilder ) v0 ).append ( v3 ); // invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
/* .line 63 */
} // .end local v2 # "info":Lcom/miui/server/stability/GpuMemoryProcUsageInfo;
/* .line 64 */
} // :cond_1
(( java.lang.StringBuilder ) v0 ).toString ( ); // invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
/* .line 57 */
} // :cond_2
final String v1 = "Error Info"; // const-string v1, "Error Info"
} // .end method
