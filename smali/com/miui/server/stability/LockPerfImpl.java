public class com.miui.server.stability.LockPerfImpl implements com.android.server.LockPerfStub {
	 /* .source "LockPerfImpl.java" */
	 /* # interfaces */
	 /* # annotations */
	 /* .annotation system Ldalvik/annotation/MemberClasses; */
	 /* value = { */
	 /* Lcom/miui/server/stability/LockPerfImpl$ThreadLocalData;, */
	 /* Lcom/miui/server/stability/LockPerfImpl$PerfStack;, */
	 /* Lcom/miui/server/stability/LockPerfImpl$PerfInfo;, */
	 /* Lcom/miui/server/stability/LockPerfImpl$ThreadSafeRingBuffer;, */
	 /* Lcom/miui/server/stability/LockPerfImpl$SlowLockedMethodEvent; */
	 /* } */
} // .end annotation
/* # static fields */
private static final Boolean DEBUG;
private static final Integer DEFAULT_LOCK_PERF_THRESHOLD_SINGLE;
private static final Integer DEFAULT_RING_BUFFER_SIZE;
private static final java.lang.String LOCK_PERF_SELF_DEBUG_PROP;
private static final java.lang.String LOCK_PERF_THRESHOLD_PROP;
private static final Integer LOCK_PERF_THRESHOLD_SINGLE;
private static final Integer MEANINGFUL_TRACE_START;
private static final java.lang.String TAG;
private static final com.miui.server.stability.LockPerfImpl$ThreadSafeRingBuffer sEventRingBuffer;
/* .annotation system Ldalvik/annotation/Signature; */
/* value = { */
/* "Lcom/miui/server/stability/LockPerfImpl$ThreadSafeRingBuffer<", */
/* "Lcom/miui/server/stability/LockPerfImpl$SlowLockedMethodEvent;", */
/* ">;" */
/* } */
} // .end annotation
} // .end field
private static final java.lang.ThreadLocal sThreadLocalData;
/* .annotation system Ldalvik/annotation/Signature; */
/* value = { */
/* "Ljava/lang/ThreadLocal<", */
/* "Lcom/miui/server/stability/LockPerfImpl$ThreadLocalData;", */
/* ">;" */
/* } */
} // .end annotation
} // .end field
/* # direct methods */
public static com.miui.server.stability.LockPerfImpl$ThreadLocalData $r8$lambda$ClCO5txLu8hRiftiu7srcjS1yXQ ( ) { //synthethic
/* .locals 2 */
/* new-instance v0, Lcom/miui/server/stability/LockPerfImpl$ThreadLocalData; */
int v1 = 0; // const/4 v1, 0x0
/* invoke-direct {v0, v1}, Lcom/miui/server/stability/LockPerfImpl$ThreadLocalData;-><init>(Lcom/miui/server/stability/LockPerfImpl$ThreadLocalData-IA;)V */
} // .end method
static java.lang.String -$$Nest$sfgetTAG ( ) { //bridge//synthethic
/* .locals 1 */
v0 = com.miui.server.stability.LockPerfImpl.TAG;
} // .end method
static com.miui.server.stability.LockPerfImpl$ThreadSafeRingBuffer -$$Nest$sfgetsEventRingBuffer ( ) { //bridge//synthethic
/* .locals 1 */
v0 = com.miui.server.stability.LockPerfImpl.sEventRingBuffer;
} // .end method
static java.lang.String -$$Nest$smparseFullMethod ( java.lang.String p0 ) { //bridge//synthethic
/* .locals 0 */
com.miui.server.stability.LockPerfImpl .parseFullMethod ( p0 );
} // .end method
static com.miui.server.stability.LockPerfImpl ( ) {
/* .locals 3 */
/* .line 22 */
/* const-class v0, Lcom/miui/server/stability/LockPerfImpl; */
(( java.lang.Class ) v0 ).getSimpleName ( ); // invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;
/* .line 32 */
/* nop */
/* .line 33 */
final String v0 = "persist.debug.lockperf.threshold"; // const-string v0, "persist.debug.lockperf.threshold"
/* const/16 v1, 0x64 */
v0 = android.os.SystemProperties .getInt ( v0,v1 );
/* .line 34 */
final String v0 = "persist.debug.lockperf.self"; // const-string v0, "persist.debug.lockperf.self"
int v2 = 1; // const/4 v2, 0x1
v0 = android.os.SystemProperties .getBoolean ( v0,v2 );
com.miui.server.stability.LockPerfImpl.DEBUG = (v0!= 0);
/* .line 36 */
/* new-instance v0, Lcom/miui/server/stability/LockPerfImpl$$ExternalSyntheticLambda0; */
/* invoke-direct {v0}, Lcom/miui/server/stability/LockPerfImpl$$ExternalSyntheticLambda0;-><init>()V */
java.lang.ThreadLocal .withInitial ( v0 );
/* .line 38 */
/* new-instance v0, Lcom/miui/server/stability/LockPerfImpl$ThreadSafeRingBuffer; */
/* const-class v2, Lcom/miui/server/stability/LockPerfImpl$SlowLockedMethodEvent; */
/* invoke-direct {v0, v2, v1}, Lcom/miui/server/stability/LockPerfImpl$ThreadSafeRingBuffer;-><init>(Ljava/lang/Class;I)V */
return;
} // .end method
public com.miui.server.stability.LockPerfImpl ( ) {
/* .locals 0 */
/* .line 20 */
/* invoke-direct {p0}, Ljava/lang/Object;-><init>()V */
return;
} // .end method
private static java.lang.String parseFullMethod ( java.lang.String p0 ) {
/* .locals 6 */
/* .param p0, "fullMethod" # Ljava/lang/String; */
/* .line 144 */
int v0 = 3; // const/4 v0, 0x3
/* new-array v0, v0, [Ljava/lang/String; */
/* .line 145 */
/* .local v0, "rst":[Ljava/lang/String; */
/* const/16 v1, 0x28 */
v1 = (( java.lang.String ) p0 ).indexOf ( v1 ); // invoke-virtual {p0, v1}, Ljava/lang/String;->indexOf(I)I
/* .line 146 */
/* .local v1, "pos":I */
int v2 = 0; // const/4 v2, 0x0
(( java.lang.String ) p0 ).substring ( v2, v1 ); // invoke-virtual {p0, v2, v1}, Ljava/lang/String;->substring(II)Ljava/lang/String;
/* .line 147 */
/* .local v3, "classAndName":Ljava/lang/String; */
int v4 = 2; // const/4 v4, 0x2
(( java.lang.String ) p0 ).substring ( v1 ); // invoke-virtual {p0, v1}, Ljava/lang/String;->substring(I)Ljava/lang/String;
/* aput-object v5, v0, v4 */
/* .line 148 */
/* const/16 v4, 0x2e */
v1 = (( java.lang.String ) v3 ).lastIndexOf ( v4 ); // invoke-virtual {v3, v4}, Ljava/lang/String;->lastIndexOf(I)I
/* .line 149 */
(( java.lang.String ) v3 ).substring ( v2, v1 ); // invoke-virtual {v3, v2, v1}, Ljava/lang/String;->substring(II)Ljava/lang/String;
/* aput-object v4, v0, v2 */
/* .line 150 */
/* add-int/lit8 v2, v1, 0x1 */
(( java.lang.String ) v3 ).substring ( v2 ); // invoke-virtual {v3, v2}, Ljava/lang/String;->substring(I)Ljava/lang/String;
int v4 = 1; // const/4 v4, 0x1
/* aput-object v2, v0, v4 */
/* .line 151 */
} // .end method
private static java.lang.StackTraceElement trimStackTrace ( java.lang.StackTraceElement[] p0 ) {
/* .locals 3 */
/* .param p0, "rawTrace" # [Ljava/lang/StackTraceElement; */
/* .line 156 */
/* new-instance v0, Ljava/util/ArrayList; */
/* invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V */
/* .line 157 */
/* .local v0, "elements":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/StackTraceElement;>;" */
int v1 = 4; // const/4 v1, 0x4
/* .local v1, "i":I */
} // :goto_0
/* array-length v2, p0 */
/* if-ge v1, v2, :cond_0 */
/* .line 158 */
/* aget-object v2, p0, v1 */
(( java.util.ArrayList ) v0 ).add ( v2 ); // invoke-virtual {v0, v2}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z
/* .line 157 */
/* add-int/lit8 v1, v1, 0x1 */
/* .line 160 */
} // .end local v1 # "i":I
} // :cond_0
int v1 = 0; // const/4 v1, 0x0
/* new-array v1, v1, [Ljava/lang/StackTraceElement; */
(( java.util.ArrayList ) v0 ).toArray ( v1 ); // invoke-virtual {v0, v1}, Ljava/util/ArrayList;->toArray([Ljava/lang/Object;)[Ljava/lang/Object;
/* check-cast v1, [Ljava/lang/StackTraceElement; */
} // .end method
/* # virtual methods */
public void dumpRecentEvents ( java.io.File p0, Boolean p1, Integer p2, Integer p3 ) {
/* .locals 16 */
/* .param p1, "tracesFile" # Ljava/io/File; */
/* .param p2, "append" # Z */
/* .param p3, "recentSeconds" # I */
/* .param p4, "maxEventNum" # I */
/* .line 106 */
try { // :try_start_0
/* new-instance v0, Ljava/io/FileWriter; */
/* :try_end_0 */
/* .catch Ljava/io/IOException; {:try_start_0 ..:try_end_0} :catch_2 */
/* move-object/from16 v1, p1 */
/* move/from16 v2, p2 */
try { // :try_start_1
/* invoke-direct {v0, v1, v2}, Ljava/io/FileWriter;-><init>(Ljava/io/File;Z)V */
/* :try_end_1 */
/* .catch Ljava/io/IOException; {:try_start_1 ..:try_end_1} :catch_1 */
/* move-object v3, v0 */
/* .line 107 */
/* .local v3, "fw":Ljava/io/FileWriter; */
try { // :try_start_2
v0 = com.miui.server.stability.LockPerfImpl.sEventRingBuffer;
(( com.miui.server.stability.LockPerfImpl$ThreadSafeRingBuffer ) v0 ).toArray ( ); // invoke-virtual {v0}, Lcom/miui/server/stability/LockPerfImpl$ThreadSafeRingBuffer;->toArray()[Ljava/lang/Object;
/* check-cast v0, [Lcom/miui/server/stability/LockPerfImpl$SlowLockedMethodEvent; */
/* .line 108 */
/* .local v0, "events":[Lcom/miui/server/stability/LockPerfImpl$SlowLockedMethodEvent; */
/* array-length v4, v0 */
/* if-nez v4, :cond_0 */
/* .line 109 */
final String v4 = "No events in buffer.Is the rom debuggable?"; // const-string v4, "No events in buffer.Is the rom debuggable?"
(( java.io.FileWriter ) v3 ).append ( v4 ); // invoke-virtual {v3, v4}, Ljava/io/FileWriter;->append(Ljava/lang/CharSequence;)Ljava/io/Writer;
/* :try_end_2 */
/* .catchall {:try_start_2 ..:try_end_2} :catchall_2 */
/* .line 138 */
try { // :try_start_3
(( java.io.FileWriter ) v3 ).close ( ); // invoke-virtual {v3}, Ljava/io/FileWriter;->close()V
/* :try_end_3 */
/* .catch Ljava/io/IOException; {:try_start_3 ..:try_end_3} :catch_1 */
/* .line 110 */
return;
/* .line 112 */
} // :cond_0
try { // :try_start_4
/* new-instance v4, Ljava/util/ArrayList; */
/* invoke-direct {v4}, Ljava/util/ArrayList;-><init>()V */
/* .line 113 */
/* .local v4, "eventsToDump":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/miui/server/stability/LockPerfImpl$SlowLockedMethodEvent;>;" */
java.time.LocalDateTime .now ( );
/* :try_end_4 */
/* .catchall {:try_start_4 ..:try_end_4} :catchall_2 */
/* move/from16 v6, p3 */
/* int-to-long v7, v6 */
try { // :try_start_5
(( java.time.LocalDateTime ) v5 ).minusSeconds ( v7, v8 ); // invoke-virtual {v5, v7, v8}, Ljava/time/LocalDateTime;->minusSeconds(J)Ljava/time/LocalDateTime;
/* .line 114 */
/* .local v5, "eventStartTime":Ljava/time/LocalDateTime; */
/* array-length v7, v0 */
/* add-int/lit8 v7, v7, -0x1 */
/* .local v7, "i":I */
} // :goto_0
/* if-ltz v7, :cond_3 */
/* .line 115 */
/* aget-object v8, v0, v7 */
com.miui.server.stability.LockPerfImpl$SlowLockedMethodEvent .-$$Nest$fgetdatetime ( v8 );
v8 = (( java.time.LocalDateTime ) v8 ).isBefore ( v5 ); // invoke-virtual {v8, v5}, Ljava/time/LocalDateTime;->isBefore(Ljava/time/chrono/ChronoLocalDateTime;)Z
/* if-nez v8, :cond_2 */
v8 = (( java.util.ArrayList ) v4 ).size ( ); // invoke-virtual {v4}, Ljava/util/ArrayList;->size()I
/* :try_end_5 */
/* .catchall {:try_start_5 ..:try_end_5} :catchall_1 */
/* move/from16 v9, p4 */
/* if-lt v8, v9, :cond_1 */
/* .line 116 */
/* .line 118 */
} // :cond_1
try { // :try_start_6
/* aget-object v8, v0, v7 */
(( java.util.ArrayList ) v4 ).add ( v8 ); // invoke-virtual {v4, v8}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z
/* .line 114 */
/* add-int/lit8 v7, v7, -0x1 */
/* .line 115 */
} // :cond_2
/* move/from16 v9, p4 */
/* .line 114 */
} // :cond_3
/* move/from16 v9, p4 */
/* .line 121 */
} // .end local v7 # "i":I
} // :goto_1
v7 = (( java.util.ArrayList ) v4 ).isEmpty ( ); // invoke-virtual {v4}, Ljava/util/ArrayList;->isEmpty()Z
if ( v7 != null) { // if-eqz v7, :cond_4
/* .line 122 */
final String v7 = "No events to dump in last "; // const-string v7, "No events to dump in last "
(( java.io.FileWriter ) v3 ).append ( v7 ); // invoke-virtual {v3, v7}, Ljava/io/FileWriter;->append(Ljava/lang/CharSequence;)Ljava/io/Writer;
/* invoke-static/range {p3 ..p3}, Ljava/lang/String;->valueOf(I)Ljava/lang/String; */
(( java.io.Writer ) v7 ).append ( v8 ); // invoke-virtual {v7, v8}, Ljava/io/Writer;->append(Ljava/lang/CharSequence;)Ljava/io/Writer;
final String v8 = " seconds."; // const-string v8, " seconds."
/* .line 123 */
(( java.io.Writer ) v7 ).append ( v8 ); // invoke-virtual {v7, v8}, Ljava/io/Writer;->append(Ljava/lang/CharSequence;)Ljava/io/Writer;
/* :try_end_6 */
/* .catchall {:try_start_6 ..:try_end_6} :catchall_0 */
/* .line 138 */
try { // :try_start_7
(( java.io.FileWriter ) v3 ).close ( ); // invoke-virtual {v3}, Ljava/io/FileWriter;->close()V
/* :try_end_7 */
/* .catch Ljava/io/IOException; {:try_start_7 ..:try_end_7} :catch_0 */
/* .line 124 */
return;
/* .line 127 */
} // :cond_4
try { // :try_start_8
final String v7 = "--- Slow locked method ---\n"; // const-string v7, "--- Slow locked method ---\n"
(( java.io.FileWriter ) v3 ).append ( v7 ); // invoke-virtual {v3, v7}, Ljava/io/FileWriter;->append(Ljava/lang/CharSequence;)Ljava/io/Writer;
/* .line 128 */
int v7 = 0; // const/4 v7, 0x0
/* .restart local v7 # "i":I */
} // :goto_2
v8 = (( java.util.ArrayList ) v4 ).size ( ); // invoke-virtual {v4}, Ljava/util/ArrayList;->size()I
/* if-ge v7, v8, :cond_6 */
/* .line 129 */
(( java.util.ArrayList ) v4 ).get ( v7 ); // invoke-virtual {v4, v7}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;
/* check-cast v8, Lcom/miui/server/stability/LockPerfImpl$SlowLockedMethodEvent; */
/* .line 130 */
/* .local v8, "event":Lcom/miui/server/stability/LockPerfImpl$SlowLockedMethodEvent; */
/* add-int/lit8 v10, v7, 0x1 */
java.lang.String .valueOf ( v10 );
(( java.io.FileWriter ) v3 ).append ( v10 ); // invoke-virtual {v3, v10}, Ljava/io/FileWriter;->append(Ljava/lang/CharSequence;)Ljava/io/Writer;
final String v11 = ") "; // const-string v11, ") "
(( java.io.Writer ) v10 ).append ( v11 ); // invoke-virtual {v10, v11}, Ljava/io/Writer;->append(Ljava/lang/CharSequence;)Ljava/io/Writer;
/* .line 131 */
com.miui.server.stability.LockPerfImpl$SlowLockedMethodEvent .-$$Nest$mformattedDatetime ( v8 );
(( java.io.FileWriter ) v3 ).append ( v10 ); // invoke-virtual {v3, v10}, Ljava/io/FileWriter;->append(Ljava/lang/CharSequence;)Ljava/io/Writer;
/* const/16 v11, 0x20 */
(( java.io.Writer ) v10 ).append ( v11 ); // invoke-virtual {v10, v11}, Ljava/io/Writer;->append(C)Ljava/io/Writer;
/* .line 132 */
/* const-string/jumbo v10, "tid=" */
(( java.io.FileWriter ) v3 ).append ( v10 ); // invoke-virtual {v3, v10}, Ljava/io/FileWriter;->append(Ljava/lang/CharSequence;)Ljava/io/Writer;
com.miui.server.stability.LockPerfImpl$SlowLockedMethodEvent .-$$Nest$fgettid ( v8 );
/* move-result-wide v12 */
java.lang.String .valueOf ( v12,v13 );
(( java.io.Writer ) v10 ).append ( v12 ); // invoke-virtual {v10, v12}, Ljava/io/Writer;->append(Ljava/lang/CharSequence;)Ljava/io/Writer;
(( java.io.Writer ) v10 ).append ( v11 ); // invoke-virtual {v10, v11}, Ljava/io/Writer;->append(C)Ljava/io/Writer;
/* .line 133 */
/* const-string/jumbo v10, "tname=" */
(( java.io.FileWriter ) v3 ).append ( v10 ); // invoke-virtual {v3, v10}, Ljava/io/FileWriter;->append(Ljava/lang/CharSequence;)Ljava/io/Writer;
com.miui.server.stability.LockPerfImpl$SlowLockedMethodEvent .-$$Nest$fgettname ( v8 );
(( java.io.Writer ) v10 ).append ( v11 ); // invoke-virtual {v10, v11}, Ljava/io/Writer;->append(Ljava/lang/CharSequence;)Ljava/io/Writer;
final String v11 = "\n"; // const-string v11, "\n"
(( java.io.Writer ) v10 ).append ( v11 ); // invoke-virtual {v10, v11}, Ljava/io/Writer;->append(Ljava/lang/CharSequence;)Ljava/io/Writer;
/* .line 134 */
com.miui.server.stability.LockPerfImpl$SlowLockedMethodEvent .-$$Nest$mgetStackTraceLines ( v8 );
/* array-length v11, v10 */
int v12 = 0; // const/4 v12, 0x0
} // :goto_3
/* if-ge v12, v11, :cond_5 */
/* aget-object v13, v10, v12 */
/* .line 135 */
/* .local v13, "line":Ljava/lang/String; */
final String v14 = "\t "; // const-string v14, "\t "
(( java.io.FileWriter ) v3 ).append ( v14 ); // invoke-virtual {v3, v14}, Ljava/io/FileWriter;->append(Ljava/lang/CharSequence;)Ljava/io/Writer;
(( java.io.Writer ) v14 ).append ( v13 ); // invoke-virtual {v14, v13}, Ljava/io/Writer;->append(Ljava/lang/CharSequence;)Ljava/io/Writer;
/* const/16 v15, 0xa */
(( java.io.Writer ) v14 ).append ( v15 ); // invoke-virtual {v14, v15}, Ljava/io/Writer;->append(C)Ljava/io/Writer;
/* :try_end_8 */
/* .catchall {:try_start_8 ..:try_end_8} :catchall_0 */
/* .line 134 */
/* nop */
} // .end local v13 # "line":Ljava/lang/String;
/* add-int/lit8 v12, v12, 0x1 */
/* .line 128 */
} // .end local v8 # "event":Lcom/miui/server/stability/LockPerfImpl$SlowLockedMethodEvent;
} // :cond_5
/* add-int/lit8 v7, v7, 0x1 */
/* .line 138 */
} // .end local v0 # "events":[Lcom/miui/server/stability/LockPerfImpl$SlowLockedMethodEvent;
} // .end local v4 # "eventsToDump":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/miui/server/stability/LockPerfImpl$SlowLockedMethodEvent;>;"
} // .end local v5 # "eventStartTime":Ljava/time/LocalDateTime;
} // .end local v7 # "i":I
} // :cond_6
try { // :try_start_9
(( java.io.FileWriter ) v3 ).close ( ); // invoke-virtual {v3}, Ljava/io/FileWriter;->close()V
/* :try_end_9 */
/* .catch Ljava/io/IOException; {:try_start_9 ..:try_end_9} :catch_0 */
/* .line 140 */
} // .end local v3 # "fw":Ljava/io/FileWriter;
/* .line 106 */
/* .restart local v3 # "fw":Ljava/io/FileWriter; */
/* :catchall_0 */
/* move-exception v0 */
/* :catchall_1 */
/* move-exception v0 */
/* :catchall_2 */
/* move-exception v0 */
/* move/from16 v6, p3 */
} // :goto_4
/* move/from16 v9, p4 */
} // :goto_5
/* move-object v4, v0 */
try { // :try_start_a
(( java.io.FileWriter ) v3 ).close ( ); // invoke-virtual {v3}, Ljava/io/FileWriter;->close()V
/* :try_end_a */
/* .catchall {:try_start_a ..:try_end_a} :catchall_3 */
/* :catchall_3 */
/* move-exception v0 */
/* move-object v5, v0 */
try { // :try_start_b
(( java.lang.Throwable ) v4 ).addSuppressed ( v5 ); // invoke-virtual {v4, v5}, Ljava/lang/Throwable;->addSuppressed(Ljava/lang/Throwable;)V
} // .end local p0 # "this":Lcom/miui/server/stability/LockPerfImpl;
} // .end local p1 # "tracesFile":Ljava/io/File;
} // .end local p2 # "append":Z
} // .end local p3 # "recentSeconds":I
} // .end local p4 # "maxEventNum":I
} // :goto_6
/* throw v4 */
/* :try_end_b */
/* .catch Ljava/io/IOException; {:try_start_b ..:try_end_b} :catch_0 */
/* .line 138 */
} // .end local v3 # "fw":Ljava/io/FileWriter;
/* .restart local p0 # "this":Lcom/miui/server/stability/LockPerfImpl; */
/* .restart local p1 # "tracesFile":Ljava/io/File; */
/* .restart local p2 # "append":Z */
/* .restart local p3 # "recentSeconds":I */
/* .restart local p4 # "maxEventNum":I */
/* :catch_0 */
/* move-exception v0 */
/* :catch_1 */
/* move-exception v0 */
/* :catch_2 */
/* move-exception v0 */
/* move-object/from16 v1, p1 */
/* move/from16 v2, p2 */
} // :goto_7
/* move/from16 v6, p3 */
/* move/from16 v9, p4 */
/* .line 139 */
/* .local v0, "e":Ljava/io/IOException; */
} // :goto_8
v3 = com.miui.server.stability.LockPerfImpl.TAG;
final String v4 = "Dumping slow locked events failed: "; // const-string v4, "Dumping slow locked events failed: "
android.util.Slog .w ( v3,v4,v0 );
/* .line 141 */
} // .end local v0 # "e":Ljava/io/IOException;
} // :goto_9
return;
} // .end method
public void onPerfEnd ( java.lang.String p0 ) {
/* .locals 8 */
/* .param p1, "fullMethod" # Ljava/lang/String; */
/* .line 64 */
v0 = com.miui.server.stability.LockPerfImpl.sThreadLocalData;
(( java.lang.ThreadLocal ) v0 ).get ( ); // invoke-virtual {v0}, Ljava/lang/ThreadLocal;->get()Ljava/lang/Object;
/* check-cast v0, Lcom/miui/server/stability/LockPerfImpl$ThreadLocalData; */
/* .line 65 */
/* .local v0, "tls":Lcom/miui/server/stability/LockPerfImpl$ThreadLocalData; */
/* sget-boolean v1, Lcom/miui/server/stability/LockPerfImpl;->DEBUG:Z */
if ( v1 != null) { // if-eqz v1, :cond_0
com.miui.server.stability.LockPerfImpl$ThreadLocalData .-$$Nest$fgetdebugEventBuffer ( v0 );
/* new-instance v3, Ljava/lang/StringBuilder; */
/* invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V */
final String v4 = "-"; // const-string v4, "-"
(( java.lang.StringBuilder ) v3 ).append ( v4 ); // invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v3 ).append ( p1 ); // invoke-virtual {v3, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v3 ).toString ( ); // invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
(( com.android.internal.util.RingBuffer ) v2 ).append ( v3 ); // invoke-virtual {v2, v3}, Lcom/android/internal/util/RingBuffer;->append(Ljava/lang/Object;)V
/* .line 66 */
} // :cond_0
com.miui.server.stability.LockPerfImpl$ThreadLocalData .-$$Nest$fgetperfStack ( v0 );
/* .line 67 */
/* .local v2, "perfStack":Lcom/miui/server/stability/LockPerfImpl$PerfStack;, "Lcom/miui/server/stability/LockPerfImpl$PerfStack<Lcom/miui/server/stability/LockPerfImpl$PerfInfo;>;" */
(( com.miui.server.stability.LockPerfImpl$PerfStack ) v2 ).topElement ( ); // invoke-virtual {v2}, Lcom/miui/server/stability/LockPerfImpl$PerfStack;->topElement()Ljava/lang/Object;
/* check-cast v3, Lcom/miui/server/stability/LockPerfImpl$PerfInfo; */
/* .line 68 */
/* .local v3, "perfInfo":Lcom/miui/server/stability/LockPerfImpl$PerfInfo; */
if ( v1 != null) { // if-eqz v1, :cond_2
v1 = com.miui.server.stability.LockPerfImpl$PerfInfo .-$$Nest$mmatchMethod ( v3,p1 );
if ( v1 != null) { // if-eqz v1, :cond_1
/* .line 69 */
} // :cond_1
v1 = com.miui.server.stability.LockPerfImpl.TAG;
/* new-instance v4, Ljava/lang/StringBuilder; */
/* invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V */
final String v5 = "Not matched perf start and end! Expected: "; // const-string v5, "Not matched perf start and end! Expected: "
(( java.lang.StringBuilder ) v4 ).append ( v5 ); // invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
com.miui.server.stability.LockPerfImpl$PerfInfo .-$$Nest$mgetMethodString ( v3 );
(( java.lang.StringBuilder ) v4 ).append ( v5 ); // invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
final String v5 = ".Actual: "; // const-string v5, ".Actual: "
(( java.lang.StringBuilder ) v4 ).append ( v5 ); // invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v4 ).append ( p1 ); // invoke-virtual {v4, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v4 ).toString ( ); // invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
android.util.Slog .e ( v1,v4 );
/* .line 71 */
(( com.miui.server.stability.LockPerfImpl$ThreadLocalData ) v0 ).outputRecentEvents ( ); // invoke-virtual {v0}, Lcom/miui/server/stability/LockPerfImpl$ThreadLocalData;->outputRecentEvents()V
/* .line 72 */
/* new-instance v1, Ljava/lang/RuntimeException; */
final String v4 = "Lock perf error! Please check log."; // const-string v4, "Lock perf error! Please check log."
/* invoke-direct {v1, v4}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V */
/* throw v1 */
/* .line 75 */
} // :cond_2
} // :goto_0
android.os.SystemClock .elapsedRealtime ( );
/* move-result-wide v4 */
com.miui.server.stability.LockPerfImpl$PerfInfo .-$$Nest$fgetstart ( v3 );
/* move-result-wide v6 */
/* sub-long/2addr v4, v6 */
/* .line 76 */
/* .local v4, "duration":J */
com.miui.server.stability.LockPerfImpl$PerfInfo .-$$Nest$msetDuration ( v3,v4,v5 );
/* .line 77 */
/* int-to-long v6, v1 */
/* cmp-long v1, v4, v6 */
/* if-lez v1, :cond_3 */
/* .line 78 */
v1 = com.miui.server.stability.LockPerfImpl.TAG;
/* new-instance v6, Ljava/lang/StringBuilder; */
/* invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V */
final String v7 = "Single long locked method."; // const-string v7, "Single long locked method."
(( java.lang.StringBuilder ) v6 ).append ( v7 ); // invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v6 ).append ( v4, v5 ); // invoke-virtual {v6, v4, v5}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;
final String v7 = " ms.method: "; // const-string v7, " ms.method: "
(( java.lang.StringBuilder ) v6 ).append ( v7 ); // invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v6 ).append ( p1 ); // invoke-virtual {v6, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v6 ).toString ( ); // invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
android.util.Slog .w ( v1,v6 );
/* .line 80 */
com.miui.server.stability.LockPerfImpl$ThreadLocalData .-$$Nest$fgetunwindingStack ( v0 );
/* if-nez v1, :cond_3 */
/* .line 81 */
java.lang.Thread .currentThread ( );
(( java.lang.Thread ) v1 ).getStackTrace ( ); // invoke-virtual {v1}, Ljava/lang/Thread;->getStackTrace()[Ljava/lang/StackTraceElement;
com.miui.server.stability.LockPerfImpl .trimStackTrace ( v1 );
com.miui.server.stability.LockPerfImpl$ThreadLocalData .-$$Nest$msetUnwindingStack ( v0,v1 );
/* .line 83 */
com.miui.server.stability.LockPerfImpl$PerfStack .-$$Nest$mclearBuffer ( v2 );
/* .line 87 */
} // :cond_3
(( com.miui.server.stability.LockPerfImpl$PerfStack ) v2 ).pop ( ); // invoke-virtual {v2}, Lcom/miui/server/stability/LockPerfImpl$PerfStack;->pop()Ljava/lang/Object;
/* .line 88 */
com.miui.server.stability.LockPerfImpl$ThreadLocalData .-$$Nest$fgetunwindingStack ( v0 );
if ( v1 != null) { // if-eqz v1, :cond_4
v1 = (( com.miui.server.stability.LockPerfImpl$PerfStack ) v2 ).isEmpty ( ); // invoke-virtual {v2}, Lcom/miui/server/stability/LockPerfImpl$PerfStack;->isEmpty()Z
if ( v1 != null) { // if-eqz v1, :cond_4
/* .line 89 */
v1 = com.miui.server.stability.LockPerfImpl.TAG;
final String v6 = "Record long locked event point 1"; // const-string v6, "Record long locked event point 1"
android.util.Slog .i ( v1,v6 );
/* .line 90 */
com.miui.server.stability.LockPerfImpl$ThreadLocalData .-$$Nest$mrecordEvent ( v0 );
/* .line 92 */
} // :cond_4
return;
} // .end method
public void onPerfStart ( java.lang.String p0 ) {
/* .locals 5 */
/* .param p1, "fullMethod" # Ljava/lang/String; */
/* .line 49 */
v0 = com.miui.server.stability.LockPerfImpl.sThreadLocalData;
(( java.lang.ThreadLocal ) v0 ).get ( ); // invoke-virtual {v0}, Ljava/lang/ThreadLocal;->get()Ljava/lang/Object;
/* check-cast v0, Lcom/miui/server/stability/LockPerfImpl$ThreadLocalData; */
/* .line 50 */
/* .local v0, "tls":Lcom/miui/server/stability/LockPerfImpl$ThreadLocalData; */
/* sget-boolean v1, Lcom/miui/server/stability/LockPerfImpl;->DEBUG:Z */
if ( v1 != null) { // if-eqz v1, :cond_0
com.miui.server.stability.LockPerfImpl$ThreadLocalData .-$$Nest$fgetdebugEventBuffer ( v0 );
(( com.android.internal.util.RingBuffer ) v1 ).append ( p1 ); // invoke-virtual {v1, p1}, Lcom/android/internal/util/RingBuffer;->append(Ljava/lang/Object;)V
/* .line 51 */
} // :cond_0
com.miui.server.stability.LockPerfImpl$ThreadLocalData .-$$Nest$fgetunwindingStack ( v0 );
if ( v1 != null) { // if-eqz v1, :cond_1
/* .line 52 */
v1 = com.miui.server.stability.LockPerfImpl.TAG;
final String v2 = "Record long locked event point 2"; // const-string v2, "Record long locked event point 2"
android.util.Slog .i ( v1,v2 );
/* .line 53 */
com.miui.server.stability.LockPerfImpl$ThreadLocalData .-$$Nest$mrecordEvent ( v0 );
/* .line 55 */
} // :cond_1
com.miui.server.stability.LockPerfImpl$ThreadLocalData .-$$Nest$fgetperfStack ( v0 );
/* new-instance v2, Lcom/miui/server/stability/LockPerfImpl$PerfInfo; */
android.os.SystemClock .elapsedRealtime ( );
/* move-result-wide v3 */
/* invoke-direct {v2, p1, v3, v4}, Lcom/miui/server/stability/LockPerfImpl$PerfInfo;-><init>(Ljava/lang/String;J)V */
(( com.miui.server.stability.LockPerfImpl$PerfStack ) v1 ).push ( v2 ); // invoke-virtual {v1, v2}, Lcom/miui/server/stability/LockPerfImpl$PerfStack;->push(Ljava/lang/Object;)Z
/* .line 56 */
return;
} // .end method
public void perf ( Long p0, java.lang.String p1 ) {
/* .locals 5 */
/* .param p1, "start" # J */
/* .param p3, "fullMethod" # Ljava/lang/String; */
/* .line 97 */
java.lang.System .currentTimeMillis ( );
/* move-result-wide v0 */
/* sub-long/2addr v0, p1 */
/* .line 98 */
/* .local v0, "duration":J */
/* int-to-long v2, v2 */
/* cmp-long v2, v0, v2 */
/* if-lez v2, :cond_0 */
/* .line 99 */
v2 = com.miui.server.stability.LockPerfImpl.TAG;
/* new-instance v3, Ljava/lang/StringBuilder; */
/* invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V */
final String v4 = "Simple long locked method: "; // const-string v4, "Simple long locked method: "
(( java.lang.StringBuilder ) v3 ).append ( v4 ); // invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v3 ).append ( p3 ); // invoke-virtual {v3, p3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
final String v4 = ", Duration: "; // const-string v4, ", Duration: "
(( java.lang.StringBuilder ) v3 ).append ( v4 ); // invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v3 ).append ( v0, v1 ); // invoke-virtual {v3, v0, v1}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v3 ).toString ( ); // invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
android.util.Slog .w ( v2,v3 );
/* .line 101 */
} // :cond_0
return;
} // .end method
