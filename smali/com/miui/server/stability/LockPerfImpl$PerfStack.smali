.class Lcom/miui/server/stability/LockPerfImpl$PerfStack;
.super Ljava/util/ArrayList;
.source "LockPerfImpl.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/miui/server/stability/LockPerfImpl;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0xa
    name = "PerfStack"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<E:",
        "Ljava/lang/Object;",
        ">",
        "Ljava/util/ArrayList<",
        "TE;>;"
    }
.end annotation


# instance fields
.field private currTop:I


# direct methods
.method static bridge synthetic -$$Nest$mclearBuffer(Lcom/miui/server/stability/LockPerfImpl$PerfStack;)V
    .locals 0

    invoke-direct {p0}, Lcom/miui/server/stability/LockPerfImpl$PerfStack;->clearBuffer()V

    return-void
.end method

.method private constructor <init>()V
    .locals 1

    .line 358
    .local p0, "this":Lcom/miui/server/stability/LockPerfImpl$PerfStack;, "Lcom/miui/server/stability/LockPerfImpl$PerfStack<TE;>;"
    invoke-direct {p0}, Ljava/util/ArrayList;-><init>()V

    .line 360
    const/4 v0, 0x0

    iput v0, p0, Lcom/miui/server/stability/LockPerfImpl$PerfStack;->currTop:I

    return-void
.end method

.method synthetic constructor <init>(Lcom/miui/server/stability/LockPerfImpl$PerfStack-IA;)V
    .locals 0

    invoke-direct {p0}, Lcom/miui/server/stability/LockPerfImpl$PerfStack;-><init>()V

    return-void
.end method

.method private clearBuffer()V
    .locals 2

    .line 392
    .local p0, "this":Lcom/miui/server/stability/LockPerfImpl$PerfStack;, "Lcom/miui/server/stability/LockPerfImpl$PerfStack<TE;>;"
    iget v0, p0, Lcom/miui/server/stability/LockPerfImpl$PerfStack;->currTop:I

    invoke-virtual {p0}, Lcom/miui/server/stability/LockPerfImpl$PerfStack;->size()I

    move-result v1

    invoke-virtual {p0, v0, v1}, Lcom/miui/server/stability/LockPerfImpl$PerfStack;->removeRange(II)V

    .line 393
    return-void
.end method

.method private hasBufferedElement()Z
    .locals 2

    .line 388
    .local p0, "this":Lcom/miui/server/stability/LockPerfImpl$PerfStack;, "Lcom/miui/server/stability/LockPerfImpl$PerfStack<TE;>;"
    invoke-virtual {p0}, Lcom/miui/server/stability/LockPerfImpl$PerfStack;->size()I

    move-result v0

    if-lez v0, :cond_0

    invoke-virtual {p0}, Lcom/miui/server/stability/LockPerfImpl$PerfStack;->size()I

    move-result v0

    iget v1, p0, Lcom/miui/server/stability/LockPerfImpl$PerfStack;->currTop:I

    if-le v0, v1, :cond_0

    const/4 v0, 0x1

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    :goto_0
    return v0
.end method


# virtual methods
.method public isEmpty()Z
    .locals 1

    .line 384
    .local p0, "this":Lcom/miui/server/stability/LockPerfImpl$PerfStack;, "Lcom/miui/server/stability/LockPerfImpl$PerfStack<TE;>;"
    iget v0, p0, Lcom/miui/server/stability/LockPerfImpl$PerfStack;->currTop:I

    if-nez v0, :cond_0

    const/4 v0, 0x1

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    :goto_0
    return v0
.end method

.method public pop()Ljava/lang/Object;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()TE;"
        }
    .end annotation

    .line 371
    .local p0, "this":Lcom/miui/server/stability/LockPerfImpl$PerfStack;, "Lcom/miui/server/stability/LockPerfImpl$PerfStack<TE;>;"
    iget v0, p0, Lcom/miui/server/stability/LockPerfImpl$PerfStack;->currTop:I

    add-int/lit8 v0, v0, -0x1

    iput v0, p0, Lcom/miui/server/stability/LockPerfImpl$PerfStack;->currTop:I

    invoke-virtual {p0, v0}, Lcom/miui/server/stability/LockPerfImpl$PerfStack;->get(I)Ljava/lang/Object;

    move-result-object v0

    return-object v0
.end method

.method public push(Ljava/lang/Object;)Z
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(TE;)Z"
        }
    .end annotation

    .line 363
    .local p0, "this":Lcom/miui/server/stability/LockPerfImpl$PerfStack;, "Lcom/miui/server/stability/LockPerfImpl$PerfStack<TE;>;"
    .local p1, "item":Ljava/lang/Object;, "TE;"
    invoke-direct {p0}, Lcom/miui/server/stability/LockPerfImpl$PerfStack;->hasBufferedElement()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 364
    invoke-direct {p0}, Lcom/miui/server/stability/LockPerfImpl$PerfStack;->clearBuffer()V

    .line 366
    :cond_0
    iget v0, p0, Lcom/miui/server/stability/LockPerfImpl$PerfStack;->currTop:I

    add-int/lit8 v0, v0, 0x1

    iput v0, p0, Lcom/miui/server/stability/LockPerfImpl$PerfStack;->currTop:I

    .line 367
    invoke-virtual {p0, p1}, Lcom/miui/server/stability/LockPerfImpl$PerfStack;->add(Ljava/lang/Object;)Z

    move-result v0

    return v0
.end method

.method public top()I
    .locals 1

    .line 379
    .local p0, "this":Lcom/miui/server/stability/LockPerfImpl$PerfStack;, "Lcom/miui/server/stability/LockPerfImpl$PerfStack<TE;>;"
    iget v0, p0, Lcom/miui/server/stability/LockPerfImpl$PerfStack;->currTop:I

    return v0
.end method

.method public topElement()Ljava/lang/Object;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()TE;"
        }
    .end annotation

    .line 375
    .local p0, "this":Lcom/miui/server/stability/LockPerfImpl$PerfStack;, "Lcom/miui/server/stability/LockPerfImpl$PerfStack<TE;>;"
    iget v0, p0, Lcom/miui/server/stability/LockPerfImpl$PerfStack;->currTop:I

    add-int/lit8 v0, v0, -0x1

    invoke-virtual {p0, v0}, Lcom/miui/server/stability/LockPerfImpl$PerfStack;->get(I)Ljava/lang/Object;

    move-result-object v0

    return-object v0
.end method
