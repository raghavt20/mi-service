.class Lcom/miui/server/stability/DumpSysInfoUtil$1;
.super Ljava/lang/Object;
.source "DumpSysInfoUtil.java"

# interfaces
.implements Ljava/lang/Runnable;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/miui/server/stability/DumpSysInfoUtil;->crawlLogsByPower()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# direct methods
.method constructor <init>()V
    .locals 0

    .line 92
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public run()V
    .locals 9

    .line 96
    const-string v0, "dumpsys"

    :try_start_0
    const-string v1, "dumpsys.txt"

    invoke-static {v1}, Lcom/miui/server/stability/DumpSysInfoUtil;->-$$Nest$smcreateFile(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 97
    .local v1, "path":Ljava/lang/String;
    invoke-static {}, Lcom/miui/server/stability/DumpSysInfoUtil;->-$$Nest$smgetmDaemon()Lmiui/mqsas/IMQSNative;

    move-result-object v2

    .line 98
    .local v2, "mClient":Lmiui/mqsas/IMQSNative;
    if-eqz v2, :cond_0

    if-eqz v1, :cond_0

    .line 99
    const/4 v3, 0x4

    new-array v4, v3, [Ljava/lang/String;

    const/4 v5, 0x0

    aput-object v0, v4, v5

    const/4 v6, 0x1

    aput-object v0, v4, v6

    const/4 v7, 0x2

    aput-object v0, v4, v7

    const/4 v8, 0x3

    aput-object v0, v4, v8

    invoke-static {v4}, Ljava/util/Arrays;->asList([Ljava/lang/Object;)Ljava/util/List;

    move-result-object v0

    .line 101
    .local v0, "actions":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;"
    new-array v3, v3, [Ljava/lang/String;

    const-string v4, "SurfaceFlinger"

    aput-object v4, v3, v5

    const-string v4, "activity activities"

    aput-object v4, v3, v6

    const-string/jumbo v4, "window"

    aput-object v4, v3, v7

    const-string v4, "input"

    aput-object v4, v3, v8

    invoke-static {v3}, Ljava/util/Arrays;->asList([Ljava/lang/Object;)Ljava/util/List;

    move-result-object v3

    .line 103
    .local v3, "params":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;"
    invoke-interface {v2, v0, v3, v1, v8}, Lmiui/mqsas/IMQSNative;->captureLogByRunCommand(Ljava/util/List;Ljava/util/List;Ljava/lang/String;I)V

    .line 104
    invoke-static {}, Lcom/miui/server/stability/DumpSysInfoUtil;->-$$Nest$sfgettemporaryDir()Ljava/io/File;

    move-result-object v4

    invoke-static {v4}, Lcom/miui/server/stability/DumpSysInfoUtil;->-$$Nest$smdeleteMissFetchByPower(Ljava/io/File;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 108
    .end local v0    # "actions":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;"
    .end local v1    # "path":Ljava/lang/String;
    .end local v2    # "mClient":Lmiui/mqsas/IMQSNative;
    .end local v3    # "params":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;"
    :cond_0
    goto :goto_0

    .line 106
    :catch_0
    move-exception v0

    .line 107
    .local v0, "e":Ljava/lang/Exception;
    const-string v1, "DumpSysInfoUtil"

    const-string v2, "crash in the crawlDumpsysLogs()"

    invoke-static {v1, v2, v0}, Landroid/util/Slog;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    .line 109
    .end local v0    # "e":Ljava/lang/Exception;
    :goto_0
    return-void
.end method
