.class public Lcom/miui/server/stability/ScoutMemoryUtils;
.super Ljava/lang/Object;
.source "ScoutMemoryUtils.java"


# static fields
.field public static final DIR_DMABUF_ID:I = 0x1

.field public static final DIR_GPU_MEMORY_ID:I = 0x2

.field public static final DIR_PROCS_MEMORY_ID:I = 0x3

.field private static final FILE_DUMP_SUFFIX:Ljava/lang/String; = "_memory_info"

.field public static final FILE_KGSL:Ljava/lang/String; = "/sys/class/kgsl/kgsl/page_alloc"

.field public static final FILE_MALI:Ljava/lang/String; = "/proc/mtk_mali/gpu_memory"

.field public static final GPU_TYPE_KGSL:I = 0x1

.field public static final GPU_TYPE_MTK_MALI:I = 0x2

.field public static final GPU_TYPE_UNKNOW:I = 0x0

.field private static final MAX_MEMLEAK_FILE:I = 0x2

.field private static final MEMLEAK:Ljava/lang/String; = "memleak"

.field private static final PROC_MEMINFO:Ljava/lang/String; = "/proc/meminfo"

.field private static final PROC_SLABINFO:Ljava/lang/String; = "/proc/slabinfo"

.field private static final PROC_VMALLOCINFO:Ljava/lang/String; = "/proc/vmallocinfo"

.field private static final PROC_VMSTAT:Ljava/lang/String; = "/proc/vmstat"

.field private static final PROC_ZONEINFO:Ljava/lang/String; = "/proc/zoneinfo"

.field private static final TAG:Ljava/lang/String; = "ScoutMemoryUtils"

.field public static final dirMap:Ljava/util/HashMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/HashMap<",
            "Ljava/lang/Integer;",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field public static final gpuMemoryWhiteList:Ljava/util/Set;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Set<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private static sGetTagMethod:Ljava/lang/reflect/Method;

.field private static sGetTagTypeMethod:Ljava/lang/reflect/Method;

.field private static sGetTagValueMethod:Ljava/lang/reflect/Method;

.field public static final skipIonProcList:Ljava/util/Set;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Set<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .locals 11

    .line 61
    new-instance v0, Lcom/miui/server/stability/ScoutMemoryUtils$1;

    invoke-direct {v0}, Lcom/miui/server/stability/ScoutMemoryUtils$1;-><init>()V

    sput-object v0, Lcom/miui/server/stability/ScoutMemoryUtils;->dirMap:Ljava/util/HashMap;

    .line 69
    new-instance v0, Ljava/util/HashSet;

    const-string v1, "/system/bin/surfaceflinger"

    const-string v2, "/system/bin/cameraserver"

    const-string v3, "com.android.camera"

    const-string v4, "/vendor/bin/hw/vendor.qti.hardware.display.composer-service"

    const-string v5, "/vendor/bin/hw/android.hardware.graphics.composer@2.3-service"

    const-string v6, "/vendor/bin/hw/vendor.qti.camera.provider@2.7-service_64"

    const-string v7, "/vendor/bin/hw/android.hardware.camera.provider@2.4-service_64"

    const-string v8, "/vendor/bin/hw/camerahalserver"

    const-string v9, "/vendor/bin/hw/vendor.qti.camera.provider-service_64"

    const-string v10, "/vendor/bin/hw/android.hardware.graphics.composer@2.1-service"

    filled-new-array/range {v1 .. v10}, [Ljava/lang/String;

    move-result-object v1

    .line 70
    invoke-static {v1}, Ljava/util/Arrays;->asList([Ljava/lang/Object;)Ljava/util/List;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/util/HashSet;-><init>(Ljava/util/Collection;)V

    sput-object v0, Lcom/miui/server/stability/ScoutMemoryUtils;->skipIonProcList:Ljava/util/Set;

    .line 83
    new-instance v0, Ljava/util/HashSet;

    const-string v1, "com.antutu.benchmark.full:era"

    const-string v2, "com.antutu.benchmark.full:unity"

    filled-new-array {v1, v2}, [Ljava/lang/String;

    move-result-object v1

    .line 84
    invoke-static {v1}, Ljava/util/Arrays;->asList([Ljava/lang/Object;)Ljava/util/List;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/util/HashSet;-><init>(Ljava/util/Collection;)V

    sput-object v0, Lcom/miui/server/stability/ScoutMemoryUtils;->gpuMemoryWhiteList:Ljava/util/Set;

    .line 281
    const/4 v0, 0x0

    sput-object v0, Lcom/miui/server/stability/ScoutMemoryUtils;->sGetTagMethod:Ljava/lang/reflect/Method;

    .line 282
    sput-object v0, Lcom/miui/server/stability/ScoutMemoryUtils;->sGetTagTypeMethod:Ljava/lang/reflect/Method;

    .line 283
    sput-object v0, Lcom/miui/server/stability/ScoutMemoryUtils;->sGetTagValueMethod:Ljava/lang/reflect/Method;

    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .line 38
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static captureGpuMemoryLeakLog(Ljava/lang/String;Ljava/lang/String;I)Ljava/lang/String;
    .locals 6
    .param p0, "gpuMemoryInfo"    # Ljava/lang/String;
    .param p1, "reason"    # Ljava/lang/String;
    .param p2, "type"    # I

    .line 394
    invoke-static {}, Lcom/miui/server/stability/ScoutMemoryUtils;->deleteOldKgslDir()V

    .line 395
    const/4 v0, 0x2

    invoke-static {v0}, Lcom/miui/server/stability/ScoutMemoryUtils;->deleteMemLeakFile(I)V

    .line 396
    invoke-static {p1, v0}, Lcom/miui/server/stability/ScoutMemoryUtils;->getExceptionFile(Ljava/lang/String;I)Ljava/io/File;

    move-result-object v1

    .line 397
    .local v1, "exectionFile":Ljava/io/File;
    invoke-static {p0, v1, p1}, Lcom/miui/server/stability/ScoutMemoryUtils;->dumpProcInfo(Ljava/lang/String;Ljava/io/File;Ljava/lang/String;)V

    .line 398
    invoke-static {v1, p2}, Lcom/miui/server/stability/ScoutMemoryUtils;->dumpGpuMemoryInfo(Ljava/io/File;I)V

    .line 399
    invoke-static {v1}, Lcom/miui/server/stability/ScoutMemoryUtils;->dumpMemoryInfo(Ljava/io/File;)V

    .line 400
    const-string v2, ""

    .line 401
    .local v2, "zipPath":Ljava/lang/String;
    new-instance v3, Lcom/android/server/ScoutHelper$Action;

    invoke-direct {v3}, Lcom/android/server/ScoutHelper$Action;-><init>()V

    .line 402
    .local v3, "action":Lcom/android/server/ScoutHelper$Action;
    const-string v4, "meminfo"

    const-string v5, "dumpsys"

    invoke-virtual {v3, v5, v4}, Lcom/android/server/ScoutHelper$Action;->addActionAndParam(Ljava/lang/String;Ljava/lang/String;)V

    .line 403
    const-string v4, "SurfaceFlinger"

    invoke-virtual {v3, v5, v4}, Lcom/android/server/ScoutHelper$Action;->addActionAndParam(Ljava/lang/String;Ljava/lang/String;)V

    .line 404
    const-string v4, "gfxinfo"

    invoke-virtual {v3, v5, v4}, Lcom/android/server/ScoutHelper$Action;->addActionAndParam(Ljava/lang/String;Ljava/lang/String;)V

    .line 405
    invoke-static {}, Lmiui/mqsas/scout/ScoutUtils;->isLibraryTest()Z

    move-result v4

    if-eqz v4, :cond_0

    .line 406
    const-string v4, "logcat"

    const-string v5, "-b main,system,crash,events"

    invoke-virtual {v3, v4, v5}, Lcom/android/server/ScoutHelper$Action;->addActionAndParam(Ljava/lang/String;Ljava/lang/String;)V

    .line 408
    :cond_0
    if-eqz v1, :cond_1

    .line 409
    invoke-virtual {v1}, Ljava/io/File;->getAbsolutePath()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Lcom/android/server/ScoutHelper$Action;->addIncludeFile(Ljava/lang/String;)V

    .line 411
    :cond_1
    invoke-static {v0}, Lcom/miui/server/stability/ScoutMemoryUtils;->getMemLeakDir(I)Ljava/io/File;

    move-result-object v0

    .line 412
    .local v0, "memleakDir":Ljava/io/File;
    if-eqz v0, :cond_2

    .line 413
    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v0}, Ljava/io/File;->getAbsolutePath()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, "/"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    const-string v5, "memleak"

    invoke-static {p1, v3, v5, v4}, Lcom/android/server/ScoutHelper;->dumpOfflineLog(Ljava/lang/String;Lcom/android/server/ScoutHelper$Action;Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    .line 415
    :cond_2
    return-object v2
.end method

.method public static captureIonLeakLog(Ljava/lang/String;Ljava/lang/String;Ljava/util/HashSet;)Ljava/lang/String;
    .locals 7
    .param p0, "dmabufInfo"    # Ljava/lang/String;
    .param p1, "reason"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            "Ljava/util/HashSet<",
            "Ljava/lang/Integer;",
            ">;)",
            "Ljava/lang/String;"
        }
    .end annotation

    .line 364
    .local p2, "fdPids":Ljava/util/HashSet;, "Ljava/util/HashSet<Ljava/lang/Integer;>;"
    const/4 v0, 0x1

    invoke-static {v0}, Lcom/miui/server/stability/ScoutMemoryUtils;->deleteMemLeakFile(I)V

    .line 365
    invoke-static {p1, v0}, Lcom/miui/server/stability/ScoutMemoryUtils;->getExceptionFile(Ljava/lang/String;I)Ljava/io/File;

    move-result-object v1

    .line 366
    .local v1, "exectionFile":Ljava/io/File;
    const-string v2, ""

    .line 367
    .local v2, "zipPath":Ljava/lang/String;
    invoke-static {p0, v1, p1}, Lcom/miui/server/stability/ScoutMemoryUtils;->dumpProcInfo(Ljava/lang/String;Ljava/io/File;Ljava/lang/String;)V

    .line 368
    invoke-virtual {p2}, Ljava/util/HashSet;->size()I

    move-result v3

    if-lez v3, :cond_0

    .line 369
    invoke-virtual {p2}, Ljava/util/HashSet;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :goto_0
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v4

    if-eqz v4, :cond_0

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Ljava/lang/Integer;

    invoke-virtual {v4}, Ljava/lang/Integer;->intValue()I

    move-result v4

    .line 370
    .local v4, "fdPid":I
    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "captureIonLeakLog: "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    const-string v6, "ScoutMemoryUtils"

    invoke-static {v6, v5}, Landroid/util/Slog;->w(Ljava/lang/String;Ljava/lang/String;)I

    .line 371
    invoke-static {v4, v1}, Lcom/miui/server/stability/ScoutMemoryUtils;->dumpOpenFds(ILjava/io/File;)V

    .line 372
    .end local v4    # "fdPid":I
    goto :goto_0

    .line 374
    :cond_0
    new-instance v3, Lcom/android/server/ScoutHelper$Action;

    invoke-direct {v3}, Lcom/android/server/ScoutHelper$Action;-><init>()V

    .line 375
    .local v3, "action":Lcom/android/server/ScoutHelper$Action;
    const-string v4, "SurfaceFlinger"

    const-string v5, "dumpsys"

    invoke-virtual {v3, v5, v4}, Lcom/android/server/ScoutHelper$Action;->addActionAndParam(Ljava/lang/String;Ljava/lang/String;)V

    .line 376
    const-string v4, "gfxinfo"

    invoke-virtual {v3, v5, v4}, Lcom/android/server/ScoutHelper$Action;->addActionAndParam(Ljava/lang/String;Ljava/lang/String;)V

    .line 377
    const-string v4, "activity"

    invoke-virtual {v3, v5, v4}, Lcom/android/server/ScoutHelper$Action;->addActionAndParam(Ljava/lang/String;Ljava/lang/String;)V

    .line 378
    const-string/jumbo v4, "window"

    invoke-virtual {v3, v5, v4}, Lcom/android/server/ScoutHelper$Action;->addActionAndParam(Ljava/lang/String;Ljava/lang/String;)V

    .line 379
    const-string v4, "meminfo"

    invoke-virtual {v3, v5, v4}, Lcom/android/server/ScoutHelper$Action;->addActionAndParam(Ljava/lang/String;Ljava/lang/String;)V

    .line 380
    invoke-static {}, Lmiui/mqsas/scout/ScoutUtils;->isLibraryTest()Z

    move-result v4

    if-eqz v4, :cond_1

    .line 381
    const-string v4, "logcat"

    const-string v5, "-b main,system,crash,events"

    invoke-virtual {v3, v4, v5}, Lcom/android/server/ScoutHelper$Action;->addActionAndParam(Ljava/lang/String;Ljava/lang/String;)V

    .line 383
    :cond_1
    if-eqz v1, :cond_2

    .line 384
    invoke-virtual {v1}, Ljava/io/File;->getAbsolutePath()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Lcom/android/server/ScoutHelper$Action;->addIncludeFile(Ljava/lang/String;)V

    .line 386
    :cond_2
    invoke-static {v0}, Lcom/miui/server/stability/ScoutMemoryUtils;->getMemLeakDir(I)Ljava/io/File;

    move-result-object v0

    .line 387
    .local v0, "memleakDir":Ljava/io/File;
    if-eqz v0, :cond_3

    .line 388
    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v0}, Ljava/io/File;->getAbsolutePath()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, "/"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    const-string v5, "memleak"

    invoke-static {p1, v3, v5, v4}, Lcom/android/server/ScoutHelper;->dumpOfflineLog(Ljava/lang/String;Lcom/android/server/ScoutHelper$Action;Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    .line 390
    :cond_3
    return-object v2
.end method

.method public static deleteMemLeakFile(I)V
    .locals 9
    .param p0, "dirId"    # I

    .line 158
    new-instance v0, Ljava/util/TreeSet;

    invoke-direct {v0}, Ljava/util/TreeSet;-><init>()V

    .line 159
    .local v0, "existinglog":Ljava/util/TreeSet;, "Ljava/util/TreeSet<Ljava/io/File;>;"
    invoke-static {p0}, Lcom/miui/server/stability/ScoutMemoryUtils;->getMemLeakDir(I)Ljava/io/File;

    move-result-object v1

    .line 160
    .local v1, "memleakDir":Ljava/io/File;
    if-nez v1, :cond_0

    return-void

    .line 161
    :cond_0
    invoke-virtual {v1}, Ljava/io/File;->listFiles()[Ljava/io/File;

    move-result-object v2

    array-length v3, v2

    const/4 v4, 0x0

    :goto_0
    const-string v5, "ScoutMemoryUtils"

    if-ge v4, v3, :cond_3

    aget-object v6, v2, v4

    .line 162
    .local v6, "file":Ljava/io/File;
    invoke-virtual {v6}, Ljava/io/File;->getName()Ljava/lang/String;

    move-result-object v7

    const-string v8, ".txt"

    invoke-virtual {v7, v8}, Ljava/lang/String;->endsWith(Ljava/lang/String;)Z

    move-result v7

    if-eqz v7, :cond_1

    .line 163
    invoke-virtual {v6}, Ljava/io/File;->delete()Z

    move-result v7

    if-nez v7, :cond_2

    .line 164
    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    const-string v8, "Failed to clean up memleak txt file:"

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-static {v5, v7}, Landroid/util/Slog;->w(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_1

    .line 168
    :cond_1
    invoke-virtual {v0, v6}, Ljava/util/TreeSet;->add(Ljava/lang/Object;)Z

    .line 161
    .end local v6    # "file":Ljava/io/File;
    :cond_2
    :goto_1
    add-int/lit8 v4, v4, 0x1

    goto :goto_0

    .line 170
    :cond_3
    invoke-virtual {v0}, Ljava/util/TreeSet;->size()I

    move-result v2

    const/4 v3, 0x2

    if-lt v2, v3, :cond_6

    .line 171
    const/4 v2, 0x0

    .local v2, "i":I
    :goto_2
    const/4 v3, 0x1

    if-ge v2, v3, :cond_4

    .line 172
    invoke-virtual {v0}, Ljava/util/TreeSet;->pollLast()Ljava/lang/Object;

    .line 171
    add-int/lit8 v2, v2, 0x1

    goto :goto_2

    .line 174
    .end local v2    # "i":I
    :cond_4
    invoke-virtual {v0}, Ljava/util/TreeSet;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_3
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_6

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/io/File;

    .line 175
    .local v3, "file":Ljava/io/File;
    invoke-virtual {v3}, Ljava/io/File;->delete()Z

    move-result v4

    if-nez v4, :cond_5

    .line 176
    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "Failed to clean up memleak log:"

    invoke-virtual {v4, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v5, v4}, Landroid/util/Slog;->w(Ljava/lang/String;Ljava/lang/String;)I

    .line 178
    .end local v3    # "file":Ljava/io/File;
    :cond_5
    goto :goto_3

    .line 180
    :cond_6
    return-void
.end method

.method public static deleteOldFiles(II)V
    .locals 5
    .param p0, "dirId"    # I
    .param p1, "limit"    # I

    .line 183
    invoke-static {p0}, Lcom/miui/server/stability/ScoutMemoryUtils;->getMemLeakDir(I)Ljava/io/File;

    move-result-object v0

    .line 184
    .local v0, "memleakDir":Ljava/io/File;
    if-nez v0, :cond_0

    return-void

    .line 185
    :cond_0
    invoke-virtual {v0}, Ljava/io/File;->listFiles()[Ljava/io/File;

    move-result-object v1

    .line 186
    .local v1, "files":[Ljava/io/File;
    array-length v2, v1

    if-gt v2, p1, :cond_1

    .line 187
    return-void

    .line 189
    :cond_1
    new-instance v2, Lcom/miui/server/stability/ScoutMemoryUtils$$ExternalSyntheticLambda0;

    invoke-direct {v2}, Lcom/miui/server/stability/ScoutMemoryUtils$$ExternalSyntheticLambda0;-><init>()V

    invoke-static {v2}, Ljava/util/Comparator;->comparingLong(Ljava/util/function/ToLongFunction;)Ljava/util/Comparator;

    move-result-object v2

    invoke-static {v1, v2}, Ljava/util/Arrays;->sort([Ljava/lang/Object;Ljava/util/Comparator;)V

    .line 190
    const/4 v2, 0x0

    .local v2, "i":I
    :goto_0
    array-length v3, v1

    sub-int/2addr v3, p1

    if-ge v2, v3, :cond_3

    .line 191
    aget-object v3, v1, v2

    invoke-virtual {v3}, Ljava/io/File;->delete()Z

    move-result v3

    if-nez v3, :cond_2

    .line 192
    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "Failed to delete log file: "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    aget-object v4, v1, v2

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    const-string v4, "ScoutMemoryUtils"

    invoke-static {v4, v3}, Landroid/util/Slog;->w(Ljava/lang/String;Ljava/lang/String;)I

    .line 190
    :cond_2
    add-int/lit8 v2, v2, 0x1

    goto :goto_0

    .line 195
    .end local v2    # "i":I
    :cond_3
    return-void
.end method

.method public static deleteOldKgslDir()V
    .locals 8

    .line 136
    new-instance v0, Ljava/io/File;

    const-string v1, "/data/miuilog/stability"

    invoke-direct {v0, v1}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 137
    .local v0, "stDir":Ljava/io/File;
    invoke-virtual {v0}, Ljava/io/File;->exists()Z

    move-result v1

    const-string v2, "ScoutMemoryUtils"

    if-nez v1, :cond_0

    .line 138
    const-string/jumbo v1, "stability log dir isn\'t exists"

    invoke-static {v2, v1}, Landroid/util/Slog;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 139
    return-void

    .line 141
    :cond_0
    new-instance v1, Ljava/io/File;

    const-string v3, "memleak"

    invoke-direct {v1, v0, v3}, Ljava/io/File;-><init>(Ljava/io/File;Ljava/lang/String;)V

    .line 142
    .local v1, "memleakDir":Ljava/io/File;
    invoke-virtual {v1}, Ljava/io/File;->exists()Z

    move-result v3

    if-nez v3, :cond_1

    .line 143
    const-string v3, "memleak dir isn\'t exists"

    invoke-static {v2, v3}, Landroid/util/Slog;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 144
    return-void

    .line 146
    :cond_1
    new-instance v3, Ljava/io/File;

    const-string v4, "kgsl"

    invoke-direct {v3, v1, v4}, Ljava/io/File;-><init>(Ljava/io/File;Ljava/lang/String;)V

    .line 147
    .local v3, "kgslDir":Ljava/io/File;
    invoke-virtual {v3}, Ljava/io/File;->exists()Z

    move-result v4

    if-eqz v4, :cond_3

    invoke-virtual {v3}, Ljava/io/File;->isDirectory()Z

    move-result v4

    if-eqz v4, :cond_3

    .line 148
    invoke-virtual {v3}, Ljava/io/File;->listFiles()[Ljava/io/File;

    move-result-object v4

    .line 149
    .local v4, "listFiles":[Ljava/io/File;
    array-length v5, v4

    const/4 v6, 0x0

    :goto_0
    if-ge v6, v5, :cond_2

    aget-object v7, v4, v6

    .line 150
    .local v7, "file":Ljava/io/File;
    invoke-virtual {v7}, Ljava/io/File;->delete()Z

    .line 149
    .end local v7    # "file":Ljava/io/File;
    add-int/lit8 v6, v6, 0x1

    goto :goto_0

    .line 152
    :cond_2
    invoke-virtual {v3}, Ljava/io/File;->delete()Z

    .line 153
    const-string v5, "delete kgsl dir"

    invoke-static {v2, v5}, Landroid/util/Slog;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 155
    .end local v4    # "listFiles":[Ljava/io/File;
    :cond_3
    return-void
.end method

.method public static doFsyncZipFile(I)V
    .locals 9
    .param p0, "dirId"    # I

    .line 198
    new-instance v0, Ljava/util/TreeSet;

    invoke-direct {v0}, Ljava/util/TreeSet;-><init>()V

    .line 199
    .local v0, "existinglog":Ljava/util/TreeSet;, "Ljava/util/TreeSet<Ljava/io/File;>;"
    invoke-static {p0}, Lcom/miui/server/stability/ScoutMemoryUtils;->getMemLeakDir(I)Ljava/io/File;

    move-result-object v1

    .line 200
    .local v1, "memleakDir":Ljava/io/File;
    if-nez v1, :cond_0

    return-void

    .line 201
    :cond_0
    invoke-virtual {v1}, Ljava/io/File;->listFiles()[Ljava/io/File;

    move-result-object v2

    array-length v3, v2

    const/4 v4, 0x0

    move v5, v4

    :goto_0
    if-ge v5, v3, :cond_2

    aget-object v6, v2, v5

    .line 202
    .local v6, "file":Ljava/io/File;
    invoke-virtual {v6}, Ljava/io/File;->getName()Ljava/lang/String;

    move-result-object v7

    const-string v8, ".zip"

    invoke-virtual {v7, v8}, Ljava/lang/String;->endsWith(Ljava/lang/String;)Z

    move-result v7

    if-eqz v7, :cond_1

    .line 203
    invoke-virtual {v0, v6}, Ljava/util/TreeSet;->add(Ljava/lang/Object;)Z

    .line 201
    .end local v6    # "file":Ljava/io/File;
    :cond_1
    add-int/lit8 v5, v5, 0x1

    goto :goto_0

    .line 206
    :cond_2
    invoke-virtual {v0}, Ljava/util/TreeSet;->pollLast()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/io/File;

    .line 207
    .local v2, "fsyncFile":Ljava/io/File;
    const/4 v3, 0x0

    .line 209
    .local v3, "fd":Ljava/io/FileDescriptor;
    :try_start_0
    invoke-virtual {v2}, Ljava/io/File;->getAbsolutePath()Ljava/lang/String;

    move-result-object v5

    sget v6, Landroid/system/OsConstants;->O_RDONLY:I

    invoke-static {v5, v6, v4}, Landroid/system/Os;->open(Ljava/lang/String;II)Ljava/io/FileDescriptor;

    move-result-object v4

    move-object v3, v4

    .line 210
    invoke-static {v3}, Landroid/system/Os;->fsync(Ljava/io/FileDescriptor;)V

    .line 211
    invoke-static {v3}, Landroid/system/Os;->close(Ljava/io/FileDescriptor;)V

    .line 212
    const-string v4, "ScoutMemoryUtils"

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "finish fsync file: "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v2}, Ljava/io/File;->getName()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5}, Landroid/util/Slog;->w(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto :goto_1

    .line 216
    :catchall_0
    move-exception v4

    goto :goto_2

    .line 213
    :catch_0
    move-exception v4

    .line 214
    .local v4, "e":Ljava/lang/Exception;
    :try_start_1
    invoke-virtual {v4}, Ljava/lang/Exception;->printStackTrace()V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 216
    .end local v4    # "e":Ljava/lang/Exception;
    :goto_1
    invoke-static {v3}, Llibcore/io/IoUtils;->closeQuietly(Ljava/io/FileDescriptor;)V

    .line 217
    nop

    .line 218
    return-void

    .line 216
    :goto_2
    invoke-static {v3}, Llibcore/io/IoUtils;->closeQuietly(Ljava/io/FileDescriptor;)V

    .line 217
    throw v4
.end method

.method public static dumpGpuMemoryInfo(Ljava/io/File;I)V
    .locals 1
    .param p0, "file"    # Ljava/io/File;
    .param p1, "type"    # I

    .line 274
    const/4 v0, 0x1

    if-ne p1, v0, :cond_0

    .line 275
    const-string v0, "/sys/class/kgsl/kgsl/page_alloc"

    invoke-static {v0, p0}, Lcom/miui/server/stability/ScoutMemoryUtils;->dumpInfo(Ljava/lang/String;Ljava/io/File;)V

    goto :goto_0

    .line 276
    :cond_0
    const/4 v0, 0x2

    if-ne p1, v0, :cond_1

    .line 277
    const-string v0, "/proc/mtk_mali/gpu_memory"

    invoke-static {v0, p0}, Lcom/miui/server/stability/ScoutMemoryUtils;->dumpInfo(Ljava/lang/String;Ljava/io/File;)V

    .line 279
    :cond_1
    :goto_0
    return-void
.end method

.method public static dumpInfo(Ljava/lang/String;Ljava/io/File;)V
    .locals 7
    .param p0, "path"    # Ljava/lang/String;
    .param p1, "file"    # Ljava/io/File;

    .line 245
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    .line 246
    .local v0, "builder":Ljava/lang/StringBuilder;
    new-instance v1, Ljava/io/File;

    invoke-direct {v1, p0}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 247
    .local v1, "readerFile":Ljava/io/File;
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "---Dump "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v4, " START---\n"

    invoke-virtual {v2, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 248
    :try_start_0
    new-instance v2, Ljava/io/BufferedReader;

    new-instance v4, Ljava/io/FileReader;

    invoke-direct {v4, v1}, Ljava/io/FileReader;-><init>(Ljava/io/File;)V

    invoke-direct {v2, v4}, Ljava/io/BufferedReader;-><init>(Ljava/io/Reader;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 249
    .local v2, "reader":Ljava/io/BufferedReader;
    const/4 v4, 0x0

    .line 250
    .local v4, "readInfo":Ljava/lang/String;
    :goto_0
    :try_start_1
    invoke-virtual {v2}, Ljava/io/BufferedReader;->readLine()Ljava/lang/String;

    move-result-object v5

    move-object v4, v5

    if-eqz v5, :cond_0

    .line 251
    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v5, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, "\n"

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v0, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0

    .line 253
    .end local v4    # "readInfo":Ljava/lang/String;
    :cond_0
    :try_start_2
    invoke-virtual {v2}, Ljava/io/BufferedReader;->close()V
    :try_end_2
    .catch Ljava/lang/Exception; {:try_start_2 .. :try_end_2} :catch_0

    .line 255
    .end local v2    # "reader":Ljava/io/BufferedReader;
    goto :goto_2

    .line 248
    .restart local v2    # "reader":Ljava/io/BufferedReader;
    :catchall_0
    move-exception v4

    :try_start_3
    invoke-virtual {v2}, Ljava/io/BufferedReader;->close()V
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    goto :goto_1

    :catchall_1
    move-exception v5

    :try_start_4
    invoke-virtual {v4, v5}, Ljava/lang/Throwable;->addSuppressed(Ljava/lang/Throwable;)V

    .end local v0    # "builder":Ljava/lang/StringBuilder;
    .end local v1    # "readerFile":Ljava/io/File;
    .end local p0    # "path":Ljava/lang/String;
    .end local p1    # "file":Ljava/io/File;
    :goto_1
    throw v4
    :try_end_4
    .catch Ljava/lang/Exception; {:try_start_4 .. :try_end_4} :catch_0

    .line 253
    .end local v2    # "reader":Ljava/io/BufferedReader;
    .restart local v0    # "builder":Ljava/lang/StringBuilder;
    .restart local v1    # "readerFile":Ljava/io/File;
    .restart local p0    # "path":Ljava/lang/String;
    .restart local p1    # "file":Ljava/io/File;
    :catch_0
    move-exception v2

    .line 254
    .local v2, "e":Ljava/lang/Exception;
    const-string v4, "ScoutMemoryUtils"

    const-string v5, "Failed to read thread stat"

    invoke-static {v4, v5, v2}, Landroid/util/Slog;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    .line 256
    .end local v2    # "e":Ljava/lang/Exception;
    :goto_2
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, " END---\n\n"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 257
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v2, p1}, Lcom/miui/server/stability/ScoutMemoryUtils;->dumpInfoToFile(Ljava/lang/String;Ljava/io/File;)Z

    .line 259
    return-void
.end method

.method public static dumpInfoToFile(Ljava/lang/String;Ljava/io/File;)Z
    .locals 8
    .param p0, "info"    # Ljava/lang/String;
    .param p1, "file"    # Ljava/io/File;

    .line 221
    const/4 v0, 0x0

    if-nez p1, :cond_0

    return v0

    .line 222
    :cond_0
    const/4 v1, 0x0

    .line 224
    .local v1, "fd":Ljava/io/FileDescriptor;
    :try_start_0
    new-instance v2, Ljava/io/FileOutputStream;

    const/4 v3, 0x1

    invoke-direct {v2, p1, v3}, Ljava/io/FileOutputStream;-><init>(Ljava/io/File;Z)V

    .line 225
    .local v2, "out":Ljava/io/FileOutputStream;
    new-instance v4, Lcom/android/internal/util/FastPrintWriter;

    invoke-direct {v4, v2}, Lcom/android/internal/util/FastPrintWriter;-><init>(Ljava/io/OutputStream;)V

    .line 226
    .local v4, "pw":Ljava/io/PrintWriter;
    invoke-virtual {v4, p0}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V

    .line 227
    invoke-virtual {v4}, Ljava/io/PrintWriter;->close()V

    .line 228
    nop

    .line 229
    invoke-virtual {p1}, Ljava/io/File;->toString()Ljava/lang/String;

    move-result-object v5

    .line 228
    const/4 v6, -0x1

    const/16 v7, 0x1fc

    invoke-static {v5, v7, v6, v6}, Landroid/os/FileUtils;->setPermissions(Ljava/lang/String;III)I

    .line 232
    invoke-virtual {p1}, Ljava/io/File;->getAbsolutePath()Ljava/lang/String;

    move-result-object v5

    sget v6, Landroid/system/OsConstants;->O_RDONLY:I

    invoke-static {v5, v6, v0}, Landroid/system/Os;->open(Ljava/lang/String;II)Ljava/io/FileDescriptor;

    move-result-object v5

    move-object v1, v5

    .line 233
    invoke-static {v1}, Landroid/system/Os;->fsync(Ljava/io/FileDescriptor;)V

    .line 234
    invoke-static {v1}, Landroid/system/Os;->close(Ljava/io/FileDescriptor;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 239
    .end local v2    # "out":Ljava/io/FileOutputStream;
    .end local v4    # "pw":Ljava/io/PrintWriter;
    invoke-static {v1}, Llibcore/io/IoUtils;->closeQuietly(Ljava/io/FileDescriptor;)V

    .line 240
    nop

    .line 241
    return v3

    .line 239
    :catchall_0
    move-exception v0

    goto :goto_0

    .line 235
    :catch_0
    move-exception v2

    .line 236
    .local v2, "e":Ljava/lang/Exception;
    :try_start_1
    invoke-virtual {v2}, Ljava/lang/Exception;->printStackTrace()V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 237
    nop

    .line 239
    invoke-static {v1}, Llibcore/io/IoUtils;->closeQuietly(Ljava/io/FileDescriptor;)V

    .line 237
    return v0

    .line 239
    .end local v2    # "e":Ljava/lang/Exception;
    :goto_0
    invoke-static {v1}, Llibcore/io/IoUtils;->closeQuietly(Ljava/io/FileDescriptor;)V

    .line 240
    throw v0
.end method

.method public static dumpMemoryInfo(Ljava/io/File;)V
    .locals 1
    .param p0, "file"    # Ljava/io/File;

    .line 270
    const-string v0, "/proc/meminfo"

    invoke-static {v0, p0}, Lcom/miui/server/stability/ScoutMemoryUtils;->dumpInfo(Ljava/lang/String;Ljava/io/File;)V

    .line 271
    return-void
.end method

.method public static dumpOpenFds(ILjava/io/File;)V
    .locals 10
    .param p0, "pid"    # I
    .param p1, "file"    # Ljava/io/File;

    .line 338
    new-instance v0, Ljava/io/File;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "/proc/"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-static {p0}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "/fdinfo"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 339
    .local v0, "fdPath":Ljava/io/File;
    invoke-virtual {v0}, Ljava/io/File;->listFiles()[Ljava/io/File;

    move-result-object v1

    .line 340
    .local v1, "fds":[Ljava/io/File;
    invoke-static {v1}, Lcom/android/internal/util/ArrayUtils;->isEmpty([Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_0

    .line 341
    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "failed to read fds! path=/proc/"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-static {p0}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    const-string v3, "ScoutMemoryUtils"

    invoke-static {v3, v2}, Landroid/util/Slog;->w(Ljava/lang/String;Ljava/lang/String;)I

    .line 342
    return-void

    .line 345
    :cond_0
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    .line 346
    .local v2, "builder":Ljava/lang/StringBuilder;
    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "---Dump fd info pid: "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, p0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, ", size: "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    array-length v4, v1

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, " START---\n"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 347
    array-length v3, v1

    const/4 v4, 0x0

    :goto_0
    if-ge v4, v3, :cond_2

    aget-object v5, v1, v4

    .line 348
    .local v5, "fd":Ljava/io/File;
    invoke-virtual {v5}, Ljava/io/File;->getAbsolutePath()Ljava/lang/String;

    move-result-object v6

    .line 350
    .local v6, "fd_path":Ljava/lang/String;
    invoke-static {p0, v5}, Lcom/miui/server/stability/ScoutMemoryUtils;->getFdIno(ILjava/io/File;)Ljava/lang/String;

    move-result-object v7

    .line 351
    .local v7, "fdinfo":Ljava/lang/String;
    const-string v8, "exp_name"

    invoke-virtual {v7, v8}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v8

    if-eqz v8, :cond_1

    .line 352
    new-instance v8, Ljava/lang/StringBuilder;

    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v8, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    const-string v9, " ----> "

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-static {v5}, Lcom/miui/server/stability/ScoutMemoryUtils;->getFdOwner(Ljava/io/File;)Ljava/lang/String;

    move-result-object v9

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    const-string v9, "\n"

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v2, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 347
    .end local v5    # "fd":Ljava/io/File;
    .end local v6    # "fd_path":Ljava/lang/String;
    .end local v7    # "fdinfo":Ljava/lang/String;
    :cond_1
    add-int/lit8 v4, v4, 0x1

    goto :goto_0

    .line 355
    :cond_2
    const-string v3, "---Dump fd info END---\n\n"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 356
    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v3, p1}, Lcom/miui/server/stability/ScoutMemoryUtils;->dumpInfoToFile(Ljava/lang/String;Ljava/io/File;)Z

    .line 357
    return-void
.end method

.method public static dumpProcInfo(Ljava/lang/String;Ljava/io/File;Ljava/lang/String;)V
    .locals 4
    .param p0, "info"    # Ljava/lang/String;
    .param p1, "file"    # Ljava/io/File;
    .param p2, "reason"    # Ljava/lang/String;

    .line 262
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    .line 263
    .local v0, "builder":Ljava/lang/StringBuilder;
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "---Dump "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v3, " usage info START---\n"

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 264
    invoke-virtual {v0, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 265
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " usage info END---\n\n"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 266
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1, p1}, Lcom/miui/server/stability/ScoutMemoryUtils;->dumpInfoToFile(Ljava/lang/String;Ljava/io/File;)Z

    .line 267
    return-void
.end method

.method public static getExceptionFile(Ljava/lang/String;I)Ljava/io/File;
    .locals 6
    .param p0, "reason"    # Ljava/lang/String;
    .param p1, "dirId"    # I

    .line 127
    invoke-static {p1}, Lcom/miui/server/stability/ScoutMemoryUtils;->getMemLeakDir(I)Ljava/io/File;

    move-result-object v0

    .line 128
    .local v0, "memleakDir":Ljava/io/File;
    if-nez v0, :cond_0

    const/4 v1, 0x0

    return-object v1

    .line 129
    :cond_0
    new-instance v1, Ljava/text/SimpleDateFormat;

    const-string/jumbo v2, "yyyy-MM-dd-HH-mm-ss-SSS"

    sget-object v3, Ljava/util/Locale;->US:Ljava/util/Locale;

    invoke-direct {v1, v2, v3}, Ljava/text/SimpleDateFormat;-><init>(Ljava/lang/String;Ljava/util/Locale;)V

    .line 130
    .local v1, "memleakFileDateFormat":Ljava/text/SimpleDateFormat;
    new-instance v2, Ljava/util/Date;

    invoke-direct {v2}, Ljava/util/Date;-><init>()V

    invoke-virtual {v1, v2}, Ljava/text/SimpleDateFormat;->format(Ljava/util/Date;)Ljava/lang/String;

    move-result-object v2

    .line 131
    .local v2, "formattedDate":Ljava/lang/String;
    new-instance v3, Ljava/io/File;

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v4, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, "_memory_info"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, "_"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, ".txt"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-direct {v3, v0, v4}, Ljava/io/File;-><init>(Ljava/io/File;Ljava/lang/String;)V

    .line 132
    .local v3, "fname":Ljava/io/File;
    return-object v3
.end method

.method public static getFdIno(ILjava/io/File;)Ljava/lang/String;
    .locals 6
    .param p0, "pid"    # I
    .param p1, "fdPath"    # Ljava/io/File;

    .line 321
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "/proc/"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-static {p0}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "/fdinfo/"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {p1}, Ljava/io/File;->getName()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    .line 322
    .local v0, "name":Ljava/lang/String;
    :try_start_0
    new-instance v1, Ljava/io/BufferedReader;

    new-instance v2, Ljava/io/FileReader;

    invoke-direct {v2, v0}, Ljava/io/FileReader;-><init>(Ljava/lang/String;)V

    invoke-direct {v1, v2}, Ljava/io/BufferedReader;-><init>(Ljava/io/Reader;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 323
    .local v1, "fdinfoReader":Ljava/io/BufferedReader;
    const/4 v2, 0x0

    .line 324
    .local v2, "fdinfo":Ljava/lang/String;
    :try_start_1
    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    .line 325
    .local v3, "builder":Ljava/lang/StringBuilder;
    const-string v4, " ( fdinfo -->"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 326
    :goto_0
    invoke-virtual {v1}, Ljava/io/BufferedReader;->readLine()Ljava/lang/String;

    move-result-object v4

    move-object v2, v4

    if-eqz v4, :cond_0

    .line 327
    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, " "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    goto :goto_0

    .line 329
    :cond_0
    const-string v4, " <--)"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 330
    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 331
    :try_start_2
    invoke-virtual {v1}, Ljava/io/BufferedReader;->close()V
    :try_end_2
    .catch Ljava/lang/Exception; {:try_start_2 .. :try_end_2} :catch_0

    .line 330
    return-object v4

    .line 322
    .end local v2    # "fdinfo":Ljava/lang/String;
    .end local v3    # "builder":Ljava/lang/StringBuilder;
    :catchall_0
    move-exception v2

    :try_start_3
    invoke-virtual {v1}, Ljava/io/BufferedReader;->close()V
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    goto :goto_1

    :catchall_1
    move-exception v3

    :try_start_4
    invoke-virtual {v2, v3}, Ljava/lang/Throwable;->addSuppressed(Ljava/lang/Throwable;)V

    .end local v0    # "name":Ljava/lang/String;
    .end local p0    # "pid":I
    .end local p1    # "fdPath":Ljava/io/File;
    :goto_1
    throw v2
    :try_end_4
    .catch Ljava/lang/Exception; {:try_start_4 .. :try_end_4} :catch_0

    .line 331
    .end local v1    # "fdinfoReader":Ljava/io/BufferedReader;
    .restart local v0    # "name":Ljava/lang/String;
    .restart local p0    # "pid":I
    .restart local p1    # "fdPath":Ljava/io/File;
    :catch_0
    move-exception v1

    .line 332
    .local v1, "e":Ljava/lang/Exception;
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "getFdIno: "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    const-string v3, "ScoutMemoryUtils"

    invoke-static {v3, v2, v1}, Landroid/util/Slog;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    .line 334
    .end local v1    # "e":Ljava/lang/Exception;
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "(unknow ino: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ")"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    return-object v1
.end method

.method public static getFdOwner(Ljava/io/File;)Ljava/lang/String;
    .locals 7
    .param p0, "fdPath"    # Ljava/io/File;

    .line 305
    const-string v0, ""

    const-string v1, "getFdOwner: "

    const-string v2, "ScoutMemoryUtils"

    :try_start_0
    invoke-virtual {p0}, Ljava/io/File;->getName()Ljava/lang/String;

    move-result-object v3

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(Ljava/lang/String;)Ljava/lang/Integer;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/Integer;->intValue()I

    move-result v3
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_1

    .line 309
    .local v3, "rawFd":I
    nop

    .line 311
    :try_start_1
    new-instance v4, Ljava/io/FileDescriptor;

    invoke-direct {v4}, Ljava/io/FileDescriptor;-><init>()V

    .line 312
    .local v4, "fd":Ljava/io/FileDescriptor;
    invoke-virtual {v4, v3}, Ljava/io/FileDescriptor;->setInt$(I)V

    .line 313
    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, " ("

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-static {v4}, Lcom/miui/server/stability/ScoutMemoryUtils;->getFdOwnerImpl(Ljava/io/FileDescriptor;)Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, ") "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_0

    return-object v0

    .line 314
    .end local v4    # "fd":Ljava/io/FileDescriptor;
    :catch_0
    move-exception v4

    .line 315
    .local v4, "e":Ljava/lang/Exception;
    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v5, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v2, v1, v4}, Landroid/util/Slog;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    .line 316
    return-object v0

    .line 306
    .end local v3    # "rawFd":I
    .end local v4    # "e":Ljava/lang/Exception;
    :catch_1
    move-exception v3

    .line 307
    .local v3, "e":Ljava/lang/Exception;
    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v4, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v4, " is not a valid fd path"

    invoke-virtual {v1, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v2, v1, v3}, Landroid/util/Slog;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    .line 308
    return-object v0
.end method

.method private static getFdOwnerImpl(Ljava/io/FileDescriptor;)Ljava/lang/String;
    .locals 8
    .param p0, "fd"    # Ljava/io/FileDescriptor;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    .line 285
    invoke-static {}, Llibcore/io/Os;->getDefault()Llibcore/io/Os;

    move-result-object v0

    .line 287
    .local v0, "os":Llibcore/io/Os;
    sget-object v1, Lcom/miui/server/stability/ScoutMemoryUtils;->sGetTagMethod:Ljava/lang/reflect/Method;

    if-nez v1, :cond_0

    .line 288
    const-class v1, Llibcore/io/Os;

    const-class v2, Ljava/io/FileDescriptor;

    filled-new-array {v2}, [Ljava/lang/Class;

    move-result-object v2

    const-string v3, "android_fdsan_get_owner_tag"

    invoke-virtual {v1, v3, v2}, Ljava/lang/Class;->getDeclaredMethod(Ljava/lang/String;[Ljava/lang/Class;)Ljava/lang/reflect/Method;

    move-result-object v1

    sput-object v1, Lcom/miui/server/stability/ScoutMemoryUtils;->sGetTagMethod:Ljava/lang/reflect/Method;

    .line 290
    const-class v1, Llibcore/io/Os;

    sget-object v2, Ljava/lang/Long;->TYPE:Ljava/lang/Class;

    filled-new-array {v2}, [Ljava/lang/Class;

    move-result-object v2

    const-string v3, "android_fdsan_get_tag_type"

    invoke-virtual {v1, v3, v2}, Ljava/lang/Class;->getDeclaredMethod(Ljava/lang/String;[Ljava/lang/Class;)Ljava/lang/reflect/Method;

    move-result-object v1

    sput-object v1, Lcom/miui/server/stability/ScoutMemoryUtils;->sGetTagTypeMethod:Ljava/lang/reflect/Method;

    .line 292
    const-class v1, Llibcore/io/Os;

    sget-object v2, Ljava/lang/Long;->TYPE:Ljava/lang/Class;

    filled-new-array {v2}, [Ljava/lang/Class;

    move-result-object v2

    const-string v3, "android_fdsan_get_tag_value"

    invoke-virtual {v1, v3, v2}, Ljava/lang/Class;->getDeclaredMethod(Ljava/lang/String;[Ljava/lang/Class;)Ljava/lang/reflect/Method;

    move-result-object v1

    sput-object v1, Lcom/miui/server/stability/ScoutMemoryUtils;->sGetTagValueMethod:Ljava/lang/reflect/Method;

    .line 295
    :cond_0
    sget-object v1, Lcom/miui/server/stability/ScoutMemoryUtils;->sGetTagMethod:Ljava/lang/reflect/Method;

    filled-new-array {p0}, [Ljava/lang/Object;

    move-result-object v2

    invoke-virtual {v1, v0, v2}, Ljava/lang/reflect/Method;->invoke(Ljava/lang/Object;[Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/Long;

    invoke-virtual {v1}, Ljava/lang/Long;->longValue()J

    move-result-wide v1

    .line 296
    .local v1, "tag":J
    const-wide/16 v3, 0x0

    cmp-long v3, v1, v3

    if-nez v3, :cond_1

    const-string/jumbo v3, "unowned"

    return-object v3

    .line 297
    :cond_1
    sget-object v3, Lcom/miui/server/stability/ScoutMemoryUtils;->sGetTagTypeMethod:Ljava/lang/reflect/Method;

    invoke-static {v1, v2}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v4

    filled-new-array {v4}, [Ljava/lang/Object;

    move-result-object v4

    invoke-virtual {v3, v0, v4}, Ljava/lang/reflect/Method;->invoke(Ljava/lang/Object;[Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/lang/String;

    .line 298
    .local v3, "type":Ljava/lang/String;
    sget-object v4, Lcom/miui/server/stability/ScoutMemoryUtils;->sGetTagValueMethod:Ljava/lang/reflect/Method;

    invoke-static {v1, v2}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v5

    filled-new-array {v5}, [Ljava/lang/Object;

    move-result-object v5

    invoke-virtual {v4, v0, v5}, Ljava/lang/reflect/Method;->invoke(Ljava/lang/Object;[Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Ljava/lang/Long;

    invoke-virtual {v4}, Ljava/lang/Long;->longValue()J

    move-result-wide v4

    .line 299
    .local v4, "value":J
    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string v7, "owned by "

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    const-string v7, " 0x"

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-static {v4, v5}, Ljava/lang/Long;->toHexString(J)Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    return-object v6
.end method

.method private static getMemLeakDir(I)Ljava/io/File;
    .locals 9
    .param p0, "dirId"    # I

    .line 90
    sget-object v0, Lcom/miui/server/stability/ScoutMemoryUtils;->dirMap:Ljava/util/HashMap;

    invoke-static {p0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/util/HashMap;->containsKey(Ljava/lang/Object;)Z

    move-result v1

    const/4 v2, 0x0

    const-string v3, "ScoutMemoryUtils"

    if-nez v1, :cond_0

    .line 91
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "invalid dir id : "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v3, v0}, Landroid/util/Slog;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 92
    return-object v2

    .line 94
    :cond_0
    new-instance v1, Ljava/io/File;

    const-string v4, "/data/miuilog/stability"

    invoke-direct {v1, v4}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 95
    .local v1, "stDir":Ljava/io/File;
    invoke-virtual {v1}, Ljava/io/File;->exists()Z

    move-result v4

    if-nez v4, :cond_1

    .line 96
    const-string/jumbo v0, "stability log dir isn\'t exists"

    invoke-static {v3, v0}, Landroid/util/Slog;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 97
    return-object v2

    .line 99
    :cond_1
    new-instance v4, Ljava/io/File;

    const-string v5, "memleak"

    invoke-direct {v4, v1, v5}, Ljava/io/File;-><init>(Ljava/io/File;Ljava/lang/String;)V

    .line 100
    .local v4, "memleakDir":Ljava/io/File;
    invoke-virtual {v4}, Ljava/io/File;->exists()Z

    move-result v5

    const/16 v6, 0x1ff

    const/4 v7, -0x1

    if-nez v5, :cond_3

    .line 101
    invoke-virtual {v4}, Ljava/io/File;->mkdirs()Z

    move-result v5

    if-nez v5, :cond_2

    .line 102
    const-string v0, "Cannot create memleak dir"

    invoke-static {v3, v0}, Landroid/util/Slog;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 103
    return-object v2

    .line 105
    :cond_2
    nop

    .line 106
    invoke-virtual {v4}, Ljava/io/File;->toString()Ljava/lang/String;

    move-result-object v5

    .line 105
    invoke-static {v5, v6, v7, v7}, Landroid/os/FileUtils;->setPermissions(Ljava/lang/String;III)I

    .line 109
    const-string v5, "mkdir memleak dir"

    invoke-static {v3, v5}, Landroid/util/Slog;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 111
    :cond_3
    new-instance v5, Ljava/io/File;

    invoke-static {p0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v8

    invoke-virtual {v0, v8}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v8

    check-cast v8, Ljava/lang/String;

    invoke-direct {v5, v4, v8}, Ljava/io/File;-><init>(Ljava/io/File;Ljava/lang/String;)V

    .line 112
    .local v5, "exDir":Ljava/io/File;
    invoke-virtual {v5}, Ljava/io/File;->exists()Z

    move-result v8

    if-nez v8, :cond_5

    .line 113
    invoke-virtual {v5}, Ljava/io/File;->mkdirs()Z

    move-result v8

    if-nez v8, :cond_4

    .line 114
    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string v7, "Cannot create memleak dir "

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-static {p0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v7

    invoke-virtual {v0, v7}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    invoke-virtual {v6, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v3, v0}, Landroid/util/Slog;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 115
    return-object v2

    .line 117
    :cond_4
    nop

    .line 118
    invoke-virtual {v5}, Ljava/io/File;->toString()Ljava/lang/String;

    move-result-object v0

    .line 117
    invoke-static {v0, v6, v7, v7}, Landroid/os/FileUtils;->setPermissions(Ljava/lang/String;III)I

    .line 123
    :cond_5
    return-object v5
.end method

.method public static triggerCrash()V
    .locals 1

    .line 360
    const/16 v0, 0x63

    invoke-static {v0}, Lcom/android/server/ScoutHelper;->doSysRqInterface(C)V

    .line 361
    return-void
.end method
