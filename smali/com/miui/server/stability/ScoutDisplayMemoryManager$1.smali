.class Lcom/miui/server/stability/ScoutDisplayMemoryManager$1;
.super Ljava/lang/Object;
.source "ScoutDisplayMemoryManager.java"

# interfaces
.implements Ljava/util/Comparator;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/miui/server/stability/ScoutDisplayMemoryManager;->getDmabufUsageInfo()Ljava/lang/String;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Ljava/util/Comparator<",
        "Lcom/miui/server/stability/DmaBufProcUsageInfo;",
        ">;"
    }
.end annotation


# instance fields
.field final synthetic this$0:Lcom/miui/server/stability/ScoutDisplayMemoryManager;


# direct methods
.method constructor <init>(Lcom/miui/server/stability/ScoutDisplayMemoryManager;)V
    .locals 0
    .param p1, "this$0"    # Lcom/miui/server/stability/ScoutDisplayMemoryManager;

    .line 183
    iput-object p1, p0, Lcom/miui/server/stability/ScoutDisplayMemoryManager$1;->this$0:Lcom/miui/server/stability/ScoutDisplayMemoryManager;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public compare(Lcom/miui/server/stability/DmaBufProcUsageInfo;Lcom/miui/server/stability/DmaBufProcUsageInfo;)I
    .locals 4
    .param p1, "o1"    # Lcom/miui/server/stability/DmaBufProcUsageInfo;
    .param p2, "o2"    # Lcom/miui/server/stability/DmaBufProcUsageInfo;

    .line 186
    invoke-virtual {p2}, Lcom/miui/server/stability/DmaBufProcUsageInfo;->getRss()J

    move-result-wide v0

    invoke-virtual {p1}, Lcom/miui/server/stability/DmaBufProcUsageInfo;->getRss()J

    move-result-wide v2

    cmp-long v0, v0, v2

    if-nez v0, :cond_0

    .line 187
    const/4 v0, 0x0

    return v0

    .line 188
    :cond_0
    invoke-virtual {p2}, Lcom/miui/server/stability/DmaBufProcUsageInfo;->getRss()J

    move-result-wide v0

    invoke-virtual {p1}, Lcom/miui/server/stability/DmaBufProcUsageInfo;->getRss()J

    move-result-wide v2

    cmp-long v0, v0, v2

    if-lez v0, :cond_1

    .line 189
    const/4 v0, 0x1

    return v0

    .line 191
    :cond_1
    const/4 v0, -0x1

    return v0
.end method

.method public bridge synthetic compare(Ljava/lang/Object;Ljava/lang/Object;)I
    .locals 0

    .line 183
    check-cast p1, Lcom/miui/server/stability/DmaBufProcUsageInfo;

    check-cast p2, Lcom/miui/server/stability/DmaBufProcUsageInfo;

    invoke-virtual {p0, p1, p2}, Lcom/miui/server/stability/ScoutDisplayMemoryManager$1;->compare(Lcom/miui/server/stability/DmaBufProcUsageInfo;Lcom/miui/server/stability/DmaBufProcUsageInfo;)I

    move-result p1

    return p1
.end method
