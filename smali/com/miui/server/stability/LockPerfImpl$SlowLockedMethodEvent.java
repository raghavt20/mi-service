class com.miui.server.stability.LockPerfImpl$SlowLockedMethodEvent {
	 /* .source "LockPerfImpl.java" */
	 /* # annotations */
	 /* .annotation system Ldalvik/annotation/EnclosingClass; */
	 /* value = Lcom/miui/server/stability/LockPerfImpl; */
} // .end annotation
/* .annotation system Ldalvik/annotation/InnerClass; */
/* accessFlags = 0xa */
/* name = "SlowLockedMethodEvent" */
} // .end annotation
/* # static fields */
private static final java.time.format.DateTimeFormatter DATE_TIME_FORMATTER;
/* # instance fields */
private final java.time.LocalDateTime datetime;
private final java.util.List perfStackSnapshot;
/* .annotation system Ldalvik/annotation/Signature; */
/* value = { */
/* "Ljava/util/List<", */
/* "Lcom/miui/server/stability/LockPerfImpl$PerfInfo;", */
/* ">;" */
/* } */
} // .end annotation
} // .end field
private final java.lang.String stackTraceLines;
private final Long tid;
private final java.lang.String tname;
private final java.lang.StackTraceElement unwindingStack;
/* # direct methods */
static java.time.LocalDateTime -$$Nest$fgetdatetime ( com.miui.server.stability.LockPerfImpl$SlowLockedMethodEvent p0 ) { //bridge//synthethic
/* .locals 0 */
p0 = this.datetime;
} // .end method
static Long -$$Nest$fgettid ( com.miui.server.stability.LockPerfImpl$SlowLockedMethodEvent p0 ) { //bridge//synthethic
/* .locals 2 */
/* iget-wide v0, p0, Lcom/miui/server/stability/LockPerfImpl$SlowLockedMethodEvent;->tid:J */
/* return-wide v0 */
} // .end method
static java.lang.String -$$Nest$fgettname ( com.miui.server.stability.LockPerfImpl$SlowLockedMethodEvent p0 ) { //bridge//synthethic
/* .locals 0 */
p0 = this.tname;
} // .end method
static java.lang.String -$$Nest$mformattedDatetime ( com.miui.server.stability.LockPerfImpl$SlowLockedMethodEvent p0 ) { //bridge//synthethic
/* .locals 0 */
/* invoke-direct {p0}, Lcom/miui/server/stability/LockPerfImpl$SlowLockedMethodEvent;->formattedDatetime()Ljava/lang/String; */
} // .end method
static java.lang.String -$$Nest$mgetStackTraceLines ( com.miui.server.stability.LockPerfImpl$SlowLockedMethodEvent p0 ) { //bridge//synthethic
/* .locals 0 */
/* invoke-direct {p0}, Lcom/miui/server/stability/LockPerfImpl$SlowLockedMethodEvent;->getStackTraceLines()[Ljava/lang/String; */
} // .end method
static void -$$Nest$mprintToLogcat ( com.miui.server.stability.LockPerfImpl$SlowLockedMethodEvent p0 ) { //bridge//synthethic
/* .locals 0 */
/* invoke-direct {p0}, Lcom/miui/server/stability/LockPerfImpl$SlowLockedMethodEvent;->printToLogcat()V */
return;
} // .end method
static com.miui.server.stability.LockPerfImpl$SlowLockedMethodEvent ( ) {
/* .locals 1 */
/* .line 234 */
/* nop */
/* .line 235 */
/* const-string/jumbo v0, "uu-MM-dd HH:mm:ss.SSS" */
java.time.format.DateTimeFormatter .ofPattern ( v0 );
/* .line 234 */
return;
} // .end method
private com.miui.server.stability.LockPerfImpl$SlowLockedMethodEvent ( ) {
/* .locals 3 */
/* .param p2, "unwindingStack" # [Ljava/lang/StackTraceElement; */
/* .param p3, "tid" # J */
/* .param p5, "tname" # Ljava/lang/String; */
/* .param p6, "datetime" # Ljava/time/LocalDateTime; */
/* .annotation system Ldalvik/annotation/Signature; */
/* value = { */
/* "(", */
/* "Lcom/miui/server/stability/LockPerfImpl$PerfStack<", */
/* "Lcom/miui/server/stability/LockPerfImpl$PerfInfo;", */
/* ">;[", */
/* "Ljava/lang/StackTraceElement;", */
/* "J", */
/* "Ljava/lang/String;", */
/* "Ljava/time/LocalDateTime;", */
/* ")V" */
/* } */
} // .end annotation
/* .line 246 */
/* .local p1, "perfStack":Lcom/miui/server/stability/LockPerfImpl$PerfStack;, "Lcom/miui/server/stability/LockPerfImpl$PerfStack<Lcom/miui/server/stability/LockPerfImpl$PerfInfo;>;" */
/* invoke-direct {p0}, Ljava/lang/Object;-><init>()V */
/* .line 240 */
/* new-instance v0, Ljava/util/ArrayList; */
/* invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V */
this.perfStackSnapshot = v0;
/* .line 247 */
/* iput-wide p3, p0, Lcom/miui/server/stability/LockPerfImpl$SlowLockedMethodEvent;->tid:J */
/* .line 248 */
this.tname = p5;
/* .line 249 */
this.unwindingStack = p2;
/* .line 250 */
v0 = (( com.miui.server.stability.LockPerfImpl$PerfStack ) p1 ).size ( ); // invoke-virtual {p1}, Lcom/miui/server/stability/LockPerfImpl$PerfStack;->size()I
/* add-int/lit8 v0, v0, -0x1 */
/* .local v0, "i":I */
} // :goto_0
/* if-ltz v0, :cond_0 */
/* .line 251 */
v1 = this.perfStackSnapshot;
(( com.miui.server.stability.LockPerfImpl$PerfStack ) p1 ).get ( v0 ); // invoke-virtual {p1, v0}, Lcom/miui/server/stability/LockPerfImpl$PerfStack;->get(I)Ljava/lang/Object;
/* check-cast v2, Lcom/miui/server/stability/LockPerfImpl$PerfInfo; */
/* .line 250 */
/* add-int/lit8 v0, v0, -0x1 */
/* .line 253 */
} // .end local v0 # "i":I
} // :cond_0
this.datetime = p6;
/* .line 254 */
/* invoke-direct {p0}, Lcom/miui/server/stability/LockPerfImpl$SlowLockedMethodEvent;->initStackTraceLines()[Ljava/lang/String; */
this.stackTraceLines = v0;
/* .line 255 */
return;
} // .end method
 com.miui.server.stability.LockPerfImpl$SlowLockedMethodEvent ( ) { //synthethic
/* .locals 0 */
/* invoke-direct/range {p0 ..p6}, Lcom/miui/server/stability/LockPerfImpl$SlowLockedMethodEvent;-><init>(Lcom/miui/server/stability/LockPerfImpl$PerfStack;[Ljava/lang/StackTraceElement;JLjava/lang/String;Ljava/time/LocalDateTime;)V */
return;
} // .end method
private java.lang.String formattedDatetime ( ) {
/* .locals 2 */
/* .line 258 */
v0 = this.datetime;
v1 = com.miui.server.stability.LockPerfImpl$SlowLockedMethodEvent.DATE_TIME_FORMATTER;
(( java.time.LocalDateTime ) v0 ).format ( v1 ); // invoke-virtual {v0, v1}, Ljava/time/LocalDateTime;->format(Ljava/time/format/DateTimeFormatter;)Ljava/lang/String;
} // .end method
private java.lang.String getStackTraceLines ( ) {
/* .locals 1 */
/* .line 262 */
v0 = this.stackTraceLines;
} // .end method
private java.lang.String initStackTraceLines ( ) {
/* .locals 9 */
/* .line 267 */
v0 = this.unwindingStack;
/* .line 268 */
/* .local v0, "stackTrace":[Ljava/lang/StackTraceElement; */
/* array-length v1, v0 */
/* new-array v1, v1, [Ljava/lang/String; */
/* .line 269 */
/* .local v1, "lines":[Ljava/lang/String; */
int v2 = 0; // const/4 v2, 0x0
/* .line 270 */
/* .local v2, "currPerfIdx":I */
v3 = this.perfStackSnapshot;
/* check-cast v3, Lcom/miui/server/stability/LockPerfImpl$PerfInfo; */
/* .line 271 */
/* .local v3, "perfInfo":Lcom/miui/server/stability/LockPerfImpl$PerfInfo; */
int v4 = 0; // const/4 v4, 0x0
/* .local v4, "i":I */
} // :goto_0
/* array-length v5, v0 */
/* if-ge v4, v5, :cond_1 */
/* .line 272 */
/* aget-object v5, v0, v4 */
/* .line 273 */
/* .local v5, "stackElement":Ljava/lang/StackTraceElement; */
/* new-instance v6, Ljava/lang/StringBuilder; */
/* invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V */
/* .line 274 */
/* .local v6, "sb":Ljava/lang/StringBuilder; */
java.lang.Integer .valueOf ( v4 );
/* filled-new-array {v7}, [Ljava/lang/Object; */
final String v8 = "#%02d "; // const-string v8, "#%02d "
java.lang.String .format ( v8,v7 );
(( java.lang.StringBuilder ) v6 ).append ( v7 ); // invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
/* .line 275 */
(( java.lang.StackTraceElement ) v5 ).toString ( ); // invoke-virtual {v5}, Ljava/lang/StackTraceElement;->toString()Ljava/lang/String;
(( java.lang.StringBuilder ) v6 ).append ( v7 ); // invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
/* .line 276 */
v7 = v7 = this.perfStackSnapshot;
/* if-ge v2, v7, :cond_0 */
/* .line 277 */
v7 = com.miui.server.stability.LockPerfImpl$PerfInfo .-$$Nest$mmatchStackTraceElement ( v3,v5 );
if ( v7 != null) { // if-eqz v7, :cond_0
/* .line 278 */
final String v7 = " "; // const-string v7, " "
(( java.lang.StringBuilder ) v6 ).append ( v7 ); // invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
com.miui.server.stability.LockPerfImpl$PerfInfo .-$$Nest$mgetDurationString ( v3 );
(( java.lang.StringBuilder ) v7 ).append ( v8 ); // invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
/* .line 279 */
/* add-int/lit8 v2, v2, 0x1 */
v7 = v7 = this.perfStackSnapshot;
/* if-ge v2, v7, :cond_0 */
/* .line 280 */
v7 = this.perfStackSnapshot;
/* move-object v3, v7 */
/* check-cast v3, Lcom/miui/server/stability/LockPerfImpl$PerfInfo; */
/* .line 284 */
} // :cond_0
(( java.lang.StringBuilder ) v6 ).toString ( ); // invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
/* aput-object v7, v1, v4 */
/* .line 271 */
} // .end local v5 # "stackElement":Ljava/lang/StackTraceElement;
} // .end local v6 # "sb":Ljava/lang/StringBuilder;
/* add-int/lit8 v4, v4, 0x1 */
/* .line 286 */
} // .end local v4 # "i":I
} // :cond_1
} // .end method
private void printToLogcat ( ) {
/* .locals 7 */
/* .line 290 */
com.miui.server.stability.LockPerfImpl .-$$Nest$sfgetTAG ( );
/* new-instance v1, Ljava/lang/StringBuilder; */
/* invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V */
final String v2 = "Long locked method event.Thread: "; // const-string v2, "Long locked method event.Thread: "
(( java.lang.StringBuilder ) v1 ).append ( v2 ); // invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
v2 = this.tname;
(( java.lang.StringBuilder ) v1 ).append ( v2 ); // invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
final String v2 = ".Full stack trace: "; // const-string v2, ".Full stack trace: "
(( java.lang.StringBuilder ) v1 ).append ( v2 ); // invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v1 ).toString ( ); // invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
android.util.Slog .w ( v0,v1 );
/* .line 291 */
/* invoke-direct {p0}, Lcom/miui/server/stability/LockPerfImpl$SlowLockedMethodEvent;->getStackTraceLines()[Ljava/lang/String; */
/* array-length v1, v0 */
int v2 = 0; // const/4 v2, 0x0
} // :goto_0
/* if-ge v2, v1, :cond_0 */
/* aget-object v3, v0, v2 */
/* .line 292 */
/* .local v3, "line":Ljava/lang/String; */
com.miui.server.stability.LockPerfImpl .-$$Nest$sfgetTAG ( );
/* new-instance v5, Ljava/lang/StringBuilder; */
/* invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V */
final String v6 = "\t "; // const-string v6, "\t "
(( java.lang.StringBuilder ) v5 ).append ( v6 ); // invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v5 ).append ( v3 ); // invoke-virtual {v5, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v5 ).toString ( ); // invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
android.util.Slog .w ( v4,v5 );
/* .line 291 */
} // .end local v3 # "line":Ljava/lang/String;
/* add-int/lit8 v2, v2, 0x1 */
/* .line 294 */
} // :cond_0
return;
} // .end method
