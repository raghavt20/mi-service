.class public Lcom/miui/server/stability/ScoutDisplayMemoryManager$DiaplayMemoryErrorInfo;
.super Ljava/lang/Object;
.source "ScoutDisplayMemoryManager.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/miui/server/stability/ScoutDisplayMemoryManager;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x9
    name = "DiaplayMemoryErrorInfo"
.end annotation


# instance fields
.field private action:I

.field private adj:I

.field private pid:I

.field private procName:Ljava/lang/String;

.field private reason:Ljava/lang/String;

.field private rss:J

.field private threshold:J

.field private totalSize:J

.field private type:I


# direct methods
.method public constructor <init>(Lcom/miui/server/stability/DmaBufProcUsageInfo;JJ)V
    .locals 2
    .param p1, "dmabufInfo"    # Lcom/miui/server/stability/DmaBufProcUsageInfo;
    .param p2, "thresholdSize"    # J
    .param p4, "totalSize"    # J

    .line 806
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 807
    invoke-virtual {p1}, Lcom/miui/server/stability/DmaBufProcUsageInfo;->getPid()I

    move-result v0

    iput v0, p0, Lcom/miui/server/stability/ScoutDisplayMemoryManager$DiaplayMemoryErrorInfo;->pid:I

    .line 808
    invoke-virtual {p1}, Lcom/miui/server/stability/DmaBufProcUsageInfo;->getName()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/miui/server/stability/ScoutDisplayMemoryManager$DiaplayMemoryErrorInfo;->procName:Ljava/lang/String;

    .line 809
    invoke-virtual {p1}, Lcom/miui/server/stability/DmaBufProcUsageInfo;->getRss()J

    move-result-wide v0

    iput-wide v0, p0, Lcom/miui/server/stability/ScoutDisplayMemoryManager$DiaplayMemoryErrorInfo;->rss:J

    .line 810
    invoke-virtual {p1}, Lcom/miui/server/stability/DmaBufProcUsageInfo;->getOomadj()I

    move-result v0

    iput v0, p0, Lcom/miui/server/stability/ScoutDisplayMemoryManager$DiaplayMemoryErrorInfo;->adj:I

    .line 811
    iput-wide p2, p0, Lcom/miui/server/stability/ScoutDisplayMemoryManager$DiaplayMemoryErrorInfo;->threshold:J

    .line 812
    iput-wide p4, p0, Lcom/miui/server/stability/ScoutDisplayMemoryManager$DiaplayMemoryErrorInfo;->totalSize:J

    .line 813
    const-string v0, "DMA-BUF"

    iput-object v0, p0, Lcom/miui/server/stability/ScoutDisplayMemoryManager$DiaplayMemoryErrorInfo;->reason:Ljava/lang/String;

    .line 814
    const/4 v0, 0x1

    iput v0, p0, Lcom/miui/server/stability/ScoutDisplayMemoryManager$DiaplayMemoryErrorInfo;->type:I

    .line 815
    return-void
.end method

.method public constructor <init>(Lcom/miui/server/stability/GpuMemoryProcUsageInfo;JJ)V
    .locals 2
    .param p1, "gpuMemoryInfo"    # Lcom/miui/server/stability/GpuMemoryProcUsageInfo;
    .param p2, "thresholdSize"    # J
    .param p4, "totalSize"    # J

    .line 794
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 795
    invoke-virtual {p1}, Lcom/miui/server/stability/GpuMemoryProcUsageInfo;->getPid()I

    move-result v0

    iput v0, p0, Lcom/miui/server/stability/ScoutDisplayMemoryManager$DiaplayMemoryErrorInfo;->pid:I

    .line 796
    invoke-virtual {p1}, Lcom/miui/server/stability/GpuMemoryProcUsageInfo;->getName()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/miui/server/stability/ScoutDisplayMemoryManager$DiaplayMemoryErrorInfo;->procName:Ljava/lang/String;

    .line 797
    invoke-virtual {p1}, Lcom/miui/server/stability/GpuMemoryProcUsageInfo;->getRss()J

    move-result-wide v0

    iput-wide v0, p0, Lcom/miui/server/stability/ScoutDisplayMemoryManager$DiaplayMemoryErrorInfo;->rss:J

    .line 798
    invoke-virtual {p1}, Lcom/miui/server/stability/GpuMemoryProcUsageInfo;->getOomadj()I

    move-result v0

    iput v0, p0, Lcom/miui/server/stability/ScoutDisplayMemoryManager$DiaplayMemoryErrorInfo;->adj:I

    .line 799
    iput-wide p2, p0, Lcom/miui/server/stability/ScoutDisplayMemoryManager$DiaplayMemoryErrorInfo;->threshold:J

    .line 800
    iput-wide p4, p0, Lcom/miui/server/stability/ScoutDisplayMemoryManager$DiaplayMemoryErrorInfo;->totalSize:J

    .line 801
    const-string v0, "GpuMemory"

    iput-object v0, p0, Lcom/miui/server/stability/ScoutDisplayMemoryManager$DiaplayMemoryErrorInfo;->reason:Ljava/lang/String;

    .line 802
    const/4 v0, 0x2

    iput v0, p0, Lcom/miui/server/stability/ScoutDisplayMemoryManager$DiaplayMemoryErrorInfo;->type:I

    .line 803
    return-void
.end method


# virtual methods
.method public getAction()I
    .locals 1

    .line 878
    iget v0, p0, Lcom/miui/server/stability/ScoutDisplayMemoryManager$DiaplayMemoryErrorInfo;->action:I

    return v0
.end method

.method public getName()Ljava/lang/String;
    .locals 1

    .line 822
    iget-object v0, p0, Lcom/miui/server/stability/ScoutDisplayMemoryManager$DiaplayMemoryErrorInfo;->procName:Ljava/lang/String;

    return-object v0
.end method

.method public getOomadj()I
    .locals 1

    .line 838
    iget v0, p0, Lcom/miui/server/stability/ScoutDisplayMemoryManager$DiaplayMemoryErrorInfo;->adj:I

    return v0
.end method

.method public getPid()I
    .locals 1

    .line 830
    iget v0, p0, Lcom/miui/server/stability/ScoutDisplayMemoryManager$DiaplayMemoryErrorInfo;->pid:I

    return v0
.end method

.method public getReason()Ljava/lang/String;
    .locals 1

    .line 870
    iget-object v0, p0, Lcom/miui/server/stability/ScoutDisplayMemoryManager$DiaplayMemoryErrorInfo;->reason:Ljava/lang/String;

    return-object v0
.end method

.method public getRss()J
    .locals 2

    .line 846
    iget-wide v0, p0, Lcom/miui/server/stability/ScoutDisplayMemoryManager$DiaplayMemoryErrorInfo;->rss:J

    return-wide v0
.end method

.method public getThreshold()J
    .locals 2

    .line 854
    iget-wide v0, p0, Lcom/miui/server/stability/ScoutDisplayMemoryManager$DiaplayMemoryErrorInfo;->threshold:J

    return-wide v0
.end method

.method public getTotalSize()J
    .locals 2

    .line 862
    iget-wide v0, p0, Lcom/miui/server/stability/ScoutDisplayMemoryManager$DiaplayMemoryErrorInfo;->totalSize:J

    return-wide v0
.end method

.method public getType()I
    .locals 1

    .line 882
    iget v0, p0, Lcom/miui/server/stability/ScoutDisplayMemoryManager$DiaplayMemoryErrorInfo;->type:I

    return v0
.end method

.method public setAction(I)V
    .locals 0
    .param p1, "action"    # I

    .line 874
    iput p1, p0, Lcom/miui/server/stability/ScoutDisplayMemoryManager$DiaplayMemoryErrorInfo;->action:I

    .line 875
    return-void
.end method

.method public setName(Ljava/lang/String;)V
    .locals 0
    .param p1, "procName"    # Ljava/lang/String;

    .line 818
    iput-object p1, p0, Lcom/miui/server/stability/ScoutDisplayMemoryManager$DiaplayMemoryErrorInfo;->procName:Ljava/lang/String;

    .line 819
    return-void
.end method

.method public setOomadj(I)V
    .locals 0
    .param p1, "adj"    # I

    .line 834
    iput p1, p0, Lcom/miui/server/stability/ScoutDisplayMemoryManager$DiaplayMemoryErrorInfo;->adj:I

    .line 835
    return-void
.end method

.method public setPid(I)V
    .locals 0
    .param p1, "pid"    # I

    .line 826
    iput p1, p0, Lcom/miui/server/stability/ScoutDisplayMemoryManager$DiaplayMemoryErrorInfo;->pid:I

    .line 827
    return-void
.end method

.method public setReason(Ljava/lang/String;)V
    .locals 0
    .param p1, "reason"    # Ljava/lang/String;

    .line 866
    iput-object p1, p0, Lcom/miui/server/stability/ScoutDisplayMemoryManager$DiaplayMemoryErrorInfo;->reason:Ljava/lang/String;

    .line 867
    return-void
.end method

.method public setRss(J)V
    .locals 0
    .param p1, "rss"    # J

    .line 842
    iput-wide p1, p0, Lcom/miui/server/stability/ScoutDisplayMemoryManager$DiaplayMemoryErrorInfo;->rss:J

    .line 843
    return-void
.end method

.method public setThreshold(J)V
    .locals 0
    .param p1, "threshold"    # J

    .line 850
    iput-wide p1, p0, Lcom/miui/server/stability/ScoutDisplayMemoryManager$DiaplayMemoryErrorInfo;->threshold:J

    .line 851
    return-void
.end method

.method public setTotalSize(J)V
    .locals 0
    .param p1, "totalSize"    # J

    .line 858
    iput-wide p1, p0, Lcom/miui/server/stability/ScoutDisplayMemoryManager$DiaplayMemoryErrorInfo;->totalSize:J

    .line 859
    return-void
.end method
