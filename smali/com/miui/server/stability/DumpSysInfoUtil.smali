.class public Lcom/miui/server/stability/DumpSysInfoUtil;
.super Ljava/lang/Object;
.source "DumpSysInfoUtil.java"


# static fields
.field public static final ACTIVITY:Ljava/lang/String; = "activity activities"

.field public static final DIR_NAME:Ljava/lang/String; = "dumpsys_by_power_"

.field public static final DUMPSYS:Ljava/lang/String; = "dumpsys"

.field public static final DUMPSYS_FILE_NAME:Ljava/lang/String; = "dumpsys.txt"

.field public static final FILE_DIR_HANGLOG:Ljava/lang/String; = "hanglog"

.field public static final FILE_DIR_STABILITY:Ljava/lang/String; = "/data/miuilog/stability/"

.field public static final INPUT:Ljava/lang/String; = "input"

.field public static final LOGCAT:Ljava/lang/String; = "logcat"

.field public static final LOGCAT_FILE_NAME:Ljava/lang/String; = "logcat.txt"

.field private static final MAX_FREEZE_SCREEN_STUCK_FILE:I = 0x3

.field private static final MQSASD:Ljava/lang/String; = "miui.mqsas.IMQSNative"

.field public static final SURFACEFLINGER:Ljava/lang/String; = "SurfaceFlinger"

.field private static final TAG:Ljava/lang/String; = "DumpSysInfoUtil"

.field public static final WINDOW:Ljava/lang/String; = "window"

.field public static final ZIP_NAME:Ljava/lang/String; = "Freeze_Screen_Stuck"

.field private static mDaemon:Lmiui/mqsas/IMQSNative;

.field private static temporaryDir:Ljava/io/File;


# direct methods
.method static bridge synthetic -$$Nest$sfgettemporaryDir()Ljava/io/File;
    .locals 1

    sget-object v0, Lcom/miui/server/stability/DumpSysInfoUtil;->temporaryDir:Ljava/io/File;

    return-object v0
.end method

.method static bridge synthetic -$$Nest$smcreateFile(Ljava/lang/String;)Ljava/lang/String;
    .locals 0

    invoke-static {p0}, Lcom/miui/server/stability/DumpSysInfoUtil;->createFile(Ljava/lang/String;)Ljava/lang/String;

    move-result-object p0

    return-object p0
.end method

.method static bridge synthetic -$$Nest$smdeleteMissFetchByPower(Ljava/io/File;)V
    .locals 0

    invoke-static {p0}, Lcom/miui/server/stability/DumpSysInfoUtil;->deleteMissFetchByPower(Ljava/io/File;)V

    return-void
.end method

.method static bridge synthetic -$$Nest$smgetmDaemon()Lmiui/mqsas/IMQSNative;
    .locals 1

    invoke-static {}, Lcom/miui/server/stability/DumpSysInfoUtil;->getmDaemon()Lmiui/mqsas/IMQSNative;

    move-result-object v0

    return-object v0
.end method

.method public constructor <init>()V
    .locals 0

    .line 18
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static captureDumpLog()V
    .locals 2

    .line 57
    invoke-static {}, Lcom/android/server/MiuiBgThread;->getHandler()Landroid/os/Handler;

    move-result-object v0

    new-instance v1, Lcom/miui/server/stability/DumpSysInfoUtil$$ExternalSyntheticLambda0;

    invoke-direct {v1}, Lcom/miui/server/stability/DumpSysInfoUtil$$ExternalSyntheticLambda0;-><init>()V

    invoke-virtual {v0, v1}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    .line 76
    return-void
.end method

.method private static crawlLogcat()V
    .locals 2

    .line 122
    new-instance v0, Ljava/lang/Thread;

    new-instance v1, Lcom/miui/server/stability/DumpSysInfoUtil$2;

    invoke-direct {v1}, Lcom/miui/server/stability/DumpSysInfoUtil$2;-><init>()V

    invoke-direct {v0, v1}, Ljava/lang/Thread;-><init>(Ljava/lang/Runnable;)V

    .line 139
    .local v0, "crawlLocat":Ljava/lang/Thread;
    invoke-virtual {v0}, Ljava/lang/Thread;->start()V

    .line 140
    return-void
.end method

.method public static crawlLogsByPower()V
    .locals 2

    .line 86
    const-string v0, "dumpsys_by_power_"

    invoke-static {v0}, Lcom/miui/server/stability/DumpSysInfoUtil;->deleteDumpSysFile(Ljava/lang/String;)V

    .line 92
    new-instance v0, Ljava/lang/Thread;

    new-instance v1, Lcom/miui/server/stability/DumpSysInfoUtil$1;

    invoke-direct {v1}, Lcom/miui/server/stability/DumpSysInfoUtil$1;-><init>()V

    invoke-direct {v0, v1}, Ljava/lang/Thread;-><init>(Ljava/lang/Runnable;)V

    .line 111
    .local v0, "crawlDumpsysInfo":Ljava/lang/Thread;
    invoke-virtual {v0}, Ljava/lang/Thread;->start()V

    .line 112
    return-void
.end method

.method private static declared-synchronized createDumpsysByPowerPath()Ljava/io/File;
    .locals 7

    const-class v0, Lcom/miui/server/stability/DumpSysInfoUtil;

    monitor-enter v0

    .line 213
    const/4 v1, 0x0

    :try_start_0
    invoke-static {}, Lcom/miui/server/stability/DumpSysInfoUtil;->getDumpSysFilePath()Ljava/io/File;

    move-result-object v2

    .line 214
    .local v2, "file":Ljava/io/File;
    new-instance v3, Ljava/text/SimpleDateFormat;

    const-string/jumbo v4, "yyyy-MM-dd-HH-mm-ss"

    invoke-direct {v3, v4}, Ljava/text/SimpleDateFormat;-><init>(Ljava/lang/String;)V

    .line 215
    .local v3, "dateFormat":Ljava/text/SimpleDateFormat;
    new-instance v4, Ljava/io/File;

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "dumpsys_by_power_"

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    new-instance v6, Ljava/util/Date;

    invoke-direct {v6}, Ljava/util/Date;-><init>()V

    invoke-virtual {v3, v6}, Ljava/text/SimpleDateFormat;->format(Ljava/util/Date;)Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-direct {v4, v2, v5}, Ljava/io/File;-><init>(Ljava/io/File;Ljava/lang/String;)V

    .line 217
    .local v4, "dumpsysPath":Ljava/io/File;
    if-eqz v2, :cond_1

    invoke-virtual {v4}, Ljava/io/File;->exists()Z

    move-result v5

    if-nez v5, :cond_0

    invoke-virtual {v4}, Ljava/io/File;->mkdirs()Z

    move-result v5

    if-nez v5, :cond_0

    goto :goto_0

    .line 223
    :cond_0
    const-string v5, "DumpSysInfoUtil"

    const-string v6, "mkdir dumpsysPath dir"

    invoke-static {v5, v6}, Landroid/util/Slog;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 224
    const/16 v5, 0x1fc

    const/4 v6, -0x1

    invoke-static {v4, v5, v6, v6}, Landroid/os/FileUtils;->setPermissions(Ljava/io/File;III)I

    .line 225
    sput-object v4, Lcom/miui/server/stability/DumpSysInfoUtil;->temporaryDir:Ljava/io/File;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 226
    monitor-exit v0

    return-object v4

    .line 218
    :cond_1
    :goto_0
    :try_start_1
    const-string v5, "DumpSysInfoUtil"

    const-string v6, "Cannot create dumpsysPath dir"

    invoke-static {v5, v6}, Landroid/util/Slog;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 219
    sput-object v1, Lcom/miui/server/stability/DumpSysInfoUtil;->temporaryDir:Ljava/io/File;
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_0
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 220
    monitor-exit v0

    return-object v1

    .line 212
    .end local v2    # "file":Ljava/io/File;
    .end local v3    # "dateFormat":Ljava/text/SimpleDateFormat;
    .end local v4    # "dumpsysPath":Ljava/io/File;
    :catchall_0
    move-exception v1

    goto :goto_1

    .line 227
    :catch_0
    move-exception v2

    .line 228
    .local v2, "e":Ljava/lang/Exception;
    :try_start_2
    const-string v3, "DumpSysInfoUtil"

    const-string v4, "crash in the createDumpsysByPowerPath()"

    invoke-static {v3, v4, v2}, Landroid/util/Slog;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    .line 229
    monitor-exit v0

    return-object v1

    .line 212
    .end local v2    # "e":Ljava/lang/Exception;
    :goto_1
    monitor-exit v0

    throw v1
.end method

.method private static createFile(Ljava/lang/String;)Ljava/lang/String;
    .locals 6
    .param p0, "mFileName"    # Ljava/lang/String;

    .line 235
    const-string v0, "DumpSysInfoUtil"

    const/4 v1, 0x0

    :try_start_0
    invoke-static {}, Lcom/miui/server/stability/DumpSysInfoUtil;->createDumpsysByPowerPath()Ljava/io/File;

    move-result-object v2

    .line 236
    .local v2, "dumpsysLogPath":Ljava/io/File;
    if-eqz v2, :cond_0

    .line 237
    new-instance v3, Ljava/io/File;

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v2}, Ljava/io/File;->getAbsolutePath()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, "/"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-direct {v3, v4}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 238
    .local v3, "file":Ljava/io/File;
    invoke-virtual {v3}, Ljava/io/File;->createNewFile()Z

    move-result v4

    if-eqz v4, :cond_0

    .line 239
    const/16 v4, 0x1fc

    const/4 v5, -0x1

    invoke-static {v3, v4, v5, v5}, Landroid/os/FileUtils;->setPermissions(Ljava/io/File;III)I

    .line 240
    const-string v4, "create file success"

    invoke-static {v0, v4}, Landroid/util/Slog;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 241
    invoke-virtual {v3}, Ljava/io/File;->getAbsolutePath()Ljava/lang/String;

    move-result-object v0

    return-object v0

    .line 244
    .end local v3    # "file":Ljava/io/File;
    :cond_0
    const-string v3, "create file fail"

    invoke-static {v0, v3}, Landroid/util/Slog;->e(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 245
    return-object v1

    .line 246
    .end local v2    # "dumpsysLogPath":Ljava/io/File;
    :catch_0
    move-exception v2

    .line 247
    .local v2, "e":Ljava/lang/Exception;
    const-string v3, "crash in the createFile()"

    invoke-static {v0, v3, v2}, Landroid/util/Slog;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    .line 248
    return-object v1
.end method

.method private static deleteDumpSysFile(Ljava/lang/String;)V
    .locals 8
    .param p0, "fileName"    # Ljava/lang/String;

    .line 165
    :try_start_0
    invoke-static {}, Lcom/miui/server/stability/DumpSysInfoUtil;->getDumpSysFilePath()Ljava/io/File;

    move-result-object v0

    .line 166
    .local v0, "hanglog":Ljava/io/File;
    new-instance v1, Ljava/util/TreeSet;

    invoke-direct {v1}, Ljava/util/TreeSet;-><init>()V

    .line 168
    .local v1, "existinglog":Ljava/util/TreeSet;, "Ljava/util/TreeSet<Ljava/io/File;>;"
    if-eqz v0, :cond_3

    invoke-virtual {v0}, Ljava/io/File;->exists()Z

    move-result v2

    if-eqz v2, :cond_3

    .line 169
    invoke-virtual {v0}, Ljava/io/File;->listFiles()[Ljava/io/File;

    move-result-object v2

    array-length v3, v2

    const/4 v4, 0x0

    move v5, v4

    :goto_0
    if-ge v5, v3, :cond_1

    aget-object v6, v2, v5

    .line 170
    .local v6, "file":Ljava/io/File;
    invoke-virtual {v6}, Ljava/io/File;->getName()Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v7, p0}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v7

    if-eqz v7, :cond_0

    .line 171
    invoke-virtual {v1, v6}, Ljava/util/TreeSet;->add(Ljava/lang/Object;)Z

    .line 169
    .end local v6    # "file":Ljava/io/File;
    :cond_0
    add-int/lit8 v5, v5, 0x1

    goto :goto_0

    .line 174
    :cond_1
    invoke-virtual {v1}, Ljava/util/TreeSet;->size()I

    move-result v2

    const/4 v3, 0x3

    if-lt v2, v3, :cond_3

    .line 175
    invoke-virtual {v1}, Ljava/util/TreeSet;->pollFirst()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/io/File;

    .line 176
    .local v2, "deleteFile":Ljava/io/File;
    invoke-virtual {v2}, Ljava/io/File;->getName()Ljava/lang/String;

    move-result-object v3

    const-string/jumbo v5, "zip"

    invoke-virtual {v3, v5}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v3

    if-nez v3, :cond_2

    .line 177
    invoke-virtual {v2}, Ljava/io/File;->listFiles()[Ljava/io/File;

    move-result-object v3

    array-length v5, v3

    :goto_1
    if-ge v4, v5, :cond_2

    aget-object v6, v3, v4

    .line 178
    .restart local v6    # "file":Ljava/io/File;
    invoke-virtual {v6}, Ljava/io/File;->delete()Z

    .line 177
    nop

    .end local v6    # "file":Ljava/io/File;
    add-int/lit8 v4, v4, 0x1

    goto :goto_1

    .line 181
    :cond_2
    invoke-virtual {v2}, Ljava/io/File;->delete()Z
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 186
    .end local v0    # "hanglog":Ljava/io/File;
    .end local v1    # "existinglog":Ljava/util/TreeSet;, "Ljava/util/TreeSet<Ljava/io/File;>;"
    .end local v2    # "deleteFile":Ljava/io/File;
    :cond_3
    goto :goto_2

    .line 184
    :catch_0
    move-exception v0

    .line 185
    .local v0, "e":Ljava/lang/Exception;
    const-string v1, "DumpSysInfoUtil"

    const-string v2, "crash in the deleteDumpSysFile()"

    invoke-static {v1, v2, v0}, Landroid/util/Slog;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    .line 187
    .end local v0    # "e":Ljava/lang/Exception;
    :goto_2
    return-void
.end method

.method private static deleteMissFetchByPower(Ljava/io/File;)V
    .locals 4
    .param p0, "filePath"    # Ljava/io/File;

    .line 149
    invoke-static {}, Lcom/android/server/MiuiBgThread;->getHandler()Landroid/os/Handler;

    move-result-object v0

    new-instance v1, Lcom/miui/server/stability/DumpSysInfoUtil$$ExternalSyntheticLambda1;

    invoke-direct {v1, p0}, Lcom/miui/server/stability/DumpSysInfoUtil$$ExternalSyntheticLambda1;-><init>(Ljava/io/File;)V

    const-wide/16 v2, 0x2710

    invoke-virtual {v0, v1, v2, v3}, Landroid/os/Handler;->postDelayed(Ljava/lang/Runnable;J)Z

    .line 161
    return-void
.end method

.method private static getDumpSysFilePath()Ljava/io/File;
    .locals 7

    .line 191
    const-string v0, "DumpSysInfoUtil"

    const/4 v1, 0x0

    :try_start_0
    new-instance v2, Ljava/io/File;

    const-string v3, "/data/miuilog/stability/"

    invoke-direct {v2, v3}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 192
    .local v2, "stabilityLog":Ljava/io/File;
    new-instance v3, Ljava/io/File;

    const-string v4, "hanglog"

    invoke-direct {v3, v2, v4}, Ljava/io/File;-><init>(Ljava/io/File;Ljava/lang/String;)V

    .line 193
    .local v3, "dumpsyslog":Ljava/io/File;
    invoke-virtual {v3}, Ljava/io/File;->exists()Z

    move-result v4

    if-nez v4, :cond_1

    .line 194
    invoke-virtual {v3}, Ljava/io/File;->mkdirs()Z

    move-result v4

    if-nez v4, :cond_0

    .line 195
    const-string v4, "Cannot create dumpsyslog dir"

    invoke-static {v0, v4}, Landroid/util/Slog;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 196
    return-object v1

    .line 198
    :cond_0
    nop

    .line 199
    invoke-virtual {v3}, Ljava/io/File;->toString()Ljava/lang/String;

    move-result-object v4

    .line 198
    const/4 v5, -0x1

    const/16 v6, 0x1ff

    invoke-static {v4, v6, v5, v5}, Landroid/os/FileUtils;->setPermissions(Ljava/lang/String;III)I

    .line 202
    const-string v4, "mkdir dumpsyslog dir"

    invoke-static {v0, v4}, Landroid/util/Slog;->d(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 204
    :cond_1
    return-object v3

    .line 205
    .end local v2    # "stabilityLog":Ljava/io/File;
    .end local v3    # "dumpsyslog":Ljava/io/File;
    :catch_0
    move-exception v2

    .line 206
    .local v2, "e":Ljava/lang/Exception;
    const-string v3, "crash in the getDumpSysFilePath()"

    invoke-static {v0, v3, v2}, Landroid/util/Slog;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    .line 207
    return-object v1
.end method

.method private static getmDaemon()Lmiui/mqsas/IMQSNative;
    .locals 2

    .line 43
    sget-object v0, Lcom/miui/server/stability/DumpSysInfoUtil;->mDaemon:Lmiui/mqsas/IMQSNative;

    if-nez v0, :cond_0

    .line 44
    const-string v0, "miui.mqsas.IMQSNative"

    invoke-static {v0}, Landroid/os/ServiceManager;->getService(Ljava/lang/String;)Landroid/os/IBinder;

    move-result-object v0

    invoke-static {v0}, Lmiui/mqsas/IMQSNative$Stub;->asInterface(Landroid/os/IBinder;)Lmiui/mqsas/IMQSNative;

    move-result-object v0

    sput-object v0, Lcom/miui/server/stability/DumpSysInfoUtil;->mDaemon:Lmiui/mqsas/IMQSNative;

    .line 45
    if-nez v0, :cond_0

    .line 46
    const-string v0, "DumpSysInfoUtil"

    const-string v1, "mqsasd not available!"

    invoke-static {v0, v1}, Landroid/util/Slog;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 49
    :cond_0
    sget-object v0, Lcom/miui/server/stability/DumpSysInfoUtil;->mDaemon:Lmiui/mqsas/IMQSNative;

    return-object v0
.end method

.method static synthetic lambda$captureDumpLog$0()V
    .locals 6

    .line 59
    const-string v0, "Freeze_Screen_Stuck"

    const-string v1, "dumpsys"

    :try_start_0
    invoke-static {v0}, Lcom/miui/server/stability/DumpSysInfoUtil;->deleteDumpSysFile(Ljava/lang/String;)V

    .line 61
    new-instance v2, Lcom/android/server/ScoutHelper$Action;

    invoke-direct {v2}, Lcom/android/server/ScoutHelper$Action;-><init>()V

    .line 62
    .local v2, "action":Lcom/android/server/ScoutHelper$Action;
    const-string v3, "SurfaceFlinger"

    invoke-virtual {v2, v1, v3}, Lcom/android/server/ScoutHelper$Action;->addActionAndParam(Ljava/lang/String;Ljava/lang/String;)V

    .line 63
    const-string v3, "activity activities"

    invoke-virtual {v2, v1, v3}, Lcom/android/server/ScoutHelper$Action;->addActionAndParam(Ljava/lang/String;Ljava/lang/String;)V

    .line 64
    const-string/jumbo v3, "window"

    invoke-virtual {v2, v1, v3}, Lcom/android/server/ScoutHelper$Action;->addActionAndParam(Ljava/lang/String;Ljava/lang/String;)V

    .line 65
    const-string v3, "input"

    invoke-virtual {v2, v1, v3}, Lcom/android/server/ScoutHelper$Action;->addActionAndParam(Ljava/lang/String;Ljava/lang/String;)V

    .line 67
    invoke-static {}, Lcom/miui/server/stability/DumpSysInfoUtil;->getDumpSysFilePath()Ljava/io/File;

    move-result-object v1

    .line 68
    .local v1, "dumpsysLogPath":Ljava/io/File;
    if-eqz v1, :cond_0

    .line 69
    const-string v3, "hanglog"

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    .line 70
    invoke-virtual {v1}, Ljava/io/File;->getAbsolutePath()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, "/"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    .line 69
    invoke-static {v0, v2, v3, v4}, Lcom/android/server/ScoutHelper;->dumpOfflineLog(Ljava/lang/String;Lcom/android/server/ScoutHelper$Action;Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 74
    .end local v1    # "dumpsysLogPath":Ljava/io/File;
    .end local v2    # "action":Lcom/android/server/ScoutHelper$Action;
    :cond_0
    goto :goto_0

    .line 72
    :catch_0
    move-exception v0

    .line 73
    .local v0, "e":Ljava/lang/Exception;
    const-string v1, "DumpSysInfoUtil"

    const-string v2, "crash in the captureDumpLog()"

    invoke-static {v1, v2, v0}, Landroid/util/Slog;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    .line 75
    .end local v0    # "e":Ljava/lang/Exception;
    :goto_0
    return-void
.end method

.method static synthetic lambda$deleteMissFetchByPower$1(Ljava/io/File;)V
    .locals 4
    .param p0, "filePath"    # Ljava/io/File;

    .line 151
    if-eqz p0, :cond_1

    :try_start_0
    invoke-virtual {p0}, Ljava/io/File;->exists()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 152
    invoke-virtual {p0}, Ljava/io/File;->listFiles()[Ljava/io/File;

    move-result-object v0

    array-length v1, v0

    const/4 v2, 0x0

    :goto_0
    if-ge v2, v1, :cond_0

    aget-object v3, v0, v2

    .line 153
    .local v3, "file":Ljava/io/File;
    invoke-virtual {v3}, Ljava/io/File;->delete()Z

    .line 152
    nop

    .end local v3    # "file":Ljava/io/File;
    add-int/lit8 v2, v2, 0x1

    goto :goto_0

    .line 155
    :cond_0
    invoke-virtual {p0}, Ljava/io/File;->delete()Z
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_1

    .line 157
    :catch_0
    move-exception v0

    .line 158
    .local v0, "e":Ljava/lang/Exception;
    const-string v1, "DumpSysInfoUtil"

    const-string v2, "crash in the deleteMissFetchByPower()"

    invoke-static {v1, v2, v0}, Landroid/util/Slog;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    goto :goto_2

    .line 159
    .end local v0    # "e":Ljava/lang/Exception;
    :cond_1
    :goto_1
    nop

    .line 160
    :goto_2
    return-void
.end method
