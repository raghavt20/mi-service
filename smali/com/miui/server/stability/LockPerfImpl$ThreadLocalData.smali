.class Lcom/miui/server/stability/LockPerfImpl$ThreadLocalData;
.super Ljava/lang/Object;
.source "LockPerfImpl.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/miui/server/stability/LockPerfImpl;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0xa
    name = "ThreadLocalData"
.end annotation


# instance fields
.field private final debugEventBuffer:Lcom/android/internal/util/RingBuffer;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/android/internal/util/RingBuffer<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private final perfStack:Lcom/miui/server/stability/LockPerfImpl$PerfStack;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/miui/server/stability/LockPerfImpl$PerfStack<",
            "Lcom/miui/server/stability/LockPerfImpl$PerfInfo;",
            ">;"
        }
    .end annotation
.end field

.field private unwindingStack:[Ljava/lang/StackTraceElement;


# direct methods
.method static bridge synthetic -$$Nest$fgetdebugEventBuffer(Lcom/miui/server/stability/LockPerfImpl$ThreadLocalData;)Lcom/android/internal/util/RingBuffer;
    .locals 0

    iget-object p0, p0, Lcom/miui/server/stability/LockPerfImpl$ThreadLocalData;->debugEventBuffer:Lcom/android/internal/util/RingBuffer;

    return-object p0
.end method

.method static bridge synthetic -$$Nest$fgetperfStack(Lcom/miui/server/stability/LockPerfImpl$ThreadLocalData;)Lcom/miui/server/stability/LockPerfImpl$PerfStack;
    .locals 0

    iget-object p0, p0, Lcom/miui/server/stability/LockPerfImpl$ThreadLocalData;->perfStack:Lcom/miui/server/stability/LockPerfImpl$PerfStack;

    return-object p0
.end method

.method static bridge synthetic -$$Nest$fgetunwindingStack(Lcom/miui/server/stability/LockPerfImpl$ThreadLocalData;)[Ljava/lang/StackTraceElement;
    .locals 0

    iget-object p0, p0, Lcom/miui/server/stability/LockPerfImpl$ThreadLocalData;->unwindingStack:[Ljava/lang/StackTraceElement;

    return-object p0
.end method

.method static bridge synthetic -$$Nest$mrecordEvent(Lcom/miui/server/stability/LockPerfImpl$ThreadLocalData;)V
    .locals 0

    invoke-direct {p0}, Lcom/miui/server/stability/LockPerfImpl$ThreadLocalData;->recordEvent()V

    return-void
.end method

.method static bridge synthetic -$$Nest$msetUnwindingStack(Lcom/miui/server/stability/LockPerfImpl$ThreadLocalData;[Ljava/lang/StackTraceElement;)V
    .locals 0

    invoke-direct {p0, p1}, Lcom/miui/server/stability/LockPerfImpl$ThreadLocalData;->setUnwindingStack([Ljava/lang/StackTraceElement;)V

    return-void
.end method

.method private constructor <init>()V
    .locals 3

    .line 324
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 320
    new-instance v0, Lcom/miui/server/stability/LockPerfImpl$PerfStack;

    const/4 v1, 0x0

    invoke-direct {v0, v1}, Lcom/miui/server/stability/LockPerfImpl$PerfStack;-><init>(Lcom/miui/server/stability/LockPerfImpl$PerfStack-IA;)V

    iput-object v0, p0, Lcom/miui/server/stability/LockPerfImpl$ThreadLocalData;->perfStack:Lcom/miui/server/stability/LockPerfImpl$PerfStack;

    .line 322
    new-instance v0, Lcom/android/internal/util/RingBuffer;

    const-class v1, Ljava/lang/String;

    const/16 v2, 0x14

    invoke-direct {v0, v1, v2}, Lcom/android/internal/util/RingBuffer;-><init>(Ljava/lang/Class;I)V

    iput-object v0, p0, Lcom/miui/server/stability/LockPerfImpl$ThreadLocalData;->debugEventBuffer:Lcom/android/internal/util/RingBuffer;

    .line 325
    return-void
.end method

.method synthetic constructor <init>(Lcom/miui/server/stability/LockPerfImpl$ThreadLocalData-IA;)V
    .locals 0

    invoke-direct {p0}, Lcom/miui/server/stability/LockPerfImpl$ThreadLocalData;-><init>()V

    return-void
.end method

.method private recordEvent()V
    .locals 10

    .line 333
    invoke-static {}, Ljava/lang/Thread;->currentThread()Ljava/lang/Thread;

    move-result-object v0

    .line 334
    .local v0, "currentThread":Ljava/lang/Thread;
    new-instance v9, Lcom/miui/server/stability/LockPerfImpl$SlowLockedMethodEvent;

    iget-object v2, p0, Lcom/miui/server/stability/LockPerfImpl$ThreadLocalData;->perfStack:Lcom/miui/server/stability/LockPerfImpl$PerfStack;

    iget-object v3, p0, Lcom/miui/server/stability/LockPerfImpl$ThreadLocalData;->unwindingStack:[Ljava/lang/StackTraceElement;

    invoke-virtual {v0}, Ljava/lang/Thread;->getId()J

    move-result-wide v4

    .line 335
    invoke-virtual {v0}, Ljava/lang/Thread;->getName()Ljava/lang/String;

    move-result-object v6

    invoke-static {}, Ljava/time/LocalDateTime;->now()Ljava/time/LocalDateTime;

    move-result-object v7

    const/4 v8, 0x0

    move-object v1, v9

    invoke-direct/range {v1 .. v8}, Lcom/miui/server/stability/LockPerfImpl$SlowLockedMethodEvent;-><init>(Lcom/miui/server/stability/LockPerfImpl$PerfStack;[Ljava/lang/StackTraceElement;JLjava/lang/String;Ljava/time/LocalDateTime;Lcom/miui/server/stability/LockPerfImpl$SlowLockedMethodEvent-IA;)V

    .line 337
    .local v1, "event":Lcom/miui/server/stability/LockPerfImpl$SlowLockedMethodEvent;
    const/4 v2, 0x0

    invoke-direct {p0, v2}, Lcom/miui/server/stability/LockPerfImpl$ThreadLocalData;->setUnwindingStack([Ljava/lang/StackTraceElement;)V

    .line 338
    iget-object v2, p0, Lcom/miui/server/stability/LockPerfImpl$ThreadLocalData;->perfStack:Lcom/miui/server/stability/LockPerfImpl$PerfStack;

    invoke-static {v2}, Lcom/miui/server/stability/LockPerfImpl$PerfStack;->-$$Nest$mclearBuffer(Lcom/miui/server/stability/LockPerfImpl$PerfStack;)V

    .line 339
    invoke-static {v1}, Lcom/miui/server/stability/LockPerfImpl$SlowLockedMethodEvent;->-$$Nest$mprintToLogcat(Lcom/miui/server/stability/LockPerfImpl$SlowLockedMethodEvent;)V

    .line 340
    invoke-static {}, Lcom/miui/server/stability/LockPerfImpl;->-$$Nest$sfgetsEventRingBuffer()Lcom/miui/server/stability/LockPerfImpl$ThreadSafeRingBuffer;

    move-result-object v2

    invoke-virtual {v2, v1}, Lcom/miui/server/stability/LockPerfImpl$ThreadSafeRingBuffer;->append(Ljava/lang/Object;)V

    .line 341
    return-void
.end method

.method private setUnwindingStack([Ljava/lang/StackTraceElement;)V
    .locals 0
    .param p1, "stack"    # [Ljava/lang/StackTraceElement;

    .line 328
    iput-object p1, p0, Lcom/miui/server/stability/LockPerfImpl$ThreadLocalData;->unwindingStack:[Ljava/lang/StackTraceElement;

    .line 329
    return-void
.end method


# virtual methods
.method public outputRecentEvents()V
    .locals 5

    .line 344
    iget-object v0, p0, Lcom/miui/server/stability/LockPerfImpl$ThreadLocalData;->debugEventBuffer:Lcom/android/internal/util/RingBuffer;

    invoke-virtual {v0}, Lcom/android/internal/util/RingBuffer;->toArray()[Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Ljava/lang/String;

    .line 345
    .local v0, "events":[Ljava/lang/String;
    const/4 v1, 0x0

    .local v1, "i":I
    :goto_0
    array-length v2, v0

    if-ge v1, v2, :cond_0

    .line 346
    invoke-static {}, Lcom/miui/server/stability/LockPerfImpl;->-$$Nest$sfgetTAG()Ljava/lang/String;

    move-result-object v2

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "Event["

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, "]: "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    aget-object v4, v0, v1

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Landroid/util/Slog;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 345
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 348
    .end local v1    # "i":I
    :cond_0
    return-void
.end method
