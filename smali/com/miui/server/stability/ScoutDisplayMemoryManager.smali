.class public Lcom/miui/server/stability/ScoutDisplayMemoryManager;
.super Ljava/lang/Object;
.source "ScoutDisplayMemoryManager.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/miui/server/stability/ScoutDisplayMemoryManager$DiaplayMemoryErrorInfo;,
        Lcom/miui/server/stability/ScoutDisplayMemoryManager$CrimeScenePreservedDialog;
    }
.end annotation


# static fields
.field private static final APP_ID:Ljava/lang/String; = "31000000454"

.field private static final DISPLAY_DMABUF:I = 0x1

.field private static final DISPLAY_GPU_MEMORY:I = 0x2

.field private static final DMABUF_EVENT_NAME:Ljava/lang/String; = "dmabuf_leak"

.field private static final DMABUF_LEAK_SF_THRESHOLD:I

.field private static final DMABUF_LEAK_THRESHOLD:I

.field private static final ENABLE_SCOUT_MEMORY_MONITOR:Z

.field private static final FLAG_NOT_LIMITED_BY_USER_EXPERIENCE_PLAN:I = 0x1

.field private static final GBTOKB:J = 0x100000L

.field private static final GPU_MEMORY_EVENT_NAME:Ljava/lang/String; = "kgsl_leak"

.field private static final GPU_MEMORY_LEAK_THRESHOLD:I

.field private static final MBTOKB:J = 0x400L

.field private static final MEM_DISABLE_REPORT_INTERVAL:I = 0x927c0

.field private static final MEM_ERROR_DIALOG_TIMEOUT:I = 0x493e0

.field private static final MEM_REPORT_INTERVAL:I = 0x36ee80

.field private static final ONETRACK_PACKAGE_NAME:Ljava/lang/String; = "com.miui.analytics"

.field private static final ONE_TRACK_ACTION:Ljava/lang/String; = "onetrack.action.TRACK_EVENT"

.field private static final PACKAGE:Ljava/lang/String; = "android"

.field private static final PROC_PROPORTIONAL_THRESHOLD:D = 0.6

.field public static final RESUME_ACTION_CANCLE:I = 0x5

.field public static final RESUME_ACTION_CONFIRM:I = 0x4

.field public static final RESUME_ACTION_CRASH:I = 0x2

.field public static final RESUME_ACTION_DIALOG:I = 0x3

.field public static final RESUME_ACTION_FAIL:I = 0x0

.field public static final RESUME_ACTION_KILL:I = 0x1

.field private static final SCOUT_MEMORY_DISABLE_DMABUF:Z

.field private static final SCOUT_MEMORY_DISABLE_GPU:Z

.field private static final SYSPROP_DMABUF_LEAK_THRESHOLD:Ljava/lang/String; = "persist.sys.debug.scout_memory_dmabuf_threshold"

.field private static final SYSPROP_ENABLE_SCOUT_MEMORY_MONITOR:Ljava/lang/String; = "persist.sys.debug.enable_scout_memory_monitor"

.field private static final SYSPROP_ENABLE_SCOUT_MEMORY_RESUME:Ljava/lang/String; = "persist.sys.debug.enable_scout_memory_resume"

.field private static final SYSPROP_GPU_MEMORY_LEAK_THRESHOLD:Ljava/lang/String; = "persist.sys.debug.scout_memory_gpu_threshold"

.field private static final SYSPROP_PRESERVE_CRIME_SCENE:Ljava/lang/String; = "persist.sys.debug.preserve_scout_memory_leak_scene"

.field private static final SYSPROP_SCOUT_MEMORY_DISABLE_DMABUF:Ljava/lang/String; = "persist.sys.debug.scout_memory_disable_dmabuf"

.field private static final SYSPROP_SCOUT_MEMORY_DISABLE_GPU:Ljava/lang/String; = "persist.sys.debug.scout_memory_disable_gpu"

.field private static final TAG:Ljava/lang/String; = "ScoutDisplayMemoryManager"

.field private static debug:Z

.field private static displayMemoryManager:Lcom/miui/server/stability/ScoutDisplayMemoryManager;


# instance fields
.field private gpuType:I

.field private final isBusy:Ljava/util/concurrent/atomic/AtomicBoolean;

.field private isCameraForeground:Z

.field private mContext:Landroid/content/Context;

.field private mCrashTimes:I

.field private final mDisableState:Ljava/util/concurrent/atomic/AtomicBoolean;

.field private final mIsShowDialog:Ljava/util/concurrent/atomic/AtomicBoolean;

.field private mLastCrashPid:I

.field private mLastCrashProc:Ljava/lang/String;

.field private volatile mLastReportTime:J

.field private mService:Lcom/android/server/am/ActivityManagerService;

.field private volatile mShowDialogTime:J

.field private totalRam:I


# direct methods
.method public static synthetic $r8$lambda$H9ftIko0R1HvS0uJf4czarQNMY4(Lcom/miui/server/stability/ScoutDisplayMemoryManager;Lcom/miui/server/stability/ScoutDisplayMemoryManager$DiaplayMemoryErrorInfo;)V
    .locals 0

    invoke-direct {p0, p1}, Lcom/miui/server/stability/ScoutDisplayMemoryManager;->lambda$reportDisplayMemoryLeakEvent$0(Lcom/miui/server/stability/ScoutDisplayMemoryManager$DiaplayMemoryErrorInfo;)V

    return-void
.end method

.method public static synthetic $r8$lambda$lVvY-Qo6TgHkb0rMhGyglm5tt2I(Lcom/miui/server/stability/ScoutDisplayMemoryManager;Ljava/lang/String;Lcom/miui/server/stability/ScoutDisplayMemoryManager$DiaplayMemoryErrorInfo;)V
    .locals 0

    invoke-direct {p0, p1, p2}, Lcom/miui/server/stability/ScoutDisplayMemoryManager;->lambda$handleReportMqs$1(Ljava/lang/String;Lcom/miui/server/stability/ScoutDisplayMemoryManager$DiaplayMemoryErrorInfo;)V

    return-void
.end method

.method static bridge synthetic -$$Nest$fgetgpuType(Lcom/miui/server/stability/ScoutDisplayMemoryManager;)I
    .locals 0

    iget p0, p0, Lcom/miui/server/stability/ScoutDisplayMemoryManager;->gpuType:I

    return p0
.end method

.method static bridge synthetic -$$Nest$fgetisBusy(Lcom/miui/server/stability/ScoutDisplayMemoryManager;)Ljava/util/concurrent/atomic/AtomicBoolean;
    .locals 0

    iget-object p0, p0, Lcom/miui/server/stability/ScoutDisplayMemoryManager;->isBusy:Ljava/util/concurrent/atomic/AtomicBoolean;

    return-object p0
.end method

.method static bridge synthetic -$$Nest$fgetisCameraForeground(Lcom/miui/server/stability/ScoutDisplayMemoryManager;)Z
    .locals 0

    iget-boolean p0, p0, Lcom/miui/server/stability/ScoutDisplayMemoryManager;->isCameraForeground:Z

    return p0
.end method

.method static bridge synthetic -$$Nest$mreadDmabufInfo(Lcom/miui/server/stability/ScoutDisplayMemoryManager;)Lcom/miui/server/stability/DmaBufUsageInfo;
    .locals 0

    invoke-direct {p0}, Lcom/miui/server/stability/ScoutDisplayMemoryManager;->readDmabufInfo()Lcom/miui/server/stability/DmaBufUsageInfo;

    move-result-object p0

    return-object p0
.end method

.method static bridge synthetic -$$Nest$mreadGpuMemoryInfo(Lcom/miui/server/stability/ScoutDisplayMemoryManager;I)Lcom/miui/server/stability/GpuMemoryUsageInfo;
    .locals 0

    invoke-direct {p0, p1}, Lcom/miui/server/stability/ScoutDisplayMemoryManager;->readGpuMemoryInfo(I)Lcom/miui/server/stability/GpuMemoryUsageInfo;

    move-result-object p0

    return-object p0
.end method

.method static bridge synthetic -$$Nest$mresumeMemLeak(Lcom/miui/server/stability/ScoutDisplayMemoryManager;Lcom/miui/server/stability/ScoutDisplayMemoryManager$DiaplayMemoryErrorInfo;)V
    .locals 0

    invoke-direct {p0, p1}, Lcom/miui/server/stability/ScoutDisplayMemoryManager;->resumeMemLeak(Lcom/miui/server/stability/ScoutDisplayMemoryManager$DiaplayMemoryErrorInfo;)V

    return-void
.end method

.method static bridge synthetic -$$Nest$sfgetDMABUF_LEAK_SF_THRESHOLD()I
    .locals 1

    sget v0, Lcom/miui/server/stability/ScoutDisplayMemoryManager;->DMABUF_LEAK_SF_THRESHOLD:I

    return v0
.end method

.method static bridge synthetic -$$Nest$sfgetDMABUF_LEAK_THRESHOLD()I
    .locals 1

    sget v0, Lcom/miui/server/stability/ScoutDisplayMemoryManager;->DMABUF_LEAK_THRESHOLD:I

    return v0
.end method

.method static bridge synthetic -$$Nest$sfgetGPU_MEMORY_LEAK_THRESHOLD()I
    .locals 1

    sget v0, Lcom/miui/server/stability/ScoutDisplayMemoryManager;->GPU_MEMORY_LEAK_THRESHOLD:I

    return v0
.end method

.method static bridge synthetic -$$Nest$smdoPreserveCrimeSceneIfNeed(Ljava/lang/String;IJLjava/lang/String;)Z
    .locals 0

    invoke-static {p0, p1, p2, p3, p4}, Lcom/miui/server/stability/ScoutDisplayMemoryManager;->doPreserveCrimeSceneIfNeed(Ljava/lang/String;IJLjava/lang/String;)Z

    move-result p0

    return p0
.end method

.method static constructor <clinit>()V
    .locals 4

    .line 61
    sget-boolean v0, Lcom/android/server/ScoutHelper;->ENABLED_SCOUT_DEBUG:Z

    const/4 v1, 0x0

    if-nez v0, :cond_1

    .line 62
    invoke-static {}, Lmiui/mqsas/scout/ScoutUtils;->isLibraryTest()Z

    move-result v0

    if-eqz v0, :cond_0

    goto :goto_0

    :cond_0
    move v0, v1

    goto :goto_1

    :cond_1
    :goto_0
    const/4 v0, 0x1

    :goto_1
    sput-boolean v0, Lcom/miui/server/stability/ScoutDisplayMemoryManager;->debug:Z

    .line 83
    const-string v0, "persist.sys.debug.scout_memory_dmabuf_threshold"

    const/16 v2, 0xa00

    invoke-static {v0, v2}, Landroid/os/SystemProperties;->getInt(Ljava/lang/String;I)I

    move-result v3

    sput v3, Lcom/miui/server/stability/ScoutDisplayMemoryManager;->DMABUF_LEAK_THRESHOLD:I

    .line 85
    const/16 v3, 0xbb8

    invoke-static {v0, v3}, Landroid/os/SystemProperties;->getInt(Ljava/lang/String;I)I

    move-result v0

    sput v0, Lcom/miui/server/stability/ScoutDisplayMemoryManager;->DMABUF_LEAK_SF_THRESHOLD:I

    .line 87
    const-string v0, "persist.sys.debug.scout_memory_gpu_threshold"

    invoke-static {v0, v2}, Landroid/os/SystemProperties;->getInt(Ljava/lang/String;I)I

    move-result v0

    sput v0, Lcom/miui/server/stability/ScoutDisplayMemoryManager;->GPU_MEMORY_LEAK_THRESHOLD:I

    .line 89
    const-string v0, "persist.sys.debug.enable_scout_memory_monitor"

    invoke-static {v0, v1}, Landroid/os/SystemProperties;->getBoolean(Ljava/lang/String;Z)Z

    move-result v0

    sput-boolean v0, Lcom/miui/server/stability/ScoutDisplayMemoryManager;->ENABLE_SCOUT_MEMORY_MONITOR:Z

    .line 91
    const-string v0, "persist.sys.debug.scout_memory_disable_gpu"

    invoke-static {v0, v1}, Landroid/os/SystemProperties;->getBoolean(Ljava/lang/String;Z)Z

    move-result v0

    sput-boolean v0, Lcom/miui/server/stability/ScoutDisplayMemoryManager;->SCOUT_MEMORY_DISABLE_GPU:Z

    .line 93
    const-string v0, "persist.sys.debug.scout_memory_disable_dmabuf"

    invoke-static {v0, v1}, Landroid/os/SystemProperties;->getBoolean(Ljava/lang/String;Z)Z

    move-result v0

    sput-boolean v0, Lcom/miui/server/stability/ScoutDisplayMemoryManager;->SCOUT_MEMORY_DISABLE_DMABUF:Z

    return-void
.end method

.method private constructor <init>()V
    .locals 2

    .line 142
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 96
    new-instance v0, Ljava/util/concurrent/atomic/AtomicBoolean;

    const/4 v1, 0x0

    invoke-direct {v0, v1}, Ljava/util/concurrent/atomic/AtomicBoolean;-><init>(Z)V

    iput-object v0, p0, Lcom/miui/server/stability/ScoutDisplayMemoryManager;->isBusy:Ljava/util/concurrent/atomic/AtomicBoolean;

    .line 97
    new-instance v0, Ljava/util/concurrent/atomic/AtomicBoolean;

    invoke-direct {v0, v1}, Ljava/util/concurrent/atomic/AtomicBoolean;-><init>(Z)V

    iput-object v0, p0, Lcom/miui/server/stability/ScoutDisplayMemoryManager;->mIsShowDialog:Ljava/util/concurrent/atomic/AtomicBoolean;

    .line 98
    new-instance v0, Ljava/util/concurrent/atomic/AtomicBoolean;

    invoke-direct {v0, v1}, Ljava/util/concurrent/atomic/AtomicBoolean;-><init>(Z)V

    iput-object v0, p0, Lcom/miui/server/stability/ScoutDisplayMemoryManager;->mDisableState:Ljava/util/concurrent/atomic/AtomicBoolean;

    .line 129
    iput v1, p0, Lcom/miui/server/stability/ScoutDisplayMemoryManager;->totalRam:I

    .line 143
    const-wide/16 v0, 0x0

    iput-wide v0, p0, Lcom/miui/server/stability/ScoutDisplayMemoryManager;->mLastReportTime:J

    .line 144
    return-void
.end method

.method private checkDmaBufLeak()Z
    .locals 10

    .line 627
    sget-boolean v0, Lcom/miui/server/stability/ScoutDisplayMemoryManager;->SCOUT_MEMORY_DISABLE_DMABUF:Z

    const/4 v1, 0x0

    if-eqz v0, :cond_0

    return v1

    .line 628
    :cond_0
    const-wide/16 v2, 0x0

    .line 629
    .local v2, "dmaBufTotal":J
    invoke-static {}, Landroid/os/Debug;->getIonHeapsSizeKb()J

    move-result-wide v4

    .line 630
    .local v4, "ionHeap":J
    const-wide/16 v6, 0x0

    cmp-long v0, v4, v6

    if-ltz v0, :cond_1

    .line 631
    move-wide v2, v4

    goto :goto_0

    .line 633
    :cond_1
    invoke-static {}, Landroid/os/Debug;->getDmabufTotalExportedKb()J

    move-result-wide v2

    .line 635
    :goto_0
    sget-boolean v0, Lcom/miui/server/stability/ScoutDisplayMemoryManager;->debug:Z

    const-wide/16 v6, 0x400

    if-eqz v0, :cond_2

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v8, "checkDmaBufLeak size:"

    invoke-virtual {v0, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-static {v2, v3}, Lcom/miui/server/stability/ScoutDisplayMemoryManager;->stringifyKBSize(J)Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v0, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v8, " monitor threshold = "

    invoke-virtual {v0, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    sget v8, Lcom/miui/server/stability/ScoutDisplayMemoryManager;->DMABUF_LEAK_THRESHOLD:I

    int-to-long v8, v8

    mul-long/2addr v8, v6

    invoke-virtual {v0, v8, v9}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v8, "kB"

    invoke-virtual {v0, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    const-string v8, "MIUIScout Memory"

    invoke-static {v8, v0}, Landroid/util/Slog;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 637
    :cond_2
    sget v0, Lcom/miui/server/stability/ScoutDisplayMemoryManager;->DMABUF_LEAK_THRESHOLD:I

    int-to-long v8, v0

    mul-long/2addr v8, v6

    cmp-long v0, v2, v8

    if-lez v0, :cond_3

    .line 638
    const-string v0, "dmabuf_leak"

    invoke-direct {p0, v0, v2, v3}, Lcom/miui/server/stability/ScoutDisplayMemoryManager;->reportDmabufLeakException(Ljava/lang/String;J)V

    .line 639
    const/4 v0, 0x1

    return v0

    .line 641
    :cond_3
    return v1
.end method

.method private checkDmaBufLeak(Lcom/android/server/am/ScoutMeminfo;)Z
    .locals 10
    .param p1, "scoutMeminfo"    # Lcom/android/server/am/ScoutMeminfo;

    .line 609
    sget-boolean v0, Lcom/miui/server/stability/ScoutDisplayMemoryManager;->SCOUT_MEMORY_DISABLE_DMABUF:Z

    const/4 v1, 0x0

    if-eqz v0, :cond_0

    return v1

    .line 610
    :cond_0
    const-wide/16 v2, 0x0

    .line 611
    .local v2, "dmaBufTotal":J
    invoke-static {}, Landroid/os/Debug;->getIonHeapsSizeKb()J

    move-result-wide v4

    .line 612
    .local v4, "ionHeap":J
    const-wide/16 v6, 0x0

    cmp-long v0, v4, v6

    if-ltz v0, :cond_1

    .line 613
    move-wide v2, v4

    goto :goto_0

    .line 615
    :cond_1
    invoke-virtual {p1}, Lcom/android/server/am/ScoutMeminfo;->getTotalExportedDmabuf()J

    move-result-wide v2

    .line 617
    :goto_0
    sget-boolean v0, Lcom/miui/server/stability/ScoutDisplayMemoryManager;->debug:Z

    const-wide/16 v6, 0x400

    if-eqz v0, :cond_2

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v8, "checkDmaBufLeak(root) size:"

    invoke-virtual {v0, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-static {v2, v3}, Lcom/miui/server/stability/ScoutDisplayMemoryManager;->stringifyKBSize(J)Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v0, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v8, " monitor threshold = "

    invoke-virtual {v0, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    sget v8, Lcom/miui/server/stability/ScoutDisplayMemoryManager;->DMABUF_LEAK_THRESHOLD:I

    int-to-long v8, v8

    mul-long/2addr v8, v6

    invoke-virtual {v0, v8, v9}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v8, "kB"

    invoke-virtual {v0, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    const-string v8, "MIUIScout Memory"

    invoke-static {v8, v0}, Landroid/util/Slog;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 619
    :cond_2
    sget v0, Lcom/miui/server/stability/ScoutDisplayMemoryManager;->DMABUF_LEAK_THRESHOLD:I

    int-to-long v8, v0

    mul-long/2addr v8, v6

    cmp-long v0, v2, v8

    if-lez v0, :cond_3

    .line 620
    const-string v0, "dmabuf_leak"

    invoke-direct {p0, v0, v2, v3}, Lcom/miui/server/stability/ScoutDisplayMemoryManager;->reportDmabufLeakException(Ljava/lang/String;J)V

    .line 621
    const/4 v0, 0x1

    return v0

    .line 623
    :cond_3
    return v1
.end method

.method private checkGpuMemoryLeak()Z
    .locals 9

    .line 645
    iget v0, p0, Lcom/miui/server/stability/ScoutDisplayMemoryManager;->gpuType:I

    const/4 v1, 0x0

    const/4 v2, 0x1

    if-lt v0, v2, :cond_3

    sget-boolean v3, Lcom/miui/server/stability/ScoutDisplayMemoryManager;->SCOUT_MEMORY_DISABLE_GPU:Z

    if-eqz v3, :cond_0

    goto :goto_0

    .line 646
    :cond_0
    invoke-direct {p0, v0}, Lcom/miui/server/stability/ScoutDisplayMemoryManager;->getTotalGpuMemory(I)J

    move-result-wide v3

    .line 647
    .local v3, "totalGpuMemoru":J
    sget-boolean v0, Lcom/miui/server/stability/ScoutDisplayMemoryManager;->debug:Z

    const-wide/16 v5, 0x400

    if-eqz v0, :cond_1

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v7, "checkGpuMemoryLeak size:"

    invoke-virtual {v0, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-static {v3, v4}, Lcom/miui/server/stability/ScoutDisplayMemoryManager;->stringifyKBSize(J)Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v0, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v7, " monitor threshold = "

    invoke-virtual {v0, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    sget v7, Lcom/miui/server/stability/ScoutDisplayMemoryManager;->GPU_MEMORY_LEAK_THRESHOLD:I

    int-to-long v7, v7

    mul-long/2addr v7, v5

    invoke-virtual {v0, v7, v8}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v7, "kB gpuType = "

    invoke-virtual {v0, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget v7, p0, Lcom/miui/server/stability/ScoutDisplayMemoryManager;->gpuType:I

    invoke-virtual {v0, v7}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    const-string v7, "MIUIScout Memory"

    invoke-static {v7, v0}, Landroid/util/Slog;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 649
    :cond_1
    sget v0, Lcom/miui/server/stability/ScoutDisplayMemoryManager;->GPU_MEMORY_LEAK_THRESHOLD:I

    int-to-long v7, v0

    mul-long/2addr v7, v5

    cmp-long v0, v3, v7

    if-lez v0, :cond_2

    .line 650
    const-string v0, "GpuMemory_leak"

    invoke-direct {p0, v0, v3, v4}, Lcom/miui/server/stability/ScoutDisplayMemoryManager;->reportGpuMemoryLeakException(Ljava/lang/String;J)V

    .line 651
    return v2

    .line 653
    :cond_2
    return v1

    .line 645
    .end local v3    # "totalGpuMemoru":J
    :cond_3
    :goto_0
    return v1
.end method

.method private static doPreserveCrimeSceneIfNeed(Ljava/lang/String;IJLjava/lang/String;)Z
    .locals 10
    .param p0, "leakingProcess"    # Ljava/lang/String;
    .param p1, "pid"    # I
    .param p2, "rssKB"    # J
    .param p4, "leakType"    # Ljava/lang/String;

    .line 709
    const-string v0, "MIUIScout Memory"

    sget-boolean v1, Landroid/os/Build;->IS_DEBUGGABLE:Z

    const/4 v2, 0x0

    if-nez v1, :cond_0

    .line 710
    return v2

    .line 712
    :cond_0
    const-string v1, "persist.sys.debug.preserve_scout_memory_leak_scene"

    invoke-static {v1}, Landroid/os/SystemProperties;->get(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    const-string v3, ","

    invoke-virtual {v1, v3}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v1

    .line 713
    .local v1, "processList":[Ljava/lang/String;
    const-string v3, "any"

    filled-new-array {v3, p0}, [Ljava/lang/String;

    move-result-object v3

    invoke-static {v1, v3}, Lcom/android/internal/util/ArrayUtils;->containsAny([Ljava/lang/Object;[Ljava/lang/Object;)Z

    move-result v3

    if-nez v3, :cond_1

    .line 714
    return v2

    .line 718
    :cond_1
    :try_start_0
    const-string v3, "package"

    .line 719
    invoke-static {v3}, Landroid/os/ServiceManager;->getService(Ljava/lang/String;)Landroid/os/IBinder;

    move-result-object v3

    .line 718
    invoke-static {v3}, Landroid/content/pm/IPackageManager$Stub;->asInterface(Landroid/os/IBinder;)Landroid/content/pm/IPackageManager;

    move-result-object v4

    .line 720
    .local v4, "pm":Landroid/content/pm/IPackageManager;
    const-string v5, "com.phonetest.stresstest"

    const/4 v6, 0x2

    const/4 v7, 0x0

    const/4 v8, 0x0

    const-string v9, "MIUIScout Memory"

    invoke-interface/range {v4 .. v9}, Landroid/content/pm/IPackageManager;->setApplicationEnabledSetting(Ljava/lang/String;IIILjava/lang/String;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 725
    .end local v4    # "pm":Landroid/content/pm/IPackageManager;
    goto :goto_0

    .line 723
    :catch_0
    move-exception v3

    .line 724
    .local v3, "e":Ljava/lang/Exception;
    const-string v4, "Failed to disable MTBF app"

    invoke-static {v0, v4, v3}, Landroid/util/Slog;->d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    .line 726
    .end local v3    # "e":Ljava/lang/Exception;
    :goto_0
    const-string v3, "com.android.commands.monkey"

    filled-new-array {v3}, [Ljava/lang/String;

    move-result-object v3

    invoke-static {v3}, Landroid/os/Process;->getPidsForCommands([Ljava/lang/String;)[I

    move-result-object v3

    .line 727
    .local v3, "monkeyPids":[I
    if-eqz v3, :cond_2

    .line 728
    array-length v4, v3

    :goto_1
    if-ge v2, v4, :cond_2

    aget v5, v3, v2

    .line 729
    .local v5, "monkeyPid":I
    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string v7, "Killing monkey process: pid="

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6, v5}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-static {v0, v6}, Landroid/util/Slog;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 731
    const/16 v6, 0x9

    :try_start_1
    invoke-static {v5, v6}, Landroid/system/Os;->kill(II)V
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_1

    .line 733
    goto :goto_2

    .line 732
    :catch_1
    move-exception v6

    .line 728
    .end local v5    # "monkeyPid":I
    :goto_2
    add-int/lit8 v2, v2, 0x1

    goto :goto_1

    .line 736
    :cond_2
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {p4}, Ljava/lang/String;->toUpperCase()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v2, " MEMORY LEAKED"

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    .line 737
    .local v0, "title":Ljava/lang/String;
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v2, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v4, ":"

    invoke-virtual {v2, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v4, " consumed "

    invoke-virtual {v2, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p2, p3}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v4, "KB memory, please contact the engineers for help ASAP!"

    invoke-virtual {v2, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    .line 740
    .local v2, "msg":Ljava/lang/String;
    invoke-static {v0, v2}, Lcom/miui/server/stability/ScoutDisplayMemoryManager$CrimeScenePreservedDialog;->show(Ljava/lang/String;Ljava/lang/String;)V

    .line 741
    const/4 v4, 0x1

    return v4
.end method

.method public static getInstance()Lcom/miui/server/stability/ScoutDisplayMemoryManager;
    .locals 1

    .line 136
    sget-object v0, Lcom/miui/server/stability/ScoutDisplayMemoryManager;->displayMemoryManager:Lcom/miui/server/stability/ScoutDisplayMemoryManager;

    if-nez v0, :cond_0

    .line 137
    new-instance v0, Lcom/miui/server/stability/ScoutDisplayMemoryManager;

    invoke-direct {v0}, Lcom/miui/server/stability/ScoutDisplayMemoryManager;-><init>()V

    sput-object v0, Lcom/miui/server/stability/ScoutDisplayMemoryManager;->displayMemoryManager:Lcom/miui/server/stability/ScoutDisplayMemoryManager;

    .line 139
    :cond_0
    sget-object v0, Lcom/miui/server/stability/ScoutDisplayMemoryManager;->displayMemoryManager:Lcom/miui/server/stability/ScoutDisplayMemoryManager;

    return-object v0
.end method

.method private native getTotalGpuMemory(I)J
.end method

.method private getTotalRam()I
    .locals 6

    .line 242
    iget v0, p0, Lcom/miui/server/stability/ScoutDisplayMemoryManager;->totalRam:I

    if-lez v0, :cond_0

    return v0

    .line 243
    :cond_0
    const/16 v0, 0x1b

    new-array v0, v0, [J

    .line 244
    .local v0, "infos":[J
    invoke-static {v0}, Landroid/os/Debug;->getMemInfo([J)V

    .line 245
    const/4 v1, 0x0

    aget-wide v2, v0, v1

    const-wide/32 v4, 0x100000

    div-long/2addr v2, v4

    long-to-int v2, v2

    iput v2, p0, Lcom/miui/server/stability/ScoutDisplayMemoryManager;->totalRam:I

    .line 246
    sget-boolean v2, Lcom/miui/server/stability/ScoutDisplayMemoryManager;->debug:Z

    if-eqz v2, :cond_1

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "getTotalRam total memory "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    aget-wide v3, v0, v1

    invoke-virtual {v2, v3, v4}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " kB, "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget v2, p0, Lcom/miui/server/stability/ScoutDisplayMemoryManager;->totalRam:I

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "GB"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    const-string v2, "MIUIScout Memory"

    invoke-static {v2, v1}, Landroid/util/Slog;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 248
    :cond_1
    iget v1, p0, Lcom/miui/server/stability/ScoutDisplayMemoryManager;->totalRam:I

    return v1
.end method

.method private handleReportMisight(ILmiui/mqsas/sdk/event/ExceptionEvent;)V
    .locals 5
    .param p1, "errorType"    # I
    .param p2, "event"    # Lmiui/mqsas/sdk/event/ExceptionEvent;

    .line 320
    const/4 v0, 0x1

    if-ne p1, v0, :cond_0

    .line 321
    new-instance v0, Lcom/miui/misight/MiEvent;

    const v1, 0x35b43ba9

    invoke-direct {v0, v1}, Lcom/miui/misight/MiEvent;-><init>(I)V

    .line 322
    .local v0, "miEvent":Lcom/miui/misight/MiEvent;
    const-string v1, "DmaBuf"

    invoke-virtual {p2}, Lmiui/mqsas/sdk/event/ExceptionEvent;->getDetails()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Lcom/miui/misight/MiEvent;->addStr(Ljava/lang/String;Ljava/lang/String;)Lcom/miui/misight/MiEvent;

    goto :goto_0

    .line 323
    .end local v0    # "miEvent":Lcom/miui/misight/MiEvent;
    :cond_0
    const/4 v0, 0x2

    if-ne p1, v0, :cond_1

    .line 324
    new-instance v0, Lcom/miui/misight/MiEvent;

    const v1, 0x35b43baa

    invoke-direct {v0, v1}, Lcom/miui/misight/MiEvent;-><init>(I)V

    .line 325
    .restart local v0    # "miEvent":Lcom/miui/misight/MiEvent;
    const-string v1, "Gpu"

    invoke-virtual {p2}, Lmiui/mqsas/sdk/event/ExceptionEvent;->getDetails()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Lcom/miui/misight/MiEvent;->addStr(Ljava/lang/String;Ljava/lang/String;)Lcom/miui/misight/MiEvent;

    .line 330
    :goto_0
    const-string v1, "Summary"

    invoke-virtual {p2}, Lmiui/mqsas/sdk/event/ExceptionEvent;->getSummary()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Lcom/miui/misight/MiEvent;->addStr(Ljava/lang/String;Ljava/lang/String;)Lcom/miui/misight/MiEvent;

    move-result-object v1

    .line 331
    invoke-virtual {p2}, Lmiui/mqsas/sdk/event/ExceptionEvent;->getPackageName()Ljava/lang/String;

    move-result-object v2

    const-string v3, "PackageName"

    invoke-virtual {v1, v3, v2}, Lcom/miui/misight/MiEvent;->addStr(Ljava/lang/String;Ljava/lang/String;)Lcom/miui/misight/MiEvent;

    move-result-object v1

    .line 332
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v2

    const-string v4, "CurrentTime"

    invoke-virtual {v1, v4, v2, v3}, Lcom/miui/misight/MiEvent;->addLong(Ljava/lang/String;J)Lcom/miui/misight/MiEvent;

    .line 333
    invoke-static {v0}, Lcom/miui/misight/MiSight;->sendEvent(Lcom/miui/misight/MiEvent;)V

    .line 334
    return-void

    .line 327
    .end local v0    # "miEvent":Lcom/miui/misight/MiEvent;
    :cond_1
    const-string v0, "ScoutDisplayMemoryManager"

    const-string v1, "handleReportMisight: invalid errorType"

    invoke-static {v0, v1}, Landroid/util/Slog;->w(Ljava/lang/String;Ljava/lang/String;)I

    .line 328
    return-void
.end method

.method private initGpuType()I
    .locals 3

    .line 147
    new-instance v0, Ljava/io/File;

    const-string v1, "/sys/class/kgsl/kgsl/page_alloc"

    invoke-direct {v0, v1}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 148
    .local v0, "kgslFile":Ljava/io/File;
    invoke-virtual {v0}, Ljava/io/File;->exists()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 149
    const/4 v1, 0x1

    return v1

    .line 151
    :cond_0
    new-instance v1, Ljava/io/File;

    const-string v2, "/proc/mtk_mali/gpu_memory"

    invoke-direct {v1, v2}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 152
    .local v1, "maliFile":Ljava/io/File;
    invoke-virtual {v1}, Ljava/io/File;->exists()Z

    move-result v2

    if-eqz v2, :cond_1

    .line 153
    const/4 v2, 0x2

    return v2

    .line 155
    :cond_1
    const/4 v2, 0x0

    return v2
.end method

.method private synthetic lambda$handleReportMqs$1(Ljava/lang/String;Lcom/miui/server/stability/ScoutDisplayMemoryManager$DiaplayMemoryErrorInfo;)V
    .locals 11
    .param p1, "filePath"    # Ljava/lang/String;
    .param p2, "errorInfo"    # Lcom/miui/server/stability/ScoutDisplayMemoryManager$DiaplayMemoryErrorInfo;

    .line 292
    new-instance v0, Lmiui/mqsas/sdk/event/GeneralExceptionEvent;

    invoke-direct {v0}, Lmiui/mqsas/sdk/event/GeneralExceptionEvent;-><init>()V

    .line 293
    .local v0, "event":Lmiui/mqsas/sdk/event/GeneralExceptionEvent;
    const/4 v1, 0x1

    new-array v2, v1, [Ljava/lang/String;

    if-eqz p1, :cond_0

    move-object v3, p1

    goto :goto_0

    :cond_0
    const-string v3, ""

    :goto_0
    const/4 v4, 0x0

    aput-object v3, v2, v4

    invoke-static {v2}, Ljava/util/Arrays;->asList([Ljava/lang/Object;)Ljava/util/List;

    move-result-object v2

    .line 294
    .local v2, "files":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;"
    const-wide/16 v3, 0x0

    .line 295
    .local v3, "thresholdSize":J
    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {p2}, Lcom/miui/server/stability/ScoutDisplayMemoryManager$DiaplayMemoryErrorInfo;->getName()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, ", occurring "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {p2}, Lcom/miui/server/stability/ScoutDisplayMemoryManager$DiaplayMemoryErrorInfo;->getReason()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, " OOM"

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    .line 296
    .local v5, "sum":Ljava/lang/String;
    invoke-virtual {p2}, Lcom/miui/server/stability/ScoutDisplayMemoryManager$DiaplayMemoryErrorInfo;->getType()I

    move-result v6

    const-wide/16 v7, 0x400

    if-ne v6, v1, :cond_1

    .line 297
    const/16 v1, 0x1a8

    invoke-virtual {v0, v1}, Lmiui/mqsas/sdk/event/GeneralExceptionEvent;->setType(I)V

    .line 298
    sget v1, Lcom/miui/server/stability/ScoutDisplayMemoryManager;->DMABUF_LEAK_THRESHOLD:I

    int-to-long v9, v1

    mul-long/2addr v9, v7

    .end local v3    # "thresholdSize":J
    .local v9, "thresholdSize":J
    goto :goto_1

    .line 299
    .end local v9    # "thresholdSize":J
    .restart local v3    # "thresholdSize":J
    :cond_1
    invoke-virtual {p2}, Lcom/miui/server/stability/ScoutDisplayMemoryManager$DiaplayMemoryErrorInfo;->getType()I

    move-result v1

    const/4 v6, 0x2

    if-ne v1, v6, :cond_2

    .line 300
    const/16 v1, 0x1a9

    invoke-virtual {v0, v1}, Lmiui/mqsas/sdk/event/GeneralExceptionEvent;->setType(I)V

    .line 301
    sget v1, Lcom/miui/server/stability/ScoutDisplayMemoryManager;->GPU_MEMORY_LEAK_THRESHOLD:I

    int-to-long v9, v1

    mul-long/2addr v9, v7

    .line 305
    .end local v3    # "thresholdSize":J
    .restart local v9    # "thresholdSize":J
    :goto_1
    invoke-virtual {v0, v5}, Lmiui/mqsas/sdk/event/GeneralExceptionEvent;->setSummary(Ljava/lang/String;)V

    .line 306
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v3

    invoke-virtual {v0, v3, v4}, Lmiui/mqsas/sdk/event/GeneralExceptionEvent;->setTimeStamp(J)V

    .line 307
    invoke-virtual {p2}, Lcom/miui/server/stability/ScoutDisplayMemoryManager$DiaplayMemoryErrorInfo;->getName()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lmiui/mqsas/sdk/event/GeneralExceptionEvent;->setPackageName(Ljava/lang/String;)V

    .line 308
    invoke-virtual {v0, v2}, Lmiui/mqsas/sdk/event/GeneralExceptionEvent;->setExtraFiles(Ljava/util/List;)V

    .line 309
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v1, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v3, "\ntotal_threshold: "

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v9, v10}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v3, "(kB)\nmemory_app_package: "

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    .line 311
    invoke-virtual {p2}, Lcom/miui/server/stability/ScoutDisplayMemoryManager$DiaplayMemoryErrorInfo;->getName()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v3, "\napp_threshold: "

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    .line 312
    invoke-virtual {p2}, Lcom/miui/server/stability/ScoutDisplayMemoryManager$DiaplayMemoryErrorInfo;->getThreshold()J

    move-result-wide v3

    invoke-virtual {v1, v3, v4}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v3, "(kB)\n"

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    .line 309
    invoke-virtual {v0, v1}, Lmiui/mqsas/sdk/event/GeneralExceptionEvent;->setDetails(Ljava/lang/String;)V

    .line 313
    invoke-static {}, Lmiui/mqsas/sdk/MQSEventManagerDelegate;->getInstance()Lmiui/mqsas/sdk/MQSEventManagerDelegate;

    move-result-object v1

    invoke-virtual {v1, v0}, Lmiui/mqsas/sdk/MQSEventManagerDelegate;->reportGeneralException(Lmiui/mqsas/sdk/event/GeneralExceptionEvent;)Z

    .line 314
    invoke-virtual {p2}, Lcom/miui/server/stability/ScoutDisplayMemoryManager$DiaplayMemoryErrorInfo;->getType()I

    move-result v1

    invoke-direct {p0, v1, v0}, Lcom/miui/server/stability/ScoutDisplayMemoryManager;->handleReportMisight(ILmiui/mqsas/sdk/event/ExceptionEvent;)V

    .line 315
    return-void

    .line 303
    .end local v9    # "thresholdSize":J
    .restart local v3    # "thresholdSize":J
    :cond_2
    return-void
.end method

.method private synthetic lambda$reportDisplayMemoryLeakEvent$0(Lcom/miui/server/stability/ScoutDisplayMemoryManager$DiaplayMemoryErrorInfo;)V
    .locals 5
    .param p1, "errorInfo"    # Lcom/miui/server/stability/ScoutDisplayMemoryManager$DiaplayMemoryErrorInfo;

    .line 256
    new-instance v0, Landroid/content/Intent;

    const-string v1, "onetrack.action.TRACK_EVENT"

    invoke-direct {v0, v1}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 257
    .local v0, "intent":Landroid/content/Intent;
    const-string v1, "com.miui.analytics"

    invoke-virtual {v0, v1}, Landroid/content/Intent;->setPackage(Ljava/lang/String;)Landroid/content/Intent;

    .line 258
    const-string v1, "APP_ID"

    const-string v2, "31000000454"

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 259
    const-string v1, "PACKAGE"

    const-string v2, "android"

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 260
    invoke-virtual {p1}, Lcom/miui/server/stability/ScoutDisplayMemoryManager$DiaplayMemoryErrorInfo;->getType()I

    move-result v1

    const-string v2, "EVENT_NAME"

    const/4 v3, 0x1

    if-ne v1, v3, :cond_0

    .line 261
    const-string v1, "dmabuf_leak"

    invoke-virtual {v0, v2, v1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 262
    invoke-virtual {p1}, Lcom/miui/server/stability/ScoutDisplayMemoryManager$DiaplayMemoryErrorInfo;->getTotalSize()J

    move-result-wide v1

    invoke-static {v1, v2}, Ljava/lang/String;->valueOf(J)Ljava/lang/String;

    move-result-object v1

    const-string v2, "dmabuf_total_size"

    invoke-virtual {v0, v2, v1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    goto :goto_0

    .line 263
    :cond_0
    invoke-virtual {p1}, Lcom/miui/server/stability/ScoutDisplayMemoryManager$DiaplayMemoryErrorInfo;->getType()I

    move-result v1

    const/4 v4, 0x2

    if-ne v1, v4, :cond_2

    .line 264
    const-string v1, "kgsl_leak"

    invoke-virtual {v0, v2, v1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 265
    invoke-virtual {p1}, Lcom/miui/server/stability/ScoutDisplayMemoryManager$DiaplayMemoryErrorInfo;->getTotalSize()J

    move-result-wide v1

    invoke-static {v1, v2}, Ljava/lang/String;->valueOf(J)Ljava/lang/String;

    move-result-object v1

    const-string v2, "kgsl_total_size"

    invoke-virtual {v0, v2, v1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 269
    :goto_0
    const-string v1, "memory_total_size"

    invoke-direct {p0}, Lcom/miui/server/stability/ScoutDisplayMemoryManager;->getTotalRam()I

    move-result v2

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    .line 270
    const-string v1, "memory_app_package"

    invoke-virtual {p1}, Lcom/miui/server/stability/ScoutDisplayMemoryManager$DiaplayMemoryErrorInfo;->getName()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 271
    invoke-virtual {p1}, Lcom/miui/server/stability/ScoutDisplayMemoryManager$DiaplayMemoryErrorInfo;->getRss()J

    move-result-wide v1

    invoke-static {v1, v2}, Ljava/lang/String;->valueOf(J)Ljava/lang/String;

    move-result-object v1

    const-string v2, "memory_app_size"

    invoke-virtual {v0, v2, v1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 272
    invoke-virtual {p1}, Lcom/miui/server/stability/ScoutDisplayMemoryManager$DiaplayMemoryErrorInfo;->getOomadj()I

    move-result v1

    invoke-static {v1}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v1

    const-string v2, "memory_app_adj"

    invoke-virtual {v0, v2, v1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 273
    invoke-virtual {p1}, Lcom/miui/server/stability/ScoutDisplayMemoryManager$DiaplayMemoryErrorInfo;->getAction()I

    move-result v1

    invoke-static {v1}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v1

    const-string v2, "resume_action"

    invoke-virtual {v0, v2, v1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 274
    invoke-virtual {v0, v3}, Landroid/content/Intent;->setFlags(I)Landroid/content/Intent;

    .line 275
    sget-boolean v1, Lcom/miui/server/stability/ScoutDisplayMemoryManager;->debug:Z

    if-eqz v1, :cond_1

    .line 276
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "report type="

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {p1}, Lcom/miui/server/stability/ScoutDisplayMemoryManager$DiaplayMemoryErrorInfo;->getType()I

    move-result v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " memory_total_size="

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-direct {p0}, Lcom/miui/server/stability/ScoutDisplayMemoryManager;->getTotalRam()I

    move-result v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " total_size="

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    .line 277
    invoke-virtual {p1}, Lcom/miui/server/stability/ScoutDisplayMemoryManager$DiaplayMemoryErrorInfo;->getTotalSize()J

    move-result-wide v2

    invoke-virtual {v1, v2, v3}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " memory_app_package="

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {p1}, Lcom/miui/server/stability/ScoutDisplayMemoryManager$DiaplayMemoryErrorInfo;->getName()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " memory_app_size ="

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    .line 278
    invoke-virtual {p1}, Lcom/miui/server/stability/ScoutDisplayMemoryManager$DiaplayMemoryErrorInfo;->getRss()J

    move-result-wide v2

    invoke-virtual {v1, v2, v3}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " memory_app_adj="

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {p1}, Lcom/miui/server/stability/ScoutDisplayMemoryManager$DiaplayMemoryErrorInfo;->getOomadj()I

    move-result v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " action = "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    .line 279
    invoke-virtual {p1}, Lcom/miui/server/stability/ScoutDisplayMemoryManager$DiaplayMemoryErrorInfo;->getAction()I

    move-result v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    .line 276
    const-string v2, "MIUIScout Memory"

    invoke-static {v2, v1}, Landroid/util/Slog;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 282
    :cond_1
    :try_start_0
    iget-object v1, p0, Lcom/miui/server/stability/ScoutDisplayMemoryManager;->mContext:Landroid/content/Context;

    sget-object v2, Landroid/os/UserHandle;->CURRENT:Landroid/os/UserHandle;

    invoke-virtual {v1, v0, v2}, Landroid/content/Context;->startServiceAsUser(Landroid/content/Intent;Landroid/os/UserHandle;)Landroid/content/ComponentName;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 285
    goto :goto_1

    .line 283
    :catch_0
    move-exception v1

    .line 284
    .local v1, "e":Ljava/lang/Exception;
    const-string v2, "ScoutDisplayMemoryManager"

    const-string v3, "Upload onetrack exception!"

    invoke-static {v2, v3, v1}, Landroid/util/Slog;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    .line 286
    .end local v1    # "e":Ljava/lang/Exception;
    :goto_1
    return-void

    .line 267
    :cond_2
    return-void
.end method

.method static preserveCrimeSceneIfNeed(Lcom/miui/server/stability/ScoutDisplayMemoryManager$DiaplayMemoryErrorInfo;)Z
    .locals 6
    .param p0, "errorInfo"    # Lcom/miui/server/stability/ScoutDisplayMemoryManager$DiaplayMemoryErrorInfo;

    .line 700
    invoke-virtual {p0}, Lcom/miui/server/stability/ScoutDisplayMemoryManager$DiaplayMemoryErrorInfo;->getName()Ljava/lang/String;

    move-result-object v0

    .line 701
    .local v0, "leakingProcess":Ljava/lang/String;
    invoke-virtual {p0}, Lcom/miui/server/stability/ScoutDisplayMemoryManager$DiaplayMemoryErrorInfo;->getPid()I

    move-result v1

    .line 702
    .local v1, "pid":I
    invoke-virtual {p0}, Lcom/miui/server/stability/ScoutDisplayMemoryManager$DiaplayMemoryErrorInfo;->getRss()J

    move-result-wide v2

    .line 703
    .local v2, "rss":J
    invoke-virtual {p0}, Lcom/miui/server/stability/ScoutDisplayMemoryManager$DiaplayMemoryErrorInfo;->getReason()Ljava/lang/String;

    move-result-object v4

    .line 704
    .local v4, "leakType":Ljava/lang/String;
    invoke-static {v0, v1, v2, v3, v4}, Lcom/miui/server/stability/ScoutDisplayMemoryManager;->doPreserveCrimeSceneIfNeed(Ljava/lang/String;IJLjava/lang/String;)Z

    move-result v5

    return v5
.end method

.method private native readDmabufInfo()Lcom/miui/server/stability/DmaBufUsageInfo;
.end method

.method private native readGpuMemoryInfo(I)Lcom/miui/server/stability/GpuMemoryUsageInfo;
.end method

.method private reportDmabufLeakException(Ljava/lang/String;J)V
    .locals 4
    .param p1, "reason"    # Ljava/lang/String;
    .param p2, "totalSizeKB"    # J

    .line 438
    iget-object v0, p0, Lcom/miui/server/stability/ScoutDisplayMemoryManager;->isBusy:Ljava/util/concurrent/atomic/AtomicBoolean;

    const/4 v1, 0x0

    const/4 v2, 0x1

    invoke-virtual {v0, v1, v2}, Ljava/util/concurrent/atomic/AtomicBoolean;->compareAndSet(ZZ)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 439
    new-instance v0, Ljava/lang/Thread;

    new-instance v1, Lcom/miui/server/stability/ScoutDisplayMemoryManager$3;

    invoke-direct {v1, p0, p2, p3, p1}, Lcom/miui/server/stability/ScoutDisplayMemoryManager$3;-><init>(Lcom/miui/server/stability/ScoutDisplayMemoryManager;JLjava/lang/String;)V

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, "-"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    .line 527
    invoke-static {p2, p3}, Lcom/miui/server/stability/ScoutDisplayMemoryManager;->stringifyKBSize(J)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-direct {v0, v1, v2}, Ljava/lang/Thread;-><init>(Ljava/lang/Runnable;Ljava/lang/String;)V

    invoke-virtual {v0}, Ljava/lang/Thread;->start()V

    goto :goto_0

    .line 529
    :cond_0
    const-string v0, "MIUIScout Memory"

    const-string v1, "Is Busy! skip report Dma-buf Leak Exception"

    invoke-static {v0, v1}, Landroid/util/Slog;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 531
    :goto_0
    return-void
.end method

.method private reportGpuMemoryLeakException(Ljava/lang/String;J)V
    .locals 4
    .param p1, "reason"    # Ljava/lang/String;
    .param p2, "totalSizeKB"    # J

    .line 534
    iget-object v0, p0, Lcom/miui/server/stability/ScoutDisplayMemoryManager;->isBusy:Ljava/util/concurrent/atomic/AtomicBoolean;

    const/4 v1, 0x0

    const/4 v2, 0x1

    invoke-virtual {v0, v1, v2}, Ljava/util/concurrent/atomic/AtomicBoolean;->compareAndSet(ZZ)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 535
    new-instance v0, Ljava/lang/Thread;

    new-instance v1, Lcom/miui/server/stability/ScoutDisplayMemoryManager$4;

    invoke-direct {v1, p0, p2, p3, p1}, Lcom/miui/server/stability/ScoutDisplayMemoryManager$4;-><init>(Lcom/miui/server/stability/ScoutDisplayMemoryManager;JLjava/lang/String;)V

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, "-"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    .line 602
    invoke-static {p2, p3}, Lcom/miui/server/stability/ScoutDisplayMemoryManager;->stringifyKBSize(J)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-direct {v0, v1, v2}, Ljava/lang/Thread;-><init>(Ljava/lang/Runnable;Ljava/lang/String;)V

    invoke-virtual {v0}, Ljava/lang/Thread;->start()V

    goto :goto_0

    .line 604
    :cond_0
    const-string v0, "MIUIScout Memory"

    const-string v1, "Is Busy! skip report Gpu Memory Leak Exception"

    invoke-static {v0, v1}, Landroid/util/Slog;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 606
    :goto_0
    return-void
.end method

.method private resumeMemLeak(Lcom/miui/server/stability/ScoutDisplayMemoryManager$DiaplayMemoryErrorInfo;)V
    .locals 25
    .param p1, "errorInfo"    # Lcom/miui/server/stability/ScoutDisplayMemoryManager$DiaplayMemoryErrorInfo;

    .line 361
    move-object/from16 v0, p0

    move-object/from16 v1, p1

    invoke-virtual/range {p1 .. p1}, Lcom/miui/server/stability/ScoutDisplayMemoryManager$DiaplayMemoryErrorInfo;->getPid()I

    move-result v2

    .line 362
    .local v2, "pid":I
    invoke-virtual/range {p1 .. p1}, Lcom/miui/server/stability/ScoutDisplayMemoryManager$DiaplayMemoryErrorInfo;->getOomadj()I

    move-result v3

    .line 363
    .local v3, "adj":I
    invoke-virtual/range {p1 .. p1}, Lcom/miui/server/stability/ScoutDisplayMemoryManager$DiaplayMemoryErrorInfo;->getName()Ljava/lang/String;

    move-result-object v4

    .line 364
    .local v4, "name":Ljava/lang/String;
    invoke-virtual/range {p1 .. p1}, Lcom/miui/server/stability/ScoutDisplayMemoryManager$DiaplayMemoryErrorInfo;->getReason()Ljava/lang/String;

    move-result-object v5

    .line 365
    .local v5, "reason":Ljava/lang/String;
    invoke-virtual/range {p1 .. p1}, Lcom/miui/server/stability/ScoutDisplayMemoryManager$DiaplayMemoryErrorInfo;->getThreshold()J

    move-result-wide v6

    .line 366
    .local v6, "threshold":J
    invoke-virtual/range {p1 .. p1}, Lcom/miui/server/stability/ScoutDisplayMemoryManager$DiaplayMemoryErrorInfo;->getRss()J

    move-result-wide v8

    .line 367
    .local v8, "size":J
    const-string v11, "("

    const-string v12, "MIUIScout Memory"

    if-lez v2, :cond_d

    invoke-static {v2}, Landroid/os/Process;->getThreadGroupLeader(I)I

    move-result v13

    if-ne v13, v2, :cond_d

    .line 368
    const-string v15, ") used "

    const-string v10, " OOM"

    const-string v13, "kB), occurring "

    const-string v14, " is greater than the monitoring threshold("

    move-object/from16 v19, v11

    const-string v11, "kB "

    move-object/from16 v20, v4

    .end local v4    # "name":Ljava/lang/String;
    .local v20, "name":Ljava/lang/String;
    const/16 v4, -0x384

    if-ne v3, v4, :cond_1

    invoke-static {}, Landroid/os/Process;->myPid()I

    move-result v4

    if-ne v2, v4, :cond_1

    .line 369
    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    move/from16 v21, v3

    .end local v3    # "adj":I
    .local v21, "adj":I
    const-string/jumbo v3, "system_server("

    invoke-virtual {v4, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v15}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v8, v9}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v14}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v6, v7}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v13}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v12, v3}, Landroid/util/Slog;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 372
    invoke-static {}, Lmiui/mqsas/scout/ScoutUtils;->isLibraryTest()Z

    move-result v3

    if-nez v3, :cond_0

    move-object v0, v1

    move-object/from16 v15, v20

    move/from16 v23, v21

    goto/16 :goto_6

    .line 373
    :cond_0
    invoke-static {}, Landroid/view/SurfaceControlStub;->getInstance()Landroid/view/SurfaceControlStub;

    move-result-object v3

    check-cast v3, Landroid/view/SurfaceControlImpl;

    .line 374
    .local v3, "scImpl":Landroid/view/SurfaceControlImpl;
    const/4 v4, 0x0

    invoke-virtual {v3, v4}, Landroid/view/SurfaceControlImpl;->startAsyncDumpIfNeed(Z)V

    .line 375
    invoke-virtual {v3}, Landroid/view/SurfaceControlImpl;->ensureDumpFinished()V

    .line 376
    const/4 v4, 0x1

    invoke-virtual {v1, v4}, Lcom/miui/server/stability/ScoutDisplayMemoryManager$DiaplayMemoryErrorInfo;->setAction(I)V

    .line 379
    invoke-virtual {v3, v4}, Landroid/view/SurfaceControlImpl;->increaseLeakLevel(Z)Ljava/lang/String;

    .line 380
    new-instance v4, Ljava/lang/RuntimeException;

    new-instance v12, Ljava/lang/StringBuilder;

    invoke-direct {v12}, Ljava/lang/StringBuilder;-><init>()V

    move-object/from16 v16, v3

    .end local v3    # "scImpl":Landroid/view/SurfaceControlImpl;
    .local v16, "scImpl":Landroid/view/SurfaceControlImpl;
    const-string/jumbo v3, "system_server ("

    invoke-virtual {v12, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v15}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v8, v9}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v14}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v6, v7}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v13}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-direct {v4, v3}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    throw v4

    .line 368
    .end local v16    # "scImpl":Landroid/view/SurfaceControlImpl;
    .end local v21    # "adj":I
    .local v3, "adj":I
    :cond_1
    move/from16 v21, v3

    .line 384
    .end local v3    # "adj":I
    .restart local v21    # "adj":I
    iget-object v3, v0, Lcom/miui/server/stability/ScoutDisplayMemoryManager;->mService:Lcom/android/server/am/ActivityManagerService;

    const-string v4, "), Used "

    move-object/from16 v22, v12

    const-string v12, "Kill "

    if-eqz v3, :cond_c

    move-object/from16 v18, v4

    move/from16 v3, v21

    const/16 v4, -0x384

    .end local v21    # "adj":I
    .restart local v3    # "adj":I
    if-le v3, v4, :cond_b

    const/16 v4, 0x3e9

    if-ge v3, v4, :cond_b

    .line 385
    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    move-object/from16 v21, v12

    move-object/from16 v12, v20

    .end local v20    # "name":Ljava/lang/String;
    .local v12, "name":Ljava/lang/String;
    invoke-virtual {v4, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    move-object/from16 v12, v19

    .end local v12    # "name":Ljava/lang/String;
    .restart local v20    # "name":Ljava/lang/String;
    invoke-virtual {v4, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v15}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v8, v9}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v14}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v6, v7}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v13}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    .line 388
    .local v4, "appReason":Ljava/lang/String;
    const/4 v15, 0x0

    .line 389
    .local v15, "killAction":Z
    move/from16 v19, v15

    .end local v15    # "killAction":Z
    .local v19, "killAction":Z
    const/16 v15, 0xc8

    if-ge v3, v15, :cond_4

    invoke-static {}, Lmiui/mqsas/scout/ScoutUtils;->isLibraryTest()Z

    move-result v15

    if-eqz v15, :cond_2

    move/from16 v23, v3

    goto :goto_0

    .line 409
    :cond_2
    invoke-static {}, Lcom/android/server/am/ScoutMemoryError;->getInstance()Lcom/android/server/am/ScoutMemoryError;

    move-result-object v15

    move/from16 v23, v3

    .end local v3    # "adj":I
    .local v23, "adj":I
    invoke-static {v2}, Lcom/android/server/am/ProcessUtils;->getProcessRecordByPid(I)Lcom/android/server/am/ProcessRecord;

    move-result-object v3

    invoke-virtual {v15, v3, v4, v1}, Lcom/android/server/am/ScoutMemoryError;->showAppDisplayMemoryErrorDialog(Lcom/android/server/am/ProcessRecord;Ljava/lang/String;Lcom/miui/server/stability/ScoutDisplayMemoryManager$DiaplayMemoryErrorInfo;)Z

    move-result v3

    if-eqz v3, :cond_3

    .line 410
    const/4 v15, 0x1

    .line 411
    .end local v19    # "killAction":Z
    .restart local v15    # "killAction":Z
    const/4 v3, 0x3

    invoke-virtual {v1, v3}, Lcom/miui/server/stability/ScoutDisplayMemoryManager$DiaplayMemoryErrorInfo;->setAction(I)V

    move v3, v15

    move-object/from16 v15, v20

    move-object/from16 v20, v10

    goto/16 :goto_4

    .line 409
    .end local v15    # "killAction":Z
    .restart local v19    # "killAction":Z
    :cond_3
    move-object/from16 v15, v20

    move-object/from16 v20, v10

    goto/16 :goto_3

    .line 389
    .end local v23    # "adj":I
    .restart local v3    # "adj":I
    :cond_4
    move/from16 v23, v3

    .line 390
    .end local v3    # "adj":I
    .restart local v23    # "adj":I
    :goto_0
    iget v3, v0, Lcom/miui/server/stability/ScoutDisplayMemoryManager;->mLastCrashPid:I

    if-ne v3, v2, :cond_6

    iget-object v3, v0, Lcom/miui/server/stability/ScoutDisplayMemoryManager;->mLastCrashProc:Ljava/lang/String;

    move-object/from16 v15, v20

    .end local v20    # "name":Ljava/lang/String;
    .local v15, "name":Ljava/lang/String;
    invoke-static {v3, v15}, Landroid/text/TextUtils;->equals(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Z

    move-result v3

    if-eqz v3, :cond_5

    iget v3, v0, Lcom/miui/server/stability/ScoutDisplayMemoryManager;->mCrashTimes:I

    move-object/from16 v20, v10

    const/4 v10, 0x2

    if-ne v3, v10, :cond_7

    .line 392
    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v10, "fail to crash "

    invoke-virtual {v3, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v15}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v10, " pid: "

    invoke-virtual {v3, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v10, " for two times, kill it instead"

    invoke-virtual {v3, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    .line 394
    invoke-static {v2}, Landroid/os/Process;->killProcess(I)V

    .line 395
    const/4 v3, 0x1

    .line 396
    .end local v19    # "killAction":Z
    .local v3, "killAction":Z
    const/4 v10, 0x1

    invoke-virtual {v1, v10}, Lcom/miui/server/stability/ScoutDisplayMemoryManager$DiaplayMemoryErrorInfo;->setAction(I)V

    .line 397
    const/4 v10, 0x0

    iput v10, v0, Lcom/miui/server/stability/ScoutDisplayMemoryManager;->mLastCrashPid:I

    iput v10, v0, Lcom/miui/server/stability/ScoutDisplayMemoryManager;->mCrashTimes:I

    .line 398
    const/4 v10, 0x0

    iput-object v10, v0, Lcom/miui/server/stability/ScoutDisplayMemoryManager;->mLastCrashProc:Ljava/lang/String;

    goto :goto_4

    .line 390
    .end local v3    # "killAction":Z
    .restart local v19    # "killAction":Z
    :cond_5
    move-object/from16 v20, v10

    goto :goto_1

    .end local v15    # "name":Ljava/lang/String;
    .restart local v20    # "name":Ljava/lang/String;
    :cond_6
    move-object/from16 v15, v20

    move-object/from16 v20, v10

    .line 399
    .end local v20    # "name":Ljava/lang/String;
    .restart local v15    # "name":Ljava/lang/String;
    :cond_7
    :goto_1
    invoke-static {}, Lcom/android/server/am/ScoutMemoryError;->getInstance()Lcom/android/server/am/ScoutMemoryError;

    move-result-object v3

    .line 400
    invoke-static {v2}, Lcom/android/server/am/ProcessUtils;->getProcessRecordByPid(I)Lcom/android/server/am/ProcessRecord;

    move-result-object v10

    .line 399
    invoke-virtual {v3, v10, v4}, Lcom/android/server/am/ScoutMemoryError;->scheduleCrashApp(Lcom/android/server/am/ProcessRecord;Ljava/lang/String;)Z

    move-result v3

    if-eqz v3, :cond_9

    .line 401
    const/4 v3, 0x1

    .line 402
    .end local v19    # "killAction":Z
    .restart local v3    # "killAction":Z
    const/4 v10, 0x2

    invoke-virtual {v1, v10}, Lcom/miui/server/stability/ScoutDisplayMemoryManager$DiaplayMemoryErrorInfo;->setAction(I)V

    .line 403
    iget v10, v0, Lcom/miui/server/stability/ScoutDisplayMemoryManager;->mLastCrashPid:I

    if-ne v10, v2, :cond_8

    iget-object v10, v0, Lcom/miui/server/stability/ScoutDisplayMemoryManager;->mLastCrashProc:Ljava/lang/String;

    invoke-static {v10, v15}, Landroid/text/TextUtils;->equals(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Z

    move-result v10

    if-eqz v10, :cond_8

    .line 404
    iget v10, v0, Lcom/miui/server/stability/ScoutDisplayMemoryManager;->mCrashTimes:I

    const/16 v17, 0x1

    add-int/lit8 v10, v10, 0x1

    goto :goto_2

    :cond_8
    const/4 v10, 0x1

    :goto_2
    iput v10, v0, Lcom/miui/server/stability/ScoutDisplayMemoryManager;->mCrashTimes:I

    .line 405
    iput v2, v0, Lcom/miui/server/stability/ScoutDisplayMemoryManager;->mLastCrashPid:I

    .line 406
    iput-object v15, v0, Lcom/miui/server/stability/ScoutDisplayMemoryManager;->mLastCrashProc:Ljava/lang/String;

    goto :goto_4

    .line 414
    .end local v3    # "killAction":Z
    .restart local v19    # "killAction":Z
    :cond_9
    :goto_3
    move/from16 v3, v19

    .end local v19    # "killAction":Z
    .restart local v3    # "killAction":Z
    :goto_4
    if-nez v3, :cond_a

    .line 415
    new-instance v10, Ljava/lang/StringBuilder;

    invoke-direct {v10}, Ljava/lang/StringBuilder;-><init>()V

    move-object/from16 v0, v21

    invoke-virtual {v10, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, v15}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    move-object/from16 v10, v18

    invoke-virtual {v0, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, v8, v9}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, v14}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, v6, v7}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, v13}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    move-object/from16 v10, v20

    invoke-virtual {v0, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    .line 418
    invoke-static {v2}, Landroid/os/Process;->killProcess(I)V

    .line 419
    const/4 v0, 0x1

    invoke-virtual {v1, v0}, Lcom/miui/server/stability/ScoutDisplayMemoryManager$DiaplayMemoryErrorInfo;->setAction(I)V

    .line 421
    :cond_a
    move-object/from16 v0, v22

    invoke-static {v0, v4}, Landroid/util/Slog;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 422
    .end local v3    # "killAction":Z
    .end local v4    # "appReason":Ljava/lang/String;
    move-object v0, v1

    goto/16 :goto_6

    .line 384
    .end local v15    # "name":Ljava/lang/String;
    .end local v23    # "adj":I
    .local v3, "adj":I
    .restart local v20    # "name":Ljava/lang/String;
    :cond_b
    move/from16 v23, v3

    move-object v4, v10

    move-object v0, v12

    move-object/from16 v10, v18

    move-object/from16 v12, v19

    move-object/from16 v15, v20

    move-object/from16 v3, v22

    .end local v3    # "adj":I
    .end local v20    # "name":Ljava/lang/String;
    .restart local v15    # "name":Ljava/lang/String;
    .restart local v23    # "adj":I
    goto :goto_5

    .end local v15    # "name":Ljava/lang/String;
    .end local v23    # "adj":I
    .restart local v20    # "name":Ljava/lang/String;
    .restart local v21    # "adj":I
    :cond_c
    move-object v0, v12

    move-object/from16 v12, v19

    move-object/from16 v15, v20

    move/from16 v23, v21

    move-object/from16 v3, v22

    move-object/from16 v24, v10

    move-object v10, v4

    move-object/from16 v4, v24

    .line 423
    .end local v20    # "name":Ljava/lang/String;
    .end local v21    # "adj":I
    .restart local v15    # "name":Ljava/lang/String;
    .restart local v23    # "adj":I
    :goto_5
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, v15}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, v8, v9}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, v14}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, v6, v7}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, v13}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v3, v0}, Landroid/util/Slog;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 426
    invoke-static {v2}, Landroid/os/Process;->killProcess(I)V

    .line 427
    move-object/from16 v0, p1

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Lcom/miui/server/stability/ScoutDisplayMemoryManager$DiaplayMemoryErrorInfo;->setAction(I)V

    goto :goto_6

    .line 367
    .end local v15    # "name":Ljava/lang/String;
    .end local v23    # "adj":I
    .restart local v3    # "adj":I
    .local v4, "name":Ljava/lang/String;
    :cond_d
    move-object v0, v1

    move/from16 v23, v3

    move-object v15, v4

    move-object v3, v12

    move-object v12, v11

    .line 430
    .end local v3    # "adj":I
    .end local v4    # "name":Ljava/lang/String;
    .restart local v15    # "name":Ljava/lang/String;
    .restart local v23    # "adj":I
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v1, v15}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v4, ") is invalid"

    invoke-virtual {v1, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v3, v1}, Landroid/util/Slog;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 432
    :goto_6
    invoke-virtual/range {p1 .. p1}, Lcom/miui/server/stability/ScoutDisplayMemoryManager$DiaplayMemoryErrorInfo;->getAction()I

    move-result v1

    const/4 v3, 0x3

    if-eq v1, v3, :cond_e

    .line 433
    invoke-virtual/range {p0 .. p1}, Lcom/miui/server/stability/ScoutDisplayMemoryManager;->reportDisplayMemoryLeakEvent(Lcom/miui/server/stability/ScoutDisplayMemoryManager$DiaplayMemoryErrorInfo;)V

    .line 435
    :cond_e
    return-void
.end method

.method static stringifyKBSize(J)Ljava/lang/String;
    .locals 3
    .param p0, "size"    # J

    .line 353
    const-wide/16 v0, 0x400

    mul-long/2addr v0, p0

    const/16 v2, 0x400

    invoke-static {v0, v1, v2}, Lcom/miui/server/stability/ScoutDisplayMemoryManager;->stringifySize(JI)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method static stringifySize(JI)Ljava/lang/String;
    .locals 5
    .param p0, "size"    # J
    .param p2, "order"    # I

    .line 337
    sget-object v0, Ljava/util/Locale;->US:Ljava/util/Locale;

    .line 338
    .local v0, "locale":Ljava/util/Locale;
    const-wide/16 v1, 0x400

    sparse-switch p2, :sswitch_data_0

    .line 348
    new-instance v1, Ljava/lang/IllegalArgumentException;

    const-string v2, "Invalid size order"

    invoke-direct {v1, v2}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v1

    .line 346
    :sswitch_0
    div-long v3, p0, v1

    div-long/2addr v3, v1

    div-long/2addr v3, v1

    invoke-static {v3, v4}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v1

    filled-new-array {v1}, [Ljava/lang/Object;

    move-result-object v1

    const-string v2, "%,1dGB"

    invoke-static {v0, v2, v1}, Ljava/lang/String;->format(Ljava/util/Locale;Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    return-object v1

    .line 344
    :sswitch_1
    div-long v3, p0, v1

    div-long/2addr v3, v1

    invoke-static {v3, v4}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v1

    filled-new-array {v1}, [Ljava/lang/Object;

    move-result-object v1

    const-string v2, "%,5dMB"

    invoke-static {v0, v2, v1}, Ljava/lang/String;->format(Ljava/util/Locale;Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    return-object v1

    .line 342
    :sswitch_2
    div-long v1, p0, v1

    invoke-static {v1, v2}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v1

    filled-new-array {v1}, [Ljava/lang/Object;

    move-result-object v1

    const-string v2, "%,9dkB"

    invoke-static {v0, v2, v1}, Ljava/lang/String;->format(Ljava/util/Locale;Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    return-object v1

    .line 340
    :sswitch_3
    invoke-static {p0, p1}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v1

    filled-new-array {v1}, [Ljava/lang/Object;

    move-result-object v1

    const-string v2, "%,13d"

    invoke-static {v0, v2, v1}, Ljava/lang/String;->format(Ljava/util/Locale;Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    return-object v1

    :sswitch_data_0
    .sparse-switch
        0x1 -> :sswitch_3
        0x400 -> :sswitch_2
        0x100000 -> :sswitch_1
        0x40000000 -> :sswitch_0
    .end sparse-switch
.end method


# virtual methods
.method public checkScoutLowMemory()V
    .locals 9

    .line 667
    iget-object v0, p0, Lcom/miui/server/stability/ScoutDisplayMemoryManager;->mService:Lcom/android/server/am/ActivityManagerService;

    if-eqz v0, :cond_a

    invoke-virtual {p0}, Lcom/miui/server/stability/ScoutDisplayMemoryManager;->isEnableScoutMemory()Z

    move-result v0

    if-nez v0, :cond_0

    goto/16 :goto_0

    .line 671
    :cond_0
    invoke-static {}, Landroid/os/SystemClock;->uptimeMillis()J

    move-result-wide v0

    .line 672
    .local v0, "now":J
    iget-object v2, p0, Lcom/miui/server/stability/ScoutDisplayMemoryManager;->mIsShowDialog:Ljava/util/concurrent/atomic/AtomicBoolean;

    invoke-virtual {v2}, Ljava/util/concurrent/atomic/AtomicBoolean;->get()Z

    move-result v2

    const/4 v3, 0x0

    const-string v4, "MIUIScout Memory"

    if-eqz v2, :cond_2

    .line 673
    iget-wide v5, p0, Lcom/miui/server/stability/ScoutDisplayMemoryManager;->mShowDialogTime:J

    const-wide/32 v7, 0x493e0

    add-long/2addr v5, v7

    cmp-long v2, v0, v5

    if-gez v2, :cond_1

    .line 674
    const-string/jumbo v2, "skip check display memory leak, dialog show"

    invoke-static {v4, v2}, Landroid/util/Slog;->w(Ljava/lang/String;Ljava/lang/String;)I

    .line 675
    return-void

    .line 677
    :cond_1
    invoke-virtual {p0, v3}, Lcom/miui/server/stability/ScoutDisplayMemoryManager;->setShowDialogState(Z)V

    .line 681
    :cond_2
    iget-wide v5, p0, Lcom/miui/server/stability/ScoutDisplayMemoryManager;->mLastReportTime:J

    const-wide/16 v7, 0x0

    cmp-long v2, v5, v7

    if-eqz v2, :cond_6

    iget-object v2, p0, Lcom/miui/server/stability/ScoutDisplayMemoryManager;->mDisableState:Ljava/util/concurrent/atomic/AtomicBoolean;

    invoke-virtual {v2}, Ljava/util/concurrent/atomic/AtomicBoolean;->get()Z

    move-result v2

    if-eqz v2, :cond_3

    iget-wide v5, p0, Lcom/miui/server/stability/ScoutDisplayMemoryManager;->mLastReportTime:J

    const-wide/32 v7, 0x927c0

    add-long/2addr v5, v7

    cmp-long v2, v0, v5

    if-ltz v2, :cond_4

    .line 682
    :cond_3
    invoke-virtual {p0}, Lcom/miui/server/stability/ScoutDisplayMemoryManager;->isEnableResumeFeature()Z

    move-result v2

    if-nez v2, :cond_6

    iget-wide v5, p0, Lcom/miui/server/stability/ScoutDisplayMemoryManager;->mLastReportTime:J

    const-wide/32 v7, 0x36ee80

    add-long/2addr v5, v7

    cmp-long v2, v0, v5

    if-gez v2, :cond_6

    .line 683
    :cond_4
    sget-boolean v2, Lcom/miui/server/stability/ScoutDisplayMemoryManager;->debug:Z

    if-eqz v2, :cond_5

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "mLastReportTime = "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget-wide v5, p0, Lcom/miui/server/stability/ScoutDisplayMemoryManager;->mLastReportTime:J

    invoke-virtual {v2, v5, v6}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, " mDisableState = "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget-object v3, p0, Lcom/miui/server/stability/ScoutDisplayMemoryManager;->mDisableState:Ljava/util/concurrent/atomic/AtomicBoolean;

    .line 684
    invoke-virtual {v3}, Ljava/util/concurrent/atomic/AtomicBoolean;->get()Z

    move-result v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, " now = "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v0, v1}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, " MEM_DISABLE_REPORT_INTERVAL = "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const v3, 0x927c0

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, " MEM_REPORT_INTERVAL = "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const v3, 0x36ee80

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    .line 683
    invoke-static {v4, v2}, Landroid/util/Slog;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 687
    :cond_5
    const-string/jumbo v2, "skip check display memory leak, less than last check interval"

    invoke-static {v4, v2}, Landroid/util/Slog;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 688
    return-void

    .line 689
    :cond_6
    iget-object v2, p0, Lcom/miui/server/stability/ScoutDisplayMemoryManager;->mDisableState:Ljava/util/concurrent/atomic/AtomicBoolean;

    invoke-virtual {v2}, Ljava/util/concurrent/atomic/AtomicBoolean;->get()Z

    move-result v2

    if-eqz v2, :cond_7

    .line 690
    iget-object v2, p0, Lcom/miui/server/stability/ScoutDisplayMemoryManager;->mDisableState:Ljava/util/concurrent/atomic/AtomicBoolean;

    invoke-virtual {v2, v3}, Ljava/util/concurrent/atomic/AtomicBoolean;->set(Z)V

    .line 693
    :cond_7
    sget-boolean v2, Lcom/miui/server/stability/ScoutDisplayMemoryManager;->debug:Z

    if-eqz v2, :cond_8

    const-string v2, "check MIUI Scout display memory"

    invoke-static {v4, v2}, Landroid/util/Slog;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 694
    :cond_8
    invoke-direct {p0}, Lcom/miui/server/stability/ScoutDisplayMemoryManager;->checkGpuMemoryLeak()Z

    move-result v2

    if-nez v2, :cond_9

    .line 695
    invoke-direct {p0}, Lcom/miui/server/stability/ScoutDisplayMemoryManager;->checkDmaBufLeak()Z

    .line 697
    :cond_9
    return-void

    .line 668
    .end local v0    # "now":J
    :cond_a
    :goto_0
    return-void
.end method

.method public checkScoutLowMemory(Lcom/android/server/am/ScoutMeminfo;)V
    .locals 2
    .param p1, "scoutMeminfo"    # Lcom/android/server/am/ScoutMeminfo;

    .line 657
    if-eqz p1, :cond_3

    invoke-virtual {p0}, Lcom/miui/server/stability/ScoutDisplayMemoryManager;->isEnableScoutMemory()Z

    move-result v0

    if-nez v0, :cond_0

    goto :goto_0

    .line 660
    :cond_0
    sget-boolean v0, Lcom/miui/server/stability/ScoutDisplayMemoryManager;->debug:Z

    if-eqz v0, :cond_1

    const-string v0, "MIUIScout Memory"

    const-string v1, "check MIUI Scout display memory(root)"

    invoke-static {v0, v1}, Landroid/util/Slog;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 661
    :cond_1
    invoke-direct {p0}, Lcom/miui/server/stability/ScoutDisplayMemoryManager;->checkGpuMemoryLeak()Z

    move-result v0

    if-nez v0, :cond_2

    .line 662
    invoke-direct {p0, p1}, Lcom/miui/server/stability/ScoutDisplayMemoryManager;->checkDmaBufLeak(Lcom/android/server/am/ScoutMeminfo;)Z

    .line 664
    :cond_2
    return-void

    .line 658
    :cond_3
    :goto_0
    return-void
.end method

.method public getDmabufUsageInfo()Ljava/lang/String;
    .locals 3

    .line 180
    invoke-direct {p0}, Lcom/miui/server/stability/ScoutDisplayMemoryManager;->readDmabufInfo()Lcom/miui/server/stability/DmaBufUsageInfo;

    move-result-object v0

    .line 181
    .local v0, "dmabufInfo":Lcom/miui/server/stability/DmaBufUsageInfo;
    if-eqz v0, :cond_0

    .line 182
    invoke-virtual {v0}, Lcom/miui/server/stability/DmaBufUsageInfo;->getList()Ljava/util/ArrayList;

    move-result-object v1

    .line 183
    .local v1, "infoList":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/miui/server/stability/DmaBufProcUsageInfo;>;"
    new-instance v2, Lcom/miui/server/stability/ScoutDisplayMemoryManager$1;

    invoke-direct {v2, p0}, Lcom/miui/server/stability/ScoutDisplayMemoryManager$1;-><init>(Lcom/miui/server/stability/ScoutDisplayMemoryManager;)V

    invoke-static {v1, v2}, Ljava/util/Collections;->sort(Ljava/util/List;Ljava/util/Comparator;)V

    .line 195
    invoke-virtual {v0}, Lcom/miui/server/stability/DmaBufUsageInfo;->toString()Ljava/lang/String;

    move-result-object v2

    return-object v2

    .line 197
    .end local v1    # "infoList":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/miui/server/stability/DmaBufProcUsageInfo;>;"
    :cond_0
    const/4 v1, 0x0

    return-object v1
.end method

.method public getGpuMemoryUsageInfo()Ljava/lang/String;
    .locals 3

    .line 201
    iget v0, p0, Lcom/miui/server/stability/ScoutDisplayMemoryManager;->gpuType:I

    const/4 v1, 0x0

    if-nez v0, :cond_0

    return-object v1

    .line 202
    :cond_0
    invoke-direct {p0, v0}, Lcom/miui/server/stability/ScoutDisplayMemoryManager;->readGpuMemoryInfo(I)Lcom/miui/server/stability/GpuMemoryUsageInfo;

    move-result-object v0

    .line 203
    .local v0, "gpuInfo":Lcom/miui/server/stability/GpuMemoryUsageInfo;
    if-eqz v0, :cond_1

    .line 204
    invoke-virtual {v0}, Lcom/miui/server/stability/GpuMemoryUsageInfo;->getList()Ljava/util/ArrayList;

    move-result-object v1

    .line 205
    .local v1, "infoList":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/miui/server/stability/GpuMemoryProcUsageInfo;>;"
    new-instance v2, Lcom/miui/server/stability/ScoutDisplayMemoryManager$2;

    invoke-direct {v2, p0}, Lcom/miui/server/stability/ScoutDisplayMemoryManager$2;-><init>(Lcom/miui/server/stability/ScoutDisplayMemoryManager;)V

    invoke-static {v1, v2}, Ljava/util/Collections;->sort(Ljava/util/List;Ljava/util/Comparator;)V

    .line 217
    iget v2, p0, Lcom/miui/server/stability/ScoutDisplayMemoryManager;->gpuType:I

    invoke-virtual {v0, v2}, Lcom/miui/server/stability/GpuMemoryUsageInfo;->toString(I)Ljava/lang/String;

    move-result-object v2

    return-object v2

    .line 219
    .end local v1    # "infoList":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/miui/server/stability/GpuMemoryProcUsageInfo;>;"
    :cond_1
    return-object v1
.end method

.method public handleReportMqs(Lcom/miui/server/stability/ScoutDisplayMemoryManager$DiaplayMemoryErrorInfo;Ljava/lang/String;)V
    .locals 2
    .param p1, "errorInfo"    # Lcom/miui/server/stability/ScoutDisplayMemoryManager$DiaplayMemoryErrorInfo;
    .param p2, "filePath"    # Ljava/lang/String;

    .line 291
    invoke-static {}, Lcom/android/server/MiuiBgThread;->getHandler()Landroid/os/Handler;

    move-result-object v0

    new-instance v1, Lcom/miui/server/stability/ScoutDisplayMemoryManager$$ExternalSyntheticLambda1;

    invoke-direct {v1, p0, p2, p1}, Lcom/miui/server/stability/ScoutDisplayMemoryManager$$ExternalSyntheticLambda1;-><init>(Lcom/miui/server/stability/ScoutDisplayMemoryManager;Ljava/lang/String;Lcom/miui/server/stability/ScoutDisplayMemoryManager$DiaplayMemoryErrorInfo;)V

    invoke-virtual {v0, v1}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    .line 316
    return-void
.end method

.method public init(Lcom/android/server/am/ActivityManagerService;Landroid/content/Context;)V
    .locals 2
    .param p1, "mService"    # Lcom/android/server/am/ActivityManagerService;
    .param p2, "mContext"    # Landroid/content/Context;

    .line 159
    iput-object p1, p0, Lcom/miui/server/stability/ScoutDisplayMemoryManager;->mService:Lcom/android/server/am/ActivityManagerService;

    .line 160
    iput-object p2, p0, Lcom/miui/server/stability/ScoutDisplayMemoryManager;->mContext:Landroid/content/Context;

    .line 161
    invoke-static {}, Lcom/android/server/am/ScoutMemoryError;->getInstance()Lcom/android/server/am/ScoutMemoryError;

    move-result-object v0

    invoke-virtual {v0, p1, p2}, Lcom/android/server/am/ScoutMemoryError;->init(Lcom/android/server/am/ActivityManagerService;Landroid/content/Context;)V

    .line 162
    invoke-direct {p0}, Lcom/miui/server/stability/ScoutDisplayMemoryManager;->initGpuType()I

    move-result v0

    iput v0, p0, Lcom/miui/server/stability/ScoutDisplayMemoryManager;->gpuType:I

    .line 163
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "gpuType = "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget v1, p0, Lcom/miui/server/stability/ScoutDisplayMemoryManager;->gpuType:I

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    const-string v1, "MIUIScout Memory"

    invoke-static {v1, v0}, Landroid/util/Slog;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 164
    return-void
.end method

.method public isEnableResumeFeature()Z
    .locals 2

    .line 172
    invoke-static {}, Lmiui/mqsas/scout/ScoutUtils;->isLibraryTest()Z

    move-result v0

    if-nez v0, :cond_1

    invoke-static {}, Lmiui/mqsas/scout/ScoutUtils;->isUnReleased()Z

    move-result v0

    if-nez v0, :cond_1

    .line 173
    const-string v0, "persist.sys.debug.enable_scout_memory_resume"

    const/4 v1, 0x0

    invoke-static {v0, v1}, Landroid/os/SystemProperties;->getBoolean(Ljava/lang/String;Z)Z

    move-result v0

    if-eqz v0, :cond_0

    goto :goto_0

    .line 176
    :cond_0
    return v1

    .line 174
    :cond_1
    :goto_0
    const/4 v0, 0x1

    return v0
.end method

.method public isEnableScoutMemory()Z
    .locals 1

    .line 167
    sget-boolean v0, Lcom/miui/server/stability/ScoutDisplayMemoryManager;->ENABLE_SCOUT_MEMORY_MONITOR:Z

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    return v0

    .line 168
    :cond_0
    const/4 v0, 0x0

    return v0
.end method

.method public reportDisplayMemoryLeakEvent(Lcom/miui/server/stability/ScoutDisplayMemoryManager$DiaplayMemoryErrorInfo;)V
    .locals 2
    .param p1, "errorInfo"    # Lcom/miui/server/stability/ScoutDisplayMemoryManager$DiaplayMemoryErrorInfo;

    .line 252
    iget-object v0, p0, Lcom/miui/server/stability/ScoutDisplayMemoryManager;->mContext:Landroid/content/Context;

    if-eqz v0, :cond_1

    if-nez p1, :cond_0

    goto :goto_0

    .line 255
    :cond_0
    invoke-static {}, Lcom/android/server/MiuiBgThread;->getHandler()Landroid/os/Handler;

    move-result-object v0

    new-instance v1, Lcom/miui/server/stability/ScoutDisplayMemoryManager$$ExternalSyntheticLambda0;

    invoke-direct {v1, p0, p1}, Lcom/miui/server/stability/ScoutDisplayMemoryManager$$ExternalSyntheticLambda0;-><init>(Lcom/miui/server/stability/ScoutDisplayMemoryManager;Lcom/miui/server/stability/ScoutDisplayMemoryManager$DiaplayMemoryErrorInfo;)V

    invoke-virtual {v0, v1}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    .line 287
    return-void

    .line 253
    :cond_1
    :goto_0
    return-void
.end method

.method public setDisableState(Z)V
    .locals 1
    .param p1, "state"    # Z

    .line 231
    iget-object v0, p0, Lcom/miui/server/stability/ScoutDisplayMemoryManager;->mDisableState:Ljava/util/concurrent/atomic/AtomicBoolean;

    invoke-virtual {v0, p1}, Ljava/util/concurrent/atomic/AtomicBoolean;->set(Z)V

    .line 232
    return-void
.end method

.method public setShowDialogState(Z)V
    .locals 2
    .param p1, "state"    # Z

    .line 235
    iget-object v0, p0, Lcom/miui/server/stability/ScoutDisplayMemoryManager;->mIsShowDialog:Ljava/util/concurrent/atomic/AtomicBoolean;

    invoke-virtual {v0, p1}, Ljava/util/concurrent/atomic/AtomicBoolean;->set(Z)V

    .line 236
    if-eqz p1, :cond_0

    .line 237
    invoke-static {}, Landroid/os/SystemClock;->uptimeMillis()J

    move-result-wide v0

    iput-wide v0, p0, Lcom/miui/server/stability/ScoutDisplayMemoryManager;->mShowDialogTime:J

    .line 239
    :cond_0
    return-void
.end method

.method public updateCameraForegroundState(Z)V
    .locals 0
    .param p1, "isCameraForeground"    # Z

    .line 223
    iput-boolean p1, p0, Lcom/miui/server/stability/ScoutDisplayMemoryManager;->isCameraForeground:Z

    .line 224
    return-void
.end method

.method public updateLastReportTime()V
    .locals 2

    .line 227
    invoke-static {}, Landroid/os/SystemClock;->uptimeMillis()J

    move-result-wide v0

    iput-wide v0, p0, Lcom/miui/server/stability/ScoutDisplayMemoryManager;->mLastReportTime:J

    .line 228
    return-void
.end method
