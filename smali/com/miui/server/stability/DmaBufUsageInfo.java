public class com.miui.server.stability.DmaBufUsageInfo {
	 /* .source "DmaBufUsageInfo.java" */
	 /* # instance fields */
	 private Long kernelRss;
	 private Long nativeTotalSize;
	 private Long runtimeTotalSize;
	 private Long totalPss;
	 private Long totalRss;
	 private Long totalSize;
	 private java.util.ArrayList usageList;
	 /* .annotation system Ldalvik/annotation/Signature; */
	 /* value = { */
	 /* "Ljava/util/ArrayList<", */
	 /* "Lcom/miui/server/stability/DmaBufProcUsageInfo;", */
	 /* ">;" */
	 /* } */
} // .end annotation
} // .end field
/* # direct methods */
public com.miui.server.stability.DmaBufUsageInfo ( ) {
/* .locals 1 */
/* .line 15 */
/* invoke-direct {p0}, Ljava/lang/Object;-><init>()V */
/* .line 16 */
/* new-instance v0, Ljava/util/ArrayList; */
/* invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V */
this.usageList = v0;
/* .line 17 */
return;
} // .end method
/* # virtual methods */
public void add ( com.miui.server.stability.DmaBufProcUsageInfo p0 ) {
/* .locals 1 */
/* .param p1, "procInfo" # Lcom/miui/server/stability/DmaBufProcUsageInfo; */
/* .line 20 */
v0 = this.usageList;
(( java.util.ArrayList ) v0 ).add ( p1 ); // invoke-virtual {v0, p1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z
/* .line 21 */
return;
} // .end method
public Long getKernelRss ( ) {
/* .locals 2 */
/* .line 56 */
/* iget-wide v0, p0, Lcom/miui/server/stability/DmaBufUsageInfo;->kernelRss:J */
/* return-wide v0 */
} // .end method
public java.util.ArrayList getList ( ) {
/* .locals 1 */
/* .annotation system Ldalvik/annotation/Signature; */
/* value = { */
/* "()", */
/* "Ljava/util/ArrayList<", */
/* "Lcom/miui/server/stability/DmaBufProcUsageInfo;", */
/* ">;" */
/* } */
} // .end annotation
/* .line 24 */
v0 = this.usageList;
} // .end method
public Long getNativeTotalSize ( ) {
/* .locals 2 */
/* .line 48 */
/* iget-wide v0, p0, Lcom/miui/server/stability/DmaBufUsageInfo;->nativeTotalSize:J */
/* return-wide v0 */
} // .end method
public Long getRuntimeTotalSize ( ) {
/* .locals 2 */
/* .line 40 */
/* iget-wide v0, p0, Lcom/miui/server/stability/DmaBufUsageInfo;->runtimeTotalSize:J */
/* return-wide v0 */
} // .end method
public Long getTotalPss ( ) {
/* .locals 2 */
/* .line 72 */
/* iget-wide v0, p0, Lcom/miui/server/stability/DmaBufUsageInfo;->totalPss:J */
/* return-wide v0 */
} // .end method
public Long getTotalRss ( ) {
/* .locals 2 */
/* .line 64 */
/* iget-wide v0, p0, Lcom/miui/server/stability/DmaBufUsageInfo;->totalRss:J */
/* return-wide v0 */
} // .end method
public Long getTotalSize ( ) {
/* .locals 2 */
/* .line 32 */
/* iget-wide v0, p0, Lcom/miui/server/stability/DmaBufUsageInfo;->totalSize:J */
/* return-wide v0 */
} // .end method
public void setKernelRss ( Long p0 ) {
/* .locals 0 */
/* .param p1, "kernelRss" # J */
/* .line 52 */
/* iput-wide p1, p0, Lcom/miui/server/stability/DmaBufUsageInfo;->kernelRss:J */
/* .line 53 */
return;
} // .end method
public void setNativeTotalSize ( Long p0 ) {
/* .locals 0 */
/* .param p1, "nativeTotalSize" # J */
/* .line 44 */
/* iput-wide p1, p0, Lcom/miui/server/stability/DmaBufUsageInfo;->nativeTotalSize:J */
/* .line 45 */
return;
} // .end method
public void setRuntimeTotalSize ( Long p0 ) {
/* .locals 0 */
/* .param p1, "runtimeTotalSize" # J */
/* .line 36 */
/* iput-wide p1, p0, Lcom/miui/server/stability/DmaBufUsageInfo;->runtimeTotalSize:J */
/* .line 37 */
return;
} // .end method
public void setTotalPss ( Long p0 ) {
/* .locals 0 */
/* .param p1, "totalPss" # J */
/* .line 68 */
/* iput-wide p1, p0, Lcom/miui/server/stability/DmaBufUsageInfo;->totalPss:J */
/* .line 69 */
return;
} // .end method
public void setTotalRss ( Long p0 ) {
/* .locals 0 */
/* .param p1, "totalRss" # J */
/* .line 60 */
/* iput-wide p1, p0, Lcom/miui/server/stability/DmaBufUsageInfo;->totalRss:J */
/* .line 61 */
return;
} // .end method
public void setTotalSize ( Long p0 ) {
/* .locals 0 */
/* .param p1, "totalSize" # J */
/* .line 28 */
/* iput-wide p1, p0, Lcom/miui/server/stability/DmaBufUsageInfo;->totalSize:J */
/* .line 29 */
return;
} // .end method
public java.lang.String toString ( ) {
/* .locals 5 */
/* .line 76 */
/* new-instance v0, Ljava/lang/StringBuilder; */
/* invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V */
/* .line 77 */
/* .local v0, "sb":Ljava/lang/StringBuilder; */
/* new-instance v1, Ljava/lang/StringBuilder; */
/* invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V */
final String v2 = "DMA-BUF info total: "; // const-string v2, "DMA-BUF info total: "
(( java.lang.StringBuilder ) v1 ).append ( v2 ); // invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
/* iget-wide v2, p0, Lcom/miui/server/stability/DmaBufUsageInfo;->totalSize:J */
(( java.lang.StringBuilder ) v1 ).append ( v2, v3 ); // invoke-virtual {v1, v2, v3}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;
final String v2 = "kB kernel_rss="; // const-string v2, "kB kernel_rss="
(( java.lang.StringBuilder ) v1 ).append ( v2 ); // invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
/* iget-wide v2, p0, Lcom/miui/server/stability/DmaBufUsageInfo;->kernelRss:J */
(( java.lang.StringBuilder ) v1 ).append ( v2, v3 ); // invoke-virtual {v1, v2, v3}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;
final String v2 = "kB userspace_rss="; // const-string v2, "kB userspace_rss="
(( java.lang.StringBuilder ) v1 ).append ( v2 ); // invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
/* iget-wide v2, p0, Lcom/miui/server/stability/DmaBufUsageInfo;->totalRss:J */
(( java.lang.StringBuilder ) v1 ).append ( v2, v3 ); // invoke-virtual {v1, v2, v3}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;
final String v2 = "kB userspace_pss="; // const-string v2, "kB userspace_pss="
(( java.lang.StringBuilder ) v1 ).append ( v2 ); // invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
/* iget-wide v2, p0, Lcom/miui/server/stability/DmaBufUsageInfo;->totalPss:J */
(( java.lang.StringBuilder ) v1 ).append ( v2, v3 ); // invoke-virtual {v1, v2, v3}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;
final String v2 = "kB RuntimeTotalSize="; // const-string v2, "kB RuntimeTotalSize="
(( java.lang.StringBuilder ) v1 ).append ( v2 ); // invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
/* iget-wide v2, p0, Lcom/miui/server/stability/DmaBufUsageInfo;->runtimeTotalSize:J */
(( java.lang.StringBuilder ) v1 ).append ( v2, v3 ); // invoke-virtual {v1, v2, v3}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;
final String v2 = "kB NativeTotalSize="; // const-string v2, "kB NativeTotalSize="
(( java.lang.StringBuilder ) v1 ).append ( v2 ); // invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
/* iget-wide v2, p0, Lcom/miui/server/stability/DmaBufUsageInfo;->nativeTotalSize:J */
(( java.lang.StringBuilder ) v1 ).append ( v2, v3 ); // invoke-virtual {v1, v2, v3}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;
final String v2 = "kB\n"; // const-string v2, "kB\n"
(( java.lang.StringBuilder ) v1 ).append ( v2 ); // invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v1 ).toString ( ); // invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
(( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
/* .line 81 */
v1 = this.usageList;
(( java.util.ArrayList ) v1 ).iterator ( ); // invoke-virtual {v1}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;
v2 = } // :goto_0
if ( v2 != null) { // if-eqz v2, :cond_0
/* check-cast v2, Lcom/miui/server/stability/DmaBufProcUsageInfo; */
/* .line 82 */
/* .local v2, "info":Lcom/miui/server/stability/DmaBufProcUsageInfo; */
/* new-instance v3, Ljava/lang/StringBuilder; */
/* invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V */
(( com.miui.server.stability.DmaBufProcUsageInfo ) v2 ).toString ( ); // invoke-virtual {v2}, Lcom/miui/server/stability/DmaBufProcUsageInfo;->toString()Ljava/lang/String;
(( java.lang.StringBuilder ) v3 ).append ( v4 ); // invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
final String v4 = "\n"; // const-string v4, "\n"
(( java.lang.StringBuilder ) v3 ).append ( v4 ); // invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v3 ).toString ( ); // invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
(( java.lang.StringBuilder ) v0 ).append ( v3 ); // invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
/* .line 83 */
} // .end local v2 # "info":Lcom/miui/server/stability/DmaBufProcUsageInfo;
/* .line 84 */
} // :cond_0
(( java.lang.StringBuilder ) v0 ).toString ( ); // invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
} // .end method
