.class public Lcom/miui/server/stability/GpuMemoryProcUsageInfo;
.super Ljava/lang/Object;
.source "GpuMemoryProcUsageInfo.java"


# instance fields
.field private gfxDev:J

.field private glMtrack:J

.field private oomadj:I

.field private pid:I

.field private procName:Ljava/lang/String;

.field private rss:J

.field private type:I


# direct methods
.method public constructor <init>(I)V
    .locals 0
    .param p1, "type"    # I

    .line 18
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 19
    iput p1, p0, Lcom/miui/server/stability/GpuMemoryProcUsageInfo;->type:I

    .line 20
    return-void
.end method


# virtual methods
.method public getGfxDev()J
    .locals 2

    .line 59
    iget-wide v0, p0, Lcom/miui/server/stability/GpuMemoryProcUsageInfo;->gfxDev:J

    return-wide v0
.end method

.method public getGlMtrack()J
    .locals 2

    .line 67
    iget-wide v0, p0, Lcom/miui/server/stability/GpuMemoryProcUsageInfo;->glMtrack:J

    return-wide v0
.end method

.method public getName()Ljava/lang/String;
    .locals 1

    .line 27
    iget-object v0, p0, Lcom/miui/server/stability/GpuMemoryProcUsageInfo;->procName:Ljava/lang/String;

    return-object v0
.end method

.method public getOomadj()I
    .locals 1

    .line 43
    iget v0, p0, Lcom/miui/server/stability/GpuMemoryProcUsageInfo;->oomadj:I

    return v0
.end method

.method public getPid()I
    .locals 1

    .line 35
    iget v0, p0, Lcom/miui/server/stability/GpuMemoryProcUsageInfo;->pid:I

    return v0
.end method

.method public getRss()J
    .locals 2

    .line 51
    iget-wide v0, p0, Lcom/miui/server/stability/GpuMemoryProcUsageInfo;->rss:J

    return-wide v0
.end method

.method public setGfxDev(J)V
    .locals 0
    .param p1, "gfxDev"    # J

    .line 55
    iput-wide p1, p0, Lcom/miui/server/stability/GpuMemoryProcUsageInfo;->gfxDev:J

    .line 56
    return-void
.end method

.method public setGlMtrack(J)V
    .locals 0
    .param p1, "glMtrack"    # J

    .line 63
    iput-wide p1, p0, Lcom/miui/server/stability/GpuMemoryProcUsageInfo;->glMtrack:J

    .line 64
    return-void
.end method

.method public setName(Ljava/lang/String;)V
    .locals 0
    .param p1, "procName"    # Ljava/lang/String;

    .line 23
    iput-object p1, p0, Lcom/miui/server/stability/GpuMemoryProcUsageInfo;->procName:Ljava/lang/String;

    .line 24
    return-void
.end method

.method public setOomadj(I)V
    .locals 0
    .param p1, "oomadj"    # I

    .line 39
    iput p1, p0, Lcom/miui/server/stability/GpuMemoryProcUsageInfo;->oomadj:I

    .line 40
    return-void
.end method

.method public setPid(I)V
    .locals 0
    .param p1, "pid"    # I

    .line 31
    iput p1, p0, Lcom/miui/server/stability/GpuMemoryProcUsageInfo;->pid:I

    .line 32
    return-void
.end method

.method public setRss(J)V
    .locals 0
    .param p1, "rss"    # J

    .line 47
    iput-wide p1, p0, Lcom/miui/server/stability/GpuMemoryProcUsageInfo;->rss:J

    .line 48
    return-void
.end method

.method public toString(I)Ljava/lang/String;
    .locals 7
    .param p1, "gpuType"    # I

    .line 71
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    .line 72
    .local v0, "sb":Ljava/lang/StringBuilder;
    const/4 v1, 0x1

    const-string v2, "kB"

    const-string v3, " Rss="

    const-string v4, " Oomadj="

    const-string v5, " Pid="

    if-ne p1, v1, :cond_0

    .line 73
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "KgslMemory proc info Name: "

    invoke-virtual {v1, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v6, p0, Lcom/miui/server/stability/GpuMemoryProcUsageInfo;->procName:Ljava/lang/String;

    invoke-virtual {v1, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget v5, p0, Lcom/miui/server/stability/GpuMemoryProcUsageInfo;->pid:I

    invoke-virtual {v1, v5}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget v4, p0, Lcom/miui/server/stability/GpuMemoryProcUsageInfo;->oomadj:I

    invoke-virtual {v1, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-wide v3, p0, Lcom/miui/server/stability/GpuMemoryProcUsageInfo;->rss:J

    invoke-virtual {v1, v3, v4}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v3, "kB GfxDev="

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-wide v3, p0, Lcom/miui/server/stability/GpuMemoryProcUsageInfo;->gfxDev:J

    invoke-virtual {v1, v3, v4}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v3, "kB GlMtrack="

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-wide v3, p0, Lcom/miui/server/stability/GpuMemoryProcUsageInfo;->glMtrack:J

    invoke-virtual {v1, v3, v4}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    goto :goto_0

    .line 75
    :cond_0
    const/4 v1, 0x2

    if-ne p1, v1, :cond_1

    .line 76
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "MaliMemory proc info Name: "

    invoke-virtual {v1, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v6, p0, Lcom/miui/server/stability/GpuMemoryProcUsageInfo;->procName:Ljava/lang/String;

    invoke-virtual {v1, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget v5, p0, Lcom/miui/server/stability/GpuMemoryProcUsageInfo;->pid:I

    invoke-virtual {v1, v5}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget v4, p0, Lcom/miui/server/stability/GpuMemoryProcUsageInfo;->oomadj:I

    invoke-virtual {v1, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-wide v3, p0, Lcom/miui/server/stability/GpuMemoryProcUsageInfo;->rss:J

    invoke-virtual {v1, v3, v4}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 81
    :goto_0
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    return-object v1

    .line 79
    :cond_1
    const-string v1, "Error Info"

    return-object v1
.end method
