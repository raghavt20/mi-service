public class com.miui.server.stability.KgslUsageInfo {
	 /* .source "KgslUsageInfo.java" */
	 /* # instance fields */
	 private Long nativeTotalSize;
	 private Long runtimeTotalSize;
	 private Long totalSize;
	 private java.util.ArrayList usageList;
	 /* .annotation system Ldalvik/annotation/Signature; */
	 /* value = { */
	 /* "Ljava/util/ArrayList<", */
	 /* "Lcom/miui/server/stability/KgslProcUsageInfo;", */
	 /* ">;" */
	 /* } */
} // .end annotation
} // .end field
/* # direct methods */
public com.miui.server.stability.KgslUsageInfo ( ) {
/* .locals 1 */
/* .line 12 */
/* invoke-direct {p0}, Ljava/lang/Object;-><init>()V */
/* .line 13 */
/* new-instance v0, Ljava/util/ArrayList; */
/* invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V */
this.usageList = v0;
/* .line 14 */
return;
} // .end method
/* # virtual methods */
public void add ( com.miui.server.stability.KgslProcUsageInfo p0 ) {
/* .locals 1 */
/* .param p1, "procInfo" # Lcom/miui/server/stability/KgslProcUsageInfo; */
/* .line 17 */
v0 = this.usageList;
(( java.util.ArrayList ) v0 ).add ( p1 ); // invoke-virtual {v0, p1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z
/* .line 18 */
return;
} // .end method
public java.util.ArrayList getList ( ) {
/* .locals 1 */
/* .annotation system Ldalvik/annotation/Signature; */
/* value = { */
/* "()", */
/* "Ljava/util/ArrayList<", */
/* "Lcom/miui/server/stability/KgslProcUsageInfo;", */
/* ">;" */
/* } */
} // .end annotation
/* .line 21 */
v0 = this.usageList;
} // .end method
public Long getNativeTotalSize ( ) {
/* .locals 2 */
/* .line 45 */
/* iget-wide v0, p0, Lcom/miui/server/stability/KgslUsageInfo;->nativeTotalSize:J */
/* return-wide v0 */
} // .end method
public Long getRuntimeTotalSize ( ) {
/* .locals 2 */
/* .line 37 */
/* iget-wide v0, p0, Lcom/miui/server/stability/KgslUsageInfo;->runtimeTotalSize:J */
/* return-wide v0 */
} // .end method
public Long getTotalSize ( ) {
/* .locals 2 */
/* .line 29 */
/* iget-wide v0, p0, Lcom/miui/server/stability/KgslUsageInfo;->totalSize:J */
/* return-wide v0 */
} // .end method
public void setNativeTotalSize ( Long p0 ) {
/* .locals 0 */
/* .param p1, "nativeTotalSize" # J */
/* .line 41 */
/* iput-wide p1, p0, Lcom/miui/server/stability/KgslUsageInfo;->nativeTotalSize:J */
/* .line 42 */
return;
} // .end method
public void setRuntimeTotalSize ( Long p0 ) {
/* .locals 0 */
/* .param p1, "runtimeTotalSize" # J */
/* .line 33 */
/* iput-wide p1, p0, Lcom/miui/server/stability/KgslUsageInfo;->runtimeTotalSize:J */
/* .line 34 */
return;
} // .end method
public void setTotalSize ( Long p0 ) {
/* .locals 0 */
/* .param p1, "totalSize" # J */
/* .line 25 */
/* iput-wide p1, p0, Lcom/miui/server/stability/KgslUsageInfo;->totalSize:J */
/* .line 26 */
return;
} // .end method
public java.lang.String toString ( ) {
/* .locals 5 */
/* .line 49 */
/* new-instance v0, Ljava/lang/StringBuilder; */
/* invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V */
/* .line 50 */
/* .local v0, "sb":Ljava/lang/StringBuilder; */
/* new-instance v1, Ljava/lang/StringBuilder; */
/* invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V */
final String v2 = "Kgsl info total: "; // const-string v2, "Kgsl info total: "
(( java.lang.StringBuilder ) v1 ).append ( v2 ); // invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
/* iget-wide v2, p0, Lcom/miui/server/stability/KgslUsageInfo;->totalSize:J */
(( java.lang.StringBuilder ) v1 ).append ( v2, v3 ); // invoke-virtual {v1, v2, v3}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;
final String v2 = "kB RuntimeTotalSize="; // const-string v2, "kB RuntimeTotalSize="
(( java.lang.StringBuilder ) v1 ).append ( v2 ); // invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
/* iget-wide v2, p0, Lcom/miui/server/stability/KgslUsageInfo;->runtimeTotalSize:J */
(( java.lang.StringBuilder ) v1 ).append ( v2, v3 ); // invoke-virtual {v1, v2, v3}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;
final String v2 = "kB NativeTotalSize="; // const-string v2, "kB NativeTotalSize="
(( java.lang.StringBuilder ) v1 ).append ( v2 ); // invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
/* iget-wide v2, p0, Lcom/miui/server/stability/KgslUsageInfo;->nativeTotalSize:J */
(( java.lang.StringBuilder ) v1 ).append ( v2, v3 ); // invoke-virtual {v1, v2, v3}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;
final String v2 = "kB\n"; // const-string v2, "kB\n"
(( java.lang.StringBuilder ) v1 ).append ( v2 ); // invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v1 ).toString ( ); // invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
(( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
/* .line 52 */
v1 = this.usageList;
(( java.util.ArrayList ) v1 ).iterator ( ); // invoke-virtual {v1}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;
v2 = } // :goto_0
if ( v2 != null) { // if-eqz v2, :cond_0
/* check-cast v2, Lcom/miui/server/stability/KgslProcUsageInfo; */
/* .line 53 */
/* .local v2, "info":Lcom/miui/server/stability/KgslProcUsageInfo; */
/* new-instance v3, Ljava/lang/StringBuilder; */
/* invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V */
(( com.miui.server.stability.KgslProcUsageInfo ) v2 ).toString ( ); // invoke-virtual {v2}, Lcom/miui/server/stability/KgslProcUsageInfo;->toString()Ljava/lang/String;
(( java.lang.StringBuilder ) v3 ).append ( v4 ); // invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
final String v4 = "\n"; // const-string v4, "\n"
(( java.lang.StringBuilder ) v3 ).append ( v4 ); // invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v3 ).toString ( ); // invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
(( java.lang.StringBuilder ) v0 ).append ( v3 ); // invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
/* .line 54 */
} // .end local v2 # "info":Lcom/miui/server/stability/KgslProcUsageInfo;
/* .line 55 */
} // :cond_0
(( java.lang.StringBuilder ) v0 ).toString ( ); // invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
} // .end method
