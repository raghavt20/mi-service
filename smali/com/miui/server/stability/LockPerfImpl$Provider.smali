.class public final Lcom/miui/server/stability/LockPerfImpl$Provider;
.super Ljava/lang/Object;
.source "LockPerfImpl$Provider.java"

# interfaces
.implements Lcom/miui/base/MiuiStubRegistry$ImplProvider;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/miui/server/stability/LockPerfImpl$Provider$SINGLETON;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Lcom/miui/base/MiuiStubRegistry$ImplProvider<",
        "Lcom/miui/server/stability/LockPerfImpl;",
        ">;"
    }
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 0

    .line 11
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public provideNewInstance()Lcom/miui/server/stability/LockPerfImpl;
    .locals 1

    .line 17
    new-instance v0, Lcom/miui/server/stability/LockPerfImpl;

    invoke-direct {v0}, Lcom/miui/server/stability/LockPerfImpl;-><init>()V

    return-object v0
.end method

.method public bridge synthetic provideNewInstance()Ljava/lang/Object;
    .locals 1

    .line 10
    invoke-virtual {p0}, Lcom/miui/server/stability/LockPerfImpl$Provider;->provideNewInstance()Lcom/miui/server/stability/LockPerfImpl;

    move-result-object v0

    return-object v0
.end method

.method public provideSingleton()Lcom/miui/server/stability/LockPerfImpl;
    .locals 1

    .line 13
    sget-object v0, Lcom/miui/server/stability/LockPerfImpl$Provider$SINGLETON;->INSTANCE:Lcom/miui/server/stability/LockPerfImpl;

    return-object v0
.end method

.method public bridge synthetic provideSingleton()Ljava/lang/Object;
    .locals 1

    .line 10
    invoke-virtual {p0}, Lcom/miui/server/stability/LockPerfImpl$Provider;->provideSingleton()Lcom/miui/server/stability/LockPerfImpl;

    move-result-object v0

    return-object v0
.end method
