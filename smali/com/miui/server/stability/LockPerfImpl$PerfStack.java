class com.miui.server.stability.LockPerfImpl$PerfStack extends java.util.ArrayList {
	 /* .source "LockPerfImpl.java" */
	 /* # annotations */
	 /* .annotation system Ldalvik/annotation/EnclosingClass; */
	 /* value = Lcom/miui/server/stability/LockPerfImpl; */
} // .end annotation
/* .annotation system Ldalvik/annotation/InnerClass; */
/* accessFlags = 0xa */
/* name = "PerfStack" */
} // .end annotation
/* .annotation system Ldalvik/annotation/Signature; */
/* value = { */
/* "<E:", */
/* "Ljava/lang/Object;", */
/* ">", */
/* "Ljava/util/ArrayList<", */
/* "TE;>;" */
/* } */
} // .end annotation
/* # instance fields */
private Integer currTop;
/* # direct methods */
static void -$$Nest$mclearBuffer ( com.miui.server.stability.LockPerfImpl$PerfStack p0 ) { //bridge//synthethic
/* .locals 0 */
/* invoke-direct {p0}, Lcom/miui/server/stability/LockPerfImpl$PerfStack;->clearBuffer()V */
return;
} // .end method
private com.miui.server.stability.LockPerfImpl$PerfStack ( ) {
/* .locals 1 */
/* .line 358 */
/* .local p0, "this":Lcom/miui/server/stability/LockPerfImpl$PerfStack;, "Lcom/miui/server/stability/LockPerfImpl$PerfStack<TE;>;" */
/* invoke-direct {p0}, Ljava/util/ArrayList;-><init>()V */
/* .line 360 */
int v0 = 0; // const/4 v0, 0x0
/* iput v0, p0, Lcom/miui/server/stability/LockPerfImpl$PerfStack;->currTop:I */
return;
} // .end method
 com.miui.server.stability.LockPerfImpl$PerfStack ( ) { //synthethic
/* .locals 0 */
/* invoke-direct {p0}, Lcom/miui/server/stability/LockPerfImpl$PerfStack;-><init>()V */
return;
} // .end method
private void clearBuffer ( ) {
/* .locals 2 */
/* .line 392 */
/* .local p0, "this":Lcom/miui/server/stability/LockPerfImpl$PerfStack;, "Lcom/miui/server/stability/LockPerfImpl$PerfStack<TE;>;" */
/* iget v0, p0, Lcom/miui/server/stability/LockPerfImpl$PerfStack;->currTop:I */
v1 = (( com.miui.server.stability.LockPerfImpl$PerfStack ) p0 ).size ( ); // invoke-virtual {p0}, Lcom/miui/server/stability/LockPerfImpl$PerfStack;->size()I
(( com.miui.server.stability.LockPerfImpl$PerfStack ) p0 ).removeRange ( v0, v1 ); // invoke-virtual {p0, v0, v1}, Lcom/miui/server/stability/LockPerfImpl$PerfStack;->removeRange(II)V
/* .line 393 */
return;
} // .end method
private Boolean hasBufferedElement ( ) {
/* .locals 2 */
/* .line 388 */
/* .local p0, "this":Lcom/miui/server/stability/LockPerfImpl$PerfStack;, "Lcom/miui/server/stability/LockPerfImpl$PerfStack<TE;>;" */
v0 = (( com.miui.server.stability.LockPerfImpl$PerfStack ) p0 ).size ( ); // invoke-virtual {p0}, Lcom/miui/server/stability/LockPerfImpl$PerfStack;->size()I
/* if-lez v0, :cond_0 */
v0 = (( com.miui.server.stability.LockPerfImpl$PerfStack ) p0 ).size ( ); // invoke-virtual {p0}, Lcom/miui/server/stability/LockPerfImpl$PerfStack;->size()I
/* iget v1, p0, Lcom/miui/server/stability/LockPerfImpl$PerfStack;->currTop:I */
/* if-le v0, v1, :cond_0 */
int v0 = 1; // const/4 v0, 0x1
} // :cond_0
int v0 = 0; // const/4 v0, 0x0
} // :goto_0
} // .end method
/* # virtual methods */
public Boolean isEmpty ( ) {
/* .locals 1 */
/* .line 384 */
/* .local p0, "this":Lcom/miui/server/stability/LockPerfImpl$PerfStack;, "Lcom/miui/server/stability/LockPerfImpl$PerfStack<TE;>;" */
/* iget v0, p0, Lcom/miui/server/stability/LockPerfImpl$PerfStack;->currTop:I */
/* if-nez v0, :cond_0 */
int v0 = 1; // const/4 v0, 0x1
} // :cond_0
int v0 = 0; // const/4 v0, 0x0
} // :goto_0
} // .end method
public java.lang.Object pop ( ) {
/* .locals 1 */
/* .annotation system Ldalvik/annotation/Signature; */
/* value = { */
/* "()TE;" */
/* } */
} // .end annotation
/* .line 371 */
/* .local p0, "this":Lcom/miui/server/stability/LockPerfImpl$PerfStack;, "Lcom/miui/server/stability/LockPerfImpl$PerfStack<TE;>;" */
/* iget v0, p0, Lcom/miui/server/stability/LockPerfImpl$PerfStack;->currTop:I */
/* add-int/lit8 v0, v0, -0x1 */
/* iput v0, p0, Lcom/miui/server/stability/LockPerfImpl$PerfStack;->currTop:I */
(( com.miui.server.stability.LockPerfImpl$PerfStack ) p0 ).get ( v0 ); // invoke-virtual {p0, v0}, Lcom/miui/server/stability/LockPerfImpl$PerfStack;->get(I)Ljava/lang/Object;
} // .end method
public Boolean push ( java.lang.Object p0 ) {
/* .locals 1 */
/* .annotation system Ldalvik/annotation/Signature; */
/* value = { */
/* "(TE;)Z" */
/* } */
} // .end annotation
/* .line 363 */
/* .local p0, "this":Lcom/miui/server/stability/LockPerfImpl$PerfStack;, "Lcom/miui/server/stability/LockPerfImpl$PerfStack<TE;>;" */
/* .local p1, "item":Ljava/lang/Object;, "TE;" */
v0 = /* invoke-direct {p0}, Lcom/miui/server/stability/LockPerfImpl$PerfStack;->hasBufferedElement()Z */
if ( v0 != null) { // if-eqz v0, :cond_0
/* .line 364 */
/* invoke-direct {p0}, Lcom/miui/server/stability/LockPerfImpl$PerfStack;->clearBuffer()V */
/* .line 366 */
} // :cond_0
/* iget v0, p0, Lcom/miui/server/stability/LockPerfImpl$PerfStack;->currTop:I */
/* add-int/lit8 v0, v0, 0x1 */
/* iput v0, p0, Lcom/miui/server/stability/LockPerfImpl$PerfStack;->currTop:I */
/* .line 367 */
v0 = (( com.miui.server.stability.LockPerfImpl$PerfStack ) p0 ).add ( p1 ); // invoke-virtual {p0, p1}, Lcom/miui/server/stability/LockPerfImpl$PerfStack;->add(Ljava/lang/Object;)Z
} // .end method
public Integer top ( ) {
/* .locals 1 */
/* .line 379 */
/* .local p0, "this":Lcom/miui/server/stability/LockPerfImpl$PerfStack;, "Lcom/miui/server/stability/LockPerfImpl$PerfStack<TE;>;" */
/* iget v0, p0, Lcom/miui/server/stability/LockPerfImpl$PerfStack;->currTop:I */
} // .end method
public java.lang.Object topElement ( ) {
/* .locals 1 */
/* .annotation system Ldalvik/annotation/Signature; */
/* value = { */
/* "()TE;" */
/* } */
} // .end annotation
/* .line 375 */
/* .local p0, "this":Lcom/miui/server/stability/LockPerfImpl$PerfStack;, "Lcom/miui/server/stability/LockPerfImpl$PerfStack<TE;>;" */
/* iget v0, p0, Lcom/miui/server/stability/LockPerfImpl$PerfStack;->currTop:I */
/* add-int/lit8 v0, v0, -0x1 */
(( com.miui.server.stability.LockPerfImpl$PerfStack ) p0 ).get ( v0 ); // invoke-virtual {p0, v0}, Lcom/miui/server/stability/LockPerfImpl$PerfStack;->get(I)Ljava/lang/Object;
} // .end method
