public class com.miui.server.stability.ScoutDisplayMemoryManager$DiaplayMemoryErrorInfo {
	 /* .source "ScoutDisplayMemoryManager.java" */
	 /* # annotations */
	 /* .annotation system Ldalvik/annotation/EnclosingClass; */
	 /* value = Lcom/miui/server/stability/ScoutDisplayMemoryManager; */
} // .end annotation
/* .annotation system Ldalvik/annotation/InnerClass; */
/* accessFlags = 0x9 */
/* name = "DiaplayMemoryErrorInfo" */
} // .end annotation
/* # instance fields */
private Integer action;
private Integer adj;
private Integer pid;
private java.lang.String procName;
private java.lang.String reason;
private Long rss;
private Long threshold;
private Long totalSize;
private Integer type;
/* # direct methods */
public com.miui.server.stability.ScoutDisplayMemoryManager$DiaplayMemoryErrorInfo ( ) {
/* .locals 2 */
/* .param p1, "dmabufInfo" # Lcom/miui/server/stability/DmaBufProcUsageInfo; */
/* .param p2, "thresholdSize" # J */
/* .param p4, "totalSize" # J */
/* .line 806 */
/* invoke-direct {p0}, Ljava/lang/Object;-><init>()V */
/* .line 807 */
v0 = (( com.miui.server.stability.DmaBufProcUsageInfo ) p1 ).getPid ( ); // invoke-virtual {p1}, Lcom/miui/server/stability/DmaBufProcUsageInfo;->getPid()I
/* iput v0, p0, Lcom/miui/server/stability/ScoutDisplayMemoryManager$DiaplayMemoryErrorInfo;->pid:I */
/* .line 808 */
(( com.miui.server.stability.DmaBufProcUsageInfo ) p1 ).getName ( ); // invoke-virtual {p1}, Lcom/miui/server/stability/DmaBufProcUsageInfo;->getName()Ljava/lang/String;
this.procName = v0;
/* .line 809 */
(( com.miui.server.stability.DmaBufProcUsageInfo ) p1 ).getRss ( ); // invoke-virtual {p1}, Lcom/miui/server/stability/DmaBufProcUsageInfo;->getRss()J
/* move-result-wide v0 */
/* iput-wide v0, p0, Lcom/miui/server/stability/ScoutDisplayMemoryManager$DiaplayMemoryErrorInfo;->rss:J */
/* .line 810 */
v0 = (( com.miui.server.stability.DmaBufProcUsageInfo ) p1 ).getOomadj ( ); // invoke-virtual {p1}, Lcom/miui/server/stability/DmaBufProcUsageInfo;->getOomadj()I
/* iput v0, p0, Lcom/miui/server/stability/ScoutDisplayMemoryManager$DiaplayMemoryErrorInfo;->adj:I */
/* .line 811 */
/* iput-wide p2, p0, Lcom/miui/server/stability/ScoutDisplayMemoryManager$DiaplayMemoryErrorInfo;->threshold:J */
/* .line 812 */
/* iput-wide p4, p0, Lcom/miui/server/stability/ScoutDisplayMemoryManager$DiaplayMemoryErrorInfo;->totalSize:J */
/* .line 813 */
final String v0 = "DMA-BUF"; // const-string v0, "DMA-BUF"
this.reason = v0;
/* .line 814 */
int v0 = 1; // const/4 v0, 0x1
/* iput v0, p0, Lcom/miui/server/stability/ScoutDisplayMemoryManager$DiaplayMemoryErrorInfo;->type:I */
/* .line 815 */
return;
} // .end method
public com.miui.server.stability.ScoutDisplayMemoryManager$DiaplayMemoryErrorInfo ( ) {
/* .locals 2 */
/* .param p1, "gpuMemoryInfo" # Lcom/miui/server/stability/GpuMemoryProcUsageInfo; */
/* .param p2, "thresholdSize" # J */
/* .param p4, "totalSize" # J */
/* .line 794 */
/* invoke-direct {p0}, Ljava/lang/Object;-><init>()V */
/* .line 795 */
v0 = (( com.miui.server.stability.GpuMemoryProcUsageInfo ) p1 ).getPid ( ); // invoke-virtual {p1}, Lcom/miui/server/stability/GpuMemoryProcUsageInfo;->getPid()I
/* iput v0, p0, Lcom/miui/server/stability/ScoutDisplayMemoryManager$DiaplayMemoryErrorInfo;->pid:I */
/* .line 796 */
(( com.miui.server.stability.GpuMemoryProcUsageInfo ) p1 ).getName ( ); // invoke-virtual {p1}, Lcom/miui/server/stability/GpuMemoryProcUsageInfo;->getName()Ljava/lang/String;
this.procName = v0;
/* .line 797 */
(( com.miui.server.stability.GpuMemoryProcUsageInfo ) p1 ).getRss ( ); // invoke-virtual {p1}, Lcom/miui/server/stability/GpuMemoryProcUsageInfo;->getRss()J
/* move-result-wide v0 */
/* iput-wide v0, p0, Lcom/miui/server/stability/ScoutDisplayMemoryManager$DiaplayMemoryErrorInfo;->rss:J */
/* .line 798 */
v0 = (( com.miui.server.stability.GpuMemoryProcUsageInfo ) p1 ).getOomadj ( ); // invoke-virtual {p1}, Lcom/miui/server/stability/GpuMemoryProcUsageInfo;->getOomadj()I
/* iput v0, p0, Lcom/miui/server/stability/ScoutDisplayMemoryManager$DiaplayMemoryErrorInfo;->adj:I */
/* .line 799 */
/* iput-wide p2, p0, Lcom/miui/server/stability/ScoutDisplayMemoryManager$DiaplayMemoryErrorInfo;->threshold:J */
/* .line 800 */
/* iput-wide p4, p0, Lcom/miui/server/stability/ScoutDisplayMemoryManager$DiaplayMemoryErrorInfo;->totalSize:J */
/* .line 801 */
final String v0 = "GpuMemory"; // const-string v0, "GpuMemory"
this.reason = v0;
/* .line 802 */
int v0 = 2; // const/4 v0, 0x2
/* iput v0, p0, Lcom/miui/server/stability/ScoutDisplayMemoryManager$DiaplayMemoryErrorInfo;->type:I */
/* .line 803 */
return;
} // .end method
/* # virtual methods */
public Integer getAction ( ) {
/* .locals 1 */
/* .line 878 */
/* iget v0, p0, Lcom/miui/server/stability/ScoutDisplayMemoryManager$DiaplayMemoryErrorInfo;->action:I */
} // .end method
public java.lang.String getName ( ) {
/* .locals 1 */
/* .line 822 */
v0 = this.procName;
} // .end method
public Integer getOomadj ( ) {
/* .locals 1 */
/* .line 838 */
/* iget v0, p0, Lcom/miui/server/stability/ScoutDisplayMemoryManager$DiaplayMemoryErrorInfo;->adj:I */
} // .end method
public Integer getPid ( ) {
/* .locals 1 */
/* .line 830 */
/* iget v0, p0, Lcom/miui/server/stability/ScoutDisplayMemoryManager$DiaplayMemoryErrorInfo;->pid:I */
} // .end method
public java.lang.String getReason ( ) {
/* .locals 1 */
/* .line 870 */
v0 = this.reason;
} // .end method
public Long getRss ( ) {
/* .locals 2 */
/* .line 846 */
/* iget-wide v0, p0, Lcom/miui/server/stability/ScoutDisplayMemoryManager$DiaplayMemoryErrorInfo;->rss:J */
/* return-wide v0 */
} // .end method
public Long getThreshold ( ) {
/* .locals 2 */
/* .line 854 */
/* iget-wide v0, p0, Lcom/miui/server/stability/ScoutDisplayMemoryManager$DiaplayMemoryErrorInfo;->threshold:J */
/* return-wide v0 */
} // .end method
public Long getTotalSize ( ) {
/* .locals 2 */
/* .line 862 */
/* iget-wide v0, p0, Lcom/miui/server/stability/ScoutDisplayMemoryManager$DiaplayMemoryErrorInfo;->totalSize:J */
/* return-wide v0 */
} // .end method
public Integer getType ( ) {
/* .locals 1 */
/* .line 882 */
/* iget v0, p0, Lcom/miui/server/stability/ScoutDisplayMemoryManager$DiaplayMemoryErrorInfo;->type:I */
} // .end method
public void setAction ( Integer p0 ) {
/* .locals 0 */
/* .param p1, "action" # I */
/* .line 874 */
/* iput p1, p0, Lcom/miui/server/stability/ScoutDisplayMemoryManager$DiaplayMemoryErrorInfo;->action:I */
/* .line 875 */
return;
} // .end method
public void setName ( java.lang.String p0 ) {
/* .locals 0 */
/* .param p1, "procName" # Ljava/lang/String; */
/* .line 818 */
this.procName = p1;
/* .line 819 */
return;
} // .end method
public void setOomadj ( Integer p0 ) {
/* .locals 0 */
/* .param p1, "adj" # I */
/* .line 834 */
/* iput p1, p0, Lcom/miui/server/stability/ScoutDisplayMemoryManager$DiaplayMemoryErrorInfo;->adj:I */
/* .line 835 */
return;
} // .end method
public void setPid ( Integer p0 ) {
/* .locals 0 */
/* .param p1, "pid" # I */
/* .line 826 */
/* iput p1, p0, Lcom/miui/server/stability/ScoutDisplayMemoryManager$DiaplayMemoryErrorInfo;->pid:I */
/* .line 827 */
return;
} // .end method
public void setReason ( java.lang.String p0 ) {
/* .locals 0 */
/* .param p1, "reason" # Ljava/lang/String; */
/* .line 866 */
this.reason = p1;
/* .line 867 */
return;
} // .end method
public void setRss ( Long p0 ) {
/* .locals 0 */
/* .param p1, "rss" # J */
/* .line 842 */
/* iput-wide p1, p0, Lcom/miui/server/stability/ScoutDisplayMemoryManager$DiaplayMemoryErrorInfo;->rss:J */
/* .line 843 */
return;
} // .end method
public void setThreshold ( Long p0 ) {
/* .locals 0 */
/* .param p1, "threshold" # J */
/* .line 850 */
/* iput-wide p1, p0, Lcom/miui/server/stability/ScoutDisplayMemoryManager$DiaplayMemoryErrorInfo;->threshold:J */
/* .line 851 */
return;
} // .end method
public void setTotalSize ( Long p0 ) {
/* .locals 0 */
/* .param p1, "totalSize" # J */
/* .line 858 */
/* iput-wide p1, p0, Lcom/miui/server/stability/ScoutDisplayMemoryManager$DiaplayMemoryErrorInfo;->totalSize:J */
/* .line 859 */
return;
} // .end method
