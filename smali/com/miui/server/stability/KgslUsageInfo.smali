.class public Lcom/miui/server/stability/KgslUsageInfo;
.super Ljava/lang/Object;
.source "KgslUsageInfo.java"


# instance fields
.field private nativeTotalSize:J

.field private runtimeTotalSize:J

.field private totalSize:J

.field private usageList:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList<",
            "Lcom/miui/server/stability/KgslProcUsageInfo;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>()V
    .locals 1

    .line 12
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 13
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/miui/server/stability/KgslUsageInfo;->usageList:Ljava/util/ArrayList;

    .line 14
    return-void
.end method


# virtual methods
.method public add(Lcom/miui/server/stability/KgslProcUsageInfo;)V
    .locals 1
    .param p1, "procInfo"    # Lcom/miui/server/stability/KgslProcUsageInfo;

    .line 17
    iget-object v0, p0, Lcom/miui/server/stability/KgslUsageInfo;->usageList:Ljava/util/ArrayList;

    invoke-virtual {v0, p1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 18
    return-void
.end method

.method public getList()Ljava/util/ArrayList;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/ArrayList<",
            "Lcom/miui/server/stability/KgslProcUsageInfo;",
            ">;"
        }
    .end annotation

    .line 21
    iget-object v0, p0, Lcom/miui/server/stability/KgslUsageInfo;->usageList:Ljava/util/ArrayList;

    return-object v0
.end method

.method public getNativeTotalSize()J
    .locals 2

    .line 45
    iget-wide v0, p0, Lcom/miui/server/stability/KgslUsageInfo;->nativeTotalSize:J

    return-wide v0
.end method

.method public getRuntimeTotalSize()J
    .locals 2

    .line 37
    iget-wide v0, p0, Lcom/miui/server/stability/KgslUsageInfo;->runtimeTotalSize:J

    return-wide v0
.end method

.method public getTotalSize()J
    .locals 2

    .line 29
    iget-wide v0, p0, Lcom/miui/server/stability/KgslUsageInfo;->totalSize:J

    return-wide v0
.end method

.method public setNativeTotalSize(J)V
    .locals 0
    .param p1, "nativeTotalSize"    # J

    .line 41
    iput-wide p1, p0, Lcom/miui/server/stability/KgslUsageInfo;->nativeTotalSize:J

    .line 42
    return-void
.end method

.method public setRuntimeTotalSize(J)V
    .locals 0
    .param p1, "runtimeTotalSize"    # J

    .line 33
    iput-wide p1, p0, Lcom/miui/server/stability/KgslUsageInfo;->runtimeTotalSize:J

    .line 34
    return-void
.end method

.method public setTotalSize(J)V
    .locals 0
    .param p1, "totalSize"    # J

    .line 25
    iput-wide p1, p0, Lcom/miui/server/stability/KgslUsageInfo;->totalSize:J

    .line 26
    return-void
.end method

.method public toString()Ljava/lang/String;
    .locals 5

    .line 49
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    .line 50
    .local v0, "sb":Ljava/lang/StringBuilder;
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Kgsl info total: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-wide v2, p0, Lcom/miui/server/stability/KgslUsageInfo;->totalSize:J

    invoke-virtual {v1, v2, v3}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "kB RuntimeTotalSize="

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-wide v2, p0, Lcom/miui/server/stability/KgslUsageInfo;->runtimeTotalSize:J

    invoke-virtual {v1, v2, v3}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "kB NativeTotalSize="

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-wide v2, p0, Lcom/miui/server/stability/KgslUsageInfo;->nativeTotalSize:J

    invoke-virtual {v1, v2, v3}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "kB\n"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 52
    iget-object v1, p0, Lcom/miui/server/stability/KgslUsageInfo;->usageList:Ljava/util/ArrayList;

    invoke-virtual {v1}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/miui/server/stability/KgslProcUsageInfo;

    .line 53
    .local v2, "info":Lcom/miui/server/stability/KgslProcUsageInfo;
    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v2}, Lcom/miui/server/stability/KgslProcUsageInfo;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, "\n"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 54
    .end local v2    # "info":Lcom/miui/server/stability/KgslProcUsageInfo;
    goto :goto_0

    .line 55
    :cond_0
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    return-object v1
.end method
