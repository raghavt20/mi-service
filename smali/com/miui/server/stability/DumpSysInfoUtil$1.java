class com.miui.server.stability.DumpSysInfoUtil$1 implements java.lang.Runnable {
	 /* .source "DumpSysInfoUtil.java" */
	 /* # interfaces */
	 /* # annotations */
	 /* .annotation system Ldalvik/annotation/EnclosingMethod; */
	 /* value = Lcom/miui/server/stability/DumpSysInfoUtil;->crawlLogsByPower()V */
} // .end annotation
/* .annotation system Ldalvik/annotation/InnerClass; */
/* accessFlags = 0x0 */
/* name = null */
} // .end annotation
/* # direct methods */
 com.miui.server.stability.DumpSysInfoUtil$1 ( ) {
/* .locals 0 */
/* .line 92 */
/* invoke-direct {p0}, Ljava/lang/Object;-><init>()V */
return;
} // .end method
/* # virtual methods */
public void run ( ) {
/* .locals 9 */
/* .line 96 */
final String v0 = "dumpsys"; // const-string v0, "dumpsys"
try { // :try_start_0
	 final String v1 = "dumpsys.txt"; // const-string v1, "dumpsys.txt"
	 com.miui.server.stability.DumpSysInfoUtil .-$$Nest$smcreateFile ( v1 );
	 /* .line 97 */
	 /* .local v1, "path":Ljava/lang/String; */
	 com.miui.server.stability.DumpSysInfoUtil .-$$Nest$smgetmDaemon ( );
	 /* .line 98 */
	 /* .local v2, "mClient":Lmiui/mqsas/IMQSNative; */
	 if ( v2 != null) { // if-eqz v2, :cond_0
		 if ( v1 != null) { // if-eqz v1, :cond_0
			 /* .line 99 */
			 int v3 = 4; // const/4 v3, 0x4
			 /* new-array v4, v3, [Ljava/lang/String; */
			 int v5 = 0; // const/4 v5, 0x0
			 /* aput-object v0, v4, v5 */
			 int v6 = 1; // const/4 v6, 0x1
			 /* aput-object v0, v4, v6 */
			 int v7 = 2; // const/4 v7, 0x2
			 /* aput-object v0, v4, v7 */
			 int v8 = 3; // const/4 v8, 0x3
			 /* aput-object v0, v4, v8 */
			 java.util.Arrays .asList ( v4 );
			 /* .line 101 */
			 /* .local v0, "actions":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;" */
			 /* new-array v3, v3, [Ljava/lang/String; */
			 final String v4 = "SurfaceFlinger"; // const-string v4, "SurfaceFlinger"
			 /* aput-object v4, v3, v5 */
			 final String v4 = "activity activities"; // const-string v4, "activity activities"
			 /* aput-object v4, v3, v6 */
			 /* const-string/jumbo v4, "window" */
			 /* aput-object v4, v3, v7 */
			 final String v4 = "input"; // const-string v4, "input"
			 /* aput-object v4, v3, v8 */
			 java.util.Arrays .asList ( v3 );
			 /* .line 103 */
			 /* .local v3, "params":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;" */
			 /* .line 104 */
			 com.miui.server.stability.DumpSysInfoUtil .-$$Nest$sfgettemporaryDir ( );
			 com.miui.server.stability.DumpSysInfoUtil .-$$Nest$smdeleteMissFetchByPower ( v4 );
			 /* :try_end_0 */
			 /* .catch Ljava/lang/Exception; {:try_start_0 ..:try_end_0} :catch_0 */
			 /* .line 108 */
		 } // .end local v0 # "actions":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;"
	 } // .end local v1 # "path":Ljava/lang/String;
} // .end local v2 # "mClient":Lmiui/mqsas/IMQSNative;
} // .end local v3 # "params":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;"
} // :cond_0
/* .line 106 */
/* :catch_0 */
/* move-exception v0 */
/* .line 107 */
/* .local v0, "e":Ljava/lang/Exception; */
final String v1 = "DumpSysInfoUtil"; // const-string v1, "DumpSysInfoUtil"
final String v2 = "crash in the crawlDumpsysLogs()"; // const-string v2, "crash in the crawlDumpsysLogs()"
android.util.Slog .e ( v1,v2,v0 );
/* .line 109 */
} // .end local v0 # "e":Ljava/lang/Exception;
} // :goto_0
return;
} // .end method
