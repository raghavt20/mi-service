public class com.miui.server.stability.ScoutMemoryUtils {
	 /* .source "ScoutMemoryUtils.java" */
	 /* # static fields */
	 public static final Integer DIR_DMABUF_ID;
	 public static final Integer DIR_GPU_MEMORY_ID;
	 public static final Integer DIR_PROCS_MEMORY_ID;
	 private static final java.lang.String FILE_DUMP_SUFFIX;
	 public static final java.lang.String FILE_KGSL;
	 public static final java.lang.String FILE_MALI;
	 public static final Integer GPU_TYPE_KGSL;
	 public static final Integer GPU_TYPE_MTK_MALI;
	 public static final Integer GPU_TYPE_UNKNOW;
	 private static final Integer MAX_MEMLEAK_FILE;
	 private static final java.lang.String MEMLEAK;
	 private static final java.lang.String PROC_MEMINFO;
	 private static final java.lang.String PROC_SLABINFO;
	 private static final java.lang.String PROC_VMALLOCINFO;
	 private static final java.lang.String PROC_VMSTAT;
	 private static final java.lang.String PROC_ZONEINFO;
	 private static final java.lang.String TAG;
	 public static final java.util.HashMap dirMap;
	 /* .annotation system Ldalvik/annotation/Signature; */
	 /* value = { */
	 /* "Ljava/util/HashMap<", */
	 /* "Ljava/lang/Integer;", */
	 /* "Ljava/lang/String;", */
	 /* ">;" */
	 /* } */
} // .end annotation
} // .end field
public static final java.util.Set gpuMemoryWhiteList;
/* .annotation system Ldalvik/annotation/Signature; */
/* value = { */
/* "Ljava/util/Set<", */
/* "Ljava/lang/String;", */
/* ">;" */
/* } */
} // .end annotation
} // .end field
private static java.lang.reflect.Method sGetTagMethod;
private static java.lang.reflect.Method sGetTagTypeMethod;
private static java.lang.reflect.Method sGetTagValueMethod;
public static final java.util.Set skipIonProcList;
/* .annotation system Ldalvik/annotation/Signature; */
/* value = { */
/* "Ljava/util/Set<", */
/* "Ljava/lang/String;", */
/* ">;" */
/* } */
} // .end annotation
} // .end field
/* # direct methods */
static com.miui.server.stability.ScoutMemoryUtils ( ) {
/* .locals 11 */
/* .line 61 */
/* new-instance v0, Lcom/miui/server/stability/ScoutMemoryUtils$1; */
/* invoke-direct {v0}, Lcom/miui/server/stability/ScoutMemoryUtils$1;-><init>()V */
/* .line 69 */
/* new-instance v0, Ljava/util/HashSet; */
final String v1 = "/system/bin/surfaceflinger"; // const-string v1, "/system/bin/surfaceflinger"
final String v2 = "/system/bin/cameraserver"; // const-string v2, "/system/bin/cameraserver"
final String v3 = "com.android.camera"; // const-string v3, "com.android.camera"
final String v4 = "/vendor/bin/hw/vendor.qti.hardware.display.composer-service"; // const-string v4, "/vendor/bin/hw/vendor.qti.hardware.display.composer-service"
final String v5 = "/vendor/bin/hw/android.hardware.graphics.composer@2.3-service"; // const-string v5, "/vendor/bin/hw/android.hardware.graphics.composer@2.3-service"
final String v6 = "/vendor/bin/hw/vendor.qti.camera.provider@2.7-service_64"; // const-string v6, "/vendor/bin/hw/vendor.qti.camera.provider@2.7-service_64"
final String v7 = "/vendor/bin/hw/android.hardware.camera.provider@2.4-service_64"; // const-string v7, "/vendor/bin/hw/android.hardware.camera.provider@2.4-service_64"
final String v8 = "/vendor/bin/hw/camerahalserver"; // const-string v8, "/vendor/bin/hw/camerahalserver"
final String v9 = "/vendor/bin/hw/vendor.qti.camera.provider-service_64"; // const-string v9, "/vendor/bin/hw/vendor.qti.camera.provider-service_64"
final String v10 = "/vendor/bin/hw/android.hardware.graphics.composer@2.1-service"; // const-string v10, "/vendor/bin/hw/android.hardware.graphics.composer@2.1-service"
/* filled-new-array/range {v1 ..v10}, [Ljava/lang/String; */
/* .line 70 */
java.util.Arrays .asList ( v1 );
/* invoke-direct {v0, v1}, Ljava/util/HashSet;-><init>(Ljava/util/Collection;)V */
/* .line 83 */
/* new-instance v0, Ljava/util/HashSet; */
final String v1 = "com.antutu.benchmark.full:era"; // const-string v1, "com.antutu.benchmark.full:era"
final String v2 = "com.antutu.benchmark.full:unity"; // const-string v2, "com.antutu.benchmark.full:unity"
/* filled-new-array {v1, v2}, [Ljava/lang/String; */
/* .line 84 */
java.util.Arrays .asList ( v1 );
/* invoke-direct {v0, v1}, Ljava/util/HashSet;-><init>(Ljava/util/Collection;)V */
/* .line 281 */
int v0 = 0; // const/4 v0, 0x0
/* .line 282 */
/* .line 283 */
return;
} // .end method
public com.miui.server.stability.ScoutMemoryUtils ( ) {
/* .locals 0 */
/* .line 38 */
/* invoke-direct {p0}, Ljava/lang/Object;-><init>()V */
return;
} // .end method
public static java.lang.String captureGpuMemoryLeakLog ( java.lang.String p0, java.lang.String p1, Integer p2 ) {
/* .locals 6 */
/* .param p0, "gpuMemoryInfo" # Ljava/lang/String; */
/* .param p1, "reason" # Ljava/lang/String; */
/* .param p2, "type" # I */
/* .line 394 */
com.miui.server.stability.ScoutMemoryUtils .deleteOldKgslDir ( );
/* .line 395 */
int v0 = 2; // const/4 v0, 0x2
com.miui.server.stability.ScoutMemoryUtils .deleteMemLeakFile ( v0 );
/* .line 396 */
com.miui.server.stability.ScoutMemoryUtils .getExceptionFile ( p1,v0 );
/* .line 397 */
/* .local v1, "exectionFile":Ljava/io/File; */
com.miui.server.stability.ScoutMemoryUtils .dumpProcInfo ( p0,v1,p1 );
/* .line 398 */
com.miui.server.stability.ScoutMemoryUtils .dumpGpuMemoryInfo ( v1,p2 );
/* .line 399 */
com.miui.server.stability.ScoutMemoryUtils .dumpMemoryInfo ( v1 );
/* .line 400 */
final String v2 = ""; // const-string v2, ""
/* .line 401 */
/* .local v2, "zipPath":Ljava/lang/String; */
/* new-instance v3, Lcom/android/server/ScoutHelper$Action; */
/* invoke-direct {v3}, Lcom/android/server/ScoutHelper$Action;-><init>()V */
/* .line 402 */
/* .local v3, "action":Lcom/android/server/ScoutHelper$Action; */
final String v4 = "meminfo"; // const-string v4, "meminfo"
final String v5 = "dumpsys"; // const-string v5, "dumpsys"
(( com.android.server.ScoutHelper$Action ) v3 ).addActionAndParam ( v5, v4 ); // invoke-virtual {v3, v5, v4}, Lcom/android/server/ScoutHelper$Action;->addActionAndParam(Ljava/lang/String;Ljava/lang/String;)V
/* .line 403 */
final String v4 = "SurfaceFlinger"; // const-string v4, "SurfaceFlinger"
(( com.android.server.ScoutHelper$Action ) v3 ).addActionAndParam ( v5, v4 ); // invoke-virtual {v3, v5, v4}, Lcom/android/server/ScoutHelper$Action;->addActionAndParam(Ljava/lang/String;Ljava/lang/String;)V
/* .line 404 */
final String v4 = "gfxinfo"; // const-string v4, "gfxinfo"
(( com.android.server.ScoutHelper$Action ) v3 ).addActionAndParam ( v5, v4 ); // invoke-virtual {v3, v5, v4}, Lcom/android/server/ScoutHelper$Action;->addActionAndParam(Ljava/lang/String;Ljava/lang/String;)V
/* .line 405 */
v4 = miui.mqsas.scout.ScoutUtils .isLibraryTest ( );
if ( v4 != null) { // if-eqz v4, :cond_0
/* .line 406 */
final String v4 = "logcat"; // const-string v4, "logcat"
final String v5 = "-b main,system,crash,events"; // const-string v5, "-b main,system,crash,events"
(( com.android.server.ScoutHelper$Action ) v3 ).addActionAndParam ( v4, v5 ); // invoke-virtual {v3, v4, v5}, Lcom/android/server/ScoutHelper$Action;->addActionAndParam(Ljava/lang/String;Ljava/lang/String;)V
/* .line 408 */
} // :cond_0
if ( v1 != null) { // if-eqz v1, :cond_1
/* .line 409 */
(( java.io.File ) v1 ).getAbsolutePath ( ); // invoke-virtual {v1}, Ljava/io/File;->getAbsolutePath()Ljava/lang/String;
(( com.android.server.ScoutHelper$Action ) v3 ).addIncludeFile ( v4 ); // invoke-virtual {v3, v4}, Lcom/android/server/ScoutHelper$Action;->addIncludeFile(Ljava/lang/String;)V
/* .line 411 */
} // :cond_1
com.miui.server.stability.ScoutMemoryUtils .getMemLeakDir ( v0 );
/* .line 412 */
/* .local v0, "memleakDir":Ljava/io/File; */
if ( v0 != null) { // if-eqz v0, :cond_2
/* .line 413 */
/* new-instance v4, Ljava/lang/StringBuilder; */
/* invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V */
(( java.io.File ) v0 ).getAbsolutePath ( ); // invoke-virtual {v0}, Ljava/io/File;->getAbsolutePath()Ljava/lang/String;
(( java.lang.StringBuilder ) v4 ).append ( v5 ); // invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
final String v5 = "/"; // const-string v5, "/"
(( java.lang.StringBuilder ) v4 ).append ( v5 ); // invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v4 ).toString ( ); // invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
final String v5 = "memleak"; // const-string v5, "memleak"
com.android.server.ScoutHelper .dumpOfflineLog ( p1,v3,v5,v4 );
/* .line 415 */
} // :cond_2
} // .end method
public static java.lang.String captureIonLeakLog ( java.lang.String p0, java.lang.String p1, java.util.HashSet p2 ) {
/* .locals 7 */
/* .param p0, "dmabufInfo" # Ljava/lang/String; */
/* .param p1, "reason" # Ljava/lang/String; */
/* .annotation system Ldalvik/annotation/Signature; */
/* value = { */
/* "(", */
/* "Ljava/lang/String;", */
/* "Ljava/lang/String;", */
/* "Ljava/util/HashSet<", */
/* "Ljava/lang/Integer;", */
/* ">;)", */
/* "Ljava/lang/String;" */
/* } */
} // .end annotation
/* .line 364 */
/* .local p2, "fdPids":Ljava/util/HashSet;, "Ljava/util/HashSet<Ljava/lang/Integer;>;" */
int v0 = 1; // const/4 v0, 0x1
com.miui.server.stability.ScoutMemoryUtils .deleteMemLeakFile ( v0 );
/* .line 365 */
com.miui.server.stability.ScoutMemoryUtils .getExceptionFile ( p1,v0 );
/* .line 366 */
/* .local v1, "exectionFile":Ljava/io/File; */
final String v2 = ""; // const-string v2, ""
/* .line 367 */
/* .local v2, "zipPath":Ljava/lang/String; */
com.miui.server.stability.ScoutMemoryUtils .dumpProcInfo ( p0,v1,p1 );
/* .line 368 */
v3 = (( java.util.HashSet ) p2 ).size ( ); // invoke-virtual {p2}, Ljava/util/HashSet;->size()I
/* if-lez v3, :cond_0 */
/* .line 369 */
(( java.util.HashSet ) p2 ).iterator ( ); // invoke-virtual {p2}, Ljava/util/HashSet;->iterator()Ljava/util/Iterator;
v4 = } // :goto_0
if ( v4 != null) { // if-eqz v4, :cond_0
/* check-cast v4, Ljava/lang/Integer; */
v4 = (( java.lang.Integer ) v4 ).intValue ( ); // invoke-virtual {v4}, Ljava/lang/Integer;->intValue()I
/* .line 370 */
/* .local v4, "fdPid":I */
/* new-instance v5, Ljava/lang/StringBuilder; */
/* invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V */
final String v6 = "captureIonLeakLog: "; // const-string v6, "captureIonLeakLog: "
(( java.lang.StringBuilder ) v5 ).append ( v6 ); // invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v5 ).append ( v4 ); // invoke-virtual {v5, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v5 ).toString ( ); // invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
final String v6 = "ScoutMemoryUtils"; // const-string v6, "ScoutMemoryUtils"
android.util.Slog .w ( v6,v5 );
/* .line 371 */
com.miui.server.stability.ScoutMemoryUtils .dumpOpenFds ( v4,v1 );
/* .line 372 */
} // .end local v4 # "fdPid":I
/* .line 374 */
} // :cond_0
/* new-instance v3, Lcom/android/server/ScoutHelper$Action; */
/* invoke-direct {v3}, Lcom/android/server/ScoutHelper$Action;-><init>()V */
/* .line 375 */
/* .local v3, "action":Lcom/android/server/ScoutHelper$Action; */
final String v4 = "SurfaceFlinger"; // const-string v4, "SurfaceFlinger"
final String v5 = "dumpsys"; // const-string v5, "dumpsys"
(( com.android.server.ScoutHelper$Action ) v3 ).addActionAndParam ( v5, v4 ); // invoke-virtual {v3, v5, v4}, Lcom/android/server/ScoutHelper$Action;->addActionAndParam(Ljava/lang/String;Ljava/lang/String;)V
/* .line 376 */
final String v4 = "gfxinfo"; // const-string v4, "gfxinfo"
(( com.android.server.ScoutHelper$Action ) v3 ).addActionAndParam ( v5, v4 ); // invoke-virtual {v3, v5, v4}, Lcom/android/server/ScoutHelper$Action;->addActionAndParam(Ljava/lang/String;Ljava/lang/String;)V
/* .line 377 */
final String v4 = "activity"; // const-string v4, "activity"
(( com.android.server.ScoutHelper$Action ) v3 ).addActionAndParam ( v5, v4 ); // invoke-virtual {v3, v5, v4}, Lcom/android/server/ScoutHelper$Action;->addActionAndParam(Ljava/lang/String;Ljava/lang/String;)V
/* .line 378 */
/* const-string/jumbo v4, "window" */
(( com.android.server.ScoutHelper$Action ) v3 ).addActionAndParam ( v5, v4 ); // invoke-virtual {v3, v5, v4}, Lcom/android/server/ScoutHelper$Action;->addActionAndParam(Ljava/lang/String;Ljava/lang/String;)V
/* .line 379 */
final String v4 = "meminfo"; // const-string v4, "meminfo"
(( com.android.server.ScoutHelper$Action ) v3 ).addActionAndParam ( v5, v4 ); // invoke-virtual {v3, v5, v4}, Lcom/android/server/ScoutHelper$Action;->addActionAndParam(Ljava/lang/String;Ljava/lang/String;)V
/* .line 380 */
v4 = miui.mqsas.scout.ScoutUtils .isLibraryTest ( );
if ( v4 != null) { // if-eqz v4, :cond_1
/* .line 381 */
final String v4 = "logcat"; // const-string v4, "logcat"
final String v5 = "-b main,system,crash,events"; // const-string v5, "-b main,system,crash,events"
(( com.android.server.ScoutHelper$Action ) v3 ).addActionAndParam ( v4, v5 ); // invoke-virtual {v3, v4, v5}, Lcom/android/server/ScoutHelper$Action;->addActionAndParam(Ljava/lang/String;Ljava/lang/String;)V
/* .line 383 */
} // :cond_1
if ( v1 != null) { // if-eqz v1, :cond_2
/* .line 384 */
(( java.io.File ) v1 ).getAbsolutePath ( ); // invoke-virtual {v1}, Ljava/io/File;->getAbsolutePath()Ljava/lang/String;
(( com.android.server.ScoutHelper$Action ) v3 ).addIncludeFile ( v4 ); // invoke-virtual {v3, v4}, Lcom/android/server/ScoutHelper$Action;->addIncludeFile(Ljava/lang/String;)V
/* .line 386 */
} // :cond_2
com.miui.server.stability.ScoutMemoryUtils .getMemLeakDir ( v0 );
/* .line 387 */
/* .local v0, "memleakDir":Ljava/io/File; */
if ( v0 != null) { // if-eqz v0, :cond_3
/* .line 388 */
/* new-instance v4, Ljava/lang/StringBuilder; */
/* invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V */
(( java.io.File ) v0 ).getAbsolutePath ( ); // invoke-virtual {v0}, Ljava/io/File;->getAbsolutePath()Ljava/lang/String;
(( java.lang.StringBuilder ) v4 ).append ( v5 ); // invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
final String v5 = "/"; // const-string v5, "/"
(( java.lang.StringBuilder ) v4 ).append ( v5 ); // invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v4 ).toString ( ); // invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
final String v5 = "memleak"; // const-string v5, "memleak"
com.android.server.ScoutHelper .dumpOfflineLog ( p1,v3,v5,v4 );
/* .line 390 */
} // :cond_3
} // .end method
public static void deleteMemLeakFile ( Integer p0 ) {
/* .locals 9 */
/* .param p0, "dirId" # I */
/* .line 158 */
/* new-instance v0, Ljava/util/TreeSet; */
/* invoke-direct {v0}, Ljava/util/TreeSet;-><init>()V */
/* .line 159 */
/* .local v0, "existinglog":Ljava/util/TreeSet;, "Ljava/util/TreeSet<Ljava/io/File;>;" */
com.miui.server.stability.ScoutMemoryUtils .getMemLeakDir ( p0 );
/* .line 160 */
/* .local v1, "memleakDir":Ljava/io/File; */
/* if-nez v1, :cond_0 */
return;
/* .line 161 */
} // :cond_0
(( java.io.File ) v1 ).listFiles ( ); // invoke-virtual {v1}, Ljava/io/File;->listFiles()[Ljava/io/File;
/* array-length v3, v2 */
int v4 = 0; // const/4 v4, 0x0
} // :goto_0
final String v5 = "ScoutMemoryUtils"; // const-string v5, "ScoutMemoryUtils"
/* if-ge v4, v3, :cond_3 */
/* aget-object v6, v2, v4 */
/* .line 162 */
/* .local v6, "file":Ljava/io/File; */
(( java.io.File ) v6 ).getName ( ); // invoke-virtual {v6}, Ljava/io/File;->getName()Ljava/lang/String;
final String v8 = ".txt"; // const-string v8, ".txt"
v7 = (( java.lang.String ) v7 ).endsWith ( v8 ); // invoke-virtual {v7, v8}, Ljava/lang/String;->endsWith(Ljava/lang/String;)Z
if ( v7 != null) { // if-eqz v7, :cond_1
/* .line 163 */
v7 = (( java.io.File ) v6 ).delete ( ); // invoke-virtual {v6}, Ljava/io/File;->delete()Z
/* if-nez v7, :cond_2 */
/* .line 164 */
/* new-instance v7, Ljava/lang/StringBuilder; */
/* invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V */
final String v8 = "Failed to clean up memleak txt file:"; // const-string v8, "Failed to clean up memleak txt file:"
(( java.lang.StringBuilder ) v7 ).append ( v8 ); // invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v7 ).append ( v6 ); // invoke-virtual {v7, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v7 ).toString ( ); // invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
android.util.Slog .w ( v5,v7 );
/* .line 168 */
} // :cond_1
(( java.util.TreeSet ) v0 ).add ( v6 ); // invoke-virtual {v0, v6}, Ljava/util/TreeSet;->add(Ljava/lang/Object;)Z
/* .line 161 */
} // .end local v6 # "file":Ljava/io/File;
} // :cond_2
} // :goto_1
/* add-int/lit8 v4, v4, 0x1 */
/* .line 170 */
} // :cond_3
v2 = (( java.util.TreeSet ) v0 ).size ( ); // invoke-virtual {v0}, Ljava/util/TreeSet;->size()I
int v3 = 2; // const/4 v3, 0x2
/* if-lt v2, v3, :cond_6 */
/* .line 171 */
int v2 = 0; // const/4 v2, 0x0
/* .local v2, "i":I */
} // :goto_2
int v3 = 1; // const/4 v3, 0x1
/* if-ge v2, v3, :cond_4 */
/* .line 172 */
(( java.util.TreeSet ) v0 ).pollLast ( ); // invoke-virtual {v0}, Ljava/util/TreeSet;->pollLast()Ljava/lang/Object;
/* .line 171 */
/* add-int/lit8 v2, v2, 0x1 */
/* .line 174 */
} // .end local v2 # "i":I
} // :cond_4
(( java.util.TreeSet ) v0 ).iterator ( ); // invoke-virtual {v0}, Ljava/util/TreeSet;->iterator()Ljava/util/Iterator;
v3 = } // :goto_3
if ( v3 != null) { // if-eqz v3, :cond_6
/* check-cast v3, Ljava/io/File; */
/* .line 175 */
/* .local v3, "file":Ljava/io/File; */
v4 = (( java.io.File ) v3 ).delete ( ); // invoke-virtual {v3}, Ljava/io/File;->delete()Z
/* if-nez v4, :cond_5 */
/* .line 176 */
/* new-instance v4, Ljava/lang/StringBuilder; */
/* invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V */
final String v6 = "Failed to clean up memleak log:"; // const-string v6, "Failed to clean up memleak log:"
(( java.lang.StringBuilder ) v4 ).append ( v6 ); // invoke-virtual {v4, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v4 ).append ( v3 ); // invoke-virtual {v4, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v4 ).toString ( ); // invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
android.util.Slog .w ( v5,v4 );
/* .line 178 */
} // .end local v3 # "file":Ljava/io/File;
} // :cond_5
/* .line 180 */
} // :cond_6
return;
} // .end method
public static void deleteOldFiles ( Integer p0, Integer p1 ) {
/* .locals 5 */
/* .param p0, "dirId" # I */
/* .param p1, "limit" # I */
/* .line 183 */
com.miui.server.stability.ScoutMemoryUtils .getMemLeakDir ( p0 );
/* .line 184 */
/* .local v0, "memleakDir":Ljava/io/File; */
/* if-nez v0, :cond_0 */
return;
/* .line 185 */
} // :cond_0
(( java.io.File ) v0 ).listFiles ( ); // invoke-virtual {v0}, Ljava/io/File;->listFiles()[Ljava/io/File;
/* .line 186 */
/* .local v1, "files":[Ljava/io/File; */
/* array-length v2, v1 */
/* if-gt v2, p1, :cond_1 */
/* .line 187 */
return;
/* .line 189 */
} // :cond_1
/* new-instance v2, Lcom/miui/server/stability/ScoutMemoryUtils$$ExternalSyntheticLambda0; */
/* invoke-direct {v2}, Lcom/miui/server/stability/ScoutMemoryUtils$$ExternalSyntheticLambda0;-><init>()V */
java.util.Comparator .comparingLong ( v2 );
java.util.Arrays .sort ( v1,v2 );
/* .line 190 */
int v2 = 0; // const/4 v2, 0x0
/* .local v2, "i":I */
} // :goto_0
/* array-length v3, v1 */
/* sub-int/2addr v3, p1 */
/* if-ge v2, v3, :cond_3 */
/* .line 191 */
/* aget-object v3, v1, v2 */
v3 = (( java.io.File ) v3 ).delete ( ); // invoke-virtual {v3}, Ljava/io/File;->delete()Z
/* if-nez v3, :cond_2 */
/* .line 192 */
/* new-instance v3, Ljava/lang/StringBuilder; */
/* invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V */
final String v4 = "Failed to delete log file: "; // const-string v4, "Failed to delete log file: "
(( java.lang.StringBuilder ) v3 ).append ( v4 ); // invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
/* aget-object v4, v1, v2 */
(( java.lang.StringBuilder ) v3 ).append ( v4 ); // invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v3 ).toString ( ); // invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
final String v4 = "ScoutMemoryUtils"; // const-string v4, "ScoutMemoryUtils"
android.util.Slog .w ( v4,v3 );
/* .line 190 */
} // :cond_2
/* add-int/lit8 v2, v2, 0x1 */
/* .line 195 */
} // .end local v2 # "i":I
} // :cond_3
return;
} // .end method
public static void deleteOldKgslDir ( ) {
/* .locals 8 */
/* .line 136 */
/* new-instance v0, Ljava/io/File; */
final String v1 = "/data/miuilog/stability"; // const-string v1, "/data/miuilog/stability"
/* invoke-direct {v0, v1}, Ljava/io/File;-><init>(Ljava/lang/String;)V */
/* .line 137 */
/* .local v0, "stDir":Ljava/io/File; */
v1 = (( java.io.File ) v0 ).exists ( ); // invoke-virtual {v0}, Ljava/io/File;->exists()Z
final String v2 = "ScoutMemoryUtils"; // const-string v2, "ScoutMemoryUtils"
/* if-nez v1, :cond_0 */
/* .line 138 */
/* const-string/jumbo v1, "stability log dir isn\'t exists" */
android.util.Slog .e ( v2,v1 );
/* .line 139 */
return;
/* .line 141 */
} // :cond_0
/* new-instance v1, Ljava/io/File; */
final String v3 = "memleak"; // const-string v3, "memleak"
/* invoke-direct {v1, v0, v3}, Ljava/io/File;-><init>(Ljava/io/File;Ljava/lang/String;)V */
/* .line 142 */
/* .local v1, "memleakDir":Ljava/io/File; */
v3 = (( java.io.File ) v1 ).exists ( ); // invoke-virtual {v1}, Ljava/io/File;->exists()Z
/* if-nez v3, :cond_1 */
/* .line 143 */
final String v3 = "memleak dir isn\'t exists"; // const-string v3, "memleak dir isn\'t exists"
android.util.Slog .e ( v2,v3 );
/* .line 144 */
return;
/* .line 146 */
} // :cond_1
/* new-instance v3, Ljava/io/File; */
final String v4 = "kgsl"; // const-string v4, "kgsl"
/* invoke-direct {v3, v1, v4}, Ljava/io/File;-><init>(Ljava/io/File;Ljava/lang/String;)V */
/* .line 147 */
/* .local v3, "kgslDir":Ljava/io/File; */
v4 = (( java.io.File ) v3 ).exists ( ); // invoke-virtual {v3}, Ljava/io/File;->exists()Z
if ( v4 != null) { // if-eqz v4, :cond_3
v4 = (( java.io.File ) v3 ).isDirectory ( ); // invoke-virtual {v3}, Ljava/io/File;->isDirectory()Z
if ( v4 != null) { // if-eqz v4, :cond_3
/* .line 148 */
(( java.io.File ) v3 ).listFiles ( ); // invoke-virtual {v3}, Ljava/io/File;->listFiles()[Ljava/io/File;
/* .line 149 */
/* .local v4, "listFiles":[Ljava/io/File; */
/* array-length v5, v4 */
int v6 = 0; // const/4 v6, 0x0
} // :goto_0
/* if-ge v6, v5, :cond_2 */
/* aget-object v7, v4, v6 */
/* .line 150 */
/* .local v7, "file":Ljava/io/File; */
(( java.io.File ) v7 ).delete ( ); // invoke-virtual {v7}, Ljava/io/File;->delete()Z
/* .line 149 */
} // .end local v7 # "file":Ljava/io/File;
/* add-int/lit8 v6, v6, 0x1 */
/* .line 152 */
} // :cond_2
(( java.io.File ) v3 ).delete ( ); // invoke-virtual {v3}, Ljava/io/File;->delete()Z
/* .line 153 */
final String v5 = "delete kgsl dir"; // const-string v5, "delete kgsl dir"
android.util.Slog .d ( v2,v5 );
/* .line 155 */
} // .end local v4 # "listFiles":[Ljava/io/File;
} // :cond_3
return;
} // .end method
public static void doFsyncZipFile ( Integer p0 ) {
/* .locals 9 */
/* .param p0, "dirId" # I */
/* .line 198 */
/* new-instance v0, Ljava/util/TreeSet; */
/* invoke-direct {v0}, Ljava/util/TreeSet;-><init>()V */
/* .line 199 */
/* .local v0, "existinglog":Ljava/util/TreeSet;, "Ljava/util/TreeSet<Ljava/io/File;>;" */
com.miui.server.stability.ScoutMemoryUtils .getMemLeakDir ( p0 );
/* .line 200 */
/* .local v1, "memleakDir":Ljava/io/File; */
/* if-nez v1, :cond_0 */
return;
/* .line 201 */
} // :cond_0
(( java.io.File ) v1 ).listFiles ( ); // invoke-virtual {v1}, Ljava/io/File;->listFiles()[Ljava/io/File;
/* array-length v3, v2 */
int v4 = 0; // const/4 v4, 0x0
/* move v5, v4 */
} // :goto_0
/* if-ge v5, v3, :cond_2 */
/* aget-object v6, v2, v5 */
/* .line 202 */
/* .local v6, "file":Ljava/io/File; */
(( java.io.File ) v6 ).getName ( ); // invoke-virtual {v6}, Ljava/io/File;->getName()Ljava/lang/String;
final String v8 = ".zip"; // const-string v8, ".zip"
v7 = (( java.lang.String ) v7 ).endsWith ( v8 ); // invoke-virtual {v7, v8}, Ljava/lang/String;->endsWith(Ljava/lang/String;)Z
if ( v7 != null) { // if-eqz v7, :cond_1
/* .line 203 */
(( java.util.TreeSet ) v0 ).add ( v6 ); // invoke-virtual {v0, v6}, Ljava/util/TreeSet;->add(Ljava/lang/Object;)Z
/* .line 201 */
} // .end local v6 # "file":Ljava/io/File;
} // :cond_1
/* add-int/lit8 v5, v5, 0x1 */
/* .line 206 */
} // :cond_2
(( java.util.TreeSet ) v0 ).pollLast ( ); // invoke-virtual {v0}, Ljava/util/TreeSet;->pollLast()Ljava/lang/Object;
/* check-cast v2, Ljava/io/File; */
/* .line 207 */
/* .local v2, "fsyncFile":Ljava/io/File; */
int v3 = 0; // const/4 v3, 0x0
/* .line 209 */
/* .local v3, "fd":Ljava/io/FileDescriptor; */
try { // :try_start_0
(( java.io.File ) v2 ).getAbsolutePath ( ); // invoke-virtual {v2}, Ljava/io/File;->getAbsolutePath()Ljava/lang/String;
android.system.Os .open ( v5,v6,v4 );
/* move-object v3, v4 */
/* .line 210 */
android.system.Os .fsync ( v3 );
/* .line 211 */
android.system.Os .close ( v3 );
/* .line 212 */
final String v4 = "ScoutMemoryUtils"; // const-string v4, "ScoutMemoryUtils"
/* new-instance v5, Ljava/lang/StringBuilder; */
/* invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V */
final String v6 = "finish fsync file: "; // const-string v6, "finish fsync file: "
(( java.lang.StringBuilder ) v5 ).append ( v6 ); // invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.io.File ) v2 ).getName ( ); // invoke-virtual {v2}, Ljava/io/File;->getName()Ljava/lang/String;
(( java.lang.StringBuilder ) v5 ).append ( v6 ); // invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v5 ).toString ( ); // invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
android.util.Slog .w ( v4,v5 );
/* :try_end_0 */
/* .catch Ljava/lang/Exception; {:try_start_0 ..:try_end_0} :catch_0 */
/* .catchall {:try_start_0 ..:try_end_0} :catchall_0 */
/* .line 216 */
/* :catchall_0 */
/* move-exception v4 */
/* .line 213 */
/* :catch_0 */
/* move-exception v4 */
/* .line 214 */
/* .local v4, "e":Ljava/lang/Exception; */
try { // :try_start_1
(( java.lang.Exception ) v4 ).printStackTrace ( ); // invoke-virtual {v4}, Ljava/lang/Exception;->printStackTrace()V
/* :try_end_1 */
/* .catchall {:try_start_1 ..:try_end_1} :catchall_0 */
/* .line 216 */
} // .end local v4 # "e":Ljava/lang/Exception;
} // :goto_1
libcore.io.IoUtils .closeQuietly ( v3 );
/* .line 217 */
/* nop */
/* .line 218 */
return;
/* .line 216 */
} // :goto_2
libcore.io.IoUtils .closeQuietly ( v3 );
/* .line 217 */
/* throw v4 */
} // .end method
public static void dumpGpuMemoryInfo ( java.io.File p0, Integer p1 ) {
/* .locals 1 */
/* .param p0, "file" # Ljava/io/File; */
/* .param p1, "type" # I */
/* .line 274 */
int v0 = 1; // const/4 v0, 0x1
/* if-ne p1, v0, :cond_0 */
/* .line 275 */
final String v0 = "/sys/class/kgsl/kgsl/page_alloc"; // const-string v0, "/sys/class/kgsl/kgsl/page_alloc"
com.miui.server.stability.ScoutMemoryUtils .dumpInfo ( v0,p0 );
/* .line 276 */
} // :cond_0
int v0 = 2; // const/4 v0, 0x2
/* if-ne p1, v0, :cond_1 */
/* .line 277 */
final String v0 = "/proc/mtk_mali/gpu_memory"; // const-string v0, "/proc/mtk_mali/gpu_memory"
com.miui.server.stability.ScoutMemoryUtils .dumpInfo ( v0,p0 );
/* .line 279 */
} // :cond_1
} // :goto_0
return;
} // .end method
public static void dumpInfo ( java.lang.String p0, java.io.File p1 ) {
/* .locals 7 */
/* .param p0, "path" # Ljava/lang/String; */
/* .param p1, "file" # Ljava/io/File; */
/* .line 245 */
/* new-instance v0, Ljava/lang/StringBuilder; */
/* invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V */
/* .line 246 */
/* .local v0, "builder":Ljava/lang/StringBuilder; */
/* new-instance v1, Ljava/io/File; */
/* invoke-direct {v1, p0}, Ljava/io/File;-><init>(Ljava/lang/String;)V */
/* .line 247 */
/* .local v1, "readerFile":Ljava/io/File; */
/* new-instance v2, Ljava/lang/StringBuilder; */
/* invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V */
final String v3 = "---Dump "; // const-string v3, "---Dump "
(( java.lang.StringBuilder ) v2 ).append ( v3 ); // invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v2 ).append ( p0 ); // invoke-virtual {v2, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
final String v4 = " START---\n"; // const-string v4, " START---\n"
(( java.lang.StringBuilder ) v2 ).append ( v4 ); // invoke-virtual {v2, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v2 ).toString ( ); // invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
(( java.lang.StringBuilder ) v0 ).append ( v2 ); // invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
/* .line 248 */
try { // :try_start_0
/* new-instance v2, Ljava/io/BufferedReader; */
/* new-instance v4, Ljava/io/FileReader; */
/* invoke-direct {v4, v1}, Ljava/io/FileReader;-><init>(Ljava/io/File;)V */
/* invoke-direct {v2, v4}, Ljava/io/BufferedReader;-><init>(Ljava/io/Reader;)V */
/* :try_end_0 */
/* .catch Ljava/lang/Exception; {:try_start_0 ..:try_end_0} :catch_0 */
/* .line 249 */
/* .local v2, "reader":Ljava/io/BufferedReader; */
int v4 = 0; // const/4 v4, 0x0
/* .line 250 */
/* .local v4, "readInfo":Ljava/lang/String; */
} // :goto_0
try { // :try_start_1
(( java.io.BufferedReader ) v2 ).readLine ( ); // invoke-virtual {v2}, Ljava/io/BufferedReader;->readLine()Ljava/lang/String;
/* move-object v4, v5 */
if ( v5 != null) { // if-eqz v5, :cond_0
/* .line 251 */
/* new-instance v5, Ljava/lang/StringBuilder; */
/* invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V */
(( java.lang.StringBuilder ) v5 ).append ( v4 ); // invoke-virtual {v5, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
final String v6 = "\n"; // const-string v6, "\n"
(( java.lang.StringBuilder ) v5 ).append ( v6 ); // invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v5 ).toString ( ); // invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
(( java.lang.StringBuilder ) v0 ).append ( v5 ); // invoke-virtual {v0, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
/* :try_end_1 */
/* .catchall {:try_start_1 ..:try_end_1} :catchall_0 */
/* .line 253 */
} // .end local v4 # "readInfo":Ljava/lang/String;
} // :cond_0
try { // :try_start_2
(( java.io.BufferedReader ) v2 ).close ( ); // invoke-virtual {v2}, Ljava/io/BufferedReader;->close()V
/* :try_end_2 */
/* .catch Ljava/lang/Exception; {:try_start_2 ..:try_end_2} :catch_0 */
/* .line 255 */
} // .end local v2 # "reader":Ljava/io/BufferedReader;
/* .line 248 */
/* .restart local v2 # "reader":Ljava/io/BufferedReader; */
/* :catchall_0 */
/* move-exception v4 */
try { // :try_start_3
(( java.io.BufferedReader ) v2 ).close ( ); // invoke-virtual {v2}, Ljava/io/BufferedReader;->close()V
/* :try_end_3 */
/* .catchall {:try_start_3 ..:try_end_3} :catchall_1 */
/* :catchall_1 */
/* move-exception v5 */
try { // :try_start_4
(( java.lang.Throwable ) v4 ).addSuppressed ( v5 ); // invoke-virtual {v4, v5}, Ljava/lang/Throwable;->addSuppressed(Ljava/lang/Throwable;)V
} // .end local v0 # "builder":Ljava/lang/StringBuilder;
} // .end local v1 # "readerFile":Ljava/io/File;
} // .end local p0 # "path":Ljava/lang/String;
} // .end local p1 # "file":Ljava/io/File;
} // :goto_1
/* throw v4 */
/* :try_end_4 */
/* .catch Ljava/lang/Exception; {:try_start_4 ..:try_end_4} :catch_0 */
/* .line 253 */
} // .end local v2 # "reader":Ljava/io/BufferedReader;
/* .restart local v0 # "builder":Ljava/lang/StringBuilder; */
/* .restart local v1 # "readerFile":Ljava/io/File; */
/* .restart local p0 # "path":Ljava/lang/String; */
/* .restart local p1 # "file":Ljava/io/File; */
/* :catch_0 */
/* move-exception v2 */
/* .line 254 */
/* .local v2, "e":Ljava/lang/Exception; */
final String v4 = "ScoutMemoryUtils"; // const-string v4, "ScoutMemoryUtils"
final String v5 = "Failed to read thread stat"; // const-string v5, "Failed to read thread stat"
android.util.Slog .w ( v4,v5,v2 );
/* .line 256 */
} // .end local v2 # "e":Ljava/lang/Exception;
} // :goto_2
/* new-instance v2, Ljava/lang/StringBuilder; */
/* invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V */
(( java.lang.StringBuilder ) v2 ).append ( v3 ); // invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v2 ).append ( p0 ); // invoke-virtual {v2, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
final String v3 = " END---\n\n"; // const-string v3, " END---\n\n"
(( java.lang.StringBuilder ) v2 ).append ( v3 ); // invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v2 ).toString ( ); // invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
(( java.lang.StringBuilder ) v0 ).append ( v2 ); // invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
/* .line 257 */
(( java.lang.StringBuilder ) v0 ).toString ( ); // invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
com.miui.server.stability.ScoutMemoryUtils .dumpInfoToFile ( v2,p1 );
/* .line 259 */
return;
} // .end method
public static Boolean dumpInfoToFile ( java.lang.String p0, java.io.File p1 ) {
/* .locals 8 */
/* .param p0, "info" # Ljava/lang/String; */
/* .param p1, "file" # Ljava/io/File; */
/* .line 221 */
int v0 = 0; // const/4 v0, 0x0
/* if-nez p1, :cond_0 */
/* .line 222 */
} // :cond_0
int v1 = 0; // const/4 v1, 0x0
/* .line 224 */
/* .local v1, "fd":Ljava/io/FileDescriptor; */
try { // :try_start_0
/* new-instance v2, Ljava/io/FileOutputStream; */
int v3 = 1; // const/4 v3, 0x1
/* invoke-direct {v2, p1, v3}, Ljava/io/FileOutputStream;-><init>(Ljava/io/File;Z)V */
/* .line 225 */
/* .local v2, "out":Ljava/io/FileOutputStream; */
/* new-instance v4, Lcom/android/internal/util/FastPrintWriter; */
/* invoke-direct {v4, v2}, Lcom/android/internal/util/FastPrintWriter;-><init>(Ljava/io/OutputStream;)V */
/* .line 226 */
/* .local v4, "pw":Ljava/io/PrintWriter; */
(( java.io.PrintWriter ) v4 ).println ( p0 ); // invoke-virtual {v4, p0}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V
/* .line 227 */
(( java.io.PrintWriter ) v4 ).close ( ); // invoke-virtual {v4}, Ljava/io/PrintWriter;->close()V
/* .line 228 */
/* nop */
/* .line 229 */
(( java.io.File ) p1 ).toString ( ); // invoke-virtual {p1}, Ljava/io/File;->toString()Ljava/lang/String;
/* .line 228 */
int v6 = -1; // const/4 v6, -0x1
/* const/16 v7, 0x1fc */
android.os.FileUtils .setPermissions ( v5,v7,v6,v6 );
/* .line 232 */
(( java.io.File ) p1 ).getAbsolutePath ( ); // invoke-virtual {p1}, Ljava/io/File;->getAbsolutePath()Ljava/lang/String;
android.system.Os .open ( v5,v6,v0 );
/* move-object v1, v5 */
/* .line 233 */
android.system.Os .fsync ( v1 );
/* .line 234 */
android.system.Os .close ( v1 );
/* :try_end_0 */
/* .catch Ljava/lang/Exception; {:try_start_0 ..:try_end_0} :catch_0 */
/* .catchall {:try_start_0 ..:try_end_0} :catchall_0 */
/* .line 239 */
} // .end local v2 # "out":Ljava/io/FileOutputStream;
} // .end local v4 # "pw":Ljava/io/PrintWriter;
libcore.io.IoUtils .closeQuietly ( v1 );
/* .line 240 */
/* nop */
/* .line 241 */
/* .line 239 */
/* :catchall_0 */
/* move-exception v0 */
/* .line 235 */
/* :catch_0 */
/* move-exception v2 */
/* .line 236 */
/* .local v2, "e":Ljava/lang/Exception; */
try { // :try_start_1
(( java.lang.Exception ) v2 ).printStackTrace ( ); // invoke-virtual {v2}, Ljava/lang/Exception;->printStackTrace()V
/* :try_end_1 */
/* .catchall {:try_start_1 ..:try_end_1} :catchall_0 */
/* .line 237 */
/* nop */
/* .line 239 */
libcore.io.IoUtils .closeQuietly ( v1 );
/* .line 237 */
/* .line 239 */
} // .end local v2 # "e":Ljava/lang/Exception;
} // :goto_0
libcore.io.IoUtils .closeQuietly ( v1 );
/* .line 240 */
/* throw v0 */
} // .end method
public static void dumpMemoryInfo ( java.io.File p0 ) {
/* .locals 1 */
/* .param p0, "file" # Ljava/io/File; */
/* .line 270 */
final String v0 = "/proc/meminfo"; // const-string v0, "/proc/meminfo"
com.miui.server.stability.ScoutMemoryUtils .dumpInfo ( v0,p0 );
/* .line 271 */
return;
} // .end method
public static void dumpOpenFds ( Integer p0, java.io.File p1 ) {
/* .locals 10 */
/* .param p0, "pid" # I */
/* .param p1, "file" # Ljava/io/File; */
/* .line 338 */
/* new-instance v0, Ljava/io/File; */
/* new-instance v1, Ljava/lang/StringBuilder; */
/* invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V */
final String v2 = "/proc/"; // const-string v2, "/proc/"
(( java.lang.StringBuilder ) v1 ).append ( v2 ); // invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
java.lang.String .valueOf ( p0 );
(( java.lang.StringBuilder ) v1 ).append ( v2 ); // invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
final String v2 = "/fdinfo"; // const-string v2, "/fdinfo"
(( java.lang.StringBuilder ) v1 ).append ( v2 ); // invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v1 ).toString ( ); // invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
/* invoke-direct {v0, v1}, Ljava/io/File;-><init>(Ljava/lang/String;)V */
/* .line 339 */
/* .local v0, "fdPath":Ljava/io/File; */
(( java.io.File ) v0 ).listFiles ( ); // invoke-virtual {v0}, Ljava/io/File;->listFiles()[Ljava/io/File;
/* .line 340 */
/* .local v1, "fds":[Ljava/io/File; */
v3 = com.android.internal.util.ArrayUtils .isEmpty ( v1 );
if ( v3 != null) { // if-eqz v3, :cond_0
/* .line 341 */
/* new-instance v3, Ljava/lang/StringBuilder; */
/* invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V */
final String v4 = "failed to read fds! path=/proc/"; // const-string v4, "failed to read fds! path=/proc/"
(( java.lang.StringBuilder ) v3 ).append ( v4 ); // invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
java.lang.String .valueOf ( p0 );
(( java.lang.StringBuilder ) v3 ).append ( v4 ); // invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v3 ).append ( v2 ); // invoke-virtual {v3, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v2 ).toString ( ); // invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
final String v3 = "ScoutMemoryUtils"; // const-string v3, "ScoutMemoryUtils"
android.util.Slog .w ( v3,v2 );
/* .line 342 */
return;
/* .line 345 */
} // :cond_0
/* new-instance v2, Ljava/lang/StringBuilder; */
/* invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V */
/* .line 346 */
/* .local v2, "builder":Ljava/lang/StringBuilder; */
/* new-instance v3, Ljava/lang/StringBuilder; */
/* invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V */
final String v4 = "---Dump fd info pid: "; // const-string v4, "---Dump fd info pid: "
(( java.lang.StringBuilder ) v3 ).append ( v4 ); // invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v3 ).append ( p0 ); // invoke-virtual {v3, p0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;
final String v4 = ", size: "; // const-string v4, ", size: "
(( java.lang.StringBuilder ) v3 ).append ( v4 ); // invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
/* array-length v4, v1 */
(( java.lang.StringBuilder ) v3 ).append ( v4 ); // invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;
final String v4 = " START---\n"; // const-string v4, " START---\n"
(( java.lang.StringBuilder ) v3 ).append ( v4 ); // invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v3 ).toString ( ); // invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
(( java.lang.StringBuilder ) v2 ).append ( v3 ); // invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
/* .line 347 */
/* array-length v3, v1 */
int v4 = 0; // const/4 v4, 0x0
} // :goto_0
/* if-ge v4, v3, :cond_2 */
/* aget-object v5, v1, v4 */
/* .line 348 */
/* .local v5, "fd":Ljava/io/File; */
(( java.io.File ) v5 ).getAbsolutePath ( ); // invoke-virtual {v5}, Ljava/io/File;->getAbsolutePath()Ljava/lang/String;
/* .line 350 */
/* .local v6, "fd_path":Ljava/lang/String; */
com.miui.server.stability.ScoutMemoryUtils .getFdIno ( p0,v5 );
/* .line 351 */
/* .local v7, "fdinfo":Ljava/lang/String; */
final String v8 = "exp_name"; // const-string v8, "exp_name"
v8 = (( java.lang.String ) v7 ).contains ( v8 ); // invoke-virtual {v7, v8}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z
if ( v8 != null) { // if-eqz v8, :cond_1
/* .line 352 */
/* new-instance v8, Ljava/lang/StringBuilder; */
/* invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V */
(( java.lang.StringBuilder ) v8 ).append ( v6 ); // invoke-virtual {v8, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
final String v9 = " ----> "; // const-string v9, " ----> "
(( java.lang.StringBuilder ) v8 ).append ( v9 ); // invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
com.miui.server.stability.ScoutMemoryUtils .getFdOwner ( v5 );
(( java.lang.StringBuilder ) v8 ).append ( v9 ); // invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v8 ).append ( v7 ); // invoke-virtual {v8, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
final String v9 = "\n"; // const-string v9, "\n"
(( java.lang.StringBuilder ) v8 ).append ( v9 ); // invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v8 ).toString ( ); // invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
(( java.lang.StringBuilder ) v2 ).append ( v8 ); // invoke-virtual {v2, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
/* .line 347 */
} // .end local v5 # "fd":Ljava/io/File;
} // .end local v6 # "fd_path":Ljava/lang/String;
} // .end local v7 # "fdinfo":Ljava/lang/String;
} // :cond_1
/* add-int/lit8 v4, v4, 0x1 */
/* .line 355 */
} // :cond_2
final String v3 = "---Dump fd info END---\n\n"; // const-string v3, "---Dump fd info END---\n\n"
(( java.lang.StringBuilder ) v2 ).append ( v3 ); // invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
/* .line 356 */
(( java.lang.StringBuilder ) v2 ).toString ( ); // invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
com.miui.server.stability.ScoutMemoryUtils .dumpInfoToFile ( v3,p1 );
/* .line 357 */
return;
} // .end method
public static void dumpProcInfo ( java.lang.String p0, java.io.File p1, java.lang.String p2 ) {
/* .locals 4 */
/* .param p0, "info" # Ljava/lang/String; */
/* .param p1, "file" # Ljava/io/File; */
/* .param p2, "reason" # Ljava/lang/String; */
/* .line 262 */
/* new-instance v0, Ljava/lang/StringBuilder; */
/* invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V */
/* .line 263 */
/* .local v0, "builder":Ljava/lang/StringBuilder; */
/* new-instance v1, Ljava/lang/StringBuilder; */
/* invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V */
final String v2 = "---Dump "; // const-string v2, "---Dump "
(( java.lang.StringBuilder ) v1 ).append ( v2 ); // invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v1 ).append ( p2 ); // invoke-virtual {v1, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
final String v3 = " usage info START---\n"; // const-string v3, " usage info START---\n"
(( java.lang.StringBuilder ) v1 ).append ( v3 ); // invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v1 ).toString ( ); // invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
(( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
/* .line 264 */
(( java.lang.StringBuilder ) v0 ).append ( p0 ); // invoke-virtual {v0, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
/* .line 265 */
/* new-instance v1, Ljava/lang/StringBuilder; */
/* invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V */
(( java.lang.StringBuilder ) v1 ).append ( v2 ); // invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v1 ).append ( p2 ); // invoke-virtual {v1, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
final String v2 = " usage info END---\n\n"; // const-string v2, " usage info END---\n\n"
(( java.lang.StringBuilder ) v1 ).append ( v2 ); // invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v1 ).toString ( ); // invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
(( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
/* .line 266 */
(( java.lang.StringBuilder ) v0 ).toString ( ); // invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
com.miui.server.stability.ScoutMemoryUtils .dumpInfoToFile ( v1,p1 );
/* .line 267 */
return;
} // .end method
public static java.io.File getExceptionFile ( java.lang.String p0, Integer p1 ) {
/* .locals 6 */
/* .param p0, "reason" # Ljava/lang/String; */
/* .param p1, "dirId" # I */
/* .line 127 */
com.miui.server.stability.ScoutMemoryUtils .getMemLeakDir ( p1 );
/* .line 128 */
/* .local v0, "memleakDir":Ljava/io/File; */
/* if-nez v0, :cond_0 */
int v1 = 0; // const/4 v1, 0x0
/* .line 129 */
} // :cond_0
/* new-instance v1, Ljava/text/SimpleDateFormat; */
/* const-string/jumbo v2, "yyyy-MM-dd-HH-mm-ss-SSS" */
v3 = java.util.Locale.US;
/* invoke-direct {v1, v2, v3}, Ljava/text/SimpleDateFormat;-><init>(Ljava/lang/String;Ljava/util/Locale;)V */
/* .line 130 */
/* .local v1, "memleakFileDateFormat":Ljava/text/SimpleDateFormat; */
/* new-instance v2, Ljava/util/Date; */
/* invoke-direct {v2}, Ljava/util/Date;-><init>()V */
(( java.text.SimpleDateFormat ) v1 ).format ( v2 ); // invoke-virtual {v1, v2}, Ljava/text/SimpleDateFormat;->format(Ljava/util/Date;)Ljava/lang/String;
/* .line 131 */
/* .local v2, "formattedDate":Ljava/lang/String; */
/* new-instance v3, Ljava/io/File; */
/* new-instance v4, Ljava/lang/StringBuilder; */
/* invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V */
(( java.lang.StringBuilder ) v4 ).append ( p0 ); // invoke-virtual {v4, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
final String v5 = "_memory_info"; // const-string v5, "_memory_info"
(( java.lang.StringBuilder ) v4 ).append ( v5 ); // invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
final String v5 = "_"; // const-string v5, "_"
(( java.lang.StringBuilder ) v4 ).append ( v5 ); // invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v4 ).append ( v2 ); // invoke-virtual {v4, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
final String v5 = ".txt"; // const-string v5, ".txt"
(( java.lang.StringBuilder ) v4 ).append ( v5 ); // invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v4 ).toString ( ); // invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
/* invoke-direct {v3, v0, v4}, Ljava/io/File;-><init>(Ljava/io/File;Ljava/lang/String;)V */
/* .line 132 */
/* .local v3, "fname":Ljava/io/File; */
} // .end method
public static java.lang.String getFdIno ( Integer p0, java.io.File p1 ) {
/* .locals 6 */
/* .param p0, "pid" # I */
/* .param p1, "fdPath" # Ljava/io/File; */
/* .line 321 */
/* new-instance v0, Ljava/lang/StringBuilder; */
/* invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V */
final String v1 = "/proc/"; // const-string v1, "/proc/"
(( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
java.lang.String .valueOf ( p0 );
(( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
final String v1 = "/fdinfo/"; // const-string v1, "/fdinfo/"
(( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.io.File ) p1 ).getName ( ); // invoke-virtual {p1}, Ljava/io/File;->getName()Ljava/lang/String;
(( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v0 ).toString ( ); // invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
/* .line 322 */
/* .local v0, "name":Ljava/lang/String; */
try { // :try_start_0
/* new-instance v1, Ljava/io/BufferedReader; */
/* new-instance v2, Ljava/io/FileReader; */
/* invoke-direct {v2, v0}, Ljava/io/FileReader;-><init>(Ljava/lang/String;)V */
/* invoke-direct {v1, v2}, Ljava/io/BufferedReader;-><init>(Ljava/io/Reader;)V */
/* :try_end_0 */
/* .catch Ljava/lang/Exception; {:try_start_0 ..:try_end_0} :catch_0 */
/* .line 323 */
/* .local v1, "fdinfoReader":Ljava/io/BufferedReader; */
int v2 = 0; // const/4 v2, 0x0
/* .line 324 */
/* .local v2, "fdinfo":Ljava/lang/String; */
try { // :try_start_1
/* new-instance v3, Ljava/lang/StringBuilder; */
/* invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V */
/* .line 325 */
/* .local v3, "builder":Ljava/lang/StringBuilder; */
final String v4 = " ( fdinfo -->"; // const-string v4, " ( fdinfo -->"
(( java.lang.StringBuilder ) v3 ).append ( v4 ); // invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
/* .line 326 */
} // :goto_0
(( java.io.BufferedReader ) v1 ).readLine ( ); // invoke-virtual {v1}, Ljava/io/BufferedReader;->readLine()Ljava/lang/String;
/* move-object v2, v4 */
if ( v4 != null) { // if-eqz v4, :cond_0
/* .line 327 */
/* new-instance v4, Ljava/lang/StringBuilder; */
/* invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V */
final String v5 = " "; // const-string v5, " "
(( java.lang.StringBuilder ) v4 ).append ( v5 ); // invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v4 ).append ( v2 ); // invoke-virtual {v4, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v4 ).toString ( ); // invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
(( java.lang.StringBuilder ) v3 ).append ( v4 ); // invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
/* .line 329 */
} // :cond_0
final String v4 = " <--)"; // const-string v4, " <--)"
(( java.lang.StringBuilder ) v3 ).append ( v4 ); // invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
/* .line 330 */
(( java.lang.StringBuilder ) v3 ).toString ( ); // invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
/* :try_end_1 */
/* .catchall {:try_start_1 ..:try_end_1} :catchall_0 */
/* .line 331 */
try { // :try_start_2
(( java.io.BufferedReader ) v1 ).close ( ); // invoke-virtual {v1}, Ljava/io/BufferedReader;->close()V
/* :try_end_2 */
/* .catch Ljava/lang/Exception; {:try_start_2 ..:try_end_2} :catch_0 */
/* .line 330 */
/* .line 322 */
} // .end local v2 # "fdinfo":Ljava/lang/String;
} // .end local v3 # "builder":Ljava/lang/StringBuilder;
/* :catchall_0 */
/* move-exception v2 */
try { // :try_start_3
(( java.io.BufferedReader ) v1 ).close ( ); // invoke-virtual {v1}, Ljava/io/BufferedReader;->close()V
/* :try_end_3 */
/* .catchall {:try_start_3 ..:try_end_3} :catchall_1 */
/* :catchall_1 */
/* move-exception v3 */
try { // :try_start_4
(( java.lang.Throwable ) v2 ).addSuppressed ( v3 ); // invoke-virtual {v2, v3}, Ljava/lang/Throwable;->addSuppressed(Ljava/lang/Throwable;)V
} // .end local v0 # "name":Ljava/lang/String;
} // .end local p0 # "pid":I
} // .end local p1 # "fdPath":Ljava/io/File;
} // :goto_1
/* throw v2 */
/* :try_end_4 */
/* .catch Ljava/lang/Exception; {:try_start_4 ..:try_end_4} :catch_0 */
/* .line 331 */
} // .end local v1 # "fdinfoReader":Ljava/io/BufferedReader;
/* .restart local v0 # "name":Ljava/lang/String; */
/* .restart local p0 # "pid":I */
/* .restart local p1 # "fdPath":Ljava/io/File; */
/* :catch_0 */
/* move-exception v1 */
/* .line 332 */
/* .local v1, "e":Ljava/lang/Exception; */
/* new-instance v2, Ljava/lang/StringBuilder; */
/* invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V */
final String v3 = "getFdIno: "; // const-string v3, "getFdIno: "
(( java.lang.StringBuilder ) v2 ).append ( v3 ); // invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v2 ).append ( p1 ); // invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v2 ).toString ( ); // invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
final String v3 = "ScoutMemoryUtils"; // const-string v3, "ScoutMemoryUtils"
android.util.Slog .w ( v3,v2,v1 );
/* .line 334 */
} // .end local v1 # "e":Ljava/lang/Exception;
/* new-instance v1, Ljava/lang/StringBuilder; */
/* invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V */
final String v2 = "(unknow ino: "; // const-string v2, "(unknow ino: "
(( java.lang.StringBuilder ) v1 ).append ( v2 ); // invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v1 ).append ( v0 ); // invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
final String v2 = ")"; // const-string v2, ")"
(( java.lang.StringBuilder ) v1 ).append ( v2 ); // invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v1 ).toString ( ); // invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
} // .end method
public static java.lang.String getFdOwner ( java.io.File p0 ) {
/* .locals 7 */
/* .param p0, "fdPath" # Ljava/io/File; */
/* .line 305 */
final String v0 = ""; // const-string v0, ""
final String v1 = "getFdOwner: "; // const-string v1, "getFdOwner: "
final String v2 = "ScoutMemoryUtils"; // const-string v2, "ScoutMemoryUtils"
try { // :try_start_0
(( java.io.File ) p0 ).getName ( ); // invoke-virtual {p0}, Ljava/io/File;->getName()Ljava/lang/String;
java.lang.Integer .valueOf ( v3 );
v3 = (( java.lang.Integer ) v3 ).intValue ( ); // invoke-virtual {v3}, Ljava/lang/Integer;->intValue()I
/* :try_end_0 */
/* .catch Ljava/lang/Exception; {:try_start_0 ..:try_end_0} :catch_1 */
/* .line 309 */
/* .local v3, "rawFd":I */
/* nop */
/* .line 311 */
try { // :try_start_1
/* new-instance v4, Ljava/io/FileDescriptor; */
/* invoke-direct {v4}, Ljava/io/FileDescriptor;-><init>()V */
/* .line 312 */
/* .local v4, "fd":Ljava/io/FileDescriptor; */
(( java.io.FileDescriptor ) v4 ).setInt$ ( v3 ); // invoke-virtual {v4, v3}, Ljava/io/FileDescriptor;->setInt$(I)V
/* .line 313 */
/* new-instance v5, Ljava/lang/StringBuilder; */
/* invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V */
final String v6 = " ("; // const-string v6, " ("
(( java.lang.StringBuilder ) v5 ).append ( v6 ); // invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
com.miui.server.stability.ScoutMemoryUtils .getFdOwnerImpl ( v4 );
(( java.lang.StringBuilder ) v5 ).append ( v6 ); // invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
final String v6 = ") "; // const-string v6, ") "
(( java.lang.StringBuilder ) v5 ).append ( v6 ); // invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v5 ).toString ( ); // invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
/* :try_end_1 */
/* .catch Ljava/lang/Exception; {:try_start_1 ..:try_end_1} :catch_0 */
/* .line 314 */
} // .end local v4 # "fd":Ljava/io/FileDescriptor;
/* :catch_0 */
/* move-exception v4 */
/* .line 315 */
/* .local v4, "e":Ljava/lang/Exception; */
/* new-instance v5, Ljava/lang/StringBuilder; */
/* invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V */
(( java.lang.StringBuilder ) v5 ).append ( v1 ); // invoke-virtual {v5, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v1 ).append ( p0 ); // invoke-virtual {v1, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v1 ).toString ( ); // invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
android.util.Slog .w ( v2,v1,v4 );
/* .line 316 */
/* .line 306 */
} // .end local v3 # "rawFd":I
} // .end local v4 # "e":Ljava/lang/Exception;
/* :catch_1 */
/* move-exception v3 */
/* .line 307 */
/* .local v3, "e":Ljava/lang/Exception; */
/* new-instance v4, Ljava/lang/StringBuilder; */
/* invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V */
(( java.lang.StringBuilder ) v4 ).append ( v1 ); // invoke-virtual {v4, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v1 ).append ( p0 ); // invoke-virtual {v1, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;
final String v4 = " is not a valid fd path"; // const-string v4, " is not a valid fd path"
(( java.lang.StringBuilder ) v1 ).append ( v4 ); // invoke-virtual {v1, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v1 ).toString ( ); // invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
android.util.Slog .w ( v2,v1,v3 );
/* .line 308 */
} // .end method
private static java.lang.String getFdOwnerImpl ( java.io.FileDescriptor p0 ) {
/* .locals 8 */
/* .param p0, "fd" # Ljava/io/FileDescriptor; */
/* .annotation system Ldalvik/annotation/Throws; */
/* value = { */
/* Ljava/lang/Exception; */
/* } */
} // .end annotation
/* .line 285 */
libcore.io.Os .getDefault ( );
/* .line 287 */
/* .local v0, "os":Llibcore/io/Os; */
v1 = com.miui.server.stability.ScoutMemoryUtils.sGetTagMethod;
/* if-nez v1, :cond_0 */
/* .line 288 */
/* const-class v1, Llibcore/io/Os; */
/* const-class v2, Ljava/io/FileDescriptor; */
/* filled-new-array {v2}, [Ljava/lang/Class; */
final String v3 = "android_fdsan_get_owner_tag"; // const-string v3, "android_fdsan_get_owner_tag"
(( java.lang.Class ) v1 ).getDeclaredMethod ( v3, v2 ); // invoke-virtual {v1, v3, v2}, Ljava/lang/Class;->getDeclaredMethod(Ljava/lang/String;[Ljava/lang/Class;)Ljava/lang/reflect/Method;
/* .line 290 */
/* const-class v1, Llibcore/io/Os; */
v2 = java.lang.Long.TYPE;
/* filled-new-array {v2}, [Ljava/lang/Class; */
final String v3 = "android_fdsan_get_tag_type"; // const-string v3, "android_fdsan_get_tag_type"
(( java.lang.Class ) v1 ).getDeclaredMethod ( v3, v2 ); // invoke-virtual {v1, v3, v2}, Ljava/lang/Class;->getDeclaredMethod(Ljava/lang/String;[Ljava/lang/Class;)Ljava/lang/reflect/Method;
/* .line 292 */
/* const-class v1, Llibcore/io/Os; */
v2 = java.lang.Long.TYPE;
/* filled-new-array {v2}, [Ljava/lang/Class; */
final String v3 = "android_fdsan_get_tag_value"; // const-string v3, "android_fdsan_get_tag_value"
(( java.lang.Class ) v1 ).getDeclaredMethod ( v3, v2 ); // invoke-virtual {v1, v3, v2}, Ljava/lang/Class;->getDeclaredMethod(Ljava/lang/String;[Ljava/lang/Class;)Ljava/lang/reflect/Method;
/* .line 295 */
} // :cond_0
v1 = com.miui.server.stability.ScoutMemoryUtils.sGetTagMethod;
/* filled-new-array {p0}, [Ljava/lang/Object; */
(( java.lang.reflect.Method ) v1 ).invoke ( v0, v2 ); // invoke-virtual {v1, v0, v2}, Ljava/lang/reflect/Method;->invoke(Ljava/lang/Object;[Ljava/lang/Object;)Ljava/lang/Object;
/* check-cast v1, Ljava/lang/Long; */
(( java.lang.Long ) v1 ).longValue ( ); // invoke-virtual {v1}, Ljava/lang/Long;->longValue()J
/* move-result-wide v1 */
/* .line 296 */
/* .local v1, "tag":J */
/* const-wide/16 v3, 0x0 */
/* cmp-long v3, v1, v3 */
/* if-nez v3, :cond_1 */
/* const-string/jumbo v3, "unowned" */
/* .line 297 */
} // :cond_1
v3 = com.miui.server.stability.ScoutMemoryUtils.sGetTagTypeMethod;
java.lang.Long .valueOf ( v1,v2 );
/* filled-new-array {v4}, [Ljava/lang/Object; */
(( java.lang.reflect.Method ) v3 ).invoke ( v0, v4 ); // invoke-virtual {v3, v0, v4}, Ljava/lang/reflect/Method;->invoke(Ljava/lang/Object;[Ljava/lang/Object;)Ljava/lang/Object;
/* check-cast v3, Ljava/lang/String; */
/* .line 298 */
/* .local v3, "type":Ljava/lang/String; */
v4 = com.miui.server.stability.ScoutMemoryUtils.sGetTagValueMethod;
java.lang.Long .valueOf ( v1,v2 );
/* filled-new-array {v5}, [Ljava/lang/Object; */
(( java.lang.reflect.Method ) v4 ).invoke ( v0, v5 ); // invoke-virtual {v4, v0, v5}, Ljava/lang/reflect/Method;->invoke(Ljava/lang/Object;[Ljava/lang/Object;)Ljava/lang/Object;
/* check-cast v4, Ljava/lang/Long; */
(( java.lang.Long ) v4 ).longValue ( ); // invoke-virtual {v4}, Ljava/lang/Long;->longValue()J
/* move-result-wide v4 */
/* .line 299 */
/* .local v4, "value":J */
/* new-instance v6, Ljava/lang/StringBuilder; */
/* invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V */
final String v7 = "owned by "; // const-string v7, "owned by "
(( java.lang.StringBuilder ) v6 ).append ( v7 ); // invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v6 ).append ( v3 ); // invoke-virtual {v6, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
final String v7 = " 0x"; // const-string v7, " 0x"
(( java.lang.StringBuilder ) v6 ).append ( v7 ); // invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
java.lang.Long .toHexString ( v4,v5 );
(( java.lang.StringBuilder ) v6 ).append ( v7 ); // invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v6 ).toString ( ); // invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
} // .end method
private static java.io.File getMemLeakDir ( Integer p0 ) {
/* .locals 9 */
/* .param p0, "dirId" # I */
/* .line 90 */
v0 = com.miui.server.stability.ScoutMemoryUtils.dirMap;
java.lang.Integer .valueOf ( p0 );
v1 = (( java.util.HashMap ) v0 ).containsKey ( v1 ); // invoke-virtual {v0, v1}, Ljava/util/HashMap;->containsKey(Ljava/lang/Object;)Z
int v2 = 0; // const/4 v2, 0x0
final String v3 = "ScoutMemoryUtils"; // const-string v3, "ScoutMemoryUtils"
/* if-nez v1, :cond_0 */
/* .line 91 */
/* new-instance v0, Ljava/lang/StringBuilder; */
/* invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V */
final String v1 = "invalid dir id : "; // const-string v1, "invalid dir id : "
(( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v0 ).append ( p0 ); // invoke-virtual {v0, p0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v0 ).toString ( ); // invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
android.util.Slog .e ( v3,v0 );
/* .line 92 */
/* .line 94 */
} // :cond_0
/* new-instance v1, Ljava/io/File; */
final String v4 = "/data/miuilog/stability"; // const-string v4, "/data/miuilog/stability"
/* invoke-direct {v1, v4}, Ljava/io/File;-><init>(Ljava/lang/String;)V */
/* .line 95 */
/* .local v1, "stDir":Ljava/io/File; */
v4 = (( java.io.File ) v1 ).exists ( ); // invoke-virtual {v1}, Ljava/io/File;->exists()Z
/* if-nez v4, :cond_1 */
/* .line 96 */
/* const-string/jumbo v0, "stability log dir isn\'t exists" */
android.util.Slog .e ( v3,v0 );
/* .line 97 */
/* .line 99 */
} // :cond_1
/* new-instance v4, Ljava/io/File; */
final String v5 = "memleak"; // const-string v5, "memleak"
/* invoke-direct {v4, v1, v5}, Ljava/io/File;-><init>(Ljava/io/File;Ljava/lang/String;)V */
/* .line 100 */
/* .local v4, "memleakDir":Ljava/io/File; */
v5 = (( java.io.File ) v4 ).exists ( ); // invoke-virtual {v4}, Ljava/io/File;->exists()Z
/* const/16 v6, 0x1ff */
int v7 = -1; // const/4 v7, -0x1
/* if-nez v5, :cond_3 */
/* .line 101 */
v5 = (( java.io.File ) v4 ).mkdirs ( ); // invoke-virtual {v4}, Ljava/io/File;->mkdirs()Z
/* if-nez v5, :cond_2 */
/* .line 102 */
final String v0 = "Cannot create memleak dir"; // const-string v0, "Cannot create memleak dir"
android.util.Slog .e ( v3,v0 );
/* .line 103 */
/* .line 105 */
} // :cond_2
/* nop */
/* .line 106 */
(( java.io.File ) v4 ).toString ( ); // invoke-virtual {v4}, Ljava/io/File;->toString()Ljava/lang/String;
/* .line 105 */
android.os.FileUtils .setPermissions ( v5,v6,v7,v7 );
/* .line 109 */
final String v5 = "mkdir memleak dir"; // const-string v5, "mkdir memleak dir"
android.util.Slog .e ( v3,v5 );
/* .line 111 */
} // :cond_3
/* new-instance v5, Ljava/io/File; */
java.lang.Integer .valueOf ( p0 );
(( java.util.HashMap ) v0 ).get ( v8 ); // invoke-virtual {v0, v8}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;
/* check-cast v8, Ljava/lang/String; */
/* invoke-direct {v5, v4, v8}, Ljava/io/File;-><init>(Ljava/io/File;Ljava/lang/String;)V */
/* .line 112 */
/* .local v5, "exDir":Ljava/io/File; */
v8 = (( java.io.File ) v5 ).exists ( ); // invoke-virtual {v5}, Ljava/io/File;->exists()Z
/* if-nez v8, :cond_5 */
/* .line 113 */
v8 = (( java.io.File ) v5 ).mkdirs ( ); // invoke-virtual {v5}, Ljava/io/File;->mkdirs()Z
/* if-nez v8, :cond_4 */
/* .line 114 */
/* new-instance v6, Ljava/lang/StringBuilder; */
/* invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V */
final String v7 = "Cannot create memleak dir "; // const-string v7, "Cannot create memleak dir "
(( java.lang.StringBuilder ) v6 ).append ( v7 ); // invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
java.lang.Integer .valueOf ( p0 );
(( java.util.HashMap ) v0 ).get ( v7 ); // invoke-virtual {v0, v7}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;
/* check-cast v0, Ljava/lang/String; */
(( java.lang.StringBuilder ) v6 ).append ( v0 ); // invoke-virtual {v6, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v0 ).toString ( ); // invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
android.util.Slog .e ( v3,v0 );
/* .line 115 */
/* .line 117 */
} // :cond_4
/* nop */
/* .line 118 */
(( java.io.File ) v5 ).toString ( ); // invoke-virtual {v5}, Ljava/io/File;->toString()Ljava/lang/String;
/* .line 117 */
android.os.FileUtils .setPermissions ( v0,v6,v7,v7 );
/* .line 123 */
} // :cond_5
} // .end method
public static void triggerCrash ( ) {
/* .locals 1 */
/* .line 360 */
/* const/16 v0, 0x63 */
com.android.server.ScoutHelper .doSysRqInterface ( v0 );
/* .line 361 */
return;
} // .end method
