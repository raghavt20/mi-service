.class Lcom/miui/server/stability/LockPerfImpl$SlowLockedMethodEvent;
.super Ljava/lang/Object;
.source "LockPerfImpl.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/miui/server/stability/LockPerfImpl;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0xa
    name = "SlowLockedMethodEvent"
.end annotation


# static fields
.field private static final DATE_TIME_FORMATTER:Ljava/time/format/DateTimeFormatter;


# instance fields
.field private final datetime:Ljava/time/LocalDateTime;

.field private final perfStackSnapshot:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List<",
            "Lcom/miui/server/stability/LockPerfImpl$PerfInfo;",
            ">;"
        }
    .end annotation
.end field

.field private final stackTraceLines:[Ljava/lang/String;

.field private final tid:J

.field private final tname:Ljava/lang/String;

.field private final unwindingStack:[Ljava/lang/StackTraceElement;


# direct methods
.method static bridge synthetic -$$Nest$fgetdatetime(Lcom/miui/server/stability/LockPerfImpl$SlowLockedMethodEvent;)Ljava/time/LocalDateTime;
    .locals 0

    iget-object p0, p0, Lcom/miui/server/stability/LockPerfImpl$SlowLockedMethodEvent;->datetime:Ljava/time/LocalDateTime;

    return-object p0
.end method

.method static bridge synthetic -$$Nest$fgettid(Lcom/miui/server/stability/LockPerfImpl$SlowLockedMethodEvent;)J
    .locals 2

    iget-wide v0, p0, Lcom/miui/server/stability/LockPerfImpl$SlowLockedMethodEvent;->tid:J

    return-wide v0
.end method

.method static bridge synthetic -$$Nest$fgettname(Lcom/miui/server/stability/LockPerfImpl$SlowLockedMethodEvent;)Ljava/lang/String;
    .locals 0

    iget-object p0, p0, Lcom/miui/server/stability/LockPerfImpl$SlowLockedMethodEvent;->tname:Ljava/lang/String;

    return-object p0
.end method

.method static bridge synthetic -$$Nest$mformattedDatetime(Lcom/miui/server/stability/LockPerfImpl$SlowLockedMethodEvent;)Ljava/lang/String;
    .locals 0

    invoke-direct {p0}, Lcom/miui/server/stability/LockPerfImpl$SlowLockedMethodEvent;->formattedDatetime()Ljava/lang/String;

    move-result-object p0

    return-object p0
.end method

.method static bridge synthetic -$$Nest$mgetStackTraceLines(Lcom/miui/server/stability/LockPerfImpl$SlowLockedMethodEvent;)[Ljava/lang/String;
    .locals 0

    invoke-direct {p0}, Lcom/miui/server/stability/LockPerfImpl$SlowLockedMethodEvent;->getStackTraceLines()[Ljava/lang/String;

    move-result-object p0

    return-object p0
.end method

.method static bridge synthetic -$$Nest$mprintToLogcat(Lcom/miui/server/stability/LockPerfImpl$SlowLockedMethodEvent;)V
    .locals 0

    invoke-direct {p0}, Lcom/miui/server/stability/LockPerfImpl$SlowLockedMethodEvent;->printToLogcat()V

    return-void
.end method

.method static constructor <clinit>()V
    .locals 1

    .line 234
    nop

    .line 235
    const-string/jumbo v0, "uu-MM-dd HH:mm:ss.SSS"

    invoke-static {v0}, Ljava/time/format/DateTimeFormatter;->ofPattern(Ljava/lang/String;)Ljava/time/format/DateTimeFormatter;

    move-result-object v0

    sput-object v0, Lcom/miui/server/stability/LockPerfImpl$SlowLockedMethodEvent;->DATE_TIME_FORMATTER:Ljava/time/format/DateTimeFormatter;

    .line 234
    return-void
.end method

.method private constructor <init>(Lcom/miui/server/stability/LockPerfImpl$PerfStack;[Ljava/lang/StackTraceElement;JLjava/lang/String;Ljava/time/LocalDateTime;)V
    .locals 3
    .param p2, "unwindingStack"    # [Ljava/lang/StackTraceElement;
    .param p3, "tid"    # J
    .param p5, "tname"    # Ljava/lang/String;
    .param p6, "datetime"    # Ljava/time/LocalDateTime;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/miui/server/stability/LockPerfImpl$PerfStack<",
            "Lcom/miui/server/stability/LockPerfImpl$PerfInfo;",
            ">;[",
            "Ljava/lang/StackTraceElement;",
            "J",
            "Ljava/lang/String;",
            "Ljava/time/LocalDateTime;",
            ")V"
        }
    .end annotation

    .line 246
    .local p1, "perfStack":Lcom/miui/server/stability/LockPerfImpl$PerfStack;, "Lcom/miui/server/stability/LockPerfImpl$PerfStack<Lcom/miui/server/stability/LockPerfImpl$PerfInfo;>;"
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 240
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/miui/server/stability/LockPerfImpl$SlowLockedMethodEvent;->perfStackSnapshot:Ljava/util/List;

    .line 247
    iput-wide p3, p0, Lcom/miui/server/stability/LockPerfImpl$SlowLockedMethodEvent;->tid:J

    .line 248
    iput-object p5, p0, Lcom/miui/server/stability/LockPerfImpl$SlowLockedMethodEvent;->tname:Ljava/lang/String;

    .line 249
    iput-object p2, p0, Lcom/miui/server/stability/LockPerfImpl$SlowLockedMethodEvent;->unwindingStack:[Ljava/lang/StackTraceElement;

    .line 250
    invoke-virtual {p1}, Lcom/miui/server/stability/LockPerfImpl$PerfStack;->size()I

    move-result v0

    add-int/lit8 v0, v0, -0x1

    .local v0, "i":I
    :goto_0
    if-ltz v0, :cond_0

    .line 251
    iget-object v1, p0, Lcom/miui/server/stability/LockPerfImpl$SlowLockedMethodEvent;->perfStackSnapshot:Ljava/util/List;

    invoke-virtual {p1, v0}, Lcom/miui/server/stability/LockPerfImpl$PerfStack;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/miui/server/stability/LockPerfImpl$PerfInfo;

    invoke-interface {v1, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 250
    add-int/lit8 v0, v0, -0x1

    goto :goto_0

    .line 253
    .end local v0    # "i":I
    :cond_0
    iput-object p6, p0, Lcom/miui/server/stability/LockPerfImpl$SlowLockedMethodEvent;->datetime:Ljava/time/LocalDateTime;

    .line 254
    invoke-direct {p0}, Lcom/miui/server/stability/LockPerfImpl$SlowLockedMethodEvent;->initStackTraceLines()[Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/miui/server/stability/LockPerfImpl$SlowLockedMethodEvent;->stackTraceLines:[Ljava/lang/String;

    .line 255
    return-void
.end method

.method synthetic constructor <init>(Lcom/miui/server/stability/LockPerfImpl$PerfStack;[Ljava/lang/StackTraceElement;JLjava/lang/String;Ljava/time/LocalDateTime;Lcom/miui/server/stability/LockPerfImpl$SlowLockedMethodEvent-IA;)V
    .locals 0

    invoke-direct/range {p0 .. p6}, Lcom/miui/server/stability/LockPerfImpl$SlowLockedMethodEvent;-><init>(Lcom/miui/server/stability/LockPerfImpl$PerfStack;[Ljava/lang/StackTraceElement;JLjava/lang/String;Ljava/time/LocalDateTime;)V

    return-void
.end method

.method private formattedDatetime()Ljava/lang/String;
    .locals 2

    .line 258
    iget-object v0, p0, Lcom/miui/server/stability/LockPerfImpl$SlowLockedMethodEvent;->datetime:Ljava/time/LocalDateTime;

    sget-object v1, Lcom/miui/server/stability/LockPerfImpl$SlowLockedMethodEvent;->DATE_TIME_FORMATTER:Ljava/time/format/DateTimeFormatter;

    invoke-virtual {v0, v1}, Ljava/time/LocalDateTime;->format(Ljava/time/format/DateTimeFormatter;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method private getStackTraceLines()[Ljava/lang/String;
    .locals 1

    .line 262
    iget-object v0, p0, Lcom/miui/server/stability/LockPerfImpl$SlowLockedMethodEvent;->stackTraceLines:[Ljava/lang/String;

    return-object v0
.end method

.method private initStackTraceLines()[Ljava/lang/String;
    .locals 9

    .line 267
    iget-object v0, p0, Lcom/miui/server/stability/LockPerfImpl$SlowLockedMethodEvent;->unwindingStack:[Ljava/lang/StackTraceElement;

    .line 268
    .local v0, "stackTrace":[Ljava/lang/StackTraceElement;
    array-length v1, v0

    new-array v1, v1, [Ljava/lang/String;

    .line 269
    .local v1, "lines":[Ljava/lang/String;
    const/4 v2, 0x0

    .line 270
    .local v2, "currPerfIdx":I
    iget-object v3, p0, Lcom/miui/server/stability/LockPerfImpl$SlowLockedMethodEvent;->perfStackSnapshot:Ljava/util/List;

    invoke-interface {v3, v2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/miui/server/stability/LockPerfImpl$PerfInfo;

    .line 271
    .local v3, "perfInfo":Lcom/miui/server/stability/LockPerfImpl$PerfInfo;
    const/4 v4, 0x0

    .local v4, "i":I
    :goto_0
    array-length v5, v0

    if-ge v4, v5, :cond_1

    .line 272
    aget-object v5, v0, v4

    .line 273
    .local v5, "stackElement":Ljava/lang/StackTraceElement;
    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    .line 274
    .local v6, "sb":Ljava/lang/StringBuilder;
    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v7

    filled-new-array {v7}, [Ljava/lang/Object;

    move-result-object v7

    const-string v8, "#%02d "

    invoke-static {v8, v7}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 275
    invoke-virtual {v5}, Ljava/lang/StackTraceElement;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 276
    iget-object v7, p0, Lcom/miui/server/stability/LockPerfImpl$SlowLockedMethodEvent;->perfStackSnapshot:Ljava/util/List;

    invoke-interface {v7}, Ljava/util/List;->size()I

    move-result v7

    if-ge v2, v7, :cond_0

    .line 277
    invoke-static {v3, v5}, Lcom/miui/server/stability/LockPerfImpl$PerfInfo;->-$$Nest$mmatchStackTraceElement(Lcom/miui/server/stability/LockPerfImpl$PerfInfo;Ljava/lang/StackTraceElement;)Z

    move-result v7

    if-eqz v7, :cond_0

    .line 278
    const-string v7, " "

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-static {v3}, Lcom/miui/server/stability/LockPerfImpl$PerfInfo;->-$$Nest$mgetDurationString(Lcom/miui/server/stability/LockPerfImpl$PerfInfo;)Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 279
    add-int/lit8 v2, v2, 0x1

    iget-object v7, p0, Lcom/miui/server/stability/LockPerfImpl$SlowLockedMethodEvent;->perfStackSnapshot:Ljava/util/List;

    invoke-interface {v7}, Ljava/util/List;->size()I

    move-result v7

    if-ge v2, v7, :cond_0

    .line 280
    iget-object v7, p0, Lcom/miui/server/stability/LockPerfImpl$SlowLockedMethodEvent;->perfStackSnapshot:Ljava/util/List;

    invoke-interface {v7, v2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v7

    move-object v3, v7

    check-cast v3, Lcom/miui/server/stability/LockPerfImpl$PerfInfo;

    .line 284
    :cond_0
    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    aput-object v7, v1, v4

    .line 271
    .end local v5    # "stackElement":Ljava/lang/StackTraceElement;
    .end local v6    # "sb":Ljava/lang/StringBuilder;
    add-int/lit8 v4, v4, 0x1

    goto :goto_0

    .line 286
    .end local v4    # "i":I
    :cond_1
    return-object v1
.end method

.method private printToLogcat()V
    .locals 7

    .line 290
    invoke-static {}, Lcom/miui/server/stability/LockPerfImpl;->-$$Nest$sfgetTAG()Ljava/lang/String;

    move-result-object v0

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Long locked method event. Thread: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lcom/miui/server/stability/LockPerfImpl$SlowLockedMethodEvent;->tname:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ". Full stack trace: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Slog;->w(Ljava/lang/String;Ljava/lang/String;)I

    .line 291
    invoke-direct {p0}, Lcom/miui/server/stability/LockPerfImpl$SlowLockedMethodEvent;->getStackTraceLines()[Ljava/lang/String;

    move-result-object v0

    array-length v1, v0

    const/4 v2, 0x0

    :goto_0
    if-ge v2, v1, :cond_0

    aget-object v3, v0, v2

    .line 292
    .local v3, "line":Ljava/lang/String;
    invoke-static {}, Lcom/miui/server/stability/LockPerfImpl;->-$$Nest$sfgetTAG()Ljava/lang/String;

    move-result-object v4

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "\t "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5}, Landroid/util/Slog;->w(Ljava/lang/String;Ljava/lang/String;)I

    .line 291
    .end local v3    # "line":Ljava/lang/String;
    add-int/lit8 v2, v2, 0x1

    goto :goto_0

    .line 294
    :cond_0
    return-void
.end method
