class com.miui.server.stability.ScoutDisplayMemoryManager$3 implements java.lang.Runnable {
	 /* .source "ScoutDisplayMemoryManager.java" */
	 /* # interfaces */
	 /* # annotations */
	 /* .annotation system Ldalvik/annotation/EnclosingMethod; */
	 /* value = Lcom/miui/server/stability/ScoutDisplayMemoryManager;->reportDmabufLeakException(Ljava/lang/String;J)V */
} // .end annotation
/* .annotation system Ldalvik/annotation/InnerClass; */
/* accessFlags = 0x0 */
/* name = null */
} // .end annotation
/* # instance fields */
final com.miui.server.stability.ScoutDisplayMemoryManager this$0; //synthetic
final java.lang.String val$reason; //synthetic
final Long val$totalSizeKB; //synthetic
/* # direct methods */
 com.miui.server.stability.ScoutDisplayMemoryManager$3 ( ) {
/* .locals 0 */
/* .param p1, "this$0" # Lcom/miui/server/stability/ScoutDisplayMemoryManager; */
/* .annotation system Ldalvik/annotation/Signature; */
/* value = { */
/* "()V" */
/* } */
} // .end annotation
/* .line 439 */
this.this$0 = p1;
/* iput-wide p2, p0, Lcom/miui/server/stability/ScoutDisplayMemoryManager$3;->val$totalSizeKB:J */
this.val$reason = p4;
/* invoke-direct {p0}, Ljava/lang/Object;-><init>()V */
return;
} // .end method
/* # virtual methods */
public void run ( ) {
/* .locals 22 */
/* .line 442 */
/* move-object/from16 v0, p0 */
/* new-instance v1, Ljava/lang/StringBuilder; */
/* invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V */
final String v2 = "Warring!!! dma-buf leak:"; // const-string v2, "Warring!!! dma-buf leak:"
(( java.lang.StringBuilder ) v1 ).append ( v2 ); // invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
/* iget-wide v2, v0, Lcom/miui/server/stability/ScoutDisplayMemoryManager$3;->val$totalSizeKB:J */
/* .line 443 */
com.miui.server.stability.ScoutDisplayMemoryManager .stringifyKBSize ( v2,v3 );
(( java.lang.StringBuilder ) v1 ).append ( v2 ); // invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
/* .line 444 */
v2 = this.this$0;
v2 = com.miui.server.stability.ScoutDisplayMemoryManager .-$$Nest$fgetisCameraForeground ( v2 );
if ( v2 != null) { // if-eqz v2, :cond_0
final String v2 = " , camera in the foreground"; // const-string v2, " , camera in the foreground"
} // :cond_0
final String v2 = ""; // const-string v2, ""
} // :goto_0
(( java.lang.StringBuilder ) v1 ).append ( v2 ); // invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v1 ).toString ( ); // invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
/* .line 442 */
final String v2 = "MIUIScout Memory"; // const-string v2, "MIUIScout Memory"
android.util.Slog .e ( v2,v1 );
/* .line 445 */
final String v1 = ""; // const-string v1, ""
/* .line 446 */
/* .local v1, "dambufLog":Ljava/lang/String; */
v3 = this.this$0;
com.miui.server.stability.ScoutDisplayMemoryManager .-$$Nest$mreadDmabufInfo ( v3 );
/* .line 447 */
/* .local v3, "dmabufInfo":Lcom/miui/server/stability/DmaBufUsageInfo; */
int v4 = 0; // const/4 v4, 0x0
/* .line 448 */
/* .local v4, "infoList":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/miui/server/stability/DmaBufProcUsageInfo;>;" */
int v5 = 0; // const/4 v5, 0x0
/* const-wide/16 v6, 0x400 */
if ( v3 != null) { // if-eqz v3, :cond_1
/* .line 449 */
(( com.miui.server.stability.DmaBufUsageInfo ) v3 ).getList ( ); // invoke-virtual {v3}, Lcom/miui/server/stability/DmaBufUsageInfo;->getList()Ljava/util/ArrayList;
/* .line 450 */
/* new-instance v8, Lcom/miui/server/stability/ScoutDisplayMemoryManager$3$1; */
/* invoke-direct {v8, v0}, Lcom/miui/server/stability/ScoutDisplayMemoryManager$3$1;-><init>(Lcom/miui/server/stability/ScoutDisplayMemoryManager$3;)V */
java.util.Collections .sort ( v4,v8 );
/* .line 462 */
(( com.miui.server.stability.DmaBufUsageInfo ) v3 ).toString ( ); // invoke-virtual {v3}, Lcom/miui/server/stability/DmaBufUsageInfo;->toString()Ljava/lang/String;
/* .line 463 */
android.util.Slog .e ( v2,v1 );
/* .line 464 */
(( com.miui.server.stability.DmaBufUsageInfo ) v3 ).getTotalSize ( ); // invoke-virtual {v3}, Lcom/miui/server/stability/DmaBufUsageInfo;->getTotalSize()J
/* move-result-wide v8 */
v10 = com.miui.server.stability.ScoutDisplayMemoryManager .-$$Nest$sfgetDMABUF_LEAK_THRESHOLD ( );
/* int-to-long v10, v10 */
/* mul-long/2addr v10, v6 */
/* cmp-long v8, v8, v10 */
/* if-gez v8, :cond_1 */
/* .line 465 */
/* new-instance v6, Ljava/lang/StringBuilder; */
/* invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V */
final String v7 = "TotalSize "; // const-string v7, "TotalSize "
(( java.lang.StringBuilder ) v6 ).append ( v7 ); // invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
/* iget-wide v7, v0, Lcom/miui/server/stability/ScoutDisplayMemoryManager$3;->val$totalSizeKB:J */
com.miui.server.stability.ScoutDisplayMemoryManager .stringifyKBSize ( v7,v8 );
(( java.lang.StringBuilder ) v6 ).append ( v7 ); // invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
final String v7 = " dma-buf, DmabufInfo totalSize "; // const-string v7, " dma-buf, DmabufInfo totalSize "
(( java.lang.StringBuilder ) v6 ).append ( v7 ); // invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
/* .line 466 */
(( com.miui.server.stability.DmaBufUsageInfo ) v3 ).getTotalSize ( ); // invoke-virtual {v3}, Lcom/miui/server/stability/DmaBufUsageInfo;->getTotalSize()J
/* move-result-wide v7 */
(( java.lang.StringBuilder ) v6 ).append ( v7, v8 ); // invoke-virtual {v6, v7, v8}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;
final String v7 = "kB less than threshold, Skip"; // const-string v7, "kB less than threshold, Skip"
(( java.lang.StringBuilder ) v6 ).append ( v7 ); // invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v6 ).toString ( ); // invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
/* .line 467 */
/* .local v6, "failMsg":Ljava/lang/String; */
android.util.Slog .e ( v2,v6 );
/* .line 468 */
v2 = this.this$0;
com.miui.server.stability.ScoutDisplayMemoryManager .-$$Nest$fgetisBusy ( v2 );
(( java.util.concurrent.atomic.AtomicBoolean ) v2 ).set ( v5 ); // invoke-virtual {v2, v5}, Ljava/util/concurrent/atomic/AtomicBoolean;->set(Z)V
/* .line 469 */
return;
/* .line 472 */
} // .end local v6 # "failMsg":Ljava/lang/String;
} // :cond_1
int v8 = 0; // const/4 v8, 0x0
/* .line 473 */
/* .local v8, "topProc":Lcom/miui/server/stability/DmaBufProcUsageInfo; */
int v9 = 0; // const/4 v9, 0x0
/* .line 474 */
/* .local v9, "sfProc":Lcom/miui/server/stability/DmaBufProcUsageInfo; */
if ( v4 != null) { // if-eqz v4, :cond_a
/* .line 475 */
/* new-instance v10, Ljava/util/HashSet; */
int v11 = 5; // const/4 v11, 0x5
/* invoke-direct {v10, v11}, Ljava/util/HashSet;-><init>(I)V */
/* .line 476 */
/* .local v10, "fdPids":Ljava/util/HashSet;, "Ljava/util/HashSet<Ljava/lang/Integer;>;" */
(( java.util.ArrayList ) v4 ).iterator ( ); // invoke-virtual {v4}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;
v12 = } // :goto_1
if ( v12 != null) { // if-eqz v12, :cond_4
/* check-cast v12, Lcom/miui/server/stability/DmaBufProcUsageInfo; */
/* .line 477 */
/* .local v12, "procInfo":Lcom/miui/server/stability/DmaBufProcUsageInfo; */
(( com.miui.server.stability.DmaBufProcUsageInfo ) v12 ).getName ( ); // invoke-virtual {v12}, Lcom/miui/server/stability/DmaBufProcUsageInfo;->getName()Ljava/lang/String;
/* .line 478 */
/* .local v13, "procName":Ljava/lang/String; */
final String v14 = "/system/bin/surfaceflinger"; // const-string v14, "/system/bin/surfaceflinger"
v14 = (( java.lang.String ) v14 ).equals ( v13 ); // invoke-virtual {v14, v13}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z
if ( v14 != null) { // if-eqz v14, :cond_2
/* .line 479 */
/* move-object v9, v12 */
/* .line 481 */
} // :cond_2
v14 = v14 = com.miui.server.stability.ScoutMemoryUtils.skipIonProcList;
final String v15 = " adj="; // const-string v15, " adj="
if ( v14 != null) { // if-eqz v14, :cond_3
/* .line 482 */
v14 = (( com.miui.server.stability.DmaBufProcUsageInfo ) v12 ).getPid ( ); // invoke-virtual {v12}, Lcom/miui/server/stability/DmaBufProcUsageInfo;->getPid()I
java.lang.Integer .valueOf ( v14 );
(( java.util.HashSet ) v10 ).add ( v14 ); // invoke-virtual {v10, v14}, Ljava/util/HashSet;->add(Ljava/lang/Object;)Z
/* .line 483 */
/* new-instance v14, Ljava/lang/StringBuilder; */
/* invoke-direct {v14}, Ljava/lang/StringBuilder;-><init>()V */
final String v5 = "Skip "; // const-string v5, "Skip "
(( java.lang.StringBuilder ) v14 ).append ( v5 ); // invoke-virtual {v14, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v5 ).append ( v13 ); // invoke-virtual {v5, v13}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
final String v14 = "(pid="; // const-string v14, "(pid="
(( java.lang.StringBuilder ) v5 ).append ( v14 ); // invoke-virtual {v5, v14}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
v14 = (( com.miui.server.stability.DmaBufProcUsageInfo ) v12 ).getPid ( ); // invoke-virtual {v12}, Lcom/miui/server/stability/DmaBufProcUsageInfo;->getPid()I
(( java.lang.StringBuilder ) v5 ).append ( v14 ); // invoke-virtual {v5, v14}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v5 ).append ( v15 ); // invoke-virtual {v5, v15}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
/* .line 484 */
v14 = (( com.miui.server.stability.DmaBufProcUsageInfo ) v12 ).getOomadj ( ); // invoke-virtual {v12}, Lcom/miui/server/stability/DmaBufProcUsageInfo;->getOomadj()I
(( java.lang.StringBuilder ) v5 ).append ( v14 ); // invoke-virtual {v5, v14}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;
final String v14 = ")"; // const-string v14, ")"
(( java.lang.StringBuilder ) v5 ).append ( v14 ); // invoke-virtual {v5, v14}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v5 ).toString ( ); // invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
/* .line 483 */
android.util.Slog .e ( v2,v5 );
/* .line 485 */
int v5 = 0; // const/4 v5, 0x0
/* .line 487 */
} // :cond_3
/* move-object v8, v12 */
/* .line 488 */
v5 = (( com.miui.server.stability.DmaBufProcUsageInfo ) v8 ).getPid ( ); // invoke-virtual {v8}, Lcom/miui/server/stability/DmaBufProcUsageInfo;->getPid()I
java.lang.Integer .valueOf ( v5 );
(( java.util.HashSet ) v10 ).add ( v5 ); // invoke-virtual {v10, v5}, Ljava/util/HashSet;->add(Ljava/lang/Object;)Z
/* .line 489 */
/* new-instance v5, Ljava/lang/StringBuilder; */
/* invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V */
final String v11 = "Most used process name="; // const-string v11, "Most used process name="
(( java.lang.StringBuilder ) v5 ).append ( v11 ); // invoke-virtual {v5, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v5 ).append ( v13 ); // invoke-virtual {v5, v13}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
final String v11 = " pid="; // const-string v11, " pid="
(( java.lang.StringBuilder ) v5 ).append ( v11 ); // invoke-virtual {v5, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
/* .line 490 */
v11 = (( com.miui.server.stability.DmaBufProcUsageInfo ) v12 ).getPid ( ); // invoke-virtual {v12}, Lcom/miui/server/stability/DmaBufProcUsageInfo;->getPid()I
(( java.lang.StringBuilder ) v5 ).append ( v11 ); // invoke-virtual {v5, v11}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v5 ).append ( v15 ); // invoke-virtual {v5, v15}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
v11 = (( com.miui.server.stability.DmaBufProcUsageInfo ) v12 ).getOomadj ( ); // invoke-virtual {v12}, Lcom/miui/server/stability/DmaBufProcUsageInfo;->getOomadj()I
(( java.lang.StringBuilder ) v5 ).append ( v11 ); // invoke-virtual {v5, v11}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;
final String v11 = " rss="; // const-string v11, " rss="
(( java.lang.StringBuilder ) v5 ).append ( v11 ); // invoke-virtual {v5, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
/* .line 491 */
(( com.miui.server.stability.DmaBufProcUsageInfo ) v12 ).getRss ( ); // invoke-virtual {v12}, Lcom/miui/server/stability/DmaBufProcUsageInfo;->getRss()J
/* move-result-wide v14 */
(( java.lang.StringBuilder ) v5 ).append ( v14, v15 ); // invoke-virtual {v5, v14, v15}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v5 ).toString ( ); // invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
/* .line 489 */
android.util.Slog .e ( v2,v5 );
/* .line 492 */
/* nop */
/* .line 495 */
} // .end local v12 # "procInfo":Lcom/miui/server/stability/DmaBufProcUsageInfo;
} // .end local v13 # "procName":Ljava/lang/String;
} // :cond_4
if ( v8 != null) { // if-eqz v8, :cond_9
/* .line 496 */
v5 = android.os.Process .myPid ( );
java.lang.Integer .valueOf ( v5 );
(( java.util.HashSet ) v10 ).add ( v5 ); // invoke-virtual {v10, v5}, Ljava/util/HashSet;->add(Ljava/lang/Object;)Z
/* .line 497 */
v5 = this.val$reason;
com.miui.server.stability.ScoutMemoryUtils .captureIonLeakLog ( v1,v5,v10 );
/* .line 498 */
/* .local v5, "zipPath":Ljava/lang/String; */
v11 = com.miui.server.stability.ScoutDisplayMemoryManager .-$$Nest$sfgetDMABUF_LEAK_THRESHOLD ( );
/* int-to-long v11, v11 */
/* mul-long/2addr v11, v6 */
/* long-to-double v11, v11 */
/* const-wide v13, 0x3fe3333333333333L # 0.6 */
/* mul-double/2addr v11, v13 */
/* double-to-long v11, v11 */
/* .line 499 */
/* .local v11, "thresholdSize":J */
/* new-instance v13, Lcom/miui/server/stability/ScoutDisplayMemoryManager$DiaplayMemoryErrorInfo; */
/* iget-wide v14, v0, Lcom/miui/server/stability/ScoutDisplayMemoryManager$3;->val$totalSizeKB:J */
/* move-object/from16 v16, v13 */
/* move-object/from16 v17, v8 */
/* move-wide/from16 v18, v11 */
/* move-wide/from16 v20, v14 */
/* invoke-direct/range {v16 ..v21}, Lcom/miui/server/stability/ScoutDisplayMemoryManager$DiaplayMemoryErrorInfo;-><init>(Lcom/miui/server/stability/DmaBufProcUsageInfo;JJ)V */
/* .line 500 */
/* .local v13, "errorInfo":Lcom/miui/server/stability/ScoutDisplayMemoryManager$DiaplayMemoryErrorInfo; */
(( com.miui.server.stability.DmaBufProcUsageInfo ) v8 ).getRss ( ); // invoke-virtual {v8}, Lcom/miui/server/stability/DmaBufProcUsageInfo;->getRss()J
/* move-result-wide v14 */
/* cmp-long v14, v14, v11 */
/* if-lez v14, :cond_6 */
/* .line 501 */
v14 = com.miui.server.stability.ScoutDisplayMemoryManager .preserveCrimeSceneIfNeed ( v13 );
/* if-nez v14, :cond_6 */
/* .line 502 */
v2 = this.this$0;
v2 = (( com.miui.server.stability.ScoutDisplayMemoryManager ) v2 ).isEnableResumeFeature ( ); // invoke-virtual {v2}, Lcom/miui/server/stability/ScoutDisplayMemoryManager;->isEnableResumeFeature()Z
if ( v2 != null) { // if-eqz v2, :cond_5
/* .line 503 */
v2 = this.this$0;
com.miui.server.stability.ScoutDisplayMemoryManager .-$$Nest$mresumeMemLeak ( v2,v13 );
/* .line 505 */
} // :cond_5
v2 = this.this$0;
(( com.miui.server.stability.ScoutDisplayMemoryManager ) v2 ).reportDisplayMemoryLeakEvent ( v13 ); // invoke-virtual {v2, v13}, Lcom/miui/server/stability/ScoutDisplayMemoryManager;->reportDisplayMemoryLeakEvent(Lcom/miui/server/stability/ScoutDisplayMemoryManager$DiaplayMemoryErrorInfo;)V
/* .line 507 */
} // :goto_2
v2 = this.this$0;
(( com.miui.server.stability.ScoutDisplayMemoryManager ) v2 ).handleReportMqs ( v13, v5 ); // invoke-virtual {v2, v13, v5}, Lcom/miui/server/stability/ScoutDisplayMemoryManager;->handleReportMqs(Lcom/miui/server/stability/ScoutDisplayMemoryManager$DiaplayMemoryErrorInfo;Ljava/lang/String;)V
/* move-object/from16 v16, v1 */
/* move-object/from16 v17, v3 */
/* move-object/from16 v18, v4 */
/* .line 508 */
} // :cond_6
v14 = miui.mqsas.scout.ScoutUtils .isLibraryTest ( );
if ( v14 != null) { // if-eqz v14, :cond_7
if ( v9 != null) { // if-eqz v9, :cond_7
/* .line 509 */
(( com.miui.server.stability.DmaBufProcUsageInfo ) v9 ).getRss ( ); // invoke-virtual {v9}, Lcom/miui/server/stability/DmaBufProcUsageInfo;->getRss()J
/* move-result-wide v14 */
/* move-object/from16 v16, v1 */
} // .end local v1 # "dambufLog":Ljava/lang/String;
/* .local v16, "dambufLog":Ljava/lang/String; */
v1 = com.miui.server.stability.ScoutDisplayMemoryManager .-$$Nest$sfgetDMABUF_LEAK_SF_THRESHOLD ( );
/* move-object/from16 v17, v3 */
/* move-object/from16 v18, v4 */
} // .end local v3 # "dmabufInfo":Lcom/miui/server/stability/DmaBufUsageInfo;
} // .end local v4 # "infoList":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/miui/server/stability/DmaBufProcUsageInfo;>;"
/* .local v17, "dmabufInfo":Lcom/miui/server/stability/DmaBufUsageInfo; */
/* .local v18, "infoList":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/miui/server/stability/DmaBufProcUsageInfo;>;" */
/* int-to-long v3, v1 */
/* mul-long/2addr v3, v6 */
/* cmp-long v1, v14, v3 */
/* if-lez v1, :cond_8 */
/* iget-wide v3, v0, Lcom/miui/server/stability/ScoutDisplayMemoryManager$3;->val$totalSizeKB:J */
/* .line 510 */
/* const-string/jumbo v1, "surfaceflinger" */
int v6 = -1; // const/4 v6, -0x1
final String v7 = "dmabuf"; // const-string v7, "dmabuf"
v1 = com.miui.server.stability.ScoutDisplayMemoryManager .-$$Nest$smdoPreserveCrimeSceneIfNeed ( v1,v6,v3,v4,v7 );
/* if-nez v1, :cond_8 */
/* .line 513 */
/* new-instance v1, Ljava/lang/StringBuilder; */
/* invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V */
/* const-string/jumbo v3, "surfaceflinger consumed " */
(( java.lang.StringBuilder ) v1 ).append ( v3 ); // invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
/* iget-wide v3, v0, Lcom/miui/server/stability/ScoutDisplayMemoryManager$3;->val$totalSizeKB:J */
(( java.lang.StringBuilder ) v1 ).append ( v3, v4 ); // invoke-virtual {v1, v3, v4}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;
final String v3 = "KB dmabuf memory totally, killing systemui and miui home"; // const-string v3, "KB dmabuf memory totally, killing systemui and miui home"
(( java.lang.StringBuilder ) v1 ).append ( v3 ); // invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v1 ).toString ( ); // invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
android.util.Slog .w ( v2,v1 );
/* .line 515 */
android.view.SurfaceControlStub .getInstance ( );
/* check-cast v1, Landroid/view/SurfaceControlImpl; */
/* .line 516 */
/* .local v1, "scImpl":Landroid/view/SurfaceControlImpl; */
int v2 = 0; // const/4 v2, 0x0
(( android.view.SurfaceControlImpl ) v1 ).startAsyncDumpIfNeed ( v2 ); // invoke-virtual {v1, v2}, Landroid/view/SurfaceControlImpl;->startAsyncDumpIfNeed(Z)V
/* .line 517 */
(( android.view.SurfaceControlImpl ) v1 ).applyGenialRenovation ( ); // invoke-virtual {v1}, Landroid/view/SurfaceControlImpl;->applyGenialRenovation()V
/* .line 520 */
int v2 = 1; // const/4 v2, 0x1
(( android.view.SurfaceControlImpl ) v1 ).increaseLeakLevel ( v2 ); // invoke-virtual {v1, v2}, Landroid/view/SurfaceControlImpl;->increaseLeakLevel(Z)Ljava/lang/String;
/* .line 508 */
} // .end local v16 # "dambufLog":Ljava/lang/String;
} // .end local v17 # "dmabufInfo":Lcom/miui/server/stability/DmaBufUsageInfo;
} // .end local v18 # "infoList":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/miui/server/stability/DmaBufProcUsageInfo;>;"
/* .local v1, "dambufLog":Ljava/lang/String; */
/* .restart local v3 # "dmabufInfo":Lcom/miui/server/stability/DmaBufUsageInfo; */
/* .restart local v4 # "infoList":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/miui/server/stability/DmaBufProcUsageInfo;>;" */
} // :cond_7
/* move-object/from16 v16, v1 */
/* move-object/from16 v17, v3 */
/* move-object/from16 v18, v4 */
/* .line 522 */
} // .end local v1 # "dambufLog":Ljava/lang/String;
} // .end local v3 # "dmabufInfo":Lcom/miui/server/stability/DmaBufUsageInfo;
} // .end local v4 # "infoList":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/miui/server/stability/DmaBufProcUsageInfo;>;"
/* .restart local v16 # "dambufLog":Ljava/lang/String; */
/* .restart local v17 # "dmabufInfo":Lcom/miui/server/stability/DmaBufUsageInfo; */
/* .restart local v18 # "infoList":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/miui/server/stability/DmaBufProcUsageInfo;>;" */
} // :cond_8
} // :goto_3
v1 = this.this$0;
(( com.miui.server.stability.ScoutDisplayMemoryManager ) v1 ).updateLastReportTime ( ); // invoke-virtual {v1}, Lcom/miui/server/stability/ScoutDisplayMemoryManager;->updateLastReportTime()V
/* .line 495 */
} // .end local v5 # "zipPath":Ljava/lang/String;
} // .end local v11 # "thresholdSize":J
} // .end local v13 # "errorInfo":Lcom/miui/server/stability/ScoutDisplayMemoryManager$DiaplayMemoryErrorInfo;
} // .end local v16 # "dambufLog":Ljava/lang/String;
} // .end local v17 # "dmabufInfo":Lcom/miui/server/stability/DmaBufUsageInfo;
} // .end local v18 # "infoList":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/miui/server/stability/DmaBufProcUsageInfo;>;"
/* .restart local v1 # "dambufLog":Ljava/lang/String; */
/* .restart local v3 # "dmabufInfo":Lcom/miui/server/stability/DmaBufUsageInfo; */
/* .restart local v4 # "infoList":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/miui/server/stability/DmaBufProcUsageInfo;>;" */
} // :cond_9
/* move-object/from16 v16, v1 */
/* move-object/from16 v17, v3 */
/* move-object/from16 v18, v4 */
} // .end local v1 # "dambufLog":Ljava/lang/String;
} // .end local v3 # "dmabufInfo":Lcom/miui/server/stability/DmaBufUsageInfo;
} // .end local v4 # "infoList":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/miui/server/stability/DmaBufProcUsageInfo;>;"
/* .restart local v16 # "dambufLog":Ljava/lang/String; */
/* .restart local v17 # "dmabufInfo":Lcom/miui/server/stability/DmaBufUsageInfo; */
/* .restart local v18 # "infoList":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/miui/server/stability/DmaBufProcUsageInfo;>;" */
/* .line 474 */
} // .end local v10 # "fdPids":Ljava/util/HashSet;, "Ljava/util/HashSet<Ljava/lang/Integer;>;"
} // .end local v16 # "dambufLog":Ljava/lang/String;
} // .end local v17 # "dmabufInfo":Lcom/miui/server/stability/DmaBufUsageInfo;
} // .end local v18 # "infoList":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/miui/server/stability/DmaBufProcUsageInfo;>;"
/* .restart local v1 # "dambufLog":Ljava/lang/String; */
/* .restart local v3 # "dmabufInfo":Lcom/miui/server/stability/DmaBufUsageInfo; */
/* .restart local v4 # "infoList":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/miui/server/stability/DmaBufProcUsageInfo;>;" */
} // :cond_a
/* move-object/from16 v16, v1 */
/* move-object/from16 v17, v3 */
/* move-object/from16 v18, v4 */
/* .line 525 */
} // .end local v1 # "dambufLog":Ljava/lang/String;
} // .end local v3 # "dmabufInfo":Lcom/miui/server/stability/DmaBufUsageInfo;
} // .end local v4 # "infoList":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/miui/server/stability/DmaBufProcUsageInfo;>;"
/* .restart local v16 # "dambufLog":Ljava/lang/String; */
/* .restart local v17 # "dmabufInfo":Lcom/miui/server/stability/DmaBufUsageInfo; */
/* .restart local v18 # "infoList":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/miui/server/stability/DmaBufProcUsageInfo;>;" */
} // :goto_4
v1 = this.this$0;
com.miui.server.stability.ScoutDisplayMemoryManager .-$$Nest$fgetisBusy ( v1 );
int v2 = 0; // const/4 v2, 0x0
(( java.util.concurrent.atomic.AtomicBoolean ) v1 ).set ( v2 ); // invoke-virtual {v1, v2}, Ljava/util/concurrent/atomic/AtomicBoolean;->set(Z)V
/* .line 526 */
return;
} // .end method
