public class inal extends android.content.BroadcastReceiver {
	 /* .source "StabilityMemoryMonitor.java" */
	 /* # annotations */
	 /* .annotation system Ldalvik/annotation/EnclosingClass; */
	 /* value = Lcom/miui/server/stability/StabilityMemoryMonitor; */
} // .end annotation
/* .annotation system Ldalvik/annotation/InnerClass; */
/* accessFlags = 0x11 */
/* name = "AlarmReceiver" */
} // .end annotation
/* # instance fields */
final com.miui.server.stability.StabilityMemoryMonitor this$0; //synthetic
/* # direct methods */
public inal ( ) {
/* .locals 0 */
/* .param p1, "this$0" # Lcom/miui/server/stability/StabilityMemoryMonitor; */
/* .line 59 */
this.this$0 = p1;
/* invoke-direct {p0}, Landroid/content/BroadcastReceiver;-><init>()V */
return;
} // .end method
/* # virtual methods */
public void onReceive ( android.content.Context p0, android.content.Intent p1 ) {
/* .locals 3 */
/* .param p1, "context" # Landroid/content/Context; */
/* .param p2, "intent" # Landroid/content/Intent; */
/* .line 62 */
v0 = this.this$0;
final String v1 = "persist.sys.stability_memory_monitor.enable"; // const-string v1, "persist.sys.stability_memory_monitor.enable"
int v2 = 0; // const/4 v2, 0x0
v1 = android.os.SystemProperties .getBoolean ( v1,v2 );
com.miui.server.stability.StabilityMemoryMonitor .-$$Nest$fputmEnabled ( v0,v1 );
/* .line 63 */
v0 = this.this$0;
v0 = com.miui.server.stability.StabilityMemoryMonitor .-$$Nest$fgetmEnabled ( v0 );
/* if-nez v0, :cond_0 */
/* .line 64 */
return;
/* .line 67 */
} // :cond_0
(( android.content.Intent ) p2 ).getAction ( ); // invoke-virtual {p2}, Landroid/content/Intent;->getAction()Ljava/lang/String;
v1 = (( java.lang.String ) v0 ).hashCode ( ); // invoke-virtual {v0}, Ljava/lang/String;->hashCode()I
/* packed-switch v1, :pswitch_data_0 */
} // :cond_1
/* :pswitch_0 */
final String v1 = "miui.intent.action.SET_STABILITY_MEMORY_MONITOR_ALARM"; // const-string v1, "miui.intent.action.SET_STABILITY_MEMORY_MONITOR_ALARM"
v0 = (( java.lang.String ) v0 ).equals ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z
if ( v0 != null) { // if-eqz v0, :cond_1
} // :goto_0
int v2 = -1; // const/4 v2, -0x1
} // :goto_1
/* packed-switch v2, :pswitch_data_1 */
/* .line 69 */
/* :pswitch_1 */
v0 = this.this$0;
v0 = com.miui.server.stability.StabilityMemoryMonitor .-$$Nest$fgetmScreenState ( v0 );
/* if-nez v0, :cond_2 */
/* .line 70 */
v0 = this.this$0;
com.miui.server.stability.StabilityMemoryMonitor .-$$Nest$msendTask ( v0 );
/* .line 72 */
} // :cond_2
v0 = this.this$0;
int v1 = 1; // const/4 v1, 0x1
com.miui.server.stability.StabilityMemoryMonitor .-$$Nest$fputmPendingWork ( v0,v1 );
/* .line 74 */
/* nop */
/* .line 78 */
} // :goto_2
return;
/* nop */
/* :pswitch_data_0 */
/* .packed-switch 0x207f19a5 */
/* :pswitch_0 */
} // .end packed-switch
/* :pswitch_data_1 */
/* .packed-switch 0x0 */
/* :pswitch_1 */
} // .end packed-switch
} // .end method
