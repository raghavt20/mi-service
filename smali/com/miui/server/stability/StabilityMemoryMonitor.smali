.class public Lcom/miui/server/stability/StabilityMemoryMonitor;
.super Ljava/lang/Object;
.source "StabilityMemoryMonitor.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/miui/server/stability/StabilityMemoryMonitor$AlarmReceiver;
    }
.end annotation


# static fields
.field private static final ACTION_SMM_ALARM:Ljava/lang/String; = "miui.intent.action.SET_STABILITY_MEMORY_MONITOR_ALARM"

.field private static final ALARM_HOUR:I = 0x3

.field private static final ALARM_MINUTE:I = 0x0

.field private static final ALARM_SECOND:I = 0x0

.field private static final CLOUD_CONFIG_FILE:Ljava/lang/String; = "/data/mqsas/cloud/stability_memory_monitor_config.json"

.field private static final DEFAULT_CONFIG_DATA:Ljava/util/HashMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/HashMap<",
            "Ljava/lang/String;",
            "Ljava/lang/Long;",
            ">;"
        }
    .end annotation
.end field

.field private static final SMM_CONTROL_PROPERTY:Ljava/lang/String; = "persist.sys.stability_memory_monitor.enable"

.field private static final TAG:Ljava/lang/String; = "StabilityMemoryMonitor"

.field private static volatile sInstance:Lcom/miui/server/stability/StabilityMemoryMonitor;


# instance fields
.field private mContext:Landroid/content/Context;

.field private mEnabled:Z

.field private volatile mPendingWork:Z

.field private volatile mScreenState:Z


# direct methods
.method public static synthetic $r8$lambda$6uMJGpZ24PDQiQz19BGYy8VVh7w(Lcom/miui/server/stability/StabilityMemoryMonitor;)V
    .locals 0

    invoke-direct {p0}, Lcom/miui/server/stability/StabilityMemoryMonitor;->lambda$sendTask$0()V

    return-void
.end method

.method static bridge synthetic -$$Nest$fgetmEnabled(Lcom/miui/server/stability/StabilityMemoryMonitor;)Z
    .locals 0

    iget-boolean p0, p0, Lcom/miui/server/stability/StabilityMemoryMonitor;->mEnabled:Z

    return p0
.end method

.method static bridge synthetic -$$Nest$fgetmScreenState(Lcom/miui/server/stability/StabilityMemoryMonitor;)Z
    .locals 0

    iget-boolean p0, p0, Lcom/miui/server/stability/StabilityMemoryMonitor;->mScreenState:Z

    return p0
.end method

.method static bridge synthetic -$$Nest$fputmEnabled(Lcom/miui/server/stability/StabilityMemoryMonitor;Z)V
    .locals 0

    iput-boolean p1, p0, Lcom/miui/server/stability/StabilityMemoryMonitor;->mEnabled:Z

    return-void
.end method

.method static bridge synthetic -$$Nest$fputmPendingWork(Lcom/miui/server/stability/StabilityMemoryMonitor;Z)V
    .locals 0

    iput-boolean p1, p0, Lcom/miui/server/stability/StabilityMemoryMonitor;->mPendingWork:Z

    return-void
.end method

.method static bridge synthetic -$$Nest$msendTask(Lcom/miui/server/stability/StabilityMemoryMonitor;)V
    .locals 0

    invoke-direct {p0}, Lcom/miui/server/stability/StabilityMemoryMonitor;->sendTask()V

    return-void
.end method

.method static constructor <clinit>()V
    .locals 1

    .line 40
    new-instance v0, Lcom/miui/server/stability/StabilityMemoryMonitor$1;

    invoke-direct {v0}, Lcom/miui/server/stability/StabilityMemoryMonitor$1;-><init>()V

    sput-object v0, Lcom/miui/server/stability/StabilityMemoryMonitor;->DEFAULT_CONFIG_DATA:Ljava/util/HashMap;

    return-void
.end method

.method private constructor <init>()V
    .locals 2

    .line 81
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 55
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/miui/server/stability/StabilityMemoryMonitor;->mPendingWork:Z

    .line 57
    iput-boolean v0, p0, Lcom/miui/server/stability/StabilityMemoryMonitor;->mScreenState:Z

    .line 82
    const-string v1, "persist.sys.stability_memory_monitor.enable"

    invoke-static {v1, v0}, Landroid/os/SystemProperties;->getBoolean(Ljava/lang/String;Z)Z

    move-result v0

    iput-boolean v0, p0, Lcom/miui/server/stability/StabilityMemoryMonitor;->mEnabled:Z

    .line 83
    return-void
.end method

.method private getCloudConfig()Ljava/util/HashMap;
    .locals 11
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/HashMap<",
            "Ljava/lang/String;",
            "Ljava/lang/Long;",
            ">;"
        }
    .end annotation

    .line 183
    new-instance v0, Ljava/io/File;

    const-string v1, "/data/mqsas/cloud/stability_memory_monitor_config.json"

    invoke-direct {v0, v1}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 184
    .local v0, "cloudConfigFile":Ljava/io/File;
    new-instance v1, Ljava/util/HashMap;

    sget-object v2, Lcom/miui/server/stability/StabilityMemoryMonitor;->DEFAULT_CONFIG_DATA:Ljava/util/HashMap;

    invoke-direct {v1, v2}, Ljava/util/HashMap;-><init>(Ljava/util/Map;)V

    .line 186
    .local v1, "configData":Ljava/util/HashMap;, "Ljava/util/HashMap<Ljava/lang/String;Ljava/lang/Long;>;"
    invoke-virtual {v0}, Ljava/io/File;->exists()Z

    move-result v2

    const-string v3, "StabilityMemoryMonitor"

    if-eqz v2, :cond_3

    .line 188
    const/4 v2, 0x0

    const/4 v4, 0x0

    :try_start_0
    invoke-static {v0, v2, v4}, Landroid/os/FileUtils;->readTextFile(Ljava/io/File;ILjava/lang/String;)Ljava/lang/String;

    move-result-object v2

    .line 189
    .local v2, "originData":Ljava/lang/String;
    invoke-virtual {v2}, Ljava/lang/String;->isEmpty()Z

    move-result v4

    if-nez v4, :cond_2

    .line 190
    new-instance v4, Lorg/json/JSONObject;

    invoke-direct {v4, v2}, Lorg/json/JSONObject;-><init>(Ljava/lang/String;)V

    .line 191
    .local v4, "jsonConfig":Lorg/json/JSONObject;
    const-string v5, "config"

    invoke-virtual {v4, v5}, Lorg/json/JSONObject;->getJSONArray(Ljava/lang/String;)Lorg/json/JSONArray;

    move-result-object v5

    .line 192
    .local v5, "configItems":Lorg/json/JSONArray;
    const/4 v6, 0x0

    .local v6, "i":I
    :goto_0
    invoke-virtual {v5}, Lorg/json/JSONArray;->length()I

    move-result v7

    if-ge v6, v7, :cond_1

    .line 193
    invoke-virtual {v5, v6}, Lorg/json/JSONArray;->get(I)Ljava/lang/Object;

    move-result-object v7

    check-cast v7, Lorg/json/JSONObject;

    .line 194
    .local v7, "item":Lorg/json/JSONObject;
    const-string v8, "name"

    invoke-virtual {v7, v8}, Lorg/json/JSONObject;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v8

    .line 195
    .local v8, "appPackage":Ljava/lang/String;
    const-string v9, "pss"

    invoke-virtual {v7, v9}, Lorg/json/JSONObject;->getInt(Ljava/lang/String;)I

    move-result v9

    int-to-long v9, v9

    invoke-static {v9, v10}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v9

    .line 196
    .local v9, "appPssLimit":Ljava/lang/Long;
    invoke-virtual {v1, v8}, Ljava/util/HashMap;->containsKey(Ljava/lang/Object;)Z

    move-result v10

    if-eqz v10, :cond_0

    .line 197
    invoke-virtual {v1, v8, v9}, Ljava/util/HashMap;->replace(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    goto :goto_1

    .line 199
    :cond_0
    invoke-virtual {v1, v8, v9}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 192
    .end local v7    # "item":Lorg/json/JSONObject;
    .end local v8    # "appPackage":Ljava/lang/String;
    .end local v9    # "appPssLimit":Ljava/lang/Long;
    :goto_1
    add-int/lit8 v6, v6, 0x1

    goto :goto_0

    .line 202
    .end local v4    # "jsonConfig":Lorg/json/JSONObject;
    .end local v5    # "configItems":Lorg/json/JSONArray;
    .end local v6    # "i":I
    :cond_1
    goto :goto_2

    .line 203
    :cond_2
    const-string v4, "cloud config file contains noting"

    invoke-static {v3, v4}, Landroid/util/Slog;->e(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_1
    .catch Lorg/json/JSONException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_2

    .line 208
    .end local v2    # "originData":Ljava/lang/String;
    :catch_0
    move-exception v2

    .line 209
    .local v2, "e":Lorg/json/JSONException;
    const-string v4, "retrive config data failed: JSONException"

    invoke-static {v3, v4}, Landroid/util/Slog;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 210
    invoke-virtual {v1}, Ljava/util/HashMap;->clear()V

    .line 211
    sget-object v3, Lcom/miui/server/stability/StabilityMemoryMonitor;->DEFAULT_CONFIG_DATA:Ljava/util/HashMap;

    invoke-virtual {v1, v3}, Ljava/util/HashMap;->putAll(Ljava/util/Map;)V

    .line 212
    invoke-virtual {v2}, Lorg/json/JSONException;->printStackTrace()V

    .end local v2    # "e":Lorg/json/JSONException;
    goto :goto_2

    .line 205
    :catch_1
    move-exception v2

    .line 206
    .local v2, "e":Ljava/io/IOException;
    const-string v4, "read cloud config file failed: IOException"

    invoke-static {v3, v4}, Landroid/util/Slog;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 207
    invoke-virtual {v2}, Ljava/io/IOException;->printStackTrace()V

    .line 213
    .end local v2    # "e":Ljava/io/IOException;
    :goto_2
    goto :goto_3

    .line 215
    :cond_3
    const-string v2, "can\'t find cloud config file"

    invoke-static {v3, v2}, Landroid/util/Slog;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 218
    :goto_3
    return-object v1
.end method

.method public static getInstance()Lcom/miui/server/stability/StabilityMemoryMonitor;
    .locals 1

    .line 86
    sget-object v0, Lcom/miui/server/stability/StabilityMemoryMonitor;->sInstance:Lcom/miui/server/stability/StabilityMemoryMonitor;

    if-nez v0, :cond_0

    .line 87
    new-instance v0, Lcom/miui/server/stability/StabilityMemoryMonitor;

    invoke-direct {v0}, Lcom/miui/server/stability/StabilityMemoryMonitor;-><init>()V

    sput-object v0, Lcom/miui/server/stability/StabilityMemoryMonitor;->sInstance:Lcom/miui/server/stability/StabilityMemoryMonitor;

    .line 89
    :cond_0
    sget-object v0, Lcom/miui/server/stability/StabilityMemoryMonitor;->sInstance:Lcom/miui/server/stability/StabilityMemoryMonitor;

    return-object v0
.end method

.method private getPidByPackage(Ljava/lang/String;)I
    .locals 6
    .param p1, "packageName"    # Ljava/lang/String;

    .line 169
    iget-object v0, p0, Lcom/miui/server/stability/StabilityMemoryMonitor;->mContext:Landroid/content/Context;

    const-string v1, "activity"

    invoke-virtual {v0, v1}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/app/ActivityManager;

    .line 170
    .local v0, "am":Landroid/app/ActivityManager;
    invoke-virtual {v0}, Landroid/app/ActivityManager;->getRunningAppProcesses()Ljava/util/List;

    move-result-object v1

    .line 171
    .local v1, "infos":Ljava/util/List;, "Ljava/util/List<Landroid/app/ActivityManager$RunningAppProcessInfo;>;"
    const/4 v2, -0x1

    .line 173
    .local v2, "pid":I
    invoke-interface {v1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :goto_0
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v4

    if-eqz v4, :cond_1

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Landroid/app/ActivityManager$RunningAppProcessInfo;

    .line 174
    .local v4, "info":Landroid/app/ActivityManager$RunningAppProcessInfo;
    iget-object v5, v4, Landroid/app/ActivityManager$RunningAppProcessInfo;->processName:Ljava/lang/String;

    invoke-virtual {v5, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-eqz v5, :cond_0

    .line 175
    iget v2, v4, Landroid/app/ActivityManager$RunningAppProcessInfo;->pid:I

    .line 176
    goto :goto_1

    .line 178
    .end local v4    # "info":Landroid/app/ActivityManager$RunningAppProcessInfo;
    :cond_0
    goto :goto_0

    .line 179
    :cond_1
    :goto_1
    return v2
.end method

.method private getPssByPackage(Ljava/lang/String;)J
    .locals 7
    .param p1, "packageName"    # Ljava/lang/String;

    .line 155
    iget-object v0, p0, Lcom/miui/server/stability/StabilityMemoryMonitor;->mContext:Landroid/content/Context;

    const-string v1, "activity"

    invoke-virtual {v0, v1}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/app/ActivityManager;

    .line 156
    .local v0, "am":Landroid/app/ActivityManager;
    invoke-virtual {v0}, Landroid/app/ActivityManager;->getRunningAppProcesses()Ljava/util/List;

    move-result-object v1

    .line 157
    .local v1, "infos":Ljava/util/List;, "Ljava/util/List<Landroid/app/ActivityManager$RunningAppProcessInfo;>;"
    const-wide/16 v2, 0x0

    .line 159
    .local v2, "pss":J
    invoke-interface {v1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v4

    :goto_0
    invoke-interface {v4}, Ljava/util/Iterator;->hasNext()Z

    move-result v5

    if-eqz v5, :cond_1

    invoke-interface {v4}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Landroid/app/ActivityManager$RunningAppProcessInfo;

    .line 160
    .local v5, "info":Landroid/app/ActivityManager$RunningAppProcessInfo;
    iget-object v6, v5, Landroid/app/ActivityManager$RunningAppProcessInfo;->processName:Ljava/lang/String;

    invoke-virtual {v6, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-eqz v6, :cond_0

    .line 161
    iget v4, v5, Landroid/app/ActivityManager$RunningAppProcessInfo;->pid:I

    const/4 v6, 0x0

    invoke-static {v4, v6, v6}, Landroid/os/Debug;->getPss(I[J[J)J

    move-result-wide v2

    .line 162
    goto :goto_1

    .line 164
    .end local v5    # "info":Landroid/app/ActivityManager$RunningAppProcessInfo;
    :cond_0
    goto :goto_0

    .line 165
    :cond_1
    :goto_1
    return-wide v2
.end method

.method private synthetic lambda$sendTask$0()V
    .locals 9

    .line 114
    invoke-direct {p0}, Lcom/miui/server/stability/StabilityMemoryMonitor;->getCloudConfig()Ljava/util/HashMap;

    move-result-object v0

    invoke-virtual {v0}, Ljava/util/HashMap;->entrySet()Ljava/util/Set;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v0

    :goto_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_1

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/util/Map$Entry;

    .line 115
    .local v1, "entry":Ljava/util/Map$Entry;, "Ljava/util/Map$Entry<Ljava/lang/String;Ljava/lang/Long;>;"
    invoke-interface {v1}, Ljava/util/Map$Entry;->getKey()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/lang/String;

    .line 116
    .local v2, "procName":Ljava/lang/String;
    invoke-interface {v1}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/lang/Long;

    invoke-virtual {v3}, Ljava/lang/Long;->longValue()J

    move-result-wide v3

    .line 117
    .local v3, "pssLimit":J
    invoke-direct {p0, v2}, Lcom/miui/server/stability/StabilityMemoryMonitor;->getPssByPackage(Ljava/lang/String;)J

    move-result-wide v5

    .line 118
    .local v5, "pssNow":J
    cmp-long v7, v5, v3

    if-lez v7, :cond_0

    .line 119
    invoke-direct {p0, v2}, Lcom/miui/server/stability/StabilityMemoryMonitor;->getPidByPackage(Ljava/lang/String;)I

    move-result v7

    invoke-static {v7}, Landroid/os/Process;->killProcess(I)V

    .line 120
    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    const-string v8, "kill process: "

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    const-string v8, " due to abnormal PSS occupied: "

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7, v5, v6}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    const-string v8, "StabilityMemoryMonitor"

    invoke-static {v8, v7}, Landroid/util/Slog;->w(Ljava/lang/String;Ljava/lang/String;)I

    .line 123
    .end local v1    # "entry":Ljava/util/Map$Entry;, "Ljava/util/Map$Entry<Ljava/lang/String;Ljava/lang/Long;>;"
    .end local v2    # "procName":Ljava/lang/String;
    .end local v3    # "pssLimit":J
    .end local v5    # "pssNow":J
    :cond_0
    goto :goto_0

    .line 124
    :cond_1
    return-void
.end method

.method private registerAlarmReceiver()V
    .locals 3

    .line 129
    new-instance v0, Landroid/content/IntentFilter;

    invoke-direct {v0}, Landroid/content/IntentFilter;-><init>()V

    .line 130
    .local v0, "filter":Landroid/content/IntentFilter;
    const-string v1, "miui.intent.action.SET_STABILITY_MEMORY_MONITOR_ALARM"

    invoke-virtual {v0, v1}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    .line 131
    iget-object v1, p0, Lcom/miui/server/stability/StabilityMemoryMonitor;->mContext:Landroid/content/Context;

    new-instance v2, Lcom/miui/server/stability/StabilityMemoryMonitor$AlarmReceiver;

    invoke-direct {v2, p0}, Lcom/miui/server/stability/StabilityMemoryMonitor$AlarmReceiver;-><init>(Lcom/miui/server/stability/StabilityMemoryMonitor;)V

    invoke-virtual {v1, v2, v0}, Landroid/content/Context;->registerReceiver(Landroid/content/BroadcastReceiver;Landroid/content/IntentFilter;)Landroid/content/Intent;

    .line 132
    return-void
.end method

.method private sendTask()V
    .locals 2

    .line 112
    invoke-static {}, Lcom/android/internal/os/BackgroundThread;->getHandler()Landroid/os/Handler;

    move-result-object v0

    .line 113
    .local v0, "bgHandler":Landroid/os/Handler;
    new-instance v1, Lcom/miui/server/stability/StabilityMemoryMonitor$$ExternalSyntheticLambda0;

    invoke-direct {v1, p0}, Lcom/miui/server/stability/StabilityMemoryMonitor$$ExternalSyntheticLambda0;-><init>(Lcom/miui/server/stability/StabilityMemoryMonitor;)V

    invoke-virtual {v0, v1}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    .line 125
    const/4 v1, 0x0

    iput-boolean v1, p0, Lcom/miui/server/stability/StabilityMemoryMonitor;->mPendingWork:Z

    .line 126
    return-void
.end method

.method private setAlarm()V
    .locals 12

    .line 135
    iget-object v0, p0, Lcom/miui/server/stability/StabilityMemoryMonitor;->mContext:Landroid/content/Context;

    const-string v1, "alarm"

    invoke-virtual {v0, v1}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/app/AlarmManager;

    .line 136
    .local v0, "alarm":Landroid/app/AlarmManager;
    new-instance v1, Landroid/content/Intent;

    invoke-direct {v1}, Landroid/content/Intent;-><init>()V

    move-object v8, v1

    .line 137
    .local v8, "intent":Landroid/content/Intent;
    const-string v1, "miui.intent.action.SET_STABILITY_MEMORY_MONITOR_ALARM"

    invoke-virtual {v8, v1}, Landroid/content/Intent;->setAction(Ljava/lang/String;)Landroid/content/Intent;

    .line 138
    iget-object v1, p0, Lcom/miui/server/stability/StabilityMemoryMonitor;->mContext:Landroid/content/Context;

    const/high16 v2, 0x4000000

    const/4 v3, 0x0

    invoke-static {v1, v3, v8, v2}, Landroid/app/PendingIntent;->getBroadcast(Landroid/content/Context;ILandroid/content/Intent;I)Landroid/app/PendingIntent;

    move-result-object v9

    .line 141
    .local v9, "pi":Landroid/app/PendingIntent;
    invoke-static {}, Ljava/util/Calendar;->getInstance()Ljava/util/Calendar;

    move-result-object v10

    .line 142
    .local v10, "calendar":Ljava/util/Calendar;
    const/16 v1, 0xb

    const/4 v2, 0x3

    invoke-virtual {v10, v1, v2}, Ljava/util/Calendar;->set(II)V

    .line 143
    const/16 v1, 0xc

    invoke-virtual {v10, v1, v3}, Ljava/util/Calendar;->set(II)V

    .line 144
    const/16 v1, 0xd

    invoke-virtual {v10, v1, v3}, Ljava/util/Calendar;->set(II)V

    .line 145
    invoke-static {}, Ljava/util/Calendar;->getInstance()Ljava/util/Calendar;

    move-result-object v11

    .line 146
    .local v11, "now":Ljava/util/Calendar;
    invoke-virtual {v11, v10}, Ljava/util/Calendar;->compareTo(Ljava/util/Calendar;)I

    move-result v1

    if-lez v1, :cond_0

    .line 147
    const/4 v1, 0x5

    const/4 v2, 0x1

    invoke-virtual {v10, v1, v2}, Ljava/util/Calendar;->add(II)V

    .line 150
    :cond_0
    const/4 v2, 0x0

    invoke-virtual {v10}, Ljava/util/Calendar;->getTimeInMillis()J

    move-result-wide v3

    const-wide/32 v5, 0x5265c00

    move-object v1, v0

    move-object v7, v9

    invoke-virtual/range {v1 .. v7}, Landroid/app/AlarmManager;->setRepeating(IJJLandroid/app/PendingIntent;)V

    .line 152
    return-void
.end method


# virtual methods
.method public initContext(Landroid/content/Context;)V
    .locals 0
    .param p1, "context"    # Landroid/content/Context;

    .line 93
    iput-object p1, p0, Lcom/miui/server/stability/StabilityMemoryMonitor;->mContext:Landroid/content/Context;

    .line 94
    return-void
.end method

.method public startMemoryMonitor()V
    .locals 0

    .line 97
    invoke-direct {p0}, Lcom/miui/server/stability/StabilityMemoryMonitor;->registerAlarmReceiver()V

    .line 98
    invoke-direct {p0}, Lcom/miui/server/stability/StabilityMemoryMonitor;->setAlarm()V

    .line 99
    return-void
.end method

.method public updateScreenState(Z)V
    .locals 1
    .param p1, "screenOn"    # Z

    .line 102
    iput-boolean p1, p0, Lcom/miui/server/stability/StabilityMemoryMonitor;->mScreenState:Z

    .line 103
    iget-boolean v0, p0, Lcom/miui/server/stability/StabilityMemoryMonitor;->mEnabled:Z

    if-nez v0, :cond_0

    .line 104
    return-void

    .line 106
    :cond_0
    iget-boolean v0, p0, Lcom/miui/server/stability/StabilityMemoryMonitor;->mScreenState:Z

    if-nez v0, :cond_1

    iget-boolean v0, p0, Lcom/miui/server/stability/StabilityMemoryMonitor;->mPendingWork:Z

    if-eqz v0, :cond_1

    .line 107
    invoke-direct {p0}, Lcom/miui/server/stability/StabilityMemoryMonitor;->sendTask()V

    .line 109
    :cond_1
    return-void
.end method
