public class com.miui.server.stability.ScoutLibraryTestManager {
	 /* .source "ScoutLibraryTestManager.java" */
	 /* # static fields */
	 public static final java.lang.String PROPERTIES_APP_MTBF_TEST;
	 private static final java.lang.String TAG;
	 private static Boolean debug;
	 private static com.miui.server.stability.ScoutLibraryTestManager scoutLibraryTestManager;
	 /* # direct methods */
	 static com.miui.server.stability.ScoutLibraryTestManager ( ) {
		 /* .locals 1 */
		 /* .line 30 */
		 /* sget-boolean v0, Lcom/android/server/ScoutHelper;->ENABLED_SCOUT_DEBUG:Z */
		 com.miui.server.stability.ScoutLibraryTestManager.debug = (v0!= 0);
		 return;
	 } // .end method
	 private com.miui.server.stability.ScoutLibraryTestManager ( ) {
		 /* .locals 0 */
		 /* .line 43 */
		 /* invoke-direct {p0}, Ljava/lang/Object;-><init>()V */
		 /* .line 44 */
		 return;
	 } // .end method
	 private native void enableCoreDump ( ) {
	 } // .end method
	 public static com.miui.server.stability.ScoutLibraryTestManager getInstance ( ) {
		 /* .locals 1 */
		 /* .line 37 */
		 v0 = com.miui.server.stability.ScoutLibraryTestManager.scoutLibraryTestManager;
		 /* if-nez v0, :cond_0 */
		 /* .line 38 */
		 /* new-instance v0, Lcom/miui/server/stability/ScoutLibraryTestManager; */
		 /* invoke-direct {v0}, Lcom/miui/server/stability/ScoutLibraryTestManager;-><init>()V */
		 /* .line 40 */
	 } // :cond_0
	 v0 = com.miui.server.stability.ScoutLibraryTestManager.scoutLibraryTestManager;
} // .end method
/* # virtual methods */
public void init ( ) {
	 /* .locals 2 */
	 /* .line 47 */
	 /* sget-boolean v0, Lmiui/mqsas/scout/ScoutUtils;->REBOOT_COREDUMP:Z */
	 if ( v0 != null) { // if-eqz v0, :cond_0
		 /* .line 48 */
		 /* invoke-direct {p0}, Lcom/miui/server/stability/ScoutLibraryTestManager;->enableCoreDump()V */
		 /* .line 49 */
		 final String v0 = "ScoutLibraryTestManager"; // const-string v0, "ScoutLibraryTestManager"
		 final String v1 = "enable system_server CoreDump"; // const-string v1, "enable system_server CoreDump"
		 android.util.Slog .d ( v0,v1 );
		 /* .line 52 */
	 } // :cond_0
	 final String v0 = "persist.sys.debug.app.mtbf_test"; // const-string v0, "persist.sys.debug.app.mtbf_test"
	 /* const-string/jumbo v1, "true" */
	 android.os.SystemProperties .set ( v0,v1 );
	 /* .line 53 */
	 return;
} // .end method
