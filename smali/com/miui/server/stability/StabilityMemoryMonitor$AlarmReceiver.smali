.class public final Lcom/miui/server/stability/StabilityMemoryMonitor$AlarmReceiver;
.super Landroid/content/BroadcastReceiver;
.source "StabilityMemoryMonitor.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/miui/server/stability/StabilityMemoryMonitor;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x11
    name = "AlarmReceiver"
.end annotation


# instance fields
.field final synthetic this$0:Lcom/miui/server/stability/StabilityMemoryMonitor;


# direct methods
.method public constructor <init>(Lcom/miui/server/stability/StabilityMemoryMonitor;)V
    .locals 0
    .param p1, "this$0"    # Lcom/miui/server/stability/StabilityMemoryMonitor;

    .line 59
    iput-object p1, p0, Lcom/miui/server/stability/StabilityMemoryMonitor$AlarmReceiver;->this$0:Lcom/miui/server/stability/StabilityMemoryMonitor;

    invoke-direct {p0}, Landroid/content/BroadcastReceiver;-><init>()V

    return-void
.end method


# virtual methods
.method public onReceive(Landroid/content/Context;Landroid/content/Intent;)V
    .locals 3
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "intent"    # Landroid/content/Intent;

    .line 62
    iget-object v0, p0, Lcom/miui/server/stability/StabilityMemoryMonitor$AlarmReceiver;->this$0:Lcom/miui/server/stability/StabilityMemoryMonitor;

    const-string v1, "persist.sys.stability_memory_monitor.enable"

    const/4 v2, 0x0

    invoke-static {v1, v2}, Landroid/os/SystemProperties;->getBoolean(Ljava/lang/String;Z)Z

    move-result v1

    invoke-static {v0, v1}, Lcom/miui/server/stability/StabilityMemoryMonitor;->-$$Nest$fputmEnabled(Lcom/miui/server/stability/StabilityMemoryMonitor;Z)V

    .line 63
    iget-object v0, p0, Lcom/miui/server/stability/StabilityMemoryMonitor$AlarmReceiver;->this$0:Lcom/miui/server/stability/StabilityMemoryMonitor;

    invoke-static {v0}, Lcom/miui/server/stability/StabilityMemoryMonitor;->-$$Nest$fgetmEnabled(Lcom/miui/server/stability/StabilityMemoryMonitor;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 64
    return-void

    .line 67
    :cond_0
    invoke-virtual {p2}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/String;->hashCode()I

    move-result v1

    packed-switch v1, :pswitch_data_0

    :cond_1
    goto :goto_0

    :pswitch_0
    const-string v1, "miui.intent.action.SET_STABILITY_MEMORY_MONITOR_ALARM"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    goto :goto_1

    :goto_0
    const/4 v2, -0x1

    :goto_1
    packed-switch v2, :pswitch_data_1

    goto :goto_2

    .line 69
    :pswitch_1
    iget-object v0, p0, Lcom/miui/server/stability/StabilityMemoryMonitor$AlarmReceiver;->this$0:Lcom/miui/server/stability/StabilityMemoryMonitor;

    invoke-static {v0}, Lcom/miui/server/stability/StabilityMemoryMonitor;->-$$Nest$fgetmScreenState(Lcom/miui/server/stability/StabilityMemoryMonitor;)Z

    move-result v0

    if-nez v0, :cond_2

    .line 70
    iget-object v0, p0, Lcom/miui/server/stability/StabilityMemoryMonitor$AlarmReceiver;->this$0:Lcom/miui/server/stability/StabilityMemoryMonitor;

    invoke-static {v0}, Lcom/miui/server/stability/StabilityMemoryMonitor;->-$$Nest$msendTask(Lcom/miui/server/stability/StabilityMemoryMonitor;)V

    goto :goto_2

    .line 72
    :cond_2
    iget-object v0, p0, Lcom/miui/server/stability/StabilityMemoryMonitor$AlarmReceiver;->this$0:Lcom/miui/server/stability/StabilityMemoryMonitor;

    const/4 v1, 0x1

    invoke-static {v0, v1}, Lcom/miui/server/stability/StabilityMemoryMonitor;->-$$Nest$fputmPendingWork(Lcom/miui/server/stability/StabilityMemoryMonitor;Z)V

    .line 74
    nop

    .line 78
    :goto_2
    return-void

    nop

    :pswitch_data_0
    .packed-switch 0x207f19a5
        :pswitch_0
    .end packed-switch

    :pswitch_data_1
    .packed-switch 0x0
        :pswitch_1
    .end packed-switch
.end method
