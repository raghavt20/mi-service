.class public Lcom/miui/server/stability/KgslProcUsageInfo;
.super Ljava/lang/Object;
.source "KgslProcUsageInfo.java"


# instance fields
.field private gfxDev:J

.field private glMtrack:J

.field private oomadj:I

.field private pid:I

.field private procName:Ljava/lang/String;

.field private rss:J


# direct methods
.method public constructor <init>()V
    .locals 0

    .line 15
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 16
    return-void
.end method


# virtual methods
.method public getGfxDev()J
    .locals 2

    .line 55
    iget-wide v0, p0, Lcom/miui/server/stability/KgslProcUsageInfo;->gfxDev:J

    return-wide v0
.end method

.method public getGlMtrack()J
    .locals 2

    .line 63
    iget-wide v0, p0, Lcom/miui/server/stability/KgslProcUsageInfo;->glMtrack:J

    return-wide v0
.end method

.method public getName()Ljava/lang/String;
    .locals 1

    .line 23
    iget-object v0, p0, Lcom/miui/server/stability/KgslProcUsageInfo;->procName:Ljava/lang/String;

    return-object v0
.end method

.method public getOomadj()I
    .locals 1

    .line 39
    iget v0, p0, Lcom/miui/server/stability/KgslProcUsageInfo;->oomadj:I

    return v0
.end method

.method public getPid()I
    .locals 1

    .line 31
    iget v0, p0, Lcom/miui/server/stability/KgslProcUsageInfo;->pid:I

    return v0
.end method

.method public getRss()J
    .locals 2

    .line 47
    iget-wide v0, p0, Lcom/miui/server/stability/KgslProcUsageInfo;->rss:J

    return-wide v0
.end method

.method public setGfxDev(J)V
    .locals 0
    .param p1, "gfxDev"    # J

    .line 51
    iput-wide p1, p0, Lcom/miui/server/stability/KgslProcUsageInfo;->gfxDev:J

    .line 52
    return-void
.end method

.method public setGlMtrack(J)V
    .locals 0
    .param p1, "glMtrack"    # J

    .line 59
    iput-wide p1, p0, Lcom/miui/server/stability/KgslProcUsageInfo;->glMtrack:J

    .line 60
    return-void
.end method

.method public setName(Ljava/lang/String;)V
    .locals 0
    .param p1, "procName"    # Ljava/lang/String;

    .line 19
    iput-object p1, p0, Lcom/miui/server/stability/KgslProcUsageInfo;->procName:Ljava/lang/String;

    .line 20
    return-void
.end method

.method public setOomadj(I)V
    .locals 0
    .param p1, "oomadj"    # I

    .line 35
    iput p1, p0, Lcom/miui/server/stability/KgslProcUsageInfo;->oomadj:I

    .line 36
    return-void
.end method

.method public setPid(I)V
    .locals 0
    .param p1, "pid"    # I

    .line 27
    iput p1, p0, Lcom/miui/server/stability/KgslProcUsageInfo;->pid:I

    .line 28
    return-void
.end method

.method public setRss(J)V
    .locals 0
    .param p1, "rss"    # J

    .line 43
    iput-wide p1, p0, Lcom/miui/server/stability/KgslProcUsageInfo;->rss:J

    .line 44
    return-void
.end method

.method public toString()Ljava/lang/String;
    .locals 4

    .line 67
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    .line 68
    .local v0, "sb":Ljava/lang/StringBuilder;
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Kgsl proc info Name: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lcom/miui/server/stability/KgslProcUsageInfo;->procName:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " Pid="

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget v2, p0, Lcom/miui/server/stability/KgslProcUsageInfo;->pid:I

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " Oomadj="

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget v2, p0, Lcom/miui/server/stability/KgslProcUsageInfo;->oomadj:I

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " Rss="

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-wide v2, p0, Lcom/miui/server/stability/KgslProcUsageInfo;->rss:J

    invoke-virtual {v1, v2, v3}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "kB GfxDev="

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-wide v2, p0, Lcom/miui/server/stability/KgslProcUsageInfo;->gfxDev:J

    invoke-virtual {v1, v2, v3}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "kB GlMtrack="

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-wide v2, p0, Lcom/miui/server/stability/KgslProcUsageInfo;->glMtrack:J

    invoke-virtual {v1, v2, v3}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "kB"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 70
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    return-object v1
.end method
