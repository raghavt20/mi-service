public class com.miui.server.stability.StabilityLocalService implements com.miui.server.stability.StabilityLocalServiceInternal {
	 /* .source "StabilityLocalService.java" */
	 /* # interfaces */
	 /* # annotations */
	 /* .annotation system Ldalvik/annotation/MemberClasses; */
	 /* value = { */
	 /* Lcom/miui/server/stability/StabilityLocalService$Lifecycle; */
	 /* } */
} // .end annotation
/* # static fields */
private static final java.lang.String TAG;
private static com.miui.server.stability.StabilityMemoryMonitor mStabilityMemoryMonitor;
/* # direct methods */
static com.miui.server.stability.StabilityLocalService ( ) {
	 /* .locals 1 */
	 /* .line 9 */
	 /* const-class v0, Lcom/miui/server/stability/StabilityLocalService; */
	 (( java.lang.Class ) v0 ).getSimpleName ( ); // invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;
	 /* .line 11 */
	 com.miui.server.stability.StabilityMemoryMonitor .getInstance ( );
	 /* .line 10 */
	 return;
} // .end method
public com.miui.server.stability.StabilityLocalService ( ) {
	 /* .locals 0 */
	 /* .line 7 */
	 /* invoke-direct {p0}, Ljava/lang/Object;-><init>()V */
	 return;
} // .end method
/* # virtual methods */
public void captureDumpLog ( ) {
	 /* .locals 0 */
	 /* .line 27 */
	 com.miui.server.stability.DumpSysInfoUtil .captureDumpLog ( );
	 /* .line 28 */
	 return;
} // .end method
public void crawlLogsByPower ( ) {
	 /* .locals 0 */
	 /* .line 32 */
	 com.miui.server.stability.DumpSysInfoUtil .crawlLogsByPower ( );
	 /* .line 33 */
	 return;
} // .end method
public void initContext ( android.content.Context p0 ) {
	 /* .locals 1 */
	 /* .param p1, "context" # Landroid/content/Context; */
	 /* .line 37 */
	 v0 = com.miui.server.stability.StabilityLocalService.mStabilityMemoryMonitor;
	 (( com.miui.server.stability.StabilityMemoryMonitor ) v0 ).initContext ( p1 ); // invoke-virtual {v0, p1}, Lcom/miui/server/stability/StabilityMemoryMonitor;->initContext(Landroid/content/Context;)V
	 /* .line 38 */
	 return;
} // .end method
public void startMemoryMonitor ( ) {
	 /* .locals 1 */
	 /* .line 42 */
	 v0 = com.miui.server.stability.StabilityLocalService.mStabilityMemoryMonitor;
	 (( com.miui.server.stability.StabilityMemoryMonitor ) v0 ).startMemoryMonitor ( ); // invoke-virtual {v0}, Lcom/miui/server/stability/StabilityMemoryMonitor;->startMemoryMonitor()V
	 /* .line 43 */
	 return;
} // .end method
