class com.miui.server.stability.ScoutDisplayMemoryManager$2 implements java.util.Comparator {
	 /* .source "ScoutDisplayMemoryManager.java" */
	 /* # interfaces */
	 /* # annotations */
	 /* .annotation system Ldalvik/annotation/EnclosingMethod; */
	 /* value = Lcom/miui/server/stability/ScoutDisplayMemoryManager;->getGpuMemoryUsageInfo()Ljava/lang/String; */
} // .end annotation
/* .annotation system Ldalvik/annotation/InnerClass; */
/* accessFlags = 0x0 */
/* name = null */
} // .end annotation
/* .annotation system Ldalvik/annotation/Signature; */
/* value = { */
/* "Ljava/lang/Object;", */
/* "Ljava/util/Comparator<", */
/* "Lcom/miui/server/stability/GpuMemoryProcUsageInfo;", */
/* ">;" */
/* } */
} // .end annotation
/* # instance fields */
final com.miui.server.stability.ScoutDisplayMemoryManager this$0; //synthetic
/* # direct methods */
 com.miui.server.stability.ScoutDisplayMemoryManager$2 ( ) {
/* .locals 0 */
/* .param p1, "this$0" # Lcom/miui/server/stability/ScoutDisplayMemoryManager; */
/* .line 205 */
this.this$0 = p1;
/* invoke-direct {p0}, Ljava/lang/Object;-><init>()V */
return;
} // .end method
/* # virtual methods */
public Integer compare ( com.miui.server.stability.GpuMemoryProcUsageInfo p0, com.miui.server.stability.GpuMemoryProcUsageInfo p1 ) {
/* .locals 4 */
/* .param p1, "o1" # Lcom/miui/server/stability/GpuMemoryProcUsageInfo; */
/* .param p2, "o2" # Lcom/miui/server/stability/GpuMemoryProcUsageInfo; */
/* .line 208 */
(( com.miui.server.stability.GpuMemoryProcUsageInfo ) p2 ).getRss ( ); // invoke-virtual {p2}, Lcom/miui/server/stability/GpuMemoryProcUsageInfo;->getRss()J
/* move-result-wide v0 */
(( com.miui.server.stability.GpuMemoryProcUsageInfo ) p1 ).getRss ( ); // invoke-virtual {p1}, Lcom/miui/server/stability/GpuMemoryProcUsageInfo;->getRss()J
/* move-result-wide v2 */
/* cmp-long v0, v0, v2 */
/* if-nez v0, :cond_0 */
/* .line 209 */
int v0 = 0; // const/4 v0, 0x0
/* .line 210 */
} // :cond_0
(( com.miui.server.stability.GpuMemoryProcUsageInfo ) p2 ).getRss ( ); // invoke-virtual {p2}, Lcom/miui/server/stability/GpuMemoryProcUsageInfo;->getRss()J
/* move-result-wide v0 */
(( com.miui.server.stability.GpuMemoryProcUsageInfo ) p1 ).getRss ( ); // invoke-virtual {p1}, Lcom/miui/server/stability/GpuMemoryProcUsageInfo;->getRss()J
/* move-result-wide v2 */
/* cmp-long v0, v0, v2 */
/* if-lez v0, :cond_1 */
/* .line 211 */
int v0 = 1; // const/4 v0, 0x1
/* .line 213 */
} // :cond_1
int v0 = -1; // const/4 v0, -0x1
} // .end method
public Integer compare ( java.lang.Object p0, java.lang.Object p1 ) { //bridge//synthethic
/* .locals 0 */
/* .line 205 */
/* check-cast p1, Lcom/miui/server/stability/GpuMemoryProcUsageInfo; */
/* check-cast p2, Lcom/miui/server/stability/GpuMemoryProcUsageInfo; */
p1 = (( com.miui.server.stability.ScoutDisplayMemoryManager$2 ) p0 ).compare ( p1, p2 ); // invoke-virtual {p0, p1, p2}, Lcom/miui/server/stability/ScoutDisplayMemoryManager$2;->compare(Lcom/miui/server/stability/GpuMemoryProcUsageInfo;Lcom/miui/server/stability/GpuMemoryProcUsageInfo;)I
} // .end method
