.class public Lcom/miui/server/stability/LockPerfImpl;
.super Ljava/lang/Object;
.source "LockPerfImpl.java"

# interfaces
.implements Lcom/android/server/LockPerfStub;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/miui/server/stability/LockPerfImpl$ThreadLocalData;,
        Lcom/miui/server/stability/LockPerfImpl$PerfStack;,
        Lcom/miui/server/stability/LockPerfImpl$PerfInfo;,
        Lcom/miui/server/stability/LockPerfImpl$ThreadSafeRingBuffer;,
        Lcom/miui/server/stability/LockPerfImpl$SlowLockedMethodEvent;
    }
.end annotation


# static fields
.field private static final DEBUG:Z

.field private static final DEFAULT_LOCK_PERF_THRESHOLD_SINGLE:I = 0x64

.field private static final DEFAULT_RING_BUFFER_SIZE:I = 0x64

.field private static final LOCK_PERF_SELF_DEBUG_PROP:Ljava/lang/String; = "persist.debug.lockperf.self"

.field private static final LOCK_PERF_THRESHOLD_PROP:Ljava/lang/String; = "persist.debug.lockperf.threshold"

.field private static final LOCK_PERF_THRESHOLD_SINGLE:I

.field private static final MEANINGFUL_TRACE_START:I = 0x4

.field private static final TAG:Ljava/lang/String;

.field private static final sEventRingBuffer:Lcom/miui/server/stability/LockPerfImpl$ThreadSafeRingBuffer;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/miui/server/stability/LockPerfImpl$ThreadSafeRingBuffer<",
            "Lcom/miui/server/stability/LockPerfImpl$SlowLockedMethodEvent;",
            ">;"
        }
    .end annotation
.end field

.field private static final sThreadLocalData:Ljava/lang/ThreadLocal;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/lang/ThreadLocal<",
            "Lcom/miui/server/stability/LockPerfImpl$ThreadLocalData;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public static synthetic $r8$lambda$ClCO5txLu8hRiftiu7srcjS1yXQ()Lcom/miui/server/stability/LockPerfImpl$ThreadLocalData;
    .locals 2

    new-instance v0, Lcom/miui/server/stability/LockPerfImpl$ThreadLocalData;

    const/4 v1, 0x0

    invoke-direct {v0, v1}, Lcom/miui/server/stability/LockPerfImpl$ThreadLocalData;-><init>(Lcom/miui/server/stability/LockPerfImpl$ThreadLocalData-IA;)V

    return-object v0
.end method

.method static bridge synthetic -$$Nest$sfgetTAG()Ljava/lang/String;
    .locals 1

    sget-object v0, Lcom/miui/server/stability/LockPerfImpl;->TAG:Ljava/lang/String;

    return-object v0
.end method

.method static bridge synthetic -$$Nest$sfgetsEventRingBuffer()Lcom/miui/server/stability/LockPerfImpl$ThreadSafeRingBuffer;
    .locals 1

    sget-object v0, Lcom/miui/server/stability/LockPerfImpl;->sEventRingBuffer:Lcom/miui/server/stability/LockPerfImpl$ThreadSafeRingBuffer;

    return-object v0
.end method

.method static bridge synthetic -$$Nest$smparseFullMethod(Ljava/lang/String;)[Ljava/lang/String;
    .locals 0

    invoke-static {p0}, Lcom/miui/server/stability/LockPerfImpl;->parseFullMethod(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object p0

    return-object p0
.end method

.method static constructor <clinit>()V
    .locals 3

    .line 22
    const-class v0, Lcom/miui/server/stability/LockPerfImpl;

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/miui/server/stability/LockPerfImpl;->TAG:Ljava/lang/String;

    .line 32
    nop

    .line 33
    const-string v0, "persist.debug.lockperf.threshold"

    const/16 v1, 0x64

    invoke-static {v0, v1}, Landroid/os/SystemProperties;->getInt(Ljava/lang/String;I)I

    move-result v0

    sput v0, Lcom/miui/server/stability/LockPerfImpl;->LOCK_PERF_THRESHOLD_SINGLE:I

    .line 34
    const-string v0, "persist.debug.lockperf.self"

    const/4 v2, 0x1

    invoke-static {v0, v2}, Landroid/os/SystemProperties;->getBoolean(Ljava/lang/String;Z)Z

    move-result v0

    sput-boolean v0, Lcom/miui/server/stability/LockPerfImpl;->DEBUG:Z

    .line 36
    new-instance v0, Lcom/miui/server/stability/LockPerfImpl$$ExternalSyntheticLambda0;

    invoke-direct {v0}, Lcom/miui/server/stability/LockPerfImpl$$ExternalSyntheticLambda0;-><init>()V

    invoke-static {v0}, Ljava/lang/ThreadLocal;->withInitial(Ljava/util/function/Supplier;)Ljava/lang/ThreadLocal;

    move-result-object v0

    sput-object v0, Lcom/miui/server/stability/LockPerfImpl;->sThreadLocalData:Ljava/lang/ThreadLocal;

    .line 38
    new-instance v0, Lcom/miui/server/stability/LockPerfImpl$ThreadSafeRingBuffer;

    const-class v2, Lcom/miui/server/stability/LockPerfImpl$SlowLockedMethodEvent;

    invoke-direct {v0, v2, v1}, Lcom/miui/server/stability/LockPerfImpl$ThreadSafeRingBuffer;-><init>(Ljava/lang/Class;I)V

    sput-object v0, Lcom/miui/server/stability/LockPerfImpl;->sEventRingBuffer:Lcom/miui/server/stability/LockPerfImpl$ThreadSafeRingBuffer;

    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .line 20
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method private static parseFullMethod(Ljava/lang/String;)[Ljava/lang/String;
    .locals 6
    .param p0, "fullMethod"    # Ljava/lang/String;

    .line 144
    const/4 v0, 0x3

    new-array v0, v0, [Ljava/lang/String;

    .line 145
    .local v0, "rst":[Ljava/lang/String;
    const/16 v1, 0x28

    invoke-virtual {p0, v1}, Ljava/lang/String;->indexOf(I)I

    move-result v1

    .line 146
    .local v1, "pos":I
    const/4 v2, 0x0

    invoke-virtual {p0, v2, v1}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v3

    .line 147
    .local v3, "classAndName":Ljava/lang/String;
    const/4 v4, 0x2

    invoke-virtual {p0, v1}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    move-result-object v5

    aput-object v5, v0, v4

    .line 148
    const/16 v4, 0x2e

    invoke-virtual {v3, v4}, Ljava/lang/String;->lastIndexOf(I)I

    move-result v1

    .line 149
    invoke-virtual {v3, v2, v1}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v4

    aput-object v4, v0, v2

    .line 150
    add-int/lit8 v2, v1, 0x1

    invoke-virtual {v3, v2}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    move-result-object v2

    const/4 v4, 0x1

    aput-object v2, v0, v4

    .line 151
    return-object v0
.end method

.method private static trimStackTrace([Ljava/lang/StackTraceElement;)[Ljava/lang/StackTraceElement;
    .locals 3
    .param p0, "rawTrace"    # [Ljava/lang/StackTraceElement;

    .line 156
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    .line 157
    .local v0, "elements":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/StackTraceElement;>;"
    const/4 v1, 0x4

    .local v1, "i":I
    :goto_0
    array-length v2, p0

    if-ge v1, v2, :cond_0

    .line 158
    aget-object v2, p0, v1

    invoke-virtual {v0, v2}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 157
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 160
    .end local v1    # "i":I
    :cond_0
    const/4 v1, 0x0

    new-array v1, v1, [Ljava/lang/StackTraceElement;

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->toArray([Ljava/lang/Object;)[Ljava/lang/Object;

    move-result-object v1

    check-cast v1, [Ljava/lang/StackTraceElement;

    return-object v1
.end method


# virtual methods
.method public dumpRecentEvents(Ljava/io/File;ZII)V
    .locals 16
    .param p1, "tracesFile"    # Ljava/io/File;
    .param p2, "append"    # Z
    .param p3, "recentSeconds"    # I
    .param p4, "maxEventNum"    # I

    .line 106
    :try_start_0
    new-instance v0, Ljava/io/FileWriter;
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_2

    move-object/from16 v1, p1

    move/from16 v2, p2

    :try_start_1
    invoke-direct {v0, v1, v2}, Ljava/io/FileWriter;-><init>(Ljava/io/File;Z)V
    :try_end_1
    .catch Ljava/io/IOException; {:try_start_1 .. :try_end_1} :catch_1

    move-object v3, v0

    .line 107
    .local v3, "fw":Ljava/io/FileWriter;
    :try_start_2
    sget-object v0, Lcom/miui/server/stability/LockPerfImpl;->sEventRingBuffer:Lcom/miui/server/stability/LockPerfImpl$ThreadSafeRingBuffer;

    invoke-virtual {v0}, Lcom/miui/server/stability/LockPerfImpl$ThreadSafeRingBuffer;->toArray()[Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/miui/server/stability/LockPerfImpl$SlowLockedMethodEvent;

    .line 108
    .local v0, "events":[Lcom/miui/server/stability/LockPerfImpl$SlowLockedMethodEvent;
    array-length v4, v0

    if-nez v4, :cond_0

    .line 109
    const-string v4, "No events in buffer. Is the rom debuggable?"

    invoke-virtual {v3, v4}, Ljava/io/FileWriter;->append(Ljava/lang/CharSequence;)Ljava/io/Writer;
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_2

    .line 138
    :try_start_3
    invoke-virtual {v3}, Ljava/io/FileWriter;->close()V
    :try_end_3
    .catch Ljava/io/IOException; {:try_start_3 .. :try_end_3} :catch_1

    .line 110
    return-void

    .line 112
    :cond_0
    :try_start_4
    new-instance v4, Ljava/util/ArrayList;

    invoke-direct {v4}, Ljava/util/ArrayList;-><init>()V

    .line 113
    .local v4, "eventsToDump":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/miui/server/stability/LockPerfImpl$SlowLockedMethodEvent;>;"
    invoke-static {}, Ljava/time/LocalDateTime;->now()Ljava/time/LocalDateTime;

    move-result-object v5
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_2

    move/from16 v6, p3

    int-to-long v7, v6

    :try_start_5
    invoke-virtual {v5, v7, v8}, Ljava/time/LocalDateTime;->minusSeconds(J)Ljava/time/LocalDateTime;

    move-result-object v5

    .line 114
    .local v5, "eventStartTime":Ljava/time/LocalDateTime;
    array-length v7, v0

    add-int/lit8 v7, v7, -0x1

    .local v7, "i":I
    :goto_0
    if-ltz v7, :cond_3

    .line 115
    aget-object v8, v0, v7

    invoke-static {v8}, Lcom/miui/server/stability/LockPerfImpl$SlowLockedMethodEvent;->-$$Nest$fgetdatetime(Lcom/miui/server/stability/LockPerfImpl$SlowLockedMethodEvent;)Ljava/time/LocalDateTime;

    move-result-object v8

    invoke-virtual {v8, v5}, Ljava/time/LocalDateTime;->isBefore(Ljava/time/chrono/ChronoLocalDateTime;)Z

    move-result v8

    if-nez v8, :cond_2

    invoke-virtual {v4}, Ljava/util/ArrayList;->size()I

    move-result v8
    :try_end_5
    .catchall {:try_start_5 .. :try_end_5} :catchall_1

    move/from16 v9, p4

    if-lt v8, v9, :cond_1

    .line 116
    goto :goto_1

    .line 118
    :cond_1
    :try_start_6
    aget-object v8, v0, v7

    invoke-virtual {v4, v8}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 114
    add-int/lit8 v7, v7, -0x1

    goto :goto_0

    .line 115
    :cond_2
    move/from16 v9, p4

    goto :goto_1

    .line 114
    :cond_3
    move/from16 v9, p4

    .line 121
    .end local v7    # "i":I
    :goto_1
    invoke-virtual {v4}, Ljava/util/ArrayList;->isEmpty()Z

    move-result v7

    if-eqz v7, :cond_4

    .line 122
    const-string v7, "No events to dump in last "

    invoke-virtual {v3, v7}, Ljava/io/FileWriter;->append(Ljava/lang/CharSequence;)Ljava/io/Writer;

    move-result-object v7

    invoke-static/range {p3 .. p3}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v7, v8}, Ljava/io/Writer;->append(Ljava/lang/CharSequence;)Ljava/io/Writer;

    move-result-object v7

    const-string v8, " seconds."

    .line 123
    invoke-virtual {v7, v8}, Ljava/io/Writer;->append(Ljava/lang/CharSequence;)Ljava/io/Writer;
    :try_end_6
    .catchall {:try_start_6 .. :try_end_6} :catchall_0

    .line 138
    :try_start_7
    invoke-virtual {v3}, Ljava/io/FileWriter;->close()V
    :try_end_7
    .catch Ljava/io/IOException; {:try_start_7 .. :try_end_7} :catch_0

    .line 124
    return-void

    .line 127
    :cond_4
    :try_start_8
    const-string v7, "--- Slow locked method ---\n"

    invoke-virtual {v3, v7}, Ljava/io/FileWriter;->append(Ljava/lang/CharSequence;)Ljava/io/Writer;

    .line 128
    const/4 v7, 0x0

    .restart local v7    # "i":I
    :goto_2
    invoke-virtual {v4}, Ljava/util/ArrayList;->size()I

    move-result v8

    if-ge v7, v8, :cond_6

    .line 129
    invoke-virtual {v4, v7}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v8

    check-cast v8, Lcom/miui/server/stability/LockPerfImpl$SlowLockedMethodEvent;

    .line 130
    .local v8, "event":Lcom/miui/server/stability/LockPerfImpl$SlowLockedMethodEvent;
    add-int/lit8 v10, v7, 0x1

    invoke-static {v10}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v10

    invoke-virtual {v3, v10}, Ljava/io/FileWriter;->append(Ljava/lang/CharSequence;)Ljava/io/Writer;

    move-result-object v10

    const-string v11, ") "

    invoke-virtual {v10, v11}, Ljava/io/Writer;->append(Ljava/lang/CharSequence;)Ljava/io/Writer;

    .line 131
    invoke-static {v8}, Lcom/miui/server/stability/LockPerfImpl$SlowLockedMethodEvent;->-$$Nest$mformattedDatetime(Lcom/miui/server/stability/LockPerfImpl$SlowLockedMethodEvent;)Ljava/lang/String;

    move-result-object v10

    invoke-virtual {v3, v10}, Ljava/io/FileWriter;->append(Ljava/lang/CharSequence;)Ljava/io/Writer;

    move-result-object v10

    const/16 v11, 0x20

    invoke-virtual {v10, v11}, Ljava/io/Writer;->append(C)Ljava/io/Writer;

    .line 132
    const-string/jumbo v10, "tid="

    invoke-virtual {v3, v10}, Ljava/io/FileWriter;->append(Ljava/lang/CharSequence;)Ljava/io/Writer;

    move-result-object v10

    invoke-static {v8}, Lcom/miui/server/stability/LockPerfImpl$SlowLockedMethodEvent;->-$$Nest$fgettid(Lcom/miui/server/stability/LockPerfImpl$SlowLockedMethodEvent;)J

    move-result-wide v12

    invoke-static {v12, v13}, Ljava/lang/String;->valueOf(J)Ljava/lang/String;

    move-result-object v12

    invoke-virtual {v10, v12}, Ljava/io/Writer;->append(Ljava/lang/CharSequence;)Ljava/io/Writer;

    move-result-object v10

    invoke-virtual {v10, v11}, Ljava/io/Writer;->append(C)Ljava/io/Writer;

    .line 133
    const-string/jumbo v10, "tname="

    invoke-virtual {v3, v10}, Ljava/io/FileWriter;->append(Ljava/lang/CharSequence;)Ljava/io/Writer;

    move-result-object v10

    invoke-static {v8}, Lcom/miui/server/stability/LockPerfImpl$SlowLockedMethodEvent;->-$$Nest$fgettname(Lcom/miui/server/stability/LockPerfImpl$SlowLockedMethodEvent;)Ljava/lang/String;

    move-result-object v11

    invoke-virtual {v10, v11}, Ljava/io/Writer;->append(Ljava/lang/CharSequence;)Ljava/io/Writer;

    move-result-object v10

    const-string v11, "\n"

    invoke-virtual {v10, v11}, Ljava/io/Writer;->append(Ljava/lang/CharSequence;)Ljava/io/Writer;

    .line 134
    invoke-static {v8}, Lcom/miui/server/stability/LockPerfImpl$SlowLockedMethodEvent;->-$$Nest$mgetStackTraceLines(Lcom/miui/server/stability/LockPerfImpl$SlowLockedMethodEvent;)[Ljava/lang/String;

    move-result-object v10

    array-length v11, v10

    const/4 v12, 0x0

    :goto_3
    if-ge v12, v11, :cond_5

    aget-object v13, v10, v12

    .line 135
    .local v13, "line":Ljava/lang/String;
    const-string v14, "\t "

    invoke-virtual {v3, v14}, Ljava/io/FileWriter;->append(Ljava/lang/CharSequence;)Ljava/io/Writer;

    move-result-object v14

    invoke-virtual {v14, v13}, Ljava/io/Writer;->append(Ljava/lang/CharSequence;)Ljava/io/Writer;

    move-result-object v14

    const/16 v15, 0xa

    invoke-virtual {v14, v15}, Ljava/io/Writer;->append(C)Ljava/io/Writer;
    :try_end_8
    .catchall {:try_start_8 .. :try_end_8} :catchall_0

    .line 134
    nop

    .end local v13    # "line":Ljava/lang/String;
    add-int/lit8 v12, v12, 0x1

    goto :goto_3

    .line 128
    .end local v8    # "event":Lcom/miui/server/stability/LockPerfImpl$SlowLockedMethodEvent;
    :cond_5
    add-int/lit8 v7, v7, 0x1

    goto :goto_2

    .line 138
    .end local v0    # "events":[Lcom/miui/server/stability/LockPerfImpl$SlowLockedMethodEvent;
    .end local v4    # "eventsToDump":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/miui/server/stability/LockPerfImpl$SlowLockedMethodEvent;>;"
    .end local v5    # "eventStartTime":Ljava/time/LocalDateTime;
    .end local v7    # "i":I
    :cond_6
    :try_start_9
    invoke-virtual {v3}, Ljava/io/FileWriter;->close()V
    :try_end_9
    .catch Ljava/io/IOException; {:try_start_9 .. :try_end_9} :catch_0

    .line 140
    .end local v3    # "fw":Ljava/io/FileWriter;
    goto :goto_9

    .line 106
    .restart local v3    # "fw":Ljava/io/FileWriter;
    :catchall_0
    move-exception v0

    goto :goto_5

    :catchall_1
    move-exception v0

    goto :goto_4

    :catchall_2
    move-exception v0

    move/from16 v6, p3

    :goto_4
    move/from16 v9, p4

    :goto_5
    move-object v4, v0

    :try_start_a
    invoke-virtual {v3}, Ljava/io/FileWriter;->close()V
    :try_end_a
    .catchall {:try_start_a .. :try_end_a} :catchall_3

    goto :goto_6

    :catchall_3
    move-exception v0

    move-object v5, v0

    :try_start_b
    invoke-virtual {v4, v5}, Ljava/lang/Throwable;->addSuppressed(Ljava/lang/Throwable;)V

    .end local p0    # "this":Lcom/miui/server/stability/LockPerfImpl;
    .end local p1    # "tracesFile":Ljava/io/File;
    .end local p2    # "append":Z
    .end local p3    # "recentSeconds":I
    .end local p4    # "maxEventNum":I
    :goto_6
    throw v4
    :try_end_b
    .catch Ljava/io/IOException; {:try_start_b .. :try_end_b} :catch_0

    .line 138
    .end local v3    # "fw":Ljava/io/FileWriter;
    .restart local p0    # "this":Lcom/miui/server/stability/LockPerfImpl;
    .restart local p1    # "tracesFile":Ljava/io/File;
    .restart local p2    # "append":Z
    .restart local p3    # "recentSeconds":I
    .restart local p4    # "maxEventNum":I
    :catch_0
    move-exception v0

    goto :goto_8

    :catch_1
    move-exception v0

    goto :goto_7

    :catch_2
    move-exception v0

    move-object/from16 v1, p1

    move/from16 v2, p2

    :goto_7
    move/from16 v6, p3

    move/from16 v9, p4

    .line 139
    .local v0, "e":Ljava/io/IOException;
    :goto_8
    sget-object v3, Lcom/miui/server/stability/LockPerfImpl;->TAG:Ljava/lang/String;

    const-string v4, "Dumping slow locked events failed: "

    invoke-static {v3, v4, v0}, Landroid/util/Slog;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    .line 141
    .end local v0    # "e":Ljava/io/IOException;
    :goto_9
    return-void
.end method

.method public onPerfEnd(Ljava/lang/String;)V
    .locals 8
    .param p1, "fullMethod"    # Ljava/lang/String;

    .line 64
    sget-object v0, Lcom/miui/server/stability/LockPerfImpl;->sThreadLocalData:Ljava/lang/ThreadLocal;

    invoke-virtual {v0}, Ljava/lang/ThreadLocal;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/miui/server/stability/LockPerfImpl$ThreadLocalData;

    .line 65
    .local v0, "tls":Lcom/miui/server/stability/LockPerfImpl$ThreadLocalData;
    sget-boolean v1, Lcom/miui/server/stability/LockPerfImpl;->DEBUG:Z

    if-eqz v1, :cond_0

    invoke-static {v0}, Lcom/miui/server/stability/LockPerfImpl$ThreadLocalData;->-$$Nest$fgetdebugEventBuffer(Lcom/miui/server/stability/LockPerfImpl$ThreadLocalData;)Lcom/android/internal/util/RingBuffer;

    move-result-object v2

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "-"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Lcom/android/internal/util/RingBuffer;->append(Ljava/lang/Object;)V

    .line 66
    :cond_0
    invoke-static {v0}, Lcom/miui/server/stability/LockPerfImpl$ThreadLocalData;->-$$Nest$fgetperfStack(Lcom/miui/server/stability/LockPerfImpl$ThreadLocalData;)Lcom/miui/server/stability/LockPerfImpl$PerfStack;

    move-result-object v2

    .line 67
    .local v2, "perfStack":Lcom/miui/server/stability/LockPerfImpl$PerfStack;, "Lcom/miui/server/stability/LockPerfImpl$PerfStack<Lcom/miui/server/stability/LockPerfImpl$PerfInfo;>;"
    invoke-virtual {v2}, Lcom/miui/server/stability/LockPerfImpl$PerfStack;->topElement()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/miui/server/stability/LockPerfImpl$PerfInfo;

    .line 68
    .local v3, "perfInfo":Lcom/miui/server/stability/LockPerfImpl$PerfInfo;
    if-eqz v1, :cond_2

    invoke-static {v3, p1}, Lcom/miui/server/stability/LockPerfImpl$PerfInfo;->-$$Nest$mmatchMethod(Lcom/miui/server/stability/LockPerfImpl$PerfInfo;Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_1

    goto :goto_0

    .line 69
    :cond_1
    sget-object v1, Lcom/miui/server/stability/LockPerfImpl;->TAG:Ljava/lang/String;

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "Not matched perf start and end! Expected: "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-static {v3}, Lcom/miui/server/stability/LockPerfImpl$PerfInfo;->-$$Nest$mgetMethodString(Lcom/miui/server/stability/LockPerfImpl$PerfInfo;)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, ". Actual: "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v1, v4}, Landroid/util/Slog;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 71
    invoke-virtual {v0}, Lcom/miui/server/stability/LockPerfImpl$ThreadLocalData;->outputRecentEvents()V

    .line 72
    new-instance v1, Ljava/lang/RuntimeException;

    const-string v4, "Lock perf error! Please check log."

    invoke-direct {v1, v4}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    throw v1

    .line 75
    :cond_2
    :goto_0
    invoke-static {}, Landroid/os/SystemClock;->elapsedRealtime()J

    move-result-wide v4

    invoke-static {v3}, Lcom/miui/server/stability/LockPerfImpl$PerfInfo;->-$$Nest$fgetstart(Lcom/miui/server/stability/LockPerfImpl$PerfInfo;)J

    move-result-wide v6

    sub-long/2addr v4, v6

    .line 76
    .local v4, "duration":J
    invoke-static {v3, v4, v5}, Lcom/miui/server/stability/LockPerfImpl$PerfInfo;->-$$Nest$msetDuration(Lcom/miui/server/stability/LockPerfImpl$PerfInfo;J)V

    .line 77
    sget v1, Lcom/miui/server/stability/LockPerfImpl;->LOCK_PERF_THRESHOLD_SINGLE:I

    int-to-long v6, v1

    cmp-long v1, v4, v6

    if-lez v1, :cond_3

    .line 78
    sget-object v1, Lcom/miui/server/stability/LockPerfImpl;->TAG:Ljava/lang/String;

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string v7, "Single long locked method. "

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6, v4, v5}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v6

    const-string v7, " ms. method: "

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-static {v1, v6}, Landroid/util/Slog;->w(Ljava/lang/String;Ljava/lang/String;)I

    .line 80
    invoke-static {v0}, Lcom/miui/server/stability/LockPerfImpl$ThreadLocalData;->-$$Nest$fgetunwindingStack(Lcom/miui/server/stability/LockPerfImpl$ThreadLocalData;)[Ljava/lang/StackTraceElement;

    move-result-object v1

    if-nez v1, :cond_3

    .line 81
    invoke-static {}, Ljava/lang/Thread;->currentThread()Ljava/lang/Thread;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Thread;->getStackTrace()[Ljava/lang/StackTraceElement;

    move-result-object v1

    invoke-static {v1}, Lcom/miui/server/stability/LockPerfImpl;->trimStackTrace([Ljava/lang/StackTraceElement;)[Ljava/lang/StackTraceElement;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/miui/server/stability/LockPerfImpl$ThreadLocalData;->-$$Nest$msetUnwindingStack(Lcom/miui/server/stability/LockPerfImpl$ThreadLocalData;[Ljava/lang/StackTraceElement;)V

    .line 83
    invoke-static {v2}, Lcom/miui/server/stability/LockPerfImpl$PerfStack;->-$$Nest$mclearBuffer(Lcom/miui/server/stability/LockPerfImpl$PerfStack;)V

    .line 87
    :cond_3
    invoke-virtual {v2}, Lcom/miui/server/stability/LockPerfImpl$PerfStack;->pop()Ljava/lang/Object;

    .line 88
    invoke-static {v0}, Lcom/miui/server/stability/LockPerfImpl$ThreadLocalData;->-$$Nest$fgetunwindingStack(Lcom/miui/server/stability/LockPerfImpl$ThreadLocalData;)[Ljava/lang/StackTraceElement;

    move-result-object v1

    if-eqz v1, :cond_4

    invoke-virtual {v2}, Lcom/miui/server/stability/LockPerfImpl$PerfStack;->isEmpty()Z

    move-result v1

    if-eqz v1, :cond_4

    .line 89
    sget-object v1, Lcom/miui/server/stability/LockPerfImpl;->TAG:Ljava/lang/String;

    const-string v6, "Record long locked event point 1"

    invoke-static {v1, v6}, Landroid/util/Slog;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 90
    invoke-static {v0}, Lcom/miui/server/stability/LockPerfImpl$ThreadLocalData;->-$$Nest$mrecordEvent(Lcom/miui/server/stability/LockPerfImpl$ThreadLocalData;)V

    .line 92
    :cond_4
    return-void
.end method

.method public onPerfStart(Ljava/lang/String;)V
    .locals 5
    .param p1, "fullMethod"    # Ljava/lang/String;

    .line 49
    sget-object v0, Lcom/miui/server/stability/LockPerfImpl;->sThreadLocalData:Ljava/lang/ThreadLocal;

    invoke-virtual {v0}, Ljava/lang/ThreadLocal;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/miui/server/stability/LockPerfImpl$ThreadLocalData;

    .line 50
    .local v0, "tls":Lcom/miui/server/stability/LockPerfImpl$ThreadLocalData;
    sget-boolean v1, Lcom/miui/server/stability/LockPerfImpl;->DEBUG:Z

    if-eqz v1, :cond_0

    invoke-static {v0}, Lcom/miui/server/stability/LockPerfImpl$ThreadLocalData;->-$$Nest$fgetdebugEventBuffer(Lcom/miui/server/stability/LockPerfImpl$ThreadLocalData;)Lcom/android/internal/util/RingBuffer;

    move-result-object v1

    invoke-virtual {v1, p1}, Lcom/android/internal/util/RingBuffer;->append(Ljava/lang/Object;)V

    .line 51
    :cond_0
    invoke-static {v0}, Lcom/miui/server/stability/LockPerfImpl$ThreadLocalData;->-$$Nest$fgetunwindingStack(Lcom/miui/server/stability/LockPerfImpl$ThreadLocalData;)[Ljava/lang/StackTraceElement;

    move-result-object v1

    if-eqz v1, :cond_1

    .line 52
    sget-object v1, Lcom/miui/server/stability/LockPerfImpl;->TAG:Ljava/lang/String;

    const-string v2, "Record long locked event point 2"

    invoke-static {v1, v2}, Landroid/util/Slog;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 53
    invoke-static {v0}, Lcom/miui/server/stability/LockPerfImpl$ThreadLocalData;->-$$Nest$mrecordEvent(Lcom/miui/server/stability/LockPerfImpl$ThreadLocalData;)V

    .line 55
    :cond_1
    invoke-static {v0}, Lcom/miui/server/stability/LockPerfImpl$ThreadLocalData;->-$$Nest$fgetperfStack(Lcom/miui/server/stability/LockPerfImpl$ThreadLocalData;)Lcom/miui/server/stability/LockPerfImpl$PerfStack;

    move-result-object v1

    new-instance v2, Lcom/miui/server/stability/LockPerfImpl$PerfInfo;

    invoke-static {}, Landroid/os/SystemClock;->elapsedRealtime()J

    move-result-wide v3

    invoke-direct {v2, p1, v3, v4}, Lcom/miui/server/stability/LockPerfImpl$PerfInfo;-><init>(Ljava/lang/String;J)V

    invoke-virtual {v1, v2}, Lcom/miui/server/stability/LockPerfImpl$PerfStack;->push(Ljava/lang/Object;)Z

    .line 56
    return-void
.end method

.method public perf(JLjava/lang/String;)V
    .locals 5
    .param p1, "start"    # J
    .param p3, "fullMethod"    # Ljava/lang/String;

    .line 97
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v0

    sub-long/2addr v0, p1

    .line 98
    .local v0, "duration":J
    sget v2, Lcom/miui/server/stability/LockPerfImpl;->LOCK_PERF_THRESHOLD_SINGLE:I

    int-to-long v2, v2

    cmp-long v2, v0, v2

    if-lez v2, :cond_0

    .line 99
    sget-object v2, Lcom/miui/server/stability/LockPerfImpl;->TAG:Ljava/lang/String;

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "Simple long locked method: "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, p3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, ", Duration: "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v0, v1}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Landroid/util/Slog;->w(Ljava/lang/String;Ljava/lang/String;)I

    .line 101
    :cond_0
    return-void
.end method
