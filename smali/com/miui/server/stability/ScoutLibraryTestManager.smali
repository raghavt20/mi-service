.class public Lcom/miui/server/stability/ScoutLibraryTestManager;
.super Ljava/lang/Object;
.source "ScoutLibraryTestManager.java"


# static fields
.field public static final PROPERTIES_APP_MTBF_TEST:Ljava/lang/String; = "persist.sys.debug.app.mtbf_test"

.field private static final TAG:Ljava/lang/String; = "ScoutLibraryTestManager"

.field private static debug:Z

.field private static scoutLibraryTestManager:Lcom/miui/server/stability/ScoutLibraryTestManager;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .line 30
    sget-boolean v0, Lcom/android/server/ScoutHelper;->ENABLED_SCOUT_DEBUG:Z

    sput-boolean v0, Lcom/miui/server/stability/ScoutLibraryTestManager;->debug:Z

    return-void
.end method

.method private constructor <init>()V
    .locals 0

    .line 43
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 44
    return-void
.end method

.method private native enableCoreDump()V
.end method

.method public static getInstance()Lcom/miui/server/stability/ScoutLibraryTestManager;
    .locals 1

    .line 37
    sget-object v0, Lcom/miui/server/stability/ScoutLibraryTestManager;->scoutLibraryTestManager:Lcom/miui/server/stability/ScoutLibraryTestManager;

    if-nez v0, :cond_0

    .line 38
    new-instance v0, Lcom/miui/server/stability/ScoutLibraryTestManager;

    invoke-direct {v0}, Lcom/miui/server/stability/ScoutLibraryTestManager;-><init>()V

    sput-object v0, Lcom/miui/server/stability/ScoutLibraryTestManager;->scoutLibraryTestManager:Lcom/miui/server/stability/ScoutLibraryTestManager;

    .line 40
    :cond_0
    sget-object v0, Lcom/miui/server/stability/ScoutLibraryTestManager;->scoutLibraryTestManager:Lcom/miui/server/stability/ScoutLibraryTestManager;

    return-object v0
.end method


# virtual methods
.method public init()V
    .locals 2

    .line 47
    sget-boolean v0, Lmiui/mqsas/scout/ScoutUtils;->REBOOT_COREDUMP:Z

    if-eqz v0, :cond_0

    .line 48
    invoke-direct {p0}, Lcom/miui/server/stability/ScoutLibraryTestManager;->enableCoreDump()V

    .line 49
    const-string v0, "ScoutLibraryTestManager"

    const-string v1, "enable system_server CoreDump"

    invoke-static {v0, v1}, Landroid/util/Slog;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 52
    :cond_0
    const-string v0, "persist.sys.debug.app.mtbf_test"

    const-string/jumbo v1, "true"

    invoke-static {v0, v1}, Landroid/os/SystemProperties;->set(Ljava/lang/String;Ljava/lang/String;)V

    .line 53
    return-void
.end method
