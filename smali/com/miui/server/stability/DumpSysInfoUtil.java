public class com.miui.server.stability.DumpSysInfoUtil {
	 /* .source "DumpSysInfoUtil.java" */
	 /* # static fields */
	 public static final java.lang.String ACTIVITY;
	 public static final java.lang.String DIR_NAME;
	 public static final java.lang.String DUMPSYS;
	 public static final java.lang.String DUMPSYS_FILE_NAME;
	 public static final java.lang.String FILE_DIR_HANGLOG;
	 public static final java.lang.String FILE_DIR_STABILITY;
	 public static final java.lang.String INPUT;
	 public static final java.lang.String LOGCAT;
	 public static final java.lang.String LOGCAT_FILE_NAME;
	 private static final Integer MAX_FREEZE_SCREEN_STUCK_FILE;
	 private static final java.lang.String MQSASD;
	 public static final java.lang.String SURFACEFLINGER;
	 private static final java.lang.String TAG;
	 public static final java.lang.String WINDOW;
	 public static final java.lang.String ZIP_NAME;
	 private static miui.mqsas.IMQSNative mDaemon;
	 private static java.io.File temporaryDir;
	 /* # direct methods */
	 static java.io.File -$$Nest$sfgettemporaryDir ( ) { //bridge//synthethic
		 /* .locals 1 */
		 v0 = com.miui.server.stability.DumpSysInfoUtil.temporaryDir;
	 } // .end method
	 static java.lang.String -$$Nest$smcreateFile ( java.lang.String p0 ) { //bridge//synthethic
		 /* .locals 0 */
		 com.miui.server.stability.DumpSysInfoUtil .createFile ( p0 );
	 } // .end method
	 static void -$$Nest$smdeleteMissFetchByPower ( java.io.File p0 ) { //bridge//synthethic
		 /* .locals 0 */
		 com.miui.server.stability.DumpSysInfoUtil .deleteMissFetchByPower ( p0 );
		 return;
	 } // .end method
	 static miui.mqsas.IMQSNative -$$Nest$smgetmDaemon ( ) { //bridge//synthethic
		 /* .locals 1 */
		 com.miui.server.stability.DumpSysInfoUtil .getmDaemon ( );
	 } // .end method
	 public com.miui.server.stability.DumpSysInfoUtil ( ) {
		 /* .locals 0 */
		 /* .line 18 */
		 /* invoke-direct {p0}, Ljava/lang/Object;-><init>()V */
		 return;
	 } // .end method
	 public static void captureDumpLog ( ) {
		 /* .locals 2 */
		 /* .line 57 */
		 com.android.server.MiuiBgThread .getHandler ( );
		 /* new-instance v1, Lcom/miui/server/stability/DumpSysInfoUtil$$ExternalSyntheticLambda0; */
		 /* invoke-direct {v1}, Lcom/miui/server/stability/DumpSysInfoUtil$$ExternalSyntheticLambda0;-><init>()V */
		 (( android.os.Handler ) v0 ).post ( v1 ); // invoke-virtual {v0, v1}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z
		 /* .line 76 */
		 return;
	 } // .end method
	 private static void crawlLogcat ( ) {
		 /* .locals 2 */
		 /* .line 122 */
		 /* new-instance v0, Ljava/lang/Thread; */
		 /* new-instance v1, Lcom/miui/server/stability/DumpSysInfoUtil$2; */
		 /* invoke-direct {v1}, Lcom/miui/server/stability/DumpSysInfoUtil$2;-><init>()V */
		 /* invoke-direct {v0, v1}, Ljava/lang/Thread;-><init>(Ljava/lang/Runnable;)V */
		 /* .line 139 */
		 /* .local v0, "crawlLocat":Ljava/lang/Thread; */
		 (( java.lang.Thread ) v0 ).start ( ); // invoke-virtual {v0}, Ljava/lang/Thread;->start()V
		 /* .line 140 */
		 return;
	 } // .end method
	 public static void crawlLogsByPower ( ) {
		 /* .locals 2 */
		 /* .line 86 */
		 final String v0 = "dumpsys_by_power_"; // const-string v0, "dumpsys_by_power_"
		 com.miui.server.stability.DumpSysInfoUtil .deleteDumpSysFile ( v0 );
		 /* .line 92 */
		 /* new-instance v0, Ljava/lang/Thread; */
		 /* new-instance v1, Lcom/miui/server/stability/DumpSysInfoUtil$1; */
		 /* invoke-direct {v1}, Lcom/miui/server/stability/DumpSysInfoUtil$1;-><init>()V */
		 /* invoke-direct {v0, v1}, Ljava/lang/Thread;-><init>(Ljava/lang/Runnable;)V */
		 /* .line 111 */
		 /* .local v0, "crawlDumpsysInfo":Ljava/lang/Thread; */
		 (( java.lang.Thread ) v0 ).start ( ); // invoke-virtual {v0}, Ljava/lang/Thread;->start()V
		 /* .line 112 */
		 return;
	 } // .end method
	 private static synchronized java.io.File createDumpsysByPowerPath ( ) {
		 /* .locals 7 */
		 /* const-class v0, Lcom/miui/server/stability/DumpSysInfoUtil; */
		 /* monitor-enter v0 */
		 /* .line 213 */
		 int v1 = 0; // const/4 v1, 0x0
		 try { // :try_start_0
			 com.miui.server.stability.DumpSysInfoUtil .getDumpSysFilePath ( );
			 /* .line 214 */
			 /* .local v2, "file":Ljava/io/File; */
			 /* new-instance v3, Ljava/text/SimpleDateFormat; */
			 /* const-string/jumbo v4, "yyyy-MM-dd-HH-mm-ss" */
			 /* invoke-direct {v3, v4}, Ljava/text/SimpleDateFormat;-><init>(Ljava/lang/String;)V */
			 /* .line 215 */
			 /* .local v3, "dateFormat":Ljava/text/SimpleDateFormat; */
			 /* new-instance v4, Ljava/io/File; */
			 /* new-instance v5, Ljava/lang/StringBuilder; */
			 /* invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V */
			 final String v6 = "dumpsys_by_power_"; // const-string v6, "dumpsys_by_power_"
			 (( java.lang.StringBuilder ) v5 ).append ( v6 ); // invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
			 /* new-instance v6, Ljava/util/Date; */
			 /* invoke-direct {v6}, Ljava/util/Date;-><init>()V */
			 (( java.text.SimpleDateFormat ) v3 ).format ( v6 ); // invoke-virtual {v3, v6}, Ljava/text/SimpleDateFormat;->format(Ljava/util/Date;)Ljava/lang/String;
			 (( java.lang.StringBuilder ) v5 ).append ( v6 ); // invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
			 (( java.lang.StringBuilder ) v5 ).toString ( ); // invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
			 /* invoke-direct {v4, v2, v5}, Ljava/io/File;-><init>(Ljava/io/File;Ljava/lang/String;)V */
			 /* .line 217 */
			 /* .local v4, "dumpsysPath":Ljava/io/File; */
			 if ( v2 != null) { // if-eqz v2, :cond_1
				 v5 = 				 (( java.io.File ) v4 ).exists ( ); // invoke-virtual {v4}, Ljava/io/File;->exists()Z
				 /* if-nez v5, :cond_0 */
				 v5 = 				 (( java.io.File ) v4 ).mkdirs ( ); // invoke-virtual {v4}, Ljava/io/File;->mkdirs()Z
				 /* if-nez v5, :cond_0 */
				 /* .line 223 */
			 } // :cond_0
			 final String v5 = "DumpSysInfoUtil"; // const-string v5, "DumpSysInfoUtil"
			 final String v6 = "mkdir dumpsysPath dir"; // const-string v6, "mkdir dumpsysPath dir"
			 android.util.Slog .d ( v5,v6 );
			 /* .line 224 */
			 /* const/16 v5, 0x1fc */
			 int v6 = -1; // const/4 v6, -0x1
			 android.os.FileUtils .setPermissions ( v4,v5,v6,v6 );
			 /* .line 225 */
			 /* :try_end_0 */
			 /* .catch Ljava/lang/Exception; {:try_start_0 ..:try_end_0} :catch_0 */
			 /* .catchall {:try_start_0 ..:try_end_0} :catchall_0 */
			 /* .line 226 */
			 /* monitor-exit v0 */
			 /* .line 218 */
		 } // :cond_1
	 } // :goto_0
	 try { // :try_start_1
		 final String v5 = "DumpSysInfoUtil"; // const-string v5, "DumpSysInfoUtil"
		 final String v6 = "Cannot create dumpsysPath dir"; // const-string v6, "Cannot create dumpsysPath dir"
		 android.util.Slog .e ( v5,v6 );
		 /* .line 219 */
		 /* :try_end_1 */
		 /* .catch Ljava/lang/Exception; {:try_start_1 ..:try_end_1} :catch_0 */
		 /* .catchall {:try_start_1 ..:try_end_1} :catchall_0 */
		 /* .line 220 */
		 /* monitor-exit v0 */
		 /* .line 212 */
	 } // .end local v2 # "file":Ljava/io/File;
} // .end local v3 # "dateFormat":Ljava/text/SimpleDateFormat;
} // .end local v4 # "dumpsysPath":Ljava/io/File;
/* :catchall_0 */
/* move-exception v1 */
/* .line 227 */
/* :catch_0 */
/* move-exception v2 */
/* .line 228 */
/* .local v2, "e":Ljava/lang/Exception; */
try { // :try_start_2
final String v3 = "DumpSysInfoUtil"; // const-string v3, "DumpSysInfoUtil"
final String v4 = "crash in the createDumpsysByPowerPath()"; // const-string v4, "crash in the createDumpsysByPowerPath()"
android.util.Slog .e ( v3,v4,v2 );
/* :try_end_2 */
/* .catchall {:try_start_2 ..:try_end_2} :catchall_0 */
/* .line 229 */
/* monitor-exit v0 */
/* .line 212 */
} // .end local v2 # "e":Ljava/lang/Exception;
} // :goto_1
/* monitor-exit v0 */
/* throw v1 */
} // .end method
private static java.lang.String createFile ( java.lang.String p0 ) {
/* .locals 6 */
/* .param p0, "mFileName" # Ljava/lang/String; */
/* .line 235 */
final String v0 = "DumpSysInfoUtil"; // const-string v0, "DumpSysInfoUtil"
int v1 = 0; // const/4 v1, 0x0
try { // :try_start_0
com.miui.server.stability.DumpSysInfoUtil .createDumpsysByPowerPath ( );
/* .line 236 */
/* .local v2, "dumpsysLogPath":Ljava/io/File; */
if ( v2 != null) { // if-eqz v2, :cond_0
/* .line 237 */
/* new-instance v3, Ljava/io/File; */
/* new-instance v4, Ljava/lang/StringBuilder; */
/* invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V */
(( java.io.File ) v2 ).getAbsolutePath ( ); // invoke-virtual {v2}, Ljava/io/File;->getAbsolutePath()Ljava/lang/String;
(( java.lang.StringBuilder ) v4 ).append ( v5 ); // invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
final String v5 = "/"; // const-string v5, "/"
(( java.lang.StringBuilder ) v4 ).append ( v5 ); // invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v4 ).append ( p0 ); // invoke-virtual {v4, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v4 ).toString ( ); // invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
/* invoke-direct {v3, v4}, Ljava/io/File;-><init>(Ljava/lang/String;)V */
/* .line 238 */
/* .local v3, "file":Ljava/io/File; */
v4 = (( java.io.File ) v3 ).createNewFile ( ); // invoke-virtual {v3}, Ljava/io/File;->createNewFile()Z
if ( v4 != null) { // if-eqz v4, :cond_0
	 /* .line 239 */
	 /* const/16 v4, 0x1fc */
	 int v5 = -1; // const/4 v5, -0x1
	 android.os.FileUtils .setPermissions ( v3,v4,v5,v5 );
	 /* .line 240 */
	 final String v4 = "create file success"; // const-string v4, "create file success"
	 android.util.Slog .d ( v0,v4 );
	 /* .line 241 */
	 (( java.io.File ) v3 ).getAbsolutePath ( ); // invoke-virtual {v3}, Ljava/io/File;->getAbsolutePath()Ljava/lang/String;
	 /* .line 244 */
} // .end local v3 # "file":Ljava/io/File;
} // :cond_0
final String v3 = "create file fail"; // const-string v3, "create file fail"
android.util.Slog .e ( v0,v3 );
/* :try_end_0 */
/* .catch Ljava/lang/Exception; {:try_start_0 ..:try_end_0} :catch_0 */
/* .line 245 */
/* .line 246 */
} // .end local v2 # "dumpsysLogPath":Ljava/io/File;
/* :catch_0 */
/* move-exception v2 */
/* .line 247 */
/* .local v2, "e":Ljava/lang/Exception; */
final String v3 = "crash in the createFile()"; // const-string v3, "crash in the createFile()"
android.util.Slog .e ( v0,v3,v2 );
/* .line 248 */
} // .end method
private static void deleteDumpSysFile ( java.lang.String p0 ) {
/* .locals 8 */
/* .param p0, "fileName" # Ljava/lang/String; */
/* .line 165 */
try { // :try_start_0
com.miui.server.stability.DumpSysInfoUtil .getDumpSysFilePath ( );
/* .line 166 */
/* .local v0, "hanglog":Ljava/io/File; */
/* new-instance v1, Ljava/util/TreeSet; */
/* invoke-direct {v1}, Ljava/util/TreeSet;-><init>()V */
/* .line 168 */
/* .local v1, "existinglog":Ljava/util/TreeSet;, "Ljava/util/TreeSet<Ljava/io/File;>;" */
if ( v0 != null) { // if-eqz v0, :cond_3
v2 = (( java.io.File ) v0 ).exists ( ); // invoke-virtual {v0}, Ljava/io/File;->exists()Z
if ( v2 != null) { // if-eqz v2, :cond_3
	 /* .line 169 */
	 (( java.io.File ) v0 ).listFiles ( ); // invoke-virtual {v0}, Ljava/io/File;->listFiles()[Ljava/io/File;
	 /* array-length v3, v2 */
	 int v4 = 0; // const/4 v4, 0x0
	 /* move v5, v4 */
} // :goto_0
/* if-ge v5, v3, :cond_1 */
/* aget-object v6, v2, v5 */
/* .line 170 */
/* .local v6, "file":Ljava/io/File; */
(( java.io.File ) v6 ).getName ( ); // invoke-virtual {v6}, Ljava/io/File;->getName()Ljava/lang/String;
v7 = (( java.lang.String ) v7 ).contains ( p0 ); // invoke-virtual {v7, p0}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z
if ( v7 != null) { // if-eqz v7, :cond_0
	 /* .line 171 */
	 (( java.util.TreeSet ) v1 ).add ( v6 ); // invoke-virtual {v1, v6}, Ljava/util/TreeSet;->add(Ljava/lang/Object;)Z
	 /* .line 169 */
} // .end local v6 # "file":Ljava/io/File;
} // :cond_0
/* add-int/lit8 v5, v5, 0x1 */
/* .line 174 */
} // :cond_1
v2 = (( java.util.TreeSet ) v1 ).size ( ); // invoke-virtual {v1}, Ljava/util/TreeSet;->size()I
int v3 = 3; // const/4 v3, 0x3
/* if-lt v2, v3, :cond_3 */
/* .line 175 */
(( java.util.TreeSet ) v1 ).pollFirst ( ); // invoke-virtual {v1}, Ljava/util/TreeSet;->pollFirst()Ljava/lang/Object;
/* check-cast v2, Ljava/io/File; */
/* .line 176 */
/* .local v2, "deleteFile":Ljava/io/File; */
(( java.io.File ) v2 ).getName ( ); // invoke-virtual {v2}, Ljava/io/File;->getName()Ljava/lang/String;
/* const-string/jumbo v5, "zip" */
v3 = (( java.lang.String ) v3 ).contains ( v5 ); // invoke-virtual {v3, v5}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z
/* if-nez v3, :cond_2 */
/* .line 177 */
(( java.io.File ) v2 ).listFiles ( ); // invoke-virtual {v2}, Ljava/io/File;->listFiles()[Ljava/io/File;
/* array-length v5, v3 */
} // :goto_1
/* if-ge v4, v5, :cond_2 */
/* aget-object v6, v3, v4 */
/* .line 178 */
/* .restart local v6 # "file":Ljava/io/File; */
(( java.io.File ) v6 ).delete ( ); // invoke-virtual {v6}, Ljava/io/File;->delete()Z
/* .line 177 */
/* nop */
} // .end local v6 # "file":Ljava/io/File;
/* add-int/lit8 v4, v4, 0x1 */
/* .line 181 */
} // :cond_2
(( java.io.File ) v2 ).delete ( ); // invoke-virtual {v2}, Ljava/io/File;->delete()Z
/* :try_end_0 */
/* .catch Ljava/lang/Exception; {:try_start_0 ..:try_end_0} :catch_0 */
/* .line 186 */
} // .end local v0 # "hanglog":Ljava/io/File;
} // .end local v1 # "existinglog":Ljava/util/TreeSet;, "Ljava/util/TreeSet<Ljava/io/File;>;"
} // .end local v2 # "deleteFile":Ljava/io/File;
} // :cond_3
/* .line 184 */
/* :catch_0 */
/* move-exception v0 */
/* .line 185 */
/* .local v0, "e":Ljava/lang/Exception; */
final String v1 = "DumpSysInfoUtil"; // const-string v1, "DumpSysInfoUtil"
final String v2 = "crash in the deleteDumpSysFile()"; // const-string v2, "crash in the deleteDumpSysFile()"
android.util.Slog .e ( v1,v2,v0 );
/* .line 187 */
} // .end local v0 # "e":Ljava/lang/Exception;
} // :goto_2
return;
} // .end method
private static void deleteMissFetchByPower ( java.io.File p0 ) {
/* .locals 4 */
/* .param p0, "filePath" # Ljava/io/File; */
/* .line 149 */
com.android.server.MiuiBgThread .getHandler ( );
/* new-instance v1, Lcom/miui/server/stability/DumpSysInfoUtil$$ExternalSyntheticLambda1; */
/* invoke-direct {v1, p0}, Lcom/miui/server/stability/DumpSysInfoUtil$$ExternalSyntheticLambda1;-><init>(Ljava/io/File;)V */
/* const-wide/16 v2, 0x2710 */
(( android.os.Handler ) v0 ).postDelayed ( v1, v2, v3 ); // invoke-virtual {v0, v1, v2, v3}, Landroid/os/Handler;->postDelayed(Ljava/lang/Runnable;J)Z
/* .line 161 */
return;
} // .end method
private static java.io.File getDumpSysFilePath ( ) {
/* .locals 7 */
/* .line 191 */
final String v0 = "DumpSysInfoUtil"; // const-string v0, "DumpSysInfoUtil"
int v1 = 0; // const/4 v1, 0x0
try { // :try_start_0
/* new-instance v2, Ljava/io/File; */
final String v3 = "/data/miuilog/stability/"; // const-string v3, "/data/miuilog/stability/"
/* invoke-direct {v2, v3}, Ljava/io/File;-><init>(Ljava/lang/String;)V */
/* .line 192 */
/* .local v2, "stabilityLog":Ljava/io/File; */
/* new-instance v3, Ljava/io/File; */
final String v4 = "hanglog"; // const-string v4, "hanglog"
/* invoke-direct {v3, v2, v4}, Ljava/io/File;-><init>(Ljava/io/File;Ljava/lang/String;)V */
/* .line 193 */
/* .local v3, "dumpsyslog":Ljava/io/File; */
v4 = (( java.io.File ) v3 ).exists ( ); // invoke-virtual {v3}, Ljava/io/File;->exists()Z
/* if-nez v4, :cond_1 */
/* .line 194 */
v4 = (( java.io.File ) v3 ).mkdirs ( ); // invoke-virtual {v3}, Ljava/io/File;->mkdirs()Z
/* if-nez v4, :cond_0 */
/* .line 195 */
final String v4 = "Cannot create dumpsyslog dir"; // const-string v4, "Cannot create dumpsyslog dir"
android.util.Slog .e ( v0,v4 );
/* .line 196 */
/* .line 198 */
} // :cond_0
/* nop */
/* .line 199 */
(( java.io.File ) v3 ).toString ( ); // invoke-virtual {v3}, Ljava/io/File;->toString()Ljava/lang/String;
/* .line 198 */
int v5 = -1; // const/4 v5, -0x1
/* const/16 v6, 0x1ff */
android.os.FileUtils .setPermissions ( v4,v6,v5,v5 );
/* .line 202 */
final String v4 = "mkdir dumpsyslog dir"; // const-string v4, "mkdir dumpsyslog dir"
android.util.Slog .d ( v0,v4 );
/* :try_end_0 */
/* .catch Ljava/lang/Exception; {:try_start_0 ..:try_end_0} :catch_0 */
/* .line 204 */
} // :cond_1
/* .line 205 */
} // .end local v2 # "stabilityLog":Ljava/io/File;
} // .end local v3 # "dumpsyslog":Ljava/io/File;
/* :catch_0 */
/* move-exception v2 */
/* .line 206 */
/* .local v2, "e":Ljava/lang/Exception; */
final String v3 = "crash in the getDumpSysFilePath()"; // const-string v3, "crash in the getDumpSysFilePath()"
android.util.Slog .e ( v0,v3,v2 );
/* .line 207 */
} // .end method
private static miui.mqsas.IMQSNative getmDaemon ( ) {
/* .locals 2 */
/* .line 43 */
v0 = com.miui.server.stability.DumpSysInfoUtil.mDaemon;
/* if-nez v0, :cond_0 */
/* .line 44 */
final String v0 = "miui.mqsas.IMQSNative"; // const-string v0, "miui.mqsas.IMQSNative"
android.os.ServiceManager .getService ( v0 );
miui.mqsas.IMQSNative$Stub .asInterface ( v0 );
/* .line 45 */
/* if-nez v0, :cond_0 */
/* .line 46 */
final String v0 = "DumpSysInfoUtil"; // const-string v0, "DumpSysInfoUtil"
final String v1 = "mqsasd not available!"; // const-string v1, "mqsasd not available!"
android.util.Slog .e ( v0,v1 );
/* .line 49 */
} // :cond_0
v0 = com.miui.server.stability.DumpSysInfoUtil.mDaemon;
} // .end method
static void lambda$captureDumpLog$0 ( ) { //synthethic
/* .locals 6 */
/* .line 59 */
final String v0 = "Freeze_Screen_Stuck"; // const-string v0, "Freeze_Screen_Stuck"
final String v1 = "dumpsys"; // const-string v1, "dumpsys"
try { // :try_start_0
com.miui.server.stability.DumpSysInfoUtil .deleteDumpSysFile ( v0 );
/* .line 61 */
/* new-instance v2, Lcom/android/server/ScoutHelper$Action; */
/* invoke-direct {v2}, Lcom/android/server/ScoutHelper$Action;-><init>()V */
/* .line 62 */
/* .local v2, "action":Lcom/android/server/ScoutHelper$Action; */
final String v3 = "SurfaceFlinger"; // const-string v3, "SurfaceFlinger"
(( com.android.server.ScoutHelper$Action ) v2 ).addActionAndParam ( v1, v3 ); // invoke-virtual {v2, v1, v3}, Lcom/android/server/ScoutHelper$Action;->addActionAndParam(Ljava/lang/String;Ljava/lang/String;)V
/* .line 63 */
final String v3 = "activity activities"; // const-string v3, "activity activities"
(( com.android.server.ScoutHelper$Action ) v2 ).addActionAndParam ( v1, v3 ); // invoke-virtual {v2, v1, v3}, Lcom/android/server/ScoutHelper$Action;->addActionAndParam(Ljava/lang/String;Ljava/lang/String;)V
/* .line 64 */
/* const-string/jumbo v3, "window" */
(( com.android.server.ScoutHelper$Action ) v2 ).addActionAndParam ( v1, v3 ); // invoke-virtual {v2, v1, v3}, Lcom/android/server/ScoutHelper$Action;->addActionAndParam(Ljava/lang/String;Ljava/lang/String;)V
/* .line 65 */
final String v3 = "input"; // const-string v3, "input"
(( com.android.server.ScoutHelper$Action ) v2 ).addActionAndParam ( v1, v3 ); // invoke-virtual {v2, v1, v3}, Lcom/android/server/ScoutHelper$Action;->addActionAndParam(Ljava/lang/String;Ljava/lang/String;)V
/* .line 67 */
com.miui.server.stability.DumpSysInfoUtil .getDumpSysFilePath ( );
/* .line 68 */
/* .local v1, "dumpsysLogPath":Ljava/io/File; */
if ( v1 != null) { // if-eqz v1, :cond_0
/* .line 69 */
final String v3 = "hanglog"; // const-string v3, "hanglog"
/* new-instance v4, Ljava/lang/StringBuilder; */
/* invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V */
/* .line 70 */
(( java.io.File ) v1 ).getAbsolutePath ( ); // invoke-virtual {v1}, Ljava/io/File;->getAbsolutePath()Ljava/lang/String;
(( java.lang.StringBuilder ) v4 ).append ( v5 ); // invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
final String v5 = "/"; // const-string v5, "/"
(( java.lang.StringBuilder ) v4 ).append ( v5 ); // invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v4 ).toString ( ); // invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
/* .line 69 */
com.android.server.ScoutHelper .dumpOfflineLog ( v0,v2,v3,v4 );
/* :try_end_0 */
/* .catch Ljava/lang/Exception; {:try_start_0 ..:try_end_0} :catch_0 */
/* .line 74 */
} // .end local v1 # "dumpsysLogPath":Ljava/io/File;
} // .end local v2 # "action":Lcom/android/server/ScoutHelper$Action;
} // :cond_0
/* .line 72 */
/* :catch_0 */
/* move-exception v0 */
/* .line 73 */
/* .local v0, "e":Ljava/lang/Exception; */
final String v1 = "DumpSysInfoUtil"; // const-string v1, "DumpSysInfoUtil"
final String v2 = "crash in the captureDumpLog()"; // const-string v2, "crash in the captureDumpLog()"
android.util.Slog .e ( v1,v2,v0 );
/* .line 75 */
} // .end local v0 # "e":Ljava/lang/Exception;
} // :goto_0
return;
} // .end method
static void lambda$deleteMissFetchByPower$1 ( java.io.File p0 ) { //synthethic
/* .locals 4 */
/* .param p0, "filePath" # Ljava/io/File; */
/* .line 151 */
if ( p0 != null) { // if-eqz p0, :cond_1
try { // :try_start_0
v0 = (( java.io.File ) p0 ).exists ( ); // invoke-virtual {p0}, Ljava/io/File;->exists()Z
if ( v0 != null) { // if-eqz v0, :cond_1
/* .line 152 */
(( java.io.File ) p0 ).listFiles ( ); // invoke-virtual {p0}, Ljava/io/File;->listFiles()[Ljava/io/File;
/* array-length v1, v0 */
int v2 = 0; // const/4 v2, 0x0
} // :goto_0
/* if-ge v2, v1, :cond_0 */
/* aget-object v3, v0, v2 */
/* .line 153 */
/* .local v3, "file":Ljava/io/File; */
(( java.io.File ) v3 ).delete ( ); // invoke-virtual {v3}, Ljava/io/File;->delete()Z
/* .line 152 */
/* nop */
} // .end local v3 # "file":Ljava/io/File;
/* add-int/lit8 v2, v2, 0x1 */
/* .line 155 */
} // :cond_0
(( java.io.File ) p0 ).delete ( ); // invoke-virtual {p0}, Ljava/io/File;->delete()Z
/* :try_end_0 */
/* .catch Ljava/lang/Exception; {:try_start_0 ..:try_end_0} :catch_0 */
/* .line 157 */
/* :catch_0 */
/* move-exception v0 */
/* .line 158 */
/* .local v0, "e":Ljava/lang/Exception; */
final String v1 = "DumpSysInfoUtil"; // const-string v1, "DumpSysInfoUtil"
final String v2 = "crash in the deleteMissFetchByPower()"; // const-string v2, "crash in the deleteMissFetchByPower()"
android.util.Slog .e ( v1,v2,v0 );
/* .line 159 */
} // .end local v0 # "e":Ljava/lang/Exception;
} // :cond_1
} // :goto_1
/* nop */
/* .line 160 */
} // :goto_2
return;
} // .end method
