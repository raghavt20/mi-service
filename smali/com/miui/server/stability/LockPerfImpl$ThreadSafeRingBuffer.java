class com.miui.server.stability.LockPerfImpl$ThreadSafeRingBuffer {
	 /* .source "LockPerfImpl.java" */
	 /* # annotations */
	 /* .annotation system Ldalvik/annotation/EnclosingClass; */
	 /* value = Lcom/miui/server/stability/LockPerfImpl; */
} // .end annotation
/* .annotation system Ldalvik/annotation/InnerClass; */
/* accessFlags = 0xa */
/* name = "ThreadSafeRingBuffer" */
} // .end annotation
/* .annotation system Ldalvik/annotation/Signature; */
/* value = { */
/* "<T:", */
/* "Ljava/lang/Object;", */
/* ">", */
/* "Ljava/lang/Object;" */
/* } */
} // .end annotation
/* # instance fields */
private final com.android.internal.util.RingBuffer mBuffer;
/* .annotation system Ldalvik/annotation/Signature; */
/* value = { */
/* "Lcom/android/internal/util/RingBuffer<", */
/* "TT;>;" */
/* } */
} // .end annotation
} // .end field
private final Integer mCapacity;
/* # direct methods */
 com.miui.server.stability.LockPerfImpl$ThreadSafeRingBuffer ( ) {
/* .locals 1 */
/* .param p2, "capacity" # I */
/* .annotation system Ldalvik/annotation/Signature; */
/* value = { */
/* "(", */
/* "Ljava/lang/Class<", */
/* "TT;>;I)V" */
/* } */
} // .end annotation
/* .line 301 */
/* .local p0, "this":Lcom/miui/server/stability/LockPerfImpl$ThreadSafeRingBuffer;, "Lcom/miui/server/stability/LockPerfImpl$ThreadSafeRingBuffer<TT;>;" */
/* .local p1, "clazz":Ljava/lang/Class;, "Ljava/lang/Class<TT;>;" */
/* invoke-direct {p0}, Ljava/lang/Object;-><init>()V */
/* .line 302 */
/* iput p2, p0, Lcom/miui/server/stability/LockPerfImpl$ThreadSafeRingBuffer;->mCapacity:I */
/* .line 303 */
/* new-instance v0, Lcom/android/internal/util/RingBuffer; */
/* invoke-direct {v0, p1, p2}, Lcom/android/internal/util/RingBuffer;-><init>(Ljava/lang/Class;I)V */
this.mBuffer = v0;
/* .line 304 */
return;
} // .end method
/* # virtual methods */
synchronized void append ( java.lang.Object p0 ) {
/* .locals 1 */
/* .annotation system Ldalvik/annotation/Signature; */
/* value = { */
/* "(TT;)V" */
/* } */
} // .end annotation
/* .local p0, "this":Lcom/miui/server/stability/LockPerfImpl$ThreadSafeRingBuffer;, "Lcom/miui/server/stability/LockPerfImpl$ThreadSafeRingBuffer<TT;>;" */
/* .local p1, "t":Ljava/lang/Object;, "TT;" */
/* monitor-enter p0 */
/* .line 307 */
try { // :try_start_0
v0 = this.mBuffer;
(( com.android.internal.util.RingBuffer ) v0 ).append ( p1 ); // invoke-virtual {v0, p1}, Lcom/android/internal/util/RingBuffer;->append(Ljava/lang/Object;)V
/* :try_end_0 */
/* .catchall {:try_start_0 ..:try_end_0} :catchall_0 */
/* .line 308 */
/* monitor-exit p0 */
return;
/* .line 306 */
} // .end local p0 # "this":Lcom/miui/server/stability/LockPerfImpl$ThreadSafeRingBuffer;, "Lcom/miui/server/stability/LockPerfImpl$ThreadSafeRingBuffer<TT;>;"
} // .end local p1 # "t":Ljava/lang/Object;, "TT;"
/* :catchall_0 */
/* move-exception p1 */
/* monitor-exit p0 */
/* throw p1 */
} // .end method
Integer capacity ( ) {
/* .locals 1 */
/* .line 315 */
/* .local p0, "this":Lcom/miui/server/stability/LockPerfImpl$ThreadSafeRingBuffer;, "Lcom/miui/server/stability/LockPerfImpl$ThreadSafeRingBuffer<TT;>;" */
/* iget v0, p0, Lcom/miui/server/stability/LockPerfImpl$ThreadSafeRingBuffer;->mCapacity:I */
} // .end method
synchronized java.lang.Object toArray ( ) {
/* .locals 1 */
/* .annotation system Ldalvik/annotation/Signature; */
/* value = { */
/* "()[TT;" */
/* } */
} // .end annotation
/* .local p0, "this":Lcom/miui/server/stability/LockPerfImpl$ThreadSafeRingBuffer;, "Lcom/miui/server/stability/LockPerfImpl$ThreadSafeRingBuffer<TT;>;" */
/* monitor-enter p0 */
/* .line 311 */
try { // :try_start_0
v0 = this.mBuffer;
(( com.android.internal.util.RingBuffer ) v0 ).toArray ( ); // invoke-virtual {v0}, Lcom/android/internal/util/RingBuffer;->toArray()[Ljava/lang/Object;
/* :try_end_0 */
/* .catchall {:try_start_0 ..:try_end_0} :catchall_0 */
/* monitor-exit p0 */
/* .line 311 */
} // .end local p0 # "this":Lcom/miui/server/stability/LockPerfImpl$ThreadSafeRingBuffer;, "Lcom/miui/server/stability/LockPerfImpl$ThreadSafeRingBuffer<TT;>;"
/* :catchall_0 */
/* move-exception v0 */
/* monitor-exit p0 */
/* throw v0 */
} // .end method
