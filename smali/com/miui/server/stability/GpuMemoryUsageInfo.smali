.class public Lcom/miui/server/stability/GpuMemoryUsageInfo;
.super Ljava/lang/Object;
.source "GpuMemoryUsageInfo.java"


# instance fields
.field private nativeTotalSize:J

.field private runtimeTotalSize:J

.field private totalSize:J

.field private usageList:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList<",
            "Lcom/miui/server/stability/GpuMemoryProcUsageInfo;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>()V
    .locals 1

    .line 14
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 15
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/miui/server/stability/GpuMemoryUsageInfo;->usageList:Ljava/util/ArrayList;

    .line 16
    return-void
.end method


# virtual methods
.method public add(Lcom/miui/server/stability/GpuMemoryProcUsageInfo;)V
    .locals 1
    .param p1, "procInfo"    # Lcom/miui/server/stability/GpuMemoryProcUsageInfo;

    .line 19
    iget-object v0, p0, Lcom/miui/server/stability/GpuMemoryUsageInfo;->usageList:Ljava/util/ArrayList;

    invoke-virtual {v0, p1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 20
    return-void
.end method

.method public getList()Ljava/util/ArrayList;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/ArrayList<",
            "Lcom/miui/server/stability/GpuMemoryProcUsageInfo;",
            ">;"
        }
    .end annotation

    .line 23
    iget-object v0, p0, Lcom/miui/server/stability/GpuMemoryUsageInfo;->usageList:Ljava/util/ArrayList;

    return-object v0
.end method

.method public getNativeTotalSize()J
    .locals 2

    .line 47
    iget-wide v0, p0, Lcom/miui/server/stability/GpuMemoryUsageInfo;->nativeTotalSize:J

    return-wide v0
.end method

.method public getRuntimeTotalSize()J
    .locals 2

    .line 39
    iget-wide v0, p0, Lcom/miui/server/stability/GpuMemoryUsageInfo;->runtimeTotalSize:J

    return-wide v0
.end method

.method public getTotalSize()J
    .locals 2

    .line 31
    iget-wide v0, p0, Lcom/miui/server/stability/GpuMemoryUsageInfo;->totalSize:J

    return-wide v0
.end method

.method public setNativeTotalSize(J)V
    .locals 0
    .param p1, "nativeTotalSize"    # J

    .line 43
    iput-wide p1, p0, Lcom/miui/server/stability/GpuMemoryUsageInfo;->nativeTotalSize:J

    .line 44
    return-void
.end method

.method public setRuntimeTotalSize(J)V
    .locals 0
    .param p1, "runtimeTotalSize"    # J

    .line 35
    iput-wide p1, p0, Lcom/miui/server/stability/GpuMemoryUsageInfo;->runtimeTotalSize:J

    .line 36
    return-void
.end method

.method public setTotalSize(J)V
    .locals 0
    .param p1, "totalSize"    # J

    .line 27
    iput-wide p1, p0, Lcom/miui/server/stability/GpuMemoryUsageInfo;->totalSize:J

    .line 28
    return-void
.end method

.method public toString(I)Ljava/lang/String;
    .locals 5
    .param p1, "gpuType"    # I

    .line 51
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    .line 52
    .local v0, "sb":Ljava/lang/StringBuilder;
    const/4 v1, 0x1

    if-ne p1, v1, :cond_0

    .line 53
    const-string v1, "KgslMemory"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    goto :goto_0

    .line 54
    :cond_0
    const/4 v1, 0x2

    if-ne p1, v1, :cond_2

    .line 55
    const-string v1, "MaliMemory"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 59
    :goto_0
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, " info total: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-wide v2, p0, Lcom/miui/server/stability/GpuMemoryUsageInfo;->totalSize:J

    invoke-virtual {v1, v2, v3}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "kB RuntimeTotalSize="

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-wide v2, p0, Lcom/miui/server/stability/GpuMemoryUsageInfo;->runtimeTotalSize:J

    invoke-virtual {v1, v2, v3}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "kB NativeTotalSize="

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-wide v2, p0, Lcom/miui/server/stability/GpuMemoryUsageInfo;->nativeTotalSize:J

    invoke-virtual {v1, v2, v3}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "kB\n"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 61
    iget-object v1, p0, Lcom/miui/server/stability/GpuMemoryUsageInfo;->usageList:Ljava/util/ArrayList;

    invoke-virtual {v1}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_1
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_1

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/miui/server/stability/GpuMemoryProcUsageInfo;

    .line 62
    .local v2, "info":Lcom/miui/server/stability/GpuMemoryProcUsageInfo;
    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v2, p1}, Lcom/miui/server/stability/GpuMemoryProcUsageInfo;->toString(I)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, "\n"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 63
    .end local v2    # "info":Lcom/miui/server/stability/GpuMemoryProcUsageInfo;
    goto :goto_1

    .line 64
    :cond_1
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    return-object v1

    .line 57
    :cond_2
    const-string v1, "Error Info"

    return-object v1
.end method
