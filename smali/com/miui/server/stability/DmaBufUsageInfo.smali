.class public Lcom/miui/server/stability/DmaBufUsageInfo;
.super Ljava/lang/Object;
.source "DmaBufUsageInfo.java"


# instance fields
.field private kernelRss:J

.field private nativeTotalSize:J

.field private runtimeTotalSize:J

.field private totalPss:J

.field private totalRss:J

.field private totalSize:J

.field private usageList:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList<",
            "Lcom/miui/server/stability/DmaBufProcUsageInfo;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>()V
    .locals 1

    .line 15
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 16
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/miui/server/stability/DmaBufUsageInfo;->usageList:Ljava/util/ArrayList;

    .line 17
    return-void
.end method


# virtual methods
.method public add(Lcom/miui/server/stability/DmaBufProcUsageInfo;)V
    .locals 1
    .param p1, "procInfo"    # Lcom/miui/server/stability/DmaBufProcUsageInfo;

    .line 20
    iget-object v0, p0, Lcom/miui/server/stability/DmaBufUsageInfo;->usageList:Ljava/util/ArrayList;

    invoke-virtual {v0, p1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 21
    return-void
.end method

.method public getKernelRss()J
    .locals 2

    .line 56
    iget-wide v0, p0, Lcom/miui/server/stability/DmaBufUsageInfo;->kernelRss:J

    return-wide v0
.end method

.method public getList()Ljava/util/ArrayList;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/ArrayList<",
            "Lcom/miui/server/stability/DmaBufProcUsageInfo;",
            ">;"
        }
    .end annotation

    .line 24
    iget-object v0, p0, Lcom/miui/server/stability/DmaBufUsageInfo;->usageList:Ljava/util/ArrayList;

    return-object v0
.end method

.method public getNativeTotalSize()J
    .locals 2

    .line 48
    iget-wide v0, p0, Lcom/miui/server/stability/DmaBufUsageInfo;->nativeTotalSize:J

    return-wide v0
.end method

.method public getRuntimeTotalSize()J
    .locals 2

    .line 40
    iget-wide v0, p0, Lcom/miui/server/stability/DmaBufUsageInfo;->runtimeTotalSize:J

    return-wide v0
.end method

.method public getTotalPss()J
    .locals 2

    .line 72
    iget-wide v0, p0, Lcom/miui/server/stability/DmaBufUsageInfo;->totalPss:J

    return-wide v0
.end method

.method public getTotalRss()J
    .locals 2

    .line 64
    iget-wide v0, p0, Lcom/miui/server/stability/DmaBufUsageInfo;->totalRss:J

    return-wide v0
.end method

.method public getTotalSize()J
    .locals 2

    .line 32
    iget-wide v0, p0, Lcom/miui/server/stability/DmaBufUsageInfo;->totalSize:J

    return-wide v0
.end method

.method public setKernelRss(J)V
    .locals 0
    .param p1, "kernelRss"    # J

    .line 52
    iput-wide p1, p0, Lcom/miui/server/stability/DmaBufUsageInfo;->kernelRss:J

    .line 53
    return-void
.end method

.method public setNativeTotalSize(J)V
    .locals 0
    .param p1, "nativeTotalSize"    # J

    .line 44
    iput-wide p1, p0, Lcom/miui/server/stability/DmaBufUsageInfo;->nativeTotalSize:J

    .line 45
    return-void
.end method

.method public setRuntimeTotalSize(J)V
    .locals 0
    .param p1, "runtimeTotalSize"    # J

    .line 36
    iput-wide p1, p0, Lcom/miui/server/stability/DmaBufUsageInfo;->runtimeTotalSize:J

    .line 37
    return-void
.end method

.method public setTotalPss(J)V
    .locals 0
    .param p1, "totalPss"    # J

    .line 68
    iput-wide p1, p0, Lcom/miui/server/stability/DmaBufUsageInfo;->totalPss:J

    .line 69
    return-void
.end method

.method public setTotalRss(J)V
    .locals 0
    .param p1, "totalRss"    # J

    .line 60
    iput-wide p1, p0, Lcom/miui/server/stability/DmaBufUsageInfo;->totalRss:J

    .line 61
    return-void
.end method

.method public setTotalSize(J)V
    .locals 0
    .param p1, "totalSize"    # J

    .line 28
    iput-wide p1, p0, Lcom/miui/server/stability/DmaBufUsageInfo;->totalSize:J

    .line 29
    return-void
.end method

.method public toString()Ljava/lang/String;
    .locals 5

    .line 76
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    .line 77
    .local v0, "sb":Ljava/lang/StringBuilder;
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "DMA-BUF info total: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-wide v2, p0, Lcom/miui/server/stability/DmaBufUsageInfo;->totalSize:J

    invoke-virtual {v1, v2, v3}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "kB kernel_rss="

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-wide v2, p0, Lcom/miui/server/stability/DmaBufUsageInfo;->kernelRss:J

    invoke-virtual {v1, v2, v3}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "kB userspace_rss="

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-wide v2, p0, Lcom/miui/server/stability/DmaBufUsageInfo;->totalRss:J

    invoke-virtual {v1, v2, v3}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "kB userspace_pss="

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-wide v2, p0, Lcom/miui/server/stability/DmaBufUsageInfo;->totalPss:J

    invoke-virtual {v1, v2, v3}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "kB RuntimeTotalSize="

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-wide v2, p0, Lcom/miui/server/stability/DmaBufUsageInfo;->runtimeTotalSize:J

    invoke-virtual {v1, v2, v3}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "kB NativeTotalSize="

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-wide v2, p0, Lcom/miui/server/stability/DmaBufUsageInfo;->nativeTotalSize:J

    invoke-virtual {v1, v2, v3}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "kB\n"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 81
    iget-object v1, p0, Lcom/miui/server/stability/DmaBufUsageInfo;->usageList:Ljava/util/ArrayList;

    invoke-virtual {v1}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/miui/server/stability/DmaBufProcUsageInfo;

    .line 82
    .local v2, "info":Lcom/miui/server/stability/DmaBufProcUsageInfo;
    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v2}, Lcom/miui/server/stability/DmaBufProcUsageInfo;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, "\n"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 83
    .end local v2    # "info":Lcom/miui/server/stability/DmaBufProcUsageInfo;
    goto :goto_0

    .line 84
    :cond_0
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    return-object v1
.end method
