public class com.miui.server.stability.DmaBufProcUsageInfo {
	 /* .source "DmaBufProcUsageInfo.java" */
	 /* # instance fields */
	 private Integer oomadj;
	 private Integer pid;
	 private java.lang.String procName;
	 private Long pss;
	 private Long rss;
	 /* # direct methods */
	 public com.miui.server.stability.DmaBufProcUsageInfo ( ) {
		 /* .locals 0 */
		 /* .line 13 */
		 /* invoke-direct {p0}, Ljava/lang/Object;-><init>()V */
		 /* .line 14 */
		 return;
	 } // .end method
	 /* # virtual methods */
	 public java.lang.String getName ( ) {
		 /* .locals 1 */
		 /* .line 21 */
		 v0 = this.procName;
	 } // .end method
	 public Integer getOomadj ( ) {
		 /* .locals 1 */
		 /* .line 37 */
		 /* iget v0, p0, Lcom/miui/server/stability/DmaBufProcUsageInfo;->oomadj:I */
	 } // .end method
	 public Integer getPid ( ) {
		 /* .locals 1 */
		 /* .line 29 */
		 /* iget v0, p0, Lcom/miui/server/stability/DmaBufProcUsageInfo;->pid:I */
	 } // .end method
	 public Long getPss ( ) {
		 /* .locals 2 */
		 /* .line 53 */
		 /* iget-wide v0, p0, Lcom/miui/server/stability/DmaBufProcUsageInfo;->pss:J */
		 /* return-wide v0 */
	 } // .end method
	 public Long getRss ( ) {
		 /* .locals 2 */
		 /* .line 45 */
		 /* iget-wide v0, p0, Lcom/miui/server/stability/DmaBufProcUsageInfo;->rss:J */
		 /* return-wide v0 */
	 } // .end method
	 public void setName ( java.lang.String p0 ) {
		 /* .locals 0 */
		 /* .param p1, "procName" # Ljava/lang/String; */
		 /* .line 17 */
		 this.procName = p1;
		 /* .line 18 */
		 return;
	 } // .end method
	 public void setOomadj ( Integer p0 ) {
		 /* .locals 0 */
		 /* .param p1, "oomadj" # I */
		 /* .line 33 */
		 /* iput p1, p0, Lcom/miui/server/stability/DmaBufProcUsageInfo;->oomadj:I */
		 /* .line 34 */
		 return;
	 } // .end method
	 public void setPid ( Integer p0 ) {
		 /* .locals 0 */
		 /* .param p1, "pid" # I */
		 /* .line 25 */
		 /* iput p1, p0, Lcom/miui/server/stability/DmaBufProcUsageInfo;->pid:I */
		 /* .line 26 */
		 return;
	 } // .end method
	 public void setPss ( Long p0 ) {
		 /* .locals 0 */
		 /* .param p1, "pss" # J */
		 /* .line 49 */
		 /* iput-wide p1, p0, Lcom/miui/server/stability/DmaBufProcUsageInfo;->pss:J */
		 /* .line 50 */
		 return;
	 } // .end method
	 public void setRss ( Long p0 ) {
		 /* .locals 0 */
		 /* .param p1, "rss" # J */
		 /* .line 41 */
		 /* iput-wide p1, p0, Lcom/miui/server/stability/DmaBufProcUsageInfo;->rss:J */
		 /* .line 42 */
		 return;
	 } // .end method
	 public java.lang.String toString ( ) {
		 /* .locals 4 */
		 /* .line 57 */
		 /* new-instance v0, Ljava/lang/StringBuilder; */
		 /* invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V */
		 /* .line 58 */
		 /* .local v0, "sb":Ljava/lang/StringBuilder; */
		 /* new-instance v1, Ljava/lang/StringBuilder; */
		 /* invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V */
		 final String v2 = "DMA-BUF proc info Name: "; // const-string v2, "DMA-BUF proc info Name: "
		 (( java.lang.StringBuilder ) v1 ).append ( v2 ); // invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
		 v2 = this.procName;
		 (( java.lang.StringBuilder ) v1 ).append ( v2 ); // invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
		 final String v2 = " Pid="; // const-string v2, " Pid="
		 (( java.lang.StringBuilder ) v1 ).append ( v2 ); // invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
		 /* iget v2, p0, Lcom/miui/server/stability/DmaBufProcUsageInfo;->pid:I */
		 (( java.lang.StringBuilder ) v1 ).append ( v2 ); // invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;
		 final String v2 = " Oomadj="; // const-string v2, " Oomadj="
		 (( java.lang.StringBuilder ) v1 ).append ( v2 ); // invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
		 /* iget v2, p0, Lcom/miui/server/stability/DmaBufProcUsageInfo;->oomadj:I */
		 (( java.lang.StringBuilder ) v1 ).append ( v2 ); // invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;
		 final String v2 = " Rss="; // const-string v2, " Rss="
		 (( java.lang.StringBuilder ) v1 ).append ( v2 ); // invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
		 /* iget-wide v2, p0, Lcom/miui/server/stability/DmaBufProcUsageInfo;->rss:J */
		 (( java.lang.StringBuilder ) v1 ).append ( v2, v3 ); // invoke-virtual {v1, v2, v3}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;
		 final String v2 = "kB Pss="; // const-string v2, "kB Pss="
		 (( java.lang.StringBuilder ) v1 ).append ( v2 ); // invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
		 /* iget-wide v2, p0, Lcom/miui/server/stability/DmaBufProcUsageInfo;->pss:J */
		 (( java.lang.StringBuilder ) v1 ).append ( v2, v3 ); // invoke-virtual {v1, v2, v3}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;
		 final String v2 = "kB"; // const-string v2, "kB"
		 (( java.lang.StringBuilder ) v1 ).append ( v2 ); // invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
		 (( java.lang.StringBuilder ) v1 ).toString ( ); // invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
		 (( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
		 /* .line 60 */
		 (( java.lang.StringBuilder ) v0 ).toString ( ); // invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
	 } // .end method
