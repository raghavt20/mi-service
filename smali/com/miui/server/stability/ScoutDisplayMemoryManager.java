public class com.miui.server.stability.ScoutDisplayMemoryManager {
	 /* .source "ScoutDisplayMemoryManager.java" */
	 /* # annotations */
	 /* .annotation system Ldalvik/annotation/MemberClasses; */
	 /* value = { */
	 /* Lcom/miui/server/stability/ScoutDisplayMemoryManager$DiaplayMemoryErrorInfo;, */
	 /* Lcom/miui/server/stability/ScoutDisplayMemoryManager$CrimeScenePreservedDialog; */
	 /* } */
} // .end annotation
/* # static fields */
private static final java.lang.String APP_ID;
private static final Integer DISPLAY_DMABUF;
private static final Integer DISPLAY_GPU_MEMORY;
private static final java.lang.String DMABUF_EVENT_NAME;
private static final Integer DMABUF_LEAK_SF_THRESHOLD;
private static final Integer DMABUF_LEAK_THRESHOLD;
private static final Boolean ENABLE_SCOUT_MEMORY_MONITOR;
private static final Integer FLAG_NOT_LIMITED_BY_USER_EXPERIENCE_PLAN;
private static final Long GBTOKB;
private static final java.lang.String GPU_MEMORY_EVENT_NAME;
private static final Integer GPU_MEMORY_LEAK_THRESHOLD;
private static final Long MBTOKB;
private static final Integer MEM_DISABLE_REPORT_INTERVAL;
private static final Integer MEM_ERROR_DIALOG_TIMEOUT;
private static final Integer MEM_REPORT_INTERVAL;
private static final java.lang.String ONETRACK_PACKAGE_NAME;
private static final java.lang.String ONE_TRACK_ACTION;
private static final java.lang.String PACKAGE;
private static final Double PROC_PROPORTIONAL_THRESHOLD;
public static final Integer RESUME_ACTION_CANCLE;
public static final Integer RESUME_ACTION_CONFIRM;
public static final Integer RESUME_ACTION_CRASH;
public static final Integer RESUME_ACTION_DIALOG;
public static final Integer RESUME_ACTION_FAIL;
public static final Integer RESUME_ACTION_KILL;
private static final Boolean SCOUT_MEMORY_DISABLE_DMABUF;
private static final Boolean SCOUT_MEMORY_DISABLE_GPU;
private static final java.lang.String SYSPROP_DMABUF_LEAK_THRESHOLD;
private static final java.lang.String SYSPROP_ENABLE_SCOUT_MEMORY_MONITOR;
private static final java.lang.String SYSPROP_ENABLE_SCOUT_MEMORY_RESUME;
private static final java.lang.String SYSPROP_GPU_MEMORY_LEAK_THRESHOLD;
private static final java.lang.String SYSPROP_PRESERVE_CRIME_SCENE;
private static final java.lang.String SYSPROP_SCOUT_MEMORY_DISABLE_DMABUF;
private static final java.lang.String SYSPROP_SCOUT_MEMORY_DISABLE_GPU;
private static final java.lang.String TAG;
private static Boolean debug;
private static com.miui.server.stability.ScoutDisplayMemoryManager displayMemoryManager;
/* # instance fields */
private Integer gpuType;
private final java.util.concurrent.atomic.AtomicBoolean isBusy;
private Boolean isCameraForeground;
private android.content.Context mContext;
private Integer mCrashTimes;
private final java.util.concurrent.atomic.AtomicBoolean mDisableState;
private final java.util.concurrent.atomic.AtomicBoolean mIsShowDialog;
private Integer mLastCrashPid;
private java.lang.String mLastCrashProc;
private volatile Long mLastReportTime;
private com.android.server.am.ActivityManagerService mService;
private volatile Long mShowDialogTime;
private Integer totalRam;
/* # direct methods */
public static void $r8$lambda$H9ftIko0R1HvS0uJf4czarQNMY4 ( com.miui.server.stability.ScoutDisplayMemoryManager p0, com.miui.server.stability.ScoutDisplayMemoryManager$DiaplayMemoryErrorInfo p1 ) { //synthethic
	 /* .locals 0 */
	 /* invoke-direct {p0, p1}, Lcom/miui/server/stability/ScoutDisplayMemoryManager;->lambda$reportDisplayMemoryLeakEvent$0(Lcom/miui/server/stability/ScoutDisplayMemoryManager$DiaplayMemoryErrorInfo;)V */
	 return;
} // .end method
public static void $r8$lambda$lVvY-Qo6TgHkb0rMhGyglm5tt2I ( com.miui.server.stability.ScoutDisplayMemoryManager p0, java.lang.String p1, com.miui.server.stability.ScoutDisplayMemoryManager$DiaplayMemoryErrorInfo p2 ) { //synthethic
	 /* .locals 0 */
	 /* invoke-direct {p0, p1, p2}, Lcom/miui/server/stability/ScoutDisplayMemoryManager;->lambda$handleReportMqs$1(Ljava/lang/String;Lcom/miui/server/stability/ScoutDisplayMemoryManager$DiaplayMemoryErrorInfo;)V */
	 return;
} // .end method
static Integer -$$Nest$fgetgpuType ( com.miui.server.stability.ScoutDisplayMemoryManager p0 ) { //bridge//synthethic
	 /* .locals 0 */
	 /* iget p0, p0, Lcom/miui/server/stability/ScoutDisplayMemoryManager;->gpuType:I */
} // .end method
static java.util.concurrent.atomic.AtomicBoolean -$$Nest$fgetisBusy ( com.miui.server.stability.ScoutDisplayMemoryManager p0 ) { //bridge//synthethic
	 /* .locals 0 */
	 p0 = this.isBusy;
} // .end method
static Boolean -$$Nest$fgetisCameraForeground ( com.miui.server.stability.ScoutDisplayMemoryManager p0 ) { //bridge//synthethic
	 /* .locals 0 */
	 /* iget-boolean p0, p0, Lcom/miui/server/stability/ScoutDisplayMemoryManager;->isCameraForeground:Z */
} // .end method
static com.miui.server.stability.DmaBufUsageInfo -$$Nest$mreadDmabufInfo ( com.miui.server.stability.ScoutDisplayMemoryManager p0 ) { //bridge//synthethic
	 /* .locals 0 */
	 /* invoke-direct {p0}, Lcom/miui/server/stability/ScoutDisplayMemoryManager;->readDmabufInfo()Lcom/miui/server/stability/DmaBufUsageInfo; */
} // .end method
static com.miui.server.stability.GpuMemoryUsageInfo -$$Nest$mreadGpuMemoryInfo ( com.miui.server.stability.ScoutDisplayMemoryManager p0, Integer p1 ) { //bridge//synthethic
	 /* .locals 0 */
	 /* invoke-direct {p0, p1}, Lcom/miui/server/stability/ScoutDisplayMemoryManager;->readGpuMemoryInfo(I)Lcom/miui/server/stability/GpuMemoryUsageInfo; */
} // .end method
static void -$$Nest$mresumeMemLeak ( com.miui.server.stability.ScoutDisplayMemoryManager p0, com.miui.server.stability.ScoutDisplayMemoryManager$DiaplayMemoryErrorInfo p1 ) { //bridge//synthethic
	 /* .locals 0 */
	 /* invoke-direct {p0, p1}, Lcom/miui/server/stability/ScoutDisplayMemoryManager;->resumeMemLeak(Lcom/miui/server/stability/ScoutDisplayMemoryManager$DiaplayMemoryErrorInfo;)V */
	 return;
} // .end method
static Integer -$$Nest$sfgetDMABUF_LEAK_SF_THRESHOLD ( ) { //bridge//synthethic
	 /* .locals 1 */
} // .end method
static Integer -$$Nest$sfgetDMABUF_LEAK_THRESHOLD ( ) { //bridge//synthethic
	 /* .locals 1 */
} // .end method
static Integer -$$Nest$sfgetGPU_MEMORY_LEAK_THRESHOLD ( ) { //bridge//synthethic
	 /* .locals 1 */
} // .end method
static Boolean -$$Nest$smdoPreserveCrimeSceneIfNeed ( java.lang.String p0, Integer p1, Long p2, java.lang.String p3 ) { //bridge//synthethic
	 /* .locals 0 */
	 p0 = 	 com.miui.server.stability.ScoutDisplayMemoryManager .doPreserveCrimeSceneIfNeed ( p0,p1,p2,p3,p4 );
} // .end method
static com.miui.server.stability.ScoutDisplayMemoryManager ( ) {
	 /* .locals 4 */
	 /* .line 61 */
	 /* sget-boolean v0, Lcom/android/server/ScoutHelper;->ENABLED_SCOUT_DEBUG:Z */
	 int v1 = 0; // const/4 v1, 0x0
	 /* if-nez v0, :cond_1 */
	 /* .line 62 */
	 v0 = 	 miui.mqsas.scout.ScoutUtils .isLibraryTest ( );
	 if ( v0 != null) { // if-eqz v0, :cond_0
	 } // :cond_0
	 /* move v0, v1 */
} // :cond_1
} // :goto_0
int v0 = 1; // const/4 v0, 0x1
} // :goto_1
com.miui.server.stability.ScoutDisplayMemoryManager.debug = (v0!= 0);
/* .line 83 */
final String v0 = "persist.sys.debug.scout_memory_dmabuf_threshold"; // const-string v0, "persist.sys.debug.scout_memory_dmabuf_threshold"
/* const/16 v2, 0xa00 */
v3 = android.os.SystemProperties .getInt ( v0,v2 );
/* .line 85 */
/* const/16 v3, 0xbb8 */
v0 = android.os.SystemProperties .getInt ( v0,v3 );
/* .line 87 */
final String v0 = "persist.sys.debug.scout_memory_gpu_threshold"; // const-string v0, "persist.sys.debug.scout_memory_gpu_threshold"
v0 = android.os.SystemProperties .getInt ( v0,v2 );
/* .line 89 */
final String v0 = "persist.sys.debug.enable_scout_memory_monitor"; // const-string v0, "persist.sys.debug.enable_scout_memory_monitor"
v0 = android.os.SystemProperties .getBoolean ( v0,v1 );
com.miui.server.stability.ScoutDisplayMemoryManager.ENABLE_SCOUT_MEMORY_MONITOR = (v0!= 0);
/* .line 91 */
final String v0 = "persist.sys.debug.scout_memory_disable_gpu"; // const-string v0, "persist.sys.debug.scout_memory_disable_gpu"
v0 = android.os.SystemProperties .getBoolean ( v0,v1 );
com.miui.server.stability.ScoutDisplayMemoryManager.SCOUT_MEMORY_DISABLE_GPU = (v0!= 0);
/* .line 93 */
final String v0 = "persist.sys.debug.scout_memory_disable_dmabuf"; // const-string v0, "persist.sys.debug.scout_memory_disable_dmabuf"
v0 = android.os.SystemProperties .getBoolean ( v0,v1 );
com.miui.server.stability.ScoutDisplayMemoryManager.SCOUT_MEMORY_DISABLE_DMABUF = (v0!= 0);
return;
} // .end method
private com.miui.server.stability.ScoutDisplayMemoryManager ( ) {
/* .locals 2 */
/* .line 142 */
/* invoke-direct {p0}, Ljava/lang/Object;-><init>()V */
/* .line 96 */
/* new-instance v0, Ljava/util/concurrent/atomic/AtomicBoolean; */
int v1 = 0; // const/4 v1, 0x0
/* invoke-direct {v0, v1}, Ljava/util/concurrent/atomic/AtomicBoolean;-><init>(Z)V */
this.isBusy = v0;
/* .line 97 */
/* new-instance v0, Ljava/util/concurrent/atomic/AtomicBoolean; */
/* invoke-direct {v0, v1}, Ljava/util/concurrent/atomic/AtomicBoolean;-><init>(Z)V */
this.mIsShowDialog = v0;
/* .line 98 */
/* new-instance v0, Ljava/util/concurrent/atomic/AtomicBoolean; */
/* invoke-direct {v0, v1}, Ljava/util/concurrent/atomic/AtomicBoolean;-><init>(Z)V */
this.mDisableState = v0;
/* .line 129 */
/* iput v1, p0, Lcom/miui/server/stability/ScoutDisplayMemoryManager;->totalRam:I */
/* .line 143 */
/* const-wide/16 v0, 0x0 */
/* iput-wide v0, p0, Lcom/miui/server/stability/ScoutDisplayMemoryManager;->mLastReportTime:J */
/* .line 144 */
return;
} // .end method
private Boolean checkDmaBufLeak ( ) {
/* .locals 10 */
/* .line 627 */
/* sget-boolean v0, Lcom/miui/server/stability/ScoutDisplayMemoryManager;->SCOUT_MEMORY_DISABLE_DMABUF:Z */
int v1 = 0; // const/4 v1, 0x0
if ( v0 != null) { // if-eqz v0, :cond_0
/* .line 628 */
} // :cond_0
/* const-wide/16 v2, 0x0 */
/* .line 629 */
/* .local v2, "dmaBufTotal":J */
android.os.Debug .getIonHeapsSizeKb ( );
/* move-result-wide v4 */
/* .line 630 */
/* .local v4, "ionHeap":J */
/* const-wide/16 v6, 0x0 */
/* cmp-long v0, v4, v6 */
/* if-ltz v0, :cond_1 */
/* .line 631 */
/* move-wide v2, v4 */
/* .line 633 */
} // :cond_1
android.os.Debug .getDmabufTotalExportedKb ( );
/* move-result-wide v2 */
/* .line 635 */
} // :goto_0
/* sget-boolean v0, Lcom/miui/server/stability/ScoutDisplayMemoryManager;->debug:Z */
/* const-wide/16 v6, 0x400 */
if ( v0 != null) { // if-eqz v0, :cond_2
/* new-instance v0, Ljava/lang/StringBuilder; */
/* invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V */
final String v8 = "checkDmaBufLeak size:"; // const-string v8, "checkDmaBufLeak size:"
(( java.lang.StringBuilder ) v0 ).append ( v8 ); // invoke-virtual {v0, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
com.miui.server.stability.ScoutDisplayMemoryManager .stringifyKBSize ( v2,v3 );
(( java.lang.StringBuilder ) v0 ).append ( v8 ); // invoke-virtual {v0, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
final String v8 = " monitor threshold = "; // const-string v8, " monitor threshold = "
(( java.lang.StringBuilder ) v0 ).append ( v8 ); // invoke-virtual {v0, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
/* int-to-long v8, v8 */
/* mul-long/2addr v8, v6 */
(( java.lang.StringBuilder ) v0 ).append ( v8, v9 ); // invoke-virtual {v0, v8, v9}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;
final String v8 = "kB"; // const-string v8, "kB"
(( java.lang.StringBuilder ) v0 ).append ( v8 ); // invoke-virtual {v0, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v0 ).toString ( ); // invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
final String v8 = "MIUIScout Memory"; // const-string v8, "MIUIScout Memory"
android.util.Slog .d ( v8,v0 );
/* .line 637 */
} // :cond_2
/* int-to-long v8, v0 */
/* mul-long/2addr v8, v6 */
/* cmp-long v0, v2, v8 */
/* if-lez v0, :cond_3 */
/* .line 638 */
final String v0 = "dmabuf_leak"; // const-string v0, "dmabuf_leak"
/* invoke-direct {p0, v0, v2, v3}, Lcom/miui/server/stability/ScoutDisplayMemoryManager;->reportDmabufLeakException(Ljava/lang/String;J)V */
/* .line 639 */
int v0 = 1; // const/4 v0, 0x1
/* .line 641 */
} // :cond_3
} // .end method
private Boolean checkDmaBufLeak ( com.android.server.am.ScoutMeminfo p0 ) {
/* .locals 10 */
/* .param p1, "scoutMeminfo" # Lcom/android/server/am/ScoutMeminfo; */
/* .line 609 */
/* sget-boolean v0, Lcom/miui/server/stability/ScoutDisplayMemoryManager;->SCOUT_MEMORY_DISABLE_DMABUF:Z */
int v1 = 0; // const/4 v1, 0x0
if ( v0 != null) { // if-eqz v0, :cond_0
/* .line 610 */
} // :cond_0
/* const-wide/16 v2, 0x0 */
/* .line 611 */
/* .local v2, "dmaBufTotal":J */
android.os.Debug .getIonHeapsSizeKb ( );
/* move-result-wide v4 */
/* .line 612 */
/* .local v4, "ionHeap":J */
/* const-wide/16 v6, 0x0 */
/* cmp-long v0, v4, v6 */
/* if-ltz v0, :cond_1 */
/* .line 613 */
/* move-wide v2, v4 */
/* .line 615 */
} // :cond_1
(( com.android.server.am.ScoutMeminfo ) p1 ).getTotalExportedDmabuf ( ); // invoke-virtual {p1}, Lcom/android/server/am/ScoutMeminfo;->getTotalExportedDmabuf()J
/* move-result-wide v2 */
/* .line 617 */
} // :goto_0
/* sget-boolean v0, Lcom/miui/server/stability/ScoutDisplayMemoryManager;->debug:Z */
/* const-wide/16 v6, 0x400 */
if ( v0 != null) { // if-eqz v0, :cond_2
/* new-instance v0, Ljava/lang/StringBuilder; */
/* invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V */
final String v8 = "checkDmaBufLeak(root) size:"; // const-string v8, "checkDmaBufLeak(root) size:"
(( java.lang.StringBuilder ) v0 ).append ( v8 ); // invoke-virtual {v0, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
com.miui.server.stability.ScoutDisplayMemoryManager .stringifyKBSize ( v2,v3 );
(( java.lang.StringBuilder ) v0 ).append ( v8 ); // invoke-virtual {v0, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
final String v8 = " monitor threshold = "; // const-string v8, " monitor threshold = "
(( java.lang.StringBuilder ) v0 ).append ( v8 ); // invoke-virtual {v0, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
/* int-to-long v8, v8 */
/* mul-long/2addr v8, v6 */
(( java.lang.StringBuilder ) v0 ).append ( v8, v9 ); // invoke-virtual {v0, v8, v9}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;
final String v8 = "kB"; // const-string v8, "kB"
(( java.lang.StringBuilder ) v0 ).append ( v8 ); // invoke-virtual {v0, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v0 ).toString ( ); // invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
final String v8 = "MIUIScout Memory"; // const-string v8, "MIUIScout Memory"
android.util.Slog .d ( v8,v0 );
/* .line 619 */
} // :cond_2
/* int-to-long v8, v0 */
/* mul-long/2addr v8, v6 */
/* cmp-long v0, v2, v8 */
/* if-lez v0, :cond_3 */
/* .line 620 */
final String v0 = "dmabuf_leak"; // const-string v0, "dmabuf_leak"
/* invoke-direct {p0, v0, v2, v3}, Lcom/miui/server/stability/ScoutDisplayMemoryManager;->reportDmabufLeakException(Ljava/lang/String;J)V */
/* .line 621 */
int v0 = 1; // const/4 v0, 0x1
/* .line 623 */
} // :cond_3
} // .end method
private Boolean checkGpuMemoryLeak ( ) {
/* .locals 9 */
/* .line 645 */
/* iget v0, p0, Lcom/miui/server/stability/ScoutDisplayMemoryManager;->gpuType:I */
int v1 = 0; // const/4 v1, 0x0
int v2 = 1; // const/4 v2, 0x1
/* if-lt v0, v2, :cond_3 */
/* sget-boolean v3, Lcom/miui/server/stability/ScoutDisplayMemoryManager;->SCOUT_MEMORY_DISABLE_GPU:Z */
if ( v3 != null) { // if-eqz v3, :cond_0
/* .line 646 */
} // :cond_0
/* invoke-direct {p0, v0}, Lcom/miui/server/stability/ScoutDisplayMemoryManager;->getTotalGpuMemory(I)J */
/* move-result-wide v3 */
/* .line 647 */
/* .local v3, "totalGpuMemoru":J */
/* sget-boolean v0, Lcom/miui/server/stability/ScoutDisplayMemoryManager;->debug:Z */
/* const-wide/16 v5, 0x400 */
if ( v0 != null) { // if-eqz v0, :cond_1
/* new-instance v0, Ljava/lang/StringBuilder; */
/* invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V */
final String v7 = "checkGpuMemoryLeak size:"; // const-string v7, "checkGpuMemoryLeak size:"
(( java.lang.StringBuilder ) v0 ).append ( v7 ); // invoke-virtual {v0, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
com.miui.server.stability.ScoutDisplayMemoryManager .stringifyKBSize ( v3,v4 );
(( java.lang.StringBuilder ) v0 ).append ( v7 ); // invoke-virtual {v0, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
final String v7 = " monitor threshold = "; // const-string v7, " monitor threshold = "
(( java.lang.StringBuilder ) v0 ).append ( v7 ); // invoke-virtual {v0, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
/* int-to-long v7, v7 */
/* mul-long/2addr v7, v5 */
(( java.lang.StringBuilder ) v0 ).append ( v7, v8 ); // invoke-virtual {v0, v7, v8}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;
final String v7 = "kB gpuType = "; // const-string v7, "kB gpuType = "
(( java.lang.StringBuilder ) v0 ).append ( v7 ); // invoke-virtual {v0, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
/* iget v7, p0, Lcom/miui/server/stability/ScoutDisplayMemoryManager;->gpuType:I */
(( java.lang.StringBuilder ) v0 ).append ( v7 ); // invoke-virtual {v0, v7}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v0 ).toString ( ); // invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
final String v7 = "MIUIScout Memory"; // const-string v7, "MIUIScout Memory"
android.util.Slog .d ( v7,v0 );
/* .line 649 */
} // :cond_1
/* int-to-long v7, v0 */
/* mul-long/2addr v7, v5 */
/* cmp-long v0, v3, v7 */
/* if-lez v0, :cond_2 */
/* .line 650 */
final String v0 = "GpuMemory_leak"; // const-string v0, "GpuMemory_leak"
/* invoke-direct {p0, v0, v3, v4}, Lcom/miui/server/stability/ScoutDisplayMemoryManager;->reportGpuMemoryLeakException(Ljava/lang/String;J)V */
/* .line 651 */
/* .line 653 */
} // :cond_2
/* .line 645 */
} // .end local v3 # "totalGpuMemoru":J
} // :cond_3
} // :goto_0
} // .end method
private static Boolean doPreserveCrimeSceneIfNeed ( java.lang.String p0, Integer p1, Long p2, java.lang.String p3 ) {
/* .locals 10 */
/* .param p0, "leakingProcess" # Ljava/lang/String; */
/* .param p1, "pid" # I */
/* .param p2, "rssKB" # J */
/* .param p4, "leakType" # Ljava/lang/String; */
/* .line 709 */
final String v0 = "MIUIScout Memory"; // const-string v0, "MIUIScout Memory"
/* sget-boolean v1, Landroid/os/Build;->IS_DEBUGGABLE:Z */
int v2 = 0; // const/4 v2, 0x0
/* if-nez v1, :cond_0 */
/* .line 710 */
/* .line 712 */
} // :cond_0
final String v1 = "persist.sys.debug.preserve_scout_memory_leak_scene"; // const-string v1, "persist.sys.debug.preserve_scout_memory_leak_scene"
android.os.SystemProperties .get ( v1 );
final String v3 = ","; // const-string v3, ","
(( java.lang.String ) v1 ).split ( v3 ); // invoke-virtual {v1, v3}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;
/* .line 713 */
/* .local v1, "processList":[Ljava/lang/String; */
final String v3 = "any"; // const-string v3, "any"
/* filled-new-array {v3, p0}, [Ljava/lang/String; */
v3 = com.android.internal.util.ArrayUtils .containsAny ( v1,v3 );
/* if-nez v3, :cond_1 */
/* .line 714 */
/* .line 718 */
} // :cond_1
try { // :try_start_0
final String v3 = "package"; // const-string v3, "package"
/* .line 719 */
android.os.ServiceManager .getService ( v3 );
/* .line 718 */
android.content.pm.IPackageManager$Stub .asInterface ( v3 );
/* .line 720 */
/* .local v4, "pm":Landroid/content/pm/IPackageManager; */
final String v5 = "com.phonetest.stresstest"; // const-string v5, "com.phonetest.stresstest"
int v6 = 2; // const/4 v6, 0x2
int v7 = 0; // const/4 v7, 0x0
int v8 = 0; // const/4 v8, 0x0
final String v9 = "MIUIScout Memory"; // const-string v9, "MIUIScout Memory"
/* invoke-interface/range {v4 ..v9}, Landroid/content/pm/IPackageManager;->setApplicationEnabledSetting(Ljava/lang/String;IIILjava/lang/String;)V */
/* :try_end_0 */
/* .catch Ljava/lang/Exception; {:try_start_0 ..:try_end_0} :catch_0 */
/* .line 725 */
} // .end local v4 # "pm":Landroid/content/pm/IPackageManager;
/* .line 723 */
/* :catch_0 */
/* move-exception v3 */
/* .line 724 */
/* .local v3, "e":Ljava/lang/Exception; */
final String v4 = "Failed to disable MTBF app"; // const-string v4, "Failed to disable MTBF app"
android.util.Slog .d ( v0,v4,v3 );
/* .line 726 */
} // .end local v3 # "e":Ljava/lang/Exception;
} // :goto_0
final String v3 = "com.android.commands.monkey"; // const-string v3, "com.android.commands.monkey"
/* filled-new-array {v3}, [Ljava/lang/String; */
android.os.Process .getPidsForCommands ( v3 );
/* .line 727 */
/* .local v3, "monkeyPids":[I */
if ( v3 != null) { // if-eqz v3, :cond_2
/* .line 728 */
/* array-length v4, v3 */
} // :goto_1
/* if-ge v2, v4, :cond_2 */
/* aget v5, v3, v2 */
/* .line 729 */
/* .local v5, "monkeyPid":I */
/* new-instance v6, Ljava/lang/StringBuilder; */
/* invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V */
final String v7 = "Killing monkey process: pid="; // const-string v7, "Killing monkey process: pid="
(( java.lang.StringBuilder ) v6 ).append ( v7 ); // invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v6 ).append ( v5 ); // invoke-virtual {v6, v5}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v6 ).toString ( ); // invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
android.util.Slog .d ( v0,v6 );
/* .line 731 */
/* const/16 v6, 0x9 */
try { // :try_start_1
android.system.Os .kill ( v5,v6 );
/* :try_end_1 */
/* .catch Ljava/lang/Exception; {:try_start_1 ..:try_end_1} :catch_1 */
/* .line 733 */
/* .line 732 */
/* :catch_1 */
/* move-exception v6 */
/* .line 728 */
} // .end local v5 # "monkeyPid":I
} // :goto_2
/* add-int/lit8 v2, v2, 0x1 */
/* .line 736 */
} // :cond_2
/* new-instance v0, Ljava/lang/StringBuilder; */
/* invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V */
(( java.lang.String ) p4 ).toUpperCase ( ); // invoke-virtual {p4}, Ljava/lang/String;->toUpperCase()Ljava/lang/String;
(( java.lang.StringBuilder ) v0 ).append ( v2 ); // invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
final String v2 = " MEMORY LEAKED"; // const-string v2, " MEMORY LEAKED"
(( java.lang.StringBuilder ) v0 ).append ( v2 ); // invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v0 ).toString ( ); // invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
/* .line 737 */
/* .local v0, "title":Ljava/lang/String; */
/* new-instance v2, Ljava/lang/StringBuilder; */
/* invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V */
(( java.lang.StringBuilder ) v2 ).append ( p0 ); // invoke-virtual {v2, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
final String v4 = ":"; // const-string v4, ":"
(( java.lang.StringBuilder ) v2 ).append ( v4 ); // invoke-virtual {v2, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v2 ).append ( p1 ); // invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;
final String v4 = " consumed "; // const-string v4, " consumed "
(( java.lang.StringBuilder ) v2 ).append ( v4 ); // invoke-virtual {v2, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v2 ).append ( p2, p3 ); // invoke-virtual {v2, p2, p3}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;
final String v4 = "KB memory, please contact the engineers for help ASAP!"; // const-string v4, "KB memory, please contact the engineers for help ASAP!"
(( java.lang.StringBuilder ) v2 ).append ( v4 ); // invoke-virtual {v2, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v2 ).toString ( ); // invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
/* .line 740 */
/* .local v2, "msg":Ljava/lang/String; */
com.miui.server.stability.ScoutDisplayMemoryManager$CrimeScenePreservedDialog .show ( v0,v2 );
/* .line 741 */
int v4 = 1; // const/4 v4, 0x1
} // .end method
public static com.miui.server.stability.ScoutDisplayMemoryManager getInstance ( ) {
/* .locals 1 */
/* .line 136 */
v0 = com.miui.server.stability.ScoutDisplayMemoryManager.displayMemoryManager;
/* if-nez v0, :cond_0 */
/* .line 137 */
/* new-instance v0, Lcom/miui/server/stability/ScoutDisplayMemoryManager; */
/* invoke-direct {v0}, Lcom/miui/server/stability/ScoutDisplayMemoryManager;-><init>()V */
/* .line 139 */
} // :cond_0
v0 = com.miui.server.stability.ScoutDisplayMemoryManager.displayMemoryManager;
} // .end method
private native Long getTotalGpuMemory ( Integer p0 ) {
} // .end method
private Integer getTotalRam ( ) {
/* .locals 6 */
/* .line 242 */
/* iget v0, p0, Lcom/miui/server/stability/ScoutDisplayMemoryManager;->totalRam:I */
/* if-lez v0, :cond_0 */
/* .line 243 */
} // :cond_0
/* const/16 v0, 0x1b */
/* new-array v0, v0, [J */
/* .line 244 */
/* .local v0, "infos":[J */
android.os.Debug .getMemInfo ( v0 );
/* .line 245 */
int v1 = 0; // const/4 v1, 0x0
/* aget-wide v2, v0, v1 */
/* const-wide/32 v4, 0x100000 */
/* div-long/2addr v2, v4 */
/* long-to-int v2, v2 */
/* iput v2, p0, Lcom/miui/server/stability/ScoutDisplayMemoryManager;->totalRam:I */
/* .line 246 */
/* sget-boolean v2, Lcom/miui/server/stability/ScoutDisplayMemoryManager;->debug:Z */
if ( v2 != null) { // if-eqz v2, :cond_1
/* new-instance v2, Ljava/lang/StringBuilder; */
/* invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V */
final String v3 = "getTotalRam total memory "; // const-string v3, "getTotalRam total memory "
(( java.lang.StringBuilder ) v2 ).append ( v3 ); // invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
/* aget-wide v3, v0, v1 */
(( java.lang.StringBuilder ) v2 ).append ( v3, v4 ); // invoke-virtual {v2, v3, v4}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;
final String v2 = " kB, "; // const-string v2, " kB, "
(( java.lang.StringBuilder ) v1 ).append ( v2 ); // invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
/* iget v2, p0, Lcom/miui/server/stability/ScoutDisplayMemoryManager;->totalRam:I */
(( java.lang.StringBuilder ) v1 ).append ( v2 ); // invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;
final String v2 = "GB"; // const-string v2, "GB"
(( java.lang.StringBuilder ) v1 ).append ( v2 ); // invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v1 ).toString ( ); // invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
final String v2 = "MIUIScout Memory"; // const-string v2, "MIUIScout Memory"
android.util.Slog .d ( v2,v1 );
/* .line 248 */
} // :cond_1
/* iget v1, p0, Lcom/miui/server/stability/ScoutDisplayMemoryManager;->totalRam:I */
} // .end method
private void handleReportMisight ( Integer p0, miui.mqsas.sdk.event.ExceptionEvent p1 ) {
/* .locals 5 */
/* .param p1, "errorType" # I */
/* .param p2, "event" # Lmiui/mqsas/sdk/event/ExceptionEvent; */
/* .line 320 */
int v0 = 1; // const/4 v0, 0x1
/* if-ne p1, v0, :cond_0 */
/* .line 321 */
/* new-instance v0, Lcom/miui/misight/MiEvent; */
/* const v1, 0x35b43ba9 */
/* invoke-direct {v0, v1}, Lcom/miui/misight/MiEvent;-><init>(I)V */
/* .line 322 */
/* .local v0, "miEvent":Lcom/miui/misight/MiEvent; */
final String v1 = "DmaBuf"; // const-string v1, "DmaBuf"
(( miui.mqsas.sdk.event.ExceptionEvent ) p2 ).getDetails ( ); // invoke-virtual {p2}, Lmiui/mqsas/sdk/event/ExceptionEvent;->getDetails()Ljava/lang/String;
(( com.miui.misight.MiEvent ) v0 ).addStr ( v1, v2 ); // invoke-virtual {v0, v1, v2}, Lcom/miui/misight/MiEvent;->addStr(Ljava/lang/String;Ljava/lang/String;)Lcom/miui/misight/MiEvent;
/* .line 323 */
} // .end local v0 # "miEvent":Lcom/miui/misight/MiEvent;
} // :cond_0
int v0 = 2; // const/4 v0, 0x2
/* if-ne p1, v0, :cond_1 */
/* .line 324 */
/* new-instance v0, Lcom/miui/misight/MiEvent; */
/* const v1, 0x35b43baa */
/* invoke-direct {v0, v1}, Lcom/miui/misight/MiEvent;-><init>(I)V */
/* .line 325 */
/* .restart local v0 # "miEvent":Lcom/miui/misight/MiEvent; */
final String v1 = "Gpu"; // const-string v1, "Gpu"
(( miui.mqsas.sdk.event.ExceptionEvent ) p2 ).getDetails ( ); // invoke-virtual {p2}, Lmiui/mqsas/sdk/event/ExceptionEvent;->getDetails()Ljava/lang/String;
(( com.miui.misight.MiEvent ) v0 ).addStr ( v1, v2 ); // invoke-virtual {v0, v1, v2}, Lcom/miui/misight/MiEvent;->addStr(Ljava/lang/String;Ljava/lang/String;)Lcom/miui/misight/MiEvent;
/* .line 330 */
} // :goto_0
final String v1 = "Summary"; // const-string v1, "Summary"
(( miui.mqsas.sdk.event.ExceptionEvent ) p2 ).getSummary ( ); // invoke-virtual {p2}, Lmiui/mqsas/sdk/event/ExceptionEvent;->getSummary()Ljava/lang/String;
(( com.miui.misight.MiEvent ) v0 ).addStr ( v1, v2 ); // invoke-virtual {v0, v1, v2}, Lcom/miui/misight/MiEvent;->addStr(Ljava/lang/String;Ljava/lang/String;)Lcom/miui/misight/MiEvent;
/* .line 331 */
(( miui.mqsas.sdk.event.ExceptionEvent ) p2 ).getPackageName ( ); // invoke-virtual {p2}, Lmiui/mqsas/sdk/event/ExceptionEvent;->getPackageName()Ljava/lang/String;
final String v3 = "PackageName"; // const-string v3, "PackageName"
(( com.miui.misight.MiEvent ) v1 ).addStr ( v3, v2 ); // invoke-virtual {v1, v3, v2}, Lcom/miui/misight/MiEvent;->addStr(Ljava/lang/String;Ljava/lang/String;)Lcom/miui/misight/MiEvent;
/* .line 332 */
java.lang.System .currentTimeMillis ( );
/* move-result-wide v2 */
final String v4 = "CurrentTime"; // const-string v4, "CurrentTime"
(( com.miui.misight.MiEvent ) v1 ).addLong ( v4, v2, v3 ); // invoke-virtual {v1, v4, v2, v3}, Lcom/miui/misight/MiEvent;->addLong(Ljava/lang/String;J)Lcom/miui/misight/MiEvent;
/* .line 333 */
com.miui.misight.MiSight .sendEvent ( v0 );
/* .line 334 */
return;
/* .line 327 */
} // .end local v0 # "miEvent":Lcom/miui/misight/MiEvent;
} // :cond_1
final String v0 = "ScoutDisplayMemoryManager"; // const-string v0, "ScoutDisplayMemoryManager"
final String v1 = "handleReportMisight: invalid errorType"; // const-string v1, "handleReportMisight: invalid errorType"
android.util.Slog .w ( v0,v1 );
/* .line 328 */
return;
} // .end method
private Integer initGpuType ( ) {
/* .locals 3 */
/* .line 147 */
/* new-instance v0, Ljava/io/File; */
final String v1 = "/sys/class/kgsl/kgsl/page_alloc"; // const-string v1, "/sys/class/kgsl/kgsl/page_alloc"
/* invoke-direct {v0, v1}, Ljava/io/File;-><init>(Ljava/lang/String;)V */
/* .line 148 */
/* .local v0, "kgslFile":Ljava/io/File; */
v1 = (( java.io.File ) v0 ).exists ( ); // invoke-virtual {v0}, Ljava/io/File;->exists()Z
if ( v1 != null) { // if-eqz v1, :cond_0
/* .line 149 */
int v1 = 1; // const/4 v1, 0x1
/* .line 151 */
} // :cond_0
/* new-instance v1, Ljava/io/File; */
final String v2 = "/proc/mtk_mali/gpu_memory"; // const-string v2, "/proc/mtk_mali/gpu_memory"
/* invoke-direct {v1, v2}, Ljava/io/File;-><init>(Ljava/lang/String;)V */
/* .line 152 */
/* .local v1, "maliFile":Ljava/io/File; */
v2 = (( java.io.File ) v1 ).exists ( ); // invoke-virtual {v1}, Ljava/io/File;->exists()Z
if ( v2 != null) { // if-eqz v2, :cond_1
/* .line 153 */
int v2 = 2; // const/4 v2, 0x2
/* .line 155 */
} // :cond_1
int v2 = 0; // const/4 v2, 0x0
} // .end method
private void lambda$handleReportMqs$1 ( java.lang.String p0, com.miui.server.stability.ScoutDisplayMemoryManager$DiaplayMemoryErrorInfo p1 ) { //synthethic
/* .locals 11 */
/* .param p1, "filePath" # Ljava/lang/String; */
/* .param p2, "errorInfo" # Lcom/miui/server/stability/ScoutDisplayMemoryManager$DiaplayMemoryErrorInfo; */
/* .line 292 */
/* new-instance v0, Lmiui/mqsas/sdk/event/GeneralExceptionEvent; */
/* invoke-direct {v0}, Lmiui/mqsas/sdk/event/GeneralExceptionEvent;-><init>()V */
/* .line 293 */
/* .local v0, "event":Lmiui/mqsas/sdk/event/GeneralExceptionEvent; */
int v1 = 1; // const/4 v1, 0x1
/* new-array v2, v1, [Ljava/lang/String; */
if ( p1 != null) { // if-eqz p1, :cond_0
/* move-object v3, p1 */
} // :cond_0
final String v3 = ""; // const-string v3, ""
} // :goto_0
int v4 = 0; // const/4 v4, 0x0
/* aput-object v3, v2, v4 */
java.util.Arrays .asList ( v2 );
/* .line 294 */
/* .local v2, "files":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;" */
/* const-wide/16 v3, 0x0 */
/* .line 295 */
/* .local v3, "thresholdSize":J */
/* new-instance v5, Ljava/lang/StringBuilder; */
/* invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V */
(( com.miui.server.stability.ScoutDisplayMemoryManager$DiaplayMemoryErrorInfo ) p2 ).getName ( ); // invoke-virtual {p2}, Lcom/miui/server/stability/ScoutDisplayMemoryManager$DiaplayMemoryErrorInfo;->getName()Ljava/lang/String;
(( java.lang.StringBuilder ) v5 ).append ( v6 ); // invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
final String v6 = ", occurring "; // const-string v6, ", occurring "
(( java.lang.StringBuilder ) v5 ).append ( v6 ); // invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( com.miui.server.stability.ScoutDisplayMemoryManager$DiaplayMemoryErrorInfo ) p2 ).getReason ( ); // invoke-virtual {p2}, Lcom/miui/server/stability/ScoutDisplayMemoryManager$DiaplayMemoryErrorInfo;->getReason()Ljava/lang/String;
(( java.lang.StringBuilder ) v5 ).append ( v6 ); // invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
final String v6 = " OOM"; // const-string v6, " OOM"
(( java.lang.StringBuilder ) v5 ).append ( v6 ); // invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v5 ).toString ( ); // invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
/* .line 296 */
/* .local v5, "sum":Ljava/lang/String; */
v6 = (( com.miui.server.stability.ScoutDisplayMemoryManager$DiaplayMemoryErrorInfo ) p2 ).getType ( ); // invoke-virtual {p2}, Lcom/miui/server/stability/ScoutDisplayMemoryManager$DiaplayMemoryErrorInfo;->getType()I
/* const-wide/16 v7, 0x400 */
/* if-ne v6, v1, :cond_1 */
/* .line 297 */
/* const/16 v1, 0x1a8 */
(( miui.mqsas.sdk.event.GeneralExceptionEvent ) v0 ).setType ( v1 ); // invoke-virtual {v0, v1}, Lmiui/mqsas/sdk/event/GeneralExceptionEvent;->setType(I)V
/* .line 298 */
/* int-to-long v9, v1 */
/* mul-long/2addr v9, v7 */
} // .end local v3 # "thresholdSize":J
/* .local v9, "thresholdSize":J */
/* .line 299 */
} // .end local v9 # "thresholdSize":J
/* .restart local v3 # "thresholdSize":J */
} // :cond_1
v1 = (( com.miui.server.stability.ScoutDisplayMemoryManager$DiaplayMemoryErrorInfo ) p2 ).getType ( ); // invoke-virtual {p2}, Lcom/miui/server/stability/ScoutDisplayMemoryManager$DiaplayMemoryErrorInfo;->getType()I
int v6 = 2; // const/4 v6, 0x2
/* if-ne v1, v6, :cond_2 */
/* .line 300 */
/* const/16 v1, 0x1a9 */
(( miui.mqsas.sdk.event.GeneralExceptionEvent ) v0 ).setType ( v1 ); // invoke-virtual {v0, v1}, Lmiui/mqsas/sdk/event/GeneralExceptionEvent;->setType(I)V
/* .line 301 */
/* int-to-long v9, v1 */
/* mul-long/2addr v9, v7 */
/* .line 305 */
} // .end local v3 # "thresholdSize":J
/* .restart local v9 # "thresholdSize":J */
} // :goto_1
(( miui.mqsas.sdk.event.GeneralExceptionEvent ) v0 ).setSummary ( v5 ); // invoke-virtual {v0, v5}, Lmiui/mqsas/sdk/event/GeneralExceptionEvent;->setSummary(Ljava/lang/String;)V
/* .line 306 */
java.lang.System .currentTimeMillis ( );
/* move-result-wide v3 */
(( miui.mqsas.sdk.event.GeneralExceptionEvent ) v0 ).setTimeStamp ( v3, v4 ); // invoke-virtual {v0, v3, v4}, Lmiui/mqsas/sdk/event/GeneralExceptionEvent;->setTimeStamp(J)V
/* .line 307 */
(( com.miui.server.stability.ScoutDisplayMemoryManager$DiaplayMemoryErrorInfo ) p2 ).getName ( ); // invoke-virtual {p2}, Lcom/miui/server/stability/ScoutDisplayMemoryManager$DiaplayMemoryErrorInfo;->getName()Ljava/lang/String;
(( miui.mqsas.sdk.event.GeneralExceptionEvent ) v0 ).setPackageName ( v1 ); // invoke-virtual {v0, v1}, Lmiui/mqsas/sdk/event/GeneralExceptionEvent;->setPackageName(Ljava/lang/String;)V
/* .line 308 */
(( miui.mqsas.sdk.event.GeneralExceptionEvent ) v0 ).setExtraFiles ( v2 ); // invoke-virtual {v0, v2}, Lmiui/mqsas/sdk/event/GeneralExceptionEvent;->setExtraFiles(Ljava/util/List;)V
/* .line 309 */
/* new-instance v1, Ljava/lang/StringBuilder; */
/* invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V */
(( java.lang.StringBuilder ) v1 ).append ( v5 ); // invoke-virtual {v1, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
final String v3 = "\ntotal_threshold: "; // const-string v3, "\ntotal_threshold: "
(( java.lang.StringBuilder ) v1 ).append ( v3 ); // invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v1 ).append ( v9, v10 ); // invoke-virtual {v1, v9, v10}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;
final String v3 = "(kB)\nmemory_app_package: "; // const-string v3, "(kB)\nmemory_app_package: "
(( java.lang.StringBuilder ) v1 ).append ( v3 ); // invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
/* .line 311 */
(( com.miui.server.stability.ScoutDisplayMemoryManager$DiaplayMemoryErrorInfo ) p2 ).getName ( ); // invoke-virtual {p2}, Lcom/miui/server/stability/ScoutDisplayMemoryManager$DiaplayMemoryErrorInfo;->getName()Ljava/lang/String;
(( java.lang.StringBuilder ) v1 ).append ( v3 ); // invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
final String v3 = "\napp_threshold: "; // const-string v3, "\napp_threshold: "
(( java.lang.StringBuilder ) v1 ).append ( v3 ); // invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
/* .line 312 */
(( com.miui.server.stability.ScoutDisplayMemoryManager$DiaplayMemoryErrorInfo ) p2 ).getThreshold ( ); // invoke-virtual {p2}, Lcom/miui/server/stability/ScoutDisplayMemoryManager$DiaplayMemoryErrorInfo;->getThreshold()J
/* move-result-wide v3 */
(( java.lang.StringBuilder ) v1 ).append ( v3, v4 ); // invoke-virtual {v1, v3, v4}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;
final String v3 = "(kB)\n"; // const-string v3, "(kB)\n"
(( java.lang.StringBuilder ) v1 ).append ( v3 ); // invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v1 ).toString ( ); // invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
/* .line 309 */
(( miui.mqsas.sdk.event.GeneralExceptionEvent ) v0 ).setDetails ( v1 ); // invoke-virtual {v0, v1}, Lmiui/mqsas/sdk/event/GeneralExceptionEvent;->setDetails(Ljava/lang/String;)V
/* .line 313 */
miui.mqsas.sdk.MQSEventManagerDelegate .getInstance ( );
(( miui.mqsas.sdk.MQSEventManagerDelegate ) v1 ).reportGeneralException ( v0 ); // invoke-virtual {v1, v0}, Lmiui/mqsas/sdk/MQSEventManagerDelegate;->reportGeneralException(Lmiui/mqsas/sdk/event/GeneralExceptionEvent;)Z
/* .line 314 */
v1 = (( com.miui.server.stability.ScoutDisplayMemoryManager$DiaplayMemoryErrorInfo ) p2 ).getType ( ); // invoke-virtual {p2}, Lcom/miui/server/stability/ScoutDisplayMemoryManager$DiaplayMemoryErrorInfo;->getType()I
/* invoke-direct {p0, v1, v0}, Lcom/miui/server/stability/ScoutDisplayMemoryManager;->handleReportMisight(ILmiui/mqsas/sdk/event/ExceptionEvent;)V */
/* .line 315 */
return;
/* .line 303 */
} // .end local v9 # "thresholdSize":J
/* .restart local v3 # "thresholdSize":J */
} // :cond_2
return;
} // .end method
private void lambda$reportDisplayMemoryLeakEvent$0 ( com.miui.server.stability.ScoutDisplayMemoryManager$DiaplayMemoryErrorInfo p0 ) { //synthethic
/* .locals 5 */
/* .param p1, "errorInfo" # Lcom/miui/server/stability/ScoutDisplayMemoryManager$DiaplayMemoryErrorInfo; */
/* .line 256 */
/* new-instance v0, Landroid/content/Intent; */
final String v1 = "onetrack.action.TRACK_EVENT"; // const-string v1, "onetrack.action.TRACK_EVENT"
/* invoke-direct {v0, v1}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V */
/* .line 257 */
/* .local v0, "intent":Landroid/content/Intent; */
final String v1 = "com.miui.analytics"; // const-string v1, "com.miui.analytics"
(( android.content.Intent ) v0 ).setPackage ( v1 ); // invoke-virtual {v0, v1}, Landroid/content/Intent;->setPackage(Ljava/lang/String;)Landroid/content/Intent;
/* .line 258 */
final String v1 = "APP_ID"; // const-string v1, "APP_ID"
final String v2 = "31000000454"; // const-string v2, "31000000454"
(( android.content.Intent ) v0 ).putExtra ( v1, v2 ); // invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;
/* .line 259 */
final String v1 = "PACKAGE"; // const-string v1, "PACKAGE"
final String v2 = "android"; // const-string v2, "android"
(( android.content.Intent ) v0 ).putExtra ( v1, v2 ); // invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;
/* .line 260 */
v1 = (( com.miui.server.stability.ScoutDisplayMemoryManager$DiaplayMemoryErrorInfo ) p1 ).getType ( ); // invoke-virtual {p1}, Lcom/miui/server/stability/ScoutDisplayMemoryManager$DiaplayMemoryErrorInfo;->getType()I
final String v2 = "EVENT_NAME"; // const-string v2, "EVENT_NAME"
int v3 = 1; // const/4 v3, 0x1
/* if-ne v1, v3, :cond_0 */
/* .line 261 */
final String v1 = "dmabuf_leak"; // const-string v1, "dmabuf_leak"
(( android.content.Intent ) v0 ).putExtra ( v2, v1 ); // invoke-virtual {v0, v2, v1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;
/* .line 262 */
(( com.miui.server.stability.ScoutDisplayMemoryManager$DiaplayMemoryErrorInfo ) p1 ).getTotalSize ( ); // invoke-virtual {p1}, Lcom/miui/server/stability/ScoutDisplayMemoryManager$DiaplayMemoryErrorInfo;->getTotalSize()J
/* move-result-wide v1 */
java.lang.String .valueOf ( v1,v2 );
final String v2 = "dmabuf_total_size"; // const-string v2, "dmabuf_total_size"
(( android.content.Intent ) v0 ).putExtra ( v2, v1 ); // invoke-virtual {v0, v2, v1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;
/* .line 263 */
} // :cond_0
v1 = (( com.miui.server.stability.ScoutDisplayMemoryManager$DiaplayMemoryErrorInfo ) p1 ).getType ( ); // invoke-virtual {p1}, Lcom/miui/server/stability/ScoutDisplayMemoryManager$DiaplayMemoryErrorInfo;->getType()I
int v4 = 2; // const/4 v4, 0x2
/* if-ne v1, v4, :cond_2 */
/* .line 264 */
final String v1 = "kgsl_leak"; // const-string v1, "kgsl_leak"
(( android.content.Intent ) v0 ).putExtra ( v2, v1 ); // invoke-virtual {v0, v2, v1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;
/* .line 265 */
(( com.miui.server.stability.ScoutDisplayMemoryManager$DiaplayMemoryErrorInfo ) p1 ).getTotalSize ( ); // invoke-virtual {p1}, Lcom/miui/server/stability/ScoutDisplayMemoryManager$DiaplayMemoryErrorInfo;->getTotalSize()J
/* move-result-wide v1 */
java.lang.String .valueOf ( v1,v2 );
final String v2 = "kgsl_total_size"; // const-string v2, "kgsl_total_size"
(( android.content.Intent ) v0 ).putExtra ( v2, v1 ); // invoke-virtual {v0, v2, v1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;
/* .line 269 */
} // :goto_0
final String v1 = "memory_total_size"; // const-string v1, "memory_total_size"
v2 = /* invoke-direct {p0}, Lcom/miui/server/stability/ScoutDisplayMemoryManager;->getTotalRam()I */
(( android.content.Intent ) v0 ).putExtra ( v1, v2 ); // invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;
/* .line 270 */
final String v1 = "memory_app_package"; // const-string v1, "memory_app_package"
(( com.miui.server.stability.ScoutDisplayMemoryManager$DiaplayMemoryErrorInfo ) p1 ).getName ( ); // invoke-virtual {p1}, Lcom/miui/server/stability/ScoutDisplayMemoryManager$DiaplayMemoryErrorInfo;->getName()Ljava/lang/String;
(( android.content.Intent ) v0 ).putExtra ( v1, v2 ); // invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;
/* .line 271 */
(( com.miui.server.stability.ScoutDisplayMemoryManager$DiaplayMemoryErrorInfo ) p1 ).getRss ( ); // invoke-virtual {p1}, Lcom/miui/server/stability/ScoutDisplayMemoryManager$DiaplayMemoryErrorInfo;->getRss()J
/* move-result-wide v1 */
java.lang.String .valueOf ( v1,v2 );
final String v2 = "memory_app_size"; // const-string v2, "memory_app_size"
(( android.content.Intent ) v0 ).putExtra ( v2, v1 ); // invoke-virtual {v0, v2, v1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;
/* .line 272 */
v1 = (( com.miui.server.stability.ScoutDisplayMemoryManager$DiaplayMemoryErrorInfo ) p1 ).getOomadj ( ); // invoke-virtual {p1}, Lcom/miui/server/stability/ScoutDisplayMemoryManager$DiaplayMemoryErrorInfo;->getOomadj()I
java.lang.String .valueOf ( v1 );
final String v2 = "memory_app_adj"; // const-string v2, "memory_app_adj"
(( android.content.Intent ) v0 ).putExtra ( v2, v1 ); // invoke-virtual {v0, v2, v1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;
/* .line 273 */
v1 = (( com.miui.server.stability.ScoutDisplayMemoryManager$DiaplayMemoryErrorInfo ) p1 ).getAction ( ); // invoke-virtual {p1}, Lcom/miui/server/stability/ScoutDisplayMemoryManager$DiaplayMemoryErrorInfo;->getAction()I
java.lang.String .valueOf ( v1 );
final String v2 = "resume_action"; // const-string v2, "resume_action"
(( android.content.Intent ) v0 ).putExtra ( v2, v1 ); // invoke-virtual {v0, v2, v1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;
/* .line 274 */
(( android.content.Intent ) v0 ).setFlags ( v3 ); // invoke-virtual {v0, v3}, Landroid/content/Intent;->setFlags(I)Landroid/content/Intent;
/* .line 275 */
/* sget-boolean v1, Lcom/miui/server/stability/ScoutDisplayMemoryManager;->debug:Z */
if ( v1 != null) { // if-eqz v1, :cond_1
/* .line 276 */
/* new-instance v1, Ljava/lang/StringBuilder; */
/* invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V */
final String v2 = "report type="; // const-string v2, "report type="
(( java.lang.StringBuilder ) v1 ).append ( v2 ); // invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
v2 = (( com.miui.server.stability.ScoutDisplayMemoryManager$DiaplayMemoryErrorInfo ) p1 ).getType ( ); // invoke-virtual {p1}, Lcom/miui/server/stability/ScoutDisplayMemoryManager$DiaplayMemoryErrorInfo;->getType()I
(( java.lang.StringBuilder ) v1 ).append ( v2 ); // invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;
final String v2 = " memory_total_size="; // const-string v2, " memory_total_size="
(( java.lang.StringBuilder ) v1 ).append ( v2 ); // invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
v2 = /* invoke-direct {p0}, Lcom/miui/server/stability/ScoutDisplayMemoryManager;->getTotalRam()I */
(( java.lang.StringBuilder ) v1 ).append ( v2 ); // invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;
final String v2 = " total_size="; // const-string v2, " total_size="
(( java.lang.StringBuilder ) v1 ).append ( v2 ); // invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
/* .line 277 */
(( com.miui.server.stability.ScoutDisplayMemoryManager$DiaplayMemoryErrorInfo ) p1 ).getTotalSize ( ); // invoke-virtual {p1}, Lcom/miui/server/stability/ScoutDisplayMemoryManager$DiaplayMemoryErrorInfo;->getTotalSize()J
/* move-result-wide v2 */
(( java.lang.StringBuilder ) v1 ).append ( v2, v3 ); // invoke-virtual {v1, v2, v3}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;
final String v2 = " memory_app_package="; // const-string v2, " memory_app_package="
(( java.lang.StringBuilder ) v1 ).append ( v2 ); // invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( com.miui.server.stability.ScoutDisplayMemoryManager$DiaplayMemoryErrorInfo ) p1 ).getName ( ); // invoke-virtual {p1}, Lcom/miui/server/stability/ScoutDisplayMemoryManager$DiaplayMemoryErrorInfo;->getName()Ljava/lang/String;
(( java.lang.StringBuilder ) v1 ).append ( v2 ); // invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
final String v2 = " memory_app_size ="; // const-string v2, " memory_app_size ="
(( java.lang.StringBuilder ) v1 ).append ( v2 ); // invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
/* .line 278 */
(( com.miui.server.stability.ScoutDisplayMemoryManager$DiaplayMemoryErrorInfo ) p1 ).getRss ( ); // invoke-virtual {p1}, Lcom/miui/server/stability/ScoutDisplayMemoryManager$DiaplayMemoryErrorInfo;->getRss()J
/* move-result-wide v2 */
(( java.lang.StringBuilder ) v1 ).append ( v2, v3 ); // invoke-virtual {v1, v2, v3}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;
final String v2 = " memory_app_adj="; // const-string v2, " memory_app_adj="
(( java.lang.StringBuilder ) v1 ).append ( v2 ); // invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
v2 = (( com.miui.server.stability.ScoutDisplayMemoryManager$DiaplayMemoryErrorInfo ) p1 ).getOomadj ( ); // invoke-virtual {p1}, Lcom/miui/server/stability/ScoutDisplayMemoryManager$DiaplayMemoryErrorInfo;->getOomadj()I
(( java.lang.StringBuilder ) v1 ).append ( v2 ); // invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;
final String v2 = " action = "; // const-string v2, " action = "
(( java.lang.StringBuilder ) v1 ).append ( v2 ); // invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
/* .line 279 */
v2 = (( com.miui.server.stability.ScoutDisplayMemoryManager$DiaplayMemoryErrorInfo ) p1 ).getAction ( ); // invoke-virtual {p1}, Lcom/miui/server/stability/ScoutDisplayMemoryManager$DiaplayMemoryErrorInfo;->getAction()I
(( java.lang.StringBuilder ) v1 ).append ( v2 ); // invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v1 ).toString ( ); // invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
/* .line 276 */
final String v2 = "MIUIScout Memory"; // const-string v2, "MIUIScout Memory"
android.util.Slog .d ( v2,v1 );
/* .line 282 */
} // :cond_1
try { // :try_start_0
v1 = this.mContext;
v2 = android.os.UserHandle.CURRENT;
(( android.content.Context ) v1 ).startServiceAsUser ( v0, v2 ); // invoke-virtual {v1, v0, v2}, Landroid/content/Context;->startServiceAsUser(Landroid/content/Intent;Landroid/os/UserHandle;)Landroid/content/ComponentName;
/* :try_end_0 */
/* .catch Ljava/lang/Exception; {:try_start_0 ..:try_end_0} :catch_0 */
/* .line 285 */
/* .line 283 */
/* :catch_0 */
/* move-exception v1 */
/* .line 284 */
/* .local v1, "e":Ljava/lang/Exception; */
final String v2 = "ScoutDisplayMemoryManager"; // const-string v2, "ScoutDisplayMemoryManager"
final String v3 = "Upload onetrack exception!"; // const-string v3, "Upload onetrack exception!"
android.util.Slog .e ( v2,v3,v1 );
/* .line 286 */
} // .end local v1 # "e":Ljava/lang/Exception;
} // :goto_1
return;
/* .line 267 */
} // :cond_2
return;
} // .end method
static Boolean preserveCrimeSceneIfNeed ( com.miui.server.stability.ScoutDisplayMemoryManager$DiaplayMemoryErrorInfo p0 ) {
/* .locals 6 */
/* .param p0, "errorInfo" # Lcom/miui/server/stability/ScoutDisplayMemoryManager$DiaplayMemoryErrorInfo; */
/* .line 700 */
(( com.miui.server.stability.ScoutDisplayMemoryManager$DiaplayMemoryErrorInfo ) p0 ).getName ( ); // invoke-virtual {p0}, Lcom/miui/server/stability/ScoutDisplayMemoryManager$DiaplayMemoryErrorInfo;->getName()Ljava/lang/String;
/* .line 701 */
/* .local v0, "leakingProcess":Ljava/lang/String; */
v1 = (( com.miui.server.stability.ScoutDisplayMemoryManager$DiaplayMemoryErrorInfo ) p0 ).getPid ( ); // invoke-virtual {p0}, Lcom/miui/server/stability/ScoutDisplayMemoryManager$DiaplayMemoryErrorInfo;->getPid()I
/* .line 702 */
/* .local v1, "pid":I */
(( com.miui.server.stability.ScoutDisplayMemoryManager$DiaplayMemoryErrorInfo ) p0 ).getRss ( ); // invoke-virtual {p0}, Lcom/miui/server/stability/ScoutDisplayMemoryManager$DiaplayMemoryErrorInfo;->getRss()J
/* move-result-wide v2 */
/* .line 703 */
/* .local v2, "rss":J */
(( com.miui.server.stability.ScoutDisplayMemoryManager$DiaplayMemoryErrorInfo ) p0 ).getReason ( ); // invoke-virtual {p0}, Lcom/miui/server/stability/ScoutDisplayMemoryManager$DiaplayMemoryErrorInfo;->getReason()Ljava/lang/String;
/* .line 704 */
/* .local v4, "leakType":Ljava/lang/String; */
v5 = com.miui.server.stability.ScoutDisplayMemoryManager .doPreserveCrimeSceneIfNeed ( v0,v1,v2,v3,v4 );
} // .end method
private native com.miui.server.stability.DmaBufUsageInfo readDmabufInfo ( ) {
} // .end method
private native com.miui.server.stability.GpuMemoryUsageInfo readGpuMemoryInfo ( Integer p0 ) {
} // .end method
private void reportDmabufLeakException ( java.lang.String p0, Long p1 ) {
/* .locals 4 */
/* .param p1, "reason" # Ljava/lang/String; */
/* .param p2, "totalSizeKB" # J */
/* .line 438 */
v0 = this.isBusy;
int v1 = 0; // const/4 v1, 0x0
int v2 = 1; // const/4 v2, 0x1
v0 = (( java.util.concurrent.atomic.AtomicBoolean ) v0 ).compareAndSet ( v1, v2 ); // invoke-virtual {v0, v1, v2}, Ljava/util/concurrent/atomic/AtomicBoolean;->compareAndSet(ZZ)Z
if ( v0 != null) { // if-eqz v0, :cond_0
/* .line 439 */
/* new-instance v0, Ljava/lang/Thread; */
/* new-instance v1, Lcom/miui/server/stability/ScoutDisplayMemoryManager$3; */
/* invoke-direct {v1, p0, p2, p3, p1}, Lcom/miui/server/stability/ScoutDisplayMemoryManager$3;-><init>(Lcom/miui/server/stability/ScoutDisplayMemoryManager;JLjava/lang/String;)V */
/* new-instance v2, Ljava/lang/StringBuilder; */
/* invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V */
(( java.lang.StringBuilder ) v2 ).append ( p1 ); // invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
final String v3 = "-"; // const-string v3, "-"
(( java.lang.StringBuilder ) v2 ).append ( v3 ); // invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
/* .line 527 */
com.miui.server.stability.ScoutDisplayMemoryManager .stringifyKBSize ( p2,p3 );
(( java.lang.StringBuilder ) v2 ).append ( v3 ); // invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v2 ).toString ( ); // invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
/* invoke-direct {v0, v1, v2}, Ljava/lang/Thread;-><init>(Ljava/lang/Runnable;Ljava/lang/String;)V */
(( java.lang.Thread ) v0 ).start ( ); // invoke-virtual {v0}, Ljava/lang/Thread;->start()V
/* .line 529 */
} // :cond_0
final String v0 = "MIUIScout Memory"; // const-string v0, "MIUIScout Memory"
final String v1 = "Is Busy! skip report Dma-buf Leak Exception"; // const-string v1, "Is Busy! skip report Dma-buf Leak Exception"
android.util.Slog .d ( v0,v1 );
/* .line 531 */
} // :goto_0
return;
} // .end method
private void reportGpuMemoryLeakException ( java.lang.String p0, Long p1 ) {
/* .locals 4 */
/* .param p1, "reason" # Ljava/lang/String; */
/* .param p2, "totalSizeKB" # J */
/* .line 534 */
v0 = this.isBusy;
int v1 = 0; // const/4 v1, 0x0
int v2 = 1; // const/4 v2, 0x1
v0 = (( java.util.concurrent.atomic.AtomicBoolean ) v0 ).compareAndSet ( v1, v2 ); // invoke-virtual {v0, v1, v2}, Ljava/util/concurrent/atomic/AtomicBoolean;->compareAndSet(ZZ)Z
if ( v0 != null) { // if-eqz v0, :cond_0
/* .line 535 */
/* new-instance v0, Ljava/lang/Thread; */
/* new-instance v1, Lcom/miui/server/stability/ScoutDisplayMemoryManager$4; */
/* invoke-direct {v1, p0, p2, p3, p1}, Lcom/miui/server/stability/ScoutDisplayMemoryManager$4;-><init>(Lcom/miui/server/stability/ScoutDisplayMemoryManager;JLjava/lang/String;)V */
/* new-instance v2, Ljava/lang/StringBuilder; */
/* invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V */
(( java.lang.StringBuilder ) v2 ).append ( p1 ); // invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
final String v3 = "-"; // const-string v3, "-"
(( java.lang.StringBuilder ) v2 ).append ( v3 ); // invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
/* .line 602 */
com.miui.server.stability.ScoutDisplayMemoryManager .stringifyKBSize ( p2,p3 );
(( java.lang.StringBuilder ) v2 ).append ( v3 ); // invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v2 ).toString ( ); // invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
/* invoke-direct {v0, v1, v2}, Ljava/lang/Thread;-><init>(Ljava/lang/Runnable;Ljava/lang/String;)V */
(( java.lang.Thread ) v0 ).start ( ); // invoke-virtual {v0}, Ljava/lang/Thread;->start()V
/* .line 604 */
} // :cond_0
final String v0 = "MIUIScout Memory"; // const-string v0, "MIUIScout Memory"
final String v1 = "Is Busy! skip report Gpu Memory Leak Exception"; // const-string v1, "Is Busy! skip report Gpu Memory Leak Exception"
android.util.Slog .d ( v0,v1 );
/* .line 606 */
} // :goto_0
return;
} // .end method
private void resumeMemLeak ( com.miui.server.stability.ScoutDisplayMemoryManager$DiaplayMemoryErrorInfo p0 ) {
/* .locals 25 */
/* .param p1, "errorInfo" # Lcom/miui/server/stability/ScoutDisplayMemoryManager$DiaplayMemoryErrorInfo; */
/* .line 361 */
/* move-object/from16 v0, p0 */
/* move-object/from16 v1, p1 */
v2 = /* invoke-virtual/range {p1 ..p1}, Lcom/miui/server/stability/ScoutDisplayMemoryManager$DiaplayMemoryErrorInfo;->getPid()I */
/* .line 362 */
/* .local v2, "pid":I */
v3 = /* invoke-virtual/range {p1 ..p1}, Lcom/miui/server/stability/ScoutDisplayMemoryManager$DiaplayMemoryErrorInfo;->getOomadj()I */
/* .line 363 */
/* .local v3, "adj":I */
/* invoke-virtual/range {p1 ..p1}, Lcom/miui/server/stability/ScoutDisplayMemoryManager$DiaplayMemoryErrorInfo;->getName()Ljava/lang/String; */
/* .line 364 */
/* .local v4, "name":Ljava/lang/String; */
/* invoke-virtual/range {p1 ..p1}, Lcom/miui/server/stability/ScoutDisplayMemoryManager$DiaplayMemoryErrorInfo;->getReason()Ljava/lang/String; */
/* .line 365 */
/* .local v5, "reason":Ljava/lang/String; */
/* invoke-virtual/range {p1 ..p1}, Lcom/miui/server/stability/ScoutDisplayMemoryManager$DiaplayMemoryErrorInfo;->getThreshold()J */
/* move-result-wide v6 */
/* .line 366 */
/* .local v6, "threshold":J */
/* invoke-virtual/range {p1 ..p1}, Lcom/miui/server/stability/ScoutDisplayMemoryManager$DiaplayMemoryErrorInfo;->getRss()J */
/* move-result-wide v8 */
/* .line 367 */
/* .local v8, "size":J */
final String v11 = "("; // const-string v11, "("
final String v12 = "MIUIScout Memory"; // const-string v12, "MIUIScout Memory"
/* if-lez v2, :cond_d */
v13 = android.os.Process .getThreadGroupLeader ( v2 );
/* if-ne v13, v2, :cond_d */
/* .line 368 */
final String v15 = ") used "; // const-string v15, ") used "
final String v10 = " OOM"; // const-string v10, " OOM"
final String v13 = "kB), occurring "; // const-string v13, "kB), occurring "
final String v14 = " is greater than the monitoring threshold("; // const-string v14, " is greater than the monitoring threshold("
/* move-object/from16 v19, v11 */
final String v11 = "kB "; // const-string v11, "kB "
/* move-object/from16 v20, v4 */
} // .end local v4 # "name":Ljava/lang/String;
/* .local v20, "name":Ljava/lang/String; */
/* const/16 v4, -0x384 */
/* if-ne v3, v4, :cond_1 */
v4 = android.os.Process .myPid ( );
/* if-ne v2, v4, :cond_1 */
/* .line 369 */
/* new-instance v4, Ljava/lang/StringBuilder; */
/* invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V */
/* move/from16 v21, v3 */
} // .end local v3 # "adj":I
/* .local v21, "adj":I */
/* const-string/jumbo v3, "system_server(" */
(( java.lang.StringBuilder ) v4 ).append ( v3 ); // invoke-virtual {v4, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v3 ).append ( v2 ); // invoke-virtual {v3, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v3 ).append ( v15 ); // invoke-virtual {v3, v15}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v3 ).append ( v8, v9 ); // invoke-virtual {v3, v8, v9}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v3 ).append ( v11 ); // invoke-virtual {v3, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v3 ).append ( v5 ); // invoke-virtual {v3, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v3 ).append ( v14 ); // invoke-virtual {v3, v14}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v3 ).append ( v6, v7 ); // invoke-virtual {v3, v6, v7}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v3 ).append ( v13 ); // invoke-virtual {v3, v13}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v3 ).append ( v5 ); // invoke-virtual {v3, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v3 ).append ( v10 ); // invoke-virtual {v3, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v3 ).toString ( ); // invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
android.util.Slog .e ( v12,v3 );
/* .line 372 */
v3 = miui.mqsas.scout.ScoutUtils .isLibraryTest ( );
/* if-nez v3, :cond_0 */
/* move-object v0, v1 */
/* move-object/from16 v15, v20 */
/* move/from16 v23, v21 */
/* goto/16 :goto_6 */
/* .line 373 */
} // :cond_0
android.view.SurfaceControlStub .getInstance ( );
/* check-cast v3, Landroid/view/SurfaceControlImpl; */
/* .line 374 */
/* .local v3, "scImpl":Landroid/view/SurfaceControlImpl; */
int v4 = 0; // const/4 v4, 0x0
(( android.view.SurfaceControlImpl ) v3 ).startAsyncDumpIfNeed ( v4 ); // invoke-virtual {v3, v4}, Landroid/view/SurfaceControlImpl;->startAsyncDumpIfNeed(Z)V
/* .line 375 */
(( android.view.SurfaceControlImpl ) v3 ).ensureDumpFinished ( ); // invoke-virtual {v3}, Landroid/view/SurfaceControlImpl;->ensureDumpFinished()V
/* .line 376 */
int v4 = 1; // const/4 v4, 0x1
(( com.miui.server.stability.ScoutDisplayMemoryManager$DiaplayMemoryErrorInfo ) v1 ).setAction ( v4 ); // invoke-virtual {v1, v4}, Lcom/miui/server/stability/ScoutDisplayMemoryManager$DiaplayMemoryErrorInfo;->setAction(I)V
/* .line 379 */
(( android.view.SurfaceControlImpl ) v3 ).increaseLeakLevel ( v4 ); // invoke-virtual {v3, v4}, Landroid/view/SurfaceControlImpl;->increaseLeakLevel(Z)Ljava/lang/String;
/* .line 380 */
/* new-instance v4, Ljava/lang/RuntimeException; */
/* new-instance v12, Ljava/lang/StringBuilder; */
/* invoke-direct {v12}, Ljava/lang/StringBuilder;-><init>()V */
/* move-object/from16 v16, v3 */
} // .end local v3 # "scImpl":Landroid/view/SurfaceControlImpl;
/* .local v16, "scImpl":Landroid/view/SurfaceControlImpl; */
/* const-string/jumbo v3, "system_server (" */
(( java.lang.StringBuilder ) v12 ).append ( v3 ); // invoke-virtual {v12, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v3 ).append ( v2 ); // invoke-virtual {v3, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v3 ).append ( v15 ); // invoke-virtual {v3, v15}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v3 ).append ( v8, v9 ); // invoke-virtual {v3, v8, v9}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v3 ).append ( v11 ); // invoke-virtual {v3, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v3 ).append ( v5 ); // invoke-virtual {v3, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v3 ).append ( v14 ); // invoke-virtual {v3, v14}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v3 ).append ( v6, v7 ); // invoke-virtual {v3, v6, v7}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v3 ).append ( v13 ); // invoke-virtual {v3, v13}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v3 ).append ( v5 ); // invoke-virtual {v3, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v3 ).append ( v10 ); // invoke-virtual {v3, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v3 ).toString ( ); // invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
/* invoke-direct {v4, v3}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V */
/* throw v4 */
/* .line 368 */
} // .end local v16 # "scImpl":Landroid/view/SurfaceControlImpl;
} // .end local v21 # "adj":I
/* .local v3, "adj":I */
} // :cond_1
/* move/from16 v21, v3 */
/* .line 384 */
} // .end local v3 # "adj":I
/* .restart local v21 # "adj":I */
v3 = this.mService;
final String v4 = "), Used "; // const-string v4, "), Used "
/* move-object/from16 v22, v12 */
final String v12 = "Kill "; // const-string v12, "Kill "
if ( v3 != null) { // if-eqz v3, :cond_c
/* move-object/from16 v18, v4 */
/* move/from16 v3, v21 */
/* const/16 v4, -0x384 */
} // .end local v21 # "adj":I
/* .restart local v3 # "adj":I */
/* if-le v3, v4, :cond_b */
/* const/16 v4, 0x3e9 */
/* if-ge v3, v4, :cond_b */
/* .line 385 */
/* new-instance v4, Ljava/lang/StringBuilder; */
/* invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V */
/* move-object/from16 v21, v12 */
/* move-object/from16 v12, v20 */
} // .end local v20 # "name":Ljava/lang/String;
/* .local v12, "name":Ljava/lang/String; */
(( java.lang.StringBuilder ) v4 ).append ( v12 ); // invoke-virtual {v4, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
/* move-object/from16 v12, v19 */
} // .end local v12 # "name":Ljava/lang/String;
/* .restart local v20 # "name":Ljava/lang/String; */
(( java.lang.StringBuilder ) v4 ).append ( v12 ); // invoke-virtual {v4, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v4 ).append ( v2 ); // invoke-virtual {v4, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v4 ).append ( v15 ); // invoke-virtual {v4, v15}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v4 ).append ( v8, v9 ); // invoke-virtual {v4, v8, v9}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v4 ).append ( v11 ); // invoke-virtual {v4, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v4 ).append ( v5 ); // invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v4 ).append ( v14 ); // invoke-virtual {v4, v14}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v4 ).append ( v6, v7 ); // invoke-virtual {v4, v6, v7}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v4 ).append ( v13 ); // invoke-virtual {v4, v13}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v4 ).append ( v5 ); // invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v4 ).append ( v10 ); // invoke-virtual {v4, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v4 ).toString ( ); // invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
/* .line 388 */
/* .local v4, "appReason":Ljava/lang/String; */
int v15 = 0; // const/4 v15, 0x0
/* .line 389 */
/* .local v15, "killAction":Z */
/* move/from16 v19, v15 */
} // .end local v15 # "killAction":Z
/* .local v19, "killAction":Z */
/* const/16 v15, 0xc8 */
/* if-ge v3, v15, :cond_4 */
v15 = miui.mqsas.scout.ScoutUtils .isLibraryTest ( );
if ( v15 != null) { // if-eqz v15, :cond_2
/* move/from16 v23, v3 */
/* .line 409 */
} // :cond_2
com.android.server.am.ScoutMemoryError .getInstance ( );
/* move/from16 v23, v3 */
} // .end local v3 # "adj":I
/* .local v23, "adj":I */
com.android.server.am.ProcessUtils .getProcessRecordByPid ( v2 );
v3 = (( com.android.server.am.ScoutMemoryError ) v15 ).showAppDisplayMemoryErrorDialog ( v3, v4, v1 ); // invoke-virtual {v15, v3, v4, v1}, Lcom/android/server/am/ScoutMemoryError;->showAppDisplayMemoryErrorDialog(Lcom/android/server/am/ProcessRecord;Ljava/lang/String;Lcom/miui/server/stability/ScoutDisplayMemoryManager$DiaplayMemoryErrorInfo;)Z
if ( v3 != null) { // if-eqz v3, :cond_3
/* .line 410 */
int v15 = 1; // const/4 v15, 0x1
/* .line 411 */
} // .end local v19 # "killAction":Z
/* .restart local v15 # "killAction":Z */
int v3 = 3; // const/4 v3, 0x3
(( com.miui.server.stability.ScoutDisplayMemoryManager$DiaplayMemoryErrorInfo ) v1 ).setAction ( v3 ); // invoke-virtual {v1, v3}, Lcom/miui/server/stability/ScoutDisplayMemoryManager$DiaplayMemoryErrorInfo;->setAction(I)V
/* move v3, v15 */
/* move-object/from16 v15, v20 */
/* move-object/from16 v20, v10 */
/* goto/16 :goto_4 */
/* .line 409 */
} // .end local v15 # "killAction":Z
/* .restart local v19 # "killAction":Z */
} // :cond_3
/* move-object/from16 v15, v20 */
/* move-object/from16 v20, v10 */
/* goto/16 :goto_3 */
/* .line 389 */
} // .end local v23 # "adj":I
/* .restart local v3 # "adj":I */
} // :cond_4
/* move/from16 v23, v3 */
/* .line 390 */
} // .end local v3 # "adj":I
/* .restart local v23 # "adj":I */
} // :goto_0
/* iget v3, v0, Lcom/miui/server/stability/ScoutDisplayMemoryManager;->mLastCrashPid:I */
/* if-ne v3, v2, :cond_6 */
v3 = this.mLastCrashProc;
/* move-object/from16 v15, v20 */
} // .end local v20 # "name":Ljava/lang/String;
/* .local v15, "name":Ljava/lang/String; */
v3 = android.text.TextUtils .equals ( v3,v15 );
if ( v3 != null) { // if-eqz v3, :cond_5
/* iget v3, v0, Lcom/miui/server/stability/ScoutDisplayMemoryManager;->mCrashTimes:I */
/* move-object/from16 v20, v10 */
int v10 = 2; // const/4 v10, 0x2
/* if-ne v3, v10, :cond_7 */
/* .line 392 */
/* new-instance v3, Ljava/lang/StringBuilder; */
/* invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V */
final String v10 = "fail to crash "; // const-string v10, "fail to crash "
(( java.lang.StringBuilder ) v3 ).append ( v10 ); // invoke-virtual {v3, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v3 ).append ( v15 ); // invoke-virtual {v3, v15}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
final String v10 = " pid: "; // const-string v10, " pid: "
(( java.lang.StringBuilder ) v3 ).append ( v10 ); // invoke-virtual {v3, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v3 ).append ( v2 ); // invoke-virtual {v3, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;
final String v10 = " for two times, kill it instead"; // const-string v10, " for two times, kill it instead"
(( java.lang.StringBuilder ) v3 ).append ( v10 ); // invoke-virtual {v3, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v3 ).toString ( ); // invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
/* .line 394 */
android.os.Process .killProcess ( v2 );
/* .line 395 */
int v3 = 1; // const/4 v3, 0x1
/* .line 396 */
} // .end local v19 # "killAction":Z
/* .local v3, "killAction":Z */
int v10 = 1; // const/4 v10, 0x1
(( com.miui.server.stability.ScoutDisplayMemoryManager$DiaplayMemoryErrorInfo ) v1 ).setAction ( v10 ); // invoke-virtual {v1, v10}, Lcom/miui/server/stability/ScoutDisplayMemoryManager$DiaplayMemoryErrorInfo;->setAction(I)V
/* .line 397 */
int v10 = 0; // const/4 v10, 0x0
/* iput v10, v0, Lcom/miui/server/stability/ScoutDisplayMemoryManager;->mLastCrashPid:I */
/* iput v10, v0, Lcom/miui/server/stability/ScoutDisplayMemoryManager;->mCrashTimes:I */
/* .line 398 */
int v10 = 0; // const/4 v10, 0x0
this.mLastCrashProc = v10;
/* .line 390 */
} // .end local v3 # "killAction":Z
/* .restart local v19 # "killAction":Z */
} // :cond_5
/* move-object/from16 v20, v10 */
} // .end local v15 # "name":Ljava/lang/String;
/* .restart local v20 # "name":Ljava/lang/String; */
} // :cond_6
/* move-object/from16 v15, v20 */
/* move-object/from16 v20, v10 */
/* .line 399 */
} // .end local v20 # "name":Ljava/lang/String;
/* .restart local v15 # "name":Ljava/lang/String; */
} // :cond_7
} // :goto_1
com.android.server.am.ScoutMemoryError .getInstance ( );
/* .line 400 */
com.android.server.am.ProcessUtils .getProcessRecordByPid ( v2 );
/* .line 399 */
v3 = (( com.android.server.am.ScoutMemoryError ) v3 ).scheduleCrashApp ( v10, v4 ); // invoke-virtual {v3, v10, v4}, Lcom/android/server/am/ScoutMemoryError;->scheduleCrashApp(Lcom/android/server/am/ProcessRecord;Ljava/lang/String;)Z
if ( v3 != null) { // if-eqz v3, :cond_9
/* .line 401 */
int v3 = 1; // const/4 v3, 0x1
/* .line 402 */
} // .end local v19 # "killAction":Z
/* .restart local v3 # "killAction":Z */
int v10 = 2; // const/4 v10, 0x2
(( com.miui.server.stability.ScoutDisplayMemoryManager$DiaplayMemoryErrorInfo ) v1 ).setAction ( v10 ); // invoke-virtual {v1, v10}, Lcom/miui/server/stability/ScoutDisplayMemoryManager$DiaplayMemoryErrorInfo;->setAction(I)V
/* .line 403 */
/* iget v10, v0, Lcom/miui/server/stability/ScoutDisplayMemoryManager;->mLastCrashPid:I */
/* if-ne v10, v2, :cond_8 */
v10 = this.mLastCrashProc;
v10 = android.text.TextUtils .equals ( v10,v15 );
if ( v10 != null) { // if-eqz v10, :cond_8
/* .line 404 */
/* iget v10, v0, Lcom/miui/server/stability/ScoutDisplayMemoryManager;->mCrashTimes:I */
/* const/16 v17, 0x1 */
/* add-int/lit8 v10, v10, 0x1 */
} // :cond_8
int v10 = 1; // const/4 v10, 0x1
} // :goto_2
/* iput v10, v0, Lcom/miui/server/stability/ScoutDisplayMemoryManager;->mCrashTimes:I */
/* .line 405 */
/* iput v2, v0, Lcom/miui/server/stability/ScoutDisplayMemoryManager;->mLastCrashPid:I */
/* .line 406 */
this.mLastCrashProc = v15;
/* .line 414 */
} // .end local v3 # "killAction":Z
/* .restart local v19 # "killAction":Z */
} // :cond_9
} // :goto_3
/* move/from16 v3, v19 */
} // .end local v19 # "killAction":Z
/* .restart local v3 # "killAction":Z */
} // :goto_4
/* if-nez v3, :cond_a */
/* .line 415 */
/* new-instance v10, Ljava/lang/StringBuilder; */
/* invoke-direct {v10}, Ljava/lang/StringBuilder;-><init>()V */
/* move-object/from16 v0, v21 */
(( java.lang.StringBuilder ) v10 ).append ( v0 ); // invoke-virtual {v10, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v0 ).append ( v15 ); // invoke-virtual {v0, v15}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v0 ).append ( v12 ); // invoke-virtual {v0, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v0 ).append ( v2 ); // invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;
/* move-object/from16 v10, v18 */
(( java.lang.StringBuilder ) v0 ).append ( v10 ); // invoke-virtual {v0, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v0 ).append ( v8, v9 ); // invoke-virtual {v0, v8, v9}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v0 ).append ( v11 ); // invoke-virtual {v0, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v0 ).append ( v5 ); // invoke-virtual {v0, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v0 ).append ( v14 ); // invoke-virtual {v0, v14}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v0 ).append ( v6, v7 ); // invoke-virtual {v0, v6, v7}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v0 ).append ( v13 ); // invoke-virtual {v0, v13}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v0 ).append ( v5 ); // invoke-virtual {v0, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
/* move-object/from16 v10, v20 */
(( java.lang.StringBuilder ) v0 ).append ( v10 ); // invoke-virtual {v0, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v0 ).toString ( ); // invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
/* .line 418 */
android.os.Process .killProcess ( v2 );
/* .line 419 */
int v0 = 1; // const/4 v0, 0x1
(( com.miui.server.stability.ScoutDisplayMemoryManager$DiaplayMemoryErrorInfo ) v1 ).setAction ( v0 ); // invoke-virtual {v1, v0}, Lcom/miui/server/stability/ScoutDisplayMemoryManager$DiaplayMemoryErrorInfo;->setAction(I)V
/* .line 421 */
} // :cond_a
/* move-object/from16 v0, v22 */
android.util.Slog .e ( v0,v4 );
/* .line 422 */
} // .end local v3 # "killAction":Z
} // .end local v4 # "appReason":Ljava/lang/String;
/* move-object v0, v1 */
/* goto/16 :goto_6 */
/* .line 384 */
} // .end local v15 # "name":Ljava/lang/String;
} // .end local v23 # "adj":I
/* .local v3, "adj":I */
/* .restart local v20 # "name":Ljava/lang/String; */
} // :cond_b
/* move/from16 v23, v3 */
/* move-object v4, v10 */
/* move-object v0, v12 */
/* move-object/from16 v10, v18 */
/* move-object/from16 v12, v19 */
/* move-object/from16 v15, v20 */
/* move-object/from16 v3, v22 */
} // .end local v3 # "adj":I
} // .end local v20 # "name":Ljava/lang/String;
/* .restart local v15 # "name":Ljava/lang/String; */
/* .restart local v23 # "adj":I */
} // .end local v15 # "name":Ljava/lang/String;
} // .end local v23 # "adj":I
/* .restart local v20 # "name":Ljava/lang/String; */
/* .restart local v21 # "adj":I */
} // :cond_c
/* move-object v0, v12 */
/* move-object/from16 v12, v19 */
/* move-object/from16 v15, v20 */
/* move/from16 v23, v21 */
/* move-object/from16 v3, v22 */
/* move-object/from16 v24, v10 */
/* move-object v10, v4 */
/* move-object/from16 v4, v24 */
/* .line 423 */
} // .end local v20 # "name":Ljava/lang/String;
} // .end local v21 # "adj":I
/* .restart local v15 # "name":Ljava/lang/String; */
/* .restart local v23 # "adj":I */
} // :goto_5
/* new-instance v1, Ljava/lang/StringBuilder; */
/* invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V */
(( java.lang.StringBuilder ) v1 ).append ( v0 ); // invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v0 ).append ( v15 ); // invoke-virtual {v0, v15}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v0 ).append ( v12 ); // invoke-virtual {v0, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v0 ).append ( v2 ); // invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v0 ).append ( v10 ); // invoke-virtual {v0, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v0 ).append ( v8, v9 ); // invoke-virtual {v0, v8, v9}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v0 ).append ( v11 ); // invoke-virtual {v0, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v0 ).append ( v5 ); // invoke-virtual {v0, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v0 ).append ( v14 ); // invoke-virtual {v0, v14}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v0 ).append ( v6, v7 ); // invoke-virtual {v0, v6, v7}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v0 ).append ( v13 ); // invoke-virtual {v0, v13}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v0 ).append ( v5 ); // invoke-virtual {v0, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v0 ).append ( v4 ); // invoke-virtual {v0, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v0 ).toString ( ); // invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
android.util.Slog .e ( v3,v0 );
/* .line 426 */
android.os.Process .killProcess ( v2 );
/* .line 427 */
/* move-object/from16 v0, p1 */
int v1 = 1; // const/4 v1, 0x1
(( com.miui.server.stability.ScoutDisplayMemoryManager$DiaplayMemoryErrorInfo ) v0 ).setAction ( v1 ); // invoke-virtual {v0, v1}, Lcom/miui/server/stability/ScoutDisplayMemoryManager$DiaplayMemoryErrorInfo;->setAction(I)V
/* .line 367 */
} // .end local v15 # "name":Ljava/lang/String;
} // .end local v23 # "adj":I
/* .restart local v3 # "adj":I */
/* .local v4, "name":Ljava/lang/String; */
} // :cond_d
/* move-object v0, v1 */
/* move/from16 v23, v3 */
/* move-object v15, v4 */
/* move-object v3, v12 */
/* move-object v12, v11 */
/* .line 430 */
} // .end local v3 # "adj":I
} // .end local v4 # "name":Ljava/lang/String;
/* .restart local v15 # "name":Ljava/lang/String; */
/* .restart local v23 # "adj":I */
/* new-instance v1, Ljava/lang/StringBuilder; */
/* invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V */
(( java.lang.StringBuilder ) v1 ).append ( v15 ); // invoke-virtual {v1, v15}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v1 ).append ( v12 ); // invoke-virtual {v1, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v1 ).append ( v2 ); // invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;
final String v4 = ") is invalid"; // const-string v4, ") is invalid"
(( java.lang.StringBuilder ) v1 ).append ( v4 ); // invoke-virtual {v1, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v1 ).toString ( ); // invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
android.util.Slog .e ( v3,v1 );
/* .line 432 */
} // :goto_6
v1 = /* invoke-virtual/range {p1 ..p1}, Lcom/miui/server/stability/ScoutDisplayMemoryManager$DiaplayMemoryErrorInfo;->getAction()I */
int v3 = 3; // const/4 v3, 0x3
/* if-eq v1, v3, :cond_e */
/* .line 433 */
/* invoke-virtual/range {p0 ..p1}, Lcom/miui/server/stability/ScoutDisplayMemoryManager;->reportDisplayMemoryLeakEvent(Lcom/miui/server/stability/ScoutDisplayMemoryManager$DiaplayMemoryErrorInfo;)V */
/* .line 435 */
} // :cond_e
return;
} // .end method
static java.lang.String stringifyKBSize ( Long p0 ) {
/* .locals 3 */
/* .param p0, "size" # J */
/* .line 353 */
/* const-wide/16 v0, 0x400 */
/* mul-long/2addr v0, p0 */
/* const/16 v2, 0x400 */
com.miui.server.stability.ScoutDisplayMemoryManager .stringifySize ( v0,v1,v2 );
} // .end method
static java.lang.String stringifySize ( Long p0, Integer p1 ) {
/* .locals 5 */
/* .param p0, "size" # J */
/* .param p2, "order" # I */
/* .line 337 */
v0 = java.util.Locale.US;
/* .line 338 */
/* .local v0, "locale":Ljava/util/Locale; */
/* const-wide/16 v1, 0x400 */
/* sparse-switch p2, :sswitch_data_0 */
/* .line 348 */
/* new-instance v1, Ljava/lang/IllegalArgumentException; */
final String v2 = "Invalid size order"; // const-string v2, "Invalid size order"
/* invoke-direct {v1, v2}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V */
/* throw v1 */
/* .line 346 */
/* :sswitch_0 */
/* div-long v3, p0, v1 */
/* div-long/2addr v3, v1 */
/* div-long/2addr v3, v1 */
java.lang.Long .valueOf ( v3,v4 );
/* filled-new-array {v1}, [Ljava/lang/Object; */
final String v2 = "%,1dGB"; // const-string v2, "%,1dGB"
java.lang.String .format ( v0,v2,v1 );
/* .line 344 */
/* :sswitch_1 */
/* div-long v3, p0, v1 */
/* div-long/2addr v3, v1 */
java.lang.Long .valueOf ( v3,v4 );
/* filled-new-array {v1}, [Ljava/lang/Object; */
final String v2 = "%,5dMB"; // const-string v2, "%,5dMB"
java.lang.String .format ( v0,v2,v1 );
/* .line 342 */
/* :sswitch_2 */
/* div-long v1, p0, v1 */
java.lang.Long .valueOf ( v1,v2 );
/* filled-new-array {v1}, [Ljava/lang/Object; */
final String v2 = "%,9dkB"; // const-string v2, "%,9dkB"
java.lang.String .format ( v0,v2,v1 );
/* .line 340 */
/* :sswitch_3 */
java.lang.Long .valueOf ( p0,p1 );
/* filled-new-array {v1}, [Ljava/lang/Object; */
final String v2 = "%,13d"; // const-string v2, "%,13d"
java.lang.String .format ( v0,v2,v1 );
/* :sswitch_data_0 */
/* .sparse-switch */
/* 0x1 -> :sswitch_3 */
/* 0x400 -> :sswitch_2 */
/* 0x100000 -> :sswitch_1 */
/* 0x40000000 -> :sswitch_0 */
} // .end sparse-switch
} // .end method
/* # virtual methods */
public void checkScoutLowMemory ( ) {
/* .locals 9 */
/* .line 667 */
v0 = this.mService;
if ( v0 != null) { // if-eqz v0, :cond_a
v0 = (( com.miui.server.stability.ScoutDisplayMemoryManager ) p0 ).isEnableScoutMemory ( ); // invoke-virtual {p0}, Lcom/miui/server/stability/ScoutDisplayMemoryManager;->isEnableScoutMemory()Z
/* if-nez v0, :cond_0 */
/* goto/16 :goto_0 */
/* .line 671 */
} // :cond_0
android.os.SystemClock .uptimeMillis ( );
/* move-result-wide v0 */
/* .line 672 */
/* .local v0, "now":J */
v2 = this.mIsShowDialog;
v2 = (( java.util.concurrent.atomic.AtomicBoolean ) v2 ).get ( ); // invoke-virtual {v2}, Ljava/util/concurrent/atomic/AtomicBoolean;->get()Z
int v3 = 0; // const/4 v3, 0x0
final String v4 = "MIUIScout Memory"; // const-string v4, "MIUIScout Memory"
if ( v2 != null) { // if-eqz v2, :cond_2
/* .line 673 */
/* iget-wide v5, p0, Lcom/miui/server/stability/ScoutDisplayMemoryManager;->mShowDialogTime:J */
/* const-wide/32 v7, 0x493e0 */
/* add-long/2addr v5, v7 */
/* cmp-long v2, v0, v5 */
/* if-gez v2, :cond_1 */
/* .line 674 */
/* const-string/jumbo v2, "skip check display memory leak, dialog show" */
android.util.Slog .w ( v4,v2 );
/* .line 675 */
return;
/* .line 677 */
} // :cond_1
(( com.miui.server.stability.ScoutDisplayMemoryManager ) p0 ).setShowDialogState ( v3 ); // invoke-virtual {p0, v3}, Lcom/miui/server/stability/ScoutDisplayMemoryManager;->setShowDialogState(Z)V
/* .line 681 */
} // :cond_2
/* iget-wide v5, p0, Lcom/miui/server/stability/ScoutDisplayMemoryManager;->mLastReportTime:J */
/* const-wide/16 v7, 0x0 */
/* cmp-long v2, v5, v7 */
if ( v2 != null) { // if-eqz v2, :cond_6
v2 = this.mDisableState;
v2 = (( java.util.concurrent.atomic.AtomicBoolean ) v2 ).get ( ); // invoke-virtual {v2}, Ljava/util/concurrent/atomic/AtomicBoolean;->get()Z
if ( v2 != null) { // if-eqz v2, :cond_3
/* iget-wide v5, p0, Lcom/miui/server/stability/ScoutDisplayMemoryManager;->mLastReportTime:J */
/* const-wide/32 v7, 0x927c0 */
/* add-long/2addr v5, v7 */
/* cmp-long v2, v0, v5 */
/* if-ltz v2, :cond_4 */
/* .line 682 */
} // :cond_3
v2 = (( com.miui.server.stability.ScoutDisplayMemoryManager ) p0 ).isEnableResumeFeature ( ); // invoke-virtual {p0}, Lcom/miui/server/stability/ScoutDisplayMemoryManager;->isEnableResumeFeature()Z
/* if-nez v2, :cond_6 */
/* iget-wide v5, p0, Lcom/miui/server/stability/ScoutDisplayMemoryManager;->mLastReportTime:J */
/* const-wide/32 v7, 0x36ee80 */
/* add-long/2addr v5, v7 */
/* cmp-long v2, v0, v5 */
/* if-gez v2, :cond_6 */
/* .line 683 */
} // :cond_4
/* sget-boolean v2, Lcom/miui/server/stability/ScoutDisplayMemoryManager;->debug:Z */
if ( v2 != null) { // if-eqz v2, :cond_5
/* new-instance v2, Ljava/lang/StringBuilder; */
/* invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V */
final String v3 = "mLastReportTime = "; // const-string v3, "mLastReportTime = "
(( java.lang.StringBuilder ) v2 ).append ( v3 ); // invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
/* iget-wide v5, p0, Lcom/miui/server/stability/ScoutDisplayMemoryManager;->mLastReportTime:J */
(( java.lang.StringBuilder ) v2 ).append ( v5, v6 ); // invoke-virtual {v2, v5, v6}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;
final String v3 = " mDisableState = "; // const-string v3, " mDisableState = "
(( java.lang.StringBuilder ) v2 ).append ( v3 ); // invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
v3 = this.mDisableState;
/* .line 684 */
v3 = (( java.util.concurrent.atomic.AtomicBoolean ) v3 ).get ( ); // invoke-virtual {v3}, Ljava/util/concurrent/atomic/AtomicBoolean;->get()Z
(( java.lang.StringBuilder ) v2 ).append ( v3 ); // invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;
final String v3 = " now = "; // const-string v3, " now = "
(( java.lang.StringBuilder ) v2 ).append ( v3 ); // invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v2 ).append ( v0, v1 ); // invoke-virtual {v2, v0, v1}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;
final String v3 = " MEM_DISABLE_REPORT_INTERVAL = "; // const-string v3, " MEM_DISABLE_REPORT_INTERVAL = "
(( java.lang.StringBuilder ) v2 ).append ( v3 ); // invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
/* const v3, 0x927c0 */
(( java.lang.StringBuilder ) v2 ).append ( v3 ); // invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;
final String v3 = " MEM_REPORT_INTERVAL = "; // const-string v3, " MEM_REPORT_INTERVAL = "
(( java.lang.StringBuilder ) v2 ).append ( v3 ); // invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
/* const v3, 0x36ee80 */
(( java.lang.StringBuilder ) v2 ).append ( v3 ); // invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v2 ).toString ( ); // invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
/* .line 683 */
android.util.Slog .d ( v4,v2 );
/* .line 687 */
} // :cond_5
/* const-string/jumbo v2, "skip check display memory leak, less than last check interval" */
android.util.Slog .d ( v4,v2 );
/* .line 688 */
return;
/* .line 689 */
} // :cond_6
v2 = this.mDisableState;
v2 = (( java.util.concurrent.atomic.AtomicBoolean ) v2 ).get ( ); // invoke-virtual {v2}, Ljava/util/concurrent/atomic/AtomicBoolean;->get()Z
if ( v2 != null) { // if-eqz v2, :cond_7
/* .line 690 */
v2 = this.mDisableState;
(( java.util.concurrent.atomic.AtomicBoolean ) v2 ).set ( v3 ); // invoke-virtual {v2, v3}, Ljava/util/concurrent/atomic/AtomicBoolean;->set(Z)V
/* .line 693 */
} // :cond_7
/* sget-boolean v2, Lcom/miui/server/stability/ScoutDisplayMemoryManager;->debug:Z */
if ( v2 != null) { // if-eqz v2, :cond_8
final String v2 = "check MIUI Scout display memory"; // const-string v2, "check MIUI Scout display memory"
android.util.Slog .d ( v4,v2 );
/* .line 694 */
} // :cond_8
v2 = /* invoke-direct {p0}, Lcom/miui/server/stability/ScoutDisplayMemoryManager;->checkGpuMemoryLeak()Z */
/* if-nez v2, :cond_9 */
/* .line 695 */
/* invoke-direct {p0}, Lcom/miui/server/stability/ScoutDisplayMemoryManager;->checkDmaBufLeak()Z */
/* .line 697 */
} // :cond_9
return;
/* .line 668 */
} // .end local v0 # "now":J
} // :cond_a
} // :goto_0
return;
} // .end method
public void checkScoutLowMemory ( com.android.server.am.ScoutMeminfo p0 ) {
/* .locals 2 */
/* .param p1, "scoutMeminfo" # Lcom/android/server/am/ScoutMeminfo; */
/* .line 657 */
if ( p1 != null) { // if-eqz p1, :cond_3
v0 = (( com.miui.server.stability.ScoutDisplayMemoryManager ) p0 ).isEnableScoutMemory ( ); // invoke-virtual {p0}, Lcom/miui/server/stability/ScoutDisplayMemoryManager;->isEnableScoutMemory()Z
/* if-nez v0, :cond_0 */
/* .line 660 */
} // :cond_0
/* sget-boolean v0, Lcom/miui/server/stability/ScoutDisplayMemoryManager;->debug:Z */
if ( v0 != null) { // if-eqz v0, :cond_1
final String v0 = "MIUIScout Memory"; // const-string v0, "MIUIScout Memory"
final String v1 = "check MIUI Scout display memory(root)"; // const-string v1, "check MIUI Scout display memory(root)"
android.util.Slog .d ( v0,v1 );
/* .line 661 */
} // :cond_1
v0 = /* invoke-direct {p0}, Lcom/miui/server/stability/ScoutDisplayMemoryManager;->checkGpuMemoryLeak()Z */
/* if-nez v0, :cond_2 */
/* .line 662 */
/* invoke-direct {p0, p1}, Lcom/miui/server/stability/ScoutDisplayMemoryManager;->checkDmaBufLeak(Lcom/android/server/am/ScoutMeminfo;)Z */
/* .line 664 */
} // :cond_2
return;
/* .line 658 */
} // :cond_3
} // :goto_0
return;
} // .end method
public java.lang.String getDmabufUsageInfo ( ) {
/* .locals 3 */
/* .line 180 */
/* invoke-direct {p0}, Lcom/miui/server/stability/ScoutDisplayMemoryManager;->readDmabufInfo()Lcom/miui/server/stability/DmaBufUsageInfo; */
/* .line 181 */
/* .local v0, "dmabufInfo":Lcom/miui/server/stability/DmaBufUsageInfo; */
if ( v0 != null) { // if-eqz v0, :cond_0
/* .line 182 */
(( com.miui.server.stability.DmaBufUsageInfo ) v0 ).getList ( ); // invoke-virtual {v0}, Lcom/miui/server/stability/DmaBufUsageInfo;->getList()Ljava/util/ArrayList;
/* .line 183 */
/* .local v1, "infoList":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/miui/server/stability/DmaBufProcUsageInfo;>;" */
/* new-instance v2, Lcom/miui/server/stability/ScoutDisplayMemoryManager$1; */
/* invoke-direct {v2, p0}, Lcom/miui/server/stability/ScoutDisplayMemoryManager$1;-><init>(Lcom/miui/server/stability/ScoutDisplayMemoryManager;)V */
java.util.Collections .sort ( v1,v2 );
/* .line 195 */
(( com.miui.server.stability.DmaBufUsageInfo ) v0 ).toString ( ); // invoke-virtual {v0}, Lcom/miui/server/stability/DmaBufUsageInfo;->toString()Ljava/lang/String;
/* .line 197 */
} // .end local v1 # "infoList":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/miui/server/stability/DmaBufProcUsageInfo;>;"
} // :cond_0
int v1 = 0; // const/4 v1, 0x0
} // .end method
public java.lang.String getGpuMemoryUsageInfo ( ) {
/* .locals 3 */
/* .line 201 */
/* iget v0, p0, Lcom/miui/server/stability/ScoutDisplayMemoryManager;->gpuType:I */
int v1 = 0; // const/4 v1, 0x0
/* if-nez v0, :cond_0 */
/* .line 202 */
} // :cond_0
/* invoke-direct {p0, v0}, Lcom/miui/server/stability/ScoutDisplayMemoryManager;->readGpuMemoryInfo(I)Lcom/miui/server/stability/GpuMemoryUsageInfo; */
/* .line 203 */
/* .local v0, "gpuInfo":Lcom/miui/server/stability/GpuMemoryUsageInfo; */
if ( v0 != null) { // if-eqz v0, :cond_1
/* .line 204 */
(( com.miui.server.stability.GpuMemoryUsageInfo ) v0 ).getList ( ); // invoke-virtual {v0}, Lcom/miui/server/stability/GpuMemoryUsageInfo;->getList()Ljava/util/ArrayList;
/* .line 205 */
/* .local v1, "infoList":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/miui/server/stability/GpuMemoryProcUsageInfo;>;" */
/* new-instance v2, Lcom/miui/server/stability/ScoutDisplayMemoryManager$2; */
/* invoke-direct {v2, p0}, Lcom/miui/server/stability/ScoutDisplayMemoryManager$2;-><init>(Lcom/miui/server/stability/ScoutDisplayMemoryManager;)V */
java.util.Collections .sort ( v1,v2 );
/* .line 217 */
/* iget v2, p0, Lcom/miui/server/stability/ScoutDisplayMemoryManager;->gpuType:I */
(( com.miui.server.stability.GpuMemoryUsageInfo ) v0 ).toString ( v2 ); // invoke-virtual {v0, v2}, Lcom/miui/server/stability/GpuMemoryUsageInfo;->toString(I)Ljava/lang/String;
/* .line 219 */
} // .end local v1 # "infoList":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/miui/server/stability/GpuMemoryProcUsageInfo;>;"
} // :cond_1
} // .end method
public void handleReportMqs ( com.miui.server.stability.ScoutDisplayMemoryManager$DiaplayMemoryErrorInfo p0, java.lang.String p1 ) {
/* .locals 2 */
/* .param p1, "errorInfo" # Lcom/miui/server/stability/ScoutDisplayMemoryManager$DiaplayMemoryErrorInfo; */
/* .param p2, "filePath" # Ljava/lang/String; */
/* .line 291 */
com.android.server.MiuiBgThread .getHandler ( );
/* new-instance v1, Lcom/miui/server/stability/ScoutDisplayMemoryManager$$ExternalSyntheticLambda1; */
/* invoke-direct {v1, p0, p2, p1}, Lcom/miui/server/stability/ScoutDisplayMemoryManager$$ExternalSyntheticLambda1;-><init>(Lcom/miui/server/stability/ScoutDisplayMemoryManager;Ljava/lang/String;Lcom/miui/server/stability/ScoutDisplayMemoryManager$DiaplayMemoryErrorInfo;)V */
(( android.os.Handler ) v0 ).post ( v1 ); // invoke-virtual {v0, v1}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z
/* .line 316 */
return;
} // .end method
public void init ( com.android.server.am.ActivityManagerService p0, android.content.Context p1 ) {
/* .locals 2 */
/* .param p1, "mService" # Lcom/android/server/am/ActivityManagerService; */
/* .param p2, "mContext" # Landroid/content/Context; */
/* .line 159 */
this.mService = p1;
/* .line 160 */
this.mContext = p2;
/* .line 161 */
com.android.server.am.ScoutMemoryError .getInstance ( );
(( com.android.server.am.ScoutMemoryError ) v0 ).init ( p1, p2 ); // invoke-virtual {v0, p1, p2}, Lcom/android/server/am/ScoutMemoryError;->init(Lcom/android/server/am/ActivityManagerService;Landroid/content/Context;)V
/* .line 162 */
v0 = /* invoke-direct {p0}, Lcom/miui/server/stability/ScoutDisplayMemoryManager;->initGpuType()I */
/* iput v0, p0, Lcom/miui/server/stability/ScoutDisplayMemoryManager;->gpuType:I */
/* .line 163 */
/* new-instance v0, Ljava/lang/StringBuilder; */
/* invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V */
final String v1 = "gpuType = "; // const-string v1, "gpuType = "
(( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
/* iget v1, p0, Lcom/miui/server/stability/ScoutDisplayMemoryManager;->gpuType:I */
(( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v0 ).toString ( ); // invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
final String v1 = "MIUIScout Memory"; // const-string v1, "MIUIScout Memory"
android.util.Slog .d ( v1,v0 );
/* .line 164 */
return;
} // .end method
public Boolean isEnableResumeFeature ( ) {
/* .locals 2 */
/* .line 172 */
v0 = miui.mqsas.scout.ScoutUtils .isLibraryTest ( );
/* if-nez v0, :cond_1 */
v0 = miui.mqsas.scout.ScoutUtils .isUnReleased ( );
/* if-nez v0, :cond_1 */
/* .line 173 */
final String v0 = "persist.sys.debug.enable_scout_memory_resume"; // const-string v0, "persist.sys.debug.enable_scout_memory_resume"
int v1 = 0; // const/4 v1, 0x0
v0 = android.os.SystemProperties .getBoolean ( v0,v1 );
if ( v0 != null) { // if-eqz v0, :cond_0
/* .line 176 */
} // :cond_0
/* .line 174 */
} // :cond_1
} // :goto_0
int v0 = 1; // const/4 v0, 0x1
} // .end method
public Boolean isEnableScoutMemory ( ) {
/* .locals 1 */
/* .line 167 */
/* sget-boolean v0, Lcom/miui/server/stability/ScoutDisplayMemoryManager;->ENABLE_SCOUT_MEMORY_MONITOR:Z */
if ( v0 != null) { // if-eqz v0, :cond_0
int v0 = 1; // const/4 v0, 0x1
/* .line 168 */
} // :cond_0
int v0 = 0; // const/4 v0, 0x0
} // .end method
public void reportDisplayMemoryLeakEvent ( com.miui.server.stability.ScoutDisplayMemoryManager$DiaplayMemoryErrorInfo p0 ) {
/* .locals 2 */
/* .param p1, "errorInfo" # Lcom/miui/server/stability/ScoutDisplayMemoryManager$DiaplayMemoryErrorInfo; */
/* .line 252 */
v0 = this.mContext;
if ( v0 != null) { // if-eqz v0, :cond_1
/* if-nez p1, :cond_0 */
/* .line 255 */
} // :cond_0
com.android.server.MiuiBgThread .getHandler ( );
/* new-instance v1, Lcom/miui/server/stability/ScoutDisplayMemoryManager$$ExternalSyntheticLambda0; */
/* invoke-direct {v1, p0, p1}, Lcom/miui/server/stability/ScoutDisplayMemoryManager$$ExternalSyntheticLambda0;-><init>(Lcom/miui/server/stability/ScoutDisplayMemoryManager;Lcom/miui/server/stability/ScoutDisplayMemoryManager$DiaplayMemoryErrorInfo;)V */
(( android.os.Handler ) v0 ).post ( v1 ); // invoke-virtual {v0, v1}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z
/* .line 287 */
return;
/* .line 253 */
} // :cond_1
} // :goto_0
return;
} // .end method
public void setDisableState ( Boolean p0 ) {
/* .locals 1 */
/* .param p1, "state" # Z */
/* .line 231 */
v0 = this.mDisableState;
(( java.util.concurrent.atomic.AtomicBoolean ) v0 ).set ( p1 ); // invoke-virtual {v0, p1}, Ljava/util/concurrent/atomic/AtomicBoolean;->set(Z)V
/* .line 232 */
return;
} // .end method
public void setShowDialogState ( Boolean p0 ) {
/* .locals 2 */
/* .param p1, "state" # Z */
/* .line 235 */
v0 = this.mIsShowDialog;
(( java.util.concurrent.atomic.AtomicBoolean ) v0 ).set ( p1 ); // invoke-virtual {v0, p1}, Ljava/util/concurrent/atomic/AtomicBoolean;->set(Z)V
/* .line 236 */
if ( p1 != null) { // if-eqz p1, :cond_0
/* .line 237 */
android.os.SystemClock .uptimeMillis ( );
/* move-result-wide v0 */
/* iput-wide v0, p0, Lcom/miui/server/stability/ScoutDisplayMemoryManager;->mShowDialogTime:J */
/* .line 239 */
} // :cond_0
return;
} // .end method
public void updateCameraForegroundState ( Boolean p0 ) {
/* .locals 0 */
/* .param p1, "isCameraForeground" # Z */
/* .line 223 */
/* iput-boolean p1, p0, Lcom/miui/server/stability/ScoutDisplayMemoryManager;->isCameraForeground:Z */
/* .line 224 */
return;
} // .end method
public void updateLastReportTime ( ) {
/* .locals 2 */
/* .line 227 */
android.os.SystemClock .uptimeMillis ( );
/* move-result-wide v0 */
/* iput-wide v0, p0, Lcom/miui/server/stability/ScoutDisplayMemoryManager;->mLastReportTime:J */
/* .line 228 */
return;
} // .end method
