class com.miui.server.SplashScreenServiceDelegate$2$1 implements java.lang.Runnable {
	 /* .source "SplashScreenServiceDelegate.java" */
	 /* # interfaces */
	 /* # annotations */
	 /* .annotation system Ldalvik/annotation/EnclosingMethod; */
	 /* value = Lcom/miui/server/SplashScreenServiceDelegate$2;->asyncSetSplashPackageCheckListener()V */
} // .end annotation
/* .annotation system Ldalvik/annotation/InnerClass; */
/* accessFlags = 0x0 */
/* name = null */
} // .end annotation
/* # instance fields */
final com.miui.server.SplashScreenServiceDelegate$2 this$1; //synthetic
/* # direct methods */
 com.miui.server.SplashScreenServiceDelegate$2$1 ( ) {
/* .locals 0 */
/* .param p1, "this$1" # Lcom/miui/server/SplashScreenServiceDelegate$2; */
/* .line 205 */
this.this$1 = p1;
/* invoke-direct {p0}, Ljava/lang/Object;-><init>()V */
return;
} // .end method
/* # virtual methods */
public void run ( ) {
/* .locals 4 */
/* .line 208 */
v0 = this.this$1;
v0 = this.this$0;
com.miui.server.SplashScreenServiceDelegate .-$$Nest$fgetmSplashScreenService ( v0 );
/* .line 209 */
/* .local v0, "sss":Lcom/miui/server/ISplashScreenService; */
if ( v0 != null) { // if-eqz v0, :cond_0
	 /* .line 211 */
	 try { // :try_start_0
		 v1 = this.this$1;
		 v1 = this.this$0;
		 final String v2 = "Set splash package check listener"; // const-string v2, "Set splash package check listener"
		 com.miui.server.SplashScreenServiceDelegate .-$$Nest$mlogI ( v1,v2 );
		 /* .line 212 */
		 v1 = this.this$1;
		 v1 = this.this$0;
		 com.miui.server.SplashScreenServiceDelegate .-$$Nest$fgetmSplashPackageCheckListener ( v1 );
		 /* :try_end_0 */
		 /* .catch Ljava/lang/Exception; {:try_start_0 ..:try_end_0} :catch_0 */
		 /* .line 215 */
		 /* .line 213 */
		 /* :catch_0 */
		 /* move-exception v1 */
		 /* .line 214 */
		 /* .local v1, "e":Ljava/lang/Exception; */
		 v2 = this.this$1;
		 v2 = this.this$0;
		 final String v3 = "asyncSetSplashPackageCheckListener exception"; // const-string v3, "asyncSetSplashPackageCheckListener exception"
		 com.miui.server.SplashScreenServiceDelegate .-$$Nest$mlogE ( v2,v3,v1 );
		 /* .line 217 */
	 } // .end local v1 # "e":Ljava/lang/Exception;
} // :cond_0
} // :goto_0
return;
} // .end method
