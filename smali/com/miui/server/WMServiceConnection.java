public class com.miui.server.WMServiceConnection implements android.content.ServiceConnection {
	 /* .source "WMServiceConnection.java" */
	 /* # interfaces */
	 /* # static fields */
	 private static final java.lang.String ACTION;
	 private static final Integer BIND_DELAY;
	 private static final Integer MAX_DEATH_COUNT_IN_ONE_DAY;
	 private static final Integer MAX_DEATH_COUNT_IN_TOTAL;
	 private static final Integer ONE_DAY_IN_MILLISECONDS;
	 private static final java.lang.String PACKAGE_NAME;
	 private static final java.lang.String TAG;
	 /* # instance fields */
	 private java.lang.Runnable mBindRunnable;
	 private android.content.Context mContext;
	 android.os.IBinder$DeathRecipient mDeathHandler;
	 private java.util.List mDeathTimes;
	 /* .annotation system Ldalvik/annotation/Signature; */
	 /* value = { */
	 /* "Ljava/util/List<", */
	 /* "Ljava/lang/Long;", */
	 /* ">;" */
	 /* } */
} // .end annotation
} // .end field
private android.os.Handler mHandler;
private android.os.IBinder mRemote;
/* # direct methods */
static java.util.List -$$Nest$fgetmDeathTimes ( com.miui.server.WMServiceConnection p0 ) { //bridge//synthethic
/* .locals 0 */
p0 = this.mDeathTimes;
} // .end method
static android.os.Handler -$$Nest$fgetmHandler ( com.miui.server.WMServiceConnection p0 ) { //bridge//synthethic
/* .locals 0 */
p0 = this.mHandler;
} // .end method
static void -$$Nest$mbind ( com.miui.server.WMServiceConnection p0 ) { //bridge//synthethic
/* .locals 0 */
/* invoke-direct {p0}, Lcom/miui/server/WMServiceConnection;->bind()V */
return;
} // .end method
static void -$$Nest$mbindDelay ( com.miui.server.WMServiceConnection p0 ) { //bridge//synthethic
/* .locals 0 */
/* invoke-direct {p0}, Lcom/miui/server/WMServiceConnection;->bindDelay()V */
return;
} // .end method
static Boolean -$$Nest$mshouldBind ( com.miui.server.WMServiceConnection p0 ) { //bridge//synthethic
/* .locals 0 */
p0 = /* invoke-direct {p0}, Lcom/miui/server/WMServiceConnection;->shouldBind()Z */
} // .end method
public com.miui.server.WMServiceConnection ( ) {
/* .locals 2 */
/* .param p1, "context" # Landroid/content/Context; */
/* .line 30 */
/* invoke-direct {p0}, Ljava/lang/Object;-><init>()V */
/* .line 43 */
/* new-instance v0, Lcom/miui/server/WMServiceConnection$1; */
/* invoke-direct {v0, p0}, Lcom/miui/server/WMServiceConnection$1;-><init>(Lcom/miui/server/WMServiceConnection;)V */
this.mBindRunnable = v0;
/* .line 120 */
/* new-instance v0, Lcom/miui/server/WMServiceConnection$2; */
/* invoke-direct {v0, p0}, Lcom/miui/server/WMServiceConnection$2;-><init>(Lcom/miui/server/WMServiceConnection;)V */
this.mDeathHandler = v0;
/* .line 31 */
this.mContext = p1;
/* .line 32 */
/* new-instance v0, Landroid/os/Handler; */
/* invoke-direct {v0}, Landroid/os/Handler;-><init>()V */
this.mHandler = v0;
/* .line 33 */
/* new-instance v0, Ljava/util/ArrayList; */
int v1 = 3; // const/4 v1, 0x3
/* invoke-direct {v0, v1}, Ljava/util/ArrayList;-><init>(I)V */
this.mDeathTimes = v0;
/* .line 34 */
/* invoke-direct {p0}, Lcom/miui/server/WMServiceConnection;->bindDelay()V */
/* .line 35 */
return;
} // .end method
private void bind ( ) {
/* .locals 4 */
/* .line 57 */
final String v0 = "WMServiceConnection"; // const-string v0, "WMServiceConnection"
try { // :try_start_0
	 /* new-instance v1, Landroid/content/Intent; */
	 final String v2 = "com.miui.wmsvc.LINK"; // const-string v2, "com.miui.wmsvc.LINK"
	 /* invoke-direct {v1, v2}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V */
	 /* .line 58 */
	 /* .local v1, "intent":Landroid/content/Intent; */
	 final String v2 = "com.miui.wmsvc"; // const-string v2, "com.miui.wmsvc"
	 (( android.content.Intent ) v1 ).setPackage ( v2 ); // invoke-virtual {v1, v2}, Landroid/content/Intent;->setPackage(Ljava/lang/String;)Landroid/content/Intent;
	 /* .line 59 */
	 v2 = this.mContext;
	 int v3 = 1; // const/4 v3, 0x1
	 v2 = 	 (( android.content.Context ) v2 ).bindService ( v1, p0, v3 ); // invoke-virtual {v2, v1, p0, v3}, Landroid/content/Context;->bindService(Landroid/content/Intent;Landroid/content/ServiceConnection;I)Z
	 if ( v2 != null) { // if-eqz v2, :cond_0
		 /* .line 60 */
		 final String v2 = "Bind Inspector success!"; // const-string v2, "Bind Inspector success!"
		 android.util.Slog .d ( v0,v2 );
		 /* .line 62 */
	 } // :cond_0
	 final String v2 = "Bind Inspector failed!"; // const-string v2, "Bind Inspector failed!"
	 android.util.Slog .e ( v0,v2 );
	 /* :try_end_0 */
	 /* .catch Ljava/lang/Exception; {:try_start_0 ..:try_end_0} :catch_0 */
	 /* .line 66 */
} // .end local v1 # "intent":Landroid/content/Intent;
} // :goto_0
/* .line 64 */
/* :catch_0 */
/* move-exception v1 */
/* .line 65 */
/* .local v1, "e":Ljava/lang/Exception; */
final String v2 = "Bind Inspector failed"; // const-string v2, "Bind Inspector failed"
android.util.Slog .e ( v0,v2 );
/* .line 67 */
} // .end local v1 # "e":Ljava/lang/Exception;
} // :goto_1
return;
} // .end method
private void bindDelay ( ) {
/* .locals 4 */
/* .line 38 */
final String v0 = "WMServiceConnection"; // const-string v0, "WMServiceConnection"
final String v1 = "schedule bind in 60000ms"; // const-string v1, "schedule bind in 60000ms"
android.util.Slog .d ( v0,v1 );
/* .line 39 */
v0 = this.mHandler;
v1 = this.mBindRunnable;
(( android.os.Handler ) v0 ).removeCallbacks ( v1 ); // invoke-virtual {v0, v1}, Landroid/os/Handler;->removeCallbacks(Ljava/lang/Runnable;)V
/* .line 40 */
v0 = this.mHandler;
v1 = this.mBindRunnable;
/* const-wide/32 v2, 0xea60 */
(( android.os.Handler ) v0 ).postDelayed ( v1, v2, v3 ); // invoke-virtual {v0, v1, v2, v3}, Landroid/os/Handler;->postDelayed(Ljava/lang/Runnable;J)Z
/* .line 41 */
return;
} // .end method
private Boolean shouldBind ( ) {
/* .locals 9 */
/* .line 92 */
v0 = this.mContext;
(( android.content.Context ) v0 ).getContentResolver ( ); // invoke-virtual {v0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;
v0 = android.provider.MiuiSettings$Secure .isHttpInvokeAppEnable ( v0 );
int v1 = 0; // const/4 v1, 0x0
final String v2 = "WMServiceConnection"; // const-string v2, "WMServiceConnection"
/* if-nez v0, :cond_0 */
/* .line 93 */
final String v0 = "Cancel bind for http invoke disabled"; // const-string v0, "Cancel bind for http invoke disabled"
android.util.Slog .d ( v2,v0 );
/* .line 94 */
/* .line 96 */
} // :cond_0
v0 = this.mRemote;
if ( v0 != null) { // if-eqz v0, :cond_1
/* .line 97 */
final String v0 = "Cancel bind for connected"; // const-string v0, "Cancel bind for connected"
android.util.Slog .d ( v2,v0 );
/* .line 98 */
/* .line 101 */
} // :cond_1
v0 = v0 = this.mDeathTimes;
/* const/16 v3, 0xa */
/* if-lt v0, v3, :cond_2 */
/* .line 102 */
final String v0 = "Cancel bind for MAX_DEATH_COUNT_IN_TOTAL reached"; // const-string v0, "Cancel bind for MAX_DEATH_COUNT_IN_TOTAL reached"
android.util.Slog .w ( v2,v0 );
/* .line 103 */
/* .line 106 */
} // :cond_2
v0 = v0 = this.mDeathTimes;
int v3 = 3; // const/4 v3, 0x3
/* if-lt v0, v3, :cond_3 */
/* .line 107 */
v4 = v0 = this.mDeathTimes;
/* sub-int/2addr v4, v3 */
/* check-cast v0, Ljava/lang/Long; */
(( java.lang.Long ) v0 ).longValue ( ); // invoke-virtual {v0}, Ljava/lang/Long;->longValue()J
/* move-result-wide v3 */
/* .line 108 */
/* .local v3, "time":J */
/* const-wide/32 v5, 0x5265c00 */
/* add-long/2addr v5, v3 */
android.os.SystemClock .elapsedRealtime ( );
/* move-result-wide v7 */
/* sub-long/2addr v5, v7 */
/* .line 109 */
/* .local v5, "delay":J */
/* const-wide/16 v7, 0x0 */
/* cmp-long v0, v5, v7 */
/* if-lez v0, :cond_3 */
/* .line 110 */
final String v0 = "Cancel bind for MAX_DEATH_COUNT_IN_ONE_DAY reached"; // const-string v0, "Cancel bind for MAX_DEATH_COUNT_IN_ONE_DAY reached"
android.util.Slog .w ( v2,v0 );
/* .line 111 */
v0 = this.mHandler;
v2 = this.mBindRunnable;
(( android.os.Handler ) v0 ).removeCallbacks ( v2 ); // invoke-virtual {v0, v2}, Landroid/os/Handler;->removeCallbacks(Ljava/lang/Runnable;)V
/* .line 112 */
v0 = this.mHandler;
v2 = this.mBindRunnable;
(( android.os.Handler ) v0 ).postDelayed ( v2, v5, v6 ); // invoke-virtual {v0, v2, v5, v6}, Landroid/os/Handler;->postDelayed(Ljava/lang/Runnable;J)Z
/* .line 113 */
/* .line 117 */
} // .end local v3 # "time":J
} // .end local v5 # "delay":J
} // :cond_3
int v0 = 1; // const/4 v0, 0x1
} // .end method
/* # virtual methods */
public void onServiceConnected ( android.content.ComponentName p0, android.os.IBinder p1 ) {
/* .locals 4 */
/* .param p1, "name" # Landroid/content/ComponentName; */
/* .param p2, "service" # Landroid/os/IBinder; */
/* .line 71 */
final String v0 = "WMServiceConnection"; // const-string v0, "WMServiceConnection"
this.mRemote = p2;
/* .line 72 */
v1 = this.mHandler;
v2 = this.mBindRunnable;
(( android.os.Handler ) v1 ).removeCallbacks ( v2 ); // invoke-virtual {v1, v2}, Landroid/os/Handler;->removeCallbacks(Ljava/lang/Runnable;)V
/* .line 74 */
try { // :try_start_0
v1 = this.mRemote;
v2 = this.mDeathHandler;
int v3 = 0; // const/4 v3, 0x0
/* :try_end_0 */
/* .catch Ljava/lang/Exception; {:try_start_0 ..:try_end_0} :catch_0 */
/* .line 77 */
/* .line 75 */
/* :catch_0 */
/* move-exception v1 */
/* .line 76 */
/* .local v1, "e":Ljava/lang/Exception; */
final String v2 = "linkToDeath failed"; // const-string v2, "linkToDeath failed"
android.util.Slog .e ( v0,v2 );
/* .line 78 */
} // .end local v1 # "e":Ljava/lang/Exception;
} // :goto_0
final String v1 = "onServiceConnected"; // const-string v1, "onServiceConnected"
android.util.Slog .d ( v0,v1 );
/* .line 79 */
return;
} // .end method
public void onServiceDisconnected ( android.content.ComponentName p0 ) {
/* .locals 2 */
/* .param p1, "name" # Landroid/content/ComponentName; */
/* .line 83 */
int v0 = 0; // const/4 v0, 0x0
this.mRemote = v0;
/* .line 84 */
final String v0 = "WMServiceConnection"; // const-string v0, "WMServiceConnection"
final String v1 = "onServiceDisconnected"; // const-string v1, "onServiceDisconnected"
android.util.Slog .d ( v0,v1 );
/* .line 85 */
v0 = this.mContext;
if ( v0 != null) { // if-eqz v0, :cond_0
/* .line 86 */
(( android.content.Context ) v0 ).unbindService ( p0 ); // invoke-virtual {v0, p0}, Landroid/content/Context;->unbindService(Landroid/content/ServiceConnection;)V
/* .line 88 */
} // :cond_0
return;
} // .end method
