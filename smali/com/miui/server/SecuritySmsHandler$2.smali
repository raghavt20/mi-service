.class Lcom/miui/server/SecuritySmsHandler$2;
.super Landroid/content/BroadcastReceiver;
.source "SecuritySmsHandler.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/miui/server/SecuritySmsHandler;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/miui/server/SecuritySmsHandler;


# direct methods
.method constructor <init>(Lcom/miui/server/SecuritySmsHandler;)V
    .locals 0
    .param p1, "this$0"    # Lcom/miui/server/SecuritySmsHandler;

    .line 299
    iput-object p1, p0, Lcom/miui/server/SecuritySmsHandler$2;->this$0:Lcom/miui/server/SecuritySmsHandler;

    invoke-direct {p0}, Landroid/content/BroadcastReceiver;-><init>()V

    return-void
.end method


# virtual methods
.method public onReceive(Landroid/content/Context;Landroid/content/Intent;)V
    .locals 11
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "intent"    # Landroid/content/Intent;

    .line 302
    invoke-virtual {p2}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v0

    .line 303
    .local v0, "action":Ljava/lang/String;
    invoke-static {p2}, Lcom/miui/server/SecuritySmsHandler;->getSlotIdFromIntent(Landroid/content/Intent;)I

    move-result v1

    .line 304
    .local v1, "slotId":I
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "mInterceptedSmsResultReceiver sms dispatched, action:"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    const-string v3, "SecuritySmsHandler"

    invoke-static {v3, v2}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 306
    const-string v2, "android.provider.Telephony.SMS_RECEIVED"

    invoke-virtual {v2, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_2

    .line 307
    invoke-virtual {p0}, Lcom/miui/server/SecuritySmsHandler$2;->getResultCode()I

    move-result v2

    .line 309
    .local v2, "resultCode":I
    const/4 v4, -0x1

    if-ne v2, v4, :cond_2

    .line 310
    const-string v4, "mInterceptedSmsResultReceiver SMS_RECEIVED_ACTION not aborted"

    invoke-static {v3, v4}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 311
    invoke-static {p2}, Landroid/provider/Telephony$Sms$Intents;->getMessagesFromIntent(Landroid/content/Intent;)[Landroid/telephony/SmsMessage;

    move-result-object v4

    .line 312
    .local v4, "msgs":[Landroid/telephony/SmsMessage;
    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    .line 313
    .local v5, "sb":Ljava/lang/StringBuilder;
    const/4 v6, 0x0

    .local v6, "i":I
    :goto_0
    array-length v7, v4

    if-ge v6, v7, :cond_0

    .line 314
    aget-object v7, v4, v6

    invoke-virtual {v7}, Landroid/telephony/SmsMessage;->getDisplayMessageBody()Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v5, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 313
    add-int/lit8 v6, v6, 0x1

    goto :goto_0

    .line 316
    .end local v6    # "i":I
    :cond_0
    const/4 v6, 0x0

    aget-object v6, v4, v6

    invoke-virtual {v6}, Landroid/telephony/SmsMessage;->getOriginatingAddress()Ljava/lang/String;

    move-result-object v6

    .line 317
    .local v6, "address":Ljava/lang/String;
    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    .line 320
    .local v7, "body":Ljava/lang/String;
    iget-object v8, p0, Lcom/miui/server/SecuritySmsHandler$2;->this$0:Lcom/miui/server/SecuritySmsHandler;

    invoke-static {v8, v6, v7, v1}, Lcom/miui/server/SecuritySmsHandler;->-$$Nest$mcheckByAntiSpam(Lcom/miui/server/SecuritySmsHandler;Ljava/lang/String;Ljava/lang/String;I)I

    move-result v8

    .line 321
    .local v8, "blockType":I
    if-eqz v8, :cond_2

    .line 322
    const-string v9, "blockType"

    invoke-virtual {p2, v9, v8}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    .line 323
    invoke-static {v8}, Lmiui/provider/ExtraTelephony;->getRealBlockType(I)I

    move-result v9

    const/4 v10, 0x3

    if-lt v9, v10, :cond_1

    .line 324
    const-string v9, "mInterceptedSmsResultReceiver: This sms is intercepted by AntiSpam"

    invoke-static {v3, v9}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 325
    iget-object v3, p0, Lcom/miui/server/SecuritySmsHandler$2;->this$0:Lcom/miui/server/SecuritySmsHandler;

    invoke-static {v3, p2}, Lcom/miui/server/SecuritySmsHandler;->-$$Nest$mdispatchSmsToAntiSpam(Lcom/miui/server/SecuritySmsHandler;Landroid/content/Intent;)V

    goto :goto_1

    .line 327
    :cond_1
    iget-object v3, p0, Lcom/miui/server/SecuritySmsHandler$2;->this$0:Lcom/miui/server/SecuritySmsHandler;

    invoke-static {v3, p2}, Lcom/miui/server/SecuritySmsHandler;->-$$Nest$mdispatchNormalSms(Lcom/miui/server/SecuritySmsHandler;Landroid/content/Intent;)V

    .line 332
    .end local v2    # "resultCode":I
    .end local v4    # "msgs":[Landroid/telephony/SmsMessage;
    .end local v5    # "sb":Ljava/lang/StringBuilder;
    .end local v6    # "address":Ljava/lang/String;
    .end local v7    # "body":Ljava/lang/String;
    .end local v8    # "blockType":I
    :cond_2
    :goto_1
    return-void
.end method
