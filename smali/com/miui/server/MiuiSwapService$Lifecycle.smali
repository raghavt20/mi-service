.class public final Lcom/miui/server/MiuiSwapService$Lifecycle;
.super Lcom/android/server/SystemService;
.source "MiuiSwapService.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/miui/server/MiuiSwapService;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "Lifecycle"
.end annotation


# instance fields
.field private final mService:Lcom/miui/server/MiuiSwapService;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 1
    .param p1, "context"    # Landroid/content/Context;

    .line 59
    invoke-direct {p0, p1}, Lcom/android/server/SystemService;-><init>(Landroid/content/Context;)V

    .line 60
    new-instance v0, Lcom/miui/server/MiuiSwapService;

    invoke-direct {v0, p1}, Lcom/miui/server/MiuiSwapService;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Lcom/miui/server/MiuiSwapService$Lifecycle;->mService:Lcom/miui/server/MiuiSwapService;

    .line 61
    return-void
.end method


# virtual methods
.method public onStart()V
    .locals 2

    .line 65
    const-string v0, "miui.swap.service"

    iget-object v1, p0, Lcom/miui/server/MiuiSwapService$Lifecycle;->mService:Lcom/miui/server/MiuiSwapService;

    invoke-virtual {p0, v0, v1}, Lcom/miui/server/MiuiSwapService$Lifecycle;->publishBinderService(Ljava/lang/String;Landroid/os/IBinder;)V

    .line 66
    return-void
.end method
