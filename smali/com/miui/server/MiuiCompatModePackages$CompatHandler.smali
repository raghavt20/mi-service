.class final Lcom/miui/server/MiuiCompatModePackages$CompatHandler;
.super Landroid/os/Handler;
.source "MiuiCompatModePackages.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/miui/server/MiuiCompatModePackages;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x12
    name = "CompatHandler"
.end annotation


# instance fields
.field final synthetic this$0:Lcom/miui/server/MiuiCompatModePackages;


# direct methods
.method public constructor <init>(Lcom/miui/server/MiuiCompatModePackages;Landroid/os/Looper;)V
    .locals 1
    .param p2, "looper"    # Landroid/os/Looper;

    .line 114
    iput-object p1, p0, Lcom/miui/server/MiuiCompatModePackages$CompatHandler;->this$0:Lcom/miui/server/MiuiCompatModePackages;

    .line 115
    const/4 p1, 0x0

    const/4 v0, 0x1

    invoke-direct {p0, p2, p1, v0}, Landroid/os/Handler;-><init>(Landroid/os/Looper;Landroid/os/Handler$Callback;Z)V

    .line 116
    return-void
.end method


# virtual methods
.method public handleMessage(Landroid/os/Message;)V
    .locals 1
    .param p1, "msg"    # Landroid/os/Message;

    .line 120
    iget v0, p1, Landroid/os/Message;->what:I

    packed-switch v0, :pswitch_data_0

    goto :goto_0

    .line 151
    :pswitch_0
    iget-object v0, p0, Lcom/miui/server/MiuiCompatModePackages$CompatHandler;->this$0:Lcom/miui/server/MiuiCompatModePackages;

    invoke-static {v0}, Lcom/miui/server/MiuiCompatModePackages;->-$$Nest$mupdateCloudData(Lcom/miui/server/MiuiCompatModePackages;)V

    goto :goto_0

    .line 148
    :pswitch_1
    iget-object v0, p0, Lcom/miui/server/MiuiCompatModePackages$CompatHandler;->this$0:Lcom/miui/server/MiuiCompatModePackages;

    invoke-virtual {v0}, Lcom/miui/server/MiuiCompatModePackages;->saveCutoutModeFile()V

    .line 149
    goto :goto_0

    .line 145
    :pswitch_2
    iget-object v0, p0, Lcom/miui/server/MiuiCompatModePackages$CompatHandler;->this$0:Lcom/miui/server/MiuiCompatModePackages;

    invoke-virtual {v0}, Lcom/miui/server/MiuiCompatModePackages;->saveSpecialModeFile()V

    .line 146
    goto :goto_0

    .line 140
    :pswitch_3
    iget-object v0, p1, Landroid/os/Message;->obj:Ljava/lang/Object;

    if-eqz v0, :cond_0

    iget-object v0, p1, Landroid/os/Message;->obj:Ljava/lang/Object;

    check-cast v0, Ljava/lang/Boolean;

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 141
    iget-object v0, p0, Lcom/miui/server/MiuiCompatModePackages$CompatHandler;->this$0:Lcom/miui/server/MiuiCompatModePackages;

    invoke-static {v0}, Lcom/miui/server/MiuiCompatModePackages;->-$$Nest$mhandleDontShowAgain(Lcom/miui/server/MiuiCompatModePackages;)V

    goto :goto_0

    .line 137
    :pswitch_4
    iget-object v0, p1, Landroid/os/Message;->obj:Ljava/lang/Object;

    check-cast v0, Ljava/lang/String;

    .line 138
    .local v0, "packageName":Ljava/lang/String;
    goto :goto_0

    .line 134
    .end local v0    # "packageName":Ljava/lang/String;
    :pswitch_5
    iget-object v0, p0, Lcom/miui/server/MiuiCompatModePackages$CompatHandler;->this$0:Lcom/miui/server/MiuiCompatModePackages;

    invoke-static {v0}, Lcom/miui/server/MiuiCompatModePackages;->-$$Nest$mhandleUnregisterObservers(Lcom/miui/server/MiuiCompatModePackages;)V

    .line 135
    goto :goto_0

    .line 131
    :pswitch_6
    iget-object v0, p0, Lcom/miui/server/MiuiCompatModePackages$CompatHandler;->this$0:Lcom/miui/server/MiuiCompatModePackages;

    invoke-static {v0}, Lcom/miui/server/MiuiCompatModePackages;->-$$Nest$mhandleRegisterObservers(Lcom/miui/server/MiuiCompatModePackages;)V

    .line 132
    goto :goto_0

    .line 125
    :pswitch_7
    iget-object v0, p0, Lcom/miui/server/MiuiCompatModePackages$CompatHandler;->this$0:Lcom/miui/server/MiuiCompatModePackages;

    invoke-static {v0}, Lcom/miui/server/MiuiCompatModePackages;->-$$Nest$mreadCutoutModeConfig(Lcom/miui/server/MiuiCompatModePackages;)V

    .line 126
    iget-object v0, p0, Lcom/miui/server/MiuiCompatModePackages$CompatHandler;->this$0:Lcom/miui/server/MiuiCompatModePackages;

    invoke-static {v0}, Lcom/miui/server/MiuiCompatModePackages;->-$$Nest$mreadSpecialModeConfig(Lcom/miui/server/MiuiCompatModePackages;)V

    .line 127
    iget-object v0, p0, Lcom/miui/server/MiuiCompatModePackages$CompatHandler;->this$0:Lcom/miui/server/MiuiCompatModePackages;

    invoke-static {v0}, Lcom/miui/server/MiuiCompatModePackages;->-$$Nest$mreadPackagesConfig(Lcom/miui/server/MiuiCompatModePackages;)V

    .line 128
    iget-object v0, p0, Lcom/miui/server/MiuiCompatModePackages$CompatHandler;->this$0:Lcom/miui/server/MiuiCompatModePackages;

    invoke-static {v0}, Lcom/miui/server/MiuiCompatModePackages;->-$$Nest$mreadSuggestApps(Lcom/miui/server/MiuiCompatModePackages;)V

    .line 129
    goto :goto_0

    .line 122
    :pswitch_8
    iget-object v0, p0, Lcom/miui/server/MiuiCompatModePackages$CompatHandler;->this$0:Lcom/miui/server/MiuiCompatModePackages;

    invoke-virtual {v0}, Lcom/miui/server/MiuiCompatModePackages;->saveCompatModes()V

    .line 123
    nop

    .line 154
    :cond_0
    :goto_0
    return-void

    nop

    :pswitch_data_0
    .packed-switch 0x64
        :pswitch_8
        :pswitch_7
        :pswitch_6
        :pswitch_5
        :pswitch_4
        :pswitch_3
        :pswitch_2
        :pswitch_1
        :pswitch_0
    .end packed-switch
.end method
