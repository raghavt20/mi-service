class com.miui.server.SecurityManagerService$MyPackageObserver implements android.content.pm.PackageManagerInternal$PackageListObserver {
	 /* .source "SecurityManagerService.java" */
	 /* # interfaces */
	 /* # annotations */
	 /* .annotation system Ldalvik/annotation/EnclosingClass; */
	 /* value = Lcom/miui/server/SecurityManagerService; */
} // .end annotation
/* .annotation system Ldalvik/annotation/InnerClass; */
/* accessFlags = 0x8 */
/* name = "MyPackageObserver" */
} // .end annotation
/* # instance fields */
private final android.content.Context mContext;
private final com.miui.server.security.DefaultBrowserImpl mDefaultBrowserImpl;
/* # direct methods */
 com.miui.server.SecurityManagerService$MyPackageObserver ( ) {
/* .locals 0 */
/* .param p1, "context" # Landroid/content/Context; */
/* .param p2, "defaultBrowserImpl" # Lcom/miui/server/security/DefaultBrowserImpl; */
/* .line 163 */
/* invoke-direct {p0}, Ljava/lang/Object;-><init>()V */
/* .line 164 */
this.mContext = p1;
/* .line 165 */
this.mDefaultBrowserImpl = p2;
/* .line 166 */
return;
} // .end method
/* # virtual methods */
public void onPackageAdded ( java.lang.String p0, Integer p1 ) {
/* .locals 2 */
/* .param p1, "packageName" # Ljava/lang/String; */
/* .param p2, "uid" # I */
/* .line 169 */
miui.security.WakePathChecker .getInstance ( );
v1 = this.mContext;
(( miui.security.WakePathChecker ) v0 ).onPackageAdded ( v1 ); // invoke-virtual {v0, v1}, Lmiui/security/WakePathChecker;->onPackageAdded(Landroid/content/Context;)V
/* .line 170 */
v0 = this.mDefaultBrowserImpl;
(( com.miui.server.security.DefaultBrowserImpl ) v0 ).checkDefaultBrowser ( p1 ); // invoke-virtual {v0, p1}, Lcom/miui/server/security/DefaultBrowserImpl;->checkDefaultBrowser(Ljava/lang/String;)V
/* .line 171 */
return;
} // .end method
public void onPackageChanged ( java.lang.String p0, Integer p1 ) {
/* .locals 1 */
/* .param p1, "packageName" # Ljava/lang/String; */
/* .param p2, "uid" # I */
/* .line 180 */
v0 = this.mDefaultBrowserImpl;
(( com.miui.server.security.DefaultBrowserImpl ) v0 ).checkDefaultBrowser ( p1 ); // invoke-virtual {v0, p1}, Lcom/miui/server/security/DefaultBrowserImpl;->checkDefaultBrowser(Ljava/lang/String;)V
/* .line 181 */
return;
} // .end method
public void onPackageRemoved ( java.lang.String p0, Integer p1 ) {
/* .locals 1 */
/* .param p1, "packageName" # Ljava/lang/String; */
/* .param p2, "uid" # I */
/* .line 175 */
v0 = this.mDefaultBrowserImpl;
(( com.miui.server.security.DefaultBrowserImpl ) v0 ).checkDefaultBrowser ( p1 ); // invoke-virtual {v0, p1}, Lcom/miui/server/security/DefaultBrowserImpl;->checkDefaultBrowser(Ljava/lang/String;)V
/* .line 176 */
return;
} // .end method
