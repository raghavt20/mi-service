public class com.miui.server.MiuiFreeDragService extends miui.freedrag.IMiuiFreeDragService$Stub implements com.miui.app.MiuiFreeDragServiceInternal {
	 /* .source "MiuiFreeDragService.java" */
	 /* # interfaces */
	 /* # annotations */
	 /* .annotation system Ldalvik/annotation/MemberClasses; */
	 /* value = { */
	 /* Lcom/miui/server/MiuiFreeDragService$Lifecycle; */
	 /* } */
} // .end annotation
/* # static fields */
public static final java.lang.String SERVICE_NAME;
private static com.miui.server.MiuiFreeDragService instance;
/* # instance fields */
private final android.util.ArrayMap mPackagesWithMiuiDragAndDropMetaData;
/* .annotation system Ldalvik/annotation/Signature; */
/* value = { */
/* "Landroid/util/ArrayMap<", */
/* "Ljava/lang/String;", */
/* "Landroid/util/ArraySet<", */
/* "Ljava/lang/String;", */
/* ">;>;" */
/* } */
} // .end annotation
} // .end field
/* # direct methods */
private com.miui.server.MiuiFreeDragService ( ) {
/* .locals 1 */
/* .line 41 */
/* invoke-direct {p0}, Lmiui/freedrag/IMiuiFreeDragService$Stub;-><init>()V */
/* .line 52 */
/* new-instance v0, Landroid/util/ArrayMap; */
/* invoke-direct {v0}, Landroid/util/ArrayMap;-><init>()V */
this.mPackagesWithMiuiDragAndDropMetaData = v0;
/* .line 42 */
/* const-class v0, Lcom/miui/app/MiuiFreeDragServiceInternal; */
com.android.server.LocalServices .addService ( v0,p0 );
/* .line 43 */
return;
} // .end method
public static synchronized com.miui.server.MiuiFreeDragService getInstance ( ) {
/* .locals 2 */
/* const-class v0, Lcom/miui/server/MiuiFreeDragService; */
/* monitor-enter v0 */
/* .line 46 */
try { // :try_start_0
v1 = com.miui.server.MiuiFreeDragService.instance;
/* if-nez v1, :cond_0 */
/* .line 47 */
/* new-instance v1, Lcom/miui/server/MiuiFreeDragService; */
/* invoke-direct {v1}, Lcom/miui/server/MiuiFreeDragService;-><init>()V */
/* .line 49 */
} // :cond_0
v1 = com.miui.server.MiuiFreeDragService.instance;
/* :try_end_0 */
/* .catchall {:try_start_0 ..:try_end_0} :catchall_0 */
/* monitor-exit v0 */
/* .line 45 */
/* :catchall_0 */
/* move-exception v1 */
/* monitor-exit v0 */
/* throw v1 */
} // .end method
/* # virtual methods */
public java.util.List getFilterRules ( ) {
/* .locals 9 */
/* .annotation system Ldalvik/annotation/Signature; */
/* value = { */
/* "()", */
/* "Ljava/util/List<", */
/* "Lmiui/freedrag/FilterRule;", */
/* ">;" */
/* } */
} // .end annotation
/* .line 68 */
v0 = android.os.Binder .getCallingPid ( );
com.android.server.am.ProcessUtils .getPackageNameByPid ( v0 );
/* .line 69 */
/* .local v0, "packageName":Ljava/lang/String; */
int v1 = 0; // const/4 v1, 0x0
if ( v0 != null) { // if-eqz v0, :cond_6
v2 = (( java.lang.String ) v0 ).isEmpty ( ); // invoke-virtual {v0}, Ljava/lang/String;->isEmpty()Z
if ( v2 != null) { // if-eqz v2, :cond_0
/* goto/16 :goto_2 */
/* .line 74 */
} // :cond_0
com.android.server.MiuiCommonCloudServiceStub .getInstance ( );
final String v3 = "miui_free_drag"; // const-string v3, "miui_free_drag"
(( com.android.server.MiuiCommonCloudServiceStub ) v2 ).getDataByModuleName ( v3 ); // invoke-virtual {v2, v3}, Lcom/android/server/MiuiCommonCloudServiceStub;->getDataByModuleName(Ljava/lang/String;)Ljava/lang/Object;
/* check-cast v2, Lmiui/freedrag/FilterRuleData; */
/* .line 75 */
/* .local v2, "filterRuleData":Lmiui/freedrag/FilterRuleData; */
/* if-nez v2, :cond_1 */
/* .line 76 */
final String v3 = "MiuiFreeDragService"; // const-string v3, "MiuiFreeDragService"
final String v4 = "getFilterRules filterRuleData = null, can\'t drag!"; // const-string v4, "getFilterRules filterRuleData = null, can\'t drag!"
android.util.Slog .e ( v3,v4 );
/* .line 77 */
/* .line 80 */
} // :cond_1
/* new-instance v1, Ljava/util/ArrayList; */
/* invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V */
/* move-object v3, v1 */
/* .line 81 */
/* .local v3, "rules":Ljava/util/List;, "Ljava/util/List<Lmiui/freedrag/FilterRule;>;" */
(( miui.freedrag.FilterRuleData ) v2 ).getRules ( ); // invoke-virtual {v2}, Lmiui/freedrag/FilterRuleData;->getRules()Ljava/util/List;
v4 = } // :goto_0
if ( v4 != null) { // if-eqz v4, :cond_4
/* check-cast v4, Lmiui/freedrag/FilterRule; */
/* .line 82 */
/* .local v4, "rule":Lmiui/freedrag/FilterRule; */
(( miui.freedrag.FilterRule ) v4 ).getPackageName ( ); // invoke-virtual {v4}, Lmiui/freedrag/FilterRule;->getPackageName()Ljava/lang/String;
v5 = (( java.lang.String ) v5 ).isEmpty ( ); // invoke-virtual {v5}, Ljava/lang/String;->isEmpty()Z
/* if-nez v5, :cond_2 */
(( miui.freedrag.FilterRule ) v4 ).getPackageName ( ); // invoke-virtual {v4}, Lmiui/freedrag/FilterRule;->getPackageName()Ljava/lang/String;
v5 = (( java.lang.String ) v5 ).equals ( v0 ); // invoke-virtual {v5, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z
if ( v5 != null) { // if-eqz v5, :cond_3
/* .line 83 */
} // :cond_2
/* .line 85 */
} // .end local v4 # "rule":Lmiui/freedrag/FilterRule;
} // :cond_3
/* .line 87 */
} // :cond_4
v4 = this.mPackagesWithMiuiDragAndDropMetaData;
/* monitor-enter v4 */
/* .line 88 */
try { // :try_start_0
v1 = this.mPackagesWithMiuiDragAndDropMetaData;
v1 = (( android.util.ArrayMap ) v1 ).containsKey ( v0 ); // invoke-virtual {v1, v0}, Landroid/util/ArrayMap;->containsKey(Ljava/lang/Object;)Z
if ( v1 != null) { // if-eqz v1, :cond_5
/* .line 89 */
v1 = this.mPackagesWithMiuiDragAndDropMetaData;
(( android.util.ArrayMap ) v1 ).get ( v0 ); // invoke-virtual {v1, v0}, Landroid/util/ArrayMap;->get(Ljava/lang/Object;)Ljava/lang/Object;
/* check-cast v1, Landroid/util/ArraySet; */
/* .line 90 */
/* .local v1, "activityClassSet":Landroid/util/ArraySet;, "Landroid/util/ArraySet<Ljava/lang/String;>;" */
(( android.util.ArraySet ) v1 ).iterator ( ); // invoke-virtual {v1}, Landroid/util/ArraySet;->iterator()Ljava/util/Iterator;
v6 = } // :goto_1
if ( v6 != null) { // if-eqz v6, :cond_5
/* check-cast v6, Ljava/lang/String; */
/* .line 91 */
/* .local v6, "activityClass":Ljava/lang/String; */
/* new-instance v7, Lmiui/freedrag/FilterRule; */
final String v8 = ""; // const-string v8, ""
/* invoke-direct {v7, v0, v6, v8}, Lmiui/freedrag/FilterRule;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V */
/* .line 92 */
/* .local v7, "filterRule":Lmiui/freedrag/FilterRule; */
/* .line 93 */
/* nop */
} // .end local v6 # "activityClass":Ljava/lang/String;
} // .end local v7 # "filterRule":Lmiui/freedrag/FilterRule;
/* .line 95 */
} // .end local v1 # "activityClassSet":Landroid/util/ArraySet;, "Landroid/util/ArraySet<Ljava/lang/String;>;"
} // :cond_5
/* monitor-exit v4 */
/* .line 97 */
/* .line 95 */
/* :catchall_0 */
/* move-exception v1 */
/* monitor-exit v4 */
/* :try_end_0 */
/* .catchall {:try_start_0 ..:try_end_0} :catchall_0 */
/* throw v1 */
/* .line 70 */
} // .end local v2 # "filterRuleData":Lmiui/freedrag/FilterRuleData;
} // .end local v3 # "rules":Ljava/util/List;, "Ljava/util/List<Lmiui/freedrag/FilterRule;>;"
} // :cond_6
} // :goto_2
final String v2 = "MiuiFreeDragService"; // const-string v2, "MiuiFreeDragService"
/* new-instance v3, Ljava/lang/StringBuilder; */
/* invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V */
final String v4 = "getFilterRules packageName = "; // const-string v4, "getFilterRules packageName = "
(( java.lang.StringBuilder ) v3 ).append ( v4 ); // invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v3 ).append ( v0 ); // invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
final String v4 = ", can\'t drag!"; // const-string v4, ", can\'t drag!"
(( java.lang.StringBuilder ) v3 ).append ( v4 ); // invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v3 ).toString ( ); // invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
android.util.Slog .e ( v2,v3 );
/* .line 71 */
} // .end method
public void notifyHasMiuiDragAndDropMetaData ( java.lang.String p0, java.lang.String p1 ) {
/* .locals 3 */
/* .param p1, "packageName" # Ljava/lang/String; */
/* .param p2, "className" # Ljava/lang/String; */
/* .line 56 */
v0 = this.mPackagesWithMiuiDragAndDropMetaData;
/* monitor-enter v0 */
/* .line 57 */
try { // :try_start_0
v1 = this.mPackagesWithMiuiDragAndDropMetaData;
(( android.util.ArrayMap ) v1 ).get ( p1 ); // invoke-virtual {v1, p1}, Landroid/util/ArrayMap;->get(Ljava/lang/Object;)Ljava/lang/Object;
/* check-cast v1, Landroid/util/ArraySet; */
/* .line 58 */
/* .local v1, "set":Landroid/util/ArraySet;, "Landroid/util/ArraySet<Ljava/lang/String;>;" */
/* if-nez v1, :cond_0 */
/* .line 59 */
/* new-instance v2, Landroid/util/ArraySet; */
/* invoke-direct {v2}, Landroid/util/ArraySet;-><init>()V */
/* move-object v1, v2 */
/* .line 61 */
} // :cond_0
(( android.util.ArraySet ) v1 ).add ( p2 ); // invoke-virtual {v1, p2}, Landroid/util/ArraySet;->add(Ljava/lang/Object;)Z
/* .line 62 */
v2 = this.mPackagesWithMiuiDragAndDropMetaData;
(( android.util.ArrayMap ) v2 ).put ( p1, v1 ); // invoke-virtual {v2, p1, v1}, Landroid/util/ArrayMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;
/* .line 63 */
/* nop */
} // .end local v1 # "set":Landroid/util/ArraySet;, "Landroid/util/ArraySet<Ljava/lang/String;>;"
/* monitor-exit v0 */
/* .line 64 */
return;
/* .line 63 */
/* :catchall_0 */
/* move-exception v1 */
/* monitor-exit v0 */
/* :try_end_0 */
/* .catchall {:try_start_0 ..:try_end_0} :catchall_0 */
/* throw v1 */
} // .end method
