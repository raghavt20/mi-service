.class public final Lcom/miui/server/MiuiCompatModePackages;
.super Ljava/lang/Object;
.source "MiuiCompatModePackages.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/miui/server/MiuiCompatModePackages$CompatHandler;,
        Lcom/miui/server/MiuiCompatModePackages$AppLaunchObserver;
    }
.end annotation


# static fields
.field private static final ATTR_CONFIG_NOTIFY_SUGGEST_APPS:Ljava/lang/String; = "notifySuggestApps"

.field private static final MODULE_CUTOUT_MODE:Ljava/lang/String; = "cutout_mode"

.field private static final MSG_DONT_SHOW_AGAIN:I = 0x69

.field private static final MSG_ON_APP_LAUNCH:I = 0x68

.field private static final MSG_READ:I = 0x65

.field private static final MSG_REGISTER_OBSERVER:I = 0x66

.field private static final MSG_UNREGISTER_OBSERVER:I = 0x67

.field private static final MSG_UPDATE_CLOUD_DATA:I = 0x6c

.field private static final MSG_WRITE:I = 0x64

.field private static final MSG_WRITE_CUTOUT_MODE:I = 0x6b

.field private static final MSG_WRITE_SPECIAL_MODE:I = 0x6a

.field private static final TAG:Ljava/lang/String; = "MiuiCompatModePackages"

.field private static final TAG_NAME_CONFIG:Ljava/lang/String; = "config"

.field private static final URI_CLOUD_ALL_DATA_NOTIFY:Landroid/net/Uri;


# instance fields
.field private mAlertDialog:Landroid/app/AlertDialog;

.field private mBroadcastReceiver:Landroid/content/BroadcastReceiver;

.field private final mCloudCutoutModePackages:Ljava/util/HashMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/HashMap<",
            "Ljava/lang/String;",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation
.end field

.field private final mCloudDataObserver:Landroid/database/ContentObserver;

.field private final mContext:Landroid/content/Context;

.field private final mCutoutModeFile:Landroid/util/AtomicFile;

.field private mDefaultAspect:F

.field private final mDefaultType:Ljava/util/HashMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/HashMap<",
            "Ljava/lang/String;",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation
.end field

.field private final mFile:Landroid/util/AtomicFile;

.field private final mHandler:Lcom/miui/server/MiuiCompatModePackages$CompatHandler;

.field private final mLock:Ljava/lang/Object;

.field private final mMainHandler:Landroid/os/Handler;

.field private final mNotchConfig:Ljava/util/HashMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/HashMap<",
            "Ljava/lang/String;",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation
.end field

.field private final mNotchSpecialModePackages:Ljava/util/HashMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/HashMap<",
            "Ljava/lang/String;",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation
.end field

.field private mNotifySuggestApps:Z

.field private final mPackages:Ljava/util/HashMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/HashMap<",
            "Ljava/lang/String;",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation
.end field

.field private mProcessObserver:Landroid/app/IMiuiProcessObserver;

.field private final mRestrictList:Ljava/util/HashSet;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/HashSet<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private final mSpecialModeFile:Landroid/util/AtomicFile;

.field private final mSuggestList:Ljava/util/HashSet;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/HashSet<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private final mSupportNotchList:Ljava/util/HashSet;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/HashSet<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private final mUserCutoutModePackages:Ljava/util/HashMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/HashMap<",
            "Ljava/lang/String;",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method static bridge synthetic -$$Nest$fgetmCloudDataObserver(Lcom/miui/server/MiuiCompatModePackages;)Landroid/database/ContentObserver;
    .locals 0

    iget-object p0, p0, Lcom/miui/server/MiuiCompatModePackages;->mCloudDataObserver:Landroid/database/ContentObserver;

    return-object p0
.end method

.method static bridge synthetic -$$Nest$fgetmContext(Lcom/miui/server/MiuiCompatModePackages;)Landroid/content/Context;
    .locals 0

    iget-object p0, p0, Lcom/miui/server/MiuiCompatModePackages;->mContext:Landroid/content/Context;

    return-object p0
.end method

.method static bridge synthetic -$$Nest$fgetmHandler(Lcom/miui/server/MiuiCompatModePackages;)Lcom/miui/server/MiuiCompatModePackages$CompatHandler;
    .locals 0

    iget-object p0, p0, Lcom/miui/server/MiuiCompatModePackages;->mHandler:Lcom/miui/server/MiuiCompatModePackages$CompatHandler;

    return-object p0
.end method

.method static bridge synthetic -$$Nest$mhandleDontShowAgain(Lcom/miui/server/MiuiCompatModePackages;)V
    .locals 0

    invoke-direct {p0}, Lcom/miui/server/MiuiCompatModePackages;->handleDontShowAgain()V

    return-void
.end method

.method static bridge synthetic -$$Nest$mhandleRegisterObservers(Lcom/miui/server/MiuiCompatModePackages;)V
    .locals 0

    invoke-direct {p0}, Lcom/miui/server/MiuiCompatModePackages;->handleRegisterObservers()V

    return-void
.end method

.method static bridge synthetic -$$Nest$mhandleRemovePackage(Lcom/miui/server/MiuiCompatModePackages;Ljava/lang/String;)V
    .locals 0

    invoke-direct {p0, p1}, Lcom/miui/server/MiuiCompatModePackages;->handleRemovePackage(Ljava/lang/String;)V

    return-void
.end method

.method static bridge synthetic -$$Nest$mhandleUnregisterObservers(Lcom/miui/server/MiuiCompatModePackages;)V
    .locals 0

    invoke-direct {p0}, Lcom/miui/server/MiuiCompatModePackages;->handleUnregisterObservers()V

    return-void
.end method

.method static bridge synthetic -$$Nest$mhandleUpdatePackage(Lcom/miui/server/MiuiCompatModePackages;Ljava/lang/String;)V
    .locals 0

    invoke-direct {p0, p1}, Lcom/miui/server/MiuiCompatModePackages;->handleUpdatePackage(Ljava/lang/String;)V

    return-void
.end method

.method static bridge synthetic -$$Nest$mreadCutoutModeConfig(Lcom/miui/server/MiuiCompatModePackages;)V
    .locals 0

    invoke-direct {p0}, Lcom/miui/server/MiuiCompatModePackages;->readCutoutModeConfig()V

    return-void
.end method

.method static bridge synthetic -$$Nest$mreadPackagesConfig(Lcom/miui/server/MiuiCompatModePackages;)V
    .locals 0

    invoke-direct {p0}, Lcom/miui/server/MiuiCompatModePackages;->readPackagesConfig()V

    return-void
.end method

.method static bridge synthetic -$$Nest$mreadSpecialModeConfig(Lcom/miui/server/MiuiCompatModePackages;)V
    .locals 0

    invoke-direct {p0}, Lcom/miui/server/MiuiCompatModePackages;->readSpecialModeConfig()V

    return-void
.end method

.method static bridge synthetic -$$Nest$mreadSuggestApps(Lcom/miui/server/MiuiCompatModePackages;)V
    .locals 0

    invoke-direct {p0}, Lcom/miui/server/MiuiCompatModePackages;->readSuggestApps()V

    return-void
.end method

.method static bridge synthetic -$$Nest$mupdateCloudData(Lcom/miui/server/MiuiCompatModePackages;)V
    .locals 0

    invoke-direct {p0}, Lcom/miui/server/MiuiCompatModePackages;->updateCloudData()V

    return-void
.end method

.method static bridge synthetic -$$Nest$sfgetURI_CLOUD_ALL_DATA_NOTIFY()Landroid/net/Uri;
    .locals 1

    sget-object v0, Lcom/miui/server/MiuiCompatModePackages;->URI_CLOUD_ALL_DATA_NOTIFY:Landroid/net/Uri;

    return-object v0
.end method

.method static constructor <clinit>()V
    .locals 1

    .line 69
    nop

    .line 70
    const-string v0, "content://com.android.settings.cloud.CloudSettings/cloud_all_data/notify"

    invoke-static {v0}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v0

    sput-object v0, Lcom/miui/server/MiuiCompatModePackages;->URI_CLOUD_ALL_DATA_NOTIFY:Landroid/net/Uri;

    .line 69
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;)V
    .locals 9
    .param p1, "context"    # Landroid/content/Context;

    .line 157
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 73
    new-instance v0, Ljava/lang/Object;

    invoke-direct {v0}, Ljava/lang/Object;-><init>()V

    iput-object v0, p0, Lcom/miui/server/MiuiCompatModePackages;->mLock:Ljava/lang/Object;

    .line 75
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    iput-object v0, p0, Lcom/miui/server/MiuiCompatModePackages;->mPackages:Ljava/util/HashMap;

    .line 77
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    iput-object v0, p0, Lcom/miui/server/MiuiCompatModePackages;->mDefaultType:Ljava/util/HashMap;

    .line 79
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    iput-object v0, p0, Lcom/miui/server/MiuiCompatModePackages;->mNotchConfig:Ljava/util/HashMap;

    .line 81
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    iput-object v0, p0, Lcom/miui/server/MiuiCompatModePackages;->mNotchSpecialModePackages:Ljava/util/HashMap;

    .line 83
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    iput-object v0, p0, Lcom/miui/server/MiuiCompatModePackages;->mUserCutoutModePackages:Ljava/util/HashMap;

    .line 85
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    iput-object v0, p0, Lcom/miui/server/MiuiCompatModePackages;->mCloudCutoutModePackages:Ljava/util/HashMap;

    .line 88
    new-instance v0, Ljava/util/HashSet;

    invoke-direct {v0}, Ljava/util/HashSet;-><init>()V

    iput-object v0, p0, Lcom/miui/server/MiuiCompatModePackages;->mSuggestList:Ljava/util/HashSet;

    .line 89
    new-instance v0, Ljava/util/HashSet;

    invoke-direct {v0}, Ljava/util/HashSet;-><init>()V

    iput-object v0, p0, Lcom/miui/server/MiuiCompatModePackages;->mRestrictList:Ljava/util/HashSet;

    .line 90
    new-instance v1, Ljava/util/HashSet;

    invoke-direct {v1}, Ljava/util/HashSet;-><init>()V

    iput-object v1, p0, Lcom/miui/server/MiuiCompatModePackages;->mSupportNotchList:Ljava/util/HashSet;

    .line 108
    const/4 v1, 0x1

    iput-boolean v1, p0, Lcom/miui/server/MiuiCompatModePackages;->mNotifySuggestApps:Z

    .line 437
    new-instance v1, Lcom/miui/server/MiuiCompatModePackages$3;

    invoke-direct {v1, p0}, Lcom/miui/server/MiuiCompatModePackages$3;-><init>(Lcom/miui/server/MiuiCompatModePackages;)V

    iput-object v1, p0, Lcom/miui/server/MiuiCompatModePackages;->mBroadcastReceiver:Landroid/content/BroadcastReceiver;

    .line 158
    iput-object p1, p0, Lcom/miui/server/MiuiCompatModePackages;->mContext:Landroid/content/Context;

    .line 160
    const-string v1, "android.dpi.cts"

    invoke-virtual {v0, v1}, Ljava/util/HashSet;->add(Ljava/lang/Object;)Z

    .line 162
    nop

    .line 184
    new-instance v0, Ljava/io/File;

    invoke-static {}, Landroid/os/Environment;->getDataDirectory()Ljava/io/File;

    move-result-object v1

    const-string/jumbo v2, "system"

    invoke-direct {v0, v1, v2}, Ljava/io/File;-><init>(Ljava/io/File;Ljava/lang/String;)V

    .line 185
    .local v0, "systemDir":Ljava/io/File;
    new-instance v1, Landroid/util/AtomicFile;

    new-instance v2, Ljava/io/File;

    const-string v3, "miui-packages-compat.xml"

    invoke-direct {v2, v0, v3}, Ljava/io/File;-><init>(Ljava/io/File;Ljava/lang/String;)V

    invoke-direct {v1, v2}, Landroid/util/AtomicFile;-><init>(Ljava/io/File;)V

    iput-object v1, p0, Lcom/miui/server/MiuiCompatModePackages;->mFile:Landroid/util/AtomicFile;

    .line 186
    new-instance v1, Landroid/util/AtomicFile;

    new-instance v2, Ljava/io/File;

    const-string v3, "miui-specail-mode-v2.xml"

    invoke-direct {v2, v0, v3}, Ljava/io/File;-><init>(Ljava/io/File;Ljava/lang/String;)V

    invoke-direct {v1, v2}, Landroid/util/AtomicFile;-><init>(Ljava/io/File;)V

    iput-object v1, p0, Lcom/miui/server/MiuiCompatModePackages;->mSpecialModeFile:Landroid/util/AtomicFile;

    .line 187
    new-instance v1, Landroid/util/AtomicFile;

    new-instance v2, Ljava/io/File;

    const-string v3, "cutout-mode.xml"

    invoke-direct {v2, v0, v3}, Ljava/io/File;-><init>(Ljava/io/File;Ljava/lang/String;)V

    invoke-direct {v1, v2}, Landroid/util/AtomicFile;-><init>(Ljava/io/File;)V

    iput-object v1, p0, Lcom/miui/server/MiuiCompatModePackages;->mCutoutModeFile:Landroid/util/AtomicFile;

    .line 189
    new-instance v1, Lcom/miui/server/MiuiCompatModePackages$CompatHandler;

    invoke-static {}, Lcom/android/server/MiuiFgThread;->getHandler()Landroid/os/Handler;

    move-result-object v2

    invoke-virtual {v2}, Landroid/os/Handler;->getLooper()Landroid/os/Looper;

    move-result-object v2

    invoke-direct {v1, p0, v2}, Lcom/miui/server/MiuiCompatModePackages$CompatHandler;-><init>(Lcom/miui/server/MiuiCompatModePackages;Landroid/os/Looper;)V

    iput-object v1, p0, Lcom/miui/server/MiuiCompatModePackages;->mHandler:Lcom/miui/server/MiuiCompatModePackages$CompatHandler;

    .line 190
    const/16 v2, 0x65

    invoke-virtual {v1, v2}, Lcom/miui/server/MiuiCompatModePackages$CompatHandler;->sendEmptyMessage(I)Z

    .line 192
    new-instance v2, Landroid/content/IntentFilter;

    invoke-direct {v2}, Landroid/content/IntentFilter;-><init>()V

    .line 193
    .local v2, "filter":Landroid/content/IntentFilter;
    const-string v3, "android.intent.action.PACKAGE_ADDED"

    invoke-virtual {v2, v3}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    .line 194
    const-string v3, "android.intent.action.PACKAGE_REMOVED"

    invoke-virtual {v2, v3}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    .line 195
    const-string v3, "package"

    invoke-virtual {v2, v3}, Landroid/content/IntentFilter;->addDataScheme(Ljava/lang/String;)V

    .line 196
    iget-object v4, p0, Lcom/miui/server/MiuiCompatModePackages;->mBroadcastReceiver:Landroid/content/BroadcastReceiver;

    sget-object v5, Landroid/os/UserHandle;->ALL:Landroid/os/UserHandle;

    const/4 v7, 0x0

    invoke-static {}, Lcom/android/server/MiuiFgThread;->getHandler()Landroid/os/Handler;

    move-result-object v8

    move-object v3, p1

    move-object v6, v2

    invoke-virtual/range {v3 .. v8}, Landroid/content/Context;->registerReceiverAsUser(Landroid/content/BroadcastReceiver;Landroid/os/UserHandle;Landroid/content/IntentFilter;Ljava/lang/String;Landroid/os/Handler;)Landroid/content/Intent;

    .line 198
    invoke-direct {p0}, Lcom/miui/server/MiuiCompatModePackages;->getDeviceAspect()F

    .line 199
    new-instance v3, Lcom/miui/server/MiuiCompatModePackages$1;

    invoke-direct {v3, p0, v1}, Lcom/miui/server/MiuiCompatModePackages$1;-><init>(Lcom/miui/server/MiuiCompatModePackages;Landroid/os/Handler;)V

    iput-object v3, p0, Lcom/miui/server/MiuiCompatModePackages;->mCloudDataObserver:Landroid/database/ContentObserver;

    .line 205
    new-instance v1, Landroid/os/Handler;

    invoke-static {}, Landroid/os/Looper;->getMainLooper()Landroid/os/Looper;

    move-result-object v3

    invoke-direct {v1, v3}, Landroid/os/Handler;-><init>(Landroid/os/Looper;)V

    iput-object v1, p0, Lcom/miui/server/MiuiCompatModePackages;->mMainHandler:Landroid/os/Handler;

    .line 206
    new-instance v3, Lcom/miui/server/MiuiCompatModePackages$2;

    invoke-direct {v3, p0}, Lcom/miui/server/MiuiCompatModePackages$2;-><init>(Lcom/miui/server/MiuiCompatModePackages;)V

    invoke-virtual {v1, v3}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    .line 218
    return-void
.end method

.method private getDefaultMode(Ljava/lang/String;)I
    .locals 1
    .param p1, "pkg"    # Ljava/lang/String;

    .line 666
    invoke-direct {p0, p1}, Lcom/miui/server/MiuiCompatModePackages;->isDefaultRestrict(Ljava/lang/String;)Z

    move-result v0

    return v0
.end method

.method private getDefaultNotchConfig(Ljava/lang/String;)I
    .locals 4
    .param p1, "packageName"    # Ljava/lang/String;

    .line 735
    iget-object v0, p0, Lcom/miui/server/MiuiCompatModePackages;->mLock:Ljava/lang/Object;

    monitor-enter v0

    .line 736
    :try_start_0
    iget-object v1, p0, Lcom/miui/server/MiuiCompatModePackages;->mNotchConfig:Ljava/util/HashMap;

    invoke-virtual {v1, p1}, Ljava/util/HashMap;->containsKey(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 737
    iget-object v1, p0, Lcom/miui/server/MiuiCompatModePackages;->mNotchConfig:Ljava/util/HashMap;

    invoke-virtual {v1, p1}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/Integer;

    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v1

    monitor-exit v0

    return v1

    .line 739
    :cond_0
    monitor-exit v0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 741
    invoke-direct {p0, p1}, Lcom/miui/server/MiuiCompatModePackages;->resolveNotchConfig(Ljava/lang/String;)I

    move-result v1

    .line 742
    .local v1, "type":I
    iget-object v2, p0, Lcom/miui/server/MiuiCompatModePackages;->mLock:Ljava/lang/Object;

    monitor-enter v2

    .line 743
    :try_start_1
    iget-object v0, p0, Lcom/miui/server/MiuiCompatModePackages;->mNotchConfig:Ljava/util/HashMap;

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    invoke-virtual {v0, p1, v3}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 744
    monitor-exit v2

    .line 745
    return v1

    .line 744
    :catchall_0
    move-exception v0

    monitor-exit v2
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    throw v0

    .line 739
    .end local v1    # "type":I
    :catchall_1
    move-exception v1

    :try_start_2
    monitor-exit v0
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    throw v1
.end method

.method private getDeviceAspect()F
    .locals 6

    .line 653
    iget v0, p0, Lcom/miui/server/MiuiCompatModePackages;->mDefaultAspect:F

    const/4 v1, 0x0

    cmpg-float v0, v0, v1

    if-gtz v0, :cond_1

    .line 654
    iget-object v0, p0, Lcom/miui/server/MiuiCompatModePackages;->mContext:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getDisplay()Landroid/view/Display;

    move-result-object v0

    .line 655
    .local v0, "display":Landroid/view/Display;
    new-instance v2, Landroid/graphics/Point;

    invoke-direct {v2}, Landroid/graphics/Point;-><init>()V

    .line 656
    .local v2, "point":Landroid/graphics/Point;
    invoke-virtual {v0, v2}, Landroid/view/Display;->getRealSize(Landroid/graphics/Point;)V

    .line 657
    iget v3, v2, Landroid/graphics/Point;->x:I

    iget v4, v2, Landroid/graphics/Point;->y:I

    invoke-static {v3, v4}, Ljava/lang/Math;->min(II)I

    move-result v3

    .line 658
    .local v3, "min":I
    iget v4, v2, Landroid/graphics/Point;->x:I

    iget v5, v2, Landroid/graphics/Point;->y:I

    invoke-static {v4, v5}, Ljava/lang/Math;->max(II)I

    move-result v4

    .line 659
    .local v4, "max":I
    if-nez v3, :cond_0

    goto :goto_0

    :cond_0
    const/high16 v1, 0x3f800000    # 1.0f

    int-to-float v5, v4

    mul-float/2addr v5, v1

    int-to-float v1, v3

    div-float v1, v5, v1

    .line 660
    .local v1, "ratio":F
    :goto_0
    iput v1, p0, Lcom/miui/server/MiuiCompatModePackages;->mDefaultAspect:F

    .line 662
    .end local v0    # "display":Landroid/view/Display;
    .end local v1    # "ratio":F
    .end local v2    # "point":Landroid/graphics/Point;
    .end local v3    # "min":I
    .end local v4    # "max":I
    :cond_1
    iget v0, p0, Lcom/miui/server/MiuiCompatModePackages;->mDefaultAspect:F

    return v0
.end method

.method private getPackageMode(Ljava/lang/String;)F
    .locals 3
    .param p1, "packageName"    # Ljava/lang/String;

    .line 457
    const/4 v0, 0x0

    .line 458
    .local v0, "mode":Ljava/lang/Integer;
    iget-object v1, p0, Lcom/miui/server/MiuiCompatModePackages;->mLock:Ljava/lang/Object;

    monitor-enter v1

    .line 459
    :try_start_0
    iget-object v2, p0, Lcom/miui/server/MiuiCompatModePackages;->mPackages:Ljava/util/HashMap;

    invoke-virtual {v2, p1}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/lang/Integer;

    move-object v0, v2

    .line 460
    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 461
    if-eqz v0, :cond_0

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v1

    goto :goto_0

    :cond_0
    invoke-direct {p0, p1}, Lcom/miui/server/MiuiCompatModePackages;->getDefaultMode(Ljava/lang/String;)I

    move-result v1

    :goto_0
    int-to-float v1, v1

    return v1

    .line 460
    :catchall_0
    move-exception v2

    :try_start_1
    monitor-exit v1
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    throw v2
.end method

.method private getSpecialMode(Ljava/lang/String;)I
    .locals 3
    .param p1, "packageName"    # Ljava/lang/String;

    .line 465
    const/4 v0, 0x0

    .line 466
    .local v0, "mode":Ljava/lang/Integer;
    iget-object v1, p0, Lcom/miui/server/MiuiCompatModePackages;->mLock:Ljava/lang/Object;

    monitor-enter v1

    .line 467
    :try_start_0
    iget-object v2, p0, Lcom/miui/server/MiuiCompatModePackages;->mNotchSpecialModePackages:Ljava/util/HashMap;

    invoke-virtual {v2, p1}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/lang/Integer;

    move-object v0, v2

    .line 468
    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 469
    if-eqz v0, :cond_0

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v1

    goto :goto_0

    :cond_0
    const/4 v1, 0x0

    :goto_0
    return v1

    .line 468
    :catchall_0
    move-exception v2

    :try_start_1
    monitor-exit v1
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    throw v2
.end method

.method private gotoMaxAspectSettings()V
    .locals 3

    .line 935
    :try_start_0
    new-instance v0, Landroid/content/Intent;

    invoke-direct {v0}, Landroid/content/Intent;-><init>()V

    .line 936
    .local v0, "intent":Landroid/content/Intent;
    const-string v1, "com.android.settings"

    const-string v2, "com.android.settings.SubSettings"

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->setClassName(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 937
    const-string v1, ":settings:show_fragment"

    const-string v2, "com.android.settings.MaxAspectRatioSettings"

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 938
    const/high16 v1, 0x10000000

    invoke-virtual {v0, v1}, Landroid/content/Intent;->addFlags(I)Landroid/content/Intent;

    .line 940
    iget-object v1, p0, Lcom/miui/server/MiuiCompatModePackages;->mContext:Landroid/content/Context;

    sget-object v2, Landroid/os/UserHandle;->CURRENT:Landroid/os/UserHandle;

    invoke-virtual {v1, v0, v2}, Landroid/content/Context;->startActivityAsUser(Landroid/content/Intent;Landroid/os/UserHandle;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 943
    .end local v0    # "intent":Landroid/content/Intent;
    goto :goto_0

    .line 941
    :catch_0
    move-exception v0

    .line 942
    .local v0, "e":Ljava/lang/Exception;
    const-string v1, "MiuiCompatModePackages"

    const-string v2, "error when goto max aspect settings"

    invoke-static {v1, v2, v0}, Landroid/util/Slog;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    .line 944
    .end local v0    # "e":Ljava/lang/Exception;
    :goto_0
    return-void
.end method

.method private handleDontShowAgain()V
    .locals 2

    .line 928
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/miui/server/MiuiCompatModePackages;->mNotifySuggestApps:Z

    .line 929
    iget-object v0, p0, Lcom/miui/server/MiuiCompatModePackages;->mHandler:Lcom/miui/server/MiuiCompatModePackages$CompatHandler;

    const/16 v1, 0x67

    invoke-virtual {v0, v1}, Lcom/miui/server/MiuiCompatModePackages$CompatHandler;->sendEmptyMessage(I)Z

    .line 930
    iget-object v0, p0, Lcom/miui/server/MiuiCompatModePackages;->mHandler:Lcom/miui/server/MiuiCompatModePackages$CompatHandler;

    const/16 v1, 0x64

    invoke-virtual {v0, v1}, Lcom/miui/server/MiuiCompatModePackages$CompatHandler;->sendEmptyMessage(I)Z

    .line 931
    return-void
.end method

.method private handleRegisterObservers()V
    .locals 3

    .line 893
    const-string v0, "MiuiCompatModePackages"

    iget-boolean v1, p0, Lcom/miui/server/MiuiCompatModePackages;->mNotifySuggestApps:Z

    if-nez v1, :cond_0

    .line 894
    return-void

    .line 897
    :cond_0
    new-instance v1, Lcom/miui/server/MiuiCompatModePackages$4;

    invoke-direct {v1, p0}, Lcom/miui/server/MiuiCompatModePackages$4;-><init>(Lcom/miui/server/MiuiCompatModePackages;)V

    iput-object v1, p0, Lcom/miui/server/MiuiCompatModePackages;->mProcessObserver:Landroid/app/IMiuiProcessObserver;

    .line 906
    :try_start_0
    const-string v1, "registering process observer..."

    invoke-static {v0, v1}, Landroid/util/Slog;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 907
    invoke-static {}, Landroid/app/ActivityManagerNative;->getDefault()Landroid/app/IActivityManager;

    move-result-object v1

    iget-object v2, p0, Lcom/miui/server/MiuiCompatModePackages;->mProcessObserver:Landroid/app/IMiuiProcessObserver;

    invoke-interface {v1, v2}, Landroid/app/IActivityManager;->registerProcessObserver(Landroid/app/IProcessObserver;)V
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    .line 911
    goto :goto_0

    .line 908
    :catch_0
    move-exception v1

    .line 909
    .local v1, "e":Landroid/os/RemoteException;
    const/4 v2, 0x0

    iput-object v2, p0, Lcom/miui/server/MiuiCompatModePackages;->mProcessObserver:Landroid/app/IMiuiProcessObserver;

    .line 910
    const-string v2, "error when registering process observer"

    invoke-static {v0, v2, v1}, Landroid/util/Slog;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    .line 912
    .end local v1    # "e":Landroid/os/RemoteException;
    :goto_0
    return-void
.end method

.method private handleRemovePackage(Ljava/lang/String;)V
    .locals 0
    .param p1, "packageName"    # Ljava/lang/String;

    .line 698
    invoke-direct {p0, p1}, Lcom/miui/server/MiuiCompatModePackages;->removePackage(Ljava/lang/String;)V

    .line 699
    invoke-direct {p0, p1}, Lcom/miui/server/MiuiCompatModePackages;->removeSpecialModePackage(Ljava/lang/String;)V

    .line 700
    return-void
.end method

.method private handleUnregisterObservers()V
    .locals 4

    .line 915
    iget-object v0, p0, Lcom/miui/server/MiuiCompatModePackages;->mProcessObserver:Landroid/app/IMiuiProcessObserver;

    if-eqz v0, :cond_0

    .line 916
    const-string/jumbo v0, "unregistering process observer..."

    const-string v1, "MiuiCompatModePackages"

    invoke-static {v1, v0}, Landroid/util/Slog;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 918
    const/4 v0, 0x0

    :try_start_0
    invoke-static {}, Landroid/app/ActivityManagerNative;->getDefault()Landroid/app/IActivityManager;

    move-result-object v2

    iget-object v3, p0, Lcom/miui/server/MiuiCompatModePackages;->mProcessObserver:Landroid/app/IMiuiProcessObserver;

    invoke-interface {v2, v3}, Landroid/app/IActivityManager;->unregisterProcessObserver(Landroid/app/IProcessObserver;)V
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 922
    nop

    :goto_0
    iput-object v0, p0, Lcom/miui/server/MiuiCompatModePackages;->mProcessObserver:Landroid/app/IMiuiProcessObserver;

    .line 923
    goto :goto_2

    .line 922
    :catchall_0
    move-exception v1

    goto :goto_1

    .line 919
    :catch_0
    move-exception v2

    .line 920
    .local v2, "e":Landroid/os/RemoteException;
    :try_start_1
    const-string v3, "error when unregistering process observer"

    invoke-static {v1, v3, v2}, Landroid/util/Slog;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 922
    nop

    .end local v2    # "e":Landroid/os/RemoteException;
    goto :goto_0

    :goto_1
    iput-object v0, p0, Lcom/miui/server/MiuiCompatModePackages;->mProcessObserver:Landroid/app/IMiuiProcessObserver;

    .line 923
    throw v1

    .line 925
    :cond_0
    :goto_2
    return-void
.end method

.method private handleUpdatePackage(Ljava/lang/String;)V
    .locals 5
    .param p1, "packageName"    # Ljava/lang/String;

    .line 703
    iget-object v0, p0, Lcom/miui/server/MiuiCompatModePackages;->mLock:Ljava/lang/Object;

    monitor-enter v0

    .line 704
    :try_start_0
    iget-object v1, p0, Lcom/miui/server/MiuiCompatModePackages;->mDefaultType:Ljava/util/HashMap;

    invoke-virtual {v1, p1}, Ljava/util/HashMap;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    .line 705
    iget-object v1, p0, Lcom/miui/server/MiuiCompatModePackages;->mNotchConfig:Ljava/util/HashMap;

    invoke-virtual {v1, p1}, Ljava/util/HashMap;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    .line 706
    monitor-exit v0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 707
    invoke-direct {p0, p1}, Lcom/miui/server/MiuiCompatModePackages;->isDefaultRestrict(Ljava/lang/String;)Z

    move-result v0

    .line 708
    .local v0, "isDefaultRestrict":Z
    invoke-virtual {p0, p1}, Lcom/miui/server/MiuiCompatModePackages;->isRestrictAspect(Ljava/lang/String;)Z

    move-result v1

    .line 709
    .local v1, "isRestrict":Z
    if-eq v0, v1, :cond_0

    .line 710
    invoke-virtual {p0, p1}, Lcom/miui/server/MiuiCompatModePackages;->getDefaultAspectType(Ljava/lang/String;)I

    move-result v2

    const/4 v3, 0x1

    if-ne v2, v3, :cond_1

    .line 711
    :cond_0
    const-string v2, "MiuiCompatModePackages"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "package "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, " updated, removing config"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Landroid/util/Slog;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 712
    invoke-direct {p0, p1}, Lcom/miui/server/MiuiCompatModePackages;->removePackage(Ljava/lang/String;)V

    .line 714
    :cond_1
    return-void

    .line 706
    .end local v0    # "isDefaultRestrict":Z
    .end local v1    # "isRestrict":Z
    :catchall_0
    move-exception v1

    :try_start_1
    monitor-exit v0
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    throw v1
.end method

.method private isDefaultRestrict(Ljava/lang/String;)Z
    .locals 2
    .param p1, "pkg"    # Ljava/lang/String;

    .line 648
    invoke-virtual {p0, p1}, Lcom/miui/server/MiuiCompatModePackages;->getDefaultAspectType(Ljava/lang/String;)I

    move-result v0

    .line 649
    .local v0, "type":I
    const/4 v1, 0x4

    if-eq v0, v1, :cond_1

    const/4 v1, 0x5

    if-ne v0, v1, :cond_0

    goto :goto_0

    :cond_0
    const/4 v1, 0x0

    goto :goto_1

    :cond_1
    :goto_0
    const/4 v1, 0x1

    :goto_1
    return v1
.end method

.method private isNotchSpecailMode(Ljava/lang/String;)Z
    .locals 1
    .param p1, "pkg"    # Ljava/lang/String;

    .line 793
    invoke-direct {p0, p1}, Lcom/miui/server/MiuiCompatModePackages;->getSpecialMode(Ljava/lang/String;)I

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    :goto_0
    return v0
.end method

.method private readCutoutModeConfig()V
    .locals 11

    .line 316
    const/4 v0, 0x0

    .line 318
    .local v0, "fis":Ljava/io/FileInputStream;
    :try_start_0
    iget-object v1, p0, Lcom/miui/server/MiuiCompatModePackages;->mCutoutModeFile:Landroid/util/AtomicFile;

    invoke-virtual {v1}, Landroid/util/AtomicFile;->openRead()Ljava/io/FileInputStream;

    move-result-object v1

    move-object v0, v1

    .line 319
    invoke-static {}, Landroid/util/Xml;->newPullParser()Lorg/xmlpull/v1/XmlPullParser;

    move-result-object v1

    .line 320
    .local v1, "parser":Lorg/xmlpull/v1/XmlPullParser;
    sget-object v2, Ljava/nio/charset/StandardCharsets;->UTF_8:Ljava/nio/charset/Charset;

    invoke-virtual {v2}, Ljava/nio/charset/Charset;->name()Ljava/lang/String;

    move-result-object v2

    invoke-interface {v1, v0, v2}, Lorg/xmlpull/v1/XmlPullParser;->setInput(Ljava/io/InputStream;Ljava/lang/String;)V

    .line 321
    invoke-interface {v1}, Lorg/xmlpull/v1/XmlPullParser;->getEventType()I

    move-result v2

    .line 322
    .local v2, "eventType":I
    :goto_0
    const/4 v3, 0x1

    const/4 v4, 0x2

    if-eq v2, v4, :cond_0

    if-eq v2, v3, :cond_0

    .line 324
    invoke-interface {v1}, Lorg/xmlpull/v1/XmlPullParser;->next()I

    move-result v3
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_3
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    move v2, v3

    goto :goto_0

    .line 326
    :cond_0
    if-ne v2, v3, :cond_2

    .line 363
    if-eqz v0, :cond_1

    .line 365
    :try_start_1
    invoke-virtual {v0}, Ljava/io/FileInputStream;->close()V
    :try_end_1
    .catch Ljava/io/IOException; {:try_start_1 .. :try_end_1} :catch_0

    .line 367
    goto :goto_1

    .line 366
    :catch_0
    move-exception v3

    .line 327
    :cond_1
    :goto_1
    return-void

    .line 330
    :cond_2
    :try_start_2
    new-instance v5, Ljava/util/HashMap;

    invoke-direct {v5}, Ljava/util/HashMap;-><init>()V

    .line 331
    .local v5, "pkgs":Ljava/util/HashMap;, "Ljava/util/HashMap<Ljava/lang/String;Ljava/lang/Integer;>;"
    invoke-interface {v1}, Lorg/xmlpull/v1/XmlPullParser;->getName()Ljava/lang/String;

    move-result-object v6

    .line 332
    .local v6, "tagName":Ljava/lang/String;
    const-string v7, "cutout-mode"

    invoke-virtual {v7, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v7

    if-eqz v7, :cond_5

    .line 333
    invoke-interface {v1}, Lorg/xmlpull/v1/XmlPullParser;->next()I

    move-result v7

    move v2, v7

    .line 335
    :cond_3
    if-ne v2, v4, :cond_4

    .line 336
    invoke-interface {v1}, Lorg/xmlpull/v1/XmlPullParser;->getName()Ljava/lang/String;

    move-result-object v7

    move-object v6, v7

    .line 337
    invoke-interface {v1}, Lorg/xmlpull/v1/XmlPullParser;->getDepth()I

    move-result v7

    if-ne v7, v4, :cond_4

    .line 338
    const-string v7, "pkg"

    invoke-virtual {v7, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v7

    if-eqz v7, :cond_4

    .line 339
    const-string v7, "name"

    const/4 v8, 0x0

    invoke-interface {v1, v8, v7}, Lorg/xmlpull/v1/XmlPullParser;->getAttributeValue(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v7

    .line 340
    .local v7, "pkg":Ljava/lang/String;
    if-eqz v7, :cond_4

    .line 341
    const-string v9, "mode"

    invoke-interface {v1, v8, v9}, Lorg/xmlpull/v1/XmlPullParser;->getAttributeValue(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v8
    :try_end_2
    .catch Ljava/lang/Exception; {:try_start_2 .. :try_end_2} :catch_3
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 342
    .local v8, "mode":Ljava/lang/String;
    const/4 v9, 0x0

    .line 343
    .local v9, "modeInt":I
    if-eqz v8, :cond_4

    .line 345
    :try_start_3
    invoke-static {v8}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v10

    move v9, v10

    .line 346
    invoke-static {v9}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v10

    invoke-virtual {v5, v7, v10}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;
    :try_end_3
    .catch Ljava/lang/NumberFormatException; {:try_start_3 .. :try_end_3} :catch_1
    .catch Ljava/lang/Exception; {:try_start_3 .. :try_end_3} :catch_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    .line 348
    goto :goto_2

    .line 347
    :catch_1
    move-exception v10

    .line 354
    .end local v7    # "pkg":Ljava/lang/String;
    .end local v8    # "mode":Ljava/lang/String;
    .end local v9    # "modeInt":I
    :cond_4
    :goto_2
    :try_start_4
    invoke-interface {v1}, Lorg/xmlpull/v1/XmlPullParser;->next()I

    move-result v7

    move v2, v7

    .line 355
    if-ne v2, v3, :cond_3

    .line 357
    :cond_5
    iget-object v3, p0, Lcom/miui/server/MiuiCompatModePackages;->mLock:Ljava/lang/Object;

    monitor-enter v3
    :try_end_4
    .catch Ljava/lang/Exception; {:try_start_4 .. :try_end_4} :catch_3
    .catchall {:try_start_4 .. :try_end_4} :catchall_1

    .line 358
    :try_start_5
    iget-object v4, p0, Lcom/miui/server/MiuiCompatModePackages;->mUserCutoutModePackages:Ljava/util/HashMap;

    invoke-virtual {v4, v5}, Ljava/util/HashMap;->putAll(Ljava/util/Map;)V

    .line 359
    monitor-exit v3
    :try_end_5
    .catchall {:try_start_5 .. :try_end_5} :catchall_0

    .line 363
    .end local v1    # "parser":Lorg/xmlpull/v1/XmlPullParser;
    .end local v2    # "eventType":I
    .end local v5    # "pkgs":Ljava/util/HashMap;, "Ljava/util/HashMap<Ljava/lang/String;Ljava/lang/Integer;>;"
    .end local v6    # "tagName":Ljava/lang/String;
    if-eqz v0, :cond_6

    .line 365
    :try_start_6
    invoke-virtual {v0}, Ljava/io/FileInputStream;->close()V
    :try_end_6
    .catch Ljava/io/IOException; {:try_start_6 .. :try_end_6} :catch_2

    .line 367
    :goto_3
    goto :goto_4

    .line 366
    :catch_2
    move-exception v1

    goto :goto_3

    .line 359
    .restart local v1    # "parser":Lorg/xmlpull/v1/XmlPullParser;
    .restart local v2    # "eventType":I
    .restart local v5    # "pkgs":Ljava/util/HashMap;, "Ljava/util/HashMap<Ljava/lang/String;Ljava/lang/Integer;>;"
    .restart local v6    # "tagName":Ljava/lang/String;
    :catchall_0
    move-exception v4

    :try_start_7
    monitor-exit v3
    :try_end_7
    .catchall {:try_start_7 .. :try_end_7} :catchall_0

    .end local v0    # "fis":Ljava/io/FileInputStream;
    .end local p0    # "this":Lcom/miui/server/MiuiCompatModePackages;
    :try_start_8
    throw v4
    :try_end_8
    .catch Ljava/lang/Exception; {:try_start_8 .. :try_end_8} :catch_3
    .catchall {:try_start_8 .. :try_end_8} :catchall_1

    .line 363
    .end local v1    # "parser":Lorg/xmlpull/v1/XmlPullParser;
    .end local v2    # "eventType":I
    .end local v5    # "pkgs":Ljava/util/HashMap;, "Ljava/util/HashMap<Ljava/lang/String;Ljava/lang/Integer;>;"
    .end local v6    # "tagName":Ljava/lang/String;
    .restart local v0    # "fis":Ljava/io/FileInputStream;
    .restart local p0    # "this":Lcom/miui/server/MiuiCompatModePackages;
    :catchall_1
    move-exception v1

    goto :goto_5

    .line 360
    :catch_3
    move-exception v1

    .line 361
    .local v1, "e":Ljava/lang/Exception;
    :try_start_9
    const-string v2, "MiuiCompatModePackages"

    const-string v3, "Error reading compat-packages"

    invoke-static {v2, v3, v1}, Landroid/util/Slog;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I
    :try_end_9
    .catchall {:try_start_9 .. :try_end_9} :catchall_1

    .line 363
    nop

    .end local v1    # "e":Ljava/lang/Exception;
    if-eqz v0, :cond_6

    .line 365
    :try_start_a
    invoke-virtual {v0}, Ljava/io/FileInputStream;->close()V
    :try_end_a
    .catch Ljava/io/IOException; {:try_start_a .. :try_end_a} :catch_2

    goto :goto_3

    .line 370
    :cond_6
    :goto_4
    return-void

    .line 363
    :goto_5
    if-eqz v0, :cond_7

    .line 365
    :try_start_b
    invoke-virtual {v0}, Ljava/io/FileInputStream;->close()V
    :try_end_b
    .catch Ljava/io/IOException; {:try_start_b .. :try_end_b} :catch_4

    .line 367
    goto :goto_6

    .line 366
    :catch_4
    move-exception v2

    .line 369
    :cond_7
    :goto_6
    throw v1
.end method

.method private readPackagesConfig()V
    .locals 11

    .line 373
    const/4 v0, 0x0

    .line 375
    .local v0, "fis":Ljava/io/FileInputStream;
    :try_start_0
    iget-object v1, p0, Lcom/miui/server/MiuiCompatModePackages;->mFile:Landroid/util/AtomicFile;

    invoke-virtual {v1}, Landroid/util/AtomicFile;->openRead()Ljava/io/FileInputStream;

    move-result-object v1

    move-object v0, v1

    .line 376
    invoke-static {}, Landroid/util/Xml;->newPullParser()Lorg/xmlpull/v1/XmlPullParser;

    move-result-object v1

    .line 377
    .local v1, "parser":Lorg/xmlpull/v1/XmlPullParser;
    sget-object v2, Ljava/nio/charset/StandardCharsets;->UTF_8:Ljava/nio/charset/Charset;

    invoke-virtual {v2}, Ljava/nio/charset/Charset;->name()Ljava/lang/String;

    move-result-object v2

    invoke-interface {v1, v0, v2}, Lorg/xmlpull/v1/XmlPullParser;->setInput(Ljava/io/InputStream;Ljava/lang/String;)V

    .line 378
    invoke-interface {v1}, Lorg/xmlpull/v1/XmlPullParser;->getEventType()I

    move-result v2

    .line 379
    .local v2, "eventType":I
    :goto_0
    const/4 v3, 0x1

    const/4 v4, 0x2

    if-eq v2, v4, :cond_0

    if-eq v2, v3, :cond_0

    .line 381
    invoke-interface {v1}, Lorg/xmlpull/v1/XmlPullParser;->next()I

    move-result v3
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_3
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    move v2, v3

    goto :goto_0

    .line 383
    :cond_0
    if-ne v2, v3, :cond_2

    .line 423
    if-eqz v0, :cond_1

    .line 425
    :try_start_1
    invoke-virtual {v0}, Ljava/io/FileInputStream;->close()V
    :try_end_1
    .catch Ljava/io/IOException; {:try_start_1 .. :try_end_1} :catch_0

    .line 427
    goto :goto_1

    .line 426
    :catch_0
    move-exception v3

    .line 384
    :cond_1
    :goto_1
    return-void

    .line 387
    :cond_2
    :try_start_2
    new-instance v5, Ljava/util/HashMap;

    invoke-direct {v5}, Ljava/util/HashMap;-><init>()V

    .line 388
    .local v5, "pkgs":Ljava/util/HashMap;, "Ljava/util/HashMap<Ljava/lang/String;Ljava/lang/Integer;>;"
    invoke-interface {v1}, Lorg/xmlpull/v1/XmlPullParser;->getName()Ljava/lang/String;

    move-result-object v6

    .line 389
    .local v6, "tagName":Ljava/lang/String;
    const-string v7, "compat-packages"

    invoke-virtual {v7, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v7

    if-eqz v7, :cond_8

    .line 390
    invoke-interface {v1}, Lorg/xmlpull/v1/XmlPullParser;->next()I

    move-result v7

    move v2, v7

    .line 392
    :cond_3
    if-ne v2, v4, :cond_7

    .line 393
    invoke-interface {v1}, Lorg/xmlpull/v1/XmlPullParser;->getName()Ljava/lang/String;

    move-result-object v7

    move-object v6, v7

    .line 394
    invoke-interface {v1}, Lorg/xmlpull/v1/XmlPullParser;->getDepth()I

    move-result v7

    if-ne v7, v4, :cond_7

    .line 395
    const-string v7, "pkg"

    invoke-virtual {v7, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v7

    const/4 v8, 0x0

    if-eqz v7, :cond_5

    .line 396
    const-string v7, "name"

    invoke-interface {v1, v8, v7}, Lorg/xmlpull/v1/XmlPullParser;->getAttributeValue(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v7

    .line 397
    .local v7, "pkg":Ljava/lang/String;
    if-eqz v7, :cond_6

    .line 398
    const-string v9, "mode"

    invoke-interface {v1, v8, v9}, Lorg/xmlpull/v1/XmlPullParser;->getAttributeValue(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v8
    :try_end_2
    .catch Ljava/lang/Exception; {:try_start_2 .. :try_end_2} :catch_3
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 399
    .local v8, "mode":Ljava/lang/String;
    const/4 v9, 0x0

    .line 400
    .local v9, "modeInt":I
    if-eqz v8, :cond_4

    .line 402
    :try_start_3
    invoke-static {v8}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v10
    :try_end_3
    .catch Ljava/lang/NumberFormatException; {:try_start_3 .. :try_end_3} :catch_1
    .catch Ljava/lang/Exception; {:try_start_3 .. :try_end_3} :catch_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    move v9, v10

    .line 404
    goto :goto_2

    .line 403
    :catch_1
    move-exception v10

    .line 406
    :cond_4
    :goto_2
    :try_start_4
    invoke-static {v9}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v10

    invoke-virtual {v5, v7, v10}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    goto :goto_3

    .line 408
    .end local v7    # "pkg":Ljava/lang/String;
    .end local v8    # "mode":Ljava/lang/String;
    .end local v9    # "modeInt":I
    :cond_5
    const-string v7, "config"

    invoke-virtual {v7, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v7

    if-eqz v7, :cond_6

    .line 409
    const-string v7, "notifySuggestApps"

    invoke-interface {v1, v8, v7}, Lorg/xmlpull/v1/XmlPullParser;->getAttributeValue(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v7

    .line 410
    .local v7, "notifySuggestApps":Ljava/lang/String;
    invoke-static {v7}, Ljava/lang/Boolean;->valueOf(Ljava/lang/String;)Ljava/lang/Boolean;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v8

    iput-boolean v8, p0, Lcom/miui/server/MiuiCompatModePackages;->mNotifySuggestApps:Z

    goto :goto_4

    .line 408
    .end local v7    # "notifySuggestApps":Ljava/lang/String;
    :cond_6
    :goto_3
    nop

    .line 414
    :cond_7
    :goto_4
    invoke-interface {v1}, Lorg/xmlpull/v1/XmlPullParser;->next()I

    move-result v7

    move v2, v7

    .line 415
    if-ne v2, v3, :cond_3

    .line 417
    :cond_8
    iget-object v3, p0, Lcom/miui/server/MiuiCompatModePackages;->mLock:Ljava/lang/Object;

    monitor-enter v3
    :try_end_4
    .catch Ljava/lang/Exception; {:try_start_4 .. :try_end_4} :catch_3
    .catchall {:try_start_4 .. :try_end_4} :catchall_1

    .line 418
    :try_start_5
    iget-object v4, p0, Lcom/miui/server/MiuiCompatModePackages;->mPackages:Ljava/util/HashMap;

    invoke-virtual {v4, v5}, Ljava/util/HashMap;->putAll(Ljava/util/Map;)V

    .line 419
    monitor-exit v3
    :try_end_5
    .catchall {:try_start_5 .. :try_end_5} :catchall_0

    .line 423
    .end local v1    # "parser":Lorg/xmlpull/v1/XmlPullParser;
    .end local v2    # "eventType":I
    .end local v5    # "pkgs":Ljava/util/HashMap;, "Ljava/util/HashMap<Ljava/lang/String;Ljava/lang/Integer;>;"
    .end local v6    # "tagName":Ljava/lang/String;
    if-eqz v0, :cond_9

    .line 425
    :try_start_6
    invoke-virtual {v0}, Ljava/io/FileInputStream;->close()V
    :try_end_6
    .catch Ljava/io/IOException; {:try_start_6 .. :try_end_6} :catch_2

    .line 427
    :goto_5
    goto :goto_6

    .line 426
    :catch_2
    move-exception v1

    goto :goto_5

    .line 419
    .restart local v1    # "parser":Lorg/xmlpull/v1/XmlPullParser;
    .restart local v2    # "eventType":I
    .restart local v5    # "pkgs":Ljava/util/HashMap;, "Ljava/util/HashMap<Ljava/lang/String;Ljava/lang/Integer;>;"
    .restart local v6    # "tagName":Ljava/lang/String;
    :catchall_0
    move-exception v4

    :try_start_7
    monitor-exit v3
    :try_end_7
    .catchall {:try_start_7 .. :try_end_7} :catchall_0

    .end local v0    # "fis":Ljava/io/FileInputStream;
    .end local p0    # "this":Lcom/miui/server/MiuiCompatModePackages;
    :try_start_8
    throw v4
    :try_end_8
    .catch Ljava/lang/Exception; {:try_start_8 .. :try_end_8} :catch_3
    .catchall {:try_start_8 .. :try_end_8} :catchall_1

    .line 423
    .end local v1    # "parser":Lorg/xmlpull/v1/XmlPullParser;
    .end local v2    # "eventType":I
    .end local v5    # "pkgs":Ljava/util/HashMap;, "Ljava/util/HashMap<Ljava/lang/String;Ljava/lang/Integer;>;"
    .end local v6    # "tagName":Ljava/lang/String;
    .restart local v0    # "fis":Ljava/io/FileInputStream;
    .restart local p0    # "this":Lcom/miui/server/MiuiCompatModePackages;
    :catchall_1
    move-exception v1

    goto :goto_7

    .line 420
    :catch_3
    move-exception v1

    .line 421
    .local v1, "e":Ljava/lang/Exception;
    :try_start_9
    const-string v2, "MiuiCompatModePackages"

    const-string v3, "Error reading compat-packages"

    invoke-static {v2, v3, v1}, Landroid/util/Slog;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I
    :try_end_9
    .catchall {:try_start_9 .. :try_end_9} :catchall_1

    .line 423
    nop

    .end local v1    # "e":Ljava/lang/Exception;
    if-eqz v0, :cond_9

    .line 425
    :try_start_a
    invoke-virtual {v0}, Ljava/io/FileInputStream;->close()V
    :try_end_a
    .catch Ljava/io/IOException; {:try_start_a .. :try_end_a} :catch_2

    goto :goto_5

    .line 430
    :cond_9
    :goto_6
    return-void

    .line 423
    :goto_7
    if-eqz v0, :cond_a

    .line 425
    :try_start_b
    invoke-virtual {v0}, Ljava/io/FileInputStream;->close()V
    :try_end_b
    .catch Ljava/io/IOException; {:try_start_b .. :try_end_b} :catch_4

    .line 427
    goto :goto_8

    .line 426
    :catch_4
    move-exception v2

    .line 429
    :cond_a
    :goto_8
    throw v1
.end method

.method private readSpecialModeConfig()V
    .locals 11

    .line 259
    const/4 v0, 0x0

    .line 261
    .local v0, "fis":Ljava/io/FileInputStream;
    :try_start_0
    iget-object v1, p0, Lcom/miui/server/MiuiCompatModePackages;->mSpecialModeFile:Landroid/util/AtomicFile;

    invoke-virtual {v1}, Landroid/util/AtomicFile;->openRead()Ljava/io/FileInputStream;

    move-result-object v1

    move-object v0, v1

    .line 262
    invoke-static {}, Landroid/util/Xml;->newPullParser()Lorg/xmlpull/v1/XmlPullParser;

    move-result-object v1

    .line 263
    .local v1, "parser":Lorg/xmlpull/v1/XmlPullParser;
    sget-object v2, Ljava/nio/charset/StandardCharsets;->UTF_8:Ljava/nio/charset/Charset;

    invoke-virtual {v2}, Ljava/nio/charset/Charset;->name()Ljava/lang/String;

    move-result-object v2

    invoke-interface {v1, v0, v2}, Lorg/xmlpull/v1/XmlPullParser;->setInput(Ljava/io/InputStream;Ljava/lang/String;)V

    .line 264
    invoke-interface {v1}, Lorg/xmlpull/v1/XmlPullParser;->getEventType()I

    move-result v2

    .line 265
    .local v2, "eventType":I
    :goto_0
    const/4 v3, 0x1

    const/4 v4, 0x2

    if-eq v2, v4, :cond_0

    if-eq v2, v3, :cond_0

    .line 267
    invoke-interface {v1}, Lorg/xmlpull/v1/XmlPullParser;->next()I

    move-result v3
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_3
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    move v2, v3

    goto :goto_0

    .line 269
    :cond_0
    if-ne v2, v3, :cond_2

    .line 306
    if-eqz v0, :cond_1

    .line 308
    :try_start_1
    invoke-virtual {v0}, Ljava/io/FileInputStream;->close()V
    :try_end_1
    .catch Ljava/io/IOException; {:try_start_1 .. :try_end_1} :catch_0

    .line 310
    goto :goto_1

    .line 309
    :catch_0
    move-exception v3

    .line 270
    :cond_1
    :goto_1
    return-void

    .line 273
    :cond_2
    :try_start_2
    new-instance v5, Ljava/util/HashMap;

    invoke-direct {v5}, Ljava/util/HashMap;-><init>()V

    .line 274
    .local v5, "pkgs":Ljava/util/HashMap;, "Ljava/util/HashMap<Ljava/lang/String;Ljava/lang/Integer;>;"
    invoke-interface {v1}, Lorg/xmlpull/v1/XmlPullParser;->getName()Ljava/lang/String;

    move-result-object v6

    .line 275
    .local v6, "tagName":Ljava/lang/String;
    const-string/jumbo v7, "special-mode"

    invoke-virtual {v7, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v7

    if-eqz v7, :cond_6

    .line 276
    invoke-interface {v1}, Lorg/xmlpull/v1/XmlPullParser;->next()I

    move-result v7

    move v2, v7

    .line 278
    :cond_3
    if-ne v2, v4, :cond_5

    .line 279
    invoke-interface {v1}, Lorg/xmlpull/v1/XmlPullParser;->getName()Ljava/lang/String;

    move-result-object v7

    move-object v6, v7

    .line 280
    invoke-interface {v1}, Lorg/xmlpull/v1/XmlPullParser;->getDepth()I

    move-result v7

    if-ne v7, v4, :cond_5

    .line 281
    const-string v7, "pkg"

    invoke-virtual {v7, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v7

    if-eqz v7, :cond_5

    .line 282
    const-string v7, "name"

    const/4 v8, 0x0

    invoke-interface {v1, v8, v7}, Lorg/xmlpull/v1/XmlPullParser;->getAttributeValue(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v7

    .line 283
    .local v7, "pkg":Ljava/lang/String;
    if-eqz v7, :cond_5

    .line 284
    const-string v9, "mode"

    invoke-interface {v1, v8, v9}, Lorg/xmlpull/v1/XmlPullParser;->getAttributeValue(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v8
    :try_end_2
    .catch Ljava/lang/Exception; {:try_start_2 .. :try_end_2} :catch_3
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 285
    .local v8, "mode":Ljava/lang/String;
    const/4 v9, 0x0

    .line 286
    .local v9, "modeInt":I
    if-eqz v8, :cond_4

    .line 288
    :try_start_3
    invoke-static {v8}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v10
    :try_end_3
    .catch Ljava/lang/NumberFormatException; {:try_start_3 .. :try_end_3} :catch_1
    .catch Ljava/lang/Exception; {:try_start_3 .. :try_end_3} :catch_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    move v9, v10

    .line 290
    goto :goto_2

    .line 289
    :catch_1
    move-exception v10

    .line 292
    :cond_4
    :goto_2
    :try_start_4
    invoke-static {v9}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v10

    invoke-virtual {v5, v7, v10}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 297
    .end local v7    # "pkg":Ljava/lang/String;
    .end local v8    # "mode":Ljava/lang/String;
    .end local v9    # "modeInt":I
    :cond_5
    invoke-interface {v1}, Lorg/xmlpull/v1/XmlPullParser;->next()I

    move-result v7

    move v2, v7

    .line 298
    if-ne v2, v3, :cond_3

    .line 300
    :cond_6
    iget-object v3, p0, Lcom/miui/server/MiuiCompatModePackages;->mLock:Ljava/lang/Object;

    monitor-enter v3
    :try_end_4
    .catch Ljava/lang/Exception; {:try_start_4 .. :try_end_4} :catch_3
    .catchall {:try_start_4 .. :try_end_4} :catchall_1

    .line 301
    :try_start_5
    iget-object v4, p0, Lcom/miui/server/MiuiCompatModePackages;->mNotchSpecialModePackages:Ljava/util/HashMap;

    invoke-virtual {v4, v5}, Ljava/util/HashMap;->putAll(Ljava/util/Map;)V

    .line 302
    monitor-exit v3
    :try_end_5
    .catchall {:try_start_5 .. :try_end_5} :catchall_0

    .line 306
    .end local v1    # "parser":Lorg/xmlpull/v1/XmlPullParser;
    .end local v2    # "eventType":I
    .end local v5    # "pkgs":Ljava/util/HashMap;, "Ljava/util/HashMap<Ljava/lang/String;Ljava/lang/Integer;>;"
    .end local v6    # "tagName":Ljava/lang/String;
    if-eqz v0, :cond_7

    .line 308
    :try_start_6
    invoke-virtual {v0}, Ljava/io/FileInputStream;->close()V
    :try_end_6
    .catch Ljava/io/IOException; {:try_start_6 .. :try_end_6} :catch_2

    .line 310
    :goto_3
    goto :goto_4

    .line 309
    :catch_2
    move-exception v1

    goto :goto_3

    .line 302
    .restart local v1    # "parser":Lorg/xmlpull/v1/XmlPullParser;
    .restart local v2    # "eventType":I
    .restart local v5    # "pkgs":Ljava/util/HashMap;, "Ljava/util/HashMap<Ljava/lang/String;Ljava/lang/Integer;>;"
    .restart local v6    # "tagName":Ljava/lang/String;
    :catchall_0
    move-exception v4

    :try_start_7
    monitor-exit v3
    :try_end_7
    .catchall {:try_start_7 .. :try_end_7} :catchall_0

    .end local v0    # "fis":Ljava/io/FileInputStream;
    .end local p0    # "this":Lcom/miui/server/MiuiCompatModePackages;
    :try_start_8
    throw v4
    :try_end_8
    .catch Ljava/lang/Exception; {:try_start_8 .. :try_end_8} :catch_3
    .catchall {:try_start_8 .. :try_end_8} :catchall_1

    .line 306
    .end local v1    # "parser":Lorg/xmlpull/v1/XmlPullParser;
    .end local v2    # "eventType":I
    .end local v5    # "pkgs":Ljava/util/HashMap;, "Ljava/util/HashMap<Ljava/lang/String;Ljava/lang/Integer;>;"
    .end local v6    # "tagName":Ljava/lang/String;
    .restart local v0    # "fis":Ljava/io/FileInputStream;
    .restart local p0    # "this":Lcom/miui/server/MiuiCompatModePackages;
    :catchall_1
    move-exception v1

    goto :goto_5

    .line 303
    :catch_3
    move-exception v1

    .line 304
    .local v1, "e":Ljava/lang/Exception;
    :try_start_9
    const-string v2, "MiuiCompatModePackages"

    const-string v3, "Error reading compat-packages"

    invoke-static {v2, v3, v1}, Landroid/util/Slog;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I
    :try_end_9
    .catchall {:try_start_9 .. :try_end_9} :catchall_1

    .line 306
    nop

    .end local v1    # "e":Ljava/lang/Exception;
    if-eqz v0, :cond_7

    .line 308
    :try_start_a
    invoke-virtual {v0}, Ljava/io/FileInputStream;->close()V
    :try_end_a
    .catch Ljava/io/IOException; {:try_start_a .. :try_end_a} :catch_2

    goto :goto_3

    .line 313
    :cond_7
    :goto_4
    return-void

    .line 306
    :goto_5
    if-eqz v0, :cond_8

    .line 308
    :try_start_b
    invoke-virtual {v0}, Ljava/io/FileInputStream;->close()V
    :try_end_b
    .catch Ljava/io/IOException; {:try_start_b .. :try_end_b} :catch_4

    .line 310
    goto :goto_6

    .line 309
    :catch_4
    move-exception v2

    .line 312
    :cond_8
    :goto_6
    throw v1
.end method

.method private readSuggestApps()V
    .locals 2

    .line 433
    iget-object v0, p0, Lcom/miui/server/MiuiCompatModePackages;->mContext:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x1103009c

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getStringArray(I)[Ljava/lang/String;

    move-result-object v0

    .line 434
    .local v0, "arr":[Ljava/lang/String;
    iget-object v1, p0, Lcom/miui/server/MiuiCompatModePackages;->mSuggestList:Ljava/util/HashSet;

    invoke-static {v1, v0}, Ljava/util/Collections;->addAll(Ljava/util/Collection;[Ljava/lang/Object;)Z

    .line 435
    return-void
.end method

.method private removePackage(Ljava/lang/String;)V
    .locals 3
    .param p1, "packageName"    # Ljava/lang/String;

    .line 670
    const/4 v0, 0x0

    .line 671
    .local v0, "realRemove":Z
    iget-object v1, p0, Lcom/miui/server/MiuiCompatModePackages;->mLock:Ljava/lang/Object;

    monitor-enter v1

    .line 672
    :try_start_0
    iget-object v2, p0, Lcom/miui/server/MiuiCompatModePackages;->mDefaultType:Ljava/util/HashMap;

    invoke-virtual {v2, p1}, Ljava/util/HashMap;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    .line 673
    iget-object v2, p0, Lcom/miui/server/MiuiCompatModePackages;->mPackages:Ljava/util/HashMap;

    invoke-virtual {v2, p1}, Ljava/util/HashMap;->containsKey(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_0

    .line 674
    iget-object v2, p0, Lcom/miui/server/MiuiCompatModePackages;->mPackages:Ljava/util/HashMap;

    invoke-virtual {v2, p1}, Ljava/util/HashMap;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    .line 675
    const/4 v0, 0x1

    .line 677
    :cond_0
    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 678
    if-eqz v0, :cond_1

    .line 679
    invoke-direct {p0}, Lcom/miui/server/MiuiCompatModePackages;->scheduleWrite()V

    .line 681
    :cond_1
    return-void

    .line 677
    :catchall_0
    move-exception v2

    :try_start_1
    monitor-exit v1
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    throw v2
.end method

.method private removeSpecialModePackage(Ljava/lang/String;)V
    .locals 3
    .param p1, "packageName"    # Ljava/lang/String;

    .line 684
    const/4 v0, 0x0

    .line 685
    .local v0, "realRemove":Z
    iget-object v1, p0, Lcom/miui/server/MiuiCompatModePackages;->mLock:Ljava/lang/Object;

    monitor-enter v1

    .line 686
    :try_start_0
    iget-object v2, p0, Lcom/miui/server/MiuiCompatModePackages;->mNotchConfig:Ljava/util/HashMap;

    invoke-virtual {v2, p1}, Ljava/util/HashMap;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    .line 687
    iget-object v2, p0, Lcom/miui/server/MiuiCompatModePackages;->mNotchSpecialModePackages:Ljava/util/HashMap;

    invoke-virtual {v2, p1}, Ljava/util/HashMap;->containsKey(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_0

    .line 688
    iget-object v2, p0, Lcom/miui/server/MiuiCompatModePackages;->mNotchSpecialModePackages:Ljava/util/HashMap;

    invoke-virtual {v2, p1}, Ljava/util/HashMap;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    .line 689
    const/4 v0, 0x1

    .line 691
    :cond_0
    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 692
    if-eqz v0, :cond_1

    .line 693
    invoke-direct {p0}, Lcom/miui/server/MiuiCompatModePackages;->scheduleWriteSpecialMode()V

    .line 695
    :cond_1
    return-void

    .line 691
    :catchall_0
    move-exception v2

    :try_start_1
    monitor-exit v1
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    throw v2
.end method

.method private resolveDefaultAspectType(Ljava/lang/String;)I
    .locals 6
    .param p1, "packageName"    # Ljava/lang/String;

    .line 845
    const-string v0, "jp.netstar.familysmile"

    invoke-virtual {v0, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    const/4 v1, 0x1

    if-nez v0, :cond_6

    const-string v0, "jp.softbank.mb.parentalcontrols"

    invoke-virtual {v0, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    goto :goto_1

    .line 849
    :cond_0
    iget-object v0, p0, Lcom/miui/server/MiuiCompatModePackages;->mRestrictList:Ljava/util/HashSet;

    invoke-virtual {v0, p1}, Ljava/util/HashSet;->contains(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 850
    const/4 v0, 0x4

    return v0

    .line 853
    :cond_1
    const/4 v0, 0x0

    .line 855
    .local v0, "ai":Landroid/content/pm/ApplicationInfo;
    const/4 v2, 0x0

    :try_start_0
    invoke-static {}, Landroid/app/AppGlobals;->getPackageManager()Landroid/content/pm/IPackageManager;

    move-result-object v3

    const-wide/16 v4, 0x80

    invoke-interface {v3, p1, v4, v5, v2}, Landroid/content/pm/IPackageManager;->getApplicationInfo(Ljava/lang/String;JI)Landroid/content/pm/ApplicationInfo;

    move-result-object v3
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    move-object v0, v3

    .line 857
    goto :goto_0

    .line 856
    :catch_0
    move-exception v3

    .line 858
    :goto_0
    if-nez v0, :cond_2

    .line 859
    return v2

    .line 861
    :cond_2
    iget-object v2, v0, Landroid/content/pm/ApplicationInfo;->metaData:Landroid/os/Bundle;

    .line 862
    .local v2, "metadata":Landroid/os/Bundle;
    const/4 v3, 0x0

    .line 863
    .local v3, "aspect":F
    if-eqz v2, :cond_3

    .line 864
    const-string v4, "android.max_aspect"

    invoke-virtual {v2, v4}, Landroid/os/Bundle;->getFloat(Ljava/lang/String;)F

    move-result v3

    .line 866
    :cond_3
    invoke-direct {p0}, Lcom/miui/server/MiuiCompatModePackages;->getDeviceAspect()F

    move-result v4

    cmpl-float v4, v3, v4

    if-ltz v4, :cond_4

    .line 867
    return v1

    .line 870
    :cond_4
    iget-object v1, p0, Lcom/miui/server/MiuiCompatModePackages;->mSuggestList:Ljava/util/HashSet;

    invoke-virtual {v1, p1}, Ljava/util/HashSet;->contains(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_5

    .line 871
    const/4 v1, 0x3

    return v1

    .line 873
    :cond_5
    const/4 v1, 0x5

    return v1

    .line 846
    .end local v0    # "ai":Landroid/content/pm/ApplicationInfo;
    .end local v2    # "metadata":Landroid/os/Bundle;
    .end local v3    # "aspect":F
    :cond_6
    :goto_1
    return v1
.end method

.method private resolveNotchConfig(Ljava/lang/String;)I
    .locals 5
    .param p1, "packageName"    # Ljava/lang/String;

    .line 749
    iget-object v0, p0, Lcom/miui/server/MiuiCompatModePackages;->mSupportNotchList:Ljava/util/HashSet;

    invoke-virtual {v0, p1}, Ljava/util/HashSet;->contains(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 750
    const/16 v0, 0x700

    return v0

    .line 754
    :cond_0
    const/4 v0, 0x0

    .line 756
    .local v0, "ai":Landroid/content/pm/ApplicationInfo;
    const/4 v1, 0x0

    :try_start_0
    invoke-static {}, Landroid/app/AppGlobals;->getPackageManager()Landroid/content/pm/IPackageManager;

    move-result-object v2

    const-wide/16 v3, 0x80

    invoke-interface {v2, p1, v3, v4, v1}, Landroid/content/pm/IPackageManager;->getApplicationInfo(Ljava/lang/String;JI)Landroid/content/pm/ApplicationInfo;

    move-result-object v2
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    move-object v0, v2

    .line 759
    goto :goto_0

    .line 758
    :catch_0
    move-exception v2

    .line 760
    :goto_0
    if-nez v0, :cond_1

    .line 761
    return v1

    .line 763
    :cond_1
    iget-object v1, v0, Landroid/content/pm/ApplicationInfo;->metaData:Landroid/os/Bundle;

    .line 764
    .local v1, "metadata":Landroid/os/Bundle;
    const/4 v2, 0x0

    .line 765
    .local v2, "config":I
    if-eqz v1, :cond_3

    .line 766
    const-string v3, "notch.config"

    invoke-virtual {v1, v3}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    .line 767
    .local v3, "notch":Ljava/lang/String;
    invoke-static {v3}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v4

    if-nez v4, :cond_3

    .line 768
    or-int/lit16 v2, v2, 0x100

    .line 769
    const-string v4, "portrait"

    invoke-virtual {v3, v4}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v4

    if-eqz v4, :cond_2

    .line 770
    or-int/lit16 v2, v2, 0x200

    .line 772
    :cond_2
    const-string v4, "landscape"

    invoke-virtual {v3, v4}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v4

    if-eqz v4, :cond_3

    .line 773
    or-int/lit16 v2, v2, 0x400

    .line 777
    .end local v3    # "notch":Ljava/lang/String;
    :cond_3
    return v2
.end method

.method private scheduleWrite()V
    .locals 4

    .line 473
    iget-object v0, p0, Lcom/miui/server/MiuiCompatModePackages;->mHandler:Lcom/miui/server/MiuiCompatModePackages$CompatHandler;

    const/16 v1, 0x64

    invoke-virtual {v0, v1}, Lcom/miui/server/MiuiCompatModePackages$CompatHandler;->removeMessages(I)V

    .line 474
    iget-object v0, p0, Lcom/miui/server/MiuiCompatModePackages;->mHandler:Lcom/miui/server/MiuiCompatModePackages$CompatHandler;

    invoke-virtual {v0, v1}, Lcom/miui/server/MiuiCompatModePackages$CompatHandler;->obtainMessage(I)Landroid/os/Message;

    move-result-object v0

    .line 475
    .local v0, "msg":Landroid/os/Message;
    iget-object v1, p0, Lcom/miui/server/MiuiCompatModePackages;->mHandler:Lcom/miui/server/MiuiCompatModePackages$CompatHandler;

    const-wide/16 v2, 0x2710

    invoke-virtual {v1, v0, v2, v3}, Lcom/miui/server/MiuiCompatModePackages$CompatHandler;->sendMessageDelayed(Landroid/os/Message;J)Z

    .line 476
    return-void
.end method

.method private scheduleWriteCutoutMode()V
    .locals 4

    .line 485
    iget-object v0, p0, Lcom/miui/server/MiuiCompatModePackages;->mHandler:Lcom/miui/server/MiuiCompatModePackages$CompatHandler;

    const/16 v1, 0x6b

    invoke-virtual {v0, v1}, Lcom/miui/server/MiuiCompatModePackages$CompatHandler;->removeMessages(I)V

    .line 486
    iget-object v0, p0, Lcom/miui/server/MiuiCompatModePackages;->mHandler:Lcom/miui/server/MiuiCompatModePackages$CompatHandler;

    invoke-virtual {v0, v1}, Lcom/miui/server/MiuiCompatModePackages$CompatHandler;->obtainMessage(I)Landroid/os/Message;

    move-result-object v0

    .line 487
    .local v0, "msg":Landroid/os/Message;
    iget-object v1, p0, Lcom/miui/server/MiuiCompatModePackages;->mHandler:Lcom/miui/server/MiuiCompatModePackages$CompatHandler;

    const-wide/16 v2, 0x2710

    invoke-virtual {v1, v0, v2, v3}, Lcom/miui/server/MiuiCompatModePackages$CompatHandler;->sendMessageDelayed(Landroid/os/Message;J)Z

    .line 488
    return-void
.end method

.method private scheduleWriteSpecialMode()V
    .locals 4

    .line 479
    iget-object v0, p0, Lcom/miui/server/MiuiCompatModePackages;->mHandler:Lcom/miui/server/MiuiCompatModePackages$CompatHandler;

    const/16 v1, 0x6a

    invoke-virtual {v0, v1}, Lcom/miui/server/MiuiCompatModePackages$CompatHandler;->removeMessages(I)V

    .line 480
    iget-object v0, p0, Lcom/miui/server/MiuiCompatModePackages;->mHandler:Lcom/miui/server/MiuiCompatModePackages$CompatHandler;

    invoke-virtual {v0, v1}, Lcom/miui/server/MiuiCompatModePackages$CompatHandler;->obtainMessage(I)Landroid/os/Message;

    move-result-object v0

    .line 481
    .local v0, "msg":Landroid/os/Message;
    iget-object v1, p0, Lcom/miui/server/MiuiCompatModePackages;->mHandler:Lcom/miui/server/MiuiCompatModePackages$CompatHandler;

    const-wide/16 v2, 0x2710

    invoke-virtual {v1, v0, v2, v3}, Lcom/miui/server/MiuiCompatModePackages$CompatHandler;->sendMessageDelayed(Landroid/os/Message;J)Z

    .line 482
    return-void
.end method

.method private updateCloudData()V
    .locals 9

    .line 226
    const-string v0, "MiuiCompatModePackages"

    const-string/jumbo v1, "updateCloudData"

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 227
    iget-object v0, p0, Lcom/miui/server/MiuiCompatModePackages;->mLock:Ljava/lang/Object;

    monitor-enter v0

    .line 228
    :try_start_0
    iget-object v1, p0, Lcom/miui/server/MiuiCompatModePackages;->mCloudCutoutModePackages:Ljava/util/HashMap;

    invoke-virtual {v1}, Ljava/util/HashMap;->clear()V

    .line 229
    monitor-exit v0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 230
    iget-object v0, p0, Lcom/miui/server/MiuiCompatModePackages;->mContext:Landroid/content/Context;

    .line 231
    invoke-virtual {v0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    const-string v1, "cutout_mode"

    invoke-static {v0, v1}, Landroid/provider/MiuiSettings$SettingsCloudData;->getCloudDataList(Landroid/content/ContentResolver;Ljava/lang/String;)Ljava/util/List;

    move-result-object v0

    .line 232
    .local v0, "dataList":Ljava/util/List;, "Ljava/util/List<Landroid/provider/MiuiSettings$SettingsCloudData$CloudData;>;"
    if-eqz v0, :cond_4

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v1

    if-nez v1, :cond_0

    goto :goto_2

    .line 236
    :cond_0
    :try_start_1
    new-instance v1, Ljava/util/HashMap;

    invoke-direct {v1}, Ljava/util/HashMap;-><init>()V

    .line 237
    .local v1, "pkgs":Ljava/util/HashMap;, "Ljava/util/HashMap<Ljava/lang/String;Ljava/lang/Integer;>;"
    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_3

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Landroid/provider/MiuiSettings$SettingsCloudData$CloudData;

    .line 238
    .local v3, "data":Landroid/provider/MiuiSettings$SettingsCloudData$CloudData;
    invoke-virtual {v3}, Landroid/provider/MiuiSettings$SettingsCloudData$CloudData;->toString()Ljava/lang/String;

    move-result-object v4

    .line 239
    .local v4, "json":Ljava/lang/String;
    invoke-static {v4}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v5

    if-eqz v5, :cond_1

    .line 240
    goto :goto_0

    .line 243
    :cond_1
    new-instance v5, Lorg/json/JSONObject;

    invoke-direct {v5, v4}, Lorg/json/JSONObject;-><init>(Ljava/lang/String;)V

    .line 244
    .local v5, "jsonObject":Lorg/json/JSONObject;
    const-string v6, "pkg"

    invoke-virtual {v5, v6}, Lorg/json/JSONObject;->optString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v6

    .line 245
    .local v6, "pkg":Ljava/lang/String;
    const-string v7, "mode"

    invoke-virtual {v5, v7}, Lorg/json/JSONObject;->optInt(Ljava/lang/String;)I

    move-result v7

    .line 246
    .local v7, "mode":I
    invoke-static {v6}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v8

    if-nez v8, :cond_2

    .line 247
    invoke-static {v7}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v8

    invoke-virtual {v1, v6, v8}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 249
    .end local v3    # "data":Landroid/provider/MiuiSettings$SettingsCloudData$CloudData;
    .end local v4    # "json":Ljava/lang/String;
    .end local v5    # "jsonObject":Lorg/json/JSONObject;
    .end local v6    # "pkg":Ljava/lang/String;
    .end local v7    # "mode":I
    :cond_2
    goto :goto_0

    .line 250
    :cond_3
    iget-object v2, p0, Lcom/miui/server/MiuiCompatModePackages;->mLock:Ljava/lang/Object;

    monitor-enter v2
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_0

    .line 251
    :try_start_2
    iget-object v3, p0, Lcom/miui/server/MiuiCompatModePackages;->mCloudCutoutModePackages:Ljava/util/HashMap;

    invoke-virtual {v3, v1}, Ljava/util/HashMap;->putAll(Ljava/util/Map;)V

    .line 252
    monitor-exit v2

    .line 255
    .end local v1    # "pkgs":Ljava/util/HashMap;, "Ljava/util/HashMap<Ljava/lang/String;Ljava/lang/Integer;>;"
    goto :goto_1

    .line 252
    .restart local v1    # "pkgs":Ljava/util/HashMap;, "Ljava/util/HashMap<Ljava/lang/String;Ljava/lang/Integer;>;"
    :catchall_0
    move-exception v3

    monitor-exit v2
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    .end local v0    # "dataList":Ljava/util/List;, "Ljava/util/List<Landroid/provider/MiuiSettings$SettingsCloudData$CloudData;>;"
    .end local p0    # "this":Lcom/miui/server/MiuiCompatModePackages;
    :try_start_3
    throw v3
    :try_end_3
    .catch Ljava/lang/Exception; {:try_start_3 .. :try_end_3} :catch_0

    .line 253
    .end local v1    # "pkgs":Ljava/util/HashMap;, "Ljava/util/HashMap<Ljava/lang/String;Ljava/lang/Integer;>;"
    .restart local v0    # "dataList":Ljava/util/List;, "Ljava/util/List<Landroid/provider/MiuiSettings$SettingsCloudData$CloudData;>;"
    .restart local p0    # "this":Lcom/miui/server/MiuiCompatModePackages;
    :catch_0
    move-exception v1

    .line 254
    .local v1, "e":Ljava/lang/Exception;
    invoke-virtual {v1}, Ljava/lang/Exception;->printStackTrace()V

    .line 256
    .end local v1    # "e":Ljava/lang/Exception;
    :goto_1
    return-void

    .line 233
    :cond_4
    :goto_2
    return-void

    .line 229
    .end local v0    # "dataList":Ljava/util/List;, "Ljava/util/List<Landroid/provider/MiuiSettings$SettingsCloudData$CloudData;>;"
    :catchall_1
    move-exception v1

    :try_start_4
    monitor-exit v0
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_1

    throw v1
.end method


# virtual methods
.method public getAspectRatio(Ljava/lang/String;)F
    .locals 1
    .param p1, "pkg"    # Ljava/lang/String;

    .line 717
    invoke-virtual {p0, p1}, Lcom/miui/server/MiuiCompatModePackages;->isRestrictAspect(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_0

    sget v0, Lmiui/util/CustomizeUtil;->RESTRICT_ASPECT_RATIO:F

    goto :goto_0

    :cond_0
    const/high16 v0, 0x40400000    # 3.0f

    :goto_0
    return v0
.end method

.method public getCutoutMode(Ljava/lang/String;)I
    .locals 4
    .param p1, "pkg"    # Ljava/lang/String;

    .line 809
    iget-object v0, p0, Lcom/miui/server/MiuiCompatModePackages;->mLock:Ljava/lang/Object;

    monitor-enter v0

    .line 810
    :try_start_0
    iget-object v1, p0, Lcom/miui/server/MiuiCompatModePackages;->mUserCutoutModePackages:Ljava/util/HashMap;

    invoke-virtual {v1, p1}, Ljava/util/HashMap;->containsKey(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 811
    iget-object v1, p0, Lcom/miui/server/MiuiCompatModePackages;->mUserCutoutModePackages:Ljava/util/HashMap;

    invoke-virtual {v1, p1}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/Integer;

    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v1

    monitor-exit v0

    return v1

    .line 813
    :cond_0
    monitor-exit v0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 814
    iget-object v1, p0, Lcom/miui/server/MiuiCompatModePackages;->mLock:Ljava/lang/Object;

    monitor-enter v1

    .line 815
    :try_start_1
    iget-object v0, p0, Lcom/miui/server/MiuiCompatModePackages;->mCloudCutoutModePackages:Ljava/util/HashMap;

    invoke-virtual {v0, p1}, Ljava/util/HashMap;->containsKey(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 816
    iget-object v0, p0, Lcom/miui/server/MiuiCompatModePackages;->mCloudCutoutModePackages:Ljava/util/HashMap;

    invoke-virtual {v0, p1}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    monitor-exit v1

    return v0

    .line 818
    :cond_1
    monitor-exit v1
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 819
    invoke-direct {p0, p1}, Lcom/miui/server/MiuiCompatModePackages;->getDefaultNotchConfig(Ljava/lang/String;)I

    move-result v0

    .line 820
    .local v0, "flag":I
    const/16 v1, 0x700

    .line 823
    .local v1, "notchFlag":I
    const/4 v2, 0x0

    .line 824
    .local v2, "mode":I
    and-int v3, v0, v1

    if-ne v3, v1, :cond_2

    .line 825
    const/4 v2, 0x1

    .line 827
    :cond_2
    return v2

    .line 818
    .end local v0    # "flag":I
    .end local v1    # "notchFlag":I
    .end local v2    # "mode":I
    :catchall_0
    move-exception v0

    :try_start_2
    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    throw v0

    .line 813
    :catchall_1
    move-exception v1

    :try_start_3
    monitor-exit v0
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    throw v1
.end method

.method public getDefaultAspectType(Ljava/lang/String;)I
    .locals 4
    .param p1, "packageName"    # Ljava/lang/String;

    .line 831
    iget-object v0, p0, Lcom/miui/server/MiuiCompatModePackages;->mLock:Ljava/lang/Object;

    monitor-enter v0

    .line 832
    :try_start_0
    iget-object v1, p0, Lcom/miui/server/MiuiCompatModePackages;->mDefaultType:Ljava/util/HashMap;

    invoke-virtual {v1, p1}, Ljava/util/HashMap;->containsKey(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 833
    iget-object v1, p0, Lcom/miui/server/MiuiCompatModePackages;->mDefaultType:Ljava/util/HashMap;

    invoke-virtual {v1, p1}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/Integer;

    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v1

    monitor-exit v0

    return v1

    .line 835
    :cond_0
    monitor-exit v0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 837
    invoke-direct {p0, p1}, Lcom/miui/server/MiuiCompatModePackages;->resolveDefaultAspectType(Ljava/lang/String;)I

    move-result v1

    .line 838
    .local v1, "type":I
    iget-object v2, p0, Lcom/miui/server/MiuiCompatModePackages;->mLock:Ljava/lang/Object;

    monitor-enter v2

    .line 839
    :try_start_1
    iget-object v0, p0, Lcom/miui/server/MiuiCompatModePackages;->mDefaultType:Ljava/util/HashMap;

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    invoke-virtual {v0, p1, v3}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 840
    monitor-exit v2

    .line 841
    return v1

    .line 840
    :catchall_0
    move-exception v0

    monitor-exit v2
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    throw v0

    .line 835
    .end local v1    # "type":I
    :catchall_1
    move-exception v1

    :try_start_2
    monitor-exit v0
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    throw v1
.end method

.method public getNotchConfig(Ljava/lang/String;)I
    .locals 4
    .param p1, "packageName"    # Ljava/lang/String;

    .line 721
    const/4 v0, 0x0

    .line 722
    .local v0, "config":I
    iget-object v1, p0, Lcom/miui/server/MiuiCompatModePackages;->mLock:Ljava/lang/Object;

    monitor-enter v1

    .line 723
    :try_start_0
    iget-object v2, p0, Lcom/miui/server/MiuiCompatModePackages;->mNotchSpecialModePackages:Ljava/util/HashMap;

    invoke-virtual {v2, p1}, Ljava/util/HashMap;->containsKey(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_1

    .line 724
    iget-object v2, p0, Lcom/miui/server/MiuiCompatModePackages;->mNotchSpecialModePackages:Ljava/util/HashMap;

    invoke-virtual {v2, p1}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/lang/Integer;

    .line 725
    .local v2, "mode":Ljava/lang/Integer;
    if-eqz v2, :cond_1

    .line 726
    invoke-virtual {v2}, Ljava/lang/Integer;->intValue()I

    move-result v3

    if-eqz v3, :cond_0

    const/16 v3, 0x80

    goto :goto_0

    :cond_0
    const/4 v3, 0x0

    :goto_0
    move v0, v3

    .line 729
    .end local v2    # "mode":Ljava/lang/Integer;
    :cond_1
    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 730
    invoke-direct {p0, p1}, Lcom/miui/server/MiuiCompatModePackages;->getDefaultNotchConfig(Ljava/lang/String;)I

    move-result v1

    or-int/2addr v0, v1

    .line 731
    return v0

    .line 729
    :catchall_0
    move-exception v2

    :try_start_1
    monitor-exit v1
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    throw v2
.end method

.method public isRestrictAspect(Ljava/lang/String;)Z
    .locals 2
    .param p1, "packageName"    # Ljava/lang/String;

    .line 877
    invoke-direct {p0, p1}, Lcom/miui/server/MiuiCompatModePackages;->getPackageMode(Ljava/lang/String;)F

    move-result v0

    const/4 v1, 0x0

    cmpl-float v0, v0, v1

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    :goto_0
    return v0
.end method

.method saveCompatModes()V
    .locals 12

    .line 491
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    .line 492
    .local v0, "pkgs":Ljava/util/HashMap;, "Ljava/util/HashMap<Ljava/lang/String;Ljava/lang/Integer;>;"
    iget-object v1, p0, Lcom/miui/server/MiuiCompatModePackages;->mLock:Ljava/lang/Object;

    monitor-enter v1

    .line 493
    :try_start_0
    iget-object v2, p0, Lcom/miui/server/MiuiCompatModePackages;->mPackages:Ljava/util/HashMap;

    invoke-virtual {v0, v2}, Ljava/util/HashMap;->putAll(Ljava/util/Map;)V

    .line 494
    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 496
    const/4 v1, 0x0

    .line 499
    .local v1, "fos":Ljava/io/FileOutputStream;
    :try_start_1
    iget-object v2, p0, Lcom/miui/server/MiuiCompatModePackages;->mFile:Landroid/util/AtomicFile;

    invoke-virtual {v2}, Landroid/util/AtomicFile;->startWrite()Ljava/io/FileOutputStream;

    move-result-object v2

    move-object v1, v2

    .line 500
    new-instance v2, Lcom/android/internal/util/FastXmlSerializer;

    invoke-direct {v2}, Lcom/android/internal/util/FastXmlSerializer;-><init>()V

    .line 501
    .local v2, "out":Lorg/xmlpull/v1/XmlSerializer;
    sget-object v3, Ljava/nio/charset/StandardCharsets;->UTF_8:Ljava/nio/charset/Charset;

    invoke-virtual {v3}, Ljava/nio/charset/Charset;->name()Ljava/lang/String;

    move-result-object v3

    invoke-interface {v2, v1, v3}, Lorg/xmlpull/v1/XmlSerializer;->setOutput(Ljava/io/OutputStream;Ljava/lang/String;)V

    .line 502
    const/4 v3, 0x1

    invoke-static {v3}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v4

    const/4 v5, 0x0

    invoke-interface {v2, v5, v4}, Lorg/xmlpull/v1/XmlSerializer;->startDocument(Ljava/lang/String;Ljava/lang/Boolean;)V

    .line 503
    const-string v4, "http://xmlpull.org/v1/doc/features.html#indent-output"

    invoke-interface {v2, v4, v3}, Lorg/xmlpull/v1/XmlSerializer;->setFeature(Ljava/lang/String;Z)V

    .line 504
    const-string v4, "compat-packages"

    invoke-interface {v2, v5, v4}, Lorg/xmlpull/v1/XmlSerializer;->startTag(Ljava/lang/String;Ljava/lang/String;)Lorg/xmlpull/v1/XmlSerializer;

    .line 507
    const-string v4, "config"

    invoke-interface {v2, v5, v4}, Lorg/xmlpull/v1/XmlSerializer;->startTag(Ljava/lang/String;Ljava/lang/String;)Lorg/xmlpull/v1/XmlSerializer;

    .line 508
    const-string v4, "notifySuggestApps"

    iget-boolean v6, p0, Lcom/miui/server/MiuiCompatModePackages;->mNotifySuggestApps:Z

    invoke-static {v6}, Ljava/lang/String;->valueOf(Z)Ljava/lang/String;

    move-result-object v6

    invoke-interface {v2, v5, v4, v6}, Lorg/xmlpull/v1/XmlSerializer;->attribute(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Lorg/xmlpull/v1/XmlSerializer;

    .line 509
    const-string v4, "config"

    invoke-interface {v2, v5, v4}, Lorg/xmlpull/v1/XmlSerializer;->endTag(Ljava/lang/String;Ljava/lang/String;)Lorg/xmlpull/v1/XmlSerializer;

    .line 511
    invoke-virtual {v0}, Ljava/util/HashMap;->entrySet()Ljava/util/Set;

    move-result-object v4

    invoke-interface {v4}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v4

    .line 512
    .local v4, "it":Ljava/util/Iterator;, "Ljava/util/Iterator<Ljava/util/Map$Entry<Ljava/lang/String;Ljava/lang/Integer;>;>;"
    :goto_0
    invoke-interface {v4}, Ljava/util/Iterator;->hasNext()Z

    move-result v6

    if-eqz v6, :cond_3

    .line 513
    invoke-interface {v4}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v6

    check-cast v6, Ljava/util/Map$Entry;

    .line 514
    .local v6, "entry":Ljava/util/Map$Entry;, "Ljava/util/Map$Entry<Ljava/lang/String;Ljava/lang/Integer;>;"
    invoke-interface {v6}, Ljava/util/Map$Entry;->getKey()Ljava/lang/Object;

    move-result-object v7

    check-cast v7, Ljava/lang/String;

    .line 515
    .local v7, "pkg":Ljava/lang/String;
    invoke-interface {v6}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v8

    check-cast v8, Ljava/lang/Integer;

    invoke-virtual {v8}, Ljava/lang/Integer;->intValue()I

    move-result v8

    .line 516
    .local v8, "mode":I
    if-lez v8, :cond_0

    move v9, v3

    goto :goto_1

    :cond_0
    const/4 v9, 0x0

    .line 517
    .local v9, "restrict":Z
    :goto_1
    invoke-direct {p0, v7}, Lcom/miui/server/MiuiCompatModePackages;->isDefaultRestrict(Ljava/lang/String;)Z

    move-result v10

    if-ne v9, v10, :cond_1

    .line 518
    goto :goto_0

    .line 522
    :cond_1
    invoke-virtual {p0, v7}, Lcom/miui/server/MiuiCompatModePackages;->getDefaultAspectType(Ljava/lang/String;)I

    move-result v10

    if-ne v10, v3, :cond_2

    .line 523
    goto :goto_0

    .line 526
    :cond_2
    const-string v10, "pkg"

    invoke-interface {v2, v5, v10}, Lorg/xmlpull/v1/XmlSerializer;->startTag(Ljava/lang/String;Ljava/lang/String;)Lorg/xmlpull/v1/XmlSerializer;

    .line 527
    const-string v10, "name"

    invoke-interface {v2, v5, v10, v7}, Lorg/xmlpull/v1/XmlSerializer;->attribute(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Lorg/xmlpull/v1/XmlSerializer;

    .line 528
    const-string v10, "mode"

    invoke-static {v8}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v11

    invoke-interface {v2, v5, v10, v11}, Lorg/xmlpull/v1/XmlSerializer;->attribute(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Lorg/xmlpull/v1/XmlSerializer;

    .line 529
    const-string v10, "pkg"

    invoke-interface {v2, v5, v10}, Lorg/xmlpull/v1/XmlSerializer;->endTag(Ljava/lang/String;Ljava/lang/String;)Lorg/xmlpull/v1/XmlSerializer;

    .line 530
    nop

    .end local v6    # "entry":Ljava/util/Map$Entry;, "Ljava/util/Map$Entry<Ljava/lang/String;Ljava/lang/Integer;>;"
    .end local v7    # "pkg":Ljava/lang/String;
    .end local v8    # "mode":I
    .end local v9    # "restrict":Z
    goto :goto_0

    .line 532
    :cond_3
    const-string v3, "compat-packages"

    invoke-interface {v2, v5, v3}, Lorg/xmlpull/v1/XmlSerializer;->endTag(Ljava/lang/String;Ljava/lang/String;)Lorg/xmlpull/v1/XmlSerializer;

    .line 533
    invoke-interface {v2}, Lorg/xmlpull/v1/XmlSerializer;->endDocument()V

    .line 535
    iget-object v3, p0, Lcom/miui/server/MiuiCompatModePackages;->mFile:Landroid/util/AtomicFile;

    invoke-virtual {v3, v1}, Landroid/util/AtomicFile;->finishWrite(Ljava/io/FileOutputStream;)V
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 542
    .end local v2    # "out":Lorg/xmlpull/v1/XmlSerializer;
    .end local v4    # "it":Ljava/util/Iterator;, "Ljava/util/Iterator<Ljava/util/Map$Entry<Ljava/lang/String;Ljava/lang/Integer;>;>;"
    if-eqz v1, :cond_5

    .line 544
    :try_start_2
    invoke-virtual {v1}, Ljava/io/FileOutputStream;->close()V
    :try_end_2
    .catch Ljava/io/IOException; {:try_start_2 .. :try_end_2} :catch_0

    .line 546
    :goto_2
    goto :goto_3

    .line 545
    :catch_0
    move-exception v2

    goto :goto_2

    .line 542
    :catchall_0
    move-exception v2

    goto :goto_4

    .line 536
    :catch_1
    move-exception v2

    .line 537
    .local v2, "e1":Ljava/lang/Exception;
    :try_start_3
    const-string v3, "MiuiCompatModePackages"

    const-string v4, "Error writing compat packages"

    invoke-static {v3, v4, v2}, Landroid/util/Slog;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    .line 538
    if-eqz v1, :cond_4

    .line 539
    iget-object v3, p0, Lcom/miui/server/MiuiCompatModePackages;->mFile:Landroid/util/AtomicFile;

    invoke-virtual {v3, v1}, Landroid/util/AtomicFile;->failWrite(Ljava/io/FileOutputStream;)V
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    .line 542
    .end local v2    # "e1":Ljava/lang/Exception;
    :cond_4
    if-eqz v1, :cond_5

    .line 544
    :try_start_4
    invoke-virtual {v1}, Ljava/io/FileOutputStream;->close()V
    :try_end_4
    .catch Ljava/io/IOException; {:try_start_4 .. :try_end_4} :catch_0

    goto :goto_2

    .line 549
    :cond_5
    :goto_3
    return-void

    .line 542
    :goto_4
    if-eqz v1, :cond_6

    .line 544
    :try_start_5
    invoke-virtual {v1}, Ljava/io/FileOutputStream;->close()V
    :try_end_5
    .catch Ljava/io/IOException; {:try_start_5 .. :try_end_5} :catch_2

    .line 546
    goto :goto_5

    .line 545
    :catch_2
    move-exception v3

    .line 548
    :cond_6
    :goto_5
    throw v2

    .line 494
    .end local v1    # "fos":Ljava/io/FileOutputStream;
    :catchall_1
    move-exception v2

    :try_start_6
    monitor-exit v1
    :try_end_6
    .catchall {:try_start_6 .. :try_end_6} :catchall_1

    throw v2
.end method

.method saveCutoutModeFile()V
    .locals 10

    .line 603
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    .line 604
    .local v0, "pkgs":Ljava/util/HashMap;, "Ljava/util/HashMap<Ljava/lang/String;Ljava/lang/Integer;>;"
    iget-object v1, p0, Lcom/miui/server/MiuiCompatModePackages;->mLock:Ljava/lang/Object;

    monitor-enter v1

    .line 605
    :try_start_0
    iget-object v2, p0, Lcom/miui/server/MiuiCompatModePackages;->mUserCutoutModePackages:Ljava/util/HashMap;

    invoke-virtual {v0, v2}, Ljava/util/HashMap;->putAll(Ljava/util/Map;)V

    .line 606
    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 608
    const/4 v1, 0x0

    .line 611
    .local v1, "fos":Ljava/io/FileOutputStream;
    :try_start_1
    iget-object v2, p0, Lcom/miui/server/MiuiCompatModePackages;->mCutoutModeFile:Landroid/util/AtomicFile;

    invoke-virtual {v2}, Landroid/util/AtomicFile;->startWrite()Ljava/io/FileOutputStream;

    move-result-object v2

    move-object v1, v2

    .line 612
    new-instance v2, Lcom/android/internal/util/FastXmlSerializer;

    invoke-direct {v2}, Lcom/android/internal/util/FastXmlSerializer;-><init>()V

    .line 613
    .local v2, "out":Lorg/xmlpull/v1/XmlSerializer;
    sget-object v3, Ljava/nio/charset/StandardCharsets;->UTF_8:Ljava/nio/charset/Charset;

    invoke-virtual {v3}, Ljava/nio/charset/Charset;->name()Ljava/lang/String;

    move-result-object v3

    invoke-interface {v2, v1, v3}, Lorg/xmlpull/v1/XmlSerializer;->setOutput(Ljava/io/OutputStream;Ljava/lang/String;)V

    .line 614
    const/4 v3, 0x1

    invoke-static {v3}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v4

    const/4 v5, 0x0

    invoke-interface {v2, v5, v4}, Lorg/xmlpull/v1/XmlSerializer;->startDocument(Ljava/lang/String;Ljava/lang/Boolean;)V

    .line 615
    const-string v4, "http://xmlpull.org/v1/doc/features.html#indent-output"

    invoke-interface {v2, v4, v3}, Lorg/xmlpull/v1/XmlSerializer;->setFeature(Ljava/lang/String;Z)V

    .line 616
    const-string v3, "cutout-mode"

    invoke-interface {v2, v5, v3}, Lorg/xmlpull/v1/XmlSerializer;->startTag(Ljava/lang/String;Ljava/lang/String;)Lorg/xmlpull/v1/XmlSerializer;

    .line 618
    invoke-virtual {v0}, Ljava/util/HashMap;->entrySet()Ljava/util/Set;

    move-result-object v3

    invoke-interface {v3}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v3

    .line 619
    .local v3, "it":Ljava/util/Iterator;, "Ljava/util/Iterator<Ljava/util/Map$Entry<Ljava/lang/String;Ljava/lang/Integer;>;>;"
    :goto_0
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v4

    if-eqz v4, :cond_0

    .line 620
    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Ljava/util/Map$Entry;

    .line 621
    .local v4, "entry":Ljava/util/Map$Entry;, "Ljava/util/Map$Entry<Ljava/lang/String;Ljava/lang/Integer;>;"
    invoke-interface {v4}, Ljava/util/Map$Entry;->getKey()Ljava/lang/Object;

    move-result-object v6

    check-cast v6, Ljava/lang/String;

    .line 622
    .local v6, "pkg":Ljava/lang/String;
    invoke-interface {v4}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v7

    check-cast v7, Ljava/lang/Integer;

    invoke-virtual {v7}, Ljava/lang/Integer;->intValue()I

    move-result v7

    .line 623
    .local v7, "mode":I
    const-string v8, "pkg"

    invoke-interface {v2, v5, v8}, Lorg/xmlpull/v1/XmlSerializer;->startTag(Ljava/lang/String;Ljava/lang/String;)Lorg/xmlpull/v1/XmlSerializer;

    .line 624
    const-string v8, "name"

    invoke-interface {v2, v5, v8, v6}, Lorg/xmlpull/v1/XmlSerializer;->attribute(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Lorg/xmlpull/v1/XmlSerializer;

    .line 625
    const-string v8, "mode"

    invoke-static {v7}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v9

    invoke-interface {v2, v5, v8, v9}, Lorg/xmlpull/v1/XmlSerializer;->attribute(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Lorg/xmlpull/v1/XmlSerializer;

    .line 626
    const-string v8, "pkg"

    invoke-interface {v2, v5, v8}, Lorg/xmlpull/v1/XmlSerializer;->endTag(Ljava/lang/String;Ljava/lang/String;)Lorg/xmlpull/v1/XmlSerializer;

    .line 627
    nop

    .end local v4    # "entry":Ljava/util/Map$Entry;, "Ljava/util/Map$Entry<Ljava/lang/String;Ljava/lang/Integer;>;"
    .end local v6    # "pkg":Ljava/lang/String;
    .end local v7    # "mode":I
    goto :goto_0

    .line 629
    :cond_0
    const-string v4, "cutout-mode"

    invoke-interface {v2, v5, v4}, Lorg/xmlpull/v1/XmlSerializer;->endTag(Ljava/lang/String;Ljava/lang/String;)Lorg/xmlpull/v1/XmlSerializer;

    .line 630
    invoke-interface {v2}, Lorg/xmlpull/v1/XmlSerializer;->endDocument()V

    .line 632
    iget-object v4, p0, Lcom/miui/server/MiuiCompatModePackages;->mCutoutModeFile:Landroid/util/AtomicFile;

    invoke-virtual {v4, v1}, Landroid/util/AtomicFile;->finishWrite(Ljava/io/FileOutputStream;)V
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 639
    .end local v2    # "out":Lorg/xmlpull/v1/XmlSerializer;
    .end local v3    # "it":Ljava/util/Iterator;, "Ljava/util/Iterator<Ljava/util/Map$Entry<Ljava/lang/String;Ljava/lang/Integer;>;>;"
    if-eqz v1, :cond_2

    .line 641
    :try_start_2
    invoke-virtual {v1}, Ljava/io/FileOutputStream;->close()V
    :try_end_2
    .catch Ljava/io/IOException; {:try_start_2 .. :try_end_2} :catch_0

    .line 643
    :goto_1
    goto :goto_2

    .line 642
    :catch_0
    move-exception v2

    goto :goto_1

    .line 639
    :catchall_0
    move-exception v2

    goto :goto_3

    .line 633
    :catch_1
    move-exception v2

    .line 634
    .local v2, "e1":Ljava/lang/Exception;
    :try_start_3
    const-string v3, "MiuiCompatModePackages"

    const-string v4, "Error writing cutout packages"

    invoke-static {v3, v4, v2}, Landroid/util/Slog;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    .line 635
    if-eqz v1, :cond_1

    .line 636
    iget-object v3, p0, Lcom/miui/server/MiuiCompatModePackages;->mCutoutModeFile:Landroid/util/AtomicFile;

    invoke-virtual {v3, v1}, Landroid/util/AtomicFile;->failWrite(Ljava/io/FileOutputStream;)V
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    .line 639
    .end local v2    # "e1":Ljava/lang/Exception;
    :cond_1
    if-eqz v1, :cond_2

    .line 641
    :try_start_4
    invoke-virtual {v1}, Ljava/io/FileOutputStream;->close()V
    :try_end_4
    .catch Ljava/io/IOException; {:try_start_4 .. :try_end_4} :catch_0

    goto :goto_1

    .line 646
    :cond_2
    :goto_2
    return-void

    .line 639
    :goto_3
    if-eqz v1, :cond_3

    .line 641
    :try_start_5
    invoke-virtual {v1}, Ljava/io/FileOutputStream;->close()V
    :try_end_5
    .catch Ljava/io/IOException; {:try_start_5 .. :try_end_5} :catch_2

    .line 643
    goto :goto_4

    .line 642
    :catch_2
    move-exception v3

    .line 645
    :cond_3
    :goto_4
    throw v2

    .line 606
    .end local v1    # "fos":Ljava/io/FileOutputStream;
    :catchall_1
    move-exception v2

    :try_start_6
    monitor-exit v1
    :try_end_6
    .catchall {:try_start_6 .. :try_end_6} :catchall_1

    throw v2
.end method

.method saveSpecialModeFile()V
    .locals 12

    .line 552
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    .line 553
    .local v0, "pkgs":Ljava/util/HashMap;, "Ljava/util/HashMap<Ljava/lang/String;Ljava/lang/Integer;>;"
    iget-object v1, p0, Lcom/miui/server/MiuiCompatModePackages;->mLock:Ljava/lang/Object;

    monitor-enter v1

    .line 554
    :try_start_0
    iget-object v2, p0, Lcom/miui/server/MiuiCompatModePackages;->mNotchSpecialModePackages:Ljava/util/HashMap;

    invoke-virtual {v0, v2}, Ljava/util/HashMap;->putAll(Ljava/util/Map;)V

    .line 555
    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 557
    const/4 v1, 0x0

    .line 560
    .local v1, "fos":Ljava/io/FileOutputStream;
    :try_start_1
    iget-object v2, p0, Lcom/miui/server/MiuiCompatModePackages;->mSpecialModeFile:Landroid/util/AtomicFile;

    invoke-virtual {v2}, Landroid/util/AtomicFile;->startWrite()Ljava/io/FileOutputStream;

    move-result-object v2

    move-object v1, v2

    .line 561
    new-instance v2, Lcom/android/internal/util/FastXmlSerializer;

    invoke-direct {v2}, Lcom/android/internal/util/FastXmlSerializer;-><init>()V

    .line 562
    .local v2, "out":Lorg/xmlpull/v1/XmlSerializer;
    sget-object v3, Ljava/nio/charset/StandardCharsets;->UTF_8:Ljava/nio/charset/Charset;

    invoke-virtual {v3}, Ljava/nio/charset/Charset;->name()Ljava/lang/String;

    move-result-object v3

    invoke-interface {v2, v1, v3}, Lorg/xmlpull/v1/XmlSerializer;->setOutput(Ljava/io/OutputStream;Ljava/lang/String;)V

    .line 563
    const/4 v3, 0x1

    invoke-static {v3}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v4

    const/4 v5, 0x0

    invoke-interface {v2, v5, v4}, Lorg/xmlpull/v1/XmlSerializer;->startDocument(Ljava/lang/String;Ljava/lang/Boolean;)V

    .line 564
    const-string v4, "http://xmlpull.org/v1/doc/features.html#indent-output"

    invoke-interface {v2, v4, v3}, Lorg/xmlpull/v1/XmlSerializer;->setFeature(Ljava/lang/String;Z)V

    .line 565
    const-string/jumbo v4, "special-mode"

    invoke-interface {v2, v5, v4}, Lorg/xmlpull/v1/XmlSerializer;->startTag(Ljava/lang/String;Ljava/lang/String;)Lorg/xmlpull/v1/XmlSerializer;

    .line 567
    invoke-virtual {v0}, Ljava/util/HashMap;->entrySet()Ljava/util/Set;

    move-result-object v4

    invoke-interface {v4}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v4

    .line 568
    .local v4, "it":Ljava/util/Iterator;, "Ljava/util/Iterator<Ljava/util/Map$Entry<Ljava/lang/String;Ljava/lang/Integer;>;>;"
    :goto_0
    invoke-interface {v4}, Ljava/util/Iterator;->hasNext()Z

    move-result v6

    if-eqz v6, :cond_2

    .line 569
    invoke-interface {v4}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v6

    check-cast v6, Ljava/util/Map$Entry;

    .line 570
    .local v6, "entry":Ljava/util/Map$Entry;, "Ljava/util/Map$Entry<Ljava/lang/String;Ljava/lang/Integer;>;"
    invoke-interface {v6}, Ljava/util/Map$Entry;->getKey()Ljava/lang/Object;

    move-result-object v7

    check-cast v7, Ljava/lang/String;

    .line 571
    .local v7, "pkg":Ljava/lang/String;
    invoke-interface {v6}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v8

    check-cast v8, Ljava/lang/Integer;

    invoke-virtual {v8}, Ljava/lang/Integer;->intValue()I

    move-result v8

    .line 572
    .local v8, "mode":I
    if-lez v8, :cond_0

    move v9, v3

    goto :goto_1

    :cond_0
    const/4 v9, 0x0

    .line 573
    .local v9, "special":Z
    :goto_1
    if-nez v9, :cond_1

    .line 574
    goto :goto_0

    .line 577
    :cond_1
    const-string v10, "pkg"

    invoke-interface {v2, v5, v10}, Lorg/xmlpull/v1/XmlSerializer;->startTag(Ljava/lang/String;Ljava/lang/String;)Lorg/xmlpull/v1/XmlSerializer;

    .line 578
    const-string v10, "name"

    invoke-interface {v2, v5, v10, v7}, Lorg/xmlpull/v1/XmlSerializer;->attribute(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Lorg/xmlpull/v1/XmlSerializer;

    .line 579
    const-string v10, "mode"

    invoke-static {v8}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v11

    invoke-interface {v2, v5, v10, v11}, Lorg/xmlpull/v1/XmlSerializer;->attribute(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Lorg/xmlpull/v1/XmlSerializer;

    .line 580
    const-string v10, "pkg"

    invoke-interface {v2, v5, v10}, Lorg/xmlpull/v1/XmlSerializer;->endTag(Ljava/lang/String;Ljava/lang/String;)Lorg/xmlpull/v1/XmlSerializer;

    .line 581
    nop

    .end local v6    # "entry":Ljava/util/Map$Entry;, "Ljava/util/Map$Entry<Ljava/lang/String;Ljava/lang/Integer;>;"
    .end local v7    # "pkg":Ljava/lang/String;
    .end local v8    # "mode":I
    .end local v9    # "special":Z
    goto :goto_0

    .line 583
    :cond_2
    const-string/jumbo v3, "special-mode"

    invoke-interface {v2, v5, v3}, Lorg/xmlpull/v1/XmlSerializer;->endTag(Ljava/lang/String;Ljava/lang/String;)Lorg/xmlpull/v1/XmlSerializer;

    .line 584
    invoke-interface {v2}, Lorg/xmlpull/v1/XmlSerializer;->endDocument()V

    .line 586
    iget-object v3, p0, Lcom/miui/server/MiuiCompatModePackages;->mSpecialModeFile:Landroid/util/AtomicFile;

    invoke-virtual {v3, v1}, Landroid/util/AtomicFile;->finishWrite(Ljava/io/FileOutputStream;)V
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 593
    .end local v2    # "out":Lorg/xmlpull/v1/XmlSerializer;
    .end local v4    # "it":Ljava/util/Iterator;, "Ljava/util/Iterator<Ljava/util/Map$Entry<Ljava/lang/String;Ljava/lang/Integer;>;>;"
    if-eqz v1, :cond_4

    .line 595
    :try_start_2
    invoke-virtual {v1}, Ljava/io/FileOutputStream;->close()V
    :try_end_2
    .catch Ljava/io/IOException; {:try_start_2 .. :try_end_2} :catch_0

    .line 597
    :goto_2
    goto :goto_3

    .line 596
    :catch_0
    move-exception v2

    goto :goto_2

    .line 593
    :catchall_0
    move-exception v2

    goto :goto_4

    .line 587
    :catch_1
    move-exception v2

    .line 588
    .local v2, "e1":Ljava/lang/Exception;
    :try_start_3
    const-string v3, "MiuiCompatModePackages"

    const-string v4, "Error writing compat packages"

    invoke-static {v3, v4, v2}, Landroid/util/Slog;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    .line 589
    if-eqz v1, :cond_3

    .line 590
    iget-object v3, p0, Lcom/miui/server/MiuiCompatModePackages;->mSpecialModeFile:Landroid/util/AtomicFile;

    invoke-virtual {v3, v1}, Landroid/util/AtomicFile;->failWrite(Ljava/io/FileOutputStream;)V
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    .line 593
    .end local v2    # "e1":Ljava/lang/Exception;
    :cond_3
    if-eqz v1, :cond_4

    .line 595
    :try_start_4
    invoke-virtual {v1}, Ljava/io/FileOutputStream;->close()V
    :try_end_4
    .catch Ljava/io/IOException; {:try_start_4 .. :try_end_4} :catch_0

    goto :goto_2

    .line 600
    :cond_4
    :goto_3
    return-void

    .line 593
    :goto_4
    if-eqz v1, :cond_5

    .line 595
    :try_start_5
    invoke-virtual {v1}, Ljava/io/FileOutputStream;->close()V
    :try_end_5
    .catch Ljava/io/IOException; {:try_start_5 .. :try_end_5} :catch_2

    .line 597
    goto :goto_5

    .line 596
    :catch_2
    move-exception v3

    .line 599
    :cond_5
    :goto_5
    throw v2

    .line 555
    .end local v1    # "fos":Ljava/io/FileOutputStream;
    :catchall_1
    move-exception v2

    :try_start_6
    monitor-exit v1
    :try_end_6
    .catchall {:try_start_6 .. :try_end_6} :catchall_1

    throw v2
.end method

.method public setCutoutMode(Ljava/lang/String;I)V
    .locals 4
    .param p1, "pkg"    # Ljava/lang/String;
    .param p2, "mode"    # I

    .line 797
    iget-object v0, p0, Lcom/miui/server/MiuiCompatModePackages;->mLock:Ljava/lang/Object;

    monitor-enter v0

    .line 798
    :try_start_0
    iget-object v1, p0, Lcom/miui/server/MiuiCompatModePackages;->mUserCutoutModePackages:Ljava/util/HashMap;

    const/4 v2, 0x0

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v1, p1, v2}, Ljava/util/HashMap;->getOrDefault(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/Integer;

    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v1

    .line 799
    .local v1, "oldMode":I
    if-ne v1, p2, :cond_0

    .line 800
    monitor-exit v0

    return-void

    .line 802
    :cond_0
    iget-object v2, p0, Lcom/miui/server/MiuiCompatModePackages;->mUserCutoutModePackages:Ljava/util/HashMap;

    invoke-static {p2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    invoke-virtual {v2, p1, v3}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 803
    nop

    .end local v1    # "oldMode":I
    monitor-exit v0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 804
    invoke-direct {p0}, Lcom/miui/server/MiuiCompatModePackages;->scheduleWriteCutoutMode()V

    .line 805
    iget-object v0, p0, Lcom/miui/server/MiuiCompatModePackages;->mContext:Landroid/content/Context;

    const-string v1, "activity"

    invoke-virtual {v0, v1}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/app/ActivityManager;

    invoke-virtual {v0, p1}, Landroid/app/ActivityManager;->forceStopPackage(Ljava/lang/String;)V

    .line 806
    return-void

    .line 803
    :catchall_0
    move-exception v1

    :try_start_1
    monitor-exit v0
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    throw v1
.end method

.method public setNotchSpecialMode(Ljava/lang/String;Z)V
    .locals 4
    .param p1, "pkg"    # Ljava/lang/String;
    .param p2, "special"    # Z

    .line 781
    invoke-direct {p0, p1}, Lcom/miui/server/MiuiCompatModePackages;->isNotchSpecailMode(Ljava/lang/String;)Z

    move-result v0

    .line 783
    .local v0, "oldSpecail":Z
    if-eq p2, v0, :cond_1

    .line 784
    iget-object v1, p0, Lcom/miui/server/MiuiCompatModePackages;->mLock:Ljava/lang/Object;

    monitor-enter v1

    .line 785
    :try_start_0
    iget-object v2, p0, Lcom/miui/server/MiuiCompatModePackages;->mNotchSpecialModePackages:Ljava/util/HashMap;

    if-eqz p2, :cond_0

    const/4 v3, 0x1

    goto :goto_0

    :cond_0
    const/4 v3, 0x0

    :goto_0
    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    invoke-virtual {v2, p1, v3}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 786
    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 787
    invoke-direct {p0}, Lcom/miui/server/MiuiCompatModePackages;->scheduleWriteSpecialMode()V

    .line 788
    iget-object v1, p0, Lcom/miui/server/MiuiCompatModePackages;->mContext:Landroid/content/Context;

    const-string v2, "activity"

    invoke-virtual {v1, v2}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/app/ActivityManager;

    invoke-virtual {v1, p1}, Landroid/app/ActivityManager;->forceStopPackage(Ljava/lang/String;)V

    goto :goto_1

    .line 786
    :catchall_0
    move-exception v2

    :try_start_1
    monitor-exit v1
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    throw v2

    .line 790
    :cond_1
    :goto_1
    return-void
.end method

.method public setRestrictAspect(Ljava/lang/String;Z)V
    .locals 4
    .param p1, "pkg"    # Ljava/lang/String;
    .param p2, "restrict"    # Z

    .line 881
    invoke-virtual {p0, p1}, Lcom/miui/server/MiuiCompatModePackages;->isRestrictAspect(Ljava/lang/String;)Z

    move-result v0

    .line 883
    .local v0, "curRestrict":Z
    if-eq p2, v0, :cond_1

    .line 884
    iget-object v1, p0, Lcom/miui/server/MiuiCompatModePackages;->mLock:Ljava/lang/Object;

    monitor-enter v1

    .line 885
    :try_start_0
    iget-object v2, p0, Lcom/miui/server/MiuiCompatModePackages;->mPackages:Ljava/util/HashMap;

    if-eqz p2, :cond_0

    const/4 v3, 0x1

    goto :goto_0

    :cond_0
    const/4 v3, 0x0

    :goto_0
    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    invoke-virtual {v2, p1, v3}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 886
    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 887
    invoke-direct {p0}, Lcom/miui/server/MiuiCompatModePackages;->scheduleWrite()V

    .line 888
    iget-object v1, p0, Lcom/miui/server/MiuiCompatModePackages;->mContext:Landroid/content/Context;

    const-string v2, "activity"

    invoke-virtual {v1, v2}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/app/ActivityManager;

    invoke-virtual {v1, p1}, Landroid/app/ActivityManager;->forceStopPackage(Ljava/lang/String;)V

    goto :goto_1

    .line 886
    :catchall_0
    move-exception v2

    :try_start_1
    monitor-exit v1
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    throw v2

    .line 890
    :cond_1
    :goto_1
    return-void
.end method

.method public updateCloudDataAsync()V
    .locals 2

    .line 221
    iget-object v0, p0, Lcom/miui/server/MiuiCompatModePackages;->mHandler:Lcom/miui/server/MiuiCompatModePackages$CompatHandler;

    const/16 v1, 0x6c

    invoke-virtual {v0, v1}, Lcom/miui/server/MiuiCompatModePackages$CompatHandler;->removeMessages(I)V

    .line 222
    iget-object v0, p0, Lcom/miui/server/MiuiCompatModePackages;->mHandler:Lcom/miui/server/MiuiCompatModePackages$CompatHandler;

    invoke-virtual {v0, v1}, Lcom/miui/server/MiuiCompatModePackages$CompatHandler;->sendEmptyMessage(I)Z

    .line 223
    return-void
.end method
