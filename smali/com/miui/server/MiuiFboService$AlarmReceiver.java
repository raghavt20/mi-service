public class com.miui.server.MiuiFboService$AlarmReceiver extends android.content.BroadcastReceiver {
	 /* .source "MiuiFboService.java" */
	 /* # annotations */
	 /* .annotation system Ldalvik/annotation/EnclosingClass; */
	 /* value = Lcom/miui/server/MiuiFboService; */
} // .end annotation
/* .annotation system Ldalvik/annotation/InnerClass; */
/* accessFlags = 0x9 */
/* name = "AlarmReceiver" */
} // .end annotation
/* # direct methods */
public com.miui.server.MiuiFboService$AlarmReceiver ( ) {
/* .locals 0 */
/* .line 299 */
/* invoke-direct {p0}, Landroid/content/BroadcastReceiver;-><init>()V */
return;
} // .end method
/* # virtual methods */
public void onReceive ( android.content.Context p0, android.content.Intent p1 ) {
/* .locals 8 */
/* .param p1, "context" # Landroid/content/Context; */
/* .param p2, "intent" # Landroid/content/Intent; */
/* .line 303 */
com.miui.server.MiuiFboService .-$$Nest$sfgetTAG ( );
/* new-instance v1, Ljava/lang/StringBuilder; */
/* invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V */
final String v2 = "received the broadcast and intent.action : "; // const-string v2, "received the broadcast and intent.action : "
(( java.lang.StringBuilder ) v1 ).append ( v2 ); // invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( android.content.Intent ) p2 ).getAction ( ); // invoke-virtual {p2}, Landroid/content/Intent;->getAction()Ljava/lang/String;
(( java.lang.StringBuilder ) v1 ).append ( v2 ); // invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
final String v2 = ",intent.getStringExtra:"; // const-string v2, ",intent.getStringExtra:"
(( java.lang.StringBuilder ) v1 ).append ( v2 ); // invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
/* .line 304 */
final String v2 = "appList"; // const-string v2, "appList"
(( android.content.Intent ) p2 ).getStringExtra ( v2 ); // invoke-virtual {p2, v2}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;
(( java.lang.StringBuilder ) v1 ).append ( v3 ); // invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v1 ).toString ( ); // invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
/* .line 303 */
android.util.Slog .d ( v0,v1 );
/* .line 305 */
(( android.content.Intent ) p2 ).getAction ( ); // invoke-virtual {p2}, Landroid/content/Intent;->getAction()Ljava/lang/String;
v1 = (( java.lang.String ) v0 ).hashCode ( ); // invoke-virtual {v0}, Ljava/lang/String;->hashCode()I
int v3 = 3; // const/4 v3, 0x3
int v4 = 1; // const/4 v4, 0x1
int v5 = 2; // const/4 v5, 0x2
/* sparse-switch v1, :sswitch_data_0 */
} // :cond_0
/* :sswitch_0 */
final String v1 = "miui.intent.action.stop"; // const-string v1, "miui.intent.action.stop"
v0 = (( java.lang.String ) v0 ).equals ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z
if ( v0 != null) { // if-eqz v0, :cond_0
/* move v0, v3 */
/* :sswitch_1 */
final String v1 = "miui.intent.action.start"; // const-string v1, "miui.intent.action.start"
v0 = (( java.lang.String ) v0 ).equals ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z
if ( v0 != null) { // if-eqz v0, :cond_0
	 int v0 = 0; // const/4 v0, 0x0
	 /* :sswitch_2 */
	 final String v1 = "miui.intent.action.transferFboTrigger"; // const-string v1, "miui.intent.action.transferFboTrigger"
	 v0 = 	 (( java.lang.String ) v0 ).equals ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z
	 if ( v0 != null) { // if-eqz v0, :cond_0
		 /* move v0, v5 */
		 /* :sswitch_3 */
		 final String v1 = "miui.intent.action.startAgain"; // const-string v1, "miui.intent.action.startAgain"
		 v0 = 		 (( java.lang.String ) v0 ).equals ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z
		 if ( v0 != null) { // if-eqz v0, :cond_0
			 /* move v0, v4 */
		 } // :goto_0
		 int v0 = -1; // const/4 v0, -0x1
	 } // :goto_1
	 /* const-wide/16 v6, 0x3e8 */
	 /* packed-switch v0, :pswitch_data_0 */
	 /* goto/16 :goto_2 */
	 /* .line 327 */
	 /* :pswitch_0 */
	 com.miui.server.MiuiFboService .-$$Nest$sfgetsInstance ( );
	 v0 = 	 (( com.miui.server.MiuiFboService ) v0 ).getGlobalSwitch ( ); // invoke-virtual {v0}, Lcom/miui/server/MiuiFboService;->getGlobalSwitch()Z
	 /* if-nez v0, :cond_1 */
	 com.miui.server.MiuiFboService .-$$Nest$sfgetsInstance ( );
	 com.miui.server.MiuiFboService .-$$Nest$fgetmFboNativeService ( v0 );
	 if ( v0 != null) { // if-eqz v0, :cond_2
		 /* .line 328 */
	 } // :cond_1
	 com.miui.server.MiuiFboService .-$$Nest$sfgetsInstance ( );
	 /* const-string/jumbo v1, "stop" */
	 (( com.miui.server.MiuiFboService ) v0 ).deliverMessage ( v1, v3, v6, v7 ); // invoke-virtual {v0, v1, v3, v6, v7}, Lcom/miui/server/MiuiFboService;->deliverMessage(Ljava/lang/String;IJ)V
	 /* .line 329 */
	 com.miui.server.MiuiFboService .-$$Nest$sfgetTAG ( );
	 final String v1 = "execute STOP"; // const-string v1, "execute STOP"
	 android.util.Slog .d ( v0,v1 );
	 /* goto/16 :goto_2 */
	 /* .line 320 */
	 /* :pswitch_1 */
	 (( android.content.Intent ) p2 ).getStringExtra ( v2 ); // invoke-virtual {p2, v2}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;
	 if ( v0 != null) { // if-eqz v0, :cond_2
		 (( android.content.Intent ) p2 ).getStringExtra ( v2 ); // invoke-virtual {p2, v2}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;
		 v0 = 		 (( java.lang.String ) v0 ).length ( ); // invoke-virtual {v0}, Ljava/lang/String;->length()I
		 /* if-lez v0, :cond_2 */
		 /* .line 321 */
		 com.miui.server.MiuiFboService .-$$Nest$sfgetsInstance ( );
		 (( android.content.Intent ) p2 ).getStringExtra ( v2 ); // invoke-virtual {p2, v2}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;
		 (( com.miui.server.MiuiFboService ) v0 ).FBO_trigger ( v1 ); // invoke-virtual {v0, v1}, Lcom/miui/server/MiuiFboService;->FBO_trigger(Ljava/lang/String;)V
		 /* .line 322 */
		 com.miui.server.MiuiFboService .-$$Nest$sfgetTAG ( );
		 /* new-instance v1, Ljava/lang/StringBuilder; */
		 /* invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V */
		 final String v3 = "execute TRANSFERFBOTRIGGER and appList:"; // const-string v3, "execute TRANSFERFBOTRIGGER and appList:"
		 (( java.lang.StringBuilder ) v1 ).append ( v3 ); // invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
		 (( android.content.Intent ) p2 ).getStringExtra ( v2 ); // invoke-virtual {p2, v2}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;
		 (( java.lang.StringBuilder ) v1 ).append ( v2 ); // invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
		 (( java.lang.StringBuilder ) v1 ).toString ( ); // invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
		 android.util.Slog .d ( v0,v1 );
		 /* .line 315 */
		 /* :pswitch_2 */
		 com.miui.server.MiuiFboService .-$$Nest$sfgetsInstance ( );
		 com.miui.server.MiuiFboService .-$$Nest$sfgetpackageNameList ( );
		 (( java.util.ArrayList ) v1 ).toString ( ); // invoke-virtual {v1}, Ljava/util/ArrayList;->toString()Ljava/lang/String;
		 (( com.miui.server.MiuiFboService ) v0 ).deliverMessage ( v1, v5, v6, v7 ); // invoke-virtual {v0, v1, v5, v6, v7}, Lcom/miui/server/MiuiFboService;->deliverMessage(Ljava/lang/String;IJ)V
		 /* .line 316 */
		 com.miui.server.MiuiFboService .-$$Nest$sfgetTAG ( );
		 final String v1 = "execute START_FBO_AGAIN"; // const-string v1, "execute START_FBO_AGAIN"
		 android.util.Slog .d ( v0,v1 );
		 /* .line 317 */
		 /* .line 308 */
		 /* :pswitch_3 */
		 com.miui.server.MiuiFboService .-$$Nest$sfgetsInstance ( );
		 v0 = 		 com.miui.server.MiuiFboService .-$$Nest$misWithinTheTimeInterval ( v0 );
		 if ( v0 != null) { // if-eqz v0, :cond_2
			 com.miui.server.MiuiFboService .-$$Nest$sfgetsInstance ( );
			 v0 = 			 (( com.miui.server.MiuiFboService ) v0 ).getGlobalSwitch ( ); // invoke-virtual {v0}, Lcom/miui/server/MiuiFboService;->getGlobalSwitch()Z
			 /* if-nez v0, :cond_2 */
			 /* .line 309 */
			 com.miui.server.MiuiFboService .-$$Nest$sfgetsInstance ( );
			 com.miui.server.MiuiFboService .-$$Nest$sfgetpackageNameList ( );
			 (( java.util.ArrayList ) v1 ).toString ( ); // invoke-virtual {v1}, Ljava/util/ArrayList;->toString()Ljava/lang/String;
			 (( com.miui.server.MiuiFboService ) v0 ).deliverMessage ( v1, v4, v6, v7 ); // invoke-virtual {v0, v1, v4, v6, v7}, Lcom/miui/server/MiuiFboService;->deliverMessage(Ljava/lang/String;IJ)V
			 /* .line 310 */
			 com.miui.server.MiuiFboService .-$$Nest$sfgetTAG ( );
			 final String v1 = "execute START_FBO"; // const-string v1, "execute START_FBO"
			 android.util.Slog .d ( v0,v1 );
			 /* .line 334 */
		 } // :cond_2
	 } // :goto_2
	 return;
	 /* :sswitch_data_0 */
	 /* .sparse-switch */
	 /* -0x769280fe -> :sswitch_3 */
	 /* -0x1bdab3cc -> :sswitch_2 */
	 /* 0x480cf2fe -> :sswitch_1 */
	 /* 0x4ca595e6 -> :sswitch_0 */
} // .end sparse-switch
/* :pswitch_data_0 */
/* .packed-switch 0x0 */
/* :pswitch_3 */
/* :pswitch_2 */
/* :pswitch_1 */
/* :pswitch_0 */
} // .end packed-switch
} // .end method
