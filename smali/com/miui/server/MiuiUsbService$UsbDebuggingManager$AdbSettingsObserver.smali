.class Lcom/miui/server/MiuiUsbService$UsbDebuggingManager$AdbSettingsObserver;
.super Landroid/database/ContentObserver;
.source "MiuiUsbService.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/miui/server/MiuiUsbService$UsbDebuggingManager;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "AdbSettingsObserver"
.end annotation


# instance fields
.field final synthetic this$1:Lcom/miui/server/MiuiUsbService$UsbDebuggingManager;


# direct methods
.method public constructor <init>(Lcom/miui/server/MiuiUsbService$UsbDebuggingManager;)V
    .locals 0

    .line 187
    iput-object p1, p0, Lcom/miui/server/MiuiUsbService$UsbDebuggingManager$AdbSettingsObserver;->this$1:Lcom/miui/server/MiuiUsbService$UsbDebuggingManager;

    .line 188
    const/4 p1, 0x0

    invoke-direct {p0, p1}, Landroid/database/ContentObserver;-><init>(Landroid/os/Handler;)V

    .line 189
    return-void
.end method


# virtual methods
.method public onChange(Z)V
    .locals 3
    .param p1, "selfChange"    # Z

    .line 193
    iget-object v0, p0, Lcom/miui/server/MiuiUsbService$UsbDebuggingManager$AdbSettingsObserver;->this$1:Lcom/miui/server/MiuiUsbService$UsbDebuggingManager;

    invoke-static {v0}, Lcom/miui/server/MiuiUsbService$UsbDebuggingManager;->-$$Nest$fgetmContentResolver(Lcom/miui/server/MiuiUsbService$UsbDebuggingManager;)Landroid/content/ContentResolver;

    move-result-object v0

    const-string v1, "adb_enabled"

    const/4 v2, 0x0

    invoke-static {v0, v1, v2}, Landroid/provider/Settings$Secure;->getInt(Landroid/content/ContentResolver;Ljava/lang/String;I)I

    move-result v0

    if-lez v0, :cond_0

    const/4 v2, 0x1

    :cond_0
    move v0, v2

    .line 195
    .local v0, "enable":Z
    iget-object v1, p0, Lcom/miui/server/MiuiUsbService$UsbDebuggingManager$AdbSettingsObserver;->this$1:Lcom/miui/server/MiuiUsbService$UsbDebuggingManager;

    invoke-virtual {v1, v0}, Lcom/miui/server/MiuiUsbService$UsbDebuggingManager;->setAdbEnabled(Z)V

    .line 196
    return-void
.end method
