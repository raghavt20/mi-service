public class com.miui.server.RestrictAppNetManager {
	 /* .source "RestrictAppNetManager.java" */
	 /* # static fields */
	 private static final Integer RULE_ALLOW;
	 private static final Integer RULE_RESTRICT;
	 private static final java.lang.String TAG;
	 private static final Integer TYPE_ALL;
	 private static final android.net.Uri URI_CLOUD_DEVICE_RELEASED_NOTIFY;
	 private static final android.content.BroadcastReceiver mAppInstallReceiver;
	 private static Long sLastUpdateTime;
	 private static java.util.ArrayList sRestrictedAppListBeforeRelease;
	 /* .annotation system Ldalvik/annotation/Signature; */
	 /* value = { */
	 /* "Ljava/util/ArrayList<", */
	 /* "Ljava/lang/String;", */
	 /* ">;" */
	 /* } */
} // .end annotation
} // .end field
private static com.android.server.net.NetworkManagementServiceStub sService;
/* # direct methods */
static com.android.server.net.NetworkManagementServiceStub -$$Nest$sfgetsService ( ) { //bridge//synthethic
/* .locals 1 */
v0 = com.miui.server.RestrictAppNetManager.sService;
} // .end method
static Boolean -$$Nest$smisAllowAccessInternet ( java.lang.String p0 ) { //bridge//synthethic
/* .locals 0 */
p0 = com.miui.server.RestrictAppNetManager .isAllowAccessInternet ( p0 );
} // .end method
static void -$$Nest$smtryDownloadCloudData ( android.content.Context p0 ) { //bridge//synthethic
/* .locals 0 */
com.miui.server.RestrictAppNetManager .tryDownloadCloudData ( p0 );
return;
} // .end method
static void -$$Nest$smupdateFirewallRule ( android.content.Context p0, Integer p1 ) { //bridge//synthethic
/* .locals 0 */
com.miui.server.RestrictAppNetManager .updateFirewallRule ( p0,p1 );
return;
} // .end method
static void -$$Nest$smupdateRestrictAppNetProp ( android.content.Context p0 ) { //bridge//synthethic
/* .locals 0 */
com.miui.server.RestrictAppNetManager .updateRestrictAppNetProp ( p0 );
return;
} // .end method
static com.miui.server.RestrictAppNetManager ( ) {
/* .locals 2 */
/* .line 31 */
/* nop */
/* .line 32 */
final String v0 = "content://com.android.settings.cloud.CloudSettings/device_released"; // const-string v0, "content://com.android.settings.cloud.CloudSettings/device_released"
android.net.Uri .parse ( v0 );
/* .line 33 */
/* const-wide/16 v0, 0x0 */
/* sput-wide v0, Lcom/miui/server/RestrictAppNetManager;->sLastUpdateTime:J */
/* .line 204 */
/* new-instance v0, Lcom/miui/server/RestrictAppNetManager$3; */
/* invoke-direct {v0}, Lcom/miui/server/RestrictAppNetManager$3;-><init>()V */
return;
} // .end method
public com.miui.server.RestrictAppNetManager ( ) {
/* .locals 0 */
/* .line 26 */
/* invoke-direct {p0}, Ljava/lang/Object;-><init>()V */
return;
} // .end method
private static Integer getUidByPackageName ( android.content.Context p0, java.lang.String p1 ) {
/* .locals 4 */
/* .param p0, "context" # Landroid/content/Context; */
/* .param p1, "pkgName" # Ljava/lang/String; */
/* .line 194 */
(( android.content.Context ) p0 ).getPackageManager ( ); // invoke-virtual {p0}, Landroid/content/Context;->getPackageManager()Landroid/content/pm/PackageManager;
/* .line 196 */
/* .local v0, "pm":Landroid/content/pm/PackageManager; */
int v1 = 0; // const/4 v1, 0x0
try { // :try_start_0
	 (( android.content.pm.PackageManager ) v0 ).getApplicationInfo ( p1, v1 ); // invoke-virtual {v0, p1, v1}, Landroid/content/pm/PackageManager;->getApplicationInfo(Ljava/lang/String;I)Landroid/content/pm/ApplicationInfo;
	 /* .line 197 */
	 /* .local v1, "appInfo":Landroid/content/pm/ApplicationInfo; */
	 /* iget v2, v1, Landroid/content/pm/ApplicationInfo;->uid:I */
	 /* :try_end_0 */
	 /* .catch Landroid/content/pm/PackageManager$NameNotFoundException; {:try_start_0 ..:try_end_0} :catch_0 */
	 /* .line 198 */
} // .end local v1 # "appInfo":Landroid/content/pm/ApplicationInfo;
/* :catch_0 */
/* move-exception v1 */
/* .line 199 */
/* .local v1, "e":Landroid/content/pm/PackageManager$NameNotFoundException; */
/* new-instance v2, Ljava/lang/StringBuilder; */
/* invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V */
final String v3 = "not find packageName :"; // const-string v3, "not find packageName :"
(( java.lang.StringBuilder ) v2 ).append ( v3 ); // invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v2 ).append ( p1 ); // invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v2 ).toString ( ); // invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
final String v3 = "RestrictAppNetManager"; // const-string v3, "RestrictAppNetManager"
android.util.Log .i ( v3,v2 );
/* .line 201 */
} // .end local v1 # "e":Landroid/content/pm/PackageManager$NameNotFoundException;
int v1 = -1; // const/4 v1, -0x1
} // .end method
static void init ( android.content.Context p0 ) {
/* .locals 3 */
/* .param p0, "context" # Landroid/content/Context; */
/* .line 39 */
final String v0 = "persist.sys.released"; // const-string v0, "persist.sys.released"
int v1 = 0; // const/4 v1, 0x0
v0 = android.os.SystemProperties .getBoolean ( v0,v1 );
/* .line 40 */
/* .local v0, "hasReleased":Z */
/* new-instance v1, Ljava/lang/StringBuilder; */
/* invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V */
final String v2 = "init released : "; // const-string v2, "init released : "
(( java.lang.StringBuilder ) v1 ).append ( v2 ); // invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v1 ).append ( v0 ); // invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v1 ).toString ( ); // invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
final String v2 = "RestrictAppNetManager"; // const-string v2, "RestrictAppNetManager"
android.util.Log .i ( v2,v1 );
/* .line 41 */
if ( v0 != null) { // if-eqz v0, :cond_0
/* .line 42 */
return;
/* .line 45 */
} // :cond_0
/* nop */
/* .line 50 */
com.android.server.net.NetworkManagementServiceStub .getInstance ( );
/* .line 51 */
com.miui.server.RestrictAppNetManager .registerCloudDataObserver ( p0 );
/* .line 52 */
com.miui.server.RestrictAppNetManager .registerCloudDataObserver1 ( p0 );
/* .line 53 */
com.miui.server.RestrictAppNetManager .registerAppInstallReceiver ( p0 );
/* .line 55 */
/* new-instance v1, Ljava/util/ArrayList; */
/* invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V */
/* .line 56 */
final String v2 = "com.antutu.ABenchMark"; // const-string v2, "com.antutu.ABenchMark"
(( java.util.ArrayList ) v1 ).add ( v2 ); // invoke-virtual {v1, v2}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z
/* .line 57 */
v1 = com.miui.server.RestrictAppNetManager.sRestrictedAppListBeforeRelease;
final String v2 = "com.antutu.ABenchMark5"; // const-string v2, "com.antutu.ABenchMark5"
(( java.util.ArrayList ) v1 ).add ( v2 ); // invoke-virtual {v1, v2}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z
/* .line 58 */
v1 = com.miui.server.RestrictAppNetManager.sRestrictedAppListBeforeRelease;
final String v2 = "com.antutu.benchmark.bench64"; // const-string v2, "com.antutu.benchmark.bench64"
(( java.util.ArrayList ) v1 ).add ( v2 ); // invoke-virtual {v1, v2}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z
/* .line 59 */
v1 = com.miui.server.RestrictAppNetManager.sRestrictedAppListBeforeRelease;
final String v2 = "com.antutu.videobench"; // const-string v2, "com.antutu.videobench"
(( java.util.ArrayList ) v1 ).add ( v2 ); // invoke-virtual {v1, v2}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z
/* .line 60 */
v1 = com.miui.server.RestrictAppNetManager.sRestrictedAppListBeforeRelease;
final String v2 = "com.antutu.ABenchMark.GL2"; // const-string v2, "com.antutu.ABenchMark.GL2"
(( java.util.ArrayList ) v1 ).add ( v2 ); // invoke-virtual {v1, v2}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z
/* .line 61 */
v1 = com.miui.server.RestrictAppNetManager.sRestrictedAppListBeforeRelease;
final String v2 = "com.antutu.tester"; // const-string v2, "com.antutu.tester"
(( java.util.ArrayList ) v1 ).add ( v2 ); // invoke-virtual {v1, v2}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z
/* .line 62 */
v1 = com.miui.server.RestrictAppNetManager.sRestrictedAppListBeforeRelease;
final String v2 = "com.antutu.benchmark.full"; // const-string v2, "com.antutu.benchmark.full"
(( java.util.ArrayList ) v1 ).add ( v2 ); // invoke-virtual {v1, v2}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z
/* .line 63 */
v1 = com.miui.server.RestrictAppNetManager.sRestrictedAppListBeforeRelease;
final String v2 = "com.music.videogame"; // const-string v2, "com.music.videogame"
(( java.util.ArrayList ) v1 ).add ( v2 ); // invoke-virtual {v1, v2}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z
/* .line 64 */
v1 = com.miui.server.RestrictAppNetManager.sRestrictedAppListBeforeRelease;
final String v2 = "com.ludashi.benchmark"; // const-string v2, "com.ludashi.benchmark"
(( java.util.ArrayList ) v1 ).add ( v2 ); // invoke-virtual {v1, v2}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z
/* .line 65 */
v1 = com.miui.server.RestrictAppNetManager.sRestrictedAppListBeforeRelease;
final String v2 = "com.ludashi.benchmarkhd"; // const-string v2, "com.ludashi.benchmarkhd"
(( java.util.ArrayList ) v1 ).add ( v2 ); // invoke-virtual {v1, v2}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z
/* .line 66 */
v1 = com.miui.server.RestrictAppNetManager.sRestrictedAppListBeforeRelease;
final String v2 = "com.qihoo360.ludashi.cooling"; // const-string v2, "com.qihoo360.ludashi.cooling"
(( java.util.ArrayList ) v1 ).add ( v2 ); // invoke-virtual {v1, v2}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z
/* .line 67 */
v1 = com.miui.server.RestrictAppNetManager.sRestrictedAppListBeforeRelease;
final String v2 = "cn.opda.android.activity"; // const-string v2, "cn.opda.android.activity"
(( java.util.ArrayList ) v1 ).add ( v2 ); // invoke-virtual {v1, v2}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z
/* .line 68 */
v1 = com.miui.server.RestrictAppNetManager.sRestrictedAppListBeforeRelease;
final String v2 = "com.shouji.cesupaofen"; // const-string v2, "com.shouji.cesupaofen"
(( java.util.ArrayList ) v1 ).add ( v2 ); // invoke-virtual {v1, v2}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z
/* .line 69 */
v1 = com.miui.server.RestrictAppNetManager.sRestrictedAppListBeforeRelease;
final String v2 = "com.colola.mobiletest"; // const-string v2, "com.colola.mobiletest"
(( java.util.ArrayList ) v1 ).add ( v2 ); // invoke-virtual {v1, v2}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z
/* .line 70 */
v1 = com.miui.server.RestrictAppNetManager.sRestrictedAppListBeforeRelease;
/* const-string/jumbo v2, "ws.j7uxli.a6urcd" */
(( java.util.ArrayList ) v1 ).add ( v2 ); // invoke-virtual {v1, v2}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z
/* .line 71 */
v1 = com.miui.server.RestrictAppNetManager.sRestrictedAppListBeforeRelease;
final String v2 = "com.gamebench.metricscollector"; // const-string v2, "com.gamebench.metricscollector"
(( java.util.ArrayList ) v1 ).add ( v2 ); // invoke-virtual {v1, v2}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z
/* .line 72 */
v1 = com.miui.server.RestrictAppNetManager.sRestrictedAppListBeforeRelease;
final String v2 = "com.huahua.test"; // const-string v2, "com.huahua.test"
(( java.util.ArrayList ) v1 ).add ( v2 ); // invoke-virtual {v1, v2}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z
/* .line 73 */
v1 = com.miui.server.RestrictAppNetManager.sRestrictedAppListBeforeRelease;
final String v2 = "com.futuremark.dmandroid.application"; // const-string v2, "com.futuremark.dmandroid.application"
(( java.util.ArrayList ) v1 ).add ( v2 ); // invoke-virtual {v1, v2}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z
/* .line 74 */
v1 = com.miui.server.RestrictAppNetManager.sRestrictedAppListBeforeRelease;
final String v2 = "com.eembc.coremark"; // const-string v2, "com.eembc.coremark"
(( java.util.ArrayList ) v1 ).add ( v2 ); // invoke-virtual {v1, v2}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z
/* .line 75 */
v1 = com.miui.server.RestrictAppNetManager.sRestrictedAppListBeforeRelease;
final String v2 = "com.rightware.BasemarkOSII"; // const-string v2, "com.rightware.BasemarkOSII"
(( java.util.ArrayList ) v1 ).add ( v2 ); // invoke-virtual {v1, v2}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z
/* .line 76 */
v1 = com.miui.server.RestrictAppNetManager.sRestrictedAppListBeforeRelease;
final String v2 = "com.glbenchmark.glbenchmark27"; // const-string v2, "com.glbenchmark.glbenchmark27"
(( java.util.ArrayList ) v1 ).add ( v2 ); // invoke-virtual {v1, v2}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z
/* .line 77 */
v1 = com.miui.server.RestrictAppNetManager.sRestrictedAppListBeforeRelease;
final String v2 = "com.greenecomputing.linpack"; // const-string v2, "com.greenecomputing.linpack"
(( java.util.ArrayList ) v1 ).add ( v2 ); // invoke-virtual {v1, v2}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z
/* .line 78 */
v1 = com.miui.server.RestrictAppNetManager.sRestrictedAppListBeforeRelease;
final String v2 = "eu.chainfire.cfbench"; // const-string v2, "eu.chainfire.cfbench"
(( java.util.ArrayList ) v1 ).add ( v2 ); // invoke-virtual {v1, v2}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z
/* .line 79 */
v1 = com.miui.server.RestrictAppNetManager.sRestrictedAppListBeforeRelease;
final String v2 = "com.primatelabs.geekbench"; // const-string v2, "com.primatelabs.geekbench"
(( java.util.ArrayList ) v1 ).add ( v2 ); // invoke-virtual {v1, v2}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z
/* .line 80 */
v1 = com.miui.server.RestrictAppNetManager.sRestrictedAppListBeforeRelease;
final String v2 = "com.primatelabs.geekbench3"; // const-string v2, "com.primatelabs.geekbench3"
(( java.util.ArrayList ) v1 ).add ( v2 ); // invoke-virtual {v1, v2}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z
/* .line 81 */
v1 = com.miui.server.RestrictAppNetManager.sRestrictedAppListBeforeRelease;
final String v2 = "com.quicinc.vellamo"; // const-string v2, "com.quicinc.vellamo"
(( java.util.ArrayList ) v1 ).add ( v2 ); // invoke-virtual {v1, v2}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z
/* .line 82 */
v1 = com.miui.server.RestrictAppNetManager.sRestrictedAppListBeforeRelease;
final String v2 = "com.aurorasoftworks.quadrant.ui.advanced"; // const-string v2, "com.aurorasoftworks.quadrant.ui.advanced"
(( java.util.ArrayList ) v1 ).add ( v2 ); // invoke-virtual {v1, v2}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z
/* .line 83 */
v1 = com.miui.server.RestrictAppNetManager.sRestrictedAppListBeforeRelease;
final String v2 = "com.aurorasoftworks.quadrant.ui.standard"; // const-string v2, "com.aurorasoftworks.quadrant.ui.standard"
(( java.util.ArrayList ) v1 ).add ( v2 ); // invoke-virtual {v1, v2}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z
/* .line 84 */
v1 = com.miui.server.RestrictAppNetManager.sRestrictedAppListBeforeRelease;
final String v2 = "eu.chainfire.perfmon"; // const-string v2, "eu.chainfire.perfmon"
(( java.util.ArrayList ) v1 ).add ( v2 ); // invoke-virtual {v1, v2}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z
/* .line 85 */
v1 = com.miui.server.RestrictAppNetManager.sRestrictedAppListBeforeRelease;
final String v2 = "com.evozi.deviceid"; // const-string v2, "com.evozi.deviceid"
(( java.util.ArrayList ) v1 ).add ( v2 ); // invoke-virtual {v1, v2}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z
/* .line 86 */
v1 = com.miui.server.RestrictAppNetManager.sRestrictedAppListBeforeRelease;
final String v2 = "com.finalwire.aida64"; // const-string v2, "com.finalwire.aida64"
(( java.util.ArrayList ) v1 ).add ( v2 ); // invoke-virtual {v1, v2}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z
/* .line 87 */
v1 = com.miui.server.RestrictAppNetManager.sRestrictedAppListBeforeRelease;
final String v2 = "com.cpuid.cpu_z"; // const-string v2, "com.cpuid.cpu_z"
(( java.util.ArrayList ) v1 ).add ( v2 ); // invoke-virtual {v1, v2}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z
/* .line 88 */
v1 = com.miui.server.RestrictAppNetManager.sRestrictedAppListBeforeRelease;
final String v2 = "rs.in.luka.android.pi"; // const-string v2, "rs.in.luka.android.pi"
(( java.util.ArrayList ) v1 ).add ( v2 ); // invoke-virtual {v1, v2}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z
/* .line 89 */
v1 = com.miui.server.RestrictAppNetManager.sRestrictedAppListBeforeRelease;
final String v2 = "com.uzywpq.cqlzahm"; // const-string v2, "com.uzywpq.cqlzahm"
(( java.util.ArrayList ) v1 ).add ( v2 ); // invoke-virtual {v1, v2}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z
/* .line 90 */
v1 = com.miui.server.RestrictAppNetManager.sRestrictedAppListBeforeRelease;
final String v2 = "com.xidige.androidinfo"; // const-string v2, "com.xidige.androidinfo"
(( java.util.ArrayList ) v1 ).add ( v2 ); // invoke-virtual {v1, v2}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z
/* .line 91 */
v1 = com.miui.server.RestrictAppNetManager.sRestrictedAppListBeforeRelease;
final String v2 = "com.appems.hawkeye"; // const-string v2, "com.appems.hawkeye"
(( java.util.ArrayList ) v1 ).add ( v2 ); // invoke-virtual {v1, v2}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z
/* .line 92 */
v1 = com.miui.server.RestrictAppNetManager.sRestrictedAppListBeforeRelease;
final String v2 = "com.tyyj89.androidsuperinfo"; // const-string v2, "com.tyyj89.androidsuperinfo"
(( java.util.ArrayList ) v1 ).add ( v2 ); // invoke-virtual {v1, v2}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z
/* .line 93 */
v1 = com.miui.server.RestrictAppNetManager.sRestrictedAppListBeforeRelease;
final String v2 = "com.ft1gp"; // const-string v2, "com.ft1gp"
(( java.util.ArrayList ) v1 ).add ( v2 ); // invoke-virtual {v1, v2}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z
/* .line 94 */
v1 = com.miui.server.RestrictAppNetManager.sRestrictedAppListBeforeRelease;
/* const-string/jumbo v2, "ws.k6t2we.b4zyjdjv" */
(( java.util.ArrayList ) v1 ).add ( v2 ); // invoke-virtual {v1, v2}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z
/* .line 95 */
v1 = com.miui.server.RestrictAppNetManager.sRestrictedAppListBeforeRelease;
final String v2 = "com.myapp.dongxie_app1"; // const-string v2, "com.myapp.dongxie_app1"
(( java.util.ArrayList ) v1 ).add ( v2 ); // invoke-virtual {v1, v2}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z
/* .line 96 */
v1 = com.miui.server.RestrictAppNetManager.sRestrictedAppListBeforeRelease;
final String v2 = "com.shoujijiance.zj"; // const-string v2, "com.shoujijiance.zj"
(( java.util.ArrayList ) v1 ).add ( v2 ); // invoke-virtual {v1, v2}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z
/* .line 97 */
v1 = com.miui.server.RestrictAppNetManager.sRestrictedAppListBeforeRelease;
final String v2 = "com.qrj.test"; // const-string v2, "com.qrj.test"
(( java.util.ArrayList ) v1 ).add ( v2 ); // invoke-virtual {v1, v2}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z
/* .line 98 */
v1 = com.miui.server.RestrictAppNetManager.sRestrictedAppListBeforeRelease;
final String v2 = "com.appems.testonetest"; // const-string v2, "com.appems.testonetest"
(( java.util.ArrayList ) v1 ).add ( v2 ); // invoke-virtual {v1, v2}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z
/* .line 99 */
v1 = com.miui.server.RestrictAppNetManager.sRestrictedAppListBeforeRelease;
final String v2 = "com.andromeda.androbench2"; // const-string v2, "com.andromeda.androbench2"
(( java.util.ArrayList ) v1 ).add ( v2 ); // invoke-virtual {v1, v2}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z
/* .line 100 */
v1 = com.miui.server.RestrictAppNetManager.sRestrictedAppListBeforeRelease;
final String v2 = "com.primatelabs.geekbench5.corporate"; // const-string v2, "com.primatelabs.geekbench5.corporate"
(( java.util.ArrayList ) v1 ).add ( v2 ); // invoke-virtual {v1, v2}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z
/* .line 101 */
v1 = com.miui.server.RestrictAppNetManager.sRestrictedAppListBeforeRelease;
final String v2 = "net.kishonti.gfxbench.vulkan.v50000.corporate"; // const-string v2, "net.kishonti.gfxbench.vulkan.v50000.corporate"
(( java.util.ArrayList ) v1 ).add ( v2 ); // invoke-virtual {v1, v2}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z
/* .line 102 */
v1 = com.miui.server.RestrictAppNetManager.sRestrictedAppListBeforeRelease;
final String v2 = "com.antutu.ABenchMark.lite"; // const-string v2, "com.antutu.ABenchMark.lite"
(( java.util.ArrayList ) v1 ).add ( v2 ); // invoke-virtual {v1, v2}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z
/* .line 103 */
v1 = com.miui.server.RestrictAppNetManager.sRestrictedAppListBeforeRelease;
final String v2 = "com.antutu.aibenchmark"; // const-string v2, "com.antutu.aibenchmark"
(( java.util.ArrayList ) v1 ).add ( v2 ); // invoke-virtual {v1, v2}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z
/* .line 104 */
v1 = com.miui.server.RestrictAppNetManager.sRestrictedAppListBeforeRelease;
final String v2 = "com.ludashi.benchmark2"; // const-string v2, "com.ludashi.benchmark2"
(( java.util.ArrayList ) v1 ).add ( v2 ); // invoke-virtual {v1, v2}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z
/* .line 105 */
v1 = com.miui.server.RestrictAppNetManager.sRestrictedAppListBeforeRelease;
final String v2 = "com.ludashi.aibench"; // const-string v2, "com.ludashi.aibench"
(( java.util.ArrayList ) v1 ).add ( v2 ); // invoke-virtual {v1, v2}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z
/* .line 106 */
v1 = com.miui.server.RestrictAppNetManager.sRestrictedAppListBeforeRelease;
final String v2 = "com.primatelabs.geekbench5c"; // const-string v2, "com.primatelabs.geekbench5c"
(( java.util.ArrayList ) v1 ).add ( v2 ); // invoke-virtual {v1, v2}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z
/* .line 107 */
v1 = com.miui.server.RestrictAppNetManager.sRestrictedAppListBeforeRelease;
final String v2 = "com.primatelabs.geekbench5"; // const-string v2, "com.primatelabs.geekbench5"
(( java.util.ArrayList ) v1 ).add ( v2 ); // invoke-virtual {v1, v2}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z
/* .line 108 */
v1 = com.miui.server.RestrictAppNetManager.sRestrictedAppListBeforeRelease;
final String v2 = "com.primatelabs.geekbench4.corporate"; // const-string v2, "com.primatelabs.geekbench4.corporate"
(( java.util.ArrayList ) v1 ).add ( v2 ); // invoke-virtual {v1, v2}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z
/* .line 109 */
v1 = com.miui.server.RestrictAppNetManager.sRestrictedAppListBeforeRelease;
final String v2 = "net.kishonti.gfxbench.gl.v40001.corporate"; // const-string v2, "net.kishonti.gfxbench.gl.v40001.corporate"
(( java.util.ArrayList ) v1 ).add ( v2 ); // invoke-virtual {v1, v2}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z
/* .line 110 */
v1 = com.miui.server.RestrictAppNetManager.sRestrictedAppListBeforeRelease;
final String v2 = "org.benchmark.demo"; // const-string v2, "org.benchmark.demo"
(( java.util.ArrayList ) v1 ).add ( v2 ); // invoke-virtual {v1, v2}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z
/* .line 111 */
v1 = com.miui.server.RestrictAppNetManager.sRestrictedAppListBeforeRelease;
final String v2 = "com.android.gputest"; // const-string v2, "com.android.gputest"
(( java.util.ArrayList ) v1 ).add ( v2 ); // invoke-virtual {v1, v2}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z
/* .line 112 */
v1 = com.miui.server.RestrictAppNetManager.sRestrictedAppListBeforeRelease;
final String v2 = "android.test.app"; // const-string v2, "android.test.app"
(( java.util.ArrayList ) v1 ).add ( v2 ); // invoke-virtual {v1, v2}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z
/* .line 113 */
v1 = com.miui.server.RestrictAppNetManager.sRestrictedAppListBeforeRelease;
final String v2 = "com.ioncannon.cpuburn.gpugflops"; // const-string v2, "com.ioncannon.cpuburn.gpugflops"
(( java.util.ArrayList ) v1 ).add ( v2 ); // invoke-virtual {v1, v2}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z
/* .line 114 */
v1 = com.miui.server.RestrictAppNetManager.sRestrictedAppListBeforeRelease;
final String v2 = "ioncannon.com.andspecmod"; // const-string v2, "ioncannon.com.andspecmod"
(( java.util.ArrayList ) v1 ).add ( v2 ); // invoke-virtual {v1, v2}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z
/* .line 115 */
v1 = com.miui.server.RestrictAppNetManager.sRestrictedAppListBeforeRelease;
/* const-string/jumbo v2, "skynet.cputhrottlingtest" */
(( java.util.ArrayList ) v1 ).add ( v2 ); // invoke-virtual {v1, v2}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z
/* .line 116 */
v1 = com.miui.server.RestrictAppNetManager.sRestrictedAppListBeforeRelease;
final String v2 = "com.primatelabs.geekbench6"; // const-string v2, "com.primatelabs.geekbench6"
(( java.util.ArrayList ) v1 ).add ( v2 ); // invoke-virtual {v1, v2}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z
/* .line 118 */
int v1 = 1; // const/4 v1, 0x1
com.miui.server.RestrictAppNetManager .updateFirewallRule ( p0,v1 );
/* .line 119 */
return;
} // .end method
private static Boolean isAllowAccessInternet ( java.lang.String p0 ) {
/* .locals 3 */
/* .param p0, "packageName" # Ljava/lang/String; */
/* .line 122 */
final String v0 = "persist.sys.released"; // const-string v0, "persist.sys.released"
int v1 = 0; // const/4 v1, 0x0
v0 = android.os.SystemProperties .getBoolean ( v0,v1 );
/* .line 123 */
/* .local v0, "hasReleased":Z */
int v1 = 1; // const/4 v1, 0x1
/* if-nez v0, :cond_1 */
v2 = com.miui.server.RestrictAppNetManager.sRestrictedAppListBeforeRelease;
/* if-nez v2, :cond_0 */
/* .line 126 */
} // :cond_0
v2 = (( java.util.ArrayList ) v2 ).contains ( p0 ); // invoke-virtual {v2, p0}, Ljava/util/ArrayList;->contains(Ljava/lang/Object;)Z
/* xor-int/2addr v1, v2 */
/* .line 124 */
} // :cond_1
} // :goto_0
} // .end method
private static void registerAppInstallReceiver ( android.content.Context p0 ) {
/* .locals 2 */
/* .param p0, "context" # Landroid/content/Context; */
/* .line 163 */
/* new-instance v0, Landroid/content/IntentFilter; */
/* invoke-direct {v0}, Landroid/content/IntentFilter;-><init>()V */
/* .line 164 */
/* .local v0, "intentFilter":Landroid/content/IntentFilter; */
final String v1 = "android.intent.action.PACKAGE_ADDED"; // const-string v1, "android.intent.action.PACKAGE_ADDED"
(( android.content.IntentFilter ) v0 ).addAction ( v1 ); // invoke-virtual {v0, v1}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V
/* .line 165 */
final String v1 = "package"; // const-string v1, "package"
(( android.content.IntentFilter ) v0 ).addDataScheme ( v1 ); // invoke-virtual {v0, v1}, Landroid/content/IntentFilter;->addDataScheme(Ljava/lang/String;)V
/* .line 166 */
v1 = com.miui.server.RestrictAppNetManager.mAppInstallReceiver;
(( android.content.Context ) p0 ).registerReceiver ( v1, v0 ); // invoke-virtual {p0, v1, v0}, Landroid/content/Context;->registerReceiver(Landroid/content/BroadcastReceiver;Landroid/content/IntentFilter;)Landroid/content/Intent;
/* .line 167 */
return;
} // .end method
private static void registerCloudDataObserver ( android.content.Context p0 ) {
/* .locals 4 */
/* .param p0, "context" # Landroid/content/Context; */
/* .line 142 */
(( android.content.Context ) p0 ).getContentResolver ( ); // invoke-virtual {p0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;
android.provider.MiuiSettings$SettingsCloudData .getCloudDataNotifyUri ( );
/* new-instance v2, Lcom/miui/server/RestrictAppNetManager$1; */
/* .line 143 */
com.android.internal.os.BackgroundThread .getHandler ( );
/* invoke-direct {v2, v3, p0}, Lcom/miui/server/RestrictAppNetManager$1;-><init>(Landroid/os/Handler;Landroid/content/Context;)V */
/* .line 142 */
int v3 = 1; // const/4 v3, 0x1
(( android.content.ContentResolver ) v0 ).registerContentObserver ( v1, v3, v2 ); // invoke-virtual {v0, v1, v3, v2}, Landroid/content/ContentResolver;->registerContentObserver(Landroid/net/Uri;ZLandroid/database/ContentObserver;)V
/* .line 149 */
return;
} // .end method
private static void registerCloudDataObserver1 ( android.content.Context p0 ) {
/* .locals 4 */
/* .param p0, "context" # Landroid/content/Context; */
/* .line 152 */
(( android.content.Context ) p0 ).getContentResolver ( ); // invoke-virtual {p0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;
v1 = com.miui.server.RestrictAppNetManager.URI_CLOUD_DEVICE_RELEASED_NOTIFY;
/* new-instance v2, Lcom/miui/server/RestrictAppNetManager$2; */
/* .line 153 */
com.android.internal.os.BackgroundThread .getHandler ( );
/* invoke-direct {v2, v3, p0}, Lcom/miui/server/RestrictAppNetManager$2;-><init>(Landroid/os/Handler;Landroid/content/Context;)V */
/* .line 152 */
int v3 = 1; // const/4 v3, 0x1
(( android.content.ContentResolver ) v0 ).registerContentObserver ( v1, v3, v2 ); // invoke-virtual {v0, v1, v3, v2}, Landroid/content/ContentResolver;->registerContentObserver(Landroid/net/Uri;ZLandroid/database/ContentObserver;)V
/* .line 160 */
return;
} // .end method
private static void tryDownloadCloudData ( android.content.Context p0 ) {
/* .locals 4 */
/* .param p0, "context" # Landroid/content/Context; */
/* .line 225 */
java.lang.System .currentTimeMillis ( );
/* move-result-wide v0 */
/* sget-wide v2, Lcom/miui/server/RestrictAppNetManager;->sLastUpdateTime:J */
/* sub-long/2addr v0, v2 */
/* const-wide/32 v2, 0x5265c00 */
/* cmp-long v0, v0, v2 */
/* if-lez v0, :cond_0 */
/* .line 226 */
java.lang.System .currentTimeMillis ( );
/* move-result-wide v0 */
/* sput-wide v0, Lcom/miui/server/RestrictAppNetManager;->sLastUpdateTime:J */
/* .line 227 */
/* new-instance v0, Landroid/content/Intent; */
final String v1 = "com.miui.action.UPDATE_RESTRICT_APP_DATA"; // const-string v1, "com.miui.action.UPDATE_RESTRICT_APP_DATA"
/* invoke-direct {v0, v1}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V */
/* .line 228 */
/* .local v0, "intent":Landroid/content/Intent; */
/* const/high16 v1, 0x1000000 */
(( android.content.Intent ) v0 ).setFlags ( v1 ); // invoke-virtual {v0, v1}, Landroid/content/Intent;->setFlags(I)Landroid/content/Intent;
/* .line 229 */
v1 = android.os.UserHandle.OWNER;
final String v2 = "com.miui.permission.UPDATE_RESTRICT_DATA"; // const-string v2, "com.miui.permission.UPDATE_RESTRICT_DATA"
(( android.content.Context ) p0 ).sendBroadcastAsUser ( v0, v1, v2 ); // invoke-virtual {p0, v0, v1, v2}, Landroid/content/Context;->sendBroadcastAsUser(Landroid/content/Intent;Landroid/os/UserHandle;Ljava/lang/String;)V
/* .line 231 */
final String v1 = "RestrictAppNetManager"; // const-string v1, "RestrictAppNetManager"
/* const-string/jumbo v2, "send\uff1a com.miui.action.UPDATE_RESTRICT_APP_DATA" */
android.util.Log .w ( v1,v2 );
/* .line 233 */
} // .end local v0 # "intent":Landroid/content/Intent;
} // :cond_0
return;
} // .end method
private static void updateFirewallRule ( android.content.Context p0, Integer p1 ) {
/* .locals 5 */
/* .param p0, "context" # Landroid/content/Context; */
/* .param p1, "rule" # I */
/* .line 130 */
v0 = com.miui.server.RestrictAppNetManager.sRestrictedAppListBeforeRelease;
if ( v0 != null) { // if-eqz v0, :cond_1
v0 = com.miui.server.RestrictAppNetManager.sService;
if ( v0 != null) { // if-eqz v0, :cond_1
/* .line 131 */
/* new-instance v0, Ljava/lang/StringBuilder; */
/* invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V */
/* const-string/jumbo v1, "updateFirewallRule : " */
(( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v0 ).append ( p1 ); // invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v0 ).toString ( ); // invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
final String v1 = "RestrictAppNetManager"; // const-string v1, "RestrictAppNetManager"
android.util.Log .i ( v1,v0 );
/* .line 132 */
v0 = com.miui.server.RestrictAppNetManager.sRestrictedAppListBeforeRelease;
(( java.util.ArrayList ) v0 ).iterator ( ); // invoke-virtual {v0}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;
v1 = } // :goto_0
if ( v1 != null) { // if-eqz v1, :cond_1
/* check-cast v1, Ljava/lang/String; */
/* .line 133 */
/* .local v1, "pkgName":Ljava/lang/String; */
v2 = com.miui.server.RestrictAppNetManager .getUidByPackageName ( p0,v1 );
/* .line 134 */
/* .local v2, "uid":I */
/* if-ltz v2, :cond_0 */
/* .line 135 */
v3 = com.miui.server.RestrictAppNetManager.sService;
int v4 = 3; // const/4 v4, 0x3
/* .line 137 */
} // .end local v1 # "pkgName":Ljava/lang/String;
} // .end local v2 # "uid":I
} // :cond_0
/* .line 139 */
} // :cond_1
return;
} // .end method
private static void updateRestrictAppNetProp ( android.content.Context p0 ) {
/* .locals 10 */
/* .param p0, "context" # Landroid/content/Context; */
/* .line 171 */
final String v0 = "RestrictAppNetManager"; // const-string v0, "RestrictAppNetManager"
final String v1 = "persist.sys.released"; // const-string v1, "persist.sys.released"
int v2 = 0; // const/4 v2, 0x0
try { // :try_start_0
v3 = android.os.SystemProperties .getBoolean ( v1,v2 );
/* .line 172 */
/* .local v3, "released":Z */
/* if-nez v3, :cond_3 */
/* .line 173 */
/* const-string/jumbo v4, "updateRestrictAppNetProp" */
android.util.Log .i ( v0,v4 );
/* .line 174 */
v4 = android.os.Build.DEVICE;
/* .line 175 */
/* .local v4, "deviceMode":Ljava/lang/String; */
/* nop */
/* .line 176 */
(( android.content.Context ) p0 ).getContentResolver ( ); // invoke-virtual {p0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;
final String v6 = "RestrictAppControl"; // const-string v6, "RestrictAppControl"
/* .line 175 */
android.provider.MiuiSettings$SettingsCloudData .getCloudDataList ( v5,v6 );
/* .line 177 */
/* .local v5, "dataList":Ljava/util/List;, "Ljava/util/List<Landroid/provider/MiuiSettings$SettingsCloudData$CloudData;>;" */
v6 = if ( v5 != null) { // if-eqz v5, :cond_2
/* if-nez v6, :cond_0 */
/* .line 180 */
} // :cond_0
v7 = } // :goto_0
if ( v7 != null) { // if-eqz v7, :cond_3
/* check-cast v7, Landroid/provider/MiuiSettings$SettingsCloudData$CloudData; */
/* .line 181 */
/* .local v7, "data":Landroid/provider/MiuiSettings$SettingsCloudData$CloudData; */
final String v8 = "released"; // const-string v8, "released"
int v9 = 0; // const/4 v9, 0x0
(( android.provider.MiuiSettings$SettingsCloudData$CloudData ) v7 ).getString ( v4, v9 ); // invoke-virtual {v7, v4, v9}, Landroid/provider/MiuiSettings$SettingsCloudData$CloudData;->getString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;
v8 = (( java.lang.String ) v8 ).equals ( v9 ); // invoke-virtual {v8, v9}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z
if ( v8 != null) { // if-eqz v8, :cond_1
/* .line 182 */
/* const-string/jumbo v6, "true" */
android.os.SystemProperties .set ( v1,v6 );
/* .line 183 */
com.miui.server.RestrictAppNetManager .updateFirewallRule ( p0,v2 );
/* :try_end_0 */
/* .catch Ljava/lang/Exception; {:try_start_0 ..:try_end_0} :catch_0 */
/* .line 184 */
return;
/* .line 186 */
} // .end local v7 # "data":Landroid/provider/MiuiSettings$SettingsCloudData$CloudData;
} // :cond_1
/* .line 178 */
} // :cond_2
} // :goto_1
return;
/* .line 190 */
} // .end local v3 # "released":Z
} // .end local v4 # "deviceMode":Ljava/lang/String;
} // .end local v5 # "dataList":Ljava/util/List;, "Ljava/util/List<Landroid/provider/MiuiSettings$SettingsCloudData$CloudData;>;"
} // :cond_3
/* .line 188 */
/* :catch_0 */
/* move-exception v1 */
/* .line 189 */
/* .local v1, "e":Ljava/lang/Exception; */
/* const-string/jumbo v2, "update released prop exception" */
android.util.Log .w ( v0,v2,v1 );
/* .line 191 */
} // .end local v1 # "e":Ljava/lang/Exception;
} // :goto_2
return;
} // .end method
