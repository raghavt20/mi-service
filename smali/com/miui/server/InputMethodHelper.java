public class com.miui.server.InputMethodHelper {
	 /* .source "InputMethodHelper.java" */
	 /* # annotations */
	 /* .annotation system Ldalvik/annotation/MemberClasses; */
	 /* value = { */
	 /* Lcom/miui/server/InputMethodHelper$InputMethodTbCmdRuleObserver; */
	 /* } */
} // .end annotation
/* # static fields */
private static final java.lang.String DEFAULT_TAOBAO_CMD_RULE;
private static final java.lang.String INPUT_METHOD_TAOBAO_CMD_ENABLE;
private static final java.lang.String INPUT_METHOD_TAOBAO_CMD_MODULE_NAME;
private static final java.lang.String INPUT_METHOD_TAOBAO_CMD_RULE;
private static final java.lang.String TAG;
private static final java.lang.String TAOBAO_RULE_ENABLE_KEY;
private static final java.lang.String TAOBAO_RULE_TEXT_KEY;
private static final android.net.Uri URI_CLOUD_ALL_DATA_NOTIFY;
/* # direct methods */
static void -$$Nest$sminitInputMethodTbCmdRule ( android.content.Context p0 ) { //bridge//synthethic
	 /* .locals 0 */
	 com.miui.server.InputMethodHelper .initInputMethodTbCmdRule ( p0 );
	 return;
} // .end method
static com.miui.server.InputMethodHelper ( ) {
	 /* .locals 1 */
	 /* .line 36 */
	 /* nop */
	 /* .line 37 */
	 final String v0 = "content://com.android.settings.cloud.CloudSettings/cloud_all_data/notify"; // const-string v0, "content://com.android.settings.cloud.CloudSettings/cloud_all_data/notify"
	 android.net.Uri .parse ( v0 );
	 /* .line 36 */
	 return;
} // .end method
public com.miui.server.InputMethodHelper ( ) {
	 /* .locals 0 */
	 /* .line 28 */
	 /* invoke-direct {p0}, Ljava/lang/Object;-><init>()V */
	 return;
} // .end method
public static void init ( android.content.Context p0 ) {
	 /* .locals 2 */
	 /* .param p0, "context" # Landroid/content/Context; */
	 /* .line 40 */
	 v0 = 	 com.miui.server.InputMethodHelper .isSupportMiuiBottom ( );
	 /* if-nez v0, :cond_0 */
	 /* .line 41 */
	 final String v0 = "InputMethodHelper"; // const-string v0, "InputMethodHelper"
	 final String v1 = "Not support miui bottom."; // const-string v1, "Not support miui bottom."
	 android.util.Log .i ( v0,v1 );
	 /* .line 42 */
	 return;
	 /* .line 44 */
} // :cond_0
com.miui.server.InputMethodHelper .registerContentObserver ( p0 );
/* .line 45 */
com.miui.server.InputMethodHelper .initInputMethodTbCmdRule ( p0 );
/* .line 46 */
int v0 = 0; // const/4 v0, 0x0
com.miui.server.InputMethodHelper .initForUser ( p0,v0 );
/* .line 77 */
return;
} // .end method
private static void initForUser ( android.content.Context p0, Integer p1 ) {
/* .locals 4 */
/* .param p0, "context" # Landroid/content/Context; */
/* .param p1, "userId" # I */
/* .line 93 */
v0 = com.miui.server.InputMethodHelper .isMiuiBottomNeedSet ( p0,p1 );
/* if-nez v0, :cond_0 */
/* .line 94 */
return;
/* .line 96 */
} // :cond_0
v0 = com.miui.server.InputMethodHelper .isFullScreenDevice ( );
final String v1 = "enable_miui_ime_bottom_view"; // const-string v1, "enable_miui_ime_bottom_view"
final String v2 = "InputMethodHelper"; // const-string v2, "InputMethodHelper"
if ( v0 != null) { // if-eqz v0, :cond_2
v0 = android.inputmethodservice.MiuiBottomConfig.sBigChinDevices;
v3 = android.os.Build.DEVICE;
v0 = (( java.util.HashSet ) v0 ).contains ( v3 ); // invoke-virtual {v0, v3}, Ljava/util/HashSet;->contains(Ljava/lang/Object;)Z
/* if-nez v0, :cond_2 */
/* sget-boolean v0, Lmiui/os/Build;->IS_INTERNATIONAL_BUILD:Z */
if ( v0 != null) { // if-eqz v0, :cond_1
	 /* .line 101 */
} // :cond_1
(( android.content.Context ) p0 ).getContentResolver ( ); // invoke-virtual {p0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;
int v3 = 1; // const/4 v3, 0x1
android.provider.Settings$Secure .putIntForUser ( v0,v1,v3,p1 );
/* .line 102 */
/* new-instance v0, Ljava/lang/StringBuilder; */
/* invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V */
final String v1 = "enable miui bottom "; // const-string v1, "enable miui bottom "
(( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v0 ).append ( p1 ); // invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v0 ).toString ( ); // invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
android.util.Log .d ( v2,v0 );
/* .line 98 */
} // :cond_2
} // :goto_0
/* new-instance v0, Ljava/lang/StringBuilder; */
/* invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V */
final String v3 = "disable miui bottom for user "; // const-string v3, "disable miui bottom for user "
(( java.lang.StringBuilder ) v0 ).append ( v3 ); // invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v0 ).append ( p1 ); // invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v0 ).toString ( ); // invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
android.util.Log .d ( v2,v0 );
/* .line 99 */
(( android.content.Context ) p0 ).getContentResolver ( ); // invoke-virtual {p0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;
int v2 = 0; // const/4 v2, 0x0
android.provider.Settings$Secure .putIntForUser ( v0,v1,v2,p1 );
/* .line 104 */
} // :goto_1
return;
} // .end method
private static void initInputMethodTbCmdRule ( android.content.Context p0 ) {
/* .locals 5 */
/* .param p0, "context" # Landroid/content/Context; */
/* .line 133 */
(( android.content.Context ) p0 ).getContentResolver ( ); // invoke-virtual {p0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;
final String v1 = "EnableTbCmd"; // const-string v1, "EnableTbCmd"
int v2 = 1; // const/4 v2, 0x1
final String v3 = "InputMethodTaobaoCmdModule"; // const-string v3, "InputMethodTaobaoCmdModule"
v0 = android.provider.MiuiSettings$SettingsCloudData .getCloudDataBoolean ( v0,v3,v1,v2 );
/* .line 135 */
/* .local v0, "isEnable":Z */
(( android.content.Context ) p0 ).getContentResolver ( ); // invoke-virtual {p0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;
final String v2 = "input_method_taobao_cmd_enable"; // const-string v2, "input_method_taobao_cmd_enable"
android.provider.Settings$Global .putInt ( v1,v2,v0 );
/* .line 137 */
(( android.content.Context ) p0 ).getContentResolver ( ); // invoke-virtual {p0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;
final String v2 = "TbCmdRule"; // const-string v2, "TbCmdRule"
final String v4 = "[\uffe5\u20ac$\u00a2\u20b3\u20a4\u20b4][a-zA-Z0-9]+[\uffe5\u20ac$\u00a2\u20b3\u20a4\u20b4]"; // const-string v4, "[\uffe5\u20ac$\u00a2\u20b3\u20a4\u20b4][a-zA-Z0-9]+[\uffe5\u20ac$\u00a2\u20b3\u20a4\u20b4]"
android.provider.MiuiSettings$SettingsCloudData .getCloudDataString ( v1,v3,v2,v4 );
/* .line 139 */
/* .local v1, "tbCmdRule":Ljava/lang/String; */
(( android.content.Context ) p0 ).getContentResolver ( ); // invoke-virtual {p0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;
final String v3 = "input_method_taobao_cmd_rule"; // const-string v3, "input_method_taobao_cmd_rule"
android.provider.Settings$Global .putString ( v2,v3,v1 );
/* .line 140 */
return;
} // .end method
private static Boolean isFullScreenDevice ( ) {
/* .locals 4 */
/* .line 81 */
int v0 = 0; // const/4 v0, 0x0
try { // :try_start_0
v0 = miui.util.CompatibilityHelper .hasNavigationBar ( v0 );
/* :try_end_0 */
/* .catch Landroid/os/RemoteException; {:try_start_0 ..:try_end_0} :catch_0 */
/* .line 82 */
/* :catch_0 */
/* move-exception v1 */
/* .line 83 */
/* .local v1, "e":Landroid/os/RemoteException; */
final String v2 = "InputMethodHelper"; // const-string v2, "InputMethodHelper"
final String v3 = "get isFullScreenDevice error"; // const-string v3, "get isFullScreenDevice error"
android.util.Log .e ( v2,v3,v1 );
/* .line 85 */
} // .end local v1 # "e":Landroid/os/RemoteException;
} // .end method
private static Boolean isMiuiBottomNeedSet ( android.content.Context p0, Integer p1 ) {
/* .locals 3 */
/* .param p0, "context" # Landroid/content/Context; */
/* .param p1, "userId" # I */
/* .line 89 */
(( android.content.Context ) p0 ).getContentResolver ( ); // invoke-virtual {p0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;
final String v1 = "enable_miui_ime_bottom_view"; // const-string v1, "enable_miui_ime_bottom_view"
int v2 = -1; // const/4 v2, -0x1
v0 = android.provider.Settings$Secure .getIntForUser ( v0,v1,v2,p1 );
/* if-ne v0, v2, :cond_0 */
int v0 = 1; // const/4 v0, 0x1
} // :cond_0
int v0 = 0; // const/4 v0, 0x0
} // :goto_0
} // .end method
private static Boolean isSupportMiuiBottom ( ) {
/* .locals 3 */
/* .line 107 */
final String v0 = "ro.miui.support_miui_ime_bottom"; // const-string v0, "ro.miui.support_miui_ime_bottom"
int v1 = 0; // const/4 v1, 0x0
v0 = android.os.SystemProperties .getInt ( v0,v1 );
int v2 = 1; // const/4 v2, 0x1
/* if-ne v0, v2, :cond_0 */
/* move v1, v2 */
} // :cond_0
} // .end method
private static void registerContentObserver ( android.content.Context p0 ) {
/* .locals 4 */
/* .param p0, "context" # Landroid/content/Context; */
/* .line 111 */
/* new-instance v0, Lcom/miui/server/InputMethodHelper$InputMethodTbCmdRuleObserver; */
com.android.internal.os.BackgroundThread .getHandler ( );
/* invoke-direct {v0, v1, p0}, Lcom/miui/server/InputMethodHelper$InputMethodTbCmdRuleObserver;-><init>(Landroid/os/Handler;Landroid/content/Context;)V */
/* .line 112 */
/* .local v0, "tbCmdRuleObserver":Lcom/miui/server/InputMethodHelper$InputMethodTbCmdRuleObserver; */
(( android.content.Context ) p0 ).getContentResolver ( ); // invoke-virtual {p0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;
v2 = com.miui.server.InputMethodHelper.URI_CLOUD_ALL_DATA_NOTIFY;
int v3 = 1; // const/4 v3, 0x1
(( android.content.ContentResolver ) v1 ).registerContentObserver ( v2, v3, v0 ); // invoke-virtual {v1, v2, v3, v0}, Landroid/content/ContentResolver;->registerContentObserver(Landroid/net/Uri;ZLandroid/database/ContentObserver;)V
/* .line 114 */
return;
} // .end method
