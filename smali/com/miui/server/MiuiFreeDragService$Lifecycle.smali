.class public final Lcom/miui/server/MiuiFreeDragService$Lifecycle;
.super Lcom/android/server/SystemService;
.source "MiuiFreeDragService.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/miui/server/MiuiFreeDragService;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "Lifecycle"
.end annotation


# instance fields
.field private final mService:Lcom/miui/server/MiuiFreeDragService;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 1
    .param p1, "context"    # Landroid/content/Context;

    .line 29
    invoke-direct {p0, p1}, Lcom/android/server/SystemService;-><init>(Landroid/content/Context;)V

    .line 30
    invoke-static {}, Lcom/miui/server/MiuiFreeDragService;->getInstance()Lcom/miui/server/MiuiFreeDragService;

    move-result-object v0

    iput-object v0, p0, Lcom/miui/server/MiuiFreeDragService$Lifecycle;->mService:Lcom/miui/server/MiuiFreeDragService;

    .line 31
    return-void
.end method


# virtual methods
.method public onStart()V
    .locals 2

    .line 35
    const-string v0, "MiuiFreeDragService"

    iget-object v1, p0, Lcom/miui/server/MiuiFreeDragService$Lifecycle;->mService:Lcom/miui/server/MiuiFreeDragService;

    invoke-virtual {p0, v0, v1}, Lcom/miui/server/MiuiFreeDragService$Lifecycle;->publishBinderService(Ljava/lang/String;Landroid/os/IBinder;)V

    .line 36
    return-void
.end method
