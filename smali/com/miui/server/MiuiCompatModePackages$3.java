class com.miui.server.MiuiCompatModePackages$3 extends android.content.BroadcastReceiver {
	 /* .source "MiuiCompatModePackages.java" */
	 /* # annotations */
	 /* .annotation system Ldalvik/annotation/EnclosingClass; */
	 /* value = Lcom/miui/server/MiuiCompatModePackages; */
} // .end annotation
/* .annotation system Ldalvik/annotation/InnerClass; */
/* accessFlags = 0x0 */
/* name = null */
} // .end annotation
/* # instance fields */
final com.miui.server.MiuiCompatModePackages this$0; //synthetic
/* # direct methods */
 com.miui.server.MiuiCompatModePackages$3 ( ) {
/* .locals 0 */
/* .param p1, "this$0" # Lcom/miui/server/MiuiCompatModePackages; */
/* .line 437 */
this.this$0 = p1;
/* invoke-direct {p0}, Landroid/content/BroadcastReceiver;-><init>()V */
return;
} // .end method
/* # virtual methods */
public void onReceive ( android.content.Context p0, android.content.Intent p1 ) {
/* .locals 5 */
/* .param p1, "context" # Landroid/content/Context; */
/* .param p2, "intent" # Landroid/content/Intent; */
/* .line 440 */
(( android.content.Intent ) p2 ).getAction ( ); // invoke-virtual {p2}, Landroid/content/Intent;->getAction()Ljava/lang/String;
/* .line 441 */
/* .local v0, "action":Ljava/lang/String; */
(( android.content.Intent ) p2 ).getData ( ); // invoke-virtual {p2}, Landroid/content/Intent;->getData()Landroid/net/Uri;
/* .line 442 */
/* .local v1, "data":Landroid/net/Uri; */
if ( v1 != null) { // if-eqz v1, :cond_1
	 /* .line 443 */
	 (( android.net.Uri ) v1 ).getSchemeSpecificPart ( ); // invoke-virtual {v1}, Landroid/net/Uri;->getSchemeSpecificPart()Ljava/lang/String;
	 /* .line 444 */
	 /* .local v2, "pkg":Ljava/lang/String; */
	 if ( v2 != null) { // if-eqz v2, :cond_1
		 /* .line 445 */
		 final String v3 = "android.intent.action.PACKAGE_ADDED"; // const-string v3, "android.intent.action.PACKAGE_ADDED"
		 v3 = 		 (( java.lang.String ) v3 ).equals ( v0 ); // invoke-virtual {v3, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z
		 if ( v3 != null) { // if-eqz v3, :cond_0
			 /* .line 446 */
			 v3 = this.this$0;
			 com.miui.server.MiuiCompatModePackages .-$$Nest$mhandleUpdatePackage ( v3,v2 );
			 /* .line 447 */
		 } // :cond_0
		 final String v3 = "android.intent.action.PACKAGE_REMOVED"; // const-string v3, "android.intent.action.PACKAGE_REMOVED"
		 v3 = 		 (( java.lang.String ) v3 ).equals ( v0 ); // invoke-virtual {v3, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z
		 if ( v3 != null) { // if-eqz v3, :cond_1
			 /* .line 448 */
			 final String v3 = "android.intent.extra.REPLACING"; // const-string v3, "android.intent.extra.REPLACING"
			 int v4 = 0; // const/4 v4, 0x0
			 v3 = 			 (( android.content.Intent ) p2 ).getBooleanExtra ( v3, v4 ); // invoke-virtual {p2, v3, v4}, Landroid/content/Intent;->getBooleanExtra(Ljava/lang/String;Z)Z
			 /* if-nez v3, :cond_1 */
			 /* .line 449 */
			 v3 = this.this$0;
			 com.miui.server.MiuiCompatModePackages .-$$Nest$mhandleRemovePackage ( v3,v2 );
			 /* .line 453 */
		 } // .end local v2 # "pkg":Ljava/lang/String;
	 } // :cond_1
} // :goto_0
return;
} // .end method
