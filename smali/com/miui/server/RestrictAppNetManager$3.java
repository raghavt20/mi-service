class com.miui.server.RestrictAppNetManager$3 extends android.content.BroadcastReceiver {
	 /* .source "RestrictAppNetManager.java" */
	 /* # annotations */
	 /* .annotation system Ldalvik/annotation/EnclosingClass; */
	 /* value = Lcom/miui/server/RestrictAppNetManager; */
} // .end annotation
/* .annotation system Ldalvik/annotation/InnerClass; */
/* accessFlags = 0x0 */
/* name = null */
} // .end annotation
/* # direct methods */
 com.miui.server.RestrictAppNetManager$3 ( ) {
/* .locals 0 */
/* .line 204 */
/* invoke-direct {p0}, Landroid/content/BroadcastReceiver;-><init>()V */
return;
} // .end method
/* # virtual methods */
public void onReceive ( android.content.Context p0, android.content.Intent p1 ) {
/* .locals 7 */
/* .param p1, "context" # Landroid/content/Context; */
/* .param p2, "intent" # Landroid/content/Intent; */
/* .line 207 */
(( android.content.Intent ) p2 ).getData ( ); // invoke-virtual {p2}, Landroid/content/Intent;->getData()Landroid/net/Uri;
/* .line 208 */
/* .local v0, "uri":Landroid/net/Uri; */
if ( v0 != null) { // if-eqz v0, :cond_0
	 (( android.net.Uri ) v0 ).getSchemeSpecificPart ( ); // invoke-virtual {v0}, Landroid/net/Uri;->getSchemeSpecificPart()Ljava/lang/String;
} // :cond_0
int v1 = 0; // const/4 v1, 0x0
/* .line 209 */
/* .local v1, "packageName":Ljava/lang/String; */
} // :goto_0
final String v2 = "android.intent.extra.UID"; // const-string v2, "android.intent.extra.UID"
int v3 = 0; // const/4 v3, 0x0
v2 = (( android.content.Intent ) p2 ).getIntExtra ( v2, v3 ); // invoke-virtual {p2, v2, v3}, Landroid/content/Intent;->getIntExtra(Ljava/lang/String;I)I
/* .line 210 */
/* .local v2, "uid":I */
final String v4 = "android.intent.extra.REPLACING"; // const-string v4, "android.intent.extra.REPLACING"
v3 = (( android.content.Intent ) p2 ).getBooleanExtra ( v4, v3 ); // invoke-virtual {p2, v4, v3}, Landroid/content/Intent;->getBooleanExtra(Ljava/lang/String;Z)Z
/* .line 212 */
/* .local v3, "replacing":Z */
com.miui.server.RestrictAppNetManager .-$$Nest$sfgetsService ( );
if ( v4 != null) { // if-eqz v4, :cond_3
v4 = android.text.TextUtils .isEmpty ( v1 );
/* if-nez v4, :cond_3 */
/* if-lez v2, :cond_3 */
if ( v3 != null) { // if-eqz v3, :cond_1
	 /* .line 216 */
} // :cond_1
v4 = com.miui.server.RestrictAppNetManager .-$$Nest$smisAllowAccessInternet ( v1 );
/* if-nez v4, :cond_2 */
/* .line 217 */
com.miui.server.RestrictAppNetManager .-$$Nest$smtryDownloadCloudData ( p1 );
/* .line 218 */
/* new-instance v4, Ljava/lang/StringBuilder; */
/* invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V */
final String v5 = "RULE_RESTRICT packageName: "; // const-string v5, "RULE_RESTRICT packageName: "
(( java.lang.StringBuilder ) v4 ).append ( v5 ); // invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v4 ).append ( v1 ); // invoke-virtual {v4, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v4 ).toString ( ); // invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
final String v5 = "RestrictAppNetManager"; // const-string v5, "RestrictAppNetManager"
android.util.Log .i ( v5,v4 );
/* .line 219 */
com.miui.server.RestrictAppNetManager .-$$Nest$sfgetsService ( );
int v5 = 1; // const/4 v5, 0x1
int v6 = 3; // const/4 v6, 0x3
/* .line 221 */
} // :cond_2
return;
/* .line 213 */
} // :cond_3
} // :goto_1
return;
} // .end method
