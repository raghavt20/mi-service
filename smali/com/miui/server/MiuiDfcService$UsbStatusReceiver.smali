.class public Lcom/miui/server/MiuiDfcService$UsbStatusReceiver;
.super Landroid/content/BroadcastReceiver;
.source "MiuiDfcService.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/miui/server/MiuiDfcService;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x1
    name = "UsbStatusReceiver"
.end annotation


# instance fields
.field final synthetic this$0:Lcom/miui/server/MiuiDfcService;


# direct methods
.method public constructor <init>(Lcom/miui/server/MiuiDfcService;)V
    .locals 0
    .param p1, "this$0"    # Lcom/miui/server/MiuiDfcService;

    .line 281
    iput-object p1, p0, Lcom/miui/server/MiuiDfcService$UsbStatusReceiver;->this$0:Lcom/miui/server/MiuiDfcService;

    invoke-direct {p0}, Landroid/content/BroadcastReceiver;-><init>()V

    return-void
.end method


# virtual methods
.method public onReceive(Landroid/content/Context;Landroid/content/Intent;)V
    .locals 8
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "intent"    # Landroid/content/Intent;

    .line 285
    const-string/jumbo v0, "sys.dfcservice.ctrl"

    const-string/jumbo v1, "sys.dfcservice.is_front_scene"

    const/4 v2, 0x0

    invoke-static {v1, v2}, Landroid/os/SystemProperties;->getInt(Ljava/lang/String;I)I

    move-result v1

    .line 287
    .local v1, "isScene":I
    const-string v3, "MiuiDfcService"

    if-eqz p2, :cond_4

    sget-boolean v4, Lcom/miui/server/MiuiDfcService;->DFC_DEBUG:Z

    if-nez v4, :cond_4

    const/4 v4, 0x1

    if-ne v1, v4, :cond_0

    goto :goto_2

    .line 291
    :cond_0
    invoke-virtual {p2}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v5

    .line 292
    .local v5, "action":Ljava/lang/String;
    const-string v6, "android.hardware.usb.action.USB_STATE"

    invoke-virtual {v6, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-eqz v6, :cond_3

    .line 293
    invoke-virtual {p2}, Landroid/content/Intent;->getExtras()Landroid/os/Bundle;

    move-result-object v6

    const-string v7, "connected"

    invoke-virtual {v6, v7}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;)Z

    move-result v6

    .line 294
    .local v6, "connected":Z
    if-eqz v6, :cond_2

    .line 297
    :try_start_0
    invoke-static {v0, v2}, Landroid/os/SystemProperties;->getBoolean(Ljava/lang/String;Z)Z

    move-result v2

    if-eqz v2, :cond_1

    .line 298
    const-string v2, "false"

    invoke-static {v0, v2}, Landroid/os/SystemProperties;->set(Ljava/lang/String;Ljava/lang/String;)V
    :try_end_0
    .catch Ljava/lang/RuntimeException; {:try_start_0 .. :try_end_0} :catch_0

    .line 302
    :cond_1
    goto :goto_0

    .line 300
    :catch_0
    move-exception v0

    .line 301
    .local v0, "e":Ljava/lang/RuntimeException;
    const-string v2, "Failed to set sys.dfcservice.ctrl property"

    invoke-static {v3, v2, v0}, Landroid/util/Slog;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    .line 304
    .end local v0    # "e":Ljava/lang/RuntimeException;
    :goto_0
    iget-object v0, p0, Lcom/miui/server/MiuiDfcService$UsbStatusReceiver;->this$0:Lcom/miui/server/MiuiDfcService;

    invoke-static {v0, v4}, Lcom/miui/server/MiuiDfcService;->-$$Nest$fputmUsbConnect(Lcom/miui/server/MiuiDfcService;Z)V

    .line 305
    const-string v0, "Usb connect, shutdown DfcNativeService"

    invoke-static {v3, v0}, Landroid/util/Slog;->d(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_1

    .line 307
    :cond_2
    iget-object v0, p0, Lcom/miui/server/MiuiDfcService$UsbStatusReceiver;->this$0:Lcom/miui/server/MiuiDfcService;

    invoke-static {v0, v2}, Lcom/miui/server/MiuiDfcService;->-$$Nest$fputmUsbConnect(Lcom/miui/server/MiuiDfcService;Z)V

    .line 308
    const-string v0, "Usb disconnect"

    invoke-static {v3, v0}, Landroid/util/Slog;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 311
    .end local v6    # "connected":Z
    :cond_3
    :goto_1
    return-void

    .line 288
    .end local v5    # "action":Ljava/lang/String;
    :cond_4
    :goto_2
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "cleanmaster isScene: "

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v3, v0}, Landroid/util/Slog;->w(Ljava/lang/String;Ljava/lang/String;)I

    .line 289
    return-void
.end method
