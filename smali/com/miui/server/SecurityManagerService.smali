.class public Lcom/miui/server/SecurityManagerService;
.super Lmiui/security/ISecurityManager$Stub;
.source "SecurityManagerService.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/miui/server/SecurityManagerService$LocalService;,
        Lcom/miui/server/SecurityManagerService$MyPackageObserver;,
        Lcom/miui/server/SecurityManagerService$Lifecycle;
    }
.end annotation


# static fields
.field private static final PACKAGE_PERMISSIONCENTER:Ljava/lang/String; = "com.lbe.security.miui"

.field private static final PACKAGE_SECURITYCENTER:Ljava/lang/String; = "com.miui.securitycenter"

.field private static final PLATFORM_VAID_PERMISSION:Ljava/lang/String; = "com.miui.securitycenter.permission.SYSTEM_PERMISSION_DECLARE"

.field private static final READ_AND_WRITE_PERMISSION_MANAGER:Ljava/lang/String; = "miui.permission.READ_AND_WIRTE_PERMISSION_MANAGER"

.field private static final TAG:Ljava/lang/String; = "SecurityManagerService"

.field private static final UPDATE_VERSION:Ljava/lang/String; = "1.0"

.field private static final VAID_PLATFORM_CACHE_PATH:Ljava/lang/String; = "/data/system/vaid_persistence_platform"

.field private static final WRITE_SETTINGS_DELAY:I = 0x3e8


# instance fields
.field public final mAccessControlImpl:Lcom/miui/server/security/AccessControlImpl;

.field public final mAccessController:Lcom/miui/server/AccessController;

.field private final mAppBehavior:Lcom/miui/server/security/AppBehaviorService;

.field private final mAppDuration:Lcom/miui/server/security/AppDurationService;

.field private mAppOpsInternal:Landroid/app/AppOpsManagerInternal;

.field private final mAppRunningControlService:Lcom/miui/server/AppRunningControlService;

.field public final mContext:Landroid/content/Context;

.field public final mDefaultBrowserImpl:Lcom/miui/server/security/DefaultBrowserImpl;

.field public final mGameBoosterImpl:Lcom/miui/server/security/GameBoosterImpl;

.field private mINotificationManager:Landroid/app/INotificationManager;

.field private final mIncompatibleAppList:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private final mInternal:Lmiui/security/SecurityManagerInternal;

.field private mIsUpdated:Z

.field private final mPermissionManagerService:Lcom/android/server/pm/permission/PermissionManagerService;

.field private mPlatformVAID:Ljava/lang/String;

.field private final mPrivacyDisplayNameList:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private final mPrivacyVirtualDisplay:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private final mSecuritySmsHandler:Lcom/miui/server/SecuritySmsHandler;

.field public final mSecurityWriteHandler:Lcom/miui/server/security/SecurityWriteHandler;

.field public final mSettingsFile:Landroid/util/AtomicFile;

.field public mSettingsObserver:Lcom/miui/server/security/SecuritySettingsObserver;

.field private final mUserManager:Lcom/android/server/pm/UserManagerService;

.field public final mUserStateLock:Ljava/lang/Object;

.field public final mUserStates:Landroid/util/SparseArray;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/util/SparseArray<",
            "Lcom/miui/server/security/SecurityUserState;",
            ">;"
        }
    .end annotation
.end field

.field public final mWakeUpTimeImpl:Lcom/miui/server/security/WakeUpTimeImpl;

.field private restrictChainMaps:Ljava/util/LinkedHashMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/LinkedHashMap<",
            "Ljava/lang/String;",
            "Ljava/util/List<",
            "Ljava/lang/String;",
            ">;>;"
        }
    .end annotation
.end field


# direct methods
.method public static synthetic $r8$lambda$5PV2H4L575PgKDbw0e_5GqthUE4(Lcom/miui/server/SecurityManagerService;Landroid/util/SparseArray;)V
    .locals 0

    invoke-direct {p0, p1}, Lcom/miui/server/SecurityManagerService;->lambda$updateAppWidgetVisibility$2(Landroid/util/SparseArray;)V

    return-void
.end method

.method public static synthetic $r8$lambda$JNP6yvVBnt3ZJqYOH3T_E3zOFZ8(Lcom/miui/server/SecurityManagerService;)V
    .locals 0

    invoke-direct {p0}, Lcom/miui/server/SecurityManagerService;->loadData()V

    return-void
.end method

.method public static synthetic $r8$lambda$Vzg6p4kbeB31-KLh3j-mYWN2PuE(Lcom/miui/server/SecurityManagerService;Ljava/lang/String;Z)Ljava/lang/Boolean;
    .locals 0

    invoke-direct {p0, p1, p2}, Lcom/miui/server/SecurityManagerService;->lambda$setAppPrivacyStatus$0(Ljava/lang/String;Z)Ljava/lang/Boolean;

    move-result-object p0

    return-object p0
.end method

.method public static synthetic $r8$lambda$lbo49f9JMJiPPzJmiMaNELWyWuU(Lcom/miui/server/SecurityManagerService;Ljava/lang/String;)Ljava/lang/Boolean;
    .locals 0

    invoke-direct {p0, p1}, Lcom/miui/server/SecurityManagerService;->lambda$isAppPrivacyEnabled$1(Ljava/lang/String;)Ljava/lang/Boolean;

    move-result-object p0

    return-object p0
.end method

.method static bridge synthetic -$$Nest$fgetmAppBehavior(Lcom/miui/server/SecurityManagerService;)Lcom/miui/server/security/AppBehaviorService;
    .locals 0

    iget-object p0, p0, Lcom/miui/server/SecurityManagerService;->mAppBehavior:Lcom/miui/server/security/AppBehaviorService;

    return-object p0
.end method

.method static bridge synthetic -$$Nest$fgetmAppDuration(Lcom/miui/server/SecurityManagerService;)Lcom/miui/server/security/AppDurationService;
    .locals 0

    iget-object p0, p0, Lcom/miui/server/SecurityManagerService;->mAppDuration:Lcom/miui/server/security/AppDurationService;

    return-object p0
.end method

.method static bridge synthetic -$$Nest$fgetmAppRunningControlService(Lcom/miui/server/SecurityManagerService;)Lcom/miui/server/AppRunningControlService;
    .locals 0

    iget-object p0, p0, Lcom/miui/server/SecurityManagerService;->mAppRunningControlService:Lcom/miui/server/AppRunningControlService;

    return-object p0
.end method

.method static bridge synthetic -$$Nest$fgetmPrivacyDisplayNameList(Lcom/miui/server/SecurityManagerService;)Ljava/util/List;
    .locals 0

    iget-object p0, p0, Lcom/miui/server/SecurityManagerService;->mPrivacyDisplayNameList:Ljava/util/List;

    return-object p0
.end method

.method static bridge synthetic -$$Nest$fgetmPrivacyVirtualDisplay(Lcom/miui/server/SecurityManagerService;)Ljava/util/List;
    .locals 0

    iget-object p0, p0, Lcom/miui/server/SecurityManagerService;->mPrivacyVirtualDisplay:Ljava/util/List;

    return-object p0
.end method

.method static bridge synthetic -$$Nest$fgetmUserManager(Lcom/miui/server/SecurityManagerService;)Lcom/android/server/pm/UserManagerService;
    .locals 0

    iget-object p0, p0, Lcom/miui/server/SecurityManagerService;->mUserManager:Lcom/android/server/pm/UserManagerService;

    return-object p0
.end method

.method static bridge synthetic -$$Nest$minitWhenBootCompleted(Lcom/miui/server/SecurityManagerService;)V
    .locals 0

    invoke-direct {p0}, Lcom/miui/server/SecurityManagerService;->initWhenBootCompleted()V

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;)V
    .locals 4
    .param p1, "context"    # Landroid/content/Context;

    .line 184
    invoke-direct {p0}, Lmiui/security/ISecurityManager$Stub;-><init>()V

    .line 116
    new-instance v0, Landroid/util/AtomicFile;

    new-instance v1, Ljava/io/File;

    invoke-static {}, Landroid/os/Environment;->getDataSystemDirectory()Ljava/io/File;

    move-result-object v2

    const-string v3, "miui-packages.xml"

    invoke-direct {v1, v2, v3}, Ljava/io/File;-><init>(Ljava/io/File;Ljava/lang/String;)V

    invoke-direct {v0, v1}, Landroid/util/AtomicFile;-><init>(Ljava/io/File;)V

    iput-object v0, p0, Lcom/miui/server/SecurityManagerService;->mSettingsFile:Landroid/util/AtomicFile;

    .line 136
    new-instance v0, Ljava/lang/Object;

    invoke-direct {v0}, Ljava/lang/Object;-><init>()V

    iput-object v0, p0, Lcom/miui/server/SecurityManagerService;->mUserStateLock:Ljava/lang/Object;

    .line 138
    new-instance v0, Landroid/util/SparseArray;

    const/4 v1, 0x3

    invoke-direct {v0, v1}, Landroid/util/SparseArray;-><init>(I)V

    iput-object v0, p0, Lcom/miui/server/SecurityManagerService;->mUserStates:Landroid/util/SparseArray;

    .line 139
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/miui/server/SecurityManagerService;->mIncompatibleAppList:Ljava/util/ArrayList;

    .line 140
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/miui/server/SecurityManagerService;->mPrivacyVirtualDisplay:Ljava/util/List;

    .line 141
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/miui/server/SecurityManagerService;->mPrivacyDisplayNameList:Ljava/util/List;

    .line 143
    new-instance v0, Ljava/util/LinkedHashMap;

    invoke-direct {v0}, Ljava/util/LinkedHashMap;-><init>()V

    iput-object v0, p0, Lcom/miui/server/SecurityManagerService;->restrictChainMaps:Ljava/util/LinkedHashMap;

    .line 185
    iput-object p1, p0, Lcom/miui/server/SecurityManagerService;->mContext:Landroid/content/Context;

    .line 186
    invoke-static {}, Lcom/android/server/pm/UserManagerService;->getInstance()Lcom/android/server/pm/UserManagerService;

    move-result-object v0

    iput-object v0, p0, Lcom/miui/server/SecurityManagerService;->mUserManager:Lcom/android/server/pm/UserManagerService;

    .line 187
    const-string v0, "permissionmgr"

    invoke-static {v0}, Landroid/os/ServiceManager;->getService(Ljava/lang/String;)Landroid/os/IBinder;

    move-result-object v0

    check-cast v0, Lcom/android/server/pm/permission/PermissionManagerService;

    iput-object v0, p0, Lcom/miui/server/SecurityManagerService;->mPermissionManagerService:Lcom/android/server/pm/permission/PermissionManagerService;

    .line 188
    new-instance v0, Landroid/os/HandlerThread;

    const-string v1, "SecurityHandlerThread"

    invoke-direct {v0, v1}, Landroid/os/HandlerThread;-><init>(Ljava/lang/String;)V

    .line 189
    .local v0, "handlerThread":Landroid/os/HandlerThread;
    invoke-virtual {v0}, Landroid/os/HandlerThread;->start()V

    .line 190
    new-instance v1, Lcom/miui/server/security/SecurityWriteHandler;

    invoke-virtual {v0}, Landroid/os/HandlerThread;->getLooper()Landroid/os/Looper;

    move-result-object v2

    invoke-direct {v1, p0, v2}, Lcom/miui/server/security/SecurityWriteHandler;-><init>(Lcom/miui/server/SecurityManagerService;Landroid/os/Looper;)V

    iput-object v1, p0, Lcom/miui/server/SecurityManagerService;->mSecurityWriteHandler:Lcom/miui/server/security/SecurityWriteHandler;

    .line 191
    new-instance v2, Lcom/miui/server/SecuritySmsHandler;

    invoke-direct {v2, p1, v1}, Lcom/miui/server/SecuritySmsHandler;-><init>(Landroid/content/Context;Landroid/os/Handler;)V

    iput-object v2, p0, Lcom/miui/server/SecurityManagerService;->mSecuritySmsHandler:Lcom/miui/server/SecuritySmsHandler;

    .line 192
    new-instance v2, Lcom/miui/server/AccessController;

    invoke-virtual {v0}, Landroid/os/HandlerThread;->getLooper()Landroid/os/Looper;

    move-result-object v3

    invoke-direct {v2, p1, v3}, Lcom/miui/server/AccessController;-><init>(Landroid/content/Context;Landroid/os/Looper;)V

    iput-object v2, p0, Lcom/miui/server/SecurityManagerService;->mAccessController:Lcom/miui/server/AccessController;

    .line 193
    new-instance v2, Lcom/miui/server/security/AccessControlImpl;

    invoke-direct {v2, p0}, Lcom/miui/server/security/AccessControlImpl;-><init>(Lcom/miui/server/SecurityManagerService;)V

    iput-object v2, p0, Lcom/miui/server/SecurityManagerService;->mAccessControlImpl:Lcom/miui/server/security/AccessControlImpl;

    .line 194
    new-instance v2, Lcom/miui/server/security/WakeUpTimeImpl;

    invoke-direct {v2, p0}, Lcom/miui/server/security/WakeUpTimeImpl;-><init>(Lcom/miui/server/SecurityManagerService;)V

    iput-object v2, p0, Lcom/miui/server/SecurityManagerService;->mWakeUpTimeImpl:Lcom/miui/server/security/WakeUpTimeImpl;

    .line 195
    new-instance v2, Lcom/miui/server/security/GameBoosterImpl;

    invoke-direct {v2, p0}, Lcom/miui/server/security/GameBoosterImpl;-><init>(Lcom/miui/server/SecurityManagerService;)V

    iput-object v2, p0, Lcom/miui/server/SecurityManagerService;->mGameBoosterImpl:Lcom/miui/server/security/GameBoosterImpl;

    .line 196
    new-instance v2, Lcom/miui/server/security/DefaultBrowserImpl;

    invoke-direct {v2, p0}, Lcom/miui/server/security/DefaultBrowserImpl;-><init>(Lcom/miui/server/SecurityManagerService;)V

    iput-object v2, p0, Lcom/miui/server/SecurityManagerService;->mDefaultBrowserImpl:Lcom/miui/server/security/DefaultBrowserImpl;

    .line 197
    new-instance v2, Lcom/miui/server/AppRunningControlService;

    invoke-direct {v2, p1}, Lcom/miui/server/AppRunningControlService;-><init>(Landroid/content/Context;)V

    iput-object v2, p0, Lcom/miui/server/SecurityManagerService;->mAppRunningControlService:Lcom/miui/server/AppRunningControlService;

    .line 198
    new-instance v2, Lcom/miui/server/security/SecuritySettingsObserver;

    invoke-direct {v2, p0, v1}, Lcom/miui/server/security/SecuritySettingsObserver;-><init>(Lcom/miui/server/SecurityManagerService;Landroid/os/Handler;)V

    iput-object v2, p0, Lcom/miui/server/SecurityManagerService;->mSettingsObserver:Lcom/miui/server/security/SecuritySettingsObserver;

    .line 199
    new-instance v2, Lcom/miui/server/security/AppBehaviorService;

    invoke-direct {v2, p1, v0}, Lcom/miui/server/security/AppBehaviorService;-><init>(Landroid/content/Context;Landroid/os/HandlerThread;)V

    iput-object v2, p0, Lcom/miui/server/SecurityManagerService;->mAppBehavior:Lcom/miui/server/security/AppBehaviorService;

    .line 200
    new-instance v3, Lcom/miui/server/security/AppDurationService;

    invoke-direct {v3, v2}, Lcom/miui/server/security/AppDurationService;-><init>(Lcom/miui/server/security/AppBehaviorService;)V

    iput-object v3, p0, Lcom/miui/server/SecurityManagerService;->mAppDuration:Lcom/miui/server/security/AppDurationService;

    .line 201
    invoke-virtual {v2, v3}, Lcom/miui/server/security/AppBehaviorService;->setAppBehaviorDuration(Lcom/miui/server/security/AppDurationService;)V

    .line 202
    new-instance v2, Lcom/miui/server/SecurityManagerService$LocalService;

    const/4 v3, 0x0

    invoke-direct {v2, p0, v3}, Lcom/miui/server/SecurityManagerService$LocalService;-><init>(Lcom/miui/server/SecurityManagerService;Lcom/miui/server/SecurityManagerService$LocalService-IA;)V

    iput-object v2, p0, Lcom/miui/server/SecurityManagerService;->mInternal:Lmiui/security/SecurityManagerInternal;

    .line 203
    const-class v3, Lmiui/security/SecurityManagerInternal;

    invoke-static {v3, v2}, Lcom/android/server/LocalServices;->addService(Ljava/lang/Class;Ljava/lang/Object;)V

    .line 204
    new-instance v2, Lcom/miui/server/SecurityManagerService$$ExternalSyntheticLambda1;

    invoke-direct {v2, p0}, Lcom/miui/server/SecurityManagerService$$ExternalSyntheticLambda1;-><init>(Lcom/miui/server/SecurityManagerService;)V

    invoke-virtual {v1, v2}, Lcom/miui/server/security/SecurityWriteHandler;->post(Ljava/lang/Runnable;)Z

    .line 205
    return-void
.end method

.method private checkBlurLocationPermission()V
    .locals 4

    .line 966
    invoke-static {}, Landroid/os/Binder;->getCallingUid()I

    move-result v0

    .line 967
    .local v0, "callingUid":I
    invoke-static {v0}, Landroid/os/UserHandle;->getAppId(I)I

    move-result v1

    const/16 v2, 0x2710

    if-ge v1, v2, :cond_0

    .line 970
    return-void

    .line 968
    :cond_0
    new-instance v1, Ljava/lang/SecurityException;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "Uid "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, " can\'t get blur location info"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-direct {v1, v2}, Ljava/lang/SecurityException;-><init>(Ljava/lang/String;)V

    throw v1
.end method

.method private checkGrantPermissionPkg(Ljava/lang/String;)V
    .locals 4
    .param p1, "allowPackage"    # Ljava/lang/String;

    .line 829
    invoke-static {}, Landroid/os/Binder;->getCallingPid()I

    move-result v0

    invoke-static {v0}, Lcom/android/server/am/ProcessUtils;->getPackageNameByPid(I)Ljava/lang/String;

    move-result-object v0

    .line 830
    .local v0, "callingPackageName":Ljava/lang/String;
    invoke-virtual {p1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 834
    return-void

    .line 831
    :cond_0
    new-instance v1, Ljava/lang/SecurityException;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "Permission Denial: attempt to grant/revoke permission from pid="

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    .line 832
    invoke-static {}, Landroid/os/Binder;->getCallingPid()I

    move-result v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, ", uid="

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-static {}, Landroid/os/Binder;->getCallingUid()I

    move-result v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, ", pkg="

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-direct {v1, v2}, Ljava/lang/SecurityException;-><init>(Ljava/lang/String;)V

    throw v1
.end method

.method private checkPermission()V
    .locals 6

    .line 837
    invoke-static {}, Landroid/os/Binder;->getCallingUid()I

    move-result v0

    .line 838
    .local v0, "callingUid":I
    invoke-static {v0}, Landroid/os/UserHandle;->getAppId(I)I

    move-result v1

    const/16 v2, 0x7d0

    if-eq v1, v2, :cond_2

    .line 841
    iget-object v1, p0, Lcom/miui/server/SecurityManagerService;->mContext:Landroid/content/Context;

    const-string v2, "android.permission.CHANGE_COMPONENT_ENABLED_STATE"

    invoke-virtual {v1, v2}, Landroid/content/Context;->checkCallingOrSelfPermission(Ljava/lang/String;)I

    move-result v1

    .line 843
    .local v1, "permission":I
    if-nez v1, :cond_0

    return-void

    .line 844
    :cond_0
    iget-object v2, p0, Lcom/miui/server/SecurityManagerService;->mContext:Landroid/content/Context;

    const-string v3, "miui.permission.READ_AND_WIRTE_PERMISSION_MANAGER"

    invoke-virtual {v2, v3}, Landroid/content/Context;->checkCallingOrSelfPermission(Ljava/lang/String;)I

    move-result v2

    .line 845
    .local v2, "managePermission":I
    if-nez v2, :cond_1

    return-void

    .line 846
    :cond_1
    new-instance v3, Ljava/lang/SecurityException;

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "Permission Denial: attempt to change application state from pid="

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    .line 847
    invoke-static {}, Landroid/os/Binder;->getCallingPid()I

    move-result v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, ", uid="

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-direct {v3, v4}, Ljava/lang/SecurityException;-><init>(Ljava/lang/String;)V

    throw v3

    .line 839
    .end local v1    # "permission":I
    .end local v2    # "managePermission":I
    :cond_2
    new-instance v1, Ljava/lang/SecurityException;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "no permission for UID:"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-direct {v1, v2}, Ljava/lang/SecurityException;-><init>(Ljava/lang/String;)V

    throw v1
.end method

.method private varargs checkPermissionByUid([I)V
    .locals 5
    .param p1, "uids"    # [I

    .line 816
    invoke-static {}, Landroid/os/Binder;->getCallingUid()I

    move-result v0

    .line 817
    .local v0, "callingUid":I
    const/4 v1, 0x0

    .line 818
    .local v1, "hasPermission":Z
    const/4 v2, 0x0

    .local v2, "i":I
    :goto_0
    array-length v3, p1

    if-ge v2, v3, :cond_1

    .line 819
    invoke-static {v0}, Landroid/os/UserHandle;->getAppId(I)I

    move-result v3

    aget v4, p1, v2

    if-ne v3, v4, :cond_0

    .line 820
    const/4 v1, 0x1

    .line 818
    :cond_0
    add-int/lit8 v2, v2, 0x1

    goto :goto_0

    .line 823
    .end local v2    # "i":I
    :cond_1
    if-eqz v1, :cond_2

    .line 826
    return-void

    .line 824
    :cond_2
    new-instance v2, Ljava/lang/SecurityException;

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "no permission to read file for UID:"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-direct {v2, v3}, Ljava/lang/SecurityException;-><init>(Ljava/lang/String;)V

    throw v2
.end method

.method private checkWakePathPermission()V
    .locals 5

    .line 607
    iget-object v0, p0, Lcom/miui/server/SecurityManagerService;->mContext:Landroid/content/Context;

    .line 608
    invoke-static {}, Landroid/os/Binder;->getCallingPid()I

    move-result v1

    invoke-static {}, Landroid/os/Binder;->getCallingUid()I

    move-result v2

    .line 607
    const-string v3, "android.permission.UPDATE_APP_OPS_STATS"

    const/4 v4, 0x0

    invoke-virtual {v0, v3, v1, v2, v4}, Landroid/content/Context;->enforcePermission(Ljava/lang/String;IILjava/lang/String;)V

    .line 609
    return-void
.end method

.method private checkWriteSecurePermission()V
    .locals 3

    .line 729
    iget-object v0, p0, Lcom/miui/server/SecurityManagerService;->mContext:Landroid/content/Context;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Permission Denial: attempt to change application privacy revoke state from pid="

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    .line 731
    invoke-static {}, Landroid/os/Binder;->getCallingPid()I

    move-result v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ", uid="

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    .line 732
    invoke-static {}, Landroid/os/Binder;->getCallingUid()I

    move-result v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    .line 729
    const-string v2, "android.permission.WRITE_SECURE_SETTINGS"

    invoke-virtual {v0, v2, v1}, Landroid/content/Context;->enforceCallingPermission(Ljava/lang/String;Ljava/lang/String;)V

    .line 733
    return-void
.end method

.method private dumpPrivacyVirtualDisplay(Ljava/io/PrintWriter;)V
    .locals 4
    .param p1, "out"    # Ljava/io/PrintWriter;

    .line 1003
    const-string v0, "===================================SCREEN SHARE PROTECTION DUMP BEGIN========================================"

    invoke-virtual {p1, v0}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V

    .line 1005
    iget-object v0, p0, Lcom/miui/server/SecurityManagerService;->mPrivacyVirtualDisplay:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    if-nez v0, :cond_0

    .line 1006
    const-string v0, "Don\'t have any privacy virtual display package!"

    invoke-virtual {p1, v0}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V

    goto :goto_1

    .line 1008
    :cond_0
    iget-object v0, p0, Lcom/miui/server/SecurityManagerService;->mPrivacyVirtualDisplay:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v0

    :goto_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_1

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/String;

    .line 1009
    .local v1, "packageName":Ljava/lang/String;
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "packageName: "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {p1, v2}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V

    .line 1010
    .end local v1    # "packageName":Ljava/lang/String;
    goto :goto_0

    .line 1012
    :cond_1
    :goto_1
    iget-object v0, p0, Lcom/miui/server/SecurityManagerService;->mPrivacyDisplayNameList:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    if-nez v0, :cond_2

    .line 1013
    const-string v0, "Don\'t have any privacy virtual display name!"

    invoke-virtual {p1, v0}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V

    goto :goto_3

    .line 1015
    :cond_2
    iget-object v0, p0, Lcom/miui/server/SecurityManagerService;->mPrivacyDisplayNameList:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v0

    :goto_2
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_3

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/String;

    .line 1016
    .local v1, "name":Ljava/lang/String;
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v3, "virtual display: "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {p1, v2}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V

    .line 1017
    .end local v1    # "name":Ljava/lang/String;
    goto :goto_2

    .line 1019
    :cond_3
    :goto_3
    const-string v0, "====================================SCREEN SHARE PROTECTION DUMP END========================================"

    invoke-virtual {p1, v0}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V

    .line 1021
    return-void
.end method

.method private initWhenBootCompleted()V
    .locals 4

    .line 215
    iget-object v0, p0, Lcom/miui/server/SecurityManagerService;->mWakeUpTimeImpl:Lcom/miui/server/security/WakeUpTimeImpl;

    invoke-virtual {v0}, Lcom/miui/server/security/WakeUpTimeImpl;->readWakeUpTime()V

    .line 216
    iget-object v0, p0, Lcom/miui/server/SecurityManagerService;->mUserStateLock:Ljava/lang/Object;

    monitor-enter v0

    .line 217
    const/4 v1, 0x0

    :try_start_0
    invoke-virtual {p0, v1}, Lcom/miui/server/SecurityManagerService;->getUserStateLocked(I)Lcom/miui/server/security/SecurityUserState;

    move-result-object v1

    .line 218
    .local v1, "userState":Lcom/miui/server/security/SecurityUserState;
    iget-object v2, p0, Lcom/miui/server/SecurityManagerService;->mSettingsObserver:Lcom/miui/server/security/SecuritySettingsObserver;

    invoke-virtual {v2, v1}, Lcom/miui/server/security/SecuritySettingsObserver;->initAccessControlSettingsLocked(Lcom/miui/server/security/SecurityUserState;)V

    .line 219
    .end local v1    # "userState":Lcom/miui/server/security/SecurityUserState;
    monitor-exit v0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 220
    iget-object v0, p0, Lcom/miui/server/SecurityManagerService;->mSettingsObserver:Lcom/miui/server/security/SecuritySettingsObserver;

    invoke-virtual {v0}, Lcom/miui/server/security/SecuritySettingsObserver;->registerForSettingsChanged()V

    .line 221
    invoke-static {}, Lmiui/security/WakePathChecker;->getInstance()Lmiui/security/WakePathChecker;

    move-result-object v0

    iget-object v1, p0, Lcom/miui/server/SecurityManagerService;->mContext:Landroid/content/Context;

    invoke-virtual {v0, v1}, Lmiui/security/WakePathChecker;->init(Landroid/content/Context;)V

    .line 223
    invoke-static {}, Lmiui/app/StorageRestrictedPathManager;->getInstance()Lmiui/app/StorageRestrictedPathManager;

    move-result-object v0

    iget-object v1, p0, Lcom/miui/server/SecurityManagerService;->mContext:Landroid/content/Context;

    invoke-virtual {v0, v1}, Lmiui/app/StorageRestrictedPathManager;->init(Landroid/content/Context;)V

    .line 225
    iget-object v0, p0, Lcom/miui/server/SecurityManagerService;->mContext:Landroid/content/Context;

    invoke-static {v0}, Lcom/miui/server/RestrictAppNetManager;->init(Landroid/content/Context;)V

    .line 227
    iget-object v0, p0, Lcom/miui/server/SecurityManagerService;->mAccessController:Lcom/miui/server/AccessController;

    invoke-static {}, Landroid/os/UserHandle;->myUserId()I

    move-result v1

    invoke-virtual {v0, v1}, Lcom/miui/server/AccessController;->updatePasswordTypeForPattern(I)V

    .line 228
    sget-boolean v0, Lmiui/os/Build;->IS_INTERNATIONAL_BUILD:Z

    if-nez v0, :cond_0

    .line 230
    iget-object v0, p0, Lcom/miui/server/SecurityManagerService;->mDefaultBrowserImpl:Lcom/miui/server/security/DefaultBrowserImpl;

    invoke-virtual {v0}, Lcom/miui/server/security/DefaultBrowserImpl;->resetDefaultBrowser()V

    .line 231
    const-class v0, Landroid/content/pm/PackageManagerInternal;

    invoke-static {v0}, Lcom/android/server/LocalServices;->getService(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/content/pm/PackageManagerInternal;

    .line 232
    .local v0, "pmi":Landroid/content/pm/PackageManagerInternal;
    new-instance v1, Lcom/miui/server/SecurityManagerService$MyPackageObserver;

    iget-object v2, p0, Lcom/miui/server/SecurityManagerService;->mContext:Landroid/content/Context;

    iget-object v3, p0, Lcom/miui/server/SecurityManagerService;->mDefaultBrowserImpl:Lcom/miui/server/security/DefaultBrowserImpl;

    invoke-direct {v1, v2, v3}, Lcom/miui/server/SecurityManagerService$MyPackageObserver;-><init>(Landroid/content/Context;Lcom/miui/server/security/DefaultBrowserImpl;)V

    invoke-virtual {v0, v1}, Landroid/content/pm/PackageManagerInternal;->getPackageList(Landroid/content/pm/PackageManagerInternal$PackageListObserver;)Lcom/android/server/pm/PackageList;

    .line 234
    .end local v0    # "pmi":Landroid/content/pm/PackageManagerInternal;
    :cond_0
    return-void

    .line 219
    :catchall_0
    move-exception v1

    :try_start_1
    monitor-exit v0
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    throw v1
.end method

.method private synthetic lambda$isAppPrivacyEnabled$1(Ljava/lang/String;)Ljava/lang/Boolean;
    .locals 3
    .param p1, "packageName"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    .line 756
    iget-object v0, p0, Lcom/miui/server/SecurityManagerService;->mContext:Landroid/content/Context;

    .line 757
    invoke-virtual {v0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "privacy_status_"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    const/4 v2, 0x1

    invoke-static {v0, v1, v2}, Landroid/provider/Settings$Secure;->getInt(Landroid/content/ContentResolver;Ljava/lang/String;I)I

    move-result v0

    if-eqz v0, :cond_0

    goto :goto_0

    :cond_0
    const/4 v2, 0x0

    .line 756
    :goto_0
    invoke-static {v2}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    return-object v0
.end method

.method private synthetic lambda$setAppPrivacyStatus$0(Ljava/lang/String;Z)Ljava/lang/Boolean;
    .locals 3
    .param p1, "packageName"    # Ljava/lang/String;
    .param p2, "isOpen"    # Z
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    .line 747
    iget-object v0, p0, Lcom/miui/server/SecurityManagerService;->mContext:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "privacy_status_"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    .line 748
    nop

    .line 747
    invoke-static {v0, v1, p2}, Landroid/provider/Settings$Secure;->putInt(Landroid/content/ContentResolver;Ljava/lang/String;I)Z

    move-result v0

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    return-object v0
.end method

.method private synthetic lambda$updateAppWidgetVisibility$2(Landroid/util/SparseArray;)V
    .locals 2
    .param p1, "uidPackageNames"    # Landroid/util/SparseArray;

    .line 1424
    iget-object v0, p0, Lcom/miui/server/SecurityManagerService;->mAppOpsInternal:Landroid/app/AppOpsManagerInternal;

    const/4 v1, 0x0

    invoke-virtual {v0, p1, v1}, Landroid/app/AppOpsManagerInternal;->updateAppWidgetVisibility(Landroid/util/SparseArray;Z)V

    return-void
.end method

.method private loadData()V
    .locals 0

    .line 208
    invoke-direct {p0}, Lcom/miui/server/SecurityManagerService;->readSettings()V

    .line 210
    invoke-direct {p0}, Lcom/miui/server/SecurityManagerService;->updateXSpaceSettings()V

    .line 211
    return-void
.end method

.method private readPackagesSettings(Ljava/io/FileInputStream;)V
    .locals 14
    .param p1, "fis"    # Ljava/io/FileInputStream;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    .line 1172
    invoke-static {}, Landroid/util/Xml;->newPullParser()Lorg/xmlpull/v1/XmlPullParser;

    move-result-object v0

    .line 1173
    .local v0, "parser":Lorg/xmlpull/v1/XmlPullParser;
    const/4 v1, 0x0

    invoke-interface {v0, p1, v1}, Lorg/xmlpull/v1/XmlPullParser;->setInput(Ljava/io/InputStream;Ljava/lang/String;)V

    .line 1175
    invoke-interface {v0}, Lorg/xmlpull/v1/XmlPullParser;->getEventType()I

    move-result v2

    .line 1176
    .local v2, "eventType":I
    :goto_0
    const/4 v3, 0x1

    const/4 v4, 0x2

    if-eq v2, v4, :cond_0

    if-eq v2, v3, :cond_0

    .line 1177
    invoke-interface {v0}, Lorg/xmlpull/v1/XmlPullParser;->next()I

    move-result v2

    goto :goto_0

    .line 1180
    :cond_0
    invoke-interface {v0}, Lorg/xmlpull/v1/XmlPullParser;->getName()Ljava/lang/String;

    move-result-object v5

    .line 1181
    .local v5, "tagName":Ljava/lang/String;
    const-string v6, "packages"

    invoke-virtual {v6, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-eqz v6, :cond_6

    .line 1182
    const-string/jumbo v6, "updateVersion"

    invoke-interface {v0, v1, v6}, Lorg/xmlpull/v1/XmlPullParser;->getAttributeValue(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v6

    .line 1183
    .local v6, "updateVersion":Ljava/lang/String;
    invoke-static {v6}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v7

    if-nez v7, :cond_1

    const-string v7, "1.0"

    invoke-virtual {v7, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v7

    if-eqz v7, :cond_1

    .line 1184
    iput-boolean v3, p0, Lcom/miui/server/SecurityManagerService;->mIsUpdated:Z

    .line 1186
    :cond_1
    invoke-interface {v0}, Lorg/xmlpull/v1/XmlPullParser;->next()I

    move-result v2

    .line 1188
    :cond_2
    if-ne v2, v4, :cond_5

    invoke-interface {v0}, Lorg/xmlpull/v1/XmlPullParser;->getDepth()I

    move-result v7

    if-ne v7, v4, :cond_5

    .line 1189
    invoke-interface {v0}, Lorg/xmlpull/v1/XmlPullParser;->getName()Ljava/lang/String;

    move-result-object v5

    .line 1190
    const-string v7, "package"

    invoke-virtual {v7, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v7

    if-eqz v7, :cond_5

    .line 1191
    const-string v7, "name"

    invoke-interface {v0, v1, v7}, Lorg/xmlpull/v1/XmlPullParser;->getAttributeValue(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v7

    .line 1192
    .local v7, "name":Ljava/lang/String;
    invoke-static {v7}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v8

    if-eqz v8, :cond_3

    .line 1193
    const-string v8, "SecurityManagerService"

    const-string v9, "read current package name is empty, skip"

    invoke-static {v8, v9}, Landroid/util/Slog;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 1194
    goto/16 :goto_2

    .line 1196
    :cond_3
    new-instance v8, Lcom/miui/server/security/SecurityPackageSettings;

    invoke-direct {v8, v7}, Lcom/miui/server/security/SecurityPackageSettings;-><init>(Ljava/lang/String;)V

    .line 1197
    .local v8, "ps":Lcom/miui/server/security/SecurityPackageSettings;
    const/4 v9, 0x0

    .line 1198
    .local v9, "userHandle":I
    const-string/jumbo v10, "u"

    invoke-interface {v0, v1, v10}, Lorg/xmlpull/v1/XmlPullParser;->getAttributeValue(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v10

    .line 1199
    .local v10, "userHandleStr":Ljava/lang/String;
    invoke-static {v10}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v11

    if-nez v11, :cond_4

    .line 1200
    invoke-static {v10}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v9

    .line 1202
    :cond_4
    const-string v11, "accessControl"

    invoke-interface {v0, v1, v11}, Lorg/xmlpull/v1/XmlPullParser;->getAttributeValue(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v11

    invoke-static {v11}, Ljava/lang/Boolean;->parseBoolean(Ljava/lang/String;)Z

    move-result v11

    iput-boolean v11, v8, Lcom/miui/server/security/SecurityPackageSettings;->accessControl:Z

    .line 1203
    const-string v11, "childrenControl"

    invoke-interface {v0, v1, v11}, Lorg/xmlpull/v1/XmlPullParser;->getAttributeValue(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v11

    invoke-static {v11}, Ljava/lang/Boolean;->parseBoolean(Ljava/lang/String;)Z

    move-result v11

    iput-boolean v11, v8, Lcom/miui/server/security/SecurityPackageSettings;->childrenControl:Z

    .line 1204
    const-string v11, "maskNotification"

    invoke-interface {v0, v1, v11}, Lorg/xmlpull/v1/XmlPullParser;->getAttributeValue(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v11

    invoke-static {v11}, Ljava/lang/Boolean;->parseBoolean(Ljava/lang/String;)Z

    move-result v11

    iput-boolean v11, v8, Lcom/miui/server/security/SecurityPackageSettings;->maskNotification:Z

    .line 1205
    const-string v11, "isPrivacyApp"

    invoke-interface {v0, v1, v11}, Lorg/xmlpull/v1/XmlPullParser;->getAttributeValue(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v11

    invoke-static {v11}, Ljava/lang/Boolean;->parseBoolean(Ljava/lang/String;)Z

    move-result v11

    iput-boolean v11, v8, Lcom/miui/server/security/SecurityPackageSettings;->isPrivacyApp:Z

    .line 1207
    const-string v11, "isDarkModeChecked"

    invoke-interface {v0, v1, v11}, Lorg/xmlpull/v1/XmlPullParser;->getAttributeValue(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v11

    invoke-static {v11}, Ljava/lang/Boolean;->parseBoolean(Ljava/lang/String;)Z

    move-result v11

    iput-boolean v11, v8, Lcom/miui/server/security/SecurityPackageSettings;->isDarkModeChecked:Z

    .line 1209
    const-string v11, "isGameStorageApp"

    invoke-interface {v0, v1, v11}, Lorg/xmlpull/v1/XmlPullParser;->getAttributeValue(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v11

    invoke-static {v11}, Ljava/lang/Boolean;->parseBoolean(Ljava/lang/String;)Z

    move-result v11

    iput-boolean v11, v8, Lcom/miui/server/security/SecurityPackageSettings;->isGameStorageApp:Z

    .line 1210
    const-string v11, "isRemindForRelaunch"

    invoke-interface {v0, v1, v11}, Lorg/xmlpull/v1/XmlPullParser;->getAttributeValue(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v11

    invoke-static {v11}, Ljava/lang/Boolean;->parseBoolean(Ljava/lang/String;)Z

    move-result v11

    iput-boolean v11, v8, Lcom/miui/server/security/SecurityPackageSettings;->isRemindForRelaunch:Z

    .line 1211
    const-string v11, "isRelaunchWhenFolded"

    invoke-interface {v0, v1, v11}, Lorg/xmlpull/v1/XmlPullParser;->getAttributeValue(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v11

    invoke-static {v11}, Ljava/lang/Boolean;->parseBoolean(Ljava/lang/String;)Z

    move-result v11

    iput-boolean v11, v8, Lcom/miui/server/security/SecurityPackageSettings;->isRelaunchWhenFolded:Z

    .line 1212
    const-string v11, "isScRelaunchConfirm"

    invoke-interface {v0, v1, v11}, Lorg/xmlpull/v1/XmlPullParser;->getAttributeValue(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v11

    invoke-static {v11}, Ljava/lang/Boolean;->parseBoolean(Ljava/lang/String;)Z

    move-result v11

    iput-boolean v11, v8, Lcom/miui/server/security/SecurityPackageSettings;->isScRelaunchConfirm:Z

    .line 1213
    iget-object v11, p0, Lcom/miui/server/SecurityManagerService;->mUserStateLock:Ljava/lang/Object;

    monitor-enter v11

    .line 1214
    :try_start_0
    invoke-virtual {p0, v9}, Lcom/miui/server/SecurityManagerService;->getUserStateLocked(I)Lcom/miui/server/security/SecurityUserState;

    move-result-object v12

    .line 1215
    .local v12, "userState":Lcom/miui/server/security/SecurityUserState;
    iget-object v13, v12, Lcom/miui/server/security/SecurityUserState;->mPackages:Ljava/util/HashMap;

    invoke-virtual {v13, v7, v8}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 1216
    nop

    .end local v12    # "userState":Lcom/miui/server/security/SecurityUserState;
    monitor-exit v11

    goto :goto_1

    :catchall_0
    move-exception v1

    monitor-exit v11
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v1

    .line 1219
    .end local v7    # "name":Ljava/lang/String;
    .end local v8    # "ps":Lcom/miui/server/security/SecurityPackageSettings;
    .end local v9    # "userHandle":I
    .end local v10    # "userHandleStr":Ljava/lang/String;
    :cond_5
    :goto_1
    invoke-interface {v0}, Lorg/xmlpull/v1/XmlPullParser;->next()I

    move-result v2

    .line 1220
    :goto_2
    if-ne v2, v3, :cond_2

    .line 1222
    .end local v6    # "updateVersion":Ljava/lang/String;
    :cond_6
    return-void
.end method

.method private readSettings()V
    .locals 3

    .line 1161
    iget-object v0, p0, Lcom/miui/server/SecurityManagerService;->mSettingsFile:Landroid/util/AtomicFile;

    invoke-virtual {v0}, Landroid/util/AtomicFile;->getBaseFile()Ljava/io/File;

    move-result-object v0

    invoke-virtual {v0}, Ljava/io/File;->exists()Z

    move-result v0

    if-nez v0, :cond_0

    .line 1162
    return-void

    .line 1164
    :cond_0
    :try_start_0
    iget-object v0, p0, Lcom/miui/server/SecurityManagerService;->mSettingsFile:Landroid/util/AtomicFile;

    invoke-virtual {v0}, Landroid/util/AtomicFile;->openRead()Ljava/io/FileInputStream;

    move-result-object v0
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 1165
    .local v0, "fis":Ljava/io/FileInputStream;
    :try_start_1
    invoke-direct {p0, v0}, Lcom/miui/server/SecurityManagerService;->readPackagesSettings(Ljava/io/FileInputStream;)V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 1166
    if-eqz v0, :cond_1

    :try_start_2
    invoke-virtual {v0}, Ljava/io/FileInputStream;->close()V
    :try_end_2
    .catch Ljava/lang/Exception; {:try_start_2 .. :try_end_2} :catch_0

    .line 1168
    .end local v0    # "fis":Ljava/io/FileInputStream;
    :cond_1
    goto :goto_1

    .line 1164
    .restart local v0    # "fis":Ljava/io/FileInputStream;
    :catchall_0
    move-exception v1

    if-eqz v0, :cond_2

    :try_start_3
    invoke-virtual {v0}, Ljava/io/FileInputStream;->close()V
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    goto :goto_0

    :catchall_1
    move-exception v2

    :try_start_4
    invoke-virtual {v1, v2}, Ljava/lang/Throwable;->addSuppressed(Ljava/lang/Throwable;)V

    .end local p0    # "this":Lcom/miui/server/SecurityManagerService;
    :cond_2
    :goto_0
    throw v1
    :try_end_4
    .catch Ljava/lang/Exception; {:try_start_4 .. :try_end_4} :catch_0

    .line 1166
    .end local v0    # "fis":Ljava/io/FileInputStream;
    .restart local p0    # "this":Lcom/miui/server/SecurityManagerService;
    :catch_0
    move-exception v0

    .line 1167
    .local v0, "e":Ljava/lang/Exception;
    const-string v1, "SecurityManagerService"

    const-string v2, "Error reading package settings"

    invoke-static {v1, v2, v0}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    .line 1169
    .end local v0    # "e":Ljava/lang/Exception;
    :goto_1
    return-void
.end method

.method private updateXSpaceSettings()V
    .locals 10

    .line 684
    iget-object v0, p0, Lcom/miui/server/SecurityManagerService;->mUserStateLock:Ljava/lang/Object;

    monitor-enter v0

    .line 685
    :try_start_0
    invoke-static {}, Lmiui/securityspace/ConfigUtils;->isSupportXSpace()Z

    move-result v1

    if-eqz v1, :cond_2

    iget-boolean v1, p0, Lcom/miui/server/SecurityManagerService;->mIsUpdated:Z

    if-nez v1, :cond_2

    .line 686
    const/4 v1, 0x0

    invoke-virtual {p0, v1}, Lcom/miui/server/SecurityManagerService;->getUserStateLocked(I)Lcom/miui/server/security/SecurityUserState;

    move-result-object v1

    .line 687
    .local v1, "userState":Lcom/miui/server/security/SecurityUserState;
    const/16 v2, 0x3e7

    invoke-virtual {p0, v2}, Lcom/miui/server/SecurityManagerService;->getUserStateLocked(I)Lcom/miui/server/security/SecurityUserState;

    move-result-object v2

    .line 688
    .local v2, "userStateXSpace":Lcom/miui/server/security/SecurityUserState;
    iget-object v3, v1, Lcom/miui/server/security/SecurityUserState;->mPackages:Ljava/util/HashMap;

    invoke-virtual {v3}, Ljava/util/HashMap;->entrySet()Ljava/util/Set;

    move-result-object v3

    .line 689
    .local v3, "packagesSet":Ljava/util/Set;, "Ljava/util/Set<Ljava/util/Map$Entry<Ljava/lang/String;Lcom/miui/server/security/SecurityPackageSettings;>;>;"
    invoke-interface {v3}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v4

    :goto_0
    invoke-interface {v4}, Ljava/util/Iterator;->hasNext()Z

    move-result v5

    if-eqz v5, :cond_1

    invoke-interface {v4}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Ljava/util/Map$Entry;

    .line 690
    .local v5, "entrySet":Ljava/util/Map$Entry;, "Ljava/util/Map$Entry<Ljava/lang/String;Lcom/miui/server/security/SecurityPackageSettings;>;"
    invoke-interface {v5}, Ljava/util/Map$Entry;->getKey()Ljava/lang/Object;

    move-result-object v6

    check-cast v6, Ljava/lang/String;

    .line 691
    .local v6, "name":Ljava/lang/String;
    invoke-static {v6}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v7

    if-nez v7, :cond_0

    iget-object v7, p0, Lcom/miui/server/SecurityManagerService;->mContext:Landroid/content/Context;

    invoke-static {v7, v6}, Lmiui/securityspace/XSpaceUserHandle;->isAppInXSpace(Landroid/content/Context;Ljava/lang/String;)Z

    move-result v7

    if-eqz v7, :cond_0

    .line 692
    invoke-interface {v5}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v7

    check-cast v7, Lcom/miui/server/security/SecurityPackageSettings;

    .line 693
    .local v7, "value":Lcom/miui/server/security/SecurityPackageSettings;
    new-instance v8, Lcom/miui/server/security/SecurityPackageSettings;

    invoke-direct {v8, v6}, Lcom/miui/server/security/SecurityPackageSettings;-><init>(Ljava/lang/String;)V

    .line 694
    .local v8, "psXSpace":Lcom/miui/server/security/SecurityPackageSettings;
    iget-boolean v9, v7, Lcom/miui/server/security/SecurityPackageSettings;->accessControl:Z

    iput-boolean v9, v8, Lcom/miui/server/security/SecurityPackageSettings;->accessControl:Z

    .line 695
    iget-boolean v9, v7, Lcom/miui/server/security/SecurityPackageSettings;->childrenControl:Z

    iput-boolean v9, v8, Lcom/miui/server/security/SecurityPackageSettings;->childrenControl:Z

    .line 696
    iget-object v9, v2, Lcom/miui/server/security/SecurityUserState;->mPackages:Ljava/util/HashMap;

    invoke-virtual {v9, v6, v8}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 698
    .end local v5    # "entrySet":Ljava/util/Map$Entry;, "Ljava/util/Map$Entry<Ljava/lang/String;Lcom/miui/server/security/SecurityPackageSettings;>;"
    .end local v6    # "name":Ljava/lang/String;
    .end local v7    # "value":Lcom/miui/server/security/SecurityPackageSettings;
    .end local v8    # "psXSpace":Lcom/miui/server/security/SecurityPackageSettings;
    :cond_0
    goto :goto_0

    .line 699
    :cond_1
    invoke-virtual {p0}, Lcom/miui/server/SecurityManagerService;->scheduleWriteSettings()V

    .line 700
    iget-object v4, p0, Lcom/miui/server/SecurityManagerService;->mAccessControlImpl:Lcom/miui/server/security/AccessControlImpl;

    invoke-virtual {v4}, Lcom/miui/server/security/AccessControlImpl;->updateMaskObserverValues()V

    .line 702
    .end local v1    # "userState":Lcom/miui/server/security/SecurityUserState;
    .end local v2    # "userStateXSpace":Lcom/miui/server/security/SecurityUserState;
    .end local v3    # "packagesSet":Ljava/util/Set;, "Ljava/util/Set<Ljava/util/Map$Entry<Ljava/lang/String;Lcom/miui/server/security/SecurityPackageSettings;>;>;"
    :cond_2
    monitor-exit v0

    .line 703
    return-void

    .line 702
    :catchall_0
    move-exception v1

    monitor-exit v0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v1
.end method


# virtual methods
.method public activityResume(Landroid/content/Intent;)I
    .locals 1
    .param p1, "intent"    # Landroid/content/Intent;

    .line 536
    iget-object v0, p0, Lcom/miui/server/SecurityManagerService;->mAccessControlImpl:Lcom/miui/server/security/AccessControlImpl;

    invoke-virtual {v0, p1}, Lcom/miui/server/security/AccessControlImpl;->activityResume(Landroid/content/Intent;)I

    move-result v0

    return v0
.end method

.method public addAccessControlPass(Ljava/lang/String;)V
    .locals 1
    .param p1, "packageName"    # Ljava/lang/String;

    .line 430
    const/16 v0, 0x3e8

    filled-new-array {v0}, [I

    move-result-object v0

    invoke-direct {p0, v0}, Lcom/miui/server/SecurityManagerService;->checkPermissionByUid([I)V

    .line 431
    invoke-static {}, Landroid/os/UserHandle;->getCallingUserId()I

    move-result v0

    invoke-virtual {p0, p1, v0}, Lcom/miui/server/SecurityManagerService;->addAccessControlPassForUser(Ljava/lang/String;I)V

    .line 432
    return-void
.end method

.method public addAccessControlPassForUser(Ljava/lang/String;I)V
    .locals 1
    .param p1, "packageName"    # Ljava/lang/String;
    .param p2, "userId"    # I

    .line 436
    invoke-direct {p0}, Lcom/miui/server/SecurityManagerService;->checkPermission()V

    .line 437
    iget-object v0, p0, Lcom/miui/server/SecurityManagerService;->mAccessControlImpl:Lcom/miui/server/security/AccessControlImpl;

    invoke-virtual {v0, p1, p2}, Lcom/miui/server/security/AccessControlImpl;->addAccessControlPassForUser(Ljava/lang/String;I)V

    .line 438
    return-void
.end method

.method public addMiuiFirewallSharedUid(I)Z
    .locals 1
    .param p1, "uid"    # I

    .line 257
    const/16 v0, 0x3e8

    filled-new-array {v0}, [I

    move-result-object v0

    invoke-direct {p0, v0}, Lcom/miui/server/SecurityManagerService;->checkPermissionByUid([I)V

    .line 258
    invoke-static {}, Lcom/android/server/net/NetworkManagementServiceStub;->getInstance()Lcom/android/server/net/NetworkManagementServiceStub;

    move-result-object v0

    invoke-interface {v0, p1}, Lcom/android/server/net/NetworkManagementServiceStub;->addMiuiFirewallSharedUid(I)Z

    move-result v0

    return v0
.end method

.method public addRestrictChainMaps(Ljava/lang/String;Ljava/lang/String;)V
    .locals 3
    .param p1, "caller"    # Ljava/lang/String;
    .param p2, "callee"    # Ljava/lang/String;

    .line 1332
    invoke-static {p2}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_5

    invoke-static {p1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_0

    goto/16 :goto_3

    .line 1335
    :cond_0
    const/16 v0, 0x3e8

    filled-new-array {v0}, [I

    move-result-object v0

    invoke-direct {p0, v0}, Lcom/miui/server/SecurityManagerService;->checkPermissionByUid([I)V

    .line 1336
    const-string v0, "com.tencent.mm"

    invoke-static {v0, p2}, Landroid/text/TextUtils;->equals(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_2

    .line 1337
    const-string v0, "com.eg.android.AlipayGphone"

    invoke-static {v0, p2}, Landroid/text/TextUtils;->equals(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_2

    .line 1338
    iget-object v0, p0, Lcom/miui/server/SecurityManagerService;->restrictChainMaps:Ljava/util/LinkedHashMap;

    invoke-virtual {v0, p1}, Ljava/util/LinkedHashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/List;

    .line 1339
    .local v0, "callees":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;"
    if-eqz v0, :cond_1

    .line 1340
    invoke-interface {v0, p2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 1342
    :cond_1
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    .line 1343
    .local v1, "strings":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/String;>;"
    invoke-virtual {v1, p2}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 1344
    iget-object v2, p0, Lcom/miui/server/SecurityManagerService;->restrictChainMaps:Ljava/util/LinkedHashMap;

    invoke-virtual {v2, p1, v1}, Ljava/util/LinkedHashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 1347
    .end local v0    # "callees":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;"
    .end local v1    # "strings":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/String;>;"
    :cond_2
    :goto_0
    iget-object v0, p0, Lcom/miui/server/SecurityManagerService;->restrictChainMaps:Ljava/util/LinkedHashMap;

    invoke-virtual {v0, p2}, Ljava/util/LinkedHashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/List;

    .line 1348
    .local v0, "callers":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;"
    if-eqz v0, :cond_3

    .line 1349
    invoke-interface {v0, p1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_1

    .line 1351
    :cond_3
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    .line 1352
    .restart local v1    # "strings":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/String;>;"
    invoke-virtual {v1, p1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 1353
    iget-object v2, p0, Lcom/miui/server/SecurityManagerService;->restrictChainMaps:Ljava/util/LinkedHashMap;

    invoke-virtual {v2, p2, v1}, Ljava/util/LinkedHashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 1355
    .end local v1    # "strings":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/String;>;"
    :goto_1
    iget-object v1, p0, Lcom/miui/server/SecurityManagerService;->restrictChainMaps:Ljava/util/LinkedHashMap;

    invoke-virtual {v1}, Ljava/util/LinkedHashMap;->size()I

    move-result v1

    const/16 v2, 0x64

    if-le v1, v2, :cond_4

    .line 1357
    :try_start_0
    iget-object v1, p0, Lcom/miui/server/SecurityManagerService;->restrictChainMaps:Ljava/util/LinkedHashMap;

    invoke-virtual {v1}, Ljava/util/LinkedHashMap;->entrySet()Ljava/util/Set;

    move-result-object v2

    invoke-interface {v2}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v2

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/util/Map$Entry;

    invoke-interface {v2}, Ljava/util/Map$Entry;->getKey()Ljava/lang/Object;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/util/LinkedHashMap;->remove(Ljava/lang/Object;)Ljava/lang/Object;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 1360
    goto :goto_2

    .line 1358
    :catch_0
    move-exception v1

    .line 1362
    :cond_4
    :goto_2
    return-void

    .line 1333
    .end local v0    # "callers":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;"
    :cond_5
    :goto_3
    return-void
.end method

.method public areNotificationsEnabledForPackage(Ljava/lang/String;I)Z
    .locals 3
    .param p1, "packageName"    # Ljava/lang/String;
    .param p2, "uid"    # I
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    .line 852
    invoke-direct {p0}, Lcom/miui/server/SecurityManagerService;->checkPermission()V

    .line 853
    iget-object v0, p0, Lcom/miui/server/SecurityManagerService;->mINotificationManager:Landroid/app/INotificationManager;

    if-nez v0, :cond_0

    .line 854
    const-string v0, "notification"

    invoke-static {v0}, Landroid/os/ServiceManager;->getService(Ljava/lang/String;)Landroid/os/IBinder;

    move-result-object v0

    invoke-static {v0}, Landroid/app/INotificationManager$Stub;->asInterface(Landroid/os/IBinder;)Landroid/app/INotificationManager;

    move-result-object v0

    iput-object v0, p0, Lcom/miui/server/SecurityManagerService;->mINotificationManager:Landroid/app/INotificationManager;

    .line 856
    :cond_0
    invoke-static {}, Landroid/os/Binder;->clearCallingIdentity()J

    move-result-wide v0

    .line 858
    .local v0, "identity":J
    :try_start_0
    iget-object v2, p0, Lcom/miui/server/SecurityManagerService;->mINotificationManager:Landroid/app/INotificationManager;

    invoke-interface {v2, p1, p2}, Landroid/app/INotificationManager;->areNotificationsEnabledForPackage(Ljava/lang/String;I)Z

    move-result v2
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 860
    invoke-static {v0, v1}, Landroid/os/Binder;->restoreCallingIdentity(J)V

    .line 858
    return v2

    .line 860
    :catchall_0
    move-exception v2

    invoke-static {v0, v1}, Landroid/os/Binder;->restoreCallingIdentity(J)V

    .line 861
    throw v2
.end method

.method public checkAccessControlPass(Ljava/lang/String;Landroid/content/Intent;)Z
    .locals 2
    .param p1, "packageName"    # Ljava/lang/String;
    .param p2, "intent"    # Landroid/content/Intent;

    .line 449
    invoke-static {}, Landroid/os/UserHandle;->getCallingUserId()I

    move-result v0

    .line 450
    .local v0, "callingUserId":I
    iget-object v1, p0, Lcom/miui/server/SecurityManagerService;->mAccessControlImpl:Lcom/miui/server/security/AccessControlImpl;

    invoke-virtual {v1, p1, p2, v0}, Lcom/miui/server/security/AccessControlImpl;->checkAccessControlPassLocked(Ljava/lang/String;Landroid/content/Intent;I)Z

    move-result v1

    return v1
.end method

.method public checkAccessControlPassAsUser(Ljava/lang/String;Landroid/content/Intent;I)Z
    .locals 1
    .param p1, "packageName"    # Ljava/lang/String;
    .param p2, "intent"    # Landroid/content/Intent;
    .param p3, "userId"    # I

    .line 455
    iget-object v0, p0, Lcom/miui/server/SecurityManagerService;->mAccessControlImpl:Lcom/miui/server/security/AccessControlImpl;

    invoke-virtual {v0, p1, p2, p3}, Lcom/miui/server/security/AccessControlImpl;->checkAccessControlPassLocked(Ljava/lang/String;Landroid/content/Intent;I)Z

    move-result v0

    return v0
.end method

.method public checkAccessControlPassword(Ljava/lang/String;Ljava/lang/String;I)Z
    .locals 1
    .param p1, "passwordType"    # Ljava/lang/String;
    .param p2, "password"    # Ljava/lang/String;
    .param p3, "userId"    # I

    .line 505
    invoke-direct {p0}, Lcom/miui/server/SecurityManagerService;->checkPermission()V

    .line 506
    invoke-static {p3}, Lmiui/security/SecurityManager;->getUserHandle(I)I

    move-result p3

    .line 507
    iget-object v0, p0, Lcom/miui/server/SecurityManagerService;->mAccessController:Lcom/miui/server/AccessController;

    invoke-virtual {v0, p1, p2, p3}, Lcom/miui/server/AccessController;->checkAccessControlPassword(Ljava/lang/String;Ljava/lang/String;I)Z

    move-result v0

    return v0
.end method

.method public checkSmsBlocked(Landroid/content/Intent;)Z
    .locals 2
    .param p1, "intent"    # Landroid/content/Intent;

    .line 239
    const/16 v0, 0x3e8

    const/16 v1, 0x3e9

    filled-new-array {v0, v1}, [I

    move-result-object v0

    invoke-direct {p0, v0}, Lcom/miui/server/SecurityManagerService;->checkPermissionByUid([I)V

    .line 240
    iget-object v0, p0, Lcom/miui/server/SecurityManagerService;->mSecuritySmsHandler:Lcom/miui/server/SecuritySmsHandler;

    invoke-virtual {v0, p1}, Lcom/miui/server/SecuritySmsHandler;->checkSmsBlocked(Landroid/content/Intent;)Z

    move-result v0

    return v0
.end method

.method protected dump(Ljava/io/FileDescriptor;Ljava/io/PrintWriter;[Ljava/lang/String;)V
    .locals 2
    .param p1, "fd"    # Ljava/io/FileDescriptor;
    .param p2, "out"    # Ljava/io/PrintWriter;
    .param p3, "args"    # [Ljava/lang/String;

    .line 147
    iget-object v0, p0, Lcom/miui/server/SecurityManagerService;->mContext:Landroid/content/Context;

    const-string v1, "android.permission.DUMP"

    invoke-virtual {v0, v1}, Landroid/content/Context;->checkCallingOrSelfPermission(Ljava/lang/String;)I

    move-result v0

    if-eqz v0, :cond_0

    .line 149
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "Permission Denial: can\'t dump SecurityManager from from pid="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    .line 150
    invoke-static {}, Landroid/os/Binder;->getCallingPid()I

    move-result v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", uid="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    .line 151
    invoke-static {}, Landroid/os/Binder;->getCallingUid()I

    move-result v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    .line 149
    invoke-virtual {p2, v0}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V

    .line 152
    return-void

    .line 154
    :cond_0
    invoke-static {}, Lmiui/security/WakePathChecker;->getInstance()Lmiui/security/WakePathChecker;

    move-result-object v0

    invoke-virtual {v0, p2}, Lmiui/security/WakePathChecker;->dump(Ljava/io/PrintWriter;)V

    .line 155
    invoke-static {}, Lcom/android/server/net/NetworkManagementServiceStub;->getInstance()Lcom/android/server/net/NetworkManagementServiceStub;

    move-result-object v0

    invoke-interface {v0, p2}, Lcom/android/server/net/NetworkManagementServiceStub;->dump(Ljava/io/PrintWriter;)V

    .line 156
    invoke-direct {p0, p2}, Lcom/miui/server/SecurityManagerService;->dumpPrivacyVirtualDisplay(Ljava/io/PrintWriter;)V

    .line 157
    iget-object v0, p0, Lcom/miui/server/SecurityManagerService;->mAppBehavior:Lcom/miui/server/security/AppBehaviorService;

    invoke-virtual {v0, p2}, Lcom/miui/server/security/AppBehaviorService;->dump(Ljava/io/PrintWriter;)V

    .line 158
    return-void
.end method

.method public exemptByRestrictChain(Ljava/lang/String;Ljava/lang/String;)Z
    .locals 3
    .param p1, "caller"    # Ljava/lang/String;
    .param p2, "callee"    # Ljava/lang/String;

    .line 1381
    iget-object v0, p0, Lcom/miui/server/SecurityManagerService;->restrictChainMaps:Ljava/util/LinkedHashMap;

    invoke-virtual {v0, p1}, Ljava/util/LinkedHashMap;->containsKey(Ljava/lang/Object;)Z

    move-result v0

    const/4 v1, 0x0

    if-nez v0, :cond_0

    .line 1382
    return v1

    .line 1384
    :cond_0
    iget-object v0, p0, Lcom/miui/server/SecurityManagerService;->restrictChainMaps:Ljava/util/LinkedHashMap;

    invoke-virtual {v0, p1}, Ljava/util/LinkedHashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/List;

    .line 1385
    .local v0, "callees":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;"
    if-eqz v0, :cond_1

    invoke-interface {v0, p2}, Ljava/util/List;->contains(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_1

    const/4 v1, 0x1

    :cond_1
    return v1
.end method

.method public exemptTemporarily(Ljava/lang/String;)V
    .locals 1
    .param p1, "packageName"    # Ljava/lang/String;

    .line 1071
    invoke-direct {p0}, Lcom/miui/server/SecurityManagerService;->checkPermission()V

    .line 1072
    invoke-static {}, Lcom/android/server/am/PendingIntentRecordStub;->get()Lcom/android/server/am/PendingIntentRecordStub;

    move-result-object v0

    invoke-interface {v0, p1}, Lcom/android/server/am/PendingIntentRecordStub;->exemptTemporarily(Ljava/lang/String;)V

    .line 1073
    return-void
.end method

.method public finishAccessControl(Ljava/lang/String;I)V
    .locals 1
    .param p1, "packageName"    # Ljava/lang/String;
    .param p2, "userId"    # I

    .line 530
    invoke-direct {p0}, Lcom/miui/server/SecurityManagerService;->checkPermission()V

    .line 531
    iget-object v0, p0, Lcom/miui/server/SecurityManagerService;->mAccessControlImpl:Lcom/miui/server/security/AccessControlImpl;

    invoke-virtual {v0, p1, p2}, Lcom/miui/server/security/AccessControlImpl;->finishAccessControl(Ljava/lang/String;I)V

    .line 532
    return-void
.end method

.method public getAccessControlPasswordType(I)Ljava/lang/String;
    .locals 2
    .param p1, "userId"    # I

    .line 518
    invoke-direct {p0}, Lcom/miui/server/SecurityManagerService;->checkPermission()V

    .line 519
    iget-object v0, p0, Lcom/miui/server/SecurityManagerService;->mAccessController:Lcom/miui/server/AccessController;

    invoke-static {p1}, Lmiui/security/SecurityManager;->getUserHandle(I)I

    move-result v1

    invoke-virtual {v0, v1}, Lcom/miui/server/AccessController;->getAccessControlPasswordType(I)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public getAllGameStorageApps(I)Ljava/util/List;
    .locals 1
    .param p1, "userId"    # I
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(I)",
            "Ljava/util/List<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .line 312
    invoke-direct {p0}, Lcom/miui/server/SecurityManagerService;->checkPermission()V

    .line 313
    iget-object v0, p0, Lcom/miui/server/SecurityManagerService;->mGameBoosterImpl:Lcom/miui/server/security/GameBoosterImpl;

    invoke-virtual {v0, p1}, Lcom/miui/server/security/GameBoosterImpl;->getAllGameStorageApps(I)Ljava/util/List;

    move-result-object v0

    return-object v0
.end method

.method public getAllPrivacyApps(I)Ljava/util/List;
    .locals 10
    .param p1, "userId"    # I
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(I)",
            "Ljava/util/List<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .line 792
    invoke-direct {p0}, Lcom/miui/server/SecurityManagerService;->checkPermission()V

    .line 793
    iget-object v0, p0, Lcom/miui/server/SecurityManagerService;->mUserStateLock:Ljava/lang/Object;

    monitor-enter v0

    .line 794
    :try_start_0
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    .line 795
    .local v1, "privacyAppsList":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;"
    invoke-virtual {p0, p1}, Lcom/miui/server/SecurityManagerService;->getUserStateLocked(I)Lcom/miui/server/security/SecurityUserState;

    move-result-object v2

    .line 796
    .local v2, "userState":Lcom/miui/server/security/SecurityUserState;
    iget-object v3, v2, Lcom/miui/server/security/SecurityUserState;->mPackages:Ljava/util/HashMap;

    .line 797
    .local v3, "packages":Ljava/util/HashMap;, "Ljava/util/HashMap<Ljava/lang/String;Lcom/miui/server/security/SecurityPackageSettings;>;"
    invoke-virtual {v3}, Ljava/util/HashMap;->keySet()Ljava/util/Set;

    move-result-object v4

    .line 798
    .local v4, "pkgNames":Ljava/util/Set;, "Ljava/util/Set<Ljava/lang/String;>;"
    invoke-interface {v4}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v5

    :goto_0
    invoke-interface {v5}, Ljava/util/Iterator;->hasNext()Z

    move-result v6

    if-eqz v6, :cond_1

    invoke-interface {v5}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v6

    check-cast v6, Ljava/lang/String;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 800
    .local v6, "pkgName":Ljava/lang/String;
    :try_start_1
    iget-object v7, v2, Lcom/miui/server/security/SecurityUserState;->mPackages:Ljava/util/HashMap;

    invoke-virtual {p0, v7, v6}, Lcom/miui/server/SecurityManagerService;->getPackageSetting(Ljava/util/HashMap;Ljava/lang/String;)Lcom/miui/server/security/SecurityPackageSettings;

    move-result-object v7

    .line 801
    .local v7, "ps":Lcom/miui/server/security/SecurityPackageSettings;
    iget-boolean v8, v7, Lcom/miui/server/security/SecurityPackageSettings;->isPrivacyApp:Z

    if-eqz v8, :cond_0

    .line 802
    invoke-interface {v1, v6}, Ljava/util/List;->add(Ljava/lang/Object;)Z
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_0
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 806
    .end local v7    # "ps":Lcom/miui/server/security/SecurityPackageSettings;
    :cond_0
    goto :goto_1

    .line 804
    :catch_0
    move-exception v7

    .line 805
    .local v7, "e":Ljava/lang/Exception;
    :try_start_2
    const-string v8, "SecurityManagerService"

    const-string v9, "getAllPrivacyApps error"

    invoke-static {v8, v9, v7}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    .line 807
    .end local v6    # "pkgName":Ljava/lang/String;
    .end local v7    # "e":Ljava/lang/Exception;
    :goto_1
    goto :goto_0

    .line 808
    :cond_1
    monitor-exit v0

    return-object v1

    .line 809
    .end local v1    # "privacyAppsList":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;"
    .end local v2    # "userState":Lcom/miui/server/security/SecurityUserState;
    .end local v3    # "packages":Ljava/util/HashMap;, "Ljava/util/HashMap<Ljava/lang/String;Lcom/miui/server/security/SecurityPackageSettings;>;"
    .end local v4    # "pkgNames":Ljava/util/Set;, "Ljava/util/Set<Ljava/lang/String;>;"
    :catchall_0
    move-exception v1

    monitor-exit v0
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    throw v1
.end method

.method public getAppDarkMode(Ljava/lang/String;)Z
    .locals 2
    .param p1, "packageName"    # Ljava/lang/String;

    .line 321
    invoke-static {}, Landroid/os/UserHandle;->getCallingUserId()I

    move-result v0

    .line 322
    .local v0, "userId":I
    invoke-virtual {p0, p1, v0}, Lcom/miui/server/SecurityManagerService;->getAppDarkModeForUser(Ljava/lang/String;I)Z

    move-result v1

    return v1
.end method

.method public getAppDarkModeForUser(Ljava/lang/String;I)Z
    .locals 1
    .param p1, "packageName"    # Ljava/lang/String;
    .param p2, "userId"    # I

    .line 327
    iget-object v0, p0, Lcom/miui/server/SecurityManagerService;->mInternal:Lmiui/security/SecurityManagerInternal;

    invoke-virtual {v0, p1, p2}, Lmiui/security/SecurityManagerInternal;->getAppDarkModeForUser(Ljava/lang/String;I)Z

    move-result v0

    return v0
.end method

.method public getAppRelaunchModeAfterFolded(Ljava/lang/String;)Z
    .locals 2
    .param p1, "packageName"    # Ljava/lang/String;

    .line 369
    invoke-static {}, Landroid/os/UserHandle;->getCallingUserId()I

    move-result v0

    .line 370
    .local v0, "userId":I
    invoke-virtual {p0, p1, v0}, Lcom/miui/server/SecurityManagerService;->getAppRelaunchModeAfterFoldedForUser(Ljava/lang/String;I)Z

    move-result v1

    return v1
.end method

.method public getAppRelaunchModeAfterFoldedForUser(Ljava/lang/String;I)Z
    .locals 4
    .param p1, "packageName"    # Ljava/lang/String;
    .param p2, "userId"    # I

    .line 375
    iget-object v0, p0, Lcom/miui/server/SecurityManagerService;->mUserStateLock:Ljava/lang/Object;

    monitor-enter v0

    .line 376
    :try_start_0
    invoke-virtual {p0, p2}, Lcom/miui/server/SecurityManagerService;->getUserStateLocked(I)Lcom/miui/server/security/SecurityUserState;

    move-result-object v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 378
    .local v1, "userState":Lcom/miui/server/security/SecurityUserState;
    :try_start_1
    iget-object v2, v1, Lcom/miui/server/security/SecurityUserState;->mPackages:Ljava/util/HashMap;

    invoke-virtual {p0, v2, p1}, Lcom/miui/server/SecurityManagerService;->getPackageSetting(Ljava/util/HashMap;Ljava/lang/String;)Lcom/miui/server/security/SecurityPackageSettings;

    move-result-object v2

    .line 379
    .local v2, "ps":Lcom/miui/server/security/SecurityPackageSettings;
    iget-boolean v3, v2, Lcom/miui/server/security/SecurityPackageSettings;->isRelaunchWhenFolded:Z
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_0
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    :try_start_2
    monitor-exit v0

    return v3

    .line 380
    .end local v2    # "ps":Lcom/miui/server/security/SecurityPackageSettings;
    :catch_0
    move-exception v2

    .line 381
    .local v2, "e":Ljava/lang/Exception;
    monitor-exit v0

    const/4 v0, 0x0

    return v0

    .line 383
    .end local v1    # "userState":Lcom/miui/server/security/SecurityUserState;
    .end local v2    # "e":Ljava/lang/Exception;
    :catchall_0
    move-exception v1

    monitor-exit v0
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    throw v1
.end method

.method public getAppRemindForRelaunch(Ljava/lang/String;)Z
    .locals 2
    .param p1, "packageName"    # Ljava/lang/String;

    .line 340
    invoke-static {}, Landroid/os/UserHandle;->getCallingUserId()I

    move-result v0

    .line 341
    .local v0, "userId":I
    invoke-virtual {p0, p1, v0}, Lcom/miui/server/SecurityManagerService;->getAppRemindForRelaunchForUser(Ljava/lang/String;I)Z

    move-result v1

    return v1
.end method

.method public getAppRemindForRelaunchForUser(Ljava/lang/String;I)Z
    .locals 4
    .param p1, "packageName"    # Ljava/lang/String;
    .param p2, "userId"    # I

    .line 346
    iget-object v0, p0, Lcom/miui/server/SecurityManagerService;->mUserStateLock:Ljava/lang/Object;

    monitor-enter v0

    .line 347
    :try_start_0
    invoke-virtual {p0, p2}, Lcom/miui/server/SecurityManagerService;->getUserStateLocked(I)Lcom/miui/server/security/SecurityUserState;

    move-result-object v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 349
    .local v1, "userState":Lcom/miui/server/security/SecurityUserState;
    :try_start_1
    iget-object v2, v1, Lcom/miui/server/security/SecurityUserState;->mPackages:Ljava/util/HashMap;

    invoke-virtual {p0, v2, p1}, Lcom/miui/server/SecurityManagerService;->getPackageSetting(Ljava/util/HashMap;Ljava/lang/String;)Lcom/miui/server/security/SecurityPackageSettings;

    move-result-object v2

    .line 350
    .local v2, "ps":Lcom/miui/server/security/SecurityPackageSettings;
    iget-boolean v3, v2, Lcom/miui/server/security/SecurityPackageSettings;->isRemindForRelaunch:Z
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_0
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    :try_start_2
    monitor-exit v0

    return v3

    .line 351
    .end local v2    # "ps":Lcom/miui/server/security/SecurityPackageSettings;
    :catch_0
    move-exception v2

    .line 352
    .local v2, "e":Ljava/lang/Exception;
    monitor-exit v0

    const/4 v0, 0x0

    return v0

    .line 354
    .end local v1    # "userState":Lcom/miui/server/security/SecurityUserState;
    .end local v2    # "e":Ljava/lang/Exception;
    :catchall_0
    move-exception v1

    monitor-exit v0
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    throw v1
.end method

.method public getAppRunningControlIBinder()Landroid/os/IBinder;
    .locals 1

    .line 580
    iget-object v0, p0, Lcom/miui/server/SecurityManagerService;->mAppRunningControlService:Lcom/miui/server/AppRunningControlService;

    invoke-virtual {v0}, Lcom/miui/server/AppRunningControlService;->asBinder()Landroid/os/IBinder;

    move-result-object v0

    return-object v0
.end method

.method public getApplicationAccessControlEnabled(Ljava/lang/String;)Z
    .locals 2
    .param p1, "packageName"    # Ljava/lang/String;

    .line 469
    iget-object v0, p0, Lcom/miui/server/SecurityManagerService;->mAccessControlImpl:Lcom/miui/server/security/AccessControlImpl;

    .line 470
    invoke-static {}, Landroid/os/UserHandle;->getCallingUserId()I

    move-result v1

    .line 469
    invoke-virtual {v0, p1, v1}, Lcom/miui/server/security/AccessControlImpl;->getApplicationAccessControlEnabledLocked(Ljava/lang/String;I)Z

    move-result v0

    return v0
.end method

.method public getApplicationAccessControlEnabledAsUser(Ljava/lang/String;I)Z
    .locals 1
    .param p1, "packageName"    # Ljava/lang/String;
    .param p2, "userId"    # I

    .line 460
    iget-object v0, p0, Lcom/miui/server/SecurityManagerService;->mAccessControlImpl:Lcom/miui/server/security/AccessControlImpl;

    invoke-virtual {v0, p1, p2}, Lcom/miui/server/security/AccessControlImpl;->getApplicationAccessControlEnabledLocked(Ljava/lang/String;I)Z

    move-result v0

    return v0
.end method

.method public getApplicationChildrenControlEnabled(Ljava/lang/String;)Z
    .locals 5
    .param p1, "packageName"    # Ljava/lang/String;

    .line 554
    invoke-static {}, Landroid/os/UserHandle;->getCallingUserId()I

    move-result v0

    .line 555
    .local v0, "callingUserId":I
    iget-object v1, p0, Lcom/miui/server/SecurityManagerService;->mUserStateLock:Ljava/lang/Object;

    monitor-enter v1

    .line 557
    :try_start_0
    invoke-virtual {p0, v0}, Lcom/miui/server/SecurityManagerService;->getUserStateLocked(I)Lcom/miui/server/security/SecurityUserState;

    move-result-object v2

    .line 558
    .local v2, "userStateLocked":Lcom/miui/server/security/SecurityUserState;
    iget-object v3, v2, Lcom/miui/server/security/SecurityUserState;->mPackages:Ljava/util/HashMap;

    invoke-virtual {p0, v3, p1}, Lcom/miui/server/SecurityManagerService;->getPackageSetting(Ljava/util/HashMap;Ljava/lang/String;)Lcom/miui/server/security/SecurityPackageSettings;

    move-result-object v3

    .line 559
    .local v3, "ps":Lcom/miui/server/security/SecurityPackageSettings;
    iget-boolean v4, v3, Lcom/miui/server/security/SecurityPackageSettings;->childrenControl:Z
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    :try_start_1
    monitor-exit v1

    return v4

    .line 563
    .end local v2    # "userStateLocked":Lcom/miui/server/security/SecurityUserState;
    .end local v3    # "ps":Lcom/miui/server/security/SecurityPackageSettings;
    :catchall_0
    move-exception v2

    goto :goto_0

    .line 560
    :catch_0
    move-exception v2

    .line 561
    .local v2, "e":Ljava/lang/Exception;
    monitor-exit v1

    const/4 v1, 0x0

    return v1

    .line 563
    .end local v2    # "e":Ljava/lang/Exception;
    :goto_0
    monitor-exit v1
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    throw v2
.end method

.method public getApplicationMaskNotificationEnabledAsUser(Ljava/lang/String;I)Z
    .locals 1
    .param p1, "packageName"    # Ljava/lang/String;
    .param p2, "userId"    # I

    .line 465
    iget-object v0, p0, Lcom/miui/server/SecurityManagerService;->mAccessControlImpl:Lcom/miui/server/security/AccessControlImpl;

    invoke-virtual {v0, p1, p2}, Lcom/miui/server/security/AccessControlImpl;->getApplicationMaskNotificationEnabledLocked(Ljava/lang/String;I)Z

    move-result v0

    return v0
.end method

.method public getBlurryCellInfos(Ljava/util/List;)Ljava/util/List;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List<",
            "Landroid/telephony/CellInfo;",
            ">;)",
            "Ljava/util/List<",
            "Landroid/telephony/CellInfo;",
            ">;"
        }
    .end annotation

    .line 961
    .local p1, "location":Ljava/util/List;, "Ljava/util/List<Landroid/telephony/CellInfo;>;"
    invoke-direct {p0}, Lcom/miui/server/SecurityManagerService;->checkBlurLocationPermission()V

    .line 962
    invoke-static {}, Lcom/android/server/location/MiuiBlurLocationManagerStub;->get()Lcom/android/server/location/MiuiBlurLocationManagerStub;

    move-result-object v0

    invoke-virtual {v0, p1}, Lcom/android/server/location/MiuiBlurLocationManagerStub;->getBlurryCellInfos(Ljava/util/List;)Ljava/util/List;

    move-result-object v0

    return-object v0
.end method

.method public getBlurryCellLocation(Landroid/telephony/CellIdentity;)Landroid/telephony/CellIdentity;
    .locals 1
    .param p1, "location"    # Landroid/telephony/CellIdentity;

    .line 955
    invoke-direct {p0}, Lcom/miui/server/SecurityManagerService;->checkBlurLocationPermission()V

    .line 956
    invoke-static {}, Lcom/android/server/location/MiuiBlurLocationManagerStub;->get()Lcom/android/server/location/MiuiBlurLocationManagerStub;

    move-result-object v0

    invoke-virtual {v0, p1}, Lcom/android/server/location/MiuiBlurLocationManagerStub;->getBlurryCellLocation(Landroid/telephony/CellIdentity;)Landroid/telephony/CellIdentity;

    move-result-object v0

    return-object v0
.end method

.method public getCurrentUserId()I
    .locals 1

    .line 680
    invoke-static {}, Lcom/android/server/am/ProcessUtils;->getCurrentUserId()I

    move-result v0

    return v0
.end method

.method public getGameMode(I)Z
    .locals 1
    .param p1, "userId"    # I

    .line 284
    iget-object v0, p0, Lcom/miui/server/SecurityManagerService;->mGameBoosterImpl:Lcom/miui/server/security/GameBoosterImpl;

    invoke-virtual {v0, p1}, Lcom/miui/server/security/GameBoosterImpl;->getGameMode(I)Z

    move-result v0

    return v0
.end method

.method public getIncompatibleAppList()Ljava/util/List;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .line 722
    iget-object v0, p0, Lcom/miui/server/SecurityManagerService;->mIncompatibleAppList:Ljava/util/ArrayList;

    return-object v0
.end method

.method public getPackageNameByPid(I)Ljava/lang/String;
    .locals 4
    .param p1, "pid"    # I

    .line 1028
    invoke-static {}, Landroid/os/Binder;->getCallingUid()I

    move-result v0

    .line 1029
    .local v0, "callingUid":I
    if-eqz v0, :cond_2

    .line 1030
    invoke-static {v0}, Landroid/os/UserHandle;->getAppId(I)I

    move-result v1

    const/16 v2, 0x3e8

    if-eq v1, v2, :cond_2

    .line 1031
    invoke-static {}, Landroid/os/Binder;->getCallingPid()I

    move-result v1

    invoke-static {v1}, Lcom/android/server/am/ProcessUtils;->getProcessRecordByPid(I)Lcom/android/server/am/ProcessRecord;

    move-result-object v1

    .line 1032
    .local v1, "record":Lcom/android/server/am/ProcessRecord;
    const/4 v2, 0x0

    if-eqz v1, :cond_1

    invoke-virtual {v1}, Lcom/android/server/am/ProcessRecord;->getApplicationInfo()Landroid/content/pm/ApplicationInfo;

    move-result-object v3

    if-nez v3, :cond_0

    goto :goto_0

    .line 1035
    :cond_0
    invoke-virtual {v1}, Lcom/android/server/am/ProcessRecord;->getApplicationInfo()Landroid/content/pm/ApplicationInfo;

    move-result-object v3

    invoke-virtual {v3}, Landroid/content/pm/ApplicationInfo;->isSystemApp()Z

    move-result v3

    if-nez v3, :cond_2

    .line 1036
    return-object v2

    .line 1033
    :cond_1
    :goto_0
    return-object v2

    .line 1039
    .end local v1    # "record":Lcom/android/server/am/ProcessRecord;
    :cond_2
    invoke-static {p1}, Lcom/android/server/am/ProcessUtils;->getPackageNameByPid(I)Ljava/lang/String;

    move-result-object v1

    return-object v1
.end method

.method public getPackageSetting(Ljava/util/HashMap;Ljava/lang/String;)Lcom/miui/server/security/SecurityPackageSettings;
    .locals 2
    .param p2, "packageName"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/HashMap<",
            "Ljava/lang/String;",
            "Lcom/miui/server/security/SecurityPackageSettings;",
            ">;",
            "Ljava/lang/String;",
            ")",
            "Lcom/miui/server/security/SecurityPackageSettings;"
        }
    .end annotation

    .line 1147
    .local p1, "packages":Ljava/util/HashMap;, "Ljava/util/HashMap<Ljava/lang/String;Lcom/miui/server/security/SecurityPackageSettings;>;"
    invoke-virtual {p1, p2}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/miui/server/security/SecurityPackageSettings;

    .line 1148
    .local v0, "ps":Lcom/miui/server/security/SecurityPackageSettings;
    if-nez v0, :cond_0

    .line 1149
    new-instance v1, Lcom/miui/server/security/SecurityPackageSettings;

    invoke-direct {v1, p2}, Lcom/miui/server/security/SecurityPackageSettings;-><init>(Ljava/lang/String;)V

    move-object v0, v1

    .line 1150
    invoke-virtual {p1, p2, v0}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 1152
    :cond_0
    return-object v0
.end method

.method public getPermissionFlagsAsUser(Ljava/lang/String;Ljava/lang/String;I)I
    .locals 3
    .param p1, "permName"    # Ljava/lang/String;
    .param p2, "packageName"    # Ljava/lang/String;
    .param p3, "userId"    # I

    .line 920
    const-string v0, "com.lbe.security.miui"

    invoke-direct {p0, v0}, Lcom/miui/server/SecurityManagerService;->checkGrantPermissionPkg(Ljava/lang/String;)V

    .line 921
    invoke-static {}, Landroid/os/Binder;->clearCallingIdentity()J

    move-result-wide v0

    .line 923
    .local v0, "identity":J
    :try_start_0
    iget-object v2, p0, Lcom/miui/server/SecurityManagerService;->mPermissionManagerService:Lcom/android/server/pm/permission/PermissionManagerService;

    invoke-virtual {v2, p2, p1, p3}, Lcom/android/server/pm/permission/PermissionManagerService;->getPermissionFlags(Ljava/lang/String;Ljava/lang/String;I)I

    move-result v2
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 925
    invoke-static {v0, v1}, Landroid/os/Binder;->restoreCallingIdentity(J)V

    .line 923
    return v2

    .line 925
    :catchall_0
    move-exception v2

    invoke-static {v0, v1}, Landroid/os/Binder;->restoreCallingIdentity(J)V

    .line 926
    throw v2
.end method

.method public getPlatformVAID()Ljava/lang/String;
    .locals 3

    .line 977
    invoke-static {}, Landroid/os/Binder;->getCallingUid()I

    move-result v0

    invoke-static {v0}, Landroid/os/UserHandle;->getAppId(I)I

    move-result v0

    const/16 v1, 0x2710

    if-le v0, v1, :cond_0

    .line 978
    iget-object v0, p0, Lcom/miui/server/SecurityManagerService;->mContext:Landroid/content/Context;

    const-string v1, "com.miui.securitycenter.permission.SYSTEM_PERMISSION_DECLARE"

    const-string v2, "Not allowed get platform vaid from other!"

    invoke-virtual {v0, v1, v2}, Landroid/content/Context;->enforceCallingOrSelfPermission(Ljava/lang/String;Ljava/lang/String;)V

    .line 980
    :cond_0
    invoke-static {}, Landroid/os/Binder;->clearCallingIdentity()J

    move-result-wide v0

    .line 982
    .local v0, "identity":J
    :try_start_0
    iget-object v2, p0, Lcom/miui/server/SecurityManagerService;->mPlatformVAID:Ljava/lang/String;

    invoke-static {v2}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v2

    if-eqz v2, :cond_1

    .line 983
    const-string v2, "/data/system/vaid_persistence_platform"

    invoke-virtual {p0, v2}, Lcom/miui/server/SecurityManagerService;->readSystemDataStringFile(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    iput-object v2, p0, Lcom/miui/server/SecurityManagerService;->mPlatformVAID:Ljava/lang/String;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 986
    :cond_1
    invoke-static {v0, v1}, Landroid/os/Binder;->restoreCallingIdentity(J)V

    .line 987
    nop

    .line 988
    iget-object v2, p0, Lcom/miui/server/SecurityManagerService;->mPlatformVAID:Ljava/lang/String;

    return-object v2

    .line 986
    :catchall_0
    move-exception v2

    invoke-static {v0, v1}, Landroid/os/Binder;->restoreCallingIdentity(J)V

    .line 987
    throw v2
.end method

.method public getSecondSpaceId()I
    .locals 6

    .line 669
    invoke-static {}, Landroid/os/Binder;->clearCallingIdentity()J

    move-result-wide v0

    .line 671
    .local v0, "callingId":J
    :try_start_0
    iget-object v2, p0, Lcom/miui/server/SecurityManagerService;->mContext:Landroid/content/Context;

    invoke-virtual {v2}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v2

    const-string/jumbo v3, "second_user_id"

    const/16 v4, -0x2710

    const/4 v5, 0x0

    invoke-static {v2, v3, v4, v5}, Landroid/provider/Settings$Secure;->getIntForUser(Landroid/content/ContentResolver;Ljava/lang/String;II)I

    move-result v2
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 674
    invoke-static {v0, v1}, Landroid/os/Binder;->restoreCallingIdentity(J)V

    .line 671
    return v2

    .line 674
    :catchall_0
    move-exception v2

    invoke-static {v0, v1}, Landroid/os/Binder;->restoreCallingIdentity(J)V

    .line 675
    throw v2
.end method

.method public getShouldMaskApps()Ljava/lang/String;
    .locals 1

    .line 545
    const/16 v0, 0x3e8

    filled-new-array {v0}, [I

    move-result-object v0

    invoke-direct {p0, v0}, Lcom/miui/server/SecurityManagerService;->checkPermissionByUid([I)V

    .line 546
    iget-object v0, p0, Lcom/miui/server/SecurityManagerService;->mAccessControlImpl:Lcom/miui/server/security/AccessControlImpl;

    invoke-virtual {v0}, Lcom/miui/server/security/AccessControlImpl;->getShouldMaskApps()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public getSimulatedTouchInfo()Ljava/util/Map;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/Map<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .line 1391
    const/16 v0, 0x3e8

    filled-new-array {v0}, [I

    move-result-object v0

    invoke-direct {p0, v0}, Lcom/miui/server/SecurityManagerService;->checkPermissionByUid([I)V

    .line 1392
    invoke-static {}, Lcom/miui/server/security/SafetyDetectManagerStub;->getInstance()Lcom/miui/server/security/SafetyDetectManagerStub;

    move-result-object v0

    invoke-virtual {v0}, Lcom/miui/server/security/SafetyDetectManagerStub;->getSimulatedTouchInfo()Ljava/util/Map;

    move-result-object v0

    return-object v0
.end method

.method public getTopActivity()Landroid/os/IBinder;
    .locals 6

    .line 1051
    invoke-direct {p0}, Lcom/miui/server/SecurityManagerService;->checkPermission()V

    .line 1052
    invoke-static {}, Lcom/android/server/wm/WindowProcessUtils;->getTopRunningActivityInfo()Ljava/util/HashMap;

    move-result-object v0

    .line 1053
    .local v0, "topActivity":Ljava/util/HashMap;, "Ljava/util/HashMap<Ljava/lang/String;Ljava/lang/Object;>;"
    if-eqz v0, :cond_0

    .line 1054
    const-string v1, "intent"

    invoke-virtual {v0, v1}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/content/Intent;

    .line 1055
    .local v1, "intent":Landroid/content/Intent;
    invoke-virtual {v1}, Landroid/content/Intent;->getComponent()Landroid/content/ComponentName;

    move-result-object v2

    .line 1056
    .local v2, "componentName":Landroid/content/ComponentName;
    if-eqz v2, :cond_0

    .line 1057
    invoke-virtual {v2}, Landroid/content/ComponentName;->getClassName()Ljava/lang/String;

    move-result-object v3

    .line 1058
    .local v3, "clsName":Ljava/lang/String;
    invoke-virtual {v2}, Landroid/content/ComponentName;->getPackageName()Ljava/lang/String;

    move-result-object v4

    .line 1059
    .local v4, "pkgName":Ljava/lang/String;
    const-string v5, "com.google.android.packageinstaller"

    invoke-virtual {v5, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-eqz v5, :cond_0

    .line 1060
    const-string v5, "com.android.packageinstaller.PackageInstallerActivity"

    invoke-virtual {v5, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-nez v5, :cond_0

    .line 1061
    const-string/jumbo v5, "token"

    invoke-virtual {v0, v5}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Landroid/os/IBinder;

    return-object v5

    .line 1066
    .end local v1    # "intent":Landroid/content/Intent;
    .end local v2    # "componentName":Landroid/content/ComponentName;
    .end local v3    # "clsName":Ljava/lang/String;
    .end local v4    # "pkgName":Ljava/lang/String;
    :cond_0
    const/4 v1, 0x0

    return-object v1
.end method

.method public getUserStateLocked(I)Lcom/miui/server/security/SecurityUserState;
    .locals 2
    .param p1, "userHandle"    # I

    .line 1137
    iget-object v0, p0, Lcom/miui/server/SecurityManagerService;->mUserStates:Landroid/util/SparseArray;

    invoke-virtual {v0, p1}, Landroid/util/SparseArray;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/miui/server/security/SecurityUserState;

    .line 1138
    .local v0, "userState":Lcom/miui/server/security/SecurityUserState;
    if-nez v0, :cond_0

    .line 1139
    new-instance v1, Lcom/miui/server/security/SecurityUserState;

    invoke-direct {v1}, Lcom/miui/server/security/SecurityUserState;-><init>()V

    move-object v0, v1

    .line 1140
    iput p1, v0, Lcom/miui/server/security/SecurityUserState;->userHandle:I

    .line 1141
    iget-object v1, p0, Lcom/miui/server/SecurityManagerService;->mUserStates:Landroid/util/SparseArray;

    invoke-virtual {v1, p1, v0}, Landroid/util/SparseArray;->put(ILjava/lang/Object;)V

    .line 1143
    :cond_0
    return-object v0
.end method

.method public getWakePathCallListLog()Landroid/content/pm/ParceledListSlice;
    .locals 1

    .line 649
    invoke-direct {p0}, Lcom/miui/server/SecurityManagerService;->checkWakePathPermission()V

    .line 650
    invoke-static {}, Lmiui/security/WakePathChecker;->getInstance()Lmiui/security/WakePathChecker;

    move-result-object v0

    invoke-virtual {v0}, Lmiui/security/WakePathChecker;->getWakePathCallListLog()Landroid/content/pm/ParceledListSlice;

    move-result-object v0

    return-object v0
.end method

.method public getWakeUpTime(Ljava/lang/String;)J
    .locals 3
    .param p1, "componentName"    # Ljava/lang/String;

    .line 599
    iget-object v0, p0, Lcom/miui/server/SecurityManagerService;->mContext:Landroid/content/Context;

    const-string v1, "com.miui.permission.MANAGE_BOOT_TIME"

    const-string v2, "SecurityManagerService"

    invoke-virtual {v0, v1, v2}, Landroid/content/Context;->enforceCallingOrSelfPermission(Ljava/lang/String;Ljava/lang/String;)V

    .line 600
    iget-object v0, p0, Lcom/miui/server/SecurityManagerService;->mWakeUpTimeImpl:Lcom/miui/server/security/WakeUpTimeImpl;

    invoke-virtual {v0, p1}, Lcom/miui/server/security/WakeUpTimeImpl;->getBootTimeFromMap(Ljava/lang/String;)J

    move-result-wide v0

    return-wide v0
.end method

.method public grantRuntimePermissionAsUser(Ljava/lang/String;Ljava/lang/String;I)V
    .locals 3
    .param p1, "packageName"    # Ljava/lang/String;
    .param p2, "permName"    # Ljava/lang/String;
    .param p3, "userId"    # I

    .line 880
    const-string v0, "com.lbe.security.miui"

    invoke-direct {p0, v0}, Lcom/miui/server/SecurityManagerService;->checkGrantPermissionPkg(Ljava/lang/String;)V

    .line 881
    invoke-static {p2, p1, p3}, Landroid/permission/PermissionManager;->checkPackageNamePermission(Ljava/lang/String;Ljava/lang/String;I)I

    move-result v0

    if-nez v0, :cond_0

    .line 882
    return-void

    .line 883
    :cond_0
    invoke-static {}, Landroid/os/Binder;->clearCallingIdentity()J

    move-result-wide v0

    .line 885
    .local v0, "identity":J
    :try_start_0
    iget-object v2, p0, Lcom/miui/server/SecurityManagerService;->mPermissionManagerService:Lcom/android/server/pm/permission/PermissionManagerService;

    invoke-virtual {v2, p1, p2, p3}, Lcom/android/server/pm/permission/PermissionManagerService;->grantRuntimePermission(Ljava/lang/String;Ljava/lang/String;I)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 887
    invoke-static {v0, v1}, Landroid/os/Binder;->restoreCallingIdentity(J)V

    .line 888
    nop

    .line 889
    return-void

    .line 887
    :catchall_0
    move-exception v2

    invoke-static {v0, v1}, Landroid/os/Binder;->restoreCallingIdentity(J)V

    .line 888
    throw v2
.end method

.method public hasAppBehaviorWatching()Z
    .locals 1

    .line 1326
    const-string v0, "com.miui.securitycenter"

    invoke-direct {p0, v0}, Lcom/miui/server/SecurityManagerService;->checkGrantPermissionPkg(Ljava/lang/String;)V

    .line 1327
    iget-object v0, p0, Lcom/miui/server/SecurityManagerService;->mAppBehavior:Lcom/miui/server/security/AppBehaviorService;

    invoke-virtual {v0}, Lcom/miui/server/security/AppBehaviorService;->hasAppBehaviorWatching()Z

    move-result v0

    return v0
.end method

.method public haveAccessControlPassword(I)Z
    .locals 1
    .param p1, "userId"    # I

    .line 512
    invoke-static {p1}, Lmiui/security/SecurityManager;->getUserHandle(I)I

    move-result p1

    .line 513
    iget-object v0, p0, Lcom/miui/server/SecurityManagerService;->mAccessController:Lcom/miui/server/AccessController;

    invoke-virtual {v0, p1}, Lcom/miui/server/AccessController;->haveAccessControlPassword(I)Z

    move-result v0

    return v0
.end method

.method public isAllowStartService(Landroid/content/Intent;I)Z
    .locals 2
    .param p1, "service"    # Landroid/content/Intent;
    .param p2, "userId"    # I

    .line 1044
    invoke-direct {p0}, Lcom/miui/server/SecurityManagerService;->checkPermission()V

    .line 1045
    invoke-static {}, Lcom/android/server/am/AutoStartManagerServiceStub;->getInstance()Lcom/android/server/am/AutoStartManagerServiceStub;

    move-result-object v0

    iget-object v1, p0, Lcom/miui/server/SecurityManagerService;->mContext:Landroid/content/Context;

    .line 1046
    invoke-interface {v0, v1, p1, p2}, Lcom/android/server/am/AutoStartManagerServiceStub;->isAllowStartService(Landroid/content/Context;Landroid/content/Intent;I)Z

    move-result v0

    .line 1045
    return v0
.end method

.method public isAppPrivacyEnabled(Ljava/lang/String;)Z
    .locals 2
    .param p1, "packageName"    # Ljava/lang/String;

    .line 753
    invoke-static {p1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 756
    sget-object v0, Ljava/lang/Boolean;->TRUE:Ljava/lang/Boolean;

    new-instance v1, Lcom/miui/server/SecurityManagerService$$ExternalSyntheticLambda2;

    invoke-direct {v1, p0, p1}, Lcom/miui/server/SecurityManagerService$$ExternalSyntheticLambda2;-><init>(Lcom/miui/server/SecurityManagerService;Ljava/lang/String;)V

    invoke-static {v1}, Landroid/os/Binder;->withCleanCallingIdentity(Lcom/android/internal/util/FunctionalUtils$ThrowingSupplier;)Ljava/lang/Object;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/Boolean;->equals(Ljava/lang/Object;)Z

    move-result v0

    return v0

    .line 754
    :cond_0
    new-instance v0, Ljava/lang/RuntimeException;

    const-string v1, "packageName can not be null or empty"

    invoke-direct {v0, v1}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    throw v0
.end method

.method public isGameStorageApp(Ljava/lang/String;I)Z
    .locals 1
    .param p1, "packageName"    # Ljava/lang/String;
    .param p2, "userId"    # I

    .line 301
    invoke-direct {p0}, Lcom/miui/server/SecurityManagerService;->checkPermission()V

    .line 302
    iget-object v0, p0, Lcom/miui/server/SecurityManagerService;->mGameBoosterImpl:Lcom/miui/server/security/GameBoosterImpl;

    invoke-virtual {v0, p1, p2}, Lcom/miui/server/security/GameBoosterImpl;->isGameStorageApp(Ljava/lang/String;I)Z

    move-result v0

    return v0
.end method

.method public isPrivacyApp(Ljava/lang/String;I)Z
    .locals 5
    .param p1, "packageName"    # Ljava/lang/String;
    .param p2, "userId"    # I

    .line 777
    invoke-direct {p0}, Lcom/miui/server/SecurityManagerService;->checkPermission()V

    .line 778
    iget-object v0, p0, Lcom/miui/server/SecurityManagerService;->mUserStateLock:Ljava/lang/Object;

    monitor-enter v0

    .line 779
    :try_start_0
    invoke-virtual {p0, p2}, Lcom/miui/server/SecurityManagerService;->getUserStateLocked(I)Lcom/miui/server/security/SecurityUserState;

    move-result-object v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 781
    .local v1, "userState":Lcom/miui/server/security/SecurityUserState;
    :try_start_1
    iget-object v2, v1, Lcom/miui/server/security/SecurityUserState;->mPackages:Ljava/util/HashMap;

    invoke-virtual {p0, v2, p1}, Lcom/miui/server/SecurityManagerService;->getPackageSetting(Ljava/util/HashMap;Ljava/lang/String;)Lcom/miui/server/security/SecurityPackageSettings;

    move-result-object v2

    .line 782
    .local v2, "ps":Lcom/miui/server/security/SecurityPackageSettings;
    iget-boolean v3, v2, Lcom/miui/server/security/SecurityPackageSettings;->isPrivacyApp:Z
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_0
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    :try_start_2
    monitor-exit v0

    return v3

    .line 783
    .end local v2    # "ps":Lcom/miui/server/security/SecurityPackageSettings;
    :catch_0
    move-exception v2

    .line 784
    .local v2, "e":Ljava/lang/Exception;
    const-string v3, "SecurityManagerService"

    const-string v4, "isPrivacyApp error"

    invoke-static {v3, v4, v2}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    .line 785
    monitor-exit v0

    const/4 v0, 0x0

    return v0

    .line 787
    .end local v1    # "userState":Lcom/miui/server/security/SecurityUserState;
    .end local v2    # "e":Ljava/lang/Exception;
    :catchall_0
    move-exception v1

    monitor-exit v0
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    throw v1
.end method

.method public isScRelaunchNeedConfirm(Ljava/lang/String;)Z
    .locals 2
    .param p1, "packageName"    # Ljava/lang/String;

    .line 398
    invoke-static {}, Landroid/os/UserHandle;->getCallingUserId()I

    move-result v0

    .line 399
    .local v0, "userId":I
    invoke-virtual {p0, p1, v0}, Lcom/miui/server/SecurityManagerService;->isScRelaunchNeedConfirmForUser(Ljava/lang/String;I)Z

    move-result v1

    return v1
.end method

.method public isScRelaunchNeedConfirmForUser(Ljava/lang/String;I)Z
    .locals 4
    .param p1, "packageName"    # Ljava/lang/String;
    .param p2, "userId"    # I

    .line 404
    iget-object v0, p0, Lcom/miui/server/SecurityManagerService;->mUserStateLock:Ljava/lang/Object;

    monitor-enter v0

    .line 405
    :try_start_0
    invoke-virtual {p0, p2}, Lcom/miui/server/SecurityManagerService;->getUserStateLocked(I)Lcom/miui/server/security/SecurityUserState;

    move-result-object v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 407
    .local v1, "userState":Lcom/miui/server/security/SecurityUserState;
    :try_start_1
    iget-object v2, v1, Lcom/miui/server/security/SecurityUserState;->mPackages:Ljava/util/HashMap;

    invoke-virtual {p0, v2, p1}, Lcom/miui/server/SecurityManagerService;->getPackageSetting(Ljava/util/HashMap;Ljava/lang/String;)Lcom/miui/server/security/SecurityPackageSettings;

    move-result-object v2

    .line 408
    .local v2, "ps":Lcom/miui/server/security/SecurityPackageSettings;
    iget-boolean v3, v2, Lcom/miui/server/security/SecurityPackageSettings;->isScRelaunchConfirm:Z
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_0
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    :try_start_2
    monitor-exit v0

    return v3

    .line 409
    .end local v2    # "ps":Lcom/miui/server/security/SecurityPackageSettings;
    :catch_0
    move-exception v2

    .line 410
    .local v2, "e":Ljava/lang/Exception;
    monitor-exit v0

    const/4 v0, 0x0

    return v0

    .line 412
    .end local v1    # "userState":Lcom/miui/server/security/SecurityUserState;
    .end local v2    # "e":Ljava/lang/Exception;
    :catchall_0
    move-exception v1

    monitor-exit v0
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    throw v1
.end method

.method public needFinishAccessControl(Landroid/os/IBinder;)Z
    .locals 1
    .param p1, "token"    # Landroid/os/IBinder;

    .line 524
    invoke-direct {p0}, Lcom/miui/server/SecurityManagerService;->checkPermission()V

    .line 525
    iget-object v0, p0, Lcom/miui/server/SecurityManagerService;->mAccessControlImpl:Lcom/miui/server/security/AccessControlImpl;

    invoke-virtual {v0, p1}, Lcom/miui/server/security/AccessControlImpl;->needFinishAccessControl(Landroid/os/IBinder;)Z

    move-result v0

    return v0
.end method

.method public persistAppBehavior()V
    .locals 1

    .line 1314
    const-string v0, "com.miui.securitycenter"

    invoke-direct {p0, v0}, Lcom/miui/server/SecurityManagerService;->checkGrantPermissionPkg(Ljava/lang/String;)V

    .line 1315
    iget-object v0, p0, Lcom/miui/server/SecurityManagerService;->mAppBehavior:Lcom/miui/server/security/AppBehaviorService;

    invoke-virtual {v0}, Lcom/miui/server/security/AppBehaviorService;->persistBehaviorRecord()V

    .line 1316
    return-void
.end method

.method public pushPrivacyVirtualDisplayList(Ljava/util/List;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List<",
            "Ljava/lang/String;",
            ">;)V"
        }
    .end annotation

    .line 996
    .local p1, "privacyList":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;"
    invoke-direct {p0}, Lcom/miui/server/SecurityManagerService;->checkPermission()V

    .line 997
    iget-object v0, p0, Lcom/miui/server/SecurityManagerService;->mPrivacyVirtualDisplay:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->clear()V

    .line 998
    iget-object v0, p0, Lcom/miui/server/SecurityManagerService;->mPrivacyVirtualDisplay:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->addAll(Ljava/util/Collection;)Z

    .line 999
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "privacy virtual display updated! size="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lcom/miui/server/SecurityManagerService;->mPrivacyVirtualDisplay:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->size()I

    move-result v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    const-string v1, "SecurityManagerService"

    invoke-static {v1, v0}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 1000
    return-void
.end method

.method public pushUpdatePkgsData(Ljava/util/List;Z)V
    .locals 1
    .param p2, "enable"    # Z
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List<",
            "Ljava/lang/String;",
            ">;Z)V"
        }
    .end annotation

    .line 613
    .local p1, "updatePkgsList":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;"
    invoke-direct {p0}, Lcom/miui/server/SecurityManagerService;->checkWakePathPermission()V

    .line 614
    invoke-static {}, Lmiui/security/WakePathChecker;->getInstance()Lmiui/security/WakePathChecker;

    move-result-object v0

    invoke-virtual {v0, p1, p2}, Lmiui/security/WakePathChecker;->pushUpdatePkgsData(Ljava/util/List;Z)V

    .line 615
    return-void
.end method

.method public pushWakePathConfirmDialogWhiteList(ILjava/util/List;)V
    .locals 1
    .param p1, "type"    # I
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(I",
            "Ljava/util/List<",
            "Ljava/lang/String;",
            ">;)V"
        }
    .end annotation

    .line 631
    .local p2, "whiteList":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;"
    invoke-direct {p0}, Lcom/miui/server/SecurityManagerService;->checkWakePathPermission()V

    .line 632
    invoke-static {}, Lmiui/security/WakePathChecker;->getInstance()Lmiui/security/WakePathChecker;

    move-result-object v0

    invoke-virtual {v0, p1, p2}, Lmiui/security/WakePathChecker;->pushWakePathConfirmDialogWhiteList(ILjava/util/List;)V

    .line 633
    return-void
.end method

.method public pushWakePathData(ILandroid/content/pm/ParceledListSlice;I)V
    .locals 2
    .param p1, "wakeType"    # I
    .param p2, "wakePathRuleInfos"    # Landroid/content/pm/ParceledListSlice;
    .param p3, "userId"    # I

    .line 619
    invoke-direct {p0}, Lcom/miui/server/SecurityManagerService;->checkWakePathPermission()V

    .line 620
    invoke-static {}, Lmiui/security/WakePathChecker;->getInstance()Lmiui/security/WakePathChecker;

    move-result-object v0

    invoke-virtual {p2}, Landroid/content/pm/ParceledListSlice;->getList()Ljava/util/List;

    move-result-object v1

    invoke-virtual {v0, p1, v1, p3}, Lmiui/security/WakePathChecker;->pushWakePathRuleInfos(ILjava/util/List;I)V

    .line 621
    return-void
.end method

.method public pushWakePathWhiteList(Ljava/util/List;I)V
    .locals 1
    .param p2, "userId"    # I
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List<",
            "Ljava/lang/String;",
            ">;I)V"
        }
    .end annotation

    .line 625
    .local p1, "wakePathWhiteList":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;"
    invoke-direct {p0}, Lcom/miui/server/SecurityManagerService;->checkWakePathPermission()V

    .line 626
    invoke-static {}, Lmiui/security/WakePathChecker;->getInstance()Lmiui/security/WakePathChecker;

    move-result-object v0

    invoke-virtual {v0, p1, p2}, Lmiui/security/WakePathChecker;->pushWakePathWhiteList(Ljava/util/List;I)V

    .line 627
    return-void
.end method

.method public putSystemDataStringFile(Ljava/lang/String;Ljava/lang/String;)Z
    .locals 4
    .param p1, "path"    # Ljava/lang/String;
    .param p2, "value"    # Ljava/lang/String;

    .line 1078
    const/16 v0, 0x3e8

    filled-new-array {v0}, [I

    move-result-object v0

    invoke-direct {p0, v0}, Lcom/miui/server/SecurityManagerService;->checkPermissionByUid([I)V

    .line 1079
    new-instance v0, Ljava/io/File;

    invoke-direct {v0, p1}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 1080
    .local v0, "file":Ljava/io/File;
    const/4 v1, 0x0

    .line 1081
    .local v1, "raf":Ljava/io/RandomAccessFile;
    invoke-virtual {v0}, Ljava/io/File;->exists()Z

    move-result v2

    if-nez v2, :cond_0

    .line 1083
    :try_start_0
    invoke-virtual {v0}, Ljava/io/File;->createNewFile()Z
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    .line 1086
    goto :goto_0

    .line 1084
    :catch_0
    move-exception v2

    .line 1085
    .local v2, "e":Ljava/io/IOException;
    invoke-virtual {v2}, Ljava/io/IOException;->printStackTrace()V

    .line 1089
    .end local v2    # "e":Ljava/io/IOException;
    :cond_0
    :goto_0
    :try_start_1
    new-instance v2, Ljava/io/RandomAccessFile;

    const-string v3, "rw"

    invoke-direct {v2, v0, v3}, Ljava/io/RandomAccessFile;-><init>(Ljava/io/File;Ljava/lang/String;)V

    move-object v1, v2

    .line 1090
    const-wide/16 v2, 0x0

    invoke-virtual {v1, v2, v3}, Ljava/io/RandomAccessFile;->setLength(J)V

    .line 1091
    invoke-virtual {v1, p2}, Ljava/io/RandomAccessFile;->writeUTF(Ljava/lang/String;)V
    :try_end_1
    .catch Ljava/io/IOException; {:try_start_1 .. :try_end_1} :catch_2
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 1092
    nop

    .line 1096
    nop

    .line 1098
    :try_start_2
    invoke-virtual {v1}, Ljava/io/RandomAccessFile;->close()V
    :try_end_2
    .catch Ljava/io/IOException; {:try_start_2 .. :try_end_2} :catch_1

    .line 1101
    goto :goto_1

    .line 1099
    :catch_1
    move-exception v2

    .line 1100
    .restart local v2    # "e":Ljava/io/IOException;
    invoke-virtual {v2}, Ljava/io/IOException;->printStackTrace()V

    .line 1092
    .end local v2    # "e":Ljava/io/IOException;
    :goto_1
    const/4 v2, 0x1

    return v2

    .line 1096
    :catchall_0
    move-exception v2

    goto :goto_4

    .line 1093
    :catch_2
    move-exception v2

    .line 1094
    .restart local v2    # "e":Ljava/io/IOException;
    :try_start_3
    invoke-virtual {v2}, Ljava/io/IOException;->printStackTrace()V
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    .line 1096
    .end local v2    # "e":Ljava/io/IOException;
    if-eqz v1, :cond_1

    .line 1098
    :try_start_4
    invoke-virtual {v1}, Ljava/io/RandomAccessFile;->close()V
    :try_end_4
    .catch Ljava/io/IOException; {:try_start_4 .. :try_end_4} :catch_3

    .line 1101
    :goto_2
    goto :goto_3

    .line 1099
    :catch_3
    move-exception v2

    .line 1100
    .restart local v2    # "e":Ljava/io/IOException;
    invoke-virtual {v2}, Ljava/io/IOException;->printStackTrace()V

    .end local v2    # "e":Ljava/io/IOException;
    goto :goto_2

    .line 1104
    :cond_1
    :goto_3
    const/4 v2, 0x0

    return v2

    .line 1096
    :goto_4
    if-eqz v1, :cond_2

    .line 1098
    :try_start_5
    invoke-virtual {v1}, Ljava/io/RandomAccessFile;->close()V
    :try_end_5
    .catch Ljava/io/IOException; {:try_start_5 .. :try_end_5} :catch_4

    .line 1101
    goto :goto_5

    .line 1099
    :catch_4
    move-exception v3

    .line 1100
    .local v3, "e":Ljava/io/IOException;
    invoke-virtual {v3}, Ljava/io/IOException;->printStackTrace()V

    .line 1103
    .end local v3    # "e":Ljava/io/IOException;
    :cond_2
    :goto_5
    throw v2
.end method

.method public readSystemDataStringFile(Ljava/lang/String;)Ljava/lang/String;
    .locals 5
    .param p1, "path"    # Ljava/lang/String;

    .line 1110
    const/16 v0, 0x3e8

    filled-new-array {v0}, [I

    move-result-object v0

    invoke-direct {p0, v0}, Lcom/miui/server/SecurityManagerService;->checkPermissionByUid([I)V

    .line 1111
    new-instance v0, Ljava/io/File;

    invoke-direct {v0, p1}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 1112
    .local v0, "file":Ljava/io/File;
    const/4 v1, 0x0

    .line 1113
    .local v1, "raf":Ljava/io/RandomAccessFile;
    const/4 v2, 0x0

    .line 1114
    .local v2, "result":Ljava/lang/String;
    invoke-virtual {v0}, Ljava/io/File;->exists()Z

    move-result v3

    if-eqz v3, :cond_1

    .line 1116
    :try_start_0
    new-instance v3, Ljava/io/RandomAccessFile;

    const-string v4, "r"

    invoke-direct {v3, v0, v4}, Ljava/io/RandomAccessFile;-><init>(Ljava/io/File;Ljava/lang/String;)V

    move-object v1, v3

    .line 1117
    invoke-virtual {v1}, Ljava/io/RandomAccessFile;->readUTF()Ljava/lang/String;

    move-result-object v3
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_1
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-object v2, v3

    .line 1121
    nop

    .line 1123
    :try_start_1
    invoke-virtual {v1}, Ljava/io/RandomAccessFile;->close()V
    :try_end_1
    .catch Ljava/io/IOException; {:try_start_1 .. :try_end_1} :catch_0

    .line 1126
    :goto_0
    goto :goto_3

    .line 1124
    :catch_0
    move-exception v3

    .line 1125
    .local v3, "e":Ljava/io/IOException;
    invoke-virtual {v3}, Ljava/io/IOException;->printStackTrace()V

    .end local v3    # "e":Ljava/io/IOException;
    goto :goto_0

    .line 1121
    :catchall_0
    move-exception v3

    goto :goto_1

    .line 1118
    :catch_1
    move-exception v3

    .line 1119
    .restart local v3    # "e":Ljava/io/IOException;
    :try_start_2
    invoke-virtual {v3}, Ljava/io/IOException;->printStackTrace()V
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    .line 1121
    .end local v3    # "e":Ljava/io/IOException;
    if-eqz v1, :cond_1

    .line 1123
    :try_start_3
    invoke-virtual {v1}, Ljava/io/RandomAccessFile;->close()V
    :try_end_3
    .catch Ljava/io/IOException; {:try_start_3 .. :try_end_3} :catch_0

    goto :goto_0

    .line 1121
    :goto_1
    if-eqz v1, :cond_0

    .line 1123
    :try_start_4
    invoke-virtual {v1}, Ljava/io/RandomAccessFile;->close()V
    :try_end_4
    .catch Ljava/io/IOException; {:try_start_4 .. :try_end_4} :catch_2

    .line 1126
    goto :goto_2

    .line 1124
    :catch_2
    move-exception v4

    .line 1125
    .local v4, "e":Ljava/io/IOException;
    invoke-virtual {v4}, Ljava/io/IOException;->printStackTrace()V

    .line 1128
    .end local v4    # "e":Ljava/io/IOException;
    :cond_0
    :goto_2
    throw v3

    .line 1130
    :cond_1
    :goto_3
    return-object v2
.end method

.method public recordAppBehavior(Lmiui/security/AppBehavior;)V
    .locals 1
    .param p1, "appBehavior"    # Lmiui/security/AppBehavior;

    .line 1306
    const-string v0, "com.miui.securitycenter"

    invoke-direct {p0, v0}, Lcom/miui/server/SecurityManagerService;->checkGrantPermissionPkg(Ljava/lang/String;)V

    .line 1307
    iget-object v0, p0, Lcom/miui/server/SecurityManagerService;->mAppBehavior:Lcom/miui/server/security/AppBehaviorService;

    invoke-virtual {v0}, Lcom/miui/server/security/AppBehaviorService;->hasAppBehaviorWatching()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1308
    iget-object v0, p0, Lcom/miui/server/SecurityManagerService;->mAppBehavior:Lcom/miui/server/security/AppBehaviorService;

    invoke-virtual {v0, p1}, Lcom/miui/server/security/AppBehaviorService;->recordAppBehaviorAsync(Lmiui/security/AppBehavior;)V

    .line 1310
    :cond_0
    return-void
.end method

.method public registerLocationBlurryManager(Lcom/android/internal/app/ILocationBlurry;)V
    .locals 1
    .param p1, "manager"    # Lcom/android/internal/app/ILocationBlurry;

    .line 949
    invoke-direct {p0}, Lcom/miui/server/SecurityManagerService;->checkWakePathPermission()V

    .line 950
    invoke-static {}, Lcom/android/server/location/MiuiBlurLocationManagerStub;->get()Lcom/android/server/location/MiuiBlurLocationManagerStub;

    move-result-object v0

    invoke-virtual {v0, p1}, Lcom/android/server/location/MiuiBlurLocationManagerStub;->registerLocationBlurryManager(Lcom/android/internal/app/ILocationBlurry;)V

    .line 951
    return-void
.end method

.method public registerWakePathCallback(Lcom/android/internal/app/IWakePathCallback;)V
    .locals 1
    .param p1, "callback"    # Lcom/android/internal/app/IWakePathCallback;

    .line 655
    invoke-direct {p0}, Lcom/miui/server/SecurityManagerService;->checkWakePathPermission()V

    .line 656
    invoke-static {}, Lmiui/security/WakePathChecker;->getInstance()Lmiui/security/WakePathChecker;

    move-result-object v0

    invoke-virtual {v0, p1}, Lmiui/security/WakePathChecker;->registerWakePathCallback(Lcom/android/internal/app/IWakePathCallback;)V

    .line 657
    return-void
.end method

.method public removeAccessControlPass(Ljava/lang/String;)V
    .locals 2
    .param p1, "packageName"    # Ljava/lang/String;

    .line 442
    invoke-direct {p0}, Lcom/miui/server/SecurityManagerService;->checkPermission()V

    .line 443
    invoke-static {}, Landroid/os/UserHandle;->getCallingUserId()I

    move-result v0

    .line 444
    .local v0, "callingUserId":I
    iget-object v1, p0, Lcom/miui/server/SecurityManagerService;->mAccessControlImpl:Lcom/miui/server/security/AccessControlImpl;

    invoke-virtual {v1, p1, v0}, Lcom/miui/server/security/AccessControlImpl;->removeAccessControlPassAsUser(Ljava/lang/String;I)V

    .line 445
    return-void
.end method

.method public removeAccessControlPassAsUser(Ljava/lang/String;I)V
    .locals 1
    .param p1, "packageName"    # Ljava/lang/String;
    .param p2, "userId"    # I

    .line 492
    invoke-direct {p0}, Lcom/miui/server/SecurityManagerService;->checkPermission()V

    .line 493
    iget-object v0, p0, Lcom/miui/server/SecurityManagerService;->mAccessControlImpl:Lcom/miui/server/security/AccessControlImpl;

    invoke-virtual {v0, p1, p2}, Lcom/miui/server/security/AccessControlImpl;->removeAccessControlPassAsUser(Ljava/lang/String;I)V

    .line 494
    return-void
.end method

.method public removeCalleeRestrictChain(Ljava/lang/String;)V
    .locals 5
    .param p1, "callee"    # Ljava/lang/String;

    .line 1365
    invoke-static {p1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1366
    return-void

    .line 1368
    :cond_0
    iget-object v0, p0, Lcom/miui/server/SecurityManagerService;->restrictChainMaps:Ljava/util/LinkedHashMap;

    invoke-virtual {v0}, Ljava/util/LinkedHashMap;->entrySet()Ljava/util/Set;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v0

    :goto_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_3

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/util/Map$Entry;

    .line 1369
    .local v1, "next":Ljava/util/Map$Entry;, "Ljava/util/Map$Entry<Ljava/lang/String;Ljava/util/List<Ljava/lang/String;>;>;"
    invoke-interface {v1}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/util/List;

    .line 1370
    .local v2, "callees":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;"
    const/4 v3, 0x0

    .local v3, "i":I
    :goto_1
    invoke-interface {v2}, Ljava/util/List;->size()I

    move-result v4

    if-ge v3, v4, :cond_2

    .line 1371
    invoke-interface {v2, v3}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v4

    invoke-virtual {p1, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_1

    .line 1372
    invoke-interface {v2, p1}, Ljava/util/List;->remove(Ljava/lang/Object;)Z

    .line 1373
    goto :goto_2

    .line 1370
    :cond_1
    add-int/lit8 v3, v3, 0x1

    goto :goto_1

    .line 1376
    .end local v1    # "next":Ljava/util/Map$Entry;, "Ljava/util/Map$Entry<Ljava/lang/String;Ljava/util/List<Ljava/lang/String;>;>;"
    .end local v2    # "callees":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;"
    .end local v3    # "i":I
    :cond_2
    :goto_2
    goto :goto_0

    .line 1377
    :cond_3
    return-void
.end method

.method public removeWakePathData(I)V
    .locals 1
    .param p1, "userId"    # I

    .line 637
    invoke-direct {p0}, Lcom/miui/server/SecurityManagerService;->checkWakePathPermission()V

    .line 638
    invoke-static {}, Lmiui/security/WakePathChecker;->getInstance()Lmiui/security/WakePathChecker;

    move-result-object v0

    invoke-virtual {v0, p1}, Lmiui/security/WakePathChecker;->removeWakePathData(I)V

    .line 639
    return-void
.end method

.method public revokeRuntimePermissionAsUser(Ljava/lang/String;Ljava/lang/String;I)V
    .locals 4
    .param p1, "packageName"    # Ljava/lang/String;
    .param p2, "permName"    # Ljava/lang/String;
    .param p3, "userId"    # I

    .line 893
    const-string v0, "com.lbe.security.miui"

    invoke-direct {p0, v0}, Lcom/miui/server/SecurityManagerService;->checkGrantPermissionPkg(Ljava/lang/String;)V

    .line 894
    invoke-static {p2, p1, p3}, Landroid/permission/PermissionManager;->checkPackageNamePermission(Ljava/lang/String;Ljava/lang/String;I)I

    move-result v0

    if-eqz v0, :cond_0

    .line 895
    return-void

    .line 896
    :cond_0
    invoke-static {}, Landroid/os/Binder;->clearCallingIdentity()J

    move-result-wide v0

    .line 898
    .local v0, "identity":J
    :try_start_0
    iget-object v2, p0, Lcom/miui/server/SecurityManagerService;->mPermissionManagerService:Lcom/android/server/pm/permission/PermissionManagerService;

    const/4 v3, 0x0

    invoke-virtual {v2, p1, p2, p3, v3}, Lcom/android/server/pm/permission/PermissionManagerService;->revokeRuntimePermission(Ljava/lang/String;Ljava/lang/String;ILjava/lang/String;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 900
    invoke-static {v0, v1}, Landroid/os/Binder;->restoreCallingIdentity(J)V

    .line 901
    nop

    .line 902
    return-void

    .line 900
    :catchall_0
    move-exception v2

    invoke-static {v0, v1}, Landroid/os/Binder;->restoreCallingIdentity(J)V

    .line 901
    throw v2
.end method

.method public revokeRuntimePermissionAsUserNotKill(Ljava/lang/String;Ljava/lang/String;I)V
    .locals 4
    .param p1, "packageName"    # Ljava/lang/String;
    .param p2, "permName"    # Ljava/lang/String;
    .param p3, "userId"    # I

    .line 906
    const-string v0, "com.lbe.security.miui"

    invoke-direct {p0, v0}, Lcom/miui/server/SecurityManagerService;->checkGrantPermissionPkg(Ljava/lang/String;)V

    .line 907
    invoke-static {p2, p1, p3}, Landroid/permission/PermissionManager;->checkPackageNamePermission(Ljava/lang/String;Ljava/lang/String;I)I

    move-result v0

    if-eqz v0, :cond_0

    .line 908
    return-void

    .line 909
    :cond_0
    invoke-static {}, Landroid/os/Binder;->clearCallingIdentity()J

    move-result-wide v0

    .line 911
    .local v0, "identity":J
    :try_start_0
    iget-object v2, p0, Lcom/miui/server/SecurityManagerService;->mPermissionManagerService:Lcom/android/server/pm/permission/PermissionManagerService;

    const-string v3, "not kill"

    invoke-virtual {v2, p1, p2, p3, v3}, Lcom/android/server/pm/permission/PermissionManagerService;->revokeRuntimePermission(Ljava/lang/String;Ljava/lang/String;ILjava/lang/String;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 914
    invoke-static {v0, v1}, Landroid/os/Binder;->restoreCallingIdentity(J)V

    .line 915
    nop

    .line 916
    return-void

    .line 914
    :catchall_0
    move-exception v2

    invoke-static {v0, v1}, Landroid/os/Binder;->restoreCallingIdentity(J)V

    .line 915
    throw v2
.end method

.method public scheduleWriteSettings()V
    .locals 4

    .line 1156
    iget-object v0, p0, Lcom/miui/server/SecurityManagerService;->mSecurityWriteHandler:Lcom/miui/server/security/SecurityWriteHandler;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Lcom/miui/server/security/SecurityWriteHandler;->hasMessages(I)Z

    move-result v0

    if-eqz v0, :cond_0

    return-void

    .line 1157
    :cond_0
    iget-object v0, p0, Lcom/miui/server/SecurityManagerService;->mSecurityWriteHandler:Lcom/miui/server/security/SecurityWriteHandler;

    const-wide/16 v2, 0x3e8

    invoke-virtual {v0, v1, v2, v3}, Lcom/miui/server/security/SecurityWriteHandler;->sendEmptyMessageDelayed(IJ)Z

    .line 1158
    return-void
.end method

.method public setAccessControlPassword(Ljava/lang/String;Ljava/lang/String;I)V
    .locals 1
    .param p1, "passwordType"    # Ljava/lang/String;
    .param p2, "password"    # Ljava/lang/String;
    .param p3, "userId"    # I

    .line 498
    invoke-direct {p0}, Lcom/miui/server/SecurityManagerService;->checkPermission()V

    .line 499
    invoke-static {p3}, Lmiui/security/SecurityManager;->getUserHandle(I)I

    move-result p3

    .line 500
    iget-object v0, p0, Lcom/miui/server/SecurityManagerService;->mAccessController:Lcom/miui/server/AccessController;

    invoke-virtual {v0, p1, p2, p3}, Lcom/miui/server/AccessController;->setAccessControlPassword(Ljava/lang/String;Ljava/lang/String;I)V

    .line 501
    return-void
.end method

.method public setAppDarkModeForUser(Ljava/lang/String;ZI)V
    .locals 1
    .param p1, "packageName"    # Ljava/lang/String;
    .param p2, "enabled"    # Z
    .param p3, "userId"    # I

    .line 332
    iget-object v0, p0, Lcom/miui/server/SecurityManagerService;->mInternal:Lmiui/security/SecurityManagerInternal;

    invoke-virtual {v0, p1, p2, p3}, Lmiui/security/SecurityManagerInternal;->setAppDarkModeForUser(Ljava/lang/String;ZI)V

    .line 333
    return-void
.end method

.method public setAppPrivacyStatus(Ljava/lang/String;Z)V
    .locals 2
    .param p1, "packageName"    # Ljava/lang/String;
    .param p2, "isOpen"    # Z

    .line 737
    invoke-static {p1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_1

    .line 740
    invoke-static {}, Landroid/os/Binder;->getCallingUid()I

    move-result v0

    invoke-static {v0}, Landroid/os/UserHandle;->getAppId(I)I

    move-result v0

    const/16 v1, 0x3e8

    if-eq v0, v1, :cond_0

    .line 741
    invoke-static {}, Landroid/os/Binder;->getCallingPid()I

    move-result v0

    invoke-static {v0}, Lcom/android/server/am/ProcessUtils;->getPackageNameByPid(I)Ljava/lang/String;

    move-result-object v0

    .line 742
    .local v0, "callingPackage":Ljava/lang/String;
    invoke-virtual {p1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_0

    .line 743
    invoke-direct {p0}, Lcom/miui/server/SecurityManagerService;->checkWriteSecurePermission()V

    .line 746
    .end local v0    # "callingPackage":Ljava/lang/String;
    :cond_0
    new-instance v0, Lcom/miui/server/SecurityManagerService$$ExternalSyntheticLambda3;

    invoke-direct {v0, p0, p1, p2}, Lcom/miui/server/SecurityManagerService$$ExternalSyntheticLambda3;-><init>(Lcom/miui/server/SecurityManagerService;Ljava/lang/String;Z)V

    invoke-static {v0}, Landroid/os/Binder;->withCleanCallingIdentity(Lcom/android/internal/util/FunctionalUtils$ThrowingSupplier;)Ljava/lang/Object;

    .line 749
    return-void

    .line 738
    :cond_1
    new-instance v0, Ljava/lang/RuntimeException;

    const-string v1, "packageName can not be null or empty"

    invoke-direct {v0, v1}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    throw v0
.end method

.method public setAppRelaunchModeAfterFoldedForUser(Ljava/lang/String;ZI)V
    .locals 3
    .param p1, "packageName"    # Ljava/lang/String;
    .param p2, "enabled"    # Z
    .param p3, "userId"    # I

    .line 388
    iget-object v0, p0, Lcom/miui/server/SecurityManagerService;->mUserStateLock:Ljava/lang/Object;

    monitor-enter v0

    .line 389
    :try_start_0
    invoke-virtual {p0, p3}, Lcom/miui/server/SecurityManagerService;->getUserStateLocked(I)Lcom/miui/server/security/SecurityUserState;

    move-result-object v1

    .line 390
    .local v1, "userStateLocked":Lcom/miui/server/security/SecurityUserState;
    iget-object v2, v1, Lcom/miui/server/security/SecurityUserState;->mPackages:Ljava/util/HashMap;

    invoke-virtual {p0, v2, p1}, Lcom/miui/server/SecurityManagerService;->getPackageSetting(Ljava/util/HashMap;Ljava/lang/String;)Lcom/miui/server/security/SecurityPackageSettings;

    move-result-object v2

    .line 391
    .local v2, "ps":Lcom/miui/server/security/SecurityPackageSettings;
    iput-boolean p2, v2, Lcom/miui/server/security/SecurityPackageSettings;->isRelaunchWhenFolded:Z

    .line 392
    invoke-virtual {p0}, Lcom/miui/server/SecurityManagerService;->scheduleWriteSettings()V

    .line 393
    .end local v1    # "userStateLocked":Lcom/miui/server/security/SecurityUserState;
    .end local v2    # "ps":Lcom/miui/server/security/SecurityPackageSettings;
    monitor-exit v0

    .line 394
    return-void

    .line 393
    :catchall_0
    move-exception v1

    monitor-exit v0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v1
.end method

.method public setAppRemindForRelaunchForUser(Ljava/lang/String;ZI)V
    .locals 3
    .param p1, "packageName"    # Ljava/lang/String;
    .param p2, "enabled"    # Z
    .param p3, "userId"    # I

    .line 359
    iget-object v0, p0, Lcom/miui/server/SecurityManagerService;->mUserStateLock:Ljava/lang/Object;

    monitor-enter v0

    .line 360
    :try_start_0
    invoke-virtual {p0, p3}, Lcom/miui/server/SecurityManagerService;->getUserStateLocked(I)Lcom/miui/server/security/SecurityUserState;

    move-result-object v1

    .line 361
    .local v1, "userStateLocked":Lcom/miui/server/security/SecurityUserState;
    iget-object v2, v1, Lcom/miui/server/security/SecurityUserState;->mPackages:Ljava/util/HashMap;

    invoke-virtual {p0, v2, p1}, Lcom/miui/server/SecurityManagerService;->getPackageSetting(Ljava/util/HashMap;Ljava/lang/String;)Lcom/miui/server/security/SecurityPackageSettings;

    move-result-object v2

    .line 362
    .local v2, "ps":Lcom/miui/server/security/SecurityPackageSettings;
    iput-boolean p2, v2, Lcom/miui/server/security/SecurityPackageSettings;->isRemindForRelaunch:Z

    .line 363
    invoke-virtual {p0}, Lcom/miui/server/SecurityManagerService;->scheduleWriteSettings()V

    .line 364
    .end local v1    # "userStateLocked":Lcom/miui/server/security/SecurityUserState;
    .end local v2    # "ps":Lcom/miui/server/security/SecurityPackageSettings;
    monitor-exit v0

    .line 365
    return-void

    .line 364
    :catchall_0
    move-exception v1

    monitor-exit v0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v1
.end method

.method public setApplicationAccessControlEnabled(Ljava/lang/String;Z)V
    .locals 1
    .param p1, "packageName"    # Ljava/lang/String;
    .param p2, "enabled"    # Z

    .line 475
    invoke-direct {p0}, Lcom/miui/server/SecurityManagerService;->checkPermission()V

    .line 476
    invoke-static {}, Landroid/os/UserHandle;->getCallingUserId()I

    move-result v0

    invoke-virtual {p0, p1, p2, v0}, Lcom/miui/server/SecurityManagerService;->setApplicationAccessControlEnabledForUser(Ljava/lang/String;ZI)V

    .line 477
    return-void
.end method

.method public setApplicationAccessControlEnabledForUser(Ljava/lang/String;ZI)V
    .locals 1
    .param p1, "packageName"    # Ljava/lang/String;
    .param p2, "enabled"    # Z
    .param p3, "userId"    # I

    .line 481
    invoke-direct {p0}, Lcom/miui/server/SecurityManagerService;->checkPermission()V

    .line 482
    iget-object v0, p0, Lcom/miui/server/SecurityManagerService;->mAccessControlImpl:Lcom/miui/server/security/AccessControlImpl;

    invoke-virtual {v0, p1, p2, p3}, Lcom/miui/server/security/AccessControlImpl;->setApplicationAccessControlEnabledForUser(Ljava/lang/String;ZI)V

    .line 483
    return-void
.end method

.method public setApplicationChildrenControlEnabled(Ljava/lang/String;Z)V
    .locals 4
    .param p1, "packageName"    # Ljava/lang/String;
    .param p2, "enabled"    # Z

    .line 568
    invoke-direct {p0}, Lcom/miui/server/SecurityManagerService;->checkPermission()V

    .line 569
    invoke-static {}, Landroid/os/UserHandle;->getCallingUserId()I

    move-result v0

    .line 570
    .local v0, "callingUserId":I
    iget-object v1, p0, Lcom/miui/server/SecurityManagerService;->mUserStateLock:Ljava/lang/Object;

    monitor-enter v1

    .line 571
    :try_start_0
    invoke-virtual {p0, v0}, Lcom/miui/server/SecurityManagerService;->getUserStateLocked(I)Lcom/miui/server/security/SecurityUserState;

    move-result-object v2

    .line 572
    .local v2, "userStateLocked":Lcom/miui/server/security/SecurityUserState;
    iget-object v3, v2, Lcom/miui/server/security/SecurityUserState;->mPackages:Ljava/util/HashMap;

    invoke-virtual {p0, v3, p1}, Lcom/miui/server/SecurityManagerService;->getPackageSetting(Ljava/util/HashMap;Ljava/lang/String;)Lcom/miui/server/security/SecurityPackageSettings;

    move-result-object v3

    .line 573
    .local v3, "ps":Lcom/miui/server/security/SecurityPackageSettings;
    iput-boolean p2, v3, Lcom/miui/server/security/SecurityPackageSettings;->childrenControl:Z

    .line 574
    invoke-virtual {p0}, Lcom/miui/server/SecurityManagerService;->scheduleWriteSettings()V

    .line 575
    .end local v2    # "userStateLocked":Lcom/miui/server/security/SecurityUserState;
    .end local v3    # "ps":Lcom/miui/server/security/SecurityPackageSettings;
    monitor-exit v1

    .line 576
    return-void

    .line 575
    :catchall_0
    move-exception v2

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v2
.end method

.method public setApplicationMaskNotificationEnabledForUser(Ljava/lang/String;ZI)V
    .locals 1
    .param p1, "packageName"    # Ljava/lang/String;
    .param p2, "enabled"    # Z
    .param p3, "userId"    # I

    .line 486
    invoke-direct {p0}, Lcom/miui/server/SecurityManagerService;->checkPermission()V

    .line 487
    iget-object v0, p0, Lcom/miui/server/SecurityManagerService;->mAccessControlImpl:Lcom/miui/server/security/AccessControlImpl;

    invoke-virtual {v0, p1, p2, p3}, Lcom/miui/server/security/AccessControlImpl;->setApplicationMaskNotificationEnabledForUser(Ljava/lang/String;ZI)V

    .line 488
    return-void
.end method

.method public setCurrentNetworkState(I)Z
    .locals 1
    .param p1, "state"    # I

    .line 269
    const/16 v0, 0x3e8

    filled-new-array {v0}, [I

    move-result-object v0

    invoke-direct {p0, v0}, Lcom/miui/server/SecurityManagerService;->checkPermissionByUid([I)V

    .line 270
    invoke-static {}, Lcom/android/server/net/NetworkManagementServiceStub;->getInstance()Lcom/android/server/net/NetworkManagementServiceStub;

    move-result-object v0

    invoke-interface {v0, p1}, Lcom/android/server/net/NetworkManagementServiceStub;->setCurrentNetworkState(I)Z

    move-result v0

    return v0
.end method

.method public setGameBoosterIBinder(Landroid/os/IBinder;IZ)V
    .locals 1
    .param p1, "gameBooster"    # Landroid/os/IBinder;
    .param p2, "userId"    # I
    .param p3, "isGameMode"    # Z

    .line 278
    invoke-direct {p0}, Lcom/miui/server/SecurityManagerService;->checkPermission()V

    .line 279
    iget-object v0, p0, Lcom/miui/server/SecurityManagerService;->mGameBoosterImpl:Lcom/miui/server/security/GameBoosterImpl;

    invoke-virtual {v0, p1, p2, p3}, Lcom/miui/server/security/GameBoosterImpl;->setGameBoosterIBinder(Landroid/os/IBinder;IZ)V

    .line 280
    return-void
.end method

.method public setGameStorageApp(Ljava/lang/String;IZ)V
    .locals 1
    .param p1, "packageName"    # Ljava/lang/String;
    .param p2, "userId"    # I
    .param p3, "isStorage"    # Z

    .line 292
    invoke-direct {p0}, Lcom/miui/server/SecurityManagerService;->checkPermission()V

    .line 293
    iget-object v0, p0, Lcom/miui/server/SecurityManagerService;->mGameBoosterImpl:Lcom/miui/server/security/GameBoosterImpl;

    invoke-virtual {v0, p1, p2, p3}, Lcom/miui/server/security/GameBoosterImpl;->setGameStorageApp(Ljava/lang/String;IZ)V

    .line 294
    return-void
.end method

.method public setIncompatibleAppList(Ljava/util/List;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List<",
            "Ljava/lang/String;",
            ">;)V"
        }
    .end annotation

    .line 710
    .local p1, "list":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;"
    invoke-direct {p0}, Lcom/miui/server/SecurityManagerService;->checkPermission()V

    .line 711
    if-eqz p1, :cond_0

    .line 714
    iget-object v0, p0, Lcom/miui/server/SecurityManagerService;->mIncompatibleAppList:Ljava/util/ArrayList;

    monitor-enter v0

    .line 715
    :try_start_0
    iget-object v1, p0, Lcom/miui/server/SecurityManagerService;->mIncompatibleAppList:Ljava/util/ArrayList;

    invoke-virtual {v1}, Ljava/util/ArrayList;->clear()V

    .line 716
    iget-object v1, p0, Lcom/miui/server/SecurityManagerService;->mIncompatibleAppList:Ljava/util/ArrayList;

    invoke-virtual {v1, p1}, Ljava/util/ArrayList;->addAll(Ljava/util/Collection;)Z

    .line 717
    monitor-exit v0

    .line 718
    return-void

    .line 717
    :catchall_0
    move-exception v1

    monitor-exit v0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v1

    .line 712
    :cond_0
    new-instance v0, Ljava/lang/NullPointerException;

    const-string v1, "List is null"

    invoke-direct {v0, v1}, Ljava/lang/NullPointerException;-><init>(Ljava/lang/String;)V

    throw v0
.end method

.method public setMiuiFirewallRule(Ljava/lang/String;III)Z
    .locals 1
    .param p1, "packageName"    # Ljava/lang/String;
    .param p2, "uid"    # I
    .param p3, "rule"    # I
    .param p4, "type"    # I

    .line 263
    const/16 v0, 0x3e8

    filled-new-array {v0}, [I

    move-result-object v0

    invoke-direct {p0, v0}, Lcom/miui/server/SecurityManagerService;->checkPermissionByUid([I)V

    .line 264
    invoke-static {}, Lcom/android/server/net/NetworkManagementServiceStub;->getInstance()Lcom/android/server/net/NetworkManagementServiceStub;

    move-result-object v0

    invoke-interface {v0, p1, p2, p3, p4}, Lcom/android/server/net/NetworkManagementServiceStub;->setMiuiFirewallRule(Ljava/lang/String;III)Z

    move-result v0

    return v0
.end method

.method public setNotificationsEnabledForPackage(Ljava/lang/String;IZ)V
    .locals 3
    .param p1, "packageName"    # Ljava/lang/String;
    .param p2, "uid"    # I
    .param p3, "enabled"    # Z
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    .line 866
    invoke-direct {p0}, Lcom/miui/server/SecurityManagerService;->checkPermission()V

    .line 867
    iget-object v0, p0, Lcom/miui/server/SecurityManagerService;->mINotificationManager:Landroid/app/INotificationManager;

    if-nez v0, :cond_0

    .line 868
    const-string v0, "notification"

    invoke-static {v0}, Landroid/os/ServiceManager;->getService(Ljava/lang/String;)Landroid/os/IBinder;

    move-result-object v0

    invoke-static {v0}, Landroid/app/INotificationManager$Stub;->asInterface(Landroid/os/IBinder;)Landroid/app/INotificationManager;

    move-result-object v0

    iput-object v0, p0, Lcom/miui/server/SecurityManagerService;->mINotificationManager:Landroid/app/INotificationManager;

    .line 870
    :cond_0
    invoke-static {}, Landroid/os/Binder;->clearCallingIdentity()J

    move-result-wide v0

    .line 872
    .local v0, "identity":J
    :try_start_0
    iget-object v2, p0, Lcom/miui/server/SecurityManagerService;->mINotificationManager:Landroid/app/INotificationManager;

    invoke-interface {v2, p1, p2, p3}, Landroid/app/INotificationManager;->setNotificationsEnabledForPackage(Ljava/lang/String;IZ)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 874
    invoke-static {v0, v1}, Landroid/os/Binder;->restoreCallingIdentity(J)V

    .line 875
    nop

    .line 876
    return-void

    .line 874
    :catchall_0
    move-exception v2

    invoke-static {v0, v1}, Landroid/os/Binder;->restoreCallingIdentity(J)V

    .line 875
    throw v2
.end method

.method public setPrivacyApp(Ljava/lang/String;IZ)V
    .locals 3
    .param p1, "packageName"    # Ljava/lang/String;
    .param p2, "userId"    # I
    .param p3, "isPrivacy"    # Z

    .line 766
    invoke-direct {p0}, Lcom/miui/server/SecurityManagerService;->checkPermission()V

    .line 767
    iget-object v0, p0, Lcom/miui/server/SecurityManagerService;->mUserStateLock:Ljava/lang/Object;

    monitor-enter v0

    .line 768
    :try_start_0
    invoke-virtual {p0, p2}, Lcom/miui/server/SecurityManagerService;->getUserStateLocked(I)Lcom/miui/server/security/SecurityUserState;

    move-result-object v1

    .line 769
    .local v1, "userStateLocked":Lcom/miui/server/security/SecurityUserState;
    iget-object v2, v1, Lcom/miui/server/security/SecurityUserState;->mPackages:Ljava/util/HashMap;

    invoke-virtual {p0, v2, p1}, Lcom/miui/server/SecurityManagerService;->getPackageSetting(Ljava/util/HashMap;Ljava/lang/String;)Lcom/miui/server/security/SecurityPackageSettings;

    move-result-object v2

    .line 770
    .local v2, "ps":Lcom/miui/server/security/SecurityPackageSettings;
    iput-boolean p3, v2, Lcom/miui/server/security/SecurityPackageSettings;->isPrivacyApp:Z

    .line 771
    invoke-virtual {p0}, Lcom/miui/server/SecurityManagerService;->scheduleWriteSettings()V

    .line 772
    .end local v1    # "userStateLocked":Lcom/miui/server/security/SecurityUserState;
    .end local v2    # "ps":Lcom/miui/server/security/SecurityPackageSettings;
    monitor-exit v0

    .line 773
    return-void

    .line 772
    :catchall_0
    move-exception v1

    monitor-exit v0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v1
.end method

.method public setScRelaunchNeedConfirmForUser(Ljava/lang/String;ZI)V
    .locals 3
    .param p1, "packageName"    # Ljava/lang/String;
    .param p2, "confirm"    # Z
    .param p3, "userId"    # I

    .line 417
    iget-object v0, p0, Lcom/miui/server/SecurityManagerService;->mUserStateLock:Ljava/lang/Object;

    monitor-enter v0

    .line 418
    :try_start_0
    invoke-virtual {p0, p3}, Lcom/miui/server/SecurityManagerService;->getUserStateLocked(I)Lcom/miui/server/security/SecurityUserState;

    move-result-object v1

    .line 419
    .local v1, "userStateLocked":Lcom/miui/server/security/SecurityUserState;
    iget-object v2, v1, Lcom/miui/server/security/SecurityUserState;->mPackages:Ljava/util/HashMap;

    invoke-virtual {p0, v2, p1}, Lcom/miui/server/SecurityManagerService;->getPackageSetting(Ljava/util/HashMap;Ljava/lang/String;)Lcom/miui/server/security/SecurityPackageSettings;

    move-result-object v2

    .line 420
    .local v2, "ps":Lcom/miui/server/security/SecurityPackageSettings;
    iput-boolean p2, v2, Lcom/miui/server/security/SecurityPackageSettings;->isScRelaunchConfirm:Z

    .line 421
    invoke-virtual {p0}, Lcom/miui/server/SecurityManagerService;->scheduleWriteSettings()V

    .line 422
    .end local v1    # "userStateLocked":Lcom/miui/server/security/SecurityUserState;
    .end local v2    # "ps":Lcom/miui/server/security/SecurityPackageSettings;
    monitor-exit v0

    .line 423
    return-void

    .line 422
    :catchall_0
    move-exception v1

    monitor-exit v0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v1
.end method

.method public setTrackWakePathCallListLogEnabled(Z)V
    .locals 1
    .param p1, "enabled"    # Z

    .line 643
    invoke-direct {p0}, Lcom/miui/server/SecurityManagerService;->checkWakePathPermission()V

    .line 644
    invoke-static {}, Lmiui/security/WakePathChecker;->getInstance()Lmiui/security/WakePathChecker;

    move-result-object v0

    invoke-virtual {v0, p1}, Lmiui/security/WakePathChecker;->setTrackWakePathCallListLogEnabled(Z)V

    .line 645
    return-void
.end method

.method public setWakeUpTime(Ljava/lang/String;J)V
    .locals 3
    .param p1, "componentName"    # Ljava/lang/String;
    .param p2, "timeInSeconds"    # J

    .line 593
    iget-object v0, p0, Lcom/miui/server/SecurityManagerService;->mContext:Landroid/content/Context;

    const-string v1, "com.miui.permission.MANAGE_BOOT_TIME"

    const-string v2, "SecurityManagerService"

    invoke-virtual {v0, v1, v2}, Landroid/content/Context;->enforceCallingOrSelfPermission(Ljava/lang/String;Ljava/lang/String;)V

    .line 594
    iget-object v0, p0, Lcom/miui/server/SecurityManagerService;->mWakeUpTimeImpl:Lcom/miui/server/security/WakeUpTimeImpl;

    invoke-virtual {v0, p1, p2, p3}, Lcom/miui/server/security/WakeUpTimeImpl;->setWakeUpTime(Ljava/lang/String;J)V

    .line 595
    return-void
.end method

.method public startInterceptSmsBySender(Ljava/lang/String;Ljava/lang/String;I)Z
    .locals 1
    .param p1, "pkgName"    # Ljava/lang/String;
    .param p2, "sender"    # Ljava/lang/String;
    .param p3, "count"    # I

    .line 245
    const/16 v0, 0x3e8

    filled-new-array {v0}, [I

    move-result-object v0

    invoke-direct {p0, v0}, Lcom/miui/server/SecurityManagerService;->checkPermissionByUid([I)V

    .line 246
    iget-object v0, p0, Lcom/miui/server/SecurityManagerService;->mSecuritySmsHandler:Lcom/miui/server/SecuritySmsHandler;

    invoke-virtual {v0, p1, p2, p3}, Lcom/miui/server/SecuritySmsHandler;->startInterceptSmsBySender(Ljava/lang/String;Ljava/lang/String;I)Z

    move-result v0

    return v0
.end method

.method public startWatchingAppBehavior(IZ)V
    .locals 1
    .param p1, "behavior"    # I
    .param p2, "includeSystem"    # Z

    .line 1282
    const-string v0, "com.miui.securitycenter"

    invoke-direct {p0, v0}, Lcom/miui/server/SecurityManagerService;->checkGrantPermissionPkg(Ljava/lang/String;)V

    .line 1283
    iget-object v0, p0, Lcom/miui/server/SecurityManagerService;->mAppBehavior:Lcom/miui/server/security/AppBehaviorService;

    invoke-virtual {v0, p1, p2}, Lcom/miui/server/security/AppBehaviorService;->startWatchingAppBehavior(IZ)V

    .line 1284
    return-void
.end method

.method public startWatchingAppBehaviors(I[Ljava/lang/String;Z)V
    .locals 1
    .param p1, "behavior"    # I
    .param p2, "pkgNames"    # [Ljava/lang/String;
    .param p3, "includeSystem"    # Z

    .line 1288
    const-string v0, "com.miui.securitycenter"

    invoke-direct {p0, v0}, Lcom/miui/server/SecurityManagerService;->checkGrantPermissionPkg(Ljava/lang/String;)V

    .line 1289
    iget-object v0, p0, Lcom/miui/server/SecurityManagerService;->mAppBehavior:Lcom/miui/server/security/AppBehaviorService;

    invoke-virtual {v0, p1, p2, p3}, Lcom/miui/server/security/AppBehaviorService;->startWatchingAppBehavior(I[Ljava/lang/String;Z)V

    .line 1290
    return-void
.end method

.method public stopInterceptSmsBySender()Z
    .locals 1

    .line 251
    const/16 v0, 0x3e8

    filled-new-array {v0}, [I

    move-result-object v0

    invoke-direct {p0, v0}, Lcom/miui/server/SecurityManagerService;->checkPermissionByUid([I)V

    .line 252
    iget-object v0, p0, Lcom/miui/server/SecurityManagerService;->mSecuritySmsHandler:Lcom/miui/server/SecuritySmsHandler;

    invoke-virtual {v0}, Lcom/miui/server/SecuritySmsHandler;->stopInterceptSmsBySender()Z

    move-result v0

    return v0
.end method

.method public stopWatchingAppBehavior(I)V
    .locals 1
    .param p1, "behavior"    # I

    .line 1294
    const-string v0, "com.miui.securitycenter"

    invoke-direct {p0, v0}, Lcom/miui/server/SecurityManagerService;->checkGrantPermissionPkg(Ljava/lang/String;)V

    .line 1295
    iget-object v0, p0, Lcom/miui/server/SecurityManagerService;->mAppBehavior:Lcom/miui/server/security/AppBehaviorService;

    invoke-virtual {v0, p1}, Lcom/miui/server/security/AppBehaviorService;->stopWatchingAppBehavior(I)V

    .line 1296
    return-void
.end method

.method public stopWatchingAppBehaviors(I[Ljava/lang/String;)V
    .locals 1
    .param p1, "behavior"    # I
    .param p2, "pkgNames"    # [Ljava/lang/String;

    .line 1300
    const-string v0, "com.miui.securitycenter"

    invoke-direct {p0, v0}, Lcom/miui/server/SecurityManagerService;->checkGrantPermissionPkg(Ljava/lang/String;)V

    .line 1301
    iget-object v0, p0, Lcom/miui/server/SecurityManagerService;->mAppBehavior:Lcom/miui/server/security/AppBehaviorService;

    invoke-virtual {v0, p1, p2}, Lcom/miui/server/security/AppBehaviorService;->stopWatchingAppBehavior(I[Ljava/lang/String;)V

    .line 1302
    return-void
.end method

.method public switchSimulatedTouchDetect(Ljava/util/Map;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/Map<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;)V"
        }
    .end annotation

    .line 1397
    .local p1, "config":Ljava/util/Map;, "Ljava/util/Map<Ljava/lang/String;Ljava/lang/String;>;"
    const/16 v0, 0x3e8

    filled-new-array {v0}, [I

    move-result-object v0

    invoke-direct {p0, v0}, Lcom/miui/server/SecurityManagerService;->checkPermissionByUid([I)V

    .line 1398
    invoke-static {}, Lcom/miui/server/security/SafetyDetectManagerStub;->getInstance()Lcom/miui/server/security/SafetyDetectManagerStub;

    move-result-object v0

    invoke-virtual {v0, p1}, Lcom/miui/server/security/SafetyDetectManagerStub;->switchSimulatedTouchDetect(Ljava/util/Map;)V

    .line 1399
    return-void
.end method

.method public updateAppWidgetVisibility(Ljava/lang/String;IZLjava/util/List;Ljava/lang/String;)V
    .locals 6
    .param p1, "packageName"    # Ljava/lang/String;
    .param p2, "uid"    # I
    .param p3, "visible"    # Z
    .param p5, "reason"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "IZ",
            "Ljava/util/List<",
            "Ljava/lang/String;",
            ">;",
            "Ljava/lang/String;",
            ")V"
        }
    .end annotation

    .line 1404
    .local p4, "permissions":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;"
    iget-object v0, p0, Lcom/miui/server/SecurityManagerService;->mContext:Landroid/content/Context;

    const-string v1, "android.permission.BIND_APPWIDGET"

    invoke-virtual {v0, v1, p5}, Landroid/content/Context;->enforceCallingOrSelfPermission(Ljava/lang/String;Ljava/lang/String;)V

    .line 1405
    if-eqz p4, :cond_4

    const-string v0, "android.permission.RECORD_AUDIO"

    invoke-interface {p4, v0}, Ljava/util/List;->contains(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_4

    .line 1406
    const-string v0, "android.permission.CAMERA"

    invoke-interface {p4, v0}, Ljava/util/List;->contains(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_4

    .line 1407
    const-string v0, "android.permission.ACCESS_FINE_LOCATION"

    invoke-interface {p4, v0}, Ljava/util/List;->contains(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    goto/16 :goto_0

    .line 1410
    :cond_0
    iget-object v0, p0, Lcom/miui/server/SecurityManagerService;->mAppOpsInternal:Landroid/app/AppOpsManagerInternal;

    if-nez v0, :cond_1

    .line 1411
    const-class v0, Landroid/app/AppOpsManagerInternal;

    invoke-static {v0}, Lcom/android/server/LocalServices;->getService(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/app/AppOpsManagerInternal;

    iput-object v0, p0, Lcom/miui/server/SecurityManagerService;->mAppOpsInternal:Landroid/app/AppOpsManagerInternal;

    .line 1413
    :cond_1
    new-instance v0, Landroid/util/SparseArray;

    invoke-direct {v0}, Landroid/util/SparseArray;-><init>()V

    .line 1414
    .local v0, "uidPackageNames":Landroid/util/SparseArray;, "Landroid/util/SparseArray<Ljava/lang/String;>;"
    invoke-virtual {v0, p2, p1}, Landroid/util/SparseArray;->put(ILjava/lang/Object;)V

    .line 1415
    iget-object v1, p0, Lcom/miui/server/SecurityManagerService;->mAppOpsInternal:Landroid/app/AppOpsManagerInternal;

    invoke-virtual {v1, v0, p3}, Landroid/app/AppOpsManagerInternal;->updateAppWidgetVisibility(Landroid/util/SparseArray;Z)V

    .line 1416
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v2, "update appwidget of uid: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " package: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " visible: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p3}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " reason: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    const-string v2, "SecurityManagerService"

    invoke-static {v2, v1}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 1418
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v1, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/String;->hashCode()I

    move-result v1

    .line 1419
    .local v1, "what":I
    iget-object v2, p0, Lcom/miui/server/SecurityManagerService;->mSecurityWriteHandler:Lcom/miui/server/security/SecurityWriteHandler;

    invoke-virtual {v2, v1}, Lcom/miui/server/security/SecurityWriteHandler;->hasMessages(I)Z

    move-result v2

    if-eqz v2, :cond_2

    .line 1420
    iget-object v2, p0, Lcom/miui/server/SecurityManagerService;->mSecurityWriteHandler:Lcom/miui/server/security/SecurityWriteHandler;

    invoke-virtual {v2, v1}, Lcom/miui/server/security/SecurityWriteHandler;->removeMessages(I)V

    .line 1422
    :cond_2
    if-eqz p3, :cond_3

    .line 1423
    iget-object v2, p0, Lcom/miui/server/SecurityManagerService;->mSecurityWriteHandler:Lcom/miui/server/security/SecurityWriteHandler;

    invoke-static {v2, v1}, Landroid/os/Message;->obtain(Landroid/os/Handler;I)Landroid/os/Message;

    move-result-object v2

    .line 1424
    .local v2, "msg":Landroid/os/Message;
    new-instance v3, Lcom/miui/server/SecurityManagerService$$ExternalSyntheticLambda0;

    invoke-direct {v3, p0, v0}, Lcom/miui/server/SecurityManagerService$$ExternalSyntheticLambda0;-><init>(Lcom/miui/server/SecurityManagerService;Landroid/util/SparseArray;)V

    invoke-virtual {v2, v3}, Landroid/os/Message;->setCallback(Ljava/lang/Runnable;)Landroid/os/Message;

    .line 1425
    iget-object v3, p0, Lcom/miui/server/SecurityManagerService;->mSecurityWriteHandler:Lcom/miui/server/security/SecurityWriteHandler;

    const-wide/16 v4, 0x2710

    invoke-virtual {v3, v2, v4, v5}, Lcom/miui/server/security/SecurityWriteHandler;->sendMessageDelayed(Landroid/os/Message;J)Z

    .line 1427
    .end local v2    # "msg":Landroid/os/Message;
    :cond_3
    return-void

    .line 1408
    .end local v0    # "uidPackageNames":Landroid/util/SparseArray;, "Landroid/util/SparseArray<Ljava/lang/String;>;"
    .end local v1    # "what":I
    :cond_4
    :goto_0
    return-void
.end method

.method public updateBehaviorThreshold(IJ)V
    .locals 1
    .param p1, "behavior"    # I
    .param p2, "threshold"    # J

    .line 1320
    const-string v0, "com.miui.securitycenter"

    invoke-direct {p0, v0}, Lcom/miui/server/SecurityManagerService;->checkGrantPermissionPkg(Ljava/lang/String;)V

    .line 1321
    iget-object v0, p0, Lcom/miui/server/SecurityManagerService;->mAppBehavior:Lcom/miui/server/security/AppBehaviorService;

    invoke-virtual {v0, p1, p2, p3}, Lcom/miui/server/security/AppBehaviorService;->updateBehaviorThreshold(IJ)V

    .line 1322
    return-void
.end method

.method public updateLauncherPackageNames()V
    .locals 2

    .line 661
    invoke-static {}, Lmiui/security/WakePathChecker;->getInstance()Lmiui/security/WakePathChecker;

    move-result-object v0

    iget-object v1, p0, Lcom/miui/server/SecurityManagerService;->mContext:Landroid/content/Context;

    invoke-virtual {v0, v1}, Lmiui/security/WakePathChecker;->init(Landroid/content/Context;)V

    .line 662
    return-void
.end method

.method public updatePermissionFlagsAsUser(Ljava/lang/String;Ljava/lang/String;III)V
    .locals 9
    .param p1, "permissionName"    # Ljava/lang/String;
    .param p2, "packageName"    # Ljava/lang/String;
    .param p3, "flagMask"    # I
    .param p4, "flagValues"    # I
    .param p5, "userId"    # I

    .line 931
    const-string v0, "com.lbe.security.miui"

    invoke-direct {p0, v0}, Lcom/miui/server/SecurityManagerService;->checkGrantPermissionPkg(Ljava/lang/String;)V

    .line 932
    invoke-static {}, Landroid/os/Binder;->clearCallingIdentity()J

    move-result-wide v0

    .line 934
    .local v0, "identity":J
    :try_start_0
    iget-object v2, p0, Lcom/miui/server/SecurityManagerService;->mPermissionManagerService:Lcom/android/server/pm/permission/PermissionManagerService;

    const/4 v7, 0x1

    move-object v3, p2

    move-object v4, p1

    move v5, p3

    move v6, p4

    move v8, p5

    invoke-virtual/range {v2 .. v8}, Lcom/android/server/pm/permission/PermissionManagerService;->updatePermissionFlags(Ljava/lang/String;Ljava/lang/String;IIZI)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 940
    nop

    :goto_0
    invoke-static {v0, v1}, Landroid/os/Binder;->restoreCallingIdentity(J)V

    .line 941
    goto :goto_1

    .line 940
    :catchall_0
    move-exception v2

    goto :goto_2

    .line 936
    :catch_0
    move-exception v2

    .line 937
    .local v2, "e":Ljava/lang/Exception;
    :try_start_1
    const-string v3, "SecurityManagerService"

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v5, "updatePermissionFlagsAsUser failed: perm="

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, ", pkg="

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, ", mask="

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, p3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, ", value="

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, p4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4, v2}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 940
    nop

    .end local v2    # "e":Ljava/lang/Exception;
    goto :goto_0

    .line 942
    :goto_1
    return-void

    .line 940
    :goto_2
    invoke-static {v0, v1}, Landroid/os/Binder;->restoreCallingIdentity(J)V

    .line 941
    throw v2
.end method

.method public watchGreenGuardProcess()V
    .locals 1

    .line 585
    iget-object v0, p0, Lcom/miui/server/SecurityManagerService;->mContext:Landroid/content/Context;

    invoke-static {v0}, Lcom/miui/server/GreenGuardManagerService;->startWatchGreenGuardProcess(Landroid/content/Context;)V

    .line 586
    return-void
.end method

.method public writeSettings()V
    .locals 10

    .line 1225
    const/4 v0, 0x0

    .line 1227
    .local v0, "fos":Ljava/io/FileOutputStream;
    :try_start_0
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    .line 1228
    .local v1, "userStates":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/miui/server/security/SecurityUserState;>;"
    iget-object v2, p0, Lcom/miui/server/SecurityManagerService;->mUserStateLock:Ljava/lang/Object;

    monitor-enter v2
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 1229
    :try_start_1
    iget-object v3, p0, Lcom/miui/server/SecurityManagerService;->mUserStates:Landroid/util/SparseArray;

    invoke-virtual {v3}, Landroid/util/SparseArray;->size()I

    move-result v3

    .line 1230
    .local v3, "size":I
    const/4 v4, 0x0

    .local v4, "i":I
    :goto_0
    if-ge v4, v3, :cond_0

    .line 1231
    iget-object v5, p0, Lcom/miui/server/SecurityManagerService;->mUserStates:Landroid/util/SparseArray;

    invoke-virtual {v5, v4}, Landroid/util/SparseArray;->valueAt(I)Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Lcom/miui/server/security/SecurityUserState;

    .line 1232
    .local v5, "state":Lcom/miui/server/security/SecurityUserState;
    new-instance v6, Lcom/miui/server/security/SecurityUserState;

    invoke-direct {v6}, Lcom/miui/server/security/SecurityUserState;-><init>()V

    .line 1233
    .local v6, "userState":Lcom/miui/server/security/SecurityUserState;
    iget v7, v5, Lcom/miui/server/security/SecurityUserState;->userHandle:I

    iput v7, v6, Lcom/miui/server/security/SecurityUserState;->userHandle:I

    .line 1234
    iget-object v7, v6, Lcom/miui/server/security/SecurityUserState;->mPackages:Ljava/util/HashMap;

    new-instance v8, Ljava/util/HashMap;

    iget-object v9, v5, Lcom/miui/server/security/SecurityUserState;->mPackages:Ljava/util/HashMap;

    invoke-direct {v8, v9}, Ljava/util/HashMap;-><init>(Ljava/util/Map;)V

    invoke-virtual {v7, v8}, Ljava/util/HashMap;->putAll(Ljava/util/Map;)V

    .line 1235
    invoke-virtual {v1, v6}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 1230
    nop

    .end local v5    # "state":Lcom/miui/server/security/SecurityUserState;
    .end local v6    # "userState":Lcom/miui/server/security/SecurityUserState;
    add-int/lit8 v4, v4, 0x1

    goto :goto_0

    .line 1237
    .end local v3    # "size":I
    .end local v4    # "i":I
    :cond_0
    monitor-exit v2
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 1238
    :try_start_2
    iget-object v2, p0, Lcom/miui/server/SecurityManagerService;->mSettingsFile:Landroid/util/AtomicFile;

    invoke-virtual {v2}, Landroid/util/AtomicFile;->startWrite()Ljava/io/FileOutputStream;

    move-result-object v2

    move-object v0, v2

    .line 1239
    new-instance v2, Lcom/android/internal/util/FastXmlSerializer;

    invoke-direct {v2}, Lcom/android/internal/util/FastXmlSerializer;-><init>()V

    .line 1240
    .local v2, "out":Lorg/xmlpull/v1/XmlSerializer;
    const-string/jumbo v3, "utf-8"

    invoke-interface {v2, v0, v3}, Lorg/xmlpull/v1/XmlSerializer;->setOutput(Ljava/io/OutputStream;Ljava/lang/String;)V

    .line 1241
    const/4 v3, 0x1

    invoke-static {v3}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v4

    const/4 v5, 0x0

    invoke-interface {v2, v5, v4}, Lorg/xmlpull/v1/XmlSerializer;->startDocument(Ljava/lang/String;Ljava/lang/Boolean;)V

    .line 1242
    const-string v4, "http://xmlpull.org/v1/doc/features.html#indent-output"

    invoke-interface {v2, v4, v3}, Lorg/xmlpull/v1/XmlSerializer;->setFeature(Ljava/lang/String;Z)V

    .line 1243
    const-string v3, "packages"

    invoke-interface {v2, v5, v3}, Lorg/xmlpull/v1/XmlSerializer;->startTag(Ljava/lang/String;Ljava/lang/String;)Lorg/xmlpull/v1/XmlSerializer;

    .line 1244
    const-string/jumbo v3, "updateVersion"

    const-string v4, "1.0"

    invoke-interface {v2, v5, v3, v4}, Lorg/xmlpull/v1/XmlSerializer;->attribute(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Lorg/xmlpull/v1/XmlSerializer;

    .line 1245
    invoke-virtual {v1}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :goto_1
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v4

    if-eqz v4, :cond_3

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lcom/miui/server/security/SecurityUserState;

    .line 1246
    .local v4, "userState":Lcom/miui/server/security/SecurityUserState;
    iget-object v6, v4, Lcom/miui/server/security/SecurityUserState;->mPackages:Ljava/util/HashMap;

    invoke-virtual {v6}, Ljava/util/HashMap;->values()Ljava/util/Collection;

    move-result-object v6

    invoke-interface {v6}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v6

    :goto_2
    invoke-interface {v6}, Ljava/util/Iterator;->hasNext()Z

    move-result v7

    if-eqz v7, :cond_2

    invoke-interface {v6}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v7

    check-cast v7, Lcom/miui/server/security/SecurityPackageSettings;

    .line 1247
    .local v7, "ps":Lcom/miui/server/security/SecurityPackageSettings;
    iget-object v8, v7, Lcom/miui/server/security/SecurityPackageSettings;->name:Ljava/lang/String;

    invoke-static {v8}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v8

    if-eqz v8, :cond_1

    .line 1248
    const-string v8, "SecurityManagerService"

    const-string/jumbo v9, "write current package name is empty, skip"

    invoke-static {v8, v9}, Landroid/util/Slog;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 1249
    goto :goto_2

    .line 1251
    :cond_1
    const-string v8, "package"

    invoke-interface {v2, v5, v8}, Lorg/xmlpull/v1/XmlSerializer;->startTag(Ljava/lang/String;Ljava/lang/String;)Lorg/xmlpull/v1/XmlSerializer;

    .line 1252
    const-string v8, "name"

    iget-object v9, v7, Lcom/miui/server/security/SecurityPackageSettings;->name:Ljava/lang/String;

    invoke-interface {v2, v5, v8, v9}, Lorg/xmlpull/v1/XmlSerializer;->attribute(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Lorg/xmlpull/v1/XmlSerializer;

    .line 1253
    const-string v8, "accessControl"

    iget-boolean v9, v7, Lcom/miui/server/security/SecurityPackageSettings;->accessControl:Z

    invoke-static {v9}, Ljava/lang/String;->valueOf(Z)Ljava/lang/String;

    move-result-object v9

    invoke-interface {v2, v5, v8, v9}, Lorg/xmlpull/v1/XmlSerializer;->attribute(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Lorg/xmlpull/v1/XmlSerializer;

    .line 1254
    const-string v8, "childrenControl"

    iget-boolean v9, v7, Lcom/miui/server/security/SecurityPackageSettings;->childrenControl:Z

    invoke-static {v9}, Ljava/lang/String;->valueOf(Z)Ljava/lang/String;

    move-result-object v9

    invoke-interface {v2, v5, v8, v9}, Lorg/xmlpull/v1/XmlSerializer;->attribute(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Lorg/xmlpull/v1/XmlSerializer;

    .line 1255
    const-string v8, "maskNotification"

    iget-boolean v9, v7, Lcom/miui/server/security/SecurityPackageSettings;->maskNotification:Z

    invoke-static {v9}, Ljava/lang/String;->valueOf(Z)Ljava/lang/String;

    move-result-object v9

    invoke-interface {v2, v5, v8, v9}, Lorg/xmlpull/v1/XmlSerializer;->attribute(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Lorg/xmlpull/v1/XmlSerializer;

    .line 1256
    const-string v8, "isPrivacyApp"

    iget-boolean v9, v7, Lcom/miui/server/security/SecurityPackageSettings;->isPrivacyApp:Z

    invoke-static {v9}, Ljava/lang/String;->valueOf(Z)Ljava/lang/String;

    move-result-object v9

    invoke-interface {v2, v5, v8, v9}, Lorg/xmlpull/v1/XmlSerializer;->attribute(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Lorg/xmlpull/v1/XmlSerializer;

    .line 1258
    const-string v8, "isDarkModeChecked"

    iget-boolean v9, v7, Lcom/miui/server/security/SecurityPackageSettings;->isDarkModeChecked:Z

    invoke-static {v9}, Ljava/lang/String;->valueOf(Z)Ljava/lang/String;

    move-result-object v9

    invoke-interface {v2, v5, v8, v9}, Lorg/xmlpull/v1/XmlSerializer;->attribute(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Lorg/xmlpull/v1/XmlSerializer;

    .line 1260
    const-string v8, "isGameStorageApp"

    iget-boolean v9, v7, Lcom/miui/server/security/SecurityPackageSettings;->isGameStorageApp:Z

    invoke-static {v9}, Ljava/lang/String;->valueOf(Z)Ljava/lang/String;

    move-result-object v9

    invoke-interface {v2, v5, v8, v9}, Lorg/xmlpull/v1/XmlSerializer;->attribute(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Lorg/xmlpull/v1/XmlSerializer;

    .line 1261
    const-string v8, "isRemindForRelaunch"

    iget-boolean v9, v7, Lcom/miui/server/security/SecurityPackageSettings;->isRemindForRelaunch:Z

    invoke-static {v9}, Ljava/lang/String;->valueOf(Z)Ljava/lang/String;

    move-result-object v9

    invoke-interface {v2, v5, v8, v9}, Lorg/xmlpull/v1/XmlSerializer;->attribute(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Lorg/xmlpull/v1/XmlSerializer;

    .line 1262
    const-string v8, "isRelaunchWhenFolded"

    iget-boolean v9, v7, Lcom/miui/server/security/SecurityPackageSettings;->isRelaunchWhenFolded:Z

    invoke-static {v9}, Ljava/lang/String;->valueOf(Z)Ljava/lang/String;

    move-result-object v9

    invoke-interface {v2, v5, v8, v9}, Lorg/xmlpull/v1/XmlSerializer;->attribute(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Lorg/xmlpull/v1/XmlSerializer;

    .line 1263
    const-string v8, "isScRelaunchConfirm"

    iget-boolean v9, v7, Lcom/miui/server/security/SecurityPackageSettings;->isScRelaunchConfirm:Z

    invoke-static {v9}, Ljava/lang/String;->valueOf(Z)Ljava/lang/String;

    move-result-object v9

    invoke-interface {v2, v5, v8, v9}, Lorg/xmlpull/v1/XmlSerializer;->attribute(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Lorg/xmlpull/v1/XmlSerializer;

    .line 1264
    const-string/jumbo v8, "u"

    iget v9, v4, Lcom/miui/server/security/SecurityUserState;->userHandle:I

    invoke-static {v9}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v9

    invoke-interface {v2, v5, v8, v9}, Lorg/xmlpull/v1/XmlSerializer;->attribute(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Lorg/xmlpull/v1/XmlSerializer;

    .line 1265
    const-string v8, "package"

    invoke-interface {v2, v5, v8}, Lorg/xmlpull/v1/XmlSerializer;->endTag(Ljava/lang/String;Ljava/lang/String;)Lorg/xmlpull/v1/XmlSerializer;

    .line 1266
    nop

    .end local v7    # "ps":Lcom/miui/server/security/SecurityPackageSettings;
    goto/16 :goto_2

    .line 1267
    .end local v4    # "userState":Lcom/miui/server/security/SecurityUserState;
    :cond_2
    goto/16 :goto_1

    .line 1268
    :cond_3
    const-string v3, "packages"

    invoke-interface {v2, v5, v3}, Lorg/xmlpull/v1/XmlSerializer;->endTag(Ljava/lang/String;Ljava/lang/String;)Lorg/xmlpull/v1/XmlSerializer;

    .line 1269
    invoke-interface {v2}, Lorg/xmlpull/v1/XmlSerializer;->endDocument()V

    .line 1270
    iget-object v3, p0, Lcom/miui/server/SecurityManagerService;->mSettingsFile:Landroid/util/AtomicFile;

    invoke-virtual {v3, v0}, Landroid/util/AtomicFile;->finishWrite(Ljava/io/FileOutputStream;)V
    :try_end_2
    .catch Ljava/lang/Exception; {:try_start_2 .. :try_end_2} :catch_0

    .line 1276
    .end local v1    # "userStates":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/miui/server/security/SecurityUserState;>;"
    .end local v2    # "out":Lorg/xmlpull/v1/XmlSerializer;
    goto :goto_3

    .line 1237
    .restart local v1    # "userStates":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/miui/server/security/SecurityUserState;>;"
    :catchall_0
    move-exception v3

    :try_start_3
    monitor-exit v2
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    .end local v0    # "fos":Ljava/io/FileOutputStream;
    .end local p0    # "this":Lcom/miui/server/SecurityManagerService;
    :try_start_4
    throw v3
    :try_end_4
    .catch Ljava/lang/Exception; {:try_start_4 .. :try_end_4} :catch_0

    .line 1271
    .end local v1    # "userStates":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/miui/server/security/SecurityUserState;>;"
    .restart local v0    # "fos":Ljava/io/FileOutputStream;
    .restart local p0    # "this":Lcom/miui/server/SecurityManagerService;
    :catch_0
    move-exception v1

    .line 1272
    .local v1, "e1":Ljava/lang/Exception;
    const-string v2, "SecurityManagerService"

    const-string v3, "Error writing package settings file"

    invoke-static {v2, v3, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    .line 1273
    if-eqz v0, :cond_4

    .line 1274
    iget-object v2, p0, Lcom/miui/server/SecurityManagerService;->mSettingsFile:Landroid/util/AtomicFile;

    invoke-virtual {v2, v0}, Landroid/util/AtomicFile;->failWrite(Ljava/io/FileOutputStream;)V

    .line 1277
    .end local v1    # "e1":Ljava/lang/Exception;
    :cond_4
    :goto_3
    return-void
.end method
