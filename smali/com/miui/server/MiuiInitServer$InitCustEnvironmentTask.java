class com.miui.server.MiuiInitServer$InitCustEnvironmentTask extends java.lang.Thread {
	 /* .source "MiuiInitServer.java" */
	 /* # annotations */
	 /* .annotation system Ldalvik/annotation/EnclosingClass; */
	 /* value = Lcom/miui/server/MiuiInitServer; */
} // .end annotation
/* .annotation system Ldalvik/annotation/InnerClass; */
/* accessFlags = 0x2 */
/* name = "InitCustEnvironmentTask" */
} // .end annotation
/* # instance fields */
private java.lang.String mCustVarinat;
private miui.os.IMiuiInitObserver mObs;
final com.miui.server.MiuiInitServer this$0; //synthetic
/* # direct methods */
 com.miui.server.MiuiInitServer$InitCustEnvironmentTask ( ) {
/* .locals 0 */
/* .param p2, "custVariant" # Ljava/lang/String; */
/* .param p3, "obs" # Lmiui/os/IMiuiInitObserver; */
/* .line 102 */
this.this$0 = p1;
/* invoke-direct {p0}, Ljava/lang/Thread;-><init>()V */
/* .line 103 */
this.mCustVarinat = p2;
/* .line 104 */
this.mObs = p3;
/* .line 105 */
return;
} // .end method
private void importCustProperties ( java.io.File p0, Boolean p1 ) {
/* .locals 7 */
/* .param p1, "custProp" # Ljava/io/File; */
/* .param p2, "isTimezoneAuto" # Z */
/* .line 217 */
v0 = (( java.io.File ) p1 ).exists ( ); // invoke-virtual {p1}, Ljava/io/File;->exists()Z
if ( v0 != null) { // if-eqz v0, :cond_6
	 /* .line 218 */
	 try { // :try_start_0
		 /* new-instance v0, Ljava/io/BufferedReader; */
		 /* new-instance v1, Ljava/io/FileReader; */
		 /* invoke-direct {v1, p1}, Ljava/io/FileReader;-><init>(Ljava/io/File;)V */
		 /* invoke-direct {v0, v1}, Ljava/io/BufferedReader;-><init>(Ljava/io/Reader;)V */
		 /* :try_end_0 */
		 /* .catch Ljava/io/IOException; {:try_start_0 ..:try_end_0} :catch_0 */
		 /* .line 220 */
		 /* .local v0, "bufferReader":Ljava/io/BufferedReader; */
	 } // :cond_0
} // :goto_0
try { // :try_start_1
	 (( java.io.BufferedReader ) v0 ).readLine ( ); // invoke-virtual {v0}, Ljava/io/BufferedReader;->readLine()Ljava/lang/String;
	 /* :try_end_1 */
	 /* .catchall {:try_start_1 ..:try_end_1} :catchall_0 */
	 /* move-object v2, v1 */
	 /* .local v2, "line":Ljava/lang/String; */
	 final String v3 = "MiuiInitServer"; // const-string v3, "MiuiInitServer"
	 final String v4 = "persist.sys.timezone"; // const-string v4, "persist.sys.timezone"
	 if ( v1 != null) { // if-eqz v1, :cond_4
		 /* .line 221 */
		 try { // :try_start_2
			 (( java.lang.String ) v2 ).trim ( ); // invoke-virtual {v2}, Ljava/lang/String;->trim()Ljava/lang/String;
			 /* .line 222 */
		 } // .end local v2 # "line":Ljava/lang/String;
		 /* .local v1, "line":Ljava/lang/String; */
		 final String v2 = "#"; // const-string v2, "#"
		 v2 = 		 (( java.lang.String ) v1 ).startsWith ( v2 ); // invoke-virtual {v1, v2}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z
		 if ( v2 != null) { // if-eqz v2, :cond_1
			 /* .line 223 */
			 /* .line 225 */
		 } // :cond_1
		 final String v2 = "="; // const-string v2, "="
		 (( java.lang.String ) v1 ).split ( v2 ); // invoke-virtual {v1, v2}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;
		 /* .line 226 */
		 /* .local v2, "ss":[Ljava/lang/String; */
		 if ( v2 != null) { // if-eqz v2, :cond_0
			 /* array-length v5, v2 */
			 int v6 = 2; // const/4 v6, 0x2
			 /* if-eq v5, v6, :cond_2 */
			 /* .line 227 */
			 /* .line 230 */
		 } // :cond_2
		 int v5 = 0; // const/4 v5, 0x0
		 /* aget-object v6, v2, v5 */
		 v4 = 		 (( java.lang.String ) v4 ).equals ( v6 ); // invoke-virtual {v4, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z
		 if ( v4 != null) { // if-eqz v4, :cond_3
			 if ( p2 != null) { // if-eqz p2, :cond_3
				 v4 = 				 /* invoke-direct {p0}, Lcom/miui/server/MiuiInitServer$InitCustEnvironmentTask;->isDeviceNotInProvision()Z */
				 if ( v4 != null) { // if-eqz v4, :cond_3
					 /* .line 231 */
					 final String v4 = "persist.sys.timezone will not be changed when AUTO_TIME_ZONE is open!"; // const-string v4, "persist.sys.timezone will not be changed when AUTO_TIME_ZONE is open!"
					 android.util.Log .i ( v3,v4 );
					 /* .line 233 */
				 } // :cond_3
				 /* aget-object v3, v2, v5 */
				 int v4 = 1; // const/4 v4, 0x1
				 /* aget-object v4, v2, v4 */
				 android.os.SystemProperties .set ( v3,v4 );
				 /* .line 235 */
			 } // .end local v2 # "ss":[Ljava/lang/String;
		 } // :goto_1
		 /* .line 236 */
	 } // .end local v1 # "line":Ljava/lang/String;
	 /* .local v2, "line":Ljava/lang/String; */
} // :cond_4
/* sget-boolean v1, Lmiui/os/Build;->IS_GLOBAL_BUILD:Z */
if ( v1 != null) { // if-eqz v1, :cond_5
	 /* .line 237 */
	 final String v1 = ""; // const-string v1, ""
	 android.os.SystemProperties .get ( v4,v1 );
	 /* .line 238 */
	 /* .local v1, "zoneid":Ljava/lang/String; */
	 /* new-instance v4, Ljava/lang/StringBuilder; */
	 /* invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V */
	 final String v5 = "init MiuiSettings RESIDENT_TIMEZONE: "; // const-string v5, "init MiuiSettings RESIDENT_TIMEZONE: "
	 (( java.lang.StringBuilder ) v4 ).append ( v5 ); // invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
	 (( java.lang.StringBuilder ) v4 ).append ( v1 ); // invoke-virtual {v4, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
	 (( java.lang.StringBuilder ) v4 ).toString ( ); // invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
	 android.util.Log .i ( v3,v4 );
	 /* .line 239 */
	 v3 = this.this$0;
	 com.miui.server.MiuiInitServer .-$$Nest$fgetmContext ( v3 );
	 (( android.content.Context ) v3 ).getContentResolver ( ); // invoke-virtual {v3}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;
	 final String v4 = "resident_timezone"; // const-string v4, "resident_timezone"
	 android.provider.Settings$System .putString ( v3,v4,v1 );
	 /* .line 242 */
} // .end local v1 # "zoneid":Ljava/lang/String;
} // :cond_5
/* invoke-direct {p0}, Lcom/miui/server/MiuiInitServer$InitCustEnvironmentTask;->pokeSystemProperties()V */
/* :try_end_2 */
/* .catchall {:try_start_2 ..:try_end_2} :catchall_0 */
/* .line 243 */
} // .end local v2 # "line":Ljava/lang/String;
try { // :try_start_3
(( java.io.BufferedReader ) v0 ).close ( ); // invoke-virtual {v0}, Ljava/io/BufferedReader;->close()V
/* :try_end_3 */
/* .catch Ljava/io/IOException; {:try_start_3 ..:try_end_3} :catch_0 */
/* .line 244 */
} // .end local v0 # "bufferReader":Ljava/io/BufferedReader;
/* .line 218 */
/* .restart local v0 # "bufferReader":Ljava/io/BufferedReader; */
/* :catchall_0 */
/* move-exception v1 */
try { // :try_start_4
(( java.io.BufferedReader ) v0 ).close ( ); // invoke-virtual {v0}, Ljava/io/BufferedReader;->close()V
/* :try_end_4 */
/* .catchall {:try_start_4 ..:try_end_4} :catchall_1 */
/* :catchall_1 */
/* move-exception v2 */
try { // :try_start_5
(( java.lang.Throwable ) v1 ).addSuppressed ( v2 ); // invoke-virtual {v1, v2}, Ljava/lang/Throwable;->addSuppressed(Ljava/lang/Throwable;)V
} // .end local p0 # "this":Lcom/miui/server/MiuiInitServer$InitCustEnvironmentTask;
} // .end local p1 # "custProp":Ljava/io/File;
} // .end local p2 # "isTimezoneAuto":Z
} // :goto_2
/* throw v1 */
/* :try_end_5 */
/* .catch Ljava/io/IOException; {:try_start_5 ..:try_end_5} :catch_0 */
/* .line 243 */
} // .end local v0 # "bufferReader":Ljava/io/BufferedReader;
/* .restart local p0 # "this":Lcom/miui/server/MiuiInitServer$InitCustEnvironmentTask; */
/* .restart local p1 # "custProp":Ljava/io/File; */
/* .restart local p2 # "isTimezoneAuto":Z */
/* :catch_0 */
/* move-exception v0 */
/* .line 246 */
} // :cond_6
} // :goto_3
return;
} // .end method
private Boolean initCustEnvironment ( java.lang.String p0 ) {
/* .locals 5 */
/* .param p1, "custVariant" # Ljava/lang/String; */
/* .line 169 */
miui.util.CustomizeUtil .setMiuiCustVariatDir ( p1 );
/* .line 170 */
int v0 = 1; // const/4 v0, 0x1
miui.util.CustomizeUtil .getMiuiCustVariantDir ( v0 );
/* .line 171 */
/* .local v0, "custVariantDir":Ljava/io/File; */
miui.util.CustomizeUtil .geCarrierRegionPropDir ( );
/* .line 172 */
/* .local v1, "carrierPropDir":Ljava/io/File; */
int v2 = 0; // const/4 v2, 0x0
/* if-nez v0, :cond_0 */
/* .line 173 */
/* .line 182 */
} // :cond_0
/* new-instance v3, Ljava/io/File; */
final String v4 = "cust.prop"; // const-string v4, "cust.prop"
/* invoke-direct {v3, v0, v4}, Ljava/io/File;-><init>(Ljava/io/File;Ljava/lang/String;)V */
v4 = /* invoke-direct {p0}, Lcom/miui/server/MiuiInitServer$InitCustEnvironmentTask;->isTimeZoneAuto()Z */
/* invoke-direct {p0, v3, v4}, Lcom/miui/server/MiuiInitServer$InitCustEnvironmentTask;->importCustProperties(Ljava/io/File;Z)V */
/* .line 184 */
/* new-instance v3, Ljava/io/File; */
final String v4 = "region_specified.prop"; // const-string v4, "region_specified.prop"
/* invoke-direct {v3, v1, v4}, Ljava/io/File;-><init>(Ljava/io/File;Ljava/lang/String;)V */
/* invoke-direct {p0, v3, v2}, Lcom/miui/server/MiuiInitServer$InitCustEnvironmentTask;->importCustProperties(Ljava/io/File;Z)V */
/* .line 187 */
/* invoke-direct {p0, p1}, Lcom/miui/server/MiuiInitServer$InitCustEnvironmentTask;->saveCustVariantToFile(Ljava/lang/String;)V */
/* .line 190 */
v2 = this.this$0;
com.miui.server.MiuiInitServer .-$$Nest$fgetmContext ( v2 );
(( android.content.Context ) v2 ).getContentResolver ( ); // invoke-virtual {v2}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;
/* const-string/jumbo v3, "wifi_country_code" */
android.provider.Settings$Global .getString ( v2,v3 );
/* .line 192 */
/* .local v2, "countryCode":Ljava/lang/String; */
v3 = android.text.TextUtils .isEmpty ( v2 );
if ( v3 != null) { // if-eqz v3, :cond_1
/* .line 193 */
miui.os.Build .getRegion ( );
/* .line 194 */
v3 = android.text.TextUtils .isEmpty ( v2 );
/* if-nez v3, :cond_1 */
/* .line 195 */
v3 = this.this$0;
com.miui.server.MiuiInitServer .-$$Nest$fgetmContext ( v3 );
/* .line 196 */
/* const-string/jumbo v4, "wifi" */
(( android.content.Context ) v3 ).getSystemService ( v4 ); // invoke-virtual {v3, v4}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;
/* check-cast v3, Landroid/net/wifi/WifiManager; */
/* .line 203 */
} // :cond_1
/* invoke-direct {p0}, Lcom/miui/server/MiuiInitServer$InitCustEnvironmentTask;->installVanwardCustApps()V */
/* .line 205 */
v3 = (( java.io.File ) v0 ).exists ( ); // invoke-virtual {v0}, Ljava/io/File;->exists()Z
} // .end method
private void installVanwardCustApps ( ) {
/* .locals 1 */
/* .line 209 */
v0 = this.this$0;
com.miui.server.MiuiInitServer .-$$Nest$fgetmMiuiPreinstallHelper ( v0 );
v0 = (( com.android.server.pm.MiuiPreinstallHelper ) v0 ).isSupportNewFrame ( ); // invoke-virtual {v0}, Lcom/android/server/pm/MiuiPreinstallHelper;->isSupportNewFrame()Z
if ( v0 != null) { // if-eqz v0, :cond_0
/* .line 210 */
v0 = this.this$0;
com.miui.server.MiuiInitServer .-$$Nest$fgetmMiuiPreinstallHelper ( v0 );
(( com.android.server.pm.MiuiPreinstallHelper ) v0 ).installVanwardApps ( ); // invoke-virtual {v0}, Lcom/android/server/pm/MiuiPreinstallHelper;->installVanwardApps()V
/* .line 212 */
} // :cond_0
v0 = this.this$0;
com.miui.server.MiuiInitServer .-$$Nest$fgetmContext ( v0 );
com.android.server.pm.PreinstallApp .installVanwardCustApps ( v0 );
/* .line 214 */
} // :goto_0
return;
} // .end method
private Boolean isDeviceNotInProvision ( ) {
/* .locals 3 */
/* .line 165 */
v0 = this.this$0;
com.miui.server.MiuiInitServer .-$$Nest$fgetmContext ( v0 );
(( android.content.Context ) v0 ).getContentResolver ( ); // invoke-virtual {v0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;
final String v1 = "device_provisioned"; // const-string v1, "device_provisioned"
int v2 = 0; // const/4 v2, 0x0
v0 = android.provider.Settings$Secure .getInt ( v0,v1,v2 );
if ( v0 != null) { // if-eqz v0, :cond_0
int v2 = 1; // const/4 v2, 0x1
} // :cond_0
} // .end method
private Boolean isTimeZoneAuto ( ) {
/* .locals 4 */
/* .line 156 */
int v0 = 0; // const/4 v0, 0x0
try { // :try_start_0
v1 = this.this$0;
com.miui.server.MiuiInitServer .-$$Nest$fgetmContext ( v1 );
(( android.content.Context ) v1 ).getContentResolver ( ); // invoke-virtual {v1}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;
final String v2 = "auto_time_zone"; // const-string v2, "auto_time_zone"
v1 = android.provider.Settings$Global .getInt ( v1,v2 );
/* :try_end_0 */
/* .catch Landroid/provider/Settings$SettingNotFoundException; {:try_start_0 ..:try_end_0} :catch_0 */
/* if-lez v1, :cond_0 */
int v0 = 1; // const/4 v0, 0x1
} // :cond_0
/* .line 158 */
/* :catch_0 */
/* move-exception v1 */
/* .line 159 */
/* .local v1, "e":Landroid/provider/Settings$SettingNotFoundException; */
/* new-instance v2, Ljava/lang/StringBuilder; */
/* invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V */
final String v3 = "AUTO_TIME_ZONE can\'t found : "; // const-string v3, "AUTO_TIME_ZONE can\'t found : "
(( java.lang.StringBuilder ) v2 ).append ( v3 ); // invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v2 ).append ( v1 ); // invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v2 ).toString ( ); // invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
final String v3 = "MiuiInitServer"; // const-string v3, "MiuiInitServer"
android.util.Log .i ( v3,v2 );
/* .line 160 */
} // .end method
private void pokeSystemProperties ( ) {
/* .locals 10 */
/* .line 251 */
try { // :try_start_0
android.os.ServiceManager .listServices ( );
/* :try_end_0 */
/* .catch Ljava/lang/Exception; {:try_start_0 ..:try_end_0} :catch_2 */
/* .line 254 */
/* .local v0, "services":[Ljava/lang/String; */
/* nop */
/* .line 255 */
/* array-length v1, v0 */
int v2 = 0; // const/4 v2, 0x0
/* move v3, v2 */
} // :goto_0
/* if-ge v3, v1, :cond_1 */
/* aget-object v4, v0, v3 */
/* .line 256 */
/* .local v4, "service":Ljava/lang/String; */
android.os.ServiceManager .checkService ( v4 );
/* .line 257 */
/* .local v5, "obj":Landroid/os/IBinder; */
if ( v5 != null) { // if-eqz v5, :cond_0
/* .line 258 */
android.os.Parcel .obtain ( );
/* .line 260 */
/* .local v6, "data":Landroid/os/Parcel; */
/* const v7, 0x5f535052 */
int v8 = 0; // const/4 v8, 0x0
try { // :try_start_1
/* :try_end_1 */
/* .catch Landroid/os/RemoteException; {:try_start_1 ..:try_end_1} :catch_1 */
/* .catch Ljava/lang/Exception; {:try_start_1 ..:try_end_1} :catch_0 */
/* .line 265 */
} // :goto_1
/* .line 262 */
/* :catch_0 */
/* move-exception v7 */
/* .line 263 */
/* .local v7, "e":Ljava/lang/Exception; */
/* new-instance v8, Ljava/lang/StringBuilder; */
/* invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V */
final String v9 = "Someone wrote a bad service \'"; // const-string v9, "Someone wrote a bad service \'"
(( java.lang.StringBuilder ) v8 ).append ( v9 ); // invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v8 ).append ( v4 ); // invoke-virtual {v8, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
final String v9 = "\' that doesn\'t like to be poked: "; // const-string v9, "\' that doesn\'t like to be poked: "
(( java.lang.StringBuilder ) v8 ).append ( v9 ); // invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v8 ).append ( v7 ); // invoke-virtual {v8, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v8 ).toString ( ); // invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
final String v9 = "MiuiInitServer"; // const-string v9, "MiuiInitServer"
android.util.Log .i ( v9,v8 );
/* .line 261 */
} // .end local v7 # "e":Ljava/lang/Exception;
/* :catch_1 */
/* move-exception v7 */
/* .line 266 */
} // :goto_2
(( android.os.Parcel ) v6 ).recycle ( ); // invoke-virtual {v6}, Landroid/os/Parcel;->recycle()V
/* .line 255 */
} // .end local v4 # "service":Ljava/lang/String;
} // .end local v5 # "obj":Landroid/os/IBinder;
} // .end local v6 # "data":Landroid/os/Parcel;
} // :cond_0
/* add-int/lit8 v3, v3, 0x1 */
/* .line 269 */
} // :cond_1
return;
/* .line 252 */
} // .end local v0 # "services":[Ljava/lang/String;
/* :catch_2 */
/* move-exception v0 */
/* .line 253 */
/* .local v0, "e":Ljava/lang/Exception; */
return;
} // .end method
private void saveCustVariantToFile ( java.lang.String p0 ) {
/* .locals 5 */
/* .param p1, "custVariant" # Ljava/lang/String; */
/* .line 272 */
miui.util.CustomizeUtil .getMiuiCustVariantFile ( );
/* .line 274 */
/* .local v0, "custVariantFile":Ljava/io/File; */
try { // :try_start_0
v1 = (( java.io.File ) v0 ).exists ( ); // invoke-virtual {v0}, Ljava/io/File;->exists()Z
/* if-nez v1, :cond_0 */
/* .line 275 */
(( java.io.File ) v0 ).getParentFile ( ); // invoke-virtual {v0}, Ljava/io/File;->getParentFile()Ljava/io/File;
(( java.io.File ) v1 ).mkdirs ( ); // invoke-virtual {v1}, Ljava/io/File;->mkdirs()Z
/* .line 276 */
(( java.io.File ) v0 ).createNewFile ( ); // invoke-virtual {v0}, Ljava/io/File;->createNewFile()Z
/* .line 278 */
} // :cond_0
/* new-instance v1, Ljava/io/FileWriter; */
int v2 = 0; // const/4 v2, 0x0
/* invoke-direct {v1, v0, v2}, Ljava/io/FileWriter;-><init>(Ljava/io/File;Z)V */
/* .line 279 */
/* .local v1, "fileWriter":Ljava/io/FileWriter; */
/* new-instance v3, Ljava/io/BufferedWriter; */
/* invoke-direct {v3, v1}, Ljava/io/BufferedWriter;-><init>(Ljava/io/Writer;)V */
/* .line 280 */
/* .local v3, "bufferWriter":Ljava/io/BufferedWriter; */
(( java.io.BufferedWriter ) v3 ).write ( p1 ); // invoke-virtual {v3, p1}, Ljava/io/BufferedWriter;->write(Ljava/lang/String;)V
/* .line 281 */
(( java.io.BufferedWriter ) v3 ).close ( ); // invoke-virtual {v3}, Ljava/io/BufferedWriter;->close()V
/* .line 282 */
(( java.io.FileWriter ) v1 ).close ( ); // invoke-virtual {v1}, Ljava/io/FileWriter;->close()V
/* .line 283 */
int v4 = 1; // const/4 v4, 0x1
(( java.io.File ) v0 ).setReadable ( v4, v2 ); // invoke-virtual {v0, v4, v2}, Ljava/io/File;->setReadable(ZZ)Z
/* :try_end_0 */
/* .catch Ljava/io/IOException; {:try_start_0 ..:try_end_0} :catch_0 */
/* .line 286 */
/* nop */
} // .end local v1 # "fileWriter":Ljava/io/FileWriter;
} // .end local v3 # "bufferWriter":Ljava/io/BufferedWriter;
/* .line 284 */
/* :catch_0 */
/* move-exception v1 */
/* .line 285 */
/* .local v1, "e":Ljava/io/IOException; */
(( java.io.IOException ) v1 ).printStackTrace ( ); // invoke-virtual {v1}, Ljava/io/IOException;->printStackTrace()V
/* .line 287 */
} // .end local v1 # "e":Ljava/io/IOException;
} // :goto_0
return;
} // .end method
/* # virtual methods */
public void run ( ) {
/* .locals 6 */
/* .line 109 */
v0 = this.mCustVarinat;
v0 = /* invoke-direct {p0, v0}, Lcom/miui/server/MiuiInitServer$InitCustEnvironmentTask;->initCustEnvironment(Ljava/lang/String;)Z */
/* .line 110 */
/* .local v0, "ret":Z */
v1 = this.mObs;
if ( v1 != null) { // if-eqz v1, :cond_0
/* .line 112 */
try { // :try_start_0
/* :try_end_0 */
/* .catch Landroid/os/RemoteException; {:try_start_0 ..:try_end_0} :catch_0 */
/* .line 114 */
/* .line 113 */
/* :catch_0 */
/* move-exception v1 */
/* .line 116 */
} // :cond_0
} // :goto_0
v1 = this.this$0;
int v2 = 0; // const/4 v2, 0x0
com.miui.server.MiuiInitServer .-$$Nest$fputmDoing ( v1,v2 );
/* .line 128 */
try { // :try_start_1
miui.content.res.GlobalConfiguration .get ( );
/* .line 129 */
/* .local v1, "curConfig":Landroid/content/res/Configuration; */
(( android.content.res.Configuration ) v1 ).getExtraConfig ( ); // invoke-virtual {v1}, Landroid/content/res/Configuration;->getExtraConfig()Landroid/content/res/IMiuiConfiguration;
/* .line 130 */
/* .local v2, "iExtraConfig":Landroid/content/res/IMiuiConfiguration; */
/* instance-of v3, v2, Landroid/content/res/MiuiConfiguration; */
if ( v3 != null) { // if-eqz v3, :cond_1
/* .line 131 */
/* move-object v3, v2 */
/* check-cast v3, Landroid/content/res/MiuiConfiguration; */
} // :cond_1
int v3 = 0; // const/4 v3, 0x0
/* .line 132 */
/* .local v3, "extraConfig":Landroid/content/res/MiuiConfiguration; */
} // :goto_1
if ( v3 != null) { // if-eqz v3, :cond_2
/* .line 133 */
/* const-wide/16 v4, 0x0 */
(( android.content.res.MiuiConfiguration ) v3 ).updateTheme ( v4, v5 ); // invoke-virtual {v3, v4, v5}, Landroid/content/res/MiuiConfiguration;->updateTheme(J)V
/* .line 135 */
} // :cond_2
miui.content.res.GlobalConfiguration .update ( v1 );
/* :try_end_1 */
/* .catch Landroid/os/RemoteException; {:try_start_1 ..:try_end_1} :catch_1 */
/* .line 138 */
} // .end local v1 # "curConfig":Landroid/content/res/Configuration;
} // .end local v2 # "iExtraConfig":Landroid/content/res/IMiuiConfiguration;
} // .end local v3 # "extraConfig":Landroid/content/res/MiuiConfiguration;
/* .line 136 */
/* :catch_1 */
/* move-exception v1 */
/* .line 137 */
/* .local v1, "e":Landroid/os/RemoteException; */
(( android.os.RemoteException ) v1 ).printStackTrace ( ); // invoke-virtual {v1}, Landroid/os/RemoteException;->printStackTrace()V
/* .line 140 */
} // .end local v1 # "e":Landroid/os/RemoteException;
} // :goto_2
v1 = this.this$0;
com.miui.server.MiuiInitServer .-$$Nest$fgetmContext ( v1 );
com.miui.server.EnableStateManager .updateApplicationEnableState ( v1 );
/* .line 141 */
v1 = this.this$0;
com.miui.server.MiuiInitServer .-$$Nest$mdeletePackagesByRegion ( v1 );
/* .line 144 */
v1 = this.this$0;
com.miui.server.MiuiInitServer .-$$Nest$fgetmContext ( v1 );
/* new-instance v2, Landroid/content/Intent; */
final String v3 = "miui.intent.action.MIUI_INIT_COMPLETED"; // const-string v3, "miui.intent.action.MIUI_INIT_COMPLETED"
/* invoke-direct {v2, v3}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V */
final String v3 = "miui.os.permisson.INIT_MIUI_ENVIRONMENT"; // const-string v3, "miui.os.permisson.INIT_MIUI_ENVIRONMENT"
(( android.content.Context ) v1 ).sendBroadcast ( v2, v3 ); // invoke-virtual {v1, v2, v3}, Landroid/content/Context;->sendBroadcast(Landroid/content/Intent;Ljava/lang/String;)V
/* .line 148 */
/* new-instance v1, Landroid/content/Intent; */
final String v2 = "miui.intent.action.MIUI_REGION_CHANGED"; // const-string v2, "miui.intent.action.MIUI_REGION_CHANGED"
/* invoke-direct {v1, v2}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V */
/* .line 149 */
/* .local v1, "intent":Landroid/content/Intent; */
/* const/high16 v2, 0x1000000 */
(( android.content.Intent ) v1 ).addFlags ( v2 ); // invoke-virtual {v1, v2}, Landroid/content/Intent;->addFlags(I)Landroid/content/Intent;
/* .line 150 */
final String v2 = "region"; // const-string v2, "region"
miui.os.Build .getRegion ( );
(( android.content.Intent ) v1 ).putExtra ( v2, v3 ); // invoke-virtual {v1, v2, v3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;
/* .line 151 */
v2 = this.this$0;
com.miui.server.MiuiInitServer .-$$Nest$fgetmContext ( v2 );
(( android.content.Context ) v2 ).sendBroadcast ( v1 ); // invoke-virtual {v2, v1}, Landroid/content/Context;->sendBroadcast(Landroid/content/Intent;)V
/* .line 152 */
return;
} // .end method
