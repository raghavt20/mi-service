.class public final Lcom/miui/server/SecurityManagerService$Lifecycle;
.super Lcom/android/server/SystemService;
.source "SecurityManagerService.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/miui/server/SecurityManagerService;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "Lifecycle"
.end annotation


# instance fields
.field private final mService:Lcom/miui/server/SecurityManagerService;


# direct methods
.method public static synthetic $r8$lambda$qFnYMFfpemsO-A5nUBhDltjbOxk(Lcom/miui/server/SecurityManagerService;)V
    .locals 0

    invoke-static {p0}, Lcom/miui/server/SecurityManagerService;->-$$Nest$minitWhenBootCompleted(Lcom/miui/server/SecurityManagerService;)V

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;)V
    .locals 1
    .param p1, "context"    # Landroid/content/Context;

    .line 1569
    invoke-direct {p0, p1}, Lcom/android/server/SystemService;-><init>(Landroid/content/Context;)V

    .line 1570
    new-instance v0, Lcom/miui/server/SecurityManagerService;

    invoke-direct {v0, p1}, Lcom/miui/server/SecurityManagerService;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Lcom/miui/server/SecurityManagerService$Lifecycle;->mService:Lcom/miui/server/SecurityManagerService;

    .line 1571
    return-void
.end method


# virtual methods
.method public onBootPhase(I)V
    .locals 3
    .param p1, "phase"    # I

    .line 1580
    const/16 v0, 0x3e8

    if-ne p1, v0, :cond_0

    .line 1581
    iget-object v0, p0, Lcom/miui/server/SecurityManagerService$Lifecycle;->mService:Lcom/miui/server/SecurityManagerService;

    iget-object v0, v0, Lcom/miui/server/SecurityManagerService;->mSecurityWriteHandler:Lcom/miui/server/security/SecurityWriteHandler;

    iget-object v1, p0, Lcom/miui/server/SecurityManagerService$Lifecycle;->mService:Lcom/miui/server/SecurityManagerService;

    invoke-static {v1}, Ljava/util/Objects;->requireNonNull(Ljava/lang/Object;)Ljava/lang/Object;

    new-instance v2, Lcom/miui/server/SecurityManagerService$Lifecycle$$ExternalSyntheticLambda0;

    invoke-direct {v2, v1}, Lcom/miui/server/SecurityManagerService$Lifecycle$$ExternalSyntheticLambda0;-><init>(Lcom/miui/server/SecurityManagerService;)V

    invoke-virtual {v0, v2}, Lcom/miui/server/security/SecurityWriteHandler;->post(Ljava/lang/Runnable;)Z

    .line 1583
    :cond_0
    return-void
.end method

.method public onStart()V
    .locals 2

    .line 1575
    const-string/jumbo v0, "security"

    iget-object v1, p0, Lcom/miui/server/SecurityManagerService$Lifecycle;->mService:Lcom/miui/server/SecurityManagerService;

    invoke-virtual {p0, v0, v1}, Lcom/miui/server/SecurityManagerService$Lifecycle;->publishBinderService(Ljava/lang/String;Landroid/os/IBinder;)V

    .line 1576
    return-void
.end method
