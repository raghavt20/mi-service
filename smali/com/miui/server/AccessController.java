public class com.miui.server.AccessController {
	 /* .source "AccessController.java" */
	 /* # annotations */
	 /* .annotation system Ldalvik/annotation/MemberClasses; */
	 /* value = { */
	 /* Lcom/miui/server/AccessController$WorkHandler; */
	 /* } */
} // .end annotation
/* # static fields */
private static final java.lang.String ACCESS_CONTROL;
private static final java.lang.String ACCESS_CONTROL_KEYSTORE;
private static final java.lang.String ACCESS_CONTROL_PASSWORD_TYPE_KEY;
private static final java.lang.String APPLOCK_WHILTE;
public static final java.lang.String APP_LOCK_CLASSNAME;
public static final Boolean DEBUG;
private static final java.lang.String GAMEBOOSTER_ANTIMSG;
private static final java.lang.String GAMEBOOSTER_PAY;
public static final java.lang.String PACKAGE_CAMERA;
public static final java.lang.String PACKAGE_GALLERY;
public static final java.lang.String PACKAGE_MEITU_CAMERA;
public static final java.lang.String PACKAGE_SYSTEMUI;
private static final java.lang.String PASSWORD_TYPE_PATTERN;
public static final java.lang.String SKIP_INTERCEPT_ACTIVITY_GALLERY_EDIT;
public static final java.lang.String SKIP_INTERCEPT_ACTIVITY_GALLERY_EXTRA;
public static final java.lang.String SKIP_INTERCEPT_ACTIVITY_GALLERY_EXTRA_TRANSLUCENT;
private static final java.lang.String SYSTEM_DIRECTORY;
private static final java.lang.String TAG;
private static final Long UPDATE_EVERY_DELAY;
private static final Long UPDATE_FIRT_DELAY;
private static final Integer UPDATE_WHITE_LIST;
private static final java.lang.String WECHAT_VIDEO_ACTIVITY_CLASSNAME;
private static android.util.ArrayMap mAntimsgInterceptList;
/* .annotation system Ldalvik/annotation/Signature; */
/* value = { */
/* "Landroid/util/ArrayMap<", */
/* "Ljava/lang/String;", */
/* "Ljava/util/ArrayList<", */
/* "Landroid/content/Intent;", */
/* ">;>;" */
/* } */
} // .end annotation
} // .end field
private static java.util.List mForceLaunchList;
/* .annotation system Ldalvik/annotation/Signature; */
/* value = { */
/* "Ljava/util/List<", */
/* "Ljava/lang/String;", */
/* ">;" */
/* } */
} // .end annotation
} // .end field
private static java.lang.reflect.Method mPasswordToHash;
private static android.util.ArrayMap mPayInterceptList;
/* .annotation system Ldalvik/annotation/Signature; */
/* value = { */
/* "Landroid/util/ArrayMap<", */
/* "Ljava/lang/String;", */
/* "Ljava/util/ArrayList<", */
/* "Landroid/content/Intent;", */
/* ">;>;" */
/* } */
} // .end annotation
} // .end field
private static android.util.ArrayMap mSkipList;
/* .annotation system Ldalvik/annotation/Signature; */
/* value = { */
/* "Landroid/util/ArrayMap<", */
/* "Ljava/lang/String;", */
/* "Ljava/util/ArrayList<", */
/* "Landroid/content/Intent;", */
/* ">;>;" */
/* } */
} // .end annotation
} // .end field
/* # instance fields */
private android.content.Context mContext;
private final java.lang.Object mFileWriteLock;
private final android.security.AccessControlKeystoreHelper mKeystoreHelper;
private com.android.internal.widget.LockPatternUtils mLockPatternUtils;
private com.miui.server.AccessController$WorkHandler mWorkHandler;
/* # direct methods */
static com.miui.server.AccessController ( ) {
/* .locals 13 */
/* .line 87 */
final String v0 = "AccessController"; // const-string v0, "AccessController"
/* new-instance v1, Landroid/util/ArrayMap; */
/* invoke-direct {v1}, Landroid/util/ArrayMap;-><init>()V */
/* .line 89 */
/* new-instance v1, Landroid/util/ArrayMap; */
/* invoke-direct {v1}, Landroid/util/ArrayMap;-><init>()V */
/* .line 92 */
/* new-instance v1, Landroid/util/ArrayMap; */
/* invoke-direct {v1}, Landroid/util/ArrayMap;-><init>()V */
/* .line 99 */
/* new-instance v1, Ljava/util/ArrayList; */
/* invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V */
/* .line 109 */
/* new-instance v1, Ljava/util/ArrayList; */
/* invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V */
/* .line 110 */
/* .local v1, "passList":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Landroid/util/Pair<Ljava/lang/String;Ljava/lang/String;>;>;" */
/* new-instance v2, Landroid/util/Pair; */
final String v3 = "com.tencent.av.ui.VideoInviteLock"; // const-string v3, "com.tencent.av.ui.VideoInviteLock"
final String v4 = "com.tencent.mobileqq"; // const-string v4, "com.tencent.mobileqq"
/* invoke-direct {v2, v4, v3}, Landroid/util/Pair;-><init>(Ljava/lang/Object;Ljava/lang/Object;)V */
(( java.util.ArrayList ) v1 ).add ( v2 ); // invoke-virtual {v1, v2}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z
/* .line 111 */
/* new-instance v2, Landroid/util/Pair; */
final String v3 = "com.tencent.av.ui.VideoInviteFull"; // const-string v3, "com.tencent.av.ui.VideoInviteFull"
/* invoke-direct {v2, v4, v3}, Landroid/util/Pair;-><init>(Ljava/lang/Object;Ljava/lang/Object;)V */
(( java.util.ArrayList ) v1 ).add ( v2 ); // invoke-virtual {v1, v2}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z
/* .line 112 */
/* new-instance v2, Landroid/util/Pair; */
final String v3 = "com.tencent.av.ui.VideoInviteActivity"; // const-string v3, "com.tencent.av.ui.VideoInviteActivity"
/* invoke-direct {v2, v4, v3}, Landroid/util/Pair;-><init>(Ljava/lang/Object;Ljava/lang/Object;)V */
(( java.util.ArrayList ) v1 ).add ( v2 ); // invoke-virtual {v1, v2}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z
/* .line 113 */
/* new-instance v2, Landroid/util/Pair; */
final String v3 = "com.tencent.mm.plugin.voip.ui.VideoActivity"; // const-string v3, "com.tencent.mm.plugin.voip.ui.VideoActivity"
final String v5 = "com.tencent.mm"; // const-string v5, "com.tencent.mm"
/* invoke-direct {v2, v5, v3}, Landroid/util/Pair;-><init>(Ljava/lang/Object;Ljava/lang/Object;)V */
(( java.util.ArrayList ) v1 ).add ( v2 ); // invoke-virtual {v1, v2}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z
/* .line 114 */
/* new-instance v2, Landroid/util/Pair; */
final String v3 = "com.tencent.mm.plugin.multitalk.ui.MultiTalkMainUI"; // const-string v3, "com.tencent.mm.plugin.multitalk.ui.MultiTalkMainUI"
/* invoke-direct {v2, v5, v3}, Landroid/util/Pair;-><init>(Ljava/lang/Object;Ljava/lang/Object;)V */
(( java.util.ArrayList ) v1 ).add ( v2 ); // invoke-virtual {v1, v2}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z
/* .line 115 */
/* new-instance v2, Landroid/util/Pair; */
final String v3 = "com.tencent.mm.plugin.base.stub.UIEntryStub"; // const-string v3, "com.tencent.mm.plugin.base.stub.UIEntryStub"
/* invoke-direct {v2, v5, v3}, Landroid/util/Pair;-><init>(Ljava/lang/Object;Ljava/lang/Object;)V */
(( java.util.ArrayList ) v1 ).add ( v2 ); // invoke-virtual {v1, v2}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z
/* .line 116 */
/* new-instance v2, Landroid/util/Pair; */
final String v3 = "com.tencent.mm.plugin.webview.ui.tools.SDKOAuthUI"; // const-string v3, "com.tencent.mm.plugin.webview.ui.tools.SDKOAuthUI"
/* invoke-direct {v2, v5, v3}, Landroid/util/Pair;-><init>(Ljava/lang/Object;Ljava/lang/Object;)V */
(( java.util.ArrayList ) v1 ).add ( v2 ); // invoke-virtual {v1, v2}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z
/* .line 117 */
/* new-instance v2, Landroid/util/Pair; */
final String v3 = "com.tencent.mm.plugin.base.stub.WXPayEntryActivity"; // const-string v3, "com.tencent.mm.plugin.base.stub.WXPayEntryActivity"
/* invoke-direct {v2, v5, v3}, Landroid/util/Pair;-><init>(Ljava/lang/Object;Ljava/lang/Object;)V */
(( java.util.ArrayList ) v1 ).add ( v2 ); // invoke-virtual {v1, v2}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z
/* .line 118 */
/* new-instance v2, Landroid/util/Pair; */
final String v6 = "com.tencent.mm.plugin.wallet_index.ui.OrderHandlerUI"; // const-string v6, "com.tencent.mm.plugin.wallet_index.ui.OrderHandlerUI"
/* invoke-direct {v2, v5, v6}, Landroid/util/Pair;-><init>(Ljava/lang/Object;Ljava/lang/Object;)V */
(( java.util.ArrayList ) v1 ).add ( v2 ); // invoke-virtual {v1, v2}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z
/* .line 119 */
/* new-instance v2, Landroid/util/Pair; */
final String v6 = "com.whatsapp.VoipActivity"; // const-string v6, "com.whatsapp.VoipActivity"
final String v7 = "com.whatsapp"; // const-string v7, "com.whatsapp"
/* invoke-direct {v2, v7, v6}, Landroid/util/Pair;-><init>(Ljava/lang/Object;Ljava/lang/Object;)V */
(( java.util.ArrayList ) v1 ).add ( v2 ); // invoke-virtual {v1, v2}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z
/* .line 120 */
/* new-instance v2, Landroid/util/Pair; */
final String v6 = "com.whatsapp.voipcalling.VoipActivityV2"; // const-string v6, "com.whatsapp.voipcalling.VoipActivityV2"
/* invoke-direct {v2, v7, v6}, Landroid/util/Pair;-><init>(Ljava/lang/Object;Ljava/lang/Object;)V */
(( java.util.ArrayList ) v1 ).add ( v2 ); // invoke-virtual {v1, v2}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z
/* .line 121 */
/* new-instance v2, Landroid/util/Pair; */
final String v6 = "jp.naver.line.android"; // const-string v6, "jp.naver.line.android"
final String v7 = "jp.naver.line.android.freecall.FreeCallActivity"; // const-string v7, "jp.naver.line.android.freecall.FreeCallActivity"
/* invoke-direct {v2, v6, v7}, Landroid/util/Pair;-><init>(Ljava/lang/Object;Ljava/lang/Object;)V */
(( java.util.ArrayList ) v1 ).add ( v2 ); // invoke-virtual {v1, v2}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z
/* .line 123 */
/* new-instance v2, Landroid/util/Pair; */
final String v6 = "com.bbm"; // const-string v6, "com.bbm"
final String v7 = "com.bbm.ui.voice.activities.IncomingCallActivity"; // const-string v7, "com.bbm.ui.voice.activities.IncomingCallActivity"
/* invoke-direct {v2, v6, v7}, Landroid/util/Pair;-><init>(Ljava/lang/Object;Ljava/lang/Object;)V */
(( java.util.ArrayList ) v1 ).add ( v2 ); // invoke-virtual {v1, v2}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z
/* .line 124 */
/* new-instance v2, Landroid/util/Pair; */
final String v6 = "com.xiaomi.channel"; // const-string v6, "com.xiaomi.channel"
final String v7 = "com.xiaomi.channel.voip.VoipCallActivity"; // const-string v7, "com.xiaomi.channel.voip.VoipCallActivity"
/* invoke-direct {v2, v6, v7}, Landroid/util/Pair;-><init>(Ljava/lang/Object;Ljava/lang/Object;)V */
(( java.util.ArrayList ) v1 ).add ( v2 ); // invoke-virtual {v1, v2}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z
/* .line 125 */
/* new-instance v2, Landroid/util/Pair; */
final String v6 = "com.facebook.orca"; // const-string v6, "com.facebook.orca"
final String v7 = "com.facebook.rtc.activities.WebrtcIncallActivity"; // const-string v7, "com.facebook.rtc.activities.WebrtcIncallActivity"
/* invoke-direct {v2, v6, v7}, Landroid/util/Pair;-><init>(Ljava/lang/Object;Ljava/lang/Object;)V */
(( java.util.ArrayList ) v1 ).add ( v2 ); // invoke-virtual {v1, v2}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z
/* .line 126 */
/* new-instance v2, Landroid/util/Pair; */
final String v6 = "com.bsb.hike"; // const-string v6, "com.bsb.hike"
final String v7 = "com.bsb.hike.voip.view.VoIPActivity"; // const-string v7, "com.bsb.hike.voip.view.VoIPActivity"
/* invoke-direct {v2, v6, v7}, Landroid/util/Pair;-><init>(Ljava/lang/Object;Ljava/lang/Object;)V */
(( java.util.ArrayList ) v1 ).add ( v2 ); // invoke-virtual {v1, v2}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z
/* .line 127 */
/* new-instance v2, Landroid/util/Pair; */
final String v6 = "com.alipay.android.app.TransProcessPayActivity"; // const-string v6, "com.alipay.android.app.TransProcessPayActivity"
final String v7 = "com.eg.android.AlipayGphone"; // const-string v7, "com.eg.android.AlipayGphone"
/* invoke-direct {v2, v7, v6}, Landroid/util/Pair;-><init>(Ljava/lang/Object;Ljava/lang/Object;)V */
(( java.util.ArrayList ) v1 ).add ( v2 ); // invoke-virtual {v1, v2}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z
/* .line 129 */
/* new-instance v2, Landroid/util/Pair; */
final String v6 = "com.alipay.mobile.security.login.ui.AlipayUserLoginActivity"; // const-string v6, "com.alipay.mobile.security.login.ui.AlipayUserLoginActivity"
/* invoke-direct {v2, v7, v6}, Landroid/util/Pair;-><init>(Ljava/lang/Object;Ljava/lang/Object;)V */
(( java.util.ArrayList ) v1 ).add ( v2 ); // invoke-virtual {v1, v2}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z
/* .line 131 */
/* new-instance v2, Landroid/util/Pair; */
final String v6 = "com.alipay.mobile.bill.detail.ui.EmptyActivity_"; // const-string v6, "com.alipay.mobile.bill.detail.ui.EmptyActivity_"
/* invoke-direct {v2, v7, v6}, Landroid/util/Pair;-><init>(Ljava/lang/Object;Ljava/lang/Object;)V */
(( java.util.ArrayList ) v1 ).add ( v2 ); // invoke-virtual {v1, v2}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z
/* .line 133 */
/* new-instance v2, Landroid/util/Pair; */
final String v6 = "com.xiaomi.smarthome"; // const-string v6, "com.xiaomi.smarthome"
final String v8 = "com.xiaomi.smarthome.miio.activity.ClientAllLockedActivity"; // const-string v8, "com.xiaomi.smarthome.miio.activity.ClientAllLockedActivity"
/* invoke-direct {v2, v6, v8}, Landroid/util/Pair;-><init>(Ljava/lang/Object;Ljava/lang/Object;)V */
(( java.util.ArrayList ) v1 ).add ( v2 ); // invoke-virtual {v1, v2}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z
/* .line 135 */
/* new-instance v2, Landroid/util/Pair; */
final String v6 = "com.google.android.dialer"; // const-string v6, "com.google.android.dialer"
final String v8 = "com.android.dialer.incall.activity.ui.InCallActivity"; // const-string v8, "com.android.dialer.incall.activity.ui.InCallActivity"
/* invoke-direct {v2, v6, v8}, Landroid/util/Pair;-><init>(Ljava/lang/Object;Ljava/lang/Object;)V */
(( java.util.ArrayList ) v1 ).add ( v2 ); // invoke-virtual {v1, v2}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z
/* .line 136 */
/* new-instance v2, Landroid/util/Pair; */
final String v6 = "com.android.settings.FallbackHome"; // const-string v6, "com.android.settings.FallbackHome"
final String v8 = "com.android.settings"; // const-string v8, "com.android.settings"
/* invoke-direct {v2, v8, v6}, Landroid/util/Pair;-><init>(Ljava/lang/Object;Ljava/lang/Object;)V */
(( java.util.ArrayList ) v1 ).add ( v2 ); // invoke-virtual {v1, v2}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z
/* .line 137 */
/* new-instance v2, Landroid/util/Pair; */
final String v6 = "com.android.mms.ui.DummyActivity"; // const-string v6, "com.android.mms.ui.DummyActivity"
final String v9 = "com.android.mms"; // const-string v9, "com.android.mms"
/* invoke-direct {v2, v9, v6}, Landroid/util/Pair;-><init>(Ljava/lang/Object;Ljava/lang/Object;)V */
(( java.util.ArrayList ) v1 ).add ( v2 ); // invoke-virtual {v1, v2}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z
/* .line 138 */
/* new-instance v2, Landroid/util/Pair; */
final String v6 = "com.android.mms.ui.ComposeMessageRouterActivity"; // const-string v6, "com.android.mms.ui.ComposeMessageRouterActivity"
/* invoke-direct {v2, v9, v6}, Landroid/util/Pair;-><init>(Ljava/lang/Object;Ljava/lang/Object;)V */
(( java.util.ArrayList ) v1 ).add ( v2 ); // invoke-virtual {v1, v2}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z
/* .line 139 */
/* new-instance v2, Landroid/util/Pair; */
final String v6 = "com.xiaomi.jr"; // const-string v6, "com.xiaomi.jr"
final String v9 = "com.xiaomi.jr.EntryActivity"; // const-string v9, "com.xiaomi.jr.EntryActivity"
/* invoke-direct {v2, v6, v9}, Landroid/util/Pair;-><init>(Ljava/lang/Object;Ljava/lang/Object;)V */
(( java.util.ArrayList ) v1 ).add ( v2 ); // invoke-virtual {v1, v2}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z
/* .line 140 */
/* new-instance v2, Landroid/util/Pair; */
final String v6 = "com.android.settings.MiuiConfirmCommonPassword"; // const-string v6, "com.android.settings.MiuiConfirmCommonPassword"
/* invoke-direct {v2, v8, v6}, Landroid/util/Pair;-><init>(Ljava/lang/Object;Ljava/lang/Object;)V */
(( java.util.ArrayList ) v1 ).add ( v2 ); // invoke-virtual {v1, v2}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z
/* .line 142 */
(( java.util.ArrayList ) v1 ).iterator ( ); // invoke-virtual {v1}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;
v6 = } // :goto_0
int v8 = 1; // const/4 v8, 0x1
if ( v6 != null) { // if-eqz v6, :cond_1
/* check-cast v6, Landroid/util/Pair; */
/* .line 143 */
/* .local v6, "pair":Landroid/util/Pair;, "Landroid/util/Pair<Ljava/lang/String;Ljava/lang/String;>;" */
v9 = com.miui.server.AccessController.mSkipList;
v10 = this.first;
(( android.util.ArrayMap ) v9 ).get ( v10 ); // invoke-virtual {v9, v10}, Landroid/util/ArrayMap;->get(Ljava/lang/Object;)Ljava/lang/Object;
/* check-cast v9, Ljava/util/ArrayList; */
/* .line 144 */
/* .local v9, "intents":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Landroid/content/Intent;>;" */
/* if-nez v9, :cond_0 */
/* .line 145 */
/* new-instance v10, Ljava/util/ArrayList; */
/* invoke-direct {v10, v8}, Ljava/util/ArrayList;-><init>(I)V */
/* move-object v9, v10 */
/* .line 146 */
v8 = com.miui.server.AccessController.mSkipList;
v10 = this.first;
/* check-cast v10, Ljava/lang/String; */
(( android.util.ArrayMap ) v8 ).put ( v10, v9 ); // invoke-virtual {v8, v10, v9}, Landroid/util/ArrayMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;
/* .line 148 */
} // :cond_0
/* new-instance v8, Landroid/content/Intent; */
/* invoke-direct {v8}, Landroid/content/Intent;-><init>()V */
/* .line 149 */
/* .local v8, "intent":Landroid/content/Intent; */
/* new-instance v10, Landroid/content/ComponentName; */
v11 = this.first;
/* check-cast v11, Ljava/lang/String; */
v12 = this.second;
/* check-cast v12, Ljava/lang/String; */
/* invoke-direct {v10, v11, v12}, Landroid/content/ComponentName;-><init>(Ljava/lang/String;Ljava/lang/String;)V */
(( android.content.Intent ) v8 ).setComponent ( v10 ); // invoke-virtual {v8, v10}, Landroid/content/Intent;->setComponent(Landroid/content/ComponentName;)Landroid/content/Intent;
/* .line 150 */
(( java.util.ArrayList ) v9 ).add ( v8 ); // invoke-virtual {v9, v8}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z
/* .line 151 */
} // .end local v6 # "pair":Landroid/util/Pair;, "Landroid/util/Pair<Ljava/lang/String;Ljava/lang/String;>;"
} // .end local v8 # "intent":Landroid/content/Intent;
} // .end local v9 # "intents":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Landroid/content/Intent;>;"
/* .line 154 */
} // :cond_1
/* new-instance v2, Ljava/util/ArrayList; */
/* invoke-direct {v2}, Ljava/util/ArrayList;-><init>()V */
/* .line 155 */
/* .local v2, "payList":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Landroid/util/Pair<Ljava/lang/String;Ljava/lang/String;>;>;" */
/* new-instance v6, Landroid/util/Pair; */
/* invoke-direct {v6, v5, v3}, Landroid/util/Pair;-><init>(Ljava/lang/Object;Ljava/lang/Object;)V */
(( java.util.ArrayList ) v2 ).add ( v6 ); // invoke-virtual {v2, v6}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z
/* .line 156 */
/* new-instance v3, Landroid/util/Pair; */
final String v6 = "com.tencent.mm.plugin.base.stub.WXEntryActivity"; // const-string v6, "com.tencent.mm.plugin.base.stub.WXEntryActivity"
/* invoke-direct {v3, v5, v6}, Landroid/util/Pair;-><init>(Ljava/lang/Object;Ljava/lang/Object;)V */
(( java.util.ArrayList ) v2 ).add ( v3 ); // invoke-virtual {v2, v3}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z
/* .line 157 */
/* new-instance v3, Landroid/util/Pair; */
final String v6 = "com.tencent.mm.plugin.base.stub.WXCustomSchemeEntryActivity"; // const-string v6, "com.tencent.mm.plugin.base.stub.WXCustomSchemeEntryActivity"
/* invoke-direct {v3, v5, v6}, Landroid/util/Pair;-><init>(Ljava/lang/Object;Ljava/lang/Object;)V */
(( java.util.ArrayList ) v2 ).add ( v3 ); // invoke-virtual {v2, v3}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z
/* .line 158 */
/* new-instance v3, Landroid/util/Pair; */
final String v5 = "cooperation.qwallet.open.AppPayActivity"; // const-string v5, "cooperation.qwallet.open.AppPayActivity"
/* invoke-direct {v3, v4, v5}, Landroid/util/Pair;-><init>(Ljava/lang/Object;Ljava/lang/Object;)V */
(( java.util.ArrayList ) v2 ).add ( v3 ); // invoke-virtual {v2, v3}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z
/* .line 159 */
/* new-instance v3, Landroid/util/Pair; */
final String v5 = "com.tencent.open.agent.AgentActivity"; // const-string v5, "com.tencent.open.agent.AgentActivity"
/* invoke-direct {v3, v4, v5}, Landroid/util/Pair;-><init>(Ljava/lang/Object;Ljava/lang/Object;)V */
(( java.util.ArrayList ) v2 ).add ( v3 ); // invoke-virtual {v2, v3}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z
/* .line 160 */
/* new-instance v3, Landroid/util/Pair; */
final String v5 = "com.tencent.mobileqq.activity.JumpActivity"; // const-string v5, "com.tencent.mobileqq.activity.JumpActivity"
/* invoke-direct {v3, v4, v5}, Landroid/util/Pair;-><init>(Ljava/lang/Object;Ljava/lang/Object;)V */
(( java.util.ArrayList ) v2 ).add ( v3 ); // invoke-virtual {v2, v3}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z
/* .line 161 */
/* new-instance v3, Landroid/util/Pair; */
final String v5 = "com.sina.weibo"; // const-string v5, "com.sina.weibo"
final String v6 = "com.sina.weibo.composerinde.ComposerDispatchActivity"; // const-string v6, "com.sina.weibo.composerinde.ComposerDispatchActivity"
/* invoke-direct {v3, v5, v6}, Landroid/util/Pair;-><init>(Ljava/lang/Object;Ljava/lang/Object;)V */
(( java.util.ArrayList ) v2 ).add ( v3 ); // invoke-virtual {v2, v3}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z
/* .line 162 */
/* new-instance v3, Landroid/util/Pair; */
final String v5 = "com.alipay.mobile.quinox.SchemeLauncherActivity"; // const-string v5, "com.alipay.mobile.quinox.SchemeLauncherActivity"
/* invoke-direct {v3, v7, v5}, Landroid/util/Pair;-><init>(Ljava/lang/Object;Ljava/lang/Object;)V */
(( java.util.ArrayList ) v2 ).add ( v3 ); // invoke-virtual {v2, v3}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z
/* .line 164 */
/* new-instance v3, Landroid/util/Pair; */
final String v5 = "com.ali.user.mobile.loginupgrade.activity.GuideActivity"; // const-string v5, "com.ali.user.mobile.loginupgrade.activity.GuideActivity"
/* invoke-direct {v3, v7, v5}, Landroid/util/Pair;-><init>(Ljava/lang/Object;Ljava/lang/Object;)V */
(( java.util.ArrayList ) v2 ).add ( v3 ); // invoke-virtual {v2, v3}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z
/* .line 165 */
/* new-instance v3, Landroid/util/Pair; */
final String v5 = "com.ali.user.mobile.loginupgrade.activity.LoginActivity"; // const-string v5, "com.ali.user.mobile.loginupgrade.activity.LoginActivity"
/* invoke-direct {v3, v7, v5}, Landroid/util/Pair;-><init>(Ljava/lang/Object;Ljava/lang/Object;)V */
(( java.util.ArrayList ) v2 ).add ( v3 ); // invoke-virtual {v2, v3}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z
/* .line 166 */
/* new-instance v3, Landroid/util/Pair; */
final String v5 = "com.alipay.mobile.security.login.ui.RecommandAlipayUserLoginActivity"; // const-string v5, "com.alipay.mobile.security.login.ui.RecommandAlipayUserLoginActivity"
/* invoke-direct {v3, v7, v5}, Landroid/util/Pair;-><init>(Ljava/lang/Object;Ljava/lang/Object;)V */
(( java.util.ArrayList ) v2 ).add ( v3 ); // invoke-virtual {v2, v3}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z
/* .line 168 */
/* new-instance v3, Landroid/util/Pair; */
final String v5 = "com.tencent.mobileqq.activity.LoginActivity"; // const-string v5, "com.tencent.mobileqq.activity.LoginActivity"
/* invoke-direct {v3, v4, v5}, Landroid/util/Pair;-><init>(Ljava/lang/Object;Ljava/lang/Object;)V */
(( java.util.ArrayList ) v2 ).add ( v3 ); // invoke-virtual {v2, v3}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z
/* .line 170 */
(( java.util.ArrayList ) v2 ).iterator ( ); // invoke-virtual {v2}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;
v4 = } // :goto_1
if ( v4 != null) { // if-eqz v4, :cond_3
/* check-cast v4, Landroid/util/Pair; */
/* .line 171 */
/* .local v4, "pair":Landroid/util/Pair;, "Landroid/util/Pair<Ljava/lang/String;Ljava/lang/String;>;" */
v5 = com.miui.server.AccessController.mPayInterceptList;
v6 = this.first;
(( android.util.ArrayMap ) v5 ).get ( v6 ); // invoke-virtual {v5, v6}, Landroid/util/ArrayMap;->get(Ljava/lang/Object;)Ljava/lang/Object;
/* check-cast v5, Ljava/util/ArrayList; */
/* .line 172 */
/* .local v5, "intents":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Landroid/content/Intent;>;" */
/* if-nez v5, :cond_2 */
/* .line 173 */
/* new-instance v6, Ljava/util/ArrayList; */
/* invoke-direct {v6, v8}, Ljava/util/ArrayList;-><init>(I)V */
/* move-object v5, v6 */
/* .line 174 */
v6 = com.miui.server.AccessController.mPayInterceptList;
v7 = this.first;
/* check-cast v7, Ljava/lang/String; */
(( android.util.ArrayMap ) v6 ).put ( v7, v5 ); // invoke-virtual {v6, v7, v5}, Landroid/util/ArrayMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;
/* .line 176 */
} // :cond_2
/* new-instance v6, Landroid/content/Intent; */
/* invoke-direct {v6}, Landroid/content/Intent;-><init>()V */
/* .line 177 */
/* .local v6, "intent":Landroid/content/Intent; */
/* new-instance v7, Landroid/content/ComponentName; */
v9 = this.first;
/* check-cast v9, Ljava/lang/String; */
v10 = this.second;
/* check-cast v10, Ljava/lang/String; */
/* invoke-direct {v7, v9, v10}, Landroid/content/ComponentName;-><init>(Ljava/lang/String;Ljava/lang/String;)V */
(( android.content.Intent ) v6 ).setComponent ( v7 ); // invoke-virtual {v6, v7}, Landroid/content/Intent;->setComponent(Landroid/content/ComponentName;)Landroid/content/Intent;
/* .line 178 */
(( java.util.ArrayList ) v5 ).add ( v6 ); // invoke-virtual {v5, v6}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z
/* .line 179 */
} // .end local v4 # "pair":Landroid/util/Pair;, "Landroid/util/Pair<Ljava/lang/String;Ljava/lang/String;>;"
} // .end local v5 # "intents":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Landroid/content/Intent;>;"
} // .end local v6 # "intent":Landroid/content/Intent;
/* .line 182 */
} // :cond_3
/* nop */
/* .line 183 */
try { // :try_start_0
final String v3 = "Nowhere to call this method, passwordToHash should not init"; // const-string v3, "Nowhere to call this method, passwordToHash should not init"
android.util.Log .i ( v0,v3 );
/* :try_end_0 */
/* .catch Ljava/lang/Exception; {:try_start_0 ..:try_end_0} :catch_0 */
/* .line 196 */
/* .line 194 */
/* :catch_0 */
/* move-exception v3 */
/* .line 195 */
/* .local v3, "e":Ljava/lang/Exception; */
final String v4 = " passwordToHash static invoke error"; // const-string v4, " passwordToHash static invoke error"
android.util.Log .e ( v0,v4,v3 );
/* .line 198 */
} // .end local v3 # "e":Ljava/lang/Exception;
} // :goto_2
v0 = com.miui.server.AccessController.mForceLaunchList;
final String v3 = "com.tencent.mobileqq/.activity.JumpActivity"; // const-string v3, "com.tencent.mobileqq/.activity.JumpActivity"
/* .line 199 */
v0 = com.miui.server.AccessController.mForceLaunchList;
final String v3 = "com.tencent.mm/.plugin.base.stub.WXCustomSchemeEntryActivity"; // const-string v3, "com.tencent.mm/.plugin.base.stub.WXCustomSchemeEntryActivity"
/* .line 200 */
v0 = com.miui.server.AccessController.mForceLaunchList;
final String v3 = "com.sina.weibo/.composerinde.ComposerDispatchActivity"; // const-string v3, "com.sina.weibo/.composerinde.ComposerDispatchActivity"
/* .line 201 */
v0 = com.miui.server.AccessController.mForceLaunchList;
final String v3 = "com.tencent.mobileqq/com.tencent.open.agent.AgentActivity"; // const-string v3, "com.tencent.mobileqq/com.tencent.open.agent.AgentActivity"
/* .line 202 */
v0 = com.miui.server.AccessController.mForceLaunchList;
final String v3 = "com.eg.android.AlipayGphone/com.alipay.android.msp.ui.views.MspContainerActivity"; // const-string v3, "com.eg.android.AlipayGphone/com.alipay.android.msp.ui.views.MspContainerActivity"
/* .line 203 */
} // .end local v1 # "passList":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Landroid/util/Pair<Ljava/lang/String;Ljava/lang/String;>;>;"
} // .end local v2 # "payList":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Landroid/util/Pair<Ljava/lang/String;Ljava/lang/String;>;>;"
return;
} // .end method
public com.miui.server.AccessController ( ) {
/* .locals 4 */
/* .param p1, "context" # Landroid/content/Context; */
/* .param p2, "looper" # Landroid/os/Looper; */
/* .line 220 */
/* invoke-direct {p0}, Ljava/lang/Object;-><init>()V */
/* .line 85 */
/* new-instance v0, Ljava/lang/Object; */
/* invoke-direct {v0}, Ljava/lang/Object;-><init>()V */
this.mFileWriteLock = v0;
/* .line 221 */
this.mContext = p1;
/* .line 222 */
/* new-instance v0, Lcom/miui/server/AccessController$WorkHandler; */
/* invoke-direct {v0, p0, p2}, Lcom/miui/server/AccessController$WorkHandler;-><init>(Lcom/miui/server/AccessController;Landroid/os/Looper;)V */
this.mWorkHandler = v0;
/* .line 223 */
int v1 = 1; // const/4 v1, 0x1
/* const-wide/32 v2, 0x2bf20 */
(( com.miui.server.AccessController$WorkHandler ) v0 ).sendEmptyMessageDelayed ( v1, v2, v3 ); // invoke-virtual {v0, v1, v2, v3}, Lcom/miui/server/AccessController$WorkHandler;->sendEmptyMessageDelayed(IJ)Z
/* .line 224 */
/* new-instance v0, Lcom/android/internal/widget/LockPatternUtils; */
/* invoke-direct {v0, p1}, Lcom/android/internal/widget/LockPatternUtils;-><init>(Landroid/content/Context;)V */
this.mLockPatternUtils = v0;
/* .line 225 */
/* new-instance v0, Landroid/security/AccessControlKeystoreHelper; */
/* invoke-direct {v0}, Landroid/security/AccessControlKeystoreHelper;-><init>()V */
this.mKeystoreHelper = v0;
/* .line 226 */
return;
} // .end method
private Boolean checkAccessControlPasswordByKeystore ( java.lang.String p0, Integer p1 ) {
/* .locals 5 */
/* .param p1, "password" # Ljava/lang/String; */
/* .param p2, "userId" # I */
/* .line 373 */
v0 = this.mKeystoreHelper;
v1 = this.mContext;
v0 = (( android.security.AccessControlKeystoreHelper ) v0 ).disableEncryptByKeystore ( v1 ); // invoke-virtual {v0, v1}, Landroid/security/AccessControlKeystoreHelper;->disableEncryptByKeystore(Landroid/content/Context;)Z
int v1 = 0; // const/4 v1, 0x0
if ( v0 != null) { // if-eqz v0, :cond_0
/* .line 374 */
/* .line 376 */
} // :cond_0
final String v0 = "access_control_keystore.key"; // const-string v0, "access_control_keystore.key"
/* invoke-direct {p0, p2, v0}, Lcom/miui/server/AccessController;->getFilePathForUser(ILjava/lang/String;)Ljava/lang/String; */
/* .line 377 */
/* .local v0, "filePath":Ljava/lang/String; */
/* invoke-direct {p0, v0}, Lcom/miui/server/AccessController;->readFile(Ljava/lang/String;)[B */
/* .line 378 */
/* .local v2, "readFile":[B */
/* new-instance v3, Ljava/lang/StringBuilder; */
/* invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V */
final String v4 = "checkAccessControlPasswordByKeystore, "; // const-string v4, "checkAccessControlPasswordByKeystore, "
(( java.lang.StringBuilder ) v3 ).append ( v4 ); // invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v3 ).append ( p2 ); // invoke-virtual {v3, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v3 ).toString ( ); // invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
final String v4 = "AccessController"; // const-string v4, "AccessController"
android.util.Slog .i ( v4,v3 );
/* .line 379 */
if ( v2 != null) { // if-eqz v2, :cond_2
/* array-length v3, v2 */
/* if-nez v3, :cond_1 */
/* .line 383 */
} // :cond_1
/* new-instance v1, Ljava/lang/StringBuilder; */
/* invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V */
final String v3 = "alias_app_lock_"; // const-string v3, "alias_app_lock_"
(( java.lang.StringBuilder ) v1 ).append ( v3 ); // invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v1 ).append ( p2 ); // invoke-virtual {v1, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v1 ).toString ( ); // invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
/* .line 384 */
/* .local v1, "alias":Ljava/lang/String; */
v3 = this.mKeystoreHelper;
(( android.security.AccessControlKeystoreHelper ) v3 ).encrypt ( v1, p1 ); // invoke-virtual {v3, v1, p1}, Landroid/security/AccessControlKeystoreHelper;->encrypt(Ljava/lang/String;Ljava/lang/String;)[B
/* .line 385 */
/* .local v3, "cipherText":[B */
v4 = java.util.Arrays .equals ( v3,v2 );
/* .line 380 */
} // .end local v1 # "alias":Ljava/lang/String;
} // .end local v3 # "cipherText":[B
} // :cond_2
} // :goto_0
final String v3 = "readFile error!"; // const-string v3, "readFile error!"
android.util.Slog .e ( v4,v3 );
/* .line 381 */
} // .end method
private Boolean checkAccessControlPattern ( java.lang.String p0, Integer p1 ) {
/* .locals 5 */
/* .param p1, "pattern" # Ljava/lang/String; */
/* .param p2, "userId" # I */
/* .line 415 */
/* if-nez p1, :cond_0 */
/* .line 416 */
int v0 = 0; // const/4 v0, 0x0
/* .line 418 */
} // :cond_0
android.security.MiuiLockPatternUtils .stringToPattern ( p1 );
/* .line 419 */
/* .local v0, "stringToPattern":Ljava/util/List;, "Ljava/util/List<Lcom/android/internal/widget/LockPatternView$Cell;>;" */
android.security.MiuiLockPatternUtils .patternToHash ( v0 );
/* .line 420 */
/* .local v1, "hash":[B */
final String v2 = "access_control.key"; // const-string v2, "access_control.key"
/* invoke-direct {p0, p2, v2}, Lcom/miui/server/AccessController;->getFilePathForUser(ILjava/lang/String;)Ljava/lang/String; */
/* .line 421 */
/* .local v2, "filePath":Ljava/lang/String; */
/* invoke-direct {p0, v2}, Lcom/miui/server/AccessController;->readFile(Ljava/lang/String;)[B */
/* .line 422 */
/* .local v3, "readFile":[B */
v4 = java.util.Arrays .equals ( v3,v1 );
} // .end method
private java.lang.String getFilePathForUser ( Integer p0, java.lang.String p1 ) {
/* .locals 3 */
/* .param p1, "userId" # I */
/* .param p2, "fileName" # Ljava/lang/String; */
/* .line 590 */
/* new-instance v0, Ljava/lang/StringBuilder; */
/* invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V */
android.os.Environment .getDataDirectory ( );
(( java.io.File ) v1 ).getAbsolutePath ( ); // invoke-virtual {v1}, Ljava/io/File;->getAbsolutePath()Ljava/lang/String;
(( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
final String v1 = "/system/"; // const-string v1, "/system/"
(( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v0 ).toString ( ); // invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
/* .line 591 */
/* .local v0, "dataSystemDirectory":Ljava/lang/String; */
/* if-nez p1, :cond_0 */
/* .line 593 */
/* new-instance v1, Ljava/lang/StringBuilder; */
/* invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V */
(( java.lang.StringBuilder ) v1 ).append ( v0 ); // invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v1 ).append ( p2 ); // invoke-virtual {v1, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v1 ).toString ( ); // invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
/* .line 595 */
} // :cond_0
/* new-instance v1, Ljava/io/File; */
android.os.Environment .getUserSystemDirectory ( p1 );
/* invoke-direct {v1, v2, p2}, Ljava/io/File;-><init>(Ljava/io/File;Ljava/lang/String;)V */
(( java.io.File ) v1 ).getAbsolutePath ( ); // invoke-virtual {v1}, Ljava/io/File;->getAbsolutePath()Ljava/lang/String;
} // .end method
private Boolean haveAccessControlPasswordType ( Integer p0 ) {
/* .locals 7 */
/* .param p1, "userId" # I */
/* .line 483 */
final String v0 = "access_control_password_type.key"; // const-string v0, "access_control_password_type.key"
/* invoke-direct {p0, p1, v0}, Lcom/miui/server/AccessController;->getFilePathForUser(ILjava/lang/String;)Ljava/lang/String; */
/* .line 484 */
/* .local v0, "filePath":Ljava/lang/String; */
v1 = this.mFileWriteLock;
/* monitor-enter v1 */
/* .line 485 */
try { // :try_start_0
/* new-instance v2, Ljava/io/File; */
/* invoke-direct {v2, v0}, Ljava/io/File;-><init>(Ljava/lang/String;)V */
/* .line 486 */
/* .local v2, "file":Ljava/io/File; */
v3 = (( java.io.File ) v2 ).exists ( ); // invoke-virtual {v2}, Ljava/io/File;->exists()Z
if ( v3 != null) { // if-eqz v3, :cond_0
(( java.io.File ) v2 ).length ( ); // invoke-virtual {v2}, Ljava/io/File;->length()J
/* move-result-wide v3 */
/* const-wide/16 v5, 0x0 */
/* cmp-long v3, v3, v5 */
/* if-lez v3, :cond_0 */
int v3 = 1; // const/4 v3, 0x1
} // :cond_0
int v3 = 0; // const/4 v3, 0x0
} // :goto_0
/* monitor-exit v1 */
/* .line 487 */
} // .end local v2 # "file":Ljava/io/File;
/* :catchall_0 */
/* move-exception v2 */
/* monitor-exit v1 */
/* :try_end_0 */
/* .catchall {:try_start_0 ..:try_end_0} :catchall_0 */
/* throw v2 */
} // .end method
private Boolean haveAccessControlPattern ( Integer p0 ) {
/* .locals 7 */
/* .param p1, "userId" # I */
/* .line 444 */
final String v0 = "access_control.key"; // const-string v0, "access_control.key"
/* invoke-direct {p0, p1, v0}, Lcom/miui/server/AccessController;->getFilePathForUser(ILjava/lang/String;)Ljava/lang/String; */
/* .line 445 */
/* .local v0, "filePath":Ljava/lang/String; */
v1 = this.mFileWriteLock;
/* monitor-enter v1 */
/* .line 446 */
try { // :try_start_0
/* new-instance v2, Ljava/io/File; */
/* invoke-direct {v2, v0}, Ljava/io/File;-><init>(Ljava/lang/String;)V */
/* .line 447 */
/* .local v2, "file":Ljava/io/File; */
v3 = (( java.io.File ) v2 ).exists ( ); // invoke-virtual {v2}, Ljava/io/File;->exists()Z
if ( v3 != null) { // if-eqz v3, :cond_0
(( java.io.File ) v2 ).length ( ); // invoke-virtual {v2}, Ljava/io/File;->length()J
/* move-result-wide v3 */
/* const-wide/16 v5, 0x0 */
/* cmp-long v3, v3, v5 */
/* if-lez v3, :cond_0 */
int v3 = 1; // const/4 v3, 0x1
} // :cond_0
int v3 = 0; // const/4 v3, 0x0
} // :goto_0
/* monitor-exit v1 */
/* .line 448 */
} // .end local v2 # "file":Ljava/io/File;
/* :catchall_0 */
/* move-exception v2 */
/* monitor-exit v1 */
/* :try_end_0 */
/* .catchall {:try_start_0 ..:try_end_0} :catchall_0 */
/* throw v2 */
} // .end method
private static Boolean isOpenedActivity ( java.lang.String p0 ) {
/* .locals 1 */
/* .param p0, "activity" # Ljava/lang/String; */
/* .line 351 */
final String v0 = "com.miui.gallery.activity.ExternalPhotoPageActivity"; // const-string v0, "com.miui.gallery.activity.ExternalPhotoPageActivity"
v0 = (( java.lang.String ) v0 ).equals ( p0 ); // invoke-virtual {v0, p0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z
/* if-nez v0, :cond_1 */
/* .line 352 */
final String v0 = "com.miui.gallery.activity.TranslucentExternalPhotoPageActivity"; // const-string v0, "com.miui.gallery.activity.TranslucentExternalPhotoPageActivity"
v0 = (( java.lang.String ) v0 ).equals ( p0 ); // invoke-virtual {v0, p0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z
/* if-nez v0, :cond_1 */
/* .line 353 */
final String v0 = "com.miui.gallery.editor.photo.screen.home.ScreenEditorActivity"; // const-string v0, "com.miui.gallery.editor.photo.screen.home.ScreenEditorActivity"
v0 = (( java.lang.String ) v0 ).equals ( p0 ); // invoke-virtual {v0, p0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z
if ( v0 != null) { // if-eqz v0, :cond_0
} // :cond_0
int v0 = 0; // const/4 v0, 0x0
} // :cond_1
} // :goto_0
int v0 = 1; // const/4 v0, 0x1
/* .line 351 */
} // :goto_1
} // .end method
private static Boolean isOpenedPkg ( java.lang.String p0 ) {
/* .locals 1 */
/* .param p0, "callingPkg" # Ljava/lang/String; */
/* .line 346 */
final String v0 = "com.miui.gallery"; // const-string v0, "com.miui.gallery"
v0 = (( java.lang.String ) v0 ).equals ( p0 ); // invoke-virtual {v0, p0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z
/* if-nez v0, :cond_1 */
final String v0 = "com.android.systemui"; // const-string v0, "com.android.systemui"
v0 = (( java.lang.String ) v0 ).equals ( p0 ); // invoke-virtual {v0, p0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z
/* if-nez v0, :cond_1 */
/* .line 347 */
final String v0 = "com.android.camera"; // const-string v0, "com.android.camera"
v0 = (( java.lang.String ) v0 ).equals ( p0 ); // invoke-virtual {v0, p0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z
/* if-nez v0, :cond_1 */
final String v0 = "com.mlab.cam"; // const-string v0, "com.mlab.cam"
v0 = (( java.lang.String ) v0 ).equals ( p0 ); // invoke-virtual {v0, p0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z
if ( v0 != null) { // if-eqz v0, :cond_0
} // :cond_0
int v0 = 0; // const/4 v0, 0x0
} // :cond_1
} // :goto_0
int v0 = 1; // const/4 v0, 0x1
/* .line 346 */
} // :goto_1
} // .end method
private passwordToHash ( java.lang.String p0, Integer p1 ) {
/* .locals 7 */
/* .param p1, "password" # Ljava/lang/String; */
/* .param p2, "userId" # I */
/* .line 675 */
v0 = android.text.TextUtils .isEmpty ( p1 );
int v1 = 0; // const/4 v1, 0x0
if ( v0 != null) { // if-eqz v0, :cond_0
/* .line 676 */
/* .line 680 */
} // :cond_0
/* nop */
/* .line 681 */
try { // :try_start_0
/* const-class v0, Lcom/android/internal/widget/LockPatternUtils; */
final String v2 = "getSalt"; // const-string v2, "getSalt"
int v3 = 1; // const/4 v3, 0x1
/* new-array v4, v3, [Ljava/lang/Class; */
v5 = java.lang.Integer.TYPE;
int v6 = 0; // const/4 v6, 0x0
/* aput-object v5, v4, v6 */
(( java.lang.Class ) v0 ).getDeclaredMethod ( v2, v4 ); // invoke-virtual {v0, v2, v4}, Ljava/lang/Class;->getDeclaredMethod(Ljava/lang/String;[Ljava/lang/Class;)Ljava/lang/reflect/Method;
/* .line 682 */
/* .local v0, "getSalt":Ljava/lang/reflect/Method; */
(( java.lang.reflect.Method ) v0 ).setAccessible ( v3 ); // invoke-virtual {v0, v3}, Ljava/lang/reflect/Method;->setAccessible(Z)V
/* .line 683 */
v2 = this.mLockPatternUtils;
/* new-array v3, v3, [Ljava/lang/Object; */
java.lang.Integer .valueOf ( p2 );
/* aput-object v4, v3, v6 */
(( java.lang.reflect.Method ) v0 ).invoke ( v2, v3 ); // invoke-virtual {v0, v2, v3}, Ljava/lang/reflect/Method;->invoke(Ljava/lang/Object;[Ljava/lang/Object;)Ljava/lang/Object;
/* check-cast v2, Ljava/lang/String; */
/* .line 684 */
/* .local v2, "salt":Ljava/lang/String; */
(( java.lang.String ) p1 ).getBytes ( ); // invoke-virtual {p1}, Ljava/lang/String;->getBytes()[B
/* .line 685 */
(( java.lang.String ) v2 ).getBytes ( ); // invoke-virtual {v2}, Ljava/lang/String;->getBytes()[B
/* .line 684 */
com.android.internal.widget.LockscreenCredential .legacyPasswordToHash ( v3,v4 );
/* move-object v0, v3 */
/* .line 686 */
} // .end local v2 # "salt":Ljava/lang/String;
/* .local v0, "hash":Ljava/lang/Object; */
/* nop */
/* .line 691 */
if ( v0 != null) { // if-eqz v0, :cond_1
/* .line 692 */
/* nop */
/* .line 693 */
/* move-object v2, v0 */
/* check-cast v2, Ljava/lang/String; */
v3 = java.nio.charset.StandardCharsets.UTF_8;
(( java.lang.String ) v2 ).getBytes ( v3 ); // invoke-virtual {v2, v3}, Ljava/lang/String;->getBytes(Ljava/nio/charset/Charset;)[B
/* :try_end_0 */
/* .catch Ljava/lang/Exception; {:try_start_0 ..:try_end_0} :catch_0 */
/* .line 700 */
} // .end local v0 # "hash":Ljava/lang/Object;
} // :cond_1
/* .line 698 */
/* :catch_0 */
/* move-exception v0 */
/* .line 699 */
/* .local v0, "e":Ljava/lang/Exception; */
final String v2 = "AccessController"; // const-string v2, "AccessController"
final String v3 = " passwordToHash invoke error"; // const-string v3, " passwordToHash invoke error"
android.util.Log .e ( v2,v3,v0 );
/* .line 701 */
} // .end local v0 # "e":Ljava/lang/Exception;
} // :goto_0
} // .end method
private readFile ( java.lang.String p0 ) {
/* .locals 8 */
/* .param p1, "name" # Ljava/lang/String; */
/* .line 491 */
v0 = this.mFileWriteLock;
/* monitor-enter v0 */
/* .line 492 */
int v1 = 0; // const/4 v1, 0x0
/* .line 493 */
/* .local v1, "raf":Ljava/io/RandomAccessFile; */
int v2 = 0; // const/4 v2, 0x0
/* .line 495 */
/* .local v2, "stored":[B */
try { // :try_start_0
/* new-instance v3, Ljava/io/RandomAccessFile; */
final String v4 = "r"; // const-string v4, "r"
/* invoke-direct {v3, p1, v4}, Ljava/io/RandomAccessFile;-><init>(Ljava/lang/String;Ljava/lang/String;)V */
/* move-object v1, v3 */
/* .line 496 */
(( java.io.RandomAccessFile ) v1 ).length ( ); // invoke-virtual {v1}, Ljava/io/RandomAccessFile;->length()J
/* move-result-wide v3 */
/* long-to-int v3, v3 */
/* new-array v3, v3, [B */
/* move-object v2, v3 */
/* .line 497 */
/* array-length v3, v2 */
int v4 = 0; // const/4 v4, 0x0
(( java.io.RandomAccessFile ) v1 ).readFully ( v2, v4, v3 ); // invoke-virtual {v1, v2, v4, v3}, Ljava/io/RandomAccessFile;->readFully([BII)V
/* .line 498 */
(( java.io.RandomAccessFile ) v1 ).close ( ); // invoke-virtual {v1}, Ljava/io/RandomAccessFile;->close()V
/* :try_end_0 */
/* .catch Ljava/io/IOException; {:try_start_0 ..:try_end_0} :catch_1 */
/* .catchall {:try_start_0 ..:try_end_0} :catchall_0 */
/* .line 502 */
/* nop */
/* .line 504 */
try { // :try_start_1
(( java.io.RandomAccessFile ) v1 ).close ( ); // invoke-virtual {v1}, Ljava/io/RandomAccessFile;->close()V
/* :try_end_1 */
/* .catch Ljava/io/IOException; {:try_start_1 ..:try_end_1} :catch_0 */
/* .catchall {:try_start_1 ..:try_end_1} :catchall_1 */
/* .line 507 */
} // :goto_0
/* .line 505 */
/* :catch_0 */
/* move-exception v3 */
/* .line 506 */
/* .local v3, "e":Ljava/io/IOException; */
try { // :try_start_2
final String v4 = "AccessController"; // const-string v4, "AccessController"
/* new-instance v5, Ljava/lang/StringBuilder; */
/* invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V */
final String v6 = "Error closing file "; // const-string v6, "Error closing file "
(( java.lang.StringBuilder ) v5 ).append ( v6 ); // invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v5 ).append ( v3 ); // invoke-virtual {v5, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v5 ).toString ( ); // invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
} // :goto_1
android.util.Slog .e ( v4,v5 );
/* :try_end_2 */
/* .catchall {:try_start_2 ..:try_end_2} :catchall_1 */
/* .line 507 */
/* nop */
} // .end local v3 # "e":Ljava/io/IOException;
/* .line 502 */
/* :catchall_0 */
/* move-exception v3 */
/* .line 499 */
/* :catch_1 */
/* move-exception v3 */
/* .line 500 */
/* .restart local v3 # "e":Ljava/io/IOException; */
try { // :try_start_3
final String v4 = "AccessController"; // const-string v4, "AccessController"
/* new-instance v5, Ljava/lang/StringBuilder; */
/* invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V */
final String v6 = "Cannot read file "; // const-string v6, "Cannot read file "
(( java.lang.StringBuilder ) v5 ).append ( v6 ); // invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v5 ).append ( v3 ); // invoke-virtual {v5, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v5 ).toString ( ); // invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
android.util.Slog .e ( v4,v5 );
/* :try_end_3 */
/* .catchall {:try_start_3 ..:try_end_3} :catchall_0 */
/* .line 502 */
/* nop */
} // .end local v3 # "e":Ljava/io/IOException;
if ( v1 != null) { // if-eqz v1, :cond_0
/* .line 504 */
try { // :try_start_4
(( java.io.RandomAccessFile ) v1 ).close ( ); // invoke-virtual {v1}, Ljava/io/RandomAccessFile;->close()V
/* :try_end_4 */
/* .catch Ljava/io/IOException; {:try_start_4 ..:try_end_4} :catch_2 */
/* .catchall {:try_start_4 ..:try_end_4} :catchall_1 */
/* .line 505 */
/* :catch_2 */
/* move-exception v3 */
/* .line 506 */
/* .restart local v3 # "e":Ljava/io/IOException; */
try { // :try_start_5
final String v4 = "AccessController"; // const-string v4, "AccessController"
/* new-instance v5, Ljava/lang/StringBuilder; */
/* invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V */
final String v6 = "Error closing file "; // const-string v6, "Error closing file "
(( java.lang.StringBuilder ) v5 ).append ( v6 ); // invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v5 ).append ( v3 ); // invoke-virtual {v5, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v5 ).toString ( ); // invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
/* .line 510 */
} // .end local v3 # "e":Ljava/io/IOException;
} // :cond_0
} // :goto_2
/* monitor-exit v0 */
/* :try_end_5 */
/* .catchall {:try_start_5 ..:try_end_5} :catchall_1 */
/* .line 502 */
} // :goto_3
if ( v1 != null) { // if-eqz v1, :cond_1
/* .line 504 */
try { // :try_start_6
(( java.io.RandomAccessFile ) v1 ).close ( ); // invoke-virtual {v1}, Ljava/io/RandomAccessFile;->close()V
/* :try_end_6 */
/* .catch Ljava/io/IOException; {:try_start_6 ..:try_end_6} :catch_3 */
/* .catchall {:try_start_6 ..:try_end_6} :catchall_1 */
/* .line 507 */
/* .line 505 */
/* :catch_3 */
/* move-exception v4 */
/* .line 506 */
/* .local v4, "e":Ljava/io/IOException; */
try { // :try_start_7
final String v5 = "AccessController"; // const-string v5, "AccessController"
/* new-instance v6, Ljava/lang/StringBuilder; */
/* invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V */
final String v7 = "Error closing file "; // const-string v7, "Error closing file "
(( java.lang.StringBuilder ) v6 ).append ( v7 ); // invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v6 ).append ( v4 ); // invoke-virtual {v6, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v6 ).toString ( ); // invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
android.util.Slog .e ( v5,v6 );
/* .line 509 */
} // .end local v4 # "e":Ljava/io/IOException;
} // :cond_1
} // :goto_4
/* nop */
} // .end local p0 # "this":Lcom/miui/server/AccessController;
} // .end local p1 # "name":Ljava/lang/String;
/* throw v3 */
/* .line 511 */
} // .end local v1 # "raf":Ljava/io/RandomAccessFile;
} // .end local v2 # "stored":[B
/* .restart local p0 # "this":Lcom/miui/server/AccessController; */
/* .restart local p1 # "name":Ljava/lang/String; */
/* :catchall_1 */
/* move-exception v1 */
/* monitor-exit v0 */
/* :try_end_7 */
/* .catchall {:try_start_7 ..:try_end_7} :catchall_1 */
/* throw v1 */
} // .end method
private java.lang.String readTypeFile ( java.lang.String p0 ) {
/* .locals 8 */
/* .param p1, "name" # Ljava/lang/String; */
/* .line 515 */
v0 = this.mFileWriteLock;
/* monitor-enter v0 */
/* .line 516 */
int v1 = 0; // const/4 v1, 0x0
/* .line 517 */
/* .local v1, "raf":Ljava/io/RandomAccessFile; */
int v2 = 0; // const/4 v2, 0x0
/* .line 519 */
/* .local v2, "stored":Ljava/lang/String; */
try { // :try_start_0
/* new-instance v3, Ljava/io/RandomAccessFile; */
final String v4 = "r"; // const-string v4, "r"
/* invoke-direct {v3, p1, v4}, Ljava/io/RandomAccessFile;-><init>(Ljava/lang/String;Ljava/lang/String;)V */
/* move-object v1, v3 */
/* .line 520 */
(( java.io.RandomAccessFile ) v1 ).readLine ( ); // invoke-virtual {v1}, Ljava/io/RandomAccessFile;->readLine()Ljava/lang/String;
/* move-object v2, v3 */
/* .line 521 */
(( java.io.RandomAccessFile ) v1 ).close ( ); // invoke-virtual {v1}, Ljava/io/RandomAccessFile;->close()V
/* :try_end_0 */
/* .catch Ljava/io/IOException; {:try_start_0 ..:try_end_0} :catch_1 */
/* .catchall {:try_start_0 ..:try_end_0} :catchall_0 */
/* .line 525 */
/* nop */
/* .line 527 */
try { // :try_start_1
(( java.io.RandomAccessFile ) v1 ).close ( ); // invoke-virtual {v1}, Ljava/io/RandomAccessFile;->close()V
/* :try_end_1 */
/* .catch Ljava/io/IOException; {:try_start_1 ..:try_end_1} :catch_0 */
/* .catchall {:try_start_1 ..:try_end_1} :catchall_1 */
/* .line 530 */
} // :goto_0
/* .line 528 */
/* :catch_0 */
/* move-exception v3 */
/* .line 529 */
/* .local v3, "e":Ljava/io/IOException; */
try { // :try_start_2
final String v4 = "AccessController"; // const-string v4, "AccessController"
/* new-instance v5, Ljava/lang/StringBuilder; */
/* invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V */
final String v6 = "Error closing file "; // const-string v6, "Error closing file "
(( java.lang.StringBuilder ) v5 ).append ( v6 ); // invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v5 ).append ( v3 ); // invoke-virtual {v5, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v5 ).toString ( ); // invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
} // :goto_1
android.util.Slog .e ( v4,v5 );
/* :try_end_2 */
/* .catchall {:try_start_2 ..:try_end_2} :catchall_1 */
/* .line 530 */
/* nop */
} // .end local v3 # "e":Ljava/io/IOException;
/* .line 525 */
/* :catchall_0 */
/* move-exception v3 */
/* .line 522 */
/* :catch_1 */
/* move-exception v3 */
/* .line 523 */
/* .restart local v3 # "e":Ljava/io/IOException; */
try { // :try_start_3
final String v4 = "AccessController"; // const-string v4, "AccessController"
/* new-instance v5, Ljava/lang/StringBuilder; */
/* invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V */
final String v6 = "Cannot read file "; // const-string v6, "Cannot read file "
(( java.lang.StringBuilder ) v5 ).append ( v6 ); // invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v5 ).append ( v3 ); // invoke-virtual {v5, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v5 ).toString ( ); // invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
android.util.Slog .e ( v4,v5 );
/* :try_end_3 */
/* .catchall {:try_start_3 ..:try_end_3} :catchall_0 */
/* .line 525 */
/* nop */
} // .end local v3 # "e":Ljava/io/IOException;
if ( v1 != null) { // if-eqz v1, :cond_0
/* .line 527 */
try { // :try_start_4
(( java.io.RandomAccessFile ) v1 ).close ( ); // invoke-virtual {v1}, Ljava/io/RandomAccessFile;->close()V
/* :try_end_4 */
/* .catch Ljava/io/IOException; {:try_start_4 ..:try_end_4} :catch_2 */
/* .catchall {:try_start_4 ..:try_end_4} :catchall_1 */
/* .line 528 */
/* :catch_2 */
/* move-exception v3 */
/* .line 529 */
/* .restart local v3 # "e":Ljava/io/IOException; */
try { // :try_start_5
final String v4 = "AccessController"; // const-string v4, "AccessController"
/* new-instance v5, Ljava/lang/StringBuilder; */
/* invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V */
final String v6 = "Error closing file "; // const-string v6, "Error closing file "
(( java.lang.StringBuilder ) v5 ).append ( v6 ); // invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v5 ).append ( v3 ); // invoke-virtual {v5, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v5 ).toString ( ); // invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
/* .line 533 */
} // .end local v3 # "e":Ljava/io/IOException;
} // :cond_0
} // :goto_2
/* monitor-exit v0 */
/* :try_end_5 */
/* .catchall {:try_start_5 ..:try_end_5} :catchall_1 */
/* .line 525 */
} // :goto_3
if ( v1 != null) { // if-eqz v1, :cond_1
/* .line 527 */
try { // :try_start_6
(( java.io.RandomAccessFile ) v1 ).close ( ); // invoke-virtual {v1}, Ljava/io/RandomAccessFile;->close()V
/* :try_end_6 */
/* .catch Ljava/io/IOException; {:try_start_6 ..:try_end_6} :catch_3 */
/* .catchall {:try_start_6 ..:try_end_6} :catchall_1 */
/* .line 530 */
/* .line 528 */
/* :catch_3 */
/* move-exception v4 */
/* .line 529 */
/* .local v4, "e":Ljava/io/IOException; */
try { // :try_start_7
final String v5 = "AccessController"; // const-string v5, "AccessController"
/* new-instance v6, Ljava/lang/StringBuilder; */
/* invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V */
final String v7 = "Error closing file "; // const-string v7, "Error closing file "
(( java.lang.StringBuilder ) v6 ).append ( v7 ); // invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v6 ).append ( v4 ); // invoke-virtual {v6, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v6 ).toString ( ); // invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
android.util.Slog .e ( v5,v6 );
/* .line 532 */
} // .end local v4 # "e":Ljava/io/IOException;
} // :cond_1
} // :goto_4
/* nop */
} // .end local p0 # "this":Lcom/miui/server/AccessController;
} // .end local p1 # "name":Ljava/lang/String;
/* throw v3 */
/* .line 534 */
} // .end local v1 # "raf":Ljava/io/RandomAccessFile;
} // .end local v2 # "stored":Ljava/lang/String;
/* .restart local p0 # "this":Lcom/miui/server/AccessController; */
/* .restart local p1 # "name":Ljava/lang/String; */
/* :catchall_1 */
/* move-exception v1 */
/* monitor-exit v0 */
/* :try_end_7 */
/* .catchall {:try_start_7 ..:try_end_7} :catchall_1 */
/* throw v1 */
} // .end method
private void saveAccessControlPasswordByKeystore ( java.lang.String p0, Integer p1 ) {
/* .locals 3 */
/* .param p1, "password" # Ljava/lang/String; */
/* .param p2, "userId" # I */
/* .line 357 */
v0 = this.mKeystoreHelper;
v1 = this.mContext;
v0 = (( android.security.AccessControlKeystoreHelper ) v0 ).disableEncryptByKeystore ( v1 ); // invoke-virtual {v0, v1}, Landroid/security/AccessControlKeystoreHelper;->disableEncryptByKeystore(Landroid/content/Context;)Z
if ( v0 != null) { // if-eqz v0, :cond_0
/* .line 358 */
return;
/* .line 360 */
} // :cond_0
/* new-instance v0, Ljava/lang/StringBuilder; */
/* invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V */
final String v1 = "saveAccessControlPasswordByKeystore "; // const-string v1, "saveAccessControlPasswordByKeystore "
(( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v0 ).append ( p2 ); // invoke-virtual {v0, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v0 ).toString ( ); // invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
final String v1 = "AccessController"; // const-string v1, "AccessController"
android.util.Slog .i ( v1,v0 );
/* .line 361 */
final String v0 = "access_control_keystore.key"; // const-string v0, "access_control_keystore.key"
/* invoke-direct {p0, p2, v0}, Lcom/miui/server/AccessController;->getFilePathForUser(ILjava/lang/String;)Ljava/lang/String; */
/* .line 363 */
/* .local v0, "filePath":Ljava/lang/String; */
/* new-instance v1, Ljava/lang/StringBuilder; */
/* invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V */
final String v2 = "alias_app_lock_"; // const-string v2, "alias_app_lock_"
(( java.lang.StringBuilder ) v1 ).append ( v2 ); // invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v1 ).append ( p2 ); // invoke-virtual {v1, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v1 ).toString ( ); // invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
/* .line 364 */
/* .local v1, "alias":Ljava/lang/String; */
/* if-nez p1, :cond_1 */
/* .line 365 */
v2 = this.mKeystoreHelper;
(( android.security.AccessControlKeystoreHelper ) v2 ).resetPassword ( v1 ); // invoke-virtual {v2, v1}, Landroid/security/AccessControlKeystoreHelper;->resetPassword(Ljava/lang/String;)[B
/* .local v2, "cipherText":[B */
/* .line 367 */
} // .end local v2 # "cipherText":[B
} // :cond_1
v2 = this.mKeystoreHelper;
(( android.security.AccessControlKeystoreHelper ) v2 ).encrypt ( v1, p1 ); // invoke-virtual {v2, v1, p1}, Landroid/security/AccessControlKeystoreHelper;->encrypt(Ljava/lang/String;Ljava/lang/String;)[B
/* .line 369 */
/* .restart local v2 # "cipherText":[B */
} // :goto_0
/* invoke-direct {p0, v0, v2}, Lcom/miui/server/AccessController;->writeFile(Ljava/lang/String;[B)V */
/* .line 370 */
return;
} // .end method
private void setAccessControlPasswordType ( java.lang.String p0, Integer p1 ) {
/* .locals 1 */
/* .param p1, "passwordType" # Ljava/lang/String; */
/* .param p2, "userId" # I */
/* .line 478 */
final String v0 = "access_control_password_type.key"; // const-string v0, "access_control_password_type.key"
/* invoke-direct {p0, p2, v0}, Lcom/miui/server/AccessController;->getFilePathForUser(ILjava/lang/String;)Ljava/lang/String; */
/* .line 479 */
/* .local v0, "filePath":Ljava/lang/String; */
/* invoke-direct {p0, v0, p1}, Lcom/miui/server/AccessController;->writeTypeFile(Ljava/lang/String;Ljava/lang/String;)V */
/* .line 480 */
return;
} // .end method
private void setAccessControlPattern ( java.lang.String p0, Integer p1 ) {
/* .locals 2 */
/* .param p1, "pattern" # Ljava/lang/String; */
/* .param p2, "userId" # I */
/* .line 389 */
int v0 = 0; // const/4 v0, 0x0
/* .line 390 */
/* .local v0, "hash":[B */
if ( p1 != null) { // if-eqz p1, :cond_0
/* .line 391 */
android.security.MiuiLockPatternUtils .stringToPattern ( p1 );
/* .line 392 */
/* .local v1, "stringToPattern":Ljava/util/List;, "Ljava/util/List<Lcom/android/internal/widget/LockPatternView$Cell;>;" */
android.security.MiuiLockPatternUtils .patternToHash ( v1 );
/* .line 394 */
} // .end local v1 # "stringToPattern":Ljava/util/List;, "Ljava/util/List<Lcom/android/internal/widget/LockPatternView$Cell;>;"
} // :cond_0
final String v1 = "access_control.key"; // const-string v1, "access_control.key"
/* invoke-direct {p0, p2, v1}, Lcom/miui/server/AccessController;->getFilePathForUser(ILjava/lang/String;)Ljava/lang/String; */
/* .line 395 */
/* .local v1, "filePath":Ljava/lang/String; */
/* invoke-direct {p0, v1, v0}, Lcom/miui/server/AccessController;->writeFile(Ljava/lang/String;[B)V */
/* .line 396 */
return;
} // .end method
private void updateWhiteList ( java.util.List p0, android.util.ArrayMap p1 ) {
/* .locals 12 */
/* .annotation system Ldalvik/annotation/Signature; */
/* value = { */
/* "(", */
/* "Ljava/util/List<", */
/* "Landroid/provider/MiuiSettings$SettingsCloudData$CloudData;", */
/* ">;", */
/* "Landroid/util/ArrayMap<", */
/* "Ljava/lang/String;", */
/* "Ljava/util/ArrayList<", */
/* "Landroid/content/Intent;", */
/* ">;>;)V" */
/* } */
} // .end annotation
/* .line 628 */
/* .local p1, "dataList":Ljava/util/List;, "Ljava/util/List<Landroid/provider/MiuiSettings$SettingsCloudData$CloudData;>;" */
/* .local p2, "list":Landroid/util/ArrayMap;, "Landroid/util/ArrayMap<Ljava/lang/String;Ljava/util/ArrayList<Landroid/content/Intent;>;>;" */
if ( p1 != null) { // if-eqz p1, :cond_6
v0 = try { // :try_start_0
/* if-nez v0, :cond_0 */
/* goto/16 :goto_4 */
/* .line 631 */
} // :cond_0
/* new-instance v0, Landroid/util/ArrayMap; */
/* invoke-direct {v0}, Landroid/util/ArrayMap;-><init>()V */
/* .line 632 */
/* .local v0, "cloudList":Landroid/util/ArrayMap;, "Landroid/util/ArrayMap<Ljava/lang/String;Ljava/util/ArrayList<Landroid/content/Intent;>;>;" */
v2 = } // :goto_0
if ( v2 != null) { // if-eqz v2, :cond_4
/* check-cast v2, Landroid/provider/MiuiSettings$SettingsCloudData$CloudData; */
/* .line 633 */
/* .local v2, "data":Landroid/provider/MiuiSettings$SettingsCloudData$CloudData; */
(( android.provider.MiuiSettings$SettingsCloudData$CloudData ) v2 ).toString ( ); // invoke-virtual {v2}, Landroid/provider/MiuiSettings$SettingsCloudData$CloudData;->toString()Ljava/lang/String;
/* .line 634 */
/* .local v3, "json":Ljava/lang/String; */
v4 = android.text.TextUtils .isEmpty ( v3 );
if ( v4 != null) { // if-eqz v4, :cond_1
/* .line 635 */
/* .line 638 */
} // :cond_1
/* new-instance v4, Lorg/json/JSONObject; */
/* invoke-direct {v4, v3}, Lorg/json/JSONObject;-><init>(Ljava/lang/String;)V */
/* .line 639 */
/* .local v4, "jsonObject":Lorg/json/JSONObject; */
final String v5 = "pkg"; // const-string v5, "pkg"
(( org.json.JSONObject ) v4 ).optString ( v5 ); // invoke-virtual {v4, v5}, Lorg/json/JSONObject;->optString(Ljava/lang/String;)Ljava/lang/String;
/* .line 640 */
/* .local v5, "pkg":Ljava/lang/String; */
final String v6 = "cls"; // const-string v6, "cls"
(( org.json.JSONObject ) v4 ).optString ( v6 ); // invoke-virtual {v4, v6}, Lorg/json/JSONObject;->optString(Ljava/lang/String;)Ljava/lang/String;
/* .line 641 */
/* .local v6, "cls":Ljava/lang/String; */
final String v7 = "act"; // const-string v7, "act"
(( org.json.JSONObject ) v4 ).optString ( v7 ); // invoke-virtual {v4, v7}, Lorg/json/JSONObject;->optString(Ljava/lang/String;)Ljava/lang/String;
/* .line 643 */
/* .local v7, "action":Ljava/lang/String; */
/* new-instance v8, Landroid/content/Intent; */
/* invoke-direct {v8}, Landroid/content/Intent;-><init>()V */
/* .line 644 */
/* .local v8, "intent":Landroid/content/Intent; */
v9 = android.text.TextUtils .isEmpty ( v7 );
/* if-nez v9, :cond_2 */
/* .line 645 */
(( android.content.Intent ) v8 ).setAction ( v7 ); // invoke-virtual {v8, v7}, Landroid/content/Intent;->setAction(Ljava/lang/String;)Landroid/content/Intent;
/* .line 647 */
} // :cond_2
/* new-instance v9, Landroid/content/ComponentName; */
/* invoke-direct {v9, v5, v6}, Landroid/content/ComponentName;-><init>(Ljava/lang/String;Ljava/lang/String;)V */
(( android.content.Intent ) v8 ).setComponent ( v9 ); // invoke-virtual {v8, v9}, Landroid/content/Intent;->setComponent(Landroid/content/ComponentName;)Landroid/content/Intent;
/* .line 650 */
} // :goto_1
(( android.util.ArrayMap ) v0 ).get ( v5 ); // invoke-virtual {v0, v5}, Landroid/util/ArrayMap;->get(Ljava/lang/Object;)Ljava/lang/Object;
/* check-cast v9, Ljava/util/ArrayList; */
/* .line 651 */
/* .local v9, "intents":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Landroid/content/Intent;>;" */
/* if-nez v9, :cond_3 */
/* .line 652 */
/* new-instance v10, Ljava/util/ArrayList; */
int v11 = 1; // const/4 v11, 0x1
/* invoke-direct {v10, v11}, Ljava/util/ArrayList;-><init>(I)V */
/* move-object v9, v10 */
/* .line 653 */
(( android.util.ArrayMap ) v0 ).put ( v5, v9 ); // invoke-virtual {v0, v5, v9}, Landroid/util/ArrayMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;
/* .line 655 */
} // :cond_3
(( java.util.ArrayList ) v9 ).add ( v8 ); // invoke-virtual {v9, v8}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z
/* .line 656 */
/* nop */
} // .end local v2 # "data":Landroid/provider/MiuiSettings$SettingsCloudData$CloudData;
} // .end local v3 # "json":Ljava/lang/String;
} // .end local v4 # "jsonObject":Lorg/json/JSONObject;
} // .end local v5 # "pkg":Ljava/lang/String;
} // .end local v6 # "cls":Ljava/lang/String;
} // .end local v7 # "action":Ljava/lang/String;
} // .end local v8 # "intent":Landroid/content/Intent;
} // .end local v9 # "intents":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Landroid/content/Intent;>;"
/* .line 657 */
} // :cond_4
v1 = (( android.util.ArrayMap ) v0 ).size ( ); // invoke-virtual {v0}, Landroid/util/ArrayMap;->size()I
/* if-lez v1, :cond_5 */
/* .line 658 */
/* monitor-enter p0 */
/* :try_end_0 */
/* .catch Ljava/lang/Exception; {:try_start_0 ..:try_end_0} :catch_0 */
/* .line 659 */
try { // :try_start_1
(( android.util.ArrayMap ) p2 ).clear ( ); // invoke-virtual {p2}, Landroid/util/ArrayMap;->clear()V
/* .line 660 */
(( android.util.ArrayMap ) p2 ).putAll ( v0 ); // invoke-virtual {p2, v0}, Landroid/util/ArrayMap;->putAll(Landroid/util/ArrayMap;)V
/* .line 661 */
/* monitor-exit p0 */
/* :catchall_0 */
/* move-exception v1 */
/* monitor-exit p0 */
/* :try_end_1 */
/* .catchall {:try_start_1 ..:try_end_1} :catchall_0 */
} // .end local p0 # "this":Lcom/miui/server/AccessController;
} // .end local p1 # "dataList":Ljava/util/List;, "Ljava/util/List<Landroid/provider/MiuiSettings$SettingsCloudData$CloudData;>;"
} // .end local p2 # "list":Landroid/util/ArrayMap;, "Landroid/util/ArrayMap<Ljava/lang/String;Ljava/util/ArrayList<Landroid/content/Intent;>;>;"
try { // :try_start_2
/* throw v1 */
/* :try_end_2 */
/* .catch Ljava/lang/Exception; {:try_start_2 ..:try_end_2} :catch_0 */
/* .line 665 */
} // .end local v0 # "cloudList":Landroid/util/ArrayMap;, "Landroid/util/ArrayMap<Ljava/lang/String;Ljava/util/ArrayList<Landroid/content/Intent;>;>;"
/* .restart local p0 # "this":Lcom/miui/server/AccessController; */
/* .restart local p1 # "dataList":Ljava/util/List;, "Ljava/util/List<Landroid/provider/MiuiSettings$SettingsCloudData$CloudData;>;" */
/* .restart local p2 # "list":Landroid/util/ArrayMap;, "Landroid/util/ArrayMap<Ljava/lang/String;Ljava/util/ArrayList<Landroid/content/Intent;>;>;" */
} // :cond_5
} // :goto_2
/* .line 663 */
/* :catch_0 */
/* move-exception v0 */
/* .line 664 */
/* .local v0, "e":Ljava/lang/Exception; */
(( java.lang.Exception ) v0 ).printStackTrace ( ); // invoke-virtual {v0}, Ljava/lang/Exception;->printStackTrace()V
/* .line 666 */
} // .end local v0 # "e":Ljava/lang/Exception;
} // :goto_3
return;
/* .line 629 */
} // :cond_6
} // :goto_4
return;
} // .end method
private void writeFile ( java.lang.String p0, Object[] p1 ) {
/* .locals 7 */
/* .param p1, "name" # Ljava/lang/String; */
/* .param p2, "hash" # [B */
/* .line 538 */
v0 = this.mFileWriteLock;
/* monitor-enter v0 */
/* .line 539 */
int v1 = 0; // const/4 v1, 0x0
/* .line 542 */
/* .local v1, "raf":Ljava/io/RandomAccessFile; */
try { // :try_start_0
/* new-instance v2, Ljava/io/RandomAccessFile; */
final String v3 = "rw"; // const-string v3, "rw"
/* invoke-direct {v2, p1, v3}, Ljava/io/RandomAccessFile;-><init>(Ljava/lang/String;Ljava/lang/String;)V */
/* move-object v1, v2 */
/* .line 544 */
/* const-wide/16 v2, 0x0 */
(( java.io.RandomAccessFile ) v1 ).setLength ( v2, v3 ); // invoke-virtual {v1, v2, v3}, Ljava/io/RandomAccessFile;->setLength(J)V
/* .line 545 */
if ( p2 != null) { // if-eqz p2, :cond_0
/* .line 546 */
/* array-length v2, p2 */
int v3 = 0; // const/4 v3, 0x0
(( java.io.RandomAccessFile ) v1 ).write ( p2, v3, v2 ); // invoke-virtual {v1, p2, v3, v2}, Ljava/io/RandomAccessFile;->write([BII)V
/* .line 548 */
} // :cond_0
(( java.io.RandomAccessFile ) v1 ).close ( ); // invoke-virtual {v1}, Ljava/io/RandomAccessFile;->close()V
/* :try_end_0 */
/* .catch Ljava/io/IOException; {:try_start_0 ..:try_end_0} :catch_1 */
/* .catchall {:try_start_0 ..:try_end_0} :catchall_0 */
/* .line 552 */
/* nop */
/* .line 554 */
try { // :try_start_1
(( java.io.RandomAccessFile ) v1 ).close ( ); // invoke-virtual {v1}, Ljava/io/RandomAccessFile;->close()V
/* :try_end_1 */
/* .catch Ljava/io/IOException; {:try_start_1 ..:try_end_1} :catch_0 */
/* .catchall {:try_start_1 ..:try_end_1} :catchall_1 */
/* .line 557 */
} // :goto_0
/* .line 555 */
/* :catch_0 */
/* move-exception v2 */
/* .line 556 */
/* .local v2, "e":Ljava/io/IOException; */
try { // :try_start_2
final String v3 = "AccessController"; // const-string v3, "AccessController"
/* new-instance v4, Ljava/lang/StringBuilder; */
/* invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V */
final String v5 = "Error closing file "; // const-string v5, "Error closing file "
(( java.lang.StringBuilder ) v4 ).append ( v5 ); // invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v4 ).append ( v2 ); // invoke-virtual {v4, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v4 ).toString ( ); // invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
} // :goto_1
android.util.Slog .e ( v3,v4 );
/* :try_end_2 */
/* .catchall {:try_start_2 ..:try_end_2} :catchall_1 */
/* .line 557 */
/* nop */
} // .end local v2 # "e":Ljava/io/IOException;
/* .line 552 */
/* :catchall_0 */
/* move-exception v2 */
/* .line 549 */
/* :catch_1 */
/* move-exception v2 */
/* .line 550 */
/* .restart local v2 # "e":Ljava/io/IOException; */
try { // :try_start_3
final String v3 = "AccessController"; // const-string v3, "AccessController"
/* new-instance v4, Ljava/lang/StringBuilder; */
/* invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V */
final String v5 = "Error writing to file "; // const-string v5, "Error writing to file "
(( java.lang.StringBuilder ) v4 ).append ( v5 ); // invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v4 ).append ( v2 ); // invoke-virtual {v4, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v4 ).toString ( ); // invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
android.util.Slog .e ( v3,v4 );
/* :try_end_3 */
/* .catchall {:try_start_3 ..:try_end_3} :catchall_0 */
/* .line 552 */
/* nop */
} // .end local v2 # "e":Ljava/io/IOException;
if ( v1 != null) { // if-eqz v1, :cond_1
/* .line 554 */
try { // :try_start_4
(( java.io.RandomAccessFile ) v1 ).close ( ); // invoke-virtual {v1}, Ljava/io/RandomAccessFile;->close()V
/* :try_end_4 */
/* .catch Ljava/io/IOException; {:try_start_4 ..:try_end_4} :catch_2 */
/* .catchall {:try_start_4 ..:try_end_4} :catchall_1 */
/* .line 555 */
/* :catch_2 */
/* move-exception v2 */
/* .line 556 */
/* .restart local v2 # "e":Ljava/io/IOException; */
try { // :try_start_5
final String v3 = "AccessController"; // const-string v3, "AccessController"
/* new-instance v4, Ljava/lang/StringBuilder; */
/* invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V */
final String v5 = "Error closing file "; // const-string v5, "Error closing file "
(( java.lang.StringBuilder ) v4 ).append ( v5 ); // invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v4 ).append ( v2 ); // invoke-virtual {v4, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v4 ).toString ( ); // invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
/* .line 560 */
} // .end local v1 # "raf":Ljava/io/RandomAccessFile;
} // .end local v2 # "e":Ljava/io/IOException;
} // :cond_1
} // :goto_2
/* monitor-exit v0 */
/* :try_end_5 */
/* .catchall {:try_start_5 ..:try_end_5} :catchall_1 */
/* .line 561 */
return;
/* .line 552 */
/* .restart local v1 # "raf":Ljava/io/RandomAccessFile; */
} // :goto_3
if ( v1 != null) { // if-eqz v1, :cond_2
/* .line 554 */
try { // :try_start_6
(( java.io.RandomAccessFile ) v1 ).close ( ); // invoke-virtual {v1}, Ljava/io/RandomAccessFile;->close()V
/* :try_end_6 */
/* .catch Ljava/io/IOException; {:try_start_6 ..:try_end_6} :catch_3 */
/* .catchall {:try_start_6 ..:try_end_6} :catchall_1 */
/* .line 557 */
/* .line 555 */
/* :catch_3 */
/* move-exception v3 */
/* .line 556 */
/* .local v3, "e":Ljava/io/IOException; */
try { // :try_start_7
final String v4 = "AccessController"; // const-string v4, "AccessController"
/* new-instance v5, Ljava/lang/StringBuilder; */
/* invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V */
final String v6 = "Error closing file "; // const-string v6, "Error closing file "
(( java.lang.StringBuilder ) v5 ).append ( v6 ); // invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v5 ).append ( v3 ); // invoke-virtual {v5, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v5 ).toString ( ); // invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
android.util.Slog .e ( v4,v5 );
/* .line 559 */
} // .end local v3 # "e":Ljava/io/IOException;
} // :cond_2
} // :goto_4
/* nop */
} // .end local p0 # "this":Lcom/miui/server/AccessController;
} // .end local p1 # "name":Ljava/lang/String;
} // .end local p2 # "hash":[B
/* throw v2 */
/* .line 560 */
} // .end local v1 # "raf":Ljava/io/RandomAccessFile;
/* .restart local p0 # "this":Lcom/miui/server/AccessController; */
/* .restart local p1 # "name":Ljava/lang/String; */
/* .restart local p2 # "hash":[B */
/* :catchall_1 */
/* move-exception v1 */
/* monitor-exit v0 */
/* :try_end_7 */
/* .catchall {:try_start_7 ..:try_end_7} :catchall_1 */
/* throw v1 */
} // .end method
private void writeTypeFile ( java.lang.String p0, java.lang.String p1 ) {
/* .locals 7 */
/* .param p1, "name" # Ljava/lang/String; */
/* .param p2, "passwordType" # Ljava/lang/String; */
/* .line 564 */
v0 = this.mFileWriteLock;
/* monitor-enter v0 */
/* .line 565 */
int v1 = 0; // const/4 v1, 0x0
/* .line 568 */
/* .local v1, "raf":Ljava/io/RandomAccessFile; */
try { // :try_start_0
/* new-instance v2, Ljava/io/RandomAccessFile; */
final String v3 = "rw"; // const-string v3, "rw"
/* invoke-direct {v2, p1, v3}, Ljava/io/RandomAccessFile;-><init>(Ljava/lang/String;Ljava/lang/String;)V */
/* move-object v1, v2 */
/* .line 570 */
/* const-wide/16 v2, 0x0 */
(( java.io.RandomAccessFile ) v1 ).setLength ( v2, v3 ); // invoke-virtual {v1, v2, v3}, Ljava/io/RandomAccessFile;->setLength(J)V
/* .line 571 */
if ( p2 != null) { // if-eqz p2, :cond_0
/* .line 572 */
(( java.io.RandomAccessFile ) v1 ).writeBytes ( p2 ); // invoke-virtual {v1, p2}, Ljava/io/RandomAccessFile;->writeBytes(Ljava/lang/String;)V
/* .line 574 */
} // :cond_0
(( java.io.RandomAccessFile ) v1 ).close ( ); // invoke-virtual {v1}, Ljava/io/RandomAccessFile;->close()V
/* :try_end_0 */
/* .catch Ljava/io/IOException; {:try_start_0 ..:try_end_0} :catch_1 */
/* .catchall {:try_start_0 ..:try_end_0} :catchall_0 */
/* .line 578 */
/* nop */
/* .line 580 */
try { // :try_start_1
(( java.io.RandomAccessFile ) v1 ).close ( ); // invoke-virtual {v1}, Ljava/io/RandomAccessFile;->close()V
/* :try_end_1 */
/* .catch Ljava/io/IOException; {:try_start_1 ..:try_end_1} :catch_0 */
/* .catchall {:try_start_1 ..:try_end_1} :catchall_1 */
/* .line 583 */
} // :goto_0
/* .line 581 */
/* :catch_0 */
/* move-exception v2 */
/* .line 582 */
/* .local v2, "e":Ljava/io/IOException; */
try { // :try_start_2
final String v3 = "AccessController"; // const-string v3, "AccessController"
/* new-instance v4, Ljava/lang/StringBuilder; */
/* invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V */
final String v5 = "Error closing type file "; // const-string v5, "Error closing type file "
(( java.lang.StringBuilder ) v4 ).append ( v5 ); // invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v4 ).append ( v2 ); // invoke-virtual {v4, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v4 ).toString ( ); // invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
} // :goto_1
android.util.Slog .e ( v3,v4 );
/* :try_end_2 */
/* .catchall {:try_start_2 ..:try_end_2} :catchall_1 */
/* .line 583 */
/* nop */
} // .end local v2 # "e":Ljava/io/IOException;
/* .line 578 */
/* :catchall_0 */
/* move-exception v2 */
/* .line 575 */
/* :catch_1 */
/* move-exception v2 */
/* .line 576 */
/* .restart local v2 # "e":Ljava/io/IOException; */
try { // :try_start_3
final String v3 = "AccessController"; // const-string v3, "AccessController"
/* new-instance v4, Ljava/lang/StringBuilder; */
/* invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V */
final String v5 = "Error writing type to file "; // const-string v5, "Error writing type to file "
(( java.lang.StringBuilder ) v4 ).append ( v5 ); // invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v4 ).append ( v2 ); // invoke-virtual {v4, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v4 ).toString ( ); // invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
android.util.Slog .e ( v3,v4 );
/* :try_end_3 */
/* .catchall {:try_start_3 ..:try_end_3} :catchall_0 */
/* .line 578 */
/* nop */
} // .end local v2 # "e":Ljava/io/IOException;
if ( v1 != null) { // if-eqz v1, :cond_1
/* .line 580 */
try { // :try_start_4
(( java.io.RandomAccessFile ) v1 ).close ( ); // invoke-virtual {v1}, Ljava/io/RandomAccessFile;->close()V
/* :try_end_4 */
/* .catch Ljava/io/IOException; {:try_start_4 ..:try_end_4} :catch_2 */
/* .catchall {:try_start_4 ..:try_end_4} :catchall_1 */
/* .line 581 */
/* :catch_2 */
/* move-exception v2 */
/* .line 582 */
/* .restart local v2 # "e":Ljava/io/IOException; */
try { // :try_start_5
final String v3 = "AccessController"; // const-string v3, "AccessController"
/* new-instance v4, Ljava/lang/StringBuilder; */
/* invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V */
final String v5 = "Error closing type file "; // const-string v5, "Error closing type file "
(( java.lang.StringBuilder ) v4 ).append ( v5 ); // invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v4 ).append ( v2 ); // invoke-virtual {v4, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v4 ).toString ( ); // invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
/* .line 586 */
} // .end local v1 # "raf":Ljava/io/RandomAccessFile;
} // .end local v2 # "e":Ljava/io/IOException;
} // :cond_1
} // :goto_2
/* monitor-exit v0 */
/* :try_end_5 */
/* .catchall {:try_start_5 ..:try_end_5} :catchall_1 */
/* .line 587 */
return;
/* .line 578 */
/* .restart local v1 # "raf":Ljava/io/RandomAccessFile; */
} // :goto_3
if ( v1 != null) { // if-eqz v1, :cond_2
/* .line 580 */
try { // :try_start_6
(( java.io.RandomAccessFile ) v1 ).close ( ); // invoke-virtual {v1}, Ljava/io/RandomAccessFile;->close()V
/* :try_end_6 */
/* .catch Ljava/io/IOException; {:try_start_6 ..:try_end_6} :catch_3 */
/* .catchall {:try_start_6 ..:try_end_6} :catchall_1 */
/* .line 583 */
/* .line 581 */
/* :catch_3 */
/* move-exception v3 */
/* .line 582 */
/* .local v3, "e":Ljava/io/IOException; */
try { // :try_start_7
final String v4 = "AccessController"; // const-string v4, "AccessController"
/* new-instance v5, Ljava/lang/StringBuilder; */
/* invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V */
final String v6 = "Error closing type file "; // const-string v6, "Error closing type file "
(( java.lang.StringBuilder ) v5 ).append ( v6 ); // invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v5 ).append ( v3 ); // invoke-virtual {v5, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v5 ).toString ( ); // invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
android.util.Slog .e ( v4,v5 );
/* .line 585 */
} // .end local v3 # "e":Ljava/io/IOException;
} // :cond_2
} // :goto_4
/* nop */
} // .end local p0 # "this":Lcom/miui/server/AccessController;
} // .end local p1 # "name":Ljava/lang/String;
} // .end local p2 # "passwordType":Ljava/lang/String;
/* throw v2 */
/* .line 586 */
} // .end local v1 # "raf":Ljava/io/RandomAccessFile;
/* .restart local p0 # "this":Lcom/miui/server/AccessController; */
/* .restart local p1 # "name":Ljava/lang/String; */
/* .restart local p2 # "passwordType":Ljava/lang/String; */
/* :catchall_1 */
/* move-exception v1 */
/* monitor-exit v0 */
/* :try_end_7 */
/* .catchall {:try_start_7 ..:try_end_7} :catchall_1 */
/* throw v1 */
} // .end method
/* # virtual methods */
Boolean checkAccessControlPassword ( java.lang.String p0, java.lang.String p1, Integer p2 ) {
/* .locals 4 */
/* .param p1, "passwordType" # Ljava/lang/String; */
/* .param p2, "password" # Ljava/lang/String; */
/* .param p3, "userId" # I */
/* .line 426 */
if ( p2 != null) { // if-eqz p2, :cond_3
/* if-nez p1, :cond_0 */
/* .line 429 */
} // :cond_0
v0 = /* invoke-direct {p0, p2, p3}, Lcom/miui/server/AccessController;->checkAccessControlPasswordByKeystore(Ljava/lang/String;I)Z */
if ( v0 != null) { // if-eqz v0, :cond_1
/* .line 430 */
int v0 = 1; // const/4 v0, 0x1
/* .line 432 */
} // :cond_1
final String v0 = "AccessController"; // const-string v0, "AccessController"
final String v1 = "checkAccessControlPassword!"; // const-string v1, "checkAccessControlPassword!"
android.util.Slog .i ( v0,v1 );
/* .line 434 */
final String v0 = "pattern"; // const-string v0, "pattern"
v0 = (( java.lang.String ) v0 ).equals ( p1 ); // invoke-virtual {v0, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z
if ( v0 != null) { // if-eqz v0, :cond_2
/* .line 435 */
v0 = /* invoke-direct {p0, p2, p3}, Lcom/miui/server/AccessController;->checkAccessControlPattern(Ljava/lang/String;I)Z */
/* .line 437 */
} // :cond_2
/* invoke-direct {p0, p2, p3}, Lcom/miui/server/AccessController;->passwordToHash(Ljava/lang/String;I)[B */
/* .line 438 */
/* .local v0, "hash":[B */
final String v1 = "access_control.key"; // const-string v1, "access_control.key"
/* invoke-direct {p0, p3, v1}, Lcom/miui/server/AccessController;->getFilePathForUser(ILjava/lang/String;)Ljava/lang/String; */
/* .line 439 */
/* .local v1, "filePath":Ljava/lang/String; */
/* invoke-direct {p0, v1}, Lcom/miui/server/AccessController;->readFile(Ljava/lang/String;)[B */
/* .line 440 */
/* .local v2, "readFile":[B */
v3 = java.util.Arrays .equals ( v2,v0 );
/* .line 427 */
} // .end local v0 # "hash":[B
} // .end local v1 # "filePath":Ljava/lang/String;
} // .end local v2 # "readFile":[B
} // :cond_3
} // :goto_0
int v0 = 0; // const/4 v0, 0x0
} // .end method
public Boolean filterIntentLocked ( Boolean p0, java.lang.String p1, android.content.Intent p2 ) {
/* .locals 1 */
/* .param p1, "isSkipIntent" # Z */
/* .param p2, "packageName" # Ljava/lang/String; */
/* .param p3, "intent" # Landroid/content/Intent; */
/* .line 229 */
int v0 = 0; // const/4 v0, 0x0
v0 = (( com.miui.server.AccessController ) p0 ).filterIntentLocked ( p1, v0, p2, p3 ); // invoke-virtual {p0, p1, v0, p2, p3}, Lcom/miui/server/AccessController;->filterIntentLocked(ZZLjava/lang/String;Landroid/content/Intent;)Z
} // .end method
public Boolean filterIntentLocked ( Boolean p0, Boolean p1, java.lang.String p2, android.content.Intent p3 ) {
/* .locals 15 */
/* .param p1, "isSkipIntent" # Z */
/* .param p2, "pay" # Z */
/* .param p3, "packageName" # Ljava/lang/String; */
/* .param p4, "intent" # Landroid/content/Intent; */
/* .line 237 */
/* move-object v1, p0 */
/* move-object/from16 v2, p3 */
/* move-object/from16 v3, p4 */
int v0 = 0; // const/4 v0, 0x0
/* if-nez v3, :cond_0 */
/* .line 238 */
/* .line 240 */
} // :cond_0
/* monitor-enter p0 */
/* .line 241 */
int v4 = 1; // const/4 v4, 0x1
if ( p2 != null) { // if-eqz p2, :cond_4
/* .line 242 */
try { // :try_start_0
final String v5 = "android.intent.extra.INTENT"; // const-string v5, "android.intent.extra.INTENT"
(( android.content.Intent ) v3 ).getParcelableExtra ( v5 ); // invoke-virtual {v3, v5}, Landroid/content/Intent;->getParcelableExtra(Ljava/lang/String;)Landroid/os/Parcelable;
/* check-cast v5, Landroid/content/Intent; */
/* .line 243 */
/* .local v5, "rawIntent":Landroid/content/Intent; */
int v6 = 0; // const/4 v6, 0x0
/* .line 244 */
/* .local v6, "rawPackageName":Ljava/lang/String; */
if ( v5 != null) { // if-eqz v5, :cond_1
(( android.content.Intent ) v5 ).getComponent ( ); // invoke-virtual {v5}, Landroid/content/Intent;->getComponent()Landroid/content/ComponentName;
if ( v7 != null) { // if-eqz v7, :cond_1
final String v7 = "com.miui.securitycenter/com.miui.applicationlock.ConfirmAccessControl"; // const-string v7, "com.miui.securitycenter/com.miui.applicationlock.ConfirmAccessControl"
/* .line 245 */
/* invoke-virtual/range {p4 ..p4}, Landroid/content/Intent;->getComponent()Landroid/content/ComponentName; */
(( android.content.ComponentName ) v8 ).flattenToShortString ( ); // invoke-virtual {v8}, Landroid/content/ComponentName;->flattenToShortString()Ljava/lang/String;
v7 = android.text.TextUtils .equals ( v7,v8 );
if ( v7 != null) { // if-eqz v7, :cond_1
/* .line 246 */
(( android.content.Intent ) v5 ).getComponent ( ); // invoke-virtual {v5}, Landroid/content/Intent;->getComponent()Landroid/content/ComponentName;
(( android.content.ComponentName ) v7 ).getPackageName ( ); // invoke-virtual {v7}, Landroid/content/ComponentName;->getPackageName()Ljava/lang/String;
/* move-object v6, v7 */
/* .line 248 */
} // :cond_1
/* move-object/from16 v6, p3 */
/* .line 249 */
/* move-object/from16 v5, p4 */
/* .line 251 */
} // :goto_0
v7 = this.mContext;
v7 = com.miui.server.security.DefaultBrowserImpl .isDefaultBrowser ( v7,v6 );
if ( v7 != null) { // if-eqz v7, :cond_4
/* .line 252 */
(( android.content.Intent ) v5 ).getData ( ); // invoke-virtual {v5}, Landroid/content/Intent;->getData()Landroid/net/Uri;
/* .line 253 */
/* .local v7, "uri":Landroid/net/Uri; */
if ( v7 != null) { // if-eqz v7, :cond_3
(( android.net.Uri ) v7 ).getHost ( ); // invoke-virtual {v7}, Landroid/net/Uri;->getHost()Ljava/lang/String;
if ( v8 != null) { // if-eqz v8, :cond_3
(( android.net.Uri ) v7 ).getScheme ( ); // invoke-virtual {v7}, Landroid/net/Uri;->getScheme()Ljava/lang/String;
if ( v8 != null) { // if-eqz v8, :cond_3
/* .line 254 */
(( android.content.Intent ) v5 ).getAction ( ); // invoke-virtual {v5}, Landroid/content/Intent;->getAction()Ljava/lang/String;
if ( v8 != null) { // if-eqz v8, :cond_3
/* .line 255 */
(( android.net.Uri ) v7 ).getScheme ( ); // invoke-virtual {v7}, Landroid/net/Uri;->getScheme()Ljava/lang/String;
final String v9 = "http"; // const-string v9, "http"
v8 = (( java.lang.String ) v8 ).equalsIgnoreCase ( v9 ); // invoke-virtual {v8, v9}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z
/* if-nez v8, :cond_2 */
/* .line 256 */
(( android.net.Uri ) v7 ).getScheme ( ); // invoke-virtual {v7}, Landroid/net/Uri;->getScheme()Ljava/lang/String;
final String v9 = "https"; // const-string v9, "https"
v8 = (( java.lang.String ) v8 ).equalsIgnoreCase ( v9 ); // invoke-virtual {v8, v9}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z
if ( v8 != null) { // if-eqz v8, :cond_3
/* .line 257 */
} // :cond_2
/* monitor-exit p0 */
/* .line 259 */
} // :cond_3
/* monitor-exit p0 */
/* .line 320 */
} // .end local v5 # "rawIntent":Landroid/content/Intent;
} // .end local v6 # "rawPackageName":Ljava/lang/String;
} // .end local v7 # "uri":Landroid/net/Uri;
/* :catchall_0 */
/* move-exception v0 */
/* goto/16 :goto_6 */
/* .line 263 */
} // :cond_4
/* move-object/from16 v5, p4 */
/* .line 265 */
/* .local v5, "oriIntent":Landroid/content/Intent; */
if ( p1 != null) { // if-eqz p1, :cond_5
/* .line 266 */
v6 = com.miui.server.AccessController.mSkipList;
(( android.util.ArrayMap ) v6 ).get ( v2 ); // invoke-virtual {v6, v2}, Landroid/util/ArrayMap;->get(Ljava/lang/Object;)Ljava/lang/Object;
/* check-cast v6, Ljava/util/ArrayList; */
/* .local v6, "intents":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Landroid/content/Intent;>;" */
/* .line 267 */
} // .end local v6 # "intents":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Landroid/content/Intent;>;"
} // :cond_5
if ( p2 != null) { // if-eqz p2, :cond_8
/* .line 268 */
final String v6 = "android.intent.extra.INTENT"; // const-string v6, "android.intent.extra.INTENT"
(( android.content.Intent ) v3 ).getParcelableExtra ( v6 ); // invoke-virtual {v3, v6}, Landroid/content/Intent;->getParcelableExtra(Ljava/lang/String;)Landroid/os/Parcelable;
/* check-cast v6, Landroid/content/Intent; */
/* .line 269 */
/* .local v6, "rawIntent":Landroid/content/Intent; */
int v7 = 0; // const/4 v7, 0x0
/* .line 270 */
/* .local v7, "rawPackageName":Ljava/lang/String; */
if ( v6 != null) { // if-eqz v6, :cond_6
(( android.content.Intent ) v6 ).getComponent ( ); // invoke-virtual {v6}, Landroid/content/Intent;->getComponent()Landroid/content/ComponentName;
if ( v8 != null) { // if-eqz v8, :cond_6
/* .line 271 */
(( android.content.Intent ) v6 ).getComponent ( ); // invoke-virtual {v6}, Landroid/content/Intent;->getComponent()Landroid/content/ComponentName;
(( android.content.ComponentName ) v8 ).getPackageName ( ); // invoke-virtual {v8}, Landroid/content/ComponentName;->getPackageName()Ljava/lang/String;
/* move-object v7, v8 */
/* .line 273 */
} // :cond_6
/* invoke-virtual/range {p4 ..p4}, Landroid/content/Intent;->getComponent()Landroid/content/ComponentName; */
if ( v8 != null) { // if-eqz v8, :cond_7
final String v8 = "com.miui.securitycenter/com.miui.applicationlock.ConfirmAccessControl"; // const-string v8, "com.miui.securitycenter/com.miui.applicationlock.ConfirmAccessControl"
/* invoke-virtual/range {p4 ..p4}, Landroid/content/Intent;->getComponent()Landroid/content/ComponentName; */
(( android.content.ComponentName ) v9 ).flattenToShortString ( ); // invoke-virtual {v9}, Landroid/content/ComponentName;->flattenToShortString()Ljava/lang/String;
v8 = android.text.TextUtils .equals ( v8,v9 );
if ( v8 != null) { // if-eqz v8, :cond_7
v8 = com.miui.server.AccessController.mPayInterceptList;
v8 = (( android.util.ArrayMap ) v8 ).containsKey ( v7 ); // invoke-virtual {v8, v7}, Landroid/util/ArrayMap;->containsKey(Ljava/lang/Object;)Z
if ( v8 != null) { // if-eqz v8, :cond_7
/* .line 274 */
/* move-object v5, v6 */
/* .line 275 */
v8 = com.miui.server.AccessController.mPayInterceptList;
(( android.util.ArrayMap ) v8 ).get ( v7 ); // invoke-virtual {v8, v7}, Landroid/util/ArrayMap;->get(Ljava/lang/Object;)Ljava/lang/Object;
/* check-cast v8, Ljava/util/ArrayList; */
/* move-object v6, v8 */
/* .local v8, "intents":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Landroid/content/Intent;>;" */
/* .line 277 */
} // .end local v8 # "intents":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Landroid/content/Intent;>;"
} // :cond_7
v8 = com.miui.server.AccessController.mPayInterceptList;
(( android.util.ArrayMap ) v8 ).get ( v2 ); // invoke-virtual {v8, v2}, Landroid/util/ArrayMap;->get(Ljava/lang/Object;)Ljava/lang/Object;
/* check-cast v8, Ljava/util/ArrayList; */
/* move-object v6, v8 */
/* .line 279 */
} // .end local v7 # "rawPackageName":Ljava/lang/String;
/* .local v6, "intents":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Landroid/content/Intent;>;" */
} // :goto_1
/* .line 280 */
} // .end local v6 # "intents":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Landroid/content/Intent;>;"
} // :cond_8
v6 = com.miui.server.AccessController.mAntimsgInterceptList;
(( android.util.ArrayMap ) v6 ).get ( v2 ); // invoke-virtual {v6, v2}, Landroid/util/ArrayMap;->get(Ljava/lang/Object;)Ljava/lang/Object;
/* check-cast v6, Ljava/util/ArrayList; */
/* .line 282 */
/* .restart local v6 # "intents":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Landroid/content/Intent;>;" */
} // :goto_2
/* if-nez v6, :cond_9 */
/* .line 283 */
/* monitor-exit p0 */
/* .line 286 */
} // :cond_9
(( android.content.Intent ) v5 ).getAction ( ); // invoke-virtual {v5}, Landroid/content/Intent;->getAction()Ljava/lang/String;
/* .line 287 */
/* .local v7, "action":Ljava/lang/String; */
(( android.content.Intent ) v5 ).getComponent ( ); // invoke-virtual {v5}, Landroid/content/Intent;->getComponent()Landroid/content/ComponentName;
/* .line 288 */
/* .local v8, "component":Landroid/content/ComponentName; */
if ( v7 != null) { // if-eqz v7, :cond_b
/* .line 289 */
(( java.util.ArrayList ) v6 ).iterator ( ); // invoke-virtual {v6}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;
v10 = } // :goto_3
if ( v10 != null) { // if-eqz v10, :cond_b
/* check-cast v10, Landroid/content/Intent; */
/* .line 290 */
/* .local v10, "i":Landroid/content/Intent; */
(( android.content.Intent ) v10 ).getAction ( ); // invoke-virtual {v10}, Landroid/content/Intent;->getAction()Ljava/lang/String;
v11 = (( java.lang.String ) v7 ).equals ( v11 ); // invoke-virtual {v7, v11}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z
if ( v11 != null) { // if-eqz v11, :cond_a
/* .line 291 */
/* monitor-exit p0 */
/* .line 293 */
} // .end local v10 # "i":Landroid/content/Intent;
} // :cond_a
/* .line 296 */
} // :cond_b
if ( v8 != null) { // if-eqz v8, :cond_10
/* .line 298 */
(( android.content.ComponentName ) v8 ).getClassName ( ); // invoke-virtual {v8}, Landroid/content/ComponentName;->getClassName()Ljava/lang/String;
/* .line 299 */
/* .local v9, "cls":Ljava/lang/String; */
/* if-nez v9, :cond_c */
/* .line 300 */
/* monitor-exit p0 */
/* .line 302 */
} // :cond_c
v10 = (( java.lang.String ) v9 ).charAt ( v0 ); // invoke-virtual {v9, v0}, Ljava/lang/String;->charAt(I)C
/* const/16 v11, 0x2e */
/* if-ne v10, v11, :cond_d */
/* .line 304 */
/* new-instance v10, Ljava/lang/StringBuilder; */
/* invoke-direct {v10}, Ljava/lang/StringBuilder;-><init>()V */
(( android.content.ComponentName ) v8 ).getPackageName ( ); // invoke-virtual {v8}, Landroid/content/ComponentName;->getPackageName()Ljava/lang/String;
(( java.lang.StringBuilder ) v10 ).append ( v11 ); // invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v10 ).append ( v9 ); // invoke-virtual {v10, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v10 ).toString ( ); // invoke-virtual {v10}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
/* .local v10, "fullName":Ljava/lang/String; */
/* .line 307 */
} // .end local v10 # "fullName":Ljava/lang/String;
} // :cond_d
/* move-object v10, v9 */
/* .line 310 */
/* .restart local v10 # "fullName":Ljava/lang/String; */
} // :goto_4
/* if-nez p1, :cond_e */
final String v11 = "com.tencent.mm.plugin.voip.ui.VideoActivity"; // const-string v11, "com.tencent.mm.plugin.voip.ui.VideoActivity"
v11 = (( java.lang.String ) v11 ).equals ( v9 ); // invoke-virtual {v11, v9}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z
if ( v11 != null) { // if-eqz v11, :cond_e
v11 = /* invoke-virtual/range {p4 ..p4}, Landroid/content/Intent;->getFlags()I */
/* const v12, -0x10000001 */
/* and-int/2addr v11, v12 */
/* if-nez v11, :cond_e */
/* .line 311 */
/* monitor-exit p0 */
/* .line 313 */
} // :cond_e
(( java.util.ArrayList ) v6 ).iterator ( ); // invoke-virtual {v6}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;
v12 = } // :goto_5
if ( v12 != null) { // if-eqz v12, :cond_10
/* check-cast v12, Landroid/content/Intent; */
/* .line 314 */
/* .local v12, "i":Landroid/content/Intent; */
(( android.content.Intent ) v12 ).getComponent ( ); // invoke-virtual {v12}, Landroid/content/Intent;->getComponent()Landroid/content/ComponentName;
/* .line 315 */
/* .local v13, "c":Landroid/content/ComponentName; */
if ( v13 != null) { // if-eqz v13, :cond_f
(( android.content.ComponentName ) v13 ).getClassName ( ); // invoke-virtual {v13}, Landroid/content/ComponentName;->getClassName()Ljava/lang/String;
v14 = (( java.lang.String ) v10 ).equals ( v14 ); // invoke-virtual {v10, v14}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z
if ( v14 != null) { // if-eqz v14, :cond_f
/* .line 316 */
/* monitor-exit p0 */
/* .line 318 */
} // .end local v12 # "i":Landroid/content/Intent;
} // .end local v13 # "c":Landroid/content/ComponentName;
} // :cond_f
/* .line 320 */
} // .end local v5 # "oriIntent":Landroid/content/Intent;
} // .end local v6 # "intents":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Landroid/content/Intent;>;"
} // .end local v7 # "action":Ljava/lang/String;
} // .end local v8 # "component":Landroid/content/ComponentName;
} // .end local v9 # "cls":Ljava/lang/String;
} // .end local v10 # "fullName":Ljava/lang/String;
} // :cond_10
/* monitor-exit p0 */
/* .line 322 */
/* .line 320 */
} // :goto_6
/* monitor-exit p0 */
/* :try_end_0 */
/* .catchall {:try_start_0 ..:try_end_0} :catchall_0 */
/* throw v0 */
} // .end method
java.lang.String getAccessControlPasswordType ( Integer p0 ) {
/* .locals 2 */
/* .param p1, "userId" # I */
/* .line 463 */
final String v0 = "access_control_password_type.key"; // const-string v0, "access_control_password_type.key"
/* invoke-direct {p0, p1, v0}, Lcom/miui/server/AccessController;->getFilePathForUser(ILjava/lang/String;)Ljava/lang/String; */
/* .line 464 */
/* .local v0, "filePath":Ljava/lang/String; */
/* if-nez v0, :cond_0 */
/* .line 465 */
final String v1 = "pattern"; // const-string v1, "pattern"
/* .line 467 */
} // :cond_0
/* invoke-direct {p0, v0}, Lcom/miui/server/AccessController;->readTypeFile(Ljava/lang/String;)Ljava/lang/String; */
} // .end method
public java.util.List getForceLaunchList ( ) {
/* .locals 1 */
/* .annotation system Ldalvik/annotation/Signature; */
/* value = { */
/* "()", */
/* "Ljava/util/List<", */
/* "Ljava/lang/String;", */
/* ">;" */
/* } */
} // .end annotation
/* .line 705 */
v0 = com.miui.server.AccessController.mForceLaunchList;
} // .end method
Boolean haveAccessControlPassword ( Integer p0 ) {
/* .locals 9 */
/* .param p1, "userId" # I */
/* .line 452 */
final String v0 = "access_control_password_type.key"; // const-string v0, "access_control_password_type.key"
/* invoke-direct {p0, p1, v0}, Lcom/miui/server/AccessController;->getFilePathForUser(ILjava/lang/String;)Ljava/lang/String; */
/* .line 453 */
/* .local v0, "filePathType":Ljava/lang/String; */
final String v1 = "access_control.key"; // const-string v1, "access_control.key"
/* invoke-direct {p0, p1, v1}, Lcom/miui/server/AccessController;->getFilePathForUser(ILjava/lang/String;)Ljava/lang/String; */
/* .line 454 */
/* .local v1, "filePathPassword":Ljava/lang/String; */
v2 = this.mFileWriteLock;
/* monitor-enter v2 */
/* .line 455 */
try { // :try_start_0
/* new-instance v3, Ljava/io/File; */
/* invoke-direct {v3, v0}, Ljava/io/File;-><init>(Ljava/lang/String;)V */
/* .line 456 */
/* .local v3, "fileType":Ljava/io/File; */
/* new-instance v4, Ljava/io/File; */
/* invoke-direct {v4, v1}, Ljava/io/File;-><init>(Ljava/lang/String;)V */
/* .line 457 */
/* .local v4, "filePassword":Ljava/io/File; */
v5 = (( java.io.File ) v3 ).exists ( ); // invoke-virtual {v3}, Ljava/io/File;->exists()Z
if ( v5 != null) { // if-eqz v5, :cond_0
v5 = (( java.io.File ) v4 ).exists ( ); // invoke-virtual {v4}, Ljava/io/File;->exists()Z
if ( v5 != null) { // if-eqz v5, :cond_0
/* .line 458 */
(( java.io.File ) v3 ).length ( ); // invoke-virtual {v3}, Ljava/io/File;->length()J
/* move-result-wide v5 */
/* const-wide/16 v7, 0x0 */
/* cmp-long v5, v5, v7 */
/* if-lez v5, :cond_0 */
(( java.io.File ) v4 ).length ( ); // invoke-virtual {v4}, Ljava/io/File;->length()J
/* move-result-wide v5 */
/* cmp-long v5, v5, v7 */
/* if-lez v5, :cond_0 */
int v5 = 1; // const/4 v5, 0x1
} // :cond_0
int v5 = 0; // const/4 v5, 0x0
} // :goto_0
/* monitor-exit v2 */
/* .line 457 */
/* .line 459 */
} // .end local v3 # "fileType":Ljava/io/File;
} // .end local v4 # "filePassword":Ljava/io/File;
/* :catchall_0 */
/* move-exception v3 */
/* monitor-exit v2 */
/* :try_end_0 */
/* .catchall {:try_start_0 ..:try_end_0} :catchall_0 */
/* throw v3 */
} // .end method
void setAccessControlPassword ( java.lang.String p0, java.lang.String p1, Integer p2 ) {
/* .locals 2 */
/* .param p1, "passwordType" # Ljava/lang/String; */
/* .param p2, "password" # Ljava/lang/String; */
/* .param p3, "userId" # I */
/* .line 399 */
/* invoke-direct {p0, p2, p3}, Lcom/miui/server/AccessController;->saveAccessControlPasswordByKeystore(Ljava/lang/String;I)V */
/* .line 401 */
final String v0 = "pattern"; // const-string v0, "pattern"
v0 = (( java.lang.String ) v0 ).equals ( p1 ); // invoke-virtual {v0, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z
if ( v0 != null) { // if-eqz v0, :cond_0
/* .line 402 */
/* invoke-direct {p0, p2, p3}, Lcom/miui/server/AccessController;->setAccessControlPattern(Ljava/lang/String;I)V */
/* .line 404 */
} // :cond_0
int v0 = 0; // const/4 v0, 0x0
/* .line 405 */
/* .local v0, "hash":[B */
if ( p2 != null) { // if-eqz p2, :cond_1
/* .line 406 */
/* invoke-direct {p0, p2, p3}, Lcom/miui/server/AccessController;->passwordToHash(Ljava/lang/String;I)[B */
/* .line 408 */
} // :cond_1
final String v1 = "access_control.key"; // const-string v1, "access_control.key"
/* invoke-direct {p0, p3, v1}, Lcom/miui/server/AccessController;->getFilePathForUser(ILjava/lang/String;)Ljava/lang/String; */
/* .line 409 */
/* .local v1, "filePath":Ljava/lang/String; */
/* invoke-direct {p0, v1, v0}, Lcom/miui/server/AccessController;->writeFile(Ljava/lang/String;[B)V */
/* .line 411 */
} // .end local v0 # "hash":[B
} // .end local v1 # "filePath":Ljava/lang/String;
} // :goto_0
/* invoke-direct {p0, p1, p3}, Lcom/miui/server/AccessController;->setAccessControlPasswordType(Ljava/lang/String;I)V */
/* .line 412 */
return;
} // .end method
public Boolean skipActivity ( android.content.Intent p0, java.lang.String p1 ) {
/* .locals 5 */
/* .param p1, "intent" # Landroid/content/Intent; */
/* .param p2, "callingPkg" # Ljava/lang/String; */
/* .line 326 */
int v0 = 0; // const/4 v0, 0x0
if ( p1 != null) { // if-eqz p1, :cond_2
/* .line 328 */
try { // :try_start_0
(( android.content.Intent ) p1 ).getComponent ( ); // invoke-virtual {p1}, Landroid/content/Intent;->getComponent()Landroid/content/ComponentName;
/* .line 329 */
/* .local v1, "componentName":Landroid/content/ComponentName; */
if ( v1 != null) { // if-eqz v1, :cond_1
/* .line 330 */
(( android.content.ComponentName ) v1 ).getPackageName ( ); // invoke-virtual {v1}, Landroid/content/ComponentName;->getPackageName()Ljava/lang/String;
/* .line 331 */
/* .local v2, "packageName":Ljava/lang/String; */
(( android.content.ComponentName ) v1 ).getClassName ( ); // invoke-virtual {v1}, Landroid/content/ComponentName;->getClassName()Ljava/lang/String;
/* .line 332 */
/* .local v3, "activity":Ljava/lang/String; */
v4 = com.miui.server.AccessController .isOpenedPkg ( p2 );
if ( v4 != null) { // if-eqz v4, :cond_1
v4 = android.text.TextUtils .isEmpty ( v2 );
/* if-nez v4, :cond_1 */
v4 = android.text.TextUtils .isEmpty ( v3 );
/* if-nez v4, :cond_1 */
/* .line 333 */
final String v4 = "com.miui.gallery"; // const-string v4, "com.miui.gallery"
v4 = (( java.lang.String ) v4 ).equals ( v2 ); // invoke-virtual {v4, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z
if ( v4 != null) { // if-eqz v4, :cond_0
/* .line 334 */
v4 = com.miui.server.AccessController .isOpenedActivity ( v3 );
if ( v4 != null) { // if-eqz v4, :cond_0
/* const-string/jumbo v4, "skip_interception" */
v4 = (( android.content.Intent ) p1 ).getBooleanExtra ( v4, v0 ); // invoke-virtual {p1, v4, v0}, Landroid/content/Intent;->getBooleanExtra(Ljava/lang/String;Z)Z
/* :try_end_0 */
/* .catchall {:try_start_0 ..:try_end_0} :catchall_0 */
if ( v4 != null) { // if-eqz v4, :cond_0
int v0 = 1; // const/4 v0, 0x1
} // :cond_0
/* nop */
/* .line 333 */
} // :goto_0
/* .line 340 */
} // .end local v1 # "componentName":Landroid/content/ComponentName;
} // .end local v2 # "packageName":Ljava/lang/String;
} // .end local v3 # "activity":Ljava/lang/String;
} // :cond_1
/* .line 338 */
/* :catchall_0 */
/* move-exception v1 */
/* .line 339 */
/* .local v1, "e":Ljava/lang/Throwable; */
/* new-instance v2, Ljava/lang/StringBuilder; */
/* invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V */
final String v3 = "can not getStringExtra"; // const-string v3, "can not getStringExtra"
(( java.lang.StringBuilder ) v2 ).append ( v3 ); // invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v2 ).append ( v1 ); // invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v2 ).toString ( ); // invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
final String v3 = "AccessController"; // const-string v3, "AccessController"
android.util.Slog .e ( v3,v2 );
/* .line 342 */
} // .end local v1 # "e":Ljava/lang/Throwable;
} // :cond_2
} // :goto_1
} // .end method
void updatePasswordTypeForPattern ( Integer p0 ) {
/* .locals 2 */
/* .param p1, "userId" # I */
/* .line 471 */
v0 = /* invoke-direct {p0, p1}, Lcom/miui/server/AccessController;->haveAccessControlPattern(I)Z */
if ( v0 != null) { // if-eqz v0, :cond_0
v0 = /* invoke-direct {p0, p1}, Lcom/miui/server/AccessController;->haveAccessControlPasswordType(I)Z */
/* if-nez v0, :cond_0 */
/* .line 472 */
final String v0 = "pattern"; // const-string v0, "pattern"
/* invoke-direct {p0, v0, p1}, Lcom/miui/server/AccessController;->setAccessControlPasswordType(Ljava/lang/String;I)V */
/* .line 473 */
final String v0 = "AccessController"; // const-string v0, "AccessController"
/* const-string/jumbo v1, "update password type succeed" */
android.util.Log .d ( v0,v1 );
/* .line 475 */
} // :cond_0
return;
} // .end method
public void updateWhiteList ( ) {
/* .locals 5 */
/* .line 609 */
try { // :try_start_0
v0 = this.mContext;
(( android.content.Context ) v0 ).getContentResolver ( ); // invoke-virtual {v0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;
/* .line 610 */
/* .local v0, "resolver":Landroid/content/ContentResolver; */
v1 = this.mWorkHandler;
int v2 = 1; // const/4 v2, 0x1
(( com.miui.server.AccessController$WorkHandler ) v1 ).removeMessages ( v2 ); // invoke-virtual {v1, v2}, Lcom/miui/server/AccessController$WorkHandler;->removeMessages(I)V
/* .line 611 */
v1 = this.mWorkHandler;
/* const-wide/32 v3, 0x2932e00 */
(( com.miui.server.AccessController$WorkHandler ) v1 ).sendEmptyMessageDelayed ( v2, v3, v4 ); // invoke-virtual {v1, v2, v3, v4}, Lcom/miui/server/AccessController$WorkHandler;->sendEmptyMessageDelayed(IJ)Z
/* .line 612 */
final String v1 = "applock_whilte"; // const-string v1, "applock_whilte"
android.provider.MiuiSettings$SettingsCloudData .getCloudDataList ( v0,v1 );
/* .line 613 */
/* .local v1, "appLockList":Ljava/util/List;, "Ljava/util/List<Landroid/provider/MiuiSettings$SettingsCloudData$CloudData;>;" */
final String v2 = "gamebooster_antimsg"; // const-string v2, "gamebooster_antimsg"
android.provider.MiuiSettings$SettingsCloudData .getCloudDataList ( v0,v2 );
/* .line 615 */
/* .local v2, "gameAntimsgList":Ljava/util/List;, "Ljava/util/List<Landroid/provider/MiuiSettings$SettingsCloudData$CloudData;>;" */
final String v3 = "gamebooster_pay"; // const-string v3, "gamebooster_pay"
android.provider.MiuiSettings$SettingsCloudData .getCloudDataList ( v0,v3 );
/* .line 616 */
/* .local v3, "gamePayList":Ljava/util/List;, "Ljava/util/List<Landroid/provider/MiuiSettings$SettingsCloudData$CloudData;>;" */
v4 = com.miui.server.AccessController.mSkipList;
/* invoke-direct {p0, v1, v4}, Lcom/miui/server/AccessController;->updateWhiteList(Ljava/util/List;Landroid/util/ArrayMap;)V */
/* .line 617 */
v4 = com.miui.server.AccessController.mPayInterceptList;
/* invoke-direct {p0, v3, v4}, Lcom/miui/server/AccessController;->updateWhiteList(Ljava/util/List;Landroid/util/ArrayMap;)V */
/* :try_end_0 */
/* .catch Ljava/lang/Exception; {:try_start_0 ..:try_end_0} :catch_0 */
/* .line 620 */
} // .end local v0 # "resolver":Landroid/content/ContentResolver;
} // .end local v1 # "appLockList":Ljava/util/List;, "Ljava/util/List<Landroid/provider/MiuiSettings$SettingsCloudData$CloudData;>;"
} // .end local v2 # "gameAntimsgList":Ljava/util/List;, "Ljava/util/List<Landroid/provider/MiuiSettings$SettingsCloudData$CloudData;>;"
} // .end local v3 # "gamePayList":Ljava/util/List;, "Ljava/util/List<Landroid/provider/MiuiSettings$SettingsCloudData$CloudData;>;"
/* .line 618 */
/* :catch_0 */
/* move-exception v0 */
/* .line 619 */
/* .local v0, "e":Ljava/lang/Exception; */
(( java.lang.Exception ) v0 ).printStackTrace ( ); // invoke-virtual {v0}, Ljava/lang/Exception;->printStackTrace()V
/* .line 621 */
} // .end local v0 # "e":Ljava/lang/Exception;
} // :goto_0
return;
} // .end method
