public abstract class com.miui.server.ISplashScreenService implements android.os.IInterface {
	 /* .source "ISplashScreenService.java" */
	 /* # interfaces */
	 /* # annotations */
	 /* .annotation system Ldalvik/annotation/MemberClasses; */
	 /* value = { */
	 /* Lcom/miui/server/ISplashScreenService$Stub; */
	 /* } */
} // .end annotation
/* # virtual methods */
public abstract void activityIdle ( android.content.pm.ActivityInfo p0 ) {
	 /* .annotation system Ldalvik/annotation/Throws; */
	 /* value = { */
	 /* Landroid/os/RemoteException; */
	 /* } */
} // .end annotation
} // .end method
public abstract void destroyActivity ( android.content.pm.ActivityInfo p0 ) {
/* .annotation system Ldalvik/annotation/Throws; */
/* value = { */
/* Landroid/os/RemoteException; */
/* } */
} // .end annotation
} // .end method
public abstract android.content.Intent requestSplashScreen ( android.content.Intent p0, android.content.pm.ActivityInfo p1 ) {
/* .annotation system Ldalvik/annotation/Throws; */
/* value = { */
/* Landroid/os/RemoteException; */
/* } */
} // .end annotation
} // .end method
public abstract void setSplashPackageListener ( com.miui.server.ISplashPackageCheckListener p0 ) {
/* .annotation system Ldalvik/annotation/Throws; */
/* value = { */
/* Landroid/os/RemoteException; */
/* } */
} // .end annotation
} // .end method
