class com.miui.server.SecuritySmsHandler$2 extends android.content.BroadcastReceiver {
	 /* .source "SecuritySmsHandler.java" */
	 /* # annotations */
	 /* .annotation system Ldalvik/annotation/EnclosingClass; */
	 /* value = Lcom/miui/server/SecuritySmsHandler; */
} // .end annotation
/* .annotation system Ldalvik/annotation/InnerClass; */
/* accessFlags = 0x0 */
/* name = null */
} // .end annotation
/* # instance fields */
final com.miui.server.SecuritySmsHandler this$0; //synthetic
/* # direct methods */
 com.miui.server.SecuritySmsHandler$2 ( ) {
/* .locals 0 */
/* .param p1, "this$0" # Lcom/miui/server/SecuritySmsHandler; */
/* .line 299 */
this.this$0 = p1;
/* invoke-direct {p0}, Landroid/content/BroadcastReceiver;-><init>()V */
return;
} // .end method
/* # virtual methods */
public void onReceive ( android.content.Context p0, android.content.Intent p1 ) {
/* .locals 11 */
/* .param p1, "context" # Landroid/content/Context; */
/* .param p2, "intent" # Landroid/content/Intent; */
/* .line 302 */
(( android.content.Intent ) p2 ).getAction ( ); // invoke-virtual {p2}, Landroid/content/Intent;->getAction()Ljava/lang/String;
/* .line 303 */
/* .local v0, "action":Ljava/lang/String; */
v1 = com.miui.server.SecuritySmsHandler .getSlotIdFromIntent ( p2 );
/* .line 304 */
/* .local v1, "slotId":I */
/* new-instance v2, Ljava/lang/StringBuilder; */
/* invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V */
final String v3 = "mInterceptedSmsResultReceiver sms dispatched, action:"; // const-string v3, "mInterceptedSmsResultReceiver sms dispatched, action:"
(( java.lang.StringBuilder ) v2 ).append ( v3 ); // invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v2 ).append ( v0 ); // invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v2 ).toString ( ); // invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
final String v3 = "SecuritySmsHandler"; // const-string v3, "SecuritySmsHandler"
android.util.Log .i ( v3,v2 );
/* .line 306 */
final String v2 = "android.provider.Telephony.SMS_RECEIVED"; // const-string v2, "android.provider.Telephony.SMS_RECEIVED"
v2 = (( java.lang.String ) v2 ).equals ( v0 ); // invoke-virtual {v2, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z
if ( v2 != null) { // if-eqz v2, :cond_2
	 /* .line 307 */
	 v2 = 	 (( com.miui.server.SecuritySmsHandler$2 ) p0 ).getResultCode ( ); // invoke-virtual {p0}, Lcom/miui/server/SecuritySmsHandler$2;->getResultCode()I
	 /* .line 309 */
	 /* .local v2, "resultCode":I */
	 int v4 = -1; // const/4 v4, -0x1
	 /* if-ne v2, v4, :cond_2 */
	 /* .line 310 */
	 final String v4 = "mInterceptedSmsResultReceiver SMS_RECEIVED_ACTION not aborted"; // const-string v4, "mInterceptedSmsResultReceiver SMS_RECEIVED_ACTION not aborted"
	 android.util.Log .i ( v3,v4 );
	 /* .line 311 */
	 android.provider.Telephony$Sms$Intents .getMessagesFromIntent ( p2 );
	 /* .line 312 */
	 /* .local v4, "msgs":[Landroid/telephony/SmsMessage; */
	 /* new-instance v5, Ljava/lang/StringBuilder; */
	 /* invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V */
	 /* .line 313 */
	 /* .local v5, "sb":Ljava/lang/StringBuilder; */
	 int v6 = 0; // const/4 v6, 0x0
	 /* .local v6, "i":I */
} // :goto_0
/* array-length v7, v4 */
/* if-ge v6, v7, :cond_0 */
/* .line 314 */
/* aget-object v7, v4, v6 */
(( android.telephony.SmsMessage ) v7 ).getDisplayMessageBody ( ); // invoke-virtual {v7}, Landroid/telephony/SmsMessage;->getDisplayMessageBody()Ljava/lang/String;
(( java.lang.StringBuilder ) v5 ).append ( v7 ); // invoke-virtual {v5, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
/* .line 313 */
/* add-int/lit8 v6, v6, 0x1 */
/* .line 316 */
} // .end local v6 # "i":I
} // :cond_0
int v6 = 0; // const/4 v6, 0x0
/* aget-object v6, v4, v6 */
(( android.telephony.SmsMessage ) v6 ).getOriginatingAddress ( ); // invoke-virtual {v6}, Landroid/telephony/SmsMessage;->getOriginatingAddress()Ljava/lang/String;
/* .line 317 */
/* .local v6, "address":Ljava/lang/String; */
(( java.lang.StringBuilder ) v5 ).toString ( ); // invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
/* .line 320 */
/* .local v7, "body":Ljava/lang/String; */
v8 = this.this$0;
v8 = com.miui.server.SecuritySmsHandler .-$$Nest$mcheckByAntiSpam ( v8,v6,v7,v1 );
/* .line 321 */
/* .local v8, "blockType":I */
if ( v8 != null) { // if-eqz v8, :cond_2
/* .line 322 */
final String v9 = "blockType"; // const-string v9, "blockType"
(( android.content.Intent ) p2 ).putExtra ( v9, v8 ); // invoke-virtual {p2, v9, v8}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;
/* .line 323 */
v9 = miui.provider.ExtraTelephony .getRealBlockType ( v8 );
int v10 = 3; // const/4 v10, 0x3
/* if-lt v9, v10, :cond_1 */
/* .line 324 */
final String v9 = "mInterceptedSmsResultReceiver: This sms is intercepted by AntiSpam"; // const-string v9, "mInterceptedSmsResultReceiver: This sms is intercepted by AntiSpam"
android.util.Log .i ( v3,v9 );
/* .line 325 */
v3 = this.this$0;
com.miui.server.SecuritySmsHandler .-$$Nest$mdispatchSmsToAntiSpam ( v3,p2 );
/* .line 327 */
} // :cond_1
v3 = this.this$0;
com.miui.server.SecuritySmsHandler .-$$Nest$mdispatchNormalSms ( v3,p2 );
/* .line 332 */
} // .end local v2 # "resultCode":I
} // .end local v4 # "msgs":[Landroid/telephony/SmsMessage;
} // .end local v5 # "sb":Ljava/lang/StringBuilder;
} // .end local v6 # "address":Ljava/lang/String;
} // .end local v7 # "body":Ljava/lang/String;
} // .end local v8 # "blockType":I
} // :cond_2
} // :goto_1
return;
} // .end method
