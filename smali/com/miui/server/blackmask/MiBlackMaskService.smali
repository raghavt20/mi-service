.class public Lcom/miui/server/blackmask/MiBlackMaskService;
.super Lmiui/blackmask/IMiBlackMask$Stub;
.source "MiBlackMaskService.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/miui/server/blackmask/MiBlackMaskService$Lifecycle;
    }
.end annotation


# static fields
.field public static final SERVICE_NAME:Ljava/lang/String; = "miblackmask"

.field private static final TAG:Ljava/lang/String; = "MiBlackMaskService"


# instance fields
.field private final mContext:Landroid/content/Context;


# direct methods
.method private constructor <init>(Landroid/content/Context;)V
    .locals 0
    .param p1, "context"    # Landroid/content/Context;

    .line 22
    invoke-direct {p0}, Lmiui/blackmask/IMiBlackMask$Stub;-><init>()V

    .line 23
    iput-object p1, p0, Lcom/miui/server/blackmask/MiBlackMaskService;->mContext:Landroid/content/Context;

    .line 24
    return-void
.end method

.method synthetic constructor <init>(Landroid/content/Context;Lcom/miui/server/blackmask/MiBlackMaskService-IA;)V
    .locals 0

    invoke-direct {p0, p1}, Lcom/miui/server/blackmask/MiBlackMaskService;-><init>(Landroid/content/Context;)V

    return-void
.end method


# virtual methods
.method public finishAndRemoveSplashScreen(Ljava/lang/String;I)V
    .locals 1
    .param p1, "packageName"    # Ljava/lang/String;
    .param p2, "uid"    # I

    .line 42
    invoke-static {}, Lcom/android/server/wm/MiuiFreezeStub;->getInstance()Lcom/android/server/wm/MiuiFreezeStub;

    move-result-object v0

    invoke-interface {v0, p1, p2}, Lcom/android/server/wm/MiuiFreezeStub;->finishAndRemoveSplashScreen(Ljava/lang/String;I)V

    .line 43
    return-void
.end method
