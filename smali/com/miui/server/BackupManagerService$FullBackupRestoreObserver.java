class com.miui.server.BackupManagerService$FullBackupRestoreObserver extends android.app.backup.IFullBackupRestoreObserver$Stub {
	 /* .source "BackupManagerService.java" */
	 /* # annotations */
	 /* .annotation system Ldalvik/annotation/EnclosingClass; */
	 /* value = Lcom/miui/server/BackupManagerService; */
} // .end annotation
/* .annotation system Ldalvik/annotation/InnerClass; */
/* accessFlags = 0x2 */
/* name = "FullBackupRestoreObserver" */
} // .end annotation
/* # instance fields */
final com.miui.server.BackupManagerService this$0; //synthetic
/* # direct methods */
private com.miui.server.BackupManagerService$FullBackupRestoreObserver ( ) {
/* .locals 0 */
/* .line 832 */
this.this$0 = p1;
/* invoke-direct {p0}, Landroid/app/backup/IFullBackupRestoreObserver$Stub;-><init>()V */
return;
} // .end method
 com.miui.server.BackupManagerService$FullBackupRestoreObserver ( ) { //synthethic
/* .locals 0 */
/* invoke-direct {p0, p1}, Lcom/miui/server/BackupManagerService$FullBackupRestoreObserver;-><init>(Lcom/miui/server/BackupManagerService;)V */
return;
} // .end method
/* # virtual methods */
public void onBackupPackage ( java.lang.String p0 ) {
/* .locals 3 */
/* .param p1, "name" # Ljava/lang/String; */
/* .annotation system Ldalvik/annotation/Throws; */
/* value = { */
/* Landroid/os/RemoteException; */
/* } */
} // .end annotation
/* .line 840 */
v0 = this.this$0;
com.miui.server.BackupManagerService .-$$Nest$fgetmCurrentWorkingPkg ( v0 );
v0 = android.text.TextUtils .isEmpty ( v0 );
/* if-nez v0, :cond_0 */
v0 = this.this$0;
com.miui.server.BackupManagerService .-$$Nest$fgetmCurrentWorkingPkg ( v0 );
(( java.lang.String ) v0 ).equals ( p1 ); // invoke-virtual {v0, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z
/* .line 843 */
} // :cond_0
v0 = this.this$0;
com.miui.server.BackupManagerService .-$$Nest$fgetmBackupRestoreObserver ( v0 );
if ( v0 != null) { // if-eqz v0, :cond_1
/* .line 844 */
v0 = this.this$0;
com.miui.server.BackupManagerService .-$$Nest$fgetmBackupRestoreObserver ( v0 );
v1 = this.this$0;
com.miui.server.BackupManagerService .-$$Nest$fgetmCurrentWorkingPkg ( v1 );
v2 = this.this$0;
v2 = com.miui.server.BackupManagerService .-$$Nest$fgetmCurrentWorkingFeature ( v2 );
/* .line 846 */
} // :cond_1
return;
} // .end method
public void onEndBackup ( ) {
/* .locals 3 */
/* .annotation system Ldalvik/annotation/Throws; */
/* value = { */
/* Landroid/os/RemoteException; */
/* } */
} // .end annotation
/* .line 850 */
v0 = this.this$0;
com.miui.server.BackupManagerService .-$$Nest$fgetmBackupRestoreObserver ( v0 );
if ( v0 != null) { // if-eqz v0, :cond_0
/* .line 851 */
v0 = this.this$0;
com.miui.server.BackupManagerService .-$$Nest$fgetmBackupRestoreObserver ( v0 );
v1 = this.this$0;
com.miui.server.BackupManagerService .-$$Nest$fgetmCurrentWorkingPkg ( v1 );
v2 = this.this$0;
v2 = com.miui.server.BackupManagerService .-$$Nest$fgetmCurrentWorkingFeature ( v2 );
/* .line 853 */
} // :cond_0
return;
} // .end method
public void onEndRestore ( ) {
/* .locals 3 */
/* .annotation system Ldalvik/annotation/Throws; */
/* value = { */
/* Landroid/os/RemoteException; */
/* } */
} // .end annotation
/* .line 869 */
v0 = this.this$0;
com.miui.server.BackupManagerService .-$$Nest$fgetmBackupRestoreObserver ( v0 );
if ( v0 != null) { // if-eqz v0, :cond_0
/* .line 871 */
v0 = this.this$0;
com.miui.server.BackupManagerService .-$$Nest$fgetmBackupRestoreObserver ( v0 );
v1 = this.this$0;
com.miui.server.BackupManagerService .-$$Nest$fgetmCurrentWorkingPkg ( v1 );
v2 = this.this$0;
v2 = com.miui.server.BackupManagerService .-$$Nest$fgetmCurrentWorkingFeature ( v2 );
/* .line 873 */
} // :cond_0
return;
} // .end method
public void onRestorePackage ( java.lang.String p0 ) {
/* .locals 3 */
/* .param p1, "name" # Ljava/lang/String; */
/* .annotation system Ldalvik/annotation/Throws; */
/* value = { */
/* Landroid/os/RemoteException; */
/* } */
} // .end annotation
/* .line 861 */
v0 = this.this$0;
com.miui.server.BackupManagerService .-$$Nest$fgetmBackupRestoreObserver ( v0 );
if ( v0 != null) { // if-eqz v0, :cond_0
/* .line 863 */
v0 = this.this$0;
com.miui.server.BackupManagerService .-$$Nest$fgetmBackupRestoreObserver ( v0 );
v1 = this.this$0;
com.miui.server.BackupManagerService .-$$Nest$fgetmCurrentWorkingPkg ( v1 );
v2 = this.this$0;
v2 = com.miui.server.BackupManagerService .-$$Nest$fgetmCurrentWorkingFeature ( v2 );
/* .line 865 */
} // :cond_0
return;
} // .end method
public void onStartBackup ( ) {
/* .locals 0 */
/* .annotation system Ldalvik/annotation/Throws; */
/* value = { */
/* Landroid/os/RemoteException; */
/* } */
} // .end annotation
/* .line 836 */
return;
} // .end method
public void onStartRestore ( ) {
/* .locals 0 */
/* .annotation system Ldalvik/annotation/Throws; */
/* value = { */
/* Landroid/os/RemoteException; */
/* } */
} // .end annotation
/* .line 857 */
return;
} // .end method
public void onTimeout ( ) {
/* .locals 0 */
/* .annotation system Ldalvik/annotation/Throws; */
/* value = { */
/* Landroid/os/RemoteException; */
/* } */
} // .end annotation
/* .line 877 */
return;
} // .end method
