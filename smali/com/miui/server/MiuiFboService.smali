.class public Lcom/miui/server/MiuiFboService;
.super Lmiui/fbo/IFboManager$Stub;
.source "MiuiFboService.java"

# interfaces
.implements Lcom/miui/app/MiuiFboServiceInternal;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/miui/server/MiuiFboService$AlarmReceiver;,
        Lcom/miui/server/MiuiFboService$FboServiceHandler;,
        Lcom/miui/server/MiuiFboService$Lifecycle;
    }
.end annotation


# static fields
.field private static final APP_GC_AND_DISCARD:I = 0x4

.field private static final APP_ID:Ljava/lang/String; = "31000000454"

.field public static final CHANGE_USB_STATUS:I = 0x6

.field private static final CONNECT_NATIVESERVICE_NAME:Ljava/lang/String; = "FboNativeService"

.field public static final CONTINUE:Ljava/lang/String; = "continue"

.field private static final ENABLED_FBO:Ljava/lang/String; = "persist.sys.stability.miui_fbo_enable"

.field private static final EUA_PECYCLE_FREEBLOCK:I = 0x6

.field private static final EUA_UNMAP:I = 0x7

.field private static final FALSE:Ljava/lang/String; = "false"

.field private static final FBO_EVENT_NAME:Ljava/lang/String; = "fbo_event"

.field private static final FBO_START_COUNT:Ljava/lang/String; = "persist.sys.stability.miui_fbo_start_count"

.field private static final FBO_STATECTL:I = 0x3

.field private static final FLAG_NOT_LIMITED_BY_USER_EXPERIENCE_PLAN:I = 0x1

.field private static final HAL_DEFAULT:Ljava/lang/String; = "default"

.field private static final HAL_INTERFACE_DESCRIPTOR:Ljava/lang/String; = "vendor.xiaomi.hardware.fbo@1.0::IFbo"

.field private static final HAL_SERVICE_NAME:Ljava/lang/String; = "vendor.xiaomi.hardware.fbo@1.0::IFbo"

.field private static final HANDLER_NAME:Ljava/lang/String; = "fboServiceWork"

.field private static final IS_FBO_SUPPORTED:I = 0x1

.field private static final MIUI_FBO_PROCESSED_DONE:Ljava/lang/String; = "miui.intent.action.FBO_PROCESSED_DONE"

.field public static final MIUI_FBO_RECEIVER_START:Ljava/lang/String; = "miui.intent.action.start"

.field public static final MIUI_FBO_RECEIVER_STARTAGAIN:Ljava/lang/String; = "miui.intent.action.startAgain"

.field public static final MIUI_FBO_RECEIVER_STOP:Ljava/lang/String; = "miui.intent.action.stop"

.field public static final MIUI_FBO_RECEIVER_TRANSFERFBOTRIGGER:Ljava/lang/String; = "miui.intent.action.transferFboTrigger"

.field private static final NATIVE_SERVICE_KEY:Ljava/lang/String; = "persist.sys.fboservice.ctrl"

.field private static final NATIVE_SOCKET_NAME:Ljava/lang/String; = "fbs_native_socket"

.field private static final ONETRACK_PACKAGE_NAME:Ljava/lang/String; = "com.miui.analytics"

.field private static final ONE_TRACK_ACTION:Ljava/lang/String; = "onetrack.action.TRACK_EVENT"

.field private static final OVERLOAD_FBO_SUPPORTED:I = 0x5

.field private static final PACKAGE:Ljava/lang/String; = "android"

.field public static final SERVICE_NAME:Ljava/lang/String; = "miui.fbo.service"

.field public static final START_FBO:I = 0x1

.field public static final START_FBO_AGAIN:I = 0x2

.field public static final STOP:Ljava/lang/String; = "stop"

.field public static final STOPDUETOBATTERYTEMPERATURE:Ljava/lang/String; = "stopDueTobatteryTemperature"

.field public static final STOPDUETOSCREEN:Ljava/lang/String; = "stopDueToScreen"

.field public static final STOP_DUETO_BATTERYTEMPERATURE:I = 0x5

.field public static final STOP_DUETO_SCREEN:I = 0x4

.field public static final STOP_FBO:I = 0x3

.field private static final TAG:Ljava/lang/String;

.field private static final TRIGGER_CLD:I = 0x2

.field private static final TRUE:Ljava/lang/String; = "true"

.field private static cldManager:Lmiui/hardware/CldManager;

.field private static inputStream:Ljava/io/InputStream;

.field private static interactClientSocket:Landroid/net/LocalSocket;

.field private static listSize:I

.field private static mAlarmManager:Landroid/app/AlarmManager;

.field private static mCurrentPackageName:Ljava/lang/String;

.field private static mFinishedApp:Z

.field private static mFragmentCount:Ljava/lang/Long;

.field private static mKeepRunning:Z

.field private static final mLock:Ljava/lang/Object;

.field private static mPendingIntent:Landroid/app/PendingIntent;

.field private static mServerSocket:Landroid/net/LocalServerSocket;

.field private static mStopReason:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private static outputStream:Ljava/io/OutputStream;

.field private static packageName:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private static packageNameList:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private static pendingIntentList:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList<",
            "Landroid/app/PendingIntent;",
            ">;"
        }
    .end annotation
.end field

.field private static volatile sInstance:Lcom/miui/server/MiuiFboService;


# instance fields
.field private batteryLevel:I

.field private batteryStatus:I

.field private batteryStatusMark:I

.field private batteryTemperature:I

.field private volatile cldStrategyStatus:Z

.field private dueToScreenWait:Z

.field private enableNightJudgment:Z

.field private globalSwitch:Z

.field private mContext:Landroid/content/Context;

.field private mCurrentBatteryLevel:I

.field private mDriverSupport:Z

.field private volatile mFboNativeService:Lmiui/fbo/IFbo;

.field private volatile mFboServiceHandler:Lcom/miui/server/MiuiFboService$FboServiceHandler;

.field private mFboServiceThread:Landroid/os/HandlerThread;

.field private mVdexPath:Ljava/lang/String;

.field private messageHasbeenSent:Z

.field private nativeIsRunning:Z

.field private screenOn:Z

.field private screenOnTimes:I

.field private stagingData:Ljava/lang/String;

.field private usbState:Z


# direct methods
.method static bridge synthetic -$$Nest$fgetbatteryLevel(Lcom/miui/server/MiuiFboService;)I
    .locals 0

    iget p0, p0, Lcom/miui/server/MiuiFboService;->batteryLevel:I

    return p0
.end method

.method static bridge synthetic -$$Nest$fgetbatteryStatus(Lcom/miui/server/MiuiFboService;)I
    .locals 0

    iget p0, p0, Lcom/miui/server/MiuiFboService;->batteryStatus:I

    return p0
.end method

.method static bridge synthetic -$$Nest$fgetbatteryTemperature(Lcom/miui/server/MiuiFboService;)I
    .locals 0

    iget p0, p0, Lcom/miui/server/MiuiFboService;->batteryTemperature:I

    return p0
.end method

.method static bridge synthetic -$$Nest$fgetmContext(Lcom/miui/server/MiuiFboService;)Landroid/content/Context;
    .locals 0

    iget-object p0, p0, Lcom/miui/server/MiuiFboService;->mContext:Landroid/content/Context;

    return-object p0
.end method

.method static bridge synthetic -$$Nest$fgetmDriverSupport(Lcom/miui/server/MiuiFboService;)Z
    .locals 0

    iget-boolean p0, p0, Lcom/miui/server/MiuiFboService;->mDriverSupport:Z

    return p0
.end method

.method static bridge synthetic -$$Nest$fgetmFboNativeService(Lcom/miui/server/MiuiFboService;)Lmiui/fbo/IFbo;
    .locals 0

    iget-object p0, p0, Lcom/miui/server/MiuiFboService;->mFboNativeService:Lmiui/fbo/IFbo;

    return-object p0
.end method

.method static bridge synthetic -$$Nest$fgetmFboServiceHandler(Lcom/miui/server/MiuiFboService;)Lcom/miui/server/MiuiFboService$FboServiceHandler;
    .locals 0

    iget-object p0, p0, Lcom/miui/server/MiuiFboService;->mFboServiceHandler:Lcom/miui/server/MiuiFboService$FboServiceHandler;

    return-object p0
.end method

.method static bridge synthetic -$$Nest$fgetmVdexPath(Lcom/miui/server/MiuiFboService;)Ljava/lang/String;
    .locals 0

    iget-object p0, p0, Lcom/miui/server/MiuiFboService;->mVdexPath:Ljava/lang/String;

    return-object p0
.end method

.method static bridge synthetic -$$Nest$fgetscreenOn(Lcom/miui/server/MiuiFboService;)Z
    .locals 0

    iget-boolean p0, p0, Lcom/miui/server/MiuiFboService;->screenOn:Z

    return p0
.end method

.method static bridge synthetic -$$Nest$fgetscreenOnTimes(Lcom/miui/server/MiuiFboService;)I
    .locals 0

    iget p0, p0, Lcom/miui/server/MiuiFboService;->screenOnTimes:I

    return p0
.end method

.method static bridge synthetic -$$Nest$fgetstagingData(Lcom/miui/server/MiuiFboService;)Ljava/lang/String;
    .locals 0

    iget-object p0, p0, Lcom/miui/server/MiuiFboService;->stagingData:Ljava/lang/String;

    return-object p0
.end method

.method static bridge synthetic -$$Nest$fputcldStrategyStatus(Lcom/miui/server/MiuiFboService;Z)V
    .locals 0

    iput-boolean p1, p0, Lcom/miui/server/MiuiFboService;->cldStrategyStatus:Z

    return-void
.end method

.method static bridge synthetic -$$Nest$fputmCurrentBatteryLevel(Lcom/miui/server/MiuiFboService;I)V
    .locals 0

    iput p1, p0, Lcom/miui/server/MiuiFboService;->mCurrentBatteryLevel:I

    return-void
.end method

.method static bridge synthetic -$$Nest$fputmFboNativeService(Lcom/miui/server/MiuiFboService;Lmiui/fbo/IFbo;)V
    .locals 0

    iput-object p1, p0, Lcom/miui/server/MiuiFboService;->mFboNativeService:Lmiui/fbo/IFbo;

    return-void
.end method

.method static bridge synthetic -$$Nest$fputmVdexPath(Lcom/miui/server/MiuiFboService;Ljava/lang/String;)V
    .locals 0

    iput-object p1, p0, Lcom/miui/server/MiuiFboService;->mVdexPath:Ljava/lang/String;

    return-void
.end method

.method static bridge synthetic -$$Nest$fputmessageHasbeenSent(Lcom/miui/server/MiuiFboService;Z)V
    .locals 0

    iput-boolean p1, p0, Lcom/miui/server/MiuiFboService;->messageHasbeenSent:Z

    return-void
.end method

.method static bridge synthetic -$$Nest$fputscreenOnTimes(Lcom/miui/server/MiuiFboService;I)V
    .locals 0

    iput p1, p0, Lcom/miui/server/MiuiFboService;->screenOnTimes:I

    return-void
.end method

.method static bridge synthetic -$$Nest$fputstagingData(Lcom/miui/server/MiuiFboService;Ljava/lang/String;)V
    .locals 0

    iput-object p1, p0, Lcom/miui/server/MiuiFboService;->stagingData:Ljava/lang/String;

    return-void
.end method

.method static bridge synthetic -$$Nest$mforSystemServerInitialization(Lcom/miui/server/MiuiFboService;Landroid/content/Context;)Lcom/miui/server/MiuiFboService;
    .locals 0

    invoke-direct {p0, p1}, Lcom/miui/server/MiuiFboService;->forSystemServerInitialization(Landroid/content/Context;)Lcom/miui/server/MiuiFboService;

    move-result-object p0

    return-object p0
.end method

.method static bridge synthetic -$$Nest$misWithinTheTimeInterval(Lcom/miui/server/MiuiFboService;)Z
    .locals 0

    invoke-direct {p0}, Lcom/miui/server/MiuiFboService;->isWithinTheTimeInterval()Z

    move-result p0

    return p0
.end method

.method static bridge synthetic -$$Nest$mreportFboEvent(Lcom/miui/server/MiuiFboService;)V
    .locals 0

    invoke-direct {p0}, Lcom/miui/server/MiuiFboService;->reportFboEvent()V

    return-void
.end method

.method static bridge synthetic -$$Nest$msendStopOrContinueToHal(Lcom/miui/server/MiuiFboService;Ljava/lang/String;)V
    .locals 0

    invoke-direct {p0, p1}, Lcom/miui/server/MiuiFboService;->sendStopOrContinueToHal(Ljava/lang/String;)V

    return-void
.end method

.method static bridge synthetic -$$Nest$muserAreaExtend(Lcom/miui/server/MiuiFboService;)V
    .locals 0

    invoke-direct {p0}, Lcom/miui/server/MiuiFboService;->userAreaExtend()V

    return-void
.end method

.method static bridge synthetic -$$Nest$sfgetTAG()Ljava/lang/String;
    .locals 1

    sget-object v0, Lcom/miui/server/MiuiFboService;->TAG:Ljava/lang/String;

    return-object v0
.end method

.method static bridge synthetic -$$Nest$sfgetinputStream()Ljava/io/InputStream;
    .locals 1

    sget-object v0, Lcom/miui/server/MiuiFboService;->inputStream:Ljava/io/InputStream;

    return-object v0
.end method

.method static bridge synthetic -$$Nest$sfgetinteractClientSocket()Landroid/net/LocalSocket;
    .locals 1

    sget-object v0, Lcom/miui/server/MiuiFboService;->interactClientSocket:Landroid/net/LocalSocket;

    return-object v0
.end method

.method static bridge synthetic -$$Nest$sfgetlistSize()I
    .locals 1

    sget v0, Lcom/miui/server/MiuiFboService;->listSize:I

    return v0
.end method

.method static bridge synthetic -$$Nest$sfgetmCurrentPackageName()Ljava/lang/String;
    .locals 1

    sget-object v0, Lcom/miui/server/MiuiFboService;->mCurrentPackageName:Ljava/lang/String;

    return-object v0
.end method

.method static bridge synthetic -$$Nest$sfgetmFinishedApp()Z
    .locals 1

    sget-boolean v0, Lcom/miui/server/MiuiFboService;->mFinishedApp:Z

    return v0
.end method

.method static bridge synthetic -$$Nest$sfgetmFragmentCount()Ljava/lang/Long;
    .locals 1

    sget-object v0, Lcom/miui/server/MiuiFboService;->mFragmentCount:Ljava/lang/Long;

    return-object v0
.end method

.method static bridge synthetic -$$Nest$sfgetmKeepRunning()Z
    .locals 1

    sget-boolean v0, Lcom/miui/server/MiuiFboService;->mKeepRunning:Z

    return v0
.end method

.method static bridge synthetic -$$Nest$sfgetmServerSocket()Landroid/net/LocalServerSocket;
    .locals 1

    sget-object v0, Lcom/miui/server/MiuiFboService;->mServerSocket:Landroid/net/LocalServerSocket;

    return-object v0
.end method

.method static bridge synthetic -$$Nest$sfgetmStopReason()Ljava/util/ArrayList;
    .locals 1

    sget-object v0, Lcom/miui/server/MiuiFboService;->mStopReason:Ljava/util/ArrayList;

    return-object v0
.end method

.method static bridge synthetic -$$Nest$sfgetoutputStream()Ljava/io/OutputStream;
    .locals 1

    sget-object v0, Lcom/miui/server/MiuiFboService;->outputStream:Ljava/io/OutputStream;

    return-object v0
.end method

.method static bridge synthetic -$$Nest$sfgetpackageNameList()Ljava/util/ArrayList;
    .locals 1

    sget-object v0, Lcom/miui/server/MiuiFboService;->packageNameList:Ljava/util/ArrayList;

    return-object v0
.end method

.method static bridge synthetic -$$Nest$sfgetsInstance()Lcom/miui/server/MiuiFboService;
    .locals 1

    sget-object v0, Lcom/miui/server/MiuiFboService;->sInstance:Lcom/miui/server/MiuiFboService;

    return-object v0
.end method

.method static bridge synthetic -$$Nest$sfputinputStream(Ljava/io/InputStream;)V
    .locals 0

    sput-object p0, Lcom/miui/server/MiuiFboService;->inputStream:Ljava/io/InputStream;

    return-void
.end method

.method static bridge synthetic -$$Nest$sfputinteractClientSocket(Landroid/net/LocalSocket;)V
    .locals 0

    sput-object p0, Lcom/miui/server/MiuiFboService;->interactClientSocket:Landroid/net/LocalSocket;

    return-void
.end method

.method static bridge synthetic -$$Nest$sfputlistSize(I)V
    .locals 0

    sput p0, Lcom/miui/server/MiuiFboService;->listSize:I

    return-void
.end method

.method static bridge synthetic -$$Nest$sfputmCurrentPackageName(Ljava/lang/String;)V
    .locals 0

    sput-object p0, Lcom/miui/server/MiuiFboService;->mCurrentPackageName:Ljava/lang/String;

    return-void
.end method

.method static bridge synthetic -$$Nest$sfputmFinishedApp(Z)V
    .locals 0

    sput-boolean p0, Lcom/miui/server/MiuiFboService;->mFinishedApp:Z

    return-void
.end method

.method static bridge synthetic -$$Nest$sfputmFragmentCount(Ljava/lang/Long;)V
    .locals 0

    sput-object p0, Lcom/miui/server/MiuiFboService;->mFragmentCount:Ljava/lang/Long;

    return-void
.end method

.method static bridge synthetic -$$Nest$sfputmKeepRunning(Z)V
    .locals 0

    sput-boolean p0, Lcom/miui/server/MiuiFboService;->mKeepRunning:Z

    return-void
.end method

.method static bridge synthetic -$$Nest$sfputmServerSocket(Landroid/net/LocalServerSocket;)V
    .locals 0

    sput-object p0, Lcom/miui/server/MiuiFboService;->mServerSocket:Landroid/net/LocalServerSocket;

    return-void
.end method

.method static bridge synthetic -$$Nest$sfputoutputStream(Ljava/io/OutputStream;)V
    .locals 0

    sput-object p0, Lcom/miui/server/MiuiFboService;->outputStream:Ljava/io/OutputStream;

    return-void
.end method

.method static bridge synthetic -$$Nest$smaggregateBroadcastData(Ljava/lang/String;)V
    .locals 0

    invoke-static {p0}, Lcom/miui/server/MiuiFboService;->aggregateBroadcastData(Ljava/lang/String;)V

    return-void
.end method

.method static bridge synthetic -$$Nest$smcallHalFunction(Ljava/lang/String;I)Ljava/lang/Object;
    .locals 0

    invoke-static {p0, p1}, Lcom/miui/server/MiuiFboService;->callHalFunction(Ljava/lang/String;I)Ljava/lang/Object;

    move-result-object p0

    return-object p0
.end method

.method static bridge synthetic -$$Nest$smclearBroadcastData()V
    .locals 0

    invoke-static {}, Lcom/miui/server/MiuiFboService;->clearBroadcastData()V

    return-void
.end method

.method static bridge synthetic -$$Nest$smsetAlarm(Ljava/lang/String;Ljava/lang/String;JZ)V
    .locals 0

    invoke-static {p0, p1, p2, p3, p4}, Lcom/miui/server/MiuiFboService;->setAlarm(Ljava/lang/String;Ljava/lang/String;JZ)V

    return-void
.end method

.method static bridge synthetic -$$Nest$smuseCldStrategy(I)V
    .locals 0

    invoke-static {p0}, Lcom/miui/server/MiuiFboService;->useCldStrategy(I)V

    return-void
.end method

.method static bridge synthetic -$$Nest$smwriteFailToHal()V
    .locals 0

    invoke-static {}, Lcom/miui/server/MiuiFboService;->writeFailToHal()V

    return-void
.end method

.method static constructor <clinit>()V
    .locals 2

    .line 56
    const-class v0, Lcom/miui/server/MiuiFboService;

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/miui/server/MiuiFboService;->TAG:Ljava/lang/String;

    .line 58
    const/4 v0, 0x0

    sput-object v0, Lcom/miui/server/MiuiFboService;->sInstance:Lcom/miui/server/MiuiFboService;

    .line 77
    const/4 v1, 0x1

    sput-boolean v1, Lcom/miui/server/MiuiFboService;->mKeepRunning:Z

    .line 78
    const/4 v1, 0x0

    sput-boolean v1, Lcom/miui/server/MiuiFboService;->mFinishedApp:Z

    .line 81
    sput-object v0, Lcom/miui/server/MiuiFboService;->outputStream:Ljava/io/OutputStream;

    .line 82
    sput-object v0, Lcom/miui/server/MiuiFboService;->inputStream:Ljava/io/InputStream;

    .line 83
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    sput-object v0, Lcom/miui/server/MiuiFboService;->packageNameList:Ljava/util/ArrayList;

    .line 85
    sput v1, Lcom/miui/server/MiuiFboService;->listSize:I

    .line 88
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    sput-object v0, Lcom/miui/server/MiuiFboService;->mStopReason:Ljava/util/ArrayList;

    .line 89
    new-instance v0, Ljava/lang/Object;

    invoke-direct {v0}, Ljava/lang/Object;-><init>()V

    sput-object v0, Lcom/miui/server/MiuiFboService;->mLock:Ljava/lang/Object;

    .line 137
    const-wide/16 v0, 0x0

    invoke-static {v0, v1}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v0

    sput-object v0, Lcom/miui/server/MiuiFboService;->mFragmentCount:Ljava/lang/Long;

    .line 138
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    sput-object v0, Lcom/miui/server/MiuiFboService;->packageName:Ljava/util/ArrayList;

    .line 139
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    sput-object v0, Lcom/miui/server/MiuiFboService;->pendingIntentList:Ljava/util/ArrayList;

    return-void
.end method

.method private constructor <init>()V
    .locals 2

    .line 378
    invoke-direct {p0}, Lmiui/fbo/IFboManager$Stub;-><init>()V

    .line 63
    const/4 v0, 0x0

    iput v0, p0, Lcom/miui/server/MiuiFboService;->batteryStatusMark:I

    .line 65
    iput-boolean v0, p0, Lcom/miui/server/MiuiFboService;->messageHasbeenSent:Z

    .line 66
    iput-boolean v0, p0, Lcom/miui/server/MiuiFboService;->nativeIsRunning:Z

    .line 67
    iput-boolean v0, p0, Lcom/miui/server/MiuiFboService;->globalSwitch:Z

    .line 68
    iput-boolean v0, p0, Lcom/miui/server/MiuiFboService;->dueToScreenWait:Z

    .line 69
    iput v0, p0, Lcom/miui/server/MiuiFboService;->screenOnTimes:I

    .line 71
    const/4 v1, 0x1

    iput-boolean v1, p0, Lcom/miui/server/MiuiFboService;->enableNightJudgment:Z

    .line 72
    iput-boolean v1, p0, Lcom/miui/server/MiuiFboService;->usbState:Z

    .line 73
    iput-boolean v0, p0, Lcom/miui/server/MiuiFboService;->mDriverSupport:Z

    .line 74
    iput v0, p0, Lcom/miui/server/MiuiFboService;->mCurrentBatteryLevel:I

    .line 379
    invoke-static {}, Lcom/miui/server/MiuiFboService;->initFboSocket()V

    .line 380
    new-instance v0, Landroid/os/HandlerThread;

    const-string v1, "fboServiceWork"

    invoke-direct {v0, v1}, Landroid/os/HandlerThread;-><init>(Ljava/lang/String;)V

    iput-object v0, p0, Lcom/miui/server/MiuiFboService;->mFboServiceThread:Landroid/os/HandlerThread;

    .line 381
    invoke-virtual {v0}, Landroid/os/HandlerThread;->start()V

    .line 382
    new-instance v0, Lcom/miui/server/MiuiFboService$FboServiceHandler;

    iget-object v1, p0, Lcom/miui/server/MiuiFboService;->mFboServiceThread:Landroid/os/HandlerThread;

    invoke-virtual {v1}, Landroid/os/HandlerThread;->getLooper()Landroid/os/Looper;

    move-result-object v1

    invoke-direct {v0, p0, v1}, Lcom/miui/server/MiuiFboService$FboServiceHandler;-><init>(Lcom/miui/server/MiuiFboService;Landroid/os/Looper;)V

    iput-object v0, p0, Lcom/miui/server/MiuiFboService;->mFboServiceHandler:Lcom/miui/server/MiuiFboService$FboServiceHandler;

    .line 383
    const-class v0, Lcom/miui/app/MiuiFboServiceInternal;

    invoke-static {v0, p0}, Lcom/android/server/LocalServices;->addService(Ljava/lang/Class;Ljava/lang/Object;)V

    .line 384
    return-void
.end method

.method private static aggregateBroadcastData(Ljava/lang/String;)V
    .locals 10
    .param p0, "dataReceived"    # Ljava/lang/String;

    .line 448
    :try_start_0
    sget-object v0, Lcom/miui/server/MiuiFboService;->mLock:Ljava/lang/Object;

    monitor-enter v0
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 449
    :try_start_1
    const-string v1, ":"

    invoke-virtual {p0, v1}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v1

    .line 450
    .local v1, "splitDataReceived":[Ljava/lang/String;
    sget-object v2, Lcom/miui/server/MiuiFboService;->packageName:Ljava/util/ArrayList;

    const/4 v3, 0x1

    aget-object v3, v1, v3

    invoke-virtual {v2, v3}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 451
    const/4 v2, 0x3

    aget-object v2, v1, v2

    invoke-static {v2}, Ljava/lang/Long;->parseLong(Ljava/lang/String;)J

    move-result-wide v2

    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v2

    .line 452
    .local v2, "beforeCleaning":Ljava/lang/Long;
    const/4 v3, 0x4

    aget-object v3, v1, v3

    invoke-static {v3}, Ljava/lang/Long;->parseLong(Ljava/lang/String;)J

    move-result-wide v3

    invoke-static {v3, v4}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v3

    .line 453
    .local v3, "afterCleaning":Ljava/lang/Long;
    sget-object v4, Lcom/miui/server/MiuiFboService;->mFragmentCount:Ljava/lang/Long;

    invoke-virtual {v4}, Ljava/lang/Long;->longValue()J

    move-result-wide v4

    invoke-virtual {v2}, Ljava/lang/Long;->longValue()J

    move-result-wide v6

    invoke-virtual {v3}, Ljava/lang/Long;->longValue()J

    move-result-wide v8

    sub-long/2addr v6, v8

    add-long/2addr v4, v6

    invoke-static {v4, v5}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v4

    sput-object v4, Lcom/miui/server/MiuiFboService;->mFragmentCount:Ljava/lang/Long;

    .line 454
    sget-object v4, Lcom/miui/server/MiuiFboService;->sInstance:Lcom/miui/server/MiuiFboService;

    invoke-direct {v4}, Lcom/miui/server/MiuiFboService;->reportFboProcessedBroadcast()V

    .line 455
    sget-object v4, Lcom/miui/server/MiuiFboService;->outputStream:Ljava/io/OutputStream;

    const-string/jumbo v5, "{broadcast send success}"

    invoke-virtual {v5}, Ljava/lang/String;->getBytes()[B

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/io/OutputStream;->write([B)V

    .line 456
    .end local v1    # "splitDataReceived":[Ljava/lang/String;
    .end local v2    # "beforeCleaning":Ljava/lang/Long;
    .end local v3    # "afterCleaning":Ljava/lang/Long;
    monitor-exit v0

    .line 460
    goto :goto_0

    .line 456
    :catchall_0
    move-exception v1

    monitor-exit v0
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .end local p0    # "dataReceived":Ljava/lang/String;
    :try_start_2
    throw v1
    :try_end_2
    .catch Ljava/lang/Exception; {:try_start_2 .. :try_end_2} :catch_0

    .line 457
    .restart local p0    # "dataReceived":Ljava/lang/String;
    :catch_0
    move-exception v0

    .line 458
    .local v0, "e":Ljava/lang/Exception;
    invoke-virtual {v0}, Ljava/lang/Exception;->printStackTrace()V

    .line 459
    invoke-static {}, Lcom/miui/server/MiuiFboService;->writeFailToNative()V

    .line 461
    .end local v0    # "e":Ljava/lang/Exception;
    :goto_0
    return-void
.end method

.method private static callHalFunction(Ljava/lang/String;I)Ljava/lang/Object;
    .locals 6
    .param p0, "writeData"    # Ljava/lang/String;
    .param p1, "halFunctionName"    # I

    .line 656
    const-string/jumbo v0, "vendor.xiaomi.hardware.fbo@1.0::IFbo"

    new-instance v1, Landroid/os/HwParcel;

    invoke-direct {v1}, Landroid/os/HwParcel;-><init>()V

    .line 658
    .local v1, "hidl_reply":Landroid/os/HwParcel;
    const/4 v2, 0x0

    :try_start_0
    const-string v3, "default"

    invoke-static {v0, v3}, Landroid/os/HwBinder;->getService(Ljava/lang/String;Ljava/lang/String;)Landroid/os/IHwBinder;

    move-result-object v3

    .line 659
    .local v3, "hwService":Landroid/os/IHwBinder;
    if-eqz v3, :cond_1

    .line 660
    new-instance v4, Landroid/os/HwParcel;

    invoke-direct {v4}, Landroid/os/HwParcel;-><init>()V

    .line 661
    .local v4, "hidl_request":Landroid/os/HwParcel;
    invoke-virtual {v4, v0}, Landroid/os/HwParcel;->writeInterfaceToken(Ljava/lang/String;)V

    .line 662
    if-eqz p0, :cond_0

    .line 663
    invoke-virtual {v4, p0}, Landroid/os/HwParcel;->writeString(Ljava/lang/String;)V

    .line 665
    :cond_0
    const/4 v0, 0x0

    invoke-interface {v3, p1, v4, v1, v0}, Landroid/os/IHwBinder;->transact(ILandroid/os/HwParcel;Landroid/os/HwParcel;I)V

    .line 666
    invoke-virtual {v1}, Landroid/os/HwParcel;->verifySuccess()V

    .line 667
    invoke-virtual {v4}, Landroid/os/HwParcel;->releaseTemporaryStorage()V

    .line 668
    packed-switch p1, :pswitch_data_0

    :pswitch_0
    goto :goto_0

    .line 676
    :pswitch_1
    invoke-virtual {v1}, Landroid/os/HwParcel;->readString()Ljava/lang/String;

    move-result-object v0
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 684
    invoke-virtual {v1}, Landroid/os/HwParcel;->release()V

    .line 676
    return-object v0

    .line 672
    :pswitch_2
    :try_start_1
    invoke-virtual {v1}, Landroid/os/HwParcel;->readBool()Z

    move-result v0

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_0
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 684
    invoke-virtual {v1}, Landroid/os/HwParcel;->release()V

    .line 672
    return-object v0

    .line 679
    .end local v4    # "hidl_request":Landroid/os/HwParcel;
    :cond_1
    :goto_0
    nop

    .line 684
    invoke-virtual {v1}, Landroid/os/HwParcel;->release()V

    .line 679
    return-object v2

    .line 684
    .end local v3    # "hwService":Landroid/os/IHwBinder;
    :catchall_0
    move-exception v0

    goto :goto_1

    .line 680
    :catch_0
    move-exception v0

    .line 681
    .local v0, "e":Ljava/lang/Exception;
    :try_start_2
    sget-object v3, Lcom/miui/server/MiuiFboService;->TAG:Ljava/lang/String;

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "crash in the callHalFunction:"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Landroid/util/Slog;->e(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    .line 682
    nop

    .line 684
    invoke-virtual {v1}, Landroid/os/HwParcel;->release()V

    .line 682
    return-object v2

    .line 684
    .end local v0    # "e":Ljava/lang/Exception;
    :goto_1
    invoke-virtual {v1}, Landroid/os/HwParcel;->release()V

    .line 685
    throw v0

    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_2
        :pswitch_1
        :pswitch_0
        :pswitch_0
        :pswitch_2
        :pswitch_1
    .end packed-switch
.end method

.method private static clearBroadcastData()V
    .locals 2

    .line 783
    const-wide/16 v0, 0x0

    invoke-static {v0, v1}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v0

    sput-object v0, Lcom/miui/server/MiuiFboService;->mFragmentCount:Ljava/lang/Long;

    .line 784
    sget-object v0, Lcom/miui/server/MiuiFboService;->packageName:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->clear()V

    .line 785
    return-void
.end method

.method private forSystemServerInitialization(Landroid/content/Context;)Lcom/miui/server/MiuiFboService;
    .locals 4
    .param p1, "context"    # Landroid/content/Context;

    .line 338
    const-string v0, "persist.sys.fboservice.ctrl"

    const-string v1, "false"

    invoke-static {v0, v1}, Landroid/os/SystemProperties;->set(Ljava/lang/String;Ljava/lang/String;)V

    .line 339
    iput-object p1, p0, Lcom/miui/server/MiuiFboService;->mContext:Landroid/content/Context;

    .line 340
    invoke-static {p1}, Lmiui/hardware/CldManager;->getInstance(Landroid/content/Context;)Lmiui/hardware/CldManager;

    move-result-object v0

    sput-object v0, Lcom/miui/server/MiuiFboService;->cldManager:Lmiui/hardware/CldManager;

    .line 341
    iget-object v0, p0, Lcom/miui/server/MiuiFboService;->mContext:Landroid/content/Context;

    const-string v1, "alarm"

    invoke-virtual {v0, v1}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/app/AlarmManager;

    sput-object v0, Lcom/miui/server/MiuiFboService;->mAlarmManager:Landroid/app/AlarmManager;

    .line 342
    new-instance v0, Landroid/content/IntentFilter;

    invoke-direct {v0}, Landroid/content/IntentFilter;-><init>()V

    .line 343
    .local v0, "intentFilter":Landroid/content/IntentFilter;
    new-instance v1, Lcom/miui/server/MiuiFboService$AlarmReceiver;

    invoke-direct {v1}, Lcom/miui/server/MiuiFboService$AlarmReceiver;-><init>()V

    .line 344
    .local v1, "alarmReceiver":Lcom/miui/server/MiuiFboService$AlarmReceiver;
    const-string v2, "miui.intent.action.start"

    invoke-virtual {v0, v2}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    .line 345
    const-string v2, "miui.intent.action.startAgain"

    invoke-virtual {v0, v2}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    .line 346
    const-string v2, "miui.intent.action.transferFboTrigger"

    invoke-virtual {v0, v2}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    .line 347
    const-string v2, "miui.intent.action.stop"

    invoke-virtual {v0, v2}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    .line 348
    iget-object v2, p0, Lcom/miui/server/MiuiFboService;->mContext:Landroid/content/Context;

    invoke-virtual {v2, v1, v0}, Landroid/content/Context;->registerReceiver(Landroid/content/BroadcastReceiver;Landroid/content/IntentFilter;)Landroid/content/Intent;

    .line 349
    const-string v2, "persist.sys.stability.miui_fbo_start_count"

    const-string v3, "0"

    invoke-static {v2, v3}, Landroid/os/SystemProperties;->set(Ljava/lang/String;Ljava/lang/String;)V

    .line 350
    invoke-static {}, Lcom/miui/server/MiuiFboService;->getInstance()Lcom/miui/server/MiuiFboService;

    move-result-object v2

    return-object v2
.end method

.method private static formatAppList(Ljava/lang/String;)V
    .locals 5
    .param p0, "appList"    # Ljava/lang/String;

    .line 605
    sget-object v0, Lcom/miui/server/MiuiFboService;->packageNameList:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->clear()V

    .line 606
    const-string v0, ","

    invoke-virtual {p0, v0}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v0

    .line 607
    .local v0, "split":[Ljava/lang/String;
    const/4 v1, 0x0

    .local v1, "i":I
    :goto_0
    array-length v2, v0

    if-ge v1, v2, :cond_0

    .line 608
    aget-object v2, v0, v1

    const-string v3, "\""

    invoke-virtual {v2, v3}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v2

    .line 609
    .local v2, "splitFinal":[Ljava/lang/String;
    sget-object v3, Lcom/miui/server/MiuiFboService;->packageNameList:Ljava/util/ArrayList;

    const/4 v4, 0x1

    aget-object v4, v2, v4

    invoke-virtual {v3, v4}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 607
    .end local v2    # "splitFinal":[Ljava/lang/String;
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 611
    .end local v1    # "i":I
    :cond_0
    return-void
.end method

.method public static getInstance()Lcom/miui/server/MiuiFboService;
    .locals 2

    .line 354
    sget-object v0, Lcom/miui/server/MiuiFboService;->sInstance:Lcom/miui/server/MiuiFboService;

    if-nez v0, :cond_1

    .line 355
    const-class v0, Lcom/miui/server/MiuiFboService;

    monitor-enter v0

    .line 356
    :try_start_0
    sget-object v1, Lcom/miui/server/MiuiFboService;->sInstance:Lcom/miui/server/MiuiFboService;

    if-nez v1, :cond_0

    .line 357
    new-instance v1, Lcom/miui/server/MiuiFboService;

    invoke-direct {v1}, Lcom/miui/server/MiuiFboService;-><init>()V

    sput-object v1, Lcom/miui/server/MiuiFboService;->sInstance:Lcom/miui/server/MiuiFboService;

    .line 359
    :cond_0
    monitor-exit v0

    goto :goto_0

    :catchall_0
    move-exception v1

    monitor-exit v0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v1

    .line 361
    :cond_1
    :goto_0
    sget-object v0, Lcom/miui/server/MiuiFboService;->sInstance:Lcom/miui/server/MiuiFboService;

    return-object v0
.end method

.method private getNativeService()Lmiui/fbo/IFbo;
    .locals 4

    .line 627
    const-string/jumbo v0, "true"

    const-string v1, "persist.sys.fboservice.ctrl"

    invoke-static {v1, v0}, Landroid/os/SystemProperties;->set(Ljava/lang/String;Ljava/lang/String;)V

    .line 628
    const/4 v0, 0x0

    .line 630
    .local v0, "binder":Landroid/os/IBinder;
    const-wide/16 v2, 0x3e8

    :try_start_0
    invoke-static {v2, v3}, Ljava/lang/Thread;->sleep(J)V

    .line 631
    const-string v2, "FboNativeService"

    invoke-static {v2}, Landroid/os/ServiceManager;->getService(Ljava/lang/String;)Landroid/os/IBinder;

    move-result-object v2

    move-object v0, v2

    .line 632
    if-eqz v0, :cond_0

    .line 633
    new-instance v2, Lcom/miui/server/MiuiFboService$2;

    invoke-direct {v2, p0}, Lcom/miui/server/MiuiFboService$2;-><init>(Lcom/miui/server/MiuiFboService;)V

    const/4 v3, 0x0

    invoke-interface {v0, v2, v3}, Landroid/os/IBinder;->linkToDeath(Landroid/os/IBinder$DeathRecipient;I)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 644
    :cond_0
    goto :goto_0

    .line 641
    :catch_0
    move-exception v2

    .line 642
    .local v2, "e":Ljava/lang/Exception;
    const/4 v0, 0x0

    .line 643
    const-string v3, "false"

    invoke-static {v1, v3}, Landroid/os/SystemProperties;->set(Ljava/lang/String;Ljava/lang/String;)V

    .line 645
    .end local v2    # "e":Ljava/lang/Exception;
    :goto_0
    if-eqz v0, :cond_1

    .line 646
    invoke-static {v0}, Lmiui/fbo/IFbo$Stub;->asInterface(Landroid/os/IBinder;)Lmiui/fbo/IFbo;

    move-result-object v1

    iput-object v1, p0, Lcom/miui/server/MiuiFboService;->mFboNativeService:Lmiui/fbo/IFbo;

    .line 647
    iget-object v1, p0, Lcom/miui/server/MiuiFboService;->mFboNativeService:Lmiui/fbo/IFbo;

    return-object v1

    .line 649
    :cond_1
    const/4 v1, 0x0

    iput-object v1, p0, Lcom/miui/server/MiuiFboService;->mFboNativeService:Lmiui/fbo/IFbo;

    .line 650
    sget-object v2, Lcom/miui/server/MiuiFboService;->TAG:Ljava/lang/String;

    const-string v3, "IFbo not found; trying again"

    invoke-static {v2, v3}, Landroid/util/Slog;->w(Ljava/lang/String;Ljava/lang/String;)I

    .line 652
    return-object v1
.end method

.method private static initFboSocket()V
    .locals 3

    .line 387
    new-instance v0, Ljava/lang/Thread;

    new-instance v1, Lcom/miui/server/MiuiFboService$1;

    invoke-direct {v1}, Lcom/miui/server/MiuiFboService$1;-><init>()V

    const-string v2, "fboSocketThread"

    invoke-direct {v0, v1, v2}, Ljava/lang/Thread;-><init>(Ljava/lang/Runnable;Ljava/lang/String;)V

    .line 443
    .local v0, "thread":Ljava/lang/Thread;
    invoke-virtual {v0}, Ljava/lang/Thread;->start()V

    .line 444
    return-void
.end method

.method private isWithinTheTimeInterval()Z
    .locals 10

    .line 731
    invoke-virtual {p0}, Lcom/miui/server/MiuiFboService;->getEnableNightJudgment()Z

    move-result v0

    const/4 v1, 0x1

    if-nez v0, :cond_0

    .line 732
    return v1

    .line 735
    :cond_0
    invoke-static {}, Ljava/util/Calendar;->getInstance()Ljava/util/Calendar;

    move-result-object v0

    .line 736
    .local v0, "calendar":Ljava/util/Calendar;
    const/16 v2, 0xb

    invoke-virtual {v0, v2}, Ljava/util/Calendar;->get(I)I

    move-result v2

    .line 737
    .local v2, "hour":I
    const/16 v3, 0xc

    invoke-virtual {v0, v3}, Ljava/util/Calendar;->get(I)I

    move-result v3

    .line 738
    .local v3, "minute":I
    mul-int/lit8 v4, v2, 0x3c

    add-int/2addr v4, v3

    .line 740
    .local v4, "minuteOfDay":I
    const/4 v5, 0x0

    .line 741
    .local v5, "start":I
    const/16 v6, 0x12c

    .line 743
    .local v6, "end":I
    if-lt v4, v5, :cond_1

    if-gt v4, v6, :cond_1

    .line 744
    sget-object v7, Lcom/miui/server/MiuiFboService;->TAG:Ljava/lang/String;

    new-instance v8, Ljava/lang/StringBuilder;

    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v9, "until five in the morning :"

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    sub-int v9, v6, v4

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    invoke-static {v7, v8}, Landroid/util/Slog;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 745
    return v1

    .line 747
    :cond_1
    const/4 v1, 0x0

    return v1
.end method

.method private static removePendingIntentList()V
    .locals 3

    .line 721
    sget-object v0, Lcom/miui/server/MiuiFboService;->pendingIntentList:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    if-lez v0, :cond_1

    .line 722
    sget-object v0, Lcom/miui/server/MiuiFboService;->pendingIntentList:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    add-int/lit8 v0, v0, -0x1

    .local v0, "i":I
    :goto_0
    if-ltz v0, :cond_0

    .line 723
    sget-object v1, Lcom/miui/server/MiuiFboService;->pendingIntentList:Ljava/util/ArrayList;

    invoke-virtual {v1, v0}, Ljava/util/ArrayList;->remove(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/app/PendingIntent;

    .line 724
    .local v1, "PendingIntent":Landroid/app/PendingIntent;
    sget-object v2, Lcom/miui/server/MiuiFboService;->mAlarmManager:Landroid/app/AlarmManager;

    invoke-virtual {v2, v1}, Landroid/app/AlarmManager;->cancel(Landroid/app/PendingIntent;)V

    .line 722
    .end local v1    # "PendingIntent":Landroid/app/PendingIntent;
    add-int/lit8 v0, v0, -0x1

    goto :goto_0

    .line 726
    .end local v0    # "i":I
    :cond_0
    sget-object v0, Lcom/miui/server/MiuiFboService;->TAG:Ljava/lang/String;

    const-string v1, "removePendingIntentList"

    invoke-static {v0, v1}, Landroid/util/Slog;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 728
    :cond_1
    return-void
.end method

.method private reportFboEvent()V
    .locals 6

    .line 485
    const-string v0, "persist.sys.stability.miui_fbo_start_count"

    :try_start_0
    new-instance v1, Landroid/content/Intent;

    const-string v2, "onetrack.action.TRACK_EVENT"

    invoke-direct {v1, v2}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 486
    .local v1, "intent":Landroid/content/Intent;
    const-string v2, "com.miui.analytics"

    invoke-virtual {v1, v2}, Landroid/content/Intent;->setPackage(Ljava/lang/String;)Landroid/content/Intent;

    .line 487
    const-string v2, "APP_ID"

    const-string v3, "31000000454"

    invoke-virtual {v1, v2, v3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 488
    const-string v2, "PACKAGE"

    const-string v3, "android"

    invoke-virtual {v1, v2, v3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 489
    const-string v2, "EVENT_NAME"

    const-string v3, "fbo_event"

    invoke-virtual {v1, v2, v3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 490
    const/4 v2, 0x0

    invoke-static {v0, v2}, Landroid/os/SystemProperties;->getInt(Ljava/lang/String;I)I

    move-result v2

    const/4 v3, 0x1

    add-int/2addr v2, v3

    .line 491
    .local v2, "startCount":I
    const-string/jumbo v4, "start_count"

    invoke-static {v2}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v1, v4, v5}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 492
    invoke-static {v2}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v4

    invoke-static {v0, v4}, Landroid/os/SystemProperties;->set(Ljava/lang/String;Ljava/lang/String;)V

    .line 493
    sget-boolean v0, Lcom/miui/server/MiuiFboService;->mFinishedApp:Z
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    const-string v4, "fragment_count"

    if-nez v0, :cond_0

    .line 494
    :try_start_1
    const-string v0, "Defragmentation of any app has not been completed"

    invoke-virtual {v1, v4, v0}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    goto :goto_0

    .line 496
    :cond_0
    sget-object v0, Lcom/miui/server/MiuiFboService;->mFragmentCount:Ljava/lang/Long;

    invoke-static {v0}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v1, v4, v0}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 498
    :goto_0
    const-string/jumbo v0, "stop_reason"

    sget-object v4, Lcom/miui/server/MiuiFboService;->mStopReason:Ljava/util/ArrayList;

    invoke-static {v4}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v1, v0, v4}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 499
    invoke-virtual {v1, v3}, Landroid/content/Intent;->setFlags(I)Landroid/content/Intent;

    .line 500
    iget-object v0, p0, Lcom/miui/server/MiuiFboService;->mContext:Landroid/content/Context;

    sget-object v3, Landroid/os/UserHandle;->CURRENT:Landroid/os/UserHandle;

    invoke-virtual {v0, v1, v3}, Landroid/content/Context;->startServiceAsUser(Landroid/content/Intent;Landroid/os/UserHandle;)Landroid/content/ComponentName;

    .line 501
    sget-object v0, Lcom/miui/server/MiuiFboService;->TAG:Ljava/lang/String;

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "fbo event report data startCount:"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, ",fragment_count:"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    sget-object v4, Lcom/miui/server/MiuiFboService;->mFragmentCount:Ljava/lang/Long;

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, ",stopReason:"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    sget-object v4, Lcom/miui/server/MiuiFboService;->mStopReason:Ljava/util/ArrayList;

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v0, v3}, Landroid/util/Slog;->d(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_0
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 506
    nop

    .end local v1    # "intent":Landroid/content/Intent;
    .end local v2    # "startCount":I
    goto :goto_1

    :catchall_0
    move-exception v0

    goto :goto_2

    .line 503
    :catch_0
    move-exception v0

    .line 504
    .local v0, "e":Ljava/lang/Exception;
    :try_start_2
    sget-object v1, Lcom/miui/server/MiuiFboService;->TAG:Ljava/lang/String;

    const-string v2, "Upload onetrack exception!"

    invoke-static {v1, v2, v0}, Landroid/util/Slog;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    .line 506
    nop

    .end local v0    # "e":Ljava/lang/Exception;
    :goto_1
    sget-object v0, Lcom/miui/server/MiuiFboService;->mStopReason:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->clear()V

    .line 507
    nop

    .line 508
    return-void

    .line 506
    :goto_2
    sget-object v1, Lcom/miui/server/MiuiFboService;->mStopReason:Ljava/util/ArrayList;

    invoke-virtual {v1}, Ljava/util/ArrayList;->clear()V

    .line 507
    throw v0
.end method

.method private reportFboProcessedBroadcast()V
    .locals 5

    .line 511
    new-instance v0, Landroid/content/Intent;

    const-string v1, "miui.intent.action.FBO_PROCESSED_DONE"

    invoke-direct {v0, v1}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 512
    .local v0, "intent":Landroid/content/Intent;
    const-string v1, "resultNumber"

    sget-object v2, Lcom/miui/server/MiuiFboService;->mFragmentCount:Ljava/lang/Long;

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/io/Serializable;)Landroid/content/Intent;

    .line 513
    sget-object v1, Lcom/miui/server/MiuiFboService;->packageName:Ljava/util/ArrayList;

    const-string v2, "resultPkg"

    invoke-virtual {v0, v2, v1}, Landroid/content/Intent;->putStringArrayListExtra(Ljava/lang/String;Ljava/util/ArrayList;)Landroid/content/Intent;

    .line 514
    iget-object v1, p0, Lcom/miui/server/MiuiFboService;->mContext:Landroid/content/Context;

    invoke-virtual {v1, v0}, Landroid/content/Context;->sendBroadcast(Landroid/content/Intent;)V

    .line 515
    sget-object v1, Lcom/miui/server/MiuiFboService;->TAG:Ljava/lang/String;

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v4, "sendBroadcast and resultNumber:"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    sget-object v4, Lcom/miui/server/MiuiFboService;->mFragmentCount:Ljava/lang/Long;

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    sget-object v3, Lcom/miui/server/MiuiFboService;->packageName:Ljava/util/ArrayList;

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Slog;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 517
    return-void
.end method

.method private sendStopMessage()V
    .locals 9

    .line 751
    invoke-static {}, Ljava/util/Calendar;->getInstance()Ljava/util/Calendar;

    move-result-object v0

    .line 752
    .local v0, "calendar":Ljava/util/Calendar;
    const/16 v1, 0xb

    invoke-virtual {v0, v1}, Ljava/util/Calendar;->get(I)I

    move-result v1

    .line 753
    .local v1, "hour":I
    const/16 v2, 0xc

    invoke-virtual {v0, v2}, Ljava/util/Calendar;->get(I)I

    move-result v2

    .line 755
    .local v2, "minute":I
    mul-int/lit8 v3, v1, 0x3c

    add-int/2addr v3, v2

    rsub-int v3, v3, 0x12c

    .line 756
    .local v3, "stopTime":I
    mul-int/lit8 v4, v3, 0x3c

    int-to-long v4, v4

    const-wide/16 v6, 0x3e8

    mul-long/2addr v4, v6

    const/4 v6, 0x0

    const-string v7, "miui.intent.action.stop"

    const/4 v8, 0x0

    invoke-static {v7, v8, v4, v5, v6}, Lcom/miui/server/MiuiFboService;->setAlarm(Ljava/lang/String;Ljava/lang/String;JZ)V

    .line 757
    return-void
.end method

.method private sendStopOrContinueToHal(Ljava/lang/String;)V
    .locals 5
    .param p1, "cmd"    # Ljava/lang/String;

    .line 760
    iget-boolean v0, p0, Lcom/miui/server/MiuiFboService;->mDriverSupport:Z

    if-nez v0, :cond_0

    .line 761
    return-void

    .line 763
    :cond_0
    new-instance v0, Landroid/os/HwParcel;

    invoke-direct {v0}, Landroid/os/HwParcel;-><init>()V

    .line 765
    .local v0, "hidl_reply":Landroid/os/HwParcel;
    const/4 v1, 0x3

    :try_start_0
    invoke-static {p1, v1}, Lcom/miui/server/MiuiFboService;->callHalFunction(Ljava/lang/String;I)Ljava/lang/Object;

    .line 766
    sget-object v1, Lcom/miui/server/MiuiFboService;->TAG:Ljava/lang/String;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v3, "sendStopOrContinueToHal:"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Slog;->d(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 769
    goto :goto_0

    .line 767
    :catch_0
    move-exception v1

    .line 768
    .local v1, "e":Ljava/lang/Exception;
    sget-object v2, Lcom/miui/server/MiuiFboService;->TAG:Ljava/lang/String;

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "crash in the sendStopOrContinueToHal:"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Landroid/util/Slog;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 770
    .end local v1    # "e":Ljava/lang/Exception;
    :goto_0
    return-void
.end method

.method private static setAlarm(Ljava/lang/String;Ljava/lang/String;JZ)V
    .locals 6
    .param p0, "action"    # Ljava/lang/String;
    .param p1, "appList"    # Ljava/lang/String;
    .param p2, "delayTime"    # J
    .param p4, "record"    # Z

    .line 614
    new-instance v0, Landroid/content/Intent;

    invoke-direct {v0}, Landroid/content/Intent;-><init>()V

    .line 615
    .local v0, "intent":Landroid/content/Intent;
    invoke-virtual {v0, p0}, Landroid/content/Intent;->setAction(Ljava/lang/String;)Landroid/content/Intent;

    .line 616
    if-eqz p1, :cond_0

    .line 617
    const-string v1, "appList"

    invoke-virtual {v0, v1, p1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 619
    :cond_0
    sget-object v1, Lcom/miui/server/MiuiFboService;->sInstance:Lcom/miui/server/MiuiFboService;

    iget-object v1, v1, Lcom/miui/server/MiuiFboService;->mContext:Landroid/content/Context;

    const/4 v2, 0x0

    const/high16 v3, 0x4000000

    invoke-static {v1, v2, v0, v3}, Landroid/app/PendingIntent;->getBroadcast(Landroid/content/Context;ILandroid/content/Intent;I)Landroid/app/PendingIntent;

    move-result-object v1

    sput-object v1, Lcom/miui/server/MiuiFboService;->mPendingIntent:Landroid/app/PendingIntent;

    .line 620
    if-eqz p4, :cond_1

    .line 621
    sget-object v2, Lcom/miui/server/MiuiFboService;->pendingIntentList:Ljava/util/ArrayList;

    invoke-virtual {v2, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 623
    :cond_1
    sget-object v1, Lcom/miui/server/MiuiFboService;->mAlarmManager:Landroid/app/AlarmManager;

    invoke-static {}, Landroid/os/SystemClock;->elapsedRealtime()J

    move-result-wide v2

    add-long/2addr v2, p2

    sget-object v4, Lcom/miui/server/MiuiFboService;->mPendingIntent:Landroid/app/PendingIntent;

    const/4 v5, 0x2

    invoke-virtual {v1, v5, v2, v3, v4}, Landroid/app/AlarmManager;->setExactAndAllowWhileIdle(IJLandroid/app/PendingIntent;)V

    .line 624
    return-void
.end method

.method private static suspendTransferFboTrigger(Ljava/lang/String;ZZ)V
    .locals 9
    .param p0, "appList"    # Ljava/lang/String;
    .param p1, "cancel"    # Z
    .param p2, "record"    # Z

    .line 707
    invoke-static {}, Ljava/util/Calendar;->getInstance()Ljava/util/Calendar;

    move-result-object v0

    .line 708
    .local v0, "calendar":Ljava/util/Calendar;
    const/16 v1, 0xb

    invoke-virtual {v0, v1}, Ljava/util/Calendar;->get(I)I

    move-result v1

    .line 709
    .local v1, "hour":I
    const/16 v2, 0xc

    invoke-virtual {v0, v2}, Ljava/util/Calendar;->get(I)I

    move-result v2

    .line 710
    .local v2, "minute":I
    mul-int/lit8 v3, v1, 0x3c

    add-int/2addr v3, v2

    .line 711
    .local v3, "minuteOfDay":I
    rsub-int v4, v3, 0x5a0

    .line 712
    .local v4, "startTime":I
    if-eqz p1, :cond_0

    sget-object v5, Lcom/miui/server/MiuiFboService;->mPendingIntent:Landroid/app/PendingIntent;

    if-eqz v5, :cond_0

    .line 713
    sget-object v6, Lcom/miui/server/MiuiFboService;->mAlarmManager:Landroid/app/AlarmManager;

    invoke-virtual {v6, v5}, Landroid/app/AlarmManager;->cancel(Landroid/app/PendingIntent;)V

    .line 715
    :cond_0
    invoke-static {}, Lcom/miui/server/MiuiFboService;->removePendingIntentList()V

    .line 716
    mul-int/lit8 v5, v4, 0x3c

    int-to-long v5, v5

    const-wide/16 v7, 0x3e8

    mul-long/2addr v5, v7

    const-string v7, "miui.intent.action.transferFboTrigger"

    invoke-static {v7, p0, v5, v6, p2}, Lcom/miui/server/MiuiFboService;->setAlarm(Ljava/lang/String;Ljava/lang/String;JZ)V

    .line 717
    sget-object v5, Lcom/miui/server/MiuiFboService;->TAG:Ljava/lang/String;

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v7, "suspendTransferFboTrigger and suspendTime:"

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-static {v5, v6}, Landroid/util/Slog;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 718
    return-void
.end method

.method private static useCldStrategy(I)V
    .locals 4
    .param p0, "val"    # I

    .line 774
    :try_start_0
    sget-object v0, Lcom/miui/server/MiuiFboService;->cldManager:Lmiui/hardware/CldManager;

    invoke-virtual {v0}, Lmiui/hardware/CldManager;->isCldSupported()Z

    move-result v0

    if-eqz v0, :cond_0

    sget-object v0, Lcom/miui/server/MiuiFboService;->sInstance:Lcom/miui/server/MiuiFboService;

    iget-boolean v0, v0, Lcom/miui/server/MiuiFboService;->cldStrategyStatus:Z

    if-eqz v0, :cond_0

    .line 775
    sget-object v0, Lcom/miui/server/MiuiFboService;->cldManager:Lmiui/hardware/CldManager;

    invoke-virtual {v0, p0}, Lmiui/hardware/CldManager;->triggerCld(I)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 779
    :cond_0
    goto :goto_0

    .line 777
    :catch_0
    move-exception v0

    .line 778
    .local v0, "e":Ljava/lang/Exception;
    sget-object v1, Lcom/miui/server/MiuiFboService;->TAG:Ljava/lang/String;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "crash in the useCldStrategy:"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Slog;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 780
    .end local v0    # "e":Ljava/lang/Exception;
    :goto_0
    return-void
.end method

.method private userAreaExtend()V
    .locals 7

    .line 690
    const/4 v0, 0x0

    const/4 v1, 0x6

    :try_start_0
    invoke-static {v0, v1}, Lcom/miui/server/MiuiFboService;->callHalFunction(Ljava/lang/String;I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    .line 691
    .local v0, "euaInfo":Ljava/lang/String;
    if-eqz v0, :cond_1

    .line 692
    const-string v1, ":"

    invoke-virtual {v0, v1}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v1

    .line 693
    .local v1, "strArray":[Ljava/lang/String;
    const/4 v2, 0x0

    aget-object v2, v1, v2

    invoke-static {v2}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v2

    .line 694
    .local v2, "ufsPecycle":I
    const/4 v3, 0x1

    aget-object v4, v1, v3

    invoke-static {v4}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v4

    .line 695
    .local v4, "supportEua":I
    iget-object v5, p0, Lcom/miui/server/MiuiFboService;->mFboNativeService:Lmiui/fbo/IFbo;

    invoke-interface {v5, v2, v4}, Lmiui/fbo/IFbo;->EUA_ReserveSpace(II)Z

    .line 696
    if-ne v4, v3, :cond_0

    .line 697
    iget-object v3, p0, Lcom/miui/server/MiuiFboService;->mFboNativeService:Lmiui/fbo/IFbo;

    invoke-interface {v3}, Lmiui/fbo/IFbo;->EUA_CheckPinFileLba()Ljava/lang/String;

    .line 699
    :cond_0
    sget-object v3, Lcom/miui/server/MiuiFboService;->TAG:Ljava/lang/String;

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v6, "ufsPecycle:"

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, ",supportEua:"

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v3, v5}, Landroid/util/Slog;->d(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 703
    .end local v0    # "euaInfo":Ljava/lang/String;
    .end local v1    # "strArray":[Ljava/lang/String;
    .end local v2    # "ufsPecycle":I
    .end local v4    # "supportEua":I
    :cond_1
    goto :goto_0

    .line 701
    :catch_0
    move-exception v0

    .line 702
    .local v0, "e":Ljava/lang/Exception;
    sget-object v1, Lcom/miui/server/MiuiFboService;->TAG:Ljava/lang/String;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "crash in the userAreaExtend:"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Slog;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 704
    .end local v0    # "e":Ljava/lang/Exception;
    :goto_0
    return-void
.end method

.method private static writeFailToHal()V
    .locals 2

    .line 465
    :try_start_0
    sget-object v0, Lcom/miui/server/MiuiFboService;->outputStream:Ljava/io/OutputStream;

    if-eqz v0, :cond_0

    .line 466
    const-string/jumbo v1, "{fail}"

    invoke-virtual {v1}, Ljava/lang/String;->getBytes()[B

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/io/OutputStream;->write([B)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 470
    :cond_0
    goto :goto_0

    .line 468
    :catch_0
    move-exception v0

    .line 469
    .local v0, "e":Ljava/lang/Exception;
    invoke-virtual {v0}, Ljava/lang/Exception;->printStackTrace()V

    .line 471
    .end local v0    # "e":Ljava/lang/Exception;
    :goto_0
    return-void
.end method

.method private static writeFailToNative()V
    .locals 2

    .line 475
    :try_start_0
    sget-object v0, Lcom/miui/server/MiuiFboService;->outputStream:Ljava/io/OutputStream;

    if-eqz v0, :cond_0

    .line 476
    const-string/jumbo v1, "{broadcast send fail}"

    invoke-virtual {v1}, Ljava/lang/String;->getBytes()[B

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/io/OutputStream;->write([B)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 480
    :cond_0
    goto :goto_0

    .line 478
    :catch_0
    move-exception v0

    .line 479
    .local v0, "e":Ljava/lang/Exception;
    invoke-virtual {v0}, Ljava/lang/Exception;->printStackTrace()V

    .line 481
    .end local v0    # "e":Ljava/lang/Exception;
    :goto_0
    return-void
.end method


# virtual methods
.method public FBO_isSupport()Z
    .locals 5

    .line 522
    const/4 v0, 0x0

    const/4 v1, 0x1

    :try_start_0
    invoke-static {v0, v1}, Lcom/miui/server/MiuiFboService;->callHalFunction(Ljava/lang/String;I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Boolean;

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    iput-boolean v0, p0, Lcom/miui/server/MiuiFboService;->mDriverSupport:Z

    .line 523
    sget-object v0, Lcom/miui/server/MiuiFboService;->TAG:Ljava/lang/String;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "mDriverSupport = "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget-boolean v3, p0, Lcom/miui/server/MiuiFboService;->mDriverSupport:Z

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v0, v2}, Landroid/util/Slog;->d(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 526
    goto :goto_0

    .line 524
    :catch_0
    move-exception v0

    .line 525
    .local v0, "e":Ljava/lang/Exception;
    sget-object v2, Lcom/miui/server/MiuiFboService;->TAG:Ljava/lang/String;

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "crash in the isSupportFbo"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Landroid/util/Slog;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 527
    .end local v0    # "e":Ljava/lang/Exception;
    :goto_0
    return v1
.end method

.method public FBO_new_isSupport(Ljava/lang/String;)Z
    .locals 4
    .param p1, "val"    # Ljava/lang/String;

    .line 533
    const/4 v0, 0x5

    :try_start_0
    invoke-static {p1, v0}, Lcom/miui/server/MiuiFboService;->callHalFunction(Ljava/lang/String;I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Boolean;

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    iput-boolean v0, p0, Lcom/miui/server/MiuiFboService;->mDriverSupport:Z

    .line 534
    sget-object v0, Lcom/miui/server/MiuiFboService;->TAG:Ljava/lang/String;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "mDriverSupport = "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-boolean v2, p0, Lcom/miui/server/MiuiFboService;->mDriverSupport:Z

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Slog;->d(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 537
    goto :goto_0

    .line 535
    :catch_0
    move-exception v0

    .line 536
    .local v0, "e":Ljava/lang/Exception;
    sget-object v1, Lcom/miui/server/MiuiFboService;->TAG:Ljava/lang/String;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "crash in the isSupportFbo"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Slog;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 538
    .end local v0    # "e":Ljava/lang/Exception;
    :goto_0
    const/4 v0, 0x1

    return v0
.end method

.method public FBO_new_trigger(Ljava/lang/String;Z)V
    .locals 1
    .param p1, "appList"    # Ljava/lang/String;
    .param p2, "flag"    # Z

    .line 543
    if-eqz p2, :cond_0

    .line 544
    const/4 v0, 0x0

    invoke-virtual {p0, v0}, Lcom/miui/server/MiuiFboService;->setEnableNightJudgment(Z)V

    goto :goto_0

    .line 546
    :cond_0
    const/4 v0, 0x1

    invoke-virtual {p0, v0}, Lcom/miui/server/MiuiFboService;->setEnableNightJudgment(Z)V

    .line 549
    :goto_0
    sget-object v0, Lcom/miui/server/MiuiFboService;->sInstance:Lcom/miui/server/MiuiFboService;

    invoke-virtual {v0, p1}, Lcom/miui/server/MiuiFboService;->FBO_trigger(Ljava/lang/String;)V

    .line 550
    return-void
.end method

.method public FBO_notifyFragStatus()V
    .locals 4

    .line 598
    :try_start_0
    invoke-direct {p0}, Lcom/miui/server/MiuiFboService;->reportFboProcessedBroadcast()V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 601
    goto :goto_0

    .line 599
    :catch_0
    move-exception v0

    .line 600
    .local v0, "e":Ljava/lang/Exception;
    sget-object v1, Lcom/miui/server/MiuiFboService;->TAG:Ljava/lang/String;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "crash in the notifyFragStatus:"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Slog;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 602
    .end local v0    # "e":Ljava/lang/Exception;
    :goto_0
    return-void
.end method

.method public FBO_trigger(Ljava/lang/String;)V
    .locals 6
    .param p1, "appList"    # Ljava/lang/String;

    .line 554
    invoke-virtual {p0}, Lcom/miui/server/MiuiFboService;->checkPolicy()Z

    move-result v0

    if-nez v0, :cond_0

    .line 555
    sget-object v0, Lcom/miui/server/MiuiFboService;->TAG:Ljava/lang/String;

    const-string v1, "Current policy disables fbo functionality"

    invoke-static {v0, v1}, Landroid/util/Slog;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 556
    return-void

    .line 559
    :cond_0
    invoke-virtual {p0}, Lcom/miui/server/MiuiFboService;->getGlobalSwitch()Z

    move-result v0

    const/4 v1, 0x1

    const/4 v2, 0x0

    if-nez v0, :cond_5

    iget-boolean v0, p0, Lcom/miui/server/MiuiFboService;->messageHasbeenSent:Z

    if-eqz v0, :cond_1

    goto/16 :goto_1

    .line 565
    :cond_1
    invoke-direct {p0}, Lcom/miui/server/MiuiFboService;->isWithinTheTimeInterval()Z

    move-result v0

    if-nez v0, :cond_2

    invoke-virtual {p0}, Lcom/miui/server/MiuiFboService;->getGlobalSwitch()Z

    move-result v0

    if-nez v0, :cond_2

    .line 566
    sget-object v0, Lcom/miui/server/MiuiFboService;->TAG:Ljava/lang/String;

    const-string v3, "execute suspendTransferFboTrigger()"

    invoke-static {v0, v3}, Landroid/util/Slog;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 567
    invoke-static {p1, v1, v2}, Lcom/miui/server/MiuiFboService;->suspendTransferFboTrigger(Ljava/lang/String;ZZ)V

    .line 568
    return-void

    .line 571
    :cond_2
    invoke-virtual {p0}, Lcom/miui/server/MiuiFboService;->getUsbState()Z

    move-result v0

    if-eqz v0, :cond_3

    .line 572
    sget-object v0, Lcom/miui/server/MiuiFboService;->TAG:Ljava/lang/String;

    const-string/jumbo v1, "will not execute fbo service because usbState"

    invoke-static {v0, v1}, Landroid/util/Slog;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 573
    return-void

    .line 576
    :cond_3
    invoke-direct {p0}, Lcom/miui/server/MiuiFboService;->getNativeService()Lmiui/fbo/IFbo;

    move-result-object v0

    iput-object v0, p0, Lcom/miui/server/MiuiFboService;->mFboNativeService:Lmiui/fbo/IFbo;

    .line 578
    :try_start_0
    iget-object v0, p0, Lcom/miui/server/MiuiFboService;->mFboNativeService:Lmiui/fbo/IFbo;

    if-eqz v0, :cond_4

    .line 579
    invoke-direct {p0}, Lcom/miui/server/MiuiFboService;->isWithinTheTimeInterval()Z

    move-result v0

    if-eqz v0, :cond_4

    invoke-virtual {p0}, Lcom/miui/server/MiuiFboService;->getGlobalSwitch()Z

    move-result v0

    if-nez v0, :cond_4

    .line 580
    invoke-static {}, Lcom/miui/server/MiuiFboService;->removePendingIntentList()V

    .line 581
    invoke-static {p1}, Lcom/miui/server/MiuiFboService;->formatAppList(Ljava/lang/String;)V

    .line 582
    sget-object v0, Lcom/miui/server/MiuiFboService;->packageNameList:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    sub-int/2addr v0, v1

    sput v0, Lcom/miui/server/MiuiFboService;->listSize:I

    .line 583
    sget-object v0, Lcom/miui/server/MiuiFboService;->TAG:Ljava/lang/String;

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "appList is:"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    sget-object v4, Lcom/miui/server/MiuiFboService;->packageNameList:Ljava/util/ArrayList;

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v0, v3}, Landroid/util/Slog;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 584
    const-string v0, "miui.intent.action.start"

    const/4 v3, 0x0

    const-wide/32 v4, 0x1b7740

    invoke-static {v0, v3, v4, v5, v2}, Lcom/miui/server/MiuiFboService;->setAlarm(Ljava/lang/String;Ljava/lang/String;JZ)V

    .line 585
    invoke-direct {p0}, Lcom/miui/server/MiuiFboService;->sendStopMessage()V

    .line 586
    iput-boolean v1, p0, Lcom/miui/server/MiuiFboService;->messageHasbeenSent:Z

    .line 587
    iput v2, p0, Lcom/miui/server/MiuiFboService;->screenOnTimes:I
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 592
    :cond_4
    goto :goto_0

    .line 590
    :catch_0
    move-exception v0

    .line 591
    .local v0, "e":Ljava/lang/Exception;
    sget-object v1, Lcom/miui/server/MiuiFboService;->TAG:Ljava/lang/String;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "crash in the FBO_trigger:"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Slog;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 593
    .end local v0    # "e":Ljava/lang/Exception;
    :goto_0
    return-void

    .line 560
    :cond_5
    :goto_1
    sget-object v0, Lcom/miui/server/MiuiFboService;->TAG:Ljava/lang/String;

    const-string v3, "Currently executing fbo or waiting for execution message, so store application list"

    invoke-static {v0, v3}, Landroid/util/Slog;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 561
    invoke-static {p1, v2, v1}, Lcom/miui/server/MiuiFboService;->suspendTransferFboTrigger(Ljava/lang/String;ZZ)V

    .line 562
    return-void
.end method

.method public checkPolicy()Z
    .locals 2

    .line 873
    const-string v0, "persist.sys.stability.miui_fbo_enable"

    const/4 v1, 0x0

    invoke-static {v0, v1}, Landroid/os/SystemProperties;->getBoolean(Ljava/lang/String;Z)Z

    move-result v0

    return v0
.end method

.method public deliverMessage(Ljava/lang/String;IJ)V
    .locals 5
    .param p1, "data"    # Ljava/lang/String;
    .param p2, "number"    # I
    .param p3, "sleepTime"    # J

    .line 789
    sget-object v0, Lcom/miui/server/MiuiFboService;->sInstance:Lcom/miui/server/MiuiFboService;

    iget-object v0, v0, Lcom/miui/server/MiuiFboService;->mFboServiceHandler:Lcom/miui/server/MiuiFboService$FboServiceHandler;

    invoke-virtual {v0}, Lcom/miui/server/MiuiFboService$FboServiceHandler;->obtainMessage()Landroid/os/Message;

    move-result-object v0

    .line 790
    .local v0, "message":Landroid/os/Message;
    iput p2, v0, Landroid/os/Message;->what:I

    .line 791
    iput-object p1, v0, Landroid/os/Message;->obj:Ljava/lang/Object;

    .line 792
    sget-object v1, Lcom/miui/server/MiuiFboService;->sInstance:Lcom/miui/server/MiuiFboService;

    iget-object v1, v1, Lcom/miui/server/MiuiFboService;->mFboServiceHandler:Lcom/miui/server/MiuiFboService$FboServiceHandler;

    invoke-virtual {v1, v0, p3, p4}, Lcom/miui/server/MiuiFboService$FboServiceHandler;->sendMessageDelayed(Landroid/os/Message;J)Z

    .line 793
    sget-object v1, Lcom/miui/server/MiuiFboService;->TAG:Ljava/lang/String;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "msg.what = "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget v3, v0, Landroid/os/Message;->what:I

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string/jumbo v3, "send message time = "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v3

    invoke-virtual {v2, v3, v4}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Slog;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 794
    return-void
.end method

.method public getDueToScreenWait()Z
    .locals 1

    .line 853
    iget-boolean v0, p0, Lcom/miui/server/MiuiFboService;->dueToScreenWait:Z

    return v0
.end method

.method public getEnableNightJudgment()Z
    .locals 1

    .line 861
    iget-boolean v0, p0, Lcom/miui/server/MiuiFboService;->enableNightJudgment:Z

    return v0
.end method

.method public getGlobalSwitch()Z
    .locals 1

    .line 844
    iget-boolean v0, p0, Lcom/miui/server/MiuiFboService;->globalSwitch:Z

    return v0
.end method

.method public getNativeIsRunning()Z
    .locals 1

    .line 835
    iget-boolean v0, p0, Lcom/miui/server/MiuiFboService;->nativeIsRunning:Z

    return v0
.end method

.method public getUsbState()Z
    .locals 1

    .line 869
    iget-boolean v0, p0, Lcom/miui/server/MiuiFboService;->usbState:Z

    return v0
.end method

.method public setBatteryInfos(III)V
    .locals 5
    .param p1, "batteryStatus"    # I
    .param p2, "batteryLevel"    # I
    .param p3, "batteryTemperature"    # I

    .line 798
    iget v0, p0, Lcom/miui/server/MiuiFboService;->batteryStatusMark:I

    if-eq p1, v0, :cond_0

    .line 799
    iput p1, p0, Lcom/miui/server/MiuiFboService;->batteryStatusMark:I

    .line 800
    sget-object v0, Lcom/miui/server/MiuiFboService;->sInstance:Lcom/miui/server/MiuiFboService;

    const/4 v1, 0x6

    const-wide/16 v2, 0x2710

    const-string v4, ""

    invoke-virtual {v0, v4, v1, v2, v3}, Lcom/miui/server/MiuiFboService;->deliverMessage(Ljava/lang/String;IJ)V

    .line 802
    :cond_0
    iget v0, p0, Lcom/miui/server/MiuiFboService;->mCurrentBatteryLevel:I

    sub-int/2addr v0, p2

    const/4 v1, 0x5

    if-le v0, v1, :cond_1

    if-gtz p1, :cond_1

    .line 803
    invoke-virtual {p0}, Lcom/miui/server/MiuiFboService;->getGlobalSwitch()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 804
    const/4 v0, 0x3

    const-wide/16 v1, 0x0

    const-string/jumbo v3, "stop"

    invoke-virtual {p0, v3, v0, v1, v2}, Lcom/miui/server/MiuiFboService;->deliverMessage(Ljava/lang/String;IJ)V

    .line 806
    :cond_1
    invoke-virtual {p0, p1}, Lcom/miui/server/MiuiFboService;->setBatteryStatus(I)V

    .line 807
    invoke-virtual {p0, p2}, Lcom/miui/server/MiuiFboService;->setBatteryLevel(I)V

    .line 808
    invoke-virtual {p0, p3}, Lcom/miui/server/MiuiFboService;->setBatteryTemperature(I)V

    .line 809
    return-void
.end method

.method public setBatteryLevel(I)V
    .locals 0
    .param p1, "batteryLevel"    # I

    .line 822
    iput p1, p0, Lcom/miui/server/MiuiFboService;->batteryLevel:I

    .line 823
    return-void
.end method

.method public setBatteryStatus(I)V
    .locals 0
    .param p1, "batteryStatus"    # I

    .line 818
    iput p1, p0, Lcom/miui/server/MiuiFboService;->batteryStatus:I

    .line 819
    return-void
.end method

.method public setBatteryTemperature(I)V
    .locals 0
    .param p1, "batteryTemperature"    # I

    .line 826
    iput p1, p0, Lcom/miui/server/MiuiFboService;->batteryTemperature:I

    .line 827
    return-void
.end method

.method public setDueToScreenWait(Z)V
    .locals 0
    .param p1, "dueToScreenWait"    # Z

    .line 848
    iput-boolean p1, p0, Lcom/miui/server/MiuiFboService;->dueToScreenWait:Z

    .line 849
    return-void
.end method

.method public setEnableNightJudgment(Z)V
    .locals 0
    .param p1, "enableNightJudgment"    # Z

    .line 857
    iput-boolean p1, p0, Lcom/miui/server/MiuiFboService;->enableNightJudgment:Z

    .line 858
    return-void
.end method

.method public setGlobalSwitch(Z)V
    .locals 0
    .param p1, "globalSwitch"    # Z

    .line 839
    iput-boolean p1, p0, Lcom/miui/server/MiuiFboService;->globalSwitch:Z

    .line 840
    return-void
.end method

.method public setNativeIsRunning(Z)V
    .locals 0
    .param p1, "nativeIsRunning"    # Z

    .line 830
    iput-boolean p1, p0, Lcom/miui/server/MiuiFboService;->nativeIsRunning:Z

    .line 831
    return-void
.end method

.method public setScreenStatus(Z)V
    .locals 1
    .param p1, "screenOn"    # Z

    .line 813
    iget v0, p0, Lcom/miui/server/MiuiFboService;->screenOnTimes:I

    add-int/lit8 v0, v0, 0x1

    iput v0, p0, Lcom/miui/server/MiuiFboService;->screenOnTimes:I

    .line 814
    iput-boolean p1, p0, Lcom/miui/server/MiuiFboService;->screenOn:Z

    .line 815
    return-void
.end method

.method public setUsbState(Z)V
    .locals 0
    .param p1, "usbState"    # Z

    .line 865
    iput-boolean p1, p0, Lcom/miui/server/MiuiFboService;->usbState:Z

    .line 866
    return-void
.end method
