class com.miui.server.MiuiCldService$HALCallback extends miui.hardware.ICldCallback$Stub {
	 /* .source "MiuiCldService.java" */
	 /* # annotations */
	 /* .annotation system Ldalvik/annotation/EnclosingClass; */
	 /* value = Lcom/miui/server/MiuiCldService; */
} // .end annotation
/* .annotation system Ldalvik/annotation/InnerClass; */
/* accessFlags = 0xa */
/* name = "HALCallback" */
} // .end annotation
/* # instance fields */
public com.miui.server.MiuiCldService cldService;
/* # direct methods */
 com.miui.server.MiuiCldService$HALCallback ( ) {
/* .locals 0 */
/* .param p1, "cldService" # Lcom/miui/server/MiuiCldService; */
/* .line 217 */
/* invoke-direct {p0}, Lmiui/hardware/ICldCallback$Stub;-><init>()V */
/* .line 218 */
this.cldService = p1;
/* .line 219 */
return;
} // .end method
/* # virtual methods */
public void notifyStatusChange ( Integer p0 ) {
/* .locals 1 */
/* .param p1, "level" # I */
/* .line 222 */
v0 = this.cldService;
com.miui.server.MiuiCldService .-$$Nest$mreportCldProcessedBroadcast ( v0,p1 );
/* .line 223 */
return;
} // .end method
