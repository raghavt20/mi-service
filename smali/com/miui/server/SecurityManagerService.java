public class com.miui.server.SecurityManagerService extends miui.security.ISecurityManager$Stub {
	 /* .source "SecurityManagerService.java" */
	 /* # annotations */
	 /* .annotation system Ldalvik/annotation/MemberClasses; */
	 /* value = { */
	 /* Lcom/miui/server/SecurityManagerService$LocalService;, */
	 /* Lcom/miui/server/SecurityManagerService$MyPackageObserver;, */
	 /* Lcom/miui/server/SecurityManagerService$Lifecycle; */
	 /* } */
} // .end annotation
/* # static fields */
private static final java.lang.String PACKAGE_PERMISSIONCENTER;
private static final java.lang.String PACKAGE_SECURITYCENTER;
private static final java.lang.String PLATFORM_VAID_PERMISSION;
private static final java.lang.String READ_AND_WRITE_PERMISSION_MANAGER;
private static final java.lang.String TAG;
private static final java.lang.String UPDATE_VERSION;
private static final java.lang.String VAID_PLATFORM_CACHE_PATH;
private static final Integer WRITE_SETTINGS_DELAY;
/* # instance fields */
public final com.miui.server.security.AccessControlImpl mAccessControlImpl;
public final com.miui.server.AccessController mAccessController;
private final com.miui.server.security.AppBehaviorService mAppBehavior;
private final com.miui.server.security.AppDurationService mAppDuration;
private android.app.AppOpsManagerInternal mAppOpsInternal;
private final com.miui.server.AppRunningControlService mAppRunningControlService;
public final android.content.Context mContext;
public final com.miui.server.security.DefaultBrowserImpl mDefaultBrowserImpl;
public final com.miui.server.security.GameBoosterImpl mGameBoosterImpl;
private android.app.INotificationManager mINotificationManager;
private final java.util.ArrayList mIncompatibleAppList;
/* .annotation system Ldalvik/annotation/Signature; */
/* value = { */
/* "Ljava/util/ArrayList<", */
/* "Ljava/lang/String;", */
/* ">;" */
/* } */
} // .end annotation
} // .end field
private final miui.security.SecurityManagerInternal mInternal;
private Boolean mIsUpdated;
private final com.android.server.pm.permission.PermissionManagerService mPermissionManagerService;
private java.lang.String mPlatformVAID;
private final java.util.List mPrivacyDisplayNameList;
/* .annotation system Ldalvik/annotation/Signature; */
/* value = { */
/* "Ljava/util/List<", */
/* "Ljava/lang/String;", */
/* ">;" */
/* } */
} // .end annotation
} // .end field
private final java.util.List mPrivacyVirtualDisplay;
/* .annotation system Ldalvik/annotation/Signature; */
/* value = { */
/* "Ljava/util/List<", */
/* "Ljava/lang/String;", */
/* ">;" */
/* } */
} // .end annotation
} // .end field
private final com.miui.server.SecuritySmsHandler mSecuritySmsHandler;
public final com.miui.server.security.SecurityWriteHandler mSecurityWriteHandler;
public final android.util.AtomicFile mSettingsFile;
public com.miui.server.security.SecuritySettingsObserver mSettingsObserver;
private final com.android.server.pm.UserManagerService mUserManager;
public final java.lang.Object mUserStateLock;
public final android.util.SparseArray mUserStates;
/* .annotation system Ldalvik/annotation/Signature; */
/* value = { */
/* "Landroid/util/SparseArray<", */
/* "Lcom/miui/server/security/SecurityUserState;", */
/* ">;" */
/* } */
} // .end annotation
} // .end field
public final com.miui.server.security.WakeUpTimeImpl mWakeUpTimeImpl;
private java.util.LinkedHashMap restrictChainMaps;
/* .annotation system Ldalvik/annotation/Signature; */
/* value = { */
/* "Ljava/util/LinkedHashMap<", */
/* "Ljava/lang/String;", */
/* "Ljava/util/List<", */
/* "Ljava/lang/String;", */
/* ">;>;" */
/* } */
} // .end annotation
} // .end field
/* # direct methods */
public static void $r8$lambda$5PV2H4L575PgKDbw0e_5GqthUE4 ( com.miui.server.SecurityManagerService p0, android.util.SparseArray p1 ) { //synthethic
/* .locals 0 */
/* invoke-direct {p0, p1}, Lcom/miui/server/SecurityManagerService;->lambda$updateAppWidgetVisibility$2(Landroid/util/SparseArray;)V */
return;
} // .end method
public static void $r8$lambda$JNP6yvVBnt3ZJqYOH3T_E3zOFZ8 ( com.miui.server.SecurityManagerService p0 ) { //synthethic
/* .locals 0 */
/* invoke-direct {p0}, Lcom/miui/server/SecurityManagerService;->loadData()V */
return;
} // .end method
public static java.lang.Boolean $r8$lambda$Vzg6p4kbeB31-KLh3j-mYWN2PuE ( com.miui.server.SecurityManagerService p0, java.lang.String p1, Boolean p2 ) { //synthethic
/* .locals 0 */
/* invoke-direct {p0, p1, p2}, Lcom/miui/server/SecurityManagerService;->lambda$setAppPrivacyStatus$0(Ljava/lang/String;Z)Ljava/lang/Boolean; */
} // .end method
public static java.lang.Boolean $r8$lambda$lbo49f9JMJiPPzJmiMaNELWyWuU ( com.miui.server.SecurityManagerService p0, java.lang.String p1 ) { //synthethic
/* .locals 0 */
/* invoke-direct {p0, p1}, Lcom/miui/server/SecurityManagerService;->lambda$isAppPrivacyEnabled$1(Ljava/lang/String;)Ljava/lang/Boolean; */
} // .end method
static com.miui.server.security.AppBehaviorService -$$Nest$fgetmAppBehavior ( com.miui.server.SecurityManagerService p0 ) { //bridge//synthethic
/* .locals 0 */
p0 = this.mAppBehavior;
} // .end method
static com.miui.server.security.AppDurationService -$$Nest$fgetmAppDuration ( com.miui.server.SecurityManagerService p0 ) { //bridge//synthethic
/* .locals 0 */
p0 = this.mAppDuration;
} // .end method
static com.miui.server.AppRunningControlService -$$Nest$fgetmAppRunningControlService ( com.miui.server.SecurityManagerService p0 ) { //bridge//synthethic
/* .locals 0 */
p0 = this.mAppRunningControlService;
} // .end method
static java.util.List -$$Nest$fgetmPrivacyDisplayNameList ( com.miui.server.SecurityManagerService p0 ) { //bridge//synthethic
/* .locals 0 */
p0 = this.mPrivacyDisplayNameList;
} // .end method
static java.util.List -$$Nest$fgetmPrivacyVirtualDisplay ( com.miui.server.SecurityManagerService p0 ) { //bridge//synthethic
/* .locals 0 */
p0 = this.mPrivacyVirtualDisplay;
} // .end method
static com.android.server.pm.UserManagerService -$$Nest$fgetmUserManager ( com.miui.server.SecurityManagerService p0 ) { //bridge//synthethic
/* .locals 0 */
p0 = this.mUserManager;
} // .end method
static void -$$Nest$minitWhenBootCompleted ( com.miui.server.SecurityManagerService p0 ) { //bridge//synthethic
/* .locals 0 */
/* invoke-direct {p0}, Lcom/miui/server/SecurityManagerService;->initWhenBootCompleted()V */
return;
} // .end method
public com.miui.server.SecurityManagerService ( ) {
/* .locals 4 */
/* .param p1, "context" # Landroid/content/Context; */
/* .line 184 */
/* invoke-direct {p0}, Lmiui/security/ISecurityManager$Stub;-><init>()V */
/* .line 116 */
/* new-instance v0, Landroid/util/AtomicFile; */
/* new-instance v1, Ljava/io/File; */
android.os.Environment .getDataSystemDirectory ( );
final String v3 = "miui-packages.xml"; // const-string v3, "miui-packages.xml"
/* invoke-direct {v1, v2, v3}, Ljava/io/File;-><init>(Ljava/io/File;Ljava/lang/String;)V */
/* invoke-direct {v0, v1}, Landroid/util/AtomicFile;-><init>(Ljava/io/File;)V */
this.mSettingsFile = v0;
/* .line 136 */
/* new-instance v0, Ljava/lang/Object; */
/* invoke-direct {v0}, Ljava/lang/Object;-><init>()V */
this.mUserStateLock = v0;
/* .line 138 */
/* new-instance v0, Landroid/util/SparseArray; */
int v1 = 3; // const/4 v1, 0x3
/* invoke-direct {v0, v1}, Landroid/util/SparseArray;-><init>(I)V */
this.mUserStates = v0;
/* .line 139 */
/* new-instance v0, Ljava/util/ArrayList; */
/* invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V */
this.mIncompatibleAppList = v0;
/* .line 140 */
/* new-instance v0, Ljava/util/ArrayList; */
/* invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V */
this.mPrivacyVirtualDisplay = v0;
/* .line 141 */
/* new-instance v0, Ljava/util/ArrayList; */
/* invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V */
this.mPrivacyDisplayNameList = v0;
/* .line 143 */
/* new-instance v0, Ljava/util/LinkedHashMap; */
/* invoke-direct {v0}, Ljava/util/LinkedHashMap;-><init>()V */
this.restrictChainMaps = v0;
/* .line 185 */
this.mContext = p1;
/* .line 186 */
com.android.server.pm.UserManagerService .getInstance ( );
this.mUserManager = v0;
/* .line 187 */
final String v0 = "permissionmgr"; // const-string v0, "permissionmgr"
android.os.ServiceManager .getService ( v0 );
/* check-cast v0, Lcom/android/server/pm/permission/PermissionManagerService; */
this.mPermissionManagerService = v0;
/* .line 188 */
/* new-instance v0, Landroid/os/HandlerThread; */
final String v1 = "SecurityHandlerThread"; // const-string v1, "SecurityHandlerThread"
/* invoke-direct {v0, v1}, Landroid/os/HandlerThread;-><init>(Ljava/lang/String;)V */
/* .line 189 */
/* .local v0, "handlerThread":Landroid/os/HandlerThread; */
(( android.os.HandlerThread ) v0 ).start ( ); // invoke-virtual {v0}, Landroid/os/HandlerThread;->start()V
/* .line 190 */
/* new-instance v1, Lcom/miui/server/security/SecurityWriteHandler; */
(( android.os.HandlerThread ) v0 ).getLooper ( ); // invoke-virtual {v0}, Landroid/os/HandlerThread;->getLooper()Landroid/os/Looper;
/* invoke-direct {v1, p0, v2}, Lcom/miui/server/security/SecurityWriteHandler;-><init>(Lcom/miui/server/SecurityManagerService;Landroid/os/Looper;)V */
this.mSecurityWriteHandler = v1;
/* .line 191 */
/* new-instance v2, Lcom/miui/server/SecuritySmsHandler; */
/* invoke-direct {v2, p1, v1}, Lcom/miui/server/SecuritySmsHandler;-><init>(Landroid/content/Context;Landroid/os/Handler;)V */
this.mSecuritySmsHandler = v2;
/* .line 192 */
/* new-instance v2, Lcom/miui/server/AccessController; */
(( android.os.HandlerThread ) v0 ).getLooper ( ); // invoke-virtual {v0}, Landroid/os/HandlerThread;->getLooper()Landroid/os/Looper;
/* invoke-direct {v2, p1, v3}, Lcom/miui/server/AccessController;-><init>(Landroid/content/Context;Landroid/os/Looper;)V */
this.mAccessController = v2;
/* .line 193 */
/* new-instance v2, Lcom/miui/server/security/AccessControlImpl; */
/* invoke-direct {v2, p0}, Lcom/miui/server/security/AccessControlImpl;-><init>(Lcom/miui/server/SecurityManagerService;)V */
this.mAccessControlImpl = v2;
/* .line 194 */
/* new-instance v2, Lcom/miui/server/security/WakeUpTimeImpl; */
/* invoke-direct {v2, p0}, Lcom/miui/server/security/WakeUpTimeImpl;-><init>(Lcom/miui/server/SecurityManagerService;)V */
this.mWakeUpTimeImpl = v2;
/* .line 195 */
/* new-instance v2, Lcom/miui/server/security/GameBoosterImpl; */
/* invoke-direct {v2, p0}, Lcom/miui/server/security/GameBoosterImpl;-><init>(Lcom/miui/server/SecurityManagerService;)V */
this.mGameBoosterImpl = v2;
/* .line 196 */
/* new-instance v2, Lcom/miui/server/security/DefaultBrowserImpl; */
/* invoke-direct {v2, p0}, Lcom/miui/server/security/DefaultBrowserImpl;-><init>(Lcom/miui/server/SecurityManagerService;)V */
this.mDefaultBrowserImpl = v2;
/* .line 197 */
/* new-instance v2, Lcom/miui/server/AppRunningControlService; */
/* invoke-direct {v2, p1}, Lcom/miui/server/AppRunningControlService;-><init>(Landroid/content/Context;)V */
this.mAppRunningControlService = v2;
/* .line 198 */
/* new-instance v2, Lcom/miui/server/security/SecuritySettingsObserver; */
/* invoke-direct {v2, p0, v1}, Lcom/miui/server/security/SecuritySettingsObserver;-><init>(Lcom/miui/server/SecurityManagerService;Landroid/os/Handler;)V */
this.mSettingsObserver = v2;
/* .line 199 */
/* new-instance v2, Lcom/miui/server/security/AppBehaviorService; */
/* invoke-direct {v2, p1, v0}, Lcom/miui/server/security/AppBehaviorService;-><init>(Landroid/content/Context;Landroid/os/HandlerThread;)V */
this.mAppBehavior = v2;
/* .line 200 */
/* new-instance v3, Lcom/miui/server/security/AppDurationService; */
/* invoke-direct {v3, v2}, Lcom/miui/server/security/AppDurationService;-><init>(Lcom/miui/server/security/AppBehaviorService;)V */
this.mAppDuration = v3;
/* .line 201 */
(( com.miui.server.security.AppBehaviorService ) v2 ).setAppBehaviorDuration ( v3 ); // invoke-virtual {v2, v3}, Lcom/miui/server/security/AppBehaviorService;->setAppBehaviorDuration(Lcom/miui/server/security/AppDurationService;)V
/* .line 202 */
/* new-instance v2, Lcom/miui/server/SecurityManagerService$LocalService; */
int v3 = 0; // const/4 v3, 0x0
/* invoke-direct {v2, p0, v3}, Lcom/miui/server/SecurityManagerService$LocalService;-><init>(Lcom/miui/server/SecurityManagerService;Lcom/miui/server/SecurityManagerService$LocalService-IA;)V */
this.mInternal = v2;
/* .line 203 */
/* const-class v3, Lmiui/security/SecurityManagerInternal; */
com.android.server.LocalServices .addService ( v3,v2 );
/* .line 204 */
/* new-instance v2, Lcom/miui/server/SecurityManagerService$$ExternalSyntheticLambda1; */
/* invoke-direct {v2, p0}, Lcom/miui/server/SecurityManagerService$$ExternalSyntheticLambda1;-><init>(Lcom/miui/server/SecurityManagerService;)V */
(( com.miui.server.security.SecurityWriteHandler ) v1 ).post ( v2 ); // invoke-virtual {v1, v2}, Lcom/miui/server/security/SecurityWriteHandler;->post(Ljava/lang/Runnable;)Z
/* .line 205 */
return;
} // .end method
private void checkBlurLocationPermission ( ) {
/* .locals 4 */
/* .line 966 */
v0 = android.os.Binder .getCallingUid ( );
/* .line 967 */
/* .local v0, "callingUid":I */
v1 = android.os.UserHandle .getAppId ( v0 );
/* const/16 v2, 0x2710 */
/* if-ge v1, v2, :cond_0 */
/* .line 970 */
return;
/* .line 968 */
} // :cond_0
/* new-instance v1, Ljava/lang/SecurityException; */
/* new-instance v2, Ljava/lang/StringBuilder; */
/* invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V */
final String v3 = "Uid "; // const-string v3, "Uid "
(( java.lang.StringBuilder ) v2 ).append ( v3 ); // invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v2 ).append ( v0 ); // invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;
final String v3 = " can\'t get blur location info"; // const-string v3, " can\'t get blur location info"
(( java.lang.StringBuilder ) v2 ).append ( v3 ); // invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v2 ).toString ( ); // invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
/* invoke-direct {v1, v2}, Ljava/lang/SecurityException;-><init>(Ljava/lang/String;)V */
/* throw v1 */
} // .end method
private void checkGrantPermissionPkg ( java.lang.String p0 ) {
/* .locals 4 */
/* .param p1, "allowPackage" # Ljava/lang/String; */
/* .line 829 */
v0 = android.os.Binder .getCallingPid ( );
com.android.server.am.ProcessUtils .getPackageNameByPid ( v0 );
/* .line 830 */
/* .local v0, "callingPackageName":Ljava/lang/String; */
v1 = (( java.lang.String ) p1 ).equals ( v0 ); // invoke-virtual {p1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z
if ( v1 != null) { // if-eqz v1, :cond_0
/* .line 834 */
return;
/* .line 831 */
} // :cond_0
/* new-instance v1, Ljava/lang/SecurityException; */
/* new-instance v2, Ljava/lang/StringBuilder; */
/* invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V */
final String v3 = "Permission Denial: attempt to grant/revoke permission from pid="; // const-string v3, "Permission Denial: attempt to grant/revoke permission from pid="
(( java.lang.StringBuilder ) v2 ).append ( v3 ); // invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
/* .line 832 */
v3 = android.os.Binder .getCallingPid ( );
(( java.lang.StringBuilder ) v2 ).append ( v3 ); // invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;
final String v3 = ", uid="; // const-string v3, ", uid="
(( java.lang.StringBuilder ) v2 ).append ( v3 ); // invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
v3 = android.os.Binder .getCallingUid ( );
(( java.lang.StringBuilder ) v2 ).append ( v3 ); // invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;
final String v3 = ", pkg="; // const-string v3, ", pkg="
(( java.lang.StringBuilder ) v2 ).append ( v3 ); // invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v2 ).append ( v0 ); // invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v2 ).toString ( ); // invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
/* invoke-direct {v1, v2}, Ljava/lang/SecurityException;-><init>(Ljava/lang/String;)V */
/* throw v1 */
} // .end method
private void checkPermission ( ) {
/* .locals 6 */
/* .line 837 */
v0 = android.os.Binder .getCallingUid ( );
/* .line 838 */
/* .local v0, "callingUid":I */
v1 = android.os.UserHandle .getAppId ( v0 );
/* const/16 v2, 0x7d0 */
/* if-eq v1, v2, :cond_2 */
/* .line 841 */
v1 = this.mContext;
final String v2 = "android.permission.CHANGE_COMPONENT_ENABLED_STATE"; // const-string v2, "android.permission.CHANGE_COMPONENT_ENABLED_STATE"
v1 = (( android.content.Context ) v1 ).checkCallingOrSelfPermission ( v2 ); // invoke-virtual {v1, v2}, Landroid/content/Context;->checkCallingOrSelfPermission(Ljava/lang/String;)I
/* .line 843 */
/* .local v1, "permission":I */
/* if-nez v1, :cond_0 */
return;
/* .line 844 */
} // :cond_0
v2 = this.mContext;
final String v3 = "miui.permission.READ_AND_WIRTE_PERMISSION_MANAGER"; // const-string v3, "miui.permission.READ_AND_WIRTE_PERMISSION_MANAGER"
v2 = (( android.content.Context ) v2 ).checkCallingOrSelfPermission ( v3 ); // invoke-virtual {v2, v3}, Landroid/content/Context;->checkCallingOrSelfPermission(Ljava/lang/String;)I
/* .line 845 */
/* .local v2, "managePermission":I */
/* if-nez v2, :cond_1 */
return;
/* .line 846 */
} // :cond_1
/* new-instance v3, Ljava/lang/SecurityException; */
/* new-instance v4, Ljava/lang/StringBuilder; */
/* invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V */
final String v5 = "Permission Denial: attempt to change application state from pid="; // const-string v5, "Permission Denial: attempt to change application state from pid="
(( java.lang.StringBuilder ) v4 ).append ( v5 ); // invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
/* .line 847 */
v5 = android.os.Binder .getCallingPid ( );
(( java.lang.StringBuilder ) v4 ).append ( v5 ); // invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;
final String v5 = ", uid="; // const-string v5, ", uid="
(( java.lang.StringBuilder ) v4 ).append ( v5 ); // invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v4 ).append ( v0 ); // invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v4 ).toString ( ); // invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
/* invoke-direct {v3, v4}, Ljava/lang/SecurityException;-><init>(Ljava/lang/String;)V */
/* throw v3 */
/* .line 839 */
} // .end local v1 # "permission":I
} // .end local v2 # "managePermission":I
} // :cond_2
/* new-instance v1, Ljava/lang/SecurityException; */
/* new-instance v2, Ljava/lang/StringBuilder; */
/* invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V */
final String v3 = "no permission for UID:"; // const-string v3, "no permission for UID:"
(( java.lang.StringBuilder ) v2 ).append ( v3 ); // invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v2 ).append ( v0 ); // invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v2 ).toString ( ); // invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
/* invoke-direct {v1, v2}, Ljava/lang/SecurityException;-><init>(Ljava/lang/String;)V */
/* throw v1 */
} // .end method
private void checkPermissionByUid ( Integer...p0 ) {
/* .locals 5 */
/* .param p1, "uids" # [I */
/* .line 816 */
v0 = android.os.Binder .getCallingUid ( );
/* .line 817 */
/* .local v0, "callingUid":I */
int v1 = 0; // const/4 v1, 0x0
/* .line 818 */
/* .local v1, "hasPermission":Z */
int v2 = 0; // const/4 v2, 0x0
/* .local v2, "i":I */
} // :goto_0
/* array-length v3, p1 */
/* if-ge v2, v3, :cond_1 */
/* .line 819 */
v3 = android.os.UserHandle .getAppId ( v0 );
/* aget v4, p1, v2 */
/* if-ne v3, v4, :cond_0 */
/* .line 820 */
int v1 = 1; // const/4 v1, 0x1
/* .line 818 */
} // :cond_0
/* add-int/lit8 v2, v2, 0x1 */
/* .line 823 */
} // .end local v2 # "i":I
} // :cond_1
if ( v1 != null) { // if-eqz v1, :cond_2
/* .line 826 */
return;
/* .line 824 */
} // :cond_2
/* new-instance v2, Ljava/lang/SecurityException; */
/* new-instance v3, Ljava/lang/StringBuilder; */
/* invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V */
final String v4 = "no permission to read file for UID:"; // const-string v4, "no permission to read file for UID:"
(( java.lang.StringBuilder ) v3 ).append ( v4 ); // invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v3 ).append ( v0 ); // invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v3 ).toString ( ); // invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
/* invoke-direct {v2, v3}, Ljava/lang/SecurityException;-><init>(Ljava/lang/String;)V */
/* throw v2 */
} // .end method
private void checkWakePathPermission ( ) {
/* .locals 5 */
/* .line 607 */
v0 = this.mContext;
/* .line 608 */
v1 = android.os.Binder .getCallingPid ( );
v2 = android.os.Binder .getCallingUid ( );
/* .line 607 */
final String v3 = "android.permission.UPDATE_APP_OPS_STATS"; // const-string v3, "android.permission.UPDATE_APP_OPS_STATS"
int v4 = 0; // const/4 v4, 0x0
(( android.content.Context ) v0 ).enforcePermission ( v3, v1, v2, v4 ); // invoke-virtual {v0, v3, v1, v2, v4}, Landroid/content/Context;->enforcePermission(Ljava/lang/String;IILjava/lang/String;)V
/* .line 609 */
return;
} // .end method
private void checkWriteSecurePermission ( ) {
/* .locals 3 */
/* .line 729 */
v0 = this.mContext;
/* new-instance v1, Ljava/lang/StringBuilder; */
/* invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V */
final String v2 = "Permission Denial: attempt to change application privacy revoke state from pid="; // const-string v2, "Permission Denial: attempt to change application privacy revoke state from pid="
(( java.lang.StringBuilder ) v1 ).append ( v2 ); // invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
/* .line 731 */
v2 = android.os.Binder .getCallingPid ( );
(( java.lang.StringBuilder ) v1 ).append ( v2 ); // invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;
final String v2 = ", uid="; // const-string v2, ", uid="
(( java.lang.StringBuilder ) v1 ).append ( v2 ); // invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
/* .line 732 */
v2 = android.os.Binder .getCallingUid ( );
(( java.lang.StringBuilder ) v1 ).append ( v2 ); // invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v1 ).toString ( ); // invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
/* .line 729 */
final String v2 = "android.permission.WRITE_SECURE_SETTINGS"; // const-string v2, "android.permission.WRITE_SECURE_SETTINGS"
(( android.content.Context ) v0 ).enforceCallingPermission ( v2, v1 ); // invoke-virtual {v0, v2, v1}, Landroid/content/Context;->enforceCallingPermission(Ljava/lang/String;Ljava/lang/String;)V
/* .line 733 */
return;
} // .end method
private void dumpPrivacyVirtualDisplay ( java.io.PrintWriter p0 ) {
/* .locals 4 */
/* .param p1, "out" # Ljava/io/PrintWriter; */
/* .line 1003 */
final String v0 = "===================================SCREEN SHARE PROTECTION DUMP BEGIN========================================"; // const-string v0, "===================================SCREEN SHARE PROTECTION DUMP BEGIN========================================"
(( java.io.PrintWriter ) p1 ).println ( v0 ); // invoke-virtual {p1, v0}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V
/* .line 1005 */
v0 = v0 = this.mPrivacyVirtualDisplay;
/* if-nez v0, :cond_0 */
/* .line 1006 */
final String v0 = "Don\'t have any privacy virtual display package!"; // const-string v0, "Don\'t have any privacy virtual display package!"
(( java.io.PrintWriter ) p1 ).println ( v0 ); // invoke-virtual {p1, v0}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V
/* .line 1008 */
} // :cond_0
v0 = this.mPrivacyVirtualDisplay;
v1 = } // :goto_0
if ( v1 != null) { // if-eqz v1, :cond_1
/* check-cast v1, Ljava/lang/String; */
/* .line 1009 */
/* .local v1, "packageName":Ljava/lang/String; */
/* new-instance v2, Ljava/lang/StringBuilder; */
/* invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V */
final String v3 = "packageName: "; // const-string v3, "packageName: "
(( java.lang.StringBuilder ) v2 ).append ( v3 ); // invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v2 ).append ( v1 ); // invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v2 ).toString ( ); // invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
(( java.io.PrintWriter ) p1 ).println ( v2 ); // invoke-virtual {p1, v2}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V
/* .line 1010 */
} // .end local v1 # "packageName":Ljava/lang/String;
/* .line 1012 */
} // :cond_1
} // :goto_1
v0 = v0 = this.mPrivacyDisplayNameList;
/* if-nez v0, :cond_2 */
/* .line 1013 */
final String v0 = "Don\'t have any privacy virtual display name!"; // const-string v0, "Don\'t have any privacy virtual display name!"
(( java.io.PrintWriter ) p1 ).println ( v0 ); // invoke-virtual {p1, v0}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V
/* .line 1015 */
} // :cond_2
v0 = this.mPrivacyDisplayNameList;
v1 = } // :goto_2
if ( v1 != null) { // if-eqz v1, :cond_3
/* check-cast v1, Ljava/lang/String; */
/* .line 1016 */
/* .local v1, "name":Ljava/lang/String; */
/* new-instance v2, Ljava/lang/StringBuilder; */
/* invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V */
/* const-string/jumbo v3, "virtual display: " */
(( java.lang.StringBuilder ) v2 ).append ( v3 ); // invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v2 ).append ( v1 ); // invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v2 ).toString ( ); // invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
(( java.io.PrintWriter ) p1 ).println ( v2 ); // invoke-virtual {p1, v2}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V
/* .line 1017 */
} // .end local v1 # "name":Ljava/lang/String;
/* .line 1019 */
} // :cond_3
} // :goto_3
final String v0 = "====================================SCREEN SHARE PROTECTION DUMP END========================================"; // const-string v0, "====================================SCREEN SHARE PROTECTION DUMP END========================================"
(( java.io.PrintWriter ) p1 ).println ( v0 ); // invoke-virtual {p1, v0}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V
/* .line 1021 */
return;
} // .end method
private void initWhenBootCompleted ( ) {
/* .locals 4 */
/* .line 215 */
v0 = this.mWakeUpTimeImpl;
(( com.miui.server.security.WakeUpTimeImpl ) v0 ).readWakeUpTime ( ); // invoke-virtual {v0}, Lcom/miui/server/security/WakeUpTimeImpl;->readWakeUpTime()V
/* .line 216 */
v0 = this.mUserStateLock;
/* monitor-enter v0 */
/* .line 217 */
int v1 = 0; // const/4 v1, 0x0
try { // :try_start_0
(( com.miui.server.SecurityManagerService ) p0 ).getUserStateLocked ( v1 ); // invoke-virtual {p0, v1}, Lcom/miui/server/SecurityManagerService;->getUserStateLocked(I)Lcom/miui/server/security/SecurityUserState;
/* .line 218 */
/* .local v1, "userState":Lcom/miui/server/security/SecurityUserState; */
v2 = this.mSettingsObserver;
(( com.miui.server.security.SecuritySettingsObserver ) v2 ).initAccessControlSettingsLocked ( v1 ); // invoke-virtual {v2, v1}, Lcom/miui/server/security/SecuritySettingsObserver;->initAccessControlSettingsLocked(Lcom/miui/server/security/SecurityUserState;)V
/* .line 219 */
} // .end local v1 # "userState":Lcom/miui/server/security/SecurityUserState;
/* monitor-exit v0 */
/* :try_end_0 */
/* .catchall {:try_start_0 ..:try_end_0} :catchall_0 */
/* .line 220 */
v0 = this.mSettingsObserver;
(( com.miui.server.security.SecuritySettingsObserver ) v0 ).registerForSettingsChanged ( ); // invoke-virtual {v0}, Lcom/miui/server/security/SecuritySettingsObserver;->registerForSettingsChanged()V
/* .line 221 */
miui.security.WakePathChecker .getInstance ( );
v1 = this.mContext;
(( miui.security.WakePathChecker ) v0 ).init ( v1 ); // invoke-virtual {v0, v1}, Lmiui/security/WakePathChecker;->init(Landroid/content/Context;)V
/* .line 223 */
miui.app.StorageRestrictedPathManager .getInstance ( );
v1 = this.mContext;
(( miui.app.StorageRestrictedPathManager ) v0 ).init ( v1 ); // invoke-virtual {v0, v1}, Lmiui/app/StorageRestrictedPathManager;->init(Landroid/content/Context;)V
/* .line 225 */
v0 = this.mContext;
com.miui.server.RestrictAppNetManager .init ( v0 );
/* .line 227 */
v0 = this.mAccessController;
v1 = android.os.UserHandle .myUserId ( );
(( com.miui.server.AccessController ) v0 ).updatePasswordTypeForPattern ( v1 ); // invoke-virtual {v0, v1}, Lcom/miui/server/AccessController;->updatePasswordTypeForPattern(I)V
/* .line 228 */
/* sget-boolean v0, Lmiui/os/Build;->IS_INTERNATIONAL_BUILD:Z */
/* if-nez v0, :cond_0 */
/* .line 230 */
v0 = this.mDefaultBrowserImpl;
(( com.miui.server.security.DefaultBrowserImpl ) v0 ).resetDefaultBrowser ( ); // invoke-virtual {v0}, Lcom/miui/server/security/DefaultBrowserImpl;->resetDefaultBrowser()V
/* .line 231 */
/* const-class v0, Landroid/content/pm/PackageManagerInternal; */
com.android.server.LocalServices .getService ( v0 );
/* check-cast v0, Landroid/content/pm/PackageManagerInternal; */
/* .line 232 */
/* .local v0, "pmi":Landroid/content/pm/PackageManagerInternal; */
/* new-instance v1, Lcom/miui/server/SecurityManagerService$MyPackageObserver; */
v2 = this.mContext;
v3 = this.mDefaultBrowserImpl;
/* invoke-direct {v1, v2, v3}, Lcom/miui/server/SecurityManagerService$MyPackageObserver;-><init>(Landroid/content/Context;Lcom/miui/server/security/DefaultBrowserImpl;)V */
(( android.content.pm.PackageManagerInternal ) v0 ).getPackageList ( v1 ); // invoke-virtual {v0, v1}, Landroid/content/pm/PackageManagerInternal;->getPackageList(Landroid/content/pm/PackageManagerInternal$PackageListObserver;)Lcom/android/server/pm/PackageList;
/* .line 234 */
} // .end local v0 # "pmi":Landroid/content/pm/PackageManagerInternal;
} // :cond_0
return;
/* .line 219 */
/* :catchall_0 */
/* move-exception v1 */
try { // :try_start_1
/* monitor-exit v0 */
/* :try_end_1 */
/* .catchall {:try_start_1 ..:try_end_1} :catchall_0 */
/* throw v1 */
} // .end method
private java.lang.Boolean lambda$isAppPrivacyEnabled$1 ( java.lang.String p0 ) { //synthethic
/* .locals 3 */
/* .param p1, "packageName" # Ljava/lang/String; */
/* .annotation system Ldalvik/annotation/Throws; */
/* value = { */
/* Ljava/lang/Exception; */
/* } */
} // .end annotation
/* .line 756 */
v0 = this.mContext;
/* .line 757 */
(( android.content.Context ) v0 ).getContentResolver ( ); // invoke-virtual {v0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;
/* new-instance v1, Ljava/lang/StringBuilder; */
/* invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V */
final String v2 = "privacy_status_"; // const-string v2, "privacy_status_"
(( java.lang.StringBuilder ) v1 ).append ( v2 ); // invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v1 ).append ( p1 ); // invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v1 ).toString ( ); // invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
int v2 = 1; // const/4 v2, 0x1
v0 = android.provider.Settings$Secure .getInt ( v0,v1,v2 );
if ( v0 != null) { // if-eqz v0, :cond_0
} // :cond_0
int v2 = 0; // const/4 v2, 0x0
/* .line 756 */
} // :goto_0
java.lang.Boolean .valueOf ( v2 );
} // .end method
private java.lang.Boolean lambda$setAppPrivacyStatus$0 ( java.lang.String p0, Boolean p1 ) { //synthethic
/* .locals 3 */
/* .param p1, "packageName" # Ljava/lang/String; */
/* .param p2, "isOpen" # Z */
/* .annotation system Ldalvik/annotation/Throws; */
/* value = { */
/* Ljava/lang/Exception; */
/* } */
} // .end annotation
/* .line 747 */
v0 = this.mContext;
(( android.content.Context ) v0 ).getContentResolver ( ); // invoke-virtual {v0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;
/* new-instance v1, Ljava/lang/StringBuilder; */
/* invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V */
final String v2 = "privacy_status_"; // const-string v2, "privacy_status_"
(( java.lang.StringBuilder ) v1 ).append ( v2 ); // invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v1 ).append ( p1 ); // invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v1 ).toString ( ); // invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
/* .line 748 */
/* nop */
/* .line 747 */
v0 = android.provider.Settings$Secure .putInt ( v0,v1,p2 );
java.lang.Boolean .valueOf ( v0 );
} // .end method
private void lambda$updateAppWidgetVisibility$2 ( android.util.SparseArray p0 ) { //synthethic
/* .locals 2 */
/* .param p1, "uidPackageNames" # Landroid/util/SparseArray; */
/* .line 1424 */
v0 = this.mAppOpsInternal;
int v1 = 0; // const/4 v1, 0x0
(( android.app.AppOpsManagerInternal ) v0 ).updateAppWidgetVisibility ( p1, v1 ); // invoke-virtual {v0, p1, v1}, Landroid/app/AppOpsManagerInternal;->updateAppWidgetVisibility(Landroid/util/SparseArray;Z)V
return;
} // .end method
private void loadData ( ) {
/* .locals 0 */
/* .line 208 */
/* invoke-direct {p0}, Lcom/miui/server/SecurityManagerService;->readSettings()V */
/* .line 210 */
/* invoke-direct {p0}, Lcom/miui/server/SecurityManagerService;->updateXSpaceSettings()V */
/* .line 211 */
return;
} // .end method
private void readPackagesSettings ( java.io.FileInputStream p0 ) {
/* .locals 14 */
/* .param p1, "fis" # Ljava/io/FileInputStream; */
/* .annotation system Ldalvik/annotation/Throws; */
/* value = { */
/* Ljava/lang/Exception; */
/* } */
} // .end annotation
/* .line 1172 */
android.util.Xml .newPullParser ( );
/* .line 1173 */
/* .local v0, "parser":Lorg/xmlpull/v1/XmlPullParser; */
int v1 = 0; // const/4 v1, 0x0
v2 = /* .line 1175 */
/* .line 1176 */
/* .local v2, "eventType":I */
} // :goto_0
int v3 = 1; // const/4 v3, 0x1
int v4 = 2; // const/4 v4, 0x2
/* if-eq v2, v4, :cond_0 */
/* if-eq v2, v3, :cond_0 */
v2 = /* .line 1177 */
/* .line 1180 */
} // :cond_0
/* .line 1181 */
/* .local v5, "tagName":Ljava/lang/String; */
final String v6 = "packages"; // const-string v6, "packages"
v6 = (( java.lang.String ) v6 ).equals ( v5 ); // invoke-virtual {v6, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z
if ( v6 != null) { // if-eqz v6, :cond_6
/* .line 1182 */
/* const-string/jumbo v6, "updateVersion" */
/* .line 1183 */
/* .local v6, "updateVersion":Ljava/lang/String; */
v7 = android.text.TextUtils .isEmpty ( v6 );
/* if-nez v7, :cond_1 */
final String v7 = "1.0"; // const-string v7, "1.0"
v7 = (( java.lang.String ) v7 ).equals ( v6 ); // invoke-virtual {v7, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z
if ( v7 != null) { // if-eqz v7, :cond_1
/* .line 1184 */
/* iput-boolean v3, p0, Lcom/miui/server/SecurityManagerService;->mIsUpdated:Z */
/* .line 1186 */
v2 = } // :cond_1
/* .line 1188 */
} // :cond_2
v7 = /* if-ne v2, v4, :cond_5 */
/* if-ne v7, v4, :cond_5 */
/* .line 1189 */
/* .line 1190 */
final String v7 = "package"; // const-string v7, "package"
v7 = (( java.lang.String ) v7 ).equals ( v5 ); // invoke-virtual {v7, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z
if ( v7 != null) { // if-eqz v7, :cond_5
/* .line 1191 */
final String v7 = "name"; // const-string v7, "name"
/* .line 1192 */
/* .local v7, "name":Ljava/lang/String; */
v8 = android.text.TextUtils .isEmpty ( v7 );
if ( v8 != null) { // if-eqz v8, :cond_3
/* .line 1193 */
final String v8 = "SecurityManagerService"; // const-string v8, "SecurityManagerService"
final String v9 = "read current package name is empty, skip"; // const-string v9, "read current package name is empty, skip"
android.util.Slog .i ( v8,v9 );
/* .line 1194 */
/* goto/16 :goto_2 */
/* .line 1196 */
} // :cond_3
/* new-instance v8, Lcom/miui/server/security/SecurityPackageSettings; */
/* invoke-direct {v8, v7}, Lcom/miui/server/security/SecurityPackageSettings;-><init>(Ljava/lang/String;)V */
/* .line 1197 */
/* .local v8, "ps":Lcom/miui/server/security/SecurityPackageSettings; */
int v9 = 0; // const/4 v9, 0x0
/* .line 1198 */
/* .local v9, "userHandle":I */
/* const-string/jumbo v10, "u" */
/* .line 1199 */
/* .local v10, "userHandleStr":Ljava/lang/String; */
v11 = android.text.TextUtils .isEmpty ( v10 );
/* if-nez v11, :cond_4 */
/* .line 1200 */
v9 = java.lang.Integer .parseInt ( v10 );
/* .line 1202 */
} // :cond_4
final String v11 = "accessControl"; // const-string v11, "accessControl"
v11 = java.lang.Boolean .parseBoolean ( v11 );
/* iput-boolean v11, v8, Lcom/miui/server/security/SecurityPackageSettings;->accessControl:Z */
/* .line 1203 */
final String v11 = "childrenControl"; // const-string v11, "childrenControl"
v11 = java.lang.Boolean .parseBoolean ( v11 );
/* iput-boolean v11, v8, Lcom/miui/server/security/SecurityPackageSettings;->childrenControl:Z */
/* .line 1204 */
final String v11 = "maskNotification"; // const-string v11, "maskNotification"
v11 = java.lang.Boolean .parseBoolean ( v11 );
/* iput-boolean v11, v8, Lcom/miui/server/security/SecurityPackageSettings;->maskNotification:Z */
/* .line 1205 */
final String v11 = "isPrivacyApp"; // const-string v11, "isPrivacyApp"
v11 = java.lang.Boolean .parseBoolean ( v11 );
/* iput-boolean v11, v8, Lcom/miui/server/security/SecurityPackageSettings;->isPrivacyApp:Z */
/* .line 1207 */
final String v11 = "isDarkModeChecked"; // const-string v11, "isDarkModeChecked"
v11 = java.lang.Boolean .parseBoolean ( v11 );
/* iput-boolean v11, v8, Lcom/miui/server/security/SecurityPackageSettings;->isDarkModeChecked:Z */
/* .line 1209 */
final String v11 = "isGameStorageApp"; // const-string v11, "isGameStorageApp"
v11 = java.lang.Boolean .parseBoolean ( v11 );
/* iput-boolean v11, v8, Lcom/miui/server/security/SecurityPackageSettings;->isGameStorageApp:Z */
/* .line 1210 */
final String v11 = "isRemindForRelaunch"; // const-string v11, "isRemindForRelaunch"
v11 = java.lang.Boolean .parseBoolean ( v11 );
/* iput-boolean v11, v8, Lcom/miui/server/security/SecurityPackageSettings;->isRemindForRelaunch:Z */
/* .line 1211 */
final String v11 = "isRelaunchWhenFolded"; // const-string v11, "isRelaunchWhenFolded"
v11 = java.lang.Boolean .parseBoolean ( v11 );
/* iput-boolean v11, v8, Lcom/miui/server/security/SecurityPackageSettings;->isRelaunchWhenFolded:Z */
/* .line 1212 */
final String v11 = "isScRelaunchConfirm"; // const-string v11, "isScRelaunchConfirm"
v11 = java.lang.Boolean .parseBoolean ( v11 );
/* iput-boolean v11, v8, Lcom/miui/server/security/SecurityPackageSettings;->isScRelaunchConfirm:Z */
/* .line 1213 */
v11 = this.mUserStateLock;
/* monitor-enter v11 */
/* .line 1214 */
try { // :try_start_0
(( com.miui.server.SecurityManagerService ) p0 ).getUserStateLocked ( v9 ); // invoke-virtual {p0, v9}, Lcom/miui/server/SecurityManagerService;->getUserStateLocked(I)Lcom/miui/server/security/SecurityUserState;
/* .line 1215 */
/* .local v12, "userState":Lcom/miui/server/security/SecurityUserState; */
v13 = this.mPackages;
(( java.util.HashMap ) v13 ).put ( v7, v8 ); // invoke-virtual {v13, v7, v8}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;
/* .line 1216 */
/* nop */
} // .end local v12 # "userState":Lcom/miui/server/security/SecurityUserState;
/* monitor-exit v11 */
/* :catchall_0 */
/* move-exception v1 */
/* monitor-exit v11 */
/* :try_end_0 */
/* .catchall {:try_start_0 ..:try_end_0} :catchall_0 */
/* throw v1 */
/* .line 1219 */
} // .end local v7 # "name":Ljava/lang/String;
} // .end local v8 # "ps":Lcom/miui/server/security/SecurityPackageSettings;
} // .end local v9 # "userHandle":I
} // .end local v10 # "userHandleStr":Ljava/lang/String;
} // :cond_5
v2 = } // :goto_1
/* .line 1220 */
} // :goto_2
/* if-ne v2, v3, :cond_2 */
/* .line 1222 */
} // .end local v6 # "updateVersion":Ljava/lang/String;
} // :cond_6
return;
} // .end method
private void readSettings ( ) {
/* .locals 3 */
/* .line 1161 */
v0 = this.mSettingsFile;
(( android.util.AtomicFile ) v0 ).getBaseFile ( ); // invoke-virtual {v0}, Landroid/util/AtomicFile;->getBaseFile()Ljava/io/File;
v0 = (( java.io.File ) v0 ).exists ( ); // invoke-virtual {v0}, Ljava/io/File;->exists()Z
/* if-nez v0, :cond_0 */
/* .line 1162 */
return;
/* .line 1164 */
} // :cond_0
try { // :try_start_0
v0 = this.mSettingsFile;
(( android.util.AtomicFile ) v0 ).openRead ( ); // invoke-virtual {v0}, Landroid/util/AtomicFile;->openRead()Ljava/io/FileInputStream;
/* :try_end_0 */
/* .catch Ljava/lang/Exception; {:try_start_0 ..:try_end_0} :catch_0 */
/* .line 1165 */
/* .local v0, "fis":Ljava/io/FileInputStream; */
try { // :try_start_1
/* invoke-direct {p0, v0}, Lcom/miui/server/SecurityManagerService;->readPackagesSettings(Ljava/io/FileInputStream;)V */
/* :try_end_1 */
/* .catchall {:try_start_1 ..:try_end_1} :catchall_0 */
/* .line 1166 */
if ( v0 != null) { // if-eqz v0, :cond_1
try { // :try_start_2
(( java.io.FileInputStream ) v0 ).close ( ); // invoke-virtual {v0}, Ljava/io/FileInputStream;->close()V
/* :try_end_2 */
/* .catch Ljava/lang/Exception; {:try_start_2 ..:try_end_2} :catch_0 */
/* .line 1168 */
} // .end local v0 # "fis":Ljava/io/FileInputStream;
} // :cond_1
/* .line 1164 */
/* .restart local v0 # "fis":Ljava/io/FileInputStream; */
/* :catchall_0 */
/* move-exception v1 */
if ( v0 != null) { // if-eqz v0, :cond_2
try { // :try_start_3
(( java.io.FileInputStream ) v0 ).close ( ); // invoke-virtual {v0}, Ljava/io/FileInputStream;->close()V
/* :try_end_3 */
/* .catchall {:try_start_3 ..:try_end_3} :catchall_1 */
/* :catchall_1 */
/* move-exception v2 */
try { // :try_start_4
(( java.lang.Throwable ) v1 ).addSuppressed ( v2 ); // invoke-virtual {v1, v2}, Ljava/lang/Throwable;->addSuppressed(Ljava/lang/Throwable;)V
} // .end local p0 # "this":Lcom/miui/server/SecurityManagerService;
} // :cond_2
} // :goto_0
/* throw v1 */
/* :try_end_4 */
/* .catch Ljava/lang/Exception; {:try_start_4 ..:try_end_4} :catch_0 */
/* .line 1166 */
} // .end local v0 # "fis":Ljava/io/FileInputStream;
/* .restart local p0 # "this":Lcom/miui/server/SecurityManagerService; */
/* :catch_0 */
/* move-exception v0 */
/* .line 1167 */
/* .local v0, "e":Ljava/lang/Exception; */
final String v1 = "SecurityManagerService"; // const-string v1, "SecurityManagerService"
final String v2 = "Error reading package settings"; // const-string v2, "Error reading package settings"
android.util.Log .w ( v1,v2,v0 );
/* .line 1169 */
} // .end local v0 # "e":Ljava/lang/Exception;
} // :goto_1
return;
} // .end method
private void updateXSpaceSettings ( ) {
/* .locals 10 */
/* .line 684 */
v0 = this.mUserStateLock;
/* monitor-enter v0 */
/* .line 685 */
try { // :try_start_0
v1 = miui.securityspace.ConfigUtils .isSupportXSpace ( );
if ( v1 != null) { // if-eqz v1, :cond_2
/* iget-boolean v1, p0, Lcom/miui/server/SecurityManagerService;->mIsUpdated:Z */
/* if-nez v1, :cond_2 */
/* .line 686 */
int v1 = 0; // const/4 v1, 0x0
(( com.miui.server.SecurityManagerService ) p0 ).getUserStateLocked ( v1 ); // invoke-virtual {p0, v1}, Lcom/miui/server/SecurityManagerService;->getUserStateLocked(I)Lcom/miui/server/security/SecurityUserState;
/* .line 687 */
/* .local v1, "userState":Lcom/miui/server/security/SecurityUserState; */
/* const/16 v2, 0x3e7 */
(( com.miui.server.SecurityManagerService ) p0 ).getUserStateLocked ( v2 ); // invoke-virtual {p0, v2}, Lcom/miui/server/SecurityManagerService;->getUserStateLocked(I)Lcom/miui/server/security/SecurityUserState;
/* .line 688 */
/* .local v2, "userStateXSpace":Lcom/miui/server/security/SecurityUserState; */
v3 = this.mPackages;
(( java.util.HashMap ) v3 ).entrySet ( ); // invoke-virtual {v3}, Ljava/util/HashMap;->entrySet()Ljava/util/Set;
/* .line 689 */
/* .local v3, "packagesSet":Ljava/util/Set;, "Ljava/util/Set<Ljava/util/Map$Entry<Ljava/lang/String;Lcom/miui/server/security/SecurityPackageSettings;>;>;" */
v5 = } // :goto_0
if ( v5 != null) { // if-eqz v5, :cond_1
/* check-cast v5, Ljava/util/Map$Entry; */
/* .line 690 */
/* .local v5, "entrySet":Ljava/util/Map$Entry;, "Ljava/util/Map$Entry<Ljava/lang/String;Lcom/miui/server/security/SecurityPackageSettings;>;" */
/* check-cast v6, Ljava/lang/String; */
/* .line 691 */
/* .local v6, "name":Ljava/lang/String; */
v7 = android.text.TextUtils .isEmpty ( v6 );
/* if-nez v7, :cond_0 */
v7 = this.mContext;
v7 = miui.securityspace.XSpaceUserHandle .isAppInXSpace ( v7,v6 );
if ( v7 != null) { // if-eqz v7, :cond_0
/* .line 692 */
/* check-cast v7, Lcom/miui/server/security/SecurityPackageSettings; */
/* .line 693 */
/* .local v7, "value":Lcom/miui/server/security/SecurityPackageSettings; */
/* new-instance v8, Lcom/miui/server/security/SecurityPackageSettings; */
/* invoke-direct {v8, v6}, Lcom/miui/server/security/SecurityPackageSettings;-><init>(Ljava/lang/String;)V */
/* .line 694 */
/* .local v8, "psXSpace":Lcom/miui/server/security/SecurityPackageSettings; */
/* iget-boolean v9, v7, Lcom/miui/server/security/SecurityPackageSettings;->accessControl:Z */
/* iput-boolean v9, v8, Lcom/miui/server/security/SecurityPackageSettings;->accessControl:Z */
/* .line 695 */
/* iget-boolean v9, v7, Lcom/miui/server/security/SecurityPackageSettings;->childrenControl:Z */
/* iput-boolean v9, v8, Lcom/miui/server/security/SecurityPackageSettings;->childrenControl:Z */
/* .line 696 */
v9 = this.mPackages;
(( java.util.HashMap ) v9 ).put ( v6, v8 ); // invoke-virtual {v9, v6, v8}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;
/* .line 698 */
} // .end local v5 # "entrySet":Ljava/util/Map$Entry;, "Ljava/util/Map$Entry<Ljava/lang/String;Lcom/miui/server/security/SecurityPackageSettings;>;"
} // .end local v6 # "name":Ljava/lang/String;
} // .end local v7 # "value":Lcom/miui/server/security/SecurityPackageSettings;
} // .end local v8 # "psXSpace":Lcom/miui/server/security/SecurityPackageSettings;
} // :cond_0
/* .line 699 */
} // :cond_1
(( com.miui.server.SecurityManagerService ) p0 ).scheduleWriteSettings ( ); // invoke-virtual {p0}, Lcom/miui/server/SecurityManagerService;->scheduleWriteSettings()V
/* .line 700 */
v4 = this.mAccessControlImpl;
(( com.miui.server.security.AccessControlImpl ) v4 ).updateMaskObserverValues ( ); // invoke-virtual {v4}, Lcom/miui/server/security/AccessControlImpl;->updateMaskObserverValues()V
/* .line 702 */
} // .end local v1 # "userState":Lcom/miui/server/security/SecurityUserState;
} // .end local v2 # "userStateXSpace":Lcom/miui/server/security/SecurityUserState;
} // .end local v3 # "packagesSet":Ljava/util/Set;, "Ljava/util/Set<Ljava/util/Map$Entry<Ljava/lang/String;Lcom/miui/server/security/SecurityPackageSettings;>;>;"
} // :cond_2
/* monitor-exit v0 */
/* .line 703 */
return;
/* .line 702 */
/* :catchall_0 */
/* move-exception v1 */
/* monitor-exit v0 */
/* :try_end_0 */
/* .catchall {:try_start_0 ..:try_end_0} :catchall_0 */
/* throw v1 */
} // .end method
/* # virtual methods */
public Integer activityResume ( android.content.Intent p0 ) {
/* .locals 1 */
/* .param p1, "intent" # Landroid/content/Intent; */
/* .line 536 */
v0 = this.mAccessControlImpl;
v0 = (( com.miui.server.security.AccessControlImpl ) v0 ).activityResume ( p1 ); // invoke-virtual {v0, p1}, Lcom/miui/server/security/AccessControlImpl;->activityResume(Landroid/content/Intent;)I
} // .end method
public void addAccessControlPass ( java.lang.String p0 ) {
/* .locals 1 */
/* .param p1, "packageName" # Ljava/lang/String; */
/* .line 430 */
/* const/16 v0, 0x3e8 */
/* filled-new-array {v0}, [I */
/* invoke-direct {p0, v0}, Lcom/miui/server/SecurityManagerService;->checkPermissionByUid([I)V */
/* .line 431 */
v0 = android.os.UserHandle .getCallingUserId ( );
(( com.miui.server.SecurityManagerService ) p0 ).addAccessControlPassForUser ( p1, v0 ); // invoke-virtual {p0, p1, v0}, Lcom/miui/server/SecurityManagerService;->addAccessControlPassForUser(Ljava/lang/String;I)V
/* .line 432 */
return;
} // .end method
public void addAccessControlPassForUser ( java.lang.String p0, Integer p1 ) {
/* .locals 1 */
/* .param p1, "packageName" # Ljava/lang/String; */
/* .param p2, "userId" # I */
/* .line 436 */
/* invoke-direct {p0}, Lcom/miui/server/SecurityManagerService;->checkPermission()V */
/* .line 437 */
v0 = this.mAccessControlImpl;
(( com.miui.server.security.AccessControlImpl ) v0 ).addAccessControlPassForUser ( p1, p2 ); // invoke-virtual {v0, p1, p2}, Lcom/miui/server/security/AccessControlImpl;->addAccessControlPassForUser(Ljava/lang/String;I)V
/* .line 438 */
return;
} // .end method
public Boolean addMiuiFirewallSharedUid ( Integer p0 ) {
/* .locals 1 */
/* .param p1, "uid" # I */
/* .line 257 */
/* const/16 v0, 0x3e8 */
/* filled-new-array {v0}, [I */
/* invoke-direct {p0, v0}, Lcom/miui/server/SecurityManagerService;->checkPermissionByUid([I)V */
/* .line 258 */
v0 = com.android.server.net.NetworkManagementServiceStub .getInstance ( );
} // .end method
public void addRestrictChainMaps ( java.lang.String p0, java.lang.String p1 ) {
/* .locals 3 */
/* .param p1, "caller" # Ljava/lang/String; */
/* .param p2, "callee" # Ljava/lang/String; */
/* .line 1332 */
v0 = android.text.TextUtils .isEmpty ( p2 );
/* if-nez v0, :cond_5 */
v0 = android.text.TextUtils .isEmpty ( p1 );
if ( v0 != null) { // if-eqz v0, :cond_0
/* goto/16 :goto_3 */
/* .line 1335 */
} // :cond_0
/* const/16 v0, 0x3e8 */
/* filled-new-array {v0}, [I */
/* invoke-direct {p0, v0}, Lcom/miui/server/SecurityManagerService;->checkPermissionByUid([I)V */
/* .line 1336 */
final String v0 = "com.tencent.mm"; // const-string v0, "com.tencent.mm"
v0 = android.text.TextUtils .equals ( v0,p2 );
/* if-nez v0, :cond_2 */
/* .line 1337 */
final String v0 = "com.eg.android.AlipayGphone"; // const-string v0, "com.eg.android.AlipayGphone"
v0 = android.text.TextUtils .equals ( v0,p2 );
/* if-nez v0, :cond_2 */
/* .line 1338 */
v0 = this.restrictChainMaps;
(( java.util.LinkedHashMap ) v0 ).get ( p1 ); // invoke-virtual {v0, p1}, Ljava/util/LinkedHashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;
/* check-cast v0, Ljava/util/List; */
/* .line 1339 */
/* .local v0, "callees":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;" */
if ( v0 != null) { // if-eqz v0, :cond_1
/* .line 1340 */
/* .line 1342 */
} // :cond_1
/* new-instance v1, Ljava/util/ArrayList; */
/* invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V */
/* .line 1343 */
/* .local v1, "strings":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/String;>;" */
(( java.util.ArrayList ) v1 ).add ( p2 ); // invoke-virtual {v1, p2}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z
/* .line 1344 */
v2 = this.restrictChainMaps;
(( java.util.LinkedHashMap ) v2 ).put ( p1, v1 ); // invoke-virtual {v2, p1, v1}, Ljava/util/LinkedHashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;
/* .line 1347 */
} // .end local v0 # "callees":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;"
} // .end local v1 # "strings":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/String;>;"
} // :cond_2
} // :goto_0
v0 = this.restrictChainMaps;
(( java.util.LinkedHashMap ) v0 ).get ( p2 ); // invoke-virtual {v0, p2}, Ljava/util/LinkedHashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;
/* check-cast v0, Ljava/util/List; */
/* .line 1348 */
/* .local v0, "callers":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;" */
if ( v0 != null) { // if-eqz v0, :cond_3
/* .line 1349 */
/* .line 1351 */
} // :cond_3
/* new-instance v1, Ljava/util/ArrayList; */
/* invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V */
/* .line 1352 */
/* .restart local v1 # "strings":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/String;>;" */
(( java.util.ArrayList ) v1 ).add ( p1 ); // invoke-virtual {v1, p1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z
/* .line 1353 */
v2 = this.restrictChainMaps;
(( java.util.LinkedHashMap ) v2 ).put ( p2, v1 ); // invoke-virtual {v2, p2, v1}, Ljava/util/LinkedHashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;
/* .line 1355 */
} // .end local v1 # "strings":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/String;>;"
} // :goto_1
v1 = this.restrictChainMaps;
v1 = (( java.util.LinkedHashMap ) v1 ).size ( ); // invoke-virtual {v1}, Ljava/util/LinkedHashMap;->size()I
/* const/16 v2, 0x64 */
/* if-le v1, v2, :cond_4 */
/* .line 1357 */
try { // :try_start_0
v1 = this.restrictChainMaps;
(( java.util.LinkedHashMap ) v1 ).entrySet ( ); // invoke-virtual {v1}, Ljava/util/LinkedHashMap;->entrySet()Ljava/util/Set;
/* check-cast v2, Ljava/util/Map$Entry; */
(( java.util.LinkedHashMap ) v1 ).remove ( v2 ); // invoke-virtual {v1, v2}, Ljava/util/LinkedHashMap;->remove(Ljava/lang/Object;)Ljava/lang/Object;
/* :try_end_0 */
/* .catch Ljava/lang/Exception; {:try_start_0 ..:try_end_0} :catch_0 */
/* .line 1360 */
/* .line 1358 */
/* :catch_0 */
/* move-exception v1 */
/* .line 1362 */
} // :cond_4
} // :goto_2
return;
/* .line 1333 */
} // .end local v0 # "callers":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;"
} // :cond_5
} // :goto_3
return;
} // .end method
public Boolean areNotificationsEnabledForPackage ( java.lang.String p0, Integer p1 ) {
/* .locals 3 */
/* .param p1, "packageName" # Ljava/lang/String; */
/* .param p2, "uid" # I */
/* .annotation system Ldalvik/annotation/Throws; */
/* value = { */
/* Landroid/os/RemoteException; */
/* } */
} // .end annotation
/* .line 852 */
/* invoke-direct {p0}, Lcom/miui/server/SecurityManagerService;->checkPermission()V */
/* .line 853 */
v0 = this.mINotificationManager;
/* if-nez v0, :cond_0 */
/* .line 854 */
final String v0 = "notification"; // const-string v0, "notification"
android.os.ServiceManager .getService ( v0 );
android.app.INotificationManager$Stub .asInterface ( v0 );
this.mINotificationManager = v0;
/* .line 856 */
} // :cond_0
android.os.Binder .clearCallingIdentity ( );
/* move-result-wide v0 */
/* .line 858 */
/* .local v0, "identity":J */
try { // :try_start_0
v2 = v2 = this.mINotificationManager;
/* :try_end_0 */
/* .catchall {:try_start_0 ..:try_end_0} :catchall_0 */
/* .line 860 */
android.os.Binder .restoreCallingIdentity ( v0,v1 );
/* .line 858 */
/* .line 860 */
/* :catchall_0 */
/* move-exception v2 */
android.os.Binder .restoreCallingIdentity ( v0,v1 );
/* .line 861 */
/* throw v2 */
} // .end method
public Boolean checkAccessControlPass ( java.lang.String p0, android.content.Intent p1 ) {
/* .locals 2 */
/* .param p1, "packageName" # Ljava/lang/String; */
/* .param p2, "intent" # Landroid/content/Intent; */
/* .line 449 */
v0 = android.os.UserHandle .getCallingUserId ( );
/* .line 450 */
/* .local v0, "callingUserId":I */
v1 = this.mAccessControlImpl;
v1 = (( com.miui.server.security.AccessControlImpl ) v1 ).checkAccessControlPassLocked ( p1, p2, v0 ); // invoke-virtual {v1, p1, p2, v0}, Lcom/miui/server/security/AccessControlImpl;->checkAccessControlPassLocked(Ljava/lang/String;Landroid/content/Intent;I)Z
} // .end method
public Boolean checkAccessControlPassAsUser ( java.lang.String p0, android.content.Intent p1, Integer p2 ) {
/* .locals 1 */
/* .param p1, "packageName" # Ljava/lang/String; */
/* .param p2, "intent" # Landroid/content/Intent; */
/* .param p3, "userId" # I */
/* .line 455 */
v0 = this.mAccessControlImpl;
v0 = (( com.miui.server.security.AccessControlImpl ) v0 ).checkAccessControlPassLocked ( p1, p2, p3 ); // invoke-virtual {v0, p1, p2, p3}, Lcom/miui/server/security/AccessControlImpl;->checkAccessControlPassLocked(Ljava/lang/String;Landroid/content/Intent;I)Z
} // .end method
public Boolean checkAccessControlPassword ( java.lang.String p0, java.lang.String p1, Integer p2 ) {
/* .locals 1 */
/* .param p1, "passwordType" # Ljava/lang/String; */
/* .param p2, "password" # Ljava/lang/String; */
/* .param p3, "userId" # I */
/* .line 505 */
/* invoke-direct {p0}, Lcom/miui/server/SecurityManagerService;->checkPermission()V */
/* .line 506 */
p3 = miui.security.SecurityManager .getUserHandle ( p3 );
/* .line 507 */
v0 = this.mAccessController;
v0 = (( com.miui.server.AccessController ) v0 ).checkAccessControlPassword ( p1, p2, p3 ); // invoke-virtual {v0, p1, p2, p3}, Lcom/miui/server/AccessController;->checkAccessControlPassword(Ljava/lang/String;Ljava/lang/String;I)Z
} // .end method
public Boolean checkSmsBlocked ( android.content.Intent p0 ) {
/* .locals 2 */
/* .param p1, "intent" # Landroid/content/Intent; */
/* .line 239 */
/* const/16 v0, 0x3e8 */
/* const/16 v1, 0x3e9 */
/* filled-new-array {v0, v1}, [I */
/* invoke-direct {p0, v0}, Lcom/miui/server/SecurityManagerService;->checkPermissionByUid([I)V */
/* .line 240 */
v0 = this.mSecuritySmsHandler;
v0 = (( com.miui.server.SecuritySmsHandler ) v0 ).checkSmsBlocked ( p1 ); // invoke-virtual {v0, p1}, Lcom/miui/server/SecuritySmsHandler;->checkSmsBlocked(Landroid/content/Intent;)Z
} // .end method
protected void dump ( java.io.FileDescriptor p0, java.io.PrintWriter p1, java.lang.String[] p2 ) {
/* .locals 2 */
/* .param p1, "fd" # Ljava/io/FileDescriptor; */
/* .param p2, "out" # Ljava/io/PrintWriter; */
/* .param p3, "args" # [Ljava/lang/String; */
/* .line 147 */
v0 = this.mContext;
final String v1 = "android.permission.DUMP"; // const-string v1, "android.permission.DUMP"
v0 = (( android.content.Context ) v0 ).checkCallingOrSelfPermission ( v1 ); // invoke-virtual {v0, v1}, Landroid/content/Context;->checkCallingOrSelfPermission(Ljava/lang/String;)I
if ( v0 != null) { // if-eqz v0, :cond_0
/* .line 149 */
/* new-instance v0, Ljava/lang/StringBuilder; */
/* invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V */
final String v1 = "Permission Denial: can\'t dump SecurityManager from from pid="; // const-string v1, "Permission Denial: can\'t dump SecurityManager from from pid="
(( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
/* .line 150 */
v1 = android.os.Binder .getCallingPid ( );
(( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;
final String v1 = ", uid="; // const-string v1, ", uid="
(( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
/* .line 151 */
v1 = android.os.Binder .getCallingUid ( );
(( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v0 ).toString ( ); // invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
/* .line 149 */
(( java.io.PrintWriter ) p2 ).println ( v0 ); // invoke-virtual {p2, v0}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V
/* .line 152 */
return;
/* .line 154 */
} // :cond_0
miui.security.WakePathChecker .getInstance ( );
(( miui.security.WakePathChecker ) v0 ).dump ( p2 ); // invoke-virtual {v0, p2}, Lmiui/security/WakePathChecker;->dump(Ljava/io/PrintWriter;)V
/* .line 155 */
com.android.server.net.NetworkManagementServiceStub .getInstance ( );
/* .line 156 */
/* invoke-direct {p0, p2}, Lcom/miui/server/SecurityManagerService;->dumpPrivacyVirtualDisplay(Ljava/io/PrintWriter;)V */
/* .line 157 */
v0 = this.mAppBehavior;
(( com.miui.server.security.AppBehaviorService ) v0 ).dump ( p2 ); // invoke-virtual {v0, p2}, Lcom/miui/server/security/AppBehaviorService;->dump(Ljava/io/PrintWriter;)V
/* .line 158 */
return;
} // .end method
public Boolean exemptByRestrictChain ( java.lang.String p0, java.lang.String p1 ) {
/* .locals 3 */
/* .param p1, "caller" # Ljava/lang/String; */
/* .param p2, "callee" # Ljava/lang/String; */
/* .line 1381 */
v0 = this.restrictChainMaps;
v0 = (( java.util.LinkedHashMap ) v0 ).containsKey ( p1 ); // invoke-virtual {v0, p1}, Ljava/util/LinkedHashMap;->containsKey(Ljava/lang/Object;)Z
int v1 = 0; // const/4 v1, 0x0
/* if-nez v0, :cond_0 */
/* .line 1382 */
/* .line 1384 */
} // :cond_0
v0 = this.restrictChainMaps;
(( java.util.LinkedHashMap ) v0 ).get ( p1 ); // invoke-virtual {v0, p1}, Ljava/util/LinkedHashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;
/* check-cast v0, Ljava/util/List; */
/* .line 1385 */
/* .local v0, "callees":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;" */
v2 = if ( v0 != null) { // if-eqz v0, :cond_1
if ( v2 != null) { // if-eqz v2, :cond_1
int v1 = 1; // const/4 v1, 0x1
} // :cond_1
} // .end method
public void exemptTemporarily ( java.lang.String p0 ) {
/* .locals 1 */
/* .param p1, "packageName" # Ljava/lang/String; */
/* .line 1071 */
/* invoke-direct {p0}, Lcom/miui/server/SecurityManagerService;->checkPermission()V */
/* .line 1072 */
com.android.server.am.PendingIntentRecordStub .get ( );
/* .line 1073 */
return;
} // .end method
public void finishAccessControl ( java.lang.String p0, Integer p1 ) {
/* .locals 1 */
/* .param p1, "packageName" # Ljava/lang/String; */
/* .param p2, "userId" # I */
/* .line 530 */
/* invoke-direct {p0}, Lcom/miui/server/SecurityManagerService;->checkPermission()V */
/* .line 531 */
v0 = this.mAccessControlImpl;
(( com.miui.server.security.AccessControlImpl ) v0 ).finishAccessControl ( p1, p2 ); // invoke-virtual {v0, p1, p2}, Lcom/miui/server/security/AccessControlImpl;->finishAccessControl(Ljava/lang/String;I)V
/* .line 532 */
return;
} // .end method
public java.lang.String getAccessControlPasswordType ( Integer p0 ) {
/* .locals 2 */
/* .param p1, "userId" # I */
/* .line 518 */
/* invoke-direct {p0}, Lcom/miui/server/SecurityManagerService;->checkPermission()V */
/* .line 519 */
v0 = this.mAccessController;
v1 = miui.security.SecurityManager .getUserHandle ( p1 );
(( com.miui.server.AccessController ) v0 ).getAccessControlPasswordType ( v1 ); // invoke-virtual {v0, v1}, Lcom/miui/server/AccessController;->getAccessControlPasswordType(I)Ljava/lang/String;
} // .end method
public java.util.List getAllGameStorageApps ( Integer p0 ) {
/* .locals 1 */
/* .param p1, "userId" # I */
/* .annotation system Ldalvik/annotation/Signature; */
/* value = { */
/* "(I)", */
/* "Ljava/util/List<", */
/* "Ljava/lang/String;", */
/* ">;" */
/* } */
} // .end annotation
/* .line 312 */
/* invoke-direct {p0}, Lcom/miui/server/SecurityManagerService;->checkPermission()V */
/* .line 313 */
v0 = this.mGameBoosterImpl;
(( com.miui.server.security.GameBoosterImpl ) v0 ).getAllGameStorageApps ( p1 ); // invoke-virtual {v0, p1}, Lcom/miui/server/security/GameBoosterImpl;->getAllGameStorageApps(I)Ljava/util/List;
} // .end method
public java.util.List getAllPrivacyApps ( Integer p0 ) {
/* .locals 10 */
/* .param p1, "userId" # I */
/* .annotation system Ldalvik/annotation/Signature; */
/* value = { */
/* "(I)", */
/* "Ljava/util/List<", */
/* "Ljava/lang/String;", */
/* ">;" */
/* } */
} // .end annotation
/* .line 792 */
/* invoke-direct {p0}, Lcom/miui/server/SecurityManagerService;->checkPermission()V */
/* .line 793 */
v0 = this.mUserStateLock;
/* monitor-enter v0 */
/* .line 794 */
try { // :try_start_0
/* new-instance v1, Ljava/util/ArrayList; */
/* invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V */
/* .line 795 */
/* .local v1, "privacyAppsList":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;" */
(( com.miui.server.SecurityManagerService ) p0 ).getUserStateLocked ( p1 ); // invoke-virtual {p0, p1}, Lcom/miui/server/SecurityManagerService;->getUserStateLocked(I)Lcom/miui/server/security/SecurityUserState;
/* .line 796 */
/* .local v2, "userState":Lcom/miui/server/security/SecurityUserState; */
v3 = this.mPackages;
/* .line 797 */
/* .local v3, "packages":Ljava/util/HashMap;, "Ljava/util/HashMap<Ljava/lang/String;Lcom/miui/server/security/SecurityPackageSettings;>;" */
(( java.util.HashMap ) v3 ).keySet ( ); // invoke-virtual {v3}, Ljava/util/HashMap;->keySet()Ljava/util/Set;
/* .line 798 */
/* .local v4, "pkgNames":Ljava/util/Set;, "Ljava/util/Set<Ljava/lang/String;>;" */
v6 = } // :goto_0
if ( v6 != null) { // if-eqz v6, :cond_1
/* check-cast v6, Ljava/lang/String; */
/* :try_end_0 */
/* .catchall {:try_start_0 ..:try_end_0} :catchall_0 */
/* .line 800 */
/* .local v6, "pkgName":Ljava/lang/String; */
try { // :try_start_1
v7 = this.mPackages;
(( com.miui.server.SecurityManagerService ) p0 ).getPackageSetting ( v7, v6 ); // invoke-virtual {p0, v7, v6}, Lcom/miui/server/SecurityManagerService;->getPackageSetting(Ljava/util/HashMap;Ljava/lang/String;)Lcom/miui/server/security/SecurityPackageSettings;
/* .line 801 */
/* .local v7, "ps":Lcom/miui/server/security/SecurityPackageSettings; */
/* iget-boolean v8, v7, Lcom/miui/server/security/SecurityPackageSettings;->isPrivacyApp:Z */
if ( v8 != null) { // if-eqz v8, :cond_0
/* .line 802 */
/* :try_end_1 */
/* .catch Ljava/lang/Exception; {:try_start_1 ..:try_end_1} :catch_0 */
/* .catchall {:try_start_1 ..:try_end_1} :catchall_0 */
/* .line 806 */
} // .end local v7 # "ps":Lcom/miui/server/security/SecurityPackageSettings;
} // :cond_0
/* .line 804 */
/* :catch_0 */
/* move-exception v7 */
/* .line 805 */
/* .local v7, "e":Ljava/lang/Exception; */
try { // :try_start_2
final String v8 = "SecurityManagerService"; // const-string v8, "SecurityManagerService"
final String v9 = "getAllPrivacyApps error"; // const-string v9, "getAllPrivacyApps error"
android.util.Log .e ( v8,v9,v7 );
/* .line 807 */
} // .end local v6 # "pkgName":Ljava/lang/String;
} // .end local v7 # "e":Ljava/lang/Exception;
} // :goto_1
/* .line 808 */
} // :cond_1
/* monitor-exit v0 */
/* .line 809 */
} // .end local v1 # "privacyAppsList":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;"
} // .end local v2 # "userState":Lcom/miui/server/security/SecurityUserState;
} // .end local v3 # "packages":Ljava/util/HashMap;, "Ljava/util/HashMap<Ljava/lang/String;Lcom/miui/server/security/SecurityPackageSettings;>;"
} // .end local v4 # "pkgNames":Ljava/util/Set;, "Ljava/util/Set<Ljava/lang/String;>;"
/* :catchall_0 */
/* move-exception v1 */
/* monitor-exit v0 */
/* :try_end_2 */
/* .catchall {:try_start_2 ..:try_end_2} :catchall_0 */
/* throw v1 */
} // .end method
public Boolean getAppDarkMode ( java.lang.String p0 ) {
/* .locals 2 */
/* .param p1, "packageName" # Ljava/lang/String; */
/* .line 321 */
v0 = android.os.UserHandle .getCallingUserId ( );
/* .line 322 */
/* .local v0, "userId":I */
v1 = (( com.miui.server.SecurityManagerService ) p0 ).getAppDarkModeForUser ( p1, v0 ); // invoke-virtual {p0, p1, v0}, Lcom/miui/server/SecurityManagerService;->getAppDarkModeForUser(Ljava/lang/String;I)Z
} // .end method
public Boolean getAppDarkModeForUser ( java.lang.String p0, Integer p1 ) {
/* .locals 1 */
/* .param p1, "packageName" # Ljava/lang/String; */
/* .param p2, "userId" # I */
/* .line 327 */
v0 = this.mInternal;
v0 = (( miui.security.SecurityManagerInternal ) v0 ).getAppDarkModeForUser ( p1, p2 ); // invoke-virtual {v0, p1, p2}, Lmiui/security/SecurityManagerInternal;->getAppDarkModeForUser(Ljava/lang/String;I)Z
} // .end method
public Boolean getAppRelaunchModeAfterFolded ( java.lang.String p0 ) {
/* .locals 2 */
/* .param p1, "packageName" # Ljava/lang/String; */
/* .line 369 */
v0 = android.os.UserHandle .getCallingUserId ( );
/* .line 370 */
/* .local v0, "userId":I */
v1 = (( com.miui.server.SecurityManagerService ) p0 ).getAppRelaunchModeAfterFoldedForUser ( p1, v0 ); // invoke-virtual {p0, p1, v0}, Lcom/miui/server/SecurityManagerService;->getAppRelaunchModeAfterFoldedForUser(Ljava/lang/String;I)Z
} // .end method
public Boolean getAppRelaunchModeAfterFoldedForUser ( java.lang.String p0, Integer p1 ) {
/* .locals 4 */
/* .param p1, "packageName" # Ljava/lang/String; */
/* .param p2, "userId" # I */
/* .line 375 */
v0 = this.mUserStateLock;
/* monitor-enter v0 */
/* .line 376 */
try { // :try_start_0
(( com.miui.server.SecurityManagerService ) p0 ).getUserStateLocked ( p2 ); // invoke-virtual {p0, p2}, Lcom/miui/server/SecurityManagerService;->getUserStateLocked(I)Lcom/miui/server/security/SecurityUserState;
/* :try_end_0 */
/* .catchall {:try_start_0 ..:try_end_0} :catchall_0 */
/* .line 378 */
/* .local v1, "userState":Lcom/miui/server/security/SecurityUserState; */
try { // :try_start_1
v2 = this.mPackages;
(( com.miui.server.SecurityManagerService ) p0 ).getPackageSetting ( v2, p1 ); // invoke-virtual {p0, v2, p1}, Lcom/miui/server/SecurityManagerService;->getPackageSetting(Ljava/util/HashMap;Ljava/lang/String;)Lcom/miui/server/security/SecurityPackageSettings;
/* .line 379 */
/* .local v2, "ps":Lcom/miui/server/security/SecurityPackageSettings; */
/* iget-boolean v3, v2, Lcom/miui/server/security/SecurityPackageSettings;->isRelaunchWhenFolded:Z */
/* :try_end_1 */
/* .catch Ljava/lang/Exception; {:try_start_1 ..:try_end_1} :catch_0 */
/* .catchall {:try_start_1 ..:try_end_1} :catchall_0 */
try { // :try_start_2
/* monitor-exit v0 */
/* .line 380 */
} // .end local v2 # "ps":Lcom/miui/server/security/SecurityPackageSettings;
/* :catch_0 */
/* move-exception v2 */
/* .line 381 */
/* .local v2, "e":Ljava/lang/Exception; */
/* monitor-exit v0 */
int v0 = 0; // const/4 v0, 0x0
/* .line 383 */
} // .end local v1 # "userState":Lcom/miui/server/security/SecurityUserState;
} // .end local v2 # "e":Ljava/lang/Exception;
/* :catchall_0 */
/* move-exception v1 */
/* monitor-exit v0 */
/* :try_end_2 */
/* .catchall {:try_start_2 ..:try_end_2} :catchall_0 */
/* throw v1 */
} // .end method
public Boolean getAppRemindForRelaunch ( java.lang.String p0 ) {
/* .locals 2 */
/* .param p1, "packageName" # Ljava/lang/String; */
/* .line 340 */
v0 = android.os.UserHandle .getCallingUserId ( );
/* .line 341 */
/* .local v0, "userId":I */
v1 = (( com.miui.server.SecurityManagerService ) p0 ).getAppRemindForRelaunchForUser ( p1, v0 ); // invoke-virtual {p0, p1, v0}, Lcom/miui/server/SecurityManagerService;->getAppRemindForRelaunchForUser(Ljava/lang/String;I)Z
} // .end method
public Boolean getAppRemindForRelaunchForUser ( java.lang.String p0, Integer p1 ) {
/* .locals 4 */
/* .param p1, "packageName" # Ljava/lang/String; */
/* .param p2, "userId" # I */
/* .line 346 */
v0 = this.mUserStateLock;
/* monitor-enter v0 */
/* .line 347 */
try { // :try_start_0
(( com.miui.server.SecurityManagerService ) p0 ).getUserStateLocked ( p2 ); // invoke-virtual {p0, p2}, Lcom/miui/server/SecurityManagerService;->getUserStateLocked(I)Lcom/miui/server/security/SecurityUserState;
/* :try_end_0 */
/* .catchall {:try_start_0 ..:try_end_0} :catchall_0 */
/* .line 349 */
/* .local v1, "userState":Lcom/miui/server/security/SecurityUserState; */
try { // :try_start_1
v2 = this.mPackages;
(( com.miui.server.SecurityManagerService ) p0 ).getPackageSetting ( v2, p1 ); // invoke-virtual {p0, v2, p1}, Lcom/miui/server/SecurityManagerService;->getPackageSetting(Ljava/util/HashMap;Ljava/lang/String;)Lcom/miui/server/security/SecurityPackageSettings;
/* .line 350 */
/* .local v2, "ps":Lcom/miui/server/security/SecurityPackageSettings; */
/* iget-boolean v3, v2, Lcom/miui/server/security/SecurityPackageSettings;->isRemindForRelaunch:Z */
/* :try_end_1 */
/* .catch Ljava/lang/Exception; {:try_start_1 ..:try_end_1} :catch_0 */
/* .catchall {:try_start_1 ..:try_end_1} :catchall_0 */
try { // :try_start_2
/* monitor-exit v0 */
/* .line 351 */
} // .end local v2 # "ps":Lcom/miui/server/security/SecurityPackageSettings;
/* :catch_0 */
/* move-exception v2 */
/* .line 352 */
/* .local v2, "e":Ljava/lang/Exception; */
/* monitor-exit v0 */
int v0 = 0; // const/4 v0, 0x0
/* .line 354 */
} // .end local v1 # "userState":Lcom/miui/server/security/SecurityUserState;
} // .end local v2 # "e":Ljava/lang/Exception;
/* :catchall_0 */
/* move-exception v1 */
/* monitor-exit v0 */
/* :try_end_2 */
/* .catchall {:try_start_2 ..:try_end_2} :catchall_0 */
/* throw v1 */
} // .end method
public android.os.IBinder getAppRunningControlIBinder ( ) {
/* .locals 1 */
/* .line 580 */
v0 = this.mAppRunningControlService;
(( com.miui.server.AppRunningControlService ) v0 ).asBinder ( ); // invoke-virtual {v0}, Lcom/miui/server/AppRunningControlService;->asBinder()Landroid/os/IBinder;
} // .end method
public Boolean getApplicationAccessControlEnabled ( java.lang.String p0 ) {
/* .locals 2 */
/* .param p1, "packageName" # Ljava/lang/String; */
/* .line 469 */
v0 = this.mAccessControlImpl;
/* .line 470 */
v1 = android.os.UserHandle .getCallingUserId ( );
/* .line 469 */
v0 = (( com.miui.server.security.AccessControlImpl ) v0 ).getApplicationAccessControlEnabledLocked ( p1, v1 ); // invoke-virtual {v0, p1, v1}, Lcom/miui/server/security/AccessControlImpl;->getApplicationAccessControlEnabledLocked(Ljava/lang/String;I)Z
} // .end method
public Boolean getApplicationAccessControlEnabledAsUser ( java.lang.String p0, Integer p1 ) {
/* .locals 1 */
/* .param p1, "packageName" # Ljava/lang/String; */
/* .param p2, "userId" # I */
/* .line 460 */
v0 = this.mAccessControlImpl;
v0 = (( com.miui.server.security.AccessControlImpl ) v0 ).getApplicationAccessControlEnabledLocked ( p1, p2 ); // invoke-virtual {v0, p1, p2}, Lcom/miui/server/security/AccessControlImpl;->getApplicationAccessControlEnabledLocked(Ljava/lang/String;I)Z
} // .end method
public Boolean getApplicationChildrenControlEnabled ( java.lang.String p0 ) {
/* .locals 5 */
/* .param p1, "packageName" # Ljava/lang/String; */
/* .line 554 */
v0 = android.os.UserHandle .getCallingUserId ( );
/* .line 555 */
/* .local v0, "callingUserId":I */
v1 = this.mUserStateLock;
/* monitor-enter v1 */
/* .line 557 */
try { // :try_start_0
(( com.miui.server.SecurityManagerService ) p0 ).getUserStateLocked ( v0 ); // invoke-virtual {p0, v0}, Lcom/miui/server/SecurityManagerService;->getUserStateLocked(I)Lcom/miui/server/security/SecurityUserState;
/* .line 558 */
/* .local v2, "userStateLocked":Lcom/miui/server/security/SecurityUserState; */
v3 = this.mPackages;
(( com.miui.server.SecurityManagerService ) p0 ).getPackageSetting ( v3, p1 ); // invoke-virtual {p0, v3, p1}, Lcom/miui/server/SecurityManagerService;->getPackageSetting(Ljava/util/HashMap;Ljava/lang/String;)Lcom/miui/server/security/SecurityPackageSettings;
/* .line 559 */
/* .local v3, "ps":Lcom/miui/server/security/SecurityPackageSettings; */
/* iget-boolean v4, v3, Lcom/miui/server/security/SecurityPackageSettings;->childrenControl:Z */
/* :try_end_0 */
/* .catch Ljava/lang/Exception; {:try_start_0 ..:try_end_0} :catch_0 */
/* .catchall {:try_start_0 ..:try_end_0} :catchall_0 */
try { // :try_start_1
/* monitor-exit v1 */
/* .line 563 */
} // .end local v2 # "userStateLocked":Lcom/miui/server/security/SecurityUserState;
} // .end local v3 # "ps":Lcom/miui/server/security/SecurityPackageSettings;
/* :catchall_0 */
/* move-exception v2 */
/* .line 560 */
/* :catch_0 */
/* move-exception v2 */
/* .line 561 */
/* .local v2, "e":Ljava/lang/Exception; */
/* monitor-exit v1 */
int v1 = 0; // const/4 v1, 0x0
/* .line 563 */
} // .end local v2 # "e":Ljava/lang/Exception;
} // :goto_0
/* monitor-exit v1 */
/* :try_end_1 */
/* .catchall {:try_start_1 ..:try_end_1} :catchall_0 */
/* throw v2 */
} // .end method
public Boolean getApplicationMaskNotificationEnabledAsUser ( java.lang.String p0, Integer p1 ) {
/* .locals 1 */
/* .param p1, "packageName" # Ljava/lang/String; */
/* .param p2, "userId" # I */
/* .line 465 */
v0 = this.mAccessControlImpl;
v0 = (( com.miui.server.security.AccessControlImpl ) v0 ).getApplicationMaskNotificationEnabledLocked ( p1, p2 ); // invoke-virtual {v0, p1, p2}, Lcom/miui/server/security/AccessControlImpl;->getApplicationMaskNotificationEnabledLocked(Ljava/lang/String;I)Z
} // .end method
public java.util.List getBlurryCellInfos ( java.util.List p0 ) {
/* .locals 1 */
/* .annotation system Ldalvik/annotation/Signature; */
/* value = { */
/* "(", */
/* "Ljava/util/List<", */
/* "Landroid/telephony/CellInfo;", */
/* ">;)", */
/* "Ljava/util/List<", */
/* "Landroid/telephony/CellInfo;", */
/* ">;" */
/* } */
} // .end annotation
/* .line 961 */
/* .local p1, "location":Ljava/util/List;, "Ljava/util/List<Landroid/telephony/CellInfo;>;" */
/* invoke-direct {p0}, Lcom/miui/server/SecurityManagerService;->checkBlurLocationPermission()V */
/* .line 962 */
com.android.server.location.MiuiBlurLocationManagerStub .get ( );
(( com.android.server.location.MiuiBlurLocationManagerStub ) v0 ).getBlurryCellInfos ( p1 ); // invoke-virtual {v0, p1}, Lcom/android/server/location/MiuiBlurLocationManagerStub;->getBlurryCellInfos(Ljava/util/List;)Ljava/util/List;
} // .end method
public android.telephony.CellIdentity getBlurryCellLocation ( android.telephony.CellIdentity p0 ) {
/* .locals 1 */
/* .param p1, "location" # Landroid/telephony/CellIdentity; */
/* .line 955 */
/* invoke-direct {p0}, Lcom/miui/server/SecurityManagerService;->checkBlurLocationPermission()V */
/* .line 956 */
com.android.server.location.MiuiBlurLocationManagerStub .get ( );
(( com.android.server.location.MiuiBlurLocationManagerStub ) v0 ).getBlurryCellLocation ( p1 ); // invoke-virtual {v0, p1}, Lcom/android/server/location/MiuiBlurLocationManagerStub;->getBlurryCellLocation(Landroid/telephony/CellIdentity;)Landroid/telephony/CellIdentity;
} // .end method
public Integer getCurrentUserId ( ) {
/* .locals 1 */
/* .line 680 */
v0 = com.android.server.am.ProcessUtils .getCurrentUserId ( );
} // .end method
public Boolean getGameMode ( Integer p0 ) {
/* .locals 1 */
/* .param p1, "userId" # I */
/* .line 284 */
v0 = this.mGameBoosterImpl;
v0 = (( com.miui.server.security.GameBoosterImpl ) v0 ).getGameMode ( p1 ); // invoke-virtual {v0, p1}, Lcom/miui/server/security/GameBoosterImpl;->getGameMode(I)Z
} // .end method
public java.util.List getIncompatibleAppList ( ) {
/* .locals 1 */
/* .annotation system Ldalvik/annotation/Signature; */
/* value = { */
/* "()", */
/* "Ljava/util/List<", */
/* "Ljava/lang/String;", */
/* ">;" */
/* } */
} // .end annotation
/* .line 722 */
v0 = this.mIncompatibleAppList;
} // .end method
public java.lang.String getPackageNameByPid ( Integer p0 ) {
/* .locals 4 */
/* .param p1, "pid" # I */
/* .line 1028 */
v0 = android.os.Binder .getCallingUid ( );
/* .line 1029 */
/* .local v0, "callingUid":I */
if ( v0 != null) { // if-eqz v0, :cond_2
/* .line 1030 */
v1 = android.os.UserHandle .getAppId ( v0 );
/* const/16 v2, 0x3e8 */
/* if-eq v1, v2, :cond_2 */
/* .line 1031 */
v1 = android.os.Binder .getCallingPid ( );
com.android.server.am.ProcessUtils .getProcessRecordByPid ( v1 );
/* .line 1032 */
/* .local v1, "record":Lcom/android/server/am/ProcessRecord; */
int v2 = 0; // const/4 v2, 0x0
if ( v1 != null) { // if-eqz v1, :cond_1
(( com.android.server.am.ProcessRecord ) v1 ).getApplicationInfo ( ); // invoke-virtual {v1}, Lcom/android/server/am/ProcessRecord;->getApplicationInfo()Landroid/content/pm/ApplicationInfo;
/* if-nez v3, :cond_0 */
/* .line 1035 */
} // :cond_0
(( com.android.server.am.ProcessRecord ) v1 ).getApplicationInfo ( ); // invoke-virtual {v1}, Lcom/android/server/am/ProcessRecord;->getApplicationInfo()Landroid/content/pm/ApplicationInfo;
v3 = (( android.content.pm.ApplicationInfo ) v3 ).isSystemApp ( ); // invoke-virtual {v3}, Landroid/content/pm/ApplicationInfo;->isSystemApp()Z
/* if-nez v3, :cond_2 */
/* .line 1036 */
/* .line 1033 */
} // :cond_1
} // :goto_0
/* .line 1039 */
} // .end local v1 # "record":Lcom/android/server/am/ProcessRecord;
} // :cond_2
com.android.server.am.ProcessUtils .getPackageNameByPid ( p1 );
} // .end method
public com.miui.server.security.SecurityPackageSettings getPackageSetting ( java.util.HashMap p0, java.lang.String p1 ) {
/* .locals 2 */
/* .param p2, "packageName" # Ljava/lang/String; */
/* .annotation system Ldalvik/annotation/Signature; */
/* value = { */
/* "(", */
/* "Ljava/util/HashMap<", */
/* "Ljava/lang/String;", */
/* "Lcom/miui/server/security/SecurityPackageSettings;", */
/* ">;", */
/* "Ljava/lang/String;", */
/* ")", */
/* "Lcom/miui/server/security/SecurityPackageSettings;" */
/* } */
} // .end annotation
/* .line 1147 */
/* .local p1, "packages":Ljava/util/HashMap;, "Ljava/util/HashMap<Ljava/lang/String;Lcom/miui/server/security/SecurityPackageSettings;>;" */
(( java.util.HashMap ) p1 ).get ( p2 ); // invoke-virtual {p1, p2}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;
/* check-cast v0, Lcom/miui/server/security/SecurityPackageSettings; */
/* .line 1148 */
/* .local v0, "ps":Lcom/miui/server/security/SecurityPackageSettings; */
/* if-nez v0, :cond_0 */
/* .line 1149 */
/* new-instance v1, Lcom/miui/server/security/SecurityPackageSettings; */
/* invoke-direct {v1, p2}, Lcom/miui/server/security/SecurityPackageSettings;-><init>(Ljava/lang/String;)V */
/* move-object v0, v1 */
/* .line 1150 */
(( java.util.HashMap ) p1 ).put ( p2, v0 ); // invoke-virtual {p1, p2, v0}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;
/* .line 1152 */
} // :cond_0
} // .end method
public Integer getPermissionFlagsAsUser ( java.lang.String p0, java.lang.String p1, Integer p2 ) {
/* .locals 3 */
/* .param p1, "permName" # Ljava/lang/String; */
/* .param p2, "packageName" # Ljava/lang/String; */
/* .param p3, "userId" # I */
/* .line 920 */
final String v0 = "com.lbe.security.miui"; // const-string v0, "com.lbe.security.miui"
/* invoke-direct {p0, v0}, Lcom/miui/server/SecurityManagerService;->checkGrantPermissionPkg(Ljava/lang/String;)V */
/* .line 921 */
android.os.Binder .clearCallingIdentity ( );
/* move-result-wide v0 */
/* .line 923 */
/* .local v0, "identity":J */
try { // :try_start_0
v2 = this.mPermissionManagerService;
v2 = (( com.android.server.pm.permission.PermissionManagerService ) v2 ).getPermissionFlags ( p2, p1, p3 ); // invoke-virtual {v2, p2, p1, p3}, Lcom/android/server/pm/permission/PermissionManagerService;->getPermissionFlags(Ljava/lang/String;Ljava/lang/String;I)I
/* :try_end_0 */
/* .catchall {:try_start_0 ..:try_end_0} :catchall_0 */
/* .line 925 */
android.os.Binder .restoreCallingIdentity ( v0,v1 );
/* .line 923 */
/* .line 925 */
/* :catchall_0 */
/* move-exception v2 */
android.os.Binder .restoreCallingIdentity ( v0,v1 );
/* .line 926 */
/* throw v2 */
} // .end method
public java.lang.String getPlatformVAID ( ) {
/* .locals 3 */
/* .line 977 */
v0 = android.os.Binder .getCallingUid ( );
v0 = android.os.UserHandle .getAppId ( v0 );
/* const/16 v1, 0x2710 */
/* if-le v0, v1, :cond_0 */
/* .line 978 */
v0 = this.mContext;
final String v1 = "com.miui.securitycenter.permission.SYSTEM_PERMISSION_DECLARE"; // const-string v1, "com.miui.securitycenter.permission.SYSTEM_PERMISSION_DECLARE"
final String v2 = "Not allowed get platform vaid from other!"; // const-string v2, "Not allowed get platform vaid from other!"
(( android.content.Context ) v0 ).enforceCallingOrSelfPermission ( v1, v2 ); // invoke-virtual {v0, v1, v2}, Landroid/content/Context;->enforceCallingOrSelfPermission(Ljava/lang/String;Ljava/lang/String;)V
/* .line 980 */
} // :cond_0
android.os.Binder .clearCallingIdentity ( );
/* move-result-wide v0 */
/* .line 982 */
/* .local v0, "identity":J */
try { // :try_start_0
v2 = this.mPlatformVAID;
v2 = android.text.TextUtils .isEmpty ( v2 );
if ( v2 != null) { // if-eqz v2, :cond_1
/* .line 983 */
final String v2 = "/data/system/vaid_persistence_platform"; // const-string v2, "/data/system/vaid_persistence_platform"
(( com.miui.server.SecurityManagerService ) p0 ).readSystemDataStringFile ( v2 ); // invoke-virtual {p0, v2}, Lcom/miui/server/SecurityManagerService;->readSystemDataStringFile(Ljava/lang/String;)Ljava/lang/String;
this.mPlatformVAID = v2;
/* :try_end_0 */
/* .catchall {:try_start_0 ..:try_end_0} :catchall_0 */
/* .line 986 */
} // :cond_1
android.os.Binder .restoreCallingIdentity ( v0,v1 );
/* .line 987 */
/* nop */
/* .line 988 */
v2 = this.mPlatformVAID;
/* .line 986 */
/* :catchall_0 */
/* move-exception v2 */
android.os.Binder .restoreCallingIdentity ( v0,v1 );
/* .line 987 */
/* throw v2 */
} // .end method
public Integer getSecondSpaceId ( ) {
/* .locals 6 */
/* .line 669 */
android.os.Binder .clearCallingIdentity ( );
/* move-result-wide v0 */
/* .line 671 */
/* .local v0, "callingId":J */
try { // :try_start_0
v2 = this.mContext;
(( android.content.Context ) v2 ).getContentResolver ( ); // invoke-virtual {v2}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;
/* const-string/jumbo v3, "second_user_id" */
/* const/16 v4, -0x2710 */
int v5 = 0; // const/4 v5, 0x0
v2 = android.provider.Settings$Secure .getIntForUser ( v2,v3,v4,v5 );
/* :try_end_0 */
/* .catchall {:try_start_0 ..:try_end_0} :catchall_0 */
/* .line 674 */
android.os.Binder .restoreCallingIdentity ( v0,v1 );
/* .line 671 */
/* .line 674 */
/* :catchall_0 */
/* move-exception v2 */
android.os.Binder .restoreCallingIdentity ( v0,v1 );
/* .line 675 */
/* throw v2 */
} // .end method
public java.lang.String getShouldMaskApps ( ) {
/* .locals 1 */
/* .line 545 */
/* const/16 v0, 0x3e8 */
/* filled-new-array {v0}, [I */
/* invoke-direct {p0, v0}, Lcom/miui/server/SecurityManagerService;->checkPermissionByUid([I)V */
/* .line 546 */
v0 = this.mAccessControlImpl;
(( com.miui.server.security.AccessControlImpl ) v0 ).getShouldMaskApps ( ); // invoke-virtual {v0}, Lcom/miui/server/security/AccessControlImpl;->getShouldMaskApps()Ljava/lang/String;
} // .end method
public java.util.Map getSimulatedTouchInfo ( ) {
/* .locals 1 */
/* .annotation system Ldalvik/annotation/Signature; */
/* value = { */
/* "()", */
/* "Ljava/util/Map<", */
/* "Ljava/lang/String;", */
/* "Ljava/lang/String;", */
/* ">;" */
/* } */
} // .end annotation
/* .line 1391 */
/* const/16 v0, 0x3e8 */
/* filled-new-array {v0}, [I */
/* invoke-direct {p0, v0}, Lcom/miui/server/SecurityManagerService;->checkPermissionByUid([I)V */
/* .line 1392 */
com.miui.server.security.SafetyDetectManagerStub .getInstance ( );
(( com.miui.server.security.SafetyDetectManagerStub ) v0 ).getSimulatedTouchInfo ( ); // invoke-virtual {v0}, Lcom/miui/server/security/SafetyDetectManagerStub;->getSimulatedTouchInfo()Ljava/util/Map;
} // .end method
public android.os.IBinder getTopActivity ( ) {
/* .locals 6 */
/* .line 1051 */
/* invoke-direct {p0}, Lcom/miui/server/SecurityManagerService;->checkPermission()V */
/* .line 1052 */
com.android.server.wm.WindowProcessUtils .getTopRunningActivityInfo ( );
/* .line 1053 */
/* .local v0, "topActivity":Ljava/util/HashMap;, "Ljava/util/HashMap<Ljava/lang/String;Ljava/lang/Object;>;" */
if ( v0 != null) { // if-eqz v0, :cond_0
/* .line 1054 */
final String v1 = "intent"; // const-string v1, "intent"
(( java.util.HashMap ) v0 ).get ( v1 ); // invoke-virtual {v0, v1}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;
/* check-cast v1, Landroid/content/Intent; */
/* .line 1055 */
/* .local v1, "intent":Landroid/content/Intent; */
(( android.content.Intent ) v1 ).getComponent ( ); // invoke-virtual {v1}, Landroid/content/Intent;->getComponent()Landroid/content/ComponentName;
/* .line 1056 */
/* .local v2, "componentName":Landroid/content/ComponentName; */
if ( v2 != null) { // if-eqz v2, :cond_0
/* .line 1057 */
(( android.content.ComponentName ) v2 ).getClassName ( ); // invoke-virtual {v2}, Landroid/content/ComponentName;->getClassName()Ljava/lang/String;
/* .line 1058 */
/* .local v3, "clsName":Ljava/lang/String; */
(( android.content.ComponentName ) v2 ).getPackageName ( ); // invoke-virtual {v2}, Landroid/content/ComponentName;->getPackageName()Ljava/lang/String;
/* .line 1059 */
/* .local v4, "pkgName":Ljava/lang/String; */
final String v5 = "com.google.android.packageinstaller"; // const-string v5, "com.google.android.packageinstaller"
v5 = (( java.lang.String ) v5 ).equals ( v4 ); // invoke-virtual {v5, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z
if ( v5 != null) { // if-eqz v5, :cond_0
/* .line 1060 */
final String v5 = "com.android.packageinstaller.PackageInstallerActivity"; // const-string v5, "com.android.packageinstaller.PackageInstallerActivity"
v5 = (( java.lang.String ) v5 ).equals ( v3 ); // invoke-virtual {v5, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z
/* if-nez v5, :cond_0 */
/* .line 1061 */
/* const-string/jumbo v5, "token" */
(( java.util.HashMap ) v0 ).get ( v5 ); // invoke-virtual {v0, v5}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;
/* check-cast v5, Landroid/os/IBinder; */
/* .line 1066 */
} // .end local v1 # "intent":Landroid/content/Intent;
} // .end local v2 # "componentName":Landroid/content/ComponentName;
} // .end local v3 # "clsName":Ljava/lang/String;
} // .end local v4 # "pkgName":Ljava/lang/String;
} // :cond_0
int v1 = 0; // const/4 v1, 0x0
} // .end method
public com.miui.server.security.SecurityUserState getUserStateLocked ( Integer p0 ) {
/* .locals 2 */
/* .param p1, "userHandle" # I */
/* .line 1137 */
v0 = this.mUserStates;
(( android.util.SparseArray ) v0 ).get ( p1 ); // invoke-virtual {v0, p1}, Landroid/util/SparseArray;->get(I)Ljava/lang/Object;
/* check-cast v0, Lcom/miui/server/security/SecurityUserState; */
/* .line 1138 */
/* .local v0, "userState":Lcom/miui/server/security/SecurityUserState; */
/* if-nez v0, :cond_0 */
/* .line 1139 */
/* new-instance v1, Lcom/miui/server/security/SecurityUserState; */
/* invoke-direct {v1}, Lcom/miui/server/security/SecurityUserState;-><init>()V */
/* move-object v0, v1 */
/* .line 1140 */
/* iput p1, v0, Lcom/miui/server/security/SecurityUserState;->userHandle:I */
/* .line 1141 */
v1 = this.mUserStates;
(( android.util.SparseArray ) v1 ).put ( p1, v0 ); // invoke-virtual {v1, p1, v0}, Landroid/util/SparseArray;->put(ILjava/lang/Object;)V
/* .line 1143 */
} // :cond_0
} // .end method
public android.content.pm.ParceledListSlice getWakePathCallListLog ( ) {
/* .locals 1 */
/* .line 649 */
/* invoke-direct {p0}, Lcom/miui/server/SecurityManagerService;->checkWakePathPermission()V */
/* .line 650 */
miui.security.WakePathChecker .getInstance ( );
(( miui.security.WakePathChecker ) v0 ).getWakePathCallListLog ( ); // invoke-virtual {v0}, Lmiui/security/WakePathChecker;->getWakePathCallListLog()Landroid/content/pm/ParceledListSlice;
} // .end method
public Long getWakeUpTime ( java.lang.String p0 ) {
/* .locals 3 */
/* .param p1, "componentName" # Ljava/lang/String; */
/* .line 599 */
v0 = this.mContext;
final String v1 = "com.miui.permission.MANAGE_BOOT_TIME"; // const-string v1, "com.miui.permission.MANAGE_BOOT_TIME"
final String v2 = "SecurityManagerService"; // const-string v2, "SecurityManagerService"
(( android.content.Context ) v0 ).enforceCallingOrSelfPermission ( v1, v2 ); // invoke-virtual {v0, v1, v2}, Landroid/content/Context;->enforceCallingOrSelfPermission(Ljava/lang/String;Ljava/lang/String;)V
/* .line 600 */
v0 = this.mWakeUpTimeImpl;
(( com.miui.server.security.WakeUpTimeImpl ) v0 ).getBootTimeFromMap ( p1 ); // invoke-virtual {v0, p1}, Lcom/miui/server/security/WakeUpTimeImpl;->getBootTimeFromMap(Ljava/lang/String;)J
/* move-result-wide v0 */
/* return-wide v0 */
} // .end method
public void grantRuntimePermissionAsUser ( java.lang.String p0, java.lang.String p1, Integer p2 ) {
/* .locals 3 */
/* .param p1, "packageName" # Ljava/lang/String; */
/* .param p2, "permName" # Ljava/lang/String; */
/* .param p3, "userId" # I */
/* .line 880 */
final String v0 = "com.lbe.security.miui"; // const-string v0, "com.lbe.security.miui"
/* invoke-direct {p0, v0}, Lcom/miui/server/SecurityManagerService;->checkGrantPermissionPkg(Ljava/lang/String;)V */
/* .line 881 */
v0 = android.permission.PermissionManager .checkPackageNamePermission ( p2,p1,p3 );
/* if-nez v0, :cond_0 */
/* .line 882 */
return;
/* .line 883 */
} // :cond_0
android.os.Binder .clearCallingIdentity ( );
/* move-result-wide v0 */
/* .line 885 */
/* .local v0, "identity":J */
try { // :try_start_0
v2 = this.mPermissionManagerService;
(( com.android.server.pm.permission.PermissionManagerService ) v2 ).grantRuntimePermission ( p1, p2, p3 ); // invoke-virtual {v2, p1, p2, p3}, Lcom/android/server/pm/permission/PermissionManagerService;->grantRuntimePermission(Ljava/lang/String;Ljava/lang/String;I)V
/* :try_end_0 */
/* .catchall {:try_start_0 ..:try_end_0} :catchall_0 */
/* .line 887 */
android.os.Binder .restoreCallingIdentity ( v0,v1 );
/* .line 888 */
/* nop */
/* .line 889 */
return;
/* .line 887 */
/* :catchall_0 */
/* move-exception v2 */
android.os.Binder .restoreCallingIdentity ( v0,v1 );
/* .line 888 */
/* throw v2 */
} // .end method
public Boolean hasAppBehaviorWatching ( ) {
/* .locals 1 */
/* .line 1326 */
final String v0 = "com.miui.securitycenter"; // const-string v0, "com.miui.securitycenter"
/* invoke-direct {p0, v0}, Lcom/miui/server/SecurityManagerService;->checkGrantPermissionPkg(Ljava/lang/String;)V */
/* .line 1327 */
v0 = this.mAppBehavior;
v0 = (( com.miui.server.security.AppBehaviorService ) v0 ).hasAppBehaviorWatching ( ); // invoke-virtual {v0}, Lcom/miui/server/security/AppBehaviorService;->hasAppBehaviorWatching()Z
} // .end method
public Boolean haveAccessControlPassword ( Integer p0 ) {
/* .locals 1 */
/* .param p1, "userId" # I */
/* .line 512 */
p1 = miui.security.SecurityManager .getUserHandle ( p1 );
/* .line 513 */
v0 = this.mAccessController;
v0 = (( com.miui.server.AccessController ) v0 ).haveAccessControlPassword ( p1 ); // invoke-virtual {v0, p1}, Lcom/miui/server/AccessController;->haveAccessControlPassword(I)Z
} // .end method
public Boolean isAllowStartService ( android.content.Intent p0, Integer p1 ) {
/* .locals 2 */
/* .param p1, "service" # Landroid/content/Intent; */
/* .param p2, "userId" # I */
/* .line 1044 */
/* invoke-direct {p0}, Lcom/miui/server/SecurityManagerService;->checkPermission()V */
/* .line 1045 */
com.android.server.am.AutoStartManagerServiceStub .getInstance ( );
v1 = this.mContext;
v0 = /* .line 1046 */
/* .line 1045 */
} // .end method
public Boolean isAppPrivacyEnabled ( java.lang.String p0 ) {
/* .locals 2 */
/* .param p1, "packageName" # Ljava/lang/String; */
/* .line 753 */
v0 = android.text.TextUtils .isEmpty ( p1 );
/* if-nez v0, :cond_0 */
/* .line 756 */
v0 = java.lang.Boolean.TRUE;
/* new-instance v1, Lcom/miui/server/SecurityManagerService$$ExternalSyntheticLambda2; */
/* invoke-direct {v1, p0, p1}, Lcom/miui/server/SecurityManagerService$$ExternalSyntheticLambda2;-><init>(Lcom/miui/server/SecurityManagerService;Ljava/lang/String;)V */
android.os.Binder .withCleanCallingIdentity ( v1 );
v0 = (( java.lang.Boolean ) v0 ).equals ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/Boolean;->equals(Ljava/lang/Object;)Z
/* .line 754 */
} // :cond_0
/* new-instance v0, Ljava/lang/RuntimeException; */
final String v1 = "packageName can not be null or empty"; // const-string v1, "packageName can not be null or empty"
/* invoke-direct {v0, v1}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V */
/* throw v0 */
} // .end method
public Boolean isGameStorageApp ( java.lang.String p0, Integer p1 ) {
/* .locals 1 */
/* .param p1, "packageName" # Ljava/lang/String; */
/* .param p2, "userId" # I */
/* .line 301 */
/* invoke-direct {p0}, Lcom/miui/server/SecurityManagerService;->checkPermission()V */
/* .line 302 */
v0 = this.mGameBoosterImpl;
v0 = (( com.miui.server.security.GameBoosterImpl ) v0 ).isGameStorageApp ( p1, p2 ); // invoke-virtual {v0, p1, p2}, Lcom/miui/server/security/GameBoosterImpl;->isGameStorageApp(Ljava/lang/String;I)Z
} // .end method
public Boolean isPrivacyApp ( java.lang.String p0, Integer p1 ) {
/* .locals 5 */
/* .param p1, "packageName" # Ljava/lang/String; */
/* .param p2, "userId" # I */
/* .line 777 */
/* invoke-direct {p0}, Lcom/miui/server/SecurityManagerService;->checkPermission()V */
/* .line 778 */
v0 = this.mUserStateLock;
/* monitor-enter v0 */
/* .line 779 */
try { // :try_start_0
(( com.miui.server.SecurityManagerService ) p0 ).getUserStateLocked ( p2 ); // invoke-virtual {p0, p2}, Lcom/miui/server/SecurityManagerService;->getUserStateLocked(I)Lcom/miui/server/security/SecurityUserState;
/* :try_end_0 */
/* .catchall {:try_start_0 ..:try_end_0} :catchall_0 */
/* .line 781 */
/* .local v1, "userState":Lcom/miui/server/security/SecurityUserState; */
try { // :try_start_1
v2 = this.mPackages;
(( com.miui.server.SecurityManagerService ) p0 ).getPackageSetting ( v2, p1 ); // invoke-virtual {p0, v2, p1}, Lcom/miui/server/SecurityManagerService;->getPackageSetting(Ljava/util/HashMap;Ljava/lang/String;)Lcom/miui/server/security/SecurityPackageSettings;
/* .line 782 */
/* .local v2, "ps":Lcom/miui/server/security/SecurityPackageSettings; */
/* iget-boolean v3, v2, Lcom/miui/server/security/SecurityPackageSettings;->isPrivacyApp:Z */
/* :try_end_1 */
/* .catch Ljava/lang/Exception; {:try_start_1 ..:try_end_1} :catch_0 */
/* .catchall {:try_start_1 ..:try_end_1} :catchall_0 */
try { // :try_start_2
/* monitor-exit v0 */
/* .line 783 */
} // .end local v2 # "ps":Lcom/miui/server/security/SecurityPackageSettings;
/* :catch_0 */
/* move-exception v2 */
/* .line 784 */
/* .local v2, "e":Ljava/lang/Exception; */
final String v3 = "SecurityManagerService"; // const-string v3, "SecurityManagerService"
final String v4 = "isPrivacyApp error"; // const-string v4, "isPrivacyApp error"
android.util.Log .e ( v3,v4,v2 );
/* .line 785 */
/* monitor-exit v0 */
int v0 = 0; // const/4 v0, 0x0
/* .line 787 */
} // .end local v1 # "userState":Lcom/miui/server/security/SecurityUserState;
} // .end local v2 # "e":Ljava/lang/Exception;
/* :catchall_0 */
/* move-exception v1 */
/* monitor-exit v0 */
/* :try_end_2 */
/* .catchall {:try_start_2 ..:try_end_2} :catchall_0 */
/* throw v1 */
} // .end method
public Boolean isScRelaunchNeedConfirm ( java.lang.String p0 ) {
/* .locals 2 */
/* .param p1, "packageName" # Ljava/lang/String; */
/* .line 398 */
v0 = android.os.UserHandle .getCallingUserId ( );
/* .line 399 */
/* .local v0, "userId":I */
v1 = (( com.miui.server.SecurityManagerService ) p0 ).isScRelaunchNeedConfirmForUser ( p1, v0 ); // invoke-virtual {p0, p1, v0}, Lcom/miui/server/SecurityManagerService;->isScRelaunchNeedConfirmForUser(Ljava/lang/String;I)Z
} // .end method
public Boolean isScRelaunchNeedConfirmForUser ( java.lang.String p0, Integer p1 ) {
/* .locals 4 */
/* .param p1, "packageName" # Ljava/lang/String; */
/* .param p2, "userId" # I */
/* .line 404 */
v0 = this.mUserStateLock;
/* monitor-enter v0 */
/* .line 405 */
try { // :try_start_0
(( com.miui.server.SecurityManagerService ) p0 ).getUserStateLocked ( p2 ); // invoke-virtual {p0, p2}, Lcom/miui/server/SecurityManagerService;->getUserStateLocked(I)Lcom/miui/server/security/SecurityUserState;
/* :try_end_0 */
/* .catchall {:try_start_0 ..:try_end_0} :catchall_0 */
/* .line 407 */
/* .local v1, "userState":Lcom/miui/server/security/SecurityUserState; */
try { // :try_start_1
v2 = this.mPackages;
(( com.miui.server.SecurityManagerService ) p0 ).getPackageSetting ( v2, p1 ); // invoke-virtual {p0, v2, p1}, Lcom/miui/server/SecurityManagerService;->getPackageSetting(Ljava/util/HashMap;Ljava/lang/String;)Lcom/miui/server/security/SecurityPackageSettings;
/* .line 408 */
/* .local v2, "ps":Lcom/miui/server/security/SecurityPackageSettings; */
/* iget-boolean v3, v2, Lcom/miui/server/security/SecurityPackageSettings;->isScRelaunchConfirm:Z */
/* :try_end_1 */
/* .catch Ljava/lang/Exception; {:try_start_1 ..:try_end_1} :catch_0 */
/* .catchall {:try_start_1 ..:try_end_1} :catchall_0 */
try { // :try_start_2
/* monitor-exit v0 */
/* .line 409 */
} // .end local v2 # "ps":Lcom/miui/server/security/SecurityPackageSettings;
/* :catch_0 */
/* move-exception v2 */
/* .line 410 */
/* .local v2, "e":Ljava/lang/Exception; */
/* monitor-exit v0 */
int v0 = 0; // const/4 v0, 0x0
/* .line 412 */
} // .end local v1 # "userState":Lcom/miui/server/security/SecurityUserState;
} // .end local v2 # "e":Ljava/lang/Exception;
/* :catchall_0 */
/* move-exception v1 */
/* monitor-exit v0 */
/* :try_end_2 */
/* .catchall {:try_start_2 ..:try_end_2} :catchall_0 */
/* throw v1 */
} // .end method
public Boolean needFinishAccessControl ( android.os.IBinder p0 ) {
/* .locals 1 */
/* .param p1, "token" # Landroid/os/IBinder; */
/* .line 524 */
/* invoke-direct {p0}, Lcom/miui/server/SecurityManagerService;->checkPermission()V */
/* .line 525 */
v0 = this.mAccessControlImpl;
v0 = (( com.miui.server.security.AccessControlImpl ) v0 ).needFinishAccessControl ( p1 ); // invoke-virtual {v0, p1}, Lcom/miui/server/security/AccessControlImpl;->needFinishAccessControl(Landroid/os/IBinder;)Z
} // .end method
public void persistAppBehavior ( ) {
/* .locals 1 */
/* .line 1314 */
final String v0 = "com.miui.securitycenter"; // const-string v0, "com.miui.securitycenter"
/* invoke-direct {p0, v0}, Lcom/miui/server/SecurityManagerService;->checkGrantPermissionPkg(Ljava/lang/String;)V */
/* .line 1315 */
v0 = this.mAppBehavior;
(( com.miui.server.security.AppBehaviorService ) v0 ).persistBehaviorRecord ( ); // invoke-virtual {v0}, Lcom/miui/server/security/AppBehaviorService;->persistBehaviorRecord()V
/* .line 1316 */
return;
} // .end method
public void pushPrivacyVirtualDisplayList ( java.util.List p0 ) {
/* .locals 2 */
/* .annotation system Ldalvik/annotation/Signature; */
/* value = { */
/* "(", */
/* "Ljava/util/List<", */
/* "Ljava/lang/String;", */
/* ">;)V" */
/* } */
} // .end annotation
/* .line 996 */
/* .local p1, "privacyList":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;" */
/* invoke-direct {p0}, Lcom/miui/server/SecurityManagerService;->checkPermission()V */
/* .line 997 */
v0 = this.mPrivacyVirtualDisplay;
/* .line 998 */
v0 = this.mPrivacyVirtualDisplay;
/* .line 999 */
/* new-instance v0, Ljava/lang/StringBuilder; */
/* invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V */
final String v1 = "privacy virtual display updated! size="; // const-string v1, "privacy virtual display updated! size="
(( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
v1 = v1 = this.mPrivacyVirtualDisplay;
(( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v0 ).toString ( ); // invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
final String v1 = "SecurityManagerService"; // const-string v1, "SecurityManagerService"
android.util.Log .i ( v1,v0 );
/* .line 1000 */
return;
} // .end method
public void pushUpdatePkgsData ( java.util.List p0, Boolean p1 ) {
/* .locals 1 */
/* .param p2, "enable" # Z */
/* .annotation system Ldalvik/annotation/Signature; */
/* value = { */
/* "(", */
/* "Ljava/util/List<", */
/* "Ljava/lang/String;", */
/* ">;Z)V" */
/* } */
} // .end annotation
/* .line 613 */
/* .local p1, "updatePkgsList":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;" */
/* invoke-direct {p0}, Lcom/miui/server/SecurityManagerService;->checkWakePathPermission()V */
/* .line 614 */
miui.security.WakePathChecker .getInstance ( );
(( miui.security.WakePathChecker ) v0 ).pushUpdatePkgsData ( p1, p2 ); // invoke-virtual {v0, p1, p2}, Lmiui/security/WakePathChecker;->pushUpdatePkgsData(Ljava/util/List;Z)V
/* .line 615 */
return;
} // .end method
public void pushWakePathConfirmDialogWhiteList ( Integer p0, java.util.List p1 ) {
/* .locals 1 */
/* .param p1, "type" # I */
/* .annotation system Ldalvik/annotation/Signature; */
/* value = { */
/* "(I", */
/* "Ljava/util/List<", */
/* "Ljava/lang/String;", */
/* ">;)V" */
/* } */
} // .end annotation
/* .line 631 */
/* .local p2, "whiteList":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;" */
/* invoke-direct {p0}, Lcom/miui/server/SecurityManagerService;->checkWakePathPermission()V */
/* .line 632 */
miui.security.WakePathChecker .getInstance ( );
(( miui.security.WakePathChecker ) v0 ).pushWakePathConfirmDialogWhiteList ( p1, p2 ); // invoke-virtual {v0, p1, p2}, Lmiui/security/WakePathChecker;->pushWakePathConfirmDialogWhiteList(ILjava/util/List;)V
/* .line 633 */
return;
} // .end method
public void pushWakePathData ( Integer p0, android.content.pm.ParceledListSlice p1, Integer p2 ) {
/* .locals 2 */
/* .param p1, "wakeType" # I */
/* .param p2, "wakePathRuleInfos" # Landroid/content/pm/ParceledListSlice; */
/* .param p3, "userId" # I */
/* .line 619 */
/* invoke-direct {p0}, Lcom/miui/server/SecurityManagerService;->checkWakePathPermission()V */
/* .line 620 */
miui.security.WakePathChecker .getInstance ( );
(( android.content.pm.ParceledListSlice ) p2 ).getList ( ); // invoke-virtual {p2}, Landroid/content/pm/ParceledListSlice;->getList()Ljava/util/List;
(( miui.security.WakePathChecker ) v0 ).pushWakePathRuleInfos ( p1, v1, p3 ); // invoke-virtual {v0, p1, v1, p3}, Lmiui/security/WakePathChecker;->pushWakePathRuleInfos(ILjava/util/List;I)V
/* .line 621 */
return;
} // .end method
public void pushWakePathWhiteList ( java.util.List p0, Integer p1 ) {
/* .locals 1 */
/* .param p2, "userId" # I */
/* .annotation system Ldalvik/annotation/Signature; */
/* value = { */
/* "(", */
/* "Ljava/util/List<", */
/* "Ljava/lang/String;", */
/* ">;I)V" */
/* } */
} // .end annotation
/* .line 625 */
/* .local p1, "wakePathWhiteList":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;" */
/* invoke-direct {p0}, Lcom/miui/server/SecurityManagerService;->checkWakePathPermission()V */
/* .line 626 */
miui.security.WakePathChecker .getInstance ( );
(( miui.security.WakePathChecker ) v0 ).pushWakePathWhiteList ( p1, p2 ); // invoke-virtual {v0, p1, p2}, Lmiui/security/WakePathChecker;->pushWakePathWhiteList(Ljava/util/List;I)V
/* .line 627 */
return;
} // .end method
public Boolean putSystemDataStringFile ( java.lang.String p0, java.lang.String p1 ) {
/* .locals 4 */
/* .param p1, "path" # Ljava/lang/String; */
/* .param p2, "value" # Ljava/lang/String; */
/* .line 1078 */
/* const/16 v0, 0x3e8 */
/* filled-new-array {v0}, [I */
/* invoke-direct {p0, v0}, Lcom/miui/server/SecurityManagerService;->checkPermissionByUid([I)V */
/* .line 1079 */
/* new-instance v0, Ljava/io/File; */
/* invoke-direct {v0, p1}, Ljava/io/File;-><init>(Ljava/lang/String;)V */
/* .line 1080 */
/* .local v0, "file":Ljava/io/File; */
int v1 = 0; // const/4 v1, 0x0
/* .line 1081 */
/* .local v1, "raf":Ljava/io/RandomAccessFile; */
v2 = (( java.io.File ) v0 ).exists ( ); // invoke-virtual {v0}, Ljava/io/File;->exists()Z
/* if-nez v2, :cond_0 */
/* .line 1083 */
try { // :try_start_0
(( java.io.File ) v0 ).createNewFile ( ); // invoke-virtual {v0}, Ljava/io/File;->createNewFile()Z
/* :try_end_0 */
/* .catch Ljava/io/IOException; {:try_start_0 ..:try_end_0} :catch_0 */
/* .line 1086 */
/* .line 1084 */
/* :catch_0 */
/* move-exception v2 */
/* .line 1085 */
/* .local v2, "e":Ljava/io/IOException; */
(( java.io.IOException ) v2 ).printStackTrace ( ); // invoke-virtual {v2}, Ljava/io/IOException;->printStackTrace()V
/* .line 1089 */
} // .end local v2 # "e":Ljava/io/IOException;
} // :cond_0
} // :goto_0
try { // :try_start_1
/* new-instance v2, Ljava/io/RandomAccessFile; */
final String v3 = "rw"; // const-string v3, "rw"
/* invoke-direct {v2, v0, v3}, Ljava/io/RandomAccessFile;-><init>(Ljava/io/File;Ljava/lang/String;)V */
/* move-object v1, v2 */
/* .line 1090 */
/* const-wide/16 v2, 0x0 */
(( java.io.RandomAccessFile ) v1 ).setLength ( v2, v3 ); // invoke-virtual {v1, v2, v3}, Ljava/io/RandomAccessFile;->setLength(J)V
/* .line 1091 */
(( java.io.RandomAccessFile ) v1 ).writeUTF ( p2 ); // invoke-virtual {v1, p2}, Ljava/io/RandomAccessFile;->writeUTF(Ljava/lang/String;)V
/* :try_end_1 */
/* .catch Ljava/io/IOException; {:try_start_1 ..:try_end_1} :catch_2 */
/* .catchall {:try_start_1 ..:try_end_1} :catchall_0 */
/* .line 1092 */
/* nop */
/* .line 1096 */
/* nop */
/* .line 1098 */
try { // :try_start_2
(( java.io.RandomAccessFile ) v1 ).close ( ); // invoke-virtual {v1}, Ljava/io/RandomAccessFile;->close()V
/* :try_end_2 */
/* .catch Ljava/io/IOException; {:try_start_2 ..:try_end_2} :catch_1 */
/* .line 1101 */
/* .line 1099 */
/* :catch_1 */
/* move-exception v2 */
/* .line 1100 */
/* .restart local v2 # "e":Ljava/io/IOException; */
(( java.io.IOException ) v2 ).printStackTrace ( ); // invoke-virtual {v2}, Ljava/io/IOException;->printStackTrace()V
/* .line 1092 */
} // .end local v2 # "e":Ljava/io/IOException;
} // :goto_1
int v2 = 1; // const/4 v2, 0x1
/* .line 1096 */
/* :catchall_0 */
/* move-exception v2 */
/* .line 1093 */
/* :catch_2 */
/* move-exception v2 */
/* .line 1094 */
/* .restart local v2 # "e":Ljava/io/IOException; */
try { // :try_start_3
(( java.io.IOException ) v2 ).printStackTrace ( ); // invoke-virtual {v2}, Ljava/io/IOException;->printStackTrace()V
/* :try_end_3 */
/* .catchall {:try_start_3 ..:try_end_3} :catchall_0 */
/* .line 1096 */
} // .end local v2 # "e":Ljava/io/IOException;
if ( v1 != null) { // if-eqz v1, :cond_1
/* .line 1098 */
try { // :try_start_4
(( java.io.RandomAccessFile ) v1 ).close ( ); // invoke-virtual {v1}, Ljava/io/RandomAccessFile;->close()V
/* :try_end_4 */
/* .catch Ljava/io/IOException; {:try_start_4 ..:try_end_4} :catch_3 */
/* .line 1101 */
} // :goto_2
/* .line 1099 */
/* :catch_3 */
/* move-exception v2 */
/* .line 1100 */
/* .restart local v2 # "e":Ljava/io/IOException; */
(( java.io.IOException ) v2 ).printStackTrace ( ); // invoke-virtual {v2}, Ljava/io/IOException;->printStackTrace()V
} // .end local v2 # "e":Ljava/io/IOException;
/* .line 1104 */
} // :cond_1
} // :goto_3
int v2 = 0; // const/4 v2, 0x0
/* .line 1096 */
} // :goto_4
if ( v1 != null) { // if-eqz v1, :cond_2
/* .line 1098 */
try { // :try_start_5
(( java.io.RandomAccessFile ) v1 ).close ( ); // invoke-virtual {v1}, Ljava/io/RandomAccessFile;->close()V
/* :try_end_5 */
/* .catch Ljava/io/IOException; {:try_start_5 ..:try_end_5} :catch_4 */
/* .line 1101 */
/* .line 1099 */
/* :catch_4 */
/* move-exception v3 */
/* .line 1100 */
/* .local v3, "e":Ljava/io/IOException; */
(( java.io.IOException ) v3 ).printStackTrace ( ); // invoke-virtual {v3}, Ljava/io/IOException;->printStackTrace()V
/* .line 1103 */
} // .end local v3 # "e":Ljava/io/IOException;
} // :cond_2
} // :goto_5
/* throw v2 */
} // .end method
public java.lang.String readSystemDataStringFile ( java.lang.String p0 ) {
/* .locals 5 */
/* .param p1, "path" # Ljava/lang/String; */
/* .line 1110 */
/* const/16 v0, 0x3e8 */
/* filled-new-array {v0}, [I */
/* invoke-direct {p0, v0}, Lcom/miui/server/SecurityManagerService;->checkPermissionByUid([I)V */
/* .line 1111 */
/* new-instance v0, Ljava/io/File; */
/* invoke-direct {v0, p1}, Ljava/io/File;-><init>(Ljava/lang/String;)V */
/* .line 1112 */
/* .local v0, "file":Ljava/io/File; */
int v1 = 0; // const/4 v1, 0x0
/* .line 1113 */
/* .local v1, "raf":Ljava/io/RandomAccessFile; */
int v2 = 0; // const/4 v2, 0x0
/* .line 1114 */
/* .local v2, "result":Ljava/lang/String; */
v3 = (( java.io.File ) v0 ).exists ( ); // invoke-virtual {v0}, Ljava/io/File;->exists()Z
if ( v3 != null) { // if-eqz v3, :cond_1
/* .line 1116 */
try { // :try_start_0
/* new-instance v3, Ljava/io/RandomAccessFile; */
final String v4 = "r"; // const-string v4, "r"
/* invoke-direct {v3, v0, v4}, Ljava/io/RandomAccessFile;-><init>(Ljava/io/File;Ljava/lang/String;)V */
/* move-object v1, v3 */
/* .line 1117 */
(( java.io.RandomAccessFile ) v1 ).readUTF ( ); // invoke-virtual {v1}, Ljava/io/RandomAccessFile;->readUTF()Ljava/lang/String;
/* :try_end_0 */
/* .catch Ljava/io/IOException; {:try_start_0 ..:try_end_0} :catch_1 */
/* .catchall {:try_start_0 ..:try_end_0} :catchall_0 */
/* move-object v2, v3 */
/* .line 1121 */
/* nop */
/* .line 1123 */
try { // :try_start_1
(( java.io.RandomAccessFile ) v1 ).close ( ); // invoke-virtual {v1}, Ljava/io/RandomAccessFile;->close()V
/* :try_end_1 */
/* .catch Ljava/io/IOException; {:try_start_1 ..:try_end_1} :catch_0 */
/* .line 1126 */
} // :goto_0
/* .line 1124 */
/* :catch_0 */
/* move-exception v3 */
/* .line 1125 */
/* .local v3, "e":Ljava/io/IOException; */
(( java.io.IOException ) v3 ).printStackTrace ( ); // invoke-virtual {v3}, Ljava/io/IOException;->printStackTrace()V
} // .end local v3 # "e":Ljava/io/IOException;
/* .line 1121 */
/* :catchall_0 */
/* move-exception v3 */
/* .line 1118 */
/* :catch_1 */
/* move-exception v3 */
/* .line 1119 */
/* .restart local v3 # "e":Ljava/io/IOException; */
try { // :try_start_2
(( java.io.IOException ) v3 ).printStackTrace ( ); // invoke-virtual {v3}, Ljava/io/IOException;->printStackTrace()V
/* :try_end_2 */
/* .catchall {:try_start_2 ..:try_end_2} :catchall_0 */
/* .line 1121 */
} // .end local v3 # "e":Ljava/io/IOException;
if ( v1 != null) { // if-eqz v1, :cond_1
/* .line 1123 */
try { // :try_start_3
(( java.io.RandomAccessFile ) v1 ).close ( ); // invoke-virtual {v1}, Ljava/io/RandomAccessFile;->close()V
/* :try_end_3 */
/* .catch Ljava/io/IOException; {:try_start_3 ..:try_end_3} :catch_0 */
/* .line 1121 */
} // :goto_1
if ( v1 != null) { // if-eqz v1, :cond_0
/* .line 1123 */
try { // :try_start_4
(( java.io.RandomAccessFile ) v1 ).close ( ); // invoke-virtual {v1}, Ljava/io/RandomAccessFile;->close()V
/* :try_end_4 */
/* .catch Ljava/io/IOException; {:try_start_4 ..:try_end_4} :catch_2 */
/* .line 1126 */
/* .line 1124 */
/* :catch_2 */
/* move-exception v4 */
/* .line 1125 */
/* .local v4, "e":Ljava/io/IOException; */
(( java.io.IOException ) v4 ).printStackTrace ( ); // invoke-virtual {v4}, Ljava/io/IOException;->printStackTrace()V
/* .line 1128 */
} // .end local v4 # "e":Ljava/io/IOException;
} // :cond_0
} // :goto_2
/* throw v3 */
/* .line 1130 */
} // :cond_1
} // :goto_3
} // .end method
public void recordAppBehavior ( miui.security.AppBehavior p0 ) {
/* .locals 1 */
/* .param p1, "appBehavior" # Lmiui/security/AppBehavior; */
/* .line 1306 */
final String v0 = "com.miui.securitycenter"; // const-string v0, "com.miui.securitycenter"
/* invoke-direct {p0, v0}, Lcom/miui/server/SecurityManagerService;->checkGrantPermissionPkg(Ljava/lang/String;)V */
/* .line 1307 */
v0 = this.mAppBehavior;
v0 = (( com.miui.server.security.AppBehaviorService ) v0 ).hasAppBehaviorWatching ( ); // invoke-virtual {v0}, Lcom/miui/server/security/AppBehaviorService;->hasAppBehaviorWatching()Z
if ( v0 != null) { // if-eqz v0, :cond_0
/* .line 1308 */
v0 = this.mAppBehavior;
(( com.miui.server.security.AppBehaviorService ) v0 ).recordAppBehaviorAsync ( p1 ); // invoke-virtual {v0, p1}, Lcom/miui/server/security/AppBehaviorService;->recordAppBehaviorAsync(Lmiui/security/AppBehavior;)V
/* .line 1310 */
} // :cond_0
return;
} // .end method
public void registerLocationBlurryManager ( com.android.internal.app.ILocationBlurry p0 ) {
/* .locals 1 */
/* .param p1, "manager" # Lcom/android/internal/app/ILocationBlurry; */
/* .line 949 */
/* invoke-direct {p0}, Lcom/miui/server/SecurityManagerService;->checkWakePathPermission()V */
/* .line 950 */
com.android.server.location.MiuiBlurLocationManagerStub .get ( );
(( com.android.server.location.MiuiBlurLocationManagerStub ) v0 ).registerLocationBlurryManager ( p1 ); // invoke-virtual {v0, p1}, Lcom/android/server/location/MiuiBlurLocationManagerStub;->registerLocationBlurryManager(Lcom/android/internal/app/ILocationBlurry;)V
/* .line 951 */
return;
} // .end method
public void registerWakePathCallback ( com.android.internal.app.IWakePathCallback p0 ) {
/* .locals 1 */
/* .param p1, "callback" # Lcom/android/internal/app/IWakePathCallback; */
/* .line 655 */
/* invoke-direct {p0}, Lcom/miui/server/SecurityManagerService;->checkWakePathPermission()V */
/* .line 656 */
miui.security.WakePathChecker .getInstance ( );
(( miui.security.WakePathChecker ) v0 ).registerWakePathCallback ( p1 ); // invoke-virtual {v0, p1}, Lmiui/security/WakePathChecker;->registerWakePathCallback(Lcom/android/internal/app/IWakePathCallback;)V
/* .line 657 */
return;
} // .end method
public void removeAccessControlPass ( java.lang.String p0 ) {
/* .locals 2 */
/* .param p1, "packageName" # Ljava/lang/String; */
/* .line 442 */
/* invoke-direct {p0}, Lcom/miui/server/SecurityManagerService;->checkPermission()V */
/* .line 443 */
v0 = android.os.UserHandle .getCallingUserId ( );
/* .line 444 */
/* .local v0, "callingUserId":I */
v1 = this.mAccessControlImpl;
(( com.miui.server.security.AccessControlImpl ) v1 ).removeAccessControlPassAsUser ( p1, v0 ); // invoke-virtual {v1, p1, v0}, Lcom/miui/server/security/AccessControlImpl;->removeAccessControlPassAsUser(Ljava/lang/String;I)V
/* .line 445 */
return;
} // .end method
public void removeAccessControlPassAsUser ( java.lang.String p0, Integer p1 ) {
/* .locals 1 */
/* .param p1, "packageName" # Ljava/lang/String; */
/* .param p2, "userId" # I */
/* .line 492 */
/* invoke-direct {p0}, Lcom/miui/server/SecurityManagerService;->checkPermission()V */
/* .line 493 */
v0 = this.mAccessControlImpl;
(( com.miui.server.security.AccessControlImpl ) v0 ).removeAccessControlPassAsUser ( p1, p2 ); // invoke-virtual {v0, p1, p2}, Lcom/miui/server/security/AccessControlImpl;->removeAccessControlPassAsUser(Ljava/lang/String;I)V
/* .line 494 */
return;
} // .end method
public void removeCalleeRestrictChain ( java.lang.String p0 ) {
/* .locals 5 */
/* .param p1, "callee" # Ljava/lang/String; */
/* .line 1365 */
v0 = android.text.TextUtils .isEmpty ( p1 );
if ( v0 != null) { // if-eqz v0, :cond_0
/* .line 1366 */
return;
/* .line 1368 */
} // :cond_0
v0 = this.restrictChainMaps;
(( java.util.LinkedHashMap ) v0 ).entrySet ( ); // invoke-virtual {v0}, Ljava/util/LinkedHashMap;->entrySet()Ljava/util/Set;
v1 = } // :goto_0
if ( v1 != null) { // if-eqz v1, :cond_3
/* check-cast v1, Ljava/util/Map$Entry; */
/* .line 1369 */
/* .local v1, "next":Ljava/util/Map$Entry;, "Ljava/util/Map$Entry<Ljava/lang/String;Ljava/util/List<Ljava/lang/String;>;>;" */
/* check-cast v2, Ljava/util/List; */
/* .line 1370 */
/* .local v2, "callees":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;" */
int v3 = 0; // const/4 v3, 0x0
/* .local v3, "i":I */
v4 = } // :goto_1
/* if-ge v3, v4, :cond_2 */
/* .line 1371 */
v4 = (( java.lang.String ) p1 ).equals ( v4 ); // invoke-virtual {p1, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z
if ( v4 != null) { // if-eqz v4, :cond_1
/* .line 1372 */
/* .line 1373 */
/* .line 1370 */
} // :cond_1
/* add-int/lit8 v3, v3, 0x1 */
/* .line 1376 */
} // .end local v1 # "next":Ljava/util/Map$Entry;, "Ljava/util/Map$Entry<Ljava/lang/String;Ljava/util/List<Ljava/lang/String;>;>;"
} // .end local v2 # "callees":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;"
} // .end local v3 # "i":I
} // :cond_2
} // :goto_2
/* .line 1377 */
} // :cond_3
return;
} // .end method
public void removeWakePathData ( Integer p0 ) {
/* .locals 1 */
/* .param p1, "userId" # I */
/* .line 637 */
/* invoke-direct {p0}, Lcom/miui/server/SecurityManagerService;->checkWakePathPermission()V */
/* .line 638 */
miui.security.WakePathChecker .getInstance ( );
(( miui.security.WakePathChecker ) v0 ).removeWakePathData ( p1 ); // invoke-virtual {v0, p1}, Lmiui/security/WakePathChecker;->removeWakePathData(I)V
/* .line 639 */
return;
} // .end method
public void revokeRuntimePermissionAsUser ( java.lang.String p0, java.lang.String p1, Integer p2 ) {
/* .locals 4 */
/* .param p1, "packageName" # Ljava/lang/String; */
/* .param p2, "permName" # Ljava/lang/String; */
/* .param p3, "userId" # I */
/* .line 893 */
final String v0 = "com.lbe.security.miui"; // const-string v0, "com.lbe.security.miui"
/* invoke-direct {p0, v0}, Lcom/miui/server/SecurityManagerService;->checkGrantPermissionPkg(Ljava/lang/String;)V */
/* .line 894 */
v0 = android.permission.PermissionManager .checkPackageNamePermission ( p2,p1,p3 );
if ( v0 != null) { // if-eqz v0, :cond_0
/* .line 895 */
return;
/* .line 896 */
} // :cond_0
android.os.Binder .clearCallingIdentity ( );
/* move-result-wide v0 */
/* .line 898 */
/* .local v0, "identity":J */
try { // :try_start_0
v2 = this.mPermissionManagerService;
int v3 = 0; // const/4 v3, 0x0
(( com.android.server.pm.permission.PermissionManagerService ) v2 ).revokeRuntimePermission ( p1, p2, p3, v3 ); // invoke-virtual {v2, p1, p2, p3, v3}, Lcom/android/server/pm/permission/PermissionManagerService;->revokeRuntimePermission(Ljava/lang/String;Ljava/lang/String;ILjava/lang/String;)V
/* :try_end_0 */
/* .catchall {:try_start_0 ..:try_end_0} :catchall_0 */
/* .line 900 */
android.os.Binder .restoreCallingIdentity ( v0,v1 );
/* .line 901 */
/* nop */
/* .line 902 */
return;
/* .line 900 */
/* :catchall_0 */
/* move-exception v2 */
android.os.Binder .restoreCallingIdentity ( v0,v1 );
/* .line 901 */
/* throw v2 */
} // .end method
public void revokeRuntimePermissionAsUserNotKill ( java.lang.String p0, java.lang.String p1, Integer p2 ) {
/* .locals 4 */
/* .param p1, "packageName" # Ljava/lang/String; */
/* .param p2, "permName" # Ljava/lang/String; */
/* .param p3, "userId" # I */
/* .line 906 */
final String v0 = "com.lbe.security.miui"; // const-string v0, "com.lbe.security.miui"
/* invoke-direct {p0, v0}, Lcom/miui/server/SecurityManagerService;->checkGrantPermissionPkg(Ljava/lang/String;)V */
/* .line 907 */
v0 = android.permission.PermissionManager .checkPackageNamePermission ( p2,p1,p3 );
if ( v0 != null) { // if-eqz v0, :cond_0
/* .line 908 */
return;
/* .line 909 */
} // :cond_0
android.os.Binder .clearCallingIdentity ( );
/* move-result-wide v0 */
/* .line 911 */
/* .local v0, "identity":J */
try { // :try_start_0
v2 = this.mPermissionManagerService;
final String v3 = "not kill"; // const-string v3, "not kill"
(( com.android.server.pm.permission.PermissionManagerService ) v2 ).revokeRuntimePermission ( p1, p2, p3, v3 ); // invoke-virtual {v2, p1, p2, p3, v3}, Lcom/android/server/pm/permission/PermissionManagerService;->revokeRuntimePermission(Ljava/lang/String;Ljava/lang/String;ILjava/lang/String;)V
/* :try_end_0 */
/* .catchall {:try_start_0 ..:try_end_0} :catchall_0 */
/* .line 914 */
android.os.Binder .restoreCallingIdentity ( v0,v1 );
/* .line 915 */
/* nop */
/* .line 916 */
return;
/* .line 914 */
/* :catchall_0 */
/* move-exception v2 */
android.os.Binder .restoreCallingIdentity ( v0,v1 );
/* .line 915 */
/* throw v2 */
} // .end method
public void scheduleWriteSettings ( ) {
/* .locals 4 */
/* .line 1156 */
v0 = this.mSecurityWriteHandler;
int v1 = 1; // const/4 v1, 0x1
v0 = (( com.miui.server.security.SecurityWriteHandler ) v0 ).hasMessages ( v1 ); // invoke-virtual {v0, v1}, Lcom/miui/server/security/SecurityWriteHandler;->hasMessages(I)Z
if ( v0 != null) { // if-eqz v0, :cond_0
return;
/* .line 1157 */
} // :cond_0
v0 = this.mSecurityWriteHandler;
/* const-wide/16 v2, 0x3e8 */
(( com.miui.server.security.SecurityWriteHandler ) v0 ).sendEmptyMessageDelayed ( v1, v2, v3 ); // invoke-virtual {v0, v1, v2, v3}, Lcom/miui/server/security/SecurityWriteHandler;->sendEmptyMessageDelayed(IJ)Z
/* .line 1158 */
return;
} // .end method
public void setAccessControlPassword ( java.lang.String p0, java.lang.String p1, Integer p2 ) {
/* .locals 1 */
/* .param p1, "passwordType" # Ljava/lang/String; */
/* .param p2, "password" # Ljava/lang/String; */
/* .param p3, "userId" # I */
/* .line 498 */
/* invoke-direct {p0}, Lcom/miui/server/SecurityManagerService;->checkPermission()V */
/* .line 499 */
p3 = miui.security.SecurityManager .getUserHandle ( p3 );
/* .line 500 */
v0 = this.mAccessController;
(( com.miui.server.AccessController ) v0 ).setAccessControlPassword ( p1, p2, p3 ); // invoke-virtual {v0, p1, p2, p3}, Lcom/miui/server/AccessController;->setAccessControlPassword(Ljava/lang/String;Ljava/lang/String;I)V
/* .line 501 */
return;
} // .end method
public void setAppDarkModeForUser ( java.lang.String p0, Boolean p1, Integer p2 ) {
/* .locals 1 */
/* .param p1, "packageName" # Ljava/lang/String; */
/* .param p2, "enabled" # Z */
/* .param p3, "userId" # I */
/* .line 332 */
v0 = this.mInternal;
(( miui.security.SecurityManagerInternal ) v0 ).setAppDarkModeForUser ( p1, p2, p3 ); // invoke-virtual {v0, p1, p2, p3}, Lmiui/security/SecurityManagerInternal;->setAppDarkModeForUser(Ljava/lang/String;ZI)V
/* .line 333 */
return;
} // .end method
public void setAppPrivacyStatus ( java.lang.String p0, Boolean p1 ) {
/* .locals 2 */
/* .param p1, "packageName" # Ljava/lang/String; */
/* .param p2, "isOpen" # Z */
/* .line 737 */
v0 = android.text.TextUtils .isEmpty ( p1 );
/* if-nez v0, :cond_1 */
/* .line 740 */
v0 = android.os.Binder .getCallingUid ( );
v0 = android.os.UserHandle .getAppId ( v0 );
/* const/16 v1, 0x3e8 */
/* if-eq v0, v1, :cond_0 */
/* .line 741 */
v0 = android.os.Binder .getCallingPid ( );
com.android.server.am.ProcessUtils .getPackageNameByPid ( v0 );
/* .line 742 */
/* .local v0, "callingPackage":Ljava/lang/String; */
v1 = (( java.lang.String ) p1 ).equals ( v0 ); // invoke-virtual {p1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z
/* if-nez v1, :cond_0 */
/* .line 743 */
/* invoke-direct {p0}, Lcom/miui/server/SecurityManagerService;->checkWriteSecurePermission()V */
/* .line 746 */
} // .end local v0 # "callingPackage":Ljava/lang/String;
} // :cond_0
/* new-instance v0, Lcom/miui/server/SecurityManagerService$$ExternalSyntheticLambda3; */
/* invoke-direct {v0, p0, p1, p2}, Lcom/miui/server/SecurityManagerService$$ExternalSyntheticLambda3;-><init>(Lcom/miui/server/SecurityManagerService;Ljava/lang/String;Z)V */
android.os.Binder .withCleanCallingIdentity ( v0 );
/* .line 749 */
return;
/* .line 738 */
} // :cond_1
/* new-instance v0, Ljava/lang/RuntimeException; */
final String v1 = "packageName can not be null or empty"; // const-string v1, "packageName can not be null or empty"
/* invoke-direct {v0, v1}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V */
/* throw v0 */
} // .end method
public void setAppRelaunchModeAfterFoldedForUser ( java.lang.String p0, Boolean p1, Integer p2 ) {
/* .locals 3 */
/* .param p1, "packageName" # Ljava/lang/String; */
/* .param p2, "enabled" # Z */
/* .param p3, "userId" # I */
/* .line 388 */
v0 = this.mUserStateLock;
/* monitor-enter v0 */
/* .line 389 */
try { // :try_start_0
(( com.miui.server.SecurityManagerService ) p0 ).getUserStateLocked ( p3 ); // invoke-virtual {p0, p3}, Lcom/miui/server/SecurityManagerService;->getUserStateLocked(I)Lcom/miui/server/security/SecurityUserState;
/* .line 390 */
/* .local v1, "userStateLocked":Lcom/miui/server/security/SecurityUserState; */
v2 = this.mPackages;
(( com.miui.server.SecurityManagerService ) p0 ).getPackageSetting ( v2, p1 ); // invoke-virtual {p0, v2, p1}, Lcom/miui/server/SecurityManagerService;->getPackageSetting(Ljava/util/HashMap;Ljava/lang/String;)Lcom/miui/server/security/SecurityPackageSettings;
/* .line 391 */
/* .local v2, "ps":Lcom/miui/server/security/SecurityPackageSettings; */
/* iput-boolean p2, v2, Lcom/miui/server/security/SecurityPackageSettings;->isRelaunchWhenFolded:Z */
/* .line 392 */
(( com.miui.server.SecurityManagerService ) p0 ).scheduleWriteSettings ( ); // invoke-virtual {p0}, Lcom/miui/server/SecurityManagerService;->scheduleWriteSettings()V
/* .line 393 */
} // .end local v1 # "userStateLocked":Lcom/miui/server/security/SecurityUserState;
} // .end local v2 # "ps":Lcom/miui/server/security/SecurityPackageSettings;
/* monitor-exit v0 */
/* .line 394 */
return;
/* .line 393 */
/* :catchall_0 */
/* move-exception v1 */
/* monitor-exit v0 */
/* :try_end_0 */
/* .catchall {:try_start_0 ..:try_end_0} :catchall_0 */
/* throw v1 */
} // .end method
public void setAppRemindForRelaunchForUser ( java.lang.String p0, Boolean p1, Integer p2 ) {
/* .locals 3 */
/* .param p1, "packageName" # Ljava/lang/String; */
/* .param p2, "enabled" # Z */
/* .param p3, "userId" # I */
/* .line 359 */
v0 = this.mUserStateLock;
/* monitor-enter v0 */
/* .line 360 */
try { // :try_start_0
(( com.miui.server.SecurityManagerService ) p0 ).getUserStateLocked ( p3 ); // invoke-virtual {p0, p3}, Lcom/miui/server/SecurityManagerService;->getUserStateLocked(I)Lcom/miui/server/security/SecurityUserState;
/* .line 361 */
/* .local v1, "userStateLocked":Lcom/miui/server/security/SecurityUserState; */
v2 = this.mPackages;
(( com.miui.server.SecurityManagerService ) p0 ).getPackageSetting ( v2, p1 ); // invoke-virtual {p0, v2, p1}, Lcom/miui/server/SecurityManagerService;->getPackageSetting(Ljava/util/HashMap;Ljava/lang/String;)Lcom/miui/server/security/SecurityPackageSettings;
/* .line 362 */
/* .local v2, "ps":Lcom/miui/server/security/SecurityPackageSettings; */
/* iput-boolean p2, v2, Lcom/miui/server/security/SecurityPackageSettings;->isRemindForRelaunch:Z */
/* .line 363 */
(( com.miui.server.SecurityManagerService ) p0 ).scheduleWriteSettings ( ); // invoke-virtual {p0}, Lcom/miui/server/SecurityManagerService;->scheduleWriteSettings()V
/* .line 364 */
} // .end local v1 # "userStateLocked":Lcom/miui/server/security/SecurityUserState;
} // .end local v2 # "ps":Lcom/miui/server/security/SecurityPackageSettings;
/* monitor-exit v0 */
/* .line 365 */
return;
/* .line 364 */
/* :catchall_0 */
/* move-exception v1 */
/* monitor-exit v0 */
/* :try_end_0 */
/* .catchall {:try_start_0 ..:try_end_0} :catchall_0 */
/* throw v1 */
} // .end method
public void setApplicationAccessControlEnabled ( java.lang.String p0, Boolean p1 ) {
/* .locals 1 */
/* .param p1, "packageName" # Ljava/lang/String; */
/* .param p2, "enabled" # Z */
/* .line 475 */
/* invoke-direct {p0}, Lcom/miui/server/SecurityManagerService;->checkPermission()V */
/* .line 476 */
v0 = android.os.UserHandle .getCallingUserId ( );
(( com.miui.server.SecurityManagerService ) p0 ).setApplicationAccessControlEnabledForUser ( p1, p2, v0 ); // invoke-virtual {p0, p1, p2, v0}, Lcom/miui/server/SecurityManagerService;->setApplicationAccessControlEnabledForUser(Ljava/lang/String;ZI)V
/* .line 477 */
return;
} // .end method
public void setApplicationAccessControlEnabledForUser ( java.lang.String p0, Boolean p1, Integer p2 ) {
/* .locals 1 */
/* .param p1, "packageName" # Ljava/lang/String; */
/* .param p2, "enabled" # Z */
/* .param p3, "userId" # I */
/* .line 481 */
/* invoke-direct {p0}, Lcom/miui/server/SecurityManagerService;->checkPermission()V */
/* .line 482 */
v0 = this.mAccessControlImpl;
(( com.miui.server.security.AccessControlImpl ) v0 ).setApplicationAccessControlEnabledForUser ( p1, p2, p3 ); // invoke-virtual {v0, p1, p2, p3}, Lcom/miui/server/security/AccessControlImpl;->setApplicationAccessControlEnabledForUser(Ljava/lang/String;ZI)V
/* .line 483 */
return;
} // .end method
public void setApplicationChildrenControlEnabled ( java.lang.String p0, Boolean p1 ) {
/* .locals 4 */
/* .param p1, "packageName" # Ljava/lang/String; */
/* .param p2, "enabled" # Z */
/* .line 568 */
/* invoke-direct {p0}, Lcom/miui/server/SecurityManagerService;->checkPermission()V */
/* .line 569 */
v0 = android.os.UserHandle .getCallingUserId ( );
/* .line 570 */
/* .local v0, "callingUserId":I */
v1 = this.mUserStateLock;
/* monitor-enter v1 */
/* .line 571 */
try { // :try_start_0
(( com.miui.server.SecurityManagerService ) p0 ).getUserStateLocked ( v0 ); // invoke-virtual {p0, v0}, Lcom/miui/server/SecurityManagerService;->getUserStateLocked(I)Lcom/miui/server/security/SecurityUserState;
/* .line 572 */
/* .local v2, "userStateLocked":Lcom/miui/server/security/SecurityUserState; */
v3 = this.mPackages;
(( com.miui.server.SecurityManagerService ) p0 ).getPackageSetting ( v3, p1 ); // invoke-virtual {p0, v3, p1}, Lcom/miui/server/SecurityManagerService;->getPackageSetting(Ljava/util/HashMap;Ljava/lang/String;)Lcom/miui/server/security/SecurityPackageSettings;
/* .line 573 */
/* .local v3, "ps":Lcom/miui/server/security/SecurityPackageSettings; */
/* iput-boolean p2, v3, Lcom/miui/server/security/SecurityPackageSettings;->childrenControl:Z */
/* .line 574 */
(( com.miui.server.SecurityManagerService ) p0 ).scheduleWriteSettings ( ); // invoke-virtual {p0}, Lcom/miui/server/SecurityManagerService;->scheduleWriteSettings()V
/* .line 575 */
} // .end local v2 # "userStateLocked":Lcom/miui/server/security/SecurityUserState;
} // .end local v3 # "ps":Lcom/miui/server/security/SecurityPackageSettings;
/* monitor-exit v1 */
/* .line 576 */
return;
/* .line 575 */
/* :catchall_0 */
/* move-exception v2 */
/* monitor-exit v1 */
/* :try_end_0 */
/* .catchall {:try_start_0 ..:try_end_0} :catchall_0 */
/* throw v2 */
} // .end method
public void setApplicationMaskNotificationEnabledForUser ( java.lang.String p0, Boolean p1, Integer p2 ) {
/* .locals 1 */
/* .param p1, "packageName" # Ljava/lang/String; */
/* .param p2, "enabled" # Z */
/* .param p3, "userId" # I */
/* .line 486 */
/* invoke-direct {p0}, Lcom/miui/server/SecurityManagerService;->checkPermission()V */
/* .line 487 */
v0 = this.mAccessControlImpl;
(( com.miui.server.security.AccessControlImpl ) v0 ).setApplicationMaskNotificationEnabledForUser ( p1, p2, p3 ); // invoke-virtual {v0, p1, p2, p3}, Lcom/miui/server/security/AccessControlImpl;->setApplicationMaskNotificationEnabledForUser(Ljava/lang/String;ZI)V
/* .line 488 */
return;
} // .end method
public Boolean setCurrentNetworkState ( Integer p0 ) {
/* .locals 1 */
/* .param p1, "state" # I */
/* .line 269 */
/* const/16 v0, 0x3e8 */
/* filled-new-array {v0}, [I */
/* invoke-direct {p0, v0}, Lcom/miui/server/SecurityManagerService;->checkPermissionByUid([I)V */
/* .line 270 */
v0 = com.android.server.net.NetworkManagementServiceStub .getInstance ( );
} // .end method
public void setGameBoosterIBinder ( android.os.IBinder p0, Integer p1, Boolean p2 ) {
/* .locals 1 */
/* .param p1, "gameBooster" # Landroid/os/IBinder; */
/* .param p2, "userId" # I */
/* .param p3, "isGameMode" # Z */
/* .line 278 */
/* invoke-direct {p0}, Lcom/miui/server/SecurityManagerService;->checkPermission()V */
/* .line 279 */
v0 = this.mGameBoosterImpl;
(( com.miui.server.security.GameBoosterImpl ) v0 ).setGameBoosterIBinder ( p1, p2, p3 ); // invoke-virtual {v0, p1, p2, p3}, Lcom/miui/server/security/GameBoosterImpl;->setGameBoosterIBinder(Landroid/os/IBinder;IZ)V
/* .line 280 */
return;
} // .end method
public void setGameStorageApp ( java.lang.String p0, Integer p1, Boolean p2 ) {
/* .locals 1 */
/* .param p1, "packageName" # Ljava/lang/String; */
/* .param p2, "userId" # I */
/* .param p3, "isStorage" # Z */
/* .line 292 */
/* invoke-direct {p0}, Lcom/miui/server/SecurityManagerService;->checkPermission()V */
/* .line 293 */
v0 = this.mGameBoosterImpl;
(( com.miui.server.security.GameBoosterImpl ) v0 ).setGameStorageApp ( p1, p2, p3 ); // invoke-virtual {v0, p1, p2, p3}, Lcom/miui/server/security/GameBoosterImpl;->setGameStorageApp(Ljava/lang/String;IZ)V
/* .line 294 */
return;
} // .end method
public void setIncompatibleAppList ( java.util.List p0 ) {
/* .locals 2 */
/* .annotation system Ldalvik/annotation/Signature; */
/* value = { */
/* "(", */
/* "Ljava/util/List<", */
/* "Ljava/lang/String;", */
/* ">;)V" */
/* } */
} // .end annotation
/* .line 710 */
/* .local p1, "list":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;" */
/* invoke-direct {p0}, Lcom/miui/server/SecurityManagerService;->checkPermission()V */
/* .line 711 */
if ( p1 != null) { // if-eqz p1, :cond_0
/* .line 714 */
v0 = this.mIncompatibleAppList;
/* monitor-enter v0 */
/* .line 715 */
try { // :try_start_0
v1 = this.mIncompatibleAppList;
(( java.util.ArrayList ) v1 ).clear ( ); // invoke-virtual {v1}, Ljava/util/ArrayList;->clear()V
/* .line 716 */
v1 = this.mIncompatibleAppList;
(( java.util.ArrayList ) v1 ).addAll ( p1 ); // invoke-virtual {v1, p1}, Ljava/util/ArrayList;->addAll(Ljava/util/Collection;)Z
/* .line 717 */
/* monitor-exit v0 */
/* .line 718 */
return;
/* .line 717 */
/* :catchall_0 */
/* move-exception v1 */
/* monitor-exit v0 */
/* :try_end_0 */
/* .catchall {:try_start_0 ..:try_end_0} :catchall_0 */
/* throw v1 */
/* .line 712 */
} // :cond_0
/* new-instance v0, Ljava/lang/NullPointerException; */
final String v1 = "List is null"; // const-string v1, "List is null"
/* invoke-direct {v0, v1}, Ljava/lang/NullPointerException;-><init>(Ljava/lang/String;)V */
/* throw v0 */
} // .end method
public Boolean setMiuiFirewallRule ( java.lang.String p0, Integer p1, Integer p2, Integer p3 ) {
/* .locals 1 */
/* .param p1, "packageName" # Ljava/lang/String; */
/* .param p2, "uid" # I */
/* .param p3, "rule" # I */
/* .param p4, "type" # I */
/* .line 263 */
/* const/16 v0, 0x3e8 */
/* filled-new-array {v0}, [I */
/* invoke-direct {p0, v0}, Lcom/miui/server/SecurityManagerService;->checkPermissionByUid([I)V */
/* .line 264 */
v0 = com.android.server.net.NetworkManagementServiceStub .getInstance ( );
} // .end method
public void setNotificationsEnabledForPackage ( java.lang.String p0, Integer p1, Boolean p2 ) {
/* .locals 3 */
/* .param p1, "packageName" # Ljava/lang/String; */
/* .param p2, "uid" # I */
/* .param p3, "enabled" # Z */
/* .annotation system Ldalvik/annotation/Throws; */
/* value = { */
/* Landroid/os/RemoteException; */
/* } */
} // .end annotation
/* .line 866 */
/* invoke-direct {p0}, Lcom/miui/server/SecurityManagerService;->checkPermission()V */
/* .line 867 */
v0 = this.mINotificationManager;
/* if-nez v0, :cond_0 */
/* .line 868 */
final String v0 = "notification"; // const-string v0, "notification"
android.os.ServiceManager .getService ( v0 );
android.app.INotificationManager$Stub .asInterface ( v0 );
this.mINotificationManager = v0;
/* .line 870 */
} // :cond_0
android.os.Binder .clearCallingIdentity ( );
/* move-result-wide v0 */
/* .line 872 */
/* .local v0, "identity":J */
try { // :try_start_0
v2 = this.mINotificationManager;
/* :try_end_0 */
/* .catchall {:try_start_0 ..:try_end_0} :catchall_0 */
/* .line 874 */
android.os.Binder .restoreCallingIdentity ( v0,v1 );
/* .line 875 */
/* nop */
/* .line 876 */
return;
/* .line 874 */
/* :catchall_0 */
/* move-exception v2 */
android.os.Binder .restoreCallingIdentity ( v0,v1 );
/* .line 875 */
/* throw v2 */
} // .end method
public void setPrivacyApp ( java.lang.String p0, Integer p1, Boolean p2 ) {
/* .locals 3 */
/* .param p1, "packageName" # Ljava/lang/String; */
/* .param p2, "userId" # I */
/* .param p3, "isPrivacy" # Z */
/* .line 766 */
/* invoke-direct {p0}, Lcom/miui/server/SecurityManagerService;->checkPermission()V */
/* .line 767 */
v0 = this.mUserStateLock;
/* monitor-enter v0 */
/* .line 768 */
try { // :try_start_0
(( com.miui.server.SecurityManagerService ) p0 ).getUserStateLocked ( p2 ); // invoke-virtual {p0, p2}, Lcom/miui/server/SecurityManagerService;->getUserStateLocked(I)Lcom/miui/server/security/SecurityUserState;
/* .line 769 */
/* .local v1, "userStateLocked":Lcom/miui/server/security/SecurityUserState; */
v2 = this.mPackages;
(( com.miui.server.SecurityManagerService ) p0 ).getPackageSetting ( v2, p1 ); // invoke-virtual {p0, v2, p1}, Lcom/miui/server/SecurityManagerService;->getPackageSetting(Ljava/util/HashMap;Ljava/lang/String;)Lcom/miui/server/security/SecurityPackageSettings;
/* .line 770 */
/* .local v2, "ps":Lcom/miui/server/security/SecurityPackageSettings; */
/* iput-boolean p3, v2, Lcom/miui/server/security/SecurityPackageSettings;->isPrivacyApp:Z */
/* .line 771 */
(( com.miui.server.SecurityManagerService ) p0 ).scheduleWriteSettings ( ); // invoke-virtual {p0}, Lcom/miui/server/SecurityManagerService;->scheduleWriteSettings()V
/* .line 772 */
} // .end local v1 # "userStateLocked":Lcom/miui/server/security/SecurityUserState;
} // .end local v2 # "ps":Lcom/miui/server/security/SecurityPackageSettings;
/* monitor-exit v0 */
/* .line 773 */
return;
/* .line 772 */
/* :catchall_0 */
/* move-exception v1 */
/* monitor-exit v0 */
/* :try_end_0 */
/* .catchall {:try_start_0 ..:try_end_0} :catchall_0 */
/* throw v1 */
} // .end method
public void setScRelaunchNeedConfirmForUser ( java.lang.String p0, Boolean p1, Integer p2 ) {
/* .locals 3 */
/* .param p1, "packageName" # Ljava/lang/String; */
/* .param p2, "confirm" # Z */
/* .param p3, "userId" # I */
/* .line 417 */
v0 = this.mUserStateLock;
/* monitor-enter v0 */
/* .line 418 */
try { // :try_start_0
(( com.miui.server.SecurityManagerService ) p0 ).getUserStateLocked ( p3 ); // invoke-virtual {p0, p3}, Lcom/miui/server/SecurityManagerService;->getUserStateLocked(I)Lcom/miui/server/security/SecurityUserState;
/* .line 419 */
/* .local v1, "userStateLocked":Lcom/miui/server/security/SecurityUserState; */
v2 = this.mPackages;
(( com.miui.server.SecurityManagerService ) p0 ).getPackageSetting ( v2, p1 ); // invoke-virtual {p0, v2, p1}, Lcom/miui/server/SecurityManagerService;->getPackageSetting(Ljava/util/HashMap;Ljava/lang/String;)Lcom/miui/server/security/SecurityPackageSettings;
/* .line 420 */
/* .local v2, "ps":Lcom/miui/server/security/SecurityPackageSettings; */
/* iput-boolean p2, v2, Lcom/miui/server/security/SecurityPackageSettings;->isScRelaunchConfirm:Z */
/* .line 421 */
(( com.miui.server.SecurityManagerService ) p0 ).scheduleWriteSettings ( ); // invoke-virtual {p0}, Lcom/miui/server/SecurityManagerService;->scheduleWriteSettings()V
/* .line 422 */
} // .end local v1 # "userStateLocked":Lcom/miui/server/security/SecurityUserState;
} // .end local v2 # "ps":Lcom/miui/server/security/SecurityPackageSettings;
/* monitor-exit v0 */
/* .line 423 */
return;
/* .line 422 */
/* :catchall_0 */
/* move-exception v1 */
/* monitor-exit v0 */
/* :try_end_0 */
/* .catchall {:try_start_0 ..:try_end_0} :catchall_0 */
/* throw v1 */
} // .end method
public void setTrackWakePathCallListLogEnabled ( Boolean p0 ) {
/* .locals 1 */
/* .param p1, "enabled" # Z */
/* .line 643 */
/* invoke-direct {p0}, Lcom/miui/server/SecurityManagerService;->checkWakePathPermission()V */
/* .line 644 */
miui.security.WakePathChecker .getInstance ( );
(( miui.security.WakePathChecker ) v0 ).setTrackWakePathCallListLogEnabled ( p1 ); // invoke-virtual {v0, p1}, Lmiui/security/WakePathChecker;->setTrackWakePathCallListLogEnabled(Z)V
/* .line 645 */
return;
} // .end method
public void setWakeUpTime ( java.lang.String p0, Long p1 ) {
/* .locals 3 */
/* .param p1, "componentName" # Ljava/lang/String; */
/* .param p2, "timeInSeconds" # J */
/* .line 593 */
v0 = this.mContext;
final String v1 = "com.miui.permission.MANAGE_BOOT_TIME"; // const-string v1, "com.miui.permission.MANAGE_BOOT_TIME"
final String v2 = "SecurityManagerService"; // const-string v2, "SecurityManagerService"
(( android.content.Context ) v0 ).enforceCallingOrSelfPermission ( v1, v2 ); // invoke-virtual {v0, v1, v2}, Landroid/content/Context;->enforceCallingOrSelfPermission(Ljava/lang/String;Ljava/lang/String;)V
/* .line 594 */
v0 = this.mWakeUpTimeImpl;
(( com.miui.server.security.WakeUpTimeImpl ) v0 ).setWakeUpTime ( p1, p2, p3 ); // invoke-virtual {v0, p1, p2, p3}, Lcom/miui/server/security/WakeUpTimeImpl;->setWakeUpTime(Ljava/lang/String;J)V
/* .line 595 */
return;
} // .end method
public Boolean startInterceptSmsBySender ( java.lang.String p0, java.lang.String p1, Integer p2 ) {
/* .locals 1 */
/* .param p1, "pkgName" # Ljava/lang/String; */
/* .param p2, "sender" # Ljava/lang/String; */
/* .param p3, "count" # I */
/* .line 245 */
/* const/16 v0, 0x3e8 */
/* filled-new-array {v0}, [I */
/* invoke-direct {p0, v0}, Lcom/miui/server/SecurityManagerService;->checkPermissionByUid([I)V */
/* .line 246 */
v0 = this.mSecuritySmsHandler;
v0 = (( com.miui.server.SecuritySmsHandler ) v0 ).startInterceptSmsBySender ( p1, p2, p3 ); // invoke-virtual {v0, p1, p2, p3}, Lcom/miui/server/SecuritySmsHandler;->startInterceptSmsBySender(Ljava/lang/String;Ljava/lang/String;I)Z
} // .end method
public void startWatchingAppBehavior ( Integer p0, Boolean p1 ) {
/* .locals 1 */
/* .param p1, "behavior" # I */
/* .param p2, "includeSystem" # Z */
/* .line 1282 */
final String v0 = "com.miui.securitycenter"; // const-string v0, "com.miui.securitycenter"
/* invoke-direct {p0, v0}, Lcom/miui/server/SecurityManagerService;->checkGrantPermissionPkg(Ljava/lang/String;)V */
/* .line 1283 */
v0 = this.mAppBehavior;
(( com.miui.server.security.AppBehaviorService ) v0 ).startWatchingAppBehavior ( p1, p2 ); // invoke-virtual {v0, p1, p2}, Lcom/miui/server/security/AppBehaviorService;->startWatchingAppBehavior(IZ)V
/* .line 1284 */
return;
} // .end method
public void startWatchingAppBehaviors ( Integer p0, java.lang.String[] p1, Boolean p2 ) {
/* .locals 1 */
/* .param p1, "behavior" # I */
/* .param p2, "pkgNames" # [Ljava/lang/String; */
/* .param p3, "includeSystem" # Z */
/* .line 1288 */
final String v0 = "com.miui.securitycenter"; // const-string v0, "com.miui.securitycenter"
/* invoke-direct {p0, v0}, Lcom/miui/server/SecurityManagerService;->checkGrantPermissionPkg(Ljava/lang/String;)V */
/* .line 1289 */
v0 = this.mAppBehavior;
(( com.miui.server.security.AppBehaviorService ) v0 ).startWatchingAppBehavior ( p1, p2, p3 ); // invoke-virtual {v0, p1, p2, p3}, Lcom/miui/server/security/AppBehaviorService;->startWatchingAppBehavior(I[Ljava/lang/String;Z)V
/* .line 1290 */
return;
} // .end method
public Boolean stopInterceptSmsBySender ( ) {
/* .locals 1 */
/* .line 251 */
/* const/16 v0, 0x3e8 */
/* filled-new-array {v0}, [I */
/* invoke-direct {p0, v0}, Lcom/miui/server/SecurityManagerService;->checkPermissionByUid([I)V */
/* .line 252 */
v0 = this.mSecuritySmsHandler;
v0 = (( com.miui.server.SecuritySmsHandler ) v0 ).stopInterceptSmsBySender ( ); // invoke-virtual {v0}, Lcom/miui/server/SecuritySmsHandler;->stopInterceptSmsBySender()Z
} // .end method
public void stopWatchingAppBehavior ( Integer p0 ) {
/* .locals 1 */
/* .param p1, "behavior" # I */
/* .line 1294 */
final String v0 = "com.miui.securitycenter"; // const-string v0, "com.miui.securitycenter"
/* invoke-direct {p0, v0}, Lcom/miui/server/SecurityManagerService;->checkGrantPermissionPkg(Ljava/lang/String;)V */
/* .line 1295 */
v0 = this.mAppBehavior;
(( com.miui.server.security.AppBehaviorService ) v0 ).stopWatchingAppBehavior ( p1 ); // invoke-virtual {v0, p1}, Lcom/miui/server/security/AppBehaviorService;->stopWatchingAppBehavior(I)V
/* .line 1296 */
return;
} // .end method
public void stopWatchingAppBehaviors ( Integer p0, java.lang.String[] p1 ) {
/* .locals 1 */
/* .param p1, "behavior" # I */
/* .param p2, "pkgNames" # [Ljava/lang/String; */
/* .line 1300 */
final String v0 = "com.miui.securitycenter"; // const-string v0, "com.miui.securitycenter"
/* invoke-direct {p0, v0}, Lcom/miui/server/SecurityManagerService;->checkGrantPermissionPkg(Ljava/lang/String;)V */
/* .line 1301 */
v0 = this.mAppBehavior;
(( com.miui.server.security.AppBehaviorService ) v0 ).stopWatchingAppBehavior ( p1, p2 ); // invoke-virtual {v0, p1, p2}, Lcom/miui/server/security/AppBehaviorService;->stopWatchingAppBehavior(I[Ljava/lang/String;)V
/* .line 1302 */
return;
} // .end method
public void switchSimulatedTouchDetect ( java.util.Map p0 ) {
/* .locals 1 */
/* .annotation system Ldalvik/annotation/Signature; */
/* value = { */
/* "(", */
/* "Ljava/util/Map<", */
/* "Ljava/lang/String;", */
/* "Ljava/lang/String;", */
/* ">;)V" */
/* } */
} // .end annotation
/* .line 1397 */
/* .local p1, "config":Ljava/util/Map;, "Ljava/util/Map<Ljava/lang/String;Ljava/lang/String;>;" */
/* const/16 v0, 0x3e8 */
/* filled-new-array {v0}, [I */
/* invoke-direct {p0, v0}, Lcom/miui/server/SecurityManagerService;->checkPermissionByUid([I)V */
/* .line 1398 */
com.miui.server.security.SafetyDetectManagerStub .getInstance ( );
(( com.miui.server.security.SafetyDetectManagerStub ) v0 ).switchSimulatedTouchDetect ( p1 ); // invoke-virtual {v0, p1}, Lcom/miui/server/security/SafetyDetectManagerStub;->switchSimulatedTouchDetect(Ljava/util/Map;)V
/* .line 1399 */
return;
} // .end method
public void updateAppWidgetVisibility ( java.lang.String p0, Integer p1, Boolean p2, java.util.List p3, java.lang.String p4 ) {
/* .locals 6 */
/* .param p1, "packageName" # Ljava/lang/String; */
/* .param p2, "uid" # I */
/* .param p3, "visible" # Z */
/* .param p5, "reason" # Ljava/lang/String; */
/* .annotation system Ldalvik/annotation/Signature; */
/* value = { */
/* "(", */
/* "Ljava/lang/String;", */
/* "IZ", */
/* "Ljava/util/List<", */
/* "Ljava/lang/String;", */
/* ">;", */
/* "Ljava/lang/String;", */
/* ")V" */
/* } */
} // .end annotation
/* .line 1404 */
/* .local p4, "permissions":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;" */
v0 = this.mContext;
final String v1 = "android.permission.BIND_APPWIDGET"; // const-string v1, "android.permission.BIND_APPWIDGET"
(( android.content.Context ) v0 ).enforceCallingOrSelfPermission ( v1, p5 ); // invoke-virtual {v0, v1, p5}, Landroid/content/Context;->enforceCallingOrSelfPermission(Ljava/lang/String;Ljava/lang/String;)V
/* .line 1405 */
if ( p4 != null) { // if-eqz p4, :cond_4
v0 = final String v0 = "android.permission.RECORD_AUDIO"; // const-string v0, "android.permission.RECORD_AUDIO"
/* if-nez v0, :cond_4 */
/* .line 1406 */
v0 = final String v0 = "android.permission.CAMERA"; // const-string v0, "android.permission.CAMERA"
/* if-nez v0, :cond_4 */
/* .line 1407 */
v0 = final String v0 = "android.permission.ACCESS_FINE_LOCATION"; // const-string v0, "android.permission.ACCESS_FINE_LOCATION"
/* if-nez v0, :cond_0 */
/* goto/16 :goto_0 */
/* .line 1410 */
} // :cond_0
v0 = this.mAppOpsInternal;
/* if-nez v0, :cond_1 */
/* .line 1411 */
/* const-class v0, Landroid/app/AppOpsManagerInternal; */
com.android.server.LocalServices .getService ( v0 );
/* check-cast v0, Landroid/app/AppOpsManagerInternal; */
this.mAppOpsInternal = v0;
/* .line 1413 */
} // :cond_1
/* new-instance v0, Landroid/util/SparseArray; */
/* invoke-direct {v0}, Landroid/util/SparseArray;-><init>()V */
/* .line 1414 */
/* .local v0, "uidPackageNames":Landroid/util/SparseArray;, "Landroid/util/SparseArray<Ljava/lang/String;>;" */
(( android.util.SparseArray ) v0 ).put ( p2, p1 ); // invoke-virtual {v0, p2, p1}, Landroid/util/SparseArray;->put(ILjava/lang/Object;)V
/* .line 1415 */
v1 = this.mAppOpsInternal;
(( android.app.AppOpsManagerInternal ) v1 ).updateAppWidgetVisibility ( v0, p3 ); // invoke-virtual {v1, v0, p3}, Landroid/app/AppOpsManagerInternal;->updateAppWidgetVisibility(Landroid/util/SparseArray;Z)V
/* .line 1416 */
/* new-instance v1, Ljava/lang/StringBuilder; */
/* invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V */
/* const-string/jumbo v2, "update appwidget of uid: " */
(( java.lang.StringBuilder ) v1 ).append ( v2 ); // invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v1 ).append ( p2 ); // invoke-virtual {v1, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;
final String v2 = " package: "; // const-string v2, " package: "
(( java.lang.StringBuilder ) v1 ).append ( v2 ); // invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v1 ).append ( p1 ); // invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
final String v2 = " visible: "; // const-string v2, " visible: "
(( java.lang.StringBuilder ) v1 ).append ( v2 ); // invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v1 ).append ( p3 ); // invoke-virtual {v1, p3}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;
final String v2 = " reason: "; // const-string v2, " reason: "
(( java.lang.StringBuilder ) v1 ).append ( v2 ); // invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v1 ).append ( p5 ); // invoke-virtual {v1, p5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v1 ).toString ( ); // invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
final String v2 = "SecurityManagerService"; // const-string v2, "SecurityManagerService"
android.util.Log .i ( v2,v1 );
/* .line 1418 */
/* new-instance v1, Ljava/lang/StringBuilder; */
/* invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V */
(( java.lang.StringBuilder ) v1 ).append ( p2 ); // invoke-virtual {v1, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v1 ).append ( p1 ); // invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v1 ).toString ( ); // invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
v1 = (( java.lang.String ) v1 ).hashCode ( ); // invoke-virtual {v1}, Ljava/lang/String;->hashCode()I
/* .line 1419 */
/* .local v1, "what":I */
v2 = this.mSecurityWriteHandler;
v2 = (( com.miui.server.security.SecurityWriteHandler ) v2 ).hasMessages ( v1 ); // invoke-virtual {v2, v1}, Lcom/miui/server/security/SecurityWriteHandler;->hasMessages(I)Z
if ( v2 != null) { // if-eqz v2, :cond_2
/* .line 1420 */
v2 = this.mSecurityWriteHandler;
(( com.miui.server.security.SecurityWriteHandler ) v2 ).removeMessages ( v1 ); // invoke-virtual {v2, v1}, Lcom/miui/server/security/SecurityWriteHandler;->removeMessages(I)V
/* .line 1422 */
} // :cond_2
if ( p3 != null) { // if-eqz p3, :cond_3
/* .line 1423 */
v2 = this.mSecurityWriteHandler;
android.os.Message .obtain ( v2,v1 );
/* .line 1424 */
/* .local v2, "msg":Landroid/os/Message; */
/* new-instance v3, Lcom/miui/server/SecurityManagerService$$ExternalSyntheticLambda0; */
/* invoke-direct {v3, p0, v0}, Lcom/miui/server/SecurityManagerService$$ExternalSyntheticLambda0;-><init>(Lcom/miui/server/SecurityManagerService;Landroid/util/SparseArray;)V */
(( android.os.Message ) v2 ).setCallback ( v3 ); // invoke-virtual {v2, v3}, Landroid/os/Message;->setCallback(Ljava/lang/Runnable;)Landroid/os/Message;
/* .line 1425 */
v3 = this.mSecurityWriteHandler;
/* const-wide/16 v4, 0x2710 */
(( com.miui.server.security.SecurityWriteHandler ) v3 ).sendMessageDelayed ( v2, v4, v5 ); // invoke-virtual {v3, v2, v4, v5}, Lcom/miui/server/security/SecurityWriteHandler;->sendMessageDelayed(Landroid/os/Message;J)Z
/* .line 1427 */
} // .end local v2 # "msg":Landroid/os/Message;
} // :cond_3
return;
/* .line 1408 */
} // .end local v0 # "uidPackageNames":Landroid/util/SparseArray;, "Landroid/util/SparseArray<Ljava/lang/String;>;"
} // .end local v1 # "what":I
} // :cond_4
} // :goto_0
return;
} // .end method
public void updateBehaviorThreshold ( Integer p0, Long p1 ) {
/* .locals 1 */
/* .param p1, "behavior" # I */
/* .param p2, "threshold" # J */
/* .line 1320 */
final String v0 = "com.miui.securitycenter"; // const-string v0, "com.miui.securitycenter"
/* invoke-direct {p0, v0}, Lcom/miui/server/SecurityManagerService;->checkGrantPermissionPkg(Ljava/lang/String;)V */
/* .line 1321 */
v0 = this.mAppBehavior;
(( com.miui.server.security.AppBehaviorService ) v0 ).updateBehaviorThreshold ( p1, p2, p3 ); // invoke-virtual {v0, p1, p2, p3}, Lcom/miui/server/security/AppBehaviorService;->updateBehaviorThreshold(IJ)V
/* .line 1322 */
return;
} // .end method
public void updateLauncherPackageNames ( ) {
/* .locals 2 */
/* .line 661 */
miui.security.WakePathChecker .getInstance ( );
v1 = this.mContext;
(( miui.security.WakePathChecker ) v0 ).init ( v1 ); // invoke-virtual {v0, v1}, Lmiui/security/WakePathChecker;->init(Landroid/content/Context;)V
/* .line 662 */
return;
} // .end method
public void updatePermissionFlagsAsUser ( java.lang.String p0, java.lang.String p1, Integer p2, Integer p3, Integer p4 ) {
/* .locals 9 */
/* .param p1, "permissionName" # Ljava/lang/String; */
/* .param p2, "packageName" # Ljava/lang/String; */
/* .param p3, "flagMask" # I */
/* .param p4, "flagValues" # I */
/* .param p5, "userId" # I */
/* .line 931 */
final String v0 = "com.lbe.security.miui"; // const-string v0, "com.lbe.security.miui"
/* invoke-direct {p0, v0}, Lcom/miui/server/SecurityManagerService;->checkGrantPermissionPkg(Ljava/lang/String;)V */
/* .line 932 */
android.os.Binder .clearCallingIdentity ( );
/* move-result-wide v0 */
/* .line 934 */
/* .local v0, "identity":J */
try { // :try_start_0
v2 = this.mPermissionManagerService;
int v7 = 1; // const/4 v7, 0x1
/* move-object v3, p2 */
/* move-object v4, p1 */
/* move v5, p3 */
/* move v6, p4 */
/* move v8, p5 */
/* invoke-virtual/range {v2 ..v8}, Lcom/android/server/pm/permission/PermissionManagerService;->updatePermissionFlags(Ljava/lang/String;Ljava/lang/String;IIZI)V */
/* :try_end_0 */
/* .catch Ljava/lang/Exception; {:try_start_0 ..:try_end_0} :catch_0 */
/* .catchall {:try_start_0 ..:try_end_0} :catchall_0 */
/* .line 940 */
/* nop */
} // :goto_0
android.os.Binder .restoreCallingIdentity ( v0,v1 );
/* .line 941 */
/* .line 940 */
/* :catchall_0 */
/* move-exception v2 */
/* .line 936 */
/* :catch_0 */
/* move-exception v2 */
/* .line 937 */
/* .local v2, "e":Ljava/lang/Exception; */
try { // :try_start_1
final String v3 = "SecurityManagerService"; // const-string v3, "SecurityManagerService"
/* new-instance v4, Ljava/lang/StringBuilder; */
/* invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V */
/* const-string/jumbo v5, "updatePermissionFlagsAsUser failed: perm=" */
(( java.lang.StringBuilder ) v4 ).append ( v5 ); // invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v4 ).append ( p1 ); // invoke-virtual {v4, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
final String v5 = ", pkg="; // const-string v5, ", pkg="
(( java.lang.StringBuilder ) v4 ).append ( v5 ); // invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v4 ).append ( p2 ); // invoke-virtual {v4, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
final String v5 = ", mask="; // const-string v5, ", mask="
(( java.lang.StringBuilder ) v4 ).append ( v5 ); // invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v4 ).append ( p3 ); // invoke-virtual {v4, p3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;
final String v5 = ", value="; // const-string v5, ", value="
(( java.lang.StringBuilder ) v4 ).append ( v5 ); // invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v4 ).append ( p4 ); // invoke-virtual {v4, p4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v4 ).toString ( ); // invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
android.util.Log .e ( v3,v4,v2 );
/* :try_end_1 */
/* .catchall {:try_start_1 ..:try_end_1} :catchall_0 */
/* .line 940 */
/* nop */
} // .end local v2 # "e":Ljava/lang/Exception;
/* .line 942 */
} // :goto_1
return;
/* .line 940 */
} // :goto_2
android.os.Binder .restoreCallingIdentity ( v0,v1 );
/* .line 941 */
/* throw v2 */
} // .end method
public void watchGreenGuardProcess ( ) {
/* .locals 1 */
/* .line 585 */
v0 = this.mContext;
com.miui.server.GreenGuardManagerService .startWatchGreenGuardProcess ( v0 );
/* .line 586 */
return;
} // .end method
public void writeSettings ( ) {
/* .locals 10 */
/* .line 1225 */
int v0 = 0; // const/4 v0, 0x0
/* .line 1227 */
/* .local v0, "fos":Ljava/io/FileOutputStream; */
try { // :try_start_0
/* new-instance v1, Ljava/util/ArrayList; */
/* invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V */
/* .line 1228 */
/* .local v1, "userStates":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/miui/server/security/SecurityUserState;>;" */
v2 = this.mUserStateLock;
/* monitor-enter v2 */
/* :try_end_0 */
/* .catch Ljava/lang/Exception; {:try_start_0 ..:try_end_0} :catch_0 */
/* .line 1229 */
try { // :try_start_1
v3 = this.mUserStates;
v3 = (( android.util.SparseArray ) v3 ).size ( ); // invoke-virtual {v3}, Landroid/util/SparseArray;->size()I
/* .line 1230 */
/* .local v3, "size":I */
int v4 = 0; // const/4 v4, 0x0
/* .local v4, "i":I */
} // :goto_0
/* if-ge v4, v3, :cond_0 */
/* .line 1231 */
v5 = this.mUserStates;
(( android.util.SparseArray ) v5 ).valueAt ( v4 ); // invoke-virtual {v5, v4}, Landroid/util/SparseArray;->valueAt(I)Ljava/lang/Object;
/* check-cast v5, Lcom/miui/server/security/SecurityUserState; */
/* .line 1232 */
/* .local v5, "state":Lcom/miui/server/security/SecurityUserState; */
/* new-instance v6, Lcom/miui/server/security/SecurityUserState; */
/* invoke-direct {v6}, Lcom/miui/server/security/SecurityUserState;-><init>()V */
/* .line 1233 */
/* .local v6, "userState":Lcom/miui/server/security/SecurityUserState; */
/* iget v7, v5, Lcom/miui/server/security/SecurityUserState;->userHandle:I */
/* iput v7, v6, Lcom/miui/server/security/SecurityUserState;->userHandle:I */
/* .line 1234 */
v7 = this.mPackages;
/* new-instance v8, Ljava/util/HashMap; */
v9 = this.mPackages;
/* invoke-direct {v8, v9}, Ljava/util/HashMap;-><init>(Ljava/util/Map;)V */
(( java.util.HashMap ) v7 ).putAll ( v8 ); // invoke-virtual {v7, v8}, Ljava/util/HashMap;->putAll(Ljava/util/Map;)V
/* .line 1235 */
(( java.util.ArrayList ) v1 ).add ( v6 ); // invoke-virtual {v1, v6}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z
/* .line 1230 */
/* nop */
} // .end local v5 # "state":Lcom/miui/server/security/SecurityUserState;
} // .end local v6 # "userState":Lcom/miui/server/security/SecurityUserState;
/* add-int/lit8 v4, v4, 0x1 */
/* .line 1237 */
} // .end local v3 # "size":I
} // .end local v4 # "i":I
} // :cond_0
/* monitor-exit v2 */
/* :try_end_1 */
/* .catchall {:try_start_1 ..:try_end_1} :catchall_0 */
/* .line 1238 */
try { // :try_start_2
v2 = this.mSettingsFile;
(( android.util.AtomicFile ) v2 ).startWrite ( ); // invoke-virtual {v2}, Landroid/util/AtomicFile;->startWrite()Ljava/io/FileOutputStream;
/* move-object v0, v2 */
/* .line 1239 */
/* new-instance v2, Lcom/android/internal/util/FastXmlSerializer; */
/* invoke-direct {v2}, Lcom/android/internal/util/FastXmlSerializer;-><init>()V */
/* .line 1240 */
/* .local v2, "out":Lorg/xmlpull/v1/XmlSerializer; */
/* const-string/jumbo v3, "utf-8" */
/* .line 1241 */
int v3 = 1; // const/4 v3, 0x1
java.lang.Boolean .valueOf ( v3 );
int v5 = 0; // const/4 v5, 0x0
/* .line 1242 */
final String v4 = "http://xmlpull.org/v1/doc/features.html#indent-output"; // const-string v4, "http://xmlpull.org/v1/doc/features.html#indent-output"
/* .line 1243 */
final String v3 = "packages"; // const-string v3, "packages"
/* .line 1244 */
/* const-string/jumbo v3, "updateVersion" */
final String v4 = "1.0"; // const-string v4, "1.0"
/* .line 1245 */
(( java.util.ArrayList ) v1 ).iterator ( ); // invoke-virtual {v1}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;
v4 = } // :goto_1
if ( v4 != null) { // if-eqz v4, :cond_3
/* check-cast v4, Lcom/miui/server/security/SecurityUserState; */
/* .line 1246 */
/* .local v4, "userState":Lcom/miui/server/security/SecurityUserState; */
v6 = this.mPackages;
(( java.util.HashMap ) v6 ).values ( ); // invoke-virtual {v6}, Ljava/util/HashMap;->values()Ljava/util/Collection;
v7 = } // :goto_2
if ( v7 != null) { // if-eqz v7, :cond_2
/* check-cast v7, Lcom/miui/server/security/SecurityPackageSettings; */
/* .line 1247 */
/* .local v7, "ps":Lcom/miui/server/security/SecurityPackageSettings; */
v8 = this.name;
v8 = android.text.TextUtils .isEmpty ( v8 );
if ( v8 != null) { // if-eqz v8, :cond_1
/* .line 1248 */
final String v8 = "SecurityManagerService"; // const-string v8, "SecurityManagerService"
/* const-string/jumbo v9, "write current package name is empty, skip" */
android.util.Slog .i ( v8,v9 );
/* .line 1249 */
/* .line 1251 */
} // :cond_1
final String v8 = "package"; // const-string v8, "package"
/* .line 1252 */
final String v8 = "name"; // const-string v8, "name"
v9 = this.name;
/* .line 1253 */
final String v8 = "accessControl"; // const-string v8, "accessControl"
/* iget-boolean v9, v7, Lcom/miui/server/security/SecurityPackageSettings;->accessControl:Z */
java.lang.String .valueOf ( v9 );
/* .line 1254 */
final String v8 = "childrenControl"; // const-string v8, "childrenControl"
/* iget-boolean v9, v7, Lcom/miui/server/security/SecurityPackageSettings;->childrenControl:Z */
java.lang.String .valueOf ( v9 );
/* .line 1255 */
final String v8 = "maskNotification"; // const-string v8, "maskNotification"
/* iget-boolean v9, v7, Lcom/miui/server/security/SecurityPackageSettings;->maskNotification:Z */
java.lang.String .valueOf ( v9 );
/* .line 1256 */
final String v8 = "isPrivacyApp"; // const-string v8, "isPrivacyApp"
/* iget-boolean v9, v7, Lcom/miui/server/security/SecurityPackageSettings;->isPrivacyApp:Z */
java.lang.String .valueOf ( v9 );
/* .line 1258 */
final String v8 = "isDarkModeChecked"; // const-string v8, "isDarkModeChecked"
/* iget-boolean v9, v7, Lcom/miui/server/security/SecurityPackageSettings;->isDarkModeChecked:Z */
java.lang.String .valueOf ( v9 );
/* .line 1260 */
final String v8 = "isGameStorageApp"; // const-string v8, "isGameStorageApp"
/* iget-boolean v9, v7, Lcom/miui/server/security/SecurityPackageSettings;->isGameStorageApp:Z */
java.lang.String .valueOf ( v9 );
/* .line 1261 */
final String v8 = "isRemindForRelaunch"; // const-string v8, "isRemindForRelaunch"
/* iget-boolean v9, v7, Lcom/miui/server/security/SecurityPackageSettings;->isRemindForRelaunch:Z */
java.lang.String .valueOf ( v9 );
/* .line 1262 */
final String v8 = "isRelaunchWhenFolded"; // const-string v8, "isRelaunchWhenFolded"
/* iget-boolean v9, v7, Lcom/miui/server/security/SecurityPackageSettings;->isRelaunchWhenFolded:Z */
java.lang.String .valueOf ( v9 );
/* .line 1263 */
final String v8 = "isScRelaunchConfirm"; // const-string v8, "isScRelaunchConfirm"
/* iget-boolean v9, v7, Lcom/miui/server/security/SecurityPackageSettings;->isScRelaunchConfirm:Z */
java.lang.String .valueOf ( v9 );
/* .line 1264 */
/* const-string/jumbo v8, "u" */
/* iget v9, v4, Lcom/miui/server/security/SecurityUserState;->userHandle:I */
java.lang.String .valueOf ( v9 );
/* .line 1265 */
final String v8 = "package"; // const-string v8, "package"
/* .line 1266 */
/* nop */
} // .end local v7 # "ps":Lcom/miui/server/security/SecurityPackageSettings;
/* goto/16 :goto_2 */
/* .line 1267 */
} // .end local v4 # "userState":Lcom/miui/server/security/SecurityUserState;
} // :cond_2
/* goto/16 :goto_1 */
/* .line 1268 */
} // :cond_3
final String v3 = "packages"; // const-string v3, "packages"
/* .line 1269 */
/* .line 1270 */
v3 = this.mSettingsFile;
(( android.util.AtomicFile ) v3 ).finishWrite ( v0 ); // invoke-virtual {v3, v0}, Landroid/util/AtomicFile;->finishWrite(Ljava/io/FileOutputStream;)V
/* :try_end_2 */
/* .catch Ljava/lang/Exception; {:try_start_2 ..:try_end_2} :catch_0 */
/* .line 1276 */
} // .end local v1 # "userStates":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/miui/server/security/SecurityUserState;>;"
} // .end local v2 # "out":Lorg/xmlpull/v1/XmlSerializer;
/* .line 1237 */
/* .restart local v1 # "userStates":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/miui/server/security/SecurityUserState;>;" */
/* :catchall_0 */
/* move-exception v3 */
try { // :try_start_3
/* monitor-exit v2 */
/* :try_end_3 */
/* .catchall {:try_start_3 ..:try_end_3} :catchall_0 */
} // .end local v0 # "fos":Ljava/io/FileOutputStream;
} // .end local p0 # "this":Lcom/miui/server/SecurityManagerService;
try { // :try_start_4
/* throw v3 */
/* :try_end_4 */
/* .catch Ljava/lang/Exception; {:try_start_4 ..:try_end_4} :catch_0 */
/* .line 1271 */
} // .end local v1 # "userStates":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/miui/server/security/SecurityUserState;>;"
/* .restart local v0 # "fos":Ljava/io/FileOutputStream; */
/* .restart local p0 # "this":Lcom/miui/server/SecurityManagerService; */
/* :catch_0 */
/* move-exception v1 */
/* .line 1272 */
/* .local v1, "e1":Ljava/lang/Exception; */
final String v2 = "SecurityManagerService"; // const-string v2, "SecurityManagerService"
final String v3 = "Error writing package settings file"; // const-string v3, "Error writing package settings file"
android.util.Log .e ( v2,v3,v1 );
/* .line 1273 */
if ( v0 != null) { // if-eqz v0, :cond_4
/* .line 1274 */
v2 = this.mSettingsFile;
(( android.util.AtomicFile ) v2 ).failWrite ( v0 ); // invoke-virtual {v2, v0}, Landroid/util/AtomicFile;->failWrite(Ljava/io/FileOutputStream;)V
/* .line 1277 */
} // .end local v1 # "e1":Ljava/lang/Exception;
} // :cond_4
} // :goto_3
return;
} // .end method
