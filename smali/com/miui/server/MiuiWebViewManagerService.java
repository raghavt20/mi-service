public class com.miui.server.MiuiWebViewManagerService extends miui.webview.IMiuiWebViewManager$Stub {
	 /* .source "MiuiWebViewManagerService.java" */
	 /* # annotations */
	 /* .annotation system Ldalvik/annotation/MemberClasses; */
	 /* value = { */
	 /* Lcom/miui/server/MiuiWebViewManagerService$Lifecycle; */
	 /* } */
} // .end annotation
/* # static fields */
private static final java.util.List EXEMPT_APPS;
/* .annotation system Ldalvik/annotation/Signature; */
/* value = { */
/* "Ljava/util/List<", */
/* "Ljava/lang/String;", */
/* ">;" */
/* } */
} // .end annotation
} // .end field
private static final java.lang.String SEND_TIME_KEY;
private static final java.lang.String TAG;
/* # instance fields */
private Integer MSG_RESTART_WEBVIEW;
private final android.content.Context mContext;
private android.os.Handler mHandler;
private android.os.HandlerThread mHandlerThread;
/* # direct methods */
static Integer -$$Nest$fgetMSG_RESTART_WEBVIEW ( com.miui.server.MiuiWebViewManagerService p0 ) { //bridge//synthethic
/* .locals 0 */
/* iget p0, p0, Lcom/miui/server/MiuiWebViewManagerService;->MSG_RESTART_WEBVIEW:I */
} // .end method
static android.content.Context -$$Nest$fgetmContext ( com.miui.server.MiuiWebViewManagerService p0 ) { //bridge//synthethic
/* .locals 0 */
p0 = this.mContext;
} // .end method
static java.util.List -$$Nest$mcollectWebViewProcesses ( com.miui.server.MiuiWebViewManagerService p0, com.android.server.am.ActivityManagerService p1 ) { //bridge//synthethic
/* .locals 0 */
/* invoke-direct {p0, p1}, Lcom/miui/server/MiuiWebViewManagerService;->collectWebViewProcesses(Lcom/android/server/am/ActivityManagerService;)Ljava/util/List; */
} // .end method
static java.util.List -$$Nest$sfgetEXEMPT_APPS ( ) { //bridge//synthethic
/* .locals 1 */
v0 = com.miui.server.MiuiWebViewManagerService.EXEMPT_APPS;
} // .end method
static com.miui.server.MiuiWebViewManagerService ( ) {
/* .locals 1 */
/* .line 28 */
/* new-instance v0, Lcom/miui/server/MiuiWebViewManagerService$1; */
/* invoke-direct {v0}, Lcom/miui/server/MiuiWebViewManagerService$1;-><init>()V */
return;
} // .end method
public com.miui.server.MiuiWebViewManagerService ( ) {
/* .locals 2 */
/* .param p1, "context" # Landroid/content/Context; */
/* .line 40 */
/* invoke-direct {p0}, Lmiui/webview/IMiuiWebViewManager$Stub;-><init>()V */
/* .line 37 */
/* const/16 v0, 0x7f */
/* iput v0, p0, Lcom/miui/server/MiuiWebViewManagerService;->MSG_RESTART_WEBVIEW:I */
/* .line 41 */
this.mContext = p1;
/* .line 42 */
/* new-instance v0, Landroid/os/HandlerThread; */
final String v1 = "MiuiWebViewWorker"; // const-string v1, "MiuiWebViewWorker"
/* invoke-direct {v0, v1}, Landroid/os/HandlerThread;-><init>(Ljava/lang/String;)V */
this.mHandlerThread = v0;
/* .line 43 */
(( android.os.HandlerThread ) v0 ).start ( ); // invoke-virtual {v0}, Landroid/os/HandlerThread;->start()V
/* .line 44 */
/* new-instance v0, Lcom/miui/server/MiuiWebViewManagerService$2; */
v1 = this.mHandlerThread;
(( android.os.HandlerThread ) v1 ).getLooper ( ); // invoke-virtual {v1}, Landroid/os/HandlerThread;->getLooper()Landroid/os/Looper;
/* invoke-direct {v0, p0, v1}, Lcom/miui/server/MiuiWebViewManagerService$2;-><init>(Lcom/miui/server/MiuiWebViewManagerService;Landroid/os/Looper;)V */
this.mHandler = v0;
/* .line 76 */
return;
} // .end method
private java.util.List collectWebViewProcesses ( com.android.server.am.ActivityManagerService p0 ) {
/* .locals 14 */
/* .param p1, "am" # Lcom/android/server/am/ActivityManagerService; */
/* .annotation system Ldalvik/annotation/Signature; */
/* value = { */
/* "(", */
/* "Lcom/android/server/am/ActivityManagerService;", */
/* ")", */
/* "Ljava/util/List<", */
/* "Ljava/lang/String;", */
/* ">;" */
/* } */
} // .end annotation
/* .line 79 */
final String v0 = "com.google.android.webview"; // const-string v0, "com.google.android.webview"
/* .line 80 */
/* .local v0, "webViewComponentName":Ljava/lang/String; */
/* new-instance v1, Ljava/util/ArrayList; */
/* invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V */
/* .line 82 */
/* .local v1, "pkgsToKill":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;" */
int v2 = 0; // const/4 v2, 0x0
try { // :try_start_0
v3 = com.miui.app.am.ActivityManagerServiceProxy.collectProcesses;
int v4 = 4; // const/4 v4, 0x4
/* new-array v4, v4, [Ljava/lang/Object; */
int v5 = 0; // const/4 v5, 0x0
/* aput-object v2, v4, v5 */
/* .line 83 */
java.lang.Integer .valueOf ( v5 );
int v7 = 1; // const/4 v7, 0x1
/* aput-object v6, v4, v7 */
java.lang.Boolean .valueOf ( v5 );
int v8 = 2; // const/4 v8, 0x2
/* aput-object v6, v4, v8 */
/* new-array v6, v5, [Ljava/lang/String; */
int v8 = 3; // const/4 v8, 0x3
/* aput-object v6, v4, v8 */
(( com.xiaomi.reflect.RefMethod ) v3 ).invoke ( p1, v4 ); // invoke-virtual {v3, p1, v4}, Lcom/xiaomi/reflect/RefMethod;->invoke(Ljava/lang/Object;[Ljava/lang/Object;)Ljava/lang/Object;
/* check-cast v3, Ljava/util/ArrayList; */
/* .line 84 */
/* .local v3, "procs":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/android/server/am/ProcessRecord;>;" */
/* if-nez v3, :cond_0 */
/* .line 85 */
/* .line 87 */
} // :cond_0
v4 = com.miui.app.am.ActivityManagerServiceProxy.mProcLock;
(( com.xiaomi.reflect.RefObject ) v4 ).get ( p1 ); // invoke-virtual {v4, p1}, Lcom/xiaomi/reflect/RefObject;->get(Ljava/lang/Object;)Ljava/lang/Object;
/* monitor-enter v4 */
/* :try_end_0 */
/* .catch Ljava/lang/Exception; {:try_start_0 ..:try_end_0} :catch_0 */
/* .line 88 */
try { // :try_start_1
v6 = (( java.util.ArrayList ) v3 ).size ( ); // invoke-virtual {v3}, Ljava/util/ArrayList;->size()I
/* sub-int/2addr v6, v7 */
/* .local v6, "i":I */
} // :goto_0
/* if-ltz v6, :cond_4 */
/* .line 89 */
(( java.util.ArrayList ) v3 ).get ( v6 ); // invoke-virtual {v3, v6}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;
/* check-cast v7, Lcom/android/server/am/ProcessRecord; */
/* .line 90 */
/* .local v7, "pr":Lcom/android/server/am/ProcessRecord; */
v8 = com.miui.app.am.ProcessRecordProxy.getPkgDeps;
/* new-array v9, v5, [Ljava/lang/Object; */
(( com.xiaomi.reflect.RefMethod ) v8 ).invoke ( v7, v9 ); // invoke-virtual {v8, v7, v9}, Lcom/xiaomi/reflect/RefMethod;->invoke(Ljava/lang/Object;[Ljava/lang/Object;)Ljava/lang/Object;
/* check-cast v8, Landroid/util/ArraySet; */
/* .line 91 */
/* .local v8, "deps":Landroid/util/ArraySet;, "Landroid/util/ArraySet<Ljava/lang/String;>;" */
v9 = com.miui.app.am.ProcessRecordProxy.info;
(( com.xiaomi.reflect.RefObject ) v9 ).get ( v7 ); // invoke-virtual {v9, v7}, Lcom/xiaomi/reflect/RefObject;->get(Ljava/lang/Object;)Ljava/lang/Object;
/* check-cast v9, Landroid/content/pm/ApplicationInfo; */
/* .line 92 */
/* .local v9, "info":Landroid/content/pm/ApplicationInfo; */
if ( v8 != null) { // if-eqz v8, :cond_3
if ( v9 != null) { // if-eqz v9, :cond_3
v10 = this.packageName;
/* if-nez v10, :cond_1 */
/* .line 93 */
/* .line 95 */
} // :cond_1
v10 = (( android.util.ArraySet ) v8 ).contains ( v0 ); // invoke-virtual {v8, v0}, Landroid/util/ArraySet;->contains(Ljava/lang/Object;)Z
/* if-nez v10, :cond_2 */
v10 = this.packageName;
final String v11 = "com.android.browser"; // const-string v11, "com.android.browser"
/* .line 96 */
v10 = (( java.lang.String ) v10 ).equals ( v11 ); // invoke-virtual {v10, v11}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z
if ( v10 != null) { // if-eqz v10, :cond_3
/* .line 97 */
} // :cond_2
v10 = this.packageName;
/* .line 98 */
/* .local v10, "pkgName":Ljava/lang/String; */
/* new-instance v11, Ljava/lang/StringBuilder; */
/* invoke-direct {v11}, Ljava/lang/StringBuilder;-><init>()V */
(( java.lang.StringBuilder ) v11 ).append ( v10 ); // invoke-virtual {v11, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
final String v12 = "#"; // const-string v12, "#"
(( java.lang.StringBuilder ) v11 ).append ( v12 ); // invoke-virtual {v11, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
v12 = com.miui.app.am.ProcessRecordProxy.getPid;
/* new-array v13, v5, [Ljava/lang/Object; */
(( com.xiaomi.reflect.RefMethod ) v12 ).invoke ( v7, v13 ); // invoke-virtual {v12, v7, v13}, Lcom/xiaomi/reflect/RefMethod;->invoke(Ljava/lang/Object;[Ljava/lang/Object;)Ljava/lang/Object;
(( java.lang.StringBuilder ) v11 ).append ( v12 ); // invoke-virtual {v11, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v11 ).toString ( ); // invoke-virtual {v11}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
/* .line 88 */
} // .end local v7 # "pr":Lcom/android/server/am/ProcessRecord;
} // .end local v8 # "deps":Landroid/util/ArraySet;, "Landroid/util/ArraySet<Ljava/lang/String;>;"
} // .end local v9 # "info":Landroid/content/pm/ApplicationInfo;
} // .end local v10 # "pkgName":Ljava/lang/String;
} // :cond_3
} // :goto_1
/* add-int/lit8 v6, v6, -0x1 */
/* .line 101 */
} // .end local v6 # "i":I
} // :cond_4
/* monitor-exit v4 */
/* .line 105 */
} // .end local v3 # "procs":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/android/server/am/ProcessRecord;>;"
/* nop */
/* .line 106 */
/* .line 101 */
/* .restart local v3 # "procs":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/android/server/am/ProcessRecord;>;" */
/* :catchall_0 */
/* move-exception v5 */
/* monitor-exit v4 */
/* :try_end_1 */
/* .catchall {:try_start_1 ..:try_end_1} :catchall_0 */
} // .end local v0 # "webViewComponentName":Ljava/lang/String;
} // .end local v1 # "pkgsToKill":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;"
} // .end local p0 # "this":Lcom/miui/server/MiuiWebViewManagerService;
} // .end local p1 # "am":Lcom/android/server/am/ActivityManagerService;
try { // :try_start_2
/* throw v5 */
/* :try_end_2 */
/* .catch Ljava/lang/Exception; {:try_start_2 ..:try_end_2} :catch_0 */
/* .line 102 */
} // .end local v3 # "procs":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/android/server/am/ProcessRecord;>;"
/* .restart local v0 # "webViewComponentName":Ljava/lang/String; */
/* .restart local v1 # "pkgsToKill":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;" */
/* .restart local p0 # "this":Lcom/miui/server/MiuiWebViewManagerService; */
/* .restart local p1 # "am":Lcom/android/server/am/ActivityManagerService; */
/* :catch_0 */
/* move-exception v3 */
/* .line 103 */
/* .local v3, "e":Ljava/lang/Exception; */
final String v4 = "MiuiWebViewManagerService"; // const-string v4, "MiuiWebViewManagerService"
final String v5 = "MiuiWebViewManagerService.collectWebViewProcesses failed"; // const-string v5, "MiuiWebViewManagerService.collectWebViewProcesses failed"
android.util.Log .w ( v4,v5,v3 );
/* .line 104 */
} // .end method
/* # virtual methods */
public void restartWebViewProcesses ( ) {
/* .locals 5 */
/* .line 111 */
android.os.Message .obtain ( );
/* .line 112 */
/* .local v0, "msg":Landroid/os/Message; */
/* iget v1, p0, Lcom/miui/server/MiuiWebViewManagerService;->MSG_RESTART_WEBVIEW:I */
/* iput v1, v0, Landroid/os/Message;->what:I */
/* .line 113 */
/* new-instance v1, Landroid/os/Bundle; */
/* invoke-direct {v1}, Landroid/os/Bundle;-><init>()V */
/* .line 114 */
/* .local v1, "data":Landroid/os/Bundle; */
/* const-string/jumbo v2, "sendTime" */
java.lang.System .currentTimeMillis ( );
/* move-result-wide v3 */
(( android.os.Bundle ) v1 ).putLong ( v2, v3, v4 ); // invoke-virtual {v1, v2, v3, v4}, Landroid/os/Bundle;->putLong(Ljava/lang/String;J)V
/* .line 115 */
(( android.os.Message ) v0 ).setData ( v1 ); // invoke-virtual {v0, v1}, Landroid/os/Message;->setData(Landroid/os/Bundle;)V
/* .line 116 */
v2 = this.mHandler;
(( android.os.Handler ) v2 ).sendMessage ( v0 ); // invoke-virtual {v2, v0}, Landroid/os/Handler;->sendMessage(Landroid/os/Message;)Z
/* .line 117 */
final String v2 = "MiuiWebViewManagerService"; // const-string v2, "MiuiWebViewManagerService"
final String v3 = "restartWebViewProcesses called"; // const-string v3, "restartWebViewProcesses called"
android.util.Log .i ( v2,v3 );
/* .line 118 */
return;
} // .end method
