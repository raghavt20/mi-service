public class com.miui.server.MiuiCldService extends miui.hardware.ICldManager$Stub {
	 /* .source "MiuiCldService.java" */
	 /* # annotations */
	 /* .annotation system Ldalvik/annotation/MemberClasses; */
	 /* value = { */
	 /* Lcom/miui/server/MiuiCldService$HALCallback;, */
	 /* Lcom/miui/server/MiuiCldService$Lifecycle; */
	 /* } */
} // .end annotation
/* # static fields */
private static final Integer CLD_OPER_ABORTED;
private static final Integer CLD_OPER_DONE;
private static final Integer CLD_OPER_FATAL_ERR;
private static final Integer CLD_OPER_NOT_SUPPORTED;
private static final Integer CLD_OPER_PROCESSING;
private static final Integer FRAG_ANALYSIS;
private static final Integer FRAG_LEVEL_LOW;
private static final Integer FRAG_LEVEL_MEDIUM;
private static final Integer FRAG_LEVEL_SERVERE;
private static final Integer FRAG_LEVEL_UNKNOWN;
private static final Integer GET_CLD_OPERATION_STATUS;
private static final Integer GET_FRAGMENT_LEVEL;
private static final Long HALF_A_DAY_MS;
private static final java.lang.String HAL_DEFAULT;
private static final java.lang.String HAL_INTERFACE_DESCRIPTOR;
private static final java.lang.String HAL_SERVICE_NAME;
private static final Integer IS_CLD_SUPPORTED;
private static final java.lang.String MIUI_CLD_PROCESSED_DONE;
public static final java.lang.String SERVICE_NAME;
private static final Integer SET_CALLBACK;
private static final java.lang.String TAG;
private static final Integer TRIGGER_CLD;
/* # instance fields */
private final android.content.Context mContext;
private com.miui.server.MiuiCldService$HALCallback mHALCallback;
private java.util.Date sLastCld;
/* # direct methods */
static void -$$Nest$mreportCldProcessedBroadcast ( com.miui.server.MiuiCldService p0, Integer p1 ) { //bridge//synthethic
	 /* .locals 0 */
	 /* invoke-direct {p0, p1}, Lcom/miui/server/MiuiCldService;->reportCldProcessedBroadcast(I)V */
	 return;
} // .end method
static com.miui.server.MiuiCldService ( ) {
	 /* .locals 1 */
	 /* .line 29 */
	 /* const-class v0, Lcom/miui/server/MiuiCldService; */
	 (( java.lang.Class ) v0 ).getSimpleName ( ); // invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;
	 return;
} // .end method
public com.miui.server.MiuiCldService ( ) {
	 /* .locals 3 */
	 /* .param p1, "context" # Landroid/content/Context; */
	 /* .line 60 */
	 /* invoke-direct {p0}, Lmiui/hardware/ICldManager$Stub;-><init>()V */
	 /* .line 61 */
	 this.mContext = p1;
	 /* .line 62 */
	 /* new-instance v0, Ljava/util/Date; */
	 /* const-wide/16 v1, 0x0 */
	 /* invoke-direct {v0, v1, v2}, Ljava/util/Date;-><init>(J)V */
	 this.sLastCld = v0;
	 /* .line 63 */
	 /* invoke-direct {p0}, Lcom/miui/server/MiuiCldService;->initCallback()V */
	 /* .line 64 */
	 return;
} // .end method
private void initCallback ( ) {
	 /* .locals 5 */
	 /* .line 74 */
	 /* const-string/jumbo v0, "vendor.xiaomi.hardware.cld@1.0::ICld" */
	 /* new-instance v1, Lcom/miui/server/MiuiCldService$HALCallback; */
	 /* invoke-direct {v1, p0}, Lcom/miui/server/MiuiCldService$HALCallback;-><init>(Lcom/miui/server/MiuiCldService;)V */
	 this.mHALCallback = v1;
	 /* .line 75 */
	 /* new-instance v1, Landroid/os/HwParcel; */
	 /* invoke-direct {v1}, Landroid/os/HwParcel;-><init>()V */
	 /* .line 77 */
	 /* .local v1, "hidl_reply":Landroid/os/HwParcel; */
	 try { // :try_start_0
		 final String v2 = "default"; // const-string v2, "default"
		 android.os.HwBinder .getService ( v0,v2 );
		 /* .line 78 */
		 /* .local v2, "hwService":Landroid/os/IHwBinder; */
		 if ( v2 != null) { // if-eqz v2, :cond_0
			 /* .line 79 */
			 /* new-instance v3, Landroid/os/HwParcel; */
			 /* invoke-direct {v3}, Landroid/os/HwParcel;-><init>()V */
			 /* .line 80 */
			 /* .local v3, "hidl_request":Landroid/os/HwParcel; */
			 (( android.os.HwParcel ) v3 ).writeInterfaceToken ( v0 ); // invoke-virtual {v3, v0}, Landroid/os/HwParcel;->writeInterfaceToken(Ljava/lang/String;)V
			 /* .line 81 */
			 v0 = this.mHALCallback;
			 (( com.miui.server.MiuiCldService$HALCallback ) v0 ).asBinder ( ); // invoke-virtual {v0}, Lcom/miui/server/MiuiCldService$HALCallback;->asBinder()Landroid/os/IHwBinder;
			 (( android.os.HwParcel ) v3 ).writeStrongBinder ( v0 ); // invoke-virtual {v3, v0}, Landroid/os/HwParcel;->writeStrongBinder(Landroid/os/IHwBinder;)V
			 /* .line 82 */
			 int v0 = 5; // const/4 v0, 0x5
			 int v4 = 0; // const/4 v4, 0x0
			 /* .line 83 */
			 (( android.os.HwParcel ) v1 ).verifySuccess ( ); // invoke-virtual {v1}, Landroid/os/HwParcel;->verifySuccess()V
			 /* .line 84 */
			 (( android.os.HwParcel ) v3 ).releaseTemporaryStorage ( ); // invoke-virtual {v3}, Landroid/os/HwParcel;->releaseTemporaryStorage()V
			 /* :try_end_0 */
			 /* .catch Ljava/lang/Exception; {:try_start_0 ..:try_end_0} :catch_0 */
			 /* .catchall {:try_start_0 ..:try_end_0} :catchall_0 */
			 /* .line 90 */
			 (( android.os.HwParcel ) v1 ).release ( ); // invoke-virtual {v1}, Landroid/os/HwParcel;->release()V
			 /* .line 85 */
			 return;
			 /* .line 90 */
		 } // .end local v2 # "hwService":Landroid/os/IHwBinder;
	 } // .end local v3 # "hidl_request":Landroid/os/HwParcel;
} // :cond_0
/* nop */
} // :goto_0
(( android.os.HwParcel ) v1 ).release ( ); // invoke-virtual {v1}, Landroid/os/HwParcel;->release()V
/* .line 91 */
/* .line 90 */
/* :catchall_0 */
/* move-exception v0 */
/* .line 87 */
/* :catch_0 */
/* move-exception v0 */
/* .line 88 */
/* .local v0, "e":Ljava/lang/Exception; */
try { // :try_start_1
v2 = com.miui.server.MiuiCldService.TAG;
/* new-instance v3, Ljava/lang/StringBuilder; */
/* invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V */
final String v4 = "Transaction failed: "; // const-string v4, "Transaction failed: "
(( java.lang.StringBuilder ) v3 ).append ( v4 ); // invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v3 ).append ( v0 ); // invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v3 ).toString ( ); // invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
android.util.Slog .e ( v2,v3 );
/* :try_end_1 */
/* .catchall {:try_start_1 ..:try_end_1} :catchall_0 */
/* .line 90 */
/* nop */
} // .end local v0 # "e":Ljava/lang/Exception;
/* .line 93 */
} // :goto_1
v0 = com.miui.server.MiuiCldService.TAG;
final String v2 = "initCallback failed."; // const-string v2, "initCallback failed."
android.util.Slog .e ( v0,v2 );
/* .line 94 */
return;
/* .line 90 */
} // :goto_2
(( android.os.HwParcel ) v1 ).release ( ); // invoke-virtual {v1}, Landroid/os/HwParcel;->release()V
/* .line 91 */
/* throw v0 */
} // .end method
private void reportCldProcessedBroadcast ( Integer p0 ) {
/* .locals 4 */
/* .param p1, "level" # I */
/* .line 67 */
/* new-instance v0, Landroid/content/Intent; */
final String v1 = "miui.intent.action.CLD_PROCESSED_DONE"; // const-string v1, "miui.intent.action.CLD_PROCESSED_DONE"
/* invoke-direct {v0, v1}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V */
/* .line 68 */
/* .local v0, "intent":Landroid/content/Intent; */
/* const-string/jumbo v1, "status" */
java.lang.Integer .toString ( p1 );
(( android.content.Intent ) v0 ).putExtra ( v1, v2 ); // invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;
/* .line 69 */
v1 = com.miui.server.MiuiCldService.TAG;
/* new-instance v2, Ljava/lang/StringBuilder; */
/* invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V */
final String v3 = "Send CLD broadcast, status = "; // const-string v3, "Send CLD broadcast, status = "
(( java.lang.StringBuilder ) v2 ).append ( v3 ); // invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v2 ).append ( p1 ); // invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v2 ).toString ( ); // invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
android.util.Slog .d ( v1,v2 );
/* .line 70 */
v1 = this.mContext;
(( android.content.Context ) v1 ).sendBroadcast ( v0 ); // invoke-virtual {v1, v0}, Landroid/content/Context;->sendBroadcast(Landroid/content/Intent;)V
/* .line 71 */
return;
} // .end method
/* # virtual methods */
public Integer getCldOperationStatus ( ) {
/* .locals 8 */
/* .line 187 */
/* const-string/jumbo v0, "vendor.xiaomi.hardware.cld@1.0::ICld" */
/* new-instance v1, Landroid/os/HwParcel; */
/* invoke-direct {v1}, Landroid/os/HwParcel;-><init>()V */
/* .line 189 */
/* .local v1, "hidl_reply":Landroid/os/HwParcel; */
int v2 = 0; // const/4 v2, 0x0
try { // :try_start_0
final String v3 = "default"; // const-string v3, "default"
android.os.HwBinder .getService ( v0,v3 );
/* .line 190 */
/* .local v3, "hwService":Landroid/os/IHwBinder; */
if ( v3 != null) { // if-eqz v3, :cond_2
/* .line 191 */
/* new-instance v4, Landroid/os/HwParcel; */
/* invoke-direct {v4}, Landroid/os/HwParcel;-><init>()V */
/* .line 192 */
/* .local v4, "hidl_request":Landroid/os/HwParcel; */
(( android.os.HwParcel ) v4 ).writeInterfaceToken ( v0 ); // invoke-virtual {v4, v0}, Landroid/os/HwParcel;->writeInterfaceToken(Ljava/lang/String;)V
/* .line 193 */
int v0 = 4; // const/4 v0, 0x4
/* .line 194 */
(( android.os.HwParcel ) v1 ).verifySuccess ( ); // invoke-virtual {v1}, Landroid/os/HwParcel;->verifySuccess()V
/* .line 195 */
(( android.os.HwParcel ) v4 ).releaseTemporaryStorage ( ); // invoke-virtual {v4}, Landroid/os/HwParcel;->releaseTemporaryStorage()V
/* .line 196 */
v5 = (( android.os.HwParcel ) v1 ).readInt32 ( ); // invoke-virtual {v1}, Landroid/os/HwParcel;->readInt32()I
/* :try_end_0 */
/* .catch Ljava/lang/Exception; {:try_start_0 ..:try_end_0} :catch_0 */
/* .catchall {:try_start_0 ..:try_end_0} :catchall_0 */
/* .line 197 */
/* .local v5, "val":I */
/* if-ltz v5, :cond_1 */
/* if-le v5, v0, :cond_0 */
/* .line 202 */
} // :cond_0
/* nop */
/* .line 207 */
(( android.os.HwParcel ) v1 ).release ( ); // invoke-virtual {v1}, Landroid/os/HwParcel;->release()V
/* .line 202 */
/* .line 198 */
} // :cond_1
} // :goto_0
try { // :try_start_1
v0 = com.miui.server.MiuiCldService.TAG;
/* new-instance v6, Ljava/lang/StringBuilder; */
/* invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V */
final String v7 = "Got invalid operation status: "; // const-string v7, "Got invalid operation status: "
(( java.lang.StringBuilder ) v6 ).append ( v7 ); // invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v6 ).append ( v5 ); // invoke-virtual {v6, v5}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v6 ).toString ( ); // invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
android.util.Slog .e ( v0,v6 );
/* :try_end_1 */
/* .catch Ljava/lang/Exception; {:try_start_1 ..:try_end_1} :catch_0 */
/* .catchall {:try_start_1 ..:try_end_1} :catchall_0 */
/* .line 199 */
/* nop */
/* .line 207 */
(( android.os.HwParcel ) v1 ).release ( ); // invoke-virtual {v1}, Landroid/os/HwParcel;->release()V
/* .line 199 */
/* .line 207 */
} // .end local v3 # "hwService":Landroid/os/IHwBinder;
} // .end local v4 # "hidl_request":Landroid/os/HwParcel;
} // .end local v5 # "val":I
} // :cond_2
/* nop */
} // :goto_1
(( android.os.HwParcel ) v1 ).release ( ); // invoke-virtual {v1}, Landroid/os/HwParcel;->release()V
/* .line 208 */
/* .line 207 */
/* :catchall_0 */
/* move-exception v0 */
/* .line 204 */
/* :catch_0 */
/* move-exception v0 */
/* .line 205 */
/* .local v0, "e":Ljava/lang/Exception; */
try { // :try_start_2
v3 = com.miui.server.MiuiCldService.TAG;
/* new-instance v4, Ljava/lang/StringBuilder; */
/* invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V */
final String v5 = "Transaction failed: "; // const-string v5, "Transaction failed: "
(( java.lang.StringBuilder ) v4 ).append ( v5 ); // invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v4 ).append ( v0 ); // invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v4 ).toString ( ); // invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
android.util.Slog .e ( v3,v4 );
/* :try_end_2 */
/* .catchall {:try_start_2 ..:try_end_2} :catchall_0 */
/* .line 207 */
/* nop */
} // .end local v0 # "e":Ljava/lang/Exception;
/* .line 210 */
} // :goto_2
v0 = com.miui.server.MiuiCldService.TAG;
final String v3 = "Failed calling getCldOperationStatus."; // const-string v3, "Failed calling getCldOperationStatus."
android.util.Slog .e ( v0,v3 );
/* .line 211 */
/* .line 207 */
} // :goto_3
(( android.os.HwParcel ) v1 ).release ( ); // invoke-virtual {v1}, Landroid/os/HwParcel;->release()V
/* .line 208 */
/* throw v0 */
} // .end method
public Integer getFragmentLevel ( ) {
/* .locals 8 */
/* .line 128 */
/* const-string/jumbo v0, "vendor.xiaomi.hardware.cld@1.0::ICld" */
/* new-instance v1, Landroid/os/HwParcel; */
/* invoke-direct {v1}, Landroid/os/HwParcel;-><init>()V */
/* .line 130 */
/* .local v1, "hidl_reply":Landroid/os/HwParcel; */
int v2 = 4; // const/4 v2, 0x4
try { // :try_start_0
final String v3 = "default"; // const-string v3, "default"
android.os.HwBinder .getService ( v0,v3 );
/* .line 131 */
/* .local v3, "hwService":Landroid/os/IHwBinder; */
if ( v3 != null) { // if-eqz v3, :cond_2
/* .line 132 */
/* new-instance v4, Landroid/os/HwParcel; */
/* invoke-direct {v4}, Landroid/os/HwParcel;-><init>()V */
/* .line 133 */
/* .local v4, "hidl_request":Landroid/os/HwParcel; */
(( android.os.HwParcel ) v4 ).writeInterfaceToken ( v0 ); // invoke-virtual {v4, v0}, Landroid/os/HwParcel;->writeInterfaceToken(Ljava/lang/String;)V
/* .line 134 */
int v0 = 2; // const/4 v0, 0x2
int v5 = 0; // const/4 v5, 0x0
/* .line 135 */
(( android.os.HwParcel ) v1 ).verifySuccess ( ); // invoke-virtual {v1}, Landroid/os/HwParcel;->verifySuccess()V
/* .line 136 */
(( android.os.HwParcel ) v4 ).releaseTemporaryStorage ( ); // invoke-virtual {v4}, Landroid/os/HwParcel;->releaseTemporaryStorage()V
/* .line 137 */
v0 = (( android.os.HwParcel ) v1 ).readInt32 ( ); // invoke-virtual {v1}, Landroid/os/HwParcel;->readInt32()I
/* :try_end_0 */
/* .catch Ljava/lang/Exception; {:try_start_0 ..:try_end_0} :catch_0 */
/* .catchall {:try_start_0 ..:try_end_0} :catchall_0 */
/* .line 138 */
/* .local v0, "val":I */
/* if-ltz v0, :cond_1 */
/* if-le v0, v2, :cond_0 */
/* .line 143 */
} // :cond_0
/* nop */
/* .line 148 */
(( android.os.HwParcel ) v1 ).release ( ); // invoke-virtual {v1}, Landroid/os/HwParcel;->release()V
/* .line 143 */
/* .line 139 */
} // :cond_1
} // :goto_0
try { // :try_start_1
v5 = com.miui.server.MiuiCldService.TAG;
/* new-instance v6, Ljava/lang/StringBuilder; */
/* invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V */
final String v7 = "Got invalid fragment level: "; // const-string v7, "Got invalid fragment level: "
(( java.lang.StringBuilder ) v6 ).append ( v7 ); // invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v6 ).append ( v0 ); // invoke-virtual {v6, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v6 ).toString ( ); // invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
android.util.Slog .e ( v5,v6 );
/* :try_end_1 */
/* .catch Ljava/lang/Exception; {:try_start_1 ..:try_end_1} :catch_0 */
/* .catchall {:try_start_1 ..:try_end_1} :catchall_0 */
/* .line 140 */
/* nop */
/* .line 148 */
(( android.os.HwParcel ) v1 ).release ( ); // invoke-virtual {v1}, Landroid/os/HwParcel;->release()V
/* .line 140 */
/* .line 148 */
} // .end local v0 # "val":I
} // .end local v3 # "hwService":Landroid/os/IHwBinder;
} // .end local v4 # "hidl_request":Landroid/os/HwParcel;
} // :cond_2
/* nop */
} // :goto_1
(( android.os.HwParcel ) v1 ).release ( ); // invoke-virtual {v1}, Landroid/os/HwParcel;->release()V
/* .line 149 */
/* .line 148 */
/* :catchall_0 */
/* move-exception v0 */
/* .line 145 */
/* :catch_0 */
/* move-exception v0 */
/* .line 146 */
/* .local v0, "e":Ljava/lang/Exception; */
try { // :try_start_2
v3 = com.miui.server.MiuiCldService.TAG;
/* new-instance v4, Ljava/lang/StringBuilder; */
/* invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V */
final String v5 = "Transaction failed: "; // const-string v5, "Transaction failed: "
(( java.lang.StringBuilder ) v4 ).append ( v5 ); // invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v4 ).append ( v0 ); // invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v4 ).toString ( ); // invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
android.util.Slog .e ( v3,v4 );
/* :try_end_2 */
/* .catchall {:try_start_2 ..:try_end_2} :catchall_0 */
/* .line 148 */
/* nop */
} // .end local v0 # "e":Ljava/lang/Exception;
/* .line 151 */
} // :goto_2
v0 = com.miui.server.MiuiCldService.TAG;
final String v3 = "Failed calling getFragmentLevel."; // const-string v3, "Failed calling getFragmentLevel."
android.util.Slog .e ( v0,v3 );
/* .line 152 */
/* .line 148 */
} // :goto_3
(( android.os.HwParcel ) v1 ).release ( ); // invoke-virtual {v1}, Landroid/os/HwParcel;->release()V
/* .line 149 */
/* throw v0 */
} // .end method
public Boolean isCldSupported ( ) {
/* .locals 8 */
/* .line 98 */
/* const-string/jumbo v0, "vendor.xiaomi.hardware.cld@1.0::ICld" */
/* new-instance v1, Landroid/os/HwParcel; */
/* invoke-direct {v1}, Landroid/os/HwParcel;-><init>()V */
/* .line 100 */
/* .local v1, "hidl_reply":Landroid/os/HwParcel; */
int v2 = 0; // const/4 v2, 0x0
try { // :try_start_0
final String v3 = "default"; // const-string v3, "default"
android.os.HwBinder .getService ( v0,v3 );
/* .line 101 */
/* .local v3, "hwService":Landroid/os/IHwBinder; */
if ( v3 != null) { // if-eqz v3, :cond_1
/* .line 102 */
/* new-instance v4, Landroid/os/HwParcel; */
/* invoke-direct {v4}, Landroid/os/HwParcel;-><init>()V */
/* .line 103 */
/* .local v4, "hidl_request":Landroid/os/HwParcel; */
(( android.os.HwParcel ) v4 ).writeInterfaceToken ( v0 ); // invoke-virtual {v4, v0}, Landroid/os/HwParcel;->writeInterfaceToken(Ljava/lang/String;)V
/* .line 104 */
int v0 = 1; // const/4 v0, 0x1
/* .line 105 */
(( android.os.HwParcel ) v1 ).verifySuccess ( ); // invoke-virtual {v1}, Landroid/os/HwParcel;->verifySuccess()V
/* .line 106 */
(( android.os.HwParcel ) v4 ).releaseTemporaryStorage ( ); // invoke-virtual {v4}, Landroid/os/HwParcel;->releaseTemporaryStorage()V
/* .line 107 */
v5 = (( android.os.HwParcel ) v1 ).readInt32 ( ); // invoke-virtual {v1}, Landroid/os/HwParcel;->readInt32()I
/* .line 108 */
/* .local v5, "val":I */
/* if-nez v5, :cond_0 */
/* .line 109 */
v0 = com.miui.server.MiuiCldService.TAG;
final String v6 = "CLD not supported on current device!"; // const-string v6, "CLD not supported on current device!"
android.util.Slog .e ( v0,v6 );
/* :try_end_0 */
/* .catch Ljava/lang/Exception; {:try_start_0 ..:try_end_0} :catch_0 */
/* .catchall {:try_start_0 ..:try_end_0} :catchall_0 */
/* .line 110 */
/* nop */
/* .line 119 */
(( android.os.HwParcel ) v1 ).release ( ); // invoke-virtual {v1}, Landroid/os/HwParcel;->release()V
/* .line 110 */
/* .line 112 */
} // :cond_0
try { // :try_start_1
v6 = com.miui.server.MiuiCldService.TAG;
final String v7 = "CLD supported."; // const-string v7, "CLD supported."
android.util.Slog .i ( v6,v7 );
/* :try_end_1 */
/* .catch Ljava/lang/Exception; {:try_start_1 ..:try_end_1} :catch_0 */
/* .catchall {:try_start_1 ..:try_end_1} :catchall_0 */
/* .line 113 */
/* nop */
/* .line 119 */
(( android.os.HwParcel ) v1 ).release ( ); // invoke-virtual {v1}, Landroid/os/HwParcel;->release()V
/* .line 113 */
/* .line 119 */
} // .end local v3 # "hwService":Landroid/os/IHwBinder;
} // .end local v4 # "hidl_request":Landroid/os/HwParcel;
} // .end local v5 # "val":I
} // :cond_1
/* nop */
} // :goto_0
(( android.os.HwParcel ) v1 ).release ( ); // invoke-virtual {v1}, Landroid/os/HwParcel;->release()V
/* .line 120 */
/* .line 119 */
/* :catchall_0 */
/* move-exception v0 */
/* .line 116 */
/* :catch_0 */
/* move-exception v0 */
/* .line 117 */
/* .local v0, "e":Ljava/lang/Exception; */
try { // :try_start_2
v3 = com.miui.server.MiuiCldService.TAG;
/* new-instance v4, Ljava/lang/StringBuilder; */
/* invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V */
final String v5 = "Transaction failed: "; // const-string v5, "Transaction failed: "
(( java.lang.StringBuilder ) v4 ).append ( v5 ); // invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v4 ).append ( v0 ); // invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v4 ).toString ( ); // invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
android.util.Slog .e ( v3,v4 );
/* :try_end_2 */
/* .catchall {:try_start_2 ..:try_end_2} :catchall_0 */
/* .line 119 */
/* nop */
} // .end local v0 # "e":Ljava/lang/Exception;
/* .line 122 */
} // :goto_1
v0 = com.miui.server.MiuiCldService.TAG;
final String v3 = "Failed calling isCldSupported."; // const-string v3, "Failed calling isCldSupported."
android.util.Slog .e ( v0,v3 );
/* .line 123 */
/* .line 119 */
} // :goto_2
(( android.os.HwParcel ) v1 ).release ( ); // invoke-virtual {v1}, Landroid/os/HwParcel;->release()V
/* .line 120 */
/* throw v0 */
} // .end method
public void triggerCld ( Integer p0 ) {
/* .locals 6 */
/* .param p1, "val" # I */
/* .line 158 */
/* const-string/jumbo v0, "vendor.xiaomi.hardware.cld@1.0::ICld" */
/* new-instance v1, Ljava/util/Date; */
/* invoke-direct {v1}, Ljava/util/Date;-><init>()V */
/* .line 159 */
/* .local v1, "now":Ljava/util/Date; */
(( java.util.Date ) v1 ).getTime ( ); // invoke-virtual {v1}, Ljava/util/Date;->getTime()J
/* move-result-wide v2 */
v4 = this.sLastCld;
(( java.util.Date ) v4 ).getTime ( ); // invoke-virtual {v4}, Ljava/util/Date;->getTime()J
/* move-result-wide v4 */
/* sub-long/2addr v2, v4 */
/* const-wide/32 v4, 0x2932e00 */
/* cmp-long v2, v2, v4 */
/* if-gez v2, :cond_0 */
/* .line 160 */
v2 = com.miui.server.MiuiCldService.TAG;
final String v3 = "Less than half a day before last defragmentation!"; // const-string v3, "Less than half a day before last defragmentation!"
android.util.Slog .w ( v2,v3 );
/* .line 162 */
} // :cond_0
this.sLastCld = v1;
/* .line 164 */
/* new-instance v2, Landroid/os/HwParcel; */
/* invoke-direct {v2}, Landroid/os/HwParcel;-><init>()V */
/* .line 166 */
/* .local v2, "hidl_reply":Landroid/os/HwParcel; */
try { // :try_start_0
final String v3 = "default"; // const-string v3, "default"
android.os.HwBinder .getService ( v0,v3 );
/* .line 167 */
/* .local v3, "hwService":Landroid/os/IHwBinder; */
if ( v3 != null) { // if-eqz v3, :cond_1
/* .line 168 */
/* new-instance v4, Landroid/os/HwParcel; */
/* invoke-direct {v4}, Landroid/os/HwParcel;-><init>()V */
/* .line 169 */
/* .local v4, "hidl_request":Landroid/os/HwParcel; */
(( android.os.HwParcel ) v4 ).writeInterfaceToken ( v0 ); // invoke-virtual {v4, v0}, Landroid/os/HwParcel;->writeInterfaceToken(Ljava/lang/String;)V
/* .line 170 */
(( android.os.HwParcel ) v4 ).writeInt32 ( p1 ); // invoke-virtual {v4, p1}, Landroid/os/HwParcel;->writeInt32(I)V
/* .line 171 */
int v0 = 3; // const/4 v0, 0x3
int v5 = 0; // const/4 v5, 0x0
/* .line 172 */
(( android.os.HwParcel ) v2 ).verifySuccess ( ); // invoke-virtual {v2}, Landroid/os/HwParcel;->verifySuccess()V
/* .line 173 */
(( android.os.HwParcel ) v4 ).releaseTemporaryStorage ( ); // invoke-virtual {v4}, Landroid/os/HwParcel;->releaseTemporaryStorage()V
/* :try_end_0 */
/* .catch Ljava/lang/Exception; {:try_start_0 ..:try_end_0} :catch_0 */
/* .catchall {:try_start_0 ..:try_end_0} :catchall_0 */
/* .line 179 */
(( android.os.HwParcel ) v2 ).release ( ); // invoke-virtual {v2}, Landroid/os/HwParcel;->release()V
/* .line 174 */
return;
/* .line 179 */
} // .end local v3 # "hwService":Landroid/os/IHwBinder;
} // .end local v4 # "hidl_request":Landroid/os/HwParcel;
} // :cond_1
/* nop */
} // :goto_0
(( android.os.HwParcel ) v2 ).release ( ); // invoke-virtual {v2}, Landroid/os/HwParcel;->release()V
/* .line 180 */
/* .line 179 */
/* :catchall_0 */
/* move-exception v0 */
/* .line 176 */
/* :catch_0 */
/* move-exception v0 */
/* .line 177 */
/* .local v0, "e":Ljava/lang/Exception; */
try { // :try_start_1
v3 = com.miui.server.MiuiCldService.TAG;
/* new-instance v4, Ljava/lang/StringBuilder; */
/* invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V */
final String v5 = "Transaction failed: "; // const-string v5, "Transaction failed: "
(( java.lang.StringBuilder ) v4 ).append ( v5 ); // invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v4 ).append ( v0 ); // invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v4 ).toString ( ); // invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
android.util.Slog .e ( v3,v4 );
/* :try_end_1 */
/* .catchall {:try_start_1 ..:try_end_1} :catchall_0 */
/* .line 179 */
/* nop */
} // .end local v0 # "e":Ljava/lang/Exception;
/* .line 182 */
} // :goto_1
v0 = com.miui.server.MiuiCldService.TAG;
final String v3 = "Failed calling triggerCld."; // const-string v3, "Failed calling triggerCld."
android.util.Slog .e ( v0,v3 );
/* .line 183 */
return;
/* .line 179 */
} // :goto_2
(( android.os.HwParcel ) v2 ).release ( ); // invoke-virtual {v2}, Landroid/os/HwParcel;->release()V
/* .line 180 */
/* throw v0 */
} // .end method
