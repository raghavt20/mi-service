public abstract class com.miui.server.process.ProcessManagerInternal {
	 /* .source "ProcessManagerInternal.java" */
	 /* # static fields */
	 private static com.miui.server.process.ProcessManagerInternal sInstance;
	 /* # direct methods */
	 public com.miui.server.process.ProcessManagerInternal ( ) {
		 /* .locals 0 */
		 /* .line 25 */
		 /* invoke-direct {p0}, Ljava/lang/Object;-><init>()V */
		 return;
	 } // .end method
	 public static Boolean checkCtsProcess ( java.lang.String p0 ) {
		 /* .locals 1 */
		 /* .param p0, "processName" # Ljava/lang/String; */
		 /* .line 149 */
		 final String v0 = "com.android.cts."; // const-string v0, "com.android.cts."
		 v0 = 		 (( java.lang.String ) p0 ).startsWith ( v0 ); // invoke-virtual {p0, v0}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z
		 /* if-nez v0, :cond_1 */
		 /* .line 150 */
		 final String v0 = "android.app.cts."; // const-string v0, "android.app.cts."
		 v0 = 		 (( java.lang.String ) p0 ).startsWith ( v0 ); // invoke-virtual {p0, v0}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z
		 /* if-nez v0, :cond_1 */
		 /* .line 151 */
		 final String v0 = "com.android.server.cts."; // const-string v0, "com.android.server.cts."
		 v0 = 		 (( java.lang.String ) p0 ).startsWith ( v0 ); // invoke-virtual {p0, v0}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z
		 /* if-nez v0, :cond_1 */
		 /* .line 152 */
		 final String v0 = "com.android.RemoteDPC"; // const-string v0, "com.android.RemoteDPC"
		 v0 = 		 (( java.lang.String ) p0 ).startsWith ( v0 ); // invoke-virtual {p0, v0}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z
		 /* if-nez v0, :cond_1 */
		 /* .line 153 */
		 final String v0 = "android.camera.cts"; // const-string v0, "android.camera.cts"
		 v0 = 		 (( java.lang.String ) p0 ).startsWith ( v0 ); // invoke-virtual {p0, v0}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z
		 /* if-nez v0, :cond_1 */
		 /* .line 154 */
		 final String v0 = "android.permission.cts"; // const-string v0, "android.permission.cts"
		 v0 = 		 (( java.lang.String ) p0 ).startsWith ( v0 ); // invoke-virtual {p0, v0}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z
		 /* if-nez v0, :cond_1 */
		 /* .line 155 */
		 final String v0 = "android.jobscheduler.cts"; // const-string v0, "android.jobscheduler.cts"
		 v0 = 		 (( java.lang.String ) p0 ).startsWith ( v0 ); // invoke-virtual {p0, v0}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z
		 /* if-nez v0, :cond_1 */
		 /* .line 156 */
		 final String v0 = "android.voiceinteraction.cts"; // const-string v0, "android.voiceinteraction.cts"
		 v0 = 		 (( java.lang.String ) p0 ).startsWith ( v0 ); // invoke-virtual {p0, v0}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z
		 /* if-nez v0, :cond_1 */
		 /* .line 157 */
		 final String v0 = "android.telecom.cts"; // const-string v0, "android.telecom.cts"
		 v0 = 		 (( java.lang.String ) p0 ).startsWith ( v0 ); // invoke-virtual {p0, v0}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z
		 /* if-nez v0, :cond_1 */
		 /* .line 158 */
		 final String v0 = "com.android.app1"; // const-string v0, "com.android.app1"
		 v0 = 		 (( java.lang.String ) p0 ).startsWith ( v0 ); // invoke-virtual {p0, v0}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z
		 /* if-nez v0, :cond_1 */
		 /* .line 159 */
		 final String v0 = "android.app.stubs"; // const-string v0, "android.app.stubs"
		 v0 = 		 (( java.lang.String ) p0 ).startsWith ( v0 ); // invoke-virtual {p0, v0}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z
		 /* if-nez v0, :cond_1 */
		 /* .line 160 */
		 final String v0 = "android.cts.backup.successnotificationapp"; // const-string v0, "android.cts.backup.successnotificationapp"
		 v0 = 		 (( java.lang.String ) p0 ).startsWith ( v0 ); // invoke-virtual {p0, v0}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z
		 /* if-nez v0, :cond_1 */
		 /* .line 161 */
		 final String v0 = "com.android.Delegate"; // const-string v0, "com.android.Delegate"
		 v0 = 		 (( java.lang.String ) p0 ).startsWith ( v0 ); // invoke-virtual {p0, v0}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z
		 /* if-nez v0, :cond_1 */
		 /* .line 162 */
		 final String v0 = "com.android.bedstead.testapp"; // const-string v0, "com.android.bedstead.testapp"
		 v0 = 		 (( java.lang.String ) p0 ).startsWith ( v0 ); // invoke-virtual {p0, v0}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z
		 /* if-nez v0, :cond_1 */
		 /* .line 163 */
		 final String v0 = "com.android.RemoteAccountAuthenticator"; // const-string v0, "com.android.RemoteAccountAuthenticator"
		 v0 = 		 (( java.lang.String ) p0 ).startsWith ( v0 ); // invoke-virtual {p0, v0}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z
		 if ( v0 != null) { // if-eqz v0, :cond_0
		 } // :cond_0
		 int v0 = 0; // const/4 v0, 0x0
	 } // :cond_1
} // :goto_0
int v0 = 1; // const/4 v0, 0x1
/* .line 149 */
} // :goto_1
} // .end method
public static com.miui.server.process.ProcessManagerInternal getInstance ( ) {
/* .locals 1 */
/* .line 31 */
v0 = com.miui.server.process.ProcessManagerInternal.sInstance;
/* if-nez v0, :cond_0 */
/* .line 32 */
/* const-class v0, Lcom/miui/server/process/ProcessManagerInternal; */
com.android.server.LocalServices .getService ( v0 );
/* check-cast v0, Lcom/miui/server/process/ProcessManagerInternal; */
/* .line 34 */
} // :cond_0
v0 = com.miui.server.process.ProcessManagerInternal.sInstance;
} // .end method
/* # virtual methods */
public abstract Boolean checkAppFgServices ( Integer p0 ) {
} // .end method
public abstract void forceStopPackage ( com.android.server.am.ProcessRecord p0, java.lang.String p1, Boolean p2 ) {
} // .end method
public abstract void forceStopPackage ( java.lang.String p0, Integer p1, java.lang.String p2 ) {
} // .end method
public abstract java.util.List getAllRunningProcessInfo ( ) {
/* .annotation system Ldalvik/annotation/Signature; */
/* value = { */
/* "()", */
/* "Ljava/util/List<", */
/* "Lmiui/process/RunningProcessInfo;", */
/* ">;" */
/* } */
} // .end annotation
} // .end method
public abstract miui.process.ForegroundInfo getForegroundInfo ( ) {
/* .annotation system Ldalvik/annotation/Throws; */
/* value = { */
/* Landroid/os/RemoteException; */
/* } */
} // .end annotation
} // .end method
public abstract miui.process.IMiuiApplicationThread getMiuiApplicationThread ( Integer p0 ) {
} // .end method
public abstract android.content.pm.ApplicationInfo getMultiWindowForegroundAppInfoLocked ( ) {
} // .end method
public abstract com.android.server.am.IProcessPolicy getProcessPolicy ( ) {
} // .end method
public abstract com.android.server.am.ProcessRecord getProcessRecord ( java.lang.String p0 ) {
} // .end method
public abstract com.android.server.am.ProcessRecord getProcessRecord ( java.lang.String p0, Integer p1 ) {
} // .end method
public abstract com.android.server.am.ProcessRecord getProcessRecordByPid ( Integer p0 ) {
} // .end method
public abstract Boolean isAllowRestartProcessLock ( java.lang.String p0, Integer p1, Integer p2, java.lang.String p3, java.lang.String p4, com.android.server.am.HostingRecord p5 ) {
} // .end method
public abstract Boolean isForegroundApp ( java.lang.String p0, Integer p1 ) {
} // .end method
public abstract Boolean isInWhiteList ( com.android.server.am.ProcessRecord p0, Integer p1, Integer p2 ) {
} // .end method
public abstract Boolean isLockedApplication ( java.lang.String p0, Integer p1 ) {
/* .annotation system Ldalvik/annotation/Throws; */
/* value = { */
/* Landroid/os/RemoteException; */
/* } */
} // .end annotation
} // .end method
public abstract Boolean isPackageFastBootEnable ( java.lang.String p0, Integer p1, Boolean p2 ) {
} // .end method
public abstract Boolean isTrimMemoryEnable ( java.lang.String p0 ) {
} // .end method
public abstract void killApplication ( com.android.server.am.ProcessRecord p0, java.lang.String p1, Boolean p2 ) {
} // .end method
public abstract void notifyActivityChanged ( android.content.ComponentName p0 ) {
} // .end method
public abstract void notifyAmsProcessKill ( com.android.server.am.ProcessRecord p0, java.lang.String p1 ) {
} // .end method
public abstract void notifyForegroundInfoChanged ( com.android.server.wm.FgActivityChangedInfo p0 ) {
} // .end method
public abstract void notifyForegroundWindowChanged ( com.android.server.wm.FgWindowChangedInfo p0 ) {
} // .end method
public abstract void notifyLmkProcessKill ( Integer p0, Integer p1, Long p2, Integer p3, Integer p4, Integer p5, Integer p6, java.lang.String p7 ) {
} // .end method
public abstract void notifyProcessDied ( com.android.server.am.ProcessRecord p0 ) {
} // .end method
public abstract void notifyProcessStarted ( com.android.server.am.ProcessRecord p0 ) {
} // .end method
public abstract Boolean restartDiedAppOrNot ( com.android.server.am.ProcessRecord p0, Boolean p1, Boolean p2, Boolean p3 ) {
} // .end method
public abstract void setProcessMaxAdjLock ( Integer p0, com.android.server.am.ProcessRecord p1, Integer p2, Integer p3 ) {
} // .end method
public abstract void setSpeedTestState ( Boolean p0 ) {
} // .end method
public abstract void trimMemory ( com.android.server.am.ProcessRecord p0, Boolean p1 ) {
} // .end method
public abstract void updateEnterpriseWhiteList ( java.lang.String p0, Boolean p1 ) {
} // .end method
