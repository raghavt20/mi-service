.class public abstract Lcom/miui/server/process/ProcessManagerInternal;
.super Ljava/lang/Object;
.source "ProcessManagerInternal.java"


# static fields
.field private static sInstance:Lcom/miui/server/process/ProcessManagerInternal;


# direct methods
.method public constructor <init>()V
    .locals 0

    .line 25
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static checkCtsProcess(Ljava/lang/String;)Z
    .locals 1
    .param p0, "processName"    # Ljava/lang/String;

    .line 149
    const-string v0, "com.android.cts."

    invoke-virtual {p0, v0}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_1

    .line 150
    const-string v0, "android.app.cts."

    invoke-virtual {p0, v0}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_1

    .line 151
    const-string v0, "com.android.server.cts."

    invoke-virtual {p0, v0}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_1

    .line 152
    const-string v0, "com.android.RemoteDPC"

    invoke-virtual {p0, v0}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_1

    .line 153
    const-string v0, "android.camera.cts"

    invoke-virtual {p0, v0}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_1

    .line 154
    const-string v0, "android.permission.cts"

    invoke-virtual {p0, v0}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_1

    .line 155
    const-string v0, "android.jobscheduler.cts"

    invoke-virtual {p0, v0}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_1

    .line 156
    const-string v0, "android.voiceinteraction.cts"

    invoke-virtual {p0, v0}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_1

    .line 157
    const-string v0, "android.telecom.cts"

    invoke-virtual {p0, v0}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_1

    .line 158
    const-string v0, "com.android.app1"

    invoke-virtual {p0, v0}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_1

    .line 159
    const-string v0, "android.app.stubs"

    invoke-virtual {p0, v0}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_1

    .line 160
    const-string v0, "android.cts.backup.successnotificationapp"

    invoke-virtual {p0, v0}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_1

    .line 161
    const-string v0, "com.android.Delegate"

    invoke-virtual {p0, v0}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_1

    .line 162
    const-string v0, "com.android.bedstead.testapp"

    invoke-virtual {p0, v0}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_1

    .line 163
    const-string v0, "com.android.RemoteAccountAuthenticator"

    invoke-virtual {p0, v0}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_0

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    goto :goto_1

    :cond_1
    :goto_0
    const/4 v0, 0x1

    .line 149
    :goto_1
    return v0
.end method

.method public static getInstance()Lcom/miui/server/process/ProcessManagerInternal;
    .locals 1

    .line 31
    sget-object v0, Lcom/miui/server/process/ProcessManagerInternal;->sInstance:Lcom/miui/server/process/ProcessManagerInternal;

    if-nez v0, :cond_0

    .line 32
    const-class v0, Lcom/miui/server/process/ProcessManagerInternal;

    invoke-static {v0}, Lcom/android/server/LocalServices;->getService(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/miui/server/process/ProcessManagerInternal;

    sput-object v0, Lcom/miui/server/process/ProcessManagerInternal;->sInstance:Lcom/miui/server/process/ProcessManagerInternal;

    .line 34
    :cond_0
    sget-object v0, Lcom/miui/server/process/ProcessManagerInternal;->sInstance:Lcom/miui/server/process/ProcessManagerInternal;

    return-object v0
.end method


# virtual methods
.method public abstract checkAppFgServices(I)Z
.end method

.method public abstract forceStopPackage(Lcom/android/server/am/ProcessRecord;Ljava/lang/String;Z)V
.end method

.method public abstract forceStopPackage(Ljava/lang/String;ILjava/lang/String;)V
.end method

.method public abstract getAllRunningProcessInfo()Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List<",
            "Lmiui/process/RunningProcessInfo;",
            ">;"
        }
    .end annotation
.end method

.method public abstract getForegroundInfo()Lmiui/process/ForegroundInfo;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation
.end method

.method public abstract getMiuiApplicationThread(I)Lmiui/process/IMiuiApplicationThread;
.end method

.method public abstract getMultiWindowForegroundAppInfoLocked()Landroid/content/pm/ApplicationInfo;
.end method

.method public abstract getProcessPolicy()Lcom/android/server/am/IProcessPolicy;
.end method

.method public abstract getProcessRecord(Ljava/lang/String;)Lcom/android/server/am/ProcessRecord;
.end method

.method public abstract getProcessRecord(Ljava/lang/String;I)Lcom/android/server/am/ProcessRecord;
.end method

.method public abstract getProcessRecordByPid(I)Lcom/android/server/am/ProcessRecord;
.end method

.method public abstract isAllowRestartProcessLock(Ljava/lang/String;IILjava/lang/String;Ljava/lang/String;Lcom/android/server/am/HostingRecord;)Z
.end method

.method public abstract isForegroundApp(Ljava/lang/String;I)Z
.end method

.method public abstract isInWhiteList(Lcom/android/server/am/ProcessRecord;II)Z
.end method

.method public abstract isLockedApplication(Ljava/lang/String;I)Z
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation
.end method

.method public abstract isPackageFastBootEnable(Ljava/lang/String;IZ)Z
.end method

.method public abstract isTrimMemoryEnable(Ljava/lang/String;)Z
.end method

.method public abstract killApplication(Lcom/android/server/am/ProcessRecord;Ljava/lang/String;Z)V
.end method

.method public abstract notifyActivityChanged(Landroid/content/ComponentName;)V
.end method

.method public abstract notifyAmsProcessKill(Lcom/android/server/am/ProcessRecord;Ljava/lang/String;)V
.end method

.method public abstract notifyForegroundInfoChanged(Lcom/android/server/wm/FgActivityChangedInfo;)V
.end method

.method public abstract notifyForegroundWindowChanged(Lcom/android/server/wm/FgWindowChangedInfo;)V
.end method

.method public abstract notifyLmkProcessKill(IIJIIIILjava/lang/String;)V
.end method

.method public abstract notifyProcessDied(Lcom/android/server/am/ProcessRecord;)V
.end method

.method public abstract notifyProcessStarted(Lcom/android/server/am/ProcessRecord;)V
.end method

.method public abstract restartDiedAppOrNot(Lcom/android/server/am/ProcessRecord;ZZZ)Z
.end method

.method public abstract setProcessMaxAdjLock(ILcom/android/server/am/ProcessRecord;II)V
.end method

.method public abstract setSpeedTestState(Z)V
.end method

.method public abstract trimMemory(Lcom/android/server/am/ProcessRecord;Z)V
.end method

.method public abstract updateEnterpriseWhiteList(Ljava/lang/String;Z)V
.end method
