.class public final Lcom/miui/server/MiuiInitServer$Lifecycle;
.super Lcom/android/server/SystemService;
.source "MiuiInitServer.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/miui/server/MiuiInitServer;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "Lifecycle"
.end annotation


# instance fields
.field private final mService:Lcom/miui/server/MiuiInitServer;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 1
    .param p1, "context"    # Landroid/content/Context;

    .line 72
    invoke-direct {p0, p1}, Lcom/android/server/SystemService;-><init>(Landroid/content/Context;)V

    .line 73
    new-instance v0, Lcom/miui/server/MiuiInitServer;

    invoke-direct {v0, p1}, Lcom/miui/server/MiuiInitServer;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Lcom/miui/server/MiuiInitServer$Lifecycle;->mService:Lcom/miui/server/MiuiInitServer;

    .line 74
    return-void
.end method


# virtual methods
.method public onStart()V
    .locals 2

    .line 78
    const-string v0, "MiuiInit"

    iget-object v1, p0, Lcom/miui/server/MiuiInitServer$Lifecycle;->mService:Lcom/miui/server/MiuiInitServer;

    invoke-virtual {p0, v0, v1}, Lcom/miui/server/MiuiInitServer$Lifecycle;->publishBinderService(Ljava/lang/String;Landroid/os/IBinder;)V

    .line 79
    return-void
.end method
