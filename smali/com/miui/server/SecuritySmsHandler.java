class com.miui.server.SecuritySmsHandler {
	 /* .source "SecuritySmsHandler.java" */
	 /* # static fields */
	 private static final java.lang.String TAG;
	 /* # instance fields */
	 private final android.content.Context mContext;
	 private final android.os.Handler mHandler;
	 private java.lang.String mInterceptSmsCallerPkgName;
	 private Integer mInterceptSmsCallerUid;
	 private Integer mInterceptSmsCount;
	 private final java.lang.Object mInterceptSmsLock;
	 private java.lang.String mInterceptSmsSenderNum;
	 private android.content.BroadcastReceiver mInterceptedSmsResultReceiver;
	 private android.content.BroadcastReceiver mNormalMsgResultReceiver;
	 /* # direct methods */
	 static Integer -$$Nest$mcheckByAntiSpam ( com.miui.server.SecuritySmsHandler p0, java.lang.String p1, java.lang.String p2, Integer p3 ) { //bridge//synthethic
		 /* .locals 0 */
		 p0 = 		 /* invoke-direct {p0, p1, p2, p3}, Lcom/miui/server/SecuritySmsHandler;->checkByAntiSpam(Ljava/lang/String;Ljava/lang/String;I)I */
	 } // .end method
	 static void -$$Nest$mdispatchIntent ( com.miui.server.SecuritySmsHandler p0, android.content.Intent p1, java.lang.String p2, Integer p3, android.content.BroadcastReceiver p4 ) { //bridge//synthethic
		 /* .locals 0 */
		 /* invoke-direct {p0, p1, p2, p3, p4}, Lcom/miui/server/SecuritySmsHandler;->dispatchIntent(Landroid/content/Intent;Ljava/lang/String;ILandroid/content/BroadcastReceiver;)V */
		 return;
	 } // .end method
	 static void -$$Nest$mdispatchNormalSms ( com.miui.server.SecuritySmsHandler p0, android.content.Intent p1 ) { //bridge//synthethic
		 /* .locals 0 */
		 /* invoke-direct {p0, p1}, Lcom/miui/server/SecuritySmsHandler;->dispatchNormalSms(Landroid/content/Intent;)V */
		 return;
	 } // .end method
	 static void -$$Nest$mdispatchSmsToAntiSpam ( com.miui.server.SecuritySmsHandler p0, android.content.Intent p1 ) { //bridge//synthethic
		 /* .locals 0 */
		 /* invoke-direct {p0, p1}, Lcom/miui/server/SecuritySmsHandler;->dispatchSmsToAntiSpam(Landroid/content/Intent;)V */
		 return;
	 } // .end method
	 com.miui.server.SecuritySmsHandler ( ) {
		 /* .locals 2 */
		 /* .param p1, "context" # Landroid/content/Context; */
		 /* .param p2, "handler" # Landroid/os/Handler; */
		 /* .line 40 */
		 /* invoke-direct {p0}, Ljava/lang/Object;-><init>()V */
		 /* .line 34 */
		 int v0 = 0; // const/4 v0, 0x0
		 /* iput v0, p0, Lcom/miui/server/SecuritySmsHandler;->mInterceptSmsCallerUid:I */
		 /* .line 35 */
		 int v1 = 0; // const/4 v1, 0x0
		 this.mInterceptSmsCallerPkgName = v1;
		 /* .line 36 */
		 /* iput v0, p0, Lcom/miui/server/SecuritySmsHandler;->mInterceptSmsCount:I */
		 /* .line 37 */
		 this.mInterceptSmsSenderNum = v1;
		 /* .line 38 */
		 /* new-instance v0, Ljava/lang/Object; */
		 /* invoke-direct {v0}, Ljava/lang/Object;-><init>()V */
		 this.mInterceptSmsLock = v0;
		 /* .line 276 */
		 /* new-instance v0, Lcom/miui/server/SecuritySmsHandler$1; */
		 /* invoke-direct {v0, p0}, Lcom/miui/server/SecuritySmsHandler$1;-><init>(Lcom/miui/server/SecuritySmsHandler;)V */
		 this.mNormalMsgResultReceiver = v0;
		 /* .line 299 */
		 /* new-instance v0, Lcom/miui/server/SecuritySmsHandler$2; */
		 /* invoke-direct {v0, p0}, Lcom/miui/server/SecuritySmsHandler$2;-><init>(Lcom/miui/server/SecuritySmsHandler;)V */
		 this.mInterceptedSmsResultReceiver = v0;
		 /* .line 41 */
		 this.mHandler = p2;
		 /* .line 42 */
		 this.mContext = p1;
		 /* .line 43 */
		 return;
	 } // .end method
	 private Integer checkByAntiSpam ( java.lang.String p0, java.lang.String p1, Integer p2 ) {
		 /* .locals 5 */
		 /* .param p1, "address" # Ljava/lang/String; */
		 /* .param p2, "content" # Ljava/lang/String; */
		 /* .param p3, "slotId" # I */
		 /* .line 177 */
		 android.os.Binder .clearCallingIdentity ( );
		 /* move-result-wide v0 */
		 /* .line 178 */
		 /* .local v0, "token":J */
		 v2 = this.mContext;
		 v2 = 		 miui.provider.ExtraTelephony .getSmsBlockType ( v2,p1,p2,p3 );
		 /* .line 179 */
		 /* .local v2, "blockType":I */
		 android.os.Binder .restoreCallingIdentity ( v0,v1 );
		 /* .line 180 */
		 /* new-instance v3, Ljava/lang/StringBuilder; */
		 /* invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V */
		 final String v4 = "checkByAntiSpam : blockType = "; // const-string v4, "checkByAntiSpam : blockType = "
		 (( java.lang.StringBuilder ) v3 ).append ( v4 ); // invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
		 (( java.lang.StringBuilder ) v3 ).append ( v2 ); // invoke-virtual {v3, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;
		 (( java.lang.StringBuilder ) v3 ).toString ( ); // invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
		 final String v4 = "SecuritySmsHandler"; // const-string v4, "SecuritySmsHandler"
		 android.util.Log .i ( v4,v3 );
		 /* .line 181 */
	 } // .end method
	 private Boolean checkWithInterceptedSender ( java.lang.String p0 ) {
		 /* .locals 8 */
		 /* .param p1, "sender" # Ljava/lang/String; */
		 /* .line 151 */
		 int v0 = 0; // const/4 v0, 0x0
		 /* .line 152 */
		 /* .local v0, "result":Z */
		 v1 = this.mInterceptSmsLock;
		 /* monitor-enter v1 */
		 /* .line 153 */
		 try { // :try_start_0
			 final String v2 = "SecuritySmsHandler"; // const-string v2, "SecuritySmsHandler"
			 final String v3 = "checkWithInterceptedSender: callerUid:%d, senderNum:%s, count:%d"; // const-string v3, "checkWithInterceptedSender: callerUid:%d, senderNum:%s, count:%d"
			 int v4 = 3; // const/4 v4, 0x3
			 /* new-array v4, v4, [Ljava/lang/Object; */
			 /* iget v5, p0, Lcom/miui/server/SecuritySmsHandler;->mInterceptSmsCallerUid:I */
			 /* .line 154 */
			 java.lang.Integer .valueOf ( v5 );
			 int v6 = 0; // const/4 v6, 0x0
			 /* aput-object v5, v4, v6 */
			 v5 = this.mInterceptSmsSenderNum;
			 int v6 = 1; // const/4 v6, 0x1
			 /* aput-object v5, v4, v6 */
			 /* iget v5, p0, Lcom/miui/server/SecuritySmsHandler;->mInterceptSmsCount:I */
			 java.lang.Integer .valueOf ( v5 );
			 int v7 = 2; // const/4 v7, 0x2
			 /* aput-object v5, v4, v7 */
			 /* .line 153 */
			 java.lang.String .format ( v3,v4 );
			 android.util.Log .i ( v2,v3 );
			 /* .line 155 */
			 /* iget v2, p0, Lcom/miui/server/SecuritySmsHandler;->mInterceptSmsCallerUid:I */
			 if ( v2 != null) { // if-eqz v2, :cond_1
				 v2 = this.mInterceptSmsSenderNum;
				 v2 = 				 android.text.TextUtils .equals ( v2,p1 );
				 if ( v2 != null) { // if-eqz v2, :cond_1
					 /* .line 156 */
					 /* iget v2, p0, Lcom/miui/server/SecuritySmsHandler;->mInterceptSmsCount:I */
					 /* if-lez v2, :cond_0 */
					 /* .line 157 */
					 /* sub-int/2addr v2, v6 */
					 /* iput v2, p0, Lcom/miui/server/SecuritySmsHandler;->mInterceptSmsCount:I */
					 /* .line 158 */
					 int v0 = 1; // const/4 v0, 0x1
					 /* .line 160 */
				 } // :cond_0
				 /* iget v2, p0, Lcom/miui/server/SecuritySmsHandler;->mInterceptSmsCount:I */
				 /* if-nez v2, :cond_1 */
				 /* .line 161 */
				 /* invoke-direct {p0}, Lcom/miui/server/SecuritySmsHandler;->releaseSmsIntercept()V */
				 /* .line 164 */
			 } // :cond_1
			 /* monitor-exit v1 */
			 /* .line 165 */
			 /* .line 164 */
			 /* :catchall_0 */
			 /* move-exception v2 */
			 /* monitor-exit v1 */
			 /* :try_end_0 */
			 /* .catchall {:try_start_0 ..:try_end_0} :catchall_0 */
			 /* throw v2 */
		 } // .end method
		 private void dispatchIntent ( android.content.Intent p0, java.lang.String p1, Integer p2, android.content.BroadcastReceiver p3 ) {
			 /* .locals 9 */
			 /* .param p1, "intent" # Landroid/content/Intent; */
			 /* .param p2, "permission" # Ljava/lang/String; */
			 /* .param p3, "appOp" # I */
			 /* .param p4, "resultReceiver" # Landroid/content/BroadcastReceiver; */
			 /* .line 272 */
			 v0 = this.mContext;
			 v5 = this.mHandler;
			 int v6 = -1; // const/4 v6, -0x1
			 int v7 = 0; // const/4 v7, 0x0
			 int v8 = 0; // const/4 v8, 0x0
			 /* move-object v1, p1 */
			 /* move-object v2, p2 */
			 /* move v3, p3 */
			 /* move-object v4, p4 */
			 /* invoke-virtual/range {v0 ..v8}, Landroid/content/Context;->sendOrderedBroadcast(Landroid/content/Intent;Ljava/lang/String;ILandroid/content/BroadcastReceiver;Landroid/os/Handler;ILjava/lang/String;Landroid/os/Bundle;)V */
			 /* .line 274 */
			 return;
		 } // .end method
		 private void dispatchMmsToAntiSpam ( android.content.Intent p0 ) {
			 /* .locals 3 */
			 /* .param p1, "intent" # Landroid/content/Intent; */
			 /* .line 222 */
			 final String v0 = "SecuritySmsHandler"; // const-string v0, "SecuritySmsHandler"
			 final String v1 = "dispatchMmsToAntiSpam"; // const-string v1, "dispatchMmsToAntiSpam"
			 android.util.Log .i ( v0,v1 );
			 /* .line 223 */
			 int v0 = 0; // const/4 v0, 0x0
			 (( android.content.Intent ) p1 ).setComponent ( v0 ); // invoke-virtual {p1, v0}, Landroid/content/Intent;->setComponent(Landroid/content/ComponentName;)Landroid/content/Intent;
			 /* .line 224 */
			 final String v1 = "com.android.mms"; // const-string v1, "com.android.mms"
			 (( android.content.Intent ) p1 ).setPackage ( v1 ); // invoke-virtual {p1, v1}, Landroid/content/Intent;->setPackage(Ljava/lang/String;)Landroid/content/Intent;
			 /* .line 225 */
			 final String v1 = "android.provider.Telephony.WAP_PUSH_DELIVER"; // const-string v1, "android.provider.Telephony.WAP_PUSH_DELIVER"
			 (( android.content.Intent ) p1 ).setAction ( v1 ); // invoke-virtual {p1, v1}, Landroid/content/Intent;->setAction(Ljava/lang/String;)Landroid/content/Intent;
			 /* .line 226 */
			 final String v1 = "android.permission.RECEIVE_SMS"; // const-string v1, "android.permission.RECEIVE_SMS"
			 /* const/16 v2, 0x10 */
			 /* invoke-direct {p0, p1, v1, v2, v0}, Lcom/miui/server/SecuritySmsHandler;->dispatchIntent(Landroid/content/Intent;Ljava/lang/String;ILandroid/content/BroadcastReceiver;)V */
			 /* .line 227 */
			 return;
		 } // .end method
		 private void dispatchNormalMms ( android.content.Intent p0 ) {
			 /* .locals 4 */
			 /* .param p1, "intent" # Landroid/content/Intent; */
			 /* .line 255 */
			 final String v0 = "dispatchNormalMms"; // const-string v0, "dispatchNormalMms"
			 final String v1 = "SecuritySmsHandler"; // const-string v1, "SecuritySmsHandler"
			 android.util.Log .i ( v1,v0 );
			 /* .line 256 */
			 int v0 = 0; // const/4 v0, 0x0
			 (( android.content.Intent ) p1 ).setPackage ( v0 ); // invoke-virtual {p1, v0}, Landroid/content/Intent;->setPackage(Ljava/lang/String;)Landroid/content/Intent;
			 /* .line 259 */
			 v0 = this.mContext;
			 int v2 = 1; // const/4 v2, 0x1
			 com.android.internal.telephony.SmsApplication .getDefaultMmsApplication ( v0,v2 );
			 /* .line 260 */
			 /* .local v0, "componentName":Landroid/content/ComponentName; */
			 if ( v0 != null) { // if-eqz v0, :cond_0
				 /* .line 262 */
				 (( android.content.Intent ) p1 ).setComponent ( v0 ); // invoke-virtual {p1, v0}, Landroid/content/Intent;->setComponent(Landroid/content/ComponentName;)Landroid/content/Intent;
				 /* .line 263 */
				 (( android.content.ComponentName ) v0 ).getPackageName ( ); // invoke-virtual {v0}, Landroid/content/ComponentName;->getPackageName()Ljava/lang/String;
				 /* filled-new-array {v2}, [Ljava/lang/Object; */
				 final String v3 = "Delivering MMS to: %s"; // const-string v3, "Delivering MMS to: %s"
				 java.lang.String .format ( v3,v2 );
				 android.util.Log .i ( v1,v2 );
				 /* .line 265 */
			 } // :cond_0
			 /* const/high16 v1, 0x8000000 */
			 (( android.content.Intent ) p1 ).addFlags ( v1 ); // invoke-virtual {p1, v1}, Landroid/content/Intent;->addFlags(I)Landroid/content/Intent;
			 /* .line 266 */
			 final String v1 = "android.provider.Telephony.WAP_PUSH_DELIVER"; // const-string v1, "android.provider.Telephony.WAP_PUSH_DELIVER"
			 (( android.content.Intent ) p1 ).setAction ( v1 ); // invoke-virtual {p1, v1}, Landroid/content/Intent;->setAction(Ljava/lang/String;)Landroid/content/Intent;
			 /* .line 267 */
			 /* const/16 v1, 0x10 */
			 v2 = this.mNormalMsgResultReceiver;
			 final String v3 = "android.permission.RECEIVE_SMS"; // const-string v3, "android.permission.RECEIVE_SMS"
			 /* invoke-direct {p0, p1, v3, v1, v2}, Lcom/miui/server/SecuritySmsHandler;->dispatchIntent(Landroid/content/Intent;Ljava/lang/String;ILandroid/content/BroadcastReceiver;)V */
			 /* .line 269 */
			 return;
		 } // .end method
		 private void dispatchNormalSms ( android.content.Intent p0 ) {
			 /* .locals 4 */
			 /* .param p1, "intent" # Landroid/content/Intent; */
			 /* .line 234 */
			 final String v0 = "dispatchNormalSms"; // const-string v0, "dispatchNormalSms"
			 final String v1 = "SecuritySmsHandler"; // const-string v1, "SecuritySmsHandler"
			 android.util.Log .i ( v1,v0 );
			 /* .line 235 */
			 int v0 = 0; // const/4 v0, 0x0
			 (( android.content.Intent ) p1 ).setPackage ( v0 ); // invoke-virtual {p1, v0}, Landroid/content/Intent;->setPackage(Ljava/lang/String;)Landroid/content/Intent;
			 /* .line 238 */
			 v0 = this.mContext;
			 int v2 = 1; // const/4 v2, 0x1
			 com.android.internal.telephony.SmsApplication .getDefaultSmsApplication ( v0,v2 );
			 /* .line 239 */
			 /* .local v0, "componentName":Landroid/content/ComponentName; */
			 if ( v0 != null) { // if-eqz v0, :cond_0
				 /* .line 241 */
				 (( android.content.Intent ) p1 ).setComponent ( v0 ); // invoke-virtual {p1, v0}, Landroid/content/Intent;->setComponent(Landroid/content/ComponentName;)Landroid/content/Intent;
				 /* .line 242 */
				 (( android.content.ComponentName ) v0 ).getPackageName ( ); // invoke-virtual {v0}, Landroid/content/ComponentName;->getPackageName()Ljava/lang/String;
				 /* filled-new-array {v2}, [Ljava/lang/Object; */
				 final String v3 = "Delivering SMS to: %s"; // const-string v3, "Delivering SMS to: %s"
				 java.lang.String .format ( v3,v2 );
				 android.util.Log .i ( v1,v2 );
				 /* .line 244 */
			 } // :cond_0
			 /* const/high16 v1, 0x8000000 */
			 (( android.content.Intent ) p1 ).addFlags ( v1 ); // invoke-virtual {p1, v1}, Landroid/content/Intent;->addFlags(I)Landroid/content/Intent;
			 /* .line 245 */
			 final String v1 = "android.provider.Telephony.SMS_DELIVER"; // const-string v1, "android.provider.Telephony.SMS_DELIVER"
			 (( android.content.Intent ) p1 ).setAction ( v1 ); // invoke-virtual {p1, v1}, Landroid/content/Intent;->setAction(Ljava/lang/String;)Landroid/content/Intent;
			 /* .line 246 */
			 /* const/16 v1, 0x10 */
			 v2 = this.mNormalMsgResultReceiver;
			 final String v3 = "android.permission.RECEIVE_SMS"; // const-string v3, "android.permission.RECEIVE_SMS"
			 /* invoke-direct {p0, p1, v3, v1, v2}, Lcom/miui/server/SecuritySmsHandler;->dispatchIntent(Landroid/content/Intent;Ljava/lang/String;ILandroid/content/BroadcastReceiver;)V */
			 /* .line 248 */
			 return;
		 } // .end method
		 private void dispatchSmsToAntiSpam ( android.content.Intent p0 ) {
			 /* .locals 3 */
			 /* .param p1, "intent" # Landroid/content/Intent; */
			 /* .line 210 */
			 final String v0 = "SecuritySmsHandler"; // const-string v0, "SecuritySmsHandler"
			 final String v1 = "dispatchSmsToAntiSpam"; // const-string v1, "dispatchSmsToAntiSpam"
			 android.util.Log .i ( v0,v1 );
			 /* .line 211 */
			 int v0 = 0; // const/4 v0, 0x0
			 (( android.content.Intent ) p1 ).setComponent ( v0 ); // invoke-virtual {p1, v0}, Landroid/content/Intent;->setComponent(Landroid/content/ComponentName;)Landroid/content/Intent;
			 /* .line 212 */
			 final String v1 = "com.android.mms"; // const-string v1, "com.android.mms"
			 (( android.content.Intent ) p1 ).setPackage ( v1 ); // invoke-virtual {p1, v1}, Landroid/content/Intent;->setPackage(Ljava/lang/String;)Landroid/content/Intent;
			 /* .line 213 */
			 final String v1 = "android.provider.Telephony.SMS_DELIVER"; // const-string v1, "android.provider.Telephony.SMS_DELIVER"
			 (( android.content.Intent ) p1 ).setAction ( v1 ); // invoke-virtual {p1, v1}, Landroid/content/Intent;->setAction(Ljava/lang/String;)Landroid/content/Intent;
			 /* .line 214 */
			 final String v1 = "android.permission.RECEIVE_SMS"; // const-string v1, "android.permission.RECEIVE_SMS"
			 /* const/16 v2, 0x10 */
			 /* invoke-direct {p0, p1, v1, v2, v0}, Lcom/miui/server/SecuritySmsHandler;->dispatchIntent(Landroid/content/Intent;Ljava/lang/String;ILandroid/content/BroadcastReceiver;)V */
			 /* .line 215 */
			 return;
		 } // .end method
		 private void dispatchToInterceptApp ( android.content.Intent p0 ) {
			 /* .locals 3 */
			 /* .param p1, "intent" # Landroid/content/Intent; */
			 /* .line 196 */
			 final String v0 = "SecuritySmsHandler"; // const-string v0, "SecuritySmsHandler"
			 final String v1 = "dispatchToInterceptApp"; // const-string v1, "dispatchToInterceptApp"
			 android.util.Log .i ( v0,v1 );
			 /* .line 197 */
			 int v0 = 0; // const/4 v0, 0x0
			 (( android.content.Intent ) p1 ).setFlags ( v0 ); // invoke-virtual {p1, v0}, Landroid/content/Intent;->setFlags(I)Landroid/content/Intent;
			 /* .line 198 */
			 int v0 = 0; // const/4 v0, 0x0
			 (( android.content.Intent ) p1 ).setComponent ( v0 ); // invoke-virtual {p1, v0}, Landroid/content/Intent;->setComponent(Landroid/content/ComponentName;)Landroid/content/Intent;
			 /* .line 199 */
			 v0 = this.mInterceptSmsCallerPkgName;
			 (( android.content.Intent ) p1 ).setPackage ( v0 ); // invoke-virtual {p1, v0}, Landroid/content/Intent;->setPackage(Ljava/lang/String;)Landroid/content/Intent;
			 /* .line 200 */
			 final String v0 = "android.provider.Telephony.SMS_RECEIVED"; // const-string v0, "android.provider.Telephony.SMS_RECEIVED"
			 (( android.content.Intent ) p1 ).setAction ( v0 ); // invoke-virtual {p1, v0}, Landroid/content/Intent;->setAction(Ljava/lang/String;)Landroid/content/Intent;
			 /* .line 201 */
			 /* const/16 v0, 0x10 */
			 v1 = this.mInterceptedSmsResultReceiver;
			 final String v2 = "android.permission.RECEIVE_SMS"; // const-string v2, "android.permission.RECEIVE_SMS"
			 /* invoke-direct {p0, p1, v2, v0, v1}, Lcom/miui/server/SecuritySmsHandler;->dispatchIntent(Landroid/content/Intent;Ljava/lang/String;ILandroid/content/BroadcastReceiver;)V */
			 /* .line 203 */
			 return;
		 } // .end method
		 public static Integer getSlotIdFromIntent ( android.content.Intent p0 ) {
			 /* .locals 3 */
			 /* .param p0, "intent" # Landroid/content/Intent; */
			 /* .line 341 */
			 int v0 = 0; // const/4 v0, 0x0
			 /* .line 342 */
			 /* .local v0, "slotId":I */
			 miui.telephony.TelephonyManager .getDefault ( );
			 v1 = 			 (( miui.telephony.TelephonyManager ) v1 ).getPhoneCount ( ); // invoke-virtual {v1}, Lmiui/telephony/TelephonyManager;->getPhoneCount()I
			 int v2 = 1; // const/4 v2, 0x1
			 /* if-le v1, v2, :cond_0 */
			 /* .line 343 */
			 v1 = miui.telephony.SubscriptionManager.SLOT_KEY;
			 v0 = 			 (( android.content.Intent ) p0 ).getIntExtra ( v1, v0 ); // invoke-virtual {p0, v1, v0}, Landroid/content/Intent;->getIntExtra(Ljava/lang/String;I)I
			 /* .line 344 */
			 /* if-gez v0, :cond_0 */
			 /* .line 345 */
			 final String v1 = "SecuritySmsHandler"; // const-string v1, "SecuritySmsHandler"
			 final String v2 = "getSlotIdFromIntent slotId < 0"; // const-string v2, "getSlotIdFromIntent slotId < 0"
			 android.util.Log .e ( v1,v2 );
			 /* .line 348 */
		 } // :cond_0
	 } // .end method
	 private void releaseSmsIntercept ( ) {
		 /* .locals 2 */
		 /* .line 185 */
		 int v0 = 0; // const/4 v0, 0x0
		 /* iput v0, p0, Lcom/miui/server/SecuritySmsHandler;->mInterceptSmsCallerUid:I */
		 /* .line 186 */
		 int v1 = 0; // const/4 v1, 0x0
		 this.mInterceptSmsCallerPkgName = v1;
		 /* .line 187 */
		 this.mInterceptSmsSenderNum = v1;
		 /* .line 188 */
		 /* iput v0, p0, Lcom/miui/server/SecuritySmsHandler;->mInterceptSmsCount:I */
		 /* .line 189 */
		 return;
	 } // .end method
	 /* # virtual methods */
	 Boolean checkSmsBlocked ( android.content.Intent p0 ) {
		 /* .locals 22 */
		 /* .param p1, "intent" # Landroid/content/Intent; */
		 /* .line 46 */
		 /* move-object/from16 v1, p0 */
		 /* move-object/from16 v2, p1 */
		 /* const-class v0, [B */
		 final String v3 = "enter checkSmsBlocked"; // const-string v3, "enter checkSmsBlocked"
		 final String v4 = "SecuritySmsHandler"; // const-string v4, "SecuritySmsHandler"
		 android.util.Log .i ( v4,v3 );
		 /* .line 47 */
		 int v3 = 0; // const/4 v3, 0x0
		 /* .line 48 */
		 /* .local v3, "blocked":Z */
		 /* invoke-virtual/range {p1 ..p1}, Landroid/content/Intent;->getAction()Ljava/lang/String; */
		 /* .line 49 */
		 /* .local v5, "action":Ljava/lang/String; */
		 v6 = 		 /* invoke-static/range {p1 ..p1}, Lcom/miui/server/SecuritySmsHandler;->getSlotIdFromIntent(Landroid/content/Intent;)I */
		 /* .line 51 */
		 /* .local v6, "slotId":I */
		 final String v7 = "android.provider.Telephony.SMS_DELIVER"; // const-string v7, "android.provider.Telephony.SMS_DELIVER"
		 v7 = 		 (( java.lang.String ) v5 ).equals ( v7 ); // invoke-virtual {v5, v7}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z
		 int v8 = 3; // const/4 v8, 0x3
		 final String v9 = "blockType"; // const-string v9, "blockType"
		 int v10 = 0; // const/4 v10, 0x0
		 if ( v7 != null) { // if-eqz v7, :cond_4
			 /* .line 52 */
			 /* invoke-static/range {p1 ..p1}, Landroid/provider/Telephony$Sms$Intents;->getMessagesFromIntent(Landroid/content/Intent;)[Landroid/telephony/SmsMessage; */
			 /* .line 53 */
			 /* .local v0, "msg":[Landroid/telephony/SmsMessage; */
			 /* new-instance v7, Ljava/lang/StringBuilder; */
			 /* invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V */
			 /* .line 54 */
			 /* .local v7, "sb":Ljava/lang/StringBuilder; */
			 /* if-nez v0, :cond_0 */
			 /* .line 55 */
			 /* .line 57 */
		 } // :cond_0
		 /* array-length v11, v0 */
		 /* move v12, v10 */
	 } // :goto_0
	 /* if-ge v12, v11, :cond_1 */
	 /* aget-object v13, v0, v12 */
	 /* .line 58 */
	 /* .local v13, "smsMessage":Landroid/telephony/SmsMessage; */
	 (( android.telephony.SmsMessage ) v13 ).getDisplayMessageBody ( ); // invoke-virtual {v13}, Landroid/telephony/SmsMessage;->getDisplayMessageBody()Ljava/lang/String;
	 (( java.lang.StringBuilder ) v7 ).append ( v14 ); // invoke-virtual {v7, v14}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
	 /* .line 57 */
} // .end local v13 # "smsMessage":Landroid/telephony/SmsMessage;
/* add-int/lit8 v12, v12, 0x1 */
/* .line 61 */
} // :cond_1
/* aget-object v10, v0, v10 */
(( android.telephony.SmsMessage ) v10 ).getOriginatingAddress ( ); // invoke-virtual {v10}, Landroid/telephony/SmsMessage;->getOriginatingAddress()Ljava/lang/String;
/* .line 62 */
/* .local v10, "address":Ljava/lang/String; */
(( java.lang.StringBuilder ) v7 ).toString ( ); // invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
/* .line 64 */
/* .local v11, "body":Ljava/lang/String; */
v12 = /* invoke-direct {v1, v10}, Lcom/miui/server/SecuritySmsHandler;->checkWithInterceptedSender(Ljava/lang/String;)Z */
if ( v12 != null) { // if-eqz v12, :cond_2
/* .line 65 */
final String v12 = "Intercepted by sender address"; // const-string v12, "Intercepted by sender address"
android.util.Log .i ( v4,v12 );
/* .line 66 */
/* invoke-direct/range {p0 ..p1}, Lcom/miui/server/SecuritySmsHandler;->dispatchToInterceptApp(Landroid/content/Intent;)V */
/* .line 67 */
int v3 = 1; // const/4 v3, 0x1
/* .line 70 */
} // :cond_2
/* if-nez v3, :cond_3 */
/* .line 71 */
v12 = /* invoke-direct {v1, v10, v11, v6}, Lcom/miui/server/SecuritySmsHandler;->checkByAntiSpam(Ljava/lang/String;Ljava/lang/String;I)I */
/* .line 72 */
/* .local v12, "blockType":I */
if ( v12 != null) { // if-eqz v12, :cond_3
/* .line 73 */
(( android.content.Intent ) v2 ).putExtra ( v9, v12 ); // invoke-virtual {v2, v9, v12}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;
/* .line 74 */
v9 = miui.provider.ExtraTelephony .getRealBlockType ( v12 );
/* if-lt v9, v8, :cond_3 */
/* .line 75 */
final String v8 = "This sms is intercepted by AntiSpam"; // const-string v8, "This sms is intercepted by AntiSpam"
android.util.Log .i ( v4,v8 );
/* .line 76 */
/* invoke-direct/range {p0 ..p1}, Lcom/miui/server/SecuritySmsHandler;->dispatchSmsToAntiSpam(Landroid/content/Intent;)V */
/* .line 77 */
int v3 = 1; // const/4 v3, 0x1
/* .line 81 */
} // .end local v0 # "msg":[Landroid/telephony/SmsMessage;
} // .end local v7 # "sb":Ljava/lang/StringBuilder;
} // .end local v10 # "address":Ljava/lang/String;
} // .end local v11 # "body":Ljava/lang/String;
} // .end local v12 # "blockType":I
} // :cond_3
/* move-object/from16 v19, v5 */
/* goto/16 :goto_2 */
} // :cond_4
final String v7 = "android.provider.Telephony.WAP_PUSH_DELIVER"; // const-string v7, "android.provider.Telephony.WAP_PUSH_DELIVER"
v7 = (( java.lang.String ) v5 ).equals ( v7 ); // invoke-virtual {v5, v7}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z
if ( v7 != null) { // if-eqz v7, :cond_5
/* .line 83 */
final String v7 = "data"; // const-string v7, "data"
(( android.content.Intent ) v2 ).getByteArrayExtra ( v7 ); // invoke-virtual {v2, v7}, Landroid/content/Intent;->getByteArrayExtra(Ljava/lang/String;)[B
/* .line 85 */
/* .local v7, "pushData":[B */
android.content.res.Resources .getSystem ( );
/* const v12, 0x11101aa */
v11 = (( android.content.res.Resources ) v11 ).getBoolean ( v12 ); // invoke-virtual {v11, v12}, Landroid/content/res/Resources;->getBoolean(I)Z
/* .line 89 */
/* .local v11, "contentDisposition":Z */
try { // :try_start_0
final String v12 = "com.google.android.mms.pdu.PduParser"; // const-string v12, "com.google.android.mms.pdu.PduParser"
java.lang.Class .forName ( v12 );
/* .line 90 */
/* .local v12, "pduParserClass":Ljava/lang/Class;, "Ljava/lang/Class<*>;" */
final String v13 = "com.google.android.mms.pdu.GenericPdu"; // const-string v13, "com.google.android.mms.pdu.GenericPdu"
java.lang.Class .forName ( v13 );
/* .line 91 */
/* .local v13, "pduClass":Ljava/lang/Class;, "Ljava/lang/Class<*>;" */
final String v14 = "com.google.android.mms.pdu.EncodedStringValue"; // const-string v14, "com.google.android.mms.pdu.EncodedStringValue"
java.lang.Class .forName ( v14 );
/* .line 92 */
/* .local v14, "valueClass":Ljava/lang/Class;, "Ljava/lang/Class<*>;" */
final String v15 = "com.google.android.mms.pdu.PduPersister"; // const-string v15, "com.google.android.mms.pdu.PduPersister"
java.lang.Class .forName ( v15 );
/* .line 93 */
/* .local v15, "persisterClass":Ljava/lang/Class;, "Ljava/lang/Class<*>;" */
int v8 = 2; // const/4 v8, 0x2
/* new-array v10, v8, [Ljava/lang/Class; */
/* const/16 v16, 0x0 */
/* aput-object v0, v10, v16 */
v17 = java.lang.Boolean.TYPE;
int v8 = 1; // const/4 v8, 0x1
/* aput-object v17, v10, v8 */
(( java.lang.Class ) v12 ).getConstructor ( v10 ); // invoke-virtual {v12, v10}, Ljava/lang/Class;->getConstructor([Ljava/lang/Class;)Ljava/lang/reflect/Constructor;
int v8 = 2; // const/4 v8, 0x2
/* new-array v8, v8, [Ljava/lang/Object; */
/* const/16 v16, 0x0 */
/* aput-object v7, v8, v16 */
java.lang.Boolean .valueOf ( v11 );
/* const/16 v17, 0x1 */
/* aput-object v18, v8, v17 */
(( java.lang.reflect.Constructor ) v10 ).newInstance ( v8 ); // invoke-virtual {v10, v8}, Ljava/lang/reflect/Constructor;->newInstance([Ljava/lang/Object;)Ljava/lang/Object;
/* .line 94 */
/* .local v8, "parser":Ljava/lang/Object; */
final String v10 = "parse"; // const-string v10, "parse"
/* :try_end_0 */
/* .catch Ljava/lang/Exception; {:try_start_0 ..:try_end_0} :catch_2 */
/* move/from16 v18, v3 */
/* move-object/from16 v19, v5 */
int v3 = 0; // const/4 v3, 0x0
} // .end local v3 # "blocked":Z
} // .end local v5 # "action":Ljava/lang/String;
/* .local v18, "blocked":Z */
/* .local v19, "action":Ljava/lang/String; */
try { // :try_start_1
/* new-array v5, v3, [Ljava/lang/Class; */
(( java.lang.Class ) v12 ).getMethod ( v10, v5 ); // invoke-virtual {v12, v10, v5}, Ljava/lang/Class;->getMethod(Ljava/lang/String;[Ljava/lang/Class;)Ljava/lang/reflect/Method;
/* new-array v10, v3, [Ljava/lang/Object; */
(( java.lang.reflect.Method ) v5 ).invoke ( v8, v10 ); // invoke-virtual {v5, v8, v10}, Ljava/lang/reflect/Method;->invoke(Ljava/lang/Object;[Ljava/lang/Object;)Ljava/lang/Object;
/* .line 95 */
/* .local v5, "pdu":Ljava/lang/Object; */
final String v10 = "getFrom"; // const-string v10, "getFrom"
/* :try_end_1 */
/* .catch Ljava/lang/Exception; {:try_start_1 ..:try_end_1} :catch_1 */
/* move-object/from16 v20, v7 */
} // .end local v7 # "pushData":[B
/* .local v20, "pushData":[B */
try { // :try_start_2
/* new-array v7, v3, [Ljava/lang/Class; */
(( java.lang.Class ) v13 ).getMethod ( v10, v7 ); // invoke-virtual {v13, v10, v7}, Ljava/lang/Class;->getMethod(Ljava/lang/String;[Ljava/lang/Class;)Ljava/lang/reflect/Method;
/* new-array v10, v3, [Ljava/lang/Object; */
(( java.lang.reflect.Method ) v7 ).invoke ( v5, v10 ); // invoke-virtual {v7, v5, v10}, Ljava/lang/reflect/Method;->invoke(Ljava/lang/Object;[Ljava/lang/Object;)Ljava/lang/Object;
/* .line 96 */
/* .local v7, "encodedStringValue":Ljava/lang/Object; */
final String v10 = "getTextString"; // const-string v10, "getTextString"
/* move-object/from16 v21, v5 */
} // .end local v5 # "pdu":Ljava/lang/Object;
/* .local v21, "pdu":Ljava/lang/Object; */
/* new-array v5, v3, [Ljava/lang/Class; */
(( java.lang.Class ) v14 ).getMethod ( v10, v5 ); // invoke-virtual {v14, v10, v5}, Ljava/lang/Class;->getMethod(Ljava/lang/String;[Ljava/lang/Class;)Ljava/lang/reflect/Method;
/* new-array v10, v3, [Ljava/lang/Object; */
(( java.lang.reflect.Method ) v5 ).invoke ( v7, v10 ); // invoke-virtual {v5, v7, v10}, Ljava/lang/reflect/Method;->invoke(Ljava/lang/Object;[Ljava/lang/Object;)Ljava/lang/Object;
/* check-cast v3, [B */
/* .line 97 */
/* .local v3, "addressByte":[B */
/* const-string/jumbo v5, "toIsoString" */
int v10 = 1; // const/4 v10, 0x1
/* new-array v10, v10, [Ljava/lang/Class; */
/* const/16 v16, 0x0 */
/* aput-object v0, v10, v16 */
(( java.lang.Class ) v15 ).getMethod ( v5, v10 ); // invoke-virtual {v15, v5, v10}, Ljava/lang/Class;->getMethod(Ljava/lang/String;[Ljava/lang/Class;)Ljava/lang/reflect/Method;
/* filled-new-array {v3}, [Ljava/lang/Object; */
int v10 = 0; // const/4 v10, 0x0
(( java.lang.reflect.Method ) v0 ).invoke ( v10, v5 ); // invoke-virtual {v0, v10, v5}, Ljava/lang/reflect/Method;->invoke(Ljava/lang/Object;[Ljava/lang/Object;)Ljava/lang/Object;
/* check-cast v0, Ljava/lang/String; */
/* :try_end_2 */
/* .catch Ljava/lang/Exception; {:try_start_2 ..:try_end_2} :catch_0 */
/* .line 101 */
} // .end local v3 # "addressByte":[B
} // .end local v7 # "encodedStringValue":Ljava/lang/Object;
} // .end local v8 # "parser":Ljava/lang/Object;
} // .end local v12 # "pduParserClass":Ljava/lang/Class;, "Ljava/lang/Class<*>;"
} // .end local v13 # "pduClass":Ljava/lang/Class;, "Ljava/lang/Class<*>;"
} // .end local v14 # "valueClass":Ljava/lang/Class;, "Ljava/lang/Class<*>;"
} // .end local v15 # "persisterClass":Ljava/lang/Class;, "Ljava/lang/Class<*>;"
} // .end local v21 # "pdu":Ljava/lang/Object;
/* .local v0, "address":Ljava/lang/String; */
/* nop */
/* .line 102 */
v3 = /* invoke-direct {v1, v0, v10, v6}, Lcom/miui/server/SecuritySmsHandler;->checkByAntiSpam(Ljava/lang/String;Ljava/lang/String;I)I */
/* .line 103 */
/* .local v3, "blockType":I */
if ( v3 != null) { // if-eqz v3, :cond_6
/* .line 104 */
(( android.content.Intent ) v2 ).putExtra ( v9, v3 ); // invoke-virtual {v2, v9, v3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;
/* .line 105 */
v5 = miui.provider.ExtraTelephony .getRealBlockType ( v3 );
int v7 = 3; // const/4 v7, 0x3
/* if-lt v5, v7, :cond_6 */
/* .line 106 */
final String v5 = "This mms is intercepted by AntiSpam"; // const-string v5, "This mms is intercepted by AntiSpam"
android.util.Log .i ( v4,v5 );
/* .line 107 */
/* invoke-direct/range {p0 ..p1}, Lcom/miui/server/SecuritySmsHandler;->dispatchMmsToAntiSpam(Landroid/content/Intent;)V */
/* .line 108 */
int v5 = 1; // const/4 v5, 0x1
/* move v3, v5 */
} // .end local v18 # "blocked":Z
/* .local v5, "blocked":Z */
/* .line 98 */
} // .end local v0 # "address":Ljava/lang/String;
} // .end local v3 # "blockType":I
} // .end local v5 # "blocked":Z
/* .restart local v18 # "blocked":Z */
/* :catch_0 */
/* move-exception v0 */
} // .end local v20 # "pushData":[B
/* .local v7, "pushData":[B */
/* :catch_1 */
/* move-exception v0 */
/* move-object/from16 v20, v7 */
} // .end local v7 # "pushData":[B
/* .restart local v20 # "pushData":[B */
} // .end local v18 # "blocked":Z
} // .end local v19 # "action":Ljava/lang/String;
} // .end local v20 # "pushData":[B
/* .local v3, "blocked":Z */
/* .local v5, "action":Ljava/lang/String; */
/* .restart local v7 # "pushData":[B */
/* :catch_2 */
/* move-exception v0 */
/* move/from16 v18, v3 */
/* move-object/from16 v19, v5 */
/* move-object/from16 v20, v7 */
/* .line 99 */
} // .end local v3 # "blocked":Z
} // .end local v5 # "action":Ljava/lang/String;
} // .end local v7 # "pushData":[B
/* .local v0, "e":Ljava/lang/Exception; */
/* .restart local v18 # "blocked":Z */
/* .restart local v19 # "action":Ljava/lang/String; */
/* .restart local v20 # "pushData":[B */
} // :goto_1
(( java.lang.Exception ) v0 ).printStackTrace ( ); // invoke-virtual {v0}, Ljava/lang/Exception;->printStackTrace()V
/* .line 100 */
int v3 = 0; // const/4 v3, 0x0
/* .line 81 */
} // .end local v0 # "e":Ljava/lang/Exception;
} // .end local v11 # "contentDisposition":Z
} // .end local v18 # "blocked":Z
} // .end local v19 # "action":Ljava/lang/String;
} // .end local v20 # "pushData":[B
/* .restart local v3 # "blocked":Z */
/* .restart local v5 # "action":Ljava/lang/String; */
} // :cond_5
/* move/from16 v18, v3 */
/* move-object/from16 v19, v5 */
/* .line 112 */
} // .end local v3 # "blocked":Z
} // .end local v5 # "action":Ljava/lang/String;
/* .restart local v18 # "blocked":Z */
/* .restart local v19 # "action":Ljava/lang/String; */
} // :cond_6
/* move/from16 v3, v18 */
} // .end local v18 # "blocked":Z
/* .restart local v3 # "blocked":Z */
} // :goto_2
/* new-instance v0, Ljava/lang/StringBuilder; */
/* invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V */
final String v5 = "leave checkSmsBlocked, blocked:"; // const-string v5, "leave checkSmsBlocked, blocked:"
(( java.lang.StringBuilder ) v0 ).append ( v5 ); // invoke-virtual {v0, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v0 ).append ( v3 ); // invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v0 ).toString ( ); // invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
android.util.Log .i ( v4,v0 );
/* .line 113 */
} // .end method
Boolean startInterceptSmsBySender ( java.lang.String p0, java.lang.String p1, Integer p2 ) {
/* .locals 3 */
/* .param p1, "pkgName" # Ljava/lang/String; */
/* .param p2, "sender" # Ljava/lang/String; */
/* .param p3, "count" # I */
/* .line 117 */
v0 = this.mContext;
final String v1 = "com.miui.permission.MANAGE_SMS_INTERCEPT"; // const-string v1, "com.miui.permission.MANAGE_SMS_INTERCEPT"
final String v2 = "SecuritySmsHandler"; // const-string v2, "SecuritySmsHandler"
(( android.content.Context ) v0 ).enforceCallingOrSelfPermission ( v1, v2 ); // invoke-virtual {v0, v1, v2}, Landroid/content/Context;->enforceCallingOrSelfPermission(Ljava/lang/String;Ljava/lang/String;)V
/* .line 118 */
v0 = android.os.Binder .getCallingUid ( );
/* .line 119 */
/* .local v0, "callerUid":I */
v1 = this.mInterceptSmsLock;
/* monitor-enter v1 */
/* .line 120 */
try { // :try_start_0
/* iget v2, p0, Lcom/miui/server/SecuritySmsHandler;->mInterceptSmsCallerUid:I */
/* if-nez v2, :cond_0 */
/* .line 121 */
/* iput v0, p0, Lcom/miui/server/SecuritySmsHandler;->mInterceptSmsCallerUid:I */
/* .line 122 */
this.mInterceptSmsCallerPkgName = p1;
/* .line 123 */
this.mInterceptSmsSenderNum = p2;
/* .line 124 */
/* iput p3, p0, Lcom/miui/server/SecuritySmsHandler;->mInterceptSmsCount:I */
/* .line 125 */
/* monitor-exit v1 */
int v1 = 1; // const/4 v1, 0x1
/* .line 127 */
} // :cond_0
/* monitor-exit v1 */
/* .line 128 */
int v1 = 0; // const/4 v1, 0x0
/* .line 127 */
/* :catchall_0 */
/* move-exception v2 */
/* monitor-exit v1 */
/* :try_end_0 */
/* .catchall {:try_start_0 ..:try_end_0} :catchall_0 */
/* throw v2 */
} // .end method
Boolean stopInterceptSmsBySender ( ) {
/* .locals 4 */
/* .line 132 */
/* nop */
/* .line 136 */
v0 = this.mContext;
final String v1 = "com.miui.permission.MANAGE_SMS_INTERCEPT"; // const-string v1, "com.miui.permission.MANAGE_SMS_INTERCEPT"
final String v2 = "SecuritySmsHandler"; // const-string v2, "SecuritySmsHandler"
(( android.content.Context ) v0 ).enforceCallingOrSelfPermission ( v1, v2 ); // invoke-virtual {v0, v1, v2}, Landroid/content/Context;->enforceCallingOrSelfPermission(Ljava/lang/String;Ljava/lang/String;)V
/* .line 137 */
v0 = android.os.Binder .getCallingUid ( );
/* .line 138 */
/* .local v0, "callerUid":I */
v1 = this.mInterceptSmsLock;
/* monitor-enter v1 */
/* .line 139 */
try { // :try_start_0
/* iget v2, p0, Lcom/miui/server/SecuritySmsHandler;->mInterceptSmsCallerUid:I */
int v3 = 1; // const/4 v3, 0x1
/* if-nez v2, :cond_0 */
/* .line 140 */
/* monitor-exit v1 */
/* .line 142 */
} // :cond_0
/* if-ne v2, v0, :cond_1 */
/* .line 143 */
/* invoke-direct {p0}, Lcom/miui/server/SecuritySmsHandler;->releaseSmsIntercept()V */
/* .line 144 */
/* monitor-exit v1 */
/* .line 146 */
} // :cond_1
/* monitor-exit v1 */
/* .line 147 */
int v1 = 0; // const/4 v1, 0x0
/* .line 146 */
/* :catchall_0 */
/* move-exception v2 */
/* monitor-exit v1 */
/* :try_end_0 */
/* .catchall {:try_start_0 ..:try_end_0} :catchall_0 */
/* throw v2 */
} // .end method
