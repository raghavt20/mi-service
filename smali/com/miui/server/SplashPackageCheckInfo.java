public class com.miui.server.SplashPackageCheckInfo implements android.os.Parcelable {
	 /* .source "SplashPackageCheckInfo.java" */
	 /* # interfaces */
	 /* # static fields */
	 public static final android.os.Parcelable$Creator CREATOR;
	 /* .annotation system Ldalvik/annotation/Signature; */
	 /* value = { */
	 /* "Landroid/os/Parcelable$Creator<", */
	 /* "Lcom/miui/server/SplashPackageCheckInfo;", */
	 /* ">;" */
	 /* } */
} // .end annotation
} // .end field
public static final Long IGNORE;
/* # instance fields */
private Long mEndCheckTime;
private java.lang.String mSplashPackageName;
private Long mStartCheckTime;
/* # direct methods */
static com.miui.server.SplashPackageCheckInfo ( ) {
/* .locals 1 */
/* .line 46 */
/* new-instance v0, Lcom/miui/server/SplashPackageCheckInfo$1; */
/* invoke-direct {v0}, Lcom/miui/server/SplashPackageCheckInfo$1;-><init>()V */
return;
} // .end method
private com.miui.server.SplashPackageCheckInfo ( ) {
/* .locals 2 */
/* .param p1, "source" # Landroid/os/Parcel; */
/* .line 29 */
/* invoke-direct {p0}, Ljava/lang/Object;-><init>()V */
/* .line 30 */
(( android.os.Parcel ) p1 ).readString ( ); // invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;
this.mSplashPackageName = v0;
/* .line 31 */
(( android.os.Parcel ) p1 ).readLong ( ); // invoke-virtual {p1}, Landroid/os/Parcel;->readLong()J
/* move-result-wide v0 */
/* iput-wide v0, p0, Lcom/miui/server/SplashPackageCheckInfo;->mStartCheckTime:J */
/* .line 32 */
(( android.os.Parcel ) p1 ).readLong ( ); // invoke-virtual {p1}, Landroid/os/Parcel;->readLong()J
/* move-result-wide v0 */
/* iput-wide v0, p0, Lcom/miui/server/SplashPackageCheckInfo;->mEndCheckTime:J */
/* .line 33 */
return;
} // .end method
 com.miui.server.SplashPackageCheckInfo ( ) { //synthethic
/* .locals 0 */
/* invoke-direct {p0, p1}, Lcom/miui/server/SplashPackageCheckInfo;-><init>(Landroid/os/Parcel;)V */
return;
} // .end method
public com.miui.server.SplashPackageCheckInfo ( ) {
/* .locals 0 */
/* .param p1, "splashPackageName" # Ljava/lang/String; */
/* .param p2, "startCheckTime" # J */
/* .param p4, "endCheckTime" # J */
/* .line 23 */
/* invoke-direct {p0}, Ljava/lang/Object;-><init>()V */
/* .line 24 */
this.mSplashPackageName = p1;
/* .line 25 */
/* iput-wide p2, p0, Lcom/miui/server/SplashPackageCheckInfo;->mStartCheckTime:J */
/* .line 26 */
/* iput-wide p4, p0, Lcom/miui/server/SplashPackageCheckInfo;->mEndCheckTime:J */
/* .line 27 */
return;
} // .end method
private static java.lang.String getDateString ( Long p0 ) {
/* .locals 3 */
/* .param p0, "time" # J */
/* .line 81 */
/* const-wide/16 v0, -0x1 */
/* cmp-long v0, p0, v0 */
/* if-nez v0, :cond_0 */
/* .line 82 */
final String v0 = "IGNORE"; // const-string v0, "IGNORE"
/* .line 85 */
} // :cond_0
try { // :try_start_0
/* new-instance v0, Ljava/util/Date; */
/* invoke-direct {v0, p0, p1}, Ljava/util/Date;-><init>(J)V */
/* .line 86 */
/* .local v0, "date":Ljava/util/Date; */
/* new-instance v1, Ljava/text/SimpleDateFormat; */
/* const-string/jumbo v2, "yyyy-MM-dd HH:mm:ss" */
/* invoke-direct {v1, v2}, Ljava/text/SimpleDateFormat;-><init>(Ljava/lang/String;)V */
(( java.text.SimpleDateFormat ) v1 ).format ( v0 ); // invoke-virtual {v1, v0}, Ljava/text/SimpleDateFormat;->format(Ljava/util/Date;)Ljava/lang/String;
/* :try_end_0 */
/* .catch Ljava/lang/Exception; {:try_start_0 ..:try_end_0} :catch_0 */
/* .line 87 */
} // .end local v0 # "date":Ljava/util/Date;
/* :catch_0 */
/* move-exception v0 */
/* .line 90 */
final String v0 = "ERROR"; // const-string v0, "ERROR"
} // .end method
/* # virtual methods */
public Integer describeContents ( ) {
/* .locals 1 */
/* .line 43 */
int v0 = 0; // const/4 v0, 0x0
} // .end method
public java.lang.String getSplashPackageName ( ) {
/* .locals 1 */
/* .line 58 */
v0 = this.mSplashPackageName;
} // .end method
public Boolean isExpired ( ) {
/* .locals 4 */
/* .line 69 */
/* iget-wide v0, p0, Lcom/miui/server/SplashPackageCheckInfo;->mEndCheckTime:J */
/* const-wide/16 v2, -0x1 */
/* cmp-long v2, v0, v2 */
if ( v2 != null) { // if-eqz v2, :cond_0
java.lang.System .currentTimeMillis ( );
/* move-result-wide v2 */
/* cmp-long v0, v0, v2 */
/* if-gez v0, :cond_0 */
int v0 = 1; // const/4 v0, 0x1
} // :cond_0
int v0 = 0; // const/4 v0, 0x0
} // :goto_0
} // .end method
public Boolean matchTime ( ) {
/* .locals 7 */
/* .line 62 */
java.lang.System .currentTimeMillis ( );
/* move-result-wide v0 */
/* .line 63 */
/* .local v0, "currentTime":J */
/* iget-wide v2, p0, Lcom/miui/server/SplashPackageCheckInfo;->mStartCheckTime:J */
/* const-wide/16 v4, -0x1 */
/* cmp-long v6, v2, v4 */
if ( v6 != null) { // if-eqz v6, :cond_0
/* cmp-long v2, v2, v0 */
/* if-gez v2, :cond_1 */
} // :cond_0
/* iget-wide v2, p0, Lcom/miui/server/SplashPackageCheckInfo;->mEndCheckTime:J */
/* cmp-long v4, v2, v4 */
if ( v4 != null) { // if-eqz v4, :cond_2
/* cmp-long v2, v2, v0 */
/* if-lez v2, :cond_1 */
} // :cond_1
int v2 = 0; // const/4 v2, 0x0
} // :cond_2
} // :goto_0
int v2 = 1; // const/4 v2, 0x1
} // :goto_1
} // .end method
public java.lang.String toString ( ) {
/* .locals 4 */
/* .line 74 */
/* new-instance v0, Ljava/lang/StringBuilder; */
/* invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V */
final String v1 = "SplashPackageCheckInfo["; // const-string v1, "SplashPackageCheckInfo["
(( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
v1 = this.mSplashPackageName;
(( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
final String v1 = ", "; // const-string v1, ", "
(( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
/* iget-wide v2, p0, Lcom/miui/server/SplashPackageCheckInfo;->mStartCheckTime:J */
/* .line 75 */
com.miui.server.SplashPackageCheckInfo .getDateString ( v2,v3 );
(( java.lang.StringBuilder ) v0 ).append ( v2 ); // invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
/* iget-wide v1, p0, Lcom/miui/server/SplashPackageCheckInfo;->mEndCheckTime:J */
/* .line 76 */
com.miui.server.SplashPackageCheckInfo .getDateString ( v1,v2 );
(( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
final String v1 = "]"; // const-string v1, "]"
(( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v0 ).toString ( ); // invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
/* .line 74 */
} // .end method
public void writeToParcel ( android.os.Parcel p0, Integer p1 ) {
/* .locals 2 */
/* .param p1, "dest" # Landroid/os/Parcel; */
/* .param p2, "parcelableFlags" # I */
/* .line 36 */
v0 = this.mSplashPackageName;
(( android.os.Parcel ) p1 ).writeString ( v0 ); // invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V
/* .line 37 */
/* iget-wide v0, p0, Lcom/miui/server/SplashPackageCheckInfo;->mStartCheckTime:J */
(( android.os.Parcel ) p1 ).writeLong ( v0, v1 ); // invoke-virtual {p1, v0, v1}, Landroid/os/Parcel;->writeLong(J)V
/* .line 38 */
/* iget-wide v0, p0, Lcom/miui/server/SplashPackageCheckInfo;->mEndCheckTime:J */
(( android.os.Parcel ) p1 ).writeLong ( v0, v1 ); // invoke-virtual {p1, v0, v1}, Landroid/os/Parcel;->writeLong(J)V
/* .line 39 */
return;
} // .end method
