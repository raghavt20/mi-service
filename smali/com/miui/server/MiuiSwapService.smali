.class public Lcom/miui/server/MiuiSwapService;
.super Lmiui/swap/ISwapManager$Stub;
.source "MiuiSwapService.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/miui/server/MiuiSwapService$SwapServiceHandler;,
        Lcom/miui/server/MiuiSwapService$UsbStatusReceiver;,
        Lcom/miui/server/MiuiSwapService$Lifecycle;
    }
.end annotation


# static fields
.field public static final ACTION_TEST_USB_STATE:Ljava/lang/String; = "android.hardware.usb.action.USB_STATE"

.field private static final EXECUTE_SWAP_DISABLE:I = 0x4

.field private static final EXECUTE_SWAP_trigger:I = 0x2

.field private static final HAL_DEFAULT:Ljava/lang/String; = "default"

.field private static final HAL_INTERFACE_DESCRIPTOR:Ljava/lang/String; = "vendor.xiaomi.hardware.swap@1.0::ISwap"

.field private static final HAL_SERVICE_NAME:Ljava/lang/String; = "vendor.xiaomi.hardware.swap@1.0::ISwap"

.field public static final IS_SWAP_ENABLE:Z

.field private static final IS_SWAP_SUPPORTED:I = 0x1

.field private static final NATIVE_SERVICE_KEY:Ljava/lang/String; = "persist.sys.swapservice.ctrl"

.field private static final NATIVE_SERVICE_NAME:Ljava/lang/String; = "SwapNativeService"

.field public static final SERVICE_NAME:Ljava/lang/String; = "miui.swap.service"

.field private static final START_SWAP:I = 0x1

.field public static final SWAP_DEBUG:Z

.field private static final TAG:Ljava/lang/String;

.field private static final WHETHER_START:I = 0x2

.field private static volatile mSwapServiceHandler:Lcom/miui/server/MiuiSwapService$SwapServiceHandler;


# instance fields
.field private mContext:Landroid/content/Context;

.field private volatile mSwapNativeService:Lmiui/swap/ISwap;

.field private mSwapServiceThread:Landroid/os/HandlerThread;

.field private mUsbConnect:Z


# direct methods
.method static bridge synthetic -$$Nest$fgetmUsbConnect(Lcom/miui/server/MiuiSwapService;)Z
    .locals 0

    iget-boolean p0, p0, Lcom/miui/server/MiuiSwapService;->mUsbConnect:Z

    return p0
.end method

.method static bridge synthetic -$$Nest$fputmSwapNativeService(Lcom/miui/server/MiuiSwapService;Lmiui/swap/ISwap;)V
    .locals 0

    iput-object p1, p0, Lcom/miui/server/MiuiSwapService;->mSwapNativeService:Lmiui/swap/ISwap;

    return-void
.end method

.method static bridge synthetic -$$Nest$fputmUsbConnect(Lcom/miui/server/MiuiSwapService;Z)V
    .locals 0

    iput-boolean p1, p0, Lcom/miui/server/MiuiSwapService;->mUsbConnect:Z

    return-void
.end method

.method static bridge synthetic -$$Nest$mstartSwapLocked(Lcom/miui/server/MiuiSwapService;)V
    .locals 0

    invoke-direct {p0}, Lcom/miui/server/MiuiSwapService;->startSwapLocked()V

    return-void
.end method

.method static bridge synthetic -$$Nest$sfgetTAG()Ljava/lang/String;
    .locals 1

    sget-object v0, Lcom/miui/server/MiuiSwapService;->TAG:Ljava/lang/String;

    return-object v0
.end method

.method static constructor <clinit>()V
    .locals 2

    .line 29
    const-class v0, Lcom/miui/server/MiuiSwapService;

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/miui/server/MiuiSwapService;->TAG:Ljava/lang/String;

    .line 39
    const-string v0, "persist.sys.miui_swap_debug"

    const/4 v1, 0x0

    invoke-static {v0, v1}, Landroid/os/SystemProperties;->getBoolean(Ljava/lang/String;Z)Z

    move-result v0

    sput-boolean v0, Lcom/miui/server/MiuiSwapService;->SWAP_DEBUG:Z

    .line 40
    const-string v0, "persist.sys.stability.swapEnable"

    invoke-static {v0, v1}, Landroid/os/SystemProperties;->getBoolean(Ljava/lang/String;Z)Z

    move-result v0

    sput-boolean v0, Lcom/miui/server/MiuiSwapService;->IS_SWAP_ENABLE:Z

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;)V
    .locals 4
    .param p1, "context"    # Landroid/content/Context;

    .line 97
    invoke-direct {p0}, Lmiui/swap/ISwapManager$Stub;-><init>()V

    .line 48
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/miui/server/MiuiSwapService;->mUsbConnect:Z

    .line 98
    iput-object p1, p0, Lcom/miui/server/MiuiSwapService;->mContext:Landroid/content/Context;

    .line 99
    invoke-virtual {p0}, Lcom/miui/server/MiuiSwapService;->getUsbStatus()V

    .line 100
    new-instance v0, Landroid/os/HandlerThread;

    const-string/jumbo v1, "swapServiceWork"

    invoke-direct {v0, v1}, Landroid/os/HandlerThread;-><init>(Ljava/lang/String;)V

    iput-object v0, p0, Lcom/miui/server/MiuiSwapService;->mSwapServiceThread:Landroid/os/HandlerThread;

    .line 101
    invoke-virtual {v0}, Landroid/os/HandlerThread;->start()V

    .line 102
    new-instance v0, Lcom/miui/server/MiuiSwapService$SwapServiceHandler;

    iget-object v1, p0, Lcom/miui/server/MiuiSwapService;->mSwapServiceThread:Landroid/os/HandlerThread;

    invoke-virtual {v1}, Landroid/os/HandlerThread;->getLooper()Landroid/os/Looper;

    move-result-object v1

    invoke-direct {v0, p0, v1}, Lcom/miui/server/MiuiSwapService$SwapServiceHandler;-><init>(Lcom/miui/server/MiuiSwapService;Landroid/os/Looper;)V

    sput-object v0, Lcom/miui/server/MiuiSwapService;->mSwapServiceHandler:Lcom/miui/server/MiuiSwapService$SwapServiceHandler;

    .line 104
    sget-object v0, Lcom/miui/server/MiuiSwapService;->mSwapServiceHandler:Lcom/miui/server/MiuiSwapService$SwapServiceHandler;

    const/4 v1, 0x2

    const-wide/16 v2, 0x1388

    invoke-virtual {v0, v1, v2, v3}, Lcom/miui/server/MiuiSwapService$SwapServiceHandler;->sendEmptyMessageDelayed(IJ)Z

    .line 106
    sget-object v0, Lcom/miui/server/MiuiSwapService;->mSwapServiceHandler:Lcom/miui/server/MiuiSwapService$SwapServiceHandler;

    const/4 v1, 0x1

    const-wide/16 v2, 0x2710

    invoke-virtual {v0, v1, v2, v3}, Lcom/miui/server/MiuiSwapService$SwapServiceHandler;->sendEmptyMessageDelayed(IJ)Z

    .line 107
    return-void
.end method

.method private callHalSwap(Ljava/lang/String;)V
    .locals 7
    .param p1, "nativeResult"    # Ljava/lang/String;

    .line 188
    const-string/jumbo v0, "vendor.xiaomi.hardware.swap@1.0::ISwap"

    new-instance v1, Landroid/os/HwParcel;

    invoke-direct {v1}, Landroid/os/HwParcel;-><init>()V

    .line 190
    .local v1, "hidl_reply":Landroid/os/HwParcel;
    :try_start_0
    const-string v2, "default"

    invoke-static {v0, v2}, Landroid/os/HwBinder;->getService(Ljava/lang/String;Ljava/lang/String;)Landroid/os/IHwBinder;

    move-result-object v2

    .line 191
    .local v2, "hwService":Landroid/os/IHwBinder;
    if-eqz v2, :cond_1

    .line 192
    invoke-direct {p0}, Lcom/miui/server/MiuiSwapService;->isSWAPSupport()Z

    move-result v3

    if-eqz v3, :cond_0

    .line 193
    new-instance v3, Landroid/os/HwParcel;

    invoke-direct {v3}, Landroid/os/HwParcel;-><init>()V

    .line 194
    .local v3, "hidl_request":Landroid/os/HwParcel;
    invoke-virtual {v3, v0}, Landroid/os/HwParcel;->writeInterfaceToken(Ljava/lang/String;)V

    .line 195
    invoke-virtual {v3, p1}, Landroid/os/HwParcel;->writeString(Ljava/lang/String;)V

    .line 196
    const/4 v0, 0x2

    const/4 v4, 0x0

    invoke-interface {v2, v0, v3, v1, v4}, Landroid/os/IHwBinder;->transact(ILandroid/os/HwParcel;Landroid/os/HwParcel;I)V

    .line 197
    invoke-virtual {v1}, Landroid/os/HwParcel;->verifySuccess()V

    .line 198
    invoke-virtual {v3}, Landroid/os/HwParcel;->releaseTemporaryStorage()V

    .line 199
    invoke-virtual {v1}, Landroid/os/HwParcel;->readString()Ljava/lang/String;

    move-result-object v0

    .line 200
    .local v0, "HalReturnResult":Ljava/lang/String;
    sget-object v4, Lcom/miui/server/MiuiSwapService;->TAG:Ljava/lang/String;

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "callHalSwap return result is :"

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5}, Landroid/util/Slog;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 201
    nop

    .end local v0    # "HalReturnResult":Ljava/lang/String;
    .end local v3    # "hidl_request":Landroid/os/HwParcel;
    goto :goto_0

    .line 202
    :cond_0
    sget-object v0, Lcom/miui/server/MiuiSwapService;->TAG:Ljava/lang/String;

    const-string v3, "This device does not support swap on hal"

    invoke-static {v0, v3}, Landroid/util/Slog;->d(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 208
    .end local v2    # "hwService":Landroid/os/IHwBinder;
    :cond_1
    :goto_0
    nop

    :goto_1
    invoke-virtual {v1}, Landroid/os/HwParcel;->release()V

    .line 209
    goto :goto_2

    .line 208
    :catchall_0
    move-exception v0

    goto :goto_3

    .line 205
    :catch_0
    move-exception v0

    .line 206
    .local v0, "e":Ljava/lang/Exception;
    :try_start_1
    sget-object v2, Lcom/miui/server/MiuiSwapService;->TAG:Ljava/lang/String;

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "fail to call SwapHalService"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Landroid/util/Slog;->e(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 208
    nop

    .end local v0    # "e":Ljava/lang/Exception;
    goto :goto_1

    .line 210
    :goto_2
    return-void

    .line 208
    :goto_3
    invoke-virtual {v1}, Landroid/os/HwParcel;->release()V

    .line 209
    throw v0
.end method

.method private getNativeService()Lmiui/swap/ISwap;
    .locals 5

    .line 153
    const/4 v0, 0x0

    .line 155
    .local v0, "Ibinder":Landroid/os/IBinder;
    :try_start_0
    const-string v1, "persist.sys.swapservice.ctrl"

    const/4 v2, 0x0

    invoke-static {v1, v2}, Landroid/os/SystemProperties;->getBoolean(Ljava/lang/String;Z)Z

    move-result v1

    invoke-static {v1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v1

    .line 156
    .local v1, "isStartService":Ljava/lang/Boolean;
    invoke-virtual {v1}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v3

    if-eqz v3, :cond_0

    .line 157
    const-string v3, "SwapNativeService"

    invoke-static {v3}, Landroid/os/ServiceManager;->getService(Ljava/lang/String;)Landroid/os/IBinder;

    move-result-object v3

    move-object v0, v3

    .line 158
    if-eqz v0, :cond_0

    .line 159
    new-instance v3, Lcom/miui/server/MiuiSwapService$1;

    invoke-direct {v3, p0}, Lcom/miui/server/MiuiSwapService$1;-><init>(Lcom/miui/server/MiuiSwapService;)V

    invoke-interface {v0, v3, v2}, Landroid/os/IBinder;->linkToDeath(Landroid/os/IBinder$DeathRecipient;I)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 170
    .end local v1    # "isStartService":Ljava/lang/Boolean;
    :cond_0
    goto :goto_0

    .line 168
    :catch_0
    move-exception v1

    .line 169
    .local v1, "e":Ljava/lang/Exception;
    sget-object v2, Lcom/miui/server/MiuiSwapService;->TAG:Ljava/lang/String;

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "fail to get SwapNativeService:"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Landroid/util/Slog;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 171
    .end local v1    # "e":Ljava/lang/Exception;
    :goto_0
    if-eqz v0, :cond_1

    .line 172
    invoke-static {v0}, Lmiui/swap/ISwap$Stub;->asInterface(Landroid/os/IBinder;)Lmiui/swap/ISwap;

    move-result-object v1

    iput-object v1, p0, Lcom/miui/server/MiuiSwapService;->mSwapNativeService:Lmiui/swap/ISwap;

    .line 173
    sget-object v1, Lcom/miui/server/MiuiSwapService;->TAG:Ljava/lang/String;

    const-string v2, "get ISwap"

    invoke-static {v1, v2}, Landroid/util/Slog;->w(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_1

    .line 175
    :cond_1
    const/4 v1, 0x0

    iput-object v1, p0, Lcom/miui/server/MiuiSwapService;->mSwapNativeService:Lmiui/swap/ISwap;

    .line 176
    sget-object v1, Lcom/miui/server/MiuiSwapService;->TAG:Ljava/lang/String;

    const-string v2, "ISwap get failed, please try again"

    invoke-static {v1, v2}, Landroid/util/Slog;->w(Ljava/lang/String;Ljava/lang/String;)I

    .line 178
    :goto_1
    iget-object v1, p0, Lcom/miui/server/MiuiSwapService;->mSwapNativeService:Lmiui/swap/ISwap;

    return-object v1
.end method

.method private isSWAPSupport()Z
    .locals 6

    .line 240
    const-string/jumbo v0, "vendor.xiaomi.hardware.swap@1.0::ISwap"

    new-instance v1, Landroid/os/HwParcel;

    invoke-direct {v1}, Landroid/os/HwParcel;-><init>()V

    .line 242
    .local v1, "hidl_reply":Landroid/os/HwParcel;
    const/4 v2, 0x0

    :try_start_0
    const-string v3, "default"

    invoke-static {v0, v3}, Landroid/os/HwBinder;->getService(Ljava/lang/String;Ljava/lang/String;)Landroid/os/IHwBinder;

    move-result-object v3

    .line 243
    .local v3, "hwService":Landroid/os/IHwBinder;
    if-eqz v3, :cond_0

    .line 244
    new-instance v4, Landroid/os/HwParcel;

    invoke-direct {v4}, Landroid/os/HwParcel;-><init>()V

    .line 245
    .local v4, "hidl_request":Landroid/os/HwParcel;
    invoke-virtual {v4, v0}, Landroid/os/HwParcel;->writeInterfaceToken(Ljava/lang/String;)V

    .line 246
    const/4 v0, 0x1

    invoke-interface {v3, v0, v4, v1, v2}, Landroid/os/IHwBinder;->transact(ILandroid/os/HwParcel;Landroid/os/HwParcel;I)V

    .line 247
    invoke-virtual {v1}, Landroid/os/HwParcel;->verifySuccess()V

    .line 248
    invoke-virtual {v4}, Landroid/os/HwParcel;->releaseTemporaryStorage()V

    .line 249
    invoke-virtual {v1}, Landroid/os/HwParcel;->readBool()Z

    move-result v0
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 250
    .local v0, "flag":Z
    nop

    .line 257
    invoke-virtual {v1}, Landroid/os/HwParcel;->release()V

    .line 250
    return v0

    .line 252
    .end local v0    # "flag":Z
    .end local v4    # "hidl_request":Landroid/os/HwParcel;
    :cond_0
    :try_start_1
    sget-object v0, Lcom/miui/server/MiuiSwapService;->TAG:Ljava/lang/String;

    const-string v4, "hwService get failed, please try again"

    invoke-static {v0, v4}, Landroid/util/Slog;->d(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_0
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 257
    nop

    .end local v3    # "hwService":Landroid/os/IHwBinder;
    :goto_0
    invoke-virtual {v1}, Landroid/os/HwParcel;->release()V

    .line 258
    goto :goto_1

    .line 257
    :catchall_0
    move-exception v0

    goto :goto_2

    .line 254
    :catch_0
    move-exception v0

    .line 255
    .local v0, "e":Ljava/lang/Exception;
    :try_start_2
    sget-object v3, Lcom/miui/server/MiuiSwapService;->TAG:Ljava/lang/String;

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "fail to get SwapHalService"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Landroid/util/Slog;->e(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    .line 257
    nop

    .end local v0    # "e":Ljava/lang/Exception;
    goto :goto_0

    .line 259
    :goto_1
    return v2

    .line 257
    :goto_2
    invoke-virtual {v1}, Landroid/os/HwParcel;->release()V

    .line 258
    throw v0
.end method

.method private startSwapLocked()V
    .locals 4

    .line 111
    invoke-virtual {p0}, Lcom/miui/server/MiuiSwapService;->callNativeSwap()Ljava/lang/String;

    move-result-object v0

    .line 112
    .local v0, "nativeReturnReslut":Ljava/lang/String;
    sget-object v1, Lcom/miui/server/MiuiSwapService;->TAG:Ljava/lang/String;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "native return result :"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Slog;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 113
    const-string v2, ""

    if-eq v0, v2, :cond_0

    .line 114
    invoke-direct {p0, v0}, Lcom/miui/server/MiuiSwapService;->callHalSwap(Ljava/lang/String;)V

    goto :goto_0

    .line 116
    :cond_0
    const-string v2, "callNativeSwap return result is null"

    invoke-static {v1, v2}, Landroid/util/Slog;->w(Ljava/lang/String;Ljava/lang/String;)I

    .line 118
    :goto_0
    return-void
.end method

.method private swapHalDisable()Z
    .locals 6

    .line 217
    const-string/jumbo v0, "vendor.xiaomi.hardware.swap@1.0::ISwap"

    new-instance v1, Landroid/os/HwParcel;

    invoke-direct {v1}, Landroid/os/HwParcel;-><init>()V

    .line 219
    .local v1, "hidl_reply":Landroid/os/HwParcel;
    const/4 v2, 0x0

    :try_start_0
    const-string v3, "default"

    invoke-static {v0, v3}, Landroid/os/HwBinder;->getService(Ljava/lang/String;Ljava/lang/String;)Landroid/os/IHwBinder;

    move-result-object v3

    .line 220
    .local v3, "hwService":Landroid/os/IHwBinder;
    if-eqz v3, :cond_0

    .line 221
    new-instance v4, Landroid/os/HwParcel;

    invoke-direct {v4}, Landroid/os/HwParcel;-><init>()V

    .line 222
    .local v4, "hidl_request":Landroid/os/HwParcel;
    invoke-virtual {v4, v0}, Landroid/os/HwParcel;->writeInterfaceToken(Ljava/lang/String;)V

    .line 223
    const/4 v0, 0x4

    invoke-interface {v3, v0, v4, v1, v2}, Landroid/os/IHwBinder;->transact(ILandroid/os/HwParcel;Landroid/os/HwParcel;I)V

    .line 224
    invoke-virtual {v1}, Landroid/os/HwParcel;->verifySuccess()V

    .line 225
    invoke-virtual {v4}, Landroid/os/HwParcel;->releaseTemporaryStorage()V

    .line 226
    invoke-virtual {v1}, Landroid/os/HwParcel;->readBool()Z

    move-result v0
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 227
    .local v0, "flag":Z
    nop

    .line 234
    invoke-virtual {v1}, Landroid/os/HwParcel;->release()V

    .line 227
    return v0

    .line 229
    .end local v0    # "flag":Z
    .end local v4    # "hidl_request":Landroid/os/HwParcel;
    :cond_0
    :try_start_1
    sget-object v0, Lcom/miui/server/MiuiSwapService;->TAG:Ljava/lang/String;

    const-string v4, "hwService get failed, please try again"

    invoke-static {v0, v4}, Landroid/util/Slog;->d(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_0
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 234
    nop

    .end local v3    # "hwService":Landroid/os/IHwBinder;
    :goto_0
    invoke-virtual {v1}, Landroid/os/HwParcel;->release()V

    .line 235
    goto :goto_1

    .line 234
    :catchall_0
    move-exception v0

    goto :goto_2

    .line 231
    :catch_0
    move-exception v0

    .line 232
    .local v0, "e":Ljava/lang/Exception;
    :try_start_2
    sget-object v3, Lcom/miui/server/MiuiSwapService;->TAG:Ljava/lang/String;

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "fail to get SwapHalService"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Landroid/util/Slog;->e(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    .line 234
    nop

    .end local v0    # "e":Ljava/lang/Exception;
    goto :goto_0

    .line 236
    :goto_1
    return v2

    .line 234
    :goto_2
    invoke-virtual {v1}, Landroid/os/HwParcel;->release()V

    .line 235
    throw v0
.end method


# virtual methods
.method public callNativeSwap()Ljava/lang/String;
    .locals 4

    .line 141
    invoke-direct {p0}, Lcom/miui/server/MiuiSwapService;->getNativeService()Lmiui/swap/ISwap;

    .line 142
    const-string v0, ""

    .line 144
    .local v0, "getLba":Ljava/lang/String;
    :try_start_0
    iget-object v1, p0, Lcom/miui/server/MiuiSwapService;->mSwapNativeService:Lmiui/swap/ISwap;

    if-eqz v1, :cond_0

    iget-object v1, p0, Lcom/miui/server/MiuiSwapService;->mSwapNativeService:Lmiui/swap/ISwap;

    invoke-interface {v1}, Lmiui/swap/ISwap;->SWAP_isSupport()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 145
    iget-object v1, p0, Lcom/miui/server/MiuiSwapService;->mSwapNativeService:Lmiui/swap/ISwap;

    invoke-interface {v1}, Lmiui/swap/ISwap;->SWAP_trigger()Ljava/lang/String;

    move-result-object v1
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    :cond_0
    const/4 v1, 0x0

    :goto_0
    move-object v0, v1

    .line 148
    goto :goto_1

    .line 146
    :catch_0
    move-exception v1

    .line 147
    .local v1, "e":Ljava/lang/Exception;
    sget-object v2, Lcom/miui/server/MiuiSwapService;->TAG:Ljava/lang/String;

    const-string v3, "Failed to access native swap methods"

    invoke-static {v2, v3, v1}, Landroid/util/Slog;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    .line 149
    .end local v1    # "e":Ljava/lang/Exception;
    :goto_1
    return-object v0
.end method

.method public getUsbStatus()V
    .locals 3

    .line 263
    iget-object v0, p0, Lcom/miui/server/MiuiSwapService;->mContext:Landroid/content/Context;

    if-eqz v0, :cond_0

    .line 264
    new-instance v0, Landroid/content/IntentFilter;

    invoke-direct {v0}, Landroid/content/IntentFilter;-><init>()V

    .line 265
    .local v0, "filter":Landroid/content/IntentFilter;
    const-string v1, "android.hardware.usb.action.USB_STATE"

    invoke-virtual {v0, v1}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    .line 266
    iget-object v1, p0, Lcom/miui/server/MiuiSwapService;->mContext:Landroid/content/Context;

    new-instance v2, Lcom/miui/server/MiuiSwapService$UsbStatusReceiver;

    invoke-direct {v2, p0}, Lcom/miui/server/MiuiSwapService$UsbStatusReceiver;-><init>(Lcom/miui/server/MiuiSwapService;)V

    invoke-virtual {v1, v2, v0}, Landroid/content/Context;->registerReceiver(Landroid/content/BroadcastReceiver;Landroid/content/IntentFilter;)Landroid/content/Intent;

    .line 268
    .end local v0    # "filter":Landroid/content/IntentFilter;
    :cond_0
    return-void
.end method

.method public swapControl()V
    .locals 4

    .line 123
    const-string v0, "persist.sys.stability.swapEnable"

    const/4 v1, 0x0

    invoke-static {v0, v1}, Landroid/os/SystemProperties;->getBoolean(Ljava/lang/String;Z)Z

    move-result v0

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    .line 124
    .local v0, "swapEnable":Ljava/lang/Boolean;
    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v2

    if-eqz v2, :cond_0

    .line 125
    iget-boolean v2, p0, Lcom/miui/server/MiuiSwapService;->mUsbConnect:Z

    if-nez v2, :cond_1

    .line 126
    invoke-direct {p0}, Lcom/miui/server/MiuiSwapService;->startSwapLocked()V

    goto :goto_0

    .line 129
    :cond_0
    invoke-direct {p0}, Lcom/miui/server/MiuiSwapService;->swapHalDisable()Z

    .line 130
    sget-object v2, Lcom/miui/server/MiuiSwapService;->TAG:Ljava/lang/String;

    const-string v3, "The cloud control version does not support this function"

    invoke-static {v2, v3}, Landroid/util/Slog;->w(Ljava/lang/String;Ljava/lang/String;)I

    .line 132
    :cond_1
    :goto_0
    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v2

    invoke-static {v2}, Ljava/lang/Boolean;->toString(Z)Ljava/lang/String;

    move-result-object v2

    const-string v3, "persist.sys.stability.preswapEnable"

    invoke-static {v3, v2}, Landroid/os/SystemProperties;->set(Ljava/lang/String;Ljava/lang/String;)V

    .line 133
    const-string v2, "persist.sys.swapservice.ctrl"

    invoke-static {v1}, Ljava/lang/Boolean;->toString(Z)Ljava/lang/String;

    move-result-object v1

    invoke-static {v2, v1}, Landroid/os/SystemProperties;->set(Ljava/lang/String;Ljava/lang/String;)V

    .line 134
    return-void
.end method
