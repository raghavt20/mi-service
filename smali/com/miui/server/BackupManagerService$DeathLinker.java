class com.miui.server.BackupManagerService$DeathLinker implements android.os.IBinder$DeathRecipient {
	 /* .source "BackupManagerService.java" */
	 /* # interfaces */
	 /* # annotations */
	 /* .annotation system Ldalvik/annotation/EnclosingClass; */
	 /* value = Lcom/miui/server/BackupManagerService; */
} // .end annotation
/* .annotation system Ldalvik/annotation/InnerClass; */
/* accessFlags = 0x2 */
/* name = "DeathLinker" */
} // .end annotation
/* # instance fields */
final com.miui.server.BackupManagerService this$0; //synthetic
/* # direct methods */
private com.miui.server.BackupManagerService$DeathLinker ( ) {
/* .locals 0 */
/* .line 886 */
this.this$0 = p1;
/* invoke-direct {p0}, Ljava/lang/Object;-><init>()V */
return;
} // .end method
 com.miui.server.BackupManagerService$DeathLinker ( ) { //synthethic
/* .locals 0 */
/* invoke-direct {p0, p1}, Lcom/miui/server/BackupManagerService$DeathLinker;-><init>(Lcom/miui/server/BackupManagerService;)V */
return;
} // .end method
/* # virtual methods */
public void binderDied ( ) {
/* .locals 4 */
/* .line 889 */
final String v0 = "Client binder has died.Start canceling..."; // const-string v0, "Client binder has died.Start canceling..."
final String v1 = "Backup:BackupManagerService"; // const-string v1, "Backup:BackupManagerService"
android.util.Slog .d ( v1,v0 );
/* .line 890 */
v0 = this.this$0;
int v2 = 1; // const/4 v2, 0x1
com.miui.server.BackupManagerService .-$$Nest$fputmIsCanceling ( v0,v2 );
/* .line 891 */
v0 = this.this$0;
com.miui.server.BackupManagerService .-$$Nest$mscheduleReleaseResource ( v0 );
/* .line 892 */
v0 = this.this$0;
com.miui.server.BackupManagerService .-$$Nest$mwaitForTheLastWorkingTask ( v0 );
/* .line 893 */
v0 = this.this$0;
int v2 = 0; // const/4 v2, 0x0
com.miui.server.BackupManagerService .-$$Nest$fputmIsCanceling ( v0,v2 );
/* .line 895 */
v0 = this.this$0;
int v3 = -1; // const/4 v3, -0x1
com.miui.server.BackupManagerService .-$$Nest$fputmOwnerPid ( v0,v3 );
/* .line 896 */
v0 = this.this$0;
com.miui.server.BackupManagerService .-$$Nest$fgetmICaller ( v0 );
v3 = this.this$0;
com.miui.server.BackupManagerService .-$$Nest$fgetmDeathLinker ( v3 );
/* .line 897 */
v0 = this.this$0;
int v2 = 0; // const/4 v2, 0x0
com.miui.server.BackupManagerService .-$$Nest$fputmICaller ( v0,v2 );
/* .line 898 */
v0 = this.this$0;
com.miui.server.BackupManagerService .-$$Nest$fputmPreviousWorkingPkg ( v0,v2 );
/* .line 900 */
try { // :try_start_0
	 v0 = this.this$0;
	 com.miui.server.BackupManagerService .-$$Nest$mbroadcastServiceIdle ( v0 );
	 /* :try_end_0 */
	 /* .catch Landroid/os/RemoteException; {:try_start_0 ..:try_end_0} :catch_0 */
	 /* .line 903 */
	 /* .line 901 */
	 /* :catch_0 */
	 /* move-exception v0 */
	 /* .line 902 */
	 /* .local v0, "e":Landroid/os/RemoteException; */
	 (( android.os.RemoteException ) v0 ).printStackTrace ( ); // invoke-virtual {v0}, Landroid/os/RemoteException;->printStackTrace()V
	 /* .line 904 */
} // .end local v0 # "e":Landroid/os/RemoteException;
} // :goto_0
final String v0 = "Client binder has died.Cancel completed!"; // const-string v0, "Client binder has died.Cancel completed!"
android.util.Slog .d ( v1,v0 );
/* .line 905 */
return;
} // .end method
