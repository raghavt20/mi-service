class com.miui.server.ISplashScreenService$Stub$Proxy implements com.miui.server.ISplashScreenService {
	 /* .source "ISplashScreenService.java" */
	 /* # interfaces */
	 /* # annotations */
	 /* .annotation system Ldalvik/annotation/EnclosingClass; */
	 /* value = Lcom/miui/server/ISplashScreenService$Stub; */
} // .end annotation
/* .annotation system Ldalvik/annotation/InnerClass; */
/* accessFlags = 0xa */
/* name = "Proxy" */
} // .end annotation
/* # instance fields */
private android.os.IBinder mRemote;
/* # direct methods */
 com.miui.server.ISplashScreenService$Stub$Proxy ( ) {
/* .locals 0 */
/* .param p1, "remote" # Landroid/os/IBinder; */
/* .line 185 */
/* invoke-direct {p0}, Ljava/lang/Object;-><init>()V */
/* .line 186 */
this.mRemote = p1;
/* .line 187 */
return;
} // .end method
/* # virtual methods */
public void activityIdle ( android.content.pm.ActivityInfo p0 ) {
/* .locals 5 */
/* .param p1, "aInfo" # Landroid/content/pm/ActivityInfo; */
/* .annotation system Ldalvik/annotation/Throws; */
/* value = { */
/* Landroid/os/RemoteException; */
/* } */
} // .end annotation
/* .line 221 */
android.os.Parcel .obtain ( );
/* .line 222 */
/* .local v0, "_data":Landroid/os/Parcel; */
final String v1 = "com.miui.server.ISplashScreenService"; // const-string v1, "com.miui.server.ISplashScreenService"
(( android.os.Parcel ) v0 ).writeInterfaceToken ( v1 ); // invoke-virtual {v0, v1}, Landroid/os/Parcel;->writeInterfaceToken(Ljava/lang/String;)V
/* .line 223 */
int v1 = 0; // const/4 v1, 0x0
(( android.content.pm.ActivityInfo ) p1 ).writeToParcel ( v0, v1 ); // invoke-virtual {p1, v0, v1}, Landroid/content/pm/ActivityInfo;->writeToParcel(Landroid/os/Parcel;I)V
/* .line 224 */
android.os.Parcel .obtain ( );
/* .line 226 */
/* .local v2, "_reply":Landroid/os/Parcel; */
try { // :try_start_0
v3 = this.mRemote;
int v4 = 2; // const/4 v4, 0x2
/* .line 227 */
(( android.os.Parcel ) v2 ).readException ( ); // invoke-virtual {v2}, Landroid/os/Parcel;->readException()V
/* :try_end_0 */
/* .catchall {:try_start_0 ..:try_end_0} :catchall_0 */
/* .line 229 */
(( android.os.Parcel ) v2 ).recycle ( ); // invoke-virtual {v2}, Landroid/os/Parcel;->recycle()V
/* .line 230 */
(( android.os.Parcel ) v0 ).recycle ( ); // invoke-virtual {v0}, Landroid/os/Parcel;->recycle()V
/* .line 231 */
/* nop */
/* .line 232 */
return;
/* .line 229 */
/* :catchall_0 */
/* move-exception v1 */
(( android.os.Parcel ) v2 ).recycle ( ); // invoke-virtual {v2}, Landroid/os/Parcel;->recycle()V
/* .line 230 */
(( android.os.Parcel ) v0 ).recycle ( ); // invoke-virtual {v0}, Landroid/os/Parcel;->recycle()V
/* .line 231 */
/* throw v1 */
} // .end method
public android.os.IBinder asBinder ( ) {
/* .locals 1 */
/* .line 191 */
v0 = this.mRemote;
} // .end method
public void destroyActivity ( android.content.pm.ActivityInfo p0 ) {
/* .locals 5 */
/* .param p1, "aInfo" # Landroid/content/pm/ActivityInfo; */
/* .annotation system Ldalvik/annotation/Throws; */
/* value = { */
/* Landroid/os/RemoteException; */
/* } */
} // .end annotation
/* .line 236 */
android.os.Parcel .obtain ( );
/* .line 237 */
/* .local v0, "_data":Landroid/os/Parcel; */
final String v1 = "com.miui.server.ISplashScreenService"; // const-string v1, "com.miui.server.ISplashScreenService"
(( android.os.Parcel ) v0 ).writeInterfaceToken ( v1 ); // invoke-virtual {v0, v1}, Landroid/os/Parcel;->writeInterfaceToken(Ljava/lang/String;)V
/* .line 238 */
int v1 = 0; // const/4 v1, 0x0
(( android.content.pm.ActivityInfo ) p1 ).writeToParcel ( v0, v1 ); // invoke-virtual {p1, v0, v1}, Landroid/content/pm/ActivityInfo;->writeToParcel(Landroid/os/Parcel;I)V
/* .line 240 */
try { // :try_start_0
v1 = this.mRemote;
int v2 = 0; // const/4 v2, 0x0
int v3 = 1; // const/4 v3, 0x1
int v4 = 3; // const/4 v4, 0x3
/* :try_end_0 */
/* .catchall {:try_start_0 ..:try_end_0} :catchall_0 */
/* .line 242 */
(( android.os.Parcel ) v0 ).recycle ( ); // invoke-virtual {v0}, Landroid/os/Parcel;->recycle()V
/* .line 243 */
/* nop */
/* .line 244 */
return;
/* .line 242 */
/* :catchall_0 */
/* move-exception v1 */
(( android.os.Parcel ) v0 ).recycle ( ); // invoke-virtual {v0}, Landroid/os/Parcel;->recycle()V
/* .line 243 */
/* throw v1 */
} // .end method
public android.content.Intent requestSplashScreen ( android.content.Intent p0, android.content.pm.ActivityInfo p1 ) {
/* .locals 5 */
/* .param p1, "intent" # Landroid/content/Intent; */
/* .param p2, "aInfo" # Landroid/content/pm/ActivityInfo; */
/* .annotation system Ldalvik/annotation/Throws; */
/* value = { */
/* Landroid/os/RemoteException; */
/* } */
} // .end annotation
/* .line 198 */
android.os.Parcel .obtain ( );
/* .line 199 */
/* .local v0, "_data":Landroid/os/Parcel; */
final String v1 = "com.miui.server.ISplashScreenService"; // const-string v1, "com.miui.server.ISplashScreenService"
(( android.os.Parcel ) v0 ).writeInterfaceToken ( v1 ); // invoke-virtual {v0, v1}, Landroid/os/Parcel;->writeInterfaceToken(Ljava/lang/String;)V
/* .line 200 */
int v1 = 0; // const/4 v1, 0x0
(( android.content.Intent ) p1 ).writeToParcel ( v0, v1 ); // invoke-virtual {p1, v0, v1}, Landroid/content/Intent;->writeToParcel(Landroid/os/Parcel;I)V
/* .line 201 */
(( android.content.pm.ActivityInfo ) p2 ).writeToParcel ( v0, v1 ); // invoke-virtual {p2, v0, v1}, Landroid/content/pm/ActivityInfo;->writeToParcel(Landroid/os/Parcel;I)V
/* .line 202 */
android.os.Parcel .obtain ( );
/* .line 205 */
/* .local v2, "_reply":Landroid/os/Parcel; */
try { // :try_start_0
v3 = this.mRemote;
int v4 = 1; // const/4 v4, 0x1
/* .line 206 */
(( android.os.Parcel ) v2 ).readException ( ); // invoke-virtual {v2}, Landroid/os/Parcel;->readException()V
/* .line 207 */
v1 = (( android.os.Parcel ) v2 ).readInt ( ); // invoke-virtual {v2}, Landroid/os/Parcel;->readInt()I
if ( v1 != null) { // if-eqz v1, :cond_0
	 /* .line 208 */
	 v1 = android.content.Intent.CREATOR;
	 /* check-cast v1, Landroid/content/Intent; */
	 /* :try_end_0 */
	 /* .catchall {:try_start_0 ..:try_end_0} :catchall_0 */
	 /* .local v1, "_result":Landroid/content/Intent; */
	 /* .line 210 */
} // .end local v1 # "_result":Landroid/content/Intent;
} // :cond_0
int v1 = 0; // const/4 v1, 0x0
/* .line 213 */
/* .restart local v1 # "_result":Landroid/content/Intent; */
} // :goto_0
(( android.os.Parcel ) v2 ).recycle ( ); // invoke-virtual {v2}, Landroid/os/Parcel;->recycle()V
/* .line 214 */
(( android.os.Parcel ) v0 ).recycle ( ); // invoke-virtual {v0}, Landroid/os/Parcel;->recycle()V
/* .line 215 */
/* nop */
/* .line 216 */
/* .line 213 */
} // .end local v1 # "_result":Landroid/content/Intent;
/* :catchall_0 */
/* move-exception v1 */
(( android.os.Parcel ) v2 ).recycle ( ); // invoke-virtual {v2}, Landroid/os/Parcel;->recycle()V
/* .line 214 */
(( android.os.Parcel ) v0 ).recycle ( ); // invoke-virtual {v0}, Landroid/os/Parcel;->recycle()V
/* .line 215 */
/* throw v1 */
} // .end method
public void setSplashPackageListener ( com.miui.server.ISplashPackageCheckListener p0 ) {
/* .locals 5 */
/* .param p1, "listener" # Lcom/miui/server/ISplashPackageCheckListener; */
/* .annotation system Ldalvik/annotation/Throws; */
/* value = { */
/* Landroid/os/RemoteException; */
/* } */
} // .end annotation
/* .line 248 */
android.os.Parcel .obtain ( );
/* .line 250 */
/* .local v0, "_data":Landroid/os/Parcel; */
try { // :try_start_0
final String v1 = "com.miui.server.ISplashScreenService"; // const-string v1, "com.miui.server.ISplashScreenService"
(( android.os.Parcel ) v0 ).writeInterfaceToken ( v1 ); // invoke-virtual {v0, v1}, Landroid/os/Parcel;->writeInterfaceToken(Ljava/lang/String;)V
/* .line 251 */
int v1 = 0; // const/4 v1, 0x0
if ( p1 != null) { // if-eqz p1, :cond_0
} // :cond_0
/* move-object v2, v1 */
} // :goto_0
(( android.os.Parcel ) v0 ).writeStrongBinder ( v2 ); // invoke-virtual {v0, v2}, Landroid/os/Parcel;->writeStrongBinder(Landroid/os/IBinder;)V
/* .line 252 */
v2 = this.mRemote;
int v3 = 4; // const/4 v3, 0x4
int v4 = 1; // const/4 v4, 0x1
/* :try_end_0 */
/* .catchall {:try_start_0 ..:try_end_0} :catchall_0 */
/* .line 254 */
(( android.os.Parcel ) v0 ).recycle ( ); // invoke-virtual {v0}, Landroid/os/Parcel;->recycle()V
/* .line 255 */
/* nop */
/* .line 256 */
return;
/* .line 254 */
/* :catchall_0 */
/* move-exception v1 */
(( android.os.Parcel ) v0 ).recycle ( ); // invoke-virtual {v0}, Landroid/os/Parcel;->recycle()V
/* .line 255 */
/* throw v1 */
} // .end method
