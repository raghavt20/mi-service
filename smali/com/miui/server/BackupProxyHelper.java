public class com.miui.server.BackupProxyHelper {
	 /* .source "BackupProxyHelper.java" */
	 /* # direct methods */
	 public com.miui.server.BackupProxyHelper ( ) {
		 /* .locals 0 */
		 /* .line 18 */
		 /* invoke-direct {p0}, Ljava/lang/Object;-><init>()V */
		 return;
	 } // .end method
	 public static Boolean isAppInXSpace ( android.content.pm.IPackageManager p0, java.lang.String p1 ) {
		 /* .locals 4 */
		 /* .param p0, "pm" # Landroid/content/pm/IPackageManager; */
		 /* .param p1, "pkgName" # Ljava/lang/String; */
		 /* .line 66 */
		 /* const-wide/16 v0, 0x0 */
		 /* const/16 v2, 0x3e7 */
		 int v3 = 0; // const/4 v3, 0x0
		 try { // :try_start_0
			 /* :try_end_0 */
			 /* .catch Landroid/os/RemoteException; {:try_start_0 ..:try_end_0} :catch_0 */
			 /* .line 67 */
			 /* .local v0, "pkgInfo":Landroid/content/pm/PackageInfo; */
			 if ( v0 != null) { // if-eqz v0, :cond_0
				 int v3 = 1; // const/4 v3, 0x1
			 } // :cond_0
			 /* .line 68 */
		 } // .end local v0 # "pkgInfo":Landroid/content/pm/PackageInfo;
		 /* :catch_0 */
		 /* move-exception v0 */
		 /* .line 71 */
	 } // .end method
	 public static Boolean isMiPushRequired ( android.content.pm.IPackageManager p0, java.lang.String p1 ) {
		 /* .locals 9 */
		 /* .param p0, "pm" # Landroid/content/pm/IPackageManager; */
		 /* .param p1, "pkgName" # Ljava/lang/String; */
		 /* .line 22 */
		 /* const-wide/16 v0, 0x1004 */
		 int v2 = 0; // const/4 v2, 0x0
		 try { // :try_start_0
			 /* .line 24 */
			 /* .local v0, "pkgInfo":Landroid/content/pm/PackageInfo; */
			 /* if-nez v0, :cond_0 */
			 /* .line 25 */
			 /* .line 28 */
		 } // :cond_0
		 v1 = this.requestedPermissions;
		 /* .line 29 */
		 /* .local v1, "permissions":[Ljava/lang/String; */
		 /* if-nez v1, :cond_1 */
		 /* .line 30 */
		 /* .line 32 */
	 } // :cond_1
	 /* new-instance v3, Ljava/lang/StringBuilder; */
	 /* invoke-direct {v3, p1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V */
	 final String v4 = ".permission.MIPUSH_RECEIVE"; // const-string v4, ".permission.MIPUSH_RECEIVE"
	 (( java.lang.StringBuilder ) v3 ).append ( v4 ); // invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
	 (( java.lang.StringBuilder ) v3 ).toString ( ); // invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
	 /* .line 33 */
	 /* .local v3, "needPermission":Ljava/lang/String; */
	 int v4 = 0; // const/4 v4, 0x0
	 /* .line 34 */
	 /* .local v4, "permissionIndex":I */
} // :goto_0
/* array-length v5, v1 */
/* if-ge v4, v5, :cond_3 */
/* .line 35 */
/* aget-object v5, v1, v4 */
v5 = android.text.TextUtils .equals ( v3,v5 );
if ( v5 != null) { // if-eqz v5, :cond_2
	 /* .line 36 */
	 /* .line 34 */
} // :cond_2
/* add-int/lit8 v4, v4, 0x1 */
/* .line 39 */
} // :cond_3
} // :goto_1
/* array-length v5, v1 */
/* if-lt v4, v5, :cond_4 */
/* .line 40 */
/* .line 43 */
} // :cond_4
v5 = this.services;
/* .line 44 */
/* .local v5, "services":[Landroid/content/pm/ServiceInfo; */
/* if-nez v5, :cond_5 */
/* .line 45 */
/* .line 47 */
} // :cond_5
final String v6 = "com.xiaomi.mipush.sdk.PushMessageHandler"; // const-string v6, "com.xiaomi.mipush.sdk.PushMessageHandler"
/* .line 48 */
/* .local v6, "needServiceName":Ljava/lang/String; */
int v7 = 0; // const/4 v7, 0x0
/* .line 49 */
/* .local v7, "serviceIndex":I */
} // :goto_2
/* array-length v8, v5 */
/* if-ge v7, v8, :cond_7 */
/* .line 50 */
/* aget-object v8, v5, v7 */
v8 = this.name;
v8 = android.text.TextUtils .equals ( v8,v6 );
if ( v8 != null) { // if-eqz v8, :cond_6
/* .line 51 */
/* .line 49 */
} // :cond_6
/* add-int/lit8 v7, v7, 0x1 */
/* .line 54 */
} // :cond_7
} // :goto_3
/* array-length v8, v5 */
/* :try_end_0 */
/* .catch Ljava/lang/Exception; {:try_start_0 ..:try_end_0} :catch_0 */
/* if-lt v7, v8, :cond_8 */
/* .line 55 */
/* .line 57 */
} // :cond_8
int v2 = 1; // const/4 v2, 0x1
/* .line 58 */
} // .end local v0 # "pkgInfo":Landroid/content/pm/PackageInfo;
} // .end local v1 # "permissions":[Ljava/lang/String;
} // .end local v3 # "needPermission":Ljava/lang/String;
} // .end local v4 # "permissionIndex":I
} // .end local v5 # "services":[Landroid/content/pm/ServiceInfo;
} // .end local v6 # "needServiceName":Ljava/lang/String;
} // .end local v7 # "serviceIndex":I
/* :catch_0 */
/* move-exception v0 */
/* .line 61 */
} // .end method
