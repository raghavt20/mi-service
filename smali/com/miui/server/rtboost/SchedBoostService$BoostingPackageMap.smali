.class Lcom/miui/server/rtboost/SchedBoostService$BoostingPackageMap;
.super Ljava/lang/Object;
.source "SchedBoostService.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/miui/server/rtboost/SchedBoostService;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "BoostingPackageMap"
.end annotation


# instance fields
.field private final infoList:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList<",
            "Lcom/miui/server/rtboost/SchedBoostService$BoostThreadInfo;",
            ">;"
        }
    .end annotation
.end field

.field private final mMap:Ljava/util/HashMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/HashMap<",
            "Ljava/lang/Integer;",
            "Lcom/miui/server/rtboost/SchedBoostService$BoostThreadInfo;",
            ">;"
        }
    .end annotation
.end field

.field private final mMapLock:Ljava/lang/Object;

.field final synthetic this$0:Lcom/miui/server/rtboost/SchedBoostService;


# direct methods
.method static bridge synthetic -$$Nest$fgetinfoList(Lcom/miui/server/rtboost/SchedBoostService$BoostingPackageMap;)Ljava/util/ArrayList;
    .locals 0

    iget-object p0, p0, Lcom/miui/server/rtboost/SchedBoostService$BoostingPackageMap;->infoList:Ljava/util/ArrayList;

    return-object p0
.end method

.method static bridge synthetic -$$Nest$fgetmMapLock(Lcom/miui/server/rtboost/SchedBoostService$BoostingPackageMap;)Ljava/lang/Object;
    .locals 0

    iget-object p0, p0, Lcom/miui/server/rtboost/SchedBoostService$BoostingPackageMap;->mMapLock:Ljava/lang/Object;

    return-object p0
.end method

.method private constructor <init>(Lcom/miui/server/rtboost/SchedBoostService;)V
    .locals 0

    .line 984
    iput-object p1, p0, Lcom/miui/server/rtboost/SchedBoostService$BoostingPackageMap;->this$0:Lcom/miui/server/rtboost/SchedBoostService;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 985
    new-instance p1, Ljava/util/HashMap;

    invoke-direct {p1}, Ljava/util/HashMap;-><init>()V

    iput-object p1, p0, Lcom/miui/server/rtboost/SchedBoostService$BoostingPackageMap;->mMap:Ljava/util/HashMap;

    .line 986
    new-instance p1, Ljava/util/ArrayList;

    invoke-direct {p1}, Ljava/util/ArrayList;-><init>()V

    iput-object p1, p0, Lcom/miui/server/rtboost/SchedBoostService$BoostingPackageMap;->infoList:Ljava/util/ArrayList;

    .line 987
    new-instance p1, Ljava/lang/Object;

    invoke-direct {p1}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lcom/miui/server/rtboost/SchedBoostService$BoostingPackageMap;->mMapLock:Ljava/lang/Object;

    return-void
.end method

.method synthetic constructor <init>(Lcom/miui/server/rtboost/SchedBoostService;Lcom/miui/server/rtboost/SchedBoostService$BoostingPackageMap-IA;)V
    .locals 0

    invoke-direct {p0, p1}, Lcom/miui/server/rtboost/SchedBoostService$BoostingPackageMap;-><init>(Lcom/miui/server/rtboost/SchedBoostService;)V

    return-void
.end method


# virtual methods
.method public clear()V
    .locals 2

    .line 1019
    iget-object v0, p0, Lcom/miui/server/rtboost/SchedBoostService$BoostingPackageMap;->mMapLock:Ljava/lang/Object;

    monitor-enter v0

    .line 1020
    :try_start_0
    iget-object v1, p0, Lcom/miui/server/rtboost/SchedBoostService$BoostingPackageMap;->mMap:Ljava/util/HashMap;

    invoke-virtual {v1}, Ljava/util/HashMap;->clear()V

    .line 1021
    iget-object v1, p0, Lcom/miui/server/rtboost/SchedBoostService$BoostingPackageMap;->infoList:Ljava/util/ArrayList;

    invoke-virtual {v1}, Ljava/util/ArrayList;->clear()V

    .line 1022
    monitor-exit v0

    .line 1023
    return-void

    .line 1022
    :catchall_0
    move-exception v1

    monitor-exit v0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v1
.end method

.method public getByTid(I)Lcom/miui/server/rtboost/SchedBoostService$BoostThreadInfo;
    .locals 3
    .param p1, "tid"    # I

    .line 1026
    iget-object v0, p0, Lcom/miui/server/rtboost/SchedBoostService$BoostingPackageMap;->mMapLock:Ljava/lang/Object;

    monitor-enter v0

    .line 1027
    :try_start_0
    iget-object v1, p0, Lcom/miui/server/rtboost/SchedBoostService$BoostingPackageMap;->mMap:Ljava/util/HashMap;

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/miui/server/rtboost/SchedBoostService$BoostThreadInfo;

    monitor-exit v0

    return-object v1

    .line 1028
    :catchall_0
    move-exception v1

    monitor-exit v0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v1
.end method

.method public getList()Ljava/util/ArrayList;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/ArrayList<",
            "Lcom/miui/server/rtboost/SchedBoostService$BoostThreadInfo;",
            ">;"
        }
    .end annotation

    .line 990
    iget-object v0, p0, Lcom/miui/server/rtboost/SchedBoostService$BoostingPackageMap;->infoList:Ljava/util/ArrayList;

    return-object v0
.end method

.method public isEmpty()Z
    .locals 2

    .line 1032
    iget-object v0, p0, Lcom/miui/server/rtboost/SchedBoostService$BoostingPackageMap;->mMapLock:Ljava/lang/Object;

    monitor-enter v0

    .line 1033
    :try_start_0
    iget-object v1, p0, Lcom/miui/server/rtboost/SchedBoostService$BoostingPackageMap;->infoList:Ljava/util/ArrayList;

    invoke-virtual {v1}, Ljava/util/ArrayList;->size()I

    move-result v1

    if-nez v1, :cond_0

    iget-object v1, p0, Lcom/miui/server/rtboost/SchedBoostService$BoostingPackageMap;->mMap:Ljava/util/HashMap;

    invoke-virtual {v1}, Ljava/util/HashMap;->isEmpty()Z

    move-result v1

    if-eqz v1, :cond_0

    const/4 v1, 0x1

    goto :goto_0

    :cond_0
    const/4 v1, 0x0

    :goto_0
    monitor-exit v0

    return v1

    .line 1034
    :catchall_0
    move-exception v1

    monitor-exit v0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v1
.end method

.method public put(Lcom/miui/server/rtboost/SchedBoostService$BoostThreadInfo;)V
    .locals 4
    .param p1, "threadInfo"    # Lcom/miui/server/rtboost/SchedBoostService$BoostThreadInfo;

    .line 994
    iget-object v0, p0, Lcom/miui/server/rtboost/SchedBoostService$BoostingPackageMap;->mMapLock:Ljava/lang/Object;

    monitor-enter v0

    .line 995
    :try_start_0
    iget-object v1, p0, Lcom/miui/server/rtboost/SchedBoostService$BoostingPackageMap;->infoList:Ljava/util/ArrayList;

    invoke-virtual {v1, p1}, Ljava/util/ArrayList;->contains(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 997
    iget-object v1, p0, Lcom/miui/server/rtboost/SchedBoostService$BoostingPackageMap;->mMap:Ljava/util/HashMap;

    iget v2, p1, Lcom/miui/server/rtboost/SchedBoostService$BoostThreadInfo;->tid:I

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/miui/server/rtboost/SchedBoostService$BoostThreadInfo;

    .line 998
    .local v1, "oldThreadInfo":Lcom/miui/server/rtboost/SchedBoostService$BoostThreadInfo;
    iget-wide v2, v1, Lcom/miui/server/rtboost/SchedBoostService$BoostThreadInfo;->boostStartTime:J

    iput-wide v2, p1, Lcom/miui/server/rtboost/SchedBoostService$BoostThreadInfo;->boostStartTime:J

    .line 999
    iget v2, v1, Lcom/miui/server/rtboost/SchedBoostService$BoostThreadInfo;->savedPriority:I

    iput v2, p1, Lcom/miui/server/rtboost/SchedBoostService$BoostThreadInfo;->savedPriority:I

    .line 1000
    iget v2, v1, Lcom/miui/server/rtboost/SchedBoostService$BoostThreadInfo;->boostPriority:I

    iput v2, p1, Lcom/miui/server/rtboost/SchedBoostService$BoostThreadInfo;->boostPriority:I

    .line 1001
    iget v2, v1, Lcom/miui/server/rtboost/SchedBoostService$BoostThreadInfo;->tid:I

    invoke-virtual {p0, v2}, Lcom/miui/server/rtboost/SchedBoostService$BoostingPackageMap;->removeByTid(I)V

    .line 1004
    .end local v1    # "oldThreadInfo":Lcom/miui/server/rtboost/SchedBoostService$BoostThreadInfo;
    :cond_0
    iget-object v1, p0, Lcom/miui/server/rtboost/SchedBoostService$BoostingPackageMap;->infoList:Ljava/util/ArrayList;

    invoke-virtual {v1, p1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 1005
    iget-object v1, p0, Lcom/miui/server/rtboost/SchedBoostService$BoostingPackageMap;->mMap:Ljava/util/HashMap;

    iget v2, p1, Lcom/miui/server/rtboost/SchedBoostService$BoostThreadInfo;->tid:I

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v1, v2, p1}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 1006
    monitor-exit v0

    .line 1007
    return-void

    .line 1006
    :catchall_0
    move-exception v1

    monitor-exit v0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v1
.end method

.method public removeByTid(I)V
    .locals 3
    .param p1, "tid"    # I

    .line 1010
    iget-object v0, p0, Lcom/miui/server/rtboost/SchedBoostService$BoostingPackageMap;->mMapLock:Ljava/lang/Object;

    monitor-enter v0

    .line 1011
    :try_start_0
    iget-object v1, p0, Lcom/miui/server/rtboost/SchedBoostService$BoostingPackageMap;->mMap:Ljava/util/HashMap;

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/util/HashMap;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/miui/server/rtboost/SchedBoostService$BoostThreadInfo;

    .line 1012
    .local v1, "threadInfo":Lcom/miui/server/rtboost/SchedBoostService$BoostThreadInfo;
    if-eqz v1, :cond_0

    .line 1013
    iget-object v2, p0, Lcom/miui/server/rtboost/SchedBoostService$BoostingPackageMap;->infoList:Ljava/util/ArrayList;

    invoke-virtual {v2, v1}, Ljava/util/ArrayList;->remove(Ljava/lang/Object;)Z

    .line 1015
    .end local v1    # "threadInfo":Lcom/miui/server/rtboost/SchedBoostService$BoostThreadInfo;
    :cond_0
    monitor-exit v0

    .line 1016
    return-void

    .line 1015
    :catchall_0
    move-exception v1

    monitor-exit v0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v1
.end method
