public abstract class com.miui.server.rtboost.SchedBoostManagerInternal {
	 /* .source "SchedBoostManagerInternal.java" */
	 /* # virtual methods */
	 public abstract void beginSchedThreads ( Integer[] p0, Long p1, java.lang.String p2, Integer p3 ) {
	 } // .end method
	 public abstract void boostHomeAnim ( Long p0, Integer p1 ) {
	 } // .end method
	 public abstract Boolean checkThreadBoost ( Integer p0 ) {
	 } // .end method
	 public abstract void enableSchedBoost ( Boolean p0 ) {
	 } // .end method
	 public abstract void schedProcessBoost ( com.android.server.wm.WindowProcessListener p0, java.lang.String p1, Integer p2, Integer p3, Integer p4, Long p5 ) {
	 } // .end method
	 public abstract void setRenderThreadTid ( com.android.server.wm.WindowProcessController p0 ) {
	 } // .end method
	 public abstract void setThreadSavedPriority ( Integer[] p0, Integer p1 ) {
	 } // .end method
	 public abstract void stopCurrentSchedBoost ( ) {
	 } // .end method
