class com.miui.server.rtboost.SchedBoostService$SchedBoostShellCommand extends android.os.ShellCommand {
	 /* .source "SchedBoostService.java" */
	 /* # annotations */
	 /* .annotation system Ldalvik/annotation/EnclosingClass; */
	 /* value = Lcom/miui/server/rtboost/SchedBoostService; */
} // .end annotation
/* .annotation system Ldalvik/annotation/InnerClass; */
/* accessFlags = 0x8 */
/* name = "SchedBoostShellCommand" */
} // .end annotation
/* # instance fields */
com.miui.server.rtboost.SchedBoostService mService;
/* # direct methods */
 com.miui.server.rtboost.SchedBoostService$SchedBoostShellCommand ( ) {
/* .locals 0 */
/* .param p1, "service" # Lcom/miui/server/rtboost/SchedBoostService; */
/* .line 1321 */
/* invoke-direct {p0}, Landroid/os/ShellCommand;-><init>()V */
/* .line 1322 */
this.mService = p1;
/* .line 1323 */
return;
} // .end method
/* # virtual methods */
public Integer onCommand ( java.lang.String p0 ) {
/* .locals 4 */
/* .param p1, "cmd" # Ljava/lang/String; */
/* .line 1327 */
(( com.miui.server.rtboost.SchedBoostService$SchedBoostShellCommand ) p0 ).getOutFileDescriptor ( ); // invoke-virtual {p0}, Lcom/miui/server/rtboost/SchedBoostService$SchedBoostShellCommand;->getOutFileDescriptor()Ljava/io/FileDescriptor;
/* .line 1328 */
/* .local v0, "fd":Ljava/io/FileDescriptor; */
(( com.miui.server.rtboost.SchedBoostService$SchedBoostShellCommand ) p0 ).getOutPrintWriter ( ); // invoke-virtual {p0}, Lcom/miui/server/rtboost/SchedBoostService$SchedBoostShellCommand;->getOutPrintWriter()Ljava/io/PrintWriter;
/* .line 1329 */
/* .local v1, "pw":Ljava/io/PrintWriter; */
(( com.miui.server.rtboost.SchedBoostService$SchedBoostShellCommand ) p0 ).getAllArgs ( ); // invoke-virtual {p0}, Lcom/miui/server/rtboost/SchedBoostService$SchedBoostShellCommand;->getAllArgs()[Ljava/lang/String;
/* .line 1330 */
/* .local v2, "args":[Ljava/lang/String; */
/* if-nez p1, :cond_0 */
/* .line 1331 */
v3 = (( com.miui.server.rtboost.SchedBoostService$SchedBoostShellCommand ) p0 ).handleDefaultCommands ( p1 ); // invoke-virtual {p0, p1}, Lcom/miui/server/rtboost/SchedBoostService$SchedBoostShellCommand;->handleDefaultCommands(Ljava/lang/String;)I
/* .line 1334 */
} // :cond_0
try { // :try_start_0
v3 = this.mService;
(( com.miui.server.rtboost.SchedBoostService ) v3 ).dump ( v0, v1, v2 ); // invoke-virtual {v3, v0, v1, v2}, Lcom/miui/server/rtboost/SchedBoostService;->dump(Ljava/io/FileDescriptor;Ljava/io/PrintWriter;[Ljava/lang/String;)V
/* :try_end_0 */
/* .catch Ljava/lang/Exception; {:try_start_0 ..:try_end_0} :catch_0 */
/* .line 1337 */
/* .line 1335 */
/* :catch_0 */
/* move-exception v3 */
/* .line 1336 */
/* .local v3, "e":Ljava/lang/Exception; */
(( java.io.PrintWriter ) v1 ).println ( v3 ); // invoke-virtual {v1, v3}, Ljava/io/PrintWriter;->println(Ljava/lang/Object;)V
/* .line 1338 */
} // .end local v3 # "e":Ljava/lang/Exception;
} // :goto_0
int v3 = -1; // const/4 v3, -0x1
} // .end method
public void onHelp ( ) {
/* .locals 2 */
/* .line 1343 */
(( com.miui.server.rtboost.SchedBoostService$SchedBoostShellCommand ) p0 ).getOutPrintWriter ( ); // invoke-virtual {p0}, Lcom/miui/server/rtboost/SchedBoostService$SchedBoostShellCommand;->getOutPrintWriter()Ljava/io/PrintWriter;
/* .line 1344 */
/* .local v0, "pw":Ljava/io/PrintWriter; */
final String v1 = "sched boost (SchedBoostService) commands:"; // const-string v1, "sched boost (SchedBoostService) commands:"
(( java.io.PrintWriter ) v0 ).println ( v1 ); // invoke-virtual {v0, v1}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V
/* .line 1345 */
(( java.io.PrintWriter ) v0 ).println ( ); // invoke-virtual {v0}, Ljava/io/PrintWriter;->println()V
/* .line 1346 */
return;
} // .end method
