.class final Lcom/miui/server/rtboost/SchedBoostService$LocalService;
.super Ljava/lang/Object;
.source "SchedBoostService.java"

# interfaces
.implements Lcom/miui/server/rtboost/SchedBoostManagerInternal;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/miui/server/rtboost/SchedBoostService;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x12
    name = "LocalService"
.end annotation


# instance fields
.field final synthetic this$0:Lcom/miui/server/rtboost/SchedBoostService;


# direct methods
.method private constructor <init>(Lcom/miui/server/rtboost/SchedBoostService;)V
    .locals 0

    .line 894
    iput-object p1, p0, Lcom/miui/server/rtboost/SchedBoostService$LocalService;->this$0:Lcom/miui/server/rtboost/SchedBoostService;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method synthetic constructor <init>(Lcom/miui/server/rtboost/SchedBoostService;Lcom/miui/server/rtboost/SchedBoostService$LocalService-IA;)V
    .locals 0

    invoke-direct {p0, p1}, Lcom/miui/server/rtboost/SchedBoostService$LocalService;-><init>(Lcom/miui/server/rtboost/SchedBoostService;)V

    return-void
.end method


# virtual methods
.method public beginSchedThreads([IJLjava/lang/String;I)V
    .locals 6
    .param p1, "tids"    # [I
    .param p2, "duration"    # J
    .param p4, "procName"    # Ljava/lang/String;
    .param p5, "mode"    # I

    .line 908
    iget-object v0, p0, Lcom/miui/server/rtboost/SchedBoostService$LocalService;->this$0:Lcom/miui/server/rtboost/SchedBoostService;

    move-object v1, p1

    move-wide v2, p2

    move-object v4, p4

    move v5, p5

    invoke-virtual/range {v0 .. v5}, Lcom/miui/server/rtboost/SchedBoostService;->beginSchedThreads([IJLjava/lang/String;I)V

    .line 909
    return-void
.end method

.method public boostHomeAnim(JI)V
    .locals 1
    .param p1, "duration"    # J
    .param p3, "mode"    # I

    .line 933
    iget-object v0, p0, Lcom/miui/server/rtboost/SchedBoostService$LocalService;->this$0:Lcom/miui/server/rtboost/SchedBoostService;

    invoke-virtual {v0, p1, p2, p3}, Lcom/miui/server/rtboost/SchedBoostService;->boostHomeAnim(JI)V

    .line 934
    return-void
.end method

.method public checkThreadBoost(I)Z
    .locals 1
    .param p1, "tid"    # I

    .line 918
    iget-object v0, p0, Lcom/miui/server/rtboost/SchedBoostService$LocalService;->this$0:Lcom/miui/server/rtboost/SchedBoostService;

    invoke-virtual {v0, p1}, Lcom/miui/server/rtboost/SchedBoostService;->checkThreadBoost(I)Z

    move-result v0

    return v0
.end method

.method public enableSchedBoost(Z)V
    .locals 1
    .param p1, "enable"    # Z

    .line 903
    iget-object v0, p0, Lcom/miui/server/rtboost/SchedBoostService$LocalService;->this$0:Lcom/miui/server/rtboost/SchedBoostService;

    invoke-virtual {v0, p1}, Lcom/miui/server/rtboost/SchedBoostService;->enableSchedBoost(Z)V

    .line 904
    return-void
.end method

.method public schedProcessBoost(Lcom/android/server/wm/WindowProcessListener;Ljava/lang/String;IIIJ)V
    .locals 8
    .param p1, "proc"    # Lcom/android/server/wm/WindowProcessListener;
    .param p2, "procName"    # Ljava/lang/String;
    .param p3, "pid"    # I
    .param p4, "rtid"    # I
    .param p5, "schedMode"    # I
    .param p6, "timeout"    # J

    .line 898
    iget-object v0, p0, Lcom/miui/server/rtboost/SchedBoostService$LocalService;->this$0:Lcom/miui/server/rtboost/SchedBoostService;

    move-object v1, p1

    move-object v2, p2

    move v3, p3

    move v4, p4

    move v5, p5

    move-wide v6, p6

    invoke-virtual/range {v0 .. v7}, Lcom/miui/server/rtboost/SchedBoostService;->schedProcessBoost(Lcom/android/server/wm/WindowProcessListener;Ljava/lang/String;IIIJ)V

    .line 899
    return-void
.end method

.method public setRenderThreadTid(Lcom/android/server/wm/WindowProcessController;)V
    .locals 1
    .param p1, "wpc"    # Lcom/android/server/wm/WindowProcessController;

    .line 913
    iget-object v0, p0, Lcom/miui/server/rtboost/SchedBoostService$LocalService;->this$0:Lcom/miui/server/rtboost/SchedBoostService;

    invoke-virtual {v0, p1}, Lcom/miui/server/rtboost/SchedBoostService;->setRenderThreadTid(Lcom/android/server/wm/WindowProcessController;)V

    .line 914
    return-void
.end method

.method public setThreadSavedPriority([II)V
    .locals 1
    .param p1, "tid"    # [I
    .param p2, "prio"    # I

    .line 923
    iget-object v0, p0, Lcom/miui/server/rtboost/SchedBoostService$LocalService;->this$0:Lcom/miui/server/rtboost/SchedBoostService;

    invoke-virtual {v0, p1, p2}, Lcom/miui/server/rtboost/SchedBoostService;->setThreadSavedPriority([II)V

    .line 924
    return-void
.end method

.method public stopCurrentSchedBoost()V
    .locals 1

    .line 928
    iget-object v0, p0, Lcom/miui/server/rtboost/SchedBoostService$LocalService;->this$0:Lcom/miui/server/rtboost/SchedBoostService;

    invoke-virtual {v0}, Lcom/miui/server/rtboost/SchedBoostService;->stopCurrentSchedBoost()V

    .line 929
    return-void
.end method
