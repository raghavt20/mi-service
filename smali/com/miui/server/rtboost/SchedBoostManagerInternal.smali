.class public interface abstract Lcom/miui/server/rtboost/SchedBoostManagerInternal;
.super Ljava/lang/Object;
.source "SchedBoostManagerInternal.java"


# virtual methods
.method public abstract beginSchedThreads([IJLjava/lang/String;I)V
.end method

.method public abstract boostHomeAnim(JI)V
.end method

.method public abstract checkThreadBoost(I)Z
.end method

.method public abstract enableSchedBoost(Z)V
.end method

.method public abstract schedProcessBoost(Lcom/android/server/wm/WindowProcessListener;Ljava/lang/String;IIIJ)V
.end method

.method public abstract setRenderThreadTid(Lcom/android/server/wm/WindowProcessController;)V
.end method

.method public abstract setThreadSavedPriority([II)V
.end method

.method public abstract stopCurrentSchedBoost()V
.end method
