.class Lcom/miui/server/rtboost/SchedBoostService$3;
.super Ljava/lang/Object;
.source "SchedBoostService.java"

# interfaces
.implements Lcom/android/server/wm/SchedBoostGesturesEvent$GesturesEventListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/miui/server/rtboost/SchedBoostService;-><init>(Landroid/content/Context;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/miui/server/rtboost/SchedBoostService;


# direct methods
.method constructor <init>(Lcom/miui/server/rtboost/SchedBoostService;)V
    .locals 0
    .param p1, "this$0"    # Lcom/miui/server/rtboost/SchedBoostService;

    .line 237
    iput-object p1, p0, Lcom/miui/server/rtboost/SchedBoostService$3;->this$0:Lcom/miui/server/rtboost/SchedBoostService;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onDown()V
    .locals 3

    .line 252
    const-string v0, "onDown"

    const-wide/16 v1, 0x4

    invoke-static {v1, v2, v0}, Landroid/os/Trace;->traceBegin(JLjava/lang/String;)V

    .line 253
    invoke-static {}, Lcom/android/server/wm/RealTimeModeControllerImpl;->get()Lcom/android/server/wm/RealTimeModeControllerImpl;

    move-result-object v0

    invoke-virtual {v0}, Lcom/android/server/wm/RealTimeModeControllerImpl;->onDown()V

    .line 254
    invoke-static {v1, v2}, Landroid/os/Trace;->traceEnd(J)V

    .line 255
    return-void
.end method

.method public onFling(FFI)V
    .locals 1
    .param p1, "velocityX"    # F
    .param p2, "velocityY"    # F
    .param p3, "durationMs"    # I

    .line 240
    invoke-static {}, Lcom/android/server/wm/RealTimeModeControllerImpl;->get()Lcom/android/server/wm/RealTimeModeControllerImpl;

    move-result-object v0

    invoke-virtual {v0, p3}, Lcom/android/server/wm/RealTimeModeControllerImpl;->onFling(I)V

    .line 241
    return-void
.end method

.method public onMove()V
    .locals 3

    .line 259
    const-string v0, "onMove"

    const-wide/16 v1, 0x4

    invoke-static {v1, v2, v0}, Landroid/os/Trace;->traceBegin(JLjava/lang/String;)V

    .line 260
    iget-object v0, p0, Lcom/miui/server/rtboost/SchedBoostService$3;->this$0:Lcom/miui/server/rtboost/SchedBoostService;

    invoke-static {v0}, Lcom/miui/server/rtboost/SchedBoostService;->-$$Nest$fgetmBoostingMap(Lcom/miui/server/rtboost/SchedBoostService;)Lcom/miui/server/rtboost/SchedBoostService$BoostingPackageMap;

    move-result-object v0

    invoke-virtual {v0}, Lcom/miui/server/rtboost/SchedBoostService$BoostingPackageMap;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 261
    invoke-static {}, Lcom/android/server/wm/RealTimeModeControllerImpl;->get()Lcom/android/server/wm/RealTimeModeControllerImpl;

    move-result-object v0

    invoke-virtual {v0}, Lcom/android/server/wm/RealTimeModeControllerImpl;->onMove()V

    .line 263
    :cond_0
    invoke-static {v1, v2}, Landroid/os/Trace;->traceEnd(J)V

    .line 264
    return-void
.end method

.method public onScroll(Z)V
    .locals 3
    .param p1, "started"    # Z

    .line 245
    const-string v0, "onScroll"

    const-wide/16 v1, 0x4

    invoke-static {v1, v2, v0}, Landroid/os/Trace;->traceBegin(JLjava/lang/String;)V

    .line 246
    invoke-static {}, Lcom/android/server/wm/RealTimeModeControllerImpl;->get()Lcom/android/server/wm/RealTimeModeControllerImpl;

    move-result-object v0

    invoke-virtual {v0, p1}, Lcom/android/server/wm/RealTimeModeControllerImpl;->onScroll(Z)V

    .line 247
    invoke-static {v1, v2}, Landroid/os/Trace;->traceEnd(J)V

    .line 248
    return-void
.end method
