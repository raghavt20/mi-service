.class public Lcom/miui/server/rtboost/SchedBoostService;
.super Landroid/os/Binder;
.source "SchedBoostService.java"

# interfaces
.implements Lcom/miui/server/rtboost/SchedBoostManagerInternal;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/miui/server/rtboost/SchedBoostService$BoostingPackageMap;,
        Lcom/miui/server/rtboost/SchedBoostService$Handler;,
        Lcom/miui/server/rtboost/SchedBoostService$LocalService;,
        Lcom/miui/server/rtboost/SchedBoostService$BoostThreadInfo;,
        Lcom/miui/server/rtboost/SchedBoostService$SchedBoostShellCommand;,
        Lcom/miui/server/rtboost/SchedBoostService$Lifecycle;
    }
.end annotation


# static fields
.field private static ALL_CPU_CORES:[I = null

.field private static final BOOST_HOME_GPU_TIDS_MAX_RETRY:I = 0x3

.field private static final CMDLINE_OUT:[I

.field public static final DEBUG:Z

.field private static final DEFAULT_QCOM_PERF_LIST:[I

.field private static final DEFAULT_UCLAMP_MAX:I = 0x400

.field private static final DEFAULT_UCLAMP_MIN:I = 0x0

.field private static final DEFAULT_UCLAMP_UPPER:I = 0x266

.field private static final ENABLE_RTMODE_UCLAMP:Z

.field private static final HOME_PACKAGE_NAME:Ljava/lang/String; = "com.miui.home"

.field private static final IS_MTK_DEVICE:Z

.field public static final IS_SCHED_HOME_GPU_THREADS_ENABLED:Z

.field public static final IS_SERVICE_ENABLED:Z = true

.field public static final IS_UCLAMP_ENABLED:Z

.field public static final IS_UIGROUP_ENABLED:Z

.field private static final MTK_GPU_AND_CPU_BOOST_LIST:[I

.field private static final PLATFORM_8650:Ljava/lang/String; = "pineapple"

.field private static final PLATFORM_NAME:Ljava/lang/String;

.field private static final QCOM_GPU_AND_CPU_BOOST_LIST:[I

.field private static final QCOM_GPU_AND_CPU_HIGHER_FREQ:[I

.field private static final REPEAT_CALL_DURATION:J = 0x12cL

.field private static final RT_THREAD_COMM_LIST:[Ljava/lang/String;

.field public static final SERVICE_NAME:Ljava/lang/String; = "SchedBoostService"

.field private static final SYSTEMUI_PACKAGE_NAME:Ljava/lang/String; = "com.android.systemui"

.field public static final TAG:Ljava/lang/String; = "SchedBoost"

.field public static final THREAD_GROUP_UI:I = 0xa

.field private static final THREAD_NAME:Ljava/lang/String; = "SchedBoostServiceTh"

.field private static final THREAD_PRIORITY_HIGHEST:I = -0x14


# instance fields
.field private TASK_UCLAMP_MAX:I

.field private TASK_UCLAMP_MIN:I

.field private isInited:Z

.field private isNormalPolicy:Z

.field private final mAlwaysRtTids:Landroid/util/ArraySet;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/util/ArraySet<",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation
.end field

.field private mBoostHomeGpuTidsTryCnt:I

.field private mBoostingMap:Lcom/miui/server/rtboost/SchedBoostService$BoostingPackageMap;

.field private mContext:Landroid/content/Context;

.field private mDefultPerfList:[I

.field private mGpuAndCpuPerfList:[I

.field private mHandle:I

.field private mHandler:Lcom/miui/server/rtboost/SchedBoostService$Handler;

.field private mHandlerThread:Landroid/os/HandlerThread;

.field private mHomeMainThreadId:I

.field private mHomeRenderThreadId:I

.field private mLastPerfAcqTime:J

.field private mMTKPerfStub:Landroid/perf/PerfMTKStub;

.field private mMethodSetAffinity:Ljava/lang/reflect/Method;

.field private mMethodUclampTask:Ljava/lang/reflect/Method;

.field private mPendingHomeGpuTids:Z

.field private mPreBoostProcessName:Ljava/lang/String;

.field private mQcomPerfStub:Landroid/perf/PerfStub;

.field private final mRtTids:Landroid/util/ArraySet;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/util/ArraySet<",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation
.end field

.field private mSchedBoostGesturesEvent:Lcom/android/server/wm/SchedBoostGesturesEvent;

.field private mScreenOff:Z

.field private mSimpleNotNeedSched:Z

.field private final mSysStatusReceiver:Landroid/content/BroadcastReceiver;

.field private mSystemUIMainThreadId:I

.field private mSystemUIRenderThreadId:I

.field private mWMS:Lcom/android/server/wm/WindowManagerService;


# direct methods
.method public static synthetic $r8$lambda$MVdnxCbI3iTc_Kk4RLYO4E0P4PY(Lcom/miui/server/rtboost/SchedBoostService;I)V
    .locals 0

    invoke-direct {p0, p1}, Lcom/miui/server/rtboost/SchedBoostService;->lambda$new$0(I)V

    return-void
.end method

.method static bridge synthetic -$$Nest$fgetmBoostingMap(Lcom/miui/server/rtboost/SchedBoostService;)Lcom/miui/server/rtboost/SchedBoostService$BoostingPackageMap;
    .locals 0

    iget-object p0, p0, Lcom/miui/server/rtboost/SchedBoostService;->mBoostingMap:Lcom/miui/server/rtboost/SchedBoostService$BoostingPackageMap;

    return-object p0
.end method

.method static bridge synthetic -$$Nest$fputmScreenOff(Lcom/miui/server/rtboost/SchedBoostService;Z)V
    .locals 0

    iput-boolean p1, p0, Lcom/miui/server/rtboost/SchedBoostService;->mScreenOff:Z

    return-void
.end method

.method static bridge synthetic -$$Nest$mhandleBeginSchedThread(Lcom/miui/server/rtboost/SchedBoostService;[IJLjava/lang/String;I)V
    .locals 0

    invoke-direct/range {p0 .. p5}, Lcom/miui/server/rtboost/SchedBoostService;->handleBeginSchedThread([IJLjava/lang/String;I)V

    return-void
.end method

.method static bridge synthetic -$$Nest$mhandleBoostFreq(Lcom/miui/server/rtboost/SchedBoostService;[IJLjava/lang/String;I)V
    .locals 0

    invoke-direct/range {p0 .. p5}, Lcom/miui/server/rtboost/SchedBoostService;->handleBoostFreq([IJLjava/lang/String;I)V

    return-void
.end method

.method static bridge synthetic -$$Nest$mhandleReSetThreadsFIFO(Lcom/miui/server/rtboost/SchedBoostService;IJI)V
    .locals 0

    invoke-direct {p0, p1, p2, p3, p4}, Lcom/miui/server/rtboost/SchedBoostService;->handleReSetThreadsFIFO(IJI)V

    return-void
.end method

.method static bridge synthetic -$$Nest$mhandleResetSchedThread(Lcom/miui/server/rtboost/SchedBoostService;Lcom/miui/server/rtboost/SchedBoostService$BoostThreadInfo;)V
    .locals 0

    invoke-direct {p0, p1}, Lcom/miui/server/rtboost/SchedBoostService;->handleResetSchedThread(Lcom/miui/server/rtboost/SchedBoostService$BoostThreadInfo;)V

    return-void
.end method

.method static bridge synthetic -$$Nest$mhandleSchedThread(Lcom/miui/server/rtboost/SchedBoostService;Lcom/miui/server/rtboost/SchedBoostService$BoostThreadInfo;)V
    .locals 0

    invoke-direct {p0, p1}, Lcom/miui/server/rtboost/SchedBoostService;->handleSchedThread(Lcom/miui/server/rtboost/SchedBoostService$BoostThreadInfo;)V

    return-void
.end method

.method static bridge synthetic -$$Nest$mhandleSetThreadsAlwaysFIFO(Lcom/miui/server/rtboost/SchedBoostService;[I)V
    .locals 0

    invoke-direct {p0, p1}, Lcom/miui/server/rtboost/SchedBoostService;->handleSetThreadsAlwaysFIFO([I)V

    return-void
.end method

.method static bridge synthetic -$$Nest$mhandleSetThreadsFIFO(Lcom/miui/server/rtboost/SchedBoostService;IJI)V
    .locals 0

    invoke-direct {p0, p1, p2, p3, p4}, Lcom/miui/server/rtboost/SchedBoostService;->handleSetThreadsFIFO(IJI)V

    return-void
.end method

.method static bridge synthetic -$$Nest$mhandleThreadsBindBigCoreMsg(Lcom/miui/server/rtboost/SchedBoostService;Ljava/lang/Integer;J)V
    .locals 0

    invoke-direct {p0, p1, p2, p3}, Lcom/miui/server/rtboost/SchedBoostService;->handleThreadsBindBigCoreMsg(Ljava/lang/Integer;J)V

    return-void
.end method

.method static bridge synthetic -$$Nest$mhandleThreadsUnBindBigCoreMsg(Lcom/miui/server/rtboost/SchedBoostService;Ljava/lang/Integer;)V
    .locals 0

    invoke-direct {p0, p1}, Lcom/miui/server/rtboost/SchedBoostService;->handleThreadsUnBindBigCoreMsg(Ljava/lang/Integer;)V

    return-void
.end method

.method static constructor <clinit>()V
    .locals 3

    .line 56
    sget-boolean v0, Lcom/android/server/wm/RealTimeModeControllerImpl;->DEBUG:Z

    sput-boolean v0, Lcom/miui/server/rtboost/SchedBoostService;->DEBUG:Z

    .line 66
    const-string v0, "ro.vendor.qti.soc_name"

    invoke-static {v0}, Landroid/os/SystemProperties;->get(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/miui/server/rtboost/SchedBoostService;->PLATFORM_NAME:Ljava/lang/String;

    .line 68
    nop

    .line 69
    const-string v0, "is_mediatek"

    const/4 v1, 0x0

    invoke-static {v0, v1}, Lmiui/util/FeatureParser;->getBoolean(Ljava/lang/String;Z)Z

    move-result v0

    sput-boolean v0, Lcom/miui/server/rtboost/SchedBoostService;->IS_MTK_DEVICE:Z

    .line 70
    nop

    .line 71
    const-string v2, "persist.sys.enable_rtmode_uclamp"

    invoke-static {v2, v1}, Landroid/os/SystemProperties;->getBoolean(Ljava/lang/String;Z)Z

    move-result v2

    sput-boolean v2, Lcom/miui/server/rtboost/SchedBoostService;->ENABLE_RTMODE_UCLAMP:Z

    .line 72
    if-eqz v0, :cond_0

    if-eqz v2, :cond_0

    const/4 v0, 0x1

    goto :goto_0

    :cond_0
    move v0, v1

    :goto_0
    sput-boolean v0, Lcom/miui/server/rtboost/SchedBoostService;->IS_UCLAMP_ENABLED:Z

    .line 73
    nop

    .line 74
    const-string v0, "persist.sys.enable_setuicgroup"

    invoke-static {v0, v1}, Landroid/os/SystemProperties;->getBoolean(Ljava/lang/String;Z)Z

    move-result v0

    sput-boolean v0, Lcom/miui/server/rtboost/SchedBoostService;->IS_UIGROUP_ENABLED:Z

    .line 75
    nop

    .line 76
    const-string v0, "persist.sys.enable_sched_gpu_threads"

    invoke-static {v0, v1}, Landroid/os/SystemProperties;->getBoolean(Ljava/lang/String;Z)Z

    move-result v0

    sput-boolean v0, Lcom/miui/server/rtboost/SchedBoostService;->IS_SCHED_HOME_GPU_THREADS_ENABLED:Z

    .line 77
    const/16 v0, 0x1000

    filled-new-array {v0}, [I

    move-result-object v0

    sput-object v0, Lcom/miui/server/rtboost/SchedBoostService;->CMDLINE_OUT:[I

    .line 81
    const/4 v0, 0x0

    sput-object v0, Lcom/miui/server/rtboost/SchedBoostService;->ALL_CPU_CORES:[I

    .line 88
    const-string v0, "mali-cpu-comman"

    const-string v1, "ged-swd"

    const-string v2, "mali-event-hand"

    filled-new-array {v2, v0, v1}, [Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/miui/server/rtboost/SchedBoostService;->RT_THREAD_COMM_LIST:[Ljava/lang/String;

    .line 109
    const/4 v0, 0x6

    new-array v0, v0, [I

    fill-array-data v0, :array_0

    sput-object v0, Lcom/miui/server/rtboost/SchedBoostService;->DEFAULT_QCOM_PERF_LIST:[I

    .line 114
    const/16 v0, 0x10

    new-array v1, v0, [I

    fill-array-data v1, :array_1

    sput-object v1, Lcom/miui/server/rtboost/SchedBoostService;->QCOM_GPU_AND_CPU_BOOST_LIST:[I

    .line 125
    new-array v0, v0, [I

    fill-array-data v0, :array_2

    sput-object v0, Lcom/miui/server/rtboost/SchedBoostService;->QCOM_GPU_AND_CPU_HIGHER_FREQ:[I

    .line 135
    const/16 v0, 0x16

    new-array v0, v0, [I

    fill-array-data v0, :array_3

    sput-object v0, Lcom/miui/server/rtboost/SchedBoostService;->MTK_GPU_AND_CPU_BOOST_LIST:[I

    return-void

    nop

    :array_0
    .array-data 4
        0x40800000    # 4.0f
        0x5dc
        0x42804000    # 64.125f
        0x0
        0x42c20000    # 97.0f
        0x1
    .end array-data

    :array_1
    .array-data 4
        0x40400001    # 3.0000002f
        0x1
        0x40c00000    # 6.0f
        0x1
        0x40804000    # 4.0078125f
        0xfff
        0x40804100
        0xfff
        0x40800000    # 4.0f
        0x6a4
        0x40800100    # 4.000122f
        0x6a4
        0x42804000    # 64.125f
        0x0
        0x42c20000    # 97.0f
        0x1
    .end array-data

    :array_2
    .array-data 4
        0x40400001    # 3.0000002f
        0x1
        0x40c00000    # 6.0f
        0x1
        0x40804000    # 4.0078125f
        0xfff
        0x40804100
        0xfff
        0x40800000    # 4.0f
        0x8fc
        0x40800100    # 4.000122f
        0xfff
        0x42804000    # 64.125f
        0x0
        0x42c20000    # 97.0f
        0x1
    .end array-data

    :array_3
    .array-data 4
        0x400000
        0x1b7740
        0x400100
        0x1e8480
        0x400200
        0x1e8480
        0x1438400
        0x64
        0x1438500
        0x64
        0xc00000
        0x23
        0xc0c200
        0x11e003c
        0xc0c300
        0x303
        0x1404100
        0x1
        0x1404300
        0x0
        0x1404400
        0x1
    .end array-data
.end method

.method public constructor <init>(Landroid/content/Context;)V
    .locals 3
    .param p1, "context"    # Landroid/content/Context;

    .line 204
    invoke-direct {p0}, Landroid/os/Binder;-><init>()V

    .line 82
    nop

    .line 83
    const-string v0, "persist.sys.speedui_uclamp_min"

    const/16 v1, 0x266

    invoke-static {v0, v1}, Landroid/os/SystemProperties;->getInt(Ljava/lang/String;I)I

    move-result v0

    iput v0, p0, Lcom/miui/server/rtboost/SchedBoostService;->TASK_UCLAMP_MIN:I

    .line 84
    nop

    .line 85
    const-string v0, "persist.sys.speedui_uclamp_max"

    const/16 v1, 0x400

    invoke-static {v0, v1}, Landroid/os/SystemProperties;->getInt(Ljava/lang/String;I)I

    move-result v0

    iput v0, p0, Lcom/miui/server/rtboost/SchedBoostService;->TASK_UCLAMP_MAX:I

    .line 94
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/miui/server/rtboost/SchedBoostService;->mPendingHomeGpuTids:Z

    .line 95
    const/4 v0, 0x0

    iput v0, p0, Lcom/miui/server/rtboost/SchedBoostService;->mBoostHomeGpuTidsTryCnt:I

    .line 102
    iput v0, p0, Lcom/miui/server/rtboost/SchedBoostService;->mHandle:I

    .line 103
    const-wide/16 v1, 0x0

    iput-wide v1, p0, Lcom/miui/server/rtboost/SchedBoostService;->mLastPerfAcqTime:J

    .line 152
    new-instance v1, Lcom/miui/server/rtboost/SchedBoostService$BoostingPackageMap;

    const/4 v2, 0x0

    invoke-direct {v1, p0, v2}, Lcom/miui/server/rtboost/SchedBoostService$BoostingPackageMap;-><init>(Lcom/miui/server/rtboost/SchedBoostService;Lcom/miui/server/rtboost/SchedBoostService$BoostingPackageMap-IA;)V

    iput-object v1, p0, Lcom/miui/server/rtboost/SchedBoostService;->mBoostingMap:Lcom/miui/server/rtboost/SchedBoostService$BoostingPackageMap;

    .line 153
    new-instance v1, Landroid/util/ArraySet;

    invoke-direct {v1}, Landroid/util/ArraySet;-><init>()V

    iput-object v1, p0, Lcom/miui/server/rtboost/SchedBoostService;->mAlwaysRtTids:Landroid/util/ArraySet;

    .line 154
    new-instance v1, Landroid/util/ArraySet;

    invoke-direct {v1}, Landroid/util/ArraySet;-><init>()V

    iput-object v1, p0, Lcom/miui/server/rtboost/SchedBoostService;->mRtTids:Landroid/util/ArraySet;

    .line 155
    iput-object v2, p0, Lcom/miui/server/rtboost/SchedBoostService;->mMethodSetAffinity:Ljava/lang/reflect/Method;

    .line 156
    iput-object v2, p0, Lcom/miui/server/rtboost/SchedBoostService;->mMethodUclampTask:Ljava/lang/reflect/Method;

    .line 159
    iput-object v2, p0, Lcom/miui/server/rtboost/SchedBoostService;->mPreBoostProcessName:Ljava/lang/String;

    .line 160
    iput v0, p0, Lcom/miui/server/rtboost/SchedBoostService;->mHomeMainThreadId:I

    .line 161
    iput v0, p0, Lcom/miui/server/rtboost/SchedBoostService;->mHomeRenderThreadId:I

    .line 162
    iput v0, p0, Lcom/miui/server/rtboost/SchedBoostService;->mSystemUIMainThreadId:I

    .line 163
    iput v0, p0, Lcom/miui/server/rtboost/SchedBoostService;->mSystemUIRenderThreadId:I

    .line 168
    iput-boolean v0, p0, Lcom/miui/server/rtboost/SchedBoostService;->mScreenOff:Z

    .line 170
    new-instance v0, Lcom/miui/server/rtboost/SchedBoostService$1;

    invoke-direct {v0, p0}, Lcom/miui/server/rtboost/SchedBoostService$1;-><init>(Lcom/miui/server/rtboost/SchedBoostService;)V

    iput-object v0, p0, Lcom/miui/server/rtboost/SchedBoostService;->mSysStatusReceiver:Landroid/content/BroadcastReceiver;

    .line 205
    iput-object p1, p0, Lcom/miui/server/rtboost/SchedBoostService;->mContext:Landroid/content/Context;

    .line 207
    sget v0, Landroid/os/MiuiProcess;->PROPERTY_CPU_CORE_COUNT:I

    if-gtz v0, :cond_0

    .line 208
    return-void

    .line 210
    :cond_0
    sget v0, Landroid/os/MiuiProcess;->PROPERTY_CPU_CORE_COUNT:I

    new-array v0, v0, [I

    sput-object v0, Lcom/miui/server/rtboost/SchedBoostService;->ALL_CPU_CORES:[I

    .line 211
    const/4 v0, 0x0

    .local v0, "i":I
    :goto_0
    sget v1, Landroid/os/MiuiProcess;->PROPERTY_CPU_CORE_COUNT:I

    if-ge v0, v1, :cond_1

    .line 212
    sget-object v1, Lcom/miui/server/rtboost/SchedBoostService;->ALL_CPU_CORES:[I

    aput v0, v1, v0

    .line 211
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 215
    .end local v0    # "i":I
    :cond_1
    const-string/jumbo v0, "window"

    invoke-static {v0}, Landroid/os/ServiceManager;->getService(Ljava/lang/String;)Landroid/os/IBinder;

    move-result-object v0

    check-cast v0, Lcom/android/server/wm/WindowManagerService;

    iput-object v0, p0, Lcom/miui/server/rtboost/SchedBoostService;->mWMS:Lcom/android/server/wm/WindowManagerService;

    .line 217
    sget-boolean v0, Lcom/android/server/wm/RealTimeModeControllerImpl;->ENABLE_RT_MODE:Z

    if-eqz v0, :cond_2

    invoke-direct {p0}, Lcom/miui/server/rtboost/SchedBoostService;->init()Z

    move-result v0

    if-eqz v0, :cond_2

    .line 218
    new-instance v0, Lcom/miui/server/rtboost/SchedBoostService$2;

    const-string v1, "SchedBoostServiceTh"

    invoke-direct {v0, p0, v1}, Lcom/miui/server/rtboost/SchedBoostService$2;-><init>(Lcom/miui/server/rtboost/SchedBoostService;Ljava/lang/String;)V

    iput-object v0, p0, Lcom/miui/server/rtboost/SchedBoostService;->mHandlerThread:Landroid/os/HandlerThread;

    .line 227
    invoke-virtual {v0}, Landroid/os/HandlerThread;->start()V

    .line 228
    new-instance v0, Lcom/miui/server/rtboost/SchedBoostService$Handler;

    iget-object v1, p0, Lcom/miui/server/rtboost/SchedBoostService;->mHandlerThread:Landroid/os/HandlerThread;

    invoke-virtual {v1}, Landroid/os/HandlerThread;->getLooper()Landroid/os/Looper;

    move-result-object v1

    invoke-direct {v0, p0, v1}, Lcom/miui/server/rtboost/SchedBoostService$Handler;-><init>(Lcom/miui/server/rtboost/SchedBoostService;Landroid/os/Looper;)V

    iput-object v0, p0, Lcom/miui/server/rtboost/SchedBoostService;->mHandler:Lcom/miui/server/rtboost/SchedBoostService$Handler;

    .line 229
    const-class v0, Lcom/miui/server/rtboost/SchedBoostManagerInternal;

    new-instance v1, Lcom/miui/server/rtboost/SchedBoostService$LocalService;

    invoke-direct {v1, p0, v2}, Lcom/miui/server/rtboost/SchedBoostService$LocalService;-><init>(Lcom/miui/server/rtboost/SchedBoostService;Lcom/miui/server/rtboost/SchedBoostService$LocalService-IA;)V

    invoke-static {v0, v1}, Lcom/android/server/LocalServices;->addService(Ljava/lang/Class;Ljava/lang/Object;)V

    .line 231
    invoke-static {}, Lcom/android/server/wm/RealTimeModeControllerStub;->get()Lcom/android/server/wm/RealTimeModeControllerStub;

    move-result-object v0

    iget-object v1, p0, Lcom/miui/server/rtboost/SchedBoostService;->mContext:Landroid/content/Context;

    invoke-interface {v0, v1}, Lcom/android/server/wm/RealTimeModeControllerStub;->init(Landroid/content/Context;)V

    .line 232
    invoke-static {}, Lcom/android/server/wm/RealTimeModeControllerStub;->get()Lcom/android/server/wm/RealTimeModeControllerStub;

    move-result-object v0

    iget-object v1, p0, Lcom/miui/server/rtboost/SchedBoostService;->mWMS:Lcom/android/server/wm/WindowManagerService;

    invoke-interface {v0, v1}, Lcom/android/server/wm/RealTimeModeControllerStub;->setWindowManager(Lcom/android/server/wm/WindowManagerService;)V

    .line 234
    new-instance v0, Lcom/android/server/wm/SchedBoostGesturesEvent;

    iget-object v1, p0, Lcom/miui/server/rtboost/SchedBoostService;->mHandlerThread:Landroid/os/HandlerThread;

    invoke-virtual {v1}, Landroid/os/HandlerThread;->getLooper()Landroid/os/Looper;

    move-result-object v1

    invoke-direct {v0, v1}, Lcom/android/server/wm/SchedBoostGesturesEvent;-><init>(Landroid/os/Looper;)V

    iput-object v0, p0, Lcom/miui/server/rtboost/SchedBoostService;->mSchedBoostGesturesEvent:Lcom/android/server/wm/SchedBoostGesturesEvent;

    .line 235
    invoke-virtual {v0, p1}, Lcom/android/server/wm/SchedBoostGesturesEvent;->init(Landroid/content/Context;)V

    .line 236
    iget-object v0, p0, Lcom/miui/server/rtboost/SchedBoostService;->mSchedBoostGesturesEvent:Lcom/android/server/wm/SchedBoostGesturesEvent;

    new-instance v1, Lcom/miui/server/rtboost/SchedBoostService$3;

    invoke-direct {v1, p0}, Lcom/miui/server/rtboost/SchedBoostService$3;-><init>(Lcom/miui/server/rtboost/SchedBoostService;)V

    invoke-virtual {v0, v1}, Lcom/android/server/wm/SchedBoostGesturesEvent;->setGesturesEventListener(Lcom/android/server/wm/SchedBoostGesturesEvent$GesturesEventListener;)V

    .line 268
    invoke-static {}, Lcom/android/server/am/SystemPressureController;->getInstance()Lcom/android/server/am/SystemPressureController;

    move-result-object v0

    new-instance v1, Lcom/miui/server/rtboost/SchedBoostService$$ExternalSyntheticLambda0;

    invoke-direct {v1, p0}, Lcom/miui/server/rtboost/SchedBoostService$$ExternalSyntheticLambda0;-><init>(Lcom/miui/server/rtboost/SchedBoostService;)V

    invoke-virtual {v0, v1}, Lcom/android/server/am/SystemPressureController;->registerThermalTempListener(Lcom/android/server/am/ThermalTempListener;)V

    .line 271
    new-instance v0, Landroid/content/IntentFilter;

    invoke-direct {v0}, Landroid/content/IntentFilter;-><init>()V

    .line 272
    .local v0, "filter":Landroid/content/IntentFilter;
    const-string v1, "android.intent.action.SCREEN_OFF"

    invoke-virtual {v0, v1}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    .line 273
    const-string v1, "android.intent.action.SCREEN_ON"

    invoke-virtual {v0, v1}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    .line 274
    iget-object v1, p0, Lcom/miui/server/rtboost/SchedBoostService;->mSysStatusReceiver:Landroid/content/BroadcastReceiver;

    invoke-virtual {p1, v1, v0}, Landroid/content/Context;->registerReceiver(Landroid/content/BroadcastReceiver;Landroid/content/IntentFilter;)Landroid/content/Intent;

    .line 276
    .end local v0    # "filter":Landroid/content/IntentFilter;
    :cond_2
    return-void
.end method

.method private changeSchedBoostPolicy(I)V
    .locals 3
    .param p1, "temp"    # I

    .line 1201
    sget-boolean v0, Lcom/android/server/wm/RealTimeModeControllerImpl;->ENABLE_RT_MODE:Z

    if-nez v0, :cond_0

    .line 1202
    return-void

    .line 1204
    :cond_0
    sget-boolean v0, Lcom/android/server/wm/RealTimeModeControllerImpl;->ENABLE_TEMP_LIMIT_ENABLE:Z

    if-nez v0, :cond_1

    .line 1205
    return-void

    .line 1207
    :cond_1
    sget v0, Lcom/android/server/wm/RealTimeModeControllerImpl;->RT_TEMPLIMIT_CEILING:I

    const-string v1, "SchedBoost"

    const/4 v2, 0x0

    if-lt p1, v0, :cond_2

    iget-boolean v0, p0, Lcom/miui/server/rtboost/SchedBoostService;->isNormalPolicy:Z

    if-nez v0, :cond_2

    .line 1208
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/miui/server/rtboost/SchedBoostService;->isNormalPolicy:Z

    .line 1209
    const-string v0, "onThermalTempChange, set sched policy OTHER"

    new-array v2, v2, [Ljava/lang/Object;

    invoke-static {v0, v2}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    invoke-static {v1, v0}, Landroid/util/Slog;->d(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    .line 1210
    :cond_2
    sget v0, Lcom/android/server/wm/RealTimeModeControllerImpl;->RT_TEMPLIMIT_BOTTOM:I

    if-gt p1, v0, :cond_3

    iget-boolean v0, p0, Lcom/miui/server/rtboost/SchedBoostService;->isNormalPolicy:Z

    if-eqz v0, :cond_3

    .line 1211
    iput-boolean v2, p0, Lcom/miui/server/rtboost/SchedBoostService;->isNormalPolicy:Z

    .line 1212
    const-string v0, "onThermalTempChange, set sched policy FIFO"

    new-array v2, v2, [Ljava/lang/Object;

    invoke-static {v0, v2}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    invoke-static {v1, v0}, Landroid/util/Slog;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 1214
    :cond_3
    :goto_0
    return-void
.end method

.method private dump(Ljava/io/PrintWriter;[Ljava/lang/String;)V
    .locals 5
    .param p1, "pw"    # Ljava/io/PrintWriter;
    .param p2, "args"    # [Ljava/lang/String;

    .line 1275
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "IS_MTK_DEVICE: "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    sget-boolean v1, Lcom/miui/server/rtboost/SchedBoostService;->IS_MTK_DEVICE:Z

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v0}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V

    .line 1276
    if-eqz v1, :cond_0

    .line 1277
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "ENABLE_RTMODE_UCLAMP: "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    sget-boolean v1, Lcom/miui/server/rtboost/SchedBoostService;->ENABLE_RTMODE_UCLAMP:Z

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v0}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V

    .line 1278
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "TASK_UCLAMP_MIN: "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget v1, p0, Lcom/miui/server/rtboost/SchedBoostService;->TASK_UCLAMP_MIN:I

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v0}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V

    .line 1279
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "TASK_UCLAMP_MIN: "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget v1, p0, Lcom/miui/server/rtboost/SchedBoostService;->TASK_UCLAMP_MAX:I

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v0}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V

    .line 1281
    :cond_0
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "mPreBoostProcessName: "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lcom/miui/server/rtboost/SchedBoostService;->mPreBoostProcessName:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v0}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V

    .line 1282
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "currently isNormalPolicy: "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-boolean v1, p0, Lcom/miui/server/rtboost/SchedBoostService;->isNormalPolicy:Z

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v0}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V

    .line 1283
    const-string v0, "AlwaysRtTids: "

    invoke-virtual {p1, v0}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V

    .line 1284
    iget-object v0, p0, Lcom/miui/server/rtboost/SchedBoostService;->mAlwaysRtTids:Landroid/util/ArraySet;

    monitor-enter v0

    .line 1285
    :try_start_0
    iget-object v1, p0, Lcom/miui/server/rtboost/SchedBoostService;->mAlwaysRtTids:Landroid/util/ArraySet;

    invoke-virtual {v1}, Landroid/util/ArraySet;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_1

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/lang/Integer;

    invoke-virtual {v2}, Ljava/lang/Integer;->intValue()I

    move-result v2

    .line 1286
    .local v2, "tid":I
    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "    "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {p1, v3}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V

    .line 1287
    .end local v2    # "tid":I
    goto :goto_0

    .line 1288
    :cond_1
    monitor-exit v0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 1289
    const-string v0, "Boosting Threads: "

    invoke-virtual {p1, v0}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V

    .line 1290
    iget-object v0, p0, Lcom/miui/server/rtboost/SchedBoostService;->mBoostingMap:Lcom/miui/server/rtboost/SchedBoostService$BoostingPackageMap;

    invoke-static {v0}, Lcom/miui/server/rtboost/SchedBoostService$BoostingPackageMap;->-$$Nest$fgetmMapLock(Lcom/miui/server/rtboost/SchedBoostService$BoostingPackageMap;)Ljava/lang/Object;

    move-result-object v1

    monitor-enter v1

    .line 1291
    :try_start_1
    iget-object v0, p0, Lcom/miui/server/rtboost/SchedBoostService;->mBoostingMap:Lcom/miui/server/rtboost/SchedBoostService$BoostingPackageMap;

    invoke-static {v0}, Lcom/miui/server/rtboost/SchedBoostService$BoostingPackageMap;->-$$Nest$fgetinfoList(Lcom/miui/server/rtboost/SchedBoostService$BoostingPackageMap;)Ljava/util/ArrayList;

    move-result-object v0

    invoke-virtual {v0}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v0

    :goto_1
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_2

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/miui/server/rtboost/SchedBoostService$BoostThreadInfo;

    .line 1292
    .local v2, "info":Lcom/miui/server/rtboost/SchedBoostService$BoostThreadInfo;
    invoke-virtual {v2, p1, p2}, Lcom/miui/server/rtboost/SchedBoostService$BoostThreadInfo;->dump(Ljava/io/PrintWriter;[Ljava/lang/String;)V

    .line 1293
    .end local v2    # "info":Lcom/miui/server/rtboost/SchedBoostService$BoostThreadInfo;
    goto :goto_1

    .line 1294
    :cond_2
    monitor-exit v1

    .line 1295
    return-void

    .line 1294
    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    throw v0

    .line 1288
    :catchall_1
    move-exception v1

    :try_start_2
    monitor-exit v0
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    throw v1
.end method

.method private getCurrentBoostThreadsList([IJLjava/lang/String;I)Ljava/util/ArrayList;
    .locals 14
    .param p1, "tids"    # [I
    .param p2, "duration"    # J
    .param p4, "procName"    # Ljava/lang/String;
    .param p5, "mode"    # I
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "([IJ",
            "Ljava/lang/String;",
            "I)",
            "Ljava/util/ArrayList<",
            "Lcom/miui/server/rtboost/SchedBoostService$BoostThreadInfo;",
            ">;"
        }
    .end annotation

    .line 876
    move-object v7, p0

    move-object v8, p1

    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    move-object v9, v0

    .line 878
    .local v9, "curList":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/miui/server/rtboost/SchedBoostService$BoostThreadInfo;>;"
    array-length v10, v8

    const/4 v0, 0x0

    move v11, v0

    :goto_0
    if-ge v11, v10, :cond_3

    aget v12, v8, v11

    .line 879
    .local v12, "tid":I
    if-gtz v12, :cond_0

    goto :goto_1

    .line 880
    :cond_0
    iget-object v0, v7, Lcom/miui/server/rtboost/SchedBoostService;->mAlwaysRtTids:Landroid/util/ArraySet;

    invoke-static {v12}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/util/ArraySet;->contains(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 881
    sget-boolean v0, Lcom/miui/server/rtboost/SchedBoostService;->DEBUG:Z

    if-eqz v0, :cond_2

    .line 882
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "already rttids:"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, v7, Lcom/miui/server/rtboost/SchedBoostService;->mAlwaysRtTids:Landroid/util/ArraySet;

    invoke-virtual {v1}, Landroid/util/ArraySet;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ",tid :"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, v12}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    const-string v1, "SchedBoost"

    invoke-static {v1, v0}, Landroid/util/Slog;->d(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_1

    .line 886
    :cond_1
    new-instance v13, Lcom/miui/server/rtboost/SchedBoostService$BoostThreadInfo;

    move-object v0, v13

    move-object v1, p0

    move v2, v12

    move-wide/from16 v3, p2

    move-object/from16 v5, p4

    move/from16 v6, p5

    invoke-direct/range {v0 .. v6}, Lcom/miui/server/rtboost/SchedBoostService$BoostThreadInfo;-><init>(Lcom/miui/server/rtboost/SchedBoostService;IJLjava/lang/String;I)V

    .line 887
    .local v0, "threadInfo":Lcom/miui/server/rtboost/SchedBoostService$BoostThreadInfo;
    iget-object v1, v7, Lcom/miui/server/rtboost/SchedBoostService;->mBoostingMap:Lcom/miui/server/rtboost/SchedBoostService$BoostingPackageMap;

    invoke-virtual {v1, v0}, Lcom/miui/server/rtboost/SchedBoostService$BoostingPackageMap;->put(Lcom/miui/server/rtboost/SchedBoostService$BoostThreadInfo;)V

    .line 888
    invoke-virtual {v9, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 878
    .end local v0    # "threadInfo":Lcom/miui/server/rtboost/SchedBoostService$BoostThreadInfo;
    .end local v12    # "tid":I
    :cond_2
    :goto_1
    add-int/lit8 v11, v11, 0x1

    goto :goto_0

    .line 891
    :cond_3
    return-object v9
.end method

.method private handleBeginSchedThread([IJLjava/lang/String;I)V
    .locals 6
    .param p1, "tids"    # [I
    .param p2, "duration"    # J
    .param p4, "procName"    # Ljava/lang/String;
    .param p5, "mode"    # I

    .line 392
    iget-object v0, p0, Lcom/miui/server/rtboost/SchedBoostService;->mPreBoostProcessName:Ljava/lang/String;

    if-eqz v0, :cond_3

    invoke-static {v0, p4}, Landroid/text/TextUtils;->equals(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_3

    .line 393
    iget-object v0, p0, Lcom/miui/server/rtboost/SchedBoostService;->mBoostingMap:Lcom/miui/server/rtboost/SchedBoostService$BoostingPackageMap;

    invoke-static {v0}, Lcom/miui/server/rtboost/SchedBoostService$BoostingPackageMap;->-$$Nest$fgetmMapLock(Lcom/miui/server/rtboost/SchedBoostService$BoostingPackageMap;)Ljava/lang/Object;

    move-result-object v0

    monitor-enter v0

    .line 394
    :try_start_0
    iget-object v1, p0, Lcom/miui/server/rtboost/SchedBoostService;->mBoostingMap:Lcom/miui/server/rtboost/SchedBoostService$BoostingPackageMap;

    invoke-virtual {v1}, Lcom/miui/server/rtboost/SchedBoostService$BoostingPackageMap;->getList()Ljava/util/ArrayList;

    move-result-object v1

    .line 395
    .local v1, "list":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/miui/server/rtboost/SchedBoostService$BoostThreadInfo;>;"
    if-eqz v1, :cond_2

    invoke-virtual {v1}, Ljava/util/ArrayList;->size()I

    move-result v2

    if-lez v2, :cond_2

    .line 396
    const/4 v2, 0x0

    .local v2, "i":I
    :goto_0
    invoke-virtual {v1}, Ljava/util/ArrayList;->size()I

    move-result v3

    if-ge v2, v3, :cond_2

    .line 397
    invoke-virtual {v1, v2}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/miui/server/rtboost/SchedBoostService$BoostThreadInfo;

    .line 399
    .local v3, "threadInfo":Lcom/miui/server/rtboost/SchedBoostService$BoostThreadInfo;
    iget v4, v3, Lcom/miui/server/rtboost/SchedBoostService$BoostThreadInfo;->mode:I

    const/4 v5, 0x4

    if-eq v4, v5, :cond_0

    iget v4, v3, Lcom/miui/server/rtboost/SchedBoostService$BoostThreadInfo;->mode:I

    const/4 v5, 0x5

    if-ne v4, v5, :cond_1

    :cond_0
    iget v4, v3, Lcom/miui/server/rtboost/SchedBoostService$BoostThreadInfo;->mode:I

    if-ge p5, v4, :cond_1

    .line 402
    monitor-exit v0

    return-void

    .line 404
    :cond_1
    iget-object v4, p0, Lcom/miui/server/rtboost/SchedBoostService;->mHandler:Lcom/miui/server/rtboost/SchedBoostService$Handler;

    invoke-virtual {v4, v3}, Lcom/miui/server/rtboost/SchedBoostService$Handler;->removeResetThreadsMsg(Lcom/miui/server/rtboost/SchedBoostService$BoostThreadInfo;)V

    .line 405
    iget-object v4, p0, Lcom/miui/server/rtboost/SchedBoostService;->mHandler:Lcom/miui/server/rtboost/SchedBoostService$Handler;

    invoke-virtual {v4, v3}, Lcom/miui/server/rtboost/SchedBoostService$Handler;->obtainResetSchedThreadsMsg(Lcom/miui/server/rtboost/SchedBoostService$BoostThreadInfo;)Landroid/os/Message;

    move-result-object v4

    .line 406
    invoke-virtual {v4}, Landroid/os/Message;->sendToTarget()V

    .line 396
    .end local v3    # "threadInfo":Lcom/miui/server/rtboost/SchedBoostService$BoostThreadInfo;
    add-int/lit8 v2, v2, 0x1

    goto :goto_0

    .line 410
    .end local v2    # "i":I
    :cond_2
    iget-object v2, p0, Lcom/miui/server/rtboost/SchedBoostService;->mBoostingMap:Lcom/miui/server/rtboost/SchedBoostService$BoostingPackageMap;

    invoke-virtual {v2}, Lcom/miui/server/rtboost/SchedBoostService$BoostingPackageMap;->clear()V

    .line 411
    .end local v1    # "list":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/miui/server/rtboost/SchedBoostService$BoostThreadInfo;>;"
    monitor-exit v0

    goto :goto_1

    :catchall_0
    move-exception v1

    monitor-exit v0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v1

    .line 414
    :cond_3
    :goto_1
    iput-object p4, p0, Lcom/miui/server/rtboost/SchedBoostService;->mPreBoostProcessName:Ljava/lang/String;

    .line 416
    nop

    .line 417
    invoke-direct/range {p0 .. p5}, Lcom/miui/server/rtboost/SchedBoostService;->getCurrentBoostThreadsList([IJLjava/lang/String;I)Ljava/util/ArrayList;

    move-result-object v0

    .line 420
    .local v0, "curList":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/miui/server/rtboost/SchedBoostService$BoostThreadInfo;>;"
    if-eqz v0, :cond_4

    .line 421
    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v1

    .line 422
    .local v1, "curListLength":I
    const/4 v2, 0x0

    .restart local v2    # "i":I
    :goto_2
    if-ge v2, v1, :cond_4

    .line 423
    iget-object v3, p0, Lcom/miui/server/rtboost/SchedBoostService;->mHandler:Lcom/miui/server/rtboost/SchedBoostService$Handler;

    invoke-virtual {v0, v2}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lcom/miui/server/rtboost/SchedBoostService$BoostThreadInfo;

    invoke-virtual {v3, v4}, Lcom/miui/server/rtboost/SchedBoostService$Handler;->obtainSchedThreadsMsg(Lcom/miui/server/rtboost/SchedBoostService$BoostThreadInfo;)Landroid/os/Message;

    move-result-object v3

    .line 424
    invoke-virtual {v3}, Landroid/os/Message;->sendToTarget()V

    .line 422
    add-int/lit8 v2, v2, 0x1

    goto :goto_2

    .line 428
    .end local v1    # "curListLength":I
    .end local v2    # "i":I
    :cond_4
    sget-boolean v1, Lcom/miui/server/rtboost/SchedBoostService;->IS_MTK_DEVICE:Z

    if-eqz v1, :cond_6

    sget-boolean v1, Lcom/miui/server/rtboost/SchedBoostService;->IS_SCHED_HOME_GPU_THREADS_ENABLED:Z

    if-eqz v1, :cond_6

    iget-boolean v1, p0, Lcom/miui/server/rtboost/SchedBoostService;->mPendingHomeGpuTids:Z

    if-eqz v1, :cond_6

    if-nez p5, :cond_6

    .line 430
    invoke-virtual {p4}, Ljava/lang/String;->isEmpty()Z

    move-result v1

    if-nez v1, :cond_6

    const/4 v1, 0x0

    aget v2, p1, v1

    if-lez v2, :cond_6

    iget v2, p0, Lcom/miui/server/rtboost/SchedBoostService;->mBoostHomeGpuTidsTryCnt:I

    const/4 v3, 0x3

    if-ge v2, v3, :cond_6

    const-string v2, "com.miui.home"

    .line 432
    invoke-virtual {v2, p4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_5

    const-string v2, "com.mi.android.globallauncher"

    .line 433
    invoke-virtual {v2, p4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_6

    .line 434
    :cond_5
    aget v1, p1, v1

    invoke-direct {p0, v1, p4}, Lcom/miui/server/rtboost/SchedBoostService;->schedGpuThreads(ILjava/lang/String;)V

    .line 436
    :cond_6
    return-void
.end method

.method private handleBoostFreq([IJLjava/lang/String;I)V
    .locals 3
    .param p1, "tids"    # [I
    .param p2, "duration"    # J
    .param p4, "procName"    # Ljava/lang/String;
    .param p5, "mode"    # I

    .line 610
    sget-boolean v0, Lcom/miui/server/rtboost/SchedBoostService;->IS_MTK_DEVICE:Z

    if-eqz v0, :cond_0

    .line 611
    iget-object v0, p0, Lcom/miui/server/rtboost/SchedBoostService;->mMTKPerfStub:Landroid/perf/PerfMTKStub;

    long-to-int v1, p2

    iget-object v2, p0, Lcom/miui/server/rtboost/SchedBoostService;->mDefultPerfList:[I

    invoke-interface {v0, v1, v2}, Landroid/perf/PerfMTKStub;->perfLockAcquire(I[I)I

    .line 612
    return-void

    .line 615
    :cond_0
    const/4 v0, 0x1

    packed-switch p5, :pswitch_data_0

    .line 625
    const-string v0, "SchedBoost"

    const-string v1, "BoostFreq mode can not match with COLD_LAUNCH or WARM_LAUNCH"

    invoke-static {v0, v1}, Landroid/util/Slog;->d(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    .line 617
    :pswitch_0
    iget-object v1, p0, Lcom/miui/server/rtboost/SchedBoostService;->mQcomPerfStub:Landroid/perf/PerfStub;

    long-to-int v2, p2

    invoke-virtual {v1, p4, v2, v0}, Landroid/perf/PerfStub;->perfColdLaunchBoost(Ljava/lang/String;II)I

    .line 619
    goto :goto_0

    .line 621
    :pswitch_1
    iget-object v1, p0, Lcom/miui/server/rtboost/SchedBoostService;->mQcomPerfStub:Landroid/perf/PerfStub;

    long-to-int v2, p2

    invoke-virtual {v1, p4, v2, v0}, Landroid/perf/PerfStub;->perfWarmLaunchBoost(Ljava/lang/String;II)I

    .line 623
    nop

    .line 627
    :goto_0
    return-void

    :pswitch_data_0
    .packed-switch 0x65
        :pswitch_1
        :pswitch_0
    .end packed-switch
.end method

.method private handleReSetThreadsFIFO(IJI)V
    .locals 2
    .param p1, "tid"    # I
    .param p2, "duration"    # J
    .param p4, "mode"    # I

    .line 573
    iget-object v0, p0, Lcom/miui/server/rtboost/SchedBoostService;->mHandler:Lcom/miui/server/rtboost/SchedBoostService$Handler;

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/miui/server/rtboost/SchedBoostService$Handler;->removeReSetThreadsFIFOMsg(Ljava/lang/Integer;)V

    .line 575
    sget-boolean v0, Lcom/miui/server/rtboost/SchedBoostService;->DEBUG:Z

    if-eqz v0, :cond_0

    .line 576
    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    filled-new-array {v0}, [Ljava/lang/Object;

    move-result-object v0

    const-string v1, "handleReSetThreadsFIFO, threads: %s"

    invoke-static {v1, v0}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    const-string v1, "SchedBoost"

    invoke-static {v1, v0}, Landroid/util/Slog;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 578
    :cond_0
    iget-object v0, p0, Lcom/miui/server/rtboost/SchedBoostService;->mRtTids:Landroid/util/ArraySet;

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/util/ArraySet;->contains(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 579
    const/4 v0, 0x1

    invoke-static {p1, v0}, Lcom/android/server/am/ActivityManagerService;->scheduleAsRegularPriority(IZ)Z

    .line 580
    iget-object v0, p0, Lcom/miui/server/rtboost/SchedBoostService;->mRtTids:Landroid/util/ArraySet;

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/util/ArraySet;->remove(Ljava/lang/Object;)Z

    .line 582
    :cond_1
    return-void
.end method

.method private handleResetSchedThread(Lcom/miui/server/rtboost/SchedBoostService$BoostThreadInfo;)V
    .locals 11
    .param p1, "threadInfo"    # Lcom/miui/server/rtboost/SchedBoostService$BoostThreadInfo;

    .line 496
    const/4 v0, 0x0

    const-string v1, "SchedBoost"

    if-nez p1, :cond_0

    .line 497
    const-string v2, "handleResetSchedThread err: threadInfo null object"

    new-array v0, v0, [Ljava/lang/Object;

    invoke-static {v2, v0}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    invoke-static {v1, v0}, Landroid/util/Slog;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 498
    return-void

    .line 501
    :cond_0
    invoke-direct {p0}, Lcom/miui/server/rtboost/SchedBoostService;->stopBoostInternal()V

    .line 504
    iget-wide v2, p1, Lcom/miui/server/rtboost/SchedBoostService$BoostThreadInfo;->boostStartTime:J

    .line 505
    .local v2, "boostStartTime":J
    const-wide/16 v4, 0x0

    cmp-long v4, v2, v4

    if-eqz v4, :cond_a

    .line 506
    invoke-static {}, Landroid/os/SystemClock;->uptimeMillis()J

    move-result-wide v4

    sub-long/2addr v4, v2

    .line 511
    .local v4, "boostDuration":J
    iget-object v6, p1, Lcom/miui/server/rtboost/SchedBoostService$BoostThreadInfo;->procName:Ljava/lang/String;

    if-eqz v6, :cond_1

    iget-object v6, p1, Lcom/miui/server/rtboost/SchedBoostService$BoostThreadInfo;->procName:Ljava/lang/String;

    iget-object v7, p0, Lcom/miui/server/rtboost/SchedBoostService;->mPreBoostProcessName:Ljava/lang/String;

    invoke-virtual {v6, v7}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-eqz v6, :cond_1

    .line 512
    iget-object v6, p0, Lcom/miui/server/rtboost/SchedBoostService;->mBoostingMap:Lcom/miui/server/rtboost/SchedBoostService$BoostingPackageMap;

    iget v7, p1, Lcom/miui/server/rtboost/SchedBoostService$BoostThreadInfo;->tid:I

    invoke-virtual {v6, v7}, Lcom/miui/server/rtboost/SchedBoostService$BoostingPackageMap;->removeByTid(I)V

    .line 513
    iget-object v6, p0, Lcom/miui/server/rtboost/SchedBoostService;->mHandler:Lcom/miui/server/rtboost/SchedBoostService$Handler;

    invoke-virtual {v6, p1}, Lcom/miui/server/rtboost/SchedBoostService$Handler;->removeResetThreadsMsg(Lcom/miui/server/rtboost/SchedBoostService$BoostThreadInfo;)V

    .line 516
    :cond_1
    iget v6, p1, Lcom/miui/server/rtboost/SchedBoostService$BoostThreadInfo;->tid:I

    .line 517
    .local v6, "tid":I
    sget-object v7, Lcom/miui/server/rtboost/SchedBoostService;->ALL_CPU_CORES:[I

    invoke-direct {p0, p1, v0, v0, v7}, Lcom/miui/server/rtboost/SchedBoostService;->setAffinityAndPriority(Lcom/miui/server/rtboost/SchedBoostService$BoostThreadInfo;ZZ[I)V

    .line 518
    const/16 v7, 0x400

    invoke-direct {p0, v6, v0, v7}, Lcom/miui/server/rtboost/SchedBoostService;->setTaskUclamp(III)V

    .line 520
    sget-boolean v0, Lcom/miui/server/rtboost/SchedBoostService;->IS_UIGROUP_ENABLED:Z

    const/4 v7, 0x1

    if-eqz v0, :cond_8

    .line 522
    :try_start_0
    invoke-static {v6}, Landroid/os/Process;->getCpusetThreadGroup(I)I

    move-result v0

    const/16 v8, 0xa

    if-ne v0, v8, :cond_7

    .line 523
    sget-boolean v0, Lcom/miui/server/rtboost/SchedBoostService;->DEBUG:Z
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    const-string v8, "reset tid "

    if-eqz v0, :cond_2

    :try_start_1
    new-instance v9, Ljava/lang/StringBuilder;

    invoke-direct {v9}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v9, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9, v6}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v9

    const-string v10, " group, procName="

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    iget-object v10, p1, Lcom/miui/server/rtboost/SchedBoostService$BoostThreadInfo;->procName:Ljava/lang/String;

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v9

    invoke-static {v1, v9}, Landroid/util/Slog;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 525
    :cond_2
    const-string v9, "com.miui.home"

    iget-object v10, p1, Lcom/miui/server/rtboost/SchedBoostService$BoostThreadInfo;->procName:Ljava/lang/String;

    invoke-virtual {v9, v10}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v9

    if-nez v9, :cond_5

    const-string v9, "com.mi.android.globallauncher"

    iget-object v10, p1, Lcom/miui/server/rtboost/SchedBoostService$BoostThreadInfo;->procName:Ljava/lang/String;

    .line 526
    invoke-virtual {v9, v10}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v9

    if-eqz v9, :cond_3

    goto :goto_0

    .line 530
    :cond_3
    if-eqz v0, :cond_4

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v0, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, v6}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v8, " group to top-app"

    invoke-virtual {v0, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v1, v0}, Landroid/util/Slog;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 531
    :cond_4
    const/4 v0, 0x5

    invoke-direct {p0, v6, v0}, Lcom/miui/server/rtboost/SchedBoostService;->setTaskGroup(II)V

    goto :goto_1

    .line 527
    :cond_5
    :goto_0
    if-eqz v0, :cond_6

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v0, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, v6}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v8, " group to foreground"

    invoke-virtual {v0, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v1, v0}, Landroid/util/Slog;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 528
    :cond_6
    invoke-direct {p0, v6, v7}, Lcom/miui/server/rtboost/SchedBoostService;->setTaskGroup(II)V
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_0

    .line 536
    :cond_7
    :goto_1
    goto :goto_2

    .line 534
    :catch_0
    move-exception v0

    .line 535
    .local v0, "e":Ljava/lang/Exception;
    new-instance v8, Ljava/lang/StringBuilder;

    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    const-string v9, "fail find and move tid "

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8, v6}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v8

    const-string/jumbo v9, "to foreground or top-app"

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    invoke-static {v1, v8}, Landroid/util/Slog;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 539
    .end local v0    # "e":Ljava/lang/Exception;
    :cond_8
    :goto_2
    iget-object v0, p0, Lcom/miui/server/rtboost/SchedBoostService;->mHandlerThread:Landroid/os/HandlerThread;

    invoke-virtual {v0}, Landroid/os/HandlerThread;->getThreadId()I

    move-result v0

    invoke-static {v0, v7}, Lcom/android/server/am/ActivityManagerService;->scheduleAsFifoPriority(IZ)Z

    .line 540
    sget-boolean v0, Lcom/miui/server/rtboost/SchedBoostService;->DEBUG:Z

    if-eqz v0, :cond_9

    .line 541
    nop

    .line 542
    invoke-static {v6}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    invoke-static {v4, v5}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v7

    iget-object v8, p1, Lcom/miui/server/rtboost/SchedBoostService$BoostThreadInfo;->procName:Ljava/lang/String;

    iget v9, p1, Lcom/miui/server/rtboost/SchedBoostService$BoostThreadInfo;->mode:I

    .line 543
    invoke-static {v9}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v9

    sget-object v10, Lcom/miui/server/rtboost/SchedBoostService;->ALL_CPU_CORES:[I

    invoke-static {v10}, Ljava/util/Arrays;->toString([I)Ljava/lang/String;

    move-result-object v10

    filled-new-array {v0, v7, v8, v9, v10}, [Ljava/lang/Object;

    move-result-object v0

    .line 541
    const-string v7, "handleSchedThreads end: threads: %s, %sms, procName: %s, mode: %s aff:%s"

    invoke-static {v7, v0}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    invoke-static {v1, v0}, Landroid/util/Slog;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 545
    :cond_9
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "SchedBoost: "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p1, Lcom/miui/server/rtboost/SchedBoostService$BoostThreadInfo;->procName:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    iget v1, p1, Lcom/miui/server/rtboost/SchedBoostService$BoostThreadInfo;->tid:I

    const-wide/16 v7, 0x40

    invoke-static {v7, v8, v0, v1}, Landroid/os/Trace;->asyncTraceEnd(JLjava/lang/String;I)V

    .line 547
    return-void

    .line 508
    .end local v4    # "boostDuration":J
    .end local v6    # "tid":I
    :cond_a
    return-void
.end method

.method private handleSchedThread(Lcom/miui/server/rtboost/SchedBoostService$BoostThreadInfo;)V
    .locals 11
    .param p1, "threadInfo"    # Lcom/miui/server/rtboost/SchedBoostService$BoostThreadInfo;

    .line 439
    const-string v0, "SchedBoost"

    if-nez p1, :cond_0

    .line 440
    const/4 v1, 0x0

    new-array v1, v1, [Ljava/lang/Object;

    const-string v2, "handleSchedThreads err: threadInfo null object"

    invoke-static {v2, v1}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Slog;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 441
    return-void

    .line 443
    :cond_0
    iget-object v1, p0, Lcom/miui/server/rtboost/SchedBoostService;->mHandler:Lcom/miui/server/rtboost/SchedBoostService$Handler;

    invoke-virtual {v1, p1}, Lcom/miui/server/rtboost/SchedBoostService$Handler;->removeResetThreadsMsg(Lcom/miui/server/rtboost/SchedBoostService$BoostThreadInfo;)V

    .line 444
    iget-object v1, p0, Lcom/miui/server/rtboost/SchedBoostService;->mHandler:Lcom/miui/server/rtboost/SchedBoostService$Handler;

    invoke-virtual {v1, p1}, Lcom/miui/server/rtboost/SchedBoostService$Handler;->obtainResetSchedThreadsMsg(Lcom/miui/server/rtboost/SchedBoostService$BoostThreadInfo;)Landroid/os/Message;

    move-result-object v1

    .line 445
    .local v1, "resetMsg":Landroid/os/Message;
    iget-object v2, p0, Lcom/miui/server/rtboost/SchedBoostService;->mHandler:Lcom/miui/server/rtboost/SchedBoostService$Handler;

    iget-wide v3, p1, Lcom/miui/server/rtboost/SchedBoostService$BoostThreadInfo;->duration:J

    invoke-virtual {v2, v1, v3, v4}, Lcom/miui/server/rtboost/SchedBoostService$Handler;->sendMessageDelayed(Landroid/os/Message;J)Z

    .line 448
    iget-wide v2, p1, Lcom/miui/server/rtboost/SchedBoostService$BoostThreadInfo;->duration:J

    const-wide/16 v4, 0x0

    cmp-long v2, v2, v4

    if-nez v2, :cond_1

    .line 449
    return-void

    .line 452
    :cond_1
    sget-object v2, Lcom/miui/server/rtboost/SchedBoostService;->PLATFORM_NAME:Ljava/lang/String;

    const-string v3, "pineapple"

    invoke-static {v2, v3}, Landroid/text/TextUtils;->equals(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Z

    move-result v2

    if-eqz v2, :cond_2

    .line 453
    invoke-direct {p0, p1}, Lcom/miui/server/rtboost/SchedBoostService;->startBoostInternalWithOpcode(Lcom/miui/server/rtboost/SchedBoostService$BoostThreadInfo;)Z

    goto :goto_0

    .line 455
    :cond_2
    invoke-direct {p0, p1}, Lcom/miui/server/rtboost/SchedBoostService;->startBoostInternal(Lcom/miui/server/rtboost/SchedBoostService$BoostThreadInfo;)Z

    .line 458
    :goto_0
    iget v2, p1, Lcom/miui/server/rtboost/SchedBoostService$BoostThreadInfo;->tid:I

    .line 459
    .local v2, "tid":I
    iget-wide v6, p1, Lcom/miui/server/rtboost/SchedBoostService$BoostThreadInfo;->boostStartTime:J

    .line 461
    .local v6, "boostStartTime":J
    cmp-long v3, v6, v4

    if-eqz v3, :cond_4

    .line 462
    sget-boolean v3, Lcom/miui/server/rtboost/SchedBoostService;->DEBUG:Z

    if-eqz v3, :cond_3

    .line 463
    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    filled-new-array {v3}, [Ljava/lang/Object;

    move-result-object v3

    const-string v4, "handleSchedThreads continue: threads: %s"

    invoke-static {v4, v3}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v3

    invoke-static {v0, v3}, Landroid/util/Slog;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 465
    :cond_3
    return-void

    .line 467
    :cond_4
    invoke-static {}, Landroid/os/SystemClock;->uptimeMillis()J

    move-result-wide v3

    iput-wide v3, p1, Lcom/miui/server/rtboost/SchedBoostService$BoostThreadInfo;->boostStartTime:J

    .line 468
    iget v3, p1, Lcom/miui/server/rtboost/SchedBoostService$BoostThreadInfo;->tid:I

    invoke-static {v3, v0}, Landroid/os/MiuiProcess;->getThreadPriority(ILjava/lang/String;)I

    move-result v3

    iput v3, p1, Lcom/miui/server/rtboost/SchedBoostService$BoostThreadInfo;->savedPriority:I

    .line 470
    const/4 v3, 0x0

    .line 471
    .local v3, "forkPriority":Z
    sget-object v4, Landroid/os/MiuiProcess;->BIG_CORES_INDEX:[I

    .line 472
    .local v4, "coresIndex":[I
    iget v5, p1, Lcom/miui/server/rtboost/SchedBoostService$BoostThreadInfo;->tid:I

    iget v8, p0, Lcom/miui/server/rtboost/SchedBoostService;->mHomeRenderThreadId:I

    if-eq v5, v8, :cond_5

    iget v5, p1, Lcom/miui/server/rtboost/SchedBoostService$BoostThreadInfo;->tid:I

    iget v8, p0, Lcom/miui/server/rtboost/SchedBoostService;->mSystemUIRenderThreadId:I

    if-ne v5, v8, :cond_6

    .line 474
    :cond_5
    const/4 v3, 0x1

    .line 476
    :cond_6
    sget-object v5, Landroid/os/MiuiProcess;->BIG_PRIME_CORES_INDEX:[I

    if-eqz v5, :cond_7

    iget v5, p1, Lcom/miui/server/rtboost/SchedBoostService$BoostThreadInfo;->mode:I

    if-nez v5, :cond_7

    .line 478
    sget-object v4, Landroid/os/MiuiProcess;->BIG_PRIME_CORES_INDEX:[I

    .line 480
    :cond_7
    const/4 v5, 0x1

    invoke-direct {p0, p1, v5, v3, v4}, Lcom/miui/server/rtboost/SchedBoostService;->setAffinityAndPriority(Lcom/miui/server/rtboost/SchedBoostService$BoostThreadInfo;ZZ[I)V

    .line 481
    iget v5, p0, Lcom/miui/server/rtboost/SchedBoostService;->TASK_UCLAMP_MIN:I

    iget v8, p0, Lcom/miui/server/rtboost/SchedBoostService;->TASK_UCLAMP_MAX:I

    invoke-direct {p0, v2, v5, v8}, Lcom/miui/server/rtboost/SchedBoostService;->setTaskUclamp(III)V

    .line 482
    const/16 v5, 0xa

    invoke-direct {p0, v2, v5}, Lcom/miui/server/rtboost/SchedBoostService;->setTaskGroup(II)V

    .line 484
    iget v5, p1, Lcom/miui/server/rtboost/SchedBoostService$BoostThreadInfo;->tid:I

    invoke-static {v5, v0}, Landroid/os/MiuiProcess;->getThreadPriority(ILjava/lang/String;)I

    move-result v5

    iput v5, p1, Lcom/miui/server/rtboost/SchedBoostService$BoostThreadInfo;->boostPriority:I

    .line 486
    sget-boolean v5, Lcom/miui/server/rtboost/SchedBoostService;->DEBUG:Z

    if-eqz v5, :cond_8

    .line 487
    nop

    .line 488
    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    iget-object v8, p1, Lcom/miui/server/rtboost/SchedBoostService$BoostThreadInfo;->procName:Ljava/lang/String;

    iget v9, p1, Lcom/miui/server/rtboost/SchedBoostService$BoostThreadInfo;->mode:I

    invoke-static {v9}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v9

    .line 489
    invoke-static {v4}, Ljava/util/Arrays;->toString([I)Ljava/lang/String;

    move-result-object v10

    filled-new-array {v5, v8, v9, v10}, [Ljava/lang/Object;

    move-result-object v5

    .line 487
    const-string v8, "handleSchedThreads begin: threads: %s, procName: %s, mode: %s, aff:%s"

    invoke-static {v8, v5}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v5

    invoke-static {v0, v5}, Landroid/util/Slog;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 491
    :cond_8
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "SchedBoost: "

    invoke-virtual {v0, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v5, p1, Lcom/miui/server/rtboost/SchedBoostService$BoostThreadInfo;->procName:Ljava/lang/String;

    invoke-virtual {v0, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    iget v5, p1, Lcom/miui/server/rtboost/SchedBoostService$BoostThreadInfo;->tid:I

    const-wide/16 v8, 0x40

    invoke-static {v8, v9, v0, v5}, Landroid/os/Trace;->asyncTraceBegin(JLjava/lang/String;I)V

    .line 493
    return-void
.end method

.method private handleSetThreadsAlwaysFIFO([I)V
    .locals 5
    .param p1, "tids"    # [I

    .line 550
    array-length v0, p1

    const/4 v1, 0x0

    :goto_0
    if-ge v1, v0, :cond_2

    aget v2, p1, v1

    .line 551
    .local v2, "tid":I
    if-lez v2, :cond_1

    .line 552
    sget-boolean v3, Lcom/miui/server/rtboost/SchedBoostService;->DEBUG:Z

    if-eqz v3, :cond_0

    .line 553
    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    filled-new-array {v3}, [Ljava/lang/Object;

    move-result-object v3

    const-string v4, "handleSetThreadsAlwaysFIFO, threads: %s"

    invoke-static {v4, v3}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v3

    const-string v4, "SchedBoost"

    invoke-static {v4, v3}, Landroid/util/Slog;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 555
    :cond_0
    const/4 v3, 0x1

    invoke-static {v2, v3}, Lcom/android/server/am/ActivityManagerService;->scheduleAsFifoPriority(IZ)Z

    .line 556
    iget-object v3, p0, Lcom/miui/server/rtboost/SchedBoostService;->mAlwaysRtTids:Landroid/util/ArraySet;

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    invoke-virtual {v3, v4}, Landroid/util/ArraySet;->add(Ljava/lang/Object;)Z

    .line 550
    .end local v2    # "tid":I
    :cond_1
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 559
    :cond_2
    return-void
.end method

.method private handleSetThreadsFIFO(IJI)V
    .locals 3
    .param p1, "tid"    # I
    .param p2, "duration"    # J
    .param p4, "mode"    # I

    .line 562
    iget-object v0, p0, Lcom/miui/server/rtboost/SchedBoostService;->mHandler:Lcom/miui/server/rtboost/SchedBoostService$Handler;

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/miui/server/rtboost/SchedBoostService$Handler;->removeReSetThreadsFIFOMsg(Ljava/lang/Integer;)V

    .line 563
    iget-object v0, p0, Lcom/miui/server/rtboost/SchedBoostService;->mHandler:Lcom/miui/server/rtboost/SchedBoostService$Handler;

    invoke-virtual {v0, p1, p2, p3, p4}, Lcom/miui/server/rtboost/SchedBoostService$Handler;->obtainReSetThreadsFIFOMsg(IJI)Landroid/os/Message;

    move-result-object v0

    .line 564
    .local v0, "resetMsg":Landroid/os/Message;
    iget-object v1, p0, Lcom/miui/server/rtboost/SchedBoostService;->mHandler:Lcom/miui/server/rtboost/SchedBoostService$Handler;

    invoke-virtual {v1, v0, p2, p3}, Lcom/miui/server/rtboost/SchedBoostService$Handler;->sendMessageDelayed(Landroid/os/Message;J)Z

    .line 565
    sget-boolean v1, Lcom/miui/server/rtboost/SchedBoostService;->DEBUG:Z

    if-eqz v1, :cond_0

    .line 566
    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    filled-new-array {v1}, [Ljava/lang/Object;

    move-result-object v1

    const-string v2, "handleSetThreadsFIFO, threads: %s"

    invoke-static {v2, v1}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    const-string v2, "SchedBoost"

    invoke-static {v2, v1}, Landroid/util/Slog;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 568
    :cond_0
    const/4 v1, 0x3

    const/4 v2, 0x1

    invoke-static {p1, v1, v2}, Landroid/os/MiuiProcess;->scheduleAsFifoPriority(IIZ)Z

    .line 569
    iget-object v1, p0, Lcom/miui/server/rtboost/SchedBoostService;->mRtTids:Landroid/util/ArraySet;

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v1, v2}, Landroid/util/ArraySet;->add(Ljava/lang/Object;)Z

    .line 570
    return-void
.end method

.method private handleThreadsBindBigCoreMsg(Ljava/lang/Integer;J)V
    .locals 3
    .param p1, "tid"    # Ljava/lang/Integer;
    .param p2, "duration"    # J

    .line 585
    iget-object v0, p0, Lcom/miui/server/rtboost/SchedBoostService;->mHandler:Lcom/miui/server/rtboost/SchedBoostService$Handler;

    invoke-virtual {v0, p1}, Lcom/miui/server/rtboost/SchedBoostService$Handler;->removeUnBindBigCoreMsg(Ljava/lang/Integer;)V

    .line 586
    iget-object v0, p0, Lcom/miui/server/rtboost/SchedBoostService;->mHandler:Lcom/miui/server/rtboost/SchedBoostService$Handler;

    invoke-virtual {v0, p1}, Lcom/miui/server/rtboost/SchedBoostService$Handler;->obtainThreadUnBindBigCoreMsg(Ljava/lang/Integer;)Landroid/os/Message;

    move-result-object v0

    .line 587
    .local v0, "resetMsg":Landroid/os/Message;
    iget-object v1, p0, Lcom/miui/server/rtboost/SchedBoostService;->mHandler:Lcom/miui/server/rtboost/SchedBoostService$Handler;

    invoke-virtual {v1, v0, p2, p3}, Lcom/miui/server/rtboost/SchedBoostService$Handler;->sendMessageDelayed(Landroid/os/Message;J)Z

    .line 589
    invoke-virtual {p1}, Ljava/lang/Integer;->intValue()I

    move-result v1

    sget-object v2, Landroid/os/MiuiProcess;->BIG_CORES_INDEX:[I

    invoke-direct {p0, v1, v2}, Lcom/miui/server/rtboost/SchedBoostService;->setSchedAffinity(I[I)V

    .line 591
    sget-boolean v1, Lcom/miui/server/rtboost/SchedBoostService;->DEBUG:Z

    if-eqz v1, :cond_0

    .line 592
    invoke-virtual {p1}, Ljava/lang/Integer;->intValue()I

    move-result v1

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    sget-object v2, Landroid/os/MiuiProcess;->BIG_CORES_INDEX:[I

    .line 593
    invoke-static {v2}, Ljava/util/Arrays;->toString([I)Ljava/lang/String;

    move-result-object v2

    filled-new-array {v1, v2}, [Ljava/lang/Object;

    move-result-object v1

    .line 592
    const-string v2, "BindBigCore, threads: %s, aff:%s"

    invoke-static {v2, v1}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    const-string v2, "SchedBoost"

    invoke-static {v2, v1}, Landroid/util/Slog;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 595
    :cond_0
    return-void
.end method

.method private handleThreadsUnBindBigCoreMsg(Ljava/lang/Integer;)V
    .locals 2
    .param p1, "tid"    # Ljava/lang/Integer;

    .line 598
    if-nez p1, :cond_0

    .line 599
    return-void

    .line 600
    :cond_0
    iget-object v0, p0, Lcom/miui/server/rtboost/SchedBoostService;->mHandler:Lcom/miui/server/rtboost/SchedBoostService$Handler;

    invoke-virtual {v0, p1}, Lcom/miui/server/rtboost/SchedBoostService$Handler;->removeUnBindBigCoreMsg(Ljava/lang/Integer;)V

    .line 601
    invoke-virtual {p1}, Ljava/lang/Integer;->intValue()I

    move-result v0

    sget-object v1, Lcom/miui/server/rtboost/SchedBoostService;->ALL_CPU_CORES:[I

    invoke-direct {p0, v0, v1}, Lcom/miui/server/rtboost/SchedBoostService;->setSchedAffinity(I[I)V

    .line 603
    sget-boolean v0, Lcom/miui/server/rtboost/SchedBoostService;->DEBUG:Z

    if-eqz v0, :cond_1

    .line 604
    invoke-virtual {p1}, Ljava/lang/Integer;->intValue()I

    move-result v0

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    sget-object v1, Lcom/miui/server/rtboost/SchedBoostService;->ALL_CPU_CORES:[I

    .line 605
    invoke-static {v1}, Ljava/util/Arrays;->toString([I)Ljava/lang/String;

    move-result-object v1

    filled-new-array {v0, v1}, [Ljava/lang/Object;

    move-result-object v0

    .line 604
    const-string/jumbo v1, "unBindBigCore, threads: %s, aff:%s"

    invoke-static {v1, v0}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    const-string v1, "SchedBoost"

    invoke-static {v1, v0}, Landroid/util/Slog;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 607
    :cond_1
    return-void
.end method

.method private init()Z
    .locals 5

    .line 279
    sget-boolean v0, Lcom/miui/server/rtboost/SchedBoostService;->IS_MTK_DEVICE:Z

    if-eqz v0, :cond_0

    .line 280
    invoke-static {}, Landroid/perf/PerfMTKStub;->newInstance()Landroid/perf/PerfMTKStub;

    move-result-object v0

    iput-object v0, p0, Lcom/miui/server/rtboost/SchedBoostService;->mMTKPerfStub:Landroid/perf/PerfMTKStub;

    .line 281
    invoke-static {}, Landroid/perf/PerfMTKStub;->newInstance()Landroid/perf/PerfMTKStub;

    move-result-object v0

    invoke-interface {v0}, Landroid/perf/PerfMTKStub;->getBoostParamByDevice()[I

    move-result-object v0

    iput-object v0, p0, Lcom/miui/server/rtboost/SchedBoostService;->mDefultPerfList:[I

    .line 282
    sget-object v0, Lcom/miui/server/rtboost/SchedBoostService;->MTK_GPU_AND_CPU_BOOST_LIST:[I

    iput-object v0, p0, Lcom/miui/server/rtboost/SchedBoostService;->mGpuAndCpuPerfList:[I

    goto :goto_0

    .line 284
    :cond_0
    invoke-static {}, Landroid/perf/PerfStub;->newInstance()Landroid/perf/PerfStub;

    move-result-object v0

    iput-object v0, p0, Lcom/miui/server/rtboost/SchedBoostService;->mQcomPerfStub:Landroid/perf/PerfStub;

    .line 285
    sget-object v0, Lcom/miui/server/rtboost/SchedBoostService;->DEFAULT_QCOM_PERF_LIST:[I

    iput-object v0, p0, Lcom/miui/server/rtboost/SchedBoostService;->mDefultPerfList:[I

    .line 286
    sget-object v0, Lcom/miui/server/rtboost/SchedBoostService;->QCOM_GPU_AND_CPU_BOOST_LIST:[I

    iput-object v0, p0, Lcom/miui/server/rtboost/SchedBoostService;->mGpuAndCpuPerfList:[I

    .line 288
    :goto_0
    const-class v0, Landroid/os/Process;

    const-string/jumbo v1, "setSchedAffinity"

    sget-object v2, Ljava/lang/Integer;->TYPE:Ljava/lang/Class;

    const-class v3, [I

    filled-new-array {v2, v3}, [Ljava/lang/Class;

    move-result-object v2

    invoke-static {v0, v1, v2}, Lmiui/util/ReflectionUtils;->tryFindMethodBestMatch(Ljava/lang/Class;Ljava/lang/String;[Ljava/lang/Class;)Ljava/lang/reflect/Method;

    move-result-object v0

    iput-object v0, p0, Lcom/miui/server/rtboost/SchedBoostService;->mMethodSetAffinity:Ljava/lang/reflect/Method;

    .line 290
    if-nez v0, :cond_1

    .line 291
    const-string v0, "SchedBoost"

    const-string v1, "failed to find setSchedAffinity method"

    invoke-static {v0, v1}, Landroid/util/Slog;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 292
    const/4 v0, 0x0

    return v0

    .line 295
    :cond_1
    const-class v0, Landroid/os/Process;

    const-string/jumbo v1, "setTaskUclamp"

    sget-object v2, Ljava/lang/Integer;->TYPE:Ljava/lang/Class;

    sget-object v3, Ljava/lang/Integer;->TYPE:Ljava/lang/Class;

    sget-object v4, Ljava/lang/Integer;->TYPE:Ljava/lang/Class;

    filled-new-array {v2, v3, v4}, [Ljava/lang/Class;

    move-result-object v2

    invoke-static {v0, v1, v2}, Lmiui/util/ReflectionUtils;->tryFindMethodBestMatch(Ljava/lang/Class;Ljava/lang/String;[Ljava/lang/Class;)Ljava/lang/reflect/Method;

    move-result-object v0

    iput-object v0, p0, Lcom/miui/server/rtboost/SchedBoostService;->mMethodUclampTask:Ljava/lang/reflect/Method;

    .line 297
    const-class v0, Lcom/miui/server/rtboost/SchedBoostService;

    monitor-enter v0

    .line 298
    :try_start_0
    iget-boolean v1, p0, Lcom/miui/server/rtboost/SchedBoostService;->isInited:Z

    const/4 v2, 0x1

    if-nez v1, :cond_2

    .line 299
    iput-boolean v2, p0, Lcom/miui/server/rtboost/SchedBoostService;->isInited:Z

    .line 301
    :cond_2
    monitor-exit v0

    .line 302
    return v2

    .line 301
    :catchall_0
    move-exception v1

    monitor-exit v0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v1
.end method

.method private synthetic lambda$new$0(I)V
    .locals 0
    .param p1, "temp"    # I

    .line 269
    invoke-direct {p0, p1}, Lcom/miui/server/rtboost/SchedBoostService;->changeSchedBoostPolicy(I)V

    return-void
.end method

.method private readCmdlineFromProcfs(Ljava/lang/String;)Ljava/lang/String;
    .locals 3
    .param p1, "filePath"    # Ljava/lang/String;

    .line 673
    const/4 v0, 0x1

    new-array v0, v0, [Ljava/lang/String;

    .line 674
    .local v0, "cmdline":[Ljava/lang/String;
    sget-object v1, Lcom/miui/server/rtboost/SchedBoostService;->CMDLINE_OUT:[I

    const/4 v2, 0x0

    invoke-static {p1, v1, v0, v2, v2}, Landroid/os/Process;->readProcFile(Ljava/lang/String;[I[Ljava/lang/String;[J[F)Z

    move-result v1

    if-nez v1, :cond_0

    .line 675
    const-string v1, ""

    return-object v1

    .line 677
    :cond_0
    const/4 v1, 0x0

    aget-object v1, v0, v1

    return-object v1
.end method

.method private schedGpuThreads(ILjava/lang/String;)V
    .locals 16
    .param p1, "pid"    # I
    .param p2, "procName"    # Ljava/lang/String;

    .line 642
    move-object/from16 v6, p0

    move/from16 v7, p1

    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    move-object v8, v0

    .line 643
    .local v8, "targetTids":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/Integer;>;"
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "/proc/"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, v7}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v2, "/task"

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    .line 645
    .local v0, "filePath":Ljava/lang/String;
    const/16 v2, 0x400

    new-array v2, v2, [I

    .line 646
    .local v2, "tids":[I
    invoke-static {v0, v2}, Landroid/os/Process;->getPids(Ljava/lang/String;[I)[I

    move-result-object v9

    .line 648
    .end local v2    # "tids":[I
    .local v9, "tids":[I
    if-eqz v9, :cond_6

    array-length v2, v9

    if-nez v2, :cond_0

    goto/16 :goto_4

    .line 649
    :cond_0
    array-length v2, v9

    move-object v11, v0

    const/4 v0, 0x0

    .end local v0    # "filePath":Ljava/lang/String;
    .local v11, "filePath":Ljava/lang/String;
    :goto_0
    if-ge v0, v2, :cond_4

    aget v3, v9, v0

    .line 650
    .local v3, "tmpTid":I
    if-gez v3, :cond_1

    goto/16 :goto_2

    .line 652
    :cond_1
    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v4, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v7}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, "/task/"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, "/comm"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    .line 653
    .end local v11    # "filePath":Ljava/lang/String;
    .local v4, "filePath":Ljava/lang/String;
    invoke-direct {v6, v4}, Lcom/miui/server/rtboost/SchedBoostService;->readCmdlineFromProcfs(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v5

    .line 654
    .local v5, "taskName":Ljava/lang/String;
    if-eqz v5, :cond_3

    const-string v11, ""

    invoke-virtual {v5, v11}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v11

    if-nez v11, :cond_3

    .line 655
    sget-object v11, Lcom/miui/server/rtboost/SchedBoostService;->RT_THREAD_COMM_LIST:[Ljava/lang/String;

    array-length v12, v11

    const/4 v13, 0x0

    :goto_1
    if-ge v13, v12, :cond_3

    aget-object v14, v11, v13

    .line 656
    .local v14, "comm":Ljava/lang/String;
    invoke-virtual {v5}, Ljava/lang/String;->trim()Ljava/lang/String;

    move-result-object v15

    invoke-virtual {v15, v14}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v15

    if-eqz v15, :cond_2

    .line 657
    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v15

    invoke-virtual {v8, v15}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 658
    sget-boolean v15, Lcom/miui/server/rtboost/SchedBoostService;->DEBUG:Z

    if-eqz v15, :cond_2

    new-instance v15, Ljava/lang/StringBuilder;

    invoke-direct {v15}, Ljava/lang/StringBuilder;-><init>()V

    const-string v10, "Sched GPU tid: "

    invoke-virtual {v15, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-virtual {v10, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v10

    const-string v15, " "

    invoke-virtual {v10, v15}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-virtual {v14}, Ljava/lang/String;->trim()Ljava/lang/String;

    move-result-object v15

    invoke-virtual {v10, v15}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-virtual {v10}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v10

    const-string v15, "SchedBoost"

    invoke-static {v15, v10}, Landroid/util/Slog;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 655
    .end local v14    # "comm":Ljava/lang/String;
    :cond_2
    add-int/lit8 v13, v13, 0x1

    goto :goto_1

    .line 649
    .end local v3    # "tmpTid":I
    .end local v5    # "taskName":Ljava/lang/String;
    :cond_3
    move-object v11, v4

    .end local v4    # "filePath":Ljava/lang/String;
    .restart local v11    # "filePath":Ljava/lang/String;
    :goto_2
    add-int/lit8 v0, v0, 0x1

    goto/16 :goto_0

    .line 663
    :cond_4
    invoke-virtual {v8}, Ljava/util/ArrayList;->stream()Ljava/util/stream/Stream;

    move-result-object v0

    new-instance v1, Lcom/android/server/audio/AudioServiceStubImpl$$ExternalSyntheticLambda1;

    invoke-direct {v1}, Lcom/android/server/audio/AudioServiceStubImpl$$ExternalSyntheticLambda1;-><init>()V

    invoke-interface {v0, v1}, Ljava/util/stream/Stream;->mapToInt(Ljava/util/function/ToIntFunction;)Ljava/util/stream/IntStream;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/stream/IntStream;->toArray()[I

    move-result-object v1

    const-wide/16 v2, 0x0

    const/4 v5, 0x3

    move-object/from16 v0, p0

    move-object/from16 v4, p2

    invoke-virtual/range {v0 .. v5}, Lcom/miui/server/rtboost/SchedBoostService;->beginSchedThreads([IJLjava/lang/String;I)V

    .line 665
    invoke-virtual {v8}, Ljava/util/ArrayList;->size()I

    move-result v0

    sget-object v1, Lcom/miui/server/rtboost/SchedBoostService;->RT_THREAD_COMM_LIST:[Ljava/lang/String;

    array-length v1, v1

    if-ne v0, v1, :cond_5

    .line 666
    const/4 v0, 0x0

    iput-boolean v0, v6, Lcom/miui/server/rtboost/SchedBoostService;->mPendingHomeGpuTids:Z

    goto :goto_3

    .line 668
    :cond_5
    iget v0, v6, Lcom/miui/server/rtboost/SchedBoostService;->mBoostHomeGpuTidsTryCnt:I

    add-int/lit8 v0, v0, 0x1

    iput v0, v6, Lcom/miui/server/rtboost/SchedBoostService;->mBoostHomeGpuTidsTryCnt:I

    .line 670
    :goto_3
    return-void

    .line 648
    .end local v11    # "filePath":Ljava/lang/String;
    .restart local v0    # "filePath":Ljava/lang/String;
    :cond_6
    :goto_4
    return-void
.end method

.method private setAffinityAndPriority(Lcom/miui/server/rtboost/SchedBoostService$BoostThreadInfo;ZZ[I)V
    .locals 7
    .param p1, "threadInfo"    # Lcom/miui/server/rtboost/SchedBoostService$BoostThreadInfo;
    .param p2, "isFifo"    # Z
    .param p3, "forkPriority"    # Z
    .param p4, "coresIndex"    # [I

    .line 1040
    iget v0, p1, Lcom/miui/server/rtboost/SchedBoostService$BoostThreadInfo;->tid:I

    .line 1041
    .local v0, "tid":I
    const/4 v1, 0x0

    .line 1043
    .local v1, "setFifo":Z
    const/4 v2, 0x1

    const-string v3, "SchedBoost"

    if-eqz p2, :cond_2

    .line 1044
    iget-boolean v4, p0, Lcom/miui/server/rtboost/SchedBoostService;->isNormalPolicy:Z

    if-eqz v4, :cond_0

    .line 1045
    const/16 v2, -0x14

    invoke-static {v0, v2, v3}, Landroid/os/MiuiProcess;->setThreadPriority(IILjava/lang/String;)Z

    move-result v1

    goto :goto_0

    .line 1046
    :cond_0
    if-eqz p3, :cond_1

    .line 1047
    invoke-static {v0, v2}, Landroid/os/MiuiProcess;->scheduleAsFifoAndForkPriority(IZ)Z

    move-result v1

    goto :goto_0

    .line 1049
    :cond_1
    invoke-static {v0, v2}, Lcom/android/server/am/ActivityManagerService;->scheduleAsFifoPriority(IZ)Z

    move-result v1

    goto :goto_0

    .line 1052
    :cond_2
    iget v4, p1, Lcom/miui/server/rtboost/SchedBoostService$BoostThreadInfo;->tid:I

    invoke-static {v4, v3}, Landroid/os/MiuiProcess;->getThreadPriority(ILjava/lang/String;)I

    move-result v4

    .line 1059
    .local v4, "curPrio":I
    iget-boolean v5, p0, Lcom/miui/server/rtboost/SchedBoostService;->isNormalPolicy:Z

    if-eqz v5, :cond_3

    iget v5, p1, Lcom/miui/server/rtboost/SchedBoostService$BoostThreadInfo;->boostPriority:I

    if-ne v4, v5, :cond_4

    .line 1060
    :cond_3
    invoke-static {v0, v2}, Lcom/android/server/am/ActivityManagerService;->scheduleAsRegularPriority(IZ)Z

    move-result v1

    .line 1061
    iget v2, p1, Lcom/miui/server/rtboost/SchedBoostService$BoostThreadInfo;->savedPriority:I

    invoke-static {v0, v2, v3}, Landroid/os/MiuiProcess;->setThreadPriority(IILjava/lang/String;)Z

    .line 1065
    .end local v4    # "curPrio":I
    :cond_4
    :goto_0
    if-eqz v1, :cond_5

    .line 1066
    invoke-direct {p0, v0, p4}, Lcom/miui/server/rtboost/SchedBoostService;->setSchedAffinity(I[I)V

    .line 1068
    :cond_5
    sget-boolean v2, Lcom/miui/server/rtboost/SchedBoostService;->DEBUG:Z

    if-eqz v2, :cond_6

    .line 1069
    nop

    .line 1070
    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-static {p2}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v4

    invoke-static {p3}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v5

    .line 1071
    invoke-static {p4}, Ljava/util/Arrays;->toString([I)Ljava/lang/String;

    move-result-object v6

    filled-new-array {v2, v4, v5, v6}, [Ljava/lang/Object;

    move-result-object v2

    .line 1069
    const-string/jumbo v4, "setAffinityAndPriority, tid: %s, isFifo: %s, forkPriority: %s, coresIndex: %s"

    invoke-static {v4, v2}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    invoke-static {v3, v2}, Landroid/util/Slog;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 1073
    :cond_6
    return-void
.end method

.method private setSchedAffinity(I[I)V
    .locals 4
    .param p1, "pid"    # I
    .param p2, "cores"    # [I

    .line 1076
    iget-object v0, p0, Lcom/miui/server/rtboost/SchedBoostService;->mMethodSetAffinity:Ljava/lang/reflect/Method;

    if-eqz v0, :cond_0

    .line 1078
    const/4 v1, 0x2

    :try_start_0
    new-array v1, v1, [Ljava/lang/Object;

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    const/4 v3, 0x0

    aput-object v2, v1, v3

    const/4 v2, 0x1

    aput-object p2, v1, v2

    const/4 v2, 0x0

    invoke-virtual {v0, v2, v1}, Ljava/lang/reflect/Method;->invoke(Ljava/lang/Object;[Ljava/lang/Object;)Ljava/lang/Object;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 1081
    goto :goto_0

    .line 1079
    :catch_0
    move-exception v0

    .line 1080
    .local v0, "e":Ljava/lang/Exception;
    invoke-virtual {v0}, Ljava/lang/Exception;->printStackTrace()V

    .line 1083
    .end local v0    # "e":Ljava/lang/Exception;
    :cond_0
    :goto_0
    return-void
.end method

.method private setTaskGroup(II)V
    .locals 5
    .param p1, "tid"    # I
    .param p2, "group"    # I

    .line 1096
    sget-boolean v0, Lcom/miui/server/rtboost/SchedBoostService;->IS_UIGROUP_ENABLED:Z

    if-eqz v0, :cond_0

    .line 1097
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v1, "setTaskGroup: "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    const-string v1, "SchedBoost"

    invoke-static {v1, v0}, Landroid/util/Slog;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 1099
    :try_start_0
    invoke-static {p1, p2}, Landroid/os/Process;->setThreadGroupAndCpuset(II)V

    .line 1100
    const-string/jumbo v0, "setTaskGroup: Check cpuset of tid: %s,group= %s"

    const/4 v2, 0x2

    new-array v2, v2, [Ljava/lang/Object;

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    const/4 v4, 0x0

    aput-object v3, v2, v4

    invoke-static {p1}, Landroid/os/Process;->getCpusetThreadGroup(I)I

    move-result v3

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    const/4 v4, 0x1

    aput-object v3, v2, v4

    invoke-static {v0, v2}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    invoke-static {v1, v0}, Landroid/util/Slog;->i(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 1103
    goto :goto_0

    .line 1101
    :catch_0
    move-exception v0

    .line 1102
    .local v0, "e":Ljava/lang/Exception;
    invoke-virtual {v0}, Ljava/lang/Exception;->printStackTrace()V

    .line 1105
    .end local v0    # "e":Ljava/lang/Exception;
    :cond_0
    :goto_0
    return-void
.end method

.method private setTaskUclamp(III)V
    .locals 4
    .param p1, "tid"    # I
    .param p2, "perfIdx"    # I
    .param p3, "maxIdx"    # I

    .line 1086
    sget-boolean v0, Lcom/miui/server/rtboost/SchedBoostService;->IS_UCLAMP_ENABLED:Z

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/miui/server/rtboost/SchedBoostService;->mMethodUclampTask:Ljava/lang/reflect/Method;

    if-eqz v0, :cond_0

    .line 1088
    const/4 v1, 0x3

    :try_start_0
    new-array v1, v1, [Ljava/lang/Object;

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    const/4 v3, 0x0

    aput-object v2, v1, v3

    invoke-static {p2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    const/4 v3, 0x1

    aput-object v2, v1, v3

    invoke-static {p3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    const/4 v3, 0x2

    aput-object v2, v1, v3

    const/4 v2, 0x0

    invoke-virtual {v0, v2, v1}, Ljava/lang/reflect/Method;->invoke(Ljava/lang/Object;[Ljava/lang/Object;)Ljava/lang/Object;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 1091
    goto :goto_0

    .line 1089
    :catch_0
    move-exception v0

    .line 1090
    .local v0, "e":Ljava/lang/Exception;
    invoke-virtual {v0}, Ljava/lang/Exception;->printStackTrace()V

    .line 1093
    .end local v0    # "e":Ljava/lang/Exception;
    :cond_0
    :goto_0
    return-void
.end method

.method private declared-synchronized startBoostInternal(Lcom/miui/server/rtboost/SchedBoostService$BoostThreadInfo;)Z
    .locals 7
    .param p1, "threadInfo"    # Lcom/miui/server/rtboost/SchedBoostService$BoostThreadInfo;

    monitor-enter p0

    .line 1111
    const/4 v0, 0x0

    :try_start_0
    iget v1, p1, Lcom/miui/server/rtboost/SchedBoostService$BoostThreadInfo;->mode:I

    const/4 v2, 0x1

    if-eq v1, v2, :cond_3

    iget v1, p1, Lcom/miui/server/rtboost/SchedBoostService$BoostThreadInfo;->mode:I

    const/4 v3, 0x5

    if-eq v1, v3, :cond_3

    iget v1, p1, Lcom/miui/server/rtboost/SchedBoostService$BoostThreadInfo;->mode:I

    const/4 v3, 0x4

    if-ne v1, v3, :cond_0

    goto :goto_0

    .line 1119
    :cond_0
    iget v1, p1, Lcom/miui/server/rtboost/SchedBoostService$BoostThreadInfo;->mode:I

    const/4 v3, 0x6

    if-ne v1, v3, :cond_1

    .line 1120
    iget-object v1, p0, Lcom/miui/server/rtboost/SchedBoostService;->mGpuAndCpuPerfList:[I

    .local v1, "params":[I
    goto :goto_1

    .line 1122
    .end local v1    # "params":[I
    .end local p0    # "this":Lcom/miui/server/rtboost/SchedBoostService;
    :cond_1
    sget-boolean v1, Lcom/miui/server/rtboost/SchedBoostService;->DEBUG:Z

    if-eqz v1, :cond_2

    const-string v1, "SchedBoost"

    const-string v2, "current sched mode no need to boost"

    invoke-static {v1, v2}, Landroid/util/Slog;->d(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 1123
    :cond_2
    monitor-exit p0

    return v0

    .line 1114
    :cond_3
    :goto_0
    :try_start_1
    invoke-virtual {p0, p1}, Lcom/miui/server/rtboost/SchedBoostService;->needBoostHigherFreq(Lcom/miui/server/rtboost/SchedBoostService$BoostThreadInfo;)Z

    move-result v1

    if-eqz v1, :cond_4

    .line 1115
    sget-object v1, Lcom/miui/server/rtboost/SchedBoostService;->QCOM_GPU_AND_CPU_HIGHER_FREQ:[I

    .restart local v1    # "params":[I
    goto :goto_1

    .line 1117
    .end local v1    # "params":[I
    :cond_4
    iget-object v1, p0, Lcom/miui/server/rtboost/SchedBoostService;->mDefultPerfList:[I

    .line 1126
    .restart local v1    # "params":[I
    :goto_1
    iget v3, p0, Lcom/miui/server/rtboost/SchedBoostService;->mHandle:I

    if-eqz v3, :cond_6

    .line 1127
    invoke-static {}, Landroid/os/SystemClock;->uptimeMillis()J

    move-result-wide v3

    iget-wide v5, p0, Lcom/miui/server/rtboost/SchedBoostService;->mLastPerfAcqTime:J

    sub-long/2addr v3, v5

    .line 1128
    .local v3, "lastPerfAcqDuration":J
    const-wide/16 v5, 0x12c

    cmp-long v5, v3, v5

    if-gez v5, :cond_6

    .line 1129
    sget-boolean v2, Lcom/miui/server/rtboost/SchedBoostService;->DEBUG:Z

    if-eqz v2, :cond_5

    const-string v2, "SchedBoost"

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "last perfacq still work, skip, "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, v3, v4}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v2, v5}, Landroid/util/Slog;->d(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_0
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 1131
    :cond_5
    monitor-exit p0

    return v0

    .line 1134
    .end local v3    # "lastPerfAcqDuration":J
    :cond_6
    :try_start_2
    invoke-direct {p0}, Lcom/miui/server/rtboost/SchedBoostService;->stopBoostInternal()V

    .line 1135
    sget-boolean v3, Lcom/miui/server/rtboost/SchedBoostService;->IS_MTK_DEVICE:Z

    if-eqz v3, :cond_7

    .line 1136
    iget-object v3, p0, Lcom/miui/server/rtboost/SchedBoostService;->mMTKPerfStub:Landroid/perf/PerfMTKStub;

    iget-wide v4, p1, Lcom/miui/server/rtboost/SchedBoostService$BoostThreadInfo;->duration:J

    long-to-int v4, v4

    invoke-interface {v3, v4, v1}, Landroid/perf/PerfMTKStub;->perfLockAcquire(I[I)I

    move-result v3

    iput v3, p0, Lcom/miui/server/rtboost/SchedBoostService;->mHandle:I

    goto :goto_2

    .line 1138
    :cond_7
    iget-object v3, p0, Lcom/miui/server/rtboost/SchedBoostService;->mQcomPerfStub:Landroid/perf/PerfStub;

    iget-wide v4, p1, Lcom/miui/server/rtboost/SchedBoostService$BoostThreadInfo;->duration:J

    long-to-int v4, v4

    invoke-virtual {v3, v4, v1}, Landroid/perf/PerfStub;->perfLockAcquire(I[I)I

    move-result v3

    iput v3, p0, Lcom/miui/server/rtboost/SchedBoostService;->mHandle:I

    .line 1140
    :goto_2
    invoke-static {}, Landroid/os/SystemClock;->uptimeMillis()J

    move-result-wide v3

    iput-wide v3, p0, Lcom/miui/server/rtboost/SchedBoostService;->mLastPerfAcqTime:J

    .line 1141
    const-string v3, "SchedBoost"

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v5, "startBoostInternal "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    iget-wide v5, p1, Lcom/miui/server/rtboost/SchedBoostService$BoostThreadInfo;->duration:J

    invoke-virtual {v4, v5, v6}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Landroid/util/Slog;->d(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_2
    .catch Ljava/lang/Exception; {:try_start_2 .. :try_end_2} :catch_0
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    .line 1142
    monitor-exit p0

    return v2

    .line 1110
    .end local v1    # "params":[I
    .end local p1    # "threadInfo":Lcom/miui/server/rtboost/SchedBoostService$BoostThreadInfo;
    :catchall_0
    move-exception p1

    goto :goto_3

    .line 1143
    .restart local p1    # "threadInfo":Lcom/miui/server/rtboost/SchedBoostService$BoostThreadInfo;
    :catch_0
    move-exception v1

    .line 1144
    .local v1, "e":Ljava/lang/Exception;
    :try_start_3
    const-string v2, "SchedBoost"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v4, "startBoostInternal exception "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Landroid/util/Slog;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 1145
    invoke-virtual {v1}, Ljava/lang/Exception;->printStackTrace()V
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    .line 1147
    .end local v1    # "e":Ljava/lang/Exception;
    monitor-exit p0

    return v0

    .line 1110
    .end local p1    # "threadInfo":Lcom/miui/server/rtboost/SchedBoostService$BoostThreadInfo;
    :goto_3
    monitor-exit p0

    throw p1
.end method

.method private declared-synchronized startBoostInternalWithOpcode(Lcom/miui/server/rtboost/SchedBoostService$BoostThreadInfo;)Z
    .locals 7
    .param p1, "threadInfo"    # Lcom/miui/server/rtboost/SchedBoostService$BoostThreadInfo;

    monitor-enter p0

    .line 1152
    const/4 v0, 0x0

    :try_start_0
    invoke-virtual {p0, p1}, Lcom/miui/server/rtboost/SchedBoostService;->needBoostHigherFreq(Lcom/miui/server/rtboost/SchedBoostService$BoostThreadInfo;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 1153
    const/16 v1, 0xa

    iput v1, p1, Lcom/miui/server/rtboost/SchedBoostService$BoostThreadInfo;->mode:I

    .line 1155
    .end local p0    # "this":Lcom/miui/server/rtboost/SchedBoostService;
    :cond_0
    iget v1, p1, Lcom/miui/server/rtboost/SchedBoostService$BoostThreadInfo;->mode:I

    invoke-static {v1}, Landroid/os/MiuiProcess;->schedCodeToOpcode(I)I

    move-result v1
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 1157
    .local v1, "opcode":I
    if-gez v1, :cond_1

    monitor-exit p0

    return v0

    .line 1159
    .restart local p0    # "this":Lcom/miui/server/rtboost/SchedBoostService;
    :cond_1
    :try_start_1
    iget v2, p0, Lcom/miui/server/rtboost/SchedBoostService;->mHandle:I

    if-eqz v2, :cond_3

    .line 1160
    invoke-static {}, Landroid/os/SystemClock;->uptimeMillis()J

    move-result-wide v2

    iget-wide v4, p0, Lcom/miui/server/rtboost/SchedBoostService;->mLastPerfAcqTime:J

    sub-long/2addr v2, v4

    .line 1161
    .local v2, "lastPerfAcqDuration":J
    const-wide/16 v4, 0x12c

    cmp-long v4, v2, v4

    if-gez v4, :cond_3

    .line 1162
    sget-boolean v4, Lcom/miui/server/rtboost/SchedBoostService;->DEBUG:Z

    if-eqz v4, :cond_2

    const-string v4, "SchedBoost"

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "last perfacq still work, skip, "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, v2, v3}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5}, Landroid/util/Slog;->d(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_0
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 1164
    .end local p0    # "this":Lcom/miui/server/rtboost/SchedBoostService;
    :cond_2
    monitor-exit p0

    return v0

    .line 1167
    .end local v2    # "lastPerfAcqDuration":J
    :cond_3
    :try_start_2
    invoke-direct {p0}, Lcom/miui/server/rtboost/SchedBoostService;->stopBoostInternal()V

    .line 1168
    iget-object v2, p0, Lcom/miui/server/rtboost/SchedBoostService;->mQcomPerfStub:Landroid/perf/PerfStub;

    iget-object v3, p1, Lcom/miui/server/rtboost/SchedBoostService$BoostThreadInfo;->procName:Ljava/lang/String;

    iget-wide v4, p1, Lcom/miui/server/rtboost/SchedBoostService$BoostThreadInfo;->duration:J

    long-to-int v4, v4

    const/16 v5, 0x11a0

    invoke-virtual {v2, v5, v3, v4, v1}, Landroid/perf/PerfStub;->perfHint(ILjava/lang/String;II)I

    move-result v2

    iput v2, p0, Lcom/miui/server/rtboost/SchedBoostService;->mHandle:I

    .line 1170
    invoke-static {}, Landroid/os/SystemClock;->uptimeMillis()J

    move-result-wide v2

    iput-wide v2, p0, Lcom/miui/server/rtboost/SchedBoostService;->mLastPerfAcqTime:J

    .line 1171
    const-string v2, "SchedBoost"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v4, "startBoostInternalWithOpcode "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    iget-wide v4, p1, Lcom/miui/server/rtboost/SchedBoostService$BoostThreadInfo;->duration:J

    invoke-virtual {v3, v4, v5}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, " ms, opcode: "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Landroid/util/Slog;->d(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_2
    .catch Ljava/lang/Exception; {:try_start_2 .. :try_end_2} :catch_0
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    .line 1173
    monitor-exit p0

    const/4 v0, 0x1

    return v0

    .line 1151
    .end local v1    # "opcode":I
    .end local p1    # "threadInfo":Lcom/miui/server/rtboost/SchedBoostService$BoostThreadInfo;
    :catchall_0
    move-exception p1

    goto :goto_0

    .line 1174
    .restart local p1    # "threadInfo":Lcom/miui/server/rtboost/SchedBoostService$BoostThreadInfo;
    :catch_0
    move-exception v1

    .line 1175
    .local v1, "e":Ljava/lang/Exception;
    :try_start_3
    const-string v2, "SchedBoost"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v4, "startBoostInternalWithOpcode exception "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Landroid/util/Slog;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 1176
    invoke-virtual {v1}, Ljava/lang/Exception;->printStackTrace()V
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    .line 1178
    .end local v1    # "e":Ljava/lang/Exception;
    monitor-exit p0

    return v0

    .line 1151
    .end local p1    # "threadInfo":Lcom/miui/server/rtboost/SchedBoostService$BoostThreadInfo;
    :goto_0
    monitor-exit p0

    throw p1
.end method

.method private declared-synchronized stopBoostInternal()V
    .locals 4

    monitor-enter p0

    .line 1182
    :try_start_0
    iget v0, p0, Lcom/miui/server/rtboost/SchedBoostService;->mHandle:I

    if-nez v0, :cond_1

    .line 1183
    sget-boolean v0, Lcom/miui/server/rtboost/SchedBoostService;->DEBUG:Z

    if-eqz v0, :cond_0

    const-string v0, "SchedBoost"

    const-string v1, "has released boost yet"

    invoke-static {v0, v1}, Landroid/util/Slog;->w(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 1184
    .end local p0    # "this":Lcom/miui/server/rtboost/SchedBoostService;
    :cond_0
    monitor-exit p0

    return-void

    .line 1187
    :cond_1
    :try_start_1
    sget-boolean v1, Lcom/miui/server/rtboost/SchedBoostService;->IS_MTK_DEVICE:Z

    if-eqz v1, :cond_2

    .line 1188
    iget-object v1, p0, Lcom/miui/server/rtboost/SchedBoostService;->mMTKPerfStub:Landroid/perf/PerfMTKStub;

    invoke-interface {v1, v0}, Landroid/perf/PerfMTKStub;->perfLockReleaseHandler(I)I

    goto :goto_0

    .line 1190
    :cond_2
    iget-object v1, p0, Lcom/miui/server/rtboost/SchedBoostService;->mQcomPerfStub:Landroid/perf/PerfStub;

    invoke-virtual {v1, v0}, Landroid/perf/PerfStub;->perfLockReleaseHandler(I)I

    .line 1192
    :goto_0
    const/4 v0, 0x0

    iput v0, p0, Lcom/miui/server/rtboost/SchedBoostService;->mHandle:I

    .line 1193
    const-string v0, "SchedBoost"

    const-string/jumbo v1, "stopBoostInternal"

    invoke-static {v0, v1}, Landroid/util/Slog;->d(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_0
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 1197
    goto :goto_1

    .line 1194
    :catch_0
    move-exception v0

    .line 1195
    .local v0, "e":Ljava/lang/Exception;
    :try_start_2
    const-string v1, "SchedBoost"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v3, "stopBoostInternal exception "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Slog;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 1196
    invoke-virtual {v0}, Ljava/lang/Exception;->printStackTrace()V
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    .line 1198
    .end local v0    # "e":Ljava/lang/Exception;
    :goto_1
    monitor-exit p0

    return-void

    .line 1181
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method


# virtual methods
.method public beginSchedThreads([IJLjava/lang/String;I)V
    .locals 8
    .param p1, "tids"    # [I
    .param p2, "duration"    # J
    .param p4, "procName"    # Ljava/lang/String;
    .param p5, "mode"    # I

    .line 334
    sget-boolean v0, Lcom/miui/server/rtboost/SchedBoostService;->DEBUG:Z

    if-eqz v0, :cond_0

    .line 335
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "beginSchedThreads called, mode: "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p5}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", procName :"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    const-string v1, "SchedBoost"

    invoke-static {v1, v0}, Landroid/util/Slog;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 337
    :cond_0
    iget-object v2, p0, Lcom/miui/server/rtboost/SchedBoostService;->mHandler:Lcom/miui/server/rtboost/SchedBoostService$Handler;

    if-nez v2, :cond_1

    .line 338
    return-void

    .line 340
    :cond_1
    if-eqz p1, :cond_6

    array-length v0, p1

    if-nez v0, :cond_2

    goto :goto_2

    .line 344
    :cond_2
    sparse-switch p5, :sswitch_data_0

    goto :goto_1

    .line 376
    :sswitch_0
    move-object v3, p1

    move-wide v4, p2

    move-object v6, p4

    move v7, p5

    invoke-virtual/range {v2 .. v7}, Lcom/miui/server/rtboost/SchedBoostService$Handler;->obtainBoostFreqMsg([IJLjava/lang/String;I)Landroid/os/Message;

    move-result-object v0

    .line 377
    invoke-virtual {v0}, Landroid/os/Message;->sendToTarget()V

    .line 378
    return-void

    .line 361
    :sswitch_1
    array-length v0, p1

    const/4 v1, 0x0

    :goto_0
    if-ge v1, v0, :cond_4

    aget v2, p1, v1

    .line 362
    .local v2, "tid":I
    if-lez v2, :cond_3

    .line 363
    iget-object v3, p0, Lcom/miui/server/rtboost/SchedBoostService;->mHandler:Lcom/miui/server/rtboost/SchedBoostService$Handler;

    invoke-virtual {v3, v2, p2, p3, p5}, Lcom/miui/server/rtboost/SchedBoostService$Handler;->obtainReSetThreadsFIFOMsg(IJI)Landroid/os/Message;

    move-result-object v3

    .line 364
    invoke-virtual {v3}, Landroid/os/Message;->sendToTarget()V

    .line 361
    .end local v2    # "tid":I
    :cond_3
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 367
    :cond_4
    return-void

    .line 356
    :sswitch_2
    invoke-virtual {v2, p1, p2, p3, p5}, Lcom/miui/server/rtboost/SchedBoostService$Handler;->obtainSetThreadsFIFOMsg([IJI)Landroid/os/Message;

    move-result-object v0

    .line 357
    invoke-virtual {v0}, Landroid/os/Message;->sendToTarget()V

    .line 358
    return-void

    .line 371
    :sswitch_3
    const-string p4, "com.miui.home"

    .line 372
    goto :goto_1

    .line 351
    :sswitch_4
    invoke-virtual {v2, p1, p2, p3}, Lcom/miui/server/rtboost/SchedBoostService$Handler;->obtainThreadBindBigCoreMsg([IJ)Landroid/os/Message;

    move-result-object v0

    .line 352
    invoke-virtual {v0}, Landroid/os/Message;->sendToTarget()V

    .line 353
    return-void

    .line 346
    :sswitch_5
    invoke-virtual {v2, p1}, Lcom/miui/server/rtboost/SchedBoostService$Handler;->obtainSetThreadsAlwaysFIFOMsg([I)Landroid/os/Message;

    move-result-object v0

    .line 347
    invoke-virtual {v0}, Landroid/os/Message;->sendToTarget()V

    .line 348
    return-void

    .line 384
    :goto_1
    const-wide/16 v0, 0x0

    cmp-long v0, p2, v0

    if-ltz v0, :cond_5

    const-wide/16 v0, 0x1770

    cmp-long v0, p2, v0

    if-gtz v0, :cond_5

    .line 385
    move-object v3, p1

    move-wide v4, p2

    move-object v6, p4

    move v7, p5

    invoke-virtual/range {v2 .. v7}, Lcom/miui/server/rtboost/SchedBoostService$Handler;->obtainBeginSchedThreadsMsg([IJLjava/lang/String;I)Landroid/os/Message;

    move-result-object v0

    .line 386
    invoke-virtual {v0}, Landroid/os/Message;->sendToTarget()V

    .line 388
    :cond_5
    return-void

    .line 341
    :cond_6
    :goto_2
    return-void

    :sswitch_data_0
    .sparse-switch
        0x3 -> :sswitch_5
        0x7 -> :sswitch_4
        0x9 -> :sswitch_3
        0xc -> :sswitch_2
        0xd -> :sswitch_1
        0x65 -> :sswitch_0
        0x66 -> :sswitch_0
    .end sparse-switch
.end method

.method public boostHomeAnim(JI)V
    .locals 8
    .param p1, "duration"    # J
    .param p3, "mode"    # I

    .line 1258
    iget v0, p0, Lcom/miui/server/rtboost/SchedBoostService;->mHomeMainThreadId:I

    iget v1, p0, Lcom/miui/server/rtboost/SchedBoostService;->mHomeRenderThreadId:I

    filled-new-array {v0, v1}, [I

    move-result-object v3

    const-string v6, "com.miui.home"

    move-object v2, p0

    move-wide v4, p1

    move v7, p3

    invoke-virtual/range {v2 .. v7}, Lcom/miui/server/rtboost/SchedBoostService;->beginSchedThreads([IJLjava/lang/String;I)V

    .line 1260
    return-void
.end method

.method public checkThreadBoost(I)Z
    .locals 1
    .param p1, "tid"    # I

    .line 1217
    iget-object v0, p0, Lcom/miui/server/rtboost/SchedBoostService;->mBoostingMap:Lcom/miui/server/rtboost/SchedBoostService$BoostingPackageMap;

    invoke-virtual {v0, p1}, Lcom/miui/server/rtboost/SchedBoostService$BoostingPackageMap;->getByTid(I)Lcom/miui/server/rtboost/SchedBoostService$BoostThreadInfo;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 1218
    const/4 v0, 0x1

    return v0

    .line 1220
    :cond_0
    const/4 v0, 0x0

    return v0
.end method

.method protected dump(Ljava/io/FileDescriptor;Ljava/io/PrintWriter;[Ljava/lang/String;)V
    .locals 2
    .param p1, "fd"    # Ljava/io/FileDescriptor;
    .param p2, "pw"    # Ljava/io/PrintWriter;
    .param p3, "args"    # [Ljava/lang/String;

    .line 1299
    iget-object v0, p0, Lcom/miui/server/rtboost/SchedBoostService;->mContext:Landroid/content/Context;

    const-string v1, "SchedBoost"

    invoke-static {v0, v1, p2}, Lcom/android/internal/util/DumpUtils;->checkDumpPermission(Landroid/content/Context;Ljava/lang/String;Ljava/io/PrintWriter;)Z

    move-result v0

    if-nez v0, :cond_0

    return-void

    .line 1300
    :cond_0
    const-string v0, "sched boost (SchedBoostService):"

    invoke-virtual {p2, v0}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V

    .line 1302
    :try_start_0
    invoke-static {}, Lcom/android/server/wm/RealTimeModeControllerImpl;->get()Lcom/android/server/wm/RealTimeModeControllerImpl;

    invoke-static {p2, p3}, Lcom/android/server/wm/RealTimeModeControllerImpl;->dump(Ljava/io/PrintWriter;[Ljava/lang/String;)V

    .line 1303
    invoke-direct {p0, p2, p3}, Lcom/miui/server/rtboost/SchedBoostService;->dump(Ljava/io/PrintWriter;[Ljava/lang/String;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 1306
    goto :goto_0

    .line 1304
    :catch_0
    move-exception v0

    .line 1305
    .local v0, "e":Ljava/lang/Exception;
    invoke-virtual {v0}, Ljava/lang/Exception;->printStackTrace()V

    .line 1307
    .end local v0    # "e":Ljava/lang/Exception;
    :goto_0
    return-void
.end method

.method public enableSchedBoost(Z)V
    .locals 2
    .param p1, "enable"    # Z

    .line 328
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    invoke-static {}, Landroid/os/Binder;->getCallingPid()I

    move-result v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, " set sched boost enable:"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    const-string v1, "SchedBoost"

    invoke-static {v1, v0}, Landroid/util/Slog;->w(Ljava/lang/String;Ljava/lang/String;)I

    .line 329
    xor-int/lit8 v0, p1, 0x1

    iput-boolean v0, p0, Lcom/miui/server/rtboost/SchedBoostService;->mSimpleNotNeedSched:Z

    .line 330
    return-void
.end method

.method public needBoostHigherFreq(Lcom/miui/server/rtboost/SchedBoostService$BoostThreadInfo;)Z
    .locals 3
    .param p1, "threadInfo"    # Lcom/miui/server/rtboost/SchedBoostService$BoostThreadInfo;

    .line 1263
    sget-boolean v0, Lcom/miui/server/rtboost/SchedBoostService;->IS_MTK_DEVICE:Z

    if-nez v0, :cond_1

    .line 1264
    iget-object v0, p0, Lcom/miui/server/rtboost/SchedBoostService;->mContext:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/res/Resources;->getConfiguration()Landroid/content/res/Configuration;

    move-result-object v0

    iget v0, v0, Landroid/content/res/Configuration;->orientation:I

    .line 1265
    .local v0, "orientation":I
    const/4 v1, 0x2

    if-ne v0, v1, :cond_1

    iget v1, p1, Lcom/miui/server/rtboost/SchedBoostService$BoostThreadInfo;->mode:I

    const/4 v2, 0x4

    if-ne v1, v2, :cond_1

    .line 1267
    sget-boolean v1, Lcom/miui/server/rtboost/SchedBoostService;->DEBUG:Z

    if-eqz v1, :cond_0

    const-string v1, "SchedBoost"

    const-string v2, "needBoostHigherFreq"

    invoke-static {v1, v2}, Landroid/util/Slog;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 1268
    :cond_0
    const/4 v1, 0x1

    return v1

    .line 1271
    .end local v0    # "orientation":I
    :cond_1
    const/4 v0, 0x0

    return v0
.end method

.method public onShellCommand(Ljava/io/FileDescriptor;Ljava/io/FileDescriptor;Ljava/io/FileDescriptor;[Ljava/lang/String;Landroid/os/ShellCallback;Landroid/os/ResultReceiver;)V
    .locals 8
    .param p1, "in"    # Ljava/io/FileDescriptor;
    .param p2, "out"    # Ljava/io/FileDescriptor;
    .param p3, "err"    # Ljava/io/FileDescriptor;
    .param p4, "args"    # [Ljava/lang/String;
    .param p5, "callback"    # Landroid/os/ShellCallback;
    .param p6, "resultReceiver"    # Landroid/os/ResultReceiver;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    .line 1313
    new-instance v0, Lcom/miui/server/rtboost/SchedBoostService$SchedBoostShellCommand;

    invoke-direct {v0, p0}, Lcom/miui/server/rtboost/SchedBoostService$SchedBoostShellCommand;-><init>(Lcom/miui/server/rtboost/SchedBoostService;)V

    .line 1314
    move-object v1, p0

    move-object v2, p1

    move-object v3, p2

    move-object v4, p3

    move-object v5, p4

    move-object v6, p5

    move-object v7, p6

    invoke-virtual/range {v0 .. v7}, Lcom/miui/server/rtboost/SchedBoostService$SchedBoostShellCommand;->exec(Landroid/os/Binder;Ljava/io/FileDescriptor;Ljava/io/FileDescriptor;Ljava/io/FileDescriptor;[Ljava/lang/String;Landroid/os/ShellCallback;Landroid/os/ResultReceiver;)I

    .line 1315
    return-void
.end method

.method public schedProcessBoost(Lcom/android/server/wm/WindowProcessListener;Ljava/lang/String;IIIJ)V
    .locals 0
    .param p1, "proc"    # Lcom/android/server/wm/WindowProcessListener;
    .param p2, "procName"    # Ljava/lang/String;
    .param p3, "pid"    # I
    .param p4, "rtid"    # I
    .param p5, "schedMode"    # I
    .param p6, "timeout"    # J

    .line 308
    nop

    .line 309
    return-void
.end method

.method public setRenderThreadTid(Lcom/android/server/wm/WindowProcessController;)V
    .locals 1
    .param p1, "wpc"    # Lcom/android/server/wm/WindowProcessController;

    .line 315
    if-nez p1, :cond_0

    return-void

    .line 316
    :cond_0
    invoke-static {p1}, Lcom/android/server/wm/RealTimeModeControllerImpl;->isHomeProcess(Lcom/android/server/wm/WindowProcessController;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 317
    invoke-virtual {p1}, Lcom/android/server/wm/WindowProcessController;->getPid()I

    move-result v0

    iput v0, p0, Lcom/miui/server/rtboost/SchedBoostService;->mHomeMainThreadId:I

    .line 318
    iget v0, p1, Lcom/android/server/wm/WindowProcessController;->mRenderThreadTid:I

    iput v0, p0, Lcom/miui/server/rtboost/SchedBoostService;->mHomeRenderThreadId:I

    .line 319
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/miui/server/rtboost/SchedBoostService;->mPendingHomeGpuTids:Z

    .line 320
    const/4 v0, 0x0

    iput v0, p0, Lcom/miui/server/rtboost/SchedBoostService;->mBoostHomeGpuTidsTryCnt:I

    goto :goto_0

    .line 321
    :cond_1
    invoke-static {p1}, Lcom/android/server/wm/RealTimeModeControllerImpl;->isSystemUIProcess(Lcom/android/server/wm/WindowProcessController;)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 322
    invoke-virtual {p1}, Lcom/android/server/wm/WindowProcessController;->getPid()I

    move-result v0

    iput v0, p0, Lcom/miui/server/rtboost/SchedBoostService;->mSystemUIMainThreadId:I

    .line 323
    iget v0, p1, Lcom/android/server/wm/WindowProcessController;->mRenderThreadTid:I

    iput v0, p0, Lcom/miui/server/rtboost/SchedBoostService;->mSystemUIRenderThreadId:I

    .line 325
    :cond_2
    :goto_0
    return-void
.end method

.method public setThreadSavedPriority([II)V
    .locals 6
    .param p1, "tid"    # [I
    .param p2, "prio"    # I

    .line 1224
    array-length v0, p1

    if-lez v0, :cond_2

    .line 1225
    array-length v0, p1

    const/4 v1, 0x0

    :goto_0
    if-ge v1, v0, :cond_2

    aget v2, p1, v1

    .line 1226
    .local v2, "threadTid":I
    iget-object v3, p0, Lcom/miui/server/rtboost/SchedBoostService;->mBoostingMap:Lcom/miui/server/rtboost/SchedBoostService$BoostingPackageMap;

    invoke-virtual {v3, v2}, Lcom/miui/server/rtboost/SchedBoostService$BoostingPackageMap;->getByTid(I)Lcom/miui/server/rtboost/SchedBoostService$BoostThreadInfo;

    move-result-object v3

    .line 1227
    .local v3, "info":Lcom/miui/server/rtboost/SchedBoostService$BoostThreadInfo;
    if-eqz v3, :cond_1

    .line 1228
    sget-boolean v4, Lcom/miui/server/rtboost/SchedBoostService;->DEBUG:Z

    if-eqz v4, :cond_0

    .line 1229
    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v5, "setThreadSavedPriority tid: "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, ", before: "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    iget v5, v3, Lcom/miui/server/rtboost/SchedBoostService$BoostThreadInfo;->savedPriority:I

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, ", after: "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    const-string v5, "SchedBoost"

    invoke-static {v5, v4}, Landroid/util/Slog;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 1232
    :cond_0
    iput p2, v3, Lcom/miui/server/rtboost/SchedBoostService$BoostThreadInfo;->savedPriority:I

    .line 1225
    .end local v2    # "threadTid":I
    .end local v3    # "info":Lcom/miui/server/rtboost/SchedBoostService$BoostThreadInfo;
    :cond_1
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 1236
    :cond_2
    return-void
.end method

.method public stopCurrentSchedBoost()V
    .locals 6

    .line 1239
    iget-object v0, p0, Lcom/miui/server/rtboost/SchedBoostService;->mBoostingMap:Lcom/miui/server/rtboost/SchedBoostService$BoostingPackageMap;

    invoke-static {v0}, Lcom/miui/server/rtboost/SchedBoostService$BoostingPackageMap;->-$$Nest$fgetmMapLock(Lcom/miui/server/rtboost/SchedBoostService$BoostingPackageMap;)Ljava/lang/Object;

    move-result-object v0

    monitor-enter v0

    .line 1240
    :try_start_0
    iget-object v1, p0, Lcom/miui/server/rtboost/SchedBoostService;->mBoostingMap:Lcom/miui/server/rtboost/SchedBoostService$BoostingPackageMap;

    invoke-virtual {v1}, Lcom/miui/server/rtboost/SchedBoostService$BoostingPackageMap;->getList()Ljava/util/ArrayList;

    move-result-object v1

    .line 1241
    .local v1, "list":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/miui/server/rtboost/SchedBoostService$BoostThreadInfo;>;"
    if-eqz v1, :cond_1

    invoke-virtual {v1}, Ljava/util/ArrayList;->size()I

    move-result v2

    if-lez v2, :cond_1

    .line 1242
    const/4 v2, 0x0

    .local v2, "i":I
    :goto_0
    invoke-virtual {v1}, Ljava/util/ArrayList;->size()I

    move-result v3

    if-ge v2, v3, :cond_1

    .line 1243
    invoke-virtual {v1, v2}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/miui/server/rtboost/SchedBoostService$BoostThreadInfo;

    .line 1245
    .local v3, "threadInfo":Lcom/miui/server/rtboost/SchedBoostService$BoostThreadInfo;
    iget-boolean v4, p0, Lcom/miui/server/rtboost/SchedBoostService;->mScreenOff:Z

    if-nez v4, :cond_0

    iget v4, v3, Lcom/miui/server/rtboost/SchedBoostService$BoostThreadInfo;->mode:I

    const/4 v5, 0x4

    if-ne v4, v5, :cond_0

    .line 1246
    monitor-exit v0

    return-void

    .line 1248
    :cond_0
    iget-object v4, p0, Lcom/miui/server/rtboost/SchedBoostService;->mHandler:Lcom/miui/server/rtboost/SchedBoostService$Handler;

    invoke-virtual {v4, v3}, Lcom/miui/server/rtboost/SchedBoostService$Handler;->removeResetThreadsMsg(Lcom/miui/server/rtboost/SchedBoostService$BoostThreadInfo;)V

    .line 1249
    iget-object v4, p0, Lcom/miui/server/rtboost/SchedBoostService;->mHandler:Lcom/miui/server/rtboost/SchedBoostService$Handler;

    invoke-virtual {v4, v3}, Lcom/miui/server/rtboost/SchedBoostService$Handler;->obtainResetSchedThreadsMsg(Lcom/miui/server/rtboost/SchedBoostService$BoostThreadInfo;)Landroid/os/Message;

    move-result-object v4

    .line 1250
    invoke-virtual {v4}, Landroid/os/Message;->sendToTarget()V

    .line 1242
    .end local v3    # "threadInfo":Lcom/miui/server/rtboost/SchedBoostService$BoostThreadInfo;
    add-int/lit8 v2, v2, 0x1

    goto :goto_0

    .line 1253
    .end local v2    # "i":I
    :cond_1
    iget-object v2, p0, Lcom/miui/server/rtboost/SchedBoostService;->mBoostingMap:Lcom/miui/server/rtboost/SchedBoostService$BoostingPackageMap;

    invoke-virtual {v2}, Lcom/miui/server/rtboost/SchedBoostService$BoostingPackageMap;->clear()V

    .line 1254
    .end local v1    # "list":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/miui/server/rtboost/SchedBoostService$BoostThreadInfo;>;"
    monitor-exit v0

    .line 1255
    return-void

    .line 1254
    :catchall_0
    move-exception v1

    monitor-exit v0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v1
.end method
