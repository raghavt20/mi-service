class com.miui.server.rtboost.SchedBoostService$BoostingPackageMap {
	 /* .source "SchedBoostService.java" */
	 /* # annotations */
	 /* .annotation system Ldalvik/annotation/EnclosingClass; */
	 /* value = Lcom/miui/server/rtboost/SchedBoostService; */
} // .end annotation
/* .annotation system Ldalvik/annotation/InnerClass; */
/* accessFlags = 0x2 */
/* name = "BoostingPackageMap" */
} // .end annotation
/* # instance fields */
private final java.util.ArrayList infoList;
/* .annotation system Ldalvik/annotation/Signature; */
/* value = { */
/* "Ljava/util/ArrayList<", */
/* "Lcom/miui/server/rtboost/SchedBoostService$BoostThreadInfo;", */
/* ">;" */
/* } */
} // .end annotation
} // .end field
private final java.util.HashMap mMap;
/* .annotation system Ldalvik/annotation/Signature; */
/* value = { */
/* "Ljava/util/HashMap<", */
/* "Ljava/lang/Integer;", */
/* "Lcom/miui/server/rtboost/SchedBoostService$BoostThreadInfo;", */
/* ">;" */
/* } */
} // .end annotation
} // .end field
private final java.lang.Object mMapLock;
final com.miui.server.rtboost.SchedBoostService this$0; //synthetic
/* # direct methods */
static java.util.ArrayList -$$Nest$fgetinfoList ( com.miui.server.rtboost.SchedBoostService$BoostingPackageMap p0 ) { //bridge//synthethic
/* .locals 0 */
p0 = this.infoList;
} // .end method
static java.lang.Object -$$Nest$fgetmMapLock ( com.miui.server.rtboost.SchedBoostService$BoostingPackageMap p0 ) { //bridge//synthethic
/* .locals 0 */
p0 = this.mMapLock;
} // .end method
private com.miui.server.rtboost.SchedBoostService$BoostingPackageMap ( ) {
/* .locals 0 */
/* .line 984 */
this.this$0 = p1;
/* invoke-direct {p0}, Ljava/lang/Object;-><init>()V */
/* .line 985 */
/* new-instance p1, Ljava/util/HashMap; */
/* invoke-direct {p1}, Ljava/util/HashMap;-><init>()V */
this.mMap = p1;
/* .line 986 */
/* new-instance p1, Ljava/util/ArrayList; */
/* invoke-direct {p1}, Ljava/util/ArrayList;-><init>()V */
this.infoList = p1;
/* .line 987 */
/* new-instance p1, Ljava/lang/Object; */
/* invoke-direct {p1}, Ljava/lang/Object;-><init>()V */
this.mMapLock = p1;
return;
} // .end method
 com.miui.server.rtboost.SchedBoostService$BoostingPackageMap ( ) { //synthethic
/* .locals 0 */
/* invoke-direct {p0, p1}, Lcom/miui/server/rtboost/SchedBoostService$BoostingPackageMap;-><init>(Lcom/miui/server/rtboost/SchedBoostService;)V */
return;
} // .end method
/* # virtual methods */
public void clear ( ) {
/* .locals 2 */
/* .line 1019 */
v0 = this.mMapLock;
/* monitor-enter v0 */
/* .line 1020 */
try { // :try_start_0
v1 = this.mMap;
(( java.util.HashMap ) v1 ).clear ( ); // invoke-virtual {v1}, Ljava/util/HashMap;->clear()V
/* .line 1021 */
v1 = this.infoList;
(( java.util.ArrayList ) v1 ).clear ( ); // invoke-virtual {v1}, Ljava/util/ArrayList;->clear()V
/* .line 1022 */
/* monitor-exit v0 */
/* .line 1023 */
return;
/* .line 1022 */
/* :catchall_0 */
/* move-exception v1 */
/* monitor-exit v0 */
/* :try_end_0 */
/* .catchall {:try_start_0 ..:try_end_0} :catchall_0 */
/* throw v1 */
} // .end method
public com.miui.server.rtboost.SchedBoostService$BoostThreadInfo getByTid ( Integer p0 ) {
/* .locals 3 */
/* .param p1, "tid" # I */
/* .line 1026 */
v0 = this.mMapLock;
/* monitor-enter v0 */
/* .line 1027 */
try { // :try_start_0
v1 = this.mMap;
java.lang.Integer .valueOf ( p1 );
(( java.util.HashMap ) v1 ).get ( v2 ); // invoke-virtual {v1, v2}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;
/* check-cast v1, Lcom/miui/server/rtboost/SchedBoostService$BoostThreadInfo; */
/* monitor-exit v0 */
/* .line 1028 */
/* :catchall_0 */
/* move-exception v1 */
/* monitor-exit v0 */
/* :try_end_0 */
/* .catchall {:try_start_0 ..:try_end_0} :catchall_0 */
/* throw v1 */
} // .end method
public java.util.ArrayList getList ( ) {
/* .locals 1 */
/* .annotation system Ldalvik/annotation/Signature; */
/* value = { */
/* "()", */
/* "Ljava/util/ArrayList<", */
/* "Lcom/miui/server/rtboost/SchedBoostService$BoostThreadInfo;", */
/* ">;" */
/* } */
} // .end annotation
/* .line 990 */
v0 = this.infoList;
} // .end method
public Boolean isEmpty ( ) {
/* .locals 2 */
/* .line 1032 */
v0 = this.mMapLock;
/* monitor-enter v0 */
/* .line 1033 */
try { // :try_start_0
v1 = this.infoList;
v1 = (( java.util.ArrayList ) v1 ).size ( ); // invoke-virtual {v1}, Ljava/util/ArrayList;->size()I
/* if-nez v1, :cond_0 */
v1 = this.mMap;
v1 = (( java.util.HashMap ) v1 ).isEmpty ( ); // invoke-virtual {v1}, Ljava/util/HashMap;->isEmpty()Z
if ( v1 != null) { // if-eqz v1, :cond_0
int v1 = 1; // const/4 v1, 0x1
} // :cond_0
int v1 = 0; // const/4 v1, 0x0
} // :goto_0
/* monitor-exit v0 */
/* .line 1034 */
/* :catchall_0 */
/* move-exception v1 */
/* monitor-exit v0 */
/* :try_end_0 */
/* .catchall {:try_start_0 ..:try_end_0} :catchall_0 */
/* throw v1 */
} // .end method
public void put ( com.miui.server.rtboost.SchedBoostService$BoostThreadInfo p0 ) {
/* .locals 4 */
/* .param p1, "threadInfo" # Lcom/miui/server/rtboost/SchedBoostService$BoostThreadInfo; */
/* .line 994 */
v0 = this.mMapLock;
/* monitor-enter v0 */
/* .line 995 */
try { // :try_start_0
v1 = this.infoList;
v1 = (( java.util.ArrayList ) v1 ).contains ( p1 ); // invoke-virtual {v1, p1}, Ljava/util/ArrayList;->contains(Ljava/lang/Object;)Z
if ( v1 != null) { // if-eqz v1, :cond_0
/* .line 997 */
v1 = this.mMap;
/* iget v2, p1, Lcom/miui/server/rtboost/SchedBoostService$BoostThreadInfo;->tid:I */
java.lang.Integer .valueOf ( v2 );
(( java.util.HashMap ) v1 ).get ( v2 ); // invoke-virtual {v1, v2}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;
/* check-cast v1, Lcom/miui/server/rtboost/SchedBoostService$BoostThreadInfo; */
/* .line 998 */
/* .local v1, "oldThreadInfo":Lcom/miui/server/rtboost/SchedBoostService$BoostThreadInfo; */
/* iget-wide v2, v1, Lcom/miui/server/rtboost/SchedBoostService$BoostThreadInfo;->boostStartTime:J */
/* iput-wide v2, p1, Lcom/miui/server/rtboost/SchedBoostService$BoostThreadInfo;->boostStartTime:J */
/* .line 999 */
/* iget v2, v1, Lcom/miui/server/rtboost/SchedBoostService$BoostThreadInfo;->savedPriority:I */
/* iput v2, p1, Lcom/miui/server/rtboost/SchedBoostService$BoostThreadInfo;->savedPriority:I */
/* .line 1000 */
/* iget v2, v1, Lcom/miui/server/rtboost/SchedBoostService$BoostThreadInfo;->boostPriority:I */
/* iput v2, p1, Lcom/miui/server/rtboost/SchedBoostService$BoostThreadInfo;->boostPriority:I */
/* .line 1001 */
/* iget v2, v1, Lcom/miui/server/rtboost/SchedBoostService$BoostThreadInfo;->tid:I */
(( com.miui.server.rtboost.SchedBoostService$BoostingPackageMap ) p0 ).removeByTid ( v2 ); // invoke-virtual {p0, v2}, Lcom/miui/server/rtboost/SchedBoostService$BoostingPackageMap;->removeByTid(I)V
/* .line 1004 */
} // .end local v1 # "oldThreadInfo":Lcom/miui/server/rtboost/SchedBoostService$BoostThreadInfo;
} // :cond_0
v1 = this.infoList;
(( java.util.ArrayList ) v1 ).add ( p1 ); // invoke-virtual {v1, p1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z
/* .line 1005 */
v1 = this.mMap;
/* iget v2, p1, Lcom/miui/server/rtboost/SchedBoostService$BoostThreadInfo;->tid:I */
java.lang.Integer .valueOf ( v2 );
(( java.util.HashMap ) v1 ).put ( v2, p1 ); // invoke-virtual {v1, v2, p1}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;
/* .line 1006 */
/* monitor-exit v0 */
/* .line 1007 */
return;
/* .line 1006 */
/* :catchall_0 */
/* move-exception v1 */
/* monitor-exit v0 */
/* :try_end_0 */
/* .catchall {:try_start_0 ..:try_end_0} :catchall_0 */
/* throw v1 */
} // .end method
public void removeByTid ( Integer p0 ) {
/* .locals 3 */
/* .param p1, "tid" # I */
/* .line 1010 */
v0 = this.mMapLock;
/* monitor-enter v0 */
/* .line 1011 */
try { // :try_start_0
v1 = this.mMap;
java.lang.Integer .valueOf ( p1 );
(( java.util.HashMap ) v1 ).remove ( v2 ); // invoke-virtual {v1, v2}, Ljava/util/HashMap;->remove(Ljava/lang/Object;)Ljava/lang/Object;
/* check-cast v1, Lcom/miui/server/rtboost/SchedBoostService$BoostThreadInfo; */
/* .line 1012 */
/* .local v1, "threadInfo":Lcom/miui/server/rtboost/SchedBoostService$BoostThreadInfo; */
if ( v1 != null) { // if-eqz v1, :cond_0
/* .line 1013 */
v2 = this.infoList;
(( java.util.ArrayList ) v2 ).remove ( v1 ); // invoke-virtual {v2, v1}, Ljava/util/ArrayList;->remove(Ljava/lang/Object;)Z
/* .line 1015 */
} // .end local v1 # "threadInfo":Lcom/miui/server/rtboost/SchedBoostService$BoostThreadInfo;
} // :cond_0
/* monitor-exit v0 */
/* .line 1016 */
return;
/* .line 1015 */
/* :catchall_0 */
/* move-exception v1 */
/* monitor-exit v0 */
/* :try_end_0 */
/* .catchall {:try_start_0 ..:try_end_0} :catchall_0 */
/* throw v1 */
} // .end method
