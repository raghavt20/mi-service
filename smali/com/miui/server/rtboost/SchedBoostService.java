public class com.miui.server.rtboost.SchedBoostService extends android.os.Binder implements com.miui.server.rtboost.SchedBoostManagerInternal {
	 /* .source "SchedBoostService.java" */
	 /* # interfaces */
	 /* # annotations */
	 /* .annotation system Ldalvik/annotation/MemberClasses; */
	 /* value = { */
	 /* Lcom/miui/server/rtboost/SchedBoostService$BoostingPackageMap;, */
	 /* Lcom/miui/server/rtboost/SchedBoostService$Handler;, */
	 /* Lcom/miui/server/rtboost/SchedBoostService$LocalService;, */
	 /* Lcom/miui/server/rtboost/SchedBoostService$BoostThreadInfo;, */
	 /* Lcom/miui/server/rtboost/SchedBoostService$SchedBoostShellCommand;, */
	 /* Lcom/miui/server/rtboost/SchedBoostService$Lifecycle; */
	 /* } */
} // .end annotation
/* # static fields */
private static ALL_CPU_CORES;
private static final Integer BOOST_HOME_GPU_TIDS_MAX_RETRY;
private static final CMDLINE_OUT;
public static final Boolean DEBUG;
private static final DEFAULT_QCOM_PERF_LIST;
private static final Integer DEFAULT_UCLAMP_MAX;
private static final Integer DEFAULT_UCLAMP_MIN;
private static final Integer DEFAULT_UCLAMP_UPPER;
private static final Boolean ENABLE_RTMODE_UCLAMP;
private static final java.lang.String HOME_PACKAGE_NAME;
private static final Boolean IS_MTK_DEVICE;
public static final Boolean IS_SCHED_HOME_GPU_THREADS_ENABLED;
public static final Boolean IS_SERVICE_ENABLED;
public static final Boolean IS_UCLAMP_ENABLED;
public static final Boolean IS_UIGROUP_ENABLED;
private static final MTK_GPU_AND_CPU_BOOST_LIST;
private static final java.lang.String PLATFORM_8650;
private static final java.lang.String PLATFORM_NAME;
private static final QCOM_GPU_AND_CPU_BOOST_LIST;
private static final QCOM_GPU_AND_CPU_HIGHER_FREQ;
private static final Long REPEAT_CALL_DURATION;
private static final java.lang.String RT_THREAD_COMM_LIST;
public static final java.lang.String SERVICE_NAME;
private static final java.lang.String SYSTEMUI_PACKAGE_NAME;
public static final java.lang.String TAG;
public static final Integer THREAD_GROUP_UI;
private static final java.lang.String THREAD_NAME;
private static final Integer THREAD_PRIORITY_HIGHEST;
/* # instance fields */
private Integer TASK_UCLAMP_MAX;
private Integer TASK_UCLAMP_MIN;
private Boolean isInited;
private Boolean isNormalPolicy;
private final android.util.ArraySet mAlwaysRtTids;
/* .annotation system Ldalvik/annotation/Signature; */
/* value = { */
/* "Landroid/util/ArraySet<", */
/* "Ljava/lang/Integer;", */
/* ">;" */
/* } */
} // .end annotation
} // .end field
private Integer mBoostHomeGpuTidsTryCnt;
private com.miui.server.rtboost.SchedBoostService$BoostingPackageMap mBoostingMap;
private android.content.Context mContext;
private mDefultPerfList;
private mGpuAndCpuPerfList;
private Integer mHandle;
private com.miui.server.rtboost.SchedBoostService$Handler mHandler;
private android.os.HandlerThread mHandlerThread;
private Integer mHomeMainThreadId;
private Integer mHomeRenderThreadId;
private Long mLastPerfAcqTime;
private android.perf.PerfMTKStub mMTKPerfStub;
private java.lang.reflect.Method mMethodSetAffinity;
private java.lang.reflect.Method mMethodUclampTask;
private Boolean mPendingHomeGpuTids;
private java.lang.String mPreBoostProcessName;
private android.perf.PerfStub mQcomPerfStub;
private final android.util.ArraySet mRtTids;
/* .annotation system Ldalvik/annotation/Signature; */
/* value = { */
/* "Landroid/util/ArraySet<", */
/* "Ljava/lang/Integer;", */
/* ">;" */
/* } */
} // .end annotation
} // .end field
private com.android.server.wm.SchedBoostGesturesEvent mSchedBoostGesturesEvent;
private Boolean mScreenOff;
private Boolean mSimpleNotNeedSched;
private final android.content.BroadcastReceiver mSysStatusReceiver;
private Integer mSystemUIMainThreadId;
private Integer mSystemUIRenderThreadId;
private com.android.server.wm.WindowManagerService mWMS;
/* # direct methods */
public static void $r8$lambda$MVdnxCbI3iTc_Kk4RLYO4E0P4PY ( com.miui.server.rtboost.SchedBoostService p0, Integer p1 ) { //synthethic
/* .locals 0 */
/* invoke-direct {p0, p1}, Lcom/miui/server/rtboost/SchedBoostService;->lambda$new$0(I)V */
return;
} // .end method
static com.miui.server.rtboost.SchedBoostService$BoostingPackageMap -$$Nest$fgetmBoostingMap ( com.miui.server.rtboost.SchedBoostService p0 ) { //bridge//synthethic
/* .locals 0 */
p0 = this.mBoostingMap;
} // .end method
static void -$$Nest$fputmScreenOff ( com.miui.server.rtboost.SchedBoostService p0, Boolean p1 ) { //bridge//synthethic
/* .locals 0 */
/* iput-boolean p1, p0, Lcom/miui/server/rtboost/SchedBoostService;->mScreenOff:Z */
return;
} // .end method
static void -$$Nest$mhandleBeginSchedThread ( com.miui.server.rtboost.SchedBoostService p0, Integer[] p1, Long p2, java.lang.String p3, Integer p4 ) { //bridge//synthethic
/* .locals 0 */
/* invoke-direct/range {p0 ..p5}, Lcom/miui/server/rtboost/SchedBoostService;->handleBeginSchedThread([IJLjava/lang/String;I)V */
return;
} // .end method
static void -$$Nest$mhandleBoostFreq ( com.miui.server.rtboost.SchedBoostService p0, Integer[] p1, Long p2, java.lang.String p3, Integer p4 ) { //bridge//synthethic
/* .locals 0 */
/* invoke-direct/range {p0 ..p5}, Lcom/miui/server/rtboost/SchedBoostService;->handleBoostFreq([IJLjava/lang/String;I)V */
return;
} // .end method
static void -$$Nest$mhandleReSetThreadsFIFO ( com.miui.server.rtboost.SchedBoostService p0, Integer p1, Long p2, Integer p3 ) { //bridge//synthethic
/* .locals 0 */
/* invoke-direct {p0, p1, p2, p3, p4}, Lcom/miui/server/rtboost/SchedBoostService;->handleReSetThreadsFIFO(IJI)V */
return;
} // .end method
static void -$$Nest$mhandleResetSchedThread ( com.miui.server.rtboost.SchedBoostService p0, com.miui.server.rtboost.SchedBoostService$BoostThreadInfo p1 ) { //bridge//synthethic
/* .locals 0 */
/* invoke-direct {p0, p1}, Lcom/miui/server/rtboost/SchedBoostService;->handleResetSchedThread(Lcom/miui/server/rtboost/SchedBoostService$BoostThreadInfo;)V */
return;
} // .end method
static void -$$Nest$mhandleSchedThread ( com.miui.server.rtboost.SchedBoostService p0, com.miui.server.rtboost.SchedBoostService$BoostThreadInfo p1 ) { //bridge//synthethic
/* .locals 0 */
/* invoke-direct {p0, p1}, Lcom/miui/server/rtboost/SchedBoostService;->handleSchedThread(Lcom/miui/server/rtboost/SchedBoostService$BoostThreadInfo;)V */
return;
} // .end method
static void -$$Nest$mhandleSetThreadsAlwaysFIFO ( com.miui.server.rtboost.SchedBoostService p0, Integer[] p1 ) { //bridge//synthethic
/* .locals 0 */
/* invoke-direct {p0, p1}, Lcom/miui/server/rtboost/SchedBoostService;->handleSetThreadsAlwaysFIFO([I)V */
return;
} // .end method
static void -$$Nest$mhandleSetThreadsFIFO ( com.miui.server.rtboost.SchedBoostService p0, Integer p1, Long p2, Integer p3 ) { //bridge//synthethic
/* .locals 0 */
/* invoke-direct {p0, p1, p2, p3, p4}, Lcom/miui/server/rtboost/SchedBoostService;->handleSetThreadsFIFO(IJI)V */
return;
} // .end method
static void -$$Nest$mhandleThreadsBindBigCoreMsg ( com.miui.server.rtboost.SchedBoostService p0, java.lang.Integer p1, Long p2 ) { //bridge//synthethic
/* .locals 0 */
/* invoke-direct {p0, p1, p2, p3}, Lcom/miui/server/rtboost/SchedBoostService;->handleThreadsBindBigCoreMsg(Ljava/lang/Integer;J)V */
return;
} // .end method
static void -$$Nest$mhandleThreadsUnBindBigCoreMsg ( com.miui.server.rtboost.SchedBoostService p0, java.lang.Integer p1 ) { //bridge//synthethic
/* .locals 0 */
/* invoke-direct {p0, p1}, Lcom/miui/server/rtboost/SchedBoostService;->handleThreadsUnBindBigCoreMsg(Ljava/lang/Integer;)V */
return;
} // .end method
static com.miui.server.rtboost.SchedBoostService ( ) {
/* .locals 3 */
/* .line 56 */
/* sget-boolean v0, Lcom/android/server/wm/RealTimeModeControllerImpl;->DEBUG:Z */
com.miui.server.rtboost.SchedBoostService.DEBUG = (v0!= 0);
/* .line 66 */
final String v0 = "ro.vendor.qti.soc_name"; // const-string v0, "ro.vendor.qti.soc_name"
android.os.SystemProperties .get ( v0 );
/* .line 68 */
/* nop */
/* .line 69 */
final String v0 = "is_mediatek"; // const-string v0, "is_mediatek"
int v1 = 0; // const/4 v1, 0x0
v0 = miui.util.FeatureParser .getBoolean ( v0,v1 );
com.miui.server.rtboost.SchedBoostService.IS_MTK_DEVICE = (v0!= 0);
/* .line 70 */
/* nop */
/* .line 71 */
final String v2 = "persist.sys.enable_rtmode_uclamp"; // const-string v2, "persist.sys.enable_rtmode_uclamp"
v2 = android.os.SystemProperties .getBoolean ( v2,v1 );
com.miui.server.rtboost.SchedBoostService.ENABLE_RTMODE_UCLAMP = (v2!= 0);
/* .line 72 */
if ( v0 != null) { // if-eqz v0, :cond_0
if ( v2 != null) { // if-eqz v2, :cond_0
int v0 = 1; // const/4 v0, 0x1
} // :cond_0
/* move v0, v1 */
} // :goto_0
com.miui.server.rtboost.SchedBoostService.IS_UCLAMP_ENABLED = (v0!= 0);
/* .line 73 */
/* nop */
/* .line 74 */
final String v0 = "persist.sys.enable_setuicgroup"; // const-string v0, "persist.sys.enable_setuicgroup"
v0 = android.os.SystemProperties .getBoolean ( v0,v1 );
com.miui.server.rtboost.SchedBoostService.IS_UIGROUP_ENABLED = (v0!= 0);
/* .line 75 */
/* nop */
/* .line 76 */
final String v0 = "persist.sys.enable_sched_gpu_threads"; // const-string v0, "persist.sys.enable_sched_gpu_threads"
v0 = android.os.SystemProperties .getBoolean ( v0,v1 );
com.miui.server.rtboost.SchedBoostService.IS_SCHED_HOME_GPU_THREADS_ENABLED = (v0!= 0);
/* .line 77 */
/* const/16 v0, 0x1000 */
/* filled-new-array {v0}, [I */
/* .line 81 */
int v0 = 0; // const/4 v0, 0x0
/* .line 88 */
final String v0 = "mali-cpu-comman"; // const-string v0, "mali-cpu-comman"
final String v1 = "ged-swd"; // const-string v1, "ged-swd"
final String v2 = "mali-event-hand"; // const-string v2, "mali-event-hand"
/* filled-new-array {v2, v0, v1}, [Ljava/lang/String; */
/* .line 109 */
int v0 = 6; // const/4 v0, 0x6
/* new-array v0, v0, [I */
/* fill-array-data v0, :array_0 */
/* .line 114 */
/* const/16 v0, 0x10 */
/* new-array v1, v0, [I */
/* fill-array-data v1, :array_1 */
/* .line 125 */
/* new-array v0, v0, [I */
/* fill-array-data v0, :array_2 */
/* .line 135 */
/* const/16 v0, 0x16 */
/* new-array v0, v0, [I */
/* fill-array-data v0, :array_3 */
return;
/* nop */
/* :array_0 */
/* .array-data 4 */
/* 0x40800000 # 4.0f */
/* 0x5dc */
/* 0x42804000 # 64.125f */
/* 0x0 */
/* 0x42c20000 # 97.0f */
/* 0x1 */
} // .end array-data
/* :array_1 */
/* .array-data 4 */
/* 0x40400001 # 3.0000002f */
/* 0x1 */
/* 0x40c00000 # 6.0f */
/* 0x1 */
/* 0x40804000 # 4.0078125f */
/* 0xfff */
/* 0x40804100 */
/* 0xfff */
/* 0x40800000 # 4.0f */
/* 0x6a4 */
/* 0x40800100 # 4.000122f */
/* 0x6a4 */
/* 0x42804000 # 64.125f */
/* 0x0 */
/* 0x42c20000 # 97.0f */
/* 0x1 */
} // .end array-data
/* :array_2 */
/* .array-data 4 */
/* 0x40400001 # 3.0000002f */
/* 0x1 */
/* 0x40c00000 # 6.0f */
/* 0x1 */
/* 0x40804000 # 4.0078125f */
/* 0xfff */
/* 0x40804100 */
/* 0xfff */
/* 0x40800000 # 4.0f */
/* 0x8fc */
/* 0x40800100 # 4.000122f */
/* 0xfff */
/* 0x42804000 # 64.125f */
/* 0x0 */
/* 0x42c20000 # 97.0f */
/* 0x1 */
} // .end array-data
/* :array_3 */
/* .array-data 4 */
/* 0x400000 */
/* 0x1b7740 */
/* 0x400100 */
/* 0x1e8480 */
/* 0x400200 */
/* 0x1e8480 */
/* 0x1438400 */
/* 0x64 */
/* 0x1438500 */
/* 0x64 */
/* 0xc00000 */
/* 0x23 */
/* 0xc0c200 */
/* 0x11e003c */
/* 0xc0c300 */
/* 0x303 */
/* 0x1404100 */
/* 0x1 */
/* 0x1404300 */
/* 0x0 */
/* 0x1404400 */
/* 0x1 */
} // .end array-data
} // .end method
public com.miui.server.rtboost.SchedBoostService ( ) {
/* .locals 3 */
/* .param p1, "context" # Landroid/content/Context; */
/* .line 204 */
/* invoke-direct {p0}, Landroid/os/Binder;-><init>()V */
/* .line 82 */
/* nop */
/* .line 83 */
final String v0 = "persist.sys.speedui_uclamp_min"; // const-string v0, "persist.sys.speedui_uclamp_min"
/* const/16 v1, 0x266 */
v0 = android.os.SystemProperties .getInt ( v0,v1 );
/* iput v0, p0, Lcom/miui/server/rtboost/SchedBoostService;->TASK_UCLAMP_MIN:I */
/* .line 84 */
/* nop */
/* .line 85 */
final String v0 = "persist.sys.speedui_uclamp_max"; // const-string v0, "persist.sys.speedui_uclamp_max"
/* const/16 v1, 0x400 */
v0 = android.os.SystemProperties .getInt ( v0,v1 );
/* iput v0, p0, Lcom/miui/server/rtboost/SchedBoostService;->TASK_UCLAMP_MAX:I */
/* .line 94 */
int v0 = 1; // const/4 v0, 0x1
/* iput-boolean v0, p0, Lcom/miui/server/rtboost/SchedBoostService;->mPendingHomeGpuTids:Z */
/* .line 95 */
int v0 = 0; // const/4 v0, 0x0
/* iput v0, p0, Lcom/miui/server/rtboost/SchedBoostService;->mBoostHomeGpuTidsTryCnt:I */
/* .line 102 */
/* iput v0, p0, Lcom/miui/server/rtboost/SchedBoostService;->mHandle:I */
/* .line 103 */
/* const-wide/16 v1, 0x0 */
/* iput-wide v1, p0, Lcom/miui/server/rtboost/SchedBoostService;->mLastPerfAcqTime:J */
/* .line 152 */
/* new-instance v1, Lcom/miui/server/rtboost/SchedBoostService$BoostingPackageMap; */
int v2 = 0; // const/4 v2, 0x0
/* invoke-direct {v1, p0, v2}, Lcom/miui/server/rtboost/SchedBoostService$BoostingPackageMap;-><init>(Lcom/miui/server/rtboost/SchedBoostService;Lcom/miui/server/rtboost/SchedBoostService$BoostingPackageMap-IA;)V */
this.mBoostingMap = v1;
/* .line 153 */
/* new-instance v1, Landroid/util/ArraySet; */
/* invoke-direct {v1}, Landroid/util/ArraySet;-><init>()V */
this.mAlwaysRtTids = v1;
/* .line 154 */
/* new-instance v1, Landroid/util/ArraySet; */
/* invoke-direct {v1}, Landroid/util/ArraySet;-><init>()V */
this.mRtTids = v1;
/* .line 155 */
this.mMethodSetAffinity = v2;
/* .line 156 */
this.mMethodUclampTask = v2;
/* .line 159 */
this.mPreBoostProcessName = v2;
/* .line 160 */
/* iput v0, p0, Lcom/miui/server/rtboost/SchedBoostService;->mHomeMainThreadId:I */
/* .line 161 */
/* iput v0, p0, Lcom/miui/server/rtboost/SchedBoostService;->mHomeRenderThreadId:I */
/* .line 162 */
/* iput v0, p0, Lcom/miui/server/rtboost/SchedBoostService;->mSystemUIMainThreadId:I */
/* .line 163 */
/* iput v0, p0, Lcom/miui/server/rtboost/SchedBoostService;->mSystemUIRenderThreadId:I */
/* .line 168 */
/* iput-boolean v0, p0, Lcom/miui/server/rtboost/SchedBoostService;->mScreenOff:Z */
/* .line 170 */
/* new-instance v0, Lcom/miui/server/rtboost/SchedBoostService$1; */
/* invoke-direct {v0, p0}, Lcom/miui/server/rtboost/SchedBoostService$1;-><init>(Lcom/miui/server/rtboost/SchedBoostService;)V */
this.mSysStatusReceiver = v0;
/* .line 205 */
this.mContext = p1;
/* .line 207 */
/* if-gtz v0, :cond_0 */
/* .line 208 */
return;
/* .line 210 */
} // :cond_0
/* new-array v0, v0, [I */
/* .line 211 */
int v0 = 0; // const/4 v0, 0x0
/* .local v0, "i":I */
} // :goto_0
/* if-ge v0, v1, :cond_1 */
/* .line 212 */
v1 = com.miui.server.rtboost.SchedBoostService.ALL_CPU_CORES;
/* aput v0, v1, v0 */
/* .line 211 */
/* add-int/lit8 v0, v0, 0x1 */
/* .line 215 */
} // .end local v0 # "i":I
} // :cond_1
/* const-string/jumbo v0, "window" */
android.os.ServiceManager .getService ( v0 );
/* check-cast v0, Lcom/android/server/wm/WindowManagerService; */
this.mWMS = v0;
/* .line 217 */
/* sget-boolean v0, Lcom/android/server/wm/RealTimeModeControllerImpl;->ENABLE_RT_MODE:Z */
if ( v0 != null) { // if-eqz v0, :cond_2
v0 = /* invoke-direct {p0}, Lcom/miui/server/rtboost/SchedBoostService;->init()Z */
if ( v0 != null) { // if-eqz v0, :cond_2
/* .line 218 */
/* new-instance v0, Lcom/miui/server/rtboost/SchedBoostService$2; */
final String v1 = "SchedBoostServiceTh"; // const-string v1, "SchedBoostServiceTh"
/* invoke-direct {v0, p0, v1}, Lcom/miui/server/rtboost/SchedBoostService$2;-><init>(Lcom/miui/server/rtboost/SchedBoostService;Ljava/lang/String;)V */
this.mHandlerThread = v0;
/* .line 227 */
(( android.os.HandlerThread ) v0 ).start ( ); // invoke-virtual {v0}, Landroid/os/HandlerThread;->start()V
/* .line 228 */
/* new-instance v0, Lcom/miui/server/rtboost/SchedBoostService$Handler; */
v1 = this.mHandlerThread;
(( android.os.HandlerThread ) v1 ).getLooper ( ); // invoke-virtual {v1}, Landroid/os/HandlerThread;->getLooper()Landroid/os/Looper;
/* invoke-direct {v0, p0, v1}, Lcom/miui/server/rtboost/SchedBoostService$Handler;-><init>(Lcom/miui/server/rtboost/SchedBoostService;Landroid/os/Looper;)V */
this.mHandler = v0;
/* .line 229 */
/* const-class v0, Lcom/miui/server/rtboost/SchedBoostManagerInternal; */
/* new-instance v1, Lcom/miui/server/rtboost/SchedBoostService$LocalService; */
/* invoke-direct {v1, p0, v2}, Lcom/miui/server/rtboost/SchedBoostService$LocalService;-><init>(Lcom/miui/server/rtboost/SchedBoostService;Lcom/miui/server/rtboost/SchedBoostService$LocalService-IA;)V */
com.android.server.LocalServices .addService ( v0,v1 );
/* .line 231 */
com.android.server.wm.RealTimeModeControllerStub .get ( );
v1 = this.mContext;
/* .line 232 */
com.android.server.wm.RealTimeModeControllerStub .get ( );
v1 = this.mWMS;
/* .line 234 */
/* new-instance v0, Lcom/android/server/wm/SchedBoostGesturesEvent; */
v1 = this.mHandlerThread;
(( android.os.HandlerThread ) v1 ).getLooper ( ); // invoke-virtual {v1}, Landroid/os/HandlerThread;->getLooper()Landroid/os/Looper;
/* invoke-direct {v0, v1}, Lcom/android/server/wm/SchedBoostGesturesEvent;-><init>(Landroid/os/Looper;)V */
this.mSchedBoostGesturesEvent = v0;
/* .line 235 */
(( com.android.server.wm.SchedBoostGesturesEvent ) v0 ).init ( p1 ); // invoke-virtual {v0, p1}, Lcom/android/server/wm/SchedBoostGesturesEvent;->init(Landroid/content/Context;)V
/* .line 236 */
v0 = this.mSchedBoostGesturesEvent;
/* new-instance v1, Lcom/miui/server/rtboost/SchedBoostService$3; */
/* invoke-direct {v1, p0}, Lcom/miui/server/rtboost/SchedBoostService$3;-><init>(Lcom/miui/server/rtboost/SchedBoostService;)V */
(( com.android.server.wm.SchedBoostGesturesEvent ) v0 ).setGesturesEventListener ( v1 ); // invoke-virtual {v0, v1}, Lcom/android/server/wm/SchedBoostGesturesEvent;->setGesturesEventListener(Lcom/android/server/wm/SchedBoostGesturesEvent$GesturesEventListener;)V
/* .line 268 */
com.android.server.am.SystemPressureController .getInstance ( );
/* new-instance v1, Lcom/miui/server/rtboost/SchedBoostService$$ExternalSyntheticLambda0; */
/* invoke-direct {v1, p0}, Lcom/miui/server/rtboost/SchedBoostService$$ExternalSyntheticLambda0;-><init>(Lcom/miui/server/rtboost/SchedBoostService;)V */
(( com.android.server.am.SystemPressureController ) v0 ).registerThermalTempListener ( v1 ); // invoke-virtual {v0, v1}, Lcom/android/server/am/SystemPressureController;->registerThermalTempListener(Lcom/android/server/am/ThermalTempListener;)V
/* .line 271 */
/* new-instance v0, Landroid/content/IntentFilter; */
/* invoke-direct {v0}, Landroid/content/IntentFilter;-><init>()V */
/* .line 272 */
/* .local v0, "filter":Landroid/content/IntentFilter; */
final String v1 = "android.intent.action.SCREEN_OFF"; // const-string v1, "android.intent.action.SCREEN_OFF"
(( android.content.IntentFilter ) v0 ).addAction ( v1 ); // invoke-virtual {v0, v1}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V
/* .line 273 */
final String v1 = "android.intent.action.SCREEN_ON"; // const-string v1, "android.intent.action.SCREEN_ON"
(( android.content.IntentFilter ) v0 ).addAction ( v1 ); // invoke-virtual {v0, v1}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V
/* .line 274 */
v1 = this.mSysStatusReceiver;
(( android.content.Context ) p1 ).registerReceiver ( v1, v0 ); // invoke-virtual {p1, v1, v0}, Landroid/content/Context;->registerReceiver(Landroid/content/BroadcastReceiver;Landroid/content/IntentFilter;)Landroid/content/Intent;
/* .line 276 */
} // .end local v0 # "filter":Landroid/content/IntentFilter;
} // :cond_2
return;
} // .end method
private void changeSchedBoostPolicy ( Integer p0 ) {
/* .locals 3 */
/* .param p1, "temp" # I */
/* .line 1201 */
/* sget-boolean v0, Lcom/android/server/wm/RealTimeModeControllerImpl;->ENABLE_RT_MODE:Z */
/* if-nez v0, :cond_0 */
/* .line 1202 */
return;
/* .line 1204 */
} // :cond_0
/* sget-boolean v0, Lcom/android/server/wm/RealTimeModeControllerImpl;->ENABLE_TEMP_LIMIT_ENABLE:Z */
/* if-nez v0, :cond_1 */
/* .line 1205 */
return;
/* .line 1207 */
} // :cond_1
final String v1 = "SchedBoost"; // const-string v1, "SchedBoost"
int v2 = 0; // const/4 v2, 0x0
/* if-lt p1, v0, :cond_2 */
/* iget-boolean v0, p0, Lcom/miui/server/rtboost/SchedBoostService;->isNormalPolicy:Z */
/* if-nez v0, :cond_2 */
/* .line 1208 */
int v0 = 1; // const/4 v0, 0x1
/* iput-boolean v0, p0, Lcom/miui/server/rtboost/SchedBoostService;->isNormalPolicy:Z */
/* .line 1209 */
final String v0 = "onThermalTempChange, set sched policy OTHER"; // const-string v0, "onThermalTempChange, set sched policy OTHER"
/* new-array v2, v2, [Ljava/lang/Object; */
java.lang.String .format ( v0,v2 );
android.util.Slog .d ( v1,v0 );
/* .line 1210 */
} // :cond_2
/* if-gt p1, v0, :cond_3 */
/* iget-boolean v0, p0, Lcom/miui/server/rtboost/SchedBoostService;->isNormalPolicy:Z */
if ( v0 != null) { // if-eqz v0, :cond_3
/* .line 1211 */
/* iput-boolean v2, p0, Lcom/miui/server/rtboost/SchedBoostService;->isNormalPolicy:Z */
/* .line 1212 */
final String v0 = "onThermalTempChange, set sched policy FIFO"; // const-string v0, "onThermalTempChange, set sched policy FIFO"
/* new-array v2, v2, [Ljava/lang/Object; */
java.lang.String .format ( v0,v2 );
android.util.Slog .d ( v1,v0 );
/* .line 1214 */
} // :cond_3
} // :goto_0
return;
} // .end method
private void dump ( java.io.PrintWriter p0, java.lang.String[] p1 ) {
/* .locals 5 */
/* .param p1, "pw" # Ljava/io/PrintWriter; */
/* .param p2, "args" # [Ljava/lang/String; */
/* .line 1275 */
/* new-instance v0, Ljava/lang/StringBuilder; */
/* invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V */
final String v1 = "IS_MTK_DEVICE: "; // const-string v1, "IS_MTK_DEVICE: "
(( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
/* sget-boolean v1, Lcom/miui/server/rtboost/SchedBoostService;->IS_MTK_DEVICE:Z */
(( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v0 ).toString ( ); // invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
(( java.io.PrintWriter ) p1 ).println ( v0 ); // invoke-virtual {p1, v0}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V
/* .line 1276 */
if ( v1 != null) { // if-eqz v1, :cond_0
/* .line 1277 */
/* new-instance v0, Ljava/lang/StringBuilder; */
/* invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V */
final String v1 = "ENABLE_RTMODE_UCLAMP: "; // const-string v1, "ENABLE_RTMODE_UCLAMP: "
(( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
/* sget-boolean v1, Lcom/miui/server/rtboost/SchedBoostService;->ENABLE_RTMODE_UCLAMP:Z */
(( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v0 ).toString ( ); // invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
(( java.io.PrintWriter ) p1 ).println ( v0 ); // invoke-virtual {p1, v0}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V
/* .line 1278 */
/* new-instance v0, Ljava/lang/StringBuilder; */
/* invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V */
final String v1 = "TASK_UCLAMP_MIN: "; // const-string v1, "TASK_UCLAMP_MIN: "
(( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
/* iget v1, p0, Lcom/miui/server/rtboost/SchedBoostService;->TASK_UCLAMP_MIN:I */
(( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v0 ).toString ( ); // invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
(( java.io.PrintWriter ) p1 ).println ( v0 ); // invoke-virtual {p1, v0}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V
/* .line 1279 */
/* new-instance v0, Ljava/lang/StringBuilder; */
/* invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V */
final String v1 = "TASK_UCLAMP_MIN: "; // const-string v1, "TASK_UCLAMP_MIN: "
(( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
/* iget v1, p0, Lcom/miui/server/rtboost/SchedBoostService;->TASK_UCLAMP_MAX:I */
(( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v0 ).toString ( ); // invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
(( java.io.PrintWriter ) p1 ).println ( v0 ); // invoke-virtual {p1, v0}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V
/* .line 1281 */
} // :cond_0
/* new-instance v0, Ljava/lang/StringBuilder; */
/* invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V */
final String v1 = "mPreBoostProcessName: "; // const-string v1, "mPreBoostProcessName: "
(( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
v1 = this.mPreBoostProcessName;
(( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v0 ).toString ( ); // invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
(( java.io.PrintWriter ) p1 ).println ( v0 ); // invoke-virtual {p1, v0}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V
/* .line 1282 */
/* new-instance v0, Ljava/lang/StringBuilder; */
/* invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V */
final String v1 = "currently isNormalPolicy: "; // const-string v1, "currently isNormalPolicy: "
(( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
/* iget-boolean v1, p0, Lcom/miui/server/rtboost/SchedBoostService;->isNormalPolicy:Z */
(( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v0 ).toString ( ); // invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
(( java.io.PrintWriter ) p1 ).println ( v0 ); // invoke-virtual {p1, v0}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V
/* .line 1283 */
final String v0 = "AlwaysRtTids: "; // const-string v0, "AlwaysRtTids: "
(( java.io.PrintWriter ) p1 ).println ( v0 ); // invoke-virtual {p1, v0}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V
/* .line 1284 */
v0 = this.mAlwaysRtTids;
/* monitor-enter v0 */
/* .line 1285 */
try { // :try_start_0
v1 = this.mAlwaysRtTids;
(( android.util.ArraySet ) v1 ).iterator ( ); // invoke-virtual {v1}, Landroid/util/ArraySet;->iterator()Ljava/util/Iterator;
v2 = } // :goto_0
if ( v2 != null) { // if-eqz v2, :cond_1
/* check-cast v2, Ljava/lang/Integer; */
v2 = (( java.lang.Integer ) v2 ).intValue ( ); // invoke-virtual {v2}, Ljava/lang/Integer;->intValue()I
/* .line 1286 */
/* .local v2, "tid":I */
/* new-instance v3, Ljava/lang/StringBuilder; */
/* invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V */
final String v4 = " "; // const-string v4, " "
(( java.lang.StringBuilder ) v3 ).append ( v4 ); // invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v3 ).append ( v2 ); // invoke-virtual {v3, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v3 ).toString ( ); // invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
(( java.io.PrintWriter ) p1 ).println ( v3 ); // invoke-virtual {p1, v3}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V
/* .line 1287 */
} // .end local v2 # "tid":I
/* .line 1288 */
} // :cond_1
/* monitor-exit v0 */
/* :try_end_0 */
/* .catchall {:try_start_0 ..:try_end_0} :catchall_1 */
/* .line 1289 */
final String v0 = "Boosting Threads: "; // const-string v0, "Boosting Threads: "
(( java.io.PrintWriter ) p1 ).println ( v0 ); // invoke-virtual {p1, v0}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V
/* .line 1290 */
v0 = this.mBoostingMap;
com.miui.server.rtboost.SchedBoostService$BoostingPackageMap .-$$Nest$fgetmMapLock ( v0 );
/* monitor-enter v1 */
/* .line 1291 */
try { // :try_start_1
v0 = this.mBoostingMap;
com.miui.server.rtboost.SchedBoostService$BoostingPackageMap .-$$Nest$fgetinfoList ( v0 );
(( java.util.ArrayList ) v0 ).iterator ( ); // invoke-virtual {v0}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;
v2 = } // :goto_1
if ( v2 != null) { // if-eqz v2, :cond_2
/* check-cast v2, Lcom/miui/server/rtboost/SchedBoostService$BoostThreadInfo; */
/* .line 1292 */
/* .local v2, "info":Lcom/miui/server/rtboost/SchedBoostService$BoostThreadInfo; */
(( com.miui.server.rtboost.SchedBoostService$BoostThreadInfo ) v2 ).dump ( p1, p2 ); // invoke-virtual {v2, p1, p2}, Lcom/miui/server/rtboost/SchedBoostService$BoostThreadInfo;->dump(Ljava/io/PrintWriter;[Ljava/lang/String;)V
/* .line 1293 */
} // .end local v2 # "info":Lcom/miui/server/rtboost/SchedBoostService$BoostThreadInfo;
/* .line 1294 */
} // :cond_2
/* monitor-exit v1 */
/* .line 1295 */
return;
/* .line 1294 */
/* :catchall_0 */
/* move-exception v0 */
/* monitor-exit v1 */
/* :try_end_1 */
/* .catchall {:try_start_1 ..:try_end_1} :catchall_0 */
/* throw v0 */
/* .line 1288 */
/* :catchall_1 */
/* move-exception v1 */
try { // :try_start_2
/* monitor-exit v0 */
/* :try_end_2 */
/* .catchall {:try_start_2 ..:try_end_2} :catchall_1 */
/* throw v1 */
} // .end method
private java.util.ArrayList getCurrentBoostThreadsList ( Integer[] p0, Long p1, java.lang.String p2, Integer p3 ) {
/* .locals 14 */
/* .param p1, "tids" # [I */
/* .param p2, "duration" # J */
/* .param p4, "procName" # Ljava/lang/String; */
/* .param p5, "mode" # I */
/* .annotation system Ldalvik/annotation/Signature; */
/* value = { */
/* "([IJ", */
/* "Ljava/lang/String;", */
/* "I)", */
/* "Ljava/util/ArrayList<", */
/* "Lcom/miui/server/rtboost/SchedBoostService$BoostThreadInfo;", */
/* ">;" */
/* } */
} // .end annotation
/* .line 876 */
/* move-object v7, p0 */
/* move-object v8, p1 */
/* new-instance v0, Ljava/util/ArrayList; */
/* invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V */
/* move-object v9, v0 */
/* .line 878 */
/* .local v9, "curList":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/miui/server/rtboost/SchedBoostService$BoostThreadInfo;>;" */
/* array-length v10, v8 */
int v0 = 0; // const/4 v0, 0x0
/* move v11, v0 */
} // :goto_0
/* if-ge v11, v10, :cond_3 */
/* aget v12, v8, v11 */
/* .line 879 */
/* .local v12, "tid":I */
/* if-gtz v12, :cond_0 */
/* .line 880 */
} // :cond_0
v0 = this.mAlwaysRtTids;
java.lang.Integer .valueOf ( v12 );
v0 = (( android.util.ArraySet ) v0 ).contains ( v1 ); // invoke-virtual {v0, v1}, Landroid/util/ArraySet;->contains(Ljava/lang/Object;)Z
if ( v0 != null) { // if-eqz v0, :cond_1
/* .line 881 */
/* sget-boolean v0, Lcom/miui/server/rtboost/SchedBoostService;->DEBUG:Z */
if ( v0 != null) { // if-eqz v0, :cond_2
/* .line 882 */
/* new-instance v0, Ljava/lang/StringBuilder; */
/* invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V */
final String v1 = "already rttids:"; // const-string v1, "already rttids:"
(( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
v1 = this.mAlwaysRtTids;
(( android.util.ArraySet ) v1 ).toString ( ); // invoke-virtual {v1}, Landroid/util/ArraySet;->toString()Ljava/lang/String;
(( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
final String v1 = ",tid :"; // const-string v1, ",tid :"
(( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v0 ).append ( v12 ); // invoke-virtual {v0, v12}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v0 ).toString ( ); // invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
final String v1 = "SchedBoost"; // const-string v1, "SchedBoost"
android.util.Slog .d ( v1,v0 );
/* .line 886 */
} // :cond_1
/* new-instance v13, Lcom/miui/server/rtboost/SchedBoostService$BoostThreadInfo; */
/* move-object v0, v13 */
/* move-object v1, p0 */
/* move v2, v12 */
/* move-wide/from16 v3, p2 */
/* move-object/from16 v5, p4 */
/* move/from16 v6, p5 */
/* invoke-direct/range {v0 ..v6}, Lcom/miui/server/rtboost/SchedBoostService$BoostThreadInfo;-><init>(Lcom/miui/server/rtboost/SchedBoostService;IJLjava/lang/String;I)V */
/* .line 887 */
/* .local v0, "threadInfo":Lcom/miui/server/rtboost/SchedBoostService$BoostThreadInfo; */
v1 = this.mBoostingMap;
(( com.miui.server.rtboost.SchedBoostService$BoostingPackageMap ) v1 ).put ( v0 ); // invoke-virtual {v1, v0}, Lcom/miui/server/rtboost/SchedBoostService$BoostingPackageMap;->put(Lcom/miui/server/rtboost/SchedBoostService$BoostThreadInfo;)V
/* .line 888 */
(( java.util.ArrayList ) v9 ).add ( v0 ); // invoke-virtual {v9, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z
/* .line 878 */
} // .end local v0 # "threadInfo":Lcom/miui/server/rtboost/SchedBoostService$BoostThreadInfo;
} // .end local v12 # "tid":I
} // :cond_2
} // :goto_1
/* add-int/lit8 v11, v11, 0x1 */
/* .line 891 */
} // :cond_3
} // .end method
private void handleBeginSchedThread ( Integer[] p0, Long p1, java.lang.String p2, Integer p3 ) {
/* .locals 6 */
/* .param p1, "tids" # [I */
/* .param p2, "duration" # J */
/* .param p4, "procName" # Ljava/lang/String; */
/* .param p5, "mode" # I */
/* .line 392 */
v0 = this.mPreBoostProcessName;
if ( v0 != null) { // if-eqz v0, :cond_3
v0 = android.text.TextUtils .equals ( v0,p4 );
/* if-nez v0, :cond_3 */
/* .line 393 */
v0 = this.mBoostingMap;
com.miui.server.rtboost.SchedBoostService$BoostingPackageMap .-$$Nest$fgetmMapLock ( v0 );
/* monitor-enter v0 */
/* .line 394 */
try { // :try_start_0
v1 = this.mBoostingMap;
(( com.miui.server.rtboost.SchedBoostService$BoostingPackageMap ) v1 ).getList ( ); // invoke-virtual {v1}, Lcom/miui/server/rtboost/SchedBoostService$BoostingPackageMap;->getList()Ljava/util/ArrayList;
/* .line 395 */
/* .local v1, "list":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/miui/server/rtboost/SchedBoostService$BoostThreadInfo;>;" */
if ( v1 != null) { // if-eqz v1, :cond_2
v2 = (( java.util.ArrayList ) v1 ).size ( ); // invoke-virtual {v1}, Ljava/util/ArrayList;->size()I
/* if-lez v2, :cond_2 */
/* .line 396 */
int v2 = 0; // const/4 v2, 0x0
/* .local v2, "i":I */
} // :goto_0
v3 = (( java.util.ArrayList ) v1 ).size ( ); // invoke-virtual {v1}, Ljava/util/ArrayList;->size()I
/* if-ge v2, v3, :cond_2 */
/* .line 397 */
(( java.util.ArrayList ) v1 ).get ( v2 ); // invoke-virtual {v1, v2}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;
/* check-cast v3, Lcom/miui/server/rtboost/SchedBoostService$BoostThreadInfo; */
/* .line 399 */
/* .local v3, "threadInfo":Lcom/miui/server/rtboost/SchedBoostService$BoostThreadInfo; */
/* iget v4, v3, Lcom/miui/server/rtboost/SchedBoostService$BoostThreadInfo;->mode:I */
int v5 = 4; // const/4 v5, 0x4
/* if-eq v4, v5, :cond_0 */
/* iget v4, v3, Lcom/miui/server/rtboost/SchedBoostService$BoostThreadInfo;->mode:I */
int v5 = 5; // const/4 v5, 0x5
/* if-ne v4, v5, :cond_1 */
} // :cond_0
/* iget v4, v3, Lcom/miui/server/rtboost/SchedBoostService$BoostThreadInfo;->mode:I */
/* if-ge p5, v4, :cond_1 */
/* .line 402 */
/* monitor-exit v0 */
return;
/* .line 404 */
} // :cond_1
v4 = this.mHandler;
(( com.miui.server.rtboost.SchedBoostService$Handler ) v4 ).removeResetThreadsMsg ( v3 ); // invoke-virtual {v4, v3}, Lcom/miui/server/rtboost/SchedBoostService$Handler;->removeResetThreadsMsg(Lcom/miui/server/rtboost/SchedBoostService$BoostThreadInfo;)V
/* .line 405 */
v4 = this.mHandler;
(( com.miui.server.rtboost.SchedBoostService$Handler ) v4 ).obtainResetSchedThreadsMsg ( v3 ); // invoke-virtual {v4, v3}, Lcom/miui/server/rtboost/SchedBoostService$Handler;->obtainResetSchedThreadsMsg(Lcom/miui/server/rtboost/SchedBoostService$BoostThreadInfo;)Landroid/os/Message;
/* .line 406 */
(( android.os.Message ) v4 ).sendToTarget ( ); // invoke-virtual {v4}, Landroid/os/Message;->sendToTarget()V
/* .line 396 */
} // .end local v3 # "threadInfo":Lcom/miui/server/rtboost/SchedBoostService$BoostThreadInfo;
/* add-int/lit8 v2, v2, 0x1 */
/* .line 410 */
} // .end local v2 # "i":I
} // :cond_2
v2 = this.mBoostingMap;
(( com.miui.server.rtboost.SchedBoostService$BoostingPackageMap ) v2 ).clear ( ); // invoke-virtual {v2}, Lcom/miui/server/rtboost/SchedBoostService$BoostingPackageMap;->clear()V
/* .line 411 */
} // .end local v1 # "list":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/miui/server/rtboost/SchedBoostService$BoostThreadInfo;>;"
/* monitor-exit v0 */
/* :catchall_0 */
/* move-exception v1 */
/* monitor-exit v0 */
/* :try_end_0 */
/* .catchall {:try_start_0 ..:try_end_0} :catchall_0 */
/* throw v1 */
/* .line 414 */
} // :cond_3
} // :goto_1
this.mPreBoostProcessName = p4;
/* .line 416 */
/* nop */
/* .line 417 */
/* invoke-direct/range {p0 ..p5}, Lcom/miui/server/rtboost/SchedBoostService;->getCurrentBoostThreadsList([IJLjava/lang/String;I)Ljava/util/ArrayList; */
/* .line 420 */
/* .local v0, "curList":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/miui/server/rtboost/SchedBoostService$BoostThreadInfo;>;" */
if ( v0 != null) { // if-eqz v0, :cond_4
/* .line 421 */
v1 = (( java.util.ArrayList ) v0 ).size ( ); // invoke-virtual {v0}, Ljava/util/ArrayList;->size()I
/* .line 422 */
/* .local v1, "curListLength":I */
int v2 = 0; // const/4 v2, 0x0
/* .restart local v2 # "i":I */
} // :goto_2
/* if-ge v2, v1, :cond_4 */
/* .line 423 */
v3 = this.mHandler;
(( java.util.ArrayList ) v0 ).get ( v2 ); // invoke-virtual {v0, v2}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;
/* check-cast v4, Lcom/miui/server/rtboost/SchedBoostService$BoostThreadInfo; */
(( com.miui.server.rtboost.SchedBoostService$Handler ) v3 ).obtainSchedThreadsMsg ( v4 ); // invoke-virtual {v3, v4}, Lcom/miui/server/rtboost/SchedBoostService$Handler;->obtainSchedThreadsMsg(Lcom/miui/server/rtboost/SchedBoostService$BoostThreadInfo;)Landroid/os/Message;
/* .line 424 */
(( android.os.Message ) v3 ).sendToTarget ( ); // invoke-virtual {v3}, Landroid/os/Message;->sendToTarget()V
/* .line 422 */
/* add-int/lit8 v2, v2, 0x1 */
/* .line 428 */
} // .end local v1 # "curListLength":I
} // .end local v2 # "i":I
} // :cond_4
/* sget-boolean v1, Lcom/miui/server/rtboost/SchedBoostService;->IS_MTK_DEVICE:Z */
if ( v1 != null) { // if-eqz v1, :cond_6
/* sget-boolean v1, Lcom/miui/server/rtboost/SchedBoostService;->IS_SCHED_HOME_GPU_THREADS_ENABLED:Z */
if ( v1 != null) { // if-eqz v1, :cond_6
/* iget-boolean v1, p0, Lcom/miui/server/rtboost/SchedBoostService;->mPendingHomeGpuTids:Z */
if ( v1 != null) { // if-eqz v1, :cond_6
/* if-nez p5, :cond_6 */
/* .line 430 */
v1 = (( java.lang.String ) p4 ).isEmpty ( ); // invoke-virtual {p4}, Ljava/lang/String;->isEmpty()Z
/* if-nez v1, :cond_6 */
int v1 = 0; // const/4 v1, 0x0
/* aget v2, p1, v1 */
/* if-lez v2, :cond_6 */
/* iget v2, p0, Lcom/miui/server/rtboost/SchedBoostService;->mBoostHomeGpuTidsTryCnt:I */
int v3 = 3; // const/4 v3, 0x3
/* if-ge v2, v3, :cond_6 */
final String v2 = "com.miui.home"; // const-string v2, "com.miui.home"
/* .line 432 */
v2 = (( java.lang.String ) v2 ).equals ( p4 ); // invoke-virtual {v2, p4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z
/* if-nez v2, :cond_5 */
final String v2 = "com.mi.android.globallauncher"; // const-string v2, "com.mi.android.globallauncher"
/* .line 433 */
v2 = (( java.lang.String ) v2 ).equals ( p4 ); // invoke-virtual {v2, p4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z
if ( v2 != null) { // if-eqz v2, :cond_6
/* .line 434 */
} // :cond_5
/* aget v1, p1, v1 */
/* invoke-direct {p0, v1, p4}, Lcom/miui/server/rtboost/SchedBoostService;->schedGpuThreads(ILjava/lang/String;)V */
/* .line 436 */
} // :cond_6
return;
} // .end method
private void handleBoostFreq ( Integer[] p0, Long p1, java.lang.String p2, Integer p3 ) {
/* .locals 3 */
/* .param p1, "tids" # [I */
/* .param p2, "duration" # J */
/* .param p4, "procName" # Ljava/lang/String; */
/* .param p5, "mode" # I */
/* .line 610 */
/* sget-boolean v0, Lcom/miui/server/rtboost/SchedBoostService;->IS_MTK_DEVICE:Z */
if ( v0 != null) { // if-eqz v0, :cond_0
/* .line 611 */
v0 = this.mMTKPerfStub;
/* long-to-int v1, p2 */
v2 = this.mDefultPerfList;
/* .line 612 */
return;
/* .line 615 */
} // :cond_0
int v0 = 1; // const/4 v0, 0x1
/* packed-switch p5, :pswitch_data_0 */
/* .line 625 */
final String v0 = "SchedBoost"; // const-string v0, "SchedBoost"
final String v1 = "BoostFreq mode can not match with COLD_LAUNCH or WARM_LAUNCH"; // const-string v1, "BoostFreq mode can not match with COLD_LAUNCH or WARM_LAUNCH"
android.util.Slog .d ( v0,v1 );
/* .line 617 */
/* :pswitch_0 */
v1 = this.mQcomPerfStub;
/* long-to-int v2, p2 */
(( android.perf.PerfStub ) v1 ).perfColdLaunchBoost ( p4, v2, v0 ); // invoke-virtual {v1, p4, v2, v0}, Landroid/perf/PerfStub;->perfColdLaunchBoost(Ljava/lang/String;II)I
/* .line 619 */
/* .line 621 */
/* :pswitch_1 */
v1 = this.mQcomPerfStub;
/* long-to-int v2, p2 */
(( android.perf.PerfStub ) v1 ).perfWarmLaunchBoost ( p4, v2, v0 ); // invoke-virtual {v1, p4, v2, v0}, Landroid/perf/PerfStub;->perfWarmLaunchBoost(Ljava/lang/String;II)I
/* .line 623 */
/* nop */
/* .line 627 */
} // :goto_0
return;
/* :pswitch_data_0 */
/* .packed-switch 0x65 */
/* :pswitch_1 */
/* :pswitch_0 */
} // .end packed-switch
} // .end method
private void handleReSetThreadsFIFO ( Integer p0, Long p1, Integer p2 ) {
/* .locals 2 */
/* .param p1, "tid" # I */
/* .param p2, "duration" # J */
/* .param p4, "mode" # I */
/* .line 573 */
v0 = this.mHandler;
java.lang.Integer .valueOf ( p1 );
(( com.miui.server.rtboost.SchedBoostService$Handler ) v0 ).removeReSetThreadsFIFOMsg ( v1 ); // invoke-virtual {v0, v1}, Lcom/miui/server/rtboost/SchedBoostService$Handler;->removeReSetThreadsFIFOMsg(Ljava/lang/Integer;)V
/* .line 575 */
/* sget-boolean v0, Lcom/miui/server/rtboost/SchedBoostService;->DEBUG:Z */
if ( v0 != null) { // if-eqz v0, :cond_0
/* .line 576 */
java.lang.Integer .valueOf ( p1 );
/* filled-new-array {v0}, [Ljava/lang/Object; */
final String v1 = "handleReSetThreadsFIFO, threads: %s"; // const-string v1, "handleReSetThreadsFIFO, threads: %s"
java.lang.String .format ( v1,v0 );
final String v1 = "SchedBoost"; // const-string v1, "SchedBoost"
android.util.Slog .d ( v1,v0 );
/* .line 578 */
} // :cond_0
v0 = this.mRtTids;
java.lang.Integer .valueOf ( p1 );
v0 = (( android.util.ArraySet ) v0 ).contains ( v1 ); // invoke-virtual {v0, v1}, Landroid/util/ArraySet;->contains(Ljava/lang/Object;)Z
if ( v0 != null) { // if-eqz v0, :cond_1
/* .line 579 */
int v0 = 1; // const/4 v0, 0x1
com.android.server.am.ActivityManagerService .scheduleAsRegularPriority ( p1,v0 );
/* .line 580 */
v0 = this.mRtTids;
java.lang.Integer .valueOf ( p1 );
(( android.util.ArraySet ) v0 ).remove ( v1 ); // invoke-virtual {v0, v1}, Landroid/util/ArraySet;->remove(Ljava/lang/Object;)Z
/* .line 582 */
} // :cond_1
return;
} // .end method
private void handleResetSchedThread ( com.miui.server.rtboost.SchedBoostService$BoostThreadInfo p0 ) {
/* .locals 11 */
/* .param p1, "threadInfo" # Lcom/miui/server/rtboost/SchedBoostService$BoostThreadInfo; */
/* .line 496 */
int v0 = 0; // const/4 v0, 0x0
final String v1 = "SchedBoost"; // const-string v1, "SchedBoost"
/* if-nez p1, :cond_0 */
/* .line 497 */
final String v2 = "handleResetSchedThread err: threadInfo null object"; // const-string v2, "handleResetSchedThread err: threadInfo null object"
/* new-array v0, v0, [Ljava/lang/Object; */
java.lang.String .format ( v2,v0 );
android.util.Slog .e ( v1,v0 );
/* .line 498 */
return;
/* .line 501 */
} // :cond_0
/* invoke-direct {p0}, Lcom/miui/server/rtboost/SchedBoostService;->stopBoostInternal()V */
/* .line 504 */
/* iget-wide v2, p1, Lcom/miui/server/rtboost/SchedBoostService$BoostThreadInfo;->boostStartTime:J */
/* .line 505 */
/* .local v2, "boostStartTime":J */
/* const-wide/16 v4, 0x0 */
/* cmp-long v4, v2, v4 */
if ( v4 != null) { // if-eqz v4, :cond_a
/* .line 506 */
android.os.SystemClock .uptimeMillis ( );
/* move-result-wide v4 */
/* sub-long/2addr v4, v2 */
/* .line 511 */
/* .local v4, "boostDuration":J */
v6 = this.procName;
if ( v6 != null) { // if-eqz v6, :cond_1
v6 = this.procName;
v7 = this.mPreBoostProcessName;
v6 = (( java.lang.String ) v6 ).equals ( v7 ); // invoke-virtual {v6, v7}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z
if ( v6 != null) { // if-eqz v6, :cond_1
/* .line 512 */
v6 = this.mBoostingMap;
/* iget v7, p1, Lcom/miui/server/rtboost/SchedBoostService$BoostThreadInfo;->tid:I */
(( com.miui.server.rtboost.SchedBoostService$BoostingPackageMap ) v6 ).removeByTid ( v7 ); // invoke-virtual {v6, v7}, Lcom/miui/server/rtboost/SchedBoostService$BoostingPackageMap;->removeByTid(I)V
/* .line 513 */
v6 = this.mHandler;
(( com.miui.server.rtboost.SchedBoostService$Handler ) v6 ).removeResetThreadsMsg ( p1 ); // invoke-virtual {v6, p1}, Lcom/miui/server/rtboost/SchedBoostService$Handler;->removeResetThreadsMsg(Lcom/miui/server/rtboost/SchedBoostService$BoostThreadInfo;)V
/* .line 516 */
} // :cond_1
/* iget v6, p1, Lcom/miui/server/rtboost/SchedBoostService$BoostThreadInfo;->tid:I */
/* .line 517 */
/* .local v6, "tid":I */
v7 = com.miui.server.rtboost.SchedBoostService.ALL_CPU_CORES;
/* invoke-direct {p0, p1, v0, v0, v7}, Lcom/miui/server/rtboost/SchedBoostService;->setAffinityAndPriority(Lcom/miui/server/rtboost/SchedBoostService$BoostThreadInfo;ZZ[I)V */
/* .line 518 */
/* const/16 v7, 0x400 */
/* invoke-direct {p0, v6, v0, v7}, Lcom/miui/server/rtboost/SchedBoostService;->setTaskUclamp(III)V */
/* .line 520 */
/* sget-boolean v0, Lcom/miui/server/rtboost/SchedBoostService;->IS_UIGROUP_ENABLED:Z */
int v7 = 1; // const/4 v7, 0x1
if ( v0 != null) { // if-eqz v0, :cond_8
/* .line 522 */
try { // :try_start_0
v0 = android.os.Process .getCpusetThreadGroup ( v6 );
/* const/16 v8, 0xa */
/* if-ne v0, v8, :cond_7 */
/* .line 523 */
/* sget-boolean v0, Lcom/miui/server/rtboost/SchedBoostService;->DEBUG:Z */
/* :try_end_0 */
/* .catch Ljava/lang/Exception; {:try_start_0 ..:try_end_0} :catch_0 */
final String v8 = "reset tid "; // const-string v8, "reset tid "
if ( v0 != null) { // if-eqz v0, :cond_2
try { // :try_start_1
/* new-instance v9, Ljava/lang/StringBuilder; */
/* invoke-direct {v9}, Ljava/lang/StringBuilder;-><init>()V */
(( java.lang.StringBuilder ) v9 ).append ( v8 ); // invoke-virtual {v9, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v9 ).append ( v6 ); // invoke-virtual {v9, v6}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;
final String v10 = " group, procName="; // const-string v10, " group, procName="
(( java.lang.StringBuilder ) v9 ).append ( v10 ); // invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
v10 = this.procName;
(( java.lang.StringBuilder ) v9 ).append ( v10 ); // invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v9 ).toString ( ); // invoke-virtual {v9}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
android.util.Slog .d ( v1,v9 );
/* .line 525 */
} // :cond_2
final String v9 = "com.miui.home"; // const-string v9, "com.miui.home"
v10 = this.procName;
v9 = (( java.lang.String ) v9 ).equals ( v10 ); // invoke-virtual {v9, v10}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z
/* if-nez v9, :cond_5 */
final String v9 = "com.mi.android.globallauncher"; // const-string v9, "com.mi.android.globallauncher"
v10 = this.procName;
/* .line 526 */
v9 = (( java.lang.String ) v9 ).equals ( v10 ); // invoke-virtual {v9, v10}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z
if ( v9 != null) { // if-eqz v9, :cond_3
/* .line 530 */
} // :cond_3
if ( v0 != null) { // if-eqz v0, :cond_4
/* new-instance v0, Ljava/lang/StringBuilder; */
/* invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V */
(( java.lang.StringBuilder ) v0 ).append ( v8 ); // invoke-virtual {v0, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v0 ).append ( v6 ); // invoke-virtual {v0, v6}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;
final String v8 = " group to top-app"; // const-string v8, " group to top-app"
(( java.lang.StringBuilder ) v0 ).append ( v8 ); // invoke-virtual {v0, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v0 ).toString ( ); // invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
android.util.Slog .d ( v1,v0 );
/* .line 531 */
} // :cond_4
int v0 = 5; // const/4 v0, 0x5
/* invoke-direct {p0, v6, v0}, Lcom/miui/server/rtboost/SchedBoostService;->setTaskGroup(II)V */
/* .line 527 */
} // :cond_5
} // :goto_0
if ( v0 != null) { // if-eqz v0, :cond_6
/* new-instance v0, Ljava/lang/StringBuilder; */
/* invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V */
(( java.lang.StringBuilder ) v0 ).append ( v8 ); // invoke-virtual {v0, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v0 ).append ( v6 ); // invoke-virtual {v0, v6}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;
final String v8 = " group to foreground"; // const-string v8, " group to foreground"
(( java.lang.StringBuilder ) v0 ).append ( v8 ); // invoke-virtual {v0, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v0 ).toString ( ); // invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
android.util.Slog .d ( v1,v0 );
/* .line 528 */
} // :cond_6
/* invoke-direct {p0, v6, v7}, Lcom/miui/server/rtboost/SchedBoostService;->setTaskGroup(II)V */
/* :try_end_1 */
/* .catch Ljava/lang/Exception; {:try_start_1 ..:try_end_1} :catch_0 */
/* .line 536 */
} // :cond_7
} // :goto_1
/* .line 534 */
/* :catch_0 */
/* move-exception v0 */
/* .line 535 */
/* .local v0, "e":Ljava/lang/Exception; */
/* new-instance v8, Ljava/lang/StringBuilder; */
/* invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V */
final String v9 = "fail find and move tid "; // const-string v9, "fail find and move tid "
(( java.lang.StringBuilder ) v8 ).append ( v9 ); // invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v8 ).append ( v6 ); // invoke-virtual {v8, v6}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;
/* const-string/jumbo v9, "to foreground or top-app" */
(( java.lang.StringBuilder ) v8 ).append ( v9 ); // invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v8 ).append ( v0 ); // invoke-virtual {v8, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v8 ).toString ( ); // invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
android.util.Slog .e ( v1,v8 );
/* .line 539 */
} // .end local v0 # "e":Ljava/lang/Exception;
} // :cond_8
} // :goto_2
v0 = this.mHandlerThread;
v0 = (( android.os.HandlerThread ) v0 ).getThreadId ( ); // invoke-virtual {v0}, Landroid/os/HandlerThread;->getThreadId()I
com.android.server.am.ActivityManagerService .scheduleAsFifoPriority ( v0,v7 );
/* .line 540 */
/* sget-boolean v0, Lcom/miui/server/rtboost/SchedBoostService;->DEBUG:Z */
if ( v0 != null) { // if-eqz v0, :cond_9
/* .line 541 */
/* nop */
/* .line 542 */
java.lang.Integer .valueOf ( v6 );
java.lang.Long .valueOf ( v4,v5 );
v8 = this.procName;
/* iget v9, p1, Lcom/miui/server/rtboost/SchedBoostService$BoostThreadInfo;->mode:I */
/* .line 543 */
java.lang.Integer .valueOf ( v9 );
v10 = com.miui.server.rtboost.SchedBoostService.ALL_CPU_CORES;
java.util.Arrays .toString ( v10 );
/* filled-new-array {v0, v7, v8, v9, v10}, [Ljava/lang/Object; */
/* .line 541 */
final String v7 = "handleSchedThreads end: threads: %s, %sms, procName: %s, mode: %s aff:%s"; // const-string v7, "handleSchedThreads end: threads: %s, %sms, procName: %s, mode: %s aff:%s"
java.lang.String .format ( v7,v0 );
android.util.Slog .d ( v1,v0 );
/* .line 545 */
} // :cond_9
/* new-instance v0, Ljava/lang/StringBuilder; */
/* invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V */
final String v1 = "SchedBoost: "; // const-string v1, "SchedBoost: "
(( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
v1 = this.procName;
(( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v0 ).toString ( ); // invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
/* iget v1, p1, Lcom/miui/server/rtboost/SchedBoostService$BoostThreadInfo;->tid:I */
/* const-wide/16 v7, 0x40 */
android.os.Trace .asyncTraceEnd ( v7,v8,v0,v1 );
/* .line 547 */
return;
/* .line 508 */
} // .end local v4 # "boostDuration":J
} // .end local v6 # "tid":I
} // :cond_a
return;
} // .end method
private void handleSchedThread ( com.miui.server.rtboost.SchedBoostService$BoostThreadInfo p0 ) {
/* .locals 11 */
/* .param p1, "threadInfo" # Lcom/miui/server/rtboost/SchedBoostService$BoostThreadInfo; */
/* .line 439 */
final String v0 = "SchedBoost"; // const-string v0, "SchedBoost"
/* if-nez p1, :cond_0 */
/* .line 440 */
int v1 = 0; // const/4 v1, 0x0
/* new-array v1, v1, [Ljava/lang/Object; */
final String v2 = "handleSchedThreads err: threadInfo null object"; // const-string v2, "handleSchedThreads err: threadInfo null object"
java.lang.String .format ( v2,v1 );
android.util.Slog .e ( v0,v1 );
/* .line 441 */
return;
/* .line 443 */
} // :cond_0
v1 = this.mHandler;
(( com.miui.server.rtboost.SchedBoostService$Handler ) v1 ).removeResetThreadsMsg ( p1 ); // invoke-virtual {v1, p1}, Lcom/miui/server/rtboost/SchedBoostService$Handler;->removeResetThreadsMsg(Lcom/miui/server/rtboost/SchedBoostService$BoostThreadInfo;)V
/* .line 444 */
v1 = this.mHandler;
(( com.miui.server.rtboost.SchedBoostService$Handler ) v1 ).obtainResetSchedThreadsMsg ( p1 ); // invoke-virtual {v1, p1}, Lcom/miui/server/rtboost/SchedBoostService$Handler;->obtainResetSchedThreadsMsg(Lcom/miui/server/rtboost/SchedBoostService$BoostThreadInfo;)Landroid/os/Message;
/* .line 445 */
/* .local v1, "resetMsg":Landroid/os/Message; */
v2 = this.mHandler;
/* iget-wide v3, p1, Lcom/miui/server/rtboost/SchedBoostService$BoostThreadInfo;->duration:J */
(( com.miui.server.rtboost.SchedBoostService$Handler ) v2 ).sendMessageDelayed ( v1, v3, v4 ); // invoke-virtual {v2, v1, v3, v4}, Lcom/miui/server/rtboost/SchedBoostService$Handler;->sendMessageDelayed(Landroid/os/Message;J)Z
/* .line 448 */
/* iget-wide v2, p1, Lcom/miui/server/rtboost/SchedBoostService$BoostThreadInfo;->duration:J */
/* const-wide/16 v4, 0x0 */
/* cmp-long v2, v2, v4 */
/* if-nez v2, :cond_1 */
/* .line 449 */
return;
/* .line 452 */
} // :cond_1
v2 = com.miui.server.rtboost.SchedBoostService.PLATFORM_NAME;
final String v3 = "pineapple"; // const-string v3, "pineapple"
v2 = android.text.TextUtils .equals ( v2,v3 );
if ( v2 != null) { // if-eqz v2, :cond_2
/* .line 453 */
/* invoke-direct {p0, p1}, Lcom/miui/server/rtboost/SchedBoostService;->startBoostInternalWithOpcode(Lcom/miui/server/rtboost/SchedBoostService$BoostThreadInfo;)Z */
/* .line 455 */
} // :cond_2
/* invoke-direct {p0, p1}, Lcom/miui/server/rtboost/SchedBoostService;->startBoostInternal(Lcom/miui/server/rtboost/SchedBoostService$BoostThreadInfo;)Z */
/* .line 458 */
} // :goto_0
/* iget v2, p1, Lcom/miui/server/rtboost/SchedBoostService$BoostThreadInfo;->tid:I */
/* .line 459 */
/* .local v2, "tid":I */
/* iget-wide v6, p1, Lcom/miui/server/rtboost/SchedBoostService$BoostThreadInfo;->boostStartTime:J */
/* .line 461 */
/* .local v6, "boostStartTime":J */
/* cmp-long v3, v6, v4 */
if ( v3 != null) { // if-eqz v3, :cond_4
/* .line 462 */
/* sget-boolean v3, Lcom/miui/server/rtboost/SchedBoostService;->DEBUG:Z */
if ( v3 != null) { // if-eqz v3, :cond_3
/* .line 463 */
java.lang.Integer .valueOf ( v2 );
/* filled-new-array {v3}, [Ljava/lang/Object; */
final String v4 = "handleSchedThreads continue: threads: %s"; // const-string v4, "handleSchedThreads continue: threads: %s"
java.lang.String .format ( v4,v3 );
android.util.Slog .d ( v0,v3 );
/* .line 465 */
} // :cond_3
return;
/* .line 467 */
} // :cond_4
android.os.SystemClock .uptimeMillis ( );
/* move-result-wide v3 */
/* iput-wide v3, p1, Lcom/miui/server/rtboost/SchedBoostService$BoostThreadInfo;->boostStartTime:J */
/* .line 468 */
/* iget v3, p1, Lcom/miui/server/rtboost/SchedBoostService$BoostThreadInfo;->tid:I */
v3 = android.os.MiuiProcess .getThreadPriority ( v3,v0 );
/* iput v3, p1, Lcom/miui/server/rtboost/SchedBoostService$BoostThreadInfo;->savedPriority:I */
/* .line 470 */
int v3 = 0; // const/4 v3, 0x0
/* .line 471 */
/* .local v3, "forkPriority":Z */
v4 = android.os.MiuiProcess.BIG_CORES_INDEX;
/* .line 472 */
/* .local v4, "coresIndex":[I */
/* iget v5, p1, Lcom/miui/server/rtboost/SchedBoostService$BoostThreadInfo;->tid:I */
/* iget v8, p0, Lcom/miui/server/rtboost/SchedBoostService;->mHomeRenderThreadId:I */
/* if-eq v5, v8, :cond_5 */
/* iget v5, p1, Lcom/miui/server/rtboost/SchedBoostService$BoostThreadInfo;->tid:I */
/* iget v8, p0, Lcom/miui/server/rtboost/SchedBoostService;->mSystemUIRenderThreadId:I */
/* if-ne v5, v8, :cond_6 */
/* .line 474 */
} // :cond_5
int v3 = 1; // const/4 v3, 0x1
/* .line 476 */
} // :cond_6
v5 = android.os.MiuiProcess.BIG_PRIME_CORES_INDEX;
if ( v5 != null) { // if-eqz v5, :cond_7
/* iget v5, p1, Lcom/miui/server/rtboost/SchedBoostService$BoostThreadInfo;->mode:I */
/* if-nez v5, :cond_7 */
/* .line 478 */
v4 = android.os.MiuiProcess.BIG_PRIME_CORES_INDEX;
/* .line 480 */
} // :cond_7
int v5 = 1; // const/4 v5, 0x1
/* invoke-direct {p0, p1, v5, v3, v4}, Lcom/miui/server/rtboost/SchedBoostService;->setAffinityAndPriority(Lcom/miui/server/rtboost/SchedBoostService$BoostThreadInfo;ZZ[I)V */
/* .line 481 */
/* iget v5, p0, Lcom/miui/server/rtboost/SchedBoostService;->TASK_UCLAMP_MIN:I */
/* iget v8, p0, Lcom/miui/server/rtboost/SchedBoostService;->TASK_UCLAMP_MAX:I */
/* invoke-direct {p0, v2, v5, v8}, Lcom/miui/server/rtboost/SchedBoostService;->setTaskUclamp(III)V */
/* .line 482 */
/* const/16 v5, 0xa */
/* invoke-direct {p0, v2, v5}, Lcom/miui/server/rtboost/SchedBoostService;->setTaskGroup(II)V */
/* .line 484 */
/* iget v5, p1, Lcom/miui/server/rtboost/SchedBoostService$BoostThreadInfo;->tid:I */
v5 = android.os.MiuiProcess .getThreadPriority ( v5,v0 );
/* iput v5, p1, Lcom/miui/server/rtboost/SchedBoostService$BoostThreadInfo;->boostPriority:I */
/* .line 486 */
/* sget-boolean v5, Lcom/miui/server/rtboost/SchedBoostService;->DEBUG:Z */
if ( v5 != null) { // if-eqz v5, :cond_8
/* .line 487 */
/* nop */
/* .line 488 */
java.lang.Integer .valueOf ( v2 );
v8 = this.procName;
/* iget v9, p1, Lcom/miui/server/rtboost/SchedBoostService$BoostThreadInfo;->mode:I */
java.lang.Integer .valueOf ( v9 );
/* .line 489 */
java.util.Arrays .toString ( v4 );
/* filled-new-array {v5, v8, v9, v10}, [Ljava/lang/Object; */
/* .line 487 */
final String v8 = "handleSchedThreads begin: threads: %s, procName: %s, mode: %s, aff:%s"; // const-string v8, "handleSchedThreads begin: threads: %s, procName: %s, mode: %s, aff:%s"
java.lang.String .format ( v8,v5 );
android.util.Slog .d ( v0,v5 );
/* .line 491 */
} // :cond_8
/* new-instance v0, Ljava/lang/StringBuilder; */
/* invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V */
final String v5 = "SchedBoost: "; // const-string v5, "SchedBoost: "
(( java.lang.StringBuilder ) v0 ).append ( v5 ); // invoke-virtual {v0, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
v5 = this.procName;
(( java.lang.StringBuilder ) v0 ).append ( v5 ); // invoke-virtual {v0, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v0 ).toString ( ); // invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
/* iget v5, p1, Lcom/miui/server/rtboost/SchedBoostService$BoostThreadInfo;->tid:I */
/* const-wide/16 v8, 0x40 */
android.os.Trace .asyncTraceBegin ( v8,v9,v0,v5 );
/* .line 493 */
return;
} // .end method
private void handleSetThreadsAlwaysFIFO ( Integer[] p0 ) {
/* .locals 5 */
/* .param p1, "tids" # [I */
/* .line 550 */
/* array-length v0, p1 */
int v1 = 0; // const/4 v1, 0x0
} // :goto_0
/* if-ge v1, v0, :cond_2 */
/* aget v2, p1, v1 */
/* .line 551 */
/* .local v2, "tid":I */
/* if-lez v2, :cond_1 */
/* .line 552 */
/* sget-boolean v3, Lcom/miui/server/rtboost/SchedBoostService;->DEBUG:Z */
if ( v3 != null) { // if-eqz v3, :cond_0
/* .line 553 */
java.lang.Integer .valueOf ( v2 );
/* filled-new-array {v3}, [Ljava/lang/Object; */
final String v4 = "handleSetThreadsAlwaysFIFO, threads: %s"; // const-string v4, "handleSetThreadsAlwaysFIFO, threads: %s"
java.lang.String .format ( v4,v3 );
final String v4 = "SchedBoost"; // const-string v4, "SchedBoost"
android.util.Slog .d ( v4,v3 );
/* .line 555 */
} // :cond_0
int v3 = 1; // const/4 v3, 0x1
com.android.server.am.ActivityManagerService .scheduleAsFifoPriority ( v2,v3 );
/* .line 556 */
v3 = this.mAlwaysRtTids;
java.lang.Integer .valueOf ( v2 );
(( android.util.ArraySet ) v3 ).add ( v4 ); // invoke-virtual {v3, v4}, Landroid/util/ArraySet;->add(Ljava/lang/Object;)Z
/* .line 550 */
} // .end local v2 # "tid":I
} // :cond_1
/* add-int/lit8 v1, v1, 0x1 */
/* .line 559 */
} // :cond_2
return;
} // .end method
private void handleSetThreadsFIFO ( Integer p0, Long p1, Integer p2 ) {
/* .locals 3 */
/* .param p1, "tid" # I */
/* .param p2, "duration" # J */
/* .param p4, "mode" # I */
/* .line 562 */
v0 = this.mHandler;
java.lang.Integer .valueOf ( p1 );
(( com.miui.server.rtboost.SchedBoostService$Handler ) v0 ).removeReSetThreadsFIFOMsg ( v1 ); // invoke-virtual {v0, v1}, Lcom/miui/server/rtboost/SchedBoostService$Handler;->removeReSetThreadsFIFOMsg(Ljava/lang/Integer;)V
/* .line 563 */
v0 = this.mHandler;
(( com.miui.server.rtboost.SchedBoostService$Handler ) v0 ).obtainReSetThreadsFIFOMsg ( p1, p2, p3, p4 ); // invoke-virtual {v0, p1, p2, p3, p4}, Lcom/miui/server/rtboost/SchedBoostService$Handler;->obtainReSetThreadsFIFOMsg(IJI)Landroid/os/Message;
/* .line 564 */
/* .local v0, "resetMsg":Landroid/os/Message; */
v1 = this.mHandler;
(( com.miui.server.rtboost.SchedBoostService$Handler ) v1 ).sendMessageDelayed ( v0, p2, p3 ); // invoke-virtual {v1, v0, p2, p3}, Lcom/miui/server/rtboost/SchedBoostService$Handler;->sendMessageDelayed(Landroid/os/Message;J)Z
/* .line 565 */
/* sget-boolean v1, Lcom/miui/server/rtboost/SchedBoostService;->DEBUG:Z */
if ( v1 != null) { // if-eqz v1, :cond_0
/* .line 566 */
java.lang.Integer .valueOf ( p1 );
/* filled-new-array {v1}, [Ljava/lang/Object; */
final String v2 = "handleSetThreadsFIFO, threads: %s"; // const-string v2, "handleSetThreadsFIFO, threads: %s"
java.lang.String .format ( v2,v1 );
final String v2 = "SchedBoost"; // const-string v2, "SchedBoost"
android.util.Slog .d ( v2,v1 );
/* .line 568 */
} // :cond_0
int v1 = 3; // const/4 v1, 0x3
int v2 = 1; // const/4 v2, 0x1
android.os.MiuiProcess .scheduleAsFifoPriority ( p1,v1,v2 );
/* .line 569 */
v1 = this.mRtTids;
java.lang.Integer .valueOf ( p1 );
(( android.util.ArraySet ) v1 ).add ( v2 ); // invoke-virtual {v1, v2}, Landroid/util/ArraySet;->add(Ljava/lang/Object;)Z
/* .line 570 */
return;
} // .end method
private void handleThreadsBindBigCoreMsg ( java.lang.Integer p0, Long p1 ) {
/* .locals 3 */
/* .param p1, "tid" # Ljava/lang/Integer; */
/* .param p2, "duration" # J */
/* .line 585 */
v0 = this.mHandler;
(( com.miui.server.rtboost.SchedBoostService$Handler ) v0 ).removeUnBindBigCoreMsg ( p1 ); // invoke-virtual {v0, p1}, Lcom/miui/server/rtboost/SchedBoostService$Handler;->removeUnBindBigCoreMsg(Ljava/lang/Integer;)V
/* .line 586 */
v0 = this.mHandler;
(( com.miui.server.rtboost.SchedBoostService$Handler ) v0 ).obtainThreadUnBindBigCoreMsg ( p1 ); // invoke-virtual {v0, p1}, Lcom/miui/server/rtboost/SchedBoostService$Handler;->obtainThreadUnBindBigCoreMsg(Ljava/lang/Integer;)Landroid/os/Message;
/* .line 587 */
/* .local v0, "resetMsg":Landroid/os/Message; */
v1 = this.mHandler;
(( com.miui.server.rtboost.SchedBoostService$Handler ) v1 ).sendMessageDelayed ( v0, p2, p3 ); // invoke-virtual {v1, v0, p2, p3}, Lcom/miui/server/rtboost/SchedBoostService$Handler;->sendMessageDelayed(Landroid/os/Message;J)Z
/* .line 589 */
v1 = (( java.lang.Integer ) p1 ).intValue ( ); // invoke-virtual {p1}, Ljava/lang/Integer;->intValue()I
v2 = android.os.MiuiProcess.BIG_CORES_INDEX;
/* invoke-direct {p0, v1, v2}, Lcom/miui/server/rtboost/SchedBoostService;->setSchedAffinity(I[I)V */
/* .line 591 */
/* sget-boolean v1, Lcom/miui/server/rtboost/SchedBoostService;->DEBUG:Z */
if ( v1 != null) { // if-eqz v1, :cond_0
/* .line 592 */
v1 = (( java.lang.Integer ) p1 ).intValue ( ); // invoke-virtual {p1}, Ljava/lang/Integer;->intValue()I
java.lang.Integer .valueOf ( v1 );
v2 = android.os.MiuiProcess.BIG_CORES_INDEX;
/* .line 593 */
java.util.Arrays .toString ( v2 );
/* filled-new-array {v1, v2}, [Ljava/lang/Object; */
/* .line 592 */
final String v2 = "BindBigCore, threads: %s, aff:%s"; // const-string v2, "BindBigCore, threads: %s, aff:%s"
java.lang.String .format ( v2,v1 );
final String v2 = "SchedBoost"; // const-string v2, "SchedBoost"
android.util.Slog .d ( v2,v1 );
/* .line 595 */
} // :cond_0
return;
} // .end method
private void handleThreadsUnBindBigCoreMsg ( java.lang.Integer p0 ) {
/* .locals 2 */
/* .param p1, "tid" # Ljava/lang/Integer; */
/* .line 598 */
/* if-nez p1, :cond_0 */
/* .line 599 */
return;
/* .line 600 */
} // :cond_0
v0 = this.mHandler;
(( com.miui.server.rtboost.SchedBoostService$Handler ) v0 ).removeUnBindBigCoreMsg ( p1 ); // invoke-virtual {v0, p1}, Lcom/miui/server/rtboost/SchedBoostService$Handler;->removeUnBindBigCoreMsg(Ljava/lang/Integer;)V
/* .line 601 */
v0 = (( java.lang.Integer ) p1 ).intValue ( ); // invoke-virtual {p1}, Ljava/lang/Integer;->intValue()I
v1 = com.miui.server.rtboost.SchedBoostService.ALL_CPU_CORES;
/* invoke-direct {p0, v0, v1}, Lcom/miui/server/rtboost/SchedBoostService;->setSchedAffinity(I[I)V */
/* .line 603 */
/* sget-boolean v0, Lcom/miui/server/rtboost/SchedBoostService;->DEBUG:Z */
if ( v0 != null) { // if-eqz v0, :cond_1
/* .line 604 */
v0 = (( java.lang.Integer ) p1 ).intValue ( ); // invoke-virtual {p1}, Ljava/lang/Integer;->intValue()I
java.lang.Integer .valueOf ( v0 );
v1 = com.miui.server.rtboost.SchedBoostService.ALL_CPU_CORES;
/* .line 605 */
java.util.Arrays .toString ( v1 );
/* filled-new-array {v0, v1}, [Ljava/lang/Object; */
/* .line 604 */
/* const-string/jumbo v1, "unBindBigCore, threads: %s, aff:%s" */
java.lang.String .format ( v1,v0 );
final String v1 = "SchedBoost"; // const-string v1, "SchedBoost"
android.util.Slog .d ( v1,v0 );
/* .line 607 */
} // :cond_1
return;
} // .end method
private Boolean init ( ) {
/* .locals 5 */
/* .line 279 */
/* sget-boolean v0, Lcom/miui/server/rtboost/SchedBoostService;->IS_MTK_DEVICE:Z */
if ( v0 != null) { // if-eqz v0, :cond_0
/* .line 280 */
android.perf.PerfMTKStub .newInstance ( );
this.mMTKPerfStub = v0;
/* .line 281 */
android.perf.PerfMTKStub .newInstance ( );
this.mDefultPerfList = v0;
/* .line 282 */
v0 = com.miui.server.rtboost.SchedBoostService.MTK_GPU_AND_CPU_BOOST_LIST;
this.mGpuAndCpuPerfList = v0;
/* .line 284 */
} // :cond_0
android.perf.PerfStub .newInstance ( );
this.mQcomPerfStub = v0;
/* .line 285 */
v0 = com.miui.server.rtboost.SchedBoostService.DEFAULT_QCOM_PERF_LIST;
this.mDefultPerfList = v0;
/* .line 286 */
v0 = com.miui.server.rtboost.SchedBoostService.QCOM_GPU_AND_CPU_BOOST_LIST;
this.mGpuAndCpuPerfList = v0;
/* .line 288 */
} // :goto_0
/* const-class v0, Landroid/os/Process; */
/* const-string/jumbo v1, "setSchedAffinity" */
v2 = java.lang.Integer.TYPE;
/* const-class v3, [I */
/* filled-new-array {v2, v3}, [Ljava/lang/Class; */
miui.util.ReflectionUtils .tryFindMethodBestMatch ( v0,v1,v2 );
this.mMethodSetAffinity = v0;
/* .line 290 */
/* if-nez v0, :cond_1 */
/* .line 291 */
final String v0 = "SchedBoost"; // const-string v0, "SchedBoost"
final String v1 = "failed to find setSchedAffinity method"; // const-string v1, "failed to find setSchedAffinity method"
android.util.Slog .e ( v0,v1 );
/* .line 292 */
int v0 = 0; // const/4 v0, 0x0
/* .line 295 */
} // :cond_1
/* const-class v0, Landroid/os/Process; */
/* const-string/jumbo v1, "setTaskUclamp" */
v2 = java.lang.Integer.TYPE;
v3 = java.lang.Integer.TYPE;
v4 = java.lang.Integer.TYPE;
/* filled-new-array {v2, v3, v4}, [Ljava/lang/Class; */
miui.util.ReflectionUtils .tryFindMethodBestMatch ( v0,v1,v2 );
this.mMethodUclampTask = v0;
/* .line 297 */
/* const-class v0, Lcom/miui/server/rtboost/SchedBoostService; */
/* monitor-enter v0 */
/* .line 298 */
try { // :try_start_0
/* iget-boolean v1, p0, Lcom/miui/server/rtboost/SchedBoostService;->isInited:Z */
int v2 = 1; // const/4 v2, 0x1
/* if-nez v1, :cond_2 */
/* .line 299 */
/* iput-boolean v2, p0, Lcom/miui/server/rtboost/SchedBoostService;->isInited:Z */
/* .line 301 */
} // :cond_2
/* monitor-exit v0 */
/* .line 302 */
/* .line 301 */
/* :catchall_0 */
/* move-exception v1 */
/* monitor-exit v0 */
/* :try_end_0 */
/* .catchall {:try_start_0 ..:try_end_0} :catchall_0 */
/* throw v1 */
} // .end method
private void lambda$new$0 ( Integer p0 ) { //synthethic
/* .locals 0 */
/* .param p1, "temp" # I */
/* .line 269 */
/* invoke-direct {p0, p1}, Lcom/miui/server/rtboost/SchedBoostService;->changeSchedBoostPolicy(I)V */
return;
} // .end method
private java.lang.String readCmdlineFromProcfs ( java.lang.String p0 ) {
/* .locals 3 */
/* .param p1, "filePath" # Ljava/lang/String; */
/* .line 673 */
int v0 = 1; // const/4 v0, 0x1
/* new-array v0, v0, [Ljava/lang/String; */
/* .line 674 */
/* .local v0, "cmdline":[Ljava/lang/String; */
v1 = com.miui.server.rtboost.SchedBoostService.CMDLINE_OUT;
int v2 = 0; // const/4 v2, 0x0
v1 = android.os.Process .readProcFile ( p1,v1,v0,v2,v2 );
/* if-nez v1, :cond_0 */
/* .line 675 */
final String v1 = ""; // const-string v1, ""
/* .line 677 */
} // :cond_0
int v1 = 0; // const/4 v1, 0x0
/* aget-object v1, v0, v1 */
} // .end method
private void schedGpuThreads ( Integer p0, java.lang.String p1 ) {
/* .locals 16 */
/* .param p1, "pid" # I */
/* .param p2, "procName" # Ljava/lang/String; */
/* .line 642 */
/* move-object/from16 v6, p0 */
/* move/from16 v7, p1 */
/* new-instance v0, Ljava/util/ArrayList; */
/* invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V */
/* move-object v8, v0 */
/* .line 643 */
/* .local v8, "targetTids":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/Integer;>;" */
/* new-instance v0, Ljava/lang/StringBuilder; */
/* invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V */
final String v1 = "/proc/"; // const-string v1, "/proc/"
(( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v0 ).append ( v7 ); // invoke-virtual {v0, v7}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;
final String v2 = "/task"; // const-string v2, "/task"
(( java.lang.StringBuilder ) v0 ).append ( v2 ); // invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v0 ).toString ( ); // invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
/* .line 645 */
/* .local v0, "filePath":Ljava/lang/String; */
/* const/16 v2, 0x400 */
/* new-array v2, v2, [I */
/* .line 646 */
/* .local v2, "tids":[I */
android.os.Process .getPids ( v0,v2 );
/* .line 648 */
} // .end local v2 # "tids":[I
/* .local v9, "tids":[I */
if ( v9 != null) { // if-eqz v9, :cond_6
/* array-length v2, v9 */
/* if-nez v2, :cond_0 */
/* goto/16 :goto_4 */
/* .line 649 */
} // :cond_0
/* array-length v2, v9 */
/* move-object v11, v0 */
int v0 = 0; // const/4 v0, 0x0
} // .end local v0 # "filePath":Ljava/lang/String;
/* .local v11, "filePath":Ljava/lang/String; */
} // :goto_0
/* if-ge v0, v2, :cond_4 */
/* aget v3, v9, v0 */
/* .line 650 */
/* .local v3, "tmpTid":I */
/* if-gez v3, :cond_1 */
/* goto/16 :goto_2 */
/* .line 652 */
} // :cond_1
/* new-instance v4, Ljava/lang/StringBuilder; */
/* invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V */
(( java.lang.StringBuilder ) v4 ).append ( v1 ); // invoke-virtual {v4, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v4 ).append ( v7 ); // invoke-virtual {v4, v7}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;
final String v5 = "/task/"; // const-string v5, "/task/"
(( java.lang.StringBuilder ) v4 ).append ( v5 ); // invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v4 ).append ( v3 ); // invoke-virtual {v4, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;
final String v5 = "/comm"; // const-string v5, "/comm"
(( java.lang.StringBuilder ) v4 ).append ( v5 ); // invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v4 ).toString ( ); // invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
/* .line 653 */
} // .end local v11 # "filePath":Ljava/lang/String;
/* .local v4, "filePath":Ljava/lang/String; */
/* invoke-direct {v6, v4}, Lcom/miui/server/rtboost/SchedBoostService;->readCmdlineFromProcfs(Ljava/lang/String;)Ljava/lang/String; */
/* .line 654 */
/* .local v5, "taskName":Ljava/lang/String; */
if ( v5 != null) { // if-eqz v5, :cond_3
final String v11 = ""; // const-string v11, ""
v11 = (( java.lang.String ) v5 ).equals ( v11 ); // invoke-virtual {v5, v11}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z
/* if-nez v11, :cond_3 */
/* .line 655 */
v11 = com.miui.server.rtboost.SchedBoostService.RT_THREAD_COMM_LIST;
/* array-length v12, v11 */
int v13 = 0; // const/4 v13, 0x0
} // :goto_1
/* if-ge v13, v12, :cond_3 */
/* aget-object v14, v11, v13 */
/* .line 656 */
/* .local v14, "comm":Ljava/lang/String; */
(( java.lang.String ) v5 ).trim ( ); // invoke-virtual {v5}, Ljava/lang/String;->trim()Ljava/lang/String;
v15 = (( java.lang.String ) v15 ).equals ( v14 ); // invoke-virtual {v15, v14}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z
if ( v15 != null) { // if-eqz v15, :cond_2
/* .line 657 */
java.lang.Integer .valueOf ( v3 );
(( java.util.ArrayList ) v8 ).add ( v15 ); // invoke-virtual {v8, v15}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z
/* .line 658 */
/* sget-boolean v15, Lcom/miui/server/rtboost/SchedBoostService;->DEBUG:Z */
if ( v15 != null) { // if-eqz v15, :cond_2
/* new-instance v15, Ljava/lang/StringBuilder; */
/* invoke-direct {v15}, Ljava/lang/StringBuilder;-><init>()V */
final String v10 = "Sched GPU tid: "; // const-string v10, "Sched GPU tid: "
(( java.lang.StringBuilder ) v15 ).append ( v10 ); // invoke-virtual {v15, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v10 ).append ( v3 ); // invoke-virtual {v10, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;
final String v15 = " "; // const-string v15, " "
(( java.lang.StringBuilder ) v10 ).append ( v15 ); // invoke-virtual {v10, v15}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.String ) v14 ).trim ( ); // invoke-virtual {v14}, Ljava/lang/String;->trim()Ljava/lang/String;
(( java.lang.StringBuilder ) v10 ).append ( v15 ); // invoke-virtual {v10, v15}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v10 ).toString ( ); // invoke-virtual {v10}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
final String v15 = "SchedBoost"; // const-string v15, "SchedBoost"
android.util.Slog .d ( v15,v10 );
/* .line 655 */
} // .end local v14 # "comm":Ljava/lang/String;
} // :cond_2
/* add-int/lit8 v13, v13, 0x1 */
/* .line 649 */
} // .end local v3 # "tmpTid":I
} // .end local v5 # "taskName":Ljava/lang/String;
} // :cond_3
/* move-object v11, v4 */
} // .end local v4 # "filePath":Ljava/lang/String;
/* .restart local v11 # "filePath":Ljava/lang/String; */
} // :goto_2
/* add-int/lit8 v0, v0, 0x1 */
/* goto/16 :goto_0 */
/* .line 663 */
} // :cond_4
(( java.util.ArrayList ) v8 ).stream ( ); // invoke-virtual {v8}, Ljava/util/ArrayList;->stream()Ljava/util/stream/Stream;
/* new-instance v1, Lcom/android/server/audio/AudioServiceStubImpl$$ExternalSyntheticLambda1; */
/* invoke-direct {v1}, Lcom/android/server/audio/AudioServiceStubImpl$$ExternalSyntheticLambda1;-><init>()V */
/* const-wide/16 v2, 0x0 */
int v5 = 3; // const/4 v5, 0x3
/* move-object/from16 v0, p0 */
/* move-object/from16 v4, p2 */
/* invoke-virtual/range {v0 ..v5}, Lcom/miui/server/rtboost/SchedBoostService;->beginSchedThreads([IJLjava/lang/String;I)V */
/* .line 665 */
v0 = (( java.util.ArrayList ) v8 ).size ( ); // invoke-virtual {v8}, Ljava/util/ArrayList;->size()I
v1 = com.miui.server.rtboost.SchedBoostService.RT_THREAD_COMM_LIST;
/* array-length v1, v1 */
/* if-ne v0, v1, :cond_5 */
/* .line 666 */
int v0 = 0; // const/4 v0, 0x0
/* iput-boolean v0, v6, Lcom/miui/server/rtboost/SchedBoostService;->mPendingHomeGpuTids:Z */
/* .line 668 */
} // :cond_5
/* iget v0, v6, Lcom/miui/server/rtboost/SchedBoostService;->mBoostHomeGpuTidsTryCnt:I */
/* add-int/lit8 v0, v0, 0x1 */
/* iput v0, v6, Lcom/miui/server/rtboost/SchedBoostService;->mBoostHomeGpuTidsTryCnt:I */
/* .line 670 */
} // :goto_3
return;
/* .line 648 */
} // .end local v11 # "filePath":Ljava/lang/String;
/* .restart local v0 # "filePath":Ljava/lang/String; */
} // :cond_6
} // :goto_4
return;
} // .end method
private void setAffinityAndPriority ( com.miui.server.rtboost.SchedBoostService$BoostThreadInfo p0, Boolean p1, Boolean p2, Integer[] p3 ) {
/* .locals 7 */
/* .param p1, "threadInfo" # Lcom/miui/server/rtboost/SchedBoostService$BoostThreadInfo; */
/* .param p2, "isFifo" # Z */
/* .param p3, "forkPriority" # Z */
/* .param p4, "coresIndex" # [I */
/* .line 1040 */
/* iget v0, p1, Lcom/miui/server/rtboost/SchedBoostService$BoostThreadInfo;->tid:I */
/* .line 1041 */
/* .local v0, "tid":I */
int v1 = 0; // const/4 v1, 0x0
/* .line 1043 */
/* .local v1, "setFifo":Z */
int v2 = 1; // const/4 v2, 0x1
final String v3 = "SchedBoost"; // const-string v3, "SchedBoost"
if ( p2 != null) { // if-eqz p2, :cond_2
/* .line 1044 */
/* iget-boolean v4, p0, Lcom/miui/server/rtboost/SchedBoostService;->isNormalPolicy:Z */
if ( v4 != null) { // if-eqz v4, :cond_0
/* .line 1045 */
/* const/16 v2, -0x14 */
v1 = android.os.MiuiProcess .setThreadPriority ( v0,v2,v3 );
/* .line 1046 */
} // :cond_0
if ( p3 != null) { // if-eqz p3, :cond_1
/* .line 1047 */
v1 = android.os.MiuiProcess .scheduleAsFifoAndForkPriority ( v0,v2 );
/* .line 1049 */
} // :cond_1
v1 = com.android.server.am.ActivityManagerService .scheduleAsFifoPriority ( v0,v2 );
/* .line 1052 */
} // :cond_2
/* iget v4, p1, Lcom/miui/server/rtboost/SchedBoostService$BoostThreadInfo;->tid:I */
v4 = android.os.MiuiProcess .getThreadPriority ( v4,v3 );
/* .line 1059 */
/* .local v4, "curPrio":I */
/* iget-boolean v5, p0, Lcom/miui/server/rtboost/SchedBoostService;->isNormalPolicy:Z */
if ( v5 != null) { // if-eqz v5, :cond_3
/* iget v5, p1, Lcom/miui/server/rtboost/SchedBoostService$BoostThreadInfo;->boostPriority:I */
/* if-ne v4, v5, :cond_4 */
/* .line 1060 */
} // :cond_3
v1 = com.android.server.am.ActivityManagerService .scheduleAsRegularPriority ( v0,v2 );
/* .line 1061 */
/* iget v2, p1, Lcom/miui/server/rtboost/SchedBoostService$BoostThreadInfo;->savedPriority:I */
android.os.MiuiProcess .setThreadPriority ( v0,v2,v3 );
/* .line 1065 */
} // .end local v4 # "curPrio":I
} // :cond_4
} // :goto_0
if ( v1 != null) { // if-eqz v1, :cond_5
/* .line 1066 */
/* invoke-direct {p0, v0, p4}, Lcom/miui/server/rtboost/SchedBoostService;->setSchedAffinity(I[I)V */
/* .line 1068 */
} // :cond_5
/* sget-boolean v2, Lcom/miui/server/rtboost/SchedBoostService;->DEBUG:Z */
if ( v2 != null) { // if-eqz v2, :cond_6
/* .line 1069 */
/* nop */
/* .line 1070 */
java.lang.Integer .valueOf ( v0 );
java.lang.Boolean .valueOf ( p2 );
java.lang.Boolean .valueOf ( p3 );
/* .line 1071 */
java.util.Arrays .toString ( p4 );
/* filled-new-array {v2, v4, v5, v6}, [Ljava/lang/Object; */
/* .line 1069 */
/* const-string/jumbo v4, "setAffinityAndPriority, tid: %s, isFifo: %s, forkPriority: %s, coresIndex: %s" */
java.lang.String .format ( v4,v2 );
android.util.Slog .d ( v3,v2 );
/* .line 1073 */
} // :cond_6
return;
} // .end method
private void setSchedAffinity ( Integer p0, Integer[] p1 ) {
/* .locals 4 */
/* .param p1, "pid" # I */
/* .param p2, "cores" # [I */
/* .line 1076 */
v0 = this.mMethodSetAffinity;
if ( v0 != null) { // if-eqz v0, :cond_0
/* .line 1078 */
int v1 = 2; // const/4 v1, 0x2
try { // :try_start_0
/* new-array v1, v1, [Ljava/lang/Object; */
java.lang.Integer .valueOf ( p1 );
int v3 = 0; // const/4 v3, 0x0
/* aput-object v2, v1, v3 */
int v2 = 1; // const/4 v2, 0x1
/* aput-object p2, v1, v2 */
int v2 = 0; // const/4 v2, 0x0
(( java.lang.reflect.Method ) v0 ).invoke ( v2, v1 ); // invoke-virtual {v0, v2, v1}, Ljava/lang/reflect/Method;->invoke(Ljava/lang/Object;[Ljava/lang/Object;)Ljava/lang/Object;
/* :try_end_0 */
/* .catch Ljava/lang/Exception; {:try_start_0 ..:try_end_0} :catch_0 */
/* .line 1081 */
/* .line 1079 */
/* :catch_0 */
/* move-exception v0 */
/* .line 1080 */
/* .local v0, "e":Ljava/lang/Exception; */
(( java.lang.Exception ) v0 ).printStackTrace ( ); // invoke-virtual {v0}, Ljava/lang/Exception;->printStackTrace()V
/* .line 1083 */
} // .end local v0 # "e":Ljava/lang/Exception;
} // :cond_0
} // :goto_0
return;
} // .end method
private void setTaskGroup ( Integer p0, Integer p1 ) {
/* .locals 5 */
/* .param p1, "tid" # I */
/* .param p2, "group" # I */
/* .line 1096 */
/* sget-boolean v0, Lcom/miui/server/rtboost/SchedBoostService;->IS_UIGROUP_ENABLED:Z */
if ( v0 != null) { // if-eqz v0, :cond_0
/* .line 1097 */
/* new-instance v0, Ljava/lang/StringBuilder; */
/* invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V */
/* const-string/jumbo v1, "setTaskGroup: " */
(( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v0 ).append ( p1 ); // invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;
final String v1 = ", "; // const-string v1, ", "
(( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v0 ).append ( p2 ); // invoke-virtual {v0, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v0 ).toString ( ); // invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
final String v1 = "SchedBoost"; // const-string v1, "SchedBoost"
android.util.Slog .i ( v1,v0 );
/* .line 1099 */
try { // :try_start_0
android.os.Process .setThreadGroupAndCpuset ( p1,p2 );
/* .line 1100 */
/* const-string/jumbo v0, "setTaskGroup: Check cpuset of tid: %s,group= %s" */
int v2 = 2; // const/4 v2, 0x2
/* new-array v2, v2, [Ljava/lang/Object; */
java.lang.Integer .valueOf ( p1 );
int v4 = 0; // const/4 v4, 0x0
/* aput-object v3, v2, v4 */
v3 = android.os.Process .getCpusetThreadGroup ( p1 );
java.lang.Integer .valueOf ( v3 );
int v4 = 1; // const/4 v4, 0x1
/* aput-object v3, v2, v4 */
java.lang.String .format ( v0,v2 );
android.util.Slog .i ( v1,v0 );
/* :try_end_0 */
/* .catch Ljava/lang/Exception; {:try_start_0 ..:try_end_0} :catch_0 */
/* .line 1103 */
/* .line 1101 */
/* :catch_0 */
/* move-exception v0 */
/* .line 1102 */
/* .local v0, "e":Ljava/lang/Exception; */
(( java.lang.Exception ) v0 ).printStackTrace ( ); // invoke-virtual {v0}, Ljava/lang/Exception;->printStackTrace()V
/* .line 1105 */
} // .end local v0 # "e":Ljava/lang/Exception;
} // :cond_0
} // :goto_0
return;
} // .end method
private void setTaskUclamp ( Integer p0, Integer p1, Integer p2 ) {
/* .locals 4 */
/* .param p1, "tid" # I */
/* .param p2, "perfIdx" # I */
/* .param p3, "maxIdx" # I */
/* .line 1086 */
/* sget-boolean v0, Lcom/miui/server/rtboost/SchedBoostService;->IS_UCLAMP_ENABLED:Z */
if ( v0 != null) { // if-eqz v0, :cond_0
v0 = this.mMethodUclampTask;
if ( v0 != null) { // if-eqz v0, :cond_0
/* .line 1088 */
int v1 = 3; // const/4 v1, 0x3
try { // :try_start_0
/* new-array v1, v1, [Ljava/lang/Object; */
java.lang.Integer .valueOf ( p1 );
int v3 = 0; // const/4 v3, 0x0
/* aput-object v2, v1, v3 */
java.lang.Integer .valueOf ( p2 );
int v3 = 1; // const/4 v3, 0x1
/* aput-object v2, v1, v3 */
java.lang.Integer .valueOf ( p3 );
int v3 = 2; // const/4 v3, 0x2
/* aput-object v2, v1, v3 */
int v2 = 0; // const/4 v2, 0x0
(( java.lang.reflect.Method ) v0 ).invoke ( v2, v1 ); // invoke-virtual {v0, v2, v1}, Ljava/lang/reflect/Method;->invoke(Ljava/lang/Object;[Ljava/lang/Object;)Ljava/lang/Object;
/* :try_end_0 */
/* .catch Ljava/lang/Exception; {:try_start_0 ..:try_end_0} :catch_0 */
/* .line 1091 */
/* .line 1089 */
/* :catch_0 */
/* move-exception v0 */
/* .line 1090 */
/* .local v0, "e":Ljava/lang/Exception; */
(( java.lang.Exception ) v0 ).printStackTrace ( ); // invoke-virtual {v0}, Ljava/lang/Exception;->printStackTrace()V
/* .line 1093 */
} // .end local v0 # "e":Ljava/lang/Exception;
} // :cond_0
} // :goto_0
return;
} // .end method
private synchronized Boolean startBoostInternal ( com.miui.server.rtboost.SchedBoostService$BoostThreadInfo p0 ) {
/* .locals 7 */
/* .param p1, "threadInfo" # Lcom/miui/server/rtboost/SchedBoostService$BoostThreadInfo; */
/* monitor-enter p0 */
/* .line 1111 */
int v0 = 0; // const/4 v0, 0x0
try { // :try_start_0
/* iget v1, p1, Lcom/miui/server/rtboost/SchedBoostService$BoostThreadInfo;->mode:I */
int v2 = 1; // const/4 v2, 0x1
/* if-eq v1, v2, :cond_3 */
/* iget v1, p1, Lcom/miui/server/rtboost/SchedBoostService$BoostThreadInfo;->mode:I */
int v3 = 5; // const/4 v3, 0x5
/* if-eq v1, v3, :cond_3 */
/* iget v1, p1, Lcom/miui/server/rtboost/SchedBoostService$BoostThreadInfo;->mode:I */
int v3 = 4; // const/4 v3, 0x4
/* if-ne v1, v3, :cond_0 */
/* .line 1119 */
} // :cond_0
/* iget v1, p1, Lcom/miui/server/rtboost/SchedBoostService$BoostThreadInfo;->mode:I */
int v3 = 6; // const/4 v3, 0x6
/* if-ne v1, v3, :cond_1 */
/* .line 1120 */
v1 = this.mGpuAndCpuPerfList;
/* .local v1, "params":[I */
/* .line 1122 */
} // .end local v1 # "params":[I
} // .end local p0 # "this":Lcom/miui/server/rtboost/SchedBoostService;
} // :cond_1
/* sget-boolean v1, Lcom/miui/server/rtboost/SchedBoostService;->DEBUG:Z */
if ( v1 != null) { // if-eqz v1, :cond_2
final String v1 = "SchedBoost"; // const-string v1, "SchedBoost"
final String v2 = "current sched mode no need to boost"; // const-string v2, "current sched mode no need to boost"
android.util.Slog .d ( v1,v2 );
/* :try_end_0 */
/* .catch Ljava/lang/Exception; {:try_start_0 ..:try_end_0} :catch_0 */
/* .catchall {:try_start_0 ..:try_end_0} :catchall_0 */
/* .line 1123 */
} // :cond_2
/* monitor-exit p0 */
/* .line 1114 */
} // :cond_3
} // :goto_0
try { // :try_start_1
v1 = (( com.miui.server.rtboost.SchedBoostService ) p0 ).needBoostHigherFreq ( p1 ); // invoke-virtual {p0, p1}, Lcom/miui/server/rtboost/SchedBoostService;->needBoostHigherFreq(Lcom/miui/server/rtboost/SchedBoostService$BoostThreadInfo;)Z
if ( v1 != null) { // if-eqz v1, :cond_4
/* .line 1115 */
v1 = com.miui.server.rtboost.SchedBoostService.QCOM_GPU_AND_CPU_HIGHER_FREQ;
/* .restart local v1 # "params":[I */
/* .line 1117 */
} // .end local v1 # "params":[I
} // :cond_4
v1 = this.mDefultPerfList;
/* .line 1126 */
/* .restart local v1 # "params":[I */
} // :goto_1
/* iget v3, p0, Lcom/miui/server/rtboost/SchedBoostService;->mHandle:I */
if ( v3 != null) { // if-eqz v3, :cond_6
/* .line 1127 */
android.os.SystemClock .uptimeMillis ( );
/* move-result-wide v3 */
/* iget-wide v5, p0, Lcom/miui/server/rtboost/SchedBoostService;->mLastPerfAcqTime:J */
/* sub-long/2addr v3, v5 */
/* .line 1128 */
/* .local v3, "lastPerfAcqDuration":J */
/* const-wide/16 v5, 0x12c */
/* cmp-long v5, v3, v5 */
/* if-gez v5, :cond_6 */
/* .line 1129 */
/* sget-boolean v2, Lcom/miui/server/rtboost/SchedBoostService;->DEBUG:Z */
if ( v2 != null) { // if-eqz v2, :cond_5
final String v2 = "SchedBoost"; // const-string v2, "SchedBoost"
/* new-instance v5, Ljava/lang/StringBuilder; */
/* invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V */
final String v6 = "last perfacq still work, skip, "; // const-string v6, "last perfacq still work, skip, "
(( java.lang.StringBuilder ) v5 ).append ( v6 ); // invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v5 ).append ( v3, v4 ); // invoke-virtual {v5, v3, v4}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v5 ).toString ( ); // invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
android.util.Slog .d ( v2,v5 );
/* :try_end_1 */
/* .catch Ljava/lang/Exception; {:try_start_1 ..:try_end_1} :catch_0 */
/* .catchall {:try_start_1 ..:try_end_1} :catchall_0 */
/* .line 1131 */
} // :cond_5
/* monitor-exit p0 */
/* .line 1134 */
} // .end local v3 # "lastPerfAcqDuration":J
} // :cond_6
try { // :try_start_2
/* invoke-direct {p0}, Lcom/miui/server/rtboost/SchedBoostService;->stopBoostInternal()V */
/* .line 1135 */
/* sget-boolean v3, Lcom/miui/server/rtboost/SchedBoostService;->IS_MTK_DEVICE:Z */
if ( v3 != null) { // if-eqz v3, :cond_7
/* .line 1136 */
v3 = this.mMTKPerfStub;
/* iget-wide v4, p1, Lcom/miui/server/rtboost/SchedBoostService$BoostThreadInfo;->duration:J */
v3 = /* long-to-int v4, v4 */
/* iput v3, p0, Lcom/miui/server/rtboost/SchedBoostService;->mHandle:I */
/* .line 1138 */
} // :cond_7
v3 = this.mQcomPerfStub;
/* iget-wide v4, p1, Lcom/miui/server/rtboost/SchedBoostService$BoostThreadInfo;->duration:J */
/* long-to-int v4, v4 */
v3 = (( android.perf.PerfStub ) v3 ).perfLockAcquire ( v4, v1 ); // invoke-virtual {v3, v4, v1}, Landroid/perf/PerfStub;->perfLockAcquire(I[I)I
/* iput v3, p0, Lcom/miui/server/rtboost/SchedBoostService;->mHandle:I */
/* .line 1140 */
} // :goto_2
android.os.SystemClock .uptimeMillis ( );
/* move-result-wide v3 */
/* iput-wide v3, p0, Lcom/miui/server/rtboost/SchedBoostService;->mLastPerfAcqTime:J */
/* .line 1141 */
final String v3 = "SchedBoost"; // const-string v3, "SchedBoost"
/* new-instance v4, Ljava/lang/StringBuilder; */
/* invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V */
/* const-string/jumbo v5, "startBoostInternal " */
(( java.lang.StringBuilder ) v4 ).append ( v5 ); // invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
/* iget-wide v5, p1, Lcom/miui/server/rtboost/SchedBoostService$BoostThreadInfo;->duration:J */
(( java.lang.StringBuilder ) v4 ).append ( v5, v6 ); // invoke-virtual {v4, v5, v6}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v4 ).toString ( ); // invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
android.util.Slog .d ( v3,v4 );
/* :try_end_2 */
/* .catch Ljava/lang/Exception; {:try_start_2 ..:try_end_2} :catch_0 */
/* .catchall {:try_start_2 ..:try_end_2} :catchall_0 */
/* .line 1142 */
/* monitor-exit p0 */
/* .line 1110 */
} // .end local v1 # "params":[I
} // .end local p1 # "threadInfo":Lcom/miui/server/rtboost/SchedBoostService$BoostThreadInfo;
/* :catchall_0 */
/* move-exception p1 */
/* .line 1143 */
/* .restart local p1 # "threadInfo":Lcom/miui/server/rtboost/SchedBoostService$BoostThreadInfo; */
/* :catch_0 */
/* move-exception v1 */
/* .line 1144 */
/* .local v1, "e":Ljava/lang/Exception; */
try { // :try_start_3
final String v2 = "SchedBoost"; // const-string v2, "SchedBoost"
/* new-instance v3, Ljava/lang/StringBuilder; */
/* invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V */
/* const-string/jumbo v4, "startBoostInternal exception " */
(( java.lang.StringBuilder ) v3 ).append ( v4 ); // invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v3 ).append ( v1 ); // invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v3 ).toString ( ); // invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
android.util.Slog .e ( v2,v3 );
/* .line 1145 */
(( java.lang.Exception ) v1 ).printStackTrace ( ); // invoke-virtual {v1}, Ljava/lang/Exception;->printStackTrace()V
/* :try_end_3 */
/* .catchall {:try_start_3 ..:try_end_3} :catchall_0 */
/* .line 1147 */
} // .end local v1 # "e":Ljava/lang/Exception;
/* monitor-exit p0 */
/* .line 1110 */
} // .end local p1 # "threadInfo":Lcom/miui/server/rtboost/SchedBoostService$BoostThreadInfo;
} // :goto_3
/* monitor-exit p0 */
/* throw p1 */
} // .end method
private synchronized Boolean startBoostInternalWithOpcode ( com.miui.server.rtboost.SchedBoostService$BoostThreadInfo p0 ) {
/* .locals 7 */
/* .param p1, "threadInfo" # Lcom/miui/server/rtboost/SchedBoostService$BoostThreadInfo; */
/* monitor-enter p0 */
/* .line 1152 */
int v0 = 0; // const/4 v0, 0x0
try { // :try_start_0
v1 = (( com.miui.server.rtboost.SchedBoostService ) p0 ).needBoostHigherFreq ( p1 ); // invoke-virtual {p0, p1}, Lcom/miui/server/rtboost/SchedBoostService;->needBoostHigherFreq(Lcom/miui/server/rtboost/SchedBoostService$BoostThreadInfo;)Z
if ( v1 != null) { // if-eqz v1, :cond_0
/* .line 1153 */
/* const/16 v1, 0xa */
/* iput v1, p1, Lcom/miui/server/rtboost/SchedBoostService$BoostThreadInfo;->mode:I */
/* .line 1155 */
} // .end local p0 # "this":Lcom/miui/server/rtboost/SchedBoostService;
} // :cond_0
/* iget v1, p1, Lcom/miui/server/rtboost/SchedBoostService$BoostThreadInfo;->mode:I */
v1 = android.os.MiuiProcess .schedCodeToOpcode ( v1 );
/* :try_end_0 */
/* .catch Ljava/lang/Exception; {:try_start_0 ..:try_end_0} :catch_0 */
/* .catchall {:try_start_0 ..:try_end_0} :catchall_0 */
/* .line 1157 */
/* .local v1, "opcode":I */
/* if-gez v1, :cond_1 */
/* monitor-exit p0 */
/* .line 1159 */
/* .restart local p0 # "this":Lcom/miui/server/rtboost/SchedBoostService; */
} // :cond_1
try { // :try_start_1
/* iget v2, p0, Lcom/miui/server/rtboost/SchedBoostService;->mHandle:I */
if ( v2 != null) { // if-eqz v2, :cond_3
/* .line 1160 */
android.os.SystemClock .uptimeMillis ( );
/* move-result-wide v2 */
/* iget-wide v4, p0, Lcom/miui/server/rtboost/SchedBoostService;->mLastPerfAcqTime:J */
/* sub-long/2addr v2, v4 */
/* .line 1161 */
/* .local v2, "lastPerfAcqDuration":J */
/* const-wide/16 v4, 0x12c */
/* cmp-long v4, v2, v4 */
/* if-gez v4, :cond_3 */
/* .line 1162 */
/* sget-boolean v4, Lcom/miui/server/rtboost/SchedBoostService;->DEBUG:Z */
if ( v4 != null) { // if-eqz v4, :cond_2
final String v4 = "SchedBoost"; // const-string v4, "SchedBoost"
/* new-instance v5, Ljava/lang/StringBuilder; */
/* invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V */
final String v6 = "last perfacq still work, skip, "; // const-string v6, "last perfacq still work, skip, "
(( java.lang.StringBuilder ) v5 ).append ( v6 ); // invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v5 ).append ( v2, v3 ); // invoke-virtual {v5, v2, v3}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v5 ).toString ( ); // invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
android.util.Slog .d ( v4,v5 );
/* :try_end_1 */
/* .catch Ljava/lang/Exception; {:try_start_1 ..:try_end_1} :catch_0 */
/* .catchall {:try_start_1 ..:try_end_1} :catchall_0 */
/* .line 1164 */
} // .end local p0 # "this":Lcom/miui/server/rtboost/SchedBoostService;
} // :cond_2
/* monitor-exit p0 */
/* .line 1167 */
} // .end local v2 # "lastPerfAcqDuration":J
} // :cond_3
try { // :try_start_2
/* invoke-direct {p0}, Lcom/miui/server/rtboost/SchedBoostService;->stopBoostInternal()V */
/* .line 1168 */
v2 = this.mQcomPerfStub;
v3 = this.procName;
/* iget-wide v4, p1, Lcom/miui/server/rtboost/SchedBoostService$BoostThreadInfo;->duration:J */
/* long-to-int v4, v4 */
/* const/16 v5, 0x11a0 */
v2 = (( android.perf.PerfStub ) v2 ).perfHint ( v5, v3, v4, v1 ); // invoke-virtual {v2, v5, v3, v4, v1}, Landroid/perf/PerfStub;->perfHint(ILjava/lang/String;II)I
/* iput v2, p0, Lcom/miui/server/rtboost/SchedBoostService;->mHandle:I */
/* .line 1170 */
android.os.SystemClock .uptimeMillis ( );
/* move-result-wide v2 */
/* iput-wide v2, p0, Lcom/miui/server/rtboost/SchedBoostService;->mLastPerfAcqTime:J */
/* .line 1171 */
final String v2 = "SchedBoost"; // const-string v2, "SchedBoost"
/* new-instance v3, Ljava/lang/StringBuilder; */
/* invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V */
/* const-string/jumbo v4, "startBoostInternalWithOpcode " */
(( java.lang.StringBuilder ) v3 ).append ( v4 ); // invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
/* iget-wide v4, p1, Lcom/miui/server/rtboost/SchedBoostService$BoostThreadInfo;->duration:J */
(( java.lang.StringBuilder ) v3 ).append ( v4, v5 ); // invoke-virtual {v3, v4, v5}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;
final String v4 = " ms, opcode: "; // const-string v4, " ms, opcode: "
(( java.lang.StringBuilder ) v3 ).append ( v4 ); // invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v3 ).append ( v1 ); // invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v3 ).toString ( ); // invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
android.util.Slog .d ( v2,v3 );
/* :try_end_2 */
/* .catch Ljava/lang/Exception; {:try_start_2 ..:try_end_2} :catch_0 */
/* .catchall {:try_start_2 ..:try_end_2} :catchall_0 */
/* .line 1173 */
/* monitor-exit p0 */
int v0 = 1; // const/4 v0, 0x1
/* .line 1151 */
} // .end local v1 # "opcode":I
} // .end local p1 # "threadInfo":Lcom/miui/server/rtboost/SchedBoostService$BoostThreadInfo;
/* :catchall_0 */
/* move-exception p1 */
/* .line 1174 */
/* .restart local p1 # "threadInfo":Lcom/miui/server/rtboost/SchedBoostService$BoostThreadInfo; */
/* :catch_0 */
/* move-exception v1 */
/* .line 1175 */
/* .local v1, "e":Ljava/lang/Exception; */
try { // :try_start_3
final String v2 = "SchedBoost"; // const-string v2, "SchedBoost"
/* new-instance v3, Ljava/lang/StringBuilder; */
/* invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V */
/* const-string/jumbo v4, "startBoostInternalWithOpcode exception " */
(( java.lang.StringBuilder ) v3 ).append ( v4 ); // invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v3 ).append ( v1 ); // invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v3 ).toString ( ); // invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
android.util.Slog .e ( v2,v3 );
/* .line 1176 */
(( java.lang.Exception ) v1 ).printStackTrace ( ); // invoke-virtual {v1}, Ljava/lang/Exception;->printStackTrace()V
/* :try_end_3 */
/* .catchall {:try_start_3 ..:try_end_3} :catchall_0 */
/* .line 1178 */
} // .end local v1 # "e":Ljava/lang/Exception;
/* monitor-exit p0 */
/* .line 1151 */
} // .end local p1 # "threadInfo":Lcom/miui/server/rtboost/SchedBoostService$BoostThreadInfo;
} // :goto_0
/* monitor-exit p0 */
/* throw p1 */
} // .end method
private synchronized void stopBoostInternal ( ) {
/* .locals 4 */
/* monitor-enter p0 */
/* .line 1182 */
try { // :try_start_0
/* iget v0, p0, Lcom/miui/server/rtboost/SchedBoostService;->mHandle:I */
/* if-nez v0, :cond_1 */
/* .line 1183 */
/* sget-boolean v0, Lcom/miui/server/rtboost/SchedBoostService;->DEBUG:Z */
if ( v0 != null) { // if-eqz v0, :cond_0
final String v0 = "SchedBoost"; // const-string v0, "SchedBoost"
final String v1 = "has released boost yet"; // const-string v1, "has released boost yet"
android.util.Slog .w ( v0,v1 );
/* :try_end_0 */
/* .catchall {:try_start_0 ..:try_end_0} :catchall_0 */
/* .line 1184 */
} // .end local p0 # "this":Lcom/miui/server/rtboost/SchedBoostService;
} // :cond_0
/* monitor-exit p0 */
return;
/* .line 1187 */
} // :cond_1
try { // :try_start_1
/* sget-boolean v1, Lcom/miui/server/rtboost/SchedBoostService;->IS_MTK_DEVICE:Z */
if ( v1 != null) { // if-eqz v1, :cond_2
/* .line 1188 */
v1 = this.mMTKPerfStub;
/* .line 1190 */
} // :cond_2
v1 = this.mQcomPerfStub;
(( android.perf.PerfStub ) v1 ).perfLockReleaseHandler ( v0 ); // invoke-virtual {v1, v0}, Landroid/perf/PerfStub;->perfLockReleaseHandler(I)I
/* .line 1192 */
} // :goto_0
int v0 = 0; // const/4 v0, 0x0
/* iput v0, p0, Lcom/miui/server/rtboost/SchedBoostService;->mHandle:I */
/* .line 1193 */
final String v0 = "SchedBoost"; // const-string v0, "SchedBoost"
/* const-string/jumbo v1, "stopBoostInternal" */
android.util.Slog .d ( v0,v1 );
/* :try_end_1 */
/* .catch Ljava/lang/Exception; {:try_start_1 ..:try_end_1} :catch_0 */
/* .catchall {:try_start_1 ..:try_end_1} :catchall_0 */
/* .line 1197 */
/* .line 1194 */
/* :catch_0 */
/* move-exception v0 */
/* .line 1195 */
/* .local v0, "e":Ljava/lang/Exception; */
try { // :try_start_2
final String v1 = "SchedBoost"; // const-string v1, "SchedBoost"
/* new-instance v2, Ljava/lang/StringBuilder; */
/* invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V */
/* const-string/jumbo v3, "stopBoostInternal exception " */
(( java.lang.StringBuilder ) v2 ).append ( v3 ); // invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v2 ).append ( v0 ); // invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v2 ).toString ( ); // invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
android.util.Slog .e ( v1,v2 );
/* .line 1196 */
(( java.lang.Exception ) v0 ).printStackTrace ( ); // invoke-virtual {v0}, Ljava/lang/Exception;->printStackTrace()V
/* :try_end_2 */
/* .catchall {:try_start_2 ..:try_end_2} :catchall_0 */
/* .line 1198 */
} // .end local v0 # "e":Ljava/lang/Exception;
} // :goto_1
/* monitor-exit p0 */
return;
/* .line 1181 */
/* :catchall_0 */
/* move-exception v0 */
/* monitor-exit p0 */
/* throw v0 */
} // .end method
/* # virtual methods */
public void beginSchedThreads ( Integer[] p0, Long p1, java.lang.String p2, Integer p3 ) {
/* .locals 8 */
/* .param p1, "tids" # [I */
/* .param p2, "duration" # J */
/* .param p4, "procName" # Ljava/lang/String; */
/* .param p5, "mode" # I */
/* .line 334 */
/* sget-boolean v0, Lcom/miui/server/rtboost/SchedBoostService;->DEBUG:Z */
if ( v0 != null) { // if-eqz v0, :cond_0
/* .line 335 */
/* new-instance v0, Ljava/lang/StringBuilder; */
/* invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V */
final String v1 = "beginSchedThreads called, mode: "; // const-string v1, "beginSchedThreads called, mode: "
(( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v0 ).append ( p5 ); // invoke-virtual {v0, p5}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;
final String v1 = ", procName :"; // const-string v1, ", procName :"
(( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v0 ).append ( p4 ); // invoke-virtual {v0, p4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v0 ).toString ( ); // invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
final String v1 = "SchedBoost"; // const-string v1, "SchedBoost"
android.util.Slog .d ( v1,v0 );
/* .line 337 */
} // :cond_0
v2 = this.mHandler;
/* if-nez v2, :cond_1 */
/* .line 338 */
return;
/* .line 340 */
} // :cond_1
if ( p1 != null) { // if-eqz p1, :cond_6
/* array-length v0, p1 */
/* if-nez v0, :cond_2 */
/* .line 344 */
} // :cond_2
/* sparse-switch p5, :sswitch_data_0 */
/* .line 376 */
/* :sswitch_0 */
/* move-object v3, p1 */
/* move-wide v4, p2 */
/* move-object v6, p4 */
/* move v7, p5 */
/* invoke-virtual/range {v2 ..v7}, Lcom/miui/server/rtboost/SchedBoostService$Handler;->obtainBoostFreqMsg([IJLjava/lang/String;I)Landroid/os/Message; */
/* .line 377 */
(( android.os.Message ) v0 ).sendToTarget ( ); // invoke-virtual {v0}, Landroid/os/Message;->sendToTarget()V
/* .line 378 */
return;
/* .line 361 */
/* :sswitch_1 */
/* array-length v0, p1 */
int v1 = 0; // const/4 v1, 0x0
} // :goto_0
/* if-ge v1, v0, :cond_4 */
/* aget v2, p1, v1 */
/* .line 362 */
/* .local v2, "tid":I */
/* if-lez v2, :cond_3 */
/* .line 363 */
v3 = this.mHandler;
(( com.miui.server.rtboost.SchedBoostService$Handler ) v3 ).obtainReSetThreadsFIFOMsg ( v2, p2, p3, p5 ); // invoke-virtual {v3, v2, p2, p3, p5}, Lcom/miui/server/rtboost/SchedBoostService$Handler;->obtainReSetThreadsFIFOMsg(IJI)Landroid/os/Message;
/* .line 364 */
(( android.os.Message ) v3 ).sendToTarget ( ); // invoke-virtual {v3}, Landroid/os/Message;->sendToTarget()V
/* .line 361 */
} // .end local v2 # "tid":I
} // :cond_3
/* add-int/lit8 v1, v1, 0x1 */
/* .line 367 */
} // :cond_4
return;
/* .line 356 */
/* :sswitch_2 */
(( com.miui.server.rtboost.SchedBoostService$Handler ) v2 ).obtainSetThreadsFIFOMsg ( p1, p2, p3, p5 ); // invoke-virtual {v2, p1, p2, p3, p5}, Lcom/miui/server/rtboost/SchedBoostService$Handler;->obtainSetThreadsFIFOMsg([IJI)Landroid/os/Message;
/* .line 357 */
(( android.os.Message ) v0 ).sendToTarget ( ); // invoke-virtual {v0}, Landroid/os/Message;->sendToTarget()V
/* .line 358 */
return;
/* .line 371 */
/* :sswitch_3 */
final String p4 = "com.miui.home"; // const-string p4, "com.miui.home"
/* .line 372 */
/* .line 351 */
/* :sswitch_4 */
(( com.miui.server.rtboost.SchedBoostService$Handler ) v2 ).obtainThreadBindBigCoreMsg ( p1, p2, p3 ); // invoke-virtual {v2, p1, p2, p3}, Lcom/miui/server/rtboost/SchedBoostService$Handler;->obtainThreadBindBigCoreMsg([IJ)Landroid/os/Message;
/* .line 352 */
(( android.os.Message ) v0 ).sendToTarget ( ); // invoke-virtual {v0}, Landroid/os/Message;->sendToTarget()V
/* .line 353 */
return;
/* .line 346 */
/* :sswitch_5 */
(( com.miui.server.rtboost.SchedBoostService$Handler ) v2 ).obtainSetThreadsAlwaysFIFOMsg ( p1 ); // invoke-virtual {v2, p1}, Lcom/miui/server/rtboost/SchedBoostService$Handler;->obtainSetThreadsAlwaysFIFOMsg([I)Landroid/os/Message;
/* .line 347 */
(( android.os.Message ) v0 ).sendToTarget ( ); // invoke-virtual {v0}, Landroid/os/Message;->sendToTarget()V
/* .line 348 */
return;
/* .line 384 */
} // :goto_1
/* const-wide/16 v0, 0x0 */
/* cmp-long v0, p2, v0 */
/* if-ltz v0, :cond_5 */
/* const-wide/16 v0, 0x1770 */
/* cmp-long v0, p2, v0 */
/* if-gtz v0, :cond_5 */
/* .line 385 */
/* move-object v3, p1 */
/* move-wide v4, p2 */
/* move-object v6, p4 */
/* move v7, p5 */
/* invoke-virtual/range {v2 ..v7}, Lcom/miui/server/rtboost/SchedBoostService$Handler;->obtainBeginSchedThreadsMsg([IJLjava/lang/String;I)Landroid/os/Message; */
/* .line 386 */
(( android.os.Message ) v0 ).sendToTarget ( ); // invoke-virtual {v0}, Landroid/os/Message;->sendToTarget()V
/* .line 388 */
} // :cond_5
return;
/* .line 341 */
} // :cond_6
} // :goto_2
return;
/* :sswitch_data_0 */
/* .sparse-switch */
/* 0x3 -> :sswitch_5 */
/* 0x7 -> :sswitch_4 */
/* 0x9 -> :sswitch_3 */
/* 0xc -> :sswitch_2 */
/* 0xd -> :sswitch_1 */
/* 0x65 -> :sswitch_0 */
/* 0x66 -> :sswitch_0 */
} // .end sparse-switch
} // .end method
public void boostHomeAnim ( Long p0, Integer p1 ) {
/* .locals 8 */
/* .param p1, "duration" # J */
/* .param p3, "mode" # I */
/* .line 1258 */
/* iget v0, p0, Lcom/miui/server/rtboost/SchedBoostService;->mHomeMainThreadId:I */
/* iget v1, p0, Lcom/miui/server/rtboost/SchedBoostService;->mHomeRenderThreadId:I */
/* filled-new-array {v0, v1}, [I */
final String v6 = "com.miui.home"; // const-string v6, "com.miui.home"
/* move-object v2, p0 */
/* move-wide v4, p1 */
/* move v7, p3 */
/* invoke-virtual/range {v2 ..v7}, Lcom/miui/server/rtboost/SchedBoostService;->beginSchedThreads([IJLjava/lang/String;I)V */
/* .line 1260 */
return;
} // .end method
public Boolean checkThreadBoost ( Integer p0 ) {
/* .locals 1 */
/* .param p1, "tid" # I */
/* .line 1217 */
v0 = this.mBoostingMap;
(( com.miui.server.rtboost.SchedBoostService$BoostingPackageMap ) v0 ).getByTid ( p1 ); // invoke-virtual {v0, p1}, Lcom/miui/server/rtboost/SchedBoostService$BoostingPackageMap;->getByTid(I)Lcom/miui/server/rtboost/SchedBoostService$BoostThreadInfo;
if ( v0 != null) { // if-eqz v0, :cond_0
/* .line 1218 */
int v0 = 1; // const/4 v0, 0x1
/* .line 1220 */
} // :cond_0
int v0 = 0; // const/4 v0, 0x0
} // .end method
protected void dump ( java.io.FileDescriptor p0, java.io.PrintWriter p1, java.lang.String[] p2 ) {
/* .locals 2 */
/* .param p1, "fd" # Ljava/io/FileDescriptor; */
/* .param p2, "pw" # Ljava/io/PrintWriter; */
/* .param p3, "args" # [Ljava/lang/String; */
/* .line 1299 */
v0 = this.mContext;
final String v1 = "SchedBoost"; // const-string v1, "SchedBoost"
v0 = com.android.internal.util.DumpUtils .checkDumpPermission ( v0,v1,p2 );
/* if-nez v0, :cond_0 */
return;
/* .line 1300 */
} // :cond_0
final String v0 = "sched boost (SchedBoostService):"; // const-string v0, "sched boost (SchedBoostService):"
(( java.io.PrintWriter ) p2 ).println ( v0 ); // invoke-virtual {p2, v0}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V
/* .line 1302 */
try { // :try_start_0
com.android.server.wm.RealTimeModeControllerImpl .get ( );
com.android.server.wm.RealTimeModeControllerImpl .dump ( p2,p3 );
/* .line 1303 */
/* invoke-direct {p0, p2, p3}, Lcom/miui/server/rtboost/SchedBoostService;->dump(Ljava/io/PrintWriter;[Ljava/lang/String;)V */
/* :try_end_0 */
/* .catch Ljava/lang/Exception; {:try_start_0 ..:try_end_0} :catch_0 */
/* .line 1306 */
/* .line 1304 */
/* :catch_0 */
/* move-exception v0 */
/* .line 1305 */
/* .local v0, "e":Ljava/lang/Exception; */
(( java.lang.Exception ) v0 ).printStackTrace ( ); // invoke-virtual {v0}, Ljava/lang/Exception;->printStackTrace()V
/* .line 1307 */
} // .end local v0 # "e":Ljava/lang/Exception;
} // :goto_0
return;
} // .end method
public void enableSchedBoost ( Boolean p0 ) {
/* .locals 2 */
/* .param p1, "enable" # Z */
/* .line 328 */
/* new-instance v0, Ljava/lang/StringBuilder; */
/* invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V */
v1 = android.os.Binder .getCallingPid ( );
(( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;
final String v1 = " set sched boost enable:"; // const-string v1, " set sched boost enable:"
(( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v0 ).append ( p1 ); // invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v0 ).toString ( ); // invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
final String v1 = "SchedBoost"; // const-string v1, "SchedBoost"
android.util.Slog .w ( v1,v0 );
/* .line 329 */
/* xor-int/lit8 v0, p1, 0x1 */
/* iput-boolean v0, p0, Lcom/miui/server/rtboost/SchedBoostService;->mSimpleNotNeedSched:Z */
/* .line 330 */
return;
} // .end method
public Boolean needBoostHigherFreq ( com.miui.server.rtboost.SchedBoostService$BoostThreadInfo p0 ) {
/* .locals 3 */
/* .param p1, "threadInfo" # Lcom/miui/server/rtboost/SchedBoostService$BoostThreadInfo; */
/* .line 1263 */
/* sget-boolean v0, Lcom/miui/server/rtboost/SchedBoostService;->IS_MTK_DEVICE:Z */
/* if-nez v0, :cond_1 */
/* .line 1264 */
v0 = this.mContext;
(( android.content.Context ) v0 ).getResources ( ); // invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;
(( android.content.res.Resources ) v0 ).getConfiguration ( ); // invoke-virtual {v0}, Landroid/content/res/Resources;->getConfiguration()Landroid/content/res/Configuration;
/* iget v0, v0, Landroid/content/res/Configuration;->orientation:I */
/* .line 1265 */
/* .local v0, "orientation":I */
int v1 = 2; // const/4 v1, 0x2
/* if-ne v0, v1, :cond_1 */
/* iget v1, p1, Lcom/miui/server/rtboost/SchedBoostService$BoostThreadInfo;->mode:I */
int v2 = 4; // const/4 v2, 0x4
/* if-ne v1, v2, :cond_1 */
/* .line 1267 */
/* sget-boolean v1, Lcom/miui/server/rtboost/SchedBoostService;->DEBUG:Z */
if ( v1 != null) { // if-eqz v1, :cond_0
final String v1 = "SchedBoost"; // const-string v1, "SchedBoost"
final String v2 = "needBoostHigherFreq"; // const-string v2, "needBoostHigherFreq"
android.util.Slog .d ( v1,v2 );
/* .line 1268 */
} // :cond_0
int v1 = 1; // const/4 v1, 0x1
/* .line 1271 */
} // .end local v0 # "orientation":I
} // :cond_1
int v0 = 0; // const/4 v0, 0x0
} // .end method
public void onShellCommand ( java.io.FileDescriptor p0, java.io.FileDescriptor p1, java.io.FileDescriptor p2, java.lang.String[] p3, android.os.ShellCallback p4, android.os.ResultReceiver p5 ) {
/* .locals 8 */
/* .param p1, "in" # Ljava/io/FileDescriptor; */
/* .param p2, "out" # Ljava/io/FileDescriptor; */
/* .param p3, "err" # Ljava/io/FileDescriptor; */
/* .param p4, "args" # [Ljava/lang/String; */
/* .param p5, "callback" # Landroid/os/ShellCallback; */
/* .param p6, "resultReceiver" # Landroid/os/ResultReceiver; */
/* .annotation system Ldalvik/annotation/Throws; */
/* value = { */
/* Landroid/os/RemoteException; */
/* } */
} // .end annotation
/* .line 1313 */
/* new-instance v0, Lcom/miui/server/rtboost/SchedBoostService$SchedBoostShellCommand; */
/* invoke-direct {v0, p0}, Lcom/miui/server/rtboost/SchedBoostService$SchedBoostShellCommand;-><init>(Lcom/miui/server/rtboost/SchedBoostService;)V */
/* .line 1314 */
/* move-object v1, p0 */
/* move-object v2, p1 */
/* move-object v3, p2 */
/* move-object v4, p3 */
/* move-object v5, p4 */
/* move-object v6, p5 */
/* move-object v7, p6 */
/* invoke-virtual/range {v0 ..v7}, Lcom/miui/server/rtboost/SchedBoostService$SchedBoostShellCommand;->exec(Landroid/os/Binder;Ljava/io/FileDescriptor;Ljava/io/FileDescriptor;Ljava/io/FileDescriptor;[Ljava/lang/String;Landroid/os/ShellCallback;Landroid/os/ResultReceiver;)I */
/* .line 1315 */
return;
} // .end method
public void schedProcessBoost ( com.android.server.wm.WindowProcessListener p0, java.lang.String p1, Integer p2, Integer p3, Integer p4, Long p5 ) {
/* .locals 0 */
/* .param p1, "proc" # Lcom/android/server/wm/WindowProcessListener; */
/* .param p2, "procName" # Ljava/lang/String; */
/* .param p3, "pid" # I */
/* .param p4, "rtid" # I */
/* .param p5, "schedMode" # I */
/* .param p6, "timeout" # J */
/* .line 308 */
/* nop */
/* .line 309 */
return;
} // .end method
public void setRenderThreadTid ( com.android.server.wm.WindowProcessController p0 ) {
/* .locals 1 */
/* .param p1, "wpc" # Lcom/android/server/wm/WindowProcessController; */
/* .line 315 */
/* if-nez p1, :cond_0 */
return;
/* .line 316 */
} // :cond_0
v0 = com.android.server.wm.RealTimeModeControllerImpl .isHomeProcess ( p1 );
if ( v0 != null) { // if-eqz v0, :cond_1
/* .line 317 */
v0 = (( com.android.server.wm.WindowProcessController ) p1 ).getPid ( ); // invoke-virtual {p1}, Lcom/android/server/wm/WindowProcessController;->getPid()I
/* iput v0, p0, Lcom/miui/server/rtboost/SchedBoostService;->mHomeMainThreadId:I */
/* .line 318 */
/* iget v0, p1, Lcom/android/server/wm/WindowProcessController;->mRenderThreadTid:I */
/* iput v0, p0, Lcom/miui/server/rtboost/SchedBoostService;->mHomeRenderThreadId:I */
/* .line 319 */
int v0 = 1; // const/4 v0, 0x1
/* iput-boolean v0, p0, Lcom/miui/server/rtboost/SchedBoostService;->mPendingHomeGpuTids:Z */
/* .line 320 */
int v0 = 0; // const/4 v0, 0x0
/* iput v0, p0, Lcom/miui/server/rtboost/SchedBoostService;->mBoostHomeGpuTidsTryCnt:I */
/* .line 321 */
} // :cond_1
v0 = com.android.server.wm.RealTimeModeControllerImpl .isSystemUIProcess ( p1 );
if ( v0 != null) { // if-eqz v0, :cond_2
/* .line 322 */
v0 = (( com.android.server.wm.WindowProcessController ) p1 ).getPid ( ); // invoke-virtual {p1}, Lcom/android/server/wm/WindowProcessController;->getPid()I
/* iput v0, p0, Lcom/miui/server/rtboost/SchedBoostService;->mSystemUIMainThreadId:I */
/* .line 323 */
/* iget v0, p1, Lcom/android/server/wm/WindowProcessController;->mRenderThreadTid:I */
/* iput v0, p0, Lcom/miui/server/rtboost/SchedBoostService;->mSystemUIRenderThreadId:I */
/* .line 325 */
} // :cond_2
} // :goto_0
return;
} // .end method
public void setThreadSavedPriority ( Integer[] p0, Integer p1 ) {
/* .locals 6 */
/* .param p1, "tid" # [I */
/* .param p2, "prio" # I */
/* .line 1224 */
/* array-length v0, p1 */
/* if-lez v0, :cond_2 */
/* .line 1225 */
/* array-length v0, p1 */
int v1 = 0; // const/4 v1, 0x0
} // :goto_0
/* if-ge v1, v0, :cond_2 */
/* aget v2, p1, v1 */
/* .line 1226 */
/* .local v2, "threadTid":I */
v3 = this.mBoostingMap;
(( com.miui.server.rtboost.SchedBoostService$BoostingPackageMap ) v3 ).getByTid ( v2 ); // invoke-virtual {v3, v2}, Lcom/miui/server/rtboost/SchedBoostService$BoostingPackageMap;->getByTid(I)Lcom/miui/server/rtboost/SchedBoostService$BoostThreadInfo;
/* .line 1227 */
/* .local v3, "info":Lcom/miui/server/rtboost/SchedBoostService$BoostThreadInfo; */
if ( v3 != null) { // if-eqz v3, :cond_1
/* .line 1228 */
/* sget-boolean v4, Lcom/miui/server/rtboost/SchedBoostService;->DEBUG:Z */
if ( v4 != null) { // if-eqz v4, :cond_0
/* .line 1229 */
/* new-instance v4, Ljava/lang/StringBuilder; */
/* invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V */
/* const-string/jumbo v5, "setThreadSavedPriority tid: " */
(( java.lang.StringBuilder ) v4 ).append ( v5 ); // invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v4 ).append ( v2 ); // invoke-virtual {v4, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;
final String v5 = ", before: "; // const-string v5, ", before: "
(( java.lang.StringBuilder ) v4 ).append ( v5 ); // invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
/* iget v5, v3, Lcom/miui/server/rtboost/SchedBoostService$BoostThreadInfo;->savedPriority:I */
(( java.lang.StringBuilder ) v4 ).append ( v5 ); // invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;
final String v5 = ", after: "; // const-string v5, ", after: "
(( java.lang.StringBuilder ) v4 ).append ( v5 ); // invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v4 ).append ( p2 ); // invoke-virtual {v4, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v4 ).toString ( ); // invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
final String v5 = "SchedBoost"; // const-string v5, "SchedBoost"
android.util.Slog .d ( v5,v4 );
/* .line 1232 */
} // :cond_0
/* iput p2, v3, Lcom/miui/server/rtboost/SchedBoostService$BoostThreadInfo;->savedPriority:I */
/* .line 1225 */
} // .end local v2 # "threadTid":I
} // .end local v3 # "info":Lcom/miui/server/rtboost/SchedBoostService$BoostThreadInfo;
} // :cond_1
/* add-int/lit8 v1, v1, 0x1 */
/* .line 1236 */
} // :cond_2
return;
} // .end method
public void stopCurrentSchedBoost ( ) {
/* .locals 6 */
/* .line 1239 */
v0 = this.mBoostingMap;
com.miui.server.rtboost.SchedBoostService$BoostingPackageMap .-$$Nest$fgetmMapLock ( v0 );
/* monitor-enter v0 */
/* .line 1240 */
try { // :try_start_0
v1 = this.mBoostingMap;
(( com.miui.server.rtboost.SchedBoostService$BoostingPackageMap ) v1 ).getList ( ); // invoke-virtual {v1}, Lcom/miui/server/rtboost/SchedBoostService$BoostingPackageMap;->getList()Ljava/util/ArrayList;
/* .line 1241 */
/* .local v1, "list":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/miui/server/rtboost/SchedBoostService$BoostThreadInfo;>;" */
if ( v1 != null) { // if-eqz v1, :cond_1
v2 = (( java.util.ArrayList ) v1 ).size ( ); // invoke-virtual {v1}, Ljava/util/ArrayList;->size()I
/* if-lez v2, :cond_1 */
/* .line 1242 */
int v2 = 0; // const/4 v2, 0x0
/* .local v2, "i":I */
} // :goto_0
v3 = (( java.util.ArrayList ) v1 ).size ( ); // invoke-virtual {v1}, Ljava/util/ArrayList;->size()I
/* if-ge v2, v3, :cond_1 */
/* .line 1243 */
(( java.util.ArrayList ) v1 ).get ( v2 ); // invoke-virtual {v1, v2}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;
/* check-cast v3, Lcom/miui/server/rtboost/SchedBoostService$BoostThreadInfo; */
/* .line 1245 */
/* .local v3, "threadInfo":Lcom/miui/server/rtboost/SchedBoostService$BoostThreadInfo; */
/* iget-boolean v4, p0, Lcom/miui/server/rtboost/SchedBoostService;->mScreenOff:Z */
/* if-nez v4, :cond_0 */
/* iget v4, v3, Lcom/miui/server/rtboost/SchedBoostService$BoostThreadInfo;->mode:I */
int v5 = 4; // const/4 v5, 0x4
/* if-ne v4, v5, :cond_0 */
/* .line 1246 */
/* monitor-exit v0 */
return;
/* .line 1248 */
} // :cond_0
v4 = this.mHandler;
(( com.miui.server.rtboost.SchedBoostService$Handler ) v4 ).removeResetThreadsMsg ( v3 ); // invoke-virtual {v4, v3}, Lcom/miui/server/rtboost/SchedBoostService$Handler;->removeResetThreadsMsg(Lcom/miui/server/rtboost/SchedBoostService$BoostThreadInfo;)V
/* .line 1249 */
v4 = this.mHandler;
(( com.miui.server.rtboost.SchedBoostService$Handler ) v4 ).obtainResetSchedThreadsMsg ( v3 ); // invoke-virtual {v4, v3}, Lcom/miui/server/rtboost/SchedBoostService$Handler;->obtainResetSchedThreadsMsg(Lcom/miui/server/rtboost/SchedBoostService$BoostThreadInfo;)Landroid/os/Message;
/* .line 1250 */
(( android.os.Message ) v4 ).sendToTarget ( ); // invoke-virtual {v4}, Landroid/os/Message;->sendToTarget()V
/* .line 1242 */
} // .end local v3 # "threadInfo":Lcom/miui/server/rtboost/SchedBoostService$BoostThreadInfo;
/* add-int/lit8 v2, v2, 0x1 */
/* .line 1253 */
} // .end local v2 # "i":I
} // :cond_1
v2 = this.mBoostingMap;
(( com.miui.server.rtboost.SchedBoostService$BoostingPackageMap ) v2 ).clear ( ); // invoke-virtual {v2}, Lcom/miui/server/rtboost/SchedBoostService$BoostingPackageMap;->clear()V
/* .line 1254 */
} // .end local v1 # "list":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/miui/server/rtboost/SchedBoostService$BoostThreadInfo;>;"
/* monitor-exit v0 */
/* .line 1255 */
return;
/* .line 1254 */
/* :catchall_0 */
/* move-exception v1 */
/* monitor-exit v0 */
/* :try_end_0 */
/* .catchall {:try_start_0 ..:try_end_0} :catchall_0 */
/* throw v1 */
} // .end method
