.class Lcom/miui/server/rtboost/SchedBoostService$1;
.super Landroid/content/BroadcastReceiver;
.source "SchedBoostService.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/miui/server/rtboost/SchedBoostService;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/miui/server/rtboost/SchedBoostService;


# direct methods
.method constructor <init>(Lcom/miui/server/rtboost/SchedBoostService;)V
    .locals 0
    .param p1, "this$0"    # Lcom/miui/server/rtboost/SchedBoostService;

    .line 170
    iput-object p1, p0, Lcom/miui/server/rtboost/SchedBoostService$1;->this$0:Lcom/miui/server/rtboost/SchedBoostService;

    invoke-direct {p0}, Landroid/content/BroadcastReceiver;-><init>()V

    return-void
.end method


# virtual methods
.method public onReceive(Landroid/content/Context;Landroid/content/Intent;)V
    .locals 2
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "intent"    # Landroid/content/Intent;

    .line 173
    const-string v0, "android.intent.action.SCREEN_OFF"

    invoke-virtual {p2}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 174
    iget-object v0, p0, Lcom/miui/server/rtboost/SchedBoostService$1;->this$0:Lcom/miui/server/rtboost/SchedBoostService;

    const/4 v1, 0x1

    invoke-static {v0, v1}, Lcom/miui/server/rtboost/SchedBoostService;->-$$Nest$fputmScreenOff(Lcom/miui/server/rtboost/SchedBoostService;Z)V

    .line 175
    iget-object v0, p0, Lcom/miui/server/rtboost/SchedBoostService$1;->this$0:Lcom/miui/server/rtboost/SchedBoostService;

    invoke-virtual {v0}, Lcom/miui/server/rtboost/SchedBoostService;->stopCurrentSchedBoost()V

    goto :goto_0

    .line 176
    :cond_0
    const-string v0, "android.intent.action.SCREEN_ON"

    invoke-virtual {p2}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 178
    iget-object v0, p0, Lcom/miui/server/rtboost/SchedBoostService$1;->this$0:Lcom/miui/server/rtboost/SchedBoostService;

    const/4 v1, 0x0

    invoke-static {v0, v1}, Lcom/miui/server/rtboost/SchedBoostService;->-$$Nest$fputmScreenOff(Lcom/miui/server/rtboost/SchedBoostService;Z)V

    .line 179
    return-void

    .line 181
    :cond_1
    :goto_0
    return-void
.end method
