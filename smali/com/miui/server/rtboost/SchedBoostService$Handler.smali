.class final Lcom/miui/server/rtboost/SchedBoostService$Handler;
.super Landroid/os/Handler;
.source "SchedBoostService.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/miui/server/rtboost/SchedBoostService;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x12
    name = "Handler"
.end annotation


# instance fields
.field private final MSG_BEGIN_SCHED_THREADS:I

.field private final MSG_BOOST_FREQ:I

.field private final MSG_RESET_SCHED_THREADS:I

.field private final MSG_RESET_THREAD_FIFO:I

.field private final MSG_SCHED_THREADS:I

.field private final MSG_SET_THREAD_ALWAYS_FIFO:I

.field private final MSG_SET_THREAD_BIND_BIG_CORE:I

.field private final MSG_SET_THREAD_FIFO:I

.field private final MSG_SET_THREAD_UN_BIND_BIG_CORE:I

.field final synthetic this$0:Lcom/miui/server/rtboost/SchedBoostService;


# direct methods
.method public constructor <init>(Lcom/miui/server/rtboost/SchedBoostService;Landroid/os/Looper;)V
    .locals 0
    .param p2, "looper"    # Landroid/os/Looper;

    .line 692
    iput-object p1, p0, Lcom/miui/server/rtboost/SchedBoostService$Handler;->this$0:Lcom/miui/server/rtboost/SchedBoostService;

    .line 693
    invoke-direct {p0, p2}, Landroid/os/Handler;-><init>(Landroid/os/Looper;)V

    .line 682
    const/4 p1, 0x1

    iput p1, p0, Lcom/miui/server/rtboost/SchedBoostService$Handler;->MSG_SCHED_THREADS:I

    .line 683
    const/4 p1, 0x2

    iput p1, p0, Lcom/miui/server/rtboost/SchedBoostService$Handler;->MSG_RESET_SCHED_THREADS:I

    .line 684
    const/4 p1, 0x3

    iput p1, p0, Lcom/miui/server/rtboost/SchedBoostService$Handler;->MSG_BEGIN_SCHED_THREADS:I

    .line 685
    const/4 p1, 0x4

    iput p1, p0, Lcom/miui/server/rtboost/SchedBoostService$Handler;->MSG_SET_THREAD_ALWAYS_FIFO:I

    .line 686
    const/4 p1, 0x5

    iput p1, p0, Lcom/miui/server/rtboost/SchedBoostService$Handler;->MSG_SET_THREAD_BIND_BIG_CORE:I

    .line 687
    const/4 p1, 0x6

    iput p1, p0, Lcom/miui/server/rtboost/SchedBoostService$Handler;->MSG_SET_THREAD_UN_BIND_BIG_CORE:I

    .line 688
    const/4 p1, 0x7

    iput p1, p0, Lcom/miui/server/rtboost/SchedBoostService$Handler;->MSG_SET_THREAD_FIFO:I

    .line 689
    const/16 p1, 0x8

    iput p1, p0, Lcom/miui/server/rtboost/SchedBoostService$Handler;->MSG_RESET_THREAD_FIFO:I

    .line 690
    const/16 p1, 0x9

    iput p1, p0, Lcom/miui/server/rtboost/SchedBoostService$Handler;->MSG_BOOST_FREQ:I

    .line 694
    return-void
.end method


# virtual methods
.method public handleMessage(Landroid/os/Message;)V
    .locals 19
    .param p1, "msg"    # Landroid/os/Message;

    .line 698
    move-object/from16 v0, p0

    move-object/from16 v1, p1

    invoke-super/range {p0 .. p1}, Landroid/os/Handler;->handleMessage(Landroid/os/Message;)V

    .line 699
    iget v2, v1, Landroid/os/Message;->what:I

    const/4 v3, 0x0

    const-string v4, "beginSchedThread, mode: "

    const-string v5, "name"

    const-wide/16 v6, 0x40

    const-string/jumbo v8, "timeout"

    const-string v9, "mode"

    const-string/jumbo v10, "tids"

    packed-switch v2, :pswitch_data_0

    goto/16 :goto_2

    .line 767
    :pswitch_0
    invoke-virtual/range {p1 .. p1}, Landroid/os/Message;->getData()Landroid/os/Bundle;

    move-result-object v2

    .line 768
    .local v2, "bundle":Landroid/os/Bundle;
    invoke-virtual {v2, v5}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    .line 769
    .local v3, "procName":Ljava/lang/String;
    invoke-virtual {v2, v10}, Landroid/os/Bundle;->getIntArray(Ljava/lang/String;)[I

    move-result-object v5

    .line 770
    .local v5, "tids":[I
    invoke-virtual {v2, v8}, Landroid/os/Bundle;->getLong(Ljava/lang/String;)J

    move-result-wide v17

    .line 771
    .local v17, "duration":J
    invoke-virtual {v2, v9}, Landroid/os/Bundle;->getInt(Ljava/lang/String;)I

    move-result v8

    .line 772
    .local v8, "mode":I
    new-instance v9, Ljava/lang/StringBuilder;

    invoke-direct {v9}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v9, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v8}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v6, v7, v4}, Landroid/os/Trace;->traceBegin(JLjava/lang/String;)V

    .line 775
    iget-object v11, v0, Lcom/miui/server/rtboost/SchedBoostService$Handler;->this$0:Lcom/miui/server/rtboost/SchedBoostService;

    const-wide/16 v13, -0x1

    move-object v12, v5

    move-object v15, v3

    move/from16 v16, v8

    invoke-static/range {v11 .. v16}, Lcom/miui/server/rtboost/SchedBoostService;->-$$Nest$mhandleBoostFreq(Lcom/miui/server/rtboost/SchedBoostService;[IJLjava/lang/String;I)V

    .line 776
    invoke-static {v6, v7}, Landroid/os/Trace;->traceEnd(J)V

    .line 777
    goto/16 :goto_2

    .line 743
    .end local v2    # "bundle":Landroid/os/Bundle;
    .end local v3    # "procName":Ljava/lang/String;
    .end local v5    # "tids":[I
    .end local v8    # "mode":I
    .end local v17    # "duration":J
    :pswitch_1
    invoke-virtual/range {p1 .. p1}, Landroid/os/Message;->getData()Landroid/os/Bundle;

    move-result-object v2

    .line 744
    .restart local v2    # "bundle":Landroid/os/Bundle;
    const-string/jumbo v3, "tid"

    invoke-virtual {v2, v3}, Landroid/os/Bundle;->getInt(Ljava/lang/String;)I

    move-result v3

    .line 745
    .local v3, "tid":I
    invoke-virtual {v2, v8}, Landroid/os/Bundle;->getLong(Ljava/lang/String;)J

    move-result-wide v4

    .line 746
    .local v4, "duration":J
    invoke-virtual {v2, v9}, Landroid/os/Bundle;->getInt(Ljava/lang/String;)I

    move-result v6

    .line 747
    .local v6, "mode":I
    iget-object v7, v0, Lcom/miui/server/rtboost/SchedBoostService$Handler;->this$0:Lcom/miui/server/rtboost/SchedBoostService;

    invoke-static {v7, v3, v4, v5, v6}, Lcom/miui/server/rtboost/SchedBoostService;->-$$Nest$mhandleReSetThreadsFIFO(Lcom/miui/server/rtboost/SchedBoostService;IJI)V

    .line 748
    goto/16 :goto_2

    .line 731
    .end local v2    # "bundle":Landroid/os/Bundle;
    .end local v3    # "tid":I
    .end local v4    # "duration":J
    .end local v6    # "mode":I
    :pswitch_2
    invoke-virtual/range {p1 .. p1}, Landroid/os/Message;->getData()Landroid/os/Bundle;

    move-result-object v2

    .line 732
    .restart local v2    # "bundle":Landroid/os/Bundle;
    invoke-virtual {v2, v10}, Landroid/os/Bundle;->getIntArray(Ljava/lang/String;)[I

    move-result-object v4

    .line 733
    .local v4, "tids":[I
    invoke-virtual {v2, v8}, Landroid/os/Bundle;->getLong(Ljava/lang/String;)J

    move-result-wide v5

    .line 734
    .local v5, "duration":J
    invoke-virtual {v2, v9}, Landroid/os/Bundle;->getInt(Ljava/lang/String;)I

    move-result v7

    .line 735
    .local v7, "mode":I
    if-eqz v4, :cond_0

    array-length v8, v4

    if-lez v8, :cond_0

    .line 736
    array-length v8, v4

    :goto_0
    if-ge v3, v8, :cond_0

    aget v9, v4, v3

    .line 737
    .local v9, "tid":I
    iget-object v10, v0, Lcom/miui/server/rtboost/SchedBoostService$Handler;->this$0:Lcom/miui/server/rtboost/SchedBoostService;

    invoke-static {v10, v9, v5, v6, v7}, Lcom/miui/server/rtboost/SchedBoostService;->-$$Nest$mhandleSetThreadsFIFO(Lcom/miui/server/rtboost/SchedBoostService;IJI)V

    .line 736
    .end local v9    # "tid":I
    add-int/lit8 v3, v3, 0x1

    goto :goto_0

    .line 762
    .end local v2    # "bundle":Landroid/os/Bundle;
    .end local v4    # "tids":[I
    .end local v5    # "duration":J
    .end local v7    # "mode":I
    :pswitch_3
    iget-object v2, v1, Landroid/os/Message;->obj:Ljava/lang/Object;

    check-cast v2, Ljava/lang/Integer;

    .line 763
    .local v2, "tid":Ljava/lang/Integer;
    iget-object v3, v0, Lcom/miui/server/rtboost/SchedBoostService$Handler;->this$0:Lcom/miui/server/rtboost/SchedBoostService;

    invoke-static {v3, v2}, Lcom/miui/server/rtboost/SchedBoostService;->-$$Nest$mhandleThreadsUnBindBigCoreMsg(Lcom/miui/server/rtboost/SchedBoostService;Ljava/lang/Integer;)V

    .line 764
    goto/16 :goto_2

    .line 751
    .end local v2    # "tid":Ljava/lang/Integer;
    :pswitch_4
    invoke-virtual/range {p1 .. p1}, Landroid/os/Message;->getData()Landroid/os/Bundle;

    move-result-object v2

    .line 752
    .local v2, "bundle":Landroid/os/Bundle;
    invoke-virtual {v2, v10}, Landroid/os/Bundle;->getIntArray(Ljava/lang/String;)[I

    move-result-object v4

    .line 753
    .restart local v4    # "tids":[I
    const-string v5, "duration"

    invoke-virtual {v2, v5}, Landroid/os/Bundle;->getLong(Ljava/lang/String;)J

    move-result-wide v5

    .line 754
    .restart local v5    # "duration":J
    if-eqz v4, :cond_0

    array-length v7, v4

    if-lez v7, :cond_0

    .line 755
    array-length v7, v4

    :goto_1
    if-ge v3, v7, :cond_0

    aget v8, v4, v3

    .line 756
    .local v8, "tid":I
    iget-object v9, v0, Lcom/miui/server/rtboost/SchedBoostService$Handler;->this$0:Lcom/miui/server/rtboost/SchedBoostService;

    invoke-static {v8}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v10

    invoke-static {v9, v10, v5, v6}, Lcom/miui/server/rtboost/SchedBoostService;->-$$Nest$mhandleThreadsBindBigCoreMsg(Lcom/miui/server/rtboost/SchedBoostService;Ljava/lang/Integer;J)V

    .line 755
    .end local v8    # "tid":I
    add-int/lit8 v3, v3, 0x1

    goto :goto_1

    .line 723
    .end local v2    # "bundle":Landroid/os/Bundle;
    .end local v4    # "tids":[I
    .end local v5    # "duration":J
    :pswitch_5
    invoke-virtual/range {p1 .. p1}, Landroid/os/Message;->getData()Landroid/os/Bundle;

    move-result-object v2

    .line 724
    .restart local v2    # "bundle":Landroid/os/Bundle;
    invoke-virtual {v2, v10}, Landroid/os/Bundle;->getIntArray(Ljava/lang/String;)[I

    move-result-object v3

    .line 725
    .local v3, "tids":[I
    if-eqz v3, :cond_0

    array-length v4, v3

    if-lez v4, :cond_0

    .line 726
    iget-object v4, v0, Lcom/miui/server/rtboost/SchedBoostService$Handler;->this$0:Lcom/miui/server/rtboost/SchedBoostService;

    invoke-static {v4, v3}, Lcom/miui/server/rtboost/SchedBoostService;->-$$Nest$mhandleSetThreadsAlwaysFIFO(Lcom/miui/server/rtboost/SchedBoostService;[I)V

    goto :goto_2

    .line 711
    .end local v2    # "bundle":Landroid/os/Bundle;
    .end local v3    # "tids":[I
    :pswitch_6
    invoke-virtual/range {p1 .. p1}, Landroid/os/Message;->getData()Landroid/os/Bundle;

    move-result-object v2

    .line 712
    .restart local v2    # "bundle":Landroid/os/Bundle;
    invoke-virtual {v2, v5}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    .line 713
    .local v3, "procName":Ljava/lang/String;
    invoke-virtual {v2, v10}, Landroid/os/Bundle;->getIntArray(Ljava/lang/String;)[I

    move-result-object v5

    .line 714
    .local v5, "tids":[I
    invoke-virtual {v2, v9}, Landroid/os/Bundle;->getInt(Ljava/lang/String;)I

    move-result v9

    .line 715
    .local v9, "mode":I
    invoke-virtual {v2, v8}, Landroid/os/Bundle;->getLong(Ljava/lang/String;)J

    move-result-wide v17

    .line 716
    .restart local v17    # "duration":J
    new-instance v8, Ljava/lang/StringBuilder;

    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v8, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v9}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v6, v7, v4}, Landroid/os/Trace;->traceBegin(JLjava/lang/String;)V

    .line 718
    iget-object v11, v0, Lcom/miui/server/rtboost/SchedBoostService$Handler;->this$0:Lcom/miui/server/rtboost/SchedBoostService;

    move-object v12, v5

    move-wide/from16 v13, v17

    move-object v15, v3

    move/from16 v16, v9

    invoke-static/range {v11 .. v16}, Lcom/miui/server/rtboost/SchedBoostService;->-$$Nest$mhandleBeginSchedThread(Lcom/miui/server/rtboost/SchedBoostService;[IJLjava/lang/String;I)V

    .line 719
    invoke-static {v6, v7}, Landroid/os/Trace;->traceEnd(J)V

    .line 720
    goto :goto_2

    .line 706
    .end local v2    # "bundle":Landroid/os/Bundle;
    .end local v3    # "procName":Ljava/lang/String;
    .end local v5    # "tids":[I
    .end local v9    # "mode":I
    .end local v17    # "duration":J
    :pswitch_7
    iget-object v2, v1, Landroid/os/Message;->obj:Ljava/lang/Object;

    check-cast v2, Lcom/miui/server/rtboost/SchedBoostService$BoostThreadInfo;

    .line 707
    .local v2, "threadInfo":Lcom/miui/server/rtboost/SchedBoostService$BoostThreadInfo;
    iget-object v3, v0, Lcom/miui/server/rtboost/SchedBoostService$Handler;->this$0:Lcom/miui/server/rtboost/SchedBoostService;

    invoke-static {v3, v2}, Lcom/miui/server/rtboost/SchedBoostService;->-$$Nest$mhandleResetSchedThread(Lcom/miui/server/rtboost/SchedBoostService;Lcom/miui/server/rtboost/SchedBoostService$BoostThreadInfo;)V

    .line 708
    goto :goto_2

    .line 701
    .end local v2    # "threadInfo":Lcom/miui/server/rtboost/SchedBoostService$BoostThreadInfo;
    :pswitch_8
    iget-object v2, v1, Landroid/os/Message;->obj:Ljava/lang/Object;

    check-cast v2, Lcom/miui/server/rtboost/SchedBoostService$BoostThreadInfo;

    .line 702
    .restart local v2    # "threadInfo":Lcom/miui/server/rtboost/SchedBoostService$BoostThreadInfo;
    iget-object v3, v0, Lcom/miui/server/rtboost/SchedBoostService$Handler;->this$0:Lcom/miui/server/rtboost/SchedBoostService;

    invoke-static {v3, v2}, Lcom/miui/server/rtboost/SchedBoostService;->-$$Nest$mhandleSchedThread(Lcom/miui/server/rtboost/SchedBoostService;Lcom/miui/server/rtboost/SchedBoostService$BoostThreadInfo;)V

    .line 703
    nop

    .line 782
    .end local v2    # "threadInfo":Lcom/miui/server/rtboost/SchedBoostService$BoostThreadInfo;
    :cond_0
    :goto_2
    return-void

    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_8
        :pswitch_7
        :pswitch_6
        :pswitch_5
        :pswitch_4
        :pswitch_3
        :pswitch_2
        :pswitch_1
        :pswitch_0
    .end packed-switch
.end method

.method public obtainBeginSchedThreadsMsg([IJLjava/lang/String;I)Landroid/os/Message;
    .locals 3
    .param p1, "tids"    # [I
    .param p2, "duration"    # J
    .param p4, "procName"    # Ljava/lang/String;
    .param p5, "mode"    # I

    .line 798
    invoke-static {p0}, Ljava/util/Objects;->requireNonNull(Ljava/lang/Object;)Ljava/lang/Object;

    const/4 v0, 0x3

    invoke-virtual {p0, v0}, Lcom/miui/server/rtboost/SchedBoostService$Handler;->obtainMessage(I)Landroid/os/Message;

    move-result-object v0

    .line 799
    .local v0, "msg":Landroid/os/Message;
    new-instance v1, Landroid/os/Bundle;

    invoke-direct {v1}, Landroid/os/Bundle;-><init>()V

    .line 800
    .local v1, "data":Landroid/os/Bundle;
    const-string v2, "name"

    invoke-virtual {v1, v2, p4}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 801
    const-string/jumbo v2, "tids"

    invoke-virtual {v1, v2, p1}, Landroid/os/Bundle;->putIntArray(Ljava/lang/String;[I)V

    .line 802
    const-string v2, "mode"

    invoke-virtual {v1, v2, p5}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    .line 803
    const-string/jumbo v2, "timeout"

    invoke-virtual {v1, v2, p2, p3}, Landroid/os/Bundle;->putLong(Ljava/lang/String;J)V

    .line 804
    invoke-virtual {v0, v1}, Landroid/os/Message;->setData(Landroid/os/Bundle;)V

    .line 805
    return-object v0
.end method

.method public obtainBoostFreqMsg([IJLjava/lang/String;I)Landroid/os/Message;
    .locals 3
    .param p1, "tids"    # [I
    .param p2, "duration"    # J
    .param p4, "procName"    # Ljava/lang/String;
    .param p5, "mode"    # I

    .line 863
    invoke-static {p0}, Ljava/util/Objects;->requireNonNull(Ljava/lang/Object;)Ljava/lang/Object;

    const/16 v0, 0x9

    invoke-virtual {p0, v0}, Lcom/miui/server/rtboost/SchedBoostService$Handler;->obtainMessage(I)Landroid/os/Message;

    move-result-object v0

    .line 864
    .local v0, "msg":Landroid/os/Message;
    new-instance v1, Landroid/os/Bundle;

    invoke-direct {v1}, Landroid/os/Bundle;-><init>()V

    .line 865
    .local v1, "data":Landroid/os/Bundle;
    const-string v2, "name"

    invoke-virtual {v1, v2, p4}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 866
    const-string/jumbo v2, "tids"

    invoke-virtual {v1, v2, p1}, Landroid/os/Bundle;->putIntArray(Ljava/lang/String;[I)V

    .line 867
    const-string/jumbo v2, "timeout"

    invoke-virtual {v1, v2, p2, p3}, Landroid/os/Bundle;->putLong(Ljava/lang/String;J)V

    .line 868
    const-string v2, "mode"

    invoke-virtual {v1, v2, p5}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    .line 869
    invoke-virtual {v0, v1}, Landroid/os/Message;->setData(Landroid/os/Bundle;)V

    .line 870
    return-object v0
.end method

.method public obtainReSetThreadsFIFOMsg(IJI)Landroid/os/Message;
    .locals 3
    .param p1, "tid"    # I
    .param p2, "duration"    # J
    .param p4, "mode"    # I

    .line 817
    invoke-static {p0}, Ljava/util/Objects;->requireNonNull(Ljava/lang/Object;)Ljava/lang/Object;

    const/16 v0, 0x8

    invoke-virtual {p0, v0}, Lcom/miui/server/rtboost/SchedBoostService$Handler;->obtainMessage(I)Landroid/os/Message;

    move-result-object v0

    .line 818
    .local v0, "msg":Landroid/os/Message;
    new-instance v1, Landroid/os/Bundle;

    invoke-direct {v1}, Landroid/os/Bundle;-><init>()V

    .line 819
    .local v1, "data":Landroid/os/Bundle;
    const-string/jumbo v2, "tid"

    invoke-virtual {v1, v2, p1}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    .line 820
    const-string/jumbo v2, "timeout"

    invoke-virtual {v1, v2, p2, p3}, Landroid/os/Bundle;->putLong(Ljava/lang/String;J)V

    .line 821
    const-string v2, "mode"

    invoke-virtual {v1, v2, p4}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    .line 822
    invoke-virtual {v0, v1}, Landroid/os/Message;->setData(Landroid/os/Bundle;)V

    .line 823
    return-object v0
.end method

.method public obtainResetSchedThreadsMsg(Lcom/miui/server/rtboost/SchedBoostService$BoostThreadInfo;)Landroid/os/Message;
    .locals 1
    .param p1, "threadInfo"    # Lcom/miui/server/rtboost/SchedBoostService$BoostThreadInfo;

    .line 791
    invoke-static {p0}, Ljava/util/Objects;->requireNonNull(Ljava/lang/Object;)Ljava/lang/Object;

    const/4 v0, 0x2

    invoke-virtual {p0, v0, p1}, Lcom/miui/server/rtboost/SchedBoostService$Handler;->obtainMessage(ILjava/lang/Object;)Landroid/os/Message;

    move-result-object v0

    .line 792
    .local v0, "msg":Landroid/os/Message;
    iput-object p1, v0, Landroid/os/Message;->obj:Ljava/lang/Object;

    .line 793
    return-object v0
.end method

.method public obtainSchedThreadsMsg(Lcom/miui/server/rtboost/SchedBoostService$BoostThreadInfo;)Landroid/os/Message;
    .locals 1
    .param p1, "threadInfo"    # Lcom/miui/server/rtboost/SchedBoostService$BoostThreadInfo;

    .line 785
    invoke-static {p0}, Ljava/util/Objects;->requireNonNull(Ljava/lang/Object;)Ljava/lang/Object;

    const/4 v0, 0x1

    invoke-virtual {p0, v0, p1}, Lcom/miui/server/rtboost/SchedBoostService$Handler;->obtainMessage(ILjava/lang/Object;)Landroid/os/Message;

    move-result-object v0

    .line 786
    .local v0, "msg":Landroid/os/Message;
    iput-object p1, v0, Landroid/os/Message;->obj:Ljava/lang/Object;

    .line 787
    return-object v0
.end method

.method public obtainSetThreadsAlwaysFIFOMsg([I)Landroid/os/Message;
    .locals 3
    .param p1, "tids"    # [I

    .line 809
    invoke-static {p0}, Ljava/util/Objects;->requireNonNull(Ljava/lang/Object;)Ljava/lang/Object;

    const/4 v0, 0x4

    invoke-virtual {p0, v0}, Lcom/miui/server/rtboost/SchedBoostService$Handler;->obtainMessage(I)Landroid/os/Message;

    move-result-object v0

    .line 810
    .local v0, "msg":Landroid/os/Message;
    new-instance v1, Landroid/os/Bundle;

    invoke-direct {v1}, Landroid/os/Bundle;-><init>()V

    .line 811
    .local v1, "data":Landroid/os/Bundle;
    const-string/jumbo v2, "tids"

    invoke-virtual {v1, v2, p1}, Landroid/os/Bundle;->putIntArray(Ljava/lang/String;[I)V

    .line 812
    invoke-virtual {v0, v1}, Landroid/os/Message;->setData(Landroid/os/Bundle;)V

    .line 813
    return-object v0
.end method

.method public obtainSetThreadsFIFOMsg([IJI)Landroid/os/Message;
    .locals 3
    .param p1, "tids"    # [I
    .param p2, "duration"    # J
    .param p4, "mode"    # I

    .line 826
    invoke-static {p0}, Ljava/util/Objects;->requireNonNull(Ljava/lang/Object;)Ljava/lang/Object;

    const/4 v0, 0x7

    invoke-virtual {p0, v0}, Lcom/miui/server/rtboost/SchedBoostService$Handler;->obtainMessage(I)Landroid/os/Message;

    move-result-object v0

    .line 827
    .local v0, "msg":Landroid/os/Message;
    new-instance v1, Landroid/os/Bundle;

    invoke-direct {v1}, Landroid/os/Bundle;-><init>()V

    .line 828
    .local v1, "data":Landroid/os/Bundle;
    const-string/jumbo v2, "tids"

    invoke-virtual {v1, v2, p1}, Landroid/os/Bundle;->putIntArray(Ljava/lang/String;[I)V

    .line 829
    const-string/jumbo v2, "timeout"

    invoke-virtual {v1, v2, p2, p3}, Landroid/os/Bundle;->putLong(Ljava/lang/String;J)V

    .line 830
    const-string v2, "mode"

    invoke-virtual {v1, v2, p4}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    .line 831
    invoke-virtual {v0, v1}, Landroid/os/Message;->setData(Landroid/os/Bundle;)V

    .line 832
    return-object v0
.end method

.method public obtainThreadBindBigCoreMsg([IJ)Landroid/os/Message;
    .locals 3
    .param p1, "tids"    # [I
    .param p2, "duration"    # J

    .line 836
    invoke-static {p0}, Ljava/util/Objects;->requireNonNull(Ljava/lang/Object;)Ljava/lang/Object;

    const/4 v0, 0x5

    invoke-virtual {p0, v0}, Lcom/miui/server/rtboost/SchedBoostService$Handler;->obtainMessage(I)Landroid/os/Message;

    move-result-object v0

    .line 837
    .local v0, "msg":Landroid/os/Message;
    new-instance v1, Landroid/os/Bundle;

    invoke-direct {v1}, Landroid/os/Bundle;-><init>()V

    .line 838
    .local v1, "data":Landroid/os/Bundle;
    const-string/jumbo v2, "tids"

    invoke-virtual {v1, v2, p1}, Landroid/os/Bundle;->putIntArray(Ljava/lang/String;[I)V

    .line 839
    const-string v2, "duration"

    invoke-virtual {v1, v2, p2, p3}, Landroid/os/Bundle;->putLong(Ljava/lang/String;J)V

    .line 840
    invoke-virtual {v0, v1}, Landroid/os/Message;->setData(Landroid/os/Bundle;)V

    .line 841
    return-object v0
.end method

.method public obtainThreadUnBindBigCoreMsg(Ljava/lang/Integer;)Landroid/os/Message;
    .locals 1
    .param p1, "tid"    # Ljava/lang/Integer;

    .line 845
    invoke-static {p0}, Ljava/util/Objects;->requireNonNull(Ljava/lang/Object;)Ljava/lang/Object;

    const/4 v0, 0x6

    invoke-virtual {p0, v0}, Lcom/miui/server/rtboost/SchedBoostService$Handler;->obtainMessage(I)Landroid/os/Message;

    move-result-object v0

    .line 846
    .local v0, "msg":Landroid/os/Message;
    iput-object p1, v0, Landroid/os/Message;->obj:Ljava/lang/Object;

    .line 847
    return-object v0
.end method

.method public removeReSetThreadsFIFOMsg(Ljava/lang/Integer;)V
    .locals 1
    .param p1, "tid"    # Ljava/lang/Integer;

    .line 859
    invoke-static {p0}, Ljava/util/Objects;->requireNonNull(Ljava/lang/Object;)Ljava/lang/Object;

    const/16 v0, 0x8

    invoke-virtual {p0, v0, p1}, Lcom/miui/server/rtboost/SchedBoostService$Handler;->removeEqualMessages(ILjava/lang/Object;)V

    .line 860
    return-void
.end method

.method public removeResetThreadsMsg(Lcom/miui/server/rtboost/SchedBoostService$BoostThreadInfo;)V
    .locals 1
    .param p1, "threadInfo"    # Lcom/miui/server/rtboost/SchedBoostService$BoostThreadInfo;

    .line 851
    invoke-static {p0}, Ljava/util/Objects;->requireNonNull(Ljava/lang/Object;)Ljava/lang/Object;

    const/4 v0, 0x2

    invoke-virtual {p0, v0, p1}, Lcom/miui/server/rtboost/SchedBoostService$Handler;->removeEqualMessages(ILjava/lang/Object;)V

    .line 852
    return-void
.end method

.method public removeUnBindBigCoreMsg(Ljava/lang/Integer;)V
    .locals 1
    .param p1, "tid"    # Ljava/lang/Integer;

    .line 855
    invoke-static {p0}, Ljava/util/Objects;->requireNonNull(Ljava/lang/Object;)Ljava/lang/Object;

    const/4 v0, 0x6

    invoke-virtual {p0, v0, p1}, Lcom/miui/server/rtboost/SchedBoostService$Handler;->removeEqualMessages(ILjava/lang/Object;)V

    .line 856
    return-void
.end method
