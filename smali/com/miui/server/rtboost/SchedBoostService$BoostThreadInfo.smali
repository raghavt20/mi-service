.class Lcom/miui/server/rtboost/SchedBoostService$BoostThreadInfo;
.super Ljava/lang/Object;
.source "SchedBoostService.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/miui/server/rtboost/SchedBoostService;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "BoostThreadInfo"
.end annotation


# static fields
.field private static final DEFAULT_SAVED_PRIO:I

.field private static final DEFAULT_START_TIME:I


# instance fields
.field boostPriority:I

.field boostStartTime:J

.field duration:J

.field mode:I

.field procName:Ljava/lang/String;

.field savedPriority:I

.field final synthetic this$0:Lcom/miui/server/rtboost/SchedBoostService;

.field tid:I


# direct methods
.method public constructor <init>(Lcom/miui/server/rtboost/SchedBoostService;IJLjava/lang/String;I)V
    .locals 0
    .param p2, "tid"    # I
    .param p3, "duration"    # J
    .param p5, "procName"    # Ljava/lang/String;
    .param p6, "mode"    # I

    .line 950
    iput-object p1, p0, Lcom/miui/server/rtboost/SchedBoostService$BoostThreadInfo;->this$0:Lcom/miui/server/rtboost/SchedBoostService;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 951
    iput p2, p0, Lcom/miui/server/rtboost/SchedBoostService$BoostThreadInfo;->tid:I

    .line 952
    iput-wide p3, p0, Lcom/miui/server/rtboost/SchedBoostService$BoostThreadInfo;->duration:J

    .line 953
    iput-object p5, p0, Lcom/miui/server/rtboost/SchedBoostService$BoostThreadInfo;->procName:Ljava/lang/String;

    .line 954
    iput p6, p0, Lcom/miui/server/rtboost/SchedBoostService$BoostThreadInfo;->mode:I

    .line 955
    return-void
.end method


# virtual methods
.method public dump(Ljava/io/PrintWriter;[Ljava/lang/String;)V
    .locals 3
    .param p1, "pw"    # Ljava/io/PrintWriter;
    .param p2, "args"    # [Ljava/lang/String;

    .line 970
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "#"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lcom/miui/server/rtboost/SchedBoostService$BoostThreadInfo;->procName:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, " tid:"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget v1, p0, Lcom/miui/server/rtboost/SchedBoostService$BoostThreadInfo;->tid:I

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, " duration: "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-wide v1, p0, Lcom/miui/server/rtboost/SchedBoostService$BoostThreadInfo;->duration:J

    invoke-virtual {v0, v1, v2}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, " boostStartTime: "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-wide v1, p0, Lcom/miui/server/rtboost/SchedBoostService$BoostThreadInfo;->boostStartTime:J

    invoke-virtual {v0, v1, v2}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, " mode: "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget v1, p0, Lcom/miui/server/rtboost/SchedBoostService$BoostThreadInfo;->mode:I

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, " savedPriority: "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget v1, p0, Lcom/miui/server/rtboost/SchedBoostService$BoostThreadInfo;->savedPriority:I

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, " boostPriority: "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget v1, p0, Lcom/miui/server/rtboost/SchedBoostService$BoostThreadInfo;->boostPriority:I

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v0}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V

    .line 977
    return-void
.end method

.method public equals(Ljava/lang/Object;)Z
    .locals 5
    .param p1, "obj"    # Ljava/lang/Object;

    .line 959
    const/4 v0, 0x1

    if-ne p0, p1, :cond_0

    return v0

    .line 961
    :cond_0
    instance-of v1, p1, Lcom/miui/server/rtboost/SchedBoostService$BoostThreadInfo;

    const/4 v2, 0x0

    if-eqz v1, :cond_2

    .line 962
    move-object v1, p1

    check-cast v1, Lcom/miui/server/rtboost/SchedBoostService$BoostThreadInfo;

    .line 963
    .local v1, "threadInfo":Lcom/miui/server/rtboost/SchedBoostService$BoostThreadInfo;
    iget v3, v1, Lcom/miui/server/rtboost/SchedBoostService$BoostThreadInfo;->tid:I

    iget v4, p0, Lcom/miui/server/rtboost/SchedBoostService$BoostThreadInfo;->tid:I

    if-ne v3, v4, :cond_1

    goto :goto_0

    :cond_1
    move v0, v2

    :goto_0
    return v0

    .line 966
    .end local v1    # "threadInfo":Lcom/miui/server/rtboost/SchedBoostService$BoostThreadInfo;
    :cond_2
    return v2
.end method
