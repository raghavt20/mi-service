class inal implements com.miui.server.rtboost.SchedBoostManagerInternal {
	 /* .source "SchedBoostService.java" */
	 /* # interfaces */
	 /* # annotations */
	 /* .annotation system Ldalvik/annotation/EnclosingClass; */
	 /* value = Lcom/miui/server/rtboost/SchedBoostService; */
} // .end annotation
/* .annotation system Ldalvik/annotation/InnerClass; */
/* accessFlags = 0x12 */
/* name = "LocalService" */
} // .end annotation
/* # instance fields */
final com.miui.server.rtboost.SchedBoostService this$0; //synthetic
/* # direct methods */
private inal ( ) {
/* .locals 0 */
/* .line 894 */
this.this$0 = p1;
/* invoke-direct {p0}, Ljava/lang/Object;-><init>()V */
return;
} // .end method
 inal ( ) { //synthethic
/* .locals 0 */
/* invoke-direct {p0, p1}, Lcom/miui/server/rtboost/SchedBoostService$LocalService;-><init>(Lcom/miui/server/rtboost/SchedBoostService;)V */
return;
} // .end method
/* # virtual methods */
public void beginSchedThreads ( Integer[] p0, Long p1, java.lang.String p2, Integer p3 ) {
/* .locals 6 */
/* .param p1, "tids" # [I */
/* .param p2, "duration" # J */
/* .param p4, "procName" # Ljava/lang/String; */
/* .param p5, "mode" # I */
/* .line 908 */
v0 = this.this$0;
/* move-object v1, p1 */
/* move-wide v2, p2 */
/* move-object v4, p4 */
/* move v5, p5 */
/* invoke-virtual/range {v0 ..v5}, Lcom/miui/server/rtboost/SchedBoostService;->beginSchedThreads([IJLjava/lang/String;I)V */
/* .line 909 */
return;
} // .end method
public void boostHomeAnim ( Long p0, Integer p1 ) {
/* .locals 1 */
/* .param p1, "duration" # J */
/* .param p3, "mode" # I */
/* .line 933 */
v0 = this.this$0;
(( com.miui.server.rtboost.SchedBoostService ) v0 ).boostHomeAnim ( p1, p2, p3 ); // invoke-virtual {v0, p1, p2, p3}, Lcom/miui/server/rtboost/SchedBoostService;->boostHomeAnim(JI)V
/* .line 934 */
return;
} // .end method
public Boolean checkThreadBoost ( Integer p0 ) {
/* .locals 1 */
/* .param p1, "tid" # I */
/* .line 918 */
v0 = this.this$0;
v0 = (( com.miui.server.rtboost.SchedBoostService ) v0 ).checkThreadBoost ( p1 ); // invoke-virtual {v0, p1}, Lcom/miui/server/rtboost/SchedBoostService;->checkThreadBoost(I)Z
} // .end method
public void enableSchedBoost ( Boolean p0 ) {
/* .locals 1 */
/* .param p1, "enable" # Z */
/* .line 903 */
v0 = this.this$0;
(( com.miui.server.rtboost.SchedBoostService ) v0 ).enableSchedBoost ( p1 ); // invoke-virtual {v0, p1}, Lcom/miui/server/rtboost/SchedBoostService;->enableSchedBoost(Z)V
/* .line 904 */
return;
} // .end method
public void schedProcessBoost ( com.android.server.wm.WindowProcessListener p0, java.lang.String p1, Integer p2, Integer p3, Integer p4, Long p5 ) {
/* .locals 8 */
/* .param p1, "proc" # Lcom/android/server/wm/WindowProcessListener; */
/* .param p2, "procName" # Ljava/lang/String; */
/* .param p3, "pid" # I */
/* .param p4, "rtid" # I */
/* .param p5, "schedMode" # I */
/* .param p6, "timeout" # J */
/* .line 898 */
v0 = this.this$0;
/* move-object v1, p1 */
/* move-object v2, p2 */
/* move v3, p3 */
/* move v4, p4 */
/* move v5, p5 */
/* move-wide v6, p6 */
/* invoke-virtual/range {v0 ..v7}, Lcom/miui/server/rtboost/SchedBoostService;->schedProcessBoost(Lcom/android/server/wm/WindowProcessListener;Ljava/lang/String;IIIJ)V */
/* .line 899 */
return;
} // .end method
public void setRenderThreadTid ( com.android.server.wm.WindowProcessController p0 ) {
/* .locals 1 */
/* .param p1, "wpc" # Lcom/android/server/wm/WindowProcessController; */
/* .line 913 */
v0 = this.this$0;
(( com.miui.server.rtboost.SchedBoostService ) v0 ).setRenderThreadTid ( p1 ); // invoke-virtual {v0, p1}, Lcom/miui/server/rtboost/SchedBoostService;->setRenderThreadTid(Lcom/android/server/wm/WindowProcessController;)V
/* .line 914 */
return;
} // .end method
public void setThreadSavedPriority ( Integer[] p0, Integer p1 ) {
/* .locals 1 */
/* .param p1, "tid" # [I */
/* .param p2, "prio" # I */
/* .line 923 */
v0 = this.this$0;
(( com.miui.server.rtboost.SchedBoostService ) v0 ).setThreadSavedPriority ( p1, p2 ); // invoke-virtual {v0, p1, p2}, Lcom/miui/server/rtboost/SchedBoostService;->setThreadSavedPriority([II)V
/* .line 924 */
return;
} // .end method
public void stopCurrentSchedBoost ( ) {
/* .locals 1 */
/* .line 928 */
v0 = this.this$0;
(( com.miui.server.rtboost.SchedBoostService ) v0 ).stopCurrentSchedBoost ( ); // invoke-virtual {v0}, Lcom/miui/server/rtboost/SchedBoostService;->stopCurrentSchedBoost()V
/* .line 929 */
return;
} // .end method
