class com.miui.server.rtboost.SchedBoostService$BoostThreadInfo {
	 /* .source "SchedBoostService.java" */
	 /* # annotations */
	 /* .annotation system Ldalvik/annotation/EnclosingClass; */
	 /* value = Lcom/miui/server/rtboost/SchedBoostService; */
} // .end annotation
/* .annotation system Ldalvik/annotation/InnerClass; */
/* accessFlags = 0x2 */
/* name = "BoostThreadInfo" */
} // .end annotation
/* # static fields */
private static final Integer DEFAULT_SAVED_PRIO;
private static final Integer DEFAULT_START_TIME;
/* # instance fields */
Integer boostPriority;
Long boostStartTime;
Long duration;
Integer mode;
java.lang.String procName;
Integer savedPriority;
final com.miui.server.rtboost.SchedBoostService this$0; //synthetic
Integer tid;
/* # direct methods */
public com.miui.server.rtboost.SchedBoostService$BoostThreadInfo ( ) {
/* .locals 0 */
/* .param p2, "tid" # I */
/* .param p3, "duration" # J */
/* .param p5, "procName" # Ljava/lang/String; */
/* .param p6, "mode" # I */
/* .line 950 */
this.this$0 = p1;
/* invoke-direct {p0}, Ljava/lang/Object;-><init>()V */
/* .line 951 */
/* iput p2, p0, Lcom/miui/server/rtboost/SchedBoostService$BoostThreadInfo;->tid:I */
/* .line 952 */
/* iput-wide p3, p0, Lcom/miui/server/rtboost/SchedBoostService$BoostThreadInfo;->duration:J */
/* .line 953 */
this.procName = p5;
/* .line 954 */
/* iput p6, p0, Lcom/miui/server/rtboost/SchedBoostService$BoostThreadInfo;->mode:I */
/* .line 955 */
return;
} // .end method
/* # virtual methods */
public void dump ( java.io.PrintWriter p0, java.lang.String[] p1 ) {
/* .locals 3 */
/* .param p1, "pw" # Ljava/io/PrintWriter; */
/* .param p2, "args" # [Ljava/lang/String; */
/* .line 970 */
/* new-instance v0, Ljava/lang/StringBuilder; */
/* invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V */
final String v1 = "#"; // const-string v1, "#"
(( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
v1 = this.procName;
(( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
final String v1 = " tid:"; // const-string v1, " tid:"
(( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
/* iget v1, p0, Lcom/miui/server/rtboost/SchedBoostService$BoostThreadInfo;->tid:I */
(( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;
final String v1 = " duration: "; // const-string v1, " duration: "
(( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
/* iget-wide v1, p0, Lcom/miui/server/rtboost/SchedBoostService$BoostThreadInfo;->duration:J */
(( java.lang.StringBuilder ) v0 ).append ( v1, v2 ); // invoke-virtual {v0, v1, v2}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;
final String v1 = " boostStartTime: "; // const-string v1, " boostStartTime: "
(( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
/* iget-wide v1, p0, Lcom/miui/server/rtboost/SchedBoostService$BoostThreadInfo;->boostStartTime:J */
(( java.lang.StringBuilder ) v0 ).append ( v1, v2 ); // invoke-virtual {v0, v1, v2}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;
final String v1 = " mode: "; // const-string v1, " mode: "
(( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
/* iget v1, p0, Lcom/miui/server/rtboost/SchedBoostService$BoostThreadInfo;->mode:I */
(( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;
final String v1 = " savedPriority: "; // const-string v1, " savedPriority: "
(( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
/* iget v1, p0, Lcom/miui/server/rtboost/SchedBoostService$BoostThreadInfo;->savedPriority:I */
(( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;
final String v1 = " boostPriority: "; // const-string v1, " boostPriority: "
(( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
/* iget v1, p0, Lcom/miui/server/rtboost/SchedBoostService$BoostThreadInfo;->boostPriority:I */
(( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v0 ).toString ( ); // invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
(( java.io.PrintWriter ) p1 ).println ( v0 ); // invoke-virtual {p1, v0}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V
/* .line 977 */
return;
} // .end method
public Boolean equals ( java.lang.Object p0 ) {
/* .locals 5 */
/* .param p1, "obj" # Ljava/lang/Object; */
/* .line 959 */
int v0 = 1; // const/4 v0, 0x1
/* if-ne p0, p1, :cond_0 */
/* .line 961 */
} // :cond_0
/* instance-of v1, p1, Lcom/miui/server/rtboost/SchedBoostService$BoostThreadInfo; */
int v2 = 0; // const/4 v2, 0x0
if ( v1 != null) { // if-eqz v1, :cond_2
/* .line 962 */
/* move-object v1, p1 */
/* check-cast v1, Lcom/miui/server/rtboost/SchedBoostService$BoostThreadInfo; */
/* .line 963 */
/* .local v1, "threadInfo":Lcom/miui/server/rtboost/SchedBoostService$BoostThreadInfo; */
/* iget v3, v1, Lcom/miui/server/rtboost/SchedBoostService$BoostThreadInfo;->tid:I */
/* iget v4, p0, Lcom/miui/server/rtboost/SchedBoostService$BoostThreadInfo;->tid:I */
/* if-ne v3, v4, :cond_1 */
} // :cond_1
/* move v0, v2 */
} // :goto_0
/* .line 966 */
} // .end local v1 # "threadInfo":Lcom/miui/server/rtboost/SchedBoostService$BoostThreadInfo;
} // :cond_2
} // .end method
