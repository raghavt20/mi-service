.class Lcom/miui/server/rtboost/SchedBoostService$SchedBoostShellCommand;
.super Landroid/os/ShellCommand;
.source "SchedBoostService.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/miui/server/rtboost/SchedBoostService;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x8
    name = "SchedBoostShellCommand"
.end annotation


# instance fields
.field mService:Lcom/miui/server/rtboost/SchedBoostService;


# direct methods
.method constructor <init>(Lcom/miui/server/rtboost/SchedBoostService;)V
    .locals 0
    .param p1, "service"    # Lcom/miui/server/rtboost/SchedBoostService;

    .line 1321
    invoke-direct {p0}, Landroid/os/ShellCommand;-><init>()V

    .line 1322
    iput-object p1, p0, Lcom/miui/server/rtboost/SchedBoostService$SchedBoostShellCommand;->mService:Lcom/miui/server/rtboost/SchedBoostService;

    .line 1323
    return-void
.end method


# virtual methods
.method public onCommand(Ljava/lang/String;)I
    .locals 4
    .param p1, "cmd"    # Ljava/lang/String;

    .line 1327
    invoke-virtual {p0}, Lcom/miui/server/rtboost/SchedBoostService$SchedBoostShellCommand;->getOutFileDescriptor()Ljava/io/FileDescriptor;

    move-result-object v0

    .line 1328
    .local v0, "fd":Ljava/io/FileDescriptor;
    invoke-virtual {p0}, Lcom/miui/server/rtboost/SchedBoostService$SchedBoostShellCommand;->getOutPrintWriter()Ljava/io/PrintWriter;

    move-result-object v1

    .line 1329
    .local v1, "pw":Ljava/io/PrintWriter;
    invoke-virtual {p0}, Lcom/miui/server/rtboost/SchedBoostService$SchedBoostShellCommand;->getAllArgs()[Ljava/lang/String;

    move-result-object v2

    .line 1330
    .local v2, "args":[Ljava/lang/String;
    if-nez p1, :cond_0

    .line 1331
    invoke-virtual {p0, p1}, Lcom/miui/server/rtboost/SchedBoostService$SchedBoostShellCommand;->handleDefaultCommands(Ljava/lang/String;)I

    move-result v3

    return v3

    .line 1334
    :cond_0
    :try_start_0
    iget-object v3, p0, Lcom/miui/server/rtboost/SchedBoostService$SchedBoostShellCommand;->mService:Lcom/miui/server/rtboost/SchedBoostService;

    invoke-virtual {v3, v0, v1, v2}, Lcom/miui/server/rtboost/SchedBoostService;->dump(Ljava/io/FileDescriptor;Ljava/io/PrintWriter;[Ljava/lang/String;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 1337
    goto :goto_0

    .line 1335
    :catch_0
    move-exception v3

    .line 1336
    .local v3, "e":Ljava/lang/Exception;
    invoke-virtual {v1, v3}, Ljava/io/PrintWriter;->println(Ljava/lang/Object;)V

    .line 1338
    .end local v3    # "e":Ljava/lang/Exception;
    :goto_0
    const/4 v3, -0x1

    return v3
.end method

.method public onHelp()V
    .locals 2

    .line 1343
    invoke-virtual {p0}, Lcom/miui/server/rtboost/SchedBoostService$SchedBoostShellCommand;->getOutPrintWriter()Ljava/io/PrintWriter;

    move-result-object v0

    .line 1344
    .local v0, "pw":Ljava/io/PrintWriter;
    const-string v1, "sched boost (SchedBoostService) commands:"

    invoke-virtual {v0, v1}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V

    .line 1345
    invoke-virtual {v0}, Ljava/io/PrintWriter;->println()V

    .line 1346
    return-void
.end method
