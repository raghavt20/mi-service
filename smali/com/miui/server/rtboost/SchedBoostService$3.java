class com.miui.server.rtboost.SchedBoostService$3 implements com.android.server.wm.SchedBoostGesturesEvent$GesturesEventListener {
	 /* .source "SchedBoostService.java" */
	 /* # interfaces */
	 /* # annotations */
	 /* .annotation system Ldalvik/annotation/EnclosingMethod; */
	 /* value = Lcom/miui/server/rtboost/SchedBoostService;-><init>(Landroid/content/Context;)V */
} // .end annotation
/* .annotation system Ldalvik/annotation/InnerClass; */
/* accessFlags = 0x0 */
/* name = null */
} // .end annotation
/* # instance fields */
final com.miui.server.rtboost.SchedBoostService this$0; //synthetic
/* # direct methods */
 com.miui.server.rtboost.SchedBoostService$3 ( ) {
/* .locals 0 */
/* .param p1, "this$0" # Lcom/miui/server/rtboost/SchedBoostService; */
/* .line 237 */
this.this$0 = p1;
/* invoke-direct {p0}, Ljava/lang/Object;-><init>()V */
return;
} // .end method
/* # virtual methods */
public void onDown ( ) {
/* .locals 3 */
/* .line 252 */
final String v0 = "onDown"; // const-string v0, "onDown"
/* const-wide/16 v1, 0x4 */
android.os.Trace .traceBegin ( v1,v2,v0 );
/* .line 253 */
com.android.server.wm.RealTimeModeControllerImpl .get ( );
(( com.android.server.wm.RealTimeModeControllerImpl ) v0 ).onDown ( ); // invoke-virtual {v0}, Lcom/android/server/wm/RealTimeModeControllerImpl;->onDown()V
/* .line 254 */
android.os.Trace .traceEnd ( v1,v2 );
/* .line 255 */
return;
} // .end method
public void onFling ( Float p0, Float p1, Integer p2 ) {
/* .locals 1 */
/* .param p1, "velocityX" # F */
/* .param p2, "velocityY" # F */
/* .param p3, "durationMs" # I */
/* .line 240 */
com.android.server.wm.RealTimeModeControllerImpl .get ( );
(( com.android.server.wm.RealTimeModeControllerImpl ) v0 ).onFling ( p3 ); // invoke-virtual {v0, p3}, Lcom/android/server/wm/RealTimeModeControllerImpl;->onFling(I)V
/* .line 241 */
return;
} // .end method
public void onMove ( ) {
/* .locals 3 */
/* .line 259 */
final String v0 = "onMove"; // const-string v0, "onMove"
/* const-wide/16 v1, 0x4 */
android.os.Trace .traceBegin ( v1,v2,v0 );
/* .line 260 */
v0 = this.this$0;
com.miui.server.rtboost.SchedBoostService .-$$Nest$fgetmBoostingMap ( v0 );
v0 = (( com.miui.server.rtboost.SchedBoostService$BoostingPackageMap ) v0 ).isEmpty ( ); // invoke-virtual {v0}, Lcom/miui/server/rtboost/SchedBoostService$BoostingPackageMap;->isEmpty()Z
if ( v0 != null) { // if-eqz v0, :cond_0
	 /* .line 261 */
	 com.android.server.wm.RealTimeModeControllerImpl .get ( );
	 (( com.android.server.wm.RealTimeModeControllerImpl ) v0 ).onMove ( ); // invoke-virtual {v0}, Lcom/android/server/wm/RealTimeModeControllerImpl;->onMove()V
	 /* .line 263 */
} // :cond_0
android.os.Trace .traceEnd ( v1,v2 );
/* .line 264 */
return;
} // .end method
public void onScroll ( Boolean p0 ) {
/* .locals 3 */
/* .param p1, "started" # Z */
/* .line 245 */
final String v0 = "onScroll"; // const-string v0, "onScroll"
/* const-wide/16 v1, 0x4 */
android.os.Trace .traceBegin ( v1,v2,v0 );
/* .line 246 */
com.android.server.wm.RealTimeModeControllerImpl .get ( );
(( com.android.server.wm.RealTimeModeControllerImpl ) v0 ).onScroll ( p1 ); // invoke-virtual {v0, p1}, Lcom/android/server/wm/RealTimeModeControllerImpl;->onScroll(Z)V
/* .line 247 */
android.os.Trace .traceEnd ( v1,v2 );
/* .line 248 */
return;
} // .end method
