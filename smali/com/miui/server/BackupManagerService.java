public class com.miui.server.BackupManagerService extends miui.app.backup.IBackupManager$Stub {
	 /* .source "BackupManagerService.java" */
	 /* # annotations */
	 /* .annotation system Ldalvik/annotation/MemberClasses; */
	 /* value = { */
	 /* Lcom/miui/server/BackupManagerService$DeathLinker;, */
	 /* Lcom/miui/server/BackupManagerService$BackupHandler;, */
	 /* Lcom/miui/server/BackupManagerService$FullBackupRestoreObserver;, */
	 /* Lcom/miui/server/BackupManagerService$Lifecycle; */
	 /* } */
} // .end annotation
/* # static fields */
private static final Integer COMPONENT_ENABLED_STATE_NONE;
public static final Integer FD_CLOSE;
public static final Integer FD_NONE;
private static final Integer PID_NONE;
private static final java.lang.String TAG;
/* # instance fields */
private android.app.ActivityManager mActivityManager;
private Integer mAppUserId;
private miui.app.backup.IPackageBackupRestoreObserver mBackupRestoreObserver;
private volatile Integer mCallerFd;
private android.content.Context mContext;
private Long mCurrentCompletedSize;
private Long mCurrentTotalSize;
private Integer mCurrentWorkingFeature;
private java.lang.String mCurrentWorkingPkg;
private com.miui.server.BackupManagerService$DeathLinker mDeathLinker;
private java.lang.String mEncryptedPwd;
private java.lang.String mEncryptedPwdInBakFile;
private android.os.Handler mHandler;
private android.os.HandlerThread mHandlerThread;
private android.os.IBinder mICaller;
private Boolean mIsCanceling;
private Integer mLastError;
private java.util.HashMap mNeedBeKilledPkgs;
/* .annotation system Ldalvik/annotation/Signature; */
/* value = { */
/* "Ljava/util/HashMap<", */
/* "Ljava/lang/String;", */
/* "Ljava/lang/Boolean;", */
/* ">;" */
/* } */
} // .end annotation
} // .end field
private android.os.ParcelFileDescriptor mOutputFile;
private Integer mOwnerPid;
private Integer mPackageLastEnableState;
private android.content.pm.PackageManager mPackageManager;
private android.content.pm.IPackageManager mPackageManagerBinder;
com.android.internal.content.PackageMonitor mPackageMonitor;
private android.content.pm.IPackageStatsObserver mPackageStatsObserver;
private final java.util.concurrent.atomic.AtomicBoolean mPkgChangingLock;
private java.lang.String mPreviousWorkingPkg;
private Integer mProgType;
private java.lang.String mPwd;
private Boolean mShouldSkipData;
private Integer mState;
private android.os.RemoteCallbackList mStateObservers;
/* .annotation system Ldalvik/annotation/Signature; */
/* value = { */
/* "Landroid/os/RemoteCallbackList<", */
/* "Lmiui/app/backup/IBackupServiceStateObserver;", */
/* ">;" */
/* } */
} // .end annotation
} // .end field
private java.util.concurrent.atomic.AtomicBoolean mTaskLatch;
/* # direct methods */
static Integer -$$Nest$fgetmAppUserId ( com.miui.server.BackupManagerService p0 ) { //bridge//synthethic
/* .locals 0 */
/* iget p0, p0, Lcom/miui/server/BackupManagerService;->mAppUserId:I */
} // .end method
static miui.app.backup.IPackageBackupRestoreObserver -$$Nest$fgetmBackupRestoreObserver ( com.miui.server.BackupManagerService p0 ) { //bridge//synthethic
/* .locals 0 */
p0 = this.mBackupRestoreObserver;
} // .end method
static android.content.Context -$$Nest$fgetmContext ( com.miui.server.BackupManagerService p0 ) { //bridge//synthethic
/* .locals 0 */
p0 = this.mContext;
} // .end method
static Integer -$$Nest$fgetmCurrentWorkingFeature ( com.miui.server.BackupManagerService p0 ) { //bridge//synthethic
/* .locals 0 */
/* iget p0, p0, Lcom/miui/server/BackupManagerService;->mCurrentWorkingFeature:I */
} // .end method
static java.lang.String -$$Nest$fgetmCurrentWorkingPkg ( com.miui.server.BackupManagerService p0 ) { //bridge//synthethic
/* .locals 0 */
p0 = this.mCurrentWorkingPkg;
} // .end method
static com.miui.server.BackupManagerService$DeathLinker -$$Nest$fgetmDeathLinker ( com.miui.server.BackupManagerService p0 ) { //bridge//synthethic
/* .locals 0 */
p0 = this.mDeathLinker;
} // .end method
static android.os.IBinder -$$Nest$fgetmICaller ( com.miui.server.BackupManagerService p0 ) { //bridge//synthethic
/* .locals 0 */
p0 = this.mICaller;
} // .end method
static android.content.pm.PackageManager -$$Nest$fgetmPackageManager ( com.miui.server.BackupManagerService p0 ) { //bridge//synthethic
/* .locals 0 */
p0 = this.mPackageManager;
} // .end method
static java.util.concurrent.atomic.AtomicBoolean -$$Nest$fgetmPkgChangingLock ( com.miui.server.BackupManagerService p0 ) { //bridge//synthethic
/* .locals 0 */
p0 = this.mPkgChangingLock;
} // .end method
static java.lang.String -$$Nest$fgetmPwd ( com.miui.server.BackupManagerService p0 ) { //bridge//synthethic
/* .locals 0 */
p0 = this.mPwd;
} // .end method
static void -$$Nest$fputmCurrentTotalSize ( com.miui.server.BackupManagerService p0, Long p1 ) { //bridge//synthethic
/* .locals 0 */
/* iput-wide p1, p0, Lcom/miui/server/BackupManagerService;->mCurrentTotalSize:J */
return;
} // .end method
static void -$$Nest$fputmICaller ( com.miui.server.BackupManagerService p0, android.os.IBinder p1 ) { //bridge//synthethic
/* .locals 0 */
this.mICaller = p1;
return;
} // .end method
static void -$$Nest$fputmIsCanceling ( com.miui.server.BackupManagerService p0, Boolean p1 ) { //bridge//synthethic
/* .locals 0 */
/* iput-boolean p1, p0, Lcom/miui/server/BackupManagerService;->mIsCanceling:Z */
return;
} // .end method
static void -$$Nest$fputmOwnerPid ( com.miui.server.BackupManagerService p0, Integer p1 ) { //bridge//synthethic
/* .locals 0 */
/* iput p1, p0, Lcom/miui/server/BackupManagerService;->mOwnerPid:I */
return;
} // .end method
static void -$$Nest$fputmPreviousWorkingPkg ( com.miui.server.BackupManagerService p0, java.lang.String p1 ) { //bridge//synthethic
/* .locals 0 */
this.mPreviousWorkingPkg = p1;
return;
} // .end method
static void -$$Nest$mbroadcastServiceIdle ( com.miui.server.BackupManagerService p0 ) { //bridge//synthethic
/* .locals 0 */
/* invoke-direct {p0}, Lcom/miui/server/BackupManagerService;->broadcastServiceIdle()V */
return;
} // .end method
static void -$$Nest$mrestoreLastPackageEnableState ( com.miui.server.BackupManagerService p0, java.io.File p1 ) { //bridge//synthethic
/* .locals 0 */
/* invoke-direct {p0, p1}, Lcom/miui/server/BackupManagerService;->restoreLastPackageEnableState(Ljava/io/File;)V */
return;
} // .end method
static void -$$Nest$mscheduleReleaseResource ( com.miui.server.BackupManagerService p0 ) { //bridge//synthethic
/* .locals 0 */
/* invoke-direct {p0}, Lcom/miui/server/BackupManagerService;->scheduleReleaseResource()V */
return;
} // .end method
static void -$$Nest$mwaitForTheLastWorkingTask ( com.miui.server.BackupManagerService p0 ) { //bridge//synthethic
/* .locals 0 */
/* invoke-direct {p0}, Lcom/miui/server/BackupManagerService;->waitForTheLastWorkingTask()V */
return;
} // .end method
static java.io.File -$$Nest$smgetPackageEnableStateFile ( ) { //bridge//synthethic
/* .locals 1 */
com.miui.server.BackupManagerService .getPackageEnableStateFile ( );
} // .end method
public com.miui.server.BackupManagerService ( ) {
/* .locals 4 */
/* .param p1, "context" # Landroid/content/Context; */
/* .line 172 */
/* invoke-direct {p0}, Lmiui/app/backup/IBackupManager$Stub;-><init>()V */
/* .line 68 */
/* new-instance v0, Ljava/util/HashMap; */
/* invoke-direct {v0}, Ljava/util/HashMap;-><init>()V */
this.mNeedBeKilledPkgs = v0;
/* .line 88 */
/* new-instance v0, Landroid/os/RemoteCallbackList; */
/* invoke-direct {v0}, Landroid/os/RemoteCallbackList;-><init>()V */
this.mStateObservers = v0;
/* .line 89 */
int v0 = -1; // const/4 v0, -0x1
/* iput v0, p0, Lcom/miui/server/BackupManagerService;->mOwnerPid:I */
/* .line 90 */
int v1 = 0; // const/4 v1, 0x0
this.mICaller = v1;
/* .line 91 */
/* new-instance v2, Lcom/miui/server/BackupManagerService$DeathLinker; */
/* invoke-direct {v2, p0, v1}, Lcom/miui/server/BackupManagerService$DeathLinker;-><init>(Lcom/miui/server/BackupManagerService;Lcom/miui/server/BackupManagerService$DeathLinker-IA;)V */
this.mDeathLinker = v2;
/* .line 92 */
this.mTaskLatch = v1;
/* .line 98 */
/* iput v0, p0, Lcom/miui/server/BackupManagerService;->mCallerFd:I */
/* .line 99 */
int v0 = 0; // const/4 v0, 0x0
/* iput-boolean v0, p0, Lcom/miui/server/BackupManagerService;->mIsCanceling:Z */
/* .line 100 */
this.mOutputFile = v1;
/* .line 101 */
/* iput v0, p0, Lcom/miui/server/BackupManagerService;->mState:I */
/* .line 105 */
/* iput v0, p0, Lcom/miui/server/BackupManagerService;->mAppUserId:I */
/* .line 111 */
/* new-instance v2, Ljava/util/concurrent/atomic/AtomicBoolean; */
/* invoke-direct {v2, v0}, Ljava/util/concurrent/atomic/AtomicBoolean;-><init>(Z)V */
this.mPkgChangingLock = v2;
/* .line 113 */
/* new-instance v0, Lcom/miui/server/BackupManagerService$1; */
/* invoke-direct {v0, p0}, Lcom/miui/server/BackupManagerService$1;-><init>(Lcom/miui/server/BackupManagerService;)V */
this.mPackageMonitor = v0;
/* .line 908 */
/* new-instance v0, Lcom/miui/server/BackupManagerService$4; */
/* invoke-direct {v0, p0}, Lcom/miui/server/BackupManagerService$4;-><init>(Lcom/miui/server/BackupManagerService;)V */
this.mPackageStatsObserver = v0;
/* .line 173 */
this.mContext = p1;
/* .line 174 */
(( android.content.Context ) p1 ).getPackageManager ( ); // invoke-virtual {p1}, Landroid/content/Context;->getPackageManager()Landroid/content/pm/PackageManager;
this.mPackageManager = v0;
/* .line 175 */
android.app.AppGlobals .getPackageManager ( );
this.mPackageManagerBinder = v0;
/* .line 176 */
final String v0 = "activity"; // const-string v0, "activity"
(( android.content.Context ) p1 ).getSystemService ( v0 ); // invoke-virtual {p1, v0}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;
/* check-cast v0, Landroid/app/ActivityManager; */
this.mActivityManager = v0;
/* .line 177 */
/* new-instance v0, Landroid/os/HandlerThread; */
final String v2 = "MiuiBackup"; // const-string v2, "MiuiBackup"
/* const/16 v3, 0xa */
/* invoke-direct {v0, v2, v3}, Landroid/os/HandlerThread;-><init>(Ljava/lang/String;I)V */
this.mHandlerThread = v0;
/* .line 178 */
(( android.os.HandlerThread ) v0 ).start ( ); // invoke-virtual {v0}, Landroid/os/HandlerThread;->start()V
/* .line 179 */
/* new-instance v0, Lcom/miui/server/BackupManagerService$BackupHandler; */
v2 = this.mHandlerThread;
(( android.os.HandlerThread ) v2 ).getLooper ( ); // invoke-virtual {v2}, Landroid/os/HandlerThread;->getLooper()Landroid/os/Looper;
/* invoke-direct {v0, p0, v2, v1}, Lcom/miui/server/BackupManagerService$BackupHandler;-><init>(Lcom/miui/server/BackupManagerService;Landroid/os/Looper;Lcom/miui/server/BackupManagerService$BackupHandler-IA;)V */
this.mHandler = v0;
/* .line 180 */
return;
} // .end method
private void broadcastServiceIdle ( ) {
/* .locals 3 */
/* .annotation system Ldalvik/annotation/Throws; */
/* value = { */
/* Landroid/os/RemoteException; */
/* } */
} // .end annotation
/* .line 552 */
/* monitor-enter p0 */
/* .line 554 */
try { // :try_start_0
v0 = this.mStateObservers;
v0 = (( android.os.RemoteCallbackList ) v0 ).beginBroadcast ( ); // invoke-virtual {v0}, Landroid/os/RemoteCallbackList;->beginBroadcast()I
/* .line 555 */
/* .local v0, "cnt":I */
int v1 = 0; // const/4 v1, 0x0
/* .local v1, "i":I */
} // :goto_0
/* if-ge v1, v0, :cond_0 */
/* .line 556 */
v2 = this.mStateObservers;
(( android.os.RemoteCallbackList ) v2 ).getBroadcastItem ( v1 ); // invoke-virtual {v2, v1}, Landroid/os/RemoteCallbackList;->getBroadcastItem(I)Landroid/os/IInterface;
/* check-cast v2, Lmiui/app/backup/IBackupServiceStateObserver; */
/* :try_end_0 */
/* .catchall {:try_start_0 ..:try_end_0} :catchall_0 */
/* .line 555 */
/* add-int/lit8 v1, v1, 0x1 */
/* .line 559 */
} // .end local v0 # "cnt":I
} // .end local v1 # "i":I
} // :cond_0
try { // :try_start_1
v0 = this.mStateObservers;
(( android.os.RemoteCallbackList ) v0 ).finishBroadcast ( ); // invoke-virtual {v0}, Landroid/os/RemoteCallbackList;->finishBroadcast()V
/* .line 560 */
/* nop */
/* .line 561 */
/* monitor-exit p0 */
/* .line 562 */
return;
/* .line 559 */
/* :catchall_0 */
/* move-exception v0 */
v1 = this.mStateObservers;
(( android.os.RemoteCallbackList ) v1 ).finishBroadcast ( ); // invoke-virtual {v1}, Landroid/os/RemoteCallbackList;->finishBroadcast()V
/* .line 560 */
/* nop */
} // .end local p0 # "this":Lcom/miui/server/BackupManagerService;
/* throw v0 */
/* .line 561 */
/* .restart local p0 # "this":Lcom/miui/server/BackupManagerService; */
/* :catchall_1 */
/* move-exception v0 */
/* monitor-exit p0 */
/* :try_end_1 */
/* .catchall {:try_start_1 ..:try_end_1} :catchall_1 */
/* throw v0 */
} // .end method
private Boolean checkPackageAvailable ( java.lang.String p0, Integer p1 ) {
/* .locals 1 */
/* .param p1, "pkg" # Ljava/lang/String; */
/* .param p2, "userId" # I */
/* .line 580 */
v0 = /* invoke-direct {p0, p1, p2}, Lcom/miui/server/BackupManagerService;->isApplicationInstalled(Ljava/lang/String;I)Z */
if ( v0 != null) { // if-eqz v0, :cond_1
v0 = this.mPackageManager;
v0 = com.miui.server.BackupManagerServiceProxy .isPackageStateProtected ( v0,p1,p2 );
if ( v0 != null) { // if-eqz v0, :cond_0
/* .line 583 */
} // :cond_0
int v0 = 1; // const/4 v0, 0x1
/* .line 581 */
} // :cond_1
} // :goto_0
int v0 = 0; // const/4 v0, 0x0
} // .end method
private void closeBackupWriteStream ( android.os.ParcelFileDescriptor p0 ) {
/* .locals 1 */
/* .param p1, "outputFile" # Landroid/os/ParcelFileDescriptor; */
/* .line 961 */
if ( p1 != null) { // if-eqz p1, :cond_0
/* .line 963 */
try { // :try_start_0
(( android.os.ParcelFileDescriptor ) p1 ).getFileDescriptor ( ); // invoke-virtual {p1}, Landroid/os/ParcelFileDescriptor;->getFileDescriptor()Ljava/io/FileDescriptor;
libcore.io.IoBridge .closeAndSignalBlockedThreads ( v0 );
/* :try_end_0 */
/* .catch Ljava/io/IOException; {:try_start_0 ..:try_end_0} :catch_0 */
/* .line 966 */
/* .line 964 */
/* :catch_0 */
/* move-exception v0 */
/* .line 965 */
/* .local v0, "e":Ljava/io/IOException; */
(( java.io.IOException ) v0 ).printStackTrace ( ); // invoke-virtual {v0}, Ljava/io/IOException;->printStackTrace()V
/* .line 968 */
} // .end local v0 # "e":Ljava/io/IOException;
} // :cond_0
} // :goto_0
return;
} // .end method
private void disablePackageAndWait ( java.lang.String p0, Integer p1 ) {
/* .locals 8 */
/* .param p1, "pkg" # Ljava/lang/String; */
/* .param p2, "userId" # I */
/* .line 618 */
v0 = /* invoke-direct {p0, p1, p2}, Lcom/miui/server/BackupManagerService;->checkPackageAvailable(Ljava/lang/String;I)Z */
/* if-nez v0, :cond_0 */
/* .line 619 */
return;
/* .line 621 */
} // :cond_0
v0 = /* invoke-direct {p0, p1, p2}, Lcom/miui/server/BackupManagerService;->getApplicationEnabledSetting(Ljava/lang/String;I)I */
/* .line 622 */
/* .local v0, "applicationSetting":I */
/* iget v1, p0, Lcom/miui/server/BackupManagerService;->mPackageLastEnableState:I */
int v2 = -1; // const/4 v2, -0x1
/* if-ne v1, v2, :cond_1 */
/* .line 623 */
/* iput v0, p0, Lcom/miui/server/BackupManagerService;->mPackageLastEnableState:I */
/* .line 625 */
} // :cond_1
final String v1 = "Backup:BackupManagerService"; // const-string v1, "Backup:BackupManagerService"
/* new-instance v2, Ljava/lang/StringBuilder; */
/* invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V */
final String v3 = "disablePackageAndWait, pkg:"; // const-string v3, "disablePackageAndWait, pkg:"
(( java.lang.StringBuilder ) v2 ).append ( v3 ); // invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v2 ).append ( p1 ); // invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
final String v3 = " userId:"; // const-string v3, " userId:"
(( java.lang.StringBuilder ) v2 ).append ( v3 ); // invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v2 ).append ( p2 ); // invoke-virtual {v2, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;
final String v3 = ", state:"; // const-string v3, ", state:"
(( java.lang.StringBuilder ) v2 ).append ( v3 ); // invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v2 ).append ( v0 ); // invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;
final String v3 = ", lastState:"; // const-string v3, ", lastState:"
(( java.lang.StringBuilder ) v2 ).append ( v3 ); // invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
/* iget v3, p0, Lcom/miui/server/BackupManagerService;->mPackageLastEnableState:I */
(( java.lang.StringBuilder ) v2 ).append ( v3 ); // invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v2 ).toString ( ); // invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
android.util.Slog .d ( v1,v2 );
/* .line 627 */
com.miui.server.BackupManagerService .getPackageEnableStateFile ( );
/* iget v2, p0, Lcom/miui/server/BackupManagerService;->mPackageLastEnableState:I */
/* invoke-direct {p0, v1, p1, v2, p2}, Lcom/miui/server/BackupManagerService;->saveCurrentPackageEnableState(Ljava/io/File;Ljava/lang/String;II)V */
/* .line 628 */
int v1 = 2; // const/4 v1, 0x2
/* if-ne v0, v1, :cond_2 */
/* .line 629 */
return;
/* .line 632 */
} // :cond_2
try { // :try_start_0
v2 = this.mPackageMonitor;
v3 = this.mContext;
(( android.content.Context ) v3 ).getMainLooper ( ); // invoke-virtual {v3}, Landroid/content/Context;->getMainLooper()Landroid/os/Looper;
int v5 = 0; // const/4 v5, 0x0
(( com.android.internal.content.PackageMonitor ) v2 ).register ( v3, v4, v5 ); // invoke-virtual {v2, v3, v4, v5}, Lcom/android/internal/content/PackageMonitor;->register(Landroid/content/Context;Landroid/os/Looper;Z)V
/* .line 633 */
android.os.SystemClock .elapsedRealtime ( );
/* move-result-wide v2 */
/* .line 634 */
/* .local v2, "waitStartTime":J */
v4 = this.mPkgChangingLock;
/* monitor-enter v4 */
/* :try_end_0 */
/* .catchall {:try_start_0 ..:try_end_0} :catchall_1 */
/* .line 636 */
try { // :try_start_1
v6 = this.mPkgChangingLock;
int v7 = 1; // const/4 v7, 0x1
(( java.util.concurrent.atomic.AtomicBoolean ) v6 ).set ( v7 ); // invoke-virtual {v6, v7}, Ljava/util/concurrent/atomic/AtomicBoolean;->set(Z)V
/* .line 637 */
/* invoke-direct {p0, p1, p2, v1, v5}, Lcom/miui/server/BackupManagerService;->setApplicationEnabledSetting(Ljava/lang/String;III)V */
/* .line 638 */
v1 = this.mPkgChangingLock;
/* const-wide/16 v6, 0x1388 */
(( java.lang.Object ) v1 ).wait ( v6, v7 ); // invoke-virtual {v1, v6, v7}, Ljava/lang/Object;->wait(J)V
/* .line 639 */
v1 = this.mPkgChangingLock;
(( java.util.concurrent.atomic.AtomicBoolean ) v1 ).set ( v5 ); // invoke-virtual {v1, v5}, Ljava/util/concurrent/atomic/AtomicBoolean;->set(Z)V
/* :try_end_1 */
/* .catch Ljava/lang/InterruptedException; {:try_start_1 ..:try_end_1} :catch_0 */
/* .catchall {:try_start_1 ..:try_end_1} :catchall_0 */
/* .line 642 */
/* .line 643 */
/* :catchall_0 */
/* move-exception v1 */
/* .line 640 */
/* :catch_0 */
/* move-exception v1 */
/* .line 641 */
/* .local v1, "e":Ljava/lang/InterruptedException; */
try { // :try_start_2
final String v5 = "Backup:BackupManagerService"; // const-string v5, "Backup:BackupManagerService"
final String v6 = "mPkgChangingLock wait error"; // const-string v6, "mPkgChangingLock wait error"
android.util.Slog .e ( v5,v6,v1 );
/* .line 643 */
} // .end local v1 # "e":Ljava/lang/InterruptedException;
} // :goto_0
/* monitor-exit v4 */
/* :try_end_2 */
/* .catchall {:try_start_2 ..:try_end_2} :catchall_0 */
/* .line 644 */
try { // :try_start_3
final String v1 = "Backup:BackupManagerService"; // const-string v1, "Backup:BackupManagerService"
/* new-instance v4, Ljava/lang/StringBuilder; */
/* invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V */
/* const-string/jumbo v5, "setApplicationEnabledSetting wait time=" */
(( java.lang.StringBuilder ) v4 ).append ( v5 ); // invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
android.os.SystemClock .elapsedRealtime ( );
/* move-result-wide v5 */
/* sub-long/2addr v5, v2 */
(( java.lang.StringBuilder ) v4 ).append ( v5, v6 ); // invoke-virtual {v4, v5, v6}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;
final String v5 = ", pkg="; // const-string v5, ", pkg="
(( java.lang.StringBuilder ) v4 ).append ( v5 ); // invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v4 ).append ( p1 ); // invoke-virtual {v4, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v4 ).toString ( ); // invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
android.util.Slog .i ( v1,v4 );
/* :try_end_3 */
/* .catchall {:try_start_3 ..:try_end_3} :catchall_1 */
/* .line 646 */
/* nop */
} // .end local v2 # "waitStartTime":J
v1 = this.mPackageMonitor;
(( com.android.internal.content.PackageMonitor ) v1 ).unregister ( ); // invoke-virtual {v1}, Lcom/android/internal/content/PackageMonitor;->unregister()V
/* .line 647 */
/* nop */
/* .line 648 */
v1 = this.mContext;
final String v2 = "activity"; // const-string v2, "activity"
(( android.content.Context ) v1 ).getSystemService ( v2 ); // invoke-virtual {v1, v2}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;
/* check-cast v1, Landroid/app/ActivityManager; */
(( android.app.ActivityManager ) v1 ).forceStopPackage ( p1 ); // invoke-virtual {v1, p1}, Landroid/app/ActivityManager;->forceStopPackage(Ljava/lang/String;)V
/* .line 649 */
/* invoke-direct {p0, p1}, Lcom/miui/server/BackupManagerService;->waitUntilAppKilled(Ljava/lang/String;)V */
/* .line 650 */
return;
/* .line 643 */
/* .restart local v2 # "waitStartTime":J */
} // :goto_1
try { // :try_start_4
/* monitor-exit v4 */
/* :try_end_4 */
/* .catchall {:try_start_4 ..:try_end_4} :catchall_0 */
} // .end local v0 # "applicationSetting":I
} // .end local p0 # "this":Lcom/miui/server/BackupManagerService;
} // .end local p1 # "pkg":Ljava/lang/String;
} // .end local p2 # "userId":I
try { // :try_start_5
/* throw v1 */
/* :try_end_5 */
/* .catchall {:try_start_5 ..:try_end_5} :catchall_1 */
/* .line 646 */
} // .end local v2 # "waitStartTime":J
/* .restart local v0 # "applicationSetting":I */
/* .restart local p0 # "this":Lcom/miui/server/BackupManagerService; */
/* .restart local p1 # "pkg":Ljava/lang/String; */
/* .restart local p2 # "userId":I */
/* :catchall_1 */
/* move-exception v1 */
v2 = this.mPackageMonitor;
(( com.android.internal.content.PackageMonitor ) v2 ).unregister ( ); // invoke-virtual {v2}, Lcom/android/internal/content/PackageMonitor;->unregister()V
/* .line 647 */
/* throw v1 */
} // .end method
private void enablePackage ( java.lang.String p0, Integer p1, java.lang.String p2 ) {
/* .locals 4 */
/* .param p1, "pkg" # Ljava/lang/String; */
/* .param p2, "userId" # I */
/* .param p3, "defaultIme" # Ljava/lang/String; */
/* .line 588 */
v0 = /* invoke-direct {p0, p1, p2}, Lcom/miui/server/BackupManagerService;->checkPackageAvailable(Ljava/lang/String;I)Z */
/* if-nez v0, :cond_0 */
/* .line 589 */
return;
/* .line 591 */
} // :cond_0
/* iget v0, p0, Lcom/miui/server/BackupManagerService;->mPackageLastEnableState:I */
int v1 = -1; // const/4 v1, -0x1
int v2 = 0; // const/4 v2, 0x0
/* if-ne v0, v1, :cond_1 */
/* .line 592 */
/* iput v2, p0, Lcom/miui/server/BackupManagerService;->mPackageLastEnableState:I */
/* .line 594 */
} // :cond_1
/* new-instance v0, Ljava/lang/StringBuilder; */
/* invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V */
final String v1 = "enablePackage, pkg:"; // const-string v1, "enablePackage, pkg:"
(( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v0 ).append ( p1 ); // invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
final String v1 = " userId:"; // const-string v1, " userId:"
(( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v0 ).append ( p2 ); // invoke-virtual {v0, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;
final String v1 = ", state:"; // const-string v1, ", state:"
(( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
/* iget v1, p0, Lcom/miui/server/BackupManagerService;->mPackageLastEnableState:I */
(( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;
final String v1 = ", defaultIme:"; // const-string v1, ", defaultIme:"
(( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v0 ).append ( p3 ); // invoke-virtual {v0, p3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v0 ).toString ( ); // invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
final String v1 = "Backup:BackupManagerService"; // const-string v1, "Backup:BackupManagerService"
android.util.Slog .d ( v1,v0 );
/* .line 596 */
v0 = /* invoke-direct {p0, p1, p3}, Lcom/miui/server/BackupManagerService;->isDefaultIme(Ljava/lang/String;Ljava/lang/String;)Z */
if ( v0 != null) { // if-eqz v0, :cond_2
/* .line 601 */
/* iget v0, p0, Lcom/miui/server/BackupManagerService;->mPackageLastEnableState:I */
/* invoke-direct {p0, p1, p2, v0, v2}, Lcom/miui/server/BackupManagerService;->setApplicationEnabledSetting(Ljava/lang/String;III)V */
/* .line 602 */
v0 = this.mHandler;
/* new-instance v1, Lcom/miui/server/BackupManagerService$3; */
/* invoke-direct {v1, p0, p3}, Lcom/miui/server/BackupManagerService$3;-><init>(Lcom/miui/server/BackupManagerService;Ljava/lang/String;)V */
/* const-wide/16 v2, 0x7d0 */
(( android.os.Handler ) v0 ).postDelayed ( v1, v2, v3 ); // invoke-virtual {v0, v1, v2, v3}, Landroid/os/Handler;->postDelayed(Ljava/lang/Runnable;J)Z
/* .line 609 */
return;
/* .line 612 */
} // :cond_2
/* iget v0, p0, Lcom/miui/server/BackupManagerService;->mPackageLastEnableState:I */
int v1 = 1; // const/4 v1, 0x1
/* invoke-direct {p0, p1, p2, v0, v1}, Lcom/miui/server/BackupManagerService;->setApplicationEnabledSetting(Ljava/lang/String;III)V */
/* .line 613 */
com.miui.server.BackupManagerService .getPackageEnableStateFile ( );
/* .line 614 */
/* .local v0, "pkgStateFile":Ljava/io/File; */
(( java.io.File ) v0 ).delete ( ); // invoke-virtual {v0}, Ljava/io/File;->delete()Z
/* .line 615 */
return;
} // .end method
private Integer getApplicationEnabledSetting ( java.lang.String p0, Integer p1 ) {
/* .locals 2 */
/* .param p1, "packageName" # Ljava/lang/String; */
/* .param p2, "userId" # I */
/* .line 993 */
try { // :try_start_0
v0 = v0 = this.mPackageManagerBinder;
/* :try_end_0 */
/* .catch Landroid/os/RemoteException; {:try_start_0 ..:try_end_0} :catch_0 */
/* .line 994 */
/* :catch_0 */
/* move-exception v0 */
/* .line 995 */
/* .local v0, "e":Landroid/os/RemoteException; */
(( android.os.RemoteException ) v0 ).rethrowFromSystemServer ( ); // invoke-virtual {v0}, Landroid/os/RemoteException;->rethrowFromSystemServer()Ljava/lang/RuntimeException;
/* throw v1 */
} // .end method
public static java.io.File getCachedInstallFile ( ) {
/* .locals 3 */
/* .line 693 */
/* new-instance v0, Ljava/io/File; */
android.os.Environment .getDataDirectory ( );
/* const-string/jumbo v2, "system" */
/* invoke-direct {v0, v1, v2}, Ljava/io/File;-><init>(Ljava/io/File;Ljava/lang/String;)V */
/* .line 694 */
/* .local v0, "sysDir":Ljava/io/File; */
/* new-instance v1, Ljava/io/File; */
final String v2 = "restoring_cached_file"; // const-string v2, "restoring_cached_file"
/* invoke-direct {v1, v0, v2}, Ljava/io/File;-><init>(Ljava/io/File;Ljava/lang/String;)V */
/* .line 695 */
/* .local v1, "cachedFile":Ljava/io/File; */
} // .end method
private java.lang.String getDefaultIme ( android.content.Context p0 ) {
/* .locals 2 */
/* .param p1, "context" # Landroid/content/Context; */
/* .line 931 */
(( android.content.Context ) p1 ).getContentResolver ( ); // invoke-virtual {p1}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;
final String v1 = "default_input_method"; // const-string v1, "default_input_method"
android.provider.Settings$Secure .getString ( v0,v1 );
/* .line 933 */
/* .local v0, "defaultIme":Ljava/lang/String; */
} // .end method
private static java.io.File getPackageEnableStateFile ( ) {
/* .locals 3 */
/* .line 687 */
/* new-instance v0, Ljava/io/File; */
android.os.Environment .getDataDirectory ( );
/* const-string/jumbo v2, "system" */
/* invoke-direct {v0, v1, v2}, Ljava/io/File;-><init>(Ljava/io/File;Ljava/lang/String;)V */
/* .line 688 */
/* .local v0, "systemDir":Ljava/io/File; */
/* new-instance v1, Ljava/io/File; */
final String v2 = "backup_pkg_enable_state"; // const-string v2, "backup_pkg_enable_state"
/* invoke-direct {v1, v0, v2}, Ljava/io/File;-><init>(Ljava/io/File;Ljava/lang/String;)V */
/* .line 689 */
/* .local v1, "pkgStateFile":Ljava/io/File; */
} // .end method
private Boolean isApplicationInstalled ( java.lang.String p0, Integer p1 ) {
/* .locals 4 */
/* .param p1, "packageName" # Ljava/lang/String; */
/* .param p2, "userId" # I */
/* .line 275 */
v0 = this.mPackageManager;
int v1 = 0; // const/4 v1, 0x0
(( android.content.pm.PackageManager ) v0 ).getInstalledPackagesAsUser ( v1, p2 ); // invoke-virtual {v0, v1, p2}, Landroid/content/pm/PackageManager;->getInstalledPackagesAsUser(II)Ljava/util/List;
/* .line 276 */
/* .local v0, "installedList":Ljava/util/List;, "Ljava/util/List<Landroid/content/pm/PackageInfo;>;" */
int v1 = 0; // const/4 v1, 0x0
/* .line 277 */
/* .local v1, "isInstalled":Z */
int v2 = 0; // const/4 v2, 0x0
/* .local v2, "i":I */
v3 = } // :goto_0
/* if-ge v2, v3, :cond_1 */
/* .line 278 */
/* check-cast v3, Landroid/content/pm/PackageInfo; */
v3 = this.packageName;
v3 = (( java.lang.String ) v3 ).equals ( p1 ); // invoke-virtual {v3, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z
if ( v3 != null) { // if-eqz v3, :cond_0
/* .line 279 */
int v1 = 1; // const/4 v1, 0x1
/* .line 280 */
/* .line 277 */
} // :cond_0
/* add-int/lit8 v2, v2, 0x1 */
/* .line 283 */
} // .end local v2 # "i":I
} // :cond_1
} // :goto_1
/* new-instance v2, Ljava/lang/StringBuilder; */
/* invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V */
final String v3 = "isApplicationInstalled, packageName:"; // const-string v3, "isApplicationInstalled, packageName:"
(( java.lang.StringBuilder ) v2 ).append ( v3 ); // invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v2 ).append ( p1 ); // invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
final String v3 = " isInstalled:"; // const-string v3, " isInstalled:"
(( java.lang.StringBuilder ) v2 ).append ( v3 ); // invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v2 ).append ( v1 ); // invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v2 ).toString ( ); // invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
final String v3 = "Backup:BackupManagerService"; // const-string v3, "Backup:BackupManagerService"
android.util.Slog .d ( v3,v2 );
/* .line 284 */
} // .end method
private Boolean isDefaultIme ( java.lang.String p0, java.lang.String p1 ) {
/* .locals 2 */
/* .param p1, "pkg" # Ljava/lang/String; */
/* .param p2, "defaultIme" # Ljava/lang/String; */
/* .line 937 */
if ( p1 != null) { // if-eqz p1, :cond_0
if ( p2 != null) { // if-eqz p2, :cond_0
/* .line 938 */
android.content.ComponentName .unflattenFromString ( p2 );
/* .line 939 */
/* .local v0, "cn":Landroid/content/ComponentName; */
if ( v0 != null) { // if-eqz v0, :cond_0
(( android.content.ComponentName ) v0 ).getPackageName ( ); // invoke-virtual {v0}, Landroid/content/ComponentName;->getPackageName()Ljava/lang/String;
v1 = android.text.TextUtils .equals ( p1,v1 );
if ( v1 != null) { // if-eqz v1, :cond_0
/* .line 940 */
int v1 = 1; // const/4 v1, 0x1
/* .line 943 */
} // .end local v0 # "cn":Landroid/content/ComponentName;
} // :cond_0
int v0 = 0; // const/4 v0, 0x0
} // .end method
static void lambda$releaseBackupWriteStream$0 ( android.os.ParcelFileDescriptor p0 ) { //synthethic
/* .locals 6 */
/* .param p0, "outputFile" # Landroid/os/ParcelFileDescriptor; */
/* .line 973 */
final String v0 = "IOException"; // const-string v0, "IOException"
final String v1 = "Backup:BackupManagerService"; // const-string v1, "Backup:BackupManagerService"
/* const/16 v2, 0x400 */
/* new-array v2, v2, [B */
/* .line 974 */
/* .local v2, "b":[B */
/* new-instance v3, Ljava/io/FileInputStream; */
(( android.os.ParcelFileDescriptor ) p0 ).getFileDescriptor ( ); // invoke-virtual {p0}, Landroid/os/ParcelFileDescriptor;->getFileDescriptor()Ljava/io/FileDescriptor;
/* invoke-direct {v3, v4}, Ljava/io/FileInputStream;-><init>(Ljava/io/FileDescriptor;)V */
/* .line 976 */
/* .local v3, "fis":Ljava/io/FileInputStream; */
} // :goto_0
try { // :try_start_0
v4 = (( java.io.FileInputStream ) v3 ).read ( v2 ); // invoke-virtual {v3, v2}, Ljava/io/FileInputStream;->read([B)I
/* :try_end_0 */
/* .catch Ljava/io/IOException; {:try_start_0 ..:try_end_0} :catch_1 */
/* .catchall {:try_start_0 ..:try_end_0} :catchall_0 */
/* if-lez v4, :cond_0 */
/* .line 981 */
} // :cond_0
try { // :try_start_1
(( java.io.FileInputStream ) v3 ).close ( ); // invoke-virtual {v3}, Ljava/io/FileInputStream;->close()V
/* :try_end_1 */
/* .catch Ljava/io/IOException; {:try_start_1 ..:try_end_1} :catch_0 */
/* .line 984 */
} // :goto_1
/* .line 982 */
/* :catch_0 */
/* move-exception v4 */
/* .line 983 */
/* .local v4, "e":Ljava/io/IOException; */
android.util.Slog .e ( v1,v0,v4 );
/* .line 985 */
} // .end local v4 # "e":Ljava/io/IOException;
/* .line 980 */
/* :catchall_0 */
/* move-exception v4 */
/* .line 977 */
/* :catch_1 */
/* move-exception v4 */
/* .line 978 */
/* .restart local v4 # "e":Ljava/io/IOException; */
try { // :try_start_2
final String v5 = "releaseBackupReadStream"; // const-string v5, "releaseBackupReadStream"
android.util.Slog .e ( v1,v5,v4 );
/* :try_end_2 */
/* .catchall {:try_start_2 ..:try_end_2} :catchall_0 */
/* .line 981 */
} // .end local v4 # "e":Ljava/io/IOException;
try { // :try_start_3
(( java.io.FileInputStream ) v3 ).close ( ); // invoke-virtual {v3}, Ljava/io/FileInputStream;->close()V
/* :try_end_3 */
/* .catch Ljava/io/IOException; {:try_start_3 ..:try_end_3} :catch_0 */
/* .line 986 */
} // :goto_2
return;
/* .line 981 */
} // :goto_3
try { // :try_start_4
(( java.io.FileInputStream ) v3 ).close ( ); // invoke-virtual {v3}, Ljava/io/FileInputStream;->close()V
/* :try_end_4 */
/* .catch Ljava/io/IOException; {:try_start_4 ..:try_end_4} :catch_2 */
/* .line 984 */
/* .line 982 */
/* :catch_2 */
/* move-exception v5 */
/* .line 983 */
/* .local v5, "e":Ljava/io/IOException; */
android.util.Slog .e ( v1,v0,v5 );
/* .line 985 */
} // .end local v5 # "e":Ljava/io/IOException;
} // :goto_4
/* throw v4 */
} // .end method
private static java.lang.String readHeaderLine ( java.io.InputStream p0 ) {
/* .locals 3 */
/* .param p0, "in" # Ljava/io/InputStream; */
/* .annotation system Ldalvik/annotation/Throws; */
/* value = { */
/* Ljava/io/IOException; */
/* } */
} // .end annotation
/* .line 403 */
/* new-instance v0, Ljava/lang/StringBuilder; */
/* const/16 v1, 0x50 */
/* invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(I)V */
/* .line 404 */
/* .local v0, "buffer":Ljava/lang/StringBuilder; */
} // :goto_0
v1 = (( java.io.InputStream ) p0 ).read ( ); // invoke-virtual {p0}, Ljava/io/InputStream;->read()I
/* move v2, v1 */
/* .local v2, "c":I */
/* if-ltz v1, :cond_1 */
/* .line 405 */
/* const/16 v1, 0xa */
/* if-ne v2, v1, :cond_0 */
/* .line 406 */
} // :cond_0
/* int-to-char v1, v2 */
(( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;
/* .line 408 */
} // :cond_1
} // :goto_1
(( java.lang.StringBuilder ) v0 ).toString ( ); // invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
} // .end method
private void releaseBackupWriteStream ( android.os.ParcelFileDescriptor p0 ) {
/* .locals 2 */
/* .param p1, "outputFile" # Landroid/os/ParcelFileDescriptor; */
/* .line 971 */
if ( p1 != null) { // if-eqz p1, :cond_0
/* .line 972 */
v0 = this.mHandler;
/* new-instance v1, Lcom/miui/server/BackupManagerService$$ExternalSyntheticLambda0; */
/* invoke-direct {v1, p1}, Lcom/miui/server/BackupManagerService$$ExternalSyntheticLambda0;-><init>(Landroid/os/ParcelFileDescriptor;)V */
(( android.os.Handler ) v0 ).post ( v1 ); // invoke-virtual {v0, v1}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z
/* .line 988 */
} // :cond_0
return;
} // .end method
private void restoreLastPackageEnableState ( java.io.File p0 ) {
/* .locals 17 */
/* .param p1, "pkgStateFile" # Ljava/io/File; */
/* .line 720 */
/* move-object/from16 v1, p0 */
final String v2 = "IOEception"; // const-string v2, "IOEception"
final String v3 = "Backup:BackupManagerService"; // const-string v3, "Backup:BackupManagerService"
com.miui.server.BackupManagerService .getCachedInstallFile ( );
/* .line 721 */
/* .local v4, "cachedFile":Ljava/io/File; */
v0 = (( java.io.File ) v4 ).exists ( ); // invoke-virtual {v4}, Ljava/io/File;->exists()Z
if ( v0 != null) { // if-eqz v0, :cond_0
/* .line 722 */
(( java.io.File ) v4 ).delete ( ); // invoke-virtual {v4}, Ljava/io/File;->delete()Z
/* .line 725 */
} // :cond_0
v0 = /* invoke-virtual/range {p1 ..p1}, Ljava/io/File;->exists()Z */
if ( v0 != null) { // if-eqz v0, :cond_8
/* .line 726 */
int v5 = 0; // const/4 v5, 0x0
/* .line 727 */
/* .local v5, "in":Ljava/io/FileInputStream; */
int v6 = 0; // const/4 v6, 0x0
/* .line 728 */
/* .local v6, "pkg":Ljava/lang/String; */
/* const/high16 v7, -0x80000000 */
/* .line 729 */
/* .local v7, "state":I */
int v8 = 0; // const/4 v8, 0x0
/* .line 732 */
/* .local v8, "userId":I */
try { // :try_start_0
/* new-instance v0, Ljava/io/FileInputStream; */
/* :try_end_0 */
/* .catch Ljava/io/IOException; {:try_start_0 ..:try_end_0} :catch_4 */
/* .catchall {:try_start_0 ..:try_end_0} :catchall_1 */
/* move-object/from16 v9, p1 */
try { // :try_start_1
/* invoke-direct {v0, v9}, Ljava/io/FileInputStream;-><init>(Ljava/io/File;)V */
/* move-object v5, v0 */
/* .line 734 */
/* new-instance v0, Ljava/util/ArrayList; */
/* invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V */
/* move-object v10, v0 */
/* .line 735 */
/* .local v10, "buffer":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/Byte;>;" */
} // :goto_0
v0 = (( java.io.FileInputStream ) v5 ).read ( ); // invoke-virtual {v5}, Ljava/io/FileInputStream;->read()I
/* move v11, v0 */
/* .local v11, "c":I */
/* if-ltz v0, :cond_1 */
/* .line 736 */
/* int-to-byte v0, v11 */
java.lang.Byte .valueOf ( v0 );
(( java.util.ArrayList ) v10 ).add ( v0 ); // invoke-virtual {v10, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z
/* .line 738 */
} // :cond_1
v0 = (( java.util.ArrayList ) v10 ).size ( ); // invoke-virtual {v10}, Ljava/util/ArrayList;->size()I
/* new-array v0, v0, [B */
/* move-object v12, v0 */
/* .line 739 */
/* .local v12, "bytes":[B */
int v0 = 0; // const/4 v0, 0x0
/* .local v0, "i":I */
} // :goto_1
v13 = (( java.util.ArrayList ) v10 ).size ( ); // invoke-virtual {v10}, Ljava/util/ArrayList;->size()I
/* if-ge v0, v13, :cond_2 */
/* .line 740 */
(( java.util.ArrayList ) v10 ).get ( v0 ); // invoke-virtual {v10, v0}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;
/* check-cast v13, Ljava/lang/Byte; */
v13 = (( java.lang.Byte ) v13 ).byteValue ( ); // invoke-virtual {v13}, Ljava/lang/Byte;->byteValue()B
/* aput-byte v13, v12, v0 */
/* .line 739 */
/* add-int/lit8 v0, v0, 0x1 */
/* .line 742 */
} // .end local v0 # "i":I
} // :cond_2
/* new-instance v0, Ljava/lang/String; */
/* invoke-direct {v0, v12}, Ljava/lang/String;-><init>([B)V */
final String v13 = " "; // const-string v13, " "
(( java.lang.String ) v0 ).split ( v13 ); // invoke-virtual {v0, v13}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;
/* move-object v13, v0 */
/* .line 743 */
/* .local v13, "ss":[Ljava/lang/String; */
/* array-length v0, v13 */
int v14 = 0; // const/4 v14, 0x0
int v15 = 2; // const/4 v15, 0x2
/* const/16 v16, 0x1 */
/* if-ne v0, v15, :cond_3 */
/* .line 744 */
/* aget-object v0, v13, v14 */
/* :try_end_1 */
/* .catch Ljava/io/IOException; {:try_start_1 ..:try_end_1} :catch_3 */
/* .catchall {:try_start_1 ..:try_end_1} :catchall_0 */
/* move-object v6, v0 */
/* .line 746 */
try { // :try_start_2
/* aget-object v0, v13, v16 */
v0 = java.lang.Integer .parseInt ( v0 );
/* :try_end_2 */
/* .catch Ljava/lang/NumberFormatException; {:try_start_2 ..:try_end_2} :catch_0 */
/* .catch Ljava/io/IOException; {:try_start_2 ..:try_end_2} :catch_3 */
/* .catchall {:try_start_2 ..:try_end_2} :catchall_0 */
/* move v7, v0 */
/* .line 747 */
/* :catch_0 */
/* move-exception v0 */
/* .line 748 */
} // :goto_2
/* .line 749 */
} // :cond_3
try { // :try_start_3
/* array-length v0, v13 */
int v15 = 3; // const/4 v15, 0x3
/* if-ne v0, v15, :cond_4 */
/* .line 750 */
/* aget-object v0, v13, v14 */
/* :try_end_3 */
/* .catch Ljava/io/IOException; {:try_start_3 ..:try_end_3} :catch_3 */
/* .catchall {:try_start_3 ..:try_end_3} :catchall_0 */
/* move-object v6, v0 */
/* .line 752 */
try { // :try_start_4
/* aget-object v0, v13, v16 */
v0 = java.lang.Integer .parseInt ( v0 );
/* move v7, v0 */
/* .line 753 */
int v0 = 2; // const/4 v0, 0x2
/* aget-object v0, v13, v0 */
v0 = java.lang.Integer .parseInt ( v0 );
/* :try_end_4 */
/* .catch Ljava/lang/NumberFormatException; {:try_start_4 ..:try_end_4} :catch_1 */
/* .catch Ljava/io/IOException; {:try_start_4 ..:try_end_4} :catch_3 */
/* .catchall {:try_start_4 ..:try_end_4} :catchall_0 */
/* .line 755 */
} // .end local v8 # "userId":I
/* .local v0, "userId":I */
/* move v8, v0 */
/* .line 754 */
} // .end local v0 # "userId":I
/* .restart local v8 # "userId":I */
/* :catch_1 */
/* move-exception v0 */
/* .line 760 */
} // .end local v10 # "buffer":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/Byte;>;"
} // .end local v11 # "c":I
} // .end local v12 # "bytes":[B
} // .end local v13 # "ss":[Ljava/lang/String;
} // :cond_4
} // :goto_3
/* nop */
/* .line 762 */
try { // :try_start_5
(( java.io.FileInputStream ) v5 ).close ( ); // invoke-virtual {v5}, Ljava/io/FileInputStream;->close()V
/* :try_end_5 */
/* .catch Ljava/io/IOException; {:try_start_5 ..:try_end_5} :catch_2 */
/* .line 765 */
} // :goto_4
/* .line 763 */
/* :catch_2 */
/* move-exception v0 */
/* move-object v10, v0 */
/* move-object v0, v10 */
/* .line 764 */
/* .local v0, "e":Ljava/io/IOException; */
android.util.Slog .e ( v3,v2,v0 );
} // .end local v0 # "e":Ljava/io/IOException;
/* .line 760 */
/* :catchall_0 */
/* move-exception v0 */
/* .line 757 */
/* :catch_3 */
/* move-exception v0 */
/* .line 760 */
/* :catchall_1 */
/* move-exception v0 */
/* move-object/from16 v9, p1 */
} // :goto_5
/* move-object v10, v0 */
/* .line 757 */
/* :catch_4 */
/* move-exception v0 */
/* move-object/from16 v9, p1 */
/* .line 758 */
/* .restart local v0 # "e":Ljava/io/IOException; */
} // :goto_6
try { // :try_start_6
final String v10 = "IOException"; // const-string v10, "IOException"
android.util.Slog .e ( v3,v10,v0 );
/* :try_end_6 */
/* .catchall {:try_start_6 ..:try_end_6} :catchall_0 */
/* .line 760 */
/* nop */
} // .end local v0 # "e":Ljava/io/IOException;
if ( v5 != null) { // if-eqz v5, :cond_5
/* .line 762 */
try { // :try_start_7
(( java.io.FileInputStream ) v5 ).close ( ); // invoke-virtual {v5}, Ljava/io/FileInputStream;->close()V
/* :try_end_7 */
/* .catch Ljava/io/IOException; {:try_start_7 ..:try_end_7} :catch_2 */
/* .line 769 */
} // :cond_5
} // :goto_7
if ( v6 != null) { // if-eqz v6, :cond_6
/* const/high16 v0, -0x80000000 */
/* if-eq v7, v0, :cond_6 */
/* .line 770 */
final String v0 = "Unfinished backup package found, restore it\'s enable state"; // const-string v0, "Unfinished backup package found, restore it\'s enable state"
android.util.Slog .v ( v3,v0 );
/* .line 771 */
v0 = this.mContext;
/* invoke-direct {v1, v0}, Lcom/miui/server/BackupManagerService;->getDefaultIme(Landroid/content/Context;)Ljava/lang/String; */
/* .line 772 */
/* .local v0, "defaultIme":Ljava/lang/String; */
/* iput v7, v1, Lcom/miui/server/BackupManagerService;->mPackageLastEnableState:I */
/* .line 773 */
/* invoke-direct {v1, v6, v8, v0}, Lcom/miui/server/BackupManagerService;->enablePackage(Ljava/lang/String;ILjava/lang/String;)V */
/* .line 774 */
} // .end local v0 # "defaultIme":Ljava/lang/String;
/* .line 775 */
} // :cond_6
final String v0 = "backup_pkg_enable_state file broken"; // const-string v0, "backup_pkg_enable_state file broken"
android.util.Slog .e ( v3,v0 );
/* .line 760 */
} // :goto_8
if ( v5 != null) { // if-eqz v5, :cond_7
/* .line 762 */
try { // :try_start_8
(( java.io.FileInputStream ) v5 ).close ( ); // invoke-virtual {v5}, Ljava/io/FileInputStream;->close()V
/* :try_end_8 */
/* .catch Ljava/io/IOException; {:try_start_8 ..:try_end_8} :catch_5 */
/* .line 765 */
/* .line 763 */
/* :catch_5 */
/* move-exception v0 */
/* move-object v11, v0 */
/* move-object v0, v11 */
/* .line 764 */
/* .local v0, "e":Ljava/io/IOException; */
android.util.Slog .e ( v3,v2,v0 );
/* .line 767 */
} // .end local v0 # "e":Ljava/io/IOException;
} // :cond_7
} // :goto_9
/* throw v10 */
/* .line 725 */
} // .end local v5 # "in":Ljava/io/FileInputStream;
} // .end local v6 # "pkg":Ljava/lang/String;
} // .end local v7 # "state":I
} // .end local v8 # "userId":I
} // :cond_8
/* move-object/from16 v9, p1 */
/* .line 778 */
} // :goto_a
return;
} // .end method
private void saveCurrentPackageEnableState ( java.io.File p0, java.lang.String p1, Integer p2, Integer p3 ) {
/* .locals 6 */
/* .param p1, "pkgStateFile" # Ljava/io/File; */
/* .param p2, "pkg" # Ljava/lang/String; */
/* .param p3, "state" # I */
/* .param p4, "userId" # I */
/* .line 699 */
final String v0 = " "; // const-string v0, " "
final String v1 = "IOException"; // const-string v1, "IOException"
final String v2 = "Backup:BackupManagerService"; // const-string v2, "Backup:BackupManagerService"
int v3 = 0; // const/4 v3, 0x0
/* .line 701 */
/* .local v3, "out":Ljava/io/FileOutputStream; */
try { // :try_start_0
/* new-instance v4, Ljava/io/FileOutputStream; */
/* invoke-direct {v4, p1}, Ljava/io/FileOutputStream;-><init>(Ljava/io/File;)V */
/* move-object v3, v4 */
/* .line 702 */
/* new-instance v4, Ljava/lang/StringBuilder; */
/* invoke-direct {v4, p2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V */
/* .line 703 */
/* .local v4, "sb":Ljava/lang/StringBuilder; */
(( java.lang.StringBuilder ) v4 ).append ( v0 ); // invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v5 ).append ( p3 ); // invoke-virtual {v5, p3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;
/* .line 704 */
(( java.lang.StringBuilder ) v4 ).append ( v0 ); // invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v0 ).append ( p4 ); // invoke-virtual {v0, p4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;
/* .line 705 */
(( java.lang.StringBuilder ) v4 ).toString ( ); // invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
(( java.lang.String ) v0 ).getBytes ( ); // invoke-virtual {v0}, Ljava/lang/String;->getBytes()[B
(( java.io.FileOutputStream ) v3 ).write ( v0 ); // invoke-virtual {v3, v0}, Ljava/io/FileOutputStream;->write([B)V
/* :try_end_0 */
/* .catch Ljava/io/IOException; {:try_start_0 ..:try_end_0} :catch_1 */
/* .catchall {:try_start_0 ..:try_end_0} :catchall_0 */
/* .line 709 */
} // .end local v4 # "sb":Ljava/lang/StringBuilder;
/* nop */
/* .line 711 */
try { // :try_start_1
(( java.io.FileOutputStream ) v3 ).close ( ); // invoke-virtual {v3}, Ljava/io/FileOutputStream;->close()V
/* :try_end_1 */
/* .catch Ljava/io/IOException; {:try_start_1 ..:try_end_1} :catch_0 */
/* .line 714 */
} // :goto_0
/* .line 712 */
/* :catch_0 */
/* move-exception v0 */
/* .line 713 */
/* .local v0, "e":Ljava/io/IOException; */
android.util.Slog .e ( v2,v1,v0 );
} // .end local v0 # "e":Ljava/io/IOException;
/* .line 709 */
/* :catchall_0 */
/* move-exception v0 */
/* .line 706 */
/* :catch_1 */
/* move-exception v0 */
/* .line 707 */
/* .restart local v0 # "e":Ljava/io/IOException; */
try { // :try_start_2
android.util.Slog .e ( v2,v1,v0 );
/* :try_end_2 */
/* .catchall {:try_start_2 ..:try_end_2} :catchall_0 */
/* .line 709 */
/* nop */
} // .end local v0 # "e":Ljava/io/IOException;
if ( v3 != null) { // if-eqz v3, :cond_0
/* .line 711 */
try { // :try_start_3
(( java.io.FileOutputStream ) v3 ).close ( ); // invoke-virtual {v3}, Ljava/io/FileOutputStream;->close()V
/* :try_end_3 */
/* .catch Ljava/io/IOException; {:try_start_3 ..:try_end_3} :catch_0 */
/* .line 717 */
} // :cond_0
} // :goto_1
return;
/* .line 709 */
} // :goto_2
if ( v3 != null) { // if-eqz v3, :cond_1
/* .line 711 */
try { // :try_start_4
(( java.io.FileOutputStream ) v3 ).close ( ); // invoke-virtual {v3}, Ljava/io/FileOutputStream;->close()V
/* :try_end_4 */
/* .catch Ljava/io/IOException; {:try_start_4 ..:try_end_4} :catch_2 */
/* .line 714 */
/* .line 712 */
/* :catch_2 */
/* move-exception v4 */
/* .line 713 */
/* .local v4, "e":Ljava/io/IOException; */
android.util.Slog .e ( v2,v1,v4 );
/* .line 716 */
} // .end local v4 # "e":Ljava/io/IOException;
} // :cond_1
} // :goto_3
/* throw v0 */
} // .end method
private void scheduleReleaseResource ( ) {
/* .locals 1 */
/* .line 537 */
v0 = this.mTaskLatch;
if ( v0 != null) { // if-eqz v0, :cond_0
v0 = (( java.util.concurrent.atomic.AtomicBoolean ) v0 ).get ( ); // invoke-virtual {v0}, Ljava/util/concurrent/atomic/AtomicBoolean;->get()Z
/* if-nez v0, :cond_0 */
/* .line 538 */
/* nop */
/* .line 540 */
try { // :try_start_0
com.miui.server.BackupManagerServiceProxy .fullCancel ( );
/* :try_end_0 */
/* .catch Landroid/os/RemoteException; {:try_start_0 ..:try_end_0} :catch_0 */
/* .line 543 */
/* .line 541 */
/* :catch_0 */
/* move-exception v0 */
/* .line 542 */
/* .local v0, "e":Landroid/os/RemoteException; */
(( android.os.RemoteException ) v0 ).printStackTrace ( ); // invoke-virtual {v0}, Landroid/os/RemoteException;->printStackTrace()V
/* .line 544 */
} // .end local v0 # "e":Landroid/os/RemoteException;
} // :goto_0
v0 = this.mOutputFile;
/* invoke-direct {p0, v0}, Lcom/miui/server/BackupManagerService;->closeBackupWriteStream(Landroid/os/ParcelFileDescriptor;)V */
/* .line 549 */
} // :cond_0
return;
} // .end method
private void setApplicationEnabledSetting ( java.lang.String p0, Integer p1, Integer p2, Integer p3 ) {
/* .locals 11 */
/* .param p1, "packageName" # Ljava/lang/String; */
/* .param p2, "userId" # I */
/* .param p3, "newState" # I */
/* .param p4, "flags" # I */
/* .line 1001 */
android.os.Binder .clearCallingIdentity ( );
/* move-result-wide v0 */
/* .line 1003 */
/* .local v0, "token":J */
int v2 = 2; // const/4 v2, 0x2
/* if-ne p3, v2, :cond_0 */
int v2 = 1; // const/4 v2, 0x1
} // :cond_0
int v2 = 0; // const/4 v2, 0x0
/* .line 1004 */
/* .local v2, "suspend":Z */
} // :goto_0
try { // :try_start_0
v3 = this.mPackageManagerBinder;
/* filled-new-array {p1}, [Ljava/lang/String; */
int v6 = 0; // const/4 v6, 0x0
int v7 = 0; // const/4 v7, 0x0
/* new-instance v5, Landroid/content/pm/SuspendDialogInfo$Builder; */
/* invoke-direct {v5}, Landroid/content/pm/SuspendDialogInfo$Builder;-><init>()V */
v8 = this.mContext;
(( android.content.Context ) v8 ).getResources ( ); // invoke-virtual {v8}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;
/* const v9, 0x110f006e */
(( android.content.res.Resources ) v8 ).getString ( v9 ); // invoke-virtual {v8, v9}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;
(( android.content.pm.SuspendDialogInfo$Builder ) v5 ).setMessage ( v8 ); // invoke-virtual {v5, v8}, Landroid/content/pm/SuspendDialogInfo$Builder;->setMessage(Ljava/lang/String;)Landroid/content/pm/SuspendDialogInfo$Builder;
(( android.content.pm.SuspendDialogInfo$Builder ) v5 ).build ( ); // invoke-virtual {v5}, Landroid/content/pm/SuspendDialogInfo$Builder;->build()Landroid/content/pm/SuspendDialogInfo;
v5 = this.mContext;
(( android.content.Context ) v5 ).getOpPackageName ( ); // invoke-virtual {v5}, Landroid/content/Context;->getOpPackageName()Ljava/lang/String;
/* move v5, v2 */
/* move v10, p2 */
/* invoke-interface/range {v3 ..v10}, Landroid/content/pm/IPackageManager;->setPackagesSuspendedAsUser([Ljava/lang/String;ZLandroid/os/PersistableBundle;Landroid/os/PersistableBundle;Landroid/content/pm/SuspendDialogInfo;Ljava/lang/String;I)[Ljava/lang/String; */
/* .line 1007 */
final String v3 = "Backup:BackupManagerService"; // const-string v3, "Backup:BackupManagerService"
/* new-instance v4, Ljava/lang/StringBuilder; */
/* invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V */
final String v5 = "packageName "; // const-string v5, "packageName "
(( java.lang.StringBuilder ) v4 ).append ( v5 ); // invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v4 ).append ( p1 ); // invoke-virtual {v4, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
final String v5 = " setPackagesSuspended suspend: "; // const-string v5, " setPackagesSuspended suspend: "
(( java.lang.StringBuilder ) v4 ).append ( v5 ); // invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v4 ).append ( v2 ); // invoke-virtual {v4, v2}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v4 ).toString ( ); // invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
android.util.Slog .d ( v3,v4 );
/* :try_end_0 */
/* .catch Landroid/os/RemoteException; {:try_start_0 ..:try_end_0} :catch_0 */
/* .catchall {:try_start_0 ..:try_end_0} :catchall_0 */
/* .line 1011 */
/* nop */
} // .end local v2 # "suspend":Z
android.os.Binder .restoreCallingIdentity ( v0,v1 );
/* .line 1012 */
/* nop */
/* .line 1014 */
return;
/* .line 1011 */
/* :catchall_0 */
/* move-exception v2 */
/* .line 1008 */
/* :catch_0 */
/* move-exception v2 */
/* .line 1009 */
/* .local v2, "e":Landroid/os/RemoteException; */
try { // :try_start_1
(( android.os.RemoteException ) v2 ).rethrowFromSystemServer ( ); // invoke-virtual {v2}, Landroid/os/RemoteException;->rethrowFromSystemServer()Ljava/lang/RuntimeException;
} // .end local v0 # "token":J
} // .end local p0 # "this":Lcom/miui/server/BackupManagerService;
} // .end local p1 # "packageName":Ljava/lang/String;
} // .end local p2 # "userId":I
} // .end local p3 # "newState":I
} // .end local p4 # "flags":I
/* throw v3 */
/* :try_end_1 */
/* .catchall {:try_start_1 ..:try_end_1} :catchall_0 */
/* .line 1011 */
} // .end local v2 # "e":Landroid/os/RemoteException;
/* .restart local v0 # "token":J */
/* .restart local p0 # "this":Lcom/miui/server/BackupManagerService; */
/* .restart local p1 # "packageName":Ljava/lang/String; */
/* .restart local p2 # "userId":I */
/* .restart local p3 # "newState":I */
/* .restart local p4 # "flags":I */
} // :goto_1
android.os.Binder .restoreCallingIdentity ( v0,v1 );
/* .line 1012 */
/* throw v2 */
} // .end method
private void waitForTheLastWorkingTask ( ) {
/* .locals 2 */
/* .line 565 */
v0 = this.mTaskLatch;
if ( v0 != null) { // if-eqz v0, :cond_1
/* .line 566 */
/* monitor-enter v0 */
/* .line 567 */
} // :goto_0
try { // :try_start_0
v1 = this.mTaskLatch;
if ( v1 != null) { // if-eqz v1, :cond_0
v1 = (( java.util.concurrent.atomic.AtomicBoolean ) v1 ).get ( ); // invoke-virtual {v1}, Ljava/util/concurrent/atomic/AtomicBoolean;->get()Z
/* :try_end_0 */
/* .catchall {:try_start_0 ..:try_end_0} :catchall_0 */
/* if-nez v1, :cond_0 */
/* .line 569 */
try { // :try_start_1
v1 = this.mTaskLatch;
(( java.lang.Object ) v1 ).wait ( ); // invoke-virtual {v1}, Ljava/lang/Object;->wait()V
/* :try_end_1 */
/* .catch Ljava/lang/InterruptedException; {:try_start_1 ..:try_end_1} :catch_0 */
/* .catchall {:try_start_1 ..:try_end_1} :catchall_0 */
/* .line 572 */
} // :goto_1
/* .line 570 */
/* :catch_0 */
/* move-exception v1 */
/* .line 571 */
/* .local v1, "e":Ljava/lang/InterruptedException; */
try { // :try_start_2
(( java.lang.InterruptedException ) v1 ).printStackTrace ( ); // invoke-virtual {v1}, Ljava/lang/InterruptedException;->printStackTrace()V
} // .end local v1 # "e":Ljava/lang/InterruptedException;
/* .line 574 */
} // :cond_0
/* monitor-exit v0 */
/* :catchall_0 */
/* move-exception v1 */
/* monitor-exit v0 */
/* :try_end_2 */
/* .catchall {:try_start_2 ..:try_end_2} :catchall_0 */
/* throw v1 */
/* .line 576 */
} // :cond_1
} // :goto_2
return;
} // .end method
private void waitUntilAppKilled ( java.lang.String p0 ) {
/* .locals 10 */
/* .param p1, "pkg" # Ljava/lang/String; */
/* .line 653 */
/* const/16 v0, 0x14 */
/* .line 654 */
/* .local v0, "MAX_ROUND":I */
int v1 = 0; // const/4 v1, 0x0
/* .line 655 */
/* .local v1, "round":I */
int v2 = 1; // const/4 v2, 0x1
/* .line 656 */
/* .local v2, "killed":Z */
v3 = this.mContext;
final String v4 = "activity"; // const-string v4, "activity"
(( android.content.Context ) v3 ).getSystemService ( v4 ); // invoke-virtual {v3, v4}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;
/* check-cast v3, Landroid/app/ActivityManager; */
/* .line 658 */
/* .local v3, "am":Landroid/app/ActivityManager; */
} // :goto_0
int v2 = 1; // const/4 v2, 0x1
/* .line 659 */
(( android.app.ActivityManager ) v3 ).getRunningAppProcesses ( ); // invoke-virtual {v3}, Landroid/app/ActivityManager;->getRunningAppProcesses()Ljava/util/List;
/* .line 660 */
/* .local v4, "procInfos":Ljava/util/List;, "Ljava/util/List<Landroid/app/ActivityManager$RunningAppProcessInfo;>;" */
v6 = } // :goto_1
if ( v6 != null) { // if-eqz v6, :cond_2
/* check-cast v6, Landroid/app/ActivityManager$RunningAppProcessInfo; */
/* .line 661 */
/* .local v6, "procInfo":Landroid/app/ActivityManager$RunningAppProcessInfo; */
v7 = this.processName;
v7 = (( java.lang.String ) v7 ).equals ( p1 ); // invoke-virtual {v7, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z
/* if-nez v7, :cond_0 */
v7 = this.processName;
/* new-instance v8, Ljava/lang/StringBuilder; */
/* invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V */
(( java.lang.StringBuilder ) v8 ).append ( p1 ); // invoke-virtual {v8, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
final String v9 = ":"; // const-string v9, ":"
(( java.lang.StringBuilder ) v8 ).append ( v9 ); // invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v8 ).toString ( ); // invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
v7 = (( java.lang.String ) v7 ).startsWith ( v8 ); // invoke-virtual {v7, v8}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z
if ( v7 != null) { // if-eqz v7, :cond_1
} // :cond_0
/* iget v7, p0, Lcom/miui/server/BackupManagerService;->mAppUserId:I */
/* iget v8, v6, Landroid/app/ActivityManager$RunningAppProcessInfo;->uid:I */
/* .line 662 */
v8 = android.os.UserHandle .getUserId ( v8 );
/* if-ne v7, v8, :cond_1 */
/* .line 663 */
int v2 = 0; // const/4 v2, 0x0
/* .line 664 */
/* .line 666 */
} // .end local v6 # "procInfo":Landroid/app/ActivityManager$RunningAppProcessInfo;
} // :cond_1
/* .line 667 */
} // :cond_2
} // :goto_2
final String v5 = "Backup:BackupManagerService"; // const-string v5, "Backup:BackupManagerService"
if ( v2 != null) { // if-eqz v2, :cond_3
/* .line 668 */
/* .line 672 */
} // :cond_3
/* const-wide/16 v6, 0x1f4 */
try { // :try_start_0
java.lang.Thread .sleep ( v6,v7 );
/* :try_end_0 */
/* .catch Ljava/lang/InterruptedException; {:try_start_0 ..:try_end_0} :catch_0 */
/* .line 676 */
/* nop */
/* .line 677 */
} // .end local v4 # "procInfos":Ljava/util/List;, "Ljava/util/List<Landroid/app/ActivityManager$RunningAppProcessInfo;>;"
/* add-int/lit8 v4, v1, 0x1 */
} // .end local v1 # "round":I
/* .local v4, "round":I */
/* const/16 v6, 0x14 */
/* if-lt v1, v6, :cond_4 */
/* move v1, v4 */
} // :cond_4
/* move v1, v4 */
/* .line 673 */
/* .restart local v1 # "round":I */
/* .local v4, "procInfos":Ljava/util/List;, "Ljava/util/List<Landroid/app/ActivityManager$RunningAppProcessInfo;>;" */
/* :catch_0 */
/* move-exception v6 */
/* .line 674 */
/* .local v6, "e":Ljava/lang/InterruptedException; */
final String v7 = "interrupted while waiting"; // const-string v7, "interrupted while waiting"
android.util.Slog .e ( v5,v7,v6 );
/* .line 675 */
/* nop */
/* .line 679 */
} // .end local v4 # "procInfos":Ljava/util/List;, "Ljava/util/List<Landroid/app/ActivityManager$RunningAppProcessInfo;>;"
} // .end local v6 # "e":Ljava/lang/InterruptedException;
} // :goto_3
if ( v2 != null) { // if-eqz v2, :cond_5
/* .line 680 */
/* new-instance v4, Ljava/lang/StringBuilder; */
/* invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V */
final String v6 = "app: "; // const-string v6, "app: "
(( java.lang.StringBuilder ) v4 ).append ( v6 ); // invoke-virtual {v4, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v4 ).append ( p1 ); // invoke-virtual {v4, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
final String v6 = " is killed.continue our routine."; // const-string v6, " is killed.continue our routine."
(( java.lang.StringBuilder ) v4 ).append ( v6 ); // invoke-virtual {v4, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v4 ).toString ( ); // invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
android.util.Slog .i ( v5,v4 );
/* .line 682 */
} // :cond_5
/* new-instance v4, Ljava/lang/StringBuilder; */
/* invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V */
final String v6 = "continue while app: "; // const-string v6, "continue while app: "
(( java.lang.StringBuilder ) v4 ).append ( v6 ); // invoke-virtual {v4, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v4 ).append ( p1 ); // invoke-virtual {v4, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
final String v6 = " is still alive!"; // const-string v6, " is still alive!"
(( java.lang.StringBuilder ) v4 ).append ( v6 ); // invoke-virtual {v4, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v4 ).toString ( ); // invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
android.util.Slog .w ( v5,v4 );
/* .line 684 */
} // :goto_4
return;
} // .end method
/* # virtual methods */
public Boolean acquire ( miui.app.backup.IBackupServiceStateObserver p0, android.os.IBinder p1 ) {
/* .locals 5 */
/* .param p1, "stateObserver" # Lmiui/app/backup/IBackupServiceStateObserver; */
/* .param p2, "iCaller" # Landroid/os/IBinder; */
/* .annotation system Ldalvik/annotation/Throws; */
/* value = { */
/* Landroid/os/RemoteException; */
/* } */
} // .end annotation
/* .line 475 */
int v0 = 0; // const/4 v0, 0x0
/* if-nez p2, :cond_0 */
/* .line 476 */
final String v1 = "Backup:BackupManagerService"; // const-string v1, "Backup:BackupManagerService"
final String v2 = "caller should not be null"; // const-string v2, "caller should not be null"
android.util.Slog .w ( v1,v2 );
/* .line 477 */
/* .line 480 */
} // :cond_0
v1 = this.mContext;
final String v2 = "android.permission.BACKUP"; // const-string v2, "android.permission.BACKUP"
v3 = android.os.Binder .getCallingPid ( );
v4 = android.os.Binder .getCallingUid ( );
v1 = (( android.content.Context ) v1 ).checkPermission ( v2, v3, v4 ); // invoke-virtual {v1, v2, v3, v4}, Landroid/content/Context;->checkPermission(Ljava/lang/String;II)I
int v2 = -1; // const/4 v2, -0x1
/* if-ne v1, v2, :cond_1 */
/* .line 481 */
final String v1 = "Backup:BackupManagerService"; // const-string v1, "Backup:BackupManagerService"
/* new-instance v2, Ljava/lang/StringBuilder; */
/* invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V */
final String v3 = "Uid "; // const-string v3, "Uid "
(( java.lang.StringBuilder ) v2 ).append ( v3 ); // invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
v3 = android.os.Binder .getCallingUid ( );
(( java.lang.StringBuilder ) v2 ).append ( v3 ); // invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;
final String v3 = " has no permission to call acquire "; // const-string v3, " has no permission to call acquire "
(( java.lang.StringBuilder ) v2 ).append ( v3 ); // invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v2 ).toString ( ); // invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
android.util.Slog .w ( v1,v2 );
/* .line 482 */
/* .line 485 */
} // :cond_1
final String v1 = "Backup:BackupManagerService"; // const-string v1, "Backup:BackupManagerService"
/* new-instance v3, Ljava/lang/StringBuilder; */
/* invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V */
final String v4 = "Client tries to acquire service.CallingPid="; // const-string v4, "Client tries to acquire service.CallingPid="
(( java.lang.StringBuilder ) v3 ).append ( v4 ); // invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
v4 = android.os.Binder .getCallingPid ( );
(( java.lang.StringBuilder ) v3 ).append ( v4 ); // invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;
final String v4 = " mOwnerPid="; // const-string v4, " mOwnerPid="
(( java.lang.StringBuilder ) v3 ).append ( v4 ); // invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
/* iget v4, p0, Lcom/miui/server/BackupManagerService;->mOwnerPid:I */
(( java.lang.StringBuilder ) v3 ).append ( v4 ); // invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v3 ).toString ( ); // invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
android.util.Slog .d ( v1,v3 );
/* .line 486 */
/* monitor-enter p0 */
/* .line 487 */
try { // :try_start_0
final String v1 = "Backup:BackupManagerService"; // const-string v1, "Backup:BackupManagerService"
final String v3 = "Client acquire service."; // const-string v3, "Client acquire service."
android.util.Slog .d ( v1,v3 );
/* .line 488 */
/* iget v1, p0, Lcom/miui/server/BackupManagerService;->mOwnerPid:I */
/* if-ne v1, v2, :cond_2 */
/* .line 489 */
v1 = android.os.Binder .getCallingPid ( );
/* iput v1, p0, Lcom/miui/server/BackupManagerService;->mOwnerPid:I */
/* .line 490 */
this.mICaller = p2;
/* .line 491 */
v1 = this.mDeathLinker;
/* .line 492 */
/* const-class v0, Lcom/miui/app/smartpower/SmartPowerServiceInternal; */
com.android.server.LocalServices .getService ( v0 );
/* check-cast v0, Lcom/miui/app/smartpower/SmartPowerServiceInternal; */
/* .line 493 */
v1 = android.os.Binder .getCallingUid ( );
/* iget v2, p0, Lcom/miui/server/BackupManagerService;->mOwnerPid:I */
/* .line 492 */
int v3 = 1; // const/4 v3, 0x1
/* .line 494 */
/* monitor-exit p0 */
/* .line 496 */
} // :cond_2
if ( p1 != null) { // if-eqz p1, :cond_3
/* .line 497 */
v1 = this.mStateObservers;
(( android.os.RemoteCallbackList ) v1 ).register ( p1 ); // invoke-virtual {v1, p1}, Landroid/os/RemoteCallbackList;->register(Landroid/os/IInterface;)Z
/* .line 499 */
} // :cond_3
/* monitor-exit p0 */
/* .line 501 */
/* :catchall_0 */
/* move-exception v0 */
/* monitor-exit p0 */
/* :try_end_0 */
/* .catchall {:try_start_0 ..:try_end_0} :catchall_0 */
/* throw v0 */
} // .end method
public void addCompletedSize ( Long p0 ) {
/* .locals 10 */
/* .param p1, "size" # J */
/* .line 359 */
/* iget-wide v0, p0, Lcom/miui/server/BackupManagerService;->mCurrentCompletedSize:J */
/* add-long v6, v0, p1 */
/* iput-wide v6, p0, Lcom/miui/server/BackupManagerService;->mCurrentCompletedSize:J */
/* .line 360 */
/* iget v0, p0, Lcom/miui/server/BackupManagerService;->mProgType:I */
/* if-nez v0, :cond_0 */
v2 = this.mBackupRestoreObserver;
if ( v2 != null) { // if-eqz v2, :cond_0
/* .line 362 */
try { // :try_start_0
v3 = this.mCurrentWorkingPkg;
/* iget v4, p0, Lcom/miui/server/BackupManagerService;->mCurrentWorkingFeature:I */
int v5 = 0; // const/4 v5, 0x0
/* iget-wide v8, p0, Lcom/miui/server/BackupManagerService;->mCurrentTotalSize:J */
/* invoke-interface/range {v2 ..v9}, Lmiui/app/backup/IPackageBackupRestoreObserver;->onCustomProgressChange(Ljava/lang/String;IIJJ)V */
/* :try_end_0 */
/* .catch Landroid/os/RemoteException; {:try_start_0 ..:try_end_0} :catch_0 */
/* .line 366 */
/* .line 364 */
/* :catch_0 */
/* move-exception v0 */
/* .line 365 */
/* .local v0, "e":Landroid/os/RemoteException; */
(( android.os.RemoteException ) v0 ).printStackTrace ( ); // invoke-virtual {v0}, Landroid/os/RemoteException;->printStackTrace()V
/* .line 368 */
} // .end local v0 # "e":Landroid/os/RemoteException;
} // :cond_0
} // :goto_0
return;
} // .end method
public void backupPackage ( android.os.ParcelFileDescriptor p0, android.os.ParcelFileDescriptor p1, java.lang.String p2, Integer p3, java.lang.String p4, java.lang.String p5, Boolean p6, Boolean p7, Boolean p8, Boolean p9, miui.app.backup.IPackageBackupRestoreObserver p10 ) {
/* .locals 16 */
/* .param p1, "outFileDescriptor" # Landroid/os/ParcelFileDescriptor; */
/* .param p2, "readSide" # Landroid/os/ParcelFileDescriptor; */
/* .param p3, "pkg" # Ljava/lang/String; */
/* .param p4, "feature" # I */
/* .param p5, "pwd" # Ljava/lang/String; */
/* .param p6, "encryptedPwd" # Ljava/lang/String; */
/* .param p7, "includeApk" # Z */
/* .param p8, "forceBackup" # Z */
/* .param p9, "shouldSkipData" # Z */
/* .param p10, "isXSpace" # Z */
/* .param p11, "observer" # Lmiui/app/backup/IPackageBackupRestoreObserver; */
/* .annotation system Ldalvik/annotation/Throws; */
/* value = { */
/* Landroid/os/RemoteException; */
/* } */
} // .end annotation
/* .line 186 */
/* move-object/from16 v1, p0 */
/* move-object/from16 v2, p1 */
/* move-object/from16 v3, p3 */
/* move/from16 v4, p4 */
/* move/from16 v5, p7 */
/* move/from16 v6, p9 */
/* move/from16 v7, p10 */
/* move-object/from16 v8, p11 */
this.mBackupRestoreObserver = v8;
/* .line 188 */
v9 = android.os.Binder .getCallingPid ( );
/* .line 189 */
/* .local v9, "pid":I */
/* iget v0, v1, Lcom/miui/server/BackupManagerService;->mOwnerPid:I */
/* const/16 v10, 0x9 */
/* if-eq v9, v0, :cond_0 */
/* .line 190 */
final String v0 = "Backup:BackupManagerService"; // const-string v0, "Backup:BackupManagerService"
final String v11 = "You must acquire first to use the backup or restore service"; // const-string v11, "You must acquire first to use the backup or restore service"
android.util.Slog .e ( v0,v11 );
/* .line 191 */
(( com.miui.server.BackupManagerService ) v1 ).errorOccur ( v10 ); // invoke-virtual {v1, v10}, Lcom/miui/server/BackupManagerService;->errorOccur(I)V
/* .line 192 */
return;
/* .line 195 */
} // :cond_0
v0 = this.mICaller;
/* if-nez v0, :cond_1 */
/* .line 196 */
final String v0 = "Backup:BackupManagerService"; // const-string v0, "Backup:BackupManagerService"
final String v11 = "Caller is null You must acquire first with a binder"; // const-string v11, "Caller is null You must acquire first with a binder"
android.util.Slog .e ( v0,v11 );
/* .line 197 */
(( com.miui.server.BackupManagerService ) v1 ).errorOccur ( v10 ); // invoke-virtual {v1, v10}, Lcom/miui/server/BackupManagerService;->errorOccur(I)V
/* .line 198 */
return;
/* .line 201 */
} // :cond_1
v0 = /* invoke-static/range {p5 ..p5}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z */
/* if-nez v0, :cond_2 */
v0 = /* invoke-static/range {p6 ..p6}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z */
/* if-nez v0, :cond_2 */
/* .line 202 */
/* move-object/from16 v10, p5 */
this.mPwd = v10;
/* .line 203 */
/* move-object/from16 v11, p6 */
this.mEncryptedPwd = v11;
/* .line 201 */
} // :cond_2
/* move-object/from16 v10, p5 */
/* move-object/from16 v11, p6 */
/* .line 206 */
} // :goto_0
v0 = this.mContext;
/* invoke-direct {v1, v0}, Lcom/miui/server/BackupManagerService;->getDefaultIme(Landroid/content/Context;)Ljava/lang/String; */
/* .line 208 */
/* .local v12, "defaultIme":Ljava/lang/String; */
v0 = this.mContext;
v13 = miui.app.backup.BackupManager .isSysAppForBackup ( v0,v3 );
/* .line 209 */
/* .local v13, "isSystemApp":Z */
final String v0 = "Backup:BackupManagerService"; // const-string v0, "Backup:BackupManagerService"
/* new-instance v14, Ljava/lang/StringBuilder; */
/* invoke-direct {v14}, Ljava/lang/StringBuilder;-><init>()V */
final String v15 = "backupPackage: pkg="; // const-string v15, "backupPackage: pkg="
(( java.lang.StringBuilder ) v14 ).append ( v15 ); // invoke-virtual {v14, v15}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v14 ).append ( v3 ); // invoke-virtual {v14, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
final String v15 = " feature="; // const-string v15, " feature="
(( java.lang.StringBuilder ) v14 ).append ( v15 ); // invoke-virtual {v14, v15}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v14 ).append ( v4 ); // invoke-virtual {v14, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;
final String v15 = " includeApk="; // const-string v15, " includeApk="
(( java.lang.StringBuilder ) v14 ).append ( v15 ); // invoke-virtual {v14, v15}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v14 ).append ( v5 ); // invoke-virtual {v14, v5}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;
final String v15 = " shouldSkipData="; // const-string v15, " shouldSkipData="
(( java.lang.StringBuilder ) v14 ).append ( v15 ); // invoke-virtual {v14, v15}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v14 ).append ( v6 ); // invoke-virtual {v14, v6}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;
final String v15 = " isXSpace="; // const-string v15, " isXSpace="
(( java.lang.StringBuilder ) v14 ).append ( v15 ); // invoke-virtual {v14, v15}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v14 ).append ( v7 ); // invoke-virtual {v14, v7}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;
final String v15 = " isSystemApp="; // const-string v15, " isSystemApp="
(( java.lang.StringBuilder ) v14 ).append ( v15 ); // invoke-virtual {v14, v15}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v14 ).append ( v13 ); // invoke-virtual {v14, v13}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v14 ).toString ( ); // invoke-virtual {v14}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
android.util.Slog .v ( v0,v14 );
/* .line 212 */
int v0 = 0; // const/4 v0, 0x0
if ( v7 != null) { // if-eqz v7, :cond_3
/* const/16 v14, 0x3e7 */
} // :cond_3
/* move v14, v0 */
} // :goto_1
/* iput v14, v1, Lcom/miui/server/BackupManagerService;->mAppUserId:I */
/* .line 213 */
int v15 = -1; // const/4 v15, -0x1
/* iput v15, v1, Lcom/miui/server/BackupManagerService;->mPackageLastEnableState:I */
/* .line 214 */
this.mCurrentWorkingPkg = v3;
/* .line 215 */
/* if-nez v13, :cond_4 */
/* .line 216 */
/* invoke-direct {v1, v3, v14}, Lcom/miui/server/BackupManagerService;->disablePackageAndWait(Ljava/lang/String;I)V */
/* .line 219 */
} // :cond_4
this.mOutputFile = v2;
/* .line 220 */
/* iput-boolean v6, v1, Lcom/miui/server/BackupManagerService;->mShouldSkipData:Z */
/* .line 222 */
/* iput v4, v1, Lcom/miui/server/BackupManagerService;->mCurrentWorkingFeature:I */
/* .line 223 */
/* iput v0, v1, Lcom/miui/server/BackupManagerService;->mLastError:I */
/* .line 224 */
/* iput v0, v1, Lcom/miui/server/BackupManagerService;->mProgType:I */
/* .line 226 */
int v14 = 1; // const/4 v14, 0x1
/* iput v14, v1, Lcom/miui/server/BackupManagerService;->mState:I */
/* .line 228 */
/* const-wide/16 v14, 0x0 */
/* iput-wide v14, v1, Lcom/miui/server/BackupManagerService;->mCurrentCompletedSize:J */
/* .line 229 */
/* const-wide/16 v14, -0x1 */
/* iput-wide v14, v1, Lcom/miui/server/BackupManagerService;->mCurrentTotalSize:J */
/* .line 230 */
v14 = this.mContext;
v15 = this.mPackageManager;
/* iget v0, v1, Lcom/miui/server/BackupManagerService;->mAppUserId:I */
v4 = this.mPackageStatsObserver;
com.miui.server.BackupManagerServiceProxy .getPackageSizeInfo ( v14,v15,v3,v0,v4 );
/* .line 231 */
/* monitor-enter p0 */
/* .line 232 */
try { // :try_start_0
/* new-instance v0, Ljava/util/concurrent/atomic/AtomicBoolean; */
int v4 = 0; // const/4 v4, 0x0
/* invoke-direct {v0, v4}, Ljava/util/concurrent/atomic/AtomicBoolean;-><init>(Z)V */
this.mTaskLatch = v0;
/* .line 233 */
v0 = /* invoke-virtual/range {p1 ..p1}, Landroid/os/ParcelFileDescriptor;->getFd()I */
/* iput v0, v1, Lcom/miui/server/BackupManagerService;->mCallerFd:I */
/* .line 234 */
final String v0 = "Backup:BackupManagerService"; // const-string v0, "Backup:BackupManagerService"
/* new-instance v4, Ljava/lang/StringBuilder; */
/* invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V */
final String v14 = "backupPackage, MIUI FD is "; // const-string v14, "backupPackage, MIUI FD is "
(( java.lang.StringBuilder ) v4 ).append ( v14 ); // invoke-virtual {v4, v14}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
/* iget v14, v1, Lcom/miui/server/BackupManagerService;->mCallerFd:I */
(( java.lang.StringBuilder ) v4 ).append ( v14 ); // invoke-virtual {v4, v14}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v4 ).toString ( ); // invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
android.util.Slog .d ( v0,v4 );
/* .line 235 */
/* monitor-exit p0 */
/* :try_end_0 */
/* .catchall {:try_start_0 ..:try_end_0} :catchall_2 */
/* .line 236 */
v4 = this.mTaskLatch;
/* monitor-enter v4 */
/* .line 238 */
try { // :try_start_1
/* iget v0, v1, Lcom/miui/server/BackupManagerService;->mOwnerPid:I */
int v14 = -1; // const/4 v14, -0x1
/* if-eq v0, v14, :cond_5 */
/* .line 239 */
/* filled-new-array/range {p3 ..p3}, [Ljava/lang/String; */
com.miui.server.BackupManagerServiceProxy .fullBackup ( v2,v0,v5 );
/* .line 241 */
} // :cond_5
/* const/16 v0, 0xa */
(( com.miui.server.BackupManagerService ) v1 ).errorOccur ( v0 ); // invoke-virtual {v1, v0}, Lcom/miui/server/BackupManagerService;->errorOccur(I)V
/* :try_end_1 */
/* .catchall {:try_start_1 ..:try_end_1} :catchall_0 */
/* .line 244 */
} // :goto_2
try { // :try_start_2
final String v0 = "Backup:BackupManagerService"; // const-string v0, "Backup:BackupManagerService"
/* new-instance v14, Ljava/lang/StringBuilder; */
/* invoke-direct {v14}, Ljava/lang/StringBuilder;-><init>()V */
final String v15 = "backupPackage finish, MIUI FD is "; // const-string v15, "backupPackage finish, MIUI FD is "
(( java.lang.StringBuilder ) v14 ).append ( v15 ); // invoke-virtual {v14, v15}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
/* iget v15, v1, Lcom/miui/server/BackupManagerService;->mCallerFd:I */
(( java.lang.StringBuilder ) v14 ).append ( v15 ); // invoke-virtual {v14, v15}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v14 ).toString ( ); // invoke-virtual {v14}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
android.util.Slog .d ( v0,v14 );
/* .line 245 */
v0 = this.mTaskLatch;
int v14 = 1; // const/4 v14, 0x1
(( java.util.concurrent.atomic.AtomicBoolean ) v0 ).set ( v14 ); // invoke-virtual {v0, v14}, Ljava/util/concurrent/atomic/AtomicBoolean;->set(Z)V
/* .line 246 */
v0 = this.mTaskLatch;
(( java.lang.Object ) v0 ).notifyAll ( ); // invoke-virtual {v0}, Ljava/lang/Object;->notifyAll()V
/* .line 247 */
/* nop */
/* .line 248 */
/* monitor-exit v4 */
/* :try_end_2 */
/* .catchall {:try_start_2 ..:try_end_2} :catchall_1 */
/* .line 250 */
/* if-nez v13, :cond_6 */
/* .line 251 */
/* iget v0, v1, Lcom/miui/server/BackupManagerService;->mAppUserId:I */
/* invoke-direct {v1, v3, v0, v12}, Lcom/miui/server/BackupManagerService;->enablePackage(Ljava/lang/String;ILjava/lang/String;)V */
/* .line 254 */
} // :cond_6
int v0 = 0; // const/4 v0, 0x0
this.mPwd = v0;
/* .line 255 */
this.mEncryptedPwd = v0;
/* .line 256 */
this.mTaskLatch = v0;
/* .line 257 */
int v4 = -1; // const/4 v4, -0x1
/* iput v4, v1, Lcom/miui/server/BackupManagerService;->mCallerFd:I */
/* .line 258 */
this.mOutputFile = v0;
/* .line 259 */
int v0 = 0; // const/4 v0, 0x0
/* iput v0, v1, Lcom/miui/server/BackupManagerService;->mProgType:I */
/* .line 260 */
/* iput v0, v1, Lcom/miui/server/BackupManagerService;->mState:I */
/* .line 261 */
/* iput v4, v1, Lcom/miui/server/BackupManagerService;->mPackageLastEnableState:I */
/* .line 262 */
/* const-wide/16 v14, -0x1 */
/* iput-wide v14, v1, Lcom/miui/server/BackupManagerService;->mCurrentTotalSize:J */
/* .line 263 */
/* const-wide/16 v14, 0x0 */
/* iput-wide v14, v1, Lcom/miui/server/BackupManagerService;->mCurrentCompletedSize:J */
/* .line 264 */
v4 = this.mCurrentWorkingPkg;
this.mPreviousWorkingPkg = v4;
/* .line 265 */
/* iput v0, v1, Lcom/miui/server/BackupManagerService;->mAppUserId:I */
/* .line 266 */
return;
/* .line 244 */
/* :catchall_0 */
/* move-exception v0 */
try { // :try_start_3
final String v14 = "Backup:BackupManagerService"; // const-string v14, "Backup:BackupManagerService"
/* new-instance v15, Ljava/lang/StringBuilder; */
/* invoke-direct {v15}, Ljava/lang/StringBuilder;-><init>()V */
final String v2 = "backupPackage finish, MIUI FD is "; // const-string v2, "backupPackage finish, MIUI FD is "
(( java.lang.StringBuilder ) v15 ).append ( v2 ); // invoke-virtual {v15, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
/* iget v15, v1, Lcom/miui/server/BackupManagerService;->mCallerFd:I */
(( java.lang.StringBuilder ) v2 ).append ( v15 ); // invoke-virtual {v2, v15}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v2 ).toString ( ); // invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
android.util.Slog .d ( v14,v2 );
/* .line 245 */
v2 = this.mTaskLatch;
int v14 = 1; // const/4 v14, 0x1
(( java.util.concurrent.atomic.AtomicBoolean ) v2 ).set ( v14 ); // invoke-virtual {v2, v14}, Ljava/util/concurrent/atomic/AtomicBoolean;->set(Z)V
/* .line 246 */
v2 = this.mTaskLatch;
(( java.lang.Object ) v2 ).notifyAll ( ); // invoke-virtual {v2}, Ljava/lang/Object;->notifyAll()V
/* .line 247 */
/* nop */
} // .end local v9 # "pid":I
} // .end local v12 # "defaultIme":Ljava/lang/String;
} // .end local v13 # "isSystemApp":Z
} // .end local p0 # "this":Lcom/miui/server/BackupManagerService;
} // .end local p1 # "outFileDescriptor":Landroid/os/ParcelFileDescriptor;
} // .end local p2 # "readSide":Landroid/os/ParcelFileDescriptor;
} // .end local p3 # "pkg":Ljava/lang/String;
} // .end local p4 # "feature":I
} // .end local p5 # "pwd":Ljava/lang/String;
} // .end local p6 # "encryptedPwd":Ljava/lang/String;
} // .end local p7 # "includeApk":Z
} // .end local p8 # "forceBackup":Z
} // .end local p9 # "shouldSkipData":Z
} // .end local p10 # "isXSpace":Z
} // .end local p11 # "observer":Lmiui/app/backup/IPackageBackupRestoreObserver;
/* throw v0 */
/* .line 248 */
/* .restart local v9 # "pid":I */
/* .restart local v12 # "defaultIme":Ljava/lang/String; */
/* .restart local v13 # "isSystemApp":Z */
/* .restart local p0 # "this":Lcom/miui/server/BackupManagerService; */
/* .restart local p1 # "outFileDescriptor":Landroid/os/ParcelFileDescriptor; */
/* .restart local p2 # "readSide":Landroid/os/ParcelFileDescriptor; */
/* .restart local p3 # "pkg":Ljava/lang/String; */
/* .restart local p4 # "feature":I */
/* .restart local p5 # "pwd":Ljava/lang/String; */
/* .restart local p6 # "encryptedPwd":Ljava/lang/String; */
/* .restart local p7 # "includeApk":Z */
/* .restart local p8 # "forceBackup":Z */
/* .restart local p9 # "shouldSkipData":Z */
/* .restart local p10 # "isXSpace":Z */
/* .restart local p11 # "observer":Lmiui/app/backup/IPackageBackupRestoreObserver; */
/* :catchall_1 */
/* move-exception v0 */
/* monitor-exit v4 */
/* :try_end_3 */
/* .catchall {:try_start_3 ..:try_end_3} :catchall_1 */
/* throw v0 */
/* .line 235 */
/* :catchall_2 */
/* move-exception v0 */
try { // :try_start_4
/* monitor-exit p0 */
/* :try_end_4 */
/* .catchall {:try_start_4 ..:try_end_4} :catchall_2 */
/* throw v0 */
} // .end method
public void delCacheBackup ( ) {
/* .locals 3 */
/* .line 948 */
v0 = android.os.Binder .getCallingUid ( );
/* .line 949 */
/* .local v0, "uid":I */
/* const/16 v1, 0x17d6 */
/* if-eq v0, v1, :cond_0 */
/* const/16 v1, 0x17d4 */
/* if-ne v0, v1, :cond_1 */
/* .line 950 */
} // :cond_0
/* new-instance v1, Ljava/io/File; */
final String v2 = "/cache/backup"; // const-string v2, "/cache/backup"
/* invoke-direct {v1, v2}, Ljava/io/File;-><init>(Ljava/lang/String;)V */
/* .line 951 */
/* .local v1, "file":Ljava/io/File; */
android.os.FileUtils .deleteContents ( v1 );
/* .line 953 */
} // .end local v1 # "file":Ljava/io/File;
} // :cond_1
return;
} // .end method
public void errorOccur ( Integer p0 ) {
/* .locals 3 */
/* .param p1, "err" # I */
/* .annotation system Ldalvik/annotation/Throws; */
/* value = { */
/* Landroid/os/RemoteException; */
/* } */
} // .end annotation
/* .line 790 */
/* iget v0, p0, Lcom/miui/server/BackupManagerService;->mLastError:I */
/* if-nez v0, :cond_0 */
/* .line 791 */
/* iput p1, p0, Lcom/miui/server/BackupManagerService;->mLastError:I */
/* .line 792 */
v0 = this.mBackupRestoreObserver;
if ( v0 != null) { // if-eqz v0, :cond_0
/* .line 793 */
v1 = this.mCurrentWorkingPkg;
/* iget v2, p0, Lcom/miui/server/BackupManagerService;->mCurrentWorkingFeature:I */
/* .line 796 */
} // :cond_0
return;
} // .end method
public Integer getAppUserId ( ) {
/* .locals 1 */
/* .line 829 */
/* iget v0, p0, Lcom/miui/server/BackupManagerService;->mAppUserId:I */
} // .end method
public Integer getBackupTimeoutScale ( ) {
/* .locals 2 */
/* .annotation system Ldalvik/annotation/Throws; */
/* value = { */
/* Landroid/os/RemoteException; */
/* } */
} // .end annotation
/* .line 815 */
v0 = this.mCurrentWorkingPkg;
/* iget v1, p0, Lcom/miui/server/BackupManagerService;->mCurrentWorkingFeature:I */
v0 = miui.app.backup.BackupManager .isProgRecordApp ( v0,v1 );
if ( v0 != null) { // if-eqz v0, :cond_0
/* .line 816 */
int v0 = 6; // const/4 v0, 0x6
/* .line 818 */
} // :cond_0
int v0 = 1; // const/4 v0, 0x1
} // .end method
public java.lang.String getCurrentRunningPackage ( ) {
/* .locals 1 */
/* .annotation system Ldalvik/annotation/Throws; */
/* value = { */
/* Landroid/os/RemoteException; */
/* } */
} // .end annotation
/* .line 800 */
v0 = this.mCurrentWorkingPkg;
} // .end method
public Integer getCurrentWorkingFeature ( ) {
/* .locals 1 */
/* .annotation system Ldalvik/annotation/Throws; */
/* value = { */
/* Landroid/os/RemoteException; */
/* } */
} // .end annotation
/* .line 805 */
/* iget v0, p0, Lcom/miui/server/BackupManagerService;->mCurrentWorkingFeature:I */
} // .end method
public Integer getState ( ) {
/* .locals 1 */
/* .annotation system Ldalvik/annotation/Throws; */
/* value = { */
/* Landroid/os/RemoteException; */
/* } */
} // .end annotation
/* .line 810 */
/* iget v0, p0, Lcom/miui/server/BackupManagerService;->mState:I */
} // .end method
public Boolean isCanceling ( ) {
/* .locals 1 */
/* .annotation system Ldalvik/annotation/Throws; */
/* value = { */
/* Landroid/os/RemoteException; */
/* } */
} // .end annotation
/* .line 957 */
/* iget-boolean v0, p0, Lcom/miui/server/BackupManagerService;->mIsCanceling:Z */
} // .end method
public Boolean isNeedBeKilled ( java.lang.String p0 ) {
/* .locals 2 */
/* .param p1, "pkg" # Ljava/lang/String; */
/* .annotation system Ldalvik/annotation/Throws; */
/* value = { */
/* Landroid/os/RemoteException; */
/* } */
} // .end annotation
/* .line 379 */
v0 = this.mNeedBeKilledPkgs;
(( java.util.HashMap ) v0 ).get ( p1 ); // invoke-virtual {v0, p1}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;
/* check-cast v0, Ljava/lang/Boolean; */
/* .line 380 */
/* .local v0, "isKilled":Ljava/lang/Boolean; */
if ( v0 != null) { // if-eqz v0, :cond_0
v1 = (( java.lang.Boolean ) v0 ).booleanValue ( ); // invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z
} // :cond_0
int v1 = 1; // const/4 v1, 0x1
} // :goto_0
} // .end method
public Boolean isRunningFromMiui ( Integer p0 ) {
/* .locals 1 */
/* .param p1, "fd" # I */
/* .annotation system Ldalvik/annotation/Throws; */
/* value = { */
/* Landroid/os/RemoteException; */
/* } */
} // .end annotation
/* .line 385 */
/* iget v0, p0, Lcom/miui/server/BackupManagerService;->mCallerFd:I */
/* if-ne v0, p1, :cond_0 */
int v0 = 1; // const/4 v0, 0x1
} // :cond_0
int v0 = 0; // const/4 v0, 0x0
} // :goto_0
} // .end method
public Boolean isServiceIdle ( ) {
/* .locals 1 */
/* .line 390 */
/* iget v0, p0, Lcom/miui/server/BackupManagerService;->mState:I */
/* if-nez v0, :cond_0 */
int v0 = 1; // const/4 v0, 0x1
} // :cond_0
int v0 = 0; // const/4 v0, 0x0
} // :goto_0
} // .end method
public void onApkInstalled ( ) {
/* .locals 2 */
/* .line 782 */
v0 = this.mContext;
v1 = this.mCurrentWorkingPkg;
v0 = miui.app.backup.BackupManager .isSysAppForBackup ( v0,v1 );
/* if-nez v0, :cond_0 */
/* .line 783 */
/* new-instance v0, Ljava/lang/StringBuilder; */
/* invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V */
final String v1 = "onApkInstalled, mCurrentWorkingPkg:"; // const-string v1, "onApkInstalled, mCurrentWorkingPkg:"
(( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
v1 = this.mCurrentWorkingPkg;
(( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v0 ).toString ( ); // invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
final String v1 = "Backup:BackupManagerService"; // const-string v1, "Backup:BackupManagerService"
android.util.Slog .d ( v1,v0 );
/* .line 784 */
v0 = this.mCurrentWorkingPkg;
/* iget v1, p0, Lcom/miui/server/BackupManagerService;->mAppUserId:I */
/* invoke-direct {p0, v0, v1}, Lcom/miui/server/BackupManagerService;->disablePackageAndWait(Ljava/lang/String;I)V */
/* .line 786 */
} // :cond_0
return;
} // .end method
public void readMiuiBackupHeader ( android.os.ParcelFileDescriptor p0 ) {
/* .locals 6 */
/* .param p1, "inFileDescriptor" # Landroid/os/ParcelFileDescriptor; */
/* .line 325 */
int v0 = 0; // const/4 v0, 0x0
/* .line 327 */
/* .local v0, "in":Ljava/io/InputStream; */
try { // :try_start_0
/* new-instance v1, Ljava/io/FileInputStream; */
(( android.os.ParcelFileDescriptor ) p1 ).getFileDescriptor ( ); // invoke-virtual {p1}, Landroid/os/ParcelFileDescriptor;->getFileDescriptor()Ljava/io/FileDescriptor;
/* invoke-direct {v1, v2}, Ljava/io/FileInputStream;-><init>(Ljava/io/FileDescriptor;)V */
/* move-object v0, v1 */
/* .line 329 */
miui.app.backup.BackupFileResolver .readMiuiHeader ( v0 );
/* :try_end_0 */
/* .catchall {:try_start_0 ..:try_end_0} :catchall_0 */
/* .line 330 */
/* .local v1, "header":Lmiui/app/backup/BackupFileResolver$BackupFileMiuiHeader; */
final String v2 = "Backup:BackupManagerService"; // const-string v2, "Backup:BackupManagerService"
if ( v1 != null) { // if-eqz v1, :cond_2
try { // :try_start_1
/* iget v3, v1, Lmiui/app/backup/BackupFileResolver$BackupFileMiuiHeader;->version:I */
int v4 = 2; // const/4 v4, 0x2
/* if-ne v3, v4, :cond_2 */
/* .line 331 */
v3 = this.packageName;
this.mCurrentWorkingPkg = v3;
/* .line 332 */
/* iget v3, v1, Lmiui/app/backup/BackupFileResolver$BackupFileMiuiHeader;->featureId:I */
/* iput v3, p0, Lcom/miui/server/BackupManagerService;->mCurrentWorkingFeature:I */
/* .line 333 */
/* iget v3, v1, Lmiui/app/backup/BackupFileResolver$BackupFileMiuiHeader;->userId:I */
/* iput v3, p0, Lcom/miui/server/BackupManagerService;->mAppUserId:I */
/* .line 334 */
/* iget-boolean v3, v1, Lmiui/app/backup/BackupFileResolver$BackupFileMiuiHeader;->isEncrypted:Z */
if ( v3 != null) { // if-eqz v3, :cond_0
int v3 = 0; // const/4 v3, 0x0
} // :cond_0
v3 = this.encryptedPwd;
} // :goto_0
this.mEncryptedPwdInBakFile = v3;
/* .line 336 */
v3 = this.mContext;
v4 = this.mCurrentWorkingPkg;
v3 = miui.app.backup.BackupManager .isSysAppForBackup ( v3,v4 );
/* .line 337 */
/* .local v3, "isSystemApp":Z */
/* new-instance v4, Ljava/lang/StringBuilder; */
/* invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V */
final String v5 = "readMiuiBackupHeader, BackupFileMiuiHeader:"; // const-string v5, "readMiuiBackupHeader, BackupFileMiuiHeader:"
(( java.lang.StringBuilder ) v4 ).append ( v5 ); // invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v4 ).append ( v1 ); // invoke-virtual {v4, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;
final String v5 = " isSystemApp:"; // const-string v5, " isSystemApp:"
(( java.lang.StringBuilder ) v4 ).append ( v5 ); // invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v4 ).append ( v3 ); // invoke-virtual {v4, v3}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;
final String v5 = " mAppUserId:"; // const-string v5, " mAppUserId:"
(( java.lang.StringBuilder ) v4 ).append ( v5 ); // invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
/* iget v5, p0, Lcom/miui/server/BackupManagerService;->mAppUserId:I */
(( java.lang.StringBuilder ) v4 ).append ( v5 ); // invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v4 ).toString ( ); // invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
android.util.Slog .d ( v2,v4 );
/* .line 339 */
/* if-nez v3, :cond_1 */
/* .line 340 */
v2 = this.mCurrentWorkingPkg;
/* iget v4, p0, Lcom/miui/server/BackupManagerService;->mAppUserId:I */
/* invoke-direct {p0, v2, v4}, Lcom/miui/server/BackupManagerService;->disablePackageAndWait(Ljava/lang/String;I)V */
/* .line 342 */
} // .end local v3 # "isSystemApp":Z
} // :cond_1
/* .line 344 */
} // :cond_2
/* new-instance v3, Ljava/lang/StringBuilder; */
/* invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V */
final String v4 = "readMiuiBackupHeader is error, header="; // const-string v4, "readMiuiBackupHeader is error, header="
(( java.lang.StringBuilder ) v3 ).append ( v4 ); // invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v3 ).append ( v1 ); // invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v3 ).toString ( ); // invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
android.util.Slog .e ( v2,v3 );
/* :try_end_1 */
/* .catchall {:try_start_1 ..:try_end_1} :catchall_0 */
/* .line 347 */
} // .end local v1 # "header":Lmiui/app/backup/BackupFileResolver$BackupFileMiuiHeader;
} // :goto_1
/* nop */
/* .line 349 */
try { // :try_start_2
(( java.io.InputStream ) v0 ).close ( ); // invoke-virtual {v0}, Ljava/io/InputStream;->close()V
/* :try_end_2 */
/* .catch Ljava/io/IOException; {:try_start_2 ..:try_end_2} :catch_0 */
/* .line 352 */
/* nop */
/* .line 355 */
return;
/* .line 350 */
/* :catch_0 */
/* move-exception v1 */
/* .line 351 */
/* .local v1, "e":Ljava/io/IOException; */
/* new-instance v2, Ljava/lang/RuntimeException; */
/* invoke-direct {v2, v1}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/Throwable;)V */
/* throw v2 */
/* .line 347 */
} // .end local v1 # "e":Ljava/io/IOException;
/* :catchall_0 */
/* move-exception v1 */
if ( v0 != null) { // if-eqz v0, :cond_3
/* .line 349 */
try { // :try_start_3
(( java.io.InputStream ) v0 ).close ( ); // invoke-virtual {v0}, Ljava/io/InputStream;->close()V
/* :try_end_3 */
/* .catch Ljava/io/IOException; {:try_start_3 ..:try_end_3} :catch_1 */
/* .line 352 */
/* .line 350 */
/* :catch_1 */
/* move-exception v1 */
/* .line 351 */
/* .restart local v1 # "e":Ljava/io/IOException; */
/* new-instance v2, Ljava/lang/RuntimeException; */
/* invoke-direct {v2, v1}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/Throwable;)V */
/* throw v2 */
/* .line 354 */
} // .end local v1 # "e":Ljava/io/IOException;
} // :cond_3
} // :goto_2
/* throw v1 */
} // .end method
public void release ( miui.app.backup.IBackupServiceStateObserver p0 ) {
/* .locals 4 */
/* .param p1, "stateObserver" # Lmiui/app/backup/IBackupServiceStateObserver; */
/* .annotation system Ldalvik/annotation/Throws; */
/* value = { */
/* Landroid/os/RemoteException; */
/* } */
} // .end annotation
/* .line 506 */
final String v0 = "Backup:BackupManagerService"; // const-string v0, "Backup:BackupManagerService"
/* new-instance v1, Ljava/lang/StringBuilder; */
/* invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V */
final String v2 = "Client tries to release service.CallingPid="; // const-string v2, "Client tries to release service.CallingPid="
(( java.lang.StringBuilder ) v1 ).append ( v2 ); // invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
v2 = android.os.Binder .getCallingPid ( );
(( java.lang.StringBuilder ) v1 ).append ( v2 ); // invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;
final String v2 = " mOwnerPid="; // const-string v2, " mOwnerPid="
(( java.lang.StringBuilder ) v1 ).append ( v2 ); // invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
/* iget v2, p0, Lcom/miui/server/BackupManagerService;->mOwnerPid:I */
(( java.lang.StringBuilder ) v1 ).append ( v2 ); // invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v1 ).toString ( ); // invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
android.util.Slog .d ( v0,v1 );
/* .line 507 */
/* monitor-enter p0 */
/* .line 508 */
try { // :try_start_0
final String v0 = "Backup:BackupManagerService"; // const-string v0, "Backup:BackupManagerService"
final String v1 = "Client release service.Start canceling..."; // const-string v1, "Client release service.Start canceling..."
android.util.Slog .d ( v0,v1 );
/* .line 509 */
if ( p1 != null) { // if-eqz p1, :cond_0
/* .line 510 */
v0 = this.mStateObservers;
(( android.os.RemoteCallbackList ) v0 ).unregister ( p1 ); // invoke-virtual {v0, p1}, Landroid/os/RemoteCallbackList;->unregister(Landroid/os/IInterface;)Z
/* .line 513 */
} // :cond_0
v0 = android.os.Binder .getCallingPid ( );
/* .line 514 */
/* .local v0, "pid":I */
/* iget v1, p0, Lcom/miui/server/BackupManagerService;->mOwnerPid:I */
/* if-ne v0, v1, :cond_1 */
/* .line 515 */
int v1 = 1; // const/4 v1, 0x1
/* iput-boolean v1, p0, Lcom/miui/server/BackupManagerService;->mIsCanceling:Z */
/* .line 516 */
/* invoke-direct {p0}, Lcom/miui/server/BackupManagerService;->scheduleReleaseResource()V */
/* .line 517 */
/* invoke-direct {p0}, Lcom/miui/server/BackupManagerService;->waitForTheLastWorkingTask()V */
/* .line 518 */
int v1 = 0; // const/4 v1, 0x0
/* iput-boolean v1, p0, Lcom/miui/server/BackupManagerService;->mIsCanceling:Z */
/* .line 520 */
int v2 = -1; // const/4 v2, -0x1
/* iput v2, p0, Lcom/miui/server/BackupManagerService;->mOwnerPid:I */
/* .line 521 */
v2 = this.mICaller;
v3 = this.mDeathLinker;
/* .line 522 */
int v2 = 0; // const/4 v2, 0x0
this.mICaller = v2;
/* .line 523 */
this.mPreviousWorkingPkg = v2;
/* :try_end_0 */
/* .catchall {:try_start_0 ..:try_end_0} :catchall_0 */
/* .line 525 */
try { // :try_start_1
/* invoke-direct {p0}, Lcom/miui/server/BackupManagerService;->broadcastServiceIdle()V */
/* :try_end_1 */
/* .catch Landroid/os/RemoteException; {:try_start_1 ..:try_end_1} :catch_0 */
/* .catchall {:try_start_1 ..:try_end_1} :catchall_0 */
/* .line 528 */
/* .line 526 */
/* :catch_0 */
/* move-exception v2 */
/* .line 527 */
/* .local v2, "e":Landroid/os/RemoteException; */
try { // :try_start_2
(( android.os.RemoteException ) v2 ).printStackTrace ( ); // invoke-virtual {v2}, Landroid/os/RemoteException;->printStackTrace()V
/* .line 529 */
} // .end local v2 # "e":Landroid/os/RemoteException;
} // :goto_0
/* const-class v2, Lcom/miui/app/smartpower/SmartPowerServiceInternal; */
com.android.server.LocalServices .getService ( v2 );
/* check-cast v2, Lcom/miui/app/smartpower/SmartPowerServiceInternal; */
/* .line 530 */
v3 = android.os.Binder .getCallingUid ( );
/* .line 529 */
/* .line 532 */
} // :cond_1
final String v1 = "Backup:BackupManagerService"; // const-string v1, "Backup:BackupManagerService"
final String v2 = "Client release service.Cancel completed!"; // const-string v2, "Client release service.Cancel completed!"
android.util.Slog .d ( v1,v2 );
/* .line 533 */
/* nop */
} // .end local v0 # "pid":I
/* monitor-exit p0 */
/* .line 534 */
return;
/* .line 533 */
/* :catchall_0 */
/* move-exception v0 */
/* monitor-exit p0 */
/* :try_end_2 */
/* .catchall {:try_start_2 ..:try_end_2} :catchall_0 */
/* throw v0 */
} // .end method
public void restoreFile ( android.os.ParcelFileDescriptor p0, java.lang.String p1, Boolean p2, miui.app.backup.IPackageBackupRestoreObserver p3 ) {
/* .locals 9 */
/* .param p1, "bakFd" # Landroid/os/ParcelFileDescriptor; */
/* .param p2, "pwd" # Ljava/lang/String; */
/* .param p3, "forceBackup" # Z */
/* .param p4, "observer" # Lmiui/app/backup/IPackageBackupRestoreObserver; */
/* .annotation system Ldalvik/annotation/Throws; */
/* value = { */
/* Landroid/os/RemoteException; */
/* } */
} // .end annotation
/* .line 414 */
this.mBackupRestoreObserver = p4;
/* .line 416 */
v0 = com.miui.server.BackupManagerService .getCallingPid ( );
/* .line 417 */
/* .local v0, "pid":I */
/* iget v1, p0, Lcom/miui/server/BackupManagerService;->mOwnerPid:I */
/* const/16 v2, 0x9 */
/* if-eq v0, v1, :cond_0 */
/* .line 418 */
final String v1 = "Backup:BackupManagerService"; // const-string v1, "Backup:BackupManagerService"
final String v3 = "You must acquire first to use the backup or restore service"; // const-string v3, "You must acquire first to use the backup or restore service"
android.util.Slog .e ( v1,v3 );
/* .line 419 */
(( com.miui.server.BackupManagerService ) p0 ).errorOccur ( v2 ); // invoke-virtual {p0, v2}, Lcom/miui/server/BackupManagerService;->errorOccur(I)V
/* .line 420 */
return;
/* .line 423 */
} // :cond_0
v1 = this.mICaller;
/* if-nez v1, :cond_1 */
/* .line 424 */
final String v1 = "Backup:BackupManagerService"; // const-string v1, "Backup:BackupManagerService"
final String v3 = "Caller is null You must acquire first with a binder"; // const-string v3, "Caller is null You must acquire first with a binder"
android.util.Slog .e ( v1,v3 );
/* .line 425 */
(( com.miui.server.BackupManagerService ) p0 ).errorOccur ( v2 ); // invoke-virtual {p0, v2}, Lcom/miui/server/BackupManagerService;->errorOccur(I)V
/* .line 426 */
return;
/* .line 429 */
} // :cond_1
v1 = this.mContext;
/* invoke-direct {p0, v1}, Lcom/miui/server/BackupManagerService;->getDefaultIme(Landroid/content/Context;)Ljava/lang/String; */
/* .line 431 */
/* .local v1, "defaultIme":Ljava/lang/String; */
this.mPwd = p2;
/* .line 433 */
int v2 = 0; // const/4 v2, 0x0
/* iput v2, p0, Lcom/miui/server/BackupManagerService;->mLastError:I */
/* .line 434 */
/* iput v2, p0, Lcom/miui/server/BackupManagerService;->mProgType:I */
/* .line 435 */
int v3 = -1; // const/4 v3, -0x1
/* iput v3, p0, Lcom/miui/server/BackupManagerService;->mPackageLastEnableState:I */
/* .line 436 */
int v4 = 2; // const/4 v4, 0x2
/* iput v4, p0, Lcom/miui/server/BackupManagerService;->mState:I */
/* .line 438 */
(( android.os.ParcelFileDescriptor ) p1 ).getStatSize ( ); // invoke-virtual {p1}, Landroid/os/ParcelFileDescriptor;->getStatSize()J
/* move-result-wide v4 */
/* iput-wide v4, p0, Lcom/miui/server/BackupManagerService;->mCurrentTotalSize:J */
/* .line 439 */
/* const-wide/16 v4, 0x0 */
/* iput-wide v4, p0, Lcom/miui/server/BackupManagerService;->mCurrentCompletedSize:J */
/* .line 440 */
/* monitor-enter p0 */
/* .line 441 */
try { // :try_start_0
/* new-instance v4, Ljava/util/concurrent/atomic/AtomicBoolean; */
/* invoke-direct {v4, v2}, Ljava/util/concurrent/atomic/AtomicBoolean;-><init>(Z)V */
this.mTaskLatch = v4;
/* .line 442 */
v4 = (( android.os.ParcelFileDescriptor ) p1 ).getFd ( ); // invoke-virtual {p1}, Landroid/os/ParcelFileDescriptor;->getFd()I
/* iput v4, p0, Lcom/miui/server/BackupManagerService;->mCallerFd:I */
/* .line 443 */
final String v4 = "Backup:BackupManagerService"; // const-string v4, "Backup:BackupManagerService"
/* new-instance v5, Ljava/lang/StringBuilder; */
/* invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V */
final String v6 = "restoreFile, MIUI FD is "; // const-string v6, "restoreFile, MIUI FD is "
(( java.lang.StringBuilder ) v5 ).append ( v6 ); // invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
/* iget v6, p0, Lcom/miui/server/BackupManagerService;->mCallerFd:I */
(( java.lang.StringBuilder ) v5 ).append ( v6 ); // invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v5 ).toString ( ); // invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
android.util.Slog .d ( v4,v5 );
/* .line 444 */
/* monitor-exit p0 */
/* :try_end_0 */
/* .catchall {:try_start_0 ..:try_end_0} :catchall_2 */
/* .line 445 */
v4 = this.mTaskLatch;
/* monitor-enter v4 */
/* .line 447 */
int v5 = 1; // const/4 v5, 0x1
try { // :try_start_1
/* iget v6, p0, Lcom/miui/server/BackupManagerService;->mOwnerPid:I */
/* if-eq v6, v3, :cond_2 */
/* .line 448 */
com.miui.server.BackupManagerServiceProxy .fullRestore ( p1 );
/* .line 450 */
} // :cond_2
/* const/16 v6, 0xa */
(( com.miui.server.BackupManagerService ) p0 ).errorOccur ( v6 ); // invoke-virtual {p0, v6}, Lcom/miui/server/BackupManagerService;->errorOccur(I)V
/* :try_end_1 */
/* .catchall {:try_start_1 ..:try_end_1} :catchall_0 */
/* .line 453 */
} // :goto_0
try { // :try_start_2
final String v6 = "Backup:BackupManagerService"; // const-string v6, "Backup:BackupManagerService"
/* new-instance v7, Ljava/lang/StringBuilder; */
/* invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V */
final String v8 = "restoreFile finish, MIUI FD is "; // const-string v8, "restoreFile finish, MIUI FD is "
(( java.lang.StringBuilder ) v7 ).append ( v8 ); // invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
/* iget v8, p0, Lcom/miui/server/BackupManagerService;->mCallerFd:I */
(( java.lang.StringBuilder ) v7 ).append ( v8 ); // invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v7 ).toString ( ); // invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
android.util.Slog .d ( v6,v7 );
/* .line 454 */
v6 = this.mTaskLatch;
(( java.util.concurrent.atomic.AtomicBoolean ) v6 ).set ( v5 ); // invoke-virtual {v6, v5}, Ljava/util/concurrent/atomic/AtomicBoolean;->set(Z)V
/* .line 455 */
v5 = this.mTaskLatch;
(( java.lang.Object ) v5 ).notifyAll ( ); // invoke-virtual {v5}, Ljava/lang/Object;->notifyAll()V
/* .line 456 */
/* nop */
/* .line 457 */
/* monitor-exit v4 */
/* :try_end_2 */
/* .catchall {:try_start_2 ..:try_end_2} :catchall_1 */
/* .line 458 */
v4 = this.mContext;
v5 = this.mCurrentWorkingPkg;
v4 = miui.app.backup.BackupManager .isSysAppForBackup ( v4,v5 );
/* if-nez v4, :cond_3 */
/* .line 459 */
v4 = this.mCurrentWorkingPkg;
/* iget v5, p0, Lcom/miui/server/BackupManagerService;->mAppUserId:I */
/* invoke-direct {p0, v4, v5, v1}, Lcom/miui/server/BackupManagerService;->enablePackage(Ljava/lang/String;ILjava/lang/String;)V */
/* .line 461 */
} // :cond_3
int v4 = 0; // const/4 v4, 0x0
this.mPwd = v4;
/* .line 462 */
this.mEncryptedPwd = v4;
/* .line 463 */
this.mTaskLatch = v4;
/* .line 464 */
/* iput v3, p0, Lcom/miui/server/BackupManagerService;->mCallerFd:I */
/* .line 465 */
/* iput v2, p0, Lcom/miui/server/BackupManagerService;->mProgType:I */
/* .line 466 */
/* iput v2, p0, Lcom/miui/server/BackupManagerService;->mState:I */
/* .line 467 */
/* iput v3, p0, Lcom/miui/server/BackupManagerService;->mPackageLastEnableState:I */
/* .line 468 */
/* const-wide/16 v3, -0x1 */
/* iput-wide v3, p0, Lcom/miui/server/BackupManagerService;->mCurrentTotalSize:J */
/* .line 469 */
v3 = this.mCurrentWorkingPkg;
this.mPreviousWorkingPkg = v3;
/* .line 470 */
/* iput v2, p0, Lcom/miui/server/BackupManagerService;->mAppUserId:I */
/* .line 471 */
return;
/* .line 453 */
/* :catchall_0 */
/* move-exception v2 */
try { // :try_start_3
final String v3 = "Backup:BackupManagerService"; // const-string v3, "Backup:BackupManagerService"
/* new-instance v6, Ljava/lang/StringBuilder; */
/* invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V */
final String v7 = "restoreFile finish, MIUI FD is "; // const-string v7, "restoreFile finish, MIUI FD is "
(( java.lang.StringBuilder ) v6 ).append ( v7 ); // invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
/* iget v7, p0, Lcom/miui/server/BackupManagerService;->mCallerFd:I */
(( java.lang.StringBuilder ) v6 ).append ( v7 ); // invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v6 ).toString ( ); // invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
android.util.Slog .d ( v3,v6 );
/* .line 454 */
v3 = this.mTaskLatch;
(( java.util.concurrent.atomic.AtomicBoolean ) v3 ).set ( v5 ); // invoke-virtual {v3, v5}, Ljava/util/concurrent/atomic/AtomicBoolean;->set(Z)V
/* .line 455 */
v3 = this.mTaskLatch;
(( java.lang.Object ) v3 ).notifyAll ( ); // invoke-virtual {v3}, Ljava/lang/Object;->notifyAll()V
/* .line 456 */
/* nop */
} // .end local v0 # "pid":I
} // .end local v1 # "defaultIme":Ljava/lang/String;
} // .end local p0 # "this":Lcom/miui/server/BackupManagerService;
} // .end local p1 # "bakFd":Landroid/os/ParcelFileDescriptor;
} // .end local p2 # "pwd":Ljava/lang/String;
} // .end local p3 # "forceBackup":Z
} // .end local p4 # "observer":Lmiui/app/backup/IPackageBackupRestoreObserver;
/* throw v2 */
/* .line 457 */
/* .restart local v0 # "pid":I */
/* .restart local v1 # "defaultIme":Ljava/lang/String; */
/* .restart local p0 # "this":Lcom/miui/server/BackupManagerService; */
/* .restart local p1 # "bakFd":Landroid/os/ParcelFileDescriptor; */
/* .restart local p2 # "pwd":Ljava/lang/String; */
/* .restart local p3 # "forceBackup":Z */
/* .restart local p4 # "observer":Lmiui/app/backup/IPackageBackupRestoreObserver; */
/* :catchall_1 */
/* move-exception v2 */
/* monitor-exit v4 */
/* :try_end_3 */
/* .catchall {:try_start_3 ..:try_end_3} :catchall_1 */
/* throw v2 */
/* .line 444 */
/* :catchall_2 */
/* move-exception v2 */
try { // :try_start_4
/* monitor-exit p0 */
/* :try_end_4 */
/* .catchall {:try_start_4 ..:try_end_4} :catchall_2 */
/* throw v2 */
} // .end method
public void setCustomProgress ( Integer p0, Integer p1, Integer p2 ) {
/* .locals 8 */
/* .param p1, "progType" # I */
/* .param p2, "prog" # I */
/* .param p3, "total" # I */
/* .annotation system Ldalvik/annotation/Throws; */
/* value = { */
/* Landroid/os/RemoteException; */
/* } */
} // .end annotation
/* .line 395 */
/* iput p1, p0, Lcom/miui/server/BackupManagerService;->mProgType:I */
/* .line 396 */
v0 = this.mBackupRestoreObserver;
if ( v0 != null) { // if-eqz v0, :cond_0
/* .line 397 */
v1 = this.mCurrentWorkingPkg;
/* iget v2, p0, Lcom/miui/server/BackupManagerService;->mCurrentWorkingFeature:I */
/* int-to-long v4, p2 */
/* int-to-long v6, p3 */
/* move v3, p1 */
/* invoke-interface/range {v0 ..v7}, Lmiui/app/backup/IPackageBackupRestoreObserver;->onCustomProgressChange(Ljava/lang/String;IIJJ)V */
/* .line 399 */
} // :cond_0
return;
} // .end method
public void setFutureTask ( java.util.List p0 ) {
/* .locals 0 */
/* .annotation system Ldalvik/annotation/Signature; */
/* value = { */
/* "(", */
/* "Ljava/util/List<", */
/* "Ljava/lang/String;", */
/* ">;)V" */
/* } */
} // .end annotation
/* .annotation runtime Ljava/lang/Deprecated; */
} // .end annotation
/* .line 272 */
/* .local p1, "packages":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;" */
return;
} // .end method
public void setIsNeedBeKilled ( java.lang.String p0, Boolean p1 ) {
/* .locals 2 */
/* .param p1, "pkg" # Ljava/lang/String; */
/* .param p2, "isNeedBeKilled" # Z */
/* .annotation system Ldalvik/annotation/Throws; */
/* value = { */
/* Landroid/os/RemoteException; */
/* } */
} // .end annotation
/* .line 373 */
/* new-instance v0, Ljava/lang/StringBuilder; */
/* invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V */
/* const-string/jumbo v1, "setIsNeedBeKilled, pkg=" */
(( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v0 ).append ( p1 ); // invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
final String v1 = ", isNeedBeKilled="; // const-string v1, ", isNeedBeKilled="
(( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v0 ).append ( p2 ); // invoke-virtual {v0, p2}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v0 ).toString ( ); // invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
final String v1 = "Backup:BackupManagerService"; // const-string v1, "Backup:BackupManagerService"
android.util.Slog .d ( v1,v0 );
/* .line 374 */
v0 = this.mNeedBeKilledPkgs;
java.lang.Boolean .valueOf ( p2 );
(( java.util.HashMap ) v0 ).put ( p1, v1 ); // invoke-virtual {v0, p1, v1}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;
/* .line 375 */
return;
} // .end method
public Boolean shouldSkipData ( ) {
/* .locals 1 */
/* .line 824 */
/* iget-boolean v0, p0, Lcom/miui/server/BackupManagerService;->mShouldSkipData:Z */
} // .end method
public void startConfirmationUi ( Integer p0, java.lang.String p1 ) {
/* .locals 4 */
/* .param p1, "token" # I */
/* .param p2, "action" # Ljava/lang/String; */
/* .annotation system Ldalvik/annotation/Throws; */
/* value = { */
/* Landroid/os/RemoteException; */
/* } */
} // .end annotation
/* .line 290 */
v0 = this.mHandler;
/* new-instance v1, Lcom/miui/server/BackupManagerService$2; */
/* invoke-direct {v1, p0, p1}, Lcom/miui/server/BackupManagerService$2;-><init>(Lcom/miui/server/BackupManagerService;I)V */
/* .line 301 */
v2 = this.mPreviousWorkingPkg;
if ( v2 != null) { // if-eqz v2, :cond_0
v3 = this.mCurrentWorkingPkg;
v2 = (( java.lang.String ) v2 ).equals ( v3 ); // invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z
if ( v2 != null) { // if-eqz v2, :cond_0
/* const-wide/16 v2, 0x5dc */
} // :cond_0
/* const-wide/16 v2, 0x64 */
/* .line 290 */
} // :goto_0
(( android.os.Handler ) v0 ).postDelayed ( v1, v2, v3 ); // invoke-virtual {v0, v1, v2, v3}, Landroid/os/Handler;->postDelayed(Ljava/lang/Runnable;J)Z
/* .line 302 */
return;
} // .end method
public void writeMiuiBackupHeader ( android.os.ParcelFileDescriptor p0 ) {
/* .locals 8 */
/* .param p1, "outFileDescriptor" # Landroid/os/ParcelFileDescriptor; */
/* .line 306 */
int v0 = 0; // const/4 v0, 0x0
/* .line 308 */
/* .local v0, "os":Ljava/io/FileOutputStream; */
try { // :try_start_0
/* new-instance v1, Ljava/io/FileOutputStream; */
(( android.os.ParcelFileDescriptor ) p1 ).getFileDescriptor ( ); // invoke-virtual {p1}, Landroid/os/ParcelFileDescriptor;->getFileDescriptor()Ljava/io/FileDescriptor;
/* invoke-direct {v1, v2}, Ljava/io/FileOutputStream;-><init>(Ljava/io/FileDescriptor;)V */
/* :try_end_0 */
/* .catch Ljava/io/IOException; {:try_start_0 ..:try_end_0} :catch_2 */
/* .catchall {:try_start_0 ..:try_end_0} :catchall_0 */
/* .line 309 */
} // .end local v0 # "os":Ljava/io/FileOutputStream;
/* .local v1, "os":Ljava/io/FileOutputStream; */
try { // :try_start_1
v2 = this.mContext;
v3 = this.mCurrentWorkingPkg;
/* iget v4, p0, Lcom/miui/server/BackupManagerService;->mCurrentWorkingFeature:I */
v5 = this.mEncryptedPwd;
/* iget v6, p0, Lcom/miui/server/BackupManagerService;->mAppUserId:I */
/* invoke-static/range {v1 ..v6}, Lmiui/app/backup/BackupFileResolver;->writeMiuiHeader(Ljava/io/OutputStream;Landroid/content/Context;Ljava/lang/String;ILjava/lang/String;I)V */
/* :try_end_1 */
/* .catch Ljava/io/IOException; {:try_start_1 ..:try_end_1} :catch_1 */
/* .catchall {:try_start_1 ..:try_end_1} :catchall_1 */
/* .line 313 */
/* nop */
/* .line 315 */
try { // :try_start_2
(( java.io.FileOutputStream ) v1 ).close ( ); // invoke-virtual {v1}, Ljava/io/FileOutputStream;->close()V
/* :try_end_2 */
/* .catch Ljava/io/IOException; {:try_start_2 ..:try_end_2} :catch_0 */
/* .line 318 */
/* nop */
/* .line 321 */
return;
/* .line 316 */
/* :catch_0 */
/* move-exception v0 */
/* .line 317 */
/* .local v0, "e":Ljava/io/IOException; */
/* new-instance v2, Ljava/lang/RuntimeException; */
/* invoke-direct {v2, v0}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/Throwable;)V */
/* throw v2 */
/* .line 310 */
} // .end local v0 # "e":Ljava/io/IOException;
/* :catch_1 */
/* move-exception v0 */
/* .line 313 */
} // .end local v1 # "os":Ljava/io/FileOutputStream;
/* .local v0, "os":Ljava/io/FileOutputStream; */
/* :catchall_0 */
/* move-exception v1 */
/* move-object v7, v1 */
/* move-object v1, v0 */
/* move-object v0, v7 */
/* .line 310 */
/* :catch_2 */
/* move-exception v1 */
/* move-object v7, v1 */
/* move-object v1, v0 */
/* move-object v0, v7 */
/* .line 311 */
/* .local v0, "e":Ljava/io/IOException; */
/* .restart local v1 # "os":Ljava/io/FileOutputStream; */
} // :goto_0
try { // :try_start_3
/* new-instance v2, Ljava/lang/RuntimeException; */
/* invoke-direct {v2, v0}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/Throwable;)V */
} // .end local v1 # "os":Ljava/io/FileOutputStream;
} // .end local p0 # "this":Lcom/miui/server/BackupManagerService;
} // .end local p1 # "outFileDescriptor":Landroid/os/ParcelFileDescriptor;
/* throw v2 */
/* :try_end_3 */
/* .catchall {:try_start_3 ..:try_end_3} :catchall_1 */
/* .line 313 */
} // .end local v0 # "e":Ljava/io/IOException;
/* .restart local v1 # "os":Ljava/io/FileOutputStream; */
/* .restart local p0 # "this":Lcom/miui/server/BackupManagerService; */
/* .restart local p1 # "outFileDescriptor":Landroid/os/ParcelFileDescriptor; */
/* :catchall_1 */
/* move-exception v0 */
} // :goto_1
if ( v1 != null) { // if-eqz v1, :cond_0
/* .line 315 */
try { // :try_start_4
(( java.io.FileOutputStream ) v1 ).close ( ); // invoke-virtual {v1}, Ljava/io/FileOutputStream;->close()V
/* :try_end_4 */
/* .catch Ljava/io/IOException; {:try_start_4 ..:try_end_4} :catch_3 */
/* .line 318 */
/* .line 316 */
/* :catch_3 */
/* move-exception v0 */
/* .line 317 */
/* .restart local v0 # "e":Ljava/io/IOException; */
/* new-instance v2, Ljava/lang/RuntimeException; */
/* invoke-direct {v2, v0}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/Throwable;)V */
/* throw v2 */
/* .line 320 */
} // .end local v0 # "e":Ljava/io/IOException;
} // :cond_0
} // :goto_2
/* throw v0 */
} // .end method
