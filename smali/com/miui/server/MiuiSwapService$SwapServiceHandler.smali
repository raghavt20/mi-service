.class final Lcom/miui/server/MiuiSwapService$SwapServiceHandler;
.super Landroid/os/Handler;
.source "MiuiSwapService.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/miui/server/MiuiSwapService;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x12
    name = "SwapServiceHandler"
.end annotation


# instance fields
.field final synthetic this$0:Lcom/miui/server/MiuiSwapService;


# direct methods
.method public constructor <init>(Lcom/miui/server/MiuiSwapService;Landroid/os/Looper;)V
    .locals 0
    .param p2, "looper"    # Landroid/os/Looper;

    .line 71
    iput-object p1, p0, Lcom/miui/server/MiuiSwapService$SwapServiceHandler;->this$0:Lcom/miui/server/MiuiSwapService;

    .line 72
    const/4 p1, 0x0

    invoke-direct {p0, p2, p1}, Landroid/os/Handler;-><init>(Landroid/os/Looper;Landroid/os/Handler$Callback;)V

    .line 73
    return-void
.end method


# virtual methods
.method public handleMessage(Landroid/os/Message;)V
    .locals 2
    .param p1, "msg"    # Landroid/os/Message;

    .line 77
    iget v0, p1, Landroid/os/Message;->what:I

    const-string v1, "persist.sys.swapservice.ctrl"

    packed-switch v0, :pswitch_data_0

    .line 92
    invoke-static {}, Lcom/miui/server/MiuiSwapService;->-$$Nest$sfgetTAG()Ljava/lang/String;

    move-result-object v0

    const-string v1, "fail match message"

    invoke-static {v0, v1}, Landroid/util/Slog;->w(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    .line 86
    :pswitch_0
    iget-object v0, p0, Lcom/miui/server/MiuiSwapService$SwapServiceHandler;->this$0:Lcom/miui/server/MiuiSwapService;

    invoke-static {v0}, Lcom/miui/server/MiuiSwapService;->-$$Nest$fgetmUsbConnect(Lcom/miui/server/MiuiSwapService;)Z

    move-result v0

    if-nez v0, :cond_1

    .line 87
    const/4 v0, 0x1

    invoke-static {v0}, Ljava/lang/Boolean;->toString(Z)Ljava/lang/String;

    move-result-object v0

    invoke-static {v1, v0}, Landroid/os/SystemProperties;->set(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    .line 79
    :pswitch_1
    iget-object v0, p0, Lcom/miui/server/MiuiSwapService$SwapServiceHandler;->this$0:Lcom/miui/server/MiuiSwapService;

    invoke-static {v0}, Lcom/miui/server/MiuiSwapService;->-$$Nest$fgetmUsbConnect(Lcom/miui/server/MiuiSwapService;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 80
    iget-object v0, p0, Lcom/miui/server/MiuiSwapService$SwapServiceHandler;->this$0:Lcom/miui/server/MiuiSwapService;

    invoke-static {v0}, Lcom/miui/server/MiuiSwapService;->-$$Nest$mstartSwapLocked(Lcom/miui/server/MiuiSwapService;)V

    .line 82
    :cond_0
    const/4 v0, 0x0

    invoke-static {v0}, Ljava/lang/Boolean;->toString(Z)Ljava/lang/String;

    move-result-object v0

    invoke-static {v1, v0}, Landroid/os/SystemProperties;->set(Ljava/lang/String;Ljava/lang/String;)V

    .line 83
    nop

    .line 94
    :cond_1
    :goto_0
    return-void

    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_1
        :pswitch_0
    .end packed-switch
.end method
