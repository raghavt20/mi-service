.class Lcom/miui/server/BackupManagerServiceProxy;
.super Ljava/lang/Object;
.source "BackupManagerServiceProxy.java"


# static fields
.field public static final TAG:Ljava/lang/String; = "BackupManagerServiceProxy"


# direct methods
.method constructor <init>()V
    .locals 0

    .line 18
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method static fullBackup(Landroid/os/ParcelFileDescriptor;[Ljava/lang/String;Z)V
    .locals 13
    .param p0, "outFileDescriptor"    # Landroid/os/ParcelFileDescriptor;
    .param p1, "pkgs"    # [Ljava/lang/String;
    .param p2, "includeApk"    # Z
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    .line 21
    const-string v0, "backup"

    invoke-static {v0}, Landroid/os/ServiceManager;->getService(Ljava/lang/String;)Landroid/os/IBinder;

    move-result-object v0

    check-cast v0, Landroid/app/backup/IBackupManager;

    .line 22
    .local v0, "bm":Landroid/app/backup/IBackupManager;
    const/4 v2, 0x0

    const/4 v5, 0x1

    const/4 v6, 0x0

    const/4 v7, 0x0

    const/4 v8, 0x0

    const/4 v9, 0x0

    const/4 v10, 0x0

    const/4 v11, 0x0

    move-object v1, v0

    move-object v3, p0

    move v4, p2

    move-object v12, p1

    invoke-interface/range {v1 .. v12}, Landroid/app/backup/IBackupManager;->adbBackup(ILandroid/os/ParcelFileDescriptor;ZZZZZZZZ[Ljava/lang/String;)V

    .line 23
    return-void
.end method

.method static fullCancel()V
    .locals 2
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    .line 31
    const-string v0, "backup"

    invoke-static {v0}, Landroid/os/ServiceManager;->getService(Ljava/lang/String;)Landroid/os/IBinder;

    move-result-object v0

    check-cast v0, Landroid/app/backup/IBackupManager;

    .line 32
    .local v0, "bm":Landroid/app/backup/IBackupManager;
    const/4 v1, 0x0

    invoke-interface {v0, v1}, Landroid/app/backup/IBackupManager;->cancelMiuiBackupsForUser(I)V

    .line 33
    return-void
.end method

.method static fullRestore(Landroid/os/ParcelFileDescriptor;)V
    .locals 2
    .param p0, "fd"    # Landroid/os/ParcelFileDescriptor;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    .line 26
    const-string v0, "backup"

    invoke-static {v0}, Landroid/os/ServiceManager;->getService(Ljava/lang/String;)Landroid/os/IBinder;

    move-result-object v0

    check-cast v0, Landroid/app/backup/IBackupManager;

    .line 27
    .local v0, "bm":Landroid/app/backup/IBackupManager;
    const/4 v1, 0x0

    invoke-interface {v0, v1, p0}, Landroid/app/backup/IBackupManager;->adbRestore(ILandroid/os/ParcelFileDescriptor;)V

    .line 28
    return-void
.end method

.method public static getPackageSizeInfo(Landroid/content/Context;Landroid/content/pm/PackageManager;Ljava/lang/String;ILandroid/content/pm/IPackageStatsObserver;)V
    .locals 10
    .param p0, "context"    # Landroid/content/Context;
    .param p1, "pm"    # Landroid/content/pm/PackageManager;
    .param p2, "pkg"    # Ljava/lang/String;
    .param p3, "userId"    # I
    .param p4, "observer"    # Landroid/content/pm/IPackageStatsObserver;

    .line 37
    nop

    .line 38
    const-string/jumbo v0, "storagestats"

    invoke-virtual {p0, v0}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/app/usage/StorageStatsManager;

    .line 39
    .local v0, "ssm":Landroid/app/usage/StorageStatsManager;
    invoke-static {}, Landroid/os/Binder;->clearCallingIdentity()J

    move-result-wide v1

    .line 41
    .local v1, "oldId":J
    const/4 v3, 0x0

    :try_start_0
    invoke-virtual {p1, p2, v3, p3}, Landroid/content/pm/PackageManager;->getPackageInfoAsUser(Ljava/lang/String;II)Landroid/content/pm/PackageInfo;

    move-result-object v4

    .line 42
    .local v4, "pInfo":Landroid/content/pm/PackageInfo;
    iget-object v5, v4, Landroid/content/pm/PackageInfo;->applicationInfo:Landroid/content/pm/ApplicationInfo;

    .line 43
    .local v5, "appInfo":Landroid/content/pm/ApplicationInfo;
    iget-object v6, v5, Landroid/content/pm/ApplicationInfo;->storageUuid:Ljava/util/UUID;

    iget-object v7, v5, Landroid/content/pm/ApplicationInfo;->packageName:Ljava/lang/String;

    iget v8, v5, Landroid/content/pm/ApplicationInfo;->uid:I

    .line 44
    invoke-static {v8}, Landroid/os/UserHandle;->getUserHandleForUid(I)Landroid/os/UserHandle;

    move-result-object v8

    .line 43
    invoke-virtual {v0, v6, v7, v8}, Landroid/app/usage/StorageStatsManager;->queryStatsForPackage(Ljava/util/UUID;Ljava/lang/String;Landroid/os/UserHandle;)Landroid/app/usage/StorageStats;

    move-result-object v6

    .line 45
    .local v6, "stats":Landroid/app/usage/StorageStats;
    new-instance v7, Landroid/content/pm/PackageStats;

    iget v8, v5, Landroid/content/pm/ApplicationInfo;->uid:I

    invoke-direct {v7, p2, v8}, Landroid/content/pm/PackageStats;-><init>(Ljava/lang/String;I)V

    .line 46
    .local v7, "legacy":Landroid/content/pm/PackageStats;
    invoke-virtual {v6}, Landroid/app/usage/StorageStats;->getAppBytes()J

    move-result-wide v8

    iput-wide v8, v7, Landroid/content/pm/PackageStats;->codeSize:J

    .line 47
    invoke-virtual {v6}, Landroid/app/usage/StorageStats;->getDataBytes()J

    move-result-wide v8

    iput-wide v8, v7, Landroid/content/pm/PackageStats;->dataSize:J

    .line 48
    invoke-virtual {v6}, Landroid/app/usage/StorageStats;->getCacheBytes()J

    move-result-wide v8

    iput-wide v8, v7, Landroid/content/pm/PackageStats;->cacheSize:J
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_1
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 50
    const/4 v8, 0x1

    :try_start_1
    invoke-interface {p4, v7, v8}, Landroid/content/pm/IPackageStatsObserver;->onGetStatsCompleted(Landroid/content/pm/PackageStats;Z)V
    :try_end_1
    .catch Landroid/os/RemoteException; {:try_start_1 .. :try_end_1} :catch_0
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 52
    goto :goto_0

    .line 51
    :catch_0
    move-exception v3

    goto :goto_0

    .line 61
    .end local v4    # "pInfo":Landroid/content/pm/PackageInfo;
    .end local v5    # "appInfo":Landroid/content/pm/ApplicationInfo;
    .end local v6    # "stats":Landroid/app/usage/StorageStats;
    .end local v7    # "legacy":Landroid/content/pm/PackageStats;
    :catchall_0
    move-exception v3

    goto :goto_1

    .line 53
    :catch_1
    move-exception v4

    .line 54
    .local v4, "e":Ljava/lang/Exception;
    :try_start_2
    const-string v5, "BackupManagerServiceProxy"

    const-string v6, "getPackageSizeInfo error"

    invoke-static {v5, v6, v4}, Landroid/util/Slog;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    .line 56
    :try_start_3
    new-instance v5, Landroid/content/pm/PackageStats;

    invoke-direct {v5, p2, p3}, Landroid/content/pm/PackageStats;-><init>(Ljava/lang/String;I)V

    .line 57
    .local v5, "legacy":Landroid/content/pm/PackageStats;
    invoke-interface {p4, v5, v3}, Landroid/content/pm/IPackageStatsObserver;->onGetStatsCompleted(Landroid/content/pm/PackageStats;Z)V
    :try_end_3
    .catch Landroid/os/RemoteException; {:try_start_3 .. :try_end_3} :catch_2
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    .line 59
    .end local v5    # "legacy":Landroid/content/pm/PackageStats;
    goto :goto_0

    .line 58
    :catch_2
    move-exception v3

    .line 61
    .end local v4    # "e":Ljava/lang/Exception;
    :goto_0
    invoke-static {v1, v2}, Landroid/os/Binder;->restoreCallingIdentity(J)V

    .line 62
    nop

    .line 63
    return-void

    .line 61
    :goto_1
    invoke-static {v1, v2}, Landroid/os/Binder;->restoreCallingIdentity(J)V

    .line 62
    throw v3
.end method

.method public static isPackageStateProtected(Landroid/content/pm/PackageManager;Ljava/lang/String;I)Z
    .locals 3
    .param p0, "pm"    # Landroid/content/pm/PackageManager;
    .param p1, "packageName"    # Ljava/lang/String;
    .param p2, "userId"    # I

    .line 66
    const/4 v0, 0x0

    .line 68
    .local v0, "isProtected":Z
    :try_start_0
    invoke-virtual {p0, p1, p2}, Landroid/content/pm/PackageManager;->isPackageStateProtected(Ljava/lang/String;I)Z

    move-result v1
    :try_end_0
    .catch Ljava/lang/UnsupportedOperationException; {:try_start_0 .. :try_end_0} :catch_0

    move v0, v1

    .line 71
    goto :goto_0

    .line 69
    :catch_0
    move-exception v1

    .line 70
    .local v1, "e":Ljava/lang/UnsupportedOperationException;
    invoke-virtual {v1}, Ljava/lang/UnsupportedOperationException;->printStackTrace()V

    .line 72
    .end local v1    # "e":Ljava/lang/UnsupportedOperationException;
    :goto_0
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "isPackageStateProtected, packageName:"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " userId:"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " isProtected:"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    const-string v2, "BackupManagerServiceProxy"

    invoke-static {v2, v1}, Landroid/util/Slog;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 74
    return v0
.end method
