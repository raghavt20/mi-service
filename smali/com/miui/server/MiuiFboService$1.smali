.class Lcom/miui/server/MiuiFboService$1;
.super Ljava/lang/Object;
.source "MiuiFboService.java"

# interfaces
.implements Ljava/lang/Runnable;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/miui/server/MiuiFboService;->initFboSocket()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# direct methods
.method constructor <init>()V
    .locals 0

    .line 387
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public run()V
    .locals 13

    .line 391
    const/4 v0, 0x0

    :try_start_0
    new-instance v1, Landroid/net/LocalServerSocket;

    const-string v2, "fbs_native_socket"

    invoke-direct {v1, v2}, Landroid/net/LocalServerSocket;-><init>(Ljava/lang/String;)V

    invoke-static {v1}, Lcom/miui/server/MiuiFboService;->-$$Nest$sfputmServerSocket(Landroid/net/LocalServerSocket;)V
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    .line 395
    goto :goto_0

    .line 392
    :catch_0
    move-exception v1

    .line 393
    .local v1, "e":Ljava/io/IOException;
    invoke-virtual {v1}, Ljava/io/IOException;->printStackTrace()V

    .line 394
    invoke-static {v0}, Lcom/miui/server/MiuiFboService;->-$$Nest$sfputmKeepRunning(Z)V

    .line 396
    .end local v1    # "e":Ljava/io/IOException;
    :goto_0
    invoke-static {}, Lcom/miui/server/MiuiFboService;->-$$Nest$sfgetmKeepRunning()Z

    move-result v1

    if-eqz v1, :cond_4

    .line 398
    :try_start_1
    invoke-static {}, Lcom/miui/server/MiuiFboService;->-$$Nest$sfgetmServerSocket()Landroid/net/LocalServerSocket;

    move-result-object v1

    invoke-virtual {v1}, Landroid/net/LocalServerSocket;->accept()Landroid/net/LocalSocket;

    move-result-object v1

    invoke-static {v1}, Lcom/miui/server/MiuiFboService;->-$$Nest$sfputinteractClientSocket(Landroid/net/LocalSocket;)V

    .line 399
    invoke-static {}, Lcom/miui/server/MiuiFboService;->-$$Nest$sfgetinteractClientSocket()Landroid/net/LocalSocket;

    move-result-object v1

    invoke-virtual {v1}, Landroid/net/LocalSocket;->getInputStream()Ljava/io/InputStream;

    move-result-object v1

    invoke-static {v1}, Lcom/miui/server/MiuiFboService;->-$$Nest$sfputinputStream(Ljava/io/InputStream;)V

    .line 400
    invoke-static {}, Lcom/miui/server/MiuiFboService;->-$$Nest$sfgetinteractClientSocket()Landroid/net/LocalSocket;

    move-result-object v1

    invoke-virtual {v1}, Landroid/net/LocalSocket;->getOutputStream()Ljava/io/OutputStream;

    move-result-object v1

    invoke-static {v1}, Lcom/miui/server/MiuiFboService;->-$$Nest$sfputoutputStream(Ljava/io/OutputStream;)V

    .line 401
    const v1, 0x19000

    new-array v1, v1, [B

    .line 402
    .local v1, "bytes":[B
    invoke-static {}, Lcom/miui/server/MiuiFboService;->-$$Nest$sfgetinputStream()Ljava/io/InputStream;

    move-result-object v2

    invoke-virtual {v2, v1}, Ljava/io/InputStream;->read([B)I

    .line 403
    new-instance v2, Ljava/lang/String;

    invoke-direct {v2, v1}, Ljava/lang/String;-><init>([B)V

    .line 404
    .local v2, "dataReceived":Ljava/lang/String;
    const/16 v3, 0x7d

    invoke-virtual {v2, v3}, Ljava/lang/String;->indexOf(I)I

    move-result v3

    invoke-virtual {v2, v0, v3}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v3

    move-object v2, v3

    .line 405
    invoke-static {}, Lcom/miui/server/MiuiFboService;->-$$Nest$sfgetTAG()Ljava/lang/String;

    move-result-object v3

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "Receive data from native:"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Landroid/util/Slog;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 406
    const-string v3, "pkg:"

    invoke-virtual {v2, v3}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v3

    const/4 v4, 0x1

    if-eqz v3, :cond_1

    .line 407
    invoke-static {v4}, Lcom/miui/server/MiuiFboService;->-$$Nest$sfputmFinishedApp(Z)V

    .line 408
    invoke-static {}, Lcom/miui/server/MiuiFboService;->-$$Nest$sfgetsInstance()Lcom/miui/server/MiuiFboService;

    move-result-object v3

    invoke-static {v3, v4}, Lcom/miui/server/MiuiFboService;->-$$Nest$fputcldStrategyStatus(Lcom/miui/server/MiuiFboService;Z)V

    .line 409
    invoke-static {v4}, Lcom/miui/server/MiuiFboService;->-$$Nest$smuseCldStrategy(I)V

    .line 410
    invoke-static {}, Lcom/miui/server/MiuiFboService;->-$$Nest$sfgetsInstance()Lcom/miui/server/MiuiFboService;

    move-result-object v3

    invoke-static {v3, v0}, Lcom/miui/server/MiuiFboService;->-$$Nest$fputcldStrategyStatus(Lcom/miui/server/MiuiFboService;Z)V

    .line 411
    invoke-static {v2}, Lcom/miui/server/MiuiFboService;->-$$Nest$smaggregateBroadcastData(Ljava/lang/String;)V

    .line 412
    invoke-static {}, Lcom/miui/server/MiuiFboService;->-$$Nest$sfgetsInstance()Lcom/miui/server/MiuiFboService;

    move-result-object v3

    invoke-static {}, Lcom/miui/server/MiuiFboService;->-$$Nest$sfgetpackageNameList()Ljava/util/ArrayList;

    move-result-object v5

    invoke-virtual {v5}, Ljava/util/ArrayList;->toString()Ljava/lang/String;

    move-result-object v5

    const-wide/16 v6, 0x0

    invoke-virtual {v3, v5, v4, v6, v7}, Lcom/miui/server/MiuiFboService;->deliverMessage(Ljava/lang/String;IJ)V

    .line 413
    invoke-static {}, Lcom/miui/server/MiuiFboService;->-$$Nest$sfgetlistSize()I

    move-result v3

    if-gez v3, :cond_0

    .line 414
    invoke-static {}, Lcom/miui/server/MiuiFboService;->-$$Nest$sfgetsInstance()Lcom/miui/server/MiuiFboService;

    move-result-object v3

    const-string/jumbo v4, "stop"

    const/4 v5, 0x3

    invoke-virtual {v3, v4, v5, v6, v7}, Lcom/miui/server/MiuiFboService;->deliverMessage(Ljava/lang/String;IJ)V
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_2
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 434
    :cond_0
    :try_start_2
    invoke-static {}, Lcom/miui/server/MiuiFboService;->-$$Nest$sfgetinputStream()Ljava/io/InputStream;

    move-result-object v3

    invoke-virtual {v3}, Ljava/io/InputStream;->close()V

    .line 435
    invoke-static {}, Lcom/miui/server/MiuiFboService;->-$$Nest$sfgetoutputStream()Ljava/io/OutputStream;

    move-result-object v3

    invoke-virtual {v3}, Ljava/io/OutputStream;->close()V
    :try_end_2
    .catch Ljava/io/IOException; {:try_start_2 .. :try_end_2} :catch_1

    .line 438
    goto/16 :goto_0

    .line 436
    :catch_1
    move-exception v3

    .line 437
    .local v3, "e":Ljava/io/IOException;
    invoke-virtual {v3}, Ljava/io/IOException;->printStackTrace()V

    .line 416
    .end local v3    # "e":Ljava/io/IOException;
    goto/16 :goto_0

    .line 418
    :cond_1
    :try_start_3
    invoke-static {}, Lcom/miui/server/MiuiFboService;->-$$Nest$sfgetsInstance()Lcom/miui/server/MiuiFboService;

    move-result-object v3

    invoke-static {v3}, Lcom/miui/server/MiuiFboService;->-$$Nest$fgetmDriverSupport(Lcom/miui/server/MiuiFboService;)Z

    move-result v3

    if-eqz v3, :cond_3

    .line 419
    const/4 v3, 0x2

    invoke-static {v2, v3}, Lcom/miui/server/MiuiFboService;->-$$Nest$smcallHalFunction(Ljava/lang/String;I)Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Ljava/lang/String;

    .line 420
    .local v5, "halReturnData":Ljava/lang/String;
    const/4 v6, 0x0

    .line 421
    .local v6, "split":[Ljava/lang/String;
    if-eqz v5, :cond_2

    .line 422
    const-string v7, ":"

    invoke-virtual {v5, v7}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v7

    move-object v6, v7

    .line 424
    :cond_2
    array-length v7, v6

    sub-int/2addr v7, v3

    aget-object v3, v6, v7

    invoke-static {v3}, Ljava/lang/Long;->parseLong(Ljava/lang/String;)J

    move-result-wide v7

    invoke-static {v7, v8}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v3

    .line 425
    .local v3, "beforeCleanup":Ljava/lang/Long;
    array-length v7, v6

    sub-int/2addr v7, v4

    aget-object v4, v6, v7

    invoke-static {v4}, Ljava/lang/Long;->parseLong(Ljava/lang/String;)J

    move-result-wide v7

    invoke-static {v7, v8}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v4

    .line 426
    .local v4, "afterCleanup":Ljava/lang/Long;
    invoke-static {}, Lcom/miui/server/MiuiFboService;->-$$Nest$sfgetmFragmentCount()Ljava/lang/Long;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/Long;->longValue()J

    move-result-wide v7

    invoke-virtual {v3}, Ljava/lang/Long;->longValue()J

    move-result-wide v9

    invoke-virtual {v4}, Ljava/lang/Long;->longValue()J

    move-result-wide v11

    sub-long/2addr v9, v11

    add-long/2addr v7, v9

    invoke-static {v7, v8}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v7

    invoke-static {v7}, Lcom/miui/server/MiuiFboService;->-$$Nest$sfputmFragmentCount(Ljava/lang/Long;)V

    .line 428
    .end local v3    # "beforeCleanup":Ljava/lang/Long;
    .end local v4    # "afterCleanup":Ljava/lang/Long;
    .end local v5    # "halReturnData":Ljava/lang/String;
    .end local v6    # "split":[Ljava/lang/String;
    :cond_3
    invoke-static {}, Lcom/miui/server/MiuiFboService;->-$$Nest$sfgetoutputStream()Ljava/io/OutputStream;

    move-result-object v3

    const-string/jumbo v4, "{send message to native}"

    invoke-virtual {v4}, Ljava/lang/String;->getBytes()[B

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/io/OutputStream;->write([B)V
    :try_end_3
    .catch Ljava/lang/Exception; {:try_start_3 .. :try_end_3} :catch_2
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    .line 434
    .end local v1    # "bytes":[B
    .end local v2    # "dataReceived":Ljava/lang/String;
    :try_start_4
    invoke-static {}, Lcom/miui/server/MiuiFboService;->-$$Nest$sfgetinputStream()Ljava/io/InputStream;

    move-result-object v1

    invoke-virtual {v1}, Ljava/io/InputStream;->close()V

    .line 435
    invoke-static {}, Lcom/miui/server/MiuiFboService;->-$$Nest$sfgetoutputStream()Ljava/io/OutputStream;

    move-result-object v1

    invoke-virtual {v1}, Ljava/io/OutputStream;->close()V
    :try_end_4
    .catch Ljava/io/IOException; {:try_start_4 .. :try_end_4} :catch_3

    goto :goto_1

    .line 433
    :catchall_0
    move-exception v0

    goto :goto_3

    .line 429
    :catch_2
    move-exception v1

    .line 430
    .local v1, "e":Ljava/lang/Exception;
    :try_start_5
    invoke-virtual {v1}, Ljava/lang/Exception;->printStackTrace()V

    .line 431
    invoke-static {}, Lcom/miui/server/MiuiFboService;->-$$Nest$smwriteFailToHal()V
    :try_end_5
    .catchall {:try_start_5 .. :try_end_5} :catchall_0

    .line 434
    .end local v1    # "e":Ljava/lang/Exception;
    :try_start_6
    invoke-static {}, Lcom/miui/server/MiuiFboService;->-$$Nest$sfgetinputStream()Ljava/io/InputStream;

    move-result-object v1

    invoke-virtual {v1}, Ljava/io/InputStream;->close()V

    .line 435
    invoke-static {}, Lcom/miui/server/MiuiFboService;->-$$Nest$sfgetoutputStream()Ljava/io/OutputStream;

    move-result-object v1

    invoke-virtual {v1}, Ljava/io/OutputStream;->close()V
    :try_end_6
    .catch Ljava/io/IOException; {:try_start_6 .. :try_end_6} :catch_3

    .line 438
    :goto_1
    goto :goto_2

    .line 436
    :catch_3
    move-exception v1

    .line 437
    .local v1, "e":Ljava/io/IOException;
    invoke-virtual {v1}, Ljava/io/IOException;->printStackTrace()V

    .line 439
    .end local v1    # "e":Ljava/io/IOException;
    nop

    :goto_2
    goto/16 :goto_0

    .line 434
    :goto_3
    :try_start_7
    invoke-static {}, Lcom/miui/server/MiuiFboService;->-$$Nest$sfgetinputStream()Ljava/io/InputStream;

    move-result-object v1

    invoke-virtual {v1}, Ljava/io/InputStream;->close()V

    .line 435
    invoke-static {}, Lcom/miui/server/MiuiFboService;->-$$Nest$sfgetoutputStream()Ljava/io/OutputStream;

    move-result-object v1

    invoke-virtual {v1}, Ljava/io/OutputStream;->close()V
    :try_end_7
    .catch Ljava/io/IOException; {:try_start_7 .. :try_end_7} :catch_4

    .line 438
    goto :goto_4

    .line 436
    :catch_4
    move-exception v1

    .line 437
    .restart local v1    # "e":Ljava/io/IOException;
    invoke-virtual {v1}, Ljava/io/IOException;->printStackTrace()V

    .line 439
    .end local v1    # "e":Ljava/io/IOException;
    :goto_4
    throw v0

    .line 441
    :cond_4
    return-void
.end method
