abstract class com.miui.server.UsbManagerConnect implements java.lang.Runnable {
	 /* .source "UsbManagerConnect.java" */
	 /* # interfaces */
	 /* # static fields */
	 static final Integer BUFFER_SIZE;
	 private static final java.lang.String TAG;
	 /* # instance fields */
	 protected final Integer MSG_LOCAL;
	 protected final Integer MSG_SHARE_NET;
	 protected final Integer MSG_TO_PC;
	 java.io.OutputStream mOutputStream;
	 android.net.LocalSocket mSocket;
	 /* # direct methods */
	 com.miui.server.UsbManagerConnect ( ) {
		 /* .locals 1 */
		 /* .line 9 */
		 /* invoke-direct {p0}, Ljava/lang/Object;-><init>()V */
		 /* .line 12 */
		 int v0 = 0; // const/4 v0, 0x0
		 /* iput v0, p0, Lcom/miui/server/UsbManagerConnect;->MSG_TO_PC:I */
		 /* .line 13 */
		 int v0 = 1; // const/4 v0, 0x1
		 /* iput v0, p0, Lcom/miui/server/UsbManagerConnect;->MSG_LOCAL:I */
		 /* .line 14 */
		 int v0 = 2; // const/4 v0, 0x2
		 /* iput v0, p0, Lcom/miui/server/UsbManagerConnect;->MSG_SHARE_NET:I */
		 return;
	 } // .end method
	 /* # virtual methods */
	 synchronized void closeSocket ( ) {
		 /* .locals 4 */
		 /* monitor-enter p0 */
		 /* .line 21 */
		 try { // :try_start_0
			 v0 = this.mOutputStream;
			 /* :try_end_0 */
			 /* .catchall {:try_start_0 ..:try_end_0} :catchall_0 */
			 if ( v0 != null) { // if-eqz v0, :cond_0
				 /* .line 23 */
				 try { // :try_start_1
					 (( java.io.OutputStream ) v0 ).close ( ); // invoke-virtual {v0}, Ljava/io/OutputStream;->close()V
					 /* :try_end_1 */
					 /* .catch Ljava/io/IOException; {:try_start_1 ..:try_end_1} :catch_0 */
					 /* .catchall {:try_start_1 ..:try_end_1} :catchall_0 */
					 /* .line 26 */
					 /* .line 24 */
				 } // .end local p0 # "this":Lcom/miui/server/UsbManagerConnect;
				 /* :catch_0 */
				 /* move-exception v0 */
				 /* .line 25 */
				 /* .local v0, "ex":Ljava/io/IOException; */
				 try { // :try_start_2
					 final String v1 = "UsbManagerConnect"; // const-string v1, "UsbManagerConnect"
					 /* new-instance v2, Ljava/lang/StringBuilder; */
					 /* invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V */
					 final String v3 = "Failed closing output stream: "; // const-string v3, "Failed closing output stream: "
					 (( java.lang.StringBuilder ) v2 ).append ( v3 ); // invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
					 (( java.lang.StringBuilder ) v2 ).append ( v0 ); // invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;
					 (( java.lang.StringBuilder ) v2 ).toString ( ); // invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
					 android.util.Slog .e ( v1,v2 );
					 /* .line 29 */
				 } // .end local v0 # "ex":Ljava/io/IOException;
			 } // :cond_0
		 } // :goto_0
		 v0 = this.mSocket;
		 /* :try_end_2 */
		 /* .catchall {:try_start_2 ..:try_end_2} :catchall_0 */
		 if ( v0 != null) { // if-eqz v0, :cond_1
			 /* .line 31 */
			 try { // :try_start_3
				 (( android.net.LocalSocket ) v0 ).close ( ); // invoke-virtual {v0}, Landroid/net/LocalSocket;->close()V
				 /* :try_end_3 */
				 /* .catch Ljava/io/IOException; {:try_start_3 ..:try_end_3} :catch_1 */
				 /* .catchall {:try_start_3 ..:try_end_3} :catchall_0 */
				 /* .line 34 */
				 /* .line 32 */
				 /* :catch_1 */
				 /* move-exception v0 */
				 /* .line 33 */
				 /* .restart local v0 # "ex":Ljava/io/IOException; */
				 try { // :try_start_4
					 final String v1 = "UsbManagerConnect"; // const-string v1, "UsbManagerConnect"
					 /* new-instance v2, Ljava/lang/StringBuilder; */
					 /* invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V */
					 final String v3 = "Failed closing socket: "; // const-string v3, "Failed closing socket: "
					 (( java.lang.StringBuilder ) v2 ).append ( v3 ); // invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
					 (( java.lang.StringBuilder ) v2 ).append ( v0 ); // invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;
					 (( java.lang.StringBuilder ) v2 ).toString ( ); // invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
					 android.util.Slog .e ( v1,v2 );
					 /* :try_end_4 */
					 /* .catchall {:try_start_4 ..:try_end_4} :catchall_0 */
					 /* .line 36 */
				 } // .end local v0 # "ex":Ljava/io/IOException;
			 } // :cond_1
		 } // :goto_1
		 /* monitor-exit p0 */
		 return;
		 /* .line 20 */
		 /* :catchall_0 */
		 /* move-exception v0 */
		 /* monitor-exit p0 */
		 /* throw v0 */
	 } // .end method
	 java.lang.String getErrMsg ( java.lang.String p0, java.lang.String p1 ) {
		 /* .locals 2 */
		 /* .param p1, "functionName" # Ljava/lang/String; */
		 /* .param p2, "reason" # Ljava/lang/String; */
		 /* .line 82 */
		 /* new-instance v0, Ljava/lang/StringBuilder; */
		 /* invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V */
		 final String v1 = "FAIL, function name: "; // const-string v1, "FAIL, function name: "
		 (( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
		 (( java.lang.StringBuilder ) v0 ).append ( p1 ); // invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
		 final String v1 = ", reason: "; // const-string v1, ", reason: "
		 (( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
		 (( java.lang.StringBuilder ) v0 ).append ( p2 ); // invoke-virtual {v0, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
		 (( java.lang.StringBuilder ) v0 ).toString ( ); // invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
	 } // .end method
	 getLargeMsg ( Object[] p0 ) {
		 /* .locals 5 */
		 /* .param p1, "msg" # [B */
		 /* .line 94 */
		 /* array-length v0, p1 */
		 java.lang.Integer .valueOf ( v0 );
		 /* filled-new-array {v0}, [Ljava/lang/Object; */
		 final String v1 = "%08x"; // const-string v1, "%08x"
		 java.lang.String .format ( v1,v0 );
		 (( java.lang.String ) v0 ).getBytes ( ); // invoke-virtual {v0}, Ljava/lang/String;->getBytes()[B
		 /* .line 95 */
		 /* .local v0, "msgLenByte":[B */
		 /* array-length v1, v0 */
		 /* array-length v2, p1 */
		 /* add-int/2addr v1, v2 */
		 /* new-array v1, v1, [B */
		 /* .line 97 */
		 /* .local v1, "largeMsg":[B */
		 int v2 = 0; // const/4 v2, 0x0
		 /* .line 98 */
		 /* .local v2, "startPos":I */
		 /* array-length v3, v0 */
		 int v4 = 0; // const/4 v4, 0x0
		 java.lang.System .arraycopy ( v0,v4,v1,v2,v3 );
		 /* .line 99 */
		 /* array-length v3, v0 */
		 /* add-int/2addr v2, v3 */
		 /* .line 100 */
		 /* array-length v3, p1 */
		 java.lang.System .arraycopy ( p1,v4,v1,v2,v3 );
		 /* .line 102 */
	 } // .end method
	 abstract void listenToSocket ( ) {
		 /* .annotation system Ldalvik/annotation/Throws; */
		 /* value = { */
		 /* Ljava/io/IOException; */
		 /* } */
	 } // .end annotation
} // .end method
synchronized void sendResponse ( Integer p0, Integer p1, Object[] p2 ) {
	 /* .locals 6 */
	 /* .param p1, "msgType" # I */
	 /* .param p2, "msgId" # I */
	 /* .param p3, "byteMsg" # [B */
	 /* monitor-enter p0 */
	 /* .line 59 */
	 try { // :try_start_0
		 v0 = this.mOutputStream;
		 /* :try_end_0 */
		 /* .catchall {:try_start_0 ..:try_end_0} :catchall_0 */
		 if ( v0 != null) { // if-eqz v0, :cond_0
			 if ( p3 != null) { // if-eqz p3, :cond_0
				 /* .line 61 */
				 try { // :try_start_1
					 final String v1 = "%04x"; // const-string v1, "%04x"
					 int v2 = 1; // const/4 v2, 0x1
					 /* new-array v3, v2, [Ljava/lang/Object; */
					 java.lang.Integer .valueOf ( p2 );
					 int v5 = 0; // const/4 v5, 0x0
					 /* aput-object v4, v3, v5 */
					 java.lang.String .format ( v1,v3 );
					 (( java.lang.String ) v1 ).getBytes ( ); // invoke-virtual {v1}, Ljava/lang/String;->getBytes()[B
					 (( java.io.OutputStream ) v0 ).write ( v1 ); // invoke-virtual {v0, v1}, Ljava/io/OutputStream;->write([B)V
					 /* .line 62 */
					 v0 = this.mOutputStream;
					 final String v1 = "%04x"; // const-string v1, "%04x"
					 /* new-array v3, v2, [Ljava/lang/Object; */
					 java.lang.Integer .valueOf ( p1 );
					 /* aput-object v4, v3, v5 */
					 java.lang.String .format ( v1,v3 );
					 (( java.lang.String ) v1 ).getBytes ( ); // invoke-virtual {v1}, Ljava/lang/String;->getBytes()[B
					 (( java.io.OutputStream ) v0 ).write ( v1 ); // invoke-virtual {v0, v1}, Ljava/io/OutputStream;->write([B)V
					 /* .line 63 */
					 v0 = this.mOutputStream;
					 final String v1 = "%08x"; // const-string v1, "%08x"
					 /* new-array v2, v2, [Ljava/lang/Object; */
					 /* array-length v3, p3 */
					 java.lang.Integer .valueOf ( v3 );
					 /* aput-object v3, v2, v5 */
					 java.lang.String .format ( v1,v2 );
					 (( java.lang.String ) v1 ).getBytes ( ); // invoke-virtual {v1}, Ljava/lang/String;->getBytes()[B
					 (( java.io.OutputStream ) v0 ).write ( v1 ); // invoke-virtual {v0, v1}, Ljava/io/OutputStream;->write([B)V
					 /* .line 64 */
					 v0 = this.mOutputStream;
					 (( java.io.OutputStream ) v0 ).write ( p3 ); // invoke-virtual {v0, p3}, Ljava/io/OutputStream;->write([B)V
					 /* .line 65 */
					 v0 = this.mOutputStream;
					 (( java.io.OutputStream ) v0 ).flush ( ); // invoke-virtual {v0}, Ljava/io/OutputStream;->flush()V
					 /* :try_end_1 */
					 /* .catch Ljava/io/IOException; {:try_start_1 ..:try_end_1} :catch_0 */
					 /* .catchall {:try_start_1 ..:try_end_1} :catchall_0 */
					 /* .line 68 */
					 /* .line 66 */
				 } // .end local p0 # "this":Lcom/miui/server/UsbManagerConnect;
				 /* :catch_0 */
				 /* move-exception v0 */
				 /* .line 67 */
				 /* .local v0, "ex":Ljava/io/IOException; */
				 try { // :try_start_2
					 final String v1 = "UsbManagerConnect"; // const-string v1, "UsbManagerConnect"
					 final String v2 = "Failed to write response:"; // const-string v2, "Failed to write response:"
					 android.util.Slog .e ( v1,v2,v0 );
					 /* :try_end_2 */
					 /* .catchall {:try_start_2 ..:try_end_2} :catchall_0 */
					 /* .line 70 */
				 } // .end local v0 # "ex":Ljava/io/IOException;
			 } // :cond_0
		 } // :goto_0
		 /* monitor-exit p0 */
		 return;
		 /* .line 58 */
	 } // .end local p1 # "msgType":I
} // .end local p2 # "msgId":I
} // .end local p3 # "byteMsg":[B
/* :catchall_0 */
/* move-exception p1 */
/* monitor-exit p0 */
/* throw p1 */
} // .end method
synchronized void sendResponse ( java.lang.String p0 ) {
/* .locals 3 */
/* .param p1, "msg" # Ljava/lang/String; */
/* monitor-enter p0 */
/* .line 39 */
try { // :try_start_0
v0 = this.mOutputStream;
/* :try_end_0 */
/* .catchall {:try_start_0 ..:try_end_0} :catchall_0 */
if ( v0 != null) { // if-eqz v0, :cond_0
	 /* .line 41 */
	 try { // :try_start_1
		 (( java.lang.String ) p1 ).getBytes ( ); // invoke-virtual {p1}, Ljava/lang/String;->getBytes()[B
		 (( java.io.OutputStream ) v0 ).write ( v1 ); // invoke-virtual {v0, v1}, Ljava/io/OutputStream;->write([B)V
		 /* :try_end_1 */
		 /* .catch Ljava/io/IOException; {:try_start_1 ..:try_end_1} :catch_0 */
		 /* .catchall {:try_start_1 ..:try_end_1} :catchall_0 */
		 /* .line 44 */
		 /* .line 42 */
	 } // .end local p0 # "this":Lcom/miui/server/UsbManagerConnect;
	 /* :catch_0 */
	 /* move-exception v0 */
	 /* .line 43 */
	 /* .local v0, "ex":Ljava/io/IOException; */
	 try { // :try_start_2
		 final String v1 = "UsbManagerConnect"; // const-string v1, "UsbManagerConnect"
		 final String v2 = "Failed to write response:"; // const-string v2, "Failed to write response:"
		 android.util.Slog .e ( v1,v2,v0 );
		 /* :try_end_2 */
		 /* .catchall {:try_start_2 ..:try_end_2} :catchall_0 */
		 /* .line 46 */
	 } // .end local v0 # "ex":Ljava/io/IOException;
} // :cond_0
} // :goto_0
/* monitor-exit p0 */
return;
/* .line 38 */
} // .end local p1 # "msg":Ljava/lang/String;
/* :catchall_0 */
/* move-exception p1 */
/* monitor-exit p0 */
/* throw p1 */
} // .end method
