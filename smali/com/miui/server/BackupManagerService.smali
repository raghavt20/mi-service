.class public Lcom/miui/server/BackupManagerService;
.super Lmiui/app/backup/IBackupManager$Stub;
.source "BackupManagerService.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/miui/server/BackupManagerService$DeathLinker;,
        Lcom/miui/server/BackupManagerService$BackupHandler;,
        Lcom/miui/server/BackupManagerService$FullBackupRestoreObserver;,
        Lcom/miui/server/BackupManagerService$Lifecycle;
    }
.end annotation


# static fields
.field private static final COMPONENT_ENABLED_STATE_NONE:I = -0x1

.field public static final FD_CLOSE:I = -0x2

.field public static final FD_NONE:I = -0x1

.field private static final PID_NONE:I = -0x1

.field private static final TAG:Ljava/lang/String; = "Backup:BackupManagerService"


# instance fields
.field private mActivityManager:Landroid/app/ActivityManager;

.field private mAppUserId:I

.field private mBackupRestoreObserver:Lmiui/app/backup/IPackageBackupRestoreObserver;

.field private volatile mCallerFd:I

.field private mContext:Landroid/content/Context;

.field private mCurrentCompletedSize:J

.field private mCurrentTotalSize:J

.field private mCurrentWorkingFeature:I

.field private mCurrentWorkingPkg:Ljava/lang/String;

.field private mDeathLinker:Lcom/miui/server/BackupManagerService$DeathLinker;

.field private mEncryptedPwd:Ljava/lang/String;

.field private mEncryptedPwdInBakFile:Ljava/lang/String;

.field private mHandler:Landroid/os/Handler;

.field private mHandlerThread:Landroid/os/HandlerThread;

.field private mICaller:Landroid/os/IBinder;

.field private mIsCanceling:Z

.field private mLastError:I

.field private mNeedBeKilledPkgs:Ljava/util/HashMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/HashMap<",
            "Ljava/lang/String;",
            "Ljava/lang/Boolean;",
            ">;"
        }
    .end annotation
.end field

.field private mOutputFile:Landroid/os/ParcelFileDescriptor;

.field private mOwnerPid:I

.field private mPackageLastEnableState:I

.field private mPackageManager:Landroid/content/pm/PackageManager;

.field private mPackageManagerBinder:Landroid/content/pm/IPackageManager;

.field mPackageMonitor:Lcom/android/internal/content/PackageMonitor;

.field private mPackageStatsObserver:Landroid/content/pm/IPackageStatsObserver;

.field private final mPkgChangingLock:Ljava/util/concurrent/atomic/AtomicBoolean;

.field private mPreviousWorkingPkg:Ljava/lang/String;

.field private mProgType:I

.field private mPwd:Ljava/lang/String;

.field private mShouldSkipData:Z

.field private mState:I

.field private mStateObservers:Landroid/os/RemoteCallbackList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/os/RemoteCallbackList<",
            "Lmiui/app/backup/IBackupServiceStateObserver;",
            ">;"
        }
    .end annotation
.end field

.field private mTaskLatch:Ljava/util/concurrent/atomic/AtomicBoolean;


# direct methods
.method static bridge synthetic -$$Nest$fgetmAppUserId(Lcom/miui/server/BackupManagerService;)I
    .locals 0

    iget p0, p0, Lcom/miui/server/BackupManagerService;->mAppUserId:I

    return p0
.end method

.method static bridge synthetic -$$Nest$fgetmBackupRestoreObserver(Lcom/miui/server/BackupManagerService;)Lmiui/app/backup/IPackageBackupRestoreObserver;
    .locals 0

    iget-object p0, p0, Lcom/miui/server/BackupManagerService;->mBackupRestoreObserver:Lmiui/app/backup/IPackageBackupRestoreObserver;

    return-object p0
.end method

.method static bridge synthetic -$$Nest$fgetmContext(Lcom/miui/server/BackupManagerService;)Landroid/content/Context;
    .locals 0

    iget-object p0, p0, Lcom/miui/server/BackupManagerService;->mContext:Landroid/content/Context;

    return-object p0
.end method

.method static bridge synthetic -$$Nest$fgetmCurrentWorkingFeature(Lcom/miui/server/BackupManagerService;)I
    .locals 0

    iget p0, p0, Lcom/miui/server/BackupManagerService;->mCurrentWorkingFeature:I

    return p0
.end method

.method static bridge synthetic -$$Nest$fgetmCurrentWorkingPkg(Lcom/miui/server/BackupManagerService;)Ljava/lang/String;
    .locals 0

    iget-object p0, p0, Lcom/miui/server/BackupManagerService;->mCurrentWorkingPkg:Ljava/lang/String;

    return-object p0
.end method

.method static bridge synthetic -$$Nest$fgetmDeathLinker(Lcom/miui/server/BackupManagerService;)Lcom/miui/server/BackupManagerService$DeathLinker;
    .locals 0

    iget-object p0, p0, Lcom/miui/server/BackupManagerService;->mDeathLinker:Lcom/miui/server/BackupManagerService$DeathLinker;

    return-object p0
.end method

.method static bridge synthetic -$$Nest$fgetmICaller(Lcom/miui/server/BackupManagerService;)Landroid/os/IBinder;
    .locals 0

    iget-object p0, p0, Lcom/miui/server/BackupManagerService;->mICaller:Landroid/os/IBinder;

    return-object p0
.end method

.method static bridge synthetic -$$Nest$fgetmPackageManager(Lcom/miui/server/BackupManagerService;)Landroid/content/pm/PackageManager;
    .locals 0

    iget-object p0, p0, Lcom/miui/server/BackupManagerService;->mPackageManager:Landroid/content/pm/PackageManager;

    return-object p0
.end method

.method static bridge synthetic -$$Nest$fgetmPkgChangingLock(Lcom/miui/server/BackupManagerService;)Ljava/util/concurrent/atomic/AtomicBoolean;
    .locals 0

    iget-object p0, p0, Lcom/miui/server/BackupManagerService;->mPkgChangingLock:Ljava/util/concurrent/atomic/AtomicBoolean;

    return-object p0
.end method

.method static bridge synthetic -$$Nest$fgetmPwd(Lcom/miui/server/BackupManagerService;)Ljava/lang/String;
    .locals 0

    iget-object p0, p0, Lcom/miui/server/BackupManagerService;->mPwd:Ljava/lang/String;

    return-object p0
.end method

.method static bridge synthetic -$$Nest$fputmCurrentTotalSize(Lcom/miui/server/BackupManagerService;J)V
    .locals 0

    iput-wide p1, p0, Lcom/miui/server/BackupManagerService;->mCurrentTotalSize:J

    return-void
.end method

.method static bridge synthetic -$$Nest$fputmICaller(Lcom/miui/server/BackupManagerService;Landroid/os/IBinder;)V
    .locals 0

    iput-object p1, p0, Lcom/miui/server/BackupManagerService;->mICaller:Landroid/os/IBinder;

    return-void
.end method

.method static bridge synthetic -$$Nest$fputmIsCanceling(Lcom/miui/server/BackupManagerService;Z)V
    .locals 0

    iput-boolean p1, p0, Lcom/miui/server/BackupManagerService;->mIsCanceling:Z

    return-void
.end method

.method static bridge synthetic -$$Nest$fputmOwnerPid(Lcom/miui/server/BackupManagerService;I)V
    .locals 0

    iput p1, p0, Lcom/miui/server/BackupManagerService;->mOwnerPid:I

    return-void
.end method

.method static bridge synthetic -$$Nest$fputmPreviousWorkingPkg(Lcom/miui/server/BackupManagerService;Ljava/lang/String;)V
    .locals 0

    iput-object p1, p0, Lcom/miui/server/BackupManagerService;->mPreviousWorkingPkg:Ljava/lang/String;

    return-void
.end method

.method static bridge synthetic -$$Nest$mbroadcastServiceIdle(Lcom/miui/server/BackupManagerService;)V
    .locals 0

    invoke-direct {p0}, Lcom/miui/server/BackupManagerService;->broadcastServiceIdle()V

    return-void
.end method

.method static bridge synthetic -$$Nest$mrestoreLastPackageEnableState(Lcom/miui/server/BackupManagerService;Ljava/io/File;)V
    .locals 0

    invoke-direct {p0, p1}, Lcom/miui/server/BackupManagerService;->restoreLastPackageEnableState(Ljava/io/File;)V

    return-void
.end method

.method static bridge synthetic -$$Nest$mscheduleReleaseResource(Lcom/miui/server/BackupManagerService;)V
    .locals 0

    invoke-direct {p0}, Lcom/miui/server/BackupManagerService;->scheduleReleaseResource()V

    return-void
.end method

.method static bridge synthetic -$$Nest$mwaitForTheLastWorkingTask(Lcom/miui/server/BackupManagerService;)V
    .locals 0

    invoke-direct {p0}, Lcom/miui/server/BackupManagerService;->waitForTheLastWorkingTask()V

    return-void
.end method

.method static bridge synthetic -$$Nest$smgetPackageEnableStateFile()Ljava/io/File;
    .locals 1

    invoke-static {}, Lcom/miui/server/BackupManagerService;->getPackageEnableStateFile()Ljava/io/File;

    move-result-object v0

    return-object v0
.end method

.method public constructor <init>(Landroid/content/Context;)V
    .locals 4
    .param p1, "context"    # Landroid/content/Context;

    .line 172
    invoke-direct {p0}, Lmiui/app/backup/IBackupManager$Stub;-><init>()V

    .line 68
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    iput-object v0, p0, Lcom/miui/server/BackupManagerService;->mNeedBeKilledPkgs:Ljava/util/HashMap;

    .line 88
    new-instance v0, Landroid/os/RemoteCallbackList;

    invoke-direct {v0}, Landroid/os/RemoteCallbackList;-><init>()V

    iput-object v0, p0, Lcom/miui/server/BackupManagerService;->mStateObservers:Landroid/os/RemoteCallbackList;

    .line 89
    const/4 v0, -0x1

    iput v0, p0, Lcom/miui/server/BackupManagerService;->mOwnerPid:I

    .line 90
    const/4 v1, 0x0

    iput-object v1, p0, Lcom/miui/server/BackupManagerService;->mICaller:Landroid/os/IBinder;

    .line 91
    new-instance v2, Lcom/miui/server/BackupManagerService$DeathLinker;

    invoke-direct {v2, p0, v1}, Lcom/miui/server/BackupManagerService$DeathLinker;-><init>(Lcom/miui/server/BackupManagerService;Lcom/miui/server/BackupManagerService$DeathLinker-IA;)V

    iput-object v2, p0, Lcom/miui/server/BackupManagerService;->mDeathLinker:Lcom/miui/server/BackupManagerService$DeathLinker;

    .line 92
    iput-object v1, p0, Lcom/miui/server/BackupManagerService;->mTaskLatch:Ljava/util/concurrent/atomic/AtomicBoolean;

    .line 98
    iput v0, p0, Lcom/miui/server/BackupManagerService;->mCallerFd:I

    .line 99
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/miui/server/BackupManagerService;->mIsCanceling:Z

    .line 100
    iput-object v1, p0, Lcom/miui/server/BackupManagerService;->mOutputFile:Landroid/os/ParcelFileDescriptor;

    .line 101
    iput v0, p0, Lcom/miui/server/BackupManagerService;->mState:I

    .line 105
    iput v0, p0, Lcom/miui/server/BackupManagerService;->mAppUserId:I

    .line 111
    new-instance v2, Ljava/util/concurrent/atomic/AtomicBoolean;

    invoke-direct {v2, v0}, Ljava/util/concurrent/atomic/AtomicBoolean;-><init>(Z)V

    iput-object v2, p0, Lcom/miui/server/BackupManagerService;->mPkgChangingLock:Ljava/util/concurrent/atomic/AtomicBoolean;

    .line 113
    new-instance v0, Lcom/miui/server/BackupManagerService$1;

    invoke-direct {v0, p0}, Lcom/miui/server/BackupManagerService$1;-><init>(Lcom/miui/server/BackupManagerService;)V

    iput-object v0, p0, Lcom/miui/server/BackupManagerService;->mPackageMonitor:Lcom/android/internal/content/PackageMonitor;

    .line 908
    new-instance v0, Lcom/miui/server/BackupManagerService$4;

    invoke-direct {v0, p0}, Lcom/miui/server/BackupManagerService$4;-><init>(Lcom/miui/server/BackupManagerService;)V

    iput-object v0, p0, Lcom/miui/server/BackupManagerService;->mPackageStatsObserver:Landroid/content/pm/IPackageStatsObserver;

    .line 173
    iput-object p1, p0, Lcom/miui/server/BackupManagerService;->mContext:Landroid/content/Context;

    .line 174
    invoke-virtual {p1}, Landroid/content/Context;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v0

    iput-object v0, p0, Lcom/miui/server/BackupManagerService;->mPackageManager:Landroid/content/pm/PackageManager;

    .line 175
    invoke-static {}, Landroid/app/AppGlobals;->getPackageManager()Landroid/content/pm/IPackageManager;

    move-result-object v0

    iput-object v0, p0, Lcom/miui/server/BackupManagerService;->mPackageManagerBinder:Landroid/content/pm/IPackageManager;

    .line 176
    const-string v0, "activity"

    invoke-virtual {p1, v0}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/app/ActivityManager;

    iput-object v0, p0, Lcom/miui/server/BackupManagerService;->mActivityManager:Landroid/app/ActivityManager;

    .line 177
    new-instance v0, Landroid/os/HandlerThread;

    const-string v2, "MiuiBackup"

    const/16 v3, 0xa

    invoke-direct {v0, v2, v3}, Landroid/os/HandlerThread;-><init>(Ljava/lang/String;I)V

    iput-object v0, p0, Lcom/miui/server/BackupManagerService;->mHandlerThread:Landroid/os/HandlerThread;

    .line 178
    invoke-virtual {v0}, Landroid/os/HandlerThread;->start()V

    .line 179
    new-instance v0, Lcom/miui/server/BackupManagerService$BackupHandler;

    iget-object v2, p0, Lcom/miui/server/BackupManagerService;->mHandlerThread:Landroid/os/HandlerThread;

    invoke-virtual {v2}, Landroid/os/HandlerThread;->getLooper()Landroid/os/Looper;

    move-result-object v2

    invoke-direct {v0, p0, v2, v1}, Lcom/miui/server/BackupManagerService$BackupHandler;-><init>(Lcom/miui/server/BackupManagerService;Landroid/os/Looper;Lcom/miui/server/BackupManagerService$BackupHandler-IA;)V

    iput-object v0, p0, Lcom/miui/server/BackupManagerService;->mHandler:Landroid/os/Handler;

    .line 180
    return-void
.end method

.method private broadcastServiceIdle()V
    .locals 3
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    .line 552
    monitor-enter p0

    .line 554
    :try_start_0
    iget-object v0, p0, Lcom/miui/server/BackupManagerService;->mStateObservers:Landroid/os/RemoteCallbackList;

    invoke-virtual {v0}, Landroid/os/RemoteCallbackList;->beginBroadcast()I

    move-result v0

    .line 555
    .local v0, "cnt":I
    const/4 v1, 0x0

    .local v1, "i":I
    :goto_0
    if-ge v1, v0, :cond_0

    .line 556
    iget-object v2, p0, Lcom/miui/server/BackupManagerService;->mStateObservers:Landroid/os/RemoteCallbackList;

    invoke-virtual {v2, v1}, Landroid/os/RemoteCallbackList;->getBroadcastItem(I)Landroid/os/IInterface;

    move-result-object v2

    check-cast v2, Lmiui/app/backup/IBackupServiceStateObserver;

    invoke-interface {v2}, Lmiui/app/backup/IBackupServiceStateObserver;->onServiceStateIdle()V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 555
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 559
    .end local v0    # "cnt":I
    .end local v1    # "i":I
    :cond_0
    :try_start_1
    iget-object v0, p0, Lcom/miui/server/BackupManagerService;->mStateObservers:Landroid/os/RemoteCallbackList;

    invoke-virtual {v0}, Landroid/os/RemoteCallbackList;->finishBroadcast()V

    .line 560
    nop

    .line 561
    monitor-exit p0

    .line 562
    return-void

    .line 559
    :catchall_0
    move-exception v0

    iget-object v1, p0, Lcom/miui/server/BackupManagerService;->mStateObservers:Landroid/os/RemoteCallbackList;

    invoke-virtual {v1}, Landroid/os/RemoteCallbackList;->finishBroadcast()V

    .line 560
    nop

    .end local p0    # "this":Lcom/miui/server/BackupManagerService;
    throw v0

    .line 561
    .restart local p0    # "this":Lcom/miui/server/BackupManagerService;
    :catchall_1
    move-exception v0

    monitor-exit p0
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_1

    throw v0
.end method

.method private checkPackageAvailable(Ljava/lang/String;I)Z
    .locals 1
    .param p1, "pkg"    # Ljava/lang/String;
    .param p2, "userId"    # I

    .line 580
    invoke-direct {p0, p1, p2}, Lcom/miui/server/BackupManagerService;->isApplicationInstalled(Ljava/lang/String;I)Z

    move-result v0

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/miui/server/BackupManagerService;->mPackageManager:Landroid/content/pm/PackageManager;

    invoke-static {v0, p1, p2}, Lcom/miui/server/BackupManagerServiceProxy;->isPackageStateProtected(Landroid/content/pm/PackageManager;Ljava/lang/String;I)Z

    move-result v0

    if-eqz v0, :cond_0

    goto :goto_0

    .line 583
    :cond_0
    const/4 v0, 0x1

    return v0

    .line 581
    :cond_1
    :goto_0
    const/4 v0, 0x0

    return v0
.end method

.method private closeBackupWriteStream(Landroid/os/ParcelFileDescriptor;)V
    .locals 1
    .param p1, "outputFile"    # Landroid/os/ParcelFileDescriptor;

    .line 961
    if-eqz p1, :cond_0

    .line 963
    :try_start_0
    invoke-virtual {p1}, Landroid/os/ParcelFileDescriptor;->getFileDescriptor()Ljava/io/FileDescriptor;

    move-result-object v0

    invoke-static {v0}, Llibcore/io/IoBridge;->closeAndSignalBlockedThreads(Ljava/io/FileDescriptor;)V
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    .line 966
    goto :goto_0

    .line 964
    :catch_0
    move-exception v0

    .line 965
    .local v0, "e":Ljava/io/IOException;
    invoke-virtual {v0}, Ljava/io/IOException;->printStackTrace()V

    .line 968
    .end local v0    # "e":Ljava/io/IOException;
    :cond_0
    :goto_0
    return-void
.end method

.method private disablePackageAndWait(Ljava/lang/String;I)V
    .locals 8
    .param p1, "pkg"    # Ljava/lang/String;
    .param p2, "userId"    # I

    .line 618
    invoke-direct {p0, p1, p2}, Lcom/miui/server/BackupManagerService;->checkPackageAvailable(Ljava/lang/String;I)Z

    move-result v0

    if-nez v0, :cond_0

    .line 619
    return-void

    .line 621
    :cond_0
    invoke-direct {p0, p1, p2}, Lcom/miui/server/BackupManagerService;->getApplicationEnabledSetting(Ljava/lang/String;I)I

    move-result v0

    .line 622
    .local v0, "applicationSetting":I
    iget v1, p0, Lcom/miui/server/BackupManagerService;->mPackageLastEnableState:I

    const/4 v2, -0x1

    if-ne v1, v2, :cond_1

    .line 623
    iput v0, p0, Lcom/miui/server/BackupManagerService;->mPackageLastEnableState:I

    .line 625
    :cond_1
    const-string v1, "Backup:BackupManagerService"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "disablePackageAndWait, pkg:"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, " userId:"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, ", state:"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, ", lastState:"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget v3, p0, Lcom/miui/server/BackupManagerService;->mPackageLastEnableState:I

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Slog;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 627
    invoke-static {}, Lcom/miui/server/BackupManagerService;->getPackageEnableStateFile()Ljava/io/File;

    move-result-object v1

    iget v2, p0, Lcom/miui/server/BackupManagerService;->mPackageLastEnableState:I

    invoke-direct {p0, v1, p1, v2, p2}, Lcom/miui/server/BackupManagerService;->saveCurrentPackageEnableState(Ljava/io/File;Ljava/lang/String;II)V

    .line 628
    const/4 v1, 0x2

    if-ne v0, v1, :cond_2

    .line 629
    return-void

    .line 632
    :cond_2
    :try_start_0
    iget-object v2, p0, Lcom/miui/server/BackupManagerService;->mPackageMonitor:Lcom/android/internal/content/PackageMonitor;

    iget-object v3, p0, Lcom/miui/server/BackupManagerService;->mContext:Landroid/content/Context;

    invoke-virtual {v3}, Landroid/content/Context;->getMainLooper()Landroid/os/Looper;

    move-result-object v4

    const/4 v5, 0x0

    invoke-virtual {v2, v3, v4, v5}, Lcom/android/internal/content/PackageMonitor;->register(Landroid/content/Context;Landroid/os/Looper;Z)V

    .line 633
    invoke-static {}, Landroid/os/SystemClock;->elapsedRealtime()J

    move-result-wide v2

    .line 634
    .local v2, "waitStartTime":J
    iget-object v4, p0, Lcom/miui/server/BackupManagerService;->mPkgChangingLock:Ljava/util/concurrent/atomic/AtomicBoolean;

    monitor-enter v4
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 636
    :try_start_1
    iget-object v6, p0, Lcom/miui/server/BackupManagerService;->mPkgChangingLock:Ljava/util/concurrent/atomic/AtomicBoolean;

    const/4 v7, 0x1

    invoke-virtual {v6, v7}, Ljava/util/concurrent/atomic/AtomicBoolean;->set(Z)V

    .line 637
    invoke-direct {p0, p1, p2, v1, v5}, Lcom/miui/server/BackupManagerService;->setApplicationEnabledSetting(Ljava/lang/String;III)V

    .line 638
    iget-object v1, p0, Lcom/miui/server/BackupManagerService;->mPkgChangingLock:Ljava/util/concurrent/atomic/AtomicBoolean;

    const-wide/16 v6, 0x1388

    invoke-virtual {v1, v6, v7}, Ljava/lang/Object;->wait(J)V

    .line 639
    iget-object v1, p0, Lcom/miui/server/BackupManagerService;->mPkgChangingLock:Ljava/util/concurrent/atomic/AtomicBoolean;

    invoke-virtual {v1, v5}, Ljava/util/concurrent/atomic/AtomicBoolean;->set(Z)V
    :try_end_1
    .catch Ljava/lang/InterruptedException; {:try_start_1 .. :try_end_1} :catch_0
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 642
    goto :goto_0

    .line 643
    :catchall_0
    move-exception v1

    goto :goto_1

    .line 640
    :catch_0
    move-exception v1

    .line 641
    .local v1, "e":Ljava/lang/InterruptedException;
    :try_start_2
    const-string v5, "Backup:BackupManagerService"

    const-string v6, "mPkgChangingLock wait error"

    invoke-static {v5, v6, v1}, Landroid/util/Slog;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    .line 643
    .end local v1    # "e":Ljava/lang/InterruptedException;
    :goto_0
    monitor-exit v4
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    .line 644
    :try_start_3
    const-string v1, "Backup:BackupManagerService"

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v5, "setApplicationEnabledSetting wait time="

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-static {}, Landroid/os/SystemClock;->elapsedRealtime()J

    move-result-wide v5

    sub-long/2addr v5, v2

    invoke-virtual {v4, v5, v6}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, ", pkg="

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v1, v4}, Landroid/util/Slog;->i(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    .line 646
    nop

    .end local v2    # "waitStartTime":J
    iget-object v1, p0, Lcom/miui/server/BackupManagerService;->mPackageMonitor:Lcom/android/internal/content/PackageMonitor;

    invoke-virtual {v1}, Lcom/android/internal/content/PackageMonitor;->unregister()V

    .line 647
    nop

    .line 648
    iget-object v1, p0, Lcom/miui/server/BackupManagerService;->mContext:Landroid/content/Context;

    const-string v2, "activity"

    invoke-virtual {v1, v2}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/app/ActivityManager;

    invoke-virtual {v1, p1}, Landroid/app/ActivityManager;->forceStopPackage(Ljava/lang/String;)V

    .line 649
    invoke-direct {p0, p1}, Lcom/miui/server/BackupManagerService;->waitUntilAppKilled(Ljava/lang/String;)V

    .line 650
    return-void

    .line 643
    .restart local v2    # "waitStartTime":J
    :goto_1
    :try_start_4
    monitor-exit v4
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_0

    .end local v0    # "applicationSetting":I
    .end local p0    # "this":Lcom/miui/server/BackupManagerService;
    .end local p1    # "pkg":Ljava/lang/String;
    .end local p2    # "userId":I
    :try_start_5
    throw v1
    :try_end_5
    .catchall {:try_start_5 .. :try_end_5} :catchall_1

    .line 646
    .end local v2    # "waitStartTime":J
    .restart local v0    # "applicationSetting":I
    .restart local p0    # "this":Lcom/miui/server/BackupManagerService;
    .restart local p1    # "pkg":Ljava/lang/String;
    .restart local p2    # "userId":I
    :catchall_1
    move-exception v1

    iget-object v2, p0, Lcom/miui/server/BackupManagerService;->mPackageMonitor:Lcom/android/internal/content/PackageMonitor;

    invoke-virtual {v2}, Lcom/android/internal/content/PackageMonitor;->unregister()V

    .line 647
    throw v1
.end method

.method private enablePackage(Ljava/lang/String;ILjava/lang/String;)V
    .locals 4
    .param p1, "pkg"    # Ljava/lang/String;
    .param p2, "userId"    # I
    .param p3, "defaultIme"    # Ljava/lang/String;

    .line 588
    invoke-direct {p0, p1, p2}, Lcom/miui/server/BackupManagerService;->checkPackageAvailable(Ljava/lang/String;I)Z

    move-result v0

    if-nez v0, :cond_0

    .line 589
    return-void

    .line 591
    :cond_0
    iget v0, p0, Lcom/miui/server/BackupManagerService;->mPackageLastEnableState:I

    const/4 v1, -0x1

    const/4 v2, 0x0

    if-ne v0, v1, :cond_1

    .line 592
    iput v2, p0, Lcom/miui/server/BackupManagerService;->mPackageLastEnableState:I

    .line 594
    :cond_1
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "enablePackage, pkg:"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, " userId:"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", state:"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget v1, p0, Lcom/miui/server/BackupManagerService;->mPackageLastEnableState:I

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", defaultIme:"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    const-string v1, "Backup:BackupManagerService"

    invoke-static {v1, v0}, Landroid/util/Slog;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 596
    invoke-direct {p0, p1, p3}, Lcom/miui/server/BackupManagerService;->isDefaultIme(Ljava/lang/String;Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 601
    iget v0, p0, Lcom/miui/server/BackupManagerService;->mPackageLastEnableState:I

    invoke-direct {p0, p1, p2, v0, v2}, Lcom/miui/server/BackupManagerService;->setApplicationEnabledSetting(Ljava/lang/String;III)V

    .line 602
    iget-object v0, p0, Lcom/miui/server/BackupManagerService;->mHandler:Landroid/os/Handler;

    new-instance v1, Lcom/miui/server/BackupManagerService$3;

    invoke-direct {v1, p0, p3}, Lcom/miui/server/BackupManagerService$3;-><init>(Lcom/miui/server/BackupManagerService;Ljava/lang/String;)V

    const-wide/16 v2, 0x7d0

    invoke-virtual {v0, v1, v2, v3}, Landroid/os/Handler;->postDelayed(Ljava/lang/Runnable;J)Z

    .line 609
    return-void

    .line 612
    :cond_2
    iget v0, p0, Lcom/miui/server/BackupManagerService;->mPackageLastEnableState:I

    const/4 v1, 0x1

    invoke-direct {p0, p1, p2, v0, v1}, Lcom/miui/server/BackupManagerService;->setApplicationEnabledSetting(Ljava/lang/String;III)V

    .line 613
    invoke-static {}, Lcom/miui/server/BackupManagerService;->getPackageEnableStateFile()Ljava/io/File;

    move-result-object v0

    .line 614
    .local v0, "pkgStateFile":Ljava/io/File;
    invoke-virtual {v0}, Ljava/io/File;->delete()Z

    .line 615
    return-void
.end method

.method private getApplicationEnabledSetting(Ljava/lang/String;I)I
    .locals 2
    .param p1, "packageName"    # Ljava/lang/String;
    .param p2, "userId"    # I

    .line 993
    :try_start_0
    iget-object v0, p0, Lcom/miui/server/BackupManagerService;->mPackageManagerBinder:Landroid/content/pm/IPackageManager;

    invoke-interface {v0, p1, p2}, Landroid/content/pm/IPackageManager;->getApplicationEnabledSetting(Ljava/lang/String;I)I

    move-result v0
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    return v0

    .line 994
    :catch_0
    move-exception v0

    .line 995
    .local v0, "e":Landroid/os/RemoteException;
    invoke-virtual {v0}, Landroid/os/RemoteException;->rethrowFromSystemServer()Ljava/lang/RuntimeException;

    move-result-object v1

    throw v1
.end method

.method public static getCachedInstallFile()Ljava/io/File;
    .locals 3

    .line 693
    new-instance v0, Ljava/io/File;

    invoke-static {}, Landroid/os/Environment;->getDataDirectory()Ljava/io/File;

    move-result-object v1

    const-string/jumbo v2, "system"

    invoke-direct {v0, v1, v2}, Ljava/io/File;-><init>(Ljava/io/File;Ljava/lang/String;)V

    .line 694
    .local v0, "sysDir":Ljava/io/File;
    new-instance v1, Ljava/io/File;

    const-string v2, "restoring_cached_file"

    invoke-direct {v1, v0, v2}, Ljava/io/File;-><init>(Ljava/io/File;Ljava/lang/String;)V

    .line 695
    .local v1, "cachedFile":Ljava/io/File;
    return-object v1
.end method

.method private getDefaultIme(Landroid/content/Context;)Ljava/lang/String;
    .locals 2
    .param p1, "context"    # Landroid/content/Context;

    .line 931
    invoke-virtual {p1}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    const-string v1, "default_input_method"

    invoke-static {v0, v1}, Landroid/provider/Settings$Secure;->getString(Landroid/content/ContentResolver;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 933
    .local v0, "defaultIme":Ljava/lang/String;
    return-object v0
.end method

.method private static getPackageEnableStateFile()Ljava/io/File;
    .locals 3

    .line 687
    new-instance v0, Ljava/io/File;

    invoke-static {}, Landroid/os/Environment;->getDataDirectory()Ljava/io/File;

    move-result-object v1

    const-string/jumbo v2, "system"

    invoke-direct {v0, v1, v2}, Ljava/io/File;-><init>(Ljava/io/File;Ljava/lang/String;)V

    .line 688
    .local v0, "systemDir":Ljava/io/File;
    new-instance v1, Ljava/io/File;

    const-string v2, "backup_pkg_enable_state"

    invoke-direct {v1, v0, v2}, Ljava/io/File;-><init>(Ljava/io/File;Ljava/lang/String;)V

    .line 689
    .local v1, "pkgStateFile":Ljava/io/File;
    return-object v1
.end method

.method private isApplicationInstalled(Ljava/lang/String;I)Z
    .locals 4
    .param p1, "packageName"    # Ljava/lang/String;
    .param p2, "userId"    # I

    .line 275
    iget-object v0, p0, Lcom/miui/server/BackupManagerService;->mPackageManager:Landroid/content/pm/PackageManager;

    const/4 v1, 0x0

    invoke-virtual {v0, v1, p2}, Landroid/content/pm/PackageManager;->getInstalledPackagesAsUser(II)Ljava/util/List;

    move-result-object v0

    .line 276
    .local v0, "installedList":Ljava/util/List;, "Ljava/util/List<Landroid/content/pm/PackageInfo;>;"
    const/4 v1, 0x0

    .line 277
    .local v1, "isInstalled":Z
    const/4 v2, 0x0

    .local v2, "i":I
    :goto_0
    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v3

    if-ge v2, v3, :cond_1

    .line 278
    invoke-interface {v0, v2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Landroid/content/pm/PackageInfo;

    iget-object v3, v3, Landroid/content/pm/PackageInfo;->packageName:Ljava/lang/String;

    invoke-virtual {v3, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_0

    .line 279
    const/4 v1, 0x1

    .line 280
    goto :goto_1

    .line 277
    :cond_0
    add-int/lit8 v2, v2, 0x1

    goto :goto_0

    .line 283
    .end local v2    # "i":I
    :cond_1
    :goto_1
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "isApplicationInstalled, packageName:"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, " isInstalled:"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    const-string v3, "Backup:BackupManagerService"

    invoke-static {v3, v2}, Landroid/util/Slog;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 284
    return v1
.end method

.method private isDefaultIme(Ljava/lang/String;Ljava/lang/String;)Z
    .locals 2
    .param p1, "pkg"    # Ljava/lang/String;
    .param p2, "defaultIme"    # Ljava/lang/String;

    .line 937
    if-eqz p1, :cond_0

    if-eqz p2, :cond_0

    .line 938
    invoke-static {p2}, Landroid/content/ComponentName;->unflattenFromString(Ljava/lang/String;)Landroid/content/ComponentName;

    move-result-object v0

    .line 939
    .local v0, "cn":Landroid/content/ComponentName;
    if-eqz v0, :cond_0

    invoke-virtual {v0}, Landroid/content/ComponentName;->getPackageName()Ljava/lang/String;

    move-result-object v1

    invoke-static {p1, v1}, Landroid/text/TextUtils;->equals(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 940
    const/4 v1, 0x1

    return v1

    .line 943
    .end local v0    # "cn":Landroid/content/ComponentName;
    :cond_0
    const/4 v0, 0x0

    return v0
.end method

.method static synthetic lambda$releaseBackupWriteStream$0(Landroid/os/ParcelFileDescriptor;)V
    .locals 6
    .param p0, "outputFile"    # Landroid/os/ParcelFileDescriptor;

    .line 973
    const-string v0, "IOException"

    const-string v1, "Backup:BackupManagerService"

    const/16 v2, 0x400

    new-array v2, v2, [B

    .line 974
    .local v2, "b":[B
    new-instance v3, Ljava/io/FileInputStream;

    invoke-virtual {p0}, Landroid/os/ParcelFileDescriptor;->getFileDescriptor()Ljava/io/FileDescriptor;

    move-result-object v4

    invoke-direct {v3, v4}, Ljava/io/FileInputStream;-><init>(Ljava/io/FileDescriptor;)V

    .line 976
    .local v3, "fis":Ljava/io/FileInputStream;
    :goto_0
    :try_start_0
    invoke-virtual {v3, v2}, Ljava/io/FileInputStream;->read([B)I

    move-result v4
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_1
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    if-lez v4, :cond_0

    goto :goto_0

    .line 981
    :cond_0
    :try_start_1
    invoke-virtual {v3}, Ljava/io/FileInputStream;->close()V
    :try_end_1
    .catch Ljava/io/IOException; {:try_start_1 .. :try_end_1} :catch_0

    .line 984
    :goto_1
    goto :goto_2

    .line 982
    :catch_0
    move-exception v4

    .line 983
    .local v4, "e":Ljava/io/IOException;
    invoke-static {v1, v0, v4}, Landroid/util/Slog;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    .line 985
    .end local v4    # "e":Ljava/io/IOException;
    goto :goto_2

    .line 980
    :catchall_0
    move-exception v4

    goto :goto_3

    .line 977
    :catch_1
    move-exception v4

    .line 978
    .restart local v4    # "e":Ljava/io/IOException;
    :try_start_2
    const-string v5, "releaseBackupReadStream"

    invoke-static {v1, v5, v4}, Landroid/util/Slog;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    .line 981
    .end local v4    # "e":Ljava/io/IOException;
    :try_start_3
    invoke-virtual {v3}, Ljava/io/FileInputStream;->close()V
    :try_end_3
    .catch Ljava/io/IOException; {:try_start_3 .. :try_end_3} :catch_0

    goto :goto_1

    .line 986
    :goto_2
    return-void

    .line 981
    :goto_3
    :try_start_4
    invoke-virtual {v3}, Ljava/io/FileInputStream;->close()V
    :try_end_4
    .catch Ljava/io/IOException; {:try_start_4 .. :try_end_4} :catch_2

    .line 984
    goto :goto_4

    .line 982
    :catch_2
    move-exception v5

    .line 983
    .local v5, "e":Ljava/io/IOException;
    invoke-static {v1, v0, v5}, Landroid/util/Slog;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    .line 985
    .end local v5    # "e":Ljava/io/IOException;
    :goto_4
    throw v4
.end method

.method private static readHeaderLine(Ljava/io/InputStream;)Ljava/lang/String;
    .locals 3
    .param p0, "in"    # Ljava/io/InputStream;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .line 403
    new-instance v0, Ljava/lang/StringBuilder;

    const/16 v1, 0x50

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(I)V

    .line 404
    .local v0, "buffer":Ljava/lang/StringBuilder;
    :goto_0
    invoke-virtual {p0}, Ljava/io/InputStream;->read()I

    move-result v1

    move v2, v1

    .local v2, "c":I
    if-ltz v1, :cond_1

    .line 405
    const/16 v1, 0xa

    if-ne v2, v1, :cond_0

    goto :goto_1

    .line 406
    :cond_0
    int-to-char v1, v2

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    goto :goto_0

    .line 408
    :cond_1
    :goto_1
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    return-object v1
.end method

.method private releaseBackupWriteStream(Landroid/os/ParcelFileDescriptor;)V
    .locals 2
    .param p1, "outputFile"    # Landroid/os/ParcelFileDescriptor;

    .line 971
    if-eqz p1, :cond_0

    .line 972
    iget-object v0, p0, Lcom/miui/server/BackupManagerService;->mHandler:Landroid/os/Handler;

    new-instance v1, Lcom/miui/server/BackupManagerService$$ExternalSyntheticLambda0;

    invoke-direct {v1, p1}, Lcom/miui/server/BackupManagerService$$ExternalSyntheticLambda0;-><init>(Landroid/os/ParcelFileDescriptor;)V

    invoke-virtual {v0, v1}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    .line 988
    :cond_0
    return-void
.end method

.method private restoreLastPackageEnableState(Ljava/io/File;)V
    .locals 17
    .param p1, "pkgStateFile"    # Ljava/io/File;

    .line 720
    move-object/from16 v1, p0

    const-string v2, "IOEception"

    const-string v3, "Backup:BackupManagerService"

    invoke-static {}, Lcom/miui/server/BackupManagerService;->getCachedInstallFile()Ljava/io/File;

    move-result-object v4

    .line 721
    .local v4, "cachedFile":Ljava/io/File;
    invoke-virtual {v4}, Ljava/io/File;->exists()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 722
    invoke-virtual {v4}, Ljava/io/File;->delete()Z

    .line 725
    :cond_0
    invoke-virtual/range {p1 .. p1}, Ljava/io/File;->exists()Z

    move-result v0

    if-eqz v0, :cond_8

    .line 726
    const/4 v5, 0x0

    .line 727
    .local v5, "in":Ljava/io/FileInputStream;
    const/4 v6, 0x0

    .line 728
    .local v6, "pkg":Ljava/lang/String;
    const/high16 v7, -0x80000000

    .line 729
    .local v7, "state":I
    const/4 v8, 0x0

    .line 732
    .local v8, "userId":I
    :try_start_0
    new-instance v0, Ljava/io/FileInputStream;
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_4
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    move-object/from16 v9, p1

    :try_start_1
    invoke-direct {v0, v9}, Ljava/io/FileInputStream;-><init>(Ljava/io/File;)V

    move-object v5, v0

    .line 734
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    move-object v10, v0

    .line 735
    .local v10, "buffer":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/Byte;>;"
    :goto_0
    invoke-virtual {v5}, Ljava/io/FileInputStream;->read()I

    move-result v0

    move v11, v0

    .local v11, "c":I
    if-ltz v0, :cond_1

    .line 736
    int-to-byte v0, v11

    invoke-static {v0}, Ljava/lang/Byte;->valueOf(B)Ljava/lang/Byte;

    move-result-object v0

    invoke-virtual {v10, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 738
    :cond_1
    invoke-virtual {v10}, Ljava/util/ArrayList;->size()I

    move-result v0

    new-array v0, v0, [B

    move-object v12, v0

    .line 739
    .local v12, "bytes":[B
    const/4 v0, 0x0

    .local v0, "i":I
    :goto_1
    invoke-virtual {v10}, Ljava/util/ArrayList;->size()I

    move-result v13

    if-ge v0, v13, :cond_2

    .line 740
    invoke-virtual {v10, v0}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v13

    check-cast v13, Ljava/lang/Byte;

    invoke-virtual {v13}, Ljava/lang/Byte;->byteValue()B

    move-result v13

    aput-byte v13, v12, v0

    .line 739
    add-int/lit8 v0, v0, 0x1

    goto :goto_1

    .line 742
    .end local v0    # "i":I
    :cond_2
    new-instance v0, Ljava/lang/String;

    invoke-direct {v0, v12}, Ljava/lang/String;-><init>([B)V

    const-string v13, " "

    invoke-virtual {v0, v13}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v0

    move-object v13, v0

    .line 743
    .local v13, "ss":[Ljava/lang/String;
    array-length v0, v13

    const/4 v14, 0x0

    const/4 v15, 0x2

    const/16 v16, 0x1

    if-ne v0, v15, :cond_3

    .line 744
    aget-object v0, v13, v14
    :try_end_1
    .catch Ljava/io/IOException; {:try_start_1 .. :try_end_1} :catch_3
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    move-object v6, v0

    .line 746
    :try_start_2
    aget-object v0, v13, v16

    invoke-static {v0}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v0
    :try_end_2
    .catch Ljava/lang/NumberFormatException; {:try_start_2 .. :try_end_2} :catch_0
    .catch Ljava/io/IOException; {:try_start_2 .. :try_end_2} :catch_3
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    move v7, v0

    goto :goto_2

    .line 747
    :catch_0
    move-exception v0

    .line 748
    :goto_2
    goto :goto_3

    .line 749
    :cond_3
    :try_start_3
    array-length v0, v13

    const/4 v15, 0x3

    if-ne v0, v15, :cond_4

    .line 750
    aget-object v0, v13, v14
    :try_end_3
    .catch Ljava/io/IOException; {:try_start_3 .. :try_end_3} :catch_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    move-object v6, v0

    .line 752
    :try_start_4
    aget-object v0, v13, v16

    invoke-static {v0}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v0

    move v7, v0

    .line 753
    const/4 v0, 0x2

    aget-object v0, v13, v0

    invoke-static {v0}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v0
    :try_end_4
    .catch Ljava/lang/NumberFormatException; {:try_start_4 .. :try_end_4} :catch_1
    .catch Ljava/io/IOException; {:try_start_4 .. :try_end_4} :catch_3
    .catchall {:try_start_4 .. :try_end_4} :catchall_0

    .line 755
    .end local v8    # "userId":I
    .local v0, "userId":I
    move v8, v0

    goto :goto_3

    .line 754
    .end local v0    # "userId":I
    .restart local v8    # "userId":I
    :catch_1
    move-exception v0

    .line 760
    .end local v10    # "buffer":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/Byte;>;"
    .end local v11    # "c":I
    .end local v12    # "bytes":[B
    .end local v13    # "ss":[Ljava/lang/String;
    :cond_4
    :goto_3
    nop

    .line 762
    :try_start_5
    invoke-virtual {v5}, Ljava/io/FileInputStream;->close()V
    :try_end_5
    .catch Ljava/io/IOException; {:try_start_5 .. :try_end_5} :catch_2

    .line 765
    :goto_4
    goto :goto_7

    .line 763
    :catch_2
    move-exception v0

    move-object v10, v0

    move-object v0, v10

    .line 764
    .local v0, "e":Ljava/io/IOException;
    invoke-static {v3, v2, v0}, Landroid/util/Slog;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    .end local v0    # "e":Ljava/io/IOException;
    goto :goto_4

    .line 760
    :catchall_0
    move-exception v0

    goto :goto_5

    .line 757
    :catch_3
    move-exception v0

    goto :goto_6

    .line 760
    :catchall_1
    move-exception v0

    move-object/from16 v9, p1

    :goto_5
    move-object v10, v0

    goto :goto_8

    .line 757
    :catch_4
    move-exception v0

    move-object/from16 v9, p1

    .line 758
    .restart local v0    # "e":Ljava/io/IOException;
    :goto_6
    :try_start_6
    const-string v10, "IOException"

    invoke-static {v3, v10, v0}, Landroid/util/Slog;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I
    :try_end_6
    .catchall {:try_start_6 .. :try_end_6} :catchall_0

    .line 760
    nop

    .end local v0    # "e":Ljava/io/IOException;
    if-eqz v5, :cond_5

    .line 762
    :try_start_7
    invoke-virtual {v5}, Ljava/io/FileInputStream;->close()V
    :try_end_7
    .catch Ljava/io/IOException; {:try_start_7 .. :try_end_7} :catch_2

    goto :goto_4

    .line 769
    :cond_5
    :goto_7
    if-eqz v6, :cond_6

    const/high16 v0, -0x80000000

    if-eq v7, v0, :cond_6

    .line 770
    const-string v0, "Unfinished backup package found, restore it\'s enable state"

    invoke-static {v3, v0}, Landroid/util/Slog;->v(Ljava/lang/String;Ljava/lang/String;)I

    .line 771
    iget-object v0, v1, Lcom/miui/server/BackupManagerService;->mContext:Landroid/content/Context;

    invoke-direct {v1, v0}, Lcom/miui/server/BackupManagerService;->getDefaultIme(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v0

    .line 772
    .local v0, "defaultIme":Ljava/lang/String;
    iput v7, v1, Lcom/miui/server/BackupManagerService;->mPackageLastEnableState:I

    .line 773
    invoke-direct {v1, v6, v8, v0}, Lcom/miui/server/BackupManagerService;->enablePackage(Ljava/lang/String;ILjava/lang/String;)V

    .line 774
    .end local v0    # "defaultIme":Ljava/lang/String;
    goto :goto_a

    .line 775
    :cond_6
    const-string v0, "backup_pkg_enable_state file broken"

    invoke-static {v3, v0}, Landroid/util/Slog;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_a

    .line 760
    :goto_8
    if-eqz v5, :cond_7

    .line 762
    :try_start_8
    invoke-virtual {v5}, Ljava/io/FileInputStream;->close()V
    :try_end_8
    .catch Ljava/io/IOException; {:try_start_8 .. :try_end_8} :catch_5

    .line 765
    goto :goto_9

    .line 763
    :catch_5
    move-exception v0

    move-object v11, v0

    move-object v0, v11

    .line 764
    .local v0, "e":Ljava/io/IOException;
    invoke-static {v3, v2, v0}, Landroid/util/Slog;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    .line 767
    .end local v0    # "e":Ljava/io/IOException;
    :cond_7
    :goto_9
    throw v10

    .line 725
    .end local v5    # "in":Ljava/io/FileInputStream;
    .end local v6    # "pkg":Ljava/lang/String;
    .end local v7    # "state":I
    .end local v8    # "userId":I
    :cond_8
    move-object/from16 v9, p1

    .line 778
    :goto_a
    return-void
.end method

.method private saveCurrentPackageEnableState(Ljava/io/File;Ljava/lang/String;II)V
    .locals 6
    .param p1, "pkgStateFile"    # Ljava/io/File;
    .param p2, "pkg"    # Ljava/lang/String;
    .param p3, "state"    # I
    .param p4, "userId"    # I

    .line 699
    const-string v0, " "

    const-string v1, "IOException"

    const-string v2, "Backup:BackupManagerService"

    const/4 v3, 0x0

    .line 701
    .local v3, "out":Ljava/io/FileOutputStream;
    :try_start_0
    new-instance v4, Ljava/io/FileOutputStream;

    invoke-direct {v4, p1}, Ljava/io/FileOutputStream;-><init>(Ljava/io/File;)V

    move-object v3, v4

    .line 702
    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4, p2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    .line 703
    .local v4, "sb":Ljava/lang/StringBuilder;
    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, p3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    .line 704
    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    .line 705
    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/String;->getBytes()[B

    move-result-object v0

    invoke-virtual {v3, v0}, Ljava/io/FileOutputStream;->write([B)V
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_1
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 709
    .end local v4    # "sb":Ljava/lang/StringBuilder;
    nop

    .line 711
    :try_start_1
    invoke-virtual {v3}, Ljava/io/FileOutputStream;->close()V
    :try_end_1
    .catch Ljava/io/IOException; {:try_start_1 .. :try_end_1} :catch_0

    .line 714
    :goto_0
    goto :goto_1

    .line 712
    :catch_0
    move-exception v0

    .line 713
    .local v0, "e":Ljava/io/IOException;
    invoke-static {v2, v1, v0}, Landroid/util/Slog;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    .end local v0    # "e":Ljava/io/IOException;
    goto :goto_0

    .line 709
    :catchall_0
    move-exception v0

    goto :goto_2

    .line 706
    :catch_1
    move-exception v0

    .line 707
    .restart local v0    # "e":Ljava/io/IOException;
    :try_start_2
    invoke-static {v2, v1, v0}, Landroid/util/Slog;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    .line 709
    nop

    .end local v0    # "e":Ljava/io/IOException;
    if-eqz v3, :cond_0

    .line 711
    :try_start_3
    invoke-virtual {v3}, Ljava/io/FileOutputStream;->close()V
    :try_end_3
    .catch Ljava/io/IOException; {:try_start_3 .. :try_end_3} :catch_0

    goto :goto_0

    .line 717
    :cond_0
    :goto_1
    return-void

    .line 709
    :goto_2
    if-eqz v3, :cond_1

    .line 711
    :try_start_4
    invoke-virtual {v3}, Ljava/io/FileOutputStream;->close()V
    :try_end_4
    .catch Ljava/io/IOException; {:try_start_4 .. :try_end_4} :catch_2

    .line 714
    goto :goto_3

    .line 712
    :catch_2
    move-exception v4

    .line 713
    .local v4, "e":Ljava/io/IOException;
    invoke-static {v2, v1, v4}, Landroid/util/Slog;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    .line 716
    .end local v4    # "e":Ljava/io/IOException;
    :cond_1
    :goto_3
    throw v0
.end method

.method private scheduleReleaseResource()V
    .locals 1

    .line 537
    iget-object v0, p0, Lcom/miui/server/BackupManagerService;->mTaskLatch:Ljava/util/concurrent/atomic/AtomicBoolean;

    if-eqz v0, :cond_0

    invoke-virtual {v0}, Ljava/util/concurrent/atomic/AtomicBoolean;->get()Z

    move-result v0

    if-nez v0, :cond_0

    .line 538
    nop

    .line 540
    :try_start_0
    invoke-static {}, Lcom/miui/server/BackupManagerServiceProxy;->fullCancel()V
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    .line 543
    goto :goto_0

    .line 541
    :catch_0
    move-exception v0

    .line 542
    .local v0, "e":Landroid/os/RemoteException;
    invoke-virtual {v0}, Landroid/os/RemoteException;->printStackTrace()V

    .line 544
    .end local v0    # "e":Landroid/os/RemoteException;
    :goto_0
    iget-object v0, p0, Lcom/miui/server/BackupManagerService;->mOutputFile:Landroid/os/ParcelFileDescriptor;

    invoke-direct {p0, v0}, Lcom/miui/server/BackupManagerService;->closeBackupWriteStream(Landroid/os/ParcelFileDescriptor;)V

    .line 549
    :cond_0
    return-void
.end method

.method private setApplicationEnabledSetting(Ljava/lang/String;III)V
    .locals 11
    .param p1, "packageName"    # Ljava/lang/String;
    .param p2, "userId"    # I
    .param p3, "newState"    # I
    .param p4, "flags"    # I

    .line 1001
    invoke-static {}, Landroid/os/Binder;->clearCallingIdentity()J

    move-result-wide v0

    .line 1003
    .local v0, "token":J
    const/4 v2, 0x2

    if-ne p3, v2, :cond_0

    const/4 v2, 0x1

    goto :goto_0

    :cond_0
    const/4 v2, 0x0

    .line 1004
    .local v2, "suspend":Z
    :goto_0
    :try_start_0
    iget-object v3, p0, Lcom/miui/server/BackupManagerService;->mPackageManagerBinder:Landroid/content/pm/IPackageManager;

    filled-new-array {p1}, [Ljava/lang/String;

    move-result-object v4

    const/4 v6, 0x0

    const/4 v7, 0x0

    new-instance v5, Landroid/content/pm/SuspendDialogInfo$Builder;

    invoke-direct {v5}, Landroid/content/pm/SuspendDialogInfo$Builder;-><init>()V

    iget-object v8, p0, Lcom/miui/server/BackupManagerService;->mContext:Landroid/content/Context;

    invoke-virtual {v8}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v8

    const v9, 0x110f006e

    invoke-virtual {v8, v9}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v5, v8}, Landroid/content/pm/SuspendDialogInfo$Builder;->setMessage(Ljava/lang/String;)Landroid/content/pm/SuspendDialogInfo$Builder;

    move-result-object v5

    invoke-virtual {v5}, Landroid/content/pm/SuspendDialogInfo$Builder;->build()Landroid/content/pm/SuspendDialogInfo;

    move-result-object v8

    iget-object v5, p0, Lcom/miui/server/BackupManagerService;->mContext:Landroid/content/Context;

    invoke-virtual {v5}, Landroid/content/Context;->getOpPackageName()Ljava/lang/String;

    move-result-object v9

    move v5, v2

    move v10, p2

    invoke-interface/range {v3 .. v10}, Landroid/content/pm/IPackageManager;->setPackagesSuspendedAsUser([Ljava/lang/String;ZLandroid/os/PersistableBundle;Landroid/os/PersistableBundle;Landroid/content/pm/SuspendDialogInfo;Ljava/lang/String;I)[Ljava/lang/String;

    .line 1007
    const-string v3, "Backup:BackupManagerService"

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "packageName "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, " setPackagesSuspended suspend: "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v2}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Landroid/util/Slog;->d(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 1011
    nop

    .end local v2    # "suspend":Z
    invoke-static {v0, v1}, Landroid/os/Binder;->restoreCallingIdentity(J)V

    .line 1012
    nop

    .line 1014
    return-void

    .line 1011
    :catchall_0
    move-exception v2

    goto :goto_1

    .line 1008
    :catch_0
    move-exception v2

    .line 1009
    .local v2, "e":Landroid/os/RemoteException;
    :try_start_1
    invoke-virtual {v2}, Landroid/os/RemoteException;->rethrowFromSystemServer()Ljava/lang/RuntimeException;

    move-result-object v3

    .end local v0    # "token":J
    .end local p0    # "this":Lcom/miui/server/BackupManagerService;
    .end local p1    # "packageName":Ljava/lang/String;
    .end local p2    # "userId":I
    .end local p3    # "newState":I
    .end local p4    # "flags":I
    throw v3
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 1011
    .end local v2    # "e":Landroid/os/RemoteException;
    .restart local v0    # "token":J
    .restart local p0    # "this":Lcom/miui/server/BackupManagerService;
    .restart local p1    # "packageName":Ljava/lang/String;
    .restart local p2    # "userId":I
    .restart local p3    # "newState":I
    .restart local p4    # "flags":I
    :goto_1
    invoke-static {v0, v1}, Landroid/os/Binder;->restoreCallingIdentity(J)V

    .line 1012
    throw v2
.end method

.method private waitForTheLastWorkingTask()V
    .locals 2

    .line 565
    iget-object v0, p0, Lcom/miui/server/BackupManagerService;->mTaskLatch:Ljava/util/concurrent/atomic/AtomicBoolean;

    if-eqz v0, :cond_1

    .line 566
    monitor-enter v0

    .line 567
    :goto_0
    :try_start_0
    iget-object v1, p0, Lcom/miui/server/BackupManagerService;->mTaskLatch:Ljava/util/concurrent/atomic/AtomicBoolean;

    if-eqz v1, :cond_0

    invoke-virtual {v1}, Ljava/util/concurrent/atomic/AtomicBoolean;->get()Z

    move-result v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    if-nez v1, :cond_0

    .line 569
    :try_start_1
    iget-object v1, p0, Lcom/miui/server/BackupManagerService;->mTaskLatch:Ljava/util/concurrent/atomic/AtomicBoolean;

    invoke-virtual {v1}, Ljava/lang/Object;->wait()V
    :try_end_1
    .catch Ljava/lang/InterruptedException; {:try_start_1 .. :try_end_1} :catch_0
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 572
    :goto_1
    goto :goto_0

    .line 570
    :catch_0
    move-exception v1

    .line 571
    .local v1, "e":Ljava/lang/InterruptedException;
    :try_start_2
    invoke-virtual {v1}, Ljava/lang/InterruptedException;->printStackTrace()V

    .end local v1    # "e":Ljava/lang/InterruptedException;
    goto :goto_1

    .line 574
    :cond_0
    monitor-exit v0

    goto :goto_2

    :catchall_0
    move-exception v1

    monitor-exit v0
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    throw v1

    .line 576
    :cond_1
    :goto_2
    return-void
.end method

.method private waitUntilAppKilled(Ljava/lang/String;)V
    .locals 10
    .param p1, "pkg"    # Ljava/lang/String;

    .line 653
    const/16 v0, 0x14

    .line 654
    .local v0, "MAX_ROUND":I
    const/4 v1, 0x0

    .line 655
    .local v1, "round":I
    const/4 v2, 0x1

    .line 656
    .local v2, "killed":Z
    iget-object v3, p0, Lcom/miui/server/BackupManagerService;->mContext:Landroid/content/Context;

    const-string v4, "activity"

    invoke-virtual {v3, v4}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Landroid/app/ActivityManager;

    .line 658
    .local v3, "am":Landroid/app/ActivityManager;
    :goto_0
    const/4 v2, 0x1

    .line 659
    invoke-virtual {v3}, Landroid/app/ActivityManager;->getRunningAppProcesses()Ljava/util/List;

    move-result-object v4

    .line 660
    .local v4, "procInfos":Ljava/util/List;, "Ljava/util/List<Landroid/app/ActivityManager$RunningAppProcessInfo;>;"
    invoke-interface {v4}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v5

    :goto_1
    invoke-interface {v5}, Ljava/util/Iterator;->hasNext()Z

    move-result v6

    if-eqz v6, :cond_2

    invoke-interface {v5}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v6

    check-cast v6, Landroid/app/ActivityManager$RunningAppProcessInfo;

    .line 661
    .local v6, "procInfo":Landroid/app/ActivityManager$RunningAppProcessInfo;
    iget-object v7, v6, Landroid/app/ActivityManager$RunningAppProcessInfo;->processName:Ljava/lang/String;

    invoke-virtual {v7, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v7

    if-nez v7, :cond_0

    iget-object v7, v6, Landroid/app/ActivityManager$RunningAppProcessInfo;->processName:Ljava/lang/String;

    new-instance v8, Ljava/lang/StringBuilder;

    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v8, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    const-string v9, ":"

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v7, v8}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v7

    if-eqz v7, :cond_1

    :cond_0
    iget v7, p0, Lcom/miui/server/BackupManagerService;->mAppUserId:I

    iget v8, v6, Landroid/app/ActivityManager$RunningAppProcessInfo;->uid:I

    .line 662
    invoke-static {v8}, Landroid/os/UserHandle;->getUserId(I)I

    move-result v8

    if-ne v7, v8, :cond_1

    .line 663
    const/4 v2, 0x0

    .line 664
    goto :goto_2

    .line 666
    .end local v6    # "procInfo":Landroid/app/ActivityManager$RunningAppProcessInfo;
    :cond_1
    goto :goto_1

    .line 667
    :cond_2
    :goto_2
    const-string v5, "Backup:BackupManagerService"

    if-eqz v2, :cond_3

    .line 668
    goto :goto_3

    .line 672
    :cond_3
    const-wide/16 v6, 0x1f4

    :try_start_0
    invoke-static {v6, v7}, Ljava/lang/Thread;->sleep(J)V
    :try_end_0
    .catch Ljava/lang/InterruptedException; {:try_start_0 .. :try_end_0} :catch_0

    .line 676
    nop

    .line 677
    .end local v4    # "procInfos":Ljava/util/List;, "Ljava/util/List<Landroid/app/ActivityManager$RunningAppProcessInfo;>;"
    add-int/lit8 v4, v1, 0x1

    .end local v1    # "round":I
    .local v4, "round":I
    const/16 v6, 0x14

    if-lt v1, v6, :cond_4

    move v1, v4

    goto :goto_3

    :cond_4
    move v1, v4

    goto :goto_0

    .line 673
    .restart local v1    # "round":I
    .local v4, "procInfos":Ljava/util/List;, "Ljava/util/List<Landroid/app/ActivityManager$RunningAppProcessInfo;>;"
    :catch_0
    move-exception v6

    .line 674
    .local v6, "e":Ljava/lang/InterruptedException;
    const-string v7, "interrupted while waiting"

    invoke-static {v5, v7, v6}, Landroid/util/Slog;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    .line 675
    nop

    .line 679
    .end local v4    # "procInfos":Ljava/util/List;, "Ljava/util/List<Landroid/app/ActivityManager$RunningAppProcessInfo;>;"
    .end local v6    # "e":Ljava/lang/InterruptedException;
    :goto_3
    if-eqz v2, :cond_5

    .line 680
    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "app: "

    invoke-virtual {v4, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v6, " is killed. continue our routine."

    invoke-virtual {v4, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v5, v4}, Landroid/util/Slog;->i(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_4

    .line 682
    :cond_5
    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "continue while app: "

    invoke-virtual {v4, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v6, " is still alive!"

    invoke-virtual {v4, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v5, v4}, Landroid/util/Slog;->w(Ljava/lang/String;Ljava/lang/String;)I

    .line 684
    :goto_4
    return-void
.end method


# virtual methods
.method public acquire(Lmiui/app/backup/IBackupServiceStateObserver;Landroid/os/IBinder;)Z
    .locals 5
    .param p1, "stateObserver"    # Lmiui/app/backup/IBackupServiceStateObserver;
    .param p2, "iCaller"    # Landroid/os/IBinder;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    .line 475
    const/4 v0, 0x0

    if-nez p2, :cond_0

    .line 476
    const-string v1, "Backup:BackupManagerService"

    const-string v2, "caller should not be null"

    invoke-static {v1, v2}, Landroid/util/Slog;->w(Ljava/lang/String;Ljava/lang/String;)I

    .line 477
    return v0

    .line 480
    :cond_0
    iget-object v1, p0, Lcom/miui/server/BackupManagerService;->mContext:Landroid/content/Context;

    const-string v2, "android.permission.BACKUP"

    invoke-static {}, Landroid/os/Binder;->getCallingPid()I

    move-result v3

    invoke-static {}, Landroid/os/Binder;->getCallingUid()I

    move-result v4

    invoke-virtual {v1, v2, v3, v4}, Landroid/content/Context;->checkPermission(Ljava/lang/String;II)I

    move-result v1

    const/4 v2, -0x1

    if-ne v1, v2, :cond_1

    .line 481
    const-string v1, "Backup:BackupManagerService"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "Uid "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-static {}, Landroid/os/Binder;->getCallingUid()I

    move-result v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, " has no permission to call acquire "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Slog;->w(Ljava/lang/String;Ljava/lang/String;)I

    .line 482
    return v0

    .line 485
    :cond_1
    const-string v1, "Backup:BackupManagerService"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "Client tries to acquire service. CallingPid="

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-static {}, Landroid/os/Binder;->getCallingPid()I

    move-result v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, " mOwnerPid="

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    iget v4, p0, Lcom/miui/server/BackupManagerService;->mOwnerPid:I

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v1, v3}, Landroid/util/Slog;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 486
    monitor-enter p0

    .line 487
    :try_start_0
    const-string v1, "Backup:BackupManagerService"

    const-string v3, "Client acquire service. "

    invoke-static {v1, v3}, Landroid/util/Slog;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 488
    iget v1, p0, Lcom/miui/server/BackupManagerService;->mOwnerPid:I

    if-ne v1, v2, :cond_2

    .line 489
    invoke-static {}, Landroid/os/Binder;->getCallingPid()I

    move-result v1

    iput v1, p0, Lcom/miui/server/BackupManagerService;->mOwnerPid:I

    .line 490
    iput-object p2, p0, Lcom/miui/server/BackupManagerService;->mICaller:Landroid/os/IBinder;

    .line 491
    iget-object v1, p0, Lcom/miui/server/BackupManagerService;->mDeathLinker:Lcom/miui/server/BackupManagerService$DeathLinker;

    invoke-interface {p2, v1, v0}, Landroid/os/IBinder;->linkToDeath(Landroid/os/IBinder$DeathRecipient;I)V

    .line 492
    const-class v0, Lcom/miui/app/smartpower/SmartPowerServiceInternal;

    invoke-static {v0}, Lcom/android/server/LocalServices;->getService(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/miui/app/smartpower/SmartPowerServiceInternal;

    .line 493
    invoke-static {}, Landroid/os/Binder;->getCallingUid()I

    move-result v1

    iget v2, p0, Lcom/miui/server/BackupManagerService;->mOwnerPid:I

    .line 492
    const/4 v3, 0x1

    invoke-interface {v0, v3, v1, v2}, Lcom/miui/app/smartpower/SmartPowerServiceInternal;->onBackupServiceAppChanged(ZII)V

    .line 494
    monitor-exit p0

    return v3

    .line 496
    :cond_2
    if-eqz p1, :cond_3

    .line 497
    iget-object v1, p0, Lcom/miui/server/BackupManagerService;->mStateObservers:Landroid/os/RemoteCallbackList;

    invoke-virtual {v1, p1}, Landroid/os/RemoteCallbackList;->register(Landroid/os/IInterface;)Z

    .line 499
    :cond_3
    monitor-exit p0

    return v0

    .line 501
    :catchall_0
    move-exception v0

    monitor-exit p0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method

.method public addCompletedSize(J)V
    .locals 10
    .param p1, "size"    # J

    .line 359
    iget-wide v0, p0, Lcom/miui/server/BackupManagerService;->mCurrentCompletedSize:J

    add-long v6, v0, p1

    iput-wide v6, p0, Lcom/miui/server/BackupManagerService;->mCurrentCompletedSize:J

    .line 360
    iget v0, p0, Lcom/miui/server/BackupManagerService;->mProgType:I

    if-nez v0, :cond_0

    iget-object v2, p0, Lcom/miui/server/BackupManagerService;->mBackupRestoreObserver:Lmiui/app/backup/IPackageBackupRestoreObserver;

    if-eqz v2, :cond_0

    .line 362
    :try_start_0
    iget-object v3, p0, Lcom/miui/server/BackupManagerService;->mCurrentWorkingPkg:Ljava/lang/String;

    iget v4, p0, Lcom/miui/server/BackupManagerService;->mCurrentWorkingFeature:I

    const/4 v5, 0x0

    iget-wide v8, p0, Lcom/miui/server/BackupManagerService;->mCurrentTotalSize:J

    invoke-interface/range {v2 .. v9}, Lmiui/app/backup/IPackageBackupRestoreObserver;->onCustomProgressChange(Ljava/lang/String;IIJJ)V
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    .line 366
    goto :goto_0

    .line 364
    :catch_0
    move-exception v0

    .line 365
    .local v0, "e":Landroid/os/RemoteException;
    invoke-virtual {v0}, Landroid/os/RemoteException;->printStackTrace()V

    .line 368
    .end local v0    # "e":Landroid/os/RemoteException;
    :cond_0
    :goto_0
    return-void
.end method

.method public backupPackage(Landroid/os/ParcelFileDescriptor;Landroid/os/ParcelFileDescriptor;Ljava/lang/String;ILjava/lang/String;Ljava/lang/String;ZZZZLmiui/app/backup/IPackageBackupRestoreObserver;)V
    .locals 16
    .param p1, "outFileDescriptor"    # Landroid/os/ParcelFileDescriptor;
    .param p2, "readSide"    # Landroid/os/ParcelFileDescriptor;
    .param p3, "pkg"    # Ljava/lang/String;
    .param p4, "feature"    # I
    .param p5, "pwd"    # Ljava/lang/String;
    .param p6, "encryptedPwd"    # Ljava/lang/String;
    .param p7, "includeApk"    # Z
    .param p8, "forceBackup"    # Z
    .param p9, "shouldSkipData"    # Z
    .param p10, "isXSpace"    # Z
    .param p11, "observer"    # Lmiui/app/backup/IPackageBackupRestoreObserver;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    .line 186
    move-object/from16 v1, p0

    move-object/from16 v2, p1

    move-object/from16 v3, p3

    move/from16 v4, p4

    move/from16 v5, p7

    move/from16 v6, p9

    move/from16 v7, p10

    move-object/from16 v8, p11

    iput-object v8, v1, Lcom/miui/server/BackupManagerService;->mBackupRestoreObserver:Lmiui/app/backup/IPackageBackupRestoreObserver;

    .line 188
    invoke-static {}, Landroid/os/Binder;->getCallingPid()I

    move-result v9

    .line 189
    .local v9, "pid":I
    iget v0, v1, Lcom/miui/server/BackupManagerService;->mOwnerPid:I

    const/16 v10, 0x9

    if-eq v9, v0, :cond_0

    .line 190
    const-string v0, "Backup:BackupManagerService"

    const-string v11, "You must acquire first to use the backup or restore service"

    invoke-static {v0, v11}, Landroid/util/Slog;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 191
    invoke-virtual {v1, v10}, Lcom/miui/server/BackupManagerService;->errorOccur(I)V

    .line 192
    return-void

    .line 195
    :cond_0
    iget-object v0, v1, Lcom/miui/server/BackupManagerService;->mICaller:Landroid/os/IBinder;

    if-nez v0, :cond_1

    .line 196
    const-string v0, "Backup:BackupManagerService"

    const-string v11, "Caller is null You must acquire first with a binder"

    invoke-static {v0, v11}, Landroid/util/Slog;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 197
    invoke-virtual {v1, v10}, Lcom/miui/server/BackupManagerService;->errorOccur(I)V

    .line 198
    return-void

    .line 201
    :cond_1
    invoke-static/range {p5 .. p5}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_2

    invoke-static/range {p6 .. p6}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_2

    .line 202
    move-object/from16 v10, p5

    iput-object v10, v1, Lcom/miui/server/BackupManagerService;->mPwd:Ljava/lang/String;

    .line 203
    move-object/from16 v11, p6

    iput-object v11, v1, Lcom/miui/server/BackupManagerService;->mEncryptedPwd:Ljava/lang/String;

    goto :goto_0

    .line 201
    :cond_2
    move-object/from16 v10, p5

    move-object/from16 v11, p6

    .line 206
    :goto_0
    iget-object v0, v1, Lcom/miui/server/BackupManagerService;->mContext:Landroid/content/Context;

    invoke-direct {v1, v0}, Lcom/miui/server/BackupManagerService;->getDefaultIme(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v12

    .line 208
    .local v12, "defaultIme":Ljava/lang/String;
    iget-object v0, v1, Lcom/miui/server/BackupManagerService;->mContext:Landroid/content/Context;

    invoke-static {v0, v3}, Lmiui/app/backup/BackupManager;->isSysAppForBackup(Landroid/content/Context;Ljava/lang/String;)Z

    move-result v13

    .line 209
    .local v13, "isSystemApp":Z
    const-string v0, "Backup:BackupManagerService"

    new-instance v14, Ljava/lang/StringBuilder;

    invoke-direct {v14}, Ljava/lang/StringBuilder;-><init>()V

    const-string v15, "backupPackage: pkg="

    invoke-virtual {v14, v15}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v14

    invoke-virtual {v14, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v14

    const-string v15, " feature="

    invoke-virtual {v14, v15}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v14

    invoke-virtual {v14, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v14

    const-string v15, " includeApk="

    invoke-virtual {v14, v15}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v14

    invoke-virtual {v14, v5}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v14

    const-string v15, " shouldSkipData="

    invoke-virtual {v14, v15}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v14

    invoke-virtual {v14, v6}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v14

    const-string v15, " isXSpace="

    invoke-virtual {v14, v15}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v14

    invoke-virtual {v14, v7}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v14

    const-string v15, " isSystemApp="

    invoke-virtual {v14, v15}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v14

    invoke-virtual {v14, v13}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v14

    invoke-virtual {v14}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v14

    invoke-static {v0, v14}, Landroid/util/Slog;->v(Ljava/lang/String;Ljava/lang/String;)I

    .line 212
    const/4 v0, 0x0

    if-eqz v7, :cond_3

    const/16 v14, 0x3e7

    goto :goto_1

    :cond_3
    move v14, v0

    :goto_1
    iput v14, v1, Lcom/miui/server/BackupManagerService;->mAppUserId:I

    .line 213
    const/4 v15, -0x1

    iput v15, v1, Lcom/miui/server/BackupManagerService;->mPackageLastEnableState:I

    .line 214
    iput-object v3, v1, Lcom/miui/server/BackupManagerService;->mCurrentWorkingPkg:Ljava/lang/String;

    .line 215
    if-nez v13, :cond_4

    .line 216
    invoke-direct {v1, v3, v14}, Lcom/miui/server/BackupManagerService;->disablePackageAndWait(Ljava/lang/String;I)V

    .line 219
    :cond_4
    iput-object v2, v1, Lcom/miui/server/BackupManagerService;->mOutputFile:Landroid/os/ParcelFileDescriptor;

    .line 220
    iput-boolean v6, v1, Lcom/miui/server/BackupManagerService;->mShouldSkipData:Z

    .line 222
    iput v4, v1, Lcom/miui/server/BackupManagerService;->mCurrentWorkingFeature:I

    .line 223
    iput v0, v1, Lcom/miui/server/BackupManagerService;->mLastError:I

    .line 224
    iput v0, v1, Lcom/miui/server/BackupManagerService;->mProgType:I

    .line 226
    const/4 v14, 0x1

    iput v14, v1, Lcom/miui/server/BackupManagerService;->mState:I

    .line 228
    const-wide/16 v14, 0x0

    iput-wide v14, v1, Lcom/miui/server/BackupManagerService;->mCurrentCompletedSize:J

    .line 229
    const-wide/16 v14, -0x1

    iput-wide v14, v1, Lcom/miui/server/BackupManagerService;->mCurrentTotalSize:J

    .line 230
    iget-object v14, v1, Lcom/miui/server/BackupManagerService;->mContext:Landroid/content/Context;

    iget-object v15, v1, Lcom/miui/server/BackupManagerService;->mPackageManager:Landroid/content/pm/PackageManager;

    iget v0, v1, Lcom/miui/server/BackupManagerService;->mAppUserId:I

    iget-object v4, v1, Lcom/miui/server/BackupManagerService;->mPackageStatsObserver:Landroid/content/pm/IPackageStatsObserver;

    invoke-static {v14, v15, v3, v0, v4}, Lcom/miui/server/BackupManagerServiceProxy;->getPackageSizeInfo(Landroid/content/Context;Landroid/content/pm/PackageManager;Ljava/lang/String;ILandroid/content/pm/IPackageStatsObserver;)V

    .line 231
    monitor-enter p0

    .line 232
    :try_start_0
    new-instance v0, Ljava/util/concurrent/atomic/AtomicBoolean;

    const/4 v4, 0x0

    invoke-direct {v0, v4}, Ljava/util/concurrent/atomic/AtomicBoolean;-><init>(Z)V

    iput-object v0, v1, Lcom/miui/server/BackupManagerService;->mTaskLatch:Ljava/util/concurrent/atomic/AtomicBoolean;

    .line 233
    invoke-virtual/range {p1 .. p1}, Landroid/os/ParcelFileDescriptor;->getFd()I

    move-result v0

    iput v0, v1, Lcom/miui/server/BackupManagerService;->mCallerFd:I

    .line 234
    const-string v0, "Backup:BackupManagerService"

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v14, "backupPackage, MIUI FD is "

    invoke-virtual {v4, v14}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    iget v14, v1, Lcom/miui/server/BackupManagerService;->mCallerFd:I

    invoke-virtual {v4, v14}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v0, v4}, Landroid/util/Slog;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 235
    monitor-exit p0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_2

    .line 236
    iget-object v4, v1, Lcom/miui/server/BackupManagerService;->mTaskLatch:Ljava/util/concurrent/atomic/AtomicBoolean;

    monitor-enter v4

    .line 238
    :try_start_1
    iget v0, v1, Lcom/miui/server/BackupManagerService;->mOwnerPid:I

    const/4 v14, -0x1

    if-eq v0, v14, :cond_5

    .line 239
    filled-new-array/range {p3 .. p3}, [Ljava/lang/String;

    move-result-object v0

    invoke-static {v2, v0, v5}, Lcom/miui/server/BackupManagerServiceProxy;->fullBackup(Landroid/os/ParcelFileDescriptor;[Ljava/lang/String;Z)V

    goto :goto_2

    .line 241
    :cond_5
    const/16 v0, 0xa

    invoke-virtual {v1, v0}, Lcom/miui/server/BackupManagerService;->errorOccur(I)V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 244
    :goto_2
    :try_start_2
    const-string v0, "Backup:BackupManagerService"

    new-instance v14, Ljava/lang/StringBuilder;

    invoke-direct {v14}, Ljava/lang/StringBuilder;-><init>()V

    const-string v15, "backupPackage finish, MIUI FD is "

    invoke-virtual {v14, v15}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v14

    iget v15, v1, Lcom/miui/server/BackupManagerService;->mCallerFd:I

    invoke-virtual {v14, v15}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v14

    invoke-virtual {v14}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v14

    invoke-static {v0, v14}, Landroid/util/Slog;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 245
    iget-object v0, v1, Lcom/miui/server/BackupManagerService;->mTaskLatch:Ljava/util/concurrent/atomic/AtomicBoolean;

    const/4 v14, 0x1

    invoke-virtual {v0, v14}, Ljava/util/concurrent/atomic/AtomicBoolean;->set(Z)V

    .line 246
    iget-object v0, v1, Lcom/miui/server/BackupManagerService;->mTaskLatch:Ljava/util/concurrent/atomic/AtomicBoolean;

    invoke-virtual {v0}, Ljava/lang/Object;->notifyAll()V

    .line 247
    nop

    .line 248
    monitor-exit v4
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 250
    if-nez v13, :cond_6

    .line 251
    iget v0, v1, Lcom/miui/server/BackupManagerService;->mAppUserId:I

    invoke-direct {v1, v3, v0, v12}, Lcom/miui/server/BackupManagerService;->enablePackage(Ljava/lang/String;ILjava/lang/String;)V

    .line 254
    :cond_6
    const/4 v0, 0x0

    iput-object v0, v1, Lcom/miui/server/BackupManagerService;->mPwd:Ljava/lang/String;

    .line 255
    iput-object v0, v1, Lcom/miui/server/BackupManagerService;->mEncryptedPwd:Ljava/lang/String;

    .line 256
    iput-object v0, v1, Lcom/miui/server/BackupManagerService;->mTaskLatch:Ljava/util/concurrent/atomic/AtomicBoolean;

    .line 257
    const/4 v4, -0x1

    iput v4, v1, Lcom/miui/server/BackupManagerService;->mCallerFd:I

    .line 258
    iput-object v0, v1, Lcom/miui/server/BackupManagerService;->mOutputFile:Landroid/os/ParcelFileDescriptor;

    .line 259
    const/4 v0, 0x0

    iput v0, v1, Lcom/miui/server/BackupManagerService;->mProgType:I

    .line 260
    iput v0, v1, Lcom/miui/server/BackupManagerService;->mState:I

    .line 261
    iput v4, v1, Lcom/miui/server/BackupManagerService;->mPackageLastEnableState:I

    .line 262
    const-wide/16 v14, -0x1

    iput-wide v14, v1, Lcom/miui/server/BackupManagerService;->mCurrentTotalSize:J

    .line 263
    const-wide/16 v14, 0x0

    iput-wide v14, v1, Lcom/miui/server/BackupManagerService;->mCurrentCompletedSize:J

    .line 264
    iget-object v4, v1, Lcom/miui/server/BackupManagerService;->mCurrentWorkingPkg:Ljava/lang/String;

    iput-object v4, v1, Lcom/miui/server/BackupManagerService;->mPreviousWorkingPkg:Ljava/lang/String;

    .line 265
    iput v0, v1, Lcom/miui/server/BackupManagerService;->mAppUserId:I

    .line 266
    return-void

    .line 244
    :catchall_0
    move-exception v0

    :try_start_3
    const-string v14, "Backup:BackupManagerService"

    new-instance v15, Ljava/lang/StringBuilder;

    invoke-direct {v15}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "backupPackage finish, MIUI FD is "

    invoke-virtual {v15, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget v15, v1, Lcom/miui/server/BackupManagerService;->mCallerFd:I

    invoke-virtual {v2, v15}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v14, v2}, Landroid/util/Slog;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 245
    iget-object v2, v1, Lcom/miui/server/BackupManagerService;->mTaskLatch:Ljava/util/concurrent/atomic/AtomicBoolean;

    const/4 v14, 0x1

    invoke-virtual {v2, v14}, Ljava/util/concurrent/atomic/AtomicBoolean;->set(Z)V

    .line 246
    iget-object v2, v1, Lcom/miui/server/BackupManagerService;->mTaskLatch:Ljava/util/concurrent/atomic/AtomicBoolean;

    invoke-virtual {v2}, Ljava/lang/Object;->notifyAll()V

    .line 247
    nop

    .end local v9    # "pid":I
    .end local v12    # "defaultIme":Ljava/lang/String;
    .end local v13    # "isSystemApp":Z
    .end local p0    # "this":Lcom/miui/server/BackupManagerService;
    .end local p1    # "outFileDescriptor":Landroid/os/ParcelFileDescriptor;
    .end local p2    # "readSide":Landroid/os/ParcelFileDescriptor;
    .end local p3    # "pkg":Ljava/lang/String;
    .end local p4    # "feature":I
    .end local p5    # "pwd":Ljava/lang/String;
    .end local p6    # "encryptedPwd":Ljava/lang/String;
    .end local p7    # "includeApk":Z
    .end local p8    # "forceBackup":Z
    .end local p9    # "shouldSkipData":Z
    .end local p10    # "isXSpace":Z
    .end local p11    # "observer":Lmiui/app/backup/IPackageBackupRestoreObserver;
    throw v0

    .line 248
    .restart local v9    # "pid":I
    .restart local v12    # "defaultIme":Ljava/lang/String;
    .restart local v13    # "isSystemApp":Z
    .restart local p0    # "this":Lcom/miui/server/BackupManagerService;
    .restart local p1    # "outFileDescriptor":Landroid/os/ParcelFileDescriptor;
    .restart local p2    # "readSide":Landroid/os/ParcelFileDescriptor;
    .restart local p3    # "pkg":Ljava/lang/String;
    .restart local p4    # "feature":I
    .restart local p5    # "pwd":Ljava/lang/String;
    .restart local p6    # "encryptedPwd":Ljava/lang/String;
    .restart local p7    # "includeApk":Z
    .restart local p8    # "forceBackup":Z
    .restart local p9    # "shouldSkipData":Z
    .restart local p10    # "isXSpace":Z
    .restart local p11    # "observer":Lmiui/app/backup/IPackageBackupRestoreObserver;
    :catchall_1
    move-exception v0

    monitor-exit v4
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    throw v0

    .line 235
    :catchall_2
    move-exception v0

    :try_start_4
    monitor-exit p0
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_2

    throw v0
.end method

.method public delCacheBackup()V
    .locals 3

    .line 948
    invoke-static {}, Landroid/os/Binder;->getCallingUid()I

    move-result v0

    .line 949
    .local v0, "uid":I
    const/16 v1, 0x17d6

    if-eq v0, v1, :cond_0

    const/16 v1, 0x17d4

    if-ne v0, v1, :cond_1

    .line 950
    :cond_0
    new-instance v1, Ljava/io/File;

    const-string v2, "/cache/backup"

    invoke-direct {v1, v2}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 951
    .local v1, "file":Ljava/io/File;
    invoke-static {v1}, Landroid/os/FileUtils;->deleteContents(Ljava/io/File;)Z

    .line 953
    .end local v1    # "file":Ljava/io/File;
    :cond_1
    return-void
.end method

.method public errorOccur(I)V
    .locals 3
    .param p1, "err"    # I
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    .line 790
    iget v0, p0, Lcom/miui/server/BackupManagerService;->mLastError:I

    if-nez v0, :cond_0

    .line 791
    iput p1, p0, Lcom/miui/server/BackupManagerService;->mLastError:I

    .line 792
    iget-object v0, p0, Lcom/miui/server/BackupManagerService;->mBackupRestoreObserver:Lmiui/app/backup/IPackageBackupRestoreObserver;

    if-eqz v0, :cond_0

    .line 793
    iget-object v1, p0, Lcom/miui/server/BackupManagerService;->mCurrentWorkingPkg:Ljava/lang/String;

    iget v2, p0, Lcom/miui/server/BackupManagerService;->mCurrentWorkingFeature:I

    invoke-interface {v0, v1, v2, p1}, Lmiui/app/backup/IPackageBackupRestoreObserver;->onError(Ljava/lang/String;II)V

    .line 796
    :cond_0
    return-void
.end method

.method public getAppUserId()I
    .locals 1

    .line 829
    iget v0, p0, Lcom/miui/server/BackupManagerService;->mAppUserId:I

    return v0
.end method

.method public getBackupTimeoutScale()I
    .locals 2
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    .line 815
    iget-object v0, p0, Lcom/miui/server/BackupManagerService;->mCurrentWorkingPkg:Ljava/lang/String;

    iget v1, p0, Lcom/miui/server/BackupManagerService;->mCurrentWorkingFeature:I

    invoke-static {v0, v1}, Lmiui/app/backup/BackupManager;->isProgRecordApp(Ljava/lang/String;I)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 816
    const/4 v0, 0x6

    return v0

    .line 818
    :cond_0
    const/4 v0, 0x1

    return v0
.end method

.method public getCurrentRunningPackage()Ljava/lang/String;
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    .line 800
    iget-object v0, p0, Lcom/miui/server/BackupManagerService;->mCurrentWorkingPkg:Ljava/lang/String;

    return-object v0
.end method

.method public getCurrentWorkingFeature()I
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    .line 805
    iget v0, p0, Lcom/miui/server/BackupManagerService;->mCurrentWorkingFeature:I

    return v0
.end method

.method public getState()I
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    .line 810
    iget v0, p0, Lcom/miui/server/BackupManagerService;->mState:I

    return v0
.end method

.method public isCanceling()Z
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    .line 957
    iget-boolean v0, p0, Lcom/miui/server/BackupManagerService;->mIsCanceling:Z

    return v0
.end method

.method public isNeedBeKilled(Ljava/lang/String;)Z
    .locals 2
    .param p1, "pkg"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    .line 379
    iget-object v0, p0, Lcom/miui/server/BackupManagerService;->mNeedBeKilledPkgs:Ljava/util/HashMap;

    invoke-virtual {v0, p1}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Boolean;

    .line 380
    .local v0, "isKilled":Ljava/lang/Boolean;
    if-eqz v0, :cond_0

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v1

    goto :goto_0

    :cond_0
    const/4 v1, 0x1

    :goto_0
    return v1
.end method

.method public isRunningFromMiui(I)Z
    .locals 1
    .param p1, "fd"    # I
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    .line 385
    iget v0, p0, Lcom/miui/server/BackupManagerService;->mCallerFd:I

    if-ne v0, p1, :cond_0

    const/4 v0, 0x1

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    :goto_0
    return v0
.end method

.method public isServiceIdle()Z
    .locals 1

    .line 390
    iget v0, p0, Lcom/miui/server/BackupManagerService;->mState:I

    if-nez v0, :cond_0

    const/4 v0, 0x1

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    :goto_0
    return v0
.end method

.method public onApkInstalled()V
    .locals 2

    .line 782
    iget-object v0, p0, Lcom/miui/server/BackupManagerService;->mContext:Landroid/content/Context;

    iget-object v1, p0, Lcom/miui/server/BackupManagerService;->mCurrentWorkingPkg:Ljava/lang/String;

    invoke-static {v0, v1}, Lmiui/app/backup/BackupManager;->isSysAppForBackup(Landroid/content/Context;Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 783
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "onApkInstalled, mCurrentWorkingPkg:"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lcom/miui/server/BackupManagerService;->mCurrentWorkingPkg:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    const-string v1, "Backup:BackupManagerService"

    invoke-static {v1, v0}, Landroid/util/Slog;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 784
    iget-object v0, p0, Lcom/miui/server/BackupManagerService;->mCurrentWorkingPkg:Ljava/lang/String;

    iget v1, p0, Lcom/miui/server/BackupManagerService;->mAppUserId:I

    invoke-direct {p0, v0, v1}, Lcom/miui/server/BackupManagerService;->disablePackageAndWait(Ljava/lang/String;I)V

    .line 786
    :cond_0
    return-void
.end method

.method public readMiuiBackupHeader(Landroid/os/ParcelFileDescriptor;)V
    .locals 6
    .param p1, "inFileDescriptor"    # Landroid/os/ParcelFileDescriptor;

    .line 325
    const/4 v0, 0x0

    .line 327
    .local v0, "in":Ljava/io/InputStream;
    :try_start_0
    new-instance v1, Ljava/io/FileInputStream;

    invoke-virtual {p1}, Landroid/os/ParcelFileDescriptor;->getFileDescriptor()Ljava/io/FileDescriptor;

    move-result-object v2

    invoke-direct {v1, v2}, Ljava/io/FileInputStream;-><init>(Ljava/io/FileDescriptor;)V

    move-object v0, v1

    .line 329
    invoke-static {v0}, Lmiui/app/backup/BackupFileResolver;->readMiuiHeader(Ljava/io/InputStream;)Lmiui/app/backup/BackupFileResolver$BackupFileMiuiHeader;

    move-result-object v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 330
    .local v1, "header":Lmiui/app/backup/BackupFileResolver$BackupFileMiuiHeader;
    const-string v2, "Backup:BackupManagerService"

    if-eqz v1, :cond_2

    :try_start_1
    iget v3, v1, Lmiui/app/backup/BackupFileResolver$BackupFileMiuiHeader;->version:I

    const/4 v4, 0x2

    if-ne v3, v4, :cond_2

    .line 331
    iget-object v3, v1, Lmiui/app/backup/BackupFileResolver$BackupFileMiuiHeader;->packageName:Ljava/lang/String;

    iput-object v3, p0, Lcom/miui/server/BackupManagerService;->mCurrentWorkingPkg:Ljava/lang/String;

    .line 332
    iget v3, v1, Lmiui/app/backup/BackupFileResolver$BackupFileMiuiHeader;->featureId:I

    iput v3, p0, Lcom/miui/server/BackupManagerService;->mCurrentWorkingFeature:I

    .line 333
    iget v3, v1, Lmiui/app/backup/BackupFileResolver$BackupFileMiuiHeader;->userId:I

    iput v3, p0, Lcom/miui/server/BackupManagerService;->mAppUserId:I

    .line 334
    iget-boolean v3, v1, Lmiui/app/backup/BackupFileResolver$BackupFileMiuiHeader;->isEncrypted:Z

    if-eqz v3, :cond_0

    const/4 v3, 0x0

    goto :goto_0

    :cond_0
    iget-object v3, v1, Lmiui/app/backup/BackupFileResolver$BackupFileMiuiHeader;->encryptedPwd:Ljava/lang/String;

    :goto_0
    iput-object v3, p0, Lcom/miui/server/BackupManagerService;->mEncryptedPwdInBakFile:Ljava/lang/String;

    .line 336
    iget-object v3, p0, Lcom/miui/server/BackupManagerService;->mContext:Landroid/content/Context;

    iget-object v4, p0, Lcom/miui/server/BackupManagerService;->mCurrentWorkingPkg:Ljava/lang/String;

    invoke-static {v3, v4}, Lmiui/app/backup/BackupManager;->isSysAppForBackup(Landroid/content/Context;Ljava/lang/String;)Z

    move-result v3

    .line 337
    .local v3, "isSystemApp":Z
    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "readMiuiBackupHeader, BackupFileMiuiHeader:"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, " isSystemApp:"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v3}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, " mAppUserId:"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    iget v5, p0, Lcom/miui/server/BackupManagerService;->mAppUserId:I

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v2, v4}, Landroid/util/Slog;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 339
    if-nez v3, :cond_1

    .line 340
    iget-object v2, p0, Lcom/miui/server/BackupManagerService;->mCurrentWorkingPkg:Ljava/lang/String;

    iget v4, p0, Lcom/miui/server/BackupManagerService;->mAppUserId:I

    invoke-direct {p0, v2, v4}, Lcom/miui/server/BackupManagerService;->disablePackageAndWait(Ljava/lang/String;I)V

    .line 342
    .end local v3    # "isSystemApp":Z
    :cond_1
    goto :goto_1

    .line 344
    :cond_2
    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "readMiuiBackupHeader is error, header="

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Landroid/util/Slog;->e(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 347
    .end local v1    # "header":Lmiui/app/backup/BackupFileResolver$BackupFileMiuiHeader;
    :goto_1
    nop

    .line 349
    :try_start_2
    invoke-virtual {v0}, Ljava/io/InputStream;->close()V
    :try_end_2
    .catch Ljava/io/IOException; {:try_start_2 .. :try_end_2} :catch_0

    .line 352
    nop

    .line 355
    return-void

    .line 350
    :catch_0
    move-exception v1

    .line 351
    .local v1, "e":Ljava/io/IOException;
    new-instance v2, Ljava/lang/RuntimeException;

    invoke-direct {v2, v1}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/Throwable;)V

    throw v2

    .line 347
    .end local v1    # "e":Ljava/io/IOException;
    :catchall_0
    move-exception v1

    if-eqz v0, :cond_3

    .line 349
    :try_start_3
    invoke-virtual {v0}, Ljava/io/InputStream;->close()V
    :try_end_3
    .catch Ljava/io/IOException; {:try_start_3 .. :try_end_3} :catch_1

    .line 352
    goto :goto_2

    .line 350
    :catch_1
    move-exception v1

    .line 351
    .restart local v1    # "e":Ljava/io/IOException;
    new-instance v2, Ljava/lang/RuntimeException;

    invoke-direct {v2, v1}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/Throwable;)V

    throw v2

    .line 354
    .end local v1    # "e":Ljava/io/IOException;
    :cond_3
    :goto_2
    throw v1
.end method

.method public release(Lmiui/app/backup/IBackupServiceStateObserver;)V
    .locals 4
    .param p1, "stateObserver"    # Lmiui/app/backup/IBackupServiceStateObserver;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    .line 506
    const-string v0, "Backup:BackupManagerService"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Client tries to release service. CallingPid="

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-static {}, Landroid/os/Binder;->getCallingPid()I

    move-result v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " mOwnerPid="

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget v2, p0, Lcom/miui/server/BackupManagerService;->mOwnerPid:I

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Slog;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 507
    monitor-enter p0

    .line 508
    :try_start_0
    const-string v0, "Backup:BackupManagerService"

    const-string v1, "Client release service. Start canceling..."

    invoke-static {v0, v1}, Landroid/util/Slog;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 509
    if-eqz p1, :cond_0

    .line 510
    iget-object v0, p0, Lcom/miui/server/BackupManagerService;->mStateObservers:Landroid/os/RemoteCallbackList;

    invoke-virtual {v0, p1}, Landroid/os/RemoteCallbackList;->unregister(Landroid/os/IInterface;)Z

    .line 513
    :cond_0
    invoke-static {}, Landroid/os/Binder;->getCallingPid()I

    move-result v0

    .line 514
    .local v0, "pid":I
    iget v1, p0, Lcom/miui/server/BackupManagerService;->mOwnerPid:I

    if-ne v0, v1, :cond_1

    .line 515
    const/4 v1, 0x1

    iput-boolean v1, p0, Lcom/miui/server/BackupManagerService;->mIsCanceling:Z

    .line 516
    invoke-direct {p0}, Lcom/miui/server/BackupManagerService;->scheduleReleaseResource()V

    .line 517
    invoke-direct {p0}, Lcom/miui/server/BackupManagerService;->waitForTheLastWorkingTask()V

    .line 518
    const/4 v1, 0x0

    iput-boolean v1, p0, Lcom/miui/server/BackupManagerService;->mIsCanceling:Z

    .line 520
    const/4 v2, -0x1

    iput v2, p0, Lcom/miui/server/BackupManagerService;->mOwnerPid:I

    .line 521
    iget-object v2, p0, Lcom/miui/server/BackupManagerService;->mICaller:Landroid/os/IBinder;

    iget-object v3, p0, Lcom/miui/server/BackupManagerService;->mDeathLinker:Lcom/miui/server/BackupManagerService$DeathLinker;

    invoke-interface {v2, v3, v1}, Landroid/os/IBinder;->unlinkToDeath(Landroid/os/IBinder$DeathRecipient;I)Z

    .line 522
    const/4 v2, 0x0

    iput-object v2, p0, Lcom/miui/server/BackupManagerService;->mICaller:Landroid/os/IBinder;

    .line 523
    iput-object v2, p0, Lcom/miui/server/BackupManagerService;->mPreviousWorkingPkg:Ljava/lang/String;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 525
    :try_start_1
    invoke-direct {p0}, Lcom/miui/server/BackupManagerService;->broadcastServiceIdle()V
    :try_end_1
    .catch Landroid/os/RemoteException; {:try_start_1 .. :try_end_1} :catch_0
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 528
    goto :goto_0

    .line 526
    :catch_0
    move-exception v2

    .line 527
    .local v2, "e":Landroid/os/RemoteException;
    :try_start_2
    invoke-virtual {v2}, Landroid/os/RemoteException;->printStackTrace()V

    .line 529
    .end local v2    # "e":Landroid/os/RemoteException;
    :goto_0
    const-class v2, Lcom/miui/app/smartpower/SmartPowerServiceInternal;

    invoke-static {v2}, Lcom/android/server/LocalServices;->getService(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/miui/app/smartpower/SmartPowerServiceInternal;

    .line 530
    invoke-static {}, Landroid/os/Binder;->getCallingUid()I

    move-result v3

    .line 529
    invoke-interface {v2, v1, v3, v0}, Lcom/miui/app/smartpower/SmartPowerServiceInternal;->onBackupServiceAppChanged(ZII)V

    .line 532
    :cond_1
    const-string v1, "Backup:BackupManagerService"

    const-string v2, "Client release service. Cancel completed!"

    invoke-static {v1, v2}, Landroid/util/Slog;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 533
    nop

    .end local v0    # "pid":I
    monitor-exit p0

    .line 534
    return-void

    .line 533
    :catchall_0
    move-exception v0

    monitor-exit p0
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    throw v0
.end method

.method public restoreFile(Landroid/os/ParcelFileDescriptor;Ljava/lang/String;ZLmiui/app/backup/IPackageBackupRestoreObserver;)V
    .locals 9
    .param p1, "bakFd"    # Landroid/os/ParcelFileDescriptor;
    .param p2, "pwd"    # Ljava/lang/String;
    .param p3, "forceBackup"    # Z
    .param p4, "observer"    # Lmiui/app/backup/IPackageBackupRestoreObserver;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    .line 414
    iput-object p4, p0, Lcom/miui/server/BackupManagerService;->mBackupRestoreObserver:Lmiui/app/backup/IPackageBackupRestoreObserver;

    .line 416
    invoke-static {}, Lcom/miui/server/BackupManagerService;->getCallingPid()I

    move-result v0

    .line 417
    .local v0, "pid":I
    iget v1, p0, Lcom/miui/server/BackupManagerService;->mOwnerPid:I

    const/16 v2, 0x9

    if-eq v0, v1, :cond_0

    .line 418
    const-string v1, "Backup:BackupManagerService"

    const-string v3, "You must acquire first to use the backup or restore service"

    invoke-static {v1, v3}, Landroid/util/Slog;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 419
    invoke-virtual {p0, v2}, Lcom/miui/server/BackupManagerService;->errorOccur(I)V

    .line 420
    return-void

    .line 423
    :cond_0
    iget-object v1, p0, Lcom/miui/server/BackupManagerService;->mICaller:Landroid/os/IBinder;

    if-nez v1, :cond_1

    .line 424
    const-string v1, "Backup:BackupManagerService"

    const-string v3, "Caller is null You must acquire first with a binder"

    invoke-static {v1, v3}, Landroid/util/Slog;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 425
    invoke-virtual {p0, v2}, Lcom/miui/server/BackupManagerService;->errorOccur(I)V

    .line 426
    return-void

    .line 429
    :cond_1
    iget-object v1, p0, Lcom/miui/server/BackupManagerService;->mContext:Landroid/content/Context;

    invoke-direct {p0, v1}, Lcom/miui/server/BackupManagerService;->getDefaultIme(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v1

    .line 431
    .local v1, "defaultIme":Ljava/lang/String;
    iput-object p2, p0, Lcom/miui/server/BackupManagerService;->mPwd:Ljava/lang/String;

    .line 433
    const/4 v2, 0x0

    iput v2, p0, Lcom/miui/server/BackupManagerService;->mLastError:I

    .line 434
    iput v2, p0, Lcom/miui/server/BackupManagerService;->mProgType:I

    .line 435
    const/4 v3, -0x1

    iput v3, p0, Lcom/miui/server/BackupManagerService;->mPackageLastEnableState:I

    .line 436
    const/4 v4, 0x2

    iput v4, p0, Lcom/miui/server/BackupManagerService;->mState:I

    .line 438
    invoke-virtual {p1}, Landroid/os/ParcelFileDescriptor;->getStatSize()J

    move-result-wide v4

    iput-wide v4, p0, Lcom/miui/server/BackupManagerService;->mCurrentTotalSize:J

    .line 439
    const-wide/16 v4, 0x0

    iput-wide v4, p0, Lcom/miui/server/BackupManagerService;->mCurrentCompletedSize:J

    .line 440
    monitor-enter p0

    .line 441
    :try_start_0
    new-instance v4, Ljava/util/concurrent/atomic/AtomicBoolean;

    invoke-direct {v4, v2}, Ljava/util/concurrent/atomic/AtomicBoolean;-><init>(Z)V

    iput-object v4, p0, Lcom/miui/server/BackupManagerService;->mTaskLatch:Ljava/util/concurrent/atomic/AtomicBoolean;

    .line 442
    invoke-virtual {p1}, Landroid/os/ParcelFileDescriptor;->getFd()I

    move-result v4

    iput v4, p0, Lcom/miui/server/BackupManagerService;->mCallerFd:I

    .line 443
    const-string v4, "Backup:BackupManagerService"

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "restoreFile, MIUI FD is "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    iget v6, p0, Lcom/miui/server/BackupManagerService;->mCallerFd:I

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5}, Landroid/util/Slog;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 444
    monitor-exit p0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_2

    .line 445
    iget-object v4, p0, Lcom/miui/server/BackupManagerService;->mTaskLatch:Ljava/util/concurrent/atomic/AtomicBoolean;

    monitor-enter v4

    .line 447
    const/4 v5, 0x1

    :try_start_1
    iget v6, p0, Lcom/miui/server/BackupManagerService;->mOwnerPid:I

    if-eq v6, v3, :cond_2

    .line 448
    invoke-static {p1}, Lcom/miui/server/BackupManagerServiceProxy;->fullRestore(Landroid/os/ParcelFileDescriptor;)V

    goto :goto_0

    .line 450
    :cond_2
    const/16 v6, 0xa

    invoke-virtual {p0, v6}, Lcom/miui/server/BackupManagerService;->errorOccur(I)V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 453
    :goto_0
    :try_start_2
    const-string v6, "Backup:BackupManagerService"

    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    const-string v8, "restoreFile finish, MIUI FD is "

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    iget v8, p0, Lcom/miui/server/BackupManagerService;->mCallerFd:I

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-static {v6, v7}, Landroid/util/Slog;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 454
    iget-object v6, p0, Lcom/miui/server/BackupManagerService;->mTaskLatch:Ljava/util/concurrent/atomic/AtomicBoolean;

    invoke-virtual {v6, v5}, Ljava/util/concurrent/atomic/AtomicBoolean;->set(Z)V

    .line 455
    iget-object v5, p0, Lcom/miui/server/BackupManagerService;->mTaskLatch:Ljava/util/concurrent/atomic/AtomicBoolean;

    invoke-virtual {v5}, Ljava/lang/Object;->notifyAll()V

    .line 456
    nop

    .line 457
    monitor-exit v4
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 458
    iget-object v4, p0, Lcom/miui/server/BackupManagerService;->mContext:Landroid/content/Context;

    iget-object v5, p0, Lcom/miui/server/BackupManagerService;->mCurrentWorkingPkg:Ljava/lang/String;

    invoke-static {v4, v5}, Lmiui/app/backup/BackupManager;->isSysAppForBackup(Landroid/content/Context;Ljava/lang/String;)Z

    move-result v4

    if-nez v4, :cond_3

    .line 459
    iget-object v4, p0, Lcom/miui/server/BackupManagerService;->mCurrentWorkingPkg:Ljava/lang/String;

    iget v5, p0, Lcom/miui/server/BackupManagerService;->mAppUserId:I

    invoke-direct {p0, v4, v5, v1}, Lcom/miui/server/BackupManagerService;->enablePackage(Ljava/lang/String;ILjava/lang/String;)V

    .line 461
    :cond_3
    const/4 v4, 0x0

    iput-object v4, p0, Lcom/miui/server/BackupManagerService;->mPwd:Ljava/lang/String;

    .line 462
    iput-object v4, p0, Lcom/miui/server/BackupManagerService;->mEncryptedPwd:Ljava/lang/String;

    .line 463
    iput-object v4, p0, Lcom/miui/server/BackupManagerService;->mTaskLatch:Ljava/util/concurrent/atomic/AtomicBoolean;

    .line 464
    iput v3, p0, Lcom/miui/server/BackupManagerService;->mCallerFd:I

    .line 465
    iput v2, p0, Lcom/miui/server/BackupManagerService;->mProgType:I

    .line 466
    iput v2, p0, Lcom/miui/server/BackupManagerService;->mState:I

    .line 467
    iput v3, p0, Lcom/miui/server/BackupManagerService;->mPackageLastEnableState:I

    .line 468
    const-wide/16 v3, -0x1

    iput-wide v3, p0, Lcom/miui/server/BackupManagerService;->mCurrentTotalSize:J

    .line 469
    iget-object v3, p0, Lcom/miui/server/BackupManagerService;->mCurrentWorkingPkg:Ljava/lang/String;

    iput-object v3, p0, Lcom/miui/server/BackupManagerService;->mPreviousWorkingPkg:Ljava/lang/String;

    .line 470
    iput v2, p0, Lcom/miui/server/BackupManagerService;->mAppUserId:I

    .line 471
    return-void

    .line 453
    :catchall_0
    move-exception v2

    :try_start_3
    const-string v3, "Backup:BackupManagerService"

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string v7, "restoreFile finish, MIUI FD is "

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    iget v7, p0, Lcom/miui/server/BackupManagerService;->mCallerFd:I

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-static {v3, v6}, Landroid/util/Slog;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 454
    iget-object v3, p0, Lcom/miui/server/BackupManagerService;->mTaskLatch:Ljava/util/concurrent/atomic/AtomicBoolean;

    invoke-virtual {v3, v5}, Ljava/util/concurrent/atomic/AtomicBoolean;->set(Z)V

    .line 455
    iget-object v3, p0, Lcom/miui/server/BackupManagerService;->mTaskLatch:Ljava/util/concurrent/atomic/AtomicBoolean;

    invoke-virtual {v3}, Ljava/lang/Object;->notifyAll()V

    .line 456
    nop

    .end local v0    # "pid":I
    .end local v1    # "defaultIme":Ljava/lang/String;
    .end local p0    # "this":Lcom/miui/server/BackupManagerService;
    .end local p1    # "bakFd":Landroid/os/ParcelFileDescriptor;
    .end local p2    # "pwd":Ljava/lang/String;
    .end local p3    # "forceBackup":Z
    .end local p4    # "observer":Lmiui/app/backup/IPackageBackupRestoreObserver;
    throw v2

    .line 457
    .restart local v0    # "pid":I
    .restart local v1    # "defaultIme":Ljava/lang/String;
    .restart local p0    # "this":Lcom/miui/server/BackupManagerService;
    .restart local p1    # "bakFd":Landroid/os/ParcelFileDescriptor;
    .restart local p2    # "pwd":Ljava/lang/String;
    .restart local p3    # "forceBackup":Z
    .restart local p4    # "observer":Lmiui/app/backup/IPackageBackupRestoreObserver;
    :catchall_1
    move-exception v2

    monitor-exit v4
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    throw v2

    .line 444
    :catchall_2
    move-exception v2

    :try_start_4
    monitor-exit p0
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_2

    throw v2
.end method

.method public setCustomProgress(III)V
    .locals 8
    .param p1, "progType"    # I
    .param p2, "prog"    # I
    .param p3, "total"    # I
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    .line 395
    iput p1, p0, Lcom/miui/server/BackupManagerService;->mProgType:I

    .line 396
    iget-object v0, p0, Lcom/miui/server/BackupManagerService;->mBackupRestoreObserver:Lmiui/app/backup/IPackageBackupRestoreObserver;

    if-eqz v0, :cond_0

    .line 397
    iget-object v1, p0, Lcom/miui/server/BackupManagerService;->mCurrentWorkingPkg:Ljava/lang/String;

    iget v2, p0, Lcom/miui/server/BackupManagerService;->mCurrentWorkingFeature:I

    int-to-long v4, p2

    int-to-long v6, p3

    move v3, p1

    invoke-interface/range {v0 .. v7}, Lmiui/app/backup/IPackageBackupRestoreObserver;->onCustomProgressChange(Ljava/lang/String;IIJJ)V

    .line 399
    :cond_0
    return-void
.end method

.method public setFutureTask(Ljava/util/List;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List<",
            "Ljava/lang/String;",
            ">;)V"
        }
    .end annotation

    .annotation runtime Ljava/lang/Deprecated;
    .end annotation

    .line 272
    .local p1, "packages":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;"
    return-void
.end method

.method public setIsNeedBeKilled(Ljava/lang/String;Z)V
    .locals 2
    .param p1, "pkg"    # Ljava/lang/String;
    .param p2, "isNeedBeKilled"    # Z
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    .line 373
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v1, "setIsNeedBeKilled, pkg="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", isNeedBeKilled="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p2}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    const-string v1, "Backup:BackupManagerService"

    invoke-static {v1, v0}, Landroid/util/Slog;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 374
    iget-object v0, p0, Lcom/miui/server/BackupManagerService;->mNeedBeKilledPkgs:Ljava/util/HashMap;

    invoke-static {p2}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v1

    invoke-virtual {v0, p1, v1}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 375
    return-void
.end method

.method public shouldSkipData()Z
    .locals 1

    .line 824
    iget-boolean v0, p0, Lcom/miui/server/BackupManagerService;->mShouldSkipData:Z

    return v0
.end method

.method public startConfirmationUi(ILjava/lang/String;)V
    .locals 4
    .param p1, "token"    # I
    .param p2, "action"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    .line 290
    iget-object v0, p0, Lcom/miui/server/BackupManagerService;->mHandler:Landroid/os/Handler;

    new-instance v1, Lcom/miui/server/BackupManagerService$2;

    invoke-direct {v1, p0, p1}, Lcom/miui/server/BackupManagerService$2;-><init>(Lcom/miui/server/BackupManagerService;I)V

    .line 301
    iget-object v2, p0, Lcom/miui/server/BackupManagerService;->mPreviousWorkingPkg:Ljava/lang/String;

    if-eqz v2, :cond_0

    iget-object v3, p0, Lcom/miui/server/BackupManagerService;->mCurrentWorkingPkg:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_0

    const-wide/16 v2, 0x5dc

    goto :goto_0

    :cond_0
    const-wide/16 v2, 0x64

    .line 290
    :goto_0
    invoke-virtual {v0, v1, v2, v3}, Landroid/os/Handler;->postDelayed(Ljava/lang/Runnable;J)Z

    .line 302
    return-void
.end method

.method public writeMiuiBackupHeader(Landroid/os/ParcelFileDescriptor;)V
    .locals 8
    .param p1, "outFileDescriptor"    # Landroid/os/ParcelFileDescriptor;

    .line 306
    const/4 v0, 0x0

    .line 308
    .local v0, "os":Ljava/io/FileOutputStream;
    :try_start_0
    new-instance v1, Ljava/io/FileOutputStream;

    invoke-virtual {p1}, Landroid/os/ParcelFileDescriptor;->getFileDescriptor()Ljava/io/FileDescriptor;

    move-result-object v2

    invoke-direct {v1, v2}, Ljava/io/FileOutputStream;-><init>(Ljava/io/FileDescriptor;)V
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_2
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 309
    .end local v0    # "os":Ljava/io/FileOutputStream;
    .local v1, "os":Ljava/io/FileOutputStream;
    :try_start_1
    iget-object v2, p0, Lcom/miui/server/BackupManagerService;->mContext:Landroid/content/Context;

    iget-object v3, p0, Lcom/miui/server/BackupManagerService;->mCurrentWorkingPkg:Ljava/lang/String;

    iget v4, p0, Lcom/miui/server/BackupManagerService;->mCurrentWorkingFeature:I

    iget-object v5, p0, Lcom/miui/server/BackupManagerService;->mEncryptedPwd:Ljava/lang/String;

    iget v6, p0, Lcom/miui/server/BackupManagerService;->mAppUserId:I

    invoke-static/range {v1 .. v6}, Lmiui/app/backup/BackupFileResolver;->writeMiuiHeader(Ljava/io/OutputStream;Landroid/content/Context;Ljava/lang/String;ILjava/lang/String;I)V
    :try_end_1
    .catch Ljava/io/IOException; {:try_start_1 .. :try_end_1} :catch_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_1

    .line 313
    nop

    .line 315
    :try_start_2
    invoke-virtual {v1}, Ljava/io/FileOutputStream;->close()V
    :try_end_2
    .catch Ljava/io/IOException; {:try_start_2 .. :try_end_2} :catch_0

    .line 318
    nop

    .line 321
    return-void

    .line 316
    :catch_0
    move-exception v0

    .line 317
    .local v0, "e":Ljava/io/IOException;
    new-instance v2, Ljava/lang/RuntimeException;

    invoke-direct {v2, v0}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/Throwable;)V

    throw v2

    .line 310
    .end local v0    # "e":Ljava/io/IOException;
    :catch_1
    move-exception v0

    goto :goto_0

    .line 313
    .end local v1    # "os":Ljava/io/FileOutputStream;
    .local v0, "os":Ljava/io/FileOutputStream;
    :catchall_0
    move-exception v1

    move-object v7, v1

    move-object v1, v0

    move-object v0, v7

    goto :goto_1

    .line 310
    :catch_2
    move-exception v1

    move-object v7, v1

    move-object v1, v0

    move-object v0, v7

    .line 311
    .local v0, "e":Ljava/io/IOException;
    .restart local v1    # "os":Ljava/io/FileOutputStream;
    :goto_0
    :try_start_3
    new-instance v2, Ljava/lang/RuntimeException;

    invoke-direct {v2, v0}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/Throwable;)V

    .end local v1    # "os":Ljava/io/FileOutputStream;
    .end local p0    # "this":Lcom/miui/server/BackupManagerService;
    .end local p1    # "outFileDescriptor":Landroid/os/ParcelFileDescriptor;
    throw v2
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    .line 313
    .end local v0    # "e":Ljava/io/IOException;
    .restart local v1    # "os":Ljava/io/FileOutputStream;
    .restart local p0    # "this":Lcom/miui/server/BackupManagerService;
    .restart local p1    # "outFileDescriptor":Landroid/os/ParcelFileDescriptor;
    :catchall_1
    move-exception v0

    :goto_1
    if-eqz v1, :cond_0

    .line 315
    :try_start_4
    invoke-virtual {v1}, Ljava/io/FileOutputStream;->close()V
    :try_end_4
    .catch Ljava/io/IOException; {:try_start_4 .. :try_end_4} :catch_3

    .line 318
    goto :goto_2

    .line 316
    :catch_3
    move-exception v0

    .line 317
    .restart local v0    # "e":Ljava/io/IOException;
    new-instance v2, Ljava/lang/RuntimeException;

    invoke-direct {v2, v0}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/Throwable;)V

    throw v2

    .line 320
    .end local v0    # "e":Ljava/io/IOException;
    :cond_0
    :goto_2
    throw v0
.end method
