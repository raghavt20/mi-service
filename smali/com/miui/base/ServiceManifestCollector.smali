.class public Lcom/miui/base/ServiceManifestCollector;
.super Ljava/lang/Object;


# direct methods
.method public static collectManifests(Ljava/util/List;)V
    .locals 1

    new-instance v0, Lcom/android/server/ConsumerIrServiceStub$$;

    invoke-direct {v0}, Lcom/android/server/ConsumerIrServiceStub$$;-><init>()V

    invoke-interface {p0, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    new-instance v0, Lcom/android/server/ExtendMStub$$;

    invoke-direct {v0}, Lcom/android/server/ExtendMStub$$;-><init>()V

    invoke-interface {p0, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    new-instance v0, Lcom/android/server/MiuiBatteryServiceStub$$;

    invoke-direct {v0}, Lcom/android/server/MiuiBatteryServiceStub$$;-><init>()V

    invoke-interface {p0, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    new-instance v0, Lcom/android/server/MiuiCldStub$$;

    invoke-direct {v0}, Lcom/android/server/MiuiCldStub$$;-><init>()V

    invoke-interface {p0, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    new-instance v0, Lcom/android/server/MiuiCommonCloudServiceStub$$;

    invoke-direct {v0}, Lcom/android/server/MiuiCommonCloudServiceStub$$;-><init>()V

    invoke-interface {p0, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    new-instance v0, Lcom/android/server/MiuiPaperContrastOverlayStub$$;

    invoke-direct {v0}, Lcom/android/server/MiuiPaperContrastOverlayStub$$;-><init>()V

    invoke-interface {p0, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    new-instance v0, Lcom/android/server/MiuiStubImplManifest$$;

    invoke-direct {v0}, Lcom/android/server/MiuiStubImplManifest$$;-><init>()V

    invoke-interface {p0, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    new-instance v0, Lcom/android/server/MiuiUiModeManagerServiceStub$$;

    invoke-direct {v0}, Lcom/android/server/MiuiUiModeManagerServiceStub$$;-><init>()V

    invoke-interface {p0, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    new-instance v0, Lcom/android/server/NetworkManagementServiceStub$$;

    invoke-direct {v0}, Lcom/android/server/NetworkManagementServiceStub$$;-><init>()V

    invoke-interface {p0, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    new-instance v0, Lcom/android/server/WatchdogStub$$;

    invoke-direct {v0}, Lcom/android/server/WatchdogStub$$;-><init>()V

    invoke-interface {p0, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    new-instance v0, Lcom/android/server/accessibility/AccessibilityStateListenerStub$$;

    invoke-direct {v0}, Lcom/android/server/accessibility/AccessibilityStateListenerStub$$;-><init>()V

    invoke-interface {p0, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    new-instance v0, Lcom/android/server/accounts/AccountManagerServiceStub$$;

    invoke-direct {v0}, Lcom/android/server/accounts/AccountManagerServiceStub$$;-><init>()V

    invoke-interface {p0, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    new-instance v0, Lcom/android/server/alarm/AlarmManagerServiceStub$$;

    invoke-direct {v0}, Lcom/android/server/alarm/AlarmManagerServiceStub$$;-><init>()V

    invoke-interface {p0, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    new-instance v0, Lcom/android/server/am/ActivityManagerServiceStub$$;

    invoke-direct {v0}, Lcom/android/server/am/ActivityManagerServiceStub$$;-><init>()V

    invoke-interface {p0, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    new-instance v0, Lcom/android/server/am/SystemPressureControllerStub$$;

    invoke-direct {v0}, Lcom/android/server/am/SystemPressureControllerStub$$;-><init>()V

    invoke-interface {p0, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    new-instance v0, Lcom/android/server/app/GameManagerServiceStub$$;

    invoke-direct {v0}, Lcom/android/server/app/GameManagerServiceStub$$;-><init>()V

    invoke-interface {p0, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    new-instance v0, Lcom/android/server/app/PreCacheUtilStubImpl$$;

    invoke-direct {v0}, Lcom/android/server/app/PreCacheUtilStubImpl$$;-><init>()V

    invoke-interface {p0, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    new-instance v0, Lcom/android/server/appop/AppOpsServiceStub$$;

    invoke-direct {v0}, Lcom/android/server/appop/AppOpsServiceStub$$;-><init>()V

    invoke-interface {p0, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    new-instance v0, Lcom/android/server/appwidget/MiuiAppWidgetServiceStub$$;

    invoke-direct {v0}, Lcom/android/server/appwidget/MiuiAppWidgetServiceStub$$;-><init>()V

    invoke-interface {p0, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    new-instance v0, Lcom/android/server/audio/AudioServiceStub$$;

    invoke-direct {v0}, Lcom/android/server/audio/AudioServiceStub$$;-><init>()V

    invoke-interface {p0, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    new-instance v0, Lcom/android/server/autofill/ui/SaveUiStub$$;

    invoke-direct {v0}, Lcom/android/server/autofill/ui/SaveUiStub$$;-><init>()V

    invoke-interface {p0, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    new-instance v0, Lcom/android/server/backup/BackupManagerServiceStub$$;

    invoke-direct {v0}, Lcom/android/server/backup/BackupManagerServiceStub$$;-><init>()V

    invoke-interface {p0, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    new-instance v0, Lcom/android/server/backup/SystemBackupAgentStub$$;

    invoke-direct {v0}, Lcom/android/server/backup/SystemBackupAgentStub$$;-><init>()V

    invoke-interface {p0, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    new-instance v0, Lcom/android/server/biometrics/sensors/fingerprint/FingerprintServiceStub$$;

    invoke-direct {v0}, Lcom/android/server/biometrics/sensors/fingerprint/FingerprintServiceStub$$;-><init>()V

    invoke-interface {p0, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    new-instance v0, Lcom/android/server/camera/CameraActivitySceneStub$$;

    invoke-direct {v0}, Lcom/android/server/camera/CameraActivitySceneStub$$;-><init>()V

    invoke-interface {p0, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    new-instance v0, Lcom/android/server/clipboard/ClipboardServiceStub$$;

    invoke-direct {v0}, Lcom/android/server/clipboard/ClipboardServiceStub$$;-><init>()V

    invoke-interface {p0, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    new-instance v0, Lcom/android/server/content/ContentServiceStub$$;

    invoke-direct {v0}, Lcom/android/server/content/ContentServiceStub$$;-><init>()V

    invoke-interface {p0, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    new-instance v0, Lcom/android/server/devicepolicy/DevicePolicyManagerServiceStub$$;

    invoke-direct {v0}, Lcom/android/server/devicepolicy/DevicePolicyManagerServiceStub$$;-><init>()V

    invoke-interface {p0, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    new-instance v0, Lcom/android/server/devicestate/DeviceStateManagerServiceStub$$;

    invoke-direct {v0}, Lcom/android/server/devicestate/DeviceStateManagerServiceStub$$;-><init>()V

    invoke-interface {p0, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    new-instance v0, Lcom/android/server/display/DisplayManagerServiceStub$$;

    invoke-direct {v0}, Lcom/android/server/display/DisplayManagerServiceStub$$;-><init>()V

    invoke-interface {p0, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    new-instance v0, Lcom/android/server/inputmethod/InputMethodManagerServiceStub$$;

    invoke-direct {v0}, Lcom/android/server/inputmethod/InputMethodManagerServiceStub$$;-><init>()V

    invoke-interface {p0, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    new-instance v0, Lcom/android/server/job/JobServiceContextStub$$;

    invoke-direct {v0}, Lcom/android/server/job/JobServiceContextStub$$;-><init>()V

    invoke-interface {p0, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    new-instance v0, Lcom/android/server/lights/LightsManagerStub$$;

    invoke-direct {v0}, Lcom/android/server/lights/LightsManagerStub$$;-><init>()V

    invoke-interface {p0, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    new-instance v0, Lcom/android/server/location/gnss/exp/GnssBackgroundUsageOptStub$$;

    invoke-direct {v0}, Lcom/android/server/location/gnss/exp/GnssBackgroundUsageOptStub$$;-><init>()V

    invoke-interface {p0, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    new-instance v0, Lcom/android/server/locksettings/LockSettingsStub$$;

    invoke-direct {v0}, Lcom/android/server/locksettings/LockSettingsStub$$;-><init>()V

    invoke-interface {p0, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    new-instance v0, Lcom/android/server/logcat/LogcatManagerServiceStub$$;

    invoke-direct {v0}, Lcom/android/server/logcat/LogcatManagerServiceStub$$;-><init>()V

    invoke-interface {p0, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    new-instance v0, Lcom/android/server/media/MediaServiceStub$$;

    invoke-direct {v0}, Lcom/android/server/media/MediaServiceStub$$;-><init>()V

    invoke-interface {p0, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    new-instance v0, Lcom/android/server/notification/NotificationManagerServiceStub$$;

    invoke-direct {v0}, Lcom/android/server/notification/NotificationManagerServiceStub$$;-><init>()V

    invoke-interface {p0, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    new-instance v0, Lcom/android/server/pm/PackageManagerServiceStub$$;

    invoke-direct {v0}, Lcom/android/server/pm/PackageManagerServiceStub$$;-><init>()V

    invoke-interface {p0, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    new-instance v0, Lcom/android/server/power/PowerManagerServiceStub$$;

    invoke-direct {v0}, Lcom/android/server/power/PowerManagerServiceStub$$;-><init>()V

    invoke-interface {p0, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    new-instance v0, Lcom/android/server/print/UserState$$;

    invoke-direct {v0}, Lcom/android/server/print/UserState$$;-><init>()V

    invoke-interface {p0, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    new-instance v0, Lcom/android/server/sensors/SensorServiceStub$$;

    invoke-direct {v0}, Lcom/android/server/sensors/SensorServiceStub$$;-><init>()V

    invoke-interface {p0, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    new-instance v0, Lcom/android/server/statusbar/StatusBarManagerServiceStub$$;

    invoke-direct {v0}, Lcom/android/server/statusbar/StatusBarManagerServiceStub$$;-><init>()V

    invoke-interface {p0, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    new-instance v0, Lcom/android/server/storage/DeviceStorageMonitorServiceStub$$;

    invoke-direct {v0}, Lcom/android/server/storage/DeviceStorageMonitorServiceStub$$;-><init>()V

    invoke-interface {p0, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    new-instance v0, Lcom/android/server/testharness/TestHarnessModeServiceStub$$;

    invoke-direct {v0}, Lcom/android/server/testharness/TestHarnessModeServiceStub$$;-><init>()V

    invoke-interface {p0, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    new-instance v0, Lcom/android/server/usage/UsageStatsServiceStub$$;

    invoke-direct {v0}, Lcom/android/server/usage/UsageStatsServiceStub$$;-><init>()V

    invoke-interface {p0, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    new-instance v0, Lcom/android/server/usb/MiuiUsbServiceStub$$;

    invoke-direct {v0}, Lcom/android/server/usb/MiuiUsbServiceStub$$;-><init>()V

    invoke-interface {p0, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    new-instance v0, Lcom/android/server/vibrator/VibratorManagerServiceStub$$;

    invoke-direct {v0}, Lcom/android/server/vibrator/VibratorManagerServiceStub$$;-><init>()V

    invoke-interface {p0, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    new-instance v0, Lcom/android/server/wallpaper/WallpaperManagerServiceStub$$;

    invoke-direct {v0}, Lcom/android/server/wallpaper/WallpaperManagerServiceStub$$;-><init>()V

    invoke-interface {p0, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    new-instance v0, Lcom/android/server/wm/AppResurrectionServiceStub$$;

    invoke-direct {v0}, Lcom/android/server/wm/AppResurrectionServiceStub$$;-><init>()V

    invoke-interface {p0, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    new-instance v0, Lcom/android/server/wm/BoundsCompatControllerStub$$;

    invoke-direct {v0}, Lcom/android/server/wm/BoundsCompatControllerStub$$;-><init>()V

    invoke-interface {p0, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    new-instance v0, Lcom/android/server/wm/MultiSenceManagerInternalStub$$;

    invoke-direct {v0}, Lcom/android/server/wm/MultiSenceManagerInternalStub$$;-><init>()V

    invoke-interface {p0, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    new-instance v0, Lcom/miui/server/security/SafetyDetectManagerStub$$;

    invoke-direct {v0}, Lcom/miui/server/security/SafetyDetectManagerStub$$;-><init>()V

    invoke-interface {p0, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    new-instance v0, Lcom/miui/server/smartpower/SmartPowerServiceStub$$;

    invoke-direct {v0}, Lcom/miui/server/smartpower/SmartPowerServiceStub$$;-><init>()V

    invoke-interface {p0, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    new-instance v0, Lcom/miui/server/sptm/SpeedTestModeServiceStub$$;

    invoke-direct {v0}, Lcom/miui/server/sptm/SpeedTestModeServiceStub$$;-><init>()V

    invoke-interface {p0, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    new-instance v0, Lcom/miui/server/xspace/XSpaceManagerServiceStub$$;

    invoke-direct {v0}, Lcom/miui/server/xspace/XSpaceManagerServiceStub$$;-><init>()V

    invoke-interface {p0, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    new-instance v0, Lcom/xiaomi/mirror/MirrorServiceStub$$;

    invoke-direct {v0}, Lcom/xiaomi/mirror/MirrorServiceStub$$;-><init>()V

    invoke-interface {p0, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    return-void
.end method
