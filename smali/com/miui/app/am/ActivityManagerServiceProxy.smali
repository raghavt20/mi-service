.class public Lcom/miui/app/am/ActivityManagerServiceProxy;
.super Ljava/lang/Object;
.source "ActivityManagerServiceProxy.java"


# static fields
.field public static collectProcesses:Lcom/xiaomi/reflect/RefMethod;
    .annotation runtime Lcom/xiaomi/reflect/annotation/MethodArguments;
        cls = {
            Ljava/io/PrintWriter;,
            I,
            Z,
            [Ljava/lang/String;
        }
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/xiaomi/reflect/RefMethod<",
            "Ljava/util/ArrayList<",
            "Lcom/android/server/am/ProcessRecord;",
            ">;>;"
        }
    .end annotation
.end field

.field public static mProcLock:Lcom/xiaomi/reflect/RefObject;
    .annotation runtime Lcom/xiaomi/reflect/annotation/FieldArguments;
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .locals 2

    .line 16
    const-class v0, Lcom/miui/app/am/ActivityManagerServiceProxy;

    const-string v1, "com.android.server.am.ActivityManagerService"

    invoke-static {v0, v1}, Lcom/xiaomi/reflect/RefClass;->attach(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Class;

    .line 17
    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .line 13
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method
