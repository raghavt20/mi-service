.class public Lcom/miui/app/am/ProcessRecordProxy;
.super Ljava/lang/Object;
.source "ProcessRecordProxy.java"


# static fields
.field public static getPid:Lcom/xiaomi/reflect/RefMethod;
    .annotation runtime Lcom/xiaomi/reflect/annotation/MethodArguments;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/xiaomi/reflect/RefMethod<",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation
.end field

.field public static getPkgDeps:Lcom/xiaomi/reflect/RefMethod;
    .annotation runtime Lcom/xiaomi/reflect/annotation/MethodArguments;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/xiaomi/reflect/RefMethod<",
            "Landroid/util/ArraySet<",
            "Ljava/lang/String;",
            ">;>;"
        }
    .end annotation
.end field

.field public static info:Lcom/xiaomi/reflect/RefObject;
    .annotation runtime Lcom/xiaomi/reflect/annotation/FieldArguments;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/xiaomi/reflect/RefObject<",
            "Landroid/content/pm/ApplicationInfo;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .locals 2

    .line 18
    const-class v0, Lcom/miui/app/am/ProcessRecordProxy;

    const-string v1, "com.android.server.am.ProcessRecord"

    invoke-static {v0, v1}, Lcom/xiaomi/reflect/RefClass;->attach(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Class;

    .line 19
    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .line 15
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method
