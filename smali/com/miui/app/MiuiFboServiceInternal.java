public abstract class com.miui.app.MiuiFboServiceInternal {
	 /* .source "MiuiFboServiceInternal.java" */
	 /* # virtual methods */
	 public abstract void deliverMessage ( java.lang.String p0, Integer p1, Long p2 ) {
	 } // .end method
	 public abstract Boolean getDueToScreenWait ( ) {
	 } // .end method
	 public abstract Boolean getGlobalSwitch ( ) {
	 } // .end method
	 public abstract Boolean getNativeIsRunning ( ) {
	 } // .end method
	 public abstract void setBatteryInfos ( Integer p0, Integer p1, Integer p2 ) {
	 } // .end method
	 public abstract void setScreenStatus ( Boolean p0 ) {
	 } // .end method
