public abstract class com.miui.app.SpeedTestModeServiceInternal {
	 /* .source "SpeedTestModeServiceInternal.java" */
	 /* # virtual methods */
	 public abstract void init ( android.content.Context p0 ) {
	 } // .end method
	 public abstract Boolean isSpeedTestMode ( ) {
	 } // .end method
	 public abstract void onBootPhase ( ) {
	 } // .end method
	 public abstract void reportOneKeyCleanEvent ( ) {
	 } // .end method
