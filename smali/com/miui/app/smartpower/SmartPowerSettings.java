public class com.miui.app.smartpower.SmartPowerSettings {
	 /* .source "SmartPowerSettings.java" */
	 /* # static fields */
	 public static Boolean APP_STATE_ENABLE;
	 public static final java.lang.String APP_STATE_PROP;
	 public static final Boolean DEBUG_ALL;
	 public static final Boolean DEBUG_PROC;
	 public static final Long DEF_LAST_INTER_ACTIVE_DURATION;
	 public static final Long DEF_RES_NET_ACTIVE_SPEED;
	 public static final Long DEF_RES_NET_MONITOR_PERIOD;
	 public static Boolean DISPLAY_POLICY_ENABLE;
	 public static final java.lang.String DISPLAY_POLICY_PROP;
	 public static final Integer EVENT_TAGS;
	 public static final java.lang.String FROZEN_PROP;
	 public static final java.lang.String GAME_PAY_PROTECT;
	 public static Boolean GAME_PAY_PROTECT_ENABLED;
	 public static final Long HIBERNATE_DURATION;
	 public static final Long IDLE_DURATION;
	 public static final Long INACTIVE_DURATION;
	 public static final Long INACTIVE_SPTM_DURATION;
	 public static final java.lang.String INTERCEPT_PROP;
	 public static final Long MAINTENANCE_DURATION;
	 public static final Long MAX_HIBERNATE_DURATION;
	 public static final Long MAX_HISTORY_REPORT_DURATION;
	 public static final Integer MAX_HISTORY_REPORT_SIZE;
	 public static Long PROC_MEM_LVL1_PSS_LIMIT_KB;
	 public static Long PROC_MEM_LVL2_PSS_LIMIT_KB;
	 public static Long PROC_MEM_LVL3_PSS_LIMIT_KB;
	 private static final java.lang.String PROPERTY_PREFIX;
	 public static final Boolean PROP_CAMERA_REFRESH_RATE_ENABLE;
	 public static final Boolean PROP_FROZEN_CGROUPV1_ENABLE;
	 public static Boolean PROP_FROZEN_ENABLE;
	 public static Boolean PROP_INTERCEPT_ENABLE;
	 public static final Integer PROP_LIMIT_MAX_REFRESH_RATE;
	 public static final Integer PROP_THERMAL_TEMP_THRESHOLD;
	 public static final java.lang.String TIME_FORMAT_PATTERN;
	 public static final Long UPDATE_USERSTATS_DURATION;
	 public static final java.lang.String USB_DATA_TRANS_PROC;
	 public static Boolean WINDOW_POLICY_ENABLE;
	 public static final java.lang.String WINDOW_POLICY_PROP;
	 /* # direct methods */
	 static com.miui.app.smartpower.SmartPowerSettings ( ) {
		 /* .locals 6 */
		 /* .line 11 */
		 /* nop */
		 /* .line 12 */
		 final String v0 = "persist.sys.smartpower.debug"; // const-string v0, "persist.sys.smartpower.debug"
		 int v1 = 0; // const/4 v1, 0x0
		 v0 = 		 android.os.SystemProperties .getBoolean ( v0,v1 );
		 com.miui.app.smartpower.SmartPowerSettings.DEBUG_ALL = (v0!= 0);
		 /* .line 13 */
		 /* nop */
		 /* .line 14 */
		 final String v0 = "persist.sys.smartpower.debug.proc"; // const-string v0, "persist.sys.smartpower.debug.proc"
		 v0 = 		 android.os.SystemProperties .getBoolean ( v0,v1 );
		 com.miui.app.smartpower.SmartPowerSettings.DEBUG_PROC = (v0!= 0);
		 /* .line 16 */
		 /* nop */
		 /* .line 17 */
		 final String v0 = "persist.sys.millet.cgroup"; // const-string v0, "persist.sys.millet.cgroup"
		 v0 = 		 android.os.SystemProperties .getBoolean ( v0,v1 );
		 com.miui.app.smartpower.SmartPowerSettings.PROP_FROZEN_CGROUPV1_ENABLE = (v0!= 0);
		 /* .line 21 */
		 final String v0 = "persist.sys.smartpower.appstate.enable"; // const-string v0, "persist.sys.smartpower.appstate.enable"
		 int v2 = 1; // const/4 v2, 0x1
		 v0 = 		 android.os.SystemProperties .getBoolean ( v0,v2 );
		 com.miui.app.smartpower.SmartPowerSettings.APP_STATE_ENABLE = (v0!= 0);
		 /* .line 25 */
		 final String v0 = "persist.sys.smartpower.fz.enable"; // const-string v0, "persist.sys.smartpower.fz.enable"
		 v0 = 		 android.os.SystemProperties .getBoolean ( v0,v1 );
		 com.miui.app.smartpower.SmartPowerSettings.PROP_FROZEN_ENABLE = (v0!= 0);
		 /* .line 29 */
		 /* nop */
		 /* .line 30 */
		 final String v0 = "persist.sys.smartpower.intercept.enable"; // const-string v0, "persist.sys.smartpower.intercept.enable"
		 v0 = 		 android.os.SystemProperties .getBoolean ( v0,v1 );
		 com.miui.app.smartpower.SmartPowerSettings.PROP_INTERCEPT_ENABLE = (v0!= 0);
		 /* .line 34 */
		 final String v0 = "persist.sys.smartpower.display.enable"; // const-string v0, "persist.sys.smartpower.display.enable"
		 v0 = 		 android.os.SystemProperties .getBoolean ( v0,v1 );
		 com.miui.app.smartpower.SmartPowerSettings.DISPLAY_POLICY_ENABLE = (v0!= 0);
		 /* .line 39 */
		 final String v0 = "persist.sys.smartpower.window.enable"; // const-string v0, "persist.sys.smartpower.window.enable"
		 v0 = 		 android.os.SystemProperties .getBoolean ( v0,v2 );
		 com.miui.app.smartpower.SmartPowerSettings.WINDOW_POLICY_ENABLE = (v0!= 0);
		 /* .line 42 */
		 /* nop */
		 /* .line 43 */
		 final String v0 = "persist.sys.smartpower.history.dur"; // const-string v0, "persist.sys.smartpower.history.dur"
		 /* const-wide/32 v3, 0xdbba00 */
		 android.os.SystemProperties .getLong ( v0,v3,v4 );
		 /* move-result-wide v3 */
		 /* sput-wide v3, Lcom/miui/app/smartpower/SmartPowerSettings;->MAX_HISTORY_REPORT_DURATION:J */
		 /* .line 44 */
		 /* nop */
		 /* .line 45 */
		 final String v0 = "persist.sys.smartpower.history.size"; // const-string v0, "persist.sys.smartpower.history.size"
		 /* const/16 v3, 0x64 */
		 v0 = 		 android.os.SystemProperties .getInt ( v0,v3 );
		 /* .line 48 */
		 /* nop */
		 /* .line 49 */
		 final String v0 = "persist.sys.smartpower.gamepay.protect.enabled"; // const-string v0, "persist.sys.smartpower.gamepay.protect.enabled"
		 v0 = 		 android.os.SystemProperties .getBoolean ( v0,v2 );
		 com.miui.app.smartpower.SmartPowerSettings.GAME_PAY_PROTECT_ENABLED = (v0!= 0);
		 /* .line 56 */
		 /* nop */
		 /* .line 57 */
		 final String v0 = "persist.sys.smartpower.inactive.dur"; // const-string v0, "persist.sys.smartpower.inactive.dur"
		 /* const-wide/16 v2, 0xbb8 */
		 android.os.SystemProperties .getLong ( v0,v2,v3 );
		 /* move-result-wide v2 */
		 /* sput-wide v2, Lcom/miui/app/smartpower/SmartPowerSettings;->INACTIVE_DURATION:J */
		 /* .line 58 */
		 /* nop */
		 /* .line 59 */
		 final String v0 = "persist.sys.smartpower.inactive.sptm.dur"; // const-string v0, "persist.sys.smartpower.inactive.sptm.dur"
		 /* const-wide/16 v2, 0x7d0 */
		 android.os.SystemProperties .getLong ( v0,v2,v3 );
		 /* move-result-wide v4 */
		 /* sput-wide v4, Lcom/miui/app/smartpower/SmartPowerSettings;->INACTIVE_SPTM_DURATION:J */
		 /* .line 64 */
		 /* nop */
		 /* .line 65 */
		 final String v0 = "persist.sys.smartpower.hibernate.dur"; // const-string v0, "persist.sys.smartpower.hibernate.dur"
		 /* const-wide/32 v4, 0x927c0 */
		 android.os.SystemProperties .getLong ( v0,v4,v5 );
		 /* move-result-wide v4 */
		 /* sput-wide v4, Lcom/miui/app/smartpower/SmartPowerSettings;->HIBERNATE_DURATION:J */
		 /* .line 70 */
		 /* nop */
		 /* .line 71 */
		 final String v0 = "persist.sys.smartpower.hibernate.max.dur"; // const-string v0, "persist.sys.smartpower.hibernate.max.dur"
		 /* const-wide/32 v4, 0x1b7740 */
		 android.os.SystemProperties .getLong ( v0,v4,v5 );
		 /* move-result-wide v4 */
		 /* sput-wide v4, Lcom/miui/app/smartpower/SmartPowerSettings;->MAX_HIBERNATE_DURATION:J */
		 /* .line 77 */
		 /* nop */
		 /* .line 78 */
		 final String v0 = "persist.sys.smartpower.idle.dur"; // const-string v0, "persist.sys.smartpower.idle.dur"
		 android.os.SystemProperties .getLong ( v0,v2,v3 );
		 /* move-result-wide v4 */
		 /* sput-wide v4, Lcom/miui/app/smartpower/SmartPowerSettings;->IDLE_DURATION:J */
		 /* .line 84 */
		 /* nop */
		 /* .line 85 */
		 final String v0 = "persist.sys.smartpower.maintenance.dur"; // const-string v0, "persist.sys.smartpower.maintenance.dur"
		 android.os.SystemProperties .getLong ( v0,v2,v3 );
		 /* move-result-wide v2 */
		 /* sput-wide v2, Lcom/miui/app/smartpower/SmartPowerSettings;->MAINTENANCE_DURATION:J */
		 /* .line 90 */
		 /* nop */
		 /* .line 91 */
		 final String v0 = "persist.sys.smartpower.update.userstats.dur"; // const-string v0, "persist.sys.smartpower.update.userstats.dur"
		 /* const-wide/32 v2, 0x36ee80 */
		 android.os.SystemProperties .getLong ( v0,v2,v3 );
		 /* move-result-wide v2 */
		 /* sput-wide v2, Lcom/miui/app/smartpower/SmartPowerSettings;->UPDATE_USERSTATS_DURATION:J */
		 /* .line 97 */
		 /* nop */
		 /* .line 98 */
		 final String v0 = "persist.sys.smartpower.res.netactive.speed"; // const-string v0, "persist.sys.smartpower.res.netactive.speed"
		 /* const/16 v2, 0x1f4 */
		 v0 = 		 android.os.SystemProperties .getInt ( v0,v2 );
		 /* int-to-long v2, v0 */
		 /* sput-wide v2, Lcom/miui/app/smartpower/SmartPowerSettings;->DEF_RES_NET_ACTIVE_SPEED:J */
		 /* .line 103 */
		 /* nop */
		 /* .line 104 */
		 final String v0 = "persist.sys.smartpower.res.netmonitor.periodic"; // const-string v0, "persist.sys.smartpower.res.netmonitor.periodic"
		 /* const/16 v2, 0x3e8 */
		 v0 = 		 android.os.SystemProperties .getInt ( v0,v2 );
		 /* int-to-long v2, v0 */
		 /* sput-wide v2, Lcom/miui/app/smartpower/SmartPowerSettings;->DEF_RES_NET_MONITOR_PERIOD:J */
		 /* .line 106 */
		 /* nop */
		 /* .line 107 */
		 final String v0 = "persist.sys.smartpower.lastinteractive.dur"; // const-string v0, "persist.sys.smartpower.lastinteractive.dur"
		 /* const/16 v2, 0x1388 */
		 v0 = 		 android.os.SystemProperties .getInt ( v0,v2 );
		 /* int-to-long v2, v0 */
		 /* sput-wide v2, Lcom/miui/app/smartpower/SmartPowerSettings;->DEF_LAST_INTER_ACTIVE_DURATION:J */
		 /* .line 109 */
		 /* nop */
		 /* .line 110 */
		 final String v0 = "persist.sys.smartpower.memthreshold.app.lvl1"; // const-string v0, "persist.sys.smartpower.memthreshold.app.lvl1"
		 /* const-wide/32 v2, 0x96000 */
		 android.os.SystemProperties .getLong ( v0,v2,v3 );
		 /* move-result-wide v2 */
		 /* sput-wide v2, Lcom/miui/app/smartpower/SmartPowerSettings;->PROC_MEM_LVL1_PSS_LIMIT_KB:J */
		 /* .line 111 */
		 /* nop */
		 /* .line 112 */
		 final String v0 = "persist.sys.smartpower.memthreshold.app.lvl2"; // const-string v0, "persist.sys.smartpower.memthreshold.app.lvl2"
		 /* const-wide/32 v2, 0xc8000 */
		 android.os.SystemProperties .getLong ( v0,v2,v3 );
		 /* move-result-wide v2 */
		 /* sput-wide v2, Lcom/miui/app/smartpower/SmartPowerSettings;->PROC_MEM_LVL2_PSS_LIMIT_KB:J */
		 /* .line 113 */
		 /* nop */
		 /* .line 114 */
		 final String v0 = "persist.sys.smartpower.memthreshold.app.lvl3"; // const-string v0, "persist.sys.smartpower.memthreshold.app.lvl3"
		 /* const-wide/32 v2, 0x100000 */
		 android.os.SystemProperties .getLong ( v0,v2,v3 );
		 /* move-result-wide v2 */
		 /* sput-wide v2, Lcom/miui/app/smartpower/SmartPowerSettings;->PROC_MEM_LVL3_PSS_LIMIT_KB:J */
		 /* .line 121 */
		 final String v0 = "persist.sys.smartpower.limit.max.refresh.rate"; // const-string v0, "persist.sys.smartpower.limit.max.refresh.rate"
		 int v2 = -1; // const/4 v2, -0x1
		 v0 = 		 android.os.SystemProperties .getInt ( v0,v2 );
		 /* .line 128 */
		 final String v0 = "persist.sys.smartpower.display_camera_fps_enable"; // const-string v0, "persist.sys.smartpower.display_camera_fps_enable"
		 v0 = 		 android.os.SystemProperties .getBoolean ( v0,v1 );
		 com.miui.app.smartpower.SmartPowerSettings.PROP_CAMERA_REFRESH_RATE_ENABLE = (v0!= 0);
		 /* .line 136 */
		 final String v0 = "persist.sys.smartpower.display_thermal_temp_threshold"; // const-string v0, "persist.sys.smartpower.display_thermal_temp_threshold"
		 /* const/16 v1, 0x2e */
		 v0 = 		 android.os.SystemProperties .getInt ( v0,v1 );
		 return;
	 } // .end method
	 public com.miui.app.smartpower.SmartPowerSettings ( ) {
		 /* .locals 0 */
		 /* .line 5 */
		 /* invoke-direct {p0}, Ljava/lang/Object;-><init>()V */
		 return;
	 } // .end method
