.class public Lcom/miui/app/smartpower/SmartPowerSettings;
.super Ljava/lang/Object;
.source "SmartPowerSettings.java"


# static fields
.field public static APP_STATE_ENABLE:Z = false

.field public static final APP_STATE_PROP:Ljava/lang/String; = "persist.sys.smartpower.appstate.enable"

.field public static final DEBUG_ALL:Z

.field public static final DEBUG_PROC:Z

.field public static final DEF_LAST_INTER_ACTIVE_DURATION:J

.field public static final DEF_RES_NET_ACTIVE_SPEED:J

.field public static final DEF_RES_NET_MONITOR_PERIOD:J

.field public static DISPLAY_POLICY_ENABLE:Z = false

.field public static final DISPLAY_POLICY_PROP:Ljava/lang/String; = "persist.sys.smartpower.display.enable"

.field public static final EVENT_TAGS:I = 0x15ff2

.field public static final FROZEN_PROP:Ljava/lang/String; = "persist.sys.smartpower.fz.enable"

.field public static final GAME_PAY_PROTECT:Ljava/lang/String; = "persist.sys.smartpower.gamepay.protect.enabled"

.field public static GAME_PAY_PROTECT_ENABLED:Z = false

.field public static final HIBERNATE_DURATION:J

.field public static final IDLE_DURATION:J

.field public static final INACTIVE_DURATION:J

.field public static final INACTIVE_SPTM_DURATION:J

.field public static final INTERCEPT_PROP:Ljava/lang/String; = "persist.sys.smartpower.intercept.enable"

.field public static final MAINTENANCE_DURATION:J

.field public static final MAX_HIBERNATE_DURATION:J

.field public static final MAX_HISTORY_REPORT_DURATION:J

.field public static final MAX_HISTORY_REPORT_SIZE:I

.field public static PROC_MEM_LVL1_PSS_LIMIT_KB:J = 0x0L

.field public static PROC_MEM_LVL2_PSS_LIMIT_KB:J = 0x0L

.field public static PROC_MEM_LVL3_PSS_LIMIT_KB:J = 0x0L

.field private static final PROPERTY_PREFIX:Ljava/lang/String; = "persist.sys.smartpower."

.field public static final PROP_CAMERA_REFRESH_RATE_ENABLE:Z

.field public static final PROP_FROZEN_CGROUPV1_ENABLE:Z

.field public static PROP_FROZEN_ENABLE:Z = false

.field public static PROP_INTERCEPT_ENABLE:Z = false

.field public static final PROP_LIMIT_MAX_REFRESH_RATE:I

.field public static final PROP_THERMAL_TEMP_THRESHOLD:I

.field public static final TIME_FORMAT_PATTERN:Ljava/lang/String; = "HH:mm:ss.SSS"

.field public static final UPDATE_USERSTATS_DURATION:J

.field public static final USB_DATA_TRANS_PROC:Ljava/lang/String; = "android.process.media"

.field public static WINDOW_POLICY_ENABLE:Z = false

.field public static final WINDOW_POLICY_PROP:Ljava/lang/String; = "persist.sys.smartpower.window.enable"


# direct methods
.method static constructor <clinit>()V
    .locals 6

    .line 11
    nop

    .line 12
    const-string v0, "persist.sys.smartpower.debug"

    const/4 v1, 0x0

    invoke-static {v0, v1}, Landroid/os/SystemProperties;->getBoolean(Ljava/lang/String;Z)Z

    move-result v0

    sput-boolean v0, Lcom/miui/app/smartpower/SmartPowerSettings;->DEBUG_ALL:Z

    .line 13
    nop

    .line 14
    const-string v0, "persist.sys.smartpower.debug.proc"

    invoke-static {v0, v1}, Landroid/os/SystemProperties;->getBoolean(Ljava/lang/String;Z)Z

    move-result v0

    sput-boolean v0, Lcom/miui/app/smartpower/SmartPowerSettings;->DEBUG_PROC:Z

    .line 16
    nop

    .line 17
    const-string v0, "persist.sys.millet.cgroup"

    invoke-static {v0, v1}, Landroid/os/SystemProperties;->getBoolean(Ljava/lang/String;Z)Z

    move-result v0

    sput-boolean v0, Lcom/miui/app/smartpower/SmartPowerSettings;->PROP_FROZEN_CGROUPV1_ENABLE:Z

    .line 21
    const-string v0, "persist.sys.smartpower.appstate.enable"

    const/4 v2, 0x1

    invoke-static {v0, v2}, Landroid/os/SystemProperties;->getBoolean(Ljava/lang/String;Z)Z

    move-result v0

    sput-boolean v0, Lcom/miui/app/smartpower/SmartPowerSettings;->APP_STATE_ENABLE:Z

    .line 25
    const-string v0, "persist.sys.smartpower.fz.enable"

    invoke-static {v0, v1}, Landroid/os/SystemProperties;->getBoolean(Ljava/lang/String;Z)Z

    move-result v0

    sput-boolean v0, Lcom/miui/app/smartpower/SmartPowerSettings;->PROP_FROZEN_ENABLE:Z

    .line 29
    nop

    .line 30
    const-string v0, "persist.sys.smartpower.intercept.enable"

    invoke-static {v0, v1}, Landroid/os/SystemProperties;->getBoolean(Ljava/lang/String;Z)Z

    move-result v0

    sput-boolean v0, Lcom/miui/app/smartpower/SmartPowerSettings;->PROP_INTERCEPT_ENABLE:Z

    .line 34
    const-string v0, "persist.sys.smartpower.display.enable"

    invoke-static {v0, v1}, Landroid/os/SystemProperties;->getBoolean(Ljava/lang/String;Z)Z

    move-result v0

    sput-boolean v0, Lcom/miui/app/smartpower/SmartPowerSettings;->DISPLAY_POLICY_ENABLE:Z

    .line 39
    const-string v0, "persist.sys.smartpower.window.enable"

    invoke-static {v0, v2}, Landroid/os/SystemProperties;->getBoolean(Ljava/lang/String;Z)Z

    move-result v0

    sput-boolean v0, Lcom/miui/app/smartpower/SmartPowerSettings;->WINDOW_POLICY_ENABLE:Z

    .line 42
    nop

    .line 43
    const-string v0, "persist.sys.smartpower.history.dur"

    const-wide/32 v3, 0xdbba00

    invoke-static {v0, v3, v4}, Landroid/os/SystemProperties;->getLong(Ljava/lang/String;J)J

    move-result-wide v3

    sput-wide v3, Lcom/miui/app/smartpower/SmartPowerSettings;->MAX_HISTORY_REPORT_DURATION:J

    .line 44
    nop

    .line 45
    const-string v0, "persist.sys.smartpower.history.size"

    const/16 v3, 0x64

    invoke-static {v0, v3}, Landroid/os/SystemProperties;->getInt(Ljava/lang/String;I)I

    move-result v0

    sput v0, Lcom/miui/app/smartpower/SmartPowerSettings;->MAX_HISTORY_REPORT_SIZE:I

    .line 48
    nop

    .line 49
    const-string v0, "persist.sys.smartpower.gamepay.protect.enabled"

    invoke-static {v0, v2}, Landroid/os/SystemProperties;->getBoolean(Ljava/lang/String;Z)Z

    move-result v0

    sput-boolean v0, Lcom/miui/app/smartpower/SmartPowerSettings;->GAME_PAY_PROTECT_ENABLED:Z

    .line 56
    nop

    .line 57
    const-string v0, "persist.sys.smartpower.inactive.dur"

    const-wide/16 v2, 0xbb8

    invoke-static {v0, v2, v3}, Landroid/os/SystemProperties;->getLong(Ljava/lang/String;J)J

    move-result-wide v2

    sput-wide v2, Lcom/miui/app/smartpower/SmartPowerSettings;->INACTIVE_DURATION:J

    .line 58
    nop

    .line 59
    const-string v0, "persist.sys.smartpower.inactive.sptm.dur"

    const-wide/16 v2, 0x7d0

    invoke-static {v0, v2, v3}, Landroid/os/SystemProperties;->getLong(Ljava/lang/String;J)J

    move-result-wide v4

    sput-wide v4, Lcom/miui/app/smartpower/SmartPowerSettings;->INACTIVE_SPTM_DURATION:J

    .line 64
    nop

    .line 65
    const-string v0, "persist.sys.smartpower.hibernate.dur"

    const-wide/32 v4, 0x927c0

    invoke-static {v0, v4, v5}, Landroid/os/SystemProperties;->getLong(Ljava/lang/String;J)J

    move-result-wide v4

    sput-wide v4, Lcom/miui/app/smartpower/SmartPowerSettings;->HIBERNATE_DURATION:J

    .line 70
    nop

    .line 71
    const-string v0, "persist.sys.smartpower.hibernate.max.dur"

    const-wide/32 v4, 0x1b7740

    invoke-static {v0, v4, v5}, Landroid/os/SystemProperties;->getLong(Ljava/lang/String;J)J

    move-result-wide v4

    sput-wide v4, Lcom/miui/app/smartpower/SmartPowerSettings;->MAX_HIBERNATE_DURATION:J

    .line 77
    nop

    .line 78
    const-string v0, "persist.sys.smartpower.idle.dur"

    invoke-static {v0, v2, v3}, Landroid/os/SystemProperties;->getLong(Ljava/lang/String;J)J

    move-result-wide v4

    sput-wide v4, Lcom/miui/app/smartpower/SmartPowerSettings;->IDLE_DURATION:J

    .line 84
    nop

    .line 85
    const-string v0, "persist.sys.smartpower.maintenance.dur"

    invoke-static {v0, v2, v3}, Landroid/os/SystemProperties;->getLong(Ljava/lang/String;J)J

    move-result-wide v2

    sput-wide v2, Lcom/miui/app/smartpower/SmartPowerSettings;->MAINTENANCE_DURATION:J

    .line 90
    nop

    .line 91
    const-string v0, "persist.sys.smartpower.update.userstats.dur"

    const-wide/32 v2, 0x36ee80

    invoke-static {v0, v2, v3}, Landroid/os/SystemProperties;->getLong(Ljava/lang/String;J)J

    move-result-wide v2

    sput-wide v2, Lcom/miui/app/smartpower/SmartPowerSettings;->UPDATE_USERSTATS_DURATION:J

    .line 97
    nop

    .line 98
    const-string v0, "persist.sys.smartpower.res.netactive.speed"

    const/16 v2, 0x1f4

    invoke-static {v0, v2}, Landroid/os/SystemProperties;->getInt(Ljava/lang/String;I)I

    move-result v0

    int-to-long v2, v0

    sput-wide v2, Lcom/miui/app/smartpower/SmartPowerSettings;->DEF_RES_NET_ACTIVE_SPEED:J

    .line 103
    nop

    .line 104
    const-string v0, "persist.sys.smartpower.res.netmonitor.periodic"

    const/16 v2, 0x3e8

    invoke-static {v0, v2}, Landroid/os/SystemProperties;->getInt(Ljava/lang/String;I)I

    move-result v0

    int-to-long v2, v0

    sput-wide v2, Lcom/miui/app/smartpower/SmartPowerSettings;->DEF_RES_NET_MONITOR_PERIOD:J

    .line 106
    nop

    .line 107
    const-string v0, "persist.sys.smartpower.lastinteractive.dur"

    const/16 v2, 0x1388

    invoke-static {v0, v2}, Landroid/os/SystemProperties;->getInt(Ljava/lang/String;I)I

    move-result v0

    int-to-long v2, v0

    sput-wide v2, Lcom/miui/app/smartpower/SmartPowerSettings;->DEF_LAST_INTER_ACTIVE_DURATION:J

    .line 109
    nop

    .line 110
    const-string v0, "persist.sys.smartpower.memthreshold.app.lvl1"

    const-wide/32 v2, 0x96000

    invoke-static {v0, v2, v3}, Landroid/os/SystemProperties;->getLong(Ljava/lang/String;J)J

    move-result-wide v2

    sput-wide v2, Lcom/miui/app/smartpower/SmartPowerSettings;->PROC_MEM_LVL1_PSS_LIMIT_KB:J

    .line 111
    nop

    .line 112
    const-string v0, "persist.sys.smartpower.memthreshold.app.lvl2"

    const-wide/32 v2, 0xc8000

    invoke-static {v0, v2, v3}, Landroid/os/SystemProperties;->getLong(Ljava/lang/String;J)J

    move-result-wide v2

    sput-wide v2, Lcom/miui/app/smartpower/SmartPowerSettings;->PROC_MEM_LVL2_PSS_LIMIT_KB:J

    .line 113
    nop

    .line 114
    const-string v0, "persist.sys.smartpower.memthreshold.app.lvl3"

    const-wide/32 v2, 0x100000

    invoke-static {v0, v2, v3}, Landroid/os/SystemProperties;->getLong(Ljava/lang/String;J)J

    move-result-wide v2

    sput-wide v2, Lcom/miui/app/smartpower/SmartPowerSettings;->PROC_MEM_LVL3_PSS_LIMIT_KB:J

    .line 121
    const-string v0, "persist.sys.smartpower.limit.max.refresh.rate"

    const/4 v2, -0x1

    invoke-static {v0, v2}, Landroid/os/SystemProperties;->getInt(Ljava/lang/String;I)I

    move-result v0

    sput v0, Lcom/miui/app/smartpower/SmartPowerSettings;->PROP_LIMIT_MAX_REFRESH_RATE:I

    .line 128
    const-string v0, "persist.sys.smartpower.display_camera_fps_enable"

    invoke-static {v0, v1}, Landroid/os/SystemProperties;->getBoolean(Ljava/lang/String;Z)Z

    move-result v0

    sput-boolean v0, Lcom/miui/app/smartpower/SmartPowerSettings;->PROP_CAMERA_REFRESH_RATE_ENABLE:Z

    .line 136
    const-string v0, "persist.sys.smartpower.display_thermal_temp_threshold"

    const/16 v1, 0x2e

    invoke-static {v0, v1}, Landroid/os/SystemProperties;->getInt(Ljava/lang/String;I)I

    move-result v0

    sput v0, Lcom/miui/app/smartpower/SmartPowerSettings;->PROP_THERMAL_TEMP_THRESHOLD:I

    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .line 5
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method
