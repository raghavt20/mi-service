public class com.miui.app.smartpower.SmartPowerPolicyConstants {
	 /* .source "SmartPowerPolicyConstants.java" */
	 /* # static fields */
	 public static final Long NEVER_PERIODIC_ACTIVE;
	 public static final Integer PROCESS_ACTIVE_LEVEL_CRITICAL;
	 public static final Integer PROCESS_ACTIVE_LEVEL_HEAVY;
	 public static final Integer PROCESS_ACTIVE_LEVEL_LOW;
	 public static final Integer PROCESS_ACTIVE_LEVEL_MODERATE;
	 public static final Integer PROCESS_USAGE_ACTIVE_LEVEL_DEFAULT;
	 public static final Integer PROCESS_USAGE_LEVEL_AUTOSTART;
	 public static final Integer PROCESS_USAGE_LEVEL_CRITICAL;
	 public static final Integer PROCESS_USAGE_LEVEL_LOW;
	 public static final Integer PROCESS_USAGE_LEVEL_MODERATE;
	 private static final java.lang.String PROP_MIUI_OPTIMIZATION;
	 public static Boolean TESTSUITSPECIFIC;
	 public static final Integer WHITE_LIST_ACTION_HIBERNATION;
	 public static final Integer WHITE_LIST_ACTION_INTERCEPT_ALARM;
	 public static final Integer WHITE_LIST_ACTION_INTERCEPT_PROVIDER;
	 public static final Integer WHITE_LIST_ACTION_INTERCEPT_SERVICE;
	 public static final Integer WHITE_LIST_ACTION_SCREENON_HIBERNATION;
	 public static final Integer WHITE_LIST_TYPE_ALARM_CLOUDCONTROL;
	 public static final Integer WHITE_LIST_TYPE_ALARM_DEFAULT;
	 public static final Integer WHITE_LIST_TYPE_ALARM_MASK;
	 public static final Integer WHITE_LIST_TYPE_ALARM_MAX;
	 public static final Integer WHITE_LIST_TYPE_ALARM_MIN;
	 public static final Integer WHITE_LIST_TYPE_BACKUP;
	 public static final Integer WHITE_LIST_TYPE_CLOUDCONTROL;
	 public static final Integer WHITE_LIST_TYPE_CTS;
	 public static final Integer WHITE_LIST_TYPE_DEFAULT;
	 public static final Integer WHITE_LIST_TYPE_DEPEND;
	 public static final Integer WHITE_LIST_TYPE_EXTAUDIO;
	 public static final Integer WHITE_LIST_TYPE_FREQUENTTHAW;
	 public static final Integer WHITE_LIST_TYPE_HIBERNATION_MASK;
	 public static final Integer WHITE_LIST_TYPE_MAX;
	 private static final Integer WHITE_LIST_TYPE_MIN;
	 public static final Integer WHITE_LIST_TYPE_PROVIDER_CLOUDCONTROL;
	 public static final Integer WHITE_LIST_TYPE_PROVIDER_DEFAULT;
	 public static final Integer WHITE_LIST_TYPE_PROVIDER_MASK;
	 public static final Integer WHITE_LIST_TYPE_PROVIDER_MAX;
	 public static final Integer WHITE_LIST_TYPE_PROVIDER_MIN;
	 public static final Integer WHITE_LIST_TYPE_SCREENON_CLOUDCONTROL;
	 public static final Integer WHITE_LIST_TYPE_SCREENON_DEFAULT;
	 public static final Integer WHITE_LIST_TYPE_SCREENON_MASK;
	 public static final Integer WHITE_LIST_TYPE_SCREENON_MAX;
	 private static final Integer WHITE_LIST_TYPE_SCREENON_MIN;
	 public static final Integer WHITE_LIST_TYPE_SERVICE_CLOUDCONTROL;
	 public static final Integer WHITE_LIST_TYPE_SERVICE_DEFAULT;
	 public static final Integer WHITE_LIST_TYPE_SERVICE_MASK;
	 public static final Integer WHITE_LIST_TYPE_SERVICE_MAX;
	 public static final Integer WHITE_LIST_TYPE_SERVICE_MIN;
	 public static final Integer WHITE_LIST_TYPE_USB;
	 public static final Integer WHITE_LIST_TYPE_VPN;
	 public static final Integer WHITE_LIST_TYPE_WALLPAPER;
	 /* # direct methods */
	 static com.miui.app.smartpower.SmartPowerPolicyConstants ( ) {
		 /* .locals 1 */
		 /* .line 60 */
		 int v0 = 0; // const/4 v0, 0x0
		 com.miui.app.smartpower.SmartPowerPolicyConstants.TESTSUITSPECIFIC = (v0!= 0);
		 return;
	 } // .end method
	 public com.miui.app.smartpower.SmartPowerPolicyConstants ( ) {
		 /* .locals 0 */
		 /* .line 5 */
		 /* invoke-direct {p0}, Ljava/lang/Object;-><init>()V */
		 return;
	 } // .end method
	 public static void updateTestSuitSpecificEnable ( ) {
		 /* .locals 2 */
		 /* .line 63 */
		 /* nop */
		 /* .line 64 */
		 final String v0 = "ro.miui.cts"; // const-string v0, "ro.miui.cts"
		 android.os.SystemProperties .get ( v0 );
		 final String v1 = "1"; // const-string v1, "1"
		 v0 = 		 (( java.lang.String ) v1 ).equals ( v0 ); // invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z
		 /* .line 63 */
		 /* xor-int/lit8 v0, v0, 0x1 */
		 final String v1 = "persist.sys.miui_optimization"; // const-string v1, "persist.sys.miui_optimization"
		 v0 = 		 android.os.SystemProperties .getBoolean ( v1,v0 );
		 /* xor-int/lit8 v0, v0, 0x1 */
		 com.miui.app.smartpower.SmartPowerPolicyConstants.TESTSUITSPECIFIC = (v0!= 0);
		 /* .line 65 */
		 return;
	 } // .end method
