.class public interface abstract Lcom/miui/app/smartpower/SmartPowerServiceInternal;
.super Ljava/lang/Object;
.source "SmartPowerServiceInternal.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/miui/app/smartpower/SmartPowerServiceInternal$MultiTaskActionListener;
    }
.end annotation


# static fields
.field public static final MULTI_TASK_INVALID_LISTENER:I = 0x3

.field public static final MULTI_TASK_LISTENER:I = 0x0

.field public static final MULTI_TASK_LISTENER_MULTI_SCENE:I = 0x2

.field public static final MULTI_TASK_LISTENER_SCHED_BOOST:I = 0x1


# virtual methods
.method public abstract addFrozenPid(II)V
.end method

.method public abstract applyOomAdjLocked(Lcom/android/server/am/ProcessRecord;)V
.end method

.method public abstract checkSystemSignature(I)Z
.end method

.method public abstract getAllAppState()Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/ArrayList<",
            "Lcom/miui/server/smartpower/IAppState;",
            ">;"
        }
    .end annotation
.end method

.method public abstract getAppState(I)Lcom/miui/server/smartpower/IAppState;
.end method

.method public abstract getBackgroundCpuCoreNum()I
.end method

.method public abstract getLastMusicPlayTimeStamp(I)J
.end method

.method public abstract getLruProcesses()Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/ArrayList<",
            "Lcom/miui/server/smartpower/IAppState$IRunningProcess;",
            ">;"
        }
    .end annotation
.end method

.method public abstract getLruProcesses(ILjava/lang/String;)Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(I",
            "Ljava/lang/String;",
            ")",
            "Ljava/util/ArrayList<",
            "Lcom/miui/server/smartpower/IAppState$IRunningProcess;",
            ">;"
        }
    .end annotation
.end method

.method public abstract getRunningProcess(ILjava/lang/String;)Lcom/miui/server/smartpower/IAppState$IRunningProcess;
.end method

.method public abstract hibernateAllIfNeeded(Ljava/lang/String;)V
.end method

.method public abstract isAppAudioActive(I)Z
.end method

.method public abstract isAppAudioActive(II)Z
.end method

.method public abstract isAppGpsActive(I)Z
.end method

.method public abstract isAppGpsActive(II)Z
.end method

.method public abstract isProcessInTaskStack(II)Z
.end method

.method public abstract isProcessInTaskStack(Ljava/lang/String;)Z
.end method

.method public abstract isProcessPerceptible(ILjava/lang/String;)Z
.end method

.method public abstract isProcessWhiteList(ILjava/lang/String;Ljava/lang/String;)Z
.end method

.method public abstract isUidIdle(I)Z
.end method

.method public abstract isUidVisible(I)Z
.end method

.method public abstract notifyCameraForegroundState(Ljava/lang/String;ZLjava/lang/String;II)V
.end method

.method public abstract notifyMultiSenceEnable(Z)V
.end method

.method public abstract onAcquireWakelock(Landroid/os/IBinder;ILjava/lang/String;II)V
.end method

.method public abstract onActivityReusmeUnchecked(Ljava/lang/String;IILjava/lang/String;IILjava/lang/String;Z)V
.end method

.method public abstract onActivityStartUnchecked(Ljava/lang/String;IILjava/lang/String;IILjava/lang/String;Z)V
.end method

.method public abstract onActivityVisibilityChanged(ILjava/lang/String;Lcom/android/server/wm/ActivityRecord;Z)V
.end method

.method public abstract onAlarmStatusChanged(IZ)V
.end method

.method public abstract onAppTransitionStartLocked(J)V
.end method

.method public abstract onBackupChanged(ZLcom/android/server/am/ProcessRecord;)V
.end method

.method public abstract onBackupServiceAppChanged(ZII)V
.end method

.method public abstract onBluetoothEvent(ZIIILjava/lang/String;I)V
.end method

.method public abstract onBroadcastStatusChanged(Lcom/android/server/am/ProcessRecord;ZLandroid/content/Intent;)V
.end method

.method public abstract onCpuExceptionEvents(I)V
.end method

.method public abstract onCpuPressureEvents(I)V
.end method

.method public abstract onDisplayDeviceStateChangeLocked(I)V
.end method

.method public abstract onDisplaySwitchResolutionLocked(III)V
.end method

.method public abstract onExternalAudioRegister(II)V
.end method

.method public abstract onFocusedWindowChangeLocked(Ljava/lang/String;Ljava/lang/String;)V
.end method

.method public abstract onForegroundActivityChangedLocked(Ljava/lang/String;IILjava/lang/String;IILjava/lang/String;ZIILjava/lang/String;)V
.end method

.method public abstract onInputMethodShow(I)V
.end method

.method public abstract onInsetAnimationHide(I)V
.end method

.method public abstract onInsetAnimationShow(I)V
.end method

.method public abstract onMediaKey(I)V
.end method

.method public abstract onMediaKey(II)V
.end method

.method public abstract onPendingPackagesExempt(Ljava/lang/String;)V
.end method

.method public abstract onPlayerEvent(IIII)V
.end method

.method public abstract onPlayerRlease(III)V
.end method

.method public abstract onPlayerTrack(IIII)V
.end method

.method public abstract onProcessKill(Lcom/android/server/am/ProcessRecord;)V
.end method

.method public abstract onProcessStart(Lcom/android/server/am/ProcessRecord;)V
.end method

.method public abstract onProviderConnectionChanged(Lcom/android/server/am/ContentProviderConnection;Z)V
.end method

.method public abstract onRecentsAnimationEnd()V
.end method

.method public abstract onRecentsAnimationStart(Lcom/android/server/wm/ActivityRecord;)V
.end method

.method public abstract onRecorderEvent(III)V
.end method

.method public abstract onRecorderRlease(II)V
.end method

.method public abstract onRecorderTrack(III)V
.end method

.method public abstract onReleaseWakelock(Landroid/os/IBinder;I)V
.end method

.method public abstract onRemoteAnimationEnd()V
.end method

.method public abstract onRemoteAnimationStart([Landroid/view/RemoteAnimationTarget;)V
.end method

.method public abstract onSendPendingIntent(Lcom/android/server/am/PendingIntentRecord$Key;Ljava/lang/String;Ljava/lang/String;Landroid/content/Intent;)V
.end method

.method public abstract onUsbStateChanged(ZJLandroid/content/Intent;)V
.end method

.method public abstract onVpnChanged(ZILjava/lang/String;)V
.end method

.method public abstract onWallpaperComponentChanged(ZILjava/lang/String;)V
.end method

.method public abstract onWindowAnimationStartLocked(JLandroid/view/SurfaceControl;)V
.end method

.method public abstract onWindowVisibilityChanged(IILcom/android/server/policy/WindowManagerPolicy$WindowState;Landroid/view/WindowManager$LayoutParams;Z)V
.end method

.method public abstract playbackStateChanged(IIII)V
.end method

.method public abstract powerFrozenServiceReady(Z)V
.end method

.method public abstract recordAudioFocus(IILjava/lang/String;Z)V
.end method

.method public abstract recordAudioFocusLoss(ILjava/lang/String;I)V
.end method

.method public abstract registerMultiTaskActionListener(ILandroid/os/Handler;Lcom/miui/app/smartpower/SmartPowerServiceInternal$MultiTaskActionListener;)Z
.end method

.method public abstract removeFrozenPid(II)V
.end method

.method public abstract reportBinderState(IIIIJ)V
.end method

.method public abstract reportBinderTrans(IIIIIZJJ)V
.end method

.method public abstract reportNet(IJ)V
.end method

.method public abstract reportSignal(IIJ)V
.end method

.method public abstract reportTrackStatus(IIIZ)V
.end method

.method public abstract shouldInterceptAlarm(II)Z
.end method

.method public abstract shouldInterceptProvider(ILcom/android/server/am/ProcessRecord;Lcom/android/server/am/ProcessRecord;Z)Z
.end method

.method public abstract shouldInterceptService(Landroid/content/Intent;Lmiui/security/CallerInfo;Landroid/content/pm/ServiceInfo;)Z
.end method

.method public abstract shouldInterceptUpdateDisplayModeSpecs(ILcom/android/server/display/mode/DisplayModeDirector$DesiredDisplayModeSpecs;)Z
.end method

.method public abstract skipFrozenAppAnr(Landroid/content/pm/ApplicationInfo;ILjava/lang/String;)Z
.end method

.method public abstract thawedByOther(II)V
.end method

.method public abstract uidAudioStatusChanged(IZ)V
.end method

.method public abstract uidVideoStatusChanged(IZ)V
.end method

.method public abstract unregisterMultiTaskActionListener(ILcom/miui/app/smartpower/SmartPowerServiceInternal$MultiTaskActionListener;)Z
.end method

.method public abstract updateActivitiesVisibility()V
.end method

.method public abstract updateAllAppUsageStats()Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/ArrayList<",
            "Lcom/miui/server/smartpower/IAppState;",
            ">;"
        }
    .end annotation
.end method

.method public abstract updateCpuStatsNow(Z)J
.end method

.method public abstract updateProcess(Lcom/android/server/am/ProcessRecord;)V
.end method
