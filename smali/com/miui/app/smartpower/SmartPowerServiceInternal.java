public abstract class com.miui.app.smartpower.SmartPowerServiceInternal {
	 /* .source "SmartPowerServiceInternal.java" */
	 /* # annotations */
	 /* .annotation system Ldalvik/annotation/MemberClasses; */
	 /* value = { */
	 /* Lcom/miui/app/smartpower/SmartPowerServiceInternal$MultiTaskActionListener; */
	 /* } */
} // .end annotation
/* # static fields */
public static final Integer MULTI_TASK_INVALID_LISTENER;
public static final Integer MULTI_TASK_LISTENER;
public static final Integer MULTI_TASK_LISTENER_MULTI_SCENE;
public static final Integer MULTI_TASK_LISTENER_SCHED_BOOST;
/* # virtual methods */
public abstract void addFrozenPid ( Integer p0, Integer p1 ) {
} // .end method
public abstract void applyOomAdjLocked ( com.android.server.am.ProcessRecord p0 ) {
} // .end method
public abstract Boolean checkSystemSignature ( Integer p0 ) {
} // .end method
public abstract java.util.ArrayList getAllAppState ( ) {
	 /* .annotation system Ldalvik/annotation/Signature; */
	 /* value = { */
	 /* "()", */
	 /* "Ljava/util/ArrayList<", */
	 /* "Lcom/miui/server/smartpower/IAppState;", */
	 /* ">;" */
	 /* } */
} // .end annotation
} // .end method
public abstract com.miui.server.smartpower.IAppState getAppState ( Integer p0 ) {
} // .end method
public abstract Integer getBackgroundCpuCoreNum ( ) {
} // .end method
public abstract Long getLastMusicPlayTimeStamp ( Integer p0 ) {
} // .end method
public abstract java.util.ArrayList getLruProcesses ( ) {
/* .annotation system Ldalvik/annotation/Signature; */
/* value = { */
/* "()", */
/* "Ljava/util/ArrayList<", */
/* "Lcom/miui/server/smartpower/IAppState$IRunningProcess;", */
/* ">;" */
/* } */
} // .end annotation
} // .end method
public abstract java.util.ArrayList getLruProcesses ( Integer p0, java.lang.String p1 ) {
/* .annotation system Ldalvik/annotation/Signature; */
/* value = { */
/* "(I", */
/* "Ljava/lang/String;", */
/* ")", */
/* "Ljava/util/ArrayList<", */
/* "Lcom/miui/server/smartpower/IAppState$IRunningProcess;", */
/* ">;" */
/* } */
} // .end annotation
} // .end method
public abstract com.miui.server.smartpower.IAppState$IRunningProcess getRunningProcess ( Integer p0, java.lang.String p1 ) {
} // .end method
public abstract void hibernateAllIfNeeded ( java.lang.String p0 ) {
} // .end method
public abstract Boolean isAppAudioActive ( Integer p0 ) {
} // .end method
public abstract Boolean isAppAudioActive ( Integer p0, Integer p1 ) {
} // .end method
public abstract Boolean isAppGpsActive ( Integer p0 ) {
} // .end method
public abstract Boolean isAppGpsActive ( Integer p0, Integer p1 ) {
} // .end method
public abstract Boolean isProcessInTaskStack ( Integer p0, Integer p1 ) {
} // .end method
public abstract Boolean isProcessInTaskStack ( java.lang.String p0 ) {
} // .end method
public abstract Boolean isProcessPerceptible ( Integer p0, java.lang.String p1 ) {
} // .end method
public abstract Boolean isProcessWhiteList ( Integer p0, java.lang.String p1, java.lang.String p2 ) {
} // .end method
public abstract Boolean isUidIdle ( Integer p0 ) {
} // .end method
public abstract Boolean isUidVisible ( Integer p0 ) {
} // .end method
public abstract void notifyCameraForegroundState ( java.lang.String p0, Boolean p1, java.lang.String p2, Integer p3, Integer p4 ) {
} // .end method
public abstract void notifyMultiSenceEnable ( Boolean p0 ) {
} // .end method
public abstract void onAcquireWakelock ( android.os.IBinder p0, Integer p1, java.lang.String p2, Integer p3, Integer p4 ) {
} // .end method
public abstract void onActivityReusmeUnchecked ( java.lang.String p0, Integer p1, Integer p2, java.lang.String p3, Integer p4, Integer p5, java.lang.String p6, Boolean p7 ) {
} // .end method
public abstract void onActivityStartUnchecked ( java.lang.String p0, Integer p1, Integer p2, java.lang.String p3, Integer p4, Integer p5, java.lang.String p6, Boolean p7 ) {
} // .end method
public abstract void onActivityVisibilityChanged ( Integer p0, java.lang.String p1, com.android.server.wm.ActivityRecord p2, Boolean p3 ) {
} // .end method
public abstract void onAlarmStatusChanged ( Integer p0, Boolean p1 ) {
} // .end method
public abstract void onAppTransitionStartLocked ( Long p0 ) {
} // .end method
public abstract void onBackupChanged ( Boolean p0, com.android.server.am.ProcessRecord p1 ) {
} // .end method
public abstract void onBackupServiceAppChanged ( Boolean p0, Integer p1, Integer p2 ) {
} // .end method
public abstract void onBluetoothEvent ( Boolean p0, Integer p1, Integer p2, Integer p3, java.lang.String p4, Integer p5 ) {
} // .end method
public abstract void onBroadcastStatusChanged ( com.android.server.am.ProcessRecord p0, Boolean p1, android.content.Intent p2 ) {
} // .end method
public abstract void onCpuExceptionEvents ( Integer p0 ) {
} // .end method
public abstract void onCpuPressureEvents ( Integer p0 ) {
} // .end method
public abstract void onDisplayDeviceStateChangeLocked ( Integer p0 ) {
} // .end method
public abstract void onDisplaySwitchResolutionLocked ( Integer p0, Integer p1, Integer p2 ) {
} // .end method
public abstract void onExternalAudioRegister ( Integer p0, Integer p1 ) {
} // .end method
public abstract void onFocusedWindowChangeLocked ( java.lang.String p0, java.lang.String p1 ) {
} // .end method
public abstract void onForegroundActivityChangedLocked ( java.lang.String p0, Integer p1, Integer p2, java.lang.String p3, Integer p4, Integer p5, java.lang.String p6, Boolean p7, Integer p8, Integer p9, java.lang.String p10 ) {
} // .end method
public abstract void onInputMethodShow ( Integer p0 ) {
} // .end method
public abstract void onInsetAnimationHide ( Integer p0 ) {
} // .end method
public abstract void onInsetAnimationShow ( Integer p0 ) {
} // .end method
public abstract void onMediaKey ( Integer p0 ) {
} // .end method
public abstract void onMediaKey ( Integer p0, Integer p1 ) {
} // .end method
public abstract void onPendingPackagesExempt ( java.lang.String p0 ) {
} // .end method
public abstract void onPlayerEvent ( Integer p0, Integer p1, Integer p2, Integer p3 ) {
} // .end method
public abstract void onPlayerRlease ( Integer p0, Integer p1, Integer p2 ) {
} // .end method
public abstract void onPlayerTrack ( Integer p0, Integer p1, Integer p2, Integer p3 ) {
} // .end method
public abstract void onProcessKill ( com.android.server.am.ProcessRecord p0 ) {
} // .end method
public abstract void onProcessStart ( com.android.server.am.ProcessRecord p0 ) {
} // .end method
public abstract void onProviderConnectionChanged ( com.android.server.am.ContentProviderConnection p0, Boolean p1 ) {
} // .end method
public abstract void onRecentsAnimationEnd ( ) {
} // .end method
public abstract void onRecentsAnimationStart ( com.android.server.wm.ActivityRecord p0 ) {
} // .end method
public abstract void onRecorderEvent ( Integer p0, Integer p1, Integer p2 ) {
} // .end method
public abstract void onRecorderRlease ( Integer p0, Integer p1 ) {
} // .end method
public abstract void onRecorderTrack ( Integer p0, Integer p1, Integer p2 ) {
} // .end method
public abstract void onReleaseWakelock ( android.os.IBinder p0, Integer p1 ) {
} // .end method
public abstract void onRemoteAnimationEnd ( ) {
} // .end method
public abstract void onRemoteAnimationStart ( android.view.RemoteAnimationTarget[] p0 ) {
} // .end method
public abstract void onSendPendingIntent ( com.android.server.am.PendingIntentRecord$Key p0, java.lang.String p1, java.lang.String p2, android.content.Intent p3 ) {
} // .end method
public abstract void onUsbStateChanged ( Boolean p0, Long p1, android.content.Intent p2 ) {
} // .end method
public abstract void onVpnChanged ( Boolean p0, Integer p1, java.lang.String p2 ) {
} // .end method
public abstract void onWallpaperComponentChanged ( Boolean p0, Integer p1, java.lang.String p2 ) {
} // .end method
public abstract void onWindowAnimationStartLocked ( Long p0, android.view.SurfaceControl p1 ) {
} // .end method
public abstract void onWindowVisibilityChanged ( Integer p0, Integer p1, com.android.server.policy.WindowManagerPolicy$WindowState p2, android.view.WindowManager$LayoutParams p3, Boolean p4 ) {
} // .end method
public abstract void playbackStateChanged ( Integer p0, Integer p1, Integer p2, Integer p3 ) {
} // .end method
public abstract void powerFrozenServiceReady ( Boolean p0 ) {
} // .end method
public abstract void recordAudioFocus ( Integer p0, Integer p1, java.lang.String p2, Boolean p3 ) {
} // .end method
public abstract void recordAudioFocusLoss ( Integer p0, java.lang.String p1, Integer p2 ) {
} // .end method
public abstract Boolean registerMultiTaskActionListener ( Integer p0, android.os.Handler p1, com.miui.app.smartpower.SmartPowerServiceInternal$MultiTaskActionListener p2 ) {
} // .end method
public abstract void removeFrozenPid ( Integer p0, Integer p1 ) {
} // .end method
public abstract void reportBinderState ( Integer p0, Integer p1, Integer p2, Integer p3, Long p4 ) {
} // .end method
public abstract void reportBinderTrans ( Integer p0, Integer p1, Integer p2, Integer p3, Integer p4, Boolean p5, Long p6, Long p7 ) {
} // .end method
public abstract void reportNet ( Integer p0, Long p1 ) {
} // .end method
public abstract void reportSignal ( Integer p0, Integer p1, Long p2 ) {
} // .end method
public abstract void reportTrackStatus ( Integer p0, Integer p1, Integer p2, Boolean p3 ) {
} // .end method
public abstract Boolean shouldInterceptAlarm ( Integer p0, Integer p1 ) {
} // .end method
public abstract Boolean shouldInterceptProvider ( Integer p0, com.android.server.am.ProcessRecord p1, com.android.server.am.ProcessRecord p2, Boolean p3 ) {
} // .end method
public abstract Boolean shouldInterceptService ( android.content.Intent p0, miui.security.CallerInfo p1, android.content.pm.ServiceInfo p2 ) {
} // .end method
public abstract Boolean shouldInterceptUpdateDisplayModeSpecs ( Integer p0, com.android.server.display.mode.DisplayModeDirector$DesiredDisplayModeSpecs p1 ) {
} // .end method
public abstract Boolean skipFrozenAppAnr ( android.content.pm.ApplicationInfo p0, Integer p1, java.lang.String p2 ) {
} // .end method
public abstract void thawedByOther ( Integer p0, Integer p1 ) {
} // .end method
public abstract void uidAudioStatusChanged ( Integer p0, Boolean p1 ) {
} // .end method
public abstract void uidVideoStatusChanged ( Integer p0, Boolean p1 ) {
} // .end method
public abstract Boolean unregisterMultiTaskActionListener ( Integer p0, com.miui.app.smartpower.SmartPowerServiceInternal$MultiTaskActionListener p1 ) {
} // .end method
public abstract void updateActivitiesVisibility ( ) {
} // .end method
public abstract java.util.ArrayList updateAllAppUsageStats ( ) {
/* .annotation system Ldalvik/annotation/Signature; */
/* value = { */
/* "()", */
/* "Ljava/util/ArrayList<", */
/* "Lcom/miui/server/smartpower/IAppState;", */
/* ">;" */
/* } */
} // .end annotation
} // .end method
public abstract Long updateCpuStatsNow ( Boolean p0 ) {
} // .end method
public abstract void updateProcess ( com.android.server.am.ProcessRecord p0 ) {
} // .end method
