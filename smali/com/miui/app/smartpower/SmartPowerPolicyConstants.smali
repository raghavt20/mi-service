.class public Lcom/miui/app/smartpower/SmartPowerPolicyConstants;
.super Ljava/lang/Object;
.source "SmartPowerPolicyConstants.java"


# static fields
.field public static final NEVER_PERIODIC_ACTIVE:J = 0x0L

.field public static final PROCESS_ACTIVE_LEVEL_CRITICAL:I = 0x1

.field public static final PROCESS_ACTIVE_LEVEL_HEAVY:I = 0x0

.field public static final PROCESS_ACTIVE_LEVEL_LOW:I = 0x3

.field public static final PROCESS_ACTIVE_LEVEL_MODERATE:I = 0x2

.field public static final PROCESS_USAGE_ACTIVE_LEVEL_DEFAULT:I = 0x9

.field public static final PROCESS_USAGE_LEVEL_AUTOSTART:I = 0x4

.field public static final PROCESS_USAGE_LEVEL_CRITICAL:I = 0x5

.field public static final PROCESS_USAGE_LEVEL_LOW:I = 0x7

.field public static final PROCESS_USAGE_LEVEL_MODERATE:I = 0x6

.field private static final PROP_MIUI_OPTIMIZATION:Ljava/lang/String; = "persist.sys.miui_optimization"

.field public static TESTSUITSPECIFIC:Z = false

.field public static final WHITE_LIST_ACTION_HIBERNATION:I = 0x2

.field public static final WHITE_LIST_ACTION_INTERCEPT_ALARM:I = 0x8

.field public static final WHITE_LIST_ACTION_INTERCEPT_PROVIDER:I = 0x20

.field public static final WHITE_LIST_ACTION_INTERCEPT_SERVICE:I = 0x10

.field public static final WHITE_LIST_ACTION_SCREENON_HIBERNATION:I = 0x4

.field public static final WHITE_LIST_TYPE_ALARM_CLOUDCONTROL:I = 0x10000

.field public static final WHITE_LIST_TYPE_ALARM_DEFAULT:I = 0x8000

.field public static final WHITE_LIST_TYPE_ALARM_MASK:I = 0x1c000

.field public static final WHITE_LIST_TYPE_ALARM_MAX:I = 0x20000

.field public static final WHITE_LIST_TYPE_ALARM_MIN:I = 0x4000

.field public static final WHITE_LIST_TYPE_BACKUP:I = 0x40

.field public static final WHITE_LIST_TYPE_CLOUDCONTROL:I = 0x4

.field public static final WHITE_LIST_TYPE_CTS:I = 0x400

.field public static final WHITE_LIST_TYPE_DEFAULT:I = 0x2

.field public static final WHITE_LIST_TYPE_DEPEND:I = 0x100

.field public static final WHITE_LIST_TYPE_EXTAUDIO:I = 0x200

.field public static final WHITE_LIST_TYPE_FREQUENTTHAW:I = 0x20

.field public static final WHITE_LIST_TYPE_HIBERNATION_MASK:I = 0x7fe

.field public static final WHITE_LIST_TYPE_MAX:I = 0x800

.field private static final WHITE_LIST_TYPE_MIN:I = 0x1

.field public static final WHITE_LIST_TYPE_PROVIDER_CLOUDCONTROL:I = 0x400000

.field public static final WHITE_LIST_TYPE_PROVIDER_DEFAULT:I = 0x200000

.field public static final WHITE_LIST_TYPE_PROVIDER_MASK:I = 0x700000

.field public static final WHITE_LIST_TYPE_PROVIDER_MAX:I = 0x800000

.field public static final WHITE_LIST_TYPE_PROVIDER_MIN:I = 0x100000

.field public static final WHITE_LIST_TYPE_SCREENON_CLOUDCONTROL:I = 0x2000

.field public static final WHITE_LIST_TYPE_SCREENON_DEFAULT:I = 0x1000

.field public static final WHITE_LIST_TYPE_SCREENON_MASK:I = 0x3800

.field public static final WHITE_LIST_TYPE_SCREENON_MAX:I = 0x4000

.field private static final WHITE_LIST_TYPE_SCREENON_MIN:I = 0x800

.field public static final WHITE_LIST_TYPE_SERVICE_CLOUDCONTROL:I = 0x80000

.field public static final WHITE_LIST_TYPE_SERVICE_DEFAULT:I = 0x40000

.field public static final WHITE_LIST_TYPE_SERVICE_MASK:I = 0xe0000

.field public static final WHITE_LIST_TYPE_SERVICE_MAX:I = 0x100000

.field public static final WHITE_LIST_TYPE_SERVICE_MIN:I = 0x20000

.field public static final WHITE_LIST_TYPE_USB:I = 0x80

.field public static final WHITE_LIST_TYPE_VPN:I = 0x8

.field public static final WHITE_LIST_TYPE_WALLPAPER:I = 0x10


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .line 60
    const/4 v0, 0x0

    sput-boolean v0, Lcom/miui/app/smartpower/SmartPowerPolicyConstants;->TESTSUITSPECIFIC:Z

    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .line 5
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static updateTestSuitSpecificEnable()V
    .locals 2

    .line 63
    nop

    .line 64
    const-string v0, "ro.miui.cts"

    invoke-static {v0}, Landroid/os/SystemProperties;->get(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    const-string v1, "1"

    invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    .line 63
    xor-int/lit8 v0, v0, 0x1

    const-string v1, "persist.sys.miui_optimization"

    invoke-static {v1, v0}, Landroid/os/SystemProperties;->getBoolean(Ljava/lang/String;Z)Z

    move-result v0

    xor-int/lit8 v0, v0, 0x1

    sput-boolean v0, Lcom/miui/app/smartpower/SmartPowerPolicyConstants;->TESTSUITSPECIFIC:Z

    .line 65
    return-void
.end method
