.class public abstract Lcom/miui/daemon/performance/server/IMiuiPerfService$Stub;
.super Landroid/os/Binder;
.source "IMiuiPerfService.java"

# interfaces
.implements Lcom/miui/daemon/performance/server/IMiuiPerfService;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/miui/daemon/performance/server/IMiuiPerfService;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x409
    name = "Stub"
.end annotation

.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/miui/daemon/performance/server/IMiuiPerfService$Stub$Proxy;
    }
.end annotation


# static fields
.field private static final DESCRIPTOR:Ljava/lang/String; = "com.miui.daemon.performance.server.IMiuiPerfService"

.field static final TRANSACTION_abortMatchingScenario:I = 0x7

.field static final TRANSACTION_abortSpecificScenario:I = 0x8

.field static final TRANSACTION_beginScenario:I = 0x6

.field static final TRANSACTION_dump:I = 0x3

.field static final TRANSACTION_finishMatchingScenario:I = 0x9

.field static final TRANSACTION_finishSpecificScenario:I = 0xa

.field static final TRANSACTION_getPerfEventSocketFd:I = 0x5

.field static final TRANSACTION_markPerceptibleJank:I = 0x1

.field static final TRANSACTION_reportActivityLaunchRecords:I = 0x2

.field static final TRANSACTION_reportApplicationStart:I = 0x11

.field static final TRANSACTION_reportExcessiveCpuUsageRecords:I = 0xb

.field static final TRANSACTION_reportKillMessage:I = 0x10

.field static final TRANSACTION_reportKillProcessEvent:I = 0x12

.field static final TRANSACTION_reportMiSpeedRecord:I = 0xf

.field static final TRANSACTION_reportNotificationClick:I = 0xc

.field static final TRANSACTION_reportProcessCleanEvent:I = 0xd

.field static final TRANSACTION_reportPssRecord:I = 0xe

.field static final TRANSACTION_setSchedFgPid:I = 0x4


# direct methods
.method public constructor <init>()V
    .locals 1

    .line 21
    invoke-direct {p0}, Landroid/os/Binder;-><init>()V

    .line 22
    const-string v0, "com.miui.daemon.performance.server.IMiuiPerfService"

    invoke-virtual {p0, p0, v0}, Lcom/miui/daemon/performance/server/IMiuiPerfService$Stub;->attachInterface(Landroid/os/IInterface;Ljava/lang/String;)V

    .line 23
    return-void
.end method

.method public static asInterface(Landroid/os/IBinder;)Lcom/miui/daemon/performance/server/IMiuiPerfService;
    .locals 2
    .param p0, "obj"    # Landroid/os/IBinder;

    .line 32
    if-nez p0, :cond_0

    .line 33
    const/4 v0, 0x0

    return-object v0

    .line 35
    :cond_0
    const-string v0, "com.miui.daemon.performance.server.IMiuiPerfService"

    invoke-interface {p0, v0}, Landroid/os/IBinder;->queryLocalInterface(Ljava/lang/String;)Landroid/os/IInterface;

    move-result-object v0

    .line 36
    .local v0, "iin":Landroid/os/IInterface;
    if-eqz v0, :cond_1

    instance-of v1, v0, Lcom/miui/daemon/performance/server/IMiuiPerfService;

    if-eqz v1, :cond_1

    .line 37
    move-object v1, v0

    check-cast v1, Lcom/miui/daemon/performance/server/IMiuiPerfService;

    return-object v1

    .line 39
    :cond_1
    new-instance v1, Lcom/miui/daemon/performance/server/IMiuiPerfService$Stub$Proxy;

    invoke-direct {v1, p0}, Lcom/miui/daemon/performance/server/IMiuiPerfService$Stub$Proxy;-><init>(Landroid/os/IBinder;)V

    return-object v1
.end method


# virtual methods
.method public asBinder()Landroid/os/IBinder;
    .locals 0

    .line 45
    return-object p0
.end method

.method public onTransact(ILandroid/os/Parcel;Landroid/os/Parcel;I)Z
    .locals 26
    .param p1, "code"    # I
    .param p2, "data"    # Landroid/os/Parcel;
    .param p3, "reply"    # Landroid/os/Parcel;
    .param p4, "flags"    # I
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    .line 52
    move-object/from16 v12, p0

    move-object/from16 v13, p2

    move-object/from16 v14, p3

    const/4 v0, 0x0

    const/4 v1, 0x0

    const-string v2, "com.miui.daemon.performance.server.IMiuiPerfService"

    const/4 v15, 0x1

    sparse-switch p1, :sswitch_data_0

    .line 247
    invoke-super/range {p0 .. p4}, Landroid/os/Binder;->onTransact(ILandroid/os/Parcel;Landroid/os/Parcel;I)Z

    move-result v0

    return v0

    .line 54
    :sswitch_0
    invoke-virtual {v14, v2}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 55
    return v15

    .line 239
    :sswitch_1
    invoke-virtual {v13, v2}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    .line 240
    sget-object v0, Landroid/os/Bundle;->CREATOR:Landroid/os/Parcelable$Creator;

    invoke-interface {v0, v13}, Landroid/os/Parcelable$Creator;->createFromParcel(Landroid/os/Parcel;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/os/Bundle;

    .line 241
    .local v0, "bundle":Landroid/os/Bundle;
    invoke-virtual {v12, v0}, Lcom/miui/daemon/performance/server/IMiuiPerfService$Stub;->reportKillProcessEvent(Landroid/os/Bundle;)V

    .line 242
    invoke-virtual/range {p3 .. p3}, Landroid/os/Parcel;->writeNoException()V

    .line 243
    return v15

    .line 231
    .end local v0    # "bundle":Landroid/os/Bundle;
    :sswitch_2
    invoke-virtual {v13, v2}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    .line 232
    sget-object v0, Landroid/os/Bundle;->CREATOR:Landroid/os/Parcelable$Creator;

    invoke-interface {v0, v13}, Landroid/os/Parcelable$Creator;->createFromParcel(Landroid/os/Parcel;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/os/Bundle;

    .line 233
    .restart local v0    # "bundle":Landroid/os/Bundle;
    invoke-virtual {v12, v0}, Lcom/miui/daemon/performance/server/IMiuiPerfService$Stub;->reportApplicationStart(Landroid/os/Bundle;)V

    .line 234
    invoke-virtual/range {p3 .. p3}, Landroid/os/Parcel;->writeNoException()V

    .line 235
    return v15

    .line 212
    .end local v0    # "bundle":Landroid/os/Bundle;
    :sswitch_3
    invoke-virtual {v13, v2}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    .line 213
    invoke-virtual/range {p2 .. p2}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v6

    .line 214
    .local v6, "processName":Ljava/lang/String;
    invoke-virtual/range {p2 .. p2}, Landroid/os/Parcel;->readInt()I

    move-result v7

    .line 215
    .local v7, "pid":I
    invoke-virtual/range {p2 .. p2}, Landroid/os/Parcel;->readInt()I

    move-result v8

    .line 216
    .local v8, "uid":I
    invoke-virtual/range {p2 .. p2}, Landroid/os/Parcel;->readLong()J

    move-result-wide v9

    .line 217
    .local v9, "pss":J
    move-object/from16 v0, p0

    move-object v1, v6

    move v2, v7

    move v3, v8

    move-wide v4, v9

    invoke-virtual/range {v0 .. v5}, Lcom/miui/daemon/performance/server/IMiuiPerfService$Stub;->reportKillMessage(Ljava/lang/String;IIJ)V

    .line 218
    invoke-virtual/range {p3 .. p3}, Landroid/os/Parcel;->writeNoException()V

    .line 219
    return v15

    .line 223
    .end local v6    # "processName":Ljava/lang/String;
    .end local v7    # "pid":I
    .end local v8    # "uid":I
    .end local v9    # "pss":J
    :sswitch_4
    invoke-virtual {v13, v2}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    .line 224
    sget-object v0, Landroid/os/Bundle;->CREATOR:Landroid/os/Parcelable$Creator;

    invoke-interface {v0, v13}, Landroid/os/Parcelable$Creator;->createFromParcel(Landroid/os/Parcel;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/os/Bundle;

    .line 225
    .restart local v0    # "bundle":Landroid/os/Bundle;
    invoke-virtual {v12, v0}, Lcom/miui/daemon/performance/server/IMiuiPerfService$Stub;->reportMiSpeedRecord(Landroid/os/Bundle;)V

    .line 226
    invoke-virtual/range {p3 .. p3}, Landroid/os/Parcel;->writeNoException()V

    .line 227
    return v15

    .line 200
    .end local v0    # "bundle":Landroid/os/Bundle;
    :sswitch_5
    invoke-virtual {v13, v2}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    .line 201
    invoke-virtual/range {p2 .. p2}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v7

    .line 202
    .local v7, "processName":Ljava/lang/String;
    invoke-virtual/range {p2 .. p2}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v8

    .line 203
    .local v8, "packageName":Ljava/lang/String;
    invoke-virtual/range {p2 .. p2}, Landroid/os/Parcel;->readLong()J

    move-result-wide v9

    .line 204
    .restart local v9    # "pss":J
    invoke-virtual/range {p2 .. p2}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v11

    .line 205
    .local v11, "versionName":Ljava/lang/String;
    invoke-virtual/range {p2 .. p2}, Landroid/os/Parcel;->readInt()I

    move-result v16

    .line 206
    .local v16, "versionCode":I
    move-object/from16 v0, p0

    move-object v1, v7

    move-object v2, v8

    move-wide v3, v9

    move-object v5, v11

    move/from16 v6, v16

    invoke-virtual/range {v0 .. v6}, Lcom/miui/daemon/performance/server/IMiuiPerfService$Stub;->reportPssRecord(Ljava/lang/String;Ljava/lang/String;JLjava/lang/String;I)V

    .line 207
    invoke-virtual/range {p3 .. p3}, Landroid/os/Parcel;->writeNoException()V

    .line 208
    return v15

    .line 191
    .end local v7    # "processName":Ljava/lang/String;
    .end local v8    # "packageName":Ljava/lang/String;
    .end local v9    # "pss":J
    .end local v11    # "versionName":Ljava/lang/String;
    .end local v16    # "versionCode":I
    :sswitch_6
    invoke-virtual {v13, v2}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    .line 192
    invoke-virtual/range {p2 .. p2}, Landroid/os/Parcel;->readInt()I

    move-result v0

    if-eqz v0, :cond_0

    .line 193
    sget-object v0, Landroid/os/Bundle;->CREATOR:Landroid/os/Parcelable$Creator;

    invoke-interface {v0, v13}, Landroid/os/Parcelable$Creator;->createFromParcel(Landroid/os/Parcel;)Ljava/lang/Object;

    move-result-object v0

    move-object v1, v0

    check-cast v1, Landroid/os/Bundle;

    goto :goto_0

    :cond_0
    nop

    :goto_0
    move-object v0, v1

    .line 194
    .restart local v0    # "bundle":Landroid/os/Bundle;
    invoke-virtual {v12, v0}, Lcom/miui/daemon/performance/server/IMiuiPerfService$Stub;->reportProcessCleanEvent(Landroid/os/Bundle;)V

    .line 195
    invoke-virtual/range {p3 .. p3}, Landroid/os/Parcel;->writeNoException()V

    .line 196
    return v15

    .line 176
    .end local v0    # "bundle":Landroid/os/Bundle;
    :sswitch_7
    invoke-virtual {v13, v2}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    .line 177
    invoke-virtual/range {p2 .. p2}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    .line 179
    .local v0, "postPackage":Ljava/lang/String;
    invoke-virtual/range {p2 .. p2}, Landroid/os/Parcel;->readInt()I

    move-result v1

    if-eqz v1, :cond_1

    .line 180
    sget-object v1, Landroid/content/Intent;->CREATOR:Landroid/os/Parcelable$Creator;

    invoke-interface {v1, v13}, Landroid/os/Parcelable$Creator;->createFromParcel(Landroid/os/Parcel;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/content/Intent;

    .local v1, "intent":Landroid/content/Intent;
    goto :goto_1

    .line 182
    .end local v1    # "intent":Landroid/content/Intent;
    :cond_1
    const/4 v1, 0x0

    .line 184
    .restart local v1    # "intent":Landroid/content/Intent;
    :goto_1
    invoke-virtual/range {p2 .. p2}, Landroid/os/Parcel;->readLong()J

    move-result-wide v2

    .line 185
    .local v2, "uptimeMillis":J
    invoke-virtual {v12, v0, v1, v2, v3}, Lcom/miui/daemon/performance/server/IMiuiPerfService$Stub;->reportNotificationClick(Ljava/lang/String;Landroid/content/Intent;J)V

    .line 186
    invoke-virtual/range {p3 .. p3}, Landroid/os/Parcel;->writeNoException()V

    .line 187
    return v15

    .line 168
    .end local v0    # "postPackage":Ljava/lang/String;
    .end local v1    # "intent":Landroid/content/Intent;
    .end local v2    # "uptimeMillis":J
    :sswitch_8
    invoke-virtual {v13, v2}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    .line 169
    sget-object v0, Landroid/os/Bundle;->CREATOR:Landroid/os/Parcelable$Creator;

    invoke-virtual {v13, v0}, Landroid/os/Parcel;->createTypedArrayList(Landroid/os/Parcelable$Creator;)Ljava/util/ArrayList;

    move-result-object v0

    .line 170
    .local v0, "records":Ljava/util/List;, "Ljava/util/List<Landroid/os/Bundle;>;"
    invoke-virtual {v12, v0}, Lcom/miui/daemon/performance/server/IMiuiPerfService$Stub;->reportExcessiveCpuUsageRecords(Ljava/util/List;)V

    .line 171
    invoke-virtual/range {p3 .. p3}, Landroid/os/Parcel;->writeNoException()V

    .line 172
    return v15

    .line 155
    .end local v0    # "records":Ljava/util/List;, "Ljava/util/List<Landroid/os/Bundle;>;"
    :sswitch_9
    invoke-virtual {v13, v2}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    .line 156
    invoke-virtual/range {p2 .. p2}, Landroid/os/Parcel;->readBundle()Landroid/os/Bundle;

    move-result-object v9

    .line 157
    .local v9, "scenarioBundle":Landroid/os/Bundle;
    invoke-virtual {v13, v1}, Landroid/os/Parcel;->readParcelable(Ljava/lang/ClassLoader;)Landroid/os/Parcelable;

    move-result-object v0

    move-object v10, v0

    check-cast v10, Landroid/os/statistics/E2EScenarioPayload;

    .line 158
    .local v10, "payload":Landroid/os/statistics/E2EScenarioPayload;
    invoke-virtual/range {p2 .. p2}, Landroid/os/Parcel;->readLong()J

    move-result-wide v16

    .line 159
    .local v16, "uptimeMillis":J
    invoke-virtual/range {p2 .. p2}, Landroid/os/Parcel;->readInt()I

    move-result v11

    .line 160
    .local v11, "pid":I
    invoke-virtual/range {p2 .. p2}, Landroid/os/Parcel;->readInt()I

    move-result v18

    .line 161
    .local v18, "tid":I
    invoke-virtual/range {p2 .. p2}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v19

    .line 162
    .local v19, "processName":Ljava/lang/String;
    invoke-virtual/range {p2 .. p2}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v20

    .line 163
    .local v20, "packageName":Ljava/lang/String;
    move-object/from16 v0, p0

    move-object v1, v9

    move-object v2, v10

    move-wide/from16 v3, v16

    move v5, v11

    move/from16 v6, v18

    move-object/from16 v7, v19

    move-object/from16 v8, v20

    invoke-virtual/range {v0 .. v8}, Lcom/miui/daemon/performance/server/IMiuiPerfService$Stub;->finishSpecificScenario(Landroid/os/Bundle;Landroid/os/statistics/E2EScenarioPayload;JIILjava/lang/String;Ljava/lang/String;)V

    .line 164
    invoke-virtual/range {p3 .. p3}, Landroid/os/Parcel;->writeNoException()V

    .line 165
    return v15

    .line 141
    .end local v9    # "scenarioBundle":Landroid/os/Bundle;
    .end local v10    # "payload":Landroid/os/statistics/E2EScenarioPayload;
    .end local v11    # "pid":I
    .end local v16    # "uptimeMillis":J
    .end local v18    # "tid":I
    .end local v19    # "processName":Ljava/lang/String;
    .end local v20    # "packageName":Ljava/lang/String;
    :sswitch_a
    invoke-virtual {v13, v2}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    .line 142
    invoke-virtual {v13, v1}, Landroid/os/Parcel;->readParcelable(Ljava/lang/ClassLoader;)Landroid/os/Parcelable;

    move-result-object v0

    move-object v10, v0

    check-cast v10, Landroid/os/statistics/E2EScenario;

    .line 143
    .local v10, "scenario":Landroid/os/statistics/E2EScenario;
    invoke-virtual/range {p2 .. p2}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v11

    .line 144
    .local v11, "tag":Ljava/lang/String;
    invoke-virtual {v13, v1}, Landroid/os/Parcel;->readParcelable(Ljava/lang/ClassLoader;)Landroid/os/Parcelable;

    move-result-object v0

    move-object/from16 v16, v0

    check-cast v16, Landroid/os/statistics/E2EScenarioPayload;

    .line 145
    .local v16, "payload":Landroid/os/statistics/E2EScenarioPayload;
    invoke-virtual/range {p2 .. p2}, Landroid/os/Parcel;->readLong()J

    move-result-wide v17

    .line 146
    .local v17, "uptimeMillis":J
    invoke-virtual/range {p2 .. p2}, Landroid/os/Parcel;->readInt()I

    move-result v19

    .line 147
    .local v19, "pid":I
    invoke-virtual/range {p2 .. p2}, Landroid/os/Parcel;->readInt()I

    move-result v20

    .line 148
    .local v20, "tid":I
    invoke-virtual/range {p2 .. p2}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v21

    .line 149
    .local v21, "processName":Ljava/lang/String;
    invoke-virtual/range {p2 .. p2}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v22

    .line 150
    .local v22, "packageName":Ljava/lang/String;
    move-object/from16 v0, p0

    move-object v1, v10

    move-object v2, v11

    move-object/from16 v3, v16

    move-wide/from16 v4, v17

    move/from16 v6, v19

    move/from16 v7, v20

    move-object/from16 v8, v21

    move-object/from16 v9, v22

    invoke-virtual/range {v0 .. v9}, Lcom/miui/daemon/performance/server/IMiuiPerfService$Stub;->finishMatchingScenario(Landroid/os/statistics/E2EScenario;Ljava/lang/String;Landroid/os/statistics/E2EScenarioPayload;JIILjava/lang/String;Ljava/lang/String;)V

    .line 151
    invoke-virtual/range {p3 .. p3}, Landroid/os/Parcel;->writeNoException()V

    .line 152
    return v15

    .line 129
    .end local v10    # "scenario":Landroid/os/statistics/E2EScenario;
    .end local v11    # "tag":Ljava/lang/String;
    .end local v16    # "payload":Landroid/os/statistics/E2EScenarioPayload;
    .end local v17    # "uptimeMillis":J
    .end local v19    # "pid":I
    .end local v20    # "tid":I
    .end local v21    # "processName":Ljava/lang/String;
    .end local v22    # "packageName":Ljava/lang/String;
    :sswitch_b
    invoke-virtual {v13, v2}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    .line 130
    invoke-virtual/range {p2 .. p2}, Landroid/os/Parcel;->readBundle()Landroid/os/Bundle;

    move-result-object v8

    .line 131
    .local v8, "scenarioBundle":Landroid/os/Bundle;
    invoke-virtual/range {p2 .. p2}, Landroid/os/Parcel;->readLong()J

    move-result-wide v9

    .line 132
    .local v9, "uptimeMillis":J
    invoke-virtual/range {p2 .. p2}, Landroid/os/Parcel;->readInt()I

    move-result v11

    .line 133
    .local v11, "pid":I
    invoke-virtual/range {p2 .. p2}, Landroid/os/Parcel;->readInt()I

    move-result v16

    .line 134
    .local v16, "tid":I
    invoke-virtual/range {p2 .. p2}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v17

    .line 135
    .local v17, "processName":Ljava/lang/String;
    invoke-virtual/range {p2 .. p2}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v18

    .line 136
    .local v18, "packageName":Ljava/lang/String;
    move-object/from16 v0, p0

    move-object v1, v8

    move-wide v2, v9

    move v4, v11

    move/from16 v5, v16

    move-object/from16 v6, v17

    move-object/from16 v7, v18

    invoke-virtual/range {v0 .. v7}, Lcom/miui/daemon/performance/server/IMiuiPerfService$Stub;->abortSpecificScenario(Landroid/os/Bundle;JIILjava/lang/String;Ljava/lang/String;)V

    .line 137
    invoke-virtual/range {p3 .. p3}, Landroid/os/Parcel;->writeNoException()V

    .line 138
    return v15

    .line 116
    .end local v8    # "scenarioBundle":Landroid/os/Bundle;
    .end local v9    # "uptimeMillis":J
    .end local v11    # "pid":I
    .end local v16    # "tid":I
    .end local v17    # "processName":Ljava/lang/String;
    .end local v18    # "packageName":Ljava/lang/String;
    :sswitch_c
    invoke-virtual {v13, v2}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    .line 117
    invoke-virtual {v13, v1}, Landroid/os/Parcel;->readParcelable(Ljava/lang/ClassLoader;)Landroid/os/Parcelable;

    move-result-object v0

    move-object v9, v0

    check-cast v9, Landroid/os/statistics/E2EScenario;

    .line 118
    .local v9, "scenario":Landroid/os/statistics/E2EScenario;
    invoke-virtual/range {p2 .. p2}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v10

    .line 119
    .local v10, "tag":Ljava/lang/String;
    invoke-virtual/range {p2 .. p2}, Landroid/os/Parcel;->readLong()J

    move-result-wide v16

    .line 120
    .local v16, "uptimeMillis":J
    invoke-virtual/range {p2 .. p2}, Landroid/os/Parcel;->readInt()I

    move-result v11

    .line 121
    .restart local v11    # "pid":I
    invoke-virtual/range {p2 .. p2}, Landroid/os/Parcel;->readInt()I

    move-result v18

    .line 122
    .local v18, "tid":I
    invoke-virtual/range {p2 .. p2}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v19

    .line 123
    .local v19, "processName":Ljava/lang/String;
    invoke-virtual/range {p2 .. p2}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v20

    .line 124
    .local v20, "packageName":Ljava/lang/String;
    move-object/from16 v0, p0

    move-object v1, v9

    move-object v2, v10

    move-wide/from16 v3, v16

    move v5, v11

    move/from16 v6, v18

    move-object/from16 v7, v19

    move-object/from16 v8, v20

    invoke-virtual/range {v0 .. v8}, Lcom/miui/daemon/performance/server/IMiuiPerfService$Stub;->abortMatchingScenario(Landroid/os/statistics/E2EScenario;Ljava/lang/String;JIILjava/lang/String;Ljava/lang/String;)V

    .line 125
    invoke-virtual/range {p3 .. p3}, Landroid/os/Parcel;->writeNoException()V

    .line 126
    return v15

    .line 99
    .end local v9    # "scenario":Landroid/os/statistics/E2EScenario;
    .end local v10    # "tag":Ljava/lang/String;
    .end local v11    # "pid":I
    .end local v16    # "uptimeMillis":J
    .end local v18    # "tid":I
    .end local v19    # "processName":Ljava/lang/String;
    .end local v20    # "packageName":Ljava/lang/String;
    :sswitch_d
    invoke-virtual {v13, v2}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    .line 100
    invoke-virtual {v13, v1}, Landroid/os/Parcel;->readParcelable(Ljava/lang/ClassLoader;)Landroid/os/Parcelable;

    move-result-object v2

    move-object/from16 v16, v2

    check-cast v16, Landroid/os/statistics/E2EScenario;

    .line 101
    .local v16, "scenario":Landroid/os/statistics/E2EScenario;
    invoke-virtual {v13, v1}, Landroid/os/Parcel;->readParcelable(Ljava/lang/ClassLoader;)Landroid/os/Parcelable;

    move-result-object v2

    move-object/from16 v17, v2

    check-cast v17, Landroid/os/statistics/E2EScenarioSettings;

    .line 102
    .local v17, "settings":Landroid/os/statistics/E2EScenarioSettings;
    invoke-virtual/range {p2 .. p2}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v18

    .line 103
    .local v18, "tag":Ljava/lang/String;
    invoke-virtual {v13, v1}, Landroid/os/Parcel;->readParcelable(Ljava/lang/ClassLoader;)Landroid/os/Parcelable;

    move-result-object v1

    move-object/from16 v19, v1

    check-cast v19, Landroid/os/statistics/E2EScenarioPayload;

    .line 104
    .local v19, "payload":Landroid/os/statistics/E2EScenarioPayload;
    invoke-virtual/range {p2 .. p2}, Landroid/os/Parcel;->readLong()J

    move-result-wide v20

    .line 105
    .local v20, "uptimeMillis":J
    invoke-virtual/range {p2 .. p2}, Landroid/os/Parcel;->readInt()I

    move-result v22

    .line 106
    .local v22, "pid":I
    invoke-virtual/range {p2 .. p2}, Landroid/os/Parcel;->readInt()I

    move-result v23

    .line 107
    .local v23, "tid":I
    invoke-virtual/range {p2 .. p2}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v24

    .line 108
    .local v24, "processName":Ljava/lang/String;
    invoke-virtual/range {p2 .. p2}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v25

    .line 109
    .local v25, "packageName":Ljava/lang/String;
    invoke-virtual/range {p2 .. p2}, Landroid/os/Parcel;->readInt()I

    move-result v1

    if-ne v1, v15, :cond_2

    move v11, v15

    goto :goto_2

    :cond_2
    move v11, v0

    .line 110
    .local v11, "needResultBundle":Z
    :goto_2
    move-object/from16 v0, p0

    move-object/from16 v1, v16

    move-object/from16 v2, v17

    move-object/from16 v3, v18

    move-object/from16 v4, v19

    move-wide/from16 v5, v20

    move/from16 v7, v22

    move/from16 v8, v23

    move-object/from16 v9, v24

    move-object/from16 v10, v25

    invoke-virtual/range {v0 .. v11}, Lcom/miui/daemon/performance/server/IMiuiPerfService$Stub;->beginScenario(Landroid/os/statistics/E2EScenario;Landroid/os/statistics/E2EScenarioSettings;Ljava/lang/String;Landroid/os/statistics/E2EScenarioPayload;JIILjava/lang/String;Ljava/lang/String;Z)Landroid/os/Bundle;

    move-result-object v0

    .line 111
    .local v0, "_result":Landroid/os/Bundle;
    invoke-virtual/range {p3 .. p3}, Landroid/os/Parcel;->writeNoException()V

    .line 112
    invoke-virtual {v14, v0}, Landroid/os/Parcel;->writeBundle(Landroid/os/Bundle;)V

    .line 113
    return v15

    .line 87
    .end local v0    # "_result":Landroid/os/Bundle;
    .end local v11    # "needResultBundle":Z
    .end local v16    # "scenario":Landroid/os/statistics/E2EScenario;
    .end local v17    # "settings":Landroid/os/statistics/E2EScenarioSettings;
    .end local v18    # "tag":Ljava/lang/String;
    .end local v19    # "payload":Landroid/os/statistics/E2EScenarioPayload;
    .end local v20    # "uptimeMillis":J
    .end local v22    # "pid":I
    .end local v23    # "tid":I
    .end local v24    # "processName":Ljava/lang/String;
    .end local v25    # "packageName":Ljava/lang/String;
    :sswitch_e
    invoke-virtual {v13, v2}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    .line 88
    invoke-virtual/range {p0 .. p0}, Lcom/miui/daemon/performance/server/IMiuiPerfService$Stub;->getPerfEventSocketFd()Landroid/os/ParcelFileDescriptor;

    move-result-object v1

    .line 89
    .local v1, "_result":Landroid/os/ParcelFileDescriptor;
    invoke-virtual/range {p3 .. p3}, Landroid/os/Parcel;->writeNoException()V

    .line 90
    if-eqz v1, :cond_3

    .line 91
    invoke-virtual {v14, v15}, Landroid/os/Parcel;->writeInt(I)V

    .line 92
    invoke-virtual {v1, v14, v15}, Landroid/os/ParcelFileDescriptor;->writeToParcel(Landroid/os/Parcel;I)V

    goto :goto_3

    .line 94
    :cond_3
    invoke-virtual {v14, v0}, Landroid/os/Parcel;->writeInt(I)V

    .line 96
    :goto_3
    return v15

    .line 80
    .end local v1    # "_result":Landroid/os/ParcelFileDescriptor;
    :sswitch_f
    invoke-virtual {v13, v2}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    .line 81
    invoke-virtual/range {p2 .. p2}, Landroid/os/Parcel;->readInt()I

    move-result v0

    .line 82
    .local v0, "pid":I
    invoke-virtual {v12, v0}, Lcom/miui/daemon/performance/server/IMiuiPerfService$Stub;->setSchedFgPid(I)V

    .line 83
    invoke-virtual/range {p3 .. p3}, Landroid/os/Parcel;->writeNoException()V

    .line 84
    return v15

    .line 73
    .end local v0    # "pid":I
    :sswitch_10
    invoke-virtual {v13, v2}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    .line 74
    invoke-virtual/range {p2 .. p2}, Landroid/os/Parcel;->createStringArray()[Ljava/lang/String;

    move-result-object v0

    .line 75
    .local v0, "args":[Ljava/lang/String;
    invoke-virtual {v12, v0}, Lcom/miui/daemon/performance/server/IMiuiPerfService$Stub;->dump([Ljava/lang/String;)V

    .line 76
    invoke-virtual/range {p3 .. p3}, Landroid/os/Parcel;->writeNoException()V

    .line 77
    return v15

    .line 66
    .end local v0    # "args":[Ljava/lang/String;
    :sswitch_11
    invoke-virtual {v13, v2}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    .line 67
    sget-object v0, Landroid/os/Bundle;->CREATOR:Landroid/os/Parcelable$Creator;

    invoke-virtual {v13, v0}, Landroid/os/Parcel;->createTypedArrayList(Landroid/os/Parcelable$Creator;)Ljava/util/ArrayList;

    move-result-object v0

    .line 68
    .local v0, "launchRecords":Ljava/util/List;, "Ljava/util/List<Landroid/os/Bundle;>;"
    invoke-virtual {v12, v0}, Lcom/miui/daemon/performance/server/IMiuiPerfService$Stub;->reportActivityLaunchRecords(Ljava/util/List;)V

    .line 69
    invoke-virtual/range {p3 .. p3}, Landroid/os/Parcel;->writeNoException()V

    .line 70
    return v15

    .line 58
    .end local v0    # "launchRecords":Ljava/util/List;, "Ljava/util/List<Landroid/os/Bundle;>;"
    :sswitch_12
    invoke-virtual {v13, v2}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    .line 59
    invoke-virtual/range {p2 .. p2}, Landroid/os/Parcel;->readInt()I

    move-result v0

    if-eqz v0, :cond_4

    .line 60
    sget-object v0, Landroid/os/Bundle;->CREATOR:Landroid/os/Parcelable$Creator;

    invoke-interface {v0, v13}, Landroid/os/Parcelable$Creator;->createFromParcel(Landroid/os/Parcel;)Ljava/lang/Object;

    move-result-object v0

    move-object v1, v0

    check-cast v1, Landroid/os/Bundle;

    goto :goto_4

    :cond_4
    nop

    :goto_4
    move-object v0, v1

    .line 61
    .local v0, "bundle":Landroid/os/Bundle;
    invoke-virtual {v12, v0}, Lcom/miui/daemon/performance/server/IMiuiPerfService$Stub;->markPerceptibleJank(Landroid/os/Bundle;)V

    .line 62
    invoke-virtual/range {p3 .. p3}, Landroid/os/Parcel;->writeNoException()V

    .line 63
    return v15

    nop

    :sswitch_data_0
    .sparse-switch
        0x1 -> :sswitch_12
        0x2 -> :sswitch_11
        0x3 -> :sswitch_10
        0x4 -> :sswitch_f
        0x5 -> :sswitch_e
        0x6 -> :sswitch_d
        0x7 -> :sswitch_c
        0x8 -> :sswitch_b
        0x9 -> :sswitch_a
        0xa -> :sswitch_9
        0xb -> :sswitch_8
        0xc -> :sswitch_7
        0xd -> :sswitch_6
        0xe -> :sswitch_5
        0xf -> :sswitch_4
        0x10 -> :sswitch_3
        0x11 -> :sswitch_2
        0x12 -> :sswitch_1
        0x5f4e5446 -> :sswitch_0
    .end sparse-switch
.end method
