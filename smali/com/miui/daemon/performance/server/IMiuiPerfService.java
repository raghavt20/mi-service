public abstract class com.miui.daemon.performance.server.IMiuiPerfService implements android.os.IInterface {
	 /* .source "IMiuiPerfService.java" */
	 /* # interfaces */
	 /* # annotations */
	 /* .annotation system Ldalvik/annotation/MemberClasses; */
	 /* value = { */
	 /* Lcom/miui/daemon/performance/server/IMiuiPerfService$Stub; */
	 /* } */
} // .end annotation
/* # virtual methods */
public abstract void abortMatchingScenario ( android.os.statistics.E2EScenario p0, java.lang.String p1, Long p2, Integer p3, Integer p4, java.lang.String p5, java.lang.String p6 ) {
	 /* .annotation system Ldalvik/annotation/Throws; */
	 /* value = { */
	 /* Landroid/os/RemoteException; */
	 /* } */
} // .end annotation
} // .end method
public abstract void abortSpecificScenario ( android.os.Bundle p0, Long p1, Integer p2, Integer p3, java.lang.String p4, java.lang.String p5 ) {
/* .annotation system Ldalvik/annotation/Throws; */
/* value = { */
/* Landroid/os/RemoteException; */
/* } */
} // .end annotation
} // .end method
public abstract android.os.Bundle beginScenario ( android.os.statistics.E2EScenario p0, android.os.statistics.E2EScenarioSettings p1, java.lang.String p2, android.os.statistics.E2EScenarioPayload p3, Long p4, Integer p5, Integer p6, java.lang.String p7, java.lang.String p8, Boolean p9 ) {
/* .annotation system Ldalvik/annotation/Throws; */
/* value = { */
/* Landroid/os/RemoteException; */
/* } */
} // .end annotation
} // .end method
public abstract void dump ( java.lang.String[] p0 ) {
/* .annotation system Ldalvik/annotation/Throws; */
/* value = { */
/* Landroid/os/RemoteException; */
/* } */
} // .end annotation
} // .end method
public abstract void finishMatchingScenario ( android.os.statistics.E2EScenario p0, java.lang.String p1, android.os.statistics.E2EScenarioPayload p2, Long p3, Integer p4, Integer p5, java.lang.String p6, java.lang.String p7 ) {
/* .annotation system Ldalvik/annotation/Throws; */
/* value = { */
/* Landroid/os/RemoteException; */
/* } */
} // .end annotation
} // .end method
public abstract void finishSpecificScenario ( android.os.Bundle p0, android.os.statistics.E2EScenarioPayload p1, Long p2, Integer p3, Integer p4, java.lang.String p5, java.lang.String p6 ) {
/* .annotation system Ldalvik/annotation/Throws; */
/* value = { */
/* Landroid/os/RemoteException; */
/* } */
} // .end annotation
} // .end method
public abstract android.os.ParcelFileDescriptor getPerfEventSocketFd ( ) {
/* .annotation system Ldalvik/annotation/Throws; */
/* value = { */
/* Landroid/os/RemoteException; */
/* } */
} // .end annotation
} // .end method
public abstract void markPerceptibleJank ( android.os.Bundle p0 ) {
/* .annotation system Ldalvik/annotation/Throws; */
/* value = { */
/* Landroid/os/RemoteException; */
/* } */
} // .end annotation
} // .end method
public abstract void reportActivityLaunchRecords ( java.util.List p0 ) {
/* .annotation system Ldalvik/annotation/Signature; */
/* value = { */
/* "(", */
/* "Ljava/util/List<", */
/* "Landroid/os/Bundle;", */
/* ">;)V" */
/* } */
} // .end annotation
/* .annotation system Ldalvik/annotation/Throws; */
/* value = { */
/* Landroid/os/RemoteException; */
/* } */
} // .end annotation
} // .end method
public abstract void reportApplicationStart ( android.os.Bundle p0 ) {
/* .annotation system Ldalvik/annotation/Throws; */
/* value = { */
/* Landroid/os/RemoteException; */
/* } */
} // .end annotation
} // .end method
public abstract void reportExcessiveCpuUsageRecords ( java.util.List p0 ) {
/* .annotation system Ldalvik/annotation/Signature; */
/* value = { */
/* "(", */
/* "Ljava/util/List<", */
/* "Landroid/os/Bundle;", */
/* ">;)V" */
/* } */
} // .end annotation
/* .annotation system Ldalvik/annotation/Throws; */
/* value = { */
/* Landroid/os/RemoteException; */
/* } */
} // .end annotation
} // .end method
public abstract void reportKillMessage ( java.lang.String p0, Integer p1, Integer p2, Long p3 ) {
/* .annotation system Ldalvik/annotation/Throws; */
/* value = { */
/* Landroid/os/RemoteException; */
/* } */
} // .end annotation
} // .end method
public abstract void reportKillProcessEvent ( android.os.Bundle p0 ) {
/* .annotation system Ldalvik/annotation/Throws; */
/* value = { */
/* Landroid/os/RemoteException; */
/* } */
} // .end annotation
} // .end method
public abstract void reportMiSpeedRecord ( android.os.Bundle p0 ) {
/* .annotation system Ldalvik/annotation/Throws; */
/* value = { */
/* Landroid/os/RemoteException; */
/* } */
} // .end annotation
} // .end method
public abstract void reportNotificationClick ( java.lang.String p0, android.content.Intent p1, Long p2 ) {
/* .annotation system Ldalvik/annotation/Throws; */
/* value = { */
/* Landroid/os/RemoteException; */
/* } */
} // .end annotation
} // .end method
public abstract void reportProcessCleanEvent ( android.os.Bundle p0 ) {
/* .annotation system Ldalvik/annotation/Throws; */
/* value = { */
/* Landroid/os/RemoteException; */
/* } */
} // .end annotation
} // .end method
public abstract void reportPssRecord ( java.lang.String p0, java.lang.String p1, Long p2, java.lang.String p3, Integer p4 ) {
/* .annotation system Ldalvik/annotation/Throws; */
/* value = { */
/* Landroid/os/RemoteException; */
/* } */
} // .end annotation
} // .end method
public abstract void setSchedFgPid ( Integer p0 ) {
/* .annotation system Ldalvik/annotation/Throws; */
/* value = { */
/* Landroid/os/RemoteException; */
/* } */
} // .end annotation
} // .end method
