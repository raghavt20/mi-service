class com.miui.daemon.performance.server.IMiuiPerfService$Stub$Proxy implements com.miui.daemon.performance.server.IMiuiPerfService {
	 /* .source "IMiuiPerfService.java" */
	 /* # interfaces */
	 /* # annotations */
	 /* .annotation system Ldalvik/annotation/EnclosingClass; */
	 /* value = Lcom/miui/daemon/performance/server/IMiuiPerfService$Stub; */
} // .end annotation
/* .annotation system Ldalvik/annotation/InnerClass; */
/* accessFlags = 0xa */
/* name = "Proxy" */
} // .end annotation
/* # instance fields */
private android.os.IBinder mRemote;
/* # direct methods */
 com.miui.daemon.performance.server.IMiuiPerfService$Stub$Proxy ( ) {
/* .locals 0 */
/* .param p1, "remote" # Landroid/os/IBinder; */
/* .line 254 */
/* invoke-direct {p0}, Ljava/lang/Object;-><init>()V */
/* .line 255 */
this.mRemote = p1;
/* .line 256 */
return;
} // .end method
/* # virtual methods */
public void abortMatchingScenario ( android.os.statistics.E2EScenario p0, java.lang.String p1, Long p2, Integer p3, Integer p4, java.lang.String p5, java.lang.String p6 ) {
/* .locals 5 */
/* .param p1, "scenario" # Landroid/os/statistics/E2EScenario; */
/* .param p2, "tag" # Ljava/lang/String; */
/* .param p3, "uptimeMillis" # J */
/* .param p5, "pid" # I */
/* .param p6, "tid" # I */
/* .param p7, "processName" # Ljava/lang/String; */
/* .param p8, "packageName" # Ljava/lang/String; */
/* .annotation system Ldalvik/annotation/Throws; */
/* value = { */
/* Landroid/os/RemoteException; */
/* } */
} // .end annotation
/* .line 374 */
android.os.Parcel .obtain ( );
/* .line 375 */
/* .local v0, "_data":Landroid/os/Parcel; */
android.os.Parcel .obtain ( );
/* .line 377 */
/* .local v1, "_reply":Landroid/os/Parcel; */
try { // :try_start_0
final String v2 = "com.miui.daemon.performance.server.IMiuiPerfService"; // const-string v2, "com.miui.daemon.performance.server.IMiuiPerfService"
(( android.os.Parcel ) v0 ).writeInterfaceToken ( v2 ); // invoke-virtual {v0, v2}, Landroid/os/Parcel;->writeInterfaceToken(Ljava/lang/String;)V
/* .line 378 */
int v2 = 0; // const/4 v2, 0x0
(( android.os.Parcel ) v0 ).writeParcelable ( p1, v2 ); // invoke-virtual {v0, p1, v2}, Landroid/os/Parcel;->writeParcelable(Landroid/os/Parcelable;I)V
/* .line 379 */
(( android.os.Parcel ) v0 ).writeString ( p2 ); // invoke-virtual {v0, p2}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V
/* .line 380 */
(( android.os.Parcel ) v0 ).writeLong ( p3, p4 ); // invoke-virtual {v0, p3, p4}, Landroid/os/Parcel;->writeLong(J)V
/* .line 381 */
(( android.os.Parcel ) v0 ).writeInt ( p5 ); // invoke-virtual {v0, p5}, Landroid/os/Parcel;->writeInt(I)V
/* .line 382 */
(( android.os.Parcel ) v0 ).writeInt ( p6 ); // invoke-virtual {v0, p6}, Landroid/os/Parcel;->writeInt(I)V
/* .line 383 */
(( android.os.Parcel ) v0 ).writeString ( p7 ); // invoke-virtual {v0, p7}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V
/* .line 384 */
(( android.os.Parcel ) v0 ).writeString ( p8 ); // invoke-virtual {v0, p8}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V
/* .line 385 */
v3 = this.mRemote;
int v4 = 7; // const/4 v4, 0x7
/* .line 386 */
(( android.os.Parcel ) v1 ).readException ( ); // invoke-virtual {v1}, Landroid/os/Parcel;->readException()V
/* :try_end_0 */
/* .catchall {:try_start_0 ..:try_end_0} :catchall_0 */
/* .line 388 */
(( android.os.Parcel ) v1 ).recycle ( ); // invoke-virtual {v1}, Landroid/os/Parcel;->recycle()V
/* .line 389 */
(( android.os.Parcel ) v0 ).recycle ( ); // invoke-virtual {v0}, Landroid/os/Parcel;->recycle()V
/* .line 390 */
/* nop */
/* .line 391 */
return;
/* .line 388 */
/* :catchall_0 */
/* move-exception v2 */
(( android.os.Parcel ) v1 ).recycle ( ); // invoke-virtual {v1}, Landroid/os/Parcel;->recycle()V
/* .line 389 */
(( android.os.Parcel ) v0 ).recycle ( ); // invoke-virtual {v0}, Landroid/os/Parcel;->recycle()V
/* .line 390 */
/* throw v2 */
} // .end method
public void abortSpecificScenario ( android.os.Bundle p0, Long p1, Integer p2, Integer p3, java.lang.String p4, java.lang.String p5 ) {
/* .locals 5 */
/* .param p1, "scenarioBundle" # Landroid/os/Bundle; */
/* .param p2, "uptimeMillis" # J */
/* .param p4, "pid" # I */
/* .param p5, "tid" # I */
/* .param p6, "processName" # Ljava/lang/String; */
/* .param p7, "packageName" # Ljava/lang/String; */
/* .annotation system Ldalvik/annotation/Throws; */
/* value = { */
/* Landroid/os/RemoteException; */
/* } */
} // .end annotation
/* .line 394 */
android.os.Parcel .obtain ( );
/* .line 395 */
/* .local v0, "_data":Landroid/os/Parcel; */
android.os.Parcel .obtain ( );
/* .line 397 */
/* .local v1, "_reply":Landroid/os/Parcel; */
try { // :try_start_0
final String v2 = "com.miui.daemon.performance.server.IMiuiPerfService"; // const-string v2, "com.miui.daemon.performance.server.IMiuiPerfService"
(( android.os.Parcel ) v0 ).writeInterfaceToken ( v2 ); // invoke-virtual {v0, v2}, Landroid/os/Parcel;->writeInterfaceToken(Ljava/lang/String;)V
/* .line 398 */
(( android.os.Parcel ) v0 ).writeBundle ( p1 ); // invoke-virtual {v0, p1}, Landroid/os/Parcel;->writeBundle(Landroid/os/Bundle;)V
/* .line 399 */
(( android.os.Parcel ) v0 ).writeLong ( p2, p3 ); // invoke-virtual {v0, p2, p3}, Landroid/os/Parcel;->writeLong(J)V
/* .line 400 */
(( android.os.Parcel ) v0 ).writeInt ( p4 ); // invoke-virtual {v0, p4}, Landroid/os/Parcel;->writeInt(I)V
/* .line 401 */
(( android.os.Parcel ) v0 ).writeInt ( p5 ); // invoke-virtual {v0, p5}, Landroid/os/Parcel;->writeInt(I)V
/* .line 402 */
(( android.os.Parcel ) v0 ).writeString ( p6 ); // invoke-virtual {v0, p6}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V
/* .line 403 */
(( android.os.Parcel ) v0 ).writeString ( p7 ); // invoke-virtual {v0, p7}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V
/* .line 404 */
v2 = this.mRemote;
/* const/16 v3, 0x8 */
int v4 = 0; // const/4 v4, 0x0
/* .line 405 */
(( android.os.Parcel ) v1 ).readException ( ); // invoke-virtual {v1}, Landroid/os/Parcel;->readException()V
/* :try_end_0 */
/* .catchall {:try_start_0 ..:try_end_0} :catchall_0 */
/* .line 407 */
(( android.os.Parcel ) v1 ).recycle ( ); // invoke-virtual {v1}, Landroid/os/Parcel;->recycle()V
/* .line 408 */
(( android.os.Parcel ) v0 ).recycle ( ); // invoke-virtual {v0}, Landroid/os/Parcel;->recycle()V
/* .line 409 */
/* nop */
/* .line 410 */
return;
/* .line 407 */
/* :catchall_0 */
/* move-exception v2 */
(( android.os.Parcel ) v1 ).recycle ( ); // invoke-virtual {v1}, Landroid/os/Parcel;->recycle()V
/* .line 408 */
(( android.os.Parcel ) v0 ).recycle ( ); // invoke-virtual {v0}, Landroid/os/Parcel;->recycle()V
/* .line 409 */
/* throw v2 */
} // .end method
public android.os.IBinder asBinder ( ) {
/* .locals 1 */
/* .line 260 */
v0 = this.mRemote;
} // .end method
public android.os.Bundle beginScenario ( android.os.statistics.E2EScenario p0, android.os.statistics.E2EScenarioSettings p1, java.lang.String p2, android.os.statistics.E2EScenarioPayload p3, Long p4, Integer p5, Integer p6, java.lang.String p7, java.lang.String p8, Boolean p9 ) {
/* .locals 16 */
/* .param p1, "scenario" # Landroid/os/statistics/E2EScenario; */
/* .param p2, "settings" # Landroid/os/statistics/E2EScenarioSettings; */
/* .param p3, "tag" # Ljava/lang/String; */
/* .param p4, "payload" # Landroid/os/statistics/E2EScenarioPayload; */
/* .param p5, "uptimeMillis" # J */
/* .param p7, "pid" # I */
/* .param p8, "tid" # I */
/* .param p9, "processName" # Ljava/lang/String; */
/* .param p10, "packageName" # Ljava/lang/String; */
/* .param p11, "needResultBundle" # Z */
/* .annotation system Ldalvik/annotation/Throws; */
/* value = { */
/* Landroid/os/RemoteException; */
/* } */
} // .end annotation
/* .line 348 */
android.os.Parcel .obtain ( );
/* .line 349 */
/* .local v1, "_data":Landroid/os/Parcel; */
android.os.Parcel .obtain ( );
/* .line 352 */
/* .local v2, "_reply":Landroid/os/Parcel; */
try { // :try_start_0
final String v0 = "com.miui.daemon.performance.server.IMiuiPerfService"; // const-string v0, "com.miui.daemon.performance.server.IMiuiPerfService"
(( android.os.Parcel ) v1 ).writeInterfaceToken ( v0 ); // invoke-virtual {v1, v0}, Landroid/os/Parcel;->writeInterfaceToken(Ljava/lang/String;)V
/* :try_end_0 */
/* .catchall {:try_start_0 ..:try_end_0} :catchall_a */
/* .line 353 */
int v0 = 0; // const/4 v0, 0x0
/* move-object/from16 v3, p1 */
try { // :try_start_1
	 (( android.os.Parcel ) v1 ).writeParcelable ( v3, v0 ); // invoke-virtual {v1, v3, v0}, Landroid/os/Parcel;->writeParcelable(Landroid/os/Parcelable;I)V
	 /* :try_end_1 */
	 /* .catchall {:try_start_1 ..:try_end_1} :catchall_9 */
	 /* .line 354 */
	 /* move-object/from16 v4, p2 */
	 try { // :try_start_2
		 (( android.os.Parcel ) v1 ).writeParcelable ( v4, v0 ); // invoke-virtual {v1, v4, v0}, Landroid/os/Parcel;->writeParcelable(Landroid/os/Parcelable;I)V
		 /* :try_end_2 */
		 /* .catchall {:try_start_2 ..:try_end_2} :catchall_8 */
		 /* .line 355 */
		 /* move-object/from16 v5, p3 */
		 try { // :try_start_3
			 (( android.os.Parcel ) v1 ).writeString ( v5 ); // invoke-virtual {v1, v5}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V
			 /* :try_end_3 */
			 /* .catchall {:try_start_3 ..:try_end_3} :catchall_7 */
			 /* .line 356 */
			 /* move-object/from16 v6, p4 */
			 try { // :try_start_4
				 (( android.os.Parcel ) v1 ).writeParcelable ( v6, v0 ); // invoke-virtual {v1, v6, v0}, Landroid/os/Parcel;->writeParcelable(Landroid/os/Parcelable;I)V
				 /* :try_end_4 */
				 /* .catchall {:try_start_4 ..:try_end_4} :catchall_6 */
				 /* .line 357 */
				 /* move-wide/from16 v7, p5 */
				 try { // :try_start_5
					 (( android.os.Parcel ) v1 ).writeLong ( v7, v8 ); // invoke-virtual {v1, v7, v8}, Landroid/os/Parcel;->writeLong(J)V
					 /* :try_end_5 */
					 /* .catchall {:try_start_5 ..:try_end_5} :catchall_5 */
					 /* .line 358 */
					 /* move/from16 v9, p7 */
					 try { // :try_start_6
						 (( android.os.Parcel ) v1 ).writeInt ( v9 ); // invoke-virtual {v1, v9}, Landroid/os/Parcel;->writeInt(I)V
						 /* :try_end_6 */
						 /* .catchall {:try_start_6 ..:try_end_6} :catchall_4 */
						 /* .line 359 */
						 /* move/from16 v10, p8 */
						 try { // :try_start_7
							 (( android.os.Parcel ) v1 ).writeInt ( v10 ); // invoke-virtual {v1, v10}, Landroid/os/Parcel;->writeInt(I)V
							 /* :try_end_7 */
							 /* .catchall {:try_start_7 ..:try_end_7} :catchall_3 */
							 /* .line 360 */
							 /* move-object/from16 v11, p9 */
							 try { // :try_start_8
								 (( android.os.Parcel ) v1 ).writeString ( v11 ); // invoke-virtual {v1, v11}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V
								 /* :try_end_8 */
								 /* .catchall {:try_start_8 ..:try_end_8} :catchall_2 */
								 /* .line 361 */
								 /* move-object/from16 v12, p10 */
								 try { // :try_start_9
									 (( android.os.Parcel ) v1 ).writeString ( v12 ); // invoke-virtual {v1, v12}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V
									 /* .line 362 */
									 if ( p11 != null) { // if-eqz p11, :cond_0
										 int v13 = 1; // const/4 v13, 0x1
									 } // :cond_0
									 /* move v13, v0 */
								 } // :goto_0
								 (( android.os.Parcel ) v1 ).writeInt ( v13 ); // invoke-virtual {v1, v13}, Landroid/os/Parcel;->writeInt(I)V
								 /* :try_end_9 */
								 /* .catchall {:try_start_9 ..:try_end_9} :catchall_1 */
								 /* .line 363 */
								 /* move-object/from16 v13, p0 */
								 try { // :try_start_a
									 v14 = this.mRemote;
									 int v15 = 6; // const/4 v15, 0x6
									 /* .line 364 */
									 (( android.os.Parcel ) v2 ).readException ( ); // invoke-virtual {v2}, Landroid/os/Parcel;->readException()V
									 /* .line 365 */
									 (( android.os.Parcel ) v2 ).readBundle ( ); // invoke-virtual {v2}, Landroid/os/Parcel;->readBundle()Landroid/os/Bundle;
									 /* :try_end_a */
									 /* .catchall {:try_start_a ..:try_end_a} :catchall_0 */
									 /* .line 367 */
									 /* .local v0, "_result":Landroid/os/Bundle; */
									 (( android.os.Parcel ) v2 ).recycle ( ); // invoke-virtual {v2}, Landroid/os/Parcel;->recycle()V
									 /* .line 368 */
									 (( android.os.Parcel ) v1 ).recycle ( ); // invoke-virtual {v1}, Landroid/os/Parcel;->recycle()V
									 /* .line 369 */
									 /* nop */
									 /* .line 370 */
									 /* .line 367 */
								 } // .end local v0 # "_result":Landroid/os/Bundle;
								 /* :catchall_0 */
								 /* move-exception v0 */
								 /* :catchall_1 */
								 /* move-exception v0 */
								 /* move-object/from16 v13, p0 */
								 /* :catchall_2 */
								 /* move-exception v0 */
								 /* move-object/from16 v13, p0 */
								 /* :catchall_3 */
								 /* move-exception v0 */
								 /* move-object/from16 v13, p0 */
								 /* :catchall_4 */
								 /* move-exception v0 */
								 /* move-object/from16 v13, p0 */
								 /* :catchall_5 */
								 /* move-exception v0 */
								 /* move-object/from16 v13, p0 */
								 /* :catchall_6 */
								 /* move-exception v0 */
								 /* move-object/from16 v13, p0 */
								 /* :catchall_7 */
								 /* move-exception v0 */
								 /* move-object/from16 v13, p0 */
								 /* :catchall_8 */
								 /* move-exception v0 */
								 /* move-object/from16 v13, p0 */
								 /* :catchall_9 */
								 /* move-exception v0 */
								 /* move-object/from16 v13, p0 */
								 /* :catchall_a */
								 /* move-exception v0 */
								 /* move-object/from16 v13, p0 */
								 /* move-object/from16 v3, p1 */
							 } // :goto_1
							 /* move-object/from16 v4, p2 */
						 } // :goto_2
						 /* move-object/from16 v5, p3 */
					 } // :goto_3
					 /* move-object/from16 v6, p4 */
				 } // :goto_4
				 /* move-wide/from16 v7, p5 */
			 } // :goto_5
			 /* move/from16 v9, p7 */
		 } // :goto_6
		 /* move/from16 v10, p8 */
	 } // :goto_7
	 /* move-object/from16 v11, p9 */
} // :goto_8
/* move-object/from16 v12, p10 */
} // :goto_9
(( android.os.Parcel ) v2 ).recycle ( ); // invoke-virtual {v2}, Landroid/os/Parcel;->recycle()V
/* .line 368 */
(( android.os.Parcel ) v1 ).recycle ( ); // invoke-virtual {v1}, Landroid/os/Parcel;->recycle()V
/* .line 369 */
/* throw v0 */
} // .end method
public void dump ( java.lang.String[] p0 ) {
/* .locals 5 */
/* .param p1, "args" # [Ljava/lang/String; */
/* .annotation system Ldalvik/annotation/Throws; */
/* value = { */
/* Landroid/os/RemoteException; */
/* } */
} // .end annotation
/* .line 300 */
android.os.Parcel .obtain ( );
/* .line 301 */
/* .local v0, "_data":Landroid/os/Parcel; */
final String v1 = "com.miui.daemon.performance.server.IMiuiPerfService"; // const-string v1, "com.miui.daemon.performance.server.IMiuiPerfService"
(( android.os.Parcel ) v0 ).writeInterfaceToken ( v1 ); // invoke-virtual {v0, v1}, Landroid/os/Parcel;->writeInterfaceToken(Ljava/lang/String;)V
/* .line 302 */
(( android.os.Parcel ) v0 ).writeStringArray ( p1 ); // invoke-virtual {v0, p1}, Landroid/os/Parcel;->writeStringArray([Ljava/lang/String;)V
/* .line 303 */
android.os.Parcel .obtain ( );
/* .line 305 */
/* .local v1, "_reply":Landroid/os/Parcel; */
try { // :try_start_0
v2 = this.mRemote;
int v3 = 3; // const/4 v3, 0x3
int v4 = 1; // const/4 v4, 0x1
/* .line 306 */
(( android.os.Parcel ) v1 ).readException ( ); // invoke-virtual {v1}, Landroid/os/Parcel;->readException()V
/* :try_end_0 */
/* .catchall {:try_start_0 ..:try_end_0} :catchall_0 */
/* .line 308 */
(( android.os.Parcel ) v1 ).recycle ( ); // invoke-virtual {v1}, Landroid/os/Parcel;->recycle()V
/* .line 309 */
(( android.os.Parcel ) v0 ).recycle ( ); // invoke-virtual {v0}, Landroid/os/Parcel;->recycle()V
/* .line 310 */
/* nop */
/* .line 311 */
return;
/* .line 308 */
/* :catchall_0 */
/* move-exception v2 */
(( android.os.Parcel ) v1 ).recycle ( ); // invoke-virtual {v1}, Landroid/os/Parcel;->recycle()V
/* .line 309 */
(( android.os.Parcel ) v0 ).recycle ( ); // invoke-virtual {v0}, Landroid/os/Parcel;->recycle()V
/* .line 310 */
/* throw v2 */
} // .end method
public void finishMatchingScenario ( android.os.statistics.E2EScenario p0, java.lang.String p1, android.os.statistics.E2EScenarioPayload p2, Long p3, Integer p4, Integer p5, java.lang.String p6, java.lang.String p7 ) {
/* .locals 5 */
/* .param p1, "scenario" # Landroid/os/statistics/E2EScenario; */
/* .param p2, "tag" # Ljava/lang/String; */
/* .param p3, "payload" # Landroid/os/statistics/E2EScenarioPayload; */
/* .param p4, "uptimeMillis" # J */
/* .param p6, "pid" # I */
/* .param p7, "tid" # I */
/* .param p8, "processName" # Ljava/lang/String; */
/* .param p9, "packageName" # Ljava/lang/String; */
/* .annotation system Ldalvik/annotation/Throws; */
/* value = { */
/* Landroid/os/RemoteException; */
/* } */
} // .end annotation
/* .line 413 */
android.os.Parcel .obtain ( );
/* .line 414 */
/* .local v0, "_data":Landroid/os/Parcel; */
android.os.Parcel .obtain ( );
/* .line 416 */
/* .local v1, "_reply":Landroid/os/Parcel; */
try { // :try_start_0
final String v2 = "com.miui.daemon.performance.server.IMiuiPerfService"; // const-string v2, "com.miui.daemon.performance.server.IMiuiPerfService"
(( android.os.Parcel ) v0 ).writeInterfaceToken ( v2 ); // invoke-virtual {v0, v2}, Landroid/os/Parcel;->writeInterfaceToken(Ljava/lang/String;)V
/* .line 417 */
int v2 = 0; // const/4 v2, 0x0
(( android.os.Parcel ) v0 ).writeParcelable ( p1, v2 ); // invoke-virtual {v0, p1, v2}, Landroid/os/Parcel;->writeParcelable(Landroid/os/Parcelable;I)V
/* .line 418 */
(( android.os.Parcel ) v0 ).writeString ( p2 ); // invoke-virtual {v0, p2}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V
/* .line 419 */
(( android.os.Parcel ) v0 ).writeParcelable ( p3, v2 ); // invoke-virtual {v0, p3, v2}, Landroid/os/Parcel;->writeParcelable(Landroid/os/Parcelable;I)V
/* .line 420 */
(( android.os.Parcel ) v0 ).writeLong ( p4, p5 ); // invoke-virtual {v0, p4, p5}, Landroid/os/Parcel;->writeLong(J)V
/* .line 421 */
(( android.os.Parcel ) v0 ).writeInt ( p6 ); // invoke-virtual {v0, p6}, Landroid/os/Parcel;->writeInt(I)V
/* .line 422 */
(( android.os.Parcel ) v0 ).writeInt ( p7 ); // invoke-virtual {v0, p7}, Landroid/os/Parcel;->writeInt(I)V
/* .line 423 */
(( android.os.Parcel ) v0 ).writeString ( p8 ); // invoke-virtual {v0, p8}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V
/* .line 424 */
(( android.os.Parcel ) v0 ).writeString ( p9 ); // invoke-virtual {v0, p9}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V
/* .line 425 */
v3 = this.mRemote;
/* const/16 v4, 0x9 */
/* .line 426 */
(( android.os.Parcel ) v1 ).readException ( ); // invoke-virtual {v1}, Landroid/os/Parcel;->readException()V
/* :try_end_0 */
/* .catchall {:try_start_0 ..:try_end_0} :catchall_0 */
/* .line 428 */
(( android.os.Parcel ) v1 ).recycle ( ); // invoke-virtual {v1}, Landroid/os/Parcel;->recycle()V
/* .line 429 */
(( android.os.Parcel ) v0 ).recycle ( ); // invoke-virtual {v0}, Landroid/os/Parcel;->recycle()V
/* .line 430 */
/* nop */
/* .line 431 */
return;
/* .line 428 */
/* :catchall_0 */
/* move-exception v2 */
(( android.os.Parcel ) v1 ).recycle ( ); // invoke-virtual {v1}, Landroid/os/Parcel;->recycle()V
/* .line 429 */
(( android.os.Parcel ) v0 ).recycle ( ); // invoke-virtual {v0}, Landroid/os/Parcel;->recycle()V
/* .line 430 */
/* throw v2 */
} // .end method
public void finishSpecificScenario ( android.os.Bundle p0, android.os.statistics.E2EScenarioPayload p1, Long p2, Integer p3, Integer p4, java.lang.String p5, java.lang.String p6 ) {
/* .locals 5 */
/* .param p1, "scenarioBundle" # Landroid/os/Bundle; */
/* .param p2, "payload" # Landroid/os/statistics/E2EScenarioPayload; */
/* .param p3, "uptimeMillis" # J */
/* .param p5, "pid" # I */
/* .param p6, "tid" # I */
/* .param p7, "processName" # Ljava/lang/String; */
/* .param p8, "packageName" # Ljava/lang/String; */
/* .annotation system Ldalvik/annotation/Throws; */
/* value = { */
/* Landroid/os/RemoteException; */
/* } */
} // .end annotation
/* .line 434 */
android.os.Parcel .obtain ( );
/* .line 435 */
/* .local v0, "_data":Landroid/os/Parcel; */
android.os.Parcel .obtain ( );
/* .line 437 */
/* .local v1, "_reply":Landroid/os/Parcel; */
try { // :try_start_0
final String v2 = "com.miui.daemon.performance.server.IMiuiPerfService"; // const-string v2, "com.miui.daemon.performance.server.IMiuiPerfService"
(( android.os.Parcel ) v0 ).writeInterfaceToken ( v2 ); // invoke-virtual {v0, v2}, Landroid/os/Parcel;->writeInterfaceToken(Ljava/lang/String;)V
/* .line 438 */
(( android.os.Parcel ) v0 ).writeBundle ( p1 ); // invoke-virtual {v0, p1}, Landroid/os/Parcel;->writeBundle(Landroid/os/Bundle;)V
/* .line 439 */
int v2 = 0; // const/4 v2, 0x0
(( android.os.Parcel ) v0 ).writeParcelable ( p2, v2 ); // invoke-virtual {v0, p2, v2}, Landroid/os/Parcel;->writeParcelable(Landroid/os/Parcelable;I)V
/* .line 440 */
(( android.os.Parcel ) v0 ).writeLong ( p3, p4 ); // invoke-virtual {v0, p3, p4}, Landroid/os/Parcel;->writeLong(J)V
/* .line 441 */
(( android.os.Parcel ) v0 ).writeInt ( p5 ); // invoke-virtual {v0, p5}, Landroid/os/Parcel;->writeInt(I)V
/* .line 442 */
(( android.os.Parcel ) v0 ).writeInt ( p6 ); // invoke-virtual {v0, p6}, Landroid/os/Parcel;->writeInt(I)V
/* .line 443 */
(( android.os.Parcel ) v0 ).writeString ( p7 ); // invoke-virtual {v0, p7}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V
/* .line 444 */
(( android.os.Parcel ) v0 ).writeString ( p8 ); // invoke-virtual {v0, p8}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V
/* .line 445 */
v3 = this.mRemote;
/* const/16 v4, 0xa */
/* .line 446 */
(( android.os.Parcel ) v1 ).readException ( ); // invoke-virtual {v1}, Landroid/os/Parcel;->readException()V
/* :try_end_0 */
/* .catchall {:try_start_0 ..:try_end_0} :catchall_0 */
/* .line 448 */
(( android.os.Parcel ) v1 ).recycle ( ); // invoke-virtual {v1}, Landroid/os/Parcel;->recycle()V
/* .line 449 */
(( android.os.Parcel ) v0 ).recycle ( ); // invoke-virtual {v0}, Landroid/os/Parcel;->recycle()V
/* .line 450 */
/* nop */
/* .line 451 */
return;
/* .line 448 */
/* :catchall_0 */
/* move-exception v2 */
(( android.os.Parcel ) v1 ).recycle ( ); // invoke-virtual {v1}, Landroid/os/Parcel;->recycle()V
/* .line 449 */
(( android.os.Parcel ) v0 ).recycle ( ); // invoke-virtual {v0}, Landroid/os/Parcel;->recycle()V
/* .line 450 */
/* throw v2 */
} // .end method
public java.lang.String getInterfaceDescriptor ( ) {
/* .locals 1 */
/* .line 264 */
final String v0 = "com.miui.daemon.performance.server.IMiuiPerfService"; // const-string v0, "com.miui.daemon.performance.server.IMiuiPerfService"
} // .end method
public android.os.ParcelFileDescriptor getPerfEventSocketFd ( ) {
/* .locals 5 */
/* .annotation system Ldalvik/annotation/Throws; */
/* value = { */
/* Landroid/os/RemoteException; */
/* } */
} // .end annotation
/* .line 328 */
android.os.Parcel .obtain ( );
/* .line 329 */
/* .local v0, "_data":Landroid/os/Parcel; */
android.os.Parcel .obtain ( );
/* .line 332 */
/* .local v1, "_reply":Landroid/os/Parcel; */
try { // :try_start_0
final String v2 = "com.miui.daemon.performance.server.IMiuiPerfService"; // const-string v2, "com.miui.daemon.performance.server.IMiuiPerfService"
(( android.os.Parcel ) v0 ).writeInterfaceToken ( v2 ); // invoke-virtual {v0, v2}, Landroid/os/Parcel;->writeInterfaceToken(Ljava/lang/String;)V
/* .line 333 */
v2 = this.mRemote;
int v3 = 5; // const/4 v3, 0x5
int v4 = 0; // const/4 v4, 0x0
/* .line 334 */
(( android.os.Parcel ) v1 ).readException ( ); // invoke-virtual {v1}, Landroid/os/Parcel;->readException()V
/* .line 335 */
v2 = (( android.os.Parcel ) v1 ).readInt ( ); // invoke-virtual {v1}, Landroid/os/Parcel;->readInt()I
if ( v2 != null) { // if-eqz v2, :cond_0
/* .line 336 */
v2 = android.os.ParcelFileDescriptor.CREATOR;
/* check-cast v2, Landroid/os/ParcelFileDescriptor; */
/* :try_end_0 */
/* .catchall {:try_start_0 ..:try_end_0} :catchall_0 */
/* .local v2, "_result":Landroid/os/ParcelFileDescriptor; */
/* .line 338 */
} // .end local v2 # "_result":Landroid/os/ParcelFileDescriptor;
} // :cond_0
int v2 = 0; // const/4 v2, 0x0
/* .line 341 */
/* .restart local v2 # "_result":Landroid/os/ParcelFileDescriptor; */
} // :goto_0
(( android.os.Parcel ) v1 ).recycle ( ); // invoke-virtual {v1}, Landroid/os/Parcel;->recycle()V
/* .line 342 */
(( android.os.Parcel ) v0 ).recycle ( ); // invoke-virtual {v0}, Landroid/os/Parcel;->recycle()V
/* .line 343 */
/* nop */
/* .line 344 */
/* .line 341 */
} // .end local v2 # "_result":Landroid/os/ParcelFileDescriptor;
/* :catchall_0 */
/* move-exception v2 */
(( android.os.Parcel ) v1 ).recycle ( ); // invoke-virtual {v1}, Landroid/os/Parcel;->recycle()V
/* .line 342 */
(( android.os.Parcel ) v0 ).recycle ( ); // invoke-virtual {v0}, Landroid/os/Parcel;->recycle()V
/* .line 343 */
/* throw v2 */
} // .end method
public void markPerceptibleJank ( android.os.Bundle p0 ) {
/* .locals 4 */
/* .param p1, "bundle" # Landroid/os/Bundle; */
/* .annotation system Ldalvik/annotation/Throws; */
/* value = { */
/* Landroid/os/RemoteException; */
/* } */
} // .end annotation
/* .line 268 */
android.os.Parcel .obtain ( );
/* .line 269 */
/* .local v0, "_data":Landroid/os/Parcel; */
android.os.Parcel .obtain ( );
/* .line 271 */
/* .local v1, "_reply":Landroid/os/Parcel; */
try { // :try_start_0
final String v2 = "com.miui.daemon.performance.server.IMiuiPerfService"; // const-string v2, "com.miui.daemon.performance.server.IMiuiPerfService"
(( android.os.Parcel ) v0 ).writeInterfaceToken ( v2 ); // invoke-virtual {v0, v2}, Landroid/os/Parcel;->writeInterfaceToken(Ljava/lang/String;)V
/* .line 272 */
int v2 = 1; // const/4 v2, 0x1
if ( p1 != null) { // if-eqz p1, :cond_0
/* .line 273 */
(( android.os.Parcel ) v0 ).writeInt ( v2 ); // invoke-virtual {v0, v2}, Landroid/os/Parcel;->writeInt(I)V
/* .line 274 */
int v3 = 0; // const/4 v3, 0x0
(( android.os.Bundle ) p1 ).writeToParcel ( v0, v3 ); // invoke-virtual {p1, v0, v3}, Landroid/os/Bundle;->writeToParcel(Landroid/os/Parcel;I)V
/* .line 276 */
} // :cond_0
v3 = this.mRemote;
/* .line 277 */
(( android.os.Parcel ) v1 ).readException ( ); // invoke-virtual {v1}, Landroid/os/Parcel;->readException()V
/* :try_end_0 */
/* .catchall {:try_start_0 ..:try_end_0} :catchall_0 */
/* .line 280 */
(( android.os.Parcel ) v1 ).recycle ( ); // invoke-virtual {v1}, Landroid/os/Parcel;->recycle()V
/* .line 281 */
(( android.os.Parcel ) v0 ).recycle ( ); // invoke-virtual {v0}, Landroid/os/Parcel;->recycle()V
/* .line 282 */
/* nop */
/* .line 283 */
return;
/* .line 280 */
/* :catchall_0 */
/* move-exception v2 */
(( android.os.Parcel ) v1 ).recycle ( ); // invoke-virtual {v1}, Landroid/os/Parcel;->recycle()V
/* .line 281 */
(( android.os.Parcel ) v0 ).recycle ( ); // invoke-virtual {v0}, Landroid/os/Parcel;->recycle()V
/* .line 282 */
/* throw v2 */
} // .end method
public void reportActivityLaunchRecords ( java.util.List p0 ) {
/* .locals 5 */
/* .annotation system Ldalvik/annotation/Signature; */
/* value = { */
/* "(", */
/* "Ljava/util/List<", */
/* "Landroid/os/Bundle;", */
/* ">;)V" */
/* } */
} // .end annotation
/* .annotation system Ldalvik/annotation/Throws; */
/* value = { */
/* Landroid/os/RemoteException; */
/* } */
} // .end annotation
/* .line 286 */
/* .local p1, "launchRecords":Ljava/util/List;, "Ljava/util/List<Landroid/os/Bundle;>;" */
android.os.Parcel .obtain ( );
/* .line 287 */
/* .local v0, "_data":Landroid/os/Parcel; */
final String v1 = "com.miui.daemon.performance.server.IMiuiPerfService"; // const-string v1, "com.miui.daemon.performance.server.IMiuiPerfService"
(( android.os.Parcel ) v0 ).writeInterfaceToken ( v1 ); // invoke-virtual {v0, v1}, Landroid/os/Parcel;->writeInterfaceToken(Ljava/lang/String;)V
/* .line 288 */
(( android.os.Parcel ) v0 ).writeTypedList ( p1 ); // invoke-virtual {v0, p1}, Landroid/os/Parcel;->writeTypedList(Ljava/util/List;)V
/* .line 289 */
android.os.Parcel .obtain ( );
/* .line 291 */
/* .local v1, "_reply":Landroid/os/Parcel; */
try { // :try_start_0
v2 = this.mRemote;
int v3 = 2; // const/4 v3, 0x2
int v4 = 1; // const/4 v4, 0x1
/* .line 292 */
(( android.os.Parcel ) v1 ).readException ( ); // invoke-virtual {v1}, Landroid/os/Parcel;->readException()V
/* :try_end_0 */
/* .catchall {:try_start_0 ..:try_end_0} :catchall_0 */
/* .line 294 */
(( android.os.Parcel ) v1 ).recycle ( ); // invoke-virtual {v1}, Landroid/os/Parcel;->recycle()V
/* .line 295 */
(( android.os.Parcel ) v0 ).recycle ( ); // invoke-virtual {v0}, Landroid/os/Parcel;->recycle()V
/* .line 296 */
/* nop */
/* .line 297 */
return;
/* .line 294 */
/* :catchall_0 */
/* move-exception v2 */
(( android.os.Parcel ) v1 ).recycle ( ); // invoke-virtual {v1}, Landroid/os/Parcel;->recycle()V
/* .line 295 */
(( android.os.Parcel ) v0 ).recycle ( ); // invoke-virtual {v0}, Landroid/os/Parcel;->recycle()V
/* .line 296 */
/* throw v2 */
} // .end method
public void reportApplicationStart ( android.os.Bundle p0 ) {
/* .locals 5 */
/* .param p1, "bundle" # Landroid/os/Bundle; */
/* .annotation system Ldalvik/annotation/Throws; */
/* value = { */
/* Landroid/os/RemoteException; */
/* } */
} // .end annotation
/* .line 560 */
android.os.Parcel .obtain ( );
/* .line 561 */
/* .local v0, "_data":Landroid/os/Parcel; */
android.os.Parcel .obtain ( );
/* .line 563 */
/* .local v1, "_reply":Landroid/os/Parcel; */
try { // :try_start_0
final String v2 = "com.miui.daemon.performance.server.IMiuiPerfService"; // const-string v2, "com.miui.daemon.performance.server.IMiuiPerfService"
(( android.os.Parcel ) v0 ).writeInterfaceToken ( v2 ); // invoke-virtual {v0, v2}, Landroid/os/Parcel;->writeInterfaceToken(Ljava/lang/String;)V
/* .line 564 */
if ( p1 != null) { // if-eqz p1, :cond_0
/* .line 565 */
(( android.os.Parcel ) v0 ).writeBundle ( p1 ); // invoke-virtual {v0, p1}, Landroid/os/Parcel;->writeBundle(Landroid/os/Bundle;)V
/* .line 566 */
v2 = this.mRemote;
/* const/16 v3, 0x11 */
int v4 = 1; // const/4 v4, 0x1
/* .line 569 */
} // :cond_0
(( android.os.Parcel ) v1 ).readException ( ); // invoke-virtual {v1}, Landroid/os/Parcel;->readException()V
/* :try_end_0 */
/* .catchall {:try_start_0 ..:try_end_0} :catchall_0 */
/* .line 572 */
(( android.os.Parcel ) v1 ).recycle ( ); // invoke-virtual {v1}, Landroid/os/Parcel;->recycle()V
/* .line 573 */
(( android.os.Parcel ) v0 ).recycle ( ); // invoke-virtual {v0}, Landroid/os/Parcel;->recycle()V
/* .line 574 */
/* nop */
/* .line 575 */
return;
/* .line 572 */
/* :catchall_0 */
/* move-exception v2 */
(( android.os.Parcel ) v1 ).recycle ( ); // invoke-virtual {v1}, Landroid/os/Parcel;->recycle()V
/* .line 573 */
(( android.os.Parcel ) v0 ).recycle ( ); // invoke-virtual {v0}, Landroid/os/Parcel;->recycle()V
/* .line 574 */
/* throw v2 */
} // .end method
public void reportExcessiveCpuUsageRecords ( java.util.List p0 ) {
/* .locals 5 */
/* .annotation system Ldalvik/annotation/Signature; */
/* value = { */
/* "(", */
/* "Ljava/util/List<", */
/* "Landroid/os/Bundle;", */
/* ">;)V" */
/* } */
} // .end annotation
/* .annotation system Ldalvik/annotation/Throws; */
/* value = { */
/* Landroid/os/RemoteException; */
/* } */
} // .end annotation
/* .line 454 */
/* .local p1, "records":Ljava/util/List;, "Ljava/util/List<Landroid/os/Bundle;>;" */
android.os.Parcel .obtain ( );
/* .line 455 */
/* .local v0, "_data":Landroid/os/Parcel; */
final String v1 = "com.miui.daemon.performance.server.IMiuiPerfService"; // const-string v1, "com.miui.daemon.performance.server.IMiuiPerfService"
(( android.os.Parcel ) v0 ).writeInterfaceToken ( v1 ); // invoke-virtual {v0, v1}, Landroid/os/Parcel;->writeInterfaceToken(Ljava/lang/String;)V
/* .line 456 */
(( android.os.Parcel ) v0 ).writeTypedList ( p1 ); // invoke-virtual {v0, p1}, Landroid/os/Parcel;->writeTypedList(Ljava/util/List;)V
/* .line 457 */
android.os.Parcel .obtain ( );
/* .line 459 */
/* .local v1, "_reply":Landroid/os/Parcel; */
try { // :try_start_0
v2 = this.mRemote;
/* const/16 v3, 0xb */
int v4 = 1; // const/4 v4, 0x1
/* .line 460 */
(( android.os.Parcel ) v1 ).readException ( ); // invoke-virtual {v1}, Landroid/os/Parcel;->readException()V
/* :try_end_0 */
/* .catchall {:try_start_0 ..:try_end_0} :catchall_0 */
/* .line 462 */
(( android.os.Parcel ) v1 ).recycle ( ); // invoke-virtual {v1}, Landroid/os/Parcel;->recycle()V
/* .line 463 */
(( android.os.Parcel ) v0 ).recycle ( ); // invoke-virtual {v0}, Landroid/os/Parcel;->recycle()V
/* .line 464 */
/* nop */
/* .line 465 */
return;
/* .line 462 */
/* :catchall_0 */
/* move-exception v2 */
(( android.os.Parcel ) v1 ).recycle ( ); // invoke-virtual {v1}, Landroid/os/Parcel;->recycle()V
/* .line 463 */
(( android.os.Parcel ) v0 ).recycle ( ); // invoke-virtual {v0}, Landroid/os/Parcel;->recycle()V
/* .line 464 */
/* throw v2 */
} // .end method
public void reportKillMessage ( java.lang.String p0, Integer p1, Integer p2, Long p3 ) {
/* .locals 5 */
/* .param p1, "processName" # Ljava/lang/String; */
/* .param p2, "pid" # I */
/* .param p3, "uid" # I */
/* .param p4, "pss" # J */
/* .annotation system Ldalvik/annotation/Throws; */
/* value = { */
/* Landroid/os/RemoteException; */
/* } */
} // .end annotation
/* .line 525 */
android.os.Parcel .obtain ( );
/* .line 526 */
/* .local v0, "_data":Landroid/os/Parcel; */
android.os.Parcel .obtain ( );
/* .line 528 */
/* .local v1, "_reply":Landroid/os/Parcel; */
try { // :try_start_0
final String v2 = "com.miui.daemon.performance.server.IMiuiPerfService"; // const-string v2, "com.miui.daemon.performance.server.IMiuiPerfService"
(( android.os.Parcel ) v0 ).writeInterfaceToken ( v2 ); // invoke-virtual {v0, v2}, Landroid/os/Parcel;->writeInterfaceToken(Ljava/lang/String;)V
/* .line 529 */
(( android.os.Parcel ) v0 ).writeString ( p1 ); // invoke-virtual {v0, p1}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V
/* .line 530 */
(( android.os.Parcel ) v0 ).writeInt ( p2 ); // invoke-virtual {v0, p2}, Landroid/os/Parcel;->writeInt(I)V
/* .line 531 */
(( android.os.Parcel ) v0 ).writeInt ( p3 ); // invoke-virtual {v0, p3}, Landroid/os/Parcel;->writeInt(I)V
/* .line 532 */
(( android.os.Parcel ) v0 ).writeLong ( p4, p5 ); // invoke-virtual {v0, p4, p5}, Landroid/os/Parcel;->writeLong(J)V
/* .line 533 */
v2 = this.mRemote;
/* const/16 v3, 0x10 */
int v4 = 1; // const/4 v4, 0x1
/* .line 534 */
(( android.os.Parcel ) v1 ).readException ( ); // invoke-virtual {v1}, Landroid/os/Parcel;->readException()V
/* :try_end_0 */
/* .catchall {:try_start_0 ..:try_end_0} :catchall_0 */
/* .line 536 */
(( android.os.Parcel ) v1 ).recycle ( ); // invoke-virtual {v1}, Landroid/os/Parcel;->recycle()V
/* .line 537 */
(( android.os.Parcel ) v0 ).recycle ( ); // invoke-virtual {v0}, Landroid/os/Parcel;->recycle()V
/* .line 538 */
/* nop */
/* .line 539 */
return;
/* .line 536 */
/* :catchall_0 */
/* move-exception v2 */
(( android.os.Parcel ) v1 ).recycle ( ); // invoke-virtual {v1}, Landroid/os/Parcel;->recycle()V
/* .line 537 */
(( android.os.Parcel ) v0 ).recycle ( ); // invoke-virtual {v0}, Landroid/os/Parcel;->recycle()V
/* .line 538 */
/* throw v2 */
} // .end method
public void reportKillProcessEvent ( android.os.Bundle p0 ) {
/* .locals 5 */
/* .param p1, "bundle" # Landroid/os/Bundle; */
/* .annotation system Ldalvik/annotation/Throws; */
/* value = { */
/* Landroid/os/RemoteException; */
/* } */
} // .end annotation
/* .line 579 */
android.os.Parcel .obtain ( );
/* .line 580 */
/* .local v0, "_data":Landroid/os/Parcel; */
android.os.Parcel .obtain ( );
/* .line 582 */
/* .local v1, "_reply":Landroid/os/Parcel; */
try { // :try_start_0
final String v2 = "com.miui.daemon.performance.server.IMiuiPerfService"; // const-string v2, "com.miui.daemon.performance.server.IMiuiPerfService"
(( android.os.Parcel ) v0 ).writeInterfaceToken ( v2 ); // invoke-virtual {v0, v2}, Landroid/os/Parcel;->writeInterfaceToken(Ljava/lang/String;)V
/* .line 583 */
if ( p1 != null) { // if-eqz p1, :cond_0
/* .line 584 */
(( android.os.Parcel ) v0 ).writeBundle ( p1 ); // invoke-virtual {v0, p1}, Landroid/os/Parcel;->writeBundle(Landroid/os/Bundle;)V
/* .line 585 */
v2 = this.mRemote;
/* const/16 v3, 0x12 */
int v4 = 1; // const/4 v4, 0x1
/* .line 588 */
} // :cond_0
(( android.os.Parcel ) v1 ).readException ( ); // invoke-virtual {v1}, Landroid/os/Parcel;->readException()V
/* :try_end_0 */
/* .catchall {:try_start_0 ..:try_end_0} :catchall_0 */
/* .line 591 */
(( android.os.Parcel ) v1 ).recycle ( ); // invoke-virtual {v1}, Landroid/os/Parcel;->recycle()V
/* .line 592 */
(( android.os.Parcel ) v0 ).recycle ( ); // invoke-virtual {v0}, Landroid/os/Parcel;->recycle()V
/* .line 593 */
/* nop */
/* .line 594 */
return;
/* .line 591 */
/* :catchall_0 */
/* move-exception v2 */
(( android.os.Parcel ) v1 ).recycle ( ); // invoke-virtual {v1}, Landroid/os/Parcel;->recycle()V
/* .line 592 */
(( android.os.Parcel ) v0 ).recycle ( ); // invoke-virtual {v0}, Landroid/os/Parcel;->recycle()V
/* .line 593 */
/* throw v2 */
} // .end method
public void reportMiSpeedRecord ( android.os.Bundle p0 ) {
/* .locals 5 */
/* .param p1, "data" # Landroid/os/Bundle; */
/* .annotation system Ldalvik/annotation/Throws; */
/* value = { */
/* Landroid/os/RemoteException; */
/* } */
} // .end annotation
/* .line 542 */
android.os.Parcel .obtain ( );
/* .line 543 */
/* .local v0, "_data":Landroid/os/Parcel; */
android.os.Parcel .obtain ( );
/* .line 546 */
/* .local v1, "_reply":Landroid/os/Parcel; */
try { // :try_start_0
final String v2 = "com.miui.daemon.performance.server.IMiuiPerfService"; // const-string v2, "com.miui.daemon.performance.server.IMiuiPerfService"
(( android.os.Parcel ) v0 ).writeInterfaceToken ( v2 ); // invoke-virtual {v0, v2}, Landroid/os/Parcel;->writeInterfaceToken(Ljava/lang/String;)V
/* .line 547 */
(( android.os.Parcel ) v0 ).writeBundle ( p1 ); // invoke-virtual {v0, p1}, Landroid/os/Parcel;->writeBundle(Landroid/os/Bundle;)V
/* .line 548 */
v2 = this.mRemote;
/* const/16 v3, 0xf */
int v4 = 1; // const/4 v4, 0x1
/* .line 549 */
(( android.os.Parcel ) v1 ).readException ( ); // invoke-virtual {v1}, Landroid/os/Parcel;->readException()V
/* :try_end_0 */
/* .catchall {:try_start_0 ..:try_end_0} :catchall_0 */
/* .line 552 */
(( android.os.Parcel ) v1 ).recycle ( ); // invoke-virtual {v1}, Landroid/os/Parcel;->recycle()V
/* .line 553 */
(( android.os.Parcel ) v0 ).recycle ( ); // invoke-virtual {v0}, Landroid/os/Parcel;->recycle()V
/* .line 554 */
/* nop */
/* .line 556 */
return;
/* .line 552 */
/* :catchall_0 */
/* move-exception v2 */
(( android.os.Parcel ) v1 ).recycle ( ); // invoke-virtual {v1}, Landroid/os/Parcel;->recycle()V
/* .line 553 */
(( android.os.Parcel ) v0 ).recycle ( ); // invoke-virtual {v0}, Landroid/os/Parcel;->recycle()V
/* .line 554 */
/* throw v2 */
} // .end method
public void reportNotificationClick ( java.lang.String p0, android.content.Intent p1, Long p2 ) {
/* .locals 5 */
/* .param p1, "postPackage" # Ljava/lang/String; */
/* .param p2, "intent" # Landroid/content/Intent; */
/* .param p3, "uptimeMillis" # J */
/* .annotation system Ldalvik/annotation/Throws; */
/* value = { */
/* Landroid/os/RemoteException; */
/* } */
} // .end annotation
/* .line 468 */
android.os.Parcel .obtain ( );
/* .line 469 */
/* .local v0, "_data":Landroid/os/Parcel; */
android.os.Parcel .obtain ( );
/* .line 471 */
/* .local v1, "_reply":Landroid/os/Parcel; */
try { // :try_start_0
final String v2 = "com.miui.daemon.performance.server.IMiuiPerfService"; // const-string v2, "com.miui.daemon.performance.server.IMiuiPerfService"
(( android.os.Parcel ) v0 ).writeInterfaceToken ( v2 ); // invoke-virtual {v0, v2}, Landroid/os/Parcel;->writeInterfaceToken(Ljava/lang/String;)V
/* .line 472 */
(( android.os.Parcel ) v0 ).writeString ( p1 ); // invoke-virtual {v0, p1}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V
/* .line 473 */
int v2 = 1; // const/4 v2, 0x1
int v3 = 0; // const/4 v3, 0x0
if ( p2 != null) { // if-eqz p2, :cond_0
/* .line 474 */
(( android.os.Parcel ) v0 ).writeInt ( v2 ); // invoke-virtual {v0, v2}, Landroid/os/Parcel;->writeInt(I)V
/* .line 475 */
(( android.content.Intent ) p2 ).writeToParcel ( v0, v3 ); // invoke-virtual {p2, v0, v3}, Landroid/content/Intent;->writeToParcel(Landroid/os/Parcel;I)V
/* .line 477 */
} // :cond_0
(( android.os.Parcel ) v0 ).writeInt ( v3 ); // invoke-virtual {v0, v3}, Landroid/os/Parcel;->writeInt(I)V
/* .line 479 */
} // :goto_0
(( android.os.Parcel ) v0 ).writeLong ( p3, p4 ); // invoke-virtual {v0, p3, p4}, Landroid/os/Parcel;->writeLong(J)V
/* .line 480 */
v3 = this.mRemote;
/* const/16 v4, 0xc */
/* .line 481 */
(( android.os.Parcel ) v1 ).readException ( ); // invoke-virtual {v1}, Landroid/os/Parcel;->readException()V
/* :try_end_0 */
/* .catchall {:try_start_0 ..:try_end_0} :catchall_0 */
/* .line 483 */
(( android.os.Parcel ) v1 ).recycle ( ); // invoke-virtual {v1}, Landroid/os/Parcel;->recycle()V
/* .line 484 */
(( android.os.Parcel ) v0 ).recycle ( ); // invoke-virtual {v0}, Landroid/os/Parcel;->recycle()V
/* .line 485 */
/* nop */
/* .line 486 */
return;
/* .line 483 */
/* :catchall_0 */
/* move-exception v2 */
(( android.os.Parcel ) v1 ).recycle ( ); // invoke-virtual {v1}, Landroid/os/Parcel;->recycle()V
/* .line 484 */
(( android.os.Parcel ) v0 ).recycle ( ); // invoke-virtual {v0}, Landroid/os/Parcel;->recycle()V
/* .line 485 */
/* throw v2 */
} // .end method
public void reportProcessCleanEvent ( android.os.Bundle p0 ) {
/* .locals 5 */
/* .param p1, "bundle" # Landroid/os/Bundle; */
/* .annotation system Ldalvik/annotation/Throws; */
/* value = { */
/* Landroid/os/RemoteException; */
/* } */
} // .end annotation
/* .line 489 */
android.os.Parcel .obtain ( );
/* .line 490 */
/* .local v0, "_data":Landroid/os/Parcel; */
android.os.Parcel .obtain ( );
/* .line 492 */
/* .local v1, "_reply":Landroid/os/Parcel; */
try { // :try_start_0
final String v2 = "com.miui.daemon.performance.server.IMiuiPerfService"; // const-string v2, "com.miui.daemon.performance.server.IMiuiPerfService"
(( android.os.Parcel ) v0 ).writeInterfaceToken ( v2 ); // invoke-virtual {v0, v2}, Landroid/os/Parcel;->writeInterfaceToken(Ljava/lang/String;)V
/* .line 493 */
int v2 = 1; // const/4 v2, 0x1
if ( p1 != null) { // if-eqz p1, :cond_0
/* .line 494 */
(( android.os.Parcel ) v0 ).writeInt ( v2 ); // invoke-virtual {v0, v2}, Landroid/os/Parcel;->writeInt(I)V
/* .line 495 */
int v3 = 0; // const/4 v3, 0x0
(( android.os.Bundle ) p1 ).writeToParcel ( v0, v3 ); // invoke-virtual {p1, v0, v3}, Landroid/os/Bundle;->writeToParcel(Landroid/os/Parcel;I)V
/* .line 497 */
} // :cond_0
v3 = this.mRemote;
/* const/16 v4, 0xd */
/* .line 498 */
(( android.os.Parcel ) v1 ).readException ( ); // invoke-virtual {v1}, Landroid/os/Parcel;->readException()V
/* :try_end_0 */
/* .catchall {:try_start_0 ..:try_end_0} :catchall_0 */
/* .line 501 */
(( android.os.Parcel ) v1 ).recycle ( ); // invoke-virtual {v1}, Landroid/os/Parcel;->recycle()V
/* .line 502 */
(( android.os.Parcel ) v0 ).recycle ( ); // invoke-virtual {v0}, Landroid/os/Parcel;->recycle()V
/* .line 503 */
/* nop */
/* .line 504 */
return;
/* .line 501 */
/* :catchall_0 */
/* move-exception v2 */
(( android.os.Parcel ) v1 ).recycle ( ); // invoke-virtual {v1}, Landroid/os/Parcel;->recycle()V
/* .line 502 */
(( android.os.Parcel ) v0 ).recycle ( ); // invoke-virtual {v0}, Landroid/os/Parcel;->recycle()V
/* .line 503 */
/* throw v2 */
} // .end method
public void reportPssRecord ( java.lang.String p0, java.lang.String p1, Long p2, java.lang.String p3, Integer p4 ) {
/* .locals 5 */
/* .param p1, "processName" # Ljava/lang/String; */
/* .param p2, "packageName" # Ljava/lang/String; */
/* .param p3, "pss" # J */
/* .param p5, "versionName" # Ljava/lang/String; */
/* .param p6, "versionCode" # I */
/* .annotation system Ldalvik/annotation/Throws; */
/* value = { */
/* Landroid/os/RemoteException; */
/* } */
} // .end annotation
/* .line 507 */
android.os.Parcel .obtain ( );
/* .line 508 */
/* .local v0, "_data":Landroid/os/Parcel; */
android.os.Parcel .obtain ( );
/* .line 510 */
/* .local v1, "_reply":Landroid/os/Parcel; */
try { // :try_start_0
final String v2 = "com.miui.daemon.performance.server.IMiuiPerfService"; // const-string v2, "com.miui.daemon.performance.server.IMiuiPerfService"
(( android.os.Parcel ) v0 ).writeInterfaceToken ( v2 ); // invoke-virtual {v0, v2}, Landroid/os/Parcel;->writeInterfaceToken(Ljava/lang/String;)V
/* .line 511 */
(( android.os.Parcel ) v0 ).writeString ( p1 ); // invoke-virtual {v0, p1}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V
/* .line 512 */
(( android.os.Parcel ) v0 ).writeString ( p2 ); // invoke-virtual {v0, p2}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V
/* .line 513 */
(( android.os.Parcel ) v0 ).writeLong ( p3, p4 ); // invoke-virtual {v0, p3, p4}, Landroid/os/Parcel;->writeLong(J)V
/* .line 514 */
(( android.os.Parcel ) v0 ).writeString ( p5 ); // invoke-virtual {v0, p5}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V
/* .line 515 */
(( android.os.Parcel ) v0 ).writeInt ( p6 ); // invoke-virtual {v0, p6}, Landroid/os/Parcel;->writeInt(I)V
/* .line 516 */
v2 = this.mRemote;
/* const/16 v3, 0xe */
int v4 = 1; // const/4 v4, 0x1
/* .line 517 */
(( android.os.Parcel ) v1 ).readException ( ); // invoke-virtual {v1}, Landroid/os/Parcel;->readException()V
/* :try_end_0 */
/* .catchall {:try_start_0 ..:try_end_0} :catchall_0 */
/* .line 519 */
(( android.os.Parcel ) v1 ).recycle ( ); // invoke-virtual {v1}, Landroid/os/Parcel;->recycle()V
/* .line 520 */
(( android.os.Parcel ) v0 ).recycle ( ); // invoke-virtual {v0}, Landroid/os/Parcel;->recycle()V
/* .line 521 */
/* nop */
/* .line 522 */
return;
/* .line 519 */
/* :catchall_0 */
/* move-exception v2 */
(( android.os.Parcel ) v1 ).recycle ( ); // invoke-virtual {v1}, Landroid/os/Parcel;->recycle()V
/* .line 520 */
(( android.os.Parcel ) v0 ).recycle ( ); // invoke-virtual {v0}, Landroid/os/Parcel;->recycle()V
/* .line 521 */
/* throw v2 */
} // .end method
public void setSchedFgPid ( Integer p0 ) {
/* .locals 5 */
/* .param p1, "pid" # I */
/* .annotation system Ldalvik/annotation/Throws; */
/* value = { */
/* Landroid/os/RemoteException; */
/* } */
} // .end annotation
/* .line 314 */
android.os.Parcel .obtain ( );
/* .line 315 */
/* .local v0, "_data":Landroid/os/Parcel; */
final String v1 = "com.miui.daemon.performance.server.IMiuiPerfService"; // const-string v1, "com.miui.daemon.performance.server.IMiuiPerfService"
(( android.os.Parcel ) v0 ).writeInterfaceToken ( v1 ); // invoke-virtual {v0, v1}, Landroid/os/Parcel;->writeInterfaceToken(Ljava/lang/String;)V
/* .line 316 */
(( android.os.Parcel ) v0 ).writeInt ( p1 ); // invoke-virtual {v0, p1}, Landroid/os/Parcel;->writeInt(I)V
/* .line 317 */
android.os.Parcel .obtain ( );
/* .line 319 */
/* .local v1, "_reply":Landroid/os/Parcel; */
try { // :try_start_0
v2 = this.mRemote;
int v3 = 4; // const/4 v3, 0x4
int v4 = 1; // const/4 v4, 0x1
/* .line 320 */
(( android.os.Parcel ) v1 ).readException ( ); // invoke-virtual {v1}, Landroid/os/Parcel;->readException()V
/* :try_end_0 */
/* .catchall {:try_start_0 ..:try_end_0} :catchall_0 */
/* .line 322 */
(( android.os.Parcel ) v1 ).recycle ( ); // invoke-virtual {v1}, Landroid/os/Parcel;->recycle()V
/* .line 323 */
(( android.os.Parcel ) v0 ).recycle ( ); // invoke-virtual {v0}, Landroid/os/Parcel;->recycle()V
/* .line 324 */
/* nop */
/* .line 325 */
return;
/* .line 322 */
/* :catchall_0 */
/* move-exception v2 */
(( android.os.Parcel ) v1 ).recycle ( ); // invoke-virtual {v1}, Landroid/os/Parcel;->recycle()V
/* .line 323 */
(( android.os.Parcel ) v0 ).recycle ( ); // invoke-virtual {v0}, Landroid/os/Parcel;->recycle()V
/* .line 324 */
/* throw v2 */
} // .end method
