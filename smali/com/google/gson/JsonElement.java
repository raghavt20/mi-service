public abstract class com.google.gson.JsonElement {
	 /* .source "JsonElement.java" */
	 /* # direct methods */
	 public com.google.gson.JsonElement ( ) {
		 /* .locals 0 */
		 /* .line 33 */
		 /* invoke-direct {p0}, Ljava/lang/Object;-><init>()V */
		 return;
	 } // .end method
	 /* # virtual methods */
	 public abstract com.google.gson.JsonElement deepCopy ( ) {
	 } // .end method
	 public java.math.BigDecimal getAsBigDecimal ( ) {
		 /* .locals 2 */
		 /* .line 277 */
		 /* new-instance v0, Ljava/lang/UnsupportedOperationException; */
		 (( java.lang.Object ) p0 ).getClass ( ); // invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;
		 (( java.lang.Class ) v1 ).getSimpleName ( ); // invoke-virtual {v1}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;
		 /* invoke-direct {v0, v1}, Ljava/lang/UnsupportedOperationException;-><init>(Ljava/lang/String;)V */
		 /* throw v0 */
	 } // .end method
	 public java.math.BigInteger getAsBigInteger ( ) {
		 /* .locals 2 */
		 /* .line 291 */
		 /* new-instance v0, Ljava/lang/UnsupportedOperationException; */
		 (( java.lang.Object ) p0 ).getClass ( ); // invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;
		 (( java.lang.Class ) v1 ).getSimpleName ( ); // invoke-virtual {v1}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;
		 /* invoke-direct {v0, v1}, Ljava/lang/UnsupportedOperationException;-><init>(Ljava/lang/String;)V */
		 /* throw v0 */
	 } // .end method
	 public Boolean getAsBoolean ( ) {
		 /* .locals 2 */
		 /* .line 153 */
		 /* new-instance v0, Ljava/lang/UnsupportedOperationException; */
		 (( java.lang.Object ) p0 ).getClass ( ); // invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;
		 (( java.lang.Class ) v1 ).getSimpleName ( ); // invoke-virtual {v1}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;
		 /* invoke-direct {v0, v1}, Ljava/lang/UnsupportedOperationException;-><init>(Ljava/lang/String;)V */
		 /* throw v0 */
	 } // .end method
	 public Object getAsByte ( ) {
		 /* .locals 2 */
		 /* .line 245 */
		 /* new-instance v0, Ljava/lang/UnsupportedOperationException; */
		 (( java.lang.Object ) p0 ).getClass ( ); // invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;
		 (( java.lang.Class ) v1 ).getSimpleName ( ); // invoke-virtual {v1}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;
		 /* invoke-direct {v0, v1}, Ljava/lang/UnsupportedOperationException;-><init>(Ljava/lang/String;)V */
		 /* throw v0 */
	 } // .end method
	 public Object getAsCharacter ( ) {
		 /* .locals 2 */
		 /* .annotation runtime Ljava/lang/Deprecated; */
	 } // .end annotation
	 /* .line 263 */
	 /* new-instance v0, Ljava/lang/UnsupportedOperationException; */
	 (( java.lang.Object ) p0 ).getClass ( ); // invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;
	 (( java.lang.Class ) v1 ).getSimpleName ( ); // invoke-virtual {v1}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;
	 /* invoke-direct {v0, v1}, Ljava/lang/UnsupportedOperationException;-><init>(Ljava/lang/String;)V */
	 /* throw v0 */
} // .end method
public Double getAsDouble ( ) {
	 /* .locals 2 */
	 /* .line 192 */
	 /* new-instance v0, Ljava/lang/UnsupportedOperationException; */
	 (( java.lang.Object ) p0 ).getClass ( ); // invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;
	 (( java.lang.Class ) v1 ).getSimpleName ( ); // invoke-virtual {v1}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;
	 /* invoke-direct {v0, v1}, Ljava/lang/UnsupportedOperationException;-><init>(Ljava/lang/String;)V */
	 /* throw v0 */
} // .end method
public Float getAsFloat ( ) {
	 /* .locals 2 */
	 /* .line 205 */
	 /* new-instance v0, Ljava/lang/UnsupportedOperationException; */
	 (( java.lang.Object ) p0 ).getClass ( ); // invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;
	 (( java.lang.Class ) v1 ).getSimpleName ( ); // invoke-virtual {v1}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;
	 /* invoke-direct {v0, v1}, Ljava/lang/UnsupportedOperationException;-><init>(Ljava/lang/String;)V */
	 /* throw v0 */
} // .end method
public Integer getAsInt ( ) {
	 /* .locals 2 */
	 /* .line 231 */
	 /* new-instance v0, Ljava/lang/UnsupportedOperationException; */
	 (( java.lang.Object ) p0 ).getClass ( ); // invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;
	 (( java.lang.Class ) v1 ).getSimpleName ( ); // invoke-virtual {v1}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;
	 /* invoke-direct {v0, v1}, Ljava/lang/UnsupportedOperationException;-><init>(Ljava/lang/String;)V */
	 /* throw v0 */
} // .end method
public com.google.gson.JsonArray getAsJsonArray ( ) {
	 /* .locals 3 */
	 /* .line 104 */
	 v0 = 	 (( com.google.gson.JsonElement ) p0 ).isJsonArray ( ); // invoke-virtual {p0}, Lcom/google/gson/JsonElement;->isJsonArray()Z
	 if ( v0 != null) { // if-eqz v0, :cond_0
		 /* .line 105 */
		 /* move-object v0, p0 */
		 /* check-cast v0, Lcom/google/gson/JsonArray; */
		 /* .line 107 */
	 } // :cond_0
	 /* new-instance v0, Ljava/lang/IllegalStateException; */
	 /* new-instance v1, Ljava/lang/StringBuilder; */
	 /* invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V */
	 final String v2 = "Not a JSON Array: "; // const-string v2, "Not a JSON Array: "
	 (( java.lang.StringBuilder ) v1 ).append ( v2 ); // invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
	 (( java.lang.StringBuilder ) v1 ).append ( p0 ); // invoke-virtual {v1, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;
	 (( java.lang.StringBuilder ) v1 ).toString ( ); // invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
	 /* invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V */
	 /* throw v0 */
} // .end method
public com.google.gson.JsonNull getAsJsonNull ( ) {
	 /* .locals 3 */
	 /* .line 137 */
	 v0 = 	 (( com.google.gson.JsonElement ) p0 ).isJsonNull ( ); // invoke-virtual {p0}, Lcom/google/gson/JsonElement;->isJsonNull()Z
	 if ( v0 != null) { // if-eqz v0, :cond_0
		 /* .line 138 */
		 /* move-object v0, p0 */
		 /* check-cast v0, Lcom/google/gson/JsonNull; */
		 /* .line 140 */
	 } // :cond_0
	 /* new-instance v0, Ljava/lang/IllegalStateException; */
	 /* new-instance v1, Ljava/lang/StringBuilder; */
	 /* invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V */
	 final String v2 = "Not a JSON Null: "; // const-string v2, "Not a JSON Null: "
	 (( java.lang.StringBuilder ) v1 ).append ( v2 ); // invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
	 (( java.lang.StringBuilder ) v1 ).append ( p0 ); // invoke-virtual {v1, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;
	 (( java.lang.StringBuilder ) v1 ).toString ( ); // invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
	 /* invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V */
	 /* throw v0 */
} // .end method
public com.google.gson.JsonObject getAsJsonObject ( ) {
	 /* .locals 3 */
	 /* .line 88 */
	 v0 = 	 (( com.google.gson.JsonElement ) p0 ).isJsonObject ( ); // invoke-virtual {p0}, Lcom/google/gson/JsonElement;->isJsonObject()Z
	 if ( v0 != null) { // if-eqz v0, :cond_0
		 /* .line 89 */
		 /* move-object v0, p0 */
		 /* check-cast v0, Lcom/google/gson/JsonObject; */
		 /* .line 91 */
	 } // :cond_0
	 /* new-instance v0, Ljava/lang/IllegalStateException; */
	 /* new-instance v1, Ljava/lang/StringBuilder; */
	 /* invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V */
	 final String v2 = "Not a JSON Object: "; // const-string v2, "Not a JSON Object: "
	 (( java.lang.StringBuilder ) v1 ).append ( v2 ); // invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
	 (( java.lang.StringBuilder ) v1 ).append ( p0 ); // invoke-virtual {v1, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;
	 (( java.lang.StringBuilder ) v1 ).toString ( ); // invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
	 /* invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V */
	 /* throw v0 */
} // .end method
public com.google.gson.JsonPrimitive getAsJsonPrimitive ( ) {
	 /* .locals 3 */
	 /* .line 120 */
	 v0 = 	 (( com.google.gson.JsonElement ) p0 ).isJsonPrimitive ( ); // invoke-virtual {p0}, Lcom/google/gson/JsonElement;->isJsonPrimitive()Z
	 if ( v0 != null) { // if-eqz v0, :cond_0
		 /* .line 121 */
		 /* move-object v0, p0 */
		 /* check-cast v0, Lcom/google/gson/JsonPrimitive; */
		 /* .line 123 */
	 } // :cond_0
	 /* new-instance v0, Ljava/lang/IllegalStateException; */
	 /* new-instance v1, Ljava/lang/StringBuilder; */
	 /* invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V */
	 final String v2 = "Not a JSON Primitive: "; // const-string v2, "Not a JSON Primitive: "
	 (( java.lang.StringBuilder ) v1 ).append ( v2 ); // invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
	 (( java.lang.StringBuilder ) v1 ).append ( p0 ); // invoke-virtual {v1, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;
	 (( java.lang.StringBuilder ) v1 ).toString ( ); // invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
	 /* invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V */
	 /* throw v0 */
} // .end method
public Long getAsLong ( ) {
	 /* .locals 2 */
	 /* .line 218 */
	 /* new-instance v0, Ljava/lang/UnsupportedOperationException; */
	 (( java.lang.Object ) p0 ).getClass ( ); // invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;
	 (( java.lang.Class ) v1 ).getSimpleName ( ); // invoke-virtual {v1}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;
	 /* invoke-direct {v0, v1}, Ljava/lang/UnsupportedOperationException;-><init>(Ljava/lang/String;)V */
	 /* throw v0 */
} // .end method
public java.lang.Number getAsNumber ( ) {
	 /* .locals 2 */
	 /* .line 166 */
	 /* new-instance v0, Ljava/lang/UnsupportedOperationException; */
	 (( java.lang.Object ) p0 ).getClass ( ); // invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;
	 (( java.lang.Class ) v1 ).getSimpleName ( ); // invoke-virtual {v1}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;
	 /* invoke-direct {v0, v1}, Ljava/lang/UnsupportedOperationException;-><init>(Ljava/lang/String;)V */
	 /* throw v0 */
} // .end method
public Object getAsShort ( ) {
	 /* .locals 2 */
	 /* .line 304 */
	 /* new-instance v0, Ljava/lang/UnsupportedOperationException; */
	 (( java.lang.Object ) p0 ).getClass ( ); // invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;
	 (( java.lang.Class ) v1 ).getSimpleName ( ); // invoke-virtual {v1}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;
	 /* invoke-direct {v0, v1}, Ljava/lang/UnsupportedOperationException;-><init>(Ljava/lang/String;)V */
	 /* throw v0 */
} // .end method
public java.lang.String getAsString ( ) {
	 /* .locals 2 */
	 /* .line 179 */
	 /* new-instance v0, Ljava/lang/UnsupportedOperationException; */
	 (( java.lang.Object ) p0 ).getClass ( ); // invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;
	 (( java.lang.Class ) v1 ).getSimpleName ( ); // invoke-virtual {v1}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;
	 /* invoke-direct {v0, v1}, Ljava/lang/UnsupportedOperationException;-><init>(Ljava/lang/String;)V */
	 /* throw v0 */
} // .end method
public Boolean isJsonArray ( ) {
	 /* .locals 1 */
	 /* .line 47 */
	 /* instance-of v0, p0, Lcom/google/gson/JsonArray; */
} // .end method
public Boolean isJsonNull ( ) {
	 /* .locals 1 */
	 /* .line 75 */
	 /* instance-of v0, p0, Lcom/google/gson/JsonNull; */
} // .end method
public Boolean isJsonObject ( ) {
	 /* .locals 1 */
	 /* .line 56 */
	 /* instance-of v0, p0, Lcom/google/gson/JsonObject; */
} // .end method
public Boolean isJsonPrimitive ( ) {
	 /* .locals 1 */
	 /* .line 65 */
	 /* instance-of v0, p0, Lcom/google/gson/JsonPrimitive; */
} // .end method
public java.lang.String toString ( ) {
	 /* .locals 3 */
	 /* .line 313 */
	 try { // :try_start_0
		 /* new-instance v0, Ljava/io/StringWriter; */
		 /* invoke-direct {v0}, Ljava/io/StringWriter;-><init>()V */
		 /* .line 314 */
		 /* .local v0, "stringWriter":Ljava/io/StringWriter; */
		 /* new-instance v1, Lcom/google/gson/stream/JsonWriter; */
		 /* invoke-direct {v1, v0}, Lcom/google/gson/stream/JsonWriter;-><init>(Ljava/io/Writer;)V */
		 /* .line 315 */
		 /* .local v1, "jsonWriter":Lcom/google/gson/stream/JsonWriter; */
		 int v2 = 1; // const/4 v2, 0x1
		 (( com.google.gson.stream.JsonWriter ) v1 ).setLenient ( v2 ); // invoke-virtual {v1, v2}, Lcom/google/gson/stream/JsonWriter;->setLenient(Z)V
		 /* .line 316 */
		 com.google.gson.internal.Streams .write ( p0,v1 );
		 /* .line 317 */
		 (( java.io.StringWriter ) v0 ).toString ( ); // invoke-virtual {v0}, Ljava/io/StringWriter;->toString()Ljava/lang/String;
		 /* :try_end_0 */
		 /* .catch Ljava/io/IOException; {:try_start_0 ..:try_end_0} :catch_0 */
		 /* .line 318 */
	 } // .end local v0 # "stringWriter":Ljava/io/StringWriter;
} // .end local v1 # "jsonWriter":Lcom/google/gson/stream/JsonWriter;
/* :catch_0 */
/* move-exception v0 */
/* .line 319 */
/* .local v0, "e":Ljava/io/IOException; */
/* new-instance v1, Ljava/lang/AssertionError; */
/* invoke-direct {v1, v0}, Ljava/lang/AssertionError;-><init>(Ljava/lang/Object;)V */
/* throw v1 */
} // .end method
