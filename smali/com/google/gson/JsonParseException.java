public class com.google.gson.JsonParseException extends java.lang.RuntimeException {
	 /* .source "JsonParseException.java" */
	 /* # static fields */
	 static final Long serialVersionUID;
	 /* # direct methods */
	 public com.google.gson.JsonParseException ( ) {
		 /* .locals 0 */
		 /* .param p1, "msg" # Ljava/lang/String; */
		 /* .line 42 */
		 /* invoke-direct {p0, p1}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V */
		 /* .line 43 */
		 return;
	 } // .end method
	 public com.google.gson.JsonParseException ( ) {
		 /* .locals 0 */
		 /* .param p1, "msg" # Ljava/lang/String; */
		 /* .param p2, "cause" # Ljava/lang/Throwable; */
		 /* .line 52 */
		 /* invoke-direct {p0, p1, p2}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;Ljava/lang/Throwable;)V */
		 /* .line 53 */
		 return;
	 } // .end method
	 public com.google.gson.JsonParseException ( ) {
		 /* .locals 0 */
		 /* .param p1, "cause" # Ljava/lang/Throwable; */
		 /* .line 62 */
		 /* invoke-direct {p0, p1}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/Throwable;)V */
		 /* .line 63 */
		 return;
	 } // .end method
