public class com.google.gson.reflect.TypeToken {
	 /* .source "TypeToken.java" */
	 /* # annotations */
	 /* .annotation system Ldalvik/annotation/Signature; */
	 /* value = { */
	 /* "<T:", */
	 /* "Ljava/lang/Object;", */
	 /* ">", */
	 /* "Ljava/lang/Object;" */
	 /* } */
} // .end annotation
/* # instance fields */
final Integer hashCode;
final java.lang.Class rawType;
/* .annotation system Ldalvik/annotation/Signature; */
/* value = { */
/* "Ljava/lang/Class<", */
/* "-TT;>;" */
/* } */
} // .end annotation
} // .end field
final java.lang.reflect.Type type;
/* # direct methods */
protected com.google.gson.reflect.TypeToken ( ) {
/* .locals 2 */
/* .line 61 */
/* .local p0, "this":Lcom/google/gson/reflect/TypeToken;, "Lcom/google/gson/reflect/TypeToken<TT;>;" */
/* invoke-direct {p0}, Ljava/lang/Object;-><init>()V */
/* .line 62 */
(( java.lang.Object ) p0 ).getClass ( ); // invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;
com.google.gson.reflect.TypeToken .getSuperclassTypeParameter ( v0 );
this.type = v0;
/* .line 63 */
com.google.gson.internal.$Gson$Types .getRawType ( v0 );
this.rawType = v1;
/* .line 64 */
v0 = (( java.lang.Object ) v0 ).hashCode ( ); // invoke-virtual {v0}, Ljava/lang/Object;->hashCode()I
/* iput v0, p0, Lcom/google/gson/reflect/TypeToken;->hashCode:I */
/* .line 65 */
return;
} // .end method
 com.google.gson.reflect.TypeToken ( ) {
/* .locals 2 */
/* .param p1, "type" # Ljava/lang/reflect/Type; */
/* .line 71 */
/* .local p0, "this":Lcom/google/gson/reflect/TypeToken;, "Lcom/google/gson/reflect/TypeToken<TT;>;" */
/* invoke-direct {p0}, Ljava/lang/Object;-><init>()V */
/* .line 72 */
com.google.gson.internal.$Gson$Preconditions .checkNotNull ( p1 );
/* check-cast v0, Ljava/lang/reflect/Type; */
com.google.gson.internal.$Gson$Types .canonicalize ( v0 );
this.type = v0;
/* .line 73 */
com.google.gson.internal.$Gson$Types .getRawType ( v0 );
this.rawType = v1;
/* .line 74 */
v0 = (( java.lang.Object ) v0 ).hashCode ( ); // invoke-virtual {v0}, Ljava/lang/Object;->hashCode()I
/* iput v0, p0, Lcom/google/gson/reflect/TypeToken;->hashCode:I */
/* .line 75 */
return;
} // .end method
private static java.lang.AssertionError buildUnexpectedTypeError ( java.lang.reflect.Type p0, java.lang.Class...p1 ) {
/* .locals 6 */
/* .param p0, "token" # Ljava/lang/reflect/Type; */
/* .annotation system Ldalvik/annotation/Signature; */
/* value = { */
/* "(", */
/* "Ljava/lang/reflect/Type;", */
/* "[", */
/* "Ljava/lang/Class<", */
/* "*>;)", */
/* "Ljava/lang/AssertionError;" */
/* } */
} // .end annotation
/* .line 257 */
/* .local p1, "expected":[Ljava/lang/Class;, "[Ljava/lang/Class<*>;" */
/* new-instance v0, Ljava/lang/StringBuilder; */
final String v1 = "Unexpected type.Expected one of: "; // const-string v1, "Unexpected type.Expected one of: "
/* invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V */
/* .line 259 */
/* .local v0, "exceptionMessage":Ljava/lang/StringBuilder; */
/* array-length v1, p1 */
int v2 = 0; // const/4 v2, 0x0
} // :goto_0
/* if-ge v2, v1, :cond_0 */
/* aget-object v3, p1, v2 */
/* .line 260 */
/* .local v3, "clazz":Ljava/lang/Class;, "Ljava/lang/Class<*>;" */
(( java.lang.Class ) v3 ).getName ( ); // invoke-virtual {v3}, Ljava/lang/Class;->getName()Ljava/lang/String;
(( java.lang.StringBuilder ) v0 ).append ( v4 ); // invoke-virtual {v0, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
final String v5 = ", "; // const-string v5, ", "
(( java.lang.StringBuilder ) v4 ).append ( v5 ); // invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
/* .line 259 */
} // .end local v3 # "clazz":Ljava/lang/Class;, "Ljava/lang/Class<*>;"
/* add-int/lit8 v2, v2, 0x1 */
/* .line 262 */
} // :cond_0
final String v1 = "but got: "; // const-string v1, "but got: "
(( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.Object ) p0 ).getClass ( ); // invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;
(( java.lang.Class ) v2 ).getName ( ); // invoke-virtual {v2}, Ljava/lang/Class;->getName()Ljava/lang/String;
(( java.lang.StringBuilder ) v1 ).append ( v2 ); // invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
/* .line 263 */
final String v2 = ", for type token: "; // const-string v2, ", for type token: "
(( java.lang.StringBuilder ) v1 ).append ( v2 ); // invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.Object ) p0 ).toString ( ); // invoke-virtual {p0}, Ljava/lang/Object;->toString()Ljava/lang/String;
(( java.lang.StringBuilder ) v1 ).append ( v2 ); // invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
/* const/16 v2, 0x2e */
(( java.lang.StringBuilder ) v1 ).append ( v2 ); // invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;
/* .line 265 */
/* new-instance v1, Ljava/lang/AssertionError; */
(( java.lang.StringBuilder ) v0 ).toString ( ); // invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
/* invoke-direct {v1, v2}, Ljava/lang/AssertionError;-><init>(Ljava/lang/Object;)V */
} // .end method
public static com.google.gson.reflect.TypeToken get ( java.lang.Class p0 ) {
/* .locals 1 */
/* .annotation system Ldalvik/annotation/Signature; */
/* value = { */
/* "<T:", */
/* "Ljava/lang/Object;", */
/* ">(", */
/* "Ljava/lang/Class<", */
/* "TT;>;)", */
/* "Lcom/google/gson/reflect/TypeToken<", */
/* "TT;>;" */
/* } */
} // .end annotation
/* .line 303 */
/* .local p0, "type":Ljava/lang/Class;, "Ljava/lang/Class<TT;>;" */
/* new-instance v0, Lcom/google/gson/reflect/TypeToken; */
/* invoke-direct {v0, p0}, Lcom/google/gson/reflect/TypeToken;-><init>(Ljava/lang/reflect/Type;)V */
} // .end method
public static com.google.gson.reflect.TypeToken get ( java.lang.reflect.Type p0 ) {
/* .locals 1 */
/* .param p0, "type" # Ljava/lang/reflect/Type; */
/* .annotation system Ldalvik/annotation/Signature; */
/* value = { */
/* "(", */
/* "Ljava/lang/reflect/Type;", */
/* ")", */
/* "Lcom/google/gson/reflect/TypeToken<", */
/* "*>;" */
/* } */
} // .end annotation
/* .line 296 */
/* new-instance v0, Lcom/google/gson/reflect/TypeToken; */
/* invoke-direct {v0, p0}, Lcom/google/gson/reflect/TypeToken;-><init>(Ljava/lang/reflect/Type;)V */
} // .end method
public static com.google.gson.reflect.TypeToken getArray ( java.lang.reflect.Type p0 ) {
/* .locals 2 */
/* .param p0, "componentType" # Ljava/lang/reflect/Type; */
/* .annotation system Ldalvik/annotation/Signature; */
/* value = { */
/* "(", */
/* "Ljava/lang/reflect/Type;", */
/* ")", */
/* "Lcom/google/gson/reflect/TypeToken<", */
/* "*>;" */
/* } */
} // .end annotation
/* .line 318 */
/* new-instance v0, Lcom/google/gson/reflect/TypeToken; */
com.google.gson.internal.$Gson$Types .arrayOf ( p0 );
/* invoke-direct {v0, v1}, Lcom/google/gson/reflect/TypeToken;-><init>(Ljava/lang/reflect/Type;)V */
} // .end method
public static com.google.gson.reflect.TypeToken getParameterized ( java.lang.reflect.Type p0, java.lang.reflect.Type...p1 ) {
/* .locals 2 */
/* .param p0, "rawType" # Ljava/lang/reflect/Type; */
/* .param p1, "typeArguments" # [Ljava/lang/reflect/Type; */
/* .annotation system Ldalvik/annotation/Signature; */
/* value = { */
/* "(", */
/* "Ljava/lang/reflect/Type;", */
/* "[", */
/* "Ljava/lang/reflect/Type;", */
/* ")", */
/* "Lcom/google/gson/reflect/TypeToken<", */
/* "*>;" */
/* } */
} // .end annotation
/* .line 311 */
/* new-instance v0, Lcom/google/gson/reflect/TypeToken; */
int v1 = 0; // const/4 v1, 0x0
com.google.gson.internal.$Gson$Types .newParameterizedTypeWithOwner ( v1,p0,p1 );
/* invoke-direct {v0, v1}, Lcom/google/gson/reflect/TypeToken;-><init>(Ljava/lang/reflect/Type;)V */
} // .end method
static java.lang.reflect.Type getSuperclassTypeParameter ( java.lang.Class p0 ) {
/* .locals 4 */
/* .annotation system Ldalvik/annotation/Signature; */
/* value = { */
/* "(", */
/* "Ljava/lang/Class<", */
/* "*>;)", */
/* "Ljava/lang/reflect/Type;" */
/* } */
} // .end annotation
/* .line 82 */
/* .local p0, "subclass":Ljava/lang/Class;, "Ljava/lang/Class<*>;" */
(( java.lang.Class ) p0 ).getGenericSuperclass ( ); // invoke-virtual {p0}, Ljava/lang/Class;->getGenericSuperclass()Ljava/lang/reflect/Type;
/* .line 83 */
/* .local v0, "superclass":Ljava/lang/reflect/Type; */
/* instance-of v1, v0, Ljava/lang/Class; */
/* if-nez v1, :cond_0 */
/* .line 86 */
/* move-object v1, v0 */
/* check-cast v1, Ljava/lang/reflect/ParameterizedType; */
/* .line 87 */
/* .local v1, "parameterized":Ljava/lang/reflect/ParameterizedType; */
int v3 = 0; // const/4 v3, 0x0
/* aget-object v2, v2, v3 */
com.google.gson.internal.$Gson$Types .canonicalize ( v2 );
/* .line 84 */
} // .end local v1 # "parameterized":Ljava/lang/reflect/ParameterizedType;
} // :cond_0
/* new-instance v1, Ljava/lang/RuntimeException; */
final String v2 = "Missing type parameter."; // const-string v2, "Missing type parameter."
/* invoke-direct {v1, v2}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V */
/* throw v1 */
} // .end method
private static Boolean isAssignableFrom ( java.lang.reflect.Type p0, java.lang.reflect.GenericArrayType p1 ) {
/* .locals 4 */
/* .param p0, "from" # Ljava/lang/reflect/Type; */
/* .param p1, "to" # Ljava/lang/reflect/GenericArrayType; */
/* .line 161 */
/* .line 162 */
/* .local v0, "toGenericComponentType":Ljava/lang/reflect/Type; */
/* instance-of v1, v0, Ljava/lang/reflect/ParameterizedType; */
if ( v1 != null) { // if-eqz v1, :cond_3
/* .line 163 */
/* move-object v1, p0 */
/* .line 164 */
/* .local v1, "t":Ljava/lang/reflect/Type; */
/* instance-of v2, p0, Ljava/lang/reflect/GenericArrayType; */
if ( v2 != null) { // if-eqz v2, :cond_0
/* .line 165 */
/* move-object v2, p0 */
/* check-cast v2, Ljava/lang/reflect/GenericArrayType; */
/* .line 166 */
} // :cond_0
/* instance-of v2, p0, Ljava/lang/Class; */
if ( v2 != null) { // if-eqz v2, :cond_2
/* .line 167 */
/* move-object v2, p0 */
/* check-cast v2, Ljava/lang/Class; */
/* .line 168 */
/* .local v2, "classType":Ljava/lang/Class;, "Ljava/lang/Class<*>;" */
} // :goto_0
v3 = (( java.lang.Class ) v2 ).isArray ( ); // invoke-virtual {v2}, Ljava/lang/Class;->isArray()Z
if ( v3 != null) { // if-eqz v3, :cond_1
/* .line 169 */
(( java.lang.Class ) v2 ).getComponentType ( ); // invoke-virtual {v2}, Ljava/lang/Class;->getComponentType()Ljava/lang/Class;
/* .line 171 */
} // :cond_1
/* move-object v1, v2 */
/* .line 173 */
} // .end local v2 # "classType":Ljava/lang/Class;, "Ljava/lang/Class<*>;"
} // :cond_2
} // :goto_1
/* move-object v2, v0 */
/* check-cast v2, Ljava/lang/reflect/ParameterizedType; */
/* new-instance v3, Ljava/util/HashMap; */
/* invoke-direct {v3}, Ljava/util/HashMap;-><init>()V */
v2 = com.google.gson.reflect.TypeToken .isAssignableFrom ( v1,v2,v3 );
/* .line 178 */
} // .end local v1 # "t":Ljava/lang/reflect/Type;
} // :cond_3
int v1 = 1; // const/4 v1, 0x1
} // .end method
private static Boolean isAssignableFrom ( java.lang.reflect.Type p0, java.lang.reflect.ParameterizedType p1, java.util.Map p2 ) {
/* .locals 11 */
/* .param p0, "from" # Ljava/lang/reflect/Type; */
/* .param p1, "to" # Ljava/lang/reflect/ParameterizedType; */
/* .annotation system Ldalvik/annotation/Signature; */
/* value = { */
/* "(", */
/* "Ljava/lang/reflect/Type;", */
/* "Ljava/lang/reflect/ParameterizedType;", */
/* "Ljava/util/Map<", */
/* "Ljava/lang/String;", */
/* "Ljava/lang/reflect/Type;", */
/* ">;)Z" */
/* } */
} // .end annotation
/* .line 188 */
/* .local p2, "typeVarMap":Ljava/util/Map;, "Ljava/util/Map<Ljava/lang/String;Ljava/lang/reflect/Type;>;" */
int v0 = 0; // const/4 v0, 0x0
/* if-nez p0, :cond_0 */
/* .line 189 */
/* .line 192 */
} // :cond_0
v1 = (( java.lang.Object ) p1 ).equals ( p0 ); // invoke-virtual {p1, p0}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z
int v2 = 1; // const/4 v2, 0x1
if ( v1 != null) { // if-eqz v1, :cond_1
/* .line 193 */
/* .line 197 */
} // :cond_1
com.google.gson.internal.$Gson$Types .getRawType ( p0 );
/* .line 198 */
/* .local v1, "clazz":Ljava/lang/Class;, "Ljava/lang/Class<*>;" */
int v3 = 0; // const/4 v3, 0x0
/* .line 199 */
/* .local v3, "ptype":Ljava/lang/reflect/ParameterizedType; */
/* instance-of v4, p0, Ljava/lang/reflect/ParameterizedType; */
if ( v4 != null) { // if-eqz v4, :cond_2
/* .line 200 */
/* move-object v3, p0 */
/* check-cast v3, Ljava/lang/reflect/ParameterizedType; */
/* .line 204 */
} // :cond_2
if ( v3 != null) { // if-eqz v3, :cond_5
/* .line 205 */
/* .line 206 */
/* .local v4, "tArgs":[Ljava/lang/reflect/Type; */
(( java.lang.Class ) v1 ).getTypeParameters ( ); // invoke-virtual {v1}, Ljava/lang/Class;->getTypeParameters()[Ljava/lang/reflect/TypeVariable;
/* .line 207 */
/* .local v5, "tParams":[Ljava/lang/reflect/TypeVariable;, "[Ljava/lang/reflect/TypeVariable<*>;" */
int v6 = 0; // const/4 v6, 0x0
/* .local v6, "i":I */
} // :goto_0
/* array-length v7, v4 */
/* if-ge v6, v7, :cond_4 */
/* .line 208 */
/* aget-object v7, v4, v6 */
/* .line 209 */
/* .local v7, "arg":Ljava/lang/reflect/Type; */
/* aget-object v8, v5, v6 */
/* .line 210 */
/* .local v8, "var":Ljava/lang/reflect/TypeVariable;, "Ljava/lang/reflect/TypeVariable<*>;" */
} // :goto_1
/* instance-of v9, v7, Ljava/lang/reflect/TypeVariable; */
if ( v9 != null) { // if-eqz v9, :cond_3
/* .line 211 */
/* move-object v9, v7 */
/* check-cast v9, Ljava/lang/reflect/TypeVariable; */
/* .line 212 */
/* .local v9, "v":Ljava/lang/reflect/TypeVariable;, "Ljava/lang/reflect/TypeVariable<*>;" */
/* move-object v7, v10 */
/* check-cast v7, Ljava/lang/reflect/Type; */
/* .line 213 */
} // .end local v9 # "v":Ljava/lang/reflect/TypeVariable;, "Ljava/lang/reflect/TypeVariable<*>;"
/* .line 214 */
} // :cond_3
/* .line 207 */
} // .end local v7 # "arg":Ljava/lang/reflect/Type;
} // .end local v8 # "var":Ljava/lang/reflect/TypeVariable;, "Ljava/lang/reflect/TypeVariable<*>;"
/* add-int/lit8 v6, v6, 0x1 */
/* .line 218 */
} // .end local v6 # "i":I
} // :cond_4
v6 = com.google.gson.reflect.TypeToken .typeEquals ( v3,p1,p2 );
if ( v6 != null) { // if-eqz v6, :cond_5
/* .line 219 */
/* .line 223 */
} // .end local v4 # "tArgs":[Ljava/lang/reflect/Type;
} // .end local v5 # "tParams":[Ljava/lang/reflect/TypeVariable;, "[Ljava/lang/reflect/TypeVariable<*>;"
} // :cond_5
(( java.lang.Class ) v1 ).getGenericInterfaces ( ); // invoke-virtual {v1}, Ljava/lang/Class;->getGenericInterfaces()[Ljava/lang/reflect/Type;
/* array-length v5, v4 */
} // :goto_2
/* if-ge v0, v5, :cond_7 */
/* aget-object v6, v4, v0 */
/* .line 224 */
/* .local v6, "itype":Ljava/lang/reflect/Type; */
/* new-instance v7, Ljava/util/HashMap; */
/* invoke-direct {v7, p2}, Ljava/util/HashMap;-><init>(Ljava/util/Map;)V */
v7 = com.google.gson.reflect.TypeToken .isAssignableFrom ( v6,p1,v7 );
if ( v7 != null) { // if-eqz v7, :cond_6
/* .line 225 */
/* .line 223 */
} // .end local v6 # "itype":Ljava/lang/reflect/Type;
} // :cond_6
/* add-int/lit8 v0, v0, 0x1 */
/* .line 230 */
} // :cond_7
(( java.lang.Class ) v1 ).getGenericSuperclass ( ); // invoke-virtual {v1}, Ljava/lang/Class;->getGenericSuperclass()Ljava/lang/reflect/Type;
/* .line 231 */
/* .local v0, "sType":Ljava/lang/reflect/Type; */
/* new-instance v2, Ljava/util/HashMap; */
/* invoke-direct {v2, p2}, Ljava/util/HashMap;-><init>(Ljava/util/Map;)V */
v2 = com.google.gson.reflect.TypeToken .isAssignableFrom ( v0,p1,v2 );
} // .end method
private static Boolean matches ( java.lang.reflect.Type p0, java.lang.reflect.Type p1, java.util.Map p2 ) {
/* .locals 1 */
/* .param p0, "from" # Ljava/lang/reflect/Type; */
/* .param p1, "to" # Ljava/lang/reflect/Type; */
/* .annotation system Ldalvik/annotation/Signature; */
/* value = { */
/* "(", */
/* "Ljava/lang/reflect/Type;", */
/* "Ljava/lang/reflect/Type;", */
/* "Ljava/util/Map<", */
/* "Ljava/lang/String;", */
/* "Ljava/lang/reflect/Type;", */
/* ">;)Z" */
/* } */
} // .end annotation
/* .line 273 */
/* .local p2, "typeMap":Ljava/util/Map;, "Ljava/util/Map<Ljava/lang/String;Ljava/lang/reflect/Type;>;" */
v0 = (( java.lang.Object ) p1 ).equals ( p0 ); // invoke-virtual {p1, p0}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z
/* if-nez v0, :cond_1 */
/* instance-of v0, p0, Ljava/lang/reflect/TypeVariable; */
if ( v0 != null) { // if-eqz v0, :cond_0
/* move-object v0, p0 */
/* check-cast v0, Ljava/lang/reflect/TypeVariable; */
/* .line 275 */
v0 = (( java.lang.Object ) p1 ).equals ( v0 ); // invoke-virtual {p1, v0}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z
if ( v0 != null) { // if-eqz v0, :cond_0
} // :cond_0
int v0 = 0; // const/4 v0, 0x0
} // :cond_1
} // :goto_0
int v0 = 1; // const/4 v0, 0x1
/* .line 273 */
} // :goto_1
} // .end method
private static Boolean typeEquals ( java.lang.reflect.ParameterizedType p0, java.lang.reflect.ParameterizedType p1, java.util.Map p2 ) {
/* .locals 6 */
/* .param p0, "from" # Ljava/lang/reflect/ParameterizedType; */
/* .param p1, "to" # Ljava/lang/reflect/ParameterizedType; */
/* .annotation system Ldalvik/annotation/Signature; */
/* value = { */
/* "(", */
/* "Ljava/lang/reflect/ParameterizedType;", */
/* "Ljava/lang/reflect/ParameterizedType;", */
/* "Ljava/util/Map<", */
/* "Ljava/lang/String;", */
/* "Ljava/lang/reflect/Type;", */
/* ">;)Z" */
/* } */
} // .end annotation
/* .line 240 */
/* .local p2, "typeVarMap":Ljava/util/Map;, "Ljava/util/Map<Ljava/lang/String;Ljava/lang/reflect/Type;>;" */
v0 = (( java.lang.Object ) v0 ).equals ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z
int v1 = 0; // const/4 v1, 0x0
if ( v0 != null) { // if-eqz v0, :cond_2
/* .line 241 */
/* .line 242 */
/* .local v0, "fromArgs":[Ljava/lang/reflect/Type; */
/* .line 243 */
/* .local v2, "toArgs":[Ljava/lang/reflect/Type; */
int v3 = 0; // const/4 v3, 0x0
/* .local v3, "i":I */
} // :goto_0
/* array-length v4, v0 */
/* if-ge v3, v4, :cond_1 */
/* .line 244 */
/* aget-object v4, v0, v3 */
/* aget-object v5, v2, v3 */
v4 = com.google.gson.reflect.TypeToken .matches ( v4,v5,p2 );
/* if-nez v4, :cond_0 */
/* .line 245 */
/* .line 243 */
} // :cond_0
/* add-int/lit8 v3, v3, 0x1 */
/* .line 248 */
} // .end local v3 # "i":I
} // :cond_1
int v1 = 1; // const/4 v1, 0x1
/* .line 250 */
} // .end local v0 # "fromArgs":[Ljava/lang/reflect/Type;
} // .end local v2 # "toArgs":[Ljava/lang/reflect/Type;
} // :cond_2
} // .end method
/* # virtual methods */
public final Boolean equals ( java.lang.Object p0 ) {
/* .locals 2 */
/* .param p1, "o" # Ljava/lang/Object; */
/* .line 284 */
/* .local p0, "this":Lcom/google/gson/reflect/TypeToken;, "Lcom/google/gson/reflect/TypeToken<TT;>;" */
/* instance-of v0, p1, Lcom/google/gson/reflect/TypeToken; */
if ( v0 != null) { // if-eqz v0, :cond_0
v0 = this.type;
/* move-object v1, p1 */
/* check-cast v1, Lcom/google/gson/reflect/TypeToken; */
v1 = this.type;
/* .line 285 */
v0 = com.google.gson.internal.$Gson$Types .equals ( v0,v1 );
if ( v0 != null) { // if-eqz v0, :cond_0
int v0 = 1; // const/4 v0, 0x1
} // :cond_0
int v0 = 0; // const/4 v0, 0x0
/* .line 284 */
} // :goto_0
} // .end method
public final java.lang.Class getRawType ( ) {
/* .locals 1 */
/* .annotation system Ldalvik/annotation/Signature; */
/* value = { */
/* "()", */
/* "Ljava/lang/Class<", */
/* "-TT;>;" */
/* } */
} // .end annotation
/* .line 94 */
/* .local p0, "this":Lcom/google/gson/reflect/TypeToken;, "Lcom/google/gson/reflect/TypeToken<TT;>;" */
v0 = this.rawType;
} // .end method
public final java.lang.reflect.Type getType ( ) {
/* .locals 1 */
/* .line 101 */
/* .local p0, "this":Lcom/google/gson/reflect/TypeToken;, "Lcom/google/gson/reflect/TypeToken<TT;>;" */
v0 = this.type;
} // .end method
public final Integer hashCode ( ) {
/* .locals 1 */
/* .line 280 */
/* .local p0, "this":Lcom/google/gson/reflect/TypeToken;, "Lcom/google/gson/reflect/TypeToken<TT;>;" */
/* iget v0, p0, Lcom/google/gson/reflect/TypeToken;->hashCode:I */
} // .end method
public Boolean isAssignableFrom ( com.google.gson.reflect.TypeToken p0 ) {
/* .locals 1 */
/* .annotation system Ldalvik/annotation/Signature; */
/* value = { */
/* "(", */
/* "Lcom/google/gson/reflect/TypeToken<", */
/* "*>;)Z" */
/* } */
} // .end annotation
/* .annotation runtime Ljava/lang/Deprecated; */
} // .end annotation
/* .line 153 */
/* .local p0, "this":Lcom/google/gson/reflect/TypeToken;, "Lcom/google/gson/reflect/TypeToken<TT;>;" */
/* .local p1, "token":Lcom/google/gson/reflect/TypeToken;, "Lcom/google/gson/reflect/TypeToken<*>;" */
(( com.google.gson.reflect.TypeToken ) p1 ).getType ( ); // invoke-virtual {p1}, Lcom/google/gson/reflect/TypeToken;->getType()Ljava/lang/reflect/Type;
v0 = (( com.google.gson.reflect.TypeToken ) p0 ).isAssignableFrom ( v0 ); // invoke-virtual {p0, v0}, Lcom/google/gson/reflect/TypeToken;->isAssignableFrom(Ljava/lang/reflect/Type;)Z
} // .end method
public Boolean isAssignableFrom ( java.lang.Class p0 ) {
/* .locals 1 */
/* .annotation system Ldalvik/annotation/Signature; */
/* value = { */
/* "(", */
/* "Ljava/lang/Class<", */
/* "*>;)Z" */
/* } */
} // .end annotation
/* .annotation runtime Ljava/lang/Deprecated; */
} // .end annotation
/* .line 112 */
/* .local p0, "this":Lcom/google/gson/reflect/TypeToken;, "Lcom/google/gson/reflect/TypeToken<TT;>;" */
/* .local p1, "cls":Ljava/lang/Class;, "Ljava/lang/Class<*>;" */
v0 = (( com.google.gson.reflect.TypeToken ) p0 ).isAssignableFrom ( p1 ); // invoke-virtual {p0, p1}, Lcom/google/gson/reflect/TypeToken;->isAssignableFrom(Ljava/lang/reflect/Type;)Z
} // .end method
public Boolean isAssignableFrom ( java.lang.reflect.Type p0 ) {
/* .locals 4 */
/* .param p1, "from" # Ljava/lang/reflect/Type; */
/* .annotation runtime Ljava/lang/Deprecated; */
} // .end annotation
/* .line 123 */
/* .local p0, "this":Lcom/google/gson/reflect/TypeToken;, "Lcom/google/gson/reflect/TypeToken<TT;>;" */
int v0 = 0; // const/4 v0, 0x0
/* if-nez p1, :cond_0 */
/* .line 124 */
/* .line 127 */
} // :cond_0
v1 = this.type;
v1 = (( java.lang.Object ) v1 ).equals ( p1 ); // invoke-virtual {v1, p1}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z
int v2 = 1; // const/4 v2, 0x1
if ( v1 != null) { // if-eqz v1, :cond_1
/* .line 128 */
/* .line 131 */
} // :cond_1
v1 = this.type;
/* instance-of v3, v1, Ljava/lang/Class; */
if ( v3 != null) { // if-eqz v3, :cond_2
/* .line 132 */
v0 = this.rawType;
com.google.gson.internal.$Gson$Types .getRawType ( p1 );
v0 = (( java.lang.Class ) v0 ).isAssignableFrom ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/Class;->isAssignableFrom(Ljava/lang/Class;)Z
/* .line 133 */
} // :cond_2
/* instance-of v3, v1, Ljava/lang/reflect/ParameterizedType; */
if ( v3 != null) { // if-eqz v3, :cond_3
/* .line 134 */
/* check-cast v1, Ljava/lang/reflect/ParameterizedType; */
/* new-instance v0, Ljava/util/HashMap; */
/* invoke-direct {v0}, Ljava/util/HashMap;-><init>()V */
v0 = com.google.gson.reflect.TypeToken .isAssignableFrom ( p1,v1,v0 );
/* .line 136 */
} // :cond_3
/* instance-of v3, v1, Ljava/lang/reflect/GenericArrayType; */
if ( v3 != null) { // if-eqz v3, :cond_5
/* .line 137 */
v1 = this.rawType;
com.google.gson.internal.$Gson$Types .getRawType ( p1 );
v1 = (( java.lang.Class ) v1 ).isAssignableFrom ( v3 ); // invoke-virtual {v1, v3}, Ljava/lang/Class;->isAssignableFrom(Ljava/lang/Class;)Z
if ( v1 != null) { // if-eqz v1, :cond_4
v1 = this.type;
/* check-cast v1, Ljava/lang/reflect/GenericArrayType; */
/* .line 138 */
v1 = com.google.gson.reflect.TypeToken .isAssignableFrom ( p1,v1 );
if ( v1 != null) { // if-eqz v1, :cond_4
/* move v0, v2 */
} // :cond_4
/* nop */
/* .line 137 */
} // :goto_0
/* .line 140 */
} // :cond_5
/* const-class v0, Ljava/lang/Class; */
/* const-class v2, Ljava/lang/reflect/ParameterizedType; */
/* const-class v3, Ljava/lang/reflect/GenericArrayType; */
/* filled-new-array {v0, v2, v3}, [Ljava/lang/Class; */
com.google.gson.reflect.TypeToken .buildUnexpectedTypeError ( v1,v0 );
/* throw v0 */
} // .end method
public final java.lang.String toString ( ) {
/* .locals 1 */
/* .line 289 */
/* .local p0, "this":Lcom/google/gson/reflect/TypeToken;, "Lcom/google/gson/reflect/TypeToken<TT;>;" */
v0 = this.type;
com.google.gson.internal.$Gson$Types .typeToString ( v0 );
} // .end method
