public class com.google.gson.stream.JsonWriter implements java.io.Closeable implements java.io.Flushable {
	 /* .source "JsonWriter.java" */
	 /* # interfaces */
	 /* # static fields */
	 private static final java.lang.String HTML_SAFE_REPLACEMENT_CHARS;
	 private static final java.lang.String REPLACEMENT_CHARS;
	 private static final java.util.regex.Pattern VALID_JSON_NUMBER_PATTERN;
	 /* # instance fields */
	 private java.lang.String deferredName;
	 private Boolean htmlSafe;
	 private java.lang.String indent;
	 private Boolean lenient;
	 private final java.io.Writer out;
	 private java.lang.String separator;
	 private Boolean serializeNulls;
	 private stack;
	 private Integer stackSize;
	 /* # direct methods */
	 static com.google.gson.stream.JsonWriter ( ) {
		 /* .locals 4 */
		 /* .line 139 */
		 final String v0 = "-?(?:0|[1-9][0-9]*)(?:\\.[0-9]+)?(?:[eE][-+]?[0-9]+)?"; // const-string v0, "-?(?:0|[1-9][0-9]*)(?:\\.[0-9]+)?(?:[eE][-+]?[0-9]+)?"
		 java.util.regex.Pattern .compile ( v0 );
		 /* .line 154 */
		 /* const/16 v0, 0x80 */
		 /* new-array v0, v0, [Ljava/lang/String; */
		 /* .line 155 */
		 int v0 = 0; // const/4 v0, 0x0
		 /* .local v0, "i":I */
	 } // :goto_0
	 /* const/16 v1, 0x1f */
	 /* if-gt v0, v1, :cond_0 */
	 /* .line 156 */
	 v1 = com.google.gson.stream.JsonWriter.REPLACEMENT_CHARS;
	 java.lang.Integer .valueOf ( v0 );
	 /* filled-new-array {v2}, [Ljava/lang/Object; */
	 final String v3 = "\\u%04x"; // const-string v3, "\\u%04x"
	 java.lang.String .format ( v3,v2 );
	 /* aput-object v2, v1, v0 */
	 /* .line 155 */
	 /* add-int/lit8 v0, v0, 0x1 */
	 /* .line 158 */
} // .end local v0 # "i":I
} // :cond_0
v0 = com.google.gson.stream.JsonWriter.REPLACEMENT_CHARS;
/* const/16 v1, 0x22 */
final String v2 = "\\\""; // const-string v2, "\\\""
/* aput-object v2, v0, v1 */
/* .line 159 */
/* const/16 v1, 0x5c */
final String v2 = "\\\\"; // const-string v2, "\\\\"
/* aput-object v2, v0, v1 */
/* .line 160 */
/* const/16 v1, 0x9 */
final String v2 = "\\t"; // const-string v2, "\\t"
/* aput-object v2, v0, v1 */
/* .line 161 */
/* const/16 v1, 0x8 */
final String v2 = "\\b"; // const-string v2, "\\b"
/* aput-object v2, v0, v1 */
/* .line 162 */
/* const/16 v1, 0xa */
final String v2 = "\\n"; // const-string v2, "\\n"
/* aput-object v2, v0, v1 */
/* .line 163 */
/* const/16 v1, 0xd */
final String v2 = "\\r"; // const-string v2, "\\r"
/* aput-object v2, v0, v1 */
/* .line 164 */
/* const/16 v1, 0xc */
final String v2 = "\\f"; // const-string v2, "\\f"
/* aput-object v2, v0, v1 */
/* .line 165 */
(( java.lang.String ) v0 ).clone ( ); // invoke-virtual {v0}, [Ljava/lang/String;->clone()Ljava/lang/Object;
/* check-cast v0, [Ljava/lang/String; */
/* .line 166 */
/* const/16 v1, 0x3c */
final String v2 = "\\u003c"; // const-string v2, "\\u003c"
/* aput-object v2, v0, v1 */
/* .line 167 */
/* const/16 v1, 0x3e */
final String v2 = "\\u003e"; // const-string v2, "\\u003e"
/* aput-object v2, v0, v1 */
/* .line 168 */
/* const/16 v1, 0x26 */
final String v2 = "\\u0026"; // const-string v2, "\\u0026"
/* aput-object v2, v0, v1 */
/* .line 169 */
/* const/16 v1, 0x3d */
final String v2 = "\\u003d"; // const-string v2, "\\u003d"
/* aput-object v2, v0, v1 */
/* .line 170 */
/* const/16 v1, 0x27 */
final String v2 = "\\u0027"; // const-string v2, "\\u0027"
/* aput-object v2, v0, v1 */
/* .line 171 */
return;
} // .end method
public com.google.gson.stream.JsonWriter ( ) {
/* .locals 2 */
/* .param p1, "out" # Ljava/io/Writer; */
/* .line 206 */
/* invoke-direct {p0}, Ljava/lang/Object;-><init>()V */
/* .line 176 */
/* const/16 v0, 0x20 */
/* new-array v0, v0, [I */
this.stack = v0;
/* .line 177 */
int v0 = 0; // const/4 v0, 0x0
/* iput v0, p0, Lcom/google/gson/stream/JsonWriter;->stackSize:I */
/* .line 179 */
int v0 = 6; // const/4 v0, 0x6
/* invoke-direct {p0, v0}, Lcom/google/gson/stream/JsonWriter;->push(I)V */
/* .line 191 */
final String v0 = ":"; // const-string v0, ":"
this.separator = v0;
/* .line 199 */
int v0 = 1; // const/4 v0, 0x1
/* iput-boolean v0, p0, Lcom/google/gson/stream/JsonWriter;->serializeNulls:Z */
/* .line 207 */
if ( p1 != null) { // if-eqz p1, :cond_0
/* .line 210 */
this.out = p1;
/* .line 211 */
return;
/* .line 208 */
} // :cond_0
/* new-instance v0, Ljava/lang/NullPointerException; */
final String v1 = "out == null"; // const-string v1, "out == null"
/* invoke-direct {v0, v1}, Ljava/lang/NullPointerException;-><init>(Ljava/lang/String;)V */
/* throw v0 */
} // .end method
private void beforeName ( ) {
/* .locals 3 */
/* .annotation system Ldalvik/annotation/Throws; */
/* value = { */
/* Ljava/io/IOException; */
/* } */
} // .end annotation
/* .line 644 */
v0 = /* invoke-direct {p0}, Lcom/google/gson/stream/JsonWriter;->peek()I */
/* .line 645 */
/* .local v0, "context":I */
int v1 = 5; // const/4 v1, 0x5
/* if-ne v0, v1, :cond_0 */
/* .line 646 */
v1 = this.out;
/* const/16 v2, 0x2c */
(( java.io.Writer ) v1 ).write ( v2 ); // invoke-virtual {v1, v2}, Ljava/io/Writer;->write(I)V
/* .line 647 */
} // :cond_0
int v1 = 3; // const/4 v1, 0x3
/* if-ne v0, v1, :cond_1 */
/* .line 650 */
} // :goto_0
/* invoke-direct {p0}, Lcom/google/gson/stream/JsonWriter;->newline()V */
/* .line 651 */
int v1 = 4; // const/4 v1, 0x4
/* invoke-direct {p0, v1}, Lcom/google/gson/stream/JsonWriter;->replaceTop(I)V */
/* .line 652 */
return;
/* .line 648 */
} // :cond_1
/* new-instance v1, Ljava/lang/IllegalStateException; */
final String v2 = "Nesting problem."; // const-string v2, "Nesting problem."
/* invoke-direct {v1, v2}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V */
/* throw v1 */
} // .end method
private void beforeValue ( ) {
/* .locals 2 */
/* .annotation system Ldalvik/annotation/Throws; */
/* value = { */
/* Ljava/io/IOException; */
/* } */
} // .end annotation
/* .line 661 */
v0 = /* invoke-direct {p0}, Lcom/google/gson/stream/JsonWriter;->peek()I */
/* packed-switch v0, :pswitch_data_0 */
/* .line 688 */
/* :pswitch_0 */
/* new-instance v0, Ljava/lang/IllegalStateException; */
final String v1 = "Nesting problem."; // const-string v1, "Nesting problem."
/* invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V */
/* throw v0 */
/* .line 663 */
/* :pswitch_1 */
/* iget-boolean v0, p0, Lcom/google/gson/stream/JsonWriter;->lenient:Z */
if ( v0 != null) { // if-eqz v0, :cond_0
/* .line 664 */
} // :cond_0
/* new-instance v0, Ljava/lang/IllegalStateException; */
final String v1 = "JSON must have only one top-level value."; // const-string v1, "JSON must have only one top-level value."
/* invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V */
/* throw v0 */
/* .line 669 */
} // :goto_0
/* :pswitch_2 */
int v0 = 7; // const/4 v0, 0x7
/* invoke-direct {p0, v0}, Lcom/google/gson/stream/JsonWriter;->replaceTop(I)V */
/* .line 670 */
/* .line 683 */
/* :pswitch_3 */
v0 = this.out;
v1 = this.separator;
(( java.io.Writer ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/io/Writer;->append(Ljava/lang/CharSequence;)Ljava/io/Writer;
/* .line 684 */
int v0 = 5; // const/4 v0, 0x5
/* invoke-direct {p0, v0}, Lcom/google/gson/stream/JsonWriter;->replaceTop(I)V */
/* .line 685 */
/* .line 678 */
/* :pswitch_4 */
v0 = this.out;
/* const/16 v1, 0x2c */
(( java.io.Writer ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/io/Writer;->append(C)Ljava/io/Writer;
/* .line 679 */
/* invoke-direct {p0}, Lcom/google/gson/stream/JsonWriter;->newline()V */
/* .line 680 */
/* .line 673 */
/* :pswitch_5 */
int v0 = 2; // const/4 v0, 0x2
/* invoke-direct {p0, v0}, Lcom/google/gson/stream/JsonWriter;->replaceTop(I)V */
/* .line 674 */
/* invoke-direct {p0}, Lcom/google/gson/stream/JsonWriter;->newline()V */
/* .line 675 */
/* nop */
/* .line 690 */
} // :goto_1
return;
/* nop */
/* :pswitch_data_0 */
/* .packed-switch 0x1 */
/* :pswitch_5 */
/* :pswitch_4 */
/* :pswitch_0 */
/* :pswitch_3 */
/* :pswitch_0 */
/* :pswitch_2 */
/* :pswitch_1 */
} // .end packed-switch
} // .end method
private com.google.gson.stream.JsonWriter close ( Integer p0, Integer p1, Object p2 ) {
/* .locals 4 */
/* .param p1, "empty" # I */
/* .param p2, "nonempty" # I */
/* .param p3, "closeBracket" # C */
/* .annotation system Ldalvik/annotation/Throws; */
/* value = { */
/* Ljava/io/IOException; */
/* } */
} // .end annotation
/* .line 346 */
v0 = /* invoke-direct {p0}, Lcom/google/gson/stream/JsonWriter;->peek()I */
/* .line 347 */
/* .local v0, "context":I */
/* if-eq v0, p2, :cond_1 */
/* if-ne v0, p1, :cond_0 */
/* .line 348 */
} // :cond_0
/* new-instance v1, Ljava/lang/IllegalStateException; */
final String v2 = "Nesting problem."; // const-string v2, "Nesting problem."
/* invoke-direct {v1, v2}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V */
/* throw v1 */
/* .line 350 */
} // :cond_1
} // :goto_0
v1 = this.deferredName;
/* if-nez v1, :cond_3 */
/* .line 354 */
/* iget v1, p0, Lcom/google/gson/stream/JsonWriter;->stackSize:I */
/* add-int/lit8 v1, v1, -0x1 */
/* iput v1, p0, Lcom/google/gson/stream/JsonWriter;->stackSize:I */
/* .line 355 */
/* if-ne v0, p2, :cond_2 */
/* .line 356 */
/* invoke-direct {p0}, Lcom/google/gson/stream/JsonWriter;->newline()V */
/* .line 358 */
} // :cond_2
v1 = this.out;
(( java.io.Writer ) v1 ).write ( p3 ); // invoke-virtual {v1, p3}, Ljava/io/Writer;->write(I)V
/* .line 359 */
/* .line 351 */
} // :cond_3
/* new-instance v1, Ljava/lang/IllegalStateException; */
/* new-instance v2, Ljava/lang/StringBuilder; */
/* invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V */
final String v3 = "Dangling name: "; // const-string v3, "Dangling name: "
(( java.lang.StringBuilder ) v2 ).append ( v3 ); // invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
v3 = this.deferredName;
(( java.lang.StringBuilder ) v2 ).append ( v3 ); // invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v2 ).toString ( ); // invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
/* invoke-direct {v1, v2}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V */
/* throw v1 */
} // .end method
private static Boolean isTrustedNumberType ( java.lang.Class p0 ) {
/* .locals 1 */
/* .annotation system Ldalvik/annotation/Signature; */
/* value = { */
/* "(", */
/* "Ljava/lang/Class<", */
/* "+", */
/* "Ljava/lang/Number;", */
/* ">;)Z" */
/* } */
} // .end annotation
/* .line 531 */
/* .local p0, "c":Ljava/lang/Class;, "Ljava/lang/Class<+Ljava/lang/Number;>;" */
/* const-class v0, Ljava/lang/Integer; */
/* if-eq p0, v0, :cond_1 */
/* const-class v0, Ljava/lang/Long; */
/* if-eq p0, v0, :cond_1 */
/* const-class v0, Ljava/lang/Double; */
/* if-eq p0, v0, :cond_1 */
/* const-class v0, Ljava/lang/Float; */
/* if-eq p0, v0, :cond_1 */
/* const-class v0, Ljava/lang/Byte; */
/* if-eq p0, v0, :cond_1 */
/* const-class v0, Ljava/lang/Short; */
/* if-eq p0, v0, :cond_1 */
/* const-class v0, Ljava/math/BigDecimal; */
/* if-eq p0, v0, :cond_1 */
/* const-class v0, Ljava/math/BigInteger; */
/* if-eq p0, v0, :cond_1 */
/* const-class v0, Ljava/util/concurrent/atomic/AtomicInteger; */
/* if-eq p0, v0, :cond_1 */
/* const-class v0, Ljava/util/concurrent/atomic/AtomicLong; */
/* if-ne p0, v0, :cond_0 */
} // :cond_0
int v0 = 0; // const/4 v0, 0x0
} // :cond_1
} // :goto_0
int v0 = 1; // const/4 v0, 0x1
} // :goto_1
} // .end method
private void newline ( ) {
/* .locals 4 */
/* .annotation system Ldalvik/annotation/Throws; */
/* value = { */
/* Ljava/io/IOException; */
/* } */
} // .end annotation
/* .line 629 */
v0 = this.indent;
/* if-nez v0, :cond_0 */
/* .line 630 */
return;
/* .line 633 */
} // :cond_0
v0 = this.out;
/* const/16 v1, 0xa */
(( java.io.Writer ) v0 ).write ( v1 ); // invoke-virtual {v0, v1}, Ljava/io/Writer;->write(I)V
/* .line 634 */
int v0 = 1; // const/4 v0, 0x1
/* .local v0, "i":I */
/* iget v1, p0, Lcom/google/gson/stream/JsonWriter;->stackSize:I */
/* .local v1, "size":I */
} // :goto_0
/* if-ge v0, v1, :cond_1 */
/* .line 635 */
v2 = this.out;
v3 = this.indent;
(( java.io.Writer ) v2 ).write ( v3 ); // invoke-virtual {v2, v3}, Ljava/io/Writer;->write(Ljava/lang/String;)V
/* .line 634 */
/* add-int/lit8 v0, v0, 0x1 */
/* .line 637 */
} // .end local v0 # "i":I
} // .end local v1 # "size":I
} // :cond_1
return;
} // .end method
private com.google.gson.stream.JsonWriter open ( Integer p0, Object p1 ) {
/* .locals 1 */
/* .param p1, "empty" # I */
/* .param p2, "openBracket" # C */
/* .annotation system Ldalvik/annotation/Throws; */
/* value = { */
/* Ljava/io/IOException; */
/* } */
} // .end annotation
/* .line 334 */
/* invoke-direct {p0}, Lcom/google/gson/stream/JsonWriter;->beforeValue()V */
/* .line 335 */
/* invoke-direct {p0, p1}, Lcom/google/gson/stream/JsonWriter;->push(I)V */
/* .line 336 */
v0 = this.out;
(( java.io.Writer ) v0 ).write ( p2 ); // invoke-virtual {v0, p2}, Ljava/io/Writer;->write(I)V
/* .line 337 */
} // .end method
private Integer peek ( ) {
/* .locals 2 */
/* .line 373 */
/* iget v0, p0, Lcom/google/gson/stream/JsonWriter;->stackSize:I */
if ( v0 != null) { // if-eqz v0, :cond_0
/* .line 376 */
v1 = this.stack;
/* add-int/lit8 v0, v0, -0x1 */
/* aget v0, v1, v0 */
/* .line 374 */
} // :cond_0
/* new-instance v0, Ljava/lang/IllegalStateException; */
final String v1 = "JsonWriter is closed."; // const-string v1, "JsonWriter is closed."
/* invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V */
/* throw v0 */
} // .end method
private void push ( Integer p0 ) {
/* .locals 3 */
/* .param p1, "newTop" # I */
/* .line 363 */
/* iget v0, p0, Lcom/google/gson/stream/JsonWriter;->stackSize:I */
v1 = this.stack;
/* array-length v2, v1 */
/* if-ne v0, v2, :cond_0 */
/* .line 364 */
/* mul-int/lit8 v0, v0, 0x2 */
java.util.Arrays .copyOf ( v1,v0 );
this.stack = v0;
/* .line 366 */
} // :cond_0
v0 = this.stack;
/* iget v1, p0, Lcom/google/gson/stream/JsonWriter;->stackSize:I */
/* add-int/lit8 v2, v1, 0x1 */
/* iput v2, p0, Lcom/google/gson/stream/JsonWriter;->stackSize:I */
/* aput p1, v0, v1 */
/* .line 367 */
return;
} // .end method
private void replaceTop ( Integer p0 ) {
/* .locals 2 */
/* .param p1, "topOfStack" # I */
/* .line 383 */
v0 = this.stack;
/* iget v1, p0, Lcom/google/gson/stream/JsonWriter;->stackSize:I */
/* add-int/lit8 v1, v1, -0x1 */
/* aput p1, v0, v1 */
/* .line 384 */
return;
} // .end method
private void string ( java.lang.String p0 ) {
/* .locals 9 */
/* .param p1, "value" # Ljava/lang/String; */
/* .annotation system Ldalvik/annotation/Throws; */
/* value = { */
/* Ljava/io/IOException; */
/* } */
} // .end annotation
/* .line 597 */
/* iget-boolean v0, p0, Lcom/google/gson/stream/JsonWriter;->htmlSafe:Z */
if ( v0 != null) { // if-eqz v0, :cond_0
v0 = com.google.gson.stream.JsonWriter.HTML_SAFE_REPLACEMENT_CHARS;
} // :cond_0
v0 = com.google.gson.stream.JsonWriter.REPLACEMENT_CHARS;
/* .line 598 */
/* .local v0, "replacements":[Ljava/lang/String; */
} // :goto_0
v1 = this.out;
/* const/16 v2, 0x22 */
(( java.io.Writer ) v1 ).write ( v2 ); // invoke-virtual {v1, v2}, Ljava/io/Writer;->write(I)V
/* .line 599 */
int v1 = 0; // const/4 v1, 0x0
/* .line 600 */
/* .local v1, "last":I */
v3 = (( java.lang.String ) p1 ).length ( ); // invoke-virtual {p1}, Ljava/lang/String;->length()I
/* .line 601 */
/* .local v3, "length":I */
int v4 = 0; // const/4 v4, 0x0
/* .local v4, "i":I */
} // :goto_1
/* if-ge v4, v3, :cond_6 */
/* .line 602 */
v5 = (( java.lang.String ) p1 ).charAt ( v4 ); // invoke-virtual {p1, v4}, Ljava/lang/String;->charAt(I)C
/* .line 604 */
/* .local v5, "c":C */
/* const/16 v6, 0x80 */
/* if-ge v5, v6, :cond_1 */
/* .line 605 */
/* aget-object v6, v0, v5 */
/* .line 606 */
/* .local v6, "replacement":Ljava/lang/String; */
/* if-nez v6, :cond_3 */
/* .line 607 */
/* .line 609 */
} // .end local v6 # "replacement":Ljava/lang/String;
} // :cond_1
/* const/16 v6, 0x2028 */
/* if-ne v5, v6, :cond_2 */
/* .line 610 */
final String v6 = "\\u2028"; // const-string v6, "\\u2028"
/* .restart local v6 # "replacement":Ljava/lang/String; */
/* .line 611 */
} // .end local v6 # "replacement":Ljava/lang/String;
} // :cond_2
/* const/16 v6, 0x2029 */
/* if-ne v5, v6, :cond_5 */
/* .line 612 */
final String v6 = "\\u2029"; // const-string v6, "\\u2029"
/* .line 616 */
/* .restart local v6 # "replacement":Ljava/lang/String; */
} // :cond_3
} // :goto_2
/* if-ge v1, v4, :cond_4 */
/* .line 617 */
v7 = this.out;
/* sub-int v8, v4, v1 */
(( java.io.Writer ) v7 ).write ( p1, v1, v8 ); // invoke-virtual {v7, p1, v1, v8}, Ljava/io/Writer;->write(Ljava/lang/String;II)V
/* .line 619 */
} // :cond_4
v7 = this.out;
(( java.io.Writer ) v7 ).write ( v6 ); // invoke-virtual {v7, v6}, Ljava/io/Writer;->write(Ljava/lang/String;)V
/* .line 620 */
/* add-int/lit8 v1, v4, 0x1 */
/* .line 601 */
} // .end local v5 # "c":C
} // .end local v6 # "replacement":Ljava/lang/String;
} // :cond_5
} // :goto_3
/* add-int/lit8 v4, v4, 0x1 */
/* .line 622 */
} // .end local v4 # "i":I
} // :cond_6
/* if-ge v1, v3, :cond_7 */
/* .line 623 */
v4 = this.out;
/* sub-int v5, v3, v1 */
(( java.io.Writer ) v4 ).write ( p1, v1, v5 ); // invoke-virtual {v4, p1, v1, v5}, Ljava/io/Writer;->write(Ljava/lang/String;II)V
/* .line 625 */
} // :cond_7
v4 = this.out;
(( java.io.Writer ) v4 ).write ( v2 ); // invoke-virtual {v4, v2}, Ljava/io/Writer;->write(I)V
/* .line 626 */
return;
} // .end method
private void writeDeferredName ( ) {
/* .locals 1 */
/* .annotation system Ldalvik/annotation/Throws; */
/* value = { */
/* Ljava/io/IOException; */
/* } */
} // .end annotation
/* .line 407 */
v0 = this.deferredName;
if ( v0 != null) { // if-eqz v0, :cond_0
/* .line 408 */
/* invoke-direct {p0}, Lcom/google/gson/stream/JsonWriter;->beforeName()V */
/* .line 409 */
v0 = this.deferredName;
/* invoke-direct {p0, v0}, Lcom/google/gson/stream/JsonWriter;->string(Ljava/lang/String;)V */
/* .line 410 */
int v0 = 0; // const/4 v0, 0x0
this.deferredName = v0;
/* .line 412 */
} // :cond_0
return;
} // .end method
/* # virtual methods */
public com.google.gson.stream.JsonWriter beginArray ( ) {
/* .locals 2 */
/* .annotation system Ldalvik/annotation/Throws; */
/* value = { */
/* Ljava/io/IOException; */
/* } */
} // .end annotation
/* .line 296 */
/* invoke-direct {p0}, Lcom/google/gson/stream/JsonWriter;->writeDeferredName()V */
/* .line 297 */
int v0 = 1; // const/4 v0, 0x1
/* const/16 v1, 0x5b */
/* invoke-direct {p0, v0, v1}, Lcom/google/gson/stream/JsonWriter;->open(IC)Lcom/google/gson/stream/JsonWriter; */
} // .end method
public com.google.gson.stream.JsonWriter beginObject ( ) {
/* .locals 2 */
/* .annotation system Ldalvik/annotation/Throws; */
/* value = { */
/* Ljava/io/IOException; */
/* } */
} // .end annotation
/* .line 316 */
/* invoke-direct {p0}, Lcom/google/gson/stream/JsonWriter;->writeDeferredName()V */
/* .line 317 */
int v0 = 3; // const/4 v0, 0x3
/* const/16 v1, 0x7b */
/* invoke-direct {p0, v0, v1}, Lcom/google/gson/stream/JsonWriter;->open(IC)Lcom/google/gson/stream/JsonWriter; */
} // .end method
public void close ( ) {
/* .locals 3 */
/* .annotation system Ldalvik/annotation/Throws; */
/* value = { */
/* Ljava/io/IOException; */
/* } */
} // .end annotation
/* .line 587 */
v0 = this.out;
(( java.io.Writer ) v0 ).close ( ); // invoke-virtual {v0}, Ljava/io/Writer;->close()V
/* .line 589 */
/* iget v0, p0, Lcom/google/gson/stream/JsonWriter;->stackSize:I */
/* .line 590 */
/* .local v0, "size":I */
int v1 = 1; // const/4 v1, 0x1
/* if-gt v0, v1, :cond_1 */
/* if-ne v0, v1, :cond_0 */
v1 = this.stack;
/* add-int/lit8 v2, v0, -0x1 */
/* aget v1, v1, v2 */
int v2 = 7; // const/4 v2, 0x7
/* if-ne v1, v2, :cond_1 */
/* .line 593 */
} // :cond_0
int v1 = 0; // const/4 v1, 0x0
/* iput v1, p0, Lcom/google/gson/stream/JsonWriter;->stackSize:I */
/* .line 594 */
return;
/* .line 591 */
} // :cond_1
/* new-instance v1, Ljava/io/IOException; */
final String v2 = "Incomplete document"; // const-string v2, "Incomplete document"
/* invoke-direct {v1, v2}, Ljava/io/IOException;-><init>(Ljava/lang/String;)V */
/* throw v1 */
} // .end method
public com.google.gson.stream.JsonWriter endArray ( ) {
/* .locals 3 */
/* .annotation system Ldalvik/annotation/Throws; */
/* value = { */
/* Ljava/io/IOException; */
/* } */
} // .end annotation
/* .line 306 */
int v0 = 2; // const/4 v0, 0x2
/* const/16 v1, 0x5d */
int v2 = 1; // const/4 v2, 0x1
/* invoke-direct {p0, v2, v0, v1}, Lcom/google/gson/stream/JsonWriter;->close(IIC)Lcom/google/gson/stream/JsonWriter; */
} // .end method
public com.google.gson.stream.JsonWriter endObject ( ) {
/* .locals 3 */
/* .annotation system Ldalvik/annotation/Throws; */
/* value = { */
/* Ljava/io/IOException; */
/* } */
} // .end annotation
/* .line 326 */
int v0 = 5; // const/4 v0, 0x5
/* const/16 v1, 0x7d */
int v2 = 3; // const/4 v2, 0x3
/* invoke-direct {p0, v2, v0, v1}, Lcom/google/gson/stream/JsonWriter;->close(IIC)Lcom/google/gson/stream/JsonWriter; */
} // .end method
public void flush ( ) {
/* .locals 2 */
/* .annotation system Ldalvik/annotation/Throws; */
/* value = { */
/* Ljava/io/IOException; */
/* } */
} // .end annotation
/* .line 575 */
/* iget v0, p0, Lcom/google/gson/stream/JsonWriter;->stackSize:I */
if ( v0 != null) { // if-eqz v0, :cond_0
/* .line 578 */
v0 = this.out;
(( java.io.Writer ) v0 ).flush ( ); // invoke-virtual {v0}, Ljava/io/Writer;->flush()V
/* .line 579 */
return;
/* .line 576 */
} // :cond_0
/* new-instance v0, Ljava/lang/IllegalStateException; */
final String v1 = "JsonWriter is closed."; // const-string v1, "JsonWriter is closed."
/* invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V */
/* throw v0 */
} // .end method
public final Boolean getSerializeNulls ( ) {
/* .locals 1 */
/* .line 286 */
/* iget-boolean v0, p0, Lcom/google/gson/stream/JsonWriter;->serializeNulls:Z */
} // .end method
public final Boolean isHtmlSafe ( ) {
/* .locals 1 */
/* .line 270 */
/* iget-boolean v0, p0, Lcom/google/gson/stream/JsonWriter;->htmlSafe:Z */
} // .end method
public Boolean isLenient ( ) {
/* .locals 1 */
/* .line 251 */
/* iget-boolean v0, p0, Lcom/google/gson/stream/JsonWriter;->lenient:Z */
} // .end method
public com.google.gson.stream.JsonWriter jsonValue ( java.lang.String p0 ) {
/* .locals 1 */
/* .param p1, "value" # Ljava/lang/String; */
/* .annotation system Ldalvik/annotation/Throws; */
/* value = { */
/* Ljava/io/IOException; */
/* } */
} // .end annotation
/* .line 438 */
/* if-nez p1, :cond_0 */
/* .line 439 */
(( com.google.gson.stream.JsonWriter ) p0 ).nullValue ( ); // invoke-virtual {p0}, Lcom/google/gson/stream/JsonWriter;->nullValue()Lcom/google/gson/stream/JsonWriter;
/* .line 441 */
} // :cond_0
/* invoke-direct {p0}, Lcom/google/gson/stream/JsonWriter;->writeDeferredName()V */
/* .line 442 */
/* invoke-direct {p0}, Lcom/google/gson/stream/JsonWriter;->beforeValue()V */
/* .line 443 */
v0 = this.out;
(( java.io.Writer ) v0 ).append ( p1 ); // invoke-virtual {v0, p1}, Ljava/io/Writer;->append(Ljava/lang/CharSequence;)Ljava/io/Writer;
/* .line 444 */
} // .end method
public com.google.gson.stream.JsonWriter name ( java.lang.String p0 ) {
/* .locals 2 */
/* .param p1, "name" # Ljava/lang/String; */
/* .annotation system Ldalvik/annotation/Throws; */
/* value = { */
/* Ljava/io/IOException; */
/* } */
} // .end annotation
/* .line 393 */
if ( p1 != null) { // if-eqz p1, :cond_2
/* .line 396 */
v0 = this.deferredName;
/* if-nez v0, :cond_1 */
/* .line 399 */
/* iget v0, p0, Lcom/google/gson/stream/JsonWriter;->stackSize:I */
if ( v0 != null) { // if-eqz v0, :cond_0
/* .line 402 */
this.deferredName = p1;
/* .line 403 */
/* .line 400 */
} // :cond_0
/* new-instance v0, Ljava/lang/IllegalStateException; */
final String v1 = "JsonWriter is closed."; // const-string v1, "JsonWriter is closed."
/* invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V */
/* throw v0 */
/* .line 397 */
} // :cond_1
/* new-instance v0, Ljava/lang/IllegalStateException; */
/* invoke-direct {v0}, Ljava/lang/IllegalStateException;-><init>()V */
/* throw v0 */
/* .line 394 */
} // :cond_2
/* new-instance v0, Ljava/lang/NullPointerException; */
final String v1 = "name == null"; // const-string v1, "name == null"
/* invoke-direct {v0, v1}, Ljava/lang/NullPointerException;-><init>(Ljava/lang/String;)V */
/* throw v0 */
} // .end method
public com.google.gson.stream.JsonWriter nullValue ( ) {
/* .locals 2 */
/* .annotation system Ldalvik/annotation/Throws; */
/* value = { */
/* Ljava/io/IOException; */
/* } */
} // .end annotation
/* .line 453 */
v0 = this.deferredName;
if ( v0 != null) { // if-eqz v0, :cond_1
/* .line 454 */
/* iget-boolean v0, p0, Lcom/google/gson/stream/JsonWriter;->serializeNulls:Z */
if ( v0 != null) { // if-eqz v0, :cond_0
/* .line 455 */
/* invoke-direct {p0}, Lcom/google/gson/stream/JsonWriter;->writeDeferredName()V */
/* .line 457 */
} // :cond_0
int v0 = 0; // const/4 v0, 0x0
this.deferredName = v0;
/* .line 458 */
/* .line 461 */
} // :cond_1
} // :goto_0
/* invoke-direct {p0}, Lcom/google/gson/stream/JsonWriter;->beforeValue()V */
/* .line 462 */
v0 = this.out;
final String v1 = "null"; // const-string v1, "null"
(( java.io.Writer ) v0 ).write ( v1 ); // invoke-virtual {v0, v1}, Ljava/io/Writer;->write(Ljava/lang/String;)V
/* .line 463 */
} // .end method
public final void setHtmlSafe ( Boolean p0 ) {
/* .locals 0 */
/* .param p1, "htmlSafe" # Z */
/* .line 262 */
/* iput-boolean p1, p0, Lcom/google/gson/stream/JsonWriter;->htmlSafe:Z */
/* .line 263 */
return;
} // .end method
public final void setIndent ( java.lang.String p0 ) {
/* .locals 1 */
/* .param p1, "indent" # Ljava/lang/String; */
/* .line 222 */
v0 = (( java.lang.String ) p1 ).length ( ); // invoke-virtual {p1}, Ljava/lang/String;->length()I
/* if-nez v0, :cond_0 */
/* .line 223 */
int v0 = 0; // const/4 v0, 0x0
this.indent = v0;
/* .line 224 */
final String v0 = ":"; // const-string v0, ":"
this.separator = v0;
/* .line 226 */
} // :cond_0
this.indent = p1;
/* .line 227 */
final String v0 = ": "; // const-string v0, ": "
this.separator = v0;
/* .line 229 */
} // :goto_0
return;
} // .end method
public final void setLenient ( Boolean p0 ) {
/* .locals 0 */
/* .param p1, "lenient" # Z */
/* .line 244 */
/* iput-boolean p1, p0, Lcom/google/gson/stream/JsonWriter;->lenient:Z */
/* .line 245 */
return;
} // .end method
public final void setSerializeNulls ( Boolean p0 ) {
/* .locals 0 */
/* .param p1, "serializeNulls" # Z */
/* .line 278 */
/* iput-boolean p1, p0, Lcom/google/gson/stream/JsonWriter;->serializeNulls:Z */
/* .line 279 */
return;
} // .end method
public com.google.gson.stream.JsonWriter value ( Double p0 ) {
/* .locals 3 */
/* .param p1, "value" # D */
/* .annotation system Ldalvik/annotation/Throws; */
/* value = { */
/* Ljava/io/IOException; */
/* } */
} // .end annotation
/* .line 503 */
/* invoke-direct {p0}, Lcom/google/gson/stream/JsonWriter;->writeDeferredName()V */
/* .line 504 */
/* iget-boolean v0, p0, Lcom/google/gson/stream/JsonWriter;->lenient:Z */
/* if-nez v0, :cond_1 */
v0 = java.lang.Double .isNaN ( p1,p2 );
/* if-nez v0, :cond_0 */
v0 = java.lang.Double .isInfinite ( p1,p2 );
/* if-nez v0, :cond_0 */
/* .line 505 */
} // :cond_0
/* new-instance v0, Ljava/lang/IllegalArgumentException; */
/* new-instance v1, Ljava/lang/StringBuilder; */
/* invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V */
final String v2 = "Numeric values must be finite, but was "; // const-string v2, "Numeric values must be finite, but was "
(( java.lang.StringBuilder ) v1 ).append ( v2 ); // invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v1 ).append ( p1, p2 ); // invoke-virtual {v1, p1, p2}, Ljava/lang/StringBuilder;->append(D)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v1 ).toString ( ); // invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
/* invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V */
/* throw v0 */
/* .line 507 */
} // :cond_1
} // :goto_0
/* invoke-direct {p0}, Lcom/google/gson/stream/JsonWriter;->beforeValue()V */
/* .line 508 */
v0 = this.out;
java.lang.Double .toString ( p1,p2 );
(( java.io.Writer ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/io/Writer;->append(Ljava/lang/CharSequence;)Ljava/io/Writer;
/* .line 509 */
} // .end method
public com.google.gson.stream.JsonWriter value ( Long p0 ) {
/* .locals 2 */
/* .param p1, "value" # J */
/* .annotation system Ldalvik/annotation/Throws; */
/* value = { */
/* Ljava/io/IOException; */
/* } */
} // .end annotation
/* .line 518 */
/* invoke-direct {p0}, Lcom/google/gson/stream/JsonWriter;->writeDeferredName()V */
/* .line 519 */
/* invoke-direct {p0}, Lcom/google/gson/stream/JsonWriter;->beforeValue()V */
/* .line 520 */
v0 = this.out;
java.lang.Long .toString ( p1,p2 );
(( java.io.Writer ) v0 ).write ( v1 ); // invoke-virtual {v0, v1}, Ljava/io/Writer;->write(Ljava/lang/String;)V
/* .line 521 */
} // .end method
public com.google.gson.stream.JsonWriter value ( java.lang.Boolean p0 ) {
/* .locals 2 */
/* .param p1, "value" # Ljava/lang/Boolean; */
/* .annotation system Ldalvik/annotation/Throws; */
/* value = { */
/* Ljava/io/IOException; */
/* } */
} // .end annotation
/* .line 484 */
/* if-nez p1, :cond_0 */
/* .line 485 */
(( com.google.gson.stream.JsonWriter ) p0 ).nullValue ( ); // invoke-virtual {p0}, Lcom/google/gson/stream/JsonWriter;->nullValue()Lcom/google/gson/stream/JsonWriter;
/* .line 487 */
} // :cond_0
/* invoke-direct {p0}, Lcom/google/gson/stream/JsonWriter;->writeDeferredName()V */
/* .line 488 */
/* invoke-direct {p0}, Lcom/google/gson/stream/JsonWriter;->beforeValue()V */
/* .line 489 */
v0 = this.out;
v1 = (( java.lang.Boolean ) p1 ).booleanValue ( ); // invoke-virtual {p1}, Ljava/lang/Boolean;->booleanValue()Z
if ( v1 != null) { // if-eqz v1, :cond_1
/* const-string/jumbo v1, "true" */
} // :cond_1
final String v1 = "false"; // const-string v1, "false"
} // :goto_0
(( java.io.Writer ) v0 ).write ( v1 ); // invoke-virtual {v0, v1}, Ljava/io/Writer;->write(Ljava/lang/String;)V
/* .line 490 */
} // .end method
public com.google.gson.stream.JsonWriter value ( java.lang.Number p0 ) {
/* .locals 5 */
/* .param p1, "value" # Ljava/lang/Number; */
/* .annotation system Ldalvik/annotation/Throws; */
/* value = { */
/* Ljava/io/IOException; */
/* } */
} // .end annotation
/* .line 547 */
/* if-nez p1, :cond_0 */
/* .line 548 */
(( com.google.gson.stream.JsonWriter ) p0 ).nullValue ( ); // invoke-virtual {p0}, Lcom/google/gson/stream/JsonWriter;->nullValue()Lcom/google/gson/stream/JsonWriter;
/* .line 551 */
} // :cond_0
/* invoke-direct {p0}, Lcom/google/gson/stream/JsonWriter;->writeDeferredName()V */
/* .line 552 */
(( java.lang.Object ) p1 ).toString ( ); // invoke-virtual {p1}, Ljava/lang/Object;->toString()Ljava/lang/String;
/* .line 553 */
/* .local v0, "string":Ljava/lang/String; */
final String v1 = "-Infinity"; // const-string v1, "-Infinity"
v1 = (( java.lang.String ) v0 ).equals ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z
/* if-nez v1, :cond_3 */
final String v1 = "Infinity"; // const-string v1, "Infinity"
v1 = (( java.lang.String ) v0 ).equals ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z
/* if-nez v1, :cond_3 */
final String v1 = "NaN"; // const-string v1, "NaN"
v1 = (( java.lang.String ) v0 ).equals ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z
if ( v1 != null) { // if-eqz v1, :cond_1
/* .line 558 */
} // :cond_1
(( java.lang.Object ) p1 ).getClass ( ); // invoke-virtual {p1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;
/* .line 560 */
/* .local v1, "numberClass":Ljava/lang/Class;, "Ljava/lang/Class<+Ljava/lang/Number;>;" */
v2 = com.google.gson.stream.JsonWriter .isTrustedNumberType ( v1 );
/* if-nez v2, :cond_4 */
v2 = com.google.gson.stream.JsonWriter.VALID_JSON_NUMBER_PATTERN;
(( java.util.regex.Pattern ) v2 ).matcher ( v0 ); // invoke-virtual {v2, v0}, Ljava/util/regex/Pattern;->matcher(Ljava/lang/CharSequence;)Ljava/util/regex/Matcher;
v2 = (( java.util.regex.Matcher ) v2 ).matches ( ); // invoke-virtual {v2}, Ljava/util/regex/Matcher;->matches()Z
if ( v2 != null) { // if-eqz v2, :cond_2
/* .line 561 */
} // :cond_2
/* new-instance v2, Ljava/lang/IllegalArgumentException; */
/* new-instance v3, Ljava/lang/StringBuilder; */
/* invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V */
final String v4 = "String created by "; // const-string v4, "String created by "
(( java.lang.StringBuilder ) v3 ).append ( v4 ); // invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v3 ).append ( v1 ); // invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;
final String v4 = " is not a valid JSON number: "; // const-string v4, " is not a valid JSON number: "
(( java.lang.StringBuilder ) v3 ).append ( v4 ); // invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v3 ).append ( v0 ); // invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v3 ).toString ( ); // invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
/* invoke-direct {v2, v3}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V */
/* throw v2 */
/* .line 554 */
} // .end local v1 # "numberClass":Ljava/lang/Class;, "Ljava/lang/Class<+Ljava/lang/Number;>;"
} // :cond_3
} // :goto_0
/* iget-boolean v1, p0, Lcom/google/gson/stream/JsonWriter;->lenient:Z */
if ( v1 != null) { // if-eqz v1, :cond_5
/* .line 565 */
} // :cond_4
} // :goto_1
/* invoke-direct {p0}, Lcom/google/gson/stream/JsonWriter;->beforeValue()V */
/* .line 566 */
v1 = this.out;
(( java.io.Writer ) v1 ).append ( v0 ); // invoke-virtual {v1, v0}, Ljava/io/Writer;->append(Ljava/lang/CharSequence;)Ljava/io/Writer;
/* .line 567 */
/* .line 555 */
} // :cond_5
/* new-instance v1, Ljava/lang/IllegalArgumentException; */
/* new-instance v2, Ljava/lang/StringBuilder; */
/* invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V */
final String v3 = "Numeric values must be finite, but was "; // const-string v3, "Numeric values must be finite, but was "
(( java.lang.StringBuilder ) v2 ).append ( v3 ); // invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v2 ).append ( v0 ); // invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v2 ).toString ( ); // invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
/* invoke-direct {v1, v2}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V */
/* throw v1 */
} // .end method
public com.google.gson.stream.JsonWriter value ( java.lang.String p0 ) {
/* .locals 1 */
/* .param p1, "value" # Ljava/lang/String; */
/* .annotation system Ldalvik/annotation/Throws; */
/* value = { */
/* Ljava/io/IOException; */
/* } */
} // .end annotation
/* .line 421 */
/* if-nez p1, :cond_0 */
/* .line 422 */
(( com.google.gson.stream.JsonWriter ) p0 ).nullValue ( ); // invoke-virtual {p0}, Lcom/google/gson/stream/JsonWriter;->nullValue()Lcom/google/gson/stream/JsonWriter;
/* .line 424 */
} // :cond_0
/* invoke-direct {p0}, Lcom/google/gson/stream/JsonWriter;->writeDeferredName()V */
/* .line 425 */
/* invoke-direct {p0}, Lcom/google/gson/stream/JsonWriter;->beforeValue()V */
/* .line 426 */
/* invoke-direct {p0, p1}, Lcom/google/gson/stream/JsonWriter;->string(Ljava/lang/String;)V */
/* .line 427 */
} // .end method
public com.google.gson.stream.JsonWriter value ( Boolean p0 ) {
/* .locals 2 */
/* .param p1, "value" # Z */
/* .annotation system Ldalvik/annotation/Throws; */
/* value = { */
/* Ljava/io/IOException; */
/* } */
} // .end annotation
/* .line 472 */
/* invoke-direct {p0}, Lcom/google/gson/stream/JsonWriter;->writeDeferredName()V */
/* .line 473 */
/* invoke-direct {p0}, Lcom/google/gson/stream/JsonWriter;->beforeValue()V */
/* .line 474 */
v0 = this.out;
if ( p1 != null) { // if-eqz p1, :cond_0
/* const-string/jumbo v1, "true" */
} // :cond_0
final String v1 = "false"; // const-string v1, "false"
} // :goto_0
(( java.io.Writer ) v0 ).write ( v1 ); // invoke-virtual {v0, v1}, Ljava/io/Writer;->write(Ljava/lang/String;)V
/* .line 475 */
} // .end method
