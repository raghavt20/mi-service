public class com.google.gson.stream.JsonReader implements java.io.Closeable {
	 /* .source "JsonReader.java" */
	 /* # interfaces */
	 /* # static fields */
	 static final Integer BUFFER_SIZE;
	 private static final Long MIN_INCOMPLETE_INTEGER;
	 private static final Integer NUMBER_CHAR_DECIMAL;
	 private static final Integer NUMBER_CHAR_DIGIT;
	 private static final Integer NUMBER_CHAR_EXP_DIGIT;
	 private static final Integer NUMBER_CHAR_EXP_E;
	 private static final Integer NUMBER_CHAR_EXP_SIGN;
	 private static final Integer NUMBER_CHAR_FRACTION_DIGIT;
	 private static final Integer NUMBER_CHAR_NONE;
	 private static final Integer NUMBER_CHAR_SIGN;
	 private static final Integer PEEKED_BEGIN_ARRAY;
	 private static final Integer PEEKED_BEGIN_OBJECT;
	 private static final Integer PEEKED_BUFFERED;
	 private static final Integer PEEKED_DOUBLE_QUOTED;
	 private static final Integer PEEKED_DOUBLE_QUOTED_NAME;
	 private static final Integer PEEKED_END_ARRAY;
	 private static final Integer PEEKED_END_OBJECT;
	 private static final Integer PEEKED_EOF;
	 private static final Integer PEEKED_FALSE;
	 private static final Integer PEEKED_LONG;
	 private static final Integer PEEKED_NONE;
	 private static final Integer PEEKED_NULL;
	 private static final Integer PEEKED_NUMBER;
	 private static final Integer PEEKED_SINGLE_QUOTED;
	 private static final Integer PEEKED_SINGLE_QUOTED_NAME;
	 private static final Integer PEEKED_TRUE;
	 private static final Integer PEEKED_UNQUOTED;
	 private static final Integer PEEKED_UNQUOTED_NAME;
	 /* # instance fields */
	 private final buffer;
	 private final java.io.Reader in;
	 private Boolean lenient;
	 private Integer limit;
	 private Integer lineNumber;
	 private Integer lineStart;
	 private pathIndices;
	 private java.lang.String pathNames;
	 Integer peeked;
	 private Long peekedLong;
	 private Integer peekedNumberLength;
	 private java.lang.String peekedString;
	 private Integer pos;
	 private stack;
	 private Integer stackSize;
	 /* # direct methods */
	 static com.google.gson.stream.JsonReader ( ) {
		 /* .locals 1 */
		 /* .line 1623 */
		 /* new-instance v0, Lcom/google/gson/stream/JsonReader$1; */
		 /* invoke-direct {v0}, Lcom/google/gson/stream/JsonReader$1;-><init>()V */
		 /* .line 1645 */
		 return;
	 } // .end method
	 public com.google.gson.stream.JsonReader ( ) {
		 /* .locals 4 */
		 /* .param p1, "in" # Ljava/io/Reader; */
		 /* .line 289 */
		 /* invoke-direct {p0}, Ljava/lang/Object;-><init>()V */
		 /* .line 229 */
		 int v0 = 0; // const/4 v0, 0x0
		 /* iput-boolean v0, p0, Lcom/google/gson/stream/JsonReader;->lenient:Z */
		 /* .line 238 */
		 /* const/16 v1, 0x400 */
		 /* new-array v1, v1, [C */
		 this.buffer = v1;
		 /* .line 239 */
		 /* iput v0, p0, Lcom/google/gson/stream/JsonReader;->pos:I */
		 /* .line 240 */
		 /* iput v0, p0, Lcom/google/gson/stream/JsonReader;->limit:I */
		 /* .line 242 */
		 /* iput v0, p0, Lcom/google/gson/stream/JsonReader;->lineNumber:I */
		 /* .line 243 */
		 /* iput v0, p0, Lcom/google/gson/stream/JsonReader;->lineStart:I */
		 /* .line 245 */
		 /* iput v0, p0, Lcom/google/gson/stream/JsonReader;->peeked:I */
		 /* .line 269 */
		 /* const/16 v1, 0x20 */
		 /* new-array v2, v1, [I */
		 this.stack = v2;
		 /* .line 270 */
		 /* iput v0, p0, Lcom/google/gson/stream/JsonReader;->stackSize:I */
		 /* .line 272 */
		 /* add-int/lit8 v3, v0, 0x1 */
		 /* iput v3, p0, Lcom/google/gson/stream/JsonReader;->stackSize:I */
		 int v3 = 6; // const/4 v3, 0x6
		 /* aput v3, v2, v0 */
		 /* .line 283 */
		 /* new-array v0, v1, [Ljava/lang/String; */
		 this.pathNames = v0;
		 /* .line 284 */
		 /* new-array v0, v1, [I */
		 this.pathIndices = v0;
		 /* .line 290 */
		 if ( p1 != null) { // if-eqz p1, :cond_0
			 /* .line 293 */
			 this.in = p1;
			 /* .line 294 */
			 return;
			 /* .line 291 */
		 } // :cond_0
		 /* new-instance v0, Ljava/lang/NullPointerException; */
		 final String v1 = "in == null"; // const-string v1, "in == null"
		 /* invoke-direct {v0, v1}, Ljava/lang/NullPointerException;-><init>(Ljava/lang/String;)V */
		 /* throw v0 */
	 } // .end method
	 private void checkLenient ( ) {
		 /* .locals 1 */
		 /* .annotation system Ldalvik/annotation/Throws; */
		 /* value = { */
		 /* Ljava/io/IOException; */
		 /* } */
	 } // .end annotation
	 /* .line 1403 */
	 /* iget-boolean v0, p0, Lcom/google/gson/stream/JsonReader;->lenient:Z */
	 if ( v0 != null) { // if-eqz v0, :cond_0
		 /* .line 1406 */
		 return;
		 /* .line 1404 */
	 } // :cond_0
	 final String v0 = "Use JsonReader.setLenient(true) to accept malformed JSON"; // const-string v0, "Use JsonReader.setLenient(true) to accept malformed JSON"
	 /* invoke-direct {p0, v0}, Lcom/google/gson/stream/JsonReader;->syntaxError(Ljava/lang/String;)Ljava/io/IOException; */
	 /* throw v0 */
} // .end method
private void consumeNonExecutePrefix ( ) {
	 /* .locals 5 */
	 /* .annotation system Ldalvik/annotation/Throws; */
	 /* value = { */
	 /* Ljava/io/IOException; */
	 /* } */
} // .end annotation
/* .line 1605 */
int v0 = 1; // const/4 v0, 0x1
/* invoke-direct {p0, v0}, Lcom/google/gson/stream/JsonReader;->nextNonWhitespace(Z)I */
/* .line 1606 */
/* iget v1, p0, Lcom/google/gson/stream/JsonReader;->pos:I */
/* sub-int/2addr v1, v0 */
/* iput v1, p0, Lcom/google/gson/stream/JsonReader;->pos:I */
/* .line 1608 */
int v0 = 5; // const/4 v0, 0x5
/* add-int/2addr v1, v0 */
/* iget v2, p0, Lcom/google/gson/stream/JsonReader;->limit:I */
/* if-le v1, v2, :cond_0 */
v1 = /* invoke-direct {p0, v0}, Lcom/google/gson/stream/JsonReader;->fillBuffer(I)Z */
/* if-nez v1, :cond_0 */
/* .line 1609 */
return;
/* .line 1612 */
} // :cond_0
/* iget v1, p0, Lcom/google/gson/stream/JsonReader;->pos:I */
/* .line 1613 */
/* .local v1, "p":I */
v2 = this.buffer;
/* .line 1614 */
/* .local v2, "buf":[C */
/* aget-char v3, v2, v1 */
/* const/16 v4, 0x29 */
/* if-ne v3, v4, :cond_2 */
/* add-int/lit8 v3, v1, 0x1 */
/* aget-char v3, v2, v3 */
/* const/16 v4, 0x5d */
/* if-ne v3, v4, :cond_2 */
/* add-int/lit8 v3, v1, 0x2 */
/* aget-char v3, v2, v3 */
/* const/16 v4, 0x7d */
/* if-ne v3, v4, :cond_2 */
/* add-int/lit8 v3, v1, 0x3 */
/* aget-char v3, v2, v3 */
/* const/16 v4, 0x27 */
/* if-ne v3, v4, :cond_2 */
/* add-int/lit8 v3, v1, 0x4 */
/* aget-char v3, v2, v3 */
/* const/16 v4, 0xa */
/* if-eq v3, v4, :cond_1 */
/* .line 1619 */
} // :cond_1
/* iget v3, p0, Lcom/google/gson/stream/JsonReader;->pos:I */
/* add-int/2addr v3, v0 */
/* iput v3, p0, Lcom/google/gson/stream/JsonReader;->pos:I */
/* .line 1620 */
return;
/* .line 1615 */
} // :cond_2
} // :goto_0
return;
} // .end method
private Boolean fillBuffer ( Integer p0 ) {
/* .locals 8 */
/* .param p1, "minimum" # I */
/* .annotation system Ldalvik/annotation/Throws; */
/* value = { */
/* Ljava/io/IOException; */
/* } */
} // .end annotation
/* .line 1279 */
v0 = this.buffer;
/* .line 1280 */
/* .local v0, "buffer":[C */
/* iget v1, p0, Lcom/google/gson/stream/JsonReader;->lineStart:I */
/* iget v2, p0, Lcom/google/gson/stream/JsonReader;->pos:I */
/* sub-int/2addr v1, v2 */
/* iput v1, p0, Lcom/google/gson/stream/JsonReader;->lineStart:I */
/* .line 1281 */
/* iget v1, p0, Lcom/google/gson/stream/JsonReader;->limit:I */
int v3 = 0; // const/4 v3, 0x0
/* if-eq v1, v2, :cond_0 */
/* .line 1282 */
/* sub-int/2addr v1, v2 */
/* iput v1, p0, Lcom/google/gson/stream/JsonReader;->limit:I */
/* .line 1283 */
java.lang.System .arraycopy ( v0,v2,v0,v3,v1 );
/* .line 1285 */
} // :cond_0
/* iput v3, p0, Lcom/google/gson/stream/JsonReader;->limit:I */
/* .line 1288 */
} // :goto_0
/* iput v3, p0, Lcom/google/gson/stream/JsonReader;->pos:I */
/* .line 1290 */
} // :cond_1
v1 = this.in;
/* iget v2, p0, Lcom/google/gson/stream/JsonReader;->limit:I */
/* array-length v4, v0 */
/* sub-int/2addr v4, v2 */
v1 = (( java.io.Reader ) v1 ).read ( v0, v2, v4 ); // invoke-virtual {v1, v0, v2, v4}, Ljava/io/Reader;->read([CII)I
/* move v2, v1 */
/* .local v2, "total":I */
int v4 = -1; // const/4 v4, -0x1
/* if-eq v1, v4, :cond_3 */
/* .line 1291 */
/* iget v1, p0, Lcom/google/gson/stream/JsonReader;->limit:I */
/* add-int/2addr v1, v2 */
/* iput v1, p0, Lcom/google/gson/stream/JsonReader;->limit:I */
/* .line 1294 */
/* iget v4, p0, Lcom/google/gson/stream/JsonReader;->lineNumber:I */
int v5 = 1; // const/4 v5, 0x1
/* if-nez v4, :cond_2 */
/* iget v4, p0, Lcom/google/gson/stream/JsonReader;->lineStart:I */
/* if-nez v4, :cond_2 */
/* if-lez v1, :cond_2 */
/* aget-char v6, v0, v3 */
/* const v7, 0xfeff */
/* if-ne v6, v7, :cond_2 */
/* .line 1295 */
/* iget v6, p0, Lcom/google/gson/stream/JsonReader;->pos:I */
/* add-int/2addr v6, v5 */
/* iput v6, p0, Lcom/google/gson/stream/JsonReader;->pos:I */
/* .line 1296 */
/* add-int/lit8 v4, v4, 0x1 */
/* iput v4, p0, Lcom/google/gson/stream/JsonReader;->lineStart:I */
/* .line 1297 */
/* add-int/lit8 p1, p1, 0x1 */
/* .line 1300 */
} // :cond_2
/* if-lt v1, p1, :cond_1 */
/* .line 1301 */
/* .line 1304 */
} // :cond_3
} // .end method
private java.lang.String getPath ( Boolean p0 ) {
/* .locals 5 */
/* .param p1, "usePreviousPath" # Z */
/* .line 1459 */
/* new-instance v0, Ljava/lang/StringBuilder; */
/* invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V */
/* const/16 v1, 0x24 */
(( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;
/* .line 1460 */
/* .local v0, "result":Ljava/lang/StringBuilder; */
int v1 = 0; // const/4 v1, 0x0
/* .local v1, "i":I */
} // :goto_0
/* iget v2, p0, Lcom/google/gson/stream/JsonReader;->stackSize:I */
/* if-ge v1, v2, :cond_2 */
/* .line 1461 */
v3 = this.stack;
/* aget v3, v3, v1 */
/* packed-switch v3, :pswitch_data_0 */
/* .line 1474 */
/* :pswitch_0 */
/* const/16 v2, 0x2e */
(( java.lang.StringBuilder ) v0 ).append ( v2 ); // invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;
/* .line 1475 */
v2 = this.pathNames;
/* aget-object v2, v2, v1 */
if ( v2 != null) { // if-eqz v2, :cond_1
/* .line 1476 */
(( java.lang.StringBuilder ) v0 ).append ( v2 ); // invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
/* .line 1464 */
/* :pswitch_1 */
v3 = this.pathIndices;
/* aget v3, v3, v1 */
/* .line 1466 */
/* .local v3, "pathIndex":I */
if ( p1 != null) { // if-eqz p1, :cond_0
/* if-lez v3, :cond_0 */
/* add-int/lit8 v2, v2, -0x1 */
/* if-ne v1, v2, :cond_0 */
/* .line 1467 */
/* add-int/lit8 v3, v3, -0x1 */
/* .line 1469 */
} // :cond_0
/* const/16 v2, 0x5b */
(( java.lang.StringBuilder ) v0 ).append ( v2 ); // invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v2 ).append ( v3 ); // invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;
/* const/16 v4, 0x5d */
(( java.lang.StringBuilder ) v2 ).append ( v4 ); // invoke-virtual {v2, v4}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;
/* .line 1470 */
/* nop */
/* .line 1460 */
} // .end local v3 # "pathIndex":I
} // :cond_1
} // :goto_1
/* add-int/lit8 v1, v1, 0x1 */
/* .line 1485 */
} // .end local v1 # "i":I
} // :cond_2
(( java.lang.StringBuilder ) v0 ).toString ( ); // invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
/* nop */
/* :pswitch_data_0 */
/* .packed-switch 0x1 */
/* :pswitch_1 */
/* :pswitch_1 */
/* :pswitch_0 */
/* :pswitch_0 */
/* :pswitch_0 */
} // .end packed-switch
} // .end method
private Boolean isLiteral ( Object p0 ) {
/* .locals 1 */
/* .param p1, "c" # C */
/* .annotation system Ldalvik/annotation/Throws; */
/* value = { */
/* Ljava/io/IOException; */
/* } */
} // .end annotation
/* .line 745 */
/* sparse-switch p1, :sswitch_data_0 */
/* .line 765 */
int v0 = 1; // const/4 v0, 0x1
/* .line 751 */
/* :sswitch_0 */
/* invoke-direct {p0}, Lcom/google/gson/stream/JsonReader;->checkLenient()V */
/* .line 763 */
/* :sswitch_1 */
int v0 = 0; // const/4 v0, 0x0
/* :sswitch_data_0 */
/* .sparse-switch */
/* 0x9 -> :sswitch_1 */
/* 0xa -> :sswitch_1 */
/* 0xc -> :sswitch_1 */
/* 0xd -> :sswitch_1 */
/* 0x20 -> :sswitch_1 */
/* 0x23 -> :sswitch_0 */
/* 0x2c -> :sswitch_1 */
/* 0x2f -> :sswitch_0 */
/* 0x3a -> :sswitch_1 */
/* 0x3b -> :sswitch_0 */
/* 0x3d -> :sswitch_0 */
/* 0x5b -> :sswitch_1 */
/* 0x5c -> :sswitch_0 */
/* 0x5d -> :sswitch_1 */
/* 0x7b -> :sswitch_1 */
/* 0x7d -> :sswitch_1 */
} // .end sparse-switch
} // .end method
private Integer nextNonWhitespace ( Boolean p0 ) {
/* .locals 8 */
/* .param p1, "throwOnEof" # Z */
/* .annotation system Ldalvik/annotation/Throws; */
/* value = { */
/* Ljava/io/IOException; */
/* } */
} // .end annotation
/* .line 1322 */
v0 = this.buffer;
/* .line 1323 */
/* .local v0, "buffer":[C */
/* iget v1, p0, Lcom/google/gson/stream/JsonReader;->pos:I */
/* .line 1324 */
/* .local v1, "p":I */
/* iget v2, p0, Lcom/google/gson/stream/JsonReader;->limit:I */
/* .line 1326 */
/* .local v2, "l":I */
} // :goto_0
int v3 = 1; // const/4 v3, 0x1
/* if-ne v1, v2, :cond_2 */
/* .line 1327 */
/* iput v1, p0, Lcom/google/gson/stream/JsonReader;->pos:I */
/* .line 1328 */
v4 = /* invoke-direct {p0, v3}, Lcom/google/gson/stream/JsonReader;->fillBuffer(I)Z */
/* if-nez v4, :cond_1 */
/* .line 1329 */
/* nop */
/* .line 1395 */
/* if-nez p1, :cond_0 */
/* .line 1398 */
int v3 = -1; // const/4 v3, -0x1
/* .line 1396 */
} // :cond_0
/* new-instance v3, Ljava/io/EOFException; */
/* new-instance v4, Ljava/lang/StringBuilder; */
/* invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V */
final String v5 = "End of input"; // const-string v5, "End of input"
(( java.lang.StringBuilder ) v4 ).append ( v5 ); // invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( com.google.gson.stream.JsonReader ) p0 ).locationString ( ); // invoke-virtual {p0}, Lcom/google/gson/stream/JsonReader;->locationString()Ljava/lang/String;
(( java.lang.StringBuilder ) v4 ).append ( v5 ); // invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v4 ).toString ( ); // invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
/* invoke-direct {v3, v4}, Ljava/io/EOFException;-><init>(Ljava/lang/String;)V */
/* throw v3 */
/* .line 1331 */
} // :cond_1
/* iget v1, p0, Lcom/google/gson/stream/JsonReader;->pos:I */
/* .line 1332 */
/* iget v2, p0, Lcom/google/gson/stream/JsonReader;->limit:I */
/* .line 1335 */
} // :cond_2
/* add-int/lit8 v4, v1, 0x1 */
} // .end local v1 # "p":I
/* .local v4, "p":I */
/* aget-char v1, v0, v1 */
/* .line 1336 */
/* .local v1, "c":I */
/* const/16 v5, 0xa */
/* if-ne v1, v5, :cond_3 */
/* .line 1337 */
/* iget v5, p0, Lcom/google/gson/stream/JsonReader;->lineNumber:I */
/* add-int/2addr v5, v3 */
/* iput v5, p0, Lcom/google/gson/stream/JsonReader;->lineNumber:I */
/* .line 1338 */
/* iput v4, p0, Lcom/google/gson/stream/JsonReader;->lineStart:I */
/* .line 1339 */
/* goto/16 :goto_1 */
/* .line 1340 */
} // :cond_3
/* const/16 v5, 0x20 */
/* if-eq v1, v5, :cond_9 */
/* const/16 v5, 0xd */
/* if-eq v1, v5, :cond_9 */
/* const/16 v5, 0x9 */
/* if-ne v1, v5, :cond_4 */
/* .line 1341 */
/* .line 1344 */
} // :cond_4
/* const/16 v5, 0x2f */
/* if-ne v1, v5, :cond_7 */
/* .line 1345 */
/* iput v4, p0, Lcom/google/gson/stream/JsonReader;->pos:I */
/* .line 1346 */
int v5 = 2; // const/4 v5, 0x2
/* if-ne v4, v2, :cond_5 */
/* .line 1347 */
/* add-int/lit8 v6, v4, -0x1 */
/* iput v6, p0, Lcom/google/gson/stream/JsonReader;->pos:I */
/* .line 1348 */
v6 = /* invoke-direct {p0, v5}, Lcom/google/gson/stream/JsonReader;->fillBuffer(I)Z */
/* .line 1349 */
/* .local v6, "charsLoaded":Z */
/* iget v7, p0, Lcom/google/gson/stream/JsonReader;->pos:I */
/* add-int/2addr v7, v3 */
/* iput v7, p0, Lcom/google/gson/stream/JsonReader;->pos:I */
/* .line 1350 */
/* if-nez v6, :cond_5 */
/* .line 1351 */
/* .line 1355 */
} // .end local v6 # "charsLoaded":Z
} // :cond_5
/* invoke-direct {p0}, Lcom/google/gson/stream/JsonReader;->checkLenient()V */
/* .line 1356 */
/* iget v3, p0, Lcom/google/gson/stream/JsonReader;->pos:I */
/* aget-char v6, v0, v3 */
/* .line 1357 */
/* .local v6, "peek":C */
/* sparse-switch v6, :sswitch_data_0 */
/* .line 1377 */
/* .line 1370 */
/* :sswitch_0 */
/* add-int/lit8 v3, v3, 0x1 */
/* iput v3, p0, Lcom/google/gson/stream/JsonReader;->pos:I */
/* .line 1371 */
/* invoke-direct {p0}, Lcom/google/gson/stream/JsonReader;->skipToEndOfLine()V */
/* .line 1372 */
/* iget v3, p0, Lcom/google/gson/stream/JsonReader;->pos:I */
/* .line 1373 */
} // .end local v4 # "p":I
/* .local v3, "p":I */
/* iget v2, p0, Lcom/google/gson/stream/JsonReader;->limit:I */
/* .line 1374 */
/* move v1, v3 */
/* .line 1360 */
} // .end local v3 # "p":I
/* .restart local v4 # "p":I */
/* :sswitch_1 */
/* add-int/lit8 v3, v3, 0x1 */
/* iput v3, p0, Lcom/google/gson/stream/JsonReader;->pos:I */
/* .line 1361 */
final String v3 = "*/"; // const-string v3, "*/"
v3 = /* invoke-direct {p0, v3}, Lcom/google/gson/stream/JsonReader;->skipTo(Ljava/lang/String;)Z */
if ( v3 != null) { // if-eqz v3, :cond_6
/* .line 1364 */
/* iget v3, p0, Lcom/google/gson/stream/JsonReader;->pos:I */
/* add-int/2addr v3, v5 */
/* .line 1365 */
} // .end local v4 # "p":I
/* .restart local v3 # "p":I */
/* iget v2, p0, Lcom/google/gson/stream/JsonReader;->limit:I */
/* .line 1366 */
/* move v1, v3 */
/* goto/16 :goto_0 */
/* .line 1362 */
} // .end local v3 # "p":I
/* .restart local v4 # "p":I */
} // :cond_6
final String v3 = "Unterminated comment"; // const-string v3, "Unterminated comment"
/* invoke-direct {p0, v3}, Lcom/google/gson/stream/JsonReader;->syntaxError(Ljava/lang/String;)Ljava/io/IOException; */
/* throw v3 */
/* .line 1379 */
} // .end local v6 # "peek":C
} // :cond_7
/* const/16 v3, 0x23 */
/* if-ne v1, v3, :cond_8 */
/* .line 1380 */
/* iput v4, p0, Lcom/google/gson/stream/JsonReader;->pos:I */
/* .line 1386 */
/* invoke-direct {p0}, Lcom/google/gson/stream/JsonReader;->checkLenient()V */
/* .line 1387 */
/* invoke-direct {p0}, Lcom/google/gson/stream/JsonReader;->skipToEndOfLine()V */
/* .line 1388 */
/* iget v3, p0, Lcom/google/gson/stream/JsonReader;->pos:I */
/* .line 1389 */
} // .end local v4 # "p":I
/* .restart local v3 # "p":I */
/* iget v2, p0, Lcom/google/gson/stream/JsonReader;->limit:I */
/* .line 1394 */
} // .end local v1 # "c":I
/* move v1, v3 */
/* goto/16 :goto_0 */
/* .line 1391 */
} // .end local v3 # "p":I
/* .restart local v1 # "c":I */
/* .restart local v4 # "p":I */
} // :cond_8
/* iput v4, p0, Lcom/google/gson/stream/JsonReader;->pos:I */
/* .line 1392 */
/* .line 1326 */
} // .end local v1 # "c":I
} // :cond_9
} // :goto_1
/* move v1, v4 */
/* goto/16 :goto_0 */
/* :sswitch_data_0 */
/* .sparse-switch */
/* 0x2a -> :sswitch_1 */
/* 0x2f -> :sswitch_0 */
} // .end sparse-switch
} // .end method
private java.lang.String nextQuotedValue ( Object p0 ) {
/* .locals 10 */
/* .param p1, "quote" # C */
/* .annotation system Ldalvik/annotation/Throws; */
/* value = { */
/* Ljava/io/IOException; */
/* } */
} // .end annotation
/* .line 987 */
v0 = this.buffer;
/* .line 988 */
/* .local v0, "buffer":[C */
int v1 = 0; // const/4 v1, 0x0
/* .line 990 */
/* .local v1, "builder":Ljava/lang/StringBuilder; */
} // :goto_0
/* iget v2, p0, Lcom/google/gson/stream/JsonReader;->pos:I */
/* .line 991 */
/* .local v2, "p":I */
/* iget v3, p0, Lcom/google/gson/stream/JsonReader;->limit:I */
/* .line 993 */
/* .local v3, "l":I */
/* move v4, v2 */
/* .line 994 */
/* .local v4, "start":I */
} // :goto_1
/* const/16 v5, 0x10 */
int v6 = 1; // const/4 v6, 0x1
/* if-ge v2, v3, :cond_5 */
/* .line 995 */
/* add-int/lit8 v7, v2, 0x1 */
} // .end local v2 # "p":I
/* .local v7, "p":I */
/* aget-char v2, v0, v2 */
/* .line 997 */
/* .local v2, "c":I */
/* if-ne v2, p1, :cond_1 */
/* .line 998 */
/* iput v7, p0, Lcom/google/gson/stream/JsonReader;->pos:I */
/* .line 999 */
/* sub-int v5, v7, v4 */
/* sub-int/2addr v5, v6 */
/* .line 1000 */
/* .local v5, "len":I */
/* if-nez v1, :cond_0 */
/* .line 1001 */
/* new-instance v6, Ljava/lang/String; */
/* invoke-direct {v6, v0, v4, v5}, Ljava/lang/String;-><init>([CII)V */
/* .line 1003 */
} // :cond_0
(( java.lang.StringBuilder ) v1 ).append ( v0, v4, v5 ); // invoke-virtual {v1, v0, v4, v5}, Ljava/lang/StringBuilder;->append([CII)Ljava/lang/StringBuilder;
/* .line 1004 */
(( java.lang.StringBuilder ) v1 ).toString ( ); // invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
/* .line 1006 */
} // .end local v5 # "len":I
} // :cond_1
/* const/16 v8, 0x5c */
/* if-ne v2, v8, :cond_3 */
/* .line 1007 */
/* iput v7, p0, Lcom/google/gson/stream/JsonReader;->pos:I */
/* .line 1008 */
/* sub-int v8, v7, v4 */
/* sub-int/2addr v8, v6 */
/* .line 1009 */
/* .local v8, "len":I */
/* if-nez v1, :cond_2 */
/* .line 1010 */
/* add-int/lit8 v6, v8, 0x1 */
/* mul-int/lit8 v6, v6, 0x2 */
/* .line 1011 */
/* .local v6, "estimatedLength":I */
/* new-instance v9, Ljava/lang/StringBuilder; */
v5 = java.lang.Math .max ( v6,v5 );
/* invoke-direct {v9, v5}, Ljava/lang/StringBuilder;-><init>(I)V */
/* move-object v1, v9 */
/* .line 1013 */
} // .end local v6 # "estimatedLength":I
} // :cond_2
(( java.lang.StringBuilder ) v1 ).append ( v0, v4, v8 ); // invoke-virtual {v1, v0, v4, v8}, Ljava/lang/StringBuilder;->append([CII)Ljava/lang/StringBuilder;
/* .line 1014 */
v5 = /* invoke-direct {p0}, Lcom/google/gson/stream/JsonReader;->readEscapeCharacter()C */
(( java.lang.StringBuilder ) v1 ).append ( v5 ); // invoke-virtual {v1, v5}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;
/* .line 1015 */
/* iget v5, p0, Lcom/google/gson/stream/JsonReader;->pos:I */
/* .line 1016 */
} // .end local v7 # "p":I
/* .local v5, "p":I */
/* iget v3, p0, Lcom/google/gson/stream/JsonReader;->limit:I */
/* .line 1017 */
/* move v4, v5 */
/* .line 1018 */
} // .end local v8 # "len":I
/* move v2, v5 */
} // .end local v5 # "p":I
/* .restart local v7 # "p":I */
} // :cond_3
/* const/16 v5, 0xa */
/* if-ne v2, v5, :cond_4 */
/* .line 1019 */
/* iget v5, p0, Lcom/google/gson/stream/JsonReader;->lineNumber:I */
/* add-int/2addr v5, v6 */
/* iput v5, p0, Lcom/google/gson/stream/JsonReader;->lineNumber:I */
/* .line 1020 */
/* iput v7, p0, Lcom/google/gson/stream/JsonReader;->lineStart:I */
/* .line 1022 */
} // .end local v2 # "c":I
} // :cond_4
/* move v2, v7 */
} // .end local v7 # "p":I
/* .local v2, "p":I */
} // :goto_2
/* .line 1024 */
} // :cond_5
/* if-nez v1, :cond_6 */
/* .line 1025 */
/* sub-int v7, v2, v4 */
/* mul-int/lit8 v7, v7, 0x2 */
/* .line 1026 */
/* .local v7, "estimatedLength":I */
/* new-instance v8, Ljava/lang/StringBuilder; */
v5 = java.lang.Math .max ( v7,v5 );
/* invoke-direct {v8, v5}, Ljava/lang/StringBuilder;-><init>(I)V */
/* move-object v1, v8 */
/* .line 1028 */
} // .end local v7 # "estimatedLength":I
} // :cond_6
/* sub-int v5, v2, v4 */
(( java.lang.StringBuilder ) v1 ).append ( v0, v4, v5 ); // invoke-virtual {v1, v0, v4, v5}, Ljava/lang/StringBuilder;->append([CII)Ljava/lang/StringBuilder;
/* .line 1029 */
/* iput v2, p0, Lcom/google/gson/stream/JsonReader;->pos:I */
/* .line 1030 */
v5 = /* invoke-direct {p0, v6}, Lcom/google/gson/stream/JsonReader;->fillBuffer(I)Z */
if ( v5 != null) { // if-eqz v5, :cond_7
/* .line 1033 */
} // .end local v2 # "p":I
} // .end local v3 # "l":I
} // .end local v4 # "start":I
/* .line 1031 */
/* .restart local v2 # "p":I */
/* .restart local v3 # "l":I */
/* .restart local v4 # "start":I */
} // :cond_7
final String v5 = "Unterminated string"; // const-string v5, "Unterminated string"
/* invoke-direct {p0, v5}, Lcom/google/gson/stream/JsonReader;->syntaxError(Ljava/lang/String;)Ljava/io/IOException; */
/* throw v5 */
} // .end method
private java.lang.String nextUnquotedValue ( ) {
/* .locals 5 */
/* .annotation system Ldalvik/annotation/Throws; */
/* value = { */
/* Ljava/io/IOException; */
/* } */
} // .end annotation
/* .line 1041 */
int v0 = 0; // const/4 v0, 0x0
/* .line 1042 */
/* .local v0, "builder":Ljava/lang/StringBuilder; */
int v1 = 0; // const/4 v1, 0x0
/* .line 1046 */
/* .local v1, "i":I */
} // :cond_0
} // :goto_0
/* iget v2, p0, Lcom/google/gson/stream/JsonReader;->pos:I */
/* add-int v3, v2, v1 */
/* iget v4, p0, Lcom/google/gson/stream/JsonReader;->limit:I */
/* if-ge v3, v4, :cond_1 */
/* .line 1047 */
v3 = this.buffer;
/* add-int/2addr v2, v1 */
/* aget-char v2, v3, v2 */
/* sparse-switch v2, :sswitch_data_0 */
/* .line 1046 */
/* add-int/lit8 v1, v1, 0x1 */
/* .line 1053 */
/* :sswitch_0 */
/* invoke-direct {p0}, Lcom/google/gson/stream/JsonReader;->checkLenient()V */
/* .line 1065 */
/* :sswitch_1 */
/* .line 1070 */
} // :cond_1
v2 = this.buffer;
/* array-length v2, v2 */
/* if-ge v1, v2, :cond_2 */
/* .line 1071 */
/* add-int/lit8 v2, v1, 0x1 */
v2 = /* invoke-direct {p0, v2}, Lcom/google/gson/stream/JsonReader;->fillBuffer(I)Z */
if ( v2 != null) { // if-eqz v2, :cond_4
/* .line 1072 */
/* .line 1079 */
} // :cond_2
/* if-nez v0, :cond_3 */
/* .line 1080 */
/* new-instance v2, Ljava/lang/StringBuilder; */
/* const/16 v3, 0x10 */
v3 = java.lang.Math .max ( v1,v3 );
/* invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(I)V */
/* move-object v0, v2 */
/* .line 1082 */
} // :cond_3
v2 = this.buffer;
/* iget v3, p0, Lcom/google/gson/stream/JsonReader;->pos:I */
(( java.lang.StringBuilder ) v0 ).append ( v2, v3, v1 ); // invoke-virtual {v0, v2, v3, v1}, Ljava/lang/StringBuilder;->append([CII)Ljava/lang/StringBuilder;
/* .line 1083 */
/* iget v2, p0, Lcom/google/gson/stream/JsonReader;->pos:I */
/* add-int/2addr v2, v1 */
/* iput v2, p0, Lcom/google/gson/stream/JsonReader;->pos:I */
/* .line 1084 */
int v1 = 0; // const/4 v1, 0x0
/* .line 1085 */
int v2 = 1; // const/4 v2, 0x1
v2 = /* invoke-direct {p0, v2}, Lcom/google/gson/stream/JsonReader;->fillBuffer(I)Z */
/* if-nez v2, :cond_0 */
/* .line 1086 */
/* nop */
/* .line 1090 */
} // :cond_4
} // :goto_1
/* if-nez v0, :cond_5 */
/* new-instance v2, Ljava/lang/String; */
v3 = this.buffer;
/* iget v4, p0, Lcom/google/gson/stream/JsonReader;->pos:I */
/* invoke-direct {v2, v3, v4, v1}, Ljava/lang/String;-><init>([CII)V */
} // :cond_5
v2 = this.buffer;
/* iget v3, p0, Lcom/google/gson/stream/JsonReader;->pos:I */
(( java.lang.StringBuilder ) v0 ).append ( v2, v3, v1 ); // invoke-virtual {v0, v2, v3, v1}, Ljava/lang/StringBuilder;->append([CII)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v2 ).toString ( ); // invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
/* .line 1091 */
/* .local v2, "result":Ljava/lang/String; */
} // :goto_2
/* iget v3, p0, Lcom/google/gson/stream/JsonReader;->pos:I */
/* add-int/2addr v3, v1 */
/* iput v3, p0, Lcom/google/gson/stream/JsonReader;->pos:I */
/* .line 1092 */
/* :sswitch_data_0 */
/* .sparse-switch */
/* 0x9 -> :sswitch_1 */
/* 0xa -> :sswitch_1 */
/* 0xc -> :sswitch_1 */
/* 0xd -> :sswitch_1 */
/* 0x20 -> :sswitch_1 */
/* 0x23 -> :sswitch_0 */
/* 0x2c -> :sswitch_1 */
/* 0x2f -> :sswitch_0 */
/* 0x3a -> :sswitch_1 */
/* 0x3b -> :sswitch_0 */
/* 0x3d -> :sswitch_0 */
/* 0x5b -> :sswitch_1 */
/* 0x5c -> :sswitch_0 */
/* 0x5d -> :sswitch_1 */
/* 0x7b -> :sswitch_1 */
/* 0x7d -> :sswitch_1 */
} // .end sparse-switch
} // .end method
private Integer peekKeyword ( ) {
/* .locals 9 */
/* .annotation system Ldalvik/annotation/Throws; */
/* value = { */
/* Ljava/io/IOException; */
/* } */
} // .end annotation
/* .line 599 */
v0 = this.buffer;
/* iget v1, p0, Lcom/google/gson/stream/JsonReader;->pos:I */
/* aget-char v0, v0, v1 */
/* .line 603 */
/* .local v0, "c":C */
/* const/16 v1, 0x74 */
int v2 = 0; // const/4 v2, 0x0
/* if-eq v0, v1, :cond_5 */
/* const/16 v1, 0x54 */
/* if-ne v0, v1, :cond_0 */
/* .line 607 */
} // :cond_0
/* const/16 v1, 0x66 */
/* if-eq v0, v1, :cond_4 */
/* const/16 v1, 0x46 */
/* if-ne v0, v1, :cond_1 */
/* .line 611 */
} // :cond_1
/* const/16 v1, 0x6e */
/* if-eq v0, v1, :cond_3 */
/* const/16 v1, 0x4e */
/* if-ne v0, v1, :cond_2 */
/* .line 616 */
} // :cond_2
/* .line 612 */
} // :cond_3
} // :goto_0
final String v1 = "null"; // const-string v1, "null"
/* .line 613 */
/* .local v1, "keyword":Ljava/lang/String; */
final String v3 = "NULL"; // const-string v3, "NULL"
/* .line 614 */
/* .local v3, "keywordUpper":Ljava/lang/String; */
int v4 = 7; // const/4 v4, 0x7
/* .local v4, "peeking":I */
/* .line 608 */
} // .end local v1 # "keyword":Ljava/lang/String;
} // .end local v3 # "keywordUpper":Ljava/lang/String;
} // .end local v4 # "peeking":I
} // :cond_4
} // :goto_1
final String v1 = "false"; // const-string v1, "false"
/* .line 609 */
/* .restart local v1 # "keyword":Ljava/lang/String; */
final String v3 = "FALSE"; // const-string v3, "FALSE"
/* .line 610 */
/* .restart local v3 # "keywordUpper":Ljava/lang/String; */
int v4 = 6; // const/4 v4, 0x6
/* .restart local v4 # "peeking":I */
/* .line 604 */
} // .end local v1 # "keyword":Ljava/lang/String;
} // .end local v3 # "keywordUpper":Ljava/lang/String;
} // .end local v4 # "peeking":I
} // :cond_5
} // :goto_2
/* const-string/jumbo v1, "true" */
/* .line 605 */
/* .restart local v1 # "keyword":Ljava/lang/String; */
final String v3 = "TRUE"; // const-string v3, "TRUE"
/* .line 606 */
/* .restart local v3 # "keywordUpper":Ljava/lang/String; */
int v4 = 5; // const/4 v4, 0x5
/* .line 620 */
/* .restart local v4 # "peeking":I */
} // :goto_3
v5 = (( java.lang.String ) v1 ).length ( ); // invoke-virtual {v1}, Ljava/lang/String;->length()I
/* .line 621 */
/* .local v5, "length":I */
int v6 = 1; // const/4 v6, 0x1
/* .local v6, "i":I */
} // :goto_4
/* if-ge v6, v5, :cond_8 */
/* .line 622 */
/* iget v7, p0, Lcom/google/gson/stream/JsonReader;->pos:I */
/* add-int/2addr v7, v6 */
/* iget v8, p0, Lcom/google/gson/stream/JsonReader;->limit:I */
/* if-lt v7, v8, :cond_6 */
/* add-int/lit8 v7, v6, 0x1 */
v7 = /* invoke-direct {p0, v7}, Lcom/google/gson/stream/JsonReader;->fillBuffer(I)Z */
/* if-nez v7, :cond_6 */
/* .line 623 */
/* .line 625 */
} // :cond_6
v7 = this.buffer;
/* iget v8, p0, Lcom/google/gson/stream/JsonReader;->pos:I */
/* add-int/2addr v8, v6 */
/* aget-char v0, v7, v8 */
/* .line 626 */
v7 = (( java.lang.String ) v1 ).charAt ( v6 ); // invoke-virtual {v1, v6}, Ljava/lang/String;->charAt(I)C
/* if-eq v0, v7, :cond_7 */
v7 = (( java.lang.String ) v3 ).charAt ( v6 ); // invoke-virtual {v3, v6}, Ljava/lang/String;->charAt(I)C
/* if-eq v0, v7, :cond_7 */
/* .line 627 */
/* .line 621 */
} // :cond_7
/* add-int/lit8 v6, v6, 0x1 */
/* .line 631 */
} // .end local v6 # "i":I
} // :cond_8
/* iget v6, p0, Lcom/google/gson/stream/JsonReader;->pos:I */
/* add-int/2addr v6, v5 */
/* iget v7, p0, Lcom/google/gson/stream/JsonReader;->limit:I */
/* if-lt v6, v7, :cond_9 */
/* add-int/lit8 v6, v5, 0x1 */
v6 = /* invoke-direct {p0, v6}, Lcom/google/gson/stream/JsonReader;->fillBuffer(I)Z */
if ( v6 != null) { // if-eqz v6, :cond_a
} // :cond_9
v6 = this.buffer;
/* iget v7, p0, Lcom/google/gson/stream/JsonReader;->pos:I */
/* add-int/2addr v7, v5 */
/* aget-char v6, v6, v7 */
/* .line 632 */
v6 = /* invoke-direct {p0, v6}, Lcom/google/gson/stream/JsonReader;->isLiteral(C)Z */
if ( v6 != null) { // if-eqz v6, :cond_a
/* .line 633 */
/* .line 637 */
} // :cond_a
/* iget v2, p0, Lcom/google/gson/stream/JsonReader;->pos:I */
/* add-int/2addr v2, v5 */
/* iput v2, p0, Lcom/google/gson/stream/JsonReader;->pos:I */
/* .line 638 */
/* iput v4, p0, Lcom/google/gson/stream/JsonReader;->peeked:I */
} // .end method
private Integer peekNumber ( ) {
/* .locals 18 */
/* .annotation system Ldalvik/annotation/Throws; */
/* value = { */
/* Ljava/io/IOException; */
/* } */
} // .end annotation
/* .line 643 */
/* move-object/from16 v0, p0 */
v1 = this.buffer;
/* .line 644 */
/* .local v1, "buffer":[C */
/* iget v2, v0, Lcom/google/gson/stream/JsonReader;->pos:I */
/* .line 645 */
/* .local v2, "p":I */
/* iget v3, v0, Lcom/google/gson/stream/JsonReader;->limit:I */
/* .line 647 */
/* .local v3, "l":I */
/* const-wide/16 v4, 0x0 */
/* .line 648 */
/* .local v4, "value":J */
int v6 = 0; // const/4 v6, 0x0
/* .line 649 */
/* .local v6, "negative":Z */
int v7 = 1; // const/4 v7, 0x1
/* .line 650 */
/* .local v7, "fitsInLong":Z */
int v8 = 0; // const/4 v8, 0x0
/* .line 652 */
/* .local v8, "last":I */
int v9 = 0; // const/4 v9, 0x0
/* .line 656 */
/* .local v9, "i":I */
} // :goto_0
/* add-int v10, v2, v9 */
int v13 = 4; // const/4 v13, 0x4
int v14 = 2; // const/4 v14, 0x2
int v15 = 0; // const/4 v15, 0x0
/* if-ne v10, v3, :cond_2 */
/* .line 657 */
/* array-length v10, v1 */
/* if-ne v9, v10, :cond_0 */
/* .line 660 */
/* .line 662 */
} // :cond_0
/* add-int/lit8 v10, v9, 0x1 */
v10 = /* invoke-direct {v0, v10}, Lcom/google/gson/stream/JsonReader;->fillBuffer(I)Z */
/* if-nez v10, :cond_1 */
/* .line 663 */
/* move-object/from16 v16, v1 */
/* goto/16 :goto_5 */
/* .line 665 */
} // :cond_1
/* iget v2, v0, Lcom/google/gson/stream/JsonReader;->pos:I */
/* .line 666 */
/* iget v3, v0, Lcom/google/gson/stream/JsonReader;->limit:I */
/* .line 669 */
} // :cond_2
/* add-int v10, v2, v9 */
/* aget-char v10, v1, v10 */
/* .line 670 */
/* .local v10, "c":C */
int v11 = 5; // const/4 v11, 0x5
/* sparse-switch v10, :sswitch_data_0 */
/* .line 705 */
/* const/16 v12, 0x30 */
/* if-lt v10, v12, :cond_13 */
/* const/16 v12, 0x39 */
/* if-le v10, v12, :cond_9 */
/* move-object/from16 v16, v1 */
/* move/from16 v17, v2 */
/* goto/16 :goto_4 */
/* .line 691 */
/* :sswitch_0 */
/* if-eq v8, v14, :cond_4 */
/* if-ne v8, v13, :cond_3 */
/* .line 695 */
} // :cond_3
/* .line 692 */
} // :cond_4
} // :goto_1
int v8 = 5; // const/4 v8, 0x5
/* .line 693 */
/* move-object/from16 v16, v1 */
/* move/from16 v17, v2 */
/* goto/16 :goto_3 */
/* .line 698 */
/* :sswitch_1 */
/* if-ne v8, v14, :cond_5 */
/* .line 699 */
int v8 = 3; // const/4 v8, 0x3
/* .line 700 */
/* move-object/from16 v16, v1 */
/* move/from16 v17, v2 */
/* goto/16 :goto_3 */
/* .line 702 */
} // :cond_5
/* .line 672 */
/* :sswitch_2 */
/* if-nez v8, :cond_6 */
/* .line 673 */
int v6 = 1; // const/4 v6, 0x1
/* .line 674 */
int v8 = 1; // const/4 v8, 0x1
/* .line 675 */
/* move-object/from16 v16, v1 */
/* move/from16 v17, v2 */
/* goto/16 :goto_3 */
/* .line 676 */
} // :cond_6
/* if-ne v8, v11, :cond_7 */
/* .line 677 */
int v8 = 6; // const/4 v8, 0x6
/* .line 678 */
/* move-object/from16 v16, v1 */
/* move/from16 v17, v2 */
/* goto/16 :goto_3 */
/* .line 680 */
} // :cond_7
/* .line 683 */
/* :sswitch_3 */
/* if-ne v8, v11, :cond_8 */
/* .line 684 */
int v8 = 6; // const/4 v8, 0x6
/* .line 685 */
/* move-object/from16 v16, v1 */
/* move/from16 v17, v2 */
/* .line 687 */
} // :cond_8
/* .line 711 */
} // :cond_9
int v12 = 1; // const/4 v12, 0x1
/* if-eq v8, v12, :cond_11 */
/* if-nez v8, :cond_a */
/* move-object/from16 v16, v1 */
/* move/from16 v17, v2 */
/* .line 714 */
} // :cond_a
/* if-ne v8, v14, :cond_e */
/* .line 715 */
/* const-wide/16 v13, 0x0 */
/* cmp-long v11, v4, v13 */
/* if-nez v11, :cond_b */
/* .line 716 */
/* .line 718 */
} // :cond_b
/* const-wide/16 v13, 0xa */
/* mul-long/2addr v13, v4 */
/* add-int/lit8 v11, v10, -0x30 */
/* move-object/from16 v16, v1 */
/* move/from16 v17, v2 */
} // .end local v1 # "buffer":[C
} // .end local v2 # "p":I
/* .local v16, "buffer":[C */
/* .local v17, "p":I */
/* int-to-long v1, v11 */
/* sub-long/2addr v13, v1 */
/* .line 719 */
/* .local v13, "newValue":J */
/* const-wide v1, -0xcccccccccccccccL */
/* cmp-long v11, v4, v1 */
/* if-gtz v11, :cond_c */
/* cmp-long v1, v4, v1 */
/* if-nez v1, :cond_d */
/* cmp-long v1, v13, v4 */
/* if-gez v1, :cond_d */
} // :cond_c
/* move v15, v12 */
} // :cond_d
/* and-int v1, v7, v15 */
/* .line 721 */
} // .end local v7 # "fitsInLong":Z
/* .local v1, "fitsInLong":Z */
/* move-wide v4, v13 */
/* .line 722 */
} // .end local v13 # "newValue":J
/* move v7, v1 */
} // .end local v16 # "buffer":[C
} // .end local v17 # "p":I
/* .local v1, "buffer":[C */
/* .restart local v2 # "p":I */
/* .restart local v7 # "fitsInLong":Z */
} // :cond_e
/* move-object/from16 v16, v1 */
/* move/from16 v17, v2 */
} // .end local v1 # "buffer":[C
} // .end local v2 # "p":I
/* .restart local v16 # "buffer":[C */
/* .restart local v17 # "p":I */
int v1 = 3; // const/4 v1, 0x3
/* if-ne v8, v1, :cond_f */
/* .line 723 */
int v1 = 4; // const/4 v1, 0x4
/* move v8, v1 */
} // .end local v8 # "last":I
/* .local v1, "last":I */
/* .line 724 */
} // .end local v1 # "last":I
/* .restart local v8 # "last":I */
} // :cond_f
/* if-eq v8, v11, :cond_10 */
int v1 = 6; // const/4 v1, 0x6
/* if-ne v8, v1, :cond_12 */
/* .line 725 */
} // :cond_10
int v1 = 7; // const/4 v1, 0x7
/* move v8, v1 */
} // .end local v8 # "last":I
/* .restart local v1 # "last":I */
/* .line 711 */
} // .end local v16 # "buffer":[C
} // .end local v17 # "p":I
/* .local v1, "buffer":[C */
/* .restart local v2 # "p":I */
/* .restart local v8 # "last":I */
} // :cond_11
/* move-object/from16 v16, v1 */
/* move/from16 v17, v2 */
/* .line 712 */
} // .end local v1 # "buffer":[C
} // .end local v2 # "p":I
/* .restart local v16 # "buffer":[C */
/* .restart local v17 # "p":I */
} // :goto_2
/* add-int/lit8 v1, v10, -0x30 */
/* neg-int v1, v1 */
/* int-to-long v1, v1 */
/* .line 713 */
} // .end local v4 # "value":J
/* .local v1, "value":J */
int v4 = 2; // const/4 v4, 0x2
/* move v8, v4 */
/* move-wide v4, v1 */
/* .line 655 */
} // .end local v1 # "value":J
} // .end local v10 # "c":C
/* .restart local v4 # "value":J */
} // :cond_12
} // :goto_3
/* add-int/lit8 v9, v9, 0x1 */
/* move-object/from16 v1, v16 */
/* move/from16 v2, v17 */
/* goto/16 :goto_0 */
/* .line 705 */
} // .end local v16 # "buffer":[C
} // .end local v17 # "p":I
/* .local v1, "buffer":[C */
/* .restart local v2 # "p":I */
/* .restart local v10 # "c":C */
} // :cond_13
/* move-object/from16 v16, v1 */
/* move/from16 v17, v2 */
/* .line 706 */
} // .end local v1 # "buffer":[C
} // .end local v2 # "p":I
/* .restart local v16 # "buffer":[C */
/* .restart local v17 # "p":I */
} // :goto_4
v1 = /* invoke-direct {v0, v10}, Lcom/google/gson/stream/JsonReader;->isLiteral(C)Z */
/* if-nez v1, :cond_1a */
/* .line 707 */
/* move/from16 v2, v17 */
/* .line 731 */
} // .end local v10 # "c":C
} // .end local v17 # "p":I
/* .restart local v2 # "p":I */
} // :goto_5
/* if-ne v8, v14, :cond_17 */
if ( v7 != null) { // if-eqz v7, :cond_17
/* const-wide/high16 v10, -0x8000000000000000L */
/* cmp-long v1, v4, v10 */
/* if-nez v1, :cond_14 */
if ( v6 != null) { // if-eqz v6, :cond_17
} // :cond_14
/* const-wide/16 v10, 0x0 */
/* cmp-long v1, v4, v10 */
/* if-nez v1, :cond_15 */
/* if-nez v6, :cond_17 */
/* .line 732 */
} // :cond_15
if ( v6 != null) { // if-eqz v6, :cond_16
/* move-wide v10, v4 */
} // :cond_16
/* neg-long v10, v4 */
} // :goto_6
/* iput-wide v10, v0, Lcom/google/gson/stream/JsonReader;->peekedLong:J */
/* .line 733 */
/* iget v1, v0, Lcom/google/gson/stream/JsonReader;->pos:I */
/* add-int/2addr v1, v9 */
/* iput v1, v0, Lcom/google/gson/stream/JsonReader;->pos:I */
/* .line 734 */
/* const/16 v1, 0xf */
/* iput v1, v0, Lcom/google/gson/stream/JsonReader;->peeked:I */
/* .line 735 */
} // :cond_17
/* if-eq v8, v14, :cond_19 */
/* if-eq v8, v13, :cond_19 */
int v1 = 7; // const/4 v1, 0x7
/* if-ne v8, v1, :cond_18 */
/* .line 740 */
} // :cond_18
/* .line 737 */
} // :cond_19
} // :goto_7
/* iput v9, v0, Lcom/google/gson/stream/JsonReader;->peekedNumberLength:I */
/* .line 738 */
/* const/16 v1, 0x10 */
/* iput v1, v0, Lcom/google/gson/stream/JsonReader;->peeked:I */
/* .line 709 */
} // .end local v2 # "p":I
/* .restart local v10 # "c":C */
/* .restart local v17 # "p":I */
} // :cond_1a
/* nop */
/* :sswitch_data_0 */
/* .sparse-switch */
/* 0x2b -> :sswitch_3 */
/* 0x2d -> :sswitch_2 */
/* 0x2e -> :sswitch_1 */
/* 0x45 -> :sswitch_0 */
/* 0x65 -> :sswitch_0 */
} // .end sparse-switch
} // .end method
private void push ( Integer p0 ) {
/* .locals 3 */
/* .param p1, "newTop" # I */
/* .line 1264 */
/* iget v0, p0, Lcom/google/gson/stream/JsonReader;->stackSize:I */
v1 = this.stack;
/* array-length v2, v1 */
/* if-ne v0, v2, :cond_0 */
/* .line 1265 */
/* mul-int/lit8 v0, v0, 0x2 */
/* .line 1266 */
/* .local v0, "newLength":I */
java.util.Arrays .copyOf ( v1,v0 );
this.stack = v1;
/* .line 1267 */
v1 = this.pathIndices;
java.util.Arrays .copyOf ( v1,v0 );
this.pathIndices = v1;
/* .line 1268 */
v1 = this.pathNames;
java.util.Arrays .copyOf ( v1,v0 );
/* check-cast v1, [Ljava/lang/String; */
this.pathNames = v1;
/* .line 1270 */
} // .end local v0 # "newLength":I
} // :cond_0
v0 = this.stack;
/* iget v1, p0, Lcom/google/gson/stream/JsonReader;->stackSize:I */
/* add-int/lit8 v2, v1, 0x1 */
/* iput v2, p0, Lcom/google/gson/stream/JsonReader;->stackSize:I */
/* aput p1, v0, v1 */
/* .line 1271 */
return;
} // .end method
private Object readEscapeCharacter ( ) {
/* .locals 11 */
/* .annotation system Ldalvik/annotation/Throws; */
/* value = { */
/* Ljava/io/IOException; */
/* } */
} // .end annotation
/* .line 1533 */
/* iget v0, p0, Lcom/google/gson/stream/JsonReader;->pos:I */
/* iget v1, p0, Lcom/google/gson/stream/JsonReader;->limit:I */
final String v2 = "Unterminated escape sequence"; // const-string v2, "Unterminated escape sequence"
int v3 = 1; // const/4 v3, 0x1
/* if-ne v0, v1, :cond_1 */
v0 = /* invoke-direct {p0, v3}, Lcom/google/gson/stream/JsonReader;->fillBuffer(I)Z */
if ( v0 != null) { // if-eqz v0, :cond_0
/* .line 1534 */
} // :cond_0
/* invoke-direct {p0, v2}, Lcom/google/gson/stream/JsonReader;->syntaxError(Ljava/lang/String;)Ljava/io/IOException; */
/* throw v0 */
/* .line 1537 */
} // :cond_1
} // :goto_0
v0 = this.buffer;
/* iget v1, p0, Lcom/google/gson/stream/JsonReader;->pos:I */
/* add-int/lit8 v4, v1, 0x1 */
/* iput v4, p0, Lcom/google/gson/stream/JsonReader;->pos:I */
/* aget-char v0, v0, v1 */
/* .line 1538 */
/* .local v0, "escaped":C */
/* const/16 v1, 0xa */
/* sparse-switch v0, :sswitch_data_0 */
/* .line 1588 */
final String v1 = "Invalid escape sequence"; // const-string v1, "Invalid escape sequence"
/* invoke-direct {p0, v1}, Lcom/google/gson/stream/JsonReader;->syntaxError(Ljava/lang/String;)Ljava/io/IOException; */
/* throw v1 */
/* .line 1540 */
/* :sswitch_0 */
int v3 = 4; // const/4 v3, 0x4
/* add-int/2addr v4, v3 */
/* iget v5, p0, Lcom/google/gson/stream/JsonReader;->limit:I */
/* if-le v4, v5, :cond_3 */
v4 = /* invoke-direct {p0, v3}, Lcom/google/gson/stream/JsonReader;->fillBuffer(I)Z */
if ( v4 != null) { // if-eqz v4, :cond_2
/* .line 1541 */
} // :cond_2
/* invoke-direct {p0, v2}, Lcom/google/gson/stream/JsonReader;->syntaxError(Ljava/lang/String;)Ljava/io/IOException; */
/* throw v1 */
/* .line 1544 */
} // :cond_3
} // :goto_1
int v2 = 0; // const/4 v2, 0x0
/* .line 1545 */
/* .local v2, "result":C */
/* iget v4, p0, Lcom/google/gson/stream/JsonReader;->pos:I */
/* .local v4, "i":I */
/* add-int/lit8 v5, v4, 0x4 */
/* .local v5, "end":I */
} // :goto_2
/* if-ge v4, v5, :cond_7 */
/* .line 1546 */
v6 = this.buffer;
/* aget-char v6, v6, v4 */
/* .line 1547 */
/* .local v6, "c":C */
/* shl-int/lit8 v7, v2, 0x4 */
/* int-to-char v2, v7 */
/* .line 1548 */
/* const/16 v7, 0x30 */
/* if-lt v6, v7, :cond_4 */
/* const/16 v7, 0x39 */
/* if-gt v6, v7, :cond_4 */
/* .line 1549 */
/* add-int/lit8 v7, v6, -0x30 */
/* add-int/2addr v7, v2 */
/* int-to-char v2, v7 */
/* .line 1550 */
} // :cond_4
/* const/16 v7, 0x61 */
/* if-lt v6, v7, :cond_5 */
/* const/16 v7, 0x66 */
/* if-gt v6, v7, :cond_5 */
/* .line 1551 */
/* add-int/lit8 v7, v6, -0x61 */
/* add-int/2addr v7, v1 */
/* add-int/2addr v7, v2 */
/* int-to-char v2, v7 */
/* .line 1552 */
} // :cond_5
/* const/16 v7, 0x41 */
/* if-lt v6, v7, :cond_6 */
/* const/16 v7, 0x46 */
/* if-gt v6, v7, :cond_6 */
/* .line 1553 */
/* add-int/lit8 v7, v6, -0x41 */
/* add-int/2addr v7, v1 */
/* add-int/2addr v7, v2 */
/* int-to-char v2, v7 */
/* .line 1545 */
} // .end local v6 # "c":C
} // :goto_3
/* add-int/lit8 v4, v4, 0x1 */
/* .line 1555 */
/* .restart local v6 # "c":C */
} // :cond_6
/* new-instance v1, Ljava/lang/NumberFormatException; */
/* new-instance v7, Ljava/lang/StringBuilder; */
/* invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V */
final String v8 = "\\u"; // const-string v8, "\\u"
(( java.lang.StringBuilder ) v7 ).append ( v8 ); // invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
/* new-instance v8, Ljava/lang/String; */
v9 = this.buffer;
/* iget v10, p0, Lcom/google/gson/stream/JsonReader;->pos:I */
/* invoke-direct {v8, v9, v10, v3}, Ljava/lang/String;-><init>([CII)V */
(( java.lang.StringBuilder ) v7 ).append ( v8 ); // invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v3 ).toString ( ); // invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
/* invoke-direct {v1, v3}, Ljava/lang/NumberFormatException;-><init>(Ljava/lang/String;)V */
/* throw v1 */
/* .line 1558 */
} // .end local v4 # "i":I
} // .end local v5 # "end":I
} // .end local v6 # "c":C
} // :cond_7
/* iget v1, p0, Lcom/google/gson/stream/JsonReader;->pos:I */
/* add-int/2addr v1, v3 */
/* iput v1, p0, Lcom/google/gson/stream/JsonReader;->pos:I */
/* .line 1559 */
/* .line 1562 */
} // .end local v2 # "result":C
/* :sswitch_1 */
/* const/16 v1, 0x9 */
/* .line 1571 */
/* :sswitch_2 */
/* const/16 v1, 0xd */
/* .line 1568 */
/* :sswitch_3 */
/* .line 1574 */
/* :sswitch_4 */
/* const/16 v1, 0xc */
/* .line 1565 */
/* :sswitch_5 */
/* const/16 v1, 0x8 */
/* .line 1577 */
/* :sswitch_6 */
/* iget v1, p0, Lcom/google/gson/stream/JsonReader;->lineNumber:I */
/* add-int/2addr v1, v3 */
/* iput v1, p0, Lcom/google/gson/stream/JsonReader;->lineNumber:I */
/* .line 1578 */
/* iput v4, p0, Lcom/google/gson/stream/JsonReader;->lineStart:I */
/* .line 1585 */
/* :sswitch_7 */
/* nop */
/* :sswitch_data_0 */
/* .sparse-switch */
/* 0xa -> :sswitch_6 */
/* 0x22 -> :sswitch_7 */
/* 0x27 -> :sswitch_7 */
/* 0x2f -> :sswitch_7 */
/* 0x5c -> :sswitch_7 */
/* 0x62 -> :sswitch_5 */
/* 0x66 -> :sswitch_4 */
/* 0x6e -> :sswitch_3 */
/* 0x72 -> :sswitch_2 */
/* 0x74 -> :sswitch_1 */
/* 0x75 -> :sswitch_0 */
} // .end sparse-switch
} // .end method
private void skipQuotedValue ( Object p0 ) {
/* .locals 6 */
/* .param p1, "quote" # C */
/* .annotation system Ldalvik/annotation/Throws; */
/* value = { */
/* Ljava/io/IOException; */
/* } */
} // .end annotation
/* .line 1097 */
v0 = this.buffer;
/* .line 1099 */
/* .local v0, "buffer":[C */
} // :goto_0
/* iget v1, p0, Lcom/google/gson/stream/JsonReader;->pos:I */
/* .line 1100 */
/* .local v1, "p":I */
/* iget v2, p0, Lcom/google/gson/stream/JsonReader;->limit:I */
/* .line 1102 */
/* .local v2, "l":I */
} // :goto_1
int v3 = 1; // const/4 v3, 0x1
/* if-ge v1, v2, :cond_3 */
/* .line 1103 */
/* add-int/lit8 v4, v1, 0x1 */
} // .end local v1 # "p":I
/* .local v4, "p":I */
/* aget-char v1, v0, v1 */
/* .line 1104 */
/* .local v1, "c":I */
/* if-ne v1, p1, :cond_0 */
/* .line 1105 */
/* iput v4, p0, Lcom/google/gson/stream/JsonReader;->pos:I */
/* .line 1106 */
return;
/* .line 1107 */
} // :cond_0
/* const/16 v5, 0x5c */
/* if-ne v1, v5, :cond_1 */
/* .line 1108 */
/* iput v4, p0, Lcom/google/gson/stream/JsonReader;->pos:I */
/* .line 1109 */
/* invoke-direct {p0}, Lcom/google/gson/stream/JsonReader;->readEscapeCharacter()C */
/* .line 1110 */
/* iget v3, p0, Lcom/google/gson/stream/JsonReader;->pos:I */
/* .line 1111 */
} // .end local v4 # "p":I
/* .local v3, "p":I */
/* iget v2, p0, Lcom/google/gson/stream/JsonReader;->limit:I */
/* move v1, v3 */
/* .line 1112 */
} // .end local v3 # "p":I
/* .restart local v4 # "p":I */
} // :cond_1
/* const/16 v5, 0xa */
/* if-ne v1, v5, :cond_2 */
/* .line 1113 */
/* iget v5, p0, Lcom/google/gson/stream/JsonReader;->lineNumber:I */
/* add-int/2addr v5, v3 */
/* iput v5, p0, Lcom/google/gson/stream/JsonReader;->lineNumber:I */
/* .line 1114 */
/* iput v4, p0, Lcom/google/gson/stream/JsonReader;->lineStart:I */
/* .line 1116 */
} // .end local v1 # "c":I
} // :cond_2
/* move v1, v4 */
} // .end local v4 # "p":I
/* .local v1, "p":I */
} // :goto_2
/* .line 1117 */
} // :cond_3
/* iput v1, p0, Lcom/google/gson/stream/JsonReader;->pos:I */
/* .line 1118 */
} // .end local v1 # "p":I
} // .end local v2 # "l":I
v1 = /* invoke-direct {p0, v3}, Lcom/google/gson/stream/JsonReader;->fillBuffer(I)Z */
if ( v1 != null) { // if-eqz v1, :cond_4
/* .line 1119 */
} // :cond_4
final String v1 = "Unterminated string"; // const-string v1, "Unterminated string"
/* invoke-direct {p0, v1}, Lcom/google/gson/stream/JsonReader;->syntaxError(Ljava/lang/String;)Ljava/io/IOException; */
/* throw v1 */
} // .end method
private Boolean skipTo ( java.lang.String p0 ) {
/* .locals 5 */
/* .param p1, "toFind" # Ljava/lang/String; */
/* .annotation system Ldalvik/annotation/Throws; */
/* value = { */
/* Ljava/io/IOException; */
/* } */
} // .end annotation
/* .line 1430 */
v0 = (( java.lang.String ) p1 ).length ( ); // invoke-virtual {p1}, Ljava/lang/String;->length()I
/* .line 1432 */
/* .local v0, "length":I */
} // :goto_0
/* iget v1, p0, Lcom/google/gson/stream/JsonReader;->pos:I */
/* add-int/2addr v1, v0 */
/* iget v2, p0, Lcom/google/gson/stream/JsonReader;->limit:I */
/* if-le v1, v2, :cond_1 */
v1 = /* invoke-direct {p0, v0}, Lcom/google/gson/stream/JsonReader;->fillBuffer(I)Z */
if ( v1 != null) { // if-eqz v1, :cond_0
/* .line 1445 */
} // :cond_0
int v1 = 0; // const/4 v1, 0x0
/* .line 1433 */
} // :cond_1
} // :goto_1
v1 = this.buffer;
/* iget v2, p0, Lcom/google/gson/stream/JsonReader;->pos:I */
/* aget-char v1, v1, v2 */
/* const/16 v3, 0xa */
int v4 = 1; // const/4 v4, 0x1
/* if-ne v1, v3, :cond_2 */
/* .line 1434 */
/* iget v1, p0, Lcom/google/gson/stream/JsonReader;->lineNumber:I */
/* add-int/2addr v1, v4 */
/* iput v1, p0, Lcom/google/gson/stream/JsonReader;->lineNumber:I */
/* .line 1435 */
/* add-int/lit8 v2, v2, 0x1 */
/* iput v2, p0, Lcom/google/gson/stream/JsonReader;->lineStart:I */
/* .line 1436 */
/* .line 1438 */
} // :cond_2
int v1 = 0; // const/4 v1, 0x0
/* .local v1, "c":I */
} // :goto_2
/* if-ge v1, v0, :cond_4 */
/* .line 1439 */
v2 = this.buffer;
/* iget v3, p0, Lcom/google/gson/stream/JsonReader;->pos:I */
/* add-int/2addr v3, v1 */
/* aget-char v2, v2, v3 */
v3 = (( java.lang.String ) p1 ).charAt ( v1 ); // invoke-virtual {p1, v1}, Ljava/lang/String;->charAt(I)C
/* if-eq v2, v3, :cond_3 */
/* .line 1440 */
/* nop */
/* .line 1432 */
} // .end local v1 # "c":I
} // :goto_3
/* iget v1, p0, Lcom/google/gson/stream/JsonReader;->pos:I */
/* add-int/2addr v1, v4 */
/* iput v1, p0, Lcom/google/gson/stream/JsonReader;->pos:I */
/* .line 1438 */
/* .restart local v1 # "c":I */
} // :cond_3
/* add-int/lit8 v1, v1, 0x1 */
/* .line 1443 */
} // .end local v1 # "c":I
} // :cond_4
} // .end method
private void skipToEndOfLine ( ) {
/* .locals 4 */
/* .annotation system Ldalvik/annotation/Throws; */
/* value = { */
/* Ljava/io/IOException; */
/* } */
} // .end annotation
/* .line 1414 */
/* nop */
} // :goto_0
/* iget v0, p0, Lcom/google/gson/stream/JsonReader;->pos:I */
/* iget v1, p0, Lcom/google/gson/stream/JsonReader;->limit:I */
int v2 = 1; // const/4 v2, 0x1
/* if-lt v0, v1, :cond_0 */
v0 = /* invoke-direct {p0, v2}, Lcom/google/gson/stream/JsonReader;->fillBuffer(I)Z */
if ( v0 != null) { // if-eqz v0, :cond_2
/* .line 1415 */
} // :cond_0
v0 = this.buffer;
/* iget v1, p0, Lcom/google/gson/stream/JsonReader;->pos:I */
/* add-int/lit8 v3, v1, 0x1 */
/* iput v3, p0, Lcom/google/gson/stream/JsonReader;->pos:I */
/* aget-char v0, v0, v1 */
/* .line 1416 */
/* .local v0, "c":C */
/* const/16 v1, 0xa */
/* if-ne v0, v1, :cond_1 */
/* .line 1417 */
/* iget v1, p0, Lcom/google/gson/stream/JsonReader;->lineNumber:I */
/* add-int/2addr v1, v2 */
/* iput v1, p0, Lcom/google/gson/stream/JsonReader;->lineNumber:I */
/* .line 1418 */
/* iput v3, p0, Lcom/google/gson/stream/JsonReader;->lineStart:I */
/* .line 1419 */
/* .line 1420 */
} // :cond_1
/* const/16 v1, 0xd */
/* if-ne v0, v1, :cond_3 */
/* .line 1421 */
/* nop */
/* .line 1424 */
} // .end local v0 # "c":C
} // :cond_2
} // :goto_1
return;
/* .line 1423 */
} // :cond_3
} // .end method
private void skipUnquotedValue ( ) {
/* .locals 4 */
/* .annotation system Ldalvik/annotation/Throws; */
/* value = { */
/* Ljava/io/IOException; */
/* } */
} // .end annotation
/* .line 1124 */
/* nop */
} // :cond_0
int v0 = 0; // const/4 v0, 0x0
/* .line 1125 */
/* .local v0, "i":I */
} // :goto_0
/* iget v1, p0, Lcom/google/gson/stream/JsonReader;->pos:I */
/* add-int v2, v1, v0 */
/* iget v3, p0, Lcom/google/gson/stream/JsonReader;->limit:I */
/* if-ge v2, v3, :cond_1 */
/* .line 1126 */
v2 = this.buffer;
/* add-int/2addr v1, v0 */
/* aget-char v1, v2, v1 */
/* sparse-switch v1, :sswitch_data_0 */
/* .line 1125 */
/* add-int/lit8 v0, v0, 0x1 */
/* .line 1132 */
/* :sswitch_0 */
/* invoke-direct {p0}, Lcom/google/gson/stream/JsonReader;->checkLenient()V */
/* .line 1144 */
/* :sswitch_1 */
/* iget v1, p0, Lcom/google/gson/stream/JsonReader;->pos:I */
/* add-int/2addr v1, v0 */
/* iput v1, p0, Lcom/google/gson/stream/JsonReader;->pos:I */
/* .line 1145 */
return;
/* .line 1148 */
} // :cond_1
/* add-int/2addr v1, v0 */
/* iput v1, p0, Lcom/google/gson/stream/JsonReader;->pos:I */
/* .line 1149 */
} // .end local v0 # "i":I
int v0 = 1; // const/4 v0, 0x1
v0 = /* invoke-direct {p0, v0}, Lcom/google/gson/stream/JsonReader;->fillBuffer(I)Z */
/* if-nez v0, :cond_0 */
/* .line 1150 */
return;
/* nop */
/* :sswitch_data_0 */
/* .sparse-switch */
/* 0x9 -> :sswitch_1 */
/* 0xa -> :sswitch_1 */
/* 0xc -> :sswitch_1 */
/* 0xd -> :sswitch_1 */
/* 0x20 -> :sswitch_1 */
/* 0x23 -> :sswitch_0 */
/* 0x2c -> :sswitch_1 */
/* 0x2f -> :sswitch_0 */
/* 0x3a -> :sswitch_1 */
/* 0x3b -> :sswitch_0 */
/* 0x3d -> :sswitch_0 */
/* 0x5b -> :sswitch_1 */
/* 0x5c -> :sswitch_0 */
/* 0x5d -> :sswitch_1 */
/* 0x7b -> :sswitch_1 */
/* 0x7d -> :sswitch_1 */
} // .end sparse-switch
} // .end method
private java.io.IOException syntaxError ( java.lang.String p0 ) {
/* .locals 3 */
/* .param p1, "message" # Ljava/lang/String; */
/* .annotation system Ldalvik/annotation/Throws; */
/* value = { */
/* Ljava/io/IOException; */
/* } */
} // .end annotation
/* .line 1597 */
/* new-instance v0, Lcom/google/gson/stream/MalformedJsonException; */
/* new-instance v1, Ljava/lang/StringBuilder; */
/* invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V */
(( java.lang.StringBuilder ) v1 ).append ( p1 ); // invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( com.google.gson.stream.JsonReader ) p0 ).locationString ( ); // invoke-virtual {p0}, Lcom/google/gson/stream/JsonReader;->locationString()Ljava/lang/String;
(( java.lang.StringBuilder ) v1 ).append ( v2 ); // invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v1 ).toString ( ); // invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
/* invoke-direct {v0, v1}, Lcom/google/gson/stream/MalformedJsonException;-><init>(Ljava/lang/String;)V */
/* throw v0 */
} // .end method
/* # virtual methods */
public void beginArray ( ) {
/* .locals 4 */
/* .annotation system Ldalvik/annotation/Throws; */
/* value = { */
/* Ljava/io/IOException; */
/* } */
} // .end annotation
/* .line 341 */
/* iget v0, p0, Lcom/google/gson/stream/JsonReader;->peeked:I */
/* .line 342 */
/* .local v0, "p":I */
/* if-nez v0, :cond_0 */
/* .line 343 */
v0 = (( com.google.gson.stream.JsonReader ) p0 ).doPeek ( ); // invoke-virtual {p0}, Lcom/google/gson/stream/JsonReader;->doPeek()I
/* .line 345 */
} // :cond_0
int v1 = 3; // const/4 v1, 0x3
/* if-ne v0, v1, :cond_1 */
/* .line 346 */
int v1 = 1; // const/4 v1, 0x1
/* invoke-direct {p0, v1}, Lcom/google/gson/stream/JsonReader;->push(I)V */
/* .line 347 */
v2 = this.pathIndices;
/* iget v3, p0, Lcom/google/gson/stream/JsonReader;->stackSize:I */
/* sub-int/2addr v3, v1 */
int v1 = 0; // const/4 v1, 0x0
/* aput v1, v2, v3 */
/* .line 348 */
/* iput v1, p0, Lcom/google/gson/stream/JsonReader;->peeked:I */
/* .line 352 */
return;
/* .line 350 */
} // :cond_1
/* new-instance v1, Ljava/lang/IllegalStateException; */
/* new-instance v2, Ljava/lang/StringBuilder; */
/* invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V */
final String v3 = "Expected BEGIN_ARRAY but was "; // const-string v3, "Expected BEGIN_ARRAY but was "
(( java.lang.StringBuilder ) v2 ).append ( v3 ); // invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( com.google.gson.stream.JsonReader ) p0 ).peek ( ); // invoke-virtual {p0}, Lcom/google/gson/stream/JsonReader;->peek()Lcom/google/gson/stream/JsonToken;
(( java.lang.StringBuilder ) v2 ).append ( v3 ); // invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;
(( com.google.gson.stream.JsonReader ) p0 ).locationString ( ); // invoke-virtual {p0}, Lcom/google/gson/stream/JsonReader;->locationString()Ljava/lang/String;
(( java.lang.StringBuilder ) v2 ).append ( v3 ); // invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v2 ).toString ( ); // invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
/* invoke-direct {v1, v2}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V */
/* throw v1 */
} // .end method
public void beginObject ( ) {
/* .locals 4 */
/* .annotation system Ldalvik/annotation/Throws; */
/* value = { */
/* Ljava/io/IOException; */
/* } */
} // .end annotation
/* .line 377 */
/* iget v0, p0, Lcom/google/gson/stream/JsonReader;->peeked:I */
/* .line 378 */
/* .local v0, "p":I */
/* if-nez v0, :cond_0 */
/* .line 379 */
v0 = (( com.google.gson.stream.JsonReader ) p0 ).doPeek ( ); // invoke-virtual {p0}, Lcom/google/gson/stream/JsonReader;->doPeek()I
/* .line 381 */
} // :cond_0
int v1 = 1; // const/4 v1, 0x1
/* if-ne v0, v1, :cond_1 */
/* .line 382 */
int v1 = 3; // const/4 v1, 0x3
/* invoke-direct {p0, v1}, Lcom/google/gson/stream/JsonReader;->push(I)V */
/* .line 383 */
int v1 = 0; // const/4 v1, 0x0
/* iput v1, p0, Lcom/google/gson/stream/JsonReader;->peeked:I */
/* .line 387 */
return;
/* .line 385 */
} // :cond_1
/* new-instance v1, Ljava/lang/IllegalStateException; */
/* new-instance v2, Ljava/lang/StringBuilder; */
/* invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V */
final String v3 = "Expected BEGIN_OBJECT but was "; // const-string v3, "Expected BEGIN_OBJECT but was "
(( java.lang.StringBuilder ) v2 ).append ( v3 ); // invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( com.google.gson.stream.JsonReader ) p0 ).peek ( ); // invoke-virtual {p0}, Lcom/google/gson/stream/JsonReader;->peek()Lcom/google/gson/stream/JsonToken;
(( java.lang.StringBuilder ) v2 ).append ( v3 ); // invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;
(( com.google.gson.stream.JsonReader ) p0 ).locationString ( ); // invoke-virtual {p0}, Lcom/google/gson/stream/JsonReader;->locationString()Ljava/lang/String;
(( java.lang.StringBuilder ) v2 ).append ( v3 ); // invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v2 ).toString ( ); // invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
/* invoke-direct {v1, v2}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V */
/* throw v1 */
} // .end method
public void close ( ) {
/* .locals 3 */
/* .annotation system Ldalvik/annotation/Throws; */
/* value = { */
/* Ljava/io/IOException; */
/* } */
} // .end annotation
/* .line 1216 */
int v0 = 0; // const/4 v0, 0x0
/* iput v0, p0, Lcom/google/gson/stream/JsonReader;->peeked:I */
/* .line 1217 */
v1 = this.stack;
/* const/16 v2, 0x8 */
/* aput v2, v1, v0 */
/* .line 1218 */
int v0 = 1; // const/4 v0, 0x1
/* iput v0, p0, Lcom/google/gson/stream/JsonReader;->stackSize:I */
/* .line 1219 */
v0 = this.in;
(( java.io.Reader ) v0 ).close ( ); // invoke-virtual {v0}, Ljava/io/Reader;->close()V
/* .line 1220 */
return;
} // .end method
Integer doPeek ( ) {
/* .locals 11 */
/* .annotation system Ldalvik/annotation/Throws; */
/* value = { */
/* Ljava/io/IOException; */
/* } */
} // .end annotation
/* .line 462 */
v0 = this.stack;
/* iget v1, p0, Lcom/google/gson/stream/JsonReader;->stackSize:I */
/* add-int/lit8 v2, v1, -0x1 */
/* aget v2, v0, v2 */
/* .line 463 */
/* .local v2, "peekStack":I */
/* const/16 v3, 0x8 */
int v4 = 3; // const/4 v4, 0x3
int v5 = 7; // const/4 v5, 0x7
int v6 = 4; // const/4 v6, 0x4
int v7 = 2; // const/4 v7, 0x2
int v8 = 1; // const/4 v8, 0x1
/* if-ne v2, v8, :cond_0 */
/* .line 464 */
/* sub-int/2addr v1, v8 */
/* aput v7, v0, v1 */
/* goto/16 :goto_2 */
/* .line 465 */
} // :cond_0
/* if-ne v2, v7, :cond_1 */
/* .line 467 */
v0 = /* invoke-direct {p0, v8}, Lcom/google/gson/stream/JsonReader;->nextNonWhitespace(Z)I */
/* .line 468 */
/* .local v0, "c":I */
/* sparse-switch v0, :sswitch_data_0 */
/* .line 476 */
final String v1 = "Unterminated array"; // const-string v1, "Unterminated array"
/* invoke-direct {p0, v1}, Lcom/google/gson/stream/JsonReader;->syntaxError(Ljava/lang/String;)Ljava/io/IOException; */
/* throw v1 */
/* .line 470 */
/* :sswitch_0 */
/* iput v6, p0, Lcom/google/gson/stream/JsonReader;->peeked:I */
/* .line 472 */
/* :sswitch_1 */
/* invoke-direct {p0}, Lcom/google/gson/stream/JsonReader;->checkLenient()V */
/* .line 474 */
/* :sswitch_2 */
/* nop */
/* .line 478 */
} // .end local v0 # "c":I
} // :cond_1
int v9 = 5; // const/4 v9, 0x5
/* if-eq v2, v4, :cond_11 */
/* if-ne v2, v9, :cond_2 */
/* goto/16 :goto_4 */
/* .line 516 */
} // :cond_2
/* if-ne v2, v6, :cond_5 */
/* .line 517 */
/* sub-int/2addr v1, v8 */
/* aput v9, v0, v1 */
/* .line 519 */
v0 = /* invoke-direct {p0, v8}, Lcom/google/gson/stream/JsonReader;->nextNonWhitespace(Z)I */
/* .line 520 */
/* .restart local v0 # "c":I */
/* sparse-switch v0, :sswitch_data_1 */
/* .line 530 */
final String v1 = "Expected \':\'"; // const-string v1, "Expected \':\'"
/* invoke-direct {p0, v1}, Lcom/google/gson/stream/JsonReader;->syntaxError(Ljava/lang/String;)Ljava/io/IOException; */
/* throw v1 */
/* .line 524 */
/* :sswitch_3 */
/* invoke-direct {p0}, Lcom/google/gson/stream/JsonReader;->checkLenient()V */
/* .line 525 */
/* iget v1, p0, Lcom/google/gson/stream/JsonReader;->pos:I */
/* iget v9, p0, Lcom/google/gson/stream/JsonReader;->limit:I */
/* if-lt v1, v9, :cond_3 */
v1 = /* invoke-direct {p0, v8}, Lcom/google/gson/stream/JsonReader;->fillBuffer(I)Z */
if ( v1 != null) { // if-eqz v1, :cond_4
} // :cond_3
v1 = this.buffer;
/* iget v9, p0, Lcom/google/gson/stream/JsonReader;->pos:I */
/* aget-char v1, v1, v9 */
/* const/16 v10, 0x3e */
/* if-ne v1, v10, :cond_4 */
/* .line 526 */
/* add-int/2addr v9, v8 */
/* iput v9, p0, Lcom/google/gson/stream/JsonReader;->pos:I */
/* .line 522 */
/* :sswitch_4 */
/* nop */
/* .line 532 */
} // .end local v0 # "c":I
} // :cond_4
} // :goto_0
} // :cond_5
int v0 = 6; // const/4 v0, 0x6
/* if-ne v2, v0, :cond_7 */
/* .line 533 */
/* iget-boolean v0, p0, Lcom/google/gson/stream/JsonReader;->lenient:Z */
if ( v0 != null) { // if-eqz v0, :cond_6
/* .line 534 */
/* invoke-direct {p0}, Lcom/google/gson/stream/JsonReader;->consumeNonExecutePrefix()V */
/* .line 536 */
} // :cond_6
v0 = this.stack;
/* iget v1, p0, Lcom/google/gson/stream/JsonReader;->stackSize:I */
/* sub-int/2addr v1, v8 */
/* aput v5, v0, v1 */
/* .line 537 */
} // :cond_7
/* if-ne v2, v5, :cond_9 */
/* .line 538 */
int v0 = 0; // const/4 v0, 0x0
v0 = /* invoke-direct {p0, v0}, Lcom/google/gson/stream/JsonReader;->nextNonWhitespace(Z)I */
/* .line 539 */
/* .restart local v0 # "c":I */
int v1 = -1; // const/4 v1, -0x1
/* if-ne v0, v1, :cond_8 */
/* .line 540 */
/* const/16 v1, 0x11 */
/* iput v1, p0, Lcom/google/gson/stream/JsonReader;->peeked:I */
/* .line 542 */
} // :cond_8
/* invoke-direct {p0}, Lcom/google/gson/stream/JsonReader;->checkLenient()V */
/* .line 543 */
/* iget v1, p0, Lcom/google/gson/stream/JsonReader;->pos:I */
/* sub-int/2addr v1, v8 */
/* iput v1, p0, Lcom/google/gson/stream/JsonReader;->pos:I */
} // .end local v0 # "c":I
/* .line 545 */
} // :cond_9
/* if-eq v2, v3, :cond_10 */
} // :goto_1
/* nop */
/* .line 549 */
} // :goto_2
v0 = /* invoke-direct {p0, v8}, Lcom/google/gson/stream/JsonReader;->nextNonWhitespace(Z)I */
/* .line 550 */
/* .restart local v0 # "c":I */
/* sparse-switch v0, :sswitch_data_2 */
/* .line 576 */
/* iget v1, p0, Lcom/google/gson/stream/JsonReader;->pos:I */
/* sub-int/2addr v1, v8 */
/* iput v1, p0, Lcom/google/gson/stream/JsonReader;->pos:I */
/* .line 579 */
v1 = /* invoke-direct {p0}, Lcom/google/gson/stream/JsonReader;->peekKeyword()I */
/* .line 580 */
/* .local v1, "result":I */
if ( v1 != null) { // if-eqz v1, :cond_d
/* .line 581 */
/* .line 574 */
} // .end local v1 # "result":I
/* :sswitch_5 */
/* iput v8, p0, Lcom/google/gson/stream/JsonReader;->peeked:I */
/* .line 552 */
/* :sswitch_6 */
/* if-ne v2, v8, :cond_a */
/* .line 553 */
/* iput v6, p0, Lcom/google/gson/stream/JsonReader;->peeked:I */
/* .line 572 */
/* :sswitch_7 */
/* iput v4, p0, Lcom/google/gson/stream/JsonReader;->peeked:I */
/* .line 559 */
} // :cond_a
/* :sswitch_8 */
/* if-eq v2, v8, :cond_c */
/* if-ne v2, v7, :cond_b */
/* .line 564 */
} // :cond_b
final String v1 = "Unexpected value"; // const-string v1, "Unexpected value"
/* invoke-direct {p0, v1}, Lcom/google/gson/stream/JsonReader;->syntaxError(Ljava/lang/String;)Ljava/io/IOException; */
/* throw v1 */
/* .line 560 */
} // :cond_c
} // :goto_3
/* invoke-direct {p0}, Lcom/google/gson/stream/JsonReader;->checkLenient()V */
/* .line 561 */
/* iget v1, p0, Lcom/google/gson/stream/JsonReader;->pos:I */
/* sub-int/2addr v1, v8 */
/* iput v1, p0, Lcom/google/gson/stream/JsonReader;->pos:I */
/* .line 562 */
/* iput v5, p0, Lcom/google/gson/stream/JsonReader;->peeked:I */
/* .line 567 */
/* :sswitch_9 */
/* invoke-direct {p0}, Lcom/google/gson/stream/JsonReader;->checkLenient()V */
/* .line 568 */
/* iput v3, p0, Lcom/google/gson/stream/JsonReader;->peeked:I */
/* .line 570 */
/* :sswitch_a */
/* const/16 v1, 0x9 */
/* iput v1, p0, Lcom/google/gson/stream/JsonReader;->peeked:I */
/* .line 584 */
/* .restart local v1 # "result":I */
} // :cond_d
v1 = /* invoke-direct {p0}, Lcom/google/gson/stream/JsonReader;->peekNumber()I */
/* .line 585 */
if ( v1 != null) { // if-eqz v1, :cond_e
/* .line 586 */
/* .line 589 */
} // :cond_e
v3 = this.buffer;
/* iget v4, p0, Lcom/google/gson/stream/JsonReader;->pos:I */
/* aget-char v3, v3, v4 */
v3 = /* invoke-direct {p0, v3}, Lcom/google/gson/stream/JsonReader;->isLiteral(C)Z */
if ( v3 != null) { // if-eqz v3, :cond_f
/* .line 593 */
/* invoke-direct {p0}, Lcom/google/gson/stream/JsonReader;->checkLenient()V */
/* .line 594 */
/* const/16 v3, 0xa */
/* iput v3, p0, Lcom/google/gson/stream/JsonReader;->peeked:I */
/* .line 590 */
} // :cond_f
final String v3 = "Expected value"; // const-string v3, "Expected value"
/* invoke-direct {p0, v3}, Lcom/google/gson/stream/JsonReader;->syntaxError(Ljava/lang/String;)Ljava/io/IOException; */
/* throw v3 */
/* .line 546 */
} // .end local v0 # "c":I
} // .end local v1 # "result":I
} // :cond_10
/* new-instance v0, Ljava/lang/IllegalStateException; */
final String v1 = "JsonReader is closed"; // const-string v1, "JsonReader is closed"
/* invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V */
/* throw v0 */
/* .line 479 */
} // :cond_11
} // :goto_4
/* sub-int/2addr v1, v8 */
/* aput v6, v0, v1 */
/* .line 481 */
/* if-ne v2, v9, :cond_12 */
/* .line 482 */
v0 = /* invoke-direct {p0, v8}, Lcom/google/gson/stream/JsonReader;->nextNonWhitespace(Z)I */
/* .line 483 */
/* .restart local v0 # "c":I */
/* sparse-switch v0, :sswitch_data_3 */
/* .line 491 */
final String v1 = "Unterminated object"; // const-string v1, "Unterminated object"
/* invoke-direct {p0, v1}, Lcom/google/gson/stream/JsonReader;->syntaxError(Ljava/lang/String;)Ljava/io/IOException; */
/* throw v1 */
/* .line 485 */
/* :sswitch_b */
/* iput v7, p0, Lcom/google/gson/stream/JsonReader;->peeked:I */
/* .line 487 */
/* :sswitch_c */
/* invoke-direct {p0}, Lcom/google/gson/stream/JsonReader;->checkLenient()V */
/* .line 489 */
/* :sswitch_d */
/* nop */
/* .line 494 */
} // .end local v0 # "c":I
} // :cond_12
v0 = /* invoke-direct {p0, v8}, Lcom/google/gson/stream/JsonReader;->nextNonWhitespace(Z)I */
/* .line 495 */
/* .restart local v0 # "c":I */
final String v1 = "Expected name"; // const-string v1, "Expected name"
/* sparse-switch v0, :sswitch_data_4 */
/* .line 508 */
/* invoke-direct {p0}, Lcom/google/gson/stream/JsonReader;->checkLenient()V */
/* .line 509 */
/* iget v3, p0, Lcom/google/gson/stream/JsonReader;->pos:I */
/* sub-int/2addr v3, v8 */
/* iput v3, p0, Lcom/google/gson/stream/JsonReader;->pos:I */
/* .line 510 */
/* int-to-char v3, v0 */
v3 = /* invoke-direct {p0, v3}, Lcom/google/gson/stream/JsonReader;->isLiteral(C)Z */
if ( v3 != null) { // if-eqz v3, :cond_14
/* .line 511 */
/* const/16 v1, 0xe */
/* iput v1, p0, Lcom/google/gson/stream/JsonReader;->peeked:I */
/* .line 502 */
/* :sswitch_e */
/* if-eq v2, v9, :cond_13 */
/* .line 503 */
/* iput v7, p0, Lcom/google/gson/stream/JsonReader;->peeked:I */
/* .line 505 */
} // :cond_13
/* invoke-direct {p0, v1}, Lcom/google/gson/stream/JsonReader;->syntaxError(Ljava/lang/String;)Ljava/io/IOException; */
/* throw v1 */
/* .line 499 */
/* :sswitch_f */
/* invoke-direct {p0}, Lcom/google/gson/stream/JsonReader;->checkLenient()V */
/* .line 500 */
/* const/16 v1, 0xc */
/* iput v1, p0, Lcom/google/gson/stream/JsonReader;->peeked:I */
/* .line 497 */
/* :sswitch_10 */
/* const/16 v1, 0xd */
/* iput v1, p0, Lcom/google/gson/stream/JsonReader;->peeked:I */
/* .line 513 */
} // :cond_14
/* invoke-direct {p0, v1}, Lcom/google/gson/stream/JsonReader;->syntaxError(Ljava/lang/String;)Ljava/io/IOException; */
/* throw v1 */
/* nop */
/* :sswitch_data_0 */
/* .sparse-switch */
/* 0x2c -> :sswitch_2 */
/* 0x3b -> :sswitch_1 */
/* 0x5d -> :sswitch_0 */
} // .end sparse-switch
/* :sswitch_data_1 */
/* .sparse-switch */
/* 0x3a -> :sswitch_4 */
/* 0x3d -> :sswitch_3 */
} // .end sparse-switch
/* :sswitch_data_2 */
/* .sparse-switch */
/* 0x22 -> :sswitch_a */
/* 0x27 -> :sswitch_9 */
/* 0x2c -> :sswitch_8 */
/* 0x3b -> :sswitch_8 */
/* 0x5b -> :sswitch_7 */
/* 0x5d -> :sswitch_6 */
/* 0x7b -> :sswitch_5 */
} // .end sparse-switch
/* :sswitch_data_3 */
/* .sparse-switch */
/* 0x2c -> :sswitch_d */
/* 0x3b -> :sswitch_c */
/* 0x7d -> :sswitch_b */
} // .end sparse-switch
/* :sswitch_data_4 */
/* .sparse-switch */
/* 0x22 -> :sswitch_10 */
/* 0x27 -> :sswitch_f */
/* 0x7d -> :sswitch_e */
} // .end sparse-switch
} // .end method
public void endArray ( ) {
/* .locals 4 */
/* .annotation system Ldalvik/annotation/Throws; */
/* value = { */
/* Ljava/io/IOException; */
/* } */
} // .end annotation
/* .line 359 */
/* iget v0, p0, Lcom/google/gson/stream/JsonReader;->peeked:I */
/* .line 360 */
/* .local v0, "p":I */
/* if-nez v0, :cond_0 */
/* .line 361 */
v0 = (( com.google.gson.stream.JsonReader ) p0 ).doPeek ( ); // invoke-virtual {p0}, Lcom/google/gson/stream/JsonReader;->doPeek()I
/* .line 363 */
} // :cond_0
int v1 = 4; // const/4 v1, 0x4
/* if-ne v0, v1, :cond_1 */
/* .line 364 */
/* iget v1, p0, Lcom/google/gson/stream/JsonReader;->stackSize:I */
/* add-int/lit8 v1, v1, -0x1 */
/* iput v1, p0, Lcom/google/gson/stream/JsonReader;->stackSize:I */
/* .line 365 */
v2 = this.pathIndices;
/* add-int/lit8 v1, v1, -0x1 */
/* aget v3, v2, v1 */
/* add-int/lit8 v3, v3, 0x1 */
/* aput v3, v2, v1 */
/* .line 366 */
int v1 = 0; // const/4 v1, 0x0
/* iput v1, p0, Lcom/google/gson/stream/JsonReader;->peeked:I */
/* .line 370 */
return;
/* .line 368 */
} // :cond_1
/* new-instance v1, Ljava/lang/IllegalStateException; */
/* new-instance v2, Ljava/lang/StringBuilder; */
/* invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V */
final String v3 = "Expected END_ARRAY but was "; // const-string v3, "Expected END_ARRAY but was "
(( java.lang.StringBuilder ) v2 ).append ( v3 ); // invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( com.google.gson.stream.JsonReader ) p0 ).peek ( ); // invoke-virtual {p0}, Lcom/google/gson/stream/JsonReader;->peek()Lcom/google/gson/stream/JsonToken;
(( java.lang.StringBuilder ) v2 ).append ( v3 ); // invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;
(( com.google.gson.stream.JsonReader ) p0 ).locationString ( ); // invoke-virtual {p0}, Lcom/google/gson/stream/JsonReader;->locationString()Ljava/lang/String;
(( java.lang.StringBuilder ) v2 ).append ( v3 ); // invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v2 ).toString ( ); // invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
/* invoke-direct {v1, v2}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V */
/* throw v1 */
} // .end method
public void endObject ( ) {
/* .locals 4 */
/* .annotation system Ldalvik/annotation/Throws; */
/* value = { */
/* Ljava/io/IOException; */
/* } */
} // .end annotation
/* .line 394 */
/* iget v0, p0, Lcom/google/gson/stream/JsonReader;->peeked:I */
/* .line 395 */
/* .local v0, "p":I */
/* if-nez v0, :cond_0 */
/* .line 396 */
v0 = (( com.google.gson.stream.JsonReader ) p0 ).doPeek ( ); // invoke-virtual {p0}, Lcom/google/gson/stream/JsonReader;->doPeek()I
/* .line 398 */
} // :cond_0
int v1 = 2; // const/4 v1, 0x2
/* if-ne v0, v1, :cond_1 */
/* .line 399 */
/* iget v1, p0, Lcom/google/gson/stream/JsonReader;->stackSize:I */
/* add-int/lit8 v1, v1, -0x1 */
/* iput v1, p0, Lcom/google/gson/stream/JsonReader;->stackSize:I */
/* .line 400 */
v2 = this.pathNames;
int v3 = 0; // const/4 v3, 0x0
/* aput-object v3, v2, v1 */
/* .line 401 */
v2 = this.pathIndices;
/* add-int/lit8 v1, v1, -0x1 */
/* aget v3, v2, v1 */
/* add-int/lit8 v3, v3, 0x1 */
/* aput v3, v2, v1 */
/* .line 402 */
int v1 = 0; // const/4 v1, 0x0
/* iput v1, p0, Lcom/google/gson/stream/JsonReader;->peeked:I */
/* .line 406 */
return;
/* .line 404 */
} // :cond_1
/* new-instance v1, Ljava/lang/IllegalStateException; */
/* new-instance v2, Ljava/lang/StringBuilder; */
/* invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V */
final String v3 = "Expected END_OBJECT but was "; // const-string v3, "Expected END_OBJECT but was "
(( java.lang.StringBuilder ) v2 ).append ( v3 ); // invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( com.google.gson.stream.JsonReader ) p0 ).peek ( ); // invoke-virtual {p0}, Lcom/google/gson/stream/JsonReader;->peek()Lcom/google/gson/stream/JsonToken;
(( java.lang.StringBuilder ) v2 ).append ( v3 ); // invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;
(( com.google.gson.stream.JsonReader ) p0 ).locationString ( ); // invoke-virtual {p0}, Lcom/google/gson/stream/JsonReader;->locationString()Ljava/lang/String;
(( java.lang.StringBuilder ) v2 ).append ( v3 ); // invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v2 ).toString ( ); // invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
/* invoke-direct {v1, v2}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V */
/* throw v1 */
} // .end method
public java.lang.String getPath ( ) {
/* .locals 1 */
/* .line 1520 */
int v0 = 0; // const/4 v0, 0x0
/* invoke-direct {p0, v0}, Lcom/google/gson/stream/JsonReader;->getPath(Z)Ljava/lang/String; */
} // .end method
public java.lang.String getPreviousPath ( ) {
/* .locals 1 */
/* .line 1502 */
int v0 = 1; // const/4 v0, 0x1
/* invoke-direct {p0, v0}, Lcom/google/gson/stream/JsonReader;->getPath(Z)Ljava/lang/String; */
} // .end method
public Boolean hasNext ( ) {
/* .locals 2 */
/* .annotation system Ldalvik/annotation/Throws; */
/* value = { */
/* Ljava/io/IOException; */
/* } */
} // .end annotation
/* .line 412 */
/* iget v0, p0, Lcom/google/gson/stream/JsonReader;->peeked:I */
/* .line 413 */
/* .local v0, "p":I */
/* if-nez v0, :cond_0 */
/* .line 414 */
v0 = (( com.google.gson.stream.JsonReader ) p0 ).doPeek ( ); // invoke-virtual {p0}, Lcom/google/gson/stream/JsonReader;->doPeek()I
/* .line 416 */
} // :cond_0
int v1 = 2; // const/4 v1, 0x2
/* if-eq v0, v1, :cond_1 */
int v1 = 4; // const/4 v1, 0x4
/* if-eq v0, v1, :cond_1 */
/* const/16 v1, 0x11 */
/* if-eq v0, v1, :cond_1 */
int v1 = 1; // const/4 v1, 0x1
} // :cond_1
int v1 = 0; // const/4 v1, 0x0
} // :goto_0
} // .end method
public final Boolean isLenient ( ) {
/* .locals 1 */
/* .line 333 */
/* iget-boolean v0, p0, Lcom/google/gson/stream/JsonReader;->lenient:Z */
} // .end method
java.lang.String locationString ( ) {
/* .locals 4 */
/* .line 1453 */
/* iget v0, p0, Lcom/google/gson/stream/JsonReader;->lineNumber:I */
/* add-int/lit8 v0, v0, 0x1 */
/* .line 1454 */
/* .local v0, "line":I */
/* iget v1, p0, Lcom/google/gson/stream/JsonReader;->pos:I */
/* iget v2, p0, Lcom/google/gson/stream/JsonReader;->lineStart:I */
/* sub-int/2addr v1, v2 */
/* add-int/lit8 v1, v1, 0x1 */
/* .line 1455 */
/* .local v1, "column":I */
/* new-instance v2, Ljava/lang/StringBuilder; */
/* invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V */
final String v3 = " at line "; // const-string v3, " at line "
(( java.lang.StringBuilder ) v2 ).append ( v3 ); // invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v2 ).append ( v0 ); // invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;
final String v3 = " column "; // const-string v3, " column "
(( java.lang.StringBuilder ) v2 ).append ( v3 ); // invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v2 ).append ( v1 ); // invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;
final String v3 = " path "; // const-string v3, " path "
(( java.lang.StringBuilder ) v2 ).append ( v3 ); // invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( com.google.gson.stream.JsonReader ) p0 ).getPath ( ); // invoke-virtual {p0}, Lcom/google/gson/stream/JsonReader;->getPath()Ljava/lang/String;
(( java.lang.StringBuilder ) v2 ).append ( v3 ); // invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v2 ).toString ( ); // invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
} // .end method
public Boolean nextBoolean ( ) {
/* .locals 6 */
/* .annotation system Ldalvik/annotation/Throws; */
/* value = { */
/* Ljava/io/IOException; */
/* } */
} // .end annotation
/* .line 840 */
/* iget v0, p0, Lcom/google/gson/stream/JsonReader;->peeked:I */
/* .line 841 */
/* .local v0, "p":I */
/* if-nez v0, :cond_0 */
/* .line 842 */
v0 = (( com.google.gson.stream.JsonReader ) p0 ).doPeek ( ); // invoke-virtual {p0}, Lcom/google/gson/stream/JsonReader;->doPeek()I
/* .line 844 */
} // :cond_0
int v1 = 5; // const/4 v1, 0x5
int v2 = 0; // const/4 v2, 0x0
int v3 = 1; // const/4 v3, 0x1
/* if-ne v0, v1, :cond_1 */
/* .line 845 */
/* iput v2, p0, Lcom/google/gson/stream/JsonReader;->peeked:I */
/* .line 846 */
v1 = this.pathIndices;
/* iget v2, p0, Lcom/google/gson/stream/JsonReader;->stackSize:I */
/* sub-int/2addr v2, v3 */
/* aget v4, v1, v2 */
/* add-int/2addr v4, v3 */
/* aput v4, v1, v2 */
/* .line 847 */
/* .line 848 */
} // :cond_1
int v1 = 6; // const/4 v1, 0x6
/* if-ne v0, v1, :cond_2 */
/* .line 849 */
/* iput v2, p0, Lcom/google/gson/stream/JsonReader;->peeked:I */
/* .line 850 */
v1 = this.pathIndices;
/* iget v4, p0, Lcom/google/gson/stream/JsonReader;->stackSize:I */
/* sub-int/2addr v4, v3 */
/* aget v5, v1, v4 */
/* add-int/2addr v5, v3 */
/* aput v5, v1, v4 */
/* .line 851 */
/* .line 853 */
} // :cond_2
/* new-instance v1, Ljava/lang/IllegalStateException; */
/* new-instance v2, Ljava/lang/StringBuilder; */
/* invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V */
final String v3 = "Expected a boolean but was "; // const-string v3, "Expected a boolean but was "
(( java.lang.StringBuilder ) v2 ).append ( v3 ); // invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( com.google.gson.stream.JsonReader ) p0 ).peek ( ); // invoke-virtual {p0}, Lcom/google/gson/stream/JsonReader;->peek()Lcom/google/gson/stream/JsonToken;
(( java.lang.StringBuilder ) v2 ).append ( v3 ); // invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;
(( com.google.gson.stream.JsonReader ) p0 ).locationString ( ); // invoke-virtual {p0}, Lcom/google/gson/stream/JsonReader;->locationString()Ljava/lang/String;
(( java.lang.StringBuilder ) v2 ).append ( v3 ); // invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v2 ).toString ( ); // invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
/* invoke-direct {v1, v2}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V */
/* throw v1 */
} // .end method
public Double nextDouble ( ) {
/* .locals 7 */
/* .annotation system Ldalvik/annotation/Throws; */
/* value = { */
/* Ljava/io/IOException; */
/* } */
} // .end annotation
/* .line 886 */
/* iget v0, p0, Lcom/google/gson/stream/JsonReader;->peeked:I */
/* .line 887 */
/* .local v0, "p":I */
/* if-nez v0, :cond_0 */
/* .line 888 */
v0 = (( com.google.gson.stream.JsonReader ) p0 ).doPeek ( ); // invoke-virtual {p0}, Lcom/google/gson/stream/JsonReader;->doPeek()I
/* .line 891 */
} // :cond_0
/* const/16 v1, 0xf */
int v2 = 0; // const/4 v2, 0x0
/* if-ne v0, v1, :cond_1 */
/* .line 892 */
/* iput v2, p0, Lcom/google/gson/stream/JsonReader;->peeked:I */
/* .line 893 */
v1 = this.pathIndices;
/* iget v2, p0, Lcom/google/gson/stream/JsonReader;->stackSize:I */
/* add-int/lit8 v2, v2, -0x1 */
/* aget v3, v1, v2 */
/* add-int/lit8 v3, v3, 0x1 */
/* aput v3, v1, v2 */
/* .line 894 */
/* iget-wide v1, p0, Lcom/google/gson/stream/JsonReader;->peekedLong:J */
/* long-to-double v1, v1 */
/* return-wide v1 */
/* .line 897 */
} // :cond_1
/* const/16 v1, 0x10 */
/* const/16 v3, 0xb */
/* if-ne v0, v1, :cond_2 */
/* .line 898 */
/* new-instance v1, Ljava/lang/String; */
v4 = this.buffer;
/* iget v5, p0, Lcom/google/gson/stream/JsonReader;->pos:I */
/* iget v6, p0, Lcom/google/gson/stream/JsonReader;->peekedNumberLength:I */
/* invoke-direct {v1, v4, v5, v6}, Ljava/lang/String;-><init>([CII)V */
this.peekedString = v1;
/* .line 899 */
/* iget v1, p0, Lcom/google/gson/stream/JsonReader;->pos:I */
/* iget v4, p0, Lcom/google/gson/stream/JsonReader;->peekedNumberLength:I */
/* add-int/2addr v1, v4 */
/* iput v1, p0, Lcom/google/gson/stream/JsonReader;->pos:I */
/* .line 900 */
} // :cond_2
/* const/16 v1, 0x8 */
/* if-eq v0, v1, :cond_6 */
/* const/16 v4, 0x9 */
/* if-ne v0, v4, :cond_3 */
/* .line 902 */
} // :cond_3
/* const/16 v1, 0xa */
/* if-ne v0, v1, :cond_4 */
/* .line 903 */
/* invoke-direct {p0}, Lcom/google/gson/stream/JsonReader;->nextUnquotedValue()Ljava/lang/String; */
this.peekedString = v1;
/* .line 904 */
} // :cond_4
/* if-ne v0, v3, :cond_5 */
/* .line 905 */
} // :cond_5
/* new-instance v1, Ljava/lang/IllegalStateException; */
/* new-instance v2, Ljava/lang/StringBuilder; */
/* invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V */
final String v3 = "Expected a double but was "; // const-string v3, "Expected a double but was "
(( java.lang.StringBuilder ) v2 ).append ( v3 ); // invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( com.google.gson.stream.JsonReader ) p0 ).peek ( ); // invoke-virtual {p0}, Lcom/google/gson/stream/JsonReader;->peek()Lcom/google/gson/stream/JsonToken;
(( java.lang.StringBuilder ) v2 ).append ( v3 ); // invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;
(( com.google.gson.stream.JsonReader ) p0 ).locationString ( ); // invoke-virtual {p0}, Lcom/google/gson/stream/JsonReader;->locationString()Ljava/lang/String;
(( java.lang.StringBuilder ) v2 ).append ( v3 ); // invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v2 ).toString ( ); // invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
/* invoke-direct {v1, v2}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V */
/* throw v1 */
/* .line 901 */
} // :cond_6
} // :goto_0
/* if-ne v0, v1, :cond_7 */
/* const/16 v1, 0x27 */
} // :cond_7
/* const/16 v1, 0x22 */
} // :goto_1
/* invoke-direct {p0, v1}, Lcom/google/gson/stream/JsonReader;->nextQuotedValue(C)Ljava/lang/String; */
this.peekedString = v1;
/* .line 908 */
} // :goto_2
/* iput v3, p0, Lcom/google/gson/stream/JsonReader;->peeked:I */
/* .line 909 */
v1 = this.peekedString;
java.lang.Double .parseDouble ( v1 );
/* move-result-wide v3 */
/* .line 910 */
/* .local v3, "result":D */
/* iget-boolean v1, p0, Lcom/google/gson/stream/JsonReader;->lenient:Z */
/* if-nez v1, :cond_9 */
v1 = java.lang.Double .isNaN ( v3,v4 );
/* if-nez v1, :cond_8 */
v1 = java.lang.Double .isInfinite ( v3,v4 );
/* if-nez v1, :cond_8 */
/* .line 911 */
} // :cond_8
/* new-instance v1, Lcom/google/gson/stream/MalformedJsonException; */
/* new-instance v2, Ljava/lang/StringBuilder; */
/* invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V */
final String v5 = "JSON forbids NaN and infinities: "; // const-string v5, "JSON forbids NaN and infinities: "
(( java.lang.StringBuilder ) v2 ).append ( v5 ); // invoke-virtual {v2, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v2 ).append ( v3, v4 ); // invoke-virtual {v2, v3, v4}, Ljava/lang/StringBuilder;->append(D)Ljava/lang/StringBuilder;
/* .line 912 */
(( com.google.gson.stream.JsonReader ) p0 ).locationString ( ); // invoke-virtual {p0}, Lcom/google/gson/stream/JsonReader;->locationString()Ljava/lang/String;
(( java.lang.StringBuilder ) v2 ).append ( v5 ); // invoke-virtual {v2, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v2 ).toString ( ); // invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
/* invoke-direct {v1, v2}, Lcom/google/gson/stream/MalformedJsonException;-><init>(Ljava/lang/String;)V */
/* throw v1 */
/* .line 914 */
} // :cond_9
} // :goto_3
int v1 = 0; // const/4 v1, 0x0
this.peekedString = v1;
/* .line 915 */
/* iput v2, p0, Lcom/google/gson/stream/JsonReader;->peeked:I */
/* .line 916 */
v1 = this.pathIndices;
/* iget v2, p0, Lcom/google/gson/stream/JsonReader;->stackSize:I */
/* add-int/lit8 v2, v2, -0x1 */
/* aget v5, v1, v2 */
/* add-int/lit8 v5, v5, 0x1 */
/* aput v5, v1, v2 */
/* .line 917 */
/* return-wide v3 */
} // .end method
public Integer nextInt ( ) {
/* .locals 8 */
/* .annotation system Ldalvik/annotation/Throws; */
/* value = { */
/* Ljava/io/IOException; */
/* } */
} // .end annotation
/* .line 1163 */
/* iget v0, p0, Lcom/google/gson/stream/JsonReader;->peeked:I */
/* .line 1164 */
/* .local v0, "p":I */
/* if-nez v0, :cond_0 */
/* .line 1165 */
v0 = (( com.google.gson.stream.JsonReader ) p0 ).doPeek ( ); // invoke-virtual {p0}, Lcom/google/gson/stream/JsonReader;->doPeek()I
/* .line 1169 */
} // :cond_0
/* const/16 v1, 0xf */
final String v2 = "Expected an int but was "; // const-string v2, "Expected an int but was "
int v3 = 0; // const/4 v3, 0x0
/* if-ne v0, v1, :cond_2 */
/* .line 1170 */
/* iget-wide v4, p0, Lcom/google/gson/stream/JsonReader;->peekedLong:J */
/* long-to-int v1, v4 */
/* .line 1171 */
/* .local v1, "result":I */
/* int-to-long v6, v1 */
/* cmp-long v4, v4, v6 */
/* if-nez v4, :cond_1 */
/* .line 1174 */
/* iput v3, p0, Lcom/google/gson/stream/JsonReader;->peeked:I */
/* .line 1175 */
v2 = this.pathIndices;
/* iget v3, p0, Lcom/google/gson/stream/JsonReader;->stackSize:I */
/* add-int/lit8 v3, v3, -0x1 */
/* aget v4, v2, v3 */
/* add-int/lit8 v4, v4, 0x1 */
/* aput v4, v2, v3 */
/* .line 1176 */
/* .line 1172 */
} // :cond_1
/* new-instance v3, Ljava/lang/NumberFormatException; */
/* new-instance v4, Ljava/lang/StringBuilder; */
/* invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V */
(( java.lang.StringBuilder ) v4 ).append ( v2 ); // invoke-virtual {v4, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
/* iget-wide v4, p0, Lcom/google/gson/stream/JsonReader;->peekedLong:J */
(( java.lang.StringBuilder ) v2 ).append ( v4, v5 ); // invoke-virtual {v2, v4, v5}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;
(( com.google.gson.stream.JsonReader ) p0 ).locationString ( ); // invoke-virtual {p0}, Lcom/google/gson/stream/JsonReader;->locationString()Ljava/lang/String;
(( java.lang.StringBuilder ) v2 ).append ( v4 ); // invoke-virtual {v2, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v2 ).toString ( ); // invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
/* invoke-direct {v3, v2}, Ljava/lang/NumberFormatException;-><init>(Ljava/lang/String;)V */
/* throw v3 */
/* .line 1179 */
} // .end local v1 # "result":I
} // :cond_2
/* const/16 v1, 0x10 */
/* if-ne v0, v1, :cond_3 */
/* .line 1180 */
/* new-instance v1, Ljava/lang/String; */
v4 = this.buffer;
/* iget v5, p0, Lcom/google/gson/stream/JsonReader;->pos:I */
/* iget v6, p0, Lcom/google/gson/stream/JsonReader;->peekedNumberLength:I */
/* invoke-direct {v1, v4, v5, v6}, Ljava/lang/String;-><init>([CII)V */
this.peekedString = v1;
/* .line 1181 */
/* iget v1, p0, Lcom/google/gson/stream/JsonReader;->pos:I */
/* iget v4, p0, Lcom/google/gson/stream/JsonReader;->peekedNumberLength:I */
/* add-int/2addr v1, v4 */
/* iput v1, p0, Lcom/google/gson/stream/JsonReader;->pos:I */
/* .line 1182 */
} // :cond_3
/* const/16 v1, 0xa */
/* const/16 v4, 0x8 */
/* if-eq v0, v4, :cond_5 */
/* const/16 v5, 0x9 */
/* if-eq v0, v5, :cond_5 */
/* if-ne v0, v1, :cond_4 */
/* .line 1197 */
} // :cond_4
/* new-instance v1, Ljava/lang/IllegalStateException; */
/* new-instance v3, Ljava/lang/StringBuilder; */
/* invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V */
(( java.lang.StringBuilder ) v3 ).append ( v2 ); // invoke-virtual {v3, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( com.google.gson.stream.JsonReader ) p0 ).peek ( ); // invoke-virtual {p0}, Lcom/google/gson/stream/JsonReader;->peek()Lcom/google/gson/stream/JsonToken;
(( java.lang.StringBuilder ) v2 ).append ( v3 ); // invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;
(( com.google.gson.stream.JsonReader ) p0 ).locationString ( ); // invoke-virtual {p0}, Lcom/google/gson/stream/JsonReader;->locationString()Ljava/lang/String;
(( java.lang.StringBuilder ) v2 ).append ( v3 ); // invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v2 ).toString ( ); // invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
/* invoke-direct {v1, v2}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V */
/* throw v1 */
/* .line 1183 */
} // :cond_5
} // :goto_0
/* if-ne v0, v1, :cond_6 */
/* .line 1184 */
/* invoke-direct {p0}, Lcom/google/gson/stream/JsonReader;->nextUnquotedValue()Ljava/lang/String; */
this.peekedString = v1;
/* .line 1186 */
} // :cond_6
/* if-ne v0, v4, :cond_7 */
/* const/16 v1, 0x27 */
} // :cond_7
/* const/16 v1, 0x22 */
} // :goto_1
/* invoke-direct {p0, v1}, Lcom/google/gson/stream/JsonReader;->nextQuotedValue(C)Ljava/lang/String; */
this.peekedString = v1;
/* .line 1189 */
} // :goto_2
try { // :try_start_0
v1 = this.peekedString;
v1 = java.lang.Integer .parseInt ( v1 );
/* .line 1190 */
/* .restart local v1 # "result":I */
/* iput v3, p0, Lcom/google/gson/stream/JsonReader;->peeked:I */
/* .line 1191 */
v4 = this.pathIndices;
/* iget v5, p0, Lcom/google/gson/stream/JsonReader;->stackSize:I */
/* add-int/lit8 v5, v5, -0x1 */
/* aget v6, v4, v5 */
/* add-int/lit8 v6, v6, 0x1 */
/* aput v6, v4, v5 */
/* :try_end_0 */
/* .catch Ljava/lang/NumberFormatException; {:try_start_0 ..:try_end_0} :catch_0 */
/* .line 1192 */
/* .line 1193 */
} // .end local v1 # "result":I
/* :catch_0 */
/* move-exception v1 */
/* .line 1195 */
/* nop */
/* .line 1200 */
} // :goto_3
/* const/16 v1, 0xb */
/* iput v1, p0, Lcom/google/gson/stream/JsonReader;->peeked:I */
/* .line 1201 */
v1 = this.peekedString;
java.lang.Double .parseDouble ( v1 );
/* move-result-wide v4 */
/* .line 1202 */
/* .local v4, "asDouble":D */
/* double-to-int v1, v4 */
/* .line 1203 */
/* .restart local v1 # "result":I */
/* int-to-double v6, v1 */
/* cmpl-double v6, v6, v4 */
/* if-nez v6, :cond_8 */
/* .line 1206 */
int v2 = 0; // const/4 v2, 0x0
this.peekedString = v2;
/* .line 1207 */
/* iput v3, p0, Lcom/google/gson/stream/JsonReader;->peeked:I */
/* .line 1208 */
v2 = this.pathIndices;
/* iget v3, p0, Lcom/google/gson/stream/JsonReader;->stackSize:I */
/* add-int/lit8 v3, v3, -0x1 */
/* aget v6, v2, v3 */
/* add-int/lit8 v6, v6, 0x1 */
/* aput v6, v2, v3 */
/* .line 1209 */
/* .line 1204 */
} // :cond_8
/* new-instance v3, Ljava/lang/NumberFormatException; */
/* new-instance v6, Ljava/lang/StringBuilder; */
/* invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V */
(( java.lang.StringBuilder ) v6 ).append ( v2 ); // invoke-virtual {v6, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
v6 = this.peekedString;
(( java.lang.StringBuilder ) v2 ).append ( v6 ); // invoke-virtual {v2, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( com.google.gson.stream.JsonReader ) p0 ).locationString ( ); // invoke-virtual {p0}, Lcom/google/gson/stream/JsonReader;->locationString()Ljava/lang/String;
(( java.lang.StringBuilder ) v2 ).append ( v6 ); // invoke-virtual {v2, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v2 ).toString ( ); // invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
/* invoke-direct {v3, v2}, Ljava/lang/NumberFormatException;-><init>(Ljava/lang/String;)V */
/* throw v3 */
} // .end method
public Long nextLong ( ) {
/* .locals 10 */
/* .annotation system Ldalvik/annotation/Throws; */
/* value = { */
/* Ljava/io/IOException; */
/* } */
} // .end annotation
/* .line 931 */
/* iget v0, p0, Lcom/google/gson/stream/JsonReader;->peeked:I */
/* .line 932 */
/* .local v0, "p":I */
/* if-nez v0, :cond_0 */
/* .line 933 */
v0 = (( com.google.gson.stream.JsonReader ) p0 ).doPeek ( ); // invoke-virtual {p0}, Lcom/google/gson/stream/JsonReader;->doPeek()I
/* .line 936 */
} // :cond_0
/* const/16 v1, 0xf */
int v2 = 0; // const/4 v2, 0x0
/* if-ne v0, v1, :cond_1 */
/* .line 937 */
/* iput v2, p0, Lcom/google/gson/stream/JsonReader;->peeked:I */
/* .line 938 */
v1 = this.pathIndices;
/* iget v2, p0, Lcom/google/gson/stream/JsonReader;->stackSize:I */
/* add-int/lit8 v2, v2, -0x1 */
/* aget v3, v1, v2 */
/* add-int/lit8 v3, v3, 0x1 */
/* aput v3, v1, v2 */
/* .line 939 */
/* iget-wide v1, p0, Lcom/google/gson/stream/JsonReader;->peekedLong:J */
/* return-wide v1 */
/* .line 942 */
} // :cond_1
/* const/16 v1, 0x10 */
final String v3 = "Expected a long but was "; // const-string v3, "Expected a long but was "
/* if-ne v0, v1, :cond_2 */
/* .line 943 */
/* new-instance v1, Ljava/lang/String; */
v4 = this.buffer;
/* iget v5, p0, Lcom/google/gson/stream/JsonReader;->pos:I */
/* iget v6, p0, Lcom/google/gson/stream/JsonReader;->peekedNumberLength:I */
/* invoke-direct {v1, v4, v5, v6}, Ljava/lang/String;-><init>([CII)V */
this.peekedString = v1;
/* .line 944 */
/* iget v1, p0, Lcom/google/gson/stream/JsonReader;->pos:I */
/* iget v4, p0, Lcom/google/gson/stream/JsonReader;->peekedNumberLength:I */
/* add-int/2addr v1, v4 */
/* iput v1, p0, Lcom/google/gson/stream/JsonReader;->pos:I */
/* .line 945 */
} // :cond_2
/* const/16 v1, 0xa */
/* const/16 v4, 0x8 */
/* if-eq v0, v4, :cond_4 */
/* const/16 v5, 0x9 */
/* if-eq v0, v5, :cond_4 */
/* if-ne v0, v1, :cond_3 */
/* .line 960 */
} // :cond_3
/* new-instance v1, Ljava/lang/IllegalStateException; */
/* new-instance v2, Ljava/lang/StringBuilder; */
/* invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V */
(( java.lang.StringBuilder ) v2 ).append ( v3 ); // invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( com.google.gson.stream.JsonReader ) p0 ).peek ( ); // invoke-virtual {p0}, Lcom/google/gson/stream/JsonReader;->peek()Lcom/google/gson/stream/JsonToken;
(( java.lang.StringBuilder ) v2 ).append ( v3 ); // invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;
(( com.google.gson.stream.JsonReader ) p0 ).locationString ( ); // invoke-virtual {p0}, Lcom/google/gson/stream/JsonReader;->locationString()Ljava/lang/String;
(( java.lang.StringBuilder ) v2 ).append ( v3 ); // invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v2 ).toString ( ); // invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
/* invoke-direct {v1, v2}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V */
/* throw v1 */
/* .line 946 */
} // :cond_4
} // :goto_0
/* if-ne v0, v1, :cond_5 */
/* .line 947 */
/* invoke-direct {p0}, Lcom/google/gson/stream/JsonReader;->nextUnquotedValue()Ljava/lang/String; */
this.peekedString = v1;
/* .line 949 */
} // :cond_5
/* if-ne v0, v4, :cond_6 */
/* const/16 v1, 0x27 */
} // :cond_6
/* const/16 v1, 0x22 */
} // :goto_1
/* invoke-direct {p0, v1}, Lcom/google/gson/stream/JsonReader;->nextQuotedValue(C)Ljava/lang/String; */
this.peekedString = v1;
/* .line 952 */
} // :goto_2
try { // :try_start_0
v1 = this.peekedString;
java.lang.Long .parseLong ( v1 );
/* move-result-wide v4 */
/* .line 953 */
/* .local v4, "result":J */
/* iput v2, p0, Lcom/google/gson/stream/JsonReader;->peeked:I */
/* .line 954 */
v1 = this.pathIndices;
/* iget v6, p0, Lcom/google/gson/stream/JsonReader;->stackSize:I */
/* add-int/lit8 v6, v6, -0x1 */
/* aget v7, v1, v6 */
/* add-int/lit8 v7, v7, 0x1 */
/* aput v7, v1, v6 */
/* :try_end_0 */
/* .catch Ljava/lang/NumberFormatException; {:try_start_0 ..:try_end_0} :catch_0 */
/* .line 955 */
/* return-wide v4 */
/* .line 956 */
} // .end local v4 # "result":J
/* :catch_0 */
/* move-exception v1 */
/* .line 958 */
/* nop */
/* .line 963 */
} // :goto_3
/* const/16 v1, 0xb */
/* iput v1, p0, Lcom/google/gson/stream/JsonReader;->peeked:I */
/* .line 964 */
v1 = this.peekedString;
java.lang.Double .parseDouble ( v1 );
/* move-result-wide v4 */
/* .line 965 */
/* .local v4, "asDouble":D */
/* double-to-long v6, v4 */
/* .line 966 */
/* .local v6, "result":J */
/* long-to-double v8, v6 */
/* cmpl-double v1, v8, v4 */
/* if-nez v1, :cond_7 */
/* .line 969 */
int v1 = 0; // const/4 v1, 0x0
this.peekedString = v1;
/* .line 970 */
/* iput v2, p0, Lcom/google/gson/stream/JsonReader;->peeked:I */
/* .line 971 */
v1 = this.pathIndices;
/* iget v2, p0, Lcom/google/gson/stream/JsonReader;->stackSize:I */
/* add-int/lit8 v2, v2, -0x1 */
/* aget v3, v1, v2 */
/* add-int/lit8 v3, v3, 0x1 */
/* aput v3, v1, v2 */
/* .line 972 */
/* return-wide v6 */
/* .line 967 */
} // :cond_7
/* new-instance v1, Ljava/lang/NumberFormatException; */
/* new-instance v2, Ljava/lang/StringBuilder; */
/* invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V */
(( java.lang.StringBuilder ) v2 ).append ( v3 ); // invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
v3 = this.peekedString;
(( java.lang.StringBuilder ) v2 ).append ( v3 ); // invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( com.google.gson.stream.JsonReader ) p0 ).locationString ( ); // invoke-virtual {p0}, Lcom/google/gson/stream/JsonReader;->locationString()Ljava/lang/String;
(( java.lang.StringBuilder ) v2 ).append ( v3 ); // invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v2 ).toString ( ); // invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
/* invoke-direct {v1, v2}, Ljava/lang/NumberFormatException;-><init>(Ljava/lang/String;)V */
/* throw v1 */
} // .end method
public java.lang.String nextName ( ) {
/* .locals 4 */
/* .annotation system Ldalvik/annotation/Throws; */
/* value = { */
/* Ljava/io/IOException; */
/* } */
} // .end annotation
/* .line 777 */
/* iget v0, p0, Lcom/google/gson/stream/JsonReader;->peeked:I */
/* .line 778 */
/* .local v0, "p":I */
/* if-nez v0, :cond_0 */
/* .line 779 */
v0 = (( com.google.gson.stream.JsonReader ) p0 ).doPeek ( ); // invoke-virtual {p0}, Lcom/google/gson/stream/JsonReader;->doPeek()I
/* .line 782 */
} // :cond_0
/* const/16 v1, 0xe */
/* if-ne v0, v1, :cond_1 */
/* .line 783 */
/* invoke-direct {p0}, Lcom/google/gson/stream/JsonReader;->nextUnquotedValue()Ljava/lang/String; */
/* .local v1, "result":Ljava/lang/String; */
/* .line 784 */
} // .end local v1 # "result":Ljava/lang/String;
} // :cond_1
/* const/16 v1, 0xc */
/* if-ne v0, v1, :cond_2 */
/* .line 785 */
/* const/16 v1, 0x27 */
/* invoke-direct {p0, v1}, Lcom/google/gson/stream/JsonReader;->nextQuotedValue(C)Ljava/lang/String; */
/* .restart local v1 # "result":Ljava/lang/String; */
/* .line 786 */
} // .end local v1 # "result":Ljava/lang/String;
} // :cond_2
/* const/16 v1, 0xd */
/* if-ne v0, v1, :cond_3 */
/* .line 787 */
/* const/16 v1, 0x22 */
/* invoke-direct {p0, v1}, Lcom/google/gson/stream/JsonReader;->nextQuotedValue(C)Ljava/lang/String; */
/* .line 791 */
/* .restart local v1 # "result":Ljava/lang/String; */
} // :goto_0
int v2 = 0; // const/4 v2, 0x0
/* iput v2, p0, Lcom/google/gson/stream/JsonReader;->peeked:I */
/* .line 792 */
v2 = this.pathNames;
/* iget v3, p0, Lcom/google/gson/stream/JsonReader;->stackSize:I */
/* add-int/lit8 v3, v3, -0x1 */
/* aput-object v1, v2, v3 */
/* .line 793 */
/* .line 789 */
} // .end local v1 # "result":Ljava/lang/String;
} // :cond_3
/* new-instance v1, Ljava/lang/IllegalStateException; */
/* new-instance v2, Ljava/lang/StringBuilder; */
/* invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V */
final String v3 = "Expected a name but was "; // const-string v3, "Expected a name but was "
(( java.lang.StringBuilder ) v2 ).append ( v3 ); // invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( com.google.gson.stream.JsonReader ) p0 ).peek ( ); // invoke-virtual {p0}, Lcom/google/gson/stream/JsonReader;->peek()Lcom/google/gson/stream/JsonToken;
(( java.lang.StringBuilder ) v2 ).append ( v3 ); // invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;
(( com.google.gson.stream.JsonReader ) p0 ).locationString ( ); // invoke-virtual {p0}, Lcom/google/gson/stream/JsonReader;->locationString()Ljava/lang/String;
(( java.lang.StringBuilder ) v2 ).append ( v3 ); // invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v2 ).toString ( ); // invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
/* invoke-direct {v1, v2}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V */
/* throw v1 */
} // .end method
public void nextNull ( ) {
/* .locals 4 */
/* .annotation system Ldalvik/annotation/Throws; */
/* value = { */
/* Ljava/io/IOException; */
/* } */
} // .end annotation
/* .line 864 */
/* iget v0, p0, Lcom/google/gson/stream/JsonReader;->peeked:I */
/* .line 865 */
/* .local v0, "p":I */
/* if-nez v0, :cond_0 */
/* .line 866 */
v0 = (( com.google.gson.stream.JsonReader ) p0 ).doPeek ( ); // invoke-virtual {p0}, Lcom/google/gson/stream/JsonReader;->doPeek()I
/* .line 868 */
} // :cond_0
int v1 = 7; // const/4 v1, 0x7
/* if-ne v0, v1, :cond_1 */
/* .line 869 */
int v1 = 0; // const/4 v1, 0x0
/* iput v1, p0, Lcom/google/gson/stream/JsonReader;->peeked:I */
/* .line 870 */
v1 = this.pathIndices;
/* iget v2, p0, Lcom/google/gson/stream/JsonReader;->stackSize:I */
/* add-int/lit8 v2, v2, -0x1 */
/* aget v3, v1, v2 */
/* add-int/lit8 v3, v3, 0x1 */
/* aput v3, v1, v2 */
/* .line 874 */
return;
/* .line 872 */
} // :cond_1
/* new-instance v1, Ljava/lang/IllegalStateException; */
/* new-instance v2, Ljava/lang/StringBuilder; */
/* invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V */
final String v3 = "Expected null but was "; // const-string v3, "Expected null but was "
(( java.lang.StringBuilder ) v2 ).append ( v3 ); // invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( com.google.gson.stream.JsonReader ) p0 ).peek ( ); // invoke-virtual {p0}, Lcom/google/gson/stream/JsonReader;->peek()Lcom/google/gson/stream/JsonToken;
(( java.lang.StringBuilder ) v2 ).append ( v3 ); // invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;
(( com.google.gson.stream.JsonReader ) p0 ).locationString ( ); // invoke-virtual {p0}, Lcom/google/gson/stream/JsonReader;->locationString()Ljava/lang/String;
(( java.lang.StringBuilder ) v2 ).append ( v3 ); // invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v2 ).toString ( ); // invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
/* invoke-direct {v1, v2}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V */
/* throw v1 */
} // .end method
public java.lang.String nextString ( ) {
/* .locals 5 */
/* .annotation system Ldalvik/annotation/Throws; */
/* value = { */
/* Ljava/io/IOException; */
/* } */
} // .end annotation
/* .line 805 */
/* iget v0, p0, Lcom/google/gson/stream/JsonReader;->peeked:I */
/* .line 806 */
/* .local v0, "p":I */
/* if-nez v0, :cond_0 */
/* .line 807 */
v0 = (( com.google.gson.stream.JsonReader ) p0 ).doPeek ( ); // invoke-virtual {p0}, Lcom/google/gson/stream/JsonReader;->doPeek()I
/* .line 810 */
} // :cond_0
/* const/16 v1, 0xa */
/* if-ne v0, v1, :cond_1 */
/* .line 811 */
/* invoke-direct {p0}, Lcom/google/gson/stream/JsonReader;->nextUnquotedValue()Ljava/lang/String; */
/* .local v1, "result":Ljava/lang/String; */
/* .line 812 */
} // .end local v1 # "result":Ljava/lang/String;
} // :cond_1
/* const/16 v1, 0x8 */
/* if-ne v0, v1, :cond_2 */
/* .line 813 */
/* const/16 v1, 0x27 */
/* invoke-direct {p0, v1}, Lcom/google/gson/stream/JsonReader;->nextQuotedValue(C)Ljava/lang/String; */
/* .restart local v1 # "result":Ljava/lang/String; */
/* .line 814 */
} // .end local v1 # "result":Ljava/lang/String;
} // :cond_2
/* const/16 v1, 0x9 */
/* if-ne v0, v1, :cond_3 */
/* .line 815 */
/* const/16 v1, 0x22 */
/* invoke-direct {p0, v1}, Lcom/google/gson/stream/JsonReader;->nextQuotedValue(C)Ljava/lang/String; */
/* .restart local v1 # "result":Ljava/lang/String; */
/* .line 816 */
} // .end local v1 # "result":Ljava/lang/String;
} // :cond_3
/* const/16 v1, 0xb */
/* if-ne v0, v1, :cond_4 */
/* .line 817 */
v1 = this.peekedString;
/* .line 818 */
/* .restart local v1 # "result":Ljava/lang/String; */
int v2 = 0; // const/4 v2, 0x0
this.peekedString = v2;
/* .line 819 */
} // .end local v1 # "result":Ljava/lang/String;
} // :cond_4
/* const/16 v1, 0xf */
/* if-ne v0, v1, :cond_5 */
/* .line 820 */
/* iget-wide v1, p0, Lcom/google/gson/stream/JsonReader;->peekedLong:J */
java.lang.Long .toString ( v1,v2 );
/* .restart local v1 # "result":Ljava/lang/String; */
/* .line 821 */
} // .end local v1 # "result":Ljava/lang/String;
} // :cond_5
/* const/16 v1, 0x10 */
/* if-ne v0, v1, :cond_6 */
/* .line 822 */
/* new-instance v1, Ljava/lang/String; */
v2 = this.buffer;
/* iget v3, p0, Lcom/google/gson/stream/JsonReader;->pos:I */
/* iget v4, p0, Lcom/google/gson/stream/JsonReader;->peekedNumberLength:I */
/* invoke-direct {v1, v2, v3, v4}, Ljava/lang/String;-><init>([CII)V */
/* .line 823 */
/* .restart local v1 # "result":Ljava/lang/String; */
/* iget v2, p0, Lcom/google/gson/stream/JsonReader;->pos:I */
/* iget v3, p0, Lcom/google/gson/stream/JsonReader;->peekedNumberLength:I */
/* add-int/2addr v2, v3 */
/* iput v2, p0, Lcom/google/gson/stream/JsonReader;->pos:I */
/* .line 827 */
} // :goto_0
int v2 = 0; // const/4 v2, 0x0
/* iput v2, p0, Lcom/google/gson/stream/JsonReader;->peeked:I */
/* .line 828 */
v2 = this.pathIndices;
/* iget v3, p0, Lcom/google/gson/stream/JsonReader;->stackSize:I */
/* add-int/lit8 v3, v3, -0x1 */
/* aget v4, v2, v3 */
/* add-int/lit8 v4, v4, 0x1 */
/* aput v4, v2, v3 */
/* .line 829 */
/* .line 825 */
} // .end local v1 # "result":Ljava/lang/String;
} // :cond_6
/* new-instance v1, Ljava/lang/IllegalStateException; */
/* new-instance v2, Ljava/lang/StringBuilder; */
/* invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V */
final String v3 = "Expected a string but was "; // const-string v3, "Expected a string but was "
(( java.lang.StringBuilder ) v2 ).append ( v3 ); // invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( com.google.gson.stream.JsonReader ) p0 ).peek ( ); // invoke-virtual {p0}, Lcom/google/gson/stream/JsonReader;->peek()Lcom/google/gson/stream/JsonToken;
(( java.lang.StringBuilder ) v2 ).append ( v3 ); // invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;
(( com.google.gson.stream.JsonReader ) p0 ).locationString ( ); // invoke-virtual {p0}, Lcom/google/gson/stream/JsonReader;->locationString()Ljava/lang/String;
(( java.lang.StringBuilder ) v2 ).append ( v3 ); // invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v2 ).toString ( ); // invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
/* invoke-direct {v1, v2}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V */
/* throw v1 */
} // .end method
public com.google.gson.stream.JsonToken peek ( ) {
/* .locals 2 */
/* .annotation system Ldalvik/annotation/Throws; */
/* value = { */
/* Ljava/io/IOException; */
/* } */
} // .end annotation
/* .line 423 */
/* iget v0, p0, Lcom/google/gson/stream/JsonReader;->peeked:I */
/* .line 424 */
/* .local v0, "p":I */
/* if-nez v0, :cond_0 */
/* .line 425 */
v0 = (( com.google.gson.stream.JsonReader ) p0 ).doPeek ( ); // invoke-virtual {p0}, Lcom/google/gson/stream/JsonReader;->doPeek()I
/* .line 428 */
} // :cond_0
/* packed-switch v0, :pswitch_data_0 */
/* .line 457 */
/* new-instance v1, Ljava/lang/AssertionError; */
/* invoke-direct {v1}, Ljava/lang/AssertionError;-><init>()V */
/* throw v1 */
/* .line 455 */
/* :pswitch_0 */
v1 = com.google.gson.stream.JsonToken.END_DOCUMENT;
/* .line 453 */
/* :pswitch_1 */
v1 = com.google.gson.stream.JsonToken.NUMBER;
/* .line 440 */
/* :pswitch_2 */
v1 = com.google.gson.stream.JsonToken.NAME;
/* .line 450 */
/* :pswitch_3 */
v1 = com.google.gson.stream.JsonToken.STRING;
/* .line 445 */
/* :pswitch_4 */
v1 = com.google.gson.stream.JsonToken.NULL;
/* .line 443 */
/* :pswitch_5 */
v1 = com.google.gson.stream.JsonToken.BOOLEAN;
/* .line 436 */
/* :pswitch_6 */
v1 = com.google.gson.stream.JsonToken.END_ARRAY;
/* .line 434 */
/* :pswitch_7 */
v1 = com.google.gson.stream.JsonToken.BEGIN_ARRAY;
/* .line 432 */
/* :pswitch_8 */
v1 = com.google.gson.stream.JsonToken.END_OBJECT;
/* .line 430 */
/* :pswitch_9 */
v1 = com.google.gson.stream.JsonToken.BEGIN_OBJECT;
/* nop */
/* :pswitch_data_0 */
/* .packed-switch 0x1 */
/* :pswitch_9 */
/* :pswitch_8 */
/* :pswitch_7 */
/* :pswitch_6 */
/* :pswitch_5 */
/* :pswitch_5 */
/* :pswitch_4 */
/* :pswitch_3 */
/* :pswitch_3 */
/* :pswitch_3 */
/* :pswitch_3 */
/* :pswitch_2 */
/* :pswitch_2 */
/* :pswitch_2 */
/* :pswitch_1 */
/* :pswitch_1 */
/* :pswitch_0 */
} // .end packed-switch
} // .end method
public final void setLenient ( Boolean p0 ) {
/* .locals 0 */
/* .param p1, "lenient" # Z */
/* .line 326 */
/* iput-boolean p1, p0, Lcom/google/gson/stream/JsonReader;->lenient:Z */
/* .line 327 */
return;
} // .end method
public void skipValue ( ) {
/* .locals 6 */
/* .annotation system Ldalvik/annotation/Throws; */
/* value = { */
/* Ljava/io/IOException; */
/* } */
} // .end annotation
/* .line 1228 */
int v0 = 0; // const/4 v0, 0x0
/* .line 1230 */
/* .local v0, "count":I */
} // :cond_0
/* iget v1, p0, Lcom/google/gson/stream/JsonReader;->peeked:I */
/* .line 1231 */
/* .local v1, "p":I */
/* if-nez v1, :cond_1 */
/* .line 1232 */
v1 = (( com.google.gson.stream.JsonReader ) p0 ).doPeek ( ); // invoke-virtual {p0}, Lcom/google/gson/stream/JsonReader;->doPeek()I
/* .line 1235 */
} // :cond_1
int v2 = 3; // const/4 v2, 0x3
int v3 = 1; // const/4 v3, 0x1
/* if-ne v1, v2, :cond_2 */
/* .line 1236 */
/* invoke-direct {p0, v3}, Lcom/google/gson/stream/JsonReader;->push(I)V */
/* .line 1237 */
/* add-int/lit8 v0, v0, 0x1 */
/* .line 1238 */
} // :cond_2
/* if-ne v1, v3, :cond_3 */
/* .line 1239 */
/* invoke-direct {p0, v2}, Lcom/google/gson/stream/JsonReader;->push(I)V */
/* .line 1240 */
/* add-int/lit8 v0, v0, 0x1 */
/* .line 1241 */
} // :cond_3
int v2 = 4; // const/4 v2, 0x4
/* if-ne v1, v2, :cond_4 */
/* .line 1242 */
/* iget v2, p0, Lcom/google/gson/stream/JsonReader;->stackSize:I */
/* sub-int/2addr v2, v3 */
/* iput v2, p0, Lcom/google/gson/stream/JsonReader;->stackSize:I */
/* .line 1243 */
/* add-int/lit8 v0, v0, -0x1 */
/* .line 1244 */
} // :cond_4
int v2 = 2; // const/4 v2, 0x2
/* if-ne v1, v2, :cond_5 */
/* .line 1245 */
/* iget v2, p0, Lcom/google/gson/stream/JsonReader;->stackSize:I */
/* sub-int/2addr v2, v3 */
/* iput v2, p0, Lcom/google/gson/stream/JsonReader;->stackSize:I */
/* .line 1246 */
/* add-int/lit8 v0, v0, -0x1 */
/* .line 1247 */
} // :cond_5
/* const/16 v2, 0xe */
/* if-eq v1, v2, :cond_b */
/* const/16 v2, 0xa */
/* if-ne v1, v2, :cond_6 */
/* .line 1249 */
} // :cond_6
/* const/16 v2, 0x8 */
/* if-eq v1, v2, :cond_a */
/* const/16 v2, 0xc */
/* if-ne v1, v2, :cond_7 */
/* .line 1251 */
} // :cond_7
/* const/16 v2, 0x9 */
/* if-eq v1, v2, :cond_9 */
/* const/16 v2, 0xd */
/* if-ne v1, v2, :cond_8 */
/* .line 1253 */
} // :cond_8
/* const/16 v2, 0x10 */
/* if-ne v1, v2, :cond_c */
/* .line 1254 */
/* iget v2, p0, Lcom/google/gson/stream/JsonReader;->pos:I */
/* iget v4, p0, Lcom/google/gson/stream/JsonReader;->peekedNumberLength:I */
/* add-int/2addr v2, v4 */
/* iput v2, p0, Lcom/google/gson/stream/JsonReader;->pos:I */
/* .line 1252 */
} // :cond_9
} // :goto_0
/* const/16 v2, 0x22 */
/* invoke-direct {p0, v2}, Lcom/google/gson/stream/JsonReader;->skipQuotedValue(C)V */
/* .line 1250 */
} // :cond_a
} // :goto_1
/* const/16 v2, 0x27 */
/* invoke-direct {p0, v2}, Lcom/google/gson/stream/JsonReader;->skipQuotedValue(C)V */
/* .line 1248 */
} // :cond_b
} // :goto_2
/* invoke-direct {p0}, Lcom/google/gson/stream/JsonReader;->skipUnquotedValue()V */
/* .line 1256 */
} // :cond_c
} // :goto_3
int v2 = 0; // const/4 v2, 0x0
/* iput v2, p0, Lcom/google/gson/stream/JsonReader;->peeked:I */
/* .line 1257 */
} // .end local v1 # "p":I
/* if-nez v0, :cond_0 */
/* .line 1259 */
v1 = this.pathIndices;
/* iget v2, p0, Lcom/google/gson/stream/JsonReader;->stackSize:I */
/* add-int/lit8 v4, v2, -0x1 */
/* aget v5, v1, v4 */
/* add-int/2addr v5, v3 */
/* aput v5, v1, v4 */
/* .line 1260 */
v1 = this.pathNames;
/* sub-int/2addr v2, v3 */
final String v3 = "null"; // const-string v3, "null"
/* aput-object v3, v1, v2 */
/* .line 1261 */
return;
} // .end method
public java.lang.String toString ( ) {
/* .locals 2 */
/* .line 1449 */
/* new-instance v0, Ljava/lang/StringBuilder; */
/* invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V */
(( java.lang.Object ) p0 ).getClass ( ); // invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;
(( java.lang.Class ) v1 ).getSimpleName ( ); // invoke-virtual {v1}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;
(( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( com.google.gson.stream.JsonReader ) p0 ).locationString ( ); // invoke-virtual {p0}, Lcom/google/gson/stream/JsonReader;->locationString()Ljava/lang/String;
(( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v0 ).toString ( ); // invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
} // .end method
