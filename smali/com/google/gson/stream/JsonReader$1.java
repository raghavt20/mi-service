class com.google.gson.stream.JsonReader$1 extends com.google.gson.internal.JsonReaderInternalAccess {
	 /* .source "JsonReader.java" */
	 /* # annotations */
	 /* .annotation system Ldalvik/annotation/EnclosingClass; */
	 /* value = Lcom/google/gson/stream/JsonReader; */
} // .end annotation
/* .annotation system Ldalvik/annotation/InnerClass; */
/* accessFlags = 0x0 */
/* name = null */
} // .end annotation
/* # direct methods */
 com.google.gson.stream.JsonReader$1 ( ) {
/* .locals 0 */
/* .line 1623 */
/* invoke-direct {p0}, Lcom/google/gson/internal/JsonReaderInternalAccess;-><init>()V */
return;
} // .end method
/* # virtual methods */
public void promoteNameToValue ( com.google.gson.stream.JsonReader p0 ) {
/* .locals 4 */
/* .param p1, "reader" # Lcom/google/gson/stream/JsonReader; */
/* .annotation system Ldalvik/annotation/Throws; */
/* value = { */
/* Ljava/io/IOException; */
/* } */
} // .end annotation
/* .line 1625 */
/* instance-of v0, p1, Lcom/google/gson/internal/bind/JsonTreeReader; */
if ( v0 != null) { // if-eqz v0, :cond_0
/* .line 1626 */
/* move-object v0, p1 */
/* check-cast v0, Lcom/google/gson/internal/bind/JsonTreeReader; */
(( com.google.gson.internal.bind.JsonTreeReader ) v0 ).promoteNameToValue ( ); // invoke-virtual {v0}, Lcom/google/gson/internal/bind/JsonTreeReader;->promoteNameToValue()V
/* .line 1627 */
return;
/* .line 1629 */
} // :cond_0
/* iget v0, p1, Lcom/google/gson/stream/JsonReader;->peeked:I */
/* .line 1630 */
/* .local v0, "p":I */
/* if-nez v0, :cond_1 */
/* .line 1631 */
v0 = (( com.google.gson.stream.JsonReader ) p1 ).doPeek ( ); // invoke-virtual {p1}, Lcom/google/gson/stream/JsonReader;->doPeek()I
/* .line 1633 */
} // :cond_1
/* const/16 v1, 0xd */
/* if-ne v0, v1, :cond_2 */
/* .line 1634 */
/* const/16 v1, 0x9 */
/* iput v1, p1, Lcom/google/gson/stream/JsonReader;->peeked:I */
/* .line 1635 */
} // :cond_2
/* const/16 v1, 0xc */
/* if-ne v0, v1, :cond_3 */
/* .line 1636 */
/* const/16 v1, 0x8 */
/* iput v1, p1, Lcom/google/gson/stream/JsonReader;->peeked:I */
/* .line 1637 */
} // :cond_3
/* const/16 v1, 0xe */
/* if-ne v0, v1, :cond_4 */
/* .line 1638 */
/* const/16 v1, 0xa */
/* iput v1, p1, Lcom/google/gson/stream/JsonReader;->peeked:I */
/* .line 1643 */
} // :goto_0
return;
/* .line 1640 */
} // :cond_4
/* new-instance v1, Ljava/lang/IllegalStateException; */
/* new-instance v2, Ljava/lang/StringBuilder; */
/* invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V */
final String v3 = "Expected a name but was "; // const-string v3, "Expected a name but was "
(( java.lang.StringBuilder ) v2 ).append ( v3 ); // invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
/* .line 1641 */
(( com.google.gson.stream.JsonReader ) p1 ).peek ( ); // invoke-virtual {p1}, Lcom/google/gson/stream/JsonReader;->peek()Lcom/google/gson/stream/JsonToken;
(( java.lang.StringBuilder ) v2 ).append ( v3 ); // invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;
(( com.google.gson.stream.JsonReader ) p1 ).locationString ( ); // invoke-virtual {p1}, Lcom/google/gson/stream/JsonReader;->locationString()Ljava/lang/String;
(( java.lang.StringBuilder ) v2 ).append ( v3 ); // invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v2 ).toString ( ); // invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
/* invoke-direct {v1, v2}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V */
/* throw v1 */
} // .end method
