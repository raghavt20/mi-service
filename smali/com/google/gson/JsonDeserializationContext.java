public abstract class com.google.gson.JsonDeserializationContext {
	 /* .source "JsonDeserializationContext.java" */
	 /* # virtual methods */
	 public abstract java.lang.Object deserialize ( com.google.gson.JsonElement p0, java.lang.reflect.Type p1 ) {
		 /* .annotation system Ldalvik/annotation/Signature; */
		 /* value = { */
		 /* "<T:", */
		 /* "Ljava/lang/Object;", */
		 /* ">(", */
		 /* "Lcom/google/gson/JsonElement;", */
		 /* "Ljava/lang/reflect/Type;", */
		 /* ")TT;" */
		 /* } */
	 } // .end annotation
	 /* .annotation system Ldalvik/annotation/Throws; */
	 /* value = { */
	 /* Lcom/google/gson/JsonParseException; */
	 /* } */
} // .end annotation
} // .end method
