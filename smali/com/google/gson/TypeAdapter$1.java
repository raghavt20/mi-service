class com.google.gson.TypeAdapter$1 extends com.google.gson.TypeAdapter {
	 /* .source "TypeAdapter.java" */
	 /* # annotations */
	 /* .annotation system Ldalvik/annotation/EnclosingMethod; */
	 /* value = Lcom/google/gson/TypeAdapter;->nullSafe()Lcom/google/gson/TypeAdapter; */
} // .end annotation
/* .annotation system Ldalvik/annotation/InnerClass; */
/* accessFlags = 0x0 */
/* name = null */
} // .end annotation
/* .annotation system Ldalvik/annotation/Signature; */
/* value = { */
/* "Lcom/google/gson/TypeAdapter<", */
/* "TT;>;" */
/* } */
} // .end annotation
/* # instance fields */
final com.google.gson.TypeAdapter this$0; //synthetic
/* # direct methods */
 com.google.gson.TypeAdapter$1 ( ) {
/* .locals 0 */
/* .param p1, "this$0" # Lcom/google/gson/TypeAdapter; */
/* .line 186 */
/* .local p0, "this":Lcom/google/gson/TypeAdapter$1;, "Lcom/google/gson/TypeAdapter$1;" */
this.this$0 = p1;
/* invoke-direct {p0}, Lcom/google/gson/TypeAdapter;-><init>()V */
return;
} // .end method
/* # virtual methods */
public java.lang.Object read ( com.google.gson.stream.JsonReader p0 ) {
/* .locals 2 */
/* .param p1, "reader" # Lcom/google/gson/stream/JsonReader; */
/* .annotation system Ldalvik/annotation/Signature; */
/* value = { */
/* "(", */
/* "Lcom/google/gson/stream/JsonReader;", */
/* ")TT;" */
/* } */
} // .end annotation
/* .annotation system Ldalvik/annotation/Throws; */
/* value = { */
/* Ljava/io/IOException; */
/* } */
} // .end annotation
/* .line 195 */
/* .local p0, "this":Lcom/google/gson/TypeAdapter$1;, "Lcom/google/gson/TypeAdapter$1;" */
(( com.google.gson.stream.JsonReader ) p1 ).peek ( ); // invoke-virtual {p1}, Lcom/google/gson/stream/JsonReader;->peek()Lcom/google/gson/stream/JsonToken;
v1 = com.google.gson.stream.JsonToken.NULL;
/* if-ne v0, v1, :cond_0 */
/* .line 196 */
(( com.google.gson.stream.JsonReader ) p1 ).nextNull ( ); // invoke-virtual {p1}, Lcom/google/gson/stream/JsonReader;->nextNull()V
/* .line 197 */
int v0 = 0; // const/4 v0, 0x0
/* .line 199 */
} // :cond_0
v0 = this.this$0;
(( com.google.gson.TypeAdapter ) v0 ).read ( p1 ); // invoke-virtual {v0, p1}, Lcom/google/gson/TypeAdapter;->read(Lcom/google/gson/stream/JsonReader;)Ljava/lang/Object;
} // .end method
public void write ( com.google.gson.stream.JsonWriter p0, java.lang.Object p1 ) {
/* .locals 1 */
/* .param p1, "out" # Lcom/google/gson/stream/JsonWriter; */
/* .annotation system Ldalvik/annotation/Signature; */
/* value = { */
/* "(", */
/* "Lcom/google/gson/stream/JsonWriter;", */
/* "TT;)V" */
/* } */
} // .end annotation
/* .annotation system Ldalvik/annotation/Throws; */
/* value = { */
/* Ljava/io/IOException; */
/* } */
} // .end annotation
/* .line 188 */
/* .local p0, "this":Lcom/google/gson/TypeAdapter$1;, "Lcom/google/gson/TypeAdapter$1;" */
/* .local p2, "value":Ljava/lang/Object;, "TT;" */
/* if-nez p2, :cond_0 */
/* .line 189 */
(( com.google.gson.stream.JsonWriter ) p1 ).nullValue ( ); // invoke-virtual {p1}, Lcom/google/gson/stream/JsonWriter;->nullValue()Lcom/google/gson/stream/JsonWriter;
/* .line 191 */
} // :cond_0
v0 = this.this$0;
(( com.google.gson.TypeAdapter ) v0 ).write ( p1, p2 ); // invoke-virtual {v0, p1, p2}, Lcom/google/gson/TypeAdapter;->write(Lcom/google/gson/stream/JsonWriter;Ljava/lang/Object;)V
/* .line 193 */
} // :goto_0
return;
} // .end method
