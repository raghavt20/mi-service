class com.google.gson.Gson$2 extends com.google.gson.TypeAdapter {
	 /* .source "Gson.java" */
	 /* # annotations */
	 /* .annotation system Ldalvik/annotation/EnclosingMethod; */
	 /* value = Lcom/google/gson/Gson;->floatAdapter(Z)Lcom/google/gson/TypeAdapter; */
} // .end annotation
/* .annotation system Ldalvik/annotation/InnerClass; */
/* accessFlags = 0x0 */
/* name = null */
} // .end annotation
/* .annotation system Ldalvik/annotation/Signature; */
/* value = { */
/* "Lcom/google/gson/TypeAdapter<", */
/* "Ljava/lang/Number;", */
/* ">;" */
/* } */
} // .end annotation
/* # instance fields */
final com.google.gson.Gson this$0; //synthetic
/* # direct methods */
 com.google.gson.Gson$2 ( ) {
/* .locals 0 */
/* .param p1, "this$0" # Lcom/google/gson/Gson; */
/* .line 380 */
this.this$0 = p1;
/* invoke-direct {p0}, Lcom/google/gson/TypeAdapter;-><init>()V */
return;
} // .end method
/* # virtual methods */
public java.lang.Float read ( com.google.gson.stream.JsonReader p0 ) {
/* .locals 2 */
/* .param p1, "in" # Lcom/google/gson/stream/JsonReader; */
/* .annotation system Ldalvik/annotation/Throws; */
/* value = { */
/* Ljava/io/IOException; */
/* } */
} // .end annotation
/* .line 382 */
(( com.google.gson.stream.JsonReader ) p1 ).peek ( ); // invoke-virtual {p1}, Lcom/google/gson/stream/JsonReader;->peek()Lcom/google/gson/stream/JsonToken;
v1 = com.google.gson.stream.JsonToken.NULL;
/* if-ne v0, v1, :cond_0 */
/* .line 383 */
(( com.google.gson.stream.JsonReader ) p1 ).nextNull ( ); // invoke-virtual {p1}, Lcom/google/gson/stream/JsonReader;->nextNull()V
/* .line 384 */
int v0 = 0; // const/4 v0, 0x0
/* .line 386 */
} // :cond_0
(( com.google.gson.stream.JsonReader ) p1 ).nextDouble ( ); // invoke-virtual {p1}, Lcom/google/gson/stream/JsonReader;->nextDouble()D
/* move-result-wide v0 */
/* double-to-float v0, v0 */
java.lang.Float .valueOf ( v0 );
} // .end method
public java.lang.Object read ( com.google.gson.stream.JsonReader p0 ) { //bridge//synthethic
/* .locals 0 */
/* .annotation system Ldalvik/annotation/Throws; */
/* value = { */
/* Ljava/io/IOException; */
/* } */
} // .end annotation
/* .line 380 */
(( com.google.gson.Gson$2 ) p0 ).read ( p1 ); // invoke-virtual {p0, p1}, Lcom/google/gson/Gson$2;->read(Lcom/google/gson/stream/JsonReader;)Ljava/lang/Float;
} // .end method
public void write ( com.google.gson.stream.JsonWriter p0, java.lang.Number p1 ) {
/* .locals 3 */
/* .param p1, "out" # Lcom/google/gson/stream/JsonWriter; */
/* .param p2, "value" # Ljava/lang/Number; */
/* .annotation system Ldalvik/annotation/Throws; */
/* value = { */
/* Ljava/io/IOException; */
/* } */
} // .end annotation
/* .line 389 */
/* if-nez p2, :cond_0 */
/* .line 390 */
(( com.google.gson.stream.JsonWriter ) p1 ).nullValue ( ); // invoke-virtual {p1}, Lcom/google/gson/stream/JsonWriter;->nullValue()Lcom/google/gson/stream/JsonWriter;
/* .line 391 */
return;
/* .line 393 */
} // :cond_0
v0 = (( java.lang.Number ) p2 ).floatValue ( ); // invoke-virtual {p2}, Ljava/lang/Number;->floatValue()F
/* .line 394 */
/* .local v0, "floatValue":F */
/* float-to-double v1, v0 */
com.google.gson.Gson .checkValidFloatingPoint ( v1,v2 );
/* .line 395 */
(( com.google.gson.stream.JsonWriter ) p1 ).value ( p2 ); // invoke-virtual {p1, p2}, Lcom/google/gson/stream/JsonWriter;->value(Ljava/lang/Number;)Lcom/google/gson/stream/JsonWriter;
/* .line 396 */
return;
} // .end method
public void write ( com.google.gson.stream.JsonWriter p0, java.lang.Object p1 ) { //bridge//synthethic
/* .locals 0 */
/* .annotation system Ldalvik/annotation/Throws; */
/* value = { */
/* Ljava/io/IOException; */
/* } */
} // .end annotation
/* .line 380 */
/* check-cast p2, Ljava/lang/Number; */
(( com.google.gson.Gson$2 ) p0 ).write ( p1, p2 ); // invoke-virtual {p0, p1, p2}, Lcom/google/gson/Gson$2;->write(Lcom/google/gson/stream/JsonWriter;Ljava/lang/Number;)V
return;
} // .end method
