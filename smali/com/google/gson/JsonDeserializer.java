public abstract class com.google.gson.JsonDeserializer {
	 /* .source "JsonDeserializer.java" */
	 /* # annotations */
	 /* .annotation system Ldalvik/annotation/Signature; */
	 /* value = { */
	 /* "<T:", */
	 /* "Ljava/lang/Object;", */
	 /* ">", */
	 /* "Ljava/lang/Object;" */
	 /* } */
} // .end annotation
/* # virtual methods */
public abstract java.lang.Object deserialize ( com.google.gson.JsonElement p0, java.lang.reflect.Type p1, com.google.gson.JsonDeserializationContext p2 ) {
	 /* .annotation system Ldalvik/annotation/Signature; */
	 /* value = { */
	 /* "(", */
	 /* "Lcom/google/gson/JsonElement;", */
	 /* "Ljava/lang/reflect/Type;", */
	 /* "Lcom/google/gson/JsonDeserializationContext;", */
	 /* ")TT;" */
	 /* } */
} // .end annotation
/* .annotation system Ldalvik/annotation/Throws; */
/* value = { */
/* Lcom/google/gson/JsonParseException; */
/* } */
} // .end annotation
} // .end method
