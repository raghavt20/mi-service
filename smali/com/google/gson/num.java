public abstract class num extends java.lang.Enum implements com.google.gson.FieldNamingStrategy {
	 /* .source "FieldNamingPolicy.java" */
	 /* # interfaces */
	 /* # annotations */
	 /* .annotation system Ldalvik/annotation/Signature; */
	 /* value = { */
	 /* "Ljava/lang/Enum<", */
	 /* "Lcom/google/gson/FieldNamingPolicy;", */
	 /* ">;", */
	 /* "Lcom/google/gson/FieldNamingStrategy;" */
	 /* } */
} // .end annotation
/* # static fields */
private static final com.google.gson.FieldNamingPolicy $VALUES; //synthetic
public static final com.google.gson.FieldNamingPolicy IDENTITY;
public static final com.google.gson.FieldNamingPolicy LOWER_CASE_WITH_DASHES;
public static final com.google.gson.FieldNamingPolicy LOWER_CASE_WITH_DOTS;
public static final com.google.gson.FieldNamingPolicy LOWER_CASE_WITH_UNDERSCORES;
public static final com.google.gson.FieldNamingPolicy UPPER_CAMEL_CASE;
public static final com.google.gson.FieldNamingPolicy UPPER_CAMEL_CASE_WITH_SPACES;
public static final com.google.gson.FieldNamingPolicy UPPER_CASE_WITH_UNDERSCORES;
/* # direct methods */
static num ( ) {
	 /* .locals 9 */
	 /* .line 37 */
	 /* new-instance v0, Lcom/google/gson/FieldNamingPolicy$1; */
	 final String v1 = "IDENTITY"; // const-string v1, "IDENTITY"
	 int v2 = 0; // const/4 v2, 0x0
	 /* invoke-direct {v0, v1, v2}, Lcom/google/gson/FieldNamingPolicy$1;-><init>(Ljava/lang/String;I)V */
	 /* .line 53 */
	 /* new-instance v1, Lcom/google/gson/FieldNamingPolicy$2; */
	 final String v2 = "UPPER_CAMEL_CASE"; // const-string v2, "UPPER_CAMEL_CASE"
	 int v3 = 1; // const/4 v3, 0x1
	 /* invoke-direct {v1, v2, v3}, Lcom/google/gson/FieldNamingPolicy$2;-><init>(Ljava/lang/String;I)V */
	 /* .line 72 */
	 /* new-instance v2, Lcom/google/gson/FieldNamingPolicy$3; */
	 final String v3 = "UPPER_CAMEL_CASE_WITH_SPACES"; // const-string v3, "UPPER_CAMEL_CASE_WITH_SPACES"
	 int v4 = 2; // const/4 v4, 0x2
	 /* invoke-direct {v2, v3, v4}, Lcom/google/gson/FieldNamingPolicy$3;-><init>(Ljava/lang/String;I)V */
	 /* .line 90 */
	 /* new-instance v3, Lcom/google/gson/FieldNamingPolicy$4; */
	 final String v4 = "UPPER_CASE_WITH_UNDERSCORES"; // const-string v4, "UPPER_CASE_WITH_UNDERSCORES"
	 int v5 = 3; // const/4 v5, 0x3
	 /* invoke-direct {v3, v4, v5}, Lcom/google/gson/FieldNamingPolicy$4;-><init>(Ljava/lang/String;I)V */
	 /* .line 108 */
	 /* new-instance v4, Lcom/google/gson/FieldNamingPolicy$5; */
	 final String v5 = "LOWER_CASE_WITH_UNDERSCORES"; // const-string v5, "LOWER_CASE_WITH_UNDERSCORES"
	 int v6 = 4; // const/4 v6, 0x4
	 /* invoke-direct {v4, v5, v6}, Lcom/google/gson/FieldNamingPolicy$5;-><init>(Ljava/lang/String;I)V */
	 /* .line 131 */
	 /* new-instance v5, Lcom/google/gson/FieldNamingPolicy$6; */
	 final String v6 = "LOWER_CASE_WITH_DASHES"; // const-string v6, "LOWER_CASE_WITH_DASHES"
	 int v7 = 5; // const/4 v7, 0x5
	 /* invoke-direct {v5, v6, v7}, Lcom/google/gson/FieldNamingPolicy$6;-><init>(Ljava/lang/String;I)V */
	 /* .line 154 */
	 /* new-instance v6, Lcom/google/gson/FieldNamingPolicy$7; */
	 final String v7 = "LOWER_CASE_WITH_DOTS"; // const-string v7, "LOWER_CASE_WITH_DOTS"
	 int v8 = 6; // const/4 v8, 0x6
	 /* invoke-direct {v6, v7, v8}, Lcom/google/gson/FieldNamingPolicy$7;-><init>(Ljava/lang/String;I)V */
	 /* .line 31 */
	 /* filled-new-array/range {v0 ..v6}, [Lcom/google/gson/FieldNamingPolicy; */
	 return;
} // .end method
private num ( ) {
	 /* .locals 0 */
	 /* .annotation system Ldalvik/annotation/Signature; */
	 /* value = { */
	 /* "()V" */
	 /* } */
} // .end annotation
/* .line 31 */
/* invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V */
return;
} // .end method
 num ( ) { //synthethic
/* .locals 0 */
/* .param p1, "x0" # Ljava/lang/String; */
/* .param p2, "x1" # I */
/* .param p3, "x2" # Lcom/google/gson/FieldNamingPolicy$1; */
/* .line 31 */
/* invoke-direct {p0, p1, p2}, Lcom/google/gson/FieldNamingPolicy;-><init>(Ljava/lang/String;I)V */
return;
} // .end method
static java.lang.String separateCamelCase ( java.lang.String p0, Object p1 ) {
/* .locals 5 */
/* .param p0, "name" # Ljava/lang/String; */
/* .param p1, "separator" # C */
/* .line 165 */
/* new-instance v0, Ljava/lang/StringBuilder; */
/* invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V */
/* .line 166 */
/* .local v0, "translation":Ljava/lang/StringBuilder; */
int v1 = 0; // const/4 v1, 0x0
/* .local v1, "i":I */
v2 = (( java.lang.String ) p0 ).length ( ); // invoke-virtual {p0}, Ljava/lang/String;->length()I
/* .local v2, "length":I */
} // :goto_0
/* if-ge v1, v2, :cond_1 */
/* .line 167 */
v3 = (( java.lang.String ) p0 ).charAt ( v1 ); // invoke-virtual {p0, v1}, Ljava/lang/String;->charAt(I)C
/* .line 168 */
/* .local v3, "character":C */
v4 = java.lang.Character .isUpperCase ( v3 );
if ( v4 != null) { // if-eqz v4, :cond_0
v4 = (( java.lang.StringBuilder ) v0 ).length ( ); // invoke-virtual {v0}, Ljava/lang/StringBuilder;->length()I
if ( v4 != null) { // if-eqz v4, :cond_0
	 /* .line 169 */
	 (( java.lang.StringBuilder ) v0 ).append ( p1 ); // invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;
	 /* .line 171 */
} // :cond_0
(( java.lang.StringBuilder ) v0 ).append ( v3 ); // invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;
/* .line 166 */
} // .end local v3 # "character":C
/* add-int/lit8 v1, v1, 0x1 */
/* .line 173 */
} // .end local v1 # "i":I
} // .end local v2 # "length":I
} // :cond_1
(( java.lang.StringBuilder ) v0 ).toString ( ); // invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
} // .end method
static java.lang.String upperCaseFirstLetter ( java.lang.String p0 ) {
/* .locals 6 */
/* .param p0, "s" # Ljava/lang/String; */
/* .line 180 */
v0 = (( java.lang.String ) p0 ).length ( ); // invoke-virtual {p0}, Ljava/lang/String;->length()I
/* .line 181 */
/* .local v0, "length":I */
int v1 = 0; // const/4 v1, 0x0
/* .local v1, "i":I */
} // :goto_0
/* if-ge v1, v0, :cond_3 */
/* .line 182 */
v2 = (( java.lang.String ) p0 ).charAt ( v1 ); // invoke-virtual {p0, v1}, Ljava/lang/String;->charAt(I)C
/* .line 183 */
/* .local v2, "c":C */
v3 = java.lang.Character .isLetter ( v2 );
if ( v3 != null) { // if-eqz v3, :cond_2
/* .line 184 */
v3 = java.lang.Character .isUpperCase ( v2 );
if ( v3 != null) { // if-eqz v3, :cond_0
/* .line 185 */
/* .line 188 */
} // :cond_0
v3 = java.lang.Character .toUpperCase ( v2 );
/* .line 190 */
/* .local v3, "uppercased":C */
/* if-nez v1, :cond_1 */
/* .line 191 */
/* new-instance v4, Ljava/lang/StringBuilder; */
/* invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V */
(( java.lang.StringBuilder ) v4 ).append ( v3 ); // invoke-virtual {v4, v3}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;
int v5 = 1; // const/4 v5, 0x1
(( java.lang.String ) p0 ).substring ( v5 ); // invoke-virtual {p0, v5}, Ljava/lang/String;->substring(I)Ljava/lang/String;
(( java.lang.StringBuilder ) v4 ).append ( v5 ); // invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v4 ).toString ( ); // invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
/* .line 193 */
} // :cond_1
/* new-instance v4, Ljava/lang/StringBuilder; */
/* invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V */
int v5 = 0; // const/4 v5, 0x0
(( java.lang.String ) p0 ).substring ( v5, v1 ); // invoke-virtual {p0, v5, v1}, Ljava/lang/String;->substring(II)Ljava/lang/String;
(( java.lang.StringBuilder ) v4 ).append ( v5 ); // invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v4 ).append ( v3 ); // invoke-virtual {v4, v3}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;
/* add-int/lit8 v5, v1, 0x1 */
(( java.lang.String ) p0 ).substring ( v5 ); // invoke-virtual {p0, v5}, Ljava/lang/String;->substring(I)Ljava/lang/String;
(( java.lang.StringBuilder ) v4 ).append ( v5 ); // invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v4 ).toString ( ); // invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
/* .line 181 */
} // .end local v2 # "c":C
} // .end local v3 # "uppercased":C
} // :cond_2
/* add-int/lit8 v1, v1, 0x1 */
/* .line 198 */
} // .end local v1 # "i":I
} // :cond_3
} // .end method
public static com.google.gson.FieldNamingPolicy valueOf ( java.lang.String p0 ) {
/* .locals 1 */
/* .param p0, "name" # Ljava/lang/String; */
/* .line 31 */
/* const-class v0, Lcom/google/gson/FieldNamingPolicy; */
java.lang.Enum .valueOf ( v0,p0 );
/* check-cast v0, Lcom/google/gson/FieldNamingPolicy; */
} // .end method
public static com.google.gson.FieldNamingPolicy values ( ) {
/* .locals 1 */
/* .line 31 */
v0 = com.google.gson.FieldNamingPolicy.$VALUES;
(( com.google.gson.FieldNamingPolicy ) v0 ).clone ( ); // invoke-virtual {v0}, [Lcom/google/gson/FieldNamingPolicy;->clone()Ljava/lang/Object;
/* check-cast v0, [Lcom/google/gson/FieldNamingPolicy; */
} // .end method
