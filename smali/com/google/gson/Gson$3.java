class com.google.gson.Gson$3 extends com.google.gson.TypeAdapter {
	 /* .source "Gson.java" */
	 /* # annotations */
	 /* .annotation system Ldalvik/annotation/EnclosingMethod; */
	 /* value = Lcom/google/gson/Gson;->longAdapter(Lcom/google/gson/LongSerializationPolicy;)Lcom/google/gson/TypeAdapter; */
} // .end annotation
/* .annotation system Ldalvik/annotation/InnerClass; */
/* accessFlags = 0x0 */
/* name = null */
} // .end annotation
/* .annotation system Ldalvik/annotation/Signature; */
/* value = { */
/* "Lcom/google/gson/TypeAdapter<", */
/* "Ljava/lang/Number;", */
/* ">;" */
/* } */
} // .end annotation
/* # direct methods */
 com.google.gson.Gson$3 ( ) {
/* .locals 0 */
/* .line 412 */
/* invoke-direct {p0}, Lcom/google/gson/TypeAdapter;-><init>()V */
return;
} // .end method
/* # virtual methods */
public java.lang.Number read ( com.google.gson.stream.JsonReader p0 ) {
/* .locals 2 */
/* .param p1, "in" # Lcom/google/gson/stream/JsonReader; */
/* .annotation system Ldalvik/annotation/Throws; */
/* value = { */
/* Ljava/io/IOException; */
/* } */
} // .end annotation
/* .line 414 */
(( com.google.gson.stream.JsonReader ) p1 ).peek ( ); // invoke-virtual {p1}, Lcom/google/gson/stream/JsonReader;->peek()Lcom/google/gson/stream/JsonToken;
v1 = com.google.gson.stream.JsonToken.NULL;
/* if-ne v0, v1, :cond_0 */
/* .line 415 */
(( com.google.gson.stream.JsonReader ) p1 ).nextNull ( ); // invoke-virtual {p1}, Lcom/google/gson/stream/JsonReader;->nextNull()V
/* .line 416 */
int v0 = 0; // const/4 v0, 0x0
/* .line 418 */
} // :cond_0
(( com.google.gson.stream.JsonReader ) p1 ).nextLong ( ); // invoke-virtual {p1}, Lcom/google/gson/stream/JsonReader;->nextLong()J
/* move-result-wide v0 */
java.lang.Long .valueOf ( v0,v1 );
} // .end method
public java.lang.Object read ( com.google.gson.stream.JsonReader p0 ) { //bridge//synthethic
/* .locals 0 */
/* .annotation system Ldalvik/annotation/Throws; */
/* value = { */
/* Ljava/io/IOException; */
/* } */
} // .end annotation
/* .line 412 */
(( com.google.gson.Gson$3 ) p0 ).read ( p1 ); // invoke-virtual {p0, p1}, Lcom/google/gson/Gson$3;->read(Lcom/google/gson/stream/JsonReader;)Ljava/lang/Number;
} // .end method
public void write ( com.google.gson.stream.JsonWriter p0, java.lang.Number p1 ) {
/* .locals 1 */
/* .param p1, "out" # Lcom/google/gson/stream/JsonWriter; */
/* .param p2, "value" # Ljava/lang/Number; */
/* .annotation system Ldalvik/annotation/Throws; */
/* value = { */
/* Ljava/io/IOException; */
/* } */
} // .end annotation
/* .line 421 */
/* if-nez p2, :cond_0 */
/* .line 422 */
(( com.google.gson.stream.JsonWriter ) p1 ).nullValue ( ); // invoke-virtual {p1}, Lcom/google/gson/stream/JsonWriter;->nullValue()Lcom/google/gson/stream/JsonWriter;
/* .line 423 */
return;
/* .line 425 */
} // :cond_0
(( java.lang.Object ) p2 ).toString ( ); // invoke-virtual {p2}, Ljava/lang/Object;->toString()Ljava/lang/String;
(( com.google.gson.stream.JsonWriter ) p1 ).value ( v0 ); // invoke-virtual {p1, v0}, Lcom/google/gson/stream/JsonWriter;->value(Ljava/lang/String;)Lcom/google/gson/stream/JsonWriter;
/* .line 426 */
return;
} // .end method
public void write ( com.google.gson.stream.JsonWriter p0, java.lang.Object p1 ) { //bridge//synthethic
/* .locals 0 */
/* .annotation system Ldalvik/annotation/Throws; */
/* value = { */
/* Ljava/io/IOException; */
/* } */
} // .end annotation
/* .line 412 */
/* check-cast p2, Ljava/lang/Number; */
(( com.google.gson.Gson$3 ) p0 ).write ( p1, p2 ); // invoke-virtual {p0, p1, p2}, Lcom/google/gson/Gson$3;->write(Lcom/google/gson/stream/JsonWriter;Ljava/lang/Number;)V
return;
} // .end method
