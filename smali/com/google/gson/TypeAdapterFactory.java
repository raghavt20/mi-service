public abstract class com.google.gson.TypeAdapterFactory {
	 /* .source "TypeAdapterFactory.java" */
	 /* # virtual methods */
	 public abstract com.google.gson.TypeAdapter create ( com.google.gson.Gson p0, com.google.gson.reflect.TypeToken p1 ) {
		 /* .annotation system Ldalvik/annotation/Signature; */
		 /* value = { */
		 /* "<T:", */
		 /* "Ljava/lang/Object;", */
		 /* ">(", */
		 /* "Lcom/google/gson/Gson;", */
		 /* "Lcom/google/gson/reflect/TypeToken<", */
		 /* "TT;>;)", */
		 /* "Lcom/google/gson/TypeAdapter<", */
		 /* "TT;>;" */
		 /* } */
	 } // .end annotation
} // .end method
