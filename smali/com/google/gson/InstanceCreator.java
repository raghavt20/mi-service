public abstract class com.google.gson.InstanceCreator {
	 /* .source "InstanceCreator.java" */
	 /* # annotations */
	 /* .annotation system Ldalvik/annotation/Signature; */
	 /* value = { */
	 /* "<T:", */
	 /* "Ljava/lang/Object;", */
	 /* ">", */
	 /* "Ljava/lang/Object;" */
	 /* } */
} // .end annotation
/* # virtual methods */
public abstract java.lang.Object createInstance ( java.lang.reflect.Type p0 ) {
	 /* .annotation system Ldalvik/annotation/Signature; */
	 /* value = { */
	 /* "(", */
	 /* "Ljava/lang/reflect/Type;", */
	 /* ")TT;" */
	 /* } */
} // .end annotation
} // .end method
