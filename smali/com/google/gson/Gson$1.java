class com.google.gson.Gson$1 extends com.google.gson.TypeAdapter {
	 /* .source "Gson.java" */
	 /* # annotations */
	 /* .annotation system Ldalvik/annotation/EnclosingMethod; */
	 /* value = Lcom/google/gson/Gson;->doubleAdapter(Z)Lcom/google/gson/TypeAdapter; */
} // .end annotation
/* .annotation system Ldalvik/annotation/InnerClass; */
/* accessFlags = 0x0 */
/* name = null */
} // .end annotation
/* .annotation system Ldalvik/annotation/Signature; */
/* value = { */
/* "Lcom/google/gson/TypeAdapter<", */
/* "Ljava/lang/Number;", */
/* ">;" */
/* } */
} // .end annotation
/* # instance fields */
final com.google.gson.Gson this$0; //synthetic
/* # direct methods */
 com.google.gson.Gson$1 ( ) {
/* .locals 0 */
/* .param p1, "this$0" # Lcom/google/gson/Gson; */
/* .line 356 */
this.this$0 = p1;
/* invoke-direct {p0}, Lcom/google/gson/TypeAdapter;-><init>()V */
return;
} // .end method
/* # virtual methods */
public java.lang.Double read ( com.google.gson.stream.JsonReader p0 ) {
/* .locals 2 */
/* .param p1, "in" # Lcom/google/gson/stream/JsonReader; */
/* .annotation system Ldalvik/annotation/Throws; */
/* value = { */
/* Ljava/io/IOException; */
/* } */
} // .end annotation
/* .line 358 */
(( com.google.gson.stream.JsonReader ) p1 ).peek ( ); // invoke-virtual {p1}, Lcom/google/gson/stream/JsonReader;->peek()Lcom/google/gson/stream/JsonToken;
v1 = com.google.gson.stream.JsonToken.NULL;
/* if-ne v0, v1, :cond_0 */
/* .line 359 */
(( com.google.gson.stream.JsonReader ) p1 ).nextNull ( ); // invoke-virtual {p1}, Lcom/google/gson/stream/JsonReader;->nextNull()V
/* .line 360 */
int v0 = 0; // const/4 v0, 0x0
/* .line 362 */
} // :cond_0
(( com.google.gson.stream.JsonReader ) p1 ).nextDouble ( ); // invoke-virtual {p1}, Lcom/google/gson/stream/JsonReader;->nextDouble()D
/* move-result-wide v0 */
java.lang.Double .valueOf ( v0,v1 );
} // .end method
public java.lang.Object read ( com.google.gson.stream.JsonReader p0 ) { //bridge//synthethic
/* .locals 0 */
/* .annotation system Ldalvik/annotation/Throws; */
/* value = { */
/* Ljava/io/IOException; */
/* } */
} // .end annotation
/* .line 356 */
(( com.google.gson.Gson$1 ) p0 ).read ( p1 ); // invoke-virtual {p0, p1}, Lcom/google/gson/Gson$1;->read(Lcom/google/gson/stream/JsonReader;)Ljava/lang/Double;
} // .end method
public void write ( com.google.gson.stream.JsonWriter p0, java.lang.Number p1 ) {
/* .locals 2 */
/* .param p1, "out" # Lcom/google/gson/stream/JsonWriter; */
/* .param p2, "value" # Ljava/lang/Number; */
/* .annotation system Ldalvik/annotation/Throws; */
/* value = { */
/* Ljava/io/IOException; */
/* } */
} // .end annotation
/* .line 365 */
/* if-nez p2, :cond_0 */
/* .line 366 */
(( com.google.gson.stream.JsonWriter ) p1 ).nullValue ( ); // invoke-virtual {p1}, Lcom/google/gson/stream/JsonWriter;->nullValue()Lcom/google/gson/stream/JsonWriter;
/* .line 367 */
return;
/* .line 369 */
} // :cond_0
(( java.lang.Number ) p2 ).doubleValue ( ); // invoke-virtual {p2}, Ljava/lang/Number;->doubleValue()D
/* move-result-wide v0 */
/* .line 370 */
/* .local v0, "doubleValue":D */
com.google.gson.Gson .checkValidFloatingPoint ( v0,v1 );
/* .line 371 */
(( com.google.gson.stream.JsonWriter ) p1 ).value ( p2 ); // invoke-virtual {p1, p2}, Lcom/google/gson/stream/JsonWriter;->value(Ljava/lang/Number;)Lcom/google/gson/stream/JsonWriter;
/* .line 372 */
return;
} // .end method
public void write ( com.google.gson.stream.JsonWriter p0, java.lang.Object p1 ) { //bridge//synthethic
/* .locals 0 */
/* .annotation system Ldalvik/annotation/Throws; */
/* value = { */
/* Ljava/io/IOException; */
/* } */
} // .end annotation
/* .line 356 */
/* check-cast p2, Ljava/lang/Number; */
(( com.google.gson.Gson$1 ) p0 ).write ( p1, p2 ); // invoke-virtual {p0, p1, p2}, Lcom/google/gson/Gson$1;->write(Lcom/google/gson/stream/JsonWriter;Ljava/lang/Number;)V
return;
} // .end method
