class com.google.gson.internal.UnsafeAllocator$1 extends com.google.gson.internal.UnsafeAllocator {
	 /* .source "UnsafeAllocator.java" */
	 /* # annotations */
	 /* .annotation system Ldalvik/annotation/EnclosingMethod; */
	 /* value = Lcom/google/gson/internal/UnsafeAllocator;->create()Lcom/google/gson/internal/UnsafeAllocator; */
} // .end annotation
/* .annotation system Ldalvik/annotation/InnerClass; */
/* accessFlags = 0x0 */
/* name = null */
} // .end annotation
/* # instance fields */
final java.lang.reflect.Method val$allocateInstance; //synthetic
final java.lang.Object val$unsafe; //synthetic
/* # direct methods */
 com.google.gson.internal.UnsafeAllocator$1 ( ) {
/* .locals 0 */
/* .line 45 */
this.val$allocateInstance = p1;
this.val$unsafe = p2;
/* invoke-direct {p0}, Lcom/google/gson/internal/UnsafeAllocator;-><init>()V */
return;
} // .end method
/* # virtual methods */
public java.lang.Object newInstance ( java.lang.Class p0 ) {
/* .locals 3 */
/* .annotation system Ldalvik/annotation/Signature; */
/* value = { */
/* "<T:", */
/* "Ljava/lang/Object;", */
/* ">(", */
/* "Ljava/lang/Class<", */
/* "TT;>;)TT;" */
/* } */
} // .end annotation
/* .annotation system Ldalvik/annotation/Throws; */
/* value = { */
/* Ljava/lang/Exception; */
/* } */
} // .end annotation
/* .line 49 */
/* .local p1, "c":Ljava/lang/Class;, "Ljava/lang/Class<TT;>;" */
com.google.gson.internal.UnsafeAllocator$1 .assertInstantiable ( p1 );
/* .line 50 */
v0 = this.val$allocateInstance;
v1 = this.val$unsafe;
/* filled-new-array {p1}, [Ljava/lang/Object; */
(( java.lang.reflect.Method ) v0 ).invoke ( v1, v2 ); // invoke-virtual {v0, v1, v2}, Ljava/lang/reflect/Method;->invoke(Ljava/lang/Object;[Ljava/lang/Object;)Ljava/lang/Object;
} // .end method
