class com.google.gson.internal.ConstructorConstructor$4 implements com.google.gson.internal.ObjectConstructor {
	 /* .source "ConstructorConstructor.java" */
	 /* # interfaces */
	 /* # annotations */
	 /* .annotation system Ldalvik/annotation/EnclosingMethod; */
	 /* value = Lcom/google/gson/internal/ConstructorConstructor;->newDefaultConstructor(Ljava/lang/Class;)Lcom/google/gson/internal/ObjectConstructor; */
} // .end annotation
/* .annotation system Ldalvik/annotation/InnerClass; */
/* accessFlags = 0x0 */
/* name = null */
} // .end annotation
/* .annotation system Ldalvik/annotation/Signature; */
/* value = { */
/* "Ljava/lang/Object;", */
/* "Lcom/google/gson/internal/ObjectConstructor<", */
/* "TT;>;" */
/* } */
} // .end annotation
/* # instance fields */
final com.google.gson.internal.ConstructorConstructor this$0; //synthetic
final java.lang.reflect.Constructor val$constructor; //synthetic
/* # direct methods */
 com.google.gson.internal.ConstructorConstructor$4 ( ) {
/* .locals 0 */
/* .param p1, "this$0" # Lcom/google/gson/internal/ConstructorConstructor; */
/* .line 136 */
this.this$0 = p1;
this.val$constructor = p2;
/* invoke-direct {p0}, Ljava/lang/Object;-><init>()V */
return;
} // .end method
/* # virtual methods */
public java.lang.Object construct ( ) {
/* .locals 5 */
/* .annotation system Ldalvik/annotation/Signature; */
/* value = { */
/* "()TT;" */
/* } */
} // .end annotation
/* .line 140 */
final String v0 = " with no args"; // const-string v0, " with no args"
final String v1 = "Failed to invoke "; // const-string v1, "Failed to invoke "
try { // :try_start_0
v2 = this.val$constructor;
int v3 = 0; // const/4 v3, 0x0
/* new-array v3, v3, [Ljava/lang/Object; */
(( java.lang.reflect.Constructor ) v2 ).newInstance ( v3 ); // invoke-virtual {v2, v3}, Ljava/lang/reflect/Constructor;->newInstance([Ljava/lang/Object;)Ljava/lang/Object;
/* :try_end_0 */
/* .catch Ljava/lang/InstantiationException; {:try_start_0 ..:try_end_0} :catch_2 */
/* .catch Ljava/lang/reflect/InvocationTargetException; {:try_start_0 ..:try_end_0} :catch_1 */
/* .catch Ljava/lang/IllegalAccessException; {:try_start_0 ..:try_end_0} :catch_0 */
/* .line 141 */
/* .local v0, "newInstance":Ljava/lang/Object;, "TT;" */
/* .line 150 */
} // .end local v0 # "newInstance":Ljava/lang/Object;, "TT;"
/* :catch_0 */
/* move-exception v0 */
/* .line 151 */
/* .local v0, "e":Ljava/lang/IllegalAccessException; */
/* new-instance v1, Ljava/lang/AssertionError; */
/* invoke-direct {v1, v0}, Ljava/lang/AssertionError;-><init>(Ljava/lang/Object;)V */
/* throw v1 */
/* .line 145 */
} // .end local v0 # "e":Ljava/lang/IllegalAccessException;
/* :catch_1 */
/* move-exception v2 */
/* .line 148 */
/* .local v2, "e":Ljava/lang/reflect/InvocationTargetException; */
/* new-instance v3, Ljava/lang/RuntimeException; */
/* new-instance v4, Ljava/lang/StringBuilder; */
/* invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V */
(( java.lang.StringBuilder ) v4 ).append ( v1 ); // invoke-virtual {v4, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
v4 = this.val$constructor;
(( java.lang.StringBuilder ) v1 ).append ( v4 ); // invoke-virtual {v1, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v1 ).append ( v0 ); // invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v0 ).toString ( ); // invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
/* .line 149 */
(( java.lang.reflect.InvocationTargetException ) v2 ).getTargetException ( ); // invoke-virtual {v2}, Ljava/lang/reflect/InvocationTargetException;->getTargetException()Ljava/lang/Throwable;
/* invoke-direct {v3, v0, v1}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;Ljava/lang/Throwable;)V */
/* throw v3 */
/* .line 142 */
} // .end local v2 # "e":Ljava/lang/reflect/InvocationTargetException;
/* :catch_2 */
/* move-exception v2 */
/* .line 144 */
/* .local v2, "e":Ljava/lang/InstantiationException; */
/* new-instance v3, Ljava/lang/RuntimeException; */
/* new-instance v4, Ljava/lang/StringBuilder; */
/* invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V */
(( java.lang.StringBuilder ) v4 ).append ( v1 ); // invoke-virtual {v4, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
v4 = this.val$constructor;
(( java.lang.StringBuilder ) v1 ).append ( v4 ); // invoke-virtual {v1, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v1 ).append ( v0 ); // invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v0 ).toString ( ); // invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
/* invoke-direct {v3, v0, v2}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;Ljava/lang/Throwable;)V */
/* throw v3 */
} // .end method
