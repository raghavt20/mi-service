public class com.google.gson.internal.reflect.ReflectionHelper {
	 /* .source "ReflectionHelper.java" */
	 /* # direct methods */
	 private com.google.gson.internal.reflect.ReflectionHelper ( ) {
		 /* .locals 0 */
		 /* .line 8 */
		 /* invoke-direct {p0}, Ljava/lang/Object;-><init>()V */
		 return;
	 } // .end method
	 private static java.lang.String constructorToString ( java.lang.reflect.Constructor p0 ) {
		 /* .locals 4 */
		 /* .annotation system Ldalvik/annotation/Signature; */
		 /* value = { */
		 /* "(", */
		 /* "Ljava/lang/reflect/Constructor<", */
		 /* "*>;)", */
		 /* "Ljava/lang/String;" */
		 /* } */
	 } // .end annotation
	 /* .line 32 */
	 /* .local p0, "constructor":Ljava/lang/reflect/Constructor;, "Ljava/lang/reflect/Constructor<*>;" */
	 /* new-instance v0, Ljava/lang/StringBuilder; */
	 (( java.lang.reflect.Constructor ) p0 ).getDeclaringClass ( ); // invoke-virtual {p0}, Ljava/lang/reflect/Constructor;->getDeclaringClass()Ljava/lang/Class;
	 (( java.lang.Class ) v1 ).getName ( ); // invoke-virtual {v1}, Ljava/lang/Class;->getName()Ljava/lang/String;
	 /* invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V */
	 /* .line 33 */
	 /* const/16 v1, 0x23 */
	 (( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;
	 /* .line 34 */
	 (( java.lang.reflect.Constructor ) p0 ).getDeclaringClass ( ); // invoke-virtual {p0}, Ljava/lang/reflect/Constructor;->getDeclaringClass()Ljava/lang/Class;
	 (( java.lang.Class ) v1 ).getSimpleName ( ); // invoke-virtual {v1}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;
	 (( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
	 /* .line 35 */
	 /* const/16 v1, 0x28 */
	 (( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;
	 /* .line 36 */
	 /* .local v0, "stringBuilder":Ljava/lang/StringBuilder; */
	 (( java.lang.reflect.Constructor ) p0 ).getParameterTypes ( ); // invoke-virtual {p0}, Ljava/lang/reflect/Constructor;->getParameterTypes()[Ljava/lang/Class;
	 /* .line 37 */
	 /* .local v1, "parameters":[Ljava/lang/Class;, "[Ljava/lang/Class<*>;" */
	 int v2 = 0; // const/4 v2, 0x0
	 /* .local v2, "i":I */
} // :goto_0
/* array-length v3, v1 */
/* if-ge v2, v3, :cond_1 */
/* .line 38 */
/* if-lez v2, :cond_0 */
/* .line 39 */
final String v3 = ", "; // const-string v3, ", "
(( java.lang.StringBuilder ) v0 ).append ( v3 ); // invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
/* .line 41 */
} // :cond_0
/* aget-object v3, v1, v2 */
(( java.lang.Class ) v3 ).getSimpleName ( ); // invoke-virtual {v3}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;
(( java.lang.StringBuilder ) v0 ).append ( v3 ); // invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
/* .line 37 */
/* add-int/lit8 v2, v2, 0x1 */
/* .line 44 */
} // .end local v2 # "i":I
} // :cond_1
/* const/16 v2, 0x29 */
(( java.lang.StringBuilder ) v0 ).append ( v2 ); // invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v2 ).toString ( ); // invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
} // .end method
public static void makeAccessible ( java.lang.reflect.Field p0 ) {
/* .locals 4 */
/* .param p0, "field" # Ljava/lang/reflect/Field; */
/* .annotation system Ldalvik/annotation/Throws; */
/* value = { */
/* Lcom/google/gson/JsonIOException; */
/* } */
} // .end annotation
/* .line 19 */
int v0 = 1; // const/4 v0, 0x1
try { // :try_start_0
(( java.lang.reflect.Field ) p0 ).setAccessible ( v0 ); // invoke-virtual {p0, v0}, Ljava/lang/reflect/Field;->setAccessible(Z)V
/* :try_end_0 */
/* .catch Ljava/lang/Exception; {:try_start_0 ..:try_end_0} :catch_0 */
/* .line 24 */
/* nop */
/* .line 25 */
return;
/* .line 20 */
/* :catch_0 */
/* move-exception v0 */
/* .line 21 */
/* .local v0, "exception":Ljava/lang/Exception; */
/* new-instance v1, Lcom/google/gson/JsonIOException; */
/* new-instance v2, Ljava/lang/StringBuilder; */
/* invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V */
final String v3 = "Failed making field \'"; // const-string v3, "Failed making field \'"
(( java.lang.StringBuilder ) v2 ).append ( v3 ); // invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.reflect.Field ) p0 ).getDeclaringClass ( ); // invoke-virtual {p0}, Ljava/lang/reflect/Field;->getDeclaringClass()Ljava/lang/Class;
(( java.lang.Class ) v3 ).getName ( ); // invoke-virtual {v3}, Ljava/lang/Class;->getName()Ljava/lang/String;
(( java.lang.StringBuilder ) v2 ).append ( v3 ); // invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
final String v3 = "#"; // const-string v3, "#"
(( java.lang.StringBuilder ) v2 ).append ( v3 ); // invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
/* .line 22 */
(( java.lang.reflect.Field ) p0 ).getName ( ); // invoke-virtual {p0}, Ljava/lang/reflect/Field;->getName()Ljava/lang/String;
(( java.lang.StringBuilder ) v2 ).append ( v3 ); // invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
final String v3 = "\' accessible; either change its visibility or write a custom TypeAdapter for its declaring type"; // const-string v3, "\' accessible; either change its visibility or write a custom TypeAdapter for its declaring type"
(( java.lang.StringBuilder ) v2 ).append ( v3 ); // invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v2 ).toString ( ); // invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
/* invoke-direct {v1, v2, v0}, Lcom/google/gson/JsonIOException;-><init>(Ljava/lang/String;Ljava/lang/Throwable;)V */
/* throw v1 */
} // .end method
public static java.lang.String tryMakeAccessible ( java.lang.reflect.Constructor p0 ) {
/* .locals 3 */
/* .annotation system Ldalvik/annotation/Signature; */
/* value = { */
/* "(", */
/* "Ljava/lang/reflect/Constructor<", */
/* "*>;)", */
/* "Ljava/lang/String;" */
/* } */
} // .end annotation
/* .line 57 */
/* .local p0, "constructor":Ljava/lang/reflect/Constructor;, "Ljava/lang/reflect/Constructor<*>;" */
int v0 = 1; // const/4 v0, 0x1
try { // :try_start_0
(( java.lang.reflect.Constructor ) p0 ).setAccessible ( v0 ); // invoke-virtual {p0, v0}, Ljava/lang/reflect/Constructor;->setAccessible(Z)V
/* :try_end_0 */
/* .catch Ljava/lang/Exception; {:try_start_0 ..:try_end_0} :catch_0 */
/* .line 58 */
int v0 = 0; // const/4 v0, 0x0
/* .line 59 */
/* :catch_0 */
/* move-exception v0 */
/* .line 60 */
/* .local v0, "exception":Ljava/lang/Exception; */
/* new-instance v1, Ljava/lang/StringBuilder; */
/* invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V */
final String v2 = "Failed making constructor \'"; // const-string v2, "Failed making constructor \'"
(( java.lang.StringBuilder ) v1 ).append ( v2 ); // invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
com.google.gson.internal.reflect.ReflectionHelper .constructorToString ( p0 );
(( java.lang.StringBuilder ) v1 ).append ( v2 ); // invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
final String v2 = "\' accessible; either change its visibility or write a custom InstanceCreator or TypeAdapter for its declaring type: "; // const-string v2, "\' accessible; either change its visibility or write a custom InstanceCreator or TypeAdapter for its declaring type: "
(( java.lang.StringBuilder ) v1 ).append ( v2 ); // invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
/* .line 63 */
(( java.lang.Exception ) v0 ).getMessage ( ); // invoke-virtual {v0}, Ljava/lang/Exception;->getMessage()Ljava/lang/String;
(( java.lang.StringBuilder ) v1 ).append ( v2 ); // invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v1 ).toString ( ); // invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
/* .line 60 */
} // .end method
