public class inal {
	 /* .source "Streams.java" */
	 /* # annotations */
	 /* .annotation system Ldalvik/annotation/MemberClasses; */
	 /* value = { */
	 /* Lcom/google/gson/internal/Streams$AppendableWriter; */
	 /* } */
} // .end annotation
/* # direct methods */
private inal ( ) {
	 /* .locals 1 */
	 /* .line 36 */
	 /* invoke-direct {p0}, Ljava/lang/Object;-><init>()V */
	 /* .line 37 */
	 /* new-instance v0, Ljava/lang/UnsupportedOperationException; */
	 /* invoke-direct {v0}, Ljava/lang/UnsupportedOperationException;-><init>()V */
	 /* throw v0 */
} // .end method
public static com.google.gson.JsonElement parse ( com.google.gson.stream.JsonReader p0 ) {
	 /* .locals 3 */
	 /* .param p0, "reader" # Lcom/google/gson/stream/JsonReader; */
	 /* .annotation system Ldalvik/annotation/Throws; */
	 /* value = { */
	 /* Lcom/google/gson/JsonParseException; */
	 /* } */
} // .end annotation
/* .line 44 */
int v0 = 1; // const/4 v0, 0x1
/* .line 46 */
/* .local v0, "isEmpty":Z */
try { // :try_start_0
	 (( com.google.gson.stream.JsonReader ) p0 ).peek ( ); // invoke-virtual {p0}, Lcom/google/gson/stream/JsonReader;->peek()Lcom/google/gson/stream/JsonToken;
	 /* .line 47 */
	 int v0 = 0; // const/4 v0, 0x0
	 /* .line 48 */
	 v1 = com.google.gson.internal.bind.TypeAdapters.JSON_ELEMENT;
	 (( com.google.gson.TypeAdapter ) v1 ).read ( p0 ); // invoke-virtual {v1, p0}, Lcom/google/gson/TypeAdapter;->read(Lcom/google/gson/stream/JsonReader;)Ljava/lang/Object;
	 /* check-cast v1, Lcom/google/gson/JsonElement; */
	 /* :try_end_0 */
	 /* .catch Ljava/io/EOFException; {:try_start_0 ..:try_end_0} :catch_3 */
	 /* .catch Lcom/google/gson/stream/MalformedJsonException; {:try_start_0 ..:try_end_0} :catch_2 */
	 /* .catch Ljava/io/IOException; {:try_start_0 ..:try_end_0} :catch_1 */
	 /* .catch Ljava/lang/NumberFormatException; {:try_start_0 ..:try_end_0} :catch_0 */
	 /* .line 63 */
	 /* :catch_0 */
	 /* move-exception v1 */
	 /* .line 64 */
	 /* .local v1, "e":Ljava/lang/NumberFormatException; */
	 /* new-instance v2, Lcom/google/gson/JsonSyntaxException; */
	 /* invoke-direct {v2, v1}, Lcom/google/gson/JsonSyntaxException;-><init>(Ljava/lang/Throwable;)V */
	 /* throw v2 */
	 /* .line 61 */
} // .end local v1 # "e":Ljava/lang/NumberFormatException;
/* :catch_1 */
/* move-exception v1 */
/* .line 62 */
/* .local v1, "e":Ljava/io/IOException; */
/* new-instance v2, Lcom/google/gson/JsonIOException; */
/* invoke-direct {v2, v1}, Lcom/google/gson/JsonIOException;-><init>(Ljava/lang/Throwable;)V */
/* throw v2 */
/* .line 59 */
} // .end local v1 # "e":Ljava/io/IOException;
/* :catch_2 */
/* move-exception v1 */
/* .line 60 */
/* .local v1, "e":Lcom/google/gson/stream/MalformedJsonException; */
/* new-instance v2, Lcom/google/gson/JsonSyntaxException; */
/* invoke-direct {v2, v1}, Lcom/google/gson/JsonSyntaxException;-><init>(Ljava/lang/Throwable;)V */
/* throw v2 */
/* .line 49 */
} // .end local v1 # "e":Lcom/google/gson/stream/MalformedJsonException;
/* :catch_3 */
/* move-exception v1 */
/* .line 54 */
/* .local v1, "e":Ljava/io/EOFException; */
if ( v0 != null) { // if-eqz v0, :cond_0
/* .line 55 */
v2 = com.google.gson.JsonNull.INSTANCE;
/* .line 58 */
} // :cond_0
/* new-instance v2, Lcom/google/gson/JsonSyntaxException; */
/* invoke-direct {v2, v1}, Lcom/google/gson/JsonSyntaxException;-><init>(Ljava/lang/Throwable;)V */
/* throw v2 */
} // .end method
public static void write ( com.google.gson.JsonElement p0, com.google.gson.stream.JsonWriter p1 ) {
/* .locals 1 */
/* .param p0, "element" # Lcom/google/gson/JsonElement; */
/* .param p1, "writer" # Lcom/google/gson/stream/JsonWriter; */
/* .annotation system Ldalvik/annotation/Throws; */
/* value = { */
/* Ljava/io/IOException; */
/* } */
} // .end annotation
/* .line 72 */
v0 = com.google.gson.internal.bind.TypeAdapters.JSON_ELEMENT;
(( com.google.gson.TypeAdapter ) v0 ).write ( p1, p0 ); // invoke-virtual {v0, p1, p0}, Lcom/google/gson/TypeAdapter;->write(Lcom/google/gson/stream/JsonWriter;Ljava/lang/Object;)V
/* .line 73 */
return;
} // .end method
public static java.io.Writer writerForAppendable ( java.lang.Appendable p0 ) {
/* .locals 1 */
/* .param p0, "appendable" # Ljava/lang/Appendable; */
/* .line 76 */
/* instance-of v0, p0, Ljava/io/Writer; */
if ( v0 != null) { // if-eqz v0, :cond_0
/* move-object v0, p0 */
/* check-cast v0, Ljava/io/Writer; */
} // :cond_0
/* new-instance v0, Lcom/google/gson/internal/Streams$AppendableWriter; */
/* invoke-direct {v0, p0}, Lcom/google/gson/internal/Streams$AppendableWriter;-><init>(Ljava/lang/Appendable;)V */
} // :goto_0
} // .end method
