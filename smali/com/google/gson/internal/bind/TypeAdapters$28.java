class com.google.gson.internal.bind.TypeAdapters$28 extends com.google.gson.TypeAdapter {
	 /* .source "TypeAdapters.java" */
	 /* # annotations */
	 /* .annotation system Ldalvik/annotation/EnclosingClass; */
	 /* value = Lcom/google/gson/internal/bind/TypeAdapters; */
} // .end annotation
/* .annotation system Ldalvik/annotation/InnerClass; */
/* accessFlags = 0x0 */
/* name = null */
} // .end annotation
/* .annotation system Ldalvik/annotation/Signature; */
/* value = { */
/* "Lcom/google/gson/TypeAdapter<", */
/* "Lcom/google/gson/JsonElement;", */
/* ">;" */
/* } */
} // .end annotation
/* # direct methods */
 com.google.gson.internal.bind.TypeAdapters$28 ( ) {
/* .locals 0 */
/* .line 697 */
/* invoke-direct {p0}, Lcom/google/gson/TypeAdapter;-><init>()V */
return;
} // .end method
/* # virtual methods */
public com.google.gson.JsonElement read ( com.google.gson.stream.JsonReader p0 ) {
/* .locals 3 */
/* .param p1, "in" # Lcom/google/gson/stream/JsonReader; */
/* .annotation system Ldalvik/annotation/Throws; */
/* value = { */
/* Ljava/io/IOException; */
/* } */
} // .end annotation
/* .line 699 */
/* instance-of v0, p1, Lcom/google/gson/internal/bind/JsonTreeReader; */
if ( v0 != null) { // if-eqz v0, :cond_0
/* .line 700 */
/* move-object v0, p1 */
/* check-cast v0, Lcom/google/gson/internal/bind/JsonTreeReader; */
(( com.google.gson.internal.bind.JsonTreeReader ) v0 ).nextJsonElement ( ); // invoke-virtual {v0}, Lcom/google/gson/internal/bind/JsonTreeReader;->nextJsonElement()Lcom/google/gson/JsonElement;
/* .line 703 */
} // :cond_0
v0 = com.google.gson.internal.bind.TypeAdapters$35.$SwitchMap$com$google$gson$stream$JsonToken;
(( com.google.gson.stream.JsonReader ) p1 ).peek ( ); // invoke-virtual {p1}, Lcom/google/gson/stream/JsonReader;->peek()Lcom/google/gson/stream/JsonToken;
v1 = (( com.google.gson.stream.JsonToken ) v1 ).ordinal ( ); // invoke-virtual {v1}, Lcom/google/gson/stream/JsonToken;->ordinal()I
/* aget v0, v0, v1 */
/* packed-switch v0, :pswitch_data_0 */
/* .line 735 */
/* new-instance v0, Ljava/lang/IllegalArgumentException; */
/* invoke-direct {v0}, Ljava/lang/IllegalArgumentException;-><init>()V */
/* throw v0 */
/* .line 723 */
/* :pswitch_0 */
/* new-instance v0, Lcom/google/gson/JsonObject; */
/* invoke-direct {v0}, Lcom/google/gson/JsonObject;-><init>()V */
/* .line 724 */
/* .local v0, "object":Lcom/google/gson/JsonObject; */
(( com.google.gson.stream.JsonReader ) p1 ).beginObject ( ); // invoke-virtual {p1}, Lcom/google/gson/stream/JsonReader;->beginObject()V
/* .line 725 */
} // :goto_0
v1 = (( com.google.gson.stream.JsonReader ) p1 ).hasNext ( ); // invoke-virtual {p1}, Lcom/google/gson/stream/JsonReader;->hasNext()Z
if ( v1 != null) { // if-eqz v1, :cond_1
/* .line 726 */
(( com.google.gson.stream.JsonReader ) p1 ).nextName ( ); // invoke-virtual {p1}, Lcom/google/gson/stream/JsonReader;->nextName()Ljava/lang/String;
(( com.google.gson.internal.bind.TypeAdapters$28 ) p0 ).read ( p1 ); // invoke-virtual {p0, p1}, Lcom/google/gson/internal/bind/TypeAdapters$28;->read(Lcom/google/gson/stream/JsonReader;)Lcom/google/gson/JsonElement;
(( com.google.gson.JsonObject ) v0 ).add ( v1, v2 ); // invoke-virtual {v0, v1, v2}, Lcom/google/gson/JsonObject;->add(Ljava/lang/String;Lcom/google/gson/JsonElement;)V
/* .line 728 */
} // :cond_1
(( com.google.gson.stream.JsonReader ) p1 ).endObject ( ); // invoke-virtual {p1}, Lcom/google/gson/stream/JsonReader;->endObject()V
/* .line 729 */
/* .line 715 */
} // .end local v0 # "object":Lcom/google/gson/JsonObject;
/* :pswitch_1 */
/* new-instance v0, Lcom/google/gson/JsonArray; */
/* invoke-direct {v0}, Lcom/google/gson/JsonArray;-><init>()V */
/* .line 716 */
/* .local v0, "array":Lcom/google/gson/JsonArray; */
(( com.google.gson.stream.JsonReader ) p1 ).beginArray ( ); // invoke-virtual {p1}, Lcom/google/gson/stream/JsonReader;->beginArray()V
/* .line 717 */
} // :goto_1
v1 = (( com.google.gson.stream.JsonReader ) p1 ).hasNext ( ); // invoke-virtual {p1}, Lcom/google/gson/stream/JsonReader;->hasNext()Z
if ( v1 != null) { // if-eqz v1, :cond_2
/* .line 718 */
(( com.google.gson.internal.bind.TypeAdapters$28 ) p0 ).read ( p1 ); // invoke-virtual {p0, p1}, Lcom/google/gson/internal/bind/TypeAdapters$28;->read(Lcom/google/gson/stream/JsonReader;)Lcom/google/gson/JsonElement;
(( com.google.gson.JsonArray ) v0 ).add ( v1 ); // invoke-virtual {v0, v1}, Lcom/google/gson/JsonArray;->add(Lcom/google/gson/JsonElement;)V
/* .line 720 */
} // :cond_2
(( com.google.gson.stream.JsonReader ) p1 ).endArray ( ); // invoke-virtual {p1}, Lcom/google/gson/stream/JsonReader;->endArray()V
/* .line 721 */
/* .line 712 */
} // .end local v0 # "array":Lcom/google/gson/JsonArray;
/* :pswitch_2 */
(( com.google.gson.stream.JsonReader ) p1 ).nextNull ( ); // invoke-virtual {p1}, Lcom/google/gson/stream/JsonReader;->nextNull()V
/* .line 713 */
v0 = com.google.gson.JsonNull.INSTANCE;
/* .line 710 */
/* :pswitch_3 */
/* new-instance v0, Lcom/google/gson/JsonPrimitive; */
v1 = (( com.google.gson.stream.JsonReader ) p1 ).nextBoolean ( ); // invoke-virtual {p1}, Lcom/google/gson/stream/JsonReader;->nextBoolean()Z
java.lang.Boolean .valueOf ( v1 );
/* invoke-direct {v0, v1}, Lcom/google/gson/JsonPrimitive;-><init>(Ljava/lang/Boolean;)V */
/* .line 705 */
/* :pswitch_4 */
/* new-instance v0, Lcom/google/gson/JsonPrimitive; */
(( com.google.gson.stream.JsonReader ) p1 ).nextString ( ); // invoke-virtual {p1}, Lcom/google/gson/stream/JsonReader;->nextString()Ljava/lang/String;
/* invoke-direct {v0, v1}, Lcom/google/gson/JsonPrimitive;-><init>(Ljava/lang/String;)V */
/* .line 707 */
/* :pswitch_5 */
(( com.google.gson.stream.JsonReader ) p1 ).nextString ( ); // invoke-virtual {p1}, Lcom/google/gson/stream/JsonReader;->nextString()Ljava/lang/String;
/* .line 708 */
/* .local v0, "number":Ljava/lang/String; */
/* new-instance v1, Lcom/google/gson/JsonPrimitive; */
/* new-instance v2, Lcom/google/gson/internal/LazilyParsedNumber; */
/* invoke-direct {v2, v0}, Lcom/google/gson/internal/LazilyParsedNumber;-><init>(Ljava/lang/String;)V */
/* invoke-direct {v1, v2}, Lcom/google/gson/JsonPrimitive;-><init>(Ljava/lang/Number;)V */
/* :pswitch_data_0 */
/* .packed-switch 0x1 */
/* :pswitch_5 */
/* :pswitch_4 */
/* :pswitch_3 */
/* :pswitch_2 */
/* :pswitch_1 */
/* :pswitch_0 */
} // .end packed-switch
} // .end method
public java.lang.Object read ( com.google.gson.stream.JsonReader p0 ) { //bridge//synthethic
/* .locals 0 */
/* .annotation system Ldalvik/annotation/Throws; */
/* value = { */
/* Ljava/io/IOException; */
/* } */
} // .end annotation
/* .line 697 */
(( com.google.gson.internal.bind.TypeAdapters$28 ) p0 ).read ( p1 ); // invoke-virtual {p0, p1}, Lcom/google/gson/internal/bind/TypeAdapters$28;->read(Lcom/google/gson/stream/JsonReader;)Lcom/google/gson/JsonElement;
} // .end method
public void write ( com.google.gson.stream.JsonWriter p0, com.google.gson.JsonElement p1 ) {
/* .locals 3 */
/* .param p1, "out" # Lcom/google/gson/stream/JsonWriter; */
/* .param p2, "value" # Lcom/google/gson/JsonElement; */
/* .annotation system Ldalvik/annotation/Throws; */
/* value = { */
/* Ljava/io/IOException; */
/* } */
} // .end annotation
/* .line 740 */
if ( p2 != null) { // if-eqz p2, :cond_8
v0 = (( com.google.gson.JsonElement ) p2 ).isJsonNull ( ); // invoke-virtual {p2}, Lcom/google/gson/JsonElement;->isJsonNull()Z
if ( v0 != null) { // if-eqz v0, :cond_0
/* goto/16 :goto_3 */
/* .line 742 */
} // :cond_0
v0 = (( com.google.gson.JsonElement ) p2 ).isJsonPrimitive ( ); // invoke-virtual {p2}, Lcom/google/gson/JsonElement;->isJsonPrimitive()Z
if ( v0 != null) { // if-eqz v0, :cond_3
/* .line 743 */
(( com.google.gson.JsonElement ) p2 ).getAsJsonPrimitive ( ); // invoke-virtual {p2}, Lcom/google/gson/JsonElement;->getAsJsonPrimitive()Lcom/google/gson/JsonPrimitive;
/* .line 744 */
/* .local v0, "primitive":Lcom/google/gson/JsonPrimitive; */
v1 = (( com.google.gson.JsonPrimitive ) v0 ).isNumber ( ); // invoke-virtual {v0}, Lcom/google/gson/JsonPrimitive;->isNumber()Z
if ( v1 != null) { // if-eqz v1, :cond_1
/* .line 745 */
(( com.google.gson.JsonPrimitive ) v0 ).getAsNumber ( ); // invoke-virtual {v0}, Lcom/google/gson/JsonPrimitive;->getAsNumber()Ljava/lang/Number;
(( com.google.gson.stream.JsonWriter ) p1 ).value ( v1 ); // invoke-virtual {p1, v1}, Lcom/google/gson/stream/JsonWriter;->value(Ljava/lang/Number;)Lcom/google/gson/stream/JsonWriter;
/* .line 746 */
} // :cond_1
v1 = (( com.google.gson.JsonPrimitive ) v0 ).isBoolean ( ); // invoke-virtual {v0}, Lcom/google/gson/JsonPrimitive;->isBoolean()Z
if ( v1 != null) { // if-eqz v1, :cond_2
/* .line 747 */
v1 = (( com.google.gson.JsonPrimitive ) v0 ).getAsBoolean ( ); // invoke-virtual {v0}, Lcom/google/gson/JsonPrimitive;->getAsBoolean()Z
(( com.google.gson.stream.JsonWriter ) p1 ).value ( v1 ); // invoke-virtual {p1, v1}, Lcom/google/gson/stream/JsonWriter;->value(Z)Lcom/google/gson/stream/JsonWriter;
/* .line 749 */
} // :cond_2
(( com.google.gson.JsonPrimitive ) v0 ).getAsString ( ); // invoke-virtual {v0}, Lcom/google/gson/JsonPrimitive;->getAsString()Ljava/lang/String;
(( com.google.gson.stream.JsonWriter ) p1 ).value ( v1 ); // invoke-virtual {p1, v1}, Lcom/google/gson/stream/JsonWriter;->value(Ljava/lang/String;)Lcom/google/gson/stream/JsonWriter;
/* .line 752 */
} // .end local v0 # "primitive":Lcom/google/gson/JsonPrimitive;
} // :goto_0
/* goto/16 :goto_4 */
} // :cond_3
v0 = (( com.google.gson.JsonElement ) p2 ).isJsonArray ( ); // invoke-virtual {p2}, Lcom/google/gson/JsonElement;->isJsonArray()Z
if ( v0 != null) { // if-eqz v0, :cond_5
/* .line 753 */
(( com.google.gson.stream.JsonWriter ) p1 ).beginArray ( ); // invoke-virtual {p1}, Lcom/google/gson/stream/JsonWriter;->beginArray()Lcom/google/gson/stream/JsonWriter;
/* .line 754 */
(( com.google.gson.JsonElement ) p2 ).getAsJsonArray ( ); // invoke-virtual {p2}, Lcom/google/gson/JsonElement;->getAsJsonArray()Lcom/google/gson/JsonArray;
(( com.google.gson.JsonArray ) v0 ).iterator ( ); // invoke-virtual {v0}, Lcom/google/gson/JsonArray;->iterator()Ljava/util/Iterator;
v1 = } // :goto_1
if ( v1 != null) { // if-eqz v1, :cond_4
/* check-cast v1, Lcom/google/gson/JsonElement; */
/* .line 755 */
/* .local v1, "e":Lcom/google/gson/JsonElement; */
(( com.google.gson.internal.bind.TypeAdapters$28 ) p0 ).write ( p1, v1 ); // invoke-virtual {p0, p1, v1}, Lcom/google/gson/internal/bind/TypeAdapters$28;->write(Lcom/google/gson/stream/JsonWriter;Lcom/google/gson/JsonElement;)V
/* .line 756 */
} // .end local v1 # "e":Lcom/google/gson/JsonElement;
/* .line 757 */
} // :cond_4
(( com.google.gson.stream.JsonWriter ) p1 ).endArray ( ); // invoke-virtual {p1}, Lcom/google/gson/stream/JsonWriter;->endArray()Lcom/google/gson/stream/JsonWriter;
/* .line 759 */
} // :cond_5
v0 = (( com.google.gson.JsonElement ) p2 ).isJsonObject ( ); // invoke-virtual {p2}, Lcom/google/gson/JsonElement;->isJsonObject()Z
if ( v0 != null) { // if-eqz v0, :cond_7
/* .line 760 */
(( com.google.gson.stream.JsonWriter ) p1 ).beginObject ( ); // invoke-virtual {p1}, Lcom/google/gson/stream/JsonWriter;->beginObject()Lcom/google/gson/stream/JsonWriter;
/* .line 761 */
(( com.google.gson.JsonElement ) p2 ).getAsJsonObject ( ); // invoke-virtual {p2}, Lcom/google/gson/JsonElement;->getAsJsonObject()Lcom/google/gson/JsonObject;
(( com.google.gson.JsonObject ) v0 ).entrySet ( ); // invoke-virtual {v0}, Lcom/google/gson/JsonObject;->entrySet()Ljava/util/Set;
v1 = } // :goto_2
if ( v1 != null) { // if-eqz v1, :cond_6
/* check-cast v1, Ljava/util/Map$Entry; */
/* .line 762 */
/* .local v1, "e":Ljava/util/Map$Entry;, "Ljava/util/Map$Entry<Ljava/lang/String;Lcom/google/gson/JsonElement;>;" */
/* check-cast v2, Ljava/lang/String; */
(( com.google.gson.stream.JsonWriter ) p1 ).name ( v2 ); // invoke-virtual {p1, v2}, Lcom/google/gson/stream/JsonWriter;->name(Ljava/lang/String;)Lcom/google/gson/stream/JsonWriter;
/* .line 763 */
/* check-cast v2, Lcom/google/gson/JsonElement; */
(( com.google.gson.internal.bind.TypeAdapters$28 ) p0 ).write ( p1, v2 ); // invoke-virtual {p0, p1, v2}, Lcom/google/gson/internal/bind/TypeAdapters$28;->write(Lcom/google/gson/stream/JsonWriter;Lcom/google/gson/JsonElement;)V
/* .line 764 */
} // .end local v1 # "e":Ljava/util/Map$Entry;, "Ljava/util/Map$Entry<Ljava/lang/String;Lcom/google/gson/JsonElement;>;"
/* .line 765 */
} // :cond_6
(( com.google.gson.stream.JsonWriter ) p1 ).endObject ( ); // invoke-virtual {p1}, Lcom/google/gson/stream/JsonWriter;->endObject()Lcom/google/gson/stream/JsonWriter;
/* .line 768 */
} // :cond_7
/* new-instance v0, Ljava/lang/IllegalArgumentException; */
/* new-instance v1, Ljava/lang/StringBuilder; */
/* invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V */
final String v2 = "Couldn\'t write "; // const-string v2, "Couldn\'t write "
(( java.lang.StringBuilder ) v1 ).append ( v2 ); // invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.Object ) p2 ).getClass ( ); // invoke-virtual {p2}, Ljava/lang/Object;->getClass()Ljava/lang/Class;
(( java.lang.StringBuilder ) v1 ).append ( v2 ); // invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v1 ).toString ( ); // invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
/* invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V */
/* throw v0 */
/* .line 741 */
} // :cond_8
} // :goto_3
(( com.google.gson.stream.JsonWriter ) p1 ).nullValue ( ); // invoke-virtual {p1}, Lcom/google/gson/stream/JsonWriter;->nullValue()Lcom/google/gson/stream/JsonWriter;
/* .line 770 */
} // :goto_4
return;
} // .end method
public void write ( com.google.gson.stream.JsonWriter p0, java.lang.Object p1 ) { //bridge//synthethic
/* .locals 0 */
/* .annotation system Ldalvik/annotation/Throws; */
/* value = { */
/* Ljava/io/IOException; */
/* } */
} // .end annotation
/* .line 697 */
/* check-cast p2, Lcom/google/gson/JsonElement; */
(( com.google.gson.internal.bind.TypeAdapters$28 ) p0 ).write ( p1, p2 ); // invoke-virtual {p0, p1, p2}, Lcom/google/gson/internal/bind/TypeAdapters$28;->write(Lcom/google/gson/stream/JsonWriter;Lcom/google/gson/JsonElement;)V
return;
} // .end method
