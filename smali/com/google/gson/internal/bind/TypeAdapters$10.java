class com.google.gson.internal.bind.TypeAdapters$10 extends com.google.gson.TypeAdapter {
	 /* .source "TypeAdapters.java" */
	 /* # annotations */
	 /* .annotation system Ldalvik/annotation/EnclosingClass; */
	 /* value = Lcom/google/gson/internal/bind/TypeAdapters; */
} // .end annotation
/* .annotation system Ldalvik/annotation/InnerClass; */
/* accessFlags = 0x0 */
/* name = null */
} // .end annotation
/* .annotation system Ldalvik/annotation/Signature; */
/* value = { */
/* "Lcom/google/gson/TypeAdapter<", */
/* "Ljava/util/concurrent/atomic/AtomicIntegerArray;", */
/* ">;" */
/* } */
} // .end annotation
/* # direct methods */
 com.google.gson.internal.bind.TypeAdapters$10 ( ) {
/* .locals 0 */
/* .line 279 */
/* invoke-direct {p0}, Lcom/google/gson/TypeAdapter;-><init>()V */
return;
} // .end method
/* # virtual methods */
public java.lang.Object read ( com.google.gson.stream.JsonReader p0 ) { //bridge//synthethic
/* .locals 0 */
/* .annotation system Ldalvik/annotation/Throws; */
/* value = { */
/* Ljava/io/IOException; */
/* } */
} // .end annotation
/* .line 279 */
(( com.google.gson.internal.bind.TypeAdapters$10 ) p0 ).read ( p1 ); // invoke-virtual {p0, p1}, Lcom/google/gson/internal/bind/TypeAdapters$10;->read(Lcom/google/gson/stream/JsonReader;)Ljava/util/concurrent/atomic/AtomicIntegerArray;
} // .end method
public java.util.concurrent.atomic.AtomicIntegerArray read ( com.google.gson.stream.JsonReader p0 ) {
/* .locals 5 */
/* .param p1, "in" # Lcom/google/gson/stream/JsonReader; */
/* .annotation system Ldalvik/annotation/Throws; */
/* value = { */
/* Ljava/io/IOException; */
/* } */
} // .end annotation
/* .line 281 */
/* new-instance v0, Ljava/util/ArrayList; */
/* invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V */
/* .line 282 */
/* .local v0, "list":Ljava/util/List;, "Ljava/util/List<Ljava/lang/Integer;>;" */
(( com.google.gson.stream.JsonReader ) p1 ).beginArray ( ); // invoke-virtual {p1}, Lcom/google/gson/stream/JsonReader;->beginArray()V
/* .line 283 */
} // :goto_0
v1 = (( com.google.gson.stream.JsonReader ) p1 ).hasNext ( ); // invoke-virtual {p1}, Lcom/google/gson/stream/JsonReader;->hasNext()Z
if ( v1 != null) { // if-eqz v1, :cond_0
/* .line 285 */
try { // :try_start_0
v1 = (( com.google.gson.stream.JsonReader ) p1 ).nextInt ( ); // invoke-virtual {p1}, Lcom/google/gson/stream/JsonReader;->nextInt()I
/* .line 286 */
/* .local v1, "integer":I */
java.lang.Integer .valueOf ( v1 );
/* :try_end_0 */
/* .catch Ljava/lang/NumberFormatException; {:try_start_0 ..:try_end_0} :catch_0 */
/* .line 289 */
/* nop */
} // .end local v1 # "integer":I
/* .line 287 */
/* :catch_0 */
/* move-exception v1 */
/* .line 288 */
/* .local v1, "e":Ljava/lang/NumberFormatException; */
/* new-instance v2, Lcom/google/gson/JsonSyntaxException; */
/* invoke-direct {v2, v1}, Lcom/google/gson/JsonSyntaxException;-><init>(Ljava/lang/Throwable;)V */
/* throw v2 */
/* .line 291 */
} // .end local v1 # "e":Ljava/lang/NumberFormatException;
} // :cond_0
(( com.google.gson.stream.JsonReader ) p1 ).endArray ( ); // invoke-virtual {p1}, Lcom/google/gson/stream/JsonReader;->endArray()V
v1 = /* .line 292 */
/* .line 293 */
/* .local v1, "length":I */
/* new-instance v2, Ljava/util/concurrent/atomic/AtomicIntegerArray; */
/* invoke-direct {v2, v1}, Ljava/util/concurrent/atomic/AtomicIntegerArray;-><init>(I)V */
/* .line 294 */
/* .local v2, "array":Ljava/util/concurrent/atomic/AtomicIntegerArray; */
int v3 = 0; // const/4 v3, 0x0
/* .local v3, "i":I */
} // :goto_1
/* if-ge v3, v1, :cond_1 */
/* .line 295 */
/* check-cast v4, Ljava/lang/Integer; */
v4 = (( java.lang.Integer ) v4 ).intValue ( ); // invoke-virtual {v4}, Ljava/lang/Integer;->intValue()I
(( java.util.concurrent.atomic.AtomicIntegerArray ) v2 ).set ( v3, v4 ); // invoke-virtual {v2, v3, v4}, Ljava/util/concurrent/atomic/AtomicIntegerArray;->set(II)V
/* .line 294 */
/* add-int/lit8 v3, v3, 0x1 */
/* .line 297 */
} // .end local v3 # "i":I
} // :cond_1
} // .end method
public void write ( com.google.gson.stream.JsonWriter p0, java.lang.Object p1 ) { //bridge//synthethic
/* .locals 0 */
/* .annotation system Ldalvik/annotation/Throws; */
/* value = { */
/* Ljava/io/IOException; */
/* } */
} // .end annotation
/* .line 279 */
/* check-cast p2, Ljava/util/concurrent/atomic/AtomicIntegerArray; */
(( com.google.gson.internal.bind.TypeAdapters$10 ) p0 ).write ( p1, p2 ); // invoke-virtual {p0, p1, p2}, Lcom/google/gson/internal/bind/TypeAdapters$10;->write(Lcom/google/gson/stream/JsonWriter;Ljava/util/concurrent/atomic/AtomicIntegerArray;)V
return;
} // .end method
public void write ( com.google.gson.stream.JsonWriter p0, java.util.concurrent.atomic.AtomicIntegerArray p1 ) {
/* .locals 4 */
/* .param p1, "out" # Lcom/google/gson/stream/JsonWriter; */
/* .param p2, "value" # Ljava/util/concurrent/atomic/AtomicIntegerArray; */
/* .annotation system Ldalvik/annotation/Throws; */
/* value = { */
/* Ljava/io/IOException; */
/* } */
} // .end annotation
/* .line 300 */
(( com.google.gson.stream.JsonWriter ) p1 ).beginArray ( ); // invoke-virtual {p1}, Lcom/google/gson/stream/JsonWriter;->beginArray()Lcom/google/gson/stream/JsonWriter;
/* .line 301 */
int v0 = 0; // const/4 v0, 0x0
/* .local v0, "i":I */
v1 = (( java.util.concurrent.atomic.AtomicIntegerArray ) p2 ).length ( ); // invoke-virtual {p2}, Ljava/util/concurrent/atomic/AtomicIntegerArray;->length()I
/* .local v1, "length":I */
} // :goto_0
/* if-ge v0, v1, :cond_0 */
/* .line 302 */
v2 = (( java.util.concurrent.atomic.AtomicIntegerArray ) p2 ).get ( v0 ); // invoke-virtual {p2, v0}, Ljava/util/concurrent/atomic/AtomicIntegerArray;->get(I)I
/* int-to-long v2, v2 */
(( com.google.gson.stream.JsonWriter ) p1 ).value ( v2, v3 ); // invoke-virtual {p1, v2, v3}, Lcom/google/gson/stream/JsonWriter;->value(J)Lcom/google/gson/stream/JsonWriter;
/* .line 301 */
/* add-int/lit8 v0, v0, 0x1 */
/* .line 304 */
} // .end local v0 # "i":I
} // .end local v1 # "length":I
} // :cond_0
(( com.google.gson.stream.JsonWriter ) p1 ).endArray ( ); // invoke-virtual {p1}, Lcom/google/gson/stream/JsonWriter;->endArray()Lcom/google/gson/stream/JsonWriter;
/* .line 305 */
return;
} // .end method
