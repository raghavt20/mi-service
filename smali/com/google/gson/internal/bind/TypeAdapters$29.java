class com.google.gson.internal.bind.TypeAdapters$29 implements com.google.gson.TypeAdapterFactory {
	 /* .source "TypeAdapters.java" */
	 /* # interfaces */
	 /* # annotations */
	 /* .annotation system Ldalvik/annotation/EnclosingClass; */
	 /* value = Lcom/google/gson/internal/bind/TypeAdapters; */
} // .end annotation
/* .annotation system Ldalvik/annotation/InnerClass; */
/* accessFlags = 0x0 */
/* name = null */
} // .end annotation
/* # direct methods */
 com.google.gson.internal.bind.TypeAdapters$29 ( ) {
/* .locals 0 */
/* .line 831 */
/* invoke-direct {p0}, Ljava/lang/Object;-><init>()V */
return;
} // .end method
/* # virtual methods */
public com.google.gson.TypeAdapter create ( com.google.gson.Gson p0, com.google.gson.reflect.TypeToken p1 ) {
/* .locals 2 */
/* .param p1, "gson" # Lcom/google/gson/Gson; */
/* .annotation system Ldalvik/annotation/Signature; */
/* value = { */
/* "<T:", */
/* "Ljava/lang/Object;", */
/* ">(", */
/* "Lcom/google/gson/Gson;", */
/* "Lcom/google/gson/reflect/TypeToken<", */
/* "TT;>;)", */
/* "Lcom/google/gson/TypeAdapter<", */
/* "TT;>;" */
/* } */
} // .end annotation
/* .line 834 */
/* .local p2, "typeToken":Lcom/google/gson/reflect/TypeToken;, "Lcom/google/gson/reflect/TypeToken<TT;>;" */
(( com.google.gson.reflect.TypeToken ) p2 ).getRawType ( ); // invoke-virtual {p2}, Lcom/google/gson/reflect/TypeToken;->getRawType()Ljava/lang/Class;
/* .line 835 */
/* .local v0, "rawType":Ljava/lang/Class;, "Ljava/lang/Class<-TT;>;" */
/* const-class v1, Ljava/lang/Enum; */
v1 = (( java.lang.Class ) v1 ).isAssignableFrom ( v0 ); // invoke-virtual {v1, v0}, Ljava/lang/Class;->isAssignableFrom(Ljava/lang/Class;)Z
if ( v1 != null) { // if-eqz v1, :cond_2
/* const-class v1, Ljava/lang/Enum; */
/* if-ne v0, v1, :cond_0 */
/* .line 838 */
} // :cond_0
v1 = (( java.lang.Class ) v0 ).isEnum ( ); // invoke-virtual {v0}, Ljava/lang/Class;->isEnum()Z
/* if-nez v1, :cond_1 */
/* .line 839 */
(( java.lang.Class ) v0 ).getSuperclass ( ); // invoke-virtual {v0}, Ljava/lang/Class;->getSuperclass()Ljava/lang/Class;
/* .line 841 */
} // :cond_1
/* new-instance v1, Lcom/google/gson/internal/bind/TypeAdapters$EnumTypeAdapter; */
/* invoke-direct {v1, v0}, Lcom/google/gson/internal/bind/TypeAdapters$EnumTypeAdapter;-><init>(Ljava/lang/Class;)V */
/* .line 836 */
} // :cond_2
} // :goto_0
int v1 = 0; // const/4 v1, 0x0
} // .end method
