abstract class com.google.gson.internal.bind.ReflectiveTypeAdapterFactory$BoundField {
	 /* .source "ReflectiveTypeAdapterFactory.java" */
	 /* # annotations */
	 /* .annotation system Ldalvik/annotation/EnclosingClass; */
	 /* value = Lcom/google/gson/internal/bind/ReflectiveTypeAdapterFactory; */
} // .end annotation
/* .annotation system Ldalvik/annotation/InnerClass; */
/* accessFlags = 0x408 */
/* name = "BoundField" */
} // .end annotation
/* # instance fields */
final Boolean deserialized;
final java.lang.String name;
final Boolean serialized;
/* # direct methods */
protected com.google.gson.internal.bind.ReflectiveTypeAdapterFactory$BoundField ( ) {
/* .locals 0 */
/* .param p1, "name" # Ljava/lang/String; */
/* .param p2, "serialized" # Z */
/* .param p3, "deserialized" # Z */
/* .line 186 */
/* invoke-direct {p0}, Ljava/lang/Object;-><init>()V */
/* .line 187 */
this.name = p1;
/* .line 188 */
/* iput-boolean p2, p0, Lcom/google/gson/internal/bind/ReflectiveTypeAdapterFactory$BoundField;->serialized:Z */
/* .line 189 */
/* iput-boolean p3, p0, Lcom/google/gson/internal/bind/ReflectiveTypeAdapterFactory$BoundField;->deserialized:Z */
/* .line 190 */
return;
} // .end method
/* # virtual methods */
abstract void read ( com.google.gson.stream.JsonReader p0, java.lang.Object p1 ) {
/* .annotation system Ldalvik/annotation/Throws; */
/* value = { */
/* Ljava/io/IOException;, */
/* Ljava/lang/IllegalAccessException; */
/* } */
} // .end annotation
} // .end method
abstract void write ( com.google.gson.stream.JsonWriter p0, java.lang.Object p1 ) {
/* .annotation system Ldalvik/annotation/Throws; */
/* value = { */
/* Ljava/io/IOException;, */
/* Ljava/lang/IllegalAccessException; */
/* } */
} // .end annotation
} // .end method
abstract Boolean writeField ( java.lang.Object p0 ) {
/* .annotation system Ldalvik/annotation/Throws; */
/* value = { */
/* Ljava/io/IOException;, */
/* Ljava/lang/IllegalAccessException; */
/* } */
} // .end annotation
} // .end method
