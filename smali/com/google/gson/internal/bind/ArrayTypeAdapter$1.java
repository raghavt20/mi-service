class com.google.gson.internal.bind.ArrayTypeAdapter$1 implements com.google.gson.TypeAdapterFactory {
	 /* .source "ArrayTypeAdapter.java" */
	 /* # interfaces */
	 /* # annotations */
	 /* .annotation system Ldalvik/annotation/EnclosingClass; */
	 /* value = Lcom/google/gson/internal/bind/ArrayTypeAdapter; */
} // .end annotation
/* .annotation system Ldalvik/annotation/InnerClass; */
/* accessFlags = 0x0 */
/* name = null */
} // .end annotation
/* # direct methods */
 com.google.gson.internal.bind.ArrayTypeAdapter$1 ( ) {
/* .locals 0 */
/* .line 39 */
/* invoke-direct {p0}, Ljava/lang/Object;-><init>()V */
return;
} // .end method
/* # virtual methods */
public com.google.gson.TypeAdapter create ( com.google.gson.Gson p0, com.google.gson.reflect.TypeToken p1 ) {
/* .locals 5 */
/* .param p1, "gson" # Lcom/google/gson/Gson; */
/* .annotation system Ldalvik/annotation/Signature; */
/* value = { */
/* "<T:", */
/* "Ljava/lang/Object;", */
/* ">(", */
/* "Lcom/google/gson/Gson;", */
/* "Lcom/google/gson/reflect/TypeToken<", */
/* "TT;>;)", */
/* "Lcom/google/gson/TypeAdapter<", */
/* "TT;>;" */
/* } */
} // .end annotation
/* .line 42 */
/* .local p2, "typeToken":Lcom/google/gson/reflect/TypeToken;, "Lcom/google/gson/reflect/TypeToken<TT;>;" */
(( com.google.gson.reflect.TypeToken ) p2 ).getType ( ); // invoke-virtual {p2}, Lcom/google/gson/reflect/TypeToken;->getType()Ljava/lang/reflect/Type;
/* .line 43 */
/* .local v0, "type":Ljava/lang/reflect/Type; */
/* instance-of v1, v0, Ljava/lang/reflect/GenericArrayType; */
/* if-nez v1, :cond_1 */
/* instance-of v1, v0, Ljava/lang/Class; */
if ( v1 != null) { // if-eqz v1, :cond_0
/* move-object v1, v0 */
/* check-cast v1, Ljava/lang/Class; */
v1 = (( java.lang.Class ) v1 ).isArray ( ); // invoke-virtual {v1}, Ljava/lang/Class;->isArray()Z
/* if-nez v1, :cond_1 */
/* .line 44 */
} // :cond_0
int v1 = 0; // const/4 v1, 0x0
/* .line 47 */
} // :cond_1
com.google.gson.internal.$Gson$Types .getArrayComponentType ( v0 );
/* .line 48 */
/* .local v1, "componentType":Ljava/lang/reflect/Type; */
com.google.gson.reflect.TypeToken .get ( v1 );
(( com.google.gson.Gson ) p1 ).getAdapter ( v2 ); // invoke-virtual {p1, v2}, Lcom/google/gson/Gson;->getAdapter(Lcom/google/gson/reflect/TypeToken;)Lcom/google/gson/TypeAdapter;
/* .line 49 */
/* .local v2, "componentTypeAdapter":Lcom/google/gson/TypeAdapter;, "Lcom/google/gson/TypeAdapter<*>;" */
/* new-instance v3, Lcom/google/gson/internal/bind/ArrayTypeAdapter; */
/* .line 50 */
com.google.gson.internal.$Gson$Types .getRawType ( v1 );
/* invoke-direct {v3, p1, v2, v4}, Lcom/google/gson/internal/bind/ArrayTypeAdapter;-><init>(Lcom/google/gson/Gson;Lcom/google/gson/TypeAdapter;Ljava/lang/Class;)V */
/* .line 49 */
} // .end method
