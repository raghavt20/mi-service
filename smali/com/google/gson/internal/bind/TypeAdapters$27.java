class com.google.gson.internal.bind.TypeAdapters$27 extends com.google.gson.TypeAdapter {
	 /* .source "TypeAdapters.java" */
	 /* # annotations */
	 /* .annotation system Ldalvik/annotation/EnclosingClass; */
	 /* value = Lcom/google/gson/internal/bind/TypeAdapters; */
} // .end annotation
/* .annotation system Ldalvik/annotation/InnerClass; */
/* accessFlags = 0x0 */
/* name = null */
} // .end annotation
/* .annotation system Ldalvik/annotation/Signature; */
/* value = { */
/* "Lcom/google/gson/TypeAdapter<", */
/* "Ljava/util/Locale;", */
/* ">;" */
/* } */
} // .end annotation
/* # direct methods */
 com.google.gson.internal.bind.TypeAdapters$27 ( ) {
/* .locals 0 */
/* .line 660 */
/* invoke-direct {p0}, Lcom/google/gson/TypeAdapter;-><init>()V */
return;
} // .end method
/* # virtual methods */
public java.lang.Object read ( com.google.gson.stream.JsonReader p0 ) { //bridge//synthethic
/* .locals 0 */
/* .annotation system Ldalvik/annotation/Throws; */
/* value = { */
/* Ljava/io/IOException; */
/* } */
} // .end annotation
/* .line 660 */
(( com.google.gson.internal.bind.TypeAdapters$27 ) p0 ).read ( p1 ); // invoke-virtual {p0, p1}, Lcom/google/gson/internal/bind/TypeAdapters$27;->read(Lcom/google/gson/stream/JsonReader;)Ljava/util/Locale;
} // .end method
public java.util.Locale read ( com.google.gson.stream.JsonReader p0 ) {
/* .locals 6 */
/* .param p1, "in" # Lcom/google/gson/stream/JsonReader; */
/* .annotation system Ldalvik/annotation/Throws; */
/* value = { */
/* Ljava/io/IOException; */
/* } */
} // .end annotation
/* .line 663 */
(( com.google.gson.stream.JsonReader ) p1 ).peek ( ); // invoke-virtual {p1}, Lcom/google/gson/stream/JsonReader;->peek()Lcom/google/gson/stream/JsonToken;
v1 = com.google.gson.stream.JsonToken.NULL;
/* if-ne v0, v1, :cond_0 */
/* .line 664 */
(( com.google.gson.stream.JsonReader ) p1 ).nextNull ( ); // invoke-virtual {p1}, Lcom/google/gson/stream/JsonReader;->nextNull()V
/* .line 665 */
int v0 = 0; // const/4 v0, 0x0
/* .line 667 */
} // :cond_0
(( com.google.gson.stream.JsonReader ) p1 ).nextString ( ); // invoke-virtual {p1}, Lcom/google/gson/stream/JsonReader;->nextString()Ljava/lang/String;
/* .line 668 */
/* .local v0, "locale":Ljava/lang/String; */
/* new-instance v1, Ljava/util/StringTokenizer; */
final String v2 = "_"; // const-string v2, "_"
/* invoke-direct {v1, v0, v2}, Ljava/util/StringTokenizer;-><init>(Ljava/lang/String;Ljava/lang/String;)V */
/* .line 669 */
/* .local v1, "tokenizer":Ljava/util/StringTokenizer; */
int v2 = 0; // const/4 v2, 0x0
/* .line 670 */
/* .local v2, "language":Ljava/lang/String; */
int v3 = 0; // const/4 v3, 0x0
/* .line 671 */
/* .local v3, "country":Ljava/lang/String; */
int v4 = 0; // const/4 v4, 0x0
/* .line 672 */
/* .local v4, "variant":Ljava/lang/String; */
v5 = (( java.util.StringTokenizer ) v1 ).hasMoreElements ( ); // invoke-virtual {v1}, Ljava/util/StringTokenizer;->hasMoreElements()Z
if ( v5 != null) { // if-eqz v5, :cond_1
/* .line 673 */
(( java.util.StringTokenizer ) v1 ).nextToken ( ); // invoke-virtual {v1}, Ljava/util/StringTokenizer;->nextToken()Ljava/lang/String;
/* .line 675 */
} // :cond_1
v5 = (( java.util.StringTokenizer ) v1 ).hasMoreElements ( ); // invoke-virtual {v1}, Ljava/util/StringTokenizer;->hasMoreElements()Z
if ( v5 != null) { // if-eqz v5, :cond_2
/* .line 676 */
(( java.util.StringTokenizer ) v1 ).nextToken ( ); // invoke-virtual {v1}, Ljava/util/StringTokenizer;->nextToken()Ljava/lang/String;
/* .line 678 */
} // :cond_2
v5 = (( java.util.StringTokenizer ) v1 ).hasMoreElements ( ); // invoke-virtual {v1}, Ljava/util/StringTokenizer;->hasMoreElements()Z
if ( v5 != null) { // if-eqz v5, :cond_3
/* .line 679 */
(( java.util.StringTokenizer ) v1 ).nextToken ( ); // invoke-virtual {v1}, Ljava/util/StringTokenizer;->nextToken()Ljava/lang/String;
/* .line 681 */
} // :cond_3
/* if-nez v3, :cond_4 */
/* if-nez v4, :cond_4 */
/* .line 682 */
/* new-instance v5, Ljava/util/Locale; */
/* invoke-direct {v5, v2}, Ljava/util/Locale;-><init>(Ljava/lang/String;)V */
/* .line 683 */
} // :cond_4
/* if-nez v4, :cond_5 */
/* .line 684 */
/* new-instance v5, Ljava/util/Locale; */
/* invoke-direct {v5, v2, v3}, Ljava/util/Locale;-><init>(Ljava/lang/String;Ljava/lang/String;)V */
/* .line 686 */
} // :cond_5
/* new-instance v5, Ljava/util/Locale; */
/* invoke-direct {v5, v2, v3, v4}, Ljava/util/Locale;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V */
} // .end method
public void write ( com.google.gson.stream.JsonWriter p0, java.lang.Object p1 ) { //bridge//synthethic
/* .locals 0 */
/* .annotation system Ldalvik/annotation/Throws; */
/* value = { */
/* Ljava/io/IOException; */
/* } */
} // .end annotation
/* .line 660 */
/* check-cast p2, Ljava/util/Locale; */
(( com.google.gson.internal.bind.TypeAdapters$27 ) p0 ).write ( p1, p2 ); // invoke-virtual {p0, p1, p2}, Lcom/google/gson/internal/bind/TypeAdapters$27;->write(Lcom/google/gson/stream/JsonWriter;Ljava/util/Locale;)V
return;
} // .end method
public void write ( com.google.gson.stream.JsonWriter p0, java.util.Locale p1 ) {
/* .locals 1 */
/* .param p1, "out" # Lcom/google/gson/stream/JsonWriter; */
/* .param p2, "value" # Ljava/util/Locale; */
/* .annotation system Ldalvik/annotation/Throws; */
/* value = { */
/* Ljava/io/IOException; */
/* } */
} // .end annotation
/* .line 691 */
/* if-nez p2, :cond_0 */
int v0 = 0; // const/4 v0, 0x0
} // :cond_0
(( java.util.Locale ) p2 ).toString ( ); // invoke-virtual {p2}, Ljava/util/Locale;->toString()Ljava/lang/String;
} // :goto_0
(( com.google.gson.stream.JsonWriter ) p1 ).value ( v0 ); // invoke-virtual {p1, v0}, Lcom/google/gson/stream/JsonWriter;->value(Ljava/lang/String;)Lcom/google/gson/stream/JsonWriter;
/* .line 692 */
return;
} // .end method
