public class com.google.gson.internal.bind.util.ISO8601Utils {
	 /* .source "ISO8601Utils.java" */
	 /* # static fields */
	 private static final java.util.TimeZone TIMEZONE_UTC;
	 private static final java.lang.String UTC_ID;
	 /* # direct methods */
	 static com.google.gson.internal.bind.util.ISO8601Utils ( ) {
		 /* .locals 1 */
		 /* .line 30 */
		 final String v0 = "UTC"; // const-string v0, "UTC"
		 java.util.TimeZone .getTimeZone ( v0 );
		 return;
	 } // .end method
	 public com.google.gson.internal.bind.util.ISO8601Utils ( ) {
		 /* .locals 0 */
		 /* .line 17 */
		 /* invoke-direct {p0}, Ljava/lang/Object;-><init>()V */
		 return;
	 } // .end method
	 private static Boolean checkOffset ( java.lang.String p0, Integer p1, Object p2 ) {
		 /* .locals 1 */
		 /* .param p0, "value" # Ljava/lang/String; */
		 /* .param p1, "offset" # I */
		 /* .param p2, "expected" # C */
		 /* .line 288 */
		 v0 = 		 (( java.lang.String ) p0 ).length ( ); // invoke-virtual {p0}, Ljava/lang/String;->length()I
		 /* if-ge p1, v0, :cond_0 */
		 v0 = 		 (( java.lang.String ) p0 ).charAt ( p1 ); // invoke-virtual {p0, p1}, Ljava/lang/String;->charAt(I)C
		 /* if-ne v0, p2, :cond_0 */
		 int v0 = 1; // const/4 v0, 0x1
	 } // :cond_0
	 int v0 = 0; // const/4 v0, 0x0
} // :goto_0
} // .end method
public static java.lang.String format ( java.util.Date p0 ) {
/* .locals 2 */
/* .param p0, "date" # Ljava/util/Date; */
/* .line 45 */
int v0 = 0; // const/4 v0, 0x0
v1 = com.google.gson.internal.bind.util.ISO8601Utils.TIMEZONE_UTC;
com.google.gson.internal.bind.util.ISO8601Utils .format ( p0,v0,v1 );
} // .end method
public static java.lang.String format ( java.util.Date p0, Boolean p1 ) {
/* .locals 1 */
/* .param p0, "date" # Ljava/util/Date; */
/* .param p1, "millis" # Z */
/* .line 56 */
v0 = com.google.gson.internal.bind.util.ISO8601Utils.TIMEZONE_UTC;
com.google.gson.internal.bind.util.ISO8601Utils .format ( p0,p1,v0 );
} // .end method
public static java.lang.String format ( java.util.Date p0, Boolean p1, java.util.TimeZone p2 ) {
/* .locals 10 */
/* .param p0, "date" # Ljava/util/Date; */
/* .param p1, "millis" # Z */
/* .param p2, "tz" # Ljava/util/TimeZone; */
/* .line 68 */
/* new-instance v0, Ljava/util/GregorianCalendar; */
v1 = java.util.Locale.US;
/* invoke-direct {v0, p2, v1}, Ljava/util/GregorianCalendar;-><init>(Ljava/util/TimeZone;Ljava/util/Locale;)V */
/* .line 69 */
/* .local v0, "calendar":Ljava/util/Calendar; */
(( java.util.Calendar ) v0 ).setTime ( p0 ); // invoke-virtual {v0, p0}, Ljava/util/Calendar;->setTime(Ljava/util/Date;)V
/* .line 72 */
/* const-string/jumbo v1, "yyyy-MM-ddThh:mm:ss" */
v1 = (( java.lang.String ) v1 ).length ( ); // invoke-virtual {v1}, Ljava/lang/String;->length()I
/* .line 73 */
/* .local v1, "capacity":I */
if ( p1 != null) { // if-eqz p1, :cond_0
	 final String v2 = ".sss"; // const-string v2, ".sss"
	 v2 = 	 (( java.lang.String ) v2 ).length ( ); // invoke-virtual {v2}, Ljava/lang/String;->length()I
} // :cond_0
int v2 = 0; // const/4 v2, 0x0
} // :goto_0
/* add-int/2addr v1, v2 */
/* .line 74 */
v2 = (( java.util.TimeZone ) p2 ).getRawOffset ( ); // invoke-virtual {p2}, Ljava/util/TimeZone;->getRawOffset()I
/* if-nez v2, :cond_1 */
final String v2 = "Z"; // const-string v2, "Z"
} // :cond_1
final String v2 = "+hh:mm"; // const-string v2, "+hh:mm"
} // :goto_1
v2 = (( java.lang.String ) v2 ).length ( ); // invoke-virtual {v2}, Ljava/lang/String;->length()I
/* add-int/2addr v1, v2 */
/* .line 75 */
/* new-instance v2, Ljava/lang/StringBuilder; */
/* invoke-direct {v2, v1}, Ljava/lang/StringBuilder;-><init>(I)V */
/* .line 77 */
/* .local v2, "formatted":Ljava/lang/StringBuilder; */
int v3 = 1; // const/4 v3, 0x1
v4 = (( java.util.Calendar ) v0 ).get ( v3 ); // invoke-virtual {v0, v3}, Ljava/util/Calendar;->get(I)I
/* const-string/jumbo v5, "yyyy" */
v5 = (( java.lang.String ) v5 ).length ( ); // invoke-virtual {v5}, Ljava/lang/String;->length()I
com.google.gson.internal.bind.util.ISO8601Utils .padInt ( v2,v4,v5 );
/* .line 78 */
/* const/16 v4, 0x2d */
(( java.lang.StringBuilder ) v2 ).append ( v4 ); // invoke-virtual {v2, v4}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;
/* .line 79 */
int v5 = 2; // const/4 v5, 0x2
v5 = (( java.util.Calendar ) v0 ).get ( v5 ); // invoke-virtual {v0, v5}, Ljava/util/Calendar;->get(I)I
/* add-int/2addr v5, v3 */
final String v3 = "MM"; // const-string v3, "MM"
v3 = (( java.lang.String ) v3 ).length ( ); // invoke-virtual {v3}, Ljava/lang/String;->length()I
com.google.gson.internal.bind.util.ISO8601Utils .padInt ( v2,v5,v3 );
/* .line 80 */
(( java.lang.StringBuilder ) v2 ).append ( v4 ); // invoke-virtual {v2, v4}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;
/* .line 81 */
int v3 = 5; // const/4 v3, 0x5
v3 = (( java.util.Calendar ) v0 ).get ( v3 ); // invoke-virtual {v0, v3}, Ljava/util/Calendar;->get(I)I
final String v5 = "dd"; // const-string v5, "dd"
v5 = (( java.lang.String ) v5 ).length ( ); // invoke-virtual {v5}, Ljava/lang/String;->length()I
com.google.gson.internal.bind.util.ISO8601Utils .padInt ( v2,v3,v5 );
/* .line 82 */
/* const/16 v3, 0x54 */
(( java.lang.StringBuilder ) v2 ).append ( v3 ); // invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;
/* .line 83 */
/* const/16 v3, 0xb */
v3 = (( java.util.Calendar ) v0 ).get ( v3 ); // invoke-virtual {v0, v3}, Ljava/util/Calendar;->get(I)I
final String v5 = "hh"; // const-string v5, "hh"
v6 = (( java.lang.String ) v5 ).length ( ); // invoke-virtual {v5}, Ljava/lang/String;->length()I
com.google.gson.internal.bind.util.ISO8601Utils .padInt ( v2,v3,v6 );
/* .line 84 */
/* const/16 v3, 0x3a */
(( java.lang.StringBuilder ) v2 ).append ( v3 ); // invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;
/* .line 85 */
/* const/16 v6, 0xc */
v6 = (( java.util.Calendar ) v0 ).get ( v6 ); // invoke-virtual {v0, v6}, Ljava/util/Calendar;->get(I)I
final String v7 = "mm"; // const-string v7, "mm"
v8 = (( java.lang.String ) v7 ).length ( ); // invoke-virtual {v7}, Ljava/lang/String;->length()I
com.google.gson.internal.bind.util.ISO8601Utils .padInt ( v2,v6,v8 );
/* .line 86 */
(( java.lang.StringBuilder ) v2 ).append ( v3 ); // invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;
/* .line 87 */
/* const/16 v6, 0xd */
v6 = (( java.util.Calendar ) v0 ).get ( v6 ); // invoke-virtual {v0, v6}, Ljava/util/Calendar;->get(I)I
/* const-string/jumbo v8, "ss" */
v8 = (( java.lang.String ) v8 ).length ( ); // invoke-virtual {v8}, Ljava/lang/String;->length()I
com.google.gson.internal.bind.util.ISO8601Utils .padInt ( v2,v6,v8 );
/* .line 88 */
if ( p1 != null) { // if-eqz p1, :cond_2
/* .line 89 */
/* const/16 v6, 0x2e */
(( java.lang.StringBuilder ) v2 ).append ( v6 ); // invoke-virtual {v2, v6}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;
/* .line 90 */
/* const/16 v6, 0xe */
v6 = (( java.util.Calendar ) v0 ).get ( v6 ); // invoke-virtual {v0, v6}, Ljava/util/Calendar;->get(I)I
/* const-string/jumbo v8, "sss" */
v8 = (( java.lang.String ) v8 ).length ( ); // invoke-virtual {v8}, Ljava/lang/String;->length()I
com.google.gson.internal.bind.util.ISO8601Utils .padInt ( v2,v6,v8 );
/* .line 93 */
} // :cond_2
(( java.util.Calendar ) v0 ).getTimeInMillis ( ); // invoke-virtual {v0}, Ljava/util/Calendar;->getTimeInMillis()J
/* move-result-wide v8 */
v6 = (( java.util.TimeZone ) p2 ).getOffset ( v8, v9 ); // invoke-virtual {p2, v8, v9}, Ljava/util/TimeZone;->getOffset(J)I
/* .line 94 */
/* .local v6, "offset":I */
if ( v6 != null) { // if-eqz v6, :cond_4
/* .line 95 */
/* const v8, 0xea60 */
/* div-int v9, v6, v8 */
/* div-int/lit8 v9, v9, 0x3c */
v9 = java.lang.Math .abs ( v9 );
/* .line 96 */
/* .local v9, "hours":I */
/* div-int v8, v6, v8 */
/* rem-int/lit8 v8, v8, 0x3c */
v8 = java.lang.Math .abs ( v8 );
/* .line 97 */
/* .local v8, "minutes":I */
/* if-gez v6, :cond_3 */
} // :cond_3
/* const/16 v4, 0x2b */
} // :goto_2
(( java.lang.StringBuilder ) v2 ).append ( v4 ); // invoke-virtual {v2, v4}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;
/* .line 98 */
v4 = (( java.lang.String ) v5 ).length ( ); // invoke-virtual {v5}, Ljava/lang/String;->length()I
com.google.gson.internal.bind.util.ISO8601Utils .padInt ( v2,v9,v4 );
/* .line 99 */
(( java.lang.StringBuilder ) v2 ).append ( v3 ); // invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;
/* .line 100 */
v3 = (( java.lang.String ) v7 ).length ( ); // invoke-virtual {v7}, Ljava/lang/String;->length()I
com.google.gson.internal.bind.util.ISO8601Utils .padInt ( v2,v8,v3 );
/* .line 101 */
} // .end local v8 # "minutes":I
} // .end local v9 # "hours":I
/* .line 102 */
} // :cond_4
/* const/16 v3, 0x5a */
(( java.lang.StringBuilder ) v2 ).append ( v3 ); // invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;
/* .line 105 */
} // :goto_3
(( java.lang.StringBuilder ) v2 ).toString ( ); // invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
} // .end method
private static Integer indexOfNonDigit ( java.lang.String p0, Integer p1 ) {
/* .locals 3 */
/* .param p0, "string" # Ljava/lang/String; */
/* .param p1, "offset" # I */
/* .line 345 */
/* move v0, p1 */
/* .local v0, "i":I */
} // :goto_0
v1 = (( java.lang.String ) p0 ).length ( ); // invoke-virtual {p0}, Ljava/lang/String;->length()I
/* if-ge v0, v1, :cond_2 */
/* .line 346 */
v1 = (( java.lang.String ) p0 ).charAt ( v0 ); // invoke-virtual {p0, v0}, Ljava/lang/String;->charAt(I)C
/* .line 347 */
/* .local v1, "c":C */
/* const/16 v2, 0x30 */
/* if-lt v1, v2, :cond_1 */
/* const/16 v2, 0x39 */
/* if-le v1, v2, :cond_0 */
/* .line 345 */
} // .end local v1 # "c":C
} // :cond_0
/* add-int/lit8 v0, v0, 0x1 */
/* .line 347 */
/* .restart local v1 # "c":C */
} // :cond_1
} // :goto_1
/* .line 349 */
} // .end local v0 # "i":I
} // .end local v1 # "c":C
} // :cond_2
v0 = (( java.lang.String ) p0 ).length ( ); // invoke-virtual {p0}, Ljava/lang/String;->length()I
} // .end method
private static void padInt ( java.lang.StringBuilder p0, Integer p1, Integer p2 ) {
/* .locals 3 */
/* .param p0, "buffer" # Ljava/lang/StringBuilder; */
/* .param p1, "value" # I */
/* .param p2, "length" # I */
/* .line 334 */
java.lang.Integer .toString ( p1 );
/* .line 335 */
/* .local v0, "strValue":Ljava/lang/String; */
v1 = (( java.lang.String ) v0 ).length ( ); // invoke-virtual {v0}, Ljava/lang/String;->length()I
/* sub-int v1, p2, v1 */
/* .local v1, "i":I */
} // :goto_0
/* if-lez v1, :cond_0 */
/* .line 336 */
/* const/16 v2, 0x30 */
(( java.lang.StringBuilder ) p0 ).append ( v2 ); // invoke-virtual {p0, v2}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;
/* .line 335 */
/* add-int/lit8 v1, v1, -0x1 */
/* .line 338 */
} // .end local v1 # "i":I
} // :cond_0
(( java.lang.StringBuilder ) p0 ).append ( v0 ); // invoke-virtual {p0, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
/* .line 339 */
return;
} // .end method
public static java.util.Date parse ( java.lang.String p0, java.text.ParsePosition p1 ) {
/* .locals 22 */
/* .param p0, "date" # Ljava/lang/String; */
/* .param p1, "pos" # Ljava/text/ParsePosition; */
/* .annotation system Ldalvik/annotation/Throws; */
/* value = { */
/* Ljava/text/ParseException; */
/* } */
} // .end annotation
/* .line 124 */
/* move-object/from16 v1, p0 */
/* move-object/from16 v2, p1 */
int v3 = 0; // const/4 v3, 0x0
/* .line 126 */
/* .local v3, "fail":Ljava/lang/Exception; */
try { // :try_start_0
v0 = /* invoke-virtual/range {p1 ..p1}, Ljava/text/ParsePosition;->getIndex()I */
/* .line 129 */
/* .local v0, "offset":I */
/* add-int/lit8 v4, v0, 0x4 */
} // .end local v0 # "offset":I
/* .local v4, "offset":I */
v0 = com.google.gson.internal.bind.util.ISO8601Utils .parseInt ( v1,v0,v4 );
/* .line 130 */
/* .local v0, "year":I */
/* const/16 v5, 0x2d */
v6 = com.google.gson.internal.bind.util.ISO8601Utils .checkOffset ( v1,v4,v5 );
if ( v6 != null) { // if-eqz v6, :cond_0
/* .line 131 */
/* add-int/lit8 v4, v4, 0x1 */
/* .line 135 */
} // :cond_0
/* add-int/lit8 v6, v4, 0x2 */
} // .end local v4 # "offset":I
/* .local v6, "offset":I */
v4 = com.google.gson.internal.bind.util.ISO8601Utils .parseInt ( v1,v4,v6 );
/* .line 136 */
/* .local v4, "month":I */
v7 = com.google.gson.internal.bind.util.ISO8601Utils .checkOffset ( v1,v6,v5 );
if ( v7 != null) { // if-eqz v7, :cond_1
/* .line 137 */
/* add-int/lit8 v6, v6, 0x1 */
/* .line 141 */
} // :cond_1
/* add-int/lit8 v7, v6, 0x2 */
} // .end local v6 # "offset":I
/* .local v7, "offset":I */
v6 = com.google.gson.internal.bind.util.ISO8601Utils .parseInt ( v1,v6,v7 );
/* .line 143 */
/* .local v6, "day":I */
int v8 = 0; // const/4 v8, 0x0
/* .line 144 */
/* .local v8, "hour":I */
int v9 = 0; // const/4 v9, 0x0
/* .line 145 */
/* .local v9, "minutes":I */
int v10 = 0; // const/4 v10, 0x0
/* .line 146 */
/* .local v10, "seconds":I */
int v11 = 0; // const/4 v11, 0x0
/* .line 149 */
/* .local v11, "milliseconds":I */
/* const/16 v12, 0x54 */
v12 = com.google.gson.internal.bind.util.ISO8601Utils .checkOffset ( v1,v7,v12 );
/* :try_end_0 */
/* .catch Ljava/lang/IndexOutOfBoundsException; {:try_start_0 ..:try_end_0} :catch_8 */
/* .catch Ljava/lang/NumberFormatException; {:try_start_0 ..:try_end_0} :catch_7 */
/* .catch Ljava/lang/IllegalArgumentException; {:try_start_0 ..:try_end_0} :catch_6 */
/* .line 151 */
/* .local v12, "hasT":Z */
/* if-nez v12, :cond_2 */
try { // :try_start_1
v13 = /* invoke-virtual/range {p0 ..p0}, Ljava/lang/String;->length()I */
/* if-gt v13, v7, :cond_2 */
/* .line 152 */
/* new-instance v5, Ljava/util/GregorianCalendar; */
/* add-int/lit8 v13, v4, -0x1 */
/* invoke-direct {v5, v0, v13, v6}, Ljava/util/GregorianCalendar;-><init>(III)V */
/* .line 154 */
/* .local v5, "calendar":Ljava/util/Calendar; */
(( java.text.ParsePosition ) v2 ).setIndex ( v7 ); // invoke-virtual {v2, v7}, Ljava/text/ParsePosition;->setIndex(I)V
/* .line 155 */
(( java.util.Calendar ) v5 ).getTime ( ); // invoke-virtual {v5}, Ljava/util/Calendar;->getTime()Ljava/util/Date;
/* .line 266 */
} // .end local v0 # "year":I
} // .end local v4 # "month":I
} // .end local v5 # "calendar":Ljava/util/Calendar;
} // .end local v6 # "day":I
} // .end local v7 # "offset":I
} // .end local v8 # "hour":I
} // .end local v9 # "minutes":I
} // .end local v10 # "seconds":I
} // .end local v11 # "milliseconds":I
} // .end local v12 # "hasT":Z
/* :catch_0 */
/* move-exception v0 */
/* move-object/from16 v16, v3 */
/* goto/16 :goto_9 */
/* .line 264 */
/* :catch_1 */
/* move-exception v0 */
/* move-object/from16 v16, v3 */
/* goto/16 :goto_a */
/* .line 262 */
/* :catch_2 */
/* move-exception v0 */
/* move-object/from16 v16, v3 */
/* goto/16 :goto_b */
/* .line 158 */
/* .restart local v0 # "year":I */
/* .restart local v4 # "month":I */
/* .restart local v6 # "day":I */
/* .restart local v7 # "offset":I */
/* .restart local v8 # "hour":I */
/* .restart local v9 # "minutes":I */
/* .restart local v10 # "seconds":I */
/* .restart local v11 # "milliseconds":I */
/* .restart local v12 # "hasT":Z */
} // :cond_2
/* const/16 v13, 0x2b */
/* const/16 v14, 0x5a */
if ( v12 != null) { // if-eqz v12, :cond_7
/* .line 161 */
/* add-int/lit8 v7, v7, 0x1 */
/* add-int/lit8 v15, v7, 0x2 */
} // .end local v7 # "offset":I
/* .local v15, "offset":I */
v7 = com.google.gson.internal.bind.util.ISO8601Utils .parseInt ( v1,v7,v15 );
/* move v8, v7 */
/* .line 162 */
/* const/16 v7, 0x3a */
v16 = com.google.gson.internal.bind.util.ISO8601Utils .checkOffset ( v1,v15,v7 );
if ( v16 != null) { // if-eqz v16, :cond_3
/* .line 163 */
/* add-int/lit8 v15, v15, 0x1 */
/* .line 166 */
} // :cond_3
/* add-int/lit8 v5, v15, 0x2 */
} // .end local v15 # "offset":I
/* .local v5, "offset":I */
v15 = com.google.gson.internal.bind.util.ISO8601Utils .parseInt ( v1,v15,v5 );
/* move v9, v15 */
/* .line 167 */
v7 = com.google.gson.internal.bind.util.ISO8601Utils .checkOffset ( v1,v5,v7 );
if ( v7 != null) { // if-eqz v7, :cond_4
/* .line 168 */
/* add-int/lit8 v5, v5, 0x1 */
/* move v7, v5 */
/* .line 167 */
} // :cond_4
/* move v7, v5 */
/* .line 171 */
} // .end local v5 # "offset":I
/* .restart local v7 # "offset":I */
} // :goto_0
v5 = /* invoke-virtual/range {p0 ..p0}, Ljava/lang/String;->length()I */
/* if-le v5, v7, :cond_7 */
/* .line 172 */
v5 = (( java.lang.String ) v1 ).charAt ( v7 ); // invoke-virtual {v1, v7}, Ljava/lang/String;->charAt(I)C
/* .line 173 */
/* .local v5, "c":C */
/* if-eq v5, v14, :cond_7 */
/* if-eq v5, v13, :cond_7 */
/* const/16 v15, 0x2d */
/* if-eq v5, v15, :cond_7 */
/* .line 174 */
/* add-int/lit8 v15, v7, 0x2 */
} // .end local v7 # "offset":I
/* .restart local v15 # "offset":I */
v7 = com.google.gson.internal.bind.util.ISO8601Utils .parseInt ( v1,v7,v15 );
/* .line 175 */
} // .end local v10 # "seconds":I
/* .local v7, "seconds":I */
/* const/16 v10, 0x3b */
/* if-le v7, v10, :cond_5 */
/* const/16 v10, 0x3f */
/* if-ge v7, v10, :cond_5 */
/* const/16 v7, 0x3b */
/* move v10, v7 */
/* .line 177 */
} // :cond_5
/* move v10, v7 */
} // .end local v7 # "seconds":I
/* .restart local v10 # "seconds":I */
} // :goto_1
/* const/16 v7, 0x2e */
v7 = com.google.gson.internal.bind.util.ISO8601Utils .checkOffset ( v1,v15,v7 );
if ( v7 != null) { // if-eqz v7, :cond_6
/* .line 178 */
/* add-int/lit8 v15, v15, 0x1 */
/* .line 179 */
/* add-int/lit8 v7, v15, 0x1 */
v7 = com.google.gson.internal.bind.util.ISO8601Utils .indexOfNonDigit ( v1,v7 );
/* .line 180 */
/* .local v7, "endOffset":I */
/* add-int/lit8 v13, v15, 0x3 */
v13 = java.lang.Math .min ( v7,v13 );
/* .line 181 */
/* .local v13, "parseEndOffset":I */
v17 = com.google.gson.internal.bind.util.ISO8601Utils .parseInt ( v1,v15,v13 );
/* :try_end_1 */
/* .catch Ljava/lang/IndexOutOfBoundsException; {:try_start_1 ..:try_end_1} :catch_2 */
/* .catch Ljava/lang/NumberFormatException; {:try_start_1 ..:try_end_1} :catch_1 */
/* .catch Ljava/lang/IllegalArgumentException; {:try_start_1 ..:try_end_1} :catch_0 */
/* .line 183 */
/* .local v17, "fraction":I */
/* sub-int v18, v13, v15 */
/* packed-switch v18, :pswitch_data_0 */
/* .line 191 */
/* move/from16 v11, v17 */
/* .line 185 */
/* :pswitch_0 */
/* mul-int/lit8 v11, v17, 0xa */
/* .line 186 */
/* .line 188 */
/* :pswitch_1 */
/* mul-int/lit8 v11, v17, 0x64 */
/* .line 189 */
/* nop */
/* .line 193 */
} // :goto_2
/* move v15, v7 */
/* .line 177 */
} // .end local v7 # "endOffset":I
} // .end local v13 # "parseEndOffset":I
} // .end local v17 # "fraction":I
} // :cond_6
/* move v7, v15 */
/* .line 200 */
} // .end local v5 # "c":C
} // .end local v15 # "offset":I
/* .local v7, "offset":I */
} // :cond_7
} // :goto_3
try { // :try_start_2
v5 = /* invoke-virtual/range {p0 ..p0}, Ljava/lang/String;->length()I */
/* if-le v5, v7, :cond_10 */
/* .line 204 */
int v5 = 0; // const/4 v5, 0x0
/* .line 205 */
/* .local v5, "timezone":Ljava/util/TimeZone; */
v13 = (( java.lang.String ) v1 ).charAt ( v7 ); // invoke-virtual {v1, v7}, Ljava/lang/String;->charAt(I)C
/* :try_end_2 */
/* .catch Ljava/lang/IndexOutOfBoundsException; {:try_start_2 ..:try_end_2} :catch_8 */
/* .catch Ljava/lang/NumberFormatException; {:try_start_2 ..:try_end_2} :catch_7 */
/* .catch Ljava/lang/IllegalArgumentException; {:try_start_2 ..:try_end_2} :catch_6 */
/* .line 207 */
/* .local v13, "timezoneIndicator":C */
int v15 = 1; // const/4 v15, 0x1
/* if-ne v13, v14, :cond_8 */
/* .line 208 */
try { // :try_start_3
v14 = com.google.gson.internal.bind.util.ISO8601Utils.TIMEZONE_UTC;
/* :try_end_3 */
/* .catch Ljava/lang/IndexOutOfBoundsException; {:try_start_3 ..:try_end_3} :catch_2 */
/* .catch Ljava/lang/NumberFormatException; {:try_start_3 ..:try_end_3} :catch_1 */
/* .catch Ljava/lang/IllegalArgumentException; {:try_start_3 ..:try_end_3} :catch_0 */
/* move-object v5, v14 */
/* .line 209 */
/* add-int/2addr v7, v15 */
/* move-object/from16 v16, v3 */
/* move/from16 v21, v12 */
/* goto/16 :goto_8 */
/* .line 210 */
} // :cond_8
/* const/16 v14, 0x2b */
/* if-eq v13, v14, :cond_a */
/* const/16 v14, 0x2d */
/* if-ne v13, v14, :cond_9 */
/* move-object/from16 v16, v3 */
/* .line 245 */
} // :cond_9
try { // :try_start_4
/* new-instance v14, Ljava/lang/IndexOutOfBoundsException; */
/* new-instance v15, Ljava/lang/StringBuilder; */
/* invoke-direct {v15}, Ljava/lang/StringBuilder;-><init>()V */
/* :try_end_4 */
/* .catch Ljava/lang/IndexOutOfBoundsException; {:try_start_4 ..:try_end_4} :catch_8 */
/* .catch Ljava/lang/NumberFormatException; {:try_start_4 ..:try_end_4} :catch_7 */
/* .catch Ljava/lang/IllegalArgumentException; {:try_start_4 ..:try_end_4} :catch_6 */
/* move-object/from16 v16, v3 */
} // .end local v3 # "fail":Ljava/lang/Exception;
/* .local v16, "fail":Ljava/lang/Exception; */
try { // :try_start_5
final String v3 = "Invalid time zone indicator \'"; // const-string v3, "Invalid time zone indicator \'"
(( java.lang.StringBuilder ) v15 ).append ( v3 ); // invoke-virtual {v15, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v3 ).append ( v13 ); // invoke-virtual {v3, v13}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;
final String v15 = "\'"; // const-string v15, "\'"
(( java.lang.StringBuilder ) v3 ).append ( v15 ); // invoke-virtual {v3, v15}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v3 ).toString ( ); // invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
/* invoke-direct {v14, v3}, Ljava/lang/IndexOutOfBoundsException;-><init>(Ljava/lang/String;)V */
} // .end local v16 # "fail":Ljava/lang/Exception;
} // .end local p0 # "date":Ljava/lang/String;
} // .end local p1 # "pos":Ljava/text/ParsePosition;
/* throw v14 */
/* .line 210 */
/* .restart local v3 # "fail":Ljava/lang/Exception; */
/* .restart local p0 # "date":Ljava/lang/String; */
/* .restart local p1 # "pos":Ljava/text/ParsePosition; */
} // :cond_a
/* move-object/from16 v16, v3 */
/* .line 211 */
} // .end local v3 # "fail":Ljava/lang/Exception;
/* .restart local v16 # "fail":Ljava/lang/Exception; */
} // :goto_4
(( java.lang.String ) v1 ).substring ( v7 ); // invoke-virtual {v1, v7}, Ljava/lang/String;->substring(I)Ljava/lang/String;
/* .line 214 */
/* .local v3, "timezoneOffset":Ljava/lang/String; */
v14 = (( java.lang.String ) v3 ).length ( ); // invoke-virtual {v3}, Ljava/lang/String;->length()I
int v15 = 5; // const/4 v15, 0x5
/* if-lt v14, v15, :cond_b */
/* move-object v14, v3 */
} // :cond_b
/* new-instance v14, Ljava/lang/StringBuilder; */
/* invoke-direct {v14}, Ljava/lang/StringBuilder;-><init>()V */
(( java.lang.StringBuilder ) v14 ).append ( v3 ); // invoke-virtual {v14, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
final String v15 = "00"; // const-string v15, "00"
(( java.lang.StringBuilder ) v14 ).append ( v15 ); // invoke-virtual {v14, v15}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v14 ).toString ( ); // invoke-virtual {v14}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
} // :goto_5
/* move-object v3, v14 */
/* .line 216 */
v14 = (( java.lang.String ) v3 ).length ( ); // invoke-virtual {v3}, Ljava/lang/String;->length()I
/* add-int/2addr v7, v14 */
/* .line 218 */
final String v14 = "+0000"; // const-string v14, "+0000"
v14 = (( java.lang.String ) v14 ).equals ( v3 ); // invoke-virtual {v14, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z
/* if-nez v14, :cond_f */
final String v14 = "+00:00"; // const-string v14, "+00:00"
v14 = (( java.lang.String ) v14 ).equals ( v3 ); // invoke-virtual {v14, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z
if ( v14 != null) { // if-eqz v14, :cond_c
/* move-object/from16 v18, v3 */
/* move/from16 v19, v7 */
/* move/from16 v21, v12 */
/* goto/16 :goto_6 */
/* .line 225 */
} // :cond_c
/* new-instance v14, Ljava/lang/StringBuilder; */
/* invoke-direct {v14}, Ljava/lang/StringBuilder;-><init>()V */
final String v15 = "GMT"; // const-string v15, "GMT"
(( java.lang.StringBuilder ) v14 ).append ( v15 ); // invoke-virtual {v14, v15}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v14 ).append ( v3 ); // invoke-virtual {v14, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v14 ).toString ( ); // invoke-virtual {v14}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
/* .line 228 */
/* .local v14, "timezoneId":Ljava/lang/String; */
java.util.TimeZone .getTimeZone ( v14 );
/* move-object v5, v15 */
/* .line 230 */
(( java.util.TimeZone ) v5 ).getID ( ); // invoke-virtual {v5}, Ljava/util/TimeZone;->getID()Ljava/lang/String;
/* .line 231 */
/* .local v15, "act":Ljava/lang/String; */
v18 = (( java.lang.String ) v15 ).equals ( v14 ); // invoke-virtual {v15, v14}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z
/* if-nez v18, :cond_e */
/* .line 237 */
/* move-object/from16 v18, v3 */
} // .end local v3 # "timezoneOffset":Ljava/lang/String;
/* .local v18, "timezoneOffset":Ljava/lang/String; */
final String v3 = ":"; // const-string v3, ":"
/* move/from16 v19, v7 */
} // .end local v7 # "offset":I
/* .local v19, "offset":I */
final String v7 = ""; // const-string v7, ""
(( java.lang.String ) v15 ).replace ( v3, v7 ); // invoke-virtual {v15, v3, v7}, Ljava/lang/String;->replace(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Ljava/lang/String;
/* .line 238 */
/* .local v3, "cleaned":Ljava/lang/String; */
v7 = (( java.lang.String ) v3 ).equals ( v14 ); // invoke-virtual {v3, v14}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z
if ( v7 != null) { // if-eqz v7, :cond_d
/* move/from16 v21, v12 */
/* .line 239 */
} // :cond_d
/* new-instance v7, Ljava/lang/IndexOutOfBoundsException; */
/* move-object/from16 v20, v3 */
} // .end local v3 # "cleaned":Ljava/lang/String;
/* .local v20, "cleaned":Ljava/lang/String; */
/* new-instance v3, Ljava/lang/StringBuilder; */
/* invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V */
/* move/from16 v21, v12 */
} // .end local v12 # "hasT":Z
/* .local v21, "hasT":Z */
final String v12 = "Mismatching time zone indicator: "; // const-string v12, "Mismatching time zone indicator: "
(( java.lang.StringBuilder ) v3 ).append ( v12 ); // invoke-virtual {v3, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v3 ).append ( v14 ); // invoke-virtual {v3, v14}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
final String v12 = " given, resolves to "; // const-string v12, " given, resolves to "
(( java.lang.StringBuilder ) v3 ).append ( v12 ); // invoke-virtual {v3, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
/* .line 240 */
(( java.util.TimeZone ) v5 ).getID ( ); // invoke-virtual {v5}, Ljava/util/TimeZone;->getID()Ljava/lang/String;
(( java.lang.StringBuilder ) v3 ).append ( v12 ); // invoke-virtual {v3, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v3 ).toString ( ); // invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
/* invoke-direct {v7, v3}, Ljava/lang/IndexOutOfBoundsException;-><init>(Ljava/lang/String;)V */
} // .end local v16 # "fail":Ljava/lang/Exception;
} // .end local p0 # "date":Ljava/lang/String;
} // .end local p1 # "pos":Ljava/text/ParsePosition;
/* throw v7 */
/* .line 231 */
} // .end local v18 # "timezoneOffset":Ljava/lang/String;
} // .end local v19 # "offset":I
} // .end local v20 # "cleaned":Ljava/lang/String;
} // .end local v21 # "hasT":Z
/* .local v3, "timezoneOffset":Ljava/lang/String; */
/* .restart local v7 # "offset":I */
/* .restart local v12 # "hasT":Z */
/* .restart local v16 # "fail":Ljava/lang/Exception; */
/* .restart local p0 # "date":Ljava/lang/String; */
/* .restart local p1 # "pos":Ljava/text/ParsePosition; */
} // :cond_e
/* move-object/from16 v18, v3 */
/* move/from16 v19, v7 */
/* move/from16 v21, v12 */
} // .end local v3 # "timezoneOffset":Ljava/lang/String;
} // .end local v7 # "offset":I
} // .end local v12 # "hasT":Z
/* .restart local v18 # "timezoneOffset":Ljava/lang/String; */
/* .restart local v19 # "offset":I */
/* .restart local v21 # "hasT":Z */
/* .line 218 */
} // .end local v14 # "timezoneId":Ljava/lang/String;
} // .end local v15 # "act":Ljava/lang/String;
} // .end local v18 # "timezoneOffset":Ljava/lang/String;
} // .end local v19 # "offset":I
} // .end local v21 # "hasT":Z
/* .restart local v3 # "timezoneOffset":Ljava/lang/String; */
/* .restart local v7 # "offset":I */
/* .restart local v12 # "hasT":Z */
} // :cond_f
/* move-object/from16 v18, v3 */
/* move/from16 v19, v7 */
/* move/from16 v21, v12 */
/* .line 219 */
} // .end local v3 # "timezoneOffset":Ljava/lang/String;
} // .end local v7 # "offset":I
} // .end local v12 # "hasT":Z
/* .restart local v18 # "timezoneOffset":Ljava/lang/String; */
/* .restart local v19 # "offset":I */
/* .restart local v21 # "hasT":Z */
} // :goto_6
v3 = com.google.gson.internal.bind.util.ISO8601Utils.TIMEZONE_UTC;
/* move-object v5, v3 */
/* .line 244 */
} // .end local v18 # "timezoneOffset":Ljava/lang/String;
} // :goto_7
/* move/from16 v7, v19 */
/* .line 248 */
} // .end local v19 # "offset":I
/* .restart local v7 # "offset":I */
} // :goto_8
/* new-instance v3, Ljava/util/GregorianCalendar; */
/* invoke-direct {v3, v5}, Ljava/util/GregorianCalendar;-><init>(Ljava/util/TimeZone;)V */
/* .line 249 */
/* .local v3, "calendar":Ljava/util/Calendar; */
int v12 = 0; // const/4 v12, 0x0
(( java.util.Calendar ) v3 ).setLenient ( v12 ); // invoke-virtual {v3, v12}, Ljava/util/Calendar;->setLenient(Z)V
/* .line 250 */
int v12 = 1; // const/4 v12, 0x1
(( java.util.Calendar ) v3 ).set ( v12, v0 ); // invoke-virtual {v3, v12, v0}, Ljava/util/Calendar;->set(II)V
/* .line 251 */
/* add-int/lit8 v12, v4, -0x1 */
int v14 = 2; // const/4 v14, 0x2
(( java.util.Calendar ) v3 ).set ( v14, v12 ); // invoke-virtual {v3, v14, v12}, Ljava/util/Calendar;->set(II)V
/* .line 252 */
int v12 = 5; // const/4 v12, 0x5
(( java.util.Calendar ) v3 ).set ( v12, v6 ); // invoke-virtual {v3, v12, v6}, Ljava/util/Calendar;->set(II)V
/* .line 253 */
/* const/16 v12, 0xb */
(( java.util.Calendar ) v3 ).set ( v12, v8 ); // invoke-virtual {v3, v12, v8}, Ljava/util/Calendar;->set(II)V
/* .line 254 */
/* const/16 v12, 0xc */
(( java.util.Calendar ) v3 ).set ( v12, v9 ); // invoke-virtual {v3, v12, v9}, Ljava/util/Calendar;->set(II)V
/* .line 255 */
/* const/16 v12, 0xd */
(( java.util.Calendar ) v3 ).set ( v12, v10 ); // invoke-virtual {v3, v12, v10}, Ljava/util/Calendar;->set(II)V
/* .line 256 */
/* const/16 v12, 0xe */
(( java.util.Calendar ) v3 ).set ( v12, v11 ); // invoke-virtual {v3, v12, v11}, Ljava/util/Calendar;->set(II)V
/* .line 258 */
(( java.text.ParsePosition ) v2 ).setIndex ( v7 ); // invoke-virtual {v2, v7}, Ljava/text/ParsePosition;->setIndex(I)V
/* .line 259 */
(( java.util.Calendar ) v3 ).getTime ( ); // invoke-virtual {v3}, Ljava/util/Calendar;->getTime()Ljava/util/Date;
/* .line 201 */
} // .end local v5 # "timezone":Ljava/util/TimeZone;
} // .end local v13 # "timezoneIndicator":C
} // .end local v16 # "fail":Ljava/lang/Exception;
} // .end local v21 # "hasT":Z
/* .local v3, "fail":Ljava/lang/Exception; */
/* .restart local v12 # "hasT":Z */
} // :cond_10
/* move-object/from16 v16, v3 */
/* move/from16 v21, v12 */
} // .end local v3 # "fail":Ljava/lang/Exception;
} // .end local v12 # "hasT":Z
/* .restart local v16 # "fail":Ljava/lang/Exception; */
/* .restart local v21 # "hasT":Z */
/* new-instance v3, Ljava/lang/IllegalArgumentException; */
final String v5 = "No time zone indicator"; // const-string v5, "No time zone indicator"
/* invoke-direct {v3, v5}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V */
} // .end local v16 # "fail":Ljava/lang/Exception;
} // .end local p0 # "date":Ljava/lang/String;
} // .end local p1 # "pos":Ljava/text/ParsePosition;
/* throw v3 */
/* :try_end_5 */
/* .catch Ljava/lang/IndexOutOfBoundsException; {:try_start_5 ..:try_end_5} :catch_5 */
/* .catch Ljava/lang/NumberFormatException; {:try_start_5 ..:try_end_5} :catch_4 */
/* .catch Ljava/lang/IllegalArgumentException; {:try_start_5 ..:try_end_5} :catch_3 */
/* .line 266 */
} // .end local v0 # "year":I
} // .end local v4 # "month":I
} // .end local v6 # "day":I
} // .end local v7 # "offset":I
} // .end local v8 # "hour":I
} // .end local v9 # "minutes":I
} // .end local v10 # "seconds":I
} // .end local v11 # "milliseconds":I
} // .end local v21 # "hasT":Z
/* .restart local v16 # "fail":Ljava/lang/Exception; */
/* .restart local p0 # "date":Ljava/lang/String; */
/* .restart local p1 # "pos":Ljava/text/ParsePosition; */
/* :catch_3 */
/* move-exception v0 */
/* .line 264 */
/* :catch_4 */
/* move-exception v0 */
/* .line 262 */
/* :catch_5 */
/* move-exception v0 */
/* .line 266 */
} // .end local v16 # "fail":Ljava/lang/Exception;
/* .restart local v3 # "fail":Ljava/lang/Exception; */
/* :catch_6 */
/* move-exception v0 */
/* move-object/from16 v16, v3 */
/* .line 267 */
} // .end local v3 # "fail":Ljava/lang/Exception;
/* .local v0, "e":Ljava/lang/IllegalArgumentException; */
/* .restart local v16 # "fail":Ljava/lang/Exception; */
} // :goto_9
/* move-object v3, v0 */
} // .end local v16 # "fail":Ljava/lang/Exception;
/* .restart local v3 # "fail":Ljava/lang/Exception; */
/* .line 264 */
} // .end local v0 # "e":Ljava/lang/IllegalArgumentException;
/* :catch_7 */
/* move-exception v0 */
/* move-object/from16 v16, v3 */
/* .line 265 */
} // .end local v3 # "fail":Ljava/lang/Exception;
/* .local v0, "e":Ljava/lang/NumberFormatException; */
/* .restart local v16 # "fail":Ljava/lang/Exception; */
} // :goto_a
/* move-object v3, v0 */
} // .end local v0 # "e":Ljava/lang/NumberFormatException;
} // .end local v16 # "fail":Ljava/lang/Exception;
/* .restart local v3 # "fail":Ljava/lang/Exception; */
/* .line 262 */
/* :catch_8 */
/* move-exception v0 */
/* move-object/from16 v16, v3 */
/* .line 263 */
} // .end local v3 # "fail":Ljava/lang/Exception;
/* .local v0, "e":Ljava/lang/IndexOutOfBoundsException; */
/* .restart local v16 # "fail":Ljava/lang/Exception; */
} // :goto_b
/* move-object v3, v0 */
/* .line 268 */
} // .end local v0 # "e":Ljava/lang/IndexOutOfBoundsException;
} // .end local v16 # "fail":Ljava/lang/Exception;
/* .restart local v3 # "fail":Ljava/lang/Exception; */
} // :goto_c
/* nop */
/* .line 269 */
} // :goto_d
/* if-nez v1, :cond_11 */
int v0 = 0; // const/4 v0, 0x0
} // :cond_11
/* new-instance v0, Ljava/lang/StringBuilder; */
/* invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V */
/* const/16 v4, 0x22 */
(( java.lang.StringBuilder ) v0 ).append ( v4 ); // invoke-virtual {v0, v4}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v0 ).append ( v4 ); // invoke-virtual {v0, v4}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v0 ).toString ( ); // invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
/* .line 270 */
/* .local v0, "input":Ljava/lang/String; */
} // :goto_e
(( java.lang.Exception ) v3 ).getMessage ( ); // invoke-virtual {v3}, Ljava/lang/Exception;->getMessage()Ljava/lang/String;
/* .line 271 */
/* .local v4, "msg":Ljava/lang/String; */
if ( v4 != null) { // if-eqz v4, :cond_12
v5 = (( java.lang.String ) v4 ).isEmpty ( ); // invoke-virtual {v4}, Ljava/lang/String;->isEmpty()Z
if ( v5 != null) { // if-eqz v5, :cond_13
/* .line 272 */
} // :cond_12
/* new-instance v5, Ljava/lang/StringBuilder; */
/* invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V */
final String v6 = "("; // const-string v6, "("
(( java.lang.StringBuilder ) v5 ).append ( v6 ); // invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.Object ) v3 ).getClass ( ); // invoke-virtual {v3}, Ljava/lang/Object;->getClass()Ljava/lang/Class;
(( java.lang.Class ) v6 ).getName ( ); // invoke-virtual {v6}, Ljava/lang/Class;->getName()Ljava/lang/String;
(( java.lang.StringBuilder ) v5 ).append ( v6 ); // invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
final String v6 = ")"; // const-string v6, ")"
(( java.lang.StringBuilder ) v5 ).append ( v6 ); // invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v5 ).toString ( ); // invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
/* .line 274 */
} // :cond_13
/* new-instance v5, Ljava/text/ParseException; */
/* new-instance v6, Ljava/lang/StringBuilder; */
/* invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V */
final String v7 = "Failed to parse date ["; // const-string v7, "Failed to parse date ["
(( java.lang.StringBuilder ) v6 ).append ( v7 ); // invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v6 ).append ( v0 ); // invoke-virtual {v6, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
final String v7 = "]: "; // const-string v7, "]: "
(( java.lang.StringBuilder ) v6 ).append ( v7 ); // invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v6 ).append ( v4 ); // invoke-virtual {v6, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v6 ).toString ( ); // invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
v7 = /* invoke-virtual/range {p1 ..p1}, Ljava/text/ParsePosition;->getIndex()I */
/* invoke-direct {v5, v6, v7}, Ljava/text/ParseException;-><init>(Ljava/lang/String;I)V */
/* .line 275 */
/* .local v5, "ex":Ljava/text/ParseException; */
(( java.text.ParseException ) v5 ).initCause ( v3 ); // invoke-virtual {v5, v3}, Ljava/text/ParseException;->initCause(Ljava/lang/Throwable;)Ljava/lang/Throwable;
/* .line 276 */
/* throw v5 */
/* :pswitch_data_0 */
/* .packed-switch 0x1 */
/* :pswitch_1 */
/* :pswitch_0 */
} // .end packed-switch
} // .end method
private static Integer parseInt ( java.lang.String p0, Integer p1, Integer p2 ) {
/* .locals 6 */
/* .param p0, "value" # Ljava/lang/String; */
/* .param p1, "beginIndex" # I */
/* .param p2, "endIndex" # I */
/* .annotation system Ldalvik/annotation/Throws; */
/* value = { */
/* Ljava/lang/NumberFormatException; */
/* } */
} // .end annotation
/* .line 301 */
/* if-ltz p1, :cond_4 */
v0 = (( java.lang.String ) p0 ).length ( ); // invoke-virtual {p0}, Ljava/lang/String;->length()I
/* if-gt p2, v0, :cond_4 */
/* if-gt p1, p2, :cond_4 */
/* .line 305 */
/* move v0, p1 */
/* .line 306 */
/* .local v0, "i":I */
int v1 = 0; // const/4 v1, 0x0
/* .line 308 */
/* .local v1, "result":I */
final String v2 = "Invalid number: "; // const-string v2, "Invalid number: "
/* const/16 v3, 0xa */
/* if-ge v0, p2, :cond_1 */
/* .line 309 */
/* add-int/lit8 v4, v0, 0x1 */
} // .end local v0 # "i":I
/* .local v4, "i":I */
v0 = (( java.lang.String ) p0 ).charAt ( v0 ); // invoke-virtual {p0, v0}, Ljava/lang/String;->charAt(I)C
v0 = java.lang.Character .digit ( v0,v3 );
/* .line 310 */
/* .local v0, "digit":I */
/* if-ltz v0, :cond_0 */
/* .line 313 */
/* neg-int v1, v0 */
/* move v0, v4 */
/* .line 311 */
} // :cond_0
/* new-instance v3, Ljava/lang/NumberFormatException; */
/* new-instance v5, Ljava/lang/StringBuilder; */
/* invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V */
(( java.lang.StringBuilder ) v5 ).append ( v2 ); // invoke-virtual {v5, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.String ) p0 ).substring ( p1, p2 ); // invoke-virtual {p0, p1, p2}, Ljava/lang/String;->substring(II)Ljava/lang/String;
(( java.lang.StringBuilder ) v2 ).append ( v5 ); // invoke-virtual {v2, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v2 ).toString ( ); // invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
/* invoke-direct {v3, v2}, Ljava/lang/NumberFormatException;-><init>(Ljava/lang/String;)V */
/* throw v3 */
/* .line 315 */
} // .end local v4 # "i":I
/* .local v0, "i":I */
} // :cond_1
} // :goto_0
/* if-ge v0, p2, :cond_3 */
/* .line 316 */
/* add-int/lit8 v4, v0, 0x1 */
} // .end local v0 # "i":I
/* .restart local v4 # "i":I */
v0 = (( java.lang.String ) p0 ).charAt ( v0 ); // invoke-virtual {p0, v0}, Ljava/lang/String;->charAt(I)C
v0 = java.lang.Character .digit ( v0,v3 );
/* .line 317 */
/* .local v0, "digit":I */
/* if-ltz v0, :cond_2 */
/* .line 320 */
/* mul-int/lit8 v1, v1, 0xa */
/* .line 321 */
/* sub-int/2addr v1, v0 */
/* move v0, v4 */
/* .line 318 */
} // :cond_2
/* new-instance v3, Ljava/lang/NumberFormatException; */
/* new-instance v5, Ljava/lang/StringBuilder; */
/* invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V */
(( java.lang.StringBuilder ) v5 ).append ( v2 ); // invoke-virtual {v5, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.String ) p0 ).substring ( p1, p2 ); // invoke-virtual {p0, p1, p2}, Ljava/lang/String;->substring(II)Ljava/lang/String;
(( java.lang.StringBuilder ) v2 ).append ( v5 ); // invoke-virtual {v2, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v2 ).toString ( ); // invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
/* invoke-direct {v3, v2}, Ljava/lang/NumberFormatException;-><init>(Ljava/lang/String;)V */
/* throw v3 */
/* .line 323 */
} // .end local v4 # "i":I
/* .local v0, "i":I */
} // :cond_3
/* neg-int v2, v1 */
/* .line 302 */
} // .end local v0 # "i":I
} // .end local v1 # "result":I
} // :cond_4
/* new-instance v0, Ljava/lang/NumberFormatException; */
/* invoke-direct {v0, p0}, Ljava/lang/NumberFormatException;-><init>(Ljava/lang/String;)V */
/* throw v0 */
} // .end method
