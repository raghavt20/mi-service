class com.google.gson.internal.bind.TypeAdapters$34$1 extends com.google.gson.TypeAdapter {
	 /* .source "TypeAdapters.java" */
	 /* # annotations */
	 /* .annotation system Ldalvik/annotation/EnclosingMethod; */
	 /* value = Lcom/google/gson/internal/bind/TypeAdapters$34;->create(Lcom/google/gson/Gson;Lcom/google/gson/reflect/TypeToken;)Lcom/google/gson/TypeAdapter; */
} // .end annotation
/* .annotation system Ldalvik/annotation/InnerClass; */
/* accessFlags = 0x0 */
/* name = null */
} // .end annotation
/* .annotation system Ldalvik/annotation/Signature; */
/* value = { */
/* "Lcom/google/gson/TypeAdapter<", */
/* "TT1;>;" */
/* } */
} // .end annotation
/* # instance fields */
final com.google.gson.internal.bind.TypeAdapters$34 this$0; //synthetic
final java.lang.Class val$requestedType; //synthetic
/* # direct methods */
 com.google.gson.internal.bind.TypeAdapters$34$1 ( ) {
/* .locals 0 */
/* .param p1, "this$0" # Lcom/google/gson/internal/bind/TypeAdapters$34; */
/* .line 911 */
this.this$0 = p1;
this.val$requestedType = p2;
/* invoke-direct {p0}, Lcom/google/gson/TypeAdapter;-><init>()V */
return;
} // .end method
/* # virtual methods */
public java.lang.Object read ( com.google.gson.stream.JsonReader p0 ) {
/* .locals 4 */
/* .param p1, "in" # Lcom/google/gson/stream/JsonReader; */
/* .annotation system Ldalvik/annotation/Signature; */
/* value = { */
/* "(", */
/* "Lcom/google/gson/stream/JsonReader;", */
/* ")TT1;" */
/* } */
} // .end annotation
/* .annotation system Ldalvik/annotation/Throws; */
/* value = { */
/* Ljava/io/IOException; */
/* } */
} // .end annotation
/* .line 917 */
v0 = this.this$0;
v0 = this.val$typeAdapter;
(( com.google.gson.TypeAdapter ) v0 ).read ( p1 ); // invoke-virtual {v0, p1}, Lcom/google/gson/TypeAdapter;->read(Lcom/google/gson/stream/JsonReader;)Ljava/lang/Object;
/* .line 918 */
/* .local v0, "result":Ljava/lang/Object;, "TT1;" */
if ( v0 != null) { // if-eqz v0, :cond_1
v1 = this.val$requestedType;
v1 = (( java.lang.Class ) v1 ).isInstance ( v0 ); // invoke-virtual {v1, v0}, Ljava/lang/Class;->isInstance(Ljava/lang/Object;)Z
if ( v1 != null) { // if-eqz v1, :cond_0
/* .line 919 */
} // :cond_0
/* new-instance v1, Lcom/google/gson/JsonSyntaxException; */
/* new-instance v2, Ljava/lang/StringBuilder; */
/* invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V */
final String v3 = "Expected a "; // const-string v3, "Expected a "
(( java.lang.StringBuilder ) v2 ).append ( v3 ); // invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
v3 = this.val$requestedType;
(( java.lang.Class ) v3 ).getName ( ); // invoke-virtual {v3}, Ljava/lang/Class;->getName()Ljava/lang/String;
(( java.lang.StringBuilder ) v2 ).append ( v3 ); // invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
final String v3 = " but was "; // const-string v3, " but was "
(( java.lang.StringBuilder ) v2 ).append ( v3 ); // invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
/* .line 920 */
(( java.lang.Object ) v0 ).getClass ( ); // invoke-virtual {v0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;
(( java.lang.Class ) v3 ).getName ( ); // invoke-virtual {v3}, Ljava/lang/Class;->getName()Ljava/lang/String;
(( java.lang.StringBuilder ) v2 ).append ( v3 ); // invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
final String v3 = "; at path "; // const-string v3, "; at path "
(( java.lang.StringBuilder ) v2 ).append ( v3 ); // invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( com.google.gson.stream.JsonReader ) p1 ).getPreviousPath ( ); // invoke-virtual {p1}, Lcom/google/gson/stream/JsonReader;->getPreviousPath()Ljava/lang/String;
(( java.lang.StringBuilder ) v2 ).append ( v3 ); // invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v2 ).toString ( ); // invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
/* invoke-direct {v1, v2}, Lcom/google/gson/JsonSyntaxException;-><init>(Ljava/lang/String;)V */
/* throw v1 */
/* .line 922 */
} // :cond_1
} // :goto_0
} // .end method
public void write ( com.google.gson.stream.JsonWriter p0, java.lang.Object p1 ) {
/* .locals 1 */
/* .param p1, "out" # Lcom/google/gson/stream/JsonWriter; */
/* .annotation system Ldalvik/annotation/Signature; */
/* value = { */
/* "(", */
/* "Lcom/google/gson/stream/JsonWriter;", */
/* "TT1;)V" */
/* } */
} // .end annotation
/* .annotation system Ldalvik/annotation/Throws; */
/* value = { */
/* Ljava/io/IOException; */
/* } */
} // .end annotation
/* .line 913 */
/* .local p2, "value":Ljava/lang/Object;, "TT1;" */
v0 = this.this$0;
v0 = this.val$typeAdapter;
(( com.google.gson.TypeAdapter ) v0 ).write ( p1, p2 ); // invoke-virtual {v0, p1, p2}, Lcom/google/gson/TypeAdapter;->write(Lcom/google/gson/stream/JsonWriter;Ljava/lang/Object;)V
/* .line 914 */
return;
} // .end method
