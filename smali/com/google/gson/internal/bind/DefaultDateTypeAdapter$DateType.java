public abstract class com.google.gson.internal.bind.DefaultDateTypeAdapter$DateType {
	 /* .source "DefaultDateTypeAdapter.java" */
	 /* # annotations */
	 /* .annotation system Ldalvik/annotation/EnclosingClass; */
	 /* value = Lcom/google/gson/internal/bind/DefaultDateTypeAdapter; */
} // .end annotation
/* .annotation system Ldalvik/annotation/InnerClass; */
/* accessFlags = 0x409 */
/* name = "DateType" */
} // .end annotation
/* .annotation system Ldalvik/annotation/Signature; */
/* value = { */
/* "<T:", */
/* "Ljava/util/Date;", */
/* ">", */
/* "Ljava/lang/Object;" */
/* } */
} // .end annotation
/* # static fields */
public static final com.google.gson.internal.bind.DefaultDateTypeAdapter$DateType DATE;
/* .annotation system Ldalvik/annotation/Signature; */
/* value = { */
/* "Lcom/google/gson/internal/bind/DefaultDateTypeAdapter$DateType<", */
/* "Ljava/util/Date;", */
/* ">;" */
/* } */
} // .end annotation
} // .end field
/* # instance fields */
private final java.lang.Class dateClass;
/* .annotation system Ldalvik/annotation/Signature; */
/* value = { */
/* "Ljava/lang/Class<", */
/* "TT;>;" */
/* } */
} // .end annotation
} // .end field
/* # direct methods */
static com.google.gson.internal.bind.DefaultDateTypeAdapter$DateType ( ) {
/* .locals 2 */
/* .line 52 */
/* new-instance v0, Lcom/google/gson/internal/bind/DefaultDateTypeAdapter$DateType$1; */
/* const-class v1, Ljava/util/Date; */
/* invoke-direct {v0, v1}, Lcom/google/gson/internal/bind/DefaultDateTypeAdapter$DateType$1;-><init>(Ljava/lang/Class;)V */
return;
} // .end method
protected com.google.gson.internal.bind.DefaultDateTypeAdapter$DateType ( ) {
/* .locals 0 */
/* .annotation system Ldalvik/annotation/Signature; */
/* value = { */
/* "(", */
/* "Ljava/lang/Class<", */
/* "TT;>;)V" */
/* } */
} // .end annotation
/* .line 60 */
/* .local p0, "this":Lcom/google/gson/internal/bind/DefaultDateTypeAdapter$DateType;, "Lcom/google/gson/internal/bind/DefaultDateTypeAdapter$DateType<TT;>;" */
/* .local p1, "dateClass":Ljava/lang/Class;, "Ljava/lang/Class<TT;>;" */
/* invoke-direct {p0}, Ljava/lang/Object;-><init>()V */
/* .line 61 */
this.dateClass = p1;
/* .line 62 */
return;
} // .end method
private final com.google.gson.TypeAdapterFactory createFactory ( com.google.gson.internal.bind.DefaultDateTypeAdapter p0 ) {
/* .locals 1 */
/* .annotation system Ldalvik/annotation/Signature; */
/* value = { */
/* "(", */
/* "Lcom/google/gson/internal/bind/DefaultDateTypeAdapter<", */
/* "TT;>;)", */
/* "Lcom/google/gson/TypeAdapterFactory;" */
/* } */
} // .end annotation
/* .line 67 */
/* .local p0, "this":Lcom/google/gson/internal/bind/DefaultDateTypeAdapter$DateType;, "Lcom/google/gson/internal/bind/DefaultDateTypeAdapter$DateType<TT;>;" */
/* .local p1, "adapter":Lcom/google/gson/internal/bind/DefaultDateTypeAdapter;, "Lcom/google/gson/internal/bind/DefaultDateTypeAdapter<TT;>;" */
v0 = this.dateClass;
com.google.gson.internal.bind.TypeAdapters .newFactory ( v0,p1 );
} // .end method
/* # virtual methods */
public final com.google.gson.TypeAdapterFactory createAdapterFactory ( Integer p0 ) {
/* .locals 2 */
/* .param p1, "style" # I */
/* .line 75 */
/* .local p0, "this":Lcom/google/gson/internal/bind/DefaultDateTypeAdapter$DateType;, "Lcom/google/gson/internal/bind/DefaultDateTypeAdapter$DateType<TT;>;" */
/* new-instance v0, Lcom/google/gson/internal/bind/DefaultDateTypeAdapter; */
int v1 = 0; // const/4 v1, 0x0
/* invoke-direct {v0, p0, p1, v1}, Lcom/google/gson/internal/bind/DefaultDateTypeAdapter;-><init>(Lcom/google/gson/internal/bind/DefaultDateTypeAdapter$DateType;ILcom/google/gson/internal/bind/DefaultDateTypeAdapter$1;)V */
/* invoke-direct {p0, v0}, Lcom/google/gson/internal/bind/DefaultDateTypeAdapter$DateType;->createFactory(Lcom/google/gson/internal/bind/DefaultDateTypeAdapter;)Lcom/google/gson/TypeAdapterFactory; */
} // .end method
public final com.google.gson.TypeAdapterFactory createAdapterFactory ( Integer p0, Integer p1 ) {
/* .locals 2 */
/* .param p1, "dateStyle" # I */
/* .param p2, "timeStyle" # I */
/* .line 79 */
/* .local p0, "this":Lcom/google/gson/internal/bind/DefaultDateTypeAdapter$DateType;, "Lcom/google/gson/internal/bind/DefaultDateTypeAdapter$DateType<TT;>;" */
/* new-instance v0, Lcom/google/gson/internal/bind/DefaultDateTypeAdapter; */
int v1 = 0; // const/4 v1, 0x0
/* invoke-direct {v0, p0, p1, p2, v1}, Lcom/google/gson/internal/bind/DefaultDateTypeAdapter;-><init>(Lcom/google/gson/internal/bind/DefaultDateTypeAdapter$DateType;IILcom/google/gson/internal/bind/DefaultDateTypeAdapter$1;)V */
/* invoke-direct {p0, v0}, Lcom/google/gson/internal/bind/DefaultDateTypeAdapter$DateType;->createFactory(Lcom/google/gson/internal/bind/DefaultDateTypeAdapter;)Lcom/google/gson/TypeAdapterFactory; */
} // .end method
public final com.google.gson.TypeAdapterFactory createAdapterFactory ( java.lang.String p0 ) {
/* .locals 2 */
/* .param p1, "datePattern" # Ljava/lang/String; */
/* .line 71 */
/* .local p0, "this":Lcom/google/gson/internal/bind/DefaultDateTypeAdapter$DateType;, "Lcom/google/gson/internal/bind/DefaultDateTypeAdapter$DateType<TT;>;" */
/* new-instance v0, Lcom/google/gson/internal/bind/DefaultDateTypeAdapter; */
int v1 = 0; // const/4 v1, 0x0
/* invoke-direct {v0, p0, p1, v1}, Lcom/google/gson/internal/bind/DefaultDateTypeAdapter;-><init>(Lcom/google/gson/internal/bind/DefaultDateTypeAdapter$DateType;Ljava/lang/String;Lcom/google/gson/internal/bind/DefaultDateTypeAdapter$1;)V */
/* invoke-direct {p0, v0}, Lcom/google/gson/internal/bind/DefaultDateTypeAdapter$DateType;->createFactory(Lcom/google/gson/internal/bind/DefaultDateTypeAdapter;)Lcom/google/gson/TypeAdapterFactory; */
} // .end method
public final com.google.gson.TypeAdapterFactory createDefaultsAdapterFactory ( ) {
/* .locals 3 */
/* .line 83 */
/* .local p0, "this":Lcom/google/gson/internal/bind/DefaultDateTypeAdapter$DateType;, "Lcom/google/gson/internal/bind/DefaultDateTypeAdapter$DateType<TT;>;" */
/* new-instance v0, Lcom/google/gson/internal/bind/DefaultDateTypeAdapter; */
int v1 = 2; // const/4 v1, 0x2
int v2 = 0; // const/4 v2, 0x0
/* invoke-direct {v0, p0, v1, v1, v2}, Lcom/google/gson/internal/bind/DefaultDateTypeAdapter;-><init>(Lcom/google/gson/internal/bind/DefaultDateTypeAdapter$DateType;IILcom/google/gson/internal/bind/DefaultDateTypeAdapter$1;)V */
/* invoke-direct {p0, v0}, Lcom/google/gson/internal/bind/DefaultDateTypeAdapter$DateType;->createFactory(Lcom/google/gson/internal/bind/DefaultDateTypeAdapter;)Lcom/google/gson/TypeAdapterFactory; */
} // .end method
protected abstract java.util.Date deserialize ( java.util.Date p0 ) {
/* .annotation system Ldalvik/annotation/Signature; */
/* value = { */
/* "(", */
/* "Ljava/util/Date;", */
/* ")TT;" */
/* } */
} // .end annotation
} // .end method
