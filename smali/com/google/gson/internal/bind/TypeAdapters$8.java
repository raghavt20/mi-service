class com.google.gson.internal.bind.TypeAdapters$8 extends com.google.gson.TypeAdapter {
	 /* .source "TypeAdapters.java" */
	 /* # annotations */
	 /* .annotation system Ldalvik/annotation/EnclosingClass; */
	 /* value = Lcom/google/gson/internal/bind/TypeAdapters; */
} // .end annotation
/* .annotation system Ldalvik/annotation/InnerClass; */
/* accessFlags = 0x0 */
/* name = null */
} // .end annotation
/* .annotation system Ldalvik/annotation/Signature; */
/* value = { */
/* "Lcom/google/gson/TypeAdapter<", */
/* "Ljava/util/concurrent/atomic/AtomicInteger;", */
/* ">;" */
/* } */
} // .end annotation
/* # direct methods */
 com.google.gson.internal.bind.TypeAdapters$8 ( ) {
/* .locals 0 */
/* .line 253 */
/* invoke-direct {p0}, Lcom/google/gson/TypeAdapter;-><init>()V */
return;
} // .end method
/* # virtual methods */
public java.lang.Object read ( com.google.gson.stream.JsonReader p0 ) { //bridge//synthethic
/* .locals 0 */
/* .annotation system Ldalvik/annotation/Throws; */
/* value = { */
/* Ljava/io/IOException; */
/* } */
} // .end annotation
/* .line 253 */
(( com.google.gson.internal.bind.TypeAdapters$8 ) p0 ).read ( p1 ); // invoke-virtual {p0, p1}, Lcom/google/gson/internal/bind/TypeAdapters$8;->read(Lcom/google/gson/stream/JsonReader;)Ljava/util/concurrent/atomic/AtomicInteger;
} // .end method
public java.util.concurrent.atomic.AtomicInteger read ( com.google.gson.stream.JsonReader p0 ) {
/* .locals 2 */
/* .param p1, "in" # Lcom/google/gson/stream/JsonReader; */
/* .annotation system Ldalvik/annotation/Throws; */
/* value = { */
/* Ljava/io/IOException; */
/* } */
} // .end annotation
/* .line 256 */
try { // :try_start_0
/* new-instance v0, Ljava/util/concurrent/atomic/AtomicInteger; */
v1 = (( com.google.gson.stream.JsonReader ) p1 ).nextInt ( ); // invoke-virtual {p1}, Lcom/google/gson/stream/JsonReader;->nextInt()I
/* invoke-direct {v0, v1}, Ljava/util/concurrent/atomic/AtomicInteger;-><init>(I)V */
/* :try_end_0 */
/* .catch Ljava/lang/NumberFormatException; {:try_start_0 ..:try_end_0} :catch_0 */
/* .line 257 */
/* :catch_0 */
/* move-exception v0 */
/* .line 258 */
/* .local v0, "e":Ljava/lang/NumberFormatException; */
/* new-instance v1, Lcom/google/gson/JsonSyntaxException; */
/* invoke-direct {v1, v0}, Lcom/google/gson/JsonSyntaxException;-><init>(Ljava/lang/Throwable;)V */
/* throw v1 */
} // .end method
public void write ( com.google.gson.stream.JsonWriter p0, java.lang.Object p1 ) { //bridge//synthethic
/* .locals 0 */
/* .annotation system Ldalvik/annotation/Throws; */
/* value = { */
/* Ljava/io/IOException; */
/* } */
} // .end annotation
/* .line 253 */
/* check-cast p2, Ljava/util/concurrent/atomic/AtomicInteger; */
(( com.google.gson.internal.bind.TypeAdapters$8 ) p0 ).write ( p1, p2 ); // invoke-virtual {p0, p1, p2}, Lcom/google/gson/internal/bind/TypeAdapters$8;->write(Lcom/google/gson/stream/JsonWriter;Ljava/util/concurrent/atomic/AtomicInteger;)V
return;
} // .end method
public void write ( com.google.gson.stream.JsonWriter p0, java.util.concurrent.atomic.AtomicInteger p1 ) {
/* .locals 2 */
/* .param p1, "out" # Lcom/google/gson/stream/JsonWriter; */
/* .param p2, "value" # Ljava/util/concurrent/atomic/AtomicInteger; */
/* .annotation system Ldalvik/annotation/Throws; */
/* value = { */
/* Ljava/io/IOException; */
/* } */
} // .end annotation
/* .line 262 */
v0 = (( java.util.concurrent.atomic.AtomicInteger ) p2 ).get ( ); // invoke-virtual {p2}, Ljava/util/concurrent/atomic/AtomicInteger;->get()I
/* int-to-long v0, v0 */
(( com.google.gson.stream.JsonWriter ) p1 ).value ( v0, v1 ); // invoke-virtual {p1, v0, v1}, Lcom/google/gson/stream/JsonWriter;->value(J)Lcom/google/gson/stream/JsonWriter;
/* .line 263 */
return;
} // .end method
