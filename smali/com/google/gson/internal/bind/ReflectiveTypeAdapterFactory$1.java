class com.google.gson.internal.bind.ReflectiveTypeAdapterFactory$1 extends com.google.gson.internal.bind.ReflectiveTypeAdapterFactory$BoundField {
	 /* .source "ReflectiveTypeAdapterFactory.java" */
	 /* # annotations */
	 /* .annotation system Ldalvik/annotation/EnclosingMethod; */
	 /* value = Lcom/google/gson/internal/bind/ReflectiveTypeAdapterFactory;->createBoundField(Lcom/google/gson/Gson;Ljava/lang/reflect/Field;Ljava/lang/String;Lcom/google/gson/reflect/TypeToken;ZZ)Lcom/google/gson/internal/bind/ReflectiveTypeAdapterFactory$BoundField; */
} // .end annotation
/* .annotation system Ldalvik/annotation/InnerClass; */
/* accessFlags = 0x0 */
/* name = null */
} // .end annotation
/* # instance fields */
final com.google.gson.internal.bind.ReflectiveTypeAdapterFactory this$0; //synthetic
final com.google.gson.Gson val$context; //synthetic
final java.lang.reflect.Field val$field; //synthetic
final com.google.gson.reflect.TypeToken val$fieldType; //synthetic
final Boolean val$isPrimitive; //synthetic
final Boolean val$jsonAdapterPresent; //synthetic
final com.google.gson.TypeAdapter val$typeAdapter; //synthetic
/* # direct methods */
 com.google.gson.internal.bind.ReflectiveTypeAdapterFactory$1 ( ) {
/* .locals 0 */
/* .param p1, "this$0" # Lcom/google/gson/internal/bind/ReflectiveTypeAdapterFactory; */
/* .param p2, "name" # Ljava/lang/String; */
/* .param p3, "serialized" # Z */
/* .param p4, "deserialized" # Z */
/* .line 119 */
this.this$0 = p1;
this.val$field = p5;
/* iput-boolean p6, p0, Lcom/google/gson/internal/bind/ReflectiveTypeAdapterFactory$1;->val$jsonAdapterPresent:Z */
this.val$typeAdapter = p7;
this.val$context = p8;
this.val$fieldType = p9;
/* iput-boolean p10, p0, Lcom/google/gson/internal/bind/ReflectiveTypeAdapterFactory$1;->val$isPrimitive:Z */
/* invoke-direct {p0, p2, p3, p4}, Lcom/google/gson/internal/bind/ReflectiveTypeAdapterFactory$BoundField;-><init>(Ljava/lang/String;ZZ)V */
return;
} // .end method
/* # virtual methods */
void read ( com.google.gson.stream.JsonReader p0, java.lang.Object p1 ) {
/* .locals 2 */
/* .param p1, "reader" # Lcom/google/gson/stream/JsonReader; */
/* .param p2, "value" # Ljava/lang/Object; */
/* .annotation system Ldalvik/annotation/Throws; */
/* value = { */
/* Ljava/io/IOException;, */
/* Ljava/lang/IllegalAccessException; */
/* } */
} // .end annotation
/* .line 130 */
v0 = this.val$typeAdapter;
(( com.google.gson.TypeAdapter ) v0 ).read ( p1 ); // invoke-virtual {v0, p1}, Lcom/google/gson/TypeAdapter;->read(Lcom/google/gson/stream/JsonReader;)Ljava/lang/Object;
/* .line 131 */
/* .local v0, "fieldValue":Ljava/lang/Object; */
/* if-nez v0, :cond_0 */
/* iget-boolean v1, p0, Lcom/google/gson/internal/bind/ReflectiveTypeAdapterFactory$1;->val$isPrimitive:Z */
/* if-nez v1, :cond_1 */
/* .line 132 */
} // :cond_0
v1 = this.val$field;
(( java.lang.reflect.Field ) v1 ).set ( p2, v0 ); // invoke-virtual {v1, p2, v0}, Ljava/lang/reflect/Field;->set(Ljava/lang/Object;Ljava/lang/Object;)V
/* .line 134 */
} // :cond_1
return;
} // .end method
void write ( com.google.gson.stream.JsonWriter p0, java.lang.Object p1 ) {
/* .locals 5 */
/* .param p1, "writer" # Lcom/google/gson/stream/JsonWriter; */
/* .param p2, "value" # Ljava/lang/Object; */
/* .annotation system Ldalvik/annotation/Throws; */
/* value = { */
/* Ljava/io/IOException;, */
/* Ljava/lang/IllegalAccessException; */
/* } */
} // .end annotation
/* .line 123 */
v0 = this.val$field;
(( java.lang.reflect.Field ) v0 ).get ( p2 ); // invoke-virtual {v0, p2}, Ljava/lang/reflect/Field;->get(Ljava/lang/Object;)Ljava/lang/Object;
/* .line 124 */
/* .local v0, "fieldValue":Ljava/lang/Object; */
/* iget-boolean v1, p0, Lcom/google/gson/internal/bind/ReflectiveTypeAdapterFactory$1;->val$jsonAdapterPresent:Z */
if ( v1 != null) { // if-eqz v1, :cond_0
v1 = this.val$typeAdapter;
/* .line 125 */
} // :cond_0
/* new-instance v1, Lcom/google/gson/internal/bind/TypeAdapterRuntimeTypeWrapper; */
v2 = this.val$context;
v3 = this.val$typeAdapter;
v4 = this.val$fieldType;
(( com.google.gson.reflect.TypeToken ) v4 ).getType ( ); // invoke-virtual {v4}, Lcom/google/gson/reflect/TypeToken;->getType()Ljava/lang/reflect/Type;
/* invoke-direct {v1, v2, v3, v4}, Lcom/google/gson/internal/bind/TypeAdapterRuntimeTypeWrapper;-><init>(Lcom/google/gson/Gson;Lcom/google/gson/TypeAdapter;Ljava/lang/reflect/Type;)V */
} // :goto_0
/* nop */
/* .line 126 */
/* .local v1, "t":Lcom/google/gson/TypeAdapter; */
(( com.google.gson.TypeAdapter ) v1 ).write ( p1, v0 ); // invoke-virtual {v1, p1, v0}, Lcom/google/gson/TypeAdapter;->write(Lcom/google/gson/stream/JsonWriter;Ljava/lang/Object;)V
/* .line 127 */
return;
} // .end method
public Boolean writeField ( java.lang.Object p0 ) {
/* .locals 2 */
/* .param p1, "value" # Ljava/lang/Object; */
/* .annotation system Ldalvik/annotation/Throws; */
/* value = { */
/* Ljava/io/IOException;, */
/* Ljava/lang/IllegalAccessException; */
/* } */
} // .end annotation
/* .line 136 */
/* iget-boolean v0, p0, Lcom/google/gson/internal/bind/ReflectiveTypeAdapterFactory$1;->serialized:Z */
int v1 = 0; // const/4 v1, 0x0
/* if-nez v0, :cond_0 */
/* .line 137 */
} // :cond_0
v0 = this.val$field;
(( java.lang.reflect.Field ) v0 ).get ( p1 ); // invoke-virtual {v0, p1}, Ljava/lang/reflect/Field;->get(Ljava/lang/Object;)Ljava/lang/Object;
/* .line 138 */
/* .local v0, "fieldValue":Ljava/lang/Object; */
/* if-eq v0, p1, :cond_1 */
int v1 = 1; // const/4 v1, 0x1
} // :cond_1
} // .end method
