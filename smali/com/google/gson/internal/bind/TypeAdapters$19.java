class com.google.gson.internal.bind.TypeAdapters$19 extends com.google.gson.TypeAdapter {
	 /* .source "TypeAdapters.java" */
	 /* # annotations */
	 /* .annotation system Ldalvik/annotation/EnclosingClass; */
	 /* value = Lcom/google/gson/internal/bind/TypeAdapters; */
} // .end annotation
/* .annotation system Ldalvik/annotation/InnerClass; */
/* accessFlags = 0x0 */
/* name = null */
} // .end annotation
/* .annotation system Ldalvik/annotation/Signature; */
/* value = { */
/* "Lcom/google/gson/TypeAdapter<", */
/* "Ljava/lang/StringBuilder;", */
/* ">;" */
/* } */
} // .end annotation
/* # direct methods */
 com.google.gson.internal.bind.TypeAdapters$19 ( ) {
/* .locals 0 */
/* .line 458 */
/* invoke-direct {p0}, Lcom/google/gson/TypeAdapter;-><init>()V */
return;
} // .end method
/* # virtual methods */
public java.lang.Object read ( com.google.gson.stream.JsonReader p0 ) { //bridge//synthethic
/* .locals 0 */
/* .annotation system Ldalvik/annotation/Throws; */
/* value = { */
/* Ljava/io/IOException; */
/* } */
} // .end annotation
/* .line 458 */
(( com.google.gson.internal.bind.TypeAdapters$19 ) p0 ).read ( p1 ); // invoke-virtual {p0, p1}, Lcom/google/gson/internal/bind/TypeAdapters$19;->read(Lcom/google/gson/stream/JsonReader;)Ljava/lang/StringBuilder;
} // .end method
public java.lang.StringBuilder read ( com.google.gson.stream.JsonReader p0 ) {
/* .locals 2 */
/* .param p1, "in" # Lcom/google/gson/stream/JsonReader; */
/* .annotation system Ldalvik/annotation/Throws; */
/* value = { */
/* Ljava/io/IOException; */
/* } */
} // .end annotation
/* .line 461 */
(( com.google.gson.stream.JsonReader ) p1 ).peek ( ); // invoke-virtual {p1}, Lcom/google/gson/stream/JsonReader;->peek()Lcom/google/gson/stream/JsonToken;
v1 = com.google.gson.stream.JsonToken.NULL;
/* if-ne v0, v1, :cond_0 */
/* .line 462 */
(( com.google.gson.stream.JsonReader ) p1 ).nextNull ( ); // invoke-virtual {p1}, Lcom/google/gson/stream/JsonReader;->nextNull()V
/* .line 463 */
int v0 = 0; // const/4 v0, 0x0
/* .line 465 */
} // :cond_0
/* new-instance v0, Ljava/lang/StringBuilder; */
(( com.google.gson.stream.JsonReader ) p1 ).nextString ( ); // invoke-virtual {p1}, Lcom/google/gson/stream/JsonReader;->nextString()Ljava/lang/String;
/* invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V */
} // .end method
public void write ( com.google.gson.stream.JsonWriter p0, java.lang.Object p1 ) { //bridge//synthethic
/* .locals 0 */
/* .annotation system Ldalvik/annotation/Throws; */
/* value = { */
/* Ljava/io/IOException; */
/* } */
} // .end annotation
/* .line 458 */
/* check-cast p2, Ljava/lang/StringBuilder; */
(( com.google.gson.internal.bind.TypeAdapters$19 ) p0 ).write ( p1, p2 ); // invoke-virtual {p0, p1, p2}, Lcom/google/gson/internal/bind/TypeAdapters$19;->write(Lcom/google/gson/stream/JsonWriter;Ljava/lang/StringBuilder;)V
return;
} // .end method
public void write ( com.google.gson.stream.JsonWriter p0, java.lang.StringBuilder p1 ) {
/* .locals 1 */
/* .param p1, "out" # Lcom/google/gson/stream/JsonWriter; */
/* .param p2, "value" # Ljava/lang/StringBuilder; */
/* .annotation system Ldalvik/annotation/Throws; */
/* value = { */
/* Ljava/io/IOException; */
/* } */
} // .end annotation
/* .line 469 */
/* if-nez p2, :cond_0 */
int v0 = 0; // const/4 v0, 0x0
} // :cond_0
(( java.lang.StringBuilder ) p2 ).toString ( ); // invoke-virtual {p2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
} // :goto_0
(( com.google.gson.stream.JsonWriter ) p1 ).value ( v0 ); // invoke-virtual {p1, v0}, Lcom/google/gson/stream/JsonWriter;->value(Ljava/lang/String;)Lcom/google/gson/stream/JsonWriter;
/* .line 470 */
return;
} // .end method
