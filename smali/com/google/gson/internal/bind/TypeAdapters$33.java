class com.google.gson.internal.bind.TypeAdapters$33 implements com.google.gson.TypeAdapterFactory {
	 /* .source "TypeAdapters.java" */
	 /* # interfaces */
	 /* # annotations */
	 /* .annotation system Ldalvik/annotation/EnclosingMethod; */
	 /* value = Lcom/google/gson/internal/bind/TypeAdapters;->newFactoryForMultipleTypes(Ljava/lang/Class;Ljava/lang/Class;Lcom/google/gson/TypeAdapter;)Lcom/google/gson/TypeAdapterFactory; */
} // .end annotation
/* .annotation system Ldalvik/annotation/InnerClass; */
/* accessFlags = 0x0 */
/* name = null */
} // .end annotation
/* # instance fields */
final java.lang.Class val$base; //synthetic
final java.lang.Class val$sub; //synthetic
final com.google.gson.TypeAdapter val$typeAdapter; //synthetic
/* # direct methods */
 com.google.gson.internal.bind.TypeAdapters$33 ( ) {
/* .locals 0 */
/* .line 885 */
this.val$base = p1;
this.val$sub = p2;
this.val$typeAdapter = p3;
/* invoke-direct {p0}, Ljava/lang/Object;-><init>()V */
return;
} // .end method
/* # virtual methods */
public com.google.gson.TypeAdapter create ( com.google.gson.Gson p0, com.google.gson.reflect.TypeToken p1 ) {
/* .locals 2 */
/* .param p1, "gson" # Lcom/google/gson/Gson; */
/* .annotation system Ldalvik/annotation/Signature; */
/* value = { */
/* "<T:", */
/* "Ljava/lang/Object;", */
/* ">(", */
/* "Lcom/google/gson/Gson;", */
/* "Lcom/google/gson/reflect/TypeToken<", */
/* "TT;>;)", */
/* "Lcom/google/gson/TypeAdapter<", */
/* "TT;>;" */
/* } */
} // .end annotation
/* .line 888 */
/* .local p2, "typeToken":Lcom/google/gson/reflect/TypeToken;, "Lcom/google/gson/reflect/TypeToken<TT;>;" */
(( com.google.gson.reflect.TypeToken ) p2 ).getRawType ( ); // invoke-virtual {p2}, Lcom/google/gson/reflect/TypeToken;->getRawType()Ljava/lang/Class;
/* .line 889 */
/* .local v0, "rawType":Ljava/lang/Class;, "Ljava/lang/Class<-TT;>;" */
v1 = this.val$base;
/* if-eq v0, v1, :cond_1 */
v1 = this.val$sub;
/* if-ne v0, v1, :cond_0 */
} // :cond_0
int v1 = 0; // const/4 v1, 0x0
} // :cond_1
} // :goto_0
v1 = this.val$typeAdapter;
} // :goto_1
} // .end method
public java.lang.String toString ( ) {
/* .locals 2 */
/* .line 892 */
/* new-instance v0, Ljava/lang/StringBuilder; */
/* invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V */
final String v1 = "Factory[type="; // const-string v1, "Factory[type="
(( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
v1 = this.val$base;
(( java.lang.Class ) v1 ).getName ( ); // invoke-virtual {v1}, Ljava/lang/Class;->getName()Ljava/lang/String;
(( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
final String v1 = "+"; // const-string v1, "+"
(( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
v1 = this.val$sub;
/* .line 893 */
(( java.lang.Class ) v1 ).getName ( ); // invoke-virtual {v1}, Ljava/lang/Class;->getName()Ljava/lang/String;
(( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
final String v1 = ",adapter="; // const-string v1, ",adapter="
(( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
v1 = this.val$typeAdapter;
(( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;
final String v1 = "]"; // const-string v1, "]"
(( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v0 ).toString ( ); // invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
/* .line 892 */
} // .end method
