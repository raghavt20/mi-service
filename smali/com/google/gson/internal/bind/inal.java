public class inal implements com.google.gson.TypeAdapterFactory {
	 /* .source "CollectionTypeAdapterFactory.java" */
	 /* # interfaces */
	 /* # annotations */
	 /* .annotation system Ldalvik/annotation/MemberClasses; */
	 /* value = { */
	 /* Lcom/google/gson/internal/bind/CollectionTypeAdapterFactory$Adapter; */
	 /* } */
} // .end annotation
/* # instance fields */
private final com.google.gson.internal.ConstructorConstructor constructorConstructor;
/* # direct methods */
public inal ( ) {
	 /* .locals 0 */
	 /* .param p1, "constructorConstructor" # Lcom/google/gson/internal/ConstructorConstructor; */
	 /* .line 39 */
	 /* invoke-direct {p0}, Ljava/lang/Object;-><init>()V */
	 /* .line 40 */
	 this.constructorConstructor = p1;
	 /* .line 41 */
	 return;
} // .end method
/* # virtual methods */
public com.google.gson.TypeAdapter create ( com.google.gson.Gson p0, com.google.gson.reflect.TypeToken p1 ) {
	 /* .locals 6 */
	 /* .param p1, "gson" # Lcom/google/gson/Gson; */
	 /* .annotation system Ldalvik/annotation/Signature; */
	 /* value = { */
	 /* "<T:", */
	 /* "Ljava/lang/Object;", */
	 /* ">(", */
	 /* "Lcom/google/gson/Gson;", */
	 /* "Lcom/google/gson/reflect/TypeToken<", */
	 /* "TT;>;)", */
	 /* "Lcom/google/gson/TypeAdapter<", */
	 /* "TT;>;" */
	 /* } */
} // .end annotation
/* .line 45 */
/* .local p2, "typeToken":Lcom/google/gson/reflect/TypeToken;, "Lcom/google/gson/reflect/TypeToken<TT;>;" */
(( com.google.gson.reflect.TypeToken ) p2 ).getType ( ); // invoke-virtual {p2}, Lcom/google/gson/reflect/TypeToken;->getType()Ljava/lang/reflect/Type;
/* .line 47 */
/* .local v0, "type":Ljava/lang/reflect/Type; */
(( com.google.gson.reflect.TypeToken ) p2 ).getRawType ( ); // invoke-virtual {p2}, Lcom/google/gson/reflect/TypeToken;->getRawType()Ljava/lang/Class;
/* .line 48 */
/* .local v1, "rawType":Ljava/lang/Class;, "Ljava/lang/Class<-TT;>;" */
/* const-class v2, Ljava/util/Collection; */
v2 = (( java.lang.Class ) v2 ).isAssignableFrom ( v1 ); // invoke-virtual {v2, v1}, Ljava/lang/Class;->isAssignableFrom(Ljava/lang/Class;)Z
/* if-nez v2, :cond_0 */
/* .line 49 */
int v2 = 0; // const/4 v2, 0x0
/* .line 52 */
} // :cond_0
com.google.gson.internal.$Gson$Types .getCollectionElementType ( v0,v1 );
/* .line 53 */
/* .local v2, "elementType":Ljava/lang/reflect/Type; */
com.google.gson.reflect.TypeToken .get ( v2 );
(( com.google.gson.Gson ) p1 ).getAdapter ( v3 ); // invoke-virtual {p1, v3}, Lcom/google/gson/Gson;->getAdapter(Lcom/google/gson/reflect/TypeToken;)Lcom/google/gson/TypeAdapter;
/* .line 54 */
/* .local v3, "elementTypeAdapter":Lcom/google/gson/TypeAdapter;, "Lcom/google/gson/TypeAdapter<*>;" */
v4 = this.constructorConstructor;
(( com.google.gson.internal.ConstructorConstructor ) v4 ).get ( p2 ); // invoke-virtual {v4, p2}, Lcom/google/gson/internal/ConstructorConstructor;->get(Lcom/google/gson/reflect/TypeToken;)Lcom/google/gson/internal/ObjectConstructor;
/* .line 57 */
/* .local v4, "constructor":Lcom/google/gson/internal/ObjectConstructor;, "Lcom/google/gson/internal/ObjectConstructor<TT;>;" */
/* new-instance v5, Lcom/google/gson/internal/bind/CollectionTypeAdapterFactory$Adapter; */
/* invoke-direct {v5, p1, v2, v3, v4}, Lcom/google/gson/internal/bind/CollectionTypeAdapterFactory$Adapter;-><init>(Lcom/google/gson/Gson;Ljava/lang/reflect/Type;Lcom/google/gson/TypeAdapter;Lcom/google/gson/internal/ObjectConstructor;)V */
/* .line 58 */
/* .local v5, "result":Lcom/google/gson/TypeAdapter;, "Lcom/google/gson/TypeAdapter<TT;>;" */
} // .end method
