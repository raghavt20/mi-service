class com.google.gson.internal.bind.TypeAdapters$EnumTypeAdapter$1 implements java.security.PrivilegedAction {
	 /* .source "TypeAdapters.java" */
	 /* # interfaces */
	 /* # annotations */
	 /* .annotation system Ldalvik/annotation/EnclosingMethod; */
	 /* value = Lcom/google/gson/internal/bind/TypeAdapters$EnumTypeAdapter;-><init>(Ljava/lang/Class;)V */
} // .end annotation
/* .annotation system Ldalvik/annotation/InnerClass; */
/* accessFlags = 0x0 */
/* name = null */
} // .end annotation
/* .annotation system Ldalvik/annotation/Signature; */
/* value = { */
/* "Ljava/lang/Object;", */
/* "Ljava/security/PrivilegedAction<", */
/* "[", */
/* "Ljava/lang/reflect/Field;", */
/* ">;" */
/* } */
} // .end annotation
/* # instance fields */
final com.google.gson.internal.bind.TypeAdapters$EnumTypeAdapter this$0; //synthetic
final java.lang.Class val$classOfT; //synthetic
/* # direct methods */
 com.google.gson.internal.bind.TypeAdapters$EnumTypeAdapter$1 ( ) {
/* .locals 0 */
/* .param p1, "this$0" # Lcom/google/gson/internal/bind/TypeAdapters$EnumTypeAdapter; */
/* .line 785 */
/* .local p0, "this":Lcom/google/gson/internal/bind/TypeAdapters$EnumTypeAdapter$1;, "Lcom/google/gson/internal/bind/TypeAdapters$EnumTypeAdapter$1;" */
this.this$0 = p1;
this.val$classOfT = p2;
/* invoke-direct {p0}, Ljava/lang/Object;-><init>()V */
return;
} // .end method
/* # virtual methods */
public java.lang.Object run ( ) { //bridge//synthethic
/* .locals 1 */
/* .line 785 */
/* .local p0, "this":Lcom/google/gson/internal/bind/TypeAdapters$EnumTypeAdapter$1;, "Lcom/google/gson/internal/bind/TypeAdapters$EnumTypeAdapter$1;" */
(( com.google.gson.internal.bind.TypeAdapters$EnumTypeAdapter$1 ) p0 ).run ( ); // invoke-virtual {p0}, Lcom/google/gson/internal/bind/TypeAdapters$EnumTypeAdapter$1;->run()[Ljava/lang/reflect/Field;
} // .end method
public java.lang.reflect.Field run ( ) {
/* .locals 7 */
/* .line 787 */
/* .local p0, "this":Lcom/google/gson/internal/bind/TypeAdapters$EnumTypeAdapter$1;, "Lcom/google/gson/internal/bind/TypeAdapters$EnumTypeAdapter$1;" */
v0 = this.val$classOfT;
(( java.lang.Class ) v0 ).getDeclaredFields ( ); // invoke-virtual {v0}, Ljava/lang/Class;->getDeclaredFields()[Ljava/lang/reflect/Field;
/* .line 788 */
/* .local v0, "fields":[Ljava/lang/reflect/Field; */
/* new-instance v1, Ljava/util/ArrayList; */
/* array-length v2, v0 */
/* invoke-direct {v1, v2}, Ljava/util/ArrayList;-><init>(I)V */
/* .line 789 */
/* .local v1, "constantFieldsList":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/reflect/Field;>;" */
/* array-length v2, v0 */
int v3 = 0; // const/4 v3, 0x0
/* move v4, v3 */
} // :goto_0
/* if-ge v4, v2, :cond_1 */
/* aget-object v5, v0, v4 */
/* .line 790 */
/* .local v5, "f":Ljava/lang/reflect/Field; */
v6 = (( java.lang.reflect.Field ) v5 ).isEnumConstant ( ); // invoke-virtual {v5}, Ljava/lang/reflect/Field;->isEnumConstant()Z
if ( v6 != null) { // if-eqz v6, :cond_0
/* .line 791 */
(( java.util.ArrayList ) v1 ).add ( v5 ); // invoke-virtual {v1, v5}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z
/* .line 789 */
} // .end local v5 # "f":Ljava/lang/reflect/Field;
} // :cond_0
/* add-int/lit8 v4, v4, 0x1 */
/* .line 795 */
} // :cond_1
/* new-array v2, v3, [Ljava/lang/reflect/Field; */
(( java.util.ArrayList ) v1 ).toArray ( v2 ); // invoke-virtual {v1, v2}, Ljava/util/ArrayList;->toArray([Ljava/lang/Object;)[Ljava/lang/Object;
/* check-cast v2, [Ljava/lang/reflect/Field; */
/* .line 796 */
/* .local v2, "constantFields":[Ljava/lang/reflect/Field; */
int v3 = 1; // const/4 v3, 0x1
java.lang.reflect.AccessibleObject .setAccessible ( v2,v3 );
/* .line 797 */
} // .end method
