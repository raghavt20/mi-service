class com.google.gson.internal.bind.TypeAdapters$2 extends com.google.gson.TypeAdapter {
	 /* .source "TypeAdapters.java" */
	 /* # annotations */
	 /* .annotation system Ldalvik/annotation/EnclosingClass; */
	 /* value = Lcom/google/gson/internal/bind/TypeAdapters; */
} // .end annotation
/* .annotation system Ldalvik/annotation/InnerClass; */
/* accessFlags = 0x0 */
/* name = null */
} // .end annotation
/* .annotation system Ldalvik/annotation/Signature; */
/* value = { */
/* "Lcom/google/gson/TypeAdapter<", */
/* "Ljava/util/BitSet;", */
/* ">;" */
/* } */
} // .end annotation
/* # direct methods */
 com.google.gson.internal.bind.TypeAdapters$2 ( ) {
/* .locals 0 */
/* .line 86 */
/* invoke-direct {p0}, Lcom/google/gson/TypeAdapter;-><init>()V */
return;
} // .end method
/* # virtual methods */
public java.lang.Object read ( com.google.gson.stream.JsonReader p0 ) { //bridge//synthethic
/* .locals 0 */
/* .annotation system Ldalvik/annotation/Throws; */
/* value = { */
/* Ljava/io/IOException; */
/* } */
} // .end annotation
/* .line 86 */
(( com.google.gson.internal.bind.TypeAdapters$2 ) p0 ).read ( p1 ); // invoke-virtual {p0, p1}, Lcom/google/gson/internal/bind/TypeAdapters$2;->read(Lcom/google/gson/stream/JsonReader;)Ljava/util/BitSet;
} // .end method
public java.util.BitSet read ( com.google.gson.stream.JsonReader p0 ) {
/* .locals 7 */
/* .param p1, "in" # Lcom/google/gson/stream/JsonReader; */
/* .annotation system Ldalvik/annotation/Throws; */
/* value = { */
/* Ljava/io/IOException; */
/* } */
} // .end annotation
/* .line 88 */
/* new-instance v0, Ljava/util/BitSet; */
/* invoke-direct {v0}, Ljava/util/BitSet;-><init>()V */
/* .line 89 */
/* .local v0, "bitset":Ljava/util/BitSet; */
(( com.google.gson.stream.JsonReader ) p1 ).beginArray ( ); // invoke-virtual {p1}, Lcom/google/gson/stream/JsonReader;->beginArray()V
/* .line 90 */
int v1 = 0; // const/4 v1, 0x0
/* .line 91 */
/* .local v1, "i":I */
(( com.google.gson.stream.JsonReader ) p1 ).peek ( ); // invoke-virtual {p1}, Lcom/google/gson/stream/JsonReader;->peek()Lcom/google/gson/stream/JsonToken;
/* .line 92 */
/* .local v2, "tokenType":Lcom/google/gson/stream/JsonToken; */
} // :goto_0
v3 = com.google.gson.stream.JsonToken.END_ARRAY;
/* if-eq v2, v3, :cond_3 */
/* .line 94 */
v3 = com.google.gson.internal.bind.TypeAdapters$35.$SwitchMap$com$google$gson$stream$JsonToken;
v4 = (( com.google.gson.stream.JsonToken ) v2 ).ordinal ( ); // invoke-virtual {v2}, Lcom/google/gson/stream/JsonToken;->ordinal()I
/* aget v3, v3, v4 */
/* packed-switch v3, :pswitch_data_0 */
/* .line 110 */
/* new-instance v3, Lcom/google/gson/JsonSyntaxException; */
/* new-instance v4, Ljava/lang/StringBuilder; */
/* invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V */
final String v5 = "Invalid bitset value type: "; // const-string v5, "Invalid bitset value type: "
(( java.lang.StringBuilder ) v4 ).append ( v5 ); // invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v4 ).append ( v2 ); // invoke-virtual {v4, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;
final String v5 = "; at path "; // const-string v5, "; at path "
(( java.lang.StringBuilder ) v4 ).append ( v5 ); // invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( com.google.gson.stream.JsonReader ) p1 ).getPath ( ); // invoke-virtual {p1}, Lcom/google/gson/stream/JsonReader;->getPath()Ljava/lang/String;
(( java.lang.StringBuilder ) v4 ).append ( v5 ); // invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v4 ).toString ( ); // invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
/* invoke-direct {v3, v4}, Lcom/google/gson/JsonSyntaxException;-><init>(Ljava/lang/String;)V */
/* throw v3 */
/* .line 107 */
/* :pswitch_0 */
v3 = (( com.google.gson.stream.JsonReader ) p1 ).nextBoolean ( ); // invoke-virtual {p1}, Lcom/google/gson/stream/JsonReader;->nextBoolean()Z
/* .line 108 */
/* .local v3, "set":Z */
/* .line 97 */
} // .end local v3 # "set":Z
/* :pswitch_1 */
v3 = (( com.google.gson.stream.JsonReader ) p1 ).nextInt ( ); // invoke-virtual {p1}, Lcom/google/gson/stream/JsonReader;->nextInt()I
/* .line 98 */
/* .local v3, "intValue":I */
/* if-nez v3, :cond_0 */
/* .line 99 */
int v4 = 0; // const/4 v4, 0x0
/* move v3, v4 */
/* .local v4, "set":Z */
/* .line 100 */
} // .end local v4 # "set":Z
} // :cond_0
int v4 = 1; // const/4 v4, 0x1
/* if-ne v3, v4, :cond_2 */
/* .line 101 */
int v4 = 1; // const/4 v4, 0x1
/* move v3, v4 */
/* .line 112 */
/* .local v3, "set":Z */
} // :goto_1
if ( v3 != null) { // if-eqz v3, :cond_1
/* .line 113 */
(( java.util.BitSet ) v0 ).set ( v1 ); // invoke-virtual {v0, v1}, Ljava/util/BitSet;->set(I)V
/* .line 115 */
} // :cond_1
/* add-int/lit8 v1, v1, 0x1 */
/* .line 116 */
(( com.google.gson.stream.JsonReader ) p1 ).peek ( ); // invoke-virtual {p1}, Lcom/google/gson/stream/JsonReader;->peek()Lcom/google/gson/stream/JsonToken;
/* .line 117 */
} // .end local v3 # "set":Z
/* .line 103 */
/* .local v3, "intValue":I */
} // :cond_2
/* new-instance v4, Lcom/google/gson/JsonSyntaxException; */
/* new-instance v5, Ljava/lang/StringBuilder; */
/* invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V */
final String v6 = "Invalid bitset value "; // const-string v6, "Invalid bitset value "
(( java.lang.StringBuilder ) v5 ).append ( v6 ); // invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v5 ).append ( v3 ); // invoke-virtual {v5, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;
final String v6 = ", expected 0 or 1; at path "; // const-string v6, ", expected 0 or 1; at path "
(( java.lang.StringBuilder ) v5 ).append ( v6 ); // invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( com.google.gson.stream.JsonReader ) p1 ).getPreviousPath ( ); // invoke-virtual {p1}, Lcom/google/gson/stream/JsonReader;->getPreviousPath()Ljava/lang/String;
(( java.lang.StringBuilder ) v5 ).append ( v6 ); // invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v5 ).toString ( ); // invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
/* invoke-direct {v4, v5}, Lcom/google/gson/JsonSyntaxException;-><init>(Ljava/lang/String;)V */
/* throw v4 */
/* .line 118 */
} // .end local v3 # "intValue":I
} // :cond_3
(( com.google.gson.stream.JsonReader ) p1 ).endArray ( ); // invoke-virtual {p1}, Lcom/google/gson/stream/JsonReader;->endArray()V
/* .line 119 */
/* nop */
/* :pswitch_data_0 */
/* .packed-switch 0x1 */
/* :pswitch_1 */
/* :pswitch_1 */
/* :pswitch_0 */
} // .end packed-switch
} // .end method
public void write ( com.google.gson.stream.JsonWriter p0, java.lang.Object p1 ) { //bridge//synthethic
/* .locals 0 */
/* .annotation system Ldalvik/annotation/Throws; */
/* value = { */
/* Ljava/io/IOException; */
/* } */
} // .end annotation
/* .line 86 */
/* check-cast p2, Ljava/util/BitSet; */
(( com.google.gson.internal.bind.TypeAdapters$2 ) p0 ).write ( p1, p2 ); // invoke-virtual {p0, p1, p2}, Lcom/google/gson/internal/bind/TypeAdapters$2;->write(Lcom/google/gson/stream/JsonWriter;Ljava/util/BitSet;)V
return;
} // .end method
public void write ( com.google.gson.stream.JsonWriter p0, java.util.BitSet p1 ) {
/* .locals 5 */
/* .param p1, "out" # Lcom/google/gson/stream/JsonWriter; */
/* .param p2, "src" # Ljava/util/BitSet; */
/* .annotation system Ldalvik/annotation/Throws; */
/* value = { */
/* Ljava/io/IOException; */
/* } */
} // .end annotation
/* .line 123 */
(( com.google.gson.stream.JsonWriter ) p1 ).beginArray ( ); // invoke-virtual {p1}, Lcom/google/gson/stream/JsonWriter;->beginArray()Lcom/google/gson/stream/JsonWriter;
/* .line 124 */
int v0 = 0; // const/4 v0, 0x0
/* .local v0, "i":I */
v1 = (( java.util.BitSet ) p2 ).length ( ); // invoke-virtual {p2}, Ljava/util/BitSet;->length()I
/* .local v1, "length":I */
} // :goto_0
/* if-ge v0, v1, :cond_0 */
/* .line 125 */
v2 = (( java.util.BitSet ) p2 ).get ( v0 ); // invoke-virtual {p2, v0}, Ljava/util/BitSet;->get(I)Z
/* .line 126 */
/* .local v2, "value":I */
/* int-to-long v3, v2 */
(( com.google.gson.stream.JsonWriter ) p1 ).value ( v3, v4 ); // invoke-virtual {p1, v3, v4}, Lcom/google/gson/stream/JsonWriter;->value(J)Lcom/google/gson/stream/JsonWriter;
/* .line 124 */
} // .end local v2 # "value":I
/* add-int/lit8 v0, v0, 0x1 */
/* .line 128 */
} // .end local v0 # "i":I
} // .end local v1 # "length":I
} // :cond_0
(( com.google.gson.stream.JsonWriter ) p1 ).endArray ( ); // invoke-virtual {p1}, Lcom/google/gson/stream/JsonWriter;->endArray()Lcom/google/gson/stream/JsonWriter;
/* .line 129 */
return;
} // .end method
