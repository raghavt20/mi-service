class com.google.gson.internal.bind.TypeAdapters$30 implements com.google.gson.TypeAdapterFactory {
	 /* .source "TypeAdapters.java" */
	 /* # interfaces */
	 /* # annotations */
	 /* .annotation system Ldalvik/annotation/EnclosingMethod; */
	 /* value = Lcom/google/gson/internal/bind/TypeAdapters;->newFactory(Lcom/google/gson/reflect/TypeToken;Lcom/google/gson/TypeAdapter;)Lcom/google/gson/TypeAdapterFactory; */
} // .end annotation
/* .annotation system Ldalvik/annotation/InnerClass; */
/* accessFlags = 0x0 */
/* name = null */
} // .end annotation
/* # instance fields */
final com.google.gson.reflect.TypeToken val$type; //synthetic
final com.google.gson.TypeAdapter val$typeAdapter; //synthetic
/* # direct methods */
 com.google.gson.internal.bind.TypeAdapters$30 ( ) {
/* .locals 0 */
/* .line 847 */
this.val$type = p1;
this.val$typeAdapter = p2;
/* invoke-direct {p0}, Ljava/lang/Object;-><init>()V */
return;
} // .end method
/* # virtual methods */
public com.google.gson.TypeAdapter create ( com.google.gson.Gson p0, com.google.gson.reflect.TypeToken p1 ) {
/* .locals 1 */
/* .param p1, "gson" # Lcom/google/gson/Gson; */
/* .annotation system Ldalvik/annotation/Signature; */
/* value = { */
/* "<T:", */
/* "Ljava/lang/Object;", */
/* ">(", */
/* "Lcom/google/gson/Gson;", */
/* "Lcom/google/gson/reflect/TypeToken<", */
/* "TT;>;)", */
/* "Lcom/google/gson/TypeAdapter<", */
/* "TT;>;" */
/* } */
} // .end annotation
/* .line 850 */
/* .local p2, "typeToken":Lcom/google/gson/reflect/TypeToken;, "Lcom/google/gson/reflect/TypeToken<TT;>;" */
v0 = this.val$type;
v0 = (( com.google.gson.reflect.TypeToken ) p2 ).equals ( v0 ); // invoke-virtual {p2, v0}, Lcom/google/gson/reflect/TypeToken;->equals(Ljava/lang/Object;)Z
if ( v0 != null) { // if-eqz v0, :cond_0
v0 = this.val$typeAdapter;
} // :cond_0
int v0 = 0; // const/4 v0, 0x0
} // :goto_0
} // .end method
