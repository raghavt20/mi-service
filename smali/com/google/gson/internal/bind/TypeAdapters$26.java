class com.google.gson.internal.bind.TypeAdapters$26 extends com.google.gson.TypeAdapter {
	 /* .source "TypeAdapters.java" */
	 /* # annotations */
	 /* .annotation system Ldalvik/annotation/EnclosingClass; */
	 /* value = Lcom/google/gson/internal/bind/TypeAdapters; */
} // .end annotation
/* .annotation system Ldalvik/annotation/InnerClass; */
/* accessFlags = 0x0 */
/* name = null */
} // .end annotation
/* .annotation system Ldalvik/annotation/Signature; */
/* value = { */
/* "Lcom/google/gson/TypeAdapter<", */
/* "Ljava/util/Calendar;", */
/* ">;" */
/* } */
} // .end annotation
/* # static fields */
private static final java.lang.String DAY_OF_MONTH;
private static final java.lang.String HOUR_OF_DAY;
private static final java.lang.String MINUTE;
private static final java.lang.String MONTH;
private static final java.lang.String SECOND;
private static final java.lang.String YEAR;
/* # direct methods */
 com.google.gson.internal.bind.TypeAdapters$26 ( ) {
/* .locals 0 */
/* .line 592 */
/* invoke-direct {p0}, Lcom/google/gson/TypeAdapter;-><init>()V */
return;
} // .end method
/* # virtual methods */
public java.lang.Object read ( com.google.gson.stream.JsonReader p0 ) { //bridge//synthethic
/* .locals 0 */
/* .annotation system Ldalvik/annotation/Throws; */
/* value = { */
/* Ljava/io/IOException; */
/* } */
} // .end annotation
/* .line 592 */
(( com.google.gson.internal.bind.TypeAdapters$26 ) p0 ).read ( p1 ); // invoke-virtual {p0, p1}, Lcom/google/gson/internal/bind/TypeAdapters$26;->read(Lcom/google/gson/stream/JsonReader;)Ljava/util/Calendar;
} // .end method
public java.util.Calendar read ( com.google.gson.stream.JsonReader p0 ) {
/* .locals 14 */
/* .param p1, "in" # Lcom/google/gson/stream/JsonReader; */
/* .annotation system Ldalvik/annotation/Throws; */
/* value = { */
/* Ljava/io/IOException; */
/* } */
} // .end annotation
/* .line 602 */
(( com.google.gson.stream.JsonReader ) p1 ).peek ( ); // invoke-virtual {p1}, Lcom/google/gson/stream/JsonReader;->peek()Lcom/google/gson/stream/JsonToken;
v1 = com.google.gson.stream.JsonToken.NULL;
/* if-ne v0, v1, :cond_0 */
/* .line 603 */
(( com.google.gson.stream.JsonReader ) p1 ).nextNull ( ); // invoke-virtual {p1}, Lcom/google/gson/stream/JsonReader;->nextNull()V
/* .line 604 */
int v0 = 0; // const/4 v0, 0x0
/* .line 606 */
} // :cond_0
(( com.google.gson.stream.JsonReader ) p1 ).beginObject ( ); // invoke-virtual {p1}, Lcom/google/gson/stream/JsonReader;->beginObject()V
/* .line 607 */
int v0 = 0; // const/4 v0, 0x0
/* .line 608 */
/* .local v0, "year":I */
int v1 = 0; // const/4 v1, 0x0
/* .line 609 */
/* .local v1, "month":I */
int v2 = 0; // const/4 v2, 0x0
/* .line 610 */
/* .local v2, "dayOfMonth":I */
int v3 = 0; // const/4 v3, 0x0
/* .line 611 */
/* .local v3, "hourOfDay":I */
int v4 = 0; // const/4 v4, 0x0
/* .line 612 */
/* .local v4, "minute":I */
int v5 = 0; // const/4 v5, 0x0
/* .line 613 */
/* .local v5, "second":I */
} // :goto_0
(( com.google.gson.stream.JsonReader ) p1 ).peek ( ); // invoke-virtual {p1}, Lcom/google/gson/stream/JsonReader;->peek()Lcom/google/gson/stream/JsonToken;
v7 = com.google.gson.stream.JsonToken.END_OBJECT;
/* if-eq v6, v7, :cond_7 */
/* .line 614 */
(( com.google.gson.stream.JsonReader ) p1 ).nextName ( ); // invoke-virtual {p1}, Lcom/google/gson/stream/JsonReader;->nextName()Ljava/lang/String;
/* .line 615 */
/* .local v6, "name":Ljava/lang/String; */
v7 = (( com.google.gson.stream.JsonReader ) p1 ).nextInt ( ); // invoke-virtual {p1}, Lcom/google/gson/stream/JsonReader;->nextInt()I
/* .line 616 */
/* .local v7, "value":I */
/* const-string/jumbo v8, "year" */
v8 = (( java.lang.String ) v8 ).equals ( v6 ); // invoke-virtual {v8, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z
if ( v8 != null) { // if-eqz v8, :cond_1
/* .line 617 */
/* move v0, v7 */
/* .line 618 */
} // :cond_1
final String v8 = "month"; // const-string v8, "month"
v8 = (( java.lang.String ) v8 ).equals ( v6 ); // invoke-virtual {v8, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z
if ( v8 != null) { // if-eqz v8, :cond_2
/* .line 619 */
/* move v1, v7 */
/* .line 620 */
} // :cond_2
final String v8 = "dayOfMonth"; // const-string v8, "dayOfMonth"
v8 = (( java.lang.String ) v8 ).equals ( v6 ); // invoke-virtual {v8, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z
if ( v8 != null) { // if-eqz v8, :cond_3
/* .line 621 */
/* move v2, v7 */
/* .line 622 */
} // :cond_3
final String v8 = "hourOfDay"; // const-string v8, "hourOfDay"
v8 = (( java.lang.String ) v8 ).equals ( v6 ); // invoke-virtual {v8, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z
if ( v8 != null) { // if-eqz v8, :cond_4
/* .line 623 */
/* move v3, v7 */
/* .line 624 */
} // :cond_4
final String v8 = "minute"; // const-string v8, "minute"
v8 = (( java.lang.String ) v8 ).equals ( v6 ); // invoke-virtual {v8, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z
if ( v8 != null) { // if-eqz v8, :cond_5
/* .line 625 */
/* move v4, v7 */
/* .line 626 */
} // :cond_5
/* const-string/jumbo v8, "second" */
v8 = (( java.lang.String ) v8 ).equals ( v6 ); // invoke-virtual {v8, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z
if ( v8 != null) { // if-eqz v8, :cond_6
/* .line 627 */
/* move v5, v7 */
/* .line 629 */
} // .end local v6 # "name":Ljava/lang/String;
} // .end local v7 # "value":I
} // :cond_6
} // :goto_1
/* .line 630 */
} // :cond_7
(( com.google.gson.stream.JsonReader ) p1 ).endObject ( ); // invoke-virtual {p1}, Lcom/google/gson/stream/JsonReader;->endObject()V
/* .line 631 */
/* new-instance v13, Ljava/util/GregorianCalendar; */
/* move-object v6, v13 */
/* move v7, v0 */
/* move v8, v1 */
/* move v9, v2 */
/* move v10, v3 */
/* move v11, v4 */
/* move v12, v5 */
/* invoke-direct/range {v6 ..v12}, Ljava/util/GregorianCalendar;-><init>(IIIIII)V */
} // .end method
public void write ( com.google.gson.stream.JsonWriter p0, java.lang.Object p1 ) { //bridge//synthethic
/* .locals 0 */
/* .annotation system Ldalvik/annotation/Throws; */
/* value = { */
/* Ljava/io/IOException; */
/* } */
} // .end annotation
/* .line 592 */
/* check-cast p2, Ljava/util/Calendar; */
(( com.google.gson.internal.bind.TypeAdapters$26 ) p0 ).write ( p1, p2 ); // invoke-virtual {p0, p1, p2}, Lcom/google/gson/internal/bind/TypeAdapters$26;->write(Lcom/google/gson/stream/JsonWriter;Ljava/util/Calendar;)V
return;
} // .end method
public void write ( com.google.gson.stream.JsonWriter p0, java.util.Calendar p1 ) {
/* .locals 2 */
/* .param p1, "out" # Lcom/google/gson/stream/JsonWriter; */
/* .param p2, "value" # Ljava/util/Calendar; */
/* .annotation system Ldalvik/annotation/Throws; */
/* value = { */
/* Ljava/io/IOException; */
/* } */
} // .end annotation
/* .line 636 */
/* if-nez p2, :cond_0 */
/* .line 637 */
(( com.google.gson.stream.JsonWriter ) p1 ).nullValue ( ); // invoke-virtual {p1}, Lcom/google/gson/stream/JsonWriter;->nullValue()Lcom/google/gson/stream/JsonWriter;
/* .line 638 */
return;
/* .line 640 */
} // :cond_0
(( com.google.gson.stream.JsonWriter ) p1 ).beginObject ( ); // invoke-virtual {p1}, Lcom/google/gson/stream/JsonWriter;->beginObject()Lcom/google/gson/stream/JsonWriter;
/* .line 641 */
/* const-string/jumbo v0, "year" */
(( com.google.gson.stream.JsonWriter ) p1 ).name ( v0 ); // invoke-virtual {p1, v0}, Lcom/google/gson/stream/JsonWriter;->name(Ljava/lang/String;)Lcom/google/gson/stream/JsonWriter;
/* .line 642 */
int v0 = 1; // const/4 v0, 0x1
v0 = (( java.util.Calendar ) p2 ).get ( v0 ); // invoke-virtual {p2, v0}, Ljava/util/Calendar;->get(I)I
/* int-to-long v0, v0 */
(( com.google.gson.stream.JsonWriter ) p1 ).value ( v0, v1 ); // invoke-virtual {p1, v0, v1}, Lcom/google/gson/stream/JsonWriter;->value(J)Lcom/google/gson/stream/JsonWriter;
/* .line 643 */
final String v0 = "month"; // const-string v0, "month"
(( com.google.gson.stream.JsonWriter ) p1 ).name ( v0 ); // invoke-virtual {p1, v0}, Lcom/google/gson/stream/JsonWriter;->name(Ljava/lang/String;)Lcom/google/gson/stream/JsonWriter;
/* .line 644 */
int v0 = 2; // const/4 v0, 0x2
v0 = (( java.util.Calendar ) p2 ).get ( v0 ); // invoke-virtual {p2, v0}, Ljava/util/Calendar;->get(I)I
/* int-to-long v0, v0 */
(( com.google.gson.stream.JsonWriter ) p1 ).value ( v0, v1 ); // invoke-virtual {p1, v0, v1}, Lcom/google/gson/stream/JsonWriter;->value(J)Lcom/google/gson/stream/JsonWriter;
/* .line 645 */
final String v0 = "dayOfMonth"; // const-string v0, "dayOfMonth"
(( com.google.gson.stream.JsonWriter ) p1 ).name ( v0 ); // invoke-virtual {p1, v0}, Lcom/google/gson/stream/JsonWriter;->name(Ljava/lang/String;)Lcom/google/gson/stream/JsonWriter;
/* .line 646 */
int v0 = 5; // const/4 v0, 0x5
v0 = (( java.util.Calendar ) p2 ).get ( v0 ); // invoke-virtual {p2, v0}, Ljava/util/Calendar;->get(I)I
/* int-to-long v0, v0 */
(( com.google.gson.stream.JsonWriter ) p1 ).value ( v0, v1 ); // invoke-virtual {p1, v0, v1}, Lcom/google/gson/stream/JsonWriter;->value(J)Lcom/google/gson/stream/JsonWriter;
/* .line 647 */
final String v0 = "hourOfDay"; // const-string v0, "hourOfDay"
(( com.google.gson.stream.JsonWriter ) p1 ).name ( v0 ); // invoke-virtual {p1, v0}, Lcom/google/gson/stream/JsonWriter;->name(Ljava/lang/String;)Lcom/google/gson/stream/JsonWriter;
/* .line 648 */
/* const/16 v0, 0xb */
v0 = (( java.util.Calendar ) p2 ).get ( v0 ); // invoke-virtual {p2, v0}, Ljava/util/Calendar;->get(I)I
/* int-to-long v0, v0 */
(( com.google.gson.stream.JsonWriter ) p1 ).value ( v0, v1 ); // invoke-virtual {p1, v0, v1}, Lcom/google/gson/stream/JsonWriter;->value(J)Lcom/google/gson/stream/JsonWriter;
/* .line 649 */
final String v0 = "minute"; // const-string v0, "minute"
(( com.google.gson.stream.JsonWriter ) p1 ).name ( v0 ); // invoke-virtual {p1, v0}, Lcom/google/gson/stream/JsonWriter;->name(Ljava/lang/String;)Lcom/google/gson/stream/JsonWriter;
/* .line 650 */
/* const/16 v0, 0xc */
v0 = (( java.util.Calendar ) p2 ).get ( v0 ); // invoke-virtual {p2, v0}, Ljava/util/Calendar;->get(I)I
/* int-to-long v0, v0 */
(( com.google.gson.stream.JsonWriter ) p1 ).value ( v0, v1 ); // invoke-virtual {p1, v0, v1}, Lcom/google/gson/stream/JsonWriter;->value(J)Lcom/google/gson/stream/JsonWriter;
/* .line 651 */
/* const-string/jumbo v0, "second" */
(( com.google.gson.stream.JsonWriter ) p1 ).name ( v0 ); // invoke-virtual {p1, v0}, Lcom/google/gson/stream/JsonWriter;->name(Ljava/lang/String;)Lcom/google/gson/stream/JsonWriter;
/* .line 652 */
/* const/16 v0, 0xd */
v0 = (( java.util.Calendar ) p2 ).get ( v0 ); // invoke-virtual {p2, v0}, Ljava/util/Calendar;->get(I)I
/* int-to-long v0, v0 */
(( com.google.gson.stream.JsonWriter ) p1 ).value ( v0, v1 ); // invoke-virtual {p1, v0, v1}, Lcom/google/gson/stream/JsonWriter;->value(J)Lcom/google/gson/stream/JsonWriter;
/* .line 653 */
(( com.google.gson.stream.JsonWriter ) p1 ).endObject ( ); // invoke-virtual {p1}, Lcom/google/gson/stream/JsonWriter;->endObject()Lcom/google/gson/stream/JsonWriter;
/* .line 654 */
return;
} // .end method
