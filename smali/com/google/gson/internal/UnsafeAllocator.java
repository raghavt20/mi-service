public abstract class com.google.gson.internal.UnsafeAllocator {
	 /* .source "UnsafeAllocator.java" */
	 /* # direct methods */
	 public com.google.gson.internal.UnsafeAllocator ( ) {
		 /* .locals 0 */
		 /* .line 31 */
		 /* invoke-direct {p0}, Ljava/lang/Object;-><init>()V */
		 return;
	 } // .end method
	 static void assertInstantiable ( java.lang.Class p0 ) {
		 /* .locals 4 */
		 /* .annotation system Ldalvik/annotation/Signature; */
		 /* value = { */
		 /* "(", */
		 /* "Ljava/lang/Class<", */
		 /* "*>;)V" */
		 /* } */
	 } // .end annotation
	 /* .line 116 */
	 /* .local p0, "c":Ljava/lang/Class;, "Ljava/lang/Class<*>;" */
	 v0 = 	 (( java.lang.Class ) p0 ).getModifiers ( ); // invoke-virtual {p0}, Ljava/lang/Class;->getModifiers()I
	 /* .line 117 */
	 /* .local v0, "modifiers":I */
	 v1 = 	 java.lang.reflect.Modifier .isInterface ( v0 );
	 /* if-nez v1, :cond_1 */
	 /* .line 120 */
	 v1 = 	 java.lang.reflect.Modifier .isAbstract ( v0 );
	 /* if-nez v1, :cond_0 */
	 /* .line 123 */
	 return;
	 /* .line 121 */
} // :cond_0
/* new-instance v1, Ljava/lang/UnsupportedOperationException; */
/* new-instance v2, Ljava/lang/StringBuilder; */
/* invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V */
final String v3 = "Abstract class can\'t be instantiated! Class name: "; // const-string v3, "Abstract class can\'t be instantiated! Class name: "
(( java.lang.StringBuilder ) v2 ).append ( v3 ); // invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.Class ) p0 ).getName ( ); // invoke-virtual {p0}, Ljava/lang/Class;->getName()Ljava/lang/String;
(( java.lang.StringBuilder ) v2 ).append ( v3 ); // invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v2 ).toString ( ); // invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
/* invoke-direct {v1, v2}, Ljava/lang/UnsupportedOperationException;-><init>(Ljava/lang/String;)V */
/* throw v1 */
/* .line 118 */
} // :cond_1
/* new-instance v1, Ljava/lang/UnsupportedOperationException; */
/* new-instance v2, Ljava/lang/StringBuilder; */
/* invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V */
final String v3 = "Interface can\'t be instantiated! Interface name: "; // const-string v3, "Interface can\'t be instantiated! Interface name: "
(( java.lang.StringBuilder ) v2 ).append ( v3 ); // invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.Class ) p0 ).getName ( ); // invoke-virtual {p0}, Ljava/lang/Class;->getName()Ljava/lang/String;
(( java.lang.StringBuilder ) v2 ).append ( v3 ); // invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v2 ).toString ( ); // invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
/* invoke-direct {v1, v2}, Ljava/lang/UnsupportedOperationException;-><init>(Ljava/lang/String;)V */
/* throw v1 */
} // .end method
public static com.google.gson.internal.UnsafeAllocator create ( ) {
/* .locals 10 */
/* .line 40 */
final String v0 = "newInstance"; // const-string v0, "newInstance"
int v1 = 0; // const/4 v1, 0x0
int v2 = 0; // const/4 v2, 0x0
int v3 = 1; // const/4 v3, 0x1
try { // :try_start_0
/* const-string/jumbo v4, "sun.misc.Unsafe" */
java.lang.Class .forName ( v4 );
/* .line 41 */
/* .local v4, "unsafeClass":Ljava/lang/Class;, "Ljava/lang/Class<*>;" */
/* const-string/jumbo v5, "theUnsafe" */
(( java.lang.Class ) v4 ).getDeclaredField ( v5 ); // invoke-virtual {v4, v5}, Ljava/lang/Class;->getDeclaredField(Ljava/lang/String;)Ljava/lang/reflect/Field;
/* .line 42 */
/* .local v5, "f":Ljava/lang/reflect/Field; */
(( java.lang.reflect.Field ) v5 ).setAccessible ( v3 ); // invoke-virtual {v5, v3}, Ljava/lang/reflect/Field;->setAccessible(Z)V
/* .line 43 */
(( java.lang.reflect.Field ) v5 ).get ( v1 ); // invoke-virtual {v5, v1}, Ljava/lang/reflect/Field;->get(Ljava/lang/Object;)Ljava/lang/Object;
/* .line 44 */
/* .local v6, "unsafe":Ljava/lang/Object; */
final String v7 = "allocateInstance"; // const-string v7, "allocateInstance"
/* new-array v8, v3, [Ljava/lang/Class; */
/* const-class v9, Ljava/lang/Class; */
/* aput-object v9, v8, v2 */
(( java.lang.Class ) v4 ).getMethod ( v7, v8 ); // invoke-virtual {v4, v7, v8}, Ljava/lang/Class;->getMethod(Ljava/lang/String;[Ljava/lang/Class;)Ljava/lang/reflect/Method;
/* .line 45 */
/* .local v7, "allocateInstance":Ljava/lang/reflect/Method; */
/* new-instance v8, Lcom/google/gson/internal/UnsafeAllocator$1; */
/* invoke-direct {v8, v7, v6}, Lcom/google/gson/internal/UnsafeAllocator$1;-><init>(Ljava/lang/reflect/Method;Ljava/lang/Object;)V */
/* :try_end_0 */
/* .catch Ljava/lang/Exception; {:try_start_0 ..:try_end_0} :catch_0 */
/* .line 53 */
} // .end local v4 # "unsafeClass":Ljava/lang/Class;, "Ljava/lang/Class<*>;"
} // .end local v5 # "f":Ljava/lang/reflect/Field;
} // .end local v6 # "unsafe":Ljava/lang/Object;
} // .end local v7 # "allocateInstance":Ljava/lang/reflect/Method;
/* :catch_0 */
/* move-exception v4 */
/* .line 62 */
int v4 = 2; // const/4 v4, 0x2
try { // :try_start_1
/* const-class v5, Ljava/io/ObjectStreamClass; */
final String v6 = "getConstructorId"; // const-string v6, "getConstructorId"
/* new-array v7, v3, [Ljava/lang/Class; */
/* const-class v8, Ljava/lang/Class; */
/* aput-object v8, v7, v2 */
/* .line 63 */
(( java.lang.Class ) v5 ).getDeclaredMethod ( v6, v7 ); // invoke-virtual {v5, v6, v7}, Ljava/lang/Class;->getDeclaredMethod(Ljava/lang/String;[Ljava/lang/Class;)Ljava/lang/reflect/Method;
/* .line 64 */
/* .local v5, "getConstructorId":Ljava/lang/reflect/Method; */
(( java.lang.reflect.Method ) v5 ).setAccessible ( v3 ); // invoke-virtual {v5, v3}, Ljava/lang/reflect/Method;->setAccessible(Z)V
/* .line 65 */
/* new-array v6, v3, [Ljava/lang/Object; */
/* const-class v7, Ljava/lang/Object; */
/* aput-object v7, v6, v2 */
(( java.lang.reflect.Method ) v5 ).invoke ( v1, v6 ); // invoke-virtual {v5, v1, v6}, Ljava/lang/reflect/Method;->invoke(Ljava/lang/Object;[Ljava/lang/Object;)Ljava/lang/Object;
/* check-cast v1, Ljava/lang/Integer; */
v1 = (( java.lang.Integer ) v1 ).intValue ( ); // invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I
/* .line 66 */
/* .local v1, "constructorId":I */
/* const-class v6, Ljava/io/ObjectStreamClass; */
/* new-array v7, v4, [Ljava/lang/Class; */
/* const-class v8, Ljava/lang/Class; */
/* aput-object v8, v7, v2 */
v8 = java.lang.Integer.TYPE;
/* aput-object v8, v7, v3 */
/* .line 67 */
(( java.lang.Class ) v6 ).getDeclaredMethod ( v0, v7 ); // invoke-virtual {v6, v0, v7}, Ljava/lang/Class;->getDeclaredMethod(Ljava/lang/String;[Ljava/lang/Class;)Ljava/lang/reflect/Method;
/* .line 68 */
/* .local v6, "newInstance":Ljava/lang/reflect/Method; */
(( java.lang.reflect.Method ) v6 ).setAccessible ( v3 ); // invoke-virtual {v6, v3}, Ljava/lang/reflect/Method;->setAccessible(Z)V
/* .line 69 */
/* new-instance v7, Lcom/google/gson/internal/UnsafeAllocator$2; */
/* invoke-direct {v7, v6, v1}, Lcom/google/gson/internal/UnsafeAllocator$2;-><init>(Ljava/lang/reflect/Method;I)V */
/* :try_end_1 */
/* .catch Ljava/lang/Exception; {:try_start_1 ..:try_end_1} :catch_1 */
/* .line 77 */
} // .end local v1 # "constructorId":I
} // .end local v5 # "getConstructorId":Ljava/lang/reflect/Method;
} // .end local v6 # "newInstance":Ljava/lang/reflect/Method;
/* :catch_1 */
/* move-exception v1 */
/* .line 86 */
try { // :try_start_2
/* const-class v1, Ljava/io/ObjectInputStream; */
/* new-array v4, v4, [Ljava/lang/Class; */
/* const-class v5, Ljava/lang/Class; */
/* aput-object v5, v4, v2 */
/* const-class v2, Ljava/lang/Class; */
/* aput-object v2, v4, v3 */
/* .line 87 */
(( java.lang.Class ) v1 ).getDeclaredMethod ( v0, v4 ); // invoke-virtual {v1, v0, v4}, Ljava/lang/Class;->getDeclaredMethod(Ljava/lang/String;[Ljava/lang/Class;)Ljava/lang/reflect/Method;
/* .line 88 */
/* .local v0, "newInstance":Ljava/lang/reflect/Method; */
(( java.lang.reflect.Method ) v0 ).setAccessible ( v3 ); // invoke-virtual {v0, v3}, Ljava/lang/reflect/Method;->setAccessible(Z)V
/* .line 89 */
/* new-instance v1, Lcom/google/gson/internal/UnsafeAllocator$3; */
/* invoke-direct {v1, v0}, Lcom/google/gson/internal/UnsafeAllocator$3;-><init>(Ljava/lang/reflect/Method;)V */
/* :try_end_2 */
/* .catch Ljava/lang/Exception; {:try_start_2 ..:try_end_2} :catch_2 */
/* .line 97 */
} // .end local v0 # "newInstance":Ljava/lang/reflect/Method;
/* :catch_2 */
/* move-exception v0 */
/* .line 101 */
/* new-instance v0, Lcom/google/gson/internal/UnsafeAllocator$4; */
/* invoke-direct {v0}, Lcom/google/gson/internal/UnsafeAllocator$4;-><init>()V */
} // .end method
/* # virtual methods */
public abstract java.lang.Object newInstance ( java.lang.Class p0 ) {
/* .annotation system Ldalvik/annotation/Signature; */
/* value = { */
/* "<T:", */
/* "Ljava/lang/Object;", */
/* ">(", */
/* "Ljava/lang/Class<", */
/* "TT;>;)TT;" */
/* } */
} // .end annotation
/* .annotation system Ldalvik/annotation/Throws; */
/* value = { */
/* Ljava/lang/Exception; */
/* } */
} // .end annotation
} // .end method
