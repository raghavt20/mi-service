abstract class com.google.gson.internal.LinkedTreeMap$LinkedTreeMapIterator implements java.util.Iterator {
	 /* .source "LinkedTreeMap.java" */
	 /* # interfaces */
	 /* # annotations */
	 /* .annotation system Ldalvik/annotation/EnclosingClass; */
	 /* value = Lcom/google/gson/internal/LinkedTreeMap; */
} // .end annotation
/* .annotation system Ldalvik/annotation/InnerClass; */
/* accessFlags = 0x402 */
/* name = "LinkedTreeMapIterator" */
} // .end annotation
/* .annotation system Ldalvik/annotation/Signature; */
/* value = { */
/* "<T:", */
/* "Ljava/lang/Object;", */
/* ">", */
/* "Ljava/lang/Object;", */
/* "Ljava/util/Iterator<", */
/* "TT;>;" */
/* } */
} // .end annotation
/* # instance fields */
Integer expectedModCount;
com.google.gson.internal.LinkedTreeMap$Node lastReturned;
/* .annotation system Ldalvik/annotation/Signature; */
/* value = { */
/* "Lcom/google/gson/internal/LinkedTreeMap$Node<", */
/* "TK;TV;>;" */
/* } */
} // .end annotation
} // .end field
com.google.gson.internal.LinkedTreeMap$Node next;
/* .annotation system Ldalvik/annotation/Signature; */
/* value = { */
/* "Lcom/google/gson/internal/LinkedTreeMap$Node<", */
/* "TK;TV;>;" */
/* } */
} // .end annotation
} // .end field
final com.google.gson.internal.LinkedTreeMap this$0; //synthetic
/* # direct methods */
 com.google.gson.internal.LinkedTreeMap$LinkedTreeMapIterator ( ) {
/* .locals 1 */
/* .line 534 */
/* .local p0, "this":Lcom/google/gson/internal/LinkedTreeMap$LinkedTreeMapIterator;, "Lcom/google/gson/internal/LinkedTreeMap<TK;TV;>.LinkedTreeMapIterator<TT;>;" */
this.this$0 = p1;
/* invoke-direct {p0}, Ljava/lang/Object;-><init>()V */
/* .line 530 */
v0 = this.header;
v0 = this.next;
this.next = v0;
/* .line 531 */
int v0 = 0; // const/4 v0, 0x0
this.lastReturned = v0;
/* .line 532 */
/* iget p1, p1, Lcom/google/gson/internal/LinkedTreeMap;->modCount:I */
/* iput p1, p0, Lcom/google/gson/internal/LinkedTreeMap$LinkedTreeMapIterator;->expectedModCount:I */
/* .line 535 */
return;
} // .end method
/* # virtual methods */
public final Boolean hasNext ( ) {
/* .locals 2 */
/* .line 538 */
/* .local p0, "this":Lcom/google/gson/internal/LinkedTreeMap$LinkedTreeMapIterator;, "Lcom/google/gson/internal/LinkedTreeMap<TK;TV;>.LinkedTreeMapIterator<TT;>;" */
v0 = this.next;
v1 = this.this$0;
v1 = this.header;
/* if-eq v0, v1, :cond_0 */
int v0 = 1; // const/4 v0, 0x1
} // :cond_0
int v0 = 0; // const/4 v0, 0x0
} // :goto_0
} // .end method
final com.google.gson.internal.LinkedTreeMap$Node nextNode ( ) {
/* .locals 3 */
/* .annotation system Ldalvik/annotation/Signature; */
/* value = { */
/* "()", */
/* "Lcom/google/gson/internal/LinkedTreeMap$Node<", */
/* "TK;TV;>;" */
/* } */
} // .end annotation
/* .line 542 */
/* .local p0, "this":Lcom/google/gson/internal/LinkedTreeMap$LinkedTreeMapIterator;, "Lcom/google/gson/internal/LinkedTreeMap<TK;TV;>.LinkedTreeMapIterator<TT;>;" */
v0 = this.next;
/* .line 543 */
/* .local v0, "e":Lcom/google/gson/internal/LinkedTreeMap$Node;, "Lcom/google/gson/internal/LinkedTreeMap$Node<TK;TV;>;" */
v1 = this.this$0;
v1 = this.header;
/* if-eq v0, v1, :cond_1 */
/* .line 546 */
v1 = this.this$0;
/* iget v1, v1, Lcom/google/gson/internal/LinkedTreeMap;->modCount:I */
/* iget v2, p0, Lcom/google/gson/internal/LinkedTreeMap$LinkedTreeMapIterator;->expectedModCount:I */
/* if-ne v1, v2, :cond_0 */
/* .line 549 */
v1 = this.next;
this.next = v1;
/* .line 550 */
this.lastReturned = v0;
/* .line 547 */
} // :cond_0
/* new-instance v1, Ljava/util/ConcurrentModificationException; */
/* invoke-direct {v1}, Ljava/util/ConcurrentModificationException;-><init>()V */
/* throw v1 */
/* .line 544 */
} // :cond_1
/* new-instance v1, Ljava/util/NoSuchElementException; */
/* invoke-direct {v1}, Ljava/util/NoSuchElementException;-><init>()V */
/* throw v1 */
} // .end method
public final void remove ( ) {
/* .locals 3 */
/* .line 554 */
/* .local p0, "this":Lcom/google/gson/internal/LinkedTreeMap$LinkedTreeMapIterator;, "Lcom/google/gson/internal/LinkedTreeMap<TK;TV;>.LinkedTreeMapIterator<TT;>;" */
v0 = this.lastReturned;
if ( v0 != null) { // if-eqz v0, :cond_0
/* .line 557 */
v1 = this.this$0;
int v2 = 1; // const/4 v2, 0x1
(( com.google.gson.internal.LinkedTreeMap ) v1 ).removeInternal ( v0, v2 ); // invoke-virtual {v1, v0, v2}, Lcom/google/gson/internal/LinkedTreeMap;->removeInternal(Lcom/google/gson/internal/LinkedTreeMap$Node;Z)V
/* .line 558 */
int v0 = 0; // const/4 v0, 0x0
this.lastReturned = v0;
/* .line 559 */
v0 = this.this$0;
/* iget v0, v0, Lcom/google/gson/internal/LinkedTreeMap;->modCount:I */
/* iput v0, p0, Lcom/google/gson/internal/LinkedTreeMap$LinkedTreeMapIterator;->expectedModCount:I */
/* .line 560 */
return;
/* .line 555 */
} // :cond_0
/* new-instance v0, Ljava/lang/IllegalStateException; */
/* invoke-direct {v0}, Ljava/lang/IllegalStateException;-><init>()V */
/* throw v0 */
} // .end method
