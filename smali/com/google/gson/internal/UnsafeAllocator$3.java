class com.google.gson.internal.UnsafeAllocator$3 extends com.google.gson.internal.UnsafeAllocator {
	 /* .source "UnsafeAllocator.java" */
	 /* # annotations */
	 /* .annotation system Ldalvik/annotation/EnclosingMethod; */
	 /* value = Lcom/google/gson/internal/UnsafeAllocator;->create()Lcom/google/gson/internal/UnsafeAllocator; */
} // .end annotation
/* .annotation system Ldalvik/annotation/InnerClass; */
/* accessFlags = 0x0 */
/* name = null */
} // .end annotation
/* # instance fields */
final java.lang.reflect.Method val$newInstance; //synthetic
/* # direct methods */
 com.google.gson.internal.UnsafeAllocator$3 ( ) {
/* .locals 0 */
/* .line 89 */
this.val$newInstance = p1;
/* invoke-direct {p0}, Lcom/google/gson/internal/UnsafeAllocator;-><init>()V */
return;
} // .end method
/* # virtual methods */
public java.lang.Object newInstance ( java.lang.Class p0 ) {
/* .locals 3 */
/* .annotation system Ldalvik/annotation/Signature; */
/* value = { */
/* "<T:", */
/* "Ljava/lang/Object;", */
/* ">(", */
/* "Ljava/lang/Class<", */
/* "TT;>;)TT;" */
/* } */
} // .end annotation
/* .annotation system Ldalvik/annotation/Throws; */
/* value = { */
/* Ljava/lang/Exception; */
/* } */
} // .end annotation
/* .line 93 */
/* .local p1, "c":Ljava/lang/Class;, "Ljava/lang/Class<TT;>;" */
com.google.gson.internal.UnsafeAllocator$3 .assertInstantiable ( p1 );
/* .line 94 */
v0 = this.val$newInstance;
/* const-class v1, Ljava/lang/Object; */
/* filled-new-array {p1, v1}, [Ljava/lang/Object; */
int v2 = 0; // const/4 v2, 0x0
(( java.lang.reflect.Method ) v0 ).invoke ( v2, v1 ); // invoke-virtual {v0, v2, v1}, Ljava/lang/reflect/Method;->invoke(Ljava/lang/Object;[Ljava/lang/Object;)Ljava/lang/Object;
} // .end method
