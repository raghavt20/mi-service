public abstract class com.google.gson.internal.JsonReaderInternalAccess {
	 /* .source "JsonReaderInternalAccess.java" */
	 /* # static fields */
	 public static com.google.gson.internal.JsonReaderInternalAccess INSTANCE;
	 /* # direct methods */
	 public com.google.gson.internal.JsonReaderInternalAccess ( ) {
		 /* .locals 0 */
		 /* .line 25 */
		 /* invoke-direct {p0}, Ljava/lang/Object;-><init>()V */
		 return;
	 } // .end method
	 /* # virtual methods */
	 public abstract void promoteNameToValue ( com.google.gson.stream.JsonReader p0 ) {
		 /* .annotation system Ldalvik/annotation/Throws; */
		 /* value = { */
		 /* Ljava/io/IOException; */
		 /* } */
	 } // .end annotation
} // .end method
