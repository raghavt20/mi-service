class com.google.gson.internal.sql.SqlTypesSupport$2 extends com.google.gson.internal.bind.DefaultDateTypeAdapter$DateType {
	 /* .source "SqlTypesSupport.java" */
	 /* # annotations */
	 /* .annotation system Ldalvik/annotation/EnclosingClass; */
	 /* value = Lcom/google/gson/internal/sql/SqlTypesSupport; */
} // .end annotation
/* .annotation system Ldalvik/annotation/InnerClass; */
/* accessFlags = 0x0 */
/* name = null */
} // .end annotation
/* .annotation system Ldalvik/annotation/Signature; */
/* value = { */
/* "Lcom/google/gson/internal/bind/DefaultDateTypeAdapter$DateType<", */
/* "Ljava/sql/Timestamp;", */
/* ">;" */
/* } */
} // .end annotation
/* # direct methods */
 com.google.gson.internal.sql.SqlTypesSupport$2 ( ) {
/* .locals 0 */
/* .line 50 */
/* .local p1, "dateClass":Ljava/lang/Class;, "Ljava/lang/Class<Ljava/sql/Timestamp;>;" */
/* invoke-direct {p0, p1}, Lcom/google/gson/internal/bind/DefaultDateTypeAdapter$DateType;-><init>(Ljava/lang/Class;)V */
return;
} // .end method
/* # virtual methods */
protected java.sql.Timestamp deserialize ( java.util.Date p0 ) {
/* .locals 3 */
/* .param p1, "date" # Ljava/util/Date; */
/* .line 52 */
/* new-instance v0, Ljava/sql/Timestamp; */
(( java.util.Date ) p1 ).getTime ( ); // invoke-virtual {p1}, Ljava/util/Date;->getTime()J
/* move-result-wide v1 */
/* invoke-direct {v0, v1, v2}, Ljava/sql/Timestamp;-><init>(J)V */
} // .end method
protected java.util.Date deserialize ( java.util.Date p0 ) { //bridge//synthethic
/* .locals 0 */
/* .line 50 */
(( com.google.gson.internal.sql.SqlTypesSupport$2 ) p0 ).deserialize ( p1 ); // invoke-virtual {p0, p1}, Lcom/google/gson/internal/sql/SqlTypesSupport$2;->deserialize(Ljava/util/Date;)Ljava/sql/Timestamp;
} // .end method
