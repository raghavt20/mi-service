class com.google.gson.internal.sql.SqlTypesSupport$1 extends com.google.gson.internal.bind.DefaultDateTypeAdapter$DateType {
	 /* .source "SqlTypesSupport.java" */
	 /* # annotations */
	 /* .annotation system Ldalvik/annotation/EnclosingClass; */
	 /* value = Lcom/google/gson/internal/sql/SqlTypesSupport; */
} // .end annotation
/* .annotation system Ldalvik/annotation/InnerClass; */
/* accessFlags = 0x0 */
/* name = null */
} // .end annotation
/* .annotation system Ldalvik/annotation/Signature; */
/* value = { */
/* "Lcom/google/gson/internal/bind/DefaultDateTypeAdapter$DateType<", */
/* "Ljava/sql/Date;", */
/* ">;" */
/* } */
} // .end annotation
/* # direct methods */
 com.google.gson.internal.sql.SqlTypesSupport$1 ( ) {
/* .locals 0 */
/* .line 45 */
/* .local p1, "dateClass":Ljava/lang/Class;, "Ljava/lang/Class<Ljava/sql/Date;>;" */
/* invoke-direct {p0, p1}, Lcom/google/gson/internal/bind/DefaultDateTypeAdapter$DateType;-><init>(Ljava/lang/Class;)V */
return;
} // .end method
/* # virtual methods */
protected java.sql.Date deserialize ( java.util.Date p0 ) {
/* .locals 3 */
/* .param p1, "date" # Ljava/util/Date; */
/* .line 47 */
/* new-instance v0, Ljava/sql/Date; */
(( java.util.Date ) p1 ).getTime ( ); // invoke-virtual {p1}, Ljava/util/Date;->getTime()J
/* move-result-wide v1 */
/* invoke-direct {v0, v1, v2}, Ljava/sql/Date;-><init>(J)V */
} // .end method
protected java.util.Date deserialize ( java.util.Date p0 ) { //bridge//synthethic
/* .locals 0 */
/* .line 45 */
(( com.google.gson.internal.sql.SqlTypesSupport$1 ) p0 ).deserialize ( p1 ); // invoke-virtual {p0, p1}, Lcom/google/gson/internal/sql/SqlTypesSupport$1;->deserialize(Ljava/util/Date;)Ljava/sql/Date;
} // .end method
