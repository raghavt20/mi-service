public class inal {
	 /* .source "SqlTypesSupport.java" */
	 /* # static fields */
	 public static final com.google.gson.internal.bind.DefaultDateTypeAdapter$DateType DATE_DATE_TYPE;
	 /* .annotation system Ldalvik/annotation/Signature; */
	 /* value = { */
	 /* "Lcom/google/gson/internal/bind/DefaultDateTypeAdapter$DateType<", */
	 /* "+", */
	 /* "Ljava/util/Date;", */
	 /* ">;" */
	 /* } */
} // .end annotation
} // .end field
public static final com.google.gson.TypeAdapterFactory DATE_FACTORY;
public static final Boolean SUPPORTS_SQL_TYPES;
public static final com.google.gson.internal.bind.DefaultDateTypeAdapter$DateType TIMESTAMP_DATE_TYPE;
/* .annotation system Ldalvik/annotation/Signature; */
/* value = { */
/* "Lcom/google/gson/internal/bind/DefaultDateTypeAdapter$DateType<", */
/* "+", */
/* "Ljava/util/Date;", */
/* ">;" */
/* } */
} // .end annotation
} // .end field
public static final com.google.gson.TypeAdapterFactory TIMESTAMP_FACTORY;
public static final com.google.gson.TypeAdapterFactory TIME_FACTORY;
/* # direct methods */
static inal ( ) {
/* .locals 3 */
/* .line 37 */
try { // :try_start_0
final String v0 = "java.sql.Date"; // const-string v0, "java.sql.Date"
java.lang.Class .forName ( v0 );
/* :try_end_0 */
/* .catch Ljava/lang/ClassNotFoundException; {:try_start_0 ..:try_end_0} :catch_0 */
/* .line 38 */
int v0 = 1; // const/4 v0, 0x1
/* .line 41 */
/* .local v0, "sqlTypesSupport":Z */
/* .line 39 */
} // .end local v0 # "sqlTypesSupport":Z
/* :catch_0 */
/* move-exception v0 */
/* .line 40 */
/* .local v0, "classNotFoundException":Ljava/lang/ClassNotFoundException; */
int v1 = 0; // const/4 v1, 0x0
/* move v0, v1 */
/* .line 42 */
/* .local v0, "sqlTypesSupport":Z */
} // :goto_0
com.google.gson.internal.sql.SqlTypesSupport.SUPPORTS_SQL_TYPES = (v0!= 0);
/* .line 44 */
if ( v0 != null) { // if-eqz v0, :cond_0
/* .line 45 */
/* new-instance v1, Lcom/google/gson/internal/sql/SqlTypesSupport$1; */
/* const-class v2, Ljava/sql/Date; */
/* invoke-direct {v1, v2}, Lcom/google/gson/internal/sql/SqlTypesSupport$1;-><init>(Ljava/lang/Class;)V */
/* .line 50 */
/* new-instance v1, Lcom/google/gson/internal/sql/SqlTypesSupport$2; */
/* const-class v2, Ljava/sql/Timestamp; */
/* invoke-direct {v1, v2}, Lcom/google/gson/internal/sql/SqlTypesSupport$2;-><init>(Ljava/lang/Class;)V */
/* .line 56 */
v1 = com.google.gson.internal.sql.SqlDateTypeAdapter.FACTORY;
/* .line 57 */
v1 = com.google.gson.internal.sql.SqlTimeTypeAdapter.FACTORY;
/* .line 58 */
v1 = com.google.gson.internal.sql.SqlTimestampTypeAdapter.FACTORY;
/* .line 60 */
} // :cond_0
int v1 = 0; // const/4 v1, 0x0
/* .line 61 */
/* .line 63 */
/* .line 64 */
/* .line 65 */
/* .line 67 */
} // .end local v0 # "sqlTypesSupport":Z
} // :goto_1
return;
} // .end method
private inal ( ) {
/* .locals 0 */
/* .line 69 */
/* invoke-direct {p0}, Ljava/lang/Object;-><init>()V */
/* .line 70 */
return;
} // .end method
