class com.google.gson.internal.sql.SqlTimestampTypeAdapter extends com.google.gson.TypeAdapter {
	 /* .source "SqlTimestampTypeAdapter.java" */
	 /* # annotations */
	 /* .annotation system Ldalvik/annotation/Signature; */
	 /* value = { */
	 /* "Lcom/google/gson/TypeAdapter<", */
	 /* "Ljava/sql/Timestamp;", */
	 /* ">;" */
	 /* } */
} // .end annotation
/* # static fields */
static final com.google.gson.TypeAdapterFactory FACTORY;
/* # instance fields */
private final com.google.gson.TypeAdapter dateTypeAdapter;
/* .annotation system Ldalvik/annotation/Signature; */
/* value = { */
/* "Lcom/google/gson/TypeAdapter<", */
/* "Ljava/util/Date;", */
/* ">;" */
/* } */
} // .end annotation
} // .end field
/* # direct methods */
static com.google.gson.internal.sql.SqlTimestampTypeAdapter ( ) {
/* .locals 1 */
/* .line 15 */
/* new-instance v0, Lcom/google/gson/internal/sql/SqlTimestampTypeAdapter$1; */
/* invoke-direct {v0}, Lcom/google/gson/internal/sql/SqlTimestampTypeAdapter$1;-><init>()V */
return;
} // .end method
private com.google.gson.internal.sql.SqlTimestampTypeAdapter ( ) {
/* .locals 0 */
/* .annotation system Ldalvik/annotation/Signature; */
/* value = { */
/* "(", */
/* "Lcom/google/gson/TypeAdapter<", */
/* "Ljava/util/Date;", */
/* ">;)V" */
/* } */
} // .end annotation
/* .line 29 */
/* .local p1, "dateTypeAdapter":Lcom/google/gson/TypeAdapter;, "Lcom/google/gson/TypeAdapter<Ljava/util/Date;>;" */
/* invoke-direct {p0}, Lcom/google/gson/TypeAdapter;-><init>()V */
/* .line 30 */
this.dateTypeAdapter = p1;
/* .line 31 */
return;
} // .end method
 com.google.gson.internal.sql.SqlTimestampTypeAdapter ( ) { //synthethic
/* .locals 0 */
/* .param p1, "x0" # Lcom/google/gson/TypeAdapter; */
/* .param p2, "x1" # Lcom/google/gson/internal/sql/SqlTimestampTypeAdapter$1; */
/* .line 14 */
/* invoke-direct {p0, p1}, Lcom/google/gson/internal/sql/SqlTimestampTypeAdapter;-><init>(Lcom/google/gson/TypeAdapter;)V */
return;
} // .end method
/* # virtual methods */
public java.lang.Object read ( com.google.gson.stream.JsonReader p0 ) { //bridge//synthethic
/* .locals 0 */
/* .annotation system Ldalvik/annotation/Throws; */
/* value = { */
/* Ljava/io/IOException; */
/* } */
} // .end annotation
/* .line 14 */
(( com.google.gson.internal.sql.SqlTimestampTypeAdapter ) p0 ).read ( p1 ); // invoke-virtual {p0, p1}, Lcom/google/gson/internal/sql/SqlTimestampTypeAdapter;->read(Lcom/google/gson/stream/JsonReader;)Ljava/sql/Timestamp;
} // .end method
public java.sql.Timestamp read ( com.google.gson.stream.JsonReader p0 ) {
/* .locals 4 */
/* .param p1, "in" # Lcom/google/gson/stream/JsonReader; */
/* .annotation system Ldalvik/annotation/Throws; */
/* value = { */
/* Ljava/io/IOException; */
/* } */
} // .end annotation
/* .line 35 */
v0 = this.dateTypeAdapter;
(( com.google.gson.TypeAdapter ) v0 ).read ( p1 ); // invoke-virtual {v0, p1}, Lcom/google/gson/TypeAdapter;->read(Lcom/google/gson/stream/JsonReader;)Ljava/lang/Object;
/* check-cast v0, Ljava/util/Date; */
/* .line 36 */
/* .local v0, "date":Ljava/util/Date; */
if ( v0 != null) { // if-eqz v0, :cond_0
/* new-instance v1, Ljava/sql/Timestamp; */
(( java.util.Date ) v0 ).getTime ( ); // invoke-virtual {v0}, Ljava/util/Date;->getTime()J
/* move-result-wide v2 */
/* invoke-direct {v1, v2, v3}, Ljava/sql/Timestamp;-><init>(J)V */
} // :cond_0
int v1 = 0; // const/4 v1, 0x0
} // :goto_0
} // .end method
public void write ( com.google.gson.stream.JsonWriter p0, java.lang.Object p1 ) { //bridge//synthethic
/* .locals 0 */
/* .annotation system Ldalvik/annotation/Throws; */
/* value = { */
/* Ljava/io/IOException; */
/* } */
} // .end annotation
/* .line 14 */
/* check-cast p2, Ljava/sql/Timestamp; */
(( com.google.gson.internal.sql.SqlTimestampTypeAdapter ) p0 ).write ( p1, p2 ); // invoke-virtual {p0, p1, p2}, Lcom/google/gson/internal/sql/SqlTimestampTypeAdapter;->write(Lcom/google/gson/stream/JsonWriter;Ljava/sql/Timestamp;)V
return;
} // .end method
public void write ( com.google.gson.stream.JsonWriter p0, java.sql.Timestamp p1 ) {
/* .locals 1 */
/* .param p1, "out" # Lcom/google/gson/stream/JsonWriter; */
/* .param p2, "value" # Ljava/sql/Timestamp; */
/* .annotation system Ldalvik/annotation/Throws; */
/* value = { */
/* Ljava/io/IOException; */
/* } */
} // .end annotation
/* .line 41 */
v0 = this.dateTypeAdapter;
(( com.google.gson.TypeAdapter ) v0 ).write ( p1, p2 ); // invoke-virtual {v0, p1, p2}, Lcom/google/gson/TypeAdapter;->write(Lcom/google/gson/stream/JsonWriter;Ljava/lang/Object;)V
/* .line 42 */
return;
} // .end method
