class com.google.gson.internal.sql.SqlTimestampTypeAdapter$1 implements com.google.gson.TypeAdapterFactory {
	 /* .source "SqlTimestampTypeAdapter.java" */
	 /* # interfaces */
	 /* # annotations */
	 /* .annotation system Ldalvik/annotation/EnclosingClass; */
	 /* value = Lcom/google/gson/internal/sql/SqlTimestampTypeAdapter; */
} // .end annotation
/* .annotation system Ldalvik/annotation/InnerClass; */
/* accessFlags = 0x0 */
/* name = null */
} // .end annotation
/* # direct methods */
 com.google.gson.internal.sql.SqlTimestampTypeAdapter$1 ( ) {
/* .locals 0 */
/* .line 15 */
/* invoke-direct {p0}, Ljava/lang/Object;-><init>()V */
return;
} // .end method
/* # virtual methods */
public com.google.gson.TypeAdapter create ( com.google.gson.Gson p0, com.google.gson.reflect.TypeToken p1 ) {
/* .locals 3 */
/* .param p1, "gson" # Lcom/google/gson/Gson; */
/* .annotation system Ldalvik/annotation/Signature; */
/* value = { */
/* "<T:", */
/* "Ljava/lang/Object;", */
/* ">(", */
/* "Lcom/google/gson/Gson;", */
/* "Lcom/google/gson/reflect/TypeToken<", */
/* "TT;>;)", */
/* "Lcom/google/gson/TypeAdapter<", */
/* "TT;>;" */
/* } */
} // .end annotation
/* .line 18 */
/* .local p2, "typeToken":Lcom/google/gson/reflect/TypeToken;, "Lcom/google/gson/reflect/TypeToken<TT;>;" */
(( com.google.gson.reflect.TypeToken ) p2 ).getRawType ( ); // invoke-virtual {p2}, Lcom/google/gson/reflect/TypeToken;->getRawType()Ljava/lang/Class;
/* const-class v1, Ljava/sql/Timestamp; */
int v2 = 0; // const/4 v2, 0x0
/* if-ne v0, v1, :cond_0 */
/* .line 19 */
/* const-class v0, Ljava/util/Date; */
(( com.google.gson.Gson ) p1 ).getAdapter ( v0 ); // invoke-virtual {p1, v0}, Lcom/google/gson/Gson;->getAdapter(Ljava/lang/Class;)Lcom/google/gson/TypeAdapter;
/* .line 20 */
/* .local v0, "dateTypeAdapter":Lcom/google/gson/TypeAdapter;, "Lcom/google/gson/TypeAdapter<Ljava/util/Date;>;" */
/* new-instance v1, Lcom/google/gson/internal/sql/SqlTimestampTypeAdapter; */
/* invoke-direct {v1, v0, v2}, Lcom/google/gson/internal/sql/SqlTimestampTypeAdapter;-><init>(Lcom/google/gson/TypeAdapter;Lcom/google/gson/internal/sql/SqlTimestampTypeAdapter$1;)V */
/* .line 22 */
} // .end local v0 # "dateTypeAdapter":Lcom/google/gson/TypeAdapter;, "Lcom/google/gson/TypeAdapter<Ljava/util/Date;>;"
} // :cond_0
} // .end method
