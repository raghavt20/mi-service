class com.google.gson.internal.Excluder$1 extends com.google.gson.TypeAdapter {
	 /* .source "Excluder.java" */
	 /* # annotations */
	 /* .annotation system Ldalvik/annotation/EnclosingMethod; */
	 /* value = Lcom/google/gson/internal/Excluder;->create(Lcom/google/gson/Gson;Lcom/google/gson/reflect/TypeToken;)Lcom/google/gson/TypeAdapter; */
} // .end annotation
/* .annotation system Ldalvik/annotation/InnerClass; */
/* accessFlags = 0x0 */
/* name = null */
} // .end annotation
/* .annotation system Ldalvik/annotation/Signature; */
/* value = { */
/* "Lcom/google/gson/TypeAdapter<", */
/* "TT;>;" */
/* } */
} // .end annotation
/* # instance fields */
private com.google.gson.TypeAdapter delegate;
/* .annotation system Ldalvik/annotation/Signature; */
/* value = { */
/* "Lcom/google/gson/TypeAdapter<", */
/* "TT;>;" */
/* } */
} // .end annotation
} // .end field
final com.google.gson.internal.Excluder this$0; //synthetic
final com.google.gson.Gson val$gson; //synthetic
final Boolean val$skipDeserialize; //synthetic
final Boolean val$skipSerialize; //synthetic
final com.google.gson.reflect.TypeToken val$type; //synthetic
/* # direct methods */
 com.google.gson.internal.Excluder$1 ( ) {
/* .locals 0 */
/* .param p1, "this$0" # Lcom/google/gson/internal/Excluder; */
/* .line 122 */
this.this$0 = p1;
/* iput-boolean p2, p0, Lcom/google/gson/internal/Excluder$1;->val$skipDeserialize:Z */
/* iput-boolean p3, p0, Lcom/google/gson/internal/Excluder$1;->val$skipSerialize:Z */
this.val$gson = p4;
this.val$type = p5;
/* invoke-direct {p0}, Lcom/google/gson/TypeAdapter;-><init>()V */
return;
} // .end method
private com.google.gson.TypeAdapter delegate ( ) {
/* .locals 4 */
/* .annotation system Ldalvik/annotation/Signature; */
/* value = { */
/* "()", */
/* "Lcom/google/gson/TypeAdapter<", */
/* "TT;>;" */
/* } */
} // .end annotation
/* .line 143 */
v0 = this.delegate;
/* .line 144 */
/* .local v0, "d":Lcom/google/gson/TypeAdapter;, "Lcom/google/gson/TypeAdapter<TT;>;" */
if ( v0 != null) { // if-eqz v0, :cond_0
/* .line 145 */
/* move-object v1, v0 */
/* .line 146 */
} // :cond_0
v1 = this.val$gson;
v2 = this.this$0;
v3 = this.val$type;
(( com.google.gson.Gson ) v1 ).getDelegateAdapter ( v2, v3 ); // invoke-virtual {v1, v2, v3}, Lcom/google/gson/Gson;->getDelegateAdapter(Lcom/google/gson/TypeAdapterFactory;Lcom/google/gson/reflect/TypeToken;)Lcom/google/gson/TypeAdapter;
this.delegate = v1;
/* .line 144 */
} // :goto_0
} // .end method
/* # virtual methods */
public java.lang.Object read ( com.google.gson.stream.JsonReader p0 ) {
/* .locals 1 */
/* .param p1, "in" # Lcom/google/gson/stream/JsonReader; */
/* .annotation system Ldalvik/annotation/Signature; */
/* value = { */
/* "(", */
/* "Lcom/google/gson/stream/JsonReader;", */
/* ")TT;" */
/* } */
} // .end annotation
/* .annotation system Ldalvik/annotation/Throws; */
/* value = { */
/* Ljava/io/IOException; */
/* } */
} // .end annotation
/* .line 127 */
/* iget-boolean v0, p0, Lcom/google/gson/internal/Excluder$1;->val$skipDeserialize:Z */
if ( v0 != null) { // if-eqz v0, :cond_0
/* .line 128 */
(( com.google.gson.stream.JsonReader ) p1 ).skipValue ( ); // invoke-virtual {p1}, Lcom/google/gson/stream/JsonReader;->skipValue()V
/* .line 129 */
int v0 = 0; // const/4 v0, 0x0
/* .line 131 */
} // :cond_0
/* invoke-direct {p0}, Lcom/google/gson/internal/Excluder$1;->delegate()Lcom/google/gson/TypeAdapter; */
(( com.google.gson.TypeAdapter ) v0 ).read ( p1 ); // invoke-virtual {v0, p1}, Lcom/google/gson/TypeAdapter;->read(Lcom/google/gson/stream/JsonReader;)Ljava/lang/Object;
} // .end method
public void write ( com.google.gson.stream.JsonWriter p0, java.lang.Object p1 ) {
/* .locals 1 */
/* .param p1, "out" # Lcom/google/gson/stream/JsonWriter; */
/* .annotation system Ldalvik/annotation/Signature; */
/* value = { */
/* "(", */
/* "Lcom/google/gson/stream/JsonWriter;", */
/* "TT;)V" */
/* } */
} // .end annotation
/* .annotation system Ldalvik/annotation/Throws; */
/* value = { */
/* Ljava/io/IOException; */
/* } */
} // .end annotation
/* .line 135 */
/* .local p2, "value":Ljava/lang/Object;, "TT;" */
/* iget-boolean v0, p0, Lcom/google/gson/internal/Excluder$1;->val$skipSerialize:Z */
if ( v0 != null) { // if-eqz v0, :cond_0
/* .line 136 */
(( com.google.gson.stream.JsonWriter ) p1 ).nullValue ( ); // invoke-virtual {p1}, Lcom/google/gson/stream/JsonWriter;->nullValue()Lcom/google/gson/stream/JsonWriter;
/* .line 137 */
return;
/* .line 139 */
} // :cond_0
/* invoke-direct {p0}, Lcom/google/gson/internal/Excluder$1;->delegate()Lcom/google/gson/TypeAdapter; */
(( com.google.gson.TypeAdapter ) v0 ).write ( p1, p2 ); // invoke-virtual {v0, p1, p2}, Lcom/google/gson/TypeAdapter;->write(Lcom/google/gson/stream/JsonWriter;Ljava/lang/Object;)V
/* .line 140 */
return;
} // .end method
