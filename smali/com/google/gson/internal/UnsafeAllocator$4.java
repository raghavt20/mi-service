class com.google.gson.internal.UnsafeAllocator$4 extends com.google.gson.internal.UnsafeAllocator {
	 /* .source "UnsafeAllocator.java" */
	 /* # annotations */
	 /* .annotation system Ldalvik/annotation/EnclosingMethod; */
	 /* value = Lcom/google/gson/internal/UnsafeAllocator;->create()Lcom/google/gson/internal/UnsafeAllocator; */
} // .end annotation
/* .annotation system Ldalvik/annotation/InnerClass; */
/* accessFlags = 0x0 */
/* name = null */
} // .end annotation
/* # direct methods */
 com.google.gson.internal.UnsafeAllocator$4 ( ) {
/* .locals 0 */
/* .line 101 */
/* invoke-direct {p0}, Lcom/google/gson/internal/UnsafeAllocator;-><init>()V */
return;
} // .end method
/* # virtual methods */
public java.lang.Object newInstance ( java.lang.Class p0 ) {
/* .locals 3 */
/* .annotation system Ldalvik/annotation/Signature; */
/* value = { */
/* "<T:", */
/* "Ljava/lang/Object;", */
/* ">(", */
/* "Ljava/lang/Class<", */
/* "TT;>;)TT;" */
/* } */
} // .end annotation
/* .line 104 */
/* .local p1, "c":Ljava/lang/Class;, "Ljava/lang/Class<TT;>;" */
/* new-instance v0, Ljava/lang/UnsupportedOperationException; */
/* new-instance v1, Ljava/lang/StringBuilder; */
/* invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V */
final String v2 = "Cannot allocate "; // const-string v2, "Cannot allocate "
(( java.lang.StringBuilder ) v1 ).append ( v2 ); // invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v1 ).append ( p1 ); // invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;
final String v2 = ".Usage of JDK sun.misc.Unsafe is enabled, but it could not be used.Make sure your runtime is configured correctly."; // const-string v2, ".Usage of JDK sun.misc.Unsafe is enabled, but it could not be used.Make sure your runtime is configured correctly."
(( java.lang.StringBuilder ) v1 ).append ( v2 ); // invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v1 ).toString ( ); // invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
/* invoke-direct {v0, v1}, Ljava/lang/UnsupportedOperationException;-><init>(Ljava/lang/String;)V */
/* throw v0 */
} // .end method
