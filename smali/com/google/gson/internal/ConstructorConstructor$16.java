class com.google.gson.internal.ConstructorConstructor$16 implements com.google.gson.internal.ObjectConstructor {
	 /* .source "ConstructorConstructor.java" */
	 /* # interfaces */
	 /* # annotations */
	 /* .annotation system Ldalvik/annotation/EnclosingMethod; */
	 /* value = Lcom/google/gson/internal/ConstructorConstructor;->newUnsafeAllocator(Ljava/lang/Class;)Lcom/google/gson/internal/ObjectConstructor; */
} // .end annotation
/* .annotation system Ldalvik/annotation/InnerClass; */
/* accessFlags = 0x0 */
/* name = null */
} // .end annotation
/* .annotation system Ldalvik/annotation/Signature; */
/* value = { */
/* "Ljava/lang/Object;", */
/* "Lcom/google/gson/internal/ObjectConstructor<", */
/* "TT;>;" */
/* } */
} // .end annotation
/* # instance fields */
final com.google.gson.internal.ConstructorConstructor this$0; //synthetic
private final com.google.gson.internal.UnsafeAllocator unsafeAllocator;
final java.lang.Class val$rawType; //synthetic
/* # direct methods */
 com.google.gson.internal.ConstructorConstructor$16 ( ) {
/* .locals 0 */
/* .param p1, "this$0" # Lcom/google/gson/internal/ConstructorConstructor; */
/* .line 267 */
this.this$0 = p1;
this.val$rawType = p2;
/* invoke-direct {p0}, Ljava/lang/Object;-><init>()V */
/* .line 268 */
com.google.gson.internal.UnsafeAllocator .create ( );
this.unsafeAllocator = p2;
return;
} // .end method
/* # virtual methods */
public java.lang.Object construct ( ) {
/* .locals 4 */
/* .annotation system Ldalvik/annotation/Signature; */
/* value = { */
/* "()TT;" */
/* } */
} // .end annotation
/* .line 272 */
try { // :try_start_0
v0 = this.unsafeAllocator;
v1 = this.val$rawType;
(( com.google.gson.internal.UnsafeAllocator ) v0 ).newInstance ( v1 ); // invoke-virtual {v0, v1}, Lcom/google/gson/internal/UnsafeAllocator;->newInstance(Ljava/lang/Class;)Ljava/lang/Object;
/* :try_end_0 */
/* .catch Ljava/lang/Exception; {:try_start_0 ..:try_end_0} :catch_0 */
/* .line 273 */
/* .local v0, "newInstance":Ljava/lang/Object;, "TT;" */
/* .line 274 */
} // .end local v0 # "newInstance":Ljava/lang/Object;, "TT;"
/* :catch_0 */
/* move-exception v0 */
/* .line 275 */
/* .local v0, "e":Ljava/lang/Exception; */
/* new-instance v1, Ljava/lang/RuntimeException; */
/* new-instance v2, Ljava/lang/StringBuilder; */
/* invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V */
final String v3 = "Unable to create instance of "; // const-string v3, "Unable to create instance of "
(( java.lang.StringBuilder ) v2 ).append ( v3 ); // invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
v3 = this.val$rawType;
(( java.lang.StringBuilder ) v2 ).append ( v3 ); // invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;
final String v3 = ".Registering an InstanceCreator or a TypeAdapter for this type, or adding a no-args constructor may fix this problem."; // const-string v3, ".Registering an InstanceCreator or a TypeAdapter for this type, or adding a no-args constructor may fix this problem."
(( java.lang.StringBuilder ) v2 ).append ( v3 ); // invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v2 ).toString ( ); // invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
/* invoke-direct {v1, v2, v0}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;Ljava/lang/Throwable;)V */
/* throw v1 */
} // .end method
