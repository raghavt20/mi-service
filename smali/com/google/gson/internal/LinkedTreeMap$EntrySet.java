class com.google.gson.internal.LinkedTreeMap$EntrySet extends java.util.AbstractSet {
	 /* .source "LinkedTreeMap.java" */
	 /* # annotations */
	 /* .annotation system Ldalvik/annotation/EnclosingClass; */
	 /* value = Lcom/google/gson/internal/LinkedTreeMap; */
} // .end annotation
/* .annotation system Ldalvik/annotation/InnerClass; */
/* accessFlags = 0x0 */
/* name = "EntrySet" */
} // .end annotation
/* .annotation system Ldalvik/annotation/Signature; */
/* value = { */
/* "Ljava/util/AbstractSet<", */
/* "Ljava/util/Map$Entry<", */
/* "TK;TV;>;>;" */
/* } */
} // .end annotation
/* # instance fields */
final com.google.gson.internal.LinkedTreeMap this$0; //synthetic
/* # direct methods */
 com.google.gson.internal.LinkedTreeMap$EntrySet ( ) {
/* .locals 0 */
/* .param p1, "this$0" # Lcom/google/gson/internal/LinkedTreeMap; */
/* .line 563 */
/* .local p0, "this":Lcom/google/gson/internal/LinkedTreeMap$EntrySet;, "Lcom/google/gson/internal/LinkedTreeMap<TK;TV;>.EntrySet;" */
this.this$0 = p1;
/* invoke-direct {p0}, Ljava/util/AbstractSet;-><init>()V */
return;
} // .end method
/* # virtual methods */
public void clear ( ) {
/* .locals 1 */
/* .line 594 */
/* .local p0, "this":Lcom/google/gson/internal/LinkedTreeMap$EntrySet;, "Lcom/google/gson/internal/LinkedTreeMap<TK;TV;>.EntrySet;" */
v0 = this.this$0;
(( com.google.gson.internal.LinkedTreeMap ) v0 ).clear ( ); // invoke-virtual {v0}, Lcom/google/gson/internal/LinkedTreeMap;->clear()V
/* .line 595 */
return;
} // .end method
public Boolean contains ( java.lang.Object p0 ) {
/* .locals 2 */
/* .param p1, "o" # Ljava/lang/Object; */
/* .line 577 */
/* .local p0, "this":Lcom/google/gson/internal/LinkedTreeMap$EntrySet;, "Lcom/google/gson/internal/LinkedTreeMap<TK;TV;>.EntrySet;" */
/* instance-of v0, p1, Ljava/util/Map$Entry; */
if ( v0 != null) { // if-eqz v0, :cond_0
v0 = this.this$0;
/* move-object v1, p1 */
/* check-cast v1, Ljava/util/Map$Entry; */
(( com.google.gson.internal.LinkedTreeMap ) v0 ).findByEntry ( v1 ); // invoke-virtual {v0, v1}, Lcom/google/gson/internal/LinkedTreeMap;->findByEntry(Ljava/util/Map$Entry;)Lcom/google/gson/internal/LinkedTreeMap$Node;
if ( v0 != null) { // if-eqz v0, :cond_0
	 int v0 = 1; // const/4 v0, 0x1
} // :cond_0
int v0 = 0; // const/4 v0, 0x0
} // :goto_0
} // .end method
public java.util.Iterator iterator ( ) {
/* .locals 1 */
/* .annotation system Ldalvik/annotation/Signature; */
/* value = { */
/* "()", */
/* "Ljava/util/Iterator<", */
/* "Ljava/util/Map$Entry<", */
/* "TK;TV;>;>;" */
/* } */
} // .end annotation
/* .line 569 */
/* .local p0, "this":Lcom/google/gson/internal/LinkedTreeMap$EntrySet;, "Lcom/google/gson/internal/LinkedTreeMap<TK;TV;>.EntrySet;" */
/* new-instance v0, Lcom/google/gson/internal/LinkedTreeMap$EntrySet$1; */
/* invoke-direct {v0, p0}, Lcom/google/gson/internal/LinkedTreeMap$EntrySet$1;-><init>(Lcom/google/gson/internal/LinkedTreeMap$EntrySet;)V */
} // .end method
public Boolean remove ( java.lang.Object p0 ) {
/* .locals 3 */
/* .param p1, "o" # Ljava/lang/Object; */
/* .line 581 */
/* .local p0, "this":Lcom/google/gson/internal/LinkedTreeMap$EntrySet;, "Lcom/google/gson/internal/LinkedTreeMap<TK;TV;>.EntrySet;" */
/* instance-of v0, p1, Ljava/util/Map$Entry; */
int v1 = 0; // const/4 v1, 0x0
/* if-nez v0, :cond_0 */
/* .line 582 */
/* .line 585 */
} // :cond_0
v0 = this.this$0;
/* move-object v2, p1 */
/* check-cast v2, Ljava/util/Map$Entry; */
(( com.google.gson.internal.LinkedTreeMap ) v0 ).findByEntry ( v2 ); // invoke-virtual {v0, v2}, Lcom/google/gson/internal/LinkedTreeMap;->findByEntry(Ljava/util/Map$Entry;)Lcom/google/gson/internal/LinkedTreeMap$Node;
/* .line 586 */
/* .local v0, "node":Lcom/google/gson/internal/LinkedTreeMap$Node;, "Lcom/google/gson/internal/LinkedTreeMap$Node<TK;TV;>;" */
/* if-nez v0, :cond_1 */
/* .line 587 */
/* .line 589 */
} // :cond_1
v1 = this.this$0;
int v2 = 1; // const/4 v2, 0x1
(( com.google.gson.internal.LinkedTreeMap ) v1 ).removeInternal ( v0, v2 ); // invoke-virtual {v1, v0, v2}, Lcom/google/gson/internal/LinkedTreeMap;->removeInternal(Lcom/google/gson/internal/LinkedTreeMap$Node;Z)V
/* .line 590 */
} // .end method
public Integer size ( ) {
/* .locals 1 */
/* .line 565 */
/* .local p0, "this":Lcom/google/gson/internal/LinkedTreeMap$EntrySet;, "Lcom/google/gson/internal/LinkedTreeMap<TK;TV;>.EntrySet;" */
v0 = this.this$0;
/* iget v0, v0, Lcom/google/gson/internal/LinkedTreeMap;->size:I */
} // .end method
