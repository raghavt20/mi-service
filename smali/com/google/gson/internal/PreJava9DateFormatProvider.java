public class com.google.gson.internal.PreJava9DateFormatProvider {
	 /* .source "PreJava9DateFormatProvider.java" */
	 /* # direct methods */
	 public com.google.gson.internal.PreJava9DateFormatProvider ( ) {
		 /* .locals 0 */
		 /* .line 25 */
		 /* invoke-direct {p0}, Ljava/lang/Object;-><init>()V */
		 return;
	 } // .end method
	 private static java.lang.String getDateFormatPattern ( Integer p0 ) {
		 /* .locals 3 */
		 /* .param p0, "style" # I */
		 /* .line 44 */
		 /* packed-switch p0, :pswitch_data_0 */
		 /* .line 54 */
		 /* new-instance v0, Ljava/lang/IllegalArgumentException; */
		 /* new-instance v1, Ljava/lang/StringBuilder; */
		 /* invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V */
		 final String v2 = "Unknown DateFormat style: "; // const-string v2, "Unknown DateFormat style: "
		 (( java.lang.StringBuilder ) v1 ).append ( v2 ); // invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
		 (( java.lang.StringBuilder ) v1 ).append ( p0 ); // invoke-virtual {v1, p0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;
		 (( java.lang.StringBuilder ) v1 ).toString ( ); // invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
		 /* invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V */
		 /* throw v0 */
		 /* .line 46 */
		 /* :pswitch_0 */
		 final String v0 = "M/d/yy"; // const-string v0, "M/d/yy"
		 /* .line 48 */
		 /* :pswitch_1 */
		 final String v0 = "MMM d, y"; // const-string v0, "MMM d, y"
		 /* .line 50 */
		 /* :pswitch_2 */
		 final String v0 = "MMMM d, y"; // const-string v0, "MMMM d, y"
		 /* .line 52 */
		 /* :pswitch_3 */
		 final String v0 = "EEEE, MMMM d, y"; // const-string v0, "EEEE, MMMM d, y"
		 /* :pswitch_data_0 */
		 /* .packed-switch 0x0 */
		 /* :pswitch_3 */
		 /* :pswitch_2 */
		 /* :pswitch_1 */
		 /* :pswitch_0 */
	 } // .end packed-switch
} // .end method
private static java.lang.String getDatePartOfDateTimePattern ( Integer p0 ) {
	 /* .locals 3 */
	 /* .param p0, "dateStyle" # I */
	 /* .line 59 */
	 /* packed-switch p0, :pswitch_data_0 */
	 /* .line 69 */
	 /* new-instance v0, Ljava/lang/IllegalArgumentException; */
	 /* new-instance v1, Ljava/lang/StringBuilder; */
	 /* invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V */
	 final String v2 = "Unknown DateFormat style: "; // const-string v2, "Unknown DateFormat style: "
	 (( java.lang.StringBuilder ) v1 ).append ( v2 ); // invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
	 (( java.lang.StringBuilder ) v1 ).append ( p0 ); // invoke-virtual {v1, p0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;
	 (( java.lang.StringBuilder ) v1 ).toString ( ); // invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
	 /* invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V */
	 /* throw v0 */
	 /* .line 61 */
	 /* :pswitch_0 */
	 final String v0 = "M/d/yy"; // const-string v0, "M/d/yy"
	 /* .line 63 */
	 /* :pswitch_1 */
	 final String v0 = "MMM d, yyyy"; // const-string v0, "MMM d, yyyy"
	 /* .line 65 */
	 /* :pswitch_2 */
	 final String v0 = "MMMM d, yyyy"; // const-string v0, "MMMM d, yyyy"
	 /* .line 67 */
	 /* :pswitch_3 */
	 final String v0 = "EEEE, MMMM d, yyyy"; // const-string v0, "EEEE, MMMM d, yyyy"
	 /* :pswitch_data_0 */
	 /* .packed-switch 0x0 */
	 /* :pswitch_3 */
	 /* :pswitch_2 */
	 /* :pswitch_1 */
	 /* :pswitch_0 */
} // .end packed-switch
} // .end method
private static java.lang.String getTimePartOfDateTimePattern ( Integer p0 ) {
/* .locals 3 */
/* .param p0, "timeStyle" # I */
/* .line 74 */
/* packed-switch p0, :pswitch_data_0 */
/* .line 83 */
/* new-instance v0, Ljava/lang/IllegalArgumentException; */
/* new-instance v1, Ljava/lang/StringBuilder; */
/* invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V */
final String v2 = "Unknown DateFormat style: "; // const-string v2, "Unknown DateFormat style: "
(( java.lang.StringBuilder ) v1 ).append ( v2 ); // invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v1 ).append ( p0 ); // invoke-virtual {v1, p0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v1 ).toString ( ); // invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
/* invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V */
/* throw v0 */
/* .line 76 */
/* :pswitch_0 */
final String v0 = "h:mm a"; // const-string v0, "h:mm a"
/* .line 78 */
/* :pswitch_1 */
final String v0 = "h:mm:ss a"; // const-string v0, "h:mm:ss a"
/* .line 81 */
/* :pswitch_2 */
final String v0 = "h:mm:ss a z"; // const-string v0, "h:mm:ss a z"
/* nop */
/* :pswitch_data_0 */
/* .packed-switch 0x0 */
/* :pswitch_2 */
/* :pswitch_2 */
/* :pswitch_1 */
/* :pswitch_0 */
} // .end packed-switch
} // .end method
public static java.text.DateFormat getUSDateFormat ( Integer p0 ) {
/* .locals 3 */
/* .param p0, "style" # I */
/* .line 31 */
/* new-instance v0, Ljava/text/SimpleDateFormat; */
com.google.gson.internal.PreJava9DateFormatProvider .getDateFormatPattern ( p0 );
v2 = java.util.Locale.US;
/* invoke-direct {v0, v1, v2}, Ljava/text/SimpleDateFormat;-><init>(Ljava/lang/String;Ljava/util/Locale;)V */
} // .end method
public static java.text.DateFormat getUSDateTimeFormat ( Integer p0, Integer p1 ) {
/* .locals 3 */
/* .param p0, "dateStyle" # I */
/* .param p1, "timeStyle" # I */
/* .line 39 */
/* new-instance v0, Ljava/lang/StringBuilder; */
/* invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V */
com.google.gson.internal.PreJava9DateFormatProvider .getDatePartOfDateTimePattern ( p0 );
(( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
final String v1 = " "; // const-string v1, " "
(( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
com.google.gson.internal.PreJava9DateFormatProvider .getTimePartOfDateTimePattern ( p1 );
(( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v0 ).toString ( ); // invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
/* .line 40 */
/* .local v0, "pattern":Ljava/lang/String; */
/* new-instance v1, Ljava/text/SimpleDateFormat; */
v2 = java.util.Locale.US;
/* invoke-direct {v1, v0, v2}, Ljava/text/SimpleDateFormat;-><init>(Ljava/lang/String;Ljava/util/Locale;)V */
} // .end method
