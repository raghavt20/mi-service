class com.google.gson.internal.UnsafeAllocator$2 extends com.google.gson.internal.UnsafeAllocator {
	 /* .source "UnsafeAllocator.java" */
	 /* # annotations */
	 /* .annotation system Ldalvik/annotation/EnclosingMethod; */
	 /* value = Lcom/google/gson/internal/UnsafeAllocator;->create()Lcom/google/gson/internal/UnsafeAllocator; */
} // .end annotation
/* .annotation system Ldalvik/annotation/InnerClass; */
/* accessFlags = 0x0 */
/* name = null */
} // .end annotation
/* # instance fields */
final Integer val$constructorId; //synthetic
final java.lang.reflect.Method val$newInstance; //synthetic
/* # direct methods */
 com.google.gson.internal.UnsafeAllocator$2 ( ) {
/* .locals 0 */
/* .line 69 */
this.val$newInstance = p1;
/* iput p2, p0, Lcom/google/gson/internal/UnsafeAllocator$2;->val$constructorId:I */
/* invoke-direct {p0}, Lcom/google/gson/internal/UnsafeAllocator;-><init>()V */
return;
} // .end method
/* # virtual methods */
public java.lang.Object newInstance ( java.lang.Class p0 ) {
/* .locals 3 */
/* .annotation system Ldalvik/annotation/Signature; */
/* value = { */
/* "<T:", */
/* "Ljava/lang/Object;", */
/* ">(", */
/* "Ljava/lang/Class<", */
/* "TT;>;)TT;" */
/* } */
} // .end annotation
/* .annotation system Ldalvik/annotation/Throws; */
/* value = { */
/* Ljava/lang/Exception; */
/* } */
} // .end annotation
/* .line 73 */
/* .local p1, "c":Ljava/lang/Class;, "Ljava/lang/Class<TT;>;" */
com.google.gson.internal.UnsafeAllocator$2 .assertInstantiable ( p1 );
/* .line 74 */
v0 = this.val$newInstance;
/* iget v1, p0, Lcom/google/gson/internal/UnsafeAllocator$2;->val$constructorId:I */
java.lang.Integer .valueOf ( v1 );
/* filled-new-array {p1, v1}, [Ljava/lang/Object; */
int v2 = 0; // const/4 v2, 0x0
(( java.lang.reflect.Method ) v0 ).invoke ( v2, v1 ); // invoke-virtual {v0, v2, v1}, Ljava/lang/reflect/Method;->invoke(Ljava/lang/Object;[Ljava/lang/Object;)Ljava/lang/Object;
} // .end method
