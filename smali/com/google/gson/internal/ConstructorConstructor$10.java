class com.google.gson.internal.ConstructorConstructor$10 implements com.google.gson.internal.ObjectConstructor {
	 /* .source "ConstructorConstructor.java" */
	 /* # interfaces */
	 /* # annotations */
	 /* .annotation system Ldalvik/annotation/EnclosingMethod; */
	 /* value = Lcom/google/gson/internal/ConstructorConstructor;->newDefaultImplementationConstructor(Ljava/lang/reflect/Type;Ljava/lang/Class;)Lcom/google/gson/internal/ObjectConstructor; */
} // .end annotation
/* .annotation system Ldalvik/annotation/InnerClass; */
/* accessFlags = 0x0 */
/* name = null */
} // .end annotation
/* .annotation system Ldalvik/annotation/Signature; */
/* value = { */
/* "Ljava/lang/Object;", */
/* "Lcom/google/gson/internal/ObjectConstructor<", */
/* "TT;>;" */
/* } */
} // .end annotation
/* # instance fields */
final com.google.gson.internal.ConstructorConstructor this$0; //synthetic
final java.lang.reflect.Type val$type; //synthetic
/* # direct methods */
 com.google.gson.internal.ConstructorConstructor$10 ( ) {
/* .locals 0 */
/* .param p1, "this$0" # Lcom/google/gson/internal/ConstructorConstructor; */
/* .line 212 */
this.this$0 = p1;
this.val$type = p2;
/* invoke-direct {p0}, Ljava/lang/Object;-><init>()V */
return;
} // .end method
/* # virtual methods */
public java.lang.Object construct ( ) {
/* .locals 4 */
/* .annotation system Ldalvik/annotation/Signature; */
/* value = { */
/* "()TT;" */
/* } */
} // .end annotation
/* .line 214 */
v0 = this.val$type;
/* instance-of v1, v0, Ljava/lang/reflect/ParameterizedType; */
final String v2 = "Invalid EnumMap type: "; // const-string v2, "Invalid EnumMap type: "
if ( v1 != null) { // if-eqz v1, :cond_1
/* .line 215 */
/* check-cast v0, Ljava/lang/reflect/ParameterizedType; */
int v1 = 0; // const/4 v1, 0x0
/* aget-object v0, v0, v1 */
/* .line 216 */
/* .local v0, "elementType":Ljava/lang/reflect/Type; */
/* instance-of v1, v0, Ljava/lang/Class; */
if ( v1 != null) { // if-eqz v1, :cond_0
/* .line 218 */
/* new-instance v1, Ljava/util/EnumMap; */
/* move-object v2, v0 */
/* check-cast v2, Ljava/lang/Class; */
/* invoke-direct {v1, v2}, Ljava/util/EnumMap;-><init>(Ljava/lang/Class;)V */
/* .line 219 */
/* .local v1, "map":Ljava/lang/Object;, "TT;" */
/* .line 221 */
} // .end local v1 # "map":Ljava/lang/Object;, "TT;"
} // :cond_0
/* new-instance v1, Lcom/google/gson/JsonIOException; */
/* new-instance v3, Ljava/lang/StringBuilder; */
/* invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V */
(( java.lang.StringBuilder ) v3 ).append ( v2 ); // invoke-virtual {v3, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
v3 = this.val$type;
(( java.lang.Object ) v3 ).toString ( ); // invoke-virtual {v3}, Ljava/lang/Object;->toString()Ljava/lang/String;
(( java.lang.StringBuilder ) v2 ).append ( v3 ); // invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v2 ).toString ( ); // invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
/* invoke-direct {v1, v2}, Lcom/google/gson/JsonIOException;-><init>(Ljava/lang/String;)V */
/* throw v1 */
/* .line 224 */
} // .end local v0 # "elementType":Ljava/lang/reflect/Type;
} // :cond_1
/* new-instance v0, Lcom/google/gson/JsonIOException; */
/* new-instance v1, Ljava/lang/StringBuilder; */
/* invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V */
(( java.lang.StringBuilder ) v1 ).append ( v2 ); // invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
v2 = this.val$type;
(( java.lang.Object ) v2 ).toString ( ); // invoke-virtual {v2}, Ljava/lang/Object;->toString()Ljava/lang/String;
(( java.lang.StringBuilder ) v1 ).append ( v2 ); // invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v1 ).toString ( ); // invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
/* invoke-direct {v0, v1}, Lcom/google/gson/JsonIOException;-><init>(Ljava/lang/String;)V */
/* throw v0 */
} // .end method
