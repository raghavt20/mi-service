class com.google.gson.Gson$4 extends com.google.gson.TypeAdapter {
	 /* .source "Gson.java" */
	 /* # annotations */
	 /* .annotation system Ldalvik/annotation/EnclosingMethod; */
	 /* value = Lcom/google/gson/Gson;->atomicLongAdapter(Lcom/google/gson/TypeAdapter;)Lcom/google/gson/TypeAdapter; */
} // .end annotation
/* .annotation system Ldalvik/annotation/InnerClass; */
/* accessFlags = 0x0 */
/* name = null */
} // .end annotation
/* .annotation system Ldalvik/annotation/Signature; */
/* value = { */
/* "Lcom/google/gson/TypeAdapter<", */
/* "Ljava/util/concurrent/atomic/AtomicLong;", */
/* ">;" */
/* } */
} // .end annotation
/* # instance fields */
final com.google.gson.TypeAdapter val$longAdapter; //synthetic
/* # direct methods */
 com.google.gson.Gson$4 ( ) {
/* .locals 0 */
/* .line 431 */
this.val$longAdapter = p1;
/* invoke-direct {p0}, Lcom/google/gson/TypeAdapter;-><init>()V */
return;
} // .end method
/* # virtual methods */
public java.lang.Object read ( com.google.gson.stream.JsonReader p0 ) { //bridge//synthethic
/* .locals 0 */
/* .annotation system Ldalvik/annotation/Throws; */
/* value = { */
/* Ljava/io/IOException; */
/* } */
} // .end annotation
/* .line 431 */
(( com.google.gson.Gson$4 ) p0 ).read ( p1 ); // invoke-virtual {p0, p1}, Lcom/google/gson/Gson$4;->read(Lcom/google/gson/stream/JsonReader;)Ljava/util/concurrent/atomic/AtomicLong;
} // .end method
public java.util.concurrent.atomic.AtomicLong read ( com.google.gson.stream.JsonReader p0 ) {
/* .locals 4 */
/* .param p1, "in" # Lcom/google/gson/stream/JsonReader; */
/* .annotation system Ldalvik/annotation/Throws; */
/* value = { */
/* Ljava/io/IOException; */
/* } */
} // .end annotation
/* .line 436 */
v0 = this.val$longAdapter;
(( com.google.gson.TypeAdapter ) v0 ).read ( p1 ); // invoke-virtual {v0, p1}, Lcom/google/gson/TypeAdapter;->read(Lcom/google/gson/stream/JsonReader;)Ljava/lang/Object;
/* check-cast v0, Ljava/lang/Number; */
/* .line 437 */
/* .local v0, "value":Ljava/lang/Number; */
/* new-instance v1, Ljava/util/concurrent/atomic/AtomicLong; */
(( java.lang.Number ) v0 ).longValue ( ); // invoke-virtual {v0}, Ljava/lang/Number;->longValue()J
/* move-result-wide v2 */
/* invoke-direct {v1, v2, v3}, Ljava/util/concurrent/atomic/AtomicLong;-><init>(J)V */
} // .end method
public void write ( com.google.gson.stream.JsonWriter p0, java.lang.Object p1 ) { //bridge//synthethic
/* .locals 0 */
/* .annotation system Ldalvik/annotation/Throws; */
/* value = { */
/* Ljava/io/IOException; */
/* } */
} // .end annotation
/* .line 431 */
/* check-cast p2, Ljava/util/concurrent/atomic/AtomicLong; */
(( com.google.gson.Gson$4 ) p0 ).write ( p1, p2 ); // invoke-virtual {p0, p1, p2}, Lcom/google/gson/Gson$4;->write(Lcom/google/gson/stream/JsonWriter;Ljava/util/concurrent/atomic/AtomicLong;)V
return;
} // .end method
public void write ( com.google.gson.stream.JsonWriter p0, java.util.concurrent.atomic.AtomicLong p1 ) {
/* .locals 3 */
/* .param p1, "out" # Lcom/google/gson/stream/JsonWriter; */
/* .param p2, "value" # Ljava/util/concurrent/atomic/AtomicLong; */
/* .annotation system Ldalvik/annotation/Throws; */
/* value = { */
/* Ljava/io/IOException; */
/* } */
} // .end annotation
/* .line 433 */
v0 = this.val$longAdapter;
(( java.util.concurrent.atomic.AtomicLong ) p2 ).get ( ); // invoke-virtual {p2}, Ljava/util/concurrent/atomic/AtomicLong;->get()J
/* move-result-wide v1 */
java.lang.Long .valueOf ( v1,v2 );
(( com.google.gson.TypeAdapter ) v0 ).write ( p1, v1 ); // invoke-virtual {v0, p1, v1}, Lcom/google/gson/TypeAdapter;->write(Lcom/google/gson/stream/JsonWriter;Ljava/lang/Object;)V
/* .line 434 */
return;
} // .end method
