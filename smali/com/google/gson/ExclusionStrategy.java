public abstract class com.google.gson.ExclusionStrategy {
	 /* .source "ExclusionStrategy.java" */
	 /* # virtual methods */
	 public abstract Boolean shouldSkipClass ( java.lang.Class p0 ) {
		 /* .annotation system Ldalvik/annotation/Signature; */
		 /* value = { */
		 /* "(", */
		 /* "Ljava/lang/Class<", */
		 /* "*>;)Z" */
		 /* } */
	 } // .end annotation
} // .end method
public abstract Boolean shouldSkipField ( com.google.gson.FieldAttributes p0 ) {
} // .end method
