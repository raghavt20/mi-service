class com.google.gson.Gson$5 extends com.google.gson.TypeAdapter {
	 /* .source "Gson.java" */
	 /* # annotations */
	 /* .annotation system Ldalvik/annotation/EnclosingMethod; */
	 /* value = Lcom/google/gson/Gson;->atomicLongArrayAdapter(Lcom/google/gson/TypeAdapter;)Lcom/google/gson/TypeAdapter; */
} // .end annotation
/* .annotation system Ldalvik/annotation/InnerClass; */
/* accessFlags = 0x0 */
/* name = null */
} // .end annotation
/* .annotation system Ldalvik/annotation/Signature; */
/* value = { */
/* "Lcom/google/gson/TypeAdapter<", */
/* "Ljava/util/concurrent/atomic/AtomicLongArray;", */
/* ">;" */
/* } */
} // .end annotation
/* # instance fields */
final com.google.gson.TypeAdapter val$longAdapter; //synthetic
/* # direct methods */
 com.google.gson.Gson$5 ( ) {
/* .locals 0 */
/* .line 443 */
this.val$longAdapter = p1;
/* invoke-direct {p0}, Lcom/google/gson/TypeAdapter;-><init>()V */
return;
} // .end method
/* # virtual methods */
public java.lang.Object read ( com.google.gson.stream.JsonReader p0 ) { //bridge//synthethic
/* .locals 0 */
/* .annotation system Ldalvik/annotation/Throws; */
/* value = { */
/* Ljava/io/IOException; */
/* } */
} // .end annotation
/* .line 443 */
(( com.google.gson.Gson$5 ) p0 ).read ( p1 ); // invoke-virtual {p0, p1}, Lcom/google/gson/Gson$5;->read(Lcom/google/gson/stream/JsonReader;)Ljava/util/concurrent/atomic/AtomicLongArray;
} // .end method
public java.util.concurrent.atomic.AtomicLongArray read ( com.google.gson.stream.JsonReader p0 ) {
/* .locals 6 */
/* .param p1, "in" # Lcom/google/gson/stream/JsonReader; */
/* .annotation system Ldalvik/annotation/Throws; */
/* value = { */
/* Ljava/io/IOException; */
/* } */
} // .end annotation
/* .line 452 */
/* new-instance v0, Ljava/util/ArrayList; */
/* invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V */
/* .line 453 */
/* .local v0, "list":Ljava/util/List;, "Ljava/util/List<Ljava/lang/Long;>;" */
(( com.google.gson.stream.JsonReader ) p1 ).beginArray ( ); // invoke-virtual {p1}, Lcom/google/gson/stream/JsonReader;->beginArray()V
/* .line 454 */
} // :goto_0
v1 = (( com.google.gson.stream.JsonReader ) p1 ).hasNext ( ); // invoke-virtual {p1}, Lcom/google/gson/stream/JsonReader;->hasNext()Z
if ( v1 != null) { // if-eqz v1, :cond_0
/* .line 455 */
v1 = this.val$longAdapter;
(( com.google.gson.TypeAdapter ) v1 ).read ( p1 ); // invoke-virtual {v1, p1}, Lcom/google/gson/TypeAdapter;->read(Lcom/google/gson/stream/JsonReader;)Ljava/lang/Object;
/* check-cast v1, Ljava/lang/Number; */
(( java.lang.Number ) v1 ).longValue ( ); // invoke-virtual {v1}, Ljava/lang/Number;->longValue()J
/* move-result-wide v1 */
/* .line 456 */
/* .local v1, "value":J */
java.lang.Long .valueOf ( v1,v2 );
/* .line 457 */
} // .end local v1 # "value":J
/* .line 458 */
} // :cond_0
(( com.google.gson.stream.JsonReader ) p1 ).endArray ( ); // invoke-virtual {p1}, Lcom/google/gson/stream/JsonReader;->endArray()V
v1 = /* .line 459 */
/* .line 460 */
/* .local v1, "length":I */
/* new-instance v2, Ljava/util/concurrent/atomic/AtomicLongArray; */
/* invoke-direct {v2, v1}, Ljava/util/concurrent/atomic/AtomicLongArray;-><init>(I)V */
/* .line 461 */
/* .local v2, "array":Ljava/util/concurrent/atomic/AtomicLongArray; */
int v3 = 0; // const/4 v3, 0x0
/* .local v3, "i":I */
} // :goto_1
/* if-ge v3, v1, :cond_1 */
/* .line 462 */
/* check-cast v4, Ljava/lang/Long; */
(( java.lang.Long ) v4 ).longValue ( ); // invoke-virtual {v4}, Ljava/lang/Long;->longValue()J
/* move-result-wide v4 */
(( java.util.concurrent.atomic.AtomicLongArray ) v2 ).set ( v3, v4, v5 ); // invoke-virtual {v2, v3, v4, v5}, Ljava/util/concurrent/atomic/AtomicLongArray;->set(IJ)V
/* .line 461 */
/* add-int/lit8 v3, v3, 0x1 */
/* .line 464 */
} // .end local v3 # "i":I
} // :cond_1
} // .end method
public void write ( com.google.gson.stream.JsonWriter p0, java.lang.Object p1 ) { //bridge//synthethic
/* .locals 0 */
/* .annotation system Ldalvik/annotation/Throws; */
/* value = { */
/* Ljava/io/IOException; */
/* } */
} // .end annotation
/* .line 443 */
/* check-cast p2, Ljava/util/concurrent/atomic/AtomicLongArray; */
(( com.google.gson.Gson$5 ) p0 ).write ( p1, p2 ); // invoke-virtual {p0, p1, p2}, Lcom/google/gson/Gson$5;->write(Lcom/google/gson/stream/JsonWriter;Ljava/util/concurrent/atomic/AtomicLongArray;)V
return;
} // .end method
public void write ( com.google.gson.stream.JsonWriter p0, java.util.concurrent.atomic.AtomicLongArray p1 ) {
/* .locals 5 */
/* .param p1, "out" # Lcom/google/gson/stream/JsonWriter; */
/* .param p2, "value" # Ljava/util/concurrent/atomic/AtomicLongArray; */
/* .annotation system Ldalvik/annotation/Throws; */
/* value = { */
/* Ljava/io/IOException; */
/* } */
} // .end annotation
/* .line 445 */
(( com.google.gson.stream.JsonWriter ) p1 ).beginArray ( ); // invoke-virtual {p1}, Lcom/google/gson/stream/JsonWriter;->beginArray()Lcom/google/gson/stream/JsonWriter;
/* .line 446 */
int v0 = 0; // const/4 v0, 0x0
/* .local v0, "i":I */
v1 = (( java.util.concurrent.atomic.AtomicLongArray ) p2 ).length ( ); // invoke-virtual {p2}, Ljava/util/concurrent/atomic/AtomicLongArray;->length()I
/* .local v1, "length":I */
} // :goto_0
/* if-ge v0, v1, :cond_0 */
/* .line 447 */
v2 = this.val$longAdapter;
(( java.util.concurrent.atomic.AtomicLongArray ) p2 ).get ( v0 ); // invoke-virtual {p2, v0}, Ljava/util/concurrent/atomic/AtomicLongArray;->get(I)J
/* move-result-wide v3 */
java.lang.Long .valueOf ( v3,v4 );
(( com.google.gson.TypeAdapter ) v2 ).write ( p1, v3 ); // invoke-virtual {v2, p1, v3}, Lcom/google/gson/TypeAdapter;->write(Lcom/google/gson/stream/JsonWriter;Ljava/lang/Object;)V
/* .line 446 */
/* add-int/lit8 v0, v0, 0x1 */
/* .line 449 */
} // .end local v0 # "i":I
} // .end local v1 # "length":I
} // :cond_0
(( com.google.gson.stream.JsonWriter ) p1 ).endArray ( ); // invoke-virtual {p1}, Lcom/google/gson/stream/JsonWriter;->endArray()Lcom/google/gson/stream/JsonWriter;
/* .line 450 */
return;
} // .end method
