public abstract class com.google.gson.JsonSerializer {
	 /* .source "JsonSerializer.java" */
	 /* # annotations */
	 /* .annotation system Ldalvik/annotation/Signature; */
	 /* value = { */
	 /* "<T:", */
	 /* "Ljava/lang/Object;", */
	 /* ">", */
	 /* "Ljava/lang/Object;" */
	 /* } */
} // .end annotation
/* # virtual methods */
public abstract com.google.gson.JsonElement serialize ( java.lang.Object p0, java.lang.reflect.Type p1, com.google.gson.JsonSerializationContext p2 ) {
	 /* .annotation system Ldalvik/annotation/Signature; */
	 /* value = { */
	 /* "(TT;", */
	 /* "Ljava/lang/reflect/Type;", */
	 /* "Lcom/google/gson/JsonSerializationContext;", */
	 /* ")", */
	 /* "Lcom/google/gson/JsonElement;" */
	 /* } */
} // .end annotation
} // .end method
