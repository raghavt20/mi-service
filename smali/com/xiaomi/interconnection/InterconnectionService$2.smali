.class Lcom/xiaomi/interconnection/InterconnectionService$2;
.super Landroid/content/BroadcastReceiver;
.source "InterconnectionService.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/xiaomi/interconnection/InterconnectionService;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/xiaomi/interconnection/InterconnectionService;


# direct methods
.method constructor <init>(Lcom/xiaomi/interconnection/InterconnectionService;)V
    .locals 0
    .param p1, "this$0"    # Lcom/xiaomi/interconnection/InterconnectionService;

    .line 87
    iput-object p1, p0, Lcom/xiaomi/interconnection/InterconnectionService$2;->this$0:Lcom/xiaomi/interconnection/InterconnectionService;

    invoke-direct {p0}, Landroid/content/BroadcastReceiver;-><init>()V

    return-void
.end method


# virtual methods
.method public onReceive(Landroid/content/Context;Landroid/content/Intent;)V
    .locals 11
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "intent"    # Landroid/content/Intent;

    .line 90
    const-string v0, "android.net.wifi.p2p.CONNECTION_STATE_CHANGE"

    invoke-virtual {p2}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_9

    .line 91
    nop

    .line 92
    const-string v0, "networkInfo"

    invoke-virtual {p2, v0}, Landroid/content/Intent;->getParcelableExtra(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Landroid/net/NetworkInfo;

    .line 93
    .local v0, "networkInfo":Landroid/net/NetworkInfo;
    if-eqz v0, :cond_8

    invoke-virtual {v0}, Landroid/net/NetworkInfo;->isConnected()Z

    move-result v1

    if-nez v1, :cond_0

    goto/16 :goto_4

    .line 95
    :cond_0
    nop

    .line 96
    const-string v1, "p2pGroupInfo"

    invoke-virtual {p2, v1}, Landroid/content/Intent;->getParcelableExtra(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v1

    check-cast v1, Landroid/net/wifi/p2p/WifiP2pGroup;

    .line 97
    .local v1, "wifiP2pGroup":Landroid/net/wifi/p2p/WifiP2pGroup;
    nop

    .line 98
    const-string/jumbo v2, "wifiP2pInfo"

    invoke-virtual {p2, v2}, Landroid/content/Intent;->getParcelableExtra(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v2

    check-cast v2, Landroid/net/wifi/p2p/WifiP2pInfo;

    .line 99
    .local v2, "wifiP2pInfo":Landroid/net/wifi/p2p/WifiP2pInfo;
    if-eqz v1, :cond_7

    if-nez v2, :cond_1

    goto/16 :goto_3

    .line 101
    :cond_1
    invoke-virtual {v1}, Landroid/net/wifi/p2p/WifiP2pGroup;->getInterface()Ljava/lang/String;

    move-result-object v3

    .line 102
    .local v3, "iface":Ljava/lang/String;
    iget-object v4, p0, Lcom/xiaomi/interconnection/InterconnectionService$2;->this$0:Lcom/xiaomi/interconnection/InterconnectionService;

    invoke-static {v3}, Lcom/xiaomi/interconnection/InterconnectionService;->-$$Nest$smgetIfaceMacAddr(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5}, Lcom/xiaomi/interconnection/InterconnectionService;->-$$Nest$fputmDeviceP2pMacAddr(Lcom/xiaomi/interconnection/InterconnectionService;Ljava/lang/String;)V

    .line 104
    invoke-virtual {v1}, Landroid/net/wifi/p2p/WifiP2pGroup;->isGroupOwner()Z

    move-result v4

    const-string v5, ""

    if-eqz v4, :cond_5

    .line 105
    iget-object v4, p0, Lcom/xiaomi/interconnection/InterconnectionService$2;->this$0:Lcom/xiaomi/interconnection/InterconnectionService;

    const/4 v6, 0x1

    invoke-static {v4, v6}, Lcom/xiaomi/interconnection/InterconnectionService;->-$$Nest$fputmIsGo(Lcom/xiaomi/interconnection/InterconnectionService;Z)V

    .line 107
    invoke-virtual {v1}, Landroid/net/wifi/p2p/WifiP2pGroup;->getClientList()Ljava/util/Collection;

    move-result-object v4

    .line 108
    .local v4, "clients":Ljava/util/Collection;, "Ljava/util/Collection<Landroid/net/wifi/p2p/WifiP2pDevice;>;"
    invoke-interface {v4}, Ljava/util/Collection;->size()I

    move-result v6

    .line 109
    .local v6, "clientNum":I
    const-string v7, ""

    .line 110
    .local v7, "macAddr":Ljava/lang/String;
    invoke-interface {v4}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v8

    :goto_0
    invoke-interface {v8}, Ljava/util/Iterator;->hasNext()Z

    move-result v9

    if-eqz v9, :cond_2

    invoke-interface {v8}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v9

    check-cast v9, Landroid/net/wifi/p2p/WifiP2pDevice;

    .line 111
    .local v9, "client":Landroid/net/wifi/p2p/WifiP2pDevice;
    iget-object v7, v9, Landroid/net/wifi/p2p/WifiP2pDevice;->deviceAddress:Ljava/lang/String;

    .line 112
    .end local v9    # "client":Landroid/net/wifi/p2p/WifiP2pDevice;
    goto :goto_0

    .line 113
    :cond_2
    iget-object v8, p0, Lcom/xiaomi/interconnection/InterconnectionService$2;->this$0:Lcom/xiaomi/interconnection/InterconnectionService;

    invoke-static {v8, v7}, Lcom/xiaomi/interconnection/InterconnectionService;->-$$Nest$fputmPeerP2pMacAddr(Lcom/xiaomi/interconnection/InterconnectionService;Ljava/lang/String;)V

    .line 115
    iget-object v8, v2, Landroid/net/wifi/p2p/WifiP2pInfo;->groupOwnerAddress:Ljava/net/InetAddress;

    .line 116
    .local v8, "addr":Ljava/net/InetAddress;
    iget-object v9, p0, Lcom/xiaomi/interconnection/InterconnectionService$2;->this$0:Lcom/xiaomi/interconnection/InterconnectionService;

    if-nez v8, :cond_3

    move-object v10, v5

    goto :goto_1

    :cond_3
    invoke-virtual {v8}, Ljava/net/InetAddress;->getHostAddress()Ljava/lang/String;

    move-result-object v10

    :goto_1
    invoke-static {v9, v10}, Lcom/xiaomi/interconnection/InterconnectionService;->-$$Nest$fputmDeviceP2pIpAddr(Lcom/xiaomi/interconnection/InterconnectionService;Ljava/lang/String;)V

    .line 117
    iget-object v9, p0, Lcom/xiaomi/interconnection/InterconnectionService$2;->this$0:Lcom/xiaomi/interconnection/InterconnectionService;

    invoke-static {v9}, Lcom/xiaomi/interconnection/InterconnectionService;->-$$Nest$fgetmPeerP2pMacAddr(Lcom/xiaomi/interconnection/InterconnectionService;)Ljava/lang/String;

    move-result-object v10

    invoke-static {v9, v10}, Lcom/xiaomi/interconnection/InterconnectionService;->-$$Nest$mgetIpFromArpTable(Lcom/xiaomi/interconnection/InterconnectionService;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v10

    invoke-static {v9, v10}, Lcom/xiaomi/interconnection/InterconnectionService;->-$$Nest$fputmPeerP2pIpAddr(Lcom/xiaomi/interconnection/InterconnectionService;Ljava/lang/String;)V

    .line 118
    iget-object v9, p0, Lcom/xiaomi/interconnection/InterconnectionService$2;->this$0:Lcom/xiaomi/interconnection/InterconnectionService;

    invoke-static {v9}, Lcom/xiaomi/interconnection/InterconnectionService;->-$$Nest$mnotifyWifiP2pInfoChanged(Lcom/xiaomi/interconnection/InterconnectionService;)V

    .line 120
    if-lez v6, :cond_4

    iget-object v9, p0, Lcom/xiaomi/interconnection/InterconnectionService$2;->this$0:Lcom/xiaomi/interconnection/InterconnectionService;

    invoke-static {v9}, Lcom/xiaomi/interconnection/InterconnectionService;->-$$Nest$fgetmPeerP2pIpAddr(Lcom/xiaomi/interconnection/InterconnectionService;)Ljava/lang/String;

    move-result-object v9

    invoke-virtual {v5, v9}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-eqz v5, :cond_4

    .line 121
    iget-object v5, p0, Lcom/xiaomi/interconnection/InterconnectionService$2;->this$0:Lcom/xiaomi/interconnection/InterconnectionService;

    invoke-static {v5}, Lcom/xiaomi/interconnection/InterconnectionService;->-$$Nest$mgetPeerIpFromArpTableUntilTimeout(Lcom/xiaomi/interconnection/InterconnectionService;)V

    .line 123
    .end local v4    # "clients":Ljava/util/Collection;, "Ljava/util/Collection<Landroid/net/wifi/p2p/WifiP2pDevice;>;"
    .end local v6    # "clientNum":I
    .end local v7    # "macAddr":Ljava/lang/String;
    .end local v8    # "addr":Ljava/net/InetAddress;
    :cond_4
    goto :goto_5

    .line 124
    :cond_5
    iget-object v4, p0, Lcom/xiaomi/interconnection/InterconnectionService$2;->this$0:Lcom/xiaomi/interconnection/InterconnectionService;

    const/4 v6, 0x0

    invoke-static {v4, v6}, Lcom/xiaomi/interconnection/InterconnectionService;->-$$Nest$fputmIsGo(Lcom/xiaomi/interconnection/InterconnectionService;Z)V

    .line 126
    invoke-virtual {v1}, Landroid/net/wifi/p2p/WifiP2pGroup;->getOwner()Landroid/net/wifi/p2p/WifiP2pDevice;

    move-result-object v4

    .line 127
    .local v4, "owner":Landroid/net/wifi/p2p/WifiP2pDevice;
    iget-object v6, p0, Lcom/xiaomi/interconnection/InterconnectionService$2;->this$0:Lcom/xiaomi/interconnection/InterconnectionService;

    iget-object v7, v4, Landroid/net/wifi/p2p/WifiP2pDevice;->deviceAddress:Ljava/lang/String;

    invoke-static {v6, v7}, Lcom/xiaomi/interconnection/InterconnectionService;->-$$Nest$fputmPeerP2pMacAddr(Lcom/xiaomi/interconnection/InterconnectionService;Ljava/lang/String;)V

    .line 129
    iget-object v6, p0, Lcom/xiaomi/interconnection/InterconnectionService$2;->this$0:Lcom/xiaomi/interconnection/InterconnectionService;

    invoke-static {v6, v3}, Lcom/xiaomi/interconnection/InterconnectionService;->-$$Nest$mgetIfaceIpAddr(Lcom/xiaomi/interconnection/InterconnectionService;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v7

    invoke-static {v6, v7}, Lcom/xiaomi/interconnection/InterconnectionService;->-$$Nest$fputmDeviceP2pIpAddr(Lcom/xiaomi/interconnection/InterconnectionService;Ljava/lang/String;)V

    .line 130
    iget-object v6, v2, Landroid/net/wifi/p2p/WifiP2pInfo;->groupOwnerAddress:Ljava/net/InetAddress;

    .line 131
    .local v6, "addr":Ljava/net/InetAddress;
    iget-object v7, p0, Lcom/xiaomi/interconnection/InterconnectionService$2;->this$0:Lcom/xiaomi/interconnection/InterconnectionService;

    if-nez v6, :cond_6

    move-object v8, v5

    goto :goto_2

    :cond_6
    invoke-virtual {v6}, Ljava/net/InetAddress;->getHostAddress()Ljava/lang/String;

    move-result-object v8

    :goto_2
    invoke-static {v7, v8}, Lcom/xiaomi/interconnection/InterconnectionService;->-$$Nest$fputmPeerP2pIpAddr(Lcom/xiaomi/interconnection/InterconnectionService;Ljava/lang/String;)V

    .line 132
    iget-object v7, p0, Lcom/xiaomi/interconnection/InterconnectionService$2;->this$0:Lcom/xiaomi/interconnection/InterconnectionService;

    invoke-static {v7}, Lcom/xiaomi/interconnection/InterconnectionService;->-$$Nest$mnotifyWifiP2pInfoChanged(Lcom/xiaomi/interconnection/InterconnectionService;)V

    .line 134
    iget-object v7, p0, Lcom/xiaomi/interconnection/InterconnectionService$2;->this$0:Lcom/xiaomi/interconnection/InterconnectionService;

    invoke-static {v7}, Lcom/xiaomi/interconnection/InterconnectionService;->-$$Nest$fgetmPeerP2pIpAddr(Lcom/xiaomi/interconnection/InterconnectionService;)Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v5, v7}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-eqz v5, :cond_9

    .line 135
    iget-object v5, p0, Lcom/xiaomi/interconnection/InterconnectionService$2;->this$0:Lcom/xiaomi/interconnection/InterconnectionService;

    invoke-static {v5}, Lcom/xiaomi/interconnection/InterconnectionService;->-$$Nest$mgetPeerIpFromArpTableUntilTimeout(Lcom/xiaomi/interconnection/InterconnectionService;)V

    goto :goto_5

    .line 99
    .end local v3    # "iface":Ljava/lang/String;
    .end local v4    # "owner":Landroid/net/wifi/p2p/WifiP2pDevice;
    .end local v6    # "addr":Ljava/net/InetAddress;
    :cond_7
    :goto_3
    return-void

    .line 93
    .end local v1    # "wifiP2pGroup":Landroid/net/wifi/p2p/WifiP2pGroup;
    .end local v2    # "wifiP2pInfo":Landroid/net/wifi/p2p/WifiP2pInfo;
    :cond_8
    :goto_4
    return-void

    .line 139
    .end local v0    # "networkInfo":Landroid/net/NetworkInfo;
    :cond_9
    :goto_5
    return-void
.end method
