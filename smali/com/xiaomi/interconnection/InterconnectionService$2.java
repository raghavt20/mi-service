class com.xiaomi.interconnection.InterconnectionService$2 extends android.content.BroadcastReceiver {
	 /* .source "InterconnectionService.java" */
	 /* # annotations */
	 /* .annotation system Ldalvik/annotation/EnclosingClass; */
	 /* value = Lcom/xiaomi/interconnection/InterconnectionService; */
} // .end annotation
/* .annotation system Ldalvik/annotation/InnerClass; */
/* accessFlags = 0x0 */
/* name = null */
} // .end annotation
/* # instance fields */
final com.xiaomi.interconnection.InterconnectionService this$0; //synthetic
/* # direct methods */
 com.xiaomi.interconnection.InterconnectionService$2 ( ) {
/* .locals 0 */
/* .param p1, "this$0" # Lcom/xiaomi/interconnection/InterconnectionService; */
/* .line 87 */
this.this$0 = p1;
/* invoke-direct {p0}, Landroid/content/BroadcastReceiver;-><init>()V */
return;
} // .end method
/* # virtual methods */
public void onReceive ( android.content.Context p0, android.content.Intent p1 ) {
/* .locals 11 */
/* .param p1, "context" # Landroid/content/Context; */
/* .param p2, "intent" # Landroid/content/Intent; */
/* .line 90 */
final String v0 = "android.net.wifi.p2p.CONNECTION_STATE_CHANGE"; // const-string v0, "android.net.wifi.p2p.CONNECTION_STATE_CHANGE"
(( android.content.Intent ) p2 ).getAction ( ); // invoke-virtual {p2}, Landroid/content/Intent;->getAction()Ljava/lang/String;
v0 = (( java.lang.String ) v0 ).equals ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z
if ( v0 != null) { // if-eqz v0, :cond_9
	 /* .line 91 */
	 /* nop */
	 /* .line 92 */
	 final String v0 = "networkInfo"; // const-string v0, "networkInfo"
	 (( android.content.Intent ) p2 ).getParcelableExtra ( v0 ); // invoke-virtual {p2, v0}, Landroid/content/Intent;->getParcelableExtra(Ljava/lang/String;)Landroid/os/Parcelable;
	 /* check-cast v0, Landroid/net/NetworkInfo; */
	 /* .line 93 */
	 /* .local v0, "networkInfo":Landroid/net/NetworkInfo; */
	 if ( v0 != null) { // if-eqz v0, :cond_8
		 v1 = 		 (( android.net.NetworkInfo ) v0 ).isConnected ( ); // invoke-virtual {v0}, Landroid/net/NetworkInfo;->isConnected()Z
		 /* if-nez v1, :cond_0 */
		 /* goto/16 :goto_4 */
		 /* .line 95 */
	 } // :cond_0
	 /* nop */
	 /* .line 96 */
	 final String v1 = "p2pGroupInfo"; // const-string v1, "p2pGroupInfo"
	 (( android.content.Intent ) p2 ).getParcelableExtra ( v1 ); // invoke-virtual {p2, v1}, Landroid/content/Intent;->getParcelableExtra(Ljava/lang/String;)Landroid/os/Parcelable;
	 /* check-cast v1, Landroid/net/wifi/p2p/WifiP2pGroup; */
	 /* .line 97 */
	 /* .local v1, "wifiP2pGroup":Landroid/net/wifi/p2p/WifiP2pGroup; */
	 /* nop */
	 /* .line 98 */
	 /* const-string/jumbo v2, "wifiP2pInfo" */
	 (( android.content.Intent ) p2 ).getParcelableExtra ( v2 ); // invoke-virtual {p2, v2}, Landroid/content/Intent;->getParcelableExtra(Ljava/lang/String;)Landroid/os/Parcelable;
	 /* check-cast v2, Landroid/net/wifi/p2p/WifiP2pInfo; */
	 /* .line 99 */
	 /* .local v2, "wifiP2pInfo":Landroid/net/wifi/p2p/WifiP2pInfo; */
	 if ( v1 != null) { // if-eqz v1, :cond_7
		 /* if-nez v2, :cond_1 */
		 /* goto/16 :goto_3 */
		 /* .line 101 */
	 } // :cond_1
	 (( android.net.wifi.p2p.WifiP2pGroup ) v1 ).getInterface ( ); // invoke-virtual {v1}, Landroid/net/wifi/p2p/WifiP2pGroup;->getInterface()Ljava/lang/String;
	 /* .line 102 */
	 /* .local v3, "iface":Ljava/lang/String; */
	 v4 = this.this$0;
	 com.xiaomi.interconnection.InterconnectionService .-$$Nest$smgetIfaceMacAddr ( v3 );
	 com.xiaomi.interconnection.InterconnectionService .-$$Nest$fputmDeviceP2pMacAddr ( v4,v5 );
	 /* .line 104 */
	 v4 = 	 (( android.net.wifi.p2p.WifiP2pGroup ) v1 ).isGroupOwner ( ); // invoke-virtual {v1}, Landroid/net/wifi/p2p/WifiP2pGroup;->isGroupOwner()Z
	 final String v5 = ""; // const-string v5, ""
	 if ( v4 != null) { // if-eqz v4, :cond_5
		 /* .line 105 */
		 v4 = this.this$0;
		 int v6 = 1; // const/4 v6, 0x1
		 com.xiaomi.interconnection.InterconnectionService .-$$Nest$fputmIsGo ( v4,v6 );
		 /* .line 107 */
		 (( android.net.wifi.p2p.WifiP2pGroup ) v1 ).getClientList ( ); // invoke-virtual {v1}, Landroid/net/wifi/p2p/WifiP2pGroup;->getClientList()Ljava/util/Collection;
		 /* .line 108 */
		 v6 = 		 /* .local v4, "clients":Ljava/util/Collection;, "Ljava/util/Collection<Landroid/net/wifi/p2p/WifiP2pDevice;>;" */
		 /* .line 109 */
		 /* .local v6, "clientNum":I */
		 final String v7 = ""; // const-string v7, ""
		 /* .line 110 */
		 /* .local v7, "macAddr":Ljava/lang/String; */
	 v9 = 	 } // :goto_0
	 if ( v9 != null) { // if-eqz v9, :cond_2
		 /* check-cast v9, Landroid/net/wifi/p2p/WifiP2pDevice; */
		 /* .line 111 */
		 /* .local v9, "client":Landroid/net/wifi/p2p/WifiP2pDevice; */
		 v7 = this.deviceAddress;
		 /* .line 112 */
	 } // .end local v9 # "client":Landroid/net/wifi/p2p/WifiP2pDevice;
	 /* .line 113 */
} // :cond_2
v8 = this.this$0;
com.xiaomi.interconnection.InterconnectionService .-$$Nest$fputmPeerP2pMacAddr ( v8,v7 );
/* .line 115 */
v8 = this.groupOwnerAddress;
/* .line 116 */
/* .local v8, "addr":Ljava/net/InetAddress; */
v9 = this.this$0;
/* if-nez v8, :cond_3 */
/* move-object v10, v5 */
} // :cond_3
(( java.net.InetAddress ) v8 ).getHostAddress ( ); // invoke-virtual {v8}, Ljava/net/InetAddress;->getHostAddress()Ljava/lang/String;
} // :goto_1
com.xiaomi.interconnection.InterconnectionService .-$$Nest$fputmDeviceP2pIpAddr ( v9,v10 );
/* .line 117 */
v9 = this.this$0;
com.xiaomi.interconnection.InterconnectionService .-$$Nest$fgetmPeerP2pMacAddr ( v9 );
com.xiaomi.interconnection.InterconnectionService .-$$Nest$mgetIpFromArpTable ( v9,v10 );
com.xiaomi.interconnection.InterconnectionService .-$$Nest$fputmPeerP2pIpAddr ( v9,v10 );
/* .line 118 */
v9 = this.this$0;
com.xiaomi.interconnection.InterconnectionService .-$$Nest$mnotifyWifiP2pInfoChanged ( v9 );
/* .line 120 */
/* if-lez v6, :cond_4 */
v9 = this.this$0;
com.xiaomi.interconnection.InterconnectionService .-$$Nest$fgetmPeerP2pIpAddr ( v9 );
v5 = (( java.lang.String ) v5 ).equals ( v9 ); // invoke-virtual {v5, v9}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z
if ( v5 != null) { // if-eqz v5, :cond_4
/* .line 121 */
v5 = this.this$0;
com.xiaomi.interconnection.InterconnectionService .-$$Nest$mgetPeerIpFromArpTableUntilTimeout ( v5 );
/* .line 123 */
} // .end local v4 # "clients":Ljava/util/Collection;, "Ljava/util/Collection<Landroid/net/wifi/p2p/WifiP2pDevice;>;"
} // .end local v6 # "clientNum":I
} // .end local v7 # "macAddr":Ljava/lang/String;
} // .end local v8 # "addr":Ljava/net/InetAddress;
} // :cond_4
/* .line 124 */
} // :cond_5
v4 = this.this$0;
int v6 = 0; // const/4 v6, 0x0
com.xiaomi.interconnection.InterconnectionService .-$$Nest$fputmIsGo ( v4,v6 );
/* .line 126 */
(( android.net.wifi.p2p.WifiP2pGroup ) v1 ).getOwner ( ); // invoke-virtual {v1}, Landroid/net/wifi/p2p/WifiP2pGroup;->getOwner()Landroid/net/wifi/p2p/WifiP2pDevice;
/* .line 127 */
/* .local v4, "owner":Landroid/net/wifi/p2p/WifiP2pDevice; */
v6 = this.this$0;
v7 = this.deviceAddress;
com.xiaomi.interconnection.InterconnectionService .-$$Nest$fputmPeerP2pMacAddr ( v6,v7 );
/* .line 129 */
v6 = this.this$0;
com.xiaomi.interconnection.InterconnectionService .-$$Nest$mgetIfaceIpAddr ( v6,v3 );
com.xiaomi.interconnection.InterconnectionService .-$$Nest$fputmDeviceP2pIpAddr ( v6,v7 );
/* .line 130 */
v6 = this.groupOwnerAddress;
/* .line 131 */
/* .local v6, "addr":Ljava/net/InetAddress; */
v7 = this.this$0;
/* if-nez v6, :cond_6 */
/* move-object v8, v5 */
} // :cond_6
(( java.net.InetAddress ) v6 ).getHostAddress ( ); // invoke-virtual {v6}, Ljava/net/InetAddress;->getHostAddress()Ljava/lang/String;
} // :goto_2
com.xiaomi.interconnection.InterconnectionService .-$$Nest$fputmPeerP2pIpAddr ( v7,v8 );
/* .line 132 */
v7 = this.this$0;
com.xiaomi.interconnection.InterconnectionService .-$$Nest$mnotifyWifiP2pInfoChanged ( v7 );
/* .line 134 */
v7 = this.this$0;
com.xiaomi.interconnection.InterconnectionService .-$$Nest$fgetmPeerP2pIpAddr ( v7 );
v5 = (( java.lang.String ) v5 ).equals ( v7 ); // invoke-virtual {v5, v7}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z
if ( v5 != null) { // if-eqz v5, :cond_9
/* .line 135 */
v5 = this.this$0;
com.xiaomi.interconnection.InterconnectionService .-$$Nest$mgetPeerIpFromArpTableUntilTimeout ( v5 );
/* .line 99 */
} // .end local v3 # "iface":Ljava/lang/String;
} // .end local v4 # "owner":Landroid/net/wifi/p2p/WifiP2pDevice;
} // .end local v6 # "addr":Ljava/net/InetAddress;
} // :cond_7
} // :goto_3
return;
/* .line 93 */
} // .end local v1 # "wifiP2pGroup":Landroid/net/wifi/p2p/WifiP2pGroup;
} // .end local v2 # "wifiP2pInfo":Landroid/net/wifi/p2p/WifiP2pInfo;
} // :cond_8
} // :goto_4
return;
/* .line 139 */
} // .end local v0 # "networkInfo":Landroid/net/NetworkInfo;
} // :cond_9
} // :goto_5
return;
} // .end method
