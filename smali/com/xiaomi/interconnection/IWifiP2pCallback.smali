.class public interface abstract Lcom/xiaomi/interconnection/IWifiP2pCallback;
.super Ljava/lang/Object;
.source "IWifiP2pCallback.java"

# interfaces
.implements Landroid/os/IInterface;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/xiaomi/interconnection/IWifiP2pCallback$Stub;,
        Lcom/xiaomi/interconnection/IWifiP2pCallback$Default;
    }
.end annotation


# static fields
.field public static final DESCRIPTOR:Ljava/lang/String; = "com.xiaomi.interconnection.IWifiP2pCallback"


# virtual methods
.method public abstract onDevicesInfoChanged(Lcom/xiaomi/interconnection/P2pDevicesInfo;)V
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation
.end method
