class com.xiaomi.interconnection.IWifiP2pCallback$Stub$Proxy implements com.xiaomi.interconnection.IWifiP2pCallback {
	 /* .source "IWifiP2pCallback.java" */
	 /* # interfaces */
	 /* # annotations */
	 /* .annotation system Ldalvik/annotation/EnclosingClass; */
	 /* value = Lcom/xiaomi/interconnection/IWifiP2pCallback$Stub; */
} // .end annotation
/* .annotation system Ldalvik/annotation/InnerClass; */
/* accessFlags = 0xa */
/* name = "Proxy" */
} // .end annotation
/* # instance fields */
private android.os.IBinder mRemote;
/* # direct methods */
 com.xiaomi.interconnection.IWifiP2pCallback$Stub$Proxy ( ) {
/* .locals 0 */
/* .param p1, "remote" # Landroid/os/IBinder; */
/* .line 100 */
/* invoke-direct {p0}, Ljava/lang/Object;-><init>()V */
/* .line 101 */
this.mRemote = p1;
/* .line 102 */
return;
} // .end method
/* # virtual methods */
public android.os.IBinder asBinder ( ) {
/* .locals 1 */
/* .line 105 */
v0 = this.mRemote;
} // .end method
public java.lang.String getInterfaceDescriptor ( ) {
/* .locals 1 */
/* .line 109 */
final String v0 = "com.xiaomi.interconnection.IWifiP2pCallback"; // const-string v0, "com.xiaomi.interconnection.IWifiP2pCallback"
} // .end method
public void onDevicesInfoChanged ( com.xiaomi.interconnection.P2pDevicesInfo p0 ) {
/* .locals 4 */
/* .param p1, "info" # Lcom/xiaomi/interconnection/P2pDevicesInfo; */
/* .annotation system Ldalvik/annotation/Throws; */
/* value = { */
/* Landroid/os/RemoteException; */
/* } */
} // .end annotation
/* .line 113 */
(( com.xiaomi.interconnection.IWifiP2pCallback$Stub$Proxy ) p0 ).asBinder ( ); // invoke-virtual {p0}, Lcom/xiaomi/interconnection/IWifiP2pCallback$Stub$Proxy;->asBinder()Landroid/os/IBinder;
android.os.Parcel .obtain ( v0 );
/* .line 115 */
/* .local v0, "_data":Landroid/os/Parcel; */
try { // :try_start_0
final String v1 = "com.xiaomi.interconnection.IWifiP2pCallback"; // const-string v1, "com.xiaomi.interconnection.IWifiP2pCallback"
(( android.os.Parcel ) v0 ).writeInterfaceToken ( v1 ); // invoke-virtual {v0, v1}, Landroid/os/Parcel;->writeInterfaceToken(Ljava/lang/String;)V
/* .line 116 */
int v1 = 0; // const/4 v1, 0x0
(( android.os.Parcel ) v0 ).writeTypedObject ( p1, v1 ); // invoke-virtual {v0, p1, v1}, Landroid/os/Parcel;->writeTypedObject(Landroid/os/Parcelable;I)V
/* .line 117 */
v1 = this.mRemote;
int v2 = 0; // const/4 v2, 0x0
int v3 = 1; // const/4 v3, 0x1
/* :try_end_0 */
/* .catchall {:try_start_0 ..:try_end_0} :catchall_0 */
/* .line 120 */
(( android.os.Parcel ) v0 ).recycle ( ); // invoke-virtual {v0}, Landroid/os/Parcel;->recycle()V
/* .line 121 */
/* nop */
/* .line 122 */
return;
/* .line 120 */
/* :catchall_0 */
/* move-exception v1 */
(( android.os.Parcel ) v0 ).recycle ( ); // invoke-virtual {v0}, Landroid/os/Parcel;->recycle()V
/* .line 121 */
/* throw v1 */
} // .end method
