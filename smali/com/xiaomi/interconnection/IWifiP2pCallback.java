public abstract class com.xiaomi.interconnection.IWifiP2pCallback implements android.os.IInterface {
	 /* .source "IWifiP2pCallback.java" */
	 /* # interfaces */
	 /* # annotations */
	 /* .annotation system Ldalvik/annotation/MemberClasses; */
	 /* value = { */
	 /* Lcom/xiaomi/interconnection/IWifiP2pCallback$Stub;, */
	 /* Lcom/xiaomi/interconnection/IWifiP2pCallback$Default; */
	 /* } */
} // .end annotation
/* # static fields */
public static final java.lang.String DESCRIPTOR;
/* # virtual methods */
public abstract void onDevicesInfoChanged ( com.xiaomi.interconnection.P2pDevicesInfo p0 ) {
	 /* .annotation system Ldalvik/annotation/Throws; */
	 /* value = { */
	 /* Landroid/os/RemoteException; */
	 /* } */
} // .end annotation
} // .end method
