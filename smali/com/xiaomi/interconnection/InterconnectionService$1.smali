.class Lcom/xiaomi/interconnection/InterconnectionService$1;
.super Ljava/lang/Object;
.source "InterconnectionService.java"

# interfaces
.implements Landroid/net/wifi/WifiManager$SoftApCallback;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/xiaomi/interconnection/InterconnectionService;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/xiaomi/interconnection/InterconnectionService;


# direct methods
.method constructor <init>(Lcom/xiaomi/interconnection/InterconnectionService;)V
    .locals 0
    .param p1, "this$0"    # Lcom/xiaomi/interconnection/InterconnectionService;

    .line 72
    iput-object p1, p0, Lcom/xiaomi/interconnection/InterconnectionService$1;->this$0:Lcom/xiaomi/interconnection/InterconnectionService;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onInfoChanged(Ljava/util/List;)V
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List<",
            "Landroid/net/wifi/SoftApInfo;",
            ">;)V"
        }
    .end annotation

    .line 75
    .local p1, "softApInfoList":Ljava/util/List;, "Ljava/util/List<Landroid/net/wifi/SoftApInfo;>;"
    invoke-interface {p1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v0

    :goto_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_0

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/net/wifi/SoftApInfo;

    .line 76
    .local v1, "info":Landroid/net/wifi/SoftApInfo;
    iget-object v2, p0, Lcom/xiaomi/interconnection/InterconnectionService$1;->this$0:Lcom/xiaomi/interconnection/InterconnectionService;

    invoke-virtual {v1}, Landroid/net/wifi/SoftApInfo;->getApInstanceIdentifier()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Lcom/xiaomi/interconnection/InterconnectionService;->-$$Nest$fputmSoftApIfaceName(Lcom/xiaomi/interconnection/InterconnectionService;Ljava/lang/String;)V

    .line 77
    iget-object v2, p0, Lcom/xiaomi/interconnection/InterconnectionService$1;->this$0:Lcom/xiaomi/interconnection/InterconnectionService;

    invoke-static {v2}, Lcom/xiaomi/interconnection/InterconnectionService;->-$$Nest$mnotifySoftApInfoChanged(Lcom/xiaomi/interconnection/InterconnectionService;)V

    .line 78
    .end local v1    # "info":Landroid/net/wifi/SoftApInfo;
    goto :goto_0

    .line 79
    :cond_0
    return-void
.end method
