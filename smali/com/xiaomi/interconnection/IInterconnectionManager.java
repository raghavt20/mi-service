public abstract class com.xiaomi.interconnection.IInterconnectionManager implements android.os.IInterface {
	 /* .source "IInterconnectionManager.java" */
	 /* # interfaces */
	 /* # annotations */
	 /* .annotation system Ldalvik/annotation/MemberClasses; */
	 /* value = { */
	 /* Lcom/xiaomi/interconnection/IInterconnectionManager$Stub;, */
	 /* Lcom/xiaomi/interconnection/IInterconnectionManager$Default; */
	 /* } */
} // .end annotation
/* # static fields */
public static final java.lang.String DESCRIPTOR;
/* # virtual methods */
public abstract java.lang.String getWifiChipModel ( ) {
	 /* .annotation system Ldalvik/annotation/Throws; */
	 /* value = { */
	 /* Landroid/os/RemoteException; */
	 /* } */
} // .end annotation
} // .end method
public abstract void notifyConcurrentNetworkState ( Boolean p0 ) {
/* .annotation system Ldalvik/annotation/Throws; */
/* value = { */
/* Landroid/os/RemoteException; */
/* } */
} // .end annotation
} // .end method
public abstract void registerSoftApCallback ( com.xiaomi.interconnection.ISoftApCallback p0 ) {
/* .annotation system Ldalvik/annotation/Throws; */
/* value = { */
/* Landroid/os/RemoteException; */
/* } */
} // .end annotation
} // .end method
public abstract void registerWifiP2pCallback ( com.xiaomi.interconnection.IWifiP2pCallback p0 ) {
/* .annotation system Ldalvik/annotation/Throws; */
/* value = { */
/* Landroid/os/RemoteException; */
/* } */
} // .end annotation
} // .end method
public abstract Integer supportDbs ( ) {
/* .annotation system Ldalvik/annotation/Throws; */
/* value = { */
/* Landroid/os/RemoteException; */
/* } */
} // .end annotation
} // .end method
public abstract Boolean supportHbs ( ) {
/* .annotation system Ldalvik/annotation/Throws; */
/* value = { */
/* Landroid/os/RemoteException; */
/* } */
} // .end annotation
} // .end method
public abstract Boolean supportP2p160Mode ( ) {
/* .annotation system Ldalvik/annotation/Throws; */
/* value = { */
/* Landroid/os/RemoteException; */
/* } */
} // .end annotation
} // .end method
public abstract Boolean supportP2pChannel165 ( ) {
/* .annotation system Ldalvik/annotation/Throws; */
/* value = { */
/* Landroid/os/RemoteException; */
/* } */
} // .end annotation
} // .end method
public abstract void unregisterSoftApCallback ( com.xiaomi.interconnection.ISoftApCallback p0 ) {
/* .annotation system Ldalvik/annotation/Throws; */
/* value = { */
/* Landroid/os/RemoteException; */
/* } */
} // .end annotation
} // .end method
public abstract void unregisterWifiP2pCallback ( com.xiaomi.interconnection.IWifiP2pCallback p0 ) {
/* .annotation system Ldalvik/annotation/Throws; */
/* value = { */
/* Landroid/os/RemoteException; */
/* } */
} // .end annotation
} // .end method
