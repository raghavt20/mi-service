.class Lcom/xiaomi/interconnection/P2pDevicesInfo$1;
.super Ljava/lang/Object;
.source "P2pDevicesInfo.java"

# interfaces
.implements Landroid/os/Parcelable$Creator;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/xiaomi/interconnection/P2pDevicesInfo;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Landroid/os/Parcelable$Creator<",
        "Lcom/xiaomi/interconnection/P2pDevicesInfo;",
        ">;"
    }
.end annotation


# direct methods
.method constructor <init>()V
    .locals 0

    .line 50
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public createFromParcel(Landroid/os/Parcel;)Lcom/xiaomi/interconnection/P2pDevicesInfo;
    .locals 1
    .param p1, "in"    # Landroid/os/Parcel;

    .line 53
    new-instance v0, Lcom/xiaomi/interconnection/P2pDevicesInfo;

    invoke-direct {v0, p1}, Lcom/xiaomi/interconnection/P2pDevicesInfo;-><init>(Landroid/os/Parcel;)V

    return-object v0
.end method

.method public bridge synthetic createFromParcel(Landroid/os/Parcel;)Ljava/lang/Object;
    .locals 0

    .line 50
    invoke-virtual {p0, p1}, Lcom/xiaomi/interconnection/P2pDevicesInfo$1;->createFromParcel(Landroid/os/Parcel;)Lcom/xiaomi/interconnection/P2pDevicesInfo;

    move-result-object p1

    return-object p1
.end method

.method public newArray(I)[Lcom/xiaomi/interconnection/P2pDevicesInfo;
    .locals 1
    .param p1, "size"    # I

    .line 58
    new-array v0, p1, [Lcom/xiaomi/interconnection/P2pDevicesInfo;

    return-object v0
.end method

.method public bridge synthetic newArray(I)[Ljava/lang/Object;
    .locals 0

    .line 50
    invoke-virtual {p0, p1}, Lcom/xiaomi/interconnection/P2pDevicesInfo$1;->newArray(I)[Lcom/xiaomi/interconnection/P2pDevicesInfo;

    move-result-object p1

    return-object p1
.end method
