class com.xiaomi.interconnection.IInterconnectionManager$Stub$Proxy implements com.xiaomi.interconnection.IInterconnectionManager {
	 /* .source "IInterconnectionManager.java" */
	 /* # interfaces */
	 /* # annotations */
	 /* .annotation system Ldalvik/annotation/EnclosingClass; */
	 /* value = Lcom/xiaomi/interconnection/IInterconnectionManager$Stub; */
} // .end annotation
/* .annotation system Ldalvik/annotation/InnerClass; */
/* accessFlags = 0xa */
/* name = "Proxy" */
} // .end annotation
/* # instance fields */
private android.os.IBinder mRemote;
/* # direct methods */
 com.xiaomi.interconnection.IInterconnectionManager$Stub$Proxy ( ) {
/* .locals 0 */
/* .param p1, "remote" # Landroid/os/IBinder; */
/* .line 240 */
/* invoke-direct {p0}, Ljava/lang/Object;-><init>()V */
/* .line 241 */
this.mRemote = p1;
/* .line 242 */
return;
} // .end method
/* # virtual methods */
public android.os.IBinder asBinder ( ) {
/* .locals 1 */
/* .line 245 */
v0 = this.mRemote;
} // .end method
public java.lang.String getInterfaceDescriptor ( ) {
/* .locals 1 */
/* .line 249 */
final String v0 = "com.xiaomi.interconnection.IInterconnectionManager"; // const-string v0, "com.xiaomi.interconnection.IInterconnectionManager"
} // .end method
public java.lang.String getWifiChipModel ( ) {
/* .locals 5 */
/* .annotation system Ldalvik/annotation/Throws; */
/* value = { */
/* Landroid/os/RemoteException; */
/* } */
} // .end annotation
/* .line 253 */
(( com.xiaomi.interconnection.IInterconnectionManager$Stub$Proxy ) p0 ).asBinder ( ); // invoke-virtual {p0}, Lcom/xiaomi/interconnection/IInterconnectionManager$Stub$Proxy;->asBinder()Landroid/os/IBinder;
android.os.Parcel .obtain ( v0 );
/* .line 254 */
/* .local v0, "_data":Landroid/os/Parcel; */
android.os.Parcel .obtain ( );
/* .line 257 */
/* .local v1, "_reply":Landroid/os/Parcel; */
try { // :try_start_0
final String v2 = "com.xiaomi.interconnection.IInterconnectionManager"; // const-string v2, "com.xiaomi.interconnection.IInterconnectionManager"
(( android.os.Parcel ) v0 ).writeInterfaceToken ( v2 ); // invoke-virtual {v0, v2}, Landroid/os/Parcel;->writeInterfaceToken(Ljava/lang/String;)V
/* .line 258 */
v2 = this.mRemote;
int v3 = 1; // const/4 v3, 0x1
v2 = int v4 = 0; // const/4 v4, 0x0
/* .line 259 */
/* .local v2, "_status":Z */
(( android.os.Parcel ) v1 ).readException ( ); // invoke-virtual {v1}, Landroid/os/Parcel;->readException()V
/* .line 260 */
(( android.os.Parcel ) v1 ).readString ( ); // invoke-virtual {v1}, Landroid/os/Parcel;->readString()Ljava/lang/String;
/* :try_end_0 */
/* .catchall {:try_start_0 ..:try_end_0} :catchall_0 */
/* move-object v2, v3 */
/* .line 263 */
/* .local v2, "_result":Ljava/lang/String; */
(( android.os.Parcel ) v1 ).recycle ( ); // invoke-virtual {v1}, Landroid/os/Parcel;->recycle()V
/* .line 264 */
(( android.os.Parcel ) v0 ).recycle ( ); // invoke-virtual {v0}, Landroid/os/Parcel;->recycle()V
/* .line 265 */
/* nop */
/* .line 266 */
/* .line 263 */
} // .end local v2 # "_result":Ljava/lang/String;
/* :catchall_0 */
/* move-exception v2 */
(( android.os.Parcel ) v1 ).recycle ( ); // invoke-virtual {v1}, Landroid/os/Parcel;->recycle()V
/* .line 264 */
(( android.os.Parcel ) v0 ).recycle ( ); // invoke-virtual {v0}, Landroid/os/Parcel;->recycle()V
/* .line 265 */
/* throw v2 */
} // .end method
public void notifyConcurrentNetworkState ( Boolean p0 ) {
/* .locals 5 */
/* .param p1, "mcc" # Z */
/* .annotation system Ldalvik/annotation/Throws; */
/* value = { */
/* Landroid/os/RemoteException; */
/* } */
} // .end annotation
/* .line 398 */
(( com.xiaomi.interconnection.IInterconnectionManager$Stub$Proxy ) p0 ).asBinder ( ); // invoke-virtual {p0}, Lcom/xiaomi/interconnection/IInterconnectionManager$Stub$Proxy;->asBinder()Landroid/os/IBinder;
android.os.Parcel .obtain ( v0 );
/* .line 399 */
/* .local v0, "_data":Landroid/os/Parcel; */
android.os.Parcel .obtain ( );
/* .line 401 */
/* .local v1, "_reply":Landroid/os/Parcel; */
try { // :try_start_0
final String v2 = "com.xiaomi.interconnection.IInterconnectionManager"; // const-string v2, "com.xiaomi.interconnection.IInterconnectionManager"
(( android.os.Parcel ) v0 ).writeInterfaceToken ( v2 ); // invoke-virtual {v0, v2}, Landroid/os/Parcel;->writeInterfaceToken(Ljava/lang/String;)V
/* .line 402 */
(( android.os.Parcel ) v0 ).writeBoolean ( p1 ); // invoke-virtual {v0, p1}, Landroid/os/Parcel;->writeBoolean(Z)V
/* .line 403 */
v2 = this.mRemote;
/* const/16 v3, 0xa */
v2 = int v4 = 0; // const/4 v4, 0x0
/* .line 404 */
/* .local v2, "_status":Z */
(( android.os.Parcel ) v1 ).readException ( ); // invoke-virtual {v1}, Landroid/os/Parcel;->readException()V
/* :try_end_0 */
/* .catchall {:try_start_0 ..:try_end_0} :catchall_0 */
/* .line 407 */
} // .end local v2 # "_status":Z
(( android.os.Parcel ) v1 ).recycle ( ); // invoke-virtual {v1}, Landroid/os/Parcel;->recycle()V
/* .line 408 */
(( android.os.Parcel ) v0 ).recycle ( ); // invoke-virtual {v0}, Landroid/os/Parcel;->recycle()V
/* .line 409 */
/* nop */
/* .line 410 */
return;
/* .line 407 */
/* :catchall_0 */
/* move-exception v2 */
(( android.os.Parcel ) v1 ).recycle ( ); // invoke-virtual {v1}, Landroid/os/Parcel;->recycle()V
/* .line 408 */
(( android.os.Parcel ) v0 ).recycle ( ); // invoke-virtual {v0}, Landroid/os/Parcel;->recycle()V
/* .line 409 */
/* throw v2 */
} // .end method
public void registerSoftApCallback ( com.xiaomi.interconnection.ISoftApCallback p0 ) {
/* .locals 5 */
/* .param p1, "cb" # Lcom/xiaomi/interconnection/ISoftApCallback; */
/* .annotation system Ldalvik/annotation/Throws; */
/* value = { */
/* Landroid/os/RemoteException; */
/* } */
} // .end annotation
/* .line 338 */
(( com.xiaomi.interconnection.IInterconnectionManager$Stub$Proxy ) p0 ).asBinder ( ); // invoke-virtual {p0}, Lcom/xiaomi/interconnection/IInterconnectionManager$Stub$Proxy;->asBinder()Landroid/os/IBinder;
android.os.Parcel .obtain ( v0 );
/* .line 339 */
/* .local v0, "_data":Landroid/os/Parcel; */
android.os.Parcel .obtain ( );
/* .line 341 */
/* .local v1, "_reply":Landroid/os/Parcel; */
try { // :try_start_0
final String v2 = "com.xiaomi.interconnection.IInterconnectionManager"; // const-string v2, "com.xiaomi.interconnection.IInterconnectionManager"
(( android.os.Parcel ) v0 ).writeInterfaceToken ( v2 ); // invoke-virtual {v0, v2}, Landroid/os/Parcel;->writeInterfaceToken(Ljava/lang/String;)V
/* .line 342 */
(( android.os.Parcel ) v0 ).writeStrongInterface ( p1 ); // invoke-virtual {v0, p1}, Landroid/os/Parcel;->writeStrongInterface(Landroid/os/IInterface;)V
/* .line 343 */
v2 = this.mRemote;
int v3 = 6; // const/4 v3, 0x6
v2 = int v4 = 0; // const/4 v4, 0x0
/* .line 344 */
/* .local v2, "_status":Z */
(( android.os.Parcel ) v1 ).readException ( ); // invoke-virtual {v1}, Landroid/os/Parcel;->readException()V
/* :try_end_0 */
/* .catchall {:try_start_0 ..:try_end_0} :catchall_0 */
/* .line 347 */
} // .end local v2 # "_status":Z
(( android.os.Parcel ) v1 ).recycle ( ); // invoke-virtual {v1}, Landroid/os/Parcel;->recycle()V
/* .line 348 */
(( android.os.Parcel ) v0 ).recycle ( ); // invoke-virtual {v0}, Landroid/os/Parcel;->recycle()V
/* .line 349 */
/* nop */
/* .line 350 */
return;
/* .line 347 */
/* :catchall_0 */
/* move-exception v2 */
(( android.os.Parcel ) v1 ).recycle ( ); // invoke-virtual {v1}, Landroid/os/Parcel;->recycle()V
/* .line 348 */
(( android.os.Parcel ) v0 ).recycle ( ); // invoke-virtual {v0}, Landroid/os/Parcel;->recycle()V
/* .line 349 */
/* throw v2 */
} // .end method
public void registerWifiP2pCallback ( com.xiaomi.interconnection.IWifiP2pCallback p0 ) {
/* .locals 5 */
/* .param p1, "cb" # Lcom/xiaomi/interconnection/IWifiP2pCallback; */
/* .annotation system Ldalvik/annotation/Throws; */
/* value = { */
/* Landroid/os/RemoteException; */
/* } */
} // .end annotation
/* .line 368 */
(( com.xiaomi.interconnection.IInterconnectionManager$Stub$Proxy ) p0 ).asBinder ( ); // invoke-virtual {p0}, Lcom/xiaomi/interconnection/IInterconnectionManager$Stub$Proxy;->asBinder()Landroid/os/IBinder;
android.os.Parcel .obtain ( v0 );
/* .line 369 */
/* .local v0, "_data":Landroid/os/Parcel; */
android.os.Parcel .obtain ( );
/* .line 371 */
/* .local v1, "_reply":Landroid/os/Parcel; */
try { // :try_start_0
final String v2 = "com.xiaomi.interconnection.IInterconnectionManager"; // const-string v2, "com.xiaomi.interconnection.IInterconnectionManager"
(( android.os.Parcel ) v0 ).writeInterfaceToken ( v2 ); // invoke-virtual {v0, v2}, Landroid/os/Parcel;->writeInterfaceToken(Ljava/lang/String;)V
/* .line 372 */
(( android.os.Parcel ) v0 ).writeStrongInterface ( p1 ); // invoke-virtual {v0, p1}, Landroid/os/Parcel;->writeStrongInterface(Landroid/os/IInterface;)V
/* .line 373 */
v2 = this.mRemote;
/* const/16 v3, 0x8 */
v2 = int v4 = 0; // const/4 v4, 0x0
/* .line 374 */
/* .local v2, "_status":Z */
(( android.os.Parcel ) v1 ).readException ( ); // invoke-virtual {v1}, Landroid/os/Parcel;->readException()V
/* :try_end_0 */
/* .catchall {:try_start_0 ..:try_end_0} :catchall_0 */
/* .line 377 */
} // .end local v2 # "_status":Z
(( android.os.Parcel ) v1 ).recycle ( ); // invoke-virtual {v1}, Landroid/os/Parcel;->recycle()V
/* .line 378 */
(( android.os.Parcel ) v0 ).recycle ( ); // invoke-virtual {v0}, Landroid/os/Parcel;->recycle()V
/* .line 379 */
/* nop */
/* .line 380 */
return;
/* .line 377 */
/* :catchall_0 */
/* move-exception v2 */
(( android.os.Parcel ) v1 ).recycle ( ); // invoke-virtual {v1}, Landroid/os/Parcel;->recycle()V
/* .line 378 */
(( android.os.Parcel ) v0 ).recycle ( ); // invoke-virtual {v0}, Landroid/os/Parcel;->recycle()V
/* .line 379 */
/* throw v2 */
} // .end method
public Integer supportDbs ( ) {
/* .locals 5 */
/* .annotation system Ldalvik/annotation/Throws; */
/* value = { */
/* Landroid/os/RemoteException; */
/* } */
} // .end annotation
/* .line 321 */
(( com.xiaomi.interconnection.IInterconnectionManager$Stub$Proxy ) p0 ).asBinder ( ); // invoke-virtual {p0}, Lcom/xiaomi/interconnection/IInterconnectionManager$Stub$Proxy;->asBinder()Landroid/os/IBinder;
android.os.Parcel .obtain ( v0 );
/* .line 322 */
/* .local v0, "_data":Landroid/os/Parcel; */
android.os.Parcel .obtain ( );
/* .line 325 */
/* .local v1, "_reply":Landroid/os/Parcel; */
try { // :try_start_0
final String v2 = "com.xiaomi.interconnection.IInterconnectionManager"; // const-string v2, "com.xiaomi.interconnection.IInterconnectionManager"
(( android.os.Parcel ) v0 ).writeInterfaceToken ( v2 ); // invoke-virtual {v0, v2}, Landroid/os/Parcel;->writeInterfaceToken(Ljava/lang/String;)V
/* .line 326 */
v2 = this.mRemote;
int v3 = 5; // const/4 v3, 0x5
v2 = int v4 = 0; // const/4 v4, 0x0
/* .line 327 */
/* .local v2, "_status":Z */
(( android.os.Parcel ) v1 ).readException ( ); // invoke-virtual {v1}, Landroid/os/Parcel;->readException()V
/* .line 328 */
v3 = (( android.os.Parcel ) v1 ).readInt ( ); // invoke-virtual {v1}, Landroid/os/Parcel;->readInt()I
/* :try_end_0 */
/* .catchall {:try_start_0 ..:try_end_0} :catchall_0 */
/* move v2, v3 */
/* .line 331 */
/* .local v2, "_result":I */
(( android.os.Parcel ) v1 ).recycle ( ); // invoke-virtual {v1}, Landroid/os/Parcel;->recycle()V
/* .line 332 */
(( android.os.Parcel ) v0 ).recycle ( ); // invoke-virtual {v0}, Landroid/os/Parcel;->recycle()V
/* .line 333 */
/* nop */
/* .line 334 */
/* .line 331 */
} // .end local v2 # "_result":I
/* :catchall_0 */
/* move-exception v2 */
(( android.os.Parcel ) v1 ).recycle ( ); // invoke-virtual {v1}, Landroid/os/Parcel;->recycle()V
/* .line 332 */
(( android.os.Parcel ) v0 ).recycle ( ); // invoke-virtual {v0}, Landroid/os/Parcel;->recycle()V
/* .line 333 */
/* throw v2 */
} // .end method
public Boolean supportHbs ( ) {
/* .locals 5 */
/* .annotation system Ldalvik/annotation/Throws; */
/* value = { */
/* Landroid/os/RemoteException; */
/* } */
} // .end annotation
/* .line 304 */
(( com.xiaomi.interconnection.IInterconnectionManager$Stub$Proxy ) p0 ).asBinder ( ); // invoke-virtual {p0}, Lcom/xiaomi/interconnection/IInterconnectionManager$Stub$Proxy;->asBinder()Landroid/os/IBinder;
android.os.Parcel .obtain ( v0 );
/* .line 305 */
/* .local v0, "_data":Landroid/os/Parcel; */
android.os.Parcel .obtain ( );
/* .line 308 */
/* .local v1, "_reply":Landroid/os/Parcel; */
try { // :try_start_0
final String v2 = "com.xiaomi.interconnection.IInterconnectionManager"; // const-string v2, "com.xiaomi.interconnection.IInterconnectionManager"
(( android.os.Parcel ) v0 ).writeInterfaceToken ( v2 ); // invoke-virtual {v0, v2}, Landroid/os/Parcel;->writeInterfaceToken(Ljava/lang/String;)V
/* .line 309 */
v2 = this.mRemote;
int v3 = 4; // const/4 v3, 0x4
v2 = int v4 = 0; // const/4 v4, 0x0
/* .line 310 */
/* .local v2, "_status":Z */
(( android.os.Parcel ) v1 ).readException ( ); // invoke-virtual {v1}, Landroid/os/Parcel;->readException()V
/* .line 311 */
v3 = (( android.os.Parcel ) v1 ).readBoolean ( ); // invoke-virtual {v1}, Landroid/os/Parcel;->readBoolean()Z
/* :try_end_0 */
/* .catchall {:try_start_0 ..:try_end_0} :catchall_0 */
/* move v2, v3 */
/* .line 314 */
/* .local v2, "_result":Z */
(( android.os.Parcel ) v1 ).recycle ( ); // invoke-virtual {v1}, Landroid/os/Parcel;->recycle()V
/* .line 315 */
(( android.os.Parcel ) v0 ).recycle ( ); // invoke-virtual {v0}, Landroid/os/Parcel;->recycle()V
/* .line 316 */
/* nop */
/* .line 317 */
/* .line 314 */
} // .end local v2 # "_result":Z
/* :catchall_0 */
/* move-exception v2 */
(( android.os.Parcel ) v1 ).recycle ( ); // invoke-virtual {v1}, Landroid/os/Parcel;->recycle()V
/* .line 315 */
(( android.os.Parcel ) v0 ).recycle ( ); // invoke-virtual {v0}, Landroid/os/Parcel;->recycle()V
/* .line 316 */
/* throw v2 */
} // .end method
public Boolean supportP2p160Mode ( ) {
/* .locals 5 */
/* .annotation system Ldalvik/annotation/Throws; */
/* value = { */
/* Landroid/os/RemoteException; */
/* } */
} // .end annotation
/* .line 287 */
(( com.xiaomi.interconnection.IInterconnectionManager$Stub$Proxy ) p0 ).asBinder ( ); // invoke-virtual {p0}, Lcom/xiaomi/interconnection/IInterconnectionManager$Stub$Proxy;->asBinder()Landroid/os/IBinder;
android.os.Parcel .obtain ( v0 );
/* .line 288 */
/* .local v0, "_data":Landroid/os/Parcel; */
android.os.Parcel .obtain ( );
/* .line 291 */
/* .local v1, "_reply":Landroid/os/Parcel; */
try { // :try_start_0
final String v2 = "com.xiaomi.interconnection.IInterconnectionManager"; // const-string v2, "com.xiaomi.interconnection.IInterconnectionManager"
(( android.os.Parcel ) v0 ).writeInterfaceToken ( v2 ); // invoke-virtual {v0, v2}, Landroid/os/Parcel;->writeInterfaceToken(Ljava/lang/String;)V
/* .line 292 */
v2 = this.mRemote;
int v3 = 3; // const/4 v3, 0x3
v2 = int v4 = 0; // const/4 v4, 0x0
/* .line 293 */
/* .local v2, "_status":Z */
(( android.os.Parcel ) v1 ).readException ( ); // invoke-virtual {v1}, Landroid/os/Parcel;->readException()V
/* .line 294 */
v3 = (( android.os.Parcel ) v1 ).readBoolean ( ); // invoke-virtual {v1}, Landroid/os/Parcel;->readBoolean()Z
/* :try_end_0 */
/* .catchall {:try_start_0 ..:try_end_0} :catchall_0 */
/* move v2, v3 */
/* .line 297 */
/* .local v2, "_result":Z */
(( android.os.Parcel ) v1 ).recycle ( ); // invoke-virtual {v1}, Landroid/os/Parcel;->recycle()V
/* .line 298 */
(( android.os.Parcel ) v0 ).recycle ( ); // invoke-virtual {v0}, Landroid/os/Parcel;->recycle()V
/* .line 299 */
/* nop */
/* .line 300 */
/* .line 297 */
} // .end local v2 # "_result":Z
/* :catchall_0 */
/* move-exception v2 */
(( android.os.Parcel ) v1 ).recycle ( ); // invoke-virtual {v1}, Landroid/os/Parcel;->recycle()V
/* .line 298 */
(( android.os.Parcel ) v0 ).recycle ( ); // invoke-virtual {v0}, Landroid/os/Parcel;->recycle()V
/* .line 299 */
/* throw v2 */
} // .end method
public Boolean supportP2pChannel165 ( ) {
/* .locals 5 */
/* .annotation system Ldalvik/annotation/Throws; */
/* value = { */
/* Landroid/os/RemoteException; */
/* } */
} // .end annotation
/* .line 270 */
(( com.xiaomi.interconnection.IInterconnectionManager$Stub$Proxy ) p0 ).asBinder ( ); // invoke-virtual {p0}, Lcom/xiaomi/interconnection/IInterconnectionManager$Stub$Proxy;->asBinder()Landroid/os/IBinder;
android.os.Parcel .obtain ( v0 );
/* .line 271 */
/* .local v0, "_data":Landroid/os/Parcel; */
android.os.Parcel .obtain ( );
/* .line 274 */
/* .local v1, "_reply":Landroid/os/Parcel; */
try { // :try_start_0
final String v2 = "com.xiaomi.interconnection.IInterconnectionManager"; // const-string v2, "com.xiaomi.interconnection.IInterconnectionManager"
(( android.os.Parcel ) v0 ).writeInterfaceToken ( v2 ); // invoke-virtual {v0, v2}, Landroid/os/Parcel;->writeInterfaceToken(Ljava/lang/String;)V
/* .line 275 */
v2 = this.mRemote;
int v3 = 2; // const/4 v3, 0x2
v2 = int v4 = 0; // const/4 v4, 0x0
/* .line 276 */
/* .local v2, "_status":Z */
(( android.os.Parcel ) v1 ).readException ( ); // invoke-virtual {v1}, Landroid/os/Parcel;->readException()V
/* .line 277 */
v3 = (( android.os.Parcel ) v1 ).readBoolean ( ); // invoke-virtual {v1}, Landroid/os/Parcel;->readBoolean()Z
/* :try_end_0 */
/* .catchall {:try_start_0 ..:try_end_0} :catchall_0 */
/* move v2, v3 */
/* .line 280 */
/* .local v2, "_result":Z */
(( android.os.Parcel ) v1 ).recycle ( ); // invoke-virtual {v1}, Landroid/os/Parcel;->recycle()V
/* .line 281 */
(( android.os.Parcel ) v0 ).recycle ( ); // invoke-virtual {v0}, Landroid/os/Parcel;->recycle()V
/* .line 282 */
/* nop */
/* .line 283 */
/* .line 280 */
} // .end local v2 # "_result":Z
/* :catchall_0 */
/* move-exception v2 */
(( android.os.Parcel ) v1 ).recycle ( ); // invoke-virtual {v1}, Landroid/os/Parcel;->recycle()V
/* .line 281 */
(( android.os.Parcel ) v0 ).recycle ( ); // invoke-virtual {v0}, Landroid/os/Parcel;->recycle()V
/* .line 282 */
/* throw v2 */
} // .end method
public void unregisterSoftApCallback ( com.xiaomi.interconnection.ISoftApCallback p0 ) {
/* .locals 5 */
/* .param p1, "cb" # Lcom/xiaomi/interconnection/ISoftApCallback; */
/* .annotation system Ldalvik/annotation/Throws; */
/* value = { */
/* Landroid/os/RemoteException; */
/* } */
} // .end annotation
/* .line 353 */
(( com.xiaomi.interconnection.IInterconnectionManager$Stub$Proxy ) p0 ).asBinder ( ); // invoke-virtual {p0}, Lcom/xiaomi/interconnection/IInterconnectionManager$Stub$Proxy;->asBinder()Landroid/os/IBinder;
android.os.Parcel .obtain ( v0 );
/* .line 354 */
/* .local v0, "_data":Landroid/os/Parcel; */
android.os.Parcel .obtain ( );
/* .line 356 */
/* .local v1, "_reply":Landroid/os/Parcel; */
try { // :try_start_0
final String v2 = "com.xiaomi.interconnection.IInterconnectionManager"; // const-string v2, "com.xiaomi.interconnection.IInterconnectionManager"
(( android.os.Parcel ) v0 ).writeInterfaceToken ( v2 ); // invoke-virtual {v0, v2}, Landroid/os/Parcel;->writeInterfaceToken(Ljava/lang/String;)V
/* .line 357 */
(( android.os.Parcel ) v0 ).writeStrongInterface ( p1 ); // invoke-virtual {v0, p1}, Landroid/os/Parcel;->writeStrongInterface(Landroid/os/IInterface;)V
/* .line 358 */
v2 = this.mRemote;
int v3 = 7; // const/4 v3, 0x7
v2 = int v4 = 0; // const/4 v4, 0x0
/* .line 359 */
/* .local v2, "_status":Z */
(( android.os.Parcel ) v1 ).readException ( ); // invoke-virtual {v1}, Landroid/os/Parcel;->readException()V
/* :try_end_0 */
/* .catchall {:try_start_0 ..:try_end_0} :catchall_0 */
/* .line 362 */
} // .end local v2 # "_status":Z
(( android.os.Parcel ) v1 ).recycle ( ); // invoke-virtual {v1}, Landroid/os/Parcel;->recycle()V
/* .line 363 */
(( android.os.Parcel ) v0 ).recycle ( ); // invoke-virtual {v0}, Landroid/os/Parcel;->recycle()V
/* .line 364 */
/* nop */
/* .line 365 */
return;
/* .line 362 */
/* :catchall_0 */
/* move-exception v2 */
(( android.os.Parcel ) v1 ).recycle ( ); // invoke-virtual {v1}, Landroid/os/Parcel;->recycle()V
/* .line 363 */
(( android.os.Parcel ) v0 ).recycle ( ); // invoke-virtual {v0}, Landroid/os/Parcel;->recycle()V
/* .line 364 */
/* throw v2 */
} // .end method
public void unregisterWifiP2pCallback ( com.xiaomi.interconnection.IWifiP2pCallback p0 ) {
/* .locals 5 */
/* .param p1, "cb" # Lcom/xiaomi/interconnection/IWifiP2pCallback; */
/* .annotation system Ldalvik/annotation/Throws; */
/* value = { */
/* Landroid/os/RemoteException; */
/* } */
} // .end annotation
/* .line 383 */
(( com.xiaomi.interconnection.IInterconnectionManager$Stub$Proxy ) p0 ).asBinder ( ); // invoke-virtual {p0}, Lcom/xiaomi/interconnection/IInterconnectionManager$Stub$Proxy;->asBinder()Landroid/os/IBinder;
android.os.Parcel .obtain ( v0 );
/* .line 384 */
/* .local v0, "_data":Landroid/os/Parcel; */
android.os.Parcel .obtain ( );
/* .line 386 */
/* .local v1, "_reply":Landroid/os/Parcel; */
try { // :try_start_0
final String v2 = "com.xiaomi.interconnection.IInterconnectionManager"; // const-string v2, "com.xiaomi.interconnection.IInterconnectionManager"
(( android.os.Parcel ) v0 ).writeInterfaceToken ( v2 ); // invoke-virtual {v0, v2}, Landroid/os/Parcel;->writeInterfaceToken(Ljava/lang/String;)V
/* .line 387 */
(( android.os.Parcel ) v0 ).writeStrongInterface ( p1 ); // invoke-virtual {v0, p1}, Landroid/os/Parcel;->writeStrongInterface(Landroid/os/IInterface;)V
/* .line 388 */
v2 = this.mRemote;
/* const/16 v3, 0x9 */
v2 = int v4 = 0; // const/4 v4, 0x0
/* .line 389 */
/* .local v2, "_status":Z */
(( android.os.Parcel ) v1 ).readException ( ); // invoke-virtual {v1}, Landroid/os/Parcel;->readException()V
/* :try_end_0 */
/* .catchall {:try_start_0 ..:try_end_0} :catchall_0 */
/* .line 392 */
} // .end local v2 # "_status":Z
(( android.os.Parcel ) v1 ).recycle ( ); // invoke-virtual {v1}, Landroid/os/Parcel;->recycle()V
/* .line 393 */
(( android.os.Parcel ) v0 ).recycle ( ); // invoke-virtual {v0}, Landroid/os/Parcel;->recycle()V
/* .line 394 */
/* nop */
/* .line 395 */
return;
/* .line 392 */
/* :catchall_0 */
/* move-exception v2 */
(( android.os.Parcel ) v1 ).recycle ( ); // invoke-virtual {v1}, Landroid/os/Parcel;->recycle()V
/* .line 393 */
(( android.os.Parcel ) v0 ).recycle ( ); // invoke-virtual {v0}, Landroid/os/Parcel;->recycle()V
/* .line 394 */
/* throw v2 */
} // .end method
