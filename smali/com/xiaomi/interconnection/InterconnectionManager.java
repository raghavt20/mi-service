public class com.xiaomi.interconnection.InterconnectionManager {
	 /* .source "InterconnectionManager.java" */
	 /* # annotations */
	 /* .annotation system Ldalvik/annotation/MemberClasses; */
	 /* value = { */
	 /* Lcom/xiaomi/interconnection/InterconnectionManager$SingletonHolder;, */
	 /* Lcom/xiaomi/interconnection/InterconnectionManager$SoftApCallbackProxy;, */
	 /* Lcom/xiaomi/interconnection/InterconnectionManager$SoftApCallback;, */
	 /* Lcom/xiaomi/interconnection/InterconnectionManager$WifiP2pCallbackProxy;, */
	 /* Lcom/xiaomi/interconnection/InterconnectionManager$WifiP2pCallback; */
	 /* } */
} // .end annotation
/* # static fields */
public static final Integer DBS_1;
public static final Integer DBS_2;
public static final Integer DBS_UNSUPPORTED;
private static final java.lang.String TAG;
private static final android.util.SparseArray sSoftApCallbackMap;
/* .annotation system Ldalvik/annotation/Signature; */
/* value = { */
/* "Landroid/util/SparseArray<", */
/* "Lcom/xiaomi/interconnection/ISoftApCallback;", */
/* ">;" */
/* } */
} // .end annotation
} // .end field
private static final android.util.SparseArray sWifiP2pCallbackMap;
/* .annotation system Ldalvik/annotation/Signature; */
/* value = { */
/* "Landroid/util/SparseArray<", */
/* "Lcom/xiaomi/interconnection/IWifiP2pCallback;", */
/* ">;" */
/* } */
} // .end annotation
} // .end field
/* # instance fields */
private android.os.Handler mHandler;
private com.xiaomi.interconnection.IInterconnectionManager mInterconnService;
/* # direct methods */
static android.os.Handler -$$Nest$fgetmHandler ( com.xiaomi.interconnection.InterconnectionManager p0 ) { //bridge//synthethic
/* .locals 0 */
p0 = this.mHandler;
} // .end method
static com.xiaomi.interconnection.InterconnectionManager ( ) {
/* .locals 1 */
/* .line 22 */
/* new-instance v0, Landroid/util/SparseArray; */
/* invoke-direct {v0}, Landroid/util/SparseArray;-><init>()V */
/* .line 23 */
/* new-instance v0, Landroid/util/SparseArray; */
/* invoke-direct {v0}, Landroid/util/SparseArray;-><init>()V */
return;
} // .end method
private com.xiaomi.interconnection.InterconnectionManager ( ) {
/* .locals 3 */
/* .line 29 */
/* invoke-direct {p0}, Ljava/lang/Object;-><init>()V */
/* .line 30 */
/* const-string/jumbo v0, "xiaomi.InterconnectionService" */
android.os.ServiceManager .getService ( v0 );
/* .line 31 */
/* .local v0, "binder":Landroid/os/IBinder; */
com.xiaomi.interconnection.IInterconnectionManager$Stub .asInterface ( v0 );
this.mInterconnService = v1;
/* .line 32 */
/* if-nez v1, :cond_0 */
/* .line 33 */
final String v1 = "InterconnectionManager"; // const-string v1, "InterconnectionManager"
final String v2 = "InterconnectionService not found"; // const-string v2, "InterconnectionService not found"
android.util.Log .e ( v1,v2 );
/* .line 36 */
} // :cond_0
/* new-instance v1, Landroid/os/Handler; */
/* invoke-direct {v1}, Landroid/os/Handler;-><init>()V */
this.mHandler = v1;
/* .line 37 */
return;
} // .end method
 com.xiaomi.interconnection.InterconnectionManager ( ) { //synthethic
/* .locals 0 */
/* invoke-direct {p0}, Lcom/xiaomi/interconnection/InterconnectionManager;-><init>()V */
return;
} // .end method
public static com.xiaomi.interconnection.InterconnectionManager getInstance ( ) {
/* .locals 1 */
/* .line 44 */
com.xiaomi.interconnection.InterconnectionManager$SingletonHolder .-$$Nest$sfgetINSTANCE ( );
} // .end method
/* # virtual methods */
public java.lang.String getWifiChipModel ( ) {
/* .locals 3 */
/* .line 168 */
v0 = this.mInterconnService;
if ( v0 != null) { // if-eqz v0, :cond_0
/* .line 170 */
try { // :try_start_0
/* :try_end_0 */
/* .catch Landroid/os/RemoteException; {:try_start_0 ..:try_end_0} :catch_0 */
/* .line 171 */
/* :catch_0 */
/* move-exception v0 */
/* .line 172 */
/* .local v0, "e":Landroid/os/RemoteException; */
/* new-instance v1, Ljava/lang/StringBuilder; */
/* invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V */
final String v2 = "exception: "; // const-string v2, "exception: "
(( java.lang.StringBuilder ) v1 ).append ( v2 ); // invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v1 ).append ( v0 ); // invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v1 ).toString ( ); // invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
final String v2 = "InterconnectionManager"; // const-string v2, "InterconnectionManager"
android.util.Log .d ( v2,v1 );
/* .line 175 */
} // .end local v0 # "e":Landroid/os/RemoteException;
} // :cond_0
/* const-string/jumbo v0, "unknown" */
} // .end method
public void notifyConcurrentNetworkState ( Boolean p0 ) {
/* .locals 3 */
/* .param p1, "mcc" # Z */
/* .line 223 */
v0 = this.mInterconnService;
if ( v0 != null) { // if-eqz v0, :cond_0
/* .line 225 */
try { // :try_start_0
/* :try_end_0 */
/* .catch Landroid/os/RemoteException; {:try_start_0 ..:try_end_0} :catch_0 */
/* .line 228 */
/* .line 226 */
/* :catch_0 */
/* move-exception v0 */
/* .line 227 */
/* .local v0, "e":Landroid/os/RemoteException; */
/* new-instance v1, Ljava/lang/StringBuilder; */
/* invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V */
final String v2 = "exception: "; // const-string v2, "exception: "
(( java.lang.StringBuilder ) v1 ).append ( v2 ); // invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v1 ).append ( v0 ); // invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v1 ).toString ( ); // invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
final String v2 = "InterconnectionManager"; // const-string v2, "InterconnectionManager"
android.util.Log .d ( v2,v1 );
/* .line 230 */
} // .end local v0 # "e":Landroid/os/RemoteException;
} // :cond_0
} // :goto_0
return;
} // .end method
public void registerSoftApCallback ( com.xiaomi.interconnection.InterconnectionManager$SoftApCallback p0 ) {
/* .locals 4 */
/* .param p1, "softApCallback" # Lcom/xiaomi/interconnection/InterconnectionManager$SoftApCallback; */
/* .line 67 */
if ( p1 != null) { // if-eqz p1, :cond_1
/* .line 71 */
v0 = this.mInterconnService;
if ( v0 != null) { // if-eqz v0, :cond_0
/* .line 73 */
try { // :try_start_0
v0 = com.xiaomi.interconnection.InterconnectionManager.sSoftApCallbackMap;
/* monitor-enter v0 */
/* :try_end_0 */
/* .catch Landroid/os/RemoteException; {:try_start_0 ..:try_end_0} :catch_0 */
/* .line 74 */
try { // :try_start_1
/* new-instance v1, Lcom/xiaomi/interconnection/InterconnectionManager$SoftApCallbackProxy; */
int v2 = 0; // const/4 v2, 0x0
/* invoke-direct {v1, p0, p1, v2}, Lcom/xiaomi/interconnection/InterconnectionManager$SoftApCallbackProxy;-><init>(Lcom/xiaomi/interconnection/InterconnectionManager;Lcom/xiaomi/interconnection/InterconnectionManager$SoftApCallback;Lcom/xiaomi/interconnection/InterconnectionManager$SoftApCallbackProxy-IA;)V */
/* .line 75 */
/* .local v1, "binderCallback":Lcom/xiaomi/interconnection/ISoftApCallback$Stub; */
v2 = java.lang.System .identityHashCode ( p1 );
(( android.util.SparseArray ) v0 ).put ( v2, v1 ); // invoke-virtual {v0, v2, v1}, Landroid/util/SparseArray;->put(ILjava/lang/Object;)V
/* .line 76 */
v2 = this.mInterconnService;
/* .line 77 */
} // .end local v1 # "binderCallback":Lcom/xiaomi/interconnection/ISoftApCallback$Stub;
/* monitor-exit v0 */
/* .line 80 */
/* .line 77 */
/* :catchall_0 */
/* move-exception v1 */
/* monitor-exit v0 */
/* :try_end_1 */
/* .catchall {:try_start_1 ..:try_end_1} :catchall_0 */
} // .end local p0 # "this":Lcom/xiaomi/interconnection/InterconnectionManager;
} // .end local p1 # "softApCallback":Lcom/xiaomi/interconnection/InterconnectionManager$SoftApCallback;
try { // :try_start_2
/* throw v1 */
/* :try_end_2 */
/* .catch Landroid/os/RemoteException; {:try_start_2 ..:try_end_2} :catch_0 */
/* .line 78 */
/* .restart local p0 # "this":Lcom/xiaomi/interconnection/InterconnectionManager; */
/* .restart local p1 # "softApCallback":Lcom/xiaomi/interconnection/InterconnectionManager$SoftApCallback; */
/* :catch_0 */
/* move-exception v0 */
/* .line 79 */
/* .local v0, "e":Landroid/os/RemoteException; */
final String v1 = "InterconnectionManager"; // const-string v1, "InterconnectionManager"
/* new-instance v2, Ljava/lang/StringBuilder; */
/* invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V */
final String v3 = "exception: "; // const-string v3, "exception: "
(( java.lang.StringBuilder ) v2 ).append ( v3 ); // invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v2 ).append ( v0 ); // invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v2 ).toString ( ); // invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
android.util.Log .d ( v1,v2 );
/* .line 82 */
} // .end local v0 # "e":Landroid/os/RemoteException;
} // :cond_0
} // :goto_0
return;
/* .line 68 */
} // :cond_1
/* new-instance v0, Ljava/lang/IllegalArgumentException; */
/* const-string/jumbo v1, "softApCallback cannot be null" */
/* invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V */
/* throw v0 */
} // .end method
public void registerWifiP2pCallback ( com.xiaomi.interconnection.InterconnectionManager$WifiP2pCallback p0 ) {
/* .locals 4 */
/* .param p1, "wifiP2pCallback" # Lcom/xiaomi/interconnection/InterconnectionManager$WifiP2pCallback; */
/* .line 127 */
if ( p1 != null) { // if-eqz p1, :cond_1
/* .line 131 */
v0 = this.mInterconnService;
if ( v0 != null) { // if-eqz v0, :cond_0
/* .line 133 */
try { // :try_start_0
v0 = com.xiaomi.interconnection.InterconnectionManager.sWifiP2pCallbackMap;
/* monitor-enter v0 */
/* :try_end_0 */
/* .catch Landroid/os/RemoteException; {:try_start_0 ..:try_end_0} :catch_0 */
/* .line 134 */
try { // :try_start_1
/* new-instance v1, Lcom/xiaomi/interconnection/InterconnectionManager$WifiP2pCallbackProxy; */
int v2 = 0; // const/4 v2, 0x0
/* invoke-direct {v1, p0, p1, v2}, Lcom/xiaomi/interconnection/InterconnectionManager$WifiP2pCallbackProxy;-><init>(Lcom/xiaomi/interconnection/InterconnectionManager;Lcom/xiaomi/interconnection/InterconnectionManager$WifiP2pCallback;Lcom/xiaomi/interconnection/InterconnectionManager$WifiP2pCallbackProxy-IA;)V */
/* .line 135 */
/* .local v1, "binderCallback":Lcom/xiaomi/interconnection/IWifiP2pCallback$Stub; */
v2 = java.lang.System .identityHashCode ( p1 );
(( android.util.SparseArray ) v0 ).put ( v2, v1 ); // invoke-virtual {v0, v2, v1}, Landroid/util/SparseArray;->put(ILjava/lang/Object;)V
/* .line 136 */
v2 = this.mInterconnService;
/* .line 137 */
} // .end local v1 # "binderCallback":Lcom/xiaomi/interconnection/IWifiP2pCallback$Stub;
/* monitor-exit v0 */
/* .line 140 */
/* .line 137 */
/* :catchall_0 */
/* move-exception v1 */
/* monitor-exit v0 */
/* :try_end_1 */
/* .catchall {:try_start_1 ..:try_end_1} :catchall_0 */
} // .end local p0 # "this":Lcom/xiaomi/interconnection/InterconnectionManager;
} // .end local p1 # "wifiP2pCallback":Lcom/xiaomi/interconnection/InterconnectionManager$WifiP2pCallback;
try { // :try_start_2
/* throw v1 */
/* :try_end_2 */
/* .catch Landroid/os/RemoteException; {:try_start_2 ..:try_end_2} :catch_0 */
/* .line 138 */
/* .restart local p0 # "this":Lcom/xiaomi/interconnection/InterconnectionManager; */
/* .restart local p1 # "wifiP2pCallback":Lcom/xiaomi/interconnection/InterconnectionManager$WifiP2pCallback; */
/* :catch_0 */
/* move-exception v0 */
/* .line 139 */
/* .local v0, "e":Landroid/os/RemoteException; */
final String v1 = "InterconnectionManager"; // const-string v1, "InterconnectionManager"
/* new-instance v2, Ljava/lang/StringBuilder; */
/* invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V */
final String v3 = "exception: "; // const-string v3, "exception: "
(( java.lang.StringBuilder ) v2 ).append ( v3 ); // invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v2 ).append ( v0 ); // invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v2 ).toString ( ); // invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
android.util.Log .d ( v1,v2 );
/* .line 142 */
} // .end local v0 # "e":Landroid/os/RemoteException;
} // :cond_0
} // :goto_0
return;
/* .line 128 */
} // :cond_1
/* new-instance v0, Ljava/lang/IllegalArgumentException; */
/* const-string/jumbo v1, "wifiP2pCallback cannot be null" */
/* invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V */
/* throw v0 */
} // .end method
public Integer supportDbs ( ) {
/* .locals 3 */
/* .line 212 */
v0 = this.mInterconnService;
if ( v0 != null) { // if-eqz v0, :cond_0
/* .line 214 */
v0 = try { // :try_start_0
/* :try_end_0 */
/* .catch Landroid/os/RemoteException; {:try_start_0 ..:try_end_0} :catch_0 */
/* .line 215 */
/* :catch_0 */
/* move-exception v0 */
/* .line 216 */
/* .local v0, "e":Landroid/os/RemoteException; */
/* new-instance v1, Ljava/lang/StringBuilder; */
/* invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V */
final String v2 = "exception: "; // const-string v2, "exception: "
(( java.lang.StringBuilder ) v1 ).append ( v2 ); // invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v1 ).append ( v0 ); // invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v1 ).toString ( ); // invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
final String v2 = "InterconnectionManager"; // const-string v2, "InterconnectionManager"
android.util.Log .d ( v2,v1 );
/* .line 219 */
} // .end local v0 # "e":Landroid/os/RemoteException;
} // :cond_0
int v0 = 0; // const/4 v0, 0x0
} // .end method
public Boolean supportHbs ( ) {
/* .locals 3 */
/* .line 201 */
v0 = this.mInterconnService;
if ( v0 != null) { // if-eqz v0, :cond_0
/* .line 203 */
v0 = try { // :try_start_0
/* :try_end_0 */
/* .catch Landroid/os/RemoteException; {:try_start_0 ..:try_end_0} :catch_0 */
/* .line 204 */
/* :catch_0 */
/* move-exception v0 */
/* .line 205 */
/* .local v0, "e":Landroid/os/RemoteException; */
/* new-instance v1, Ljava/lang/StringBuilder; */
/* invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V */
final String v2 = "exception: "; // const-string v2, "exception: "
(( java.lang.StringBuilder ) v1 ).append ( v2 ); // invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v1 ).append ( v0 ); // invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v1 ).toString ( ); // invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
final String v2 = "InterconnectionManager"; // const-string v2, "InterconnectionManager"
android.util.Log .d ( v2,v1 );
/* .line 208 */
} // .end local v0 # "e":Landroid/os/RemoteException;
} // :cond_0
int v0 = 0; // const/4 v0, 0x0
} // .end method
public Boolean supportP2p160Mode ( ) {
/* .locals 3 */
/* .line 190 */
v0 = this.mInterconnService;
if ( v0 != null) { // if-eqz v0, :cond_0
/* .line 192 */
v0 = try { // :try_start_0
/* :try_end_0 */
/* .catch Landroid/os/RemoteException; {:try_start_0 ..:try_end_0} :catch_0 */
/* .line 193 */
/* :catch_0 */
/* move-exception v0 */
/* .line 194 */
/* .local v0, "e":Landroid/os/RemoteException; */
/* new-instance v1, Ljava/lang/StringBuilder; */
/* invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V */
final String v2 = "exception: "; // const-string v2, "exception: "
(( java.lang.StringBuilder ) v1 ).append ( v2 ); // invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v1 ).append ( v0 ); // invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v1 ).toString ( ); // invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
final String v2 = "InterconnectionManager"; // const-string v2, "InterconnectionManager"
android.util.Log .d ( v2,v1 );
/* .line 197 */
} // .end local v0 # "e":Landroid/os/RemoteException;
} // :cond_0
int v0 = 0; // const/4 v0, 0x0
} // .end method
public Boolean supportP2pChannel165 ( ) {
/* .locals 3 */
/* .line 179 */
v0 = this.mInterconnService;
if ( v0 != null) { // if-eqz v0, :cond_0
/* .line 181 */
v0 = try { // :try_start_0
/* :try_end_0 */
/* .catch Landroid/os/RemoteException; {:try_start_0 ..:try_end_0} :catch_0 */
/* .line 182 */
/* :catch_0 */
/* move-exception v0 */
/* .line 183 */
/* .local v0, "e":Landroid/os/RemoteException; */
/* new-instance v1, Ljava/lang/StringBuilder; */
/* invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V */
final String v2 = "exception: "; // const-string v2, "exception: "
(( java.lang.StringBuilder ) v1 ).append ( v2 ); // invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v1 ).append ( v0 ); // invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v1 ).toString ( ); // invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
final String v2 = "InterconnectionManager"; // const-string v2, "InterconnectionManager"
android.util.Log .d ( v2,v1 );
/* .line 186 */
} // .end local v0 # "e":Landroid/os/RemoteException;
} // :cond_0
int v0 = 0; // const/4 v0, 0x0
} // .end method
public void unregisterSoftApCallback ( com.xiaomi.interconnection.InterconnectionManager$SoftApCallback p0 ) {
/* .locals 5 */
/* .param p1, "softApCallback" # Lcom/xiaomi/interconnection/InterconnectionManager$SoftApCallback; */
/* .line 85 */
if ( p1 != null) { // if-eqz p1, :cond_2
/* .line 89 */
v0 = this.mInterconnService;
if ( v0 != null) { // if-eqz v0, :cond_1
/* .line 91 */
try { // :try_start_0
v0 = com.xiaomi.interconnection.InterconnectionManager.sSoftApCallbackMap;
/* monitor-enter v0 */
/* :try_end_0 */
/* .catch Landroid/os/RemoteException; {:try_start_0 ..:try_end_0} :catch_0 */
/* .line 92 */
try { // :try_start_1
v1 = java.lang.System .identityHashCode ( p1 );
/* .line 93 */
/* .local v1, "callbackIdentifier":I */
v2 = (( android.util.SparseArray ) v0 ).contains ( v1 ); // invoke-virtual {v0, v1}, Landroid/util/SparseArray;->contains(I)Z
/* if-nez v2, :cond_0 */
/* .line 94 */
final String v2 = "InterconnectionManager"; // const-string v2, "InterconnectionManager"
/* new-instance v3, Ljava/lang/StringBuilder; */
/* invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V */
/* const-string/jumbo v4, "unknown softApCallback " */
(( java.lang.StringBuilder ) v3 ).append ( v4 ); // invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v3 ).append ( v1 ); // invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v3 ).toString ( ); // invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
android.util.Log .w ( v2,v3 );
/* .line 95 */
/* monitor-exit v0 */
return;
/* .line 97 */
} // :cond_0
v2 = this.mInterconnService;
/* .line 98 */
(( android.util.SparseArray ) v0 ).get ( v1 ); // invoke-virtual {v0, v1}, Landroid/util/SparseArray;->get(I)Ljava/lang/Object;
/* check-cast v3, Lcom/xiaomi/interconnection/ISoftApCallback; */
/* .line 97 */
/* .line 99 */
(( android.util.SparseArray ) v0 ).remove ( v1 ); // invoke-virtual {v0, v1}, Landroid/util/SparseArray;->remove(I)V
/* .line 100 */
} // .end local v1 # "callbackIdentifier":I
/* monitor-exit v0 */
/* .line 103 */
/* .line 100 */
/* :catchall_0 */
/* move-exception v1 */
/* monitor-exit v0 */
/* :try_end_1 */
/* .catchall {:try_start_1 ..:try_end_1} :catchall_0 */
} // .end local p0 # "this":Lcom/xiaomi/interconnection/InterconnectionManager;
} // .end local p1 # "softApCallback":Lcom/xiaomi/interconnection/InterconnectionManager$SoftApCallback;
try { // :try_start_2
/* throw v1 */
/* :try_end_2 */
/* .catch Landroid/os/RemoteException; {:try_start_2 ..:try_end_2} :catch_0 */
/* .line 101 */
/* .restart local p0 # "this":Lcom/xiaomi/interconnection/InterconnectionManager; */
/* .restart local p1 # "softApCallback":Lcom/xiaomi/interconnection/InterconnectionManager$SoftApCallback; */
/* :catch_0 */
/* move-exception v0 */
/* .line 102 */
/* .local v0, "e":Landroid/os/RemoteException; */
final String v1 = "InterconnectionManager"; // const-string v1, "InterconnectionManager"
/* new-instance v2, Ljava/lang/StringBuilder; */
/* invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V */
final String v3 = "exception: "; // const-string v3, "exception: "
(( java.lang.StringBuilder ) v2 ).append ( v3 ); // invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v2 ).append ( v0 ); // invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v2 ).toString ( ); // invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
android.util.Log .d ( v1,v2 );
/* .line 105 */
} // .end local v0 # "e":Landroid/os/RemoteException;
} // :cond_1
} // :goto_0
return;
/* .line 86 */
} // :cond_2
/* new-instance v0, Ljava/lang/IllegalArgumentException; */
/* const-string/jumbo v1, "softApCallback cannot be null" */
/* invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V */
/* throw v0 */
} // .end method
public void unregisterWifiP2pCallback ( com.xiaomi.interconnection.InterconnectionManager$WifiP2pCallback p0 ) {
/* .locals 5 */
/* .param p1, "wifiP2pCallback" # Lcom/xiaomi/interconnection/InterconnectionManager$WifiP2pCallback; */
/* .line 145 */
if ( p1 != null) { // if-eqz p1, :cond_2
/* .line 149 */
v0 = this.mInterconnService;
if ( v0 != null) { // if-eqz v0, :cond_1
/* .line 151 */
try { // :try_start_0
v0 = com.xiaomi.interconnection.InterconnectionManager.sWifiP2pCallbackMap;
/* monitor-enter v0 */
/* :try_end_0 */
/* .catch Landroid/os/RemoteException; {:try_start_0 ..:try_end_0} :catch_0 */
/* .line 152 */
try { // :try_start_1
v1 = java.lang.System .identityHashCode ( p1 );
/* .line 153 */
/* .local v1, "callbackIdentifier":I */
v2 = (( android.util.SparseArray ) v0 ).contains ( v1 ); // invoke-virtual {v0, v1}, Landroid/util/SparseArray;->contains(I)Z
/* if-nez v2, :cond_0 */
/* .line 154 */
final String v2 = "InterconnectionManager"; // const-string v2, "InterconnectionManager"
/* new-instance v3, Ljava/lang/StringBuilder; */
/* invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V */
/* const-string/jumbo v4, "unknown wifiP2pCallback" */
(( java.lang.StringBuilder ) v3 ).append ( v4 ); // invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v3 ).append ( v1 ); // invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v3 ).toString ( ); // invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
android.util.Log .w ( v2,v3 );
/* .line 155 */
/* monitor-exit v0 */
return;
/* .line 157 */
} // :cond_0
v2 = this.mInterconnService;
/* .line 158 */
(( android.util.SparseArray ) v0 ).get ( v1 ); // invoke-virtual {v0, v1}, Landroid/util/SparseArray;->get(I)Ljava/lang/Object;
/* check-cast v3, Lcom/xiaomi/interconnection/IWifiP2pCallback; */
/* .line 157 */
/* .line 159 */
(( android.util.SparseArray ) v0 ).remove ( v1 ); // invoke-virtual {v0, v1}, Landroid/util/SparseArray;->remove(I)V
/* .line 160 */
} // .end local v1 # "callbackIdentifier":I
/* monitor-exit v0 */
/* .line 163 */
/* .line 160 */
/* :catchall_0 */
/* move-exception v1 */
/* monitor-exit v0 */
/* :try_end_1 */
/* .catchall {:try_start_1 ..:try_end_1} :catchall_0 */
} // .end local p0 # "this":Lcom/xiaomi/interconnection/InterconnectionManager;
} // .end local p1 # "wifiP2pCallback":Lcom/xiaomi/interconnection/InterconnectionManager$WifiP2pCallback;
try { // :try_start_2
/* throw v1 */
/* :try_end_2 */
/* .catch Landroid/os/RemoteException; {:try_start_2 ..:try_end_2} :catch_0 */
/* .line 161 */
/* .restart local p0 # "this":Lcom/xiaomi/interconnection/InterconnectionManager; */
/* .restart local p1 # "wifiP2pCallback":Lcom/xiaomi/interconnection/InterconnectionManager$WifiP2pCallback; */
/* :catch_0 */
/* move-exception v0 */
/* .line 162 */
/* .local v0, "e":Landroid/os/RemoteException; */
final String v1 = "InterconnectionManager"; // const-string v1, "InterconnectionManager"
/* new-instance v2, Ljava/lang/StringBuilder; */
/* invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V */
final String v3 = "exception: "; // const-string v3, "exception: "
(( java.lang.StringBuilder ) v2 ).append ( v3 ); // invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v2 ).append ( v0 ); // invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v2 ).toString ( ); // invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
android.util.Log .d ( v1,v2 );
/* .line 165 */
} // .end local v0 # "e":Landroid/os/RemoteException;
} // :cond_1
} // :goto_0
return;
/* .line 146 */
} // :cond_2
/* new-instance v0, Ljava/lang/IllegalArgumentException; */
/* const-string/jumbo v1, "wifiP2pCallback cannot be null" */
/* invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V */
/* throw v0 */
} // .end method
