public abstract class com.xiaomi.interconnection.IWifiP2pCallback$Stub extends android.os.Binder implements com.xiaomi.interconnection.IWifiP2pCallback {
	 /* .source "IWifiP2pCallback.java" */
	 /* # interfaces */
	 /* # annotations */
	 /* .annotation system Ldalvik/annotation/EnclosingClass; */
	 /* value = Lcom/xiaomi/interconnection/IWifiP2pCallback; */
} // .end annotation
/* .annotation system Ldalvik/annotation/InnerClass; */
/* accessFlags = 0x409 */
/* name = "Stub" */
} // .end annotation
/* .annotation system Ldalvik/annotation/MemberClasses; */
/* value = { */
/* Lcom/xiaomi/interconnection/IWifiP2pCallback$Stub$Proxy; */
/* } */
} // .end annotation
/* # static fields */
static final Integer TRANSACTION_onDevicesInfoChanged;
/* # direct methods */
public com.xiaomi.interconnection.IWifiP2pCallback$Stub ( ) {
/* .locals 1 */
/* .line 23 */
/* invoke-direct {p0}, Landroid/os/Binder;-><init>()V */
/* .line 24 */
final String v0 = "com.xiaomi.interconnection.IWifiP2pCallback"; // const-string v0, "com.xiaomi.interconnection.IWifiP2pCallback"
(( com.xiaomi.interconnection.IWifiP2pCallback$Stub ) p0 ).attachInterface ( p0, v0 ); // invoke-virtual {p0, p0, v0}, Lcom/xiaomi/interconnection/IWifiP2pCallback$Stub;->attachInterface(Landroid/os/IInterface;Ljava/lang/String;)V
/* .line 25 */
return;
} // .end method
public static com.xiaomi.interconnection.IWifiP2pCallback asInterface ( android.os.IBinder p0 ) {
/* .locals 2 */
/* .param p0, "obj" # Landroid/os/IBinder; */
/* .line 32 */
/* if-nez p0, :cond_0 */
/* .line 33 */
int v0 = 0; // const/4 v0, 0x0
/* .line 35 */
} // :cond_0
final String v0 = "com.xiaomi.interconnection.IWifiP2pCallback"; // const-string v0, "com.xiaomi.interconnection.IWifiP2pCallback"
/* .line 36 */
/* .local v0, "iin":Landroid/os/IInterface; */
if ( v0 != null) { // if-eqz v0, :cond_1
/* instance-of v1, v0, Lcom/xiaomi/interconnection/IWifiP2pCallback; */
if ( v1 != null) { // if-eqz v1, :cond_1
/* .line 37 */
/* move-object v1, v0 */
/* check-cast v1, Lcom/xiaomi/interconnection/IWifiP2pCallback; */
/* .line 39 */
} // :cond_1
/* new-instance v1, Lcom/xiaomi/interconnection/IWifiP2pCallback$Stub$Proxy; */
/* invoke-direct {v1, p0}, Lcom/xiaomi/interconnection/IWifiP2pCallback$Stub$Proxy;-><init>(Landroid/os/IBinder;)V */
} // .end method
public static java.lang.String getDefaultTransactionName ( Integer p0 ) {
/* .locals 1 */
/* .param p0, "transactionCode" # I */
/* .line 48 */
/* packed-switch p0, :pswitch_data_0 */
/* .line 56 */
int v0 = 0; // const/4 v0, 0x0
/* .line 52 */
/* :pswitch_0 */
final String v0 = "onDevicesInfoChanged"; // const-string v0, "onDevicesInfoChanged"
/* :pswitch_data_0 */
/* .packed-switch 0x1 */
/* :pswitch_0 */
} // .end packed-switch
} // .end method
/* # virtual methods */
public android.os.IBinder asBinder ( ) {
/* .locals 0 */
/* .line 43 */
} // .end method
public Integer getMaxTransactionId ( ) {
/* .locals 1 */
/* .line 128 */
int v0 = 0; // const/4 v0, 0x0
} // .end method
public java.lang.String getTransactionName ( Integer p0 ) {
/* .locals 1 */
/* .param p1, "transactionCode" # I */
/* .line 63 */
com.xiaomi.interconnection.IWifiP2pCallback$Stub .getDefaultTransactionName ( p1 );
} // .end method
public Boolean onTransact ( Integer p0, android.os.Parcel p1, android.os.Parcel p2, Integer p3 ) {
/* .locals 3 */
/* .param p1, "code" # I */
/* .param p2, "data" # Landroid/os/Parcel; */
/* .param p3, "reply" # Landroid/os/Parcel; */
/* .param p4, "flags" # I */
/* .annotation system Ldalvik/annotation/Throws; */
/* value = { */
/* Landroid/os/RemoteException; */
/* } */
} // .end annotation
/* .line 67 */
final String v0 = "com.xiaomi.interconnection.IWifiP2pCallback"; // const-string v0, "com.xiaomi.interconnection.IWifiP2pCallback"
/* .line 68 */
/* .local v0, "descriptor":Ljava/lang/String; */
int v1 = 1; // const/4 v1, 0x1
/* if-lt p1, v1, :cond_0 */
/* const v2, 0xffffff */
/* if-gt p1, v2, :cond_0 */
/* .line 69 */
(( android.os.Parcel ) p2 ).enforceInterface ( v0 ); // invoke-virtual {p2, v0}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V
/* .line 71 */
} // :cond_0
/* packed-switch p1, :pswitch_data_0 */
/* .line 79 */
/* packed-switch p1, :pswitch_data_1 */
/* .line 91 */
v1 = /* invoke-super {p0, p1, p2, p3, p4}, Landroid/os/Binder;->onTransact(ILandroid/os/Parcel;Landroid/os/Parcel;I)Z */
/* .line 75 */
/* :pswitch_0 */
(( android.os.Parcel ) p3 ).writeString ( v0 ); // invoke-virtual {p3, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V
/* .line 76 */
/* .line 84 */
/* :pswitch_1 */
v2 = com.xiaomi.interconnection.P2pDevicesInfo.CREATOR;
(( android.os.Parcel ) p2 ).readTypedObject ( v2 ); // invoke-virtual {p2, v2}, Landroid/os/Parcel;->readTypedObject(Landroid/os/Parcelable$Creator;)Ljava/lang/Object;
/* check-cast v2, Lcom/xiaomi/interconnection/P2pDevicesInfo; */
/* .line 85 */
/* .local v2, "_arg0":Lcom/xiaomi/interconnection/P2pDevicesInfo; */
(( android.os.Parcel ) p2 ).enforceNoDataAvail ( ); // invoke-virtual {p2}, Landroid/os/Parcel;->enforceNoDataAvail()V
/* .line 86 */
(( com.xiaomi.interconnection.IWifiP2pCallback$Stub ) p0 ).onDevicesInfoChanged ( v2 ); // invoke-virtual {p0, v2}, Lcom/xiaomi/interconnection/IWifiP2pCallback$Stub;->onDevicesInfoChanged(Lcom/xiaomi/interconnection/P2pDevicesInfo;)V
/* .line 87 */
/* nop */
/* .line 94 */
} // .end local v2 # "_arg0":Lcom/xiaomi/interconnection/P2pDevicesInfo;
/* :pswitch_data_0 */
/* .packed-switch 0x5f4e5446 */
/* :pswitch_0 */
} // .end packed-switch
/* :pswitch_data_1 */
/* .packed-switch 0x1 */
/* :pswitch_1 */
} // .end packed-switch
} // .end method
