.class Lcom/xiaomi/interconnection/InterconnectionManager$WifiP2pCallbackProxy;
.super Lcom/xiaomi/interconnection/IWifiP2pCallback$Stub;
.source "InterconnectionManager.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/xiaomi/interconnection/InterconnectionManager;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "WifiP2pCallbackProxy"
.end annotation


# instance fields
.field private final mCallback:Lcom/xiaomi/interconnection/InterconnectionManager$WifiP2pCallback;

.field final synthetic this$0:Lcom/xiaomi/interconnection/InterconnectionManager;


# direct methods
.method public static synthetic $r8$lambda$Sqwh_lEJw-zivKoXw4x_5TaNt8Q(Lcom/xiaomi/interconnection/InterconnectionManager$WifiP2pCallbackProxy;Lcom/xiaomi/interconnection/P2pDevicesInfo;)V
    .locals 0

    invoke-direct {p0, p1}, Lcom/xiaomi/interconnection/InterconnectionManager$WifiP2pCallbackProxy;->lambda$onDevicesInfoChanged$0(Lcom/xiaomi/interconnection/P2pDevicesInfo;)V

    return-void
.end method

.method private constructor <init>(Lcom/xiaomi/interconnection/InterconnectionManager;Lcom/xiaomi/interconnection/InterconnectionManager$WifiP2pCallback;)V
    .locals 0
    .param p2, "callback"    # Lcom/xiaomi/interconnection/InterconnectionManager$WifiP2pCallback;

    .line 114
    iput-object p1, p0, Lcom/xiaomi/interconnection/InterconnectionManager$WifiP2pCallbackProxy;->this$0:Lcom/xiaomi/interconnection/InterconnectionManager;

    invoke-direct {p0}, Lcom/xiaomi/interconnection/IWifiP2pCallback$Stub;-><init>()V

    .line 115
    iput-object p2, p0, Lcom/xiaomi/interconnection/InterconnectionManager$WifiP2pCallbackProxy;->mCallback:Lcom/xiaomi/interconnection/InterconnectionManager$WifiP2pCallback;

    .line 116
    return-void
.end method

.method synthetic constructor <init>(Lcom/xiaomi/interconnection/InterconnectionManager;Lcom/xiaomi/interconnection/InterconnectionManager$WifiP2pCallback;Lcom/xiaomi/interconnection/InterconnectionManager$WifiP2pCallbackProxy-IA;)V
    .locals 0

    invoke-direct {p0, p1, p2}, Lcom/xiaomi/interconnection/InterconnectionManager$WifiP2pCallbackProxy;-><init>(Lcom/xiaomi/interconnection/InterconnectionManager;Lcom/xiaomi/interconnection/InterconnectionManager$WifiP2pCallback;)V

    return-void
.end method

.method private synthetic lambda$onDevicesInfoChanged$0(Lcom/xiaomi/interconnection/P2pDevicesInfo;)V
    .locals 1
    .param p1, "info"    # Lcom/xiaomi/interconnection/P2pDevicesInfo;

    .line 121
    iget-object v0, p0, Lcom/xiaomi/interconnection/InterconnectionManager$WifiP2pCallbackProxy;->mCallback:Lcom/xiaomi/interconnection/InterconnectionManager$WifiP2pCallback;

    invoke-interface {v0, p1}, Lcom/xiaomi/interconnection/InterconnectionManager$WifiP2pCallback;->onDevicesInfoChanged(Lcom/xiaomi/interconnection/P2pDevicesInfo;)V

    .line 122
    return-void
.end method


# virtual methods
.method public onDevicesInfoChanged(Lcom/xiaomi/interconnection/P2pDevicesInfo;)V
    .locals 2
    .param p1, "info"    # Lcom/xiaomi/interconnection/P2pDevicesInfo;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    .line 120
    iget-object v0, p0, Lcom/xiaomi/interconnection/InterconnectionManager$WifiP2pCallbackProxy;->this$0:Lcom/xiaomi/interconnection/InterconnectionManager;

    invoke-static {v0}, Lcom/xiaomi/interconnection/InterconnectionManager;->-$$Nest$fgetmHandler(Lcom/xiaomi/interconnection/InterconnectionManager;)Landroid/os/Handler;

    move-result-object v0

    new-instance v1, Lcom/xiaomi/interconnection/InterconnectionManager$WifiP2pCallbackProxy$$ExternalSyntheticLambda0;

    invoke-direct {v1, p0, p1}, Lcom/xiaomi/interconnection/InterconnectionManager$WifiP2pCallbackProxy$$ExternalSyntheticLambda0;-><init>(Lcom/xiaomi/interconnection/InterconnectionManager$WifiP2pCallbackProxy;Lcom/xiaomi/interconnection/P2pDevicesInfo;)V

    invoke-virtual {v0, v1}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    .line 123
    return-void
.end method
