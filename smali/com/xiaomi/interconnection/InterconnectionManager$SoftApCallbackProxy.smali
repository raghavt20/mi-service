.class Lcom/xiaomi/interconnection/InterconnectionManager$SoftApCallbackProxy;
.super Lcom/xiaomi/interconnection/ISoftApCallback$Stub;
.source "InterconnectionManager.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/xiaomi/interconnection/InterconnectionManager;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "SoftApCallbackProxy"
.end annotation


# instance fields
.field private final mCallback:Lcom/xiaomi/interconnection/InterconnectionManager$SoftApCallback;

.field final synthetic this$0:Lcom/xiaomi/interconnection/InterconnectionManager;


# direct methods
.method public static synthetic $r8$lambda$EdbrSW7mHjASOBT229K0IcRRWwo(Lcom/xiaomi/interconnection/InterconnectionManager$SoftApCallbackProxy;Ljava/lang/String;)V
    .locals 0

    invoke-direct {p0, p1}, Lcom/xiaomi/interconnection/InterconnectionManager$SoftApCallbackProxy;->lambda$onIfaceInfoChanged$0(Ljava/lang/String;)V

    return-void
.end method

.method private constructor <init>(Lcom/xiaomi/interconnection/InterconnectionManager;Lcom/xiaomi/interconnection/InterconnectionManager$SoftApCallback;)V
    .locals 0
    .param p2, "callback"    # Lcom/xiaomi/interconnection/InterconnectionManager$SoftApCallback;

    .line 54
    iput-object p1, p0, Lcom/xiaomi/interconnection/InterconnectionManager$SoftApCallbackProxy;->this$0:Lcom/xiaomi/interconnection/InterconnectionManager;

    invoke-direct {p0}, Lcom/xiaomi/interconnection/ISoftApCallback$Stub;-><init>()V

    .line 55
    iput-object p2, p0, Lcom/xiaomi/interconnection/InterconnectionManager$SoftApCallbackProxy;->mCallback:Lcom/xiaomi/interconnection/InterconnectionManager$SoftApCallback;

    .line 56
    return-void
.end method

.method synthetic constructor <init>(Lcom/xiaomi/interconnection/InterconnectionManager;Lcom/xiaomi/interconnection/InterconnectionManager$SoftApCallback;Lcom/xiaomi/interconnection/InterconnectionManager$SoftApCallbackProxy-IA;)V
    .locals 0

    invoke-direct {p0, p1, p2}, Lcom/xiaomi/interconnection/InterconnectionManager$SoftApCallbackProxy;-><init>(Lcom/xiaomi/interconnection/InterconnectionManager;Lcom/xiaomi/interconnection/InterconnectionManager$SoftApCallback;)V

    return-void
.end method

.method private synthetic lambda$onIfaceInfoChanged$0(Ljava/lang/String;)V
    .locals 1
    .param p1, "softApIfaceName"    # Ljava/lang/String;

    .line 61
    iget-object v0, p0, Lcom/xiaomi/interconnection/InterconnectionManager$SoftApCallbackProxy;->mCallback:Lcom/xiaomi/interconnection/InterconnectionManager$SoftApCallback;

    invoke-interface {v0, p1}, Lcom/xiaomi/interconnection/InterconnectionManager$SoftApCallback;->onIfaceInfoChanged(Ljava/lang/String;)V

    .line 62
    return-void
.end method


# virtual methods
.method public onIfaceInfoChanged(Ljava/lang/String;)V
    .locals 2
    .param p1, "softApIfaceName"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    .line 60
    iget-object v0, p0, Lcom/xiaomi/interconnection/InterconnectionManager$SoftApCallbackProxy;->this$0:Lcom/xiaomi/interconnection/InterconnectionManager;

    invoke-static {v0}, Lcom/xiaomi/interconnection/InterconnectionManager;->-$$Nest$fgetmHandler(Lcom/xiaomi/interconnection/InterconnectionManager;)Landroid/os/Handler;

    move-result-object v0

    new-instance v1, Lcom/xiaomi/interconnection/InterconnectionManager$SoftApCallbackProxy$$ExternalSyntheticLambda0;

    invoke-direct {v1, p0, p1}, Lcom/xiaomi/interconnection/InterconnectionManager$SoftApCallbackProxy$$ExternalSyntheticLambda0;-><init>(Lcom/xiaomi/interconnection/InterconnectionManager$SoftApCallbackProxy;Ljava/lang/String;)V

    invoke-virtual {v0, v1}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    .line 63
    return-void
.end method
