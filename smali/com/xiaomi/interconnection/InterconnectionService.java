public class com.xiaomi.interconnection.InterconnectionService extends com.xiaomi.interconnection.IInterconnectionManager$Stub {
	 /* .source "InterconnectionService.java" */
	 /* # static fields */
	 private static final java.lang.String ARP_TABLE_PATH;
	 private static final java.lang.String FEATURE_P2P_160M;
	 private static final java.lang.String FEATURE_P2P_165CHAN;
	 public static final java.lang.String SERVICE_NAME;
	 public static final java.lang.String TAG;
	 /* # instance fields */
	 private android.content.Context mContext;
	 private java.lang.String mDeviceP2pIpAddr;
	 private java.lang.String mDeviceP2pMacAddr;
	 private android.os.Handler mHandler;
	 private Boolean mIsGo;
	 private android.content.BroadcastReceiver mP2pReceiver;
	 private android.content.pm.PackageManager mPackageManager;
	 private java.lang.String mPeerP2pIpAddr;
	 private java.lang.String mPeerP2pMacAddr;
	 private android.net.wifi.WifiManager$SoftApCallback mSoftApCallback;
	 private android.os.RemoteCallbackList mSoftApCallbackList;
	 /* .annotation system Ldalvik/annotation/Signature; */
	 /* value = { */
	 /* "Landroid/os/RemoteCallbackList<", */
	 /* "Lcom/xiaomi/interconnection/ISoftApCallback;", */
	 /* ">;" */
	 /* } */
} // .end annotation
} // .end field
private java.lang.String mSoftApIfaceName;
private android.net.wifi.WifiManager mWifiManager;
private android.os.RemoteCallbackList mWifiP2pCallbackList;
/* .annotation system Ldalvik/annotation/Signature; */
/* value = { */
/* "Landroid/os/RemoteCallbackList<", */
/* "Lcom/xiaomi/interconnection/IWifiP2pCallback;", */
/* ">;" */
/* } */
} // .end annotation
} // .end field
/* # direct methods */
public static void $r8$lambda$V0E8mio_FV3tSjxDGYR0ApNjA8g ( com.xiaomi.interconnection.InterconnectionService p0 ) { //synthethic
/* .locals 0 */
/* invoke-direct {p0}, Lcom/xiaomi/interconnection/InterconnectionService;->lambda$getPeerIpFromArpTableUntilTimeout$1()V */
return;
} // .end method
static java.lang.String -$$Nest$fgetmPeerP2pIpAddr ( com.xiaomi.interconnection.InterconnectionService p0 ) { //bridge//synthethic
/* .locals 0 */
p0 = this.mPeerP2pIpAddr;
} // .end method
static java.lang.String -$$Nest$fgetmPeerP2pMacAddr ( com.xiaomi.interconnection.InterconnectionService p0 ) { //bridge//synthethic
/* .locals 0 */
p0 = this.mPeerP2pMacAddr;
} // .end method
static void -$$Nest$fputmDeviceP2pIpAddr ( com.xiaomi.interconnection.InterconnectionService p0, java.lang.String p1 ) { //bridge//synthethic
/* .locals 0 */
this.mDeviceP2pIpAddr = p1;
return;
} // .end method
static void -$$Nest$fputmDeviceP2pMacAddr ( com.xiaomi.interconnection.InterconnectionService p0, java.lang.String p1 ) { //bridge//synthethic
/* .locals 0 */
this.mDeviceP2pMacAddr = p1;
return;
} // .end method
static void -$$Nest$fputmIsGo ( com.xiaomi.interconnection.InterconnectionService p0, Boolean p1 ) { //bridge//synthethic
/* .locals 0 */
/* iput-boolean p1, p0, Lcom/xiaomi/interconnection/InterconnectionService;->mIsGo:Z */
return;
} // .end method
static void -$$Nest$fputmPeerP2pIpAddr ( com.xiaomi.interconnection.InterconnectionService p0, java.lang.String p1 ) { //bridge//synthethic
/* .locals 0 */
this.mPeerP2pIpAddr = p1;
return;
} // .end method
static void -$$Nest$fputmPeerP2pMacAddr ( com.xiaomi.interconnection.InterconnectionService p0, java.lang.String p1 ) { //bridge//synthethic
/* .locals 0 */
this.mPeerP2pMacAddr = p1;
return;
} // .end method
static void -$$Nest$fputmSoftApIfaceName ( com.xiaomi.interconnection.InterconnectionService p0, java.lang.String p1 ) { //bridge//synthethic
/* .locals 0 */
this.mSoftApIfaceName = p1;
return;
} // .end method
static java.lang.String -$$Nest$mgetIfaceIpAddr ( com.xiaomi.interconnection.InterconnectionService p0, java.lang.String p1 ) { //bridge//synthethic
/* .locals 0 */
/* invoke-direct {p0, p1}, Lcom/xiaomi/interconnection/InterconnectionService;->getIfaceIpAddr(Ljava/lang/String;)Ljava/lang/String; */
} // .end method
static java.lang.String -$$Nest$mgetIpFromArpTable ( com.xiaomi.interconnection.InterconnectionService p0, java.lang.String p1 ) { //bridge//synthethic
/* .locals 0 */
/* invoke-direct {p0, p1}, Lcom/xiaomi/interconnection/InterconnectionService;->getIpFromArpTable(Ljava/lang/String;)Ljava/lang/String; */
} // .end method
static void -$$Nest$mgetPeerIpFromArpTableUntilTimeout ( com.xiaomi.interconnection.InterconnectionService p0 ) { //bridge//synthethic
/* .locals 0 */
/* invoke-direct {p0}, Lcom/xiaomi/interconnection/InterconnectionService;->getPeerIpFromArpTableUntilTimeout()V */
return;
} // .end method
static void -$$Nest$mnotifySoftApInfoChanged ( com.xiaomi.interconnection.InterconnectionService p0 ) { //bridge//synthethic
/* .locals 0 */
/* invoke-direct {p0}, Lcom/xiaomi/interconnection/InterconnectionService;->notifySoftApInfoChanged()V */
return;
} // .end method
static void -$$Nest$mnotifyWifiP2pInfoChanged ( com.xiaomi.interconnection.InterconnectionService p0 ) { //bridge//synthethic
/* .locals 0 */
/* invoke-direct {p0}, Lcom/xiaomi/interconnection/InterconnectionService;->notifyWifiP2pInfoChanged()V */
return;
} // .end method
static java.lang.String -$$Nest$smgetIfaceMacAddr ( java.lang.String p0 ) { //bridge//synthethic
/* .locals 0 */
com.xiaomi.interconnection.InterconnectionService .getIfaceMacAddr ( p0 );
} // .end method
public com.xiaomi.interconnection.InterconnectionService ( ) {
/* .locals 3 */
/* .param p1, "context" # Landroid/content/Context; */
/* .line 142 */
/* invoke-direct {p0}, Lcom/xiaomi/interconnection/IInterconnectionManager$Stub;-><init>()V */
/* .line 69 */
/* new-instance v0, Landroid/os/Handler; */
/* invoke-direct {v0}, Landroid/os/Handler;-><init>()V */
this.mHandler = v0;
/* .line 71 */
final String v0 = ""; // const-string v0, ""
this.mSoftApIfaceName = v0;
/* .line 72 */
/* new-instance v1, Lcom/xiaomi/interconnection/InterconnectionService$1; */
/* invoke-direct {v1, p0}, Lcom/xiaomi/interconnection/InterconnectionService$1;-><init>(Lcom/xiaomi/interconnection/InterconnectionService;)V */
this.mSoftApCallback = v1;
/* .line 82 */
int v1 = 0; // const/4 v1, 0x0
/* iput-boolean v1, p0, Lcom/xiaomi/interconnection/InterconnectionService;->mIsGo:Z */
/* .line 83 */
this.mDeviceP2pMacAddr = v0;
/* .line 84 */
this.mPeerP2pMacAddr = v0;
/* .line 85 */
this.mDeviceP2pIpAddr = v0;
/* .line 86 */
this.mPeerP2pIpAddr = v0;
/* .line 87 */
/* new-instance v0, Lcom/xiaomi/interconnection/InterconnectionService$2; */
/* invoke-direct {v0, p0}, Lcom/xiaomi/interconnection/InterconnectionService$2;-><init>(Lcom/xiaomi/interconnection/InterconnectionService;)V */
this.mP2pReceiver = v0;
/* .line 143 */
this.mContext = p1;
/* .line 144 */
/* const-string/jumbo v0, "wifi" */
(( android.content.Context ) p1 ).getSystemService ( v0 ); // invoke-virtual {p1, v0}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;
/* check-cast v0, Landroid/net/wifi/WifiManager; */
this.mWifiManager = v0;
/* .line 145 */
v0 = this.mContext;
(( android.content.Context ) v0 ).getPackageManager ( ); // invoke-virtual {v0}, Landroid/content/Context;->getPackageManager()Landroid/content/pm/PackageManager;
this.mPackageManager = v0;
/* .line 147 */
v0 = this.mWifiManager;
/* new-instance v1, Landroid/os/HandlerExecutor; */
v2 = this.mHandler;
/* invoke-direct {v1, v2}, Landroid/os/HandlerExecutor;-><init>(Landroid/os/Handler;)V */
v2 = this.mSoftApCallback;
(( android.net.wifi.WifiManager ) v0 ).registerSoftApCallback ( v1, v2 ); // invoke-virtual {v0, v1, v2}, Landroid/net/wifi/WifiManager;->registerSoftApCallback(Ljava/util/concurrent/Executor;Landroid/net/wifi/WifiManager$SoftApCallback;)V
/* .line 149 */
/* new-instance v0, Landroid/os/RemoteCallbackList; */
/* invoke-direct {v0}, Landroid/os/RemoteCallbackList;-><init>()V */
this.mSoftApCallbackList = v0;
/* .line 150 */
/* new-instance v0, Landroid/os/RemoteCallbackList; */
/* invoke-direct {v0}, Landroid/os/RemoteCallbackList;-><init>()V */
this.mWifiP2pCallbackList = v0;
/* .line 152 */
/* new-instance v0, Landroid/content/IntentFilter; */
/* invoke-direct {v0}, Landroid/content/IntentFilter;-><init>()V */
/* .line 153 */
/* .local v0, "filter":Landroid/content/IntentFilter; */
final String v1 = "android.net.wifi.p2p.CONNECTION_STATE_CHANGE"; // const-string v1, "android.net.wifi.p2p.CONNECTION_STATE_CHANGE"
(( android.content.IntentFilter ) v0 ).addAction ( v1 ); // invoke-virtual {v0, v1}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V
/* .line 154 */
v1 = this.mContext;
v2 = this.mP2pReceiver;
(( android.content.Context ) v1 ).registerReceiver ( v2, v0 ); // invoke-virtual {v1, v2, v0}, Landroid/content/Context;->registerReceiver(Landroid/content/BroadcastReceiver;Landroid/content/IntentFilter;)Landroid/content/Intent;
/* .line 155 */
return;
} // .end method
private java.lang.String getIfaceIpAddr ( java.lang.String p0 ) {
/* .locals 5 */
/* .param p1, "iface" # Ljava/lang/String; */
/* .line 176 */
final String v0 = ""; // const-string v0, ""
/* .line 178 */
/* .local v0, "ipAddr":Ljava/lang/String; */
try { // :try_start_0
java.net.NetworkInterface .getByName ( p1 );
/* .line 179 */
/* .local v1, "nif":Ljava/net/NetworkInterface; */
/* if-nez v1, :cond_0 */
/* .line 180 */
} // :cond_0
(( java.net.NetworkInterface ) v1 ).getInetAddresses ( ); // invoke-virtual {v1}, Ljava/net/NetworkInterface;->getInetAddresses()Ljava/util/Enumeration;
java.util.Collections .list ( v2 );
(( java.util.ArrayList ) v2 ).stream ( ); // invoke-virtual {v2}, Ljava/util/ArrayList;->stream()Ljava/util/stream/Stream;
/* new-instance v3, Lcom/xiaomi/interconnection/InterconnectionService$$ExternalSyntheticLambda0; */
/* invoke-direct {v3}, Lcom/xiaomi/interconnection/InterconnectionService$$ExternalSyntheticLambda0;-><init>()V */
/* .line 181 */
/* .line 182 */
java.util.stream.Collectors .toList ( );
/* check-cast v2, Ljava/util/List; */
/* .line 184 */
v3 = /* .local v2, "ipAddrList":Ljava/util/List;, "Ljava/util/List<Ljava/net/InetAddress;>;" */
int v4 = 1; // const/4 v4, 0x1
/* if-ne v3, v4, :cond_1 */
/* .line 185 */
int v3 = 0; // const/4 v3, 0x0
/* check-cast v3, Ljava/net/InetAddress; */
(( java.net.InetAddress ) v3 ).getHostAddress ( ); // invoke-virtual {v3}, Ljava/net/InetAddress;->getHostAddress()Ljava/lang/String;
/* :try_end_0 */
/* .catch Ljava/net/SocketException; {:try_start_0 ..:try_end_0} :catch_0 */
/* move-object v0, v3 */
/* .line 189 */
} // .end local v1 # "nif":Ljava/net/NetworkInterface;
} // .end local v2 # "ipAddrList":Ljava/util/List;, "Ljava/util/List<Ljava/net/InetAddress;>;"
} // :cond_1
/* .line 187 */
/* :catch_0 */
/* move-exception v1 */
/* .line 188 */
/* .local v1, "e":Ljava/net/SocketException; */
(( java.net.SocketException ) v1 ).printStackTrace ( ); // invoke-virtual {v1}, Ljava/net/SocketException;->printStackTrace()V
/* .line 190 */
} // .end local v1 # "e":Ljava/net/SocketException;
} // :goto_0
} // .end method
private static java.lang.String getIfaceMacAddr ( java.lang.String p0 ) {
/* .locals 9 */
/* .param p0, "iface" # Ljava/lang/String; */
/* .line 158 */
final String v0 = ""; // const-string v0, ""
/* .line 160 */
/* .local v0, "macAddr":Ljava/lang/String; */
try { // :try_start_0
java.net.NetworkInterface .getByName ( p0 );
/* .line 161 */
/* .local v1, "nif":Ljava/net/NetworkInterface; */
/* if-nez v1, :cond_0 */
/* .line 163 */
} // :cond_0
(( java.net.NetworkInterface ) v1 ).getHardwareAddress ( ); // invoke-virtual {v1}, Ljava/net/NetworkInterface;->getHardwareAddress()[B
/* .line 164 */
/* .local v2, "mac":[B */
/* array-length v3, v2 */
/* new-array v3, v3, [Ljava/lang/String; */
/* .line 165 */
/* .local v3, "sa":[Ljava/lang/String; */
int v4 = 0; // const/4 v4, 0x0
/* .local v4, "i":I */
} // :goto_0
/* array-length v5, v2 */
/* if-ge v4, v5, :cond_1 */
/* .line 166 */
final String v5 = "%02x"; // const-string v5, "%02x"
int v6 = 1; // const/4 v6, 0x1
/* new-array v6, v6, [Ljava/lang/Object; */
/* aget-byte v7, v2, v4 */
java.lang.Byte .valueOf ( v7 );
int v8 = 0; // const/4 v8, 0x0
/* aput-object v7, v6, v8 */
java.lang.String .format ( v5,v6 );
/* aput-object v5, v3, v4 */
/* .line 165 */
/* add-int/lit8 v4, v4, 0x1 */
/* .line 168 */
} // .end local v4 # "i":I
} // :cond_1
final String v4 = ":"; // const-string v4, ":"
java.lang.String .join ( v4,v3 );
/* :try_end_0 */
/* .catch Ljava/net/SocketException; {:try_start_0 ..:try_end_0} :catch_0 */
/* move-object v0, v4 */
/* .line 171 */
} // .end local v1 # "nif":Ljava/net/NetworkInterface;
} // .end local v2 # "mac":[B
} // .end local v3 # "sa":[Ljava/lang/String;
/* .line 169 */
/* :catch_0 */
/* move-exception v1 */
/* .line 170 */
/* .local v1, "e":Ljava/net/SocketException; */
(( java.net.SocketException ) v1 ).printStackTrace ( ); // invoke-virtual {v1}, Ljava/net/SocketException;->printStackTrace()V
/* .line 172 */
} // .end local v1 # "e":Ljava/net/SocketException;
} // :goto_1
} // .end method
private java.lang.String getIpFromArpTable ( java.lang.String p0 ) {
/* .locals 8 */
/* .param p1, "macAddr" # Ljava/lang/String; */
/* .line 194 */
final String v0 = ""; // const-string v0, ""
/* .line 195 */
/* .local v0, "ipAddr":Ljava/lang/String; */
try { // :try_start_0
/* new-instance v1, Ljava/io/BufferedReader; */
/* new-instance v2, Ljava/io/FileReader; */
final String v3 = "/proc/net/arp"; // const-string v3, "/proc/net/arp"
/* invoke-direct {v2, v3}, Ljava/io/FileReader;-><init>(Ljava/lang/String;)V */
/* invoke-direct {v1, v2}, Ljava/io/BufferedReader;-><init>(Ljava/io/Reader;)V */
/* :try_end_0 */
/* .catch Ljava/io/FileNotFoundException; {:try_start_0 ..:try_end_0} :catch_1 */
/* .catch Ljava/io/IOException; {:try_start_0 ..:try_end_0} :catch_0 */
/* .line 196 */
/* .local v1, "reader":Ljava/io/BufferedReader; */
try { // :try_start_1
(( java.io.BufferedReader ) v1 ).readLine ( ); // invoke-virtual {v1}, Ljava/io/BufferedReader;->readLine()Ljava/lang/String;
/* .line 198 */
} // :goto_0
(( java.io.BufferedReader ) v1 ).readLine ( ); // invoke-virtual {v1}, Ljava/io/BufferedReader;->readLine()Ljava/lang/String;
/* move-object v3, v2 */
/* .local v3, "line":Ljava/lang/String; */
if ( v2 != null) { // if-eqz v2, :cond_2
/* .line 199 */
final String v2 = "[ ]+"; // const-string v2, "[ ]+"
(( java.lang.String ) v3 ).split ( v2 ); // invoke-virtual {v3, v2}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;
/* .line 200 */
/* .local v2, "tokens":[Ljava/lang/String; */
/* array-length v4, v2 */
int v5 = 6; // const/4 v5, 0x6
/* if-ge v4, v5, :cond_0 */
/* .line 201 */
/* .line 203 */
} // :cond_0
int v4 = 3; // const/4 v4, 0x3
/* aget-object v5, v2, v4 */
/* .line 204 */
/* .local v5, "addr":Ljava/lang/String; */
v6 = (( java.lang.String ) v5 ).length ( ); // invoke-virtual {v5}, Ljava/lang/String;->length()I
/* sub-int/2addr v6, v4 */
int v4 = 0; // const/4 v4, 0x0
(( java.lang.String ) v5 ).substring ( v4, v6 ); // invoke-virtual {v5, v4, v6}, Ljava/lang/String;->substring(II)Ljava/lang/String;
/* .line 205 */
/* .local v6, "subAddr":Ljava/lang/String; */
v7 = (( java.lang.String ) p1 ).contains ( v6 ); // invoke-virtual {p1, v6}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z
if ( v7 != null) { // if-eqz v7, :cond_1
/* .line 206 */
/* aget-object v4, v2, v4 */
/* :try_end_1 */
/* .catchall {:try_start_1 ..:try_end_1} :catchall_0 */
/* move-object v0, v4 */
/* .line 208 */
} // .end local v2 # "tokens":[Ljava/lang/String;
} // .end local v5 # "addr":Ljava/lang/String;
} // .end local v6 # "subAddr":Ljava/lang/String;
} // :cond_1
/* .line 209 */
} // .end local v3 # "line":Ljava/lang/String;
} // :cond_2
try { // :try_start_2
(( java.io.BufferedReader ) v1 ).close ( ); // invoke-virtual {v1}, Ljava/io/BufferedReader;->close()V
/* :try_end_2 */
/* .catch Ljava/io/FileNotFoundException; {:try_start_2 ..:try_end_2} :catch_1 */
/* .catch Ljava/io/IOException; {:try_start_2 ..:try_end_2} :catch_0 */
/* .line 213 */
} // .end local v1 # "reader":Ljava/io/BufferedReader;
/* nop */
/* .line 214 */
/* .line 195 */
/* .restart local v1 # "reader":Ljava/io/BufferedReader; */
/* :catchall_0 */
/* move-exception v2 */
try { // :try_start_3
(( java.io.BufferedReader ) v1 ).close ( ); // invoke-virtual {v1}, Ljava/io/BufferedReader;->close()V
/* :try_end_3 */
/* .catchall {:try_start_3 ..:try_end_3} :catchall_1 */
/* :catchall_1 */
/* move-exception v3 */
try { // :try_start_4
(( java.lang.Throwable ) v2 ).addSuppressed ( v3 ); // invoke-virtual {v2, v3}, Ljava/lang/Throwable;->addSuppressed(Ljava/lang/Throwable;)V
} // .end local v0 # "ipAddr":Ljava/lang/String;
} // .end local p0 # "this":Lcom/xiaomi/interconnection/InterconnectionService;
} // .end local p1 # "macAddr":Ljava/lang/String;
} // :goto_1
/* throw v2 */
/* :try_end_4 */
/* .catch Ljava/io/FileNotFoundException; {:try_start_4 ..:try_end_4} :catch_1 */
/* .catch Ljava/io/IOException; {:try_start_4 ..:try_end_4} :catch_0 */
/* .line 211 */
} // .end local v1 # "reader":Ljava/io/BufferedReader;
/* .restart local v0 # "ipAddr":Ljava/lang/String; */
/* .restart local p0 # "this":Lcom/xiaomi/interconnection/InterconnectionService; */
/* .restart local p1 # "macAddr":Ljava/lang/String; */
/* :catch_0 */
/* move-exception v1 */
/* .line 212 */
/* .local v1, "e":Ljava/io/IOException; */
/* .line 209 */
} // .end local v1 # "e":Ljava/io/IOException;
/* :catch_1 */
/* move-exception v1 */
/* .line 210 */
/* .local v1, "e":Ljava/io/FileNotFoundException; */
} // .end method
private void getPeerIpFromArpTableUntilTimeout ( ) {
/* .locals 6 */
/* .line 218 */
int v0 = 2; // const/4 v0, 0x2
java.util.concurrent.Executors .newScheduledThreadPool ( v0 );
/* .line 219 */
/* .local v0, "executor":Ljava/util/concurrent/ScheduledExecutorService; */
/* new-instance v1, Lcom/xiaomi/interconnection/InterconnectionService$$ExternalSyntheticLambda1; */
/* invoke-direct {v1, p0}, Lcom/xiaomi/interconnection/InterconnectionService$$ExternalSyntheticLambda1;-><init>(Lcom/xiaomi/interconnection/InterconnectionService;)V */
/* .line 227 */
/* .local v1, "future":Ljava/util/concurrent/Future; */
/* new-instance v2, Lcom/xiaomi/interconnection/InterconnectionService$$ExternalSyntheticLambda2; */
/* invoke-direct {v2, v1}, Lcom/xiaomi/interconnection/InterconnectionService$$ExternalSyntheticLambda2;-><init>(Ljava/util/concurrent/Future;)V */
/* .line 231 */
/* .local v2, "cancelTask":Ljava/lang/Runnable; */
/* const-wide/16 v3, 0x7d0 */
v5 = java.util.concurrent.TimeUnit.MILLISECONDS;
/* .line 232 */
/* .line 233 */
return;
} // .end method
static Boolean lambda$getIfaceIpAddr$0 ( java.net.InetAddress p0 ) { //synthethic
/* .locals 1 */
/* .param p0, "ia" # Ljava/net/InetAddress; */
/* .line 181 */
/* instance-of v0, p0, Ljava/net/Inet4Address; */
} // .end method
private void lambda$getPeerIpFromArpTableUntilTimeout$1 ( ) { //synthethic
/* .locals 3 */
/* .line 220 */
int v0 = 0; // const/4 v0, 0x0
/* .line 221 */
/* .local v0, "interrupted":Z */
} // :goto_0
java.lang.Thread .currentThread ( );
v1 = (( java.lang.Thread ) v1 ).isInterrupted ( ); // invoke-virtual {v1}, Ljava/lang/Thread;->isInterrupted()Z
/* move v0, v1 */
/* if-nez v1, :cond_0 */
v1 = this.mPeerP2pMacAddr;
/* .line 222 */
/* invoke-direct {p0, v1}, Lcom/xiaomi/interconnection/InterconnectionService;->getIpFromArpTable(Ljava/lang/String;)Ljava/lang/String; */
this.mPeerP2pIpAddr = v1;
final String v2 = ""; // const-string v2, ""
v1 = (( java.lang.String ) v2 ).equals ( v1 ); // invoke-virtual {v2, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z
if ( v1 != null) { // if-eqz v1, :cond_0
/* .line 223 */
} // :cond_0
/* if-nez v0, :cond_1 */
/* .line 224 */
/* invoke-direct {p0}, Lcom/xiaomi/interconnection/InterconnectionService;->notifyWifiP2pInfoChanged()V */
/* .line 226 */
} // :cond_1
return;
} // .end method
static void lambda$getPeerIpFromArpTableUntilTimeout$2 ( java.util.concurrent.Future p0 ) { //synthethic
/* .locals 2 */
/* .param p0, "future" # Ljava/util/concurrent/Future; */
/* .line 228 */
int v0 = 1; // const/4 v0, 0x1
/* .line 229 */
final String v0 = "InterconnectionService"; // const-string v0, "InterconnectionService"
/* const-string/jumbo v1, "task canceled due to timeout" */
android.util.Log .d ( v0,v1 );
/* .line 230 */
return;
} // .end method
private void notifySoftApInfoChanged ( ) {
/* .locals 5 */
/* .line 236 */
v0 = this.mSoftApCallbackList;
v0 = (( android.os.RemoteCallbackList ) v0 ).beginBroadcast ( ); // invoke-virtual {v0}, Landroid/os/RemoteCallbackList;->beginBroadcast()I
/* .line 237 */
/* .local v0, "itemCount":I */
int v1 = 0; // const/4 v1, 0x0
/* .local v1, "i":I */
} // :goto_0
/* if-ge v1, v0, :cond_0 */
/* .line 239 */
try { // :try_start_0
v2 = this.mSoftApCallbackList;
(( android.os.RemoteCallbackList ) v2 ).getBroadcastItem ( v1 ); // invoke-virtual {v2, v1}, Landroid/os/RemoteCallbackList;->getBroadcastItem(I)Landroid/os/IInterface;
/* check-cast v2, Lcom/xiaomi/interconnection/ISoftApCallback; */
v3 = this.mSoftApIfaceName;
/* :try_end_0 */
/* .catch Landroid/os/RemoteException; {:try_start_0 ..:try_end_0} :catch_0 */
/* .line 242 */
/* .line 240 */
/* :catch_0 */
/* move-exception v2 */
/* .line 241 */
/* .local v2, "e":Landroid/os/RemoteException; */
/* new-instance v3, Ljava/lang/StringBuilder; */
/* invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V */
final String v4 = "onSoftApInfoChanged: remote exception -- "; // const-string v4, "onSoftApInfoChanged: remote exception -- "
(( java.lang.StringBuilder ) v3 ).append ( v4 ); // invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v3 ).append ( v2 ); // invoke-virtual {v3, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v3 ).toString ( ); // invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
final String v4 = "InterconnectionService"; // const-string v4, "InterconnectionService"
android.util.Log .e ( v4,v3 );
/* .line 237 */
} // .end local v2 # "e":Landroid/os/RemoteException;
} // :goto_1
/* add-int/lit8 v1, v1, 0x1 */
/* .line 244 */
} // .end local v1 # "i":I
} // :cond_0
v1 = this.mSoftApCallbackList;
(( android.os.RemoteCallbackList ) v1 ).finishBroadcast ( ); // invoke-virtual {v1}, Landroid/os/RemoteCallbackList;->finishBroadcast()V
/* .line 245 */
return;
} // .end method
private void notifyWifiP2pInfoChanged ( ) {
/* .locals 9 */
/* .line 248 */
v0 = this.mWifiP2pCallbackList;
v0 = (( android.os.RemoteCallbackList ) v0 ).beginBroadcast ( ); // invoke-virtual {v0}, Landroid/os/RemoteCallbackList;->beginBroadcast()I
/* .line 249 */
/* .local v0, "itemCount":I */
int v1 = 0; // const/4 v1, 0x0
/* .local v1, "i":I */
} // :goto_0
/* if-ge v1, v0, :cond_0 */
/* .line 251 */
try { // :try_start_0
/* new-instance v8, Lcom/xiaomi/interconnection/P2pDevicesInfo; */
/* iget-boolean v3, p0, Lcom/xiaomi/interconnection/InterconnectionService;->mIsGo:Z */
v4 = this.mDeviceP2pMacAddr;
v5 = this.mPeerP2pMacAddr;
v6 = this.mDeviceP2pIpAddr;
v7 = this.mPeerP2pIpAddr;
/* move-object v2, v8 */
/* invoke-direct/range {v2 ..v7}, Lcom/xiaomi/interconnection/P2pDevicesInfo;-><init>(ZLjava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V */
/* move-object v2, v8 */
/* .line 253 */
/* .local v2, "info":Lcom/xiaomi/interconnection/P2pDevicesInfo; */
v3 = this.mWifiP2pCallbackList;
(( android.os.RemoteCallbackList ) v3 ).getBroadcastItem ( v1 ); // invoke-virtual {v3, v1}, Landroid/os/RemoteCallbackList;->getBroadcastItem(I)Landroid/os/IInterface;
/* check-cast v3, Lcom/xiaomi/interconnection/IWifiP2pCallback; */
/* :try_end_0 */
/* .catch Landroid/os/RemoteException; {:try_start_0 ..:try_end_0} :catch_0 */
/* .line 256 */
} // .end local v2 # "info":Lcom/xiaomi/interconnection/P2pDevicesInfo;
/* .line 254 */
/* :catch_0 */
/* move-exception v2 */
/* .line 255 */
/* .local v2, "e":Landroid/os/RemoteException; */
/* new-instance v3, Ljava/lang/StringBuilder; */
/* invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V */
final String v4 = "onWifiP2pInfoChanged: remote exception -- "; // const-string v4, "onWifiP2pInfoChanged: remote exception -- "
(( java.lang.StringBuilder ) v3 ).append ( v4 ); // invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v3 ).append ( v2 ); // invoke-virtual {v3, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v3 ).toString ( ); // invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
final String v4 = "InterconnectionService"; // const-string v4, "InterconnectionService"
android.util.Log .e ( v4,v3 );
/* .line 249 */
} // .end local v2 # "e":Landroid/os/RemoteException;
} // :goto_1
/* add-int/lit8 v1, v1, 0x1 */
/* .line 258 */
} // .end local v1 # "i":I
} // :cond_0
v1 = this.mWifiP2pCallbackList;
(( android.os.RemoteCallbackList ) v1 ).finishBroadcast ( ); // invoke-virtual {v1}, Landroid/os/RemoteCallbackList;->finishBroadcast()V
/* .line 259 */
return;
} // .end method
/* # virtual methods */
public java.lang.String getWifiChipModel ( ) {
/* .locals 4 */
/* .line 263 */
final String v0 = ""; // const-string v0, ""
/* .line 264 */
/* .local v0, "wifiChipModel":Ljava/lang/String; */
/* const-string/jumbo v1, "vendor" */
miui.util.FeatureParser .getString ( v1 );
final String v2 = "mediatek"; // const-string v2, "mediatek"
v1 = (( java.lang.String ) v2 ).equals ( v1 ); // invoke-virtual {v2, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z
/* .line 265 */
/* .local v1, "isMtk":Z */
/* const-string/jumbo v2, "unknown" */
if ( v1 != null) { // if-eqz v1, :cond_0
/* .line 266 */
/* const-string/jumbo v3, "vendor.connsys.wifi.adie.chipid" */
android.os.SystemProperties .get ( v3,v2 );
/* .line 268 */
} // :cond_0
final String v3 = "ro.hardware.wlan.chip"; // const-string v3, "ro.hardware.wlan.chip"
android.os.SystemProperties .get ( v3,v2 );
/* .line 271 */
} // :goto_0
} // .end method
public void notifyConcurrentNetworkState ( Boolean p0 ) {
/* .locals 2 */
/* .param p1, "mcc" # Z */
/* .line 345 */
/* new-instance v0, Ljava/lang/StringBuilder; */
/* invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V */
final String v1 = "mcc: "; // const-string v1, "mcc: "
(( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v0 ).append ( p1 ); // invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v0 ).toString ( ); // invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
final String v1 = "InterconnectionService"; // const-string v1, "InterconnectionService"
android.util.Log .d ( v1,v0 );
/* .line 347 */
return;
} // .end method
public void registerSoftApCallback ( com.xiaomi.interconnection.ISoftApCallback p0 ) {
/* .locals 1 */
/* .param p1, "cb" # Lcom/xiaomi/interconnection/ISoftApCallback; */
/* .line 325 */
v0 = this.mSoftApCallbackList;
(( android.os.RemoteCallbackList ) v0 ).register ( p1 ); // invoke-virtual {v0, p1}, Landroid/os/RemoteCallbackList;->register(Landroid/os/IInterface;)Z
/* .line 326 */
return;
} // .end method
public void registerWifiP2pCallback ( com.xiaomi.interconnection.IWifiP2pCallback p0 ) {
/* .locals 1 */
/* .param p1, "cb" # Lcom/xiaomi/interconnection/IWifiP2pCallback; */
/* .line 335 */
v0 = this.mWifiP2pCallbackList;
(( android.os.RemoteCallbackList ) v0 ).register ( p1 ); // invoke-virtual {v0, p1}, Landroid/os/RemoteCallbackList;->register(Landroid/os/IInterface;)Z
/* .line 336 */
return;
} // .end method
public Integer supportDbs ( ) {
/* .locals 4 */
/* .line 307 */
int v0 = 0; // const/4 v0, 0x0
/* .line 308 */
/* .local v0, "dbs":I */
final String v1 = "ro.hardware.wlan.dbs"; // const-string v1, "ro.hardware.wlan.dbs"
/* const-string/jumbo v2, "unknown" */
android.os.SystemProperties .get ( v1,v2 );
/* .line 309 */
/* .local v1, "dbsProp":Ljava/lang/String; */
v3 = (( java.lang.String ) v2 ).equals ( v1 ); // invoke-virtual {v2, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z
if ( v3 != null) { // if-eqz v3, :cond_3
/* .line 310 */
/* const-string/jumbo v3, "vendor.connsys.adie.chipid" */
android.os.SystemProperties .get ( v3,v2 );
/* .line 311 */
/* .local v3, "chipProp":Ljava/lang/String; */
v2 = (( java.lang.String ) v2 ).equals ( v3 ); // invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z
/* if-nez v2, :cond_2 */
/* .line 312 */
final String v2 = "0x6635"; // const-string v2, "0x6635"
v2 = (( java.lang.String ) v2 ).equals ( v3 ); // invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z
/* if-nez v2, :cond_1 */
final String v2 = "0x6637"; // const-string v2, "0x6637"
v2 = (( java.lang.String ) v2 ).equals ( v3 ); // invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z
if ( v2 != null) { // if-eqz v2, :cond_0
} // :cond_0
int v2 = 2; // const/4 v2, 0x2
} // :cond_1
} // :goto_0
int v2 = 1; // const/4 v2, 0x1
} // :goto_1
/* move v0, v2 */
/* .line 314 */
} // .end local v3 # "chipProp":Ljava/lang/String;
} // :cond_2
} // :cond_3
final String v2 = "1"; // const-string v2, "1"
v2 = (( java.lang.String ) v2 ).equals ( v1 ); // invoke-virtual {v2, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z
if ( v2 != null) { // if-eqz v2, :cond_4
/* .line 315 */
int v0 = 1; // const/4 v0, 0x1
/* .line 316 */
} // :cond_4
final String v2 = "2"; // const-string v2, "2"
v2 = (( java.lang.String ) v2 ).equals ( v1 ); // invoke-virtual {v2, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z
if ( v2 != null) { // if-eqz v2, :cond_5
/* .line 317 */
int v0 = 2; // const/4 v0, 0x2
/* .line 320 */
} // :cond_5
} // :goto_2
} // .end method
public Boolean supportHbs ( ) {
/* .locals 8 */
/* .line 286 */
v0 = this.mContext;
(( android.content.Context ) v0 ).getContentResolver ( ); // invoke-virtual {v0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;
final String v1 = "cloud_wifi_hbs_support"; // const-string v1, "cloud_wifi_hbs_support"
android.provider.Settings$System .getString ( v0,v1 );
/* .line 288 */
/* .local v0, "cloudvalue":Ljava/lang/String; */
final String v1 = "off"; // const-string v1, "off"
v1 = (( java.lang.String ) v1 ).equals ( v0 ); // invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z
int v2 = 0; // const/4 v2, 0x0
if ( v1 != null) { // if-eqz v1, :cond_0
/* .line 289 */
/* .line 292 */
} // :cond_0
int v1 = 0; // const/4 v1, 0x0
/* .line 294 */
/* .local v1, "support":Z */
try { // :try_start_0
v3 = this.mContext;
(( android.content.Context ) v3 ).getResources ( ); // invoke-virtual {v3}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;
v4 = this.mContext;
/* .line 295 */
(( android.content.Context ) v4 ).getResources ( ); // invoke-virtual {v4}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;
final String v5 = "config_wifi_hbs_support"; // const-string v5, "config_wifi_hbs_support"
final String v6 = "bool"; // const-string v6, "bool"
final String v7 = "android.miui"; // const-string v7, "android.miui"
v4 = (( android.content.res.Resources ) v4 ).getIdentifier ( v5, v6, v7 ); // invoke-virtual {v4, v5, v6, v7}, Landroid/content/res/Resources;->getIdentifier(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)I
/* .line 294 */
v2 = (( android.content.res.Resources ) v3 ).getBoolean ( v4 ); // invoke-virtual {v3, v4}, Landroid/content/res/Resources;->getBoolean(I)Z
/* :try_end_0 */
/* .catch Ljava/lang/Exception; {:try_start_0 ..:try_end_0} :catch_0 */
/* move v1, v2 */
/* .line 300 */
/* nop */
/* .line 302 */
/* .line 297 */
/* :catch_0 */
/* move-exception v3 */
/* .line 298 */
/* .local v3, "exception":Ljava/lang/Exception; */
final String v4 = "InterconnectionService"; // const-string v4, "InterconnectionService"
final String v5 = "config for wifi hbs not found"; // const-string v5, "config for wifi hbs not found"
android.util.Log .e ( v4,v5 );
/* .line 299 */
} // .end method
public Boolean supportP2p160Mode ( ) {
/* .locals 2 */
/* .line 281 */
v0 = this.mPackageManager;
/* const-string/jumbo v1, "xiaomi.hardware.p2p_160m" */
v0 = (( android.content.pm.PackageManager ) v0 ).hasSystemFeature ( v1 ); // invoke-virtual {v0, v1}, Landroid/content/pm/PackageManager;->hasSystemFeature(Ljava/lang/String;)Z
} // .end method
public Boolean supportP2pChannel165 ( ) {
/* .locals 2 */
/* .line 276 */
v0 = this.mPackageManager;
/* const-string/jumbo v1, "xiaomi.hardware.p2p_165chan" */
v0 = (( android.content.pm.PackageManager ) v0 ).hasSystemFeature ( v1 ); // invoke-virtual {v0, v1}, Landroid/content/pm/PackageManager;->hasSystemFeature(Ljava/lang/String;)Z
} // .end method
public void unregisterSoftApCallback ( com.xiaomi.interconnection.ISoftApCallback p0 ) {
/* .locals 1 */
/* .param p1, "cb" # Lcom/xiaomi/interconnection/ISoftApCallback; */
/* .line 330 */
v0 = this.mSoftApCallbackList;
(( android.os.RemoteCallbackList ) v0 ).unregister ( p1 ); // invoke-virtual {v0, p1}, Landroid/os/RemoteCallbackList;->unregister(Landroid/os/IInterface;)Z
/* .line 331 */
return;
} // .end method
public void unregisterWifiP2pCallback ( com.xiaomi.interconnection.IWifiP2pCallback p0 ) {
/* .locals 1 */
/* .param p1, "cb" # Lcom/xiaomi/interconnection/IWifiP2pCallback; */
/* .line 340 */
v0 = this.mWifiP2pCallbackList;
(( android.os.RemoteCallbackList ) v0 ).unregister ( p1 ); // invoke-virtual {v0, p1}, Landroid/os/RemoteCallbackList;->unregister(Landroid/os/IInterface;)Z
/* .line 341 */
return;
} // .end method
