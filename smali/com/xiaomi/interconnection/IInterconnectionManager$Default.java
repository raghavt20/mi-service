public class com.xiaomi.interconnection.IInterconnectionManager$Default implements com.xiaomi.interconnection.IInterconnectionManager {
	 /* .source "IInterconnectionManager.java" */
	 /* # interfaces */
	 /* # annotations */
	 /* .annotation system Ldalvik/annotation/EnclosingClass; */
	 /* value = Lcom/xiaomi/interconnection/IInterconnectionManager; */
} // .end annotation
/* .annotation system Ldalvik/annotation/InnerClass; */
/* accessFlags = 0x9 */
/* name = "Default" */
} // .end annotation
/* # direct methods */
public com.xiaomi.interconnection.IInterconnectionManager$Default ( ) {
/* .locals 0 */
/* .line 8 */
/* invoke-direct {p0}, Ljava/lang/Object;-><init>()V */
return;
} // .end method
/* # virtual methods */
public android.os.IBinder asBinder ( ) {
/* .locals 1 */
/* .line 47 */
int v0 = 0; // const/4 v0, 0x0
} // .end method
public java.lang.String getWifiChipModel ( ) {
/* .locals 1 */
/* .annotation system Ldalvik/annotation/Throws; */
/* value = { */
/* Landroid/os/RemoteException; */
/* } */
} // .end annotation
/* .line 12 */
int v0 = 0; // const/4 v0, 0x0
} // .end method
public void notifyConcurrentNetworkState ( Boolean p0 ) {
/* .locals 0 */
/* .param p1, "mcc" # Z */
/* .annotation system Ldalvik/annotation/Throws; */
/* value = { */
/* Landroid/os/RemoteException; */
/* } */
} // .end annotation
/* .line 44 */
return;
} // .end method
public void registerSoftApCallback ( com.xiaomi.interconnection.ISoftApCallback p0 ) {
/* .locals 0 */
/* .param p1, "cb" # Lcom/xiaomi/interconnection/ISoftApCallback; */
/* .annotation system Ldalvik/annotation/Throws; */
/* value = { */
/* Landroid/os/RemoteException; */
/* } */
} // .end annotation
/* .line 32 */
return;
} // .end method
public void registerWifiP2pCallback ( com.xiaomi.interconnection.IWifiP2pCallback p0 ) {
/* .locals 0 */
/* .param p1, "cb" # Lcom/xiaomi/interconnection/IWifiP2pCallback; */
/* .annotation system Ldalvik/annotation/Throws; */
/* value = { */
/* Landroid/os/RemoteException; */
/* } */
} // .end annotation
/* .line 38 */
return;
} // .end method
public Integer supportDbs ( ) {
/* .locals 1 */
/* .annotation system Ldalvik/annotation/Throws; */
/* value = { */
/* Landroid/os/RemoteException; */
/* } */
} // .end annotation
/* .line 28 */
int v0 = 0; // const/4 v0, 0x0
} // .end method
public Boolean supportHbs ( ) {
/* .locals 1 */
/* .annotation system Ldalvik/annotation/Throws; */
/* value = { */
/* Landroid/os/RemoteException; */
/* } */
} // .end annotation
/* .line 24 */
int v0 = 0; // const/4 v0, 0x0
} // .end method
public Boolean supportP2p160Mode ( ) {
/* .locals 1 */
/* .annotation system Ldalvik/annotation/Throws; */
/* value = { */
/* Landroid/os/RemoteException; */
/* } */
} // .end annotation
/* .line 20 */
int v0 = 0; // const/4 v0, 0x0
} // .end method
public Boolean supportP2pChannel165 ( ) {
/* .locals 1 */
/* .annotation system Ldalvik/annotation/Throws; */
/* value = { */
/* Landroid/os/RemoteException; */
/* } */
} // .end annotation
/* .line 16 */
int v0 = 0; // const/4 v0, 0x0
} // .end method
public void unregisterSoftApCallback ( com.xiaomi.interconnection.ISoftApCallback p0 ) {
/* .locals 0 */
/* .param p1, "cb" # Lcom/xiaomi/interconnection/ISoftApCallback; */
/* .annotation system Ldalvik/annotation/Throws; */
/* value = { */
/* Landroid/os/RemoteException; */
/* } */
} // .end annotation
/* .line 35 */
return;
} // .end method
public void unregisterWifiP2pCallback ( com.xiaomi.interconnection.IWifiP2pCallback p0 ) {
/* .locals 0 */
/* .param p1, "cb" # Lcom/xiaomi/interconnection/IWifiP2pCallback; */
/* .annotation system Ldalvik/annotation/Throws; */
/* value = { */
/* Landroid/os/RemoteException; */
/* } */
} // .end annotation
/* .line 41 */
return;
} // .end method
