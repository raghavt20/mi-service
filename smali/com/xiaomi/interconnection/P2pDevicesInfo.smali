.class public Lcom/xiaomi/interconnection/P2pDevicesInfo;
.super Ljava/lang/Object;
.source "P2pDevicesInfo.java"

# interfaces
.implements Landroid/os/Parcelable;


# static fields
.field public static final CREATOR:Landroid/os/Parcelable$Creator;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/os/Parcelable$Creator<",
            "Lcom/xiaomi/interconnection/P2pDevicesInfo;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field private mIsGo:Z

.field private mPeerDeviceIpAddr:Ljava/lang/String;

.field private mPeerDeviceMacAddr:Ljava/lang/String;

.field private mThisDeviceIpAddr:Ljava/lang/String;

.field private mThisDeviceMacAddr:Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .line 50
    new-instance v0, Lcom/xiaomi/interconnection/P2pDevicesInfo$1;

    invoke-direct {v0}, Lcom/xiaomi/interconnection/P2pDevicesInfo$1;-><init>()V

    sput-object v0, Lcom/xiaomi/interconnection/P2pDevicesInfo;->CREATOR:Landroid/os/Parcelable$Creator;

    return-void
.end method

.method protected constructor <init>(Landroid/os/Parcel;)V
    .locals 1
    .param p1, "in"    # Landroid/os/Parcel;

    .line 22
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 7
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/xiaomi/interconnection/P2pDevicesInfo;->mIsGo:Z

    .line 8
    const-string v0, ""

    iput-object v0, p0, Lcom/xiaomi/interconnection/P2pDevicesInfo;->mThisDeviceMacAddr:Ljava/lang/String;

    .line 9
    iput-object v0, p0, Lcom/xiaomi/interconnection/P2pDevicesInfo;->mPeerDeviceMacAddr:Ljava/lang/String;

    .line 10
    iput-object v0, p0, Lcom/xiaomi/interconnection/P2pDevicesInfo;->mThisDeviceIpAddr:Ljava/lang/String;

    .line 11
    iput-object v0, p0, Lcom/xiaomi/interconnection/P2pDevicesInfo;->mPeerDeviceIpAddr:Ljava/lang/String;

    .line 23
    invoke-virtual {p1}, Landroid/os/Parcel;->readBoolean()Z

    move-result v0

    iput-boolean v0, p0, Lcom/xiaomi/interconnection/P2pDevicesInfo;->mIsGo:Z

    .line 24
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/xiaomi/interconnection/P2pDevicesInfo;->mThisDeviceMacAddr:Ljava/lang/String;

    .line 25
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/xiaomi/interconnection/P2pDevicesInfo;->mPeerDeviceMacAddr:Ljava/lang/String;

    .line 26
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/xiaomi/interconnection/P2pDevicesInfo;->mThisDeviceIpAddr:Ljava/lang/String;

    .line 27
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/xiaomi/interconnection/P2pDevicesInfo;->mPeerDeviceIpAddr:Ljava/lang/String;

    .line 28
    return-void
.end method

.method public constructor <init>(ZLjava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V
    .locals 1
    .param p1, "isGo"    # Z
    .param p2, "thisDeviceMacAddr"    # Ljava/lang/String;
    .param p3, "peerDeviceMacAddr"    # Ljava/lang/String;
    .param p4, "thisDeviceIpAddr"    # Ljava/lang/String;
    .param p5, "peerDeviceIpAddr"    # Ljava/lang/String;

    .line 14
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 7
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/xiaomi/interconnection/P2pDevicesInfo;->mIsGo:Z

    .line 8
    const-string v0, ""

    iput-object v0, p0, Lcom/xiaomi/interconnection/P2pDevicesInfo;->mThisDeviceMacAddr:Ljava/lang/String;

    .line 9
    iput-object v0, p0, Lcom/xiaomi/interconnection/P2pDevicesInfo;->mPeerDeviceMacAddr:Ljava/lang/String;

    .line 10
    iput-object v0, p0, Lcom/xiaomi/interconnection/P2pDevicesInfo;->mThisDeviceIpAddr:Ljava/lang/String;

    .line 11
    iput-object v0, p0, Lcom/xiaomi/interconnection/P2pDevicesInfo;->mPeerDeviceIpAddr:Ljava/lang/String;

    .line 15
    iput-boolean p1, p0, Lcom/xiaomi/interconnection/P2pDevicesInfo;->mIsGo:Z

    .line 16
    iput-object p2, p0, Lcom/xiaomi/interconnection/P2pDevicesInfo;->mThisDeviceMacAddr:Ljava/lang/String;

    .line 17
    iput-object p3, p0, Lcom/xiaomi/interconnection/P2pDevicesInfo;->mPeerDeviceMacAddr:Ljava/lang/String;

    .line 18
    iput-object p4, p0, Lcom/xiaomi/interconnection/P2pDevicesInfo;->mThisDeviceIpAddr:Ljava/lang/String;

    .line 19
    iput-object p5, p0, Lcom/xiaomi/interconnection/P2pDevicesInfo;->mPeerDeviceIpAddr:Ljava/lang/String;

    .line 20
    return-void
.end method


# virtual methods
.method public describeContents()I
    .locals 1

    .line 64
    const/4 v0, 0x0

    return v0
.end method

.method public getPeerDeviceIpAddr()Ljava/lang/String;
    .locals 1

    .line 47
    iget-object v0, p0, Lcom/xiaomi/interconnection/P2pDevicesInfo;->mPeerDeviceIpAddr:Ljava/lang/String;

    return-object v0
.end method

.method public getPeerDeviceMacAddr()Ljava/lang/String;
    .locals 1

    .line 39
    iget-object v0, p0, Lcom/xiaomi/interconnection/P2pDevicesInfo;->mPeerDeviceMacAddr:Ljava/lang/String;

    return-object v0
.end method

.method public getThisDeviceIpAddr()Ljava/lang/String;
    .locals 1

    .line 43
    iget-object v0, p0, Lcom/xiaomi/interconnection/P2pDevicesInfo;->mThisDeviceIpAddr:Ljava/lang/String;

    return-object v0
.end method

.method public getThisDeviceMacAddr()Ljava/lang/String;
    .locals 1

    .line 35
    iget-object v0, p0, Lcom/xiaomi/interconnection/P2pDevicesInfo;->mThisDeviceMacAddr:Ljava/lang/String;

    return-object v0
.end method

.method public isGo()Z
    .locals 1

    .line 31
    iget-boolean v0, p0, Lcom/xiaomi/interconnection/P2pDevicesInfo;->mIsGo:Z

    return v0
.end method

.method public toString()Ljava/lang/String;
    .locals 3

    .line 78
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    .line 79
    .local v0, "sbuf":Ljava/lang/StringBuilder;
    const-string v1, "P2pDevicesInfo{"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 80
    const-string v1, "isGo= "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-boolean v2, p0, Lcom/xiaomi/interconnection/P2pDevicesInfo;->mIsGo:Z

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    .line 81
    const-string v1, ", device mac= "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "*"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 82
    const-string v1, ", peer mac= "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 83
    const-string v1, ", device ip= "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 84
    const-string v1, ", peer ip= "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 85
    const-string/jumbo v1, "}"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 86
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    return-object v1
.end method

.method public writeToParcel(Landroid/os/Parcel;I)V
    .locals 1
    .param p1, "dest"    # Landroid/os/Parcel;
    .param p2, "flags"    # I

    .line 69
    iget-boolean v0, p0, Lcom/xiaomi/interconnection/P2pDevicesInfo;->mIsGo:Z

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeBoolean(Z)V

    .line 70
    iget-object v0, p0, Lcom/xiaomi/interconnection/P2pDevicesInfo;->mThisDeviceMacAddr:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 71
    iget-object v0, p0, Lcom/xiaomi/interconnection/P2pDevicesInfo;->mPeerDeviceMacAddr:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 72
    iget-object v0, p0, Lcom/xiaomi/interconnection/P2pDevicesInfo;->mThisDeviceIpAddr:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 73
    iget-object v0, p0, Lcom/xiaomi/interconnection/P2pDevicesInfo;->mPeerDeviceIpAddr:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 74
    return-void
.end method
