class com.xiaomi.interconnection.InterconnectionManager$WifiP2pCallbackProxy extends com.xiaomi.interconnection.IWifiP2pCallback$Stub {
	 /* .source "InterconnectionManager.java" */
	 /* # annotations */
	 /* .annotation system Ldalvik/annotation/EnclosingClass; */
	 /* value = Lcom/xiaomi/interconnection/InterconnectionManager; */
} // .end annotation
/* .annotation system Ldalvik/annotation/InnerClass; */
/* accessFlags = 0x2 */
/* name = "WifiP2pCallbackProxy" */
} // .end annotation
/* # instance fields */
private final com.xiaomi.interconnection.InterconnectionManager$WifiP2pCallback mCallback;
final com.xiaomi.interconnection.InterconnectionManager this$0; //synthetic
/* # direct methods */
public static void $r8$lambda$Sqwh_lEJw-zivKoXw4x_5TaNt8Q ( com.xiaomi.interconnection.InterconnectionManager$WifiP2pCallbackProxy p0, com.xiaomi.interconnection.P2pDevicesInfo p1 ) { //synthethic
/* .locals 0 */
/* invoke-direct {p0, p1}, Lcom/xiaomi/interconnection/InterconnectionManager$WifiP2pCallbackProxy;->lambda$onDevicesInfoChanged$0(Lcom/xiaomi/interconnection/P2pDevicesInfo;)V */
return;
} // .end method
private com.xiaomi.interconnection.InterconnectionManager$WifiP2pCallbackProxy ( ) {
/* .locals 0 */
/* .param p2, "callback" # Lcom/xiaomi/interconnection/InterconnectionManager$WifiP2pCallback; */
/* .line 114 */
this.this$0 = p1;
/* invoke-direct {p0}, Lcom/xiaomi/interconnection/IWifiP2pCallback$Stub;-><init>()V */
/* .line 115 */
this.mCallback = p2;
/* .line 116 */
return;
} // .end method
 com.xiaomi.interconnection.InterconnectionManager$WifiP2pCallbackProxy ( ) { //synthethic
/* .locals 0 */
/* invoke-direct {p0, p1, p2}, Lcom/xiaomi/interconnection/InterconnectionManager$WifiP2pCallbackProxy;-><init>(Lcom/xiaomi/interconnection/InterconnectionManager;Lcom/xiaomi/interconnection/InterconnectionManager$WifiP2pCallback;)V */
return;
} // .end method
private void lambda$onDevicesInfoChanged$0 ( com.xiaomi.interconnection.P2pDevicesInfo p0 ) { //synthethic
/* .locals 1 */
/* .param p1, "info" # Lcom/xiaomi/interconnection/P2pDevicesInfo; */
/* .line 121 */
v0 = this.mCallback;
/* .line 122 */
return;
} // .end method
/* # virtual methods */
public void onDevicesInfoChanged ( com.xiaomi.interconnection.P2pDevicesInfo p0 ) {
/* .locals 2 */
/* .param p1, "info" # Lcom/xiaomi/interconnection/P2pDevicesInfo; */
/* .annotation system Ldalvik/annotation/Throws; */
/* value = { */
/* Landroid/os/RemoteException; */
/* } */
} // .end annotation
/* .line 120 */
v0 = this.this$0;
com.xiaomi.interconnection.InterconnectionManager .-$$Nest$fgetmHandler ( v0 );
/* new-instance v1, Lcom/xiaomi/interconnection/InterconnectionManager$WifiP2pCallbackProxy$$ExternalSyntheticLambda0; */
/* invoke-direct {v1, p0, p1}, Lcom/xiaomi/interconnection/InterconnectionManager$WifiP2pCallbackProxy$$ExternalSyntheticLambda0;-><init>(Lcom/xiaomi/interconnection/InterconnectionManager$WifiP2pCallbackProxy;Lcom/xiaomi/interconnection/P2pDevicesInfo;)V */
(( android.os.Handler ) v0 ).post ( v1 ); // invoke-virtual {v0, v1}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z
/* .line 123 */
return;
} // .end method
