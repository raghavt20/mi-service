.class public abstract Lcom/xiaomi/interconnection/IInterconnectionManager$Stub;
.super Landroid/os/Binder;
.source "IInterconnectionManager.java"

# interfaces
.implements Lcom/xiaomi/interconnection/IInterconnectionManager;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/xiaomi/interconnection/IInterconnectionManager;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x409
    name = "Stub"
.end annotation

.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/xiaomi/interconnection/IInterconnectionManager$Stub$Proxy;
    }
.end annotation


# static fields
.field static final TRANSACTION_getWifiChipModel:I = 0x1

.field static final TRANSACTION_notifyConcurrentNetworkState:I = 0xa

.field static final TRANSACTION_registerSoftApCallback:I = 0x6

.field static final TRANSACTION_registerWifiP2pCallback:I = 0x8

.field static final TRANSACTION_supportDbs:I = 0x5

.field static final TRANSACTION_supportHbs:I = 0x4

.field static final TRANSACTION_supportP2p160Mode:I = 0x3

.field static final TRANSACTION_supportP2pChannel165:I = 0x2

.field static final TRANSACTION_unregisterSoftApCallback:I = 0x7

.field static final TRANSACTION_unregisterWifiP2pCallback:I = 0x9


# direct methods
.method public constructor <init>()V
    .locals 1

    .line 55
    invoke-direct {p0}, Landroid/os/Binder;-><init>()V

    .line 56
    const-string v0, "com.xiaomi.interconnection.IInterconnectionManager"

    invoke-virtual {p0, p0, v0}, Lcom/xiaomi/interconnection/IInterconnectionManager$Stub;->attachInterface(Landroid/os/IInterface;Ljava/lang/String;)V

    .line 57
    return-void
.end method

.method public static asInterface(Landroid/os/IBinder;)Lcom/xiaomi/interconnection/IInterconnectionManager;
    .locals 2
    .param p0, "obj"    # Landroid/os/IBinder;

    .line 64
    if-nez p0, :cond_0

    .line 65
    const/4 v0, 0x0

    return-object v0

    .line 67
    :cond_0
    const-string v0, "com.xiaomi.interconnection.IInterconnectionManager"

    invoke-interface {p0, v0}, Landroid/os/IBinder;->queryLocalInterface(Ljava/lang/String;)Landroid/os/IInterface;

    move-result-object v0

    .line 68
    .local v0, "iin":Landroid/os/IInterface;
    if-eqz v0, :cond_1

    instance-of v1, v0, Lcom/xiaomi/interconnection/IInterconnectionManager;

    if-eqz v1, :cond_1

    .line 69
    move-object v1, v0

    check-cast v1, Lcom/xiaomi/interconnection/IInterconnectionManager;

    return-object v1

    .line 71
    :cond_1
    new-instance v1, Lcom/xiaomi/interconnection/IInterconnectionManager$Stub$Proxy;

    invoke-direct {v1, p0}, Lcom/xiaomi/interconnection/IInterconnectionManager$Stub$Proxy;-><init>(Landroid/os/IBinder;)V

    return-object v1
.end method

.method public static getDefaultTransactionName(I)Ljava/lang/String;
    .locals 1
    .param p0, "transactionCode"    # I

    .line 80
    packed-switch p0, :pswitch_data_0

    .line 124
    const/4 v0, 0x0

    return-object v0

    .line 120
    :pswitch_0
    const-string v0, "notifyConcurrentNetworkState"

    return-object v0

    .line 116
    :pswitch_1
    const-string/jumbo v0, "unregisterWifiP2pCallback"

    return-object v0

    .line 112
    :pswitch_2
    const-string v0, "registerWifiP2pCallback"

    return-object v0

    .line 108
    :pswitch_3
    const-string/jumbo v0, "unregisterSoftApCallback"

    return-object v0

    .line 104
    :pswitch_4
    const-string v0, "registerSoftApCallback"

    return-object v0

    .line 100
    :pswitch_5
    const-string/jumbo v0, "supportDbs"

    return-object v0

    .line 96
    :pswitch_6
    const-string/jumbo v0, "supportHbs"

    return-object v0

    .line 92
    :pswitch_7
    const-string/jumbo v0, "supportP2p160Mode"

    return-object v0

    .line 88
    :pswitch_8
    const-string/jumbo v0, "supportP2pChannel165"

    return-object v0

    .line 84
    :pswitch_9
    const-string v0, "getWifiChipModel"

    return-object v0

    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_9
        :pswitch_8
        :pswitch_7
        :pswitch_6
        :pswitch_5
        :pswitch_4
        :pswitch_3
        :pswitch_2
        :pswitch_1
        :pswitch_0
    .end packed-switch
.end method


# virtual methods
.method public asBinder()Landroid/os/IBinder;
    .locals 0

    .line 75
    return-object p0
.end method

.method public getMaxTransactionId()I
    .locals 1

    .line 425
    const/16 v0, 0x9

    return v0
.end method

.method public getTransactionName(I)Ljava/lang/String;
    .locals 1
    .param p1, "transactionCode"    # I

    .line 131
    invoke-static {p1}, Lcom/xiaomi/interconnection/IInterconnectionManager$Stub;->getDefaultTransactionName(I)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public onTransact(ILandroid/os/Parcel;Landroid/os/Parcel;I)Z
    .locals 3
    .param p1, "code"    # I
    .param p2, "data"    # Landroid/os/Parcel;
    .param p3, "reply"    # Landroid/os/Parcel;
    .param p4, "flags"    # I
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    .line 135
    const-string v0, "com.xiaomi.interconnection.IInterconnectionManager"

    .line 136
    .local v0, "descriptor":Ljava/lang/String;
    const/4 v1, 0x1

    if-lt p1, v1, :cond_0

    const v2, 0xffffff

    if-gt p1, v2, :cond_0

    .line 137
    invoke-virtual {p2, v0}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    .line 139
    :cond_0
    packed-switch p1, :pswitch_data_0

    .line 147
    packed-switch p1, :pswitch_data_1

    .line 231
    invoke-super {p0, p1, p2, p3, p4}, Landroid/os/Binder;->onTransact(ILandroid/os/Parcel;Landroid/os/Parcel;I)Z

    move-result v1

    return v1

    .line 143
    :pswitch_0
    invoke-virtual {p3, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 144
    return v1

    .line 223
    :pswitch_1
    invoke-virtual {p2}, Landroid/os/Parcel;->readBoolean()Z

    move-result v2

    .line 224
    .local v2, "_arg0":Z
    invoke-virtual {p2}, Landroid/os/Parcel;->enforceNoDataAvail()V

    .line 225
    invoke-virtual {p0, v2}, Lcom/xiaomi/interconnection/IInterconnectionManager$Stub;->notifyConcurrentNetworkState(Z)V

    .line 226
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    .line 227
    goto/16 :goto_0

    .line 214
    .end local v2    # "_arg0":Z
    :pswitch_2
    invoke-virtual {p2}, Landroid/os/Parcel;->readStrongBinder()Landroid/os/IBinder;

    move-result-object v2

    invoke-static {v2}, Lcom/xiaomi/interconnection/IWifiP2pCallback$Stub;->asInterface(Landroid/os/IBinder;)Lcom/xiaomi/interconnection/IWifiP2pCallback;

    move-result-object v2

    .line 215
    .local v2, "_arg0":Lcom/xiaomi/interconnection/IWifiP2pCallback;
    invoke-virtual {p2}, Landroid/os/Parcel;->enforceNoDataAvail()V

    .line 216
    invoke-virtual {p0, v2}, Lcom/xiaomi/interconnection/IInterconnectionManager$Stub;->unregisterWifiP2pCallback(Lcom/xiaomi/interconnection/IWifiP2pCallback;)V

    .line 217
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    .line 218
    goto :goto_0

    .line 205
    .end local v2    # "_arg0":Lcom/xiaomi/interconnection/IWifiP2pCallback;
    :pswitch_3
    invoke-virtual {p2}, Landroid/os/Parcel;->readStrongBinder()Landroid/os/IBinder;

    move-result-object v2

    invoke-static {v2}, Lcom/xiaomi/interconnection/IWifiP2pCallback$Stub;->asInterface(Landroid/os/IBinder;)Lcom/xiaomi/interconnection/IWifiP2pCallback;

    move-result-object v2

    .line 206
    .restart local v2    # "_arg0":Lcom/xiaomi/interconnection/IWifiP2pCallback;
    invoke-virtual {p2}, Landroid/os/Parcel;->enforceNoDataAvail()V

    .line 207
    invoke-virtual {p0, v2}, Lcom/xiaomi/interconnection/IInterconnectionManager$Stub;->registerWifiP2pCallback(Lcom/xiaomi/interconnection/IWifiP2pCallback;)V

    .line 208
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    .line 209
    goto :goto_0

    .line 196
    .end local v2    # "_arg0":Lcom/xiaomi/interconnection/IWifiP2pCallback;
    :pswitch_4
    invoke-virtual {p2}, Landroid/os/Parcel;->readStrongBinder()Landroid/os/IBinder;

    move-result-object v2

    invoke-static {v2}, Lcom/xiaomi/interconnection/ISoftApCallback$Stub;->asInterface(Landroid/os/IBinder;)Lcom/xiaomi/interconnection/ISoftApCallback;

    move-result-object v2

    .line 197
    .local v2, "_arg0":Lcom/xiaomi/interconnection/ISoftApCallback;
    invoke-virtual {p2}, Landroid/os/Parcel;->enforceNoDataAvail()V

    .line 198
    invoke-virtual {p0, v2}, Lcom/xiaomi/interconnection/IInterconnectionManager$Stub;->unregisterSoftApCallback(Lcom/xiaomi/interconnection/ISoftApCallback;)V

    .line 199
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    .line 200
    goto :goto_0

    .line 187
    .end local v2    # "_arg0":Lcom/xiaomi/interconnection/ISoftApCallback;
    :pswitch_5
    invoke-virtual {p2}, Landroid/os/Parcel;->readStrongBinder()Landroid/os/IBinder;

    move-result-object v2

    invoke-static {v2}, Lcom/xiaomi/interconnection/ISoftApCallback$Stub;->asInterface(Landroid/os/IBinder;)Lcom/xiaomi/interconnection/ISoftApCallback;

    move-result-object v2

    .line 188
    .restart local v2    # "_arg0":Lcom/xiaomi/interconnection/ISoftApCallback;
    invoke-virtual {p2}, Landroid/os/Parcel;->enforceNoDataAvail()V

    .line 189
    invoke-virtual {p0, v2}, Lcom/xiaomi/interconnection/IInterconnectionManager$Stub;->registerSoftApCallback(Lcom/xiaomi/interconnection/ISoftApCallback;)V

    .line 190
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    .line 191
    goto :goto_0

    .line 179
    .end local v2    # "_arg0":Lcom/xiaomi/interconnection/ISoftApCallback;
    :pswitch_6
    invoke-virtual {p0}, Lcom/xiaomi/interconnection/IInterconnectionManager$Stub;->supportDbs()I

    move-result v2

    .line 180
    .local v2, "_result":I
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    .line 181
    invoke-virtual {p3, v2}, Landroid/os/Parcel;->writeInt(I)V

    .line 182
    goto :goto_0

    .line 172
    .end local v2    # "_result":I
    :pswitch_7
    invoke-virtual {p0}, Lcom/xiaomi/interconnection/IInterconnectionManager$Stub;->supportHbs()Z

    move-result v2

    .line 173
    .local v2, "_result":Z
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    .line 174
    invoke-virtual {p3, v2}, Landroid/os/Parcel;->writeBoolean(Z)V

    .line 175
    goto :goto_0

    .line 165
    .end local v2    # "_result":Z
    :pswitch_8
    invoke-virtual {p0}, Lcom/xiaomi/interconnection/IInterconnectionManager$Stub;->supportP2p160Mode()Z

    move-result v2

    .line 166
    .restart local v2    # "_result":Z
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    .line 167
    invoke-virtual {p3, v2}, Landroid/os/Parcel;->writeBoolean(Z)V

    .line 168
    goto :goto_0

    .line 158
    .end local v2    # "_result":Z
    :pswitch_9
    invoke-virtual {p0}, Lcom/xiaomi/interconnection/IInterconnectionManager$Stub;->supportP2pChannel165()Z

    move-result v2

    .line 159
    .restart local v2    # "_result":Z
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    .line 160
    invoke-virtual {p3, v2}, Landroid/os/Parcel;->writeBoolean(Z)V

    .line 161
    goto :goto_0

    .line 151
    .end local v2    # "_result":Z
    :pswitch_a
    invoke-virtual {p0}, Lcom/xiaomi/interconnection/IInterconnectionManager$Stub;->getWifiChipModel()Ljava/lang/String;

    move-result-object v2

    .line 152
    .local v2, "_result":Ljava/lang/String;
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    .line 153
    invoke-virtual {p3, v2}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 154
    nop

    .line 234
    .end local v2    # "_result":Ljava/lang/String;
    :goto_0
    return v1

    nop

    :pswitch_data_0
    .packed-switch 0x5f4e5446
        :pswitch_0
    .end packed-switch

    :pswitch_data_1
    .packed-switch 0x1
        :pswitch_a
        :pswitch_9
        :pswitch_8
        :pswitch_7
        :pswitch_6
        :pswitch_5
        :pswitch_4
        :pswitch_3
        :pswitch_2
        :pswitch_1
    .end packed-switch
.end method
