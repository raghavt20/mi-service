.class public Lcom/xiaomi/interconnection/IInterconnectionManager$Default;
.super Ljava/lang/Object;
.source "IInterconnectionManager.java"

# interfaces
.implements Lcom/xiaomi/interconnection/IInterconnectionManager;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/xiaomi/interconnection/IInterconnectionManager;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x9
    name = "Default"
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 0

    .line 8
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public asBinder()Landroid/os/IBinder;
    .locals 1

    .line 47
    const/4 v0, 0x0

    return-object v0
.end method

.method public getWifiChipModel()Ljava/lang/String;
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    .line 12
    const/4 v0, 0x0

    return-object v0
.end method

.method public notifyConcurrentNetworkState(Z)V
    .locals 0
    .param p1, "mcc"    # Z
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    .line 44
    return-void
.end method

.method public registerSoftApCallback(Lcom/xiaomi/interconnection/ISoftApCallback;)V
    .locals 0
    .param p1, "cb"    # Lcom/xiaomi/interconnection/ISoftApCallback;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    .line 32
    return-void
.end method

.method public registerWifiP2pCallback(Lcom/xiaomi/interconnection/IWifiP2pCallback;)V
    .locals 0
    .param p1, "cb"    # Lcom/xiaomi/interconnection/IWifiP2pCallback;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    .line 38
    return-void
.end method

.method public supportDbs()I
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    .line 28
    const/4 v0, 0x0

    return v0
.end method

.method public supportHbs()Z
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    .line 24
    const/4 v0, 0x0

    return v0
.end method

.method public supportP2p160Mode()Z
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    .line 20
    const/4 v0, 0x0

    return v0
.end method

.method public supportP2pChannel165()Z
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    .line 16
    const/4 v0, 0x0

    return v0
.end method

.method public unregisterSoftApCallback(Lcom/xiaomi/interconnection/ISoftApCallback;)V
    .locals 0
    .param p1, "cb"    # Lcom/xiaomi/interconnection/ISoftApCallback;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    .line 35
    return-void
.end method

.method public unregisterWifiP2pCallback(Lcom/xiaomi/interconnection/IWifiP2pCallback;)V
    .locals 0
    .param p1, "cb"    # Lcom/xiaomi/interconnection/IWifiP2pCallback;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    .line 41
    return-void
.end method
