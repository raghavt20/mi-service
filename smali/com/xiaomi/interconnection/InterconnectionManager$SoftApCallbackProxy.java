class com.xiaomi.interconnection.InterconnectionManager$SoftApCallbackProxy extends com.xiaomi.interconnection.ISoftApCallback$Stub {
	 /* .source "InterconnectionManager.java" */
	 /* # annotations */
	 /* .annotation system Ldalvik/annotation/EnclosingClass; */
	 /* value = Lcom/xiaomi/interconnection/InterconnectionManager; */
} // .end annotation
/* .annotation system Ldalvik/annotation/InnerClass; */
/* accessFlags = 0x2 */
/* name = "SoftApCallbackProxy" */
} // .end annotation
/* # instance fields */
private final com.xiaomi.interconnection.InterconnectionManager$SoftApCallback mCallback;
final com.xiaomi.interconnection.InterconnectionManager this$0; //synthetic
/* # direct methods */
public static void $r8$lambda$EdbrSW7mHjASOBT229K0IcRRWwo ( com.xiaomi.interconnection.InterconnectionManager$SoftApCallbackProxy p0, java.lang.String p1 ) { //synthethic
/* .locals 0 */
/* invoke-direct {p0, p1}, Lcom/xiaomi/interconnection/InterconnectionManager$SoftApCallbackProxy;->lambda$onIfaceInfoChanged$0(Ljava/lang/String;)V */
return;
} // .end method
private com.xiaomi.interconnection.InterconnectionManager$SoftApCallbackProxy ( ) {
/* .locals 0 */
/* .param p2, "callback" # Lcom/xiaomi/interconnection/InterconnectionManager$SoftApCallback; */
/* .line 54 */
this.this$0 = p1;
/* invoke-direct {p0}, Lcom/xiaomi/interconnection/ISoftApCallback$Stub;-><init>()V */
/* .line 55 */
this.mCallback = p2;
/* .line 56 */
return;
} // .end method
 com.xiaomi.interconnection.InterconnectionManager$SoftApCallbackProxy ( ) { //synthethic
/* .locals 0 */
/* invoke-direct {p0, p1, p2}, Lcom/xiaomi/interconnection/InterconnectionManager$SoftApCallbackProxy;-><init>(Lcom/xiaomi/interconnection/InterconnectionManager;Lcom/xiaomi/interconnection/InterconnectionManager$SoftApCallback;)V */
return;
} // .end method
private void lambda$onIfaceInfoChanged$0 ( java.lang.String p0 ) { //synthethic
/* .locals 1 */
/* .param p1, "softApIfaceName" # Ljava/lang/String; */
/* .line 61 */
v0 = this.mCallback;
/* .line 62 */
return;
} // .end method
/* # virtual methods */
public void onIfaceInfoChanged ( java.lang.String p0 ) {
/* .locals 2 */
/* .param p1, "softApIfaceName" # Ljava/lang/String; */
/* .annotation system Ldalvik/annotation/Throws; */
/* value = { */
/* Landroid/os/RemoteException; */
/* } */
} // .end annotation
/* .line 60 */
v0 = this.this$0;
com.xiaomi.interconnection.InterconnectionManager .-$$Nest$fgetmHandler ( v0 );
/* new-instance v1, Lcom/xiaomi/interconnection/InterconnectionManager$SoftApCallbackProxy$$ExternalSyntheticLambda0; */
/* invoke-direct {v1, p0, p1}, Lcom/xiaomi/interconnection/InterconnectionManager$SoftApCallbackProxy$$ExternalSyntheticLambda0;-><init>(Lcom/xiaomi/interconnection/InterconnectionManager$SoftApCallbackProxy;Ljava/lang/String;)V */
(( android.os.Handler ) v0 ).post ( v1 ); // invoke-virtual {v0, v1}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z
/* .line 63 */
return;
} // .end method
