public class com.xiaomi.interconnection.P2pDevicesInfo implements android.os.Parcelable {
	 /* .source "P2pDevicesInfo.java" */
	 /* # interfaces */
	 /* # static fields */
	 public static final android.os.Parcelable$Creator CREATOR;
	 /* .annotation system Ldalvik/annotation/Signature; */
	 /* value = { */
	 /* "Landroid/os/Parcelable$Creator<", */
	 /* "Lcom/xiaomi/interconnection/P2pDevicesInfo;", */
	 /* ">;" */
	 /* } */
} // .end annotation
} // .end field
/* # instance fields */
private Boolean mIsGo;
private java.lang.String mPeerDeviceIpAddr;
private java.lang.String mPeerDeviceMacAddr;
private java.lang.String mThisDeviceIpAddr;
private java.lang.String mThisDeviceMacAddr;
/* # direct methods */
static com.xiaomi.interconnection.P2pDevicesInfo ( ) {
/* .locals 1 */
/* .line 50 */
/* new-instance v0, Lcom/xiaomi/interconnection/P2pDevicesInfo$1; */
/* invoke-direct {v0}, Lcom/xiaomi/interconnection/P2pDevicesInfo$1;-><init>()V */
return;
} // .end method
protected com.xiaomi.interconnection.P2pDevicesInfo ( ) {
/* .locals 1 */
/* .param p1, "in" # Landroid/os/Parcel; */
/* .line 22 */
/* invoke-direct {p0}, Ljava/lang/Object;-><init>()V */
/* .line 7 */
int v0 = 0; // const/4 v0, 0x0
/* iput-boolean v0, p0, Lcom/xiaomi/interconnection/P2pDevicesInfo;->mIsGo:Z */
/* .line 8 */
final String v0 = ""; // const-string v0, ""
this.mThisDeviceMacAddr = v0;
/* .line 9 */
this.mPeerDeviceMacAddr = v0;
/* .line 10 */
this.mThisDeviceIpAddr = v0;
/* .line 11 */
this.mPeerDeviceIpAddr = v0;
/* .line 23 */
v0 = (( android.os.Parcel ) p1 ).readBoolean ( ); // invoke-virtual {p1}, Landroid/os/Parcel;->readBoolean()Z
/* iput-boolean v0, p0, Lcom/xiaomi/interconnection/P2pDevicesInfo;->mIsGo:Z */
/* .line 24 */
(( android.os.Parcel ) p1 ).readString ( ); // invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;
this.mThisDeviceMacAddr = v0;
/* .line 25 */
(( android.os.Parcel ) p1 ).readString ( ); // invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;
this.mPeerDeviceMacAddr = v0;
/* .line 26 */
(( android.os.Parcel ) p1 ).readString ( ); // invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;
this.mThisDeviceIpAddr = v0;
/* .line 27 */
(( android.os.Parcel ) p1 ).readString ( ); // invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;
this.mPeerDeviceIpAddr = v0;
/* .line 28 */
return;
} // .end method
public com.xiaomi.interconnection.P2pDevicesInfo ( ) {
/* .locals 1 */
/* .param p1, "isGo" # Z */
/* .param p2, "thisDeviceMacAddr" # Ljava/lang/String; */
/* .param p3, "peerDeviceMacAddr" # Ljava/lang/String; */
/* .param p4, "thisDeviceIpAddr" # Ljava/lang/String; */
/* .param p5, "peerDeviceIpAddr" # Ljava/lang/String; */
/* .line 14 */
/* invoke-direct {p0}, Ljava/lang/Object;-><init>()V */
/* .line 7 */
int v0 = 0; // const/4 v0, 0x0
/* iput-boolean v0, p0, Lcom/xiaomi/interconnection/P2pDevicesInfo;->mIsGo:Z */
/* .line 8 */
final String v0 = ""; // const-string v0, ""
this.mThisDeviceMacAddr = v0;
/* .line 9 */
this.mPeerDeviceMacAddr = v0;
/* .line 10 */
this.mThisDeviceIpAddr = v0;
/* .line 11 */
this.mPeerDeviceIpAddr = v0;
/* .line 15 */
/* iput-boolean p1, p0, Lcom/xiaomi/interconnection/P2pDevicesInfo;->mIsGo:Z */
/* .line 16 */
this.mThisDeviceMacAddr = p2;
/* .line 17 */
this.mPeerDeviceMacAddr = p3;
/* .line 18 */
this.mThisDeviceIpAddr = p4;
/* .line 19 */
this.mPeerDeviceIpAddr = p5;
/* .line 20 */
return;
} // .end method
/* # virtual methods */
public Integer describeContents ( ) {
/* .locals 1 */
/* .line 64 */
int v0 = 0; // const/4 v0, 0x0
} // .end method
public java.lang.String getPeerDeviceIpAddr ( ) {
/* .locals 1 */
/* .line 47 */
v0 = this.mPeerDeviceIpAddr;
} // .end method
public java.lang.String getPeerDeviceMacAddr ( ) {
/* .locals 1 */
/* .line 39 */
v0 = this.mPeerDeviceMacAddr;
} // .end method
public java.lang.String getThisDeviceIpAddr ( ) {
/* .locals 1 */
/* .line 43 */
v0 = this.mThisDeviceIpAddr;
} // .end method
public java.lang.String getThisDeviceMacAddr ( ) {
/* .locals 1 */
/* .line 35 */
v0 = this.mThisDeviceMacAddr;
} // .end method
public Boolean isGo ( ) {
/* .locals 1 */
/* .line 31 */
/* iget-boolean v0, p0, Lcom/xiaomi/interconnection/P2pDevicesInfo;->mIsGo:Z */
} // .end method
public java.lang.String toString ( ) {
/* .locals 3 */
/* .line 78 */
/* new-instance v0, Ljava/lang/StringBuilder; */
/* invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V */
/* .line 79 */
/* .local v0, "sbuf":Ljava/lang/StringBuilder; */
final String v1 = "P2pDevicesInfo{"; // const-string v1, "P2pDevicesInfo{"
(( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
/* .line 80 */
final String v1 = "isGo= "; // const-string v1, "isGo= "
(( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
/* iget-boolean v2, p0, Lcom/xiaomi/interconnection/P2pDevicesInfo;->mIsGo:Z */
(( java.lang.StringBuilder ) v1 ).append ( v2 ); // invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;
/* .line 81 */
final String v1 = ", device mac= "; // const-string v1, ", device mac= "
(( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
final String v2 = "*"; // const-string v2, "*"
(( java.lang.StringBuilder ) v1 ).append ( v2 ); // invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
/* .line 82 */
final String v1 = ", peer mac= "; // const-string v1, ", peer mac= "
(( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v1 ).append ( v2 ); // invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
/* .line 83 */
final String v1 = ", device ip= "; // const-string v1, ", device ip= "
(( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v1 ).append ( v2 ); // invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
/* .line 84 */
final String v1 = ", peer ip= "; // const-string v1, ", peer ip= "
(( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v1 ).append ( v2 ); // invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
/* .line 85 */
/* const-string/jumbo v1, "}" */
(( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
/* .line 86 */
(( java.lang.StringBuilder ) v0 ).toString ( ); // invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
} // .end method
public void writeToParcel ( android.os.Parcel p0, Integer p1 ) {
/* .locals 1 */
/* .param p1, "dest" # Landroid/os/Parcel; */
/* .param p2, "flags" # I */
/* .line 69 */
/* iget-boolean v0, p0, Lcom/xiaomi/interconnection/P2pDevicesInfo;->mIsGo:Z */
(( android.os.Parcel ) p1 ).writeBoolean ( v0 ); // invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeBoolean(Z)V
/* .line 70 */
v0 = this.mThisDeviceMacAddr;
(( android.os.Parcel ) p1 ).writeString ( v0 ); // invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V
/* .line 71 */
v0 = this.mPeerDeviceMacAddr;
(( android.os.Parcel ) p1 ).writeString ( v0 ); // invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V
/* .line 72 */
v0 = this.mThisDeviceIpAddr;
(( android.os.Parcel ) p1 ).writeString ( v0 ); // invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V
/* .line 73 */
v0 = this.mPeerDeviceIpAddr;
(( android.os.Parcel ) p1 ).writeString ( v0 ); // invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V
/* .line 74 */
return;
} // .end method
