.class public Lcom/xiaomi/interconnection/InterconnectionService;
.super Lcom/xiaomi/interconnection/IInterconnectionManager$Stub;
.source "InterconnectionService.java"


# static fields
.field private static final ARP_TABLE_PATH:Ljava/lang/String; = "/proc/net/arp"

.field private static final FEATURE_P2P_160M:Ljava/lang/String; = "xiaomi.hardware.p2p_160m"

.field private static final FEATURE_P2P_165CHAN:Ljava/lang/String; = "xiaomi.hardware.p2p_165chan"

.field public static final SERVICE_NAME:Ljava/lang/String; = "xiaomi.InterconnectionService"

.field public static final TAG:Ljava/lang/String; = "InterconnectionService"


# instance fields
.field private mContext:Landroid/content/Context;

.field private mDeviceP2pIpAddr:Ljava/lang/String;

.field private mDeviceP2pMacAddr:Ljava/lang/String;

.field private mHandler:Landroid/os/Handler;

.field private mIsGo:Z

.field private mP2pReceiver:Landroid/content/BroadcastReceiver;

.field private mPackageManager:Landroid/content/pm/PackageManager;

.field private mPeerP2pIpAddr:Ljava/lang/String;

.field private mPeerP2pMacAddr:Ljava/lang/String;

.field private mSoftApCallback:Landroid/net/wifi/WifiManager$SoftApCallback;

.field private mSoftApCallbackList:Landroid/os/RemoteCallbackList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/os/RemoteCallbackList<",
            "Lcom/xiaomi/interconnection/ISoftApCallback;",
            ">;"
        }
    .end annotation
.end field

.field private mSoftApIfaceName:Ljava/lang/String;

.field private mWifiManager:Landroid/net/wifi/WifiManager;

.field private mWifiP2pCallbackList:Landroid/os/RemoteCallbackList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/os/RemoteCallbackList<",
            "Lcom/xiaomi/interconnection/IWifiP2pCallback;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public static synthetic $r8$lambda$V0E8mio_FV3tSjxDGYR0ApNjA8g(Lcom/xiaomi/interconnection/InterconnectionService;)V
    .locals 0

    invoke-direct {p0}, Lcom/xiaomi/interconnection/InterconnectionService;->lambda$getPeerIpFromArpTableUntilTimeout$1()V

    return-void
.end method

.method static bridge synthetic -$$Nest$fgetmPeerP2pIpAddr(Lcom/xiaomi/interconnection/InterconnectionService;)Ljava/lang/String;
    .locals 0

    iget-object p0, p0, Lcom/xiaomi/interconnection/InterconnectionService;->mPeerP2pIpAddr:Ljava/lang/String;

    return-object p0
.end method

.method static bridge synthetic -$$Nest$fgetmPeerP2pMacAddr(Lcom/xiaomi/interconnection/InterconnectionService;)Ljava/lang/String;
    .locals 0

    iget-object p0, p0, Lcom/xiaomi/interconnection/InterconnectionService;->mPeerP2pMacAddr:Ljava/lang/String;

    return-object p0
.end method

.method static bridge synthetic -$$Nest$fputmDeviceP2pIpAddr(Lcom/xiaomi/interconnection/InterconnectionService;Ljava/lang/String;)V
    .locals 0

    iput-object p1, p0, Lcom/xiaomi/interconnection/InterconnectionService;->mDeviceP2pIpAddr:Ljava/lang/String;

    return-void
.end method

.method static bridge synthetic -$$Nest$fputmDeviceP2pMacAddr(Lcom/xiaomi/interconnection/InterconnectionService;Ljava/lang/String;)V
    .locals 0

    iput-object p1, p0, Lcom/xiaomi/interconnection/InterconnectionService;->mDeviceP2pMacAddr:Ljava/lang/String;

    return-void
.end method

.method static bridge synthetic -$$Nest$fputmIsGo(Lcom/xiaomi/interconnection/InterconnectionService;Z)V
    .locals 0

    iput-boolean p1, p0, Lcom/xiaomi/interconnection/InterconnectionService;->mIsGo:Z

    return-void
.end method

.method static bridge synthetic -$$Nest$fputmPeerP2pIpAddr(Lcom/xiaomi/interconnection/InterconnectionService;Ljava/lang/String;)V
    .locals 0

    iput-object p1, p0, Lcom/xiaomi/interconnection/InterconnectionService;->mPeerP2pIpAddr:Ljava/lang/String;

    return-void
.end method

.method static bridge synthetic -$$Nest$fputmPeerP2pMacAddr(Lcom/xiaomi/interconnection/InterconnectionService;Ljava/lang/String;)V
    .locals 0

    iput-object p1, p0, Lcom/xiaomi/interconnection/InterconnectionService;->mPeerP2pMacAddr:Ljava/lang/String;

    return-void
.end method

.method static bridge synthetic -$$Nest$fputmSoftApIfaceName(Lcom/xiaomi/interconnection/InterconnectionService;Ljava/lang/String;)V
    .locals 0

    iput-object p1, p0, Lcom/xiaomi/interconnection/InterconnectionService;->mSoftApIfaceName:Ljava/lang/String;

    return-void
.end method

.method static bridge synthetic -$$Nest$mgetIfaceIpAddr(Lcom/xiaomi/interconnection/InterconnectionService;Ljava/lang/String;)Ljava/lang/String;
    .locals 0

    invoke-direct {p0, p1}, Lcom/xiaomi/interconnection/InterconnectionService;->getIfaceIpAddr(Ljava/lang/String;)Ljava/lang/String;

    move-result-object p0

    return-object p0
.end method

.method static bridge synthetic -$$Nest$mgetIpFromArpTable(Lcom/xiaomi/interconnection/InterconnectionService;Ljava/lang/String;)Ljava/lang/String;
    .locals 0

    invoke-direct {p0, p1}, Lcom/xiaomi/interconnection/InterconnectionService;->getIpFromArpTable(Ljava/lang/String;)Ljava/lang/String;

    move-result-object p0

    return-object p0
.end method

.method static bridge synthetic -$$Nest$mgetPeerIpFromArpTableUntilTimeout(Lcom/xiaomi/interconnection/InterconnectionService;)V
    .locals 0

    invoke-direct {p0}, Lcom/xiaomi/interconnection/InterconnectionService;->getPeerIpFromArpTableUntilTimeout()V

    return-void
.end method

.method static bridge synthetic -$$Nest$mnotifySoftApInfoChanged(Lcom/xiaomi/interconnection/InterconnectionService;)V
    .locals 0

    invoke-direct {p0}, Lcom/xiaomi/interconnection/InterconnectionService;->notifySoftApInfoChanged()V

    return-void
.end method

.method static bridge synthetic -$$Nest$mnotifyWifiP2pInfoChanged(Lcom/xiaomi/interconnection/InterconnectionService;)V
    .locals 0

    invoke-direct {p0}, Lcom/xiaomi/interconnection/InterconnectionService;->notifyWifiP2pInfoChanged()V

    return-void
.end method

.method static bridge synthetic -$$Nest$smgetIfaceMacAddr(Ljava/lang/String;)Ljava/lang/String;
    .locals 0

    invoke-static {p0}, Lcom/xiaomi/interconnection/InterconnectionService;->getIfaceMacAddr(Ljava/lang/String;)Ljava/lang/String;

    move-result-object p0

    return-object p0
.end method

.method public constructor <init>(Landroid/content/Context;)V
    .locals 3
    .param p1, "context"    # Landroid/content/Context;

    .line 142
    invoke-direct {p0}, Lcom/xiaomi/interconnection/IInterconnectionManager$Stub;-><init>()V

    .line 69
    new-instance v0, Landroid/os/Handler;

    invoke-direct {v0}, Landroid/os/Handler;-><init>()V

    iput-object v0, p0, Lcom/xiaomi/interconnection/InterconnectionService;->mHandler:Landroid/os/Handler;

    .line 71
    const-string v0, ""

    iput-object v0, p0, Lcom/xiaomi/interconnection/InterconnectionService;->mSoftApIfaceName:Ljava/lang/String;

    .line 72
    new-instance v1, Lcom/xiaomi/interconnection/InterconnectionService$1;

    invoke-direct {v1, p0}, Lcom/xiaomi/interconnection/InterconnectionService$1;-><init>(Lcom/xiaomi/interconnection/InterconnectionService;)V

    iput-object v1, p0, Lcom/xiaomi/interconnection/InterconnectionService;->mSoftApCallback:Landroid/net/wifi/WifiManager$SoftApCallback;

    .line 82
    const/4 v1, 0x0

    iput-boolean v1, p0, Lcom/xiaomi/interconnection/InterconnectionService;->mIsGo:Z

    .line 83
    iput-object v0, p0, Lcom/xiaomi/interconnection/InterconnectionService;->mDeviceP2pMacAddr:Ljava/lang/String;

    .line 84
    iput-object v0, p0, Lcom/xiaomi/interconnection/InterconnectionService;->mPeerP2pMacAddr:Ljava/lang/String;

    .line 85
    iput-object v0, p0, Lcom/xiaomi/interconnection/InterconnectionService;->mDeviceP2pIpAddr:Ljava/lang/String;

    .line 86
    iput-object v0, p0, Lcom/xiaomi/interconnection/InterconnectionService;->mPeerP2pIpAddr:Ljava/lang/String;

    .line 87
    new-instance v0, Lcom/xiaomi/interconnection/InterconnectionService$2;

    invoke-direct {v0, p0}, Lcom/xiaomi/interconnection/InterconnectionService$2;-><init>(Lcom/xiaomi/interconnection/InterconnectionService;)V

    iput-object v0, p0, Lcom/xiaomi/interconnection/InterconnectionService;->mP2pReceiver:Landroid/content/BroadcastReceiver;

    .line 143
    iput-object p1, p0, Lcom/xiaomi/interconnection/InterconnectionService;->mContext:Landroid/content/Context;

    .line 144
    const-string/jumbo v0, "wifi"

    invoke-virtual {p1, v0}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/net/wifi/WifiManager;

    iput-object v0, p0, Lcom/xiaomi/interconnection/InterconnectionService;->mWifiManager:Landroid/net/wifi/WifiManager;

    .line 145
    iget-object v0, p0, Lcom/xiaomi/interconnection/InterconnectionService;->mContext:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v0

    iput-object v0, p0, Lcom/xiaomi/interconnection/InterconnectionService;->mPackageManager:Landroid/content/pm/PackageManager;

    .line 147
    iget-object v0, p0, Lcom/xiaomi/interconnection/InterconnectionService;->mWifiManager:Landroid/net/wifi/WifiManager;

    new-instance v1, Landroid/os/HandlerExecutor;

    iget-object v2, p0, Lcom/xiaomi/interconnection/InterconnectionService;->mHandler:Landroid/os/Handler;

    invoke-direct {v1, v2}, Landroid/os/HandlerExecutor;-><init>(Landroid/os/Handler;)V

    iget-object v2, p0, Lcom/xiaomi/interconnection/InterconnectionService;->mSoftApCallback:Landroid/net/wifi/WifiManager$SoftApCallback;

    invoke-virtual {v0, v1, v2}, Landroid/net/wifi/WifiManager;->registerSoftApCallback(Ljava/util/concurrent/Executor;Landroid/net/wifi/WifiManager$SoftApCallback;)V

    .line 149
    new-instance v0, Landroid/os/RemoteCallbackList;

    invoke-direct {v0}, Landroid/os/RemoteCallbackList;-><init>()V

    iput-object v0, p0, Lcom/xiaomi/interconnection/InterconnectionService;->mSoftApCallbackList:Landroid/os/RemoteCallbackList;

    .line 150
    new-instance v0, Landroid/os/RemoteCallbackList;

    invoke-direct {v0}, Landroid/os/RemoteCallbackList;-><init>()V

    iput-object v0, p0, Lcom/xiaomi/interconnection/InterconnectionService;->mWifiP2pCallbackList:Landroid/os/RemoteCallbackList;

    .line 152
    new-instance v0, Landroid/content/IntentFilter;

    invoke-direct {v0}, Landroid/content/IntentFilter;-><init>()V

    .line 153
    .local v0, "filter":Landroid/content/IntentFilter;
    const-string v1, "android.net.wifi.p2p.CONNECTION_STATE_CHANGE"

    invoke-virtual {v0, v1}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    .line 154
    iget-object v1, p0, Lcom/xiaomi/interconnection/InterconnectionService;->mContext:Landroid/content/Context;

    iget-object v2, p0, Lcom/xiaomi/interconnection/InterconnectionService;->mP2pReceiver:Landroid/content/BroadcastReceiver;

    invoke-virtual {v1, v2, v0}, Landroid/content/Context;->registerReceiver(Landroid/content/BroadcastReceiver;Landroid/content/IntentFilter;)Landroid/content/Intent;

    .line 155
    return-void
.end method

.method private getIfaceIpAddr(Ljava/lang/String;)Ljava/lang/String;
    .locals 5
    .param p1, "iface"    # Ljava/lang/String;

    .line 176
    const-string v0, ""

    .line 178
    .local v0, "ipAddr":Ljava/lang/String;
    :try_start_0
    invoke-static {p1}, Ljava/net/NetworkInterface;->getByName(Ljava/lang/String;)Ljava/net/NetworkInterface;

    move-result-object v1

    .line 179
    .local v1, "nif":Ljava/net/NetworkInterface;
    if-nez v1, :cond_0

    return-object v0

    .line 180
    :cond_0
    invoke-virtual {v1}, Ljava/net/NetworkInterface;->getInetAddresses()Ljava/util/Enumeration;

    move-result-object v2

    invoke-static {v2}, Ljava/util/Collections;->list(Ljava/util/Enumeration;)Ljava/util/ArrayList;

    move-result-object v2

    invoke-virtual {v2}, Ljava/util/ArrayList;->stream()Ljava/util/stream/Stream;

    move-result-object v2

    new-instance v3, Lcom/xiaomi/interconnection/InterconnectionService$$ExternalSyntheticLambda0;

    invoke-direct {v3}, Lcom/xiaomi/interconnection/InterconnectionService$$ExternalSyntheticLambda0;-><init>()V

    .line 181
    invoke-interface {v2, v3}, Ljava/util/stream/Stream;->filter(Ljava/util/function/Predicate;)Ljava/util/stream/Stream;

    move-result-object v2

    .line 182
    invoke-static {}, Ljava/util/stream/Collectors;->toList()Ljava/util/stream/Collector;

    move-result-object v3

    invoke-interface {v2, v3}, Ljava/util/stream/Stream;->collect(Ljava/util/stream/Collector;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/util/List;

    .line 184
    .local v2, "ipAddrList":Ljava/util/List;, "Ljava/util/List<Ljava/net/InetAddress;>;"
    invoke-interface {v2}, Ljava/util/List;->size()I

    move-result v3

    const/4 v4, 0x1

    if-ne v3, v4, :cond_1

    .line 185
    const/4 v3, 0x0

    invoke-interface {v2, v3}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/net/InetAddress;

    invoke-virtual {v3}, Ljava/net/InetAddress;->getHostAddress()Ljava/lang/String;

    move-result-object v3
    :try_end_0
    .catch Ljava/net/SocketException; {:try_start_0 .. :try_end_0} :catch_0

    move-object v0, v3

    .line 189
    .end local v1    # "nif":Ljava/net/NetworkInterface;
    .end local v2    # "ipAddrList":Ljava/util/List;, "Ljava/util/List<Ljava/net/InetAddress;>;"
    :cond_1
    goto :goto_0

    .line 187
    :catch_0
    move-exception v1

    .line 188
    .local v1, "e":Ljava/net/SocketException;
    invoke-virtual {v1}, Ljava/net/SocketException;->printStackTrace()V

    .line 190
    .end local v1    # "e":Ljava/net/SocketException;
    :goto_0
    return-object v0
.end method

.method private static getIfaceMacAddr(Ljava/lang/String;)Ljava/lang/String;
    .locals 9
    .param p0, "iface"    # Ljava/lang/String;

    .line 158
    const-string v0, ""

    .line 160
    .local v0, "macAddr":Ljava/lang/String;
    :try_start_0
    invoke-static {p0}, Ljava/net/NetworkInterface;->getByName(Ljava/lang/String;)Ljava/net/NetworkInterface;

    move-result-object v1

    .line 161
    .local v1, "nif":Ljava/net/NetworkInterface;
    if-nez v1, :cond_0

    return-object v0

    .line 163
    :cond_0
    invoke-virtual {v1}, Ljava/net/NetworkInterface;->getHardwareAddress()[B

    move-result-object v2

    .line 164
    .local v2, "mac":[B
    array-length v3, v2

    new-array v3, v3, [Ljava/lang/String;

    .line 165
    .local v3, "sa":[Ljava/lang/String;
    const/4 v4, 0x0

    .local v4, "i":I
    :goto_0
    array-length v5, v2

    if-ge v4, v5, :cond_1

    .line 166
    const-string v5, "%02x"

    const/4 v6, 0x1

    new-array v6, v6, [Ljava/lang/Object;

    aget-byte v7, v2, v4

    invoke-static {v7}, Ljava/lang/Byte;->valueOf(B)Ljava/lang/Byte;

    move-result-object v7

    const/4 v8, 0x0

    aput-object v7, v6, v8

    invoke-static {v5, v6}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v5

    aput-object v5, v3, v4

    .line 165
    add-int/lit8 v4, v4, 0x1

    goto :goto_0

    .line 168
    .end local v4    # "i":I
    :cond_1
    const-string v4, ":"

    invoke-static {v4, v3}, Ljava/lang/String;->join(Ljava/lang/CharSequence;[Ljava/lang/CharSequence;)Ljava/lang/String;

    move-result-object v4
    :try_end_0
    .catch Ljava/net/SocketException; {:try_start_0 .. :try_end_0} :catch_0

    move-object v0, v4

    .line 171
    .end local v1    # "nif":Ljava/net/NetworkInterface;
    .end local v2    # "mac":[B
    .end local v3    # "sa":[Ljava/lang/String;
    goto :goto_1

    .line 169
    :catch_0
    move-exception v1

    .line 170
    .local v1, "e":Ljava/net/SocketException;
    invoke-virtual {v1}, Ljava/net/SocketException;->printStackTrace()V

    .line 172
    .end local v1    # "e":Ljava/net/SocketException;
    :goto_1
    return-object v0
.end method

.method private getIpFromArpTable(Ljava/lang/String;)Ljava/lang/String;
    .locals 8
    .param p1, "macAddr"    # Ljava/lang/String;

    .line 194
    const-string v0, ""

    .line 195
    .local v0, "ipAddr":Ljava/lang/String;
    :try_start_0
    new-instance v1, Ljava/io/BufferedReader;

    new-instance v2, Ljava/io/FileReader;

    const-string v3, "/proc/net/arp"

    invoke-direct {v2, v3}, Ljava/io/FileReader;-><init>(Ljava/lang/String;)V

    invoke-direct {v1, v2}, Ljava/io/BufferedReader;-><init>(Ljava/io/Reader;)V
    :try_end_0
    .catch Ljava/io/FileNotFoundException; {:try_start_0 .. :try_end_0} :catch_1
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    .line 196
    .local v1, "reader":Ljava/io/BufferedReader;
    :try_start_1
    invoke-virtual {v1}, Ljava/io/BufferedReader;->readLine()Ljava/lang/String;

    .line 198
    :goto_0
    invoke-virtual {v1}, Ljava/io/BufferedReader;->readLine()Ljava/lang/String;

    move-result-object v2

    move-object v3, v2

    .local v3, "line":Ljava/lang/String;
    if-eqz v2, :cond_2

    .line 199
    const-string v2, "[ ]+"

    invoke-virtual {v3, v2}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v2

    .line 200
    .local v2, "tokens":[Ljava/lang/String;
    array-length v4, v2

    const/4 v5, 0x6

    if-ge v4, v5, :cond_0

    .line 201
    goto :goto_0

    .line 203
    :cond_0
    const/4 v4, 0x3

    aget-object v5, v2, v4

    .line 204
    .local v5, "addr":Ljava/lang/String;
    invoke-virtual {v5}, Ljava/lang/String;->length()I

    move-result v6

    sub-int/2addr v6, v4

    const/4 v4, 0x0

    invoke-virtual {v5, v4, v6}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v6

    .line 205
    .local v6, "subAddr":Ljava/lang/String;
    invoke-virtual {p1, v6}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v7

    if-eqz v7, :cond_1

    .line 206
    aget-object v4, v2, v4
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    move-object v0, v4

    .line 208
    .end local v2    # "tokens":[Ljava/lang/String;
    .end local v5    # "addr":Ljava/lang/String;
    .end local v6    # "subAddr":Ljava/lang/String;
    :cond_1
    goto :goto_0

    .line 209
    .end local v3    # "line":Ljava/lang/String;
    :cond_2
    :try_start_2
    invoke-virtual {v1}, Ljava/io/BufferedReader;->close()V
    :try_end_2
    .catch Ljava/io/FileNotFoundException; {:try_start_2 .. :try_end_2} :catch_1
    .catch Ljava/io/IOException; {:try_start_2 .. :try_end_2} :catch_0

    .line 213
    .end local v1    # "reader":Ljava/io/BufferedReader;
    nop

    .line 214
    return-object v0

    .line 195
    .restart local v1    # "reader":Ljava/io/BufferedReader;
    :catchall_0
    move-exception v2

    :try_start_3
    invoke-virtual {v1}, Ljava/io/BufferedReader;->close()V
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    goto :goto_1

    :catchall_1
    move-exception v3

    :try_start_4
    invoke-virtual {v2, v3}, Ljava/lang/Throwable;->addSuppressed(Ljava/lang/Throwable;)V

    .end local v0    # "ipAddr":Ljava/lang/String;
    .end local p0    # "this":Lcom/xiaomi/interconnection/InterconnectionService;
    .end local p1    # "macAddr":Ljava/lang/String;
    :goto_1
    throw v2
    :try_end_4
    .catch Ljava/io/FileNotFoundException; {:try_start_4 .. :try_end_4} :catch_1
    .catch Ljava/io/IOException; {:try_start_4 .. :try_end_4} :catch_0

    .line 211
    .end local v1    # "reader":Ljava/io/BufferedReader;
    .restart local v0    # "ipAddr":Ljava/lang/String;
    .restart local p0    # "this":Lcom/xiaomi/interconnection/InterconnectionService;
    .restart local p1    # "macAddr":Ljava/lang/String;
    :catch_0
    move-exception v1

    .line 212
    .local v1, "e":Ljava/io/IOException;
    return-object v0

    .line 209
    .end local v1    # "e":Ljava/io/IOException;
    :catch_1
    move-exception v1

    .line 210
    .local v1, "e":Ljava/io/FileNotFoundException;
    return-object v0
.end method

.method private getPeerIpFromArpTableUntilTimeout()V
    .locals 6

    .line 218
    const/4 v0, 0x2

    invoke-static {v0}, Ljava/util/concurrent/Executors;->newScheduledThreadPool(I)Ljava/util/concurrent/ScheduledExecutorService;

    move-result-object v0

    .line 219
    .local v0, "executor":Ljava/util/concurrent/ScheduledExecutorService;
    new-instance v1, Lcom/xiaomi/interconnection/InterconnectionService$$ExternalSyntheticLambda1;

    invoke-direct {v1, p0}, Lcom/xiaomi/interconnection/InterconnectionService$$ExternalSyntheticLambda1;-><init>(Lcom/xiaomi/interconnection/InterconnectionService;)V

    invoke-interface {v0, v1}, Ljava/util/concurrent/ScheduledExecutorService;->submit(Ljava/lang/Runnable;)Ljava/util/concurrent/Future;

    move-result-object v1

    .line 227
    .local v1, "future":Ljava/util/concurrent/Future;
    new-instance v2, Lcom/xiaomi/interconnection/InterconnectionService$$ExternalSyntheticLambda2;

    invoke-direct {v2, v1}, Lcom/xiaomi/interconnection/InterconnectionService$$ExternalSyntheticLambda2;-><init>(Ljava/util/concurrent/Future;)V

    .line 231
    .local v2, "cancelTask":Ljava/lang/Runnable;
    const-wide/16 v3, 0x7d0

    sget-object v5, Ljava/util/concurrent/TimeUnit;->MILLISECONDS:Ljava/util/concurrent/TimeUnit;

    invoke-interface {v0, v2, v3, v4, v5}, Ljava/util/concurrent/ScheduledExecutorService;->schedule(Ljava/lang/Runnable;JLjava/util/concurrent/TimeUnit;)Ljava/util/concurrent/ScheduledFuture;

    .line 232
    invoke-interface {v0}, Ljava/util/concurrent/ScheduledExecutorService;->shutdown()V

    .line 233
    return-void
.end method

.method static synthetic lambda$getIfaceIpAddr$0(Ljava/net/InetAddress;)Z
    .locals 1
    .param p0, "ia"    # Ljava/net/InetAddress;

    .line 181
    instance-of v0, p0, Ljava/net/Inet4Address;

    return v0
.end method

.method private synthetic lambda$getPeerIpFromArpTableUntilTimeout$1()V
    .locals 3

    .line 220
    const/4 v0, 0x0

    .line 221
    .local v0, "interrupted":Z
    :goto_0
    invoke-static {}, Ljava/lang/Thread;->currentThread()Ljava/lang/Thread;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Thread;->isInterrupted()Z

    move-result v1

    move v0, v1

    if-nez v1, :cond_0

    iget-object v1, p0, Lcom/xiaomi/interconnection/InterconnectionService;->mPeerP2pMacAddr:Ljava/lang/String;

    .line 222
    invoke-direct {p0, v1}, Lcom/xiaomi/interconnection/InterconnectionService;->getIpFromArpTable(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    iput-object v1, p0, Lcom/xiaomi/interconnection/InterconnectionService;->mPeerP2pIpAddr:Ljava/lang/String;

    const-string v2, ""

    invoke-virtual {v2, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    goto :goto_0

    .line 223
    :cond_0
    if-nez v0, :cond_1

    .line 224
    invoke-direct {p0}, Lcom/xiaomi/interconnection/InterconnectionService;->notifyWifiP2pInfoChanged()V

    .line 226
    :cond_1
    return-void
.end method

.method static synthetic lambda$getPeerIpFromArpTableUntilTimeout$2(Ljava/util/concurrent/Future;)V
    .locals 2
    .param p0, "future"    # Ljava/util/concurrent/Future;

    .line 228
    const/4 v0, 0x1

    invoke-interface {p0, v0}, Ljava/util/concurrent/Future;->cancel(Z)Z

    .line 229
    const-string v0, "InterconnectionService"

    const-string/jumbo v1, "task canceled due to timeout"

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 230
    return-void
.end method

.method private notifySoftApInfoChanged()V
    .locals 5

    .line 236
    iget-object v0, p0, Lcom/xiaomi/interconnection/InterconnectionService;->mSoftApCallbackList:Landroid/os/RemoteCallbackList;

    invoke-virtual {v0}, Landroid/os/RemoteCallbackList;->beginBroadcast()I

    move-result v0

    .line 237
    .local v0, "itemCount":I
    const/4 v1, 0x0

    .local v1, "i":I
    :goto_0
    if-ge v1, v0, :cond_0

    .line 239
    :try_start_0
    iget-object v2, p0, Lcom/xiaomi/interconnection/InterconnectionService;->mSoftApCallbackList:Landroid/os/RemoteCallbackList;

    invoke-virtual {v2, v1}, Landroid/os/RemoteCallbackList;->getBroadcastItem(I)Landroid/os/IInterface;

    move-result-object v2

    check-cast v2, Lcom/xiaomi/interconnection/ISoftApCallback;

    iget-object v3, p0, Lcom/xiaomi/interconnection/InterconnectionService;->mSoftApIfaceName:Ljava/lang/String;

    invoke-interface {v2, v3}, Lcom/xiaomi/interconnection/ISoftApCallback;->onIfaceInfoChanged(Ljava/lang/String;)V
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    .line 242
    goto :goto_1

    .line 240
    :catch_0
    move-exception v2

    .line 241
    .local v2, "e":Landroid/os/RemoteException;
    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "onSoftApInfoChanged: remote exception -- "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    const-string v4, "InterconnectionService"

    invoke-static {v4, v3}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 237
    .end local v2    # "e":Landroid/os/RemoteException;
    :goto_1
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 244
    .end local v1    # "i":I
    :cond_0
    iget-object v1, p0, Lcom/xiaomi/interconnection/InterconnectionService;->mSoftApCallbackList:Landroid/os/RemoteCallbackList;

    invoke-virtual {v1}, Landroid/os/RemoteCallbackList;->finishBroadcast()V

    .line 245
    return-void
.end method

.method private notifyWifiP2pInfoChanged()V
    .locals 9

    .line 248
    iget-object v0, p0, Lcom/xiaomi/interconnection/InterconnectionService;->mWifiP2pCallbackList:Landroid/os/RemoteCallbackList;

    invoke-virtual {v0}, Landroid/os/RemoteCallbackList;->beginBroadcast()I

    move-result v0

    .line 249
    .local v0, "itemCount":I
    const/4 v1, 0x0

    .local v1, "i":I
    :goto_0
    if-ge v1, v0, :cond_0

    .line 251
    :try_start_0
    new-instance v8, Lcom/xiaomi/interconnection/P2pDevicesInfo;

    iget-boolean v3, p0, Lcom/xiaomi/interconnection/InterconnectionService;->mIsGo:Z

    iget-object v4, p0, Lcom/xiaomi/interconnection/InterconnectionService;->mDeviceP2pMacAddr:Ljava/lang/String;

    iget-object v5, p0, Lcom/xiaomi/interconnection/InterconnectionService;->mPeerP2pMacAddr:Ljava/lang/String;

    iget-object v6, p0, Lcom/xiaomi/interconnection/InterconnectionService;->mDeviceP2pIpAddr:Ljava/lang/String;

    iget-object v7, p0, Lcom/xiaomi/interconnection/InterconnectionService;->mPeerP2pIpAddr:Ljava/lang/String;

    move-object v2, v8

    invoke-direct/range {v2 .. v7}, Lcom/xiaomi/interconnection/P2pDevicesInfo;-><init>(ZLjava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    move-object v2, v8

    .line 253
    .local v2, "info":Lcom/xiaomi/interconnection/P2pDevicesInfo;
    iget-object v3, p0, Lcom/xiaomi/interconnection/InterconnectionService;->mWifiP2pCallbackList:Landroid/os/RemoteCallbackList;

    invoke-virtual {v3, v1}, Landroid/os/RemoteCallbackList;->getBroadcastItem(I)Landroid/os/IInterface;

    move-result-object v3

    check-cast v3, Lcom/xiaomi/interconnection/IWifiP2pCallback;

    invoke-interface {v3, v2}, Lcom/xiaomi/interconnection/IWifiP2pCallback;->onDevicesInfoChanged(Lcom/xiaomi/interconnection/P2pDevicesInfo;)V
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    .line 256
    .end local v2    # "info":Lcom/xiaomi/interconnection/P2pDevicesInfo;
    goto :goto_1

    .line 254
    :catch_0
    move-exception v2

    .line 255
    .local v2, "e":Landroid/os/RemoteException;
    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "onWifiP2pInfoChanged: remote exception -- "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    const-string v4, "InterconnectionService"

    invoke-static {v4, v3}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 249
    .end local v2    # "e":Landroid/os/RemoteException;
    :goto_1
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 258
    .end local v1    # "i":I
    :cond_0
    iget-object v1, p0, Lcom/xiaomi/interconnection/InterconnectionService;->mWifiP2pCallbackList:Landroid/os/RemoteCallbackList;

    invoke-virtual {v1}, Landroid/os/RemoteCallbackList;->finishBroadcast()V

    .line 259
    return-void
.end method


# virtual methods
.method public getWifiChipModel()Ljava/lang/String;
    .locals 4

    .line 263
    const-string v0, ""

    .line 264
    .local v0, "wifiChipModel":Ljava/lang/String;
    const-string/jumbo v1, "vendor"

    invoke-static {v1}, Lmiui/util/FeatureParser;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    const-string v2, "mediatek"

    invoke-virtual {v2, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    .line 265
    .local v1, "isMtk":Z
    const-string/jumbo v2, "unknown"

    if-eqz v1, :cond_0

    .line 266
    const-string/jumbo v3, "vendor.connsys.wifi.adie.chipid"

    invoke-static {v3, v2}, Landroid/os/SystemProperties;->get(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    .line 268
    :cond_0
    const-string v3, "ro.hardware.wlan.chip"

    invoke-static {v3, v2}, Landroid/os/SystemProperties;->get(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 271
    :goto_0
    return-object v0
.end method

.method public notifyConcurrentNetworkState(Z)V
    .locals 2
    .param p1, "mcc"    # Z

    .line 345
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "mcc: "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    const-string v1, "InterconnectionService"

    invoke-static {v1, v0}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 347
    return-void
.end method

.method public registerSoftApCallback(Lcom/xiaomi/interconnection/ISoftApCallback;)V
    .locals 1
    .param p1, "cb"    # Lcom/xiaomi/interconnection/ISoftApCallback;

    .line 325
    iget-object v0, p0, Lcom/xiaomi/interconnection/InterconnectionService;->mSoftApCallbackList:Landroid/os/RemoteCallbackList;

    invoke-virtual {v0, p1}, Landroid/os/RemoteCallbackList;->register(Landroid/os/IInterface;)Z

    .line 326
    return-void
.end method

.method public registerWifiP2pCallback(Lcom/xiaomi/interconnection/IWifiP2pCallback;)V
    .locals 1
    .param p1, "cb"    # Lcom/xiaomi/interconnection/IWifiP2pCallback;

    .line 335
    iget-object v0, p0, Lcom/xiaomi/interconnection/InterconnectionService;->mWifiP2pCallbackList:Landroid/os/RemoteCallbackList;

    invoke-virtual {v0, p1}, Landroid/os/RemoteCallbackList;->register(Landroid/os/IInterface;)Z

    .line 336
    return-void
.end method

.method public supportDbs()I
    .locals 4

    .line 307
    const/4 v0, 0x0

    .line 308
    .local v0, "dbs":I
    const-string v1, "ro.hardware.wlan.dbs"

    const-string/jumbo v2, "unknown"

    invoke-static {v1, v2}, Landroid/os/SystemProperties;->get(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 309
    .local v1, "dbsProp":Ljava/lang/String;
    invoke-virtual {v2, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_3

    .line 310
    const-string/jumbo v3, "vendor.connsys.adie.chipid"

    invoke-static {v3, v2}, Landroid/os/SystemProperties;->get(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    .line 311
    .local v3, "chipProp":Ljava/lang/String;
    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_2

    .line 312
    const-string v2, "0x6635"

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_1

    const-string v2, "0x6637"

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_0

    goto :goto_0

    :cond_0
    const/4 v2, 0x2

    goto :goto_1

    :cond_1
    :goto_0
    const/4 v2, 0x1

    :goto_1
    move v0, v2

    .line 314
    .end local v3    # "chipProp":Ljava/lang/String;
    :cond_2
    goto :goto_2

    :cond_3
    const-string v2, "1"

    invoke-virtual {v2, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_4

    .line 315
    const/4 v0, 0x1

    goto :goto_2

    .line 316
    :cond_4
    const-string v2, "2"

    invoke-virtual {v2, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_5

    .line 317
    const/4 v0, 0x2

    .line 320
    :cond_5
    :goto_2
    return v0
.end method

.method public supportHbs()Z
    .locals 8

    .line 286
    iget-object v0, p0, Lcom/xiaomi/interconnection/InterconnectionService;->mContext:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    const-string v1, "cloud_wifi_hbs_support"

    invoke-static {v0, v1}, Landroid/provider/Settings$System;->getString(Landroid/content/ContentResolver;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 288
    .local v0, "cloudvalue":Ljava/lang/String;
    const-string v1, "off"

    invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    const/4 v2, 0x0

    if-eqz v1, :cond_0

    .line 289
    return v2

    .line 292
    :cond_0
    const/4 v1, 0x0

    .line 294
    .local v1, "support":Z
    :try_start_0
    iget-object v3, p0, Lcom/xiaomi/interconnection/InterconnectionService;->mContext:Landroid/content/Context;

    invoke-virtual {v3}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    iget-object v4, p0, Lcom/xiaomi/interconnection/InterconnectionService;->mContext:Landroid/content/Context;

    .line 295
    invoke-virtual {v4}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v4

    const-string v5, "config_wifi_hbs_support"

    const-string v6, "bool"

    const-string v7, "android.miui"

    invoke-virtual {v4, v5, v6, v7}, Landroid/content/res/Resources;->getIdentifier(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)I

    move-result v4

    .line 294
    invoke-virtual {v3, v4}, Landroid/content/res/Resources;->getBoolean(I)Z

    move-result v2
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    move v1, v2

    .line 300
    nop

    .line 302
    return v1

    .line 297
    :catch_0
    move-exception v3

    .line 298
    .local v3, "exception":Ljava/lang/Exception;
    const-string v4, "InterconnectionService"

    const-string v5, "config for wifi hbs not found"

    invoke-static {v4, v5}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 299
    return v2
.end method

.method public supportP2p160Mode()Z
    .locals 2

    .line 281
    iget-object v0, p0, Lcom/xiaomi/interconnection/InterconnectionService;->mPackageManager:Landroid/content/pm/PackageManager;

    const-string/jumbo v1, "xiaomi.hardware.p2p_160m"

    invoke-virtual {v0, v1}, Landroid/content/pm/PackageManager;->hasSystemFeature(Ljava/lang/String;)Z

    move-result v0

    return v0
.end method

.method public supportP2pChannel165()Z
    .locals 2

    .line 276
    iget-object v0, p0, Lcom/xiaomi/interconnection/InterconnectionService;->mPackageManager:Landroid/content/pm/PackageManager;

    const-string/jumbo v1, "xiaomi.hardware.p2p_165chan"

    invoke-virtual {v0, v1}, Landroid/content/pm/PackageManager;->hasSystemFeature(Ljava/lang/String;)Z

    move-result v0

    return v0
.end method

.method public unregisterSoftApCallback(Lcom/xiaomi/interconnection/ISoftApCallback;)V
    .locals 1
    .param p1, "cb"    # Lcom/xiaomi/interconnection/ISoftApCallback;

    .line 330
    iget-object v0, p0, Lcom/xiaomi/interconnection/InterconnectionService;->mSoftApCallbackList:Landroid/os/RemoteCallbackList;

    invoke-virtual {v0, p1}, Landroid/os/RemoteCallbackList;->unregister(Landroid/os/IInterface;)Z

    .line 331
    return-void
.end method

.method public unregisterWifiP2pCallback(Lcom/xiaomi/interconnection/IWifiP2pCallback;)V
    .locals 1
    .param p1, "cb"    # Lcom/xiaomi/interconnection/IWifiP2pCallback;

    .line 340
    iget-object v0, p0, Lcom/xiaomi/interconnection/InterconnectionService;->mWifiP2pCallbackList:Landroid/os/RemoteCallbackList;

    invoke-virtual {v0, p1}, Landroid/os/RemoteCallbackList;->unregister(Landroid/os/IInterface;)Z

    .line 341
    return-void
.end method
