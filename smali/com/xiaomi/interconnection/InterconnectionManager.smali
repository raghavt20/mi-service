.class public Lcom/xiaomi/interconnection/InterconnectionManager;
.super Ljava/lang/Object;
.source "InterconnectionManager.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/xiaomi/interconnection/InterconnectionManager$SingletonHolder;,
        Lcom/xiaomi/interconnection/InterconnectionManager$SoftApCallbackProxy;,
        Lcom/xiaomi/interconnection/InterconnectionManager$SoftApCallback;,
        Lcom/xiaomi/interconnection/InterconnectionManager$WifiP2pCallbackProxy;,
        Lcom/xiaomi/interconnection/InterconnectionManager$WifiP2pCallback;
    }
.end annotation


# static fields
.field public static final DBS_1:I = 0x1

.field public static final DBS_2:I = 0x2

.field public static final DBS_UNSUPPORTED:I = 0x0

.field private static final TAG:Ljava/lang/String; = "InterconnectionManager"

.field private static final sSoftApCallbackMap:Landroid/util/SparseArray;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/util/SparseArray<",
            "Lcom/xiaomi/interconnection/ISoftApCallback;",
            ">;"
        }
    .end annotation
.end field

.field private static final sWifiP2pCallbackMap:Landroid/util/SparseArray;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/util/SparseArray<",
            "Lcom/xiaomi/interconnection/IWifiP2pCallback;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field private mHandler:Landroid/os/Handler;

.field private mInterconnService:Lcom/xiaomi/interconnection/IInterconnectionManager;


# direct methods
.method static bridge synthetic -$$Nest$fgetmHandler(Lcom/xiaomi/interconnection/InterconnectionManager;)Landroid/os/Handler;
    .locals 0

    iget-object p0, p0, Lcom/xiaomi/interconnection/InterconnectionManager;->mHandler:Landroid/os/Handler;

    return-object p0
.end method

.method static constructor <clinit>()V
    .locals 1

    .line 22
    new-instance v0, Landroid/util/SparseArray;

    invoke-direct {v0}, Landroid/util/SparseArray;-><init>()V

    sput-object v0, Lcom/xiaomi/interconnection/InterconnectionManager;->sSoftApCallbackMap:Landroid/util/SparseArray;

    .line 23
    new-instance v0, Landroid/util/SparseArray;

    invoke-direct {v0}, Landroid/util/SparseArray;-><init>()V

    sput-object v0, Lcom/xiaomi/interconnection/InterconnectionManager;->sWifiP2pCallbackMap:Landroid/util/SparseArray;

    return-void
.end method

.method private constructor <init>()V
    .locals 3

    .line 29
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 30
    const-string/jumbo v0, "xiaomi.InterconnectionService"

    invoke-static {v0}, Landroid/os/ServiceManager;->getService(Ljava/lang/String;)Landroid/os/IBinder;

    move-result-object v0

    .line 31
    .local v0, "binder":Landroid/os/IBinder;
    invoke-static {v0}, Lcom/xiaomi/interconnection/IInterconnectionManager$Stub;->asInterface(Landroid/os/IBinder;)Lcom/xiaomi/interconnection/IInterconnectionManager;

    move-result-object v1

    iput-object v1, p0, Lcom/xiaomi/interconnection/InterconnectionManager;->mInterconnService:Lcom/xiaomi/interconnection/IInterconnectionManager;

    .line 32
    if-nez v1, :cond_0

    .line 33
    const-string v1, "InterconnectionManager"

    const-string v2, "InterconnectionService not found"

    invoke-static {v1, v2}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 36
    :cond_0
    new-instance v1, Landroid/os/Handler;

    invoke-direct {v1}, Landroid/os/Handler;-><init>()V

    iput-object v1, p0, Lcom/xiaomi/interconnection/InterconnectionManager;->mHandler:Landroid/os/Handler;

    .line 37
    return-void
.end method

.method synthetic constructor <init>(Lcom/xiaomi/interconnection/InterconnectionManager-IA;)V
    .locals 0

    invoke-direct {p0}, Lcom/xiaomi/interconnection/InterconnectionManager;-><init>()V

    return-void
.end method

.method public static getInstance()Lcom/xiaomi/interconnection/InterconnectionManager;
    .locals 1

    .line 44
    invoke-static {}, Lcom/xiaomi/interconnection/InterconnectionManager$SingletonHolder;->-$$Nest$sfgetINSTANCE()Lcom/xiaomi/interconnection/InterconnectionManager;

    move-result-object v0

    return-object v0
.end method


# virtual methods
.method public getWifiChipModel()Ljava/lang/String;
    .locals 3

    .line 168
    iget-object v0, p0, Lcom/xiaomi/interconnection/InterconnectionManager;->mInterconnService:Lcom/xiaomi/interconnection/IInterconnectionManager;

    if-eqz v0, :cond_0

    .line 170
    :try_start_0
    invoke-interface {v0}, Lcom/xiaomi/interconnection/IInterconnectionManager;->getWifiChipModel()Ljava/lang/String;

    move-result-object v0
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    return-object v0

    .line 171
    :catch_0
    move-exception v0

    .line 172
    .local v0, "e":Landroid/os/RemoteException;
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "exception: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    const-string v2, "InterconnectionManager"

    invoke-static {v2, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 175
    .end local v0    # "e":Landroid/os/RemoteException;
    :cond_0
    const-string/jumbo v0, "unknown"

    return-object v0
.end method

.method public notifyConcurrentNetworkState(Z)V
    .locals 3
    .param p1, "mcc"    # Z

    .line 223
    iget-object v0, p0, Lcom/xiaomi/interconnection/InterconnectionManager;->mInterconnService:Lcom/xiaomi/interconnection/IInterconnectionManager;

    if-eqz v0, :cond_0

    .line 225
    :try_start_0
    invoke-interface {v0, p1}, Lcom/xiaomi/interconnection/IInterconnectionManager;->notifyConcurrentNetworkState(Z)V
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    .line 228
    goto :goto_0

    .line 226
    :catch_0
    move-exception v0

    .line 227
    .local v0, "e":Landroid/os/RemoteException;
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "exception: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    const-string v2, "InterconnectionManager"

    invoke-static {v2, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 230
    .end local v0    # "e":Landroid/os/RemoteException;
    :cond_0
    :goto_0
    return-void
.end method

.method public registerSoftApCallback(Lcom/xiaomi/interconnection/InterconnectionManager$SoftApCallback;)V
    .locals 4
    .param p1, "softApCallback"    # Lcom/xiaomi/interconnection/InterconnectionManager$SoftApCallback;

    .line 67
    if-eqz p1, :cond_1

    .line 71
    iget-object v0, p0, Lcom/xiaomi/interconnection/InterconnectionManager;->mInterconnService:Lcom/xiaomi/interconnection/IInterconnectionManager;

    if-eqz v0, :cond_0

    .line 73
    :try_start_0
    sget-object v0, Lcom/xiaomi/interconnection/InterconnectionManager;->sSoftApCallbackMap:Landroid/util/SparseArray;

    monitor-enter v0
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    .line 74
    :try_start_1
    new-instance v1, Lcom/xiaomi/interconnection/InterconnectionManager$SoftApCallbackProxy;

    const/4 v2, 0x0

    invoke-direct {v1, p0, p1, v2}, Lcom/xiaomi/interconnection/InterconnectionManager$SoftApCallbackProxy;-><init>(Lcom/xiaomi/interconnection/InterconnectionManager;Lcom/xiaomi/interconnection/InterconnectionManager$SoftApCallback;Lcom/xiaomi/interconnection/InterconnectionManager$SoftApCallbackProxy-IA;)V

    .line 75
    .local v1, "binderCallback":Lcom/xiaomi/interconnection/ISoftApCallback$Stub;
    invoke-static {p1}, Ljava/lang/System;->identityHashCode(Ljava/lang/Object;)I

    move-result v2

    invoke-virtual {v0, v2, v1}, Landroid/util/SparseArray;->put(ILjava/lang/Object;)V

    .line 76
    iget-object v2, p0, Lcom/xiaomi/interconnection/InterconnectionManager;->mInterconnService:Lcom/xiaomi/interconnection/IInterconnectionManager;

    invoke-interface {v2, v1}, Lcom/xiaomi/interconnection/IInterconnectionManager;->registerSoftApCallback(Lcom/xiaomi/interconnection/ISoftApCallback;)V

    .line 77
    .end local v1    # "binderCallback":Lcom/xiaomi/interconnection/ISoftApCallback$Stub;
    monitor-exit v0

    .line 80
    goto :goto_0

    .line 77
    :catchall_0
    move-exception v1

    monitor-exit v0
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .end local p0    # "this":Lcom/xiaomi/interconnection/InterconnectionManager;
    .end local p1    # "softApCallback":Lcom/xiaomi/interconnection/InterconnectionManager$SoftApCallback;
    :try_start_2
    throw v1
    :try_end_2
    .catch Landroid/os/RemoteException; {:try_start_2 .. :try_end_2} :catch_0

    .line 78
    .restart local p0    # "this":Lcom/xiaomi/interconnection/InterconnectionManager;
    .restart local p1    # "softApCallback":Lcom/xiaomi/interconnection/InterconnectionManager$SoftApCallback;
    :catch_0
    move-exception v0

    .line 79
    .local v0, "e":Landroid/os/RemoteException;
    const-string v1, "InterconnectionManager"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "exception: "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 82
    .end local v0    # "e":Landroid/os/RemoteException;
    :cond_0
    :goto_0
    return-void

    .line 68
    :cond_1
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string/jumbo v1, "softApCallback cannot be null"

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0
.end method

.method public registerWifiP2pCallback(Lcom/xiaomi/interconnection/InterconnectionManager$WifiP2pCallback;)V
    .locals 4
    .param p1, "wifiP2pCallback"    # Lcom/xiaomi/interconnection/InterconnectionManager$WifiP2pCallback;

    .line 127
    if-eqz p1, :cond_1

    .line 131
    iget-object v0, p0, Lcom/xiaomi/interconnection/InterconnectionManager;->mInterconnService:Lcom/xiaomi/interconnection/IInterconnectionManager;

    if-eqz v0, :cond_0

    .line 133
    :try_start_0
    sget-object v0, Lcom/xiaomi/interconnection/InterconnectionManager;->sWifiP2pCallbackMap:Landroid/util/SparseArray;

    monitor-enter v0
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    .line 134
    :try_start_1
    new-instance v1, Lcom/xiaomi/interconnection/InterconnectionManager$WifiP2pCallbackProxy;

    const/4 v2, 0x0

    invoke-direct {v1, p0, p1, v2}, Lcom/xiaomi/interconnection/InterconnectionManager$WifiP2pCallbackProxy;-><init>(Lcom/xiaomi/interconnection/InterconnectionManager;Lcom/xiaomi/interconnection/InterconnectionManager$WifiP2pCallback;Lcom/xiaomi/interconnection/InterconnectionManager$WifiP2pCallbackProxy-IA;)V

    .line 135
    .local v1, "binderCallback":Lcom/xiaomi/interconnection/IWifiP2pCallback$Stub;
    invoke-static {p1}, Ljava/lang/System;->identityHashCode(Ljava/lang/Object;)I

    move-result v2

    invoke-virtual {v0, v2, v1}, Landroid/util/SparseArray;->put(ILjava/lang/Object;)V

    .line 136
    iget-object v2, p0, Lcom/xiaomi/interconnection/InterconnectionManager;->mInterconnService:Lcom/xiaomi/interconnection/IInterconnectionManager;

    invoke-interface {v2, v1}, Lcom/xiaomi/interconnection/IInterconnectionManager;->registerWifiP2pCallback(Lcom/xiaomi/interconnection/IWifiP2pCallback;)V

    .line 137
    .end local v1    # "binderCallback":Lcom/xiaomi/interconnection/IWifiP2pCallback$Stub;
    monitor-exit v0

    .line 140
    goto :goto_0

    .line 137
    :catchall_0
    move-exception v1

    monitor-exit v0
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .end local p0    # "this":Lcom/xiaomi/interconnection/InterconnectionManager;
    .end local p1    # "wifiP2pCallback":Lcom/xiaomi/interconnection/InterconnectionManager$WifiP2pCallback;
    :try_start_2
    throw v1
    :try_end_2
    .catch Landroid/os/RemoteException; {:try_start_2 .. :try_end_2} :catch_0

    .line 138
    .restart local p0    # "this":Lcom/xiaomi/interconnection/InterconnectionManager;
    .restart local p1    # "wifiP2pCallback":Lcom/xiaomi/interconnection/InterconnectionManager$WifiP2pCallback;
    :catch_0
    move-exception v0

    .line 139
    .local v0, "e":Landroid/os/RemoteException;
    const-string v1, "InterconnectionManager"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "exception: "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 142
    .end local v0    # "e":Landroid/os/RemoteException;
    :cond_0
    :goto_0
    return-void

    .line 128
    :cond_1
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string/jumbo v1, "wifiP2pCallback cannot be null"

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0
.end method

.method public supportDbs()I
    .locals 3

    .line 212
    iget-object v0, p0, Lcom/xiaomi/interconnection/InterconnectionManager;->mInterconnService:Lcom/xiaomi/interconnection/IInterconnectionManager;

    if-eqz v0, :cond_0

    .line 214
    :try_start_0
    invoke-interface {v0}, Lcom/xiaomi/interconnection/IInterconnectionManager;->supportDbs()I

    move-result v0
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    return v0

    .line 215
    :catch_0
    move-exception v0

    .line 216
    .local v0, "e":Landroid/os/RemoteException;
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "exception: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    const-string v2, "InterconnectionManager"

    invoke-static {v2, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 219
    .end local v0    # "e":Landroid/os/RemoteException;
    :cond_0
    const/4 v0, 0x0

    return v0
.end method

.method public supportHbs()Z
    .locals 3

    .line 201
    iget-object v0, p0, Lcom/xiaomi/interconnection/InterconnectionManager;->mInterconnService:Lcom/xiaomi/interconnection/IInterconnectionManager;

    if-eqz v0, :cond_0

    .line 203
    :try_start_0
    invoke-interface {v0}, Lcom/xiaomi/interconnection/IInterconnectionManager;->supportHbs()Z

    move-result v0
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    return v0

    .line 204
    :catch_0
    move-exception v0

    .line 205
    .local v0, "e":Landroid/os/RemoteException;
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "exception: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    const-string v2, "InterconnectionManager"

    invoke-static {v2, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 208
    .end local v0    # "e":Landroid/os/RemoteException;
    :cond_0
    const/4 v0, 0x0

    return v0
.end method

.method public supportP2p160Mode()Z
    .locals 3

    .line 190
    iget-object v0, p0, Lcom/xiaomi/interconnection/InterconnectionManager;->mInterconnService:Lcom/xiaomi/interconnection/IInterconnectionManager;

    if-eqz v0, :cond_0

    .line 192
    :try_start_0
    invoke-interface {v0}, Lcom/xiaomi/interconnection/IInterconnectionManager;->supportP2p160Mode()Z

    move-result v0
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    return v0

    .line 193
    :catch_0
    move-exception v0

    .line 194
    .local v0, "e":Landroid/os/RemoteException;
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "exception: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    const-string v2, "InterconnectionManager"

    invoke-static {v2, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 197
    .end local v0    # "e":Landroid/os/RemoteException;
    :cond_0
    const/4 v0, 0x0

    return v0
.end method

.method public supportP2pChannel165()Z
    .locals 3

    .line 179
    iget-object v0, p0, Lcom/xiaomi/interconnection/InterconnectionManager;->mInterconnService:Lcom/xiaomi/interconnection/IInterconnectionManager;

    if-eqz v0, :cond_0

    .line 181
    :try_start_0
    invoke-interface {v0}, Lcom/xiaomi/interconnection/IInterconnectionManager;->supportP2pChannel165()Z

    move-result v0
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    return v0

    .line 182
    :catch_0
    move-exception v0

    .line 183
    .local v0, "e":Landroid/os/RemoteException;
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "exception: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    const-string v2, "InterconnectionManager"

    invoke-static {v2, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 186
    .end local v0    # "e":Landroid/os/RemoteException;
    :cond_0
    const/4 v0, 0x0

    return v0
.end method

.method public unregisterSoftApCallback(Lcom/xiaomi/interconnection/InterconnectionManager$SoftApCallback;)V
    .locals 5
    .param p1, "softApCallback"    # Lcom/xiaomi/interconnection/InterconnectionManager$SoftApCallback;

    .line 85
    if-eqz p1, :cond_2

    .line 89
    iget-object v0, p0, Lcom/xiaomi/interconnection/InterconnectionManager;->mInterconnService:Lcom/xiaomi/interconnection/IInterconnectionManager;

    if-eqz v0, :cond_1

    .line 91
    :try_start_0
    sget-object v0, Lcom/xiaomi/interconnection/InterconnectionManager;->sSoftApCallbackMap:Landroid/util/SparseArray;

    monitor-enter v0
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    .line 92
    :try_start_1
    invoke-static {p1}, Ljava/lang/System;->identityHashCode(Ljava/lang/Object;)I

    move-result v1

    .line 93
    .local v1, "callbackIdentifier":I
    invoke-virtual {v0, v1}, Landroid/util/SparseArray;->contains(I)Z

    move-result v2

    if-nez v2, :cond_0

    .line 94
    const-string v2, "InterconnectionManager"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v4, "unknown softApCallback "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    .line 95
    monitor-exit v0

    return-void

    .line 97
    :cond_0
    iget-object v2, p0, Lcom/xiaomi/interconnection/InterconnectionManager;->mInterconnService:Lcom/xiaomi/interconnection/IInterconnectionManager;

    .line 98
    invoke-virtual {v0, v1}, Landroid/util/SparseArray;->get(I)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/xiaomi/interconnection/ISoftApCallback;

    .line 97
    invoke-interface {v2, v3}, Lcom/xiaomi/interconnection/IInterconnectionManager;->unregisterSoftApCallback(Lcom/xiaomi/interconnection/ISoftApCallback;)V

    .line 99
    invoke-virtual {v0, v1}, Landroid/util/SparseArray;->remove(I)V

    .line 100
    .end local v1    # "callbackIdentifier":I
    monitor-exit v0

    .line 103
    goto :goto_0

    .line 100
    :catchall_0
    move-exception v1

    monitor-exit v0
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .end local p0    # "this":Lcom/xiaomi/interconnection/InterconnectionManager;
    .end local p1    # "softApCallback":Lcom/xiaomi/interconnection/InterconnectionManager$SoftApCallback;
    :try_start_2
    throw v1
    :try_end_2
    .catch Landroid/os/RemoteException; {:try_start_2 .. :try_end_2} :catch_0

    .line 101
    .restart local p0    # "this":Lcom/xiaomi/interconnection/InterconnectionManager;
    .restart local p1    # "softApCallback":Lcom/xiaomi/interconnection/InterconnectionManager$SoftApCallback;
    :catch_0
    move-exception v0

    .line 102
    .local v0, "e":Landroid/os/RemoteException;
    const-string v1, "InterconnectionManager"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "exception: "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 105
    .end local v0    # "e":Landroid/os/RemoteException;
    :cond_1
    :goto_0
    return-void

    .line 86
    :cond_2
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string/jumbo v1, "softApCallback cannot be null"

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0
.end method

.method public unregisterWifiP2pCallback(Lcom/xiaomi/interconnection/InterconnectionManager$WifiP2pCallback;)V
    .locals 5
    .param p1, "wifiP2pCallback"    # Lcom/xiaomi/interconnection/InterconnectionManager$WifiP2pCallback;

    .line 145
    if-eqz p1, :cond_2

    .line 149
    iget-object v0, p0, Lcom/xiaomi/interconnection/InterconnectionManager;->mInterconnService:Lcom/xiaomi/interconnection/IInterconnectionManager;

    if-eqz v0, :cond_1

    .line 151
    :try_start_0
    sget-object v0, Lcom/xiaomi/interconnection/InterconnectionManager;->sWifiP2pCallbackMap:Landroid/util/SparseArray;

    monitor-enter v0
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    .line 152
    :try_start_1
    invoke-static {p1}, Ljava/lang/System;->identityHashCode(Ljava/lang/Object;)I

    move-result v1

    .line 153
    .local v1, "callbackIdentifier":I
    invoke-virtual {v0, v1}, Landroid/util/SparseArray;->contains(I)Z

    move-result v2

    if-nez v2, :cond_0

    .line 154
    const-string v2, "InterconnectionManager"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v4, "unknown wifiP2pCallback"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    .line 155
    monitor-exit v0

    return-void

    .line 157
    :cond_0
    iget-object v2, p0, Lcom/xiaomi/interconnection/InterconnectionManager;->mInterconnService:Lcom/xiaomi/interconnection/IInterconnectionManager;

    .line 158
    invoke-virtual {v0, v1}, Landroid/util/SparseArray;->get(I)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/xiaomi/interconnection/IWifiP2pCallback;

    .line 157
    invoke-interface {v2, v3}, Lcom/xiaomi/interconnection/IInterconnectionManager;->unregisterWifiP2pCallback(Lcom/xiaomi/interconnection/IWifiP2pCallback;)V

    .line 159
    invoke-virtual {v0, v1}, Landroid/util/SparseArray;->remove(I)V

    .line 160
    .end local v1    # "callbackIdentifier":I
    monitor-exit v0

    .line 163
    goto :goto_0

    .line 160
    :catchall_0
    move-exception v1

    monitor-exit v0
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .end local p0    # "this":Lcom/xiaomi/interconnection/InterconnectionManager;
    .end local p1    # "wifiP2pCallback":Lcom/xiaomi/interconnection/InterconnectionManager$WifiP2pCallback;
    :try_start_2
    throw v1
    :try_end_2
    .catch Landroid/os/RemoteException; {:try_start_2 .. :try_end_2} :catch_0

    .line 161
    .restart local p0    # "this":Lcom/xiaomi/interconnection/InterconnectionManager;
    .restart local p1    # "wifiP2pCallback":Lcom/xiaomi/interconnection/InterconnectionManager$WifiP2pCallback;
    :catch_0
    move-exception v0

    .line 162
    .local v0, "e":Landroid/os/RemoteException;
    const-string v1, "InterconnectionManager"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "exception: "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 165
    .end local v0    # "e":Landroid/os/RemoteException;
    :cond_1
    :goto_0
    return-void

    .line 146
    :cond_2
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string/jumbo v1, "wifiP2pCallback cannot be null"

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0
.end method
