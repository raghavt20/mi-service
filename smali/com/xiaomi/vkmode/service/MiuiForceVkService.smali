.class public Lcom/xiaomi/vkmode/service/MiuiForceVkService;
.super Lcom/xiaomi/vkmode/IMiuiForceVkService$Stub;
.source "MiuiForceVkService.java"

# interfaces
.implements Lcom/xiaomi/vkmode/service/MiuiForceVkServiceInternal;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/xiaomi/vkmode/service/MiuiForceVkService$CrashPkgInfo;,
        Lcom/xiaomi/vkmode/service/MiuiForceVkService$Lifecycle;
    }
.end annotation


# static fields
.field private static APP_NE_CRASH:Ljava/lang/String; = null

.field private static final QUEUE_MAX_SIZE:I = 0x1e

.field public static final SERVICE_NAME:Ljava/lang/String; = "MiuiForceVkService"

.field public static final TAG:Ljava/lang/String; = "APP_CRASHINFO_TRACK"

.field private static final TARGET_CRASH_MILLIS:J = 0xea60L

.field private static final TARGET_CRASH_NUM:I = 0x3

.field private static instance:Lcom/xiaomi/vkmode/service/MiuiForceVkService;


# instance fields
.field private crash_msg:Z

.field private final mCrashedBlackList:Landroid/util/ArraySet;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/util/ArraySet<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private final mCrashedQueue:Ljava/util/LinkedList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/LinkedList<",
            "Lcom/xiaomi/vkmode/service/MiuiForceVkService$CrashPkgInfo;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method private constructor <init>()V
    .locals 1

    .line 49
    invoke-direct {p0}, Lcom/xiaomi/vkmode/IMiuiForceVkService$Stub;-><init>()V

    .line 44
    new-instance v0, Ljava/util/LinkedList;

    invoke-direct {v0}, Ljava/util/LinkedList;-><init>()V

    iput-object v0, p0, Lcom/xiaomi/vkmode/service/MiuiForceVkService;->mCrashedQueue:Ljava/util/LinkedList;

    .line 45
    new-instance v0, Landroid/util/ArraySet;

    invoke-direct {v0}, Landroid/util/ArraySet;-><init>()V

    iput-object v0, p0, Lcom/xiaomi/vkmode/service/MiuiForceVkService;->mCrashedBlackList:Landroid/util/ArraySet;

    .line 47
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/xiaomi/vkmode/service/MiuiForceVkService;->crash_msg:Z

    .line 50
    const-class v0, Lcom/xiaomi/vkmode/service/MiuiForceVkServiceInternal;

    invoke-static {v0, p0}, Lcom/android/server/LocalServices;->addService(Ljava/lang/Class;Ljava/lang/Object;)V

    .line 51
    return-void
.end method

.method private cleanTimeOut()V
    .locals 8

    .line 150
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v0

    .line 151
    .local v0, "currentTime":J
    iget-object v2, p0, Lcom/xiaomi/vkmode/service/MiuiForceVkService;->mCrashedQueue:Ljava/util/LinkedList;

    invoke-virtual {v2}, Ljava/util/LinkedList;->iterator()Ljava/util/Iterator;

    move-result-object v2

    .line 152
    .local v2, "itr":Ljava/util/Iterator;, "Ljava/util/Iterator<Lcom/xiaomi/vkmode/service/MiuiForceVkService$CrashPkgInfo;>;"
    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_2

    .line 153
    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/xiaomi/vkmode/service/MiuiForceVkService$CrashPkgInfo;

    .line 154
    .local v3, "info":Lcom/xiaomi/vkmode/service/MiuiForceVkService$CrashPkgInfo;
    if-eqz v3, :cond_0

    iget-wide v4, v3, Lcom/xiaomi/vkmode/service/MiuiForceVkService$CrashPkgInfo;->reportTime:J

    sub-long v4, v0, v4

    const-wide/32 v6, 0xea60

    cmp-long v4, v4, v6

    if-lez v4, :cond_1

    .line 155
    :cond_0
    invoke-interface {v2}, Ljava/util/Iterator;->remove()V

    .line 157
    .end local v3    # "info":Lcom/xiaomi/vkmode/service/MiuiForceVkService$CrashPkgInfo;
    :cond_1
    goto :goto_0

    .line 158
    :cond_2
    return-void
.end method

.method private existTargetCrash(Ljava/lang/String;)Z
    .locals 6
    .param p1, "packageName"    # Ljava/lang/String;

    .line 132
    iget-object v0, p0, Lcom/xiaomi/vkmode/service/MiuiForceVkService;->mCrashedQueue:Ljava/util/LinkedList;

    invoke-virtual {v0}, Ljava/util/LinkedList;->size()I

    move-result v0

    const/4 v1, 0x0

    const/4 v2, 0x2

    if-ge v0, v2, :cond_0

    .line 133
    return v1

    .line 135
    :cond_0
    const/4 v0, 0x0

    .line 136
    .local v0, "count":I
    iget-object v3, p0, Lcom/xiaomi/vkmode/service/MiuiForceVkService;->mCrashedQueue:Ljava/util/LinkedList;

    invoke-virtual {v3}, Ljava/util/LinkedList;->iterator()Ljava/util/Iterator;

    move-result-object v3

    .line 137
    .local v3, "itr":Ljava/util/Iterator;, "Ljava/util/Iterator<Lcom/xiaomi/vkmode/service/MiuiForceVkService$CrashPkgInfo;>;"
    :goto_0
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v4

    if-eqz v4, :cond_3

    .line 138
    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lcom/xiaomi/vkmode/service/MiuiForceVkService$CrashPkgInfo;

    .line 139
    .local v4, "info":Lcom/xiaomi/vkmode/service/MiuiForceVkService$CrashPkgInfo;
    if-eqz v4, :cond_1

    iget-object v5, v4, Lcom/xiaomi/vkmode/service/MiuiForceVkService$CrashPkgInfo;->pkgName:Ljava/lang/String;

    invoke-virtual {p1, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-eqz v5, :cond_1

    .line 140
    add-int/lit8 v0, v0, 0x1

    .line 142
    :cond_1
    if-ne v0, v2, :cond_2

    .line 143
    const/4 v1, 0x1

    return v1

    .line 145
    .end local v4    # "info":Lcom/xiaomi/vkmode/service/MiuiForceVkService$CrashPkgInfo;
    :cond_2
    goto :goto_0

    .line 146
    :cond_3
    return v1
.end method

.method public static declared-synchronized getInstance()Lcom/xiaomi/vkmode/service/MiuiForceVkService;
    .locals 3

    const-class v0, Lcom/xiaomi/vkmode/service/MiuiForceVkService;

    monitor-enter v0

    .line 54
    :try_start_0
    sget-object v1, Lcom/xiaomi/vkmode/service/MiuiForceVkService;->instance:Lcom/xiaomi/vkmode/service/MiuiForceVkService;

    if-nez v1, :cond_0

    .line 55
    new-instance v1, Lcom/xiaomi/vkmode/service/MiuiForceVkService;

    invoke-direct {v1}, Lcom/xiaomi/vkmode/service/MiuiForceVkService;-><init>()V

    sput-object v1, Lcom/xiaomi/vkmode/service/MiuiForceVkService;->instance:Lcom/xiaomi/vkmode/service/MiuiForceVkService;

    .line 56
    const-string v1, "MiuiForceVkService"

    const-string v2, "MiuiForceVkService initialized"

    invoke-static {v1, v2}, Landroid/util/Slog;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 58
    :cond_0
    sget-object v1, Lcom/xiaomi/vkmode/service/MiuiForceVkService;->instance:Lcom/xiaomi/vkmode/service/MiuiForceVkService;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit v0

    return-object v1

    .line 53
    :catchall_0
    move-exception v1

    monitor-exit v0

    throw v1
.end method

.method private recordCrash(Ljava/lang/String;)V
    .locals 3
    .param p1, "packageName"    # Ljava/lang/String;

    .line 115
    invoke-direct {p0}, Lcom/xiaomi/vkmode/service/MiuiForceVkService;->cleanTimeOut()V

    .line 116
    invoke-direct {p0, p1}, Lcom/xiaomi/vkmode/service/MiuiForceVkService;->existTargetCrash(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 117
    iget-object v0, p0, Lcom/xiaomi/vkmode/service/MiuiForceVkService;->mCrashedBlackList:Landroid/util/ArraySet;

    invoke-virtual {v0, p1}, Landroid/util/ArraySet;->add(Ljava/lang/Object;)Z

    .line 118
    return-void

    .line 120
    :cond_0
    new-instance v0, Lcom/xiaomi/vkmode/service/MiuiForceVkService$CrashPkgInfo;

    const/4 v1, 0x0

    invoke-direct {v0, p0, v1}, Lcom/xiaomi/vkmode/service/MiuiForceVkService$CrashPkgInfo;-><init>(Lcom/xiaomi/vkmode/service/MiuiForceVkService;Lcom/xiaomi/vkmode/service/MiuiForceVkService$CrashPkgInfo-IA;)V

    .line 121
    .local v0, "info":Lcom/xiaomi/vkmode/service/MiuiForceVkService$CrashPkgInfo;
    iput-object p1, v0, Lcom/xiaomi/vkmode/service/MiuiForceVkService$CrashPkgInfo;->pkgName:Ljava/lang/String;

    .line 122
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v1

    iput-wide v1, v0, Lcom/xiaomi/vkmode/service/MiuiForceVkService$CrashPkgInfo;->reportTime:J

    .line 123
    iget-object v1, p0, Lcom/xiaomi/vkmode/service/MiuiForceVkService;->mCrashedQueue:Ljava/util/LinkedList;

    invoke-virtual {v1}, Ljava/util/LinkedList;->size()I

    move-result v1

    const/16 v2, 0x1e

    if-ge v1, v2, :cond_1

    .line 124
    iget-object v1, p0, Lcom/xiaomi/vkmode/service/MiuiForceVkService;->mCrashedQueue:Ljava/util/LinkedList;

    invoke-virtual {v1, v0}, Ljava/util/LinkedList;->addLast(Ljava/lang/Object;)V

    goto :goto_0

    .line 126
    :cond_1
    iget-object v1, p0, Lcom/xiaomi/vkmode/service/MiuiForceVkService;->mCrashedQueue:Ljava/util/LinkedList;

    invoke-virtual {v1}, Ljava/util/LinkedList;->removeFirst()Ljava/lang/Object;

    .line 127
    iget-object v1, p0, Lcom/xiaomi/vkmode/service/MiuiForceVkService;->mCrashedQueue:Ljava/util/LinkedList;

    invoke-virtual {v1, v0}, Ljava/util/LinkedList;->addLast(Ljava/lang/Object;)V

    .line 129
    :goto_0
    return-void
.end method


# virtual methods
.method public onAppCrashed(Ljava/lang/String;ZLjava/lang/String;)V
    .locals 5
    .param p1, "packageName"    # Ljava/lang/String;
    .param p2, "inNative"    # Z
    .param p3, "stackTrace"    # Ljava/lang/String;

    .line 88
    const-string v0, "persist.sys.vk_mode_enabled"

    const/4 v1, 0x0

    invoke-static {v0, v1}, Landroid/os/SystemProperties;->getBoolean(Ljava/lang/String;Z)Z

    move-result v0

    .line 89
    .local v0, "vkModeEnabled":Z
    if-nez v0, :cond_0

    .line 90
    return-void

    .line 92
    :cond_0
    invoke-virtual {p3}, Ljava/lang/String;->toLowerCase()Ljava/lang/String;

    move-result-object v2

    sput-object v2, Lcom/xiaomi/vkmode/service/MiuiForceVkService;->APP_NE_CRASH:Ljava/lang/String;

    .line 93
    const-string v3, "renderthread"

    invoke-virtual {v2, v3}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v2

    if-eqz v2, :cond_1

    sget-object v2, Lcom/xiaomi/vkmode/service/MiuiForceVkService;->APP_NE_CRASH:Ljava/lang/String;

    const-string v3, "grvkgpu"

    invoke-virtual {v2, v3}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v2

    if-eqz v2, :cond_1

    const/4 v1, 0x1

    :cond_1
    iput-boolean v1, p0, Lcom/xiaomi/vkmode/service/MiuiForceVkService;->crash_msg:Z

    .line 94
    if-eqz p1, :cond_5

    if-eqz p2, :cond_5

    if-eqz p3, :cond_5

    if-nez v1, :cond_2

    goto :goto_0

    .line 97
    :cond_2
    invoke-static {}, Lcom/android/server/MiuiCommonCloudServiceStub;->getInstance()Lcom/android/server/MiuiCommonCloudServiceStub;

    move-result-object v1

    const-string v2, "miui_vk_mode"

    invoke-virtual {v1, v2}, Lcom/android/server/MiuiCommonCloudServiceStub;->getDataByModuleName(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/xiaomi/vkmode/VkRulesData;

    .line 98
    .local v1, "vkRulesData":Lcom/xiaomi/vkmode/VkRulesData;
    if-nez v1, :cond_3

    .line 99
    return-void

    .line 101
    :cond_3
    invoke-virtual {v1}, Lcom/xiaomi/vkmode/VkRulesData;->getPackages()Landroid/util/ArraySet;

    move-result-object v2

    .line 102
    .local v2, "pkgSet":Landroid/util/ArraySet;, "Landroid/util/ArraySet<Ljava/lang/String;>;"
    iget-object v3, p0, Lcom/xiaomi/vkmode/service/MiuiForceVkService;->mCrashedBlackList:Landroid/util/ArraySet;

    invoke-virtual {v3, p1}, Landroid/util/ArraySet;->contains(Ljava/lang/Object;)Z

    move-result v3

    if-nez v3, :cond_4

    if-eqz v2, :cond_4

    invoke-virtual {v2, p1}, Landroid/util/ArraySet;->contains(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_4

    .line 103
    invoke-static {}, Lcom/android/server/wm/OneTrackVulkanHelper;->getInstance()Lcom/android/server/wm/OneTrackVulkanHelper;

    move-result-object v3

    invoke-virtual {v3, p3}, Lcom/android/server/wm/OneTrackVulkanHelper;->getCrashInfo(Ljava/lang/String;)V

    .line 104
    invoke-static {}, Lcom/android/server/wm/OneTrackVulkanHelper;->getInstance()Lcom/android/server/wm/OneTrackVulkanHelper;

    move-result-object v3

    invoke-virtual {v3, p1}, Lcom/android/server/wm/OneTrackVulkanHelper;->reportOneTrack(Ljava/lang/String;)V

    .line 105
    invoke-direct {p0, p1}, Lcom/xiaomi/vkmode/service/MiuiForceVkService;->recordCrash(Ljava/lang/String;)V

    .line 106
    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v4, "target crash recorded, pkg="

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    const-string v4, "MiuiForceVkService"

    invoke-static {v4, v3}, Landroid/util/Slog;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 108
    :cond_4
    return-void

    .line 95
    .end local v1    # "vkRulesData":Lcom/xiaomi/vkmode/VkRulesData;
    .end local v2    # "pkgSet":Landroid/util/ArraySet;, "Landroid/util/ArraySet<Ljava/lang/String;>;"
    :cond_5
    :goto_0
    return-void
.end method

.method public shouldUseVk()Z
    .locals 6

    .line 63
    invoke-static {}, Landroid/os/Binder;->getCallingPid()I

    move-result v0

    invoke-static {v0}, Lcom/android/server/am/ProcessUtils;->getPackageNameByPid(I)Ljava/lang/String;

    move-result-object v0

    .line 64
    .local v0, "packageName":Ljava/lang/String;
    const/4 v1, 0x0

    const-string v2, "MiuiForceVkService"

    if-nez v0, :cond_0

    .line 65
    const-string v3, "request packageName is null"

    invoke-static {v2, v3}, Landroid/util/Slog;->w(Ljava/lang/String;Ljava/lang/String;)I

    .line 66
    return v1

    .line 68
    :cond_0
    iget-object v3, p0, Lcom/xiaomi/vkmode/service/MiuiForceVkService;->mCrashedBlackList:Landroid/util/ArraySet;

    invoke-virtual {v3, v0}, Landroid/util/ArraySet;->contains(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_1

    .line 69
    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v4, "target ne false, pkg="

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Landroid/util/Slog;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 70
    return v1

    .line 73
    :cond_1
    invoke-static {}, Lcom/android/server/MiuiCommonCloudServiceStub;->getInstance()Lcom/android/server/MiuiCommonCloudServiceStub;

    move-result-object v3

    const-string v4, "miui_vk_mode"

    invoke-virtual {v3, v4}, Lcom/android/server/MiuiCommonCloudServiceStub;->getDataByModuleName(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/xiaomi/vkmode/VkRulesData;

    .line 74
    .local v3, "vkRulesData":Lcom/xiaomi/vkmode/VkRulesData;
    if-nez v3, :cond_2

    .line 75
    const-string/jumbo v4, "vkRulesData is null"

    invoke-static {v2, v4}, Landroid/util/Slog;->w(Ljava/lang/String;Ljava/lang/String;)I

    .line 76
    return v1

    .line 78
    :cond_2
    invoke-virtual {v3}, Lcom/xiaomi/vkmode/VkRulesData;->getPackages()Landroid/util/ArraySet;

    move-result-object v4

    .line 79
    .local v4, "pkgSet":Landroid/util/ArraySet;, "Landroid/util/ArraySet<Ljava/lang/String;>;"
    if-eqz v4, :cond_3

    invoke-virtual {v4, v0}, Landroid/util/ArraySet;->contains(Ljava/lang/Object;)Z

    move-result v5

    if-eqz v5, :cond_3

    .line 80
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v5, "shouldUseVk true, pkg="

    invoke-virtual {v1, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v2, v1}, Landroid/util/Slog;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 81
    const/4 v1, 0x1

    return v1

    .line 83
    :cond_3
    return v1
.end method
