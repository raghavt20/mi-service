public class com.xiaomi.vkmode.service.MiuiForceVkService extends com.xiaomi.vkmode.IMiuiForceVkService$Stub implements com.xiaomi.vkmode.service.MiuiForceVkServiceInternal {
	 /* .source "MiuiForceVkService.java" */
	 /* # interfaces */
	 /* # annotations */
	 /* .annotation system Ldalvik/annotation/MemberClasses; */
	 /* value = { */
	 /* Lcom/xiaomi/vkmode/service/MiuiForceVkService$CrashPkgInfo;, */
	 /* Lcom/xiaomi/vkmode/service/MiuiForceVkService$Lifecycle; */
	 /* } */
} // .end annotation
/* # static fields */
private static java.lang.String APP_NE_CRASH;
private static final Integer QUEUE_MAX_SIZE;
public static final java.lang.String SERVICE_NAME;
public static final java.lang.String TAG;
private static final Long TARGET_CRASH_MILLIS;
private static final Integer TARGET_CRASH_NUM;
private static com.xiaomi.vkmode.service.MiuiForceVkService instance;
/* # instance fields */
private Boolean crash_msg;
private final android.util.ArraySet mCrashedBlackList;
/* .annotation system Ldalvik/annotation/Signature; */
/* value = { */
/* "Landroid/util/ArraySet<", */
/* "Ljava/lang/String;", */
/* ">;" */
/* } */
} // .end annotation
} // .end field
private final java.util.LinkedList mCrashedQueue;
/* .annotation system Ldalvik/annotation/Signature; */
/* value = { */
/* "Ljava/util/LinkedList<", */
/* "Lcom/xiaomi/vkmode/service/MiuiForceVkService$CrashPkgInfo;", */
/* ">;" */
/* } */
} // .end annotation
} // .end field
/* # direct methods */
private com.xiaomi.vkmode.service.MiuiForceVkService ( ) {
/* .locals 1 */
/* .line 49 */
/* invoke-direct {p0}, Lcom/xiaomi/vkmode/IMiuiForceVkService$Stub;-><init>()V */
/* .line 44 */
/* new-instance v0, Ljava/util/LinkedList; */
/* invoke-direct {v0}, Ljava/util/LinkedList;-><init>()V */
this.mCrashedQueue = v0;
/* .line 45 */
/* new-instance v0, Landroid/util/ArraySet; */
/* invoke-direct {v0}, Landroid/util/ArraySet;-><init>()V */
this.mCrashedBlackList = v0;
/* .line 47 */
int v0 = 0; // const/4 v0, 0x0
/* iput-boolean v0, p0, Lcom/xiaomi/vkmode/service/MiuiForceVkService;->crash_msg:Z */
/* .line 50 */
/* const-class v0, Lcom/xiaomi/vkmode/service/MiuiForceVkServiceInternal; */
com.android.server.LocalServices .addService ( v0,p0 );
/* .line 51 */
return;
} // .end method
private void cleanTimeOut ( ) {
/* .locals 8 */
/* .line 150 */
java.lang.System .currentTimeMillis ( );
/* move-result-wide v0 */
/* .line 151 */
/* .local v0, "currentTime":J */
v2 = this.mCrashedQueue;
(( java.util.LinkedList ) v2 ).iterator ( ); // invoke-virtual {v2}, Ljava/util/LinkedList;->iterator()Ljava/util/Iterator;
/* .line 152 */
/* .local v2, "itr":Ljava/util/Iterator;, "Ljava/util/Iterator<Lcom/xiaomi/vkmode/service/MiuiForceVkService$CrashPkgInfo;>;" */
v3 = } // :goto_0
if ( v3 != null) { // if-eqz v3, :cond_2
/* .line 153 */
/* check-cast v3, Lcom/xiaomi/vkmode/service/MiuiForceVkService$CrashPkgInfo; */
/* .line 154 */
/* .local v3, "info":Lcom/xiaomi/vkmode/service/MiuiForceVkService$CrashPkgInfo; */
if ( v3 != null) { // if-eqz v3, :cond_0
/* iget-wide v4, v3, Lcom/xiaomi/vkmode/service/MiuiForceVkService$CrashPkgInfo;->reportTime:J */
/* sub-long v4, v0, v4 */
/* const-wide/32 v6, 0xea60 */
/* cmp-long v4, v4, v6 */
/* if-lez v4, :cond_1 */
/* .line 155 */
} // :cond_0
/* .line 157 */
} // .end local v3 # "info":Lcom/xiaomi/vkmode/service/MiuiForceVkService$CrashPkgInfo;
} // :cond_1
/* .line 158 */
} // :cond_2
return;
} // .end method
private Boolean existTargetCrash ( java.lang.String p0 ) {
/* .locals 6 */
/* .param p1, "packageName" # Ljava/lang/String; */
/* .line 132 */
v0 = this.mCrashedQueue;
v0 = (( java.util.LinkedList ) v0 ).size ( ); // invoke-virtual {v0}, Ljava/util/LinkedList;->size()I
int v1 = 0; // const/4 v1, 0x0
int v2 = 2; // const/4 v2, 0x2
/* if-ge v0, v2, :cond_0 */
/* .line 133 */
/* .line 135 */
} // :cond_0
int v0 = 0; // const/4 v0, 0x0
/* .line 136 */
/* .local v0, "count":I */
v3 = this.mCrashedQueue;
(( java.util.LinkedList ) v3 ).iterator ( ); // invoke-virtual {v3}, Ljava/util/LinkedList;->iterator()Ljava/util/Iterator;
/* .line 137 */
/* .local v3, "itr":Ljava/util/Iterator;, "Ljava/util/Iterator<Lcom/xiaomi/vkmode/service/MiuiForceVkService$CrashPkgInfo;>;" */
v4 = } // :goto_0
if ( v4 != null) { // if-eqz v4, :cond_3
/* .line 138 */
/* check-cast v4, Lcom/xiaomi/vkmode/service/MiuiForceVkService$CrashPkgInfo; */
/* .line 139 */
/* .local v4, "info":Lcom/xiaomi/vkmode/service/MiuiForceVkService$CrashPkgInfo; */
if ( v4 != null) { // if-eqz v4, :cond_1
v5 = this.pkgName;
v5 = (( java.lang.String ) p1 ).equals ( v5 ); // invoke-virtual {p1, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z
if ( v5 != null) { // if-eqz v5, :cond_1
/* .line 140 */
/* add-int/lit8 v0, v0, 0x1 */
/* .line 142 */
} // :cond_1
/* if-ne v0, v2, :cond_2 */
/* .line 143 */
int v1 = 1; // const/4 v1, 0x1
/* .line 145 */
} // .end local v4 # "info":Lcom/xiaomi/vkmode/service/MiuiForceVkService$CrashPkgInfo;
} // :cond_2
/* .line 146 */
} // :cond_3
} // .end method
public static synchronized com.xiaomi.vkmode.service.MiuiForceVkService getInstance ( ) {
/* .locals 3 */
/* const-class v0, Lcom/xiaomi/vkmode/service/MiuiForceVkService; */
/* monitor-enter v0 */
/* .line 54 */
try { // :try_start_0
v1 = com.xiaomi.vkmode.service.MiuiForceVkService.instance;
/* if-nez v1, :cond_0 */
/* .line 55 */
/* new-instance v1, Lcom/xiaomi/vkmode/service/MiuiForceVkService; */
/* invoke-direct {v1}, Lcom/xiaomi/vkmode/service/MiuiForceVkService;-><init>()V */
/* .line 56 */
final String v1 = "MiuiForceVkService"; // const-string v1, "MiuiForceVkService"
final String v2 = "MiuiForceVkService initialized"; // const-string v2, "MiuiForceVkService initialized"
android.util.Slog .d ( v1,v2 );
/* .line 58 */
} // :cond_0
v1 = com.xiaomi.vkmode.service.MiuiForceVkService.instance;
/* :try_end_0 */
/* .catchall {:try_start_0 ..:try_end_0} :catchall_0 */
/* monitor-exit v0 */
/* .line 53 */
/* :catchall_0 */
/* move-exception v1 */
/* monitor-exit v0 */
/* throw v1 */
} // .end method
private void recordCrash ( java.lang.String p0 ) {
/* .locals 3 */
/* .param p1, "packageName" # Ljava/lang/String; */
/* .line 115 */
/* invoke-direct {p0}, Lcom/xiaomi/vkmode/service/MiuiForceVkService;->cleanTimeOut()V */
/* .line 116 */
v0 = /* invoke-direct {p0, p1}, Lcom/xiaomi/vkmode/service/MiuiForceVkService;->existTargetCrash(Ljava/lang/String;)Z */
if ( v0 != null) { // if-eqz v0, :cond_0
/* .line 117 */
v0 = this.mCrashedBlackList;
(( android.util.ArraySet ) v0 ).add ( p1 ); // invoke-virtual {v0, p1}, Landroid/util/ArraySet;->add(Ljava/lang/Object;)Z
/* .line 118 */
return;
/* .line 120 */
} // :cond_0
/* new-instance v0, Lcom/xiaomi/vkmode/service/MiuiForceVkService$CrashPkgInfo; */
int v1 = 0; // const/4 v1, 0x0
/* invoke-direct {v0, p0, v1}, Lcom/xiaomi/vkmode/service/MiuiForceVkService$CrashPkgInfo;-><init>(Lcom/xiaomi/vkmode/service/MiuiForceVkService;Lcom/xiaomi/vkmode/service/MiuiForceVkService$CrashPkgInfo-IA;)V */
/* .line 121 */
/* .local v0, "info":Lcom/xiaomi/vkmode/service/MiuiForceVkService$CrashPkgInfo; */
this.pkgName = p1;
/* .line 122 */
java.lang.System .currentTimeMillis ( );
/* move-result-wide v1 */
/* iput-wide v1, v0, Lcom/xiaomi/vkmode/service/MiuiForceVkService$CrashPkgInfo;->reportTime:J */
/* .line 123 */
v1 = this.mCrashedQueue;
v1 = (( java.util.LinkedList ) v1 ).size ( ); // invoke-virtual {v1}, Ljava/util/LinkedList;->size()I
/* const/16 v2, 0x1e */
/* if-ge v1, v2, :cond_1 */
/* .line 124 */
v1 = this.mCrashedQueue;
(( java.util.LinkedList ) v1 ).addLast ( v0 ); // invoke-virtual {v1, v0}, Ljava/util/LinkedList;->addLast(Ljava/lang/Object;)V
/* .line 126 */
} // :cond_1
v1 = this.mCrashedQueue;
(( java.util.LinkedList ) v1 ).removeFirst ( ); // invoke-virtual {v1}, Ljava/util/LinkedList;->removeFirst()Ljava/lang/Object;
/* .line 127 */
v1 = this.mCrashedQueue;
(( java.util.LinkedList ) v1 ).addLast ( v0 ); // invoke-virtual {v1, v0}, Ljava/util/LinkedList;->addLast(Ljava/lang/Object;)V
/* .line 129 */
} // :goto_0
return;
} // .end method
/* # virtual methods */
public void onAppCrashed ( java.lang.String p0, Boolean p1, java.lang.String p2 ) {
/* .locals 5 */
/* .param p1, "packageName" # Ljava/lang/String; */
/* .param p2, "inNative" # Z */
/* .param p3, "stackTrace" # Ljava/lang/String; */
/* .line 88 */
final String v0 = "persist.sys.vk_mode_enabled"; // const-string v0, "persist.sys.vk_mode_enabled"
int v1 = 0; // const/4 v1, 0x0
v0 = android.os.SystemProperties .getBoolean ( v0,v1 );
/* .line 89 */
/* .local v0, "vkModeEnabled":Z */
/* if-nez v0, :cond_0 */
/* .line 90 */
return;
/* .line 92 */
} // :cond_0
(( java.lang.String ) p3 ).toLowerCase ( ); // invoke-virtual {p3}, Ljava/lang/String;->toLowerCase()Ljava/lang/String;
/* .line 93 */
final String v3 = "renderthread"; // const-string v3, "renderthread"
v2 = (( java.lang.String ) v2 ).contains ( v3 ); // invoke-virtual {v2, v3}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z
if ( v2 != null) { // if-eqz v2, :cond_1
v2 = com.xiaomi.vkmode.service.MiuiForceVkService.APP_NE_CRASH;
final String v3 = "grvkgpu"; // const-string v3, "grvkgpu"
v2 = (( java.lang.String ) v2 ).contains ( v3 ); // invoke-virtual {v2, v3}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z
if ( v2 != null) { // if-eqz v2, :cond_1
int v1 = 1; // const/4 v1, 0x1
} // :cond_1
/* iput-boolean v1, p0, Lcom/xiaomi/vkmode/service/MiuiForceVkService;->crash_msg:Z */
/* .line 94 */
if ( p1 != null) { // if-eqz p1, :cond_5
if ( p2 != null) { // if-eqz p2, :cond_5
if ( p3 != null) { // if-eqz p3, :cond_5
/* if-nez v1, :cond_2 */
/* .line 97 */
} // :cond_2
com.android.server.MiuiCommonCloudServiceStub .getInstance ( );
final String v2 = "miui_vk_mode"; // const-string v2, "miui_vk_mode"
(( com.android.server.MiuiCommonCloudServiceStub ) v1 ).getDataByModuleName ( v2 ); // invoke-virtual {v1, v2}, Lcom/android/server/MiuiCommonCloudServiceStub;->getDataByModuleName(Ljava/lang/String;)Ljava/lang/Object;
/* check-cast v1, Lcom/xiaomi/vkmode/VkRulesData; */
/* .line 98 */
/* .local v1, "vkRulesData":Lcom/xiaomi/vkmode/VkRulesData; */
/* if-nez v1, :cond_3 */
/* .line 99 */
return;
/* .line 101 */
} // :cond_3
(( com.xiaomi.vkmode.VkRulesData ) v1 ).getPackages ( ); // invoke-virtual {v1}, Lcom/xiaomi/vkmode/VkRulesData;->getPackages()Landroid/util/ArraySet;
/* .line 102 */
/* .local v2, "pkgSet":Landroid/util/ArraySet;, "Landroid/util/ArraySet<Ljava/lang/String;>;" */
v3 = this.mCrashedBlackList;
v3 = (( android.util.ArraySet ) v3 ).contains ( p1 ); // invoke-virtual {v3, p1}, Landroid/util/ArraySet;->contains(Ljava/lang/Object;)Z
/* if-nez v3, :cond_4 */
if ( v2 != null) { // if-eqz v2, :cond_4
v3 = (( android.util.ArraySet ) v2 ).contains ( p1 ); // invoke-virtual {v2, p1}, Landroid/util/ArraySet;->contains(Ljava/lang/Object;)Z
if ( v3 != null) { // if-eqz v3, :cond_4
/* .line 103 */
com.android.server.wm.OneTrackVulkanHelper .getInstance ( );
(( com.android.server.wm.OneTrackVulkanHelper ) v3 ).getCrashInfo ( p3 ); // invoke-virtual {v3, p3}, Lcom/android/server/wm/OneTrackVulkanHelper;->getCrashInfo(Ljava/lang/String;)V
/* .line 104 */
com.android.server.wm.OneTrackVulkanHelper .getInstance ( );
(( com.android.server.wm.OneTrackVulkanHelper ) v3 ).reportOneTrack ( p1 ); // invoke-virtual {v3, p1}, Lcom/android/server/wm/OneTrackVulkanHelper;->reportOneTrack(Ljava/lang/String;)V
/* .line 105 */
/* invoke-direct {p0, p1}, Lcom/xiaomi/vkmode/service/MiuiForceVkService;->recordCrash(Ljava/lang/String;)V */
/* .line 106 */
/* new-instance v3, Ljava/lang/StringBuilder; */
/* invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V */
/* const-string/jumbo v4, "target crash recorded, pkg=" */
(( java.lang.StringBuilder ) v3 ).append ( v4 ); // invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v3 ).append ( p1 ); // invoke-virtual {v3, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v3 ).toString ( ); // invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
final String v4 = "MiuiForceVkService"; // const-string v4, "MiuiForceVkService"
android.util.Slog .d ( v4,v3 );
/* .line 108 */
} // :cond_4
return;
/* .line 95 */
} // .end local v1 # "vkRulesData":Lcom/xiaomi/vkmode/VkRulesData;
} // .end local v2 # "pkgSet":Landroid/util/ArraySet;, "Landroid/util/ArraySet<Ljava/lang/String;>;"
} // :cond_5
} // :goto_0
return;
} // .end method
public Boolean shouldUseVk ( ) {
/* .locals 6 */
/* .line 63 */
v0 = android.os.Binder .getCallingPid ( );
com.android.server.am.ProcessUtils .getPackageNameByPid ( v0 );
/* .line 64 */
/* .local v0, "packageName":Ljava/lang/String; */
int v1 = 0; // const/4 v1, 0x0
final String v2 = "MiuiForceVkService"; // const-string v2, "MiuiForceVkService"
/* if-nez v0, :cond_0 */
/* .line 65 */
final String v3 = "request packageName is null"; // const-string v3, "request packageName is null"
android.util.Slog .w ( v2,v3 );
/* .line 66 */
/* .line 68 */
} // :cond_0
v3 = this.mCrashedBlackList;
v3 = (( android.util.ArraySet ) v3 ).contains ( v0 ); // invoke-virtual {v3, v0}, Landroid/util/ArraySet;->contains(Ljava/lang/Object;)Z
if ( v3 != null) { // if-eqz v3, :cond_1
/* .line 69 */
/* new-instance v3, Ljava/lang/StringBuilder; */
/* invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V */
/* const-string/jumbo v4, "target ne false, pkg=" */
(( java.lang.StringBuilder ) v3 ).append ( v4 ); // invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v3 ).append ( v0 ); // invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v3 ).toString ( ); // invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
android.util.Slog .d ( v2,v3 );
/* .line 70 */
/* .line 73 */
} // :cond_1
com.android.server.MiuiCommonCloudServiceStub .getInstance ( );
final String v4 = "miui_vk_mode"; // const-string v4, "miui_vk_mode"
(( com.android.server.MiuiCommonCloudServiceStub ) v3 ).getDataByModuleName ( v4 ); // invoke-virtual {v3, v4}, Lcom/android/server/MiuiCommonCloudServiceStub;->getDataByModuleName(Ljava/lang/String;)Ljava/lang/Object;
/* check-cast v3, Lcom/xiaomi/vkmode/VkRulesData; */
/* .line 74 */
/* .local v3, "vkRulesData":Lcom/xiaomi/vkmode/VkRulesData; */
/* if-nez v3, :cond_2 */
/* .line 75 */
/* const-string/jumbo v4, "vkRulesData is null" */
android.util.Slog .w ( v2,v4 );
/* .line 76 */
/* .line 78 */
} // :cond_2
(( com.xiaomi.vkmode.VkRulesData ) v3 ).getPackages ( ); // invoke-virtual {v3}, Lcom/xiaomi/vkmode/VkRulesData;->getPackages()Landroid/util/ArraySet;
/* .line 79 */
/* .local v4, "pkgSet":Landroid/util/ArraySet;, "Landroid/util/ArraySet<Ljava/lang/String;>;" */
if ( v4 != null) { // if-eqz v4, :cond_3
v5 = (( android.util.ArraySet ) v4 ).contains ( v0 ); // invoke-virtual {v4, v0}, Landroid/util/ArraySet;->contains(Ljava/lang/Object;)Z
if ( v5 != null) { // if-eqz v5, :cond_3
/* .line 80 */
/* new-instance v1, Ljava/lang/StringBuilder; */
/* invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V */
/* const-string/jumbo v5, "shouldUseVk true, pkg=" */
(( java.lang.StringBuilder ) v1 ).append ( v5 ); // invoke-virtual {v1, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v1 ).append ( v0 ); // invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v1 ).toString ( ); // invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
android.util.Slog .d ( v2,v1 );
/* .line 81 */
int v1 = 1; // const/4 v1, 0x1
/* .line 83 */
} // :cond_3
} // .end method
