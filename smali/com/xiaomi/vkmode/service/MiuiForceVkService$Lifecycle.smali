.class public final Lcom/xiaomi/vkmode/service/MiuiForceVkService$Lifecycle;
.super Lcom/android/server/SystemService;
.source "MiuiForceVkService.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/xiaomi/vkmode/service/MiuiForceVkService;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "Lifecycle"
.end annotation


# instance fields
.field private final mService:Lcom/xiaomi/vkmode/service/MiuiForceVkService;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 1
    .param p1, "context"    # Landroid/content/Context;

    .line 28
    invoke-direct {p0, p1}, Lcom/android/server/SystemService;-><init>(Landroid/content/Context;)V

    .line 29
    invoke-static {}, Lcom/xiaomi/vkmode/service/MiuiForceVkService;->getInstance()Lcom/xiaomi/vkmode/service/MiuiForceVkService;

    move-result-object v0

    iput-object v0, p0, Lcom/xiaomi/vkmode/service/MiuiForceVkService$Lifecycle;->mService:Lcom/xiaomi/vkmode/service/MiuiForceVkService;

    .line 30
    return-void
.end method


# virtual methods
.method public onStart()V
    .locals 2

    .line 34
    const-string v0, "MiuiForceVkService"

    iget-object v1, p0, Lcom/xiaomi/vkmode/service/MiuiForceVkService$Lifecycle;->mService:Lcom/xiaomi/vkmode/service/MiuiForceVkService;

    invoke-virtual {p0, v0, v1}, Lcom/xiaomi/vkmode/service/MiuiForceVkService$Lifecycle;->publishBinderService(Ljava/lang/String;Landroid/os/IBinder;)V

    .line 35
    return-void
.end method
