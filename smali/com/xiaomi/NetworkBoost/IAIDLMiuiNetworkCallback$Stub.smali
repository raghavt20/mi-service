.class public abstract Lcom/xiaomi/NetworkBoost/IAIDLMiuiNetworkCallback$Stub;
.super Landroid/os/Binder;
.source "IAIDLMiuiNetworkCallback.java"

# interfaces
.implements Lcom/xiaomi/NetworkBoost/IAIDLMiuiNetworkCallback;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/xiaomi/NetworkBoost/IAIDLMiuiNetworkCallback;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x409
    name = "Stub"
.end annotation

.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/xiaomi/NetworkBoost/IAIDLMiuiNetworkCallback$Stub$a;
    }
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 1

    .line 1
    invoke-direct {p0}, Landroid/os/Binder;-><init>()V

    .line 2
    const-string v0, "com.xiaomi.NetworkBoost.IAIDLMiuiNetworkCallback"

    invoke-virtual {p0, p0, v0}, Landroid/os/Binder;->attachInterface(Landroid/os/IInterface;Ljava/lang/String;)V

    return-void
.end method

.method public static asInterface(Landroid/os/IBinder;)Lcom/xiaomi/NetworkBoost/IAIDLMiuiNetworkCallback;
    .locals 2

    if-nez p0, :cond_0

    const/4 p0, 0x0

    return-object p0

    .line 1
    :cond_0
    const-string v0, "com.xiaomi.NetworkBoost.IAIDLMiuiNetworkCallback"

    invoke-interface {p0, v0}, Landroid/os/IBinder;->queryLocalInterface(Ljava/lang/String;)Landroid/os/IInterface;

    move-result-object v0

    if-eqz v0, :cond_1

    .line 2
    instance-of v1, v0, Lcom/xiaomi/NetworkBoost/IAIDLMiuiNetworkCallback;

    if-eqz v1, :cond_1

    .line 3
    check-cast v0, Lcom/xiaomi/NetworkBoost/IAIDLMiuiNetworkCallback;

    return-object v0

    .line 5
    :cond_1
    new-instance v0, Lcom/xiaomi/NetworkBoost/IAIDLMiuiNetworkCallback$Stub$a;

    invoke-direct {v0, p0}, Lcom/xiaomi/NetworkBoost/IAIDLMiuiNetworkCallback$Stub$a;-><init>(Landroid/os/IBinder;)V

    return-object v0
.end method

.method public static getDefaultImpl()Lcom/xiaomi/NetworkBoost/IAIDLMiuiNetworkCallback;
    .locals 1

    .line 1
    sget-object v0, Lcom/xiaomi/NetworkBoost/IAIDLMiuiNetworkCallback$Stub$a;->b:Lcom/xiaomi/NetworkBoost/IAIDLMiuiNetworkCallback;

    return-object v0
.end method

.method public static setDefaultImpl(Lcom/xiaomi/NetworkBoost/IAIDLMiuiNetworkCallback;)Z
    .locals 1

    .line 1
    sget-object v0, Lcom/xiaomi/NetworkBoost/IAIDLMiuiNetworkCallback$Stub$a;->b:Lcom/xiaomi/NetworkBoost/IAIDLMiuiNetworkCallback;

    if-nez v0, :cond_1

    if-eqz p0, :cond_0

    .line 5
    sput-object p0, Lcom/xiaomi/NetworkBoost/IAIDLMiuiNetworkCallback$Stub$a;->b:Lcom/xiaomi/NetworkBoost/IAIDLMiuiNetworkCallback;

    const/4 p0, 0x1

    return p0

    :cond_0
    const/4 p0, 0x0

    return p0

    .line 6
    :cond_1
    new-instance p0, Ljava/lang/IllegalStateException;

    const-string/jumbo v0, "setDefaultImpl() called twice"

    invoke-direct {p0, v0}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw p0
.end method


# virtual methods
.method public asBinder()Landroid/os/IBinder;
    .locals 0

    return-object p0
.end method

.method public onTransact(ILandroid/os/Parcel;Landroid/os/Parcel;I)Z
    .locals 3
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    .line 1
    const v0, 0x5f4e5446

    const/4 v1, 0x1

    const-string v2, "com.xiaomi.NetworkBoost.IAIDLMiuiNetworkCallback"

    if-eq p1, v0, :cond_5

    const/4 v0, 0x0

    packed-switch p1, :pswitch_data_0

    .line 107
    invoke-super {p0, p1, p2, p3, p4}, Landroid/os/Binder;->onTransact(ILandroid/os/Parcel;Landroid/os/Parcel;I)Z

    move-result p1

    return p1

    .line 108
    :pswitch_0
    invoke-virtual {p2, v2}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    .line 110
    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    move-result p1

    .line 111
    invoke-interface {p0, p1}, Lcom/xiaomi/NetworkBoost/IAIDLMiuiNetworkCallback;->onSlaveWifiEnableV1(I)V

    return v1

    .line 112
    :pswitch_1
    invoke-virtual {p2, v2}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    .line 114
    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    move-result p1

    .line 116
    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    move-result p3

    .line 118
    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    move-result p2

    .line 119
    invoke-interface {p0, p1, p3, p2}, Lcom/xiaomi/NetworkBoost/IAIDLMiuiNetworkCallback;->onNetworkPriorityChanged(III)V

    return v1

    .line 120
    :pswitch_2
    invoke-virtual {p2, v2}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    .line 122
    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    move-result p1

    .line 124
    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    move-result p3

    .line 126
    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    move-result p2

    .line 127
    invoke-interface {p0, p1, p3, p2}, Lcom/xiaomi/NetworkBoost/IAIDLMiuiNetworkCallback;->mediaPlayerPolicyNotify(III)V

    return v1

    .line 128
    :pswitch_3
    invoke-virtual {p2, v2}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    .line 130
    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    move-result p1

    if-eqz p1, :cond_0

    move v0, v1

    .line 131
    :cond_0
    invoke-interface {p0, v0}, Lcom/xiaomi/NetworkBoost/IAIDLMiuiNetworkCallback;->dsdaStateChanged(Z)V

    return v1

    .line 132
    :pswitch_4
    invoke-virtual {p2, v2}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    .line 134
    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    move-result p1

    if-eqz p1, :cond_1

    move v0, v1

    .line 135
    :cond_1
    invoke-interface {p0, v0}, Lcom/xiaomi/NetworkBoost/IAIDLMiuiNetworkCallback;->onSlaveWifiEnable(Z)V

    return v1

    .line 136
    :pswitch_5
    invoke-virtual {p2, v2}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    .line 138
    invoke-virtual {p2}, Landroid/os/Parcel;->createStringArrayList()Ljava/util/ArrayList;

    move-result-object p1

    .line 139
    invoke-interface {p0, p1}, Lcom/xiaomi/NetworkBoost/IAIDLMiuiNetworkCallback;->ifaceRemoved(Ljava/util/List;)V

    return v1

    .line 140
    :pswitch_6
    invoke-virtual {p2, v2}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    .line 142
    invoke-virtual {p2}, Landroid/os/Parcel;->createStringArrayList()Ljava/util/ArrayList;

    move-result-object p1

    .line 143
    invoke-interface {p0, p1}, Lcom/xiaomi/NetworkBoost/IAIDLMiuiNetworkCallback;->ifaceAdded(Ljava/util/List;)V

    return v1

    .line 144
    :pswitch_7
    invoke-virtual {p2, v2}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    .line 146
    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    move-result p1

    if-eqz p1, :cond_2

    move v0, v1

    .line 147
    :cond_2
    invoke-interface {p0, v0}, Lcom/xiaomi/NetworkBoost/IAIDLMiuiNetworkCallback;->onSlaveWifiDisconnected(Z)V

    return v1

    .line 148
    :pswitch_8
    invoke-virtual {p2, v2}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    .line 150
    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    move-result p1

    if-eqz p1, :cond_3

    move v0, v1

    .line 151
    :cond_3
    invoke-interface {p0, v0}, Lcom/xiaomi/NetworkBoost/IAIDLMiuiNetworkCallback;->onSlaveWifiConnected(Z)V

    return v1

    .line 152
    :pswitch_9
    invoke-virtual {p2, v2}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    .line 154
    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    move-result p1

    if-eqz p1, :cond_4

    move v0, v1

    .line 155
    :cond_4
    invoke-interface {p0, v0}, Lcom/xiaomi/NetworkBoost/IAIDLMiuiNetworkCallback;->onSetSlaveWifiResult(Z)V

    return v1

    .line 156
    :pswitch_a
    invoke-virtual {p2, v2}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    .line 158
    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    move-result p1

    .line 159
    invoke-interface {p0, p1}, Lcom/xiaomi/NetworkBoost/IAIDLMiuiNetworkCallback;->onScanSuccussed(I)V

    return v1

    .line 160
    :cond_5
    invoke-virtual {p3, v2}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    return v1

    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_a
        :pswitch_9
        :pswitch_8
        :pswitch_7
        :pswitch_6
        :pswitch_5
        :pswitch_4
        :pswitch_3
        :pswitch_2
        :pswitch_1
        :pswitch_0
    .end packed-switch
.end method
