public class com.xiaomi.NetworkBoost.NetworkBoostService$SLAServiceBinder extends com.xiaomi.NetworkBoost.slaservice.IAIDLSLAService$Stub {
	 /* .source "NetworkBoostService.java" */
	 /* # annotations */
	 /* .annotation system Ldalvik/annotation/EnclosingClass; */
	 /* value = Lcom/xiaomi/NetworkBoost/NetworkBoostService; */
} // .end annotation
/* .annotation system Ldalvik/annotation/InnerClass; */
/* accessFlags = 0x1 */
/* name = "SLAServiceBinder" */
} // .end annotation
/* # static fields */
public static final java.lang.String SERVICE_NAME;
/* # instance fields */
final com.xiaomi.NetworkBoost.NetworkBoostService this$0; //synthetic
/* # direct methods */
public com.xiaomi.NetworkBoost.NetworkBoostService$SLAServiceBinder ( ) {
/* .locals 0 */
/* .param p1, "this$0" # Lcom/xiaomi/NetworkBoost/NetworkBoostService; */
/* .line 542 */
this.this$0 = p1;
/* invoke-direct {p0}, Lcom/xiaomi/NetworkBoost/slaservice/IAIDLSLAService$Stub;-><init>()V */
return;
} // .end method
/* # virtual methods */
public Boolean addUidToLinkTurboWhiteList ( java.lang.String p0 ) {
/* .locals 2 */
/* .param p1, "uid" # Ljava/lang/String; */
/* .line 547 */
int v0 = 0; // const/4 v0, 0x0
/* .line 548 */
/* .local v0, "ret":Z */
v1 = this.this$0;
com.xiaomi.NetworkBoost.NetworkBoostService .-$$Nest$fgetmSLAService ( v1 );
if ( v1 != null) { // if-eqz v1, :cond_0
	 /* .line 549 */
	 v1 = this.this$0;
	 com.xiaomi.NetworkBoost.NetworkBoostService .-$$Nest$fgetmSLAService ( v1 );
	 v0 = 	 (( com.xiaomi.NetworkBoost.slaservice.SLAService ) v1 ).addUidToLinkTurboWhiteList ( p1 ); // invoke-virtual {v1, p1}, Lcom/xiaomi/NetworkBoost/slaservice/SLAService;->addUidToLinkTurboWhiteList(Ljava/lang/String;)Z
	 /* .line 551 */
} // :cond_0
} // .end method
public java.util.List getLinkTurboAppsTraffic ( ) {
/* .locals 2 */
/* .annotation system Ldalvik/annotation/Signature; */
/* value = { */
/* "()", */
/* "Ljava/util/List<", */
/* "Lcom/xiaomi/NetworkBoost/slaservice/SLAApp;", */
/* ">;" */
/* } */
} // .end annotation
/* .line 574 */
int v0 = 0; // const/4 v0, 0x0
/* .line 575 */
/* .local v0, "tmpLists":Ljava/util/List;, "Ljava/util/List<Lcom/xiaomi/NetworkBoost/slaservice/SLAApp;>;" */
v1 = this.this$0;
com.xiaomi.NetworkBoost.NetworkBoostService .-$$Nest$fgetmSLAService ( v1 );
if ( v1 != null) { // if-eqz v1, :cond_0
/* .line 576 */
v1 = this.this$0;
com.xiaomi.NetworkBoost.NetworkBoostService .-$$Nest$fgetmSLAService ( v1 );
(( com.xiaomi.NetworkBoost.slaservice.SLAService ) v1 ).getLinkTurboAppsTraffic ( ); // invoke-virtual {v1}, Lcom/xiaomi/NetworkBoost/slaservice/SLAService;->getLinkTurboAppsTraffic()Ljava/util/List;
/* .line 578 */
} // :cond_0
} // .end method
public java.util.List getLinkTurboDefaultPn ( ) {
/* .locals 2 */
/* .annotation system Ldalvik/annotation/Signature; */
/* value = { */
/* "()", */
/* "Ljava/util/List<", */
/* "Ljava/lang/String;", */
/* ">;" */
/* } */
} // .end annotation
/* .line 592 */
int v0 = 0; // const/4 v0, 0x0
/* .line 593 */
/* .local v0, "ret":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;" */
v1 = this.this$0;
com.xiaomi.NetworkBoost.NetworkBoostService .-$$Nest$fgetmSLAService ( v1 );
if ( v1 != null) { // if-eqz v1, :cond_0
/* .line 594 */
v1 = this.this$0;
com.xiaomi.NetworkBoost.NetworkBoostService .-$$Nest$fgetmSLAService ( v1 );
(( com.xiaomi.NetworkBoost.slaservice.SLAService ) v1 ).getLinkTurboDefaultPn ( ); // invoke-virtual {v1}, Lcom/xiaomi/NetworkBoost/slaservice/SLAService;->getLinkTurboDefaultPn()Ljava/util/List;
/* .line 596 */
} // :cond_0
} // .end method
public java.lang.String getLinkTurboWhiteList ( ) {
/* .locals 2 */
/* .line 565 */
int v0 = 0; // const/4 v0, 0x0
/* .line 566 */
/* .local v0, "tmpLists":Ljava/lang/String; */
v1 = this.this$0;
com.xiaomi.NetworkBoost.NetworkBoostService .-$$Nest$fgetmSLAService ( v1 );
if ( v1 != null) { // if-eqz v1, :cond_0
/* .line 567 */
v1 = this.this$0;
com.xiaomi.NetworkBoost.NetworkBoostService .-$$Nest$fgetmSLAService ( v1 );
(( com.xiaomi.NetworkBoost.slaservice.SLAService ) v1 ).getLinkTurboWhiteList ( ); // invoke-virtual {v1}, Lcom/xiaomi/NetworkBoost/slaservice/SLAService;->getLinkTurboWhiteList()Ljava/lang/String;
/* .line 569 */
} // :cond_0
} // .end method
public Boolean removeUidInLinkTurboWhiteList ( java.lang.String p0 ) {
/* .locals 2 */
/* .param p1, "uid" # Ljava/lang/String; */
/* .line 556 */
int v0 = 0; // const/4 v0, 0x0
/* .line 557 */
/* .local v0, "ret":Z */
v1 = this.this$0;
com.xiaomi.NetworkBoost.NetworkBoostService .-$$Nest$fgetmSLAService ( v1 );
if ( v1 != null) { // if-eqz v1, :cond_0
/* .line 558 */
v1 = this.this$0;
com.xiaomi.NetworkBoost.NetworkBoostService .-$$Nest$fgetmSLAService ( v1 );
v0 = (( com.xiaomi.NetworkBoost.slaservice.SLAService ) v1 ).removeUidInLinkTurboWhiteList ( p1 ); // invoke-virtual {v1, p1}, Lcom/xiaomi/NetworkBoost/slaservice/SLAService;->removeUidInLinkTurboWhiteList(Ljava/lang/String;)Z
/* .line 560 */
} // :cond_0
} // .end method
public Boolean setLinkTurboEnable ( Boolean p0 ) {
/* .locals 2 */
/* .param p1, "enable" # Z */
/* .line 583 */
int v0 = 0; // const/4 v0, 0x0
/* .line 584 */
/* .local v0, "ret":Z */
v1 = this.this$0;
com.xiaomi.NetworkBoost.NetworkBoostService .-$$Nest$fgetmSLAService ( v1 );
if ( v1 != null) { // if-eqz v1, :cond_0
/* .line 585 */
v1 = this.this$0;
com.xiaomi.NetworkBoost.NetworkBoostService .-$$Nest$fgetmSLAService ( v1 );
v0 = (( com.xiaomi.NetworkBoost.slaservice.SLAService ) v1 ).setLinkTurboEnable ( p1 ); // invoke-virtual {v1, p1}, Lcom/xiaomi/NetworkBoost/slaservice/SLAService;->setLinkTurboEnable(Z)Z
/* .line 587 */
} // :cond_0
} // .end method
