class com.xiaomi.NetworkBoost.StatusManager$InternalHandler extends android.os.Handler {
	 /* .source "StatusManager.java" */
	 /* # annotations */
	 /* .annotation system Ldalvik/annotation/EnclosingClass; */
	 /* value = Lcom/xiaomi/NetworkBoost/StatusManager; */
} // .end annotation
/* .annotation system Ldalvik/annotation/InnerClass; */
/* accessFlags = 0x2 */
/* name = "InternalHandler" */
} // .end annotation
/* # instance fields */
final com.xiaomi.NetworkBoost.StatusManager this$0; //synthetic
/* # direct methods */
public com.xiaomi.NetworkBoost.StatusManager$InternalHandler ( ) {
/* .locals 0 */
/* .param p2, "looper" # Landroid/os/Looper; */
/* .line 516 */
this.this$0 = p1;
/* .line 517 */
/* invoke-direct {p0, p2}, Landroid/os/Handler;-><init>(Landroid/os/Looper;)V */
/* .line 518 */
return;
} // .end method
/* # virtual methods */
public void handleMessage ( android.os.Message p0 ) {
/* .locals 10 */
/* .param p1, "msg" # Landroid/os/Message; */
/* .line 521 */
final String v0 = "NetworkBoostStatusManager"; // const-string v0, "NetworkBoostStatusManager"
/* new-instance v1, Ljava/lang/StringBuilder; */
/* invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V */
final String v2 = "handle: "; // const-string v2, "handle: "
(( java.lang.StringBuilder ) v1 ).append ( v2 ); // invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
/* iget v2, p1, Landroid/os/Message;->what:I */
(( java.lang.StringBuilder ) v1 ).append ( v2 ); // invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v1 ).toString ( ); // invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
android.util.Log .i ( v0,v1 );
/* .line 522 */
/* iget v0, p1, Landroid/os/Message;->what:I */
/* packed-switch v0, :pswitch_data_0 */
/* goto/16 :goto_2 */
/* .line 595 */
/* :pswitch_0 */
v0 = this.this$0;
com.xiaomi.NetworkBoost.StatusManager .-$$Nest$fgetmVPNListener ( v0 );
/* monitor-enter v0 */
/* .line 596 */
try { // :try_start_0
	 final String v1 = "NetworkBoostStatusManager"; // const-string v1, "NetworkBoostStatusManager"
	 final String v2 = "CALLBACK_VPN_STATUS enter"; // const-string v2, "CALLBACK_VPN_STATUS enter"
	 android.util.Log .i ( v1,v2 );
	 /* .line 597 */
	 v1 = this.obj;
	 /* check-cast v1, Lcom/xiaomi/NetworkBoost/StatusManager$IVPNListener; */
	 /* .line 598 */
	 /* .local v1, "listener":Lcom/xiaomi/NetworkBoost/StatusManager$IVPNListener; */
	 v2 = this.this$0;
	 v2 = 	 com.xiaomi.NetworkBoost.StatusManager .-$$Nest$fgetmVPNstatus ( v2 );
	 /* .line 599 */
} // .end local v1 # "listener":Lcom/xiaomi/NetworkBoost/StatusManager$IVPNListener;
/* monitor-exit v0 */
/* .line 600 */
/* goto/16 :goto_2 */
/* .line 599 */
/* :catchall_0 */
/* move-exception v1 */
/* monitor-exit v0 */
/* :try_end_0 */
/* .catchall {:try_start_0 ..:try_end_0} :catchall_0 */
/* throw v1 */
/* .line 588 */
/* :pswitch_1 */
v0 = this.this$0;
com.xiaomi.NetworkBoost.StatusManager .-$$Nest$fgetmDefaultNetworkInterfaceListenerList ( v0 );
/* monitor-enter v0 */
/* .line 589 */
try { // :try_start_1
	 final String v1 = "NetworkBoostStatusManager"; // const-string v1, "NetworkBoostStatusManager"
	 final String v2 = "CALLBACK_NETWORK_PRIORITY_ADD enter"; // const-string v2, "CALLBACK_NETWORK_PRIORITY_ADD enter"
	 android.util.Log .i ( v1,v2 );
	 /* .line 590 */
	 v1 = this.obj;
	 /* check-cast v1, Lcom/xiaomi/NetworkBoost/StatusManager$IDefaultNetworkInterfaceListener; */
	 /* .line 591 */
	 /* .local v1, "listener":Lcom/xiaomi/NetworkBoost/StatusManager$IDefaultNetworkInterfaceListener; */
	 v2 = this.this$0;
	 com.xiaomi.NetworkBoost.StatusManager .-$$Nest$fgetmDefaultIface ( v2 );
	 v3 = this.this$0;
	 v3 = 	 com.xiaomi.NetworkBoost.StatusManager .-$$Nest$fgetmDefaultIfaceStatus ( v3 );
	 /* .line 592 */
} // .end local v1 # "listener":Lcom/xiaomi/NetworkBoost/StatusManager$IDefaultNetworkInterfaceListener;
/* monitor-exit v0 */
/* .line 593 */
/* goto/16 :goto_2 */
/* .line 592 */
/* :catchall_1 */
/* move-exception v1 */
/* monitor-exit v0 */
/* :try_end_1 */
/* .catchall {:try_start_1 ..:try_end_1} :catchall_1 */
/* throw v1 */
/* .line 563 */
/* :pswitch_2 */
v0 = this.this$0;
com.xiaomi.NetworkBoost.StatusManager .-$$Nest$fgetmNetworkPriorityListenerList ( v0 );
/* monitor-enter v0 */
/* .line 564 */
try { // :try_start_2
	 final String v1 = "NetworkBoostStatusManager"; // const-string v1, "NetworkBoostStatusManager"
	 final String v2 = "CALLBACK_NETWORK_PRIORITY_ADD enter"; // const-string v2, "CALLBACK_NETWORK_PRIORITY_ADD enter"
	 android.util.Log .i ( v1,v2 );
	 /* .line 565 */
	 v1 = this.obj;
	 /* check-cast v1, Lcom/xiaomi/NetworkBoost/StatusManager$INetworkPriorityListener; */
	 /* .line 566 */
	 /* .local v1, "networkListener":Lcom/xiaomi/NetworkBoost/StatusManager$INetworkPriorityListener; */
	 v2 = this.this$0;
	 v2 = 	 com.xiaomi.NetworkBoost.StatusManager .-$$Nest$fgetmPriorityMode ( v2 );
	 v3 = this.this$0;
	 v3 = 	 com.xiaomi.NetworkBoost.StatusManager .-$$Nest$fgetmTrafficPolicy ( v3 );
	 v4 = this.this$0;
	 v4 = 	 com.xiaomi.NetworkBoost.StatusManager .-$$Nest$fgetmThermalLevel ( v4 );
	 /* .line 567 */
} // .end local v1 # "networkListener":Lcom/xiaomi/NetworkBoost/StatusManager$INetworkPriorityListener;
/* monitor-exit v0 */
/* .line 568 */
/* goto/16 :goto_2 */
/* .line 567 */
/* :catchall_2 */
/* move-exception v1 */
/* monitor-exit v0 */
/* :try_end_2 */
/* .catchall {:try_start_2 ..:try_end_2} :catchall_2 */
/* throw v1 */
/* .line 560 */
/* :pswitch_3 */
v0 = this.this$0;
v1 = this.obj;
/* check-cast v1, Lcom/xiaomi/NetworkBoost/StatusManager$NetworkPriorityInfo; */
com.xiaomi.NetworkBoost.StatusManager .-$$Nest$monNetworkPriorityChanged ( v0,v1 );
/* .line 561 */
/* goto/16 :goto_2 */
/* .line 546 */
/* :pswitch_4 */
v0 = this.this$0;
com.xiaomi.NetworkBoost.StatusManager .-$$Nest$fgetmScreenStatusListenerList ( v0 );
/* monitor-enter v0 */
/* .line 547 */
try { // :try_start_3
	 final String v1 = "NetworkBoostStatusManager"; // const-string v1, "NetworkBoostStatusManager"
	 final String v2 = "CALLBACK_SCREEN_STATUS enter"; // const-string v2, "CALLBACK_SCREEN_STATUS enter"
	 android.util.Log .i ( v1,v2 );
	 /* .line 548 */
	 v1 = this.obj;
	 /* check-cast v1, Lcom/xiaomi/NetworkBoost/StatusManager$IScreenStatusListener; */
	 /* .line 549 */
	 /* .local v1, "screenListener":Lcom/xiaomi/NetworkBoost/StatusManager$IScreenStatusListener; */
	 v2 = this.this$0;
	 com.xiaomi.NetworkBoost.StatusManager .-$$Nest$fgetmPowerManager ( v2 );
	 if ( v2 != null) { // if-eqz v2, :cond_1
		 /* .line 550 */
		 v2 = this.this$0;
		 com.xiaomi.NetworkBoost.StatusManager .-$$Nest$fgetmPowerManager ( v2 );
		 v2 = 		 (( android.os.PowerManager ) v2 ).isScreenOn ( ); // invoke-virtual {v2}, Landroid/os/PowerManager;->isScreenOn()Z
		 /* .line 551 */
		 /* .local v2, "isScreenOn":Z */
		 if ( v2 != null) { // if-eqz v2, :cond_0
			 /* .line 552 */
			 /* .line 554 */
		 } // :cond_0
		 /* .line 557 */
	 } // .end local v1 # "screenListener":Lcom/xiaomi/NetworkBoost/StatusManager$IScreenStatusListener;
} // .end local v2 # "isScreenOn":Z
} // :cond_1
} // :goto_0
/* monitor-exit v0 */
/* .line 558 */
/* goto/16 :goto_2 */
/* .line 557 */
/* :catchall_3 */
/* move-exception v1 */
/* monitor-exit v0 */
/* :try_end_3 */
/* .catchall {:try_start_3 ..:try_end_3} :catchall_3 */
/* throw v1 */
/* .line 538 */
/* :pswitch_5 */
v0 = this.this$0;
com.xiaomi.NetworkBoost.StatusManager .-$$Nest$fgetmNetworkInterfaceListenerList ( v0 );
/* monitor-enter v0 */
/* .line 539 */
try { // :try_start_4
final String v1 = "NetworkBoostStatusManager"; // const-string v1, "NetworkBoostStatusManager"
final String v2 = "CALLBACK_NETWORK_INTERFACE enter"; // const-string v2, "CALLBACK_NETWORK_INTERFACE enter"
android.util.Log .i ( v1,v2 );
/* .line 540 */
v1 = this.obj;
/* move-object v2, v1 */
/* check-cast v2, Lcom/xiaomi/NetworkBoost/StatusManager$INetworkInterfaceListener; */
/* .line 541 */
/* .local v2, "networkListener":Lcom/xiaomi/NetworkBoost/StatusManager$INetworkInterfaceListener; */
v1 = this.this$0;
com.xiaomi.NetworkBoost.StatusManager .-$$Nest$fgetmInterface ( v1 );
v1 = this.this$0;
v4 = com.xiaomi.NetworkBoost.StatusManager .-$$Nest$fgetmIfaceNumber ( v1 );
v1 = this.this$0;
v5 = com.xiaomi.NetworkBoost.StatusManager .-$$Nest$fgetmWifiReady ( v1 );
v1 = this.this$0;
v6 = com.xiaomi.NetworkBoost.StatusManager .-$$Nest$fgetmSlaveWifiReady ( v1 );
v1 = this.this$0;
v7 = com.xiaomi.NetworkBoost.StatusManager .-$$Nest$fgetmDataReady ( v1 );
v1 = this.this$0;
v8 = com.xiaomi.NetworkBoost.StatusManager .-$$Nest$fgetmSlaveDataReady ( v1 );
final String v9 = "ON_AVAILABLE"; // const-string v9, "ON_AVAILABLE"
/* invoke-interface/range {v2 ..v9}, Lcom/xiaomi/NetworkBoost/StatusManager$INetworkInterfaceListener;->onNetwrokInterfaceChange(Ljava/lang/String;IZZZZLjava/lang/String;)V */
/* .line 543 */
} // .end local v2 # "networkListener":Lcom/xiaomi/NetworkBoost/StatusManager$INetworkInterfaceListener;
/* monitor-exit v0 */
/* .line 544 */
/* goto/16 :goto_2 */
/* .line 543 */
/* :catchall_4 */
/* move-exception v1 */
/* monitor-exit v0 */
/* :try_end_4 */
/* .catchall {:try_start_4 ..:try_end_4} :catchall_4 */
/* throw v1 */
/* .line 602 */
/* :pswitch_6 */
v0 = this.this$0;
com.xiaomi.NetworkBoost.StatusManager .-$$Nest$fgetmPowerManager ( v0 );
if ( v0 != null) { // if-eqz v0, :cond_4
/* .line 603 */
v0 = this.this$0;
com.xiaomi.NetworkBoost.StatusManager .-$$Nest$fgetmPowerManager ( v0 );
v0 = (( android.os.PowerManager ) v0 ).isScreenOn ( ); // invoke-virtual {v0}, Landroid/os/PowerManager;->isScreenOn()Z
/* .line 604 */
/* .local v0, "isScreenOn":Z */
if ( v0 != null) { // if-eqz v0, :cond_2
v1 = this.this$0;
v1 = com.xiaomi.NetworkBoost.StatusManager .-$$Nest$fgetmIfaceNumber ( v1 );
/* if-lez v1, :cond_2 */
/* .line 605 */
v1 = this.this$0;
com.xiaomi.NetworkBoost.StatusManager .-$$Nest$mregisterMovementSensorListener ( v1 );
/* .line 607 */
} // :cond_2
v1 = this.this$0;
com.xiaomi.NetworkBoost.StatusManager .-$$Nest$munregisterMovementSensorListener ( v1 );
/* .line 609 */
} // .end local v0 # "isScreenOn":Z
} // :goto_1
/* .line 578 */
/* :pswitch_7 */
v0 = this.this$0;
com.xiaomi.NetworkBoost.StatusManager .-$$Nest$fgetmPowerManager ( v0 );
if ( v0 != null) { // if-eqz v0, :cond_3
/* .line 579 */
v0 = this.this$0;
com.xiaomi.NetworkBoost.StatusManager .-$$Nest$fgetmPowerManager ( v0 );
v0 = (( android.os.PowerManager ) v0 ).isScreenOn ( ); // invoke-virtual {v0}, Landroid/os/PowerManager;->isScreenOn()Z
/* .line 580 */
/* .restart local v0 # "isScreenOn":Z */
if ( v0 != null) { // if-eqz v0, :cond_3
/* .line 581 */
v1 = this.this$0;
com.xiaomi.NetworkBoost.StatusManager .-$$Nest$misModemSingnalStrengthStatus ( v1 );
/* .line 584 */
} // .end local v0 # "isScreenOn":Z
} // :cond_3
v0 = this.this$0;
com.xiaomi.NetworkBoost.StatusManager .-$$Nest$fgetmHandler ( v0 );
v1 = this.this$0;
com.xiaomi.NetworkBoost.StatusManager .-$$Nest$fgetmHandler ( v1 );
/* const/16 v2, 0x69 */
(( android.os.Handler ) v1 ).obtainMessage ( v2 ); // invoke-virtual {v1, v2}, Landroid/os/Handler;->obtainMessage(I)Landroid/os/Message;
/* const-wide/16 v2, 0x7530 */
(( android.os.Handler ) v0 ).sendMessageDelayed ( v1, v2, v3 ); // invoke-virtual {v0, v1, v2, v3}, Landroid/os/Handler;->sendMessageDelayed(Landroid/os/Message;J)Z
/* .line 586 */
/* .line 575 */
/* :pswitch_8 */
v0 = this.this$0;
v1 = this.obj;
/* check-cast v1, Landroid/content/Intent; */
com.xiaomi.NetworkBoost.StatusManager .-$$Nest$mprocessSimStateChanged ( v0,v1 );
/* .line 576 */
/* .line 571 */
/* :pswitch_9 */
final String v0 = "NetworkBoostStatusManager"; // const-string v0, "NetworkBoostStatusManager"
final String v1 = "receive EVENT_DELAY_SET_CELLULAR_CALLBACK"; // const-string v1, "receive EVENT_DELAY_SET_CELLULAR_CALLBACK"
android.util.Log .d ( v0,v1 );
/* .line 572 */
v0 = this.this$0;
com.xiaomi.NetworkBoost.StatusManager .-$$Nest$mprocessRegisterCellularCallback ( v0 );
/* .line 573 */
/* .line 532 */
/* :pswitch_a */
v0 = this.obj;
/* check-cast v0, Landroid/net/Network; */
/* .line 533 */
/* .local v0, "defaultNetwork":Landroid/net/Network; */
v1 = this.this$0;
v2 = (( android.net.Network ) v0 ).getNetId ( ); // invoke-virtual {v0}, Landroid/net/Network;->getNetId()I
com.xiaomi.NetworkBoost.StatusManager .-$$Nest$fputmDefaultNetid ( v1,v2 );
/* .line 534 */
v1 = this.this$0;
com.xiaomi.NetworkBoost.StatusManager .-$$Nest$mcallbackDefaultNetworkInterface ( v1 );
/* .line 535 */
/* .line 528 */
} // .end local v0 # "defaultNetwork":Landroid/net/Network;
/* :pswitch_b */
v0 = this.obj;
/* check-cast v0, Landroid/net/Network; */
/* .line 529 */
/* .local v0, "onLostNetwork":Landroid/net/Network; */
v1 = this.this$0;
com.xiaomi.NetworkBoost.StatusManager .-$$Nest$minterfaceOnLost ( v1,v0 );
/* .line 530 */
/* .line 524 */
} // .end local v0 # "onLostNetwork":Landroid/net/Network;
/* :pswitch_c */
v0 = this.obj;
/* check-cast v0, Landroid/net/Network; */
/* .line 525 */
/* .local v0, "onAvailableNetwork":Landroid/net/Network; */
v1 = this.this$0;
com.xiaomi.NetworkBoost.StatusManager .-$$Nest$minterfaceOnAvailable ( v1,v0 );
/* .line 526 */
/* nop */
/* .line 614 */
} // .end local v0 # "onAvailableNetwork":Landroid/net/Network;
} // :cond_4
} // :goto_2
return;
/* :pswitch_data_0 */
/* .packed-switch 0x64 */
/* :pswitch_c */
/* :pswitch_b */
/* :pswitch_a */
/* :pswitch_9 */
/* :pswitch_8 */
/* :pswitch_7 */
/* :pswitch_6 */
/* :pswitch_5 */
/* :pswitch_4 */
/* :pswitch_3 */
/* :pswitch_2 */
/* :pswitch_1 */
/* :pswitch_0 */
} // .end packed-switch
} // .end method
