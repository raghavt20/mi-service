class com.xiaomi.NetworkBoost.StatusManager$7 extends android.net.ConnectivityManager$NetworkCallback {
	 /* .source "StatusManager.java" */
	 /* # annotations */
	 /* .annotation system Ldalvik/annotation/EnclosingClass; */
	 /* value = Lcom/xiaomi/NetworkBoost/StatusManager; */
} // .end annotation
/* .annotation system Ldalvik/annotation/InnerClass; */
/* accessFlags = 0x0 */
/* name = null */
} // .end annotation
/* # instance fields */
final com.xiaomi.NetworkBoost.StatusManager this$0; //synthetic
/* # direct methods */
 com.xiaomi.NetworkBoost.StatusManager$7 ( ) {
/* .locals 0 */
/* .param p1, "this$0" # Lcom/xiaomi/NetworkBoost/StatusManager; */
/* .line 1089 */
this.this$0 = p1;
/* invoke-direct {p0}, Landroid/net/ConnectivityManager$NetworkCallback;-><init>()V */
return;
} // .end method
/* # virtual methods */
public void onAvailable ( android.net.Network p0 ) {
/* .locals 4 */
/* .param p1, "network" # Landroid/net/Network; */
/* .line 1092 */
final String v0 = "NetworkBoostStatusManager"; // const-string v0, "NetworkBoostStatusManager"
final String v1 = "VPN Callback onAvailable"; // const-string v1, "VPN Callback onAvailable"
android.util.Log .i ( v0,v1 );
/* .line 1093 */
v0 = this.this$0;
com.xiaomi.NetworkBoost.StatusManager .-$$Nest$fgetmVPNListener ( v0 );
/* monitor-enter v0 */
/* .line 1094 */
try { // :try_start_0
	 v1 = this.this$0;
	 int v2 = 1; // const/4 v2, 0x1
	 com.xiaomi.NetworkBoost.StatusManager .-$$Nest$fputmVPNstatus ( v1,v2 );
	 /* .line 1095 */
	 v1 = this.this$0;
	 com.xiaomi.NetworkBoost.StatusManager .-$$Nest$fgetmVPNListener ( v1 );
v2 = } // :goto_0
if ( v2 != null) { // if-eqz v2, :cond_0
	 /* check-cast v2, Lcom/xiaomi/NetworkBoost/StatusManager$IVPNListener; */
	 /* .line 1096 */
	 /* .local v2, "listener":Lcom/xiaomi/NetworkBoost/StatusManager$IVPNListener; */
	 v3 = this.this$0;
	 v3 = 	 com.xiaomi.NetworkBoost.StatusManager .-$$Nest$fgetmVPNstatus ( v3 );
	 /* .line 1097 */
} // .end local v2 # "listener":Lcom/xiaomi/NetworkBoost/StatusManager$IVPNListener;
/* .line 1098 */
} // :cond_0
/* monitor-exit v0 */
/* .line 1099 */
return;
/* .line 1098 */
/* :catchall_0 */
/* move-exception v1 */
/* monitor-exit v0 */
/* :try_end_0 */
/* .catchall {:try_start_0 ..:try_end_0} :catchall_0 */
/* throw v1 */
} // .end method
public void onLost ( android.net.Network p0 ) {
/* .locals 4 */
/* .param p1, "network" # Landroid/net/Network; */
/* .line 1103 */
final String v0 = "NetworkBoostStatusManager"; // const-string v0, "NetworkBoostStatusManager"
final String v1 = "VPN Callback onLost"; // const-string v1, "VPN Callback onLost"
android.util.Log .i ( v0,v1 );
/* .line 1104 */
v0 = this.this$0;
com.xiaomi.NetworkBoost.StatusManager .-$$Nest$fgetmVPNListener ( v0 );
/* monitor-enter v0 */
/* .line 1105 */
try { // :try_start_0
v1 = this.this$0;
int v2 = 0; // const/4 v2, 0x0
com.xiaomi.NetworkBoost.StatusManager .-$$Nest$fputmVPNstatus ( v1,v2 );
/* .line 1106 */
v1 = this.this$0;
com.xiaomi.NetworkBoost.StatusManager .-$$Nest$fgetmVPNListener ( v1 );
v2 = } // :goto_0
if ( v2 != null) { // if-eqz v2, :cond_0
/* check-cast v2, Lcom/xiaomi/NetworkBoost/StatusManager$IVPNListener; */
/* .line 1107 */
/* .local v2, "listener":Lcom/xiaomi/NetworkBoost/StatusManager$IVPNListener; */
v3 = this.this$0;
v3 = com.xiaomi.NetworkBoost.StatusManager .-$$Nest$fgetmVPNstatus ( v3 );
/* .line 1108 */
} // .end local v2 # "listener":Lcom/xiaomi/NetworkBoost/StatusManager$IVPNListener;
/* .line 1109 */
} // :cond_0
/* monitor-exit v0 */
/* .line 1110 */
return;
/* .line 1109 */
/* :catchall_0 */
/* move-exception v1 */
/* monitor-exit v0 */
/* :try_end_0 */
/* .catchall {:try_start_0 ..:try_end_0} :catchall_0 */
/* throw v1 */
} // .end method
