public abstract class com.xiaomi.NetworkBoost.IAIDLMiuiWlanQoECallback$Stub extends android.os.Binder implements com.xiaomi.NetworkBoost.IAIDLMiuiWlanQoECallback {
	 /* .source "IAIDLMiuiWlanQoECallback.java" */
	 /* # interfaces */
	 /* # annotations */
	 /* .annotation system Ldalvik/annotation/EnclosingClass; */
	 /* value = Lcom/xiaomi/NetworkBoost/IAIDLMiuiWlanQoECallback; */
} // .end annotation
/* .annotation system Ldalvik/annotation/InnerClass; */
/* accessFlags = 0x409 */
/* name = "Stub" */
} // .end annotation
/* .annotation system Ldalvik/annotation/MemberClasses; */
/* value = { */
/* Lcom/xiaomi/NetworkBoost/IAIDLMiuiWlanQoECallback$Stub$a; */
/* } */
} // .end annotation
/* # direct methods */
public com.xiaomi.NetworkBoost.IAIDLMiuiWlanQoECallback$Stub ( ) {
/* .locals 1 */
/* .line 1 */
/* invoke-direct {p0}, Landroid/os/Binder;-><init>()V */
/* .line 2 */
final String v0 = "com.xiaomi.NetworkBoost.IAIDLMiuiWlanQoECallback"; // const-string v0, "com.xiaomi.NetworkBoost.IAIDLMiuiWlanQoECallback"
(( android.os.Binder ) p0 ).attachInterface ( p0, v0 ); // invoke-virtual {p0, p0, v0}, Landroid/os/Binder;->attachInterface(Landroid/os/IInterface;Ljava/lang/String;)V
return;
} // .end method
public static void a ( android.os.Parcel p0, java.util.Map p1, Integer p2 ) { //synthethic
/* .locals 0 */
/* .line 1 */
(( android.os.Parcel ) p0 ).readString ( ); // invoke-virtual {p0}, Landroid/os/Parcel;->readString()Ljava/lang/String;
/* .line 3 */
(( android.os.Parcel ) p0 ).readString ( ); // invoke-virtual {p0}, Landroid/os/Parcel;->readString()Ljava/lang/String;
/* .line 4 */
return;
} // .end method
public static com.xiaomi.NetworkBoost.IAIDLMiuiWlanQoECallback asInterface ( android.os.IBinder p0 ) {
/* .locals 2 */
/* if-nez p0, :cond_0 */
int p0 = 0; // const/4 p0, 0x0
/* .line 1 */
} // :cond_0
final String v0 = "com.xiaomi.NetworkBoost.IAIDLMiuiWlanQoECallback"; // const-string v0, "com.xiaomi.NetworkBoost.IAIDLMiuiWlanQoECallback"
if ( v0 != null) { // if-eqz v0, :cond_1
/* .line 2 */
/* instance-of v1, v0, Lcom/xiaomi/NetworkBoost/IAIDLMiuiWlanQoECallback; */
if ( v1 != null) { // if-eqz v1, :cond_1
/* .line 3 */
/* check-cast v0, Lcom/xiaomi/NetworkBoost/IAIDLMiuiWlanQoECallback; */
/* .line 5 */
} // :cond_1
/* new-instance v0, Lcom/xiaomi/NetworkBoost/IAIDLMiuiWlanQoECallback$Stub$a; */
/* invoke-direct {v0, p0}, Lcom/xiaomi/NetworkBoost/IAIDLMiuiWlanQoECallback$Stub$a;-><init>(Landroid/os/IBinder;)V */
} // .end method
public static void b ( android.os.Parcel p0, java.util.Map p1, Integer p2 ) { //synthethic
/* .locals 0 */
/* .line 1 */
(( android.os.Parcel ) p0 ).readString ( ); // invoke-virtual {p0}, Landroid/os/Parcel;->readString()Ljava/lang/String;
/* .line 3 */
(( android.os.Parcel ) p0 ).readString ( ); // invoke-virtual {p0}, Landroid/os/Parcel;->readString()Ljava/lang/String;
/* .line 4 */
return;
} // .end method
public static com.xiaomi.NetworkBoost.IAIDLMiuiWlanQoECallback getDefaultImpl ( ) {
/* .locals 1 */
/* .line 1 */
v0 = com.xiaomi.NetworkBoost.IAIDLMiuiWlanQoECallback$Stub$a.b;
} // .end method
public static Boolean setDefaultImpl ( com.xiaomi.NetworkBoost.IAIDLMiuiWlanQoECallback p0 ) {
/* .locals 1 */
/* .line 1 */
v0 = com.xiaomi.NetworkBoost.IAIDLMiuiWlanQoECallback$Stub$a.b;
/* if-nez v0, :cond_1 */
if ( p0 != null) { // if-eqz p0, :cond_0
/* .line 5 */
int p0 = 1; // const/4 p0, 0x1
} // :cond_0
int p0 = 0; // const/4 p0, 0x0
/* .line 6 */
} // :cond_1
/* new-instance p0, Ljava/lang/IllegalStateException; */
/* const-string/jumbo v0, "setDefaultImpl() called twice" */
/* invoke-direct {p0, v0}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V */
/* throw p0 */
} // .end method
/* # virtual methods */
public android.os.IBinder asBinder ( ) {
/* .locals 0 */
} // .end method
public Boolean onTransact ( Integer p0, android.os.Parcel p1, android.os.Parcel p2, Integer p3 ) {
/* .locals 5 */
/* .annotation system Ldalvik/annotation/Throws; */
/* value = { */
/* Landroid/os/RemoteException; */
/* } */
} // .end annotation
/* .line 1 */
int v0 = 0; // const/4 v0, 0x0
int v1 = 0; // const/4 v1, 0x0
final String v2 = "com.xiaomi.NetworkBoost.IAIDLMiuiWlanQoECallback"; // const-string v2, "com.xiaomi.NetworkBoost.IAIDLMiuiWlanQoECallback"
int v3 = 1; // const/4 v3, 0x1
/* if-eq p1, v3, :cond_3 */
int v4 = 2; // const/4 v4, 0x2
/* if-eq p1, v4, :cond_1 */
/* const v0, 0x5f4e5446 */
/* if-eq p1, v0, :cond_0 */
/* .line 47 */
p1 = /* invoke-super {p0, p1, p2, p3, p4}, Landroid/os/Binder;->onTransact(ILandroid/os/Parcel;Landroid/os/Parcel;I)Z */
/* .line 48 */
} // :cond_0
(( android.os.Parcel ) p3 ).writeString ( v2 ); // invoke-virtual {p3, v2}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V
/* .line 71 */
} // :cond_1
(( android.os.Parcel ) p2 ).enforceInterface ( v2 ); // invoke-virtual {p2, v2}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V
/* .line 74 */
p1 = (( android.os.Parcel ) p2 ).readInt ( ); // invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I
/* if-gez p1, :cond_2 */
/* .line 75 */
} // :cond_2
/* new-instance v1, Ljava/util/HashMap; */
/* invoke-direct {v1}, Ljava/util/HashMap;-><init>()V */
} // :goto_0
/* nop */
/* .line 76 */
java.util.stream.IntStream .range ( v0,p1 );
/* new-instance p4, Lcom/xiaomi/NetworkBoost/IAIDLMiuiWlanQoECallback$Stub$$ExternalSyntheticLambda0; */
/* invoke-direct {p4, p2, v1}, Lcom/xiaomi/NetworkBoost/IAIDLMiuiWlanQoECallback$Stub$$ExternalSyntheticLambda0;-><init>(Landroid/os/Parcel;Ljava/util/Map;)V */
/* .line 83 */
/* .line 84 */
(( android.os.Parcel ) p3 ).writeNoException ( ); // invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V
/* .line 85 */
} // :cond_3
(( android.os.Parcel ) p2 ).enforceInterface ( v2 ); // invoke-virtual {p2, v2}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V
/* .line 88 */
p1 = (( android.os.Parcel ) p2 ).readInt ( ); // invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I
/* if-gez p1, :cond_4 */
/* .line 89 */
} // :cond_4
/* new-instance v1, Ljava/util/HashMap; */
/* invoke-direct {v1}, Ljava/util/HashMap;-><init>()V */
} // :goto_1
/* nop */
/* .line 90 */
java.util.stream.IntStream .range ( v0,p1 );
/* new-instance p4, Lcom/xiaomi/NetworkBoost/IAIDLMiuiWlanQoECallback$Stub$$ExternalSyntheticLambda1; */
/* invoke-direct {p4, p2, v1}, Lcom/xiaomi/NetworkBoost/IAIDLMiuiWlanQoECallback$Stub$$ExternalSyntheticLambda1;-><init>(Landroid/os/Parcel;Ljava/util/Map;)V */
/* .line 97 */
/* .line 98 */
(( android.os.Parcel ) p3 ).writeNoException ( ); // invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V
} // .end method
