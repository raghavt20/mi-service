class com.xiaomi.NetworkBoost.StatusManager$3 implements android.media.AudioManager$OnModeChangedListener {
	 /* .source "StatusManager.java" */
	 /* # interfaces */
	 /* # annotations */
	 /* .annotation system Ldalvik/annotation/EnclosingMethod; */
	 /* value = Lcom/xiaomi/NetworkBoost/StatusManager;->registerAudioModeChangedListener()V */
} // .end annotation
/* .annotation system Ldalvik/annotation/InnerClass; */
/* accessFlags = 0x0 */
/* name = null */
} // .end annotation
/* # instance fields */
final com.xiaomi.NetworkBoost.StatusManager this$0; //synthetic
/* # direct methods */
 com.xiaomi.NetworkBoost.StatusManager$3 ( ) {
/* .locals 0 */
/* .param p1, "this$0" # Lcom/xiaomi/NetworkBoost/StatusManager; */
/* .line 369 */
this.this$0 = p1;
/* invoke-direct {p0}, Ljava/lang/Object;-><init>()V */
return;
} // .end method
/* # virtual methods */
public void onModeChanged ( Integer p0 ) {
/* .locals 3 */
/* .param p1, "mode" # I */
/* .line 372 */
final String v0 = "NetworkBoostStatusManager"; // const-string v0, "NetworkBoostStatusManager"
/* new-instance v1, Ljava/lang/StringBuilder; */
/* invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V */
final String v2 = "audio Broadcast Receiver mode = "; // const-string v2, "audio Broadcast Receiver mode = "
(( java.lang.StringBuilder ) v1 ).append ( v2 ); // invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v1 ).append ( p1 ); // invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v1 ).toString ( ); // invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
android.util.Log .i ( v0,v1 );
/* .line 373 */
v0 = this.this$0;
com.xiaomi.NetworkBoost.StatusManager .-$$Nest$fgetmAppStatusListenerList ( v0 );
/* monitor-enter v0 */
/* .line 374 */
try { // :try_start_0
	 v1 = this.this$0;
	 com.xiaomi.NetworkBoost.StatusManager .-$$Nest$fgetmAppStatusListenerList ( v1 );
v2 = } // :goto_0
if ( v2 != null) { // if-eqz v2, :cond_0
	 /* check-cast v2, Lcom/xiaomi/NetworkBoost/StatusManager$IAppStatusListener; */
	 /* .line 375 */
	 /* .local v2, "listener":Lcom/xiaomi/NetworkBoost/StatusManager$IAppStatusListener; */
	 /* .line 376 */
} // .end local v2 # "listener":Lcom/xiaomi/NetworkBoost/StatusManager$IAppStatusListener;
/* .line 377 */
} // :cond_0
/* monitor-exit v0 */
/* .line 378 */
return;
/* .line 377 */
/* :catchall_0 */
/* move-exception v1 */
/* monitor-exit v0 */
/* :try_end_0 */
/* .catchall {:try_start_0 ..:try_end_0} :catchall_0 */
/* throw v1 */
} // .end method
