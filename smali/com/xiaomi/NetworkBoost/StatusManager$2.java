class com.xiaomi.NetworkBoost.StatusManager$2 extends android.app.IUidObserver$Stub {
	 /* .source "StatusManager.java" */
	 /* # annotations */
	 /* .annotation system Ldalvik/annotation/EnclosingClass; */
	 /* value = Lcom/xiaomi/NetworkBoost/StatusManager; */
} // .end annotation
/* .annotation system Ldalvik/annotation/InnerClass; */
/* accessFlags = 0x0 */
/* name = null */
} // .end annotation
/* # instance fields */
final com.xiaomi.NetworkBoost.StatusManager this$0; //synthetic
/* # direct methods */
 com.xiaomi.NetworkBoost.StatusManager$2 ( ) {
/* .locals 0 */
/* .param p1, "this$0" # Lcom/xiaomi/NetworkBoost/StatusManager; */
/* .line 337 */
this.this$0 = p1;
/* invoke-direct {p0}, Landroid/app/IUidObserver$Stub;-><init>()V */
return;
} // .end method
/* # virtual methods */
public void onUidActive ( Integer p0 ) {
/* .locals 0 */
/* .param p1, "uid" # I */
/* .annotation system Ldalvik/annotation/Throws; */
/* value = { */
/* Landroid/os/RemoteException; */
/* } */
} // .end annotation
/* .line 352 */
return;
} // .end method
public void onUidCachedChanged ( Integer p0, Boolean p1 ) {
/* .locals 0 */
/* .param p1, "uid" # I */
/* .param p2, "cached" # Z */
/* .line 358 */
return;
} // .end method
public void onUidGone ( Integer p0, Boolean p1 ) {
/* .locals 3 */
/* .param p1, "uid" # I */
/* .param p2, "disabled" # Z */
/* .annotation system Ldalvik/annotation/Throws; */
/* value = { */
/* Landroid/os/RemoteException; */
/* } */
} // .end annotation
/* .line 343 */
final String v0 = "NetworkBoostStatusManager"; // const-string v0, "NetworkBoostStatusManager"
/* new-instance v1, Ljava/lang/StringBuilder; */
/* invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V */
final String v2 = "onUidGone uid: "; // const-string v2, "onUidGone uid: "
(( java.lang.StringBuilder ) v1 ).append ( v2 ); // invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v1 ).append ( p1 ); // invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v1 ).toString ( ); // invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
android.util.Log .i ( v0,v1 );
/* .line 344 */
v0 = this.this$0;
com.xiaomi.NetworkBoost.StatusManager .-$$Nest$fgetmAppStatusListenerList ( v0 );
/* monitor-enter v0 */
/* .line 345 */
try { // :try_start_0
v1 = this.this$0;
com.xiaomi.NetworkBoost.StatusManager .-$$Nest$fgetmAppStatusListenerList ( v1 );
v2 = } // :goto_0
if ( v2 != null) { // if-eqz v2, :cond_0
/* check-cast v2, Lcom/xiaomi/NetworkBoost/StatusManager$IAppStatusListener; */
/* .line 346 */
/* .local v2, "listener":Lcom/xiaomi/NetworkBoost/StatusManager$IAppStatusListener; */
/* .line 347 */
} // .end local v2 # "listener":Lcom/xiaomi/NetworkBoost/StatusManager$IAppStatusListener;
/* .line 348 */
} // :cond_0
/* monitor-exit v0 */
/* .line 349 */
return;
/* .line 348 */
/* :catchall_0 */
/* move-exception v1 */
/* monitor-exit v0 */
/* :try_end_0 */
/* .catchall {:try_start_0 ..:try_end_0} :catchall_0 */
/* throw v1 */
} // .end method
public void onUidIdle ( Integer p0, Boolean p1 ) {
/* .locals 0 */
/* .param p1, "uid" # I */
/* .param p2, "disabled" # Z */
/* .annotation system Ldalvik/annotation/Throws; */
/* value = { */
/* Landroid/os/RemoteException; */
/* } */
} // .end annotation
/* .line 355 */
return;
} // .end method
public void onUidProcAdjChanged ( Integer p0, Integer p1 ) {
/* .locals 0 */
/* .param p1, "uid" # I */
/* .param p2, "adj" # I */
/* .line 361 */
return;
} // .end method
public void onUidStateChanged ( Integer p0, Integer p1, Long p2, Integer p3 ) {
/* .locals 0 */
/* .param p1, "uid" # I */
/* .param p2, "procState" # I */
/* .param p3, "procStateSeq" # J */
/* .param p5, "capability" # I */
/* .annotation system Ldalvik/annotation/Throws; */
/* value = { */
/* Landroid/os/RemoteException; */
/* } */
} // .end annotation
/* .line 340 */
return;
} // .end method
