.class public Lcom/xiaomi/NetworkBoost/NetworkBoostService;
.super Landroid/app/Service;
.source "NetworkBoostService.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/xiaomi/NetworkBoost/NetworkBoostService$NetworkBoostServiceManager;,
        Lcom/xiaomi/NetworkBoost/NetworkBoostService$SLAServiceBinder;
    }
.end annotation


# static fields
.field public static final ACTION_START_SERVICE:Ljava/lang/String; = "ACTION_SLA_JAVA_SERVICE_START"

.field public static final BASE:I = 0x3e8

.field public static final MINETD_CMD_FLOWSTATGET:I = 0x3ed

.field public static final MINETD_CMD_FLOWSTATSTART:I = 0x3eb

.field public static final MINETD_CMD_FLOWSTATSTOP:I = 0x3ec

.field public static final MINETD_CMD_SETSOCKPRIO:I = 0x3e9

.field public static final MINETD_CMD_SETTCPCONGESTION:I = 0x3ea

.field public static final SERVICE_VERSION:I = 0x6

.field private static final TAG:Ljava/lang/String; = "NetworkBoostService"

.field private static mContext:Landroid/content/Context;

.field private static mJniLoaded:Z

.field private static volatile sInstance:Lcom/xiaomi/NetworkBoost/NetworkBoostService;


# instance fields
.field private final mAIDLMiuiNetworkBoost:Lcom/xiaomi/NetworkBoost/NetworkBoostService$NetworkBoostServiceManager;

.field private final mAIDLSLAServiceBinder:Lcom/xiaomi/NetworkBoost/NetworkBoostService$SLAServiceBinder;

.field private final mMiNetCallback:Lvendor/xiaomi/hidl/minet/V1_0/IMiNetCallback$Stub;

.field private mMscsService:Lcom/xiaomi/NetworkBoost/MscsService/MscsService;

.field private mNetworkAccelerateSwitchService:Lcom/xiaomi/NetworkBoost/NetworkAccelerateSwitch/NetworkAccelerateSwitchService;

.field private mNetworkSDKService:Lcom/xiaomi/NetworkBoost/NetworkSDK/NetworkSDKService;

.field private mSLAService:Lcom/xiaomi/NetworkBoost/slaservice/SLAService;

.field private mStatusManager:Lcom/xiaomi/NetworkBoost/StatusManager;


# direct methods
.method static bridge synthetic -$$Nest$fgetmNetworkAccelerateSwitchService(Lcom/xiaomi/NetworkBoost/NetworkBoostService;)Lcom/xiaomi/NetworkBoost/NetworkAccelerateSwitch/NetworkAccelerateSwitchService;
    .locals 0

    iget-object p0, p0, Lcom/xiaomi/NetworkBoost/NetworkBoostService;->mNetworkAccelerateSwitchService:Lcom/xiaomi/NetworkBoost/NetworkAccelerateSwitch/NetworkAccelerateSwitchService;

    return-object p0
.end method

.method static bridge synthetic -$$Nest$fgetmNetworkSDKService(Lcom/xiaomi/NetworkBoost/NetworkBoostService;)Lcom/xiaomi/NetworkBoost/NetworkSDK/NetworkSDKService;
    .locals 0

    iget-object p0, p0, Lcom/xiaomi/NetworkBoost/NetworkBoostService;->mNetworkSDKService:Lcom/xiaomi/NetworkBoost/NetworkSDK/NetworkSDKService;

    return-object p0
.end method

.method static bridge synthetic -$$Nest$fgetmSLAService(Lcom/xiaomi/NetworkBoost/NetworkBoostService;)Lcom/xiaomi/NetworkBoost/slaservice/SLAService;
    .locals 0

    iget-object p0, p0, Lcom/xiaomi/NetworkBoost/NetworkBoostService;->mSLAService:Lcom/xiaomi/NetworkBoost/slaservice/SLAService;

    return-object p0
.end method

.method static bridge synthetic -$$Nest$misSystemProcess(Lcom/xiaomi/NetworkBoost/NetworkBoostService;)Z
    .locals 0

    invoke-direct {p0}, Lcom/xiaomi/NetworkBoost/NetworkBoostService;->isSystemProcess()Z

    move-result p0

    return p0
.end method

.method static bridge synthetic -$$Nest$sfgetmContext()Landroid/content/Context;
    .locals 1

    sget-object v0, Lcom/xiaomi/NetworkBoost/NetworkBoostService;->mContext:Landroid/content/Context;

    return-object v0
.end method

.method static constructor <clinit>()V
    .locals 1

    .line 50
    const/4 v0, 0x0

    sput-boolean v0, Lcom/xiaomi/NetworkBoost/NetworkBoostService;->mJniLoaded:Z

    .line 57
    const/4 v0, 0x0

    sput-object v0, Lcom/xiaomi/NetworkBoost/NetworkBoostService;->sInstance:Lcom/xiaomi/NetworkBoost/NetworkBoostService;

    return-void
.end method

.method private constructor <init>(Landroid/content/Context;)V
    .locals 1
    .param p1, "context"    # Landroid/content/Context;

    .line 679
    invoke-direct {p0}, Landroid/app/Service;-><init>()V

    .line 60
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/xiaomi/NetworkBoost/NetworkBoostService;->mSLAService:Lcom/xiaomi/NetworkBoost/slaservice/SLAService;

    .line 63
    iput-object v0, p0, Lcom/xiaomi/NetworkBoost/NetworkBoostService;->mNetworkSDKService:Lcom/xiaomi/NetworkBoost/NetworkSDK/NetworkSDKService;

    .line 66
    iput-object v0, p0, Lcom/xiaomi/NetworkBoost/NetworkBoostService;->mStatusManager:Lcom/xiaomi/NetworkBoost/StatusManager;

    .line 69
    iput-object v0, p0, Lcom/xiaomi/NetworkBoost/NetworkBoostService;->mNetworkAccelerateSwitchService:Lcom/xiaomi/NetworkBoost/NetworkAccelerateSwitch/NetworkAccelerateSwitchService;

    .line 72
    iput-object v0, p0, Lcom/xiaomi/NetworkBoost/NetworkBoostService;->mMscsService:Lcom/xiaomi/NetworkBoost/MscsService/MscsService;

    .line 86
    new-instance v0, Lcom/xiaomi/NetworkBoost/NetworkBoostService$NetworkBoostServiceManager;

    invoke-direct {v0, p0}, Lcom/xiaomi/NetworkBoost/NetworkBoostService$NetworkBoostServiceManager;-><init>(Lcom/xiaomi/NetworkBoost/NetworkBoostService;)V

    iput-object v0, p0, Lcom/xiaomi/NetworkBoost/NetworkBoostService;->mAIDLMiuiNetworkBoost:Lcom/xiaomi/NetworkBoost/NetworkBoostService$NetworkBoostServiceManager;

    .line 541
    new-instance v0, Lcom/xiaomi/NetworkBoost/NetworkBoostService$SLAServiceBinder;

    invoke-direct {v0, p0}, Lcom/xiaomi/NetworkBoost/NetworkBoostService$SLAServiceBinder;-><init>(Lcom/xiaomi/NetworkBoost/NetworkBoostService;)V

    iput-object v0, p0, Lcom/xiaomi/NetworkBoost/NetworkBoostService;->mAIDLSLAServiceBinder:Lcom/xiaomi/NetworkBoost/NetworkBoostService$SLAServiceBinder;

    .line 601
    new-instance v0, Lcom/xiaomi/NetworkBoost/NetworkBoostService$1;

    invoke-direct {v0, p0}, Lcom/xiaomi/NetworkBoost/NetworkBoostService$1;-><init>(Lcom/xiaomi/NetworkBoost/NetworkBoostService;)V

    iput-object v0, p0, Lcom/xiaomi/NetworkBoost/NetworkBoostService;->mMiNetCallback:Lvendor/xiaomi/hidl/minet/V1_0/IMiNetCallback$Stub;

    .line 680
    sput-object p1, Lcom/xiaomi/NetworkBoost/NetworkBoostService;->mContext:Landroid/content/Context;

    .line 681
    return-void
.end method

.method public static destroyInstance()V
    .locals 4

    .line 643
    sget-object v0, Lcom/xiaomi/NetworkBoost/NetworkBoostService;->sInstance:Lcom/xiaomi/NetworkBoost/NetworkBoostService;

    if-eqz v0, :cond_1

    .line 644
    const-class v0, Lcom/xiaomi/NetworkBoost/NetworkBoostService;

    monitor-enter v0

    .line 645
    :try_start_0
    sget-object v1, Lcom/xiaomi/NetworkBoost/NetworkBoostService;->sInstance:Lcom/xiaomi/NetworkBoost/NetworkBoostService;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    if-eqz v1, :cond_0

    .line 647
    :try_start_1
    sget-object v1, Lcom/xiaomi/NetworkBoost/NetworkBoostService;->sInstance:Lcom/xiaomi/NetworkBoost/NetworkBoostService;

    invoke-virtual {v1}, Lcom/xiaomi/NetworkBoost/NetworkBoostService;->onDestroy()V
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_0
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 651
    goto :goto_0

    .line 649
    :catch_0
    move-exception v1

    .line 650
    .local v1, "e":Ljava/lang/Exception;
    :try_start_2
    const-string v2, "NetworkBoostService"

    const-string v3, "destroyInstance onDestroy catch:"

    invoke-static {v2, v3, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    .line 652
    .end local v1    # "e":Ljava/lang/Exception;
    :goto_0
    const/4 v1, 0x0

    sput-object v1, Lcom/xiaomi/NetworkBoost/NetworkBoostService;->sInstance:Lcom/xiaomi/NetworkBoost/NetworkBoostService;

    .line 654
    :cond_0
    monitor-exit v0

    goto :goto_1

    :catchall_0
    move-exception v1

    monitor-exit v0
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    throw v1

    .line 656
    :cond_1
    :goto_1
    return-void
.end method

.method public static dumpModule(Ljava/io/FileDescriptor;Ljava/io/PrintWriter;[Ljava/lang/String;)V
    .locals 4
    .param p0, "fd"    # Ljava/io/FileDescriptor;
    .param p1, "writer"    # Ljava/io/PrintWriter;
    .param p2, "args"    # [Ljava/lang/String;

    .line 659
    sget-object v0, Lcom/xiaomi/NetworkBoost/NetworkBoostService;->sInstance:Lcom/xiaomi/NetworkBoost/NetworkBoostService;

    if-eqz v0, :cond_1

    .line 660
    const-class v0, Lcom/xiaomi/NetworkBoost/NetworkBoostService;

    monitor-enter v0

    .line 661
    :try_start_0
    sget-object v1, Lcom/xiaomi/NetworkBoost/NetworkBoostService;->sInstance:Lcom/xiaomi/NetworkBoost/NetworkBoostService;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    if-eqz v1, :cond_0

    .line 663
    :try_start_1
    sget-object v1, Lcom/xiaomi/NetworkBoost/NetworkBoostService;->sInstance:Lcom/xiaomi/NetworkBoost/NetworkBoostService;

    invoke-virtual {v1, p0, p1, p2}, Lcom/xiaomi/NetworkBoost/NetworkBoostService;->dump(Ljava/io/FileDescriptor;Ljava/io/PrintWriter;[Ljava/lang/String;)V
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_0
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 667
    goto :goto_0

    .line 665
    :catch_0
    move-exception v1

    .line 666
    .local v1, "e":Ljava/lang/Exception;
    :try_start_2
    const-string v2, "NetworkBoostService"

    const-string v3, "dumpModule dump catch:"

    invoke-static {v2, v3, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    .line 667
    nop

    .end local v1    # "e":Ljava/lang/Exception;
    goto :goto_0

    .line 670
    :cond_0
    const-string v1, "NetworkBoostService"

    const-string v2, "dumpModule: instance is uninitialized."

    invoke-static {v1, v2}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    .line 672
    :goto_0
    monitor-exit v0

    goto :goto_1

    :catchall_0
    move-exception v1

    monitor-exit v0
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    throw v1

    .line 675
    :cond_1
    const-string v0, "NetworkBoostService"

    const-string v1, "dumpModule: instance is uninitialized."

    invoke-static {v0, v1}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    .line 677
    :goto_1
    return-void
.end method

.method public static getInstance(Landroid/content/Context;)Lcom/xiaomi/NetworkBoost/NetworkBoostService;
    .locals 4
    .param p0, "context"    # Landroid/content/Context;

    .line 626
    sget-object v0, Lcom/xiaomi/NetworkBoost/NetworkBoostService;->sInstance:Lcom/xiaomi/NetworkBoost/NetworkBoostService;

    if-nez v0, :cond_1

    .line 627
    const-class v0, Lcom/xiaomi/NetworkBoost/NetworkBoostService;

    monitor-enter v0

    .line 628
    :try_start_0
    sget-object v1, Lcom/xiaomi/NetworkBoost/NetworkBoostService;->sInstance:Lcom/xiaomi/NetworkBoost/NetworkBoostService;

    if-nez v1, :cond_0

    .line 629
    new-instance v1, Lcom/xiaomi/NetworkBoost/NetworkBoostService;

    invoke-direct {v1, p0}, Lcom/xiaomi/NetworkBoost/NetworkBoostService;-><init>(Landroid/content/Context;)V

    sput-object v1, Lcom/xiaomi/NetworkBoost/NetworkBoostService;->sInstance:Lcom/xiaomi/NetworkBoost/NetworkBoostService;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 631
    :try_start_1
    sget-object v1, Lcom/xiaomi/NetworkBoost/NetworkBoostService;->sInstance:Lcom/xiaomi/NetworkBoost/NetworkBoostService;

    invoke-virtual {v1}, Lcom/xiaomi/NetworkBoost/NetworkBoostService;->onCreate()V
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_0
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 635
    goto :goto_0

    .line 633
    :catch_0
    move-exception v1

    .line 634
    .local v1, "e":Ljava/lang/Exception;
    :try_start_2
    const-string v2, "NetworkBoostService"

    const-string v3, "getInstance onCreate catch:"

    invoke-static {v2, v3, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    .line 637
    .end local v1    # "e":Ljava/lang/Exception;
    :cond_0
    :goto_0
    monitor-exit v0

    goto :goto_1

    :catchall_0
    move-exception v1

    monitor-exit v0
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    throw v1

    .line 639
    :cond_1
    :goto_1
    sget-object v0, Lcom/xiaomi/NetworkBoost/NetworkBoostService;->sInstance:Lcom/xiaomi/NetworkBoost/NetworkBoostService;

    return-object v0
.end method

.method private isSystemProcess()Z
    .locals 2

    .line 536
    invoke-static {}, Landroid/os/Binder;->getCallingUid()I

    move-result v0

    .line 537
    .local v0, "uid":I
    const/16 v1, 0x2710

    if-ge v0, v1, :cond_0

    const/4 v1, 0x1

    goto :goto_0

    :cond_0
    const/4 v1, 0x0

    :goto_0
    return v1
.end method


# virtual methods
.method protected dump(Ljava/io/FileDescriptor;Ljava/io/PrintWriter;[Ljava/lang/String;)V
    .locals 5
    .param p1, "fd"    # Ljava/io/FileDescriptor;
    .param p2, "writer"    # Ljava/io/PrintWriter;
    .param p3, "args"    # [Ljava/lang/String;

    .line 826
    const-string v0, "Dump of NetworkBoostService end"

    :try_start_0
    const-string v1, "Dump of NetworkBoostService begin:"

    invoke-virtual {p2, v1}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V

    .line 828
    iget-object v1, p0, Lcom/xiaomi/NetworkBoost/NetworkBoostService;->mStatusManager:Lcom/xiaomi/NetworkBoost/StatusManager;

    if-eqz v1, :cond_0

    .line 829
    invoke-virtual {v1, p2}, Lcom/xiaomi/NetworkBoost/StatusManager;->dumpModule(Ljava/io/PrintWriter;)V

    goto :goto_0

    .line 832
    :cond_0
    const-string v1, "mStatusManager is null"

    invoke-virtual {p2, v1}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V

    .line 836
    :goto_0
    iget-object v1, p0, Lcom/xiaomi/NetworkBoost/NetworkBoostService;->mSLAService:Lcom/xiaomi/NetworkBoost/slaservice/SLAService;

    if-eqz v1, :cond_1

    .line 837
    invoke-virtual {v1, p2}, Lcom/xiaomi/NetworkBoost/slaservice/SLAService;->dumpModule(Ljava/io/PrintWriter;)V

    goto :goto_1

    .line 840
    :cond_1
    const-string v1, "mSLAService is null"

    invoke-virtual {p2, v1}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V

    .line 844
    :goto_1
    iget-object v1, p0, Lcom/xiaomi/NetworkBoost/NetworkBoostService;->mNetworkSDKService:Lcom/xiaomi/NetworkBoost/NetworkSDK/NetworkSDKService;

    if-eqz v1, :cond_2

    .line 845
    invoke-virtual {v1, p2}, Lcom/xiaomi/NetworkBoost/NetworkSDK/NetworkSDKService;->dumpModule(Ljava/io/PrintWriter;)V

    goto :goto_2

    .line 848
    :cond_2
    const-string v1, "mNetworkSDKService is null"

    invoke-virtual {p2, v1}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V

    .line 851
    :goto_2
    iget-object v1, p0, Lcom/xiaomi/NetworkBoost/NetworkBoostService;->mNetworkAccelerateSwitchService:Lcom/xiaomi/NetworkBoost/NetworkAccelerateSwitch/NetworkAccelerateSwitchService;

    if-eqz v1, :cond_3

    .line 852
    invoke-virtual {v1, p2}, Lcom/xiaomi/NetworkBoost/NetworkAccelerateSwitch/NetworkAccelerateSwitchService;->dumpModule(Ljava/io/PrintWriter;)V

    goto :goto_3

    .line 855
    :cond_3
    const-string v1, "mNetworkAccelerateSwitchService is null"

    invoke-virtual {p2, v1}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V

    .line 858
    :goto_3
    invoke-virtual {p2, v0}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 869
    goto :goto_4

    .line 860
    :catch_0
    move-exception v1

    .line 861
    .local v1, "ex":Ljava/lang/Exception;
    const-string v2, "dump failed!"

    const-string v3, "NetworkBoostService"

    invoke-static {v3, v2, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    .line 863
    :try_start_1
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "Dump of NetworkBoostService failed:"

    invoke-virtual {v2, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {p2, v2}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V

    .line 864
    invoke-virtual {p2, v0}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_1

    .line 868
    goto :goto_4

    .line 866
    :catch_1
    move-exception v0

    .line 867
    .local v0, "e":Ljava/lang/Exception;
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "dump failure failed:"

    invoke-virtual {v2, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v3, v2}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 870
    .end local v0    # "e":Ljava/lang/Exception;
    .end local v1    # "ex":Ljava/lang/Exception;
    :goto_4
    return-void
.end method

.method public onBind(Landroid/content/Intent;)Landroid/os/IBinder;
    .locals 2
    .param p1, "intent"    # Landroid/content/Intent;

    .line 768
    const-string v0, "NetworkBoostService"

    const-string v1, " onBind "

    invoke-static {v0, v1}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 770
    const-class v0, Lcom/xiaomi/NetworkBoost/IAIDLMiuiNetworkBoost;

    invoke-virtual {v0}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 771
    iget-object v0, p0, Lcom/xiaomi/NetworkBoost/NetworkBoostService;->mAIDLMiuiNetworkBoost:Lcom/xiaomi/NetworkBoost/NetworkBoostService$NetworkBoostServiceManager;

    return-object v0

    .line 774
    :cond_0
    const-class v0, Lcom/xiaomi/NetworkBoost/slaservice/IAIDLSLAService;

    invoke-virtual {v0}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 775
    iget-object v0, p0, Lcom/xiaomi/NetworkBoost/NetworkBoostService;->mAIDLSLAServiceBinder:Lcom/xiaomi/NetworkBoost/NetworkBoostService$SLAServiceBinder;

    return-object v0

    .line 777
    :cond_1
    const/4 v0, 0x0

    return-object v0
.end method

.method public onCreate()V
    .locals 4

    .line 691
    const-string v0, " onCreate "

    const-string v1, "NetworkBoostService"

    invoke-static {v1, v0}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 694
    sget-boolean v0, Lcom/xiaomi/NetworkBoost/NetworkBoostService;->mJniLoaded:Z

    if-nez v0, :cond_0

    .line 696
    :try_start_0
    const-string v0, "networkboostjni"

    invoke-static {v0}, Ljava/lang/System;->loadLibrary(Ljava/lang/String;)V

    .line 697
    const/4 v0, 0x1

    sput-boolean v0, Lcom/xiaomi/NetworkBoost/NetworkBoostService;->mJniLoaded:Z

    .line 698
    const-string v0, "load libnetworkboostjni.so"

    invoke-static {v1, v0}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_0
    .catch Ljava/lang/UnsatisfiedLinkError; {:try_start_0 .. :try_end_0} :catch_1
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    .line 703
    :catch_0
    move-exception v0

    .line 704
    .local v0, "e":Ljava/lang/Exception;
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "libnetworkboostjni.so load failed:"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_1

    .line 700
    .end local v0    # "e":Ljava/lang/Exception;
    :catch_1
    move-exception v0

    .line 701
    .local v0, "e":Ljava/lang/UnsatisfiedLinkError;
    const-string v2, "load libnetworkboostjni.so failed"

    invoke-static {v1, v2, v0}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    .line 705
    .end local v0    # "e":Ljava/lang/UnsatisfiedLinkError;
    :goto_0
    nop

    .line 709
    :cond_0
    :goto_1
    sget-object v0, Lcom/xiaomi/NetworkBoost/NetworkBoostService;->mContext:Landroid/content/Context;

    invoke-static {v0}, Lcom/xiaomi/NetworkBoost/StatusManager;->getInstance(Landroid/content/Context;)Lcom/xiaomi/NetworkBoost/StatusManager;

    move-result-object v0

    iput-object v0, p0, Lcom/xiaomi/NetworkBoost/NetworkBoostService;->mStatusManager:Lcom/xiaomi/NetworkBoost/StatusManager;

    .line 712
    sget-object v0, Lcom/xiaomi/NetworkBoost/NetworkBoostService;->mContext:Landroid/content/Context;

    invoke-static {v0}, Lcom/xiaomi/NetworkBoost/slaservice/SLAService;->getInstance(Landroid/content/Context;)Lcom/xiaomi/NetworkBoost/slaservice/SLAService;

    move-result-object v0

    iput-object v0, p0, Lcom/xiaomi/NetworkBoost/NetworkBoostService;->mSLAService:Lcom/xiaomi/NetworkBoost/slaservice/SLAService;

    .line 715
    sget-object v0, Lcom/xiaomi/NetworkBoost/NetworkBoostService;->mContext:Landroid/content/Context;

    invoke-static {v0}, Lcom/xiaomi/NetworkBoost/NetworkSDK/NetworkSDKService;->getInstance(Landroid/content/Context;)Lcom/xiaomi/NetworkBoost/NetworkSDK/NetworkSDKService;

    move-result-object v0

    iput-object v0, p0, Lcom/xiaomi/NetworkBoost/NetworkBoostService;->mNetworkSDKService:Lcom/xiaomi/NetworkBoost/NetworkSDK/NetworkSDKService;

    .line 718
    sget-boolean v0, Lcom/xiaomi/NetworkBoost/NetworkBoostService;->mJniLoaded:Z

    if-eqz v0, :cond_1

    .line 720
    sget-object v0, Lcom/xiaomi/NetworkBoost/NetworkBoostService;->mContext:Landroid/content/Context;

    iget-object v2, p0, Lcom/xiaomi/NetworkBoost/NetworkBoostService;->mMiNetCallback:Lvendor/xiaomi/hidl/minet/V1_0/IMiNetCallback$Stub;

    invoke-static {v0, v2}, Lcom/xiaomi/NetworkBoost/NetworkAccelerateSwitch/NetworkAccelerateSwitchService;->getInstance(Landroid/content/Context;Lvendor/xiaomi/hidl/minet/V1_0/IMiNetCallback;)Lcom/xiaomi/NetworkBoost/NetworkAccelerateSwitch/NetworkAccelerateSwitchService;

    move-result-object v0

    iput-object v0, p0, Lcom/xiaomi/NetworkBoost/NetworkBoostService;->mNetworkAccelerateSwitchService:Lcom/xiaomi/NetworkBoost/NetworkAccelerateSwitch/NetworkAccelerateSwitchService;

    goto :goto_2

    .line 723
    :cond_1
    const-string v0, "WlanRamdumpCollector required libnetworkboostjni.so"

    invoke-static {v1, v0}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 727
    :goto_2
    sget-boolean v0, Lmiui/util/DeviceLevel;->IS_MIUI_MIDDLE_VERSION:Z

    if-nez v0, :cond_2

    .line 728
    const-string v0, "is not MIUI Middle or is not port MIUI Middle, start MscsService"

    invoke-static {v1, v0}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 729
    sget-object v0, Lcom/xiaomi/NetworkBoost/NetworkBoostService;->mContext:Landroid/content/Context;

    invoke-static {v0}, Lcom/xiaomi/NetworkBoost/MscsService/MscsService;->getInstance(Landroid/content/Context;)Lcom/xiaomi/NetworkBoost/MscsService/MscsService;

    move-result-object v0

    iput-object v0, p0, Lcom/xiaomi/NetworkBoost/NetworkBoostService;->mMscsService:Lcom/xiaomi/NetworkBoost/MscsService/MscsService;

    .line 732
    :cond_2
    const-string v0, "addService >>>"

    invoke-static {v1, v0}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 736
    :try_start_1
    iget-object v0, p0, Lcom/xiaomi/NetworkBoost/NetworkBoostService;->mAIDLMiuiNetworkBoost:Lcom/xiaomi/NetworkBoost/NetworkBoostService$NetworkBoostServiceManager;

    const-string/jumbo v2, "xiaomi.NetworkBoostServiceManager"

    invoke-static {v2, v0}, Landroid/os/ServiceManager;->addService(Ljava/lang/String;Landroid/os/IBinder;)V

    .line 737
    const-string v0, "addService NetworkBoost success"

    invoke-static {v1, v0}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_2

    .line 741
    goto :goto_3

    .line 739
    :catch_2
    move-exception v0

    .line 740
    .local v0, "e":Ljava/lang/Exception;
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "addService NetworkBoost failed:"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 743
    .end local v0    # "e":Ljava/lang/Exception;
    :goto_3
    :try_start_2
    iget-object v0, p0, Lcom/xiaomi/NetworkBoost/NetworkBoostService;->mAIDLSLAServiceBinder:Lcom/xiaomi/NetworkBoost/NetworkBoostService$SLAServiceBinder;

    const-string/jumbo v2, "xiaomi.SLAService"

    invoke-static {v2, v0}, Landroid/os/ServiceManager;->addService(Ljava/lang/String;Landroid/os/IBinder;)V

    .line 744
    const-string v0, "addService SLAService success"

    invoke-static {v1, v0}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_2
    .catch Ljava/lang/Exception; {:try_start_2 .. :try_end_2} :catch_3

    .line 748
    goto :goto_4

    .line 746
    :catch_3
    move-exception v0

    .line 747
    .restart local v0    # "e":Ljava/lang/Exception;
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "addService SLAService failed:"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 749
    .end local v0    # "e":Ljava/lang/Exception;
    :goto_4
    const-string v0, "addService <<<"

    invoke-static {v1, v0}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 751
    invoke-super {p0}, Landroid/app/Service;->onCreate()V

    .line 752
    return-void
.end method

.method public onDestroy()V
    .locals 2

    .line 788
    const-string v0, "NetworkBoostService"

    const-string v1, " onDestroy "

    invoke-static {v0, v1}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 791
    iget-object v0, p0, Lcom/xiaomi/NetworkBoost/NetworkBoostService;->mStatusManager:Lcom/xiaomi/NetworkBoost/StatusManager;

    const/4 v1, 0x0

    if-eqz v0, :cond_0

    .line 792
    invoke-static {}, Lcom/xiaomi/NetworkBoost/StatusManager;->destroyInstance()V

    .line 793
    iput-object v1, p0, Lcom/xiaomi/NetworkBoost/NetworkBoostService;->mStatusManager:Lcom/xiaomi/NetworkBoost/StatusManager;

    .line 797
    :cond_0
    iget-object v0, p0, Lcom/xiaomi/NetworkBoost/NetworkBoostService;->mSLAService:Lcom/xiaomi/NetworkBoost/slaservice/SLAService;

    if-eqz v0, :cond_1

    .line 798
    invoke-static {}, Lcom/xiaomi/NetworkBoost/slaservice/SLAService;->destroyInstance()V

    .line 799
    iput-object v1, p0, Lcom/xiaomi/NetworkBoost/NetworkBoostService;->mSLAService:Lcom/xiaomi/NetworkBoost/slaservice/SLAService;

    .line 803
    :cond_1
    iget-object v0, p0, Lcom/xiaomi/NetworkBoost/NetworkBoostService;->mNetworkSDKService:Lcom/xiaomi/NetworkBoost/NetworkSDK/NetworkSDKService;

    if-eqz v0, :cond_2

    .line 804
    invoke-static {}, Lcom/xiaomi/NetworkBoost/NetworkSDK/NetworkSDKService;->destroyInstance()V

    .line 805
    iput-object v1, p0, Lcom/xiaomi/NetworkBoost/NetworkBoostService;->mNetworkSDKService:Lcom/xiaomi/NetworkBoost/NetworkSDK/NetworkSDKService;

    .line 809
    :cond_2
    iget-object v0, p0, Lcom/xiaomi/NetworkBoost/NetworkBoostService;->mNetworkAccelerateSwitchService:Lcom/xiaomi/NetworkBoost/NetworkAccelerateSwitch/NetworkAccelerateSwitchService;

    if-eqz v0, :cond_3

    .line 810
    invoke-static {}, Lcom/xiaomi/NetworkBoost/NetworkAccelerateSwitch/NetworkAccelerateSwitchService;->destroyInstance()V

    .line 811
    iput-object v1, p0, Lcom/xiaomi/NetworkBoost/NetworkBoostService;->mNetworkAccelerateSwitchService:Lcom/xiaomi/NetworkBoost/NetworkAccelerateSwitch/NetworkAccelerateSwitchService;

    .line 814
    :cond_3
    iget-object v0, p0, Lcom/xiaomi/NetworkBoost/NetworkBoostService;->mMscsService:Lcom/xiaomi/NetworkBoost/MscsService/MscsService;

    if-eqz v0, :cond_4

    .line 815
    invoke-static {}, Lcom/xiaomi/NetworkBoost/MscsService/MscsService;->destroyInstance()V

    .line 816
    iput-object v1, p0, Lcom/xiaomi/NetworkBoost/NetworkBoostService;->mMscsService:Lcom/xiaomi/NetworkBoost/MscsService/MscsService;

    .line 819
    :cond_4
    invoke-super {p0}, Landroid/app/Service;->onDestroy()V

    .line 820
    return-void
.end method

.method public onNetworkPriorityChanged(III)V
    .locals 1
    .param p1, "priorityMode"    # I
    .param p2, "trafficPolicy"    # I
    .param p3, "thermalLevel"    # I

    .line 684
    iget-object v0, p0, Lcom/xiaomi/NetworkBoost/NetworkBoostService;->mStatusManager:Lcom/xiaomi/NetworkBoost/StatusManager;

    if-eqz v0, :cond_0

    .line 685
    invoke-virtual {v0, p1, p2, p3}, Lcom/xiaomi/NetworkBoost/StatusManager;->onNetworkPriorityChanged(III)V

    .line 687
    :cond_0
    return-void
.end method

.method public onStart(Landroid/content/Intent;I)V
    .locals 2
    .param p1, "intent"    # Landroid/content/Intent;
    .param p2, "startId"    # I

    .line 756
    const-string v0, " onStart "

    const-string v1, "NetworkBoostService"

    invoke-static {v1, v0}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 758
    if-eqz p1, :cond_0

    invoke-virtual {p1}, Landroid/content/Intent;->getExtras()Landroid/os/Bundle;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 759
    const-string v0, "onStart, get intent:"

    invoke-static {v1, v0}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 762
    :cond_0
    invoke-super {p0, p1, p2}, Landroid/app/Service;->onStart(Landroid/content/Intent;I)V

    .line 763
    return-void
.end method

.method public onUnbind(Landroid/content/Intent;)Z
    .locals 2
    .param p1, "intent"    # Landroid/content/Intent;

    .line 782
    const-string v0, "NetworkBoostService"

    const-string v1, " onUnbind "

    invoke-static {v0, v1}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 783
    invoke-super {p0, p1}, Landroid/app/Service;->onUnbind(Landroid/content/Intent;)Z

    move-result v0

    return v0
.end method
